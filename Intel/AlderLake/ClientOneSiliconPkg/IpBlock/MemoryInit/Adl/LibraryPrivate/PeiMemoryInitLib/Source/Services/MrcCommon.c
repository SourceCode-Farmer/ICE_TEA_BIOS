/** @file
  This file include all the common tolls for the mrc algo

@copyright
  INTEL CONFIDENTIAL
  Copyright 1999 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains an 'Intel Peripheral Driver' and is uniquely identified as
  "Intel Reference Module" and is licensed for Intel CPUs and chipsets under
  the terms of your license agreement with Intel or your vendor. This file may
  be modified by the user, subject to additional terms of the license agreement.

@par Specification Reference:
**/
// Include files
#include "MrcCommon.h"
#include "MrcReadDqDqs.h"
#include "MrcMaintenance.h"
#include "MrcCpgcApi.h"
#include "Cpgc20.h"
#include "Cpgc20Patterns.h"
#include "Cpgc20TestCtl.h"
#include "MrcMemoryApi.h"
#include "MrcCpgcOffsets.h"
#include "MrcLpddr4.h"
#include "MrcLpddr5.h"
#include "MrcLpddr5Registers.h"
#include "MrcDdr4.h"
#include "MrcDdr5.h"
#include "MrcGears.h"
#include "MrcChipApi.h"
#include "MrcCrosser.h"

/// Local Defines
#define MRC_CHNG_MAR_GRP_NUM  (2)
#define MRC_1D_ERROR_LEN      (1024)           //@todo_adl - find optimal value

#define MRC_LP5_TXDQ_CENTER         (512)
#define MRC_LP5_TXDQ_RANGE          (511)

#define VOC_DEFAULT           (0x10)


#define MRC_EXIT_VALUE (0xFF)

#ifdef MRC_DEBUG_PRINT
const char  CcdString[] = "Controller, Channel, Dimm";
const char  RcvEnDelayString[] = "RcvEnDelay";
const char  WrVDelayString[] = "WriteVoltage";
const char  RdVDelayString[] = "ReadVoltage";
const char  DqsDelayString[] = "DqsDelay";
const char  CmdVDelayString[] = "CmdVoltage";
#endif
///
/// Global Constant
///

/// Bank Array Function parameter is a constant, but due to compiler type mismatch
/// with GetSet function, cannot define as constant.
/// Set the bank mapping so that bank groups are toggled before banks (we will use logical banks 0 and 1 only).
/// Only update the first two Banks in the mapping.
/// Logical Bank:         0  1  2 ... 15
///                       --------------
/// Physical Bank Group:  0  1  X      X
/// Physical Bank:        0  0  X      X
MRC_BG_BANK_PAIR Ddr4RdMprBankL2p[2] = {{0, 0}, {1, 0}};

/// Set the bank mapping so that bank groups are toggled before banks (can use logical banks 0 to 7).
/// DDR4 x16 has 2 BA bits and 1 BG bit
/// 8 banks case (x8 or x16):
/// Logical Bank:         0  1  2  3  4  5  6  7
///                       ----------------------
/// Physical Bank:        0  0  1  1  2  2  3  3
/// Physical Bank Group:  0  1  0  1  0  1  0  1
MRC_BG_BANK_PAIR Ddr4x16BankMapB2B[MAX_DDR4_x16_BANKS] = {
  { 0,0 }, { 1,0 }, { 0,1 }, { 1,1 }, { 0,2 }, { 1,2 }, { 0,3 }, { 1,3 }
};

/// Set the bank mapping so that bank groups are toggled before banks (can use logical banks 0 to 15).
/// DDR4 x8 has 2 BA bits and 2 BG bits
/// 16 banks case (x8):
/// Logical Bank:         0  1  2  3  4  5  6  7  8  9  10 11 12 13 14 15
///                       -----------------------------------------------
/// Physical Bank:        0  0  0  0  1  1  1  1  2  2  2  2  3  3  3  3
/// Physical Bank Group:  0  1  2  3  0  1  2  3  0  1  2  3  0  1  2  3
MRC_BG_BANK_PAIR Ddr4x8BankMapB2B[MAX_DDR4_x8_BANKS] = {
  { 0,0 }, { 1,0 }, { 2,0 }, { 3,0 }, { 0,1 }, { 1,1 }, { 2,1 }, { 3,1 },
  { 0,2 }, { 1,2 }, { 2,2 }, { 3,2 }, { 0,3 }, { 1,3 }, { 2,3 }, { 3,3 }
};

/// Set the bank mapping so that bank groups are toggled before banks (can use logical banks 0 to 7).
/// DDR5 8Gb x16 has 1 BA bit and 2 BG bits, 8 banks total
/// Logical Bank:         0  1  2  3  4  5  6  7
///                       ----------------------
/// Physical Bank:        0  0  0  0  1  1  1  1
/// Physical Bank Group:  0  1  2  3  0  1  2  3
MRC_BG_BANK_PAIR Ddr5_8Gbx16BankMapB2B[MAX_DDR5_8Gb_x16_BANKS] = {
  { 0,0 }, { 1,0 }, { 2,0 }, { 3,0 }, { 0,1 }, { 1,1 }, { 2,1 }, { 3,1 }
};

/// Set the bank mapping so that bank groups are toggled before banks (can use logical banks 0 to 15).
/// DDR5 8Gb x8 has 1 BA bit and 3 BG bits, 16 banks total
/// Logical Bank:         0  1  2  3  4  5  6  7  8  9  10 11 12 13 14 15
///                       -----------------------------------------------
/// Physical Bank:        0  0  0  0  0  0  0  0  1  1  1  1  1  1  1  1
/// Physical Bank Group:  0  1  2  3  4  5  6  7  0  1  2  3  4  5  6  7
MRC_BG_BANK_PAIR Ddr5_8Gbx8BankMapB2B[MAX_DDR5_8Gb_x8_BANKS] = {
  { 0,0 }, { 1,0 }, { 2,0 }, { 3,0 }, { 4,0 }, { 5,0 }, { 6,0 }, { 7,0 },
  { 0,1 }, { 1,1 }, { 2,1 }, { 3,1 }, { 4,1 }, { 5,1 }, { 6,1 }, { 7,1 }
};

/// Set the bank mapping so that bank groups are toggled before banks (can use logical banks 0 to 15).
/// DDR5 16Gb x8 has 2 BA bits and 3 BG bits, 32 banks total
/// Logical Bank:         0  1  2  3  4  5  6  7  8  9  10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30 31
///                       -----------------------------------------------------------------------------------------------
/// Physical Bank:        0  0  0  0  0  0  0  0  1  1  1  1  1  1  1  1  2  2  2  2  2  2  2  2  3  3  3  3  3  3  3  3
/// Physical Bank Group:  0  1  2  3  4  5  6  7  0  1  2  3  4  5  6  7  0  1  2  3  4  5  6  7  0  1  2  3  4  5  6  7
MRC_BG_BANK_PAIR Ddr5_16Gbx8BankMapB2B[MAX_DDR5_16Gb_x8_BANKS] = {
  { 0,0 }, { 1,0 }, { 2,0 }, { 3,0 }, { 4,0 }, { 5,0 }, { 6,0 }, { 7,0 },
  { 0,1 }, { 1,1 }, { 2,1 }, { 3,1 }, { 4,1 }, { 5,1 }, { 6,1 }, { 7,1 },
  { 0,2 }, { 1,2 }, { 2,2 }, { 3,2 }, { 4,2 }, { 5,2 }, { 6,2 }, { 7,2 },
  { 0,3 }, { 1,3 }, { 2,3 }, { 3,3 }, { 4,3 }, { 5,3 }, { 6,3 }, { 7,3 }
};

/// Galois MPR Bit Mask for LFSR Assignment: 0 - LFSR0, 1 - LFSR1
/// Victim bit is associated with LFSR0
/// Aggressor bit is associated with LFSR1
UINT8 GaloisMprBitMask[8] = {0xFE, 0xFD, 0xFB, 0xF7, 0xEF, 0xDF, 0xBF, 0x7F};

UINT32 DataGaloisSeeds[MRC_NUM_MUX_SEEDS] = {0x5A, 0x3C, 0xF0};

UINT8  ResultsString[3][3] = { "P\0", ".\0","*\0" };

/**
  Verify that the indicated socket is present and enabled.

  @param[in] MrcData    - Pointer to the MRC global data area.
  @param[in] Socket     - Zero based CPU socket number.

  @retval Success if present and enabled, otherwise SocketNotSupported.
**/
MrcStatus
IsSocketPresent (
  const MrcParameters * const MrcData,
  const UINT32                Socket
  )
{
  return ((Socket < MAX_CPU_SOCKETS) ? mrcSuccess : mrcSocketNotSupported);
}

/**
  Verify that the indicated controller is present and enabled.

  @param[in] MrcData    - Pointer to the MRC global data area.
  @param[in] Socket     - Zero based CPU socket number.
  @param[in] Controller - Zero based memory controller number.

  @retval Success if present and enabled, otherwise ControllerNotSupported.
**/
MrcStatus
IsControllerPresent (
  const MrcParameters * const MrcData,
  const UINT32                Socket,
  const UINT32                Controller
  )
{
  MrcStatus Status;

  Status = IsSocketPresent (MrcData, Socket);
  if (Status == mrcSuccess) {
    return (((Controller < MAX_CONTROLLER) &&
    (MrcData->Outputs.Controller[Controller].Status == CONTROLLER_PRESENT)) ?
            mrcSuccess : mrcControllerNotSupported);
  }
  return (Status);
}

/**
  Verify that the indicated channel is present and enabled.

  @param[in] MrcData    - Pointer to the MRC global data area.
  @param[in] Socket     - Zero based CPU socket number.
  @param[in] Controller - Zero based memory controller number.
  @param[in] Channel    - Zero based memory channel number.

  @retval Success if present and enabled, otherwise ChannelNotSupported.
**/
MrcStatus
IsChannelPresent (
  const MrcParameters * const MrcData,
  const UINT32                Socket,
  const UINT32                Controller,
  const UINT32                Channel
  )
{
  MrcStatus Status;

  Status = IsControllerPresent (MrcData, Socket, Controller);
  if (Status == mrcSuccess) {
    return (((Channel < MAX_CHANNEL) &&
    (MrcData->Outputs.Controller[Controller].Channel[Channel].Status == CHANNEL_PRESENT)) ?
            mrcSuccess : mrcChannelNotSupport);
  }
  return (Status);
}

/**
  Verify that the indicated DIMM is present and enabled.

  @param[in] MrcData    - Pointer to the MRC global data area.
  @param[in] Socket     - Zero based CPU socket number.
  @param[in] Controller - Zero based memory controller number.
  @param[in] Channel    - Zero based memory channel number.
  @param[in] Dimm       - Zero based memory DIMM number.

  @retval Success if present and enabled, otherwise DimmNotSupported.
**/
MrcStatus
IsDimmPresent (
  const MrcParameters * const MrcData,
  const UINT32                Socket,
  const UINT32                Controller,
  const UINT32                Channel,
  const UINT32                Dimm
  )
{
  MrcStatus Status;

  Status = IsChannelPresent (MrcData, Socket, Controller, Channel);
  if (Status == mrcSuccess) {
    return (((Dimm < MAX_DIMMS_IN_CHANNEL) &&
    (MrcData->Outputs.Controller[Controller].Channel[Channel].Dimm[Dimm].Status == DIMM_PRESENT)) ?
            mrcSuccess : mrcDimmNotSupport);
  }
  return (Status);
}

/**
  Verify that the indicated rank is present and enabled.

  @param[in] MrcData    - Pointer to the MRC global data area.
  @param[in] Socket     - Zero based CPU socket number.
  @param[in] Controller - Zero based memory controller number.
  @param[in] Channel    - Zero based memory channel number.
  @param[in] Dimm       - Zero based memory DIMM number.
  @param[in] Rank       - Zero based memory rank number in the DIMM.

  @retval Success if present and enabled, otherwise RankNotSupported.
**/
MrcStatus
IsRankPresent (
  const MrcParameters * const MrcData,
  const UINT32                Socket,
  const UINT32                Controller,
  const UINT32                Channel,
  const UINT32                Dimm,
  const UINT32                Rank
  )
{
  MrcStatus Status;

  Status = IsDimmPresent (MrcData, Socket, Controller, Channel, Dimm);
  if (Status == mrcSuccess) {
    return (((Rank < MAX_RANK_IN_DIMM) &&
             (((UINT32) MrcData->Outputs.Controller[Controller].Channel[Channel].Dimm[Dimm].RankInDimm - 1) >= Rank)) ?
             mrcSuccess : mrcRankNotSupported);
  }
  return (Status);
}

/**
  Return the rank mask if the rank exists in the Controller/Channel.

  @param[in] MrcData    - Pointer to MRC global data.
  @param[in] Controller - Controller to work on.
  @param[in] Channel    - Channel to work on.
  @param[in] Rank       - Rank to check.

  @retval Bit mask of Rank requested if the Rank exists in the system.
**/
UINT8
MrcRankExist (
  IN MrcParameters *const MrcData,
  IN const UINT32         Controller,
  IN const UINT32         Channel,
  IN const UINT32         Rank
  )
{
  MrcOutput *Outputs;

  Outputs = &MrcData->Outputs;

  if ((Controller < MAX_CONTROLLER) && (Channel < Outputs->MaxChannels) && (Rank < MAX_RANK_IN_CHANNEL)) {
    return ((MRC_BIT0 << Rank) & MrcData->Outputs.Controller[Controller].Channel[Channel].ValidRankBitMask);
  } else {
    return 0;
  }
}

/**
  Checks if a given sub-channel is populated in a given channel

  @param[in] MrcData    - Pointer to MRC global data.
  @param[in] Channel    - Channel to work on.
  @param[in] SubChannel - Sub-Channel to work on.

  @retval TRUE if exists, FALSE otherwise.
**/
BOOLEAN
MrcSubChannelExist (
  IN MrcParameters *const MrcData,
  IN const UINT32         Channel,
  IN const UINT32         SubChannel
  )
{
  MrcOutput     *Outputs;
  MrcChannelOut *ChannelOut;

  Outputs     = &MrcData->Outputs;
  ChannelOut  = &Outputs->Controller[0].Channel[Channel];

  if ((SubChannel == 0) || !Outputs->EnhancedChannelMode) {
    // If channel is present, then subch 0 is always present
    // In x64 channel mode both subch are present
    return (ChannelOut->Status == CHANNEL_PRESENT);
  } else {
    return (Outputs->EnhancedChannelMode && ((1 << SubChannel) & ChannelOut->ValidSubChBitMask));
  }
}

/**
  This function checks if the requested byte in the channel exists.

  @param[in]  MrcData - Pointer to MRC global data.
  @param[in]  Channel - Channel to check.
  @param[in]  Byte    - Byte to check.

  @retval BOOLEAN - TRUE if exists, FALSE otherwise.
**/
BOOLEAN
MrcByteInChannelExist (
  IN  MrcParameters *const  MrcData,
  IN  const UINT32          Channel,
  IN  const UINT32          Byte
  )
{
  MrcOutput     *Outputs;
  MrcChannelOut *ChannelOut;

  Outputs     = &MrcData->Outputs;
  ChannelOut  = &Outputs->Controller[0].Channel[Channel];

  return ((1 << Byte) & ChannelOut->ValidByteMask) != 0;
}

/**
  This function checks if the requested byte in the channel exists.

  @param[in] MrcData    - Pointer to MRC global data.
  @param[in] Controller - Controller  to check.
  @param[in] Channel    - Channel to check.
  @param[in] Byte       - Byte to check.

  @retval BOOLEAN - TRUE if exists, FALSE otherwise.
**/
BOOLEAN
MrcByteExist (
  IN  MrcParameters *const  MrcData,
  IN  const UINT32          Controller,
  IN  const UINT32          Channel,
  IN  const UINT32          Byte
  )
{
  MrcOutput     *Outputs;
  MrcChannelOut *ChannelOut;

  Outputs     = &MrcData->Outputs;
  ChannelOut  = &Outputs->Controller[Controller].Channel[Channel];

  return ((1 << Byte) & ChannelOut->ValidByteMask) != 0;
}

/**
  Return the number of ranks in specific dimm.

  @param[in] MrcData - Pointer to MRC global data.
  @param[in] Channel - Channel to work on.
  @param[in] Dimm    - Dimm in channel to return.

  @retval The number of ranks in the dimm.
**/
UINT8
MrcGetRankInDimm (
  IN MrcParameters *const MrcData,
  IN const UINT8          Dimm,
  IN const UINT8          Channel
  )
{
  return MrcData->Outputs.Controller[0].Channel[Channel].Dimm[Dimm].RankInDimm;
}

/**
  Returns whether Controller is or is not present.

  @param[in] MrcData    - Pointer to MRC global data.
  @param[in] Controller - Controller to test.

  @retval BOOLEAN - TRUE if exists, FALSE otherwise.
**/
BOOLEAN
MrcControllerExist (
  IN MrcParameters *const MrcData,
  IN const UINT32         Controller
  )
{
  if (Controller < MAX_CONTROLLER) {
    if (MrcData->Outputs.Controller[Controller].Status == CONTROLLER_PRESENT) {
      return TRUE;
    }
  }
  return FALSE;
}

/**
  Returns whether Channel is or is not present.

  @param[in] MrcData    - Pointer to MRC global data.
  @param[in] Controller - Controller of the channel.
  @param[in] Channel    - Channel to test.

  @retval TRUE  - if there is at least one enabled DIMM in the channel.
  @retval FALSE - if there are no enabled DIMMs in the channel.
**/
BOOLEAN
MrcChannelExist (
  IN MrcParameters *const MrcData,
  IN const UINT32         Controller,
  IN const UINT32         Channel
  )
{
  if ((Channel < MAX_CHANNEL) && (Controller < MAX_CONTROLLER)) {
    if (MrcData->Outputs.Controller[Controller].Channel[Channel].Status == CHANNEL_PRESENT) {
      return TRUE;
    }
  }
  return FALSE;
}

/**
  This function disable channel parameters.
  After this function the MRC don't use with the channel.

  @param[in] MrcData           - Include all MRC global data.
  @param[in] ChannelToDisable  - Channel to disable.
  @param[in] SkipDimmCapacity  - Switch to skip setting the DimmCapacity to 0 for the dimms in the channel disabled.

  @retval Nothing
**/
void
MrcChannelDisable (
  IN MrcParameters *const MrcData,
  IN const UINT8          ChannelToDisable,
  IN const UINT8          SkipDimmCapacity
  )
{
  MrcChannelOut *ChannelOut;
  MrcDimmOut    *DimmOut;
  UINT32        Dimm;

  ChannelOut = &MrcData->Outputs.Controller[0].Channel[ChannelToDisable];
  if (ChannelOut->Status == CHANNEL_PRESENT) {
    ChannelOut->Status            = CHANNEL_DISABLED;
    ChannelOut->ValidRankBitMask  = 0;
    for (Dimm = 0; Dimm < MAX_DIMMS_IN_CHANNEL; Dimm++) {
      DimmOut = &ChannelOut->Dimm[Dimm];
      if (DimmOut->Status == DIMM_PRESENT) {
        DimmOut->Status     = DIMM_DISABLED;
        DimmOut->RankInDimm = 0;
        if (!SkipDimmCapacity) {
          DimmOut->DimmCapacity = 0;
        }
      }
    }
  }
}

/**
  This function implements the flow to properly switch MC PLL during run time.

  @params[in] MrcData     - Pointer to MRC global data.
  @params[in] NewFreq     - The new frequency to lock MC PLL.
  @params[in] Gear        - Integer based value of the new Gear Ratio for MC PLL, see MRC_GEAR_TYPE
  @params[in] DebugPrint  - Boolean parameter to enable/disable debug messages for the callee.

  @retval MrcStatus - mrcSuccess if frequency is updated properly, otherwise an error status.
**/
MrcStatus
MrcFrequencySwitch (
  IN  MrcParameters *const  MrcData,
  IN  MrcFrequency          NewFreq,
  IN  UINT8                 Gear,
  IN  BOOLEAN               DebugPrint
  )
{
  MrcInput  *Inputs;
  MrcOutput *Outputs;
  MrcStatus Status;

  Inputs = &MrcData->Inputs;
  Outputs = &MrcData->Outputs;
  Outputs->Frequency = NewFreq;
  Outputs->Gear2 = (Gear == MrcGear2);
  Outputs->Gear4 = (Gear == MrcGear4);
  MrcSetGear (MrcData);
  Status = McFrequencySet (MrcData, TRUE, DebugPrint);
  if (Status != mrcSuccess) {
    return Status;
  }

  // Restore PHY calibration values.
  if (Outputs->Lpddr && Inputs->LpFreqSwitch) {
    Status = MrcLpFreqCalibrationRestore (MrcData);
    if (Status != mrcSuccess) {
      return Status;
    }
  }

  ForceRcomp (MrcData);

  return Status;
}

/**
  Convert the given frequency and reference clock to a clock ratio.

  @param[in] MrcData   - Pointer to MRC global data.
  @param[in] Frequency - The memory frequency.
  @param[in] RefClk    - The memory reference clock.
  @param[in] BClk      - The base system reference clock.

  @retval Returns the memory clock ratio.
**/
MrcClockRatio
MrcFrequencyToRatio (
  IN MrcParameters *const  MrcData,
  IN const MrcFrequency    Frequency,
  IN const MrcRefClkSelect RefClk,
  IN const MrcBClkRef      BClk
  )
{
  const MRC_FUNCTION *MrcCall;
  UINT64 Value;
  UINT64 FreqValue;
  UINT32 RefClkValue;
  UINT32 BClkValue;

  MrcCall     = MrcData->Inputs.Call.Func;
  BClkValue   = (BClk == 0) ? (BCLK_DEFAULT / 100000) : (BClk / 100000);
  RefClkValue = (RefClk == MRC_REF_CLOCK_100) ? 100000 : 133333;
  FreqValue   = MrcCall->MrcMultU64x32 (Frequency, 1000000000ULL);
  Value       = MrcCall->MrcDivU64x64 (FreqValue, (RefClkValue * BClkValue), NULL);
  Value       = ((UINT32) Value + 500) / 1000;
  return ((MrcClockRatio) Value);
}

/**
  Convert the given ratio and reference clocks to a memory frequency.

  @param[in] MrcData - Pointer to MRC global data.
  @param[in] Ratio   - The memory ratio.
  @param[in] RefClk  - The memory reference clock.
  @param[in] BClk    - The base system reference clock.

  @retval Returns the memory frequency.
**/
MrcFrequency
MrcRatioToFrequency (
  IN MrcParameters *const  MrcData,
  IN const MrcClockRatio   Ratio,
  IN const MrcRefClkSelect RefClk,
  IN const MrcBClkRef      BClk
  )
{
  const MrcInput     *Inputs;
  const MRC_FUNCTION *MrcCall;
  MrcClockRatio      LocalRatio;
  UINT64 Value;
  UINT32 BClkValue;
  UINT32 RefClkValue;

  Inputs  = &MrcData->Inputs;
  MrcCall = Inputs->Call.Func;

  LocalRatio  = Ratio;
  BClkValue   = (BClk == 0) ? BCLK_DEFAULT : BClk;
  RefClkValue = (RefClk == MRC_REF_CLOCK_100) ? 100000000 : 133333333;
  Value = BClkValue;
  Value = MrcCall->MrcMultU64x32 (Value, LocalRatio);
  Value = MrcCall->MrcMultU64x32 (Value, RefClkValue);
  Value += 50000000000000ULL;
  Value = MrcCall->MrcDivU64x64 (Value, 100000000000000ULL, NULL);
  return ((MrcFrequency) Value);
}

/**
  Convert the given ratio and reference clocks to a memory clock.

  @param[in] Ratio  - The memory ratio.
  @param[in] RefClk - The memory reference clock.
  @param[in] BClk   - The base system reference clock.

  @retval Returns the memory clock in femtoseconds.
**/
UINT32
MrcRatioToClock (
  IN MrcParameters *const  MrcData,
  IN const MrcClockRatio   Ratio,
  IN const MrcRefClkSelect RefClk,
  IN const MrcBClkRef      BClk
  )
{
  const MrcInput     *Inputs;
  const MRC_FUNCTION *MrcCall;
  UINT32 BClkValue;
  UINT32 RefClkValue;
  UINT32 Factor;
  UINT64 Value;

  Inputs  = &MrcData->Inputs;
  MrcCall = Inputs->Call.Func;

  BClkValue   = (BClk == 0) ? 100000000UL : BClk;
  Factor      = BClkValue / 100000UL;
  RefClkValue = (RefClk == MRC_REF_CLOCK_100) ? 500000000UL : 666666667UL;
  Value       = MrcCall->MrcMultU64x32 (Factor, RefClkValue);
  Value       = MrcCall->MrcMultU64x32 (Value, Ratio);
  return ((Value == 0) ? 0 : (UINT32) MrcCall->MrcDivU64x64 (10000000000000000000ULL, Value, NULL));
}

/**
  This function determines the number of PI ticks required to cover the delay requested.
  This function will ceiling the result such that the PI tick delay may be greater
  than the requested time.

  @param[in]  MrcData - Pointer to MRC global data.
  @param[in]  TimeFs  - The delay in Femtoseconds to convert to PI ticks.

  @retval UINT32 - The number of PI ticks to reach the delay requested.
**/
UINT32
MrcFemptoSec2PiTick (
  IN  MrcParameters *const  MrcData,
  IN  const UINT32          TimeFs
  )
{
  UINT32  DataRateFs;
  UINT32  PiTickFs;
  UINT32  PiCount;

  DataRateFs = MrcData->Outputs.MemoryClock / 2;

  // Pi Tick is 1/64th a Data Rate UI.
  PiTickFs  = UDIVIDEROUND (DataRateFs, 64);
  PiCount   = DIVIDECEIL (TimeFs, PiTickFs);

  return PiCount;
}

/**
  This function return the DIMM number according to the rank number.

  @param[in] Rank - The requested rank.

  @retval The DIMM number.
**/
UINT8
MrcGetDimmFromRank (
  IN const UINT8 Rank
  )
{
  UINT8 Dimm;

  if (Rank == rRank0 || Rank == rRank1) {
    Dimm = dDIMM0;
  } else {
    Dimm = dDIMM1;
  }

  return Dimm;
}

/**
  This function sets the memory frequency.

  @param[in] MrcData           - Include all MRC global data.
  @param[in] LpJedecFreqSwitch - Boolean indicator that the current frequency update is for the LPDDR frequency switch flow.
  @param[in] DebugPrint        - Enable/disable debug printing

  @retval mrcSuccess on success, mrcFrequencyError on error.
**/
MrcStatus
McFrequencySet (
  IN MrcParameters *const MrcData,
  IN BOOLEAN              LpJedecFreqSwitch,
  IN BOOLEAN              DebugPrint
  )
{
  const MRC_FUNCTION  *MrcCall;
  MrcDebug            *Debug;
  MrcDebugMsgLevel    DebugLevel;
  const MrcInput      *Inputs;
  MrcOutput           *Outputs;
  MrcClockRatio       Ratio;
  MrcRefClkSelect     RefClk;
  MrcStatus           Status;
  INT64               GetSetVal;
  UINT32              MemoryClock;
  UINT32              Vddq;
  UINT32              FinalVddq;
  UINT32              IccMax;
  UINT16              BaseClock;
  BOOLEAN             Gear2;
  BOOLEAN             Gear4;
  UINT64              Timeout;
  MC_BIOS_REQ_PCU_STRUCT    McBiosReq;
  MC_BIOS_DATA_PCU_STRUCT   McBiosData;
#ifdef MRC_DEBUG_PRINT
  UINT32                    MailBoxData;
  UINT32                    MailboxStatus;
  char                      *Str;
#endif

  Inputs  = &MrcData->Inputs;
  MrcCall = Inputs->Call.Func;
  Outputs = &MrcData->Outputs;
  Debug   = &Outputs->Debug;
  DebugLevel  = DebugPrint ? MSG_LEVEL_NOTE : MSG_LEVEL_NEVER;
  Status      = mrcSuccess;
  Gear2       = Outputs->Gear2;
  Gear4       = Outputs->Gear4;
  IccMax      = 0xF; // 4A for default ICCMAX

  if (MrcReadCR (MrcData, MC_BIOS_DATA_PCU_REG) != 0) {  // Check if PLL is locked
    // Disable DCLK
    GetSetVal = 0;
    MrcGetSetMc (MrcData, MAX_CONTROLLER, GsmMccEnableDclk, WriteNoCache, &GetSetVal);
  }

  if ((Inputs->MemoryProfile == CUSTOM_PROFILE1) && (Inputs->Ratio > 0) && (Inputs->BootMode == bmCold) && !LpJedecFreqSwitch) {
    Outputs->Ratio = Inputs->Ratio;
  } else {
    Outputs->Ratio = MrcFrequencyToRatio (MrcData, Outputs->Frequency, Outputs->RefClk, Inputs->BClkFrequency);
  }

  McBiosReq.Data           = 0;
  McBiosReq.Bits.MC_PLL_RATIO = Gear4 ? (Outputs->Ratio / 4) : (Gear2 ? (Outputs->Ratio / 2) : Outputs->Ratio);
  McBiosReq.Bits.MC_PLL_REF   = (Outputs->RefClk == MRC_REF_CLOCK_133) ? 0 : 1;
  McBiosReq.Bits.GEAR         = Gear4 ? 2 : Gear2; //  0: Gear1, 1: Gear2,  2: Gear4
  Vddq = Outputs->VccddqVoltage;
  FinalVddq = (Vddq / 5); //mV
  if ((Inputs->MemoryProfile == STD_PROFILE) || !Outputs->IsOverclockCapable) {
    /*
     *  At OverClock, usually, VCCVDDQ need be raised.
     *  If chipset is capable of OverClock, don't check the limit.
     */
    FinalVddq = MIN (Outputs->VccddqLimit, FinalVddq);
  }
  if (Inputs->UlxUlt) {
    //
    // DDQ FIVR power state
    // DCM: ICCMAX < 2A, CCM: ICCMAX > 2A
    //
    IccMax = (Outputs->Frequency > f5200) ? 0x9 : 0x7;
  }
  McBiosReq.Bits.REQ_VDDQ_TX_VOLTAGE = FinalVddq;
  McBiosReq.Bits.REQ_VDDQ_TX_ICCMAX = IccMax;
  McBiosReq.Bits.RUN_BUSY  = 1;

  MrcWriteCR (MrcData, MC_BIOS_REQ_PCU_REG, McBiosReq.Data);
  MRC_DEBUG_MSG (Debug, DebugLevel, "Attempting value = 0x%x - Pll busy wait ", McBiosReq.Data);

  Timeout = MrcCall->MrcGetCpuTime () + MRC_WAIT_TIMEOUT;  // 10 sec timeout
  while (McBiosReq.Bits.RUN_BUSY && (MrcCall->MrcGetCpuTime () < Timeout)) {
    McBiosReq.Data = MrcReadCR (MrcData, MC_BIOS_REQ_PCU_REG);
  }

  // Read the DATA REG to check RATIO result
  McBiosData.Data   = MrcReadCR (MrcData, MC_BIOS_DATA_PCU_REG);
  if (McBiosReq.Bits.RUN_BUSY || (McBiosData.Bits.MC_PLL_RATIO == 0)) {
#ifdef MRC_DEBUG_PRINT
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_ERROR, "%s DDR frequency Update FAILED!\n",(McBiosReq.Bits.RUN_BUSY) ? "- TIMEOUT." :"- ERROR.");
    // Read the error PCode command READ_BIOS_MC_REQ_ERROR for DDR checks where the ratio is not locked.
    MailBoxData   = 0;
    MailboxStatus = 0;
    MrcCall->MrcCpuMailboxRead (
      MAILBOX_TYPE_PCODE,
      MAILBOX_BIOS_CMD_READ_BIOS_MC_REQ_ERROR,
      &MailBoxData,
      &MailboxStatus
    );
    if (MailboxStatus == PCODE_MAILBOX_CC_SUCCESS) {  // Command always returns success
      if (MailBoxData == MC_REQ_ERROR_DDR_CHECKS_DDR_CHECKS_PASSED) {
        Str = "DDR Checks Passed\n";
      } else if (MailBoxData == MC_REQ_ERROR_DDR_CHECKS_ILLEGAL_BASE_FREQ) {
        Str = "Illegal Base Frequency - Request type in MCHBAR_PCU_REQ_TYPE Is Undefined\n";
      } else if (MailBoxData == MC_REQ_ERROR_DDR_CHECKS_FAILED_LT_CONDITIONS) {
        Str = "LT Conditions Not Met\n";
      } else if (MailBoxData == MC_REQ_ERROR_DDR_CHECKS_DDR_FREQ_BELOW_MIN) {
        Str = "Frequency Below Minimum\n";
      } else if (MailBoxData == MC_REQ_ERROR_DDR_CHECKS_DDR_FREQ_ABOVE_OC_LIMIT) {
        Str = "Frequency Above OC Limit\n";
      } else if (MailBoxData == MC_REQ_ERROR_DDR_CHECKS_DDR_FREQ_ABOVE_MAX_DDR_RATE_LIMIT) {
        Str = "Frequency Above Max DDR Rate\n";
      } else if (MailBoxData == MC_REQ_ERROR_DDR_CHECKS_DDR_FREQ_ABOVE_MAX_GEAR1_LIMIT) {
        Str = "Frequency Above Max GEAR1 Limit\n";
      } else if (MailBoxData == MC_REQ_ERROR_DDR_CHECKS_VDDQ_TX_VOLTAGE_ABOVE_MAX_VDDQ_TX_LIMIT) {
        Str = "VddQ_TX VID Above MAX_VDDQ_TX_LIMIT\n";
      } else if (MailBoxData == MC_REQ_ERROR_DDR_CHECKS_DDR_FREQ_100_REF_IS_DISABLED) {
        Str = "100MHz RefClk Is Disabled\n";
      } else if (MailBoxData == MC_REQ_ERROR_DDR_CHECKS_MRC_TRAINING_RFI_POINT_MISMATCH) {
        Str = "MRC Trained Point Is Different From RFI N Point\n";
      } else {
        Str = "Undefined PCode Error Status\n";
      }
      MRC_DEBUG_MSG (Debug, MSG_LEVEL_ERROR, "%s %s", gErrString, Str);
    }
#endif
    return mrcFrequencyError;
  } else {
    MRC_DEBUG_MSG (Debug, DebugLevel, "- done\n");
  }

  Outputs->Frequency = MrcGetCurrentMemoryFrequency (MrcData, &MemoryClock, &Ratio, &RefClk);
  Outputs->MemoryClock = (Outputs->DdrType == MRC_DDR_TYPE_LPDDR5) ? (MemoryClock * 4) : MemoryClock;

  MRC_DEBUG_MSG (
    Debug,
    DebugLevel,
    "Requested/actual ratio %u/%u, Frequency=%u, Gear2=%u, Gear4=%u",
    Outputs->Ratio,
    Ratio,
    Outputs->Frequency,
    Outputs->Gear2,
    Outputs->Gear4
    );

  MRC_DEBUG_MSG (
    Debug,
    DebugLevel,
    ", BClk=%uHz RefClk=%uMHz, tCK=%ufs\n",
    Inputs->BClkFrequency,
    (RefClk == MRC_REF_CLOCK_133) ? 133 : 100,
    MemoryClock
    );

  FinalVddq *= 5;
  if (FinalVddq < (UINT32) Outputs->VccddqVoltage) {
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "VccDDQ clamped by CPU Max.  Updating VccddqVoltage\n");
    Outputs->VccddqVoltage = FinalVddq;
  }
  MRC_DEBUG_MSG (Debug, DebugLevel, "VDDQ - TX_VOLTAGE: %d ICCMAX: %d\n", Outputs->VccddqVoltage, IccMax);

  if (Ratio != Outputs->Ratio) {
    if (Ratio == 0) {
      MRC_DEBUG_MSG (Debug, MSG_LEVEL_ERROR, "ERROR: MC PLL did not lock.\n");
    }
    return mrcFrequencyError;
  }

  // Enable Dclk
  GetSetVal = 1;
  MrcGetSetMc (MrcData, MAX_CONTROLLER, GsmMccEnableDclk, WriteNoCache, &GetSetVal);

  Outputs->tCKps  = (UINT16) (Outputs->MemoryClock / 1000);     // Memory clock period in pS (external bus clock)
  Outputs->Wckps  = Outputs->tCKps / 4;                         // Write Clock period in pS; 4:1 mode.
  // Determine the base clock for UI/QCLK/DCLK
  // | DDR Type | Clock |
  // |==========|=======|
  // |  LPDDR5  | WCK   |
  // |  LPDDR4  | tCK   |
  // |    DDR4  | tCK   |
  // |    DDR5  | tCK   |
  // |==========|=======|
  if (Outputs->DdrType == MRC_DDR_TYPE_LPDDR5) {
    BaseClock = Outputs->Wckps;
  } else {
    BaseClock = Outputs->tCKps;
  }
  Outputs->UIps   = BaseClock / 2;                         // Data Unit Interval period in pS (half of external bus clock)
  if (Outputs->Gear4) {
    Outputs->Qclkps = BaseClock * 2;
  } else {
    Outputs->Qclkps = BaseClock / (Outputs->Gear2 ? 1 : 2);  // QCLK period in pS, this is internal MC/DDRIO clock which is impacted by Gear1/2
  }
  Outputs->Dclkps = BaseClock * (Outputs->Gear4 ? 4 : (Outputs->Gear2 ? 2 : 1));  // DCLK period in pS, this is internal MC/DDRIO clock which is impacted by Gear1/2
  MRC_DEBUG_MSG (
    Debug,
    DebugLevel,
    "tCKps=%u, wCKps=%u UIps=%u, Dclkps=%u, Qclkps=%u\n",
    Outputs->tCKps, Outputs->Wckps, Outputs->UIps, Outputs->Dclkps, Outputs->Qclkps
    );

  return Status;
}

/**
  Returns the extrapolated margin to a fixed # of errors (logT)
  vrefpass is 10x the first passing margin (with no errors) (10x used for int math)
  Errors at vrefpass/10+1 = log1
  Errors at vrefpass/10+2 = logT

  @param[in]      vrefpass      - 10x the first pass margin (w/no errors) (10x used for int match)
  @param[in]      errLog_1      - Errors at vrefpass/10+1
  @param[in]      errLog_2      - Errors at vrefpass/10+2
  @param[in]      errLog_Target - Error target determines extrapolation vs interpolation
  @param[in, out] *berStats     - Used to track interpolation vs extrapolation or if the slope is non-monotonic.
                                  NOTE: target would be Interpolation only

  @retval Interpolated/Extrapolated vref with the scale increased by 10.
**/
UINT32
interpolateVref (
  IN     UINT32  vrefpass,
  IN     UINT32  errLog_1,
  IN     UINT32  errLog_2,
  IN     UINT32  errLog_Target,
  IN OUT UINT32  *berStats
  )
{
  UINT32 vref;
  UINT32 slope;
  UINT32 ErrLogDiff;

  ErrLogDiff = errLog_2 - errLog_1;
  if (errLog_2 <= errLog_1) {
    berStats[3] += 1;                 // non-monotonic case
    return (vrefpass * 10 + 10);
  } else if (errLog_2 < errLog_Target) {
    berStats[2] += 1;                 // error target above errLog_2 -> extrapolation
  } else if (errLog_1 <= errLog_Target) {
    berStats[1] += 1;                 // error target between errLog_1 and errLog_2 -> interpolation
  } else {
    berStats[0] += 1;                 // error target below errLog_1 -> extrapolation
  }

  //extrapolate above errLog_2, max extrapolation is 1 tick (10)
  if (errLog_2 < errLog_Target) {
    vref =  vrefpass * 10 + 20 + MIN (10, (10 * (errLog_Target - errLog_2)) / (ErrLogDiff));
  } else if ( (errLog_1 <= errLog_Target) && (errLog_Target <= errLog_2) && (ErrLogDiff != 0)) {
    vref =  vrefpass * 10 + 10 + (10 * (errLog_Target - errLog_1)) / (ErrLogDiff);  //interpolate
  } else {
    //extrapolate below errLog_1
    slope = (ErrLogDiff) > errLog_1 ? (ErrLogDiff) : errLog_1;
    if (slope != 0) {
      vref = vrefpass * 10 + (10 * errLog_Target) / slope;
    } else {
      vref = 0;
    }
  }

  return vref;  //returns a (vref * 10) interpolation/extrapolation
};

/**
  This function swaps a subfield, within a 32 bit integer value with the specified value.

  @param[in] CurrentValue - 32 bit input value.
  @param[in] NewValue     - 32 bit New value.
  @param[in] Start        - Subfield start bit.
  @param[in] Length       - Subfield length in bits/

  @retval The updated 32 bit value.
**/
UINT32
MrcBitSwap (
  IN UINT32       CurrentValue,
  IN const UINT32 NewValue,
  IN const UINT8  Start,
  IN const UINT8  Length
  )
{
  UINT32 mask;

  // Do bitwise replacement:
  mask = (MRC_BIT0 << Length) - 1;
  CurrentValue &= ~(mask << Start);
  CurrentValue |= ((NewValue & mask) << Start);

  return CurrentValue;
}

/**
  This function returns the maximum Rx margin for a given Channel, Rank(s), and byte.

  @param[in] MrcData    - Pointer to MRC global data.
  @param[in] Param      - Test parameter.
  @param[in] Controller - Memory Controller
  @param[in] Channel    - Channel to calculate max Rx margin.
  @param[in] RankRx     - Rank index.  0xFF causes all ranks to be considered.
  @param[in] Byte       - Byte to check.
  @param[in] Sign       - Sign of the margins (0 - negative/min, 1 - positive/max).
  @param[in] MaxMargin  - Current max margin value.

  @retval The max Rx margin, either MaxMargin or value from stored margins.
**/
UINT8
MrcCalcMaxRxMargin (
  IN MrcParameters  *const MrcData,
  IN UINT8                 Param,
  IN const UINT8           Controller,
  IN const UINT8           Channel,
  IN const UINT8           RankRx,
  IN const UINT8           Byte,
  IN const UINT8           Sign,
  IN UINT16                MaxMargin
  )
{
  const MRC_FUNCTION *MrcCall;
  MrcOutput     *Outputs;
  MrcSaveData   *SaveOutputs;
  INT64         GetSetVal;
  INT64         GetSetMin;
  INT64         GetSetMax;
  UINT16        ParamList[2];
  UINT8         ParamLen;
  UINT8         Idx;
  UINT8         Start;
  UINT8         Stop;
  UINT8         Rank;
  UINT16        MinRange;
  UINT16        MaxRange;
  DDRPHY_COMP_NEW_CR_DDRCRDATACOMP1_STRUCT  CompDataComp1;

  Outputs      = &MrcData->Outputs;
  SaveOutputs  = &MrcData->Save.Data;
  MrcCall = MrcData->Inputs.Call.Func;
  MrcCall->MrcSetMem ((UINT8 *) ParamList, sizeof (ParamList), 0);

  // Check for saturation on Rx Timing
  if (RankRx == 0xFF) {
    // If desired for all ranks
    Start = 0;              // Start in rank 0
    Stop = 4;               // Up to 4 ranks
  } else {
    Start = GetRankToStoreResults(MrcData, RankRx);
    Stop  = Start + 1;
  }

  ParamLen  = 1;
  MinRange  = 0;
  MaxRange  = 0;
  GetSetMin = 0;
  GetSetMax = 0;
  for (Rank = Start; Rank < Stop; Rank++) {
    if (MrcRankExist (MrcData, Controller, Channel, Rank) || (Param == RdV)) {  // RdV is not a per-rank parameter, and Rank0 might be not present
      if (Param == RdT) {
        MrcGetSetStrobe (MrcData, Controller, Channel, Rank, Byte, RxDqsPDelay, ReadFromCache, &GetSetVal);
        ParamList[0] = (UINT16) GetSetVal;
        MrcGetSetStrobe (MrcData, Controller, Channel, Rank, Byte, RxDqsNDelay, ReadFromCache, &GetSetVal);
        ParamList[1] = (UINT16) GetSetVal;
        // Assumption here is that P and N have the same limits.
        MrcGetSetLimits (MrcData, RxDqsNDelay, &GetSetMin, &GetSetMax, NULL);
        ParamLen = 2;
        if (Outputs->RxMode == MrcRxModeUnmatchedP) {
          CompDataComp1.Data = MrcReadCR (MrcData, DDRPHY_COMP_NEW_CR_DDRCRDATACOMP1_REG);
          ParamList[0] += ((UINT16) CompDataComp1.Bits.Dqs_fwdclkp - 64 * SaveOutputs->CompOffset);
          ParamList[1] += ((UINT16) CompDataComp1.Bits.Dqs_fwdclkn - 64 * SaveOutputs->CompOffset);
        }
      } else if (Param == RdTP) {
        MrcGetSetStrobe (MrcData, Controller, Channel, Rank, Byte, RxDqsPDelay, ReadFromCache, &GetSetVal);
        ParamList[0] = (UINT16) GetSetVal;
        MrcGetSetLimits (MrcData, RxDqsPDelay, &GetSetMin, &GetSetMax, NULL);
        ParamLen = 1;
      } else if (Param == RdTN) {
        MrcGetSetStrobe (MrcData, Controller, Channel, Rank, Byte, RxDqsNDelay, ReadFromCache, &GetSetVal);
        ParamList[0] = (UINT16) GetSetVal;
        MrcGetSetLimits (MrcData, RxDqsNDelay, &GetSetMin, &GetSetMax, NULL);
        ParamLen = 1;
      } else if (Param == RdV) {
        MrcGetSetChStrb (MrcData, Controller, Channel, Byte, RxVref, ReadFromCache, &GetSetVal);
        ParamList[0] = (UINT16) GetSetVal;
        MrcGetSetLimits (MrcData, RxVref, &GetSetMin, &GetSetMax, NULL);
      }
      MinRange = (UINT16) GetSetMin;
      MaxRange = (UINT16) GetSetMax;

      for (Idx = 0; Idx < ParamLen; Idx++) {
        if (Sign == 0) {
          if (MaxMargin > ParamList[Idx] - MinRange) {
            MaxMargin = (ParamList[Idx] - MinRange);
          }
        } else {
          if (MaxMargin > MaxRange - ParamList[Idx]) {
            MaxMargin = (MaxRange - ParamList[Idx]);
          }
        }
      }
      if (Param == RdV) { // RdV is not a per-rank parameter
        break;
      }
    }
  }
  return (UINT8) MaxMargin;
}

/**
  This function returns the maximim (Tx or Cmd) Vref margin for a given Channel.

  @param[in] MrcData    - Pointer to MRC global data.
  @param[in] Controller - Controller to calculate max margin.
  @param[in] Channel    - Channel to calculate max margin.
  @param[in] RankMask   - Rank Mask for DDR4 and Lpddr4.
  @param[in] Byte       - Zero based byte number.
  @param[in] Param      - Parameter of Vref to use
  @param[in] Sign       - Sign of the margins (0 - negative/min, 1 - positive/max).
  @param[in] MaxMargin  - Current max margin value.
  @param[in] Pda        - Use PDA or not.

  @retval The max Vref margin, either MaxMargin or value from stored margins.
**/
UINT8
MrcCalcMaxVrefMargin (
  IN MrcParameters  *const MrcData,
  IN const UINT8           Controller,
  IN const UINT8           Channel,
  IN const UINT8           RankMask,
  IN const UINT8           Byte,
  IN const UINT8           Param,
  IN const UINT8           Sign,
  IN       UINT8           MaxMargin,
  IN       BOOLEAN         Pda
  )
{

  MrcDebug       *Debug;
  MrcOutput      *Outputs;
  MrcChannelOut  *ChannelOut;
  MrcRankOut     *RankOut;
  MrcDimmOut     *DimmOut;
  INT32          CurrentVrefOff;
  INT32          MaxRange;
  INT32          MaxVrefOff;
  INT32          MinVrefOff;
  INT32          MaxVref;
  INT32          MinVref;
  INT32          CurrentVref;
  UINT8          Rank;
  UINT8          DimmIdx;
  UINT8          MrIndex;
  BOOLEAN        Ddr4;
  BOOLEAN        Ddr5;
  BOOLEAN        Lpddr4;
  INT64          GetSetMin;
  INT64          GetSetMax;
  INT64          GetSetVal;
  INT64          VDLC;

  Outputs          = &MrcData->Outputs;
  Debug            = &Outputs->Debug;
  Ddr4             = (Outputs->DdrType == MRC_DDR_TYPE_DDR4);
  Ddr5             = (Outputs->DdrType == MRC_DDR_TYPE_DDR5);
  Lpddr4           = (Outputs->DdrType == MRC_DDR_TYPE_LPDDR4);
  CurrentVrefOff   = 0;
  MinVrefOff       = 0;
  MaxVrefOff       = 0;

  if (!(Ddr4 && (Param == CmdV))) {
    // Get maximal offset by comparing MaxRange with the distance of the current offset from the min/max offset.
    // Algorithm:
    // 1. Get MinVrefOffset, MaxVrefOffset, CurrentVref.
    // 2. Initialize MaxRange = MaxMargin.
    // 3. For all channels, ranks:
    //      a. MaxRange = MIN (MaxRange, (CurrentVrefOff - MinVrefOff)) (for negative sign)
    //      b. MaxRange = MIN (MaxRange, (MaxVrefOff - CurrentVrefOff)) (for negative sign)
    ChannelOut = &MrcData->Outputs.Controller[Controller].Channel[Channel];
    MaxRange = MaxMargin;
    MaxVrefOff = GetVrefOffsetLimits (MrcData, Param);
    MinVrefOff = -1 * MaxVrefOff;
    for (Rank = 0; Rank < MAX_RANK_IN_CHANNEL; Rank++) {
      if (MrcRankExist (MrcData, Controller, Channel, Rank) & RankMask) {
        DimmIdx = (Outputs->Lpddr) ? dDIMM0 : Rank / MAX_RANK_IN_DIMM;
        DimmOut = &ChannelOut->Dimm[DimmIdx];
        RankOut = &DimmOut->Rank[Rank % MAX_RANK_IN_DIMM];
        if (Ddr4) {
          if (Pda) {
            CurrentVrefOff = MrcVrefDqToOffsetDdr4 (RankOut->DdrPdaVrefDq[Byte] & 0x7F);
          } else {
            CurrentVrefOff = MrcVrefDqToOffsetDdr4 ((UINT8)(RankOut->MR[mrMR6] & 0x7F));
          }
          // MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE,  "Ddr4  Channel %u Rank %u RankOut->MR[mrMR6]= 0x%X\n",Channel, Rank, RankOut->MR[mrMR6]);
        } else if (Ddr5) {
          if (Pda) {
            if (Param == CmdV) {
              CurrentVrefOff = MrcVrefToOffsetDdr5 (RankOut->DdrPdaVrefCmd[Byte]);
            } else {
              CurrentVrefOff = MrcVrefToOffsetDdr5 (RankOut->DdrPdaVrefDq[Byte]);
            }
          } else {
            MrIndex = (Param == CmdV) ? mrIndexMR11 : mrIndexMR10;
            CurrentVrefOff = MrcVrefToOffsetDdr5 ((UINT8)(RankOut->MR[MrIndex]));
          }
        } else if (Lpddr4) {
          MrIndex = (Param == CmdV) ? mrIndexMR12 : mrIndexMR14;
          MrcVrefEncToOffsetLpddr4 (
            MrcData,
            RankOut->MR[MrIndex] & 0x3F,
            (RankOut->MR[MrIndex] >> 6) & 1,
            &CurrentVrefOff
            );
          // MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE,  "Lpddr4  Channel %u Rank %u RankOut->MR[mrIndexMR%d]= 0x%X\n",Channel, Rank, (Param == CmdV) ? 12 : 14, RankOut->MR[MrIndex]);
        } else { // Lpddr5
          if (Param == CmdV) {
            MrIndex = mrIndexMR12;
          } else {
            MrIndex = mrIndexMR14;
            if (DimmOut->SdramWidth == 16) {
              VDLC = (RankOut->MR[MrIndex] >> 7) & 1;
              if (VDLC && Byte) {
                // Vref (DQ[15:7]) follow MR15 OP[6:0]
                MrIndex = mrIndexMR15;
              }
            }
          }
          MrcVrefEncToOffsetLpddr5 (MrcData, (RankOut->MR[MrIndex] & 0x7F), &CurrentVrefOff);
        }
        if (Sign == 0) {
          MaxRange = MIN (MaxRange, (CurrentVrefOff - MinVrefOff));
        } else {  // Sign == 1
          MaxRange = MIN (MaxRange, (MaxVrefOff - CurrentVrefOff));
        }
      }
    } // for Rank
  } else {
    // In this case we compare MaxRange with the distance of the current Margin Parameter absolute value
    // (not offset) with the min/max margin parameter allowed value.  Algorithm:
    // 1. Read Param MinVref, MaxVref and CurrentVref.
    // 2. Calculate MaxRange:
    //      a. MaxRange = MIN (CurrentVref - MinVref, MaxMargin) (for negative sign)
    //      b. MaxRange = MIN (MaxVref - CurrentVref, MaxMargin) (for positive sign)
    GetSetVal = 0;
    for (Rank = 0; Rank < MAX_RANK_IN_CHANNEL; Rank++) {
      if (((1 << Rank) & RankMask) != 0) {
        MrcGetSetMcChRnk (MrcData, Controller, Channel, Rank, CmdVref, ReadFromCache, &GetSetVal);
        break;
      }
    }
    MrcGetSetLimits (MrcData, CmdVref, &GetSetMin, &GetSetMax, NULL);
    CurrentVref = (INT32) GetSetVal;
    MinVref = (INT32) GetSetMin;
    MaxVref = (INT32) GetSetMax;

    //  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "C%dparam %x, MaxMargin %d, CurrentVref %d, TotalVref %d\n", Channel, param, MaxMargin, CurrentVrefOff, TotalVref);
    if (Sign == 0) {
    // Saturation point of Vref
      MaxRange = MIN (CurrentVref - MinVref, MaxMargin);
      //  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Saturation possible C%d, New MaxMargin %d\n", Channel, MaxRange);
    } else {
      // Sign == 1
      // Saturation point of Vref
      MaxRange = MIN (MaxVref - CurrentVref, MaxMargin);
      //  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Saturation possible C%d, New MaxMargin %d\n", Channel, MaxRange);
    }
  }

  if (MaxRange < 0) {
    MRC_DEBUG_MSG(Debug, MSG_LEVEL_ERROR, "Error Param %d Got MaxRange %d < 0\n", Param, MaxRange);
    MaxRange = 0;
  }

  return (UINT8) MaxRange;
}

/**
  This function determines if a bit lane[0-7] has seen a pass and a fail in each byte for all Mc/channels populated.

  @param[in] MrcData     - Pointer to MRC global data.
  @param[in] McChBitMask - Bit mask of MC Channels to consider
  @param[in] OnePass     - Array of Bit masks marking DQ lanes has had a passing value.
  @param[in] OneFail     - Array of Bit masks marking DQ lanes has had a failing value.

  @retval The AND result of each Channel/byte for OnePass and OneFail.
**/
UINT8
MrcAndBytes (
  IN MrcParameters *const MrcData,
  IN const UINT8          McChBitMask,
  IN UINT8                OnePass[MAX_CONTROLLER][MAX_CHANNEL][MAX_SDRAM_IN_DIMM],
  IN UINT8                OneFail[MAX_CONTROLLER][MAX_CHANNEL][MAX_SDRAM_IN_DIMM]
  )
{
  UINT8      Res;
  UINT8      Controller;
  UINT8      Channel;
  UINT8      byte;
  MrcOutput *Outputs;

  Outputs = &MrcData->Outputs;
  Res = 0xFF;
  for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
    for (Channel = 0; Channel < Outputs->MaxChannels; Channel++) {
      if (!((1 << ((Controller * Outputs->MaxChannels) + Channel)) & McChBitMask)) {
        continue;
      }

      for (byte = 0; byte < Outputs->SdramCount; byte++) {
        Res &= OnePass[Controller][Channel][byte];
        Res &= OneFail[Controller][Channel][byte];
      }
    }
  }

  return Res;
}

/**
  This function Finds the margin for all channels/all bits. The margin sweep is a parameterized
  Assume REUT test has already been fully setup to run
  This will unscale the results such that future tests start at the correct point
  Uses ChangeMargin function to handle a variety cases (Timing, Voltage, Fan, etc.)

  @param[in]     MrcData      - Include all MRC global data.
  @param[in]     McChBitMask  - MC Channel BIT mask for MC Channel(s) to work on
  @param[in]     Rank         - Rank to work on
  @param[in,out] marginbit    - used as the return data ( real margin measurement, no 10x)
                                marginbit[ch,byte,bit,sign] = abs(Margin)
                                Note: If param == RdTBit/RdVBit/WrVBit, marginbit is also the starting point
  @param[in,out] marginbyte   - provides the starting point on a per byte basis (still 10x)
  @param[in]     param        - defines the margin type
  @param[in]     mode         - allows for different types of modes for margining
                                {Bit0: PhLock (keep all bytes within in ch in phase),
                                 Bit1: Ch2Ch Data out of phase (LFSR seed)
                                 Bits 15:2: Reserved}
  @param[in]     MaxMargin    - Default Maximum margin
  @param[in]     MsgPrint     - Show debug prints

  @retval mrcSuccess if successful, otherwise it returns an error status.
**/
MrcStatus
MrcGetMarginBit (
  IN     MrcParameters *const MrcData,
  IN     UINT8                McChBitMask,
  IN     UINT8                Rank,
  IN OUT UINT16               marginbit[MAX_CONTROLLER][MAX_CHANNEL][MAX_SDRAM_IN_DIMM][MAX_BITS][MAX_EDGES],
  IN OUT UINT16               marginbyte[MAX_CONTROLLER][MAX_CHANNEL][MAX_SDRAM_IN_DIMM][MAX_EDGES],
  IN     UINT8                param,
  IN     UINT16               mode,
  IN     UINT8                MaxMargin,
  IN     BOOLEAN              MsgPrint
  )
{
  MrcDebug        *Debug;
  MrcInput        *Inputs;
  MrcOutput       *Outputs;
  MrcStatus       Status;
  MrcDebugMsgLevel DebugLevel;
  const MRC_FUNCTION *MrcCall;
  UINT8           Controller;
  UINT8           Channel;
  UINT8           Byte;
  UINT8           bit;
  UINT8           MaxChannels;
  UINT8           sign;
  INT8            realSign;
  UINT8           pbyte;
  BOOLEAN         PerCh;
  UINT8           PerBit;
  UINT8           Points2D;
  UINT8           DoneMask;
  UINT8           ByteMax;
  UINT8           SkipWait;
  UINT8           chPass;
  UINT8           chFail;
  UINT8           AllFail;
  // Set to 1 after mc/ch/byte/bit has a passing point
  UINT8           OnePass[MAX_CONTROLLER][MAX_CHANNEL][MAX_SDRAM_IN_DIMM];
  // Set to 1 after mc/ch/byte/bit has a failing point
  UINT8           OneFail[MAX_CONTROLLER][MAX_CHANNEL][MAX_SDRAM_IN_DIMM];
  UINT8           ErrByte;
  UINT8           ErrStatus[MAX_SDRAM_IN_DIMM];
  UINT8           MinMargin;
  UINT32          value0;
  UINT32          value1;
  UINT32          v0;
  UINT16          CMargin[MAX_CONTROLLER][MAX_CHANNEL][MAX_SDRAM_IN_DIMM][MAX_BITS];    // Current Margin Point Testing
  UINT16          ABMargin[MAX_CONTROLLER][MAX_CHANNEL][MAX_SDRAM_IN_DIMM];             // Average Byte Margin
  UINT16          MinTested[MAX_CONTROLLER][MAX_CHANNEL][MAX_SDRAM_IN_DIMM][MAX_BITS];  // Min Margin Point Tested
  UINT8           PrintPerByte;
  UINT32          BitTimePerBit;
  UINT8           BitMask;
  UINT8           RankMask;
  INT64           GetSetVal;
  GSM_GT          GroupType;
  BOOLEAN         Ddr4;
  BOOLEAN         Ddr5;
#if (defined (MRC_MARGIN_BIT_DEBUG) && (MRC_MARGIN_BIT_DEBUG == SUPPORT))
  MrcDebugMsgLevel AlgoDebug;
#endif

  Status      = mrcSuccess;
  SkipWait    = 0;
  Inputs      = &MrcData->Inputs;
  Outputs     = &MrcData->Outputs;
  MaxChannels = Outputs->MaxChannels;
  Debug       = &Outputs->Debug;
  MrcCall     = Inputs->Call.Func;
  ByteMax     = MaxMargin;
  RankMask    = 1 << Rank;
  GroupType   = GsmGtMax;
  value0      = 0;
  DebugLevel  = MsgPrint ? MSG_LEVEL_NOTE : MSG_LEVEL_NEVER;
  Ddr4        = (Outputs->DdrType == MRC_DDR_TYPE_DDR4);
  Ddr5        = (Outputs->DdrType == MRC_DDR_TYPE_DDR5);
#if (defined (MRC_MARGIN_BIT_DEBUG) && (MRC_MARGIN_BIT_DEBUG == SUPPORT))
  AlgoDebug   = MSG_LEVEL_NEVER;
#endif

  MrcCall->MrcSetMem ((UINT8 *) ABMargin, sizeof (ABMargin), 0);
  // How many points to test
  Points2D = 1 + (param / 16);
  // Define PerByte param for PerBit cases
  if (param == RdTBit) {
    pbyte   = RdT;
    PerBit  = 1;
    GroupType = RxDqsBitDelay;
  } else if (param == WrTBit) {
    pbyte   = WrT;
    PerBit  = 1;
    GroupType = TxDqBitDelay;
  } else if (param == RdVBit) {
    pbyte   = RdV;
    PerBit  = 1;
    GroupType = RxVoc;
  } else {
    pbyte   = 0;
    PerBit  = 0;
  }

  // Print results PerBit or PerByte
  PrintPerByte = (param == RdT || param == WrT || param == RdV || param == RdTN || param == RdTP);
  // Created for debug purpose
  // Are we using DIMM Vref?  If so, need to use the same Vref across all bytes
  PerCh = ((param == WrFan2) || (param == WrFan3) || (param == WrV) || (mode & 0x1)) && (PerBit == 0);
  // Get Average Byte back to real margin numbers
  for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
    for (Channel = 0; Channel < MaxChannels; Channel++) {
      if ((1 << ((Controller * MaxChannels) + Channel)) & McChBitMask) {
        for (Byte = 0; Byte < Outputs->SdramCount; Byte++) {
          ABMargin[Controller][Channel][Byte] = (marginbyte[Controller][Channel][Byte][0] + marginbyte[Controller][Channel][Byte][1]) / 20;
        }
      }
    }
  }

  if ((param == RdV) || (pbyte == RdV)) {
    // Make sure RecEnDelay / RxVref values exist in CR cache for this algorithm to work, because we will use WriteOffsetUncached
    MrcReadParamIntoCache (MrcData, param);
  }

  // Find Left and Right Edges
  for (sign = 0; sign < 2; sign++) {
    realSign = (INT8) ((2 * sign) - 1);
    MRC_DEBUG_MSG (Debug, DebugLevel, "\n+--MrcGetMarginBit, rsign = %d\n", realSign);
    MRC_DEBUG_MSG (Debug, DebugLevel, (PrintPerByte) ? "\nMargin\tBits\n" : "");
    // Initialize variables
    DoneMask = 0xFF;
    for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
      for (Channel = 0; Channel < MaxChannels; Channel++) {
        if (!((1 << ((Controller * MaxChannels) + Channel)) & McChBitMask)) {
          continue; // This channel is not populated
        }
        MinMargin = 0x7F; // Start with a huge unsigned number
        for (Byte = 0; Byte < Outputs->SdramCount; Byte++) {
          // Init arrays to 0
          OnePass[Controller][Channel][Byte] = OneFail[Controller][Channel][Byte] = 0;
          // Find MaxMargin for this byte
          ByteMax = MaxMargin;
          if ((param == RdT) || (param == RdV) || (param == RdTN) || (param == RdTP)) {
            ByteMax = MrcCalcMaxRxMargin (MrcData, param, Controller, Channel, RankMask, Byte, sign, MaxMargin);
          } else if (param == WrV) {
            ByteMax = MrcCalcMaxVrefMargin (MrcData, Controller, Channel, RankMask, Byte, param, sign, MaxMargin, FALSE);
          }
          CMargin[Controller][Channel][Byte][0] = ABMargin[Controller][Channel][Byte]; // Start on the passing edge
          MRC_MARGIN_BIT_MSG (Debug, AlgoDebug, "Pre DataTrainOffset Shift Mc%u.Ch%u.B%u\nCMargin: %d ABMargin: %d ByteMax: %d MaxMargin: %d\n",
            Controller, Channel, Byte, CMargin[Controller][Channel][Byte][0], ABMargin[Controller][Channel][Byte], ByteMax, MaxMargin);
          if ((param == RdTBit) || (param == WrTBit)) {
            // Special case for PerBit Timing
            v0 = realSign * (CMargin[Controller][Channel][Byte][0]);
            Status = ChangeMargin (MrcData, pbyte, v0, 0, 0, Controller, Channel, RankMask, Byte, 0, 0, 0);
          } else if (param == RdVBit) {
            // Special case for PerBit Voltage
            // Guardband into the failing region
            if (CMargin[Controller][Channel][Byte][0] >= 2) {
              CMargin[Controller][Channel][Byte][0] -= 2;
            } else {
              CMargin[Controller][Channel][Byte][0] = 0;
            }
            v0 = realSign * (CMargin[Controller][Channel][Byte][0]);
            Status = ChangeMargin (MrcData, pbyte, v0, 0, 0, Controller, Channel, RankMask, Byte, 0, 0, 0);
          }
          // Update the variables for PerBit tracking
          if (PerBit) {
            for (bit = 0; bit < MAX_BITS; bit++) {
              CMargin[Controller][Channel][Byte][bit] = marginbit[Controller][Channel][Byte][bit][sign];
              // Double check saturation limits as we cannot move PBD more than we can move the Byte PI.
              if (CMargin[Controller][Channel][Byte][bit] > MaxMargin) {
                CMargin[Controller][Channel][Byte][bit] = MaxMargin;
              }
            }
          } else {
            if (CMargin[Controller][Channel][Byte][0] > ByteMax) {
              CMargin[Controller][Channel][Byte][0] = ByteMax;
            }
          }
          // Find MinMargin to start and set marginbyte for the PerCh case
          if (PerCh) {
            if (CMargin[Controller][Channel][Byte][0] < MinMargin) {
              MinMargin = (UINT8) CMargin[Controller][Channel][Byte][0];
            }
            CMargin[Controller][Channel][Byte][0] = MinMargin;
          }
          MRC_MARGIN_BIT_MSG (Debug, AlgoDebug, "Setting Mins\n");
          for (bit = 0; bit < MAX_BITS; bit++) {
            MRC_MARGIN_BIT_MSG (Debug, AlgoDebug, "CMargin.Mc%u.Ch%u.B%u.L%u: %d\n", Controller, Channel, Byte, bit, CMargin[Controller][Channel][Byte][bit]);
            MinTested[Controller][Channel][Byte][bit] = CMargin[Controller][Channel][Byte][bit * PerBit];
            marginbit[Controller][Channel][Byte][bit][sign] = CMargin[Controller][Channel][Byte][bit * PerBit];
          }
        } // for Byte
      } // for Channel
    } // for Controller
    // Initialize error status to 0.
    MrcCall->MrcSetMem ((UINT8 *) ErrStatus, sizeof (ErrStatus), 0);
    // Search algorithm:
    // Walk up until everybody fails.  Then Walk down until everybody passes.
    while (MrcAndBytes (MrcData, McChBitMask, OnePass, OneFail) != DoneMask) {
      for (value1 = 0; value1 < Points2D; value1++) {
        // Set Margin level
        for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
          for (Channel = 0; Channel < MaxChannels; Channel++) {
            if (!((1 << ((Controller * MaxChannels) + Channel)) & McChBitMask)) {
              continue;
            }
            SkipWait = (McChBitMask >> ((Controller * MaxChannels) + Channel + 1)); // Skip if there are more channels
            for (Byte = 0; Byte < Outputs->SdramCount; Byte++) {
              if (PerBit) {
                for (bit = 0; bit < MAX_BITS; bit++) {
                  MRC_MARGIN_BIT_MSG (Debug, AlgoDebug, "Mc%u.Ch%u.B%u.L%u - MaxMargin = %d, CMargin = %d, marginbit = %d\n",
                    Controller, Channel, Byte, bit, MaxMargin, CMargin[Controller][Channel][Byte][bit], marginbit[Controller][Channel][Byte][bit][sign]);
                    v0 = CMargin[Controller][Channel][Byte][bit];

                  if (v0 > MaxMargin) {
                    v0 = MaxMargin;
                  }
                  GetSetVal = (UINT8) v0;
                  MrcGetSetBit (MrcData, Controller, Channel, Rank, Byte, bit, GroupType, WriteToCache, &GetSetVal);
                } // bit loop
              } else {
                value0 = realSign * CMargin[Controller][Channel][Byte][0];
                // EnMultiCast=0, ch,rank,byte,0, UpdateHost=0, SkipWait
                Status = ChangeMargin (
                  MrcData,
                  param,
                  value0,
                  value1,
                  0,
                  Controller,
                  Channel,
                  RankMask,
                  Byte,
                  0,
                  0,
                  SkipWait
                );
                if ((PerCh) && ((mode & 1) == 0)) {
                  break;
                }
              } // !perBit
            } // Byte
          } // Channel
        } // Controller
        MrcFlushRegisterCachedData (MrcData);

        // Run Test
        RunIOTest (MrcData, McChBitMask, Outputs->DQPat, (value1 == 0), 0);
        // Check if we have already failed and can stop running
        if (value1 < (UINT32) (Points2D - 1)) {
          AllFail = 1;
          for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
            for (Channel = 0; Channel < MaxChannels; Channel++) {
              if (!((1 << ((Controller * MaxChannels) + Channel)) & McChBitMask)) {
                continue;
              }
              MrcCall->MrcSetMem ((UINT8 *) ErrStatus, sizeof (ErrStatus), 0);
              MrcGetBitGroupErrStatus (MrcData, Controller, Channel, ErrStatus);
              for (Byte = 0; Byte < Outputs->SdramCount; Byte++) {
                AllFail &= (ErrStatus[Byte] == 0xFF);
              }
            } // for Channel
          } // for Controller
          if (AllFail) {
            break;  // break if any error
          }
        }
      } // for Points2D
      // Initialize error status to 0.
      MrcCall->MrcSetMem ((UINT8 *) ErrStatus, sizeof (ErrStatus), 0);
      // Collect results and Update variables for next point to test
      for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
        for (Channel = 0; Channel < MaxChannels; Channel++) {
          if (!((1 << ((Controller * MaxChannels) + Channel)) & McChBitMask)) {
            continue;
          }
          MrcGetBitGroupErrStatus (MrcData, Controller, Channel, ErrStatus);
          chPass = 0xFF;
          chFail = 0xFF;
          for (Byte = 0; Byte < Outputs->SdramCount; Byte++) {
            // Extract Errors
            ErrByte = ErrStatus[Byte];
            ErrByte &= DoneMask;
#ifdef MRC_DEBUG_PRINT
            if (PerBit == 1) {
              MRC_DEBUG_MSG (Debug, DebugLevel, "0x");
              for (bit = 0; bit < MAX_BITS; bit++) {
                MrcGetSetBit (MrcData, Controller, Channel, Rank, Byte, bit, GroupType, ReadFromCache, &GetSetVal);
                MRC_DEBUG_MSG (Debug, DebugLevel, "%02X", (UINT8) GetSetVal);
              }
            } else if (param == WrT || param == RdT || param == RdV || param == WrV || param == RdTN || param == RdTP) {
              MRC_DEBUG_MSG (Debug, DebugLevel, "% 2d", CMargin[Controller][Channel][Byte][0]);
            }
            MRC_DEBUG_MSG (Debug, DebugLevel, " ");
#endif // MRC_DEBUG_PRINT
            for (bit = 0; bit < MAX_BITS; bit++) {
              BitMask = MRC_BIT0 << bit;
              BitTimePerBit = bit * PerBit;
              // Skip if this Bit Group is done
              if (OnePass[Controller][Channel][Byte] & OneFail[Controller][Channel][Byte] & (BitMask)) {
                MRC_DEBUG_MSG (Debug, DebugLevel, "$");
                continue;
              }
              // Update variables for fail
              if (ErrByte & (BitMask)) {
                OneFail[Controller][Channel][Byte] |= (BitMask);
                MRC_DEBUG_MSG (Debug, DebugLevel, "#");
              } else {
                OnePass[Controller][Channel][Byte] |= (BitMask);
                MRC_DEBUG_MSG (Debug, DebugLevel, ".");
                  marginbit[Controller][Channel][Byte][bit][sign] = CMargin[Controller][Channel][Byte][BitTimePerBit];
                }
              }
            for (bit = 0; bit < MAX_BITS; bit++) {
              MRC_MARGIN_BIT_MSG (Debug, AlgoDebug, "Mc%u.Ch%u.B%u.L%u - marginbit = %d\n",
                    Controller, Channel, Byte, bit, marginbit[Controller][Channel][Byte][bit][sign]);
            }
            // FIND MAX Saturation limit
            ByteMax = MaxMargin;
            if ((param == RdT) || (param == RdV)) {
              ByteMax = MrcCalcMaxRxMargin (MrcData, param, Controller, Channel, RankMask, Byte, sign, MaxMargin);
            } else if (param == WrV) {
              ByteMax = MrcCalcMaxVrefMargin (MrcData, Controller, Channel, RankMask, Byte, param, sign, MaxMargin, FALSE);
            }
            // HANDLE Saturation
            if (PerBit) {
              for (bit = 0; bit < MAX_BITS; bit++) {
                BitMask = MRC_BIT0 << bit;
                if ((sign == 1 && CMargin[Controller][Channel][Byte][bit] >= ByteMax) || (sign == 0 && CMargin[Controller][Channel][Byte][bit] == 0)) {
                  OneFail[Controller][Channel][Byte] |= (BitMask);
                }
                if ((sign == 1 && CMargin[Controller][Channel][Byte][bit] == 0) || (sign == 0 && CMargin[Controller][Channel][Byte][bit] >= ByteMax)) {
                  OnePass[Controller][Channel][Byte] |= (BitMask);
                }
              }
            } else {
              if (CMargin[Controller][Channel][Byte][0] >= ByteMax) {
                OneFail[Controller][Channel][Byte] = DoneMask;
              }
              if (CMargin[Controller][Channel][Byte][0] == 0) {
                OnePass[Controller][Channel][Byte] = DoneMask;
              }
            }
            // DECIDE WHAT TO TEST NEXT
            // If PerByte, Do this within the for byte loop
            chPass &= OnePass[Controller][Channel][Byte];
            chFail &= OneFail[Controller][Channel][Byte];
            MRC_MARGIN_BIT_MSG (Debug, AlgoDebug, "Update CMargin based on edge and sign\n");
            if (PerCh == FALSE) {
              if (PerBit) {
                for (bit = 0; bit < MAX_BITS; bit++) {
                  BitMask = MRC_BIT0 << bit;
                  // Skip if this Bit Group is done
                  MRC_MARGIN_BIT_MSG (Debug, AlgoDebug, "Mc%u.Ch%u.B%u.L%u.sign%u - ", Controller, Channel, Byte, bit, sign);
                  if (OnePass[Controller][Channel][Byte] & OneFail[Controller][Channel][Byte] & (BitMask)) {
                    MRC_MARGIN_BIT_MSG (Debug, AlgoDebug, "Done\n");
                    continue;
                  }
                  if ((OneFail[Controller][Channel][Byte] & BitMask) == 0) {
                    if (sign) {
                      CMargin[Controller][Channel][Byte][bit]++;
                    } else {
                      CMargin[Controller][Channel][Byte][bit]--;
                    }
                    MRC_MARGIN_BIT_MSG (Debug, AlgoDebug, "Passed ");
                  } else if ((OnePass[Controller][Channel][Byte] & BitMask) == 0) {
                    MRC_MARGIN_BIT_MSG (Debug, AlgoDebug, "Failed ");
                    if (sign) {
                      CMargin[Controller][Channel][Byte][bit]--;
                    } else {
                      CMargin[Controller][Channel][Byte][bit]++;
                    }
                  } else {
                    MRC_DEBUG_MSG (Debug, MSG_LEVEL_WARNING, "WARNING! Can't have both: OnePass and OneFail Not Done\n");
                  }
                  MRC_MARGIN_BIT_MSG (Debug, AlgoDebug, "CMargin = %d, MinTested = %d\n", CMargin[Controller][Channel][Byte][bit], MinTested[Controller][Channel][Byte][bit]);
                }
              } else {
                // PerCh
                // Skip if this Byte Group is done
                if ((OnePass[Controller][Channel][Byte] & OneFail[Controller][Channel][Byte]) == DoneMask) {
                  MRC_DEBUG_MSG (Debug, DebugLevel, " ");
                  continue;
                }
                if (OneFail[Controller][Channel][Byte] != DoneMask) {
                  CMargin[Controller][Channel][Byte][0] += 1;
                } else if (OnePass[Controller][Channel][Byte] != DoneMask) {
                  MinTested[Controller][Channel][Byte][0] -= 1;
                  CMargin[Controller][Channel][Byte][0] = MinTested[Controller][Channel][Byte][0];
                }
              }
            }
            MRC_DEBUG_MSG (Debug, DebugLevel, " ");
          }
          // END OF BYTE LOOP
          // DECIDE WHAT TO TEST NEXT
          // If PerCh, Do this within the for ch loop
          if (PerCh == TRUE) {
            if ((chPass & chFail) == DoneMask) {
              continue;
            }
            if (chFail != DoneMask) {
              CMargin[Controller][Channel][0][0] += 1;
            } else {
              MinTested[Controller][Channel][0][0] -= 1;
              CMargin[Controller][Channel][0][0] = MinTested[Controller][Channel][0][0];
            }
            // All bytes must use the same margin point
            for (Byte = 0; Byte < Outputs->SdramCount; Byte++) {
              CMargin[Controller][Channel][Byte][0] = CMargin[Controller][Channel][0][0];
            }
          }
        } // for Channel
      } // for Controller
      MRC_DEBUG_MSG (Debug, DebugLevel, "\n");
    } // while
      // Update MarginByte to have the correct result
    if (PerBit == 0) {
      for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
        for (Channel = 0; Channel < MaxChannels; Channel++) {
          if ((1 << ((Controller * MaxChannels) + Channel)) & McChBitMask) {
            for (Byte = 0; Byte < Outputs->SdramCount; Byte++) {
              MinMargin = 0x7F; // Start with a huge unsigned number
              for (bit = 0; bit < MAX_BITS; bit++) {
                if (MinMargin > marginbit[Controller][Channel][Byte][bit][sign]) {
                  MinMargin = (UINT8) marginbit[Controller][Channel][Byte][bit][sign];
                }
              }
              marginbyte[Controller][Channel][Byte][sign] = MinMargin * 10;
              // MRC_DEBUG_MSG (Debug, DebugLevel,"+--marginbyte = MinMargin*10 = %d\n", MinMargin*10);
            } // for Byte
          }
        } // for Channel
      } // for Controller
    } else {
      // Cleanup after test
      Status = ChangeMargin (MrcData, pbyte, 0, 0, 1, 0, 0, Rank, 0, 0, 0, 0);
    }
  }
  // END OF SIGN LOOP
  // Clean up after step
  if (!PerBit) {
    Status = ChangeMargin (MrcData, param, 0, 0, 1, 0, 0, (param == WrV) ? (1 << Rank) : Rank, 0, 0, 0, 0);

    // Restore WrV PDA
    if ((Ddr4 || Ddr5) && (param == WrV) && Inputs->EnablePda) {
      for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
        for (Channel = 0; Channel < MaxChannels; Channel++) {
          if (!((1 << ((Controller * MaxChannels) + Channel)) & McChBitMask)) {
            continue;
          }
          for (Byte = 0; Byte < Outputs->SdramCount; Byte++) {
            ChangeMargin (MrcData, param, 0, 0, 0, Controller, Channel, 1 << Rank, 1 << Byte, 0, 0, 0);
          }
        }
      }
    }
  }
#ifdef MRC_DEBUG_PRINT
  if (PerBit == 1) {
    MRC_DEBUG_MSG (Debug, DebugLevel, "\nEdges");
    for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
      for (Channel = 0; Channel < MaxChannels; Channel++) {
        if ((1 << ((Controller * MaxChannels) + Channel)) & McChBitMask) {
          for (Byte = 0; Byte < Outputs->SdramCount; Byte++) {
            MRC_DEBUG_MSG(Debug, DebugLevel, "\nMc%u.Ch%u.B%d:\t", Controller, Channel, Byte);
            for (bit = 0; bit < MAX_BITS; bit++) {
              MRC_DEBUG_MSG (
                Debug,
                DebugLevel,
                "[%3d %3d]",
                marginbit[Controller][Channel][Byte][bit][0],
                marginbit[Controller][Channel][Byte][bit][1]
              );
            } // for bit
          } // for Byte
        }
      } // for Channel
    } // for Controller
  } else {
    MRC_DEBUG_MSG (Debug, DebugLevel, "\nCt");
    for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
      for (Channel = 0; Channel < MaxChannels; Channel++) {
        if ((1 << ((Controller * MaxChannels) + Channel)) & McChBitMask) {
          MRC_DEBUG_MSG (Debug, DebugLevel, "\nMc%u.Ch%u", Controller, Channel);
          for (Byte = 0; Byte < Outputs->SdramCount; Byte++) {
            MRC_DEBUG_MSG (Debug, DebugLevel, "\nByte %d\t", Byte);
            for (bit = 0; bit < MAX_BITS; bit++) {
              MRC_DEBUG_MSG (
                Debug,
                DebugLevel,
                "%8d ",
                (INT8) (marginbit[Controller][Channel][Byte][bit][1] - marginbit[Controller][Channel][Byte][bit][0]) / 2
              );
            }
            MRC_DEBUG_MSG (Debug, DebugLevel, "   ");
          } // for Byte
        }
      } // for Channel
    } // for Controller
  }
  MRC_DEBUG_MSG (Debug, DebugLevel, "\n");
#endif // MRC_DEBUG_PRINT
  return Status;
}

/**
  Assume REUT test has already been fully setup to run
  Finds the margin for all channels/all bytes
  The margin sweep is parameterized
  Uses ChangeMargin function to handle a variety of cases (Timing, Voltage, Fan, etc.)
  mode allows for different types of modes for margining:
    mode is {Bit0: PhLock (keep all bytes within in ch in phase),
             Bit1: Ch2Ch Data out of phase (LFSR seed), Bit 15:2: Reserved}
  marginByte is used as the starting point for the search (10x the actual margin)
  marginch returns the results (10x the actual margin)
  Interior: Search inside marginch limits, enabling multiple calls with different setups
  To reduce repeatibility noise, the returned margins is actually a BER extrapolation

  @param[in]     MrcData      - The global MrcData
  @param[in,out] marginByte   - Data structure with the latest margin results
  @param[in]     McChBitmask  - Bit mask of present MC channels
  @param[in]     RankIn       - Rank to change margins for
  @param[in]     RankRx       - Ranks for Rx margin
  @param[in]     param        - parameter to get margins for
  @param[in]     mode         - allows for different types of modes for margining:
  @param[in]     BMap         - Byte mapping to configure error counter control register
  @param[in]     EnBER        - Enable BER extrapolation calculations
  @param[in]     MaxMargin    - Max Margin allowed for the parameter
  @param[in]     Interior     - Search inside marginch limits, enabling multiple calls with different setups
  @param[in,out] BERStats     - Bit Error Rate Statistics.

  @retval mrcSuccess if successful, otherwise returns an error status.
**/
MrcStatus
MrcGetBERMarginByte (
  IN     MrcParameters * const MrcData,
  IN OUT UINT16          marginByte[MAX_RESULT_TYPE][MAX_RANK_IN_CHANNEL][MAX_CONTROLLER][MAX_CHANNEL][MAX_SDRAM_IN_DIMM][MAX_EDGES],
  IN     UINT8           McChBitMask,
  IN     UINT8           RankIn,
  IN     UINT8           RankRx,
  IN     UINT8           param,
  IN     UINT16          mode,
  IN     UINT8           *BMap,
  IN     UINT8           EnBER,
  IN     UINT8           MaxMargin,
  IN     UINT8           Interior,
  IN OUT UINT32          *BERStats
)
{
  const MrcInput  *Inputs;
  MrcDebug        *Debug;
  const MRC_FUNCTION *MrcCall;
  MrcOutput        *Outputs;
  UINT16           *MarginByteTemp;
  MrcStatus        Status;
  UINT8            ResultType;
  UINT8            sign;
  INT8             rSign;
  UINT8            Points2D;
  UINT8            Controller;
  UINT8            Channel;
  UINT8            byte;
  UINT8            chByte;
  UINT8            MaxChannels;
  UINT8            SkipWait;
  UINT8            byteMax;
  UINT8            Margin;
  BOOLEAN          MinEyeFlag;
  UINT8            Byte;
  UINT8            Edge;
  UINT16           DoneMask;
  // Set to 1 after mc/ch has 2 passing points
  UINT16           TwoPass[MAX_CONTROLLER][MAX_CHANNEL];
  // Set to 1 after mc/ch has 2 failing points
  UINT16           TwoFail[MAX_CONTROLLER][MAX_CHANNEL];
  UINT16           res;
  UINT64           ErrStatus;
  UINT16           BitMask;
  INT8             Delta;
  BOOLEAN          Done;
  BOOLEAN          allFail;
  BOOLEAN          PerCh;
  UINT32           value0;
  UINT32           value1;
  UINT32           tmp;
  UINT32           ErrCount;
  UINT8            LastPassVref[MAX_CONTROLLER][MAX_CHANNEL][MAX_SDRAM_IN_DIMM]; // Last passing Vref
  UINT32           InitValue[MAX_CONTROLLER][MAX_CHANNEL][MAX_SDRAM_IN_DIMM];    // Initial value from margin byte
  UINT8            MaxTested[MAX_CONTROLLER][MAX_CHANNEL][MAX_SDRAM_IN_DIMM];    // Highest Vref Point Tested
  UINT8            MinTested[MAX_CONTROLLER][MAX_CHANNEL][MAX_SDRAM_IN_DIMM];    // Lowest Vref Point Tested
                                                                 // Log8(Error count) at different Vref Points. 32 bit number that is broken in 4 bytes
                                                                 // [LastPass+2, LastPass+1, LastPass, LastPass-1]
  UINT32           Errors[MAX_CONTROLLER][MAX_CHANNEL][MAX_SDRAM_IN_DIMM];
  BOOLEAN          Ddr4;
  BOOLEAN          Ddr5;
  UINT8            Rank;
  BOOLEAN          DimmVrefParam;
  BOOLEAN          Overflow;
  MC0_REQ0_CR_CPGC_SEQ_CTL_STRUCT  CpgcSeqCtl;

  Status        = mrcSuccess;
  Inputs        = &MrcData->Inputs;
  MrcCall       = Inputs->Call.Func;
  Outputs       = &MrcData->Outputs;
  Debug         = &Outputs->Debug;
  MaxChannels   = Outputs->MaxChannels;
  chByte        = 0;
  Points2D      = (param / RdFan2) + 1;
  ResultType    = GetMarginResultType (param);
  Ddr4          = (Outputs->DdrType == MRC_DDR_TYPE_DDR4);
  Ddr5          = (Outputs->DdrType == MRC_DDR_TYPE_DDR5);
  // Are we using DIMM Vref? (and not DDR4)
  DimmVrefParam = (param == WrFan2 || param == WrFan3 || param == WrV);   // WrFan not defined
  PerCh = (param == WrFan2 || param == WrFan3 || (param == WrV) || ((mode & 1) == 1));   // WrFan not defined
  Rank = GetRankToStoreResults (MrcData, RankIn);
  //  if (param == WrV)
  //  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE,"+--->MrcGetBERMarginByte, Points2D: %d, PerCh: %d --\n", Points2D,PerCh);
  DoneMask = (MRC_BIT0 << Outputs->SdramCount) - 1; // 0x1FF or 0xFF

  if ((param == RcvEnaX) || (param == RdV)) {
    // Make sure RecEnDelay / RxVref values exist in CR cache for this algorithm to work, because we will use WriteOffsetUncached
    MrcReadParamIntoCache (MrcData, param);
  }
                                                    // Run through margin test
  for (sign = 0; sign < 2; sign++) {
    rSign = (INT8) ((2 * sign) - 1);
    // if (param == WrV) MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE,"+--MrcGetBERMarginByte, rsign = %d\n", rSign);
    MrcCall->MrcSetMem ((UINT8 *) TwoPass, sizeof (TwoPass), 0);
    MrcCall->MrcSetMem ((UINT8 *) TwoFail, sizeof (TwoFail), 0);
    // Initialize variables
    for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
      for (Channel = 0; Channel < MaxChannels; Channel++) {
        if (!((1 << ((Controller * MaxChannels) + Channel)) & McChBitMask)) {
          TwoPass[Controller][Channel] = DoneMask;
          TwoFail[Controller][Channel] = DoneMask;
          continue;
        }
        MinTested[Controller][Channel][0] = 0x7F;
        for (byte = 0; byte < Outputs->SdramCount; byte++) {
          LastPassVref[Controller][Channel][byte] = 0x7F; // Start with a huge unsigned number 127
          Errors[Controller][Channel][byte] = 0;
          // Find MaxMargin for this byte
          byteMax = MaxMargin;
          if ((param == RdT) || (param == RdV)) {
            byteMax = MrcCalcMaxRxMargin (MrcData, param, Controller, Channel, RankRx, byte, sign, MaxMargin);
          } else if ((param == CmdV) || DimmVrefParam) {
            byteMax = MrcCalcMaxVrefMargin (MrcData, Controller, Channel, RankRx, byte, param, sign, MaxMargin, FALSE);
          }
          // if (param == RdT) MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE,  "MC%u C%u R%u B%u sign=%d byteMax=%d\n", Controller, Channel, Rank, byte, sign, byteMax);
          // Scale MarginResult back to real margin numbers.  Set Max/MinTested
          MarginByteTemp = &marginByte[ResultType][Rank][Controller][Channel][byte][sign];
          *MarginByteTemp = *MarginByteTemp / 10;
          if (*MarginByteTemp > byteMax) {
            *MarginByteTemp = byteMax;
          }
          InitValue[Controller][Channel][byte] = *MarginByteTemp;
          // if Per Ch, find MinMargin to start.  Else set margin for that Byte
          if (PerCh == TRUE) {
            if (*MarginByteTemp < MinTested[Controller][Channel][0]) {
              MaxTested[Controller][Channel][0] = (UINT8) *MarginByteTemp;
              MinTested[Controller][Channel][0] = (UINT8) *MarginByteTemp;
            }
          } else {
            MaxTested[Controller][Channel][byte] = (UINT8) *MarginByteTemp;
            MinTested[Controller][Channel][byte] = (UINT8) *MarginByteTemp;
          }
        }
        for (byte = 0; byte < Outputs->SdramCount; byte++) {
          // Setup CPGC Error Counters to count errors per Byte Group, 1 count per UI with an error across all lanes in the Byte Group, all UI
          MrcSetupErrCounterCtl (MrcData, byte, ErrCounterCtlPerByte, 0);
        }

        // Set MarginResult for the PerCh case
        if (PerCh == TRUE) {
          for (byte = 0; byte < Outputs->SdramCount; byte++) {
            marginByte[ResultType][Rank][Controller][Channel][byte][sign] = MinTested[Controller][Channel][0];
          }
        }
      } // for Channel
    } // for Controller
    // Search algorithm:
    // If start with a passing point, walk to hit 2 failing points
    //    Return as needed to hit a second passing point
    // If start with a failing point, walk to hit 2 passing points
    //    Return as needed to hit a second failing point
    // Keep testing until all mc/ch/bytes find 2 passes and 2 fails
    Done = FALSE;
    do {
      // Walk through all 2D points
      CpgcSeqCtl.Data = 0;
      CpgcSeqCtl.Bits.CLEAR_ERRORS = 1;
      Cpgc20ControlRegWrite (MrcData, McChBitMask, CpgcSeqCtl);
      for (value1 = 0; value1 < Points2D; value1++) {
        // Set Vref level
        for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
          for (Channel = 0; Channel < MaxChannels; Channel++) {
            if (!((1 << ((Controller * MaxChannels) + Channel)) & McChBitMask)) {
              continue;
            }
            SkipWait = (McChBitMask >> ((Controller * MaxChannels) + Channel + 1)); // Skip if there are more channels
            for (byte = 0; byte < Outputs->SdramCount; byte++) {
              value0 = rSign * marginByte[ResultType][Rank][Controller][Channel][byte][sign];
              // if (param == RdT) MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE,"MC%u C%u RankIn %u B%u +----->Value0 is %d, Value1 is %d\n", Controller, Channel, RankIn, byte, (INT32)value0, value1);
              Status = ChangeMargin (
                MrcData,
                param,
                value0,
                value1,
                0,
                Controller,
                Channel,
                RankIn,
                byte,
                0,
                0,
                SkipWait
              );
              if ((PerCh) && ((mode & 1) == 0)) {
                // Only Byte 7 on Channel 1 is needed to update Wr DIMM Vref - Taken care of inside ChangeMargin routine
                // for Ddr4 WrV is define per ch so we break after byte 0
                break;
              }
            }
          }
        }
        // Run Test
        RunIOTest (MrcData, McChBitMask, Outputs->DQPat, (value1 == 0), 0);
        if (param == CmdV) {
          MrcResetSequence (MrcData);
        }
        // What is the idea behind this? What if all byte groups failed?
        if (EnBER == 0 && value1 < (UINT32) (Points2D - 1)) {
          allFail = TRUE;
          for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
            for (Channel = 0; Channel < MaxChannels; Channel++) {
              if (!((1 << ((Controller * MaxChannels) + Channel)) & McChBitMask)) {
                continue;
              }
              // Read out per byte error results
              res = 0;
              MrcGetMiscErrStatus (MrcData, Controller, Channel, ByteGroupErrStatus, &ErrStatus);
              res = (UINT16) ErrStatus;
              if ((res & DoneMask) != DoneMask) {
                allFail = FALSE;
              }
            } // for Channel
          } // for Controller
          if (allFail == TRUE) {
            break;
          }
        }
      }
      // Collect results and Update variables for next point to test
      for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
        for (Channel = 0; Channel < MaxChannels; Channel++) {
          if (!((1 << ((Controller * MaxChannels) + Channel)) & McChBitMask)) {
            continue;
          }
          for (byte = 0; byte < Outputs->SdramCount; byte++) {
            BitMask = MRC_BIT0 << byte;
            // Skip if this Byte Group is done
            if ((TwoPass[Controller][Channel] & TwoFail[Controller][Channel] & (BitMask)) != 0) {
              continue;
            }
            // Handle PerCh vs. PerByte variable differences
            chByte = (PerCh == TRUE ? 0 : byte);
            // Read CPGC Error Count
            MrcGetErrCounterStatus (MrcData, Controller, Channel, byte, ErrCounterCtlPerByte, &ErrCount, &Overflow);
            Margin = (UINT8) marginByte[ResultType][Rank][Controller][Channel][byte][sign];
            Delta = (Margin - LastPassVref[Controller][Channel][byte]);
             //if (param == RdT) {
             // UINT64 EccStatus;
             // UINT64 ChunkStatus;
             // MrcGetMiscErrStatus (MrcData, Controller, Channel, ByteGroupErrStatus, &ErrStatus);
             // MrcGetMiscErrStatus (MrcData, Controller, Channel, EccLaneErrStatus, &EccStatus);
             // MrcGetMiscErrStatus (MrcData, Controller, Channel, ChunkErrStatus, &ChunkStatus);
             // MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE,"+----->MC%u C%u B%u ErrCount: %6u, ErrStatus: 0x%x, EccStatus: %d, ChunkStatus: 0x%08X, TwoPass: 0x%04x, TwoFail: 0x%04x\n",
             //   Controller, Channel, byte, ErrCount, (UINT32) ErrStatus, (UINT32) EccStatus, (UINT32) ChunkStatus, TwoPass[Controller][Channel], TwoFail[Controller][Channel]);
             // MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE,"+----->Margin:%d, LastPassVref:%d, delta:%d. sign:%d\n", Margin, LastPassVref[Controller][Channel][byte], (INT8) Delta, sign);
             //}
            // Update Pass/Fail Variables:
            if (ErrCount == 0 && Margin == MaxTested[Controller][Channel][chByte]) {
              // Passing while walking up
              if (Delta < 0) {
                // First passing point
                MRC_DEBUG_ASSERT (
                  MinTested[Controller][Channel][chByte] == MaxTested[Controller][Channel][chByte],
                  Debug,
                  "Error: MaxTested < LastPass after first point"
                );
                LastPassVref[Controller][Channel][byte] = Margin;
              } else if (Delta == 1) {
                // Normal, walk to fail
                Errors[Controller][Channel][byte] = MrcBitShift (Errors[Controller][Channel][byte], -8 * Delta) & BER_ERROR_MASK;
                LastPassVref[Controller][Channel][byte] = Margin;
                TwoPass[Controller][Channel] |= (BitMask);
              } else if (Delta == 2) {
                // Speckling in response, Consider point as error
                Errors[Controller][Channel][byte] = MrcBitSwap (Errors[Controller][Channel][byte], MrcLog8 (ErrCount), 24, 8);
                TwoFail[Controller][Channel] |= (BitMask);
              } else {
                //MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "--> MC%u C%u B%u param: %u, Delta: %d\n", Controller, Channel, byte, param, Delta);
                MRC_DEBUG_ASSERT (FALSE, Debug, "Error: Tested point twice or Tested >2 above LastPass (Passing while walking up)");
              }
            } else if (ErrCount == 0 && Margin == MinTested[Controller][Channel][chByte]) {
              // Skip if this byte is already done
              if ((TwoPass[Controller][Channel] & (BitMask)) != 0) {
                continue;
              }
              if (Delta == -1) {
                // Finding 2nd pass
                Errors[Controller][Channel][byte] = MrcBitSwap (Errors[Controller][Channel][byte], 0, 0, 8);
                TwoPass[Controller][Channel] |= (BitMask);
              } else {
                // 1st passing point
                // Don't shift Errors.  Fail points already assumed correct LastPass
                LastPassVref[Controller][Channel][byte] = Margin;
                TwoPass[Controller][Channel] &= ~(BitMask);
              }
            } else if (ErrCount > 0 && Margin == MaxTested[Controller][Channel][chByte]) {
              // Failing while walking up
              MRC_DEBUG_ASSERT (Delta <= 2, Debug, "Error: Tested >2 above LastPass (Failing while walking up)");
              if (Delta < 2) {
                // first failing point
                Errors[Controller][Channel][byte] = MrcBitSwap (Errors[Controller][Channel][byte], MrcLog8 (ErrCount), 16, 8);
                TwoFail[Controller][Channel] &= ~(BitMask);
              } else if (Delta == 2) {
                // 2nd failing point
                Errors[Controller][Channel][byte] = MrcBitSwap (Errors[Controller][Channel][byte], MrcLog8 (ErrCount), 24, 8);
                TwoFail[Controller][Channel] |= (BitMask);
              }
            } else if (ErrCount > 0 && Margin == MinTested[Controller][Channel][chByte]) {
              // Failing while walking down
              if (LastPassVref[Controller][Channel][byte] < 0xFF && Delta <= 0) {
                // Adjust LastPassVref and Error count to be below this failure point.
                Errors[Controller][Channel][byte] = MrcBitSwap (Errors[Controller][Channel][byte], MrcLog8 (ErrCount), 8 * (Delta + 1), 8);
                Errors[Controller][Channel][byte] = MrcBitShift (Errors[Controller][Channel][byte], 8 * (1 - Delta));
                LastPassVref[Controller][Channel][byte] = Margin - 1;
              } else {
                tmp = ((Errors[Controller][Channel][byte] & 0xFF0000) << 8) + MrcLog8 (ErrCount);
                Errors[Controller][Channel][byte] = MrcBitSwap (Errors[Controller][Channel][byte], tmp, 16, 16);
                //              if (param == WrV)
                //              MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE,"Unexpected case for channel: %d, delta: %d.\n", Channel, Delta);
              }
              if (MinTested[Controller][Channel][chByte] < MaxTested[Controller][Channel][chByte]) {
                TwoFail[Controller][Channel] |= (BitMask);
              }
              if (Delta <= 0) {
                TwoPass[Controller][Channel] &= ~(BitMask);
              }
            } else {
              MRC_DEBUG_ASSERT (FALSE, Debug, "Error: Testing points other than Max/MinTested");
            }
            // FIND MAX Saturation limit
            byteMax = MaxMargin;
            if ((param == RdT) || (param == RdV)) {
              byteMax = MrcCalcMaxRxMargin (MrcData, param, Controller, Channel, RankRx, byte, sign, MaxMargin);
            } else if ((param == CmdV) || DimmVrefParam) {
              byteMax = MrcCalcMaxVrefMargin (MrcData, Controller, Channel, RankRx, byte, param, sign, MaxMargin, FALSE);
            }
            if (Interior && InitValue[Controller][Channel][byte] == Margin) {
              byteMax = Margin;
            }
            // HANDLE MAX Saturation
            if (Margin == byteMax) {
              TwoFail[Controller][Channel] |= (BitMask);
            }
            if (ErrCount == 0 && byteMax == LastPassVref[Controller][Channel][byte] && (TwoPass[Controller][Channel] & (BitMask)) != 0) {
              Errors[Controller][Channel][byte] = MrcBitSwap (Errors[Controller][Channel][byte], 0xFFFE, 16, 16);
            }
            // HANDLE MIN Saturation
            if (Margin == 0) {
              TwoPass[Controller][Channel] |= (BitMask);
              if (ErrCount > 0) {
                TwoFail[Controller][Channel] |= (BitMask);
                LastPassVref[Controller][Channel][byte] = 0;
                Errors[Controller][Channel][byte] = MrcBitSwap (
                  Errors[Controller][Channel][byte],
                  (BER_LOG_TARGET << 8) + BER_LOG_TARGET,
                  16,
                  16
                );
              }
            }
            // DECIDE WHAT TO TEST NEXT
            // If In PerByte, Do this within the for byte loop
            if (PerCh == FALSE) {
              // Skip if this Byte Group is done
              if ((TwoPass[Controller][Channel] & TwoFail[Controller][Channel] & (BitMask)) != 0) {
                continue;
              }
              if (ErrCount == 0) {
                if ((TwoFail[Controller][Channel] & (BitMask)) == 0) {
                  // Count up to find 2 fails
                  marginByte[ResultType][Rank][Controller][Channel][byte][sign] = ++MaxTested[Controller][Channel][chByte];
                } else {
                  // Count down to find 2 passes
                  marginByte[ResultType][Rank][Controller][Channel][byte][sign] = --MinTested[Controller][Channel][chByte];
                }
              } else {
                if ((TwoPass[Controller][Channel] & (BitMask)) == 0) {
                  marginByte[ResultType][Rank][Controller][Channel][byte][sign] = --MinTested[Controller][Channel][chByte];
                } else {
                  marginByte[ResultType][Rank][Controller][Channel][byte][sign] = ++MaxTested[Controller][Channel][chByte];
                }
              }
            }
          } // for byte
            // DECIDE WHAT TO TEST NEXT
            // If In PerCh, Do this within the for ch loop
          if (PerCh == TRUE) {
            if ((TwoPass[Controller][Channel] & TwoFail[Controller][Channel]) == DoneMask) {
              continue;
            }
            if (TwoPass[Controller][Channel] != DoneMask) {
              marginByte[ResultType][Rank][Controller][Channel][0][sign] = --MinTested[Controller][Channel][chByte];
            } else {
              marginByte[ResultType][Rank][Controller][Channel][0][sign] = ++MaxTested[Controller][Channel][chByte];
            }
            // All bytes must use the same margin point
            for (byte = 0; byte < Outputs->SdramCount; byte++) {
              marginByte[ResultType][Rank][Controller][Channel][byte][sign] = (marginByte[ResultType][Rank][Controller][Channel][0][sign]);
            }
          }
        }
      }
      // check if we are done
      Done = TRUE;
      for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
        for (Channel = 0; Channel < MaxChannels; Channel++) {
          if ((TwoPass[Controller][Channel] & DoneMask) != DoneMask || (TwoFail[Controller][Channel] & DoneMask) != DoneMask) {
            Done = FALSE;
            break;
          }
        }
      }
    } while (Done == FALSE);
    // Calculate the effective margin
    // Update MarginResult with extrapolated BER Margin
    for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
      for (Channel = 0; Channel < MaxChannels; Channel++) {
        if (!((1 << ((Controller * MaxChannels) + Channel)) & McChBitMask)) {
          continue;
        }
        for (byte = 0; byte < Outputs->SdramCount; byte++) {
          // if (param == WrV) MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE,"+----->marginByte[MC%u C%u B%u, Sign %d] is: %d\n", Controller, Channel, byte, sign,  marginByte[ResultType][Rank][Controller][Channel][byte][sign]);
          if (EnBER) {
            marginByte[ResultType][Rank][Controller][Channel][byte][sign] = (UINT16) interpolateVref (
              LastPassVref[Controller][Channel][byte],
              (Errors[Controller][Channel][byte] >> 16) & 0xFF,
              (Errors[Controller][Channel][byte] >> 24) & 0xFF,
              BER_LOG_TARGET,
              BERStats
            );
            //          if (param == WrV)
  //          MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE,"+----->BERmarginByte[Ch %d, Byte%d, Sign %d] is: %d\n", Channel, byte, sign,  marginByte[ResultType][Rank][Controller][Channel][byte][sign]);
          } else {
            marginByte[ResultType][Rank][Controller][Channel][byte][sign] = 10 * LastPassVref[Controller][Channel][byte];
            //          if (param == WrV)
  //          MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE,"+----->marginByte(%s)[Ch%d, Rank%d, Byte%d, Sign%d] is: %d\n", gResTypeStr[GetMarginResultType (param)], Channel, Rank, byte, sign,  marginByte[ResultType][Rank][Controller][Channel][byte][sign]);
          }
        }
      }
    }
  }
  // Clean up after step
  // MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\nCleanup - ChangeMargin multicast\n");
  Status = ChangeMargin (MrcData, param, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0);

  if ((Ddr4 || Ddr5) && (param == WrV) && Inputs->EnablePda) {
    // @todo: Add CmdV PDA programming for DDR5
    // If eye < 4 dont program PDA and leave it
    // Calc min eye
    for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
      for (Channel = 0; Channel < MaxChannels; Channel++) {
        if (!((1 << ((Controller * MaxChannels) + Channel)) & McChBitMask)) {
          continue;
        }
        for (Byte = 0; Byte < Outputs->SdramCount; Byte++) {
          MinEyeFlag = TRUE;
          for (Edge = 0; Edge < MAX_EDGES; Edge++) {
            // Rank here is result/first rank
            if (marginByte[ResultType][Rank][Controller][Channel][Byte][Edge] < 40) {
              MinEyeFlag = FALSE;
              break;
            }
          }
          if (MinEyeFlag || Ddr5) {
            // DDR4: Program PDA only in case of healthy eye per Byte.
            // DDR5: PDA doesn't use DQ bus (only CMD), hence program using PDA regardless of eye size
            Status = ChangeMargin (MrcData, param, 0, 0, 0, Controller, Channel, RankIn, 1 << Byte, 0, 0, 0);
          }
        }
      }
    }
    // need to reference the margins to their real center per byte
    GetPdaMargins (MrcData, Outputs->MarginResult, param, RankIn & Outputs->ValidRankMask);
  }

  return Status;
}

/**
  Assume REUT test has already been fully setup to run
  Finds the margin for all channels/all bytes
  The margin sweep is parameterized
  Uses ChangeMargin function to handle a variety of cases (Timing, Voltage, Fan, etc.)
  mode allows for different types of modes for margining:
  mode is {Bit0: PhLock (keep all bytes within in ch in phase),
  Bit1: Ch2Ch Data out of phase (LFSR seed), Bit 15:2: Reserved}
  marginCh is used as the starting point for the search (10x the actual margin)
  marginch returns the results (10x the actual margin)
  Interior: Search inside marginch limits, enabling multiple calls with different setups
  To reduce repeatibility noise, the returned margins is actually a BER extrapolation

  @param[in]     MrcData      - The global MrcData
  @param[in,out] marginCh     - Data structure with the latest margin results
  @param[in]     McChBitMask  - Bit mask of present MC channels
  @param[in]     RankRx       - Ranks for Rx margin and Tx Vref (Ddr4)
  @param[in]     RankIn       - Rank to change margins for
  @param[in]     param        - parameter to get margins for
  @param[in]     mode         - allows for different types of modes for margining:
  @param[in]     EnBER        - Enable BER extrapolation calculations
  @param[in]     MaxMargin    - Max Margin allowed for the parameter
  @param[in]     Interior     - Search inside marginch limits, enabling multiple calls with different setups
  @param[in,out] BERStats     - Bit Error Rate Statistics.

  @retval mrcSuccess if successful, otherwise returns an error status.
**/
MrcStatus
MrcGetBERMarginCh (
  IN     MrcParameters  *MrcData,
  IN     UINT16         marginCh[MAX_RESULT_TYPE][MAX_RANK_IN_CHANNEL][MAX_CONTROLLER][MAX_CHANNEL][MAX_SDRAM_IN_DIMM][MAX_EDGES],
  IN OUT UINT8          McChBitMask,
  IN     UINT8          RankRx,
  IN     UINT8          RankIn,
  IN     UINT8          param,
  IN     UINT16         mode,
  IN     UINT8          EnBER,
  IN     UINT8          MaxMargin,
  IN     UINT8          Interior,
  IN OUT UINT32         *BERStats
)
{
  const MrcInput  *Inputs;
  MrcDebug        *Debug;
  const MRC_FUNCTION *MrcCall;
  MrcOutput       *Outputs;
  MrcStatus       Status;
  UINT8           Controller;
  UINT8           ResultType;
  UINT8           sign;
  INT8            rSign;
  UINT8           Points2D;
  UINT8           Channel;
  UINT8           MaxChannels;
  UINT8           byte;
  UINT8           ByteCount;
  UINT8           SkipWait;
  UINT8           byteMax[MAX_CONTROLLER][MAX_CHANNEL];
  UINT8           Margin;
  // Set to 1 after ch has 2 passing points
  UINT16          TwoPass[MAX_CONTROLLER][MAX_CHANNEL];
  // Set to 1 after ch has 2 failing points
  UINT8           TwoFail[MAX_CONTROLLER][MAX_CHANNEL];
  INT8            Delta;
  BOOLEAN         Done;
  BOOLEAN         DimmVrefParam;
  UINT32          value0;
  UINT32          value1;
  UINT32          tmp;
  UINT8           chError;
  UINT32          ErrCount;
  UINT8           LastPassVref[MAX_CONTROLLER][MAX_CHANNEL];  // Last passing Vref
  UINT8           MaxTested[MAX_CONTROLLER][MAX_CHANNEL];     // Highest Vref Point Tested
  UINT8           MinTested[MAX_CONTROLLER][MAX_CHANNEL];     // Lowest Vref Point Tested
                                              // Log8(Error count) at different Vref Points. 32 bit number that is broken in 4 bytes
                                              // [LastPass+2, LastPass+1, LastPass, LastPass-1]
  UINT32          Errors[MAX_CONTROLLER][MAX_CHANNEL];
  BOOLEAN         PerMc;
  UINT8           McChannel;
  UINT8           Rank;
  BOOLEAN         Ddr4;
  BOOLEAN         Ddr5;
  BOOLEAN         Overflow;
  UINT16          ByteMask;

  Inputs      = &MrcData->Inputs;
  MrcCall     = Inputs->Call.Func;
  Outputs     = &MrcData->Outputs;
  Debug       = &Outputs->Debug;
  MaxChannels = Outputs->MaxChannels;
  Status      = mrcSuccess;
  ResultType  = GetMarginResultType (param);
  Points2D    = (param / 16) + 1;        // 2 for Fan2 and 3 for Fan3
  McChannel   = 0;
  Ddr4        = (Outputs->DdrType == MRC_DDR_TYPE_DDR4);
  Ddr5        = (Outputs->DdrType == MRC_DDR_TYPE_DDR5);
  Rank        = GetRankToStoreResults (MrcData, RankIn);
  // Make sure we cover all DIMM Vref cases
  DimmVrefParam = ((param == WrFan2) || (param == WrFan3) || (param == WrV));   // WrFan not defined
  // Every margin parameter is at least per Channel.  So we set this to 0 until we need to enable,
  // per MC margining for future.
  PerMc         = 0;
  // ByteCount is not used for CmdV unless DDR5 with PDA is enabled
  ByteCount     = (param == CmdV && !(Ddr5 && Inputs->EnablePda)) ? 1 : Outputs->SdramCount;

  if ((param == RcvEnaX) || (param == RdV)) {
    // Make sure RecEnDelay / RxVref values exist in CR cache for this algorithm to work, because we will use WriteOffsetUncached
    MrcReadParamIntoCache (MrcData, param);
  }

  // Run through margin test
  for (sign = 0; sign < 2; sign++) {
    rSign = (INT8) ((2 * sign) - 1);

    if ((param == CmdV) && (sign == 1)) {
      // Program safe CmdV so that JEDEC reset will work, when changing direction from left edge to the right
      ChangeMargin (MrcData, CmdV, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0);
      MrcResetSequence (MrcData);
    }
    //    if (DimmVrefParam)
    //    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE,"+----->rsign: %d \n", rSign);
    MrcCall->MrcSetMem ((UINT8 *) TwoPass, sizeof (TwoPass), 0);
    MrcCall->MrcSetMem ((UINT8 *) TwoFail, sizeof (TwoFail), 0);
    MrcCall->MrcSetMem ((UINT8 *) LastPassVref, sizeof (LastPassVref), 0x7F);  // Start with a huge unsigned number - 128
    // Initialize variables
    for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
      for (Channel = 0; Channel < MaxChannels; Channel++) {
        // Set default of all variables
        byteMax[Controller][Channel]      = MaxMargin;
        Errors[Controller][Channel]       = 0;
        MinTested[Controller][Channel]    = 0;
        MaxTested[Controller][Channel]    = 0;
        if (MC_CH_MASK_CHECK (McChBitMask, Controller, Channel, MaxChannels) == 0) {
          TwoPass[Controller][Channel] = 1;
          TwoFail[Controller][Channel] = 1;
          continue;
        }
        // Find MaxMargin for this channel
        if (param == RdT || (param == RdV)) {
          for (byte = 0; byte < Outputs->SdramCount; byte++) {
            byteMax[Controller][Channel] = MrcCalcMaxRxMargin (MrcData, param, Controller, Channel, RankRx, byte, sign, byteMax[Controller][Channel]);
          }
        } else if ((param == CmdV) || DimmVrefParam) {
          byteMax[Controller][Channel] = MrcCalcMaxVrefMargin (MrcData, Controller, Channel, RankRx, 0, param, sign, byteMax[Controller][Channel], FALSE);
        }
        // Scale back variables to normal margins and check saturation
        marginCh[ResultType][Rank][Controller][Channel][0][sign] = marginCh[ResultType][Rank][Controller][Channel][0][sign] / 10;
        if (marginCh[ResultType][Rank][Controller][Channel][0][sign] > byteMax[Controller][Channel]) {
          marginCh[ResultType][Rank][Controller][Channel][0][sign] = byteMax[Controller][Channel];
        }
        // If PerMC, all channels should start with the lowest margin across all the channel
        if (PerMc) {
          if (marginCh[ResultType][Rank][Controller][McChannel][0][sign] > marginCh[ResultType][Rank][Controller][Channel][0][sign]) {
            marginCh[ResultType][Rank][Controller][McChannel][0][sign] = marginCh[ResultType][Rank][Controller][Channel][0][sign];
          }
        }
        MinTested[Controller][Channel] = (UINT8) marginCh[ResultType][Rank][Controller][Channel][0][sign];
        MaxTested[Controller][Channel] = MinTested[Controller][Channel];
        // Setup REUT Error Counters to count errors per channel, all UI
        MrcSetupErrCounterCtl (MrcData, 0, ErrCounterCtlAllLanes, 0);
      } // for Channel
    } // Controller
    // If PerMC, set all channels to use margin associated with mcChannel = 0
    if (PerMc) {
      for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
        for (Channel = 0; Channel < MaxChannels; Channel++) {
          if (MC_CH_MASK_CHECK (McChBitMask, Controller, Channel, MaxChannels) == 0) {
            continue;
          }
          marginCh[ResultType][Rank][Controller][Channel][0][sign] = marginCh[ResultType][Rank][Controller][McChannel][0][sign];
          MinTested[Controller][Channel] = (UINT8) marginCh[ResultType][Rank][Controller][McChannel][0][sign];
          MaxTested[Controller][Channel] = MinTested[Controller][Channel];
        }
      }
    }
    // Search algorithm:
    // If start with a passing point, walk to hit 2 failing points
    //    Return as needed to hit a second passing point
    // If start with a failing point, walk to hit 2 passing points
    //    Return as needed to hit a second failing point
    // Keep testing until all ch/bytes find 2 passes and 2 fails
    Done = FALSE;
    do {
      // Walk through all 2D points
      /** @todo <ICL>: Implement CPGC 2.0 to clear errors
      ReutGlobalCtl.Data = 0;
      ReutGlobalCtl.Bits.Global_Clear_Errors = 1;
      MrcWriteCR (MrcData, REUT_GLOBAL_CTL_REG, ReutGlobalCtl.Data);  // Clear errors
      **/
      chError = 0;
      for (value1 = 0; value1 < Points2D; value1++) {
        // Set Vref level
        for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
          for (Channel = 0; Channel < MaxChannels; Channel++) {
            if (MC_CH_MASK_CHECK (McChBitMask, Controller, Channel, MaxChannels) == 0) {
              continue;
            }
            SkipWait = (McChBitMask >> MC_CH_IDX (Controller, Channel + 1, MaxChannels)); // Skip if there are more channels
            value0 = rSign * marginCh[ResultType][Rank][Controller][Channel][0][sign];
            //          if (DimmVrefParam) {
            //            MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "+----->Value0 is %d, Value1 is %d\n", (INT32) value0, value1);
            //            MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "+----->marginCh is %d\n", marginCh[ResultType][Rank][Controller][Channel][0][sign]);
            //          }
            for (byte = 0; byte < ByteCount; byte++) {
              Status = ChangeMargin (
                        MrcData,
                        param,
                        value0,
                        value1,
                        0,
                        Controller,
                        Channel,
                        RankIn,
                        byte,
                        0,
                        0,
                       SkipWait
                       );
            }
            if (PerMc) {
              break; // Just update once
            }
          } // Channel
        } // Controller
        // Run Test
        chError |= RunIOTest (MrcData, McChBitMask, Outputs->DQPat, (value1 == 0), 0);
        if (param == CmdV) {
          MrcResetSequence (MrcData);
        }
        // check if we have already failed and can stop running
        if (EnBER == 0 && value1 < (UINT32) (Points2D - 1) && chError == McChBitMask) {
          break;
        }
        // Collect results and Update variables for next point to test
        for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
          for (Channel = 0; Channel < MaxChannels; Channel++) {
            if ((TwoPass[Controller][Channel] == 1 && TwoFail[Controller][Channel] == 1) ||
                (MC_CH_MASK_CHECK (McChBitMask, Controller, Channel, MaxChannels) == 0)) {
              continue;
            }
            McChannel = (PerMc) ? 0 : Channel;
            // Read Error Count
            ErrCount = 0;
            MrcGetErrCounterStatus (MrcData, Controller, Channel, 0, ErrCounterCtlAllLanes, &ErrCount, &Overflow);
            Margin  = (UINT8) marginCh[ResultType][Rank][Controller][Channel][0][sign];
            Delta   = (Margin - LastPassVref[Controller][Channel]);

            // Update Pass/Fail Variables:
            if (ErrCount == 0 && Margin == MaxTested[Controller][McChannel]) {
              // Passing while walking up
              if (Delta < 0) {
                // First passing point
                MRC_DEBUG_ASSERT (
                  MinTested[Controller][McChannel] == MaxTested[Controller][McChannel],
                  Debug,
                  "Error: MaxTested < LastPass after first point"
                  );
                LastPassVref[Controller][Channel] = Margin;
              } else if (Delta == 1) {
                // Normal, walk to fail
                Errors[Controller][Channel]       = MrcBitShift (Errors[Controller][Channel], -8 * Delta) & BER_ERROR_MASK;
                LastPassVref[Controller][Channel] = Margin;
                TwoPass[Controller][Channel]      = 1;
              } else if (Delta == 2) {

                // Speckling in response, Consider point as error
                Errors[Controller][Channel]   = MrcBitSwap (Errors[Controller][Channel], MrcLog8 (ErrCount), 24, 8);
                TwoFail[Controller][Channel]  = 1;
              } else {
                MRC_DEBUG_ASSERT (FALSE, Debug, "Error: Tested point twice or Tested >2 above LastPass");
              }
            } else if (ErrCount == 0 && Margin == MinTested[Controller][McChannel]) {
              if (TwoPass[Controller][Channel] == 1) {
                continue; // Skip if this channel is already done
              }

              // Passing while walking down
              if (Delta == -1) {
                Errors[Controller][Channel]  = MrcBitSwap (Errors[Controller][Channel], 0, 0, 8);
                TwoPass[Controller][Channel] = 1;     // found second pass
              } else {
                // 1st passing point
                // Don't shift errors.  Fail points already assumed correct
                LastPassVref[Controller][Channel] = Margin;
                TwoPass[Controller][Channel]      = 0;
              }

            } else if (ErrCount > 0 && Margin == MaxTested[Controller][McChannel]) {
              // Failing while walking up

              MRC_DEBUG_ASSERT (Delta <= 2, Debug, "Error: Tested >2 above LastPass");
              if (Delta < 2) {
                // first failing point
                Errors[Controller][Channel]  = MrcBitSwap (Errors[Controller][Channel], MrcLog8 (ErrCount), 16, 8);
                TwoFail[Controller][Channel] = 0;
              } else if (Delta == 2) {
                // 2nd failing point
                Errors[Controller][Channel]  = MrcBitSwap (Errors[Controller][Channel], MrcLog8 (ErrCount), 24, 8);
                TwoFail[Controller][Channel] = 1;
              }
            } else if (ErrCount > 0 && Margin == MinTested[Controller][McChannel]) {
              // Failing while walking down
              if (LastPassVref[Controller][Channel] < 0xFF && Delta <= 0) {
                // Adjust LastPassVref and Error count to be below this failure point
                Errors[Controller][Channel]       = MrcBitSwap (Errors[Controller][Channel], MrcLog8 (ErrCount), 8 * (Delta + 1), 8);
                Errors[Controller][Channel]       = MrcBitShift (Errors[Controller][Channel], 8 * (1 - Delta));
                LastPassVref[Controller][Channel] = Margin - 1;
              } else {

                MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Unexpected case for Mc%u.C%u, delta: %d.\n", Controller, Channel, Delta);
                tmp = ((Errors[Controller][Channel] & 0xFF0000) >> 8) + MrcLog8 (ErrCount);
                Errors[Controller][Channel] = MrcBitSwap (Errors[Controller][Channel], tmp, 16, 16);
              }

              if (MinTested[Controller][McChannel] < MaxTested[Controller][McChannel]) {
                TwoFail[Controller][Channel] = 1;
              }
              if (Delta <= 0) {
                TwoPass[Controller][Channel] = 0;
              }
            } else {
              MRC_DEBUG_ASSERT (FALSE, Debug, "Error: Testing points other than Max/MinTested");
            }
            // Find Max Saturation limit
            if (Interior && MaxTested[Controller][Channel] == Margin) {
              byteMax[Controller][Channel] = Margin;
            }
            // Handle Max Saturation
            if (Margin == byteMax[Controller][Channel]) {
              TwoFail[Controller][Channel] = 1;
            }
            if (ErrCount == 0 && byteMax[Controller][Channel] == LastPassVref[Controller][Channel] && TwoPass[Controller][Channel] == 1) {
              Errors[Controller][Channel] = MrcBitSwap (Errors[Controller][Channel], 0xFFFE, 16, 16);
            }
            // Handle Min Saturation
            if (Margin == 0) {
              TwoPass[Controller][Channel] = 1;
              if (ErrCount > 0) {
                TwoFail[Controller][Channel]      = 1;
                LastPassVref[Controller][Channel] = 0;
                Errors[Controller][Channel]       = MrcBitSwap (Errors[Controller][Channel], (BER_LOG_TARGET << 8) + BER_LOG_TARGET, 16, 16);
              }
            }
            // Decide what to test next for PerMC == FALSE
            if (!PerMc) {
              if (TwoPass[Controller][Channel] == 1) {
                if (TwoFail[Controller][Channel] == 1) {
                  continue;
                }
                //MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE,"++---->Mc%u.C%u MaxTested before ++:%d\n", Controller, Channel, MaxTested[Controller][Channel]);
                marginCh[ResultType][Rank][Controller][Channel][0][sign] = ++MaxTested[Controller][Channel];
              } else {
                //MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE,"++---->Mc%u.C%u MinTested before --:%d\n", Controller, Channel, MinTested[Controller][Channel]);
                marginCh[ResultType][Rank][Controller][Channel][0][sign] = --MinTested[Controller][Channel];
              }
            }
          } // Channel
        } // Controller
        // Decide what to test next for PerMC == TRUE
        if (PerMc) {
          for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
            if ((TwoPass[Controller][0] == 1) && (TwoPass[Controller][1] == 1)) {
              // All Channels have 2 passes
              if ((TwoFail[Controller][0] == 1) && (TwoFail[Controller][1] == 1)) {
                // All Channels have 2 fails
                continue;
              }
              //MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE,"++---->Mc%u.C%u MaxTested before ++:%d\n", Controller, McChannel, MaxTested[Controller][McChannel]);
              marginCh[ResultType][Rank][Controller][McChannel][0][sign] = ++MaxTested[Controller][McChannel];
            } else {
              //MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE,"++---->Mc%u.C%u MinTested before --:%d\n", Controller, McChannel, MinTested[Controller][McChannel]);
              marginCh[ResultType][Rank][Controller][McChannel][0][sign] = --MinTested[Controller][McChannel];
            }
            for (Channel = 0; Channel < MaxChannels; Channel++) {
              if (MC_CH_MASK_CHECK (McChBitMask, Controller, Channel, MaxChannels) == 0) {
                continue;
              }
              marginCh[ResultType][Rank][Controller][Channel][0][sign] = marginCh[ResultType][Rank][Controller][McChannel][0][sign];
              MinTested[Controller][Channel] = MinTested[Controller][McChannel];
              MaxTested[Controller][Channel] = MaxTested[Controller][McChannel];
            }
          }
        }
      }
      // check if we are done
      Done = TRUE;
      for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
        for (Channel = 0; Channel < MaxChannels; Channel++) {
          if (TwoPass[Controller][Channel] == 0 || TwoFail[Controller][Channel] == 0) {
            Done = FALSE;
            break;
          }
        }
      }
    } while (Done == FALSE);
    // Calculate the effective margin
    // Update marginch with extroploated BER Margin
    for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
      for (Channel = 0; Channel < MaxChannels; Channel++) {
        if (MC_CH_MASK_CHECK (McChBitMask, Controller, Channel, MaxChannels) == 0) {
          continue;
        }
        //MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE,"+----->Mc%u.C%u.E%u marginCh is: %d\n", Controller, Channel, sign, marginCh[ResultType][Rank][Controller][Channel][0][sign]);
        if (EnBER) {
          marginCh[ResultType][Rank][Controller][Channel][0][sign] = (UINT16) interpolateVref (
                                                                                          LastPassVref[Controller][Channel],
                                                                                          (Errors[Controller][Channel] >> 16) & 0xFF,
                                                                                          (Errors[Controller][Channel] >> 24) & 0xFF,
                                                                                          BER_LOG_TARGET,
                                                                                          BERStats
                                                                                          );
          //MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "+----->Mc%u.C%u.E%u BERmarginCh is: %d, Errors = 0x%x\n", Channel, sign, marginCh[ResultType][Rank][Controller][Channel][0][sign], Errors[Controller][Channel]);
        } else {
          marginCh[ResultType][Rank][Controller][Channel][0][sign] = 10 * LastPassVref[Controller][Channel];
          //MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "+----->Mc%u.C%u.E%u marginCh is: %d\n", Controller, Channel, sign, marginCh[ResultType][Rank][Controller][Channel][0][sign]);
        }
      }
    }
  }
  // Clean up after step
  ByteMask = ((param == WrV) && Ddr4) ? 0x1FF : 0;
  Status = ChangeMargin (MrcData, param, 0, 0, 1, 0, 0, 0, ByteMask, 0, 0, 0);
  return Status;
}

/**
  Check whether there is errors at Point RdT/RdV or WrT/WrV

  @param[in]      MrcData     - Include all MRC global data.
  @param[in]      McChBitmask - Bit mask of present MC channels
  @param[in]      RankMask    - Bit mask of Ranks to change margins for
  @param[in]      MarginPoint - Margin Point to test
  @param[in]      ParamType   - MRC_MarginTypes: WrV, WrT, RdT, RdV.

  @retval MrcStatus - mrcSuccess if point successful pass, otherwise returns an error status.
**/
MrcStatus
MrcDataPointTest (
  IN     MrcParameters     *MrcData,
  IN     UINT8             McBitMask,
  IN     UINT8             RankMask,
  IN     MarginCheckPoint  *MarginPoint,
  IN     MRC_MarginTypes   ParamType
)
{
  MrcOutput           *Outputs;
  MrcDebug            *Debug;
  MrcStatus           Status;
  UINT8               McChError;
  INT32               Value;

  Outputs = &MrcData->Outputs;
  Debug = &Outputs->Debug;

  // Check to make sure type is RdT/RdV or WrT/WrV
  if (!(((MarginPoint->VoltageType == RdV) && (MarginPoint->TimingType == RdT)) ||
    ((MarginPoint->VoltageType == WrV) && (MarginPoint->TimingType == WrT)))) {
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_ERROR, "Function MrcDataPointTest, Invalid combination TimingType: %d VoltageType:%d\n", MarginPoint->TimingType, MarginPoint->VoltageType);
    return mrcWrongInputParameter;
  }

  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "MrcDataPointTest: ParamType: %s, TimingMargin: %d, VoltageMargin: %d\n", gMarginTypesStr[ParamType], MarginPoint->TimingMargin, MarginPoint->VoltageMargin);

  // Change margin before the test
  if ((ParamType == RdV) || (ParamType == WrV)) {
    Value = MarginPoint->VoltageMargin;
  } else {
    Value = MarginPoint->TimingMargin;
  }

  // EnMulticast = 1, byte (mask) = 0x1FF for DDR4 WrV PDA
  ChangeMargin (MrcData, ParamType, Value, 0, 1, 0, 0, RankMask, 0x1FF, 0, 0, 0);

  // Run Test
  McChError = RunIOTest (MrcData, McBitMask, Outputs->DQPat, 1, 0);
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "McChError: %d\n", McChError);

  // Set whether it passed/failed
  Status = (McChError > 0) ? mrcFail : mrcSuccess;

  // Clean up margin
  ChangeMargin (MrcData, ParamType, 0, 0, 1, 0, 0, RankMask, 0x1FF, 0, 0, 0);

  return Status;
}

/**
  Check whether there is errors at Point CmdT/CmdV

  @param[in]      MrcData     - Include all MRC global data.
  @param[in]      McChBitMask - Bit mask of present MC channels.
  @param[in]      RankMask    - Bit mask of Ranks to change margins for
  @param[in]      MarginPoint - CMD Margin Point to test
  @param[in]      ParamType   - MRC_MarginTypes: CmdT, CmdV.

  @retval MrcStatus - mrcSuccess if point successful pass, otherwise returns an error status.
**/
MrcStatus
MrcCmdPointTest (
  IN     MrcParameters    *MrcData,
  IN     UINT8            McChBitMask,
  IN     UINT8            RankMask,
  IN     MarginCheckPoint *MarginPoint,
  IN     MRC_MarginTypes  ParamType
)
{
  MrcOutput             *Outputs;
  MrcDebug              *Debug;
  MrcStatus             Status;
  UINT32                Rank;
  UINT8                 Controller;
  UINT8                 Channel;
  UINT8                 MaxChannels;
  UINT8                 McChError;
  UINT8                 SkipWait;
  MrcIntOutput          *IntOutputs;
  MrcIntControllerOut   *IntControllerOut;
  MrcIntCmdTimingOut    *IntCmdTiming;
  UINT8                 CmdGroup;
  UINT8                 CmdGroupMax;

  IntOutputs = (MrcIntOutput *) (MrcData->IntOutputs.Internal);

  Outputs = &MrcData->Outputs;
  Debug = &Outputs->Debug;
  MaxChannels = Outputs->MaxChannels;
  CmdGroupMax = (Outputs->DdrType == MRC_DDR_TYPE_DDR4) ? MRC_DDR4_CMD_GRP_MAX : ((Outputs->DdrType == MRC_DDR_TYPE_DDR5) ? MRC_DDR5_CMD_GRP_MAX : 1); // Number of cmd PI groups: DDR4: 4, DDR5: 2, LP4/5: 1

  // Check to make sure type is CmdT/CmdV
  if ((MarginPoint->VoltageType != CmdV) || (MarginPoint->TimingType != CmdT)) {
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_ERROR, "Function MrcCmdPointTest, Invalid combination TimingType: %d VoltageType:%d\n", MarginPoint->TimingType, MarginPoint->VoltageType);
    return mrcWrongInputParameter;
  }

  // Change CmdV/CmdT before the test
  for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
    IntControllerOut = &IntOutputs->Controller[Controller];
    for (Channel = 0; Channel < Outputs->MaxChannels; Channel++) {
      if (MC_CH_MASK_CHECK (McChBitMask, Controller, Channel, MaxChannels) == 0) {
        continue;
      }
      SkipWait = (McChBitMask >> ((Controller * MaxChannels) + (Channel + 1))); // Skip if there are more channels
      if (ParamType == CmdV) {
        // Change CmdV
        ChangeMargin (MrcData, MarginPoint->VoltageType, MarginPoint->VoltageMargin, 0, 0, Controller, Channel, RankMask, 0, 0, 0, SkipWait);
      } else {
        // Change CmdT
        IntCmdTiming = &IntControllerOut->CmdTiming[Channel];
        for (CmdGroup = 0; CmdGroup < CmdGroupMax; CmdGroup++) {
          ShiftPIforCmdTraining (MrcData, Controller, Channel, MrcIterationCmd, MRC_IGNORE_ARG_8, (1 << CmdGroup), (IntCmdTiming->CmdPiCode[CmdGroup] + MarginPoint->TimingMargin), 0);
        }
        for (Rank = 0; Rank < MAX_RANK_IN_CHANNEL; Rank++) {
          if ((1 << Rank) & RankMask) {
            ShiftPIforCmdTraining (MrcData, Controller, Channel, MrcIterationCtl, (1 << Rank), 0, (IntCmdTiming->CtlPiCode[Rank] + MarginPoint->TimingMargin), 0);
          }
        }
      } // CmdT
    }
  }

  // Run CPGC test on both channels
  McChError = RunIOTest (MrcData, McChBitMask, Outputs->DQPat, 1, 0);

  // Set whether it passed/failed
  Status = (McChError > 0) ? mrcFail : mrcSuccess;

  // Restore centered value for CmdT
  if (ParamType == CmdT) {
    for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
      IntControllerOut = &IntOutputs->Controller[Controller];
      for (Channel = 0; Channel < Outputs->MaxChannels; Channel++) {
        if ((1 << ((Controller * MaxChannels) + Channel)) & McChBitMask) {
          IntCmdTiming = &IntControllerOut->CmdTiming[Channel];
          for (CmdGroup = 0; CmdGroup < CmdGroupMax; CmdGroup++) {
            ShiftPIforCmdTraining (MrcData, Controller, Channel, MrcIterationCmd, 0, (1 << CmdGroup), IntCmdTiming->CmdPiCode[CmdGroup], 0);
          }
          for (Rank = 0; Rank < MAX_RANK_IN_CHANNEL; Rank++) {
            if ((1 << Rank) & RankMask) {
              ShiftPIforCmdTraining (MrcData, Controller, Channel, MrcIterationCtl, (1 << Rank), 0, IntCmdTiming->CtlPiCode[Rank], 0);
            }
          }
        }
      } // Channel
    } // Controller
  } else {
    // Clean up CmdV
    ChangeMargin (MrcData, MarginPoint->VoltageType, 0, 0, 1, 0, 0, RankMask, 0, 0, 0, 0);
  }

  MrcResetSequence (MrcData);

  return Status;
}

/**
  This function shifts a 32 bit int either positive or negative

  @param[in] Value       - Input value to be shifted
  @param[in] ShiftAmount - Number of bits places to be shifted.

  @retval 0 if ShiftAmount exceeds +/- 31.  Otherwise the updated 32 bit value.
**/
UINT32
MrcBitShift (
  IN const UINT32 Value,
  IN const INT8  ShiftAmount
  )
{
  if ((ShiftAmount > 31) || (ShiftAmount < -31)) {
    return 0;
  }

  if (ShiftAmount > 0) {
    return Value << ShiftAmount;
  } else {
    return Value >> (-1 * ShiftAmount);
  }
}

/**
  This function Sign extends OldMSB to NewMSB Bits (Eg: Bit 6 to Bit 7)

  @param[in] CurrentValue - Input value to be shifted
  @param[in] OldMSB       - The original most significant Bit
  @param[in] NewMSB       - The new most significant bit.

  @retval The updated 8 bit value.
**/
UINT16
MrcSE (
  IN UINT16     CurrentValue,
  IN const UINT16 OldMSB,
  IN const UINT16 NewMSB
  )
{
  UINT16 Scratch;

  Scratch = ((MRC_BIT0 << (NewMSB - OldMSB)) - 1) << OldMSB;
  if (CurrentValue >> (OldMSB - 1)) {
    CurrentValue |= Scratch;
  } else {
    CurrentValue &= (~Scratch);
  }

  return CurrentValue;
}

/**
  This function calculates the Log base 2 of the value to a maximum of Bits

  @param[in] Value - Input value

  @retval Returns the log base 2 of input value + 1.
**/
UINT8
MrcLog2 (
  IN const UINT32 Value
  )
{
  UINT8 Log;
  UINT8 Bit;

  // Return 0 if value is negative
  Log = 0;
  if ((Value + 1) != 0) {
    for (Bit = 0; Bit < 32; Bit++) {
      if (Value & (0x1 << Bit)) {
        Log = (Bit + 1);
      }
    }
  }

  return Log;
}

/**
  This function calculates the power of integer numbers.

  @param[in] Value - Input value.
  @param[in] Power - the power to raise the Value to

  @retval Returns (Value ^ Power) * 100
**/
UINT32
MrcPercentPower (
  IN const UINT32 Value,
  IN UINT32    Power
  )
{
  UINT32  Result;

  Result = 100;
  while (Power--) {
    Result *= Value;
    if (Result != Value) { // normalize to percent
      Result /= 100;
    }
  }

  return Result;
}

/**
  This function search for item in a list and return index + 1 or 0 in not found

  @param[in] Param        - Param to search for
  @param[in] ParamList    - list of Params
  @param[in] ParamLen     - Params length
  @param[in] Offset       - list of the corresponded Offsets to work on
  @param[out] Override    - override indicator
  @param[out] LocalOffset - localoffset

  @retval Returns result
**/
UINT8
MrcSearchList (
  IN           UINT8   Param,
  IN     const UINT8   *ParamList,
  IN     const UINT8   ParamLen,
  IN     const INT8    *Offset,
  OUT          BOOLEAN *Override,
  OUT          INT8    *LocalOffset
  )
{
  UINT8 Result = 0;
  UINT8 Idx;

  if ((NULL == ParamList) ||
      (NULL == Offset) ||
      (NULL == Override) ||
      (NULL == LocalOffset)) {
    return 0;
  }

  for (Idx = 0; Idx < ParamLen; Idx++) {
    if (ParamList[Idx] == Param) {
      Result = Idx + 1;
      break;
    }
  }
  if (Result == 0) {
    *Override = FALSE;
    *LocalOffset = 0;
  } else {
    *Override = TRUE;
    *LocalOffset = Offset[Result - 1];
  }
  return Result;
}

/**
  ***** Has Buffer Overflow for 68-71, 544-575, 4352-4607, ... ****
  This function calculates the Log base 8 of the Input parameter using integers

  @param[in] Value - Input value

  @retval Returns 10x the log base 8 of input Value
**/
UINT32
MrcLog8 (
  IN UINT32 Value
  )
{
  static const UINT8 Loglook[18] = { 0, 0, 1, 2, 3, 4, 5, 6, 7, 7, 8, 8, 9, 9, 9, 10, 10, 10 };
  UINT32    Loga;
  UINT32    Rema;

  Loga  = 0;
  Rema  = 2 * Value;
  while (Value > 8) {
    Rema  = Value >> 2;
    Value = Value >> 3;
    Loga += 10;
  };

  return (Loga + Loglook[Rema]); //returns an integer approximation of "log8(a) * 10"
}

/**
  This function Sorts Arr from largest to smallest

  @param[in,out] Arr     - Array to be sorted
  @param[in]     Channel - Channel to sort.
  @param[in]     lenArr  - Length of the array

  @retval Nothing
**/
void
MrcBsortPerChannel (
  IN OUT UINT32   Arr[MAX_CHANNEL][4],
  IN     const UINT8 Channel,
  IN     const UINT8 lenArr
  )
{
  UINT8 i;
  UINT8 j;
  UINT32 temp;

  for (i = 0; i < lenArr - 1; i++) {
    for (j = 0; j < lenArr - (1 + i); j++) {
      if (Arr[Channel][j] < Arr[Channel][j + 1]) {
        temp                = Arr[Channel][j];
        Arr[Channel][j]     = Arr[Channel][j + 1];
        Arr[Channel][j + 1] = temp;
      }
    }
  }

  return;
}

/**
  This function Sorts Arr from largest to smallest

  @param[in,out] Arr    - Array to be sort
  @param[in]     lenArr - Lenght of the array

  @retval Nothing
**/
void
MrcBsort (
  IN OUT UINT32 *const Arr,
  IN     const UINT8 lenArr
  )
{
  UINT8 i;
  UINT8 j;
  UINT32 temp;

  for (i = 0; i < lenArr - 1; i++) {
    for (j = 0; j < lenArr - (1 + i); j++) {
      if (Arr[j] < Arr[j + 1]) {
        temp        = Arr[j];
        Arr[j]      = Arr[j + 1];
        Arr[j + 1]  = temp;
      }
    }
  }

  return;
}

/**
  This function calculates the Natural Log of the Input parameter using integers

  @param[in] Input - 100 times a number to get the Natural log from.
                      Max Input Number is 40,000 (without 100x)

  @retval 100 times the actual result. Accurate within +/- 2
**/
UINT32
MrcNaturalLog (
  UINT32 Input
  )
{
  UINT32 Output;

  Output = 0;
  while (Input > 271) {
    Input = (Input * 1000) / 2718;
    Output += 100;
  }

  Output += ((-16 * Input * Input + 11578 * Input - 978860) / 10000);

  return Output;
}

/**
  This function calculates the number of bits set to 1 in a 32-bit value.

  @param[in] Input - The value to work on.

  @retval The number of bits set to 1 in Input.
**/
UINT8
MrcCountBitsEqOne (
  IN UINT32 Input
  )
{
  UINT8 NumOnes;

  NumOnes = 0;
  while (Input > 0) {
    NumOnes++;
    Input &= (Input - 1);
  }

  return NumOnes;
}

/**
  This function calculates e to the power of the Input parameter using integers.

  @param[in] Input - 100 times a number to elevate e to.

  @retval 100 times the actual result. Accurate within +/- 2.
**/
UINT32
Mrceexp (
  IN UINT32 Input
  )
{
  UINT32 Extra100;
  UINT32 Output;

  Extra100  = 0;
  Output    = 1;
  while (Input > 100) {
    Input -= 100;
    Output = (Output * 2718) / 10;
    if (Extra100) {
      Output /= 100;
    }

    Extra100 = 1;
  }

  Output = ((Output * (8 * Input * Input + 900 * Input + 101000)) / 1000);

  if (Extra100) {
    Output /= 100;
  }

  return Output;
}

/**
  This function writes a 32 bit register.

  @param[in] MrcData - Include all MRC global data.
  @param[in] Offset  - Offset of register from MchBar Base Address.
  @param[in] Value   - Value to write.

  @retval The register value.
**/
UINT32
MrcWriteCrMulticast (
  IN MrcParameters *const MrcData,
  IN const UINT32         Offset,
  IN const UINT32         Value
  )
{
  const MRC_FUNCTION *MrcCall;
  const MrcInput     *Inputs;
  MrcOutput          *Outputs;
  MrcDebug           *Debug;

  Inputs  = &MrcData->Inputs;
  Outputs = &MrcData->Outputs;
  Debug   = &Outputs->Debug;
  MrcCall = Inputs->Call.Func;
  Outputs->MchBarWriteCount++;

  MrcCall->MrcMmioWrite32 (Inputs->MchBarBaseAddress + Offset, Value);
  MRC_DEBUG_ASSERT ((Offset % 4) == 0, Debug, "Invalid Offset alignment: 0x%x\n", Offset);
#ifdef MRC_DEBUG_PRINT
  if (Debug->PostCode[MRC_POST_CODE_WRITE] == Debug->PostCode[MRC_POST_CODE]) {
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "<mc>%04X M %08X</mc>\n", Offset, Value);
  }
#endif // MRC_DEBUG_PRINT
  return (Value);
}

/**
  This function writes a 64 bit register.

  @param[in] MrcData - Include all MRC global data.
  @param[in] Offset  - Offset of register from MchBar Base Address.
  @param[in] Value   - Value to write.

  @retval The register value.
**/
UINT64
MrcWriteCR64 (
  IN MrcParameters *const MrcData,
  IN const UINT32         Offset,
  IN const UINT64         Value
  )
{
  const MRC_FUNCTION *MrcCall;
  const MrcInput     *Inputs;
  MrcOutput          *Outputs;
  MrcDebug           *Debug;

  Inputs  = &MrcData->Inputs;
  Outputs = &MrcData->Outputs;
  Debug   = &Outputs->Debug;
  MrcCall = Inputs->Call.Func;
  Outputs->MchBarWriteCount++;

  MrcCall->MrcMmioWrite64 (Inputs->MchBarBaseAddress + Offset, Value);
  MRC_DEBUG_ASSERT ((Offset % 8) == 0, Debug, "Invalid Offset alignment: 0x%x\n", Offset);
#ifdef MRC_DEBUG_PRINT
  if (Debug->PostCode[MRC_POST_CODE_WRITE] == Debug->PostCode[MRC_POST_CODE]) {
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "<mc>%04X W %llX</mc>\n", Offset, Value);
  }
#endif // MRC_DEBUG_PRINT
  return (Value);
}

/**
  This function writes a 32 bit register.

  @param[in] MrcData - Include all MRC global data.
  @param[in] Offset  - Offset of register from MchBar Base Address.
  @param[in] Value   - Value to write.

  @retval The register value.
**/
UINT32
MrcWriteCR (
  IN MrcParameters *const MrcData,
  IN const UINT32  Offset,
  IN const UINT32  Value
  )
{
  const MRC_FUNCTION *MrcCall;
  const MrcInput     *Inputs;
  MrcOutput          *Outputs;
  MrcDebug           *Debug;

  Inputs  = &MrcData->Inputs;
  Outputs = &MrcData->Outputs;
  Debug   = &Outputs->Debug;
  MrcCall = Inputs->Call.Func;
  Outputs->MchBarWriteCount++;

  MrcCall->MrcMmioWrite32 (Inputs->MchBarBaseAddress + Offset, Value);
  MRC_DEBUG_ASSERT ((Offset % 4) == 0, Debug, "Invalid Offset alignment: 0x%x\n", Offset);
#ifdef MRC_DEBUG_PRINT
  if (Debug->PostCode[MRC_POST_CODE_WRITE] == Debug->PostCode[MRC_POST_CODE]) {
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "<mc>%04X W %08X</mc>\n", Offset, Value);
  }
#endif // MRC_DEBUG_PRINT
  return (Value);
}

/**
  This function writes a 8 bit register.

  @param[in] MrcData - Include all MRC global data.
  @param[in] Offset  - Offset of register from MchBar Base Address.
  @param[in] Value   - The value to write.

  @retval The register value.
**/
UINT8
MrcWriteCR8 (
  IN MrcParameters *const MrcData,
  IN const UINT32  Offset,
  IN const UINT8   Value
  )
{
  const MRC_FUNCTION *MrcCall;
  const MrcInput     *Inputs;
  MrcOutput          *Outputs;
  MrcDebug           *Debug;

  Inputs  = &MrcData->Inputs;
  Outputs = &MrcData->Outputs;
  Debug   = &Outputs->Debug;
  MrcCall = Inputs->Call.Func;
  Outputs->MchBarWriteCount++;

  MrcCall->MrcMmioWrite8 (MrcData->Inputs.MchBarBaseAddress + Offset, Value);
#ifdef MRC_DEBUG_PRINT
  if (Debug->PostCode[MRC_POST_CODE_WRITE] == Debug->PostCode[MRC_POST_CODE]) {
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "<mc>%04X W %02X</mc>\n", Offset, Value);
  }
#endif // MRC_DEBUG_PRINT
  return (Value);
}

/**
  This function reads a 64 bit register.

  @param[in] MrcData - Include all MRC global data.
  @param[in] Offset  - Offset of register from MchBar Base Address.

  @retval Value read from the register.
**/
UINT64
MrcReadCR64 (
  IN MrcParameters *const MrcData,
  IN const UINT32  Offset
  )
{
  const MRC_FUNCTION *MrcCall;
  const MrcInput     *Inputs;
  MrcOutput          *Outputs;
  MrcDebug           *Debug;
  UINT64             Value;

  Inputs  = &MrcData->Inputs;
  Outputs = &MrcData->Outputs;
  Debug   = &Outputs->Debug;
  MrcCall = Inputs->Call.Func;
  Outputs->MchBarReadCount++;

  Value = (MrcCall->MrcMmioRead64 (Inputs->MchBarBaseAddress + Offset));
  MRC_DEBUG_ASSERT ((Offset % 8) == 0, Debug, "Invalid Offset alignment: 0x%x\n", Offset);
#ifdef MRC_DEBUG_PRINT
  if (Debug->PostCode[MRC_POST_CODE_READ] == Debug->PostCode[MRC_POST_CODE]) {
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "<mc>%04X R %llX</mc>\n", Offset, Value);
  }
#endif // MRC_DEBUG_PRINT
  return (Value);
}

/**
  This function reads a 32 bit register.

  @param[in] MrcData - Include all MRC global data.
  @param[in] Offset  - Offset of register from MchBar Base Address.

  @retval Value read from the register
**/
UINT32
MrcReadCR (
  IN MrcParameters *const MrcData,
  IN const UINT32  Offset
  )
{
  const MRC_FUNCTION *MrcCall;
  const MrcInput     *Inputs;
  MrcOutput          *Outputs;
  MrcDebug           *Debug;
  UINT32             Value;

  Inputs  = &MrcData->Inputs;
  Outputs = &MrcData->Outputs;
  Debug   = &Outputs->Debug;
  MrcCall = Inputs->Call.Func;
  Outputs->MchBarReadCount++;

  Value = MrcCall->MrcMmioRead32 (Inputs->MchBarBaseAddress + Offset);
  MRC_DEBUG_ASSERT ((Offset % 4) == 0, Debug, "Invalid Offset alignment: 0x%x\n", Offset);
#ifdef MRC_DEBUG_PRINT
  if (Debug->PostCode[MRC_POST_CODE_READ] == Debug->PostCode[MRC_POST_CODE]) {
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "<mc>%04X R %08X</mc>\n", Offset, Value);
  }
#endif // MRC_DEBUG_PRINT
  return (Value);
}

/**
  Wait for at least the given number of nanoseconds.

  @param[in] MrcData  - Include all MRC global data.
  @param[in] DelayNs  - time to wait in [ns], up to 2^32 [ns] = 4.29 seconds

  @retval Nothing
**/
void
MrcWait (
  IN MrcParameters *const MrcData,
  IN UINT32               DelayNs
  )
{
  const MRC_FUNCTION *MrcCall;

  if (DelayNs == 0) {
    MRC_DEBUG_MSG (&MrcData->Outputs.Debug, MSG_LEVEL_NOTE, "%sMrcWait(%dns)\n", gErrString, DelayNs);
    return;
  }

  MrcCall = MrcData->Inputs.Call.Func;
  MrcCall->MrcDelayNs (MrcData, DelayNs);
}

/**
  Wait for at least the given number of DRAM clocks.
  The actual delay time depends in the current DRAM clock period.

  @param[in] MrcData   - Include all MRC global data.
  @param[in] DelayTck  - time to wait in clocks, up to 2^32 [tCK]

  @retval Nothing
**/
extern
void
MrcWaitClk (
  IN MrcParameters *const MrcData,
  IN UINT32               DelayTck
  )
{
  UINT32 DelayNs;

  DelayNs = (DelayTck * MrcData->Outputs.MemoryClock);
  DelayNs = DIVIDECEIL (DelayNs, FEMTOSECONDS_PER_NANOSECOND);

  MrcWait (MrcData, DelayNs);
}

/**
  This function forces an RCOMP.

  @param[in] MrcData - Include all MRC global data.

  @retval mrcSuccess if successful, otherwise returns an error status.
**/
MrcStatus
ForceRcomp (
  IN MrcParameters *const MrcData
  )
{

  const MRC_FUNCTION        *MrcCall;
  MrcInput                  *Inputs;
  MrcIntOutput              *MrcIntData;
  M_COMP_PCU_STRUCT         PcuCrMComp;
  UINT64                    Timeout;
  UINT32                    Offset;
  BOOLEAN                   Flag;

  Inputs      = &MrcData->Inputs;
  MrcCall     = Inputs->Call.Func;
  MrcIntData  = (MrcIntOutput *) MrcData->IntOutputs.Internal;

  if (Inputs->BootMode != bmCold) {
    if (MrcIntData->LastCrValue == 0) {
      Offset = (Inputs->A0) ? DDRSCRAM_CR_DDRLASTCR_A0_REG : DDRSCRAM_CR_DDRLASTCR_REG;
      if (MrcReadCR (MrcData, Offset) == 1) { // @todo_adl L0/R0 have a new field StaticSouthSpineGateEn here
        MrcIntData->LastCrValue = 1;
      } else {
        return mrcSuccess;  // Don't issue RCOMP in Fast/Warm flows before LASTCR
      }
    }
  }

  PcuCrMComp.Data = MrcReadCR (MrcData, M_COMP_PCU_REG);
  PcuCrMComp.Bits.COMP_FORCE = 1;
  MrcWriteCR (MrcData, M_COMP_PCU_REG, PcuCrMComp.Data);

  if (Inputs->SimicsFlag == 0) {
    Timeout = MrcCall->MrcGetCpuTime () + 1000; // 1 seconds timeout
    do {
      PcuCrMComp.Data = MrcReadCR (MrcData, M_COMP_PCU_REG);
      Flag = (PcuCrMComp.Bits.COMP_FORCE == 1);
    } while (Flag && (MrcCall->MrcGetCpuTime () < Timeout));

    if (Flag) {
      MRC_DEBUG_MSG (&MrcData->Outputs.Debug, MSG_LEVEL_ERROR, "%s Rcomp Cycle %s\n",gErrString, gTimeout);
      return mrcFail;
    }
  }

  return mrcSuccess;
}

/**
  This function control's entering and exiting self refresh.

  @param[in]  MrcData - Pointer to MRC global data.
  @param[in]  SelfRef - Boolean selector to enter (1/TRUE) or exit (0/FALSE) Self Refresh.

  @retval MrcStatus - mrcSuccess if we are able to transition to the requested Self Refresh State
                    - mrcFail if we timeout before MC reports we reached the requested Self Refresh state.
**/
MrcStatus
MrcSelfRefreshState (
  IN  MrcParameters *const  MrcData,
  IN  BOOLEAN               SelfRef
  )
{
  MrcDebug  *Debug;
  MrcOutput *Outputs;
  MrcStatus Status;
  INT64     GetSetVal;
  UINT64    Timeout;
  UINT32    Controller;
  UINT32    Offset;
  BOOLEAN   Flag;
  const MRC_FUNCTION      *MrcCall;
  MC0_STALL_DRAIN_STRUCT  StallDrain;


  MrcCall = MrcData->Inputs.Call.Func;
  Outputs = &MrcData->Outputs;
  Debug   = &Outputs->Debug;
  Status  = mrcSuccess;

  GetSetVal = SelfRef;
  MrcGetSetMc (MrcData, MAX_CONTROLLER, GsmMccEnableSr, WriteNoCache, &GetSetVal);

  Timeout = MrcCall->MrcGetCpuTime () + MRC_WAIT_TIMEOUT; // 10 seconds timeout
  Flag = 0;
  for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
    if (MrcControllerExist (MrcData, Controller)) {
      do {
        Offset = OFFSET_CALC_CH (MC0_STALL_DRAIN_REG, MC1_STALL_DRAIN_REG, Controller);
        StallDrain.Data = MrcReadCR (MrcData, Offset);
        // If 1 we didn't get to the state we want
        Flag = (StallDrain.Bits.sr_state != SelfRef);
      } while (Flag && (MrcCall->MrcGetCpuTime ()  < Timeout));
    }
  }

  if (Flag) {
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "MC failed to %s Self-Refresh\n", (SelfRef == MRC_SR_ENTER) ? "enter" : "exit");
    Status = mrcFail;
  }

  return Status;
}

/**
  This function defines the Command pattern spread per DRAM technology.

  @param[in]  MrcData - Pointer to MRC global data

  @retval UINT8 - Number of bits in the set of the Victim Aggressor pattern.
**/
UINT8
MrcCommandSpread (
  IN  MrcParameters *const  MrcData
  )
{
  UINT8 Spread;

  switch (MrcData->Outputs.DdrType) {
    case MRC_DDR_TYPE_LPDDR5:
      Spread = 7;
      break;

    case MRC_DDR_TYPE_LPDDR4:
      Spread = 6;
      break;

    case MRC_DDR_TYPE_DDR4:
    case MRC_DDR_TYPE_DDR5:
    default:
      Spread = 7;
      break;
  }

  return Spread;
}

/**
  Fill the test engine with the given fixed pattern.
  In Enhanced Channel Mode only entry [0] of each chunk will be used (low 32 bit).

  @param[in] MrcData  - Include all MRC global data.
  @param[in] Pattern  - Array of chunks (64 bit), broken into two 32-bit entries (second dimension).
                        Length (first dimension) can be up to NUM_DPAT_EXTBUF
  @retval Nothing
**/
void
WriteFixedPattern (
  IN MrcParameters *const  MrcData,
  IN UINT32                Pattern[][2]
  )
{
  UINT32    *ExtBufPtr;
  MrcOutput *Outputs;
  UINT32    Offset;
  UINT32    IpCh;
  UINT32    Channel;
  UINT32    Controller;
  UINT32    Index;
  UINT8     McChMask;
  BOOLEAN   Lpddr;

  Outputs   = &MrcData->Outputs;
  McChMask  = Outputs->McChBitMask;
  Lpddr     = Outputs->Lpddr;

  for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
    for (Channel = 0; Channel < Outputs->MaxChannels; Channel++) {
      if ((MC_CH_MASK_CHECK (McChMask, Controller, Channel, Outputs->MaxChannels) == 0) ||
          (IS_MC_SUB_CH (Lpddr, Channel))) {
        continue;
      }
      IpCh = LP_IP_CH (Lpddr, Channel);
      // Program ExtBuf
      Offset = OFFSET_CALC_MC_CH (
                MC0_BUF0_CPGC_DPAT_EXTBUF_0_REG,
                MC1_BUF0_CR_CPGC_DPAT_EXTBUF_0_REG, Controller,
                MC0_BUF1_CR_CPGC_DPAT_EXTBUF_0_REG, IpCh);
      for (Index = 0; Index < CPGC_20_NUM_DPAT_EXTBUF; Index++) {
        ExtBufPtr = &Pattern[Index / 2][Index % 2];
        MrcWriteCR (MrcData, Offset, *ExtBufPtr);
        //Adjust Offset for next EXTBUF
        Offset += (MC0_BUF0_CPGC_DPAT_EXTBUF_1_REG - MC0_BUF0_CPGC_DPAT_EXTBUF_0_REG);
      }
    }
  }
  return;
}

/**
  Fill the test engine with the given fixed pattern.
  In Enhanced Channel Mode only entry [0] of each chunk will be used (low 32 bit).

  @param[in] MrcData  - Include all MRC global data.
  @param[in] Pattern  - Array of chunks (64 bit), broken into two 32-bit entries (second dimension).
  Length (first dimension) can be up to NUM_DPAT_EXTBUF
  @retval Nothing
**/
void
WriteMATSFixedPattern (
  IN MrcParameters *const  MrcData,
  IN UINT32                Pattern[]
  )
{
  UINT32    *ExtBufPtr;
  MrcOutput *Outputs;
  UINT32    Offset;
  UINT32    IpCh;
  UINT32    Channel;
  UINT32    Controller;
  UINT32    Index;
  UINT8     McChMask;
  BOOLEAN   Lpddr;

  Outputs   = &MrcData->Outputs;
  McChMask  = Outputs->McChBitMask;
  Lpddr     = Outputs->Lpddr;

  for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
    for (Channel = 0; Channel < Outputs->MaxChannels; Channel++) {
      if ((MC_CH_MASK_CHECK (McChMask, Controller, Channel, Outputs->MaxChannels) == 0) ||
        (IS_MC_SUB_CH (Lpddr, Channel))) {
        continue;
      }
      IpCh = LP_IP_CH (Lpddr, Channel);
      // Program ExtBuf
      Offset = OFFSET_CALC_MC_CH (
        MC0_BUF0_CPGC_DPAT_EXTBUF_0_REG,
        MC1_BUF0_CR_CPGC_DPAT_EXTBUF_0_REG, Controller,
        MC0_BUF1_CR_CPGC_DPAT_EXTBUF_0_REG, IpCh);
      for (Index = 0; Index < CPGC_20_NUM_DPAT_EXTBUF; Index++) {
        ExtBufPtr = &Pattern[Index % 8];
        MrcWriteCR (MrcData, Offset, *ExtBufPtr);
        //Adjust Offset for next EXTBUF
        Offset += (MC0_BUF0_CPGC_DPAT_EXTBUF_1_REG - MC0_BUF0_CPGC_DPAT_EXTBUF_0_REG);
      }
    }
  }
  return;
}

/**
  This routine programs the CPGC pattern generator using a VA pattern
  This routine performs the following steps:
  Step 0: Iterate through all CPGC DPAT External Buffers
  Step 1: Use the vmask and amask parameters to determine victim and agressor bytes
  Step 2: Create a compressed vector for a given 32 byte cacheline
          Victim bytes have a value of AA, and Aggressor bytes have a value of CC
  Step 3: Program the values into the Pattern Generator

  @param[in] MrcData     - Include all MRC global data.
  @param[in] McChBitMask - Memory Controller Channel Bit mask for which test should be setup for.
  @param[in] vmask       - victim mask.  1 indicates this bit should use LFSR0. Mask length is 16 bits (CPGC_20_NUM_DPAT_EXTBUF)
  @param[in] amask       - aggressor mask. 0/1 indicates this bit should use LFSR1/2. Mask length is 16 bits (CPGC_20_NUM_DPAT_EXTBUF)

  @retval Nothing
**/
void
WriteVAPattern (
  IN MrcParameters *const MrcData,
  IN UINT8                McChBitMask,
  IN UINT32               vmask,
  IN UINT32               amask
  )
{
  static const UINT32 VaPatVector[4] = { 0xAAAAAAAA, 0xC0C0C0C0, 0xCCCCCCCC, 0xF0F0F0F0 };
  MrcOutput          *Outputs;
  const UINT32       *ExtBufPtr;
  UINT32             Bit;
  UINT32             Vic;
  UINT32             Agg2;
  UINT32             BitMask;
  UINT32             Index;
  UINT32             Offset;
  UINT32             IpCh;
  UINT32             Channel;
  UINT32             Controller;
  BOOLEAN            Lpddr;

  Outputs = &MrcData->Outputs;
  Lpddr = Outputs->Lpddr;

  // AA = Victim (LFSR0), 0C = Agg1(LFSR1), 0xCC = Agg2 (LFSR2), 0xF0 = Agg3 (LFSR3)
  for (Bit = 0; Bit < CPGC_20_NUM_DPAT_EXTBUF; Bit++) {
    BitMask = MRC_BIT0 << Bit;
    Vic = (vmask & BitMask);
    Agg2 = (amask & BitMask);

    // Select the vector for this lane
    if (Vic && Agg2) {
      Index = 1;
    } else if (Vic && !Agg2) {
      Index = 0;
    } else if (!Vic && !Agg2) {
      Index = 3;
    } else {
      Index = 2;
    }

    ExtBufPtr = &VaPatVector[Index];

    for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
      for (Channel = 0; Channel < Outputs->MaxChannels; Channel++) {
        // Program ExtBuf
        if ((MC_CH_MASK_CHECK (McChBitMask, Controller, Channel, Outputs->MaxChannels) == 0) ||
          (IS_MC_SUB_CH (Lpddr, Channel))) {
          continue;
        }
        IpCh = LP_IP_CH (Outputs->Lpddr, Channel);
        Offset = OFFSET_CALC3 (
          MC0_BUF0_CPGC_DPAT_EXTBUF_0_REG,
          MC1_BUF0_CR_CPGC_DPAT_EXTBUF_0_REG, Controller,
          MC0_BUF1_CR_CPGC_DPAT_EXTBUF_0_REG, IpCh,
          MC0_BUF0_CPGC_DPAT_EXTBUF_1_REG, Bit);
        MrcWriteCR (MrcData, Offset, *ExtBufPtr);
      }
    }
  }
}

/**
  Write VA pattern in CADB buffer.

  Note1: CKE, ODT and CS are not used in functional mode and are ignored.

  @param[in] MrcData      - Include all MRC global data.
  @param[in] McChBitMask  - Memory Controller Channel Bit mask for which test should be setup for.
  @param[in] VictimSpread - Separation of the Victim Bit.
  @param[in] VictimBit    - The Bit to be the Victim.

  @retval mrcSuccess if successful, otherwise returns an error status.
**/
MrcStatus
SetupCadbVaPat (
  IN MrcParameters *const MrcData,
  IN UINT8                McChBitMask,
  IN const UINT8          VictimSpread,
  IN const UINT8          VictimBit
  )
{
  const MRC_FUNCTION  *MrcCall;
  MrcOutput           *Outputs;
  UINT8               Row;
  UINT8               bit;
  UINT8               CadbBits;
  UINT8               lfsr0;
  UINT8               lfsr1;
  UINT8               Lfsr;
  UINT8               Fine;
  UINT8               Cke[2];
  UINT8               Cs[2];
  UINT8               Odt;
  MRC_CA_MAP_TYPE     CadbPat;
  UINT64_STRUCT       CadbPatEncoded[CADB_20_MAX_CHUNKS];
  UINT32              CaPattern[CADB_20_MAX_CHUNKS];
  BOOLEAN             Ddr4;
  BOOLEAN             Ddr5;
  BOOLEAN             Lpddr4;
  BOOLEAN             IsVictim;
  Outputs = &MrcData->Outputs;
  Ddr4    = (Outputs->DdrType == MRC_DDR_TYPE_DDR4);
  Ddr5    = (Outputs->DdrType == MRC_DDR_TYPE_DDR5);
  Lpddr4  = (Outputs->DdrType == MRC_DDR_TYPE_LPDDR4);
/**
  we need to excercise all bits as victims on each CCC group, only one must be selected at a time per CCC group
 for DDR4 CCC groups are mapped as follows:

  VB  |---------------CCC0/4-------------------|--------------CCC1/5-------------|-------------CCC2/6-------------|------------CCC3/7-----------|
       ma3   ma1   ma4   ma7   ma8   ma6   ma5   ma13   ma16   ma14   ma15   ba1   ma9   ma12   actn   ma11   bg1   bg0   ba0   ma10   ma0   ma2
  0     V     a     a     a     a     a     a     V      a      a      a      a     V     a       a     a      a     a     V     a      a     a
  1     a     V     a     a     a     a     a     a      V      a      a      a     a     V       a     a      a     a     a     V      a     a
  2     a     a     V     a     a     a     a     a      a      V      a      a     a     a       V     a      a     a     a     a      V     a
  3     a     a     a     V     a     a     a     a      a      a      V      a     a     a       a     V      a     a     a     a      a     V
  4     a     a     a     a     V     a     a     a      a      a      a      V     a     a       a     a      V     a     a     a      a     a
  5     a     a     a     a     a     V     a     a      a      a      a      a     a     a       a     a      a     V     a     a      a     a
  6     a     a     a     a     a     a     V     a      a      a      a      a     a     a       a     a      a     a     a     a      a     a
  VB = Victim Bit Offset
  V  = Victim
  a  = agressor

  the CA mapping to CADB buffer bits must look like this for each of the Victim Bits
  VB
      actn   bg1   bg0   ba1   ba0         ma16   ma15   ma14   ma13   ma12   ma11   ma10   ma9   ma8   ma7   ma6   ma5   ma4   ma3   ma2   ma1   ma0
  0    a      a     a     a     V    a      a      a      a      V      a      a      a     V     a     a     a     a     a     V     a     a     a   [0x00042208]
  1    a      a     a     a     a    a      V      a      a      a      V      a      V     a     a     a     a     a     a     a     a     V     a   [0x00011402]
  2    V      a     a     a     a    a      a      a      V      a      a      a      a     a     a     a     a     a     V     a     a     a     V   [0x00404011]
  3    a      a     a     a     a    a      a      V      a      a      a      V      a     a     a     V     a     a     a     a     V     a     a   [0x00008884]
  4    a      V     a     V     a    a      a      a      a      a      a      a      a     a     V     a     a     a     a     a     a     a     a   [0x00280100]
  5    a      a     V     a     a    a      a      a      a      a      a      a      a     a     a     a     V     a     a     a     a     a     a   [0x00100040]
  6    a      a     a     a     a    a      a      a      a      a      a      a      a     a     a     a     a     V     a     a     a     a     a   [0x00000020]
   CA[22]   [21]   [20]  [19]  [18] [17]   [16]  [15]   [14]   [13]   [12]   [11]   [10]   [9]   [8]   [7]   [6]   [5]   [4]   [3]   [2]   [1]   [0]

  for DDR5 CA groups are mapped as follows:

   VB  CA[13]   [12]     [11]   [10]     [9]     [8]     [7]     [6]     [5]     [4]     [3]     [2]     [1]     [0]
   0     V       a       a       a       a       a       a       V       a       a       a       a       a       a       [0x0002040]
   1     a       V       a       a       a       a       a       a       V       a       a       a       a       a       [0x0001020]
   2     a       a       V       a       a       a       a       a       a       V       a       a       a       a       [0x0000810]
   3     a       a       a       V       a       a       a       a       a       a       V       a       a       a       [0x0000408]
   4     a       a       a       a       V       a       a       a       a       a       a       V       a       a       [0x0000204]
   5     a       a       a       a       a       V       a       a       a       a       a       a       V       a       [0x0000102]
   6     a       a       a       a       a       a       V       a       a       a       a       a       a       V       [0x0000081]
**/
  UINT8   CccVictimBitMapMax;
  static const UINT32  Ddr4CccVictimBitMap [] =  {0x00042208, // Victim Bit 0
                                                  0x00011402, // Victim Bit 1
                                                  0x00404011, // Victim Bit 2
                                                  0x00008884, // Victim Bit 3
                                                  0x00280100, // Victim Bit 4
                                                  0x00100040, // Victim Bit 5
                                                  0x00000020  // Victim Bit 6
                                                  };
  static const UINT32  Ddr5CccVictimBitMap [] =  {0x0002040, // Victim Bit 0
                                                  0x0001020, // Victim Bit 1
                                                  0x0000810, // Victim Bit 2
                                                  0x0000408, // Victim Bit 3
                                                  0x0000204, // Victim Bit 4
                                                  0x0000102, // Victim Bit 5
                                                  0x0000081  // Victim Bit 6
                                                 };

  CccVictimBitMapMax = sizeof(Ddr4CccVictimBitMap) / sizeof(Ddr4CccVictimBitMap[0]); // Ddr5 has the same maximum for Victim bit rotation
  MrcCall     = MrcData->Inputs.Call.Func;
  Odt         = 0;
  MrcCall->MrcSetMem ((UINT8 *) Cke, sizeof (Cke), 0);
  MrcCall->MrcSetMem ((UINT8 *) Cs, sizeof (Cs), 0);
  MrcCall->MrcSetMem ((UINT8 *) CaPattern, sizeof (CaPattern), 0);
  MrcCall->MrcSetMem ((UINT8 *) CadbPatEncoded, sizeof (CadbPatEncoded), 0);

  if (Ddr4) {
    CadbBits = 23;  // CA[22:0]
  } else if (Outputs->Lpddr) {
    // LPDDR4  or LPDDR5
    // ca[0:5] -  ca[0:6]
    CadbBits = (Lpddr4) ? 6: 7;
  } else {
    // Ddr5
    CadbBits = 14; // CA[13:0]
  }

  for (Row = 0; Row < CADB_20_MAX_CHUNKS; Row++) {
    lfsr0 = (Row & 0x1);         // 0, 1, 0, 1, 0, 1, 0, 1 for r = 0,1, ..., 7
    lfsr1 = ((Row >> 1) & 0x1);  // 0, 0, 1, 1, 0, 0, 1, 1 for r = 0,1, ..., 7
    //
    // Assign Victim/Aggressor Bits
    //
    for (bit = 0; bit < CadbBits; bit++) {

      // For DDR4 & DDR5 use victim bit map
      if ((Ddr4) && (VictimBit < CccVictimBitMapMax)) {
        IsVictim = 1 & (Ddr4CccVictimBitMap[VictimBit] >> bit);
      } else if ((Ddr5) && VictimBit < CccVictimBitMapMax) {
        IsVictim = 1 & (Ddr5CccVictimBitMap[VictimBit] >> bit);
      } else {
        Fine = bit % VictimSpread;
        IsVictim = (Fine == VictimBit);
      }

      if (IsVictim) {
        Lfsr = lfsr0;
      } else {
        Lfsr = lfsr1;
      }
      CaPattern[Row] |= Lfsr << bit;
    }
  }

  for (Row = 0; Row < CADB_20_MAX_CHUNKS; Row++) {
    CadbPat.Data32 = CaPattern[Row];
    // Translate generic pattern to CADB definition
    CpgcConvertDdrToCadb (MrcData, &CadbPat, Cke, Cs, Odt, &CadbPatEncoded[Row]);
  }

  // Write the CADB Patterns
  MrcSetCadbPgPattern (MrcData, McChBitMask, CadbPatEncoded, CADB_20_MAX_CHUNKS, 0);

  return mrcSuccess;
}

/**
  Programs all the key registers to define a CPGC test as per input mask and Outputs->McChBitMask.
  McChBitMask is initialized in MrcPretraining based on population, and used throughout internal CPGC structure.
  Modify McChBitMask to specify which MC/CH to program

  @param[in] MrcData     - Include all MRC global data.
  @param[in] McChBitMask - Memory Controller Channel Bit mask for which test should be setup for.
  @param[in] CmdPat      - [0: PatWrRd (Standard Write/Read Loopback),
                            1: PatWr (Write Only),
                            2: PatRd (Read Only),
                            3: PatRdWrTA (ReadWrite Turnarounds),
                            4: PatWrRdTA (WriteRead Turnarounds),
                            5: PatODTTA (ODT Turnaround]
  @param[in] NumCL       - Number of Cache lines
  @param[in] LC          - Loop Count exponent
  @param[in] CPGCAddress - Structure that stores address related settings
  @param[in] SOE         - [0: Never Stop, 1: Stop on Any Lane, 2: Stop on All Byte, 3: Stop on All Lane]
  @param[in] PatCtlPtr   - Structure that stores start, Stop, IncRate and Dqpat for pattern.
  @param[in] EnCADB      - Enable test to write random deselect packages on bus to create xtalk/isi
  @param[in] EnCKE       - Enable CKE power down by adding 64
  @param[in] SubSeqWait  - # of Dclks to stall at the end of a sub-sequence

  @retval Nothing
**/
void
SetupIOTest (
  IN MrcParameters *const         MrcData,
  IN const UINT8                  McChBitMask,
  IN const UINT8                  CmdPat,
  IN const UINT32                 NumCL,
  IN const UINT8                  LC,
  IN const Cpgc20Address *const   CPGCAddress,
  IN const MRC_TEST_STOP_TYPE     SOE,
  IN MRC_PATTERN_CTL *const       PatCtlPtr,
  IN const UINT8                  EnCADB,
  IN const UINT8                  EnCKE,
  IN UINT16                       SubSeqWait
  )
{
  static const UINT32 CadbLfsrSeeds[MRC_NUM_MUX_SEEDS]  = {0x096EA1, 0xCABEEF, 0x53DEAD};
  static const UINT32 DataLfsrSeeds[MRC_NUM_MUX_SEEDS]  = {0xA10CA1, 0xEF0D08, 0xAD0A1E};
  static const UINT32 DataStaticSeeds[MRC_NUM_MUX_SEEDS] = {0xAAAAAAAA, 0xCCCCCCCC, 0xF0F0F0F0};
  static const MRC_PG_LFSR_TYPE  CadbLfsrPoly[CADB_20_NUM_DSEL_UNISEQ] = {MrcLfsr8, MrcLfsr8, MrcLfsr8};
  static const MRC_PG_UNISEQ_TYPE  UniseqMode[CADB_20_NUM_DSEL_UNISEQ] = {MrcPgMuxLfsr, MrcPgMuxLfsr, MrcPgMuxLfsr};
  MRC_PG_LFSR_TYPE  LfsrPoly;
  MrcOutput         *Outputs;
  MrcControllerOut  *ControllerOut;
  MrcChannelOut     *ChannelOut;
  MrcDebug          *Debug;
  MrcIntOutput      *IntOutputs;
  MrcZqType         ZqType;
  UINT32            Offset;
  UINT32            ChunkMask;
  UINT16            Wait;
  UINT8             ShiftLeftBy;
  UINT32            LoopCount;
  UINT32            Burst;
  UINT8             Index;
  UINT8             Controller;
  UINT8             Channel;
  UINT8             CurMcChMask;
  UINT8             LaneRotateRate;
  UINT8             ValidMask;
  BOOLEAN           Lpddr4;
  BOOLEAN           Ddr4;
  BOOLEAN           Ddr5;
  MC0_CH0_CR_CADB_CFG_STRUCT                            CadbConfig;
  MC0_CH0_CR_PM_PDWN_CONFIG_STRUCT                      PmPdwnConfig;
  MC0_REQ0_CR_CPGC2_COMMAND_INSTRUCTION_0_STRUCT        Cpgc2CmdInstruction[6]; // 6 instructions defined in this function
  MC0_REQ0_CR_CPGC2_ALGORITHM_INSTRUCTION_0_STRUCT      Cpgc2AlgoInstructionWr;
  MC0_REQ0_CR_CPGC2_ALGORITHM_INSTRUCTION_0_STRUCT      Cpgc2AlgoInstructionRd;
  MC0_REQ0_CR_CPGC2_ALGORITHM_INSTRUCTION_0_STRUCT      Cpgc2AlgoInstructionRdWr;
  MC0_REQ0_CR_CPGC2_ALGORITHM_INSTRUCTION_0_STRUCT      Cpgc2AlgoInstructionWrRd;
  MC0_REQ0_CR_CPGC2_ALGORITHM_INSTRUCTION_0_STRUCT      AlgoInstruct[CPGC20_MAX_ALGORITHM_INSTRUCTION];
  MC0_REQ0_CR_CPGC2_ALGORITHM_INSTRUCTION_CTRL_0_STRUCT AlgoInstructControl;
  MC0_REQ0_CR_CPGC2_ALGORITHM_WAIT_EVENT_CONTROL_STRUCT AlgoWaitEventControl;
  MC0_REQ0_CR_CPGC2_DATA_INSTRUCTION_0_STRUCT           DataInstruct[CPGC20_MAX_DATA_INSTRUCTION];

  Outputs       = &MrcData->Outputs;
  Debug         = &Outputs->Debug;
  IntOutputs    = (MrcIntOutput *) MrcData->IntOutputs.Internal;
  Lpddr4        = (Outputs->DdrType == MRC_DDR_TYPE_LPDDR4);
  Ddr4          = (Outputs->DdrType == MRC_DDR_TYPE_DDR4);
  Ddr5          = (Outputs->DdrType == MRC_DDR_TYPE_DDR5);
  Burst         = MAX (NumCL, 1);
  ShiftLeftBy   = MIN (LC, 31);
  CurMcChMask   = 0;

  if (MrcData->Inputs.LoopBackTest) {
    Burst = 1;        // Cannot do more than a single cacheline per test in Loopback mode
  }
  // LC defines the number of Cacheline Transactions for the test: 2^LC.
  LoopCount = (1 << ShiftLeftBy);

  // Check for the cases where this MUST be 1: When we manually walk through SUBSEQ ODT and TAWR
  if ((CmdPat == PatWrRdTA) || (CmdPat == PatODTTA)) {
    //CPGC2.1 change needed? to increase Loopcount for PatWrRdTA
    LoopCount = 1;
  }
  for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
    for (Channel = 0; Channel < Outputs->MaxChannels; Channel++) {
      CurMcChMask |= ((1 << Channel) << (Controller * Outputs->MaxChannels));
      if (EnCKE && ((CurMcChMask & McChBitMask) == 0)) {
        // @todo: Need to check that PDWN is programmed already.
        Offset = OFFSET_CALC_MC_CH (MC0_CH0_CR_PM_PDWN_CONFIG_REG, MC1_CH0_CR_PM_PDWN_CONFIG_REG, Controller, MC0_CH1_CR_PM_PDWN_CONFIG_REG, Channel);
        PmPdwnConfig.Data = MrcReadCR (MrcData, Offset);
        Wait = (UINT16) (PmPdwnConfig.Bits.PDWN_idle_counter_subch0 + 16); // Adding extra DCKs, 16,  to make sure we make it to power down.
        if (Wait > SubSeqWait) {
          SubSeqWait = Wait;
        }
      }
    }
  }
  //###########################################################
  // Program CADB Pattern Controls.  PGs are selected for CADB.
  //###########################################################

  // Pattern is programmed in RunIoTest
  CadbConfig.Data = 0;
  CadbConfig.Bits.CADB_MODE = EnCADB ? Cadb20ModeTriggerDeselect : 0;
  CadbConfig.Bits.LANE_DESELECT_EN = 6;                                 // Drive CADB deselects on CA/Parity, but not on CS
  CadbConfig.Bits.CMD_DESELECT_START = 0xF;                             // Start driving deselects on RD/WR/ACT/PRE; keep CMD_DESELECT_STOP = 0
  CadbConfig.Bits.INITIAL_DSEL_EN = 1;
  CadbConfig.Bits.INITIAL_DSEL_SSEQ_EN = 1;
  CadbConfig.Bits.CADB_TO_CPGC_BIND = EnCADB ? 1 : 0;
  CurMcChMask = 0;
  // Per channel settings
  for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
    ControllerOut = &Outputs->Controller[Controller];
    for (Channel = 0; Channel < Outputs->MaxChannels; Channel++) {
      if (MrcChannelExist (MrcData, Controller, Channel)) {
        CurMcChMask |= ((1 << Channel) << (Controller * Outputs->MaxChannels));
        if (((CurMcChMask & McChBitMask) == 0)) {
          continue;
        }
        ChannelOut = &ControllerOut->Channel[Channel];
        if (Ddr4 || Ddr5) {
          if ((ChannelOut->Timing[MrcData->Inputs.MemoryProfile].NMode == 2) || Outputs->Gear2) {
            CadbConfig.Bits.CADB_DSEL_THROTTLE_MODE = 2;
            CadbConfig.Bits.CADB_SEL_THROTTLE_MODE  = 2;
          }
        }
      } // MrcChannelExist
    } // Channel
  } // Controller
  Cadb20ConfigRegWrite (MrcData, CadbConfig);

  if (EnCADB) {
    // Setup CADB in terms of LFSR selects, LFSR seeds, LMN constants and overall control
    Cadb20UniseqCfg (MrcData, UniseqMode, CadbLfsrPoly);
    // Write the LFSR seeds
    MrcInitCadbPgMux (MrcData, CadbLfsrSeeds, 0, MRC_NUM_MUX_SEEDS);
    Cpgc20AddressSetupCmdStress (MrcData);
  }

  //###########################################################
  // Program Data Pattern Controls.  PGs are selected for Data
  //###########################################################

  Cpgc20BasicMemTest (MrcData, Burst, LoopCount, 1);
  // @todo: Add enable call for ECC PG source, and ECC PG enable.

  // Write the LFSR seeds
  if (PatCtlPtr->DQPat == StaticPattern) {
    MrcInitCpgcPgMux (MrcData, DataStaticSeeds, 0, MRC_NUM_MUX_SEEDS);
    LaneRotateRate = 0;
  } else if (PatCtlPtr->DQPat == GaloisMprVa) {
    MrcInitCpgcPgMux (MrcData, DataGaloisSeeds, 0, MRC_NUM_MUX_SEEDS);
    LaneRotateRate = 0;
  } else {
    MrcInitCpgcPgMux (MrcData, DataLfsrSeeds, 0, MRC_NUM_MUX_SEEDS);
    LaneRotateRate = 2;
  }

  // Set LFSR Polynomial
  if (PatCtlPtr->DQPat == GaloisMprVa) {
    LfsrPoly = 0; // When in GaloisMprVa mode, LFSR Polynomial won't be used.
  } else {
    LfsrPoly = MrcLfsr16;
  }

  // Setup CPGC in terms of LFSR selects, LFSR seeds, LMN constants and overall control
  MrcInitUnisequencer (MrcData, LfsrPoly, PatCtlPtr, 0, MRC_NUM_MUX_SEEDS);
  //@todo should LSFR Poly match previous code or documentation?
  Cpgc20LfsrCfg (MrcData, CmdPat, LaneRotateRate);

        // Setup LMN phases
        // @todo <ICL> convert to CPGC 2.0
//      ReutPgPatMuxLmn.Data                         = 0;
//      ReutPgPatMuxLmn.Bits.N_counter               = 10;
//      ReutPgPatMuxLmn.Bits.M_counter               = 1;
//      ReutPgPatMuxLmn.Bits.L_counter               = 1;
//      ReutPgPatMuxLmn.Bits.Enable_Sweep_Frequency  = 1;
//      Offset = REUT_PG_PAT_CL_MUX_LMN_REG;
//      MrcWriteCR (MrcData, Offset, ReutPgPatMuxLmn.Data);

        //###########################################################
        // Program Sequence
        //###########################################################


//     @todo <ICL> convert to CPGC 2.0
//      Offset = REUT_CH_SEQ_CTL_0_REG +
//        ((REUT_CH_SEQ_CTL_1_REG - REUT_CH_SEQ_CTL_0_REG) * Channel);
//      MrcWriteCR (MrcData, Offset, REUT_CH_SEQ_CTL_0_Local_Clear_Errors_MSK);

  //###########################################################
  // Program Command Instuctions @todo set this up in GetSet cached and then flushed
  //###########################################################
  // Write CMD [0]
   Cpgc2CmdInstruction[0].Data          = 0;
   Cpgc2CmdInstruction[0].Bits.WriteCmd = 1;
   Cpgc2CmdInstruction[0].Bits.Address_Decode_or_PRBS_En = EnCADB;
   Cpgc2CmdInstruction[0].Bits.Last     = 1;

   // Read CMD [1]
   Cpgc2CmdInstruction[1].Data          = 0;
   Cpgc2CmdInstruction[1].Bits.Address_Decode_or_PRBS_En = EnCADB;
   Cpgc2CmdInstruction[1].Bits.Last     = 1;

   // Read-Write CMD [2-3]
   Cpgc2CmdInstruction[2].Data          = 0;
   Cpgc2CmdInstruction[3].Data          = Cpgc2CmdInstruction[0].Data;

  // Write-Read CMD [4-5]
  Cpgc2CmdInstruction[4].Data          = 0;
  Cpgc2CmdInstruction[4].Bits.WriteCmd = 1;
  Cpgc2CmdInstruction[5].Data          = Cpgc2CmdInstruction[1].Data;

  Cpgc20CmdInstructWrite (MrcData, &Cpgc2CmdInstruction[0], ARRAY_COUNT (Cpgc2CmdInstruction));
  //###########################################################
  // Program Algorithm Instructions (pointing to the Command Instructions above)
  //###########################################################
  // Write Algo
  Cpgc2AlgoInstructionWr.Data = 0;
  Cpgc2AlgoInstructionWr.Bits.Command_Start_Pointer = 0;

  // Read Algo
  Cpgc2AlgoInstructionRd.Data = 0;
  Cpgc2AlgoInstructionRd.Bits.Command_Start_Pointer = 1;

  // Read-Write Algo
  Cpgc2AlgoInstructionRdWr.Data = 0;
  Cpgc2AlgoInstructionRdWr.Bits.Command_Start_Pointer = 2;

  // Write-Read Algo
  Cpgc2AlgoInstructionWrRd.Data = 0;
  Cpgc2AlgoInstructionWrRd.Bits.Command_Start_Pointer = 4;

  //###########################################################
  // Program Sequence of Algorithm Instructions based on CmdPat
  //###########################################################
  switch (CmdPat) {
    case PatWrRdTA:
      AlgoInstruct[0].Data = Cpgc2AlgoInstructionWr.Data;        // Write CMD All data
      for (Index = 1; Index <= 6; Index++) {
        AlgoInstruct[Index].Data = Cpgc2AlgoInstructionRdWr.Data;// Read-Write CMD
      }
      AlgoInstruct[7].Data = Cpgc2AlgoInstructionRd.Data;        // Read CMD
      AlgoInstruct[7].Bits.Last = 1;
      ValidMask = 0xFF;
      break;

    case PatRdWrTA:
      AlgoInstruct[0].Data = Cpgc2AlgoInstructionWrRd.Data;      // Write-Read CMD
      AlgoInstruct[0].Bits.Last = 1;
      ValidMask = 0x01;
      break;

    case PatRdWr:
      AlgoInstruct[0].Data = Cpgc2AlgoInstructionRd.Data;
      AlgoInstruct[1].Data = Cpgc2AlgoInstructionWr.Data;
      AlgoInstruct[1].Bits.Last = 1;
      ValidMask = 0x3;
      break;

    case PatODTTA:
      AlgoInstruct[0].Data = Cpgc2AlgoInstructionWr.Data;        // Write CMD
      AlgoInstruct[1].Data = Cpgc2AlgoInstructionRdWr.Data;      // Read-Write CMD
      AlgoInstruct[2].Data = Cpgc2AlgoInstructionRd.Data;        // Read CMD
      AlgoInstruct[3].Data = Cpgc2AlgoInstructionWrRd.Data;      // Write-Read CMD
      AlgoInstruct[3].Bits.Last = 1;
      ValidMask = 0x0F;
      break;

    case PatWrRd:
      AlgoInstruct[0].Data = Cpgc2AlgoInstructionWr.Data;        // Write CMD
      AlgoInstruct[1].Data = Cpgc2AlgoInstructionRd.Data;        // Read CMD
      AlgoInstruct[1].Bits.Last = 1;
      ValidMask = 0x03;
      break;

    case PatWr:
    case PatWrScrub:
      AlgoInstruct[0].Data = Cpgc2AlgoInstructionWr.Data;        // Write CMD
      AlgoInstruct[0].Bits.Last = 1;
      ValidMask = 0x01;
      break;

    case PatRd:
      AlgoInstruct[0].Data = Cpgc2AlgoInstructionRd.Data;        // Read CMD
      AlgoInstruct[0].Bits.Last = 1;
      ValidMask = 0x01;
      break;

    case PatRdEndless:
      // Used in endless tests like in HW-based ReadMPR
      for (Index = 0; Index < CPGC20_MAX_ALGORITHM_INSTRUCTION; Index++) {
        AlgoInstruct[Index].Data = Cpgc2AlgoInstructionRd.Data;  // Read CMD, and no Last = endless test, no data rotation
      }
      ValidMask = 0xFF;
      break;

    case PatWrRdEndless:
      for (Index = 0; Index < CPGC20_MAX_ALGORITHM_INSTRUCTION; Index += 2) {
        AlgoInstruct[Index].Data = Cpgc2AlgoInstructionWr.Data;        // Write CMD
        AlgoInstruct[Index + 1].Data = Cpgc2AlgoInstructionRd.Data;    // Read CMD
      }
      ValidMask = 0xFF;
      break;

    case PatWrEndless:
      // Used in endless tests like in VccDll Post Training refinement
      for (Index = 0; Index < CPGC20_MAX_ALGORITHM_INSTRUCTION; Index++) {
        AlgoInstruct[Index].Data = Cpgc2AlgoInstructionWr.Data;  // Wr CMD, and no Last = endless test, no data rotation
      }
      ValidMask = 0xFF;
      break;

    default:
      MRC_DEBUG_MSG (Debug, MSG_LEVEL_ERROR, "SetupIOTest: Unknown value for Pattern\n");
      ValidMask = 0x00;
      break;
  }

  // Program Write Data Buffer Related Entries.
  // Assumes the PG has already been selected above for Data PG's.
  MrcProgramDataPatternRotation (MrcData, PatCtlPtr);

  AlgoInstructControl.Data = 0;
  AlgoInstructControl.Bits.Deselect_On = EnCADB;

  AlgoWaitEventControl.Data = 0;
  if (SubSeqWait != 0) {
    AlgoInstructControl.Bits.Wait_Count_Start = 1;
    AlgoInstructControl.Bits.Wait_Count_Clear = 1;
    AlgoWaitEventControl.Bits.Wait_Clock_Frequency = CPGC20_NATIVE_DUNIT_FREQ;
    AlgoWaitEventControl.Bits.Wait_Time = SubSeqWait;
  }
  Cpgc20AlgoInstructWrite (MrcData, &AlgoInstruct[0], AlgoInstructControl, AlgoWaitEventControl, ValidMask);

  DataInstruct[0].Data = 0;
  DataInstruct[0].Bits.Last = 1;
  DataInstruct[1].Data = DataInstruct[0].Data;
  DataInstruct[2].Data = DataInstruct[0].Data;
  DataInstruct[3].Data = DataInstruct[0].Data;

  Cpgc20DataInstructWrite (MrcData, &DataInstruct[0], 0x1);

  //###########################################################
  // Program Cpgc Address
  //###########################################################
  Cpgc20AddressSetup (
    MrcData,
    CPGCAddress->AddrIncOrder,
    CPGCAddress->AddrDirection,
    CPGCAddress->LastValidAddr,
    CPGCAddress->RowStart,
    CPGCAddress->ColStart,
    CPGCAddress->RowSizeBits,
    CPGCAddress->ColSizeBits,
    CPGCAddress->BankSize,
    EnCADB
    );

  //###########################################################
  // Program Error Checking
  //###########################################################

  // Enable selective_error_enable_chunk and selective_error_enable_cacheline, mask later
  // the bits we don't want to check.
  // Burst Length: DDR4: 8, DDR5: 16, LP4/LP5: 32
  ChunkMask = (Ddr4) ? 0xFF : ((Ddr5) ? 0x0000FFFF : 0xFFFFFFFF);
  MrcSetupTestErrCtl (MrcData, SOE, 1);
  MrcSetChunkAndClErrMsk (MrcData, 0xFF, ChunkMask);
  // Store the test mode in the internal cache for other functions.
  IntOutputs->TestStopMode = SOE;
  // Setup Data and ECC Errors
  MrcSetDataAndEccErrMsk (MrcData, 0xFFFFFFFFFFFFFFFFULL, 0xFF);

  // Always do a ZQ Short before the beginning of a test
  if (Lpddr4 || Ddr5) {
    ZqType  = MRC_ZQ_CAL;
  } else if (Ddr4) {
    ZqType  = MRC_ZQ_LONG;
  } else {
    ZqType  = MRC_ZQ_SHORT;
  }

  if (!IntOutputs->SkipZq) {
    MrcIssueZQ (MrcData, ZqType);
  }

  return;
}

/**
  This function sets up a test with CADB for the given channel mask and per Outputs->McChBitMask.
  McChBitMask is initialized in MrcPretraining based on population, and used throughout internal CPGC structure.
  Modify McChBitMask to specify which MC/CH to program

  @param[in,out] MrcData     - Pointer to MRC global data.
  @param[in]     McChBitMask - Memory Controller Channel Bit mask for which test should be setup for.
  @param[in]     LC          - Exponential umber of loops to run the test.
  @param[in]     SOE         - Error handling switch for test.
  @param[in]     EnCADB      - Switch to enable CADB
  @param[in]     EnCKE       - Switch to enable CKE.

  @retval Nothing
**/
void
SetupIOTestCADB (
  IN OUT MrcParameters *const MrcData,
  IN     const UINT8          McChBitMask,
  IN     const UINT8          LC,
  IN     const UINT8          SOE,
  IN     const UINT8          EnCADB,
  IN     const UINT8          EnCKE
  )
{
  // Note: Row and Col bit size below must match the values set in Cpgc20AddressSetupCmdStress()
  static const Cpgc20Address CPGCAddress_Ddr4 = {
    CPGC20_ROW_COL_2_BANK_2_RANK,
    CPGC20_FAST_Y,
    0,    // Single Address Instruction
    0,
    0,
    14,   // RowSizeBits
    7,    // ColSizeBits
    8
  };
  static const Cpgc20Address CPGCAddress_Ddr5 = {
    CPGC20_ROW_COL_2_BANK_2_RANK,
    CPGC20_FAST_Y,
    0,    // Single Address Instruction
    0,
    0,
    16,   // RowSizeBits
    6,    // ColSizeBits
    8
  };
  static const Cpgc20Address CPGCAddress_Lpddr4 = {
    CPGC20_ROW_COL_2_BANK_2_RANK,
    CPGC20_FAST_Y,
    0,    // Single Address Instruction
    0,
    0,
    14,   // RowSizeBits
    5,    // ColSizeBits
    8
  };
  const Cpgc20Address *CpgcAddress;
  MRC_PATTERN_CTL     PatCtl;
  MrcOutput           *Outputs;
  UINT16              NumCL;

  Outputs           = &MrcData->Outputs;
  PatCtl.IncRate    = 4;
  PatCtl.Start      = 0;
  PatCtl.Stop       = 9;
  PatCtl.DQPat      = CADB;
  PatCtl.PatSource  = MrcPatSrcDynamic;
  PatCtl.EnableXor  = FALSE;
  CpgcAddress       = 0;
  NumCL             = 0;

  // Column limit is 2^10
  // BL=8:  column increment is 2^3 per CL --> 10 - 3 = 7, 2^7 = 128
  // BL=16: column increment is 2^4 per CL --> 10 - 4 = 6, 2^6 = 64
  // BL=32: column increment is 2^5 per CL --> 10 - 5 = 5, 2^5 = 32
  switch (Outputs->DdrType) {
    case MRC_DDR_TYPE_DDR4:
      CpgcAddress = &CPGCAddress_Ddr4;
      NumCL = 128;
      break;
    case MRC_DDR_TYPE_DDR5:
      CpgcAddress = &CPGCAddress_Ddr5;
      NumCL = 64;
      break;
    case MRC_DDR_TYPE_LPDDR5:
      CpgcAddress = &CPGCAddress_Lpddr4;
      NumCL = 16;                          // @todo_adl See if we can go back to 32 on LP5
      break;
    default: // LP4
      CpgcAddress = &CPGCAddress_Lpddr4;
      NumCL = 32;
      break;
  }

  SetupIOTest (MrcData, McChBitMask, PatWrRd, NumCL, LC, CpgcAddress, SOE, &PatCtl, EnCADB, EnCKE, 0);

  Outputs->DQPatLC = LC - 2 - 3 + 1;
  if (Outputs->DQPatLC < 1) {
    Outputs->DQPatLC = 1;
  }

  Outputs->DQPat = CADB;
  return;
}

/**
  This function sets up a basic victim-aggressor test for the given channel mask and per Outputs->McChBitMask.
  McChBitMask is initialized in MrcPretraining based on population, and used throughout internal CPGC structure.
  Modify McChBitMask to specify which MC/CH to program

  @param[in,out] MrcData       - Pointer to MRC global data.
  @param[in]     McChBitMask   - Memory Controller Channel Bit mask for which test should be setup for.
  @param[in]     LC            - Exponential umber of loops to run the test.
  @param[in]     SOE           - Error handling switch for test.
  @param[in]     EnCADB        - Switch to enable CADB
  @param[in]     EnCKE         - Switch to enable CKE.
  @param[in]     Spread        - Stopping point of the pattern.
  @param[in]     CmdPat        - Command Pattern
  @param[in]     Wait          - Wait time between subsequences
  @param[in]     NumCLOverride - Override for NumCL (Non-zero causes override)

  @retval Nothing
**/
void
SetupIOTestBasicVA (
  IN OUT MrcParameters *const MrcData,
  IN     const UINT8          McChBitMask,
  IN     const UINT8          LC,
  IN     const UINT8          SOE,
  IN     const UINT8          EnCADB,
  IN     const UINT8          EnCKE,
  IN     const UINT8          Spread,
  IN     const UINT8          CmdPat,
  IN     const UINT8          Wait,
  IN     const UINT16         NumCLOverride
  )
{
  static const Cpgc20Address CPGCAddress_ddr4 = {
    CPGC20_BANK_2_ROW_COL_2_RANK,
    CPGC20_FAST_Y,
    0x0,    // Single Address Instruction
    0,
    0,
    0,
    6,      // ColSizeBits - 2^7 = 128, use 6 because we have twice more CL's due to Bank Group toggle
    2       // BankSize    - 2 bank groups toggle for B2B traffic
  };

  static const Cpgc20Address CPGCAddress_ddr5 = {
    CPGC20_BANK_2_ROW_COL_2_RANK,
    CPGC20_FAST_Y,
    0x0,    // Single Address Instruction
    0,
    0,
    0,
    5,      // ColSizeBits - 2^6 = 64, use 5 because we have twice more CL's due to Bank Group toggle
    2       // BankSize    - 2 bank groups toggle for B2B traffic
  };

  static const Cpgc20Address CPGCAddress_lpddr = {
    CPGC20_BANK_2_ROW_COL_2_RANK,
    CPGC20_FAST_Y,
    0x0,    // Single Address Instruction
    0,
    0,
    0,
    5,      // ColSizeBits - 2^5 = 32
    1       // BankSize    - LP4/5 doesn't need bank groups toggle for B2B   @todo_rpl LP5 BG mode might need this
  };

  MRC_PATTERN_CTL        PatCtl;
  MrcOutput             *Outputs;
  UINT16                NumCL;
  BOOLEAN               Ddr4;
  BOOLEAN               Ddr5;
  const Cpgc20Address   *CPGCAddress;
  UINT8                 AdjustedLoopCount;

  Outputs           = &MrcData->Outputs;
  Ddr4              = (Outputs->DdrType == MRC_DDR_TYPE_DDR4);
  Ddr5              = (Outputs->DdrType == MRC_DDR_TYPE_DDR5);
  PatCtl.Start      = 0;
  PatCtl.Stop       = Spread - 1;
  PatCtl.DQPat      = BasicVA;
  PatCtl.PatSource  = MrcPatSrcDynamic;
  PatCtl.EnableXor  = FALSE;

  // Column limit is 2^10
  // BL=8:  column increment is 2^3 per CL --> 10 - 3 = 7, 2^7 = 128
  // BL=16: column increment is 2^4 per CL --> 10 - 4 = 6, 2^6 = 64
  // BL=32: column increment is 2^5 per CL --> 10 - 5 = 5, 2^5 = 32
  NumCL = NumCLOverride ? NumCLOverride : (Ddr4 ? 128 : (Ddr5 ? 64 : 32));
  PatCtl.IncRate = 4;
  CPGCAddress = Ddr4 ? &CPGCAddress_ddr4 : (Ddr5 ? &CPGCAddress_ddr5 : &CPGCAddress_lpddr);

  AdjustedLoopCount = LC;
  if (Ddr5) {
    // Divide the Loopcount by 2 for Data stress, because DDR5 has twice longer burst length comparing to DDR4.
    if (AdjustedLoopCount >= 1) {
      AdjustedLoopCount -= 1;
    }
  }
  if (Outputs->Lpddr) {
    // Divide the Loopcount by 4 for Data stress, because LP4/LP5 has 4 times longer burst length comparing to DDR4.
    if (AdjustedLoopCount >= 2) {
      AdjustedLoopCount -= 2;
    }
  }

  SetupIOTest (MrcData, McChBitMask, CmdPat, NumCL, AdjustedLoopCount, CPGCAddress, SOE, &PatCtl, EnCADB, EnCKE, Wait);

  Outputs->DQPatLC = AdjustedLoopCount - 8 + 1;
  if (Outputs->DQPatLC < 1) {
    Outputs->DQPatLC = 1;
  }

  Outputs->DQPat = BasicVA;
  return;
}
// TGL_POWER_TRAINING - Define a new setup function for read-only and write-only CPGC tests based on the these changes.
/*
void
SetupIOTestBasicVA (
  IN OUT MrcParameters *const MrcData,
  IN     const UINT8          McChBitMask,
  IN     const UINT8          LC,
  IN     const UINT8          SOE,
  IN     const UINT8          EnCADB,
  IN     const UINT8          EnCKE,
  IN     const UINT8          Spread
  )
{
  static const Cpgc20Address CPGCAddress_ddr4 = {
    CPGC20_BANK_2_ROW_COL_2_RANK,
    CPGC20_FAST_Y,
    0x0,    // Single Address Instruction
    0,
    0,
    0,
    7,
    2
  };

  static const Cpgc20Address CPGCAddress_lpdd4 = {
    CPGC20_BANK_2_ROW_COL_2_RANK,
    CPGC20_FAST_Y,
    0x0,    // Single Address Instruction
    0,
    0,
    0,
    6,
    1
  };

  MRC_PATTERN_CTL        PatCtl;
  MrcOutput             *Outputs;
  UINT16                NumCL;
  BOOLEAN               Ddr4;
  const Cpgc20Address   *CPGCAddress;
  UINT8               CmdPat;
  CmdPat            = EnCADB >> 1;
  if (CmdPat == 0x0) {
    // Set command pattern to write/read.
    CmdPat = PatWrRd;
  }
  Outputs           = &MrcData->Outputs;
  Ddr4              = (Outputs->DdrType == MRC_DDR_TYPE_DDR4);
  PatCtl.Start      = 0;
  PatCtl.Stop       = Spread - 1;
  PatCtl.DQPat      = BasicVA;
  PatCtl.PatSource  = MrcPatSrcDynamic;
  PatCtl.EnableXor  = FALSE;

#ifdef CTE_FLAG
  NumCL = 2;
  PatCtl.IncRate = NumCL;
#else
  // Column limit is 2^10
  // BL=8:  column increment is 2^3 per CL --> 10 - 3 = 7, 2^7 = 128
  // BL=16: column increment is 2^4 per CL --> 10 - 4 = 6, 2^6 = 64
  NumCL = Ddr4 ? 128 : 64;
  PatCtl.IncRate = 4;
#endif // CTE_FLAG
  CPGCAddress = Ddr4 ? &CPGCAddress_ddr4 : &CPGCAddress_lpdd4;

  SetupIOTest (MrcData, McChBitMask, CmdPat, NumCL, LC, CPGCAddress, SOE, &PatCtl, (EnCADB & MRC_BIT0), EnCKE, 0);

  Outputs->DQPatLC = LC - 8 + 1;
  if (Outputs->DQPatLC < 1) {
    Outputs->DQPatLC = 1;
  }

  Outputs->DQPat = BasicVA;
  return;
}
*/

/**
  This function sets up a MPR test for the given channel mask and per Outputs->McChBitMask.
  McChBitMask is initialized in MrcPretraining based on population, and used throughout internal CPGC structure.
  Modify McChBitMask to specify which MC/CH to program

  @param[in,out] MrcData     - Pointer to MRC global data.
  @param[in]     McChBitMask - Memory Controller Channel Bit mask for which test should be setup for.
  @param[in]     LC          - Exponential umber of loops to run the test.
  @param[in]     SOE         - Error handling switch for test.
  @param[in]     EnCADB      - Switch to enable CADB
  @param[in]     EnCKE       - Switch to enable CKE.

  @retval Nothing
**/
void
SetupIOTestMPR (
  IN OUT MrcParameters *const MrcData,
  IN     const UINT8          McChBitMask,
  IN     const UINT8          LC,
  IN     const UINT8          SOE,
  IN     const UINT8          EnCADB,
  IN     const UINT8          EnCKE
  )
{
  SetupIOTestEndless (MrcData, McChBitMask, LC, SOE, EnCADB, EnCKE, PatRdEndless);
  return;
}

/**
  This function sets up an Endless (Rd, Wr, or WrRd) test for the given channel mask and per Outputs->McChBitMask.
  McChBitMask is initialized in MrcPretraining based on population, and used throughout internal CPGC structure.
  Modify McChBitMask to specify which MC/CH to program

  @param[in,out] MrcData     - Pointer to MRC global data.
  @param[in]     McChBitMask - Memory Controller Channel Bit mask for which test should be setup for.
  @param[in]     LC          - Exponential umber of loops to run the test.
  @param[in]     SOE         - Error handling switch for test.
  @param[in]     EnCADB      - Switch to enable CADB
  @param[in]     EnCKE       - Switch to enable CKE.
  @param[in]     CmdPat      - Command Pattern (Only Endless are supported: PatRdEndless, PatWrEndless, PatWrRdEndless; Defaults to PatRdEndless if incorrect CmdPat passed in).

  @retval Nothing
**/
void
SetupIOTestEndless (
  IN OUT MrcParameters *const MrcData,
  IN     const UINT8          McChBitMask,
  IN     const UINT8          LC,
  IN     const UINT8          SOE,
  IN     const UINT8          EnCADB,
  IN     const UINT8          EnCKE,
  IN     const UINT8          CmdPat
  )
{
  static const Cpgc20Address CPGCAddressDdr4 = {
    CPGC20_BANK_2_ROW_COL_2_RANK,
    CPGC20_FAST_Y,
    0xFF,               // LastValidAddr: 0xFF means endless test
    0,
    0,
    0,
    4,
    2
  };
  static const Cpgc20Address CPGCAddressLpddr4 = {
    CPGC20_BANK_2_ROW_COL_2_RANK,
    CPGC20_FAST_Y,
    0xFF,               // LastValidAddr: 0xFF means endless test
    0,
    0,
    0,
    5,
    1
  };
  const Cpgc20Address   *CpgcAddress;
  MRC_PATTERN_CTL        PatCtl;
  MrcOutput             *Outputs;
  MrcDdrType            DdrType;
  UINT16                NumCL;
  UINT8                 EndlessCmdPat;

  Outputs = &MrcData->Outputs;
  DdrType = Outputs->DdrType;

  PatCtl.IncRate    = 0;
  PatCtl.Start      = 0;
  PatCtl.Stop       = 1;  // Dont't Care as MPR doesn't use WDB  Either IO or PG.
  PatCtl.DQPat      = StaticPattern; // Don't want RunIOTest changing the PG.
  PatCtl.PatSource  = MrcPatSrcDynamic; // PG should be used instead of WDB
  PatCtl.EnableXor  = FALSE; // Disable the Xor of WDB.
  CpgcAddress = ((DdrType == MRC_DDR_TYPE_DDR4) || (DdrType == MRC_DDR_TYPE_DDR5)) ? &CPGCAddressDdr4 : &CPGCAddressLpddr4;

  switch (CmdPat) {
    case PatRdEndless:
    case PatWrEndless:
    case PatWrRdEndless:
      EndlessCmdPat = CmdPat;
      break;

    default:
      EndlessCmdPat = PatRdEndless;
      MRC_DEBUG_ASSERT ((1 == 0), &MrcData->Outputs.Debug, "Unsupported CmdPat selected for SetupIOTestEndless!\n");
  }
  NumCL = 128;

  SetupIOTest (MrcData, McChBitMask, EndlessCmdPat, NumCL, LC, CpgcAddress, SOE, &PatCtl, EnCADB, EnCKE, 0);
  WriteVAPattern (MrcData, McChBitMask, BASIC_VA_VICTIM_16, BASIC_VA_AGGRESSOR_16);
  MrcSetupTestErrCtl (MrcData, NSOE, 0);
  Cpgc20BaseRepeats (MrcData, 0x80000000, 1);

  Outputs->DQPatLC = 1;
  Outputs->DQPat   = StaticPattern;
  return;
}

/**
  This function sets up a MPR test for the given channel mask and per Outputs->McChBitMask.
  McChBitMask is initialized in MrcPretraining based on population, and used throughout internal CPGC structure.
  Modify McChBitMask to specify which MC/CH to program

@param[in,out] MrcData     - Pointer to MRC global data.
@param[in]     McChBitMask - Memory Controller Channel Bit mask for which test should be setup for.
@param[in]     LC          - Exponential umber of loops to run the test.
@param[in]     SOE         - Error handling switch for test.
@param[in]     RdDataPtn   - Read Data Pattern in 32bit  (i.e. MPR - LPDDR:0x55     DDR4:0xAA)

@retval Nothing
**/
void
SetupIOTestCpgcRead (
  IN OUT MrcParameters *const MrcData,
  IN     const UINT8          McChBitMask,
  IN     const UINT8          LC,
  IN     const UINT8          SOE,
  IN     const UINT8          RdDataPtn
  )
{
  const MRC_FUNCTION    *MrcCall;
  MRC_PATTERN_CTL       PatCtl;
  MrcOutput             *Outputs;
  UINT16                NumCL;
  UINT16                IncScale;
  BOOLEAN               Ddr5;
  BOOLEAN               Lp5Gear4;

  static const Cpgc20Address CPGCAddress = {
    CPGC20_BANK_2_ROW_COL_2_RANK,
    CPGC20_FAST_Y,
    0, //Run only on first Address Instruction
    0,
    0,
    0,
    4,
    2
  };
  UINT32 Pattern[8][2];

  MrcCall  = MrcData->Inputs.Call.Func;
  Outputs  = &MrcData->Outputs;
  MrcCall->MrcSetMem ((UINT8 *) Pattern, sizeof (Pattern), RdDataPtn);
  Ddr5     = (Outputs->DdrType == MRC_DDR_TYPE_DDR5);
  Lp5Gear4 = (Outputs->DdrType == MRC_DDR_TYPE_LPDDR5) && Outputs->Gear4;

  IncScale = 0;         // Linear
  PatCtl.IncRate = 0;
  PatCtl.Start = 0;
  PatCtl.Stop = 1;  // Dont't Care as MPR doesn't use WDB  Either IO or PG.

  if (Ddr5 && (RdDataPtn != 0)) {
    PatCtl.DQPat     = GaloisMprVa;         // Use Galois LFSR mode
    PatCtl.PatSource = MrcPatSrcDynamic;
  } else {
    PatCtl.DQPat     = StaticPattern;       // Use Pattern Buffer mode
    PatCtl.PatSource = MrcPatSrcStatic;
  }

  NumCL = (Lp5Gear4 && (Outputs->Frequency >= f5200)) ? 9 : 32;

  SetupIOTest (MrcData, McChBitMask, PatRd, NumCL, LC, &CPGCAddress, SOE, &PatCtl, 0, 0, 0);
  Cpgc20SetDpatAltBufCtl (MrcData, IncScale, PatCtl.IncRate, PatCtl.Start, PatCtl.Stop);

  if (Ddr5 && (RdDataPtn != 0)) {
    Outputs->DQPatLC = LC;
  } else {
    WriteFixedPattern (MrcData, Pattern);
    Outputs->DQPatLC = 1;
  }
  Outputs->DQPat = PatCtl.DQPat;
  MrcSetupTestErrCtl (MrcData, NSOE, 0);
  return;
}

/**
  This function sets up a Clock Pattern test for the given Mask and per Outputs->McChBitMask.
  McChBitMask is initialized in MrcPretraining based on population, and used throughout internal CPGC structure.
  Modify McChBitMask to specify which MC/CH to program

  @param[in,out] MrcData     - Pointer to MRC global data.
  @param[in]     McChBitMask - Memory Controller Channel Bit mask for which test should be setup for.
  @param[in]     LC          - Exponential umber of loops to run the test.
  @param[in]     SOE         - Error handling switch for test.
  @param[in]     EnCADB      - Switch to enable CADB
  @param[in]     EnCKE       - Switch to enable CKE.

  @retval Nothing
**/
void
SetupIOTestClock (
  IN OUT MrcParameters *const MrcData,
  IN     const UINT8          McChBitMask,
  IN     const UINT8          LC,
  IN     const UINT8          SOE,
  IN     const UINT8          EnCADB,
  IN     const UINT8          EnCKE
  )
{
  static const Cpgc20Address CPGCAddress = {
    CPGC20_ROW_COL_2_BANK_2_RANK,
    CPGC20_FAST_Y,
    0xFF,
    0,
    0,
    0,
    4,
    2
  };
  const Cpgc20Address   *CpgcAddress;
  MRC_PATTERN_CTL        PatCtl;
  MrcOutput             *Outputs;
  UINT16                NumCL;

  Outputs = &MrcData->Outputs;

  PatCtl.IncRate    = 1;
  PatCtl.Start      = 0;
  PatCtl.Stop       = 1;  // Don't Care as Clock Test doesn't use WDB:  Either IO or PG.
  PatCtl.DQPat      = StaticPattern; // Don't want RunIOTest changing the PG.
  PatCtl.PatSource  = MrcPatSrcDynamic; // PG should be used instead of WDB
  PatCtl.EnableXor  = FALSE; // Disable the Xor of WDB.

  NumCL = (Outputs->DdrType == MRC_DDR_TYPE_DDR4) ? 128 : 32;

  CpgcAddress = &CPGCAddress;

  SetupIOTest (MrcData, McChBitMask, PatWrRd, NumCL, LC, CpgcAddress, SOE, &PatCtl, EnCADB, EnCKE, 0);

  Outputs->DQPatLC = 1;
  Outputs->DQPat   = StaticPattern;
  return;
}

/**
  This function sets up a basic static WR/RD test for the given channel mask.

  @param[in,out] MrcData     - Pointer to MRC global data.
  @param[in]     McChBitMask - Memory Controller Channel Bit mask for which test should be setup for.
  @param[in]     LC          - Exponential number of loops to run the test.
  @param[in]     SOE         - Error handling switch for test.
  @param[in]     EnCADB      - Switch to enable CADB
  @param[in]     EnCKE       - Switch to enable CKE.
  @param[in]     NumIters    - Desired number of iterations on each cacheline.
  @param[in]     Pattern     - Array of chunks (64 bit), broken into two 32-bit entries (second dimension).
                               Length (first dimension) can be up to NUM_DPAT_EXTBUF

  @retval Nothing
**/
void
SetupIOTestStatic (
  IN OUT MrcParameters *const MrcData,
  IN     const UINT8          McChBitMask,
  IN     const UINT8          LC,
  IN     const UINT8          SOE,
  IN     const UINT8          EnCADB,
  IN     const UINT8          EnCKE,
  IN     const UINT16         NumIters,
  IN           UINT32         Pattern[][2]
)
{
  static const Cpgc20Address CPGCAddressDdr4 = {
    CPGC20_BANK_2_ROW_COL_2_RANK,
    CPGC20_FAST_Y,
    0, //Run only on first Address Instruction
    0,
    0,
    0,
    4,
    2
  };
  static const Cpgc20Address CPGCAddressLpddr4 = {
    CPGC20_BANK_2_ROW_COL_2_RANK,
    CPGC20_FAST_Y,
    0, //Run only on first Address Instruction
    0,
    0,
    0,
    5,
    2
  };
  const Cpgc20Address   *CpgcAddress;
  MRC_PATTERN_CTL       PatCtl;
  MrcOutput             *Outputs;
  MrcDdrType            DdrType;
  UINT16                IncScale;
  UINT8                 NumCL;

  Outputs           = &MrcData->Outputs;
  DdrType           = Outputs->DdrType;
  NumCL             = 4;
  IncScale          = (NumIters > 64) ? 0 : 1; // 0 <Exponential> : 1 <Linear>
  PatCtl.IncRate    = (IncScale == 0) ? (MrcLog2 (NumIters)) : (NumIters - 1);
  PatCtl.Start      = 0;
  PatCtl.Stop       = 3; //Run on all 4 cachelines
  PatCtl.DQPat      = StaticPattern;
  PatCtl.PatSource  = MrcPatSrcStatic;
  PatCtl.EnableXor  = TRUE;
  CpgcAddress       = (DdrType == MRC_DDR_TYPE_DDR4) ? &CPGCAddressDdr4 : &CPGCAddressLpddr4;

  SetupIOTest (MrcData, McChBitMask, PatWrRd, NumCL, LC, CpgcAddress, NSOE, &PatCtl, 0, 0, 0);
  Cpgc20SetDpatAltBufCtl (MrcData, IncScale, PatCtl.IncRate, PatCtl.Start, PatCtl.Stop);
  WriteFixedPattern (MrcData, Pattern);
  MrcSetupTestErrCtl (MrcData, NSOE, 0);

  Outputs->DQPatLC = 1;
  Outputs->DQPat   = StaticPattern;
  return;
}

/**

  CPGC setup per rank for Advanced Memory test

  @param[in] Socket            - Memory controller to train
  @param[in] DdrChEnMap        - ddr channels to setup
  @param[in] LogRank           - logical rank to setup
  @param[in] LogSubRank        - logical subrank number to test
  @param[in] ColAddressBits    - number of DRAM column address bits to test
  @param[in] RowAddressBits    - number of DRAM row address bits to test
  @param[in] BankAddressBits   - number of DRAM bank address bits to test
  @param[in] Mode              - Type of sequence MT_CPGC_WRITE, MT_CPGC_READ, or MT_CPGC_READ_WRITE
  @param[in] AddressMode       - FAST_Y for column first, FAST_X for row first
  @param[in] Direction         - Sequential address direction MT_ADDR_DIR_UP, MT_ADDR_DIR_DN
  @param[in] Bank              - Base bank address number to test
  @param[in] BaseBankBits      - Number of bank address bits to skip on increment (for BG interleave)
  @param[in] RowAddr[MAX_CH]   - Base row address to start testing
  @param[in] RowSize[MAX_CH]   - Number of rows to test
  @param[in] DeviceMask[MAX_CH][3] - Bitmask of DQ lanes to not test
  @param[in] LineMask[MAX_CH]      - Each bit corresponds to a cache line; 1 means test, 0 do not test
  @param[in] StopOnErr[MAX_CH]     - 1 = stop on 1st error; 0 = stop on all DQ lanes error
  @param[in] SeqDataInv[MT_MAX_SUBSEQ] - Enables pattern inversion per subsequence
  @param[in] IsUseInvtPat      - Info to indicate whether or not patternQW is inverted by comparing original pattern

  @retval None

  **/
VOID
EFIAPI
CpgcMemTestRankSetupMATSRowRange (
  IN OUT MrcParameters *const MrcData,
  IN UINT32                   McChBitMask,
  IN UINT32                   Rank,
  IN UINT8                    ColAddressBits[MAX_CONTROLLER][MAX_CHANNEL],
  IN UINT8                    RowAddressBits[MAX_CONTROLLER][MAX_CHANNEL],
  IN UINT8                    BankAddressBits[MAX_CONTROLLER][MAX_CHANNEL],
  IN UINT8                    Mode,
  IN UINT8                    Direction,
  IN UINT32                   Bank,
  IN UINT32                   BaseBankBits,
  IN UINT32                   RowAddr[MAX_CONTROLLER][MAX_CHANNEL],
  IN UINT32                   RowSize[MAX_CONTROLLER][MAX_CHANNEL],
  IN UINT32                   DeviceMask[MAX_CONTROLLER][MAX_CHANNEL][3],
  IN UINT8                    LineMask[MAX_CONTROLLER][MAX_CHANNEL],
  IN UINT8                    StopOnErr[MAX_CONTROLLER][MAX_CHANNEL],
  IN UINT8                    SeqDataInv[2],
  IN BOOLEAN                  IsUseInvtPat
  )
{
  MrcOutput             *Outputs;
  UINT8                 CpgcColAddressBits;
  UINT8                 CpgcBankAddressBits;
  UINT8                 CpgcBankGroupBits;
  UINT8                 CpgcBankGroups;
  UINT8                 Index;
  INT32                 MatsBurstBits = 0;
  UINT8                 ReadWriteModeSeq0;
  UINT8                 ReadWriteModeSeq1;
  UINT32                StartAddress;
  UINT8                 Controller;
  UINT8                 Channel;
  UINT32                MaxBank;
  UINT64_STRUCT         DqMask;
  UINT8                 EccMask;
  MRC_BG_BANK_PAIR      BankMapping[MAX_DDR5_BANK_GROUPS];

  Outputs = &MrcData->Outputs;

  for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
    for (Channel = 0; Channel < Outputs->MaxChannels; Channel++) {
      if ((MC_CH_MASK_CHECK (McChBitMask, Controller, Channel, Outputs->MaxChannels) == 0) ||
          (RowSize[Controller][Channel] == 0)) {
        MrcSetCpgcBindMode (MrcData, FALSE, FALSE);
        // skip ddr channels not selected or disabled ddr channels
        // Must disable global start so start test does not activate cpgc on disabled channel for this rank
        continue;
      }

      if (Direction == MT_ADDR_DIR_UP) {
        StartAddress = RowAddr[Controller][Channel];
      } else {
        StartAddress = RowAddr[Controller][Channel] + RowSize[Controller][Channel] - 1;
      }
      // Programs GPGC to run on the selected physical ranks
      SelectReutRanks (MrcData, Controller, Channel, 1 << Rank, FALSE, 0);

      MrcClearRasterRepo (MrcData, (UINT8) McChBitMask);
      MRC_DEBUG_MSG (&Outputs->Debug, MSG_LEVEL_NOTE, "Mc%d.C%d: LineMask_%02x: Row = 0x%08x, Size = 0x%08x, Bank = %2d, Dir = %d, DevMaskLo = 0x%08x, DevMaskHi = 0x%08x, DevMaskEcc = 0x%02x, ColBits = %d, RowBits = %d, BankBits = %d\n",
        Controller, Channel, LineMask[Controller][Channel], StartAddress, RowSize[Controller][Channel], Bank, Direction, DeviceMask[Controller][Channel][0], DeviceMask[Controller][Channel][1],
        DeviceMask[Controller][Channel][2] & MRC_UINT8_MAX, ColAddressBits[Controller][Channel], RowAddressBits[Controller][Channel], BankAddressBits[Controller][Channel]);

      // DDR5 Burst Length is 16
      CpgcColAddressBits = ColAddressBits[Controller][Channel] - (MrcLog2 (Outputs->BurstLength) - 1);  // The log function return +1 so we subtract 1

      if (Outputs->DdrType == MRC_DDR_TYPE_DDR4) {
        // DDR4 has 4BAx2BG per channel
        // DDR4 banks are split between 2 channels, so subtract one from total BankAddressBits
        CpgcBankAddressBits = BankAddressBits[Controller][Channel] - 1;
      } else {
        CpgcBankAddressBits = BankAddressBits[Controller][Channel];
      }

      MatsBurstBits = CpgcColAddressBits + (CpgcBankAddressBits - BaseBankBits);

      MaxBank = (1 << (CpgcBankAddressBits - BaseBankBits)) - 1;

      DqMask.Data32.Low  = DeviceMask[Controller][Channel][0];
      DqMask.Data32.High = DeviceMask[Controller][Channel][1];
      EccMask = DeviceMask[Controller][Channel][2] & MRC_UINT8_MAX;

      // Set cache line mask and stop on error control
      MrcSetupTestErrCtl (MrcData, NTHSOE, 1); // Stop on first error

      MrcSetChunkAndClErrMsk (MrcData, LineMask[Controller][Channel], 0xFFFFFFFF);
      MrcSetDataAndEccErrMsk (MrcData, ~DqMask.Data, ~EccMask);
      Cpgc20AddressSetupPPR (MrcData, McChBitMask, StartAddress, 0, RowAddressBits[Controller][Channel], CpgcColAddressBits, MaxBank, 0);

      // TODO: Review for enabling LP4/LP5
      if (!Outputs->Lpddr) {
        // Used for bank interleave
        CpgcBankGroupBits = (UINT8) (CpgcBankAddressBits - BaseBankBits);
        CpgcBankGroups = (UINT8) (1 << CpgcBankGroupBits);
        for (Index = 0; Index < CpgcBankGroups; Index++) {
          BankMapping[Index].Bank = (UINT8) Bank;
          BankMapping[Index].BankGroup = Index;
        }
        // For the current Bank in the software loop, CPGC iterates over BgInterleave logical banks
        // The bank locations are set via a logical-to-physical mapping
        MrcGetSetBankSequence (MrcData, Controller, Channel, BankMapping, CpgcBankGroups, FALSE);
      }

      Cpgc20BaseRepeats (MrcData, (1 << MatsBurstBits), 1); // Num Ranks = 1

      MrcSetLoopcount (MrcData, RowSize[Controller][Channel] - 1);

      if (Mode == PatRdWr) {
        ReadWriteModeSeq0 = CPGC_BASE_READ_SUBSEQ;
        ReadWriteModeSeq1 = CPGC_BASE_WRITE_SUBSEQ;
      } else if (Mode == PatRd) {
        ReadWriteModeSeq0 = CPGC_BASE_READ_SUBSEQ;
        ReadWriteModeSeq1 = CPGC_NO_SUBSEQ1;
      } else { // PatWr
        ReadWriteModeSeq0 = CPGC_BASE_WRITE_SUBSEQ;
        ReadWriteModeSeq1 = CPGC_NO_SUBSEQ1;
      }
      CpgcMemTestCmdPatSetup (MrcData, Controller, Channel, ReadWriteModeSeq0, ReadWriteModeSeq1, Direction, SeqDataInv, IsUseInvtPat);
    } // Ch
  }
  return;
}

/**
This function sets up a WR-only test for the given MC channel mask, with all-zero data.
This is used for ECC scrubbing or memory clear.

@param[in,out] MrcData     - Pointer to MRC global data.
@param[in]     McChbitMask - Bit masks of MC channels to enable for the test.

**/
void
SetupIOTestScrub (
  IN OUT MrcParameters *const MrcData,
  IN     const UINT8          McChBitMask
  )
{
  // Row/Col/Bank sizes may be different per DIMM, hence set them later using Cpgc20AddressSetupScrub()
  static const Cpgc20Address CpgcAddress = {
    CPGC20_BANK_2_ROW_COL_2_RANK,
    CPGC20_FAST_Y,
    0, // Run only on first Address Instruction
    0, // RowStart
    0, // ColStart
    0, // RowSizeBits
    0, // ColSizeBits
    0  // BankSize
  };
  MRC_PATTERN_CTL       PatCtl;
  UINT8                 NumCL;
  UINT8                 LC;

  NumCL = 0;
  LC = 0;
  PatCtl.IncRate = 0;
  PatCtl.Start = 0;
  PatCtl.Stop = 0;
  PatCtl.DQPat = StaticPattern;
  PatCtl.PatSource = MrcPatSrcAllZeroes;  // DC zero
  PatCtl.EnableXor = FALSE;

  SetupIOTest (MrcData, McChBitMask, PatWrScrub, NumCL, LC, &CpgcAddress, NSOE, &PatCtl, 0, 0, 0);

  MrcData->Outputs.DQPat = PatCtl.DQPat;
  return;
}

/**
This function sets up a test for the given MC channel mask, with specified data pattern.

@param[in,out] MrcData       - Pointer to MRC global data.
@param[in]     Controller    - Desired Memory Controller
@param[in]     Channel       - Desired Channel
@param[in]     DqLoopCount   - CPGC sequence loop count - number of times sequence is executed (2^(dqLoopCount - 1))
@param[in]     CmdPat        - [ 0: PatWrRd (Standard Write/Read Loopback) ///RH: this one not used!
1: PatWr (Write Only),
2: PatRd (Read Only),
13: PatRdWr ]
Other values not valid
@param[in]     SeqDataInv[2] - Enables pattern inversion per subsequence
@param[in]     Pattern[2]    - 64-bit wide data pattern to use per UI
@param[in]     IsUseInvtPat  - Info to indicate whether or not Pattern is inverted by comparing original pattern
@param[in]     NumCacheLines - Number of cachelines to use in WDB
@param[in]     Direction     - Sequential address direction MT_ADDR_DIR_UP, MT_ADDR_DIR_DN

@retval mrcSuccess
**/
MrcStatus
SetupIOTestRetention (
  IN OUT   MrcParameters *const MrcData,
  IN const UINT8                Controller,
  IN const UINT8                Channel,
  IN       UINT8                DqLoopCount,
  IN       UINT8                CmdPat,
  IN       BOOLEAN              SeqDataInv[2],
  IN       UINT64               Pattern[2],
  IN       BOOLEAN              IsUseInvtPat,
  IN       UINT8                UiShl[16],
  IN       UINT8                NumCacheLines,
  IN       UINT8                Direction
  )
{
  MrcOutput           *Outputs;
  INT64               GetSetVal;
  UINT8               McChBitMask;
  UINT8               RwMode;
  UINT8               IpChannel;
  MRC_TEST_STOP_TYPE  StopOnErrCtl;
  UINT8               UseSubSeq1;
  UINT32              Offset;
  BOOLEAN             StopOnRaster;
  BOOLEAN             RasterRepoClear;
  UINT8               RasterRepoMode;
  BOOLEAN             Ddr5;
  MRC_PATTERN_CTL     PatCtl;
  MC0_REQ0_CR_CPGC_SEQ_CTL_STRUCT       CpgcSeqCtl;
  MC0_CH0_CR_CPGC_ERR_CNTRCTL_0_STRUCT  ErrCntrCtl;
  UINT64_STRUCT                       DataPattern;
  // Row/Col/Bank sizes may be different per DIMM, hence set them later using Cpgc20AddressSetupScrub()
  static const Cpgc20Address CpgcAddress = {
    CPGC20_BANK_2_ROW_COL_2_RANK,
    CPGC20_FAST_Y,
    0, // Run only on first Address Instruction
    0, // RowStart
    0, // ColStart
    0, // RowSizeBits
    0, // ColSizeBits
    0  // BankSize
  };


  PatCtl.IncRate = 1;
  PatCtl.Start = 0;
  PatCtl.Stop = 8;
  PatCtl.DQPat = StaticPattern;
  PatCtl.PatSource = MrcPatSrcStatic;
  PatCtl.EnableXor = FALSE;

  Outputs  = &MrcData->Outputs;
  Ddr5     = Outputs->DdrType == MRC_DDR_TYPE_DDR5;
  McChBitMask = (1 << ((Controller * Outputs->MaxChannels) + Channel));
  IpChannel = DDR5_IP_CH (Ddr5, Channel);
  DataPattern.Data = Pattern[0];
  MRC_DEBUG_MSG (&Outputs->Debug, MSG_LEVEL_NOTE, "SetupIOTestRetention: Mc%d.C%d:\n  pattern[0] = 0x%08x%08x\n",
    Controller, Channel, DataPattern.Data32.High, DataPattern.Data32.Low);

  SetupIOTest (MrcData, McChBitMask, CmdPat, NumCacheLines, 1, &CpgcAddress, NSOE, &PatCtl, 0, 0, 0);

  // Set subsequence read or write data based on requested CmdPat mode
  RwMode            = CPGC_BASE_WRITE_SUBSEQ;
  UseSubSeq1        = CPGC_NO_SUBSEQ1;
  if (CmdPat == PatRdWr || CmdPat == PatRd) {
    RwMode          = CPGC_BASE_READ_SUBSEQ;
  }
  if (CmdPat == PatRdWr) {
    UseSubSeq1      = CPGC_BASE_WRITE_SUBSEQ;
  }

  StopOnErrCtl      = NTHSOE;                                // Stop on first error

  // Setup WDB
  MrcProgramMATSPattern (MrcData, McChBitMask, Pattern, UiShl, 0);

  Cpgc20SetDpatBufCtl (MrcData, 1, 0, PatCtl.Start, PatCtl.Stop);

  // MC Controller Init - disable miscellaneous events
  // functional CKE logic, do not disable CKE powerdown,
  GetSetVal = 0x00;
  MrcGetSetMcCh (MrcData, Controller, Channel, GsmMccCkeOverride,   WriteToCache, &GetSetVal);
  MrcGetSetMcCh (MrcData, Controller, Channel, GsmMccCkeOn,         WriteToCache, &GetSetVal);
  MrcGetSetMcCh (MrcData, Controller, Channel, GsmMccOdtOverride,   WriteToCache | PrintValue, &GetSetVal);
  MrcGetSetMcCh (MrcData, Controller, Channel, GsmMccMprTrainDdrOn, WriteToCache, &GetSetVal);
  MrcFlushRegisterCachedData (MrcData);

  GetSetVal = 1;
  MrcGetSetMcCh (MrcData, MAX_CONTROLLER, MAX_CHANNEL, GsmMccCpgcInOrder, WriteNoCache, &GetSetVal);

  CpgcSeqCtl.Data = 0;
  CpgcSeqCtl.Bits.CLEAR_ERRORS = 1;
  CpgcSeqCtl.Bits.STOP_TEST    = 1;     // Stop previous test if any
  Cpgc20ControlRegWrite (MrcData, McChBitMask, CpgcSeqCtl);

  Cpgc20BaseRepeats (MrcData, NumCacheLines, 1);

  CpgcMemTestCmdPatSetup (MrcData, Controller, Channel, RwMode, UseSubSeq1, Direction, SeqDataInv, IsUseInvtPat);

  RasterRepoClear = 1;
  RasterRepoMode = 1;
  StopOnRaster = 1;
  Cpgc20RasterRepoConfig (MrcData, McChBitMask, NULL, NULL, &StopOnRaster, &RasterRepoClear, &RasterRepoMode); // Clear Raster Repo

  Cpgc20SetPgInvDcEn (MrcData, 0, 0);

  Cpgc20SetDpatAltBufCtl (MrcData, 0, 0, 0, 0);

  // Error check settings
  ErrCntrCtl.Data = 0;
  ErrCntrCtl.Bits.COUNTER_POINTER = 0; // don't care for counter_control_scope=0
  ErrCntrCtl.Bits.COUNTER_CONTROL_SEL = 0; // don't care for counter_control_scope=0
  ErrCntrCtl.Bits.COUNTER_CONTROL_UI = 0; // count errors on all UI's
  ErrCntrCtl.Bits.COUNTER_CONTROL_SCOPE = 0; // non-specific selection (count everything)
  Offset = (OFFSET_CALC_MC_CH (MC0_CH0_CR_CPGC_ERR_CNTRCTL_0_REG, MC1_CH0_CR_CPGC_ERR_CNTRCTL_0_REG, Controller, MC0_CH1_CR_CPGC_ERR_CNTRCTL_0_REG, IpChannel));  // Counter 0
  MrcWriteCR (MrcData, Offset, ErrCntrCtl.Data);
  ErrCntrCtl.Data = 0;
  ErrCntrCtl.Bits.COUNTER_POINTER = 0; // only matters for particular UI (counter_control_ui = 3)
  ErrCntrCtl.Bits.COUNTER_CONTROL_SEL = 3; // set to count errors on UI's
  ErrCntrCtl.Bits.COUNTER_CONTROL_UI = 1; // count errors on even UI's
  ErrCntrCtl.Bits.COUNTER_CONTROL_SCOPE = 1; // specific selection
  Offset += (MC0_CH0_CR_CPGC_ERR_CNTRCTL_1_REG - MC0_CH0_CR_CPGC_ERR_CNTRCTL_0_REG); // Counter 1
  MrcWriteCR (MrcData, Offset, ErrCntrCtl.Data);
  ErrCntrCtl.Data = 0;
  ErrCntrCtl.Bits.COUNTER_POINTER = 0; // only matters for particular UI (counter_control_ui = 3)
  ErrCntrCtl.Bits.COUNTER_CONTROL_SEL = 3;  // set to count errors on UI's
  ErrCntrCtl.Bits.COUNTER_CONTROL_UI = 2; // count errors on odd UI's
  ErrCntrCtl.Bits.COUNTER_CONTROL_SCOPE = 1; // specific selection
  Offset += (MC0_CH0_CR_CPGC_ERR_CNTRCTL_1_REG - MC0_CH0_CR_CPGC_ERR_CNTRCTL_0_REG); // Counter 2
  MrcWriteCR (MrcData, Offset, ErrCntrCtl.Data);

  //
  // Error Control Registers
  //  stop on nth error (must be 0)
  //  do not stop on error
  //  configure monitor errors on all cachelines
  //  do not monitor errors on all chunks
  //
  MrcSetChunkAndClErrMsk (MrcData, 0xFF, 0xFFFFFFFF); // LineMask
  MrcSetDataAndEccErrMsk (MrcData, 0xFFFFFFFFFFFFFFFFULL, 0xFF);
  Cpgc20SetupTestErrCtl (MrcData, StopOnErrCtl, 0);

  return mrcSuccess;
} // CpgcGlobalTrainingSetup

/**
This function sets up a WR-only test for the given MC channel mask, with all-zero data.
Used for driving DQ low for Post Package Repair after passing repair row address.

  @param[in,out] MrcData     - Pointer to MRC global data.
  @param[in]     Controller  - Targeted controller
  @param[in]     Channel     - Targeted channel
  @param[in]     Row         - Targeted row
  @param[in]     BankGroup   - Targeted bank group
  @param[in]     BankAddress - Targeted bank address
  @param[in]     ByteMask    - Targeted Bytes for DQ toggle

  @retval MrcStatus          - mrcSuccess if no errors encountered
**/
MrcStatus
SetupIOTestPPR (
  IN OUT MrcParameters* const MrcData,
  IN     const UINT32         Controller,
  IN     const UINT32         Channel,
  IN     const UINT32         Row,
  IN     const UINT32         BankGroup,
  IN     const UINT32         BankAddress,
  IN     const UINT16         *ByteMask     OPTIONAL
  )
{
  Cpgc20Address CpgcAddress = {
    CPGC20_BANK_2_ROW_COL_2_RANK,
    CPGC20_FAST_Y,
    0,   // Run only on first Address Instruction
    0,   // RowStart
    0,   // ColStart
    0,   // RowSizeBits
    0,   // ColSizeBits
    0    // BankSize
  };
  MRC_FUNCTION    *MrcCall;
  MrcOutput       *Outputs;
  MRC_PATTERN_CTL PatCtl;
  UINT8           NumCL;
  UINT8           LC;
  UINT8           Byte;
  UINT8           EccBitMask;
  UINT8           EccByte;
  UINT64          DqBitMask;
  MrcIntOutput    *IntOutputs;
  MrcStatus       Status = mrcSuccess;
  UINT32          McChBitMask;
  UINT32          Bank;

  IntOutputs = (MrcIntOutput *)(MrcData->IntOutputs.Internal);
  Outputs    = &MrcData->Outputs;
  MrcCall    = MrcData->Inputs.Call.Func;
  CpgcAddress.RowStart = Row;

  NumCL = 1;
  LC = 0;
  PatCtl.IncRate = 0;
  PatCtl.Start = 0;
  PatCtl.Stop = 0;
  PatCtl.DQPat = StaticPattern;
  PatCtl.PatSource = MrcPatSrcAllZeroes;  // DC zero
  PatCtl.EnableXor = FALSE;

  IntOutputs->SkipZq = TRUE;  // ZQ undesired in PPR

  McChBitMask = 1 << MC_CH_IDX (Controller, Channel, Outputs->MaxChannels);

  SetupIOTest (MrcData, (UINT8) McChBitMask, PatWr, NumCL, LC, &CpgcAddress, NSOE, &PatCtl, 0, 0, 0);

  MrcConvertBankAddresstoCpgcBank (MrcData, BankAddress, BankGroup, &Bank);

  Status = Cpgc20AddressSetupPPR (
             MrcData,
             McChBitMask,
             CpgcAddress.RowStart,
             0, // Column Start
             0, // RowSizeBits
             0, // ColSizeBits
             0,
             Bank // BankStart
             );

  if (ByteMask != NULL) {
    EccByte    = (Outputs->DdrType == MRC_DDR_TYPE_DDR5) ? MRC_DDR5_ECC_BYTE : ((Outputs->DdrType == MRC_DDR_TYPE_DDR4) ? MRC_DDR4_ECC_BYTE : 0);
    DqBitMask  = 0;
    EccBitMask = 0;
    for (Byte = 0; Byte < Outputs->SdramCount; Byte++) {
      if (((1 << Byte) & *ByteMask) != 0) {
        if (Outputs->EccSupport && Byte == EccByte) {
          EccBitMask = 0xFF;
        } else if (Byte < EccByte) {
          DqBitMask |= MrcCall->MrcLeftShift64 (0xFF, 8 * Byte);
        }
      }
    }
    Cpgc20SetPgInvDcEn (MrcData, DqBitMask, EccBitMask);  // Enable DC on all lanes in Byte Mask
  }

  IntOutputs->SkipZq = FALSE;
  if (Status != mrcSuccess) {
    return Status;
  }
  Outputs->DQPat = PatCtl.DQPat;

  return mrcSuccess;
}

/**
  Runs one or more REUT tests (based on TestType).

  @param[in] MrcData      - Include all MRC global data.
  @param[in] McChBitMask  - Memory Controller Bit mask for which test should be setup for.
  @param[in] DQPat        - The Pattern type for the test.  See MrcDqPat.
  @param[in] ClearErrors  - Decision to clear or not errors.
  @param[in] ResetMode    - Parameter that specifies what training step we are running the reset in.

  @retval Returns ch errors
**/
UINT8
RunIOTest (
  IN MrcParameters *const MrcData,
  IN const UINT8          McChBitMask,
  IN const UINT8          DQPat,
  IN const UINT8          ClearErrors,
  IN const UINT16         ResetMode
  )
{
  const MrcInput      *Inputs;
  const MRC_FUNCTION  *MrcCall;
  MrcOutput           *Outputs;
  MrcChannelOut       *ChannelOut;
  INT64               GetSetVal;
  INT64               CmdTriStateDisSave[MAX_CONTROLLER][MAX_CHANNEL];
  UINT32              Offset;
  UINT32              Controller;
  UINT32              Channel;
  UINT32              IpChannel;
  UINT32              TestIdx;
  UINT8               CurMcChBitMask;
  UINT8               ErrorStatus;
  UINT8               NumTests;
  UINT8               TestDoneStatus;
  UINT8               tRDRD_dr_Min[MAX_CONTROLLER][MAX_CHANNEL];
  UINT8               tRDRD_sg_Min[MAX_CONTROLLER][MAX_CHANNEL];
  UINT8               tRDRD_dg_Min[MAX_CONTROLLER][MAX_CHANNEL];
  UINT8               TurnAroundOffset;
  UINT8               MaxChannel;
  BOOLEAN             Lpddr;
  BOOLEAN             Ddr5;
  UINT32              vmask;
  UINT32              amask;
  UINT8               Rank;
  UINT8               RankMask;  // RankBitMask for both channels
  INT64               GetSetDis;
  INT64               GetSetEn;

//  REUT_CH_SEQ_BASE_ADDR_INC_CTL_0_STRUCT ReutChSeqBaseAddrIncCtl;
  MC0_REQ0_CR_CPGC_SEQ_CTL_STRUCT            CpgcSeqCtl;
  MC0_REQ0_CR_CPGC_SEQ_STATUS_STRUCT         CpgcSeqStatus;
  MC0_CH0_CR_CPGC_ERR_TEST_ERR_STAT_STRUCT   CpgcErrTestStatus;
  MC0_CH0_CR_CADB_CTL_STRUCT                 CadbControl;
  MC0_STALL_DRAIN_STRUCT                     StallDrain;

  Inputs        = &MrcData->Inputs;
  MrcCall       = Inputs->Call.Func;
  Outputs       = &MrcData->Outputs;
  Lpddr         = Outputs->Lpddr;
  Ddr5          = (Outputs->DdrType == MRC_DDR_TYPE_DDR5);
  MaxChannel    = Outputs->MaxChannels;
  RankMask      = Outputs->ValidRankMask;
  MrcCall->MrcSetMem ((UINT8 *) tRDRD_dr_Min, sizeof (tRDRD_dr_Min), 0);
  MrcCall->MrcSetMem ((UINT8 *) tRDRD_sg_Min, sizeof (tRDRD_sg_Min), 0);
  MrcCall->MrcSetMem ((UINT8 *) tRDRD_dg_Min, sizeof (tRDRD_dg_Min), 0);

  ErrorStatus            = 0;
  NumTests               = 1;
  CpgcSeqStatus.Data     = 0;
  CpgcErrTestStatus.Data = 0;
  GetSetDis              = 0;
  GetSetEn               = 1;

  if (DQPat == CADB) {
    // With Pattern Generators, we don't need to break the test into multiple loops.
    // This is because the CADB PG has a shift feature, which is programmed in SetupIoTest.
    // If this goes away and we manually rotate the CADB pattern, the NumTests should equal
    // to the number of times we want to rotate the bus.  See MrcCommandSpread()
    NumTests = MrcCommandSpread (MrcData);
    // add one extra cycle to run with DESELECTS disabled
    NumTests += 1;

    // Enable command tri-state, in order to get CADB on Deselects
    for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
      for (Channel = 0; Channel < MaxChannel; Channel++) {
        if ((!MrcChannelExist (MrcData, Controller, Channel)) || IS_MC_SUB_CH (Lpddr, Channel)) {
          // For LPDDR4/5, only program register on even channels.
          continue;
        }
        CurMcChBitMask = (1 << ((Controller * MaxChannel) + Channel));
        if ((CurMcChBitMask & McChBitMask) == 0) {
          continue;
        }
        MrcGetSetMcCh (MrcData, Controller, Channel, GsmMccCmdTriStateDis, ReadFromCache,  &CmdTriStateDisSave[Controller][Channel]);
        GetSetVal = 0;
        MrcGetSetMcCh (MrcData, Controller, Channel, GsmMccCmdTriStateDis, WriteCached, &GetSetVal);
      }
    }

    // Enable CADB on MC
    McCadbEnable (MrcData, TRUE);
  } else if ((DQPat == GaloisMprVa) && Inputs->ReadMprVA) {
    NumTests = 8;
  } else if (DQPat == TurnAroundWR) {
    NumTests = 1;  //AlgoInstructs already setup via SetupIOTest
  } else if (DQPat == TurnAroundODT) {
    NumTests = 4;
  } else if (DQPat == RdRdTA) {
    NumTests = 2;
    for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
      for (Channel = 0; Channel < MaxChannel; Channel++) {
        if ((!MrcChannelExist (MrcData, Controller, Channel)) || IS_MC_SUB_CH (Lpddr, Channel)) {
          continue;
        }
        CurMcChBitMask = (1 << ((Controller * MaxChannel) + Channel));
        if ((CurMcChBitMask & McChBitMask) == 0) {
          continue;
        }
        MrcGetSetMcCh (MrcData, Controller, Channel, GsmMctRDRDsg, ReadFromCache, &GetSetVal);
        tRDRD_sg_Min[Controller][Channel] = (UINT8) GetSetVal; // save the min value allowed
        MrcGetSetMcCh (MrcData, Controller, Channel, GsmMctRDRDdg, ReadFromCache, &GetSetVal);
        tRDRD_dg_Min[Controller][Channel] = (UINT8) GetSetVal; // save the min value allowed
      }
    }
  } else if (DQPat == RdRdTA_All) {
    NumTests = 8;
    for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
      for (Channel = 0; Channel < MaxChannel; Channel++) {
        if ((!MrcChannelExist (MrcData, Controller, Channel)) || IS_MC_SUB_CH (Lpddr, Channel)) {
          continue;
        }
        CurMcChBitMask = (1 << ((Controller * MaxChannel) + Channel));
        if ((CurMcChBitMask & McChBitMask) == 0) {
          continue;
        }
        MrcGetSetMcCh (MrcData, Controller, Channel, GsmMctRDRDsg, ReadFromCache, &GetSetVal);
        tRDRD_sg_Min[Controller][Channel] = (UINT8) GetSetVal; // save the min value allowed
        MrcGetSetMcCh (MrcData, Controller, Channel, GsmMctRDRDdg, ReadFromCache, &GetSetVal);
        tRDRD_dg_Min[Controller][Channel] = (UINT8) GetSetVal; // save the min value allowed
        MrcGetSetMcCh (MrcData, Controller, Channel, GsmMctRDRDdr, ReadFromCache, &GetSetVal);
        tRDRD_dr_Min[Controller][Channel] = (UINT8) GetSetVal; // save the min value allowed
      }
    }
  }

  // Intial values for VA masks
  vmask = BASIC_VA_VICTIM_16;
  amask = BASIC_VA_AGGRESSOR_16;

    // Setup the Pattern Generator for the test.
    // If we are CADB, we setup DQ bus to do VA so we get failures if the address of the command is incorrect.
    // CADB setup moved below to rotate Victim Bit based on the NumTests
    // Thus, we separate the cases from if-else.  StaticPattern and CADB cannot be set at the same time.
    if (DQPat == StaticPattern) {
      // Do nothing, static pattern should be prepared by the caller
    } else if (DQPat != GaloisMprVa) {
      WriteVAPattern (MrcData, McChBitMask, BASIC_VA_VICTIM_16, BASIC_VA_AGGRESSOR_16);
    }

  for (TestIdx = 0; TestIdx < NumTests; TestIdx++) {
    for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
      for (Channel = 0; Channel < MaxChannel; Channel++) {
        CurMcChBitMask = (1 << ((Controller * MaxChannel) + Channel));
        if (!(CurMcChBitMask & McChBitMask) ||
          (IS_MC_SUB_CH (Lpddr, Channel) && ((DQPat == RdRdTA) || (DQPat == RdRdTA_All)))) {
          continue;
        }

        if (DQPat == GaloisMprVa) {
          // Take MC out of MPR Mode
          MrcGetSetMcCh (MrcData, Controller, Channel, GsmMccMprTrainDdrOn, WriteNoCache, &GetSetDis);
          for (Rank = 0; Rank < MAX_RANK_IN_CHANNEL; Rank++) {
            if (!((1 << Rank) & RankMask)) {
              continue; // Skip if both channels empty
            }
            if (!(MrcRankExist (MrcData, Controller, Channel, Rank))) {
              continue;
            }
            // Reprogram DRAM
            MrcIssueMrw (MrcData, Controller, Channel, Rank, mrMR26, 0x5A, TRUE); // LFSR0 Seed
            MrcIssueMrw (MrcData, Controller, Channel, Rank, mrMR27, 0x3C, TRUE); // LFSR1 Seed

            MrcIssueMrw (MrcData, Controller, Channel, Rank, mrMR30, (Inputs->ReadMprVA) ? GaloisMprBitMask[TestIdx] : 0xFF, TRUE); // LFSR Assignnent (0-LFSR0, 1-LFSR1)
          }
          // Place MC in MPR Mode
          MrcGetSetMcCh (MrcData, Controller, Channel, GsmMccMprTrainDdrOn, WriteNoCache, &GetSetEn);
        }

        if ((DQPat == TurnAroundWR) || (DQPat == TurnAroundODT)) {
          // Program which subsequence to run
          // @todo <ICL> convert to CPGC 2.0
//        Offset = OFFSET_CALC_CH (REUT_CH_SEQ_CFG_0_REG, REUT_CH_SEQ_CFG_1_REG, Channel);
//        ReutChSeqCfg.Data = MrcReadCR64 (MrcData, Offset);
//        ReutChSeqCfg.Bits.Subsequence_Start_Pointer = TestIdx;
//        ReutChSeqCfg.Bits.Subsequence_End_Pointer   = TestIdx;
//        MrcWriteCR64 (MrcData, Offset, ReutChSeqCfg.Data);

          // Program RankInc Rate
//          IncRate =
//            (
//              ((DQPat == TurnAroundWR) && ((TestIdx == 0) || (TestIdx == 7))) ||
//              ((DQPat == TurnAroundODT) && ((TestIdx == 0) || (TestIdx == 2)))
//            ) ? 0 : 1;
//            IncScale = 1;
          /**
          Offset = OFFSET_CALC_CH (REUT_CH_SEQ_BASE_ADDR_INC_CTL_0_REG, REUT_CH_SEQ_BASE_ADDR_INC_CTL_1_REG, Channel);
          ReutChSeqBaseAddrIncCtl.Data = MrcReadCR64 (MrcData, Offset);
          ReutChSeqBaseAddrIncCtl.Bits.Rank_Base_Address_Update_Scale = IncScale;
          ReutChSeqBaseAddrIncCtl.Bits.Rank_Base_Address_Update_Rate = IncRate;
          ReutChSeqBaseAddrIncCtl.Bits.Column_Base_Address_Update_Scale = 1;
          ReutChSeqBaseAddrIncCtl.Bits.Column_Base_Address_Update_Rate = IncRate;
          MrcWriteCR64 (MrcData, Offset, ReutChSeqBaseAddrIncCtl.Data);
          **/
        } else if (DQPat == RdRdTA) {
          // Program tRDRD parameter
          GetSetVal = tRDRD_sg_Min[Controller][Channel] + TestIdx;
          MrcGetSetMcCh (MrcData, Controller, Channel, GsmMctRDRDsg, WriteCached, &GetSetVal);
          GetSetVal = tRDRD_dg_Min[Controller][Channel] + TestIdx;
          MrcGetSetMcCh (MrcData, Controller, Channel, GsmMctRDRDdg, WriteCached, &GetSetVal);
        } else if (DQPat == RdRdTA_All) {
          // Program tRDRD for SR and DR
          //  Run 8 tests, Covering tRDRD_sr = 4,5,6,7 and tRDRD_dr = Min,+1,+2,+3
          TurnAroundOffset = (TestIdx % 4);
          GetSetVal = tRDRD_sg_Min[Controller][Channel] + TurnAroundOffset;
          MrcGetSetMcCh (MrcData, Controller, Channel, GsmMctRDRDsg, WriteCached, &GetSetVal);
          GetSetVal = tRDRD_dg_Min[Controller][Channel] + TurnAroundOffset;
          MrcGetSetMcCh (MrcData, Controller, Channel, GsmMctRDRDdg, WriteCached, &GetSetVal);
          GetSetVal = tRDRD_dr_Min[Controller][Channel] + TurnAroundOffset;
          MrcGetSetMcCh (MrcData, Controller, Channel, GsmMctRDRDdr, WriteCached, &GetSetVal);
          // Program RankInc Rate
//          IncRate = (TestIdx > 3)? 0 : 31; // this field + 1
          /**
          Offset = OFFSET_CALC_CH (REUT_CH_SEQ_BASE_ADDR_INC_CTL_0_REG, REUT_CH_SEQ_BASE_ADDR_INC_CTL_1_REG, Channel);
          ReutChSeqBaseAddrIncCtl.Data = MrcReadCR64 (MrcData, Offset);
          ReutChSeqBaseAddrIncCtl.Bits.Rank_Base_Address_Update_Scale = 1;
          ReutChSeqBaseAddrIncCtl.Bits.Rank_Base_Address_Update_Rate = IncRate;
          MrcWriteCR64 (MrcData, Offset, ReutChSeqBaseAddrIncCtl.Data);
          **/
        }
        if ((DQPat == RdRdTA) || (DQPat == RdRdTA_All)) {
          // Must update the XARB bubble injector when TAT values change
          SetTcBubbleInjector (MrcData, Controller, Channel);
        }
      } // for Channel
    }  // for Controller

    if (DQPat == GaloisMprVa) {
      // Reprogram CPGC - Reset starting Seed
      MrcInitCpgcPgMux (MrcData, DataGaloisSeeds, 0, MRC_NUM_MUX_SEEDS);

      // Set VA Mask for Test
      if (Inputs->ReadMprVA) {
        WriteVAPattern (MrcData, McChBitMask, vmask, amask);
      } else {
        WriteVAPattern (MrcData, McChBitMask, 0, 0xFFFF);
      }
    }

    // If we are CADB, we setup DQ bus to do VA so we get failures if the address of the command is incorrect.
    if (DQPat == CADB) {
      if (TestIdx < MrcCommandSpread(MrcData)) {
        SetupCadbVaPat(MrcData, McChBitMask, MrcCommandSpread(MrcData), (UINT8)TestIdx);
      } else {
        // Disable CADB on MC
        McCadbEnable(MrcData, FALSE);
      }
    }
    //###########################################################
    // Start Test and Poll on completion
    //###########################################################

    // IO Reset needed before starting test.
    IoReset(MrcData);

    if (DQPat == CADB) {
      // Start CADB, Do not start for the last cycle (Deselects)
      if (TestIdx < MrcCommandSpread(MrcData)) {
        CadbControl.Data = 0;
        CadbControl.Bits.START_TEST = 1;
        Cadb20ControlRegWrite(MrcData, McChBitMask, CadbControl);
      }
    }

    CpgcSeqCtl.Data = 0;
    if (ClearErrors && (TestIdx == 0)) {
      CpgcSeqCtl.Bits.CLEAR_ERRORS = 1;
      Cpgc20ControlRegWrite (MrcData, McChBitMask, CpgcSeqCtl);
    }

    // Do not combine with CLEAR_ERRORS above because we might use GlobalStart feature
    CpgcSeqCtl.Data = 0;
    CpgcSeqCtl.Bits.START_TEST = 1;
    Cpgc20ControlRegWrite (MrcData, McChBitMask, CpgcSeqCtl);

    // Wait until Channel test done status matches ChBitMask
    do {
      TestDoneStatus = 0;
      for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
        if (MrcControllerExist (MrcData, Controller)) {
          Offset = OFFSET_CALC_CH (MC0_STALL_DRAIN_REG, MC1_STALL_DRAIN_REG, Controller);
          StallDrain.Data = MrcReadCR(MrcData, Offset);
          for (Channel = 0; Channel < MaxChannel; Channel++) {
            CurMcChBitMask = (1 << ((Controller * MaxChannel) + Channel));
            if (((McChBitMask & CurMcChBitMask) != 0) && (!(IS_MC_SUB_CH(Lpddr, Channel)))) {
              IpChannel = LP_IP_CH (Lpddr, Channel);
              ChannelOut = &Outputs->Controller[Controller].Channel[Channel];
              Offset = OFFSET_CALC_MC_CH (MC0_REQ0_CR_CPGC_SEQ_STATUS_REG, MC1_REQ0_CR_CPGC_SEQ_STATUS_REG, Controller, MC0_REQ1_CR_CPGC_SEQ_STATUS_REG, IpChannel);
              CpgcSeqStatus.Data = MrcReadCR(MrcData, Offset);
              if ((CpgcSeqStatus.Bits.TEST_DONE) && (StallDrain.Bits.mc_drained)) {
                TestDoneStatus |= (ChannelOut->CpgcChAssign << (Controller * MaxChannel));
              }
            }
          }
        }
      }
    } while ((TestDoneStatus & McChBitMask) != McChBitMask);
    // Get test results
    for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
      for (Channel = 0; Channel < MaxChannel; Channel++) {
        CurMcChBitMask = (1 << ((Controller * MaxChannel) + Channel));
        if ((McChBitMask & CurMcChBitMask) != 0) {
          if (MrcChannelExist (MrcData, Controller, Channel)) {
            IpChannel = DDR5_IP_CH (Ddr5, Channel);
            Offset = MrcGetTestErrStatOffset (MrcData, Controller, IpChannel);
            CpgcErrTestStatus.Data = MrcReadCR (MrcData, Offset);
            // Channel 0 instance of MCx_CHx_CPGC_ERR_TEST_ERR_STAT reports the aggregate status of all channels.
            // Hence mask out other channels
            if (Ddr5) {
              CpgcErrTestStatus.Bits.ERROR_STATUS &= CPGC_23_ERROR_STATUS_MASK_DDR5;
            } else if (Lpddr) {
              CpgcErrTestStatus.Bits.ERROR_STATUS &= CPGC_23_ERROR_STATUS_MASK_LPDDR;
            } // No need to mask on DDR4 as we have only one channel per MC so there is no aggregation
            ErrorStatus |= ((CpgcErrTestStatus.Bits.ERROR_STATUS != 0) << (Channel + (Controller * MaxChannel)));  // Combine results from all controllers and channels into flat bitmask
          }
        }
      } // for Channel
    } // for Controller

    if (DQPat == CADB) {
      // Stop CADB, no need on the last test cycle (deselects)
      if (TestIdx < MrcCommandSpread(MrcData)) {
        CadbControl.Data = 0;
        CadbControl.Bits.STOP_TEST = 1;
        Cadb20ControlRegWrite(MrcData, McChBitMask, CadbControl);
      }
    }

    if (DQPat == GaloisMprVa) {
      // Change the VA variables for next test iteration
      vmask = vmask << 1;
      amask = ~vmask;
    }

    // For x64 Channels, we can break out as soon as either SubChannel has an error for the channels populated.
    // Same as Error Status mask.
    // Current assumption is SubChannels are run sequentially.  Traffic is only sent on tested sub channel.  If a failure occurs, report it as an error for that Channel.
    // If a Sch is not populated, its Error status is Don't Care.
    // Not Valid (NV)
    // Sc1,Sc0   | 0,0 | 0,1 | 1,1 | 1,0 |
    // Sc1E,Sc0E |-----------------------|
    //    0,0    | NV  |  0  |  0  |  0  |
    //    0,1    | NV  |  1  |  1  |  0  |
    //    1,1    | NV  |  1  |  1  |  1  |
    //    1,0    | NV  |  0  |  1  |  1  |
    //           |-----------------------|
    //} // NumDqRot
  } // NumTests

  if ((DQPat == RdRdTA) || (DQPat == RdRdTA_All)) {
    // Restore original tRDRD value
    for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
      for (Channel = 0; Channel < MaxChannel; Channel++) {
        CurMcChBitMask = (1 << ((Controller * MaxChannel) + Channel));
        if (!(CurMcChBitMask & McChBitMask) || IS_MC_SUB_CH (Lpddr, Channel)) {
          continue;
        }
        GetSetVal = tRDRD_sg_Min[Controller][Channel];
        MrcGetSetMcCh (MrcData, Controller, Channel, GsmMctRDRDsg, WriteCached, &GetSetVal);
        GetSetVal = tRDRD_dg_Min[Controller][Channel];
        MrcGetSetMcCh (MrcData, Controller, Channel, GsmMctRDRDdg, WriteCached, &GetSetVal);
        if (DQPat == RdRdTA_All){
          GetSetVal = tRDRD_dr_Min[Controller][Channel];
          MrcGetSetMcCh (MrcData, Controller, Channel, GsmMctRDRDdr, WriteCached, &GetSetVal);
        }
        // Must update the XARB bubble injector when TAT values change
        //@todo: Update for Ctrl
        SetTcBubbleInjector (MrcData, Controller, Channel);
      }
    }
  }
  if (DQPat == CADB) {
    // Disable CADB on MC
    McCadbEnable (MrcData, FALSE);
    // Restore original command tri-state value
    for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
      for (Channel = 0; Channel < MaxChannel; Channel++) {
        if ((!MrcChannelExist (MrcData, Controller, Channel)) || IS_MC_SUB_CH (Lpddr, Channel)) {
          // For LPDDR4/5, only program register on even channels.
          continue;
        }
        CurMcChBitMask = (1 << ((Controller * MaxChannel) + Channel));
        if ((CurMcChBitMask & McChBitMask) != 0) {
          MrcGetSetMcCh (MrcData, Controller, Channel, GsmMccCmdTriStateDis, WriteCached, &CmdTriStateDisSave[Controller][Channel]);
        }
      }
    }
  }

  return (ErrorStatus & McChBitMask);
}

/**
  Programs REUT to run on the selected physical ranks.

  @param[in] MrcData           - Pointer to MRC global data.
  @param[in] Controller        - Controller to enable.
  @param[in] ch                - Channel to enable.
  @param[in] RankBitMask       - Bit mask of ranks to enable.
  @param[in] SkipRankL2P       - Boolean control over skipping Logical 2 Physical programming for Rank.
  @param[in] RankFeatureEnable - RankFeatureEnable is a bit mask that can enable CKE, Refresh or ZQ
                                 RankFeatureEnable[0] enables Refresh on all non-selected ranks
                                 RankFeatureEnable[1] enables Refresh on all ranks
                                 RankFeatureEnable[2] enables ZQ on all non-selected ranks
                                 RankFeatureEnable[3] enables ZQ on all ranks
                                 RankFeatureEnable[4] enables CKE on all non-selected ranks
                                 RankFeatureEnable[5] enables CKE on all ranks

  @retval Bit mask of channel enabled if rank in the channel exists.
**/
UINT8
SelectReutRanks (
  IN MrcParameters *const MrcData,
  IN const UINT32         Controller,
  IN const UINT32         ch,
  IN UINT32               RankBitMask,
  IN BOOLEAN              SkipRankL2P,
  IN const UINT8          RankFeatureEnable
  )
{
  MrcOutput     *Outputs;
  UINT32  Offset;
  UINT8   En;
  UINT8   rank;
  UINT8   RankCount;
  UINT8   RankCountMinusOne;
  INT64   GetSetVal;
  INT64   GetSetVal2;
  UINT8   OrigRankCount;
  UINT32  IpChannel;
  BOOLEAN Lpddr;
  MC0_CH0_CR_REUT_CH_MISC_REFRESH_CTRL_STRUCT    ReutChMiscRefreshCtrl;
  MC0_CH0_CR_REUT_CH_MISC_ZQ_CTRL_STRUCT         ReutChMiscZqCtrl;
  MC0_REQ0_CR_CPGC2_ADDRESS_SIZE_STRUCT          Cpgc2AddrSize;
  MC0_REQ0_CR_CPGC_SEQ_RANK_L2P_MAPPING_A_STRUCT CpgcSeqRankL2PMappingA;

  Outputs   = &MrcData->Outputs;
  Lpddr     = Outputs->Lpddr;
  IpChannel = LP_IP_CH (Lpddr, ch);

  // Make sure valid rank bit mask for this channel
  RankBitMask &= (UINT32) Outputs->Controller[Controller].Channel[ch].ValidRankBitMask;

//   MRC_DEBUG_MSG (Outputs->Debug, MSG_LEVEL_ERROR, "SelectReutRanks: ch%d, RankBitMask: %d  MrcIntData->McChBitMask = 0x%08X\n", ch, RankBitMask, MrcIntData->McChBitMask);

  // Check if nothing is selected
  if ((RankBitMask & 0xF) == 0) {
    // Disable CPGC / CADB engine on this channel
    // For LPDDR4/5, only program register on even channels.
    return 0;
  } else {
    // Normal case
    // Setup REUT Test to iteration through appropriate ranks during test
    CpgcSeqRankL2PMappingA.Data = 0;
    RankCount                   = 0;
    // Prepare Rank Mapping and Max Rank
    if (!SkipRankL2P) {
      for (rank = 0; rank < MAX_RANK_IN_CHANNEL; rank++) {
        if ((MRC_BIT0 << rank) & RankBitMask) {
          CpgcSeqRankL2PMappingA.Data |= (rank << (MC0_REQ0_CR_CPGC_SEQ_RANK_L2P_MAPPING_A_L2P_RANK0_MAPPING_WID * RankCount));
          RankCount += 1;
        }
      }
      // Write New Rank Mapping and Max Rank
      if (!(IS_MC_SUB_CH (Lpddr, ch))) {
        Offset = OFFSET_CALC_MC_CH (
                  MC0_REQ0_CR_CPGC_SEQ_RANK_L2P_MAPPING_A_REG,
                  MC1_REQ0_CR_CPGC_SEQ_RANK_L2P_MAPPING_A_REG, Controller,
                  MC0_REQ1_CR_CPGC_SEQ_RANK_L2P_MAPPING_A_REG, IpChannel);
        MrcWriteCR (MrcData, Offset, CpgcSeqRankL2PMappingA.Data);
      }
    }
  }

  RankCountMinusOne = RankCount - 1;
  if (!(IS_MC_SUB_CH (Lpddr, ch))) {
    Offset = OFFSET_CALC_MC_CH (
              MC0_REQ0_CR_CPGC2_ADDRESS_SIZE_REG,
              MC1_REQ0_CR_CPGC2_ADDRESS_SIZE_REG, Controller,
              MC0_REQ1_CR_CPGC2_ADDRESS_SIZE_REG, IpChannel);
    Cpgc2AddrSize.Data = MrcReadCR64 (MrcData, Offset);
    OrigRankCount = (UINT8) (Cpgc2AddrSize.Bits.Block_Size_Max_Rank + 1);
    if (OrigRankCount != RankCount) {
      Cpgc2AddrSize.Bits.Block_Size_Max_Rank = RankCountMinusOne;
      Cpgc2AddrSize.Bits.Region_Size_Max_Rank = RankCountMinusOne;
      MrcWriteCR64 (MrcData, Offset, Cpgc2AddrSize.Data);
      Cpgc20AdjustNumOfRanks (MrcData, Controller, ch, OrigRankCount, RankCount);
    }
  }
  if (RankFeatureEnable != 0) {
    if (!(IS_MC_SUB_CH (Lpddr, ch))) {
      // Enable Refresh and ZQ - 0's to the the desired ranks
      En = RankFeatureEnable & 0x3; // Refresh
      ReutChMiscRefreshCtrl.Data                    = 0;
      ReutChMiscRefreshCtrl.Bits.Refresh_Rank_Mask  = MC0_CH0_CR_REUT_CH_MISC_REFRESH_CTRL_Refresh_Rank_Mask_MAX;
      ReutChMiscRefreshCtrl.Bits.Panic_Refresh_Only = 0;

      if (En == 1) {
        ReutChMiscRefreshCtrl.Bits.Refresh_Rank_Mask = ~RankBitMask; // Enable all non-selected ranks
      } else if (En > 1) {
        ReutChMiscRefreshCtrl.Bits.Refresh_Rank_Mask = 0;           // Enable all ranks
      }
      if (En != 0) {
        Offset = OFFSET_CALC_MC_CH (MC0_CH0_CR_REUT_CH_MISC_REFRESH_CTRL_REG, MC1_CH0_CR_REUT_CH_MISC_REFRESH_CTRL_REG, Controller, MC0_CH1_CR_REUT_CH_MISC_REFRESH_CTRL_REG, IpChannel);
        MrcWriteCR (MrcData, Offset, ReutChMiscRefreshCtrl.Data);
      }

      En = (RankFeatureEnable >> 2) & 0x3; // ZQ
      ReutChMiscZqCtrl.Data              = 0;
      ReutChMiscZqCtrl.Bits.ZQ_Rank_Mask = MC0_CH0_CR_REUT_CH_MISC_ZQ_CTRL_ZQ_Rank_Mask_MAX;
      ReutChMiscZqCtrl.Bits.Always_Do_ZQ = 1;
      if (En == 1) {
        ReutChMiscZqCtrl.Bits.ZQ_Rank_Mask = ~RankBitMask;
      } else if (En > 1) {
        ReutChMiscZqCtrl.Bits.ZQ_Rank_Mask = 0; // Enable all ranks
      }
      if (En != 0) {
        Offset = OFFSET_CALC_MC_CH (MC0_CH0_CR_REUT_CH_MISC_ZQ_CTRL_REG, MC1_CH0_CR_REUT_CH_MISC_ZQ_CTRL_REG, Controller, MC0_CH1_CR_REUT_CH_MISC_ZQ_CTRL_REG, IpChannel);
        MrcWriteCR (MrcData, Offset, ReutChMiscZqCtrl.Data);
      }
    }

    // Enable CKE ranks - 1's to enable desired ranks
    En = (RankFeatureEnable >> 4) & 0x3;
    GetSetVal = 0;
    GetSetVal2 = MRC_INT64_MAX;
    if (En == 1) {
      GetSetVal = ~RankBitMask; // Enable all non-selected ranks
      GetSetVal2 = ~RankBitMask;
    } else if (En > 1) {
      GetSetVal = MRC_INT64_MAX; // Enable all ranks.
      GetSetVal2 = MRC_INT64_MAX;
    }
    if (En != 0) {
      MrcGetSetMcCh (MrcData, Controller, ch, GsmMccCkeOverride, WriteNoCache, &GetSetVal);
      MrcGetSetMcCh (MrcData, Controller, ch, GsmMccCkeOn, WriteNoCache, &GetSetVal2);
    }
  }

  return (UINT8) (MRC_BIT0 << ((Controller * Outputs->MaxChannels) + ch));
}

/**
  Programs REUT to run on the selected physical ranks for all channels in the system.

  @param[in] MrcData           - Pointer to MRC global data.
  @param[in] RankBitMask       - Bit mask of ranks to enable.
  @param[in] SkipRankL2P       - Boolean control over skipping Logical 2 Physical programming for Rank.
  @param[in] RankFeatureEnable - RankFeatureEnable is a bit mask that can enable CKE, Refresh or ZQ
                                 RankFeatureEnable[0] enables Refresh on all non-selected ranks
                                 RankFeatureEnable[1] enables Refresh on all ranks
                                 RankFeatureEnable[2] enables ZQ on all non-selected ranks
                                 RankFeatureEnable[3] enables ZQ on all ranks
                                 RankFeatureEnable[4] enables CKE on all non-selected ranks
                                 RankFeatureEnable[5] enables CKE on all ranks

  @retval Bit mask of all channel in the system for the selected rank.
**/
UINT8
SelectReutRanksAll (
  IN MrcParameters *const MrcData,
  IN UINT8                RankBitMask,
  IN BOOLEAN              SkipRankL2P,
  IN const UINT8          RankFeatureEnable
  )
{
  UINT8       Controller;
  UINT8       Channel;
  UINT8       McChBitMask;
  MrcOutput   *Outputs;

  Outputs     = &MrcData->Outputs;
  McChBitMask = 0;

  for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
    for (Channel = 0; Channel < Outputs->MaxChannels; Channel++) {
      McChBitMask |= SelectReutRanks (MrcData, Controller, Channel, RankBitMask, SkipRankL2P, RankFeatureEnable);
    }
  }

  return McChBitMask;
}

/**
  Returns the index into the array MarginResult in the MrcOutput structure.

  @param[in] ParamV - Margin parameter

  @retval One of the following values: LastRxV(0), LastRxT (1), LastTxV(2), LastTxT (3), LastRcvEna (4),
                                       LastWrLevel (5), LastCmdT (6), LastCmdV (7)
**/
UINT8
GetMarginResultType (
  IN const UINT8 ParamV
  )
{
  switch (ParamV) {
    case WrV:
    case WrFan2:
    case WrFan3:
      return LastTxV;

    case WrT:
    case WrTLp4:
    case WrTLp5:
    case WrTDdr5:
      return LastTxT;

    case RdV:
    case RdFan2:
    case RdFan3:
      return LastRxV;

    case RdT:
      return LastRxT;

    case RcvEna:
    case RcvEnaX:
      return LastRcvEna;

    case WrLevel:
      return LastWrLevel;

    case CmdT:
      return LastCmdT;

    case CmdV:
      return LastCmdV;

    default:
      //MRC_DEBUG_MSG (Debug, MSG_LEVEL_ERROR, "GetMarginByte: Unknown Margin Parameter\n");
      break;
  }

  return 0; // Return LastRxV to point to the beginning of the array
}

/*
1D Margin Types:
RcvEn:   Shifts just RcvEn.  Only side effect is it may eat into read dq-dqs for first bit of burst
RdT:     Shifts read DQS timing, changing where DQ is sampled
WrT:     Shifts write DQ timing, margining DQ-DQS timing
WrDqsT:  Shifts write DQS timing, margining both DQ-DQS and DQS-CLK timing
RdV:     Shifts read Vref voltage for DQ only
WrV:     Shifts write Vref voltage for DQ only
WrLevel: Shifts write DQ and DQS timing, margining only DQS-CLK timing
WrTBit:  Shifts write DQ per bit timing.
RdTBit:  Shifts read DQ per bit timing.
RdVBit:  Shifts read DQ per bit voltage.

2D Margin Types (Voltage, Time)
RdFan2:  Margins both RdV and RdT at { (off, -2/3*off),  (off, 2/3*off) }
WrFan2:  Margins both WrV and WrT at { (off, -2/3*off),  (off, 2/3*off) }
RdFan3:  Margins both RdV and RdT at { (off, -2/3*off),  (5/4*off, 0),  (off, 2/3*off)  }
WrFan3:  Margins both WrV and WrT at { (off, -2/3*off),  (5/4*off, 0),  (off, 2/3*off)  }
*/
/**
  This function Reads MrcData structure and finds the minimum last recorded margin for param.
  Searches across all bytes and ranks in RankMask.

  @param[in]     MrcData         - Include all MRC global data.
  @param[in,out] MarginResult    - Data structure with the latest margin results.
  @param[in]     Param           - Defines the margin type
  @param[in]     RankMask        - Condenses down the results from multiple ranks
  @param[in]     ResultRank      - This rank will keep the results from multiple ranks

  @retval mrcWrongInputParameter if a bad Param is passed in, otherwise mrcSuccess.
**/
MrcStatus
GetMarginCh (
  IN     MrcParameters *const MrcData,
  IN OUT UINT16               MarginResult[MAX_RESULT_TYPE][MAX_RANK_IN_CHANNEL][MAX_CONTROLLER][MAX_CHANNEL][MAX_SDRAM_IN_DIMM][MAX_EDGES],
  IN     const UINT8          Param,
  IN     const UINT8          RankMask,
  IN     const UINT8          ResultRank
  )
{
  MrcOutput *Outputs;
  UINT16    *Margin1;
  UINT16    *Margin2;
  UINT32    ResultType;
  UINT32    Controller;
  UINT32    Channel;
  UINT32    Rank;
  UINT32    Byte;
  UINT32    Edge;
  UINT8     Scale;

  Outputs = &MrcData->Outputs;
  switch (Param) {
    case CmdV:
    case WrV:
    case WrT:
    case WrTLp4:
    case WrTLp5:
    case WrTDdr5:
    case WrLevel:
    case RdV:
    case RdT:
    case RcvEna:
    case RcvEnaX:
      Scale = 10;
      break;

    case WrFan2:
    case WrFan3:
    case RdFan2:
    case RdFan3:
      Scale = 21 / 3;
      break;

    default:
      MRC_DEBUG_MSG (&MrcData->Outputs.Debug, MSG_LEVEL_ERROR, "GetMarginCh: Unknown Margin Parameter\n");
      return mrcWrongInputParameter;
  }

  ResultType = GetMarginResultType (Param);
  ResultType = MIN (MAX_RESULT_TYPE - 1, ResultType);
  for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
    for (Channel = 0; Channel < Outputs->MaxChannels; Channel++) {
      if (MrcChannelExist (MrcData, Controller, Channel)) {
        Margin2 = &MarginResult[ResultType][ResultRank][Controller][Channel][0][0];
        for (Byte = 0; Byte < Outputs->SdramCount; Byte++) {
          for (Rank = 0; Rank < MAX_RANK_IN_CHANNEL; Rank++) {
            if (!MrcRankExist (MrcData, Controller, Channel, Rank)) {
              continue;
            }
            if ((1 << Rank) & RankMask) {
              Margin1 = &MarginResult[ResultType][Rank][Controller][Channel][Byte][0];
              for (Edge = 0; Edge < MAX_EDGES; Edge++) {
                if (Margin2[Edge] > Margin1[Edge]) {
                  Margin2[Edge] = Margin1[Edge];
                }
              } // Edge
            }
          } // Rank
        } // Byte
        // Scale results as needed
        for (Edge = 0; Edge < MAX_EDGES; Edge++) {
          Margin2[Edge] = (Margin2[Edge] * Scale) / 10;
        } // Edge
      }
    } // Channel
  } // Controller

  return mrcSuccess;
}


/**
  This function Reads MrcData structure and finds the minimum last recorded margin for param
  Searches across all bytes and ranks in RankMask

  @param[in]     MrcData         - Include all MRC global data.
  @param[in,out] MarginResult    - Data structure with the latest margin results.
  @param[in]     Param           - Defines the margin type
  @param[in]     Ranks           - Condenses down the results from multiple ranks

  @retval mrcWrongInputParameter if a bad Param is passed in, otherwise mrcSuccess.
**/
MrcStatus
GetPdaMargins (
  IN     MrcParameters *const MrcData,
  IN OUT UINT16               MarginResult[MAX_RESULT_TYPE][MAX_RANK_IN_CHANNEL][MAX_CONTROLLER][MAX_CHANNEL][MAX_SDRAM_IN_DIMM][MAX_EDGES],
  IN     const UINT8          Param,
  IN     const UINT8          Ranks
  )
{
  MrcOutput     *Outputs;
  MrcChannelOut *ChannelOut;
  MrcRankOut    *RankOut;
  MrcDimmOut    *DimmOut;
  UINT16        *Margin1;
  UINT8         ResultType;
  UINT8         Controller;
  UINT8         Channel;
  UINT8         Rank;
  UINT8         Device;
  UINT8         Edge;
  UINT8         MrIndex;
  UINT8         NumDevices;
  INT16         CurrentOff;
  INT16         GlobalCenter;
  INT16         MarginOffset;
  BOOLEAN       Ddr4;
  BOOLEAN       Ddr5;

  Outputs = &MrcData->Outputs;
  Ddr4    = (Outputs->DdrType == MRC_DDR_TYPE_DDR4);
  Ddr5    = (Outputs->DdrType == MRC_DDR_TYPE_DDR5);

  MrIndex = Ddr4 ? mrIndexMR6 : mrIndexMR10;

  ResultType = GetMarginResultType (Param);
  for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
    for (Channel = 0; Channel < Outputs->MaxChannels; Channel++) {
      ChannelOut = &Outputs->Controller[Controller].Channel[Channel];
      if (!MrcChannelExist (MrcData, Controller, Channel)) {
        continue;
      }
      if (Ddr5 && (ChannelOut->Mr10PdaEnabled == FALSE)) {
        continue;
      }
      for (Rank = 0; Rank < MAX_RANK_IN_CHANNEL; Rank++) {
        if ((MrcRankExist (MrcData, Controller, Channel, Rank) & Ranks) == 0) {
          continue;
        }
        RankOut = &ChannelOut->Dimm[Rank / 2].Rank[Rank % 2];
        if (Ddr4) {
          GlobalCenter = (INT16) MrcVrefDqToOffsetDdr4 ((UINT8) (RankOut->MR[MrIndex] & 0x7F));
          NumDevices = Outputs->SdramCount;
        } else { // Ddr5
          GlobalCenter = (INT16) MrcVrefToOffsetDdr5 ((UINT8) (RankOut->MR[MrIndex]));
          DimmOut = &ChannelOut->Dimm[RANK_TO_DIMM_NUMBER(Rank)];
          // Channel Width (ddr5 = 32 Bytes) / SdramWidth (x8 or x16)
          NumDevices = DimmOut->PrimaryBusWidth / DimmOut->SdramWidth;
          if ((DimmOut->SdramWidth == 8) && DimmOut->EccSupport) {
            NumDevices++;
          }
        }
        for (Device = 0; Device < NumDevices; Device++) {
          if (Ddr4) {
            CurrentOff = (INT16) MrcVrefDqToOffsetDdr4 (RankOut->DdrPdaVrefDq[Device] & 0x7F);
          } else { // Ddr5
            CurrentOff = (INT16) MrcVrefToOffsetDdr5 (RankOut->DdrPdaVrefDq[Device]);
          }
          MarginOffset = 10 * (CurrentOff - GlobalCenter);
          Margin1 = &MarginResult[ResultType][Rank][Controller][Channel][Device][0];
          for (Edge = 0; Edge < MAX_EDGES; Edge++) {
            if (Edge == 0) {
              Margin1[Edge] += MarginOffset;
            } else {
              Margin1[Edge] -= MarginOffset;
            }
            if ((INT16) Margin1[Edge] < 0) {
              Margin1[Edge] = 0;
            }
          } // for Edge
        } // for Byte
      } // for Rank
    } // for Channel
  } // for Controller

  return mrcSuccess;
}


/**
  Use this function to retrieve the last margin results from MrcData

  @param[in]     MrcData         - Include all MRC global data.
  @param[in,out] MarginResult    - Data structure with the latest margin results.
  @param[in]     Param           - Defines the margin type
  @param[in]     RankIn          - Which rank of the host structure you want the result returned on
  @param[in]     Ranks           - Condenses down the results from multiple ranks

  @retval MarginResult structure has been updated if MrcStatus returns mrcSuccess.
  @retval Otherwise, mrcWrongInputParameter is returned if an incorrect Param is passed in.
**/
MrcStatus
GetMarginByte (
  IN     MrcParameters *const MrcData,
  IN OUT UINT16               MarginResult[MAX_RESULT_TYPE][MAX_RANK_IN_CHANNEL][MAX_CONTROLLER][MAX_CHANNEL][MAX_SDRAM_IN_DIMM][MAX_EDGES],
  IN     const UINT8          Param,
  IN     const UINT8          RankIn,
  IN     const UINT8          Ranks
  )
{
  MrcOutput *Outputs;
  UINT16    *Margin1;
  UINT16    *Margin2;
  UINT32    Controller;
  UINT32    Channel;
  UINT8     ResultType;
  UINT8     Rank;
  UINT8     Byte;
  UINT8     Edge;
  UINT8     Scale;

  Outputs = &MrcData->Outputs;
  switch (Param) {
    case WrV:
    case WrT:
    case WrTLp4:
    case WrTLp5:
    case WrTDdr5:
    case WrLevel:
    case RdV:
    case RdT:
    case RcvEna:
    case RcvEnaX:
    case CmdT:
    case CmdV:
      Scale = 10;
      break;

    case WrFan2:
    case WrFan3:
    case RdFan2:
    case RdFan3:
      Scale = 21 / 3;
      break;

    default:
      MRC_DEBUG_MSG (&MrcData->Outputs.Debug, MSG_LEVEL_ERROR, "GetMarginByte: Unknown Margin Parameter\n");
      return mrcWrongInputParameter;
  }

  ResultType = GetMarginResultType (Param);
  ResultType = MIN (ResultType, MAX_RESULT_TYPE - 1);
  for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
    for (Channel = 0; Channel < MAX_CHANNEL; Channel++) {
      if (MrcChannelExist (MrcData, Controller, Channel)) {
        for (Byte = 0; Byte < Outputs->SdramCount; Byte++) {
          for (Rank = 0; Rank < MAX_RANK_IN_CHANNEL; Rank++) {
            if (MrcRankExist (MrcData, Controller, Channel, Rank) & Ranks) {
              Margin1 = &MarginResult[ResultType][RankIn][Controller][Channel][Byte][0];
              Margin2 = &MarginResult[ResultType][Rank][Controller][Channel][Byte][0];
              for (Edge = 0; Edge < MAX_EDGES; Edge++) {
                if (Margin1[Edge] > Margin2[Edge]) {
                  Margin1[Edge] = Margin2[Edge];
                }
              }
            }
          }
          // Scale results as needed
          Margin1 = &MarginResult[ResultType][RankIn][Controller][Channel][Byte][0];
          for (Edge = 0; Edge < MAX_EDGES; Edge++) {
            Margin1[Edge] = (Margin1[Edge] * Scale) / 10;
          }
        }
      }
    }
  }

  return mrcSuccess;
}

/**
  This function is use to "unscale" the MrcData last margin point
  GetMarginByte will scale the results for FAN margin
  This will unscale the results such that future tests start at the correct point

  @param[in]     MrcData      - Include all MRC global data.
  @param[in,out] MarginResult - Input array to be unscaled.
  @param[in]     Param        - Defines the margin type for proper scale selection.
  @param[in]     Rank         - Which rank of the host structure to work on

  @retval mrcSuccess
**/
MrcStatus
ScaleMarginByte (
  IN     MrcParameters *const MrcData,
  IN OUT UINT16               MarginResult[MAX_RESULT_TYPE][MAX_RANK_IN_CHANNEL][MAX_CONTROLLER][MAX_CHANNEL][MAX_SDRAM_IN_DIMM][MAX_EDGES],
  IN     const UINT8          Param,
  IN     const UINT8          Rank
  )
{
  MrcOutput *Outputs;
  UINT16    *Margin;
  UINT8     ResultType;
  UINT8     Controller;
  UINT8     Channel;
  UINT8     Byte;
  UINT8     Edge;

  // Calculate scale parameter based on param
  // Leave room for expansion in case other params needed to be scaled
  Outputs = &MrcData->Outputs;
  if ((Param == RdFan2) || (Param == RdFan3) || (Param == WrFan2) || (Param == WrFan3)) {
    ResultType = GetMarginResultType (Param);
    ResultType = MIN (MAX_RESULT_TYPE - 1, ResultType);
    for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
      for (Channel = 0; Channel < Outputs->MaxChannels; Channel++) {
        if (MrcChannelExist (MrcData, Controller, Channel)) {
          for (Byte = 0; Byte < Outputs->SdramCount; Byte++) {
            Margin = &MarginResult[ResultType][Rank][Controller][Channel][Byte][0];
            for (Edge = 0; Edge < MAX_EDGES; Edge++) {
              Margin[Edge] = (Margin[Edge] * 15) / 10;
            }
          }
        }
      }
    }
  }

  return mrcSuccess;
}

/**
  This function will return the rank used to store the results based on the Rank Mask passed in.
  Results will be stored in the first Rank existing in the RankMask.

  @param[in]  MrcData  - Include all MRC global data.
  @param[in]  RankMask - Bit mask of Ranks being margined.

  @retval mrcSuccess
**/
UINT8
GetRankToStoreResults (
  IN      MrcParameters *const MrcData,
  IN      UINT16               RankMask
  )
{
  UINT8              Rank;
  UINT8              RankLoop;
  MrcOutput         *Outputs;

  Outputs = &MrcData->Outputs;

  Rank = 0;
  if (RankMask == 0xFF) {
    return Rank;
  }
  // We return results on first available rank and RankIn is RankMask.
  for (RankLoop = 0; RankLoop < MAX_RANK_IN_CHANNEL; RankLoop++) {
    if ((1 << RankLoop) & RankMask & Outputs->ValidRankMask) {
    Rank = RankLoop;
    break;
    }
  }
  return Rank;
}

/**
  This function checks to see if the margin parameter is a rank based parameter.  These tend to be
  things which are controlled on the DRAM Mode Register side.

  @param[in]  MrcData - Pointer to MRC global data.
  @param[in]  Param   - The parameter to check.  Must be from MRC_MarginTypes.

  @retval BOOLEAN - TRUE if it is a rank based margin parameter.  Otherwise FALSE.
**/
BOOLEAN
IsPerRankMarginParam (
  IN MrcParameters *const MrcData,
  IN UINT8                Param
  )
{
  BOOLEAN   PerRankMp;

  switch (Param) {
    case WrV:
    case WrFan2:
    case WrFan3:
    case CmdV:          // CmdV is via MR in LP4/LP5/DDR5, and it is per DIMM in DDR4, so treat is as per-rank as well
      PerRankMp = TRUE;
      break;

    default:
      PerRankMp = FALSE;
      break;
  }

  return PerRankMp;
}

/**
  This function is a helper function for ChangeMargin.
  It is used to resolve inconsistent interface of ChaneMargin which interprets rankIn as a rank number (0, 1, 2, 3,...)
  and in other cases as a rank mask.
  This function will receive rankIn and additional parameters which are required in order to determine if rankIn should
  be interpreted as a rank mask or as a rank number, and return the rank mask or 0 if not a rank mask.

  @param[in,out] MrcData      - Include all MRC global data.
  @param[in]     Param        - Includes parameter(s) to change including two dimensional.
  @param[in]     RankIn       - rankIn from ChangeMargin
  @param[in]     EnMultiCast  - To enable Multicast (broadcast) or single register mode

  @retval RankMask - the RankMask or 0 if not a RankMask
**/
UINT8
CalcRankMask (
  IN OUT MrcParameters *const MrcData,
  IN     const UINT8          Param,
  IN     const UINT8          RankIn,
  IN     const UINT8          EnMultiCast
  )
{
  BOOLEAN PerRankControl;
  UINT8   RankMask;

  PerRankControl = IsPerRankMarginParam (MrcData, Param);
  if (PerRankControl) {
    if ((EnMultiCast == 1) && (RankIn == 0)) {
      // If EnMultiCast used and RankMask is 0 assume Rank Multicast
      RankMask = 0xF;
    } else {
      // If RankMask is not 0 do not override it
      RankMask = RankIn;
    }
  } else {
    // For non controllable per rank parameter don't use RankMask
    RankMask = 0;
  }

  return RankMask;
}

/**
  This function is a helper function for ChangeMargin.
  This function forms the correct Channel Mask based on the Margin Parameter, Channel, and Multicast settings.

  @param[in,out] MrcData      - Include all MRC global data.
  @param[in]     Param        - Includes parameter(s) to change including two dimensional.
  @param[in]     Controller   - 0-based Controller Index.
  @param[in]     Channel      - 0-based Channel Index.
  @param[in]     EnMultiCast  - To enable Multicast (broadcast) or single register mode

  @retval UINT16 - Channel Bit Mask.
**/
UINT16
CalcMcChannelMask (
  IN OUT MrcParameters *const MrcData,
  IN     const UINT8          Param,
  IN           UINT8          Controller,
  IN           UINT8          Channel,
  IN     const UINT8          EnMultiCast
  )
{
  MrcOutput         *Outputs;
  UINT16             McChannelMask;
  UINT16             MaxChannels;

  Outputs = &MrcData->Outputs;
  MaxChannels = Outputs->MaxChannels;

  McChannelMask = 0;
  if ((Param == CmdV) || (Param == WrV)) {
    if (EnMultiCast) {
      for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
        for (Channel = 0; Channel < MaxChannels; Channel++) {
          McChannelMask |= (1 << ((Controller) * (MaxChannels) + (Channel)));
        }
      }
    } else {
      McChannelMask = (1 << ((Controller) * (MaxChannels) + (Channel)));
    }
  }

  return McChannelMask;
}

/**
  This function is used by most margin search functions to change the underlying margin parameter.
  This function allows single search function to be used for different types of margins with minimal impact.
  It provides multiple different parameters, including 2D parameters like Read or Write FAN.
  It can work in either MultiCast or single register mode.

  @param[in,out] MrcData       - Include all MRC global data.
  @param[in]     param         - Includes parameter(s) to change including two dimensional.
  @param[in]     value0        - Selected value to program margin param to
  @param[in]     value1        - Selected value to program margin param to in 2D mode (FAN mode)
  @param[in]     EnMultiCast   - To enable Multicast (broadcast) or single register mode
  @param[in]     Controller    - Desired Memory Controller
  @param[in]     Channel       - Desired Channel
  @param[in]     rankIn        - Desired Rank - only used for the RxTBit, TxTBit, and RdVBit settings, and to propagate RdVref, for Ddr4 WrV we need to pass rank bit mask
  @param[in]     byte          - Desired byte offset register. For DDR4 PDA byte is interpreted as byte mask
  @param[in]     bitIn         - Desired bit offset
  @param[in]     UpdateMrcData - Used to decide if MRC host must be updated
  @param[in]     SkipWait      - Used to skip wait until all channel are done

  @retval MrcStatus -  if succeeded, return mrcSuccess
**/
MrcStatus
ChangeMargin (
  IN OUT MrcParameters *const MrcData,
  IN     const UINT8          param,
  IN     const INT32          value0,
  IN     const INT32          value1,
  IN     const UINT8          EnMultiCast,
  IN     const UINT8          Controller,
  IN     const UINT8          Channel,
  IN     const UINT8          rankIn,
  IN     const UINT16         byte,
  IN     const UINT8          bitIn,
  IN     const UINT8          UpdateMrcData,
  IN     const UINT8          SkipWait
  )
{
  // Programs margin param to the selected value0
  // If param is a 2D margin parameter (ex: FAN), then it uses both value0 and value1
  //    For an N point 2D parameter, value1 can be an integer from 0 to (N-1)
  //    For per bit timing parameter, value1 is the sign of the shift
  // param = {0:RcvEna, 1:RdT, 2:WrT, 3: WrDqsT, 4:RdV, 5:WrV, 6:WrLevel,
  //               7:WrTBox, 8:WrTBit, 9:RdTBit, 10:RdVBit, 13:CmdV,
  //              16:RdFan2, 17:WrFan2, 32:RdFan3, 33:WrFan3}
  // Note: For Write Vref, the trained value and margin register are the same
  // Note: rank is only used for the RxTBit, TxTBit, and RdVBit settings, and to propagate RdVref
  // Note: PerBit Settings (WrTBit, RdTBit, RdVBit) provide all 8 offsets in value0

  MrcDebug          *Debug;
  MrcOutput         *Outputs;
  MrcControllerOut  *ControllerOut;
  MrcChannelOut     *CurrentChannelOut;
  MrcStatus         Status;
  GSM_GT            Group[MRC_CHNG_MAR_GRP_NUM];
  INT64             GetSetVal[MRC_CHNG_MAR_GRP_NUM];
  UINT32            GsmMode;
  UINT32            CurrentMc;
  UINT32            McStart;
  UINT32            McEnd;
  UINT32            ChannelStart;
  UINT32            ChannelEnd;
  UINT32            McChannelMask;
  UINT32            RankStart;
  UINT32            RankEnd;
  UINT32            ByteStart;
  UINT32            ByteEnd;
  UINT32            CurrentCh;
  UINT32            CurrentRank;
  UINT32            CurrentByte;
  UINT32            GrpIdx;
  UINT32            PiCenter;
  UINT8             BitValue;
  UINT8             rank;
  UINT8             Rank;
  UINT8             bit;
  UINT16            Max0;
  UINT16            MaxT;
  INT32             sign;
  INT32             v0;
  INT32             v0Input;
  INT32             v1;
  INT32             Min;
  BOOLEAN           UpdateGrp[MRC_CHNG_MAR_GRP_NUM];
  BOOLEAN           PdaMode;
  BOOLEAN           Ddr4;
  BOOLEAN           Ddr5;
  UINT8             RankMask;
  UINT16            DeviceMask;

  Status            = mrcSuccess;
  Outputs           = &MrcData->Outputs;
  Debug             = &Outputs->Debug;
  Ddr4              = (Outputs->DdrType == MRC_DDR_TYPE_DDR4);
  Ddr5              = (Outputs->DdrType == MRC_DDR_TYPE_DDR5);
  RankMask          = CalcRankMask (MrcData, param, rankIn, EnMultiCast);
  // Used for CMDV and WRV
  McChannelMask     = CalcMcChannelMask (MrcData, param, Controller, Channel, EnMultiCast);
  PdaMode           = FALSE;
  DeviceMask        = 1;
  GsmMode           = (UpdateMrcData) ? ForceWriteCached : ForceWriteUncached;
  for (v0 = 0; v0 < MRC_CHNG_MAR_GRP_NUM; v0++) {
    UpdateGrp[v0] = FALSE;
    Group[v0]     = GsmGtMax;
    GetSetVal[v0] = 0;
  }
  if (EnMultiCast == 0) {
    McStart = Controller;
    McEnd = Controller + 1;
    ChannelStart = Channel;
    ChannelEnd = Channel + 1;
    ByteStart = (UINT8) byte;
    ByteEnd = (UINT8) byte + 1;
  } else {
    McStart = 0;
    McEnd = MAX_CONTROLLER;
    ChannelStart = 0;
    ChannelEnd = Outputs->MaxChannels;
    ByteStart = 0;
    ByteEnd = Outputs->SdramCount;
  }
  // Pre-Process the margin numbers
  MaxT   = MAX_POSSIBLE_TIME;   // Maximum value for Time
  Max0   = 0;

  if ((param < RdV) || (param == WrLevel) || (param == RcvEnaX) || (param == RdTP) || (param == RdTN)) {
    // RcvEna, RdT, WrT, WrDqsT, WrLevel
    Max0 = MaxT;
  } else if ((param == WrTBit) || (param == RdTBit) || (param == RdVBit) || (param == WrTLp4) || (param == WrTDdr5)) {
    Max0 = 0xFF;
  } else if ((param == RdV) || (param == RdFan2) || (param == RdFan3)) {
    Max0 = GetVrefOffsetLimits (MrcData, param); // Vref for RdV and RdFan modes
  } else if (param == WrTLp5) {
    MrcGetSetLimits (MrcData, TxDqDelay, NULL, &GetSetVal[0], NULL);
    Max0 = (UINT16) GetSetVal[0];
  }
  // Pre-Process the margin numbers.  Calculate 2D points based on FAN slopes
  v0    = value0;
  sign  = (2 * value1 - 1);

  // For Fan3, optimize point orders to minimize Vref changes and # of tests required
  if (param >= RdFan3) {
    //   RdFan3, WrFan3
    sign = ((3 * value1 - 5) * value1) / 2;   // Translates to {0:0, 1:-1, 2:+1}
    if (value1 == 0) {
      v0 = (5 * value0) / 4;
    }
  }

  v1 = (sign * value0) / 3;

  // Value parameter checking is handled within MrcUpdateVref for WrV and CmdV
  if ((param != WrV) && (param != WrFan2) && (param != WrFan3) && (param != CmdV)) {
    Min = (-1) - Max0;
    v0 = RANGE (v0, Min, Max0);
  }

  Min = (-1) - MaxT;
  v1 = RANGE (v1, Min, MaxT);

  // Rank = 0xFF - all rank , only apply to WrV with DDR4
  // rank param only apply for RcvEnX,and WrV in DDR4
  // DDR4 will use RanBitMask only to support per dimm optimization
  rank          = (rankIn >= MAX_RANK_IN_CHANNEL) ? 0 : rankIn;

  switch (param) {
    case RcvEna:
      UpdateGrp[0]  = TRUE;
      GetSetVal[0]  = v0;
      Group[0]      = RecEnOffset;
      break;

    case RdT:
    case RdTN:
    case RdTP:
      UpdateGrp[0]  = TRUE;
      GetSetVal[0]  = v0;
      Group[0]      = RxDqsOffset;
      break;

    case WrT:
      UpdateGrp[0]  = TRUE;
      GetSetVal[0]  = v0;
      Group[0]      = TxDqOffset;
      break;

    case WrDqsT:
      UpdateGrp[0]  = TRUE;
      GetSetVal[0]  = v0;
      Group[0]      = TxDqsOffset;
      break;

    case RdV:
      // IMPORTANT NOTE: WriteOffsetUncached assumes that the current CR value resides in the register cache.
      // It is a caller responsibility to use MrcReadParamIntoCache(RdV) to bring current RxVref into CR cache
      // at the beginning of an algorithm that uses ChangeMargin to sweep RdV.
      GetSetVal[0]  = v0;
      for (CurrentMc = McStart; CurrentMc < McEnd; CurrentMc++) {
        for (CurrentCh = ChannelStart; CurrentCh < ChannelEnd; CurrentCh++) {
          if (MrcChannelExist (MrcData, CurrentMc, CurrentCh)) {
            for (CurrentByte = ByteStart; CurrentByte < ByteEnd; CurrentByte++) {
              MrcGetSetChStrb (MrcData, CurrentMc, CurrentCh, CurrentByte, RxVref, GsmMode | WriteOffsetUncached, &GetSetVal[0]);
            } // CurrentByte
          } // MrcChannelExist
        } // CurrentCh
      } // CurrentMc

      break;

    case RcvEnaX:
      // IMPORTANT NOTE: WriteOffsetUncached assumes that the current CR value resides in the register cache.
      // It is a caller responsibility to use MrcReadParamIntoCache(RcvEnaX) to bring current RecEnDelay into CR cache
      // at the beginning of an algorithm that uses ChangeMargin to sweep RcvEnaX.
      v0Input = v0; // save the input offset (after min/max truncate)
      for (CurrentMc = McStart; CurrentMc < McEnd; CurrentMc++) {
        for (CurrentCh = ChannelStart; CurrentCh < ChannelEnd; CurrentCh++) {
          if (MrcChannelExist (MrcData, CurrentMc, CurrentCh)) {
            v0 = v0 * 4;
            for (CurrentByte = ByteStart; CurrentByte < ByteEnd; CurrentByte++) {
              for (Rank = 0; Rank < MAX_RANK_IN_CHANNEL; Rank++) {
                if (MrcRankExist (MrcData, CurrentMc, CurrentCh, Rank)) {
                  // the assumption is that we are @ 1 Qclk before edge
                  GetSetVal[0] = v0;
                  MrcGetSetStrobe (
                    MrcData,
                    CurrentMc,
                    CurrentCh,
                    Rank,
                    CurrentByte,
                    RecEnDelay,
                    GsmMode | WriteOffsetUncached,
                    &GetSetVal[0]
                    );
                }
              }
            }
          } // if channel exists
          v0 = v0Input; // restore v0 for next channel
        } // for CurrentCh
      } // for CurrentMc
      break;

    case WrTLp5:
    case WrTLp4:
    case WrTDdr5:
      if ((param == WrTLp4) || (param == WrTDdr5)) {
        // Determine the number of PI ticks to get 500ps.  TxDqs + 500ps + (32 for Gear1, 96 for Gear2) Pi ticks is the center point: v0 = 0;
        // Ddr5: TxDqs + tDQS2DQ + (32 for Gear 1, 96 for Gear 2) Pi ticks is the center point: v0 = 0;
        MrcGetTdqs2dqCenter (MrcData, &PiCenter);
        PiCenter = MrcFemptoSec2PiTick (MrcData, PiCenter);
      } else {
        //LP5
        PiCenter = 0;
      }
      for (CurrentMc = McStart; CurrentMc < McEnd; CurrentMc++) {
        for (CurrentCh = ChannelStart; CurrentCh < ChannelEnd; CurrentCh++) {
          if (MrcRankExist (MrcData, CurrentMc, CurrentCh, rankIn)) {
            for (CurrentByte = ByteStart; CurrentByte < ByteEnd; CurrentByte++) {
              if ((param == WrTLp4) || (param == WrTDdr5)) {
                // WrTLp4 and WrT Ddr5 are offsets from TxDqsDelay
                MrcGetSetStrobe (
                  MrcData,
                  CurrentMc,
                  CurrentCh,
                  rankIn,
                  CurrentByte,
                  TxDqsDelay,
                  ReadFromCache,
                  &GetSetVal[0]
                  );
                GetSetVal[0] = GetSetVal[0] + PiCenter + v0 + ((Outputs->Gear2 || Outputs->Gear4) ? 96 : 32);
              } else {
                GetSetVal[0] = PiCenter + v0;
              }
              MrcGetSetStrobe (
                MrcData,
                CurrentMc,
                CurrentCh,
                rankIn,
                CurrentByte,
                TxDqDelay,
                GsmMode,
                &GetSetVal[0]
                );
            } // CurrentByte
          } // Rank
        } // CurrentCh
      } // CurrentMc
      break;

    case CmdV:
      PdaMode = Ddr5 && (byte != 0); // Force DDR5 PDA for CmdV to OFF until specified otherwise
      MrcUpdateVref (MrcData, McChannelMask, RankMask, byte, param, v0, UpdateMrcData, PdaMode, SkipWait, TRUE);
      break;

    case WrV:
    case WrFan2:
    case WrFan3:
      PdaMode     = (Ddr4 || Ddr5) && (byte != 0);
      DeviceMask  = PdaMode ? byte  : 1;
      MrcUpdateVref (MrcData, McChannelMask, RankMask, DeviceMask, param, v0, UpdateMrcData, PdaMode, SkipWait, TRUE);
      if ((param == WrFan2) || (param == WrFan3)) {
        // Update TxDqOffset
        GetSetVal[1]  = v1;
        Group[1]      = TxDqOffset;
        UpdateGrp[1]  = TRUE;
      }
      break;

    // Read margin in FAN modes. Update RxDqsOffset and ReadVrefOffset
    case RdFan2:
    case RdFan3:
      GetSetVal[0]  = v0;
      Group[0]      = RxVrefOffset;
      GetSetVal[1]  = v1;
      Group[1]      = RxDqsOffset;
      UpdateGrp[0]  = TRUE;
      UpdateGrp[1]  = TRUE;
      GsmMode       = ForceWriteCached;
      break;

    case WrLevel: // Write DQ and DQS timing, margining only DQS-CLK timing
      GetSetVal[0]  = v0;
      Group[0]      = TxDqOffset;
      GetSetVal[1]  = v0;
      Group[1]      = TxDqsOffset;
      UpdateGrp[0]  = TRUE;
      UpdateGrp[1]  = TRUE;
      GsmMode       = ForceWriteCached;
      break;

    case WrTBit:  // Write DQ per BIT timing
      for (CurrentMc = McStart; CurrentMc < McEnd; CurrentMc++) {
        for (CurrentCh = ChannelStart; CurrentCh < ChannelEnd; CurrentCh++) {
          if (MrcChannelExist (MrcData, CurrentMc, CurrentCh)) {
            ControllerOut = &MrcData->Outputs.Controller[CurrentMc];
            CurrentChannelOut = &ControllerOut->Channel[CurrentCh];
            for (CurrentByte = ByteStart; CurrentByte < ByteEnd; CurrentByte++) {
               for (bit = 0; bit < MAX_BITS; bit++) {
                BitValue     = (((UINT32) value0) >> (4 * bit)) & 0xF;
                GetSetVal[0] = BitValue;
                // For Per Bits, they may exist in the same register.  So we always update the register cache.
                // Otherwise, the value gets corrupted.
                MrcGetSetBit (MrcData, CurrentMc, CurrentCh, rank, CurrentByte, bit, TxDqBitDelay, WriteToCache, &GetSetVal[0]);
                if (UpdateMrcData) {
                  CurrentChannelOut->TxDqPb[rank][CurrentByte][bit].Center = BitValue;
                }
              } // bit
            } // CurrentByte
          } // MrcChannelExist
        } // CurrentCh
      } // CurrentMc
      MrcFlushRegisterCachedData (MrcData);
      break;

    case RdTBit:  // Read DQ per BIT timing
      for (CurrentMc = McStart; CurrentMc < McEnd; CurrentMc++) {
        ControllerOut = &MrcData->Outputs.Controller[CurrentMc];
        for (CurrentCh = ChannelStart; CurrentCh < ChannelEnd; CurrentCh++) {
          if (MrcChannelExist (MrcData, CurrentMc, CurrentCh)) {
            CurrentChannelOut = &ControllerOut->Channel[CurrentCh];
            for (CurrentByte = ByteStart; CurrentByte < ByteEnd; CurrentByte++) {
              for (bit = 0; bit < MAX_BITS; bit++) {
                BitValue     = (((UINT32) value0) >> (4 * bit)) & 0x0F;
                GetSetVal[0] = BitValue;
                // For Per Bits, they may exist in the same register.  So we always update the register cache.
                // Otherwise, the value gets corrupted.
                // @todo change getset to use Controller
                MrcGetSetBit (MrcData, CurrentMc, CurrentCh, rank, CurrentByte, bit, RxDqsBitDelay, WriteToCache, &GetSetVal[0]);
                if (UpdateMrcData) {
                  CurrentChannelOut->RxDqPb[rank][CurrentByte][bit].Center = BitValue;
                }
              } // bit
            } // CurrentByte
          } // MrcChannelExist
        } // CurrentCh
      } // CurrentMc
      MrcFlushRegisterCachedData (MrcData);
      break;

    case RdVBit:  // Read DQ per BIT Voltage
      if (rankIn >= MAX_RANK_IN_CHANNEL) {
        RankStart     = 0;
        RankEnd       = MAX_RANK_IN_CHANNEL;
      } else {
        RankStart     = rank;
        RankEnd       = rank + 1;
      }
      for (CurrentMc = McStart; CurrentMc < McEnd; CurrentMc++) {
        ControllerOut = &MrcData->Outputs.Controller[CurrentMc];
        for (CurrentCh = ChannelStart; CurrentCh < ChannelEnd; CurrentCh++){
          if (MrcChannelExist (MrcData, CurrentMc, CurrentCh)) {
            CurrentChannelOut = &ControllerOut->Channel[CurrentCh];
            for (CurrentRank = RankStart; CurrentRank < RankEnd; CurrentRank++) {
              for (CurrentByte = ByteStart; CurrentByte < ByteEnd; CurrentByte++) {
                for (bit = 0; bit < MAX_BITS; bit++) {
                  BitValue     = (((UINT32) value0) >> (4 * bit)) & 0xF;
                  GetSetVal[0] = BitValue;
                  // For Per Bits, they may exist in the same register.  So we always update the register cache.
                  // Otherwise, the value gets corrupted.
                  MrcGetSetBit (MrcData, CurrentMc, CurrentCh, CurrentRank, CurrentByte, bit, RxVoc, WriteToCache, &GetSetVal[0]);
                  if (UpdateMrcData) {
                    CurrentChannelOut->RxDqVrefPb[CurrentRank][CurrentByte][bit].Center = BitValue;
                  }
                }
              } // bit
            } // CurrentByte
          } // MrcChannelExist
        } // CurrentRank
      } // CurrentCh
      MrcFlushRegisterCachedData (MrcData);
      break;

    default:
      MRC_DEBUG_MSG (Debug, MSG_LEVEL_ERROR, "Function ChangeMargin, Invalid parameter %d\n", param);
      return mrcWrongInputParameter;
  } // end switch (param)

  for (GrpIdx = 0; GrpIdx < MRC_CHNG_MAR_GRP_NUM; GrpIdx++) {
    if (UpdateGrp[GrpIdx] == FALSE) {
      continue;
    }
    // Write CR
    for (CurrentMc = McStart; CurrentMc < McEnd; CurrentMc++) {
      for (CurrentCh = ChannelStart; CurrentCh < ChannelEnd; CurrentCh++) {
        if (MrcChannelExist (MrcData, CurrentMc, CurrentCh)) {
          for (CurrentByte = ByteStart; CurrentByte < ByteEnd; CurrentByte++) {
            MrcGetSetChStrb (
              MrcData,
              CurrentMc,
              CurrentCh,
              CurrentByte,
              Group[GrpIdx],
              GsmMode,
              &GetSetVal[GrpIdx]
              );
          } // CurrentByte
        } //MrcChannelExist
      } // CurrentCh
    } // Controller
  } // GrpIdx

  return Status;
}

/**
  This procedure returns the minimal required eye height for a given training step.
  This information is subsequently used differently for making decisions, e.g., MRCFailure, PDA writing, etc.

  @param[in,out] MrcData        - Include all MRC global data.
  @param[in]     EarlyCentering - Is Step an Early Training step.
  @param[in]     Param          - Used for WrTLp5 (weak timing centering).

  return MinWidth UINT8
**/
UINT8
GetMinRequiredEyeWidth (
  IN    MrcParameters *const MrcData,
  IN    BOOLEAN              EarlyCentering,
  IN    const UINT8          Param
)
{
  const MrcInput    *Inputs;
  const MrcOutput   *Outputs;
  UINT8             MinWidth;
  BOOLEAN           Ddr5;

  Inputs  = &MrcData->Inputs;
  Outputs = &MrcData->Outputs;
  Ddr5       = (Outputs->DdrType == MRC_DDR_TYPE_DDR5);

  if ((Outputs->DdrType == MRC_DDR_TYPE_DDR4) && (Outputs->Frequency >= f2400)) {
    MinWidth = 4;
  } else if (Outputs->Frequency >= f4000) {
    MinWidth = 1;
  } else if (!Outputs->Lp5WeakWrTimingDone && (Param == WrTLp5)) {
    MinWidth = 4;
  } else if ((Inputs->MemoryProfile != STD_PROFILE) || EarlyCentering) {
    MinWidth = 1;
  } else if ((Ddr5) && ((Outputs->Any2Dpc) && (Outputs->Frequency >= f3600))) {
    MinWidth = 2;
  } else {
    MinWidth = 8;
  }

  return MinWidth;
}

/**
  Set default CPU Read Vref for all supported DDR technologies

  @param[in, out] MrcData      - Include all MRC global data.
  @param[in]      Print        - Print results.
  @param[in]      PhyInit      - TRUE if the function is called from PHY init.
  @retval none
**/
void
MrcSetDefaultRxVref (
  IN OUT MrcParameters *const MrcData,
  IN     BOOLEAN              Print,
  IN     BOOLEAN              PhyInit
  )
{
  UINT32        Controller;
  UINT32        Channel;
  UINT16        CPUImpedance;
  UINT8         MaxChannels;
  BOOLEAN       Lpddr;
  BOOLEAN       Lpddr5;
  BOOLEAN       Ddr4;
  BOOLEAN       Ddr5;
  BOOLEAN       Lp4x;
  UINT32        EffPullUp;
  UINT32        PuDeltaV;
  UINT32        Voh;
  UINT32        DramRon;
  UINT8         Rank;
  UINT8         NumRanks;
  INT64         GetSetVref;
  UINT32        Vref;
  MrcInput      *Inputs;
  MrcOutput     *Outputs;
  MrcDdrType    DdrType;
  MrcProfile    Profile;
  MrcVddSelect  DramVdd;
  MrcChannelOut *ChannelOut;
  MrcControllerOut  *ControllerOut;
  LPDDR4_MODE_REGISTER_3_TYPE  *Mr3;
  LPDDR5_MODE_REGISTER_3_TYPE  *Mr3Lp5;
  MrcStatus     Status;
  UINT32        PrintMode;

  Inputs      = &MrcData->Inputs;
  Outputs     = &MrcData->Outputs;
  DdrType     = Outputs->DdrType;
  Profile     = Inputs->MemoryProfile;
  MaxChannels = Outputs->MaxChannels;
  DramVdd     = Outputs->VddVoltage[Profile];

  Ddr4        = (DdrType == MRC_DDR_TYPE_DDR4);
  Ddr5        = (DdrType == MRC_DDR_TYPE_DDR5);
  Lpddr5      = (DdrType == MRC_DDR_TYPE_LPDDR5);
  Lpddr       = Outputs->Lpddr;
  Lp4x        = Outputs->Lp4x;
  PrintMode   = Print ? PrintValue : 0;

  if (Ddr4) {
    MrcSetDefaultRxVrefDdr4 (MrcData, TRUE, Print, PhyInit); // GetFromTable = TRUE
  } else if (Ddr5) {
    // @todo_adl 2DPC/1DPC MrcSetDefaultRxVrefDdr5 (MrcData, TRUE, Print, PhyInit);
    for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
      ControllerOut = &Outputs->Controller[Controller];
      for (Channel = 0; Channel < MaxChannels; Channel++) {
        if (MrcChannelExist(MrcData, Controller, Channel)) {
          if (ControllerOut->Channel[Channel].DimmCount == 2) {
            Vref = 902; // Code = 420
          } else {
            Vref = 803;
          }
          MrcEncodeRxVref (MrcData, Vref, &GetSetVref);
          MrcGetSetChStrb (MrcData, Controller, Channel, MAX_SDRAM_IN_DIMM, RxVref, WriteCached | PrintMode, &GetSetVref);
        }
      }
    }
  } else {
    if (Lpddr) {
      for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
        ControllerOut = &Outputs->Controller[Controller];
        for (Channel = 0; Channel < MaxChannels; Channel++) {
          if (MrcChannelExist (MrcData, Controller, Channel)) {
            ChannelOut  = &ControllerOut->Channel[Channel];
            if (PhyInit) {
              // This is tied to the Pull Up Calibration selected in the LPDDR4 MRs, but we can't read this in PHY init as they have not been set up yet; so use hardcodes.
              Voh = ((Lpddr5) ? DramVdd / 2 : ((Outputs->Lp4x) ? 420 : 440)); // mV
            } else {
              Mr3 = (LPDDR4_MODE_REGISTER_3_TYPE *) &ChannelOut->Dimm[dDIMM0].Rank[0].MR[mrIndexMR3];
              // Voh =
              //        Lpddr5 = DramVdd / 2
              //        Lppdr4 = (PullUpCal == 1) ? Vddq/3 : Vddq/2.5
              //        Lpddr4x = (PullUpCal == 1) ? Vddq * 0.5 : Vddq * 0.6
              if (Lpddr5) {
                Voh = DramVdd / 2;
              } else if (Lp4x) {
                Voh = (Mr3->Bits.PullUpCal) ? DIVIDEROUND (DramVdd * 7, 10) : DIVIDEROUND (DramVdd * 6, 10); // mV
              } else {
                Voh = (Mr3->Bits.PullUpCal) ? DIVIDEROUND (DramVdd, 3) : DIVIDEROUND (DramVdd * 2, 5);
              }
            }
            PuDeltaV = DramVdd - Voh;
            if (PhyInit) {
              CPUImpedance = Outputs->RcompTarget[RdOdt];
            } else {
              Status = CalcRxDqOdtAverageByteImpedance(MrcData, (UINT8)Controller, (UINT8)Channel, &CPUImpedance);
              if (mrcSuccess != Status) {
                MRC_DEBUG_MSG (&Outputs->Debug, MSG_LEVEL_ERROR, "CalcRxDqOdtAverageByteImpedance error %d\n", Status);
              }
            }
            MRC_POWER_DEBUG_MSG(&Outputs->Debug, MSG_LEVEL_NOTE, "MrcSetDefaultRxVref CPUImpedance = %d, Outputs->RcompTarget[RdOdt] = %d \n", CPUImpedance, Outputs->RcompTarget[RdOdt]);

            EffPullUp = PuDeltaV * CPUImpedance;
            EffPullUp = DIVIDEROUND (EffPullUp, Voh);

            // We need to account for DRAM Ron
            if (PhyInit) {
              DramRon = 40;
            } else {
              NumRanks = 0;
              DramRon = 0;
              for (Rank = 0; Rank < MAX_RANK_IN_CHANNEL; Rank++) {
                if (MrcRankExist(MrcData, Controller, Channel, Rank)) {
                  if (Lpddr5) {
                    Mr3Lp5 = (LPDDR5_MODE_REGISTER_3_TYPE*) &ChannelOut->Dimm[Rank / MAX_RANK_IN_DIMM].Rank[(Rank % MAX_RANK_IN_DIMM)].MR[mrIndexMR3];
                    DramRon += 240 / (Mr3Lp5->Bits.PullDnDrvStr);
                  } else {
                    Mr3 = (LPDDR4_MODE_REGISTER_3_TYPE *)&ChannelOut->Dimm[Rank / MAX_RANK_IN_DIMM].Rank[(Rank % MAX_RANK_IN_DIMM)].MR[mrIndexMR3];
                    DramRon += 240 / (Mr3->Bits.PdDriveStrength);
                  }
                  NumRanks++;
                }
              }
              if (NumRanks > 0) {
                DramRon /= NumRanks;
              }
            }

            MrcSetIdealRxVref (MrcData, Controller, Channel, EffPullUp, DramRon, CPUImpedance, Print);
          }
        } // Channel
      } // Controller
    } // Lpddr
   // @todo DDR5?
  }
}

/**
  This procedure is meant to handle basic timing centering, places strobe in the middle of the data eye,
  for both read and write DQ/DQS using a very robust, linear search algorthim.

  @param[in,out] MrcData        - Include all MRC global data.
  @param[in]     param          - {0:RcvEn, 1:RdT, 2:WrT, 3: WrDqsT, 4:RdV, 5:WrV, 6:WrLevel,
                                   8:WrTBit, 9:RdTBit, 10:RdVBit,
                                   16:RdFan2, 17:WrFan2, 32:RdFan3, 32:WrFan3}
                                   ONLY RdT and WrT are allowed in this function
  @param[in]     ResetPerBit    - Option to Reset PerBit Deskew to middle value before byte training
  @param[in]     loopcount      - loop count
  @param[in]     MsgPrint       - Show debug prints
  @param[in]     EarlyCentering - Execute as early centering routine
  @param[in]     StepSize       - Sweep step size

  @retval MrcStatus -  If succeeded, return mrcSuccess
**/
MrcStatus
DQTimeCentering1D (
  IN OUT MrcParameters *const MrcData,
  IN     const UINT8          param,
  IN     const UINT8          ResetPerBit,
  IN     const UINT8          loopcount,
  IN     BOOLEAN              MsgPrint,
  IN     BOOLEAN              EarlyCentering,
  IN     UINT8                StepSize
  )
{
  const MrcInput    *Inputs;
  const MrcChannelIn *ChannelIn;
  const MRC_FUNCTION *MrcCall;
  MrcDebug          *Debug;
  MrcDebugMsgLevel   DebugLevel;
  MrcOutput         *Outputs;
  MrcControllerOut  *ControllerOut;
  MrcChannelOut     *ChannelOut;
  MrcDimmOut        *DimmOut;
  MrcStatus         Status;
  MrcStatus         ChMarginStatus;
  UINT64            ErrStatus;
  INT64             GetSetVal;
  INT64             GetSetMax;
  INT16             *CurrentPS;
  INT16             *CurrentPE;
  INT16             *LargestPS;
  INT16             *LargestPE;
  INT32             ByteCenter[MAX_SDRAM_IN_DIMM];
  INT16             LargestPSCh[MAX_CONTROLLER][MAX_CHANNEL];
  INT16             LargestPECh[MAX_CONTROLLER][MAX_CHANNEL];
  INT16             CurrentPassingStart[MAX_CONTROLLER][MAX_CHANNEL][MAX_SDRAM_IN_DIMM];
  INT16             CurrentPassingEnd[MAX_CONTROLLER][MAX_CHANNEL][MAX_SDRAM_IN_DIMM];
  INT16             LargestPassingStart[MAX_CONTROLLER][MAX_CHANNEL][MAX_SDRAM_IN_DIMM];
  INT16             LargestPassingEnd[MAX_CONTROLLER][MAX_CHANNEL][MAX_SDRAM_IN_DIMM];
  INT16             cWidth;
  INT16             lWidth;
  INT16             Center;
  INT32             RankCenter;
  INT16             DqsDelay;
  INT16             DqsStart;
  INT16             DqsStop;
  INT32             Vref;
  UINT16            *Margin;
  UINT16            Start;
  UINT16            End;
  UINT32            Tdqs2dqCenterFs;
  UINT16            Result;
  UINT8             Controller;
  UINT8             Channel;
  UINT8             Rank;
  UINT8             RankMask;
  UINT8             RankRx;
  UINT8             Dimm;
  UINT8             Byte;
  UINT8             Device;
  UINT8             NumDevices;
  UINT8             DramByte;
  UINT8             DeviceByte0;
  UINT8             Step;
  UINT8             MinWidth;
  UINT8             McChBitMask;
  UINT32            CurMcChBitMask;
  UINT8             MaxChannels;
  UINT8             SdramCount;
  UINT8             CurrentVref;
  UINT8             CurrentMargin;
  UINT32            Index;
  BOOLEAN           Pass;
  BOOLEAN           Ddr4;
  BOOLEAN           Ddr5;
  BOOLEAN           ByteFound;
  BOOLEAN           EccSupport;
  BOOLEAN           EyeTooSmall[MAX_CONTROLLER][MAX_CHANNEL][MAX_RANK_IN_CHANNEL];
  GSM_GT            GroupType;
  UINT64            EccStatus;
  UINT8             EccByte;
#ifdef MRC_DEBUG_PRINT
  UINT64            ChunkStatus;
  UINT16            ChunkResult[MAX_CONTROLLER][MAX_CHANNEL];
  UINT64            BitLaneFailures[MAX_CONTROLLER][MAX_CHANNEL][MRC_1D_ERROR_LEN];
  UINT8             EccBitLaneFailures[MAX_CONTROLLER][MAX_CHANNEL][MRC_1D_ERROR_LEN];
  UINT8             BitErrStatus[MAX_SDRAM_IN_DIMM];
  UINT32            ErrorCount;
  UINT8             BitCount;
  const char        *DelayString;
  BOOLEAN           Overflow;
  UINT8             SpaceCount;
  UINT8             MaxBitCount;
#endif


  Inputs        = &MrcData->Inputs;
  MrcCall       = Inputs->Call.Func;
  Outputs       = &MrcData->Outputs;
  Debug         = &Outputs->Debug;
  McChBitMask   = Outputs->McChBitMask;
  MaxChannels   = Outputs->MaxChannels;
  SdramCount    = Outputs->SdramCount;
  EccSupport    = Outputs->EccSupport;
  Status        = mrcSuccess;
  ChMarginStatus = mrcSuccess;
  Center        = 0;
  RankCenter    = 0;
  Step          = StepSize;
  DqsStop       = 0;
  Ddr4          = (Outputs->DdrType == MRC_DDR_TYPE_DDR4);
  Ddr5          = (Outputs->DdrType == MRC_DDR_TYPE_DDR5);
  EccStatus     = 0;
  DeviceByte0   = 0;
  EccByte       = (Ddr5) ? MRC_DDR5_ECC_BYTE : MRC_DDR4_ECC_BYTE;
  DebugLevel    = MsgPrint ? MSG_LEVEL_NOTE : MSG_LEVEL_NEVER;
  MinWidth      = GetMinRequiredEyeWidth (MrcData, EarlyCentering, param);
  MrcCall->MrcSetMem ((UINT8 *) EyeTooSmall, sizeof (EyeTooSmall), FALSE);
  MrcCall->MrcSetMem ((UINT8 *) ByteCenter, sizeof (ByteCenter), 0);
  MrcCall->MrcSetMem ((UINT8 *) LargestPSCh, sizeof (LargestPSCh), 0);
  MrcCall->MrcSetMem ((UINT8 *) LargestPECh, sizeof (LargestPECh), 0);
  if ((param != RdT) && (param != WrT) && (param != RcvEnaX)  && (param != WrV) && (param != RdV) && (param != CmdV) && (param != WrTLp4) && (param != WrTLp5) && (param != WrTDdr5)) {
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_ERROR, "DataTimeCentering1D: Unknown Margin Parameter\n");
    return mrcWrongInputParameter;
  }

  if ((param == CmdV) && (Inputs->MemoryProfile == STD_PROFILE)) {
    if (Ddr4 && (Outputs->Frequency >= f2933)) {
      Step = 1;
    } else {
      Step = 2;
    }
  }

  if ((param == WrV) || (param == CmdV) || (param == RdV)) {
    DqsStop = GetVrefOffsetLimits (MrcData, param);
    DqsStart = -1 * DqsStop;
  } else if ((param == WrTLp4) || (param == WrTDdr5)) {
    MrcGetTdqs2dqCenter (MrcData, &Tdqs2dqCenterFs);
    DqsStart = -1 * (UINT16) MrcFemptoSec2PiTick (MrcData, Tdqs2dqCenterFs);
    DqsStop = (UINT16) MrcFemptoSec2PiTick (MrcData, Tdqs2dqCenterFs);
  } else if (param == WrTLp5) {
    MrcGetSetLimits (MrcData, TxDqDelay, &GetSetVal, &GetSetMax, NULL);
    DqsStart = ((MRC_LP5_TXDQ_CENTER - MRC_LP5_TXDQ_RANGE) > GetSetVal) ? (MRC_LP5_TXDQ_CENTER - MRC_LP5_TXDQ_RANGE) : (INT16) GetSetVal;
    DqsStop = ((MRC_LP5_TXDQ_CENTER + MRC_LP5_TXDQ_RANGE) < GetSetMax) ? (MRC_LP5_TXDQ_CENTER + MRC_LP5_TXDQ_RANGE) : (INT16) GetSetMax;
  } else {
    DqsStart  = -MAX_POSSIBLE_TIME;
    DqsStop   = MAX_POSSIBLE_TIME;
  }
  if (param == RcvEnaX) {
    SetupIOTestBasicVA (MrcData, McChBitMask, loopcount - 3, NSOE, 0, 0, 8, PatWrRd, 0, 0);
    Outputs->DQPat = RdRdTA_All;
  } else if (param == CmdV) {
    SetupIOTestCADB (MrcData, McChBitMask, loopcount, NSOE, 1, 0);
  } else if ((param == WrTLp5) && !(Outputs->Lp5WeakWrTimingDone)) { //only for WeakWriteTiming centering on LP5
    SetupIOTestBasicVA (MrcData, McChBitMask, loopcount, NSOE, 0, 0, 8, PatWrRd, 0, 1);
    Outputs->DQPat = StaticPattern;
  } else {
    SetupIOTestBasicVA (MrcData, McChBitMask, loopcount, NSOE, 0, 0, 8, PatWrRd, 0, 0);
  }

#ifdef MRC_DEBUG_PRINT
  switch (param) {
    case RcvEnaX :
      DelayString = RcvEnDelayString;
      break;
    case WrV :
      DelayString = WrVDelayString;
      break;
    case RdV :
      DelayString = RdVDelayString;
      break;
    case CmdV:
      DelayString = CmdVDelayString;
      break;
    default :
      DelayString = DqsDelayString;
  }
#endif
  // Reset PerBit Deskew to zero before byte training
  if (ResetPerBit == 1) {
    GroupType = (param == RdT) ? RxDqsBitDelay : TxDqBitDelay;
    GetSetVal = 0;
    MrcGetSetBit (MrcData, MAX_CONTROLLER, MAX_CHANNEL, MAX_RANK_IN_CHANNEL, MAX_SDRAM_IN_DIMM, MAX_BITS, GroupType, WriteToCache, &GetSetVal);

    if ((param == WrT) || (param == WrTLp4) || (param == WrTLp5) || (param == WrTDdr5)) {
      GetSetVal = 0;
      MrcGetSetStrobe (MrcData, MAX_CONTROLLER, MAX_CHANNEL, MAX_RANK_IN_CHANNEL, MAX_SDRAM_IN_DIMM, TxDqsBitDelay, WriteCached, &GetSetVal);
    }

    MrcFlushRegisterCachedData (MrcData);
  }

  if (param == RdV) {
    // Update Vref
    MrcSetDefaultRxVref (MrcData, MsgPrint, FALSE); // PhyInit = FALSE
    // RxVref adjustment to keep 1D-sweep values
    // in between Min / Max boundaries
    for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
      for (Channel = 0; Channel < MaxChannels; Channel++) {
        if (!(MrcChannelExist(MrcData, Controller, Channel))) {
          continue; // This channel is not populated
        }
        CurrentVref = MrcCalcMaxRxMargin(MrcData, param, Controller, Channel, 0, 0, 0, ((UINT8) (-1 * DqsStart)));
        if (CurrentVref < ((UINT8) (-1 * DqsStart))) {
          MRC_DEBUG_MSG (Debug, DebugLevel, "Mc%d.C%d: %s offset adjusted from %d to %d\n", Controller, Channel, "RxVref", DqsStart, (-1 * CurrentVref));
          DqsStart = (INT16) (-1 * CurrentVref);
        }
        CurrentVref = MrcCalcMaxRxMargin(MrcData, param, Controller, Channel, 0, 0, 1, ((UINT8) DqsStop));
        if (CurrentVref < ((UINT8) DqsStop)) {
          MRC_DEBUG_MSG (Debug, DebugLevel, "Mc%d.C%d: %s offset adjusted from %d to %d\n", Controller, Channel, "RxVref", DqsStop, CurrentVref);
          DqsStop = (INT16) CurrentVref;
        }
      }
    }
  }

  if ((param == RcvEnaX) || (param == RdV)) {
    // Make sure RecEnDelay / RxVref values exist in CR cache for this algorithm to work, because we will use WriteOffsetUncached
    MrcReadParamIntoCache (MrcData, param);
  }
  MRC_DEBUG_MSG (Debug, DebugLevel, "Start: %d, Stop: %d, Step: %d\n", DqsStart, DqsStop, Step);

  // Center all Ranks
  for (Rank = 0; Rank < MAX_RANK_IN_CHANNEL; Rank++) {
    Dimm = RANK_TO_DIMM_NUMBER (Rank);

    if (Ddr5 && (param == CmdV)) {
      DqsStop = GetVrefOffsetLimits (MrcData, CmdV);
      DqsStart = -1 * DqsStop;
      // Adjust start/stop to sweep relative to the current CmdVref up to the full JEDEC range
      for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
        for (Channel = 0; Channel < MaxChannels; Channel++) {
          if (!(MrcChannelExist(MrcData, Controller, Channel))) {
            continue; // This channel is not populated
          }
          CurrentVref = MrcCalcMaxVrefMargin (MrcData, Controller, Channel, 1 << Rank, 0, param, 0, (UINT8) (-1 * DqsStart), FALSE);
          if (CurrentVref < ((UINT8) (-1 * DqsStart))) {
            MRC_DEBUG_MSG (Debug, DebugLevel, "Mc%d.C%d: %s offset adjusted from %d to %d\n", Controller, Channel, "CmdV", DqsStart, (-1 * CurrentVref));
            DqsStart = (INT16) (-1 * CurrentVref);
          }
          CurrentVref = MrcCalcMaxVrefMargin (MrcData, Controller, Channel, 1 << Rank, 0, param, 1, (UINT8) DqsStop, FALSE);
          if (CurrentVref < ((UINT8) DqsStop)) {
            MRC_DEBUG_MSG (Debug, DebugLevel, "Mc%d.C%d: %s offset adjusted from %d to %d\n", Controller, Channel, "CmdV", DqsStop, CurrentVref);
            DqsStop = (INT16) CurrentVref;
          }
        }
      }
    }

    if ((param == RdT)) {
      // Adjust Start to make sure we don't underflow RxDqsP/N PI while sweeping using RdDqsOffset
      // No need to adjust the Stop as RxDqsPI range is 7 bits
      DqsStart = -MAX_POSSIBLE_TIME;
      for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
        for (Channel = 0; Channel < MaxChannels; Channel++) {
          if (!MrcRankExist (MrcData, Controller, Channel, Rank)) {
            continue; // This channel is not populated on this rank
          }
          for (Byte = 0; Byte < SdramCount; Byte++) {
            CurrentMargin = MrcCalcMaxRxMargin (MrcData, param, Controller, Channel, Rank, Byte, 0, ((UINT8) (-1 * DqsStart)));
            if (CurrentMargin < ((UINT8) (-1 * DqsStart))) {
              MRC_DEBUG_MSG (Debug, DebugLevel, "Mc%u.C%u.R%u.B%u: %s offset adjusted from %d to %d\n", Controller, Channel, Rank, Byte, "RdT", DqsStart, (-1 * CurrentMargin));
              DqsStart = (INT16) (-1 * CurrentMargin);
            }
          }
        }
      }
    }

#ifdef MRC_DEBUG_PRINT
    if (Outputs->ValidRankMask & (MRC_BIT0 << Rank)) {
      MRC_DEBUG_MSG (Debug, DebugLevel, "Rank = %d\nMc\t\t", Rank);
      for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
        if (MrcControllerExist (MrcData, Controller)) {
          MRC_DEBUG_MSG (Debug, DebugLevel, "%d", Controller);
          SpaceCount = (2 * MaxChannels * SdramCount) - 1;
          for (Index = 0; Index < SpaceCount; Index++) {
            MRC_DEBUG_MSG (Debug, DebugLevel, " ");
          }
        }
      }
      MRC_DEBUG_MSG (Debug, DebugLevel, "\nChannel\t\t");
      for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
        for (Channel = 0; Channel < MaxChannels; Channel++) {
          if (MrcChannelExist (MrcData, Controller, Channel)) {
            MRC_DEBUG_MSG (Debug, DebugLevel, "%d", Channel);
            SpaceCount = (2 * SdramCount) - 1;
            for (Index = 0; Index < SpaceCount; Index++) {
              MRC_DEBUG_MSG (Debug, DebugLevel, " ");
            }
          }
        }
      }
      MRC_DEBUG_MSG (Debug, DebugLevel, "\nByte\t\t");
      for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
        for (Channel = 0; Channel < MaxChannels; Channel++) {
          for (Byte = 0; Byte < SdramCount; Byte++) {
            if (MrcByteExist (MrcData, Controller, Channel, Byte)) {
              MRC_DEBUG_MSG (Debug, DebugLevel, "%d ", Byte);
            }
          }
        }
      }
      MRC_DEBUG_MSG (Debug, DebugLevel, " ErrorCount ChunkResult");
    }
#endif  // MRC_DEBUG_PRINT
    McChBitMask = 0;
    for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
      if (MrcControllerExist (MrcData, Controller)) {
        ControllerOut = &Outputs->Controller[Controller];
        for (Channel = 0; Channel < MaxChannels; Channel++) {
          ChannelOut = &ControllerOut->Channel[Channel];
          if (MrcChannelExist (MrcData, Controller, Channel)) {
            CurMcChBitMask = 1 << ((Controller * MaxChannels) + Channel);
            if (param == RcvEnaX) {
              if ((ChannelOut->ValidRankBitMask & (1 << Rank)) != 0) { // If rank under test exists, then select all ranks
                RankMask = 0xFF; // run on all available ranks for TAT stress
              } else {
                RankMask = 0;
              }
            } else if (param == RdV) {
              RankMask = 0xFF; // run on all available ranks for RdV param - we dont use different vref per rank
            } else {
              RankMask = (1 << Rank);
            }
            McChBitMask |= SelectReutRanks (MrcData, Controller, Channel, RankMask, FALSE, 0);
            if (CurMcChBitMask & McChBitMask) {
              // Clear out anything left over in DataOffsetTrain
              // Update rank timing to middle value
              for (Byte = 0; (Byte < SdramCount) && (param != RcvEnaX) && (param != WrV); Byte++) {
                if (!MrcByteExist (MrcData, Controller, Channel, Byte)) {
                  continue;
                }
                if (param == WrT) {
                  // Write Dq/Dqs
                  // We want to preserve the relation of tDQS2DQ for LPDDR4, so adjust TxDQ here for matched architectured DRAM types.
                  if (Ddr4) {
                    MrcGetSetStrobe (MrcData, Controller, Channel, Rank, Byte, TxDqsDelay, ReadFromCache, &GetSetVal);
                    GetSetVal += ((Outputs->Gear2 || Outputs->Gear4) ? 96 : 32);
                    MrcGetSetStrobe (MrcData, Controller, Channel, Rank, Byte, TxDqDelay, WriteCached, &GetSetVal);
                  }
                }
              } // Byte
              if ((param == WrV) || (param == CmdV)) {
                // Write Vref - program to middle (zero offset) range before sweep
                if (Ddr4 && (param == CmdV)) {
                  Vref = MID_INT_VREF;
                } else {
                  // Lpddr CmdV; Lpddr and DDR4/DDR5 WrV
                  Vref = 0;
                }
                if (!Ddr5 || (param != CmdV)) {
                  // Do not set to 0 for DDR5 CmdV - it might be out of passing range and will break JEDEC reset
                  MrcUpdateVref (MrcData, CurMcChBitMask, RankMask, 0, param, Vref, TRUE, FALSE, FALSE, FALSE);
                }
              }
            } // ((1 << Channel) & chBitMask)
          }
        } // for Channel
      }
    } // for Controller

    // Setup REUT Error Counters to count errors on all lanes, all UI
    MrcSetupErrCounterCtl (MrcData, 0, ErrCounterCtlAllLanes, 0);

    // Continue if not valid rank on any channel
    if (McChBitMask == 0) {
      continue; // This rank does not exist on any of the channels
    }
    // Sweep through values
    MRC_DEBUG_MSG (Debug, DebugLevel, "\n%s", DelayString);
    if ((param == WrV) || ((param == CmdV))) {
      RankMask = 1 << Rank;
    } else {
      RankMask = Rank;
    }

    for (DqsDelay = DqsStart, Index = 0; DqsDelay <= DqsStop; DqsDelay += Step, Index++) {
      // Program DQS Delays
      ChMarginStatus = ChangeMargin (MrcData, param, DqsDelay, 0, 1, 0, 0, RankMask, 0, 0, 0, 0);
      // Clear Errors and Run Test
      RunIOTest (MrcData, McChBitMask, Outputs->DQPat, 1, 0);
      if (param == CmdV) {
        MrcResetSequence (MrcData);
      }
      IoReset (MrcData);
      MRC_DEBUG_MSG (Debug, DebugLevel, "\n% 5d  \t\t", DqsDelay);
      // Update results for all Channel/bytes
      for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
        if (MrcControllerExist (MrcData, Controller)) {
          for (Channel = 0; Channel < MaxChannels; Channel++) {
            if (!(McChBitMask & (MRC_BIT0 << ((Controller * MaxChannels) + Channel)))) {
              MRC_DEBUG_MSG (
                Debug,
                DebugLevel,
                (Channel != 0) ? "" : ((SdramCount == MAX_SDRAM_IN_DIMM) ? "                  " : "                ")
                );
              continue;
            }
            Result = 0;
#ifdef MRC_DEBUG_PRINT
            MRC_DEBUG_ASSERT (Index < MRC_1D_ERROR_LEN, Debug, "Index exceeds debug array %u > %u\n", Index, MRC_1D_ERROR_LEN);
            BitLaneFailures[Controller][Channel][Index] = 0;
            EccBitLaneFailures[Controller][Channel][Index] = 0;
            ChunkResult[Controller][Channel] = 0;
#endif
            // Read out per byte error results and check for any byte error
            MrcGetMiscErrStatus (MrcData, Controller, Channel, ByteGroupErrStatus, &ErrStatus);
            if (EccSupport) { // Read out  ecc byte error results
              MrcGetMiscErrStatus (MrcData, Controller, Channel, EccLaneErrStatus, &EccStatus);
            }
            Result = (UINT16) (ErrStatus |  MrcCall->MrcLeftShift64 (EccStatus, EccByte));
#ifdef MRC_DEBUG_PRINT
            if (MsgPrint) {
              MrcGetBitGroupErrStatus (MrcData, Controller, Channel, BitErrStatus);
              for (Byte = 0; Byte < SdramCount; Byte++) {
                if (Byte < EccByte) {
                  BitLaneFailures[Controller][Channel][Index] |= MrcCall->MrcLeftShift64((UINT64)BitErrStatus[Byte], 8 * Byte);
                } else {
                  EccBitLaneFailures[Controller][Channel][Index] = BitErrStatus[EccByte];
                }
              }
              // Read per chunk error status
              MrcGetMiscErrStatus (MrcData, Controller, Channel, ChunkErrStatus, &ChunkStatus);
              ChunkResult[Controller][Channel] = (UINT16) ChunkStatus;
            }
#endif
            for (Byte = 0; Byte < SdramCount; Byte++) {
              if (!MrcByteExist (MrcData, Controller, Channel, Byte)) {
                MRC_DEBUG_MSG (Debug, DebugLevel, "  ");
                continue;
              }
              // Check for Byte group error status
              Pass = ((Result & (MRC_BIT0 << Byte)) == 0);
              MRC_DEBUG_MSG (Debug, DebugLevel, Pass ? ". " : "# ");
              CurrentPS = &CurrentPassingStart[Controller][Channel][Byte];
              CurrentPE = &CurrentPassingEnd[Controller][Channel][Byte];
              LargestPS = &LargestPassingStart[Controller][Channel][Byte];
              LargestPE = &LargestPassingEnd[Controller][Channel][Byte];
              if (DqsDelay == DqsStart) {
                if (Pass) {
                  // No error on this Byte group
                  *CurrentPS = *CurrentPE = *LargestPS = *LargestPE = DqsDelay;
                } else {
                  // Selected Byte group has accumulated an error during loop back pattern
                  *CurrentPS = *CurrentPE = *LargestPS = *LargestPE = DqsStart - 2;
                }
              } else {
                if (Pass) {
                  // No error on this Byte group
                  if (*CurrentPE != (DqsDelay - Step)) {
                    *CurrentPS = DqsDelay;
                  }
                  *CurrentPE = DqsDelay;
                  // Update Largest variables
                  cWidth = *CurrentPE - *CurrentPS;
                  lWidth = *LargestPE - *LargestPS;
                  if (cWidth > lWidth) {
                    *LargestPS = *CurrentPS;
                    *LargestPE = *CurrentPE;
                  }
                }
              }
              if (Byte == 0) {
                LargestPSCh[Controller][Channel] = *LargestPS;
                LargestPECh[Controller][Channel] = *LargestPE;
              } else {
                LargestPSCh[Controller][Channel] = MAX (LargestPSCh[Controller][Channel], *LargestPS);
                LargestPECh[Controller][Channel] = MIN (LargestPECh[Controller][Channel], *LargestPE);
              }
            } // for Byte
          } // for Channel
        }
      } // for Controller
#ifdef MRC_DEBUG_PRINT
      for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
        if (MrcControllerExist (MrcData, Controller)) {
          for (Channel = 0; Channel < MaxChannels; Channel++) {
            if ((McChBitMask & (MRC_BIT0 << ((Controller * MaxChannels) + Channel)))) {
              MrcGetErrCounterStatus (MrcData, Controller, Channel, 0, ErrCounterCtlAllLanes, &ErrorCount, &Overflow);
              MRC_DEBUG_MSG (Debug, DebugLevel, " 0x%x\t0x%04x", ErrorCount, ChunkResult[Controller][Channel]);
            }
          } // for Channel
        }
      } // for Controller
#endif  // MRC_DEBUG_PRINT
    } // for DqsDelay
    MRC_DEBUG_MSG (Debug, DebugLevel, "\n");  // End last line of Byte table.

#ifdef MRC_DEBUG_PRINT
    if (!Ddr4 && !Ddr5) {
      MaxBitCount = 16;
    } else if (Ddr5) {
      MaxBitCount = EccSupport ? 36 : 32;
    } else {
      MaxBitCount = EccSupport ? 72 : 64;
    }

    // Print out the bit lane failure information
    if (param != CmdV) {
      MRC_DEBUG_MSG (Debug, DebugLevel, "Bit Lane Information\n");
      for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
        if (MrcControllerExist (MrcData, Controller)) {
          MRC_DEBUG_MSG (Debug, DebugLevel, "Mc \t\t%d\n", Controller);
          for (Channel = 0; Channel < MaxChannels; Channel++) {
            if ((McChBitMask & (MRC_BIT0 << ((Controller * MaxChannels) + Channel)))) {
              MRC_DEBUG_MSG (Debug, DebugLevel, "Channel \t%d\nBitLane \t", Channel);
              for (BitCount = 0; BitCount <= (((MaxBitCount - 1) / 10)); BitCount++) {
                MRC_DEBUG_MSG (Debug, DebugLevel, "%u         ", BitCount);
              }
              MRC_DEBUG_MSG (Debug, DebugLevel, "\n\t\t");  // End tens number and align ones number
              for (BitCount = 0; BitCount < MaxBitCount; BitCount++) {
                MRC_DEBUG_MSG (Debug, DebugLevel, "%u", BitCount % 10);
              }
              MRC_DEBUG_MSG (Debug, DebugLevel, "\n%s", DelayString);
              for (DqsDelay = DqsStart, Index = 0; DqsDelay <= DqsStop; DqsDelay += Step, Index++) {
                MRC_DEBUG_MSG (Debug, DebugLevel, "\n% 5d\t\t", DqsDelay); // Begin with a new line and print the DqsDelay value
                for (BitCount = 0; BitCount < MaxBitCount; BitCount++) {
                  if (MrcByteExist (MrcData, Controller, Channel, BitCount / 8)) {
                    if (EccByte == (BitCount / 8)) {
                      ErrStatus = EccBitLaneFailures[Controller][Channel][Index] & (1 << (BitCount % 8));
                    } else {
                      ErrStatus = BitLaneFailures[Controller][Channel][Index] & MrcCall->MrcLeftShift64 (1, BitCount);
                    }
                    MRC_DEBUG_MSG (Debug, DebugLevel, ErrStatus ? "#" : ".");
                  } else {
                    MRC_DEBUG_MSG (Debug, DebugLevel, " ");
                  }
                } // BitCount
              } // DqsDelay
              MRC_DEBUG_MSG (Debug, DebugLevel, "\n"); // Gap after Channel
            } // (chBitMask & (1 << Channel))
          } // Channel
        }
      } // Controller
    } // (param != CmdV)*/
#endif // MRC_DEBUG_PRINT

    // Clean Up for next Rank
    ChMarginStatus |=ChangeMargin (MrcData, param, 0, 0, 1, 0, 0, RankMask, 0, 0, 0, 0);
    if (param == CmdV) {
      MrcResetSequence (MrcData);
    }
    for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
      if (MrcControllerExist (MrcData, Controller)) {
        ControllerOut = &Outputs->Controller[Controller];
        for (Channel = 0; Channel < MaxChannels; Channel++) {
          for (Byte = 0; Byte < SdramCount; Byte++) {
            ByteCenter[Byte] = 0;
          }
          ChannelOut = &ControllerOut->Channel[Channel];
          ChannelIn  = &Inputs->Controller[Controller].Channel[Channel];

          if (Ddr5 && ((param == WrV) || (param == CmdV))) {
            DimmOut = &ChannelOut->Dimm[RANK_TO_DIMM_NUMBER (Rank)];
            if (DimmOut->SdramWidth == 16) {
              NumDevices = DimmOut->PrimaryBusWidth / DimmOut->SdramWidth;
              DramByte = 0;
              for (Device = 0; Device < NumDevices; Device++) {
                ByteFound = FALSE; // Used to track when the first Byte is found within a Device
                for (Byte = 0; Byte < SdramCount; Byte++) {
                  if (!MrcByteExist (MrcData, Controller, Channel, Byte)) {
                    continue;
                  }
                  // Locate bytes within the same Device, and track margins per-Device rather than per-byte taking the worst case margin from both bytes.
                  if ((ChannelIn->DqsMapCpu2Dram[Dimm][Byte] == DramByte) || (ChannelIn->DqsMapCpu2Dram[Dimm][Byte] == (DramByte + 1))) {
                    if (!ByteFound) {
                      // Save Byte index for this DramByte
                      DeviceByte0 = Byte;
                      ByteFound = TRUE;
                    } else {
                      LargestPS = &LargestPassingStart[Controller][Channel][Byte];
                      LargestPE = &LargestPassingEnd[Controller][Channel][Byte];
                      // Check Start/End for passing region and apply worst case for both bytes. Compare against current Byte to DeviceByte0
                      // Use the larger Start value between both bytes
                      if (*LargestPS < LargestPassingStart[Controller][Channel][DeviceByte0]) {
                        *LargestPS = LargestPassingStart[Controller][Channel][DeviceByte0];
                      } else {
                        LargestPassingStart[Controller][Channel][DeviceByte0] = *LargestPS;
                      }
                      // Use the smaller End value between both bytes
                      if (*LargestPE < LargestPassingEnd[Controller][Channel][DeviceByte0]) {
                        LargestPassingEnd[Controller][Channel][DeviceByte0] = *LargestPE;
                      } else {
                        *LargestPE = LargestPassingEnd[Controller][Channel][DeviceByte0];
                      }
                      // Increment DramByte for the next device, and break out of Byte loop
                      DramByte = DramByte + 2;
                      break;
                    }
                  }
                }
              }
            }
          }


          if ((McChBitMask & (MRC_BIT0 << ((Controller * MaxChannels) + Channel)))) {
            MRC_DEBUG_MSG (Debug, DebugLevel, "Mc%d.C%d.R%d:\tLeft\tRight\tWidth\tCenter\n", Controller, Channel, Rank);
            for (Byte = 0; Byte < SdramCount; Byte++) {
              if (!MrcByteExist (MrcData, Controller, Channel, Byte)) {
                continue;
              }
              LargestPS = &LargestPassingStart[Controller][Channel][Byte];
              LargestPE = &LargestPassingEnd[Controller][Channel][Byte];
              if (*LargestPS == *LargestPE) {
                // we like to see 0 margins when the eye closed ;)
                *LargestPS = *LargestPE = 0;
              }
              lWidth = *LargestPE - *LargestPS;
              Center = *LargestPS + (lWidth / 2);
              if (lWidth < MinWidth) {
                EyeTooSmall[Controller][Channel][Rank] = TRUE;
                MRC_DEBUG_MSG (
                  Debug,
                  DebugLevel,
                  "ERROR!! DataTimeCentering1D Eye Too Small Mc%u.Ch%u: Rank: %u, Byte: %u\n",
                  Controller,
                  Channel,
                  Rank,
                  Byte
                  );
                if (Inputs->ExitOnFailure) {
                  Status = mrcDataTimeCentering1DErr;
                }
              }
              MRC_DEBUG_MSG (
                Debug,
                DebugLevel,
                "       B%d:\t%d\t%d\t%d\t%d\n",
                Byte,
                *LargestPS,
                *LargestPE,
                lWidth,
                Center
                );
              // Store the new margins relative to the center into MarginResult
              Start = ABS ((*LargestPS - Center) * 10);
              End   = ABS ((*LargestPE - Center) * 10);
              if ((param == RdT) || (param == RdTN) || (param == RdTP)) {
                // read Dq./Dqs
                Margin    = &Outputs->MarginResult[LastRxT][Rank][Controller][Channel][Byte][0];
                *Margin   = Start;
                Margin[1] = End;
                GetSetVal = Center;
                if (param != RdTN) {
                  MrcGetSetStrobe (MrcData, Controller, Channel, Rank, Byte, RxDqsPDelay, WriteOffsetCached, &GetSetVal);
                }
                if (param != RdTP) {
                  MrcGetSetStrobe (MrcData, Controller, Channel, Rank, Byte, RxDqsNDelay, WriteOffsetCached, &GetSetVal);
                }
              } else if (param == WrT) {
                // Write Dq/Dqs
                Margin    = &Outputs->MarginResult[LastTxT][Rank][Controller][Channel][Byte][0];
                *Margin   = Start;
                Margin[1] = End;
                GetSetVal = Center;
                MrcGetSetStrobe (MrcData, Controller, Channel, Rank, Byte, TxDqDelay, WriteOffsetCached, &GetSetVal);
              } else if (param == RcvEnaX){
                // Receive Enable
                for (RankRx = 0; RankRx < MAX_RANK_IN_CHANNEL; RankRx++) {
                  if (ChannelOut->ValidRankBitMask & (MRC_BIT0 << RankRx)) {
                    Margin    = &Outputs->MarginResult[LastRcvEna][RankRx][Controller][Channel][Byte][0];
                    *Margin   = Start;
                    Margin[1] = End;
                    GetSetVal = 4 * Center;
                    MrcGetSetStrobe (MrcData, Controller, Channel, Rank, Byte, RecEnDelay, WriteOffsetCached, &GetSetVal);
                  }
                }
              } else if ((param == WrTLp4) || (param == WrTLp5) || (param == WrTDdr5)) {
                // Write Dq/Dqs with tDQS2DQ
                Margin     = &Outputs->MarginResult[LastTxT][Rank][Controller][Channel][Byte][0];
                *Margin    = Start;
                Margin[1]  = End;
                ChMarginStatus |= ChangeMargin (MrcData, param, Center, 0, 0, Controller, Channel, Rank, Byte, 0, 1, 0);
              } else if (param == WrV) {
                // Write Vref
                Margin    = &Outputs->MarginResult[LastTxV][Rank][Controller][Channel][Byte][0];
                *Margin   = Start;
                Margin[1] = End;
                ByteCenter[Byte] = Center;
              } else if (param == RdV) {
                // Read Vref
                GetSetVal = Center;
                for (RankRx = 0; RankRx < MAX_RANK_IN_CHANNEL; RankRx++) {
                  Margin    = &Outputs->MarginResult[LastRxV][RankRx][Controller][Channel][Byte][0];
                  *Margin   = Start;
                  Margin[1] = End;
                }
                if (!Inputs->LoopBackTest) {
                  MrcGetSetChStrb (MrcData, Controller, Channel, Byte, RxVref, WriteOffsetCached, &GetSetVal);
                }
              } else if (param == CmdV) {
                // Command Vref
                Margin      = &Outputs->MarginResult[LastCmdV][Rank][Controller][Channel][Byte][0];
                Margin[0]   = (ABS ((*LargestPS) * 10));
                Margin[1]   = (ABS ((*LargestPE) * 10));
              }
            } // for Byte
          }

          if ((param == WrV) && (Ddr4 || Ddr5)) {
            /// Calculate Non-PDA TxV Offset - average Per byte offsets
            RankCenter = 0;
            for (Byte = 0; Byte < SdramCount ; Byte++) {
              RankCenter += ByteCenter[Byte];
            }
            RankCenter /= SdramCount;
            // Write using PDA only when we have a passing window.
            if ((EyeTooSmall[Controller][Channel][Rank] && !Ddr5) || (Ddr5 && !Inputs->EnablePda)) {
              // If no passing window - Program only Non-PDA value and Update PDA/Non-PDA cached value
              ChMarginStatus |= ChangeMargin (MrcData, param, RankCenter, 0, 0, Controller, Channel, RankMask, 0, 0, 1, 1);
            } else {
              // If passing window:
              if (Ddr4) {
                // 1. Program Non-PDA value (without updating cached value of PDA/Non-PDA)
                ChMarginStatus |= ChangeMargin (MrcData, param, RankCenter, 0, 0, Controller, Channel, RankMask, 0, 0, 0, 1);
              } else { // DDR5
                ChannelOut->Mr10PdaEnabled = TRUE;
              }
              // 2. Program PDA value and PDA cached value
              if (Ddr4 || Inputs->EnablePda) {
                for (Byte = 0; Byte < SdramCount; Byte++) {
                  ChMarginStatus |= ChangeMargin (MrcData, param, ByteCenter[Byte], 0, 0, Controller, Channel, RankMask, 1 << Byte, 0, 1, 1);
                }
                if (Ddr4) {
                  // 3. Update Non-PDA cached value
                  UpdatePdaCenterDdr4 (MrcData, Controller, Channel, RankMask);
                } else { // Ddr5
                  UpdatePdaCenterDdr5 (MrcData, Controller, Channel, RankMask, WrV);
                }
              }
            }
          }
          if ((param == WrV) && Outputs->Lpddr) {
            Center = (LargestPSCh[Controller][Channel] + LargestPECh[Controller][Channel]) / 2;
            MRC_DEBUG_MSG (
              Debug,
              DebugLevel,
              "  Channel %u Summary : \n   %d\t%d\t%d\t%d\n",
              Channel,
              LargestPSCh[Controller][Channel],
              LargestPECh[Controller][Channel],
              LargestPECh[Controller][Channel] - LargestPSCh[Controller][Channel],
              Center
              );
            ChMarginStatus |= ChangeMargin (MrcData, param, Center, 0, 0, Controller, Channel, 1 << Rank, 0, 0, 1, 0);
          }
        } // for Channel
      }
    } // for Controller
    if ((param == RdV) || (param == RcvEnaX)) {
      break; // We don't use different Rx vref per rank and RcvEnX already stress on all rank.
    }
  } // for Rank
  if ((param == WrV) && Ddr4) {
    Outputs->PdaEnable = TRUE;
  }

  IoReset (MrcData);
  return Status | ChMarginStatus;
}

/**
  This procedure is meant to handle much more complex centering that will use a 2D algorithm to optimize asymetical
  eyes for both timing and voltage margin.

  @param[in,out] MrcData         - Include all MRC global data.
  @param[in,out] MarginResult    - Margin data from centering
  @param[in]     McChBitMaskIn   - MC Channel bit mask.
  @param[in]     param           - {0:RcvEn, 1:RdT, 2:WrT, 3: WrDqsT, 4:RdV, 5:WrV, 6:WrLevel,
                                    8:WrTBit, 9:RdTBit, 10:RdVBit,
                                    16:RdFan2, 17:WrFan2, 32:RdFan3, 32:WrFan3}
                                    ONLY RdT and WrT are allowed in this function
  @param[in]     EnPerBit        - Option to enable per bit margining
  @param[in]     EnRxDutyCycleIn - Phase to center.
  @param[in]     ResetPerBit     - Option to Reset PerBit Deskew to middle value before byte training
  @param[in]     LoopCount       - loop count
  @param[in]     En2D            - Option to only run center at nominal Vref point

  @retval MrcStatus -  If succeeded, return mrcSuccess
**/
MrcStatus
DataTimeCentering2D (
  IN OUT MrcParameters *const MrcData,
  IN OUT UINT16               MarginResult[MAX_RESULT_TYPE][MAX_RANK_IN_CHANNEL][MAX_CONTROLLER][MAX_CHANNEL][MAX_SDRAM_IN_DIMM][MAX_EDGES],
  IN     const UINT8          McChBitMaskIn,
  IN     const UINT8          Param,
  IN     const UINT8          EnPerBit,
  IN     const UINT8          EnRxDutyCycleIn,
  IN     const UINT8          ResetPerBit,
  IN     const UINT8          LoopCount,
  IN     const UINT8          En2D
)
{
  static const UINT32      EHWeights[]       = { 6, 2, 1, 0, 2, 1, 0 };
  static const UINT32      EWWeights[]       = { 0, 1, 2, 3, 1, 2, 3 };
  // To speed up the test in CTE
  static const INT32       VrefPointsConst[] = { 0, -25, -50, -75, 25, 50, 75};
  const MrcInput    *Inputs;
  MrcDebug          *Debug;
  const MRC_FUNCTION *MrcCall;
  MrcOutput         *Outputs;
  MrcControllerOut  *ControllerOut;
  MrcChannelOut     *ChannelOut;
  UINT8             *RxDqPbCenter;
  UINT8             *TxDqPbCenter;
  UINT16            centerTiming;
  UINT16            *Margin;
  UINT16            *Eye;
  INT32             *CenterBit;
  INT32             *CSum;
  MrcStatus         Status;
  UINT8             ResultType;
  UINT8             Controller;
  UINT8             Channel;
  UINT8             Rank;
  UINT8             RankMask;
  UINT16            Byte;
  UINT8             Bit;
  UINT8             MaxChannels;
  UINT8             SdramCount;
  UINT8             ParamV;
  UINT8             ParamB;
  UINT8             MaxVScale;
  UINT8             EnPerBitEH;
  UINT8             Strobe;
  UINT8             Strobes;
  UINT8             Vref;
  UINT8             SaveLC;
  UINT8             LCloop;
  UINT8             i;
  UINT8             SkipWait;
  UINT8             McChBitMask;
  UINT8             EnRxDutyCycle;
  UINT8             Edge;
  UINT8             BMap[9];
  UINT8             LoopEnd;
  UINT16            Mode;
  UINT16            MarginBit[MAX_CONTROLLER][MAX_CHANNEL][MAX_SDRAM_IN_DIMM][MAX_BITS][MAX_EDGES];
  UINT32            OBit;
  INT32             Center;
  UINT32            Weight;
  INT32             VrefPoints[sizeof (VrefPointsConst) / sizeof (VrefPointsConst[0])];
  UINT32            SumEH;
  UINT32            SumEW;
  UINT32            BERStats[4];
  UINT16            VrefScale[MAX_CONTROLLER][MAX_CHANNEL][MAX_SDRAM_IN_DIMM][MAX_EDGES];
  UINT16            EH[MAX_CONTROLLER][MAX_CHANNEL][MAX_SDRAM_IN_DIMM];
  UINT16            EW[MAX_CONTROLLER][MAX_CHANNEL][MAX_SDRAM_IN_DIMM];
  UINT16            EyeShape[7][MAX_CONTROLLER][MAX_CHANNEL][MAX_SDRAM_IN_DIMM][MAX_EDGES]; // Store all eye edges for Per Bit
  INT32             CenterSum[MAX_CONTROLLER][MAX_CHANNEL][MAX_SDRAM_IN_DIMM];
  INT32             PStrobeOffset[MAX_CONTROLLER][MAX_CHANNEL][MAX_SDRAM_IN_DIMM];
  INT32             DivBy;
  INT8              DivBySign;
  INT32             Value0;
  INT32             CenterSumBit[MAX_CONTROLLER][MAX_CHANNEL][MAX_SDRAM_IN_DIMM][MAX_BITS];
  INT16             Calc;
  INT64             GetSetVal;
  INT64             DqBitDelayMax;
  INT64             BitMidDelay;
  BOOLEAN           Ddr4;
  BOOLEAN           PdaMode;
  UINT32            PerBitRank;
  GSM_GT            PerBitGt;
  UINT8             UpdateHost;
#ifdef MRC_DEBUG_PRINT
  GSM_GT            GroupType;
  INT64             GetSetVal2;
  UINT8             SpaceCount;
  UINT32            Index;
#endif // MRC_DEBUG_PRINT
#ifdef BDAT_SUPPORT
  UINT32            EyeType;
  UINT32            MarginValue;
#endif // BDAT_SUPPORT

  Inputs      = &MrcData->Inputs;
  Outputs     = &MrcData->Outputs;
  Debug       = &Outputs->Debug;
  MaxChannels = Outputs->MaxChannels;
  SdramCount  = Outputs->SdramCount;
  MrcCall     = Inputs->Call.Func;
  // 2D Margin Types (Voltage, Time)
  // RdFan2:  Margins both RdV and RdT at { (off, -2/3*off),  (off, 2/3*off) }
  // WrFan2:  Margins both WrV and WrT at { (off, -2/3*off),  (off, 2/3*off) }
  // RdFan3:  Margins both RdV and RdT at { (off, -2/3*off),  (5/4*off, 0),  (off, 2/3*off)  }
  // WrFan3:  Margins both WrV and WrT at { (off, -2/3*off),  (5/4*off, 0),  (off, 2/3*off)  }
  if ((Param != RdT) && (Param != WrT)) {
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_ERROR, "DataTimeCentering2D: Incorrect Margin Parameter %d\n", Param);
    return mrcWrongInputParameter;
  }

  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Parameter = %d (%sT)\n", Param, (Param == RdT) ? "Rd" : "Wr");
  McChBitMask   = McChBitMaskIn;
  EnRxDutyCycle = EnRxDutyCycleIn;
  Status        = mrcSuccess;
  MaxVScale     = 100;
  Strobes       = 2;
  Center        = 0;
  Value0        = 0;
  Ddr4          = (Outputs->DdrType == MRC_DDR_TYPE_DDR4);
  MrcCall->MrcSetMem ((UINT8 *) BERStats, sizeof (BERStats), 0);
  for (i = 0; i < (sizeof (BMap) / sizeof (BMap[0])); i++) {
    BMap[i] = i;
  }

  ResultType = GetMarginResultType (Param);

  EnPerBitEH = 1; // Repeat EH Measurement after byte training, before bit training

                  // SOE = 10b ( Stop on All Byte Groups Error )
  SetupIOTestBasicVA (MrcData, McChBitMask, LoopCount - 1, NSOE, 0, 0, 8, PatWrRd, 0, 0);
  Outputs->DQPat = RdRdTA;

  // Duty cycle should be ONLY for Rx
  if (Param != RdT) {
    EnRxDutyCycle = 0;
  }

  Strobes = 1 + EnRxDutyCycle;

  // Option to only run center at nominal Vref point
  if (En2D == 0) {
    MrcCall->MrcSetMem ((UINT8 *) &VrefPoints[0], sizeof (VrefPoints), 0);
  } else {
    MrcCall->MrcCopyMem ((UINT8 *) &VrefPoints[0], (UINT8 *) &VrefPointsConst[0], sizeof (VrefPoints));
  }

  // Calculate SumEH / SumEW for use in weighting equations
  SumEH = SumEW = 0;
  for (Vref = 0; Vref < sizeof (VrefPoints) / sizeof (VrefPoints[0]); Vref++) {
    SumEH += EHWeights[Vref];
    SumEW += EWWeights[Vref];

    // Loop once at nominal Vref point
    if (En2D == 0) {
      break;
    }
  }

  if (Param == RdT) {
    ParamV = RdV;
    ParamB = RdTBit;
    PerBitGt = RxDqsBitDelay;
  } else {
    ParamV = WrV;
    ParamB = WrTBit;
    PerBitGt = TxDqBitDelay;
  }

  if ((Param == RdV) || (ParamV == RdV)) {
    // Make sure RxVref values exist in CR cache for this algorithm to work, because we will use WriteOffsetUncached
    MrcReadParamIntoCache (MrcData, Param);
  }

  // Get Max and current value for TxDqBitDelay and RxDqsBitDelay
  MrcGetSetLimits (MrcData, PerBitGt, NULL, &DqBitDelayMax, NULL);
  BitMidDelay = (((UINT8) DqBitDelayMax) + 1) / 2;
  PdaMode = Ddr4 && (ParamV == WrV);
  if ((Outputs->DdrType == MRC_DDR_TYPE_DDR5) && (ParamV == WrV) && Inputs->EnablePda) {
    PdaMode = TRUE;
  }
  UpdateHost = 0;

  // Optimize timing per rank
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Optimization is per rank\n");
  for (Rank = 0; Rank < MAX_RANK_IN_CHANNEL; Rank++) {
    McChBitMask = 0;
    RankMask    = 1 << Rank;

    // Reset PerBit Deskew to middle value before byte training
    // Write timing offset for bit[x] of the DQ byte. Linear from 0-MAX, each step size is tQCLK/64
    // Read timing offset for bit[x] of the DQ byte. Linear from 0-MAX, each step size is tQCLK/64
    if (ResetPerBit == 1) {
      if (Param == WrT) {
        MrcGetSetStrobe (MrcData, MAX_CONTROLLER, MAX_CHANNEL, MAX_RANK_IN_CHANNEL, MAX_SDRAM_IN_DIMM, TxDqsBitDelay, WriteToCache | GSM_UPDATE_HOST, &BitMidDelay);
      }
      MrcGetSetBit (MrcData, MAX_CONTROLLER, MAX_CHANNEL, MAX_RANK_IN_CHANNEL, MAX_SDRAM_IN_DIMM, MAX_BITS, PerBitGt, WriteToCache | GSM_UPDATE_HOST, &BitMidDelay);
      MrcFlushRegisterCachedData (MrcData);
    }

    // Select rank for REUT test
    for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
      for (Channel = 0; Channel < MaxChannels; Channel++) {
        if ((1 << ((Controller * MaxChannels) + Channel)) & McChBitMaskIn) {
          McChBitMask |= SelectReutRanks (MrcData, Controller, Channel, (MRC_BIT0 << Rank), FALSE, 0);
        }
      } // for Channel
    } // for Controller
    // Continue if this rank does not exist on any of the channels
    if (McChBitMask == 0) {
      continue;
    }

    //####################################################
    //######   Get EH to scale vref sample point by  #####
    //####################################################
    // Pass the host last edges by reference
    // Get EH/VrefScale for the use in timing centering
    if (En2D > 0) {
      Status = DQTimeCenterEH (
        MrcData,
        McChBitMask,
        Rank,
        ParamV,
        BMap,
        PdaMode,
        EH,
        VrefScale,
        BERStats
      );
      if (Status != mrcSuccess) {
        MRC_DEBUG_MSG (Debug, MSG_LEVEL_WARNING, "\nDQTimeCenterEH FAILED - Using VrefScale / 2 = %d\n", MaxVScale / 2);
        for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
          for (Channel = 0; Channel < MaxChannels; Channel++) {
            MrcCall->MrcSetMemWord (&VrefScale[Controller][Channel][0][0], SdramCount * MAX_EDGES, MaxVScale / 2);
          }
        }
      }
    } else {
      for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
        for (Channel = 0; Channel < MaxChannels; Channel++) {
          MrcCall->MrcSetMemWord (&EH[Controller][Channel][0], SdramCount, 1);
          MrcCall->MrcSetMemWord (&VrefScale[Controller][Channel][0][0], SdramCount * MAX_EDGES, 1);
        }
      }
    }

    Status = GetMarginByte (MrcData, MarginResult, Param, Rank, (MRC_BIT0 << Rank));

#ifdef MRC_DEBUG_PRINT
    // Read the margins
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\nLstSavd Margins ");
    for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
      for (Channel = 0; Channel < MaxChannels; Channel++) {
        if (!((1 << ((Controller * MaxChannels) + Channel)) & McChBitMask)) {
          continue;
        }

        for (Byte = 0; Byte < SdramCount; Byte++) {
          MRC_DEBUG_MSG (
            Debug,
            MSG_LEVEL_NOTE,
            "%2d %2d\t",
            MarginResult[ResultType][Rank][Controller][Channel][Byte][0] / 10,
            MarginResult[ResultType][Rank][Controller][Channel][Byte][1] / 10
          );
        }
      }
    }
#endif // MRC_DEBUG_PRINT

    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\nRank: %d", Rank);
    for (Strobe = 0; Strobe < Strobes; Strobe++) {
#ifdef MRC_DEBUG_PRINT
      MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\nStrobe: %d\nMc\t\t", Strobe);
      for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
        if (MrcControllerExist (MrcData, Controller)) {
          MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "%d", Controller);
          if (MrcControllerExist (MrcData, Controller + 1)) {
            SpaceCount = (MaxChannels * SdramCount);
            for (Index = 0; Index < SpaceCount; Index++) {
              MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\t");
            }
          }
        }
      }
      MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\nChannel\t\t");
      for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
        if (MrcControllerExist (MrcData, Controller)) {
          for (Channel = 0; Channel < MaxChannels; Channel++) {
            if (MrcChannelExist (MrcData, Controller, Channel)) {
              MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "%d", Channel);
              if (Channel < (MaxChannels - 1)) {
                SpaceCount = SdramCount;
                for (Index = 0; Index < SpaceCount; Index++) {
                  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\t");
                }
              }
            }
          }
        }
      }
      MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\nByte\t\t");
      for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
        if (MrcControllerExist (MrcData, Controller)) {
          for (Channel = 0; Channel < MaxChannels; Channel++) {
            if (MrcChannelExist (MrcData, Controller, Channel)) {
              for (Byte = 0; Byte < SdramCount; Byte++) {
                if (MrcByteExist (MrcData, Controller, Channel, Byte)) {
                  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "%d", Byte);
                  if ((Channel < (MaxChannels - 1)) || (Byte < (SdramCount - 1))) {
                    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\t");
                  }
                }
              }
              MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\t");
            }
          }
        }
      }
#endif
      //####################################################
      //######   Measure Eye Width at all Vref Points  #####
      //####################################################
      // Program Selective error checking for RX. if strobe = 0 then Check even else Check odd
      if (EnRxDutyCycle) {
        MrcSetChunkAndClErrMsk (MrcData, 0xFF, (0x55555555 << Strobe));
      }

      // Loop through all the Vref Points to Test
      for (Vref = 0; Vref < sizeof (VrefPoints) / sizeof (VrefPoints[0]); Vref++) {
        // Setup Vref Voltage for this point
        for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
          for (Channel = 0; Channel < MaxChannels; Channel++) {
            if ((1 << ((Controller * MaxChannels) + Channel)) & McChBitMask) {
              SkipWait = (McChBitMask >> ((Controller * MaxChannels) + (Channel + 1))); // Skip if there are more channels

              LoopEnd = (UINT8) (((ParamV == RdV) || PdaMode) ? SdramCount : 1);
              for (Byte = 0; Byte < LoopEnd; Byte++) {
                Value0 = (INT32) (MIN (VrefScale[Controller][Channel][Byte][0], VrefScale[Controller][Channel][Byte][1]));
                Value0 = (Param == RdT) ? 0 : (VrefPoints[Vref] * Value0) / MaxVScale;
                Status = ChangeMargin (
                  MrcData,
                  ParamV,
                  Value0,
                  0,
                  0,
                  Controller,
                  Channel,
                  RankMask,
                  PdaMode ? (1 << Byte) : Byte,
                  0,
                  UpdateHost,
                  SkipWait
                );
                //MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "  MC%u C%u B%u VrefScale: %u, Value0: %d\n", Controller, Channel, Byte, VrefScale[Controller][Channel][Byte], Value0);
              }
            }
          }
        }

        MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\nVref = %d:\t", Value0);

        // Run Margin Test
        Mode = 0;
        Status = MrcGetBERMarginByte (
          MrcData,
          MarginResult,
          McChBitMask,
          RankMask,
          RankMask,
          Param,
          Mode,
          BMap,
          0,
          31,
          0,
          BERStats
        );

        // Store Results; Setup Vref Voltage for this point
        for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
          for (Channel = 0; Channel < MaxChannels; Channel++) {
            if ((1 << ((Controller * MaxChannels) + Channel)) & McChBitMask) {

              for (Byte = 0; Byte < SdramCount; Byte++) {
              Margin = &MarginResult[ResultType][Rank][Controller][Channel][Byte][0];
                MRC_DEBUG_MSG (
                  Debug,
                  MSG_LEVEL_NOTE,
                  "%2d %2d\t",
                  Margin[0] / 10,
                  Margin[1] / 10
                );

                Center = (INT32) (Margin[1] - *Margin);
                if (Vref == 0) {
                  EW[Controller][Channel][Byte] = (Margin[1] + *Margin) / 10;
                  CenterSum[Controller][Channel][Byte] = 0;
                }

                // Calculate weight for this point
                Weight = EHWeights[Vref] * EH[Controller][Channel][Byte] + EWWeights[Vref] * EW[Controller][Channel][Byte];
                CenterSum[Controller][Channel][Byte] += Weight * Center;

                // Store Edges for Per Bit deskew
                Eye = &EyeShape[Vref][Controller][Channel][Byte][0];
                for (Edge = 0; Edge < MAX_EDGES; Edge++) {
                  Eye[Edge] = Margin[Edge];
                }

                // RunTime Improvement.  Set margin back to Vref = 0 point when the sign  of the VrefPoint changes
                if ((VrefPoints[Vref] < 0) &&
                  (Vref < (sizeof (VrefPoints) / sizeof (VrefPoints[0]) - 1)) &&
                  (VrefPoints[Vref + 1] > 0)
                  ) {
                  Eye = &EyeShape[0][Controller][Channel][Byte][0];
                  for (Edge = 0; Edge < MAX_EDGES; Edge++) {
                    Margin[Edge] = Eye[Edge];
                  }
                }
              }
            }
          } // for Channel
        } // for Controller

          // Loop once at nominal Vref point
        if (En2D == 0) {
          Vref = sizeof (VrefPoints) / sizeof (VrefPoints[0]);
        }
      }
      if (ParamV == RdV) {
        // Restore RdV register back to zero offset
        ChangeMargin (MrcData, ParamV, 0, 0, 1, 0, 0, 0, 0, 0, UpdateHost, 0);
      }

      //####################################################
      //############    Center Results per Byte  ###########
      //####################################################
      MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\n\nWeighted Center\t");

      for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
        ControllerOut = &Outputs->Controller[Controller];
        for (Channel = 0; Channel < MaxChannels; Channel++) {
          if ((1 << ((Controller * MaxChannels) + Channel)) & McChBitMask) {
            // Calculate and apply CenterPoint.  Round to Nearest Int
            for (Byte = 0; Byte < SdramCount; Byte++) {
              DivBy = (SumEH * EH[Controller][Channel][Byte] + SumEW * EW[Controller][Channel][Byte]);
              if (DivBy == 0) {
                MRC_DEBUG_MSG (Debug, MSG_LEVEL_ERROR, "DataTimeCentering2D: Divide by zero\n");
                return mrcFail;
              }

              CSum = &CenterSum[Controller][Channel][Byte];
              DivBySign = (*CSum < 0) ? (-1) : 1;

              *CSum = (*CSum + 10 * (DivBySign * DivBy)) / (20 * DivBy);
              MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "%d\t", *CSum);

              // Apply new centerpoint
              GetSetVal = *CSum;
              if (Param == RdT) {
                if (Strobe == 0) {
                  PStrobeOffset[Controller][Channel][Byte] = *CSum;
                }
                // If EnRxDutyCycle is 0, we will store the data in the array above and go into this if-case.
                if ((!EnRxDutyCycle) || (Strobe == 1)) {
                  MrcGetSetStrobe (MrcData, Controller, Channel, Rank, Byte, RxDqsNDelay, WriteOffsetToCache, &GetSetVal);
                  GetSetVal = PStrobeOffset[Controller][Channel][Byte];
                  MrcGetSetStrobe (MrcData, Controller, Channel, Rank, Byte, RxDqsPDelay, WriteOffsetToCache, &GetSetVal);
                  MrcFlushRegisterCachedData (MrcData);
                }
              } else {
                MrcGetSetStrobe (MrcData, Controller, Channel, Rank, Byte, TxDqDelay, WriteOffsetCached, &GetSetVal);
              }

              // Update the Eye Edges
              for (Vref = 0; Vref < sizeof (VrefPoints) / sizeof (VrefPoints[0]); Vref++) {
                Calc = (INT16) (10 * *CSum);
                Eye = &EyeShape[Vref][Controller][Channel][Byte][0];
                *Eye += Calc;
                Eye[1] -= Calc;

                // Loop once at nominal Vref point
                if (En2D == 0) {
                  Vref = sizeof (VrefPoints) / sizeof (VrefPoints[0]);
                }
              }

              // Update MrcData for future tests (MarginResult points back to MrcData)
              // EyeShape for Vref 0 is assumed to have the best shape for future tests.
              for (Edge = 0; Edge < MAX_EDGES; Edge++) {
              MarginResult[ResultType][Rank][Controller][Channel][Byte][Edge] = (EyeShape[0][Controller][Channel][Byte][Edge]);
              }
            } // for Byte
          }
        } // for Channel
      } // for Controller

      centerTiming = 0;
#ifdef MRC_DEBUG_PRINT
      MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\nFinal Center\t");
      for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
        ControllerOut = &Outputs->Controller[Controller];
        for (Channel = 0; Channel < MaxChannels; Channel++) {
          if ((1 << ((Controller * MaxChannels) + Channel)) & McChBitMask) {
            ChannelOut = &ControllerOut->Channel[Channel];

            // Calculate final center point relative to "zero" as in the 1D case
            for (Byte = 0; Byte < SdramCount; Byte++) {
              if (Param == RdT) {
                if ((!EnRxDutyCycle) || (Strobe == 1)) {
                  GroupType = RxDqsNDelay;
                } else {
                  GroupType = RxDqsPDelay;
                }
                MrcGetSetStrobe (MrcData, Controller, Channel, Rank, Byte, GroupType, ReadFromCache, &GetSetVal);
                centerTiming = (UINT8) (GetSetVal - 32);
              } else {
                MrcGetSetStrobe (MrcData, Controller, Channel, Rank, Byte, TxDqDelay, ReadFromCache, &GetSetVal);
                MrcGetSetStrobe (MrcData, Controller, Channel, Rank, Byte, TxDqsDelay, ReadFromCache, &GetSetVal2);
                centerTiming = (UINT16) (GetSetVal - (GetSetVal2 + 32));
              }

              MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "%d\t", (INT8) centerTiming);
            }
          }
        } // for Channel
      } // for Controller
#endif // MRC_DEBUG_PRINT
    } // End of Byte Centering

      //######################################################
      //############     Measure Eye Width Per BIT  ##########
      //######################################################

    if (EnPerBit) {
#ifdef MRC_DEBUG_PRINT
      MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\n\nEdges we pass on to GetMarginBit are\n");
      for (Vref = 0; Vref < sizeof (VrefPoints) / sizeof (VrefPoints[0]); Vref++) {
        MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\t\t");

        // Setup Vref Voltage for this point
        for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
          for (Channel = 0; Channel < MaxChannels; Channel++) {
            if ((1 << ((Controller * MaxChannels) + Channel)) & McChBitMask) {
              for (Byte = 0; Byte < SdramCount; Byte++) {
                for (Edge = 0; Edge < MAX_EDGES; Edge++) {
                  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "%d ", EyeShape[Vref][Controller][Channel][Byte][Edge]);
                }
              }
            }
          } // for Channel
        } // for Controller

        MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\n");
        if (En2D == 0) {
          Vref = sizeof (VrefPoints) / sizeof (VrefPoints[0]);
        }
      }
#endif // MRC_DEBUG_PRINT

      // Configure the PBD array with current or reset value
      if (ResetPerBit == 1) {
        MrcCall->MrcSetMemWord (&MarginBit[0][0][0][0][0], MAX_CONTROLLER * MAX_CHANNEL * MAX_SDRAM_IN_DIMM * MAX_BITS * MAX_EDGES, (UINT8) BitMidDelay);
      } else {
        for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
          for (Channel = 0; Channel < MaxChannels; Channel++) {
            if (!MrcRankExist (MrcData, Controller, Channel, Rank)) {
              continue;
            }
            for (Byte = 0; Byte < SdramCount; Byte++) {
              for (Bit = 0; Bit < MAX_BITS; Bit++) {
                MrcGetSetBit (MrcData, Controller, Channel, Rank, Byte, Bit, PerBitGt, ReadFromCache, &GetSetVal);
                MarginBit[Controller][Channel][Byte][Bit][0] = (UINT16) GetSetVal;
                MarginBit[Controller][Channel][Byte][Bit][1] = (UINT16) GetSetVal;
              }
            }
          }
        }
      }

      MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\n\n### Measure Eye Width Per BIT\n");
      // Recalculate the EH after the Byte Centering
      if (EnPerBitEH && (En2D > 0)) {
        Status = DQTimeCenterEH (
          MrcData,
          McChBitMask,
          Rank,
          ParamV,
          BMap,
          PdaMode,
          EH,
          VrefScale,
          BERStats
        );
        if (Status != mrcSuccess) {
          MRC_DEBUG_MSG (Debug, MSG_LEVEL_WARNING, "\nDQTimeCenterEH FAILED - Using VrefScale / 2 = %d\n", MaxVScale / 2);
          for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
            for (Channel = 0; Channel < MaxChannels; Channel++) {
              MrcCall->MrcSetMemWord (&VrefScale[Controller][Channel][0][0], SdramCount * MAX_EDGES, MaxVScale / 2);
            }
          }
        }
      }

      // No stop on error or selective error checking
      // Stop on all lane fail
#ifdef MRC_DEBUG_PRINT
      MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Mc\t\t");
      for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
        if (MrcControllerExist (MrcData, Controller)) {
          MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "%d", Controller);
          if (MrcControllerExist (MrcData, Controller + 1)) {
            SpaceCount = (MaxChannels * SdramCount  * ((MAX_BITS * 3) + 4)) - 1;
            for (Index = 0; Index < SpaceCount; Index++) {
              MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, " ");
            }
          }
        }
      }
      MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\nChannel\t\t");
      for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
        for (Channel = 0; Channel < MaxChannels; Channel++) {
          if (MrcChannelExist (MrcData, Controller, Channel)) {
            MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "%d", Channel);
            // Print space only if not last channel
            if (Channel < (MaxChannels - 1)) {
              SpaceCount = (SdramCount  * ((MAX_BITS * 3) + 4)) - 1;
              for (Index = 0; Index < SpaceCount; Index++) {
                MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, " ");
              }
            }
          }
        }
      }
#endif // MRC_DEBUG_PRINT
      MrcSetChunkAndClErrMsk(MrcData, 0xFF, 0xFFFFFFFF);
      MrcSetupTestErrCtl (MrcData, ALSOE, 1);

#ifdef MRC_DEBUG_PRINT
      MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\n");
      for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
        for (Channel = 0; Channel < MaxChannels; Channel++) {
          if ((1 << ((Controller * MaxChannels) + Channel)) & McChBitMask) {
            MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Byte\t\t");
            for (Byte = 0; Byte < SdramCount; Byte++) {
              MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "%d", Byte);
              // Print space only if not last byte
              if (Byte < (SdramCount - 1)) {
                SpaceCount = ((MAX_BITS * 3) + 4) - 1;
                for (Index = 0; Index < SpaceCount; Index++) {
                  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, " ");
                }
              }
            }
          }
        }
      }

      MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\n");
#endif // MRC_DEBUG_PRINT

      // Loop through all the Vref Points to Test
      SaveLC = Outputs->DQPatLC;
      for (Vref = 0; Vref < sizeof (VrefPoints) / sizeof (VrefPoints[0]); Vref++) {
        // Setup Vref Voltage for this point
        for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
          for (Channel = 0; Channel < MaxChannels; Channel++) {
            if ((1 << ((Controller * MaxChannels) + Channel)) & McChBitMask) {
              // Skip if there are more channels
              SkipWait = (McChBitMask >> ((Controller * MaxChannels) + Channel + 1));

              LoopEnd = (UINT8) (((ParamV == RdV) || PdaMode) ? SdramCount : 1);
              for (Byte = 0; Byte < LoopEnd; Byte++) {
                Value0 = (INT32) (MIN (VrefScale[Controller][Channel][Byte][0], VrefScale[Controller][Channel][Byte][1]));
                Value0 = (VrefPoints[Vref] * Value0) / MaxVScale;
                Status = ChangeMargin (
                  MrcData,
                  ParamV,
                  Value0,
                  0,
                  0,
                  Controller,
                  Channel,
                  RankMask,
                  PdaMode ? (1 << Byte) : Byte,
                  0,
                  UpdateHost,
                  SkipWait
                );
              }
            }

            MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Mc %d Channel %d Vref = %d\n", Controller, Channel, Value0);
          }
        }

        // Run Margin Test; Loop through 2 times. Once at low loop count and Once at high loopcount. Improves runtime
        // @todo: Need loop count of 2 when not using BASICVA
        for (LCloop = 0; LCloop < 1; LCloop++) {
          Outputs->DQPatLC = (LCloop == 0) ? 1 : SaveLC;

          Mode = 0;
          Status = MrcGetMarginBit (MrcData, McChBitMask, Rank, MarginBit, EyeShape[Vref], ParamB, Mode, (UINT8) DqBitDelayMax, MRC_PRINTS_ON);
        }

        // Store Results
        // Setup Vref Voltage for this point
        for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
          for (Channel = 0; Channel < MaxChannels; Channel++) {
            if ((1 << ((Controller * MaxChannels) + Channel)) & McChBitMask) {
              ChannelOut = &Outputs->Controller[Controller].Channel[Channel];
              for (Byte = 0; Byte < SdramCount; Byte++) {
                // Calculate weight for this point
                //Weight = EHWeights[Vref] * EH[Controller][Channel][Byte] + EWWeights[Vref] * EW[Controller][Channel][Byte];
                for (Bit = 0; Bit < MAX_BITS; Bit++) {
                  Margin = &MarginBit[Controller][Channel][Byte][Bit][0];
                  CSum = &CenterSumBit[Controller][Channel][Byte][Bit];
                  OBit = (Param == RdT) ? ChannelOut->RxDqPb[Rank][Byte][Bit].Center : ChannelOut->TxDqPb[Rank][Byte][Bit].Center;
                  Center = ((Margin[1] - (UINT8) OBit) - ((UINT8) OBit - *Margin));
                  if (Vref == 0) {
                    *CSum = 0;
                  }
                  *CSum += Center;
                }
              }
            }
          } // for Channel
        } // for Controller
        // Loop once at nominal Vref point
        if (En2D == 0) {
          Vref = sizeof (VrefPoints) / sizeof (VrefPoints[0]);
        }
      }

      //######################################################
      //#############     Center Result Per BIT  #############
      //######################################################
      // Cleanup after test.  Go back to the per byte setup: NSOE and all comparisons on Chunk and Cacheline
      MrcSetChunkAndClErrMsk(MrcData, 0xFF, 0xFFFFFFFF);
      MrcSetupTestErrCtl (MrcData, NSOE, 1);

      MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\nPer Bit Offset\n");
      for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
        ControllerOut = &Outputs->Controller[Controller];
        for (Channel = 0; Channel < MaxChannels; Channel++) {
          if ((1 << ((Controller * MaxChannels) + Channel)) & McChBitMask) {
            ChannelOut = &ControllerOut->Channel[Channel];

            // Calculate and apply CenterPoint.  Round to Nearest Int
            for (Byte = 0; Byte < SdramCount; Byte++) {
              MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Mc%u.Ch%u.B%d:\t", Controller, Channel, Byte);

              PerBitRank = 0;
              for (Bit = 0; Bit < MAX_BITS; Bit++) {
                CenterBit = &CenterSumBit[Controller][Channel][Byte][Bit];
                RxDqPbCenter = &ChannelOut->RxDqPb[Rank][Byte][Bit].Center;
                TxDqPbCenter = &ChannelOut->TxDqPb[Rank][Byte][Bit].Center;

                DivBySign = (*CenterBit < 0) ? (-1) : 1;
                *CenterBit = (*CenterBit + DivBySign) / 2;
                MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "%8d ", *CenterBit);

                // Centerpoint needs to be added to starting DqPb value
                *CenterBit += (Param == RdT) ? (INT32) *RxDqPbCenter : (INT32) *TxDqPbCenter;

                // Check for saturation
                if (*CenterBit > DATA0CH0_CR_DDRDATADQRANK0LANE0_txdqperbitdeskew_MAX) {  // Rx/Tx have the same MAX
                  *CenterBit = DATA0CH0_CR_DDRDATADQRANK0LANE0_txdqperbitdeskew_MAX;
                } else if (*CenterBit < 0) {
                  *CenterBit = 0;
                }

                PerBitRank = *CenterBit;
                // Apply new centerpoint
                // ParamB already has the proper per bit parameter based on Param
                GetSetVal = (UINT32) PerBitRank;
                MrcGetSetBit (MrcData, Controller, Channel, Rank, Byte, Bit, PerBitGt, WriteToCache | GSM_UPDATE_HOST, &GetSetVal);
              } // bit
              MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\n");
            } // Byte
          } // Valid Channel
        } // Channel
      } // Controller
#ifdef MRC_DEBUG_PRINT
      MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\n%s%s\n", gStartTagStr, GsmGtDebugStrings[PerBitGt]);
      for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
        for (Channel = 0; Channel < MaxChannels; Channel++) {
          if (!MrcRankExist (MrcData, Controller, Channel, Rank)) {
            continue;
          }
          for (Byte = 0; Byte < SdramCount; Byte++) {
            for (Bit = 0; Bit < MAX_BITS; Bit++) {
              MrcGetSetBit (MrcData, Controller, Channel, Rank, Byte, Bit, PerBitGt, ReadFromCache | PrintValue, &GetSetVal);
            }
          }
        }
      }
      MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "%s%s\n", gStopTagStr, GsmGtDebugStrings[PerBitGt]);
#endif
      MrcFlushRegisterCachedData (MrcData);
      // Stop on all lane fail
      MrcSetChunkAndClErrMsk(MrcData, 0xFF, 0xFFFFFFFF);
      MrcSetupTestErrCtl (MrcData, ALSOE, 1);

#if 0  // This code is for debug purposes ONLY if we want to know the perbyte margins after calling the perbit centering
#ifdef MRC_DEBUG_PRINT
      MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\n\nEdges\t");
      for (Vref = 0; Vref < (sizeof (VrefPoints) / sizeof (VrefPoints[0])); Vref++) {
        for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
          for (Channel = 0; Channel < MaxChannels; Channel++) {
            if ((1 << ((Controller * MaxChannels) + Channel)) & McChBitMask) {
              for (Byte = 0; Byte < SdramCount; Byte++) {
                for (Edge = 0; Edge < MAX_EDGES; Edge++) {
                  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "%d ", EyeShape[Vref][Controller][Channel][Byte][Edge]);
                }
              }
            }
          }
        }

        MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\n");
        if (En2D == 0) {
          Vref = sizeof (VrefPoints) / sizeof (VrefPoints[0]);
        }
      }

      MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\nCalling GetMarginBit with per Byte Timing\nByte\n");
      for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
        for (Channel = 0; Channel < MaxChannels; Channel++) {
          if ((1 << ((Controller * MaxChannels) + Channel)) & McChBitMask) {
            for (Byte = 0; Byte < SdramCount; Byte++) {
              MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "%d\t\t\t\t\t", Byte);
            }
          }
        }
      }

      for (Vref = 0; Vref < (sizeof (VrefPoints) / sizeof (VrefPoints[0])); Vref++) {
        Mode = 0;
        Status = MrcGetMarginBit (MrcData, McChBitMask, Rank, MarginBit, EyeShape[Vref], Param, Mode, 31, MRC_PRINTS_ON);

        // Loop once at nominal Vref point
        if (En2D == 0) {
          Vref = sizeof (VrefPoints) / sizeof (VrefPoints[0]);
        }
      }

      MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\nPerByte Margins after Bit Centering\nLeft\tRight\tCenter\n");
      for (Vref = 0; Vref < (sizeof (VrefPoints) / sizeof (VrefPoints[0])); Vref++) {
        for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
          MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Controller %d\n", Controller);
          for (Channel = 0; Channel < MaxChannels; Channel++) {
            if ((1 << ((Controller * MaxChannels) + Channel)) & McChBitMask) {
              MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Channel %d\n", Channel);
              for (Byte = 0; Byte < SdramCount; Byte++) {
                MRC_DEBUG_MSG (
                  Debug,
                  MSG_LEVEL_NOTE,
                  "%d\t%d\t%d\n",
                  EyeShape[Vref][Controller][Channel][Byte][0],
                  EyeShape[Vref][Controller][Channel][Byte][1],
                  ((EyeShape[Vref][Controller][Channel][Byte][1] - EyeShape[Vref][Controller][Channel][Byte][0]) / (2 * 10))
                );
              }
            }
          }
        }

        MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\n");
        if (En2D == 0) {
          Vref = sizeof (VrefPoints) / sizeof (VrefPoints[0]);
        }
      }
#endif // MRC_DEBUG_PRINT
#endif

      // Cleanup after test
      MrcSetChunkAndClErrMsk(MrcData, 0xFF, 0xFFFFFFFF);
      MrcSetupTestErrCtl (MrcData, NSOE, 1);
    } // if (EnPerBit)

#ifdef BDAT_SUPPORT
    if (Inputs->BdatEnable && (Inputs->BdatTestType == Margin2DType)) {
      if ((Param == RdT) || (Param == WrT)) {
        Outputs->NumCL = 128;
        EyeType = (Param == RdT) ? 0 : 1;
        for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
          ControllerOut = &Outputs->Controller[Controller];
          for (Channel = 0; Channel < MaxChannels; Channel++) {
            if ((1 << ((Controller * MaxChannels) + Channel)) & McChBitMask) {
              ChannelOut = &ControllerOut->Channel[Channel];
              for (Vref = 0; Vref < ARRAY_COUNT (VrefPoints); Vref++) {
                for (Edge = 0; Edge < MAX_EDGES; Edge++) {
                  MarginValue = MRC_UINT32_MAX;
                  for (Byte = 0; Byte < Outputs->SdramCount; Byte++) {
                    MarginValue = MIN (MarginValue, (UINT8) EyeShape[Vref][Controller][Channel][Byte][Edge]);
                  }
                  Outputs->Margin2DResult[EyeType][Rank][Channel][Vref][Edge] = (UINT8) (MarginValue / 10);
                }
              }
            }
          }
        }
      }
    }
#endif // BDAT_SUPPORT

    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\n");
  } //End of Rank

  // Clean Up after test
  Byte = PdaMode ? 0x1FF : 0;
  Status = ChangeMargin (MrcData, ParamV, 0, 0, 1, 0, 0, 0, Byte, 0, UpdateHost, 0);

  // Trigger train reset(DdrMiscControl1.io_train_rst) at the end of the training.
  IoReset (MrcData);

  return Status;
}

/**
  This procedure is meant to find the calibrated step size for Per Bit DeSkew
  @param[in,out] MrcData         - Include all MRC global data.
  @param[in,out] MarginResult    - Margin data from centering
  @param[in]     ChBitMaskIn     - Channel bit mask.
  @param[in]     param           - {0:RcvEn, 1:RdT, 2:WrT, 3: WrDqsT, 4:RdV, 5:WrV, 6:WrLevel,
                                    8:WrTBit, 9:RdTBit, 10:RdVBit,
                                    16:RdFan2, 17:WrFan2, 32:RdFan3, 32:WrFan3}
                                    ONLY RdT is allowed in this function
  @param[in]     LoopCount       - loop count

  @retval MrcStatus -  If succeeded, return mrcSuccess
**/
MrcStatus
GetPerBitDeSkewStep (
  IN OUT MrcParameters *const MrcData,
  IN OUT UINT16               MarginResult[MAX_RESULT_TYPE][MAX_RANK_IN_CHANNEL][MAX_CONTROLLER][MAX_CHANNEL][MAX_SDRAM_IN_DIMM][MAX_EDGES],
  IN     const UINT8          ChBitMaskIn,
  IN     const UINT8          Param,
  IN     const UINT8          LoopCount
  )
{
  const MrcInput    *Inputs;
  MrcDebug          *Debug;
  const MRC_FUNCTION *MrcCall;
  MrcOutput         *Outputs;
  MrcStatus         Status;
  UINT8             ResultType;
  UINT8             Channel;
  UINT8             i;
  UINT8             ChBitMask;
  UINT8             Rank;
  UINT8             RankMask;
  UINT8             BMap[9];
  UINT16            Byte;
  UINT8             MarginResMed[MAX_CHANNEL][MAX_SDRAM_IN_DIMM];
  UINT8             MarginDiff[MAX_CHANNEL][MAX_SDRAM_IN_DIMM];
  UINT8             RxDeskewCal[MAX_CHANNEL];
  UINT32            AverPiDiff[MAX_CHANNEL];
  UINT8             LeftMargin;
  UINT8             RightMargin;
  UINT32            BERStats[4];
  INT64             CalStep;
  UINT8             ScaleCalculated[MAX_CHANNEL];
  BOOLEAN           CalDone;
  UINT8             MarginLoop;
  UINT32            PiTickPs;
  UINT32            PerPartConstant;
  UINT8             DeskewChBitDone;
  INT64             GetSetVal;
  UINT8             RxDeskewCalMax;
  UINT8             MidValPBD;
  UINT8             MaxValPBD;
  BOOLEAN           ForceAllSteps;

  Inputs = &MrcData->Inputs;
  Outputs = &MrcData->Outputs;
  Debug = &Outputs->Debug;
  MrcCall = Inputs->Call.Func;
  ResultType = GetMarginResultType(Param);
  ChBitMask = ChBitMaskIn;
  Status = mrcSuccess;
  MrcCall->MrcSetMem((UINT8 *)BERStats, sizeof(BERStats), 0);
  for (i = 0; i < (sizeof(BMap) / sizeof(BMap[0])); i++) {
    BMap[i] = i;
  }
  for (i = 0; i < MAX_CHANNEL; i++) {
    RxDeskewCal[i] = 0xFF;
  }
  ForceAllSteps = FALSE;

  if (Param != RdT) {
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_ERROR, "GetPerBitDeSkewStep: Incorrect Margin Parameter %d\n", Param);
    return mrcWrongInputParameter;
  }

  // PerPartConstant = Round ((MaxValPBD /(MaxValPBD - MidRangePBD)) *PiTick_PSEC)
  MrcGetSetLimits (MrcData, RxDqsBitDelay, NULL, &GetSetVal, NULL);
  MaxValPBD = (UINT8) GetSetVal;
  MidValPBD = (MaxValPBD + 1) / 2;
  PiTickPs = UDIVIDEROUND (Outputs->UIps, 64);
  PerPartConstant = UDIVIDEROUND ((MaxValPBD * PiTickPs), (MaxValPBD - MidValPBD));
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "PiTickPs = %d\n\n", PiTickPs);
  // @todo Update with McChBitMask
  SetupIOTestBasicVA (MrcData, ChBitMask, LoopCount, NSOE, 0, 0, 8, PatWrRd, 0, 0);

  // Max Step size for Per Bit Deskew calibration.
  MrcGetSetLimits (MrcData, GsmIocVccDllRxDeskewCal, NULL, &GetSetVal, NULL);
  RxDeskewCalMax = (UINT8) GetSetVal;
  DeskewChBitDone = 0;

  //Find rx-deskew-cal value per part for the 1st populated rank
  for (Rank = 0; Rank < MAX_RANK_IN_CHANNEL; Rank++) {
    ChBitMask = 0;
    RankMask  = 1 << Rank;
    for (Channel = 0; Channel < MAX_CHANNEL; Channel++) {
      // If we have "cal" value for this channel then skip
      if ((ForceAllSteps == FALSE) && (RxDeskewCal[Channel] != 0xFF)) {
        continue;
      }
      if ((MRC_BIT0 << Channel) & ChBitMaskIn) {
        ChBitMask |= SelectReutRanks (MrcData, /**Controller**/ 0, Channel, (MRC_BIT0 << Rank), FALSE, 0);
      }
    }
    // Continue if this rank does not exist on any of the channels
    if (ChBitMask == 0) {
      continue;
    }

    CalStep = 0;
    CalDone = FALSE;
    // Find rxdeskew cal value for this rank
    while ((CalDone == FALSE) && (CalStep <= RxDeskewCalMax)) {
      for (Channel = 0; Channel < MAX_CHANNEL; Channel++) {
        if (!((1 << Channel) & ChBitMask)) {
          continue;
        }
        // Set RX/TX Deskew cal for this channel
        // If we have "cal" value for this channel then skip
        if ((ForceAllSteps == FALSE) && (RxDeskewCal[Channel] != 0xFF)) {
          continue;
        }
        MrcGetSetDdrIoGroupChannelStrobe (MrcData, Channel, MAX_SDRAM_IN_DIMM, GsmIocVccDllRxDeskewCal, WriteToCache, &CalStep);
        MrcGetSetDdrIoGroupChannelStrobe (MrcData, Channel, MAX_SDRAM_IN_DIMM, GsmIocVccDllTxDeskewCal, WriteToCache, &CalStep);
      } // Channel

      MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Calibration Point %d\nRank %d\n", CalStep, Rank);
      // Run MrcGetBERMarginByte for MidRangePBD and MaxValPBD
      for (MarginLoop = 0; MarginLoop < 2; MarginLoop++) {
        // MarginLoop == 0 indicates MidValPBD (32) and MarginLoop == 1 indicates MaxValPBD (63) in the following code.
        GetSetVal = (MarginLoop == 0) ? MidValPBD : MaxValPBD;
        MrcGetSetDdrIoGroupSocket0 (MrcData, MAX_CHANNEL, Rank, MAX_SDRAM_IN_DIMM, MAX_BITS, RxDqsBitDelay,  WriteToCache, &GetSetVal);
        MrcFlushRegisterCachedData (MrcData);

        // Run Margin Test
        Status = MrcGetBERMarginByte (
          MrcData,
          MarginResult,
          ChBitMask,
          RankMask,
          RankMask,
          Param,
          0,
          BMap,
          0,
          31,
          0,
          BERStats
          );

        // Read the margins and store per byte Left edge results for both ResMid and ResMax
        MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\n%s = 0x%X\n", (MarginLoop == 0) ? "MidRangePBD" : "MaxValPBD ", (UINT32) GetSetVal);
        MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\tByte\t0\t1\t2\t3\t4\t5\t6\t7\t8\n");
        for (Channel = 0; Channel < MAX_CHANNEL; Channel++) {
          if (!((1 << Channel) & ChBitMask)) {
            continue;
          }
          MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Channel %d:\t", Channel);
          for (Byte = 0; Byte < Outputs->SdramCount; Byte++) {
            LeftMargin = (UINT8) (MarginResult[ResultType][Rank][/**Controller**/ CONTROLLER_0][Channel][Byte][0] / 10);
            RightMargin = (UINT8) (MarginResult[ResultType][Rank][/**Controller**/ CONTROLLER_0][Channel][Byte][1] / 10);
            if (MarginLoop == 0) {
              // save Left Margins for both MidPBD and MaxPBD
              MarginResMed[Channel][Byte] = LeftMargin;
            } else {
              // Store Margin calculated between left edges from Mid and Max margins
              MarginDiff[Channel][Byte] = ABS ((MarginResMed[Channel][Byte]) - LeftMargin);
            }
            MRC_DEBUG_MSG(Debug, MSG_LEVEL_NOTE, "%2d-%2d\t", LeftMargin, RightMargin);
          } // byte loop
          MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\n");
        } // /Channel
      } // Margin Loop
      MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\n");

      // Calculate the target PI diff for the applied PBD range.
      // The goal is to reach at least 100psec full PBD range w / minimal scale factor to retain small enough step size.
      MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Margin Difference: MarginResMed[Left]-MarginResMax[Left])\n");
      MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\tByte\t 0  1  2  3  4  5  6  7  8\n");
      for (Channel = 0; Channel < MAX_CHANNEL; Channel++) {
        AverPiDiff[Channel] = 0;
        if (!((1 << Channel) & ChBitMask)) {
          continue;
        }
        MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Channel %d:\t", Channel);
        for (Byte = 0; Byte < Outputs->SdramCount; Byte++) {
          MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "%2d ", MarginDiff[Channel][Byte]);
          AverPiDiff[Channel] += MarginDiff[Channel][Byte];
        }
        MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\n", Channel);
        // Average of Margin difference
        AverPiDiff[Channel] = UDIVIDEROUND (AverPiDiff[Channel], (Outputs->SdramCount));

        // FullRangePsec = AverPiDiff * PerPartConstant. Scale = Floor(100/FullRangeSpec)
        if (AverPiDiff[Channel] != 0) {
          ScaleCalculated[Channel] = (UINT8) DIVIDEFLOOR (100, (PerPartConstant * AverPiDiff[Channel]));
        } else {
          MRC_DEBUG_MSG (Debug, MSG_LEVEL_ERROR, "\nERROR: Channel %d:Average Pi Difference cannot be zero\n", Channel);
          ScaleCalculated[Channel] = 1;
        }

        // Check if we reached the 100psec target
        if ((ScaleCalculated[Channel] == 0) || (CalStep == RxDeskewCalMax)) {
          if (RxDeskewCal[Channel] == 0xFF) {
            RxDeskewCal[Channel] = (UINT8) CalStep;
            DeskewChBitDone |= 1 << Channel;
          }
        }
      } // Channel loop
      MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\n");
      CalStep++;

#ifdef MRC_DEBUG_PRINT
      for (Channel = 0; Channel < MAX_CHANNEL; Channel++) {
        if (!((1 << Channel) & ChBitMask)) {
          continue;
        }
        MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Channel %d\nAveragePiDiff in ticks = %d\nFullRangeSpecPsec = %d\nScale for 100ps => %d\n\n",
                       Channel,
                       AverPiDiff[Channel],
                       PerPartConstant * AverPiDiff[Channel],
                       ScaleCalculated[Channel]
                       );
      }
#endif // MRC_DEBUG_PRINT

      if (DeskewChBitDone == ChBitMask) {
        if (ForceAllSteps == FALSE) {
          // Got 100psec target.
          CalDone = TRUE;
        }
      }
    } // while
  } //rank loop

  // Program the final RxDeskewCal and TxDeskewCal value
  for (Channel = 0; Channel < MAX_CHANNEL; Channel++) {
    GetSetVal = RxDeskewCal[Channel];
    if (GetSetVal <= RxDeskewCalMax) {
      MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Channel %d Final RX/TX deskew cal = %d\n", Channel, GetSetVal);
      MrcGetSetDdrIoGroupSocket0 (MrcData, Channel, MAX_RANK_IN_CHANNEL, MAX_SDRAM_IN_DIMM, MAX_BITS, GsmIocVccDllRxDeskewCal, WriteToCache, &GetSetVal);
      MrcGetSetDdrIoGroupSocket0 (MrcData, Channel, MAX_RANK_IN_CHANNEL, MAX_SDRAM_IN_DIMM, MAX_BITS, GsmIocVccDllTxDeskewCal, WriteToCache, &GetSetVal);
    }
  }

  // Set PBD back to MidRangePBD
  GetSetVal = MidValPBD;
  MrcGetSetDdrIoGroupSocket0 (MrcData, MAX_CHANNEL, MAX_RANK_IN_CHANNEL, MAX_SDRAM_IN_DIMM, MAX_BITS, RxDqsBitDelay, WriteToCache, &GetSetVal);
  MrcFlushRegisterCachedData (MrcData);

  return Status;
}

/**
  Subfunction of 2D Timing Centering
  Measures paramV margin across ch/bytes and updates the EH/VrefScale variables

  @param[in]     MrcData     - Include all MRC global data.
  @param[in]     McChBitMask - MC Channel Bit mak for which test should be setup for.
  @param[in]     rank        - Defines rank to used for MrcData
  @param[in]     ParamV      - Margin parameter
  @param[in]     BMap        - Byte mapping to configure error counter control register
  @param[in]     PdaMode     - TRUE if PDA is used for WrV
  @param[in,out] EH          - Structure that stores start, stop and increment details for address
  @param[in,out] VrefScale   - Parameter to be updated
  @param[in,out] BERStats    - Bit Error Rate Statistics.

  @retval mrcSuccess if successful, otherwise the function returns an error status.
**/
MrcStatus
DQTimeCenterEH (
  IN     MrcParameters * const MrcData,
  IN     const UINT8           McChBitMask,
  IN     const UINT8           rank,
  IN     const UINT8           ParamV,
  IN     UINT8 * const         BMap,
  IN     const BOOLEAN         PdaMode,
  IN OUT UINT16                EH[MAX_CONTROLLER][MAX_CHANNEL][MAX_SDRAM_IN_DIMM],
  IN OUT UINT16                VrefScale[MAX_CONTROLLER][MAX_CHANNEL][MAX_SDRAM_IN_DIMM][MAX_EDGES],
  IN OUT UINT32 * const        BERStats
  )
{
  const MRC_FUNCTION *MrcCall;
  const MrcInput  *Inputs;
  MrcOutput       *Outputs;
  UINT16          *MarginResult;
  UINT16          *VrefS;
  MrcStatus       Status;
  UINT8           ResultType;
  UINT8           Controller;
  UINT8           Channel;
  UINT8           RankMask;
  UINT8           Byte;
  UINT16          MinVrefScale;
  UINT16          Mode;
  UINT8           MaxVref;

  Inputs  = &MrcData->Inputs;
  MrcCall = Inputs->Call.Func;
  Outputs = &MrcData->Outputs;
  MaxVref = GetVrefOffsetLimits (MrcData, ParamV);
  RankMask = 1 << rank;

  Mode    = 0;
  Status  = GetMarginByte (MrcData, Outputs->MarginResult, ParamV, rank, (MRC_BIT0 << rank));
  if (mrcSuccess == Status) {
    Status = MrcGetBERMarginByte (
              MrcData,
              Outputs->MarginResult,
              McChBitMask,
              RankMask,
              RankMask,
              ParamV,
              Mode,
              BMap,
              1,
              MaxVref,
              0,
              BERStats
              );
    if (mrcSuccess == Status) {
      Status = ScaleMarginByte (MrcData, Outputs->MarginResult, ParamV, rank);
      if (mrcSuccess == Status) {
        ResultType = GetMarginResultType (ParamV);

        // Update VrefScale with results
        for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
          for (Channel = 0; (Channel < Outputs->MaxChannels) && (mrcSuccess == Status); Channel++) {
            if ((1 << ((Controller * Outputs->MaxChannels) + Channel)) & McChBitMask) {
              // Calculate EH and VrefScale
              MinVrefScale = MRC_UINT16_MAX;
              for (Byte = 0; Byte < Outputs->SdramCount; Byte++) {
                MarginResult = &Outputs->MarginResult[ResultType][rank][Controller][Channel][Byte][0];
                VrefS = &VrefScale[Controller][Channel][Byte][0];
                EH[Controller][Channel][Byte] = (*MarginResult + *(MarginResult + 1)) / 10;
                *VrefS = *MarginResult / 10;
                VrefS[1] = *(MarginResult + 1) / 10;

                if (MinVrefScale > *VrefS) {
                  MinVrefScale = *VrefS;
                }
                if (MinVrefScale > VrefS[1]) {
                  MinVrefScale = VrefS[1];
                }

                // Scale host back to correct values
                Status = ScaleMarginByte (MrcData, Outputs->MarginResult, ParamV, rank);
                if (mrcSuccess != Status) {
                  break;
                }
                // For Tx without PDA, use the same Vref limit for all bytes. Store result in byte0
                if ((ParamV == WrV) && !PdaMode) {
                  MrcCall->MrcSetMemWord (&VrefScale[Controller][Channel][0][0], Outputs->SdramCount * MAX_EDGES, MinVrefScale);
                }
              }
            }
          }
        }
      }
    }
  }
  // Updates EH and VrefScale
  return Status;
}

/**
  This step prints out the key training parameters that are margined by Rank Margin Tool.
  This will allow tracking of current training settings across multiple steps

  @retval Nothing
**/
void
MrcTrainedStateTrace (
  IN  MrcParameters *const  MrcData
  )
{
  const GSM_GT  ByteBasedParams[] = {RecEnDelay, RxDqsPDelay, RxDqsNDelay, TxDqsDelay, TxDqDelay, RxVref};
  const GSM_GT  CmdParams[]       = {CmdGrpPi, CtlGrpPi, ClkGrpPi};
  MrcOutput *Outputs;
  INT64     GetSetDummy;
  UINT32    ParamIdx;
  UINT32    Index;
  UINT32    IndexEnd;
  UINT32    Controller;
  UINT32    Channel;
  UINT32    Rank;
  UINT32    Byte;
  UINT8     MaxChannels;

  GetSetDummy = 0;
  Outputs = &MrcData->Outputs;
  MaxChannels = Outputs->MaxChannels;

  for (ParamIdx = 0; ParamIdx < (sizeof (ByteBasedParams) / sizeof (ByteBasedParams[1])); ParamIdx++) {
    for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
      for (Channel = 0; Channel < MaxChannels; Channel++) {
        for (Rank = 0; Rank < MAX_RANK_IN_CHANNEL; Rank++) {
          if (!MrcRankExist (MrcData, Controller, Channel, Rank)) {
            continue;
          }
          for (Byte = 0; Byte < Outputs->SdramCount; Byte++) {
            MrcGetSetStrobe (MrcData, Controller, Channel, Rank, Byte, ByteBasedParams[ParamIdx], ReadFromCache | PrintValue, &GetSetDummy);
          }
        }
      }
    }
  }

  for (ParamIdx = 0; ParamIdx < (sizeof (CmdParams) / sizeof (CmdParams[1])); ParamIdx++) {
    IndexEnd = ((Outputs->Lpddr) || (CmdParams[ParamIdx] != CmdGrpPi)) ? 1 : (Outputs->DdrType == MRC_DDR_TYPE_DDR5) ? 2 : 4;
    for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
      for (Channel = 0; Channel < MaxChannels; Channel++) {
        for (Rank = 0; Rank < MAX_RANK_IN_CHANNEL; Rank++) {
          if (!MrcRankExist (MrcData, Controller, Channel, Rank)) {
            continue;
          }
          for (Index = 0; Index < IndexEnd; Index++) {
            MrcGetSetCcc (MrcData, Controller, Channel, Rank, Index, CmdParams[ParamIdx], ReadFromCache | PrintValue, &GetSetDummy);
          }
        }
      }
    }
  }
  //@todo: Add Command Vref, DIMM Mode Register Parameters
}

/**
  Update the CA/DQ Vref value

  @param[in,out] MrcData             - Include all MRC global data.
  @param[in]     McChannelMask       - Select the Channels to update Vref for.
  @param[in]     RankMask            - Selecting which Rank to talk to (DDR4: WrV only, LPDDR4: WrV and CmdV)
  @param[in]     DeviceMask          - Selecting which Devices to talk to (only valid for DDR4 and adjusting VrefDQ)
  @param[in]     VrefType            - Determines the Vref to change: WrV or CmdV only.
  @param[in]     Offset              - Vref offset value.
  @param[in]     UpdateMrcData       - Used to decide if Mrc host must be updated.
  @param[in]     PDAmode             - Selecting to use MRH or old method for MRS (only valid for DDR4 and adjusting VrefDQ)
  @param[in]     SkipWait            - Determines if we will wait for vref to settle after writing to register
  @param[in]     IsCachedOffsetParam - Determines if the parameter is an offset (relative to cache) or absolute value.

  @retval Nothing
**/
void
MrcUpdateVref (
  IN OUT MrcParameters *const MrcData,
  IN     UINT32               McChannelMask,
  IN     UINT8                RankMask,
  IN     UINT16               DeviceMask,
  IN     UINT8                VrefType,
  IN     INT32                Offset,
  IN     BOOLEAN              UpdateMrcData,
  IN     BOOLEAN              PDAmode,
  IN     BOOLEAN              SkipWait,
  IN     BOOLEAN              IsCachedOffsetParam
  )
{
  MrcDebug            *Debug;
  MrcInput            *Inputs;
  MrcOutput           *Outputs;
  MrcIntOutput        *IntOutputs;
  const MRC_FUNCTION  *MrcCall;
  INT64               GetSetVal;
  INT64               GetSetValChannels[MAX_CHANNEL];
  INT64               VrefConverged;
  INT64               CkeOn;
  UINT32              Rank;
  UINT32              Dimm;
  UINT16              NominalVref;
  UINT32              Count;
  UINT8               Controller;
  UINT8               Channel;
  BOOLEAN             Ddr4;
  BOOLEAN             NeedUpdateChannelData;
  UINT8               LowGear;

  IntOutputs       = (MrcIntOutput *)(MrcData->IntOutputs.Internal);
  Inputs           = &MrcData->Inputs;
  Outputs          = &MrcData->Outputs;
  Debug            = &Outputs->Debug;
  Ddr4             = Outputs->DdrType == MRC_DDR_TYPE_DDR4;
  MrcCall          = Inputs->Call.Func;
  LowGear          = (Outputs->DdrType == MRC_DDR_TYPE_LPDDR4) ? MrcGear1 : MrcGear2;


  if ((VrefType != WrV) && (VrefType != CmdV)) {
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_ERROR, "Function MrcUpdateVref, Invalid parameter %d\n", VrefType);
    return;
  }
  MrcCall->MrcSetMem ((UINT8 *) GetSetValChannels, sizeof(GetSetValChannels), 0);
  if (!((VrefType == CmdV) && Ddr4)) {
    if (Outputs->Lpddr && (VrefType == CmdV)) {
      // We will program MR14 for FSP[1] while staying at low speed in FSP[0]
      MrcSetFspVrcg (MrcData, ALL_RANK_MASK, MRC_IGNORE_ARG_8, Lp4FspWePoint1, LpFspOpPoint0);
      // Set Low frequency (LP4:1067, LP5:2200), unless we're already there
      // This will also update Gear2 mode in MC/DDRIO, in LP4 we switch to Gear1 when going down to 1067.
      // Lower CKE - change CLK frequency while DRAM is in Power Down
      CkeOn = 0;
      MrcGetSetMcCh (MrcData, MAX_CONTROLLER, MAX_CHANNEL, GsmMccCkeOn, WriteNoCache, &CkeOn);
      if ((Outputs->Frequency != Outputs->LowFrequency) && Inputs->LpFreqSwitch) {
        MrcFrequencySwitch (MrcData, Outputs->LowFrequency, LowGear, !Outputs->RestoreMRs);
      }
      // Restore CKE
      MrcCkeOnProgramming(MrcData);
    } // LPDDR4/5 and CmdV
    for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
      for (Channel = 0; Channel < Outputs->MaxChannels; Channel++) {
        if ((!MrcChannelExist (MrcData, Controller, Channel)) ||
            ((MC_CH_MASK_CHECK (McChannelMask, Controller, Channel, Outputs->MaxChannels)) == 0)){
          continue;
        }
        MrcSetDramVref (
          MrcData,
          Controller,
          Channel,
          RankMask,
          DeviceMask,
          VrefType,
          Offset,
          UpdateMrcData,
          PDAmode,
          IsCachedOffsetParam
          );
      } // For Channel
    } // For Controller
    if (Outputs->Lpddr && (VrefType == CmdV) && Outputs->EctDone) {
      // Set FSP-OP = 1, set High frequency
      MrcLpddrSwitchToHigh (MrcData, !Outputs->RestoreMRs);
    }
    return;
  } // end if (VrefType != WrV) && (VrefType != CmdV)

  // Below code is for DDR4 CmdV only, controlled via PHY register / CA_VREF pin
  if (VrefType == CmdV) {
    for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
      if (MrcControllerExist (MrcData, Controller)) {
        for (Channel = 0; Channel < Outputs->MaxChannels; Channel++) {
          if ((!MrcChannelExist (MrcData, Controller, Channel)) || ((MC_CH_MASK_CHECK (McChannelMask, Controller, Channel, Outputs->MaxChannels)) == 0)) {
            continue;
          }
          NeedUpdateChannelData = FALSE;
          GetSetVal = Offset;  // Avoid compiling warning
          // 2. Write Vref value - updated cached value.
          // CmdVref's fields are from a single register and we must ensure cache is updated,
          // Thus ForceWriteOffsetCached is required. Later we restore the cache according to UpdateMrcData.
          for (Dimm = 0; Dimm < MAX_DIMMS_IN_CHANNEL; Dimm++) {
            Rank = 2 * Dimm;
            if (((DIMM_TO_RANK_MASK (Dimm) & RankMask) != 0) || (RankMask == 0)) {
              GetSetVal = Offset;
              // CmdV DDR4:
              // 1. Read the Cached offset values.
              if (IsCachedOffsetParam) {
                if (Inputs->IsVrefCAPerDimm) {
                  NominalVref = IntOutputs->Controller[Controller].CaVref[Channel][Dimm];
                } else {
                  // Vref CA is per-Channel
                  NominalVref = IntOutputs->Controller[Controller].CaVref[Channel][0];
                }
                GetSetVal += NominalVref;
              }
              MrcGetSetMcChRnk (MrcData, Controller, Channel, Rank, CmdVref, ForceWriteCached, &GetSetVal);
              if (UpdateMrcData) {
                // Update nominal CaVref point
                if (Inputs->IsVrefCAPerDimm) {
                  IntOutputs->Controller[Controller].CaVref[Channel][Dimm] = (UINT16) GetSetVal;
                } else {
                  NeedUpdateChannelData = TRUE;
                  /*
                   * Continue to update another Dimm's CmdVref, in case that
                   * users might forget to set IsVrefCAPerDimm=TRUE although
                   * board uses per dimm VrefCA.
                   */
                }
              }
            }
          }
          if (NeedUpdateChannelData) {
            /*
             * Vref CA is per-Channel and only update once
             * Do update here instead of in above loop, because
             * users might forget to set IsVrefCAPerDimm=TRUE although
             * board uses per dimm VrefCA.
             */
            IntOutputs->Controller[Controller].CaVref[Channel][0] = (UINT16) GetSetVal;
          }

          // 3. Wait for Vref to settle.  Note VrefCA needs longer to settle.
          if (!SkipWait) {
            Count = 0;
            while (Count < 250) { // up to 250 us
              VrefConverged = 1;
              for (Dimm = 0; Dimm < MAX_DIMMS_IN_CHANNEL; Dimm++) {
                Rank = 2 * Dimm;
                if (MrcRankExist (MrcData, Controller, Channel, Rank)) {    // Only poll on populated DIMMs
                  MrcGetSetMcChRnk (MrcData, Controller, Channel, Rank, GsmIocCmdVrefConverge, ReadUncached, &GetSetVal);
                  VrefConverged &= GetSetVal;
                }
              }
              if (VrefConverged == 1) {
                break;
              }
              MrcWait (MrcData, 1 * MRC_TIMER_1US);
              Count += 1;
            }
            if (VrefConverged == 0) {
              MRC_DEBUG_MSG (Debug, MSG_LEVEL_WARNING, "CAVref circuit failed to converge: MC%u C%u\n", Controller, Channel);
              for (Dimm = 0; Dimm < MAX_DIMMS_IN_CHANNEL; Dimm++) {
                Rank = 2 * Dimm;
                if (MrcRankExist (MrcData, Controller, Channel, Rank)) {
                  MrcGetSetMcChRnk (MrcData, Controller, Channel, Rank, GsmIocCmdVrefConverge, ReadUncached | PrintValue, &GetSetVal);
                }
              }
            }
            MrcWait (MrcData, 5 * MRC_TIMER_1US); // Add 5us to make sure everything is done
          }
        } // Channel Loop
      } // If MrcControllerExist
    } //Controller loop
  } // if (VrefType == CmdV)
}

/**
  This function is used to move CMD/CTL/CLK/CKE PIs during training

  @param[in,out] MrcData    - Include all MRC global data.
  @param[in]     Controller - Controller to shift PI.
  @param[in]     Channel    - Channel to shift PI.
  @param[in]     Iteration  - Determines which PI to shift:
                              MrcIterationClock = 0
                              MrcIterationCmd   = 1
                              MrcIterationCtl   = 2
  @param[in]     RankMask   - Ranks to work on
  @param[in]     GroupMask  - Which groups to work on for CLK/CMD/CTL. See MrcGetCmdGroupMax()
  @param[in]     NewValue   - value to shift in case of CLK Iteration, New value for all other cases
  @param[in]     UpdateHost - Determines if MrcData structure is updated

  @retval Nothing
**/
void
ShiftPIforCmdTraining (
  IN OUT MrcParameters *const MrcData,
  IN     const UINT32         Controller,
  IN     const UINT32         Channel,
  IN     const UINT8          Iteration,
  IN     const UINT8          RankMask,
  IN     const UINT8          GroupMask,
  IN     INT32                NewValue,
  IN     const UINT8          UpdateHost
  )
{
  MrcDebug            *Debug;
  MrcOutput           *Outputs;
  MrcIntOutput        *IntOutputs;
  MrcIntCmdTimingOut  *IntCmdTiming;
  MrcDdrType          DdrType;
  GSM_GT              GsmMode;
  INT64               GetSetVal;
  INT64               GroupLimitMax;
  UINT32              ByteMask;
  UINT8               Rank;
  UINT8               Group;
  UINT8               CmdGroupMax;
  INT16               Shift;
  BOOLEAN             Lpddr;
  BOOLEAN             Lpddr4;
  BOOLEAN             Ddr4;
  BOOLEAN             Ddr5;

  Outputs       = &MrcData->Outputs;
  Debug         = &Outputs->Debug;
  IntOutputs    = (MrcIntOutput *) (MrcData->IntOutputs.Internal);
  IntCmdTiming  = &IntOutputs->Controller[Controller].CmdTiming[Channel];
  DdrType       = Outputs->DdrType;
  Lpddr         = Outputs->Lpddr;
  Lpddr4        = (DdrType == MRC_DDR_TYPE_LPDDR4);
  Ddr4          = (DdrType == MRC_DDR_TYPE_DDR4);
  Ddr5          = (DdrType == MRC_DDR_TYPE_DDR5);
  GsmMode       = ForceWriteCached;
  CmdGroupMax   = MrcGetCmdGroupMax (MrcData);

  if (Iteration != MrcIterationClock) {
    MrcGetSetLimits (MrcData, (Iteration == MrcIterationCmd) ? CmdGrpPi : CtlGrpPi, NULL, &GroupLimitMax, NULL);
    if ((NewValue < 0) || (NewValue > GroupLimitMax)) {
      MRC_DEBUG_MSG(Debug, MSG_LEVEL_NOTE, "Mc%d.C%d ShiftPIforCmdTraining: value (%d) out of limits, ", Controller, Channel, NewValue);
      if (NewValue < 0) {
        NewValue = 0;
      } else if (NewValue > GroupLimitMax) {
        NewValue = (UINT16)GroupLimitMax;
      }
      MRC_DEBUG_MSG(Debug, MSG_LEVEL_NOTE, "New value = %d\n", NewValue);
    }
  }
  //MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\nShiftPIforCmdTraining: Iteration: %d, Channel: %d, RankMask: %d, GroupMask: %d, NewValue = 0x%x\n", Iteration, Channel, RankMask, GroupMask, NewValue);

  switch (Iteration) {
    case MrcIterationClock: // SHIFT CLOCK
      MrcGetSetLimits (MrcData, ClkGrpPi, NULL, &GroupLimitMax, NULL);
      GroupLimitMax++; // round up max (256/512)
      ByteMask  = 0x1FF;    // Shift DQ PI on all 9 bytes by default on DDR4

      if (Lpddr) {
        // In LPDDR clocks not per rank
        // LPDDR clocks are per channel
        Shift = (INT16) ((IntCmdTiming->ClkPiCode[0] + NewValue) % (UINT16) GroupLimitMax);
        if (Shift < 0) {
          Shift = (INT16)(GroupLimitMax - ABS (Shift));
        }

        GetSetVal = Shift;
        MrcGetSetCcc (MrcData, Controller, Channel, MRC_IGNORE_ARG, 0, ClkGrpPi, GsmMode, &GetSetVal);
        if (UpdateHost) {
          IntCmdTiming->ClkPiCode[0] = (UINT16) GetSetVal;
        }

        // Each clock spans all ranks, so need to shift DQ PIs on all ranks, for bytes in this channel.
        ByteMask = (1 << Outputs->SdramCount) - 1;
        for (Rank = 0; Rank < MAX_RANK_IN_CHANNEL; Rank++) {
          if (MrcRankExist (MrcData, Controller, Channel, Rank)) {
            ShiftDQPIs (MrcData, Controller, Channel, Rank, ByteMask, (INT16) NewValue, UpdateHost);
          }
        }
      } else {
        for (Rank = 0; Rank < MAX_RANK_IN_CHANNEL; Rank++) {
          if (RankMask & (1 << Rank)) {
            Shift = (INT16) (((IntCmdTiming->ClkPiCode[Rank]) + NewValue) % (UINT16) GroupLimitMax);
            if (Shift < 0) {
              Shift = (INT16)(GroupLimitMax - ABS (Shift));
            }

            GetSetVal = Shift;
            MrcGetSetCcc (MrcData, Controller, Channel, Rank, 0, ClkGrpPi, GsmMode, &GetSetVal);
            if (UpdateHost) {
              IntCmdTiming->ClkPiCode[Rank] = (UINT16) GetSetVal;
            }

            ShiftDQPIs (MrcData, Controller, Channel, Rank, ByteMask, (INT16) NewValue, UpdateHost);
          }
        }
      }
      break;

    case MrcIterationCmd: // Shift Command
      GetSetVal = NewValue;
      for (Group = 0; Group < CmdGroupMax; Group++) {
        if ((1 << Group) & GroupMask) {
          MrcGetSetCcc (MrcData, Controller, Channel, MRC_IGNORE_ARG, Group, CmdGrpPi, GsmMode, &GetSetVal);
          if (UpdateHost) {
            IntCmdTiming->CmdPiCode[Group] = (UINT16) GetSetVal;
          }
        }
      }
      break;

    case MrcIterationCtl: // Shift CS/ODT and CKE.Control
      GetSetVal = NewValue;
      // Shift CS/ODT/CKE
      for (Rank = 0; Rank < MAX_RANK_IN_CHANNEL; Rank++) {
        if (RankMask & (1 << Rank)) {
          MrcGetSetCcc (MrcData, Controller, Channel, Rank, 0, CtlGrpPi, GsmMode, &GetSetVal);
          if (Ddr4 || (Lpddr4 && (Rank == 0))) {
            MrcGetSetCcc (MrcData, Controller, Channel, Rank, 0, CkeGrpPi, GsmMode, &GetSetVal);
          }
          if (UpdateHost) {
            IntCmdTiming->CtlPiCode[Rank] = (UINT16) GetSetVal;
            if (Ddr4 || Ddr5 || (Lpddr4 && (Rank == 0))) {
              IntCmdTiming->CkePiCode[Rank] = (UINT16) GetSetVal;
            }
          }
        }
      }
      break;

    default:
      MRC_DEBUG_MSG (Debug, MSG_LEVEL_WARNING, "%sUnknown parameter to shift\n", gWarnString);
      break;
  }
  IoReset (MrcData); // Align CCC signals after PI change. @todo_adl - do we need this for LPDDR5 or not

  return;
}

/**
  Shifts RcvEn, WriteLevel, WriteDQS and WcK timing for all bytes
  Usually used when moving the clocks on a channel

  @param[in,out] MrcData    - Include all MRC global data.
  @param[in]     Controller - Controller to update
  @param[in]     Channel    - Channel to update
  @param[in]     Rank       - Rank to update
  @param[in]     ByteMask   - Bytes to update
  @param[in]     Offset     - value to shift
  @param[in]     UpdateHost - Determines if MrcData structure is updated

  @retval Nothing
**/
void
ShiftDQPIs (
  IN OUT MrcParameters *const MrcData,
  IN     const UINT32         Controller,
  IN     const UINT32         Channel,
  IN     const UINT8          Rank,
  IN     const UINT32         ByteMask,
  IN     const INT16          Offset,
  IN     const UINT8          UpdateHost
  )
{
  MrcOutput             *Outputs;
  MrcIntOutput          *IntOutputs;
  MrcIntControllerOut   *IntControllerOut;
  MrcIntClkAlignedParam *ClkAlignParam;
  MrcInput              *Inputs;
  MRC_FUNCTION          *MrcCall;
  UINT8                 Byte;
  UINT8                 GsmMode;
  INT64                 RecEnDelayValue[MAX_SDRAM_IN_DIMM];
  INT64                 TxDqDelayValue[MAX_SDRAM_IN_DIMM];
  INT64                 TxDqsDelayValue[MAX_SDRAM_IN_DIMM];
  INT64                 WckDelayValue;
  BOOLEAN               WckMoved;
  BOOLEAN               Lpddr5;

  Inputs      = &MrcData->Inputs;
  Outputs     = &MrcData->Outputs;
  MrcCall     = Inputs->Call.Func;
  GsmMode     = ForceWriteCached;
  IntOutputs  = (MrcIntOutput *) (MrcData->IntOutputs.Internal);
  IntControllerOut = &IntOutputs->Controller[Controller];
  MrcCall->MrcSetMem ((UINT8 *) RecEnDelayValue, sizeof (RecEnDelayValue), 0);
  MrcCall->MrcSetMem ((UINT8 *) TxDqDelayValue, sizeof (TxDqDelayValue), 0);
  MrcCall->MrcSetMem ((UINT8 *) TxDqsDelayValue, sizeof (TxDqsDelayValue), 0);
  Lpddr5        = (Outputs->DdrType == MRC_DDR_TYPE_LPDDR5);
  WckDelayValue = 0;
  WckMoved      = FALSE;

  for (Byte = 0; Byte < Outputs->SdramCount; Byte++) {
    if (((1 << Byte) & ByteMask) == 0) {
      continue;
    }
    ClkAlignParam = &IntControllerOut->IntClkAlignedMargins[Channel][Rank][Byte];
    // Initialize local variables which the offset is applied to.
    // If the host structure isn't valid, we start with the register state.
    // Otherwise, we will pull the results from the internal host structure.
    if (!ClkAlignParam->Valid) {
      MrcGetSetStrobe (MrcData, Controller, Channel, Rank, Byte, RecEnDelay, ReadFromCache, &RecEnDelayValue[Byte]);
      MrcGetSetStrobe (MrcData, Controller, Channel, Rank, Byte, TxDqDelay,  ReadFromCache, &TxDqDelayValue[Byte]);

      if (Lpddr5) {
        if (WckMoved == FALSE) {
          MrcGetSetCcc (MrcData, Controller, Channel, MRC_IGNORE_ARG, 0, WckGrpPi, ReadFromCache, &WckDelayValue);
        }
      } else {
        MrcGetSetStrobe (MrcData, Controller, Channel, Rank, Byte, TxDqsDelay, ReadFromCache, &TxDqsDelayValue[Byte]);
      }

      if (!UpdateHost) {
        // If the host structure is not valid and we're not updating the host,
        // we will save the register value to the host structure. Then set the valid bit.
        ClkAlignParam->RcvEn = (UINT16) RecEnDelayValue[Byte];
        ClkAlignParam->TxDq  = (UINT16) TxDqDelayValue[Byte];
        if (Lpddr5) {
          if (WckMoved == FALSE) {
            ClkAlignParam->Wck = (UINT16) WckDelayValue;
          }
        } else {
          ClkAlignParam->TxDqs = (UINT16) TxDqsDelayValue[Byte];
        }
        ClkAlignParam->Valid = TRUE;
      }
    } else {
      RecEnDelayValue[Byte] = ClkAlignParam->RcvEn;
      TxDqDelayValue[Byte] = ClkAlignParam->TxDq;

      if (Lpddr5) {
        if (WckMoved == FALSE) {
          WckDelayValue = ClkAlignParam->Wck;
        }
      } else {
        TxDqsDelayValue[Byte] = ClkAlignParam->TxDqs;
      }
    }

    if (UpdateHost) {
      // If UpdateHost is set, we need to clear the valid bit from the host structure.
      ClkAlignParam->Valid = FALSE;
    }
    if (Lpddr5) {
      WckMoved = TRUE;
    }
  }
  for (Byte = 0; Byte < Outputs->SdramCount; Byte++) {
    if (((1 << Byte) & ByteMask) == 0) {
      continue;
    }
    RecEnDelayValue[Byte] = RecEnDelayValue[Byte] + Offset;
    TxDqDelayValue[Byte]  = TxDqDelayValue[Byte]  + Offset;
    MrcGetSetStrobe (MrcData, Controller, Channel, Rank, Byte, RecEnDelay, GsmMode, &RecEnDelayValue[Byte]);
    MrcGetSetStrobe (MrcData, Controller, Channel, Rank, Byte, TxDqDelay,  GsmMode, &TxDqDelayValue[Byte]);
    if (!Lpddr5) {
      TxDqsDelayValue[Byte] = TxDqsDelayValue[Byte] + Offset;
      MrcGetSetStrobe (MrcData, Controller, Channel, Rank, Byte, TxDqsDelay, GsmMode, &TxDqsDelayValue[Byte]);
    }
  }
  if (Lpddr5) {
    WckDelayValue = WckDelayValue + Offset;
    MrcGetSetCcc (MrcData, Controller, Channel, MRC_IGNORE_ARG, 0, WckGrpPi, GsmMode, &WckDelayValue);
  }

  return;
}

/**
  Retrieve the current memory frequency and clock from the memory controller.

  @param[in]      MrcData      - Include all MRC global data.
  @param[in, out] MemoryClock  - The current memory clock.
  @param[in, out] Ratio        - The current memory ratio setting.
  @param[in, out] RefClk       - The current memory reference clock.

  @retval: The current memory frequency.
**/
MrcFrequency
MrcGetCurrentMemoryFrequency (
  MrcParameters * const   MrcData,
  UINT32 * const          MemoryClock,
  MrcClockRatio * const   Ratio,
  MrcRefClkSelect * const RefClk
  )
{
  const MrcInput                 *Inputs;
  MC_BIOS_REQ_PCU_STRUCT     McBiosReqPcu;
  MC_BIOS_DATA_PCU_STRUCT    McBiosData;

  Inputs  = &MrcData->Inputs;
  McBiosReqPcu.Data = MrcReadCR (MrcData, MC_BIOS_REQ_PCU_REG);
  McBiosData.Data   = MrcReadCR (MrcData, MC_BIOS_DATA_PCU_REG);

  if (McBiosData.Bits.GEAR == 1) {
    McBiosData.Bits.MC_PLL_RATIO *= 2; // In gear2 the actual ratio is twice the ratio in the register
  }
  if (McBiosData.Bits.GEAR == 2) {
    McBiosData.Bits.MC_PLL_RATIO *= 4; // In gear4 the actual ratio is 4x the ratio in the register
  }
  if (MemoryClock != NULL) {
    *MemoryClock = MrcRatioToClock (MrcData, (MrcClockRatio) McBiosData.Bits.MC_PLL_RATIO, McBiosReqPcu.Bits.MC_PLL_REF, Inputs->BClkFrequency);
  }
  if (Ratio != NULL) {
    *Ratio = (MrcClockRatio) McBiosData.Bits.MC_PLL_RATIO;
  }
  if (RefClk != NULL) {
    *RefClk = (MrcRefClkSelect) McBiosReqPcu.Bits.MC_PLL_REF;
  }
  return MrcRatioToFrequency (
          MrcData,
          (MrcClockRatio) McBiosData.Bits.MC_PLL_RATIO,
          McBiosReqPcu.Bits.MC_PLL_REF,
          Inputs->BClkFrequency
          );
}

/**
  Determines if Per-Bit Deskew is needed.  If not, it will disable Per-Bit Deskew.

  @param[in]   MrcData      - The MRC global data.
  @param[in]   MarginResult - Data structure with the latest margin results.
  @param[in]   Param        - Only RdT(1) and WrT(2) are valid.
**/
void
MrcPerBitDeskew (
  IN MrcParameters * const MrcData,
  IN UINT16                MarginResult[MAX_RESULT_TYPE][MAX_RANK_IN_CHANNEL][MAX_CONTROLLER][MAX_CHANNEL][MAX_SDRAM_IN_DIMM][MAX_EDGES],
  IN const UINT8           Param
  )
{
  MrcDebug          *Debug;
  MrcOutput         *Outputs;
  UINT16            MinEyeWidth;
  UINT16            EyeWidth;
  UINT8             ResultType;
  UINT8             Controller;
  UINT8             Channel;
  UINT8             Rank;
  UINT8             Byte;
  BOOLEAN           Update;
  UINT16            PwrLimit;
  UINT16            UPMLimit;
  INT64             GetSetVal;
  GSM_GT            Group[MRC_CHNG_MAR_GRP_NUM];
  UINT8             GrpIdx;

  Outputs    = &MrcData->Outputs;
  Debug      = &Outputs->Debug;

  GetSetVal  = 0;
  Update    = FALSE;
  if ((Param != RdT) && (Param != WrT)) {
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_ERROR, "MrcPerBitDeskew: Incorrect Margin Parameter %d\n", Param);
    return;
  }
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Parameter = %d (%sT)\n", Param, (Param == RdT) ? "Rd" : "Wr");
  ResultType       = GetMarginResultType (Param);

  // To disable deskew set RxDqPerBitDeskew/RxDqPerBitDeskewOffset to 0
  // To disable deskew set TxDqPerBitDeskew/TxDqsPerBitDeskew to 0
  switch (Param) {
  case RdT:
    Group[0] = RxDqsBitDelay;
    Group[1] = RxDqsBitOffset;
    break;

  case WrT:
    Group[0] = TxDqBitDelay;
    Group[1] = TxDqsBitDelay;
    break;
  }

  PwrLimit = MrcGetUpmPwrLimit (MrcData, Param, PowerLimit);
  UPMLimit = MrcGetUpmPwrLimit (MrcData, Param, UpmLimit);
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_ERROR, "MrcPerBitDeskew: PwrLimit %u UPMLimit %u ((PwrLimit + UPMLimit) / 2) %u\n", PwrLimit, UPMLimit, (PwrLimit + UPMLimit) / 2);

  for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
    for (Channel = 0; Channel < Outputs->MaxChannels; Channel++) {
      if (MrcChannelExist (MrcData, 0, Channel)) {
        for (Byte = 0; Byte < Outputs->SdramCount; Byte++) {
          MinEyeWidth = 0xFFFF;
          for (Rank = 0; Rank < MAX_RANK_IN_CHANNEL; Rank++) {
            if (MrcRankExist (MrcData, Controller, Channel, Rank)) {
              EyeWidth = MarginResult[ResultType][Rank][Controller][Channel][Byte][0] + MarginResult[ResultType][Rank][Controller][Channel][Byte][1];
              MinEyeWidth = MIN (MinEyeWidth, EyeWidth);
              MRC_DEBUG_MSG (Debug, MSG_LEVEL_ERROR, "  Mc %u Channel %u Byte %u Rank %u LM %u RM %u Width %u MinWidth %u\n", Controller, Channel, Byte, Rank,
                MarginResult[ResultType][Rank][Controller][Channel][Byte][0], MarginResult[ResultType][Rank][Controller][Channel][Byte][1], EyeWidth, MinEyeWidth);
            } // if Rank Exist
          } // for Rank
          if (MinEyeWidth > ((PwrLimit + UPMLimit) / 2)) {
            Update = TRUE;
            // To disable Deskew set RxDqPerBitDeskew/RxDqPerBitDeskewOffset to 0
            // To disable deskew set TxDqPerBitDeskew/TxDqsPerBitDeskew to 0
            for (GrpIdx = 0; GrpIdx < MRC_CHNG_MAR_GRP_NUM; GrpIdx++) {
              MrcGetSetBit (MrcData, Controller, Channel, MAX_RANK_IN_CHANNEL, Byte, MAX_BITS, Group[GrpIdx], WriteToCache | PrintValue, &GetSetVal);
            }
          } // if MinEyeWidth
        } // for Byte
      } // if Channel Exist
    } // for Channel
  } // for Controller

  if (Update) {
    MrcFlushRegisterCachedData (MrcData);
  }
}

/**
  Enable / Disable CADB and Deselects on MC (on all populated channels)

  @param[in] MrcData  - The MRC global data.
  @param[in] Enable   - TRUE to enable, FALSE to disable.

  @retval Nothing
**/
void
McCadbEnable (
  IN MrcParameters *const  MrcData,
  IN BOOLEAN               Enable
  )
{
  INT64   Control;
  INT64   GetSetEn;
  INT64   GetSetDis;

  Control   = Enable ? 1 : 0;
  GetSetEn  = 1;
  GetSetDis = 0;
  MrcGetSetMcCh (MrcData, MAX_CONTROLLER, MAX_CHANNEL, GsmMccBlockXarb,            WriteNoCache, &GetSetEn);  // Block XARB when changing cadb_enable
  MrcGetSetMcCh (MrcData, MAX_CONTROLLER, MAX_CHANNEL, GsmMccCmdTriStateDisTrain,  WriteNoCache, &Control);
  MrcGetSetMcCh (MrcData, MAX_CONTROLLER, MAX_CHANNEL, GsmMccCadbEnable,           WriteNoCache, &Control);
  MrcGetSetMcCh (MrcData, MAX_CONTROLLER, MAX_CHANNEL, GsmMccDeselectEnable,       WriteNoCache, &Control);
  MrcGetSetMcCh (MrcData, MAX_CONTROLLER, MAX_CHANNEL, GsmMccBlockXarb,            WriteNoCache, &GetSetDis); // Unblock XARB
}

/**
  Run a short CADB sequence on selected channels

  @param[in] MrcData     - The MRC global data.
  @param[in] McChBitMask - Controllers and channels to work on.

  @retval Nothing
**/
void
ShortRunCADB (
  IN MrcParameters *const  MrcData,
  IN UINT8                 McChBitMask
  )
{
  INT64 CmdTriStateDisSave[MAX_CONTROLLER][MAX_CHANNEL];

  MrcCadbIoInit (MrcData, McChBitMask, MRC_ENABLE, CmdTriStateDisSave);
  MrcCustomRunCADB (MrcData, McChBitMask, CadbShortRun, 0);
  MrcCadbStopTest (MrcData, McChBitMask);
  MrcCadbIoInit (MrcData, McChBitMask, MRC_DISABLE, CmdTriStateDisSave);
}

/**
  Perform the DDRIO init required for CADB Enable or Disable.
  Modified DDRIO fields are saved in CmdTriStateDisSave.

  @param[in] MrcData            - The MRC global data.
  @param[in] Enable             - Enable CADB if TRUE, otherwise disable
  @param[in] CmdTriStateDisSave - Buffer used to save PHY settings that are clpbbered during the CADB test.
                                  This buffer is used to restore the PHY settings when Enable == FALSE

  @retval Nothing
**/
void
MrcCadbIoInit (
  IN     MrcParameters *const MrcData,
  IN     UINT8                McChBitMask,
  IN     BOOLEAN              Enable,
  IN OUT INT64                CmdTriStateDisSave[MAX_CONTROLLER][MAX_CHANNEL]
  )
{
  MrcOutput   *Outputs;
  INT64       CmdTriStateDis;
  INT64       IgnoreCkeGetSetVal;
  UINT32      Controller;
  UINT32      Channel;
  UINT8       MaxChannel;
  BOOLEAN     Lpddr;

  Outputs        = &MrcData->Outputs;
  MaxChannel     = Outputs->MaxChannels;
  Lpddr          = Outputs->Lpddr;
  CmdTriStateDis = 1;

  // Enable CADB
  McCadbEnable (MrcData, Enable);

  // Disable command tri-state in order to get CADB traffic
  for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
    for (Channel = 0; Channel < MaxChannel; Channel++) {
      if ((MC_CH_MASK_CHECK (McChBitMask, Controller, Channel, MaxChannel) != 0) && (!IS_MC_SUB_CH (Lpddr, Channel))) {
        if (Enable) {
          IgnoreCkeGetSetVal = 1;
          CmdTriStateDis = 1;
          MrcGetSetMcCh (MrcData, Controller, Channel, GsmMccCmdTriStateDis, ReadFromCache, &CmdTriStateDisSave[Controller][Channel]);
        } else {
          IgnoreCkeGetSetVal = 0;
          CmdTriStateDis = CmdTriStateDisSave[Controller][Channel];
        }
        MrcGetSetMcCh (MrcData, Controller, Channel, GsmMccCmdTriStateDis, WriteCached, &CmdTriStateDis);
        MrcGetSetMcCh (MrcData, Controller, Channel, GsmMccIgnoreCke, WriteNoCache, &IgnoreCkeGetSetVal);
      }
    }
  }
}

/**
  Run a custom CADB sequence on selected channels

  @param[in] MrcData            - The MRC global data.
  @param[in] McChBitMask        - Controllers and channels to work on.
  @param[in] Type               - CADB test type
  @param[in] Length             - CADB test length in CADB clocks

  @retval Nothing
**/
void
MrcCustomRunCADB (
  IN  MrcParameters *const  MrcData,
  IN  UINT8                 McChBitMask,
  IN  MrcRunCadbType        Type,
  IN  UINT16                Length       OPTIONAL
  )
{
  const MRC_FUNCTION  *MrcCall;
  UINT32      TestDone;
  UINT64      Timeout;
  MC0_CH0_CR_CADB_CFG_STRUCT        CadbConfig;
  MC0_CH0_CR_CADB_CTL_STRUCT        CadbControl;
  MC0_CH0_CR_CADB_AO_MRSCFG_STRUCT  MrsConfig;

  MrcCall        = MrcData->Inputs.Call.Func;

  // Enable CADB Always On mode
  CadbConfig.Data = 0;
  CadbConfig.Bits.CADB_MODE = Cadb20ModeAlwaysOn;
  CadbConfig.Bits.LANE_DESELECT_EN      = 5;        // Drive deselects on CS and CA pins; @todo - not sure this is needed in AlwaysOn mode ?
  CadbConfig.Bits.INITIAL_DSEL_EN       = 1;
  CadbConfig.Bits.INITIAL_DSEL_SSEQ_EN  = 1;
  if (Type != CadbEndlessTest) {
    CadbConfig.Bits.CADB_DSEL_THROTTLE_MODE = MrcGetNMode (MrcData);
    CadbConfig.Bits.CADB_SEL_THROTTLE_MODE  = MrcGetNMode (MrcData);
  }

  Cadb20ConfigRegWrite (MrcData, CadbConfig);

  MrsConfig.Data = 0;
  if (Type == CadbEndlessTest) {
    // Zero means endless test, until STOP_TEST is set.
    MrsConfig.Bits.MRS_AO_REPEATS = 0;
  } else if (Type == CadbCustomLengthRun) {
    // Use the input length
    MrsConfig.Bits.MRS_AO_REPEATS = Length;
  } else {
    // Issue a single loop over pattern buffer (8*4=32).
    MrsConfig.Bits.MRS_AO_REPEATS = 8;
  }
  // Keep MRS_GAP = 0 - No gap
  // Keep MRS_GAP_SCALE = 0 - Linear
  Cadb20MrsConfigRegWrite (MrcData, MrsConfig);

  // Start CADB
  CadbControl.Data = 0;
  CadbControl.Bits.START_TEST = 1;
  Cadb20ControlRegWrite (MrcData, McChBitMask, CadbControl);

  if (Type == CadbEndlessTest) {
    // If endless test is selected, exit without polling for test completion
    return;
  }

  // Poll till test done on all participating channels / subch
  Timeout = MrcCall->MrcGetCpuTime () + MRC_WAIT_TIMEOUT;   // 10 seconds timeout
  do {
    TestDone = Cadb20TestDoneStatus (MrcData, McChBitMask);
  } while ((TestDone == 0) && (MrcCall->MrcGetCpuTime () < Timeout));


}

/**
  Stop the CADB test that is currently executing.

  @param[in] MrcData     - The MRC global data.
  @param[in] McChBitMask - Controllers and channels to work on.

  @retval none
**/
void
MrcCadbStopTest (
  IN MrcParameters *const MrcData,
  IN  UINT8               McChBitMask
  )
{
  MrcOutput                   *Outputs;
  MC0_CH0_CR_CADB_CFG_STRUCT  CadbConfig;
  MC0_CH0_CR_CADB_CTL_STRUCT  CadbControl;

  Outputs        = &MrcData->Outputs;

  //Stop CADB Test
  CadbControl.Data = 0;
  CadbControl.Bits.STOP_TEST = 1;
  Cadb20ControlRegWrite (MrcData, Outputs->McChBitMask, CadbControl);

  // Disable CADB Always On mode
  CadbConfig.Data = 0;
  CadbConfig.Bits.CADB_MODE = Cadb20ModeOff;
  CadbConfig.Bits.LANE_DESELECT_EN      = 5;
  CadbConfig.Bits.INITIAL_DSEL_EN       = 1;
  CadbConfig.Bits.INITIAL_DSEL_SSEQ_EN  = 1;
  Cadb20ConfigRegWrite (MrcData, CadbConfig);
}

/**
  Get the Rx Bias values

  @param[in, out] MrcData    - Include all MRC global data.
  @param[in, out] RxFselect  - Location to save RxFselect.

  @retval Nothing
**/
void
GetRxFselect (
  IN MrcParameters *const MrcData,
  IN OUT INT8             *RxFselect
  )
{
  MrcOutput                    *Outputs;

  Outputs          = &MrcData->Outputs;
  *RxFselect       = (Outputs->Ratio - ((Outputs->RefClk == MRC_REF_CLOCK_133) ? RXF_SELECT_RC_133 : RXF_SELECT_RC_100));

  // Limit ratios for 1067, 1333, 1600, 1867 & 2133 MHz
  *RxFselect = MIN (*RxFselect, RXF_SELECT_MAX);  // Maximum 2133 MHz
  *RxFselect = MAX (*RxFselect, RXF_SELECT_MIN);  // Minimum 1067 MHz
  return;
}

/**
  Read 32-bit value from the specified bus/device/function/offset.

  @param[in] MrcData  - Include all MRC global data.
  @param[in] Bus      - PCI bus number.
  @param[in] Device   - PCI device number.
  @param[in] Function - PCI function number.
  @param[in] Offset   - PCI address offset.

  @retval 32-bit PCI value.
**/
UINT32
MrcPciRead32 (
  IN const MrcParameters * const MrcData,
  IN const UINT8 Bus,
  IN const UINT8 Device,
  IN const UINT8 Function,
  IN const UINT8 Offset
  )
{
  const MrcInput     *Inputs;
  const MRC_FUNCTION *MrcCall;

  Inputs  = &MrcData->Inputs;
  MrcCall = Inputs->Call.Func;
  MrcCall->MrcIoWrite32 (Inputs->PciIndex, MrcCall->MrcGetPciDeviceAddress (Bus, Device, Function, Offset));
  return (MrcCall->MrcIoRead32 (Inputs->PciData));
}

/**
  This function changes the DIMM Voltage to the closest desired voltage without
  going higher. Default wait time is the minimum value of 200us, if more time
  is needed before deassertion of DIMM Reset#, then change the parameter.

  @param[in, out] MrcData            - The MRC "global data" area.
  @param[in]      VddVoltage         - Selects the DDR voltage to use, in mV.
  @param[in, out] VddSettleWaitTime  - Time needed for Vdd to settle after the update

  @retval TRUE if a voltage change occurred, otherwise FALSE.
**/
BOOLEAN
MrcVDDVoltageCheckAndSwitch (
  IN OUT MrcParameters      *MrcData,
  IN     const MrcVddSelect VddVoltage,
  IN OUT UINT32 * const     VddSettleWaitTime
  )
{
  MRC_FUNCTION *MrcCall;
  MrcInput     *Inputs;
  MrcOutput    *Outputs;
  MrcDebug     *Debug;
  MrcVddSelect Current;
  MrcVddSelect DefaultVdd;
  MrcVddSelect NewVdd;
  BOOLEAN      Status;

  Inputs  = &MrcData->Inputs;
  MrcCall = Inputs->Call.Func;
  Outputs = &MrcData->Outputs;
  Debug   = &Outputs->Debug;
  Status  = FALSE;

  // DDR4 use 1.2v by default; LPDDR4 is 1.1v; LPDDR4x is 0.6v
  if (Outputs->DdrType == MRC_DDR_TYPE_LPDDR4) {
    DefaultVdd = (Outputs->Lp4x) ? VDD_0_60 : VDD_1_10;
  } else {
    DefaultVdd = VDD_1_20;
  }

  Current = (MrcVddSelect) MrcCall->MrcGetMemoryVdd (MrcData, DefaultVdd);
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Current VddVoltage is %u mV\n", Current);
  if (VddVoltage != Current) {
    NewVdd = MrcCall->MrcSetMemoryVdd (MrcData, DefaultVdd, VddVoltage);
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "*** VddVoltage updated to %u mV\n", NewVdd);
    Status = TRUE;
  }

  // Increase the VddSettleWaitTime by the amount requested in the Input structure
  *VddSettleWaitTime += Inputs->VddSettleWaitTime;

  // Either update was already done or change is not necessary every time this is called
  Outputs->VoltageDone = TRUE;

  return Status;
}
#ifdef MRC_DEBUG_PRINT
/**
  Debug output of MarginResults for specific ResultType

  @param[in] MrcData - Pointer to MRC global data.
  @param[in] ResultType - Margin Result Type.  MAX_RESULT_TYPE prints all parameters.

  @retval Nothing
**/
void
MrcDisplayMarginResultArray (
  IN MrcParameters *MrcData,
  IN UINT8         ResultType
  )
{
  MrcDebug    *Debug;
  MrcOutput   *Outputs;
  UINT32      ResTypeStart;
  UINT32      ResTypeEnd;
  UINT32      ResTypeIdx;
  UINT32      Controller;
  UINT32      Channel;
  UINT32      Rank;
  UINT32      Byte;
  UINT32      Edge;

  Outputs = &MrcData->Outputs;
  Debug   = &Outputs->Debug;

  if (ResultType >= MAX_RESULT_TYPE) {
    ResTypeStart = 0;
    ResTypeEnd = MAX_RESULT_TYPE;
  } else {
    ResTypeStart = ResultType;
    ResTypeEnd = ResultType + 1;
  }

  MRC_DEBUG_MSG (
    Debug,
    MSG_LEVEL_NOTE,
    "***********************Margin Results**********************\n"
    );
  for (ResTypeIdx = ResTypeStart; ResTypeIdx < ResTypeEnd; ResTypeIdx++) {
    MRC_DEBUG_MSG (
      Debug,
      MSG_LEVEL_NOTE,
      "%s\tB0E0\tB0E1\tB1E0\tB1E1\tB2E0\tB2E1\tB3E0\tB3E1\tB4E0\tB4E1\tB5E0\tB5E1\tB6E0\tB6E1\tB7E0\tB7E1\tB8E0\tB8E1\n",
      gResTypeStr[ResTypeIdx]
      );
    for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
      for (Channel = 0; Channel < Outputs->MaxChannels; Channel++) {
        for (Rank = 0; Rank < MAX_RANK_IN_CHANNEL; Rank++) {
          if (MrcRankExist (MrcData, Controller, Channel, Rank)) {
            MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Mc%u.C%u.R%u\t", Controller, Channel, Rank);
            for (Byte = 0; Byte < Outputs->SdramCount; Byte++) {
              for (Edge = 0; Edge < MAX_EDGES; Edge++) {
                MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "%u\t", (UINT16) Outputs->MarginResult[ResTypeIdx][Rank][Controller][Channel][Byte][Edge]);
              } //Edge
            } //Byte
            MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\n");
          }//RankExist
        } //Rank
      } //Channel
    } // Controller
  } // ResTypeIdx

  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "***********************************************************\n");
}
#endif //MRC_DEBUG_PRINT

/**
  This function sets up the REUT registers for Alias Check and DDR4 PDA mapping

  Setup for Memory Check like Alias Checking and DDR4 PDA mapping

  @param[in, out] MrcData           - Pointer to MRC global data.
  @param[in, out] ReutAddress       - Pointer to ReutAddress that will be programmed to REUT engine
  @param[in] Rank                   - Rank to setup
  @param[in] NumOfCachelines        - # of Cachelines to write and read to.  0 is a special value that will write the number
                                      of Cachelines that is required to determine Row/Column Aliasing.
  @param[in, out] ReutUninitialized - Pointer of whether ReutAddress Pointer needs initializing or not.

  @retval Nothing
**/
void
MrcMemoryCheckSetup (
  IN OUT MrcParameters *const MrcData,
  IN OUT MRC_REUTAddress      *ReutAddress,
  IN UINT8                    Rank,
  IN UINT8                    NumOfCachelines,
  IN OUT BOOLEAN              *ReutUninitialized
  )
{

//  static const UINT8 WrapCarryEn[MrcReutFieldMax]   = {0, 0, 0, 0};
//  static const UINT8 WrapTriggerEn[MrcReutFieldMax] = {0, 0, 0, 0};  // Trigger Stop on Bank Wrap
//  static const UINT8 AddrInvertEn[MrcReutFieldMax]  = {0, 0, 0, 0};
//  MrcDimmOut         *DimmOut;
//  UINT32             CrOffset;
//  UINT32             WritesPerPage;
//  UINT16             ColumnIncValUnaligned;
//  UINT8              RankToDimm;
//  UINT8              Channel;
//  UINT8              GlobalControl;
// @ todo <ICL> Update with CPGC 2.0 implementation
//  REUT_CH_SEQ_CFG_0_STRUCT              ReutChSeqCfg;
//  MCHBAR_CH0_CR_REUT_CH_PAT_WDB_CL_CTRL_STRUCT    ReutChPatWdbCl;
//  REUT_PG_PAT_CL_MUX_CFG_STRUCT ReutChPatWdbClMuxCfg;
//  REUT_CH0_SUBSEQ_CTL_0_STRUCT          ReutSubSeqCtl;

  //@todo: Use host burst length
//  BurstLength = (Outputs->DdrType == MRC_DDR_TYPE_LPDDR4) ?  16 : 8;

  if (*ReutUninitialized == TRUE) {
/**
    // Test Initialization
    // Start with IncRate = 3 so we have 4 column writes per page.  This will change with Column Size.
    // Must have 4 (reg + 1) writes to move to the next LFSR code for unique values.
    ReutAddress->IncRate[MrcReutFieldRow]  = 3;
    // IncVal[Col] is chosen to be 1/4 of the minimum column supported to get 4 writes per page.
    // Each write is 1 cache line (8 or 16 column addresses worth of data).
    // IncVal is on a cache line basis when programmed.  Account for this here by dividing by the burst length.
    ColumnIncValUnaligned                 = MRC_BIT10 / 4;
    ReutAddress->IncVal[MrcReutFieldCol]  = ColumnIncValUnaligned / BurstLength; // cache line shift

    // Smallest Row address size is 2^12, but Row_Base_Address_Increment is a 12-bit signed field [0-11].
    // Thus we have to increment by 2^10.
    ReutAddress->IncVal[MrcReutFieldRow]  = MRC_BIT10;
    ReutAddress->Stop[MrcReutFieldCol]    = 1024; // 4 ([0-3] << 3) column writes before wrapping
    ReutAddress->Start[MrcReutFieldBank]  = 1;
    ReutAddress->Stop[MrcReutFieldBank]   = 1;

    // Setup Reut all present channels.
    for (Channel = 0; Channel < MAX_CHANNEL; Channel++) {
      if (!MrcChannelExist (MrcData, 0, Channel)) {
        continue;
      }
// @ todo <ICL> Update with CPGC 2.0 implementation; MrcProgramSequenceAddress deprecated
      // Write initial Reut Address Values.
      MrcProgramSequenceAddress (
        MrcData,
        Channel,
        ReutAddress->Start,
        NULL,              // Stop
        ReutAddress->Order,
        ReutAddress->IncRate,
        ReutAddress->IncVal,
        WrapTriggerEn,
        WrapCarryEn,
        AddrInvertEn,
        0,
        FALSE
        );

      // Set Reut to Write
      CrOffset = OFFSET_CALC_CH (REUT_CH_SEQ_CFG_0_REG, REUT_CH_SEQ_CFG_1_REG, Channel);
      ReutChSeqCfg.Data                         = MrcReadCR (MrcData, CrOffset);
      GlobalControl                             = (UINT8) ReutChSeqCfg.Bits.Global_Control;
      ReutChSeqCfg.Data                         = 0;
      ReutChSeqCfg.Bits.Initialization_Mode     = REUT_Testing_Mode;
      ReutChSeqCfg.Bits.Start_Test_Delay        = 2;
      ReutChSeqCfg.Bits.Subsequence_End_Pointer = 1;
      ReutChSeqCfg.Bits.Global_Control          = GlobalControl;

      MrcWriteCR64 (
        MrcData,
        CrOffset,
        ReutChSeqCfg.Data
        );

      MrcSetLoopcount (MrcData, 1);

      // Program Write Data Buffer Control.
      // @todo: Update for new ICL design.
      MrcSelectPatternGen (MrcData, 1 << Channel, 0, 0);
      // Setup CADB in terms of LFSR selects, LFSR seeds, LMN constants and overall control
      CrOffset = REUT_PG_PAT_CL_MUX_CFG_REG;
      ReutPgPatMuxCfg.Data               = 0;
      ReutPgPatMuxCfg.Bits.Mux0_Control  = MrcPgMuxLfsr;
      ReutPgPatMuxCfg.Bits.Mux1_Control  = MrcPgMuxLfsr;
      ReutPgPatMuxCfg.Bits.Mux2_Control  = MrcPgMuxLfsr;
      ReutPgPatMuxCfg.Bits.LFSR_Type_0   = MrcLfsr16;
      ReutPgPatMuxCfg.Bits.LFSR_Type_1   = MrcLfsr16;
      ReutPgPatMuxCfg.Bits.LFSR_Type_2   = MrcLfsr16;
      MrcWriteCR64 (MrcData, CrOffset, ReutPgPatMuxCfg.Data);
      //@todo: Enable ECC to be Checked.
    }// Channel
    *ReutUninitialized = FALSE;
  }// ReutUninitialized

  if ((MRC_BIT0 << Rank) & Outputs->ValidRankMask) {
    for (Channel = 0; Channel < MAX_CHANNEL; Channel++) {
      if (!MrcChannelExist (MrcData, 0, Channel)) {
        continue;
      }
      // Update Rank stop address based on DIMM SPD if Active.
      RankToDimm  = RANK_TO_DIMM_NUMBER (Rank);
      DimmOut     = &ControllerOut->Channel[Channel].Dimm[RankToDimm];
      // Since we're counting cache lines and won't wrap the row address,
      // program Row Stop to RowSize - 1 to avoid resetting the current address.
      // Column must wrap.  The wrap occurs on the increment which is after writing,
      // to that address.  Thus, we set wrap to be the last accessed column.
      ColumnIncValUnaligned                 = ReutAddress->IncVal[MrcReutFieldCol] * BurstLength; // cache line shift
      WritesPerPage                         = DimmOut->ColumnSize / ColumnIncValUnaligned;
      ReutAddress->Stop[MrcReutFieldRow]    = (UINT16) DimmOut->RowSize - 1;
      ReutAddress->Stop[MrcReutFieldCol]    = DimmOut->ColumnSize - ColumnIncValUnaligned;
      ReutAddress->IncRate[MrcReutFieldRow] = WritesPerPage - 1; // IncRate is +1 the programmed value
// @todo <ICL> Update with CPGC 2.0 implementation; MrcProgramSequenceAddress deprecated
      MrcProgramSequenceAddress (
        MrcData,
        Channel,
        NULL,
        ReutAddress->Stop,
        NULL,
        ReutAddress->IncRate,
        NULL,
        NULL,
        NULL,
        NULL,
        0,
        FALSE
        );
      // Determine if we are doing an alias check, or a specific number of cachelines.
      WritesPerPage = (NumOfCachelines > 0) ? NumOfCachelines : ((DimmOut->RowSize / MRC_BIT10) * WritesPerPage);

      // Set up the Subsequence control.
      CrOffset = OFFSET_CALC_CH (REUT_CH0_SUBSEQ_CTL_0_REG, REUT_CH1_SUBSEQ_CTL_0_REG, Channel);
      ReutSubSeqCtl.Data = 0;
      ReutSubSeqCtl.Bits.Subsequence_Type     = BWr;
      ReutSubSeqCtl.Bits.Number_of_Cachelines = MrcLog2 (WritesPerPage - 1);
      MrcWriteCR (MrcData, CrOffset, ReutSubSeqCtl.Data);

      CrOffset += REUT_CH0_SUBSEQ_CTL_1_REG - REUT_CH0_SUBSEQ_CTL_0_REG;
      ReutSubSeqCtl.Bits.Reset_Current_Base_Address_To_Start = 1;
      ReutSubSeqCtl.Bits.Subsequence_Type = BRd;
      MrcWriteCR (MrcData, CrOffset, ReutSubSeqCtl.Data);
    }
    **/
  }
}


/**
  This function converts from the MR address to the index needed to access this MR
  inside the storage array in MrcData.

  @param[in]      MrcData   - Pointer to global MRC Data.
  @param[in, out] MrAddress - Pointer to MR address to translate.

  @retval UINT8 - The index to use in the MR array in MrcData or 0xFF if unsupported.
**/
UINT8
MrcMrAddrToIndex (
  IN      MrcParameters   *MrcData,
  IN OUT  MrcModeRegister *MrAddress
  )
{
  UINT8 MrIndex = mrIndexMax;

  switch (*MrAddress) {
    case mrMR0:
    case mrMR1:
    case mrMR2:
    case mrMR3:
    case mrMR4:
    case mrMR5:
    case mrMR6:
      MrIndex = *MrAddress;
      break;

    case mrMR8:
      MrIndex = mrIndexMR8;
      break;

    // MR10-18 start at array index 8 (Right after MR8)
    case mrMR10:
    case mrMR11:
    case mrMR12:
      MrIndex = (UINT8)*MrAddress - (mrMR10 - mrIndexMR10);
      break;

    case mrMR13:
    case mrMR14:
    case mrMR15:
    case mrMR16:
    case mrMR17:
    case mrMR18:
      MrIndex = (UINT8)*MrAddress - (mrMR13 - mrIndexMR13);
      break;

    case mrMR20:
    case mrMR21:
    case mrMR22:
    case mrMR23:
    case mrMR24:
    case mrMR25:
    case mrMR26:
    case mrMR27:
    case mrMR28:
    case mrMR29:
    case mrMR30:
      MrIndex = (UINT8)*MrAddress - (mrMR20 - mrIndexMR20);
      break;

    case mrMR34:
    case mrMR35:
    case mrMR36:
    case mrMR37:
    case mrMR38:
    case mrMR39:
    case mrMR40:
    case mrMR41:
    case mrMR42:
      MrIndex = (UINT8)*MrAddress - (mrMR34 - mrIndexMR34);
      break;
#if 0
    case mrMR43:
      MrIndex = mrIndexMR43;
      break;
      case mrMR44:
      MrIndex = mrIndexMR44;
      break;
#endif //if 0
    case mrMR45:
      MrIndex = mrIndexMR45;
      break;

    case mrMR48:
      MrIndex = mrIndexMR48;
      break;

    case mrMR54:
      MrIndex = mrIndexMR54;
      break;

    case mrMR55:
      MrIndex = mrIndexMR55;
      break;

    case mrMR56:
      MrIndex = mrIndexMR56;
      break;

    case mrMR57:
      MrIndex = mrIndexMR57;
      break;

    case mrMR58:
      MrIndex = mrIndexMR58;
      break;

    case mrMR59:
      MrIndex = mrIndexMR59;
      break;

    // Becase we only train DIMM DFE per rank, we don't need to store per-bit DFE values in the host struct, they are all the same
    // Use DQL bit [0] Tap1..4 values for all bits: MR129..132
    case mrMR129:
    case mrMR137:
    case mrMR145:
    case mrMR153:
    case mrMR161:
    case mrMR169:
    case mrMR177:
    case mrMR185:
    case mrMR193:
    case mrMR201:
    case mrMR209:
    case mrMR217:
    case mrMR225:
    case mrMR233:
    case mrMR241:
    case mrMR249:
      MrIndex = mrIndexMR129; // DFE Tap1 DQL bit [0]
      break;
    case mrMR130:
    case mrMR138:
    case mrMR146:
    case mrMR154:
    case mrMR162:
    case mrMR170:
    case mrMR178:
    case mrMR186:
    case mrMR194:
    case mrMR202:
    case mrMR210:
    case mrMR218:
    case mrMR226:
    case mrMR234:
    case mrMR242:
    case mrMR250:
      MrIndex = mrIndexMR130; // DFE Tap2 DQL bit [0]
      break;
    case mrMR131:
    case mrMR139:
    case mrMR147:
    case mrMR155:
    case mrMR163:
    case mrMR171:
    case mrMR179:
    case mrMR187:
    case mrMR195:
    case mrMR203:
    case mrMR211:
    case mrMR219:
    case mrMR227:
    case mrMR235:
    case mrMR243:
    case mrMR251:
      MrIndex = mrIndexMR131; // DFE Tap3 DQL bit [0]
      break;
    case mrMR132:
    case mrMR140:
    case mrMR148:
    case mrMR156:
    case mrMR164:
    case mrMR172:
    case mrMR180:
    case mrMR188:
    case mrMR196:
    case mrMR204:
    case mrMR212:
    case mrMR220:
    case mrMR228:
    case mrMR236:
    case mrMR244:
    case mrMR252:
      MrIndex = mrIndexMR132; // DFE Tap4 DQL bit [0]
      break;

    // MPC command indexes
    case mpcMR13:
    case mpcMR32a0:
    case mpcMR32a1:
    case mpcMR32b0:
    case mpcMR32b1:
    case mpcMR33a0:
    case mpcMR33:
    case mpcMR33b0:
    case mpcMR34:
    case mpcApplyVrefCa:
    case mpcDllReset:
    case mpcZqCal:
    case mpcZqLat:
    case mpcEnterCaTrainMode:
    case mpcSetCmdTiming:
    case mpcSelectAllPDA:
      MrIndex = (UINT8) *MrAddress - (256 - mrIndexMpcMr13);
      break;

    // Handle ByteMode for LP5 CA Vref.  Return actual MR number
    case mrMR12b:
      MrIndex = mrIndexMR12Upper;
      *MrAddress = mrMR12;
      break;

    case mrMR16FspOp:
      MrIndex = mrIndexMR16;
      *MrAddress = mrMR16;
      break;

    default:
      MrIndex = 0xFF;
      MRC_DEBUG_MSG (&MrcData->Outputs.Debug, MSG_LEVEL_ERROR, "%s Untracked MR %d", gErrString, *MrAddress);
      break;
  }

  return MrIndex;
}

/**
  Hook before normal mode is enabled.

  @param[in, out] MrcData  - The MRC "global data" area.

  @retval Nothing.
**/
void
MrcBeforeNormalModeTestMenu (
  IN OUT MrcParameters *const MrcData
  )
{
  return;
}

/**
  This function is used to calculate Vref or VSwing of a generic voltage divider.
  ReceiverOdt must be non-Zero, or exceptions occurs.
  VSS, the voltage level the PullDown is attached to, is assumed to be 0.

  @param[in]  MrcData         - Pointer to global data structure.
  @param[in]  DriverPullUp    - Used to calculate Vhigh.  Value in Ohms.
  @param[in]  DriverPullDown  - Used to calculate Vlow.  Value in Ohms.
  @param[in]  ReceiverOdt     - Value in Ohms.
  @param[in]  Vdd             - Voltage level PullUp is tied to.  Must be the same unit size as Vtermination.
  @param[in]  Vtermination    - Voltage level ReceiverOdt is tied to.  Must be the same unit size as Vdd.
  @param[in]  IsVref          - Boolean to select Vref or Vswing calculation.

  @retval - Vref in units of Vdd/Vterm
**/
UINT32
MrcCalcGenericVrefOrSwing (
  IN  MrcParameters *const  MrcData,
  IN  UINT32                DriverPullUp,
  IN  UINT32                DriverPullDown,
  IN  UINT32                ReceiverOdt,
  IN  UINT32                Vdd,
  IN  UINT32                Vtermination,
  IN  BOOLEAN               IsVref
  )
{
  MrcDebug  *Debug;
  UINT32  Vhigh;
  UINT32  Vlow;
  UINT32  Result;
  UINT32  Numerator;
  UINT32  Denominator;

  Debug = &MrcData->Outputs.Debug;

  Numerator   = ReceiverOdt;
  Denominator = DriverPullUp + ReceiverOdt;
  MRC_DEBUG_ASSERT (Denominator != 0, Debug, "Divide by 0!\n");
  //Vhigh = Vtermination + ((Vdd - Vtermination) * (ReceiverOdt / (DriverPullUp + ReceiverOdt)));
  Vhigh = (Vdd - Vtermination) * Numerator;
  Vhigh = UDIVIDEROUND (Vhigh, Denominator) + Vtermination;
  Numerator   = DriverPullDown;
  Denominator = ReceiverOdt + DriverPullDown;
  MRC_DEBUG_ASSERT (Denominator != 0, Debug, "Divide by 0!\n");
  //Vlow  = Vtermination * (DriverPullDown / (DriverPullDown + ReceoverOdt))
  Vlow = Vtermination * Numerator;
  Vlow = UDIVIDEROUND (Vlow, Denominator);

  if (IsVref) {
    Result = Vhigh + Vlow;
    Result = UDIVIDEROUND (Result, 2);
  } else {
    Result = Vhigh - Vlow;
  }

  return Result;
}

/**
  This function is used to gather current system parameters to pass to the function,
  which calculates the ideal Read/Write/Command Vref.  It will return the result to the caller.

  @param[in]  MrcData         - Pointer to global data structure.
  @param[in]  DriverPullUp    - Used to calculate Vhigh.  Value in Ohms.
  @param[in]  DriverPullDown  - Used to calculate Vlow.  Value in Ohms.
  @param[in]  ReceiverOdt     - Value in Ohms.
  @param[in]  VrefType        - MRC_MarginTypes: WrV, CmdV, RdV.
  @param[in]  Print           - Boolean switch to print the results and parameters.

  @retval - Vref in units of mV
**/
UINT32
MrcCalcIdealVref (
  IN  MrcParameters *const  MrcData,
  IN  UINT32                DriverPullUp,
  IN  UINT32                DriverPullDown,
  IN  UINT32                ReceiverOdt,
  IN  MRC_MarginTypes       VrefType,
  IN  BOOLEAN               Print
  )
{
  MrcInput    *Inputs;
  MrcOutput   *Outputs;
  MrcDebug    *Debug;
  MrcDdrType  DdrType;
  MRC_ODT_MODE_TYPE OdtMode;
  UINT32      Vref;
  UINT32      Vdd;
  UINT32      Vss;
  UINT32      Vterm;
  BOOLEAN     Lpddr;
  BOOLEAN     VddDiv2;

  Inputs  = &MrcData->Inputs;
  Outputs = &MrcData->Outputs;
  Debug   = &Outputs->Debug;
  DdrType = Outputs->DdrType;
  OdtMode = Outputs->OdtMode;
  Lpddr   = Outputs->Lpddr;
  VddDiv2 = ((VrefType == RdV) && (OdtMode == MrcOdtModeVtt));

  if (VrefType != WrV && VrefType != CmdV && VrefType != RdV) {
    MRC_DEBUG_ASSERT (1==0, Debug, "%s MrcCalcIdealVref - Invalid VrefType: %d\n", gErrString, VrefType);
    return 0;
  }

  Vdd   = Outputs->VddVoltage[Inputs->MemoryProfile];
  Vss   = 0;
  Vterm = MRC_UINT32_MAX;
  Vref  = MRC_UINT32_MAX;

  if (VddDiv2) {
    Vref = Vdd / 2;
  } else {
    if (VrefType == WrV) {
      if (Lpddr) {
        Vdd = Outputs->VccddqVoltage;
        if (Inputs->LowSupplyEnData) {
          Vdd = Inputs->VccIomV;
        }
      }
    } else if (VrefType == CmdV) {
      // Command Vref
      if (Lpddr) {
        Vdd = Outputs->VccddqVoltage;
      }
    }

    // Setup PullUp, PullDown, Vterm,
    switch (DdrType) {
      case MRC_DDR_TYPE_DDR4:
      case MRC_DDR_TYPE_DDR5:
        Vterm = Vdd;
        break;

      case MRC_DDR_TYPE_LPDDR4:
      case MRC_DDR_TYPE_LPDDR5:
        Vterm = Vss;
        break;

      default:
        MRC_DEBUG_ASSERT (FALSE, Debug, "Unsupported DRAM Type and Odt Mode: %d, %s\n", DdrType, gIoOdtModeStr[OdtMode]);
        return Vref;
    }

    Vref = MrcCalcGenericVrefOrSwing (MrcData, DriverPullUp, DriverPullDown, ReceiverOdt, Vdd, Vterm, TRUE);
  }

  if (Print) {
    MRC_DEBUG_MSG (
      Debug,
      MSG_LEVEL_NOTE,
      "DriverPullUp = %u, DriverPullDown = %u, ReceiverOdt = %u, Vdd = %u, Vterm = %u\nVrefType = %s, Vref = %u\n",
      DriverPullUp,
      DriverPullDown,
      ReceiverOdt,
      Vdd,
      Vterm,
      gMarginTypesStr[VrefType],
      Vref
      );
  }

  return Vref;
}

/**
  This function will program the ideal Rx Vref to all the bytes in the Channel.
  Channel here denotes a 64-bit bus.  It will not program Bytes that aren't present,
  in the case of Sub-Channels.

  It has two phases:
    Calculate Vref based on parameters passed to the function.
    Encode the Vref calculated into the CPU register format and program the register.

  Two parameters are determined inside the function stack:
    Vdd,
    Vtermination

  @param[in]  MrcData         - Pointer to MRC global data.
  @param[in]  Controller      - Memory Controller Number within the processor (0-based).
  @param[in]  Channel         - Channel to program.
  @param[in]  DriverPullUp    - Used to calculate Vhigh.  Value in Ohms.
  @param[in]  DriverPullDown  - Used to calculate Vlow.  Value in Ohms.
  @param[in]  ReceiverOdt     - Value in Ohms.
  @param[in]  PrintMsg        - Boolean switch to enable debug printing.

  @retval none
**/
VOID
MrcSetIdealRxVref (
  IN  MrcParameters *const  MrcData,
  IN  UINT32                Controller,
  IN  UINT32                Channel,
  IN  UINT32                DriverPullUp,
  IN  UINT32                DriverPullDown,
  IN  UINT32                ReceiverOdt,
  IN BOOLEAN                PrintMsg
  )
{
  INT64   GetSetVref;
  UINT32  Vref;
  UINT32  Mode;

  Mode = WriteCached;
#ifdef MRC_DEBUG_PRINT
  if (PrintMsg) {
    Mode |= PrintValue;
  }
#endif // MRC_DEBUG_PRINT

  Vref = MrcCalcIdealVref (MrcData, DriverPullUp, DriverPullDown, ReceiverOdt, RdV, PrintMsg);
  if (MrcData->Outputs.Lp4x) {
    Vref += 29;
  }

  MrcEncodeRxVref (MrcData, Vref, &GetSetVref);

  MrcGetSetChStrb (MrcData, Controller, Channel, MAX_SDRAM_IN_DIMM, RxVref, Mode, &GetSetVref);
}

/**
  Set ideal CPU Read Vref for DDR4.

  @param[in, out] MrcData     - Include all MRC global data.
  @param[in]      Controller  - Memory Controller Number within the processor (0-based).
  @param[in]      DimmRon     - DIMM Ron value in Ohm
  @param[in]      CpuOdt      - CPU Read ODT value in Ohm
  @param[in]      PrintMsg    - enable debug prints

  @retval none
**/
void
MrcSetIdealRxVrefDdr4 (
  IN OUT MrcParameters *const MrcData,
  IN UINT32                   Controller,
  IN UINT8                    Channel,
  IN UINT32                   DimmRon,
  IN UINT32                   CpuOdt,
  IN BOOLEAN                  PrintMsg
  )
{
  const MrcInput    *Inputs;
  MrcDebug          *Debug;
  MrcOutput         *Outputs;
  MrcVddSelect      Vdd;
  INT64             GetSetVal;
  INT64             RxVrefMinValue;
  INT64             RxVrefMaxValue;
  INT32             RxVrefVal;
  INT64             RxVrefCode;
  UINT32            GsMode;
  MRC_ODT_MODE_TYPE OdtMode;

  Inputs            = &MrcData->Inputs;
  Outputs           = &MrcData->Outputs;
  Debug             = &Outputs->Debug;

  OdtMode = Outputs->OdtMode;
  Vdd     = Outputs->VddVoltage[Inputs->MemoryProfile];
  GsMode  = WriteCached;
  if (PrintMsg) {
    GsMode |= PrintValue;
  }

  if (OdtMode == MrcOdtModeVtt) {
    // in Vtt Odt mode the ideal Vref is Vdd/2.
    RxVrefVal = Vdd / 2;
    MrcGetSetLimits (MrcData, RxVref, &RxVrefMinValue, &RxVrefMaxValue, NULL);
    RxVrefCode = ((INT32) RxVrefMinValue + (INT32) RxVrefMaxValue) / 2;
  } else if (OdtMode == MrcOdtModeVddq) {
    // (Vdd * (2 * DimmRon + DqOdt) / (DimmRon + DqOdt) / 2 - 0.75 * Vdd) * 382 / Vdd
    RxVrefVal = (Vdd * (2 * DimmRon + CpuOdt)) / (DimmRon + CpuOdt) / 2;
    RxVrefCode = DIVIDEROUND (RxVrefVal * 512, Vdd);
  } else {
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_ERROR, "%s: unsupported ODT type for DDR4: %u\n", gErrString, OdtMode);
    return;
  }
  GetSetVal  = RxVrefCode;
  if (PrintMsg) {
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "CPU Odt = %d DimmRon = %d CPU Read Vref = %dmV\n", CpuOdt, DimmRon, RxVrefVal);
  }

  // Set all the bytes with the ideal value.
  MrcGetSetChStrb (MrcData, Controller, Channel, MAX_SDRAM_IN_DIMM, RxVref, GsMode, &GetSetVal);
}

/**
  Set default CPU Read Vref for DDR4 assuming CPU uses Vddq termination.
  Use default CPU ODT and DIMM Ron values.

  @param[in, out] MrcData      - Include all MRC global data.
  @param[in]      GetFromTable - Get the DIMM ODT parameters from the initial table.
  @param[in]      PrintMsg     - Enable debug prints
  @param[in]      PhyInit      - TRUE if the function is called from PHY init.

  @retval none
**/
void
MrcSetDefaultRxVrefDdr4 (
  IN OUT MrcParameters *const MrcData,
  IN BOOLEAN                  GetFromTable,
  IN BOOLEAN                  PrintMsg,
  IN BOOLEAN                  PhyInit
  )
{
  MrcOutput         *Outputs;
  MrcChannelOut     *ChannelOut;
  UINT32            EquOdt;
  UINT16            CPUImpedance;
  UINT16            DramRon;
  UINT16            MrRegVal;
  UINT32            EffODT;
  UINT8             NumRanks;
  UINT32            Controller;
  UINT8             Channel;
  UINT8             Rank;
  UINT8             Dimm;
  UINT8             SerialR;
  BOOLEAN           is2R;
  UINT8             RankInDimm;
  UINT8             MrIndex;
  UINT8             MrNum;
  DDR4_MODE_REGISTER_1_STRUCT *Mr1;
  MrcStatus         Status;

  Outputs           = &MrcData->Outputs;
  SerialR           = 15; // Ohm
  GetOptDimmParamMrIndex(MrcData, OptDimmRon, &MrIndex, &MrNum);

  for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
    for (Channel = 0; Channel < MAX_CHANNEL; Channel++) {
      if (!(MrcChannelExist (MrcData, Controller, Channel))) {
        continue;
      }
      ChannelOut = &Outputs->Controller[Controller].Channel[Channel];
      EffODT = 0;
      NumRanks = 0;
      DramRon = 0;
      is2R = FALSE; // when 2R is detected
      for (Rank = 0; Rank < MAX_RANK_IN_CHANNEL; Rank++) {
        if (MrcRankExist (MrcData, Controller, Channel, Rank)) {
          Dimm = RANK_TO_DIMM_NUMBER (Rank);
          RankInDimm = ChannelOut->Dimm[Dimm].RankInDimm;
          if ((!is2R) && (Dimm == 1) && (RankInDimm == 2)) {
            // If first DIMM is 1R and second DIMM is 2R, take into account only the 2R DIMM
            EffODT = 0;
            NumRanks = 0;
            DramRon = 0;
          }
          if ((RankInDimm == 1) && is2R) {
            // take into account only the 2R
            break;
          }
          EffODT += MrcGetEffDimmWriteOdt (MrcData, Controller, Channel, Rank, 1, GetFromTable);
          if (PhyInit) {
            MrRegVal = INIT_DIMM_RON_34;
          } else {
            Mr1 = (DDR4_MODE_REGISTER_1_STRUCT *)&ChannelOut->Dimm[Rank / MAX_RANK_IN_DIMM].Rank[(Rank % MAX_RANK_IN_DIMM)].MR[mrIndexMR1];
            MrRegVal = 240 / (5 + 2 * !Mr1->Bits.ODImpedance);
          }
          DramRon += (MrRegVal + SerialR);
          NumRanks++;
          if (RankInDimm == 2) {
            is2R = TRUE;
          }
        }
      }
      if (NumRanks > 0) {
        EffODT /= NumRanks;
        DramRon /= NumRanks;
      }

      if (PhyInit) {
        CPUImpedance = Outputs->RcompTarget[RdOdt];
      } else {
        Status = CalcRxDqOdtAverageByteImpedance (MrcData, (UINT8)Controller, Channel, &CPUImpedance);
        if (mrcSuccess != Status) {
          MRC_DEBUG_MSG (&Outputs->Debug, MSG_LEVEL_ERROR, "CalcRxDqOdtAverageByteImpedance() error %d\n", Status);
        }
      }
      MRC_POWER_DEBUG_MSG(&Outputs->Debug, MSG_LEVEL_NOTE, "MrcSetDefaultRxVref CPUImpedance = %d, Outputs->RcompTarget[RdOdt] = %d \n", CPUImpedance, Outputs->RcompTarget[RdOdt]);
      MRC_POWER_DEBUG_MSG(&Outputs->Debug, MSG_LEVEL_NOTE, "MrcSetDefaultRxVref DramRon = %d, INIT_DIMM_RON_34 + SerialR = %d \n", DramRon, INIT_DIMM_RON_34 + SerialR);

      EquOdt = CPUImpedance *  EffODT / (CPUImpedance + EffODT);
      MrcSetIdealRxVrefDdr4 (MrcData, Controller, Channel, DramRon, EquOdt, PrintMsg);
    } // channel loop
  } // Controller loop
}

/**
  Set default CPU Read Vref for DDR5 assuming CPU uses Vddq termination.
  Use default CPU ODT and DIMM Ron values.

  @param[in, out] MrcData      - Include all MRC global data.
  @param[in]      GetFromTable - Get the DIMM ODT parameters from the initial table.
  @param[in]      PrintMsg     - Enable debug prints
  @param[in]      PhyInit      - TRUE if the function is called from PHY init.

  @retval none
**/
void
MrcSetDefaultRxVrefDdr5 (
  IN OUT MrcParameters *const MrcData,
  IN BOOLEAN                  GetFromTable,
  IN BOOLEAN                  PrintMsg,
  IN BOOLEAN                  PhyInit
  )
{
  MrcOutput         *Outputs;
  MrcChannelOut     *ChannelOut;
  UINT32            EquOdt;
  UINT16            CPUImpedance;
  UINT16            DramRon;
  UINT16            MrRegVal;
  UINT32            EffODT;
  UINT8             NumRanks;
  UINT32            Controller;
  UINT8             Channel;
  UINT8             Rank;
  UINT8             Dimm;
  UINT8             SerialR;
  BOOLEAN           is2R;
  UINT8             RankInDimm;
  UINT8             MrIndex;
  UINT8             MrNum;
  DDR5_MODE_REGISTER_5_TYPE *Mr5;
  MrcStatus         Status;

  Outputs = &MrcData->Outputs;
  SerialR = 0; // Ohm
  GetOptDimmParamMrIndex (MrcData, OptDimmRon, &MrIndex, &MrNum);

  for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
    for (Channel = 0; Channel < MAX_CHANNEL; Channel++) {
      if (!(MrcChannelExist (MrcData, Controller, Channel))) {
        continue;
      }
      ChannelOut = &Outputs->Controller[Controller].Channel[Channel];
      EffODT = 0;
      NumRanks = 0;
      DramRon = 0;
      is2R = FALSE; // when 2R is detected
      for (Rank = 0; Rank < MAX_RANK_IN_CHANNEL; Rank++) {
        if (MrcRankExist (MrcData, Controller, Channel, Rank)) {
          Dimm = RANK_TO_DIMM_NUMBER (Rank);
          RankInDimm = ChannelOut->Dimm[Dimm].RankInDimm;
          if ((!is2R) && (Dimm == 1) && (RankInDimm == 2)) {
            // If first DIMM is 1R and second DIMM is 2R, take into account only the 2R DIMM
            EffODT = 0;
            NumRanks = 0;
            DramRon = 0;
          }
          if ((RankInDimm == 1) && is2R) {
            // take into account only the 2R
            break;
          }
          EffODT += MrcGetEffDimmWriteOdt (MrcData, Controller, Channel, Rank, 1, GetFromTable);//Read mode
          if (PhyInit) {
            MrRegVal = INIT_DIMM_RON_34;
          } else {
            Mr5 = (DDR5_MODE_REGISTER_5_TYPE *)&ChannelOut->Dimm[Rank / MAX_RANK_IN_DIMM].Rank[(Rank % MAX_RANK_IN_DIMM)].MR[mrIndexMR5];
            MrRegVal = 240 / (5 + 2 * !Mr5->Bits.PullUpOutputDriverImpedance);
          }
          DramRon += (MrRegVal + SerialR);
          NumRanks++;
          if (RankInDimm == 2) {
            is2R = TRUE;
          }
        }
      }
      if (NumRanks > 0) {
        EffODT /= NumRanks;
        DramRon /= NumRanks;
      }

      if (PhyInit) {
        CPUImpedance = Outputs->RcompTarget[RdOdt];
      } else {
        Status = CalcRxDqOdtAverageByteImpedance (MrcData, (UINT8)Controller, Channel, &CPUImpedance);
        if (mrcSuccess != Status) {
          MRC_DEBUG_MSG (&Outputs->Debug, MSG_LEVEL_ERROR, "CalcRxDqOdtAverageByteImpedance() error %d\n", Status);
        }
      }
      //MRC_DEBUG_MSG (&Outputs->Debug, MSG_LEVEL_NOTE, "MrcSetDefaultRxVref CPUImpedance = %d, Outputs->RcompTarget[RdOdt] = %d \n", CPUImpedance, Outputs->RcompTarget[RdOdt]);
      //MRC_DEBUG_MSG (&Outputs->Debug, MSG_LEVEL_NOTE, "MrcSetDefaultRxVref DramRon = %d, INIT_DIMM_RON_34 + SerialR = %d \n", DramRon, INIT_DIMM_RON_34 + SerialR);

      EquOdt = CPUImpedance *  EffODT / (CPUImpedance + EffODT);
      MrcSetIdealRxVrefDdr4 (MrcData, Controller, Channel, DramRon, EquOdt, PrintMsg);
    } // channel loop
  } // Controller loop
}

/**
  Set ideal Dimm Write Vref for DDR4 assuming CPU Ron and Dimm termination.

  @param[in, out] MrcData     - Include all MRC global data.
  @param[in]      Controller  - Memory Controller Number within the processor (0-based).
  @param[in]      Channel     - Channel to setup
  @param[in]      RankBitMask - Rank bit mask to setup
  @param[in]      Byte        - Byte to setup (for PDA mode only)
  @param[in]      CpuRon      - CPU Ron value in Ohm
  @param[in]      OdtWr       - Write DIMM Odt value in Ohm
  @param[in]      DimmOdt     - Equ. DIMM Odt value in Ohm

  @retval none
**/
void
MrcSetIdealTxVrefDdr4 (
  IN OUT MrcParameters *const MrcData,
  IN UINT32                   Controller,
  UINT8                       Channel,
  UINT8                       RankBitMask,
  UINT8                       Byte,
  UINT32                      CpuRon,
  UINT32                      OdtWr,
  UINT32                      DimmOdt
  )
{
  const MrcInput    *Inputs;
  MrcDebug          *Debug;
  MrcOutput         *Outputs;
  UINT8             Rank;
  INT32             TxVrefMid;
  INT32             TxVref;
  INT32             TxVrefCode;
  MrcVddSelect      Vdd;
  UINT8             SerialR;

  Inputs            = &MrcData->Inputs;
  Outputs           = &MrcData->Outputs;
  Debug             = &Outputs->Debug;
  SerialR           = 15; // Ohm

  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "CpuRon = %d, OdtWr = %d, DimmOdt = %d\n", CpuRon, OdtWr, DimmOdt);
  Vdd = Outputs->VddVoltage[Inputs->MemoryProfile];
  TxVrefMid = (Vdd * (2 * CpuRon + DimmOdt)) / (CpuRon + DimmOdt) / 2;
  TxVref = (Vdd * SerialR + TxVrefMid * OdtWr) / (SerialR + OdtWr);
  TxVref += 30;   // Add 30mV
  if (Inputs->MemoryProfile != STD_PROFILE) {
    if (Outputs->Frequency >= f4000) {
      TxVref -= 130;
    } else if (Outputs->Frequency >= f3733) {
      TxVref -= 100;
    }
  }

  TxVref *= 1000; // convert to uV
  TxVrefCode = DIVIDEROUND (TxVref - 1000 * DDR4_VREF_MIDDLE_RANGE, DDR4_VREF_STEP_SIZE);
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Dimm Write Vref = %dmV, TxVref Code = %d\n", TxVref / 1000, TxVrefCode);

  for (Rank = 0; Rank < MAX_RANK_IN_CHANNEL; Rank++) {
    if (!(MrcRankExist (MrcData, Controller, Channel, Rank) & RankBitMask)) {
      continue;
    }
    MrcSetTxVrefDdr4 (MrcData, Controller, Channel, Rank, 0, TxVrefCode, TRUE, FALSE);
  } // for Rank
}

/**
  Set default Tx Dimm Vref for DDR4.
  Use default CPU Ron and DIMM Write Odt values.

  @param[in, out] MrcData - Include all MRC global data.

  @retval none
**/
void
MrcSetDefaultTxVrefDdr4 (
  IN OUT MrcParameters *const MrcData
  )
{
  UINT32  Controller;
  UINT8   Channel;
  UINT8   Rank;
  UINT32  EffODT;
  UINT32  OdtWr;

  for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
    for (Channel = 0; Channel < MAX_CHANNEL; Channel++) {
      if (!(MrcChannelExist (MrcData, Controller, Channel))) {
        continue;
      }
      for (Rank = 0; Rank < MAX_RANK_IN_CHANNEL; Rank++) {
        if (MrcRankExist (MrcData, Controller, Channel, Rank)) {
          EffODT = MrcGetEffDimmWriteOdt (MrcData, Controller, Channel, Rank, 0, FALSE); // GetFromTable = FALSE because we already programmed the MR's
          OdtWr = CalcDimmImpedance (MrcData, Controller,  Channel, Rank, OptDimmOdtWr, FALSE, 0, FALSE);
          OdtWr = (OdtWr == 0) ? 0xFFFF : OdtWr;
          MrcSetIdealTxVrefDdr4 (MrcData, Controller, Channel, 0x1 << Rank, 0, MrcData->Outputs.RcompTarget[WrDS], OdtWr, EffODT);
        }
      }
    } // channel loop
  } // controller loop
}


/**
  Returns the effective Dimm Write ODT.
  Consider the DRAM tech, Rtt configuration (Wr, Nom, Park) and Normal Odt logic.
  Note: We still have the assumption of same Rtt's for all ranks in the same DIMM.

  @param[in, out] MrcData       - Include all MRC global data.
  @param[in]      Controller    - Controller to setup
  @param[in]      Channel       - Channel to setup
  @param[in]      Rank          - Rank to setup
  @param[in]      OdtType       - 0:write 1:read
  @param[in]      GetFromTable  - Get the Values from Odt tables

  @retval value in ohms
**/
UINT16
MrcGetEffDimmWriteOdt (
  IN OUT MrcParameters *const MrcData,
  IN     UINT32               Controller,
  IN     UINT8                Channel,
  IN     UINT8                Rank,
  IN     UINT8                OdtType,
  IN     BOOLEAN              GetFromTable
  )
{
  MrcOutput     *Outputs;
  MrcChannelOut *ChannelOut;
  UINT32        OdtWr;
  UINT32        OdtNom;
  UINT32        OdtNom1;
  UINT32        OdtPark;
  UINT32        OdtPark1;
  BOOLEAN       Ddr4;
  BOOLEAN       Is2DPC;
  UINT32        RttT;
  UINT32        RttNT;
  UINT32        EffOdt;
  UINT8         Dimm;
  UINT8         OtherDimm;
  UINT8         SerialR;
  BOOLEAN       Ddr5;
  UINT8         ParamSelect;

  Outputs           = &MrcData->Outputs;
  ChannelOut        = &Outputs->Controller[Controller].Channel[Channel];

  Is2DPC = (ChannelOut->DimmCount == 2);
  Ddr4 = (Outputs->DdrType == MRC_DDR_TYPE_DDR4);
  Ddr5 = (Outputs->DdrType == MRC_DDR_TYPE_DDR5);
  Dimm = RANK_TO_DIMM_NUMBER (Rank);
  OtherDimm = (Dimm ^ 1) & 1;
  OdtPark = OdtPark1 = 0xFFFF;
  OdtNom = OdtNom1 = 0xFFFF;

  // @todo: This doesn't exist in Memory Down.
  SerialR = Ddr5 ? 0 : 15; // Ohm resistor on the dimm card

  OdtWr = CalcDimmImpedance (MrcData, Controller, Channel, Rank, OptDimmOdtWr, FALSE, 0, GetFromTable);
  ParamSelect = Ddr4 ? OptDimmOdtNom : (OdtType) ? OptDimmOdtNomRd : OptDimmOdtNomWr;
  OdtNom = CalcDimmImpedance (MrcData, Controller, Channel, Rank, ParamSelect, FALSE, 0, GetFromTable);

  if (Is2DPC) {
    OdtNom1 = CalcDimmImpedance (MrcData, Controller, Channel, 2 * OtherDimm, ParamSelect, FALSE, 0, GetFromTable);
  }
  if (Ddr4 || Ddr5) {
    OdtPark = CalcDimmImpedance (MrcData, Controller, Channel, Rank, OptDimmOdtPark, FALSE, 0, GetFromTable);
    if (Is2DPC) {
      OdtPark1 = CalcDimmImpedance (MrcData, Controller, Channel, 2 * OtherDimm, OptDimmOdtPark, FALSE, 0, GetFromTable);
    }
  }
   // MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Channel = %d OdtWr = %d, OdtNom = %d OdtPark = %d\n", Channel, OdtWr, OdtNom, OdtPark);
  // Add the serial resistor and protect clip at 16k to not overflow when mul
  OdtWr     = (OdtWr == 0) ? 0 : OdtWr + SerialR;
  OdtNom    = MIN((OdtNom + SerialR), 0xFFFF);
  OdtNom1   = MIN((OdtNom1 + SerialR), 0xFFFF);
  OdtPark   = MIN((OdtPark + SerialR), 0xFFFF);
  OdtPark1  = MIN((OdtPark1 + SerialR), 0xFFFF);
  // Check that in DDR4 the mc toggles odt pin for the target rank for write.
  if (Ddr4 || Ddr5) {
    RttT = (OdtWr == 0) ? 0xFFFF : OdtWr;
  } else {
    RttT = (OdtWr == 0) ? OdtNom : OdtWr;
  }

  // calc the effective park value
  if (Ddr4 || Ddr5) {
    if (ChannelOut->Dimm[Dimm].RankInDimm == 1) {
      OdtPark = 0xFFFF;
    }
    if (Is2DPC) {
      // in 1R we will have only Rtt nom
      if (ChannelOut->Dimm[OtherDimm].RankInDimm == 2) {
        OdtPark = DIVIDEROUND (OdtPark * OdtPark1, OdtPark + OdtPark1);
      }
    }
  }

  // calc the effective NT value
  if (Is2DPC) {
    RttNT = (Ddr4 || Ddr5) ? DIVIDEROUND (OdtPark * OdtNom1, OdtPark + OdtNom1) : OdtNom1;
  } else {
    RttNT = (Ddr4 || Ddr5) ? OdtPark : 0xFFFF;
  }

  if (OdtType == 0) { // Write - Target Equivalent Rtt
    EffOdt = DIVIDEROUND (RttT * RttNT, RttT + RttNT);
  } else { // Read - NonTarget Equivalent Rtt
    EffOdt = RttNT;
  }
  return (UINT16) EffOdt;
}

/**
  Generic routine to perform Linear Centering in different 2D points.
  Re-uses DQTimeCentering1D routine as the linear centering routine.
  Optimize Per channel in the 2D param space.
  Run-Time - about 20nsec for 6 vref points and loopcout of 10

  @param[in,out] MrcData        - Include all MRC global data.
  @param[in]     ChBitMask      - Channels to test.
  @param[in]     RankBitMask    - Ranks to test.
  @param[in]     Param          - Param to center with linear search - use DQTimeCentering1D (RdT,WrT,WrV,RdV,RcvX)
  @param[in]     Param2D        - The 2D Param to Offset - Use change margin routine
  @param[in]     Points2DMin    - Minimum value of second dimension points to sweep.
  @param[in]     Points2DMax    - Maxmimum value of second dimension points to sweep.
  @param[in]     ParamStepSize  - Step size of second dimension points to sweep.
  @param[in]     LoopCount      - Loop count
  @param[in]     TestType       - 0: normal reads, 1: Read MPR
  @param[in]     Prints         - Debug prints enable/disable

  @retval MrcStatus - mrcSuccess if passed, otherwise an error status.
**/
MrcStatus
EarlyLinearCentering2D (
  IN OUT MrcParameters *const MrcData,
  IN     UINT8                McChBitMask,
  IN     UINT8                RankBitMask,
  IN     UINT8                Param,
  IN     UINT8                Param2D,
  IN     INT16                Points2DMin,
  IN     INT16                Points2DMax,
  IN     UINT8                ParamStepSize,
  IN     UINT8                LoopCount,
  IN     UINT8                TestType,
  IN     BOOLEAN              Prints
  )
{
  const MrcInput      *Inputs;
  MrcDebug            *Debug;
  const MRC_FUNCTION  *MrcCall;
  MrcOutput           *Outputs;
  MrcControllerOut    *ControllerOut;
  MrcChannelOut       *ChannelOut;
  MrcStatus           Status;
  UINT16              (*MarginByte)[MAX_RANK_IN_CHANNEL][MAX_CONTROLLER][MAX_CHANNEL][MAX_SDRAM_IN_DIMM][MAX_EDGES];
  INT64               GetSetVal;
  INT16               Idx;
  INT16               Best2DPoint[MAX_CONTROLLER][MAX_CHANNEL];
  UINT16              Best1DEye[MAX_CONTROLLER][MAX_CHANNEL];
  UINT16              MinEye[MAX_CONTROLLER][MAX_CHANNEL];
  UINT16              EyeSize;
  UINT8               ResultType;
  UINT8               Controller;
  UINT8               Channel;
  UINT8               Rank;
  UINT8               Byte;
  UINT8               MaxChannel;
  UINT8               SdramCount;
  UINT8               ChRankMask;
  UINT8               MinByte[MAX_CONTROLLER][MAX_CHANNEL];
  UINT8               MinRank[MAX_CONTROLLER][MAX_CHANNEL];
  UINT8               BestByte[MAX_CONTROLLER][MAX_CHANNEL];
  UINT8               BestRank[MAX_CONTROLLER][MAX_CHANNEL];
  UINT16              MinEyePerByte[MAX_CONTROLLER][MAX_CHANNEL][MAX_SDRAM_IN_DIMM];
  UINT8               MinRankPerByte[MAX_CONTROLLER][MAX_CHANNEL][MAX_SDRAM_IN_DIMM];
  UINT16              Best1DEyePerByte[MAX_CONTROLLER][MAX_CHANNEL][MAX_SDRAM_IN_DIMM];
  INT16               Best2DPointPerByte[MAX_CONTROLLER][MAX_CHANNEL][MAX_SDRAM_IN_DIMM];
  UINT8               BestRankPerByte[MAX_CONTROLLER][MAX_CHANNEL][MAX_SDRAM_IN_DIMM];
  BOOLEAN             Ddr4;
  BOOLEAN             Ddr5;

  Inputs              = &MrcData->Inputs;
  MrcCall             = Inputs->Call.Func;
  Outputs             = &MrcData->Outputs;
  Debug               = (MRC_PRINTS_ON == Prints) ? &Outputs->Debug : NULL;
  Status              = mrcSuccess;
  Ddr4                = (Outputs->DdrType == MRC_DDR_TYPE_DDR4);
  Ddr5                = (Outputs->DdrType == MRC_DDR_TYPE_DDR5);
  MaxChannel          = Outputs->MaxChannels;
  SdramCount          = Outputs->SdramCount;
  MarginByte          = Outputs->MarginResult;
  MrcCall->MrcSetMemWord ((UINT16 *) Best1DEye, ARRAY_COUNT2D (Best1DEye), 0);
  MrcCall->MrcSetMemWord ((UINT16 *) Best2DPoint, ARRAY_COUNT2D (Best2DPoint), Points2DMin);
  MrcCall->MrcSetMem ((UINT8 *) BestByte, sizeof (BestByte), 0);
  MrcCall->MrcSetMem ((UINT8 *) BestRank, sizeof (BestRank), 0);
  MrcCall->MrcSetMem ((UINT8 *) Best1DEyePerByte,   sizeof (Best1DEyePerByte),   0);
  MrcCall->MrcSetMem ((UINT8 *) Best2DPointPerByte, sizeof (Best2DPointPerByte), 0);
  MrcCall->MrcSetMem ((UINT8 *) BestRankPerByte,    sizeof (BestRankPerByte),    0);

  MRC_DEBUG_ASSERT (Points2DMin < Points2DMax, Debug, "%s: Invalid Parameters, Points2DMin(%d) and Points2DMax(%d)\n", gErrString, Points2DMin, Points2DMax);
  MRC_DEBUG_ASSERT (((TestType == ERTC2D_NORMAL) || (TestType == ERTC2D_MPR)), Debug,"%s invalid TestType requested: %d\n", gErrString, TestType);

  // ####################################################
  // ################  Initialize EW/EH variables  ######
  // ####################################################

#ifdef MRC_DEBUG_PRINT
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\n### Measure Eye Width, per BYTE, at ALL (2D) Timing Points - RankBitMask = 0x%X\n", RankBitMask);
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Vref Sweep Range [%d:%d] and Step Size of %d\n", Points2DMin, Points2DMax, ParamStepSize);

  for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\tMc %d", Controller);
    for (Channel = 0; Channel < MaxChannel; Channel++) {
      MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\tChannel %d", Channel);
      for (Byte = 0; Byte < SdramCount - 2; Byte++) {
        MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\t");
      }
    }
  }

  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\nByte\t");
  for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
    for (Channel = 0; Channel < MaxChannel; Channel++) {
      for (Byte = 0; Byte < SdramCount; Byte++) {
        MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "%d\t", Byte);
      }
    }
  }
#endif // MRC_DEBUG_PRINT

  Status      = GetMarginByte (MrcData, Outputs->MarginResult, Param, 0, RankBitMask);
  ResultType  = GetMarginResultType (Param);

  // ####################################################
  // ######   Measure Eye Width at all Vref Points  #####
  // ####################################################
  // Loop through all the 2D Points to Test
  for (Idx = Points2DMin; Idx <= Points2DMax; Idx+=ParamStepSize) {
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\n\nVref: %d\t", Idx);

    // Setup 2D Offset for this point
    for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
      ControllerOut = &Outputs->Controller[Controller];
      for (Channel = 0; Channel < MaxChannel; Channel++) {
        if (!((1 << ((Controller * MaxChannel) + Channel)) & McChBitMask)) {
          continue;
        }
        ChannelOut = &ControllerOut->Channel[Channel];
        ChRankMask = RankBitMask & ChannelOut->ValidRankBitMask;
        if (ChRankMask == 0) {
          continue;
        }
        if (Param2D == RdV) {
          GetSetVal = Idx;
          MrcGetSetChStrb (MrcData, Controller, Channel, MAX_SDRAM_IN_DIMM, RxVref, WriteCached, &GetSetVal);
        } else {
          Status = ChangeMargin (MrcData, Param2D, Idx, 0, 0,  Controller, Channel, ((Ddr4 || Ddr5) && (Param2D == WrV)) ? ChRankMask : 0xFF, 0, 0, 0, 0);
        }
      }
    }

    // Run Param Margin Test
    if (TestType == ERTC2D_NORMAL) {
      Status = DQTimeCentering1D (MrcData, Param, 0, LoopCount, MRC_PRINTS_OFF, TRUE, 1);
    } else if (TestType == ERTC2D_MPR) {
      // Status = ReadMprTraining (MrcData, TRUE); // HW-based ReadMPR if this is done before ReadLeveling (cannot use CPGC)
      Status = MrcReadMprTrainingNormal (MrcData, RdT, FALSE, MRC_PRINTS_OFF);  // CPGC-based
    }

#ifdef MRC_DEBUG_PRINT
    for (Rank = 0; Rank < MAX_RANK_IN_CHANNEL; Rank++) {
      if ((0x1 << Rank) & Outputs->ValidRankMask & RankBitMask) {
        MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\nR%d L-R\t",Rank);
        for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
          for (Channel = 0; Channel < MaxChannel; Channel++) {
            if (!MrcRankExist (MrcData, Controller, Channel, Rank)) {
              for (Byte = 0; Byte < SdramCount; Byte++) {
                MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\t", Byte);
              }
            } else {
              for (Byte = 0; Byte < SdramCount; Byte++) {
                MRC_DEBUG_MSG (
                  Debug,
                  MSG_LEVEL_NOTE,
                  (MrcByteExist (MrcData, Controller, Channel, Byte)) ? "%d-%d\t" : "\t",
                  MarginByte[ResultType][Rank][Controller][Channel][Byte][0] / 10,
                  MarginByte[ResultType][Rank][Controller][Channel][Byte][1] / 10
                );
              }
            }
          }
        }
      }
    }
#endif // MRC_DEBUG_PRINT

    // Store Results
    MrcCall->MrcSetMem ((UINT8 *) MinEye,  sizeof (MinEye),  0xFF);
    MrcCall->MrcSetMem ((UINT8 *) MinByte, sizeof (MinByte), 0xFF);
    MrcCall->MrcSetMem ((UINT8 *) MinRank, sizeof (MinRank), 0xFF);
    MrcCall->MrcSetMem ((UINT8 *) MinEyePerByte,  sizeof (MinEyePerByte),  0xFF);
    MrcCall->MrcSetMem ((UINT8 *) MinRankPerByte, sizeof (MinRankPerByte), 0xFF);

    for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
      ControllerOut = &Outputs->Controller[Controller];
      for (Channel = 0; Channel < MaxChannel; Channel++) {
        ChannelOut = &ControllerOut->Channel[Channel];
        for (Byte = 0; Byte < SdramCount; Byte++) {
          for (Rank = 0; Rank < MAX_RANK_IN_CHANNEL; Rank++) {
            if ((1 << Rank) & Outputs->ValidRankMask & RankBitMask) {
              if (!MrcRankExist (MrcData, Controller, Channel, Rank)) {
                continue;
              }
              EyeSize = (UINT16) (MarginByte[ResultType][Rank][Controller][Channel][Byte][1] + MarginByte[ResultType][Rank][Controller][Channel][Byte][0]);
              if (Param2D == RdV) { // HW restriction: Use same vref on all Ranks Per Ch/Byte
                if (MinEyePerByte[Controller][Channel][Byte] > EyeSize) {
                  MinEyePerByte[Controller][Channel][Byte] = EyeSize;     // Merged rank information to min val on all ranks
                  MinRankPerByte[Controller][Channel][Byte] = Rank;
                }
              } else {
                // Track minimum eye width per ch/rank
                if (MinEye[Controller][Channel] > EyeSize) {
                  MinEye[Controller][Channel] = EyeSize;
                  MinByte[Controller][Channel] = Byte;
                  MinRank[Controller][Channel] = Rank;
                } // if MinEye
              } // if (Param2D == RdV)
            } // if Rank exists
          } // for Rank
        } // for Byte
      } // for Channel
    } // for Controller

    // Track best eye in the 2D space
    for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
      for (Channel = 0; Channel < MaxChannel; Channel++) {
        if (!MrcChannelExist (MrcData, Controller, Channel)) {
          continue;
        }
        if (RdV == Param2D) {
          for (Byte = 0; Byte < Outputs->SdramCount; Byte++) {
            if (Best1DEyePerByte[Controller][Channel][Byte] < MinEyePerByte[Controller][Channel][Byte]) {
              Best2DPointPerByte[Controller][Channel][Byte] = Idx;
              Best1DEyePerByte[Controller][Channel][Byte]   = MinEyePerByte[Controller][Channel][Byte];
              BestRankPerByte[Controller][Channel][Byte]    = MinRankPerByte[Controller][Channel][Byte];
            }
          }
        } else {
          if (Best1DEye[Controller][Channel] < MinEye[Controller][Channel]) {
            Best2DPoint[Controller][Channel]  = Idx;
            Best1DEye[Controller][Channel]    = MinEye[Controller][Channel];
            BestByte[Controller][Channel]     = MinByte[Controller][Channel];
            BestRank[Controller][Channel]     = MinRank[Controller][Channel];
          }
        } // if (RdV == Param2D) {
      } // for Channel
    } // for Controller
  } // 2D Points
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\n");

  // ####################################################
  // ###########   Center Results per Byte   ############
  // ####################################################
  for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
    ControllerOut = &Outputs->Controller[Controller];
    for (Channel = 0; Channel < MaxChannel; Channel++) {
      if (!((1 << ((Controller * MaxChannel) + Channel)) & McChBitMask)) {
        continue;
      }
      ChannelOut = &ControllerOut->Channel[Channel];
      ChRankMask = RankBitMask & ChannelOut->ValidRankBitMask;
      if (ChRankMask == 0) {
        continue;
      }

      // Note: the Change margin will work only on non TrainOffset params like WrV, RcvEnX (if other needed use GetSet)
      if (Param2D == RdV) {
        for (Byte = 0; Byte < SdramCount; Byte++) {
          GetSetVal = Best2DPointPerByte[Controller][Channel][Byte];
          MrcGetSetChStrb (MrcData, Controller, Channel, Byte, RxVref, WriteToCache, &GetSetVal);
          MRC_DEBUG_MSG (
            Debug,
            MSG_LEVEL_NOTE,
            "\nMc %d Channel %d Byte %d: Selected Vref <%d> Smallest eye width <%d> found at Rank %d",
            Controller,
            Channel,
            Byte,
            Best2DPointPerByte[Controller][Channel][Byte],
            Best1DEyePerByte[Controller][Channel][Byte],
            BestRankPerByte[Controller][Channel][Byte]
          );
        }
      } else {
        MRC_DEBUG_MSG (
          Debug,
          MSG_LEVEL_NOTE,
          "\nMc %d Channel %d: Selected Vref <%d>  Smallest eye width <%d> found at Rank %d Byte %d",
          Controller,
          Channel,
          Best2DPoint[Controller][Channel],
          Best1DEye[Controller][Channel],
          BestRank[Controller][Channel],
          BestByte[Controller][Channel]
        );
        ChangeMargin (MrcData, Param2D, Best2DPoint[Controller][Channel], 0, 0, Controller, Channel, ((Ddr4 || Ddr5) && (Param2D == WrV)) ? ChRankMask : 0xFF, 0, 0, 1, 1);
      }
    } // for Channel
  } // for Controller
  MrcFlushRegisterCachedData (MrcData);  // Flush updated results to the registers.

  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\n");
  // final centering at best offset
  if (TestType == ERTC2D_NORMAL) {
    if (Param2D == RdV) {
      // In WrV case this function is called per DIMM, hence the caller will do the final centering at best vref after both DIMMs are done,
      // because DQTimeCentering1D does not have a rank mask argument and always runs on all ranks.
      Status = DQTimeCentering1D (MrcData, Param, 0, LoopCount, MRC_PRINTS_OFF, TRUE, 1);
    }
  } else if (TestType == ERTC2D_MPR) {
    //Status = ReadMprTraining (MrcData, FALSE);  // HW-based RdMPR if it's used before Read Leveling
    if (Outputs->Frequency > f4800) {
      Status =  MrcReadMprTrainingNormal (MrcData, RdTN, TRUE,  MRC_PRINTS_ON); // CPGC-based, per-bit
      Status |= MrcReadMprTrainingNormal (MrcData, RdTP, TRUE,  MRC_PRINTS_ON); // CPGC-based, per-bit
    }
    if (Outputs->Frequency > f2667) {
      Status =  MrcReadMprTrainingNormal (MrcData, RdTN, FALSE, MRC_PRINTS_ON); // CPGC-based
      Status |= MrcReadMprTrainingNormal (MrcData, RdTP, FALSE, MRC_PRINTS_ON); // CPGC-based
    } else {
      Status = MrcReadMprTrainingNormal (MrcData, RdT,  FALSE, MRC_PRINTS_ON); // CPGC-based
    }
  }

  if (Param2D == RdV) {
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Final centering at best vref %s\n", (Status == mrcSuccess) ? "Passed" : "Failed");
  } else {
    Status = mrcSuccess; // Caller will do final centering at best WrV
  }

  return Status;
}

/**
  Get the Other Dimm in Channel.

  @param[in, out] MrcData  - Include all MRC global data.
  @param[in]      Channel  - Channel to search in.
  @param[in]      RankMask - Rank mask for the current DIMM.

  @retval The other Dimm outside the RankMask.  If no such DIMM, then return current DIMM.
**/
UINT8
MrcGetOtherDimm (
  IN MrcParameters *const MrcData,
  IN UINT8                Channel,
  IN UINT8                RankMask
  )
{
  UINT8 Dimm;
  UINT8 CurrentDimm;
  UINT8 OtherDimm;
  UINT8 ChannelRankMask;

  ChannelRankMask = MrcData->Outputs.Controller[0].Channel[Channel].ValidRankBitMask;
  CurrentDimm    = (RankMask & 0x3) ? 0 : 1;

  OtherDimm = CurrentDimm;
  for (Dimm = 0; Dimm < MAX_DIMMS_IN_CHANNEL; Dimm++) {
    if (Dimm == CurrentDimm) {
      continue;
    } else {
      if (DIMM_TO_RANK_MASK (Dimm) & ChannelRankMask) {
        OtherDimm = Dimm;
        break;
      }
    }
  }

  return OtherDimm;
}

/**
  Get the maximal possible offset for a given Param (e.g., WrV, RdV) and DDR technology.

  @param[in]      MrcData  - Include all MRC global data.
  @param[in]      Param    - Parameter to get the max possible offset for.

  @retval UINT32 variable the maximal possible offset.
**/
UINT8
GetVrefOffsetLimits (
  IN  MrcParameters *const MrcData,
  IN  UINT8                param
  )
{
  MrcOutput        *Outputs;
  MrcDebug         *Debug;
  BOOLEAN          Ddr4;
  BOOLEAN          Ddr5;
  BOOLEAN          Lpddr4;
  BOOLEAN          Lpddr5;
  UINT8            MaxMargin;

  Outputs = &MrcData->Outputs;
  Debug   = &Outputs->Debug;
  Ddr4    = (Outputs->DdrType == MRC_DDR_TYPE_DDR4);
  Ddr5    = (Outputs->DdrType == MRC_DDR_TYPE_DDR5);
  Lpddr4  = (Outputs->DdrType == MRC_DDR_TYPE_LPDDR4);
  Lpddr5  = (Outputs->DdrType == MRC_DDR_TYPE_LPDDR5);

  switch (param) {
    case RdV:
    case RdFan2:
    case RdFan3:
      // RdV supplied by CPU, identical for all technologies
      MaxMargin = MAX_RX_VREF_OFFSET;
      break;

    case WrV:
    case WrFan2:
    case WrFan3:
      // WrV technology dependent
      if (Ddr4) {
        MaxMargin = MAX_POSSIBLE_DDR4_WRITE_VREF;
      } else if (Ddr5) {
        MaxMargin = MAX_POSSIBLE_DDR5_WRITE_VREF;
      } else if (Lpddr4) {
        MaxMargin = MRC_LP4_VREF_OFFSET_MAX;
      } else { // LPDDR5
        MaxMargin = LP5_VREF_OFFSET_MAX;
      }
      break;

    case CmdV:
      // CmdV technology dependent
      if (Lpddr4) {
        MaxMargin = MRC_LP4_VREF_OFFSET_MAX;
      } else if (Lpddr5) {
        MaxMargin = LP5_VREF_OFFSET_MAX;
      } else if (Ddr5) {
        MaxMargin = MAX_POSSIBLE_DDR5_WRITE_VREF;
      } else { // DDR4
        MaxMargin = MAX_DDR4_CMDVREF_OFFSET;
      }
      break;

    default:
      // Unknown input param
      MaxMargin = 0;
      MRC_DEBUG_MSG(Debug, MSG_LEVEL_WARNING, "Warning unexpected param %d in GetVrefOffsetLimits\n", MaxMargin);
      break;
  }

  return MaxMargin;
}

/**
  Relax RDRD same rank turnarounds on all channels.
  Used during early write training steps to avoid stressing reads.

  @param[in] MrcData - Include all MRC global data.
  @param[in] Relax   - TRUE: relax the TAT values, FALSE: restore original values.
  @param[in] RdRdsg  - This parameter gets assigned inside the function before we relax the rd-rd timing.
  @param[in] RdRddg  - This parameter gets assigned inside the function before we relax the rd-rd timing.
**/
void
MrcRelaxReadToReadSameRank (
  IN  MrcParameters *const MrcData,
  IN BOOLEAN               Relax,
  IN OUT INT64          *const RdRdsg,
  IN OUT INT64          *const RdRddg
  )
{
  UINT32  Controller;
  UINT32  Channel;
  INT64   GetSetVal;

  for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
    for (Channel = 0; Channel < MrcData->Outputs.MaxChannels; Channel++) {
      if ((!MrcChannelExist (MrcData, Controller, Channel)) || IS_MC_SUB_CH (MrcData->Outputs.Lpddr, Channel)) {
        continue;
      }
      if (Relax) {
        MrcGetSetMcCh (MrcData, Controller, Channel, GsmMctRDRDsg, ReadCached | PrintValue, RdRdsg);
        MrcGetSetMcCh (MrcData, Controller, Channel, GsmMctRDRDdg, ReadFromCache | PrintValue, RdRddg);
        GetSetVal = 32;
        MrcGetSetMcCh (MrcData, Controller, Channel, GsmMctRDRDsg, WriteToCache | PrintValue, &GetSetVal);
        MrcGetSetMcCh (MrcData, Controller, Channel, GsmMctRDRDdg, WriteToCache | PrintValue, &GetSetVal);
      } else {
        MrcGetSetMcCh (MrcData, Controller, Channel, GsmMctRDRDsg, WriteToCache | PrintValue, RdRdsg);
        MrcGetSetMcCh (MrcData, Controller, Channel, GsmMctRDRDdg, WriteToCache | PrintValue, RdRddg);
      }
      MrcFlushRegisterCachedData (MrcData);
      // Must update the XARB bubble injector when TAT values change
      SetTcBubbleInjector (MrcData, Controller, Channel);
    }
  }
}

/**
  Relax RDWR same rank turnarounds on all channels.
  Used during Write Leveling Flyby to avoid RD and WR data overlap when WR burst is coming too early.

  @param[in] MrcData - Include all MRC global data.
  @param[in] Relax   - TRUE: relax the TAT values, FALSE: restore original values.
  @param[in] RdWrsg  - This parameter gets assigned inside the function before we relax the rd-wr timing.
  @param[in] RdWrdg  - This parameter gets assigned inside the function before we relax the rd-wr timing.
**/
void
MrcRelaxReadToWriteSameRank (
  IN  MrcParameters *const MrcData,
  IN BOOLEAN               Relax,
  IN INT64          *const RdWrsg,
  IN INT64          *const RdWrdg
  )
{
  UINT32  Controller;
  UINT32  Channel;
  INT64   GetSetVal;

  for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
    for (Channel = 0; Channel < MrcData->Outputs.MaxChannels; Channel++) {
      if ((!MrcChannelExist (MrcData, Controller, Channel)) || IS_MC_SUB_CH (MrcData->Outputs.Lpddr, Channel)) {
        continue;
      }
      if (Relax) {
        MrcGetSetMcCh (MrcData, Controller, Channel, GsmMctRDWRsg, ReadCached    | PrintValue, RdWrsg);
        MrcGetSetMcCh (MrcData, Controller, Channel, GsmMctRDWRdg, ReadFromCache | PrintValue, RdWrdg);
        GetSetVal = *RdWrdg + 16;
        MrcGetSetMcCh (MrcData, Controller, Channel, GsmMctRDWRsg, WriteToCache | PrintValue, &GetSetVal);
        MrcGetSetMcCh (MrcData, Controller, Channel, GsmMctRDWRdg, WriteToCache | PrintValue, &GetSetVal);
      } else {
        MrcGetSetMcCh (MrcData, Controller, Channel, GsmMctRDWRsg, WriteToCache | PrintValue, RdWrsg);
        MrcGetSetMcCh (MrcData, Controller, Channel, GsmMctRDWRdg, WriteToCache | PrintValue, RdWrdg);
      }
      MrcFlushRegisterCachedData (MrcData);
      // Must update the XARB bubble injector when TAT values change
      SetTcBubbleInjector (MrcData, Controller, Channel);
    }
  }
}

/**
  Relax WRWR same rank turnarounds on all channels.
  Used during Jedec Write Leveling to leave enough space between writes during training

  @param[in] MrcData - Include all MRC global data.
**/
void
MrcRelaxWriteToWriteSameRank (
  IN  MrcParameters *const MrcData
  )
{
  UINT32          Controller;
  UINT32          Channel;
  UINT32          tCK;
  UINT32          TwloMax;
  UINT8           WrWrsg;
  UINT8           WrWrdg;
  INT64           GetSetVal;
  INT64           tWCL;
  const MrcInput  *Inputs;
  MrcOutput       *Outputs;
  MrcChannelOut   *ChannelOut;
  MrcTiming       *Timing;
  TwloMax = 0;
  Outputs = &MrcData->Outputs;
  Inputs  = &MrcData->Inputs;

  for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
    for (Channel = 0; Channel < Outputs->MaxChannels; Channel++) {
      if ((!MrcChannelExist (MrcData, Controller, Channel)) || IS_MC_SUB_CH (Outputs->Lpddr, Channel)) {
        continue;
      }

      ChannelOut = &Outputs->Controller[Controller].Channel[Channel];
      Timing     = &ChannelOut->Timing[Inputs->MemoryProfile];
      tCK        = Timing->tCK;
      TwloMax     = DIVIDECEIL (MRC_DDR5_tWLO_MAX_FS + MRC_DDR5_tWLOE_MAX_FS, tCK);

      MrcGetSetMcCh (MrcData, Controller, Channel, GsmMctWRWRsg, ReadFromCache, &GetSetVal);
      WrWrsg = (UINT8) GetSetVal;
      MrcGetSetMcCh (MrcData, Controller, Channel, GsmMctWRWRdg, ReadFromCache, &GetSetVal);
      WrWrdg = (UINT8) GetSetVal;
      MrcGetSetMcCh (MrcData, Controller, Channel, GsmMctCWL,    ReadFromCache, &tWCL);
      // Relax WR to WR timings to be higher than tCWL + tWLO + tWLOE
      GetSetVal = MAX (tWCL + TwloMax, WrWrsg);
      MrcGetSetMcCh (MrcData, Controller, Channel, GsmMctWRWRsg, WriteToCache, &GetSetVal);
      GetSetVal = MAX (tWCL + TwloMax, WrWrdg);
      MrcGetSetMcCh (MrcData, Controller, Channel, GsmMctWRWRdg, WriteToCache, &GetSetVal);
      MrcFlushRegisterCachedData (MrcData);
      // Must update the XARB bubble injector when TAT values change
      SetTcBubbleInjector (MrcData, Controller, Channel);
    } // Channel
  } // Controller
}


/**
  Convert # of femto seconds to # of tCK

  @param[in] MrcData        - Include all MRC global data.
  @param[in] TimeInFemto    - Time to convert to tCK.

  @retval The # of tCK.
**/
UINT32
MrcFemtoTimeToTCK (
  IN  MrcParameters *const MrcData,
  IN  UINT32               TimeInFemto
  )
{
  MrcOutput        *Outputs;
  MrcFrequency     Frequency;
  UINT32           Value;
  UINT32           FrequencyInFemto;

  Outputs = &MrcData->Outputs;

  Frequency = Outputs->Frequency;

  Frequency /= 2;

  FrequencyInFemto = 1000000000 / Frequency;

  Value = DIVIDECEIL (TimeInFemto, FrequencyInFemto);

  return Value;
}

/**
  This function completes setting up the Generic MRS FSM configuration to enable SAGV during normal operation.

  @param[in] MrcData  - Pointer to global MRC data.
  @param[in] Print    - Boolean control for debug print messages.

  @retval MrcStatus - mrcSuccess, otherwise an error status.
**/
MrcStatus
MrcFinalizeMrSeq (
  IN  MrcParameters *const MrcData,
  IN  BOOLEAN              Print
  )
{
  const MRC_FUNCTION  *MrcCall;
  MrcStatus Status;
  MrcDebug  *Debug;
  MrcOutput *Outputs;
  MrcModeRegister *MrPerRank;
  MrcModeRegister SagvMrSeq[MAX_MR_GEN_FSM];
  MrcModeRegister CurMrAddr;
  MrcModeRegisterIndex MrIndex;
  MrcDebugMsgLevel DebugLevel;
  GmfTimingIndex MrDelay[MAX_MR_GEN_FSM];
  UINT16 Delay;
  UINT32 Controller;
  UINT32 Channel;
  UINT32 Rank;
  UINT32 Index;
  UINT32 MrSeqLen;
  UINT8 Data;
  LPDDR5_MODE_REGISTER_16_TYPE *Mr16Lp5;
  MRC_GEN_MRS_FSM_MR_TYPE *GenMrsFsmMr;
  MRC_GEN_MRS_FSM_MR_TYPE MrData[MAX_CONTROLLER][MAX_CHANNEL][MAX_RANK_IN_CHANNEL][MAX_MR_GEN_FSM];

  Status = mrcSuccess;
  Outputs = &MrcData->Outputs;
  Debug = &Outputs->Debug;
  MrSeqLen = ARRAY_COUNT (SagvMrSeq);
  MrcCall = MrcData->Inputs.Call.Func;
  DebugLevel = ((Print) ? MSG_LEVEL_ERROR : MSG_LEVEL_NEVER);

  if (Outputs->DdrType == MRC_DDR_TYPE_DDR5) {
    return MrcFinalizeDdr5MrSeq (MrcData, Print);
  }
  // Clear out our array
  MrcCall->MrcSetMem ((UINT8 *) MrData, sizeof (MrData), 0);

  MrPerRank = NULL;
  Status = MrcGetSagvMrSeq (MrcData, SagvMrSeq, MrDelay, &MrSeqLen, &MrPerRank);
  Mr16Lp5 = (LPDDR5_MODE_REGISTER_16_TYPE *) &Data;

  for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
    for (Channel = 0; Channel < MAX_CHANNEL; Channel++) {
      for (Rank = 0; Rank < MAX_RANK_IN_CHANNEL; Rank++) {
        if (!MrcRankExist (MrcData, Controller, Channel, Rank)) {
          continue;
        }
        for (Index = 0; Index < MrSeqLen; Index++) {
          // Get MR value from host structure and store it in the GEN_FSM array.
          CurMrAddr = SagvMrSeq[Index];
          // Translate from MrcModeRegister to Host structure index
          MrIndex = MrcMrAddrToIndex (MrcData, &CurMrAddr);
          if (MrIndex < MAX_MR_IN_DIMM) {
            GenMrsFsmMr = &MrData[Controller][Channel][Rank][Index];
            Data = (UINT8) Outputs->Controller[Controller].Channel[Channel].Dimm[dDIMM0].Rank[Rank % MAX_RANK_IN_DIMM].MR[MrIndex];
            if ((Outputs->DdrType == MRC_DDR_TYPE_LPDDR5) && (CurMrAddr == mrMR16)) {
              if ((Index == (UINT32) (MrSeqLen - 1)) || (Index == 0)) {
                Mr16Lp5->Bits.Vrcg = 0;
              } else {
                Mr16Lp5->Bits.Vrcg = 1;
              }
              if (Index == 0) {
                GenMrsFsmMr->FspWrToggle = TRUE;
              } else {
                GenMrsFsmMr->FspOpToggle = TRUE;
                if (Index != (UINT32) (MrSeqLen - 1)) {
                  GenMrsFsmMr->FreqSwitchPoint = TRUE;
                }
              }
            }
            GenMrsFsmMr->PdaMr = MrcMrIsPda (MrcData, Controller, Channel, CurMrAddr);
            GenMrsFsmMr->MrData = Data;
            GenMrsFsmMr->MrAddr = CurMrAddr;
            GenMrsFsmMr->Valid = TRUE;
            GenMrsFsmMr->DelayIndex = MrDelay[Index];
            MrcGetGmfDelayTiming (MrcData, MrDelay[Index], &Delay);
            MRC_DEBUG_MSG (Debug, DebugLevel, "MC%d.C%d.R%d MR%d: 0x%X Delay: %u Valid: %u FspWrToggle:%u FspOpToggle:%u\n",
              Controller, Channel, Rank, CurMrAddr, Data, Delay, GenMrsFsmMr->Valid, GenMrsFsmMr->FspWrToggle, GenMrsFsmMr->FspOpToggle);
          } else {
            MRC_DEBUG_MSG (Debug, DebugLevel, "MR index(%d) exceeded MR array length(%d)\n", MrIndex, MAX_MR_IN_DIMM);
            return mrcWrongInputParameter;
          }
        } // MR Index
      } // Rank
    } // Channel
  } // Controller

  if (Status == mrcSuccess) {
    // Program Generic MRS FSM Per Controller/Channel
    Status = MrcGenMrsFsmConfig (MrcData, MrData, MrPerRank);
  }

  return Status;
}

/**
  Convert the input timing value from picoseconds to nCK (number of clocks).
  Returns 0 if tCKmin is 0 to avoid divide by 0 error.

  Utilizes the DDR5 SPD INT math Rounding Algorithm with correction 0.30%
  and truncates down to the next lower integer value.
  It is based on DDR5 SPD spec since Release 0.9 Beta Level 26, and
  XMP3.0 Revision 1.1.

    @param[in] TimingInPs - Timing value in units of picoseconds.
    @param[in] tCKmin     - The memory clock period in units of femtoseconds.

    @return The input timing value in units of nCK
**/
UINT32
PicoSecondsToClocks (
  IN UINT32 TimingInPs,
  IN UINT32 tCKmin
  )
{
  UINT32 Clocks;
  UINT32 MathTemp;
  UINT32 tCKps;

  // Convert tCK from Femtoseconds to Picoseconds
  tCKps = DIVIDEROUND (tCKmin, 1000);

  if (tCKps == 0) {
    Clocks = 0;
  } else {
    // DDR5 SPD spec INT math Rounding Algorithm
    MathTemp = (TimingInPs * 997) / tCKps;
    MathTemp = MathTemp + 1000;
    Clocks = MathTemp / 1000;
  }

  return Clocks;
}

/**
  Find the first rank in each channel currently enabled

  @param[in] MrcData       - The MRC global data.
  @param[out] FirstRank    - The index of the first rank in each channel

  @return None
**/
VOID
GetFirstRank (
  IN  MrcParameters *const MrcData,
  OUT UINT8         FirstRank[MAX_CONTROLLER][MAX_CHANNEL]
  )
{
  MrcOutput           *Outputs;
  MrcControllerOut    *ControllerOut;
  UINT32 Controller;
  UINT32 Channel;
  UINT8  Rank;
  UINT8  MaxChannels;

  Outputs     = &MrcData->Outputs;
  MaxChannels = Outputs->MaxChannels;

  for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
    ControllerOut = &Outputs->Controller[Controller];
    for (Channel = 0; Channel < MaxChannels; Channel++) {
      for (Rank = 0; Rank < MAX_RANK_IN_CHANNEL; Rank++) {
        if ((1 << Rank) & ControllerOut->Channel[Channel].ValidRankBitMask) {  // Pick one rank to get the results from
          FirstRank[Controller][Channel] = Rank;
          break;
        }
      } // Rank
    } // Channel
  } // Controller
}

/**
  Calculate the first byte associated with the input MC Channel index

  @param[in] MrcData  - Pointer to global MRC data.
  @param[in] Channel  - Index for a channel within an MC

  @return The first byte associated with the input MC Channel
**/
UINT8
MrcFirstByte (
  IN  MrcParameters *const MrcData,
  IN  UINT32               Channel
  )
{
  MrcOutput         *Outputs;
  BOOLEAN           Lpddr;
  BOOLEAN           Ddr5;
  UINT8             FirstByte;

  Outputs        = &MrcData->Outputs;
  Lpddr          = Outputs->Lpddr;
  Ddr5           = (Outputs->DdrType == MRC_DDR_TYPE_DDR5);

  if (Lpddr) {
    FirstByte = (UINT8) (MAX_BYTE_IN_LP_CHANNEL * Channel);
  } else if (Ddr5) {
    FirstByte = (UINT8) ((Outputs->EccSupport ? MAX_BYTE_IN_DDR5_CHANNEL : MAX_BYTE_IN_DDR5_CHANNEL - 1) * Channel);
  } else {
    FirstByte = 0; // By default if Channel is present then first byte will be present
  }

  return FirstByte;
}

/**
  Get the max CMD Groups per channel associated with the current memory technology

  @param[in] MrcData  - Pointer to global MRC data.

  @return The maximum number of CMD Groups per channel for the current memory technology
**/
UINT8
MrcGetCmdGroupMax (
  MrcParameters *const MrcData
  )
{
  MrcOutput           *Outputs;
  BOOLEAN             Ddr4;
  BOOLEAN             Ddr5;
  UINT8               CmdGroupMax;

  Outputs       = &MrcData->Outputs;
  Ddr4          = (Outputs->DdrType == MRC_DDR_TYPE_DDR4);
  Ddr5          = (Outputs->DdrType == MRC_DDR_TYPE_DDR5);
  CmdGroupMax   = (Ddr4) ? MRC_DDR4_CMD_GRP_MAX : (Ddr5) ? MRC_DDR5_CMD_GRP_MAX: 1;

  return CmdGroupMax;
}


/**
  Get all the data for all Ranks all Devs for specific controller, channel and MrAddress
  it calls to the specific function according to technology

  Important:
  The *NumMrData can different depending on the MR

  @param[in] MrcData      - Pointer to global MRC data.
  @param[in] Controller   - Controller to work on.
  @param[in] Channel      - channel to work on.
  @param[in] MrAddress    - MR Address
  @param[in] MrPdaData    - Array of Data that will be filled
  @param[in] NumMrData    - pointer that will contain # of data that filled in the array.

  @retval MrcStatus - mrcSuccess, otherwise an error status.
**/
MrcStatus
MrFillPdaMrsData (
  IN      MrcParameters   *MrcData,
  IN      UINT32          Controller,
  IN      UINT32          Channel,
  IN      MrcModeRegister MrAddress,
  IN OUT  UINT8           MrPdaData[MAX_PDA_MR_IN_CHANNEL],
  IN OUT  UINT8           *NumMrData
  )
{
  MrcStatus Status;

  Status = mrcUnsupportedTechnology;

  switch (MrcData->Outputs.DdrType) {
    case MRC_DDR_TYPE_LPDDR5:
      //Status = MrcFillPdaMrLpddr5 (MrcData, MrAddress, GenMrsFsmMr);
      break;
    case MRC_DDR_TYPE_DDR5:
      Status = MrFillPdaMrsDataDdr5 (MrcData, Controller, Channel, MrAddress, MrPdaData,NumMrData);
      break;
    default:
      break;
  }

  return Status;
}

/**
  This function checks if a specific MR is treated as PDA according to the technology.

  @param[in] MrcData           - Pointer to MRC global data.
  @param[in] Controller        - Controller to work on.
  @param[in] Channel           - channel to work on.
  @param[in] MrAddress         - MR Address to check

  @retval TRUE if the specific MR is PDA otherwise FALSE
**/
BOOLEAN
MrcMrIsPda (
  IN   MrcParameters     *MrcData,
  IN   UINT32            Controller,
  IN   UINT32            Channel,
  IN   MrcModeRegister   MrAddress
  )
{
  MrcOutput       *Outputs;
  MrcChannelOut   *ChannelOut;
  BOOLEAN         Ddr5;

  Outputs    = &MrcData->Outputs;
  ChannelOut = &Outputs->Controller[Controller].Channel[Channel];
  Ddr5       = (Outputs->DdrType == MRC_DDR_TYPE_DDR5);

  // check if global PDA is enabled
  if (!MrcData->Inputs.EnablePda) {
    return FALSE;
  }

  switch (MrAddress) {
    case mrMR3:
      if (Ddr5 && ChannelOut->Mr3PdaEnabled) {
        return TRUE;
      }
      break;
    case mrMR10:
      if (Ddr5 && ChannelOut->Mr10PdaEnabled) {
        return TRUE;
      }
      break;
    case mrMR11:
      if (Ddr5 && ChannelOut->Mr11PdaEnabled) {
        return TRUE;
      }
      break;
#if 0
     case mrMR43:
      if (Ddr5&& ChannelOut->Mr43PdaEnabled) {
        return TRUE;
      }
      break;
    case mrMR44:
      if (Ddr5&& ChannelOut->Mr44PdaEnabled) {
        return TRUE;
      }
      break;
#endif //if 0
    default:
      break;
  }

  return FALSE;
}

/**
  Issue Dqs (Byte) and Dq (Bit) swizzle training algorithms

  @param[in] MrcData - The MRC global data.

  @retval mrcSuccess if succeeded
**/
MrcStatus
MapDqDqsSwizzle (
  IN MrcParameters *const MrcData
)
{
  MrcDebug            *Debug;
  MrcOutput           *Outputs;
  MrcStatus           Status;
  UINT8               McChBitMask;
  INT64               GetSetDis;
  INT64               GetSetEn;
  INT64               ForceRxAmpOnSave;
  BOOLEAN             Lpddr;
  UINT32              FirstController;
  UINT32              FirstChannel;
  UINT8               LoopCount;
  UINT8               FirstRank[MAX_CONTROLLER][MAX_CHANNEL];
  MC_MPR_CONFIG_SAVE  SaveData;

  Outputs = &MrcData->Outputs;
  McChBitMask = Outputs->McChBitMask;

  Debug = &Outputs->Debug;
  Status = mrcSuccess;
  Lpddr = Outputs->Lpddr;
  GetSetDis = 0;
  GetSetEn = 1;
  LoopCount = 10;
  FirstController = Outputs->FirstPopController;
  FirstChannel    = Outputs->Controller[FirstController].FirstPopCh;

  GetFirstRank (MrcData, FirstRank);

  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\n -- DQ and DQS Swizzle Training -- \n");

  SetupMcMprConfig (MrcData, &SaveData, MRC_ENABLE);

  MrcGetSetChStrb (MrcData, FirstController, FirstChannel, 0, GsmIocForceRxAmpOn, ReadNoCache, &ForceRxAmpOnSave);
  MrcGetSetChStrb (MrcData, MAX_CONTROLLER, MAX_CHANNEL, MAX_SDRAM_IN_DIMM, GsmIocForceRxAmpOn, WriteToCache, &GetSetEn);
  MrcGetSetChStrb (MrcData, MAX_CONTROLLER, MAX_CHANNEL, MAX_SDRAM_IN_DIMM, GsmIocForceOdtOn, WriteToCache, &GetSetEn);
  MrcFlushRegisterCachedData (MrcData);
  MrcGetSetChStrb (MrcData, MAX_CONTROLLER, MAX_CHANNEL, MAX_SDRAM_IN_DIMM, GsmIocCaTrainingMode, WriteNoCache, &GetSetEn);

  SetupIOTestMPR (MrcData, McChBitMask, LoopCount, NSOE, 0, 0);

  MrcSetLoopcount (MrcData, 0x80000000);

  if (Lpddr) {
    Status = LpByteSwizzleTraining (MrcData);
  } else { //Ddr5
    Status = Ddr5ByteSwizzleTraining (MrcData);
  }

  Status = BitSwizzleTriaining (MrcData);
  if (Status == mrcSuccess) {
    Outputs->BitByteSwizzleFound = TRUE;
    if (!Lpddr) {
      // Reassign PDA Enumeration with correct x16 byte mapping for DDR5
      Status = MrcPdaEnumeration (MrcData);
    }
  }

  // Restore DDR IO values that were used for static feedback on DQ
  MrcGetSetChStrb (MrcData, MAX_CONTROLLER, MAX_CHANNEL, MAX_SDRAM_IN_DIMM, GsmIocForceRxAmpOn, WriteToCache, &ForceRxAmpOnSave);
  MrcGetSetChStrb (MrcData, MAX_CONTROLLER, MAX_CHANNEL, MAX_SDRAM_IN_DIMM, GsmIocForceOdtOn, WriteToCache, &GetSetDis);
  MrcFlushRegisterCachedData (MrcData);
  MrcGetSetChStrb (MrcData, MAX_CONTROLLER, MAX_CHANNEL, MAX_SDRAM_IN_DIMM, GsmIocCaTrainingMode, WriteNoCache, &GetSetDis);

  // Clean up after Test.
  SetupMcMprConfig (MrcData, &SaveData, MRC_DISABLE);
  IoReset (MrcData);

  PrintDqDqsTable (MrcData);

  return Status;
}

/**
  DDR5 x16 DQS (Byte) Swizzle Training

  Iterate through DRAM bytes using PDA and CPGC to identify the CPU to DRAM DQS Mapping
  This training should only be used for DDR5 x16 devices (x8 does not need additional swizzle training)

  The following is a breakdown of the algorithm code flow:
  1. Initialze DRAM DQ patterns (MR26 & MR27) to 0x0, and MR25 and MR30 to 0x0
  2. While each CpuByte is not accounted for iterate through each device
    a. PDA Select for the current device
    b. Initialze DRAM DQ invert patterns (MR28 & MR29) to 0x01 & 0xFE
    c. Issue CPGC RDRD test and read DatatrainFeedback for every Byte
    d. If number of 0's in Datatrainfeedback equals:
        0 : Byte not used
        1 : Indicates an lower byte
        7 : Indicates an upper byte
    e. Map out ChannelIn->DqsMapCpu2Dram with byte knowledge found above

  @param[in] MrcData - The MRC global data.

  @retval mrcSuccess if succeeded
**/
MrcStatus
Ddr5ByteSwizzleTraining (
  IN OUT MrcParameters *const   MrcData
  )
{
  const MRC_FUNCTION  *MrcCall;
  MrcInput            *Inputs;
  MrcDebug            *Debug;
  MrcOutput           *Outputs;
  MrcChannelOut       *ChannelOut;
  MrcDimmOut          *DimmOut;
  MrcControllerIn     *ControllerIn;
  MrcChannelIn        *ChannelIn;
  MrcStatus           Status;
  UINT32              Offset;
  UINT8               MaxChannel;
  UINT8               Controller;
  UINT8               Channel;
  UINT8               Rank;
  UINT8               Byte;
  UINT8               LoopCount;
  UINT8               McChBitMask;
  INT64               GetSetDis;
  INT64               GetSetEn;
  UINT8               MrDqCalPatA;
  UINT8               MrDqCalPatB;
  UINT8               MrDqInvertLow;
  UINT8               MrDqInvertHigh;
  UINT8               Feedback[MAX_BYTE_IN_DDR5_CHANNEL];
  INT64               DataTrainFeedbackField;
  MC0_REQ0_CR_CPGC_SEQ_CTL_STRUCT         CpgcSeqCtl;
  MC0_REQ0_CR_CPGC_SEQ_STATUS_STRUCT      CpgcSeqStatus;
  MC0_STALL_DRAIN_STRUCT                  StallDrain;
  UINT8               CurMcChBitMask;
  UINT8               CpuByteDoneMask;
  UINT8               DevId;
  UINT8               CpuByteDone;
  UINT8               ByteMax;
  UINT8               DramByte;
  UINT8               DimmIdx;
  BOOLEAN             ByteUsed;
  BOOLEAN             Ddr5x8Device;
  BOOLEAN             EccSupport;

  Inputs  = &MrcData->Inputs;
  Outputs = &MrcData->Outputs;
  Debug   = &Outputs->Debug;
  MrcCall = Inputs->Call.Func;
  MaxChannel  = Outputs->MaxChannels;
  McChBitMask = Outputs->McChBitMask;
  EccSupport  = Outputs->EccSupport;
  Status      = mrcSuccess;
  GetSetDis = 0;
  GetSetEn  = 1;
  LoopCount = 10;
  DimmOut   = 0;
  MrDqCalPatA = 26;
  MrDqCalPatB = 27;
  MrDqInvertLow  = 28;
  MrDqInvertHigh = 29;
  Ddr5x8Device   = FALSE;
  MrcCall->MrcSetMem ((UINT8 *) Feedback, sizeof (Feedback), 0);

  CpuByteDoneMask = EccSupport ? 0x1F : 0xF;
  ByteMax         = EccSupport ? 5 : 4;

  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\n -- DDR5 Byte Swizzle -- \n");

  SetupIOTestMPR (MrcData, McChBitMask, LoopCount, NSOE, 0, 0);
  MrcSetLoopcount (MrcData, 0x80000000);

  for (Rank = 0; Rank < MAX_RANK_IN_CHANNEL; Rank += MAX_RANK_IN_DIMM) {
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\n -- Rank %d -- \n", Rank);
    DimmIdx      = RANK_TO_DIMM_NUMBER (Rank);
    McChBitMask = 0;

    for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
      ControllerIn = &Inputs->Controller[Controller];
      for (Channel = 0; Channel < MaxChannel; Channel++) {
        ChannelIn  = &ControllerIn->Channel[Channel];
        ChannelOut = &Outputs->Controller[Controller].Channel[Channel];
        if (!(MrcRankExist (MrcData, Controller, Channel, Rank))) {
          continue;
        }
        DimmOut      = &ChannelOut->Dimm[DimmIdx];
        Ddr5x8Device = (DimmOut->SdramWidth == 8);
        // Set 1 to 1 mapping for x8 devices
        if (Ddr5x8Device) { // x8 Device
          for (Byte = 0; Byte < Outputs->SdramCount; Byte++) {
            if (!MrcByteExist (MrcData, Controller, Channel, Byte)) {
              continue;
            }
            ChannelIn->DqsMapCpu2Dram[DimmIdx][Byte] = Byte;
          }
          MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Mc%d.Ch%d: x8 Device does not need byte swizzle training, skipping for this Channel\n", Controller, Channel);
        } else { // x16 Device
          McChBitMask |= SelectReutRanks (MrcData, Controller, Channel, 1 << Rank, FALSE, 0); //Save particular mask to use with runIOTest later

          // Initial setup of DRAM
          MrcIssueMrw (MrcData, Controller, Channel, Rank, MrDqCalPatA, 0x0, TRUE); // DQ Calibration Pattern "A"
          MrcIssueMrw (MrcData, Controller, Channel, Rank, MrDqCalPatB, 0x0, TRUE); // DQ Calibration Pattern "B"
          MrcIssueMrw (MrcData, Controller, Channel, Rank, mrMR25, 0x00, TRUE); // Read Training Mode Settings
          MrcIssueMrw (MrcData, Controller, Channel, Rank, mrMR30, 0x00, TRUE); // LFSR Assignment(0-LFSR0, 1-LFSR1)
        }
      } // for Channel
    } // for Controller
    if (McChBitMask == 0) {
      continue;
    }

    for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
      for (Channel = 0; Channel < MaxChannel; Channel++) {
        if (!(MrcRankExist (MrcData, Controller, Channel, Rank)) ||
            (MC_CH_MASK_CHECK (McChBitMask, Controller, Channel, MaxChannel) == 0)) {
          continue;
        }
        ControllerIn = &Inputs->Controller[Controller];
        ChannelIn = &ControllerIn->Channel[Channel];
        DevId = 0;
        CpuByteDone = 0;
        DramByte = 0;

        while (CpuByteDone != CpuByteDoneMask) {
          // PDA Select for current Device
          MrcPdaSelect (MrcData, Controller, Channel, Rank, DevId, MRC_PRINTS_ON);
          // Set Dq Inversion Patterns
          MrcIssueMrw (MrcData, Controller, Channel, Rank, MrDqInvertLow, 0x01, FALSE); // Lower Byte Invert for DQ Calibration
          MrcIssueMrw (MrcData, Controller, Channel, Rank, MrDqInvertHigh, 0xFE, FALSE); // Upper Byte Invert for DQ Calibration

          // Configure MC to issue MPC[Read DQ Calibration] instead of reads
          MrcGetSetMcCh (MrcData, Controller, Channel, GsmMccMprTrainDdrOn, WriteNoCache, &GetSetEn);

          // Start CPGC and run for 1uS
          CpgcSeqCtl.Data = 0;
          CpgcSeqCtl.Bits.START_TEST = 1;
          Cpgc20ControlRegWrite (MrcData, McChBitMask, CpgcSeqCtl);

          // Wait to obtain results in DataTrainFeedback
          MrcWait (MrcData, 200 * MRC_TIMER_1NS);

          for (Byte = 0; Byte < ByteMax; Byte++) {
            if (!MrcByteExist (MrcData, Controller, Channel, Byte)) {
              MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "  ");
              continue;
            }
            MrcGetSetChStrb (MrcData, Controller, Channel, Byte, GsmIocDataTrainFeedback, ReadUncached, &DataTrainFeedbackField);
            Feedback[Byte] = (UINT8)(DataTrainFeedbackField & 0xFF);
            MRC_DEBUG_MSG (
              Debug,
              MSG_LEVEL_NOTE,
              "\t   Controller %d Channel %d CPU Byte %d  => Feedback = %02X - %s feedback\n",
              Controller,
              Channel,
              Byte,
              Feedback[Byte],
              (MrcCountBitsEqOne (Feedback[Byte]) == 1) ? "Low" : (MrcCountBitsEqOne (Feedback[Byte]) == 7) ? "High" : "No"
            );
          } // Byte

          // Stop CPGC
          CpgcSeqCtl.Data = 0;
          CpgcSeqStatus.Data = 0;
          CpgcSeqCtl.Bits.STOP_TEST = 1;
          Cpgc20ControlRegWrite (MrcData, McChBitMask, CpgcSeqCtl);

          // Wait till CPGC test is done on all participating channels
          // Wait until Channel test done status matches ChBitMask
          do {
            Offset = OFFSET_CALC_CH (MC0_STALL_DRAIN_REG, MC1_STALL_DRAIN_REG, Controller);
            StallDrain.Data = MrcReadCR (MrcData, Offset);
              CurMcChBitMask = (1 << ((Controller * MaxChannel) + Channel));
              if ((McChBitMask & CurMcChBitMask) != 0) {
                Offset = OFFSET_CALC_MC_CH (MC0_REQ0_CR_CPGC_SEQ_STATUS_REG, MC1_REQ0_CR_CPGC_SEQ_STATUS_REG, Controller, MC0_REQ1_CR_CPGC_SEQ_STATUS_REG, Channel);
                CpgcSeqStatus.Data = MrcReadCR (MrcData, Offset);
              }
          } while (((CpgcSeqStatus.Bits.TEST_DONE) && (StallDrain.Bits.mc_drained)) != 1);

          // Exit MPR mode
          MrcGetSetMcCh (MrcData, Controller, Channel, GsmMccMprTrainDdrOn, WriteNoCache, &GetSetDis);

          // Reset Dq Inversion patterns
          MrcIssueMrw (MrcData, Controller, Channel, Rank, MrDqInvertLow, 0x0, FALSE); // Lower Byte Invert for DQ Calibration
          MrcIssueMrw (MrcData, Controller, Channel, Rank, MrDqInvertHigh, 0x0, FALSE); // Upper Byte Invert for DQ Calibration

          // Exit PDA Select for single Device
          MrcPdaSelect (MrcData, Controller, Channel, Rank, 15, MRC_PRINTS_ON);

          ByteUsed = FALSE;
          for (Byte = 0; Byte < ByteMax; Byte++) {
            if (!MrcByteExist (MrcData, Controller, Channel, Byte)) {
              MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "  ");
              continue;
            }
            if (MrcCountBitsEqOne (Feedback[Byte]) != 0) {
              ByteUsed = TRUE;
              break;
            } else {
              // Byte not used
              continue;
            }
          } // Byte

          if (ByteUsed == TRUE) {
            // If the number of ones is 1, this is the DRAM lower byte
            // If the number of ones is 7, this is the DRAM upper byte
            for (Byte = 0; Byte < ByteMax; Byte++) {
              if (!MrcByteExist (MrcData, Controller, Channel, Byte)) {
                MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "  ");
                continue;
              }
              if (MrcCountBitsEqOne (Feedback[Byte]) == 1) { // Lower Byte
                  ChannelIn->DqsMapCpu2Dram[DimmIdx][Byte] = DramByte;
                  CpuByteDone |= (1 << Byte);

              } else if (MrcCountBitsEqOne (Feedback[Byte]) == 7) { //upper Byte
                  ChannelIn->DqsMapCpu2Dram[DimmIdx][Byte] = (DramByte + 1);
                  CpuByteDone |= (1 << Byte);
              }
            }
            DramByte = DramByte + 2;
          } // ByteUsed == TRUE;

          // Check to see if Byte Mapping is unsuccessful
          if ((DevId == ByteMax) && (CpuByteDone != CpuByteDoneMask)) {
             // ERROR: Byte Mapping did not complete successfully since not all byte
             // showed correct feedback
            MRC_DEBUG_MSG (
              Debug,
              MSG_LEVEL_ERROR,
              "\nERROR! Controller %d Channel %d - Unsuccessful mapping of bytes \n",
              Controller,
              Channel
            );
            Status = mrcFail;
            break;
          }
          DevId++;
        } // while (CpuByteDone != CpuByteDoneMask)
      } // Channel
    } //Controller
  } // Rank

  return Status;
}

/**
  LP DQS (Byte) Swizzle Training

  Iterate through DRAM bytes and use CPGC to identify the CPU to DRAM DQS Mapping
  This training should only be used for LP4 and LP5

  The following is a breakdown of the algorithm code flow:
  1. Initialze DRAM DQ patterns to 0x0
  2. Initialze DRAM DQ invert patterns to 0x01 & 0xFE
  3. Issue CPGC RDRD test and read DatatrainFeedback for every Byte
  4. If number of 0's in Datatrainfeedback equals:
        1 : Indicates lower byte
        7 : Indicates upper byte
  5. Map out ChannelIn->DqsMapCpu2Dram with byte knowledge found above

  @param[in] MrcData - The MRC global data.

  @retval mrcSuccess if succeeded
**/
MrcStatus
LpByteSwizzleTraining (
  IN MrcParameters *const MrcData
  )
{
  MrcInput          *Inputs;
  MrcDebug          *Debug;
  MrcOutput         *Outputs;
  const MRC_FUNCTION  *MrcCall;
  MrcControllerIn *ControllerIn;
  MrcChannelIn    *ChannelIn;
  MrcChannelOut   *ChannelOut;
  MrcStatus        Status;
  UINT32          Offset;
  UINT8           MaxChannel;
  UINT8           Controller;
  UINT8           Channel;
  UINT8           Rank;
  UINT8           Byte;
  UINT8           McChBitMask;
  INT64           GetSetDis;
  INT64           GetSetEn;
  BOOLEAN         Lpddr;
  BOOLEAN         Lpddr4;
  UINT8           MrDqCalPatA;
  UINT8           MrDqCalPatB;
  UINT8           MrDqInvertLow;
  UINT8           MrDqInvertHigh;
  UINT8           TestDoneStatus;
  UINT8           Feedback[8];
  INT64           DataTrainFeedbackField;
  UINT8           IpChannel;
  MC0_REQ0_CR_CPGC_SEQ_CTL_STRUCT      CpgcSeqCtl;
  MC0_REQ0_CR_CPGC_SEQ_STATUS_STRUCT   CpgcSeqStatus;
  MC0_STALL_DRAIN_STRUCT               StallDrain;
  UINT8            CurMcChBitMask;
  UINT8            FirstRank[MAX_CONTROLLER][MAX_CHANNEL];
  BOOLEAN          ByteUsed;
  UINT8            DramByte;

  Inputs = &MrcData->Inputs;
  Outputs = &MrcData->Outputs;
  MrcCall = Inputs->Call.Func;
  MaxChannel = Outputs->MaxChannels;
  McChBitMask = Outputs->McChBitMask;
  Debug = &Outputs->Debug;
  Status = mrcSuccess;
  GetSetDis = 0;
  GetSetEn = 1;
  Lpddr4 = (Outputs->DdrType == MRC_DDR_TYPE_LPDDR4);
  Lpddr = Outputs->Lpddr;
  DramByte = 0;
  McChBitMask = 0;

  MrcCall->MrcSetMem ((UINT8 *) Feedback, sizeof (Feedback), 0);
  if (Lpddr4) {
    MrDqCalPatA     = 32;
    MrDqCalPatB     = 40;
    MrDqInvertLow   = 15;
    MrDqInvertHigh  = 20;
  } else {  // LPDDR5
    MrDqCalPatA     = 33;
    MrDqCalPatB     = 34;
    MrDqInvertLow   = 31;
    MrDqInvertHigh  = 32;
  }

  GetFirstRank (MrcData, FirstRank);

  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\n -- LP Byte Swizzle -- \n");

  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Start DQS (Byte) swizzle training \n");

  for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
    for (Channel = 0; Channel < MaxChannel; Channel++) {
      Rank = FirstRank[Controller][Channel];
      McChBitMask |= SelectReutRanks (MrcData, Controller, Channel, 1 << Rank, FALSE, 0); //Save particular mask to use with runIOTest later
      if (!(MrcRankExist (MrcData, Controller, Channel, Rank))) {
        continue;
      }
      ControllerIn = &Inputs->Controller[Controller];
      ChannelOut = &Outputs->Controller[Controller].Channel[Channel];

      // Initialize DQS Mapping Structure
      MrcCall->MrcSetMem (
        (UINT8 *)(ControllerIn->Channel[Channel].DqsMapCpu2Dram),
        sizeof (ControllerIn->Channel[0].DqsMapCpu2Dram),
        0xFF
      );

      // Initial setup of DRAM
      MrcIssueMrw (MrcData, Controller, Channel, Rank, MrDqCalPatA, 0x00, TRUE); // DQ Calibration Pattern "A"
      MrcIssueMrw (MrcData, Controller, Channel, Rank, MrDqCalPatB, 0x00, TRUE); // DQ Calibration Pattern "B"
      MrcIssueMrw (MrcData, Controller, Channel, Rank, MrDqInvertLow, 0x01, FALSE); // Lower Byte Invert for DQ Calibration
      MrcIssueMrw (MrcData, Controller, Channel, Rank, MrDqInvertHigh, 0xFE, FALSE); // Upper Byte Invert for DQ Calibration
    }
  }

  // Configure MC to issue MPC[Read DQ Calibration] instead of reads
  MrcGetSetMcCh (MrcData, MAX_CONTROLLER, MAX_CHANNEL, GsmMccMprTrainDdrOn, WriteNoCache, &GetSetEn);

  // Start CPGC and run for 1uS
  CpgcSeqCtl.Data = 0;
  CpgcSeqCtl.Bits.START_TEST = 1;
  Cpgc20ControlRegWrite (MrcData, McChBitMask, CpgcSeqCtl);

  // Wait to obtain results in DataTrainFeedback
  MrcWait (MrcData, 200 * MRC_TIMER_1NS);

  // Get Results for all ch/bytes
  // Update results for all ch/bytes
  for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
    for (Channel = 0; Channel < MaxChannel; Channel++) {
      if (!MrcChannelExist (MrcData, Controller, Channel)) {
        continue;
      }
      ChannelIn = &Inputs->Controller[Controller].Channel[Channel];
      for (Byte = 0; Byte < Outputs->SdramCount; Byte++) {
        if (!MrcByteExist (MrcData, Controller, Channel, Byte)) {
          MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "  ");
          continue;
        }
        MrcGetSetChStrb (MrcData, Controller, Channel, Byte, GsmIocDataTrainFeedback, ReadUncached, &DataTrainFeedbackField);
        Feedback[Byte] = (UINT8)(DataTrainFeedbackField & 0xFF);
        MRC_DEBUG_MSG (
            Debug,
          MSG_LEVEL_NOTE,
          "\t   Controller %d Channel %d CPU Byte %d  => Feedback = %02X - %s feedback\n",
          Controller,
          Channel,
          Byte,
          Feedback[Byte],
          (MrcCountBitsEqOne (Feedback[Byte]) == 1) ? "Low" : (MrcCountBitsEqOne (Feedback[Byte]) == 7) ? "High" : "No"
        );
      } // Byte

      ByteUsed = FALSE;
      DramByte = 0;
      for (Byte = 0; Byte < Outputs->SdramCount; Byte++) {
        if (!MrcByteExist (MrcData, Controller, Channel, Byte)) {
          MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "  ");
          continue;
        }
        if (MrcCountBitsEqOne (Feedback[Byte]) != 0) {
          ByteUsed = TRUE;
          break;
        } else {
          // ERROR: Byte should always be used
          MRC_DEBUG_MSG (
            Debug,
            MSG_LEVEL_ERROR,
            "\nERROR! Controller %d Channel %d Byte %d - Not showing any feedback \n",
            Controller,
            Channel,
            Byte
          );
          Status = mrcFail;
        }
      } // Byte

      if (ByteUsed == FALSE) {
        MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, " Zero Count is 0 \n");
      } else {
        // If the number of ones is 1, this is the DRAM lower byte
        // If the number of ones is 7, this is the DRAM upper byte
        for (Byte = 0; Byte < Outputs->SdramCount; Byte++) {
          if (!MrcByteExist (MrcData, Controller, Channel, Byte)) {
            MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "  ");
            continue;
          }
          if (MrcCountBitsEqOne (Feedback[Byte]) == 1) {              // Lower Byte
            ChannelIn->DqsMapCpu2Dram[dDIMM0][Byte] = DramByte;       // Lpddr only uses DIMM0 entry
          } else if (MrcCountBitsEqOne (Feedback[Byte]) == 7) {       // Upper Byte
            ChannelIn->DqsMapCpu2Dram[dDIMM0][Byte] = (DramByte + 1); // Lpddr only uses DIMM0 entry
          }
        }
      } // ByteUsed == TRUE;

    } // Channel
  } // Controller

  // Stop CPGC
  CpgcSeqCtl.Data = 0;
  CpgcSeqCtl.Bits.STOP_TEST = 1;
  Cpgc20ControlRegWrite (MrcData, McChBitMask, CpgcSeqCtl);

  // Wait till CPGC test is done on all participating channels
  // Wait until Channel test done status matches ChBitMask
  do {
    TestDoneStatus = 0;
    for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
      if (MrcControllerExist(MrcData, Controller)) {
        Offset = OFFSET_CALC_CH (MC0_STALL_DRAIN_REG, MC1_STALL_DRAIN_REG, Controller);
        StallDrain.Data = MrcReadCR (MrcData, Offset);
        for (Channel = 0; Channel < MaxChannel; Channel++) {
          CurMcChBitMask = (1 << ((Controller * MaxChannel) + Channel));
          if (((McChBitMask & CurMcChBitMask) != 0) && (!(IS_MC_SUB_CH (Lpddr, Channel)))) {
            IpChannel = LP_IP_CH (Lpddr, Channel);
            ChannelOut = &Outputs->Controller[Controller].Channel[Channel];
            Offset = OFFSET_CALC_MC_CH (MC0_REQ0_CR_CPGC_SEQ_STATUS_REG, MC1_REQ0_CR_CPGC_SEQ_STATUS_REG, Controller, MC0_REQ1_CR_CPGC_SEQ_STATUS_REG, IpChannel);
            CpgcSeqStatus.Data = MrcReadCR (MrcData, Offset);
            if ((CpgcSeqStatus.Bits.TEST_DONE) && (StallDrain.Bits.mc_drained)) {
              TestDoneStatus |= (ChannelOut->CpgcChAssign << (Controller * MaxChannel));
            }
          }
        }
      }
    }
  } while ((TestDoneStatus & McChBitMask) != McChBitMask);

  MrcGetSetMcCh (MrcData, MAX_CONTROLLER, MAX_CHANNEL, GsmMccMprTrainDdrOn, WriteNoCache, &GetSetDis);

  return Status;
}

/**
  Map CPU DQ Pins to DRAM DQ Pins for bit swizzling
  This training should only be used for DDR5, LP4, and LP5

  Main flow:
  Repeat for each of the 8 bits per DQ byte (total 8 iterations for both channels, for rank0 only):
  - Put DRAM into MPC RD DQ Calibration mode.
  - For Lp4, set MR32 and MR40 to 0, and set the current DQ bit in both MR15 and MR20.
  - For Ddr5, set MR26 and MR27 to 0, and set the current DQ bit in both MR28 and MR29.
  This will result in a pattern of 0's on all but one bits per byte.
  - Issue a sequence of READ commands.
  - Locate the single DQ in each byte in DataTrainFeedback (using SenseAmp Mode).
  - Report error if more than one DQ pin toggles.
  - Report error if no active DQ pin found.
  Update the DQ mapping data structure.

  @param[in] MrcData - The MRC global data.

  @retval mrcSuccess if succeeded
  @param[in] MrcData - The MRC global data.

  @retval mrcSuccess if succeeded
**/
MrcStatus
BitSwizzleTriaining (
  IN OUT MrcParameters *const   MrcData
  )
{
  MrcInput            *Inputs;
  MrcDebug            *Debug;
  const MRC_FUNCTION  *MrcCall;
  MrcOutput           *Outputs;
  MrcChannelOut       *ChannelOut;
  MrcControllerIn     *ControllerIn;
  MrcChannelIn        *ChannelIn;
  MrcStatus           Status;
  UINT32              Offset;
  UINT8               MaxChannel;
  UINT8               Controller;
  UINT8               Channel;
  UINT8               Rank;
  UINT8               Byte;
  UINT8               Dimm;
  UINT8               McChBitMask;
  INT64               GetSetDis;
  MrcStatus           StatusLocal;
  INT64               GetSetEn;
  BOOLEAN             Lpddr4;
  BOOLEAN             Lpddr5;
  BOOLEAN             Lpddr;
  BOOLEAN             Ddr5;
  UINT8               MrDqCalPatA;
  UINT8               MrDqCalPatB;
  UINT8               MrDqInvertLow;
  UINT8               MrDqInvertHigh;
  UINT8               Bit;
  UINT8               TestDoneStatus;
  UINT8               Feedback[8];
  UINT32              FeedbackMask;
  UINT32              InvertData;
  INT64               DataTrainFeedbackField;
  UINT8               IpChannel;
  MC0_REQ0_CR_CPGC_SEQ_CTL_STRUCT         CpgcSeqCtl;
  MC0_REQ0_CR_CPGC_SEQ_STATUS_STRUCT      CpgcSeqStatus;
  MC0_STALL_DRAIN_STRUCT                  StallDrain;
  UINT8               CurMcChBitMask;
  UINT8               FirstRank[MAX_CONTROLLER][MAX_CHANNEL];
  UINT8               DramByte;

  Inputs      = &MrcData->Inputs;
  MrcCall     = Inputs->Call.Func;
  Outputs     = &MrcData->Outputs;
  MaxChannel  = Outputs->MaxChannels;
  McChBitMask = Outputs->McChBitMask;
  Debug       = &Outputs->Debug;
  Status      = mrcSuccess;
  Lpddr4      = (Outputs->DdrType == MRC_DDR_TYPE_LPDDR4);
  Lpddr5      = (Outputs->DdrType == MRC_DDR_TYPE_LPDDR5);
  Ddr5        = (Outputs->DdrType == MRC_DDR_TYPE_DDR5);
  Lpddr       = Outputs->Lpddr;
  GetSetDis   = 0;
  GetSetEn    = 1;
  ChannelIn   = 0;

  if (Lpddr4) {
    MrDqCalPatA = 32;
    MrDqCalPatB = 40;
    MrDqInvertLow  = 15;
    MrDqInvertHigh = 20;
  } else if (Lpddr5) {
    MrDqCalPatA = 33;
    MrDqCalPatB = 34;
    MrDqInvertLow = 31;
    MrDqInvertHigh = 32;
  } else { // Ddr5
    MrDqCalPatA = 26;
    MrDqCalPatB = 27;
    MrDqInvertLow  = 28;
    MrDqInvertHigh = 29;
  }

  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "-- Bit Swizzle -- \n");

  GetFirstRank (MrcData, FirstRank);

  for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
    for (Channel = 0; Channel < MaxChannel; Channel++) {
      Rank = FirstRank[Controller][Channel];
      McChBitMask |= SelectReutRanks (MrcData, Controller, Channel, 1 << Rank, FALSE, 0); //Save particular mask to use with runIOTest later
      if (!(MrcRankExist (MrcData, Controller, Channel, Rank))) {
        continue;
      }
      ControllerIn = &Inputs->Controller[Controller];
      ChannelOut = &Outputs->Controller[Controller].Channel[Channel];

      // Initialize DQ mapping structure
      MrcCall->MrcSetMem (
        (UINT8 *)(ControllerIn->Channel[Channel].DqMapCpu2Dram),
        sizeof (ControllerIn->Channel[0].DqMapCpu2Dram),
        0xFF
      );

      // Initial setup of DRAM
      MrcIssueMrw (MrcData, Controller, Channel, Rank, MrDqCalPatA, 0x00, TRUE); // DQ Calibration Pattern "A"
      MrcIssueMrw (MrcData, Controller, Channel, Rank, MrDqCalPatB, 0x00, TRUE); // DQ Calibration Pattern "B"

      if (Ddr5) {
        MrcIssueMrw (MrcData, Controller, Channel, Rank, mrMR25, 0x00, TRUE); // Read Training Mode Settings
        MrcIssueMrw (MrcData, Controller, Channel, Rank, mrMR30, 0x00, TRUE); // LFSR Assignment(0-LFSR0, 1-LFSR1)
      }
    } // for Channel
  } // for Controller


  for (Bit = 0; Bit < MAX_BITS; Bit++) {
    // Clear out DataTrainFeedback field
    MrcGetSetChStrb (MrcData, MAX_CONTROLLER, MAX_CHANNEL, MAX_SDRAM_IN_DIMM, GsmIocDataTrainFeedback, WriteNoCache, &GetSetDis);
    InvertData = 1 << Bit;
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\nRunning Bit %d\n", Bit);
    for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
      for (Channel = 0; Channel < MaxChannel; Channel++) {
        Rank = FirstRank[Controller][Channel];
        if (!(MrcRankExist (MrcData, Controller, Channel, Rank))) {
          continue;
        }
        MrcIssueMrw (MrcData, Controller, Channel, Rank, MrDqInvertLow,  InvertData, FALSE); // Lower Byte Invert for DQ Calibration
        MrcIssueMrw (MrcData, Controller, Channel, Rank, MrDqInvertHigh, InvertData, FALSE); // Upper Byte Invert for DQ Calibration
      }
    }
    // Configure MC to issue MPC[Read DQ Calibration] instead of reads
    MrcGetSetMcCh (MrcData, MAX_CONTROLLER, MAX_CHANNEL, GsmMccMprTrainDdrOn, WriteNoCache, &GetSetEn);

    // Start CPGC and run for 1uS
    CpgcSeqCtl.Data = 0;
    CpgcSeqCtl.Bits.START_TEST = 1;
    Cpgc20ControlRegWrite (MrcData, McChBitMask, CpgcSeqCtl);

    // Wait to obtain results in DataTrainFeedback
    MrcWait (MrcData, 200 * MRC_TIMER_1NS);

    // Get Results for all ch/bytes
    // Update results for all ch/bytes
    for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
      for (Channel = 0; Channel < MaxChannel; Channel++) {
        if (!MrcChannelExist (MrcData, Controller, Channel)) {
          continue;
        }

        ChannelIn = &Inputs->Controller[Controller].Channel[Channel];
        Dimm = RANK_TO_DIMM_NUMBER (FirstRank[Controller][Channel]);
        for (Byte = 0; Byte < Outputs->SdramCount; Byte++) {
          if (!MrcByteExist (MrcData, Controller, Channel, Byte)) {
            MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "  ");
            continue;
          }
          DramByte = ChannelIn->DqsMapCpu2Dram[Dimm][Byte];

          MrcGetSetChStrb (MrcData, Controller, Channel, Byte, GsmIocDataTrainFeedback, ReadUncached, &DataTrainFeedbackField);

          if (Ddr5 && (Byte == (MAX_BYTE_IN_DDR5_CHANNEL - 1))) {
            if (Bit > 3) {
              continue;     // DDR5 ECC byte has only 4 bits
            }
            FeedbackMask = 0x0F;
          } else {
            FeedbackMask = 0xFF;
          }

          Feedback[Byte] = (UINT8) (DataTrainFeedbackField & FeedbackMask);
          MRC_DEBUG_MSG (
            Debug,
            MSG_LEVEL_NOTE,
            "\tMc%u C%u CPU Byte %u Dram Byte %u => Feedback = %02X - %s feedback\n",
            Controller,
            Channel,
            Byte,
            DramByte,
            Feedback[Byte],
            (MrcCountBitsEqOne (Feedback[Byte]) == 1) ? "Good" : "Bad"
          );
        } // for Byte

        // Store results in ChannelIn->DqMapCpu2Dram
        StatusLocal = FillCA2DQMapResult (MrcData, Controller, Channel, Feedback, Bit);
        if (StatusLocal != mrcSuccess) {
          Status = StatusLocal;
        }
      }
    }

    // Stop CPGC
    CpgcSeqCtl.Data = 0;
    CpgcSeqCtl.Bits.STOP_TEST = 1;
    Cpgc20ControlRegWrite (MrcData, McChBitMask, CpgcSeqCtl);

    // Wait till CPGC test is done on all participating channels
    // Wait until Channel test done status matches ChBitMask
    do {
      TestDoneStatus = 0;
      for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
        if (MrcControllerExist(MrcData, Controller)) {
          Offset = OFFSET_CALC_CH (MC0_STALL_DRAIN_REG, MC1_STALL_DRAIN_REG, Controller);
          StallDrain.Data = MrcReadCR (MrcData, Offset);
          for (Channel = 0; Channel < MaxChannel; Channel++) {
            CurMcChBitMask = (1 << ((Controller * MaxChannel) + Channel));
            if (((McChBitMask & CurMcChBitMask) != 0) && (!(IS_MC_SUB_CH (Lpddr, Channel)))) {
              IpChannel = LP_IP_CH (Lpddr, Channel);
              ChannelOut = &Outputs->Controller[Controller].Channel[Channel];
              Offset = OFFSET_CALC_MC_CH (MC0_REQ0_CR_CPGC_SEQ_STATUS_REG, MC1_REQ0_CR_CPGC_SEQ_STATUS_REG, Controller, MC0_REQ1_CR_CPGC_SEQ_STATUS_REG, IpChannel);
              CpgcSeqStatus.Data = MrcReadCR (MrcData, Offset);
              if ((CpgcSeqStatus.Bits.TEST_DONE) && (StallDrain.Bits.mc_drained)) {
                TestDoneStatus |= (ChannelOut->CpgcChAssign << (Controller * MaxChannel));
              }
            }
          }
        }
      }
    } while ((TestDoneStatus & McChBitMask) != McChBitMask);

    MrcGetSetMcCh (MrcData, MAX_CONTROLLER, MAX_CHANNEL, GsmMccMprTrainDdrOn, WriteNoCache, &GetSetDis);
  }  // for Bit

  return Status;
}

/**
  Configure MC for RDRD B2B CPGC test.
  For DDR4/5 this ensures B2B traffic in case tCCD_S/tCCD_L is relaxed (e.g. in OC scenario)
  For LP4/5 this configures MC to properly handle BL16 MRR data.
  CPGC BankGroup mapping for B2B traffic is done in PreTraining step.
  For LPDDR4/LPDDR5:
   - CPGC2_V_CHICKEN.MPR_TEST_REQ_DBLR   = !Gear4
   - MAD_INTER_CHANNEL.HalfCacheLineMode = 1
   - tRDRD_sg = tRDRD_dg = 8 (BL16)
  For DDR4:
   - tRDRD_sg = MIN (8, tRDRD_sg)
   - tRDRD_dg = 4
  For DDR5:
   - tRDRD_sg = MIN (16, tRDRD_sg)
   - tRDRD_dg = 8

  @param[in] MrcData       - The MRC global data.
  @param[in] SaveData      - Register data saved on enable and restored on disable
  @param[in] Enable        - TRUE to configure for B2B, FALSE to restore original config
**/
VOID
SetupMcMprConfig (
  IN     MrcParameters *const MrcData,
  IN OUT MC_MPR_CONFIG_SAVE   *SaveData,
  IN     BOOLEAN              Enable
  )
{
  MrcOutput           *Outputs;
  MrcIntOutput        *IntOutputs;
  UINT32              Offset;
  UINT32              FirstController;
  UINT32              FirstChannel;
  UINT8               Controller;
  UINT8               Channel;
  BOOLEAN             Lpddr;
  INT64               GetSetVal;
  INT64               RdRdSg;
  INT64               RdRdDg;
  MC0_CR_CPGC2_V_CHICKEN_STRUCT CpgcChickenStruct;

  Outputs     = &MrcData->Outputs;
  IntOutputs  = (MrcIntOutput *)(MrcData->IntOutputs.Internal);
  Lpddr       = Outputs->Lpddr;
  FirstController = Outputs->FirstPopController;
  FirstChannel    = Outputs->Controller[FirstController].FirstPopCh;

  if (Enable) {
    IntOutputs->SkipZq = TRUE;    // RDMPR training may call SetupIoTest multiple times, which does ZQCAL, so we skip it.

    // Save original TAT values - all channels have the same values
    MrcGetSetMcCh (MrcData, FirstController, FirstChannel, GsmMctRDRDsg, ReadFromCache, &SaveData->RdRdSg);
    MrcGetSetMcCh (MrcData, FirstController, FirstChannel, GsmMctRDRDdg, ReadFromCache, &SaveData->RdRdDg);

    if (Lpddr) {
      for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
        if (MrcControllerExist (MrcData, Controller)) {
          Offset = OFFSET_CALC_CH (MC0_CR_CPGC2_V_CHICKEN_REG, MC1_CR_CPGC2_V_CHICKEN_REG, Controller);
          SaveData->CpgcChicken[Controller] = MrcReadCR (MrcData, Offset);
          CpgcChickenStruct.Data = SaveData->CpgcChicken[Controller];
          CpgcChickenStruct.Bits.MPR_TEST_REQ_DBLR = (Outputs->Gear4) ? 0 : 1;
          MrcWriteCR (MrcData, Offset, CpgcChickenStruct.Data);
          GetSetVal = 1;
          MrcGetSetMc (MrcData, Controller, GsmMccHalfCachelineMode, WriteToCache, &GetSetVal);
        }
      }
    }
    if (Lpddr) {
      RdRdDg = RdRdSg = 8;  // LP4 and LP5 MRR is always BL16
    } else if (Outputs->DdrType == MRC_DDR_TYPE_DDR4) {
      RdRdDg = TCCD_ALL_FREQ;
      RdRdSg = MIN (8, SaveData->RdRdSg);
    } else { // DDR5
      RdRdDg = 8;
      RdRdSg = MIN (16, SaveData->RdRdSg);
    }
  } else { // Clean up after Test.
    IntOutputs->SkipZq = FALSE;
    if (Lpddr) {
      for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
        if (MrcControllerExist (MrcData, Controller)) {
          Offset = OFFSET_CALC_CH (MC0_CR_CPGC2_V_CHICKEN_REG, MC1_CR_CPGC2_V_CHICKEN_REG, Controller);
          MrcWriteCR (MrcData, Offset, SaveData->CpgcChicken[Controller]);
          GetSetVal = 0;
          MrcGetSetMc (MrcData, Controller, GsmMccHalfCachelineMode, WriteToCache, &GetSetVal);
        }
      }
    }
    RdRdSg = SaveData->RdRdSg;
    RdRdDg = SaveData->RdRdDg;
  }

  MrcGetSetMcCh (MrcData, MAX_CONTROLLER, MAX_CHANNEL, GsmMctRDRDsg, WriteToCache, &RdRdSg);
  MrcGetSetMcCh (MrcData, MAX_CONTROLLER, MAX_CHANNEL, GsmMctRDRDdg, WriteToCache, &RdRdDg);
  MrcFlushRegisterCachedData (MrcData);
  for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
    for (Channel = 0; Channel < MAX_CHANNEL; Channel++) {
      if ((!MrcChannelExist (MrcData, Controller, Channel)) || IS_MC_SUB_CH (Lpddr, Channel)) {
        continue;
      }
      SetTcBubbleInjector (MrcData, Controller, Channel);  // Must update the XARB bubble injector when TAT values change
    }
  }
}

/**
  Update DqMapCpu2Dram array

  @param[in] MrcData     - The MRC global data.
  @param[in] Controller  - the controller to work on
  @param[in] Channel     - the channel to work on
  @param[in] Feedback    - array of DATATRAINFEEDBACK values for all 8 bytes
  @param[in] Bit         - The DQ bit that should be set in each DRAM byte

  @retval mrcSuccess if succeeded
**/
MrcStatus
FillCA2DQMapResult (
  IN OUT MrcParameters *const   MrcData,
  IN const UINT32               Controller,
  IN const UINT32               Channel,
  IN const UINT8                Feedback[8],
  IN const UINT8                Bit
  )
{
  MrcInput          *Inputs;
  MrcDebug          *Debug;
  MrcOutput         *Outputs;
  MrcControllerIn   *ControllerIn;
  MrcChannelIn      *ChannelIn;
  MrcStatus         Status;
  UINT8             Byte;
  UINT8             Temp;
  UINT8             CpuBit;
  INT8              BitNumber;
  BOOLEAN           BitFound;

  Status        = mrcSuccess;
  Inputs        = &MrcData->Inputs;
  Outputs       = &MrcData->Outputs;
  Debug         = &Outputs->Debug;
  ControllerIn  = &Inputs->Controller[Controller];

  ChannelIn  = &ControllerIn->Channel[Channel];

  BitNumber = -1;

  // Loop on CPU bytes
  for (Byte = 0; Byte < Outputs->SdramCount; Byte++) {
    if (!MrcByteExist (MrcData, Controller, Channel, Byte)) {
      continue;
    }
    Temp = Feedback[Byte];
    BitNumber = 0;
    CpuBit    = 0;
    BitFound  = FALSE;
    while (Temp > 0) {
      if (Temp & 1) {
        if (!BitFound) {
          CpuBit = BitNumber;
          BitFound = TRUE;
        } else {
          Status = mrcFail;
          MRC_DEBUG_MSG (
            Debug,
            MSG_LEVEL_NOTE,
            "Ch%d: ERROR: More than one DQ pin toggled while looking for DQ%d in Byte%d, Feedback=0x%X\n",
            Channel,
            Bit,
            Byte,
            Feedback[Byte]
            );
          break;
        }
      }
      Temp >>= 1;
      BitNumber++;
    }
    if (!BitFound) {
      Status = mrcFail;
    }

    ChannelIn->DqMapCpu2Dram[Byte][CpuBit] = ChannelIn->DqsMapCpu2Dram[dDIMM0][Byte] * 8 + Bit; // Lpddr only uses DIMM0 entry
  } // for Byte

  return Status;
}

/**
  Print DqMapCpu2Dram and DqsMapCpu2Dram array

  @param[in] MrcData     - The MRC global data.

  @retval void
**/
VOID
PrintDqDqsTable (
  IN OUT MrcParameters *const   MrcData
  )
{
  MrcInput            *Inputs;
  MrcOutput           *Outputs;
  MrcDebug            *Debug;
  MrcControllerIn     *ControllerIn;
  MrcChannelIn        *ChannelIn;
  UINT8               Controller;
  UINT8               Channel;
  UINT8               Dimm;
  UINT8               Byte;
  UINT8               Iteration;
  UINT8               Bit;
  UINT8               MaxDimm;
  BOOLEAN             Ddr5;
  UINT8               BytesPerChannel;
#ifdef MRC_DEBUG_PRINT
  static const char PrintBorder[]    = "\n*******************************\n";
#endif

  Outputs         = &MrcData->Outputs;
  Inputs          = &MrcData->Inputs;
  Debug           = &Outputs->Debug;
  Ddr5            = (Outputs->DdrType == MRC_DDR_TYPE_DDR5);
  MaxDimm         = Ddr5 ? MAX_DIMMS_IN_CHANNEL : 1;
  BytesPerChannel = Outputs->Lpddr ? MAX_BYTE_IN_LP_CHANNEL : MAX_BYTE_IN_DDR5_CHANNEL;

#ifdef MRC_DEBUG_PRINT
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "%s", PrintBorder);
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "%s   DQDQS SWIZZLING   %s", "*****", "*****");
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "%s", PrintBorder);
#endif
  for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
    ControllerIn = &Inputs->Controller[Controller];
    for (Channel = 0; Channel < MAX_CHANNEL; Channel++) {
      if (!MrcChannelExist (MrcData, Controller, Channel)) {
        continue;
      }
      ChannelIn = &ControllerIn->Channel[Channel];
      if (!Ddr5) {
        MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Mc%u.Ch%u", Controller, Channel);
      }
      for (Dimm = 0; Dimm < MaxDimm; Dimm++) {
        if (!MrcRankExist (MrcData, Controller, Channel, Dimm * MAX_RANK_IN_DIMM)) {
          continue;
        }
        if (Dimm == 1) {
          MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\n");
        }
        if (Ddr5) {
          MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Mc%u.Ch%u.D%u", Controller, Channel, Dimm);
        }
        MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, ":"); // Separator
        for (Iteration = 0; Iteration < BytesPerChannel; Iteration++) {
          MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, " %u", ChannelIn->DqsMapCpu2Dram[Dimm][Iteration]);
        }

        MRC_DEBUG_MSG (Debug, MSG_LEVEL_ERROR, " DqMapCpu2Dram: ");
        for (Byte = 0; Byte < Outputs->SdramCount; Byte++) {
          if (!MrcByteExist (MrcData, Controller, Channel, Byte)) {
            continue;
          }
          for (Bit = 0; Bit < MAX_BITS; Bit++) {
            MRC_DEBUG_MSG (Debug, MSG_LEVEL_ERROR, "%d ", ChannelIn->DqMapCpu2Dram[Byte][Bit]);
          }
        }
      }
      MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\n");
    } // Channel
  } // Controller
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\n");
}

/**
  Program given VOC PSal / NSal values on all populated bytes / bits

  @param[in,out] MrcData - Include all MRC global data.
  @param[in]     VocPsal - VOC PSal value
  @param[out]    VocNsal - VOC NSal value
**/
VOID
MrcSetVocMulticast (
  IN OUT MrcParameters *const MrcData,
  IN INT64                    VocPsal,
  IN INT64                    VocNsal
  )
{

  MrcOutput             *Outputs;

  Outputs = &MrcData->Outputs;

  if ((Outputs->RxMode == MrcRxModeMatchedN) || (Outputs->RxMode == MrcRxModeMatchedP)) {
    MrcGetSetBit (MrcData, MAX_CONTROLLER, MAX_CHANNEL, MAX_RANK_IN_CHANNEL, MAX_SDRAM_IN_DIMM, MAX_BITS, RxMatchedUnmatchedOffPcal, ForceWriteUncached, &VocPsal);
  } else {
    MrcGetSetBit (MrcData, MAX_CONTROLLER, MAX_CHANNEL, MAX_RANK_IN_CHANNEL, MAX_SDRAM_IN_DIMM, MAX_BITS, RxUnmatchedOffNcal,        ForceWriteUncached, &VocNsal);
    MrcGetSetBit (MrcData, MAX_CONTROLLER, MAX_CHANNEL, MAX_RANK_IN_CHANNEL, MAX_SDRAM_IN_DIMM, MAX_BITS, RxMatchedUnmatchedOffPcal, ForceWriteUncached, &VocPsal);
  }
  // To make the VOC code take effective
  IssueRxReset (MrcData);

}

/**
  This procedure will find a Ratio of VOC step to RxVref step.

  @param[in,out] MrcData         - Include all MRC global data.
  @param[in]     LoopCount       - loop count
  @param[out]    RatioPerBit     - The Array contains the calibrated VOC to RxVref Ratio, per bit, times 100 for precision.

  @retval MrcStatus -  If succeeded, return mrcSuccess
**/
MrcStatus
VocCalibration (
  IN OUT MrcParameters *const MrcData,
  IN     const UINT8          LoopCount,
  IN OUT UINT32               RatioPerBit[MAX_RANK_IN_CHANNEL][MAX_CONTROLLER][MAX_CHANNEL][MAX_SDRAM_IN_DIMM][MAX_BITS]
  )
{
  MrcOutput           *Outputs;
  MrcStatus           Status;
  UINT8               ResultType;
  UINT32              Controller;
  UINT32              Channel;
  UINT32              SdramCount;
  UINT32              MaxChannels;
  UINT8               Rank;
  UINT32              RankMask;
  UINT8               McChBitMask;
  UINT8               MaxMargin;
  UINT32              Edge;
  UINT32              Byte;
  UINT32              Bit;
  UINT16              MarginByte[MAX_CONTROLLER][MAX_CHANNEL][MAX_SDRAM_IN_DIMM][MAX_EDGES];
  UINT16              MarginBit[MAX_CONTROLLER][MAX_CHANNEL][MAX_SDRAM_IN_DIMM][MAX_BITS][MAX_EDGES];
  INT16               TempMarginBit[MAX_RANK_IN_CHANNEL][MAX_CONTROLLER][MAX_CHANNEL][MAX_SDRAM_IN_DIMM][MAX_BITS][MAX_EDGES];
  UINT16              LowDelta;
  UINT16              HiDelta;
  INT64               VocPsal;
  INT64               VocNsal;
  static INT32        VocPoints[] = { -10, -5, 0, 5, 10 };
  INT16               VocStep;
  UINT32              Index;
  UINT32              Ratio;

  Outputs     = &MrcData->Outputs;
  MaxChannels = Outputs->MaxChannels;
  SdramCount  = Outputs->SdramCount;
  Status      = mrcSuccess;
  VocStep     = 5;

  // Setup CPGC Test
  SetupIOTestBasicVA (MrcData, Outputs->McChBitMask, LoopCount, NSOE, 0, 0, 8, PatWrRd, 0, 0);

  ResultType = GetMarginResultType (RdV);

  // @todo_adl add VOC unmatched support
  VocNsal = VOC_DEFAULT;

  for (Index = 0; Index < ARRAY_COUNT (VocPoints); Index++) {
    // Set VOC
    VocPsal = VOC_DEFAULT + VocPoints[Index];
    if (Outputs->RxMode == MrcRxModeUnmatchedP) {
      VocNsal = VOC_DEFAULT + VocPoints[Index];
    }
    MrcSetVocMulticast (MrcData, VocPsal, VocNsal);

    // Collect margins
    for (Rank = 0; Rank < MAX_RANK_IN_CHANNEL; Rank++) {
      // Select rank for REUT test
      RankMask = 1 << Rank;
      McChBitMask = 0;
      for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
        for (Channel = 0; Channel < MaxChannels; Channel++) {
          McChBitMask |= SelectReutRanks (MrcData, Controller, Channel, RankMask, FALSE, 0);
        }
      }
      // Continue with next rank if this rank is not present on any channel
      if (McChBitMask == 0) {
        continue;
      }

      for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
        for (Channel = 0; Channel < MaxChannels; Channel++) {
          for (Byte = 0; Byte < SdramCount; Byte++) {
            for (Edge = 0; Edge < MAX_EDGES; Edge++) {
              MarginByte[Controller][Channel][Byte][Edge] = Outputs->MarginResult[ResultType][Rank][Controller][Channel][Byte][Edge];
            }
          }
        }
      }

      MaxMargin = GetVrefOffsetLimits (MrcData, RdV);

      // Collect Margin per bit
      MrcGetMarginBit (MrcData, McChBitMask, Rank, MarginBit, MarginByte, RdV, 0, MaxMargin, MRC_PRINTS_OFF);

      // Calculate Center per bit
      for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
        for (Channel = 0; Channel < MaxChannels; Channel++) {
          if (!MrcRankExist (MrcData, Controller, Channel, Rank)) {
            continue;
          }
          for (Byte = 0; Byte < SdramCount; Byte++) {
            if (!MrcByteExist (MrcData, Controller, Channel, Byte)) {
              continue;
            }
            //MRC_DEBUG_MSG(Debug, MSG_LEVEL_NOTE, "\nR%dMc%dC%dB%d :\t", Rank, Controller, Channel, Byte);
            for (Bit = 0; Bit < MAX_BITS; Bit++) {
              if (Index != 0) {
                HiDelta  = ABS (TempMarginBit[Rank][Controller][Channel][Byte][Bit][1] - (INT16) (MarginBit[Controller][Channel][Byte][Bit][1]));
                LowDelta = ABS (TempMarginBit[Rank][Controller][Channel][Byte][Bit][0] - (INT16) (MarginBit[Controller][Channel][Byte][Bit][0]));
                RatioPerBit[Rank][Controller][Channel][Byte][Bit] += MAX (HiDelta, LowDelta);
              }
              //MRC_DEBUG_MSG(Debug, MSG_LEVEL_NOTE, "[ %d  %d  %d] ", MarginBit[Controller][Channel][Byte][Bit][0], MarginBit[Controller][Channel][Byte][Bit][1], RatioPerBit[Rank][Controller][Channel][Byte][Bit]);
              TempMarginBit[Rank][Controller][Channel][Byte][Bit][1] = (INT16) MarginBit[Controller][Channel][Byte][Bit][1]; // High margin
              TempMarginBit[Rank][Controller][Channel][Byte][Bit][0] = (INT16) MarginBit[Controller][Channel][Byte][Bit][0]; // Low margin
            } // Bit
          } // Byte
        } // Channel
      } // Controller
    } // Rank
  } // Index

  //MRC_DEBUG_MSG(Debug, MSG_LEVEL_NOTE, "\n");
  // Calculate final ratio
  //MRC_DEBUG_MSG(Debug, MSG_LEVEL_NOTE, "[ (10 * SumDeltas) / [(NumOfPoints - 1) * VocStep] ]\n");
  for (Rank = 0; Rank < MAX_RANK_IN_CHANNEL; Rank++) {
    for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
      for (Channel = 0; Channel < MaxChannels; Channel++) {
        if (!MrcRankExist (MrcData, Controller, Channel, Rank)) {
          continue;
        }
        for (Byte = 0; Byte < SdramCount; Byte++) {
          if (!MrcByteExist (MrcData, Controller, Channel, Byte)) {
            continue;
          }
          //MRC_DEBUG_MSG(Debug, MSG_LEVEL_NOTE, "\nR%dMc%dC%dB%d :", Rank, Controller, Channel, Byte);
          for (Bit = 0; Bit < MAX_BITS; Bit++) {
            Ratio = RatioPerBit[Rank][Controller][Channel][Byte][Bit];
            Ratio *= 10;                                        // Multiply by 10 to avoid precision loss in the divide
            Ratio /= ((ARRAY_COUNT (VocPoints) - 1) * VocStep); // Find average Ratio per bit - this is RxVref to VOC, we need the opposite
            if (Ratio < 10) {
              // If this is too small, it means that VOC change was not causing a meaningful change in RxVref
              Ratio = 0;
            } else {
              // Find the VOC step to RxVref step ratio, times 100 for precision.
              Ratio = 1000 / Ratio;
            }
            RatioPerBit[Rank][Controller][Channel][Byte][Bit] = Ratio;
            //MRC_DEBUG_MSG(Debug, MSG_LEVEL_NOTE, " %d\t ", Ratio);
          }
        }
      }
    }
  } // Rank
  //MRC_DEBUG_MSG(Debug, MSG_LEVEL_NOTE, "\n");

  // Restore PBD to middle
  MrcSetVocMulticast (MrcData, VOC_DEFAULT, VOC_DEFAULT);

  return Status;
}

/**
  This procedure will find optimal PBD (Rx/Tx) or VOC (Rx) based on per bit margins.

  @param[in,out] MrcData      - Include all MRC global data.
  @param[in]     Param        - {RdTN, RdTP, WrT, RdV}
  @param[in]     LoopCount    - loop count
  @param[in]     TestType     - 0: Normal Stress, 1: Read MPR Data
  @param[in]     Prints       - Enable/Disable print
  @param[in]     MprMarginBit - per bit margins (from ReadMPR)

  @retval MrcStatus -  If succeeded, return mrcSuccess
**/
MrcStatus
PerBit1DCentering (
  IN OUT MrcParameters *const MrcData,
  IN     const UINT8          Param,
  IN     const UINT8          LoopCount,
  IN     const UINT8          TestType,
  IN     const BOOLEAN        Prints,
  IN     const INT8           MprMarginBit[MAX_RANK_IN_CHANNEL][MAX_CONTROLLER][MAX_CHANNEL][MAX_SDRAM_IN_DIMM][MAX_BITS][MAX_EDGES]
  )
{
  const MrcInput      *Inputs;
  MrcDebug            *Debug;
  const MRC_FUNCTION  *MrcCall;
  MrcOutput           *Outputs;
  MrcSaveData         *SaveOutputs;
  MrcStatus           Status;
  MrcDebugMsgLevel    DebugLevel;
  GSM_GT              PerBitGt;
  UINT8               ResultType;
  UINT8               Channel;
  UINT8               MaxChannels;
  UINT8               Rank;
  UINT8               RankMask;
  UINT8               McChBitMask;
  UINT8               Controller;
  UINT8               MaxMargin;
  UINT8               Edge;
  UINT8               ByteMarginParam;
  UINT16              Byte;
  UINT8               Bit;
  INT32               CenterPerBit[MAX_RANK_IN_CHANNEL][MAX_CONTROLLER][MAX_CHANNEL][MAX_SDRAM_IN_DIMM][MAX_BITS];
  INT32               OffsetPerBit[MAX_RANK_IN_CHANNEL][MAX_CONTROLLER][MAX_CHANNEL][MAX_SDRAM_IN_DIMM][MAX_BITS];
  INT32               AvgCenterPerByte[MAX_RANK_IN_CHANNEL][MAX_CONTROLLER][MAX_CHANNEL][MAX_SDRAM_IN_DIMM];
  UINT32              RatioPerBit[MAX_RANK_IN_CHANNEL][MAX_CONTROLLER][MAX_CHANNEL][MAX_SDRAM_IN_DIMM][MAX_BITS];
  INT32               NormOffset;
  INT64               GetSetVal;
  UINT16              MarginByte[MAX_CONTROLLER][MAX_CHANNEL][MAX_SDRAM_IN_DIMM][MAX_EDGES];
  UINT16              MarginBit[MAX_CONTROLLER][MAX_CHANNEL][MAX_SDRAM_IN_DIMM][MAX_BITS][MAX_EDGES];
  UINT32              UiMask;
  INT64               VocPsal;
  INT64               VocNsal;
  BOOLEAN             Matched;
  UINT32              PBDRatio;
  UINT32              RankCount;
  INT32               AvgPerRank;
  INT64               GetSetEn;
  UINT8               FirstController;
  UINT8               FirstChannel;
  INT64               InternalClocksOnSave;
  UINT8               ParamList[2]; // Parameters for training
  UINT8               ParamLen;
  UINT8               ParamIndex;
  INT32               MinCenterPerBit;
  INT64               ByteDelay;
  BOOLEAN             ReadVGroup;

  Inputs  = &MrcData->Inputs;
  Outputs = &MrcData->Outputs;
  Debug   = &Outputs->Debug;
  SaveOutputs = &MrcData->Save.Data;
  MaxChannels = Outputs->MaxChannels;
  PBDRatio = 0;
  MrcCall = Inputs->Call.Func;
  Status  = mrcSuccess;
  Matched = (Outputs->RxMode == MrcRxModeMatchedN) || (Outputs->RxMode == MrcRxModeMatchedP);
  DebugLevel = (Prints) ? MSG_LEVEL_NOTE : MSG_LEVEL_NEVER;
  UiMask = 0x5555;
  ByteDelay = 0;

  MrcCall->MrcSetMem ((UINT8 *) AvgCenterPerByte, sizeof (AvgCenterPerByte), 0);
  MrcCall->MrcSetMem ((UINT8 *) RatioPerBit, sizeof (RatioPerBit), 0);

  if (Param == RdTN) {
    PerBitGt = RxDqsNBitDelay;
    ByteMarginParam = RdT;
  } else if (Param == RdTP) {
    PerBitGt = RxDqsPBitDelay;
    ByteMarginParam = RdT;
  } else if (Param == WrT) {
    PerBitGt = TxDqBitDelay;
    ByteMarginParam = WrT;
  } else {
    PerBitGt = RxVoc;
    ByteMarginParam = RdV;
  }
  FirstController = Outputs->FirstPopController;
  FirstChannel    = Outputs->Controller[FirstController].FirstPopCh;

  if (Param == RdV) {
    MRC_DEBUG_MSG (Debug, DebugLevel, "\n Set Internal Clock On\n");
    // Set InternalClocksOn to avoid clock gating
    MrcGetSetChStrb (MrcData, FirstController, FirstChannel, 0, GsmIocInternalClocksOn, ReadFromCache, &InternalClocksOnSave);

    GetSetEn      = 1;
    MrcGetSetChStrb (MrcData, MAX_CONTROLLER, MAX_CHANNEL, MAX_SDRAM_IN_DIMM, GsmIocInternalClocksOn, WriteCached, &GetSetEn);

    VocCalibration (MrcData, LoopCount, RatioPerBit);
  } else {
    PBDRatio = Outputs->PbdRatio;
  }
  // Init PBD to ZERO
  VocNsal = VOC_DEFAULT;
  VocPsal = VOC_DEFAULT;
  GetSetVal = 0;

  if (Param == RdV) {
    MrcSetVocMulticast (MrcData, VocPsal, VocNsal);
    DQTimeCentering1D (MrcData, RdV, 0, 17, MRC_PRINTS_OFF, FALSE, 1);
  } else {
    //Don't zero out the PBD and leave it as the existing value.
  }

  if (Matched) {
    ParamList[0] = Param;
    ParamLen = 1;
  } else {
    if (PerBitGt == RxVoc) {
      ParamList[0] = RdV; // VOCPcal
      ParamList[1] = RdVBit; // VOCNcal
      ParamLen = 2;
    } else {
      ParamList[0] = Param; // RdTN/P WrT
      ParamLen = 1;
    }
  }
  for (ParamIndex = 0; ParamIndex < ParamLen; ParamIndex++) {
    // Setup CPGC Test
    if (TestType == 0) { // for normal stress only
      SetupIOTestBasicVA(MrcData, Outputs->McChBitMask, LoopCount, NSOE, 0, 0, 8, PatWrRd, 0, 0);

      // Program chunk mask for RxDqsP/N.
      // This is set to all chunks in SetupIOTest, so needs to be done after calling SetupIOTest.
      // @todo add VOC unmatched support
      if ((ParamList[ParamIndex] == RdTP) || (ParamList[ParamIndex] == RdTN)) {
        // For RxDqsP check only even chunks
        UiMask = 0x5555;
        if (ParamList[ParamIndex] == RdTN) {
          // For RxDqsN check only odd chunks
          UiMask = UiMask << 1;
        }
        MrcSetChunkAndClErrMsk(MrcData, 0xFF, UiMask);
      }
      if (!Matched && ((ParamList[ParamIndex] == RdV) || (ParamList[ParamIndex] == RdVBit))) {
        UiMask = 0x5555; // VocPsal
        if (ParamList[ParamIndex] == RdVBit) {
          UiMask = UiMask << 1; // VocNsal
        }
        MrcSetChunkAndClErrMsk(MrcData, 0xFF, UiMask);
      }
    } // if TestType

    MRC_DEBUG_MSG(Debug, DebugLevel, "\nPerBitCentering Started for %s\n", (ParamList[ParamIndex] == RdTN) ? "RxDqsNBitDelay" : (ParamList[ParamIndex] == RdTP) ? "RxDqsPBitDelay" : (ParamList[ParamIndex] == WrT) ? "TxDqBitDelay" : (ParamList[ParamIndex] == RdVBit) ? "VOCNcal" : "VOCPCal");
    ReadVGroup = (ParamList[ParamIndex] == RdV || ParamList[ParamIndex] == RdVBit);
    for (Rank = 0; Rank < MAX_RANK_IN_CHANNEL; Rank++) {
      // Select rank for REUT test
      RankMask = 1 << Rank;
      McChBitMask = 0;
      for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
        for (Channel = 0; Channel < MaxChannels; Channel++) {
          McChBitMask |= SelectReutRanks (MrcData, Controller, Channel, RankMask, FALSE, 0);
        }
      }
      // Continue with next rank if this rank is not present on any channel
      if (McChBitMask == 0) {
        continue;
      }

      ResultType = GetMarginResultType (ByteMarginParam);
      for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
        for (Channel = 0; Channel < MaxChannels; Channel++) {
          for (Byte = 0; Byte < Outputs->SdramCount; Byte++) {
            for (Edge = 0; Edge < MAX_EDGES; Edge++) {
              // for RdT WC of P/N is good enough; Consider start from 0 margin;
              MarginByte[Controller][Channel][Byte][Edge] = Outputs->MarginResult[ResultType][Rank][Controller][Channel][Byte][Edge];
              if (TestType == 1) {
                for (Bit = 0; Bit < MAX_BITS; Bit++) {
                  MarginBit[Controller][Channel][Byte][Bit][1] = ABS (MprMarginBit[Rank][Controller][Channel][Byte][Bit][1]);  // Right margin
                  MarginBit[Controller][Channel][Byte][Bit][0] = ABS (MprMarginBit[Rank][Controller][Channel][Byte][Bit][0]);  // Left margin
                }
              } // if TestType
            }
          }
        }
      }

      if ((ParamList[ParamIndex] == RdV) || (ParamList[ParamIndex] == RdVBit)) {
        MaxMargin = GetVrefOffsetLimits(MrcData, ByteMarginParam);
      } else {
        MaxMargin = MAX_POSSIBLE_TIME;
      }

      // Collect Margin per bit
      if (TestType == 0) {
        MrcGetMarginBit (MrcData, McChBitMask, Rank, MarginBit, MarginByte, ByteMarginParam, 0, MaxMargin, MRC_PRINTS_OFF);
      }
      // Calculate Center per bit
      for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
        for (Channel = 0; Channel < MaxChannels; Channel++) {
          if (!MrcRankExist(MrcData, Controller, Channel, Rank)) {
            continue;
          }

          MRC_DEBUG_MSG (Debug, DebugLevel, "\nR%u MC%u C%u per-bit Edges [Left Right]", Rank, Controller, Channel);
          for (Byte = 0; Byte < Outputs->SdramCount; Byte++) {
            if (!MrcByteExist (MrcData, Controller, Channel, Byte)) {
              continue;
            }
            MRC_DEBUG_MSG (Debug, DebugLevel, "\n B%u: ", Byte);
            if (!ReadVGroup) {
              if (Param == RdTN) {
                MrcGetSetStrobe (MrcData, Controller, Channel, Rank, Byte, RxDqsNDelay, ReadCached, &ByteDelay);
              } else if (Param == RdTP) {
                MrcGetSetStrobe (MrcData, Controller, Channel, Rank, Byte, RxDqsPDelay, ReadCached, &ByteDelay);
              } else if (Param == WrT) {
                MrcGetSetStrobe (MrcData, Controller, Channel, Rank, Byte, TxDqDelay,   ReadCached, &ByteDelay);
              } else {
                ByteDelay = 0;
              }
            }
            MinCenterPerBit = MRC_INT32_MAX;
            for (Bit = 0; Bit < MAX_BITS; Bit++) {
              CenterPerBit[Rank][Controller][Channel][Byte][Bit]   = (INT32) MarginBit[Controller][Channel][Byte][Bit][1]; // Right margin
              CenterPerBit[Rank][Controller][Channel][Byte][Bit]  -= (INT32) MarginBit[Controller][Channel][Byte][Bit][0]; // Left margin
              CenterPerBit[Rank][Controller][Channel][Byte][Bit]  /= 2; // center per bit
              if (!ReadVGroup) {
                CenterPerBit[Rank][Controller][Channel][Byte][Bit] = CenterPerBit[Rank][Controller][Channel][Byte][Bit] + (INT32) ByteDelay;
                if (MinCenterPerBit > CenterPerBit[Rank][Controller][Channel][Byte][Bit]) {
                  MinCenterPerBit = CenterPerBit[Rank][Controller][Channel][Byte][Bit];
                }
              }
              AvgCenterPerByte[Rank][Controller][Channel][Byte]   += CenterPerBit[Rank][Controller][Channel][Byte][Bit]; // Sum all Bit centers
              MRC_DEBUG_MSG (Debug, DebugLevel, "[%3d %3d] ", MarginBit[Controller][Channel][Byte][Bit][0], MarginBit[Controller][Channel][Byte][Bit][1]);
            }
            if (!ReadVGroup) {
              GetSetVal = (INT64) MinCenterPerBit;
              if (Param == RdTN) {
                MrcGetSetStrobe (MrcData, Controller, Channel, Rank, Byte, RxDqsNDelay, ForceWriteCached, &GetSetVal);
              } else if (Param == RdTP) {
                MrcGetSetStrobe (MrcData, Controller, Channel, Rank, Byte, RxDqsPDelay, ForceWriteCached, &GetSetVal);
              } else if (Param == WrT) {
                MrcGetSetStrobe (MrcData, Controller, Channel, Rank, Byte, TxDqDelay,   ForceWriteCached, &GetSetVal);
              }
            }
            // Calculate Average Center per byte
            if (!ReadVGroup) {
              AvgCenterPerByte[Rank][Controller][Channel][Byte] = MinCenterPerBit;
            } else {
              AvgCenterPerByte[Rank][Controller][Channel][Byte] /= MAX_BITS;
            }
            MRC_DEBUG_MSG (Debug, DebugLevel, "  AvgCenter %d",  AvgCenterPerByte[Rank][Controller][Channel][Byte]);
          }

          MRC_DEBUG_MSG(Debug, MSG_LEVEL_NOTE, "\nPer Bit Offset:\n");
          // Calculate PBD Offset
          for (Byte = 0; Byte < Outputs->SdramCount; Byte++) {
            MRC_DEBUG_MSG(Debug, DebugLevel, "\nR%uMC%uC%uB%u:", Rank, Controller, Channel, Byte);
            if (!MrcByteExist (MrcData, Controller, Channel, Byte)) {
              continue;
            }
            NormOffset = MRC_INT32_MAX;
            for (Bit = 0; Bit < MAX_BITS; Bit++) {
              OffsetPerBit[Rank][Controller][Channel][Byte][Bit] = (AvgCenterPerByte[Rank][Controller][Channel][Byte] - CenterPerBit[Rank][Controller][Channel][Byte][Bit]); // Offset per bit
              if (!ReadVGroup) {
                OffsetPerBit[Rank][Controller][Channel][Byte][Bit] *= (-1); // change the direction for offset in case of PBD
              }
              if ((NormOffset > OffsetPerBit[Rank][Controller][Channel][Byte][Bit]) && !ReadVGroup) { // Only for PBD
                NormOffset = OffsetPerBit[Rank][Controller][Channel][Byte][Bit]; // Find the min off per Byte
              }
            } // for Bit

            // Align PBD within Byte to Zero: OffsetPerBit =( OffsetPerBit + |NormOffset| ) * ( PBDRatio )
            for (Bit = 0; Bit < MAX_BITS; Bit++) {
              if (!ReadVGroup) {
                // Align PBD within Byte to Zero
                //OffsetPerBit[Rank][Controller][Channel][Byte][Bit] += ABS(NormOffset); // PBD norm to zero
              } else {
                PBDRatio = RatioPerBit[Rank][Controller][Channel][Byte][Bit];
              }
              OffsetPerBit[Rank][Controller][Channel][Byte][Bit] *= PBDRatio;   // Offset per bit in PBD/VOC units
              OffsetPerBit[Rank][Controller][Channel][Byte][Bit] /= 100;
              MRC_DEBUG_MSG(Debug, MSG_LEVEL_NOTE, "[%3d] ", OffsetPerBit[Rank][Controller][Channel][Byte][Bit]);
            } // for Bit
            MRC_DEBUG_MSG(Debug, DebugLevel, "\n");
          } // for Byte
        } // for Channel
      } // for Controller
    } // Rank

    // Set PBD/VOC or Return Array (future option)
    MRC_DEBUG_MSG(Debug, DebugLevel, "\nSet Final %s Values", ((ParamList[ParamIndex] == RdV) || (ParamList[ParamIndex] == RdVBit)) ? "VOC" : "PBD");
    if (ParamList[ParamIndex] == RdV) {
      MRC_DEBUG_MSG(Debug, DebugLevel, " [ VocPcal ]");
    } else if (ParamList[ParamIndex] == RdVBit) {
      MRC_DEBUG_MSG(Debug, DebugLevel, " [ VocNcal ]");
    } else {
      MRC_DEBUG_MSG(Debug, DebugLevel, " %s", (ParamList[ParamIndex] == RdTP) ? "RxDqsPBitDelay" : (ParamList[ParamIndex] == RdTN) ? "RxDqsNBitDelay" : "TxDqBitDelay");
    }
    for (Rank = 0; Rank < MAX_RANK_IN_CHANNEL; Rank++) {
      for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
        for (Channel = 0; Channel < MaxChannels; Channel++) {
          if (!MrcRankExist (MrcData, Controller, Channel, Rank)) {
            continue;
          }
          MRC_DEBUG_MSG (Debug, DebugLevel, "\nR%u Mc%u C%u:", Rank, Controller, Channel);
          for (Byte = 0; Byte < Outputs->SdramCount; Byte++) {
            MRC_DEBUG_MSG (Debug, DebugLevel, "\nB%u: ", Byte);
            if (!MrcByteExist (MrcData, Controller, Channel, Byte)) {
              continue;
            }
            for (Bit = 0; Bit < MAX_BITS; Bit++) {
              GetSetVal = OffsetPerBit[Rank][Controller][Channel][Byte][Bit];
              if ((ParamList[ParamIndex] == RdV) || (ParamList[ParamIndex] == RdVBit)) {
                GetSetVal = RANGE (GetSetVal, -15, 16);
                // Translate from range [-15, 16] to [31, 0] --> y(x) = 16 - x
                if (Matched) {
                  VocPsal = VOC_DEFAULT - GetSetVal;
                  VocNsal = VOC_DEFAULT;              // @todo_adl add unmatched support
                } else {
                  if (ParamList[ParamIndex] == RdV) {
                    VocPsal = VOC_DEFAULT - GetSetVal;
                  } else {
                    VocNsal = VOC_DEFAULT - GetSetVal;
                  }
                }

                // Set VOC
                if (Matched) {
                  MRC_DEBUG_MSG (Debug, DebugLevel, "[%2d] ", (INT32) VocPsal);
                  MrcGetSetBit (MrcData, Controller, Channel, Rank, Byte, Bit, RxMatchedUnmatchedOffPcal, ForceWriteUncached, &VocPsal);
                } else {
                  if (ParamList[ParamIndex] == RdV) { // EVEN
                    //MRC_DEBUG_MSG(Debug, DebugLevel, "[%2d %2d] ", (INT32)VocPsal, (INT32)VocNsal);
                    MRC_DEBUG_MSG(Debug, DebugLevel, "[%2d] ", (INT32)VocPsal);
                    MrcGetSetBit (MrcData, Controller, Channel, Rank, Byte, Bit, RxMatchedUnmatchedOffPcal, ForceWriteUncached, &VocPsal);
                  } else { // ODD
                    MRC_DEBUG_MSG(Debug, DebugLevel, "[%2d] ", (INT32)VocNsal);
                    MrcGetSetBit (MrcData, Controller, Channel, Rank, Byte, Bit, RxUnmatchedOffNcal,        ForceWriteUncached, &VocNsal);
                  }
                }
              } else {
                MRC_DEBUG_MSG(Debug, DebugLevel, "[%2d] ", (INT32) GetSetVal);
                MrcGetSetBit (MrcData, Controller, Channel, Rank, Byte, Bit, PerBitGt, WriteOffsetCached, &GetSetVal);
              }
              // @todo_adl fill output array
            } // Bit
          } // Byte
        } // Channel
      } // Controller
    } // Rank
    if (Param == RdV) {
      // To make the VOC code take effective
      IssueRxReset (MrcData);
    }
    MRC_DEBUG_MSG (Debug, DebugLevel, "\n");

    // Average RxPBD across ranks for each bit, if we are at LockUI = 1.5
    if (((ParamList[ParamIndex] == RdTP) || (ParamList[ParamIndex] == RdTN)) &&
        ((Outputs->RxMode == MrcRxModeUnmatchedN) || (Outputs->RxMode == MrcRxModeUnmatchedP)) &&
        (SaveOutputs->UiLock == 0)
       ) {
      MRC_DEBUG_MSG (Debug, DebugLevel, "\nAlign PBD values across ranks in each bit\n");
      for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
        for (Channel = 0; Channel < MaxChannels; Channel++) {
          if (!MrcChannelExist (MrcData, Controller, Channel)) {
            continue;
          }
          MRC_DEBUG_MSG (Debug, DebugLevel, "\nMC%u C%u:", Controller, Channel);
          for (Byte = 0; Byte < Outputs->SdramCount; Byte++) {
            MRC_DEBUG_MSG (Debug, DebugLevel, "\nB%u: ", Byte);
            for (Bit = 0; Bit < MAX_BITS; Bit++) {
              RankCount  = 0;
              AvgPerRank = 0;
              for (Rank = 0; Rank < MAX_RANK_IN_CHANNEL; Rank++) {
                if (!MrcRankExist (MrcData, Controller, Channel, Rank)) {
                  continue;
                }
                RankCount++;
                AvgPerRank += OffsetPerBit[Rank][Controller][Channel][Byte][Bit];
              } // Rank
              AvgPerRank = (RankCount == 0) ? 0 : (AvgPerRank / RankCount);
              MRC_DEBUG_MSG(Debug, DebugLevel, "[%2d] ", AvgPerRank);
              GetSetVal = AvgPerRank;
              MrcGetSetBit (MrcData, Controller, Channel, MAX_RANK_IN_CHANNEL, Byte, Bit, PerBitGt, WriteToCache, &GetSetVal);
            } // Bit
          } // Byte
        } // Channel
      } // Controller
    }

    MRC_DEBUG_MSG (Debug, DebugLevel, "\n");
    MrcFlushRegisterCachedData (MrcData);
  } // Param Loop
  if (Param == RdV) {
    DQTimeCentering1D (MrcData, RdV, 0, 17, MRC_PRINTS_OFF, FALSE, 1);
  }

  if (Param == RdV) {
    // Restore InternalClocksOn
    MrcGetSetChStrb (MrcData, MAX_CONTROLLER, MAX_CHANNEL, MAX_SDRAM_IN_DIMM, GsmIocInternalClocksOn, WriteCached, &InternalClocksOnSave);
  }
  return Status;
}

/**
  Read the registers for the given Param into CR cache.
  This needs to be done before using ChangeMargin with certain parameters which use WriteOffsetUncached / ForceWriteOffsetUncached.

  @param[in]  MrcData - Global MRC data
  @param[in]  Param   - Margin parameter, see MRC_MarginTypes

  @retval None
**/
VOID
MrcReadParamIntoCache (
  IN  MrcParameters *const MrcData,
  IN  UINT32         const Param
  )
{
  MrcOutput *Outputs;
  UINT32    Controller;
  UINT32    Channel;
  UINT32    Rank;
  UINT32    MaxRank;
  UINT32    McChBitMask;
  UINT32    Byte;
  GSM_GT    Group;
  INT64     GetSetVal;

  Outputs = &MrcData->Outputs;

  switch (Param) {
    case RcvEnaX:
      Group   = RecEnDelay;
      MaxRank = MAX_RANK_IN_CHANNEL;
      break;

    case RdV:
      Group   = RxVref;
      MaxRank = MAX_RANK_IN_CHANNEL;
      break;

    default:
      Group   = GsmGtMax;
      MaxRank = 0;  // Nothing to do
      break;
  }

  McChBitMask = 0;
  for (Rank = 0; Rank < MaxRank; Rank++) {
    for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
      for (Channel = 0; Channel < Outputs->MaxChannels; Channel++) {
        if (!MrcRankExist(MrcData, Controller, Channel, Rank)) {
          continue;
        }
        // Keep track of populated channels
        McChBitMask |= (1 << ((Controller * Outputs->MaxChannels) + Channel));
        for (Byte = 0; Byte < Outputs->SdramCount; Byte++) {
          MrcGetSetStrobe (MrcData, Controller, Channel, Rank, Byte, Group, ReadCached, &GetSetVal);
        }
      }
    }
    if ((Param == RdV) && (McChBitMask == Outputs->McChBitMask)) {  // RdV is not per rank, so we are done once all controllers / channels were read on any rank
      return;
    }
  }
}


/**

  This routine sweeps any signal and returns the edges where the signal transitions from pass to fail.

  @param[in]  MrcData                     Host structure for all data related to MMRC.
  @param[in]  ChannelInput                Current Channel being examined.
  @param[in]  Rank                        Rank to sweep.
  @param[in]  Dim1Index                   Signal type to sweep (i.e. RxDqsDelay, TxVref, CmdGrp0, etc).
  @param[in]  VocSweep                    VOC Sweep or not.
  @param[in]  Dim1Start                   Starting value (per channel/knob/strobe/edge) for both the
                                          high and low edges.
  @param[in]  Dim1Min                     The minimum value to allow the sweep to hit before stopping.
  @param[in]  Dim1Max                     The maximum value to allow the sweep to hit before stopping.
  @param[in]  Dim1Step                    Step size for sweep. Set to 1 for maximum detail or higher for speed improvements
                                          at the expense of accuracy.
  @param[in]  FullSweep                   THIS IS NOT CURRENTLY IMPLEMENTED. If TRUE, sweep the entire range from min to max.
  @param[in]  SkipInitialValueProgramming When TRUE, do not program initial low edge starting values or high edge
                                          starting values. This is useful when you want to margin a signal starting
                                          at its current value or when margining a group of signals that need to stay
                                          tethered together. For example, in CMD training if CmdGrp0 and CmdGrp1 need
                                          to sweep together but be programmed to different values (i.e. tethered),
                                          this would be set the TRUE so the two groups aren't programmed to an identical'
                                          starting value. If the user wants to skip the initial value programming,
                                          they MUST assign Dim1Start[][][][] to the current register settings upon entry
                                          to this routine.
  @param[in]       NumberElements         How many elements are we sweeping? Typically, MAX_SDRAM_IN_DIMM for strobe based delays or 1
                                          for CMD signals or others which aren't strobe-based.
  @param[in]       ExecuteTest            Pointer to the point test function which evaluates pass/fail for the current value
                                          of the signal.
  @param[in, out]  Results                Results array returning the high/low edges per channel/knob/strobe.
  @param[in        HeaderLabel            Label to print if debugging is turned on. If "S" then the header will be a strobe
                                          header like S00   S01   S02, etc.
  @retval          mrcSuccess

**/
MrcStatus
Create1DSweepLastPass(
  IN MrcParameters *const  MrcData,
  IN      UINT8            ChannelInput,
  IN      UINT8            Rank,
  IN      GSM_GT           Dim1Index,
  IN      BOOLEAN          VocSweep,
  IN      UINT16           Dim1Start[MAX_CONTROLLER][MAX_CHANNEL][MAX_SDRAM_IN_DIMM][2],
  IN      UINT16           Dim1Min[MAX_CONTROLLER][MAX_CHANNEL][MAX_SDRAM_IN_DIMM],
  IN      UINT16           Dim1Max[MAX_CONTROLLER][MAX_CHANNEL][MAX_SDRAM_IN_DIMM],
  IN      UINT16           Dim1Step,
  IN      BOOLEAN          FullSweep,
  IN      BOOLEAN          SkipInitialValueProgramming,
  IN      BOOLEAN          PerBitSweep,
  IN      UINT8            NumberElements,
  IN      MrcStatus        ExecuteTest(MrcParameters *, GSM_GT Group, UINT8, UINT8 PassFail[MAX_CONTROLLER][MAX_CHANNEL][MAX_SDRAM_IN_DIMM][MAX_BITS][3], BOOLEAN VocSweepFlag),
  IN OUT  UINT16           Results[MAX_CONTROLLER][MAX_CHANNEL][MAX_SDRAM_IN_DIMM][MAX_BITS][2],
  IN      UINT8            *HeaderLabel,
  IN      UINT8            DitherVal
)
{
  UINT8     LowHighFlag[MAX_CONTROLLER][MAX_CHANNEL][MAX_SDRAM_IN_DIMM][MAX_BITS];
  MrcDebug  *Debug;
  UINT8     MaximumBits;
  UINT8     Element;
  UINT8     Controller;
  MrcOutput *Outputs;
  BOOLEAN   EdgeFound[MAX_CONTROLLER][MAX_CHANNEL];
  UINT8     Bits;
  UINT8     ElementAllDone;
  BOOLEAN   ElementDone[MAX_CONTROLLER][MAX_CHANNEL][MAX_SDRAM_IN_DIMM][MAX_BITS];
  UINT32    Value[MAX_CONTROLLER][MAX_CHANNEL][MAX_SDRAM_IN_DIMM];
  const MRC_FUNCTION    *MrcCall;
  UINT8     PassFail[MAX_CONTROLLER][MAX_CHANNEL][MAX_SDRAM_IN_DIMM][MAX_BITS][3];
  UINT8     Phase[MAX_CONTROLLER][MAX_CHANNEL][MAX_SDRAM_IN_DIMM][MAX_BITS];
  UINT8     TestFinished;
  BOOLEAN   ElementBitDone;
  BOOLEAN   ShareBetweenStrobeGroups = FALSE;
  BOOLEAN   ShareValHasBeenSet = FALSE;
  BOOLEAN   SwitchDirection;
  INT64     GetSetValueSigned = 0;
  UINT32    GetSetValue = 0;
  UINT32    TempCompRcompOdtUp = 0;
  UINT32    TempCompRcompOdtDn = 0;
  UINT8     CachedVOdtCode = 0;
  UINT8     Bits2;
  UINT8     st;
  UINT32    MaxVal;
  UINT16    HighsideOffsetAdjust;
  UINT8     Controller1;
  UINT8     Channel1;
  UINT8     PrintHighLabel;
  UINT8     Channel;

  Outputs = &MrcData->Outputs;
  MrcCall = MrcData->Inputs.Call.Func;
  Debug = &Outputs->Debug;
  MaxVal = 0;
  MrcCall->MrcSetMem((UINT8 *)Value, MAX_CONTROLLER * MAX_CHANNEL* MAX_SDRAM_IN_DIMM * sizeof(UINT32), 0);
  PrintHighLabel = 0;

  if (PerBitSweep == TRUE) {
    MaximumBits = MAX_BITS;
  } else {
    MaximumBits = 1;
  }
  //
  // Print out the header.
  //
  MRC_DEBUG_MSG(Debug, MSG_LEVEL_NOTE, "\n      Channel Rank Strobe (CRS)\n%02s    ", HeaderLabel);
  for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
    for (Channel = 0; Channel < Outputs->MaxChannels; Channel++) {
      if (!(MrcRankExist(MrcData, Controller, Channel, Rank))) {
        continue;
      }
      for (Element = 0; Element < NumberElements; Element++) {
        MRC_DEBUG_MSG(Debug, MSG_LEVEL_NOTE, "%d%d%d    ", Controller*MAX_CHANNEL + Channel, Rank, Element);
      }
    }
  }
  MRC_DEBUG_MSG(Debug, MSG_LEVEL_NOTE, "\n%s ", "LOW");

  for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
    for (Channel = 0; Channel < Outputs->MaxChannels; Channel++) {
      if (!(MrcRankExist(MrcData, Controller, Channel, Rank))) {
        continue;
      }
      for (Element = 0; Element < NumberElements; Element++) {
        MRC_DEBUG_MSG(Debug, MSG_LEVEL_NOTE, "-------");
      }
    }
  }
  MRC_DEBUG_MSG(Debug, MSG_LEVEL_NOTE, "\n");

  for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
    for (Channel = 0; Channel < Outputs->MaxChannels; Channel++) {
      if (!(MrcRankExist(MrcData, Controller, Channel, Rank))) {
        continue;
      }
      EdgeFound[Controller][Channel] = FALSE;

      for (Element = 0; Element < NumberElements; Element++) {
        ElementAllDone = TRUE;
        for (Bits = 0; Bits < MaximumBits; Bits++) {
          if (Dim1Start[Controller][Channel][Element][ONEDS_LOW] >= Dim1Min[Controller][Channel][Element]) {
            LowHighFlag[Controller][Channel][Element][Bits] = ONEDS_LOW;
            Results[Controller][Channel][Element][Bits][ONEDS_LOW] = Dim1Start[Controller][Channel][Element][ONEDS_LOW];
            Results[Controller][Channel][Element][Bits][ONEDS_HIGH] = 0;
            ElementDone[Controller][Channel][Element][Bits] = FALSE;
            ElementAllDone = FALSE;
          } else if (Dim1Start[Controller][Channel][Element][ONEDS_HIGH] <= Dim1Max[Controller][Channel][Element]) {
            LowHighFlag[Controller][Channel][Element][Bits] = ONEDS_HIGH;
            Results[Controller][Channel][Element][Bits][ONEDS_LOW] = 0;
            Results[Controller][Channel][Element][Bits][ONEDS_HIGH] = Dim1Start[Controller][Channel][Element][ONEDS_HIGH];
            ElementDone[Controller][Channel][Element][Bits] = FALSE;
            ElementAllDone = FALSE;
          } else {
            LowHighFlag[Controller][Channel][Element][Bits] = ONEDS_HIGH;
            ElementDone[Controller][Channel][Element][Bits] = TRUE;
          }
        }
        //
        // If the user wants to skip the initial value programming, they MUST assign Dim1Start[][][][]
        // to the current register settings upon entry to this routine.
        //
        if (!SkipInitialValueProgramming && ElementAllDone == FALSE) {
          //
          // The value should be set to the start value which is based on the LowHighFlag.
          //
          Value[Controller][Channel][Element] = Dim1Start[Controller][Channel][Element][LowHighFlag[Controller][Channel][Element][0]];
          GetSetValue = Value[Controller][Channel][Element];
          GetSetValueSigned = (INT64)(INT32)GetSetValue;

          if (Dim1Index == CompRcompOdtUpPerStrobe) {
            //
            // Update the value for VODT
            //
            ProgramOdtUpDnStatic(MrcData, Controller, Channel, Element, GetSetValue);
          } else if ((Dim1Index == RxMatchedUnmatchedOffPcal) || (Dim1Index == RxUnmatchedOffNcal)) {
            //
            // Update the value for VOC
            //
            MrcGetSetBit(MrcData, Controller, Channel, Rank, Element, 0, Dim1Index, WriteCached, &GetSetValueSigned);
          } else {
            MrcGetSetStrobe(MrcData, Controller, Channel, Rank, Element, Dim1Index, WriteCached, &GetSetValueSigned);
          }
        }
      }
      if ((Dim1Index == RxMatchedUnmatchedOffPcal) || (Dim1Index == RxUnmatchedOffNcal)) {
        //
        // RxReset after updating the VOC
        //
        IssueRxResetMcCh (MrcData, Controller, Channel);
      }
    } // Channel loop ...
  }
  //
  // Initialize Pass/Fail flags to fail before running the sweep for the low or high edge.
  //
  MrcCall->MrcSetMem((UINT8 *)PassFail, MAX_CONTROLLER * MAX_CHANNEL * MAX_SDRAM_IN_DIMM * MAX_BITS * 3 * sizeof(UINT8),
                     ONEDS_RESULTS_INIT);
  MrcCall->MrcSetMem((UINT8 *)Phase, MAX_CONTROLLER * MAX_CHANNEL * MAX_SDRAM_IN_DIMM * MAX_BITS * sizeof(UINT8),
                     0); // Set phases for each element to SEARCH_FOR_EDGE.
  MrcCall->MrcSetMem((UINT8 *)ElementDone, MAX_CONTROLLER * MAX_CHANNEL* MAX_SDRAM_IN_DIMM * MAX_BITS * sizeof(UINT8), 0);

  TestFinished = FALSE;
  //
  // First find the low edge then find the high edge.
  //
  while (!TestFinished) {
    for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
      for (Channel = 0; Channel < Outputs->MaxChannels; Channel++) {
        if (!(MrcRankExist(MrcData, Controller, Channel, Rank))) {
          continue;
        }
        ShareValHasBeenSet = FALSE;
        for (Element = 0; Element < NumberElements; Element++) {
          ElementBitDone = TRUE;
          for (Bits = 0; Bits < MaximumBits; Bits++) {
            ElementBitDone &= ElementDone[Controller][Channel][Element][Bits];
          }
          if ((ShareBetweenStrobeGroups == TRUE) && (ShareValHasBeenSet == TRUE)) continue;
          if (!ElementBitDone && (Value[Controller][Channel][Element] != 0)) {

            for (Bits = 0; Bits < 1; Bits++) {
              //
              // Program bit 0 and will apply to the other bits for VOC when running the ExecuteTest.
              //
              if ((PassFail[Controller][Channel][Element][Bits][ONEDS_CURRENT_SAMPLE] != ONEDS_RESULTS_INIT) ||
                  (EdgeFound[Controller][Channel] && SkipInitialValueProgramming)) {
                GetSetValue = Value[Controller][Channel][Element];

                GetSetValueSigned = (INT64)(INT32)GetSetValue;

                if (Dim1Index == CompRcompOdtUpPerStrobe) {

                  ReadWriteRcompRegPerStrobe (MrcData, Controller, Channel, Element, CompRcompOdtUpPerStrobe, ReadUncached,
                                              &TempCompRcompOdtUp );

                  ReadWriteRcompRegPerStrobe (MrcData, Controller, Channel, Element, CompRcompOdtDnPerStrobe, ReadUncached,
                                              &TempCompRcompOdtDn );

                  if (TempCompRcompOdtUp == 32) {
                    CachedVOdtCode = 63 - (TempCompRcompOdtDn & 0x3F);
                  } else {
                    CachedVOdtCode = (TempCompRcompOdtUp & 0x3F);
                  }
                  //
                  // Get the abosolute value
                  //
                  GetSetValue = CachedVOdtCode + GetSetValue;

                  ProgramOdtUpDnStatic(MrcData, Controller, Channel, Element, GetSetValue);
                } else if ((Dim1Index == RxMatchedUnmatchedOffPcal) || (Dim1Index == RxUnmatchedOffNcal)) {
                  MrcGetSetBit(MrcData, Controller, Channel, Rank, Element, 0, Dim1Index, WriteOffsetCached, &GetSetValueSigned);
                } else {
                  MrcGetSetStrobe(MrcData, Controller, Channel, Rank, Element, Dim1Index, WriteOffsetCached, &GetSetValueSigned);
                }
                if (ShareBetweenStrobeGroups == TRUE) {
                  ShareValHasBeenSet = TRUE;
                }
              }
            }
          }
        } // End: for (Element)
        if ((Dim1Index == RxMatchedUnmatchedOffPcal) || (Dim1Index == RxUnmatchedOffNcal)) {
          IssueRxResetMcCh (MrcData, Controller, Channel);
        }
      } // End: for (Channel
    } // End: for (Controller
    //
    // Run the test
    //
    ExecuteTest(MrcData, Dim1Index, NumberElements, PassFail, VocSweep);
    //
    // Look at all tested elements.
    //
    TestFinished = TRUE;

    for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
      for (Channel = 0; Channel < MAX_CHANNEL; Channel++) {
        if (!(MrcRankExist(MrcData, Controller, Channel, Rank))) {
          continue;
        }
        //
        // Print out the result from the test.
        //
        MRC_DEBUG_MSG(Debug, MSG_LEVEL_NOTE, "\nMC%d Ch%d ", Controller, Channel);

        for (Element = 0; Element < NumberElements; Element++) {
          for (Bits = 0; Bits < MaximumBits; Bits++) {
            MRC_DEBUG_MSG(Debug, MSG_LEVEL_NOTE, "%04d:%s ",
                          Results[Controller][Channel][Element][Bits][LowHighFlag[Controller][Channel][Element][Bits]],
                          ResultsString[PassFail[Controller][Channel][Element][Bits][ONEDS_CURRENT_SAMPLE]]);
          } //for Bits
        } // End: for (Element

        for (Element = 0; Element < NumberElements; Element++) {
          Value[Controller][Channel][Element] = 0;
          for (Bits = 0; Bits < MaximumBits; Bits++) {
            EdgeFound[Controller][Channel] = TRUE;
            //
            // If we are on the first pass OR the current and previous sample match, it means we haven't found an edge.
            //
            if (Phase[Controller][Channel][Element][Bits] == 0 && !FoundEdge(PassFail[Controller][Channel][Element][Bits], 0)
                && !ElementDone[Controller][Channel][Element][Bits]) {
              //
              // If we made it here it means at least 1 element/bits hasn't found an edge.
              //
              EdgeFound[Controller][Channel] = FALSE;
              //
              // Now either increase or decrease the results array in preparation for the next pass.
              // If we are on the ONEDS_LOW side and we detect a failure, increase.
              // If we are on the ONEDS_LOW side and we detect a pass, decrease.
              // If we are on the ONEDS_HIGH side and we detect a failure, decrease.
              // If we are on the ONEDS_HIGH side and we detect a pass, increase.
              //
              if ((LowHighFlag[Controller][Channel][Element][Bits] == ONEDS_LOW && PassFail[Controller][Channel][Element][Bits][ONEDS_CURRENT_SAMPLE] == ONEDS_RESULTS_PASS) ||
                (LowHighFlag[Controller][Channel][Element][Bits] == ONEDS_HIGH && PassFail[Controller][Channel][Element][Bits][ONEDS_CURRENT_SAMPLE] == ONEDS_RESULTS_FAIL)) {
                if (Results[Controller][Channel][Element][Bits][LowHighFlag[Controller][Channel][Element][Bits]] - Dim1Step < Dim1Min[Controller][Channel][Element]) {
                  //
                  // We've reached a limit. Mark it as an edge.
                  //
                  EdgeFound[Controller][Channel] = TRUE;
                } else {
                  Results[Controller][Channel][Element][Bits][LowHighFlag[Controller][Channel][Element][Bits]] -= Dim1Step;

                  Value[Controller][Channel][Element] = 0 - Dim1Step;

                  TestFinished = FALSE;
                }
              } else {
                if ((Results[Controller][Channel][Element][Bits][LowHighFlag[Controller][Channel][Element][Bits]] + Dim1Step >
                     Dim1Max[Controller][Channel][Element]) ||
                    (Results[Controller][Channel][Element][Bits][LowHighFlag[Controller][Channel][Element][Bits]] + Dim1Step <
                     Dim1Min[Controller][Channel][Element])
                   ) {
                  //
                  // We've reached a limit. Mark it as an edge.
                  //
                  EdgeFound[Controller][Channel] = TRUE;
                } else {
                  Results[Controller][Channel][Element][Bits][LowHighFlag[Controller][Channel][Element][Bits]] += Dim1Step;
                  Value[Controller][Channel][Element] = Dim1Step;
                  TestFinished = FALSE;
                }
              }
            }

            if (Phase[Controller][Channel][Element][Bits] == 0 && EdgeFound[Controller][Channel]) {
              //
              // We have found an edge or reached a min/max limit of the sweep.
              // Move back to the last passing element if this last sample was a failure.
              //
              if (!ElementDone[Controller][Channel][Element][Bits] && PassFail[Controller][Channel][Element][Bits][ONEDS_CURRENT_SAMPLE] == ONEDS_RESULTS_FAIL) {
                if (LowHighFlag[Controller][Channel][Element][Bits] == ONEDS_LOW) {
                  //
                  // Make sure we don't exceed the max.
                  //
                  if (Results[Controller][Channel][Element][Bits][LowHighFlag[Controller][Channel][Element][Bits]] + Dim1Step <= Dim1Max[Controller][Channel][Element]) {
                    Results[Controller][Channel][Element][Bits][ONEDS_LOW] += Dim1Step;
                    PassFail[Controller][Channel][Element][Bits][ONEDS_CURRENT_SAMPLE] = ONEDS_RESULTS_PASS;
                  }
                } else {
                  //
                  // Make sure we don't exceed the min.
                  //
                  if (Results[Controller][Channel][Element][Bits][LowHighFlag[Controller][Channel][Element][Bits]] - Dim1Step >= Dim1Min[Controller][Channel][Element]) {
                    Results[Controller][Channel][Element][Bits][ONEDS_HIGH] -= Dim1Step;
                    PassFail[Controller][Channel][Element][Bits][ONEDS_CURRENT_SAMPLE] = ONEDS_RESULTS_PASS;
                  }
                }
              }
              //
              // Change the phase to the dithering phase.
              //
              if (DitherVal == 0) {
                Phase[Controller][Channel][Element][Bits] = 3; // Skip Dithering.
              } else {
                Phase[Controller][Channel][Element][Bits] = 1; // Dithering.
                if (LowHighFlag[Controller][Channel][Element][Bits] == ONEDS_LOW) {
                  GetSetValue = Results[Controller][Channel][Element][Bits][ONEDS_LOW];
                } else {
                  GetSetValue = Results[Controller][Channel][Element][Bits][ONEDS_HIGH];
                }
                GetSetValueSigned = (INT64)GetSetValue;

                if (Dim1Index == CompRcompOdtUpPerStrobe) {
                  ProgramOdtUpDnStatic(MrcData, Controller, Channel, Element, GetSetValue);
                } else if ((Dim1Index == RxMatchedUnmatchedOffPcal) || (Dim1Index == RxUnmatchedOffNcal)) {
                  MrcGetSetBit(MrcData, Controller, Channel, Rank, Element, 0, Dim1Index, ForceWriteCached, &GetSetValueSigned);
                } else {
                  MrcGetSetStrobe(MrcData, Controller, Channel, Rank, Element, Dim1Index, ForceWriteCached, &GetSetValueSigned);
                }
              }
            }
            //
            // If in the dithering phase, then we are in a position of passing... margin until "DiterVal" consecutive Passes.
            //
            if (Phase[Controller][Channel][Element][Bits] == 1) {
              // Now either increase or decrease the results array in preparation for the next pass.
              // If we are on the ONEDS_LOW side and we detect a failure, increase.
              // If we are on the ONEDS_LOW side and we detect a pass, decrease.
              // If we are on the ONEDS_HIGH side and we detect a failure, decrease.
              // If we are on the ONEDS_HIGH side and we detect a pass, increase.
              //
              if (PassFail[Controller][Channel][Element][Bits][ONEDS_EDGE_SAMPLES] == DitherVal) {
                Phase[Controller][Channel][Element][Bits] = 2;
              } else {
                if (PassFail[Controller][Channel][Element][Bits][ONEDS_CURRENT_SAMPLE] == ONEDS_RESULTS_PASS) {
                  PassFail[Controller][Channel][Element][Bits][ONEDS_EDGE_SAMPLES]++;
                } else {
                  PassFail[Controller][Channel][Element][Bits][ONEDS_EDGE_SAMPLES] = 0;
                }
                if (LowHighFlag[Controller][Channel][Element][Bits] == ONEDS_LOW) {
                  if (Results[Controller][Channel][Element][Bits][ONEDS_LOW] + 1 > Dim1Max[Controller][Channel][Element]) {
                    //
                    // We've reached a limit. Go to next phase.
                    //
                    Phase[Controller][Channel][Element][Bits] = 2;
                  } else {
                    Results[Controller][Channel][Element][Bits][ONEDS_LOW] += 1;
                    Value[Controller][Channel][Element] += 1;
                    TestFinished = FALSE;
                  }
                } else {
                  if (Results[Controller][Channel][Element][Bits][ONEDS_HIGH] - 1 < Dim1Min[Controller][Channel][Element]) {
                    //
                    // We've reached a limit. G oto next phase.
                    //
                    Phase[Controller][Channel][Element][Bits] = 2;
                  } else {
                    Results[Controller][Channel][Element][Bits][LowHighFlag[Controller][Channel][Element][Bits]] -= 1;
                    Value[Controller][Channel][Element] -= 1;
                    TestFinished = FALSE;
                  }
                }
              }
            }
            //
            // Found the dithering edge.. need to subtract the amount to get back to the original passing region.
            //
            if (Phase[Controller][Channel][Element][Bits] == 2) {
              //
              // The last sample was as PASS so we need to back up to the first PASS value, taking into account
              // dithering. If we had a DitherVal of 3 and sampled F F P P P, we need to subtract 2 to get back
              // to the first P sample. However if we have 0 EDGE_SAMPLES it could mean a situation where our first
              // sample was at the limit. For example, we sample a PASS at the lower limit delay of 0. In this case
              // we don't want to do any sort of backing up.
              //
              Phase[Controller][Channel][Element][Bits] = 3;
            }
            if (Phase[Controller][Channel][Element][Bits] == 3) {
              //
              // If the high edge has been found, this element is done.
              //
              if (LowHighFlag[Controller][Channel][Element][Bits] == ONEDS_HIGH) {
                //
                // At the completion of the element edges, check if there was no low side.. if not, then assign
                // the high value to the low value.
                //
                if (Dim1Start[Controller][Channel][Element][ONEDS_LOW] < Dim1Min[Controller][Channel][Element]) {
                  Results[Controller][Channel][Element][Bits][ONEDS_LOW] = Results[Controller][Channel][Element][Bits][ONEDS_HIGH];
                }
                //
                // At the completion of the element edges, check if there was no high side.. if not, then assign
                // the low value to the high value.
                //
                if (Dim1Start[Controller][Channel][Element][ONEDS_HIGH] > Dim1Max[Controller][Channel][Element]) {
                  Results[Controller][Channel][Element][Bits][ONEDS_HIGH] = Results[Controller][Channel][Element][Bits][ONEDS_LOW];
                }
                ElementDone[Controller][Channel][Element][Bits] = TRUE;
              } else if (LowHighFlag[Controller][Channel][Element][Bits] == ONEDS_LOW
                         && Dim1Start[Controller][Channel][Element][ONEDS_HIGH] > Dim1Max[Controller][Channel][Element]) {
                ElementDone[Controller][Channel][Element][Bits] = TRUE;
                Results[Controller][Channel][Element][Bits][ONEDS_HIGH] = Results[Controller][Channel][Element][Bits][ONEDS_LOW];
              } else {
                //
                // Switch directions to find the high edge.
                //
                Phase[Controller][Channel][Element][Bits] = 4;
                PrintHighLabel = 1;
              }
            }
            if (Phase[Controller][Channel][Element][Bits] == 4) {
              SwitchDirection = TRUE;
              for (st = 0; st < NumberElements; st++) {
                for (Bits2 = 0; Bits2 < MaximumBits; Bits2++) {
                  if (Phase[Controller][Channel][st][Bits2] != 4) {
                    SwitchDirection = FALSE;
                  }
                }
              }
              if (SwitchDirection == TRUE) {
                MRC_DEBUG_MSG(Debug, MSG_LEVEL_NOTE, "%s", "HIGH");

                for (Controller1 = 0; Controller1 < MAX_CONTROLLER; Controller1++) {
                  for (Channel1 = 0; Channel1 < Outputs->MaxChannels; Channel1++) {
                    if (!(MrcRankExist(MrcData, Controller1, Channel1, Rank))) {
                      continue;
                    }
                    for (Element = 0; Element < NumberElements; Element++) {
                      MRC_DEBUG_MSG(Debug, MSG_LEVEL_NOTE, "-------");
                    }
                  }
                  MRC_DEBUG_MSG(Debug, MSG_LEVEL_NOTE, "%s", "\n");
                }

                MrcCall->MrcSetMem((UINT8 *)&(LowHighFlag[Controller][Channel]), sizeof(UINT8)* MAX_SDRAM_IN_DIMM * MAX_BITS, ONEDS_HIGH);
                MrcCall->MrcSetMem((UINT8 *)&(Phase[Controller][Channel]), sizeof(UINT8)* MAX_SDRAM_IN_DIMM * MAX_BITS, 0);

                for (st = 0; st < NumberElements; st++) {
                  for (Bits2 = 0; Bits2 < MaximumBits; Bits2++) {
                    if (Dim1Start[Controller][Channel][st][ONEDS_HIGH] > Results[Controller][Channel][st][Bits2][ONEDS_LOW]) {
                      Results[Controller][Channel][st][Bits2][ONEDS_HIGH] = Dim1Start[Controller][Channel][st][ONEDS_HIGH];
                    } else {
                      Results[Controller][Channel][st][Bits2][ONEDS_HIGH] = Results[Controller][Channel][st][Bits2][ONEDS_LOW];
                    }
                    //
                    //  This makes sure that we pick one starting value for the highside sweep, across all the bits within one Element/Strobe
                    //  Previously it could pick different start values across bits, in some unusual corner cases, and this makes no sense
                    //  because the hardware only has one physical margin param across the bits in one strobe.  Sweep ends up out of sync
                    //  with what is really happening, and incorrect results are reported
                    //
                    if (MaxVal < Results[Controller][Channel][st][Bits2][ONEDS_HIGH] || Bits2 == 0) {
                      MaxVal = Results[Controller][Channel][st][Bits2][ONEDS_HIGH];
                    }
                    //
                    //   Add this to code below.  Code added the functionality where CMD/CTL/.. sweep, when switching
                    //   directions from loside to hiside sweep, will properly place CMD timing back at start sweep position for
                    //   for hiside.   Before completing this iteration of while !TestFinished, 'Value' has to contain the delta between
                    //   lowside sweep final point and hiside starting point, so that GetSet( OFFSET, Value ) will correctly offset CMD to
                    //   the hiside start value (This GetSet is at beginning of next while loop iteration).
                    //   Below code fixes one corner case, that if lowside sweep started with a pass but subsequently saw a fail while margining,
                    //   the 'PHASE 0' section above will adjust Results[] to the last past value, but the HW is still at that first fail
                    //   position, meaning we need to do an adjustment here so that HW finally gets put at the proper hiside start position.
                    //
                    HighsideOffsetAdjust = 0;
                    if (Value[Controller][Channel][st] > 0) {
                      // Note that if any bit hit the PASS=>FAIL condition on lowside sweep, during margining, the Value[..] will increment by Dim1Step
                      // This is enforced in the PHASE 0 logic above, and is unique for this PASS=>FAIL during margining scenario
                      // All other scenarios will have Value[..] == 0.  These other scenarios don't need this re-adjustment
                      HighsideOffsetAdjust = Dim1Step;
                    }
                    //
                    // calculate the "offset" value from the ONEDS_HIGH side start to the last ONEDS_LOW side edge.
                    //
                    Value[Controller][Channel][st] = Results[Controller][Channel][st][Bits2][ONEDS_HIGH] -
                                                     Results[Controller][Channel][st][Bits2][ONEDS_LOW] + HighsideOffsetAdjust;
                    //
                    // Program ONEDS_HIGH side initial values. If user opts to skip this step, the ONEDS_HIGH side sweep
                    // will start where the ONEDS_LOW side ended.
                    //
                    TestFinished = FALSE;
                    MrcCall->MrcSetMem((UINT8 *)&PassFail[Controller][Channel][st][Bits2][0], 3, ONEDS_RESULTS_INIT);
                  }    //  Bits loop *within* SwITCHDIRECTION==TRUE
                  for (Bits2 = 0; Bits2 < MaximumBits; Bits2++) {
                    Results[Controller][Channel][st][Bits2][ONEDS_HIGH] = (UINT16)MaxVal;
                    if (SkipInitialValueProgramming) {
                      Results[Controller][Channel][st][Bits2][ONEDS_HIGH] = Results[Controller][Channel][st][Bits2][ONEDS_LOW] + Dim1Step;
                      Value[Controller][Channel][st] = Dim1Step;
                      if (Results[Controller][Channel][st][Bits2][ONEDS_HIGH] > Dim1Max[Controller][Channel][st]) {
                        Results[Controller][Channel][st][Bits2][ONEDS_HIGH] -= 1;
                        Value[Controller][Channel][st] = Dim1Step - 1;
                      }
                    }
                  }
                  if (!SkipInitialValueProgramming) {
                    GetSetValue = Value[Controller][Channel][st];
                    GetSetValue = Dim1Start[Controller][Channel][st][ONEDS_HIGH];

                    GetSetValueSigned = (INT64)GetSetValue;
                    if (Dim1Index == CompRcompOdtUpPerStrobe) {
                      ProgramOdtUpDnStatic(MrcData, Controller, Channel, Element, GetSetValue);
                    } else if ((Dim1Index == RxMatchedUnmatchedOffPcal) || (Dim1Index == RxUnmatchedOffNcal)) {
                      MrcGetSetBit(MrcData, Controller, Channel, Rank, st, 0, Dim1Index, WriteCached, &GetSetValueSigned);
                    } else {
                      MrcGetSet(
                        MrcData,
                        MRC_IGNORE_ARG,
                        Controller,
                        Channel,
                        MRC_IGNORE_ARG,
                        Rank,
                        st,
                        MRC_IGNORE_ARG,
                        MRC_IGNORE_ARG,
                        DdrLevel,
                        Dim1Index,
                        WriteCached,
                        &GetSetValueSigned
                      );
                    }
                  }
                }
                Element = NumberElements;
              }
            }
          }  //Bits
        } // Element loop ...
        if ((Dim1Index == RxMatchedUnmatchedOffPcal) || (Dim1Index == RxUnmatchedOffNcal)) {
          IssueRxResetMcCh (MrcData, Controller, Channel);
        }
      } // End: for (Channel
    } // End: for (Controller
  } // End While

  if (PrintHighLabel == 0) {
    MRC_DEBUG_MSG(Debug, MSG_LEVEL_NOTE, "%s", "HIGH");

    for (Controller1 = 0; Controller1 < MAX_CONTROLLER; Controller1++) {
      for (Channel1 = 0; Channel1 < Outputs->MaxChannels; Channel1++) {
        if (!(MrcRankExist(MrcData, Controller1, Channel1, Rank))) {
          continue;
        }
        for (Element = 0; Element < NumberElements; Element++) {
          MRC_DEBUG_MSG(Debug, MSG_LEVEL_NOTE, "-------");
        }
      }
    }
  }
  MRC_DEBUG_MSG(Debug, MSG_LEVEL_NOTE, "\n%s", "DONE");

  for (Controller1 = 0; Controller1 < MAX_CONTROLLER; Controller1++) {
    for (Channel1 = 0; Channel1 < Outputs->MaxChannels; Channel1++) {
      if (!(MrcRankExist(MrcData, Controller1, Channel1, Rank))) {
        continue;
      }
      for (Element = 0; Element < NumberElements; Element++) {
        MRC_DEBUG_MSG(Debug, MSG_LEVEL_NOTE, "-------");
      }
    }
  }
  MRC_DEBUG_MSG(Debug, MSG_LEVEL_NOTE, "\n");
  return mrcSuccess;
}

/**
Compares the initial sample with the current sample. If they don't match, it increases a counter by 1. When this
counter equals DitherVal it means we've found an edge. Higher dither values means we look for a stronger edge.
For example if DitherVal is 3 and the first sample if FAIL, it means this routine will return TRUE after sampling
3 PASS values in a row.

@param[in] PassFail      Array of samples performed by the sweep function.
@param[in] DitherVal     The number of successive samples that need to be either PASS or FAIL before this routine
                         considers an edge to be found. For example, if the first sample is FAIL, the sweep will
                         look to read X number of PASS values in a row before it considers this a FAIL to PASS
                         edge. X = DitherVal.
@retval    TRUE/FALSE    Is the edge found? TRUE: Found, FALSE: Not Found
**/
BOOLEAN
FoundEdge (
  IN        UINT8     PassFail[3],
  IN        UINT8     DitherVal
  )
{
  //
  // If this is the first time in this routine, set the edge count to 0. This
  // count will increase when successive samples equal the same value. For example,
  // if we are sweeping and get FAIL, FAIL, PASS, PassFail[EDGE_SAMPLES] would be 1
  // because we've detected 1 sample in a row that's different from the first sample.
  // If we sample FAIL, FAIL, PASS, PASS, PASS, PassFail[EDGE_SAMPLES] = 3.
  //
  if (PassFail[ONEDS_EDGE_SAMPLES] == ONEDS_RESULTS_INIT) {
    PassFail[ONEDS_EDGE_SAMPLES] = 0;
    //
    // Save the first sample for comparison later.
    //
    PassFail[ONEDS_FIRST_SAMPLE] = PassFail[ONEDS_CURRENT_SAMPLE];
    return FALSE;
  }
  //
  // If the current sample equals the first sample of this sweep, then we haven't
  // found an edge and we need to re-init the edge count to 0.
  //
  if (PassFail[ONEDS_FIRST_SAMPLE] == PassFail[ONEDS_CURRENT_SAMPLE]) {
    PassFail[ONEDS_EDGE_SAMPLES] = 0;
    return FALSE;
  }
  //
  // If the current sample is different from the first sample, we need to increase the
  // edge count. If this count equals the dither value, it means we've read X successive
  // samples in a row which a DIFFERENT from the first sample, where X = DitherVal. This
  // means we've found an edge.
  //
  if (PassFail[ONEDS_FIRST_SAMPLE] != PassFail[ONEDS_CURRENT_SAMPLE]) {
    if (PassFail[ONEDS_EDGE_SAMPLES] >= DitherVal) {
      PassFail[ONEDS_EDGE_SAMPLES] = DitherVal;
      return TRUE;
    }
    PassFail[ONEDS_EDGE_SAMPLES]++;
  }
  return FALSE;
}
