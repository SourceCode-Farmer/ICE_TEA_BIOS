/** @file
  Once read mpr training is done, can use the read mpr pattern to train the DRAM DCA
  to correct the DRAM duty cycle.

@copyright
  INTEL CONFIDENTIAL
  Copyright 1999 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains an 'Intel Peripheral Driver' and is uniquely identified as
  "Intel Reference Module" and is licensed for Intel CPUs and chipsets under
  the terms of your license agreement with Intel or your vendor. This file may
  be modified by the user, subject to additional terms of the license agreement.

@par Specification Reference:
**/
//
// Include files
//
#include "MrcReadDqDqs.h"
#include "MrcCrosser.h"
#include "MrcCpgcApi.h"
#include "Cpgc20TestCtl.h"
#include "MrcCommon.h"
#include "MrcDdr5Registers.h"
#include "MrcDdr5.h"
#include "MrcChipApi.h"
#include "MrcMaintenance.h"
#include "MrcMemoryApi.h"
#include "MrcLpddr5Registers.h"
#include "MrcWriteLeveling.h"
#include "MrcDramDca.h"
#include "MrcWriteDqDqs.h"

/**

  Set up DRAM pattern for DDR5

  @param[in]  MrcData              - Pointer to MRC global data.
  @param[in]  ReadPattern0         - Read Pattern0
  @param[in]  ReadPattern1         - Read Pattern1

  @retval NA

**/
VOID
SetupDramPattern (
  IN  MrcParameters *const  MrcData,
  IN  UINT32                ReadPattern0,
  IN  UINT32                ReadPattern1
  )
{
  UINT8               MaxChannel;
  MrcOutput           *Outputs;
  UINT32              Controller;
  UINT32              Channel;
  UINT32              Rank;
  BOOLEAN             Ddr5;
  MrcDebug            *Debug;

  Outputs     = &MrcData->Outputs;
  MaxChannel  = Outputs->MaxChannels;
  Ddr5        = (Outputs->DdrType == MRC_DDR_TYPE_DDR5);
  Debug       = &Outputs->Debug;

  if (!Ddr5) {
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_WARNING, "Not Ddr5\n");
    return;
  }

  for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
    for (Channel = 0; Channel < MaxChannel; Channel++) {
      for (Rank = 0; Rank < MAX_RANK_IN_CHANNEL; Rank++) {
        if (!MrcRankExist (MrcData, Controller, Channel, Rank)) {
          continue;
        }
        // Initial setup of DRAM
        MrcIssueMrw (MrcData, Controller, Channel, Rank, mrMR25, 0, TRUE); // Read Training Mode Settings
        MrcIssueMrw (MrcData, Controller, Channel, Rank, mrMR26, ReadPattern0, TRUE); // LFSR0 Seed
        MrcIssueMrw (MrcData, Controller, Channel, Rank, mrMR27, ReadPattern1, TRUE); // LFSR1 Seed
        MrcIssueMrw (MrcData, Controller, Channel, Rank, mrMR28, 0x00, TRUE); // Read Pattern Invert DQ(7:0)
        MrcIssueMrw (MrcData, Controller, Channel, Rank, mrMR29, 0x00, TRUE); // Read Pattern Invert DQ(15:8)
        MrcIssueMrw (MrcData, Controller, Channel, Rank, mrMR30, 0, TRUE);
      }
    }
  }
}

/**

  Determine if a specific type of DRAM Duty Cycle Adjuster is supported by the Rank

  @param[in]  MrcData         - Pointer to MRC global data.
  @param[in]  Controller      - Controller number
  @param[in]  Channel         - Channel number
  @param[in]  Rank            - Rank number
  @param[in]  DcaTypeToCheck  - The type of DCA to check for

  @retval TRUE if the rank supports the specified DCA type
  @retval FALSE otherwise

**/
BOOLEAN
CheckDramDcaSupportedPerRank (
  IN  MrcParameters *const  MrcData,
  IN  UINT8                 Controller,
  IN  UINT8                 Channel,
  IN  UINT8                 Rank,
  IN  DCA_TYPE              DcaTypeToCheck
  )
{
  const MrcInput              *Inputs;
  DCA_TYPE                    DcaType;
  DDR5_MODE_REGISTER_42_TYPE  Mr42;
  const MRC_FUNCTION          *MrcCall;

  Inputs = &MrcData->Inputs;
  MrcCall = Inputs->Call.Func;
  DcaType = DcaNotSupported;
  UINT8 Mr42Data[MAX_CONTROLLER][MAX_CHANNEL][MAX_RANK_IN_CHANNEL];

  MrcCall->MrcSetMem ((UINT8 *) Mr42Data, sizeof (Mr42Data), 0);
  //Read DRAM capability.
  ReadDcaCapability (MrcData, Mr42Data);

  Mr42.Data8 = Mr42Data[Controller][Channel][Rank];
  DcaType = Mr42.Bits.DcaTypesSupported;

  if (DcaType == DcaTypeToCheck) {
    //
    // The rank supports the caller's DRAM Duty Cycle Adjuster type
    //
    return TRUE;
  }
  return FALSE;
}

/**

  Determine if a specific type of DRAM Duty Cycle Adjuster is supported by any rank.
  Return channel mask and rank mask indicating which channels and ranks support the DCA type.

  @param[in]  MrcData          - Pointer to MRC global data.
  @param[in]  ChannelMask      - One encoding to mask a channel (0: DcaType supported, 1: DcaType not supported)
  @param[in]  RankMask         - One encoding to mask a rank per channel (0: DcaType supported, 1: DcaType not supported)
  @pararm[in] DcaTypeToCheck   - The type of DCA to check for

  @retval TRUE if the DRAM supports DCA
  @retval FALSE if the DRAM doesn't support DCA

**/
BOOLEAN
CheckDramDcaSupported (
  IN  MrcParameters *const  MrcData,
  OUT  UINT32               *ChannelMask,
  OUT  UINT32               RankMask[MAX_CONTROLLER][MAX_CHANNEL],
  IN   DCA_TYPE             DcaTypeToCheck
  )
{
  UINT8                 Channel;
  UINT8                 Rank;
  UINT8                 Controller;
  UINT8                 MaxChannels;
  const MrcInput        *Inputs;
  MrcDebug              *Debug;
  const MRC_FUNCTION    *MrcCall;
  MrcOutput             *Outputs;
  BOOLEAN               Ddr5;

  Inputs                = &MrcData->Inputs;
  MrcCall               = Inputs->Call.Func;
  Outputs               = &MrcData->Outputs;
  Debug                 = &Outputs->Debug;
  MaxChannels           = Outputs->MaxChannels;
  Ddr5                  = (Outputs->DdrType == MRC_DDR_TYPE_DDR5);
  //
  // Initialize no channels and no ranks enabled
  //
  if (Ddr5) {
    *ChannelMask = 0xFFFFFFFF;
    MrcCall->MrcSetMem ((UINT8 *) RankMask, sizeof (UINT32) * MAX_CONTROLLER * MAX_CHANNEL, 0xFF);
  } else {
    *ChannelMask = 0;
    MrcCall->MrcSetMem ((UINT8 *) RankMask, sizeof (UINT32) * MAX_CONTROLLER * MAX_CHANNEL, 0);
    return TRUE;
  }

  for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
    for (Channel = 0; Channel < MaxChannels; Channel++) {
      for (Rank = 0; Rank < MAX_RANK_IN_CHANNEL; Rank++) {
        if (!MrcRankExist (MrcData, Controller, Channel, Rank)) {
          continue;
        }

        if (CheckDramDcaSupportedPerRank (MrcData, Controller, Channel, Rank, DcaTypeToCheck)) {
          //
          // Mark this channel needs to be trained
          // One-hot encoding to mask a channel (0: executes if enabled/populated, 1: skip/masked)
          //
          *ChannelMask &= ~(1 << Channel);
          //
          // Mark this rank needs to be trained
          // One-hot encoding to mask a rank (0: executes if enabled/populated, 1: skip/masked)
          //
          RankMask[Controller][Channel] &= ~(1 << Rank);

          MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Mc%d.C%d.R%d DCA Supported: type:  %d \n", Controller, Channel, Rank,
            DcaTypeToCheck);
        }
      } // Rank
    } // Channel
  } // Controller

  if (*ChannelMask != 0xFFFFFFFF) {
    //
    // At least one channel has a rank that supports the caller's DRAM Duty Cycle Adjuster type
    //
    return TRUE;
  } else {
    return FALSE;
  }
}

/**
  Program the DRAM DCA Training Assist Mode

  @param[in]  DramDcaTrainingData    - Pointer to MRC global data.
  @param[in]  DramDcaTrainingData    - Pointer to DRAM DCA training context
  @param[in]  TrainingAssistMode     - The DCA training assist mode
                                          DCA_TRAINING_ASSIST_DISABLE
                                          DCA_TRAINING_ASSIST_IBCLK
                                          DCA_TRAINING_ASSIST_ICLK

  @retval N/A

**/
VOID
SetDcaTrainingAssistMode (
  IN  MrcParameters *const       MrcData,
  IN  DRAM_DCA_TRAINING_Data     *DramDcaTrainingData,
  IN  UINT8                      TrainingAssistMode
  )
{
  UINT8                         Channel;
  UINT8                         Rank;
  UINT32                        Controller;
  UINT8                         MaxChannel;
  MrcOutput                     *Outputs;

  Outputs     = &MrcData->Outputs;
  MaxChannel  = Outputs->MaxChannels;
  DDR5_MODE_REGISTER_42_TYPE Mr42;
  Mr42.Data8 = 0;
  Mr42.Bits.DcaTrainingAssistMode = TrainingAssistMode;

  for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
    for (Channel = 0; Channel < MaxChannel; Channel++) {
      for (Rank = 0; Rank < MAX_RANK_IN_CHANNEL; Rank++) {
        if (!MrcRankExist (MrcData, Controller, Channel, Rank)) {
          continue;
        }
        if (((1 << Rank) & DramDcaTrainingData->RankMask[Controller][Channel]) != 0) {
          continue;
        }
        // Enable DCA Training Assist Mode
        MrcIssueMrw (MrcData, Controller, Channel, Rank, mrMR42, Mr42.Data8, TRUE);
      }
    }
  }
}

/**

  Get DRAM device internal clock alignment in preparation for DCA Training.
  Peform a point test of Read DQDQS at the passing point with DRAM DCA Training Assist mode enabled.

  @param[in, out] MrcData           - Pointer to MRC global data.
  @param[in] DramDcaTrainingData    - Pointer to DRAM DCA training data.

  @retval mrcSuccess if the discovery completes
  @retval mrcFail if input parameter is invalid

**/
MrcStatus
DramDcaClkPhaseAlignmentCheck (
  IN OUT MrcParameters *const         MrcData,
  IN OUT  DRAM_DCA_TRAINING_Data      *DramDcaTrainingData
  )
{

  MrcOutput          *Outputs;
  MrcDebug           *Debug;

  Outputs = &MrcData->Outputs;
  Debug = &Outputs->Debug;

  if (DramDcaTrainingData->DcaType != DcaFourPhase) {
    //
    // Only needed for DCA 4-phase training
    //
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "DcaType is not DCA four phase\n");
    return mrcSuccess;
  }
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "DramDcaClkPhaseAlignmentCheck start\n");

  //
  // Enable DCA Training Assist Mode to block reads synchronized with IBCLK
  // In this mode if MRRs sent to the DIMM are locked to the IBCLK then DQs will be driven HIGH.
  //
  SetDcaTrainingAssistMode (MrcData, DramDcaTrainingData, DCA_TRAINING_ASSIST_IBCLK);

  MrcReadMprTrainingNormalDcaMargin (MrcData, RdT, FALSE, MRC_PRINTS_ON, 0, NULL, FALSE, DramDcaTrainingData, TRUE);
  //
  // Disable DRAM DCA training assist mode
  //
  SetDcaTrainingAssistMode (MrcData, DramDcaTrainingData, DCA_TRAINING_ASSIST_DISABLE);

  return mrcSuccess;

}

/**
  Perform DCA read margin using advanced write read pattern

  @param[in] MrcData       - Pointer to MRC global data.
  @param[in] Param         - Indication to optimize P/N or both.
  @param[in] Print         - Print debug messages.
  @param[in] UiIndex       - UI index.
  @param[in] EyeWidthAllUi - Pointer to the data struct to save the EW of the training.
  @param[in] MarginOnly    - Margin only or need to return the trained value.

  @retval MrcStatus - mrcSuccess or reason for failure.
**/

MrcStatus
MrcReadrainingNormalDcaMarginAdvanced (
  IN OUT MrcParameters *const MrcData,
  IN     UINT8                Param,
  IN     BOOLEAN              Print,
  IN     UINT32               UiIndex,
  IN OUT EYE_WIDTH_UI_ALL     *EyeWidthAllUi,
  IN     BOOLEAN              MarginOnly
  )
{
  const MrcInput      *Inputs;
  const MRC_FUNCTION  *MrcCall;
  MrcDebug            *Debug;
  MrcOutput           *Outputs;
  MrcStatus           Status;
  UINT8               Controller;
  UINT8               Channel;
  UINT8               Rank;
  UINT8               Byte;
  UINT8               RankMask;
  UINT8               MaxMargin;
  UINT32              BERStats[4];
  UINT8               BMap[MAX_SDRAM_IN_DIMM];
  UINT32              ResultsSum[MAX_CONTROLLER][MAX_CHANNEL][MAX_RANK_IN_CHANNEL];
  UINT8               ResCount[MAX_CONTROLLER][MAX_CHANNEL][MAX_RANK_IN_CHANNEL];
  UINT8               LoopCount;
  UINT8               McChBitMask;
  UINT8               MaxChannels;
  INT8                VrefOffsets[2];
  UINT16              (*MarginByte)[MAX_RESULT_TYPE][MAX_RANK_IN_CHANNEL][MAX_CONTROLLER][MAX_CHANNEL][MAX_SDRAM_IN_DIMM][MAX_EDGES];
  UINT8               ResultType;
  BOOLEAN             Ddr5;
  BOOLEAN             Lpddr5;
  UINT32              UiMask;
  INT16               lWidth;
  BOOLEAN             EnBer;

  Inputs = &MrcData->Inputs;
  Outputs = &MrcData->Outputs;
  Debug = &Outputs->Debug;
  MrcCall = Inputs->Call.Func;
  McChBitMask = Outputs->ValidChBitMask;
  RankMask = Outputs->ValidRankMask;
  MaxChannels = Outputs->MaxChannels;
  ResultType = 0;
  MarginByte = &Outputs->MarginResult;
  RankMask &= Outputs->ValidRankMask;
  Ddr5 = (Outputs->DdrType == MRC_DDR_TYPE_DDR5);
  Lpddr5 = (Outputs->DdrType == MRC_DDR_TYPE_LPDDR5);
  LoopCount = 17;
  ResultType = GetMarginResultType (RdT);
  MrcCall->MrcSetMem ((UINT8 *) BERStats, sizeof (BERStats), 0);
  MrcCall->MrcSetMem ((UINT8 *) VrefOffsets, sizeof (VrefOffsets), 0);
  MrcCall->MrcSetMemDword ((UINT32 *) ResultsSum, sizeof (ResultsSum) / sizeof (UINT32), 0);
  MrcCall->MrcSetMem ((UINT8 *) ResCount, sizeof (ResCount), 0);

  for (Byte = 0; Byte < ARRAY_COUNT (BMap); Byte++) {
    BMap[Byte] = Byte;
  }
  if (Lpddr5) {
    UiMask = (1 << MAX_UI) - 1;
  } else {
    if (!MarginOnly) {
      UiMask = 0x1111 << UiIndex;
    } else {
      UiMask = (1 << MAX_UI) - 1;
    }
  }
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "MrcReadrainingNormalDcaMarginAdvanced start\n");
  // Setup stress
  SetupIOTestBasicVA (MrcData, McChBitMask, LoopCount, NSOE, 0, 0, 8, PatWrRd, 0, 0);

  // Program chunk mask for RxDqsP/N.
  // This is set to all chunks in SetupIOTest, so needs to be done after calling SetupIOTest.
  if (Ddr5) {
    MrcSetChunkAndClErrMsk (MrcData, 0xFF, UiMask);
  } else {
    if ((Param == RdTP) || (Param == RdTN)) {
      // For RxDqsP check only even chunks
      UiMask = 0x5555;
      if (Param == RdTN) {
        // For RxDqsN check only odd chunks
        UiMask = UiMask << 1;
      }
      MrcSetChunkAndClErrMsk (MrcData, 0xFF, UiMask);
    }
  }
  for (Byte = 0; Byte < ARRAY_COUNT (BMap); Byte++) {
    BMap[Byte] = Byte;
  }
  // Collect margin per rank
  for (Rank = 0; Rank < MAX_RANK_IN_CHANNEL; Rank++) {
    // Select rank for REUT test
    Status = mrcSuccess;
    RankMask = 1 << Rank;
    McChBitMask = 0;
    for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
      for (Channel = 0; Channel < MaxChannels; Channel++) {
        McChBitMask |= SelectReutRanks (MrcData, Controller, Channel, RankMask, FALSE, 0);
      }
    }
    // Continue with next rank if this rank is not present on any channel
    if (!(McChBitMask)) {
      continue;
    }
    MaxMargin = MAX_POSSIBLE_TIME;

    EnBer = 0;//1;
    MrcGetBERMarginByte (
      MrcData,
      Outputs->MarginResult,
      McChBitMask,
      RankMask,
      RankMask,
      RdT,
      0,  // Mode
      BMap,
      EnBer,
      MaxMargin,
      0,
      BERStats
      );
  }

  // Save results results
  for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
    for (Rank = 0; Rank < MAX_RANK_IN_CHANNEL; Rank++) {
      for (Channel = 0; Channel < MaxChannels; Channel++) {
        if (MrcRankExist (MrcData, Controller, Channel, Rank)) {

          for (Byte = 0; Byte < Outputs->SdramCount; Byte++) {
            lWidth = (((*MarginByte) [ResultType][Rank][Controller][Channel][Byte][1]) / 10) + ((((*MarginByte) [ResultType][Rank][Controller][Channel][Byte][0])) / 10);
            if (!MarginOnly) {
              EyeWidthAllUi->EyeWidthAllStrobe[UiIndex].EyeWidth[Controller][Channel][Rank][Byte] = lWidth;
            }
            MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Strobe %d  LE %3d  RE %3d\n", Byte, (*MarginByte) [ResultType][Rank][Controller][Channel][Byte][0], (*MarginByte) [ResultType][Rank][Controller][Channel][Byte][1]);

          }
        }
      } // Channel
    } //  Rank
  } // Controller

  Status  = IoReset (MrcData);

  return Status;
}

/**
  Perform DCA read margin using normal read mpr pattern.

  @param[in] MrcData              - Pointer to MRC global data.
  @param[in] Param                - Indication to optimize P/N or both.
  @param[in] Print                - Print debug messages.
  @param[in] UiIndex              - UI index.
  @param[in] EyeWidthAllUi        - Pointer to the data struct to save the EW of the training.
  @param[in] MarginOnly           - Margin only or need to return the trained value.
  @param[in] DramDcaTrainingData  - Pointer to DRAM DCA training context
  @param[in] SimpleCheck          - Simple check or not. Simple check is used to check the clock phase alignment.

  @retval MrcStatus - mrcSuccess or reason for failure.
**/
MrcStatus
MrcReadMprTrainingNormalDcaMargin (
  IN OUT MrcParameters    *const MrcData,
  IN     UINT8                   Param,
  IN     BOOLEAN                 PerBit,
  IN     BOOLEAN                 Print,
  IN     UINT32                  UiIndex,
  IN OUT EYE_WIDTH_UI_ALL        *EyeWidthAllUi,
  IN     BOOLEAN                 MarginOnly,
  IN     DRAM_DCA_TRAINING_Data  *DramDcaTrainingData,
  IN     BOOLEAN                 SimpleCheck
  )
{
  const MrcInput      *Inputs;
  MrcDebug            *Debug;
  const MRC_FUNCTION  *MrcCall;
  MrcOutput           *Outputs;
  MrcStatus           Status;
  MrcStatus           MprStatus;
  UINT8               MaxChannel;
  UINT8               Controller;
  UINT8               Channel;
  UINT32              IpChannel;
  UINT8               Rank;
  UINT8               Byte;
  UINT8               Bit;
  UINT8               RankMask;  // RankBitMask for both channels
  UINT8               LoopCount;
  UINT8               McChBitMask;
  UINT8               EccByte;
  UINT64              ErrStatus;
  UINT64              EccStatus;
  UINT64              ChunkStatus;
  UINT16              *Margin;
  UINT16              ChunkResult[MAX_CONTROLLER][MAX_CHANNEL];
  INT16               DqsDelay;
  INT16               DqsStart;
  INT16               DqsStop;
  INT16               CurrentPassingStart[MAX_CONTROLLER][MAX_CHANNEL][MAX_SDRAM_IN_DIMM];
  INT16               CurrentPassingEnd[MAX_CONTROLLER][MAX_CHANNEL][MAX_SDRAM_IN_DIMM];
  INT16               LargestPassingStart[MAX_CONTROLLER][MAX_CHANNEL][MAX_SDRAM_IN_DIMM];
  INT16               LargestPassingEnd[MAX_CONTROLLER][MAX_CHANNEL][MAX_SDRAM_IN_DIMM];
  INT16               FinalDqs[MAX_CONTROLLER][MAX_CHANNEL][MAX_RANK_IN_CHANNEL][MAX_SDRAM_IN_DIMM];
  INT16               cWidth;
  INT16               lWidth;
  INT16               Center;
  INT16               Left;
  INT16               Right;
  INT64               GetSetDis;
  INT64               GetSetEn;
  INT64               GetSetVal;
  BOOLEAN             Pass;
  BOOLEAN             Lpddr5;
  BOOLEAN             Lpddr;
  BOOLEAN             Ddr5;
  UINT8               RdDataPtn;    //Read Data Pattern in 8 bit
  UINT8               MrDqCalPatA;
  UINT8               MrDqCalPatB;
  UINT8               MrDqCalLow;
  UINT8               MrDqCalHigh;
  UINT8               MrData;
  BOOLEAN             Gear4;
  UINT32              McRdbCreditsSave;
  UINT32              Offset;
  MC0_CH0_CR_MC_RDB_CREDITS_STRUCT  McRdbCredits;
  INT16               MarginBit[MAX_RANK_IN_CHANNEL][MAX_CONTROLLER][MAX_CHANNEL][MAX_SDRAM_IN_DIMM][MAX_BITS][MAX_EDGES]; // For Rx PBD
  UINT8               L2RBitFlag[MAX_RANK_IN_CHANNEL][MAX_CONTROLLER][MAX_CHANNEL][MAX_SDRAM_IN_DIMM][MAX_BITS];
  UINT8               MinWidth;
  UINT8               BitGroupErr[MAX_CONTROLLER][MAX_CHANNEL][MAX_SDRAM_IN_DIMM];
  UINT32              UiMask = 0;
  INT64               SaveRxDqsP[MAX_RANK_IN_CHANNEL][MAX_CONTROLLER][MAX_CHANNEL][MAX_SDRAM_IN_DIMM]; // For Rx PBD P delay
  INT64               SaveRxDqsN[MAX_RANK_IN_CHANNEL][MAX_CONTROLLER][MAX_CHANNEL][MAX_SDRAM_IN_DIMM]; // For Rx PBD N delay
  INT64               GetSetValN;
  INT64               GetSetValP;
  GSM_GT              Group;
  MC_MPR_CONFIG_SAVE  SaveData;

  Inputs      = &MrcData->Inputs;
  MrcCall     = Inputs->Call.Func;
  Outputs     = &MrcData->Outputs;
  Debug       = Print ? &Outputs->Debug : NULL;
  Status      = mrcSuccess;
  MprStatus   = mrcSuccess;
  Lpddr5      = (Outputs->DdrType == MRC_DDR_TYPE_LPDDR5);
  Ddr5        = (Outputs->DdrType == MRC_DDR_TYPE_DDR5);
  Lpddr       = Outputs->Lpddr;
  RankMask    = Outputs->ValidRankMask;
  GetSetDis   = 0;
  GetSetEn    = 1;
  GetSetValN  = 0;
  GetSetValP  = 0;
  MaxChannel  = Outputs->MaxChannels;
  Gear4       = Outputs->Gear4;
  MinWidth    = 10; // To avoid speckling
  MrcCall->MrcSetMem ((UINT8 *) L2RBitFlag, sizeof (L2RBitFlag), 0);
  MrcCall->MrcSetMem ((UINT8 *) MarginBit,  sizeof (MarginBit),  0);
  MrcCall->MrcSetMem ((UINT8 *) SaveRxDqsP, sizeof (SaveRxDqsP), 0);
  MrcCall->MrcSetMem ((UINT8 *) SaveRxDqsN, sizeof (SaveRxDqsN), 0);

  // Setup the Read Data Pattern.  This depends on technology. DDR5 uses Galois LFSR so this value is not used.
  //          Lpddr
  // Pattern  01011010
  RdDataPtn   = (Lpddr) ? DDR5_LFSR0_5A : 0xAA;

  EccByte  = Ddr5 ? MRC_DDR5_ECC_BYTE : MRC_DDR4_ECC_BYTE;
  McRdbCreditsSave = 0;
  if (Gear4 && Lpddr) {
    for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
      for (Channel = 0; Channel < MaxChannel; Channel++) {
        if ((!MrcChannelExist (MrcData, Controller, Channel)) || IS_MC_SUB_CH (Lpddr, Channel)) {
          continue;
        }
        IpChannel = LP_IP_CH (Lpddr, Channel);
        Offset = OFFSET_CALC_MC_CH (MC0_CH0_CR_MC_RDB_CREDITS_REG, MC1_CH0_CR_MC_RDB_CREDITS_REG, Controller, MC0_CH1_CR_MC_RDB_CREDITS_REG, IpChannel);
        McRdbCredits.Data = MrcReadCR (MrcData, Offset);
        McRdbCreditsSave = McRdbCredits.Data;             // Back up the original value, should be the same on all channels
        McRdbCredits.Bits.VC0_reads_entries   = 29;
        McRdbCredits.Bits.VC1_reads_entries   = 3;
        McRdbCredits.Bits.Total_reads_entries = 32;
        MrcWriteCR (MrcData, Offset, McRdbCredits.Data);
      }
    }
  }

  DqsStart = RMPR_DQS_START_DCA;
  DqsStop  = RMPR_DQS_STOP_DCA;

  if (Ddr5 && Inputs->ReadMprVA) {
    // Loop Count is decreased to 7 to compensate for running each IOTest 8 times. 2^7 * 2^3 = 2^10
    // DDR5 does not use a predefined RdDataPtn since it uses Galois
    LoopCount = 7;
  } else {
    LoopCount = 17;
    if (SimpleCheck) {
      LoopCount = 10;
    }
  }

  if (Lpddr5 && Gear4 && (Outputs->Frequency > f5200)) {
    LoopCount = 4;
  }
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Loopcount %d \n", LoopCount);
  SetupMcMprConfig (MrcData, &SaveData, MRC_ENABLE);

  if (Lpddr5) {
    MrDqCalPatA = 33;
    MrDqCalPatB = 34;
    MrDqCalLow  = 31;
    MrDqCalHigh = 32;
  } else {
    MrDqCalPatA = 0;
    MrDqCalPatB = 0;
    MrDqCalLow  = 0;
    MrDqCalHigh = 0;
  }

  // Setup REUT Engine
  SetupIOTestCpgcRead (MrcData, Outputs->McChBitMask, LoopCount, NSOE, RdDataPtn);

  // Program chunk mask for RxDqsP/N.
  // This is set to all chunks in SetupIOTest, so needs to be done after calling SetupIOTest.
  if ((Param == RdTP) || (Param == RdTN)) {
    // For RxDqsP check only even chunks
    UiMask = 0x5555;
    if (Param == RdTN) {
      // For RxDqsN check only odd chunks
      UiMask = UiMask << 1;
    }
    MrcSetChunkAndClErrMsk (MrcData, 0xFF, UiMask);
  } else {
    if (!MarginOnly) {
      UiMask = 0x1111 << UiIndex;
    } else {
      UiMask = (1 << MAX_UI) - 1;
    }
    MrcSetChunkAndClErrMsk (MrcData, 0xFF, UiMask);
  }
  // If training RdTN or RdTP, save the other strobe delay to be restored at the end
  for (Rank = 0; Rank < MAX_RANK_IN_CHANNEL; Rank++) {
    for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
      for (Channel = 0; Channel < MaxChannel; Channel++) {
        if (MrcRankExist (MrcData, Controller, Channel, Rank)) {
          for (Byte = 0; Byte < Outputs->SdramCount; Byte++) {
            // Save RxDqsP value
            MrcGetSetStrobe (MrcData, Controller, Channel, Rank, Byte, RxDqsPDelay, ReadCached,   &SaveRxDqsP[Rank][Controller][Channel][Byte]);
            // Save RxDqsN value
            MrcGetSetStrobe (MrcData, Controller, Channel, Rank, Byte, RxDqsNDelay, ReadCached,   &SaveRxDqsN[Rank][Controller][Channel][Byte]);
          } //Byte
        } //Rank
      } // Channel
    } // Controller
  } // Rank

  MrcFlushRegisterCachedData (MrcData);

  for (Rank = 0; Rank < MAX_RANK_IN_CHANNEL; Rank++) {
    if (!((MRC_BIT0 << Rank) & RankMask)) {
      continue; // Skip if both channels empty
    }
    McChBitMask = 0;
    // Program MR3 and Mask RAS/WE to prevent scheduler for issuing non-Read commands
    for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
      for (Channel = 0; Channel < MaxChannel; Channel++) {
        McChBitMask |= SelectReutRanks (MrcData, Controller, Channel, 1 << Rank, FALSE, 0); //Save particular mask to use with runIOTest later
        if (!(MrcRankExist (MrcData, Controller, Channel, Rank))) {
          continue;
        }
        // Enter MPR mode
        if (Lpddr) {
          MrcIssueMrw (MrcData, Controller, Channel, Rank, MrDqCalPatA, RdDataPtn, TRUE); // DQ Calibration Pattern "A"
          MrcIssueMrw (MrcData, Controller, Channel, Rank, MrDqCalPatB, RdDataPtn, TRUE); // DQ Calibration Pattern "B"
          MrcIssueMrw (MrcData, Controller, Channel, Rank, MrDqCalLow,  0x00, TRUE); // Lower Byte Invert for DQ Calibration
          MrcIssueMrw (MrcData, Controller, Channel, Rank, MrDqCalHigh, 0x00, TRUE); // Upper Byte Invert for DQ Calibration
        } else if (Ddr5) {
          // Initial setup of DRAM
          MrcIssueMrw (MrcData, Controller, Channel, Rank, mrMR25, 0x01, TRUE); // Read Training Mode Settings
          MrcIssueMrw (MrcData, Controller, Channel, Rank, mrMR26, DDR5_LFSR0_5A, TRUE); // LFSR0 Seed
          MrcIssueMrw (MrcData, Controller, Channel, Rank, mrMR27, DDR5_LFSR1_3C, TRUE); // LFSR1 Seed
          MrcIssueMrw (MrcData, Controller, Channel, Rank, mrMR28, 0x00, TRUE); // Read Pattern Invert DQ(7:0)
          MrcIssueMrw (MrcData, Controller, Channel, Rank, mrMR29, 0x00, TRUE); // Read Pattern Invert DQ(15:8)
          MrData = (Inputs->ReadMprVA) ? 0xFE : 0xFF;                           // LFSR Assignment (0-LFSR0, 1-LFSR1)
          MrcIssueMrw (MrcData, Controller, Channel, Rank, mrMR30, MrData, TRUE);
        }
      } // for Channel
    } // for Controller
    MrcGetSetMcCh (MrcData, MAX_CONTROLLER, MAX_CHANNEL, GsmMccMprTrainDdrOn, WriteNoCache, &GetSetEn);
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "--- CPGC-based RDMPR ---\n");
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Rank = %u\n", Rank);
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Channel\t0                1\nByte\t");
    MRC_DEBUG_MSG (
      Debug,
      MSG_LEVEL_NOTE, (
                        Outputs->SdramCount == (MAX_SDRAM_IN_DIMM)
                        ) ? "0 1 2 3 4 5 6 7 8 0 1 2 3 4 5 6 7 8" : "0 1 2 3 4 5 6 7 0 1 2 3 4 5 6 7"
      );

    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\n%s",(Param == RdTN) ? "DqsNDelay" : (Param == RdTP) ? "DqsPDelay" : "DqsDelay" );
    if (SimpleCheck) {
      DqsStop = DqsStart + 1;
    }
    for (DqsDelay = DqsStart; DqsDelay < DqsStop; DqsDelay += RMPR_DQS_STEP) {
      MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\n% 5d\t", DqsDelay);
      // Program DQS Delays and download the Reg File for the current rank.
      if (!SimpleCheck) {
        GetSetVal = DqsDelay;
        MrcGetSetStrobe (MrcData, MAX_CONTROLLER, MAX_CHANNEL, MAX_RANK_IN_CHANNEL, MAX_SDRAM_IN_DIMM, RxDqsPDelay,
          ForceWriteCached, &GetSetVal);
        MrcGetSetStrobe (MrcData, MAX_CONTROLLER, MAX_CHANNEL, MAX_RANK_IN_CHANNEL, MAX_SDRAM_IN_DIMM, RxDqsNDelay,
          ForceWriteCached, &GetSetVal);
      }
      for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
        for (Channel = 0; Channel < MaxChannel; Channel++) {
          if (!(MrcRankExist (MrcData, Controller, Channel, Rank))) {
            MRC_DEBUG_MSG (
              Debug,
              MSG_LEVEL_NOTE,
              (Channel != 0) ? "" : ((Outputs->SdramCount == MAX_SDRAM_IN_DIMM) ? "                  " : "                ")
              );
          }
        }
      }

      if (Lpddr) {
        // Clean the Rx FIFO using another MPR sequence - all zeroes
        MrcGetSetMcCh (MrcData, MAX_CONTROLLER, MAX_CHANNEL, GsmMccMprTrainDdrOn, WriteNoCache, &GetSetDis);
        for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
          for (Channel = 0; Channel < MaxChannel; Channel++) {
            if (!(MrcRankExist (MrcData, Controller, Channel, Rank))) {
              continue;
            }
            MrcIssueMrw (MrcData, Controller, Channel, Rank, MrDqCalPatA, 0, TRUE); // DQ Calibration Pattern "A"
            MrcIssueMrw (MrcData, Controller, Channel, Rank, MrDqCalPatB, 0, TRUE); // DQ Calibration Pattern "B"
          } // for Channel
        } // for Controller
        MrcGetSetMcCh (MrcData, MAX_CONTROLLER, MAX_CHANNEL, GsmMccMprTrainDdrOn, WriteNoCache, &GetSetEn);
      }
      // No need to clean RxFIFO in DDR5 as we use LFSR pattern
      if (!Ddr5) {
        SetupIOTestCpgcRead (MrcData, McChBitMask, 4, NSOE, RdDataPtn);
        RunIOTest (MrcData, McChBitMask, Outputs->DQPat, 1, 0);
      }

      if (Lpddr) {
        MrcGetSetMcCh (MrcData, MAX_CONTROLLER, MAX_CHANNEL, GsmMccMprTrainDdrOn, WriteNoCache, &GetSetDis);
        for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
          for (Channel = 0; Channel < MaxChannel; Channel++) {
            if (!(MrcRankExist (MrcData, Controller, Channel, Rank))) {
              continue;
            }
            MrcIssueMrw (MrcData, Controller, Channel, Rank, MrDqCalPatA, RdDataPtn, TRUE); // DQ Calibration Pattern "A"
            MrcIssueMrw (MrcData, Controller, Channel, Rank, MrDqCalPatB, RdDataPtn, TRUE); // DQ Calibration Pattern "B"
          } // for Channel
        } // for Controller
        MrcGetSetMcCh (MrcData, MAX_CONTROLLER, MAX_CHANNEL, GsmMccMprTrainDdrOn, WriteNoCache, &GetSetEn);
      }

      if (!Ddr5) { // For DDR5 we do this once in the beginning
        SetupIOTestCpgcRead (MrcData, McChBitMask, LoopCount, NSOE, RdDataPtn);
        // Program chunk mask for RxDqsP/N.
        // This is set to all chunks in SetupIOTest, so needs to be done after calling SetupIOTest.
        if ((Param == RdTP) || (Param == RdTN)) {
          // For RxDqsP check only even chunks
          UiMask = 0x5555;
          if (Param == RdTN) {
            // For RxDqsN check only odd chunks
            UiMask = UiMask << 1;
          }
          MrcSetChunkAndClErrMsk (MrcData, 0xFF, UiMask);
        }
      }
      RunIOTest (MrcData, McChBitMask, Outputs->DQPat, 1, 0);

      // Update results for all ch/bytes
      MrcCall->MrcSetMem ((UINT8 *) BitGroupErr, sizeof (BitGroupErr), 0);
      for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
        for (Channel = 0; Channel < MaxChannel; Channel++) {
          if (!(MrcRankExist (MrcData, Controller, Channel, Rank))) {
            continue;
          }
          ErrStatus = 0;
          EccStatus = 0;
          ChunkStatus = 0;
          ChunkResult[Controller][Channel] = 0;
          // Read out per byte error results and check for any byte error
          MrcGetMiscErrStatus (MrcData, Controller, Channel, ByteGroupErrStatus, &ErrStatus);
          if (Outputs->EccSupport) { //Read out per byte ecc status
            MrcGetMiscErrStatus (MrcData, Controller, Channel, EccLaneErrStatus, &EccStatus);
          }
          // Read per chunk error status
          MrcGetMiscErrStatus (MrcData, Controller, Channel, ChunkErrStatus, &ChunkStatus);
          ChunkResult[Controller][Channel] = (UINT16) ChunkStatus;
          MrcGetBitGroupErrStatus (MrcData, Controller, Channel, BitGroupErr[Controller][Channel]);
          for (Byte = 0; Byte < Outputs->SdramCount; Byte++) {
            if (!MrcByteExist (MrcData, Controller, Channel, Byte)) {
              MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "  ");
              continue;
            }
            if (Byte < EccByte) {
              Pass = ((((UINT32) ErrStatus) & (1 << Byte)) == 0);
            } else {
              Pass = (((UINT32) EccStatus) == 0);
            }
            MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, Pass ? ". " : "# ");
            if (SimpleCheck) {
              if (Pass == 0) {
                MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Mc%d.C%d.R%d.S%d Align to IBCLK\n", Controller, Channel, Rank, Byte);
                DramDcaTrainingData->ClkPhaseAlignment[Byte].DcaClkAlignment[Controller][Channel][Rank] = IBCLK_PHASE_ALIGNED;
              } else {
                MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Mc%d.C%d.R%d.S%d Align to ICLK\n", Controller, Channel, Rank, Byte);
                DramDcaTrainingData->ClkPhaseAlignment[Byte].DcaClkAlignment[Controller][Channel][Rank] = ICLK_PHASE_ALIGNED;
              }

            } else {
              if (DqsDelay == DqsStart) {
                if (Pass) {
                  CurrentPassingStart[Controller][Channel][Byte] = CurrentPassingEnd[Controller][Channel][Byte] = DqsDelay;
                  LargestPassingStart[Controller][Channel][Byte] = LargestPassingEnd[Controller][Channel][Byte] = DqsDelay;
                } else {
                  CurrentPassingStart[Controller][Channel][Byte] = CurrentPassingEnd[Controller][Channel][Byte] = -DqsStart - 1;
                  LargestPassingStart[Controller][Channel][Byte] = LargestPassingEnd[Controller][Channel][Byte] = -DqsStart - 1;
                }
              } else {
                if (Pass) {
                  if (CurrentPassingEnd[Controller][Channel][Byte] == (DqsDelay - RMPR_DQS_STEP)) {
                    CurrentPassingEnd[Controller][Channel][Byte] = DqsDelay;
                  } else {
                    CurrentPassingStart[Controller][Channel][Byte] = CurrentPassingEnd[Controller][Channel][Byte] = DqsDelay;
                  }
                  // Update Largest variables

                  cWidth = CurrentPassingEnd[Controller][Channel][Byte] - CurrentPassingStart[Controller][Channel][Byte];
                  lWidth = LargestPassingEnd[Controller][Channel][Byte] - LargestPassingStart[Controller][Channel][Byte];
                  if (cWidth > lWidth) {
                    LargestPassingStart[Controller][Channel][Byte] = CurrentPassingStart[Controller][Channel][Byte];
                    LargestPassingEnd[Controller][Channel][Byte] = CurrentPassingEnd[Controller][Channel][Byte];
                  }
                }
              }
            }
          } // for Byte
        } // for Channel
      } // for Controller
      if (!SimpleCheck) {
        for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
          for (Channel = 0; Channel < MaxChannel; Channel++) {
            ErrStatus = 0;
            for (Byte = 0; Byte < Outputs->SdramCount; Byte++) {
              if (Byte < EccByte) {
                ErrStatus |= MrcCall->MrcLeftShift64 (BitGroupErr[Controller][Channel][Byte], Byte * 8);
              }
              // Update Margin per bit
              if (DqsDelay == DqsStart) {
                for (Bit = 0; Bit < MAX_BITS; Bit++) {
                  MarginBit[Rank][Controller][Channel][Byte][Bit][0] = DqsStart;
                  MarginBit[Rank][Controller][Channel][Byte][Bit][1] = DqsStop;
                }
              } else {
                for (Bit = 0; Bit < MAX_BITS; Bit++) {
                  if (L2RBitFlag[Rank][Controller][Channel][Byte][Bit] == 0) { // Update Left Edge
                    //MRC_DEBUG_MSG(Debug, MSG_LEVEL_NOTE, "\n Update Left Edge : ErrMaskMC%dC%dB%d 0x%02X\n", Controller, Channel, Byte, BitGroupErr[Controller][Channel][Byte]);
                    MarginBit[Rank][Controller][Channel][Byte][Bit][0] = DqsDelay;
                    if (!(BitGroupErr[Controller][Channel][Byte] & (1 << Bit))) { // Fail to Pass transition (1-> 0)
                      //MRC_DEBUG_MSG(Debug, MSG_LEVEL_NOTE, "\n Update Left Edge last time\n");
                      L2RBitFlag[Rank][Controller][Channel][Byte][Bit] = 1; // not update left edge anymore
                      MarginBit[Rank][Controller][Channel][Byte][Bit][0] -= RMPR_DQS_STEP;
                    }
                  } else if (L2RBitFlag[Rank][Controller][Channel][Byte][Bit] == 1) { // Update Right Edge
                    //MRC_DEBUG_MSG(Debug, MSG_LEVEL_NOTE, "\n Update Right Edge : ErrMaskMC%dC%dB%d 0x%02X\n", Controller, Channel, Byte, BitGroupErr[Controller][Channel][Byte]);
                    MarginBit[Rank][Controller][Channel][Byte][Bit][1] = DqsDelay;
                    if ((BitGroupErr[Controller][Channel][Byte] & (1 << Bit)) && (ABS(MarginBit[Rank][Controller][Channel][Byte][Bit][1] - MarginBit[Rank][Controller][Channel][Byte][Bit][0]) > MinWidth)) { // Pass to Fail transition (0-> 1), minWidth 10 ticks
                      L2RBitFlag[Rank][Controller][Channel][Byte][Bit] = 2; // not update right edge anymore
                      //MRC_DEBUG_MSG(Debug, MSG_LEVEL_NOTE, "\n Update Right Edge last time\n");
                    }
                  }
                } // Bit
              } // else
            } // Byte
            if (MrcRankExist (MrcData, Controller, Channel, Rank)) {
              if (Outputs->EccSupport) {
                MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "0x%02X_", BitGroupErr[Controller][Channel][EccByte]);
              }
              MRC_DEBUG_MSG (
                Debug,
                MSG_LEVEL_NOTE,
                "%s%016llX 0x%04X ",
                Outputs->EccSupport ? "" : "0x",
                ErrStatus,
                ChunkResult[Controller][Channel]
                );
            }
          }
        }
      }
      Status = IoReset (MrcData);
    } // for DqsDelay

    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\n");

    // Clean Up registers.
    // MPR_TRAIN_DDR_ON bit will force a special command so clear it before MRS command
    MrcGetSetMcCh (MrcData, MAX_CONTROLLER, MAX_CHANNEL, GsmMccMprTrainDdrOn, WriteNoCache, &GetSetDis);
    if (!SimpleCheck) {
      for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
        for (Channel = 0; Channel < MaxChannel; Channel++) {
          if (MrcRankExist (MrcData, Controller, Channel, Rank)) {

            MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "MC%u.C%u.R%u: \tLeft\tRight\tWidth\tCenter\tRxDqs%s\n", Controller, Channel, Rank, (Param == RdTN) ? "N" : (Param == RdTP) ? "P" : "PN");
            for (Byte = 0; Byte < Outputs->SdramCount; Byte++) {
              if (!MrcByteExist (MrcData, Controller, Channel, Byte)) {
                continue;
              }
              Left = LargestPassingStart[Controller][Channel][Byte];
              Right = LargestPassingEnd[Controller][Channel][Byte];
              lWidth = Right - Left;
              if (!MarginOnly) {
                if (EyeWidthAllUi == NULL) {
                  MRC_DEBUG_MSG (Debug, MSG_LEVEL_ERROR, "\nERROR!! EyeWidthAllUi == NULL\n");
                  return mrcWrongInputParameter;
                }
                EyeWidthAllUi->EyeWidthAllStrobe[UiIndex].EyeWidth[Controller][Channel][Rank][Byte] = lWidth;
              }
              Center = Left + (lWidth / 2);

              // Error Handler if eye not found for all bytes
              if (lWidth == 0) {
                MRC_DEBUG_MSG (
                  Debug,
                  MSG_LEVEL_ERROR,
                  "\nERROR!! NO EYE found for Mc: %u Channel: %u Rank: %u Byte: %u \n",
                  Controller,
                  Channel,
                  Rank,
                  Byte
                  );
                // Mark it as zero margin in MarginResult
                Left = 0;
                Right = 0;
                Center = 0;
                MprStatus = mrcReadMPRErr;
              } else if (lWidth <= RMPR_MIN_WIDTH) {
                MRC_DEBUG_MSG (
                  Debug,
                  MSG_LEVEL_WARNING,
                  "WARNING!! lWidth <= %u for Mc: %u Channel: %u Rank: %u Byte: %u\n",
                  RMPR_MIN_WIDTH,
                  Controller,
                  Channel,
                  Rank,
                  Byte
                  );
              }
              Group = (Param == RdTN) ? RxDqsNDelay : RxDqsPDelay;
              MrcGetSetStrobe (MrcData, Controller, Channel, Rank, Byte, Group, ReadFromCache, &GetSetVal);
              FinalDqs[Controller][Channel][Rank][Byte] = Center + (INT16) GetSetVal;
              MRC_DEBUG_MSG (
                Debug,
                MSG_LEVEL_NOTE,
                "  B%u:\t\t%d\t%d\t%d\t%d\t%d\n",
                Byte,
                Left,
                Right,
                lWidth,
                Center,
                FinalDqs[Controller][Channel][Rank][Byte]
                );
              // Update the MarginResult struct for future tests
              Margin = &Outputs->MarginResult[LastRxT][Rank][Controller][Channel][Byte][0];
              Margin[0] = ABS (10 * (Left  - Center));
              Margin[1] = ABS (10 * (Right - Center));

              // For PBD results
              for (Bit = 0; Bit < MAX_BITS; Bit++) {
                MarginBit[Rank][Controller][Channel][Byte][Bit][1] -= (INT8) Center; // Normalize margins
                MarginBit[Rank][Controller][Channel][Byte][Bit][0] -= (INT8) Center;
              }
            } // for Byte
          }
        } // for Channel
      } // for Controller

      for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
        for (Channel = 0; Channel < MaxChannel; Channel++) {
          if (MrcRankExist (MrcData, Controller, Channel, Rank)) {
            MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\nMC%u.C%u.R%u:", Controller, Channel, Rank);
            for (Byte = 0; Byte < Outputs->SdramCount; Byte++) {
              if (!MrcByteExist (MrcData, Controller, Channel, Byte)) {
                continue;
              }
              MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\nB%u:", Byte);
              for (Bit = 0; Bit < MAX_BITS; Bit++) {
                MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\t[%3d %3d]", MarginBit[Rank][Controller][Channel][Byte][Bit][0], MarginBit[Rank][Controller][Channel][Byte][Bit][1]);
              }
            }
          }
        }
      }
    }
  } // for Rank
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\n");
  if (!SimpleCheck) {
    // Now restore the rxdqsp and rxdqsn value.
    // Need to do it after all ranks are trained, because we need to keep the same DQS value on all ranks
    // during the training.
    for (Rank = 0; Rank < MAX_RANK_IN_CHANNEL; Rank++) {
      for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
        for (Channel = 0; Channel < MaxChannel; Channel++) {
          if (MrcRankExist (MrcData, Controller, Channel, Rank)) {
            for (Byte = 0; Byte < Outputs->SdramCount; Byte++) {
              GetSetValP = SaveRxDqsP[Rank][Controller][Channel][Byte]; // Restore RxDqsP
              GetSetValN = SaveRxDqsN[Rank][Controller][Channel][Byte]; // Restore RxDqsN
              MrcGetSetStrobe (MrcData, Controller, Channel, Rank, Byte, RxDqsPDelay, WriteToCache, &GetSetValP);
              MrcGetSetStrobe (MrcData, Controller, Channel, Rank, Byte, RxDqsNDelay, WriteToCache, &GetSetValN);
            }
          }
        }
      }
    }
    MrcFlushRegisterCachedData (MrcData);

    // Call for RX PBD centering
    if (((Param == RdTN) || (Param == RdTP)) && PerBit) {
      MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "PerBit1DCentering \n");
      // PerBit1DCentering (MrcData, Param, 10, 1, MRC_PRINTS_ON, MarginBit); // TestType = MPR
    }
  }
  // Clean up after Test.
  SetupMcMprConfig (MrcData, &SaveData, MRC_DISABLE);

  // Restore MC credits
  if (Gear4 && Lpddr) {
    for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
      for (Channel = 0; Channel < MaxChannel; Channel++) {
        if ((!MrcChannelExist (MrcData, Controller, Channel)) || IS_MC_SUB_CH (Lpddr, Channel)) {
          continue;
        }
        IpChannel = LP_IP_CH (Lpddr, Channel);
        Offset = OFFSET_CALC_MC_CH (MC0_CH0_CR_MC_RDB_CREDITS_REG, MC1_CH0_CR_MC_RDB_CREDITS_REG, Controller, MC0_CH1_CR_MC_RDB_CREDITS_REG, IpChannel);
        MrcWriteCR (MrcData, Offset, McRdbCreditsSave);
      }
    }
  }

  Status  = IoReset (MrcData);

  if ((MprStatus != mrcSuccess) && (Inputs->ExitOnFailure == 1)) {
    Status = MprStatus;
  }
  return Status;
}

/**

  Get RxDqs Timing Margins for each individual UI(Chunk)

  @param[in, out] MrcData            - Pointer to MRC global data.
  @param[in] DramDcaTrainingData     - Pointer to DRAM DCA training context
  @param[out] EyeWidthAllUi          - Per-UI eyewidth

  @retval mrcSucess if the margin completes
  @retval mrcFail if input parameter is invalid

**/
MrcStatus
GetRxDqsTimingMargins (
  IN  MrcParameters *const        MrcData,
  IN  DRAM_DCA_TRAINING_Data      *DramDcaTrainingData,
  OUT EYE_WIDTH_UI_ALL            *EyeWidthAllUi,
  IN  BOOLEAN                     Print
  )
{
  UINT8                       UiIndex;
  MrcOutput                   *Outputs;
  MrcDebug                    *Debug;
  UINT8                       MaxUiForMargin = 0;
  UINT8                       Param;

  Outputs = &MrcData->Outputs;
  Debug   = &Outputs->Debug;

  if ((DramDcaTrainingData->DcaType == DcaSingleOrTwoPhase) || (DramDcaTrainingData->DcaType == 0)) {
    MaxUiForMargin = 2;
  } else {
    MaxUiForMargin = MAX_UI / 4;
  }
  for (UiIndex = 0; UiIndex < MaxUiForMargin; UiIndex++) {

    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "GetRxDqsTimingMargins UI index %d\n", UiIndex);
    if ((DramDcaTrainingData->DcaType == DcaSingleOrTwoPhase) || (DramDcaTrainingData->DcaType == 0)) {
      if (UiIndex == 0) {
        Param = RdTP;
      } else {
        Param = RdTN;
      }
    } else {
      Param = RdT;
    }
    MrcReadrainingNormalDcaMarginAdvanced (MrcData, Param, Print, UiIndex, EyeWidthAllUi, FALSE);
    //MrcReadMprTrainingNormalDcaMargin (MrcData, Param, FALSE, Print, UiIndex, EyeWidthAllUi, FALSE, DramDcaTrainingData, FALSE);
  } // UiIndex
  return mrcSuccess;
}

/**

  Print eyewidths

  @param[in] MrcData                 - Pointer to MRC global data.
  @param[in] DramDcaTrainingData     - Pointer to DRAM DCA training context
  @param[out] EyeWidthAllUi          - Per-UI eyewidth

  @retval NA

**/
VOID
DisplayDcaEyeWdith (
  IN  MrcParameters *const          MrcData,
  IN  DRAM_DCA_TRAINING_Data        *DramDcaTrainingData,
  IN  EYE_WIDTH_UI_ALL              *EyeWidthAllUi
  )
{
  UINT8         Controller;
  UINT8         Channel;
  UINT32        ChannelMask;
  UINT8         Rank;
  UINT32        RankMask;
  UINT8         Strobe;
  UINT8         UiIndex;
  MrcOutput     *Outputs;
  MrcDebug      *Debug;
  UINT8         MaxUiForMargin;

  Outputs     = &MrcData->Outputs;
  Debug       = &Outputs->Debug;
  ChannelMask = DramDcaTrainingData->ChannelMask;

  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "RxDqsDelay Eye Width\n");
  if ((DramDcaTrainingData->DcaType == DcaSingleOrTwoPhase) || (DramDcaTrainingData->DcaType == 0)) {
    MaxUiForMargin = 2;
  } else {
    MaxUiForMargin = MAX_UI / 4;
  }
  for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
    for (Channel = 0; Channel < Outputs->MaxChannels; Channel++) {
      if (!MrcChannelExist (MrcData, Controller, Channel)) {
        continue;
      }
      if (((1 << Channel) & ChannelMask) != 0) {
        continue;
      }

      RankMask = DramDcaTrainingData->RankMask[Controller][Channel];

      for (Rank = 0; Rank < MAX_RANK_IN_CHANNEL; Rank++) {
        if (!(MrcRankExist (MrcData, Controller, Channel, Rank))) {
          continue;
        }
        if (((1 << Rank) & RankMask) != 0) {
          continue;
        }

        MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Mc%d.C%d.R%d.Strobe:", Controller, Channel, Rank);

        for (Strobe = 0; Strobe < Outputs->SdramCount; Strobe++) {

          MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "   %d", Strobe);
        } // Strobe

        MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\n");

        for (UiIndex = 0; UiIndex < MaxUiForMargin; UiIndex++) {

          MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "UI %d    ", UiIndex);
          for (Strobe = 0; Strobe < Outputs->SdramCount; Strobe++) {

            MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "  %3d",
              EyeWidthAllUi->EyeWidthAllStrobe[UiIndex].EyeWidth[Controller][Channel][Rank][Strobe]);

          } // Strobe

          MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\n");
        } // UiIndex
      } // Rank
    } // Dimm
  } // Channel
}


/**

  Peform sweep of the DRAM Duty Cycle Adjuster for a specified clock
  The clock is identified by the Group parameter.
  Valid Group parameter values are: DramDcaSingleorTwoClk, DramDcaQClk, DramDcaIbClk and DramDcaQbClk

  @param[in, out] MrcData            - Pointer to MRC global data.
  @param[in] DramDcaTrainingData     - Pointer to DRAM DCA training context
  @param[out] EyeWidthAllUi          - Per-UI eyewidth

  @retval mrcSucess if the sweep completes
  @retval mrcFail if input parameter is invalid

**/
MrcStatus
RunDramDcaTraining (
  IN  MrcParameters *const         MrcData,
  IN  DRAM_DCA_TRAINING_Data       *DramDcaTrainingData,
  OUT EYE_WIDTH_UI_ALL             EyeWidthAllUi[DRAM_DCA_SWEEP_SIZE]
  )
{
  UINT8                Controller;
  UINT8                RankMask;
  UINT8                Channel;
  UINT8                Rank;
  INT8                 DcaSetting;
  UINT8                DcaSettingIndex;
  TOptParamOffset      Group;
  MrcDebug             *Debug;
  MrcOutput            *Outputs;
  UINT32               Data = 0;
  BOOLEAN              Ddr5;
  BOOLEAN              Lpddr5;
  MrcStatus            Status;
  MrcChannelOut        *ChannelOut;
  UINT16               *MrPtr;
  UINT8                MrIndex;
  BOOLEAN              PdaMode;
  UINT16               DeviceMask;
  MrcDebugMsgLevel     LevelStore;
  static const UINT8   OptParam[] = { OptDimmDcaQclk, OptDimmDcaIBclk, OptDimmDcaQBclk};

  Outputs = &MrcData->Outputs;
  Group = DramDcaTrainingData->Group;
  Debug = &Outputs->Debug;
  Ddr5 = (Outputs->DdrType == MRC_DDR_TYPE_DDR5);
  Lpddr5 = (Outputs->DdrType == MRC_DDR_TYPE_LPDDR5);
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "RunDramDcaTraining - Group: %d\n", Group);
  if (Group > ARRAY_COUNT (OptParam)) {
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Group %d is not valid %d\n", Group);
    return mrcWrongInputParameter;
  }

  for (DcaSettingIndex = 0; DcaSettingIndex < DRAM_DCA_SWEEP_SIZE; DcaSettingIndex += DRAM_DCA_SWEEP_STEP_SIZE) {
    //
    // Convert sweep index to DCA value to be programmed
    //
    DcaSetting = SWEEP_INDEX_TO_DCA_VALUE (DcaSettingIndex);

    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "DCA Setting = %d \n", DcaSetting);

    for (Rank = 0; Rank < MAX_RANK_IN_CHANNEL; Rank++) {

      for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
        for (Channel = 0; Channel < MAX_CHANNEL; Channel++) {
          if (!(MrcRankExist (MrcData, Controller, Channel, Rank))) {
            continue;
          }
          if (((1 << Rank) & DramDcaTrainingData->RankMask[Controller][Channel]) != 0) {
            continue;
          }
          ChannelOut = &Outputs->Controller[Controller].Channel[Channel];
        //ChannelOut->Mr43PdaEnabled = FALSE;
        //ChannelOut->Mr44PdaEnabled = FALSE;
          RankMask = 1 << Rank;
          MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Mc%d.C%d.R%d: %d\n", Controller, Channel, Rank, DcaSetting);
          if (Ddr5) {
            RankMask = 1 << Rank;
            MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Group %d", Group);
            PdaMode = FALSE;
            DeviceMask = 1;
            MrcSetDramDca (MrcData, Controller, Channel, RankMask, DeviceMask, DcaSetting, TRUE, PdaMode, Group);
          } else {
            //  Data = (UINT8) ChannelOut->Dimm[dDIMM0].Rank[Rank % MAX_RANK_IN_DIMM].MR[mrIndexMR30];
            MrIndex = mrIndexMR30;
            MrPtr = ChannelOut->Dimm[dDIMM0].Rank[Rank % MAX_RANK_IN_DIMM].MR;

            Data = 0;
            if (DcaSetting > 0) {
              Data = LP5_MR30_POSTIVE_OFFSET + DcaSetting;
            } else {
              Data = ABS (DcaSetting);
            }

            Data = (Data << 4) | Data;
            MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "MrcIssueMrw Mc0%d.C%d.R%d mr30 %d \n", Controller, Channel, Rank, Data);
            Status = MrcIssueMrw (MrcData, Controller, Channel, Rank, mrMR30, Data, TRUE);
            if (Status != mrcSuccess) {
              return Status;
            }
            MrPtr[MrIndex] = (UINT8) Data;
            MrcWait (MrcData,10);
          }
        } // Channel
      }  // Controller
    } // Rank
    if (Lpddr5) {
      // Reset DDR
      Status = MrcResetSequence (MrcData);
      MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, " MrcJedecWCKTraining \n");
      LevelStore = Debug->Level;
      //only print error and warning.
      Debug->Level = MSG_LEVEL_WARNING;
      MrcJedecWCKTraining (MrcData);
      //restore the debug level.
      Debug->Level = LevelStore;
    }
    if (DramDcaTrainingData->DcaType != DcaFourPhase) {
      // Reset DDR
      Status = MrcResetSequence (MrcData);
    }
    //
    // Setup and execute the test on all channels
    //
    GetRxDqsTimingMargins (MrcData, DramDcaTrainingData, &EyeWidthAllUi[DcaSettingIndex], MRC_PRINTS_OFF);

    DisplayDcaEyeWdith (MrcData, DramDcaTrainingData, &EyeWidthAllUi[DcaSettingIndex]);
  } // DcaSetting
  return mrcSuccess;
}

#if 0
/**
  Find the optimal DCA setting index for each Channel/Rank/Strobe

  @param[in] MrcData                    - Pointer to MRC global data.
  @param[in]  DramDcaTrainingData       - Pointer to DRAM DCA training context
  @param[in]  EyeWidthPerDcaSetting     - Array of eye widths for eacy UI found at each DCA setting
  @param[out] OptimalDcaSettingIndex    - Calculated optimal DCA setting index per strobe

  @retval N/A
**/
VOID
HandleDramDcaTrainingResults (
  IN  MrcParameters *const             MrcData,
  IN  DRAM_DCA_TRAINING_Data           *DramDcaTrainingData,
  IN  EYE_WIDTH_UI_ALL                 EyeWidthAllUi[DRAM_DCA_SWEEP_SIZE],
  OUT UINT8                            OptimalDcaSettingIndex[MAX_CONTROLLER][MAX_CHANNEL][MAX_RANK_IN_CHANNEL][MAX_STROBE]
  )
{

  UINT8                       Channel;
  UINT8                       Rank;
  UINT8                       Strobe;
  UINT8                       UiIndex;
  UINT8                       DcaSettingIndex;
  UINT8                       BestDcaSettingIndex;
  INT16                       EyeWidthDiff = 0;
  INT16                       MinEyeWidthDiff;
  INT16                       EwUi[MAX_UI / 2];
  INT16                       EwUiR0;
  INT16                       EwUiR1;
  DCA_GT                      Group;
  UINT8                       ClkPhaseAlignment;
  UINT16                      MaxMinimumEw;
  UINT16                      MaxMaxEw;
  UINT16                      MinEw;
  UINT16                      MaxEw;
  UINT16                      MinEwofOneDcaCode;
  UINT16                      MaxMinEw2Phase;
  UINT8                       Controller;
  MrcOutput                   *Outputs;
  MrcDebug                    *Debug;
  INT16                       DcaSetting;
  UINT8                       MaxUiForMargin;
  UINT8                       Filtering;
  DCA_SCORE                   DcaScoreData;
  BOOLEAN                     Ddr5;
  BOOLEAN                     Lpddr5;
  const MRC_FUNCTION          *MrcCall;
  const MrcInput              *Inputs;
  UINT16                      EwSum[DRAM_DCA_SWEEP_SIZE];

  EwUiR0 = 0;
  EwUiR1 = 0;
  MaxMinEw2Phase = 0;
  Inputs = &MrcData->Inputs;
  Outputs = &MrcData->Outputs;
  Debug   = &Outputs->Debug;
  Ddr5    = (Outputs->DdrType == MRC_DDR_TYPE_DDR5);
  Lpddr5  = (Outputs->DdrType == MRC_DDR_TYPE_LPDDR5);
  Group   = DramDcaTrainingData->Group;
  Filtering = FILTERING_THREE;
  MrcCall = Inputs->Call.Func;

  MrcCall->MrcSetMem ((UINT8 *)&EwSum, sizeof (EwSum), 0);
  MrcCall->MrcSetMem ((UINT8 *)&EwUi,  sizeof (EwUi), 0);
  if (DramDcaTrainingData->DcaType == DcaSingleOrTwoPhase) {
    MaxUiForMargin = 2;
  } else {
    MaxUiForMargin = MAX_UI / 4;
  }

  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "HandleDramDcaTrainingResults Group %d: \n", Group);
  for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
    for (Channel = 0; Channel < Outputs->MaxChannels; Channel++) {
      if (!MrcChannelExist (MrcData, Controller, Channel)) {
        continue;
      }
      if (((1 << Channel) & DramDcaTrainingData->ChannelMask) != 0) {
        continue;
      }

      for (Rank = 0; Rank < MAX_RANK_IN_CHANNEL; Rank++) {
        if (!MrcRankExist (MrcData, Controller, Channel, Rank)) {
          continue;
        }
        if (((1 << Rank) & DramDcaTrainingData->RankMask[Controller][Channel]) != 0) {
          continue;
        }

        for (Strobe = 0; Strobe < Outputs->SdramCount; Strobe++) {
          MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Mc%d.C%d.R%d.S%d.G%d: \n", Controller, Channel, Rank, Strobe, Group);

          //
          // Get clock phase alignment for this device
          //
          ClkPhaseAlignment = DramDcaTrainingData->ClkPhaseAlignment[Strobe].DcaClkAlignment[Controller][Channel][Rank];
          if (Ddr5) {
            MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "ClkPhaseAlignment: %d \n", ClkPhaseAlignment);
          }
          //
          // Find optimal DCA setting for this device
          //
          MinEyeWidthDiff = ((INT16) 0x7FFF);
          BestDcaSettingIndex = DRAM_DCA_SWEEP_SIZE / 2;
          MaxMinimumEw = 0;
          MaxMaxEw = 0;
          MinEw = 0;
          MaxEw = 0;
          MrcCall->MrcSetMem ((UINT8 *)&DcaScoreData, sizeof (DcaScoreData), 0);

          if ((Lpddr5) || (MaxUiForMargin == 2)) {
            for (DcaSettingIndex = 0; DcaSettingIndex < DRAM_DCA_SWEEP_SIZE; DcaSettingIndex++) {
              DcaSetting = SWEEP_INDEX_TO_DCA_VALUE (DcaSettingIndex);

              for (UiIndex = 0; UiIndex < MaxUiForMargin; UiIndex++) {
                EwUi[UiIndex] = EyeWidthAllUi[DcaSettingIndex].EyeWidthAllStrobe[UiIndex].EyeWidth[Controller][Channel][Rank][Strobe];
              }

              //[0] stores the min value, [1] stores the max value
              DcaScoreData.EwForScoring[DcaSettingIndex][0] = EwUi[0];
              DcaScoreData.EwForScoring[DcaSettingIndex][1] = EwUi[1];

            }
            //calculate the score

            for (DcaSettingIndex = 0; DcaSettingIndex < DRAM_DCA_SWEEP_SIZE; DcaSettingIndex++) {
              DcaSetting = SWEEP_INDEX_TO_DCA_VALUE (DcaSettingIndex);

              if (DcaSettingIndex == 0) {
                DcaScoreData.DcaScore[DcaSettingIndex][0] = DcaScoreData.EwForScoring[0][0] + DcaScoreData.EwForScoring[0][0] + DcaScoreData.EwForScoring[1][0];
                DcaScoreData.DcaScore[DcaSettingIndex][1] = DcaScoreData.EwForScoring[0][1] + DcaScoreData.EwForScoring[0][1] + DcaScoreData.EwForScoring[1][1];
              } else if (DcaSettingIndex == (DRAM_DCA_SWEEP_SIZE - 1)) {
                DcaScoreData.DcaScore[DcaSettingIndex][0] = DcaScoreData.EwForScoring[DcaSettingIndex - 1][0] + DcaScoreData.EwForScoring[DcaSettingIndex][0] + DcaScoreData.EwForScoring[DcaSettingIndex][0];
                DcaScoreData.DcaScore[DcaSettingIndex][1] = DcaScoreData.EwForScoring[DcaSettingIndex - 1][1] + DcaScoreData.EwForScoring[DcaSettingIndex][1] + DcaScoreData.EwForScoring[DcaSettingIndex][1];
              } else {
                DcaScoreData.DcaScore[DcaSettingIndex][0] = DcaScoreData.EwForScoring[DcaSettingIndex - 1][0] + DcaScoreData.EwForScoring[DcaSettingIndex][0] + DcaScoreData.EwForScoring[DcaSettingIndex + 1][0];
                DcaScoreData.DcaScore[DcaSettingIndex][1] = DcaScoreData.EwForScoring[DcaSettingIndex - 1][1] + DcaScoreData.EwForScoring[DcaSettingIndex][1] + DcaScoreData.EwForScoring[DcaSettingIndex + 1][1];
              }
            }
          }
          MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "DcaSetting      ");
          for (UiIndex = 0; UiIndex < MaxUiForMargin; UiIndex++) {
            MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "  UI%d       ", UiIndex);
          }
          if (MaxUiForMargin == 2) {
            for (UiIndex = 0; UiIndex < MaxUiForMargin; UiIndex++) {
              MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "UI%dAverage  ", UiIndex);
            }
          }
          for (DcaSettingIndex = 0; DcaSettingIndex < DRAM_DCA_SWEEP_SIZE; DcaSettingIndex++) {

            DcaSetting = SWEEP_INDEX_TO_DCA_VALUE (DcaSettingIndex);
            MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\n        %2d", DcaSetting);

            for (UiIndex = 0; UiIndex < MaxUiForMargin; UiIndex++) {
              EwUi[UiIndex] = EyeWidthAllUi[DcaSettingIndex].EyeWidthAllStrobe[UiIndex].EyeWidth[Controller][Channel][Rank][Strobe];
              MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "        %3d ", EwUi[UiIndex] );
            }
            for (UiIndex = 0; UiIndex < MaxUiForMargin; UiIndex++) {
              MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "        %3d  ", DcaScoreData.DcaScore[DcaSettingIndex][UiIndex] );
            }
            switch (Group) {
              case DramDcaSingleorTwoClk:
              case DramDcaIbClk:
                //
                // CLK single/double-phase
                // IBCLK 4-phase
                // ICLK aligned: Diff = Max(Abs([UI0 + UI1] - [UI2 + UI3]), Abs([UI4 + UI5] - [UI6 + UI7]))
                // IBCLK aligned: Diff = Max(Abs([UI3 - UI0]), Abs([7] - [4]))
                //
                if ((Ddr5) && (MaxUiForMargin != 2)) {
                  //EyeWidthDiff = ABS ((EwUi[0] + EwUi[1] + EwUi[4] + EwUi[5]) - (EwUi[2] + EwUi[3] + EwUi[6] + EwUi[7]));
                  if (DramDcaTrainingData->DcaType == DcaFourPhase) {
                    if (MaxUiForMargin == (MAX_UI / 2)) {
                      EyeWidthDiff = MAX (ABS ((EwUi[0] + EwUi[1]) - (EwUi[2] + EwUi[3])), ABS ((EwUi[4] + EwUi[5]) - (EwUi[6] + EwUi[7])));
                    } else {
                      EyeWidthDiff = ABS ((EwUi[0] + EwUi[1]) - (EwUi[2] + EwUi[3]));
                    }
                  } else if (DramDcaTrainingData->DcaType == DcaSingleOrTwoPhase) {
                    MinEwofOneDcaCode = EwUi[0];
                    EwSum[DcaSettingIndex] = 0;
                    for (UiIndex = 0; UiIndex < MaxUiForMargin; UiIndex++) {
                      if (MinEwofOneDcaCode > EwUi[UiIndex]) {
                        MinEwofOneDcaCode = EwUi[UiIndex];

                      }
                      EwSum[DcaSettingIndex] = EwSum[DcaSettingIndex] + EwUi[UiIndex];
                    }
                    if (DcaSettingIndex == 0) {
                      MaxMinEw2Phase = MinEwofOneDcaCode;

                    } else {
                      if (MaxMinEw2Phase == MinEwofOneDcaCode) {
                        if (EwSum[BestDcaSettingIndex] == EwSum[DcaSettingIndex]) {
                          //check who is close to 0
                          if ((ABS (SWEEP_INDEX_TO_DCA_VALUE (DcaSettingIndex) - 0)) < ABS (SWEEP_INDEX_TO_DCA_VALUE (BestDcaSettingIndex) - 0)) {
                            BestDcaSettingIndex = DcaSettingIndex;
                            MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "DcaSetting %2d is closer to 0", SWEEP_INDEX_TO_DCA_VALUE (DcaSettingIndex));
                          }
                        } else if (EwSum[BestDcaSettingIndex] < EwSum[DcaSettingIndex]) {
                          BestDcaSettingIndex = DcaSettingIndex;
                        }

                      } else if (MaxMinEw2Phase < MinEwofOneDcaCode) {
                        MaxMinEw2Phase = MinEwofOneDcaCode;
                        BestDcaSettingIndex = DcaSettingIndex;
                      }
                    }
                    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "   MinEw: %3d, MaxMinEw: %3d", MinEwofOneDcaCode, MaxMinEw2Phase);

                  }
                } else {
                  if (Filtering == FILTERING_ONE) {
                    if (EwUi[0] < EwUi[1]) {
                      MinEw = EwUi[0];
                      MaxEw = EwUi[1];
                    } else {
                      MinEw = EwUi[1];
                      MaxEw = EwUi[0];
                    }
                    if (MaxMinimumEw == MinEw) {
                      if (MaxMaxEw < MaxEw) {
                        BestDcaSettingIndex = DcaSettingIndex;
                        MaxMaxEw = MaxEw;
                      }
                    } else if (MaxMinimumEw < MinEw) {
                      MaxMinimumEw = MinEw;
                      BestDcaSettingIndex = DcaSettingIndex;
                      MaxMaxEw = MaxEw;
                    }
                  } else {
#if 0
                    if (DcaScoreData.DcaScore[DcaSettingIndex] == MaxScore) {
                      MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "DCA %d have the same score as DCA %d ", SWEEP_INDEX_TO_DCA_VALUE (DcaSettingIndex), SWEEP_INDEX_TO_DCA_VALUE (BestDcaSettingIndex));
                      if (DcaScoreData.EwForScoring[DcaSettingIndex][1] == DcaScoreData.EwForScoring[BestDcaSettingIndex][1]) {
                        //check who is close to 0
                        if ((ABS (SWEEP_INDEX_TO_DCA_VALUE (DcaSettingIndex) - 0)) < ABS (SWEEP_INDEX_TO_DCA_VALUE (BestDcaSettingIndex) - 0)) {
                          BestDcaSettingIndex = DcaSettingIndex;
                          MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "DcaSetting %d is closer to 0", SWEEP_INDEX_TO_DCA_VALUE (DcaSettingIndex));
                        }
                      } else if (DcaScoreData.EwForScoring[DcaSettingIndex][1] > DcaScoreData.EwForScoring[BestDcaSettingIndex][1]) {
                        BestDcaSettingIndex = DcaSettingIndex;
                      }
                    } else if (DcaScoreData.DcaScore[DcaSettingIndex] > MaxScore) {
                      MaxScore = DcaScoreData.DcaScore[DcaSettingIndex];
                      BestDcaSettingIndex = DcaSettingIndex;
                    }
#endif //if 0

                    EyeWidthDiff = ABS ((DcaScoreData.DcaScore[DcaSettingIndex][0]) - (DcaScoreData.DcaScore[DcaSettingIndex][1]));
                  }
                }
                break;

              case  DramDcaQClk:
                //
                // QCLK
                // ICLK aligned:  Diff = Max(Abs(UI0-UI1), Abs(UI4 - UI5))
                // IBCLK aligned: Diff = Max(Abs(UI2-UI3), Abs(UI6 - UI7))
                //
                #if 0
                if (MaxUiForMargin == (MAX_UI / 2)) {
                  if (ClkPhaseAlignment == ICLK_PHASE_ALIGNED) {
                    EyeWidthDiff = ABS ((EwUi[0] + EwUi[4]) - (EwUi[1] + EwUi[5]));
                  } else {
                    EyeWidthDiff = ABS ((EwUi[2] + EwUi[6]) - (EwUi[3] + EwUi[7]));
                  }
                } else {
                  if (ClkPhaseAlignment == ICLK_PHASE_ALIGNED) {
                    EyeWidthDiff = ABS ((EwUi[0]) - (EwUi[1]));
                  } else {
                    EyeWidthDiff = ABS ((EwUi[2]) - (EwUi[3]));
                  }
                }
              #endif //if 0

                if (ClkPhaseAlignment == ICLK_PHASE_ALIGNED) {
                  EwUiR0 = EwUi[0];
                  EwUiR1 = EwUi[1];
                } else {
                  EwUiR0 = EwUi[2];
                  EwUiR1 = EwUi[3];
                }
                if (EwUiR0 < EwUiR1) {
                  MinEw = EwUiR0;
                  MaxEw = EwUiR1;
                } else {
                  MinEw = EwUiR1;
                  MaxEw = EwUiR0;
                }
                if (MaxMinimumEw == MinEw) {
                  if (MaxMaxEw == MaxEw) {
                    //check who is close to 0
                    if ((ABS (SWEEP_INDEX_TO_DCA_VALUE (DcaSettingIndex) - 0)) < ABS (SWEEP_INDEX_TO_DCA_VALUE (BestDcaSettingIndex) - 0)) {
                      BestDcaSettingIndex = DcaSettingIndex;
                      MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "DcaSetting %2d is closer to 0", SWEEP_INDEX_TO_DCA_VALUE (DcaSettingIndex));
                    }
                  } else if (MaxMaxEw < MaxEw) {
                    BestDcaSettingIndex = DcaSettingIndex;
                    MaxMaxEw = MaxEw;
                  }
                } else if (MaxMinimumEw < MinEw) {
                  MaxMinimumEw = MinEw;
                  BestDcaSettingIndex = DcaSettingIndex;
                  MaxMaxEw = MaxEw;
                }
                MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "   MinEw: %3d, MaxMinEw: %3d", MinEw, MaxMinimumEw);

                break;

              case DramDcaQbClk:
                //
                // QBCLK
                // ICLK aligned:  Diff = Max(Abs(UI2-UI3), Abs(UI6 - UI7))
                // IBCLK aligned: Diff = Max(Abs(UI0-UI1), Abs(UI4 - UI5))
                //
                #if 0
                if (MaxUiForMargin == (MAX_UI / 2)) {
                  if (ClkPhaseAlignment == ICLK_PHASE_ALIGNED) {
                    EyeWidthDiff = ABS ((EwUi[2] + EwUi[6]) - (EwUi[3] + EwUi[7]));
                  } else {
                    EyeWidthDiff = ABS ((EwUi[0] + EwUi[4]) - (EwUi[1] + EwUi[5]));
                  }
                } else {
                  if (ClkPhaseAlignment == ICLK_PHASE_ALIGNED) {
                    EyeWidthDiff = ABS ((EwUi[2]) - (EwUi[3]));
                  } else {
                    EyeWidthDiff = ABS ((EwUi[0]) - (EwUi[1]));
                  }
                }
                #endif //if 0
                if (ClkPhaseAlignment == ICLK_PHASE_ALIGNED) {
                  EwUiR0 = EwUi[2];
                  EwUiR1 = EwUi[3];
                } else {
                  EwUiR0 = EwUi[0];
                  EwUiR1 = EwUi[1];
                }
                if (EwUiR0 < EwUiR1) {
                  MinEw = EwUiR0;
                  MaxEw = EwUiR1;
                } else {
                  MinEw = EwUiR1;
                  MaxEw = EwUiR0;
                }
                if (MaxMinimumEw == MinEw) {
                  if (MaxMaxEw == MaxEw) {
                    //check who is close to 0
                    if ((ABS (SWEEP_INDEX_TO_DCA_VALUE (DcaSettingIndex) - 0)) < ABS (SWEEP_INDEX_TO_DCA_VALUE (BestDcaSettingIndex) - 0)) {
                      BestDcaSettingIndex = DcaSettingIndex;
                      MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "DcaSetting %2d is closer to 0", SWEEP_INDEX_TO_DCA_VALUE (DcaSettingIndex));
                    }
                  } else if (MaxMaxEw < MaxEw) {
                    BestDcaSettingIndex = DcaSettingIndex;
                    MaxMaxEw = MaxEw;
                  }
                } else if (MaxMinimumEw < MinEw) {
                  MaxMinimumEw = MinEw;
                  BestDcaSettingIndex = DcaSettingIndex;
                  MaxMaxEw = MaxEw;
                }
                MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "   MinEw: %3d, MaxMinEw: %3d", MinEw, MaxMinimumEw);

                break;
              default:
                EyeWidthDiff = ((INT16) 0x7FFF);
                MRC_DEBUG_MSG (Debug, MSG_LEVEL_ERROR, "Group %d is invalid\n", Group);
                break;
            }
            if (((((Ddr5) && (MaxUiForMargin != 2)) && (DramDcaTrainingData->DcaType == DcaFourPhase)) && (Group == DramDcaIbClk)) || (MaxUiForMargin == 2)) {
              MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "   EyeWidthDiff:  %d  ", EyeWidthDiff);

              if (MinEyeWidthDiff == EyeWidthDiff) {
                //check who is close to 0
                if ((ABS (SWEEP_INDEX_TO_DCA_VALUE (DcaSettingIndex) - 0)) < ABS (SWEEP_INDEX_TO_DCA_VALUE (BestDcaSettingIndex) - 0)) {
                  BestDcaSettingIndex = DcaSettingIndex;
                  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "DcaSetting %2d is closer to 0", SWEEP_INDEX_TO_DCA_VALUE (DcaSettingIndex));
                }
              } else if (MinEyeWidthDiff > EyeWidthDiff) {
                MinEyeWidthDiff = EyeWidthDiff;
                BestDcaSettingIndex = DcaSettingIndex;
              }
            }
          } // DcaSettingIndex
#if 0
          if (Ddr5) {
            if (DramDcaTrainingData->DcaType == DcaFourPhase) {
              MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\nMc%d.C%d.R%d.S%d.G%d:MinEyeWidthDiff:  %d:  ", Controller, Channel, Rank, Strobe, Group, MinEyeWidthDiff);
            } else if (DramDcaTrainingData->DcaType == DcaSingleOrTwoPhase) {
              MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\nMax Minimum Ew:  %d:  ", MaxMinEw2Phase);
            }
          } else {
            MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Max Minimum Ew:  %d:  ", MaxMinimumEw);
          }
#endif //if 0
          MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\nMc%d.C%d.R%d.S%d.G%d:MinEyeWidthDiff:  %d:  ", Controller, Channel, Rank, Strobe, Group, MinEyeWidthDiff);

          DcaSetting = SWEEP_INDEX_TO_DCA_VALUE (BestDcaSettingIndex);
          MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "BestDcaSettingIndex:  %d DcaSetting: %d \n", BestDcaSettingIndex, DcaSetting);
          OptimalDcaSettingIndex[Controller][Channel][Rank][Strobe] = BestDcaSettingIndex;
        } // Strobe
      } // Rank
    } // Dimm
  } // Channel
}
#endif //if 0

/**
  Find the optimal DCA setting for each Channel/Rank/Device

  @param[in]  MrcData                   - Pointer to MRC global data.
  @param[in]  DramDcaTrainingData       - Pointer to DRAM DCA training context
  @param[in]  EyeWidthPerDcaSetting     - Array of eye widths for eacy UI found at each DCA setting
  @param[out] OptimalDcaSettingIndex    - Calculated optimal DCA setting index per strobe

  @retval N/A
**/
VOID
HandleDramDcaTrainingResults (
  IN  MrcParameters *const             MrcData,
  IN  DRAM_DCA_TRAINING_Data           *DramDcaTrainingData,
  IN  EYE_WIDTH_UI_ALL                 EyeWidthAllUi[DRAM_DCA_SWEEP_SIZE],
  OUT UINT8                            OptimalDcaSettingIndex[MAX_CONTROLLER][MAX_CHANNEL][MAX_RANK_IN_CHANNEL][MAX_STROBE]
  )
{

  UINT8                       Channel;
  UINT8                       Rank;
  UINT8                       Strobe;
  UINT8                       UiIndex;
  UINT8                       DcaSettingIndex;
  UINT8                       BestDcaSettingIndex;
  INT16                       EwUi[MAX_UI / 2];
  INT16                       EwUiR0;
  INT16                       EwUiR1;
  DCA_GT                      Group;
  UINT8                       ClkPhaseAlignment;
  UINT16                      MaxMinimumEw;
  UINT16                      MaxMaxEw;
  UINT16                      MinEw;
  UINT16                      MaxEw;
  UINT8                       Controller;
  MrcOutput                   *Outputs;
  MrcDebug                    *Debug;
  INT16                       DcaSetting;
  UINT8                       MaxUiForMargin;
  DCA_SCORE                   DcaScoreData;
  BOOLEAN                     Ddr5;
  const MRC_FUNCTION          *MrcCall;
  const MrcInput              *Inputs;
  UINT16                      EwSum[DRAM_DCA_SWEEP_SIZE];

  EwUiR0 = 0;
  EwUiR1 = 0;
  Inputs = &MrcData->Inputs;
  Outputs = &MrcData->Outputs;
  Debug   = &Outputs->Debug;
  Ddr5    = (Outputs->DdrType == MRC_DDR_TYPE_DDR5);
  Group   = DramDcaTrainingData->Group;
  MrcCall = Inputs->Call.Func;

  MrcCall->MrcSetMem ((UINT8 *)&EwSum, sizeof (EwSum), 0);
  MrcCall->MrcSetMem ((UINT8 *)&EwUi,  sizeof (EwUi), 0);
  if (DramDcaTrainingData->DcaType == DcaSingleOrTwoPhase) {
    MaxUiForMargin = 2;
  } else {
    MaxUiForMargin = MAX_UI / 4;
  }

  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "HandleDramDcaTrainingResults Group %d: \n", Group);
  for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
    for (Channel = 0; Channel < Outputs->MaxChannels; Channel++) {
      if (!MrcChannelExist (MrcData, Controller, Channel)) {
        continue;
      }
      if (((1 << Channel) & DramDcaTrainingData->ChannelMask) != 0) {
        continue;
      }

      for (Rank = 0; Rank < MAX_RANK_IN_CHANNEL; Rank++) {
        if (!MrcRankExist (MrcData, Controller, Channel, Rank)) {
          continue;
        }
        if (((1 << Rank) & DramDcaTrainingData->RankMask[Controller][Channel]) != 0) {
          continue;
        }

        for (Strobe = 0; Strobe < Outputs->SdramCount; Strobe++) {
          MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Mc%d.C%d.R%d.S%d.G%d: \n", Controller, Channel, Rank, Strobe, Group);

          //
          // Get clock phase alignment for this device
          //
          ClkPhaseAlignment = DramDcaTrainingData->ClkPhaseAlignment[Strobe].DcaClkAlignment[Controller][Channel][Rank];
          if (Ddr5) {
            MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "ClkPhaseAlignment: %d \n", ClkPhaseAlignment);
          }
          //
          // Find optimal DCA setting for this device
          //
          //MinEyeWidthDiff = ((INT16) 0x7FFF);
          BestDcaSettingIndex = DRAM_DCA_SWEEP_SIZE / 2;
          MaxMinimumEw = 0;
          MaxMaxEw = 0xFF;
          MinEw = 0;
          MaxEw = 0;
          MrcCall->MrcSetMem ((UINT8 *)&DcaScoreData, sizeof (DcaScoreData), 0);

          for (DcaSettingIndex = 0; DcaSettingIndex < DRAM_DCA_SWEEP_SIZE; DcaSettingIndex++) {
            DcaSetting = SWEEP_INDEX_TO_DCA_VALUE (DcaSettingIndex);

            for (UiIndex = 0; UiIndex < MaxUiForMargin; UiIndex++) {
              EwUi[UiIndex] = EyeWidthAllUi[DcaSettingIndex].EyeWidthAllStrobe[UiIndex].EyeWidth[Controller][Channel][Rank][Strobe];
            }
            switch (Group) {
              case DramDcaSingleorTwoClk:
                //
                // CLK single/double-phase
                // EyeWidthDiff = ABS ((EwUi[0]) - (EwUi[1]);
                //
                EwUiR0 = EwUi[0];
                EwUiR1 = EwUi[1];
                break;

              case DramDcaIbClk:
                //
                // IBCLK 4-phase
                // ICLK aligned: Diff = ABS ([UI0 + UI1] - [UI2 + UI3])
                // IBCLK aligned: Diff = ABS ([UI0 + UI1]) - ([3] + [2])
                // EyeWidthDiff = ABS (EwUi[0] + EwUi[1]) - (EwUi[2] + EwUi[3]);
                //
                EwUiR0 = EwUi[0] + EwUi[1];
                EwUiR1 = EwUi[2] + EwUi[3];
                break;

              case  DramDcaQClk:
                //
                // QCLK
                // ICLK aligned:  Diff = ABS (UI0 - UI1)
                // IBCLK aligned: Diff = ABS (UI2 - UI3)
                //
                if (ClkPhaseAlignment == ICLK_PHASE_ALIGNED) {
                  EwUiR0 = EwUi[0];
                  EwUiR1 = EwUi[1];
                } else {
                  EwUiR0 = EwUi[2];
                  EwUiR1 = EwUi[3];
                }
                break;

              case DramDcaQbClk:
                //
                // QBCLK
                // ICLK aligned:  Diff = ABS (UI2 - UI3)
                // IBCLK aligned: Diff = ABS (UI0 - UI1)
                //
                if (ClkPhaseAlignment == ICLK_PHASE_ALIGNED) {
                  EwUiR0 = EwUi[2];
                  EwUiR1 = EwUi[3];
                } else {
                  EwUiR0 = EwUi[0];
                  EwUiR1 = EwUi[1];
                }
                break;

              default:
                //EyeWidthDiff = ((INT16) 0x7FFF);
                MRC_DEBUG_MSG (Debug, MSG_LEVEL_ERROR, "Group %d is invalid\n", Group);
                break;
            }
            DcaScoreData.EwForScoring[DcaSettingIndex][0] = EwUiR0;
            DcaScoreData.EwForScoring[DcaSettingIndex][1] = EwUiR1;

          }
          //calculate the score

          for (DcaSettingIndex = 0; DcaSettingIndex < DRAM_DCA_SWEEP_SIZE; DcaSettingIndex++) {
            DcaSetting = SWEEP_INDEX_TO_DCA_VALUE (DcaSettingIndex);

            if (DcaSettingIndex == 0) {
              DcaScoreData.DcaScore[DcaSettingIndex][0] = DcaScoreData.EwForScoring[0][0] + DcaScoreData.EwForScoring[0][0] + DcaScoreData.EwForScoring[1][0];
              DcaScoreData.DcaScore[DcaSettingIndex][1] = DcaScoreData.EwForScoring[0][1] + DcaScoreData.EwForScoring[0][1] + DcaScoreData.EwForScoring[1][1];
            } else if (DcaSettingIndex == (DRAM_DCA_SWEEP_SIZE - 1)) {
              DcaScoreData.DcaScore[DcaSettingIndex][0] = DcaScoreData.EwForScoring[DcaSettingIndex - 1][0] + DcaScoreData.EwForScoring[DcaSettingIndex][0] + DcaScoreData.EwForScoring[DcaSettingIndex][0];
              DcaScoreData.DcaScore[DcaSettingIndex][1] = DcaScoreData.EwForScoring[DcaSettingIndex - 1][1] + DcaScoreData.EwForScoring[DcaSettingIndex][1] + DcaScoreData.EwForScoring[DcaSettingIndex][1];
            } else {
              DcaScoreData.DcaScore[DcaSettingIndex][0] = DcaScoreData.EwForScoring[DcaSettingIndex - 1][0] + DcaScoreData.EwForScoring[DcaSettingIndex][0] + DcaScoreData.EwForScoring[DcaSettingIndex + 1][0];
              DcaScoreData.DcaScore[DcaSettingIndex][1] = DcaScoreData.EwForScoring[DcaSettingIndex - 1][1] + DcaScoreData.EwForScoring[DcaSettingIndex][1] + DcaScoreData.EwForScoring[DcaSettingIndex + 1][1];
            }
          }
          MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "DcaSetting      ");
          for (UiIndex = 0; UiIndex < MaxUiForMargin; UiIndex++) {
            MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "  UI%d       ", UiIndex);
          }

          for (UiIndex = 0; UiIndex < MAX_EW_DIFF_NUM; UiIndex++) {
            MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "UI%dAverage  ", UiIndex);
          }

          for (DcaSettingIndex = 0; DcaSettingIndex < DRAM_DCA_SWEEP_SIZE; DcaSettingIndex++) {

            DcaSetting = SWEEP_INDEX_TO_DCA_VALUE (DcaSettingIndex);
            MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\n        %2d", DcaSetting);

            for (UiIndex = 0; UiIndex < MaxUiForMargin; UiIndex++) {
              EwUi[UiIndex] = EyeWidthAllUi[DcaSettingIndex].EyeWidthAllStrobe[UiIndex].EyeWidth[Controller][Channel][Rank][Strobe];
              MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "        %3d ", EwUi[UiIndex] );
            }
            for (UiIndex = 0; UiIndex < MAX_EW_DIFF_NUM; UiIndex++) {
              MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "        %3d  ", DcaScoreData.DcaScore[DcaSettingIndex][UiIndex] );
            }
            // EyeWidthDiff = ABS ((DcaScoreData.DcaScore[DcaSettingIndex][0]) - (DcaScoreData.DcaScore[DcaSettingIndex][1]));
            if (DcaScoreData.DcaScore[DcaSettingIndex][0] < DcaScoreData.DcaScore[DcaSettingIndex][1]) {
              MinEw = DcaScoreData.DcaScore[DcaSettingIndex][0];
              MaxEw = DcaScoreData.DcaScore[DcaSettingIndex][1];
            } else {
              MinEw = DcaScoreData.DcaScore[DcaSettingIndex][1];
              MaxEw = DcaScoreData.DcaScore[DcaSettingIndex][0];
            }
 #if 0
            MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "   EyeWidthDiff:  %d  ", EyeWidthDiff);

            if (MinEyeWidthDiff == EyeWidthDiff) {
              //check who is close to 0
              if ((ABS (SWEEP_INDEX_TO_DCA_VALUE (DcaSettingIndex) - 0)) < ABS (SWEEP_INDEX_TO_DCA_VALUE (BestDcaSettingIndex) - 0)) {
                BestDcaSettingIndex = DcaSettingIndex;
                MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "DcaSetting %2d is closer to 0", SWEEP_INDEX_TO_DCA_VALUE (DcaSettingIndex));
              }
            } else if (MinEyeWidthDiff > EyeWidthDiff) {
              MinEyeWidthDiff = EyeWidthDiff;
              BestDcaSettingIndex = DcaSettingIndex;
            }
#endif //if 0
            if (MaxMinimumEw == MinEw) {
              if (MaxMaxEw == MaxEw) {
                //check who is close to 0
                if ((ABS (SWEEP_INDEX_TO_DCA_VALUE (DcaSettingIndex) - 0)) < ABS (SWEEP_INDEX_TO_DCA_VALUE (BestDcaSettingIndex) - 0)) {
                  BestDcaSettingIndex = DcaSettingIndex;
                  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "DcaSetting %2d is closer to 0", SWEEP_INDEX_TO_DCA_VALUE (DcaSettingIndex));
                }
              } else if (MaxMaxEw > MaxEw) {
                // Pick the MaxEw who is close to MinEw.
                BestDcaSettingIndex = DcaSettingIndex;
                MaxMaxEw = MaxEw;
              }
            } else if (MaxMinimumEw < MinEw) {
              MaxMinimumEw = MinEw;
              BestDcaSettingIndex = DcaSettingIndex;
              MaxMaxEw = MaxEw;
            }
            MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "   MinEw: %3d, MaxMinEw: %3d", MinEw, MaxMinimumEw);
          } // DcaSettingIndex
#if 0
          if (Ddr5) {
            if (DramDcaTrainingData->DcaType == DcaFourPhase) {
              MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\nMc%d.C%d.R%d.S%d.G%d:MinEyeWidthDiff:  %d:  ", Controller, Channel, Rank, Strobe, Group, MinEyeWidthDiff);
            } else if (DramDcaTrainingData->DcaType == DcaSingleOrTwoPhase) {
              MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\nMax Minimum Ew:  %d:  ", MaxMinEw2Phase);
            }
          } else {
            MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Max Minimum Ew:  %d:  ", MaxMinimumEw);
          }
#endif //if 0
          MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\nMc%d.C%d.R%d.S%d.G%d:Max Minimum Ew:  %d:  ", Controller, Channel, Rank, Strobe, Group, MaxMinimumEw);
          DcaSetting = SWEEP_INDEX_TO_DCA_VALUE (BestDcaSettingIndex);
          MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "BestDcaSettingIndex:  %d DcaSetting: %d \n", BestDcaSettingIndex, DcaSetting);
          OptimalDcaSettingIndex[Controller][Channel][Rank][Strobe] = BestDcaSettingIndex;
        } // Strobe
      } // Rank
    } // Dimm
  } // Channel
}

/**
  Set the DRAM DCA value to the mode register.

  @param[in]      MrcData       - Include all MRC global data.
  @param[in]      Controller    - Memory Controller Number within the processor (0-based).
  @param[in]      Channel       - Selecting which Channel to talk to
  @param[in]      Rank          - Selecting which Rank to talk to
  @param[in]      Device        - Selecting which Device to talk to (only valid when PDAmode is TRUE)
  @param[in]      DcaValue      - DCA value for the mode register programing.
  @param[in]      UpdateMrcData - update MRC host struct
  @param[in]      PdaMode       - Selecting to use MPC or old way of MRS
  @param[in]      DcaGroup      - DCA group

  @retval none
**/
VOID
MrcSetDramDcaDdr5 (
  IN OUT MrcParameters *const MrcData,
  IN     UINT32               Controller,
  IN     UINT8                Channel,
  IN     UINT8                Rank,
  IN     UINT8                Device,
  IN     INT8                 DcaValue,
  IN     BOOLEAN              UpdateMrcData,
  IN     BOOLEAN              PdaMode,
  IN     DCA_GT               DcaGroup
  )
{
#if 0
  MrcChannelOut               *ChannelOut;
  MrcOutput                   *Outputs;
  MrcRankOut                  *RankOut;
  UINT8                       ByteLoop;
  UINT8                       MrAddress;
  BOOLEAN                     ProgramPda;
  DDR5_MODE_REGISTER_43_TYPE  Mr43;
  DDR5_MODE_REGISTER_44_TYPE  Mr44;
  MrcDebug                    *Debug;
  UINT8                       SignBit;
  UINT8                       AbsoluteValue;
  UINT8                       MrValueForProgram = 0;

  Outputs    = &MrcData->Outputs;
  Debug      = &Outputs->Debug;
  ChannelOut = &Outputs->Controller[Controller].Channel[Channel];
  RankOut    = &ChannelOut->Dimm[(Rank / 2) % MAX_DIMMS_IN_CHANNEL].Rank[Rank % 2];
  MrAddress  = 0;
  ProgramPda = PdaMode;

  if (DcaValue <0) {
    SignBit = 1;
  } else {
    SignBit = 0;
  }
  AbsoluteValue = ABS (DcaValue);

  switch (DcaGroup) {
    case DramDcaQClk:
    case DramDcaIbClk:
    case DramDcaSingleorTwoClk:
      MrAddress = mrMR43;
      if (ProgramPda) {
        Mr43.Data8 = RankOut->DdrPdaMr43[Device];
      } else {
        Mr43.Data8 = (RankOut->MR[mrIndexMR43])&0xFF;
      }
      if ((DcaGroup == DramDcaQClk) || (DcaGroup == DramDcaSingleorTwoClk)) {
        Mr43.Bits.DcaForQclkIn4PhaseClk = AbsoluteValue;
        Mr43.Bits.DcaForQclkSignBit = SignBit;
      } else {
        Mr43.Bits.DcaForIbclkIn4PhaseClk = AbsoluteValue;
        Mr43.Bits.DcaForIclkSignBit = SignBit;
      }
      MrValueForProgram = Mr43.Data8;

      break;
    case DramDcaQbClk:

      MrAddress = mrMR44;
      if (ProgramPda) {
        Mr44.Data8 = RankOut->DdrPdaMr44[Device];
      } else {
        Mr44.Data8 = (RankOut->MR[mrIndexMR44])&0xFF;
      }

      Mr44.Bits.DcaForQbclkIn4PhaseClk = AbsoluteValue;
      Mr44.Bits.DcaForQbclkSignBit = SignBit;

      MrValueForProgram = Mr44.Data8;
      break;
    default:
      break;
  }

  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\nMc%d.C%d.R%d.S%d MrcSetDramDcaDdr5 value %d Group%d PdaMode %d\n", Controller,
    Channel, Rank, Device, DcaValue, DcaGroup, ProgramPda);

  if (ProgramPda) {
    MrcPdaSelect (MrcData, Controller, Channel, Rank, Device, MRC_PRINTS_ON);
  }

  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "MrcIssueMrw address %d value 0x%x\n",MrAddress, MrValueForProgram);

  MrcIssueMrw (MrcData, Controller, Channel, Rank, MrAddress, MrValueForProgram, MRC_PRINTS_ON);
  MrcWait (MrcData, 10);

  if (UpdateMrcData) {
    if (ProgramPda) {
      switch (DcaGroup) {
        case DramDcaQClk:
        case DramDcaIbClk:
        case DramDcaSingleorTwoClk:
          RankOut->DdrPdaMr43[Device] = MrValueForProgram;
          break;
        case DramDcaQbClk:
          RankOut->DdrPdaMr44[Device] = MrValueForProgram;
          break;
        default:
          break;
      }
    } else {
      switch (DcaGroup) {
        case DramDcaQClk:
        case DramDcaIbClk:
        case DramDcaSingleorTwoClk:
          RankOut->MR[mrIndexMR43] = MrValueForProgram;
          break;
        case DramDcaQbClk:
          RankOut->MR[mrIndexMR44] = MrValueForProgram;
          break;
        default:
          break;
      }

      for (ByteLoop = 0; ByteLoop < Outputs->SdramCount; ByteLoop++) {
        switch (DcaGroup) {
          case DramDcaQClk:
          case DramDcaIbClk:
          case DramDcaSingleorTwoClk:
            RankOut->DdrPdaMr43[ByteLoop] = MrValueForProgram;

            break;
          case DramDcaQbClk:
            RankOut->DdrPdaMr44[ByteLoop] = MrValueForProgram;

            break;
          default:
            break;
        }
      }
    }
  }
#endif //if 0
}

/**
  Set the DRAM DCA value for DDR5

  @param[in]     MrcData        - Include all MRC global data.
  @param[in]     Controller     - Selecting which Controller to talk to.
  @param[in]     Channel        - Selecting which Channel to talk to.
  @param[in]     RankMask       - Selecting which Ranks to talk to.
  @param[in]     DeviceMask     - Selecting which Devices to talk to (only valid for DDR5).
  @param[in]     Offset         - DCA offset value.
  @param[in]     UpdateMrcData  - Used to decide if Mrc host must be updated.
  @param[in]     PdaMode        - Selecting if we are using DDR5 PDA.
  @param[in]     DcaGroup       - DCA Group.

  @retval MrcStatus - mrcWrongInputParameter if it's unsupported,  mrcSuccess otherwise
**/
MrcStatus
Ddr5SetDramDca (
  IN OUT MrcParameters *const MrcData,
  IN     UINT8                Controller,
  IN     UINT8                Channel,
  IN     UINT8                RankMask,
  IN     UINT16               DeviceMask,
  IN     INT8                 Offset,
  IN     BOOLEAN              UpdateMrcData,
  IN     BOOLEAN              PdaMode,
  IN     DCA_GT               DcaGroup
  )
{
  MrcOutput     *Outputs;
  MrcChannelOut *ChannelOut;
  UINT8         Rank;
  UINT8         Device;
  INT8          CurrentOffset;
  MrcDimmOut    *DimmOut;
  UINT8         NumDevices;
  UINT8         DimmIdx;
  BOOLEAN       ProgramPda;

  Outputs = &MrcData->Outputs;
  ChannelOut = &Outputs->Controller[Controller].Channel[Channel];
  CurrentOffset = 0;
  ProgramPda = FALSE;

  ProgramPda = (PdaMode /*&& ChannelOut->Mr43PdaEnabled*/);
  DeviceMask = (ProgramPda) ? DeviceMask : 1;

  for (Rank = 0; Rank < MAX_RANK_IN_CHANNEL; Rank++) {
    if ((MrcRankExist (MrcData, Controller, Channel, Rank)) && (RankMask & (1 << Rank))) {
      DimmIdx = Rank / MAX_RANK_IN_DIMM;
      DimmOut = &ChannelOut->Dimm[DimmIdx];
      // Channel Width (ddr5 = 32 Bytes) / SdramWidth (x8 or x16)
      if (ProgramPda) {
        NumDevices = 32 / DimmOut->SdramWidth;
        if ((DimmOut->SdramWidth == 8) && DimmOut->EccSupport) {
          NumDevices += 1;
        }
      } else {
        NumDevices = 1;
      }
      for (Device = 0; Device < NumDevices; Device++) {
        if (DeviceMask & (1 << Device)) {
          CurrentOffset = Offset;

          MrcSetDramDcaDdr5 (MrcData, Controller, Channel, Rank, Device, CurrentOffset, UpdateMrcData, ProgramPda, DcaGroup);
        }
      }
      if (ProgramPda) {
        // Send PDA command of Index 15 to resume normal rank operation mode.
        MrcPdaSelect (MrcData, Controller, Channel, Rank, 15, MRC_PRINTS_ON);
      }
    }
  }
  return mrcSuccess;
}


/**
  Set the DRAM DCA value for DDR5

  @param[in,out] MrcData             - Include all MRC global data.
  @param[in]     Controller          - Selecting which Controller to talk to.
  @param[in]     Channel             - Selecting which Channel to talk to.
  @param[in]     RankMask            - Selecting which Ranks to talk to.
  @param[in]     DeviceMask          - Selecting which Devices to talk to (only valid for DDR4 and adjusting VrefDQ).
  @param[in]     Offset              - DCA value.
  @param[in]     UpdateMrcData       - Used to decide if Mrc host must be updated.
  @param[in]     PdaMode             - Selecting to use MRH or old method for MRS (only valid for DDR4 and adjusting VrefDQ).
  @param[in]     DcaGroup            - DCA group.

  @retval Nothing.
**/
void
MrcSetDramDca (
  IN OUT MrcParameters *const MrcData,
  IN     UINT8                Controller,
  IN     UINT8                Channel,
  IN     UINT8                RankMask,
  IN     UINT16               DeviceMask,
  IN     INT8                 Offset,
  IN     BOOLEAN              UpdateMrcData,
  IN     BOOLEAN              PdaMode,
  IN     DCA_GT               DcaGroup
  )
{
  MrcDebug        *Debug;
  MrcOutput       *Outputs;
  MrcStatus       Status = mrcSuccess;

  Outputs = &MrcData->Outputs;
  Debug = &Outputs->Debug;

  switch (Outputs->DdrType) {
    case MRC_DDR_TYPE_DDR5:
      Status = Ddr5SetDramDca (MrcData, Controller, Channel, RankMask, DeviceMask, Offset, UpdateMrcData, PdaMode,
                 DcaGroup);
      break;

    default:
      Status = mrcWrongInputParameter;
      MRC_DEBUG_MSG (&Outputs->Debug, MSG_LEVEL_ERROR, "%s ", gUnsupportedTechnology);
      break;
  }

  MRC_DEBUG_ASSERT (Status == mrcSuccess, Debug, "Error Setting DRAM DCA");
}

/**
  Program the optimal DCA setting for each Channel/Rank/Strobe

  @param[in]  MrcData                - Pointer to MRC global data.
  @param[in]  DramDcaTrainingData    - Pointer to DRAM DCA training context
  @param[in]  OptimalDcaSettingIndex - Optimal DCA setting index
  @param[in]  PdaModeInput           - Use PDA or not

  @retval N/A
**/
VOID
ProgramDcaTrainingValue (
  IN  MrcParameters *const         MrcData,
  IN  DRAM_DCA_TRAINING_Data       *DramDcaTrainingData,
  IN  UINT8                        OptimalDcaSettingIndex[MAX_CONTROLLER][MAX_CHANNEL][MAX_RANK_IN_CHANNEL][MAX_STROBE],
  IN  BOOLEAN                      PdaModeInput
  )
{
  UINT8                         Channel;
  UINT8                         Rank;
  UINT8                         Strobe;
  INT8                          DcaSetting;
  DCA_GT                        Group;
  MrcOutput                     *Outputs;
  MrcDebug                      *Debug;
  UINT8                         Controller;
  UINT8                         RankMask;
  BOOLEAN                       Ddr5;
  UINT16                        DeviceMask;
  BOOLEAN                       PdaMode;
  MrcChannelOut                 *ChannelOut;
  UINT16                        *MrPtr;
  LPDDR5_MODE_REGISTER_30_TYPE  Lpddr5Mr30;
  UINT8                         MrIndex;
  MrcStatus                     Status;
  UINT8                         Data;
  BOOLEAN                       Lpddr5;
  MrcDebugMsgLevel              LevelStore;
  MrcChannelIn                  *ChannelIn;
  UINT8                         DqsMapCpu2Dram;
  MrcInput                      *Inputs;

  Outputs               = &MrcData->Outputs;
  Debug                 = &Outputs->Debug;
  Ddr5                  = (Outputs->DdrType == MRC_DDR_TYPE_DDR5);
  Group                 = DramDcaTrainingData->Group;
  DcaSetting            = 0;
  Lpddr5                = (Outputs->DdrType == MRC_DDR_TYPE_LPDDR5);
  Inputs                = &MrcData->Inputs;

  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "ProgramDcaTrainingValue Group%d  DramDcaTrainingData->ChannelMask %d\n",
                 Group, DramDcaTrainingData->ChannelMask);
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Strobe:       \n");
  for (Strobe = 0; Strobe < Outputs->SdramCount; Strobe++) {

    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "   %2d", Strobe);

  } // Strobe

  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\n");

  for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
    for (Channel = 0; Channel < Outputs->MaxChannels; Channel++) {
      if (!MrcChannelExist (MrcData, Controller, Channel)) {
        continue;
      }
      if (((1 << Channel) & DramDcaTrainingData->ChannelMask) != 0) {
        continue;
      }

      ChannelOut = &Outputs->Controller[Controller].Channel[Channel];
    //ChannelOut->Mr43PdaEnabled = PdaModeInput;
    //ChannelOut->Mr44PdaEnabled = PdaModeInput;
      ChannelIn = &Inputs->Controller[Controller].Channel[Channel];
      for (Rank = 0; Rank < MAX_RANK_IN_CHANNEL; Rank++) {
        if (!MrcRankExist (MrcData, Controller, Channel, Rank)) {
          continue;
        }
        if (((1 << Rank) & DramDcaTrainingData->RankMask[Controller][Channel]) != 0) {
          continue;
        }

        if (Ddr5) {
          for (Strobe = 0; Strobe < Outputs->SdramCount; Strobe++) {
            MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Mc%d.C%d.R%d.S%d RankMask 0x%x", Controller, Channel, Rank,
              Strobe, DramDcaTrainingData->RankMask[Controller][Channel]);
            //
            // Convert the sweep index into a physical DRAM DCA value and write it to the DRAM
            //
            DcaSetting = SWEEP_INDEX_TO_DCA_VALUE (OptimalDcaSettingIndex[Controller][Channel][Rank][Strobe]);
            MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "  %3d", DcaSetting);
            RankMask = 1 << Rank;
            MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Group %d", Group);
            PdaMode = PdaModeInput;
            DeviceMask = PdaMode ? (1 << Strobe)  : 1;
            MrcSetDramDca (MrcData, Controller, Channel, RankMask, DeviceMask, DcaSetting, TRUE, PdaMode, Group);
          } // Strobe
        } else {

          MrIndex = mrIndexMR30;
          MrPtr = ChannelOut->Dimm[dDIMM0].Rank[Rank % MAX_RANK_IN_DIMM].MR;
          for (Strobe = 0; Strobe < Outputs->SdramCount; Strobe++) {
            MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Mc%d.C%d.R%d.S%d RankMask%d  \n", Controller, Channel, Rank,
              Strobe, DramDcaTrainingData->RankMask[Controller][Channel]);

            DcaSetting = SWEEP_INDEX_TO_DCA_VALUE (OptimalDcaSettingIndex[Controller][Channel][Rank][Strobe]);
            MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Final DcaSetting  %3d   ", DcaSetting);

            DqsMapCpu2Dram = ChannelIn->DqsMapCpu2Dram[dDIMM0][Strobe];

            if (DcaSetting > 0) {
              Data = LP5_MR30_POSTIVE_OFFSET + DcaSetting;
            } else {
              Data = ABS (DcaSetting);
            }
            if ((DqsMapCpu2Dram % 2) == 0) {
              //lower byte
              Lpddr5Mr30.Bits.DcaLowByte = Data;
            } else if ((DqsMapCpu2Dram % 2) == 1) {
              //upper byte
              Lpddr5Mr30.Bits.DcaUpperByte = Data;
            }
          }

          MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\n MrcIssueMrw Mc0%d.C%d.R%d mr30 0x%x \n", Controller, Channel, Rank, Lpddr5Mr30.Data8);
          Status = MrcIssueMrw (MrcData, Controller, Channel, Rank, mrMR30, Lpddr5Mr30.Data8, TRUE);
          if (Status != mrcSuccess) {
            return;
          }
          MrPtr[MrIndex] = Lpddr5Mr30.Data8;
          MrcWait (MrcData,1);
        }
        MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\n");
      } // Rank

    } // Dimm
  } // Channel
  if (Lpddr5) {
    // Reset DDR
    Status = MrcResetSequence (MrcData);
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, " MrcJedecWCKTraining \n");
    LevelStore=Debug->Level;
    //only print error and warning.
    Debug->Level=MSG_LEVEL_WARNING;
    MrcJedecWCKTraining (MrcData);
    //restore the debug level.
    Debug->Level=LevelStore;
  }
  if (DramDcaTrainingData->DcaType != DcaFourPhase) {
    // Reset DDR
    Status = MrcResetSequence (MrcData);
  }
}

/**

  DRAM DCA Training - Two and Four phase flows

  @param[in, out] MrcData     - Pointer to MRC global data.
  @param DcaType              - Type of Duty Cycle Adjuster test to run - DcaSingleOrTwoPhase or DcaFourPhase
  @param NumGroupsToTrain     - Number of groups in the GroupsToTrain parameter
  @param GroupsToTrain        - List of DCA groups to sweep. Possible values include:
                                DramDcaSingleorTwoClk, DramDcaQClk, DramDcaIbClk and DramDcaQbClk

  @retval mrcSucess  Training succeeds or is unsupported by the DRAM
  @retval mrcFail  Training fails

**/
MrcStatus
DramDcaTrainingCore (
  IN MrcParameters *const MrcData,
  IN DCA_TYPE             DcaType,
  IN UINT8                NumGroupsToTrain,
  IN DCA_GT               *GroupsToTrain
  )
{
  UINT32                       RankMask[MAX_CONTROLLER][MAX_CHANNEL];
  UINT8                        GroupIndex;
  DRAM_DCA_TRAINING_Data       DramDcaTrainingData;
  EYE_WIDTH_UI_ALL             EyeWidthAllUi[DRAM_DCA_SWEEP_SIZE];
  UINT8                        OptimalDcaSettingIndex[MAX_CONTROLLER][MAX_CHANNEL][MAX_RANK_IN_CHANNEL][MAX_STROBE];
  const MRC_FUNCTION           *MrcCall;
  MrcOutput                    *Outputs;
  MrcDebug                     *Debug;
  BOOLEAN                      Ddr5;
  BOOLEAN                      Lpddr5;
  MrcStatus                    Status;
  UINT32                       ChannelMask = 0;

  Status  = mrcSuccess;
  Outputs = &MrcData->Outputs;
  Debug   = &Outputs->Debug;
  MrcCall = MrcData->Inputs.Call.Func;
  Ddr5    = (Outputs->DdrType == MRC_DDR_TYPE_DDR5);
  Lpddr5  = (Outputs->DdrType == MRC_DDR_TYPE_LPDDR5);
  //
  // Check the channel has a DIMM that supports this training step
  // Also get ChannelMask and DimmMask to reflect which channels/ranks support DcaType
  //
  if ((CheckDramDcaSupported (MrcData, &ChannelMask, RankMask, DcaType) == FALSE) && (Ddr5)) {
    return mrcSuccess;
  }

  // DRAM_DCA_DEBUG Begin
  MrcCall->MrcSetMem ((UINT8 *)&DramDcaTrainingData, sizeof (DramDcaTrainingData), 0);
  MrcCall->MrcSetMem ((UINT8 *)&EyeWidthAllUi, sizeof (EyeWidthAllUi), 0);

  DramDcaTrainingData.DcaType       = DcaType;
  DramDcaTrainingData.ChannelMask   = ChannelMask;

  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "ReadMPR margin before DCA\n");
  GetRxDqsTimingMargins (MrcData, &DramDcaTrainingData, &EyeWidthAllUi[0], MRC_PRINTS_OFF);
  DisplayDcaEyeWdith (MrcData, &DramDcaTrainingData, &EyeWidthAllUi[0]);
  // DRAM_DCA_DEBUG End

  MrcCall->MrcSetMem ((UINT8 *)&DramDcaTrainingData, sizeof (DramDcaTrainingData), 0);
  MrcCall->MrcSetMem ((UINT8 *)&EyeWidthAllUi, sizeof (EyeWidthAllUi), 0);
  MrcCall->MrcSetMem ((UINT8 *)&OptimalDcaSettingIndex, sizeof (OptimalDcaSettingIndex), 0);

  DramDcaTrainingData.DcaType       = DcaType;
  DramDcaTrainingData.ChannelMask   = ChannelMask;
  if (Ddr5) {
    MrcCall->MrcCopyMem ((UINT8 *) &(DramDcaTrainingData.RankMask), (UINT8 *)&RankMask, sizeof (RankMask));
  }
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "ChannelMask 0x%x  RankMask 0x%x\n ", DramDcaTrainingData.ChannelMask, DramDcaTrainingData.RankMask[0][0]);
  //
  // Discover the CLK phase alignment of each DRAM - only executes for DRAMs that support 4-phase clocks
  //
  if ((Ddr5) && (DcaType == DcaFourPhase)) {
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "DramDcaClkPhaseAlignmentCheck \n ");
    DramDcaClkPhaseAlignmentCheck (MrcData, &DramDcaTrainingData);
  }
  //
  // Do the Dram Duty Cycle Adjuster training
  // For IBCLK, QCLK and QBCLK, determine which DCA setting produces the best eye widths for all UIs
  //
  for (GroupIndex = 0; GroupIndex < NumGroupsToTrain; GroupIndex++) {
#if 0
    if ((Ddr5) && (DcaType == DcaFourPhase)) {
      MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "DramDcaClkPhaseAlignmentCheck for group %d\n ", GroupsToTrain[GroupIndex]);
      DramDcaClkPhaseAlignmentCheck (MrcData, &DramDcaTrainingData);
    }
#endif //if 0
    DramDcaTrainingData.Group = GroupsToTrain[GroupIndex];
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Group %d \n ", GroupsToTrain[GroupIndex]);
    RunDramDcaTraining (MrcData, &DramDcaTrainingData, EyeWidthAllUi);

    HandleDramDcaTrainingResults (MrcData, &DramDcaTrainingData, EyeWidthAllUi, OptimalDcaSettingIndex);

    ProgramDcaTrainingValue (MrcData, &DramDcaTrainingData, OptimalDcaSettingIndex, TRUE);
    // DRAM_DCA_DEBUG Begin
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "ReadMPR margin after programing the training result group%d\n",
      GroupsToTrain[GroupIndex]);
    //
    // Setup and execute the test on all channels
    //
    GetRxDqsTimingMargins (MrcData, &DramDcaTrainingData, &EyeWidthAllUi[0], MRC_PRINTS_OFF);
    DisplayDcaEyeWdith (MrcData, &DramDcaTrainingData, &EyeWidthAllUi[0]);
    // DRAM_DCA_DEBUG End
  } // GroupIndex
  if (Ddr5) {
    SetupDramPattern (MrcData, DDR5_LFSR0_5A, DDR5_LFSR1_3C);
  }

  // DRAM_DCA_DEBUG Begin
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "ReadMPR margin After DCA\n");
  GetRxDqsTimingMargins (MrcData, &DramDcaTrainingData, &EyeWidthAllUi[0], MRC_PRINTS_OFF);
  DisplayDcaEyeWdith (MrcData, &DramDcaTrainingData, &EyeWidthAllUi[0]);

  if (Lpddr5) {
    // Reset DDR
    Status = MrcResetSequence (MrcData);
  }
  // DRAM_DCA_DEBUG End
  return Status;
}

/**

  Read the DRAM DCA capability that it supports DCA or not, two phases or four phases for DDR5.

  @param[in, out] MrcData     - Pointer to MRC global data.
  @param Mr42Data             - The array that strores the DDR5 MR42 data.

  @retval NA

**/
VOID
ReadDcaCapability (
  IN  MrcParameters *const  MrcData,
  IN  UINT8                 Mr42Data[MAX_CONTROLLER][MAX_CHANNEL][MAX_RANK_IN_CHANNEL]

  )
{
  UINT32          Controller;
  UINT32          Channel;
  UINT32          Rank;
  UINT8           MrrResult[4];
  UINT32          MrAddr;
  MrcOutput       *Outputs;
  MrcDebug        *Debug;

  Outputs = &MrcData->Outputs;
  Debug = &Outputs->Debug;
  for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
    for (Channel = 0; Channel < MAX_CHANNEL; Channel++) {
      for (Rank = 0; Rank < MAX_RANK_IN_CHANNEL; Rank++) {
        if (!MrcRankExist (MrcData, Controller, Channel, Rank)) {
          continue;
        }
        MrAddr = mrMR42;
        MrcIssueMrr (MrcData, Controller, Channel, Rank, MrAddr, MrrResult);
        Mr42Data[Controller][Channel][Rank] = MrrResult[0];
        MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, " Mc%d.C%d.R%d Mr42Data = %d\n", Controller, Channel, Rank, MrrResult[0]);
      }
    }
  }
}

/**

  DRAM DCA Training - Single or Two-Phase Internal Clock flow

  @param[in, out] MrcData  - Pointer to MRC global data.

  @retval mrcSucess  Training succeeds or is unsupported by the DRAM
  @retval mrcFail  Training fails

**/
MrcStatus
DramDcaTwoPhaseTraining (
  IN  MrcParameters *const  MrcData
  )
{
  DCA_TYPE                  DcaType;
  UINT8                     NumGroupsToTrain;
  DCA_GT                    GroupsToTrain;
  MrcOutput                 *Outputs;
  MrcDebug                  *Debug;

  DcaType           = DcaSingleOrTwoPhase;
  GroupsToTrain     = DramDcaSingleorTwoClk ;
  NumGroupsToTrain  = 1;

  Outputs = &MrcData->Outputs;
  Debug = &Outputs->Debug;

  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, " DramDcaTwoPhaseTraining\n");
  return DramDcaTrainingCore (MrcData, DcaType, NumGroupsToTrain, &GroupsToTrain);
}


/**

  DRAM DCA Training - Four-Phase Internal Clock flow

  @param[in, out] MrcData  - Pointer to MRC global data.

  @retval mrcSuccess  Training succeeds or is unsupported by the DRAM
  @retval mrcFail  Training fails

**/
MrcStatus
DramDcaFourPhaseTraining (
  IN  MrcParameters *const  MrcData
  )
{
  DCA_TYPE                  DcaType;
  UINT8                     NumGroupsToTrain;
  MrcOutput                 *Outputs;
  MrcDebug                  *Debug;
  //The order needs to be IBCLK, QCLK, QBCLK or IBCLK, QBCLK, QCLK.
  DCA_GT                    GroupsToTrain[] = {DramDcaIbClk,
                                               DramDcaQClk,
                                               DramDcaQbClk
                                              };

  DcaType = DcaFourPhase;
  Outputs = &MrcData->Outputs;
  Debug = &Outputs->Debug;
  NumGroupsToTrain = sizeof (GroupsToTrain) / sizeof (GroupsToTrain[0]);
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, " DramDcaFourPhaseTraining\n");
  return DramDcaTrainingCore (MrcData, DcaType, NumGroupsToTrain, GroupsToTrain);
}

/**
  Perform DRAM DCA Training.

  @param[in]  MrcData   - Include all MRC global data.
  @param[in]  Prints    - Debug prints enable/disable

  @retval MrcStatus - mrcSuccess or reason for failure.
**/
MrcStatus
DramDcaTraining (
  IN  MrcParameters *const  MrcData,
  IN  BOOLEAN               Prints
  )
{
  MrcOutput          *Outputs;
//MrcChannelOut      *ChannelOut;
  UINT8              Controller;
  UINT8              Channel;
  MrcStatus          Status;
  BOOLEAN            Lpddr5;
  BOOLEAN            Ddr5;
  MrcInput           *Inputs;
  MrcDebug           *Debug;

  Inputs  = &MrcData->Inputs;
  Outputs = &MrcData->Outputs;
  Lpddr5  = (Outputs->DdrType == MRC_DDR_TYPE_LPDDR5);
  Ddr5 = (Outputs->DdrType == MRC_DDR_TYPE_DDR5);
  Status = mrcSuccess;
  Debug = &Outputs->Debug;

  if ((!Ddr5) && (!Lpddr5)) {
    return Status;
  }

  for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
    for (Channel = 0; Channel < Outputs->MaxChannels; Channel++) {
      if (!MrcChannelExist (MrcData, Controller, Channel)) {
        continue;
      }
    //ChannelOut = &Outputs->Controller[Controller].Channel[Channel];
    //ChannelOut->Mr43PdaEnabled = FALSE;
    //ChannelOut->Mr44PdaEnabled = FALSE;
    }
  }

  DramDcaTwoPhaseTraining (MrcData);

  if (Ddr5) {
    //only ddr5 suports four phase DCA
    //DramDcaFourPhaseTraining (MrcData);//To enable the DCA four phase training, just uncomment out this line.
  }

  for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
    for (Channel = 0; Channel < Outputs->MaxChannels; Channel++) {
      if (!MrcChannelExist (MrcData, Controller, Channel)) {
        continue;
      }
    //ChannelOut = &Outputs->Controller[Controller].Channel[Channel];
    //ChannelOut->Mr43PdaEnabled = TRUE;
    //ChannelOut->Mr44PdaEnabled = TRUE;
    }
  }
  Inputs->DcaExecuted = TRUE;
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Pre SDL DCC training after DCA start\n");
  MrcRxPreSdlDccTraining (MrcData);
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Post SDL DCC training after DCA start\n");
  MrcRxPostSdlDccTraining (MrcData);
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Write timing centering after DCA start\n");
  MrcWriteTimingCentering2D (MrcData);
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Read timing centering after DCA start\n");
  MrcReadTimingCenteringAfterDca (MrcData);
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Write Voltage centering after DCA start\n");
  MrcWriteVoltageCentering2D (MrcData);
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Read Voltage centering after DCA start\n");
  MrcReadVoltageCentering2D (MrcData);
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Write timing centering 2D after DCA start\n");
  MrcWriteTimingCentering2D (MrcData);
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Read timing centering 2D after DCA start\n");
  MrcReadTimingCentering2D (MrcData);

  return Status;
}


/**
  DRAM DCA Training for DDR5 and LP5

  @param[in] MrcData       - Include all MRC global data.

  @retval MrcStatus       - if it's successful, return mrcSuccess, otherwise return reason for failure.
**/
MrcStatus
MrcDramDcaTraining (
  IN     MrcParameters *const MrcData
  )
{
  return DramDcaTraining (MrcData, MRC_PRINTS_ON);
}
