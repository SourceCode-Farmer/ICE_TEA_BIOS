/** @file
  Starting point for the core memory reference code.

@copyright
  INTEL CONFIDENTIAL
  Copyright 1999 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains an 'Intel Peripheral Driver' and is uniquely identified as
  "Intel Reference Module" and is licensed for Intel CPUs and chipsets under
  the terms of your license agreement with Intel or your vendor. This file may
  be modified by the user, subject to additional terms of the license agreement.

@par Specification Reference:
**/

//
// Include files
//

#include "MrcTypes.h"
#include "MrcApi.h"
#include "MrcCommandTraining.h"
#include "MrcCommon.h"
#include "MrcCrosser.h"
#include "MrcDdr3.h"
#include "MrcDebugHook.h"
#include "MrcDebugPrint.h"
#include "MrcGeneral.h"
#include "MrcGlobal.h"
#include "MrcBdat.h"
#include "MrcMcConfiguration.h"
#include "MrcMemoryApi.h"
#include "MrcMemoryMap.h"
#include "MrcMemoryScrub.h"
#include "MrcReadDqDqs.h"
#include "MrcReadReceiveEnable.h"
#include "MrcRegisterCache.h"
#include "MrcReset.h"
#include "MrcSaveRestore.h"
#include "MrcSpdProcessing.h"
#include "MrcMaintenance.h"
#include "MrcStartMemoryConfiguration.h"
#include "MrcWriteDqDqs.h"
#include "MrcWriteLeveling.h"
#include "MrcGears.h"
#include "MrcDdrIoApi.h"
#include "MrcDramDca.h"
#include "MrcPpr.h"

const CallTableEntry  MrcCallTable[] = {
  ///
  /// The functions are executed in the following order, as the policy flag dictates.
  /// Mrctask, post_code, OEM command, Print On/Off, policy_flag, iteration, debug_string
  ///
  {MrcEarlyOverrides,               MRC_EARLY_OVERRIDES,        OemEarlyOverrides,      1, MF_COLD | MF_WARM | MF_S3 | MF_FAST | MF_GV_1                              ,                   MRC_ITERATION_MAX, MRC_DEBUG_TEXT("MRC Early Overrides")},
  {MrcSafeMode,                     MRC_SAFE_MODE,              OemMrcSafeMode,         1, MF_COLD | MF_WARM | MF_S3 | MF_FAST | MF_GV_1                              ,                   MRC_ITERATION_MAX, MRC_DEBUG_TEXT("Safe Mode Override")},
  {MrcFastBootPermitted,            MRC_FAST_BOOT_PERMITTED,    OemFastBootPermitted,   1,                             MF_FAST | MF_GV_1                              ,                   MRC_ITERATION_MAX, MRC_DEBUG_TEXT("Fast boot permitted")},
  {MrcRestoreNonTrainingValues,     MRC_RESTORE_NON_TRAINING,   OemRestoreNonTraining,  1,           MF_WARM | MF_S3 | MF_FAST | MF_GV_1 | MF_GV_2 | MF_GV_3 | MF_GV_4,                   MRC_ITERATION_MAX, MRC_DEBUG_TEXT("Restore non-training values")},
#ifdef MRC_DEBUG_PRINT
  {MrcPrintInputParameters,         MRC_PRINT_INPUT_PARAMS,     OemPrintInputParameters,1, MF_COLD | MF_WARM         | MF_FAST | MF_GV_1                              ,                   MRC_ITERATION_MAX, MRC_DEBUG_TEXT("Print input parameters")},
#endif // MRC_DEBUG_PRINT
  {MrcSpdProcessing,                MRC_SPD_PROCESSING,         OemSpdProcessingRun,    1, MF_COLD                             | MF_GV_1 | MF_GV_2 | MF_GV_3 | MF_GV_4,                   MRC_ITERATION_MAX, MRC_DEBUG_TEXT("SPD PROCESSING")},
  {MrcSetOverrides,                 MRC_SET_OVERRIDES,          OemSetOverride,         1, MF_COLD | MF_WARM | MF_S3 | MF_FAST | MF_GV_1 | MF_GV_2 | MF_GV_3 | MF_GV_4,                   MRC_ITERATION_MAX, MRC_DEBUG_TEXT("MRC Auto Updates and Overrides")},
  {MrcMcCapability,                 MRC_MC_CAPABILITY,          OemMcCapability,        1, MF_COLD | MF_WARM | MF_S3 | MF_FAST | MF_GV_1 | MF_GV_2 | MF_GV_3 | MF_GV_4,                   MRC_ITERATION_MAX, MRC_DEBUG_TEXT("MC Capabilities")},
  {MrcFrequencyLock,                MRC_FREQ_LOCK,              OemFrequencySet,        1, MF_COLD | MF_WARM | MF_S3 | MF_FAST | MF_GV_1 | MF_GV_2 | MF_GV_3 | MF_GV_4,                   MRC_ITERATION_MAX, MRC_DEBUG_TEXT("MC Frequency Lock")},
  {MrcMemorySsInit,                 MRC_MEMORY_SS_INIT,         OemMemorySsInit,        1, MF_COLD | MF_WARM | MF_S3 | MF_FAST | MF_GV_1 | MF_GV_2 | MF_GV_3 | MF_GV_4,                   MRC_ITERATION_MAX, MRC_DEBUG_TEXT("Memory SubSystem Config")},
  {MrcDdrIoPreInit,                 MRC_DDRIO_PREINIT,          OemDdrPhyInit,          1, MF_COLD | MF_WARM | MF_S3 | MF_FAST | MF_GV_1 | MF_GV_2 | MF_GV_3 | MF_GV_4,                   MRC_ITERATION_MAX, MRC_DEBUG_TEXT("DDRIO PreInit")},
  {MrcMcConfiguration,              MRC_MC_CONFIG,              OemMcInitRun,           1, MF_COLD                             | MF_GV_1 | MF_GV_2 | MF_GV_3 | MF_GV_4,                   MRC_ITERATION_MAX, MRC_DEBUG_TEXT("MC Config")},
  {MrcDdrIoInit,                    MRC_DDRIO_INIT,             OemDdrPhyInit,          1, MF_COLD                             | MF_GV_1 | MF_GV_2 | MF_GV_3 | MF_GV_4,                   MRC_ITERATION_MAX, MRC_DEBUG_TEXT("DDRIO Config")},
  {MrcSenseAmpOffsetTraining,       MRC_SENSE_AMP_OFFSET,       OemSenseAmpTraining,    1, MF_COLD                             | MF_GV_1 | MF_GV_2 | MF_GV_3 | MF_GV_4,                   MRC_ITERATION_MAX, MRC_DEBUG_TEXT("SenseAmp Offset Training")},
  {MrcPhyInitSeq3,                  MRC_DDRIO_INIT_SEQ3,        OemDdrPhyInit,          1, MF_COLD                             | MF_GV_1 | MF_GV_2 | MF_GV_3 | MF_GV_4,                   MRC_ITERATION_MAX, MRC_DEBUG_TEXT("DDRIO Config - Seq3")},
//{MrcDdrScomp,                     MRC_DDRSCOMP_INIT,          OemDdrScompInit,        1, MF_COLD                             | MF_GV_1 | MF_GV_2 | MF_GV_3 | MF_GV_4,                   MRC_ITERATION_MAX, MRC_DEBUG_TEXT("DDR SCOMP Config")},
  {MrcSetMemoryMap,                 MRC_MC_MEMORY_MAP,          OemMcMemoryMap,         1, MF_COLD | MF_WARM | MF_S3 | MF_FAST                               | MF_GV_4,                   MRC_ITERATION_MAX, MRC_DEBUG_TEXT("MC MEMORY MAP")},
  {MrcPreTraining,                  MRC_PRE_TRAINING,           OemPreTraining,         1, MF_COLD                   | MF_FAST | MF_GV_1 | MF_GV_2 | MF_GV_3 | MF_GV_4,                   MRC_ITERATION_MAX, MRC_DEBUG_TEXT("Pre-Training")},
  {MrcResetSequence,                MRC_RESET_SEQUENCE,         OemMcResetRun,          1, MF_COLD                             | MF_GV_1 | MF_GV_2 | MF_GV_3 | MF_GV_4,                   MRC_ITERATION_MAX, MRC_DEBUG_TEXT("JEDEC RESET")},
  {MrcPrintDdrMrs,                  MRC_PRINT_DDR_MRS,          OemPrintDdrMrs,         1, MF_COLD                   | MF_FAST | MF_GV_1 | MF_GV_2 | MF_GV_3 | MF_GV_4,                   MRC_ITERATION_MAX, MRC_DEBUG_TEXT("Print DDR MRs")},
  {MrcEarlyCommandTraining,         MRC_EARLY_COMMAND,          OemEarlyCommandTraining,1, MF_COLD                             | MF_GV_1 | MF_GV_2 | MF_GV_3 | MF_GV_4,                   MRC_ITERATION_MAX, MRC_DEBUG_TEXT("Early command training")},
  {MrcJedecWCKTraining,             MRC_WCK_LEVELING,           OemWckLeveling,         1, MF_COLD                             | MF_GV_1 | MF_GV_2 | MF_GV_3 | MF_GV_4,                   MRC_ITERATION_MAX, MRC_DEBUG_TEXT("Jedec WCK training")},
  {MrcRxPreSdlDccTraining,          MRC_RX_PRE_SDL_DCC,         OemRxPreSdlDccEarly,    1, MF_COLD                             | MF_GV_1 | MF_GV_2 | MF_GV_3 | MF_GV_4,                   MRC_ITERATION_MAX, MRC_DEBUG_TEXT("Rx Pre SDL DCC training Early")},
  {MrcRxPostSdlDccTraining,         MRC_RX_POST_SDL_DCC,        OemRxPostSdlDccEarly,   1, MF_COLD                             | MF_GV_1 | MF_GV_2 | MF_GV_3 | MF_GV_4,                   MRC_ITERATION_MAX, MRC_DEBUG_TEXT("Rx Post SDL DCC training Early")},
  {MrcReadLevelingTraining,         MRC_RECEIVE_ENABLE,         OemReceiveEnable,       1, MF_COLD                             | MF_GV_1 | MF_GV_2 | MF_GV_3 | MF_GV_4,                   MRC_ITERATION_MAX, MRC_DEBUG_TEXT("Read Leveling training")},
  {MrcRoundTripMatch,               MRC_ROUNDTRIP_MATCH,        OemRoundtripMatch,      1, MF_COLD                             | MF_GV_1 | MF_GV_2 | MF_GV_3 | MF_GV_4,                   MRC_ITERATION_MAX, MRC_DEBUG_TEXT("Roundtrip Match")},
  {MrcReadMprTraining,              MRC_READ_MPR,               OemReadMprTraining,     1, MF_COLD                             | MF_GV_1 | MF_GV_2 | MF_GV_3 | MF_GV_4,                   MRC_ITERATION_MAX, MRC_DEBUG_TEXT("CPGC-Based Read MPR training")},
  {MrcEarlyReadMprTimingCentering2D,MRC_EARLY_RDMPR_TIMING_2D,  OemEarlyRmpr2DPostEct,  1, MF_COLD                             | MF_GV_1 | MF_GV_2 | MF_GV_3 | MF_GV_4,                   MRC_ITERATION_MAX, MRC_DEBUG_TEXT("Early ReadMPR Timing Centering 2D")},
  {MrcRxPreSdlDccTraining,          MRC_RX_PRE_SDL_DCC_POSTDCA, OemRxPreSdlDcc,         1, MF_COLD                             | MF_GV_1 | MF_GV_2 | MF_GV_3 | MF_GV_4,                   MRC_ITERATION_MAX, MRC_DEBUG_TEXT("Rx Pre SDL DCC training")},
  {MrcRxPostSdlDccTraining,         MRC_RX_POST_SDL_DCC_POSTDCA,OemRxPostSdlDcc,        1, MF_COLD                             | MF_GV_1 | MF_GV_2 | MF_GV_3 | MF_GV_4,                   MRC_ITERATION_MAX, MRC_DEBUG_TEXT("Rx Post SDL DCC training")},
  {MrcJedecWriteLevelingTraining,   MRC_JEDEC_WRITE_LEVELING,   OemJedecWriteLeveling,  1, MF_COLD                             | MF_GV_1 | MF_GV_2 | MF_GV_3 | MF_GV_4,                   MRC_ITERATION_MAX, MRC_DEBUG_TEXT("Jedec Write Leveling training")},
  {MrcWriteLevelingFlyByTraining,   MRC_WRITE_LEVELING_FLYBY,   OemWriteLevelingFlyby,  1, MF_COLD                             | MF_GV_1 | MF_GV_2 | MF_GV_3 | MF_GV_4,                   MRC_ITERATION_MAX, MRC_DEBUG_TEXT("Write Leveling FlyBy training")},
  {MrcWeakWriteTimingCentering,     MRC_WEAK_WRITE_TIMING_1D,   OemWeakWriteDqDqs,      1, MF_COLD                             | MF_GV_1 | MF_GV_2 | MF_GV_3 | MF_GV_4 | MF_RMT | MF_TST, MRC_ITERATION_MAX, MRC_DEBUG_TEXT("Weak Write Timing Centering before Early 2Ds")},
  {MrcWeakReadTimingCentering,      MRC_WEAK_READ_TIMING_1D,    OemWeakReadDqDqs,       1, MF_COLD                             | MF_GV_1 | MF_GV_2 | MF_GV_3 | MF_GV_4 | MF_RMT | MF_TST, MRC_ITERATION_MAX, MRC_DEBUG_TEXT("Weak Read Timing Centering before Early 2Ds")},
  {MrcEarlyWriteTimingCentering2D,  MRC_EARLY_WRITE_TIMING_2D,  OemEarlyWriteDqDqs2D,   1, MF_COLD                             | MF_GV_1 | MF_GV_2 | MF_GV_3 | MF_GV_4,                   MRC_ITERATION_MAX, MRC_DEBUG_TEXT("Early Write Timing Centering 2D")},
  {MrcDimmDFETrainingEarly,         MRC_DIMM_DFE_EARLY,         OemDimmDfeTrainingEarly, 1, MF_COLD                            | MF_GV_1 | MF_GV_2 | MF_GV_3 | MF_GV_4 | MF_RMT | MF_TST, MRC_ITERATION_MAX, MRC_DEBUG_TEXT("Early DIMM DFE Training")},
  {MrcWriteTimingCentering,         MRC_WRITE_TIMING_1D,        OemWriteDqDqs,          1, MF_COLD                             | MF_GV_1 | MF_GV_2 | MF_GV_3 | MF_GV_4 | MF_RMT | MF_TST, MRC_ITERATION_MAX, MRC_DEBUG_TEXT("Write Timing Centering")},
  {MrcWriteVoltageCentering,        MRC_WRITE_VOLTAGE_1D,       OemWriteVoltage,        1, MF_COLD                             | MF_GV_1 | MF_GV_2 | MF_GV_3 | MF_GV_4 | MF_RMT | MF_TST, MRC_ITERATION_MAX, MRC_DEBUG_TEXT("Write Voltage Centering")},
  {MrcEarlyReadTimingCentering2D,   MRC_EARLY_READ_TIMING_2D,   OemEarlyReadDqDqs2D,    1, MF_COLD                             | MF_GV_1 | MF_GV_2 | MF_GV_3 | MF_GV_4 | MF_RMT | MF_TST, MRC_ITERATION_MAX, MRC_DEBUG_TEXT("Early Read Timing Centering 2D")},
//  {MrcPowerSavingMeter,             MRC_PWR_MTR,                OemPowerSavingMeter,    1, MF_COLD                             | MF_GV_1 | MF_GV_2 | MF_GV_3 | MF_GV_4,                   MRC_ITERATION_MAX, MRC_DEBUG_TEXT("PowerSavingMeter Base Line Update")},
  {MrcTxDqTcoCompTraining,          MRC_TXDQTCO_COMP_TRAINING,  OemTxDqTcoCompTraining, 1, MF_COLD                             | MF_GV_1 | MF_GV_2 | MF_GV_3 | MF_GV_4 | MF_RMT | MF_TST, MRC_ITERATION_MAX, MRC_DEBUG_TEXT("TxDqTCO Comp Training")},
  {MrcTxDqsTcoCompTraining,         MRC_TXDQSTCO_COMP_TRAINING, OemTxDqsTcoCompTraining,1, MF_COLD                             | MF_GV_1 | MF_GV_2 | MF_GV_3 | MF_GV_4 | MF_RMT | MF_TST, MRC_ITERATION_MAX, MRC_DEBUG_TEXT("TxDqsTCO Comp Training")},
  {MrcReadTimingCentering,          MRC_READ_TIMING_1D,         OemReadDqDqs,           1, MF_COLD                             | MF_GV_1 | MF_GV_2 | MF_GV_3 | MF_GV_4 | MF_RMT | MF_TST, MRC_ITERATION_MAX, MRC_DEBUG_TEXT("Read Timing Centering")},
  {MrcReadVoltageCentering,         MRC_READ_VOLTAGE_1D,        OemReadVoltage,         1, MF_COLD                             | MF_GV_1 | MF_GV_2 | MF_GV_3 | MF_GV_4 | MF_RMT,          MRC_ITERATION_MAX, MRC_DEBUG_TEXT("Read Voltage Centering")},
  {MrcWriteTimingCentering2D,       MRC_WRITE_TIMING_2D,        OemWriteDqDqs2D,        1, MF_COLD                             | MF_GV_1 | MF_GV_2 | MF_GV_3 | MF_GV_4 | MF_RMT | MF_TST, MRC_ITERATION_MAX, MRC_DEBUG_TEXT("Write Timing Centering 2D PrePwr")},
  {MrcReadTimingCentering2D,        MRC_READ_TIMING_2D,         OemReadDqDqs2D,         1, MF_COLD                             | MF_GV_1 | MF_GV_2 | MF_GV_3 | MF_GV_4 | MF_RMT | MF_TST, MRC_ITERATION_MAX, MRC_DEBUG_TEXT("Read Timing Centering 2D PrePwr")},
  {MrcEarlyCmdVoltageCentering,     MRC_EARLY_CMD_VREF,         OemEarlyCmdVoltage,     1, MF_COLD                             | MF_GV_1 | MF_GV_2 | MF_GV_3 | MF_GV_4 | MF_RMT | MF_TST, MRC_ITERATION_MAX, MRC_DEBUG_TEXT("Early Command Voltage Centering")},
// This is used for debug
//  {CommandPlot2D,                 MRC_CMD_PLOT_2D,            OemReadDqDqs,           1, MF_COLD                             | MF_GV_1 | MF_GV_2 | MF_GV_3 | MF_GV_4,                   MRC_ITERATION_MAX, MRC_DEBUG_TEXT("2D command plot")},
  {MrcPostTraining,                 MRC_POST_TRAINING,          OemPostTraining,        1, MF_COLD                             | MF_GV_1 | MF_GV_2 | MF_GV_3 | MF_GV_4,                   MRC_ITERATION_MAX, MRC_DEBUG_TEXT("Post-training")},

  {MrcLateCommandTraining,          MRC_LATE_COMMAND,           OemLateCommandTraining, 1, MF_COLD                             | MF_GV_1 | MF_GV_2 | MF_GV_3 | MF_GV_4 | MF_RMT | MF_TST, MRC_ITERATION_MAX, MRC_DEBUG_TEXT("Late command training")},
//#ifndef LOCAL_STUB_FLAG
//  {MrcClkTcoCompTraining,           MRC_CLKTCO_COMP_TRAINING,   OemClkTcoCompTraining,  1, MF_COLD                             | MF_GV_1 | MF_GV_2 | MF_GV_3 | MF_GV_4 | MF_RMT | MF_TST, MRC_ITERATION_MAX, MRC_DEBUG_TEXT("ClkTCO Comp Training")},
//#endif // LOCAL_STUB_FLAG
  {MrcCmdVoltageCentering,          MRC_CMD_VREF,               OemCmdVoltCentering,    1, MF_COLD                             | MF_GV_1 | MF_GV_2 | MF_GV_3 | MF_GV_4 | MF_RMT | MF_TST, MRC_ITERATION_MAX, MRC_DEBUG_TEXT("Command Voltage Centering")},
  {MrcCmdEqDsTraining,              MRC_CMD_EQ,                 OemCmdDriveStrengthEq,  1, MF_COLD                             | MF_GV_1 | MF_GV_2 | MF_GV_3 | MF_GV_4 | MF_RMT,          MRC_ITERATION_MAX, MRC_DEBUG_TEXT("CMD/CTL Drive Strength and Equalization 2D")},
  {MrcCmdSlewRate,                  MRC_CMD_SR,                 OemCmdSlewRate,         1, MF_COLD                             | MF_GV_1 | MF_GV_2 | MF_GV_3 | MF_GV_4 | MF_RMT,          MRC_ITERATION_MAX, MRC_DEBUG_TEXT("CMD CTL CLK Slew Rate") },
  {MrcCmdDsUpDnTraining,            MRC_CMD_DS,                 OemCmdDriveUpDn,        1, MF_COLD                             | MF_GV_1 | MF_GV_2 | MF_GV_3 | MF_GV_4 | MF_RMT,          MRC_ITERATION_MAX, MRC_DEBUG_TEXT("CMD/CTL Drive Strength Up/Dn 2D")},
  {MrcDimmOdtCaTraining,            MRC_DIMM_ODT_CA,            OemDimmOdtCaTraining,   1, MF_COLD                             | MF_GV_1 | MF_GV_2 | MF_GV_3 | MF_GV_4 | MF_RMT | MF_TST, MRC_ITERATION_MAX, MRC_DEBUG_TEXT("DIMM CA ODT Training")},
  {MrcDimmRonTraining,              MRC_DIMM_RON,               OemDimmRonTraining,     1, MF_COLD                             | MF_GV_1 | MF_GV_2 | MF_GV_3 | MF_GV_4 | MF_RMT | MF_TST, MRC_ITERATION_MAX, MRC_DEBUG_TEXT("DIMM Ron Training")},
  {MrcWriteEqDsTraining,            MRC_WRITE_EQ,               OemWriteDriveStrengthEq,1, MF_COLD                             | MF_GV_1 | MF_GV_2 | MF_GV_3 | MF_GV_4 | MF_RMT | MF_TST, MRC_ITERATION_MAX, MRC_DEBUG_TEXT("Write Drive Strength and Equalization 2D")},
  {MrcWriteDsTraining,              MRC_WRITE_DS,               OemWriteDriveStrength,  1, MF_COLD                             | MF_GV_1 | MF_GV_2 | MF_GV_3 | MF_GV_4 | MF_RMT | MF_TST, MRC_ITERATION_MAX, MRC_DEBUG_TEXT("Write Drive Strength 1D")},
  {MrcWriteDsUpDnTraining,          MRC_WRITE_DS_2D,            OemWriteDriveUpDn,      1, MF_COLD                             | MF_GV_1 | MF_GV_2 | MF_GV_3 | MF_GV_4 | MF_RMT | MF_TST, MRC_ITERATION_MAX, MRC_DEBUG_TEXT("Write Drive Strength Up/Dn 2D")},
  {MrcDimmOdtTraining,              MRC_DIMM_ODT,               OemDimmODTTraining,     1, MF_COLD                             | MF_GV_1 | MF_GV_2 | MF_GV_3 | MF_GV_4 | MF_RMT | MF_TST, MRC_ITERATION_MAX, MRC_DEBUG_TEXT("DIMM ODT Training")},
  {MrcDimmDFETraining,              MRC_DIMM_DFE,               OemDimmDfeTraining,     1, MF_COLD                             | MF_GV_1 | MF_GV_2 | MF_GV_3 | MF_GV_4 | MF_RMT | MF_TST, MRC_ITERATION_MAX, MRC_DEBUG_TEXT("DIMM DFE Training")},
  {MrcReadEQTraining,               MRC_READ_EQ,                OemReadEQTraining,      1, MF_COLD                             | MF_GV_1 | MF_GV_2 | MF_GV_3 | MF_GV_4 | MF_RMT | MF_TST, MRC_ITERATION_MAX, MRC_DEBUG_TEXT("Read Equalization Training")},
  {MrcReadODTTraining,              MRC_READ_ODT,               OemReadODTTraining,     1, MF_COLD                             | MF_GV_1 | MF_GV_2 | MF_GV_3 | MF_GV_4 | MF_RMT | MF_TST, MRC_ITERATION_MAX, MRC_DEBUG_TEXT("Read ODT Training")},
  {MrcWriteSlewRate,                MRC_WRITE_SR,               OemWriteSlewRate,       1, MF_COLD                             | MF_GV_1 | MF_GV_2 | MF_GV_3 | MF_GV_4 | MF_RMT | MF_TST, MRC_ITERATION_MAX, MRC_DEBUG_TEXT("Write Slew Rate")},
  {MrcPanicVttDnLpTraining,         MRC_PANIC_VTT_DN_LP,        OemPanicVttDnLpTraining,1, MF_COLD                             | MF_GV_1 | MF_GV_2 | MF_GV_3 | MF_GV_4 | MF_RMT | MF_TST, MRC_ITERATION_MAX, MRC_DEBUG_TEXT("PanicVttDnLp Training") },
  {MrcRxVrefDecapTraining,          MRC_READ_VREF_DECAP,        OemReadVrefDecapTraining,1, MF_COLD                            | MF_GV_1 | MF_GV_2 | MF_GV_3 | MF_GV_4 | MF_RMT | MF_TST, MRC_ITERATION_MAX, MRC_DEBUG_TEXT("Read Vref Decap Training")},
  {MrcVddqTraining,                 MRC_VDDQ,                   OemVddqTraining,        1, MF_COLD                             | MF_GV_1 | MF_GV_2 | MF_GV_3 | MF_GV_4 | MF_RMT | MF_TST, MRC_ITERATION_MAX, MRC_DEBUG_TEXT("Vddq Training") },
  {MrcReadAmplifierPower,           MRC_READ_AMP_POWER,         OemReadAmplifierPower,  1, MF_COLD                             | MF_GV_1 | MF_GV_2 | MF_GV_3 | MF_GV_4 | MF_RMT | MF_TST, MRC_ITERATION_MAX, MRC_DEBUG_TEXT("Read Amplifier Power")},
  {MrcOptimizeComp,                 MRC_CMP_OPT,                OemOptimizeComp,        1, MF_COLD                             | MF_GV_1 | MF_GV_2 | MF_GV_3 | MF_GV_4 | MF_RMT | MF_TST, MRC_ITERATION_MAX, MRC_DEBUG_TEXT("Comp Optimization Training")},
//  {MrcForceOltm,                    MRC_FORCE_OLTM,             OemForceOltm,           1, MF_COLD                             | MF_GV_1 | MF_GV_2 | MF_GV_3 | MF_GV_4,                   MRC_ITERATION_MAX, MRC_DEBUG_TEXT("Force OLTM")},

//  {MrcPerBitDeskewCalibration,      MRC_PBD_DESKEW_CAL,         OemPerBitDeskewCal,     1, MF_COLD                             | MF_GV_1 | MF_GV_2 | MF_GV_3 | MF_GV_4,                   MRC_ITERATION_MAX, MRC_DEBUG_TEXT("Read Per Bit Deskew Calibration")},
  {MrcTxDqsDccTraining,             MRC_TX_DQS_DCC_TRAINING,    OemTxDqsDccTraining,    1, MF_COLD                             | MF_GV_1 | MF_GV_2 | MF_GV_3 | MF_GV_4 | MF_RMT | MF_TST, MRC_ITERATION_MAX, MRC_DEBUG_TEXT("Tx Dqs Dcc training")},
  {MrcWriteVoltageCentering2D,      MRC_WRITE_VREF_2D,          OemWriteVoltCentering2D,1, MF_COLD                             | MF_GV_1 | MF_GV_2 | MF_GV_3 | MF_GV_4 | MF_RMT | MF_TST, MRC_ITERATION_MAX, MRC_DEBUG_TEXT("Write Voltage Centering 2D")},
  {MrcReadVoltageCentering2D,       MRC_READ_VREF_2D,           OemReadVoltCentering2D, 1, MF_COLD                             | MF_GV_1 | MF_GV_2 | MF_GV_3 | MF_GV_4 | MF_RMT | MF_TST, MRC_ITERATION_MAX, MRC_DEBUG_TEXT("Read Voltage Centering 2D")},
  {MrcWriteTimingCentering2D,       MRC_WRITE_TIMING_2D,        OemWriteDqDqs2D,        1, MF_COLD                             | MF_GV_1 | MF_GV_2 | MF_GV_3 | MF_GV_4 | MF_RMT | MF_TST, MRC_ITERATION_MAX, MRC_DEBUG_TEXT("Write Timing Centering 2D")},
  {MrcReceiveEnTimingCentering,     MRC_RCVEN_TIMING_1D,        OemRcvEnCentering1D,    1, MF_COLD                             | MF_GV_1 | MF_GV_2 | MF_GV_3 | MF_GV_4 | MF_RMT | MF_TST, MRC_ITERATION_MAX, MRC_DEBUG_TEXT("Receive Enable Timing Centering")},

  {MrcRoundTripLatency,             MRC_ROUND_TRIP_LAT,         OemRoundTripLatency,    1, MF_COLD                             | MF_GV_1 | MF_GV_2 | MF_GV_3 | MF_GV_4 | MF_RMT | MF_TST, MRC_ITERATION_MAX, MRC_DEBUG_TEXT("Round Trip Latency Training")},
  {MrcReadTimingCentering2D,        MRC_READ_TIMING_2D,         OemReadDqDqs2D,         1, MF_COLD                             | MF_GV_1 | MF_GV_2 | MF_GV_3 | MF_GV_4 | MF_RMT | MF_TST, MRC_ITERATION_MAX, MRC_DEBUG_TEXT("Read Timing Centering 2D")},

  {MrcDramDcaTraining,              MRC_DRAM_DCA,               OemDramDcaTraining,     1, MF_COLD                             | MF_GV_1 | MF_GV_2 | MF_GV_3 | MF_GV_4 | MF_RMT | MF_TST, MRC_ITERATION_MAX, MRC_DEBUG_TEXT("DRAM DCA Training")},

  {MrcReadTimingCenteringJR,        MRC_READ_TIMING_1D_JR,      OemReadDqDqsJR,         1, MF_COLD                             | MF_GV_1 | MF_GV_2 | MF_GV_3 | MF_GV_4 | MF_RMT | MF_TST, MRC_ITERATION_MAX, MRC_DEBUG_TEXT("Read Timing Centering with JR")},
  {MrcTurnAroundTiming,             MRC_TURN_AROUND,            OemTurnAroundTimes,     1, MF_COLD                             | MF_GV_1 | MF_GV_2 | MF_GV_3 | MF_GV_4 | MF_RMT | MF_TST, MRC_ITERATION_MAX, MRC_DEBUG_TEXT("Turn Around Trip Training")},

  {MrcRetrainMarginCheck,           MRC_RETRAIN_CHECK,          OemRetrainMarginCheck,  1, MF_COLD                             | MF_GV_1 | MF_GV_2 | MF_GV_3 | MF_GV_4,                   1,                 MRC_DEBUG_TEXT("Check Margin for Retrain")},
  {MrcRankMarginTool,               MRC_RMT_TOOL,               OemRmt,                 1, MF_COLD                             | MF_GV_1 | MF_GV_2 | MF_GV_3 | MF_GV_4,                   MRC_ITERATION_MAX, MRC_DEBUG_TEXT("Rank Margin Tool")},
  {MrcRankMarginToolBit,            MRC_RMT_TOOL_BIT,           OemRmtPerBit,           1, MF_COLD                             | MF_GV_1 | MF_GV_2 | MF_GV_3 | MF_GV_4,                   MRC_ITERATION_MAX, MRC_DEBUG_TEXT("Rank Margin Tool Bit")},
//  {MrcPowerSavingMeter,             MRC_PWR_MTR,                OemPowerSavingMeter,    1, MF_COLD                             | MF_GV_1 | MF_GV_2 | MF_GV_3 | MF_GV_4,                   MRC_ITERATION_MAX, MRC_DEBUG_TEXT("PowerSavingMeter update")},
  {MrcWriteDqDqsReTraining,         MRC_WRITE_DQ_DQS_RETRAINING,OemWriteDqDqsReTraining,1, MF_COLD                             | MF_GV_1 | MF_GV_2 | MF_GV_3 | MF_GV_4,                   MRC_ITERATION_MAX, MRC_DEBUG_TEXT("Write DQ/DQS Retraining")},
  {MrcPprEnable,                    MRC_PPR_ENABLE,             OemPprEnable,           1, MF_COLD                                                           | MF_GV_4,                   MRC_ITERATION_MAX, MRC_DEBUG_TEXT("Post Package Repair") },
  {MrcWrite0,                       MRC_MR_WRITE0,              OemMrcWrite0,           1, MF_COLD                             | MF_GV_1 | MF_GV_2 | MF_GV_3 | MF_GV_4,                   MRC_ITERATION_MAX, MRC_DEBUG_TEXT("Write0") },
  {MrcSaGvFinal,                    MRC_MR_FILL,                OemSaGvFinal,           1, MF_COLD                             | MF_GV_1 | MF_GV_2 | MF_GV_3 | MF_GV_4,                   MRC_ITERATION_MAX, MRC_DEBUG_TEXT("SAGV Finalization")},
  {MrcRhPrevention,                 MRC_RH_PREVENTION,          OemMrcRhPrevention,     1, MF_COLD                             | MF_GV_1 | MF_GV_2 | MF_GV_3 | MF_GV_4,                   MRC_ITERATION_MAX, MRC_DEBUG_TEXT("Row Hammer Prevention") },
  {MrcMcActivate,                   MRC_MC_ACTIVATE,            OemMrcActivate,         1, MF_COLD                             | MF_GV_1 | MF_GV_2 | MF_GV_3 | MF_GV_4,                   MRC_ITERATION_MAX, MRC_DEBUG_TEXT("MRC activate")},
  {MrcSaveMCValues,                 MRC_SAVE_MC_VALUES,         OemSaveMCValues,        1, MF_COLD                             | MF_GV_1 | MF_GV_2 | MF_GV_3 | MF_GV_4,                   MRC_ITERATION_MAX, MRC_DEBUG_TEXT("Save MC Values")},
  {MrcAliasCheck,                   MRC_ALIAS_CHECK,            OemAliasCheck,          1, MF_COLD                                                           | MF_GV_4,                   MRC_ITERATION_MAX, MRC_DEBUG_TEXT("MRC Memory alias check")},
  {MrcEccClean,                     MRC_ECC_CLEAN_START,        OemHwMemInit,           1, MF_COLD                                                           | MF_GV_4,                   MRC_ITERATION_MAX, MRC_DEBUG_TEXT("MRC Memory Scrubbing")},
  {MrcNormalMode,                   MRC_NORMAL_MODE,            OemNormalMode,          1, MF_COLD                                                           | MF_GV_4,                   MRC_ITERATION_MAX, MRC_DEBUG_TEXT("Normal Operation")},
  {MrcUpdateSavedMCValues,          MRC_UPDATE_SAVE_MC_VALUES,  OemUpdateSaveMCValues,  1,                             MF_FAST |                               MF_GV_4,                   MRC_ITERATION_MAX, MRC_DEBUG_TEXT("Update MC Values in Fast flow")},
  {MrcRestoreTrainingValues,        MRC_RESTORE_TRAINING,       OemRestoreTraining,     1,           MF_WARM | MF_S3 | MF_FAST | MF_GV_1 | MF_GV_2 | MF_GV_3 | MF_GV_4,                   MRC_ITERATION_MAX, MRC_DEBUG_TEXT("Restore Training Values")},
  {MrcIbecc,                        MRC_IBECC,                  OemIbecc,               1, MF_COLD | MF_WARM | MF_S3 | MF_FAST                               | MF_GV_4,                   MRC_ITERATION_MAX, MRC_DEBUG_TEXT("MRC In-band ECC") },
  {MrcResetSequence,                MRC_RESET_SEQUENCE,         OemMcResetRun,          1,                             MF_FAST | MF_GV_1 | MF_GV_2 | MF_GV_3 | MF_GV_4,                   MRC_ITERATION_MAX, MRC_DEBUG_TEXT("JEDEC RESET on Fast flow")},
  {MrcMarginLimitCheck,             MRC_MARGIN_LIMIT_CHECK,     OemMarginLimitCheck,    1,                             MF_FAST | MF_GV_1 | MF_GV_2 | MF_GV_3 | MF_GV_4,                   MRC_ITERATION_MAX, MRC_DEBUG_TEXT ("Margin Limit Check on Fast flow")},
//{MrcRankMarginTool,               MRC_RMT_TOOL,               OemRmt,                 1,                             MF_FAST | MF_GV_1 | MF_GV_2 | MF_GV_3 | MF_GV_4,                   MRC_ITERATION_MAX, MRC_DEBUG_TEXT("Rank Margin Tool on Fast flow")},
  {MrcEccClean,                     MRC_ECC_CLEAN_START,        OemHwMemInit,           1,                             MF_FAST                               | MF_GV_4,                   MRC_ITERATION_MAX, MRC_DEBUG_TEXT("MRC Memory Scrubbing")},
  {MrcNormalMode,                   MRC_NORMAL_MODE,            OemNormalMode,          1,                             MF_FAST                               | MF_GV_4,                   MRC_ITERATION_MAX, MRC_DEBUG_TEXT("Normal Operation For Fast flow") },
  {MrcSelfRefreshExit,              MRC_SELF_REFRESH_EXIT,      OemSelfRefreshExit,     1,           MF_WARM | MF_S3                                         | MF_GV_4,                   MRC_ITERATION_MAX, MRC_DEBUG_TEXT("Perform Self Refresh Exit")},
  {MrcNormalMode,                   MRC_NORMAL_MODE,            OemNormalMode,          1,           MF_WARM | MF_S3                                         | MF_GV_4,                   MRC_ITERATION_MAX, MRC_DEBUG_TEXT("Normal Operation For Warm / S3 flow")},
  {MrcEccClean,                     MRC_ECC_CLEAN_START,        OemHwMemInit,           1,           MF_WARM                                                 | MF_GV_4,                   MRC_ITERATION_MAX, MRC_DEBUG_TEXT("MRC Memory Scrubbing")},
  {MrcThermalConfig,                MRC_THERMAL_CONFIG,         OemThermalConfig,       1, MF_COLD | MF_WARM | MF_S3 | MF_FAST                               | MF_GV_4,                   MRC_ITERATION_MAX, MRC_DEBUG_TEXT("Configure Thermal values")},
  {MrcEnergyPerfGain,               MRC_ENG_PERF_GAIN,          OemEngPerfGain,         1, MF_COLD | MF_WARM | MF_S3 | MF_FAST                               | MF_GV_4,                   MRC_ITERATION_MAX, MRC_DEBUG_TEXT("MRC Energy Performance Gain")},
  {MrcSaGvSwitch,                   MRC_SA_GV_SWITCH,           OemSaGvSwitch,          1, MF_COLD | MF_WARM | MF_S3 | MF_FAST | MF_GV_1 | MF_GV_2 | MF_GV_3 | MF_GV_4,                   MRC_ITERATION_MAX, MRC_DEBUG_TEXT("MRC SA GV Switch")},
  {MrcTxtAliasCheck,                MRC_TXT_ALIAS_CHECK,        OemTxtAliasCheck,       1, MF_COLD | MF_WARM         | MF_FAST                               | MF_GV_4,                   MRC_ITERATION_MAX, MRC_DEBUG_TEXT("TXT Memory alias check")},
  {MrcDone,                         MRC_DONE,                   OemMrcDone,             1, MF_COLD | MF_WARM | MF_S3 | MF_FAST                               | MF_GV_4,                   MRC_ITERATION_MAX, MRC_DEBUG_TEXT("MRC done")},
#ifdef BDAT_SUPPORT
  {MrcFillBdatStructure,            MRC_FILL_BDAT_STRUCTURE,    OemMrcFillBdat,         1, MF_COLD                                                           | MF_GV_4,                   MRC_ITERATION_MAX, MRC_DEBUG_TEXT("MRC Fill BDAT Structure")},
#endif // BDAT_SUPPORT
};

/**
  this function use by the OEM to do dedicated task during the MRC.

  @param[in] MrcData           - include all the MRC data
  @param[in] OemStatusCommand  - A command that indicates the task to perform.
  @param[in] ptr               - general ptr for general use.

  @retval The status of the task.
**/
MrcStatus
MrcInternalCheckPoint (
  IN MrcParameters        *MrcData,
  IN MrcOemStatusCommand  OemStatusCommand,
  IN void                 *ptr
  )
{
  MrcInput           *Inputs;
  MRC_FUNCTION       *MrcCall;
  MrcOutput          *Outputs;
  MrcStatus          Status;
  MrcDdrType         DdrType;
  TrainingStepsEn    *TrainingEnables;
  TrainingStepsEn2   *TrainingEnables2;
  BOOLEAN            Ddr4;
  BOOLEAN            Ddr5;
  BOOLEAN            Lpddr4;
  BOOLEAN            Lpddr5;
  BOOLEAN            DtHalo;
  BOOLEAN            UlxUlt;

  Status      = mrcSuccess;
  Inputs      = &MrcData->Inputs;
  MrcCall     = Inputs->Call.Func;
  Outputs     = &MrcData->Outputs;
  DdrType     = Outputs->DdrType;
  TrainingEnables   = &Inputs->TrainingEnables;
  TrainingEnables2  = &Inputs->TrainingEnables2;

  Lpddr4 = (DdrType == MRC_DDR_TYPE_LPDDR4);
  Lpddr5 = (DdrType == MRC_DDR_TYPE_LPDDR5);
  Ddr4   = (DdrType == MRC_DDR_TYPE_DDR4);
  Ddr5   = (DdrType == MRC_DDR_TYPE_DDR5);
  DtHalo = (Inputs->DtHalo);
  UlxUlt = (Inputs->UlxUlt);

  switch (OemStatusCommand) {
    case OemMrcSafeMode:
      if (!Inputs->MrcSafeConfig) {
        Status = mrcFail; // Skip this step
      }
      break;

    case OemSpdProcessingRun:
      break;

    case OemMcResetRun:
      if (Inputs->SimicsFlag == 1) {
        Status = mrcFail; // Don't run on SIMICS.
      } else if (!Inputs->JedecReset) {
        Status = mrcFail; // Skip this training step
      }
      break;

    case OemPrintDdrMrs:
      if (!Ddr4) {
        Status = mrcFail; // Should only run on DDR4
      }
      break;

    case OemOptimizeComp:
      if (Inputs->SimicsFlag == 1) {
        Status = mrcFail; // Don't run on SIMICS.
      }
      break;

    case OemPreTraining:
    case OemPowerSavingMeter:
      if (Inputs->SimicsFlag == 1) {
        Status = mrcFail; // Don't run on SIMICS.
      }
      break;

    case OemTxDqTcoCompTraining:
      if (Inputs->SimicsFlag == 1) {
        Status = mrcFail; // Don't run on SIMICS.
      } else if (!TrainingEnables2->TXTCO) {
        Status = mrcFail; // Skip this training step
      }
      break;

    case OemTxDqsTcoCompTraining:
      if (Inputs->SimicsFlag == 1) {
        Status = mrcFail; // Don't run on SIMICS.
      }
      else if (!TrainingEnables2->TXTCO) {
        Status = mrcFail; // Skip this training step
      }
      break;

    case OemClkTcoCompTraining:
      if (Inputs->SimicsFlag == 1) {
        Status = mrcFail; // Don't run on SIMICS.
      } else if (!TrainingEnables2->CLKTCO) {
        Status = mrcFail; // Skip this step
      }
      break;

    case OemEarlyCommandTraining:
      if (Inputs->SimicsFlag == 0) {
        if ((!TrainingEnables->ECT) || Ddr4) { // Cannot run ECT on DDR4 due to PDA not stable before basic training
          Status = mrcFail; // Skip this training step
        }
      } else {
        Status = mrcFail; // Skip this training step
      }
      break;

    case OemSenseAmpTraining:
      if (Inputs->SimicsFlag == 0) {
        if (!TrainingEnables->SOT) {
          Status = mrcFail; // Skip this training step
        }
      } else {
        Status = mrcFail; // Skip this training step
      }
      break;

    case OemReadMprTraining:
      if (Inputs->SimicsFlag == 0) {
        if (!TrainingEnables->RDMPRT) {
          Status = mrcFail; // Skip this training step
        }
      } else {
        Status = mrcFail; // Skip this training step
      }
      break;

    case OemRxPreSdlDccEarly:
    case OemRxPostSdlDccEarly:
      if (Inputs->SimicsFlag == 0) {
        if (!TrainingEnables2->DCC || !UlxUlt) {  // Run pre sdl dcc and post sdl dcc for UlxUlt before read leveling.
          Status = mrcFail; // Skip this training step
        }
      } else {
        Status = mrcFail; // Skip this training step
      }
      break;

    case OemRxPreSdlDcc:
    case OemRxPostSdlDcc:
      if (Inputs->SimicsFlag == 0) {
        if ((!TrainingEnables2->DCC) || (UlxUlt && ((Inputs->DcaExecuted) == FALSE))) {
          Status = mrcFail; // Skip this training step
        }
      } else {
        Status = mrcFail; // Skip this training step
      }
      break;

    case OemDramDcaTraining:
      if (Inputs->SimicsFlag == 0) {
        if ((!TrainingEnables2->DRAMDCA) || ((Lpddr5) && (Outputs->Frequency < f4800)) || (Ddr4) || (Lpddr4) || (Ddr5)) {
          Status = mrcFail;
        }
      } else {
        Status = mrcFail; // Skip this training step
      }
      break;

    case OemReceiveEnable:
      if (Inputs->SimicsFlag == 0) {
        if (!TrainingEnables->RCVET) {
          Status = mrcFail; // Skip this training step
        }
      } else {
        Status = mrcFail; // Skip this training step
      }
      break;

    case OemWckLeveling:
      if (Inputs->SimicsFlag == 0) {
        if (!TrainingEnables->JWRL || !Lpddr5) {
          Status = mrcFail; // Skip this training step
        }
      } else {
        Status = mrcFail; // Skip this training step
      }
      break;

    case OemJedecWriteLeveling:
      if (Inputs->SimicsFlag == 0) {
        if (!TrainingEnables->JWRL || Lpddr5 || Inputs->LoopBackTest) {
          Status = mrcFail; // Skip this training step
        }
      } else {
        Status = mrcFail; // Skip this training step
      }
      break;

    case OemWriteLevelingFlyby:
      if (Inputs->SimicsFlag == 0) {
        if (!TrainingEnables->JWRL || Lpddr5 || (Ddr5 && !Inputs->LoopBackTest)) {
          Status = mrcFail; // Skip this training step
        }
      } else {
        Status = mrcFail; // Skip this training step
      }
      break;

    case OemWriteDqDqs:
      if (Inputs->SimicsFlag == 0) {
        if (!TrainingEnables->WRTC1D) {
          Status = mrcFail; // Skip this training step
        }
      } else {
        Status = mrcFail; // Skip this training step
      }
      break;

    case OemWeakWriteDqDqs:
      if (Inputs->SimicsFlag == 0) {
        if (!TrainingEnables->WRTC1D || !(Lpddr5 || Ddr4)) { // This step is only run for LP5 and DDR4
          Status = mrcFail; // Skip this training step
        }
      } else {
        Status = mrcFail; // Skip this training step
      }
      break;

    case OemWeakReadDqDqs:
      if (Inputs->SimicsFlag == 0) {
          if (!TrainingEnables->RDTC1D || !Ddr4) { // This step is only run for DDR4
           Status = mrcFail; // Skip this training step
          }
       } else {
         Status = mrcFail; // Skip this training step
       }
       break;

    case OemWriteDqDqsReTraining:
      if (Inputs->SimicsFlag == 0) {
        if (Ddr4 || !Inputs->LpDqsOscEn) { // This is for LP4/LP5/DDR5 only
          Status = mrcFail; // Skip this training step
        }
      } else {
        Status = mrcFail; // Skip this training step
      }
      break;

    case OemReadVoltage:
      if (Inputs->SimicsFlag == 0) {
        if (!TrainingEnables2->RDVC1D) {
          Status = mrcFail; // Skip this training step
        }
      }  else {
        Status = mrcFail; // Skip this training step
      }
      break;

    case OemWriteVoltage:
      if (Inputs->SimicsFlag == 0) {
        if (!TrainingEnables->WRVC1D) {
          Status = mrcFail; // Skip this training step
        }
      } else {
        Status = mrcFail; // Skip this training step
      }
      break;

    case OemEarlyWriteDqDqs2D:
      if (Inputs->SimicsFlag == 0) {
        if (!TrainingEnables->EWRTC2D) {
          Status = mrcFail; // Skip this training step
        }
      } else {
        Status = mrcFail; // Skip this training step
      }
      break;

    case OemEarlyReadDqDqs2D:
      if (Inputs->SimicsFlag == 0) {
        if (!TrainingEnables->ERDTC2D) {
          Status = mrcFail; // Skip this training step
        }
      } else {
        Status = mrcFail; // Skip this training step
      }
      break;

    case OemEarlyRmpr2DPostEct:
    case OemEarlyReadMprDqDqs2D:
      if (Inputs->SimicsFlag == 0) {
        if (!TrainingEnables->ERDMPRTC2D) {
          Status = mrcFail; // Skip this training step
        }
      } else {
        Status = mrcFail; // Skip this training step
      }
      break;

    case OemReadDqDqs:
      if (Inputs->SimicsFlag == 0) {
        if (!TrainingEnables->RDTC1D) {
          Status = mrcFail; // Skip this training step
        }
      } else {
        Status = mrcFail; // Skip this training step
      }
      break;

    case OemReadDqDqsJR:
      if (Inputs->SimicsFlag == 0) {
        if ((!TrainingEnables->RDTC1D || !Ddr5) || (Outputs->Frequency < f3200)) { // for ddr5 only
          Status = mrcFail; // Skip this training step
        }
      } else {
        Status = mrcFail; // Skip this training step
      }
      break;

    case OemPerBitDeskewCal:
      if (Inputs->SimicsFlag == 0) {
        if (Inputs->RdTPbdDis && Inputs->WrTPbdDis) {
          Status = mrcFail; // Skip this training step
        }
      } else {
        Status = mrcFail; // Skip this training step
      }
      break;

    case OemDimmODTTraining:
      if (Inputs->SimicsFlag == 0) {
        if ((!TrainingEnables->DIMMODTT) || (!DtHalo) || Ddr4) {  // @todo: ready only for DT/HALO Ddr5
          Status = mrcFail; // Skip this training step
        }
      } else {
        Status = mrcFail; // Skip this training step
      }
      break;

    case OemWriteDriveStrength:
      if (Inputs->SimicsFlag == 0) {
        if (!TrainingEnables2->WRDS || !((Outputs->Frequency >= f2933) && Ddr4 && (Outputs->IsDdr4Pn641A2 == FALSE))) {
          Status = mrcFail; // Skip this training step
        }
      } else {
        Status = mrcFail; // Skip this training step
      }
      break;

    case OemDimmOdtCaTraining:
      if (Inputs->SimicsFlag == 0) {
        if ((!TrainingEnables2->DIMMODTCA) || !Lpddr4) {  // This is for Lpddr4 only
          Status = mrcFail; // Skip this training step
        }
      } else {
        Status = mrcFail; // Skip this training step
      }
      break;

    case OemDimmRonTraining:
      if (Inputs->SimicsFlag == 0) {
        if ((!TrainingEnables->DIMMRONT) || Lpddr4) {  // @todo: Not ready for LP4
          Status = mrcFail; // Skip this training step
        }
      } else {
        Status = mrcFail; // Skip this training step
      }
      break;

    case OemWriteSlewRate:
      if (Inputs->SimicsFlag == 0) {
        if (!TrainingEnables->WRSRT) {
          Status = mrcFail; // Skip this training step
        }
      } else {
        Status = mrcFail; // Skip this training step
      }
      break;

    case OemWriteDriveUpDn:
      if (Inputs->SimicsFlag == 0) {
        if (!TrainingEnables->WRDSUDT || !Ddr4 || DtHalo) {
          Status = mrcFail; // Skip this training step
        }
      } else {
        Status = mrcFail; // Skip this training step
      }
      break;

    case OemDimmDfeTraining:
      if (Inputs->SimicsFlag == 0) {
        if (!TrainingEnables2->DIMMDFE || !Ddr5 || ((Outputs->Frequency < f3200) && (!(Outputs->IsMixedMemory)))) { // @todo_adl add LPDDR5 support
          Status = mrcFail; // Skip this training step
        }
      } else {
        Status = mrcFail; // Skip this training step
      }
      break;

    case OemDimmDfeTrainingEarly:
      if (Inputs->SimicsFlag == 0) {
        if (!TrainingEnables2->EARLYDIMMDFE || !Ddr5 || ((Outputs->Frequency < f3200) && (!(Outputs->IsMixedMemory)))) { // @todo_adl add LPDDR5 support
          Status = mrcFail; // Skip this training step
        }
      } else {
        Status = mrcFail; // Skip this training step
      }
      break;


    case OemWriteDriveStrengthEq:
      if (Inputs->SimicsFlag == 0) {
        if (!TrainingEnables->WRDSEQT || !((Outputs->Frequency > f2933) && (Ddr4 || Ddr5))) { // @todo_adl Enable on LP4 / LP5
          Status = mrcFail; // Skip this training step
        }
      } else {
        Status = mrcFail; // Skip this training step
      }
      break;

    case OemReadODTTraining:
      if (Inputs->SimicsFlag == 0) {
        if (!TrainingEnables->RDODTT) { // Enabled for LP4/LP5/DDR4/DDR5
          Status = mrcFail; // Skip this training step
        }
      } else {
        Status = mrcFail; // Skip this training step
      }
      break;

    case OemReadEQTraining:
      if (Inputs->SimicsFlag == 0) {
        // On DDR5 run RxDFE training only from C0/H0 and K0/R0, Disable RxDFE for LP5 2400
        if ((!TrainingEnables->RDEQT) || (Ddr5 && (Inputs->A0 || Inputs->B0 || Inputs->G0 || Inputs->J0 || Inputs->Q0)) ||((!(Outputs->Frequency > 2400)) && Lpddr5)) {
          Status = mrcFail; // Skip this training step
        }
      } else {
        Status = mrcFail; // Skip this training step
      }
      break;

    case OemPostTraining:
      if (Inputs->SimicsFlag == 0) {
        if ((!Ddr4) || !TrainingEnables->LCT) { // Cannot switch to 1N/2N without running LCT
          Status = mrcFail; // Skip this training step
        }
      } else {
        Status = mrcFail; // Skip this training step
      }
      break;

    case OemReadAmplifierPower:
      if (Inputs->SimicsFlag == 0) {
        if ((!TrainingEnables->RDAPT) || (DtHalo) || (UlxUlt && ((Outputs->RxMode == MrcRxModeUnmatchedP) ||(Outputs->RxMode == MrcRxModeUnmatchedN))) || (UlxUlt && (Outputs->Frequency == f2000) && Ddr5)) { //Skip it for ADL-S & Unmatched Mode/DDR5 2000 in ADL-P/ADL-M
          Status = mrcFail; // Skip this training step
        }
      } else {
        Status = mrcFail; // Skip this training step
      }
      break;

    case OemForceOltm:
      if (Inputs->SimicsFlag == 0) {
        if (!Ddr4) {        // This is for DDR4 only
          Status = mrcFail; // Skip this training step
        }
      } else {
        Status = mrcFail; // Skip this training step
      }
      break;

    case OemWriteDqDqs2D:
      if (Inputs->SimicsFlag == 0) {
        if (!TrainingEnables->WRTC2D) {
          Status = mrcFail; // Skip this training step
        }
      } else {
        Status = mrcFail; // Skip this training step
      }
      break;

    case OemTxDqsDccTraining:
      if (Inputs->SimicsFlag == 0) {
        if (((!TrainingEnables2->TXDQSDCC) || (!(Ddr5))) || ((Outputs->Frequency < f3200) && (!(Outputs->IsMixedMemory)))) {
          Status = mrcFail; // Skip this training step
        }
      } else {
        Status = mrcFail; // Skip this training step
      }
      break;

    case OemReadDqDqs2D:
      if (Inputs->SimicsFlag == 0) {
        if (!TrainingEnables->RDTC2D) {
          Status = mrcFail; // Skip this training step
        }
      } else {
        Status = mrcFail; // Skip this training step
      }
      break;

    case OemCmdVoltCentering:
      if (Inputs->SimicsFlag == 0) {
        if (!TrainingEnables->CMDVC) {
          Status = mrcFail; // Skip this training step
        }
      } else {
        Status = mrcFail; // Skip this training step
      }
      break;

    case OemEarlyCmdVoltage:
      if (Inputs->SimicsFlag == 0) {
        // Run early CMDVC only on DDR5 2DPC, or DDR4 2DPC above 2400.
        if (!TrainingEnables->CMDVC || !((Ddr5 && Outputs->Any2Dpc) || (Ddr4 && Outputs->Any2Dpc && (Outputs->Frequency > f2400)))) {
          Status = mrcFail; // Skip this training step
        }
      } else {
        Status = mrcFail; // Skip this training step
      }
      break;

    case OemCmdSlewRate:
      if (Inputs->SimicsFlag == 1) {
        Status = mrcFail; // Don't run on SIMICS.
      }
      else if (!TrainingEnables2->CMDSR) {
        Status = mrcFail; // Skip this training step
      }
      break;

    case OemCmdDriveStrengthEq:
      if (Inputs->SimicsFlag == 1) {
        Status = mrcFail; // Don't run on SIMICS.
      }
      else if (!TrainingEnables2->CMDDSEQ) {
        Status = mrcFail; // Skip this training step
      }
      break;

    case OemWriteVoltCentering2D:
      if (Inputs->SimicsFlag == 0) {
        if ((!TrainingEnables->WRVC2D)) {
          Status = mrcFail; // Skip this training step
        }
      } else {
        Status = mrcFail; // Skip this training step
      }
      break;

    case OemReadVoltCentering2D:
      if (Inputs->SimicsFlag == 0) {
        if (!TrainingEnables->RDVC2D) {
          Status = mrcFail; // Skip this training step
        }
      } else {
        Status = mrcFail; // Skip this training step
      }
      break;

    case OemLateCommandTraining:
      if (Inputs->SimicsFlag == 0) {
        if (!TrainingEnables->LCT) {
          Status = mrcFail; // Skip this training step
        }
      } else {
        Status = mrcFail; // Skip this training step
      }
      break;

    case OemRoundTripLatency:
      if (Inputs->SimicsFlag == 0) {
        if (!TrainingEnables->RTL) {
          Status = mrcFail; // Skip this training step
        }
      } else {
        Status = mrcFail; // Skip this training step
      }
      break;

    case OemTurnAroundTimes:
      if (Inputs->SimicsFlag == 0) {
        if ((!TrainingEnables->TAT) || Lpddr4 || Lpddr5) {
          Status = mrcFail; // Skip this training step
        }
      } else {
        Status = mrcFail; // Skip this training step
      }
      break;

    case OemRcvEnCentering1D:
      if (Inputs->SimicsFlag == 0) {
        if (!TrainingEnables->RCVENC1D) {
          Status = mrcFail;
        }
      } else {
        Status = mrcFail; // Skip this training step
      }
      break;

    case OemRetrainMarginCheck:
      if (Inputs->SimicsFlag == 0) {
        if ((!TrainingEnables->RMC) || (Inputs->PowerTrainingMode != MrcTmPower)) {
          Status = mrcFail; // Skip if disabled or not in Power Training mode
        }
      } else {
        Status = mrcFail; // Skip this training step
      }
      break;


    case OemRmt:
      if (Inputs->SimicsFlag == 0) {
        if (!TrainingEnables->RMT) {
          Status = mrcFail; // Skip this training step
        }
      } else {
        Status = mrcFail; // Skip this training step
      }
      break;

    case OemRmtPerBit:
      if (Inputs->SimicsFlag == 0) {
        if (!TrainingEnables2->RMTBIT) {
          Status = mrcFail; // Skip this training step
        }
      } else {
        Status = mrcFail; // Skip this training step
      }
      break;

    case OemMemTest:
      if ((!TrainingEnables->MEMTST) || (Inputs->SimicsFlag == 1)) {
        Status = mrcFail; // Skip this training step
      }
      break;

    case OemAliasCheck:
      if ((!TrainingEnables->ALIASCHK) || (Inputs->SimicsFlag == 1)) {
        Status = mrcFail; // Skip this training step
      }
      break;

    case OemBeforeNormalMode:
      break;

    case OemAfterNormalMode:
      break;

    case OemThermalConfig:
      break;

    case OemFrequencySetDone:
      break;

    case OemSaGvSwitch:
      if (Inputs->SimicsFlag == 1) {
        Status = mrcFail; // Don't run on SIMICS.
      } else {
        if ((Inputs->SaGv != MrcSaGvEnabled) && (!Lpddr4)) {
          Status = mrcFail; // Skip this training step
        }
      }
      break;

    case OemCmdDriveUpDn:
      if (Inputs->SimicsFlag == 1) {
        Status = mrcFail; // Don't run on SIMICS.
      }
      else if (!TrainingEnables2->CMDDRUD) {
        Status = mrcFail; // Skip this training step
      }
      break;

    case OemPanicVttDnLpTraining:
      if (Inputs->SimicsFlag == 1) {
        Status = mrcFail; // Don't run on SIMICS.
      }
      else if (!TrainingEnables2->PVTTDNLP) {
        Status = mrcFail; // Skip this training step
      }
      break;

    case OemReadVrefDecapTraining:
      if (Inputs->SimicsFlag == 1) {
        Status = mrcFail; // Don't run on SIMICS.
      }
      else if (!TrainingEnables2->RDVREFDC) {
        Status = mrcFail; // Skip this training step
      }
      break;

    case OemVddqTraining:
      if (Inputs->SimicsFlag == 1) {
        Status = mrcFail; // Don't run on SIMICS.
      }
      else if (!TrainingEnables2->VDDQT) {
        Status = mrcFail; // Skip this training step
      }
      break;

    case OemHwMemInit:
      if (Inputs->SimicsFlag == 1) {
        Status = mrcFail; // Don't run on SIMICS.
      }
      break;
    case OemMarginLimitCheck:
      if ((Inputs->SimicsFlag == 1) || (Inputs->MarginLimitCheck == MARGIN_CHECK_DISABLE)) {
        Status = mrcFail; //Skip this training step
      }
      break;

    case OemMrcWrite0:
      if (Inputs->SimicsFlag == 1) {
        Status = mrcFail; // Don't run on SIMICS.
      } else if (Ddr4 || Lpddr4 || !Inputs->Write0 || Inputs->A0) { // This is for DDR5 / LP5 B0 and above
        Status = mrcFail; // Skip this training step
      }
      break;

    case OemSaGvFinal:
      if (Ddr5) {
        Status = mrcFail; // For DDR5 we call MrcFinalizeMrSeq from MrcMcActivate
      }
      break;

    case OemPprEnable:
      if (Inputs->PprEnable == 0) {
        Status = mrcFail;
      }
      break;

    default:
      break;
  }
  //
  // Allow Overriding the internal checkpoint decision.
  //
  MrcCall->MrcCheckpoint (MrcData, OemStatusCommand, &Status);
  return Status;
}

/**
  Prepare MRC structures for next MRC Iteraion (SAGV or margin retrain).

  @param[in, out] MrcData - Include all MRC global data.
  @param[in]      Retrain - Turn on/off UpmPwrRetrainFlag.

**/
void
MrcPrepareNextMrcIteration (
  IN OUT MrcParameters *const MrcData,
  IN BOOLEAN                  Retrain
  )
{
  const MRC_FUNCTION      *MrcCall;
  MrcIntOutput            *MrcIntData;
  MrcOutput               *Outputs;
  MrcInput                *Inputs;
  UINT32                  SaveScramKey;
#ifdef BDAT_SUPPORT
  UINT16                    Index;
  MRC_BDAT_SCHEMA_LIST_HOB  *SaveBdatSchemasHob;
  BDAT_MEMORY_DATA_HOB      *SaveBdatMemoryHob[MAX_SCHEMA_LIST_LENGTH];
#endif

  Inputs  = &MrcData->Inputs;
  Outputs = &MrcData->Outputs;
  MrcIntData = ((MrcIntOutput *)(MrcData->IntOutputs.Internal));
  MrcCall = MrcData->Inputs.Call.Func;

  SaveScramKey = MrcIntData->ScramKey;
#ifdef BDAT_SUPPORT
  SaveBdatSchemasHob = Outputs->BdatSchemasHob.Pointer;
  for (Index = 0; Index < MAX_SCHEMA_LIST_LENGTH; Index++) {
    SaveBdatMemoryHob[Index] = Outputs->BdatMemoryHob[Index].Pointer;
  }
#endif // BDAT_SUPPORT
  MrcCall->MrcSetMem ((UINT8 *) Outputs, sizeof (MrcOutput), 0);
  MrcCall->MrcSetMem ((UINT8 *) MrcIntData, sizeof (MrcIntOutput), 0);
  MrcIntData->Size            = sizeof (MrcIntOutput);
  MrcIntData->ScramKey        = SaveScramKey;
  Outputs->Size               = sizeof (MrcOutput);
  Outputs->UpmPwrRetrainFlag  = Retrain;
#ifdef BDAT_SUPPORT
  Outputs->BdatSchemasHob.Pointer = SaveBdatSchemasHob;
  for (Index = 0; Index < MAX_SCHEMA_LIST_LENGTH; Index++) {
    Outputs->BdatMemoryHob[Index].Pointer = SaveBdatMemoryHob[Index];
  }
#endif // BDAT_SUPPORT
  MRC_DEBUG_MSG_OPEN (MrcData, Inputs->DebugLevel, Inputs->DebugStream, Inputs->SerialBuffer.DataN, Inputs->SerialBufferSize);
}

/**
  Initializes the memory controller and DIMMs.

  @param[in, out] MrcData                - Include all MRC global data.
  @param[in]      Select                 - The post code of the call table entry to execute.
  @param[in, out] CurrentSaGvPoint       - The SAGV point that we want to execute the Select post code entry or to continue from (relevant if SAGV enabled).
  @param[in]      ExecuteFromSelectPoint - Execute Memory configuration from (Select,CurrentSaGvPoint) to the end

  @retval mrcSuccess if the initialization succeeded, otherwise an error status indicating the failure.
**/
MrcStatus
MrcStartMemoryConfiguration (
  IN OUT MrcParameters *const MrcData,
  IN     MrcPostCode          Select,
  IN OUT MrcSaGvPoint  *const CurrentSaGvPoint,
  IN BOOLEAN                  ExecuteFromSelectPoint
  )
{
  const MRC_FUNCTION      *MrcCall;
  const CallTableEntry    *Task;
  MrcDebug                *Debug;
  MrcInput                *Inputs;
  MrcOutput               *Outputs;
  MrcIntOutput            *MrcIntData;
  MrcSaGvPoint            SaGvPoint;
  MrcSaGvPoint            SaGvStart;
  MrcSaGvPoint            SaGvEnd;
  MrcSaGv                 SaGv;
  MrcPostCode             post_code;
  MrcPostCode             PostCodeOut;
  MrcStatus               CpStatus;
  MrcStatus               MrcStatus;
  MRC_BOOT_MODE           BootMode;
  MrcUpmPwrRetrainLimits  RetrainLimits[MRC_NUMBER_UPM_PWR_RETRAIN_MARGINS];
  MrcSaveData             *SaveData;
  UINT64                  start_time;
  UINT64                  finish_time;
  UINTN                   LowestStackAddr;
  INT32                   DebugLevel;
  UINT32                  ElapsedTime;
  UINT32                  TotalTime;
  UINT32                  PointTime;
  UINT32                  RetrainLoop;
  UINT16                  index;
  UINT8                   Run;
  UINT8                   MemoryProfileSave;

  Inputs  = &MrcData->Inputs;
  MrcCall = Inputs->Call.Func;
  Outputs = &MrcData->Outputs;
  Debug   = &Outputs->Debug;
  SaveData   = &MrcData->Save.Data;
  MrcIntData = ((MrcIntOutput *) (MrcData->IntOutputs.Internal));
  SaGv    = Inputs->SaGv;

  BootMode  = Inputs->BootMode;
  DebugLevel = Inputs->DebugLevel;

  if (Select == 0) {
    MrcVersionGet (MrcData, &Outputs->Version);
    MrcVersionPrint (MrcData, &Outputs->Version);
  }
  MrcStatus = (Select == 0) ? mrcFail : mrcSuccess;
  post_code = MRC_INITIALIZATION_START;
  Run       = 1;
  TotalTime = 0;
  LowestStackAddr = Debug->LowestStackAddr.DataN;
  MemoryProfileSave = Inputs->MemoryProfile;

  if (BootMode == bmCold) {
    SaveData->IsXmpSagvEnabled = (Inputs->DynamicMemoryBoost || Inputs->RealtimeMemoryFrequency);
  } else if (Inputs->DynamicMemoryBoost || Inputs->RealtimeMemoryFrequency) {
    MRC_DEBUG_MSG (Debug,
                  MSG_LEVEL_NOTE,
                  "Dynamic Memory Boost is %s with MemoryProfile:%d\n",
                  SaveData->IsXmpSagvEnabled ? "running\n": "disabled\n",
                  Inputs->MemoryProfile);
  }

  if (SaGv == MrcSaGvEnabled) {
    // check if we are forced to specific SAGV in selected step
    if ((CurrentSaGvPoint != NULL) && (Select != 0)) {
      // restore current SAGV points
      if (*CurrentSaGvPoint != MrcSaGvPointMax) {
        SaGvStart = *CurrentSaGvPoint;
        SaGvEnd   = (ExecuteFromSelectPoint) ? MrcSaGvPoint4 : *CurrentSaGvPoint;
      } else {
        SaGvStart = MrcSaGvPoint1;
        SaGvEnd   = MrcSaGvPoint1;
      }
    } else {
      SaGvStart = MrcSaGvPoint1;
      SaGvEnd = MrcSaGvPoint4;
    }
  } else {
    switch (SaGv) {
      case MrcSaGvFixed1:
        SaGvStart = MrcSaGvPoint1;
        break;

      case MrcSaGvFixed2:
        SaGvStart = MrcSaGvPoint2;
        break;

      case MrcSaGvFixed3:
        SaGvStart = MrcSaGvPoint3;
        break;

      case MrcSaGvDisabled:
      case MrcSaGvFixed4:
      default:
        SaGvStart = MrcSaGvPoint4;
        break;
    }
    SaGvEnd = SaGvStart;
  }

  for (SaGvPoint = SaGvStart; Run && (SaGvPoint <= SaGvEnd); SaGvPoint++) {
    MrcIntData->SaGvPoint = SaGvPoint;
    // Update  *CurrentSaGvPoint for future calling
    if (CurrentSaGvPoint != NULL) {
      *CurrentSaGvPoint = SaGvPoint;
    }
    Inputs->Iteration = 0;
    PointTime = 0;

    if (SaveData->IsXmpSagvEnabled) {
      if (SaGvPoint == MrcSaGvPoint1) {
        // Use STD_PROFILE for SaGv point 1 and 2
        Inputs->MemoryProfile = STD_PROFILE;
      } else if ((SaGvPoint < MrcSaGvPoint4)) {
        // No need to train SaGv Points 2 and 3, they will be saved with values from the other SaGv Points in MrcSaGvSwitch()
        continue;
      } else {
        // Use selected MemoryProfile for SaGv Points 3/4 (Point 3 will be saved in MrcSaGvSwitch())
        Inputs->MemoryProfile = MemoryProfileSave;
      }
    }

    // Reset UPM/PWR limits to initial values; previous SAGV point might have done retrain.
    MrcCall->MrcCopyMem (
      (UINT8 *) RetrainLimits,
      (UINT8 *) InitialLimits,
      sizeof (MrcUpmPwrRetrainLimits) * MRC_NUMBER_UPM_PWR_RETRAIN_MARGINS
      );

    MRC_DEBUG_MSG (
      Debug,
      MSG_LEVEL_NOTE,
      "SAGV: %sabled, running at point %u\n",
      (SaGv == MrcSaGvEnabled) ? "En" : "Dis",
      SaGvPoint
      );

    for (RetrainLoop = 0; RetrainLoop <= 1; RetrainLoop++) {
      MrcVersionGet (MrcData, &Outputs->Version);
      MrcIntData->UpmPwrRetrainLimits.Pointer = RetrainLimits;
      for (index = 0; Run && (index < ARRAY_COUNT (MrcCallTable)); index++, post_code++) {
        Task = &MrcCallTable[index];
        // Decide if we need to execute the selected MRC task.
        //
        // If MRC task selected and ExecuteFromSelectPoint == TRUE, it means that we need to execute all MRC tasks from Selected point.
        // By clearing the Select variable we force to continue the execution of all remaining Tasks
        if ((Select != 0) && (ExecuteFromSelectPoint) && (Select == Task->post_code_ovr) ) {
          Select = 0;
        }

        if ((NULL == Task->mrc_task) || (Inputs->Iteration >= Task->iteration)) {
          continue;
        }
        if   (((BootMode == bmS3)   && (Task->policy_flag & MF_S3))
          ||  ((BootMode == bmFast) && (Task->policy_flag & MF_FAST))
          ||  ((BootMode == bmWarm) && (Task->policy_flag & MF_WARM))
          ||  ((BootMode == bmCold) && (Task->policy_flag & MF_COLD))
          ||  ((Select != 0))) {
          if  ((Select == 0) || (Select == Task->post_code_ovr)) {
            if (((MrcIntData->SaGvPoint == MrcSaGvPoint1)  && (Task->policy_flag & MF_GV_1)) ||
                ((MrcIntData->SaGvPoint == MrcSaGvPoint2)  && (Task->policy_flag & MF_GV_2)) ||
                ((MrcIntData->SaGvPoint == MrcSaGvPoint3)  && (Task->policy_flag & MF_GV_3)) ||
                ((MrcIntData->SaGvPoint == MrcSaGvPoint4)  && (Task->policy_flag & MF_GV_4)) ||
                (SaGv != MrcSaGvEnabled)) {
              if (Task->oem_cmd < OemNumOfCommands) {
                CpStatus = MrcInternalCheckPoint (MrcData, Task->oem_cmd, NULL);
                if ((mrcSuccess != CpStatus) && !Inputs->IgnoreCheckPoint) {
                  continue;
                }
              }
              // Output post code to post code I/O port.
              PostCodeOut = (Task->post_code_ovr == POST_CODE_NO_OVR) ? post_code : Task->post_code_ovr;
              if ((Select == 0) || ((Select > 0) && (Select == PostCodeOut))) {
                MrcCall->MrcDebugHook (MrcData, PostCodeOut);
              }
              if (Inputs->StopPostCode == PostCodeOut) {
                return mrcSuccess;
              }
              // Output debug string to serial output and execute the MRC task.
              MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\nMRC task -- %s -- Started.\n", Task->String);
              if (!Task->DebugPrint) {
                DebugLevel = Debug->Level;
                Debug->Level = MSG_LEVEL_NEVER;
              }
              start_time  = MrcCall->MrcGetCpuTime ();
              MrcStatus   = Task->mrc_task (MrcData);
              finish_time = MrcCall->MrcGetCpuTime ();
              if (!Task->DebugPrint) {
                Debug->Level = DebugLevel;
              }
              MRC_DEBUG_MSG (
                Debug,
                MSG_LEVEL_ERROR,
                "MRC task %s -- %s, Status = %Xh.",
                Task->String,
                (mrcSuccess == MrcStatus) ? "SUCCEEDED" : "FAILED",
                MrcStatus
                );
              if (Debug->LowestStackAddr.DataN < LowestStackAddr) {
                MRC_DEBUG_MSG (Debug, MSG_LEVEL_ERROR, " Stack Depth: %d\n", Debug->TopStackAddr.DataN - Debug->LowestStackAddr.DataN);
                LowestStackAddr = Debug->LowestStackAddr.DataN;
              } else {
                MRC_DEBUG_MSG (Debug, MSG_LEVEL_ERROR, "\n");
              }
              if ((mrcSuccess != MrcStatus) || (Select != 0)) {
                Run = 0;  // Stop task execution on failure or running one Task.
              }

              if (Select == 0) {
                ElapsedTime = (UINT32)(finish_time - start_time);
                TotalTime += ElapsedTime;
                PointTime += ElapsedTime;
                // <MrcTimer Task="Print input parameters" msec="2"/>
                MRC_DEBUG_MSG (Debug, MSG_LEVEL_TIME, "<MrcTimer Task=\"%s\" msec=\"%u\"/>\n", Task->String, ElapsedTime);
              }

              if (Inputs->RmtPerTask && (Task->policy_flag & MF_RMT)) {  // Run RMT after this task
                MrcRankMarginTool (MrcData);
              }
              if (Inputs->TrainTrace && (Task->policy_flag & MF_TST)) {
                // <MrcTrainedStateTrace> ... <MrcTrainedStateTrace/>
                MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "<MrcTrainedStateTrace>\n");
                MrcTrainedStateTrace (MrcData);
                MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "<MrcTrainedStateTrace/>\n");
              }
            }
          } // Select match
        } // if boot mode match
      } // for index
      // Check if MRC failed due to RetrainMarginCheck
      if ((MrcStatus == mrcRetrain) && (Select == 0)) {
        MRC_DEBUG_MSG (Debug, MSG_LEVEL_ERROR, "\nRerunning training with higher UPM/PWR limits!\n");
        MrcPrepareNextMrcIteration (MrcData, TRUE);
        MrcIntData->SaGvPoint      = SaGvPoint;
        Inputs->Iteration++;
        Run = 1;
      } else {
        break;    // No need to retrain
      }
    } // Retrain loop

    if ((SaGv == MrcSaGvEnabled) && (SaGvPoint != MrcSaGvPoint4) && (Select == 0)) {
      // Prepare for the next MRC iteration
      MrcPrepareNextMrcIteration (MrcData, FALSE);
    }

    if ((SaGv == MrcSaGvEnabled) && (Select == 0)) {
      MRC_DEBUG_MSG (
        Debug,
        MSG_LEVEL_TIME,
        "MRC timer: Total time for SAGV point %u = %u msec.\n",
        SaGvPoint,
        PointTime
        );
    }
  } // for GvPoint

  if (Select == 0) {
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_TIME, "MRC timer: Total time to execute tasks = %u msec.\n", TotalTime);
  }

  if ((Inputs->MemoryProfile != STD_PROFILE) &&
      (MrcStatus != mrcSuccess) &&
      (BootMode == bmCold) &&
      (SaveData->IsXmpSagvEnabled) &&
      Inputs->DynamicMemoryBoost) {
    // Trigger Full Train with STD_PROFILE
    MRC_DEBUG_MSG (Debug,
                  MSG_LEVEL_NOTE,
                  "MRC training fails at profile %d. Trigger reset for full train with STD_PROFILE\n",
                  Inputs->MemoryProfile);
    MrcStatus = mrcResetFullTrain;
  }

  return MrcStatus;
}

/**

@brief

  Get the number of call table entries.

  @param[in, out] MrcData - Include all MRC global data.

  @retval The number of call table entries.

**/
UINT16
MrcGetCallTableCount (
  IN OUT MrcParameters *const MrcData
  )
{
  return ARRAY_COUNT (MrcCallTable);
}
