/** @file
  This file contains code related to initializing and configuring the DDRIO.

@copyright
  INTEL CONFIDENTIAL
  Copyright 2018 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains an 'Intel Peripheral Driver' and is uniquely identified as
  "Intel Reference Module" and is licensed for Intel CPUs and chipsets under
  the terms of your license agreement with Intel or your vendor. This file may
  be modified by the user, subject to additional terms of the license agreement.

@par Specification Reference:
**/
#include "MrcTypes.h"
#include "MrcInterface.h"
#include "MrcHalRegisterAccess.h"
#include "MrcCommon.h" // Needed for OFFSET_CALC_CH
#include "MrcDdrIoApi.h"
#include "MrcDdrIoApiInt.h"
#include "MrcReset.h"
#include "MrcDdrIoOffsets.h"
#include "Cpgc20TestCtl.h"
#include "MrcMcConfiguration.h"
#include "MrcChipApi.h"
#include "MrcDdr5Registers.h"
#include "MrcMemoryApi.h"
#include "MrcMaintenance.h"

/// Defines
#define VoltageSelect_VccDDQ   (0)
#define VoltageSelect_Vccdd2   (1)
#define MaxVoltageSelect       (2)
#define DDRIO_MAX_VTT_GEN      (4)
#define VCC_DLL_BYTES          (2)
#define VCCDLL1                (1)
#define VCCDLL2                (2)
#define VSXHI                  (3)
#define MAX_VSXHI_CODES        (5)
#define MAX_OFFSET_VOLTAGE     (2)
#define MAX_FF_RCVEN_PI        (5)
#define MAX_FF_RCVEN_PRE       (5)
#define MAX_VCCDLL_CODES       (7)
#define MAX_FF_WRITESTAGGER    (2)
#define MRC_NUMBURSTS_FFTIMING (32)
#define MAX_VCCDLL_DATA_PAR    (8)
#define MAX_VCCDLL_PAR         (12)
#define MAX_KICKBACK_CODES     (5)
#define NDEFAULT               (33)
#define MAX_VCCDLL_DATA_PAR    (8)
#define COMPVTARGET_SWINGV     (260)
#define COMPVTARGET_STEPV      (26)
#define THOUSAND_MULTIPLIER    (1000)
#define MAX_TCO_FEEDBACK_BITS  (4)
#define TCO_FEEDBACK_MASK      ((1 << MAX_TCO_FEEDBACK_BITS) - 1)
#define PN_REPEAT_READS        (3)
#define MAX_OPTIMIZED_PI_CB_EN (16)
#define MAX_OPTIMIZED_PI_SWEEP (16)
#define MAX_COARSE             (15)
#define VSSHI_REGION_NUM       (2)
#define DATAFUB_NUM_IN_REGION  (10)
#define CCCFUB_NUM_IN_REGION   (4)

const UINT32 OffsetTcoComp01[MRC_NUM_CCC_INSTANCES] = {
  CCC0_GLOBAL_CR_TCOCOMP_0_REG, CCC0_GLOBAL_CR_TCOCOMP_1_REG,
  CCC1_GLOBAL_CR_TCOCOMP_0_REG, CCC1_GLOBAL_CR_TCOCOMP_1_REG,
  CCC2_GLOBAL_CR_TCOCOMP_0_REG, CCC2_GLOBAL_CR_TCOCOMP_1_REG,
  CCC3_GLOBAL_CR_TCOCOMP_0_REG, CCC3_GLOBAL_CR_TCOCOMP_1_REG
};

const UINT32 OffsetTcoComp2[MRC_NUM_CCC_INSTANCES] = {
  CCC0_GLOBAL_CR_TCOCOMP_2_REG, CCC0_GLOBAL_CR_TCOCOMP_2_REG,
  CCC1_GLOBAL_CR_TCOCOMP_2_REG, CCC1_GLOBAL_CR_TCOCOMP_2_REG,
  CCC2_GLOBAL_CR_TCOCOMP_2_REG, CCC2_GLOBAL_CR_TCOCOMP_2_REG,
  CCC3_GLOBAL_CR_TCOCOMP_2_REG, CCC3_GLOBAL_CR_TCOCOMP_2_REG
};

/// Constants
const INT8 RxFifoChDelay[MRC_DDR_TYPE_UNKNOWN][MAX_GEARS][MAX_SYS_CHANNEL] = {
  // Gear1                               Gear2                               Gear4
  // Channel
  // 0,  1,  2,  3,  4,  5,  6,  7       0,  1,  2,  3,  4,  5,  6,  7       0,  1,  2,  3,  4,  5,  6,  7
  {{ 7,  9,  0,  0,  0,  0,  0,  0 }, { 11,  8,  0,  0,  0,  0,  0,  0 }, {  0,  0,  0,  0,  0,  0,  0,  0 }}, // DDR4 - only ch0..1
  {{ 5,  5,  3,  3,  0,  0,  0,  0 }, { 14, 14, 11, 11,  0,  0,  0,  0 }, { 14, 14, 14, 14, 14, 14, 14, 14 }}, // DDR5 - only ch0..3
  {{ 7,  7,  7,  7,  5,  5,  5,  5 }, { 11, 11, 11, 11,  9,  9,  9,  9 }, { 14, 14, 14, 14, 14, 14, 14, 14 }}, // LPDDR5
  {{ 5,  5,  5,  5,  3,  3,  3,  3 }, {  9,  9,  9,  9,  7,  7,  7,  7 }, { 14, 14, 14, 14, 13, 13, 13, 13 }}  // LPDDR4
};

const INT8 RxFifoChDelay_UlxUlt[MRC_DDR_TYPE_UNKNOWN][MAX_GEARS][MAX_SYS_CHANNEL] = {
  // Gear1                               Gear2                               Gear4
  // Channel
  // 0,  1,  2,  3,  4,  5,  6,  7       0,  1,  2,  3,  4,  5,  6,  7       0,  1,  2,  3,  4,  5,  6,  7
  {{ 7,  9,  0,  0,  0,  0,  0,  0 }, { 11,  8,  0,  0,  0,  0,  0,  0 }, {  0,  0,  0,  0,  0,  0,  0,  0 }}, // DDR4 - only ch0..1
  {{ 5,  5,  3,  3,  0,  0,  0,  0 }, {  9,  9,  9,  9,  0,  0,  0,  0 }, { 11, 11, 11, 11,  0,  0,  0,  0 }}, // DDR5 - only ch0..3
  {{ 7,  7,  7,  7,  5,  5,  5,  5 }, { 16, 16, 16, 16,  9,  9,  9,  9 }, { 10, 10, 10, 10, 10, 10, 10, 10 }}, // LPDDR5
  {{ 8,  8,  8,  8,  6,  6,  6,  6 }, { 16, 16, 16, 16, 11, 11, 11, 11 }, { 14, 14, 14, 14, 13, 13, 13, 13 }}  // LPDDR4
};

const UINT8 CccIndexToFub[MRC_NUM_CCC_INSTANCES]  = { 1, 3, 0, 2, 6, 4, 7, 5 }; // From CCC index to register FUB, for ADL-S only
const UINT8 CccMcIdxMapDdr[MRC_NUM_CCC_INSTANCES] = { 1, 1, 0, 0, 0, 0, 1, 1 }; // From CCC index to MC Controller, for DDR4/5 only, on ADL-S

/// Enums
typedef enum {
  DllDdrData0,
  DllDdrData1,
  DllDdrData2,
  DllDdrData3,
  DllDdrData4,
  DllDdrData5,
  DllDdrData6,
  DllDdrData7,
  DllDdrCcc0,
  DllDdrCcc1,
  DllDdrCcc2,
  DllDdrCcc3,
  DllDdrMax
} DLL_PARTITIONS;

typedef enum {
  CaVssHi,
  CtlVssHi,
  ClkVssHi,
  MaxCccVssHi
} CCC_VSSHI;

/// Structs
typedef struct {
  UINT8 Channel;
  UINT8 Byte;
} PHY_PARTITION_BLOCK;

const PHY_PARTITION_BLOCK LpddrDdr4ILDllPartitions[DllDdrMax][VCC_DLL_BYTES] = {

  //   Physical Channel, Phy Byte
    {{ 0, 0}, { 1, 0}}, //DllDdrData0
    {{ 0, 1}, { 1, 1}}, //DllDdrData1
    {{ 0, 2}, { 1, 2}}, //DllDdrData2
    {{ 0, 3}, { 1, 3}}, //DllDdrData3
    {{ 0, 4}, { 1, 4}}, //DllDdrData4
    {{ 0, 5}, { 1, 5}}, //DllDdrData5
    {{ 0, 6}, { 1, 6}}, //DllDdrData6
    {{ 0, 7}, { 1, 7}}, //DllDdrData7
    {{ 0, 0}, { 1, 0}}, //DllDdrCcc0
    {{ 2, 0}, { 3, 0}}, //DllDdrCcc1
    {{ 4, 0}, { 5, 0}}, //DllDdrCcc2
    {{ 6, 0}, { 7, 0}}, //DllDdrCcc3
};

const PHY_PARTITION_BLOCK Ddr4NILDllPartitions[DllDdrMax][VCC_DLL_BYTES] = {
  //   Physical Channel, Phy Byte
    {{ 0, 0}, { 0, 2}}, //DllDdrData0
    {{ 0, 1}, { 0, 3}}, //DllDdrData1
    {{ 0, 4}, { 0, 6}}, //DllDdrData2
    {{ 0, 5}, { 0, 7}}, //DllDdrData3
    {{ 1, 0}, { 1, 2}}, //DllDdrData4
    {{ 1, 1}, { 1, 3}}, //DllDdrData5
    {{ 1, 4}, { 1, 6}}, //DllDdrData6
    {{ 1, 5}, { 1, 7}}, //DllDdrData7
    {{ 0, 0}, { 1, 0}}, //DllDdrCcc0
    {{ 2, 0}, { 3, 0}}, //DllDdrCcc1
    {{ 4, 0}, { 5, 0}}, //DllDdrCcc2
    {{ 6, 0}, { 7, 0}}, //DllDdrCcc3
};

// -------- IMPORTANT NOTE --------
// Size of LpFreqCalRegList has to match LPFREQ_CAL_REG_NUM in MrcInterface.h.
// Update this define whenever you add/remove registers from this table.
const UINT32 LpFreqCalRegList[] = {
  DATA0CH0_CR_DCCCTL5_REG, DATA1CH0_CR_DCCCTL5_REG, DATA2CH0_CR_DCCCTL5_REG, DATA3CH0_CR_DCCCTL5_REG, DATA4CH0_CR_DCCCTL5_REG, DATA5CH0_CR_DCCCTL5_REG, DATA6CH0_CR_DCCCTL5_REG, DATA7CH0_CR_DCCCTL5_REG,
  DATA0CH1_CR_DCCCTL5_REG, DATA1CH1_CR_DCCCTL5_REG, DATA2CH1_CR_DCCCTL5_REG, DATA3CH1_CR_DCCCTL5_REG, DATA4CH1_CR_DCCCTL5_REG, DATA5CH1_CR_DCCCTL5_REG, DATA6CH1_CR_DCCCTL5_REG, DATA7CH1_CR_DCCCTL5_REG,
  DATA0CH0_CR_DCCCTL6_REG, DATA1CH0_CR_DCCCTL6_REG, DATA2CH0_CR_DCCCTL6_REG, DATA3CH0_CR_DCCCTL6_REG, DATA4CH0_CR_DCCCTL6_REG, DATA5CH0_CR_DCCCTL6_REG, DATA6CH0_CR_DCCCTL6_REG, DATA7CH0_CR_DCCCTL6_REG,
  DATA0CH1_CR_DCCCTL6_REG, DATA1CH1_CR_DCCCTL6_REG, DATA2CH1_CR_DCCCTL6_REG, DATA3CH1_CR_DCCCTL6_REG, DATA4CH1_CR_DCCCTL6_REG, DATA5CH1_CR_DCCCTL6_REG, DATA6CH1_CR_DCCCTL6_REG, DATA7CH1_CR_DCCCTL6_REG,
  DATA0CH0_CR_DCCCTL7_REG, DATA1CH0_CR_DCCCTL7_REG, DATA2CH0_CR_DCCCTL7_REG, DATA3CH0_CR_DCCCTL7_REG, DATA4CH0_CR_DCCCTL7_REG, DATA5CH0_CR_DCCCTL7_REG, DATA6CH0_CR_DCCCTL7_REG, DATA7CH0_CR_DCCCTL7_REG,
  DATA0CH1_CR_DCCCTL7_REG, DATA1CH1_CR_DCCCTL7_REG, DATA2CH1_CR_DCCCTL7_REG, DATA3CH1_CR_DCCCTL7_REG, DATA4CH1_CR_DCCCTL7_REG, DATA5CH1_CR_DCCCTL7_REG, DATA6CH1_CR_DCCCTL7_REG, DATA7CH1_CR_DCCCTL7_REG,
  DATA0CH0_CR_DCCCTL8_REG, DATA1CH0_CR_DCCCTL8_REG, DATA2CH0_CR_DCCCTL8_REG, DATA3CH0_CR_DCCCTL8_REG, DATA4CH0_CR_DCCCTL8_REG, DATA5CH0_CR_DCCCTL8_REG, DATA6CH0_CR_DCCCTL8_REG, DATA7CH0_CR_DCCCTL8_REG,
  DATA0CH1_CR_DCCCTL8_REG, DATA1CH1_CR_DCCCTL8_REG, DATA2CH1_CR_DCCCTL8_REG, DATA3CH1_CR_DCCCTL8_REG, DATA4CH1_CR_DCCCTL8_REG, DATA5CH1_CR_DCCCTL8_REG, DATA6CH1_CR_DCCCTL8_REG, DATA7CH1_CR_DCCCTL8_REG,
  DATA0CH0_CR_DCCCTL9_REG, DATA1CH0_CR_DCCCTL9_REG, DATA2CH0_CR_DCCCTL9_REG, DATA3CH0_CR_DCCCTL9_REG, DATA4CH0_CR_DCCCTL9_REG, DATA5CH0_CR_DCCCTL9_REG, DATA6CH0_CR_DCCCTL9_REG, DATA7CH0_CR_DCCCTL9_REG,
  DATA0CH1_CR_DCCCTL9_REG, DATA1CH1_CR_DCCCTL9_REG, DATA2CH1_CR_DCCCTL9_REG, DATA3CH1_CR_DCCCTL9_REG, DATA4CH1_CR_DCCCTL9_REG, DATA5CH1_CR_DCCCTL9_REG, DATA6CH1_CR_DCCCTL9_REG, DATA7CH1_CR_DCCCTL9_REG,
  DATA0CH0_CR_VCCDLLCOMPDATA_REG, DATA1CH0_CR_VCCDLLCOMPDATA_REG, DATA2CH0_CR_VCCDLLCOMPDATA_REG, DATA3CH0_CR_VCCDLLCOMPDATA_REG, DATA4CH0_CR_VCCDLLCOMPDATA_REG, DATA5CH0_CR_VCCDLLCOMPDATA_REG, DATA6CH0_CR_VCCDLLCOMPDATA_REG, DATA7CH0_CR_VCCDLLCOMPDATA_REG,
  DATA0CH1_CR_VCCDLLCOMPDATA_REG, DATA1CH1_CR_VCCDLLCOMPDATA_REG, DATA2CH1_CR_VCCDLLCOMPDATA_REG, DATA3CH1_CR_VCCDLLCOMPDATA_REG, DATA4CH1_CR_VCCDLLCOMPDATA_REG, DATA5CH1_CR_VCCDLLCOMPDATA_REG, DATA6CH1_CR_VCCDLLCOMPDATA_REG, DATA7CH1_CR_VCCDLLCOMPDATA_REG,
  CH0CCC_CR_DCCCTL6_REG, CH1CCC_CR_DCCCTL6_REG, CH2CCC_CR_DCCCTL6_REG, CH3CCC_CR_DCCCTL6_REG, CH4CCC_CR_DCCCTL6_REG, CH5CCC_CR_DCCCTL6_REG, CH6CCC_CR_DCCCTL6_REG, CH7CCC_CR_DCCCTL6_REG,
  CH0CCC_CR_DCCCTL7_REG, CH1CCC_CR_DCCCTL7_REG, CH2CCC_CR_DCCCTL7_REG, CH3CCC_CR_DCCCTL7_REG, CH4CCC_CR_DCCCTL7_REG, CH5CCC_CR_DCCCTL7_REG, CH6CCC_CR_DCCCTL7_REG, CH7CCC_CR_DCCCTL7_REG,
  CH0CCC_CR_DCCCTL8_REG, CH1CCC_CR_DCCCTL8_REG, CH2CCC_CR_DCCCTL8_REG, CH3CCC_CR_DCCCTL8_REG, CH4CCC_CR_DCCCTL8_REG, CH5CCC_CR_DCCCTL8_REG, CH6CCC_CR_DCCCTL8_REG, CH7CCC_CR_DCCCTL8_REG,
  CH0CCC_CR_DCCCTL9_REG, CH1CCC_CR_DCCCTL9_REG, CH2CCC_CR_DCCCTL9_REG, CH3CCC_CR_DCCCTL9_REG, CH4CCC_CR_DCCCTL9_REG, CH5CCC_CR_DCCCTL9_REG, CH6CCC_CR_DCCCTL9_REG, CH7CCC_CR_DCCCTL9_REG,
  CH0CCC_CR_DCCCTL11_REG, CH1CCC_CR_DCCCTL11_REG, CH2CCC_CR_DCCCTL11_REG, CH3CCC_CR_DCCCTL11_REG, CH4CCC_CR_DCCCTL11_REG, CH5CCC_CR_DCCCTL11_REG, CH6CCC_CR_DCCCTL11_REG, CH7CCC_CR_DCCCTL11_REG,
  CH0CCC_CR_DCCCTL20_REG, CH1CCC_CR_DCCCTL20_REG, CH2CCC_CR_DCCCTL20_REG, CH3CCC_CR_DCCCTL20_REG, CH4CCC_CR_DCCCTL20_REG, CH5CCC_CR_DCCCTL20_REG, CH6CCC_CR_DCCCTL20_REG, CH7CCC_CR_DCCCTL20_REG,
  CH0CCC_CR_VCCDLLCOMPDATACCC_REG, CH1CCC_CR_VCCDLLCOMPDATACCC_REG, CH2CCC_CR_VCCDLLCOMPDATACCC_REG, CH3CCC_CR_VCCDLLCOMPDATACCC_REG, CH4CCC_CR_VCCDLLCOMPDATACCC_REG, CH5CCC_CR_VCCDLLCOMPDATACCC_REG, CH6CCC_CR_VCCDLLCOMPDATACCC_REG, CH7CCC_CR_VCCDLLCOMPDATACCC_REG,
  DDRPHY_COMP_CR_DCCCTL4_REG, DDRPHY_COMP_CR_DLLCOMP_VCDLCTRL_REG, DDRPHY_COMP_NEW_CR_VCCDLLCOMPDATACCC_REG
};


const UINT8 gPatternTypes[DCD_GROUP_MAX] = {0x0, 0x0, 0x9, 0x3, 0xa};       // dllrefclk, dllvcdl, srzph0, srzph90, srzquad

const CHAR8* gDcdGrpNameStr[DCD_GROUP_MAX+1] = {
  "DllRefClk",
  "DllVcdl",
  "SrzPh0",
  "SrzPh90",
  "SrzQuad",
  "None"
};

/**
  Translate from CCC index to system-level Controller and Channel

  @param[in]   MrcData    - Pointer to global MRC data.
  @param[in]   CccIndex   - CCC instance index [0..7]
  @param[out]  Controller - Pointer to Controller value
  @param[out]  Channel    - Pointer to Channel value

**/
VOID
DdrIoTranslateCccToMcChannel (
  IN  MrcParameters *MrcData,
  IN  UINT32        CccIndex,
  OUT UINT32        *Controller,
  OUT UINT32        *Channel
  )
{
  MrcOutput *Outputs;
  BOOLEAN   Lpddr;
  BOOLEAN   DtHalo;

  Outputs = &MrcData->Outputs;
  Lpddr   = Outputs->Lpddr;
  DtHalo  = MrcData->Inputs.DtHalo;

  if (DtHalo && !Lpddr) {
    *Controller = CccMcIdxMapDdr[CccIndex];
  } else {
    *Controller = CccIndex / MAX_CHANNEL;         // LP4/5 and ADL-P: CCC[0:3] maps to MC0 and CCC[4:7] maps to MC1
  }
  if (Outputs->DdrType == MRC_DDR_TYPE_DDR4) {
    *Channel = 0;                                 // Only one channel per MC in DDR4
  } else if (Lpddr) {
    *Channel = CccIndex % Outputs->MaxChannels;   // LPDDR is 1:1
  } else { // DDR5
    if (DtHalo) {
      *Channel = (CccIndex < 4) ? 0 : 1;
    } else {
      *Channel = (CccIndex & MRC_BIT1) ? 1 : 0;   // CCC0, 1, 4, 5 -> CH0, CCC2, 3, 6, 7 -> CH1
    }
  }
}

/**
  This function updates the Gear specific fields in the DDRIO,
  based on Outputs->Gear2 and Outputs->Gear4.

  @param[in]  MrcData - Pointer to global MRC data.
**/
VOID
DdrIoConfigGear (
  IN  MrcParameters *MrcData
  )
{
  MrcOutput      *Outputs;
  INT64          GetSetVal;
  UINT32         Index;
  UINT32         Offset;
  UINT8          Pi0DivEn;
  UINT8          Pi0Inc;
  UINT8          Pi123DivEn;
  UINT8          Pi123Inc;
  UINT8          Pi4DivEn;
  UINT8          Pi4Inc;
  UINT8          PiSyncDiv;
  BOOLEAN        Gear1;
  BOOLEAN        Gear2;
  BOOLEAN        Gear4;
  UINT32         Controller;
  UINT32         Channel;
  UINT32         TransChannel;
  UINT32         TransStrobe;
  UINT32         Byte;
  UINT32         CccFub;
  BOOLEAN        UlxUlt;
  UINT32         Rank;
  BOOLEAN        Lpddr4;
  BOOLEAN        Lpddr5;
  BOOLEAN        Ddr5;
  BOOLEAN        Lp5Gear4;
  BOOLEAN        Ddr5Gear4;
  CH0CCC_CR_DDRCRPINSUSED_STRUCT      CccPinsUsed;
  CH0CCC_CR_DDRCRCCCPIDIVIDER_STRUCT  CccPiDivider;
  CH0CCC_CR_CTL3_STRUCT               CccCtl3;
  CH0CCC_CR_CTL0_STRUCT               CccCtl0;
  DATA0CH0_CR_SRZCTL_STRUCT           DataSrzCtl;

  Outputs     = &MrcData->Outputs;
  Gear2       = Outputs->Gear2;
  Gear4       = Outputs->Gear4;
  Gear1       = (!Gear2 && !Gear4);
  UlxUlt      = MrcData->Inputs.UlxUlt;
  Lpddr4      = (Outputs->DdrType == MRC_DDR_TYPE_LPDDR4);
  Lpddr5      = (Outputs->DdrType == MRC_DDR_TYPE_LPDDR5);
  Ddr5        = (Outputs->DdrType == MRC_DDR_TYPE_DDR5);
  Lp5Gear4    = (Lpddr5 && Gear4);
  Ddr5Gear4   = (Ddr5 && Gear4);

  GetSetVal = Gear1;    // DDRIO Register encoding: 1 = Gear1, 0 = Gear2 or Gear4

  MrcGetSetNoScope (MrcData, GsmIocScramGear1, WriteToCache, &GetSetVal);
  MrcGetSetChStrb (MrcData, MAX_CONTROLLER, MAX_CHANNEL, MAX_SDRAM_IN_DIMM, GsmIocDataCtlGear1, WriteToCache, &GetSetVal);

  GetSetVal = Gear4;
  MrcGetSetNoScope (MrcData, GsmIocScramGear4, WriteToCache, &GetSetVal);
  MrcGetSetChStrb (MrcData, MAX_CONTROLLER, MAX_CHANNEL, MAX_SDRAM_IN_DIMM, GsmIocDataCtlGear4, WriteToCache, &GetSetVal);

  MrcFlushRegisterCachedData(MrcData);

  // Update CMD/CLK/CTL PI Dividers
  if (Lpddr5) {
    Pi123DivEn = 1;
    // for WCK pi4, set en to 0 and Div to 3
    Pi0DivEn = 1;
    if (Gear2) {
      PiSyncDiv = 2;
      Pi0Inc    = 2;
      Pi123Inc  = 1;
      Pi4Inc    = 3;
      Pi4DivEn  = 0;
    } else if (Gear4) {
      PiSyncDiv   = 1;
      Pi0Inc      = 0;
      Pi123Inc    = 0;
      Pi4Inc      = 0;
      Pi4DivEn    = 0;
      Pi0DivEn    = 0;
      Pi123DivEn  = 0;
    } else {
      PiSyncDiv = 3;
      Pi0Inc    = 1;
      Pi123Inc  = 0;
      Pi4Inc    = 2;
      Pi4DivEn  = 1;
    }
  } else { // LP4 / DDR4 / DDR5
    if (Gear2) {
      PiSyncDiv   = 0;
      Pi0DivEn    = 0;
      Pi0Inc      = 0;
      Pi123DivEn  = 0;
      Pi123Inc    = 0;
      Pi4Inc      = 0;
      Pi4DivEn    = 0;
    } else if (Gear4) {
      PiSyncDiv   = 0;
      Pi0Inc      = Ddr5 ? 2 : 0;
      Pi123Inc    = Ddr5 ? 1 : 0;
      Pi4Inc      = Ddr5 ? 2 : 0;
      Pi4DivEn    = Ddr5 ? 1 : 0;
      Pi0DivEn    = Ddr5 ? 1 : 0;
      Pi123DivEn  = Ddr5 ? 1 : 0;
    } else {
      PiSyncDiv   = 0;
      Pi0DivEn    = 1;
      Pi0Inc      = 2;
      Pi123DivEn  = 1;
      Pi123Inc    = 2;
      Pi4Inc      = 2;
      Pi4DivEn    = 1;
    }
  }

  CccPiDivider.Data = 0;
  CccPiDivider.Bits.PiClkDuration     = CH2CCC_CR_DDRCRCCCPIDIVIDER_PiClkDuration_MAX; // @todo - Set final value in MrcMcActivate.
  CccPiDivider.Bits.PiSyncDivider     = PiSyncDiv;
  CccPiDivider.Bits.WckHalfPreamble   = 1; // 4:1 mode
  CccPiDivider.Bits.Pi4IncPreamble    = (Gear2 ) ? 2 : (Gear4) ? 0 : 1;
  CccPiDivider.Bits.Pi4DivEnPreamble  = Lp5Gear4 ? 0 : 1;
  // Cmd
  CccPiDivider.Bits.Pi0DivEn  = Pi0DivEn;
  CccPiDivider.Bits.Pi0Inc    = Pi0Inc;
  // Ctl
  CccPiDivider.Bits.Pi1DivEn  = Pi123DivEn;
  CccPiDivider.Bits.Pi1Inc    = Pi123Inc;
  // Ctl (Lp, Ddr4 Ccc1) / Cke (Ddr4 Ccc2)
  CccPiDivider.Bits.Pi2DivEn  = Pi123DivEn;
  CccPiDivider.Bits.Pi2Inc    = Pi123Inc;
  // Clk
  CccPiDivider.Bits.Pi3DivEn  = Lp5Gear4 ? 1 : Pi123DivEn;
  CccPiDivider.Bits.Pi3Inc    = Lp5Gear4 ? 1 : (Ddr5Gear4 ? 2 : Pi123Inc);
  // Cke (Lp4) / Wck (Lp5)
  CccPiDivider.Bits.Pi4DivEn  = Pi4DivEn;
  CccPiDivider.Bits.Pi4Inc    = Pi4Inc;
  MrcWriteCR (MrcData, CCC_CR_DDRCRCCCPIDIVIDER_REG, CccPiDivider.Data);

  for (Index = 0; Index < MRC_NUM_CCC_INSTANCES; Index++) {
    CccFub = UlxUlt ? Index : CccIndexToFub[Index];
    DdrIoTranslateCccToMcChannel (MrcData, Index, &Controller, &Channel);
    if (!MrcChannelExist (MrcData, Controller, Channel)) {
      continue;
    }
    Offset = OFFSET_CALC_CH (CH2CCC_CR_DDRCRPINSUSED_REG, CH0CCC_CR_DDRCRPINSUSED_REG, CccFub);
    CccPinsUsed.Data = MrcReadCR (MrcData, Offset);
    CccPinsUsed.Bits.Gear1 = Gear1;
    CccPinsUsed.Bits.Gear4 = Gear4;
    MrcWriteCR (MrcData, Offset, CccPinsUsed.Data);

    Offset = OFFSET_CALC_CH (CH2CCC_CR_CTL3_REG, CH0CCC_CR_CTL3_REG, CccFub);
    CccCtl3.Data = MrcReadCR (MrcData, Offset);
    CccCtl3.Bits.dcdsrz_cmn_gear4en = Gear4;
    CccCtl3.Bits.dcdsrz_cmn_gear2en = Gear2;
    CccCtl3.Bits.dcdsrz_cmn_gear1en = Gear1;
    MrcWriteCR (MrcData, Offset, CccCtl3.Data);

    Offset = OFFSET_CALC_CH (CH2CCC_CR_CTL0_REG, CH0CCC_CR_CTL0_REG, CccFub);
    CccCtl0.Data = MrcReadCR (MrcData, Offset);
    CccCtl0.Bits.ccctx_rgrp4_gear4enclkdata = Gear4;
    CccCtl0.Bits.ccctx_rgrp4_gear4enclkdata = (Lpddr5 || Ddr5) && Gear4;     // 1 if CCCBuf7 and 8 are CK or WCK (in the case of LP5) modes and in G4, else 0
    CccCtl0.Bits.ccctx_rgrp3_gear4enclkdata = (Lpddr4 || Ddr5) && Gear4;     // 1 if CCCBuf5 and 6 are CK or WCK (in the case of LP5) modes and in G4, else 0
    MrcWriteCR (MrcData, Offset, CccCtl0.Data);
  }

  for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
    for (Channel = 0; Channel < Outputs->MaxChannels; Channel++) {
      for (Byte = 0; Byte < Outputs->SdramCount; Byte++) {
        if (!MrcByteExist (MrcData, Controller, Channel, Byte)) {
            continue;
        }
        TransChannel = Channel;
        TransStrobe  = Byte;
        MrcTranslateSystemToIp (MrcData, &Controller, &TransChannel, &Rank, &TransStrobe, GsmIocTxOn); // No GetSet for this register, so use another per-byte PHY CR
        Offset = DATA0CH0_CR_SRZCTL_REG +
                (DATA0CH1_CR_SRZCTL_REG - DATA0CH0_CR_SRZCTL_REG) * TransChannel +
                (DATA1CH0_CR_SRZCTL_REG - DATA0CH0_CR_SRZCTL_REG) * TransStrobe;
        DataSrzCtl.Data = MrcReadCR (MrcData, Offset);
        DataSrzCtl.Bits.dcdsrz_cmn_gear2en = Gear2;
        DataSrzCtl.Bits.dcdsrz_cmn_gear4en = Gear4;
        DataSrzCtl.Bits.dcdsrz_cmn_gear1en = Gear1;
        MrcWriteCR (MrcData, Offset, DataSrzCtl.Data);
      }
    }
  }
}

/**
  Run Comp FSM for the initial calibration stages

  @param[in, out] MrcData - Include all MRC global data.

  @retval mrcSuccess if no timeout
**/
MrcStatus
MrcInitCompRun (
  IN OUT MrcParameters *const MrcData
  )
{
  const MRC_FUNCTION *MrcCall;
  MrcStatus Status;
  UINT64    Timeout;
  BOOLEAN   Done;
  DDRPHY_COMP_CR_COMPOVERRIDE_STRUCT  CompOverride;

  Status = mrcSuccess;
  MrcCall = MrcData->Inputs.Call.Func;

  // Manually kick off compensation sequence
  CompOverride.Data = MrcReadCR (MrcData, DDRPHY_COMP_CR_COMPOVERRIDE_REG);
  CompOverride.Bits.comp_complete = 0;
  CompOverride.Bits.init_mrc      = 1;
  MrcWriteCR (MrcData, DDRPHY_COMP_CR_COMPOVERRIDE_REG, CompOverride.Data);

  // Poll for Comp Sequence Completion
  Timeout = MrcCall->MrcGetCpuTime () + 5; // 5ms
  Done = FALSE;
  do {
    CompOverride.Data = MrcReadCR (MrcData, DDRPHY_COMP_CR_COMPOVERRIDE_REG);
    if (MrcData->Inputs.SimicsFlag == 1) { // No Simics support for this FSM yet
      Done = TRUE;
      break;
    }
    Done = (CompOverride.Bits.comp_complete == 1);
  } while (!Done && (MrcCall->MrcGetCpuTime () < Timeout));

  if (!Done) {
    Status = mrcTimeout;
  }
  CompOverride.Data = MrcReadCR (MrcData, DDRPHY_COMP_CR_COMPOVERRIDE_REG);
  CompOverride.Bits.comp_complete = 0;
  CompOverride.Bits.init_mrc      = 0;
  MrcWriteCR (MrcData, DDRPHY_COMP_CR_COMPOVERRIDE_REG, CompOverride.Data);

  return Status;
}

/**
  Force comp codes distribution

  @param[in, out] MrcData - Include all MRC global data.
**/
VOID
MrcForceDistCompCodes (
  IN OUT MrcParameters *const MrcData
  )
{
  DDRSCRAM_CR_DDRSCRAMBLECH0_STRUCT DdrScramCh0;
  INT64                             GetSetVal;

  DdrScramCh0.Data = MrcReadCR (MrcData, DDRSCRAM_CR_DDRSCRAMBLECH0_REG);
  DdrScramCh0.Bits.forcecompdist = 1;
  MrcWriteCR (MrcData, DDRSCRAM_CR_DDRSCRAMBLECH0_REG, DdrScramCh0.Data);

  // Wait 2us for comp distribution to complete
  MrcWait (MrcData, 2 * MRC_TIMER_1US);

  DdrScramCh0.Bits.forcecompdist = 0;
  MrcWriteCR (MrcData, DDRSCRAM_CR_DDRSCRAMBLECH0_REG, DdrScramCh0.Data);

  if (MrcData->Inputs.B0) {
    GetSetVal = 1;
    MrcGetSetNoScope (MrcData, GsmIocForceCmpUpdt, WriteNoCache, &GetSetVal); // DDRSCRAM_CR_DDRMISCCONTROL0.forcecompupdate = 1

    // Wait 2us for comp distribution to complete
    MrcWait (MrcData, 2 * MRC_TIMER_1US);
  }
}

/**
  PBD step size calibration step (scomp.delay_range)

  @param[in, out] MrcData - Include all MRC global data.

  @retval mrcSuccess
**/
MrcStatus
MrcPbdStepSizeCalibration (
  IN OUT MrcParameters* const MrcData
  )
{
  MrcStatus Status;
  MrcDebug  *Debug;
  MrcOutput *Outputs;
  UINT32    PbdRatioDen;
  UINT32    PbdRatioNum;
  UINT32    DlyStage;
  UINT32    ScompCode;
  UINT32    Data32;
  UINT16    Qclkps;
  DDRPHY_COMP_CR_FSMSKIPCTRL_STRUCT   FsmSkipCtrl;
  DDRPHY_COMP_CR_FSMSKIPCTRL_STRUCT   FsmSkipCtrlSave;
  DDRPHY_COMP_CR_SCOMP_STRUCT         SComp;
  DDRPHY_COMP_CR_SCOMP_STRUCT         SCompSave;
  DDRPHY_COMP_CR_DDRCRCMDCOMP_STRUCT  CmdComp;

  Status  = mrcSuccess;
  Outputs = &MrcData->Outputs;
  Debug   = &Outputs->Debug;
  Qclkps  = Outputs->Qclkps;

  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\nPBD step calibration\n");
  if (Outputs->Gear4) {
    PbdRatioDen = 256;
  } else if (Outputs->Gear2) {
    PbdRatioDen = 128;
  } else { // Gear1
    PbdRatioDen = 64;
  }

  FsmSkipCtrlSave.Data = MrcReadCR (MrcData, DDRPHY_COMP_CR_FSMSKIPCTRL_REG);
  FsmSkipCtrl.Data = 0;
  FsmSkipCtrl.Bits.En_scompcmd = 1;
  MrcWriteCR (MrcData, DDRPHY_COMP_CR_FSMSKIPCTRL_REG, FsmSkipCtrl.Data);

  SComp.Data = MrcReadCR (MrcData, DDRPHY_COMP_CR_SCOMP_REG);
  SCompSave.Data = SComp.Data;
  SComp.Bits.scomp_cmn_comprangemode = 1;

  // Configure SCOMP to 100ps
  if (Qclkps > 1500) {
    SComp.Bits.scomp_cmn_refclkphasesel = 1; // Phase lock
    SComp.Bits.cmddlytapselstage = UDIVIDEROUND (Qclkps, 200);
  } else {
    SComp.Bits.scomp_cmn_refclkphasesel = 0; // Cycle lock
    SComp.Bits.cmddlytapselstage = UDIVIDEROUND (Qclkps, 100);
  }
  MrcWriteCR (MrcData, DDRPHY_COMP_CR_SCOMP_REG, SComp.Data);

  Status = MrcInitCompRun (MrcData);
  if (Status == mrcTimeout) {
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_ERROR, "%s PbdCal Comp %s\n", gErrString, gTimeout);
  } else {
    CmdComp.Data  = MrcReadCR (MrcData, DDRPHY_COMP_CR_DDRCRCMDCOMP_REG);
    DlyStage  = SComp.Bits.cmddlytapselstage;
    ScompCode = CmdComp.Bits.ScompCmd;
    PbdRatioNum = DlyStage * ScompCode;
    Data32 = PbdRatioNum * 100;                               // For integer math
    Outputs->PbdRatio = UDIVIDEROUND (Data32, PbdRatioDen);   // Final ratio, times 100
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "DlyStage: %u ScompCode: %u PbdRatioNum: %u\n", DlyStage, ScompCode, PbdRatioNum);
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "PbdRatioDen: %d --> PbdRatio*100 = %u\n", PbdRatioDen, Outputs->PbdRatio);
  }

  // Cleanup
  MrcWriteCR (MrcData, DDRPHY_COMP_CR_SCOMP_REG, SCompSave.Data);
  MrcWriteCR (MrcData, DDRPHY_COMP_CR_FSMSKIPCTRL_REG, FsmSkipCtrlSave.Data);

  return Status;
}

/**
  Force Clock Align calibration step (clkphasedetect.qclk2d0align)

  @param[in, out] MrcData - Include all MRC global data.

  @retval mrcSuccess
**/
MrcStatus
MrcForceClkAlignCalibration (
  IN OUT MrcParameters* const MrcData
  )
{
  const MRC_FUNCTION *MrcCall;
  MrcStatus Status;
  MrcDebug  *Debug;
  MrcInput  *Inputs;
  MrcOutput *Outputs;
  UINT32    Offset;
  UINT32    FirstController;
  UINT32    FirstChannel;
  UINT32    Controller;
  UINT32    Channel;
  UINT32    TransChannel;
  UINT32    Rank;
  UINT32    Strobe;
  UINT32    TransStrobe;
  UINT32    Index;
  UINT32    CccFub;
  UINT64    Timeout;
  BOOLEAN   Done;
  BOOLEAN   A0;
  BOOLEAN   UlxUlt;
  CCC0_GLOBAL_CR_PM_FSM_OVRD_0_STRUCT   CccGlobalPmFsmOvrd0;
  DATA0CH0_CR_PMFSM1_STRUCT             DataPmFsm1;
  DATA0CH0_CR_CLKALIGNCTL0_STRUCT       DataClkAlignCtl0;
  CH0CCC_CR_CLKALIGNCTL0_STRUCT         CccClkAlignCtl0;
  CH0CCC_CR_MDLLCTL0_STRUCT             CccMdllCtl0;
  DATA0CH0_CR_MDLLCTL0_STRUCT           DataMdllCtl0;
  CH0CCC_CR_CLKALIGNSTATUS_STRUCT       CccClkAlignStatus;
  DATA0CH0_CR_CLKALIGNSTATUS_STRUCT     DataClkAlignStatus;


  Status  = mrcSuccess;
  Inputs  = &MrcData->Inputs;
  Outputs = &MrcData->Outputs;
  Debug   = &Outputs->Debug;
  MrcCall = Inputs->Call.Func;
  A0      = Inputs->A0;
  UlxUlt  = Inputs->UlxUlt;

  FirstController = Outputs->FirstPopController;
  FirstChannel    = Outputs->Controller[FirstController].FirstPopCh;
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\nForceClkAlignCalibration\n");

  // First disabling all overrides - as the same seq is called multiple times during cold boot SAGV. Before we enable overrides, we need to ensure that overrides are 0
  MrcWriteCrMulticast (MrcData, CCC_GLOBAL_CR_PM_FSM_OVRD_0_REG, 0);
  MrcWriteCrMulticast (MrcData, CCC_GLOBAL_CR_PM_FSM_OVRD_1_REG, 0);
  MrcWriteCrMulticast (MrcData, DATA_CR_PMFSM1_REG, 0);

  // Set clkalignrst_b = 0
  // Read from first populated byte and broadcast to all
  Strobe = 0;    // Rank is not used here
  MrcTranslateSystemToIp (MrcData, &FirstController, &FirstChannel, &Rank, &Strobe, GsmIocTxOn); // No GetSet for this register, so use another per-byte PHY CR
  Offset  = DATA0CH0_CR_CLKALIGNCTL0_REG +
           (DATA0CH1_CR_CLKALIGNCTL0_REG - DATA0CH0_CR_CLKALIGNCTL0_REG) * FirstChannel +
           (DATA1CH0_CR_CLKALIGNCTL0_REG - DATA0CH0_CR_CLKALIGNCTL0_REG) * Strobe;

  DataClkAlignCtl0.Data = MrcReadCR (MrcData, Offset);
  DataClkAlignCtl0.Bits.clkalignrst_b = 0;
  MrcWriteCrMulticast (MrcData, DATA_CR_CLKALIGNCTL0_REG, DataClkAlignCtl0.Data);

  CccClkAlignCtl0.Data = MrcReadCR (MrcData, CH0CCC_CR_CLKALIGNCTL0_REG);
  CccClkAlignCtl0.Bits.clkalignrst_b = 0;
  MrcWriteCrMulticast (MrcData, CCC_CR_CLKALIGNCTL0_REG, CccClkAlignCtl0.Data);

  // After disabling overrides, we enable oevrride for setting ldoen, dllen, pien and clkalign before running Init DCC.
  CccGlobalPmFsmOvrd0.Data = 0;
  CccGlobalPmFsmOvrd0.Bits.pmclkgatedisable     = 1;
  CccGlobalPmFsmOvrd0.Bits.clkalign_ovren       = 1;
  CccGlobalPmFsmOvrd0.Bits.pien_ovren           = 1;
  CccGlobalPmFsmOvrd0.Bits.dllen_ovren          = 1;
  CccGlobalPmFsmOvrd0.Bits.compupdatedone_ovren = 1;
  CccGlobalPmFsmOvrd0.Bits.ldoen_ovrval         = 1;
  MrcWriteCrMulticast (MrcData, CCC_GLOBAL_CR_PM_FSM_OVRD_0_REG, CccGlobalPmFsmOvrd0.Data);
  MrcWriteCrMulticast (MrcData, CCC_GLOBAL_CR_PM_FSM_OVRD_1_REG, CccGlobalPmFsmOvrd0.Data); // Registers are identical

  DataPmFsm1.Data = 0;
  DataPmFsm1.Bits.pmclkgatedisable      = 1;
  DataPmFsm1.Bits.clkalign_ovren        = 1;
  DataPmFsm1.Bits.pien_ovren            = 1;
  DataPmFsm1.Bits.dllen_ovren           = 1;
  DataPmFsm1.Bits.compupdatedone_ovren  = 1;
  DataPmFsm1.Bits.ldoen_ovrval          = 1;
  MrcWriteCrMulticast (MrcData, DATA_CR_PMFSM1_REG, DataPmFsm1.Data);

  MrcWait (MrcData, 100 * MRC_TIMER_1NS);

  CccGlobalPmFsmOvrd0.Bits.dllen_ovrval  = 1;
  MrcWriteCrMulticast (MrcData, CCC_GLOBAL_CR_PM_FSM_OVRD_0_REG, CccGlobalPmFsmOvrd0.Data);
  MrcWriteCrMulticast (MrcData, CCC_GLOBAL_CR_PM_FSM_OVRD_1_REG, CccGlobalPmFsmOvrd0.Data); // Registers are identical

  DataPmFsm1.Bits.dllen_ovrval  = 1;
  MrcWriteCrMulticast (MrcData, DATA_CR_PMFSM1_REG, DataPmFsm1.Data);

  // Poll for MDLL lock
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Polling on MDLLCTL0.mdll_cmn_mdlllock..\n");
  Timeout = MrcCall->MrcGetCpuTime () + MRC_WAIT_TIMEOUT; // 10 seconds timeout
  do {
    Done = TRUE;                           // Assume all locked
    if (MrcData->Inputs.SimicsFlag == 1) { // No Simics support for this FSM yet
      break;
    }
    for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
      for (Channel = 0; Channel < Outputs->MaxChannels; Channel++) {
        if (!MrcChannelExist (MrcData, Controller, Channel)) {
          continue;
        }
        for (Strobe = 0; Strobe < Outputs->SdramCount; Strobe++) {
          TransChannel = Channel;
          TransStrobe  = Strobe;
          MrcTranslateSystemToIp (MrcData, &Controller, &TransChannel, &Rank, &TransStrobe, GsmIocTxOn); // No GetSet for this register, so use another per-byte PHY CR
          Offset  = DATA0CH0_CR_MDLLCTL0_REG +
                   (DATA0CH1_CR_MDLLCTL0_REG - DATA0CH0_CR_MDLLCTL0_REG) * TransChannel +
                   (DATA1CH0_CR_MDLLCTL0_REG - DATA0CH0_CR_MDLLCTL0_REG) * TransStrobe;
          DataMdllCtl0.Data = MrcReadCR (MrcData, Offset);
          if (DataMdllCtl0.Bits.mdll_cmn_mdlllock == 0) {
            Done = FALSE;
          }
        }
      }
    }
    for (Index = 0; Index < MRC_NUM_CCC_INSTANCES; Index++) {
      CccFub = UlxUlt ? Index : CccIndexToFub[Index];
      DdrIoTranslateCccToMcChannel (MrcData, Index, &Controller, &Channel);
      if (!MrcChannelExist (MrcData, Controller, Channel)) {
        continue;
      }
      if (A0) {
        Offset = OFFSET_CALC_CH (CH2CCC_CR_MDLLCTL0_A0_REG, CH0CCC_CR_MDLLCTL0_A0_REG, CccFub);
      } else {
        Offset = OFFSET_CALC_CH (CH2CCC_CR_MDLLCTL0_REG, CH0CCC_CR_MDLLCTL0_REG, CccFub);
      }
      //MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Polling on CccMdllCtl0.mdll_cmn_mdlllock at CCC%u MC%u C%u --> 0x%x\n", Index, Controller, Channel, Offset);
      CccMdllCtl0.Data  = MrcReadCR (MrcData, Offset);
      if (CccMdllCtl0.Bits.mdll_cmn_mdlllock == 0) {
        Done = FALSE;
      }
    }
  } while (!Done && (MrcCall->MrcGetCpuTime () < Timeout));

  if (!Done) {
    MrcWait (MrcData, MRC_TIMER_1MS * 100);   // Wait 100ms to print final lock values
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_ERROR, "%s mdll lock timeout\n",gErrString);
    for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
      for (Channel = 0; Channel < Outputs->MaxChannels; Channel++) {
        if (!MrcChannelExist (MrcData, Controller, Channel)) {
          continue;
        }
        for (Strobe = 0; Strobe < Outputs->SdramCount; Strobe++) {
          TransChannel = Channel;
          TransStrobe  = Strobe;
          MrcTranslateSystemToIp (MrcData, &Controller, &TransChannel, &Rank, &TransStrobe, GsmIocTxOn); // No GetSet for this register, so use another per-byte PHY CR
          Offset  = DATA0CH0_CR_MDLLCTL0_REG +
                   (DATA0CH1_CR_MDLLCTL0_REG - DATA0CH0_CR_MDLLCTL0_REG) * TransChannel +
                   (DATA1CH0_CR_MDLLCTL0_REG - DATA0CH0_CR_MDLLCTL0_REG) * TransStrobe;
          DataMdllCtl0.Data = MrcReadCR (MrcData, Offset);
          MRC_DEBUG_MSG (Debug, MSG_LEVEL_ERROR, "MC%u C%u B%u mdll_cmn_mdlllock = %u\n", Controller, Channel, Strobe, DataMdllCtl0.Bits.mdll_cmn_mdlllock);
        }
      }
    }
    for (Index = 0; Index < MRC_NUM_CCC_INSTANCES; Index++) {
      CccFub = UlxUlt ? Index : CccIndexToFub[Index];
      DdrIoTranslateCccToMcChannel (MrcData, Index, &Controller, &Channel);
      if (!MrcChannelExist (MrcData, Controller, Channel)) {
        continue;
      }
      if (A0) {
        Offset = OFFSET_CALC_CH (CH2CCC_CR_MDLLCTL0_A0_REG, CH0CCC_CR_MDLLCTL0_A0_REG, CccFub);
      } else {
        Offset = OFFSET_CALC_CH (CH2CCC_CR_MDLLCTL0_REG, CH0CCC_CR_MDLLCTL0_REG, CccFub);
      }
      CccMdllCtl0.Data  = MrcReadCR (MrcData, Offset);
      MRC_DEBUG_MSG (Debug, MSG_LEVEL_ERROR, "CCC%u: mdll_cmn_mdlllock = %u\n", Channel, CccMdllCtl0.Bits.mdll_cmn_mdlllock);
    }
    return mrcTimeout;
  }

  MrcWait (MrcData, 350 * MRC_TIMER_1NS);

  // Set pien_ovrval = 1
  CccGlobalPmFsmOvrd0.Bits.pien_ovrval  = 1;
  MrcWriteCrMulticast (MrcData, CCC_GLOBAL_CR_PM_FSM_OVRD_0_REG, CccGlobalPmFsmOvrd0.Data);
  MrcWriteCrMulticast (MrcData, CCC_GLOBAL_CR_PM_FSM_OVRD_1_REG, CccGlobalPmFsmOvrd0.Data); // Registers are identical

  DataPmFsm1.Bits.pien_ovrval  = 1;
  MrcWriteCrMulticast (MrcData, DATA_CR_PMFSM1_REG, DataPmFsm1.Data);

  // Set clkalignrst_ovrval = 1
  CccGlobalPmFsmOvrd0.Bits.clkalignrst_ovrval  = 1;
  MrcWriteCrMulticast (MrcData, CCC_GLOBAL_CR_PM_FSM_OVRD_0_REG, CccGlobalPmFsmOvrd0.Data);
  MrcWriteCrMulticast (MrcData, CCC_GLOBAL_CR_PM_FSM_OVRD_1_REG, CccGlobalPmFsmOvrd0.Data); // Registers are identical

  DataPmFsm1.Bits.clkalignrst_ovrval  = 1;
  MrcWriteCrMulticast (MrcData, DATA_CR_PMFSM1_REG, DataPmFsm1.Data);

  // Set clkalignrst_b = 1
  DataClkAlignCtl0.Bits.clkalignrst_b = 1;
  MrcWriteCrMulticast (MrcData, DATA_CR_CLKALIGNCTL0_REG, DataClkAlignCtl0.Data);

  CccClkAlignCtl0.Bits.clkalignrst_b = 1;
  MrcWriteCrMulticast (MrcData, CCC_CR_CLKALIGNCTL0_REG, CccClkAlignCtl0.Data);

  // Set start_clkalign_ovrval = 1
  CccGlobalPmFsmOvrd0.Bits.start_clkalign_ovrval  = 1;
  MrcWriteCrMulticast (MrcData, CCC_GLOBAL_CR_PM_FSM_OVRD_0_REG, CccGlobalPmFsmOvrd0.Data);
  MrcWriteCrMulticast (MrcData, CCC_GLOBAL_CR_PM_FSM_OVRD_1_REG, CccGlobalPmFsmOvrd0.Data); // Registers are identical

  DataPmFsm1.Bits.start_clkalign_ovrval  = 1;
  MrcWriteCrMulticast (MrcData, DATA_CR_PMFSM1_REG, DataPmFsm1.Data);

  // Poll for clkalign_complete
  // @todo_adl Add timeout check
  do {
    Done = TRUE;                           // Assume all complete
    // MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Polling on DATA_CR_CLKALIGNSTATUS.clkalign_complete_level\n");
    for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
      for (Channel = 0; Channel < Outputs->MaxChannels; Channel++) {
        if (!MrcChannelExist (MrcData, Controller, Channel)) {
          continue;
        }
        for (Strobe = 0; Strobe < Outputs->SdramCount; Strobe++) {
          TransChannel = Channel;
          TransStrobe  = Strobe;
          MrcTranslateSystemToIp (MrcData, &Controller, &TransChannel, &Rank, &TransStrobe, GsmIocTxOn); // No GetSet for this register, so use another per-byte PHY CR
          Offset  = DATA0CH0_CR_CLKALIGNSTATUS_REG +
                   (DATA0CH1_CR_CLKALIGNSTATUS_REG - DATA0CH0_CR_CLKALIGNSTATUS_REG) * TransChannel +
                   (DATA1CH0_CR_CLKALIGNSTATUS_REG - DATA0CH0_CR_CLKALIGNSTATUS_REG) * TransStrobe;
          MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, " MC%u C%u B%u", Controller, Channel, Strobe);
          DataClkAlignStatus.Data = MrcReadCR (MrcData, Offset);
          if (DataClkAlignStatus.Bits.clkalign_complete_level == 0) {
            Done = FALSE;
          } else {
            MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, " ref2xclkpicode: 0x%02X", DataClkAlignStatus.Bits.ref2xclkpicode);
          }
          MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\n");
        }
      }
    }
    for (Index = 0; Index < MRC_NUM_CCC_INSTANCES; Index++) {
      CccFub = UlxUlt ? Index : CccIndexToFub[Index];
      DdrIoTranslateCccToMcChannel (MrcData, Index, &Controller, &Channel);
      if (!MrcChannelExist (MrcData, Controller, Channel)) {
        continue;
      }
      Offset = OFFSET_CALC_CH (CH2CCC_CR_CLKALIGNSTATUS_REG, CH0CCC_CR_CLKALIGNSTATUS_REG, CccFub);
      //MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Polling on CLKALIGNSTATUS.clkalign_complete_level at 0x%x\n", Offset);
      MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, " CCC%u", Index);
      CccClkAlignStatus.Data  = MrcReadCR (MrcData, Offset);
      if (CccClkAlignStatus.Bits.clkalign_complete_level == 0) {
        Done = FALSE;
      } else {
        MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, " ref2xclkpicode: 0x%02X", CccClkAlignStatus.Bits.ref2xclkpicode);
      }
      MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\n");
    }

    if (MrcData->Inputs.SimicsFlag == 1) { // No Simics support for this FSM yet
      break;
    }
  } while (!Done);

  return Status;
}

/**
  Run Bandwidth Selection Calibration FSM

  @param[in, out] MrcData         - Include all MRC global data
  @param[out]     BwSel           - VCC DLL BWSEL final code
  @param[out]     CbEn            - VCC DLL cben final code
  @param[out]     Error           - BWSel Comp error status

  @retval Bandwidth Selection Calibration status
**/
MrcStatus
MrcRunBWSelEn (
  IN OUT MrcParameters* const MrcData,
  OUT UINT32                  *BwSel,
  OUT UINT32                  *CbEn,
  OUT UINT32                  *Error
  )
{
  const MRC_FUNCTION *MrcCall;
  MrcStatus          Status;
  MrcDebug           *Debug;
  UINT32             BwSelError;
  UINT64             Timeout;
  BOOLEAN            BwSelDone;
  DDRPHY_COMP_NEW_CR_VCCDLLCOMPDATACCC_STRUCT   VccDllCompDataCcc;
  DDRPHY_COMP_CR_DLLCOMP_VCDLCTRL_STRUCT        DllCompVcdlCtrl;

  MrcCall    = MrcData->Inputs.Call.Func;
  BwSelError = 0;
  BwSelDone  = FALSE;
  Timeout    = MrcCall->MrcGetCpuTime () + 100;    // 100ms timeout
  Debug      = &MrcData->Outputs.Debug;
  Status     = mrcSuccess;

  // Start bwselcomp
  DllCompVcdlCtrl.Data = MrcReadCR (MrcData, DDRPHY_COMP_CR_DLLCOMP_VCDLCTRL_REG);
  DllCompVcdlCtrl.Bits.vcdllcr_bwselen = 0;
  MrcWriteCR (MrcData, DDRPHY_COMP_CR_DLLCOMP_VCDLCTRL_REG, DllCompVcdlCtrl.Data);
  DllCompVcdlCtrl.Bits.vcdllcr_bwselen = 1;
  DllCompVcdlCtrl.Bits.vcdllcr_bwseldone = 0;
  MrcWriteCR (MrcData, DDRPHY_COMP_CR_DLLCOMP_VCDLCTRL_REG, DllCompVcdlCtrl.Data);

  do {
    DllCompVcdlCtrl.Data = MrcReadCR (MrcData, DDRPHY_COMP_CR_DLLCOMP_VCDLCTRL_REG);
    BwSelError = DllCompVcdlCtrl.Bits.vcdllcr_bwselerrout;
    BwSelDone  = (DllCompVcdlCtrl.Bits.vcdllcr_bwseldone == 1);
    if (MrcData->Inputs.SimicsFlag == 1) { // No Simics support for this FSM yet
      break;
    }
  } while ((BwSelDone == FALSE) && (MrcCall->MrcGetCpuTime () < Timeout) && (BwSelError == 0));

  if (BwSelError == 1) {
    Status = mrcFail;
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_ERROR, "MrcRunBWSelEn: FSM error reported\n");
  }

  if (BwSelDone == FALSE) {
    Status = mrcTimeout;
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_ERROR, "MrcRunBWSelEn: FSM timeout\n");
  }

  VccDllCompDataCcc.Data = MrcReadCR (MrcData, DDRPHY_COMP_NEW_CR_VCCDLLCOMPDATACCC_REG);
  *BwSel = VccDllCompDataCcc.Bits.Dll_bwsel;
  *Error = BwSelError;
  if (MrcData->Inputs.Q0Regs) {
    *CbEn = VccDllCompDataCcc.BitsQ0.cben;
  } else {
    *CbEn = DllCompVcdlCtrl.Bits.dllcomp_cmn_vcdlcben;
  }

  return Status;
}

/**
  DLL LDO Bandwidth Select calibration step (dllldocomp.dllbwsel_cal)

  @param[in, out] MrcData         - Include all MRC global data.
  @param[in]      DistributeCodes - Distribute results to data/ccc partitions if TRUE.

  @retval mrcSuccess
**/
MrcStatus
MrcBwSelCalibration (
  IN OUT MrcParameters* const MrcData,
  IN     BOOLEAN              DistributeCodes
  )
{
  MrcStatus     Status;
  MrcDebug      *Debug;
  MrcInput      *Inputs;
  MrcOutput     *Outputs;
  UINT32        BwSel;
  UINT32        CBen;
  UINT32        Error;
  BOOLEAN       A0;
  BOOLEAN       UlxUlt;
  UINT32        VdllTargDacCode;
  UINT32        VdllTargDacCodeSave;
  DDRPHY_COMP_CR_COMPOVERRIDE_STRUCT            CompOverride;
  DDRPHY_COMP_CR_DLLCOMP_VCDLCTRL_STRUCT        DllCompVcdlCtrl;
  DDRPHY_COMP_NEW_CR_VCCDLLCOMPDATACCC_STRUCT   VccDllCompDataCcc;
  DDRPHY_COMP_NEW_CR_VSSHICOMP_CTRL3_STRUCT     VsshiCompCtrl3;
  DDRPHY_COMP_CR_DLLCOMP_VDLLCTRL_STRUCT        DllCompVdllCtrl;
  DDRPHY_COMP_CR_COMPDLLWL_STRUCT               CompDllWl;

  Status  = mrcSuccess;
  Inputs  = &MrcData->Inputs;
  Outputs = &MrcData->Outputs;
  Debug   = &Outputs->Debug;
  A0      = Inputs->A0;
  UlxUlt  = Inputs->UlxUlt;
  BwSel   = 0;
  CBen    = 0;
  Error   = 0;
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\nBwSelCalibration: DistCodes = %u\n", DistributeCodes);

  CompOverride.Data = MrcReadCR (MrcData, DDRPHY_COMP_CR_COMPOVERRIDE_REG);
  CompOverride.Bits.compclkon       = 1;
  CompOverride.Bits.dllcompclkon    = 1;
  CompOverride.Bits.bwselcompclkon  = 1;
  MrcWriteCR (MrcData, DDRPHY_COMP_CR_COMPOVERRIDE_REG, CompOverride.Data);

  if (!A0) {
    VsshiCompCtrl3.Data = MrcReadCR (MrcData, DDRPHY_COMP_NEW_CR_VSSHICOMP_CTRL3_REG);
    VsshiCompCtrl3.Bits.bwsel_lo_threshold = BWSEL_LO_THRESH;
    MrcWriteCR (MrcData, DDRPHY_COMP_NEW_CR_VSSHICOMP_CTRL3_REG, VsshiCompCtrl3.Data);
  }

  if (UlxUlt) {
    CompDllWl.Data = MrcReadCR (MrcData, DDRPHY_COMP_CR_COMPDLLWL_REG);
    CompDllWl.Bits.wlcompen = 1;
    MrcWriteCR (MrcData, DDRPHY_COMP_CR_COMPDLLWL_REG,CompDllWl.Data );

    DllCompVdllCtrl.Data = MrcReadCR (MrcData, DDRPHY_COMP_CR_DLLCOMP_VDLLCTRL_REG);
    VdllTargDacCodeSave  = (DllCompVdllCtrl.Bits.dllcomp_cmn_vdlltargdaccode * Outputs->Vdd2Mv) / 256;
    VdllTargDacCode      = VdllTargDacCodeSave;

    MrcRunBWSelEn (MrcData, &BwSel, &CBen, &Error);
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "VdllTargDacCode: %u, BwSel: %u, CBen: %u, Error: %u\n", VdllTargDacCode, BwSel, CBen, Error);

    if (BwSel < SW_BWSEL_LO_THRESH) {
      do {
        VdllTargDacCode -= VDLL_TARG_STEP;
        DllCompVdllCtrl.Data = MrcReadCR (MrcData, DDRPHY_COMP_CR_DLLCOMP_VDLLCTRL_REG);
        DllCompVdllCtrl.Bits.dllcomp_cmn_vdlltargdaccode = (256 * VdllTargDacCode) / Outputs->Vdd2Mv;
        MrcWriteCR (MrcData, DDRPHY_COMP_CR_DLLCOMP_VDLLCTRL_REG, DllCompVdllCtrl.Data);
        MrcRunBWSelEn (MrcData, &BwSel, &CBen, &Error);
        MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "VdllTargDacCode: %u, BwSel: %u, CBen: %u, Error: %u\n", VdllTargDacCode, BwSel, CBen, Error);
      } while ((BwSel < SW_BWSEL_LO_THRESH) && (VdllTargDacCode > VDLL_LO_THRESH));

      if ((BwSel > (SW_BWSEL_LO_THRESH + 10)) || (CBen < 2)) {    // check for FSM divergence
        MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Divergent FSM Behavior Detected. Adding guardband to the VccDll target\n");
        VdllTargDacCode = MIN (VdllTargDacCode + VDLL_TARG_STEP + VDLL_TARG_GB, VdllTargDacCodeSave);
        DllCompVdllCtrl.Data = MrcReadCR (MrcData, DDRPHY_COMP_CR_DLLCOMP_VDLLCTRL_REG);
        DllCompVdllCtrl.Bits.dllcomp_cmn_vdlltargdaccode = (256 * VdllTargDacCode) / Outputs->Vdd2Mv;
        MrcWriteCR (MrcData, DDRPHY_COMP_CR_DLLCOMP_VDLLCTRL_REG, DllCompVdllCtrl.Data);

        MrcRunBWSelEn (MrcData, &BwSel, &CBen, &Error);
        MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "VdllTargDacCode: %u, BwSel: %u, CBen: %u, Error: %u\n", VdllTargDacCode, BwSel, CBen, Error);
      }
    }
  } else {
    MrcRunBWSelEn (MrcData, &BwSel, &CBen, &Error);
  }

  // If bwsel FSM fails, assume that full range has been checked with no valid bwsel code
  if (Error == 1) {
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "bwselerrout is 1, setting bwsel = 1 and cben = 2\n");
    BwSel = 1;
    CBen  = 2;
  } else if (BwSel < 1) {
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "bwsel is 0, changing to 1\n");
    BwSel = 1;
  }

  VccDllCompDataCcc.Data = MrcReadCR (MrcData, DDRPHY_COMP_NEW_CR_VCCDLLCOMPDATACCC_REG);
  VccDllCompDataCcc.Bits.Dll_bwsel = BwSel;
  MrcWriteCR (MrcData, DDRPHY_COMP_NEW_CR_VCCDLLCOMPDATACCC_REG, VccDllCompDataCcc.Data);

  DllCompVcdlCtrl.Data = MrcReadCR (MrcData, DDRPHY_COMP_CR_DLLCOMP_VCDLCTRL_REG);
  DllCompVcdlCtrl.Bits.dllcomp_cmn_vcdlcben = CBen;
  MrcWriteCR (MrcData, DDRPHY_COMP_CR_DLLCOMP_VCDLCTRL_REG, DllCompVcdlCtrl.Data);

  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Dll_bwsel: %u, CBen: %u\n", BwSel, CBen);

  if (DistributeCodes) {
    MrcCbEnDistribute (MrcData);
  }

  // Clean up
  DllCompVcdlCtrl.Bits.vcdllcr_bwselen   = 0;
  DllCompVcdlCtrl.Bits.vcdllcr_bwseldone = 0;
  MrcWriteCR (MrcData, DDRPHY_COMP_CR_DLLCOMP_VCDLCTRL_REG, DllCompVcdlCtrl.Data);

  CompOverride.Bits.compclkon       = 0;
  CompOverride.Bits.dllcompclkon    = 0;
  CompOverride.Bits.bwselcompclkon  = 0;
  MrcWriteCR (MrcData, DDRPHY_COMP_CR_COMPOVERRIDE_REG, CompOverride.Data);

  return Status;
}

/**
  Initial Rload calibration step (panicdrv.rload)

  @param[in, out] MrcData - Include all MRC global data.

  @retval mrcSuccess
**/
MrcStatus
MrcRloadCalibration (
  IN OUT MrcParameters* const MrcData
  )
{
  MrcStatus Status;
  MrcDebug  *Debug;
  MrcOutput *Outputs;
  UINT32    Offset;
  UINT32    FirstController;
  UINT32    FirstChannel;
  UINT32    Rank;
  UINT32    Strobe;
  UINT32    DqsRload;
  BOOLEAN   A0;
  DDRPHY_COMP_CR_FSMSKIPCTRL_STRUCT   FsmSkipCtrl;
  DDRPHY_COMP_CR_FSMSKIPCTRL_STRUCT   FsmSkipCtrlSave;
  DDRPHY_COMP_CR_MISCCOMPCODES_STRUCT MiscCompCodes;
  DDRVCCDLL0_CR_AFE_CTRL0_STRUCT      VccDllAfeCtrl0;
  DATA0CH0_CR_MDLLCTL3_STRUCT         DataMdllCtl3;
  DATA0CH0_CR_COMPCTL2_STRUCT         DataCompCtl2;
  CH0CCC_CR_MDLLCTL3_STRUCT           CccMdllCtl3;
  CH0CCC_CR_CTL0_STRUCT               CccCtl0;
  DDRPHY_COMP_CR_DDRCRCMDCOMP_STRUCT  CmdComp;
  BOOLEAN                             Lpddr5;

  Status  = mrcSuccess;
  Outputs = &MrcData->Outputs;
  Debug   = &Outputs->Debug;
  A0      = MrcData->Inputs.A0;
  Lpddr5  = (Outputs->DdrType == MRC_DDR_TYPE_LPDDR5);

  FirstController = Outputs->FirstPopController;
  FirstChannel    = Outputs->Controller[FirstController].FirstPopCh;
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\nRloadCalibration\n");

  // Run cmdup and dqsrload stages. cmdup is used as a reference for dqsrload
  FsmSkipCtrlSave.Data = MrcReadCR (MrcData, DDRPHY_COMP_CR_FSMSKIPCTRL_REG);
  FsmSkipCtrl.Data = 0;
  FsmSkipCtrl.Bits.En_cmdup = 1;
  FsmSkipCtrl.Bits.En_dqsrload = 1;
  MrcWriteCR (MrcData, DDRPHY_COMP_CR_FSMSKIPCTRL_REG, FsmSkipCtrl.Data);

  Status = MrcInitCompRun (MrcData);
  if (Status != mrcSuccess) {
    return Status;
  }

  MiscCompCodes.Data = MrcReadCR (MrcData, DDRPHY_COMP_CR_MISCCOMPCODES_REG);
  CmdComp.Data       = MrcReadCR (MrcData, DDRPHY_COMP_CR_DDRCRCMDCOMP_REG);
  DqsRload = MiscCompCodes.Bits.Dqsrload;
  if ((Lpddr5) && (Outputs->Frequency > f4800)) {
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Dqsrload=%u, cmd_up=%u, RloadTarget=%u\n", DqsRload, CmdComp.Bits.RcompDrvUp, RLOAD_TARGET_LP5_HIGH_FREQ);
  } else {
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Dqsrload=%u, cmd_up=%u, RloadTarget=%u\n", DqsRload, CmdComp.Bits.RcompDrvUp, RLOAD_TARGET);
  }
  // Manually distribute rload to destinations
  VccDllAfeCtrl0.Data = MrcReadCR (MrcData, DDRVCCDLL0_CR_AFE_CTRL0_REG);
  VccDllAfeCtrl0.Bits.comp_iolvrtop_cmn_rloadcomp = DqsRload;
  VccDllAfeCtrl0.Bits.iolvrtop_cmn_biasvrefsel    = 3;        // @todo_adl This value is taked from Seq3, static_DIG2 - VCC
  MrcWriteCrMulticast (MrcData, DDRVCCDLL_CR_AFE_CTRL0_REG, VccDllAfeCtrl0.Data);

  // Read from first populated byte and broadcast to all
  Strobe = 0;    // Rank is not used here
  MrcTranslateSystemToIp (MrcData, &FirstController, &FirstChannel, &Rank, &Strobe, GsmIocTxOn); // No GetSet for this register, so use another per-byte PHY CR
  Offset  = DATA0CH0_CR_MDLLCTL3_REG +
           (DATA0CH1_CR_MDLLCTL3_REG - DATA0CH0_CR_MDLLCTL3_REG) * FirstChannel +
           (DATA1CH0_CR_MDLLCTL3_REG - DATA0CH0_CR_MDLLCTL3_REG) * Strobe;

  DataMdllCtl3.Data = MrcReadCR (MrcData, Offset);
  DataMdllCtl3.Bits.mdll_cmn_rloadcomp = DqsRload;
  MrcWriteCrMulticast (MrcData, DATA_CR_MDLLCTL3_REG, DataMdllCtl3.Data);

  Offset  = DATA0CH0_CR_COMPCTL2_REG +
           (DATA0CH1_CR_COMPCTL2_REG - DATA0CH0_CR_COMPCTL2_REG) * FirstChannel +
           (DATA1CH0_CR_COMPCTL2_REG - DATA0CH0_CR_COMPCTL2_REG) * Strobe;

  DataCompCtl2.Data = MrcReadCR (MrcData, Offset);
  DataCompCtl2.Bits.rxbias_cmn_rloadcomp = DqsRload;
  MrcWriteCrMulticast (MrcData, DATA_CR_COMPCTL2_REG, DataCompCtl2.Data);

  // Read from CCC0 and broadcast to CCC[0..7]
  Offset = A0 ? CH0CCC_CR_MDLLCTL3_A0_REG : CH0CCC_CR_MDLLCTL3_REG;
  CccMdllCtl3.Data = MrcReadCR (MrcData, Offset);
  CccMdllCtl3.Bits.mdll_cmn_rloadcomp = DqsRload;
  Offset = A0 ? CCC_CR_MDLLCTL3_A0_REG : CCC_CR_MDLLCTL3_REG;
  MrcWriteCrMulticast (MrcData, Offset, CccMdllCtl3.Data);

  CccCtl0.Data = MrcReadCR (MrcData, CH0CCC_CR_CTL0_REG);
  CccCtl0.Bits.rxbias_cmn_rloadcomp = DqsRload;
  MrcWriteCrMulticast (MrcData, CCC_CR_CTL0_REG, CccCtl0.Data);
  MrcWriteCR (MrcData, DDRPHY_COMP_CR_FSMSKIPCTRL_REG, FsmSkipCtrlSave.Data);
  return Status;
}

/**
  DLL LDO FF Code calibration step (dllldocomp.ldoff)

  @param[in, out] MrcData - Include all MRC global data.

  @retval mrcSuccess
**/
MrcStatus
MrcLdoFFCodeCalibration (
  IN OUT MrcParameters* const MrcData
  )
{
  MrcDebug  *Debug;
  MrcStatus Status;
  DDRPHY_COMP_CR_FSMSKIPCTRL_STRUCT   FsmSkipCtrl;
  DDRPHY_COMP_CR_FSMSKIPCTRL_STRUCT   FsmSkipCtrlSave;

  Status = mrcSuccess;
  Debug  = &MrcData->Outputs.Debug;
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\nLdoFFCodeCalibration\n");

  FsmSkipCtrlSave.Data = MrcReadCR (MrcData, DDRPHY_COMP_CR_FSMSKIPCTRL_REG);
  FsmSkipCtrl.Data = 0;
  FsmSkipCtrl.Bits.En_vdlllock = 1;
  FsmSkipCtrl.Bits.En_pbiascal = 1;
  FsmSkipCtrl.Bits.En_codepi   = 1;
  FsmSkipCtrl.Bits.En_coderead = 1;
  FsmSkipCtrl.Bits.En_codewl   = 1;
  MrcWriteCR (MrcData, DDRPHY_COMP_CR_FSMSKIPCTRL_REG, FsmSkipCtrl.Data);

  Status = MrcInitCompRun (MrcData);
  if (Status != mrcSuccess) {
    return Status;
  }

  // Because the step following this one enables the DLL and PI's in the data and CCC partitions, force distribute
  MrcForceDistCompCodes (MrcData);

  MrcWriteCR (MrcData, DDRPHY_COMP_CR_FSMSKIPCTRL_REG, FsmSkipCtrlSave.Data);
  return Status;
}

/**
  Run LDO FFCode FSM and return Dll_codepi and Dll_coderead comp codes

  @param[in, out] MrcData   - Include all MRC global data.
  @param[out]     CodePi    - Return comp code
  @param[out]     CodeRead  - Return comp code

  @retval mrcSuccess
**/
MrcStatus
MrcRunLdoFFCodeStage (
  IN OUT MrcParameters *const  MrcData,
     OUT UINT32                *CodePi,
     OUT UINT32                *CodeRead
  )
{
  MrcStatus Status;
  DDRPHY_COMP_NEW_CR_VCCDLLCOMPDATACCC_STRUCT VccDllCompDataCcc;
  DDRPHY_COMP_NEW_CR_DDRCRDATACOMP1_STRUCT    CompDataComp1;

  Status = MrcInitCompRun (MrcData);

  VccDllCompDataCcc.Data = MrcReadCR (MrcData, DDRPHY_COMP_NEW_CR_VCCDLLCOMPDATACCC_REG);
  CompDataComp1.Data     = MrcReadCR (MrcData, DDRPHY_COMP_NEW_CR_DDRCRDATACOMP1_REG);

  *CodePi   = VccDllCompDataCcc.Bits.Dll_codepi;
  *CodeRead = CompDataComp1.Bits.Dll_coderead;

  return Status;
}

/**
  Set ldoffcodelock value

  @param[in, out] MrcData    - Include all MRC global data.
  @param[in]      CodeFFLock - ldoffcodelock value

  @retval mrcSuccess
**/
void
MrcLdoFFCodeLockSetVal (
  IN OUT MrcParameters* const MrcData,
  IN     UINT32               CodeFFLock
  )
{
  DDRPHY_COMP_CR_DLLCOMP_SWCAPCTRL_STRUCT DllCompSwCapCtrl;

  DllCompSwCapCtrl.Data = MrcReadCR (MrcData, DDRPHY_COMP_CR_DLLCOMP_SWCAPCTRL_REG);
  DllCompSwCapCtrl.Bits.ldoffcodelock = CodeFFLock;
  MrcWriteCR (MrcData, DDRPHY_COMP_CR_DLLCOMP_SWCAPCTRL_REG, DllCompSwCapCtrl.Data);
}

#define CODEFFREAD_THRESH_HI    96  // maximum allowed value of coderead to provide headroom for saturation, range is 0 - 127
#define CODEFFPI_THRESH_HI      48  // maximum allowed value of codepi to provide headroom for saturation, range is 0 - 63
#define CODEFFLOCK_THRESH_HI    255 // maximum allowed value of codefflock, range is CODEFFLOCK_THRESH_LO - 255
#define CODEFFLOCK_THRESH_LO    15  // minimum allowed value of codefflock, range is 1 - CODEFFLOCK_THRESH_HI
#define CODEFFLOCK_MAXIMA_INC   4   // codefflock search increment for coderead/pi maxima search
#define CODEFFLOCK_SEARCH_INC   2   // codefflock search increment for coderead/pi threshold search

/**
  DLL LDO FF Code Lock calibration step (dllldocomp.codelock_cal)

  @param[in, out] MrcData - Include all MRC global data.

  @retval mrcSuccess
**/
MrcStatus
MrcLdoFFCodeLockCalibration (
  IN OUT MrcParameters* const MrcData
  )
{
  MrcDebug  *Debug;
  MrcStatus Status;
  UINT32    CodeFFLock;
  UINT32    CodeFFLockOrig;
  UINT32    CodeFFLockMax;
  UINT32    CodeFFLockMaxForCodePi;
  UINT32    CodeFFLockMaxForCodeRead;
  UINT32    CodePi;
  UINT32    CodeRead;
  UINT32    CodePiMax;
  UINT32    CodeReadMax;
  INT64     GetSetVal;
  BOOLEAN   Found;
  BOOLEAN   A0;
  DDRPHY_COMP_CR_FSMSKIPCTRL_STRUCT       FsmSkipCtrl;
  DDRPHY_COMP_CR_FSMSKIPCTRL_STRUCT       FsmSkipCtrlSave;
  DDRPHY_COMP_CR_SCOMPCTL_STRUCT          ScompCtl;
  DDRPHY_COMP_NEW_CR_VCCDLLCOMPDATACCC_STRUCT VccDllCompDataCcc;
  DDRPHY_COMP_CR_MISCCOMPCODES_STRUCT         MiscCompCodes;
  DDRPHY_COMP_CR_DLLCOMP_SWCAPCTRL_STRUCT     DllCompSwCapCtrl;

  Status = mrcSuccess;
  Debug  = &MrcData->Outputs.Debug;
  A0     = MrcData->Inputs.A0;
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\nLdoFFCodeLockCalibration\n");

  ScompCtl.Data = MrcReadCR (MrcData, DDRPHY_COMP_CR_SCOMPCTL_REG);
  ScompCtl.Bits.Spare = 0xC;
  MrcWriteCR (MrcData, DDRPHY_COMP_CR_SCOMPCTL_REG, ScompCtl.Data);

  // Enable only dll_vdlllock, dll_pbiascal, codepi and coderead FSM Stages
  FsmSkipCtrlSave.Data = MrcReadCR (MrcData, DDRPHY_COMP_CR_FSMSKIPCTRL_REG);
  FsmSkipCtrl.Data = 0;
  FsmSkipCtrl.Bits.En_vdlllock = 1;
  FsmSkipCtrl.Bits.En_pbiascal = 1;
  FsmSkipCtrl.Bits.En_codepi   = 1;
  FsmSkipCtrl.Bits.En_coderead = 1;
  MrcWriteCR (MrcData, DDRPHY_COMP_CR_FSMSKIPCTRL_REG, FsmSkipCtrl.Data);

  // Save current value of the calculated codelock to restore at the end of the routine
  DllCompSwCapCtrl.Data = MrcReadCR (MrcData, DDRPHY_COMP_CR_DLLCOMP_SWCAPCTRL_REG);
  CodeFFLockOrig = DllCompSwCapCtrl.Bits.ldoffcodelock;

  CodeFFLockMaxForCodePi    = CODEFFLOCK_THRESH_LO; // ffcodelock @ global maxima of codepi
  CodeFFLockMaxForCodeRead  = CODEFFLOCK_THRESH_LO; // ffcodelock @ global maxima of coderead
  CodePiMax   = 0;
  CodeReadMax = 0;

  if (A0) {
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "MAX search for coderead/codepi\n");
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "CodeFFLock\tCodePi\tCodeRead  pbiascal vdlllock\n");
    // Search for the codefflock that gives global maxima for coderead and codepi
    // The threshold search will start from the global maxima
    for (CodeFFLock = CODEFFLOCK_THRESH_HI; CodeFFLock >= CODEFFLOCK_THRESH_LO; CodeFFLock -= CODEFFLOCK_MAXIMA_INC) {
      MrcLdoFFCodeLockSetVal (MrcData, CodeFFLock);
      Status = MrcRunLdoFFCodeStage (MrcData, &CodePi, &CodeRead);    // kick off a comp cycle
      if (Status == mrcTimeout) {
        MrcLdoFFCodeLockSetVal (MrcData, CODEFFLOCK_THRESH_HI);       // Unblock FSM
        MrcWait (MrcData, MRC_TIMER_1US * 100);
        break;
      }
      if (CodePi >= CodePiMax) {
        CodeFFLockMaxForCodePi = CodeFFLock;
        CodePiMax = CodePi;
      }
      if (CodeRead >= CodeReadMax) {
        CodeFFLockMaxForCodeRead = CodeFFLock;
        CodeReadMax = CodeRead;
      }
      MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "  %3u\t\t%3u\t%3u", CodeFFLock, CodePi, CodeRead);
      VccDllCompDataCcc.Data = MrcReadCR (MrcData, DDRPHY_COMP_NEW_CR_VCCDLLCOMPDATACCC_REG);
      MiscCompCodes.Data     = MrcReadCR (MrcData, DDRPHY_COMP_CR_MISCCOMPCODES_REG);
      MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\t%3u\t%3u\n", MiscCompCodes.Bits.Dllpbiascal, VccDllCompDataCcc.Bits.Dll_vdlllock);
    }
    // Start threshold search with the minimum codefflock of the codepi and coderead maxima
    CodeFFLockMax = MIN (CodeFFLockMaxForCodePi, CodeFFLockMaxForCodeRead);
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "%sForCodePi=%3u, %sForCodeRead=%3u --> %s=%3u", "CodeFFLockMax", CodeFFLockMaxForCodePi, "CodeFFLockMax", CodeFFLockMaxForCodeRead, "CodeFFLockMax", CodeFFLockMax);
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, " CodePiMax=%3u CodeReadMax=%3u\n", CodePiMax, CodeReadMax);
  } else {
    CodeFFLockMax = CODEFFLOCK_THRESH_HI;
  }
  Found = FALSE;
  // CodeRead/PI Threshold Search -  Start search from codefflock_maxima
  // Pbias will become weaker as codelock is decremented, causing coderead and codepi to also decrement
  for (CodeFFLock = CodeFFLockMax; CodeFFLock >= CODEFFLOCK_THRESH_LO; CodeFFLock -= CODEFFLOCK_SEARCH_INC) {
    MrcLdoFFCodeLockSetVal (MrcData, CodeFFLock);
    Status = MrcRunLdoFFCodeStage (MrcData, &CodePi, &CodeRead); // kick off a comp cycle
    if (Status != mrcSuccess) {
      return Status;
    }
    VccDllCompDataCcc.Data = MrcReadCR (MrcData, DDRPHY_COMP_NEW_CR_VCCDLLCOMPDATACCC_REG);
    MiscCompCodes.Data     = MrcReadCR (MrcData, DDRPHY_COMP_CR_MISCCOMPCODES_REG);
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "CodeFFLock=%3u --> CodePi=%3u CodeRead=%3u", CodeFFLock, CodePi, CodeRead);
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, " Dllpbiascal=%3u Dll_vdlllock=%3u\n", MiscCompCodes.Bits.Dllpbiascal, VccDllCompDataCcc.Bits.Dll_vdlllock);

    if ((CodePi <= CODEFFPI_THRESH_HI) && (CodeRead <= CODEFFREAD_THRESH_HI)) {
      Found = TRUE;
      break; // codelock found
    }
  }
  if (!Found) {
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_WARNING, "%s cannot find a passing code!\n", gWarnString);
  }

  if (A0) {
    // Restore original ffcodelock
    MrcLdoFFCodeLockSetVal (MrcData, CodeFFLockOrig);
  } else {
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "--> Final CodeFFLock=%u\n", CodeFFLock);
    // Distribute the final code in Data and CCC fubs
    // Note the WriteNoCache mode because MDLLCTL2 has RW/V field (mdll_cmn_vcdldcccode)
    GetSetVal = CodeFFLock;
    MrcGetSetChStrb (MrcData, MAX_CONTROLLER, MAX_CHANNEL, MAX_SDRAM_IN_DIMM, GsmIocLdoFFCodeLockData, WriteNoCache, &GetSetVal);
    MrcGetSetCccInst (MrcData, MAX_CCC_INSTANCES, GsmIocLdoFFCodeLockCcc, WriteNoCache, &GetSetVal);
  }

  // Cleanup
  MrcWriteCR (MrcData, DDRPHY_COMP_CR_FSMSKIPCTRL_REG, FsmSkipCtrlSave.Data);

  ScompCtl.Bits.Spare = 0;
  MrcWriteCR (MrcData, DDRPHY_COMP_CR_SCOMPCTL_REG, ScompCtl.Data);

  return Status;
}

/**
  Enter or Exit VssHi training mode

  @param[in, out] MrcData             - Include all MRC global data.
  @param[in]      VsshiRegion         - North / South, see VssHiRegionType
  @param[in]      VssHiVdd2TargRatio  - Target VssHi voltage
  @param[in]      SensePoint          - Near / Mid / Far, see VssHiSensePointType
  @param[in]      Enter               - Enter or Exit the mode: 1 - Enter, 0 - Exit

  @retval None
**/
VOID
MrcEnterVssHiTraining (
  IN OUT MrcParameters* const MrcData,
  IN VssHiRegionType          VsshiRegion,
  IN UINT32                   VssHiVdd2TargRatio,
  IN VssHiSensePointType      SensePoint,
  IN UINT32                   Enter
  )
{
  UINT32    VttIndex;
  UINT32    Offset;
  DDRPHY_COMP_CR_VSSHICOMP_FFCOMP_STRUCT    VsshiCompFfComp;
  DDRPHY_COMP_CR_VIEWANA_CBBSEL_STRUCT      ViewAnaCbbSel;
  DDRPHY_COMP_CR_VIEWANA_CTRL_STRUCT        ViewAnaCtrl;
  DDRVTT0_CR_AFE_VIEWCTRL_STRUCT            AfeViewCtrl;
  DDRPHY_COMP_CR_COMPOVERRIDE_STRUCT        CompOverride;

  VttIndex  = 0;

  VsshiCompFfComp.Data = MrcReadCR (MrcData, DDRPHY_COMP_CR_VSSHICOMP_FFCOMP_REG);
  VsshiCompFfComp.Bits.vsshicomp_cmn_vsxhiglobalfdbken = Enter; // 0 - selects vsxhi_local (default and comp setting), 1 - selects vsxhi_nom or analog view
  MrcWriteCR (MrcData, DDRPHY_COMP_CR_VSSHICOMP_FFCOMP_REG, VsshiCompFfComp.Data);

  if (SensePoint == VssHiSensePointNear) {
    ViewAnaCbbSel.Data = MrcReadCR (MrcData, DDRPHY_COMP_CR_VIEWANA_CBBSEL_REG);
    if (VsshiRegion == VssHiRegionNorth) {
      ViewAnaCbbSel.Bits.vsshicomp_cmn_selanaviewforobs = 0; // selects between vsxhi_nom (0) and analog view (1)
    } else { // South
      ViewAnaCtrl.Data = MrcReadCR (MrcData, DDRPHY_COMP_CR_VIEWANA_CTRL_REG);
      ViewAnaCtrl.Bits.viewmxana_cmn_viewanaen  = Enter;  // enable analog view mux in comp partition
      ViewAnaCtrl.Bits.viewmxana_cmn_viewanasel = Enter;  // select afe_vsshidrvs_viewana_cmn_anaviewmuxouta_hv (input 1) for the analog view mux
      MrcWriteCR (MrcData, DDRPHY_COMP_CR_VIEWANA_CTRL_REG, ViewAnaCtrl.Data);

      ViewAnaCbbSel.Bits.vsshicomp_cmn_selanaviewforobs = Enter;
      ViewAnaCbbSel.Bits.vsxhiopamp_ch0_anaviewen       = Enter; // enables vsshi opamp voltage to be fed back
      ViewAnaCbbSel.Bits.vsxhiopamp_ch0_anaviewsel      = 5;
    }
    MrcWriteCR (MrcData, DDRPHY_COMP_CR_VIEWANA_CBBSEL_REG, ViewAnaCbbSel.Data);
  } else { // Mid or Far
    ViewAnaCbbSel.Data = MrcReadCR (MrcData, DDRPHY_COMP_CR_VIEWANA_CBBSEL_REG);
    ViewAnaCbbSel.Bits.vsshicomp_cmn_selanaviewforobs = Enter;
    MrcWriteCR (MrcData, DDRPHY_COMP_CR_VIEWANA_CBBSEL_REG, ViewAnaCbbSel.Data);

    if (VsshiRegion == VssHiRegionNorth) {
      VttIndex = (SensePoint == VssHiSensePointMid) ? 2 : 3;
    } else { // South
      VttIndex = (SensePoint == VssHiSensePointMid) ? 1 : 0;
    }
    Offset = DdrVttOffset (MrcData, DDRVTT0_CR_AFE_VIEWCTRL_REG, VttIndex);
    AfeViewCtrl.Data = MrcReadCR (MrcData, Offset);
    AfeViewCtrl.Bits.vtttop_cmn_viewanaen     = Enter;
    AfeViewCtrl.Bits.viewmxana_cmn_viewanaen  = Enter;
    AfeViewCtrl.Bits.viewmxana_cmn_viewanasel = 5;
    MrcWriteCR (MrcData, Offset, AfeViewCtrl.Data);
  }

  // Override vsshiffcomp vref to the target voltage
  CompOverride.Data = MrcReadCR (MrcData, DDRPHY_COMP_CR_COMPOVERRIDE_REG);
  CompOverride.Bits.vsshicompvrefovrdcode = VssHiVdd2TargRatio;     // Program target VssHi Voltage
  CompOverride.Bits.vsshicompvrefovrden   = Enter;                  // Enable VssHiFFComp vref generator
  CompOverride.Bits.vsshicompvrefen       = Enter;                  // Enable override of VssHiComp Vref Code
  CompOverride.Bits.vsshicompampen        = Enter;                  // Enable Comparator
  CompOverride.Bits.vsshicomprxrstb       = Enter;                  // Enable Switch Cap Comparator Clocking, set to continuously sample
  MrcWriteCR (MrcData, DDRPHY_COMP_CR_COMPOVERRIDE_REG, CompOverride.Data);
}

/**
  Enter or Exit VssHi Offset Cancellation mode

  @param[in, out] MrcData             - Include all MRC global data.
  @param[in]      VsshiRegion         - North / South, see VssHiRegionType
  @param[in]      VssHiVdd2TargRatio  - Target VssHi voltage
  @param[in]      SensePoint          - Near / Mid / Far, see VssHiSensePointType
  @param[in]      Enter               - Enter or Exit the mode: 1 - Enter, 0 - Exit

  @retval VssHiOpAmpVrefStart value
**/
INT32
MrcEnterVssHiOffsetCancel (
  IN OUT MrcParameters* const MrcData,
  IN VssHiRegionType          VsshiRegion,
  IN UINT32                   VssHiVdd2TargRatio,
  IN VssHiSensePointType      SensePoint,
  IN UINT32                   Enter
  )
{
  MrcDebug  *Debug;
  UINT32    OpAmpVref;
  DDRPHY_COMP_CR_VSXHIOPAMPCH0_CTRL_STRUCT  VsxhiOpAmpCh0;
  DDRPHY_COMP_CR_VSXHIOPAMPCH1_CTRL_STRUCT  VsxhiOpAmpCh1;

  Debug = &MrcData->Outputs.Debug;
  OpAmpVref = 0;

  // Read the starting vref value of the VssHi opamp
  if (VsshiRegion == VssHiRegionNorth) {
    VsxhiOpAmpCh1.Data = MrcReadCR (MrcData, DDRPHY_COMP_CR_VSXHIOPAMPCH1_CTRL_REG);
    OpAmpVref = VsxhiOpAmpCh1.Bits.vsxhiopamp_ch1_opamptargetvref;
  } else { // South
    VsxhiOpAmpCh0.Data = MrcReadCR (MrcData, DDRPHY_COMP_CR_VSXHIOPAMPCH0_CTRL_REG);
    OpAmpVref = VsxhiOpAmpCh0.Bits.vsxhiopamp_ch0_opamptargetvref;
  }

  MrcEnterVssHiTraining (MrcData, VsshiRegion, VssHiVdd2TargRatio, SensePoint, Enter);

  if (Enter) {
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "VssHiVdd2TargRatio=%u InitialOpAmpVref=%u\n", VssHiVdd2TargRatio, OpAmpVref);
  }

  return OpAmpVref;
}

/**
  Set VssHi OpAmpVref value

  @param[in] MrcData        - Include all MRC global data.
  @param[in] VsshiRegion    - North / South, see VssHiRegionType
  @param[in] VssHiOpAmpVref - VssHi OpAmpVref value
**/
VOID
SetVssHiGlobalTarget (
  IN OUT MrcParameters* const MrcData,
  IN VssHiRegionType          VsshiRegion,
  IN INT32                    VssHiOpAmpVref
  )
{
  DDRPHY_COMP_CR_VSXHIOPAMPCH0_CTRL_STRUCT  VsxhiOpAmpCh0;
  DDRPHY_COMP_CR_VSXHIOPAMPCH1_CTRL_STRUCT  VsxhiOpAmpCh1;

  if (VsshiRegion == VssHiRegionNorth) {
    VsxhiOpAmpCh1.Data = MrcReadCR (MrcData, DDRPHY_COMP_CR_VSXHIOPAMPCH1_CTRL_REG);
    VsxhiOpAmpCh1.Bits.vsxhiopamp_ch1_opamptargetvref = VssHiOpAmpVref;
    MrcWriteCR (MrcData, DDRPHY_COMP_CR_VSXHIOPAMPCH1_CTRL_REG, VsxhiOpAmpCh1.Data);
  } else { // South
    VsxhiOpAmpCh0.Data = MrcReadCR (MrcData, DDRPHY_COMP_CR_VSXHIOPAMPCH0_CTRL_REG);
    VsxhiOpAmpCh0.Bits.vsxhiopamp_ch0_opamptargetvref = VssHiOpAmpVref;
    MrcWriteCR (MrcData, DDRPHY_COMP_CR_VSXHIOPAMPCH0_CTRL_REG, VsxhiOpAmpCh0.Data);
  }
}

/**
  Set vsshi local leaker.

  @param[in]      MrcData        - Include all MRC global data.
  @param[in]      VsshiRegion    - North / South, see VssHiRegionType
  @param[in]      VsshiLeakCode  - VsshiLeakCode

  @retval None
**/
VOID
SetVssHiLocalLeaker (
  IN MrcParameters *const MrcData,
  IN VssHiRegionType      VsshiRegion,
  IN UINT32               VsshiLeakCode
  )
{
  UINT32                              Index;
  UINT32                              Offset;
  CH2CCC_CR_CTL0_STRUCT               CccCtl;
  DATA0CH0_CR_AFEMISCCTRL2_STRUCT     DataAfeMiscCtrl2;

  static const UINT32 Data[VSSHI_REGION_NUM][DATAFUB_NUM_IN_REGION] = {
    { // north
      DATA4CH0_CR_AFEMISCCTRL2_REG, DATA5CH0_CR_AFEMISCCTRL2_REG, DATA6CH0_CR_AFEMISCCTRL2_REG, DATA7CH0_CR_AFEMISCCTRL2_REG, DATA4CH0_ECC_CR_AFEMISCCTRL2_REG,
      DATA4CH1_CR_AFEMISCCTRL2_REG, DATA5CH1_CR_AFEMISCCTRL2_REG, DATA6CH1_CR_AFEMISCCTRL2_REG, DATA7CH1_CR_AFEMISCCTRL2_REG, DATA4CH1_ECC_CR_AFEMISCCTRL2_REG
    },
    { // south
      DATA0CH0_CR_AFEMISCCTRL2_REG, DATA2CH0_CR_AFEMISCCTRL2_REG, DATA1CH0_CR_AFEMISCCTRL2_REG, DATA3CH0_CR_AFEMISCCTRL2_REG, DATA3CH0_ECC_CR_AFEMISCCTRL2_REG,
      DATA0CH1_CR_AFEMISCCTRL2_REG, DATA2CH1_CR_AFEMISCCTRL2_REG, DATA1CH1_CR_AFEMISCCTRL2_REG, DATA3CH1_CR_AFEMISCCTRL2_REG, DATA3CH1_ECC_CR_AFEMISCCTRL2_REG
    }
  };

  static const UINT32 Ccc[VSSHI_REGION_NUM][CCCFUB_NUM_IN_REGION] = {
    { CH5CCC_CR_CTL0_REG, CH7CCC_CR_CTL0_REG, CH4CCC_CR_CTL0_REG, CH6CCC_CR_CTL0_REG }, // north
    { CH2CCC_CR_CTL0_REG, CH0CCC_CR_CTL0_REG, CH3CCC_CR_CTL0_REG, CH1CCC_CR_CTL0_REG }  // south
  };

  for (Index = 0; Index < DATAFUB_NUM_IN_REGION; Index++) {
    Offset = Data[VsshiRegion][Index];
    DataAfeMiscCtrl2.Data = MrcReadCR (MrcData, Offset);
    DataAfeMiscCtrl2.Bits.dxtx_cmn_txvsshileakercomp = VsshiLeakCode;
    DataAfeMiscCtrl2.Bits.dxtx_cmn_txvsshileakeren   = 1;
    MrcWriteCR (MrcData, Offset, DataAfeMiscCtrl2.Data);
  }

  for (Index = 0; Index < CCCFUB_NUM_IN_REGION; Index++) {
    Offset = Ccc[VsshiRegion][Index];
    CccCtl.Data = MrcReadCR (MrcData, Offset);
    CccCtl.Bits.ccctx_cmn_txvsshileakercomp = VsshiLeakCode;
    CccCtl.Bits.ccctx_cmn_txvsshileakeren   = 1;
    MrcWriteCR (MrcData, Offset, CccCtl.Data);
  }
}

/**
  Enter or Exit VssHi local leak mode

  @param[in]      MrcData             - Include all MRC global data.
  @param[in]      VsshiRegion         - North / South, see VssHiRegionType
  @param[in]      VssHiVdd2TargRatio  - Target VssHi voltage
  @param[in]      SensePoint          - Near / Mid / Far, see VssHiSensePointType
  @param[in]      Enter               - Enter or Exit the mode: 1 - Enter, 0 - Exit

  @retval None
**/
VOID
EnterVssHiLocalLeakCal (
  IN MrcParameters *const MrcData,
  IN VssHiRegionType      VsshiRegion,
  IN UINT32               VssHiVdd2TargRatio,
  IN VssHiSensePointType  SensePoint,
  IN BOOLEAN              Enter
  )
{
  DDRPHY_COMP_CR_DDRCRCOMPCTL3_STRUCT       CompCtl3;
  DDRPHY_COMP_CR_VSXHIOPAMPCH1_CTRL_STRUCT  VsxHiOpampCh1Ctrl;
  DDRPHY_COMP_CR_VSXHIOPAMPCH0_CTRL_STRUCT  VsxHiOpampCh0Ctrl;

  CompCtl3.Data = MrcReadCR (MrcData, DDRPHY_COMP_CR_DDRCRCOMPCTL3_REG);
  if (VsshiRegion == VssHiRegionNorth) {
    CompCtl3.BitsQ0.Spare = Enter ? ((CompCtl3.BitsQ0.Spare | MRC_BIT2) & ~MRC_BIT3) : (CompCtl3.BitsQ0.Spare & ~MRC_BIT2);
    VsxHiOpampCh1Ctrl.Data = MrcReadCR (MrcData, DDRPHY_COMP_CR_VSXHIOPAMPCH1_CTRL_REG);
    VsxHiOpampCh1Ctrl.Bits.ch1_opampen = !Enter;
    VsxHiOpampCh1Ctrl.Bits.vsxhiopamp_ch1_tristateopenloop = Enter;
    MrcWriteCR (MrcData, DDRPHY_COMP_CR_VSXHIOPAMPCH1_CTRL_REG, VsxHiOpampCh1Ctrl.Data);
  } else {
    CompCtl3.BitsQ0.Spare = Enter ? ((CompCtl3.BitsQ0.Spare | MRC_BIT0) & ~MRC_BIT1) : (CompCtl3.BitsQ0.Spare & ~MRC_BIT0);
    VsxHiOpampCh0Ctrl.Data = MrcReadCR (MrcData, DDRPHY_COMP_CR_VSXHIOPAMPCH0_CTRL_REG);
    VsxHiOpampCh0Ctrl.Bits.ch0_opampen = !Enter;
    VsxHiOpampCh0Ctrl.Bits.vsxhiopamp_ch0_tristateopenloop = Enter;
    MrcWriteCR (MrcData, DDRPHY_COMP_CR_VSXHIOPAMPCH0_CTRL_REG, VsxHiOpampCh0Ctrl.Data);
  }
  MrcWriteCR (MrcData, DDRPHY_COMP_CR_DDRCRCOMPCTL3_REG, CompCtl3.Data);

  MrcEnterVssHiTraining (MrcData, VsshiRegion, VssHiVdd2TargRatio, SensePoint, Enter);
}

#define VSSHI_LEAK_SEARCH_LIMIT_LO  0
#define VSSHI_LEAK_SEARCH_LIMIT_HI  63

/*
  Local VssHi Leaker calibration (buffer.vsshileakercal)

  @param[in] MrcData - Include all MRC global data.

  @retval mrcSuccess
*/
MrcStatus
MrcVssHiLocalLeakCal (
  IN MrcParameters *const MrcData
  )
{
  MrcOutput             *Outputs;
  MrcDebug              *Debug;
  MrcSaveData           *SaveOutputs;
  VssHiRegionType       VssHiRegion;
  VssHiSensePointType   SensePoint;
  UINT32                VssHiVdd2TargRatio;
  UINT32                VsshiLeakCode;
  DDRPHY_COMP_CR_COMP_STATUS_STRUCT         CompStatus;
  DDRPHY_COMP_CR_VSXHIOPAMPCH0_CTRL_STRUCT  VsxhiOpAmpCh0;

  Outputs     = &MrcData->Outputs;
  Debug       = &Outputs->Debug;
  SaveOutputs = &MrcData->Save.Data;

  if (!(MrcData->Inputs.Q0Regs) || SaveOutputs->GlobalVsshiBypass) {
    return mrcSuccess;
  }
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\nMrcVssHiLocalLeakCalibration\n");

  VsxhiOpAmpCh0.Data = MrcReadCR (MrcData, DDRPHY_COMP_CR_VSXHIOPAMPCH0_CTRL_REG);
  VssHiVdd2TargRatio = VsxhiOpAmpCh0.Bits.vsxhiopamp_ch0_opamptargetvref;

  SensePoint = VssHiSensePointNear;
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "SensePoint=%s\n", (SensePoint == VssHiSensePointNear) ? "Near" : ((SensePoint == VssHiSensePointMid) ? "Mid" : "Far"));

  for (VssHiRegion = VssHiRegionNorth; VssHiRegion <= VssHiRegionSouth; VssHiRegion++) {
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "VssHiRegion=%s\n", (VssHiRegion == VssHiRegionNorth) ? "North" : "South");
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "VsshiLeakCode Result\n");

    EnterVssHiLocalLeakCal (MrcData, VssHiRegion, VssHiVdd2TargRatio, SensePoint, TRUE);
    for (VsshiLeakCode = VSSHI_LEAK_SEARCH_LIMIT_LO; VsshiLeakCode <= VSSHI_LEAK_SEARCH_LIMIT_HI; VsshiLeakCode++) {
      SetVssHiLocalLeaker (MrcData, VssHiRegion, VsshiLeakCode);
      CompStatus.Data = MrcReadCR (MrcData, DDRPHY_COMP_CR_COMP_STATUS_REG); // Get VssHi Comparator Sample
      MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\t%2d\t%d\n", VsshiLeakCode, CompStatus.Bits.vsshicomp_cmn_compvsxhiffupdn);
      if (CompStatus.Bits.vsshicomp_cmn_compvsxhiffupdn == 0) {
        MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, " --> Final VsshiLeakCode=%d\n", VsshiLeakCode);
        break;
      }
    }
    EnterVssHiLocalLeakCal (MrcData, VssHiRegion, VssHiVdd2TargRatio, SensePoint, FALSE);
  } // VssHiRegion

  return mrcSuccess;
}

#define VSSHI_OFFSET_SEARCH_LIMIT 16

/**
  VssHi Offset Cancellation (vsshidrv.offsetcal)
  Cancels out the opamp error for the VssHi LVR. Independent offset cancellation per region (north/south).

  @param[in, out] MrcData - Include all MRC global data.

  @retval mrcSuccess
**/
MrcStatus
MrcVsshiOffsetCancellation (
  IN OUT MrcParameters* const MrcData
  )
{
  MrcDebug            *Debug;
  MrcStatus           Status;
  VssHiRegionType     VsshiRegion;
  VssHiSensePointType SensePoint;
  UINT32              VssHiVdd2TargRatio;
  UINT32              InitialSampleDirection;
  UINT32              Result;
  INT32               VssHiOpAmpVrefStart;
  INT32               VssHiOpAmpVref;
  INT32               VssHiSearchOffsetLimitHi;
  INT32               VssHiSearchOffsetLimitLo;
  BOOLEAN             InitialSample;
  BOOLEAN             DitherDetected;
  BOOLEAN             SearchLimitReached;
  DDRPHY_COMP_CR_COMP_STATUS_STRUCT         CompStatus;
  DDRPHY_COMP_CR_VSXHIOPAMPCH0_CTRL_STRUCT  VsxhiOpAmpCh0;

  Status = mrcSuccess;
  Debug  = &MrcData->Outputs.Debug;

  if (MrcData->Save.Data.GlobalVsshiBypass) {
    return Status;
  }
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\nMrcVsshiOffsetCancellation\n");

  VsxhiOpAmpCh0.Data = MrcReadCR (MrcData, DDRPHY_COMP_CR_VSXHIOPAMPCH0_CTRL_REG);
  VssHiVdd2TargRatio = VsxhiOpAmpCh0.Bits.vsxhiopamp_ch0_opamptargetvref;

  SensePoint = VssHiSensePointNear;
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "SensePoint=%s\n", (SensePoint == VssHiSensePointNear) ? "Near" : ((SensePoint == VssHiSensePointMid) ? "Mid" : "Far"));

  for (VsshiRegion = VssHiRegionNorth; VsshiRegion <= VssHiRegionSouth; VsshiRegion++) {
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "VsshiRegion=%s\n", (VsshiRegion == VssHiRegionNorth) ? "North" : "South");

    VssHiOpAmpVrefStart = MrcEnterVssHiOffsetCancel (MrcData, VsshiRegion, VssHiVdd2TargRatio, SensePoint, 1);

    // ensure search offset limit doesn't saturate
    VssHiSearchOffsetLimitHi = MIN (255, VssHiOpAmpVrefStart + VSSHI_OFFSET_SEARCH_LIMIT);
    VssHiSearchOffsetLimitLo = MAX (0,   VssHiOpAmpVrefStart - VSSHI_OFFSET_SEARCH_LIMIT);

    // Linear Search for Correct Offset
    InitialSample           = FALSE;
    InitialSampleDirection  = 0;
    VssHiOpAmpVref          = VssHiOpAmpVrefStart;
    DitherDetected          = FALSE;
    SearchLimitReached      = FALSE;

    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "OpAmpVref\tResult\n");
    while (!DitherDetected && !SearchLimitReached) {
      CompStatus.Data = MrcReadCR (MrcData, DDRPHY_COMP_CR_COMP_STATUS_REG); // Get VssHi Comparator Sample
      Result = CompStatus.Bits.vsshicomp_cmn_compvsxhiffupdn;
      MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, " %d\t\t%u\n", VssHiOpAmpVref, Result);

      if (!InitialSample) {                       // Set Dither Reference
        InitialSampleDirection = Result;
        InitialSample = TRUE;
      } else {                                    // Look for Dither
        if (Result != InitialSampleDirection) {
          DitherDetected = TRUE;
        } else {
          if (InitialSampleDirection == 0) {
            if (VssHiOpAmpVref < VssHiSearchOffsetLimitHi) {  // check whether limit is reached
              VssHiOpAmpVref++;                               // increment vsshi opamp reference
            } else {
              SearchLimitReached = TRUE;
            }
          } else if (InitialSampleDirection == 1) {
            if (VssHiOpAmpVref > VssHiSearchOffsetLimitLo) { // check whether limit is reached
              VssHiOpAmpVref--;                              // decrement vsshi opamp reference
            } else {
              SearchLimitReached = TRUE;
            }
          }
          if (!SearchLimitReached) {
            SetVssHiGlobalTarget (MrcData, VsshiRegion, VssHiOpAmpVref); // program new offset value
          }
        }
      } // InitialSample
    } // while

    // For consistency, always dither in one direction, error on the low side for vsshi
    if (!SearchLimitReached && DitherDetected) {
      if ((InitialSampleDirection == 0) && (VssHiOpAmpVref > 0)) {
        VssHiOpAmpVref--;
        SetVssHiGlobalTarget (MrcData, VsshiRegion, VssHiOpAmpVref);
      }
    }
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, " --> Final OpAmpVref=%d\n", VssHiOpAmpVref);
    MrcEnterVssHiOffsetCancel (MrcData, VsshiRegion, VssHiVdd2TargRatio, SensePoint, 0);  // Exit the mode
  } // VsshiRegion

  return Status;
}

/**
  Run VsshiFFCompLeak FSM and return leakvddq comp code

  @param[in, out] MrcData         - Include all MRC global data.
  @param[out]     VsshiFfLeakVddq - Return comp code

  @retval mrcSuccess
**/
MrcStatus
MrcRunVsshiFfCompLeakStage (
  IN OUT MrcParameters *const MrcData,
  OUT    UINT32               *VsshiFfLeakVddq
  )
{
  MrcStatus Status;
  DDRPHY_COMP_CR_MISCCOMPCODES_STRUCT MiscCompCodes;

  Status = MrcInitCompRun (MrcData);

  MiscCompCodes.Data = MrcReadCR (MrcData, DDRPHY_COMP_CR_MISCCOMPCODES_REG);
  *VsshiFfLeakVddq = MiscCompCodes.Bits.Vsshiff_leakvddq;

  return Status;
}

/**
  Configuration for VssHiFFComp Leaker Ring Oscillator calibration

  @param[in, out] MrcData     - Include all MRC global data.
  @param[in]      RoDivSel    - ring oscillator divide select
  @param[in]      RoFreqAdj   - RO fine frequency adjust
  @param[in]      StaticLegEn - VssHiFFComp Static Leg enable
**/
void
MrcSetVsshiLeakerRingOsc (
  IN OUT MrcParameters *const  MrcData,
  IN     UINT32                RoDivSel,
  IN     UINT32                RoFreqAdj,
  IN     UINT32                StaticLegEn
  )
{
  DDRPHY_COMP_CR_VSSHICOMP_CTRL0_STRUCT   VsshiCompCtrl0;
  DDRPHY_COMP_CR_VSSHICOMP_CTRL1_STRUCT   VsshiCompCtrl1;

  VsshiCompCtrl0.Data = MrcReadCR (MrcData, DDRPHY_COMP_CR_VSSHICOMP_CTRL0_REG);
  VsshiCompCtrl0.Bits.vsshicomp_cmn_ffleakrodivsel  = RoDivSel;
  VsshiCompCtrl0.Bits.vsshiffleakstlegen            = StaticLegEn;
  MrcWriteCR (MrcData, DDRPHY_COMP_CR_VSSHICOMP_CTRL0_REG, VsshiCompCtrl0.Data);

  VsshiCompCtrl1.Data = MrcReadCR (MrcData, DDRPHY_COMP_CR_VSSHICOMP_CTRL1_REG);
  VsshiCompCtrl1.Bits.vsshicomp_cmn_ffleakrofreqadj = RoFreqAdj;
  MrcWriteCR (MrcData, DDRPHY_COMP_CR_VSSHICOMP_CTRL1_REG, VsshiCompCtrl1.Data);
}

#define VSSHIFFCODE_THRESH_LO   8
#define VSSHIFFCODE_THRESH_HI   16
#define RODIVSEL_LIMIT_HI       3   // 00 - div8, 01 - div4, 10 - div2, 11 - no div
#define RODIVSEL_LIMIT_LO       0
#define ROFREQADJ_LIMIT_HI      10  // indicates number of additional delay elements to add in the ring oscillator path, 0 - fastest (most current), 10 - slowest (least current)
#define ROFREQADJ_LIMIT_LO      0

/**
  VssHiFFComp Leaker Ring Oscillator calibration (vsshicomp.leakerrocal)

  @param[in, out] MrcData - Include all MRC global data.

  @retval mrcSuccess
**/
MrcStatus
MrcVsshiCompLeakerRoCalibration (
  IN OUT MrcParameters* const MrcData
  )
{
  MrcDebug     *Debug;
  MrcStatus    Status;
  MrcSaveData  *SaveOutputs;
  UINT32       RoFreqAdj;
  UINT32       StaticLegEn;
  UINT32       RoDivSel;
  UINT32       VsshiFfLeakVddq;
  BOOLEAN      Found;
  DDRPHY_COMP_CR_FSMSKIPCTRL_STRUCT FsmSkipCtrl;
  DDRPHY_COMP_CR_FSMSKIPCTRL_STRUCT FsmSkipCtrlSave;

  Status = mrcSuccess;
  Debug  = &MrcData->Outputs.Debug;
  SaveOutputs = &MrcData->Save.Data;

  if (SaveOutputs->GlobalVsshiBypass || SaveOutputs->LocalVsshiBypass) {
    return Status;
  }
  // Enable only VssHiFF Leak FSM Stage
  FsmSkipCtrlSave.Data = MrcReadCR (MrcData, DDRPHY_COMP_CR_FSMSKIPCTRL_REG);
  FsmSkipCtrl.Data = 0;
  FsmSkipCtrl.Bits.En_vsshiffleakvddq = 1;
  MrcWriteCR (MrcData, DDRPHY_COMP_CR_FSMSKIPCTRL_REG, FsmSkipCtrl.Data);

  RoFreqAdj = ROFREQADJ_LIMIT_HI; // start with smallest fine current

  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\nVsshiCompLeakerRoCalibration\n");
  Found  = FALSE;
  // Coarse adjustment from lowest current to highest current until upper code threshold is met
  for (StaticLegEn = 0; StaticLegEn <= 1; StaticLegEn++) { // start with static leg disabled for smallest vsshiff current
    for (RoDivSel = RODIVSEL_LIMIT_LO; RoDivSel <= RODIVSEL_LIMIT_HI; RoDivSel++) { // start with lowest rodivsel for lowest frequency clock for lowest current
      MrcSetVsshiLeakerRingOsc (MrcData, RoDivSel, RoFreqAdj, StaticLegEn);
      MrcRunVsshiFfCompLeakStage (MrcData, &VsshiFfLeakVddq);
      MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "StaticLegEn=%d RoDivSel=%d RoFreqAdj=%d --> VsshiFfLeakVddq=%d\n", StaticLegEn, RoDivSel, RoFreqAdj, VsshiFfLeakVddq);
      if (VsshiFfLeakVddq <= VSSHIFFCODE_THRESH_HI) {
        Found = TRUE;
        break; // RoDivSel found
      }
    }
    if (Found) {
      break; // StaticLegEn found
    }
  }
  if (!Found) {
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_WARNING, "%s cannot find a passing comp code in %s sweep!\n", gWarnString, "coarse");
    RoDivSel = RODIVSEL_LIMIT_HI;
    StaticLegEn = 1;
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_WARNING, "%s Override RoDivSel=3 & StaticLegEn=1 in %s sweep!\n", gWarnString, "coarse");
  }

  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Fine sweep\n");
  Found = FALSE;
  // Fine adjustment from highest current to lowest current until lower code threshold is met
  for (RoFreqAdj = ROFREQADJ_LIMIT_HI + 1; RoFreqAdj > ROFREQADJ_LIMIT_LO; RoFreqAdj--) {
    MrcSetVsshiLeakerRingOsc (MrcData, RoDivSel, RoFreqAdj - 1, StaticLegEn);
    Status = MrcRunVsshiFfCompLeakStage (MrcData, &VsshiFfLeakVddq);
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "StaticLegEn=%d RoDivSel=%d RoFreqAdj=%d --> VsshiFfLeakVddq=%d\n", StaticLegEn, RoDivSel, RoFreqAdj - 1, VsshiFfLeakVddq);
    if (VsshiFfLeakVddq >= VSSHIFFCODE_THRESH_LO) {
      Found = TRUE;
      break;
    }
  }
  if (!Found) {
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_WARNING, "%s cannot find a passing comp code in %s sweep!\n", gWarnString, "fine");
  }

  MrcWriteCR (MrcData, DDRPHY_COMP_CR_FSMSKIPCTRL_REG, FsmSkipCtrlSave.Data);
  return Status;
}

/**
  Update relevant registers with the calibrated lockui value

  @param[in, out] MrcData    - Include all MRC global data.
  @param[in]      CompOffset - LockUI COMP offset
**/
VOID
MrcUpdateLockUi (
  IN OUT MrcParameters *const MrcData,
  IN UINT8                    CompOffset
  )
{
  MrcOutput     *Outputs;
  MrcSaveData   *SaveOutputs;
  UINT32        UiLock;
  UINT32        Controller;
  UINT32        Channel;
  UINT32        Strobe;
  UINT32        TransChannel;
  UINT32        TransStrobe;
  UINT32        Offset;
  UINT32        Rank;
  DDRPHY_COMP_NEW_CR_DDRCRDATACOMP1_STRUCT        CompDataComp1;
  DDRPHY_COMP_NEW_CR_VCCDLLCOMPDATACCC_STRUCT     VccDllCompDataCcc;
  DATA0CH0_CR_DDRCRDATACOMP1_STRUCT               DdrDataComp1;
  DATA0CH0_CR_VCCDLLCOMPDATA_STRUCT               VccDllCompData;
  DATA0CH0_CR_SRZCTL_STRUCT                       DataSrzCtl;

  Outputs     = &MrcData->Outputs;
  SaveOutputs = &MrcData->Save.Data;
  UiLock      = SaveOutputs->UiLock;
  CompDataComp1.Data = MrcReadCR (MrcData, DDRPHY_COMP_NEW_CR_DDRCRDATACOMP1_REG);
  CompDataComp1.Bits.Dqsntargetnui  = UiLock;
  CompDataComp1.Bits.Dqsnoffsetnui  = CompOffset;
  MrcWriteCR (MrcData, DDRPHY_COMP_NEW_CR_DDRCRDATACOMP1_REG, CompDataComp1.Data);

  VccDllCompDataCcc.Data = MrcReadCR (MrcData, DDRPHY_COMP_NEW_CR_VCCDLLCOMPDATACCC_REG);
  VccDllCompDataCcc.Bits.Dqsptargetnui  = UiLock;
  VccDllCompDataCcc.Bits.Dqspoffsetnui  = CompOffset;
  MrcWriteCR (MrcData, DDRPHY_COMP_NEW_CR_VCCDLLCOMPDATACCC_REG, VccDllCompDataCcc.Data);

  for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
    for (Channel = 0; Channel < Outputs->MaxChannels; Channel++) {
      if (MrcChannelExist (MrcData, Controller, Channel)) {
        for (Strobe = 0; Strobe < Outputs->SdramCount; Strobe++) {
          TransChannel = Channel;
          TransStrobe  = Strobe;
          MrcTranslateSystemToIp (MrcData, &Controller, &TransChannel, &Rank, &TransStrobe, GsmIocTxOn); // No GetSet for this register, so use another per-byte PHY CR
          Offset = DATA0CH0_CR_DDRCRDATACOMP1_REG +
                    (DATA0CH1_CR_DDRCRDATACOMP1_REG - DATA0CH0_CR_DDRCRDATACOMP1_REG) * TransChannel +
                    (DATA1CH0_CR_DDRCRDATACOMP1_REG - DATA0CH0_CR_DDRCRDATACOMP1_REG) * TransStrobe;
          DdrDataComp1.Data = MrcReadCR (MrcData, Offset);
          DdrDataComp1.Bits.Dqsntargetnui = UiLock;
          DdrDataComp1.Bits.Dqsnoffsetnui = CompOffset;
          MrcWriteCR (MrcData, Offset, DdrDataComp1.Data);

          Offset = DATA0CH0_CR_VCCDLLCOMPDATA_REG +
                    (DATA0CH1_CR_VCCDLLCOMPDATA_REG - DATA0CH0_CR_VCCDLLCOMPDATA_REG) * TransChannel +
                    (DATA1CH0_CR_VCCDLLCOMPDATA_REG - DATA0CH0_CR_VCCDLLCOMPDATA_REG) * TransStrobe;
          VccDllCompData.Data = MrcReadCR (MrcData, Offset);
          VccDllCompData.Bits.Targetnui = UiLock;
          VccDllCompData.Bits.Offsetnui = CompOffset;
          MrcWriteCR (MrcData, Offset, VccDllCompData.Data);

          // Set DataSrzCtl.Bits.cktop_cmn_bonus[7] (Block SDL With Rcven) : 0 for 1.5 UiLock, otherwise 1
          Offset = DATA0CH0_CR_SRZCTL_REG +
                  (DATA0CH1_CR_SRZCTL_REG - DATA0CH0_CR_SRZCTL_REG) * TransChannel +
                  (DATA1CH0_CR_SRZCTL_REG - DATA0CH0_CR_SRZCTL_REG) * TransStrobe;
          DataSrzCtl.Data = MrcReadCR (MrcData, Offset);
          DataSrzCtl.Bits.cktop_cmn_bonus &= 0x7F;  // Clear bit [7]
          if ((UiLock - CompOffset) > 0) { // 2.5 or more
            DataSrzCtl.Bits.cktop_cmn_bonus |= MRC_BIT7;
          }
          MrcWriteCR (MrcData, Offset, DataSrzCtl.Data);
        }
      }
    }
  }

  MrcPhyInitStaticDqEarly (MrcData, TRUE);  // Only update registers that depend on lockui

}

/**
  Program a given Lock UI value

  @param[in, out] MrcData    - Include all MRC global data.
  @param[in]      LockUi     - Value to set
  @param[in]      RxPiOffset - additional PI offset

  @retval mrcSuccess
**/
MrcStatus
MrcSetCompLockUI (
  IN OUT MrcParameters *const MrcData,
  IN     UINT32               LockUi,
  IN     INT32                RxPiOffset
  )
{
  MrcStatus Status;
  MrcOutput *Outputs;
  MrcDebug  *Debug;
  UINT32    RefPathDelayPi;
  UINT32    PiCodeRxDqsRef;
  UINT32    MaxLockUi;
  UINT32    LockUiCr;
  UINT32    Index;
  static const UINT8  RxDqsCompXoverMap[4] = { 0, 1, 1, 2 };
  DDRPHY_COMP_CR_RXDQSCOMP_RXCODE_STRUCT      RxDqsCompRxCode;
  DDRPHY_COMP_CR_DLLCOMP_PIDELAY_STRUCT       DllCompPiDelay;
  DDRPHY_COMP_CR_RXDQSCOMP_CMN_CTRL0_STRUCT   RxDqsCompCmnCtrl0;

  Status         = mrcSuccess;
  Outputs        = &MrcData->Outputs;
  Debug          = &Outputs->Debug;
  RefPathDelayPi = 0;
  PiCodeRxDqsRef = 0;
  LockUiCr       = 0;
  Index          = 0;
  RefPathDelayPi = (LockUi * 256 / (Outputs->HighGear)) - RxPiOffset;

  if (RefPathDelayPi == 0) {
    Status = mrcFail;
  } else if (RefPathDelayPi >= 1023) {
    Status = mrcFail;
  } else {
    Status = mrcSuccess;
    LockUiCr       = RefPathDelayPi / 256;
    PiCodeRxDqsRef = RefPathDelayPi % 256;
  }

  MaxLockUi = (Outputs->Gear4) ? 15 : (Outputs->Gear2) ? 7 : 3;

  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\t%d\t%d", LockUiCr, PiCodeRxDqsRef);
  if (LockUi > MaxLockUi) {
    MRC_DEBUG_ASSERT (FALSE, Debug, "\n%s LockUi out of range!\n", gErrString);
    Status = mrcWrongInputParameter;
  } else if (Status != mrcFail) {
    RxDqsCompRxCode.Data = MrcReadCR (MrcData, DDRPHY_COMP_CR_RXDQSCOMP_RXCODE_REG);
    RxDqsCompRxCode.Bits.dllcomp_cmn_picoderxdqsnref = PiCodeRxDqsRef;
    MrcWriteCR (MrcData, DDRPHY_COMP_CR_RXDQSCOMP_RXCODE_REG, RxDqsCompRxCode.Data);

    DllCompPiDelay.Data = MrcReadCR (MrcData, DDRPHY_COMP_CR_DLLCOMP_PIDELAY_REG);
    DllCompPiDelay.Bits.dllcomp_cmn_picoderxdqspref = PiCodeRxDqsRef;
    MrcWriteCR (MrcData, DDRPHY_COMP_CR_DLLCOMP_PIDELAY_REG, DllCompPiDelay.Data);

    RxDqsCompCmnCtrl0.Data = MrcReadCR (MrcData, DDRPHY_COMP_CR_RXDQSCOMP_CMN_CTRL0_REG);
    RxDqsCompCmnCtrl0.Bits.rxdqscomp_cmn_dqspcomplockui = LockUiCr;
    RxDqsCompCmnCtrl0.Bits.rxdqscomp_cmn_dqsncomplockui = LockUiCr;
    RxDqsCompCmnCtrl0.Bits.rxdqscomp_cmn_dqscompxoverovrsel = 1;
    Index = ((PiCodeRxDqsRef) >> 6) & 0x3;
    RxDqsCompCmnCtrl0.Bits.rxdqscomp_cmn_dqscompxoverovrval = RxDqsCompXoverMap[Index];
    MrcWriteCR (MrcData, DDRPHY_COMP_CR_RXDQSCOMP_CMN_CTRL0_REG, RxDqsCompCmnCtrl0.Data);
  }

  return Status;
}

/**
  Run RxDQSComp for LockUI and return P/N comp codes

  @param[in, out] MrcData   - Include all MRC global data.
  @param[out]     CodeFwdP  - Return value
  @param[out]     CodeFwdN  - Return value

  @retval mrcSuccess
**/
MrcStatus
MrcRunRxDqsComp (
  IN OUT MrcParameters *const MrcData,
  OUT    UINT32               *CodeFwdP,
  OUT    UINT32               *CodeFwdN
  )
{
  MrcStatus Status;
  DDRPHY_COMP_CR_FSMSKIPCTRL_STRUCT         FsmSkipCtrl;
  DDRPHY_COMP_CR_FSMSKIPCTRL_STRUCT         FsmSkipCtrlSave;
  DDRPHY_COMP_NEW_CR_DDRCRDATACOMP1_STRUCT  CompDataComp1;

  Status = mrcSuccess;

  FsmSkipCtrlSave.Data = MrcReadCR (MrcData, DDRPHY_COMP_CR_FSMSKIPCTRL_REG);

  // Enable only RxDQSComp FSM Stages
  FsmSkipCtrl.Data = 0;
  FsmSkipCtrl.Bits.En_dqsfwdclkp = 1;
  FsmSkipCtrl.Bits.En_dqsfwdclkn = 1;
  MrcWriteCR (MrcData, DDRPHY_COMP_CR_FSMSKIPCTRL_REG, FsmSkipCtrl.Data);

  Status = MrcInitCompRun (MrcData);

  CompDataComp1.Data = MrcReadCR (MrcData, DDRPHY_COMP_NEW_CR_DDRCRDATACOMP1_REG);
  *CodeFwdP = CompDataComp1.Bits.Dqs_fwdclkp;
  *CodeFwdN = CompDataComp1.Bits.Dqs_fwdclkn;

  // Restore FSMSKIPCTRL
  MrcWriteCR (MrcData, DDRPHY_COMP_CR_FSMSKIPCTRL_REG, FsmSkipCtrlSave.Data);

  return Status;
}

/**
  Lock UI calibration for Unmatched Rx
  Uses RxDQSComp FSM

  @param[in, out] MrcData - Include all MRC global data.

  @retval mrcSuccess
**/
MrcStatus
MrcLockUiCalibration (
  IN OUT MrcParameters *const MrcData
  )
{
  MrcStatus     Status;
  MrcOutput     *Outputs;
  MrcInput      *Inputs;
  MrcDebug      *Debug;
  MrcSaveData   *SaveOutputs;

  INT32         PiCodeMin;
  INT32         DriftLimitGlobal;
  UINT32        DriftLimitLocal;
  UINT32        LockUiRange;
  UINT32        LockUiCount;
  UINT32        LockUi;
  UINT32        CodeFwdP;
  UINT32        CodeFwdN;
  INT32         FwdClkOffsetPs;
  INT32         CodeLimit;
  BOOLEAN       Ddr5;
  UINT8         CompOffset;
  INT32         RxPiOffset;
  UINT16        Qclkps;

  Outputs     = &MrcData->Outputs;
  Inputs      = &MrcData->Inputs;
  SaveOutputs = &MrcData->Save.Data;
  Debug       = &Outputs->Debug;
  Ddr5        = (Outputs->DdrType == MRC_DDR_TYPE_DDR5);
  Status      = mrcSuccess;
  LockUi      = MRC_UINT32_MAX;
  Qclkps      = Outputs->Qclkps;
  DriftLimitLocal   = 10;
  DriftLimitGlobal  = 10;
  PiCodeMin         = 10;
  CompOffset        = 0;
  CodeLimit         = 64;

  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\n%s\n", "LockUiCalibration");

  if ((Debug->PostCode[MRC_POST_CODE] == MRC_DDRIO_INIT)) {
    Inputs->InitialFwdClkOffsetPs = 120; //80;
  }

  // Determine how much additional pushout to add to the FwdClk (DQS path) for RxDQSComp
  if (MrcData->Inputs.A0) {
    FwdClkOffsetPs = SaveOutputs->GlobalVsshiBypass ? 30 : 70;
  } else {
    FwdClkOffsetPs = Inputs->InitialFwdClkOffsetPs;
  }

  FwdClkOffsetPs += Outputs->FwdClkOffsetTrained;

  // Gear1 range: 0..3
  // Gear2 range: 0..7
  // Gear4 supports lockui comp range of 0-15, but limited to 8 by the functional data path
  LockUiRange = (Outputs->Gear4) ? 9 : (Outputs->Gear2 ? 8 : 4);
  RxPiOffset = DIVIDEROUND ((FwdClkOffsetPs * 256), Qclkps);

  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Sweep LockUiCount from 1 to %d, FwdClkOffsetPs: %d\n\n", LockUiRange - 1, FwdClkOffsetPs);
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "LockUiCount LockUiCr PiCodeRxDqsRef CodeFwdP/N\n");
  for (LockUiCount = 1; LockUiCount < LockUiRange; ++LockUiCount) {
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\t%d", LockUiCount);
    Status = MrcSetCompLockUI (MrcData, LockUiCount, RxPiOffset);
    if (Status != mrcSuccess) {
      return Status;
    }
    Status = MrcRunRxDqsComp (MrcData, &CodeFwdP, &CodeFwdN);
    if (Status != mrcSuccess) {
      return Status;
    }
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\t%d/%d\n", CodeFwdP, CodeFwdN);
    if ((CodeFwdP > DriftLimitLocal) && (CodeFwdN > DriftLimitLocal)) {
      if (((INT32) CodeFwdP - CodeLimit > DriftLimitGlobal - PiCodeMin) && ((INT32) CodeFwdN - CodeLimit > DriftLimitGlobal - PiCodeMin)) {
        CompOffset = 1;
      } else {
        CompOffset = 0;
      }
      LockUi = LockUiCount;
      break;
    }
  }
  if (LockUi == MRC_UINT32_MAX) { // saturation condition, no lock UI found -- fwd DQS clock delay is too large
    MRC_DEBUG_ASSERT (FALSE, Debug, "%s %s - no LockUi found!\n", gErrString, "LockUiCalibration");
    Status = mrcFail;
  } else if (LockUi == 0) {
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_WARNING, "%s %s - LockUi is zero, Unmatched Rx is not possible, switching to Matched\n", gWarnString, "LockUiCalibration");
    Status = mrcUnmatchedRxNotPossible;
  } else {
    SaveOutputs->UiLock = (UINT8) LockUi - 1;
    SaveOutputs->CompOffset = CompOffset;

    //---- UiLock  RdDqsOffset ------
    // 1.5  0       0
    // 2.5  1       0
    // 3.5  2       0
    // 4.5  3       1
    // 5.5  4       1
    // 6.5  5       2
    // 7.5  6       2
    // 8.5  7       3

    if (Ddr5) {
      SaveOutputs->ReadDqsOffsetDdr5 = ((INT8) SaveOutputs->UiLock - (INT8) CompOffset > 1) ? ((SaveOutputs->UiLock - CompOffset - 1) / 2) : 0;
    } else {
      SaveOutputs->ReadDqsOffsetDdr5 = 0;
    }
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "--> UiLock = %d, CompOffset = %d, ReadDqsOffsetDdr5 = %d\n", SaveOutputs->UiLock, CompOffset, SaveOutputs->ReadDqsOffsetDdr5);

    // Update relevant registers with the calibrated lockui value
    MrcUpdateLockUi (MrcData, CompOffset);
  }

  return Status;
}

/**
  Enable / Disable MDLL per partition

  @param[in, out] MrcData     - Include all MRC global data.
  @param[in]      Enable      - Enable or disable the DLL
  @param[in]      FubMask     - Enable DLL in this FUB (DATA/CCC/COMP)
  @param[in]      WaitForLock - whether to wait for MDLLs to lock or proceed. Only applicable if Enable is True

  @retval mrcSuccess
**/
VOID
MrcOvrMdllEn (
  IN OUT MrcParameters* const MrcData,
  IN BOOLEAN                  Enable,
  IN DccFubMask               FubMask,
  IN BOOLEAN                  WaitForLock
  )
{
  MrcOutput *Outputs;
  MrcDebug  *Debug;
  UINT32    DllEnable;
  UINT32    Controller;
  UINT32    Channel;
  UINT32    Strobe;
  UINT32    TransStrobe;
  UINT32    TransChannel;
  UINT32    Rank;
  UINT32    Offset;
  DDRPHY_COMP_CR_COMPDLLSTRTUP_STRUCT CompDllStartup;
  CCC0_GLOBAL_CR_PM_FSM_OVRD_0_STRUCT CccGlobalPmFsmOvrd0;
  CCC0_GLOBAL_CR_PM_FSM_OVRD_1_STRUCT CccGlobalPmFsmOvrd1;
  DATA0CH0_CR_PMFSM1_STRUCT           DataPmFsm1;
  DDRPHY_COMP_CR_COMP_STATUS_STRUCT   CompStatus;
  CH0CCC_CR_MDLLCTL0_STRUCT           CccMdllCtl0;
  DATA0CH0_CR_MDLLCTL0_STRUCT         DataMdllCtl0;
  BOOLEAN                             Lpddr5;

  Outputs = &MrcData->Outputs;
  Lpddr5  = (Outputs->DdrType == MRC_DDR_TYPE_LPDDR5);
  if (Lpddr5) {
    FubMask = DccFubAll;
  }
  Debug   = &Outputs->Debug;
  DllEnable = Enable ? 1 : 0;

  if (FubMask & DccFubComp) {
    CompDllStartup.Data = MrcReadCR (MrcData, DDRPHY_COMP_CR_COMPDLLSTRTUP_REG);
    CompDllStartup.Bits.dll_start = DllEnable;
    MrcWriteCR (MrcData, DDRPHY_COMP_CR_COMPDLLSTRTUP_REG, CompDllStartup.Data);
  }

  if (FubMask & DccFubData) {
    // Read from the first instance, assume these CR's are the same across all FUBs
    DataPmFsm1.Data = MrcReadCR (MrcData, DATA0CH0_CR_PMFSM1_REG);
    DataPmFsm1.Bits.dllen_ovren   = DllEnable;
    DataPmFsm1.Bits.ldoen_ovrval  = DllEnable;
    DataPmFsm1.Bits.dllen_ovrval  = 0;
    // Program all FUBs using multicast
    MrcWriteCrMulticast (MrcData, DATA_CR_PMFSM1_REG, DataPmFsm1.Data);
  }

  if (FubMask & DccFubCcc) {
    CccGlobalPmFsmOvrd0.Data = MrcReadCR (MrcData, CCC0_GLOBAL_CR_PM_FSM_OVRD_0_REG);
    CccGlobalPmFsmOvrd1.Data = MrcReadCR (MrcData, CCC0_GLOBAL_CR_PM_FSM_OVRD_1_REG);

    CccGlobalPmFsmOvrd0.Bits.dllen_ovren  = DllEnable;
    CccGlobalPmFsmOvrd1.Bits.dllen_ovren  = DllEnable;

    CccGlobalPmFsmOvrd0.Bits.ldoen_ovrval = DllEnable;
    CccGlobalPmFsmOvrd1.Bits.ldoen_ovrval = DllEnable;

    CccGlobalPmFsmOvrd0.Bits.dllen_ovrval = 0;
    CccGlobalPmFsmOvrd1.Bits.dllen_ovrval = 0;

    MrcWriteCrMulticast (MrcData, CCC_GLOBAL_CR_PM_FSM_OVRD_0_REG, CccGlobalPmFsmOvrd0.Data);
    MrcWriteCrMulticast (MrcData, CCC_GLOBAL_CR_PM_FSM_OVRD_1_REG, CccGlobalPmFsmOvrd1.Data);
  }

  if (Enable) {
    MrcWait (MrcData, 50 * MRC_TIMER_1NS);

    if (FubMask & DccFubData) {
      DataPmFsm1.Bits.dllen_ovrval  = 1;   // assert mdllen
      MrcWriteCrMulticast (MrcData, DATA_CR_PMFSM1_REG, DataPmFsm1.Data);
    }
    if (FubMask & DccFubCcc) {
      CccGlobalPmFsmOvrd0.Bits.dllen_ovrval = 1;  // assert mdllen
      CccGlobalPmFsmOvrd1.Bits.dllen_ovrval = 1;
      MrcWriteCrMulticast (MrcData, CCC_GLOBAL_CR_PM_FSM_OVRD_0_REG, CccGlobalPmFsmOvrd0.Data);
      MrcWriteCrMulticast (MrcData, CCC_GLOBAL_CR_PM_FSM_OVRD_1_REG, CccGlobalPmFsmOvrd1.Data);
    }
    if (WaitForLock) {
      MrcWait (MrcData, 300 * MRC_TIMER_1NS); // wait 300ns for MDLL to lock

      if (FubMask & DccFubData) {
        for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
          for (Channel = 0; Channel < Outputs->MaxChannels; Channel++) {
            if (!MrcChannelExist (MrcData, Controller, Channel)) {
              continue;
            }
            for (Strobe = 0; Strobe < Outputs->SdramCount; Strobe++) {
              TransChannel = Channel;
              TransStrobe  = Strobe;
              MrcTranslateSystemToIp (MrcData, &Controller, &TransChannel, &Rank, &TransStrobe, GsmIocTxOn); // No GetSet for this register, so use another per-byte PHY CR
              Offset  = DATA0CH0_CR_MDLLCTL0_REG +
                       (DATA0CH1_CR_MDLLCTL0_REG - DATA0CH0_CR_MDLLCTL0_REG) * TransChannel +
                       (DATA1CH0_CR_MDLLCTL0_REG - DATA0CH0_CR_MDLLCTL0_REG) * TransStrobe;
              DataMdllCtl0.Data = MrcReadCR (MrcData, Offset);
              if (DataMdllCtl0.Bits.mdll_cmn_mdlllock == 0) {
                MRC_DEBUG_MSG (Debug, MSG_LEVEL_WARNING, "%s MC%u C%u B%u mdll_cmn_mdlllock is 0\n", gWarnString, Controller, Channel, Strobe);
              }
            } // Strobe
          } // Channel
        } // Controller
      }
      if (FubMask & DccFubCcc) {
        for (Channel = 0; Channel < MRC_NUM_CCC_INSTANCES; Channel++) {
          if (MrcData->Inputs.A0) {
            Offset = OFFSET_CALC_CH (CH2CCC_CR_MDLLCTL0_A0_REG, CH0CCC_CR_MDLLCTL0_A0_REG, Channel);
          } else {
            Offset = OFFSET_CALC_CH (CH2CCC_CR_MDLLCTL0_REG, CH0CCC_CR_MDLLCTL0_REG, Channel);
          }
          CccMdllCtl0.Data  = MrcReadCR (MrcData, Offset);
          if (CccMdllCtl0.Bits.mdll_cmn_mdlllock == 0) {
            MRC_DEBUG_MSG (Debug, MSG_LEVEL_WARNING, "%s CCC%u mdll_cmn_mdlllock is 0\n", gWarnString, Channel);
          }
        }
      }
      if (FubMask & DccFubComp) {
        CompStatus.Data = MrcReadCR (MrcData, DDRPHY_COMP_CR_COMP_STATUS_REG);
        if (CompStatus.Bits.dllcomp_cmn_mdlllock == 0) {
          MRC_DEBUG_MSG (Debug, MSG_LEVEL_WARNING, "%s dllcomp_cmn_mdlllock is 0\n", gWarnString);
        }
      }
    } // WaitForLock
  } // if enable
}

/**
  Setup for InitDCC calibration

  @param[in, out] MrcData   - Include all MRC global data.
  @param[in]      DcdGroup  - see DCD_GROUP_xxx defines
  @param[in]      FubMask   - Setup DCC in this FUB (DATA/CCC/COMP)

  @retval mrcSuccess
**/
VOID
MrcDccSetup (
  IN OUT MrcParameters* const MrcData,
  IN UINT32                   DcdGroup,
  IN DccFubMask               FubMask
  )
{
  MrcOutput *Outputs;
  UINT32    Offset;
  BOOLEAN   A0;
  BOOLEAN   Gear4;
  BOOLEAN   Lpddr5;
  BOOLEAN   Ddr5;
  BOOLEAN   Ddr4;
  UINT32    CccPatternEnable;
  UINT32    DccMarkerVcdlLo;
  UINT32    DccMarkerVcdlHi;
  UINT32    DccMarkerRefClkLo;
  UINT32    DccMarkerRefClkHi;
  DATA0CH0_CR_DCCCTL0_STRUCT    DataDccCtl0;
  DATA0CH0_CR_DCCCTL1_STRUCT    DataDccCtl1;
  DATA0CH0_CR_DCCCTL3_STRUCT    DataDccCtl3;
  DATA0CH0_CR_DCCCTL4_STRUCT    DataDccCtl4;
  DATA0CH0_CR_DCCCTL10_STRUCT   DataDccCtl10;
  CH0CCC_CR_DCCCTL0_STRUCT      CccDccCtl0;
  CH0CCC_CR_DCCCTL1_STRUCT      CccDccCtl1;
  CH0CCC_CR_DCCCTL3_STRUCT      CccDccCtl3;
  CH0CCC_CR_DCCCTL4_STRUCT      CccDccCtl4;
  CH0CCC_CR_DCCCTL5_STRUCT      CccDccCtl5;
  CH0CCC_CR_DCCCTL13_STRUCT     CccDccCtl13;
  DDRPHY_COMP_CR_DCCCTL0_STRUCT CompDccCtl0;
  DDRPHY_COMP_CR_DCCCTL1_STRUCT CompDccCtl1;
  DDRPHY_COMP_CR_DCCCTL3_STRUCT CompDccCtl3;
  DDRPHY_COMP_CR_DCCCTL4_STRUCT CompDccCtl4;
  DDRPHY_COMP_CR_DCCCTL6_STRUCT CompDccCtl6;

  Outputs = &MrcData->Outputs;
  A0      = MrcData->Inputs.A0;
  Gear4   = Outputs->Gear4;
  Lpddr5  = (Outputs->DdrType == MRC_DDR_TYPE_LPDDR5);
  Ddr5    = (Outputs->DdrType == MRC_DDR_TYPE_DDR5);
  Ddr4    = (Outputs->DdrType == MRC_DDR_TYPE_DDR4);

  if ((MrcData->Inputs.DtHalo && !A0 && Ddr5) || (MrcData->Inputs.UlxUlt)) {   //@todo_adl - right markers of UlxUlt?
    DccMarkerVcdlLo   = DCC_MARKER_TIGHT_LO;
    DccMarkerVcdlHi   = DCC_MARKER_TIGHT_HI;
    DccMarkerRefClkLo = DCC_MARKER_TIGHT_LO;
    DccMarkerRefClkHi = DCC_MARKER_TIGHT_HI;
  } else {
    DccMarkerVcdlLo   = DCC_MARKER_VCDL_LO;
    DccMarkerVcdlHi   = DCC_MARKER_VCDL_HI;
    DccMarkerRefClkLo = DCC_MARKER_REFCLK_LO;
    DccMarkerRefClkHi = DCC_MARKER_REFCLK_HI;
  }
  // Setup
  if (FubMask & (DccFubData | DccFubCcc)) {
    DataDccCtl0.Data = MrcReadCR (MrcData, DATA0CH0_CR_DCCCTL0_REG);
    DataDccCtl0.Bits.microstep = 1;
    DataDccCtl0.Bits.macrostep = 4;
    DataDccCtl0.Bits.totalsamplecounts = DCC_TOTAL_COUNTS;
    MrcWriteCrMulticast (MrcData, DATA_CR_DCCCTL0_REG, DataDccCtl0.Data);

    Offset = A0 ? CH0CCC_CR_DCCCTL0_A0_REG : CH0CCC_CR_DCCCTL0_REG;
    CccDccCtl0.Data = MrcReadCR (MrcData, Offset);
    CccDccCtl0.Bits.microstep = 1;
    CccDccCtl0.Bits.macrostep = 4;
    CccDccCtl0.Bits.totalsamplecounts = DCC_TOTAL_COUNTS;
    Offset = A0 ? CCC_CR_DCCCTL0_A0_REG : CCC_CR_DCCCTL0_REG;
    MrcWriteCrMulticast (MrcData, Offset,  CccDccCtl0.Data);

    DataDccCtl1.Data = MrcReadCR (MrcData, DATA0CH0_CR_DCCCTL1_REG);
    if (DcdGroup == DCD_GROUP_VCDL) {
      DataDccCtl1.Bits.dcc_low_limit_marker  = DccMarkerVcdlLo;
      DataDccCtl1.Bits.dcc_high_limit_marker = DccMarkerVcdlHi;
    } else {
      DataDccCtl1.Bits.dcc_low_limit_marker  = DccMarkerRefClkLo;
      DataDccCtl1.Bits.dcc_high_limit_marker = DccMarkerRefClkHi;
    }
    MrcWriteCrMulticast (MrcData, DATA_CR_DCCCTL1_REG, DataDccCtl1.Data);

    Offset = A0 ? CH0CCC_CR_DCCCTL1_A0_REG : CH0CCC_CR_DCCCTL1_REG;
    CccDccCtl1.Data = MrcReadCR (MrcData, Offset);
    if (DcdGroup == DCD_GROUP_VCDL) {
      CccDccCtl1.Bits.dcc_low_limit_marker  = DccMarkerVcdlLo;
      CccDccCtl1.Bits.dcc_high_limit_marker = DccMarkerVcdlHi;
    } else {
      CccDccCtl1.Bits.dcc_low_limit_marker  = DccMarkerRefClkLo;
      CccDccCtl1.Bits.dcc_high_limit_marker = DccMarkerRefClkHi;
    }
    Offset = A0 ? CCC_CR_DCCCTL1_A0_REG : CCC_CR_DCCCTL1_REG;
    MrcWriteCrMulticast (MrcData, Offset, CccDccCtl1.Data);

    DataDccCtl4.Data = MrcReadCR (MrcData, DATA0CH0_CR_DCCCTL4_REG);
    DataDccCtl4.Bits.dcc_step3pattern_enable =  Gear4 ? 0x3FE: 0;
    DataDccCtl4.Bits.dcc_step2pattern_enable = Gear4 ? 0x3FE: 0;
    MrcWriteCrMulticast (MrcData, DATA_CR_DCCCTL4_REG, DataDccCtl4.Data);

    if (Gear4) {
      // Ddr4 - 0x3C0, Lp5 - 0x300, Ddr5/Lp4 - 0xC0
      CccPatternEnable = Ddr4 ? 0x3C0 : (Lpddr5 ? 0x300 : 0xC0);
    } else {
      CccPatternEnable = 0;
    }
    CccDccCtl4.Data = MrcReadCR (MrcData, CH0CCC_CR_DCCCTL4_REG);
    CccDccCtl4.Bits.dcc_step2pattern_enable = CccPatternEnable;
    MrcWriteCrMulticast (MrcData, CCC_CR_DCCCTL4_REG,  CccDccCtl4.Data);

    Offset = A0 ? CH0CCC_CR_DCCCTL5_A0_REG : CH0CCC_CR_DCCCTL5_REG;
    CccDccCtl5.Data = MrcReadCR (MrcData, Offset);
    CccDccCtl5.Bits.dcc_step3pattern_enable = CccPatternEnable;
    Offset = A0 ? CCC_CR_DCCCTL5_A0_REG : CCC_CR_DCCCTL5_REG;
    MrcWriteCrMulticast (MrcData, Offset, CccDccCtl5.Data);

    DataDccCtl3.Data = MrcReadCR (MrcData, DATA0CH0_CR_DCCCTL3_REG);
    DataDccCtl3.Bits.dcdrocal_low_freq_marker  = 0x72;
    DataDccCtl3.Bits.dcdrocal_high_freq_marker = 0x132;
    MrcWriteCrMulticast (MrcData, DATA_CR_DCCCTL3_REG, DataDccCtl3.Data);

    Offset = A0 ? CH0CCC_CR_DCCCTL3_A0_REG : CH0CCC_CR_DCCCTL3_REG;
    CccDccCtl3.Data = MrcReadCR (MrcData, Offset);
    CccDccCtl3.Bits.dcdrocal_low_freq_marker  = 0x72;
    CccDccCtl3.Bits.dcdrocal_high_freq_marker = 0x132;
    Offset = A0 ? CCC_CR_DCCCTL3_A0_REG : CCC_CR_DCCCTL3_REG;
    MrcWriteCrMulticast (MrcData, Offset, CccDccCtl3.Data);

    DataDccCtl10.Data = MrcReadCR (MrcData, DATA0CH0_CR_DCCCTL10_REG);
    DataDccCtl10.Bits.totalrocalsamplecounts  = 0xFF;
    DataDccCtl10.Bits.step1pattern            = 9;
    DataDccCtl10.Bits.step2pattern            = 3;
    DataDccCtl10.Bits.step3pattern            = 0xa;
    MrcWriteCrMulticast (MrcData, DATA_CR_DCCCTL10_REG, DataDccCtl10.Data);

    Offset = A0 ? CH0CCC_CR_DCCCTL13_A0_REG : CH0CCC_CR_DCCCTL13_REG;
    CccDccCtl13.Data = MrcReadCR (MrcData, Offset);
    CccDccCtl13.Bits.totalrocalsamplecounts = 0xFF;
    CccDccCtl13.Bits.step1pattern           = 9;
    CccDccCtl13.Bits.step2pattern           = 3;
    CccDccCtl13.Bits.step3pattern           = 0xa;
    Offset = A0 ? CCC_CR_DCCCTL13_A0_REG : CCC_CR_DCCCTL13_REG;
    MrcWriteCrMulticast (MrcData, Offset, CccDccCtl13.Data);
  }
  if (FubMask & DccFubComp) {
    CompDccCtl0.Data = MrcReadCR (MrcData, DDRPHY_COMP_CR_DCCCTL0_REG);
    CompDccCtl0.Bits.microstep = 1;
    CompDccCtl0.Bits.macrostep = 4;
    CompDccCtl0.Bits.totalsamplecounts = DCC_TOTAL_COUNTS;
    MrcWriteCR (MrcData, DDRPHY_COMP_CR_DCCCTL0_REG, CompDccCtl0.Data);

    CompDccCtl1.Data = MrcReadCR (MrcData, DDRPHY_COMP_CR_DCCCTL1_REG);
    if (DcdGroup == DCD_GROUP_VCDL) {
      CompDccCtl1.Bits.dcc_low_limit_marker  = DccMarkerVcdlLo;
      CompDccCtl1.Bits.dcc_high_limit_marker = DccMarkerVcdlHi;
    } else {
      CompDccCtl1.Bits.dcc_low_limit_marker  = DccMarkerRefClkLo;
      CompDccCtl1.Bits.dcc_high_limit_marker = DccMarkerRefClkHi;
    }
    MrcWriteCR (MrcData, DDRPHY_COMP_CR_DCCCTL1_REG, CompDccCtl1.Data);

    CompDccCtl4.Data = MrcReadCR (MrcData, DDRPHY_COMP_CR_DCCCTL4_REG);
    CompDccCtl4.Bits.dcc_step3pattern_enable = 0;
    CompDccCtl4.Bits.dcc_step2pattern_enable = 0;
    MrcWriteCR (MrcData, DDRPHY_COMP_CR_DCCCTL4_REG, CompDccCtl4.Data);

    CompDccCtl6.Data = MrcReadCR (MrcData, DDRPHY_COMP_CR_DCCCTL6_REG);
    CompDccCtl6.Bits.totalrocalsamplecounts = 0xFF;
    MrcWriteCR (MrcData, DDRPHY_COMP_CR_DCCCTL6_REG, CompDccCtl6.Data);

    CompDccCtl3.Data = MrcReadCR (MrcData, DDRPHY_COMP_CR_DCCCTL3_REG);
    CompDccCtl3.Bits.dcdrocal_low_freq_marker  = 0x72;
    CompDccCtl3.Bits.dcdrocal_high_freq_marker = 0x132;
    MrcWriteCR (MrcData, DDRPHY_COMP_CR_DCCCTL3_REG,  CompDccCtl3.Data);
  }
}

/**
  Run DCC RO calibration for the given FUB

  @param[in, out] MrcData   - Include all MRC global data.
  @param[in]      RoFubMask - Run DCC RO init in this partition(s)
**/
VOID
MrcDccROCal (
  IN OUT MrcParameters* const MrcData,
  IN const DccFubMask         RoFubMask
  )
{
  DDRPHY_COMP_CR_DCCCTL4_STRUCT CompDccCtl4;

  if (RoFubMask & DccFubComp) {
    // Reset rocal FSM
    CompDccCtl4.Data = MrcReadCR (MrcData, DDRPHY_COMP_CR_DCCCTL4_REG);
    CompDccCtl4.Bits.dcdrocal_en = 0;
    MrcWriteCR (MrcData, DDRPHY_COMP_CR_DCCCTL4_REG, CompDccCtl4.Data);
    CompDccCtl4.Bits.dcdrocalsamplecountrst_b = 0;
    MrcWriteCR (MrcData, DDRPHY_COMP_CR_DCCCTL4_REG, CompDccCtl4.Data);
    CompDccCtl4.Bits.dcdrocalsamplecountrst_b = 1;
    MrcWriteCR (MrcData, DDRPHY_COMP_CR_DCCCTL4_REG, CompDccCtl4.Data);
    CompDccCtl4.Bits.dcdrocal_en = 1;
    MrcWriteCR (MrcData, DDRPHY_COMP_CR_DCCCTL4_REG, CompDccCtl4.Data);

    // Start ROcal FSM
    CompDccCtl4.Data = MrcReadCR (MrcData, DDRPHY_COMP_CR_DCCCTL4_REG);
    CompDccCtl4.Bits.dcdrocal_start = 1;
    MrcWriteCR (MrcData, DDRPHY_COMP_CR_DCCCTL4_REG, CompDccCtl4.Data);
    MrcWait (MrcData, MRC_TIMER_1US); // @todo_adl ask DE for exact number or poll
  }
  if (RoFubMask & DccFubData) {
    // Reset rocal FSMs
    MrcGetSetChStrb (MrcData, MAX_CONTROLLER, MAX_CHANNEL, MAX_SDRAM_IN_DIMM, GsmIocDcdRoCalEnData,             WriteNoCache, &gGetSetDis);
    MrcGetSetChStrb (MrcData, MAX_CONTROLLER, MAX_CHANNEL, MAX_SDRAM_IN_DIMM, GsmIocDcdRoCalSampleCountRstData, WriteNoCache, &gGetSetDis);
    MrcGetSetChStrb (MrcData, MAX_CONTROLLER, MAX_CHANNEL, MAX_SDRAM_IN_DIMM, GsmIocDcdRoCalSampleCountRstData, WriteNoCache, &gGetSetEn);
    MrcGetSetChStrb (MrcData, MAX_CONTROLLER, MAX_CHANNEL, MAX_SDRAM_IN_DIMM, GsmIocDcdRoCalEnData,             WriteNoCache, &gGetSetEn);

    // Start ROcal FSM
    MrcGetSetChStrb (MrcData, MAX_CONTROLLER, MAX_CHANNEL, MAX_SDRAM_IN_DIMM, GsmIocDcdRoCalStartData, WriteNoCache, &gGetSetEn);
    MrcWait (MrcData, MRC_TIMER_1US); // @todo_adl ask DE for exact number or poll
  }
  if (RoFubMask & DccFubCcc) {
    // Reset rocal FSMs
    MrcGetSetCccInst (MrcData, MAX_CCC_INSTANCES, GsmIocDcdRoCalEnCcc,              WriteNoCache, &gGetSetDis);
    MrcGetSetCccInst (MrcData, MAX_CCC_INSTANCES, GsmIocDcdRoCalSampleCountRstCcc,  WriteNoCache, &gGetSetDis);
    MrcGetSetCccInst (MrcData, MAX_CCC_INSTANCES, GsmIocDcdRoCalSampleCountRstCcc,  WriteNoCache, &gGetSetEn);
    MrcGetSetCccInst (MrcData, MAX_CCC_INSTANCES, GsmIocDcdRoCalEnCcc,              WriteNoCache, &gGetSetEn);

    // Start ROcal FSM
    MrcGetSetCccInst (MrcData, MAX_CCC_INSTANCES, GsmIocDcdRoCalStartCcc, WriteNoCache, &gGetSetEn);
    MrcWait (MrcData, MRC_TIMER_1US); // @todo_adl ask DE for exact number or poll
  }
}

/**
  DCC Q0 Workaround for DQS Buffers

  @param[in, out] MrcData   - Include all MRC global data.
**/
VOID
DqsDccWaQ0 (
  IN OUT MrcParameters* const MrcData,
  IN DccFubMask               FubMask
  )
{
  MrcOutput *Outputs;
  UINT32    Controller;
  UINT32    Channel;
  UINT32    TransChannel;
  UINT32    Rank;
  UINT32    Strobe;
  UINT32    TransStrobe;
  UINT32    Offset;
  DATA0CH0_CR_DCCCTL6_STRUCT  DataDccCtl6;
  DATA0CH0_CR_DCCCTL5_STRUCT  DataDccCtl5;
  DATA0CH0_CR_DCCCTL8_STRUCT  DataDccCtl8;
  DATA0CH0_CR_DCCCTL7_STRUCT  DataDccCtl7;

  Outputs = &MrcData->Outputs;

  if (FubMask & DccFubData) {
    for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
      for (Channel = 0; Channel < Outputs->MaxChannels; Channel++) {
        if (!MrcChannelExist (MrcData, Controller, Channel)) {
          continue;
        }
        for (Strobe = 0; Strobe < Outputs->SdramCount; Strobe++) {
          TransChannel = Channel;
          TransStrobe  = Strobe;
          Rank = 0;
          MrcTranslateSystemToIp (MrcData, &Controller, &TransChannel, &Rank, &TransStrobe, GsmIocTxOn); // No GetSet for this register, so use another per-byte PHY CR
          Offset  = DATA0CH0_CR_DCCCTL6_REG +
                   (DATA0CH1_CR_DCCCTL6_REG - DATA0CH0_CR_DCCCTL6_REG) * TransChannel +
                   (DATA1CH0_CR_DCCCTL6_REG - DATA0CH0_CR_DCCCTL6_REG) * TransStrobe;
          DataDccCtl6.Data = MrcReadCR (MrcData, Offset);
          Offset  = DATA0CH0_CR_DCCCTL5_REG +
                   (DATA0CH1_CR_DCCCTL5_REG - DATA0CH0_CR_DCCCTL5_REG) * TransChannel +
                   (DATA1CH0_CR_DCCCTL5_REG - DATA0CH0_CR_DCCCTL5_REG) * TransStrobe;
          DataDccCtl5.Data = MrcReadCR (MrcData, Offset);
          DataDccCtl5.Bits.dcccodeph0_index1 = DataDccCtl6.Bits.dcccodeph0_index5;
          MrcWriteCR (MrcData, Offset, DataDccCtl5.Data);

          Offset  = DATA0CH0_CR_DCCCTL8_REG +
                   (DATA0CH1_CR_DCCCTL8_REG - DATA0CH0_CR_DCCCTL8_REG) * TransChannel +
                   (DATA1CH0_CR_DCCCTL8_REG - DATA0CH0_CR_DCCCTL8_REG) * TransStrobe;
          DataDccCtl8.Data = MrcReadCR (MrcData, Offset);
          Offset  = DATA0CH0_CR_DCCCTL7_REG +
                   (DATA0CH1_CR_DCCCTL7_REG - DATA0CH0_CR_DCCCTL7_REG) * TransChannel +
                   (DATA1CH0_CR_DCCCTL7_REG - DATA0CH0_CR_DCCCTL7_REG) * TransStrobe;
          DataDccCtl7.Data = MrcReadCR (MrcData, Offset);
          DataDccCtl7.Bits.dcccodeph90_index1 = DataDccCtl8.Bits.dcccodeph90_index5;
          MrcWriteCR (MrcData, Offset, DataDccCtl7.Data);
        } // Strobe
      } // Channel
    } // Controller
  }
}

/**
  Run DCC FSM, poll for completion and print DCC results per buffer

  @param[in, out] MrcData   - Include all MRC global data.
  @param[in]      DcdGroup  - see DCD_GROUP_xxx defines
  @param[in]      FubMask   - Run DCC init in this partition(s)
  @param[in]      RoFubMask - Run DCC RO init in this partition(s)
  @param[in]      DcdBits   - DCD bits for serializer; zero means all bits

  @retval mrcSuccess
**/
MrcStatus
MrcDccRun (
  IN OUT MrcParameters* const MrcData,
  IN const UINT32             DcdGroup,
  IN const DccFubMask         FubMask,
  IN const DccFubMask         RoFubMask,
  IN const UINT32             DcdBits
  )
{
  const MRC_FUNCTION *MrcCall;
  MrcDebug  *Debug;
  MrcInput  *Inputs;
  MrcOutput *Outputs;
  MrcStatus Status;
  UINT32    Offset;
  UINT32    Controller;
  UINT32    Channel;
  UINT32    TransChannel;
  UINT32    Rank;
  UINT32    Strobe;
  UINT32    TransStrobe;
  UINT32    DcdTargClkSel;
  UINT32    SampleCount;
  UINT32    TotalCounts;
  UINT32    Data32;
  UINT32    DataInt;
  BOOLEAN   DccComplete;
  BOOLEAN   A0;
  BOOLEAN   Q0;
  BOOLEAN   G0;
  UINT64    Timeout;
  INT64     GetSetVal;
  CH0CCC_CR_MDLLCTL2_STRUCT   CccMdllCtl2;
  DATA0CH0_CR_MDLLCTL2_STRUCT DataMdllCtl2;
  DATA0CH0_CR_DCCCTL0_STRUCT  DataDccCtl0;
  DATA0CH0_CR_DCCCTL4_STRUCT  DataDccCtl4;
  DATA0CH0_CR_DCCCTL13_STRUCT DataDccCtl13;
  CH0CCC_CR_DCCCTL0_STRUCT    CccDccCtl0;
  CH0CCC_CR_DCCCTL4_STRUCT    CccDccCtl4;
  CH0CCC_CR_DCCCTL19_STRUCT   CccDccCtl19;
  DDRPHY_COMP_CR_DLLCOMP_DCCCTRL_STRUCT CompDccCtrl;
  DDRPHY_COMP_CR_DCCCTL0_STRUCT         CompDccCtl0;
  DDRPHY_COMP_CR_DCCCTL4_STRUCT         CompDccCtl4;
  DDRPHY_COMP_CR_DCCCTL6_STRUCT         CompDccCtl6;
  DDRPHY_COMP_CR_COMP_DCCSTATUS1_STRUCT CompDccStatus1;

  Status  = mrcSuccess;
  Inputs  = &MrcData->Inputs;
  Outputs = &MrcData->Outputs;
  Debug   = &Outputs->Debug;
  MrcCall = Inputs->Call.Func;
  A0      = Inputs->A0;
  Q0      = Inputs->Q0;
  G0      = Inputs->G0;
  TotalCounts = DCC_TOTAL_COUNTS;

  // Start MDLLs
  MrcOvrMdllEn (MrcData, TRUE, FubMask, TRUE);

  DcdTargClkSel = (DcdGroup == DCD_GROUP_REFCLK) ? 0 : 1;

  // Configure DLL DCD target Clock Selection and DCC Groups
  if (FubMask & DccFubComp) {
    CompDccCtrl.Data = MrcReadCR (MrcData, DDRPHY_COMP_CR_DLLCOMP_DCCCTRL_REG);
    CompDccCtrl.Bits.dllcomp_cmn_dcdtargclksel = DcdTargClkSel;
    MrcWriteCR (MrcData, DDRPHY_COMP_CR_DLLCOMP_DCCCTRL_REG, CompDccCtrl.Data);

    CompDccCtl4.Data = MrcReadCR (MrcData, DDRPHY_COMP_CR_DCCCTL4_REG);
    CompDccCtl4.Bits.dcc_bits_en = 1;
    MrcWriteCR (MrcData, DDRPHY_COMP_CR_DCCCTL4_REG, CompDccCtl4.Data);
  }
  if (FubMask & DccFubData) {
    if (DcdGroup != DCD_GROUP_SRZ) {
      DataMdllCtl2.Data = MrcReadCR (MrcData, DATA0CH0_CR_MDLLCTL2_REG);
      DataMdllCtl2.Bits.mdll_cmn_dcdtargclksel = DcdTargClkSel;
      MrcWriteCrMulticast (MrcData, DATA_CR_MDLLCTL2_REG, DataMdllCtl2.Data);
    }
    DataDccCtl4.Data = MrcReadCR (MrcData, DATA0CH0_CR_DCCCTL4_REG);
    if (DcdGroup == DCD_GROUP_SRZ) {
      DataDccCtl4.Bits.dcc_bits_en = (DcdBits == 0) ? ((Q0 || G0) ? 0x3FC : 0x3FE) : DcdBits;
    } else {
      DataDccCtl4.Bits.dcc_bits_en = 1;
    }
    MrcWriteCrMulticast (MrcData, DATA_CR_DCCCTL4_REG, DataDccCtl4.Data);
  }
  if (FubMask & DccFubCcc) {
    if (DcdGroup != DCD_GROUP_SRZ) {
      Offset = A0 ? CH0CCC_CR_MDLLCTL2_A0_REG : CH0CCC_CR_MDLLCTL2_REG;
      CccMdllCtl2.Data  = MrcReadCR (MrcData, Offset);
      CccMdllCtl2.Bits.mdll_cmn_dcdtargclksel  = DcdTargClkSel;
      Offset = A0 ? CCC_CR_MDLLCTL2_A0_REG : CCC_CR_MDLLCTL2_REG;
      MrcWriteCrMulticast (MrcData, Offset, CccMdllCtl2.Data);
    }
    CccDccCtl4.Data  = MrcReadCR (MrcData, CH0CCC_CR_DCCCTL4_REG);
    if (DcdGroup == DCD_GROUP_SRZ) {
      CccDccCtl4.Bits.dcc_bits_en = (DcdBits == 0) ? 0xFFFE : DcdBits;
    } else {
      CccDccCtl4.Bits.dcc_bits_en = 1;
    }
    MrcWriteCrMulticast (MrcData, CCC_CR_DCCCTL4_REG,  CccDccCtl4.Data);
  }

  // Start Ring Oscillator (aka auxclk)
  if (FubMask & DccFubComp) {
    CompDccCtl0.Data = MrcReadCR (MrcData, DDRPHY_COMP_CR_DCCCTL0_REG);
    CompDccCtl0.Bits.dcdrovcoen = 1;
    MrcWriteCR (MrcData, DDRPHY_COMP_CR_DCCCTL0_REG, CompDccCtl0.Data);
  }
  if (FubMask & DccFubData) {
    DataDccCtl0.Data = MrcReadCR (MrcData, DATA0CH0_CR_DCCCTL0_REG);
    DataDccCtl0.Bits.dcdrovcoen = 1;
    MrcWriteCrMulticast (MrcData, DATA_CR_DCCCTL0_REG, DataDccCtl0.Data);
  }
  if (FubMask & DccFubCcc) {
    Offset = A0 ? CH0CCC_CR_DCCCTL0_A0_REG : CH0CCC_CR_DCCCTL0_REG;
    CccDccCtl0.Data  = MrcReadCR (MrcData, Offset);
    CccDccCtl0.Bits.dcdrovcoen  = 1;
    Offset = A0 ? CCC_CR_DCCCTL0_A0_REG : CCC_CR_DCCCTL0_REG;
    MrcWriteCrMulticast (MrcData, Offset,  CccDccCtl0.Data);
  }

  // Bring DCC FSM Out of Reset
  if (FubMask & DccFubComp) {
    CompDccCtl6.Data = MrcReadCR (MrcData, DDRPHY_COMP_CR_DCCCTL6_REG);
    CompDccCtl6.Bits.dccfsm_rst_b = 1;
    MrcWriteCR (MrcData, DDRPHY_COMP_CR_DCCCTL6_REG, CompDccCtl6.Data);
  }
  if (FubMask & DccFubData) {
    MrcGetSetChStrb (MrcData, MAX_CONTROLLER, MAX_CHANNEL, MAX_SDRAM_IN_DIMM, GsmIocDccFsmResetData, WriteNoCache, &gGetSetEn);
  }
  if (FubMask & DccFubCcc) {
    MrcGetSetCccInst (MrcData, MAX_CCC_INSTANCES, GsmIocDccFsmResetCcc, WriteNoCache, &gGetSetEn);
  }

  // RO calibration
  MrcDccROCal (MrcData, RoFubMask);

  // Start Main DCC FSM
  if (FubMask & DccFubComp) {
    CompDccCtl4.Data = MrcReadCR (MrcData, DDRPHY_COMP_CR_DCCCTL4_REG);
    CompDccCtl4.Bits.dcc_start = 1;
    MrcWriteCR (MrcData, DDRPHY_COMP_CR_DCCCTL4_REG, CompDccCtl4.Data);
  }
  if (FubMask & DccFubData) {
    MrcGetSetChStrb (MrcData, MAX_CONTROLLER, MAX_CHANNEL, MAX_SDRAM_IN_DIMM, GsmIocDccStartData, WriteNoCache, &gGetSetEn);
  }
  if (FubMask & DccFubCcc) {
    MrcGetSetCccInst (MrcData, MAX_CCC_INSTANCES, GsmIocDccStartCcc, WriteNoCache, &gGetSetEn);
  }
  if (MrcData->Inputs.SimicsFlag == 1) { // No Simics support for FSM yet
    return Status;
  }

  DccComplete = TRUE;
  // Poll for completion
  if (FubMask & DccFubComp) {
    Timeout = MrcCall->MrcGetCpuTime () + DCC_FSM_TIMEOUT;
    do {
      DccComplete = TRUE;                    // Assume all done
      CompDccCtl4.Data = MrcReadCR (MrcData, DDRPHY_COMP_CR_DCCCTL4_REG);
      DccComplete = (CompDccCtl4.Bits.dcc_start == 0);
    } while (!DccComplete && (MrcCall->MrcGetCpuTime () < Timeout));
    if (!DccComplete) {
      MRC_DEBUG_MSG (Debug, MSG_LEVEL_WARNING, "%s COMP DCC Timeout!\n", gWarnString);
    }

    // Print measured duty cycle per fub
    CompDccStatus1.Data = MrcReadCR (MrcData, DDRPHY_COMP_CR_COMP_DCCSTATUS1_REG);
    SampleCount = CompDccStatus1.Bits.samplecountrise;
    Data32 = 10000 * SampleCount;
    Data32 = UDIVIDEROUND (Data32, 2 * TotalCounts);  // Precision of 2 decimal points
    DataInt = Data32 / 100;
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Measured duty cycle: COMP: %u.%02u\n", DataInt, Data32 - 100 * DataInt);
    CompDccCtl4.Data = MrcReadCR (MrcData, DDRPHY_COMP_CR_DCCCTL4_REG);
  }
  if (FubMask & DccFubData) {
    Timeout = MrcCall->MrcGetCpuTime () + DCC_FSM_TIMEOUT;
    do {
      DccComplete = TRUE;                    // Assume all done
      for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
        for (Channel = 0; Channel < Outputs->MaxChannels; Channel++) {
          if (!MrcChannelExist (MrcData, Controller, Channel)) {
            continue;
          }
          for (Strobe = 0; Strobe < Outputs->SdramCount; Strobe++) {
            MrcGetSetChStrb (MrcData, Controller, Channel, Strobe, GsmIocDccStartData, ReadNoCache, &GetSetVal);
            if (GetSetVal == 1) {
              DccComplete = FALSE;
            }
          } // Strobe
        } // Channel
      } // Controller
    } while (!DccComplete && (MrcCall->MrcGetCpuTime () < Timeout));

    if (!DccComplete) {
      MRC_DEBUG_MSG (Debug, MSG_LEVEL_WARNING, "%s DCC Timeout!\n", gWarnString);
    }

    if (DcdGroup != DCD_GROUP_SRZ) {
      // Print measured duty cycle per fub
      MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Measured duty cycle:\n");
      for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
        for (Channel = 0; Channel < Outputs->MaxChannels; Channel++) {
          if (!MrcChannelExist (MrcData, Controller, Channel)) {
            continue;
          }
          for (Strobe = 0; Strobe < Outputs->SdramCount; Strobe++) {
            TransChannel = Channel;
            TransStrobe  = Strobe;
            MrcTranslateSystemToIp (MrcData, &Controller, &TransChannel, &Rank, &TransStrobe, GsmIocTxOn); // No GetSet for this register, so use another per-byte PHY CR
            Offset  = DATA0CH0_CR_DCCCTL13_REG +
                     (DATA0CH1_CR_DCCCTL13_REG - DATA0CH0_CR_DCCCTL13_REG) * TransChannel +
                     (DATA1CH0_CR_DCCCTL13_REG - DATA0CH0_CR_DCCCTL13_REG) * TransStrobe;
            DataDccCtl13.Data = MrcReadCR (MrcData, Offset);
            SampleCount = DataDccCtl13.Bits.dcdsamplecount;
            Data32 = 10000 * SampleCount;
            Data32 = UDIVIDEROUND (Data32, 2 * TotalCounts);  // Precision of 2 decimal points
            DataInt = Data32 / 100;
            MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "MC%u C%u B%u: %u.%02u", Controller, Channel, Strobe, DataInt, Data32 - 100 * DataInt);

            MrcGetSetChStrb (MrcData, Controller, Channel, Strobe, GsmIocDccStartData, ReadNoCache, &GetSetVal);
            if (GetSetVal == 1) {
              MRC_DEBUG_MSG (Debug, MSG_LEVEL_WARNING, " - %s DCC Timeout!\n", gWarnString);
            } else {
              MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\n");
            }
          } // Strobe
        } // Channel
      } // Controller
    }
  } // DccFubData
  if (FubMask & DccFubCcc) {
    Timeout = MrcCall->MrcGetCpuTime () + DCC_FSM_TIMEOUT;
    do {
      DccComplete = TRUE;                    // Assume all done
      for (Channel = 0; Channel < MRC_NUM_CCC_INSTANCES; Channel++) {
        MrcGetSetCccInst (MrcData, Channel, GsmIocDccStartCcc, ReadNoCache, &GetSetVal);
        if (GetSetVal == 1) {
          DccComplete = FALSE;
        }
      }
    } while (!DccComplete && (MrcCall->MrcGetCpuTime () < Timeout));

    if (!DccComplete) {
      MRC_DEBUG_MSG (Debug, MSG_LEVEL_WARNING, "%s DCC Timeout!\n", gWarnString);
    }

    if (DcdGroup != DCD_GROUP_SRZ) {
      // Print measured duty cycle per fub
      MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Measured duty cycle:\n");
      for (Channel = 0; Channel < MRC_NUM_CCC_INSTANCES; Channel++) {
        Offset = OFFSET_CALC_CH (CH2CCC_CR_DCCCTL19_REG, CH0CCC_CR_DCCCTL19_REG, Channel);
        CccDccCtl19.Data  = MrcReadCR (MrcData, Offset);
        SampleCount = CccDccCtl19.Bits.dcdsamplecount;
        Data32 = 10000 * SampleCount;
        Data32 = UDIVIDEROUND (Data32, 2 * TotalCounts);  // Precision of 2 decimal points
        DataInt = Data32 / 100;
        MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "CCC%u: %u.%02u", Channel, DataInt, Data32 - 100 * DataInt);

        MrcGetSetCccInst (MrcData, Channel, GsmIocDccStartCcc, ReadNoCache, &GetSetVal);
        if (GetSetVal == 1) {
          MRC_DEBUG_MSG (Debug, MSG_LEVEL_WARNING, " - %s DCC Timeout!\n", gWarnString);
        } else {
          MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\n");
        }
      } // Channel
    }
  } // CccAndDataDcc

  // Cleanup

  if (FubMask & DccFubComp) {
    CompDccCtl4.Data = MrcReadCR (MrcData, DDRPHY_COMP_CR_DCCCTL4_REG);
    CompDccCtl4.Bits.dcc_bits_en = 0;
    MrcWriteCR (MrcData, DDRPHY_COMP_CR_DCCCTL4_REG, CompDccCtl4.Data);

    CompDccCtl6.Data = MrcReadCR (MrcData, DDRPHY_COMP_CR_DCCCTL6_REG);
    CompDccCtl6.Bits.dccfsm_rst_b = 0;
    MrcWriteCR (MrcData, DDRPHY_COMP_CR_DCCCTL6_REG, CompDccCtl6.Data);
    CompDccCtl6.Bits.dccfsm_rst_b = 1;
    MrcWriteCR (MrcData, DDRPHY_COMP_CR_DCCCTL6_REG, CompDccCtl6.Data);

    CompDccCtl0.Data = MrcReadCR (MrcData, DDRPHY_COMP_CR_DCCCTL0_REG);
    CompDccCtl0.Bits.dcdrovcoen = 0;
    MrcWriteCR (MrcData, DDRPHY_COMP_CR_DCCCTL0_REG, CompDccCtl0.Data);
  }
  if (FubMask & DccFubData) {
    DataDccCtl4.Data = MrcReadCR (MrcData, DATA0CH0_CR_DCCCTL4_REG);
    DataDccCtl4.Bits.dcc_bits_en = 0;
    MrcWriteCrMulticast (MrcData, DATA_CR_DCCCTL4_REG, DataDccCtl4.Data);

    // Toggle FSM reset
    MrcGetSetChStrb (MrcData, MAX_CONTROLLER, MAX_CHANNEL, MAX_SDRAM_IN_DIMM, GsmIocDccFsmResetData, WriteNoCache, &gGetSetDis);
    MrcGetSetChStrb (MrcData, MAX_CONTROLLER, MAX_CHANNEL, MAX_SDRAM_IN_DIMM, GsmIocDccFsmResetData, WriteNoCache, &gGetSetEn);

    DataDccCtl0.Data = MrcReadCR (MrcData, DATA0CH0_CR_DCCCTL0_REG);
    DataDccCtl0.Bits.dcdrovcoen = 0;
    MrcWriteCrMulticast (MrcData, DATA_CR_DCCCTL0_REG, DataDccCtl0.Data);
  }
  if (FubMask & DccFubCcc) {
    CccDccCtl4.Data  = MrcReadCR (MrcData, CH0CCC_CR_DCCCTL4_REG);
    CccDccCtl4.Bits.dcc_bits_en  = 0;
    MrcWriteCrMulticast (MrcData, CCC_CR_DCCCTL4_REG,  CccDccCtl4.Data);

    // Toggle FSM reset
    MrcGetSetCccInst (MrcData, MAX_CCC_INSTANCES, GsmIocDccFsmResetCcc, WriteNoCache, &gGetSetDis);
    MrcGetSetCccInst (MrcData, MAX_CCC_INSTANCES, GsmIocDccFsmResetCcc, WriteNoCache, &gGetSetEn);

    Offset = A0 ? CH0CCC_CR_DCCCTL0_A0_REG : CH0CCC_CR_DCCCTL0_REG;
    CccDccCtl0.Data  = MrcReadCR (MrcData, Offset);
    CccDccCtl0.Bits.dcdrovcoen  = 0;
    Offset = A0 ? CCC_CR_DCCCTL0_A0_REG : CCC_CR_DCCCTL0_REG;
    MrcWriteCrMulticast (MrcData, Offset, CccDccCtl0.Data);
  }

  // Disable MDLLs
  MrcOvrMdllEn (MrcData, FALSE, FubMask, FALSE);
  if (Q0 || G0) {
    DqsDccWaQ0 (MrcData, FubMask);
  }
  return Status;
}

#ifdef MRC_DEBUG_PRINT

/**
  Print DCC Serializer results in DATA FUBs

  @param[in, out] MrcData   - Include all MRC global data.

  @retval mrcSuccess
**/
VOID
MrcDccPrintSrzResultsData (
  IN OUT MrcParameters* const MrcData
  )
{
  MrcDebug  *Debug;
  MrcOutput *Outputs;
  UINT32    Offset;
  UINT32    Controller;
  UINT32    Channel;
  UINT32    TransChannel;
  UINT32    Rank;
  UINT32    Strobe;
  UINT32    TransStrobe;
  UINT32    SampleCount;
  UINT32    TotalCounts;
  UINT32    Data32;
  UINT32    DataInt;
  UINT32    i;
  INT64     GetSetVal;
  DATA0CH0_CR_DCCCTL0_STRUCT  DataDccCtl0;
  DATA0CH0_CR_DCCCTL4_STRUCT  DataDccCtl4;
  DATA0CH0_CR_DCCCTL13_STRUCT DataDccCtl13;

  Outputs = &MrcData->Outputs;
  Debug   = &Outputs->Debug;
  TotalCounts = DCC_TOTAL_COUNTS;

  DataDccCtl0.Data = MrcReadCR (MrcData, DATA0CH0_CR_DCCCTL0_REG);
  DataDccCtl0.Bits.dcdrovcoen = 1;
  MrcWriteCrMulticast (MrcData, DATA_CR_DCCCTL0_REG, DataDccCtl0.Data);

  MrcGetSetChStrb (MrcData, MAX_CONTROLLER, MAX_CHANNEL, MAX_SDRAM_IN_DIMM, GsmIocMdllDcdEnData, WriteNoCache, &gGetSetEn);
  MrcGetSetChStrb (MrcData, MAX_CONTROLLER, MAX_CHANNEL, MAX_SDRAM_IN_DIMM, GsmIocSrzDcdEnData, WriteNoCache, &gGetSetEn);

  DataDccCtl4.Data = MrcReadCR (MrcData, DATA0CH0_CR_DCCCTL4_REG);
  DataDccCtl4.Bits.dcc_serdata_ovren  = 1;
  DataDccCtl4.Bits.dcc_bits_en        = 0;
  MrcWriteCrMulticast (MrcData, DATA_CR_DCCCTL4_REG, DataDccCtl4.Data);

  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\nByte");
  for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
    for (Channel = 0; Channel < Outputs->MaxChannels; Channel++) {
      if (MrcChannelExist (MrcData, Controller, Channel)) {
        for (Strobe = 0; Strobe < Outputs->SdramCount; Strobe++) {
          MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\t%u", Strobe);
        }
      }
    }
  }
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\n");

  // DQS and DQ[0..7]
  for (i = 1; i <= 9; i++) {
    MrcGetSetChStrb (MrcData, MAX_CONTROLLER, MAX_CHANNEL, MAX_SDRAM_IN_DIMM, GsmIocDcdSampleCountRstData, WriteNoCache, &gGetSetDis);
    MrcGetSetChStrb (MrcData, MAX_CONTROLLER, MAX_CHANNEL, MAX_SDRAM_IN_DIMM, GsmIocDcdSampleCountRstData, WriteNoCache, &gGetSetEn);

    GetSetVal = i;
    MrcGetSetChStrb (MrcData, MAX_CONTROLLER, MAX_CHANNEL, MAX_SDRAM_IN_DIMM, GsmIocDccDcdSampleSelData, WriteNoCache, &GetSetVal);

    MrcGetSetChStrb (MrcData, MAX_CONTROLLER, MAX_CHANNEL, MAX_SDRAM_IN_DIMM, GsmIocDcdSampleCountStartData, WriteNoCache, &gGetSetEn);

    MrcWait (MrcData, 300 * MRC_TIMER_1US); // wait for totalsamplecounts number of roclk cycles

    MrcGetSetChStrb (MrcData, MAX_CONTROLLER, MAX_CHANNEL, MAX_SDRAM_IN_DIMM, GsmIocDcdSampleCountStartData, WriteNoCache, &gGetSetDis);

    if (i == 1) {
      MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "DQS");
    } else {
      MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "DQ%u", i - 2);
    }
    for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
      for (Channel = 0; Channel < Outputs->MaxChannels; Channel++) {
        if (!MrcChannelExist (MrcData, Controller, Channel)) {
          continue;
        }
        for (Strobe = 0; Strobe < Outputs->SdramCount; Strobe++) {
          TransChannel = Channel;
          TransStrobe  = Strobe;
          MrcTranslateSystemToIp (MrcData, &Controller, &TransChannel, &Rank, &TransStrobe, GsmIocTxOn); // No GetSet for this register, so use another per-byte PHY CR
          Offset  = DATA0CH0_CR_DCCCTL13_REG +
                    (DATA0CH1_CR_DCCCTL13_REG - DATA0CH0_CR_DCCCTL13_REG) * TransChannel +
                    (DATA1CH0_CR_DCCCTL13_REG - DATA0CH0_CR_DCCCTL13_REG) * TransStrobe;
          DataDccCtl13.Data = MrcReadCR (MrcData, Offset);
          SampleCount = DataDccCtl13.Bits.dcdsamplecount;
          Data32 = 10000 * SampleCount;
          Data32 = UDIVIDEROUND (Data32, 2 * TotalCounts);  // Precision of 2 decimal points
          DataInt = Data32 / 100;
          MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\t%u.%02u", DataInt, Data32 - 100 * DataInt);
        } // Strobe
      } // Channel
    } // Controller
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\n");
  } // i

  // Cleanup
  DataDccCtl0.Data = MrcReadCR (MrcData, DATA0CH0_CR_DCCCTL0_REG);
  DataDccCtl0.Bits.dcdrovcoen = 0;
  MrcWriteCrMulticast (MrcData, DATA_CR_DCCCTL0_REG, DataDccCtl0.Data);

  MrcGetSetChStrb (MrcData, MAX_CONTROLLER, MAX_CHANNEL, MAX_SDRAM_IN_DIMM, GsmIocMdllDcdEnData, WriteNoCache, &gGetSetDis);
  MrcGetSetChStrb (MrcData, MAX_CONTROLLER, MAX_CHANNEL, MAX_SDRAM_IN_DIMM, GsmIocSrzDcdEnData, WriteNoCache, &gGetSetDis);

  DataDccCtl4.Data = MrcReadCR (MrcData, DATA0CH0_CR_DCCCTL4_REG);
  DataDccCtl4.Bits.dcc_serdata_ovren  = 0;
  MrcWriteCrMulticast (MrcData, DATA_CR_DCCCTL4_REG, DataDccCtl4.Data);
}

/**
  Print DCC Serializer results in CCC FUBs

  @param[in, out] MrcData   - Include all MRC global data.

  @retval mrcSuccess
**/
VOID
MrcDccPrintSrzResultsCcc (
  IN OUT MrcParameters* const MrcData
  )
{
  MrcDebug  *Debug;
  MrcOutput *Outputs;
  UINT32    Offset;
  UINT32    Channel;
  UINT32    SampleCount;
  UINT32    TotalCounts;
  UINT32    Data32;
  UINT32    DataInt;
  UINT32    i;
  UINT32    NumBuffers;
  INT64     GetSetVal;
  CH0CCC_CR_DCCCTL0_STRUCT    CccDccCtl0;
  CH0CCC_CR_DCCCTL2_STRUCT    CccDccCtl2;
  CH0CCC_CR_DCCCTL4_STRUCT    CccDccCtl4;
  CH0CCC_CR_DCCCTL19_STRUCT   CccDccCtl19;
  Outputs = &MrcData->Outputs;
  Debug   = &Outputs->Debug;
  TotalCounts = DCC_TOTAL_COUNTS;
  NumBuffers = (MrcData->Inputs.UlxUlt) ? 13 : 15;


  CccDccCtl0.Data = MrcReadCR (MrcData, CH0CCC_CR_DCCCTL0_REG);
  CccDccCtl0.Bits.dcdrovcoen = 1;
  MrcWriteCrMulticast (MrcData, CCC_CR_DCCCTL0_REG, CccDccCtl0.Data);

  MrcGetSetCccInst (MrcData, MAX_CCC_INSTANCES, GsmIocSrzDcdEnCcc,  WriteNoCache, &gGetSetEn);
  MrcGetSetCccInst (MrcData, MAX_CCC_INSTANCES, GsmIocMdllDcdEnCcc, WriteNoCache, &gGetSetEn);

  CccDccCtl2.Data = MrcReadCR (MrcData, CH0CCC_CR_DCCCTL2_REG);
  CccDccCtl2.Bits.dcc_serdata_ovren = 1;
  MrcWriteCrMulticast (MrcData, CCC_CR_DCCCTL2_REG, CccDccCtl2.Data);

  CccDccCtl4.Data = MrcReadCR (MrcData, CH0CCC_CR_DCCCTL4_REG);
  CccDccCtl4.Bits.dcc_bits_en = 0;
  MrcWriteCrMulticast (MrcData, CCC_CR_DCCCTL4_REG, CccDccCtl4.Data);

  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\nCCC");
  for (Channel = 0; Channel < MRC_NUM_CCC_INSTANCES; Channel++) {
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\t%u", Channel);
  }
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\n");

  // DtHalo: CCC[0..14]
  // UlxUlt: CCC[0..12]
  for (i = 1; i <= NumBuffers; i++) {
    CccDccCtl2.Bits.dcdsamplecountrst_b = 0;
    MrcWriteCrMulticast (MrcData, CCC_CR_DCCCTL2_REG, CccDccCtl2.Data);
    CccDccCtl2.Bits.dcdsamplecountrst_b = 1;
    MrcWriteCrMulticast (MrcData, CCC_CR_DCCCTL2_REG, CccDccCtl2.Data);

    GetSetVal = i;
    MrcGetSetCccInst (MrcData, MAX_CCC_INSTANCES, GsmIocDccDcdSampleSelCcc, WriteNoCache, &GetSetVal);

    CccDccCtl2.Bits.dcdsamplecountstart = 1;
    MrcWriteCrMulticast (MrcData, CCC_CR_DCCCTL2_REG, CccDccCtl2.Data);

    MrcWait (MrcData, 300 * MRC_TIMER_1US); // wait for totalsamplecounts number of roclk cycles

    CccDccCtl2.Bits.dcdsamplecountstart = 0;
    MrcWriteCrMulticast (MrcData, CCC_CR_DCCCTL2_REG, CccDccCtl2.Data);

    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "CCC%u", i - 1);
    for (Channel = 0; Channel < MRC_NUM_CCC_INSTANCES; Channel++) {
      Offset = OFFSET_CALC_CH (CH2CCC_CR_DCCCTL19_REG, CH0CCC_CR_DCCCTL19_REG, Channel);
      CccDccCtl19.Data  = MrcReadCR (MrcData, Offset);
      SampleCount = CccDccCtl19.Bits.dcdsamplecount;
      Data32 = 10000 * SampleCount;
      Data32 = UDIVIDEROUND (Data32, 2 * TotalCounts);  // Precision of 2 decimal points
      DataInt = Data32 / 100;
      MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\t%u.%02u", DataInt, Data32 - 100 * DataInt);
    } // Channel
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\n");
  } // i

  // Cleanup
  CccDccCtl0.Data = MrcReadCR (MrcData, CH0CCC_CR_DCCCTL0_REG);
  CccDccCtl0.Bits.dcdrovcoen = 0;
  MrcWriteCrMulticast (MrcData, CCC_CR_DCCCTL0_REG, CccDccCtl0.Data);

  MrcGetSetCccInst (MrcData, MAX_CCC_INSTANCES, GsmIocSrzDcdEnCcc,  WriteNoCache, &gGetSetDis);
  MrcGetSetCccInst (MrcData, MAX_CCC_INSTANCES, GsmIocMdllDcdEnCcc, WriteNoCache, &gGetSetDis);

  CccDccCtl2.Data = MrcReadCR (MrcData, CH0CCC_CR_DCCCTL2_REG);
  CccDccCtl2.Bits.dcc_serdata_ovren = 0;
  MrcWriteCrMulticast (MrcData, CCC_CR_DCCCTL2_REG, CccDccCtl2.Data);
}

#endif // MRC_DEBUG_PRINT

/**
  InitDCC calibration (dllldocomp.dcc_refclk, dllldocomp.dcc_vcdl, dccinit_fsm)

  @param[in, out] MrcData   - Include all MRC global data.
  @param[in]      DcdGroup  - see DCD_GROUP_xxx defines
  @param[in]      FubMask   - Run DCC init in this partition(s)
  @param[in]      RoFubMask - Run DCC RO calibration in this partition(s)

  @retval mrcSuccess
**/
MrcStatus
MrcInitDcc (
  IN OUT MrcParameters* const MrcData,
  IN UINT32                   DcdGroup,
  IN const DccFubMask         FubMask,
  IN const DccFubMask         RoFubMask
  )
{
  MrcDebug  *Debug;
  MrcOutput *Outputs;
  MrcStatus Status;
  UINT32    DcdBits;

#if defined(MRC_DEBUG_PRINT)
  MrcInput *Inputs  = &MrcData->Inputs;
#endif // MRC_DEBUG_PRINT
  Outputs = &MrcData->Outputs;
  Debug   = &Outputs->Debug;
  Status = mrcSuccess;
#ifdef MRC_DEBUG_PRINT
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\nInitDcc: %s --> ", (DcdGroup == DCD_GROUP_REFCLK) ? "REFCLK" : ((DcdGroup == DCD_GROUP_VCDL) ? "VCDL" : "SRZ"));
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "COMP: %u, DATA: %u, CCC: %u ", (FubMask & DccFubComp) ? 1 : 0, (FubMask & DccFubData) ? 1 : 0, (FubMask & DccFubCcc) ? 1 : 0);
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "RO_Comp: %u, RO_Data: %u, RO_Ccc: %u\n", (RoFubMask & DccFubComp) ? 1 : 0, (RoFubMask & DccFubData) ? 1 : 0, (RoFubMask & DccFubCcc) ? 1 : 0);
#endif // MRC_DEBUG_PRINT

  MrcDccSetup (MrcData, DcdGroup, FubMask);

  DcdBits = 0;
  MrcDccRun (MrcData, DcdGroup, FubMask, RoFubMask, DcdBits);

#if defined(MRC_DEBUG_PRINT)
  if (DcdGroup == DCD_GROUP_SRZ) {  // Serializer flow - print the final duty cycle per pin
    // Start MDLLs
    MrcOvrMdllEn (MrcData, TRUE, FubMask, TRUE);

    if (FubMask & DccFubData) {
      MrcDccPrintSrzResultsData (MrcData);
    }
    if (FubMask & DccFubCcc) {
      MrcDccPrintSrzResultsCcc (MrcData);
    }
    // Disable MDLLs
    MrcOvrMdllEn (MrcData, FALSE, FubMask, FALSE);
    if (Inputs->Q0 || Inputs->G0) {
      DqsDccWaQ0 (MrcData, FubMask);
    }
  }
#endif // MRC_DEBUG_PRINT

  return Status;
}

/**
  FLL DCC Calibration

  @param[in]  MrcData - Include all MRC global data.

  @retval mrcSuccess
**/
MrcStatus
MrcPhyInitFllInitDcc (
  IN MrcParameters *const MrcData
  )
{
  MrcStatus             Status;
  MrcInput              *Inputs;
  MrcDebug              *Debug;
  MRC_FUNCTION          *MrcCall;
  UINT64                Timeout;
  BOOLEAN               Done;
  UINT32                DccCodePh0_Index0Save;
  DDRPHY_COMP_CR_DLLCOMP_DCCCTRL_STRUCT   DllCompDccCtrl;
  DDRPHY_COMP_CR_DLLCOMP_LDOCTRL1_STRUCT  DllCompLdoCtrl1;
  DDRPHY_COMP_CR_DCCCTL4_STRUCT           DccCtl4;
  DDRPHY_COMP_NEW_CR_DDRCRFLLWIRED_STRUCT FllWired;
  FLL_CMD_CFG_REG_STRUCT                  FllCmdCfg;
  FLL_DIAG_STAT_REG_STRUCT                FllDiagStat;

  Status = mrcSuccess;
  Inputs = &MrcData->Inputs;
  Debug  = &MrcData->Outputs.Debug;
  MrcCall = Inputs->Call.Func;
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\nFLL DCC Calibration\n");

  // Disabling FLL refclk dcc codes to be muxed during DVFS comp
  DllCompDccCtrl.Data = MrcReadCR (MrcData, DDRPHY_COMP_CR_DLLCOMP_DCCCTRL_REG);
  DllCompDccCtrl.Bits.bonus28 = 0;
  MrcWriteCR (MrcData, DDRPHY_COMP_CR_DLLCOMP_DCCCTRL_REG, DllCompDccCtrl.Data);

  // Enable the Dvfs Mode En
  DllCompLdoCtrl1.Data = MrcReadCR (MrcData, DDRPHY_COMP_CR_DLLCOMP_LDOCTRL1_REG);
  DllCompLdoCtrl1.Bits.bonus43 = 1;
  MrcWriteCR (MrcData, DDRPHY_COMP_CR_DLLCOMP_LDOCTRL1_REG, DllCompLdoCtrl1.Data);

  // Save the calibrated value of the PLL based DLL RefClk DCC Code
  DccCtl4.Data = MrcReadCR (MrcData, DDRPHY_COMP_CR_DCCCTL4_REG);
  DccCodePh0_Index0Save = DccCtl4.Bits.dcccodeph0_index0;
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Initial PllRefclkDccCode: %d\n", DccCodePh0_Index0Save);
  // Resetting the value for dccode to 'h80
  DccCtl4.Bits.dcccodeph0_index0 = 0x80;
  MrcWriteCR (MrcData, DDRPHY_COMP_CR_DCCCTL4_REG, DccCtl4.Data);

  // Enable override mode for FLL
  FllWired.Data = MrcReadCR (MrcData, DDRPHY_COMP_NEW_CR_DDRCRFLLWIRED_REG);
  FllWired.Bits.fll_enable_src_sel = 1;
  MrcWriteCR (MrcData, DDRPHY_COMP_NEW_CR_DDRCRFLLWIRED_REG, FllWired.Data);

  FllCmdCfg.Data = MrcReadCR (MrcData, FLL_CMD_CFG_REG_REG);
  FllCmdCfg.Bits.FLL_LDO_ENABLE_OVRD_EN  = 1;
  FllCmdCfg.Bits.FLL_OUT_CLK_REQ_OVRD_EN = 1;
  MrcWriteCR (MrcData, FLL_CMD_CFG_REG_REG, FllCmdCfg.Data);

  MrcWait (MrcData, 50 * MRC_TIMER_1NS); // Wait 50ns

  // Enable LDO
  FllCmdCfg.Bits.FLL_LDO_ENABLE = 1;
  MrcWriteCR (MrcData, FLL_CMD_CFG_REG_REG, FllCmdCfg.Data);

  MrcWait (MrcData, 50 * MRC_TIMER_1NS); // Wait 50ns

  // FLL Enable
  FllCmdCfg.Bits.FLL_ENABLE = 1;
  MrcWriteCR (MrcData, FLL_CMD_CFG_REG_REG, FllCmdCfg.Data);

  MrcWait (MrcData, 100 * MRC_TIMER_1NS); // Wait 100ns

  // FLL OutClk Req asserted to 1
  FllCmdCfg.Bits.FLL_OUT_CLK_REQ = 1;
  MrcWriteCR (MrcData, FLL_CMD_CFG_REG_REG, FllCmdCfg.Data);

  MrcWait (MrcData, 100 * MRC_TIMER_1NS); // Wait 100ns

  Timeout = MrcCall->MrcGetCpuTime () + MRC_WAIT_TIMEOUT; // 10 Seconds
  Done = FALSE;
  do {
    FllDiagStat.Data = MrcReadCR (MrcData, FLL_DIAG_STAT_REG_REG);
    if (Inputs->SimicsFlag == 1) { // No Simics support for this FSM yet
      Done = TRUE;
      break;
    }
    Done = (FllDiagStat.Bits.FLL_OUTCLK_ACK == 1);
  } while (!Done && (MrcCall->MrcGetCpuTime () < Timeout));

  MrcWait (MrcData, 50 * MRC_TIMER_1NS); // Wait 50ns

  //---------------------------------------------
  // RefClk DCC init in COMP Partition
  //---------------------------------------------
  Status |= MrcInitDcc (MrcData, DCD_GROUP_REFCLK, DccFubComp, DccFubComp);

  FllCmdCfg.Bits.FLL_OUT_CLK_REQ         = 0; // FLL OutClk Req asserted to 0
  FllCmdCfg.Bits.FLL_ENABLE              = 0; // Disable FLL
  FllCmdCfg.Bits.FLL_LDO_ENABLE          = 0; // Disable LDO
  FllCmdCfg.Bits.FLL_LDO_ENABLE_OVRD_EN  = 0; // Disable override mode for FLL
  FllCmdCfg.Bits.FLL_OUT_CLK_REQ_OVRD_EN = 0; // Disable override mode for FLL
  MrcWriteCR (MrcData, FLL_CMD_CFG_REG_REG, FllCmdCfg.Data);

  FllWired.Bits.fll_enable_src_sel = 0; // Disable override mode for FLL
  MrcWriteCR (MrcData, DDRPHY_COMP_NEW_CR_DDRCRFLLWIRED_REG, FllWired.Data);

  DllCompLdoCtrl1.Bits.bonus43 = 0; // Disable the Dvfs Mode En
  MrcWriteCR (MrcData, DDRPHY_COMP_CR_DLLCOMP_LDOCTRL1_REG, DllCompLdoCtrl1.Data);

  // Save the calibrated value of the PLL based DLL RefClk DCC Code
  DccCtl4.Data = MrcReadCR (MrcData, DDRPHY_COMP_CR_DCCCTL4_REG);
  DllCompDccCtrl.Data = MrcReadCR (MrcData, DDRPHY_COMP_CR_DLLCOMP_DCCCTRL_REG);
  // FLL Refclk DCC Codes are split between two bonus registers
  // bonus2 for the lower nibble
  // bonus1 for the upper nibble
  DllCompDccCtrl.Bits.bonus2 = DccCtl4.Bits.dcccodeph0_index0 & 0xF;
  DllCompDccCtrl.Bits.bonus1 = (DccCtl4.Bits.dcccodeph0_index0 >> DDRPHY_COMP_CR_DLLCOMP_DCCCTRL_bonus1_WID) & 0xF;
  DllCompDccCtrl.Bits.bonus28 = 1;
  MrcWriteCR (MrcData, DDRPHY_COMP_CR_DLLCOMP_DCCCTRL_REG, DllCompDccCtrl.Data);
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Final FllRefclkDccCode: %d\n", DccCtl4.Bits.dcccodeph0_index0);

  DccCtl4.Bits.dcccodeph0_index0 = DccCodePh0_Index0Save; // restore PLL based MDLL refclk DCC codes
  MrcWriteCR (MrcData, DDRPHY_COMP_CR_DCCCTL4_REG, DccCtl4.Data);
  //MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Restore PllRefclkDccCode =%d\n", DccCodePh0_Index0Save);

  return Status;
}

/**
  This function returns the tCL delay separation needed from Receive Enable to the RxFifo Read Enable.
  This covers all internal logic delay in the path.

  @param[in]  MrcData - Pointer to global MRC data.
  @param[in]  Controller  - 0-based index instance to select.
  @param[in]  Channel     - 0-based index instance to select.
  @param[in]  DdrType     - Enumeration of MrcDdrType which DDR type is being enabled.
  @param[in]  GearRatio   - Integer number of the current Gear ratio.
**/
UINT8
MrcRcvEn2RxFifoReadTclDelay (
  IN MrcParameters *const MrcData,
  IN UINT32               Controller,
  IN UINT32               Channel,
  IN MrcDdrType           DdrType,
  IN UINT8                GearRatio
  )
{
  UINT8 RxDelay;
  UINT32 GearIndex;
  UINT32 SysChannel;

  GearIndex = ((GearRatio == 2) ? 1 : ((MrcData->Outputs.Gear4) ? 2 : 0));

  SysChannel = (Controller * MrcData->Outputs.MaxChannels) + Channel;

  if (MrcData->Inputs.UlxUlt) {
    RxDelay = RxFifoChDelay_UlxUlt[DdrType][GearIndex][SysChannel];
  } else {
    RxDelay = RxFifoChDelay[DdrType][GearIndex][SysChannel];
  }

  if (MrcData->Inputs.DtHalo) {
    if (Controller == 1) {
      // Need to increment by 1 for Rxfifo/RcvEn separation.
      RxDelay++;
    }
  }

  return RxDelay;
}

/**
  This function configures the DDR IO ODT type to Data and Comp blocks.
  VSS and VTT blocks are one time configured in MrcDdrIoInit.
  This updates OdtMode in MrcOutput.

  @param[in]  MrcData - Pointer to global MRC data.
  @param[in]  NewMode - ODT mode to enable.

  @retval MrcStatus - mrcSuccess if a valid ODT mode is requested, otherwise mrcWrongInputParameter.
**/
MrcStatus
MrcSetIoOdtMode (
  IN  MrcParameters *const  MrcData,
  IN  MRC_ODT_MODE_TYPE     NewMode
  )
{
//  static const UINT8  OdtModeLut[MrcOdtModeMax] = {2,2,1,0}; //MrcOdtModeDefault - unused, MrcOdtModeVtt, MrcOdtModeVddq, MrcOdtModeVss,
  const MrcInput      *Inputs;
  MrcDebug            *Debug;
  MrcOutput           *Outputs;
  MRC_ODT_MODE_TYPE   CurrentMode;
  INT64               VttMode;
//  INT64               VddqMode;
//  INT64               DataOdtMode;
  INT64               RxVrefValue;
  INT32               RxVrefMinValue;
  INT32               RxVrefMaxValue;
  INT64               GetRxVrefMin;
  INT64               GetRxVrefMax;
  UINT32              EffPullUp;
  UINT32              Idx;
  UINT32              PuDeltaV;
  UINT32              Offset;
  UINT32              Voh;
  BOOLEAN             Ddr4;
  BOOLEAN             Lpddr4;
  BOOLEAN             Lpddr4Vss;
  BOOLEAN             A0;
  DDRVTT0_CR_DDRCRVTTGENCONTROL_STRUCT  VttGenCtl;

  Inputs      = &MrcData->Inputs;
  Outputs     = &MrcData->Outputs;
  Debug       = &Outputs->Debug;
  CurrentMode = Outputs->OdtMode;
  Ddr4        = (Outputs->DdrType == MRC_DDR_TYPE_DDR4);
  Lpddr4      = (Outputs->DdrType == MRC_DDR_TYPE_LPDDR4);
  Lpddr4Vss   = (Lpddr4 && (NewMode == MrcOdtModeVss));
  A0          = Inputs->A0;

  if ((NewMode >= MrcOdtModeMax) || (NewMode == MrcOdtModeDefault)) {
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_ERROR, "Invalid ODT Mode requested: %d\n", NewMode);
    return mrcWrongInputParameter;
  }
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "New ODT Mode: %s\t Original ODT Mode: %s\n", gIoOdtModeStr[NewMode], gIoOdtModeStr[CurrentMode]);
  MrcGetSetLimits (MrcData, RxVref, &GetRxVrefMin, &GetRxVrefMax, NULL);
  RxVrefMinValue = (INT32) GetRxVrefMin;
  RxVrefMaxValue = (INT32) GetRxVrefMax;

  // default mode if no other mode is enabled.  Thus we clear all other modes.
  VttMode   = (NewMode == MrcOdtModeVtt)  ? 1 : 0;
//  VddqMode  = (NewMode == MrcOdtModeVddq) ? 1 : 0;
  if (NewMode == MrcOdtModeVss) {
    RxVrefValue = (RxVrefMaxValue + (3 * RxVrefMinValue)) / 4;  // VSS termination: RxVref = 0.25 * Vddq
  } else {
    RxVrefValue = (RxVrefMinValue + RxVrefMaxValue) / 2;  // Middle of the range for VTT termination
  }
//  DataOdtMode = OdtModeLut[NewMode];

//@todo_adl  MrcGetSetChStrb (MrcData, MAX_CONTROLLER, MAX_CHANNEL, MAX_SDRAM_IN_DIMM, GsmIocDataOdtMode,  WriteToCache, &DataOdtMode);
//@todo_adl  MrcGetSetChStrb (MrcData, MAX_CONTROLLER, MAX_CHANNEL, MAX_SDRAM_IN_DIMM, GsmIocRxVrefMFC,    WriteToCache, &VttMode);

  // Update RxVref to match the new termination type
  // DDR4 will be updated below using MrcSetDefaultRxVrefDdr4
  // LPDDR4 VSS termination will be updated below.
  if (!(Ddr4 || Lpddr4Vss)) {
    // @todo_adl This will be called for DDR5 as well, but RxVrefValue is not good
    MrcGetSetChStrb (MrcData, MAX_CONTROLLER, MAX_CHANNEL, MAX_SDRAM_IN_DIMM, RxVref, WriteToCache | PrintValue, &RxVrefValue);
  }

//@todo_adl   MrcGetSetNoScope (MrcData, GsmIocCompVddqOdtEn, WriteToCache, &VddqMode);
//@todo_adl   MrcGetSetNoScope (MrcData, GsmIocCompVttOdtEn,  WriteToCache, &VttMode);

  MrcFlushRegisterCachedData (MrcData);

  for (Idx = 0; Idx < DDRIO_MAX_VTT_GEN; Idx++) {
    if (A0) {
      Offset = DDRVTT0_CR_DDRCRVTTGENCONTROL_A0_REG + ((DDRVTT1_CR_DDRCRVTTGENCONTROL_A0_REG - DDRVTT0_CR_DDRCRVTTGENCONTROL_A0_REG) * Idx);
    } else {
      Offset = DDRVTT0_CR_DDRCRVTTGENCONTROL_REG + ((DDRVTT1_CR_DDRCRVTTGENCONTROL_REG - DDRVTT0_CR_DDRCRVTTGENCONTROL_REG) * Idx);
    }
    VttGenCtl.Data = MrcReadCR (MrcData, Offset);
    if (A0) {
      VttGenCtl.BitsA0.EnVttOdt = (UINT32) VttMode;
    } else {
      VttGenCtl.Bits.EnVttOdt = (UINT32) VttMode;
    }
    MrcWriteCR (MrcData, Offset, VttGenCtl.Data);
  }

  Outputs->OdtMode = NewMode;

  if (Ddr4) {
    MrcSetDefaultRxVrefDdr4 (MrcData, TRUE, MRC_PRINTS_ON, TRUE); // GetFromTable = TRUE, PhyInit = TRUE
  } else if (Lpddr4Vss) {
    Voh = (Outputs->Lp4x) ? 360 : 440;  // mV
    PuDeltaV = ((Outputs->Lp4x) ? VDD_0_60 : VDD_1_10) - Voh;
    EffPullUp = PuDeltaV * Outputs->RcompTarget[RdOdt];
    EffPullUp = DIVIDEROUND (EffPullUp, Voh);
    MrcSetIdealRxVref (MrcData, MAX_CONTROLLER, MAX_CHANNEL, EffPullUp, 80, Outputs->RcompTarget[RdOdt], TRUE);
  }

  return mrcSuccess;
}
/**
  This function configures the Date Invert Nibble feature in the Phy based on the Phy ODT configuration

  @param[in]  MrcData - Pointer to MRC global data.

  @retval MrcStatus - mrcFail if the ODT mode doesn't have a mapping for Data Invert Nibble.
  @retval MrcStatus - mrcSuccess otherwise.
**/
MrcStatus
MrcDataInvertNibble (
  IN  MrcParameters *const MrcData
  )
{/* @todo_adl
  MrcInput *Inputs;
  MrcOutput *Outputs;
  INT64 GetSetVal;
  INT32 ByteOff;
  INT32 ByteSel;
  UINT32 Controller;
  UINT32 Channel;
  UINT32 Byte;
  UINT32 ByteMax;
  UINT32 SwizOffset;
  UINT32 DataCtrl5Offset;
  UINT8 DniMask;
  UINT8 ByteRead;
  BOOLEAN A0;
  BOOLEAN Lpddr5;
  DATA_CR_DDRCRDATACONTROL5_STRUCT DataControl5;
  DATA_CR_DDRCRWRRETRAINSWIZZLECONTROL_STRUCT WrRetrainSwizzleControl;
  DDRSCRAM_CR_DDRMISCCONTROL7_STRUCT MiscControl7;

  Inputs = &MrcData->Inputs;
  Outputs = &MrcData->Outputs;
  A0 = Inputs->A0;
  ByteMax = (Inputs->UlxUlt) ? 8 : 10;
  Lpddr5 = (Outputs->DdrType == MRC_DDR_TYPE_LPDDR5);
  switch (Outputs->OdtMode) {
    case MrcOdtModeVtt:
      DniMask = (A0) ? 2 : (Lpddr5) ? 3 : 1;
      break;

    case MrcOdtModeVss:
      DniMask = 0;
      break;

    case MrcOdtModeVddq:
      DniMask = (A0) ? 1 : 3;
      break;

    default:
      MRC_DEBUG_MSG (&Outputs->Debug, MSG_LEVEL_ERROR, "Invalid ODT mode for DataInvertNibble: %u\n", Outputs->OdtMode);
      return mrcFail;
  }
*/
/*
  if (Inputs->SafeMode) {
    DniMask = 0;
  }
#ifdef HSLE_FLAG
  // HSLE wants the data invert nibble disabled.
  DniMask = 0;
#endif

  // Update the value across the entire phy regardless of population.  Logic won't run on non-populated channels.
  for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
    for (Channel = 0; Channel < Outputs->MaxChannels; Channel++) {
      for (Byte = 0; Byte < Outputs->SdramCount; Byte++) {
        GetSetVal = (!A0 && Lpddr5 && ((Byte % 2) == 0)) ? 0 : DniMask;
        MrcGetSetChStrb (MrcData, Controller, Channel, Byte, GsmIocDataInvertNibble, WriteToCache, &GetSetVal);
      }
    }
  }

  if (Inputs->ScramblerSupport) {
    MiscControl7.Data = MrcReadCR (MrcData, DDRSCRAM_CR_DDRMISCCONTROL7_REG);
    MiscControl7.Bits.DataInvertNibble = DniMask;
    MrcWriteCR (MrcData, DDRSCRAM_CR_DDRMISCCONTROL7_REG, MiscControl7.Data);
  }

  if (!A0) {
    for (Channel = 0; Channel < 2; Channel++) {
      for (Byte = 0; Byte < ByteMax; Byte++) {
        SwizOffset = OFFSET_CALC_MC_CH (DATA0CH0_CR_DDRCRWRRETRAINSWIZZLECONTROL_REG, DATA0CH1_CR_DDRCRWRRETRAINSWIZZLECONTROL_REG, Channel, DATA1CH0_CR_DDRCRWRRETRAINSWIZZLECONTROL_REG, Byte);
        WrRetrainSwizzleControl.Data = MrcReadCR (MrcData, SwizOffset);
        if (Outputs->Lpddr) {
          ByteRead = (UINT8) (((Byte % 2) << 1) | WrRetrainSwizzleControl.Bits.DataRetrain_ByteSel);
        } else {
          ByteRead = 0;
        }
*/
/*
        switch (ByteRead) {
          case 1:
            ByteOff = 1;
            break;

          case 3:
            ByteOff = -1;
            break;

          default:
          case 0:
          case 2:
            ByteOff = 0;
            break;
        }
        ByteSel = Byte + ByteOff;

        DataCtrl5Offset = OFFSET_CALC_MC_CH (DATA0CH0_CR_DDRCRDATACONTROL5_REG, DATA0CH1_CR_DDRCRDATACONTROL5_REG, Channel, DATA1CH0_CR_DDRCRDATACONTROL5_REG, ByteSel);
        DataControl5.Data = MrcReadCR (MrcData, DataCtrl5Offset);
        WrRetrainSwizzleControl.Bits.DataInvertNibble = DataControl5.Bits.DataInvertNibble;
        MrcWriteCR (MrcData, SwizOffset, WrRetrainSwizzleControl.Data);
      }
    }
  }

  return mrcSuccess;
  */ return mrcFail;
}

/**
  This routine returns the TX FIFO separation based on technology

  @param[in]  MrcData           - Pointer to MRC global data.

  @retval TxFifoSeparation.
**/

INT32
MrcGetTxFifoSeparation (
  IN OUT MrcParameters *const MrcData
  )
{
  INT32      TxFifoSeparation;
  BOOLEAN    Gear2;
  BOOLEAN    Gear4;
  BOOLEAN    Lpddr4;
  BOOLEAN    Lpddr5;
  BOOLEAN    Ddr5;
  BOOLEAN    Ddr4;
  BOOLEAN    UlxUlt;
  MrcOutput  *Outputs;
  MrcInput   *Inputs;
  MrcFrequency DdrFrequency;

  Outputs  = &MrcData->Outputs;
  Inputs   = &MrcData->Inputs;
  Lpddr5   = (Outputs->DdrType == MRC_DDR_TYPE_LPDDR5);
  Lpddr4   = (Outputs->DdrType == MRC_DDR_TYPE_LPDDR4);
  Ddr5     = (Outputs->DdrType == MRC_DDR_TYPE_DDR5);
  Ddr4     = (Outputs->DdrType == MRC_DDR_TYPE_DDR4);
  Gear2    = Outputs->Gear2;
  Gear4    = Outputs->Gear4;
  UlxUlt = Inputs->UlxUlt;
  DdrFrequency = Outputs->Frequency;

  TxFifoSeparation = 0;
  if (UlxUlt) {
    if (Lpddr4) {
      TxFifoSeparation = -2;
    } else if (Lpddr5) {
      TxFifoSeparation =  2;
      if (Gear4 && (DdrFrequency == f2400)) {
        TxFifoSeparation = -4;
      }
    } else if (Ddr5) {
      if (Gear4) {
        TxFifoSeparation = -2;
      } else {
        TxFifoSeparation = -3;
      }
    } else if (Ddr4) {
      TxFifoSeparation = -2;
    }
  } else {
    TxFifoSeparation -= 2;
    if (Lpddr4) {
      TxFifoSeparation -= (Gear2 ? 2 : 1);  // Subtract 1 due to 2tCK WRPRE
    } else if (Lpddr5) {
      TxFifoSeparation -= 1;
    } else if (Ddr5) {
      TxFifoSeparation -= 2;
    } else if (Ddr4 && Gear2) {
      TxFifoSeparation += 1;
    }
  }

  return TxFifoSeparation;
}

/**
  This routine computes the read and write pointers for Tx FIFO

  @param[in]  MrcData           - Pointer to MRC global data.
  @param[in]  Controller        - Controller to get timings
  @param[in]  Channel           - Controller to get timings
  @param[in]  tCWL              - Write Latency for current channel
  @param[in]  AddTcwl           - Current AddTcwl value
  @param[in]  DecTcwl           - Current DecTcwl value
  @param[out] tCWL4TxDqFifoWrEn - Pointer to the write TX DQ fifo delay
  @param[out] tCWL4TxDqFifoRdEn - Pointer to the read TX DQ fifo delay
**/
VOID
MrcGetTxDqFifoDelays (
  IN OUT MrcParameters *const MrcData,
  IN     UINT32        Controller,
  IN     UINT32        Channel,
  IN     INT32         tCWL,
  IN     UINT32        AddTcwl,
  IN     UINT32        DecTcwl,
     OUT INT64         *tCWL4TxDqFifoWrEn,
     OUT INT64         *tCWL4TxDqFifoRdEn
  )
{
  BOOLEAN    Gear2;
  BOOLEAN    Gear4;
  BOOLEAN    Lpddr;
  BOOLEAN    Lpddr5;
  BOOLEAN    Lpddr4;
  BOOLEAN    Ddr5;
  MrcOutput  *Outputs;
  INT32      Ddr5Cmd2N;
  MrcFrequency DdrFrequency;

  Outputs  = &MrcData->Outputs;
  Lpddr    = Outputs->Lpddr;
  Lpddr5   = (Outputs->DdrType == MRC_DDR_TYPE_LPDDR5);
  Ddr5     = (Outputs->DdrType == MRC_DDR_TYPE_DDR5);
  Lpddr4   = (Outputs->DdrType == MRC_DDR_TYPE_LPDDR4);
  Ddr5Cmd2N = (Ddr5 && (MrcGetNMode (MrcData) == 2)) ? 1 : 0;
  DdrFrequency = (Outputs->Lpddr && MrcData->Inputs.LpFreqSwitch) ? Outputs->HighFrequency : Outputs->Frequency;

  Gear2    = Outputs->Gear2;
  Gear4    = Outputs->Gear4;

  if (Gear2) {
    *tCWL4TxDqFifoWrEn = tCWL - (2 * DecTcwl) + (2 * AddTcwl) - (Lpddr ? 1 : 3) + (tCWL % 2) - 2 * Lpddr * (tCWL % 2);
    if (Lpddr5) {
      *tCWL4TxDqFifoWrEn -= 8;
    }
    if (Ddr5) {
      *tCWL4TxDqFifoWrEn += 2;
    }
  } else if (Gear4) {
    if (Lpddr5) {
      *tCWL4TxDqFifoWrEn = tCWL - (4 * DecTcwl) + (4 * AddTcwl) - 11;
       if (DdrFrequency == f2400) {
         *tCWL4TxDqFifoWrEn = tCWL - (4 * DecTcwl) + AddTcwl - 7;
       }
    } else if (Lpddr4) {
      *tCWL4TxDqFifoWrEn = tCWL - (4 * DecTcwl) + (4 * AddTcwl) - 5;
    } else { // DDR5
      //*tCWL4TxDqFifoWrEn = tCWL - 23 + (tCWL) % 4 - 3 * Ddr5Cmd2N;  // Add dec as 4*5
      *tCWL4TxDqFifoWrEn = tCWL - (4 * DecTcwl) + (4 * AddTcwl) - 3 + (tCWL % 4) - 3 * Ddr5Cmd2N;
    }
  } else { // Gear1
    *tCWL4TxDqFifoWrEn  = tCWL - DecTcwl + AddTcwl - 2; // TxDqFifoWrEnTcwlDelay(DClk)
  }
  *tCWL4TxDqFifoRdEn  = *tCWL4TxDqFifoWrEn + MrcGetTxFifoSeparation (MrcData);

  if (Lpddr4 && Gear4 && (DdrFrequency <= f3200) && (Controller == 1) ) {
    *tCWL4TxDqFifoRdEn = *tCWL4TxDqFifoRdEn - 4;
    *tCWL4TxDqFifoRdEn = MAX (*tCWL4TxDqFifoRdEn, 0);
  }

}

/*
  This function reads result of BwSelCalibration and programs CbEn value to Data and CCC MdllCtrl registers.

  @param[in] MrcData - Include all MRC global data.
*/
VOID
MrcCbEnDistribute (
  IN MrcParameters *const MrcData
)
{
  MrcOutput           *Outputs;
  UINT32              Rank;
  UINT32              Offset;
  UINT32              Strobe;
  UINT32              CbEn;
  UINT32              FirstController;
  UINT32              FirstChannel;
  BOOLEAN             A0;
  BOOLEAN             Q0Regs;

  DDRPHY_COMP_CR_DLLCOMP_VCDLCTRL_STRUCT          DllCompVcdlCtrl;
  DATA0CH0_CR_DQRXCTL0_STRUCT                     DataDqRxCtl0;
  CH0CCC_CR_MDLLCTL3_STRUCT                       CccMdllCtl3;
  DATA0CH0_CR_MDLLCTL3_STRUCT                     DataMdllCtl3;
  DDRPHY_COMP_CR_RXDQSCOMP_CMN_CTRL1_NEW_STRUCT   RxDqsCompCmnCtrl1New;
  DDRPHY_COMP_CR_VCCDDQLDO_DVFSCOMP_CTRL0_STRUCT  VccddqldoDvfscompCtrl0;

  Outputs         = &MrcData->Outputs;
  FirstController = Outputs->FirstPopController;
  FirstChannel    = Outputs->Controller[FirstController].FirstPopCh;
  A0              = MrcData->Inputs.A0;
  Q0Regs          = MrcData->Inputs.Q0Regs;

  // Read Values from register
  DllCompVcdlCtrl.Data = MrcReadCR (MrcData, DDRPHY_COMP_CR_DLLCOMP_VCDLCTRL_REG);
  CbEn  = DllCompVcdlCtrl.Bits.dllcomp_cmn_vcdlcben;

  // Because the step following this one enables the DLL and PI's in the data and CCC partitions, force distribute
  MrcForceDistCompCodes (MrcData);

  // Distribute cben to destinations

  // Read from first populated byte and broadcast to all
  Strobe = 0;    // Rank is not used here
  MrcTranslateSystemToIp (MrcData, &FirstController, &FirstChannel, &Rank, &Strobe, GsmIocTxOn); // No GetSet for this register, so use another per-byte PHY CR
  Offset  = DATA0CH0_CR_DQRXCTL0_REG +
           (DATA0CH1_CR_DQRXCTL0_REG - DATA0CH0_CR_DQRXCTL0_REG) * FirstChannel +
           (DATA1CH0_CR_DQRXCTL0_REG - DATA0CH0_CR_DQRXCTL0_REG) * Strobe;

  DataDqRxCtl0.Data = MrcReadCR (MrcData, Offset);
  DataDqRxCtl0.Bits.dqrx_cmn_rxmdqcben = CbEn;
  MrcWriteCrMulticast (MrcData, DATA_CR_DQRXCTL0_REG, DataDqRxCtl0.Data);

  Offset  = DATA0CH0_CR_MDLLCTL3_REG +
           (DATA0CH1_CR_MDLLCTL3_REG - DATA0CH0_CR_MDLLCTL3_REG) * FirstChannel +
           (DATA1CH0_CR_MDLLCTL3_REG - DATA0CH0_CR_MDLLCTL3_REG) * Strobe;

  DataMdllCtl3.Data = MrcReadCR (MrcData, Offset);
  DataMdllCtl3.Bits.mdll_cmn_vcdlcben    = CbEn;
  DataMdllCtl3.Bits.rxsdl_cmn_rxvcdlcben = CbEn;
  MrcWriteCrMulticast (MrcData, DATA_CR_MDLLCTL3_REG, DataMdllCtl3.Data);

  // Read from CCC0 and broadcast to CCC[0..7]
  Offset = A0 ? CH0CCC_CR_MDLLCTL3_A0_REG : CH0CCC_CR_MDLLCTL3_REG;
  CccMdllCtl3.Data = MrcReadCR (MrcData, Offset);
  CccMdllCtl3.Bits.mdll_cmn_vcdlcben = CbEn;
  Offset = A0 ? CCC_CR_MDLLCTL3_A0_REG : CCC_CR_MDLLCTL3_REG;
  MrcWriteCrMulticast (MrcData, Offset, CccMdllCtl3.Data);
  if (Q0Regs) {
    VccddqldoDvfscompCtrl0.Data = MrcReadCR (MrcData, DDRPHY_COMP_CR_VCCDDQLDO_DVFSCOMP_CTRL0_REG);
    VccddqldoDvfscompCtrl0.Bits.rxdqscomp_cmn_rxvcdlcben = CbEn;
    MrcWriteCR (MrcData, DDRPHY_COMP_CR_VCCDDQLDO_DVFSCOMP_CTRL0_REG, VccddqldoDvfscompCtrl0.Data);
  } else {
    RxDqsCompCmnCtrl1New.Data = MrcReadCR (MrcData, DDRPHY_COMP_CR_RXDQSCOMP_CMN_CTRL1_NEW_REG);
    RxDqsCompCmnCtrl1New.Bits.rxdqscomp_cmn_rxvcdlcben  = CbEn;
    MrcWriteCR (MrcData, DDRPHY_COMP_CR_RXDQSCOMP_CMN_CTRL1_NEW_REG, RxDqsCompCmnCtrl1New.Data);
  }
}


/**
  Run DCC for Dqs, and Diff Clk (CLK0, CLK1, WCK)

  @param[in] MrcData           - Include all MRC global data.
  @param[in] TotalSampleCounts - Total Sample Counts.
  @param[in] Group             - Group, can be GroupDccDqs, GroupDccClk0, GroupDccClk1,  GroupDccWck.

  @retval None
**/
VOID
RunDiffClkDcc (
  IN OUT MrcParameters *const MrcData,
  IN UINT32                   TotalSampleCounts,
  IN DCC_GROUP          const Group,
  IN BOOLEAN                  DccHiLoOverride,
  IN UINT32                   DqsDutyCyleThresholdHiInput[MAX_CONTROLLER][MAX_CHANNEL][MAX_SDRAM_IN_DIMM],
  IN UINT32                   DqsDutyCyleThresholdLoInput[MAX_CONTROLLER][MAX_CHANNEL][MAX_SDRAM_IN_DIMM],
  IN BOOLEAN                  Print
  )
{
  const MRC_FUNCTION *MrcCall;
  MrcInput           *Inputs;
  MrcOutput          *Outputs;
  MrcDebug           *Debug;
  UINT8              NumSamples;
  UINT32             MinThreshold;
  DCC_GROUP          SelClkInst = Group;
  DCC_ALIGN          DqsPrise[MAX_CONTROLLER][MAX_CHANNEL][MAX_SDRAM_IN_DIMM];
  DCC_ALIGN          DqsPfall[MAX_CONTROLLER][MAX_CHANNEL][MAX_SDRAM_IN_DIMM];
  DCC_ALIGN          ClkPrise[MRC_NUM_CCC_INSTANCES]; // Prise is relative to Nfall
  DCC_ALIGN          ClkPfall[MRC_NUM_CCC_INSTANCES]; // Pfall is relative to Nrise
  UINT32             CccDcdSampleCount[MRC_NUM_CCC_INSTANCES];
  UINT32             DqsDcdSampleCount[MAX_CONTROLLER][MAX_CHANNEL][MAX_SDRAM_IN_DIMM];
  UINT8              AlignCorrectionCount;
  UINT32             DqsDutyCyle[MAX_CONTROLLER][MAX_CHANNEL][MAX_SDRAM_IN_DIMM];
  UINT32             ClkDutyCyle[MRC_NUM_CCC_INSTANCES];
  BOOLEAN            DqsDccSaturate[MAX_CONTROLLER][MAX_CHANNEL][MAX_SDRAM_IN_DIMM];
  BOOLEAN            ClkDccSaturate[MRC_NUM_CCC_INSTANCES];
  UINT32             Index;
  UINT32             Controller;
  UINT32             Channel;
  UINT32             Strobe;
  UINT32             SavedParams[MAX_DCC_SAVED_PARAMETERS];
  UINT8              DqsTcoPRise[MAX_CONTROLLER][MAX_CHANNEL][MAX_SDRAM_IN_DIMM];
  UINT8              DqsTcoPFall[MAX_CONTROLLER][MAX_CHANNEL][MAX_SDRAM_IN_DIMM];
  UINT8              DqsTcoNRise[MAX_CONTROLLER][MAX_CHANNEL][MAX_SDRAM_IN_DIMM];
  UINT8              DqsTcoNFall[MAX_CONTROLLER][MAX_CHANNEL][MAX_SDRAM_IN_DIMM];
  UINT8              ClkTcoPRise[MRC_NUM_CCC_INSTANCES];
  UINT8              ClkTcoPFall[MRC_NUM_CCC_INSTANCES];
  UINT8              ClkTcoNRise[MRC_NUM_CCC_INSTANCES];
  UINT8              ClkTcoNFall[MRC_NUM_CCC_INSTANCES];
  UINT8              AlignCorrectionLimit;  // number of correction allowed for alignment loop, per DCC loop
  UINT8              DccCorrectionLimit;    // number of corrections allowed for DCC loop
  UINT8              DqsAlignPrisePrevSample[MAX_CONTROLLER][MAX_CHANNEL][MAX_SDRAM_IN_DIMM];// previous sample to detect dither
  UINT8              DqsAlignPfallPrevSample[MAX_CONTROLLER][MAX_CHANNEL][MAX_SDRAM_IN_DIMM];// previous sample to detect dither
  UINT8              ClkAlignPrisePrevSample[MRC_NUM_CCC_INSTANCES]; // previous sample to detect dither
  UINT8              ClkAlignPfallPrevSample[MRC_NUM_CCC_INSTANCES]; // previous sample to detect dither
  BOOLEAN            DqsPriseDone[MAX_CONTROLLER][MAX_CHANNEL][MAX_SDRAM_IN_DIMM]; // status to indicate that differential rise edge alignment is achieved
  BOOLEAN            DqsPfallDone[MAX_CONTROLLER][MAX_CHANNEL][MAX_SDRAM_IN_DIMM]; // status to indicate that differential fall edge alignment is achieved
  BOOLEAN            ClkPriseDone[MRC_NUM_CCC_INSTANCES]; // status to indicate that differential rise edge alignment is achieved
  BOOLEAN            ClkPfallDone[MRC_NUM_CCC_INSTANCES]; // status to indicate that differential fall edge alignment is achieved
  UINT8              AlignResultPassStrobe[MAX_CONTROLLER][MAX_CHANNEL][MAX_SDRAM_IN_DIMM];
  BOOLEAN            AlignResultPassCh[MAX_CONTROLLER][MAX_CHANNEL];
  BOOLEAN            AlignResultPassController[MAX_CONTROLLER];
  UINT8              AlignResultPassIndex[MRC_NUM_CCC_INSTANCES];
  UINT8              DccResultPassStrobe[MAX_CONTROLLER][MAX_CHANNEL][MAX_SDRAM_IN_DIMM];
  BOOLEAN            DccResultPassCh[MAX_CONTROLLER][MAX_CHANNEL];
  BOOLEAN            DccResultPassController[MAX_CONTROLLER];
  UINT8              DccResultPassIndex[MRC_NUM_CCC_INSTANCES];
  UINT8              DccResultPassIndexGlobal[MRC_NUM_CCC_INSTANCES];
  BOOLEAN            AllStrobePass = FALSE;
  BOOLEAN            AllControllerPass = FALSE;
  BOOLEAN            AllIndexPass = TRUE;
  BOOLEAN            DccAllControllerPass = TRUE;
  BOOLEAN            DccAllIndexPass = TRUE;
  BOOLEAN            DqsAlignPriseSaturate[MAX_CONTROLLER][MAX_CHANNEL][MAX_SDRAM_IN_DIMM];// saturation indicator for differential rise edge alignment
  BOOLEAN            DqsAlignPfallSaturate[MAX_CONTROLLER][MAX_CHANNEL][MAX_SDRAM_IN_DIMM];// saturation indicator for differential fall edge alignment
  BOOLEAN            ClkAlignPriseSaturate[MRC_NUM_CCC_INSTANCES];// saturation indicator for differential rise edge alignment
  BOOLEAN            ClkAlignPfallSaturate[MRC_NUM_CCC_INSTANCES];// saturation indicator for differential fall edge alignment
  BOOLEAN            AlignOnly = FALSE; // specifies whether to run alignment only or alignment with DCC, DCC support only added to DQS in C0/Q0 stepping
  UINT32             DqsDutyCyclePrevSample[MAX_CONTROLLER][MAX_CHANNEL][MAX_SDRAM_IN_DIMM]; // previous DC to detect dither
  UINT32             ClkDutyCyclePrevSample[MRC_NUM_CCC_INSTANCES]; // previous DC to detect dither
  DCC_STATUS         DqsDccStatus[MAX_CONTROLLER][MAX_CHANNEL][MAX_SDRAM_IN_DIMM]; // tracks the result of the DCC measurement. Can be 'low', 'high', or 'align'
  DCC_STATUS         ClkDccStatus[MRC_NUM_CCC_INSTANCES]; // tracks the result of the DCC measurement. Can be 'low', 'high', or 'align'
  BOOLEAN            Enable = TRUE;
  BOOLEAN            AllChPass;
  UINT8              DccAlignCorrectionLimit;
  UINT8              DccAlignCorrectionCount = 0;
  UINT8              DccCorrectionCount = 0;
  UINT8              TruePassCount = 0;
  UINT8              SaturateCount = 0;
  UINT8              NcClkCount = 0;
  BOOLEAN            SaturateFalsePass = FALSE;
  UINT32             TcopRiseSave[MRC_NUM_CCC_INSTANCES][2];
  UINT32             TcopFallSave[MRC_NUM_CCC_INSTANCES][2];
  UINT32             TconRiseSave[MRC_NUM_CCC_INSTANCES][2];
  UINT32             TconFallSave[MRC_NUM_CCC_INSTANCES][2];
  UINT8              TcoInitValue;
  UINT32             DqsDutyCyleThresholdHi[MAX_CONTROLLER][MAX_CHANNEL][MAX_SDRAM_IN_DIMM];
  UINT32             DqsDutyCyleThresholdLo[MAX_CONTROLLER][MAX_CHANNEL][MAX_SDRAM_IN_DIMM];

  Inputs  = &MrcData->Inputs;
  MrcCall = Inputs->Call.Func;
  Outputs = &MrcData->Outputs;
  //Debug   = &Outputs->Debug;
  Debug   = (Print) ? &Outputs->Debug : NULL;
  MrcCall->MrcSetMem ((UINT8 *) SavedParams, sizeof (SavedParams), 0);
  MrcCall->MrcSetMem ((UINT8 *) DqsPrise, sizeof (DqsPrise), 0);
  MrcCall->MrcSetMem ((UINT8 *) DqsPfall, sizeof (DqsPfall), 0);
  MrcCall->MrcSetMem ((UINT8 *) ClkPrise, sizeof (ClkPrise), 0);
  MrcCall->MrcSetMem ((UINT8 *) ClkPfall, sizeof (ClkPfall), 0);
  MrcCall->MrcSetMem ((UINT8 *) DqsDutyCyle, sizeof (DqsDutyCyle), 0);
  MrcCall->MrcSetMem ((UINT8 *) ClkDutyCyle, sizeof (ClkDutyCyle), 0);
  MrcCall->MrcSetMem ((UINT8 *) DqsDccSaturate, sizeof (DqsDccSaturate), 0);
  MrcCall->MrcSetMem ((UINT8 *) ClkDccSaturate, sizeof (ClkDccSaturate), 0);
  MrcCall->MrcSetMem ((UINT8 *) DqsDccStatus, sizeof (DqsDccStatus), 0);
  MrcCall->MrcSetMem ((UINT8 *) ClkDccStatus, sizeof (ClkDccStatus), 0);
  MrcCall->MrcSetMem ((UINT8 *) DqsDutyCyclePrevSample, sizeof (DqsDutyCyclePrevSample), 0);
  MrcCall->MrcSetMem ((UINT8 *) ClkDutyCyclePrevSample, sizeof (ClkDutyCyclePrevSample), 0);
  MrcCall->MrcSetMem ((UINT8 *) TcopRiseSave, sizeof (TcopRiseSave), 0);
  MrcCall->MrcSetMem ((UINT8 *) TcopFallSave, sizeof (TcopFallSave), 0);
  MrcCall->MrcSetMem ((UINT8 *) TconRiseSave, sizeof (TconRiseSave), 0);
  MrcCall->MrcSetMem ((UINT8 *) TconFallSave, sizeof (TconFallSave), 0);
  SaveDiffDccClkCodes (MrcData, TcopRiseSave, TcopFallSave, TconRiseSave, TconFallSave);

  DccCorrectionLimit = 5;
  AlignCorrectionLimit = 6;
  DccAlignCorrectionLimit = 6;

  if ((Group == GroupDccDqs) && (((Inputs->A0) || (Inputs->B0)) || (Inputs->J0) || (Inputs->Q0) || (Inputs->G0))) {
    AlignOnly = TRUE;
  } else {
    AlignOnly = FALSE;
  }

  TcoInitValue = MAX_DCC_TCO_CODE;

  if (AlignOnly == TRUE) {
    //
    // Skip differential DCC for DQS on steppings prior to C0. Init for DQS and CLK
    //
    if ((Inputs->A0) || (Inputs->B0) || (Inputs->J0)) {
      TcoInitValue        = 0xf;
    }
    AlignCorrectionLimit  = 15;  // Alignment only only requires one long loop for the inner alignment loop
    DccCorrectionLimit    = 1;     // only single DCC loop for align only (i.e. no DCC)
  }
  //
  // Init for DQS
  //
  MrcCall->MrcSetMem ((UINT8 *) DqsTcoPRise, sizeof (DqsTcoPRise), TcoInitValue);
  MrcCall->MrcSetMem ((UINT8 *) DqsTcoPFall, sizeof (DqsTcoPFall), TcoInitValue);
  MrcCall->MrcSetMem ((UINT8 *) DqsTcoNRise, sizeof (DqsTcoNRise), TcoInitValue);
  MrcCall->MrcSetMem ((UINT8 *) DqsTcoNFall, sizeof (DqsTcoNFall), TcoInitValue);

  MrcCall->MrcSetMem ((UINT8 *) DqsDutyCyleThresholdHi, sizeof (DqsDutyCyleThresholdHi), 0);
  MrcCall->MrcSetMem ((UINT8 *) DqsDutyCyleThresholdLo, sizeof (DqsDutyCyleThresholdLo), 0);
  //
  // Init for CLK
  //
  MrcCall->MrcSetMem ((UINT8 *) ClkTcoPRise, sizeof (ClkTcoPRise), TcoInitValue);
  MrcCall->MrcSetMem ((UINT8 *) ClkTcoPFall, sizeof (ClkTcoPFall), TcoInitValue);
  MrcCall->MrcSetMem ((UINT8 *) ClkTcoNRise, sizeof (ClkTcoNRise), TcoInitValue);
  MrcCall->MrcSetMem ((UINT8 *) ClkTcoNFall, sizeof (ClkTcoNFall), TcoInitValue);
  //
  // Setup Differential Clock DCC for Data and CLK.
  //
  if (Group == GroupDccDqs) {
    if (DccHiLoOverride == TRUE) {
      for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
        for (Channel = 0; Channel < MAX_CHANNEL; Channel++) {
          for (Strobe = 0; Strobe < Outputs->SdramCount; Strobe++) {
            DqsDutyCyleThresholdHi[Controller][Channel][Strobe] = DqsDutyCyleThresholdHiInput[Controller][Channel][Strobe];
            DqsDutyCyleThresholdLo[Controller][Channel][Strobe] = DqsDutyCyleThresholdLoInput[Controller][Channel][Strobe];
          }
        }
      }

    } else {

      for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
        for (Channel = 0; Channel < MAX_CHANNEL; Channel++) {
          for (Strobe = 0; Strobe < Outputs->SdramCount; Strobe++) {
            DqsDutyCyleThresholdHi[Controller][Channel][Strobe] = DQS_DCC_THRESHOLD_HI;
            DqsDutyCyleThresholdLo[Controller][Channel][Strobe] = DQS_DCC_THRESHOLD_LO;
          }
        }
      }

    }
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_ERROR, "MC0: Dqs Duty Cyle Threshold Hi = %d, Dqs Duty Cyle Threshold Lo = %d\n", DqsDutyCyleThresholdHi[0][0][0], DqsDutyCyleThresholdLo[0][0][0]);
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_ERROR, "MC1: Dqs Duty Cyle Threshold Hi = %d, Dqs Duty Cyle Threshold Lo = %d\n", DqsDutyCyleThresholdHi[1][0][0], DqsDutyCyleThresholdLo[1][0][0]);

    Enable = TRUE;
    EnterDqsDiffDcc (MrcData, Enable, TotalSampleCounts, SavedParams);
    SelectDccAccDcdData (MrcData, DccDqs);
  } else if (Group == GroupDccClk0 || Group == GroupDccClk1 || Group == GroupDccWck) {
    //
    // Accumulator selection is done in the loop
    //
    Enable = TRUE;
    EnterClkDiffDcc (MrcData, Enable, TotalSampleCounts, Group, SavedParams);
    SelClkInst = Group;
  } else {
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_ERROR, "code error: invalid group selected\n");
    return;
  }

  NumSamples = ALIGN_NUM_SAMPLES;
  MinThreshold = ALIGN_SAMPLE_THRESHOLD;

  MrcCall->MrcSetMem ((UINT8 *) DccResultPassStrobe, sizeof (DccResultPassStrobe), 0);
  MrcCall->MrcSetMem ((UINT8 *) DccResultPassCh, sizeof (DccResultPassCh), 0);
  MrcCall->MrcSetMem ((UINT8 *) DccResultPassController, sizeof (DccResultPassController), 0);
  MrcCall->MrcSetMem ((UINT8 *) DccResultPassIndexGlobal, sizeof (DccResultPassIndexGlobal), 0);

  // MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "TotalSampleCounts %d\n", TotalSampleCounts);
  //
  // DCC Loop with software watchdog limit
  //
  for (DccAlignCorrectionCount = 0;  DccAlignCorrectionCount < DccAlignCorrectionLimit; DccAlignCorrectionCount++) {
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "DccAlignCorrectionCount %d \n", DccAlignCorrectionCount);
    MrcCall->MrcSetMem ((UINT8 *) DccResultPassIndex, sizeof (DccResultPassIndex), 0);
    //
    // Reset clock alignment variables
    //
    MrcCall->MrcSetMem ((UINT8 *) DqsAlignPrisePrevSample, sizeof (DqsAlignPrisePrevSample), 0);
    MrcCall->MrcSetMem ((UINT8 *) DqsAlignPfallPrevSample, sizeof (DqsAlignPfallPrevSample), 0);
    MrcCall->MrcSetMem ((UINT8 *) ClkAlignPrisePrevSample, sizeof (ClkAlignPrisePrevSample), 0);
    MrcCall->MrcSetMem ((UINT8 *) ClkAlignPfallPrevSample, sizeof (ClkAlignPfallPrevSample), 0);

    MrcCall->MrcSetMem ((UINT8 *) DqsPriseDone, sizeof (DqsPriseDone), 0);
    MrcCall->MrcSetMem ((UINT8 *) DqsPfallDone, sizeof (DqsPfallDone), 0);
    MrcCall->MrcSetMem ((UINT8 *) ClkPriseDone, sizeof (ClkPriseDone), 0);
    MrcCall->MrcSetMem ((UINT8 *) ClkPfallDone, sizeof (ClkPfallDone), 0);

    MrcCall->MrcSetMem ((UINT8 *) DqsAlignPriseSaturate, sizeof (DqsAlignPriseSaturate), 0);
    MrcCall->MrcSetMem ((UINT8 *) DqsAlignPfallSaturate, sizeof (DqsAlignPfallSaturate), 0);
    MrcCall->MrcSetMem ((UINT8 *) ClkAlignPriseSaturate, sizeof (ClkAlignPriseSaturate), 0);
    MrcCall->MrcSetMem ((UINT8 *) ClkAlignPfallSaturate, sizeof (ClkAlignPfallSaturate), 0);

    MrcCall->MrcSetMem ((UINT8 *) AlignResultPassStrobe, sizeof (AlignResultPassStrobe), 0);
    MrcCall->MrcSetMem ((UINT8 *) AlignResultPassCh, sizeof (AlignResultPassCh), 0);
    MrcCall->MrcSetMem ((UINT8 *) AlignResultPassController, sizeof (AlignResultPassController), 0);
    MrcCall->MrcSetMem ((UINT8 *) AlignResultPassIndex, sizeof (AlignResultPassIndex), 0);

    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Align NumSamples %d MinThreshold %d AlignOnly %d\n", NumSamples, MinThreshold, AlignOnly);
    //
    // alignment loop with watchdog limit
    //
    for (AlignCorrectionCount = 0; AlignCorrectionCount < AlignCorrectionLimit; AlignCorrectionCount++) {
      //
      // get complimentary clock alignment sample
      //
      MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "AlignCorrectionCount %d \n", AlignCorrectionCount);

      if (Group == GroupDccDqs) {
        GetPDSampleDqs (MrcData, NumSamples, DqsPrise, DqsPfall, MinThreshold, Print);
        MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\nDQS Alignment Start\n");
        AllControllerPass = TRUE;
        for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
          AllChPass = TRUE;
          if (AlignResultPassController[Controller] == 1) {
            continue;
          }
          for (Channel = 0; Channel < MAX_CHANNEL; Channel++) {
            if (!MrcChannelExist (MrcData, Controller, Channel)) {
              continue;
            }
            if (AlignResultPassCh[Controller][Channel] == 1) {
              continue;
            }
            AllStrobePass = TRUE;
            for (Strobe = 0; Strobe < Outputs->SdramCount; Strobe++) {
              if (AlignResultPassStrobe[Controller][Channel][Strobe] == 1) {
                continue;
              }
              MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Mc%u.C%u.S%u ", Controller, Channel, Strobe);
              //
              // Determine whether clock alignment is done for differential rise and differential fall
              //
              if (DqsPrise[Controller][Channel][Strobe] == DccAlignDone) {
                //
                // detect align
                //
                DqsPriseDone[Controller][Channel][Strobe] = TRUE;
                MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "DQS Prise Align Found  ");
              } else if ((DqsPrise[Controller][Channel][Strobe] == DccAlignEarly)
                         && (DqsAlignPrisePrevSample[Controller][Channel][Strobe] == DccAlignLate)) {
                //
                // detect dither
                //
                DqsPriseDone[Controller][Channel][Strobe] = TRUE;
                MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "DQS Prise Dither Found  ");
              } else if (DqsAlignPriseSaturate[Controller][Channel][Strobe] == TRUE) {
                //
                // detect saturate
                //
                DqsPriseDone[Controller][Channel][Strobe] = TRUE;
                MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "DQS Prise Saturate Found  ");
              }

              if (DqsPfall[Controller][Channel][Strobe] == DccAlignDone) {
                //
                // detect align
                //
                DqsPfallDone[Controller][Channel][Strobe] = TRUE;
                MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "DQS Pfall Align Found  ");
              } else if ((DqsPfall[Controller][Channel][Strobe] == DccAlignEarly)
                         && (DqsAlignPfallPrevSample[Controller][Channel][Strobe] == DccAlignLate)) {
                //
                // detect dither
                //
                DqsPfallDone[Controller][Channel][Strobe] = TRUE;
                MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "DQS Pfall Dither Found  ");
              } else if (DqsAlignPfallSaturate[Controller][Channel][Strobe] == TRUE) {
                //
                // detect saturate
                //
                DqsPfallDone[Controller][Channel][Strobe] = TRUE;
                MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "DQS Pfall Saturate Found  ");
              }
              //
              // determine next tco codes to try if edge alignment is not complete
              //
              if (DqsPriseDone[Controller][Channel][Strobe] == FALSE) {
                DqsAlignPriseSaturate[Controller][Channel][Strobe] = AdjustTcoCodesForAlign(MrcData, &DqsTcoPRise[Controller][Channel][Strobe], &DqsTcoNFall[Controller][Channel][Strobe], DqsPrise[Controller][Channel][Strobe], AlignOnly);// determine next prise/nfall codes
              }
              if (DqsPfallDone[Controller][Channel][Strobe] == FALSE) {
                DqsAlignPfallSaturate[Controller][Channel][Strobe] = AdjustTcoCodesForAlign(MrcData, &DqsTcoPFall[Controller][Channel][Strobe], &DqsTcoNRise[Controller][Channel][Strobe], DqsPfall[Controller][Channel][Strobe], AlignOnly); // determine next pfall/nrise codes
              }
              //
              // check whether clock alignment is complete for both differential edges. If done, then break complimentary edge alignment loop.
              //
              if (((DqsPriseDone[Controller][Channel][Strobe] == TRUE) || (DqsAlignPriseSaturate[Controller][Channel][Strobe] == TRUE)) &&
                  ((DqsPfallDone[Controller][Channel][Strobe] == TRUE) || (DqsAlignPfallSaturate[Controller][Channel][Strobe] == TRUE))) {
                AlignResultPassStrobe[Controller][Channel][Strobe] = 1;
                MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Pass\n");
              } else { // alignment loop is not done
                //
                // program new tco codes
                //
                AllStrobePass = FALSE;
                SetDiffDccDqsCodes (
                  MrcData,
                  DqsTcoPRise[Controller][Channel][Strobe],
                  DqsTcoPFall[Controller][Channel][Strobe],
                  DqsTcoNRise[Controller][Channel][Strobe],
                  DqsTcoNFall[Controller][Channel][Strobe],
                  AlignOnly,
                  Controller,
                  Channel,
                  Strobe,
                  Print
                );

                //
                // Assign new sample as previous sample for dither detection
                //
                DqsAlignPrisePrevSample[Controller][Channel][Strobe] = DqsPrise[Controller][Channel][Strobe];
                DqsAlignPfallPrevSample[Controller][Channel][Strobe] = DqsPfall[Controller][Channel][Strobe];
              } //else
            } //strobe
            if (AllStrobePass == TRUE) {
              AlignResultPassCh[Controller][Channel] = 1;
            }
            if (AlignResultPassCh[Controller][Channel] == 0) {
              AllChPass = FALSE;
            }
          } //channel
          if (AllChPass == TRUE) {
            AlignResultPassController[Controller] = 1;
          }
          if (AlignResultPassController[Controller] == 0) {
            AllControllerPass = FALSE;
          }
        }//controller
        if (AllControllerPass == TRUE) {
          break;
        }
      } else if ((Group == GroupDccClk0) || (Group == GroupDccClk1) || (Group == GroupDccWck)) {
        MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\nCLK %d Alignment Start\n", Group - 1);
        AllIndexPass = TRUE;
        GetPDSampleClk (MrcData, SelClkInst, NumSamples, ClkPrise, ClkPfall, MinThreshold);

        for (Index = 0; Index < MRC_NUM_CCC_INSTANCES; Index++) {
          if (AlignResultPassIndex[Index] == 1) {
            continue;
          }
          DdrIoTranslateCccToMcChannel (MrcData, Index, &Controller, &Channel);
          if (!MrcChannelExist (MrcData, Controller, Channel)) {
            continue;
          }
          if (DccResultPassIndexGlobal[Index] == 1) {
            continue;
          }
          MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "CCC%u ", Index);
          //
          // Determine whether clock alignment is done for differential rise and differential fall
          //
          if (ClkPrise[Index] == DccAlignDone) {
            //
            // detect align
            //
            ClkPriseDone[Index] = TRUE;
            MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "ClkPrise Align");
          } else if ((ClkPrise[Index] == DccAlignEarly) && (ClkAlignPrisePrevSample[Index] == DccAlignLate)) {
            //
            // detect dither
            //
            ClkPriseDone[Index] = TRUE;
            MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "ClkPrise Dither");
          } else if (ClkAlignPriseSaturate[Index] == TRUE) {
            //
            // detect saturate
            // the duty cycle is already 50%, and can't do any more clk alignment.
            //
            ClkPriseDone[Index] = TRUE;
            MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "ClkPrise Saturate");
          }

          if (ClkPfall[Index] == DccAlignDone) {
            //
            // detect align
            //
            ClkPfallDone[Index] = TRUE;
            MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "ClkPfall Align");
          } else if ((ClkPfall[Index] == DccAlignEarly) && (ClkAlignPfallPrevSample[Index] == DccAlignLate)) {
            //
            // detect dither
            //
            ClkPfallDone[Index] = TRUE;
            MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "ClkPfall Dither");
          } else if (ClkAlignPfallSaturate[Index] == TRUE) {
            //
            // detect saturate
            //
            ClkPfallDone[Index] = TRUE;
            MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "ClkPfall Saturate");
          }
          //
          // determine next tco codes to try if edge alignment is not complete
          //
          if (ClkPriseDone[Index] == FALSE) {
            ClkAlignPriseSaturate[Index] = AdjustTcoCodesForAlign(MrcData, &ClkTcoPRise[Index], &ClkTcoNFall[Index],
                                           ClkPrise[Index], AlignOnly);// determine next prise/nfall codes
          }
          if (ClkPfallDone[Index] == FALSE) {
            ClkAlignPfallSaturate[Index] = AdjustTcoCodesForAlign(MrcData, &ClkTcoPFall[Index], &ClkTcoNRise[Index],
                                           ClkPfall[Index], AlignOnly); // determine next pfall/nrise codes
          }
          //
          // check whether clock alignment is complete for both differential edges. If done, then break complimentary edge alignment loop.
          //
          if (((ClkPriseDone[Index] == TRUE) || (ClkAlignPriseSaturate[Index] == TRUE)) &&
              ((ClkPfallDone[Index] == TRUE) || (ClkAlignPfallSaturate[Index] == TRUE))) {
            AlignResultPassIndex[Index] = 1;
            MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Pass\n");
          } else { // alignment loop is not done
            //
            // program new tco codes
            //
            AllIndexPass = FALSE;
            SetDiffDccClkCodes (MrcData, ClkTcoPRise[Index], ClkTcoPFall[Index], ClkTcoNRise[Index], ClkTcoNFall[Index], SelClkInst, Index);

            //
            // Assign new sample as previous sample for dither detection
            //
            ClkAlignPrisePrevSample[Index] = ClkPrise[Index];
            ClkAlignPfallPrevSample[Index] = ClkPfall[Index];
          } //else
        } //Index loop
        if (AllIndexPass == TRUE) {
          break;
        }
      }
    }

    if (AlignOnly == TRUE) {

      if (((Group == GroupDccDqs) && (AllControllerPass == TRUE)) ||
          (((Group == GroupDccClk0) || (Group == GroupDccClk1) || (Group == GroupDccWck)) && (AllIndexPass == TRUE))) {
        //
        // break out of DCC loop if alignment is complete
        //
        break;
      }
    } else { // do differential clock DCC
      //
      // else keep repeating alignment until done or DccCorrectionLimit has been reached
      // measure differential duty cycle
      //
      SaturateFalsePass = FALSE;
      for (DccCorrectionCount = DCC_INITIAL_LOOP_COUNT; DccCorrectionCount < DccCorrectionLimit; DccCorrectionCount++) {
        MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "DccCorrectionCount %d\n", DccCorrectionCount);
        if (Group == GroupDccDqs) {
          RunDccAccData (MrcData, TotalSampleCounts, DqsDutyCyle, DqsDcdSampleCount);

          DccAllControllerPass = TRUE;
          for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
            AllChPass = TRUE;
            if (DccResultPassController[Controller] == 1) {
              continue;
            }
            for (Channel = 0; Channel < MAX_CHANNEL; Channel++) {
              if (!MrcChannelExist (MrcData, Controller, Channel)) {
                continue;
              }
              if (DccResultPassCh[Controller][Channel] == 1) {
                continue;
              }
              AllStrobePass = TRUE;
              for (Strobe = 0; Strobe < Outputs->SdramCount; Strobe++) {
                if (DccResultPassStrobe[Controller][Channel][Strobe] == 1) {
                  continue;
                }
                MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "MC %d Ch %d Strobe %d Duty cycle is %d \n", Controller, Channel, Strobe,
                               DqsDutyCyle[Controller][Channel][Strobe]);
                //
                // determine whether differential clock DCC is complete by comparing duty cycle with thresholds and checking saturation
                // edge alignment used as a precondition for DCC exit
                //

                if ((((DqsDutyCyle[Controller][Channel][Strobe])) <= DqsDutyCyleThresholdHi[Controller][Channel][Strobe])
                    && (((DqsDutyCyle[Controller][Channel][Strobe])) >=
                        DqsDutyCyleThresholdLo[Controller][Channel][Strobe])) {
                  //
                  //  check whether duty cycle falls within upper and lower thresholds
                  //
                  DccResultPassStrobe[Controller][Channel][Strobe] = 1;
                  continue;
                } else if (((((DqsDutyCyle[Controller][Channel][Strobe])) > DqsDutyCyleThresholdHi[Controller][Channel][Strobe])
                           && (DqsDutyCyclePrevSample[Controller][Channel][Strobe] < DqsDutyCyleThresholdLo[Controller][Channel][Strobe]))
                           && (DccCorrectionCount != DCC_INITIAL_LOOP_COUNT)) {
                  //
                  // duty cycle dithered high condition
                  //
                  DccResultPassStrobe[Controller][Channel][Strobe] = 1;
                  continue;
                }

                DqsDutyCyclePrevSample[Controller][Channel][Strobe] = DqsDutyCyle[Controller][Channel][Strobe];
                //
                // determine next tco codes in order to converge DCC
                //
                if (((DqsDutyCyle[Controller][Channel][Strobe])) < DqsDutyCyleThresholdLo[Controller][Channel][Strobe]) { //to update data dcc
                  DqsDccStatus[Controller][Channel][Strobe] = DccLow;
                } else if (((DqsDutyCyle[Controller][Channel][Strobe])) > DqsDutyCyleThresholdHi[Controller][Channel][Strobe]) {
                  DqsDccStatus[Controller][Channel][Strobe] = DccHigh;
                } else {
                  DqsDccStatus[Controller][Channel][Strobe] = DccStatusAlign;
                  DccResultPassStrobe[Controller][Channel][Strobe] = 1;
                  continue;
                }
                //
                // determine next tco codes for differential clock DCC
                //
                DqsDccSaturate[Controller][Channel][Strobe] = AdjustTcoCodesForDcc (MrcData, &DqsTcoPRise[Controller][Channel][Strobe],
                    &DqsTcoPFall[Controller][Channel][Strobe], &DqsTcoNRise[Controller][Channel][Strobe],
                    &DqsTcoNFall[Controller][Channel][Strobe], DqsDccStatus[Controller][Channel][Strobe]);
                //
                // if tcocodes are saturated after both edge alignment and attempting to program next DCC codes, then exit
                //
                if ((DqsDccSaturate[Controller][Channel][Strobe] == TRUE)) {
                  DccResultPassStrobe[Controller][Channel][Strobe] = 1;
                  continue;
                } else {
                  //
                  // program new tco codes
                  //
                  SetDiffDccDqsCodes (
                    MrcData,
                    DqsTcoPRise[Controller][Channel][Strobe],
                    DqsTcoPFall[Controller][Channel][Strobe],
                    DqsTcoNRise[Controller][Channel][Strobe],
                    DqsTcoNFall[Controller][Channel][Strobe],
                    AlignOnly,
                    Controller,
                    Channel,
                    Strobe,
                    Print
                  );
                }
                if (DccResultPassStrobe[Controller][Channel][Strobe] == 0) {
                  AllStrobePass = FALSE;
                }
              } //strobe
              if (AllStrobePass == TRUE) {
                DccResultPassCh[Controller][Channel] = 1;
              }
              if (DccResultPassCh[Controller][Channel] == 0) {
                AllChPass = FALSE;
              }
            } //channel
            if (AllChPass == TRUE) {
              DccResultPassController[Controller] = 1;
            }
            if (DccResultPassController[Controller] == 0) {
              DccAllControllerPass = FALSE;
            }
          } //controller
          if (DccAllControllerPass == TRUE) {
            break;
          }
        } else if ((Group == GroupDccClk0) || (Group == GroupDccClk1) || (Group == GroupDccWck)) {

          RunDiffDccAccCmd (MrcData, TotalSampleCounts, Group, ClkDutyCyle, CccDcdSampleCount);
          DccAllIndexPass = TRUE;
          for (Index = 0; Index < MRC_NUM_CCC_INSTANCES; Index++) {
            DdrIoTranslateCccToMcChannel (MrcData, Index, &Controller, &Channel);
            NcClkCount = 0;
            if (!MrcChannelExist (MrcData, Controller, Channel)) {
              NcClkCount = NcClkCount + 1;
              continue;
            }
            if (DccResultPassIndexGlobal[Index] == 1) {
              continue;
            }
            if (DccResultPassIndex[Index] == 1) {
              continue;
            }

            MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Index %d ", Index);

            //
            // determine whether differential clock DCC is complete by comparing duty cycle with thresholds and checking saturation
            // edge alignment used as a precondition for DCC exit
            //
            if ((((ClkDutyCyle[Index]) ) <= CLK_DCC_THRESHOLD_HI) && (((ClkDutyCyle[Index])) >= CLK_DCC_THRESHOLD_LO)) {
              //
              // check whether duty cycle falls within upper and lower thresholds
              //
              DccResultPassIndex[Index] = 1;
              TruePassCount = TruePassCount + 1;

              if (DccCorrectionCount <= DCC_PASS_LIMIT) {
                DccResultPassIndexGlobal[Index] = 1;
                MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Global Pass 1 with duty cycle %d  \n", ClkDutyCyle[Index]);
              } else {
                MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Local Pass 1 with duty cycle %d  \n", ClkDutyCyle[Index]);
              }
              continue;
            } else if ((((((ClkDutyCyle[Index])) > CLK_DCC_THRESHOLD_HI) && ((((ClkDutyCyle[Index])) < MAX_HIGH_DITHER_DUTY_CYCLE)))
                       && (ClkDutyCyclePrevSample[Index] < CLK_DCC_THRESHOLD_LO)) && (DccCorrectionCount != DCC_INITIAL_LOOP_COUNT)) {
              //
              // duty cycle dithered high condition
              //
              DccResultPassIndex[Index] = 1;
              TruePassCount = TruePassCount + 1;

              if (DccCorrectionCount <= DCC_PASS_LIMIT) {
                DccResultPassIndexGlobal[Index] = 1;
                MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Global Pass 2 with duty cycle %d  \n", ClkDutyCyle[Index]);
              } else {
                MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Local Pass 2 with duty cycle %d  \n", ClkDutyCyle[Index]);
              }
              ClkDutyCyclePrevSample[Index] = ClkDutyCyle[Index];
              continue;
            }

            ClkDutyCyclePrevSample[Index] = ClkDutyCyle[Index];
            //
            // determine next tco codes in order to converge DCC
            //
            if (((ClkDutyCyle[Index])) < CLK_DCC_THRESHOLD_LO) {
              ClkDccStatus[Index] = DccLow;
            } else if (((ClkDutyCyle[Index])) > CLK_DCC_THRESHOLD_HI) {
              ClkDccStatus[Index] = DccHigh;
            } else {
              ClkDccStatus[Index] = DccStatusAlign;
              DccResultPassIndex[Index] = 1;
              TruePassCount = TruePassCount + 1;

              if (DccCorrectionCount <= 2) {
                DccResultPassIndexGlobal[Index] = 1;
                MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Global Pass 3 with duty cycle %d  \n", ClkDutyCyle[Index]);
              } else {
                MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Local Pass 3 with duty cycle %d  \n", ClkDutyCyle[Index]);
              }
              continue;
            }
            //
            // determine next tco codes for differential clock DCC
            //
            ClkDccSaturate[Index] = AdjustTcoCodesForDcc (MrcData, &ClkTcoPRise[Index], &ClkTcoPFall[Index], &ClkTcoNRise[Index],
                                    &ClkTcoNFall[Index], ClkDccStatus[Index]);
            //
            // if tcocodes are saturated after both edge alignment and attempting to program next DCC codes, then exit
            //
            if ((ClkDccSaturate[Index] == TRUE)) {
              DccResultPassIndex[Index] = 1;
              if ((DccCorrectionCount == DCC_SATURATE_LIMIT) || (DccAlignCorrectionCount == DccAlignCorrectionLimit - 1)) {
                SaturateCount = SaturateCount + 1;
                DccResultPassIndexGlobal[Index] = 1;
                if (DccCorrectionCount == DCC_SATURATE_LIMIT) {
                  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Saturate with duty cycle %d  Restore the DCC code. Continue the next CLK and DccCorrectionCount = 0 \n", ClkDutyCyle[Index]);
                }
                if (DccAlignCorrectionCount == DccAlignCorrectionLimit - 1) {
                  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Saturate  with duty cycle %d at the last DccAlignCorrectionCount. Restore the DCC code\n", ClkDutyCyle[Index]);
                }
                if (Group == GroupDccClk0) {
                  SetDiffDccClkCodes(MrcData, TcopRiseSave[Index][0], TcopFallSave[Index][0], TconRiseSave[Index][0],
                                     TconFallSave[Index][0], SelClkInst, Index);
                } else if (Group == GroupDccClk1) {
                  SetDiffDccClkCodes(MrcData, TcopRiseSave[Index][1], TcopFallSave[Index][1], TconRiseSave[Index][1],
                                     TconFallSave[Index][1], SelClkInst, Index);

                }
              } else {
                SaturateFalsePass = TRUE;
                MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Saturate with duty cycle %d  continue the next CLK \n", ClkDutyCyle[Index]);
              }

              DccAllIndexPass = FALSE;
              continue;
            } else {
              //
              // program new tco codes
              //
              SetDiffDccClkCodes (MrcData, ClkTcoPRise[Index], ClkTcoPFall[Index], ClkTcoNRise[Index], ClkTcoNFall[Index], SelClkInst,
                                  Index);

            }

            if (DccResultPassIndex[Index] == 0) {
              DccAllIndexPass = FALSE;
            }
          } //Index
          if (DccAllIndexPass == TRUE) {
            break;
          }
        } else {
          MRC_DEBUG_MSG (Debug, MSG_LEVEL_ERROR, "code error: invalid group selected \n");
          return;
        }
      }
    }
    if (Group == GroupDccDqs) {
      //
      // Determine whether DCC convergence has been achieved
      //
      if ((DccCorrectionCount <= DCC_PASS_LIMIT) && (DccAllControllerPass == TRUE)) {
        //
        // DCC is converged and no additional alignment is required
        //
        MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "DQS Pass: DCC has completed successfully \n");
        break;
      } else if ((DccCorrectionCount <= DCC_SATURATE_LIMIT) && (DccAllControllerPass == TRUE)) {
        //
        // DCC cannot converge without impacting clock alignment
        //
        MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "DQS Pass: DCC has saturated before convergence \n");
        break;
      }
    } else if ((Group == GroupDccClk0) || (Group == GroupDccClk1) || (Group == GroupDccWck)) {
      //
      // Determine whether DCC convergence has been achieved
      //
      if ((DccCorrectionCount <= DCC_PASS_LIMIT) && ((DccAllIndexPass == TRUE) && (SaturateFalsePass != TRUE))) {
        //
        // DCC is converged and no additional alignment is required
        //
        MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "CLK Pass: DCC has completed successfully \n");
        break;
      } else if ((DccCorrectionCount <= DCC_SATURATE_LIMIT) && ((SaturateCount + TruePassCount + NcClkCount) == MRC_NUM_CCC_INSTANCES)) {
        //
        //  DCC cannot converge without impacting clock alignment
        //
        MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "CLK Pass: DCC has saturated before convergence. SaturateCount %d  TruePassCount %d  NcClkCount %d\n", SaturateCount, TruePassCount, NcClkCount);
        break;
      }
    }
  } //for  DccAlignCorrectionCount
  // Clean up
  Enable = FALSE;
  if (Group == GroupDccDqs) {
    EnterDqsDiffDcc (MrcData, Enable, TotalSampleCounts, SavedParams);
  } else {
    EnterClkDiffDcc (MrcData, Enable, TotalSampleCounts, Group, SavedParams);
  }
  return;
}

/**
  This routine enters Dqs Diff Dcc mode.

  @param[in]  MrcData              - Pointer to MRC global data.
  @param[in]  Enable               - Enable or Disable the DCC mode
  @param[in]  TotalSampleCounts    - Total Sample Counts
  @param[in]  SavedParams          - SavedParams are used to save the register value for restoration.

  @retval None
**/
VOID
EnterDqsDiffDcc (
  IN  MrcParameters *const MrcData,
  IN  BOOLEAN        Enable,
  IN  UINT32         TotalSampleCounts,
  IN  UINT32         SavedParams[MAX_DCC_SAVED_PARAMETERS]
  )
{
  INT64                                 GetSetVal;
  MrcOutput                             *Outputs;
  UINT32                                Offset;
  DATA0CH0_CR_SRZCTL_STRUCT             DataSrzCtl;
  DATA0CH0_CR_DQTXCTL0_STRUCT           DataDqTxCtl0;
  DATA0CH0_CR_PMFSM1_STRUCT             DataPmFsm1;
  UINT32                                Controller;
  UINT32                                Channel;
  UINT32                                Strobe;
  UINT32                                TransChannel;
  UINT32                                TransStrobe;
  UINT32                                Rank;

  Outputs = &MrcData->Outputs;

  if (Enable) {
    //
    //setup DCC FSM to operate in accumulator only mode.  Pattern parameter value doesn't matter for DCC as pden mode overwriting serializer data.
    //
    SetupDccAccData (MrcData, Enable, TotalSampleCounts, DCD_GROUP_SRZ, SavedParams);
    //
    // override serializer data to clock pattern and enable pre-driver output for DCD
    //
    for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
      for (Channel = 0; Channel < Outputs->MaxChannels; Channel++) {
        if (MrcChannelExist (MrcData, Controller, Channel)) {
          for (Strobe = 0; Strobe < Outputs->SdramCount; Strobe++) {
            TransChannel = Channel;
            TransStrobe  = Strobe;
            MrcTranslateSystemToIp (MrcData, &Controller, &TransChannel, &Rank, &TransStrobe, GsmIocTxOn); // No GetSet for this register, so use another per-byte PHY CR
            Offset = DATA0CH0_CR_SRZCTL_REG +
                    (DATA0CH1_CR_SRZCTL_REG - DATA0CH0_CR_SRZCTL_REG) * TransChannel +
                    (DATA1CH0_CR_SRZCTL_REG - DATA0CH0_CR_SRZCTL_REG) * TransStrobe;
            DataSrzCtl.Data = MrcReadCR (MrcData, Offset);
            //
            //enable phase detector, clock pattern through serializer
            //
            DataSrzCtl.Bits.imod_func_dqstx_cmn_txclkpden = 1;
            //
            //enable iobufact to release pwrgood to the DQ/DQS Buffers
            //
            DataSrzCtl.Bits.pghub_cmn_bonus = 1;
            MrcWriteCR (MrcData, Offset, DataSrzCtl.Data);
          }
        }
      }
    }

    DataDqTxCtl0.Data = MrcReadCR (MrcData, DATA0CH0_CR_DQTXCTL0_REG);
    DataDqTxCtl0.Bits.dxtx_cmn_txdyncpadreduxdis = 1; // enable phase detector, clock pattern through serializer, pden for DQ buffers
    MrcWriteCrMulticast (MrcData, DATA_CR_DQTXCTL0_REG, DataDqTxCtl0.Data);

    GetSetVal = 1;
    MrcGetSetChStrb (MrcData, MAX_CONTROLLER, MAX_CHANNEL, MAX_SDRAM_IN_DIMM, GsmIocTxOn, WriteCached, &GetSetVal);

    DataPmFsm1.Data = MrcReadCR (MrcData, DATA0CH0_CR_PMFSM1_REG);
    SavedParams [DATA0CH0_CR_PMFSM1_REG_INDEX] = DataPmFsm1.Data;

    DataPmFsm1.Bits.ldoen_ovrval = 0;
    DataPmFsm1.Bits.dllen_ovrval = 0;
    DataPmFsm1.Bits.dllen_ovren  = 1;
    MrcWriteCrMulticast (MrcData, DATA_CR_PMFSM1_REG, DataPmFsm1.Data);

    DataPmFsm1.Bits.ldoen_ovrval = 1;
    MrcWriteCrMulticast (MrcData, DATA_CR_PMFSM1_REG, DataPmFsm1.Data);

    MrcWait (MrcData, 1 * MRC_TIMER_1US);

    DataPmFsm1.Bits.dllen_ovrval = 1;
    MrcWriteCrMulticast (MrcData, DATA_CR_PMFSM1_REG, DataPmFsm1.Data);

  } else { // Disable
    //
    // Pattern parameter value doesn't matter due to pden mode overwriting serializer data
    //
    SetupDccAccData (MrcData, Enable, TotalSampleCounts, DCD_GROUP_SRZ, SavedParams);

    //MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "EnterDqsDiffDcc Clean Up\n");

    for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
      for (Channel = 0; Channel < Outputs->MaxChannels; Channel++) {
        if (MrcChannelExist (MrcData, Controller, Channel)) {
          for (Strobe = 0; Strobe < Outputs->SdramCount; Strobe++) {
            TransChannel = Channel;
            TransStrobe  = Strobe;
            MrcTranslateSystemToIp (MrcData, &Controller, &TransChannel, &Rank, &TransStrobe, GsmIocTxOn); // No GetSet for this register, so use another per-byte PHY CR
            Offset = DATA0CH0_CR_SRZCTL_REG +
                    (DATA0CH1_CR_SRZCTL_REG - DATA0CH0_CR_SRZCTL_REG) * TransChannel +
                    (DATA1CH0_CR_SRZCTL_REG - DATA0CH0_CR_SRZCTL_REG) * TransStrobe;
            DataSrzCtl.Data = MrcReadCR (MrcData, Offset);
            DataSrzCtl.Bits.imod_func_dqstx_cmn_txclkpden = 0;
            DataSrzCtl.Bits.pghub_cmn_bonus = 0;
            MrcWriteCR (MrcData, Offset, DataSrzCtl.Data);
          }
        }
      }
    }

    DataDqTxCtl0.Data = MrcReadCR (MrcData, DATA0CH0_CR_DQTXCTL0_REG);
    DataDqTxCtl0.Bits.dxtx_cmn_txdyncpadreduxdis = 0;   // Disable phase detector, clock pattern through serializer, pden for DQ buffers
    MrcWriteCrMulticast (MrcData, DATA_CR_DQTXCTL0_REG, DataDqTxCtl0.Data);

    GetSetVal = 0;
    MrcGetSetChStrb (MrcData, MAX_CONTROLLER, MAX_CHANNEL, MAX_SDRAM_IN_DIMM, GsmIocTxOn, WriteCached, &GetSetVal);

    // Restore DATA_CR_PMFSM1_REG
    DataPmFsm1.Data = SavedParams [DATA0CH0_CR_PMFSM1_REG_INDEX];
    MrcWriteCrMulticast (MrcData, DATA_CR_PMFSM1_REG, DataPmFsm1.Data);
  }
  return;
}

/**
  This routine setups up the data DCC mode.

  @param[in]  MrcData            - Pointer to MRC global data.
  @param[in]  Enable             - Enable or Disable the DCC mode
  @param[in]  TotalSampleCounts  - Total Sample Counts
  @param[in]  Pattern            - The Gear value, it doesn't matter for DCC.
  @param[in]  SavedParams        - SavedParams are used to save the register value for restoration.

  @retval None
**/
VOID
SetupDccAccData (
  IN  MrcParameters *const MrcData,
  IN  BOOLEAN        Enable,
  IN  UINT32         TotalSampleCounts,
  IN  UINT32         Pattern,
  IN  UINT32         SavedParams[MAX_DCC_SAVED_PARAMETERS]
  )
{
  DATA0CH0_CR_DCCCTL0_STRUCT  DataDccCtl0;
  DATA0CH0_CR_DCCCTL4_STRUCT  DataDccCtl4;
  DATA0CH0_CR_DCCCTL10_STRUCT DataDccCtl10;

  if (Enable) {
    // enable ring oscillator
    DataDccCtl0.Data = MrcReadCR (MrcData, DATA0CH0_CR_DCCCTL0_REG);
    DataDccCtl0.Bits.dcdrovcoen = 1;
    MrcWriteCrMulticast (MrcData, DATA_CR_DCCCTL0_REG, DataDccCtl0.Data);

    // force MDLL DCD Enable
    // force serializer DCD enable
    MrcGetSetChStrb (MrcData, MAX_CONTROLLER, MAX_CHANNEL, MAX_SDRAM_IN_DIMM, GsmIocMdllDcdEnData, WriteNoCache, &gGetSetEn);
    MrcGetSetChStrb (MrcData, MAX_CONTROLLER, MAX_CHANNEL, MAX_SDRAM_IN_DIMM, GsmIocSrzDcdEnData, WriteNoCache, &gGetSetEn);

    //  Enable DCD Serializer Data
    DataDccCtl4.Data = MrcReadCR (MrcData, DATA0CH0_CR_DCCCTL4_REG);
    DataDccCtl4.Bits.dcc_serdata_ovren = 1;
    MrcWriteCrMulticast (MrcData, DATA_CR_DCCCTL4_REG, DataDccCtl4.Data);

    // Program DCC pattern
    DataDccCtl10.Data = MrcReadCR (MrcData, DATA0CH0_CR_DCCCTL10_REG);
    SavedParams[DATA_STEP1PATTERN_INDEX] = DataDccCtl10.Bits.step1pattern;
    DataDccCtl10.Bits.step1pattern = gPatternTypes[Pattern];
    MrcWriteCrMulticast (MrcData, DATA_CR_DCCCTL10_REG, DataDccCtl10.Data);

    // put DCC into manual override mode to allow direct control of accumulator mux
    DataDccCtl4.Data = MrcReadCR (MrcData, DATA0CH0_CR_DCCCTL4_REG);
    DataDccCtl4.Bits.dcc_bits_en = 0;
    MrcWriteCrMulticast (MrcData, DATA_CR_DCCCTL4_REG, DataDccCtl4.Data);

    // save parameters, not used after this step, but save and restore for consistency
    DataDccCtl0.Data = MrcReadCR (MrcData, DATA0CH0_CR_DCCCTL0_REG);
    SavedParams [DATA_TOTAL_SAMPLE_COUNTS_INDEX] = DataDccCtl0.Bits.totalsamplecounts;
    DataDccCtl0.Bits.totalsamplecounts = TotalSampleCounts;
    MrcWriteCrMulticast (MrcData, DATA_CR_DCCCTL0_REG, DataDccCtl0.Data);
  } else {
    // Disable ring oscillator
    DataDccCtl0.Data = MrcReadCR (MrcData, DATA0CH0_CR_DCCCTL0_REG);
    DataDccCtl0.Bits.dcdrovcoen = 0;
    MrcWriteCrMulticast (MrcData, DATA_CR_DCCCTL0_REG, DataDccCtl0.Data);

    // force MDLL DCD disable
    // force serializer DCD disable
     MrcGetSetChStrb (MrcData, MAX_CONTROLLER, MAX_CHANNEL, MAX_SDRAM_IN_DIMM, GsmIocMdllDcdEnData, WriteNoCache, &gGetSetDis);
     MrcGetSetChStrb (MrcData, MAX_CONTROLLER, MAX_CHANNEL, MAX_SDRAM_IN_DIMM, GsmIocSrzDcdEnData, WriteNoCache, &gGetSetDis);

    // Disable DCD Serializer Data
    DataDccCtl4.Data = MrcReadCR (MrcData, DATA0CH0_CR_DCCCTL4_REG);
    DataDccCtl4.Bits.dcc_serdata_ovren = 0;
    MrcWriteCrMulticast (MrcData, DATA_CR_DCCCTL4_REG, DataDccCtl4.Data);

    // Restore DCC pattern
    DataDccCtl10.Data = MrcReadCR (MrcData, DATA0CH0_CR_DCCCTL10_REG);
    DataDccCtl10.Bits.step1pattern = SavedParams[DATA_STEP1PATTERN_INDEX];
    MrcWriteCrMulticast (MrcData, DATA_CR_DCCCTL10_REG, DataDccCtl10.Data);

    // Restore totalsamplecounts
    DataDccCtl0.Data = MrcReadCR (MrcData, DATA0CH0_CR_DCCCTL0_REG);
    DataDccCtl0.Bits.totalsamplecounts = SavedParams [DATA_TOTAL_SAMPLE_COUNTS_INDEX];
    MrcWriteCrMulticast (MrcData, DATA_CR_DCCCTL0_REG, DataDccCtl0.Data);
  }
}

/**
  This routine enters the CLK diff DCC mode.

  @param[in]  MrcData                 - Pointer to MRC global data.
  @param[in]  Enable                  - Enable or Disable the DCC mode
  @param[in]  TotalSampleCounts       - Total Sample Counts
  @param[in]  SelClkInst              - Group input, the value can be GroupDccClk0, or GroupDccClk1 or GroupDccWck.
  @param[in]  SavedParams             - SavedParams are used to save the register value for restoration.

  @retval None

**/
VOID
EnterClkDiffDcc (
  IN MrcParameters *const MrcData,
  IN BOOLEAN        Enable,
  IN UINT32         TotalSampleCounts,
  IN DCC_GROUP      SelClkInst,
  IN UINT32         SavedParams[MAX_DCC_SAVED_PARAMETERS]
  )
{
  MrcInput                             *Inputs;
  MrcOutput                            *Outputs;
  MrcDebug                             *Debug;
  BOOLEAN                              A0;
  BOOLEAN                              B0;
  BOOLEAN                              J0;
  UINT32                               Offset;
  CH0CCC_CR_SRZBYPASSCTL_STRUCT        SrzBypassCtl;
  CH0CCC_CR_COMP2_STRUCT               CccComp2;
  CH0CCC_CR_COMP2_STRUCT               CccComp2Temp;
  CH0CCC_CR_COMP3_STRUCT               CccComp3;
  CH0CCC_CR_COMP3_STRUCT               CccComp3Temp;
  CCC0_GLOBAL_CR_COMPUPDT_DLY1_STRUCT  CccCompupdtDly1;
  CCC0_GLOBAL_CR_COMPUPDT_DLY1_STRUCT  CccCompupdtDly1Temp;
  CH0CCC_CR_CTL3_STRUCT                CccCtl3;
  CH0CCC_CR_SRZDRVENBYPASSCTL_STRUCT   SrzDrvEnBypassCtl;
  CH0CCC_CR_DDRCRCCCCLKCONTROLS_STRUCT CccClkControls;
  CH0CCC_CR_DDRCRCCCCLKCONTROLS_STRUCT CccClkControlsTemp;

  Inputs  = &MrcData->Inputs;
  Outputs = &MrcData->Outputs;
  Debug   = &Outputs->Debug;
  A0      = Inputs->A0;
  B0      = Inputs->B0;
  J0      = Inputs->J0;
  //
  // setup DCC FSM to operate in accumulator only mode
  // Pattern doesn't matter due to pden mode overwriting serializer data. It (pattern gear) doesn't matter for DCC
  //
  SetupDccAccCmd (MrcData, Enable, TotalSampleCounts, DCD_GROUP_SRZ, TRUE, SavedParams);

  if (Enable == TRUE) {
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "EnterClkDiffDcc SelClkInst %d\n", SelClkInst);
    Offset = A0 ? CH0CCC_CR_DDRCRCCCCLKCONTROLS_A0_REG : CH0CCC_CR_DDRCRCCCCLKCONTROLS_REG;
    CccClkControls.Data = MrcReadCR (MrcData, Offset);
    SavedParams[CH0CCC_CR_DDRCRCCCCLKCONTROLS_REG_INDEX] = CccClkControls.Data;
    CccClkControls.Bits.IntCkOn = 1;
    Offset = A0 ? CCC_CR_DDRCRCCCCLKCONTROLS_A0_REG : CCC_CR_DDRCRCCCCLKCONTROLS_REG;
    MrcWriteCrMulticast (MrcData, Offset, CccClkControls.Data);

    SrzBypassCtl.Data = MrcReadCR (MrcData, CH0CCC_CR_SRZBYPASSCTL_REG);
    SrzBypassCtl.Bits.srzdataovrden = 0;
    MrcWriteCrMulticast (MrcData, CCC_CR_SRZBYPASSCTL_REG, SrzBypassCtl.Data);

    Offset = A0 ? CH0CCC_CR_COMP2_A0_REG : CH0CCC_CR_COMP2_REG;
    CccComp2.Data = MrcReadCR (MrcData, Offset);
    SavedParams[CH0CCC_CR_COMP2_REG_INDEX] = CccComp2.Data;
    if (A0) {
      CccComp2.BitsA0.ccctx_rgrp3_compupdtsyncen = 0;
      CccComp2.BitsA0.ccctx_rgrp4_compupdtsyncen = 0;
    } else {
      CccComp2.Bits.ccctx_rgrp3_compupdtsyncen = 0;
      CccComp2.Bits.ccctx_rgrp4_compupdtsyncen = 0;
    }
    Offset = A0 ? CCC_CR_COMP2_A0_REG : CCC_CR_COMP2_REG;
    if ((!A0) && (!B0) && (!J0)) {
      CccComp2.Bits.ccctx_rgrp4_compupdtsyncen_ovr_sel = 1;
      CccComp2.Bits.ccctx_rgrp3_compupdtsyncen_ovr_sel = 1;
    }
    MrcWriteCrMulticast (MrcData, Offset, CccComp2.Data);

    Offset = A0 ? CH0CCC_CR_COMP3_A0_REG : CH0CCC_CR_COMP3_REG;
    CccComp3.Data = MrcReadCR (MrcData, Offset);
    SavedParams[CH0CCC_CR_COMP3_REG_INDEX] = CccComp3.Data;
    CccComp3.Bits.ccctx_cmn_compupdten = 0x01e0;
    Offset = A0 ? CCC_CR_COMP3_A0_REG : CCC_CR_COMP3_REG;
    MrcWriteCrMulticast (MrcData, Offset, CccComp3.Data);

    if (!A0) {
      Offset = CCC0_GLOBAL_CR_COMPUPDT_DLY1_REG;
      CccCompupdtDly1.Data = MrcReadCR (MrcData, Offset);
      SavedParams[CCC0_GLOBAL_CR_COMPUPDT_DLY1_REG_INDEX] = CccCompupdtDly1.Data;
      CccCompupdtDly1.Bits.compupdtovr_sel = 0x01e0;
      Offset = CCC_GLOBAL_CR_COMPUPDT_DLY1_REG;
      MrcWriteCrMulticast (MrcData, Offset, CccCompupdtDly1.Data);
    }

    if (SelClkInst == GroupDccClk0) {

      //
      // Enable phase detector, clock pattern through serializer
      //
      Offset = CH0CCC_CR_CTL3_REG;
      CccCtl3.Data = MrcReadCR (MrcData, Offset);
      CccCtl3.Bits.clktx_rgrp3_txclkpden = 1;
      Offset = CCC_CR_CTL3_REG;
      MrcWriteCrMulticast (MrcData, Offset, CccCtl3.Data);
      //
      // Enable Tx Drive for CK Group
      //
      Offset = CH0CCC_CR_SRZDRVENBYPASSCTL_REG;
      SrzDrvEnBypassCtl.Data = MrcReadCR (MrcData, Offset);
      SrzDrvEnBypassCtl.Bits.ccc5_srzdrvenovrden = 1;
      SrzDrvEnBypassCtl.Bits.ccc5_srzdrvenovrdval = 1;
      SrzDrvEnBypassCtl.Bits.ccc6_srzdrvenovrden = 1;
      SrzDrvEnBypassCtl.Bits.ccc6_srzdrvenovrdval = 1;
      MrcWriteCrMulticast (MrcData, CCC_CR_SRZDRVENBYPASSCTL_REG, SrzDrvEnBypassCtl.Data);

    } else if ((SelClkInst == GroupDccClk1) || (SelClkInst == GroupDccWck)) {

      //
      // enable phase detector, clock pattern through serializer
      //
      Offset = CH0CCC_CR_CTL3_REG;
      CccCtl3.Data = MrcReadCR (MrcData, Offset);
      CccCtl3.Bits.clktx_rgrp4_txclkpden = 1;
      Offset = CCC_CR_CTL3_REG;
      MrcWriteCrMulticast (MrcData, Offset, CccCtl3.Data);
      //
      // Enable Tx Drive for WCK Group
      //
      Offset = CH0CCC_CR_SRZDRVENBYPASSCTL_REG;
      SrzDrvEnBypassCtl.Data = MrcReadCR (MrcData, Offset);
      SrzDrvEnBypassCtl.Bits.ccc7_srzdrvenovrden = 1;
      SrzDrvEnBypassCtl.Bits.ccc7_srzdrvenovrdval = 1;
      SrzDrvEnBypassCtl.Bits.ccc8_srzdrvenovrden = 1;
      SrzDrvEnBypassCtl.Bits.ccc8_srzdrvenovrdval = 1;
      MrcWriteCrMulticast (MrcData, CCC_CR_SRZDRVENBYPASSCTL_REG, SrzDrvEnBypassCtl.Data);
    }
  } else {

    Offset = A0 ? CH0CCC_CR_COMP2_A0_REG : CH0CCC_CR_COMP2_REG;
    CccComp2.Data = MrcReadCR (MrcData, Offset);
    CccComp2Temp.Data = SavedParams[CH0CCC_CR_COMP2_REG_INDEX];
    if (A0) {
      CccComp2.BitsA0.ccctx_rgrp3_compupdtsyncen = CccComp2Temp.BitsA0.ccctx_rgrp3_compupdtsyncen;
      CccComp2.BitsA0.ccctx_rgrp4_compupdtsyncen = CccComp2Temp.BitsA0.ccctx_rgrp4_compupdtsyncen;
    } else {
      CccComp2.Bits.ccctx_rgrp3_compupdtsyncen = CccComp2Temp.Bits.ccctx_rgrp3_compupdtsyncen;
      CccComp2.Bits.ccctx_rgrp4_compupdtsyncen = CccComp2Temp.Bits.ccctx_rgrp4_compupdtsyncen;
    }
    Offset = A0 ? CCC_CR_COMP2_A0_REG : CCC_CR_COMP2_REG;

    if ((!A0) && (!B0) && (!J0)) {
      CccComp2.Bits.ccctx_rgrp4_compupdtsyncen_ovr_sel = 0;
      CccComp2.Bits.ccctx_rgrp3_compupdtsyncen_ovr_sel = 0;
    }

    MrcWriteCrMulticast (MrcData, Offset, CccComp2.Data);

    Offset = A0 ? CH0CCC_CR_DDRCRCCCCLKCONTROLS_A0_REG : CH0CCC_CR_DDRCRCCCCLKCONTROLS_REG;
    CccClkControlsTemp.Data = SavedParams[CH0CCC_CR_DDRCRCCCCLKCONTROLS_REG_INDEX];
    CccClkControls.Data = MrcReadCR (MrcData, Offset);
    CccClkControls.Bits.IntCkOn = CccClkControlsTemp.Bits.IntCkOn;
    Offset = A0 ? CCC_CR_DDRCRCCCCLKCONTROLS_A0_REG : CCC_CR_DDRCRCCCCLKCONTROLS_REG;
    MrcWriteCrMulticast (MrcData, Offset, CccClkControls.Data);



    if (!A0) {
      Offset = CCC0_GLOBAL_CR_COMPUPDT_DLY1_REG;
      CccCompupdtDly1.Data = MrcReadCR (MrcData, Offset);
      CccCompupdtDly1Temp.Data = SavedParams[CCC0_GLOBAL_CR_COMPUPDT_DLY1_REG_INDEX];
      CccCompupdtDly1.Bits.compupdtovr_sel = CccCompupdtDly1Temp.Bits.compupdtovr_sel;
      Offset = CCC_GLOBAL_CR_COMPUPDT_DLY1_REG;
      MrcWriteCrMulticast (MrcData, Offset, CccCompupdtDly1.Data);
    }

    Offset = A0 ? CH0CCC_CR_COMP3_A0_REG : CH0CCC_CR_COMP3_REG;
    CccComp3.Data = MrcReadCR (MrcData, Offset);
    CccComp3Temp.Data = SavedParams[CH0CCC_CR_COMP3_REG_INDEX];
    CccComp3.Bits.ccctx_cmn_compupdten = CccComp3Temp.Bits.ccctx_cmn_compupdten;
    Offset = A0 ? CCC_CR_COMP3_A0_REG : CCC_CR_COMP3_REG;
    MrcWriteCrMulticast (MrcData, Offset, CccComp3.Data);

    Offset = CH0CCC_CR_CTL3_REG;
    CccCtl3.Data = MrcReadCR (MrcData, Offset);
    CccCtl3.Bits.clktx_rgrp3_txclkpden = 0;
    Offset = CCC_CR_CTL3_REG;
    MrcWriteCrMulticast (MrcData, Offset, CccCtl3.Data);
    //
    // Enable Tx Drive for CK Group
    //
    Offset = CH0CCC_CR_SRZDRVENBYPASSCTL_REG;
    SrzDrvEnBypassCtl.Data = MrcReadCR (MrcData, Offset);
    SrzDrvEnBypassCtl.Bits.ccc5_srzdrvenovrden = 0;
    SrzDrvEnBypassCtl.Bits.ccc5_srzdrvenovrdval = 0;
    SrzDrvEnBypassCtl.Bits.ccc6_srzdrvenovrden = 0;
    SrzDrvEnBypassCtl.Bits.ccc6_srzdrvenovrdval = 0;
    MrcWriteCrMulticast (MrcData, CCC_CR_SRZDRVENBYPASSCTL_REG, SrzDrvEnBypassCtl.Data);

    Offset = CH0CCC_CR_CTL3_REG;
    CccCtl3.Data = MrcReadCR (MrcData, Offset);
    CccCtl3.Bits.clktx_rgrp4_txclkpden = 0;
    Offset = CCC_CR_CTL3_REG;
    MrcWriteCrMulticast (MrcData, Offset, CccCtl3.Data);
    //
    // Enable Tx Drive for WCK Group
    //
    Offset = CH0CCC_CR_SRZDRVENBYPASSCTL_REG;
    SrzDrvEnBypassCtl.Data = MrcReadCR (MrcData, Offset);
    SrzDrvEnBypassCtl.Bits.ccc7_srzdrvenovrden = 0;
    SrzDrvEnBypassCtl.Bits.ccc7_srzdrvenovrdval = 0;
    SrzDrvEnBypassCtl.Bits.ccc8_srzdrvenovrden = 0;
    SrzDrvEnBypassCtl.Bits.ccc8_srzdrvenovrdval = 0;
    MrcWriteCrMulticast (MrcData, CCC_CR_SRZDRVENBYPASSCTL_REG, SrzDrvEnBypassCtl.Data);
  }
  return;
}


/**
  This routine sets up CLK diff DCC.

  @param[in]  MrcData                 - Pointer to MRC global data.
  @param[in]  Enable                  - Enable or Disable the CLK DCC mode
  @param[in]  TotalSampleCounts       - Total Sample Counts
  @param[in]  Pattern                 - Pattern. The value doesn't matter for DCC.
  @param[in]  DiffClk                 - It's diff CLK DCC or not
  @param[in]  SavedParams             - SavedParams are used to save the register value for restoration.

  @retval None

**/
VOID
SetupDccAccCmd (
  IN OUT MrcParameters *const MrcData,
  IN     BOOLEAN        Enable,
  IN     UINT32         TotalSampleCounts,
  IN     UINT32         Pattern,
  IN     BOOLEAN        DiffClk,
  IN     UINT32         SavedParams[MAX_DCC_SAVED_PARAMETERS]
  )
{
  CH0CCC_CR_DCCCTL0_STRUCT            CccDccCtl0;
  CH0CCC_CR_DCCCTL4_STRUCT            CccDccCtl4;
  CH0CCC_CR_DCCCTL2_STRUCT            CccDccCtl2;
  CH0CCC_CR_DCCCTL13_STRUCT           CccDccCtl13;
  CCC0_GLOBAL_CR_PM_FSM_OVRD_0_STRUCT CccGlobalPmFsmOvrd0;
  CCC0_GLOBAL_CR_PM_FSM_OVRD_1_STRUCT CccGlobalPmFsmOvrd1;

  if (Enable) {
    CccGlobalPmFsmOvrd0.Data = MrcReadCR (MrcData, CCC0_GLOBAL_CR_PM_FSM_OVRD_0_REG);
    CccGlobalPmFsmOvrd1.Data = MrcReadCR (MrcData, CCC0_GLOBAL_CR_PM_FSM_OVRD_1_REG);

    SavedParams[CCC0_GLOBAL_CR_PM_FSM_OVRD_0_REG_INDEX] = CccGlobalPmFsmOvrd0.Data;
    SavedParams[CCC0_GLOBAL_CR_PM_FSM_OVRD_1_REG_INDEX] = CccGlobalPmFsmOvrd1.Data;
    //
    //Force Enable DLLs
    //
    CccGlobalPmFsmOvrd0.Bits.ldoen_ovrval = 0;
    CccGlobalPmFsmOvrd0.Bits.dllen_ovrval = 0;
    CccGlobalPmFsmOvrd0.Bits.dllen_ovren = 1;
    MrcWriteCrMulticast (MrcData, CCC_GLOBAL_CR_PM_FSM_OVRD_0_REG, CccGlobalPmFsmOvrd0.Data);

    CccGlobalPmFsmOvrd0.Bits.ldoen_ovrval = 1;
    MrcWriteCrMulticast (MrcData, CCC_GLOBAL_CR_PM_FSM_OVRD_0_REG, CccGlobalPmFsmOvrd0.Data);

    MrcWait (MrcData, 1 * MRC_TIMER_1US);
    CccGlobalPmFsmOvrd0.Bits.dllen_ovrval = 1;
    MrcWriteCrMulticast (MrcData, CCC_GLOBAL_CR_PM_FSM_OVRD_0_REG, CccGlobalPmFsmOvrd0.Data);
    //
    // Force Enable DLLs
    //
    CccGlobalPmFsmOvrd1.Bits.ldoen_ovrval = 0;
    CccGlobalPmFsmOvrd1.Bits.dllen_ovrval = 0;
    CccGlobalPmFsmOvrd1.Bits.dllen_ovren = 1;
    MrcWriteCrMulticast (MrcData, CCC_GLOBAL_CR_PM_FSM_OVRD_1_REG, CccGlobalPmFsmOvrd1.Data);

    CccGlobalPmFsmOvrd1.Bits.ldoen_ovrval = 1;

    MrcWriteCrMulticast (MrcData, CCC_GLOBAL_CR_PM_FSM_OVRD_1_REG, CccGlobalPmFsmOvrd1.Data);
    MrcWait (MrcData, 1 * MRC_TIMER_1US);

    CccGlobalPmFsmOvrd1.Bits.dllen_ovrval = 1;

    MrcWriteCrMulticast (MrcData, CCC_GLOBAL_CR_PM_FSM_OVRD_1_REG, CccGlobalPmFsmOvrd1.Data);

    //
    // Enable ring oscillator
    //
    CccDccCtl0.Data = MrcReadCR (MrcData, CH0CCC_CR_DCCCTL0_REG);
    CccDccCtl0.Bits.dcdrovcoen = 1;
    MrcWriteCrMulticast (MrcData, CCC_CR_DCCCTL0_REG,  CccDccCtl0.Data);

    //
    // Put DCC into manual override mode to allow direct control of accumulator mux
    //
    CccDccCtl4.Data = MrcReadCR (MrcData, CH0CCC_CR_DCCCTL4_REG);
    CccDccCtl4.Bits.dcc_bits_en = 0;
    MrcWriteCrMulticast (MrcData, CCC_CR_DCCCTL4_REG,  CccDccCtl4.Data);
    //
    // program the total number of samples to accumulate (actual is x2)
    //
    CccDccCtl0.Data = MrcReadCR (MrcData, CH0CCC_CR_DCCCTL0_REG);
    SavedParams [CCC_TOTAL_SAMPLE_COUNTS_INDEX] = CccDccCtl0.Bits.totalsamplecounts;
    CccDccCtl0.Bits.totalsamplecounts = TotalSampleCounts;
    MrcWriteCrMulticast (MrcData, CCC_CR_DCCCTL0_REG,  CccDccCtl0.Data);

    //
    // Enable DCD Serializer Data
    //
    CccDccCtl2.Data = MrcReadCR (MrcData, CH0CCC_CR_DCCCTL2_REG);
    CccDccCtl2.Bits.dcc_serdata_ovren = 1;
    MrcWriteCrMulticast (MrcData, CCC_CR_DCCCTL2_REG, CccDccCtl2.Data);
    //
    // program DCC pattern
    //
    CccDccCtl13.Data = MrcReadCR (MrcData, CH0CCC_CR_DCCCTL13_REG);
    SavedParams[DCCCTL13_STEP1PATTERN_REG_INDEX] = CccDccCtl13.Bits.step1pattern;
    CccDccCtl13.Bits.step1pattern = gPatternTypes[Pattern];
    MrcWriteCrMulticast (MrcData, CCC_CR_DCCCTL13_REG, CccDccCtl13.Data);

    //
    // Force MDLL DCD Enable
    //
    MrcGetSetCccInst (MrcData, MAX_CCC_INSTANCES, GsmIocSrzDcdEnCcc,  WriteNoCache, &gGetSetEn);
    MrcGetSetCccInst (MrcData, MAX_CCC_INSTANCES, GsmIocMdllDcdEnCcc, WriteNoCache, &gGetSetEn);
  } else {
    //
    // Restore CCC_GLOBAL_CR_PM_FSM_OVRD_0_REG and CCC_GLOBAL_CR_PM_FSM_OVRD_1_REG
    //
    CccGlobalPmFsmOvrd0.Data = SavedParams[CCC0_GLOBAL_CR_PM_FSM_OVRD_0_REG_INDEX];
    CccGlobalPmFsmOvrd1.Data = SavedParams[CCC0_GLOBAL_CR_PM_FSM_OVRD_1_REG_INDEX];

    MrcWriteCrMulticast (MrcData, CCC_GLOBAL_CR_PM_FSM_OVRD_0_REG, CccGlobalPmFsmOvrd0.Data);
    MrcWriteCrMulticast (MrcData, CCC_GLOBAL_CR_PM_FSM_OVRD_1_REG, CccGlobalPmFsmOvrd1.Data);

    //
    // Disable ring oscillator
    //
    CccDccCtl0.Data = MrcReadCR (MrcData, CH0CCC_CR_DCCCTL0_REG);
    CccDccCtl0.Bits.dcdrovcoen = 0;
    MrcWriteCrMulticast (MrcData, CCC_CR_DCCCTL0_REG,  CccDccCtl0.Data);

    //
    // Restore the total number of samples to accumulate (actual is x2)
    //
    CccDccCtl0.Data = MrcReadCR (MrcData, CH0CCC_CR_DCCCTL0_REG);
    CccDccCtl0.Bits.totalsamplecounts = SavedParams[CCC_TOTAL_SAMPLE_COUNTS_INDEX];
    MrcWriteCrMulticast (MrcData, CCC_CR_DCCCTL0_REG,  CccDccCtl0.Data);

    CccDccCtl2.Data = MrcReadCR (MrcData, CH0CCC_CR_DCCCTL2_REG);
    CccDccCtl2.Bits.dcc_serdata_ovren = 0;
    MrcWriteCrMulticast (MrcData, CCC_CR_DCCCTL2_REG, CccDccCtl2.Data);

    CccDccCtl13.Data = MrcReadCR (MrcData, CH0CCC_CR_DCCCTL13_REG);
    CccDccCtl13.Bits.step1pattern = SavedParams[DCCCTL13_STEP1PATTERN_REG_INDEX];
    MrcWriteCrMulticast (MrcData, CCC_CR_DCCCTL13_REG, CccDccCtl13.Data);

    //
    // Force MDLL DCD Disable
    //
    MrcGetSetCccInst (MrcData, MAX_CCC_INSTANCES, GsmIocSrzDcdEnCcc,  WriteNoCache, &gGetSetDis);
    MrcGetSetCccInst (MrcData, MAX_CCC_INSTANCES, GsmIocMdllDcdEnCcc, WriteNoCache, &gGetSetDis);
  }
  return;
}

/**
  This routine selects the DCD sample selection for Dqs.

  @param[in]  MrcData                 - Pointer to MRC global data.
  @param[in]  ClockInst               - DCC signal input.

  @retval None

**/
VOID
SelectDccAccDcdData (
  IN OUT MrcParameters *const MrcData,
  IN     DCC_CLK_TYPES        ClockInst
  )
{
  //
  // ClockTypes = {'mdll_refclk':0, 'mdll_fbclk':0, DccDqs:1, 'dq0':2, 'dq1':3, 'dq2':4, 'dq3':5, 'dq4':6, 'dq5':7, 'dq6':8, 'dq7':9}
  // select which DCD to sample
  //
  DATA0CH0_CR_MDLLCTL2_STRUCT DataMdllCtl2;
  INT64 DcdSampleSelValue = 0;

  switch (ClockInst) {
    case MdllRefclk:
    case MdllFbclk:
      DcdSampleSelValue = 0;
      break;

    case DccDqs:
      DcdSampleSelValue = 1;
      break;
    case DccDq0:
      DcdSampleSelValue = 2;
      break;
    case DccDq1:
      DcdSampleSelValue = 3;
      break;
    case DccDq2:
      DcdSampleSelValue = 4;
      break;
    case DccDq3:
      DcdSampleSelValue = 5;
      break;
    case DccDq4:
      DcdSampleSelValue = 6;
      break;
    case DccDq5:
      DcdSampleSelValue = 7;
      break;
    case DccDq6:
      DcdSampleSelValue = 8;
      break;
    case DccDq7:
      DcdSampleSelValue = 9;
      break;
    default:
      break;
  }
  //
  // Select which DCD to sample
  //
  MrcGetSetChStrb (MrcData, MAX_CONTROLLER, MAX_CHANNEL, MAX_SDRAM_IN_DIMM, GsmIocDccDcdSampleSelData, WriteNoCache, &DcdSampleSelValue);

  if (ClockInst == MdllRefclk) {
    DataMdllCtl2.Data = MrcReadCR (MrcData, DATA0CH0_CR_MDLLCTL2_REG);
    DataMdllCtl2.Bits.mdll_cmn_dcdtargclksel = 0;
    MrcWriteCrMulticast (MrcData, DATA_CR_MDLLCTL2_REG, DataMdllCtl2.Data);
  } else if (ClockInst == MdllFbclk) {
    DataMdllCtl2.Data = MrcReadCR (MrcData, DATA0CH0_CR_MDLLCTL2_REG);
    DataMdllCtl2.Bits.mdll_cmn_dcdtargclksel = 1;
    MrcWriteCrMulticast (MrcData, DATA_CR_MDLLCTL2_REG, DataMdllCtl2.Data);
  }
  return;
}

/**
  This routine selects the DCD sample selection for CLK.

  @param[in]  MrcData                 - Pointer to MRC global data.
  @param[in]  SelClkInst              - DCC signal group input, the value can be GroupDccClk0, GroupDccClk1 or GroupDccWck.
  @param[in]  selClkP                 - It's CLK P or not.

  @retval None

**/
VOID
SelDccAccDiffClkCmd (
  IN OUT MrcParameters *const MrcData,
  IN     DCC_GROUP     SelClkInst,
  IN     BOOLEAN       selClkP
  )
{
  if (SelClkInst == GroupDccClk0) {
    if (selClkP == TRUE) {
      SelectDccAccDcdCmd (MrcData, DccCcc5);
    } else {
      SelectDccAccDcdCmd (MrcData, DccCcc6);
    }
  } else if ((SelClkInst == GroupDccClk1) || (SelClkInst == GroupDccWck)) {
    if (selClkP == TRUE) {
      SelectDccAccDcdCmd (MrcData, DccCcc7);
    } else {
      SelectDccAccDcdCmd (MrcData, DccCcc8);
    }
  }
  return;
}

/**
  This routine selects the DCD sample selection for CLK.

  @param[in]  MrcData                 - Pointer to MRC global data.
  @param[in]  SelClkInst              - DCC signal group input, the value can be GroupDccClk0, GroupDccClk1 or GroupDccWck.

  @retval None

**/
VOID
SelectDccAccDcdCmd (
  IN OUT MrcParameters *const MrcData,
  IN     DCC_CLK_TYPES        ClockInst
  )
{
  //
  // ClockTypes = {'mdll_refclk':0, 'mdll_fbclk':0, 'ccc0':1, 'ccc1':2, 'ccc2':3, 'ccc3':4, 'ccc4':5, 'ccc5':6, 'ccc6':7, 'ccc7':8, 'ccc8':9, 'ccc9':10, 'ccc10':11, 'ccc11':12, 'ccc12':13, 'ccc13':14, 'ccc14':15}
  // select which DCD to sample
  //
  CH0CCC_CR_MDLLCTL2_STRUCT   CccMdllCtl2;
  UINT32                      Offset;

  BOOLEAN                     A0 = MrcData->Inputs.A0;
  INT64                       DcdSampleSelValue = 0;

  switch (ClockInst) {
    case MdllRefclk:
    case MdllFbclk:
      DcdSampleSelValue = 0;
      break;
    case DccCcc0:
      DcdSampleSelValue = 1;
      break;
    case DccCcc1:
      DcdSampleSelValue = 2;
      break;
    case DccCcc2:
      DcdSampleSelValue = 3;
      break;
    case DccCcc3:
      DcdSampleSelValue = 4;
      break;
    case DccCcc4:
      DcdSampleSelValue = 5;
      break;
    case DccCcc5:
      DcdSampleSelValue = 6;
      break;
    case DccCcc6:
      DcdSampleSelValue = 7;
      break;
    case DccCcc7:
      DcdSampleSelValue = 8;
      break;
    case DccCcc8:
      DcdSampleSelValue = 9;
      break;
    case DccCcc9:
      DcdSampleSelValue = 10;
      break;
    case DccCcc10:
      DcdSampleSelValue = 11;
      break;
    case DccCcc11:
      DcdSampleSelValue = 12;
      break;
    case DccCcc12:
      DcdSampleSelValue = 13;
      break;
    case DccCcc13:
      DcdSampleSelValue = 14;
      break;
    case DccCcc14:
      DcdSampleSelValue = 15;
      break;
    default:
      break;
  }
  //
  // Select which DCD to sample
  //
  MrcGetSetCccInst (MrcData, MAX_CCC_INSTANCES, GsmIocDccDcdSampleSelCcc, WriteNoCache, &DcdSampleSelValue);

  if (ClockInst == MdllRefclk) {
    Offset = A0 ? CH0CCC_CR_MDLLCTL2_A0_REG : CH0CCC_CR_MDLLCTL2_REG;
    CccMdllCtl2.Data = MrcReadCR (MrcData, Offset);
    CccMdllCtl2.Bits.mdll_cmn_dcdtargclksel = 0;
    Offset = A0 ? CCC_CR_MDLLCTL2_A0_REG : CCC_CR_MDLLCTL2_REG;
    MrcWriteCrMulticast (MrcData, Offset, CccMdllCtl2.Data);

  } else if (ClockInst == MdllFbclk) {
    Offset = A0 ? CH0CCC_CR_MDLLCTL2_A0_REG : CH0CCC_CR_MDLLCTL2_REG;
    CccMdllCtl2.Data = MrcReadCR (MrcData, Offset);
    CccMdllCtl2.Bits.mdll_cmn_dcdtargclksel = 1;
    Offset = A0 ? CCC_CR_MDLLCTL2_A0_REG : CCC_CR_MDLLCTL2_REG;
    MrcWriteCrMulticast (MrcData, Offset, CccMdllCtl2.Data);
  }
  return;
}

/**
  This routine runs DQS DCC.

  @param[in]  MrcData                  - Pointer to MRC global data.
  @param[in]  TotalSampleCounts        - Total Sample Counts.
  @param[in]  DutyCycle                - Duty Cycle.
  @param[in]  DcdSampleCount           - Dcd Sample Count.

  @retval None

**/
VOID
RunDccAccData (
  IN  MrcParameters *const MrcData,
  IN  UINT32               TotalSampleCounts,
  IN  UINT32               DutyCycle[MAX_CONTROLLER][MAX_CHANNEL][MAX_SDRAM_IN_DIMM],
  OUT UINT32               DcdSampleCount[MAX_CONTROLLER][MAX_CHANNEL][MAX_SDRAM_IN_DIMM]
  )
{

  UINT32                      Offset;
  UINT32                      Controller;
  UINT32                      Channel;
  UINT32                      TransChannel;
  UINT32                      Rank;
  UINT32                      Strobe;
  UINT32                      TransStrobe;
  MrcOutput                   *Outputs;
  UINT32                      Data32;
  DATA0CH0_CR_DCCCTL13_STRUCT DataDccCtl13;

  Outputs = &MrcData->Outputs;

#ifdef DCC_DEBUG_PRINT
  UINT32                      DataInt;
  MrcDebug                    *Debug;

  Debug = &Outputs->Debug;
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "RunDccAccData: \n");
#endif // DCC_DEBUG_PRINT

  MrcGetSetChStrb (MrcData, MAX_CONTROLLER, MAX_CHANNEL, MAX_SDRAM_IN_DIMM, GsmIocDcdSampleCountRstData, WriteNoCache, &gGetSetDis);
  MrcGetSetChStrb (MrcData, MAX_CONTROLLER, MAX_CHANNEL, MAX_SDRAM_IN_DIMM, GsmIocDcdSampleCountRstData, WriteNoCache, &gGetSetEn);

  MrcGetSetChStrb (MrcData, MAX_CONTROLLER, MAX_CHANNEL, MAX_SDRAM_IN_DIMM, GsmIocDcdSampleCountStartData, WriteNoCache, &gGetSetEn);
  //
  //  wait for accumulator to finish, actual time is DDRDATA_CR_DCCCTL0.TotalSampleCounts * Troclk, where Troclk is the period of the ring oscillator clock.
  //  ring oscillator clock frequency Can determined by the equation: Troclk = TotalSampleCounts / (dcdrocal_low_freq_marker * Fqclk)
  //
  MrcWait (MrcData, 1000 * MRC_TIMER_1US); // wait for TotalSampleCounts number of roclk cycles. 1000 us is enough.
  //
  // clear dcdsamplecountstart so that accumulator can be rerun again later
  //
  MrcGetSetChStrb (MrcData, MAX_CONTROLLER, MAX_CHANNEL, MAX_SDRAM_IN_DIMM, GsmIocDcdSampleCountStartData, WriteNoCache, &gGetSetDis);

  for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
    for (Channel = 0; Channel < Outputs->MaxChannels; Channel++) {
      if (!MrcChannelExist (MrcData, Controller, Channel)) {
        continue;
      }
      for (Strobe = 0; Strobe < Outputs->SdramCount; Strobe++) {
        TransChannel = Channel;
        TransStrobe = Strobe;
        MrcTranslateSystemToIp (MrcData, &Controller, &TransChannel, &Rank, &TransStrobe, GsmIocTxOn); // No GetSet for this register, so use another per-byte PHY CR
        Offset = DATA0CH0_CR_DCCCTL13_REG +
                 (DATA0CH1_CR_DCCCTL13_REG - DATA0CH0_CR_DCCCTL13_REG) * TransChannel +
                 (DATA1CH0_CR_DCCCTL13_REG - DATA0CH0_CR_DCCCTL13_REG) * TransStrobe;
        DataDccCtl13.Data = MrcReadCR (MrcData, Offset);
        //
        // sample the accumulator
        //
        DcdSampleCount[Controller][Channel][Strobe] = DataDccCtl13.Bits.dcdsamplecount;
        Data32 = 10000 * (DcdSampleCount[Controller][Channel][Strobe]);
        //
        //multiply totalsamplecounts by 2 to account for rising and falling samples per roclk period
        //
        Data32 = Data32 / (TotalSampleCounts * 2);

        DutyCycle[Controller][Channel][Strobe] = Data32;
#ifdef DCC_DEBUG_PRINT
        DataInt = Data32 / 100;
        MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Mc%u.C%u.S%u  DcdSampleCount %d, DutyCycle %u.%02u\n",
                       Controller, Channel, Strobe, DcdSampleCount[Controller][Channel][Strobe], DataInt, Data32 - (100 * DataInt));
#endif // DCC_DEBUG_PRINT
      } // Strobe
    } // Channel
  } // Controller
#ifdef DCC_DEBUG_PRINT
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\n");
#endif // DCC_DEBUG_PRINT
  return;
}

/**
  This routine runs CLK DCC.

  @param[in]  MrcData                   - Pointer to MRC global data.
  @param[in]  TotalSampleCounts         - Total Sample Counts.
  @param[in]  SelClkInst                - CLK input, the value can be GroupDccClk0, GroupDccClk1, or GroupDccWck.
  @param[in]  AveDutyCycle              - Average Duty Cyle.
  @param[in]  TotalDcdSampleCount       - Total Dcd Sample Count.

  @retval None

**/
VOID
RunDiffDccAccCmd (
  IN MrcParameters *const MrcData,
  IN UINT32               TotalSampleCounts,
  IN DCC_GROUP      const SelClkInst,
  IN UINT32               AveDutyCycle[MRC_NUM_CCC_INSTANCES],
  IN UINT32               TotalDcdSampleCount[MRC_NUM_CCC_INSTANCES]
  )
{
  UINT32             Index;
  UINT32             Controller;
  UINT32             Channel;
  UINT32             ClkpDutyCycle[MRC_NUM_CCC_INSTANCES];
  UINT32             ClkpSampleCount[MRC_NUM_CCC_INSTANCES];
  UINT32             ClknSampleCount[MRC_NUM_CCC_INSTANCES];
  MrcOutput          *Outputs;
  MrcDebug           *Debug;
  UINT32             DataInt;

  Outputs = &MrcData->Outputs;
  Debug = &Outputs->Debug;

  SelDccAccDiffClkCmd (MrcData, SelClkInst, TRUE);

  RunDccAccCmd (MrcData, TotalSampleCounts, ClkpDutyCycle, ClkpSampleCount);

  SelDccAccDiffClkCmd (MrcData, SelClkInst, FALSE);

  RunDccAccCmd (MrcData, TotalSampleCounts, ClkpDutyCycle, ClknSampleCount);

  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Result: Duty cycle\n");

  for (Index = 0; Index < MRC_NUM_CCC_INSTANCES; Index++) {
    DdrIoTranslateCccToMcChannel (MrcData, Index, &Controller, &Channel);
    if (!MrcChannelExist (MrcData, Controller, Channel)) {
      continue;
    }
    TotalDcdSampleCount[Index] = ClkpSampleCount[Index] + ClknSampleCount[Index];
    //
    // Multiply TotalSampleCounts by 2 to account for rising and falling samples per roclk period. As there is CLK P and N.
    // CLKP and N both has rising and falling edge. so the mutiplier value is 4 below.
    //
    AveDutyCycle[Index] = (10000 * TotalDcdSampleCount[Index]) / (TotalSampleCounts * 4);
    DataInt = (AveDutyCycle[Index]) / 100;

    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "CCC%u\t%u.%02u\n", Index, DataInt, ((AveDutyCycle[Index]) - (100 * DataInt)));
  } //Index

  return;
}

/**
  This routine runs CLK DCC.

  @param[in]  MrcData                   - Pointer to MRC global data.
  @param[in]  TotalSampleCounts         - Total Sample Counts.
  @param[in]  DutyCycle                 - Duty Cycle.
  @param[in]  DcdSampleCount            - Dcd Sample Count.

  @retval None

**/
VOID
RunDccAccCmd (
  IN MrcParameters *const MrcData,
  IN UINT32               TotalSampleCounts,
  IN UINT32               DutyCycle[MRC_NUM_CCC_INSTANCES],
  IN UINT32               DcdSampleCount[MRC_NUM_CCC_INSTANCES]
  )
{
  UINT32                    Offset;
  BOOLEAN                   A0;
  MrcInput                  *Inputs;
  UINT32                    Controller;
  UINT32                    Channel;
  UINT32                    Index;
  BOOLEAN                   UlxUlt;
  UINT32                    CccFub;
  UINT32                    Data32;
  CH0CCC_CR_DCCCTL2_STRUCT  CccDccCtl2;
  CH0CCC_CR_DCCCTL19_STRUCT CccDccCtl19;

  Inputs  = &MrcData->Inputs;
  UlxUlt  = Inputs->UlxUlt;
  A0      = Inputs->A0;

#ifdef DCC_DEBUG_PRINT
  UINT32                    DataInt;
  MrcDebug                  *Debug;
  MrcOutput                 *Outputs;
  Outputs = &MrcData->Outputs;
  Debug   = &Outputs->Debug;

  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "RunDccAccCmd:\n\tDcdSampleCount\tDutyCycle\n");
#endif

  Offset = A0 ? CH0CCC_CR_DCCCTL2_A0_REG : CH0CCC_CR_DCCCTL2_REG;
  CccDccCtl2.Data = MrcReadCR (MrcData, Offset);
  CccDccCtl2.Bits.dcdsamplecountrst_b = 0;
  Offset = A0 ? CCC_CR_DCCCTL2_A0_REG : CCC_CR_DCCCTL2_REG;
  MrcWriteCrMulticast (MrcData, Offset, CccDccCtl2.Data);

  Offset = A0 ? CH0CCC_CR_DCCCTL2_A0_REG : CH0CCC_CR_DCCCTL2_REG;
  CccDccCtl2.Data = MrcReadCR (MrcData, Offset);
  CccDccCtl2.Bits.dcdsamplecountrst_b = 1;
  Offset = A0 ? CCC_CR_DCCCTL2_A0_REG : CCC_CR_DCCCTL2_REG;
  MrcWriteCrMulticast (MrcData, Offset, CccDccCtl2.Data);

  Offset = A0 ? CH0CCC_CR_DCCCTL2_A0_REG : CH0CCC_CR_DCCCTL2_REG;
  CccDccCtl2.Data = MrcReadCR (MrcData, Offset);
  CccDccCtl2.Bits.dcdsamplecountstart = 1;
  Offset = A0 ? CCC_CR_DCCCTL2_A0_REG : CCC_CR_DCCCTL2_REG;
  MrcWriteCrMulticast (MrcData, Offset, CccDccCtl2.Data);

  MrcWait(MrcData, 1000 * MRC_TIMER_1US); // wait for TotalSampleCounts number of roclk cycles. 1000 us is enough.
  //
  //clear dcdsamplecountstart so that accumulator can be rerun again later
  //
  Offset = A0 ? CH0CCC_CR_DCCCTL2_A0_REG : CH0CCC_CR_DCCCTL2_REG;
  CccDccCtl2.Data = MrcReadCR (MrcData, Offset);
  CccDccCtl2.Bits.dcdsamplecountstart = 0;
  Offset = A0 ? CCC_CR_DCCCTL2_A0_REG : CCC_CR_DCCCTL2_REG;
  MrcWriteCrMulticast (MrcData, Offset, CccDccCtl2.Data);

  for (Index = 0; Index < MRC_NUM_CCC_INSTANCES; Index++) {
    CccFub = UlxUlt ? Index : CccIndexToFub[Index];
    DdrIoTranslateCccToMcChannel (MrcData, Index, &Controller, &Channel);
    if (!MrcChannelExist (MrcData, Controller, Channel)) {
      continue;
    }
    Offset = OFFSET_CALC_CH (CH2CCC_CR_DCCCTL19_REG, CH0CCC_CR_DCCCTL19_REG, CccFub);
    CccDccCtl19.Data = MrcReadCR (MrcData, Offset);
    //
    //sample the accumulator
    //
    DcdSampleCount[Index] = CccDccCtl19.Bits.dcdsamplecount;
    Data32 = 10000 * (DcdSampleCount[Index]);
    //
    // multiply TotalSampleCounts by 2 to account for rising and falling samples per roclk period
    //
    Data32 = Data32 / (TotalSampleCounts * 2);
    DutyCycle[Index] = Data32;

#ifdef DCC_DEBUG_PRINT
    DataInt = Data32 / 100;
    MRC_DEBUG_MSG (&MrcData->Outputs.Debug, MSG_LEVEL_NOTE, "CCC%u\t%5d\t\t%u.%02u\n", Index, DcdSampleCount[Index], DataInt, Data32 - 100 * DataInt);
#endif
  } // Index
#ifdef DCC_DEBUG_PRINT
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\n");
#endif

  return;
}

/**
  This routine gets the phase detection counts for DQS DCC

  @param[in]  MrcData                   - Pointer to MRC global data.
  @param[in]  numSamples                - The number of samples.
  @param[in]  Prise                     - P rise.
  @param[in]  Pfall                     - P fall.
  @param[in]  MinThreshold              - Min Threshold value.

  @retval None

**/
VOID
GetPDSampleDqs (
  IN MrcParameters *const MrcData,
  IN UINT32               numSamples,
  IN DCC_ALIGN            Prise [MAX_CONTROLLER][MAX_CHANNEL][MAX_SDRAM_IN_DIMM],
  IN DCC_ALIGN            Pfall [MAX_CONTROLLER][MAX_CHANNEL][MAX_SDRAM_IN_DIMM],
  IN UINT32               MinThreshold,
  IN BOOLEAN              Print
  )
{
  MrcOutput                   *Outputs;
  UINT32                      Controller;
  UINT32                      Channel;
  UINT32                      Offset;
  UINT32                      Strobe;
  UINT32                      TransChannel;
  UINT32                      TransStrobe;
  MrcDebug                    *Debug;
  UINT32                      Rank = 0;
  DATA0CH0_CR_SRZCTL_STRUCT   DataSrzCtl;
  UINT32                      SampleCount;
  UINT8                       SampleCountRise = 0;// keeps track of the number of high samples for rise edge
  UINT8                       SampleCountFall = 0;// keeps track of the number of high samples for fall edge

  Outputs = &MrcData->Outputs;
  Debug   = (Print) ? &Outputs->Debug : NULL;
  MrcWait (MrcData, 1 * MRC_TIMER_1US);

  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "GetPDSampleDqs: \n\t\tSampleCountRise\tSampleCountFall\tPrise\tPfall\n");

  for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
    for (Channel = 0; Channel < MAX_CHANNEL; Channel++) {
      if (!MrcChannelExist (MrcData, Controller, Channel)) {
        continue;
      }
      for (Strobe = 0; Strobe < Outputs->SdramCount; Strobe++) {
        TransChannel = Channel;
        TransStrobe = Strobe;
        MrcTranslateSystemToIp (MrcData, &Controller, &TransChannel, &Rank, &TransStrobe, GsmIocTxOn);
        Offset = DATA0CH0_CR_SRZCTL_REG +
                 (DATA0CH1_CR_SRZCTL_REG - DATA0CH0_CR_SRZCTL_REG) * TransChannel +
                 (DATA1CH0_CR_SRZCTL_REG - DATA0CH0_CR_SRZCTL_REG) * TransStrobe;
        SampleCountRise = 0;
        SampleCountFall = 0;
        for (SampleCount = 0; SampleCount < numSamples; SampleCount++) {
          DataSrzCtl.Data = MrcReadCR (MrcData, Offset);
          SampleCountRise = SampleCountRise + ((DataSrzCtl.Bits.omod_func_dqstx_cmn_txpfirstonrise) & 0x1);

          DataSrzCtl.Data = MrcReadCR (MrcData, Offset);
          SampleCountFall = SampleCountFall + (((DataSrzCtl.Bits.omod_func_dqstx_cmn_txpfirstonrise) & 0x2) >> 0x01);
        }
        if (SampleCountRise >= MinThreshold) {
          Prise[Controller][Channel][Strobe] = DccAlignLate;
        } else if ((numSamples - SampleCountRise) >= MinThreshold) {
          Prise[Controller][Channel][Strobe] = DccAlignEarly;
        } else {
          Prise[Controller][Channel][Strobe] = DccAlignDone;
        }

        if (SampleCountFall >= MinThreshold) {
          Pfall[Controller][Channel][Strobe] = DccAlignEarly;
        } else if ((numSamples - SampleCountRise) >= MinThreshold) {
          Pfall[Controller][Channel][Strobe] = DccAlignLate;
        } else {
          Pfall[Controller][Channel][Strobe] = DccAlignDone;
        }
        MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE,
                       "Mc%u.C%u.S%u\t%3d\t\t%3d\t\t%d\t%d\n",
                       Controller, Channel, Strobe, SampleCountRise, SampleCountFall,
                       Prise[Controller][Channel][Strobe], Pfall[Controller][Channel][Strobe]);
      }
    }
  }
  return;
}

/**
  This routine gets the phase detection counts for CLK DCC

  @param[in]  MrcData                   - Pointer to MRC global data.
  @param[in]  SelClkInst                - CLK input, the value can be GroupDccClk0, GroupDccClk1, or GroupDccWck.
  @param[in]  numSamples                - The number of samples.
  @param[in]  Prise                     - P rise.
  @param[in]  Pfall                     - P fall.
  @param[in]  MinThreshold              - Min Threshold value.

  @retval None

**/
VOID
GetPDSampleClk (
  IN MrcParameters *const MrcData,
  IN UINT32               SelClkInst,
  IN UINT32               NumSamples,
  IN DCC_ALIGN            Prise[MRC_NUM_CCC_INSTANCES],
  IN DCC_ALIGN            Pfall[MRC_NUM_CCC_INSTANCES],
  IN UINT32               MinThreshold
  )
{
  UINT32                     Index;
  UINT32                     CccFub;
  UINT32                     Offset;
  MrcInput                   *Inputs;
  MrcDebug                   *Debug;
  MrcOutput                  *Outputs;
  UINT32                     Controller;
  UINT32                     Channel;
  UINT32                     SampleCount;
  UINT32                     TxpFirstonFall;
  UINT32                     TxpFirstonRise;
  UINT32                     TxnFirstonFall;
  UINT32                     TxnFirstonRise;
  BOOLEAN                    UlxUlt;
  CH0CCC_CR_DCCCTL20_STRUCT  CccDccCtl20;

  Inputs  = &MrcData->Inputs;
  Outputs = &MrcData->Outputs;
  Debug   = &Outputs->Debug;
  UlxUlt  = Inputs->UlxUlt;

  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "GetPDSampleClk:\n\tTxpFirstonFall\tTxpFirstonRise\tTxnFirstonFall\tTxnFirstonRise\tPrise\tPfall\n");

  MrcWait (MrcData, 1 * MRC_TIMER_1US);

  for (Index = 0; Index < MRC_NUM_CCC_INSTANCES; Index++) {
    CccFub = UlxUlt ? Index : CccIndexToFub[Index];
    DdrIoTranslateCccToMcChannel (MrcData, Index, &Controller, &Channel);
    if (!MrcChannelExist (MrcData, Controller, Channel)) {
      continue;
    }
    Offset = OFFSET_CALC_CH (CH2CCC_CR_DCCCTL20_REG, CH0CCC_CR_DCCCTL20_REG, CccFub);
    TxpFirstonFall = 0; //keeps track of the number of high samples for CKP-cccp rise edge
    TxpFirstonRise = 0; //keeps track of the number of high samples for CKP-cccp fall edge
    TxnFirstonFall = 0; //keeps track of the number of high samples for CKP-cccn rise edge
    TxnFirstonRise = 0; //keeps track of the number of high samples for CKP-cccn fall edge
    for (SampleCount = 0; SampleCount < NumSamples; SampleCount++) {
      CccDccCtl20.Data = MrcReadCR (MrcData, Offset);
      if (SelClkInst == GroupDccClk0) {
        TxpFirstonFall = TxpFirstonFall + CccDccCtl20.Bits.ccctx_ccc5_txpfirstonfall;
        TxpFirstonRise = TxpFirstonRise + CccDccCtl20.Bits.ccctx_ccc5_txpfirstonrise;
        TxnFirstonFall = TxnFirstonFall + CccDccCtl20.Bits.ccctx_ccc6_txpfirstonfall;
        TxnFirstonRise = TxnFirstonRise + CccDccCtl20.Bits.ccctx_ccc6_txpfirstonrise;
      }
      if ((SelClkInst == GroupDccClk1) || (SelClkInst == GroupDccWck)) {
        TxpFirstonFall = TxpFirstonFall + CccDccCtl20.Bits.ccctx_ccc7_txpfirstonfall;
        TxpFirstonRise = TxpFirstonRise + CccDccCtl20.Bits.ccctx_ccc7_txpfirstonrise;
        TxnFirstonFall = TxnFirstonFall + CccDccCtl20.Bits.ccctx_ccc8_txpfirstonfall;
        TxnFirstonRise = TxnFirstonRise + CccDccCtl20.Bits.ccctx_ccc8_txpfirstonrise;
      }
    }

    if ((TxpFirstonRise + TxnFirstonRise) >= (MinThreshold * 2)) {
      Prise[Index] = DccAlignLate;
    } else if (((2 * NumSamples) - (TxpFirstonRise + TxnFirstonRise)) >= (MinThreshold * 2)) {
      Prise[Index] = DccAlignEarly;
    } else {
      Prise[Index] = DccAlignDone;
    }

    if ((TxpFirstonFall + TxnFirstonFall) >= (MinThreshold * 2)) {
      Pfall[Index] = DccAlignEarly;
    } else if (((2 * NumSamples) - (TxpFirstonFall + TxnFirstonFall)) >= (MinThreshold * 2)) {
      Pfall[Index] = DccAlignLate;
    } else {
      Pfall[Index] = DccAlignDone;
    }

    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE,
                   "CCC%u\t%3d\t\t%3d\t\t%3d\t\t%3d\t\t%d\t%d\n",
                   Index, TxpFirstonFall, TxpFirstonRise, TxnFirstonFall, TxnFirstonRise, Prise[Index], Pfall[Index]);
  } //Index
  return;
}

/**
  This routine adjusts the TCO codes for alignment

  @param[in]  MrcData                   - Pointer to MRC global data.
  @param[in]  Tcop                      - Tco prise or pfall value.
  @param[in]  Tcon                      - Tco nfall or nrise value.
  @param[in]  Pstatus                   - P rise or P fall status
  @param[in]  AlignOnly                 - Align Only or not.

  @retval Saturate
**/
BOOLEAN
AdjustTcoCodesForAlign (
  IN MrcParameters *const MrcData,
  IN UINT8*               Tcop,
  IN UINT8*               Tcon,
  IN DCC_ALIGN            Pstatus,
  IN BOOLEAN              AlignOnly
  )
{
#ifdef DCC_DEBUG_PRINT
  MrcDebug             *Debug;
  MrcOutput            *Outputs;
#endif //if DCC_DEBUG_PRINT
  MrcInput             *Inputs;
  BOOLEAN              Saturate = FALSE;

  Inputs = &MrcData->Inputs;
#ifdef DCC_DEBUG_PRINT
  Outputs = &MrcData->Outputs;
  Debug = &Outputs->Debug;
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "AdjustTcoCodesForAlign in Tcop %d  Tcon %d Pstatus %d\n", *Tcop,
                 *Tcon, Pstatus);
#endif //if DCC_DEBUG_PRINT
  if ((!AlignOnly) || (Inputs->Q0) || (Inputs->G0)) {
    //for Non-Alignonly stepping or Q0 and G0, the range is from 0 to 0x1f for DQS align too.
    if (Pstatus == DccAlignEarly) {
      if (*Tcon < MAX_DCC_TCO_CODE) {
        *Tcon = *Tcon + 1;
      } else if (*Tcop > MIN_DCC_TCO_CODE) {
        *Tcop = *Tcop - 1;
      } else {
        Saturate = TRUE;
      }
    } else if (Pstatus == DccAlignLate) {
      if (*Tcop < MAX_DCC_TCO_CODE) {
        *Tcop = *Tcop + 1;
      } else if (*Tcon > MIN_DCC_TCO_CODE) {
        *Tcon = *Tcon - 1;
      } else {
        Saturate = TRUE;
      }
    }
  } else {
    //
    // Align Only, used for DQS group for < Q0/C0 steppings
    //
    if (Pstatus == DccAlignEarly) {
      if (*Tcon < 0xF) {
        *Tcon = *Tcon + 1;
      } else if (*Tcop > 0) {
        *Tcop = *Tcop - 1;
      } else {
        Saturate = TRUE;
      }
    } else if (Pstatus == DccAlignLate) {
      if (*Tcop < 0xF) {
        *Tcop = *Tcop + 1;
      } else if (*Tcon > 0) {
        *Tcon = *Tcon - 1;
      } else {
        Saturate = TRUE;
      }
    }
  }
#ifdef DCC_DEBUG_PRINT
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "AdjustTcoCodesForAlign out Tcop %d  Tcon %d Pstatus %d Saturate %d\n", *Tcop, *Tcon, Pstatus, Saturate);
#endif //DCC_DEBUG_PRINT
  return Saturate;
}

/**
  This routine adjusts the TCO codes for DCC

  @param[in]  MrcData                   - Pointer to MRC global data.
  @param[in]  TcopRise                  - Tco prise value.
  @param[in]  TcopFall                  - Tco pfall value.
  @param[in]  TconRise                  - Tco nrise value.
  @param[in]  TconFall                  - Tco nfall value.
  @param[in]  DccStatus                 - DCC status, it may be 'high', 'low', or 'align.

  @retval Saturate

**/
BOOLEAN
AdjustTcoCodesForDcc (
  IN OUT MrcParameters *const MrcData,
  IN UINT8*                   TcopRise,
  IN UINT8*                   TcopFall,
  IN UINT8*                   TconRise,
  IN UINT8*                   TconFall,
  IN DCC_STATUS               DccStatus
  )
{
  BOOLEAN Saturate = FALSE;
#ifdef DCC_DEBUG_PRINT
  MrcDebug             *Debug;
  MrcOutput            *Outputs;
  Outputs = &MrcData->Outputs;
  Debug = &Outputs->Debug;

  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "AdjustTcoCodesForDcc in TcopRise %d  TcopFall %d TconRise %d TconFall %d DccStatus %d\n", *TcopRise, *TcopFall, *TconRise, * TconFall, DccStatus);
#endif //#ifdef DCC_DEBUG_PRINT
  //
  // dccstatus may be 'high', 'low', or 'align'
  //
  if (DccStatus == DccHigh) {
    if ((*TcopFall < MAX_DCC_TCO_CODE) && (*TconRise < MAX_DCC_TCO_CODE)) {
      *TcopFall += 1;
      *TconRise += 1;
    } else if ((*TcopRise > MIN_DCC_TCO_CODE) && (*TconFall > MIN_DCC_TCO_CODE)) {
      *TcopRise -= 1;
      *TconFall -= 1;
    } else {
      Saturate = TRUE;
    }
  } else if (DccStatus == DccLow) {
    if ((*TcopRise < MAX_DCC_TCO_CODE) && (*TconFall < MAX_DCC_TCO_CODE)) {
      *TcopRise += 1;
      *TconFall += 1;
    } else if ((*TcopFall > MIN_DCC_TCO_CODE) && (*TconRise > MIN_DCC_TCO_CODE)) {
      *TcopFall -= 1;
      *TconRise -= 1;
    } else {
      Saturate = TRUE;
    }
  }
#ifdef DCC_DEBUG_PRINT
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE,
                 "AdjustTcoCodesForDcc out TcopRise %d  TcopFall %d TconRise %d TconFall %d DccStatus %d Saturate %d\n", *TcopRise,
                 *TcopFall, *TconRise, * TconFall, DccStatus, Saturate);
#endif //DCC_DEBUG_PRINT
  return Saturate;
}

/**
  This routine sets the DCC Dqs codes

  @param[in]  MrcData                   - Pointer to MRC global data.
  @param[in]  TcopRise                  - Tco prise value.
  @param[in]  TcopFall                  - Tco pfall value.
  @param[in]  TconRise                  - Tco nrise value.
  @param[in]  TconFall                  - Tco nfall value.
  @param[in]  AlignOnly                 - Align only or not.
  @param[in]  Controller                - Controller value.
  @param[in]  Channel                   - Channel value.
  @param[in]  Strobe                    - Strobe value.

  @retval None

**/
VOID
SetDiffDccDqsCodes (
  IN MrcParameters *const MrcData,
  IN UINT32               TcopRise,
  IN UINT32               TcopFall,
  IN UINT32               TcoNRise,
  IN UINT32               TcoNFall,
  IN BOOLEAN              AlignOnly,
  IN UINT32               Controller,
  IN UINT32               Channel,
  IN UINT32               Strobe,
  IN BOOLEAN              Print
  )
{
  MrcOutput                      *Outputs;
  MrcDebug                       *Debug;
  DATA0CH0_CR_DQSTXRXCTL_STRUCT  DataDqsTxRxCtl;
  DATA0CH0_CR_DQSTXRXCTL0_STRUCT DataDqsTxRxCtl0;
  UINT32                         Offset;
  UINT32                         TransChannel;
  UINT32                         TransStrobe;
  BOOLEAN                        A0;
  BOOLEAN                        B0;
  BOOLEAN                        J0;
  UINT32                         Rank;
  MrcInput                       *Inputs;
  UINT32                         PrnfDelay = 0; //P rise N fall delay
  UINT32                         PfnrDelay = 0; //P fall N rising delay

  Outputs = &MrcData->Outputs;
  Debug   = (Print) ? &Outputs->Debug : NULL;
  Inputs = &MrcData->Inputs;

  A0      = Inputs->A0;
  B0      = Inputs->B0;
  J0      = Inputs->J0;
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "pRise %2d  pFall %2d nRise %2d nFall %2d\n", TcopRise, TcopFall, TcoNRise, TcoNFall);
  //
  //  clock align only, must be True for steppings less than Q0/C0
  //
  if (AlignOnly && (A0 || B0 || J0)) {
    if (TcopRise == 0xf) {
      PrnfDelay = 31 - TcoNFall;
    } else if (TcoNFall == 0xf) {
      PrnfDelay = TcopRise;
    } else {
      MRC_DEBUG_MSG (Debug, MSG_LEVEL_ERROR, "%s for silicon stepping less than C0 and Q0, either %s or %s must be 15\n", gErrString, "TcoPRise", "TcoNFall");
    }

    if (TcoNRise == 0xf) {
      PfnrDelay = 31 - TcopFall;
    } else if (TcopFall == 0xf) {
      PfnrDelay = TcoNRise;
    } else {
      MRC_DEBUG_MSG (Debug, MSG_LEVEL_ERROR, "%s for silicon stepping less than C0 and Q0, either %s or %s must be 15\n", gErrString, "TcopFall", "TcoNRise");
    }

    TransChannel = Channel;
    TransStrobe = Strobe;
    MrcTranslateSystemToIp (MrcData, &Controller, &TransChannel, &Rank, &TransStrobe, RxDqsEq);

    Offset = DATA0CH0_CR_DQSTXRXCTL_REG +
             (DATA0CH1_CR_DQSTXRXCTL_REG - DATA0CH0_CR_DQSTXRXCTL_REG) * TransChannel +
             (DATA1CH0_CR_DQSTXRXCTL_REG - DATA0CH0_CR_DQSTXRXCTL_REG) * TransStrobe;
    DataDqsTxRxCtl.Data = MrcReadCR (MrcData, Offset);
    DataDqsTxRxCtl.Bits.dqstx_cmn_txdqsprnfdelay = PrnfDelay;
    DataDqsTxRxCtl.Bits.dqstx_cmn_txdqspfnrdelay = PfnrDelay;
    MrcWriteCR (MrcData, Offset, DataDqsTxRxCtl.Data);
  } else {

    if ((!A0) && (!B0) && (!J0)) {
      TransChannel = Channel;
      TransStrobe = Strobe;
      MrcTranslateSystemToIp (MrcData, &Controller, &TransChannel, &Rank, &TransStrobe, RxDqsEq);

      Offset = DATA0CH0_CR_DQSTXRXCTL_REG +
               (DATA0CH1_CR_DQSTXRXCTL_REG - DATA0CH0_CR_DQSTXRXCTL_REG) * TransChannel +
               (DATA1CH0_CR_DQSTXRXCTL_REG - DATA0CH0_CR_DQSTXRXCTL_REG) * TransStrobe;

      DataDqsTxRxCtl.Data = MrcReadCR (MrcData, Offset);
      DataDqsTxRxCtl.BitsQ0.dqstx_cmn_txdqsprisedelay = TcopRise;
      DataDqsTxRxCtl.BitsQ0.dqstx_cmn_txdqspfalldelay = TcopFall;
      MrcWriteCR (MrcData, Offset, DataDqsTxRxCtl.Data);


      Offset = DATA0CH0_CR_DQSTXRXCTL0_REG +
               (DATA0CH1_CR_DQSTXRXCTL0_REG - DATA0CH0_CR_DQSTXRXCTL0_REG) * TransChannel +
               (DATA1CH0_CR_DQSTXRXCTL0_REG - DATA0CH0_CR_DQSTXRXCTL0_REG) * TransStrobe;

      DataDqsTxRxCtl0.Data = MrcReadCR (MrcData, Offset);
      DataDqsTxRxCtl0.BitsQ0.dqstx_cmn_txdqsnrisedelay = TcoNRise;
      DataDqsTxRxCtl0.BitsQ0.dqstx_cmn_txdqsnfalldelay = TcoNFall;
      MrcWriteCR (MrcData, Offset, DataDqsTxRxCtl0.Data);
    }
  }
  return;
}

/**
  This routine sets the DCC Clk codes

  @param[in]  MrcData                   - Pointer to MRC global data.
  @param[in]  TcopRise                  - Tco prise value.
  @param[in]  TcopFall                  - Tco pfall value.
  @param[in]  TconRise                  - Tco nrise value.
  @param[in]  TconFall                  - Tco nfall value.
  @param[in]  SelClkInst                - CLK input, the value can be GroupDccClk0, GroupDccClk1, or GroupDccWck..
  @param[in]  Index                     - CCC partion index.

  @retval None

**/
VOID
SetDiffDccClkCodes (
  IN MrcParameters *const MrcData,
  IN UINT32               TcopRise,
  IN UINT32               TcopFall,
  IN UINT32               TconRise,
  IN UINT32               TconFall,
  IN UINT32               SelClkInst,
  IN UINT32               Index
  )
{

  MrcInput  *Inputs;
  UINT32    CccFub;
  UINT32    Offset;
  MrcOutput *Outputs;
  MrcDebug  *Debug;
  BOOLEAN   UlxUlt;
  CCC0_GLOBAL_CR_TCOCOMP_0_STRUCT Ccc0GlobalTcoComp0;
  CCC0_GLOBAL_CR_TCOCOMP_2_STRUCT Ccc0GlobalTcoComp2;
  CCC0_GLOBAL_CR_TCOCOMP_1_STRUCT Ccc0GlobalTcoComp1;

  Inputs  = &MrcData->Inputs;
  Outputs = &MrcData->Outputs;
  Debug   = &Outputs->Debug;
  UlxUlt = Inputs->UlxUlt;

  CccFub = UlxUlt ? Index : CccIndexToFub[Index];
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, " pRise %d  pFall %d  nRise %d  nFall %d\n", TcopRise, TcopFall, TconRise, TconFall);
  //
  // programs tcocomp codes for differential clock DCC for CK and WCK
  // SelClkInst and selCh is based on partition instance/buffer to signal technology map
  // SelClkInst may be clk0, clk1, or wck
  //
  Offset = OffsetTcoComp01[CccFub];
  switch (CccFub) {
    case 1:
    case 3:
    case 5:
    case 7:
      Ccc0GlobalTcoComp1.Data = MrcReadCR (MrcData, Offset);
      if (SelClkInst == GroupDccClk0) {
        Ccc0GlobalTcoComp1.Bits.ccctx_ccc5_txtcocompn_top1 = TcopFall;
        Ccc0GlobalTcoComp1.Bits.ccctx_ccc5_txtcocompp_top1 = TcopRise;
        Ccc0GlobalTcoComp1.Bits.ccctx_ccc6_txtcocompn_top1 = TconFall;
        Ccc0GlobalTcoComp1.Bits.ccctx_ccc6_txtcocompp_top1 = TconRise;
      } else if ((SelClkInst == GroupDccClk1) || (SelClkInst == GroupDccWck)) {
        Ccc0GlobalTcoComp1.Bits.ccctx_ccc7_txtcocompn_top1 = TcopFall;
        Ccc0GlobalTcoComp1.Bits.ccctx_ccc7_txtcocompp_top1 = TcopRise;
      }
      MrcWriteCR (MrcData, Offset, Ccc0GlobalTcoComp1.Data);

      if ((SelClkInst == GroupDccClk1) || (SelClkInst == GroupDccWck)) {
        Offset = OffsetTcoComp2[CccFub];
        Ccc0GlobalTcoComp2.Data = MrcReadCR (MrcData, Offset);
        Ccc0GlobalTcoComp2.Bits.ccctx_ccc8_txtcocompn_top1 = TconFall;
        Ccc0GlobalTcoComp2.Bits.ccctx_ccc8_txtcocompp_top1 = TconRise;
        MrcWriteCR (MrcData, Offset, Ccc0GlobalTcoComp2.Data);
      }
      break;
    case 0:
    case 2:
    case 4:
    case 6:
      Ccc0GlobalTcoComp0.Data = MrcReadCR (MrcData, Offset);
      if (SelClkInst == GroupDccClk0) {
        Ccc0GlobalTcoComp0.Bits.ccctx_ccc5_txtcocompn_top0 = TcopFall;
        Ccc0GlobalTcoComp0.Bits.ccctx_ccc5_txtcocompp_top0 = TcopRise;
        Ccc0GlobalTcoComp0.Bits.ccctx_ccc6_txtcocompn_top0 = TconFall;
        Ccc0GlobalTcoComp0.Bits.ccctx_ccc6_txtcocompp_top0 = TconRise;
      } else if ((SelClkInst == GroupDccClk1) || (SelClkInst == GroupDccWck)) {
        Ccc0GlobalTcoComp0.Bits.ccctx_ccc7_txtcocompn_top0 = TcopFall;
        Ccc0GlobalTcoComp0.Bits.ccctx_ccc7_txtcocompp_top0 = TcopRise;
      }
      MrcWriteCR (MrcData, Offset, Ccc0GlobalTcoComp0.Data);

      if ((SelClkInst == GroupDccClk1) || (SelClkInst == GroupDccWck)) {
        Offset = OffsetTcoComp2[CccFub];
        Ccc0GlobalTcoComp2.Data = MrcReadCR (MrcData, Offset);
        Ccc0GlobalTcoComp2.Bits.ccctx_ccc8_txtcocompn_top0 = TconFall;
        Ccc0GlobalTcoComp2.Bits.ccctx_ccc8_txtcocompp_top0 = TconRise;
        MrcWriteCR (MrcData, Offset, Ccc0GlobalTcoComp2.Data);
      }
      break;
    default:
      break;
  }
  return;
}


/**
  This routine runs DCC for CLK and DQS

  @param[in]  MrcData - Pointer to MRC global data.
**/
VOID
RunAllDiffDcc (
  IN OUT MrcParameters *const MrcData
  )
{
  MrcDebug            *Debug;
  MrcOutput           *Outputs;
  INT64               GetSetVal;
  UINT8               FirstController;
  UINT8               FirstChannel;
  UINT32              TotalSampleCounts;
  INT64               InternalClocksOnSave;
  MrcInput            *Inputs;
  BOOLEAN             Lpddr4;
  BOOLEAN             Lpddr5;

  Inputs  = &MrcData->Inputs;
  Outputs = &MrcData->Outputs;
  Debug   = &Outputs->Debug;
  TotalSampleCounts = TOTAL_SAMPLE_COUNTS_VALUE;
  Lpddr4 = (Outputs->DdrType == MRC_DDR_TYPE_LPDDR4);
  Lpddr5 = (Outputs->DdrType == MRC_DDR_TYPE_LPDDR5);

  if ((Inputs->A0) || (Inputs->G0)) {
    return;
  }

  FirstController = Outputs->FirstPopController;
  FirstChannel    = Outputs->Controller[FirstController].FirstPopCh;

  // Set InternalClocksOn to avoid clock gating
  MrcGetSetChStrb (MrcData, FirstController, FirstChannel, 0, GsmIocInternalClocksOn, ReadFromCache, &InternalClocksOnSave);
  GetSetVal = 1;
  MrcGetSetChStrb (MrcData, MAX_CONTROLLER, MAX_CHANNEL, MAX_SDRAM_IN_DIMM, GsmIocInternalClocksOn, WriteCached, &GetSetVal);

  if (!Lpddr5) {
    //Not run DQS alignment and DCC for LP5.
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\n-- RunAllDiffDcc %s start\n", "DQS");
    RunDiffClkDcc (MrcData, TotalSampleCounts, GroupDccDqs, FALSE, NULL, NULL, TRUE);

    //LP5 doesn't use CLK0
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\n-- RunAllDiffDcc %s start\n", "CLK0");
    RunDiffClkDcc (MrcData, TotalSampleCounts, GroupDccClk0, FALSE, NULL, NULL, TRUE);
  }

  if (!Lpddr4) {
    //LP4 doesn't use CLK 1 (WCK)
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\n-- RunAllDiffDcc %s start\n", "CLK1");
    RunDiffClkDcc (MrcData, TotalSampleCounts, GroupDccClk1, FALSE, NULL, NULL, TRUE);
  }

  // Restore InternalClocksOn
  MrcGetSetChStrb (MrcData, MAX_CONTROLLER, MAX_CHANNEL, MAX_SDRAM_IN_DIMM, GsmIocInternalClocksOn, WriteCached, &InternalClocksOnSave);
}

/**
  This routine saves the CLK TCO codes.

  @param[in]  MrcData                   - Pointer to MRC global data.
  @param[in]  TcopRise                  - Stroage to save Tco prise value.
  @param[in]  TcopFall                  - Stroage to save Tco pfall value.
  @param[in]  TconRise                  - Stroage to save Tco nrise value.
  @param[in]  TconFall                  - Stroage to save Tco nfall value.

  @retval None

**/
VOID
SaveDiffDccClkCodes (
  IN MrcParameters *const MrcData,
  IN UINT32               TcopRise[MRC_NUM_CCC_INSTANCES][2],
  IN UINT32               TcopFall[MRC_NUM_CCC_INSTANCES][2],
  IN UINT32               TconRise[MRC_NUM_CCC_INSTANCES][2],
  IN UINT32               TconFall[MRC_NUM_CCC_INSTANCES][2]
  )
{
  MrcInput  *Inputs;
  UINT32    Offset;
  UINT8     Index;
  UINT8     ClkNum;
  UINT32    CccFub;
  UINT32    SelClkInst;
  CCC0_GLOBAL_CR_TCOCOMP_0_STRUCT Ccc0GlobalTcoComp0;
  CCC0_GLOBAL_CR_TCOCOMP_2_STRUCT Ccc0GlobalTcoComp2;
  CCC0_GLOBAL_CR_TCOCOMP_1_STRUCT Ccc0GlobalTcoComp1;

  Inputs  = &MrcData->Inputs;

  SelClkInst = 0;
  for (ClkNum = 0; ClkNum < 2; ClkNum++) {
    for (Index = 0; Index < MRC_NUM_CCC_INSTANCES; Index++) {
      if (ClkNum == 0) {
        SelClkInst = GroupDccClk0;
      } else if (ClkNum == 1) {
        SelClkInst = GroupDccClk1;
      }

      CccFub = (Inputs->UlxUlt) ? Index : CccIndexToFub[Index];
      Offset = OffsetTcoComp01[CccFub];
      switch (CccFub) {
        case 1:
        case 3:
        case 5:
        case 7:
          Ccc0GlobalTcoComp1.Data = MrcReadCR (MrcData, Offset);
          if (SelClkInst == GroupDccClk0) {
            TcopFall[Index][ClkNum] = Ccc0GlobalTcoComp1.Bits.ccctx_ccc5_txtcocompn_top1;
            TcopRise[Index][ClkNum] = Ccc0GlobalTcoComp1.Bits.ccctx_ccc5_txtcocompp_top1;
            TconFall[Index][ClkNum] = Ccc0GlobalTcoComp1.Bits.ccctx_ccc6_txtcocompn_top1;
            TconRise[Index][ClkNum] = Ccc0GlobalTcoComp1.Bits.ccctx_ccc6_txtcocompp_top1;
          } else if ((SelClkInst == GroupDccClk1) || (SelClkInst == GroupDccWck)) {
            TcopFall[Index][ClkNum] = Ccc0GlobalTcoComp1.Bits.ccctx_ccc7_txtcocompn_top1;
            TcopRise[Index][ClkNum] = Ccc0GlobalTcoComp1.Bits.ccctx_ccc7_txtcocompp_top1;
            Offset = OffsetTcoComp2[CccFub];
            Ccc0GlobalTcoComp2.Data = MrcReadCR (MrcData, Offset);
            TconFall[Index][ClkNum] = Ccc0GlobalTcoComp2.Bits.ccctx_ccc8_txtcocompn_top1;
            TconRise[Index][ClkNum] = Ccc0GlobalTcoComp2.Bits.ccctx_ccc8_txtcocompp_top1;
          }
          break;
        case 0:
        case 2:
        case 4:
        case 6:
          Ccc0GlobalTcoComp0.Data = MrcReadCR (MrcData, Offset);
          if (SelClkInst == GroupDccClk0) {
            TcopFall[Index][ClkNum] = Ccc0GlobalTcoComp0.Bits.ccctx_ccc5_txtcocompn_top0;
            TcopRise[Index][ClkNum] = Ccc0GlobalTcoComp0.Bits.ccctx_ccc5_txtcocompp_top0;
            TconFall[Index][ClkNum] = Ccc0GlobalTcoComp0.Bits.ccctx_ccc6_txtcocompn_top0;
            TconRise[Index][ClkNum] = Ccc0GlobalTcoComp0.Bits.ccctx_ccc6_txtcocompp_top0;
          } else if ((SelClkInst == GroupDccClk1) || (SelClkInst == GroupDccWck)) {
            TcopFall[Index][ClkNum] = Ccc0GlobalTcoComp0.Bits.ccctx_ccc7_txtcocompn_top0;
            TcopRise[Index][ClkNum] = Ccc0GlobalTcoComp0.Bits.ccctx_ccc7_txtcocompp_top0;
            Offset = OffsetTcoComp2[CccFub];
            Ccc0GlobalTcoComp2.Data = MrcReadCR (MrcData, Offset);
            TconFall[Index][ClkNum] = Ccc0GlobalTcoComp2.Bits.ccctx_ccc8_txtcocompn_top0;
            TconRise[Index][ClkNum] = Ccc0GlobalTcoComp2.Bits.ccctx_ccc8_txtcocompp_top0;
          }
          break;
        default:
          break;
      }
    }
  }
  return;
}

/**
  Perform Rx PRE SDL DCC Training.
  Runs differential DCC on the DQSP/N Rx.
  Detects duty cycle at the output of the Rx just before the RxSDL input
  Corrects the duty cycle at the output of the Rx regardless of the Rx input differential duty cycle
  For DDR5, duty cycle is corrected at the DQS Rx pad input using the DDR5 DRAM DCA feature
  This procedure should be run per rank for every populated channel and byte

  @param[in]  MrcData - Pointer to MRC global data.

  @retval MrcStatus
**/
MrcStatus
MrcRxPreSdlDccTraining (
  IN OUT MrcParameters *const MrcData
  )
{
  MrcStatus          Status;
  MrcOutput          *Outputs;
  MrcDebug           *Debug;
  UINT32             Controller;
  UINT32             Channel;
  UINT32             Strobe;
  UINT32             Rank;
  BOOLEAN            Ddr5;
  BOOLEAN            Lpddr5;
  UINT8              RxPreSdlTrainingLoop;
  UINT32             RxVocValue [MAX_CONTROLLER][MAX_CHANNEL][MAX_RANK_IN_CHANNEL][MAX_SDRAM_IN_DIMM][RX_PRE_SDL_DCC_LOOP];
  UINT32             MinRxVocValue;
  UINT32             MaxRxVocValue;
  UINT32             FinalRxVocValue;
  UINT8              ValidRankMask;
  UINT8              RankMask;
  UINT8              MaxPreSdlDccLoop;
  BOOLEAN            RunJedecReset;

  Status  = mrcSuccess;
  Outputs = &MrcData->Outputs;
  Debug   = &Outputs->Debug;
  Ddr5    = (Outputs->DdrType == MRC_DDR_TYPE_DDR5);
  Lpddr5  = (Outputs->DdrType == MRC_DDR_TYPE_LPDDR5);
  ValidRankMask = Outputs->ValidRankMask;
  RunJedecReset = FALSE;

  if ((!Ddr5 && !Lpddr5) || (Outputs->Frequency <= 3200)) {
    // only run it for DDR5 or LP5 and freq > 3200
    return Status;
  }

  if ((Ddr5) && (!(Outputs->Any2Dpc))) {
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Run it with Jedec Reset in DDR5 1DPC configuration\n");
    MaxPreSdlDccLoop = RX_PRE_SDL_DCC_LOOP;
    RunJedecReset = TRUE;
  } else {
    MaxPreSdlDccLoop = 1;
    RunJedecReset = FALSE;
  }

  for (RxPreSdlTrainingLoop = 0; RxPreSdlTrainingLoop < MaxPreSdlDccLoop; RxPreSdlTrainingLoop++) {
    Status = MrcRxSdlDccTraining (MrcData, RxPreSdlDcc);
    if (RunJedecReset) {
      MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "The Rx PRE SDL DCC training trained value:\n");
      for (Rank = 0; Rank < MAX_RANK_IN_CHANNEL; Rank++) {
        RankMask = (1 << Rank);
        if ((RankMask & ValidRankMask) == 0) {
          // Skip if this rank is not present on any of the channels
          continue;
        }

        for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
          for (Channel = 0; Channel < Outputs->MaxChannels; Channel++) {
            if (!(MrcRankExist (MrcData, Controller, Channel, Rank))) {
              continue;
            }
            for (Strobe = 0; Strobe < Outputs->SdramCount; Strobe++) {
              GetRxDqsVoc (MrcData, Controller, Channel, Rank, Strobe, MRC_PRINTS_ON, RxPreSdlTrainingLoop, RxVocValue);
            }
          }
        }
      }
      Status = MrcResetSequence (MrcData);
    }
  } // RxPreSdlTrainingLoop

  //handle the training result
  if (RunJedecReset) {
    for (Rank = 0; Rank < MAX_RANK_IN_CHANNEL; Rank++) {
      RankMask = (1 << Rank);
      if ((RankMask & ValidRankMask) == 0) {
        // Skip if this rank is not present on any of the channels
        continue;
      }

      for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
        for (Channel = 0; Channel < Outputs->MaxChannels; Channel++) {
          if (!(MrcRankExist (MrcData, Controller, Channel, Rank))) {
            continue;
          }
          for (Strobe = 0; Strobe < Outputs->SdramCount; Strobe++) {
            MinRxVocValue = RxVocValue [Controller][Channel][Rank][Strobe][0];
            MaxRxVocValue = RxVocValue [Controller][Channel][Rank][Strobe][0];
            for (RxPreSdlTrainingLoop = 0; RxPreSdlTrainingLoop < RX_PRE_SDL_DCC_LOOP; RxPreSdlTrainingLoop++) {
              if (MinRxVocValue > RxVocValue [Controller][Channel][Rank][Strobe][RxPreSdlTrainingLoop]) {
                MinRxVocValue = RxVocValue [Controller][Channel][Rank][Strobe][RxPreSdlTrainingLoop];
              }
              if (MaxRxVocValue < RxVocValue [Controller][Channel][Rank][Strobe][RxPreSdlTrainingLoop]) {
                MaxRxVocValue = RxVocValue [Controller][Channel][Rank][Strobe][RxPreSdlTrainingLoop];
              }
            }
            FinalRxVocValue = (MinRxVocValue + MaxRxVocValue) / 2;
            MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "R%u MC%u C%u S%u Final Voc: %u, Min Voc: %u, Max Voc: %u\n", Rank, Controller,
                           Channel, Strobe, FinalRxVocValue, MinRxVocValue, MaxRxVocValue);
            SetRxDqsVoc (MrcData, Controller, Channel, Rank, Strobe, (UINT8)FinalRxVocValue, MRC_PRINTS_ON);
          }
        }
      }
    }
  }
  return Status;
}

#if 0
/**
  Perform Rx PRE SDL DCC Training.
  Runs differential DCC on the DQSP/N Rx.
  Detects duty cycle at the output of the Rx just before the RxSDL input
  Corrects the duty cycle at the output of the Rx regardless of the Rx input differential duty cycle
  For DDR5, duty cycle is corrected at the DQS Rx pad input using the DDR5 DRAM DCA feature
  This procedure should be run per rank for every populated channel and byte

  @param[in]  MrcData - Pointer to MRC global data.

  @retval MrcStatus
**/
MrcStatus
MrcRxPreSdlDccTraining (
  IN OUT MrcParameters *const MrcData
  )
{
  const MrcInput        *Inputs;
  MrcDebug              *Debug;
  const MRC_FUNCTION    *MrcCall;
  MrcOutput             *Outputs;
  MrcChannelOut         *ChannelOut;
  MrcStatus             Status;
  MrcProfile            Profile;
  MrcDdrType            DdrType;
  MRC_PATTERN_CTL       PatternCtl;
  UINT8                 Controller;
  UINT8                 MaxChannels;
  UINT8                 Channel;
  UINT32                Rank;
  UINT32                Strobe;
  UINT32                FirstController;
  UINT32                FirstChannel;
  UINT8                 RankMask;
  UINT8                 McChBitMask;
  UINT8                 ValidRankMask;  // RankBitMask for both channels
  UINT32                Gear2;
  UINT8                 NumCL;
  UINT8                 SamplesAdd;
  INT64                 RcvEnStartDly;
  UINT8                 NumSamples;
  INT64                 GetSetVal;
  INT64                 GetSetEn;
  INT64                 GetSetDis;
  UINT8                 RxDqsVocCode[MAX_CONTROLLER][MAX_CHANNEL][MAX_SDRAM_IN_DIMM];
  UINT8                 PrevRxDqsVocCode[MAX_CONTROLLER][MAX_CHANNEL][MAX_SDRAM_IN_DIMM];
  UINT8                 CurrentRxDqsVocCode[MAX_CONTROLLER][MAX_CHANNEL][MAX_SDRAM_IN_DIMM];
  BOOLEAN               DccCodeSaturate[MAX_CONTROLLER][MAX_CHANNEL][MAX_SDRAM_IN_DIMM];
  DCC_STATUS            DccStatusPrev[MAX_CONTROLLER][MAX_CHANNEL][MAX_SDRAM_IN_DIMM];
  DCC_STATUS            DccStatus[MAX_CONTROLLER][MAX_CHANNEL][MAX_SDRAM_IN_DIMM];
  UINT32                DqsFdbkOnesCount[MAX_CONTROLLER][MAX_CHANNEL][MAX_SDRAM_IN_DIMM];
  BOOLEAN               DccDone[MAX_CONTROLLER][MAX_CHANNEL][MAX_SDRAM_IN_DIMM];
  INT32                 PrevDutyCycle[MAX_CONTROLLER][MAX_CHANNEL][MAX_SDRAM_IN_DIMM];
  UINT8                 DccResultPassStrobe[MAX_CONTROLLER][MAX_CHANNEL][MAX_SDRAM_IN_DIMM];
  BOOLEAN               DccResultPassCh[MAX_CONTROLLER][MAX_CHANNEL];
  BOOLEAN               DccResultPassController[MAX_CONTROLLER];
  BOOLEAN               DccAllControllerPass;
  BOOLEAN               AllStrobePass;
  BOOLEAN               AllChPass;
  UINT32                OnesCount;
  INT32                 DutyCycle;
  UINT32                DccCorrectionCount;
  UINT8                 DqsPiSweep;
  UINT8                 CurrentVocCode;
  UINT32                DataInt;
  UINT32                DataIntTemp;
  static const Cpgc20Address CPGCAddress = {
    CPGC20_BANK_2_ROW_COL_2_RANK,
    CPGC20_FAST_Y,
    0,
    0,
    0,
    0,
    5,
    1 // Banks
  };
  static const Cpgc20Address CPGCAddressDdr = {
    CPGC20_BANK_2_ROW_COL_2_RANK,
    CPGC20_FAST_Y,
    0,
    0,
    0,
    0,
    6,
    2 // Banks
  };
  const Cpgc20Address   *CpgcAddress;
  INT64                 RxIoTclInit;
  INT64                 DataTrainFeedbackField;
  INT64                 ForceRxAmpOnSave;
  INT64                 ForceOdtOnSave;
  UINT32                Tcl;
  INT64                 EnDqsNRcvEnSave;
  INT64                 EnDqsNRcvEnGateSave;
  BOOLEAN               Pass;
  BOOLEAN               Lpddr;
  BOOLEAN               Lpddr5;
  BOOLEAN               Ddr4;
  BOOLEAN               Ddr5;
  DDR4_MODE_REGISTER_3_STRUCT Ddr4Mr3;
  DDR4_MODE_REGISTER_4_STRUCT Ddr4Mr4;
  DDR5_MODE_REGISTER_2_TYPE   Ddr5Mr2;

  Inputs                = &MrcData->Inputs;
  MrcCall               = Inputs->Call.Func;
  Outputs               = &MrcData->Outputs;
  Debug                 = &Outputs->Debug;
  DdrType               = Outputs->DdrType;
  ValidRankMask         = Outputs->ValidRankMask;
  Profile               = Inputs->MemoryProfile;
  Status                = mrcSuccess;
  GetSetEn              = 1;
  GetSetDis             = 0;
  Ddr4                  = (DdrType == MRC_DDR_TYPE_DDR4);
  Ddr5                  = (DdrType == MRC_DDR_TYPE_DDR5);
  Lpddr5                = (DdrType == MRC_DDR_TYPE_LPDDR5);
  Lpddr                 = Outputs->Lpddr;
  PatternCtl.IncRate    = 32;
  PatternCtl.Start      = 0;
  PatternCtl.Stop       = 9;
  PatternCtl.DQPat      = BasicVA;
  PatternCtl.PatSource  = MrcPatSrcStatic;
  Tcl                   = 0;
  Gear2                 = Outputs->Gear2 ? 1 : 0;
  MaxChannels           = Outputs->MaxChannels;
  EnDqsNRcvEnSave       = 0xFF;
  EnDqsNRcvEnGateSave   = 0xFF;

  NumSamples = 6;
  NumCL      = 8; // gives us about tCL*2 + flyby + NumCL*8 + 2(preamble) ~ 86QClk till last dqs (the init RTL should be big enough to overcome this delay !)
  Gear2      = Outputs->Gear2 ? 1 : 0;

  SamplesAdd = MrcLog2 (NumCL) - 1;  // The log function return +1 so we subtract 1
  CpgcAddress = Lpddr ? &CPGCAddress : &CPGCAddressDdr;

  // Read the first populated channel
  FirstController = Outputs->FirstPopController;
  FirstChannel    = Outputs->Controller[FirstController].FirstPopCh;

  // Read the initialized value of ForceRxAmpOn and ForceOdtOn for first populated byte
  MrcGetSetChStrb (MrcData, FirstController, FirstChannel, 0, GsmIocForceRxAmpOn, ReadFromCache, &ForceRxAmpOnSave);
  MrcGetSetChStrb (MrcData, FirstController, FirstChannel, 0, GsmIocForceOdtOn,   ReadFromCache, &ForceOdtOnSave);

  // CmdPat=PatRd, NumCL, LC, CpgcAddress, SOE=0, PatternCtl, EnCADB=0, EnCKE=0, SubSeqWait=128
  SetupIOTest (MrcData, Outputs->McChBitMask, PatRd, NumCL, NumSamples + SamplesAdd, CpgcAddress, NSOE, &PatternCtl, 0, 0, 128);

  for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
    for (Channel = 0; Channel < MaxChannels; Channel++) {
      if (!MrcChannelExist (MrcData, Controller, Channel)) {
        continue;
      }
      ChannelOut = &Outputs->Controller[Controller].Channel[Channel];

      // Enable ReadLeveling Mode, Force On ODT and SenseAmp
      // Force on SenseAmp
      MrcGetSetChStrb (MrcData, Controller, Channel, MAX_SDRAM_IN_DIMM, GsmIocForceRxAmpOn, WriteToCache, &GetSetEn);
      MrcGetSetChStrb (MrcData, Controller, Channel, MAX_SDRAM_IN_DIMM, GsmIocForceOdtOn,   WriteToCache, &GetSetEn);

      // Get tCL.  Needed for FIFO timing.  Both channels have the same tCL.
      Tcl = ChannelOut->Timing[Profile].tCL;

      if (Lpddr5) {
        Tcl *= 4;  // DCLK to WCK
      }
      // Set initial RcvEn knobs.
      // Channel is programmed in the Rank loop since we need to reset it on each rank trained.
      GetSetVal = MRC_RX_FLYBY_INIT_PRE_SDL_DCC;
      MrcGetSetMcChRnk (MrcData, Controller, Channel, MAX_RANK_IN_CHANNEL, RxFlybyDelay, WriteToCache | PrintValue, &GetSetVal);
      if (Lpddr && (EnDqsNRcvEnSave == 0xFF)) {
        // Read from Byte 0 as it will always exist on Existing Channel
        MrcGetSetChStrb (MrcData, Controller, Channel, 0, GsmIocEnDqsNRcvEn, ReadFromCache | PrintValue, &EnDqsNRcvEnSave);
      }
      if (Lpddr && (EnDqsNRcvEnGateSave == 0xFF)) {
        // Read from Byte 0 as it will always exist on Existing Channel
        MrcGetSetChStrb (MrcData, Controller, Channel, 0, GsmIocEnDqsNRcvEnGate, ReadFromCache | PrintValue, &EnDqsNRcvEnGateSave);
      }
    }
  }
  if (Lpddr) {
    // Must Disable before enabling Read Leveling Mode
    MrcGetSetChStrb (MrcData, MAX_CONTROLLER, MAX_CHANNEL, MAX_SDRAM_IN_DIMM, GsmIocEnDqsNRcvEn,     WriteToCache | PrintValue, &GetSetDis);
    MrcGetSetChStrb (MrcData, MAX_CONTROLLER, MAX_CHANNEL, MAX_SDRAM_IN_DIMM, GsmIocEnDqsNRcvEnGate, WriteToCache | PrintValue, &GetSetDis);
  }
  MrcFlushRegisterCachedData (MrcData);

  RxIoTclInit = Tcl + MrcFemtoTimeToTCK (MrcData, 3500000 / (Gear2 + 1));  // @todo Might need to adjust post-si

  MrcGetSetChStrb (MrcData, MAX_CONTROLLER, MAX_CHANNEL, MAX_SDRAM_IN_DIMM, GsmIocReadLevelMode, WriteNoCache, &GetSetEn);

  // Enable Rank Mux Override
  MrcGetSetChStrb (MrcData, MAX_CONTROLLER, MAX_CHANNEL, MAX_SDRAM_IN_DIMM, GsmIocRankOverrideEn, WriteNoCache, &GetSetEn);

  for (Rank = 0; Rank < MAX_RANK_IN_CHANNEL; Rank++) {
    RankMask = (1 << Rank);
    if ((RankMask & ValidRankMask) == 0) {
      // Skip if this rank is not present on any of the channels
      continue;
    }
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\nRank: %u\n", Rank);
    // Update Rank Mux Override for the rank under test
    GetSetVal = Rank;
    MrcGetSetChStrb (MrcData, MAX_CONTROLLER, MAX_CHANNEL, MAX_SDRAM_IN_DIMM, GsmIocRankOverrideVal, WriteNoCache, &GetSetVal);

    // Init Channel Delay Component
    MrcGetSetMcCh (MrcData, MAX_CONTROLLER, MAX_CHANNEL, RxIoTclDelay, WriteToCache, &RxIoTclInit);
    MrcFlushRegisterCachedData (MrcData);

    MrcCall->MrcSetMem ((UINT8 *) DccDone,              sizeof (DccDone), 0);
    MrcCall->MrcSetMem ((UINT8 *) DccCodeSaturate,      sizeof (DccCodeSaturate), 0);
    MrcCall->MrcSetMem ((UINT8 *) DccStatusPrev,        sizeof (DccStatusPrev), 0);
    MrcCall->MrcSetMem ((UINT8 *) DccStatus,            sizeof (DccStatus), 0);
    MrcCall->MrcSetMem ((UINT8 *) RxDqsVocCode,         sizeof (RxDqsVocCode),        RXDQS_VOC_CODE_START);
    MrcCall->MrcSetMem ((UINT8 *) PrevRxDqsVocCode,     sizeof (PrevRxDqsVocCode),    RXDQS_VOC_CODE_START);
    MrcCall->MrcSetMem ((UINT8 *) CurrentRxDqsVocCode,  sizeof (CurrentRxDqsVocCode), RXDQS_VOC_CODE_START);
    MrcCall->MrcSetMem ((UINT8 *) PrevDutyCycle,        sizeof (PrevDutyCycle), 0);

    McChBitMask = 0;
    for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
      for (Channel = 0; Channel < MaxChannels; Channel++) {
        McChBitMask |= SelectReutRanks (MrcData, Controller, Channel, RankMask, FALSE, 0);
        if (!(MrcRankExist (MrcData, Controller, Channel, Rank))) {
          continue;
        }
        ChannelOut = &Outputs->Controller[Controller].Channel[Channel];

        if (Ddr4) {
          // Enable MPR mode - needed for DDR4 Read Preamble Training mode
          Ddr4Mr3.Data = ChannelOut->Dimm[Rank / 2].Rank[Rank % 2].MR[mrMR3];
          Ddr4Mr3.Bits.MprOperation = 1;
          MrcWriteMRS (MrcData, Controller, Channel, RankMask, mrMR3, Ddr4Mr3.Data);
          // Enable DDR4 Read Preamble Training mode
          Ddr4Mr4.Data = ChannelOut->Dimm[Rank / 2].Rank[Rank % 2].MR[mrMR4];
          Ddr4Mr4.Bits.ReadPreambleTrainingMode = 1;
          MrcWriteMRS (MrcData, Controller, Channel, RankMask, mrMR4, Ddr4Mr4.Data);
        }
        if (Ddr5) {
          // Enable DDR5 Read Preamble Training mode
          Ddr5Mr2.Data8 = (UINT8) ChannelOut->Dimm[Rank / 2].Rank[Rank % 2].MR[mrMR2];
          Ddr5Mr2.Bits.ReadPreambleTraining = 1;
          MrcIssueMrw (MrcData, Controller, Channel, Rank, mrMR2, Ddr5Mr2.Data8, MRC_PRINTS_OFF);
        }
        if (Ddr4 || Ddr5) {
          // Enable MPR mode in MC
          GetSetVal = 1;
          MrcGetSetMcCh (MrcData, Controller, Channel, GsmMccMprTrainDdrOn, WriteCached, &GetSetVal);
        }
      }
    }

    // Program the rxdqsvoc start value
    for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
      for (Channel = 0; Channel < Outputs->MaxChannels; Channel++) {
        if (!MrcChannelExist (MrcData, Controller, Channel)) {
          continue;
        }
        for (Strobe = 0; Strobe < Outputs->SdramCount; Strobe++) {
          SetRxDqsVoc (MrcData, Controller, Channel, Rank, Strobe, RxDqsVocCode[Controller][Channel][Strobe], FALSE);
        }
      }
    }

    MrcCall->MrcSetMem ((UINT8 *) DccResultPassStrobe,      sizeof (DccResultPassStrobe), 0);
    MrcCall->MrcSetMem ((UINT8 *) DccResultPassCh,          sizeof (DccResultPassCh), 0);
    MrcCall->MrcSetMem ((UINT8 *) DccResultPassController,  sizeof (DccResultPassController), 0);

    RcvEnStartDly = 256;
    MrcGetSetStrobe (MrcData, MAX_CONTROLLER, MAX_CHANNEL, Rank, MAX_SDRAM_IN_DIMM, RecEnDelay, WriteCached, &RcvEnStartDly);

    // Search for 50% duty cycle by modifying the DqsRxVoc code and looking for dither
    for (DccCorrectionCount = 0; DccCorrectionCount < DCC_CORRECTION_LIMIT; DccCorrectionCount++) {
      MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "[%u] \n", DccCorrectionCount);
      // sample duty cycle over 2 UI (1 tDQS_PER)
      MrcCall->MrcSetMem ((UINT8 *) DqsFdbkOnesCount, sizeof (DqsFdbkOnesCount), 0);
      for (DqsPiSweep = 0; DqsPiSweep < DCC_PI_SWEEP_RANGE; DqsPiSweep++) {
        // program rcven so that rcvenpre asserts within the burst (beyond the 1st pulse to reduce ISI impact)
        GetSetVal = RcvEnStartDly + DqsPiSweep;
        MrcGetSetStrobe (MrcData, MAX_CONTROLLER, MAX_CHANNEL, Rank, MAX_SDRAM_IN_DIMM, RecEnDelay, WriteCached, &GetSetVal);
        // Run Test, Reset FIFOs will be done before running test
        RunIOTest (MrcData, McChBitMask, BasicVA, 1, OemReceiveEnable);
        // Issue TrainReset to force code propagation
        for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
          for (Channel = 0; Channel < MaxChannels; Channel++) {
            if (!(MrcRankExist (MrcData, Controller, Channel, Rank))) {
              continue;
            }
            for (Strobe = 0; Strobe < Outputs->SdramCount; Strobe++) {
              if (!MrcByteExist (MrcData, Controller, Channel, Strobe)) {
                continue;
              }
              MrcGetSetChStrb (MrcData, Controller, Channel, Strobe, GsmIocDataTrainFeedback, ReadUncached, &DataTrainFeedbackField);
              Pass = (DataTrainFeedbackField >= (UINT8) (MRC_BIT0 << (NumSamples - 1)));
              if (Pass) {
                DqsFdbkOnesCount[Controller][Channel][Strobe] += 1;
              }
            }
          }
        }
      } // DqsPiSweep loop

      DccAllControllerPass = TRUE;
      for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
        AllChPass = TRUE;
        if (DccResultPassController[Controller] == 1) {
          continue;
        }
        for (Channel = 0; Channel < Outputs->MaxChannels; Channel++) {
          if (!(MrcRankExist (MrcData, Controller, Channel, Rank))) {
            continue;
          }
          if (DccResultPassCh[Controller][Channel] == 1) {
            continue;
          }
          AllStrobePass = TRUE;
          for (Strobe = 0; Strobe < Outputs->SdramCount; Strobe++) {
            if (DccResultPassStrobe[Controller][Channel][Strobe] == 1) {
              continue;
            }
            OnesCount = DqsFdbkOnesCount[Controller][Channel][Strobe];
            DutyCycle = (OnesCount * 10000) / (DCC_PI_SWEEP_RANGE);
            DataInt = (DutyCycle) / 100;

            MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "MC%u.C%u.B%u: OnesCount %3d  Duty cycle %3u.%02u ",
                           Controller, Channel, Strobe, OnesCount, DataInt, (DutyCycle - (100 * DataInt)));

            // compare ones count with number half of the total number of samples to determine whether duty cycle is high or low
            if (OnesCount == DCC_PI_SWEEP_RANGE / 2) {
              // duty cycle is 50%
              DccStatus[Controller][Channel][Strobe] = DccStatusAlign;
              MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, " Align ");
            } else if (OnesCount < DCC_PI_SWEEP_RANGE / 2) {
              // duty cycle is less than 50%
              DccStatus[Controller][Channel][Strobe] = DccLow;
              MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, " Low   ");
            } else if (OnesCount > DCC_PI_SWEEP_RANGE / 2) {
              // duty cycle is greater than 50%
              DccStatus[Controller][Channel][Strobe] = DccHigh;
              MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, " High  ");
            }

            if (DccStatus[Controller][Channel][Strobe] == DccStatusAlign) {
              DccDone[Controller][Channel][Strobe] = TRUE;
              DccResultPassStrobe[Controller][Channel][Strobe] = 1;
              MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, " pass with DccStatusAlign\n");
              continue;
            } else if ((DccStatus[Controller][Channel][Strobe] == DccLow) && (DccStatusPrev[Controller][Channel][Strobe] == DccHigh)) {
              // dither from high duty cycle to low duty cycle
              DccDone[Controller][Channel][Strobe] = TRUE;
              DccResultPassStrobe[Controller][Channel][Strobe] = 1;
              MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, " pass with dither high->low\n");
              if (ABS(PrevDutyCycle[Controller][Channel][Strobe] - 5000) < ABS (5000 - DutyCycle)) {
                DataIntTemp = (PrevDutyCycle[Controller][Channel][Strobe]) / 100;
                MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, " last duty cycle %u.%02u is better than current one, choose last RxVOC code ",
                               DataIntTemp, (PrevDutyCycle[Controller][Channel][Strobe] - (100 * DataIntTemp)));
                SetRxDqsVoc (MrcData, Controller, Channel, Rank, Strobe, PrevRxDqsVocCode[Controller][Channel][Strobe], TRUE);
              }
              continue;
            }

            CurrentVocCode = RxDqsVocCode[Controller][Channel][Strobe];
            if ((DccStatus[Controller][Channel][Strobe] == DccLow) && (CurrentVocCode > RXDQS_VOC_RANGE_MIN)) {
              RxDqsVocCode[Controller][Channel][Strobe] -= 1;
            } else if ((DccStatus[Controller][Channel][Strobe] == DccLow) && (CurrentVocCode == RXDQS_VOC_RANGE_MIN)) {
              DccCodeSaturate[Controller][Channel][Strobe] = TRUE;
              DccResultPassStrobe[Controller][Channel][Strobe] = 1;
              MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "MC%u.C%u.B%u: pass with Saturate\n", Controller, Channel, Strobe);
              continue;
            } else if ((DccStatus[Controller][Channel][Strobe] == DccHigh) && (CurrentVocCode < RXDQS_VOC_RANGE_MAX)) {
              RxDqsVocCode[Controller][Channel][Strobe] += 1;
            } else if ((DccStatus[Controller][Channel][Strobe] == DccHigh) && (CurrentVocCode == RXDQS_VOC_RANGE_MAX)) {
              DccCodeSaturate[Controller][Channel][Strobe] = TRUE;
              DccResultPassStrobe[Controller][Channel][Strobe] = 1;
              MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "MC%u.C%u.B%u: pass with Saturate\n", Controller, Channel, Strobe);
              continue;
            }
            DccStatusPrev[Controller][Channel][Strobe] = DccStatus[Controller][Channel][Strobe];
            SetRxDqsVoc (MrcData, Controller, Channel, Rank, Strobe, RxDqsVocCode[Controller][Channel][Strobe], TRUE);
            if (DccCorrectionCount == 0) {
              PrevRxDqsVocCode[Controller][Channel][Strobe]    = RxDqsVocCode[Controller][Channel][Strobe];
              CurrentRxDqsVocCode[Controller][Channel][Strobe] = RxDqsVocCode[Controller][Channel][Strobe];
            } else {
              PrevRxDqsVocCode[Controller][Channel][Strobe]    = CurrentRxDqsVocCode[Controller][Channel][Strobe];
              CurrentRxDqsVocCode[Controller][Channel][Strobe] = RxDqsVocCode[Controller][Channel][Strobe];
            }
            PrevDutyCycle[Controller][Channel][Strobe] = DutyCycle;
            if (DccResultPassStrobe[Controller][Channel][Strobe] == 0) {
              AllStrobePass = FALSE;
            }
          } //strobe
          if (AllStrobePass == TRUE) {
            DccResultPassCh[Controller][Channel] = 1;
          }
          if (DccResultPassCh[Controller][Channel] == 0) {
            AllChPass = FALSE;
          }
        } //channel
        if (AllChPass == TRUE) {
          DccResultPassController[Controller] = 1;
        }
        if (DccResultPassController[Controller] == 0) {
          DccAllControllerPass = FALSE;
        }
      } //controller

      if (DccAllControllerPass == TRUE) {
        break;
      }
    }
    // Clean up dram
    if (Ddr4 || Ddr5) {
      MrcWait (MrcData, 150 * MRC_TIMER_1NS);  // Allow MC to close open pages using PRE
      for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
        for (Channel = 0; Channel < MaxChannels; Channel++) {
          if (!(MrcRankExist (MrcData, Controller, Channel, Rank))) {
            continue;
          }
          ChannelOut = &Outputs->Controller[Controller].Channel[Channel];
          // Clear MPR mode in MC before sending MRS / MRW commands
          GetSetVal = 0;
          MrcGetSetMcCh (MrcData, Controller, Channel, GsmMccMprTrainDdrOn, WriteCached, &GetSetVal);
          if (Ddr4) {
            // Disable DDR4 Read Preamble Training mode
            Ddr4Mr4.Data = ChannelOut->Dimm[Rank / 2].Rank[Rank % 2].MR[mrMR4];
            MrcWriteMRS (MrcData, Controller, Channel, RankMask, mrMR4, Ddr4Mr4.Data);
            // Disable MPR mode
            Ddr4Mr3.Data = ChannelOut->Dimm[Rank / 2].Rank[Rank % 2].MR[mrMR3];
            MrcWriteMRS (MrcData, Controller, Channel, RankMask, mrMR3, Ddr4Mr3.Data);
          }
          if (Ddr5) {
            // Disable DDR5 Read Preamble Training mode
            Ddr5Mr2.Data8 = (UINT8) ChannelOut->Dimm[Rank / 2].Rank[Rank % 2].MR[mrMR2];
            MrcIssueMrw (MrcData, Controller, Channel, Rank, mrMR2, Ddr5Mr2.Data8, MRC_PRINTS_OFF);
          }
        }
      }
    }
  } //rank loop
  // Clean up after Test
  if (Lpddr) {
    MrcGetSetChStrb (MrcData, MAX_CONTROLLER, MAX_CHANNEL, MAX_SDRAM_IN_DIMM, GsmIocEnDqsNRcvEn,     WriteToCache | PrintValue, &EnDqsNRcvEnSave);
    MrcGetSetChStrb (MrcData, MAX_CONTROLLER, MAX_CHANNEL, MAX_SDRAM_IN_DIMM, GsmIocEnDqsNRcvEnGate, WriteToCache | PrintValue, &EnDqsNRcvEnGateSave);
  }

  // Disable Read Leveling Mode and restore PHY settings
  MrcGetSetChStrb (MrcData, MAX_CONTROLLER, MAX_CHANNEL, MAX_SDRAM_IN_DIMM, GsmIocReadLevelMode, WriteNoCache, &GetSetDis);
  MrcGetSetChStrb (MrcData, MAX_CONTROLLER, MAX_CHANNEL, MAX_SDRAM_IN_DIMM, GsmIocForceRxAmpOn,  WriteToCache, &ForceRxAmpOnSave);
  MrcGetSetChStrb (MrcData, MAX_CONTROLLER, MAX_CHANNEL, MAX_SDRAM_IN_DIMM, GsmIocForceOdtOn,    WriteToCache, &ForceOdtOnSave);
  MrcFlushRegisterCachedData (MrcData);
  // Disable Rank Mux Override
  MrcGetSetChStrb (MrcData, MAX_CONTROLLER, MAX_CHANNEL, MAX_SDRAM_IN_DIMM, GsmIocRankOverrideEn, WriteNoCache, &GetSetDis);

  Status = IoReset (MrcData);
  return Status;
}
#endif //if 0

/**
  This routine sets the Rx DQS VOC code

  @param[in]  MrcData     - Pointer to MRC global data.
  @param[in]  Controller  - Controller number.
  @param[in]  Channel     - Channel number.
  @param[in]  Rank        - Rank number.
  @param[in]  Strobe      - Strobe number.
  @param[in]  VocCode     - VOC code to program.
  @param[in]  Print       - Print the debug message or not.

  @retval NA.
**/
VOID
SetRxDqsVoc (
  IN OUT MrcParameters *const MrcData,
  IN UINT32                   Controller,
  IN UINT32                   Channel,
  IN UINT32                   Rank,
  IN UINT32                   Strobe,
  IN UINT8                    VocCode,
  IN BOOLEAN                  Print
  )
{
  MrcOutput                       *Outputs;
  MrcDebug                        *Debug;
  UINT32                          ControllerLocal;
  UINT32                          Offset;
  UINT32                          TransChannel;
  UINT32                          TransStrobe;
  DATA0CH0_CR_DQSTXRXCTL0_STRUCT  DataDqsTxRxCtl0;

  Outputs         = &MrcData->Outputs;
  Debug           = &Outputs->Debug;
  ControllerLocal = Controller;
  if (Print) {
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "R%u MC%u C%u S%u Voc: %u\n", Rank, Controller, Channel, Strobe, VocCode);
  }
  //
  // Matched Rx Amplifier Voltage Offset Code - controls the Rx Offset amount
  // 0x00 - most negative vref (dqsb) offset, largest diff duty cycle. 0x10 - no vref (dqsb) offset. 0x3f - most positive vref (dqsb) offset, smallest diff duty cycle.
  //
  TransChannel = Channel;
  TransStrobe = Strobe;
  MrcTranslateSystemToIp(MrcData, &ControllerLocal, &TransChannel, &Rank, &TransStrobe, GsmIocTxOn);

  Offset = DATA0CH0_CR_DQSTXRXCTL0_REG +
          (DATA0CH1_CR_DQSTXRXCTL0_REG - DATA0CH0_CR_DQSTXRXCTL0_REG) * TransChannel +
          (DATA1CH0_CR_DQSTXRXCTL0_REG - DATA0CH0_CR_DQSTXRXCTL0_REG) * TransStrobe;
  DataDqsTxRxCtl0.Data = MrcReadCR (MrcData, Offset);
  if (Rank == 0) {
    DataDqsTxRxCtl0.Bits.dqsrx_cmn_rxmampoffset_rk0 = VocCode;
  } else if (Rank == 1) {
    DataDqsTxRxCtl0.Bits.dqsrx_cmn_rxmampoffset_rk1 = VocCode;
  } else if (Rank == 2) {
    DataDqsTxRxCtl0.Bits.dqsrx_cmn_rxmampoffset_rk2 = VocCode;
  } else if (Rank == 3) {
    DataDqsTxRxCtl0.Bits.dqsrx_cmn_rxmampoffset_rk3 = VocCode;
  }
  MrcWriteCR (MrcData, Offset, DataDqsTxRxCtl0.Data);

  return;
}

/**
  This routine gets the Rx DQS VOC code

  @param[in]  MrcData          - Pointer to MRC global data.
  @param[in]  Controller       - Controller number.
  @param[in]  Channel          - Channel number.
  @param[in]  Rank             - Rank number.
  @param[in]  Strobe           - Strobe number.
  @param[in]  Print            - Print debug message or not.
  @param[in]  RxPreSdlDccLoop  - The loop number to run Rx PRE SDL DCC for averaging.
  @param[in]  VocValue         - The RX DQS VOC value.

  @retval NA.
**/
VOID
GetRxDqsVoc (
  IN OUT MrcParameters *const MrcData,
  IN UINT32                   Controller,
  IN UINT32                   Channel,
  IN UINT32                   Rank,
  IN UINT32                   Strobe,
  IN BOOLEAN                  Print,
  IN UINT8                    RxPreSdlDccLoop,
  IN UINT32                   VocValue[MAX_CONTROLLER][MAX_CHANNEL][MAX_RANK_IN_CHANNEL][MAX_SDRAM_IN_DIMM][RX_PRE_SDL_DCC_LOOP]
  )
{
  MrcOutput                       *Outputs;
  MrcDebug                        *Debug;
  UINT32                          ControllerLocal;
  UINT32                          Offset;
  UINT32                          TransChannel;
  UINT32                          TransStrobe;
  DATA0CH0_CR_DQSTXRXCTL0_STRUCT  DataDqsTxRxCtl0;

  Outputs         = &MrcData->Outputs;
  Debug           = &Outputs->Debug;
  ControllerLocal = Controller;
  //
  // Matched Rx Amplifier Voltage Offset Code - controls the Rx Offset amount
  // 0x00 - most negative vref (dqsb) offset, largest diff duty cycle. 0x10 - no vref (dqsb) offset. 0x3f - most positive vref (dqsb) offset, smallest diff duty cycle.
  //
  TransChannel = Channel;
  TransStrobe = Strobe;
  MrcTranslateSystemToIp(MrcData, &ControllerLocal, &TransChannel, &Rank, &TransStrobe, GsmIocTxOn);

  Offset = DATA0CH0_CR_DQSTXRXCTL0_REG +
           (DATA0CH1_CR_DQSTXRXCTL0_REG - DATA0CH0_CR_DQSTXRXCTL0_REG) * TransChannel +
           (DATA1CH0_CR_DQSTXRXCTL0_REG - DATA0CH0_CR_DQSTXRXCTL0_REG) * TransStrobe;
  DataDqsTxRxCtl0.Data = MrcReadCR (MrcData, Offset);
  if (Rank == 0) {
    VocValue[Controller][Channel][Rank][Strobe][RxPreSdlDccLoop] = DataDqsTxRxCtl0.Bits.dqsrx_cmn_rxmampoffset_rk0;
  } else if (Rank == 1) {
    VocValue[Controller][Channel][Rank][Strobe][RxPreSdlDccLoop] = DataDqsTxRxCtl0.Bits.dqsrx_cmn_rxmampoffset_rk1;
  } else if (Rank == 2) {
    VocValue[Controller][Channel][Rank][Strobe][RxPreSdlDccLoop] = DataDqsTxRxCtl0.Bits.dqsrx_cmn_rxmampoffset_rk2;
  } else if (Rank == 3) {
    VocValue[Controller][Channel][Rank][Strobe][RxPreSdlDccLoop] = DataDqsTxRxCtl0.Bits.dqsrx_cmn_rxmampoffset_rk3;
  }
  if (Print) {
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Loop %d R%u MC%u C%u S%u Voc: %u\n", RxPreSdlDccLoop, Rank, Controller, Channel, Strobe, VocValue[Controller][Channel][Rank][Strobe][RxPreSdlDccLoop]);
  }
  return;
}
/**
  This routine runs Rx POST SDL DCC training

  @param[in]  MrcData - Pointer to MRC global data.

  @retval     MrcStatus - if succeeded, return mrcSuccess
**/
MrcStatus
MrcRxPostSdlDccTraining (
  IN OUT MrcParameters *const MrcData
  )
{
  return  MrcRxSdlDccTraining (MrcData, RxPostSdlDcc);
}

/**
  This routine runs Rx SDL DCC training

  @param[in]  MrcData    - Pointer to MRC global data.
  @param[in]  SdlDccType - SDL DCC training type: Pre SDL DCC or POST SDL DCC

  @retval     MrcStatus - if succeeded, return mrcSuccess
**/
MrcStatus
MrcRxSdlDccTraining (
  IN OUT MrcParameters *const MrcData,
  IN     SDL_DCC        SdlDccType
  )
{
  const MRC_FUNCTION *MrcCall;
  MrcStatus          Status;
  MrcOutput          *Outputs;
  MrcDebug           *Debug;
  UINT32             Controller;
  UINT32             Channel;
  UINT32             Strobe;
  UINT32             DqLaneDutyCycle[MAX_BITS][MAX_CONTROLLER][MAX_CHANNEL][MAX_SDRAM_IN_DIMM];
  UINT32             DqDcdSampleCount[MAX_CONTROLLER][MAX_CHANNEL][MAX_SDRAM_IN_DIMM];
  UINT32             DutyCycleMax[MAX_CONTROLLER][MAX_CHANNEL][MAX_SDRAM_IN_DIMM];
  UINT32             DutyCycleMin[MAX_CONTROLLER][MAX_CHANNEL][MAX_SDRAM_IN_DIMM];
  DCC_STATUS         DccStatus[MAX_CONTROLLER][MAX_CHANNEL][MAX_SDRAM_IN_DIMM];
  DCC_STATUS         DccPrevStatus[MAX_CONTROLLER][MAX_CHANNEL][MAX_SDRAM_IN_DIMM];
  BOOLEAN            DccDone[MAX_CONTROLLER][MAX_CHANNEL][MAX_SDRAM_IN_DIMM];
  UINT8              DccCode[MAX_CONTROLLER][MAX_CHANNEL][MAX_SDRAM_IN_DIMM];
  UINT8              DccCodeOld[MAX_CONTROLLER][MAX_CHANNEL][MAX_SDRAM_IN_DIMM];
  BOOLEAN            DccResultPassStrobe[MAX_CONTROLLER][MAX_CHANNEL][MAX_SDRAM_IN_DIMM];
  BOOLEAN            DccResultPassCh[MAX_CONTROLLER][MAX_CHANNEL];
  BOOLEAN            DccResultPassController[MAX_CONTROLLER];
  UINT32             TotalSampleCounts;
  UINT8              dqlane;
  UINT8              DccCorrectionCount;
  UINT32             DataInt;
  UINT32             DataInt2;
  BOOLEAN            Saturated;
  BOOLEAN            Lpddr5;
  BOOLEAN            Ddr5;
  BOOLEAN            UseRxDqsGen;
  BOOLEAN            AllChPass;
  BOOLEAN            AllStrobePass;
  BOOLEAN            AllControllerPass;
  UINT32             SavedParams[MAX_DCC_SAVED_PARAMETERS];
  UINT32             SavedParamsRxDqs[MAX_RX_POST_SDL_DCC_SAVED_PARAMETERS];
  INT64              SaveRxDqsP[MAX_RANK_IN_CHANNEL][MAX_CONTROLLER][MAX_CHANNEL][MAX_SDRAM_IN_DIMM]; // For Rx PBD P delay
  INT64              SaveRxDqsN[MAX_RANK_IN_CHANNEL][MAX_CONTROLLER][MAX_CHANNEL][MAX_SDRAM_IN_DIMM]; // For Rx PBD N delay
  UINT8              DccLoopCountLimit;
  UINT8              RxDccCodeIinitValue;
  UINT32             DccTarget[MAX_CONTROLLER][MAX_CHANNEL][MAX_SDRAM_IN_DIMM];
  UINT8              ValidRankMask;
  UINT32             Rank;
  UINT8              RankMask;
  UINT8              McChBitMask;
  MC_MPR_CONFIG_SAVE SaveData;

  static const DCC_CLK_TYPES DqLaneDcdMap[8] = { DccDq0, DccDq1, DccDq2, DccDq3, DccDq4, DccDq5, DccDq6, DccDq7 };

  Status  = mrcSuccess;
  MrcCall = MrcData->Inputs.Call.Func;
  Outputs = &MrcData->Outputs;
  Debug   = &Outputs->Debug;
  Ddr5    = (Outputs->DdrType == MRC_DDR_TYPE_DDR5);
  Lpddr5  = (Outputs->DdrType == MRC_DDR_TYPE_LPDDR5);
  Saturated = FALSE;
  TotalSampleCounts = TOTAL_SAMPLE_COUNTS_VALUE_POST_SDL_DCC;
  ValidRankMask     = Outputs->ValidRankMask;

  if ((!Ddr5 && !Lpddr5) || (Outputs->Frequency <= 3200)) {
    // only run it for DDR5 or LP5 and freq > 3200
    return Status;
  }

  UseRxDqsGen = FALSE;

  if (SdlDccType == RxPostSdlDcc) {
    RxDccCodeIinitValue = RXSDL_DCC_CODE_START;
    DccLoopCountLimit   = DCC_CORRECTION_LIMIT_VCDL_DCC;
  } else if (SdlDccType == RxPreSdlDcc) {
    RxDccCodeIinitValue = RXDQS_VOC_CODE_START;
    DccLoopCountLimit   = DCC_CORRECTION_LIMIT_DQS_VOC;
  } else {
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_WARNING, "SdlDccType %d is invalid\n", SdlDccType);
    return mrcWrongInputParameter;
  }

  MrcCall->MrcSetMem ((UINT8 *) SavedParams, sizeof (SavedParams), 0);
  MrcCall->MrcSetMem ((UINT8 *) SavedParamsRxDqs, sizeof (SavedParamsRxDqs), 0);
  MrcCall->MrcSetMem ((UINT8 *) SaveRxDqsP, sizeof (SaveRxDqsP), 0);
  MrcCall->MrcSetMem ((UINT8 *) SaveRxDqsN, sizeof (SaveRxDqsN), 0);
  MrcCall->MrcSetMem ((UINT8 *) DqLaneDutyCycle, sizeof (DqLaneDutyCycle), 0);
  MrcCall->MrcSetMem ((UINT8 *) DqDcdSampleCount, sizeof (DqDcdSampleCount), 0);
  // enter RxPostSdlDcc calibration mode
  // setup DCC FSM for accumulator mode
  SetupDccAccData (MrcData, TRUE, TotalSampleCounts, DCD_GROUP_SRZ, SavedParams);

  if (!UseRxDqsGen) {
    SetRxDqsDcd (MrcData, TRUE, TRUE, SavedParamsRxDqs, SaveRxDqsP, SaveRxDqsN);
    // run accumulator
    RunDccAccData (MrcData, TotalSampleCounts, DqLaneDutyCycle[0], DqDcdSampleCount);
  } else {
    // RxDqsGen added in C0 and Q0
    SetRxDqsDcd (MrcData, FALSE, TRUE, SavedParamsRxDqs, SaveRxDqsP, SaveRxDqsN);
    // run accumulator
    RunDccAccData (MrcData, TotalSampleCounts, DqLaneDutyCycle[0], DqDcdSampleCount);
    RxDqsGenEn (MrcData, TRUE, SavedParamsRxDqs);
  }

  for (Rank = 0; Rank < MAX_RANK_IN_CHANNEL; Rank++) {
    RankMask = (1 << Rank);
    if ((RankMask & ValidRankMask) == 0) {
      // Skip if this rank is not present on any of the channels
      continue;
    }
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\nRank: %u\n", Rank);
    MrcCall->MrcSetMem ((UINT8 *) DqLaneDutyCycle, sizeof (DqLaneDutyCycle), 0);
    MrcCall->MrcSetMem ((UINT8 *) DqDcdSampleCount, sizeof (DqDcdSampleCount), 0);
    MrcCall->MrcSetMem ((UINT8 *) DccStatus, sizeof (DccStatus), 0);
    MrcCall->MrcSetMem ((UINT8 *) DccDone, sizeof (DccDone), 0);
    MrcCall->MrcSetMem ((UINT8 *) DccResultPassStrobe, sizeof (DccResultPassStrobe), 0);
    MrcCall->MrcSetMem ((UINT8 *) DccResultPassCh, sizeof (DccResultPassCh), 0);
    MrcCall->MrcSetMem ((UINT8 *) DccResultPassController, sizeof (DccResultPassController), 0);
    MrcCall->MrcSetMem ((UINT8 *) DccCode, sizeof (DccCode), RxDccCodeIinitValue);
    MrcCall->MrcSetMem ((UINT8 *) DccCodeOld, sizeof (DccCodeOld), RxDccCodeIinitValue);
    MrcCall->MrcSetMem ((UINT8 *) DccPrevStatus, sizeof (DccPrevStatus), 0);
    MrcCall->MrcSetMem ((UINT8 *) DccTarget, sizeof (DccTarget), 0);

    // Enable Continuous Burst mode in DDR5
    StartJedecContBurstModePerRank (MrcData, TRUE, Rank);

    // Configure CPGC and start endless RD test
    CpgcSetupPostSdlDcc (MrcData, TRUE, Rank, &McChBitMask, &SaveData);

    for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
      for (Channel = 0; Channel < Outputs->MaxChannels; Channel++) {
        if (!(MrcRankExist (MrcData, Controller, Channel, Rank))) {
          continue;
        }
        for (Strobe = 0; Strobe < Outputs->SdramCount; Strobe++) {
          // set DCC code to default
          SetRxVcdlDccCode (MrcData, Controller, Channel, Strobe, RXSDL_DCC_CODE_START, MRC_PRINTS_OFF);
          if (SdlDccType == RxPostSdlDcc) {
            SetRxPi (MrcData, PICODE_VCDL_START, MRC_PRINTS_OFF);

            // measure duty cycle and determine max and min duty cycle across all lanes
            for (dqlane = 0;  dqlane < MAX_BITS; dqlane++) {
#ifdef DCC_DEBUG_PRINT
              MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "DQ Lane %d:\n", dqlane);
#endif // DCC_DEBUG_PRINT

              // select DQ DCD lane
              SelectDccAccDcdData(MrcData, DqLaneDcdMap[dqlane]);

              // run accumulator
              RunDccAccData (MrcData, TotalSampleCounts, DqLaneDutyCycle[dqlane], DqDcdSampleCount);

              // Correct for clock inversion in the feedback clock
              if (SavedParamsRxDqs[RX_MATCHED_PATH_EN_INDEX] == 0) {
                DqLaneDutyCycle[dqlane][Controller][Channel][Strobe] = RX_DCC_DUTY_CYCLE_SUM - DqLaneDutyCycle[dqlane][Controller][Channel][Strobe];
              }
              DccTarget[Controller][Channel][Strobe] = DccTarget[Controller][Channel][Strobe] + DqLaneDutyCycle[dqlane][Controller][Channel][Strobe];
            }
            DccTarget[Controller][Channel][Strobe] = DccTarget[Controller][Channel][Strobe] / 4;

            MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Mc%d.C%d.S%d DccTarget: %6d\n", Controller, Channel, Strobe, DccTarget[Controller][Channel][Strobe]);

            SetRxPi (MrcData, PICODE_VCDL_END, MRC_PRINTS_OFF);
            DccPrevStatus[Controller][Channel][Strobe] = DccLow;
          } else {
            SetRxDqsVoc (MrcData, Controller, Channel, Rank, Strobe, RXDQS_VOC_CODE_START, MRC_PRINTS_OFF);
            // Initialize DccPrevStatus to Low to avoid accidental Dither detection
            DccPrevStatus[Controller][Channel][Strobe] = DccLow;
            DccTarget[Controller][Channel][Strobe] = RX_DCC_DUTY_CYCLE_SUM;
          }
        }
      }
    }

    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "SetRxVcdlDccCode with DCC code %d\n", RXSDL_DCC_CODE_START);
    MrcCall->MrcSetMem ((UINT8 *) DqLaneDutyCycle, sizeof (DqLaneDutyCycle), 0);
    for (DccCorrectionCount = 0;  DccCorrectionCount < DccLoopCountLimit; DccCorrectionCount++) {
      MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "DCC Correction Loop %d\n", DccCorrectionCount);

      // reset duty cycle min and max for remeasurement
      MrcCall->MrcSetMem ((UINT8 *) DutyCycleMax, sizeof (DutyCycleMax), 0);
      MrcCall->MrcSetMemDword ((UINT32 *) DutyCycleMin, sizeof (DutyCycleMin) / sizeof (UINT32), 10000);

      // measure duty cycle and determine max and min duty cycle across all lanes
      for (dqlane = 0;  dqlane < MAX_BITS; dqlane++) {
#ifdef DCC_DEBUG_PRINT
        MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "DQ Lane %d:\n", dqlane);
#endif // DCC_DEBUG_PRINT

        // select DQ DCD lane
        SelectDccAccDcdData(MrcData, DqLaneDcdMap[dqlane]);

        // run accumulator
        RunDccAccData (MrcData, TotalSampleCounts, DqLaneDutyCycle[dqlane], DqDcdSampleCount);
      }

      MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "DQ:          0       1      2      3      4     5      6     7    DutyCycleMax  DutyCycleMin\n");
      for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
        for (Channel = 0; Channel < Outputs->MaxChannels; Channel++) {
          if (!(MrcRankExist (MrcData, Controller, Channel, Rank))) {
            continue;
          }
          for (Strobe = 0; Strobe < Outputs->SdramCount; Strobe++) {
            if (DccResultPassStrobe[Controller][Channel][Strobe] == 1) {
              continue;
            }
            MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "MC%u.C%u.S%u ", Controller, Channel, Strobe);
            for (dqlane = 0;  dqlane < MAX_BITS; dqlane++) {

              // Correct for clock inversion in the feedback clock
              if (SavedParamsRxDqs[RX_MATCHED_PATH_EN_INDEX] == 0) {
                DqLaneDutyCycle[dqlane][Controller][Channel][Strobe] = RX_DCC_DUTY_CYCLE_SUM - DqLaneDutyCycle[dqlane][Controller][Channel][Strobe];
              }

              // check for min and max
              if (DqLaneDutyCycle[dqlane][Controller][Channel][Strobe] > DutyCycleMax[Controller][Channel][Strobe]) {
                DutyCycleMax[Controller][Channel][Strobe] = DqLaneDutyCycle[dqlane][Controller][Channel][Strobe];
              }
              if (DqLaneDutyCycle[dqlane][Controller][Channel][Strobe] < DutyCycleMin[Controller][Channel][Strobe]) {
                DutyCycleMin[Controller][Channel][Strobe] = DqLaneDutyCycle[dqlane][Controller][Channel][Strobe];
              }
              DataInt = (DqLaneDutyCycle[dqlane][Controller][Channel][Strobe]) / 100;
              MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "%3u.%02u ", DataInt, ((DqLaneDutyCycle[dqlane][Controller][Channel][Strobe]) - (100 * DataInt)));

            } //dqlane loop
            DataInt = (DutyCycleMax[Controller][Channel][Strobe]) / 100;
            DataInt2 = (DutyCycleMin[Controller][Channel][Strobe]) / 100;
            MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "  %3u.%02u      %u.%02u\n",
                           DataInt,  ((DutyCycleMax[Controller][Channel][Strobe]) - (100 * DataInt)),
                           DataInt2, ((DutyCycleMin[Controller][Channel][Strobe]) - (100 * DataInt2)));

          } // strobe loop
        } // channel loop
      } // controller loop

      MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\n");

      AllControllerPass = TRUE;
      for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
        AllChPass = TRUE;
        if (DccResultPassController[Controller] == 1) {
          continue;
        }
        for (Channel = 0; Channel < Outputs->MaxChannels; Channel++) {
          if (!(MrcRankExist (MrcData, Controller, Channel, Rank))) {
            continue;
          }
          if (DccResultPassCh[Controller][Channel] == 1) {
            continue;
          }
          AllStrobePass = TRUE;
          for (Strobe = 0; Strobe < Outputs->SdramCount; Strobe++) {
            if (DccResultPassStrobe[Controller][Channel][Strobe] == 1) {
              continue;
            }
            DccCodeOld[Controller][Channel][Strobe] = DccCode[Controller][Channel][Strobe];
            //
            // determine next common DCC code step for the byte
            //
            MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "MC%u.C%u.S%u ", Controller, Channel, Strobe);
            if (DutyCycleMax[Controller][Channel][Strobe] + DutyCycleMin[Controller][Channel][Strobe] > DccTarget[Controller][Channel][Strobe]) {
              // overall duty cycle is too high:
              DccStatus[Controller][Channel][Strobe] = DccHigh;
              MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "DccHigh ");
              if (SdlDccType == RxPostSdlDcc) {
                AdjustVcdlDccCode (MrcData, DccCode[Controller][Channel][Strobe], DccHigh, &DccCode[Controller][Channel][Strobe], &Saturated);
              } else {
                AdjustRxDqsVocCode (MrcData, DccCode[Controller][Channel][Strobe], DccHigh, &DccCode[Controller][Channel][Strobe], &Saturated);
              }
            } else if (DutyCycleMax[Controller][Channel][Strobe] + DutyCycleMin[Controller][Channel][Strobe] < DccTarget[Controller][Channel][Strobe]) {
              // overall duty cycle is too low:
              DccStatus[Controller][Channel][Strobe] = DccLow;
              MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "DccLow ");
              if (SdlDccType == RxPostSdlDcc) {
                AdjustVcdlDccCode (MrcData, DccCode[Controller][Channel][Strobe], DccLow, &DccCode[Controller][Channel][Strobe], &Saturated);
              } else {
                AdjustRxDqsVocCode (MrcData, DccCode[Controller][Channel][Strobe], DccLow, &DccCode[Controller][Channel][Strobe], &Saturated);
              }
            } else {
              // perfectly centered duty cycle
              MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "PASS ");
              DccDone[Controller][Channel][Strobe] = TRUE;
            }

            MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "DccCodeOld=%d, status=%d --> DccCode=%d, Saturated=%d ",
                           DccCodeOld[Controller][Channel][Strobe], DccStatus[Controller][Channel][Strobe], DccCode[Controller][Channel][Strobe], Saturated);

            if ((Saturated == FALSE) && (DccDone[Controller][Channel][Strobe] == FALSE)) {
              //
              // next correction step is possible
              // detect dither
              //
              if ((DccStatus[Controller][Channel][Strobe] == DccLow) && (DccPrevStatus[Controller][Channel][Strobe] == DccHigh)) {
                // dither low condition
                DccDone[Controller][Channel][Strobe] = TRUE;
                MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "PASS by dither High -> Low\n", Controller, Channel, Strobe);
              } else {
                if (SdlDccType == RxPostSdlDcc) {
                  // dcc code adjustment
                  SetRxVcdlDccCode (MrcData, Controller, Channel, Strobe, DccCode[Controller][Channel][Strobe], MRC_PRINTS_OFF);
                } else {
                  SetRxDqsVoc (MrcData, Controller, Channel, Rank, Strobe, DccCode[Controller][Channel][Strobe], MRC_PRINTS_OFF);
                }
                DccPrevStatus[Controller][Channel][Strobe] = DccStatus[Controller][Channel][Strobe];
              }
            }
            if ((Saturated == TRUE) || (DccDone[Controller][Channel][Strobe] == TRUE)) {
              // dcc is done or saturated
              if ((DutyCycleMax[Controller][Channel][Strobe] < RXDQS_DCC_THRESHOLD_HI) &&
                  (DutyCycleMin[Controller][Channel][Strobe] > RXDQS_DCC_THRESHOLD_LO)) {
                MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "PASS %s the threshold\n", "within");
              } else {
                MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "PASS %s the threshold\n", "outside");
              }
              DccResultPassStrobe[Controller][Channel][Strobe] = 1;
            } else {
              AllStrobePass = FALSE;
              MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\n");
            }
          } //strobe loop
          if (AllStrobePass == TRUE) {
            DccResultPassCh[Controller][Channel] = 1;
          }
          if (DccResultPassCh[Controller][Channel] == 0) {
            AllChPass = FALSE;
          }
        } //channel loop
        if (AllChPass == TRUE) {
          DccResultPassController[Controller] = 1;
        }
        if (DccResultPassController[Controller] == 0) {
          AllControllerPass = FALSE;
        }
      } //controller loop
      if (AllControllerPass == TRUE) {
        break;
      }
    } // DccCorrectionCount

    // Stop CPGC
    CpgcSetupPostSdlDcc (MrcData, FALSE, Rank, &McChBitMask, &SaveData);

    // Disable Continuous Burst mode in DDR5
    StartJedecContBurstModePerRank (MrcData, FALSE, Rank);

    if (SdlDccType == RxPostSdlDcc) {
      //for rxpost sdl DCC, no need to run per rank, just need to run one time.
      break;
    }
  } //Rank
  //
  // Exit RxPostSdlDcc
  //
  if (!UseRxDqsGen) {
    SetRxDqsDcd (MrcData, TRUE, FALSE, SavedParamsRxDqs, SaveRxDqsP, SaveRxDqsN);
  } else {
    SetRxDqsDcd (MrcData, FALSE, FALSE, SavedParamsRxDqs, SaveRxDqsP, SaveRxDqsN);
    RxDqsGenEn (MrcData, FALSE, SavedParamsRxDqs);
  }

  // Exit Dcc Accumulator Mode
  SetupDccAccData (MrcData, FALSE, TotalSampleCounts, DCD_GROUP_SRZ, SavedParams);

  ForceRcomp (MrcData);

  Status = IoReset (MrcData);
  if (SdlDccType == RxPostSdlDcc) {
    Status = MrcResetSequence (MrcData);
  }
  return Status;
}

/**
  This routine adjusts the VCDL DCC code

  @param[in]  MrcData     - Pointer to MRC global data.
  @param[in]  VcdlDccCode - DCC code input for adjusting.
  @param[in]  Status      - DCC Status, the value can be DccLow, DccHigh or DccAlign.
  @param[in]  DccCode     - DccCode output after adjusting.
  @param[in]  Saturate    - Saturate or not.
**/
VOID
AdjustVcdlDccCode (
  IN OUT MrcParameters *const MrcData,
  IN     UINT8                VcdlDccCode,
  IN     DCC_STATUS           Status,
  IN OUT UINT8                *DccCode,
  IN OUT BOOLEAN              *Saturate
  )
{
  *DccCode = VcdlDccCode;
  *Saturate = FALSE;

  // status can be 'low' or 'high'
  if (Status == DccLow) {
    if (VcdlDccCode < 15) {
      *DccCode += 1;
    } else {
      *Saturate = TRUE;
    }
  } else if (Status == DccHigh) {
    if (VcdlDccCode > 0) {
      *DccCode -= 1;
    } else {
      *Saturate = TRUE;
    }
  }
  return;
}


/**
  This routine adjusts the RxDqs Voc code

  @param[in]  MrcData       - Pointer to MRC global data.
  @param[in]  RxDqsVocCode  - VOC code input for adjusting.
  @param[in]  Status        - DCC Status, the value can be DccLow, DccHigh or DccAlign.
  @param[in]  DccCode       - DccCode output after adjusting.
  @param[in]  Saturate      - Saturate or not.
**/
VOID
AdjustRxDqsVocCode (
  IN OUT MrcParameters *const MrcData,
  IN     UINT8                RxDqsVocCode,
  IN     DCC_STATUS           Status,
  IN OUT UINT8                *DccCode,
  IN OUT BOOLEAN              *Saturate
  )
{
  *DccCode = RxDqsVocCode;
  *Saturate = FALSE;

  // status can be 'low' or 'high'


  if ((Status == DccHigh) && (RxDqsVocCode < RXDQS_VOC_RANGE_MAX)) {
    *DccCode = RxDqsVocCode + 1;
  } else if ((Status == DccHigh) && (RxDqsVocCode == RXDQS_VOC_RANGE_MAX)) {
    *Saturate = TRUE;
  } else if ((Status == DccLow) && (RxDqsVocCode > RXDQS_VOC_RANGE_MIN)) {
    *DccCode = RxDqsVocCode - 1;
  } else if ((Status == DccLow) && (RxDqsVocCode == RXDQS_VOC_RANGE_MIN)) {
    *Saturate = TRUE;
  }


  return;
}

/**
  This routine programs the VCDL DCC code

  @param[in]  MrcData     - Pointer to MRC global data.
  @param[in]  Controller  - Controller number.
  @param[in]  Channel     - Channel number.
  @param[in]  Strobe      - Strobe number.
  @param[in]  DccCode     - DccCode to program.
  @param[in]  Print       - Print debug message or not.
**/
VOID
SetRxVcdlDccCode (
  IN OUT MrcParameters *const MrcData,
  IN UINT32                   Controller,
  IN UINT32                   Channel,
  IN UINT32                   Strobe,
  IN UINT32                   DccCode,
  IN BOOLEAN                  Print
  )
{
  MrcOutput   *Outputs;
  UINT32      TransChannel;
  UINT32      TransStrobe;
  UINT32      Offset;
  UINT32      Rank = 0;
  DATA0CH0_CR_SRZCTL_STRUCT  DataSrzCtl;

  Outputs = &MrcData->Outputs;
  if (Print) {
    MRC_DEBUG_MSG (&Outputs->Debug, MSG_LEVEL_NOTE, "MC%u C%u S%u VCDL DCC code: %d\n", Controller, Channel, Strobe, DccCode);
  }

  TransChannel = Channel;
  TransStrobe = Strobe;
  MrcTranslateSystemToIp (MrcData, &Controller, &TransChannel, &Rank, &TransStrobe, GsmIocTxOn); // No GetSet for this register, so use another per-byte PHY CR
  Offset = DATA0CH0_CR_SRZCTL_REG +
           (DATA0CH1_CR_SRZCTL_REG - DATA0CH0_CR_SRZCTL_REG) * TransChannel +
           (DATA1CH0_CR_SRZCTL_REG - DATA0CH0_CR_SRZCTL_REG) * TransStrobe;
  DataSrzCtl.Data = MrcReadCR (MrcData, Offset);
  DataSrzCtl.Bits.cktop_cmn_bonus = DataSrzCtl.Bits.cktop_cmn_bonus & 0xf0;
  DataSrzCtl.Bits.cktop_cmn_bonus = DataSrzCtl.Bits.cktop_cmn_bonus | (DccCode | 0x10);
  MrcWriteCR (MrcData, Offset, DataSrzCtl.Data);

  return;
}

/**
  This routine programs the Rx DQS PI

  @param[in]  MrcData     - Pointer to MRC global data.
  @param[in]  PiCode      - Pi Code to program.
  @param[in]  Print       - Print debug message or not.
**/
VOID
SetRxPi (
  IN OUT MrcParameters *const MrcData,
  IN UINT32                   PiCode,
  IN BOOLEAN                  Print
  )
{
  INT64                       GetSetVal;
  MrcOutput                   *Outputs;

  Outputs = &MrcData->Outputs;

  if (Print) {
    MRC_DEBUG_MSG (&Outputs->Debug, MSG_LEVEL_NOTE, " SetRxPi with Pi code %d\n", PiCode);
  }

  GetSetVal = PiCode;
  MrcGetSetStrobe (MrcData, MAX_CONTROLLER, MAX_CHANNEL, MAX_RANK_IN_CHANNEL, MAX_SDRAM_IN_DIMM, RxDqsPDelay, ForceWriteCached, &GetSetVal);
  MrcGetSetStrobe (MrcData, MAX_CONTROLLER, MAX_CHANNEL, MAX_RANK_IN_CHANNEL, MAX_SDRAM_IN_DIMM, RxDqsNDelay, ForceWriteCached, &GetSetVal);

  return;
}

/**
  This routine sets the Rx DQS DCD

  @param[in]  MrcData          - Pointer to MRC global data.
  @param[in]  PadRxEnable      - Pad Rx Enable or not.
  @param[in]  Enable           - Enable Rx DQS DCD or diable it.
  @param[in]  SavedParamsRxDqs - Storage to save and restore the CSRs.
  @param[in]  SaveRxDqsP       - Storage to save and restore the RxDqsP CSRs.
  @param[in]  SaveRxDqsN       - Storage to save and restore the RxDqsN CSRs.

**/
VOID
SetRxDqsDcd (
  IN OUT MrcParameters *const MrcData,
  IN BOOLEAN                  PadRxEnable,
  IN BOOLEAN                  Enable,
  IN UINT32                   SavedParamsRxDqs[MAX_RX_POST_SDL_DCC_SAVED_PARAMETERS],
  IN INT64                    SaveRxDqsP[MAX_RANK_IN_CHANNEL][MAX_CONTROLLER][MAX_CHANNEL][MAX_SDRAM_IN_DIMM],
  IN INT64                    SaveRxDqsN[MAX_RANK_IN_CHANNEL][MAX_CONTROLLER][MAX_CHANNEL][MAX_SDRAM_IN_DIMM]
  )
{
  MrcOutput                               *Outputs;
  const MrcInput                          *Inputs;
  UINT32                                  Controller;
  UINT32                                  Channel;
  UINT32                                  Strobe;
  UINT32                                  FirstController;
  UINT32                                  FirstChannel;
  UINT32                                  TransChannel;
  UINT32                                  TransStrobe;
  UINT32                                  Rank;
  UINT32                                  Offset;
  INT64                                   GetSetVal;
  INT64                                   GetSetForceOdtOnVal;
  UINT32                                  ViewdigCmnViewDigPort0SelValue = 0;
  UINT32                                  DqdcdSrzCmnDcdselSecondaryClkValue;
  UINT32                                  DxtxCmnTxdyncpadreduxDis;
  UINT32                                  DqsrxCmnRxdiffAmpEn;
  UINT32                                  RxsdlCmnRxunmatchclkInv;
  UINT32                                  RxDqsOffsetValue;
  UINT32                                  DqsFwdClkpOffsetValue;
  UINT32                                  DqsFwdClkpValue;
  UINT32                                  RxDqsPiUiOffsetValue;
  DATA0CH0_CR_VIEWCTL_STRUCT              DataViewCtl;
  DATA0CH0_CR_SRZCTL_STRUCT               DataSrzCtl;
  DATA0CH0_CR_DQTXCTL0_STRUCT             DataDqTxCtl0;
  DATA0CH0_CR_DQSTXRXCTL_STRUCT           DataDqsTxRxCtl;
  DATA0CH0_CR_SDLCTL0_STRUCT              DataSdlCtl0;
  DATA0CH0_CR_DDRCRDATAOFFSETTRAIN_STRUCT DdrDataOffsetTrain;
  DATA0CH0_CR_DDRCRDATAOFFSETTRAIN_STRUCT DdrDataOffsetTrainTemp;
  DATA0CH0_CR_DDRCRDATACOMPVTT_STRUCT     DdrDataCompVtt;
  DATA0CH0_CR_DDRCRDATACOMP1_STRUCT       DdrDataComp1;
  DATA0CH0_CR_DDRCRDATACONTROL2_STRUCT    DataControl2;
  UINT8                                   CompupdatedoneOvrvalValue;
  UINT8                                   CompupdateOvrenValue;
  UINT32                                  RxsdlCmnQualifysdlwithrcvenValue;
  DATA0CH0_CR_SDLCTL1_STRUCT              DataSdlCtl1;
  UINT8                                   RxsdlCmnSrzrcvenpimodovrdenValue;
  BOOLEAN                                 A0;
  DATA0CH0_CR_AFEMISCCTRL1_STRUCT         DataAfeMiscCtrl1;
  UINT32                                  CktopCmnBonusValue = 0;
  Outputs           = &MrcData->Outputs;
  Inputs            = &MrcData->Inputs;
  FirstController   = Outputs->FirstPopController;
  FirstChannel      = Outputs->Controller[FirstController].FirstPopCh;
  Rank = 0;
  A0 = Inputs->A0;

  if (Enable == TRUE) {
    if (((Inputs->A0) || (Inputs->B0)) || (Inputs->J0)) {
      ViewdigCmnViewDigPort0SelValue = 6;
    }
    DqdcdSrzCmnDcdselSecondaryClkValue = 1;
    DxtxCmnTxdyncpadreduxDis = 1;
    DqsrxCmnRxdiffAmpEn = 1;
    RxsdlCmnRxunmatchclkInv = 1;
    GetSetForceOdtOnVal = 1;
    RxDqsPiUiOffsetValue = 0;
    RxDqsOffsetValue = 0;
    DqsFwdClkpOffsetValue = 0;
    DqsFwdClkpValue = 0;
    CompupdatedoneOvrvalValue = 1;
    CompupdateOvrenValue = 1;
    RxsdlCmnQualifysdlwithrcvenValue = 0;
    RxsdlCmnSrzrcvenpimodovrdenValue = 1;
  } else {
    if (((Inputs->A0) || (Inputs->B0)) || (Inputs->J0)) {
      ViewdigCmnViewDigPort0SelValue = 0;
    }
    DqdcdSrzCmnDcdselSecondaryClkValue = 0;
    DxtxCmnTxdyncpadreduxDis = 0;
    DqsrxCmnRxdiffAmpEn = SavedParamsRxDqs[DQSRX_CMN_RXDIFFAMPEN_INDEX];
    RxsdlCmnRxunmatchclkInv = 0;
    GetSetForceOdtOnVal = 0;
    DdrDataOffsetTrainTemp.Data = SavedParamsRxDqs[DATA0CH0_CR_DDRCRDATAOFFSETTRAIN_INDEX];
    RxDqsPiUiOffsetValue = DdrDataOffsetTrainTemp.Bits.rxdqspiuioffset;
    RxDqsOffsetValue = DdrDataOffsetTrainTemp.Bits.rxdqsoffset;


    DqsFwdClkpOffsetValue = SavedParamsRxDqs[DQS_FWDCLKP_OFFSET_INDEX];
    DqsFwdClkpValue = SavedParamsRxDqs[DQS_FWDCLKP_INDEX];
    CompupdatedoneOvrvalValue = 0;
    CompupdateOvrenValue = 0;
    RxsdlCmnQualifysdlwithrcvenValue = SavedParamsRxDqs[RXSDL_CMN_QUALIFYSDLWITHRCVEN_INDEX];
    CktopCmnBonusValue = SavedParamsRxDqs[CKTOP_CMN_BONUS_INDEX];
    RxsdlCmnSrzrcvenpimodovrdenValue = 0;
  }
  //
  // For A0 enable RxDQS observation, can enable for all steppings since it doesn't hurt
  // port0 input6 selects a static high signal for observation in DQ buffer
  //
  for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
    for (Channel = 0; Channel < Outputs->MaxChannels; Channel++) {
      if (MrcChannelExist (MrcData, Controller, Channel)) {
        for (Strobe = 0; Strobe < Outputs->SdramCount; Strobe++) {
          TransChannel = Channel;
          TransStrobe  = Strobe;
          MrcTranslateSystemToIp (MrcData, &Controller, &TransChannel, &Rank, &TransStrobe, GsmIocTxOn);
          Offset = DATA0CH0_CR_VIEWCTL_REG +
                   (DATA0CH1_CR_VIEWCTL_REG - DATA0CH0_CR_VIEWCTL_REG) * TransChannel +
                   (DATA1CH0_CR_VIEWCTL_REG - DATA0CH0_CR_VIEWCTL_REG) * TransStrobe;
          DataViewCtl.Data = MrcReadCR (MrcData, Offset);
          DataViewCtl.Bits.viewdig_cmn_viewdigport0sel = ViewdigCmnViewDigPort0SelValue;
          MrcWriteCR (MrcData, Offset, DataViewCtl.Data);
          //
          //select secondary DCD signal to select RxDQS feedback
          //
          TransChannel = Channel;
          TransStrobe  = Strobe;
          MrcTranslateSystemToIp (MrcData, &Controller, &TransChannel, &Rank, &TransStrobe, GsmIocTxOn);
          Offset = DATA0CH0_CR_SRZCTL_REG +
                   (DATA0CH1_CR_SRZCTL_REG - DATA0CH0_CR_SRZCTL_REG) * TransChannel +
                   (DATA1CH0_CR_SRZCTL_REG - DATA0CH0_CR_SRZCTL_REG) * TransStrobe;
          DataSrzCtl.Data = MrcReadCR (MrcData, Offset);
          DataSrzCtl.Bits.dqdcdsrz_cmn_dcdselsecondaryclk = DqdcdSrzCmnDcdselSecondaryClkValue;
          MrcWriteCR (MrcData, Offset, DataSrzCtl.Data);

          Offset = DATA0CH0_CR_DDRCRDATACONTROL2_REG +
                   (DATA0CH1_CR_DDRCRDATACONTROL2_REG - DATA0CH0_CR_DDRCRDATACONTROL2_REG) * TransChannel +
                   (DATA1CH0_CR_DDRCRDATACONTROL2_REG - DATA0CH0_CR_DDRCRDATACONTROL2_REG) * TransStrobe;
          DataControl2.Data = MrcReadCR(MrcData, Offset);

          if (A0) {
            DataControl2.BitsA0.compupdatedone_ovrval = CompupdatedoneOvrvalValue;
          } else {
            DataControl2.Bits.compupdatedone_ovrval = CompupdatedoneOvrvalValue;
          }
          MrcWriteCR(MrcData, Offset, DataControl2.Data);
          MrcWait(MrcData, 1); //1 ns

          if (A0) {
            DataControl2.BitsA0.compupdate_ovren = CompupdateOvrenValue;
          } else {
            DataControl2.Bits.compupdate_ovren = CompupdateOvrenValue;
          }

          MrcWriteCR(MrcData, Offset, DataControl2.Data);
          MrcWait(MrcData, 1); //1 ns
        }
      }
    }
  }

  Offset = DATA0CH0_CR_DQTXCTL0_REG;
  DataDqTxCtl0.Data = MrcReadCR (MrcData, Offset);
  DataDqTxCtl0.Bits.dxtx_cmn_txdyncpadreduxdis = DxtxCmnTxdyncpadreduxDis;
  MrcWriteCrMulticast (MrcData, DATA_CR_DQTXCTL0_REG, DataDqTxCtl0.Data);
  //
  // Save and restore rxsdldqsnpicode and rxsdldqsppicode.
  //
  for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
    for (Channel = 0; Channel < Outputs->MaxChannels; Channel++) {
      if (!MrcChannelExist (MrcData, Controller, Channel)) {
        continue;
      }
      for (Rank = 0; Rank < MAX_RANK_IN_CHANNEL; Rank++) {
        if (!MrcRankExist (MrcData, Controller, Channel, Rank)) {
          continue;
        }
        for (Strobe = 0; Strobe < Outputs->SdramCount; Strobe++) {

          if (Enable) {
            MrcGetSetStrobe (MrcData, Controller, Channel, Rank, Strobe, RxDqsPDelay, ReadUncached, &SaveRxDqsP[Rank][Controller][Channel][Strobe]);
            MrcGetSetStrobe (MrcData, Controller, Channel, Rank, Strobe, RxDqsNDelay, ReadUncached, &SaveRxDqsN[Rank][Controller][Channel][Strobe]);
          } else {
            MrcGetSetStrobe (MrcData, Controller, Channel, Rank, Strobe, RxDqsPDelay, ForceWriteCached, &SaveRxDqsP[Rank][Controller][Channel][Strobe]);
            MrcGetSetStrobe (MrcData, Controller, Channel, Rank, Strobe, RxDqsNDelay, ForceWriteCached, &SaveRxDqsN[Rank][Controller][Channel][Strobe]);
          }
        }
      }
    }
  }

  if (Enable) {
    TransChannel = FirstChannel;
    Controller = FirstController;
    TransStrobe = 0;
    MrcTranslateSystemToIp (MrcData, &Controller, &TransChannel, &Rank, &TransStrobe, GsmIocTxOn);


    //
    // determine whether matched path is enabled or not, setting is not modified, but will be used to evaluate duty cycle polarity
    //
    Offset = DATA0CH0_CR_SDLCTL0_REG +
             (DATA0CH1_CR_SDLCTL0_REG - DATA0CH0_CR_SDLCTL0_REG) * TransChannel +
             (DATA1CH0_CR_SDLCTL0_REG - DATA0CH0_CR_SDLCTL0_REG) * TransStrobe;
    DataSdlCtl0.Data = MrcReadCR (MrcData, Offset);
    SavedParamsRxDqs[RX_MATCHED_PATH_EN_INDEX] = DataSdlCtl0.Bits.rxsdl_cmn_rxmatchedpathen;

    Offset = DATA0CH0_CR_SDLCTL1_REG +
             (DATA0CH1_CR_SDLCTL1_REG - DATA0CH0_CR_SDLCTL1_REG) * TransChannel +
             (DATA1CH0_CR_SDLCTL1_REG - DATA0CH0_CR_SDLCTL1_REG) * TransStrobe;
    DataSdlCtl1.Data = MrcReadCR (MrcData, Offset);
    SavedParamsRxDqs[RXSDL_CMN_QUALIFYSDLWITHRCVEN_INDEX] = DataSdlCtl1.Bits.rxsdl_cmn_qualifysdlwithrcven;


    Offset = DATA0CH0_CR_SRZCTL_REG +
             (DATA0CH1_CR_SRZCTL_REG - DATA0CH0_CR_SRZCTL_REG) * TransChannel +
             (DATA1CH0_CR_SRZCTL_REG - DATA0CH0_CR_SRZCTL_REG) * TransStrobe;
    DataSrzCtl.Data = MrcReadCR (MrcData, Offset);
    SavedParamsRxDqs[CKTOP_CMN_BONUS_INDEX] = DataSrzCtl.Bits.cktop_cmn_bonus;

    CktopCmnBonusValue = SavedParamsRxDqs[CKTOP_CMN_BONUS_INDEX] & 0x7f;

  }
  //
  // For DDR5 enable DQS senseamp and ODT to enable read path
  //
  if (PadRxEnable) {
    if (Enable) {
      TransChannel = FirstChannel;
      Controller = FirstController;
      TransStrobe = 0;
      MrcTranslateSystemToIp (MrcData, &Controller, &TransChannel, &Rank, &TransStrobe, GsmIocTxOn);
      Offset = DATA0CH0_CR_DQSTXRXCTL_REG +
               (DATA0CH1_CR_DQSTXRXCTL_REG - DATA0CH0_CR_DQSTXRXCTL_REG) * TransChannel +
               (DATA1CH0_CR_DQSTXRXCTL_REG - DATA0CH0_CR_DQSTXRXCTL_REG) * TransStrobe;
      DataDqsTxRxCtl.Data = MrcReadCR (MrcData, Offset);
      SavedParamsRxDqs[DQSRX_CMN_RXDIFFAMPEN_INDEX] = DataDqsTxRxCtl.Bits.dqsrx_cmn_rxdiffampen;
    }

    for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
      for (Channel = 0; Channel < Outputs->MaxChannels; Channel++) {
        if (MrcChannelExist (MrcData, Controller, Channel)) {
          for (Strobe = 0; Strobe < Outputs->SdramCount; Strobe++) {
            TransChannel = Channel;
            TransStrobe = Strobe;
            MrcTranslateSystemToIp (MrcData, &Controller, &TransChannel, &Rank, &TransStrobe, RxDqsEq);

            Offset = DATA0CH0_CR_DQSTXRXCTL_REG +
                     (DATA0CH1_CR_DQSTXRXCTL_REG - DATA0CH0_CR_DQSTXRXCTL_REG) * TransChannel +
                     (DATA1CH0_CR_DQSTXRXCTL_REG - DATA0CH0_CR_DQSTXRXCTL_REG) * TransStrobe;
            DataDqsTxRxCtl.Data = MrcReadCR (MrcData, Offset);
            DataDqsTxRxCtl.Bits.dqsrx_cmn_rxdiffampen = DqsrxCmnRxdiffAmpEn;
            MrcWriteCR (MrcData, Offset, DataDqsTxRxCtl.Data);
          }
        }
      }
    }
    MrcGetSetChStrb (MrcData, MAX_CONTROLLER, MAX_CHANNEL, MAX_SDRAM_IN_DIMM, GsmIocForceOdtOnWithoutDrvEn, ForceWriteUncached, &GetSetForceOdtOnVal);
  }

  //
  // set deterministic polarity of unmatched Rx by forcing rx clock invert
  //
  if (SavedParamsRxDqs[RX_MATCHED_PATH_EN_INDEX] == 0) {
    for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
      for (Channel = 0; Channel < Outputs->MaxChannels; Channel++) {
        if (MrcChannelExist (MrcData, Controller, Channel)) {
          for (Strobe = 0; Strobe < Outputs->SdramCount; Strobe++) {
            TransChannel = Channel;
            TransStrobe = Strobe;
            MrcTranslateSystemToIp (MrcData, &Controller, &TransChannel, &Rank, &TransStrobe, RxDqsEq);

            Offset = DATA0CH0_CR_SDLCTL0_REG +
                     (DATA0CH1_CR_SDLCTL0_REG - DATA0CH0_CR_SDLCTL0_REG) * TransChannel +
                     (DATA1CH0_CR_SDLCTL0_REG - DATA0CH0_CR_SDLCTL0_REG) * TransStrobe;
            DataSdlCtl0.Data = MrcReadCR (MrcData, Offset);
            DataSdlCtl0.Bits.rxsdl_cmn_rxunmatchclkinv = RxsdlCmnRxunmatchclkInv;
            MrcWriteCR (MrcData, Offset, DataSdlCtl0.Data);
          }
        }
      }
    }
  }

  if (Enable) {
    Strobe = 0;
    MrcTranslateSystemToIp (MrcData, &FirstController, &FirstChannel, &Rank, &Strobe, GsmIocTxOn);

    Offset  = DATA0CH0_CR_DDRCRDATAOFFSETTRAIN_REG  +
              (DATA0CH1_CR_DDRCRDATAOFFSETTRAIN_REG - DATA0CH0_CR_DDRCRDATAOFFSETTRAIN_REG) * FirstChannel +
              (DATA1CH0_CR_DDRCRDATAOFFSETTRAIN_REG - DATA0CH0_CR_DDRCRDATAOFFSETTRAIN_REG) * Strobe;
    DdrDataOffsetTrain.Data = MrcReadCR (MrcData, Offset);
    SavedParamsRxDqs[DATA0CH0_CR_DDRCRDATAOFFSETTRAIN_INDEX] = DdrDataOffsetTrain.Data;

    Offset = DATA0CH0_CR_DDRCRDATACOMPVTT_REG  +
             (DATA0CH1_CR_DDRCRDATACOMPVTT_REG - DATA0CH0_CR_DDRCRDATACOMPVTT_REG) * FirstChannel +
             (DATA1CH0_CR_DDRCRDATACOMPVTT_REG - DATA0CH0_CR_DDRCRDATACOMPVTT_REG) * Strobe;
    DdrDataCompVtt.Data = MrcReadCR (MrcData, Offset);
    SavedParamsRxDqs[DQS_FWDCLKP_OFFSET_INDEX] = DdrDataCompVtt.Bits.Dqs_fwdclkp_offset;
    //
    // SavedParamsRxDqs['Dqs_fwdclkp'] = DDRDATA_CR_DDRCRDATACOMP1.Dqs_fwdclkp
    //
    Offset = DATA0CH0_CR_DDRCRDATACOMP1_REG  +
             (DATA0CH1_CR_DDRCRDATACOMP1_REG - DATA0CH0_CR_DDRCRDATACOMP1_REG) * FirstChannel +
             (DATA1CH0_CR_DDRCRDATACOMP1_REG - DATA0CH0_CR_DDRCRDATACOMP1_REG) * Strobe;
    DdrDataComp1.Data = MrcReadCR (MrcData, Offset);
    SavedParamsRxDqs[DQS_FWDCLKP_INDEX] = DdrDataComp1.Bits.Dqs_fwdclkp;
  }

  GetSetVal = RxDqsPiUiOffsetValue;
  MrcGetSetChStrb (MrcData, MAX_CONTROLLER, MAX_CHANNEL, MAX_SDRAM_IN_DIMM, RxDqsPiUiOffset, ForceWriteCached,
                   &GetSetVal);

  for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
    for (Channel = 0; Channel < Outputs->MaxChannels; Channel++) {
      if (MrcChannelExist (MrcData, Controller, Channel)) {
        for (Strobe = 0; Strobe < Outputs->SdramCount; Strobe++) {
          TransChannel = Channel;
          TransStrobe  = Strobe;
          MrcTranslateSystemToIp (MrcData, &Controller, &TransChannel, &Rank, &TransStrobe, GsmIocTxOn);
          Offset = DATA0CH0_CR_DDRCRDATACOMPVTT_REG   +
                   (DATA0CH1_CR_DDRCRDATACOMPVTT_REG - DATA0CH0_CR_DDRCRDATACOMPVTT_REG  ) * TransChannel +
                   (DATA1CH0_CR_DDRCRDATACOMPVTT_REG - DATA0CH0_CR_DDRCRDATACOMPVTT_REG  ) * TransStrobe;
          DdrDataCompVtt.Data = MrcReadCR (MrcData, Offset);
          DdrDataCompVtt.Bits.Dqs_fwdclkp_offset = DqsFwdClkpOffsetValue;
          MrcWriteCR (MrcData, Offset, DdrDataCompVtt.Data);

          //
          // ddrdata_cr_ddrcrdatacomp1.dqs_fwdclkp = 0
          //
          Offset = DATA0CH0_CR_DDRCRDATACOMP1_REG   +
                   (DATA0CH1_CR_DDRCRDATACOMP1_REG - DATA0CH0_CR_DDRCRDATACOMP1_REG  ) * TransChannel +
                   (DATA1CH0_CR_DDRCRDATACOMP1_REG - DATA0CH0_CR_DDRCRDATACOMP1_REG  ) * TransStrobe;
          DdrDataComp1.Data = MrcReadCR (MrcData, Offset);
          DdrDataComp1.Bits.Dqs_fwdclkp = DqsFwdClkpValue;
          MrcWriteCR (MrcData, Offset, DdrDataComp1.Data);

          Offset = DATA0CH0_CR_SDLCTL1_REG   +
                   (DATA0CH1_CR_SDLCTL1_REG - DATA0CH0_CR_SDLCTL1_REG  ) * TransChannel +
                   (DATA1CH0_CR_SDLCTL1_REG - DATA0CH0_CR_SDLCTL1_REG  ) * TransStrobe;
          DataSdlCtl1.Data = MrcReadCR (MrcData, Offset);
          DataSdlCtl1.Bits.rxsdl_cmn_qualifysdlwithrcven = RxsdlCmnQualifysdlwithrcvenValue;
          MrcWriteCR (MrcData, Offset, DataSdlCtl1.Data);

          Offset = DATA0CH0_CR_SRZCTL_REG +
                   (DATA0CH1_CR_SRZCTL_REG - DATA0CH0_CR_SRZCTL_REG) * TransChannel +
                   (DATA1CH0_CR_SRZCTL_REG - DATA0CH0_CR_SRZCTL_REG) * TransStrobe;
          DataSrzCtl.Data = MrcReadCR (MrcData, Offset);
          if (Enable) {
            DataSrzCtl.Bits.cktop_cmn_bonus = CktopCmnBonusValue;
          } else {
            DataSrzCtl.Bits.cktop_cmn_bonus = (((DataSrzCtl.Bits.cktop_cmn_bonus) & 0x1f) | (CktopCmnBonusValue & 0xe0));
          }
          MrcWriteCR (MrcData, Offset, DataSrzCtl.Data);

          Offset = DATA0CH0_CR_AFEMISCCTRL1_REG +
                   (DATA0CH1_CR_AFEMISCCTRL1_REG - DATA0CH0_CR_AFEMISCCTRL1_REG) * TransChannel +
                   (DATA1CH0_CR_AFEMISCCTRL1_REG - DATA0CH0_CR_AFEMISCCTRL1_REG) * TransStrobe;
          DataAfeMiscCtrl1.Data = MrcReadCR (MrcData, Offset);
          DataAfeMiscCtrl1.Bits.rxsdl_cmn_srzrcvenpimodovrden = RxsdlCmnSrzrcvenpimodovrdenValue;
          if (Enable) {
            //This bit doesn't care when the rxsdl_cmn_srzrcvenpimodovrden is 0. That's the reason MRC no need to restore it back.
            DataAfeMiscCtrl1.Bits.rxsdl_cmn_srzrcvenpimodovrdval = 0;
          }
          MrcWriteCR (MrcData, Offset, DataAfeMiscCtrl1.Data);
        }
      }
    }
  }
  //
  // ddrdata_cr_ddrcrdataoffsettrain_0_0_0_mchbar.rxdqsoffset = 0
  //
  GetSetVal = RxDqsOffsetValue;
  MrcGetSetChStrb (MrcData, MAX_CONTROLLER, MAX_CHANNEL, MAX_SDRAM_IN_DIMM, RxDqsOffset, ForceWriteCached, &GetSetVal);

  return;
}

/**
This routine enables the RX Dqs Generation

@param[in]  MrcData                   - Pointer to MRC global data.
@param[in]  Enable                    - Enable Rx DQS DCD or diable it.
@param[in]  SavedParams               - Storage to save and restore the CSRs.

@retval None

**/
VOID
RxDqsGenEn (
  IN OUT MrcParameters *const MrcData,
  IN BOOLEAN                  Enable,
  IN UINT32                   SavedParams[MAX_RX_POST_SDL_DCC_SAVED_PARAMETERS]
  )
{
  MrcOutput                             *Outputs;
  UINT32                                Controller;
  UINT32                                Channel;
  UINT32                                Strobe;
  UINT32                                TransChannel;
  UINT32                                TransStrobe;
  UINT32                                FirstController;
  UINT32                                FirstChannel;
  UINT32                                Offset;
  UINT32                                Rank;
  UINT32                                RxsdlCmnQualifySdlWithRcvenValue;
  UINT32                                MdllCmnLdoLocalVsshiBypassValue;
  UINT32                                RxsdlCmnRxsdlBypassEnValue;
  UINT32                                RxPionValue;
  UINT32                                RxsdlCmnSrzRcvenpimodovrdenValue;
  INT64                                 RxPiOn;
  DATA0CH0_CR_SDLCTL1_STRUCT            DataSdlCtl1;
  DATA0CH0_CR_SDLCTL1_STRUCT            DataSdlCtl1Temp;
  DATA0CH0_CR_MDLLCTL3_STRUCT           DataMdllCtl3;
  DATA0CH0_CR_AFEMISCCTRL1_STRUCT       DataAfeMiscCtrl1;

  Outputs           = &MrcData->Outputs;
  FirstController   = Outputs->FirstPopController;
  FirstChannel      = Outputs->Controller[FirstController].FirstPopCh;
  Rank = 0;

  if (Enable) {
    RxsdlCmnQualifySdlWithRcvenValue = 1;
    MdllCmnLdoLocalVsshiBypassValue = 1;
    RxsdlCmnRxsdlBypassEnValue = 1;
    RxPionValue = 1;
    RxsdlCmnSrzRcvenpimodovrdenValue = 1;
  } else {
    DataSdlCtl1Temp.Data = SavedParams[RXSDL_CMN_QUALIFYSDLWITHRCVEN_INDEX];
    RxsdlCmnQualifySdlWithRcvenValue = DataSdlCtl1Temp.Bits.rxsdl_cmn_qualifysdlwithrcven;
    MdllCmnLdoLocalVsshiBypassValue = 0;
    RxsdlCmnRxsdlBypassEnValue = 0;
    RxPionValue = SavedParams[RX_PI_ON_INDEX];
    RxsdlCmnSrzRcvenpimodovrdenValue = 0;
  }
  if (Enable) {
    //
    // force rcven low and enable rxdqsgen
    // forces afe rcven reset, assumes no pending read transactions that will reassert rcven
    // to complete rxtrainreset pulse
    //
    IssueRxReset (MrcData);
    //
    // save the previous value of qualifysdlwithrcvern
    //
    TransChannel = FirstChannel;
    Controller = FirstController;
    TransStrobe = 0;
    MrcTranslateSystemToIp (MrcData, &Controller, &TransChannel, &Rank, &TransStrobe, GsmIocTxOn);
    Offset = DATA0CH0_CR_SDLCTL1_REG +
             (DATA0CH1_CR_SDLCTL1_REG - DATA0CH0_CR_SDLCTL1_REG) * TransChannel +
             (DATA1CH0_CR_SDLCTL1_REG - DATA0CH0_CR_SDLCTL1_REG) * TransStrobe;
    DataSdlCtl1.Data = MrcReadCR (MrcData, Offset);
    SavedParams[RXSDL_CMN_QUALIFYSDLWITHRCVEN_INDEX] = DataSdlCtl1.Data;

    MrcGetSetChStrb (MrcData, FirstController, FirstChannel, 0, GsmIocRxPiPwrDnDis, ReadUncached, &RxPiOn);
    SavedParams[RX_PI_ON_INDEX] = (UINT32) RxPiOn;
  }
  //
  // force bypass mux to use rcvenpimod as the select
  //
  for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
    for (Channel = 0; Channel < Outputs->MaxChannels; Channel++) {
      if (MrcChannelExist (MrcData, Controller, Channel)) {
        for (Strobe = 0; Strobe < Outputs->SdramCount; Strobe++) {
          TransChannel = Channel;
          TransStrobe  = Strobe;
          MrcTranslateSystemToIp (MrcData, &Controller, &TransChannel, &Rank, &TransStrobe, GsmIocTxOn);
          Offset = DATA0CH0_CR_SDLCTL1_REG   +
                   (DATA0CH1_CR_SDLCTL1_REG - DATA0CH0_CR_SDLCTL1_REG  ) * TransChannel +
                   (DATA1CH0_CR_SDLCTL1_REG - DATA0CH0_CR_SDLCTL1_REG  ) * TransStrobe;
          DataSdlCtl1.Data = MrcReadCR (MrcData, Offset);
          DataSdlCtl1.Bits.rxsdl_cmn_qualifysdlwithrcven = RxsdlCmnQualifySdlWithRcvenValue;
          MrcWriteCR (MrcData, Offset, DataSdlCtl1.Data);

          //
          // force bypass mux to use rcvenpimod as the select
          // repurposed config to enable rxdqs generator
          //
          Offset = DATA0CH0_CR_MDLLCTL3_REG   +
                   (DATA0CH1_CR_MDLLCTL3_REG - DATA0CH0_CR_MDLLCTL3_REG  ) * TransChannel +
                   (DATA1CH0_CR_MDLLCTL3_REG - DATA0CH0_CR_MDLLCTL3_REG  ) * TransStrobe;
          DataMdllCtl3.Data = MrcReadCR (MrcData, Offset);
          DataMdllCtl3.Bits.mdll_cmn_ldolocalvsshibypass = MdllCmnLdoLocalVsshiBypassValue;
          MrcWriteCR (MrcData, Offset, DataMdllCtl3.Data);
          //
          // overrides rcven in the rxflopslice to ungate strobes going to the rxfifo
          //
          Offset = DATA0CH0_CR_AFEMISCCTRL1_REG +
                   (DATA0CH1_CR_AFEMISCCTRL1_REG - DATA0CH0_CR_AFEMISCCTRL1_REG) * TransChannel +
                   (DATA1CH0_CR_AFEMISCCTRL1_REG - DATA0CH0_CR_AFEMISCCTRL1_REG) * TransStrobe;
          DataAfeMiscCtrl1.Data = MrcReadCR (MrcData, Offset);
          DataAfeMiscCtrl1.Bits.rxsdl_cmn_rxsdlbypassen = RxsdlCmnRxsdlBypassEnValue;
          MrcWriteCR (MrcData, Offset, DataAfeMiscCtrl1.Data);
          //
          // force rxpis on to allow strobe to propagate through sdl
          //
          RxPiOn = RxPionValue;
          MrcGetSetChStrb (MrcData, Controller, Channel, Strobe, GsmIocRxPiPwrDnDis, ForceWriteUncached, &RxPiOn);

          Offset = DATA0CH0_CR_AFEMISCCTRL1_REG +
                   (DATA0CH1_CR_AFEMISCCTRL1_REG - DATA0CH0_CR_AFEMISCCTRL1_REG) * TransChannel +
                   (DATA1CH0_CR_AFEMISCCTRL1_REG - DATA0CH0_CR_AFEMISCCTRL1_REG) * TransStrobe;
          DataAfeMiscCtrl1.Data = MrcReadCR (MrcData, Offset);
          DataAfeMiscCtrl1.Bits.rxsdl_cmn_srzrcvenpimodovrden = RxsdlCmnSrzRcvenpimodovrdenValue;
          MrcWriteCR (MrcData, Offset, DataAfeMiscCtrl1.Data);
        }
      }
    }
  }
}


/**
  This routine will start the DDR5 DRAM Continuous Burst Output per rank.

  @param[in]  MrcData - Pointer to MRC global data.
  @param[in]  Enable  - Enable continous burst mode or not.
  @param[in]  Rank    - Rank number.
**/
VOID
StartJedecContBurstModePerRank (
  IN OUT MrcParameters *const MrcData,
  IN BOOLEAN                  Enable,
  IN UINT32                   Rank
  )
{
  MrcOutput   *Outputs;
  UINT8       MrrResult[4];
  UINT32      Mr28Value;
  UINT32      Mr29Value;
  UINT32      MR25Value;
  UINT32      Controller;
  UINT32      Channel;

  //
  // Program MR26 to set up read data pattern0, use default of 0x5a
  // Program MR27 to set up read data pattern1, use default of 0x3c
  // Program MR28 to set up DQ invert for the lower data, program 0x55
  // Program MR29 to set up DQ invert for the upper data, program 0x55
  // Program MR25 OP[3] to 1 to set up continuous strobe mode
  // MRR to MR31 to start continuous strobe mode
  // Program MR25 OP[3] to 0 to stop continuous strobe mode
  //
  Outputs = &MrcData->Outputs;


  if (Enable) {
    Mr28Value = 0x55;
    Mr29Value = 0x55;
    MR25Value = 0x08;
  } else {
    Mr28Value = 0;
    Mr29Value = 0;
    MR25Value = 0;
  }

  for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
    for (Channel = 0; Channel < Outputs->MaxChannels; Channel++) {
      if (MrcChannelExist (MrcData, Controller, Channel)) {
        if (!MrcRankExist (MrcData, Controller, Channel, Rank)) {
          continue;
        }

        if (Outputs->DdrType == MRC_DDR_TYPE_DDR5) {
          if (Enable) {
            MrcIssueMrw (MrcData, Controller, Channel, Rank, mrMR26, 0x5a, TRUE);
            MrcIssueMrw (MrcData, Controller, Channel, Rank, mrMR27, 0x3c, TRUE);
          }

          MrcIssueMrw (MrcData, Controller, Channel, Rank, mrMR28, Mr28Value, TRUE);
          MrcIssueMrw (MrcData, Controller, Channel, Rank, mrMR29, Mr29Value, TRUE);
          MrcIssueMrw (MrcData, Controller, Channel, Rank, mrMR25, MR25Value, TRUE);

          MrcWait (MrcData, 1 * MRC_TIMER_1US);
          if (Enable) {
            //
            // Send MRR to MR31
            //
            MrcIssueMrr (MrcData, Controller, Channel, Rank, 31, MrrResult);
          } else {
            MrcIssueMrw (MrcData, Controller, Channel, Rank, mrMR25, 0x00, TRUE);
          }
        } else if (Outputs->DdrType == MRC_DDR_TYPE_LPDDR5) {
          if (Enable) {
            //Enable RDQS Toggle Mode for LP5
            MrcIssueMrw (MrcData, Controller, Channel, Rank, mrMR46, 2, MRC_PRINTS_ON);
          } else {
            //Disable RDQS Toggle Mode for LP5
            MrcIssueMrw (MrcData, Controller, Channel, Rank, mrMR46, 0, MRC_PRINTS_ON);
          }
        }
      }
    }
  }
}

/**
  This routine will setup or clean up CPGC for Rx POST SDL DCC

  @param[in]  MrcData      - Pointer to MRC global data.
  @param[in]  Enable       - CPGC setup for Rx POST SDL DCC or clean up to exit Rx POST SDL DCC.
  @param[in]  Rank         - Rank number.
  @param[in]  McChBitMask  - Pointer to the Channel bit mask.
  @param[in]  SaveData     - Pointer to the SaveData used for SetupMcMprConfig.
**/
VOID
CpgcSetupPostSdlDcc (
  IN OUT MrcParameters *const MrcData,
  IN BOOLEAN                  Enable,
  IN UINT32                   Rank,
  UINT8                       *McChBitMask,
  MC_MPR_CONFIG_SAVE          *SaveData
  )
{
  MrcOutput       *Outputs;
  MrcChannelOut   *ChannelOut;
  MrcDdrType      DdrType;
  UINT32          Offset;
  UINT32          IpChannel;
  UINT8           Channel;
  UINT8           MaxChannels;
  UINT8           Controller;
  UINT8           CurMcChBitMask;
  BOOLEAN         Lpddr;
  BOOLEAN         Ddr5;
  UINT8           TestDoneStatus;
  UINT8           LoopCount;
  MC0_REQ0_CR_CPGC_SEQ_CTL_STRUCT     CpgcSeqCtl;
  MC0_REQ0_CR_CPGC_SEQ_STATUS_STRUCT  CpgcSeqStatus;
  MC0_STALL_DRAIN_STRUCT              StallDrain;

  Outputs     = &MrcData->Outputs;
  DdrType     = Outputs->DdrType;
  Lpddr       = Outputs->Lpddr;
  Ddr5        = (DdrType == MRC_DDR_TYPE_DDR5);
  MaxChannels = Outputs->MaxChannels;
  LoopCount   = 10;

  if (Enable) {
    *McChBitMask = Outputs->McChBitMask;
    if (Ddr5) {
      SetupMcMprConfig (MrcData, SaveData, MRC_ENABLE);
    }
    SetupIOTestMPR (MrcData, *McChBitMask, LoopCount, NSOE, 0, 0);
    MrcSetLoopcount (MrcData, 0x80000000);

    *McChBitMask = 0;
    for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
      for (Channel = 0; Channel < MaxChannels; Channel++) {
        (*McChBitMask) |= SelectReutRanks (MrcData, Controller, Channel, (1 << Rank), FALSE, 0);
      }
    }
    // Start CPGC
    CpgcSeqCtl.Data = 0;
    CpgcSeqCtl.Bits.START_TEST = 1;
    Cpgc20ControlRegWrite (MrcData, (*McChBitMask), CpgcSeqCtl);

    // Wait to obtain RxDQS
    MrcWait (MrcData, 200 * MRC_TIMER_1NS);

  } else { // Disable

    // Stop CPGC
    CpgcSeqCtl.Data = 0;
    CpgcSeqCtl.Bits.STOP_TEST = 1;
    Cpgc20ControlRegWrite (MrcData, (*McChBitMask), CpgcSeqCtl);

    // Wait till CPGC test is done on all participating channels
    // Wait until Channel test done status matches ChBitMask
    do {
      TestDoneStatus = 0;
      for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
        if (MrcControllerExist(MrcData, Controller)) {
          Offset = OFFSET_CALC_CH (MC0_STALL_DRAIN_REG, MC1_STALL_DRAIN_REG, Controller);
          StallDrain.Data = MrcReadCR (MrcData, Offset);
          for (Channel = 0; Channel < MaxChannels; Channel++) {
            CurMcChBitMask = (1 << ((Controller * MaxChannels) + Channel));
            if ((((*McChBitMask) & CurMcChBitMask) != 0) && (!(IS_MC_SUB_CH (Lpddr, Channel)))) {
              IpChannel = LP_IP_CH (Lpddr, Channel);
              ChannelOut = &Outputs->Controller[Controller].Channel[Channel];
              Offset = OFFSET_CALC_MC_CH (MC0_REQ0_CR_CPGC_SEQ_STATUS_REG, MC1_REQ0_CR_CPGC_SEQ_STATUS_REG, Controller, MC0_REQ1_CR_CPGC_SEQ_STATUS_REG, IpChannel);
              CpgcSeqStatus.Data = MrcReadCR (MrcData, Offset);
              if ((CpgcSeqStatus.Bits.TEST_DONE) && (StallDrain.Bits.mc_drained)) {
                TestDoneStatus |= (ChannelOut->CpgcChAssign << (Controller * MaxChannels));
              }
            }
          }
        }
      }
    } while ((TestDoneStatus & (*McChBitMask)) != (*McChBitMask));
    if (Ddr5) {
      // Clean up after Test.
      SetupMcMprConfig (MrcData, SaveData, MRC_DISABLE);
    }
  }
  return;
}

/**
  Enable scr_gvblock_ovrride in CCC and DATA fubs.

  @param[in] MrcData     - The Mrc Host data structure
**/
VOID
ScrGvblockOverrideEnable (
  IN MrcParameters  *MrcData
  )
{
  UINT32                                CccFub;
  UINT32                                FubOffset;
  UINT32                                Offset;
  CCC0_GLOBAL_CR_PM_FSM_OVRD_0_STRUCT   CccGlobalPmFsmOvrd;
  DATA0CH0_CR_PMFSM1_STRUCT             DataPmFsm1;


  if (MrcData->Inputs.A0) {
    return;
  }


  // Multicasting cannot be used on CCC0/1/2/3_GLOBAL_CR_PM_FSM_OVRD_0/1
  // becasue iobufact_ovren and iobufact_ovrval are dependent on whether the channel is populated or not
  FubOffset = CCC1_GLOBAL_CR_PM_FSM_OVRD_0_REG - CCC0_GLOBAL_CR_PM_FSM_OVRD_0_REG;
  for (CccFub = 0; CccFub < MRC_NUM_CCC_INSTANCES; CccFub++) {
    Offset = ((CccFub % 2) ? CCC0_GLOBAL_CR_PM_FSM_OVRD_1_REG : CCC0_GLOBAL_CR_PM_FSM_OVRD_0_REG) + (CccFub / 2) * FubOffset;
    CccGlobalPmFsmOvrd.Data = MrcReadCR (MrcData, Offset);
    CccGlobalPmFsmOvrd.Bits.scr_gvblock_ovrride = 1;
    MrcWriteCR (MrcData, Offset, CccGlobalPmFsmOvrd.Data);
  }

  DataPmFsm1.Data = MrcReadCR (MrcData, DATA0CH0_CR_PMFSM1_REG);
  DataPmFsm1.Bits.scr_gvblock_ovrride = 1;
  MrcWriteCrMulticast (MrcData, DATA_CR_PMFSM1_REG, DataPmFsm1.Data);
}

/*
  Save frequency calibration values during LP frequency switch.

  @param[in, out] MrcData - Include all MRC global data.

  @retval MrcStatus       - mrcSuccess if successful, else an error status
*/
MrcStatus
MrcLpFreqCalibrationSave (
  IN OUT MrcParameters *const MrcData
  )
{
  MrcOutput           *Outputs;
  MrcSaveData         *SaveData;
  MrcIntOutput        *MrcIntData;
  MrcLpFreqCalSave    *CalSave;
  UINT32              Val;
  UINT32              Index;
  UINT32              Rank;
  UINT32              Controller;
  UINT32              Channel;
  UINT32              Strobe;
  UINT32              TransStrobe;
  UINT32              TransChannel;
  UINT32              Offset;


  DATA0CH0_CR_MDLLCTL2_STRUCT                   DataMdllCtl2;
  CH0CCC_CR_MDLLCTL2_STRUCT                     CccMdllCtl2;
  DDRPHY_COMP_CR_DLLCOMP_SWCAPCTRL_STRUCT       DllCompSwCapCtrl;
  DDRPHY_COMP_CR_DLLCOMP_VCDLCTRL_STRUCT        DllCompVcdlCtrl;
  DDRPHY_COMP_NEW_CR_VCCDLLCOMPDATACCC_STRUCT   VccDllCompDataCcc;
  CH0CCC_CR_CLKALIGNSTATUS_STRUCT               CccClkAlignStatus;
  DATA0CH0_CR_CLKALIGNSTATUS_STRUCT             DataClkAlignStatus;
  CH2CCC_CR_TXPBDOFFSET_STRUCT                  CccTxPbdOffset;

  Outputs     = &MrcData->Outputs;
  SaveData    = &MrcData->Save.Data;
  MrcIntData  = ((MrcIntOutput *) (MrcData->IntOutputs.Internal));

  if (Outputs->Frequency == Outputs->LowFrequency) {
    CalSave = &(SaveData->LowFreqCalSave);
  } else if (Outputs->Frequency == Outputs->HighFrequency) {
    CalSave = &(SaveData->HighFreqCalSave[MrcIntData->SaGvPoint]);
  } else {
    MRC_DEBUG_MSG (&Outputs->Debug, MSG_LEVEL_ERROR, "Unkown Frequency:%d\n", Outputs->Frequency);
    return mrcFrequencyError;
  }

  // Save BwSel/Cben calibration results
  VccDllCompDataCcc.Data = MrcReadCR (MrcData, DDRPHY_COMP_NEW_CR_VCCDLLCOMPDATACCC_REG);
  CalSave->BwSel = (UINT8) VccDllCompDataCcc.Bits.Dll_bwsel;
  DllCompVcdlCtrl.Data = MrcReadCR (MrcData, DDRPHY_COMP_CR_DLLCOMP_VCDLCTRL_REG);
  CalSave->CbEn = (UINT8) DllCompVcdlCtrl.Bits.dllcomp_cmn_vcdlcben;

  // Save calibration results
    for (Index = 0; Index < sizeof (LpFreqCalRegList) / sizeof (UINT32); Index++) {
      Val = MrcReadCR (MrcData, LpFreqCalRegList[Index]);
      CalSave->RegVal[Index] = Val;
    }

  CalSave->TxPbd_ph90incr.Data = 0;
  for (Channel = 0; Channel < MRC_NUM_CCC_INSTANCES; Channel++) {
    // Save ClkAlign ref2xclk PI code - CCC
    Offset = OFFSET_CALC_CH (CH2CCC_CR_CLKALIGNSTATUS_REG, CH0CCC_CR_CLKALIGNSTATUS_REG, Channel);
    CccClkAlignStatus.Data = MrcReadCR (MrcData, Offset);
    CalSave->CccRef2xClkPi[Channel] = (UINT8) CccClkAlignStatus.Bits.ref2xclkpicode;

    // Save Dcc VCDL delay cells and LdoFFCodeLock - CCC
    Offset = OFFSET_CALC_CH (CH2CCC_CR_MDLLCTL2_REG, CH0CCC_CR_MDLLCTL2_REG, Channel);
    CccMdllCtl2.Data = MrcReadCR (MrcData, Offset);
    CalSave->CccVcdlDccCode[Channel] = (UINT8) CccMdllCtl2.Bits.mdll_cmn_vcdldcccode;
    CalSave->CccLdoFFCodeLock[Channel] = (UINT8) CccMdllCtl2.Bits.mdll_cmn_ldoffcodelock;

    // Save Pbd Offsets
    Offset = OFFSET_CALC_CH (CH2CCC_CR_TXPBDOFFSET_REG, CH0CCC_CR_TXPBDOFFSET_REG, Channel);
    CccTxPbdOffset.Data = MrcReadCR (MrcData, Offset);
    if (Outputs->Frequency == Outputs->LowFrequency) {
      // Low frequency: set the sign bits to '1', so that the offset will be applied to ph90 which is a don't care in G1/G2.
      CalSave->TxPbd_ph90incr.Data = CalSave->TxPbd_ph90incr.Data | (0xF << (Channel * 4));
    } else {
      // High frequency: save the calibrated sign bits as-is
      CalSave->TxPbd_ph90incr.Data = CalSave->TxPbd_ph90incr.Data |
                                    ((CccTxPbdOffset.Bits.txpbd_ccc8_txpbd_ph90incr       |
                                     (CccTxPbdOffset.Bits.txpbd_ccc7_txpbd_ph90incr << 1) |
                                     (CccTxPbdOffset.Bits.txpbd_ccc6_txpbd_ph90incr << 2) |
                                     (CccTxPbdOffset.Bits.txpbd_ccc5_txpbd_ph90incr << 3)) << (Channel * 4));
    }
  }

  for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
    for (Channel = 0; Channel < Outputs->MaxChannels; Channel++) {
      if (!MrcChannelExist (MrcData, Controller, Channel)) {
        continue;
      }
      for (Strobe = 0; Strobe < Outputs->SdramCount; Strobe++) {
        TransChannel = Channel;
        TransStrobe  = Strobe;
        MrcTranslateSystemToIp (MrcData, &Controller, &TransChannel, &Rank, &TransStrobe, GsmIocTxOn); // No GetSet for this register, so use another per-byte PHY CR
        // Save ClkAlign ref2xclk PI code - Data
        Offset  = DATA0CH0_CR_CLKALIGNSTATUS_REG +
                  (DATA0CH1_CR_CLKALIGNSTATUS_REG - DATA0CH0_CR_CLKALIGNSTATUS_REG) * TransChannel +
                  (DATA1CH0_CR_CLKALIGNSTATUS_REG - DATA0CH0_CR_CLKALIGNSTATUS_REG) * TransStrobe;
        DataClkAlignStatus.Data = MrcReadCR (MrcData, Offset);
        CalSave->DataRef2xClkPi[TransChannel][TransStrobe] = (UINT8) DataClkAlignStatus.Bits.ref2xclkpicode;

        // Save Dcc VCDL delay cells and LdoFFCodeLock - Data
        Offset  = DATA0CH0_CR_MDLLCTL2_REG +
                  (DATA0CH1_CR_MDLLCTL2_REG - DATA0CH0_CR_MDLLCTL2_REG) * TransChannel +
                  (DATA1CH0_CR_MDLLCTL2_REG - DATA0CH0_CR_MDLLCTL2_REG) * TransStrobe;
        DataMdllCtl2.Data = MrcReadCR (MrcData, Offset);
        CalSave->DataVcdlDccCode[TransChannel][TransStrobe] = (UINT8) DataMdllCtl2.Bits.mdll_cmn_vcdldcccode;
        CalSave->DataLdoFFCodeLock[TransChannel][TransStrobe] = (UINT8) DataMdllCtl2.Bits.mdll_cmn_ldoffcodelock;
      }
    }
  }

  // Save LdoFFcodeLock - Comp
  DllCompSwCapCtrl.Data = MrcReadCR (MrcData, DDRPHY_COMP_CR_DLLCOMP_SWCAPCTRL_REG);
  CalSave->CompLdoFFCodeLock = (UINT8) DllCompSwCapCtrl.Bits.ldoffcodelock;

  return mrcSuccess;
}

/*
  Configures and restores frequency calibration values during LP frequency switch.

  @param[in, out] MrcData - Include all MRC global data.

  @retval MrcStatus       - mrcSuccess if successful, else an error status
*/
MrcStatus
MrcLpFreqCalibrationRestore (
  IN OUT MrcParameters *const MrcData
  )
{
  MrcStatus           Status;
  MrcOutput           *Outputs;
  MrcIntOutput        *MrcIntData;
  MrcSaveData         *SaveData;
  MrcLpFreqCalSave    *CalSave;
  UINT32              Index;
  UINT32              Rank;
  UINT32              Controller;
  UINT32              Channel;
  UINT32              Strobe;
  UINT32              TransStrobe;
  UINT32              TransChannel;
  UINT32              Offset;
  BOOLEAN             Gear1;
  BOOLEAN             Gear2;
  BOOLEAN             Gear4;
  BOOLEAN             Lpddr5;
  BOOLEAN             Ddr4;
  UINT32              QclkFrequency;
  INT32               SData32;
  UINT32              PbdOffset;
  UINT32              CccPatternEnable;

  DATA0CH0_CR_MDLLCTL0_STRUCT                     DataMdllCtl0;
  DATA0CH0_CR_MDLLCTL1_STRUCT                     DataMdllCtl1;
  DATA0CH0_CR_MDLLCTL2_STRUCT                     DataMdllCtl2;
  DATA0CH0_CR_DCCCTL4_STRUCT                      DataDccCtl4;
  DATA0CH0_CR_CLKALIGNCTL1_STRUCT                 DataClkAlignCtl1;
  DDRPHY_COMP_CR_RXDQSCOMP_CMN_CTRL1_STRUCT       RxDqsCompCmnCtrl1;
  CH0CCC_CR_DCCCTL4_STRUCT                        CccDccCtl4;
  CH0CCC_CR_DCCCTL5_STRUCT                        CccDccCtl5;
  CH0CCC_CR_MDLLCTL0_STRUCT                       CccMdllCtl0;
  CH0CCC_CR_MDLLCTL1_STRUCT                       CccMdllCtl1;
  CH0CCC_CR_MDLLCTL2_STRUCT                       CccMdllCtl2;
  CH0CCC_CR_CLKALIGNCTL1_STRUCT                   CccClkAlignCtl1;
  DDRPHY_COMP_CR_DLLCOMP_PICTRL_STRUCT            DllCompPiCtrl;
  DDRPHY_COMP_CR_DLLCOMP_DLLCTRL_STRUCT           DllCompDllCtrl;
  DDRPHY_COMP_CR_DLLCOMP_VCDLCTRL_STRUCT          DllCompVcdlCtrl;
  DDRPHY_COMP_NEW_CR_VCCDLLCOMPDATACCC_STRUCT     VccDllCompDataCcc;
  DDRPHY_COMP_CR_DLLCOMP_SWCAPCTRL_STRUCT         DllCompSwCapCtrl;
  CH2CCC_CR_TXPBDOFFSET_STRUCT                    CccTxPbdOffset;

  Outputs     = &MrcData->Outputs;
  SaveData    = &MrcData->Save.Data;
  MrcIntData  = ((MrcIntOutput *) (MrcData->IntOutputs.Internal));
  Gear2       = Outputs->Gear2;
  Gear4       = Outputs->Gear4;
  Gear1       = !Gear2 && !Gear4;
  Lpddr5      = (Outputs->DdrType == MRC_DDR_TYPE_LPDDR5);
  Ddr4        = (Outputs->DdrType == MRC_DDR_TYPE_DDR4);
  Status      = mrcSuccess;

  if (Outputs->Frequency == Outputs->LowFrequency) {
    CalSave = &(SaveData->LowFreqCalSave);
  } else if (Outputs->Frequency == Outputs->HighFrequency) {
    CalSave = &(SaveData->HighFreqCalSave[MrcIntData->SaGvPoint]);
  } else {
    MRC_DEBUG_MSG (&Outputs->Debug, MSG_LEVEL_ERROR, "Unkown Frequency:%d\n", Outputs->Frequency);
    return mrcFrequencyError;
  }

  if (Gear2) {
    QclkFrequency = Outputs->Frequency / 2;
  } else if (Gear4) {
    QclkFrequency = Outputs->Frequency / 4;
  } else { // Gear1
    QclkFrequency = Outputs->Frequency;
  }
  SData32 = 359955 - 112 * QclkFrequency;
  SData32 = DIVIDEROUND (SData32, 100000);
  SData32 = MAX (SData32, 0);

  // Freq/Gear based registers programming - CCC
  for (Channel = 0; Channel < MRC_NUM_CCC_INSTANCES; Channel++) {
    Offset = OFFSET_CALC_CH (CH2CCC_CR_MDLLCTL0_REG, CH0CCC_CR_MDLLCTL0_REG, Channel);
    CccMdllCtl0.Data = MrcReadCR (MrcData, Offset);
    CccMdllCtl0.Bits.mdll_cmn_turbocaptrim = SData32;
    CccMdllCtl0.Bits.mdll_cmn_mdllengthsel = Gear1 ? 0 : (Gear4 ? 2 : 1);
    MrcWriteCR (MrcData, Offset, CccMdllCtl0.Data);

    Offset = OFFSET_CALC_CH (CH2CCC_CR_MDLLCTL1_REG, CH0CCC_CR_MDLLCTL1_REG, Channel);
    CccMdllCtl1.Data = MrcReadCR (MrcData, Offset);
    CccMdllCtl1.Bits.mdll_cmn_vctlcaptrim = SData32;
    MrcWriteCR (MrcData, Offset, CccMdllCtl1.Data);

    if (Gear4) {
      // Ddr4 - 0x3C0, Lp5 - 0x300, Ddr5/Lp4 - 0xC0
      CccPatternEnable = Ddr4 ? 0x3C0 : (Lpddr5 ? 0x300 : 0xC0);
    } else {
      CccPatternEnable = 0;
    }
    Offset = OFFSET_CALC_CH (CH2CCC_CR_DCCCTL5_REG, CH0CCC_CR_DCCCTL5_REG, Channel);
    CccDccCtl5.Data = MrcReadCR (MrcData, Offset);
    CccDccCtl5.Bits.dcc_step3pattern_enable = CccPatternEnable;
    MrcWriteCR (MrcData, Offset, CccDccCtl5.Data);

    Offset = OFFSET_CALC_CH (CH2CCC_CR_DCCCTL4_REG, CH0CCC_CR_DCCCTL4_REG, Channel);
    CccDccCtl4.Data = MrcReadCR (MrcData, Offset);
    CccDccCtl4.Bits.dcc_step2pattern_enable = CccPatternEnable;
    MrcWriteCR (MrcData, Offset, CccDccCtl4.Data);

    // Restore Pbd Offsets
    PbdOffset = 0;
    Offset = OFFSET_CALC_CH (CH2CCC_CR_TXPBDOFFSET_REG, CH0CCC_CR_TXPBDOFFSET_REG, Channel);
    CccTxPbdOffset.Data = MrcReadCR (MrcData, Offset);
    PbdOffset = (CalSave->TxPbd_ph90incr.Data >> (Channel * 4)) & 0xF;
    CccTxPbdOffset.Data |= (PbdOffset << CH2CCC_CR_TXPBDOFFSET_txpbd_ccc8_txpbd_ph90incr_OFF);
    MrcWriteCR (MrcData, Offset, CccTxPbdOffset.Data);
  }

  // Freq/Gear based registers programming - COMP
  DllCompPiCtrl.Data = MrcReadCR(MrcData, DDRPHY_COMP_CR_DLLCOMP_PICTRL_REG);
  DllCompPiCtrl.Bits.dllcomp_cmn_gear1en          = Gear1;
  DllCompPiCtrl.Bits.dllcomp_cmn_gear2en          = Gear2;
  DllCompPiCtrl.Bits.dllcomp_cmn_gear4en          = Gear4;
  MrcWriteCR (MrcData, DDRPHY_COMP_CR_DLLCOMP_PICTRL_REG, DllCompPiCtrl.Data);

  DllCompDllCtrl.Data = MrcReadCR(MrcData, DDRPHY_COMP_CR_DLLCOMP_DLLCTRL_REG);
  DllCompDllCtrl.Bits.dllcomp_cmn_turbocaptrim = SData32;
  DllCompDllCtrl.Bits.dllcomp_cmn_mdllengthsel    = Gear2 ? 1 : (Gear4 ? 2 : 0);
  MrcWriteCR (MrcData, DDRPHY_COMP_CR_DLLCOMP_DLLCTRL_REG, DllCompDllCtrl.Data);

  DllCompVcdlCtrl.Data = MrcReadCR(MrcData, DDRPHY_COMP_CR_DLLCOMP_VCDLCTRL_REG);
  DllCompVcdlCtrl.Bits.dllcomp_cmn_vctlcaptrim = SData32;
  MrcWriteCR (MrcData, DDRPHY_COMP_CR_DLLCOMP_VCDLCTRL_REG, DllCompVcdlCtrl.Data);

  // Freq/Gear based registers programming - DATA
  for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
    for (Channel = 0; Channel < Outputs->MaxChannels; Channel++) {
      if (!MrcChannelExist (MrcData, Controller, Channel)) {
        continue;
      }
      for (Strobe = 0; Strobe < Outputs->SdramCount; Strobe++) {
        TransChannel = Channel;
        TransStrobe  = Strobe;
        MrcTranslateSystemToIp (MrcData, &Controller, &TransChannel, &Rank, &TransStrobe, GsmIocTxOn); // No GetSet for this register, so use another per-byte PHY CR
        Offset  = DATA0CH0_CR_MDLLCTL0_REG +
                  (DATA0CH1_CR_MDLLCTL0_REG - DATA0CH0_CR_MDLLCTL0_REG) * TransChannel +
                  (DATA1CH0_CR_MDLLCTL0_REG - DATA0CH0_CR_MDLLCTL0_REG) * TransStrobe;
        DataMdllCtl0.Data = MrcReadCR (MrcData, Offset);
        DataMdllCtl0.Bits.mdll_cmn_turbocaptrim = SData32;
        DataMdllCtl0.Bits.mdll_cmn_mdllengthsel = Gear1 ? 0 : (Gear4 ? 2 : 1);
        MrcWriteCR(MrcData, Offset, DataMdllCtl0.Data);

        Offset  = DATA0CH0_CR_MDLLCTL1_REG +
                  (DATA0CH1_CR_MDLLCTL1_REG - DATA0CH0_CR_MDLLCTL1_REG) * TransChannel +
                  (DATA1CH0_CR_MDLLCTL1_REG - DATA0CH0_CR_MDLLCTL1_REG) * TransStrobe;
        DataMdllCtl1.Data = MrcReadCR (MrcData, Offset);
        DataMdllCtl1.Bits.mdll_cmn_vctlcaptrim = SData32;
        MrcWriteCR(MrcData, Offset, DataMdllCtl1.Data);

        Offset = DATA0CH0_CR_DCCCTL4_REG +
                  (DATA0CH1_CR_DCCCTL4_REG - DATA0CH0_CR_DCCCTL4_REG) * TransChannel +
                  (DATA1CH0_CR_DCCCTL4_REG - DATA0CH0_CR_DCCCTL4_REG) * TransStrobe;
        DataDccCtl4.Data = MrcReadCR (MrcData, DATA0CH0_CR_DCCCTL4_REG);
        DataDccCtl4.Bits.dcc_step3pattern_enable = Gear4 ? 0x3FE : 0;
        DataDccCtl4.Bits.dcc_step2pattern_enable = Gear4 ? 0x3FE : 0;
        MrcWriteCR (MrcData, Offset, DataDccCtl4.Data);
      }
    }
  }

  // Restore calibration results
    for (Index = 0; Index < sizeof (LpFreqCalRegList) / sizeof (UINT32); Index++) {
      MrcWriteCR (MrcData, LpFreqCalRegList[Index], CalSave->RegVal[Index]);
    }

  for (Channel = 0; Channel < MRC_NUM_CCC_INSTANCES; Channel++) {
    // Restore ClkAlign ref2xclk PI code - CCC
    Offset = OFFSET_CALC_CH (CH2CCC_CR_CLKALIGNCTL1_REG, CH0CCC_CR_CLKALIGNCTL1_REG, Channel);
    CccClkAlignCtl1.Data = MrcReadCR (MrcData, Offset);
    CccClkAlignCtl1.Bits.swd0piclkpicode = CalSave->CccRef2xClkPi[Channel];
    MrcWriteCR (MrcData, Offset, CccClkAlignCtl1.Data);

    // Restore Dcc VCDL delay cells and LdoFFCodeLock - CCC
    Offset = OFFSET_CALC_CH (CH2CCC_CR_MDLLCTL2_REG, CH0CCC_CR_MDLLCTL2_REG, Channel);
    CccMdllCtl2.Data = MrcReadCR (MrcData, Offset);
    CccMdllCtl2.Bits.mdll_cmn_vcdldcccode = CalSave->CccVcdlDccCode[Channel];
    CccMdllCtl2.Bits.mdll_cmn_ldoffcodelock = CalSave->CccLdoFFCodeLock[Channel];
    MrcWriteCR (MrcData, Offset, CccMdllCtl2.Data);
  }

  for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
    for (Channel = 0; Channel < Outputs->MaxChannels; Channel++) {
      if (!MrcChannelExist (MrcData, Controller, Channel)) {
        continue;
      }
      for (Strobe = 0; Strobe < Outputs->SdramCount; Strobe++) {
        TransChannel = Channel;
        TransStrobe  = Strobe;
        MrcTranslateSystemToIp (MrcData, &Controller, &TransChannel, &Rank, &TransStrobe, GsmIocTxOn); // No GetSet for this register, so use another per-byte PHY CR
        // Restore ClkAlign ref2xclk PI code - Data
        Offset  = DATA0CH0_CR_CLKALIGNCTL1_REG +
                  (DATA0CH1_CR_CLKALIGNCTL1_REG - DATA0CH0_CR_CLKALIGNCTL1_REG) * TransChannel +
                  (DATA1CH0_CR_CLKALIGNCTL1_REG - DATA0CH0_CR_CLKALIGNCTL1_REG) * TransStrobe;
        DataClkAlignCtl1.Data = MrcReadCR (MrcData, Offset);
        DataClkAlignCtl1.Bits.swd0piclkpicode = CalSave->DataRef2xClkPi[TransChannel][TransStrobe];
        MrcWriteCR (MrcData, Offset, DataClkAlignCtl1.Data);

        // Restore Dcc VCDL delay cells and LdoFFCodeLock - CCC
        Offset  = DATA0CH0_CR_MDLLCTL2_REG +
                  (DATA0CH1_CR_MDLLCTL2_REG - DATA0CH0_CR_MDLLCTL2_REG) * TransChannel +
                  (DATA1CH0_CR_MDLLCTL2_REG - DATA0CH0_CR_MDLLCTL2_REG) * TransStrobe;
        DataMdllCtl2.Data = MrcReadCR (MrcData, Offset);
        DataMdllCtl2.Bits.mdll_cmn_vcdldcccode = CalSave->DataVcdlDccCode[TransChannel][TransStrobe];
        DataMdllCtl2.Bits.mdll_cmn_ldoffcodelock = CalSave->DataLdoFFCodeLock[TransChannel][TransStrobe];
        MrcWriteCR (MrcData, Offset, DataMdllCtl2.Data);
      }
    }
  }

  // Restore LdoFFcodeLock - Comp
  DllCompSwCapCtrl.Data = MrcReadCR (MrcData, DDRPHY_COMP_CR_DLLCOMP_SWCAPCTRL_REG);
  DllCompSwCapCtrl.Bits.ldoffcodelock = CalSave->CompLdoFFCodeLock;
  MrcWriteCR (MrcData, DDRPHY_COMP_CR_DLLCOMP_SWCAPCTRL_REG, DllCompSwCapCtrl.Data);

  // Restore BwSel/Cben calibration results
  VccDllCompDataCcc.Data = MrcReadCR (MrcData, DDRPHY_COMP_NEW_CR_VCCDLLCOMPDATACCC_REG);
  VccDllCompDataCcc.Bits.Dll_bwsel = CalSave->BwSel;
  MrcWriteCR (MrcData, DDRPHY_COMP_NEW_CR_VCCDLLCOMPDATACCC_REG, VccDllCompDataCcc.Data);

  RxDqsCompCmnCtrl1.Data = MrcReadCR (MrcData, DDRPHY_COMP_CR_RXDQSCOMP_CMN_CTRL1_REG);
  RxDqsCompCmnCtrl1.Bits.rxdqscomp_cmn_rxsdlbwsel = CalSave->BwSel;
  MrcWriteCR (MrcData, DDRPHY_COMP_CR_RXDQSCOMP_CMN_CTRL1_REG, RxDqsCompCmnCtrl1.Data);

  DllCompDllCtrl.Data = MrcReadCR (MrcData, DDRPHY_COMP_CR_DLLCOMP_DLLCTRL_REG);
  DllCompDllCtrl.Bits.dllcomp_cmn_mindlybwsel = CalSave->BwSel;
  MrcWriteCR (MrcData, DDRPHY_COMP_CR_DLLCOMP_DLLCTRL_REG, DllCompDllCtrl.Data);

  DllCompVcdlCtrl.Data = MrcReadCR (MrcData, DDRPHY_COMP_CR_DLLCOMP_VCDLCTRL_REG);
  DllCompVcdlCtrl.Bits.dllcomp_cmn_vcdlcben = CalSave->CbEn;
  MrcWriteCR (MrcData, DDRPHY_COMP_CR_DLLCOMP_VCDLCTRL_REG, DllCompVcdlCtrl.Data);

  // Manually distribute CbEn
  MrcCbEnDistribute (MrcData);

  return Status;
}

/**
  This routine setups up the Comp DCC mode.

  @param[in, out]  MrcData            - Pointer to MRC global data.
  @param[in]       Enable             - Enable or Disable the DCC mode
  @param[in]       TotalSampleCounts  - Total Sample Counts
  @param[in, out]  SavedParams        - SavedParams are used to save the register value for restoration.

  @retval None
**/
VOID
SetupDccAccComp (
  IN OUT MrcParameters *const MrcData,
  IN BOOLEAN           Enable,
  IN UINT32            TotalSampleCounts,
  IN OUT UINT32        SavedParams[MAX_DCC_SAVED_PARAMETERS]
  )
{
  DDRPHY_COMP_CR_DCCCTL0_STRUCT          CompDccCtl0;
  DDRPHY_COMP_CR_DLLCOMP_DCCCTRL_STRUCT  CompDllCompDccCtl;
  DDRPHY_COMP_CR_DCCCTL4_STRUCT          CompDccCtl4;

  if (Enable) {
    // enable ring oscillator
    CompDccCtl0.Data = MrcReadCR (MrcData, DDRPHY_COMP_CR_DCCCTL0_REG);
    CompDccCtl0.Bits.dcdrovcoen = 1;
    MrcWriteCR (MrcData, DDRPHY_COMP_CR_DCCCTL0_REG, CompDccCtl0.Data);

    // force DCD Enable
    CompDllCompDccCtl.Data = MrcReadCR (MrcData, DDRPHY_COMP_CR_DLLCOMP_DCCCTRL_REG);
    CompDllCompDccCtl.Bits.enabledcd = 1;
    MrcWriteCR (MrcData, DDRPHY_COMP_CR_DLLCOMP_DCCCTRL_REG, CompDllCompDccCtl.Data);

    // put DCC into manual override mode to allow direct control of accumulator mux
    CompDccCtl4.Data = MrcReadCR (MrcData, DDRPHY_COMP_CR_DCCCTL4_REG);
    CompDccCtl4.Bits.dcc_bits_en = 0;
    MrcWriteCR (MrcData, DDRPHY_COMP_CR_DCCCTL4_REG, CompDccCtl4.Data);

    // save parameters, not used after this step, but save and restore for consistency
    CompDccCtl0.Data = MrcReadCR (MrcData, DDRPHY_COMP_CR_DCCCTL0_REG);
    SavedParams [COMP_TOTAL_SAMPLE_COUNTS_INDEX] = CompDccCtl0.Bits.totalsamplecounts;
    CompDccCtl0.Bits.totalsamplecounts = TotalSampleCounts;
    MrcWriteCR (MrcData, DDRPHY_COMP_CR_DCCCTL0_REG, CompDccCtl0.Data);
  } else {
    // Disable ring oscillator
    CompDccCtl0.Data = MrcReadCR (MrcData, DDRPHY_COMP_CR_DCCCTL0_REG);
    CompDccCtl0.Bits.dcdrovcoen = 0;
    MrcWriteCR (MrcData, DDRPHY_COMP_CR_DCCCTL0_REG, CompDccCtl0.Data);

    // force DCD disable
    CompDllCompDccCtl.Data = MrcReadCR (MrcData, DDRPHY_COMP_CR_DLLCOMP_DCCCTRL_REG);
    CompDllCompDccCtl.Bits.enabledcd = 0;
    MrcWriteCR (MrcData, DDRPHY_COMP_CR_DLLCOMP_DCCCTRL_REG, CompDllCompDccCtl.Data);

    // put DCC into manual override mode to allow direct control of accumulator mux
    CompDccCtl4.Data = MrcReadCR (MrcData, DDRPHY_COMP_CR_DCCCTL4_REG);
    CompDccCtl4.Bits.dcc_bits_en = 0;
    MrcWriteCR (MrcData, DDRPHY_COMP_CR_DCCCTL4_REG, CompDccCtl4.Data);

    // Restore totalsamplecounts
    CompDccCtl0.Data = MrcReadCR (MrcData, DDRPHY_COMP_CR_DCCCTL0_REG);
    CompDccCtl0.Bits.totalsamplecounts = SavedParams [COMP_TOTAL_SAMPLE_COUNTS_INDEX];
    MrcWriteCR (MrcData, DDRPHY_COMP_CR_DCCCTL0_REG, CompDccCtl0.Data);
  }
}

/**
  This routine runs COMP DCC.

  @param[in, out]  MrcData              - Pointer to MRC global data.
  @param[in]  TotalSampleCounts         - Total Sample Counts.
  @param[out] DutyCycle                 - Duty Cycle.
  @param[out] DcdSampleCount            - Dcd Sample Count.

  @retval None
**/
VOID
RunDccAccComp (
  IN MrcParameters    *const MrcData,
  IN UINT32           TotalSampleCounts,
  OUT UINT32          *DutyCycle,
  OUT UINT32          *DcdSampleCount
  )
{

  UINT32                    Data32;
  UINT32                    SampleCount;
  DDRPHY_COMP_CR_DCCCTL2_STRUCT           CompDccCtl2;
  DDRPHY_COMP_CR_COMP_DCCSTATUS1_STRUCT   CompDccStatus1;

#ifdef DCC_DEBUG_PRINT
  MrcDebug                  *Debug;
  MrcOutput                 *Outputs;
  UINT32                    DataInt;
  Outputs = &MrcData->Outputs;
  Debug   = &Outputs->Debug;
#endif

  CompDccCtl2.Data = MrcReadCR (MrcData, DDRPHY_COMP_CR_DCCCTL2_REG);
  CompDccCtl2.Bits.dcdsamplecountrst_b = 0;
  MrcWriteCR (MrcData, DDRPHY_COMP_CR_DCCCTL2_REG, CompDccCtl2.Data);

  CompDccCtl2.Data = MrcReadCR (MrcData, DDRPHY_COMP_CR_DCCCTL2_REG);
  CompDccCtl2.Bits.dcdsamplecountrst_b = 1;
  MrcWriteCR (MrcData, DDRPHY_COMP_CR_DCCCTL2_REG, CompDccCtl2.Data);

  CompDccCtl2.Data = MrcReadCR (MrcData, DDRPHY_COMP_CR_DCCCTL2_REG);
  CompDccCtl2.Bits.dcdsamplecountstart = 1;
  MrcWriteCR (MrcData, DDRPHY_COMP_CR_DCCCTL2_REG, CompDccCtl2.Data);

  MrcWait(MrcData, 1000 * MRC_TIMER_1US); // wait for TotalSampleCounts number of roclk cycles. 1000 us is enough.
  //
  //clear dcdsamplecountstart so that accumulator can be rerun again later
  //
  CompDccCtl2.Data = MrcReadCR (MrcData, DDRPHY_COMP_CR_DCCCTL2_REG);
  CompDccCtl2.Bits.dcdsamplecountstart = 0;
  MrcWriteCR (MrcData, DDRPHY_COMP_CR_DCCCTL2_REG, CompDccCtl2.Data);

  CompDccStatus1.Data = MrcReadCR (MrcData, DDRPHY_COMP_CR_COMP_DCCSTATUS1_REG);
  SampleCount = CompDccStatus1.Bits.samplecountfall;
  SampleCount += CompDccStatus1.Bits.samplecountrise;
  *DcdSampleCount = SampleCount;
  Data32 = 10000 * SampleCount;
  Data32 = UDIVIDEROUND (Data32, 2 * TotalSampleCounts);  // Precision of 2 decimal points
  *DutyCycle = Data32;
#ifdef DCC_DEBUG_PRINT
  DataInt = Data32 / 100;
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Measured duty cycle: COMP: %u.%02u\n", DataInt, Data32 - 100 * DataInt);
#endif

  return;
}

/**
  This routine returns right DCC bits mask for DCD_GROUP and FUB (DATA/CCC/COMP)

  @param[in, out] MrcData   - Include all MRC global data.
  @param[in]      DcdGroup  - DCD_GROUP type.
  @param[in]      FubMask   - Setup DCC in this FUB (DATA/CCC/COMP)

  @retval DCC Index Mask for DCD_GROUP/Fub (DATA/CCC/COMP).
**/
UINT16
MrcGetDccIndexMask (
  IN OUT MrcParameters* const MrcData,
  IN const UINT32             DcdGroup,
  IN const DccFubMask         FubMask
  )
{
  MrcInput           *Inputs;
  MrcOutput          *Outputs;
  UINT16             DccIndexMask;
  BOOLEAN            Lpddr4;
  BOOLEAN            Lpddr5;

  Inputs       = &MrcData->Inputs;
  Outputs      = &MrcData->Outputs;
  Lpddr4       = (Outputs->DdrType == MRC_DDR_TYPE_LPDDR4);
  Lpddr5       = (Outputs->DdrType == MRC_DDR_TYPE_LPDDR5);
  DccIndexMask = 0;

  if (FubMask & DccFubData) {
    if ((DcdGroup == DCD_GROUP_REFCLK) || (DcdGroup == DCD_GROUP_VCDL)) {
      DccIndexMask = 0x1;
    } else {                   // DCD_GROUP_SRZ , DCD_GROUP_SRZ_PH90, DCD_GROUP_SRZ_QUAD
      DccIndexMask = (Inputs->Q0 || Inputs->G0) ? 0x3FC : 0x3FE;
    }
  } else if (FubMask & DccFubCcc) {
    if ((DcdGroup == DCD_GROUP_REFCLK) || (DcdGroup == DCD_GROUP_VCDL)) {
      DccIndexMask = 0x1;
    } else if (DcdGroup == DCD_GROUP_SRZ) {
      // UlxUlt: Ddr4/Ddr5 - 0x3CFE, Lp5 - 0x3FFE, Lp4 - 0x3FEE
      if (Inputs->DtHalo) {
        DccIndexMask = 0xFFFE;
      } else {
        DccIndexMask = Lpddr5 ? 0x3FFE :(Lpddr4 ? 0x3FEE : 0x3CFE);
      }
    } else {                   // DCD_GROUP_SRZ_PH90, DCD_GROUP_SRZ_QUAD
      // UlxUlt: Lp5 - 0x300, Ddr5/Lp4/Ddr4 - 0xC0
      DccIndexMask = (Inputs->DtHalo) ? 0x3C0 :(Lpddr5 ? 0x300 : 0xC0);
    }
  } else {
    /*
    if ((DcdGroup == DCD_GROUP_REFCLK) || (DcdGroup == DCD_GROUP_VCDL)) {
      DccIndexMask = 0x1;
    } else if (DcdGroup == DCD_GROUP_SRZ) {
      DccIndexMask = 0x2;
    } else {                   // DCD_GROUP_SRZ_PH90, DCD_GROUP_SRZ_QUAD
      DccIndexMask = 0x0;
    } */
    DccIndexMask = 0x0;
  }
  return DccIndexMask;
}

/**
  Update DCC code for Comp DCD groups during DCC code calibration

  @param[in, out] MrcData            - Include all MRC global data.
  @param[in]      DcdGroup           - DCD_GROUP type.
  @param[in]      CompCodeSearchData - COMP code search data structure.
  @param[in]      CompIndexDone      - Calibrartion staus of current Index.

  @retval None
**/
VOID
MrcSetDccCodeComp (
  IN OUT MrcParameters       *const MrcData,
  IN UINT32                  DcdGroup,
  IN MrcDccCodeSearchState   CompCodeSearchData,
  IN BOOLEAN                 CompIndexDone
  )
{
  DDRPHY_COMP_CR_DCCCTL4_STRUCT            DccCtl4;
  DDRPHY_COMP_CR_DLLCOMP_VCDLCTRL_STRUCT   DllCompVcdlCtrl;

  if ((CompIndexDone == FALSE) && (CompCodeSearchData.Done == FALSE)) {
    if (DcdGroup == DCD_GROUP_REFCLK) {
      DccCtl4.Data = MrcReadCR (MrcData, DDRPHY_COMP_CR_DCCCTL4_REG);
      DccCtl4.Bits.dcccodeph0_index0 = CompCodeSearchData.Code;
      MrcWriteCR (MrcData, DDRPHY_COMP_CR_DCCCTL4_REG, DccCtl4.Data);
    } else if (DcdGroup == DCD_GROUP_VCDL) {
      DllCompVcdlCtrl.Data = MrcReadCR (MrcData, DDRPHY_COMP_CR_DLLCOMP_VCDLCTRL_REG);
      DllCompVcdlCtrl.Bits.dllcomp_cmn_vcdldcccode = CompCodeSearchData.Code;
      MrcWriteCR (MrcData, DDRPHY_COMP_CR_DLLCOMP_VCDLCTRL_REG, DllCompVcdlCtrl.Data);
    } else {
      // @todo_adl - Update for DCD_GROUPs
    }
  }
}

/**
  Update DCC code for CCC DCD groups during DCC code calibration

  @param[in, out] MrcData            - Include all MRC global data.
  @param[in]      DcdGroup           - DCD_GROUP type.
  @param[in]      DccIndex           - DCC Lane Index.
  @param[in]      CccCodeSearchData  - Array of current DCC code search state data of all CCC instances.
  @param[in]      CccIndexDone       - Array of current calibration status of all CCC instances.

  @retval None
**/
VOID
MrcSetDccCodeCcc (
  IN OUT MrcParameters       *const MrcData,
  IN UINT32                  DcdGroup,
  IN UINT8                   DccIndex,
  IN MrcDccCodeSearchState   CccCodeSearchData[MRC_NUM_CCC_INSTANCES],
  IN BOOLEAN                 CccIndexDone[MRC_NUM_CCC_INSTANCES]
  )
{
  UINT8         CccInst;
  UINT32        Controller;
  UINT32        Channel;
  INT64         DccCode;
  INT64         TxPbdOffset;
  INT64         TxPbdSign;

  for (CccInst = 0; CccInst < MRC_NUM_CCC_INSTANCES; CccInst++) {
    DdrIoTranslateCccToMcChannel (MrcData, CccInst, &Controller, &Channel);
    if (!MrcChannelExist (MrcData, Controller, Channel)) {
      continue;
    }
    // Update code only if DCC index is not disabled and final DCC code is not converged within limits.
    if ((CccIndexDone[CccInst] == FALSE) && (CccCodeSearchData[CccInst].Done == FALSE)) {
      DccCode = CccCodeSearchData[CccInst].Code;
      switch (DcdGroup) {
        case DCD_GROUP_REFCLK:
        case DCD_GROUP_SRZ:
          MrcGetSetCccInstLane (MrcData, CccInst, DccIndex, GsmIocDccCodePh0IndexCcc, WriteNoCache, &DccCode);
          break;

        case DCD_GROUP_SRZ_PH90:
          MrcGetSetCccInstLane (MrcData, CccInst, DccIndex, GsmIocDccCodePh90IndexCcc, WriteNoCache, &DccCode);
          break;

        case DCD_GROUP_SRZ_QUAD:
          if (DccCode >= 64) {
            TxPbdOffset = DccCode - 64;
            TxPbdSign = 1;
          } else {
            TxPbdOffset = 63 - DccCode;
            TxPbdSign = 0;
          }
          MrcGetSetCccInstLane (MrcData, CccInst, DccIndex, GsmIocTxpbddOffsetCcc, WriteNoCache, &TxPbdOffset);
          MrcGetSetCccInstLane (MrcData, CccInst, DccIndex, GsmIocTxpbdPh90incrCcc, WriteNoCache, &TxPbdSign);
          break;

        case DCD_GROUP_VCDL:
          MrcGetSetCccInstLane (MrcData, CccInst, DccIndex, GsmIocMdllCmnVcdlDccCodeCcc, WriteNoCache, &DccCode);
          break;

        default:
          break;
      }
    }
  } //CccInst

}

/**
  Update DCC code for DATA DCD groups during DCC code calibration

  @param[in, out] MrcData            - Include all MRC global data.
  @param[in]      DcdGroup           - DCD_GROUP type.
  @param[in]      DccIndex           - DCC Lane Index.
  @param[in]      DataCodeSearchData - Array of current DCC code search state data of all DATA instances.
  @param[in]      DataIndexDone      - Array of current calibration status of all DATA instances.

  @retval None
**/
VOID
MrcSetDccCodeData (
  IN OUT MrcParameters      *const MrcData,
  IN UINT32                 DcdGroup,
  IN UINT8                  DccIndex,
  IN MrcDccCodeSearchState  DataCodeSearchData[MAX_CONTROLLER][MAX_CHANNEL][MAX_SDRAM_IN_DIMM],
  IN BOOLEAN                DataIndexDone[MAX_CONTROLLER][MAX_CHANNEL][MAX_SDRAM_IN_DIMM]
  )
{
  UINT8           Controller;
  UINT8           Channel;
  UINT8           Strobe;
  INT64           DccCode;
  INT64           TxPbdOffset;
  INT64           TxPbdSign;
  MrcOutput       *Outputs;

  Outputs = &MrcData->Outputs;

  for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
    for (Channel = 0; Channel < Outputs->MaxChannels; Channel++) {
      if (!MrcChannelExist (MrcData, Controller, Channel)) {
        continue;
      }
      for (Strobe = 0; Strobe < Outputs->SdramCount; Strobe++) {
        // Update code only if DCC index is not disabled and final DCC code is not converged within limits.
        if ((DataIndexDone[Controller][Channel][Strobe] == FALSE) &&
             (DataCodeSearchData[Controller][Channel][Strobe].Done == FALSE)) {
          DccCode = DataCodeSearchData[Controller][Channel][Strobe].Code;
          switch (DcdGroup) {
            case DCD_GROUP_REFCLK:
            case DCD_GROUP_SRZ:
              MrcGetSetBit (MrcData, Controller, Channel, MRC_IGNORE_ARG, Strobe, DccIndex, GsmIocDccCodePh0IndexData, WriteNoCache, &DccCode);
              break;

            case DCD_GROUP_SRZ_PH90:
              MrcGetSetBit (MrcData, Controller, Channel, MRC_IGNORE_ARG, Strobe, DccIndex, GsmIocDccCodePh90IndexData, WriteNoCache, &DccCode);
              break;

            case DCD_GROUP_SRZ_QUAD:
              if (DccCode >= 64) {
                TxPbdOffset = DccCode - 64;
                TxPbdSign = 1;
              } else {
                TxPbdOffset = 63 - DccCode;
                TxPbdSign = 0;
              }
              MrcGetSetBit (MrcData, Controller, Channel, MRC_IGNORE_ARG, Strobe, DccIndex, GsmIocTxpbddOffsetData, WriteNoCache, &TxPbdOffset);
              MrcGetSetBit (MrcData, Controller, Channel, MRC_IGNORE_ARG, Strobe, DccIndex, GsmIocTxpbdPh90incrData, WriteNoCache, &TxPbdSign);
              break;

            case DCD_GROUP_VCDL:
              MrcGetSetBit (MrcData, Controller, Channel, MRC_IGNORE_ARG, Strobe, DccIndex, GsmIocMdllCmnVcdlDccCodeData, WriteNoCache, &DccCode);
              break;

            default:
              break;
          }
        }
      }  // Strobe
    }  // Channel
  }  // Controller
}

/**
  Find next DCC code to be calibrated based on current DCC code

  @param[in, out]   MrcData            - Include all MRC global data.
  @param[in]        DcdGroup           - DCD_GROUP type.
  @param[in, out]   DccCodeSearchData  - Pointer to current DccCodeSearchData.

  @retval None
**/
VOID
MrcCheckAdjDccCode (
  IN OUT MrcParameters*         const MrcData,
  IN const UINT32               DcdGroup,
  IN OUT MrcDccCodeSearchState  *DccCodeSearchData
  )
{
  UINT16       DccMinCode;
  UINT16       DccMaxCode;
  UINT16       DccCurrCode;
  UINT16       DccCodeNext;
  UINT8        DccStepSize;
  DCC_STATUS   DccStatus;
  BOOLEAN      Saturate;

  DccMinCode  = 0;
  DccMaxCode  = (DcdGroup == DCD_GROUP_VCDL) ? 15 : ((DcdGroup == DCD_GROUP_SRZ_QUAD) ? 127 : 255);
  DccCurrCode = DccCodeSearchData->CodePrev;
  DccStepSize = DccCodeSearchData->StepSize;
  DccStatus   = DccCodeSearchData->DccCurStatus;
  Saturate    = FALSE;
  DccCodeNext = DccCurrCode;

  if (DccStatus == DccHigh) {
    if (DccCurrCode >= (DccMinCode + DccStepSize)) {
      DccCodeNext = DccCurrCode - DccStepSize;
    } else if (DccCurrCode > DccMinCode) {
      DccCodeNext = DccMinCode;
    } else {
      Saturate = TRUE;
    }
  } else if (DccStatus == DccLow) {
    if (DccCurrCode <= (DccMaxCode - DccStepSize)) {
      DccCodeNext = DccCurrCode + DccStepSize;
    } else if (DccCurrCode < DccMaxCode) {
      DccCodeNext = DccMaxCode;
    } else {
      Saturate = TRUE;
    }
  }
  DccCodeSearchData->Code      = DccCodeNext;
  DccCodeSearchData->Saturated = Saturate;
}

/**
  Parse DCC code search data and update the search state

  @param[in, out]   MrcData            - Include all MRC global data.
  @param[in]        DcdGroup           - DCD_GROUP type.
  @param[in, out]   DccCodeSearchData  - Pointer to current DccCodeSearchData.

  @retval None
**/
VOID
MrcParseDccCodeSearchData (
  IN OUT MrcParameters*      const MrcData,
  IN const UINT32            DcdGroup,
  IN MrcDccCodeSearchState  *DccCodeSearchData
  )
{
  INT32              FbValueDiffPrev;
  INT32              FbValueDiff;

  DccCodeSearchData->CodePrev      = DccCodeSearchData->Code;
  DccCodeSearchData->DccPrevStatus = DccCodeSearchData->DccCurStatus;

  if (DccCodeSearchData->FbValue > DCC_TARGET) {              // overall duty cycle is too high:
    DccCodeSearchData->DccCurStatus  = DccHigh;
    MrcCheckAdjDccCode (MrcData, DcdGroup, DccCodeSearchData);
  } else if (DccCodeSearchData->FbValue < DCC_TARGET) {      // overall duty cycle is too low:
    DccCodeSearchData->DccCurStatus  = DccLow;
    MrcCheckAdjDccCode (MrcData, DcdGroup, DccCodeSearchData);
  } else {                                                   // perfectly centered duty cycle
    DccCodeSearchData->DccCurStatus = DccStatusAlign;
    DccCodeSearchData->Done         = TRUE;
  }

  if ((DccCodeSearchData->Saturated == FALSE) && (DccCodeSearchData->Done == FALSE)) {                           // next correction step is possible
    if (((DccCodeSearchData->DccCurStatus == DccLow) && (DccCodeSearchData->DccPrevStatus == DccHigh)) ||
        ((DccCodeSearchData->DccCurStatus == DccHigh) && (DccCodeSearchData->DccPrevStatus == DccLow))) {        // dither condition
      DccCodeSearchData->DitherCount += 1;
      DccCodeSearchData->StepSize = DCC_STEP_SIZE_FINE;
      if (DccCodeSearchData->DitherCount >= DCC_MAX_DITHER_ALLOWED) {
        DccCodeSearchData->Done         = TRUE;
        DccCodeSearchData->DccCurStatus = DccStatusDither;
        FbValueDiffPrev = DccCodeSearchData->FbValuePrev - DCC_TARGET;
        FbValueDiff     = DccCodeSearchData->FbValue - DCC_TARGET;
        if (ABS (FbValueDiffPrev) < ABS (FbValueDiff)) {
          DccCodeSearchData->FbValue =  DccCodeSearchData->FbValuePrev;
          DccCodeSearchData->Code    =  DccCodeSearchData->CodePrev;
        }
      }
    }
  } else if (DccCodeSearchData->Saturated == TRUE) {
    DccCodeSearchData->Done = TRUE;
  }

  if ((DccCodeSearchData->DccCurStatus == DccHigh) && (DccCodeSearchData->DitherCount > 0)) {
    DccCodeSearchData->DccStepDir = DccStepDirFineDec;
  } else if ((DccCodeSearchData->DccCurStatus == DccLow) && (DccCodeSearchData->DitherCount > 0)) {
    DccCodeSearchData->DccStepDir = DccStepDirFineInc;
  } else if ((DccCodeSearchData->DccCurStatus == DccHigh) && (DccCodeSearchData->DitherCount <= 0)) {
    DccCodeSearchData->DccStepDir = DccStepDirCoarseDec;
  } else if ((DccCodeSearchData->DccCurStatus == DccLow) && (DccCodeSearchData->DitherCount <= 0)) {
    DccCodeSearchData->DccStepDir = DccStepDirCoarseInc;
  } else if (DccCodeSearchData->DccCurStatus == DccStatusDither) {
    DccCodeSearchData->DccStepDir = DccStepDirDither;
  } else if (DccCodeSearchData->DccCurStatus == DccStatusAlign) {
    DccCodeSearchData->DccStepDir = DccStepDirAlign;
  } else {
    DccCodeSearchData->DccStepDir = DccStepDirNone;
  }

  if ((DccCodeSearchData->Saturated == TRUE) || (DccCodeSearchData->Done == TRUE)) {  // search is done or saturated
    if ((DccCodeSearchData->FbValue < SW_FSM_DCC_THRESHOLD_HI) && (DccCodeSearchData->FbValue > SW_FSM_DCC_THRESHOLD_LO)) {
      DccCodeSearchData->DccResultPass = TRUE;
    } else {
      DccCodeSearchData->DccResultPass = FALSE;
    }
  }
}

/**
  Init DccCodeSearchData to defaults

  @param[in, out]   MrcData            - Include all MRC global data.
  @param[in]        DcdGroup           - DCD_GROUP type.
  @param[in, out]   DccCodeSearchData  - Pointer to current DccCodeSearchData.

  @retval None
**/
VOID
MrcDccCodeSearchDataInit (
  IN OUT MrcParameters*  const MrcData,
  IN const UINT32              DcdGroup,
  IN MrcDccCodeSearchState     *DccCodeSearchData
  )
{
  if (DcdGroup == DCD_GROUP_VCDL) {
    DccCodeSearchData->Code      = DCC_INIT_CODE_VCDL;
    DccCodeSearchData->CodePrev  = DCC_INIT_CODE_VCDL;
  } else if (DcdGroup == DCD_GROUP_SRZ_QUAD) {
    DccCodeSearchData->Code      = DCC_INIT_CODE_SRZ_QUAD;
    DccCodeSearchData->CodePrev  = DCC_INIT_CODE_SRZ_QUAD;
  } else {
    DccCodeSearchData->Code      = DCC_INIT_CODE;
    DccCodeSearchData->CodePrev  = DCC_INIT_CODE;
  }
  DccCodeSearchData->FbValue       = 0;
  DccCodeSearchData->FbValuePrev   = 0;
  DccCodeSearchData->DitherCount   = 0;
  DccCodeSearchData->DccPrevStatus = DccInit;
  DccCodeSearchData->DccCurStatus  = DccInit;
  DccCodeSearchData->Saturated     = FALSE;
  DccCodeSearchData->Done          = FALSE;
  DccCodeSearchData->StepSize      = (DcdGroup == DCD_GROUP_VCDL) ? DCC_STEP_SIZE_COARSE_VCDL : DCC_STEP_SIZE_COARSE;
  DccCodeSearchData->DccStepDir    = DccStepDirNone;
}

/**
  Check current duty cycle is within limits and update DutyCycle group values.

  @param[in, out]   MrcData            - Include all MRC global data.
  @param[in]        DcdGroup           - DCD_GROUP type.
  @param[in]        DccIndex           - DCC Lane Index.
  @param[in]        PartitionStr       - Partition Instance string.
  @param[in]        DutyCycle          - Duty cycle value to compare.
  @param[in, out]   DutyCycleGrpData   - Pointer to current DutyCycleGrpData.

  @retval           Duty Cycle limit status
**/
BOOLEAN
MrcCheckMinMaxSpec (
  IN OUT MrcParameters*    const MrcData,
  IN const UINT32          DcdGroup,
  IN UINT8                 DccIndex,
  IN CHAR8                 *PartitionStr,
  IN UINT32                DutyCycle,
  IN OUT DutyCycleGrpData  *DutyCycleGrpData
  )
{
  const MRC_FUNCTION  *MrcCall;
  MrcInput            *Inputs;
  BOOLEAN             DcLimitFail;

  Inputs      = &MrcData->Inputs;
  MrcCall     = Inputs->Call.Func;
  DcLimitFail = FALSE;

   // Update DutyCycleMin and other info if current duty cycle is less than previous one
  if (DutyCycle <= DutyCycleGrpData->DutyCycleMin) {
    DutyCycleGrpData->DutyCycleMin = DutyCycle;
#ifdef MRC_DEBUG_PRINT
    MrcCall->MrcCopyMem ((UINT8 *) DutyCycleGrpData->PartitionMin, (UINT8 *) PartitionStr, MrcStrLen(PartitionStr));
#endif
    DutyCycleGrpData->DccIndexMin  = DccIndex;
    DutyCycleGrpData->DcdGroupMin  = DcdGroup;
  }

  // Update DutyCycleMax and other info if current duty cycle is more than previous one
  if (DutyCycle >= DutyCycleGrpData->DutyCycleMax) {
    DutyCycleGrpData->DutyCycleMax = DutyCycle;
#ifdef MRC_DEBUG_PRINT
    MrcCall->MrcCopyMem ((UINT8 *) DutyCycleGrpData->PartitionMax, (UINT8 *) PartitionStr, MrcStrLen(PartitionStr));
#endif
    DutyCycleGrpData->DccIndexMax  = DccIndex;
    DutyCycleGrpData->DcdGroupMax  = DcdGroup;
  }

  if (DutyCycleGrpData->DutyCycleMin < DutyCycleGrpData->DutyCycleMinLimit) {   // Compare against Min Spec value
    DutyCycleGrpData->DutyCycleGood = FALSE;
  }

  if (DutyCycleGrpData->DutyCycleMax > DutyCycleGrpData->DutyCycleMaxLimit) {   // Compare against Max Spec value
    DutyCycleGrpData->DutyCycleGood = FALSE;
  }

  // Check Duty cycle is within SpecMin and SpecMax
  if ((DutyCycle < DutyCycleGrpData->DutyCycleMinLimit) || (DutyCycle > DutyCycleGrpData->DutyCycleMaxLimit)) {
    DcLimitFail = TRUE;
  }

  return DcLimitFail;
}

/**
  Update duty cycle groups data

  @param[in, out]   MrcData            - Include all MRC global data.
  @param[in]        DcdGroup           - DCD_GROUP type.
  @param[in]        FubMask            - DccFubMask.
  @param[in]        DccIndex           - DCC Lane Index.
  @param[in]        PartitionStr       - Partition Instance string.
  @param[in]        DutyCycle          - Duty cycle value to compare.
  @param[in, out]   DutyCycleGrpData   - Pointer to DutyCycleGrpData of all groups.

  @retval           Duty Cycle limit status
**/
BOOLEAN
MrcFindDcLimits (
  IN OUT MrcParameters* const MrcData,
  IN const UINT32             DcdGroup,
  IN const DccFubMask         FubMask,
  IN UINT8                    DccIndex,
  IN CHAR8                    *PartitionStr,
  IN UINT32                   DutyCycle,
  IN OUT DutyCycleGrpData     DutyCycleGrpData[DutyCycleMaxGrp]
  )
{
  BOOLEAN       DcLimitFail;
  DcLimitFail = FALSE;

  // Update data based on DCD_GROUP, DccFub, DCC Lane index and partition instance.

  if (FubMask & DccFubComp) {
    if ((DccIndex == 0) && (DcdGroup == DCD_GROUP_REFCLK)) {
      DcLimitFail = MrcCheckMinMaxSpec (MrcData, DcdGroup, DccIndex, PartitionStr, DutyCycle, &DutyCycleGrpData[DllRefClk]);
    } else if ((DccIndex == 0) && (DcdGroup == DCD_GROUP_VCDL)) {
      DcLimitFail = MrcCheckMinMaxSpec (MrcData, DcdGroup, DccIndex, PartitionStr, DutyCycle, &DutyCycleGrpData[DllVcdl]);
    }
    MrcCheckMinMaxSpec (MrcData, DcdGroup, DccIndex, PartitionStr, DutyCycle, &DutyCycleGrpData[CompDc]);
  } else if (FubMask & DccFubCcc) {
    if ((DccIndex == 0) && (DcdGroup == DCD_GROUP_REFCLK)) {
      DcLimitFail = MrcCheckMinMaxSpec (MrcData, DcdGroup, DccIndex, PartitionStr, DutyCycle, &DutyCycleGrpData[DllRefClk]);
    } else if ((DccIndex == 0) && (DcdGroup == DCD_GROUP_VCDL)) {
      DcLimitFail = MrcCheckMinMaxSpec (MrcData, DcdGroup, DccIndex, PartitionStr, DutyCycle, &DutyCycleGrpData[DllVcdl]);
    } else if ((DccIndex == 6) || (DccIndex == 7)) {
      DcLimitFail = MrcCheckMinMaxSpec (MrcData, DcdGroup, DccIndex, PartitionStr, DutyCycle, &DutyCycleGrpData[Ck0]);
    } else if ((DccIndex == 8) || (DccIndex == 9)) {
      DcLimitFail = MrcCheckMinMaxSpec (MrcData, DcdGroup, DccIndex, PartitionStr, DutyCycle, &DutyCycleGrpData[CkWck1]);
    } else {
      DcLimitFail = MrcCheckMinMaxSpec (MrcData, DcdGroup, DccIndex, PartitionStr, DutyCycle, &DutyCycleGrpData[NonCk]);
    }
    MrcCheckMinMaxSpec (MrcData, DcdGroup, DccIndex, PartitionStr, DutyCycle, &DutyCycleGrpData[CccDc]);
  } else if (FubMask & DccFubData) {
    if ((DccIndex == 0) && (DcdGroup == DCD_GROUP_REFCLK)) {
      DcLimitFail = MrcCheckMinMaxSpec (MrcData, DcdGroup, DccIndex, PartitionStr, DutyCycle, &DutyCycleGrpData[DllRefClk]);
    } else if ((DccIndex == 0) && (DcdGroup == DCD_GROUP_VCDL)) {
      DcLimitFail = MrcCheckMinMaxSpec (MrcData, DcdGroup, DccIndex, PartitionStr, DutyCycle, &DutyCycleGrpData[DllVcdl]);
    } else if (DccIndex == 1) {
      DcLimitFail = MrcCheckMinMaxSpec (MrcData, DcdGroup, DccIndex, PartitionStr, DutyCycle, &DutyCycleGrpData[DqsDc]);
    } else {
      DcLimitFail = MrcCheckMinMaxSpec (MrcData, DcdGroup, DccIndex, PartitionStr, DutyCycle, &DutyCycleGrpData[DqDc]);
    }
    MrcCheckMinMaxSpec (MrcData, DcdGroup, DccIndex, PartitionStr, DutyCycle, &DutyCycleGrpData[DataDc]);
  }

  return DcLimitFail;
}

/**
  Init DutyCycleGrpData to defaults

  @param[in, out]   MrcData            - Include all MRC global data.
  @param[in, out]   DutyCycleGrpData   - Pointer to DutyCycleGrpData.

  @retval None
**/
VOID
MrcDutyCycleGrpDataInit (
  IN OUT MrcParameters* const  MrcData,
  IN OUT DutyCycleGrpData      DutyCycleGrpData[DutyCycleMaxGrp]
  )
{
  const MRC_FUNCTION  *MrcCall;
  MrcInput            *Inputs;
  UINT8               Index;
#ifdef MRC_DEBUG_PRINT
  CHAR8               PartitionStrNone[] = "None";
#endif

  Inputs  = &MrcData->Inputs;
  MrcCall = Inputs->Call.Func;

  for (Index = 0; Index < DutyCycleMaxGrp; Index++) {
    DutyCycleGrpData[Index].DutyCycleMin = 10000;
    DutyCycleGrpData[Index].DutyCycleMax = 0;
    if ((Index == DllRefClk) || (Index == DllVcdl)) {
      DutyCycleGrpData[Index].DutyCycleMinLimit = 4940;
      DutyCycleGrpData[Index].DutyCycleMaxLimit = 5060;
    } else if ((Index == NonCk) || (Index == CccDc)) {
      DutyCycleGrpData[Index].DutyCycleMinLimit = 4940;
      DutyCycleGrpData[Index].DutyCycleMaxLimit = 5060;
    } else {
      DutyCycleGrpData[Index].DutyCycleMinLimit = 4940;
      DutyCycleGrpData[Index].DutyCycleMaxLimit = 5060;
    }
    DutyCycleGrpData[Index].DutyCycleGood     = TRUE;
#ifdef MRC_DEBUG_PRINT
    MrcCall->MrcCopyMem ((UINT8 *) (DutyCycleGrpData[Index].PartitionMin), (UINT8 *) (PartitionStrNone), MrcStrLen(PartitionStrNone));
    MrcCall->MrcCopyMem ((UINT8 *) (DutyCycleGrpData[Index].PartitionMax), (UINT8 *) (PartitionStrNone), MrcStrLen(PartitionStrNone));
#endif
    DutyCycleGrpData[Index].DccIndexMin       = 255;
    DutyCycleGrpData[Index].DccIndexMax       = 255;
    DutyCycleGrpData[Index].DcdGroupMin       = DCD_GROUP_MAX;
    DutyCycleGrpData[Index].DcdGroupMax       = DCD_GROUP_MAX;
  }
}

/**
  Measure duty cycles of all instances for a DCD Group/Lane combination.

  @param[in, out]   MrcData            - Include all MRC global data.
  @param[in]        DcdGroup           - DCD_GROUP type.
  @param[in]        FubMask            - DccFubMask.
  @param[in]        DccIndex           - DCC Lane Index.
  @param[in, out]   CompDutyCycle      - Pointer to Comp duty cycle.
  @param[in, out]   CccDutyCycle       - Array to duty cycle values of all CCC instances.
  @param[in, out]   DutyCycleGrpData   - Array to duty cycle values of all Data instances.

  @retval None
**/
VOID
MrcGetDccIndexDutyCycle (
  IN OUT MrcParameters* const MrcData,
  IN UINT8                    DcdGroup,
  IN DccFubMask               FubMask,
  IN UINT16                   DccIndex,
  IN OUT UINT32               *CompDutyCycle,
  IN OUT UINT32               CccDutyCycle[MRC_NUM_CCC_INSTANCES],
  IN OUT UINT32               DataDutyCycle[MAX_CONTROLLER][MAX_CHANNEL][MAX_SDRAM_IN_DIMM]
  )
{
  const MRC_FUNCTION  *MrcCall;
  MrcInput            *Inputs;
  INT64               GetSetVal;
  UINT32              TotalSampleCounts;
  UINT32              SavedParams[MAX_DCC_SAVED_PARAMETERS];
  UINT32              CccSampleCount[MRC_NUM_CCC_INSTANCES];
  UINT32              DataSampleCount[MAX_CONTROLLER][MAX_CHANNEL][MAX_SDRAM_IN_DIMM];
  UINT32              CompSampleCount;

  DDRPHY_COMP_CR_DCCCTL6_STRUCT    CompDccCtl6;

  Inputs  = &MrcData->Inputs;
  MrcCall = Inputs->Call.Func;

  MrcCall->MrcSetMem ((UINT8 *) SavedParams, sizeof (SavedParams), 0);
  MrcCall->MrcSetMem ((UINT8 *) CccSampleCount, sizeof (CccSampleCount), 0);
  MrcCall->MrcSetMem ((UINT8 *) DataSampleCount, sizeof (DataSampleCount), 0);
  CompSampleCount = 0;
  TotalSampleCounts = TOTAL_SAMPLE_COUNTS_VALUE;

  // setup DCC FSM for accumulator mode
  SetupDccAccComp (MrcData, TRUE, TotalSampleCounts, SavedParams);
  SetupDccAccCmd (MrcData, TRUE, TotalSampleCounts, DCD_GROUP_SRZ, TRUE, SavedParams);
  SetupDccAccData (MrcData, TRUE, TotalSampleCounts, DCD_GROUP_SRZ, SavedParams);

  // Start MDLLs
  MrcOvrMdllEn (MrcData, TRUE, DccFubAll, FALSE);

  if (FubMask & DccFubComp) {                               // Measure COMP fub duty cycles
    GetSetVal = DccIndex;
    CompDccCtl6.Data = MrcReadCR (MrcData, DDRPHY_COMP_CR_DCCCTL6_REG);
    CompDccCtl6.Bits.dcdsamplesel = DccIndex;
    CompDccCtl6.Bits.step1pattern = gPatternTypes[DcdGroup];
    MrcWriteCR (MrcData, DDRPHY_COMP_CR_DCCCTL6_REG, CompDccCtl6.Data);
    RunDccAccComp (MrcData, TotalSampleCounts, CompDutyCycle, &CompSampleCount);
  }

  if (FubMask & DccFubCcc) {                               // Measure CCC fub duty cycles
    GetSetVal = DccIndex;
    MrcGetSetCccInst (MrcData, MAX_CCC_INSTANCES, GsmIocDccDcdSampleSelCcc, WriteNoCache, &GetSetVal);
    GetSetVal = gPatternTypes[DcdGroup];
    MrcGetSetCccInst (MrcData, MAX_CCC_INSTANCES, GsmIocStep1PatternCcc, WriteNoCache, &GetSetVal);
    RunDccAccCmd (MrcData, TotalSampleCounts, CccDutyCycle, CccSampleCount);
  }

  if (FubMask & DccFubData) {                               // Measure DATA fub duty cycles
    GetSetVal = DccIndex;
    MrcGetSetChStrb (MrcData,MAX_CONTROLLER, MAX_CHANNEL, MAX_SDRAM_IN_DIMM, GsmIocDccDcdSampleSelData, WriteNoCache, &GetSetVal);
    GetSetVal = gPatternTypes[DcdGroup];
    MrcGetSetChStrb (MrcData,MAX_CONTROLLER, MAX_CHANNEL, MAX_SDRAM_IN_DIMM, GsmIocStep1PatternData, WriteNoCache, &GetSetVal);
    RunDccAccData (MrcData, TotalSampleCounts, DataDutyCycle, DataSampleCount);
  }

  // Disable MDLLs
  MrcOvrMdllEn (MrcData, FALSE, DccFubAll, FALSE);

  // Exit Dcc Accumulator Mode
  SetupDccAccComp (MrcData, FALSE, TotalSampleCounts, SavedParams);
  SetupDccAccCmd (MrcData, FALSE, TotalSampleCounts, DCD_GROUP_SRZ, TRUE, SavedParams);
  SetupDccAccData (MrcData, FALSE, TotalSampleCounts, DCD_GROUP_SRZ, SavedParams);
}

/**
  Check if CCC Phase0 DCC values are saturated

  @param[in, out]   MrcData             - Include all MRC global data.
  @param[in, out]   CccPh0OverflowMask  - Pointer to DCC Overflow index mask for all CCC instances.

  @retval None
**/
VOID
CheckForCccDccSaturation (
  IN OUT MrcParameters* const MrcData,
  IN OUT UINT16               CccPh0OverflowMask[MAX_CCC_INSTANCES]
  )
{
  MrcInput                  *Inputs;
  UINT8                      CccInst;
  UINT8                      CccFub;
  UINT32                     Controller;
  UINT32                     Channel;
  UINT32                     Offset;
  UINT8                      CccSaturationMask;
  CH2CCC_CR_DCCCTL18_STRUCT  CccDccCtl18;

  Inputs            = &MrcData->Inputs;
  CccSaturationMask = 0;

  for (CccInst = 0; CccInst < MRC_NUM_CCC_INSTANCES; CccInst++) {
    CccFub = (Inputs->UlxUlt) ? CccInst : CccIndexToFub[CccInst];                    // Direct register read, need CccFub mapping here?
    DdrIoTranslateCccToMcChannel (MrcData, CccFub, &Controller, &Channel);
    if (!MrcChannelExist (MrcData, Controller, Channel)) {
      continue;
    }
    Offset = OFFSET_CALC_CH (CH2CCC_CR_DCCCTL18_REG, CH0CCC_CR_DCCCTL18_REG, CccFub);
    CccDccCtl18.Data = MrcReadCR (MrcData, Offset);
    CccPh0OverflowMask[CccFub] = (UINT16) CccDccCtl18.Bits.dccctrlcodeph0overflow;
    if (CccPh0OverflowMask[CccFub] != 0) {
      CccSaturationMask |=  (MRC_BIT0 << CccFub);
    }
  }
  MRC_DEBUG_MSG (&MrcData->Outputs.Debug, MSG_LEVEL_NOTE, "Saturated CCC instances bitmask: 0x%X\n", CccSaturationMask);
}

#ifdef MRC_DEBUG_PRINT
// Indicates next DCC code correction direction.
const CHAR8 gDccStepDir[7] = {
  '.',              // DccStepDirNone
  '-',              // DccStepDirFineDec
  '+',              // DccStepDirFineInc
  '/',              // DccStepDirCoarseDec
  '*',              // DccStepDirCoarseInc
  '~',              // DccStepDirDither
  '='               // DccStepDirAlign
};

const CHAR8 gDccStrNull[]      = "------";

const CHAR8* gDutyCycleGrpStr[DutyCycleMaxGrp] = {
  "DllRefClk",
  "DllVcdl",
  "Ck0",
  "CkWck1",
  "NonCk",
  "DqsDc",
  "DqDc",
  "CompDc",
  "CccDc",
  "DataDc"
};

/**
  Print DCC code search data for an iteration during calibration

  @param[in, out] MrcData               - Include all MRC global data.
  @param[in]      DccIndex              - DCC Lane Index.
  @param[in]      Iter                  - Calibration iteration.
  @param[in]      PrintFubMask          - FubMask with valid search data
  @param[in]      CompDccCodeSearchData - Pointer to CompDccCodeSearchData.
  @param[in]      CccDccCodeSearchData  - Array of all CCC instance CccDccCodeSearchData.
  @param[in]      DataDccCodeSearchData - Array of all DATA instance CccDccCodeSearchData.

  @retval None
**/
VOID
PrintDccIter (
  IN OUT MrcParameters* const MrcData,
  IN UINT8                    DccIndex,
  IN UINT8                    Iter,
  IN DccFubMask               PrintFubMask,
  MrcDccCodeSearchState       CompDccCodeSearchData,
  MrcDccCodeSearchState       CccDccCodeSearchData[MRC_NUM_CCC_INSTANCES],
  MrcDccCodeSearchState       DataDccCodeSearchData[MAX_CONTROLLER][MAX_CHANNEL][MAX_SDRAM_IN_DIMM]
  ) {
  MrcDebug      *Debug;
  MrcOutput     *Outputs;
  UINT32        Controller;
  UINT8         Channel;
  UINT8         Strobe;
  UINT8         CccInst;
  UINT32        DutyCycle;

  Outputs = &MrcData->Outputs;
  Debug   = &Outputs->Debug;

  if (PrintFubMask == 0) {
    return;
  }

  if (Iter == 0) {
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\nData Fields - |DccCode, DutyCycle, DccStepDirection ('.':None, '-':FineDec, '+':FineInc, '/':CoarseDec, '*':CoarseInc, '~':Dither, '=':Align)|\n");
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "DccIndex| Iter |");

    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "    Comp0   ||");

    for (CccInst = 0; CccInst < MRC_NUM_CCC_INSTANCES; CccInst++) {
      MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "    CccC%u   |", CccInst);
    } //CccInst
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "|");

    for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
      for (Channel = 0; Channel < Outputs->MaxChannels; Channel++) {
        for (Strobe = 0; Strobe < Outputs->SdramCount; Strobe++) {
          MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "   M%uC%uS%u   |", Controller, Channel, Strobe);
        } // Strobe
      } // Channel
    } // Controller
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\n");
  }

  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "   %02d   |  %02d  |", DccIndex, Iter);

  if (PrintFubMask & DccFubComp) {
    DutyCycle = CompDccCodeSearchData.FbValue;
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, " %03d", CompDccCodeSearchData.CodePrev);
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, " %02d.%02d", DutyCycle / 100, DutyCycle % 100);
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, " %c||", gDccStepDir[CompDccCodeSearchData.DccStepDir]);
  } else {
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "   %s   ||",gDccStrNull);
  }

  for (CccInst = 0; CccInst < MRC_NUM_CCC_INSTANCES; CccInst++) {
    if (PrintFubMask & DccFubCcc) {
      DutyCycle = CccDccCodeSearchData[CccInst].FbValue;
      MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, " %03d", CccDccCodeSearchData[CccInst].CodePrev);
      MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, " %02d.%02d", DutyCycle / 100, DutyCycle % 100);
      MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, " %c|", gDccStepDir[CccDccCodeSearchData[CccInst].DccStepDir]);
    } else {
      MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "   %s   |",gDccStrNull);
    }
  } //CccInst
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "|");


  for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
    for (Channel = 0; Channel < Outputs->MaxChannels; Channel++) {
      for (Strobe = 0; Strobe < Outputs->SdramCount; Strobe++) {
        if (PrintFubMask & DccFubData) {
          DutyCycle = DataDccCodeSearchData[Controller][Channel][Strobe].FbValue;
          MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, " %03d", DataDccCodeSearchData[Controller][Channel][Strobe].CodePrev);
          MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, " %02d.%02d", DutyCycle / 100, DutyCycle % 100);
          MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, " %c|", gDccStepDir[DataDccCodeSearchData[Controller][Channel][Strobe].DccStepDir]);
        } else {
          MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "   %s   |",gDccStrNull);
        }
      } // Strobe
    } // Channel
  } // Controller

  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\n");
}

/**
  Print DCC code search data for an iteration during calibration

  @param[in, out]   MrcData               - Include all MRC global data.
  @param[in]        DcdGroup              - DCD_GROUP type.
  @param[in]        DccIndex              - DCC Lane Index.
  @param[in]        CompDutyCycle         - Comp Duty Cycle value.
  @param[in]        CccDutyCycle          - Array of all CCC instance Duty Cycle values.
  @param[in]        DataDutyCycle         - Array of all DATA instance Duty Cycle values.
  @param[in]        PrintHeader           - Print Header only.

  @retval None
**/
VOID
PrintDutyCycle (
  IN OUT MrcParameters* const MrcData,
  IN UINT8                    DcdGroup,
  IN UINT8                    DccIndex,
  IN UINT32                   CompDutyCycle,
  IN UINT32                   CccDutyCycle[MRC_NUM_CCC_INSTANCES],
  IN UINT32                   DataDutyCycle[MAX_CONTROLLER][MAX_CHANNEL][MAX_SDRAM_IN_DIMM],
  IN BOOLEAN                  PrintHeader
  )
{
  MrcDebug           *Debug;
  MrcOutput          *Outputs;
  UINT32             CccInst;
  UINT32             Controller;
  UINT32             Channel;
  UINT32             Strobe;
  UINT32             DutyCycle;
  BOOLEAN            ValidCccIndex;
  BOOLEAN            ValidCompIndex;
  BOOLEAN            ValidDataIndex;
  UINT16             ValidCccIndexMask;
  UINT16             ValidCompIndexMask;
  UINT16             ValidDataIndexMask;

  Outputs = &MrcData->Outputs;
  Debug   = &Outputs->Debug;

  if (PrintHeader) {
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\nDCD Groups  | DccIndex |");

    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "  Comp0 ||");
    for (CccInst = 0; CccInst < MRC_NUM_CCC_INSTANCES; CccInst++) {
      MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "  CccC%u |", CccInst);
    } //CccInst
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "|");
    for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
      for (Channel = 0; Channel < Outputs->MaxChannels; Channel++) {
        for (Strobe = 0; Strobe < Outputs->SdramCount; Strobe++) {
          MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, " M%uC%uS%u |", Controller, Channel, Strobe);
        } // Strobe
      } // Channel
    } // Controller
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\n");

  } else {

    ValidCccIndexMask  = MrcGetDccIndexMask(MrcData, DcdGroup, DccFubCcc);
    ValidDataIndexMask = MrcGetDccIndexMask(MrcData, DcdGroup, DccFubData);
    ValidCompIndexMask = MrcGetDccIndexMask(MrcData, DcdGroup, DccFubComp);

    MRC_DEBUG_MSG(Debug, MSG_LEVEL_NOTE, "%11s |    %02d    |", gDcdGrpNameStr[DcdGroup], DccIndex);

    ValidCompIndex = (((ValidCompIndexMask >> DccIndex) & MRC_BIT0) != 0);
    if (ValidCompIndex) {
      DutyCycle = CompDutyCycle;
      MRC_DEBUG_MSG(Debug, MSG_LEVEL_NOTE, "  %02d.%02d ||", (DutyCycle / 100), (DutyCycle % 100));
    } else {
      MRC_DEBUG_MSG(Debug, MSG_LEVEL_NOTE, " %s ||", gDccStrNull);
    }

    ValidCccIndex = (((ValidCccIndexMask >> DccIndex) & MRC_BIT0) != 0);
    for (CccInst = 0; CccInst < MRC_NUM_CCC_INSTANCES; CccInst++) {
      if (ValidCccIndex) {
        DutyCycle = CccDutyCycle[CccInst];
        MRC_DEBUG_MSG(Debug, MSG_LEVEL_NOTE, "  %02d.%02d |", (DutyCycle / 100), (DutyCycle % 100));
      } else {
        MRC_DEBUG_MSG(Debug, MSG_LEVEL_NOTE, " %s |", gDccStrNull);
      }
    } //CccInst
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "|");

    ValidDataIndex = (((ValidDataIndexMask >> DccIndex) & MRC_BIT0) != 0);
    for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
      for (Channel = 0; Channel < Outputs->MaxChannels; Channel++) {
        for (Strobe = 0; Strobe < Outputs->SdramCount; Strobe++) {
          if (ValidDataIndex) {
            DutyCycle = DataDutyCycle[Controller][Channel][Strobe];
            MRC_DEBUG_MSG(Debug, MSG_LEVEL_NOTE, "  %02d.%02d |", (DutyCycle / 100), (DutyCycle % 100));
          } else {
            MRC_DEBUG_MSG(Debug, MSG_LEVEL_NOTE, " %s |", gDccStrNull);
          }
        } // Strobe
      } // Channel
    } // Controller

    MRC_DEBUG_MSG(Debug, MSG_LEVEL_NOTE, "\n");
  }
}

/**
  Print duty cycle groups info

  @param[in, out]   MrcData           - Include all MRC global data.
  @param[in]        DutyCycleGrpData  - Duty Cycle group data.

  @retval None
**/
VOID
MrcPrintDutyCycleGrpData (
  IN OUT MrcParameters* const MrcData,
  DutyCycleGrpData            DutyCycleGrpData[DutyCycleMaxGrp]
  )
{
  MrcDebug      *Debug;
  UINT8         Index;
  BOOLEAN       DcGroupPass;
  UINT32        DutyCycleMin;
  UINT32        DutyCycleMax;
  UINT32        DutyCycleMinLimit;
  UINT32        DutyCycleMaxLimit;

  Debug       = &MrcData->Outputs.Debug;
  DcGroupPass = TRUE;

  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\nMin/Max Measurements\n");
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "   Param   |   Min  |  Max  | SpecMin | SpecMax | Pass | PartMin | IndexMin | GroupMin  | PartMax | IndexMax | GroupMax  |\n");
  for (Index = 0; Index < DutyCycleMaxGrp; Index++) {
    DutyCycleMin      = DutyCycleGrpData[Index].DutyCycleMin;
    DutyCycleMax      = DutyCycleGrpData[Index].DutyCycleMax;
    DutyCycleMinLimit = DutyCycleGrpData[Index].DutyCycleMinLimit;
    DutyCycleMaxLimit = DutyCycleGrpData[Index].DutyCycleMaxLimit;

    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, " %9s | %3d.%02d | %02d.%02d |  %02d.%02d  |  %02d.%02d  | %5s|%9s|   %3d    | %9s |%9s|   %3d    | %9s |\n",
                    gDutyCycleGrpStr[Index], (DutyCycleMin / 100), (DutyCycleMin % 100), (DutyCycleMax / 100), (DutyCycleMax % 100),
                    (DutyCycleMinLimit / 100), (DutyCycleMinLimit % 100), (DutyCycleMaxLimit / 100), (DutyCycleMaxLimit % 100),
                    (DutyCycleGrpData[Index].DutyCycleGood ? "True" : "False"), DutyCycleGrpData[Index].PartitionMin,  DutyCycleGrpData[Index].DccIndexMin,
                    gDcdGrpNameStr[DutyCycleGrpData[Index].DcdGroupMin], DutyCycleGrpData[Index].PartitionMax,  DutyCycleGrpData[Index].DccIndexMax,
                    gDcdGrpNameStr[DutyCycleGrpData[Index].DcdGroupMax]
                    );

    if (DutyCycleGrpData[Index].DutyCycleGood == FALSE) {
      DcGroupPass = FALSE;
    }
  }
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Duty Cycle Calibration Result: %s\n\n", (DcGroupPass ? "PASS" : "Warning: Measured Duty Cycle Outside of Spec Range"));
}

/**
  Dump DCC code values for all DCD groups and indices.

  @param[in, out]   MrcData   - Include all MRC global data.

  @retval None
**/
VOID
DumpDccCodes (
  IN OUT MrcParameters *const MrcData
  )
{
  MrcOutput      *Outputs;
  MrcDebug       *Debug;
  UINT32         CccInst;
  UINT32         Controller;
  UINT32         Channel;
  UINT32         Strobe;
  BOOLEAN        Gear4;
  INT64          GetDccVal;
  INT64          TxPbdOffset;
  INT64          TxPbdSign;
  UINT8          DcdGroup;
  UINT8          DccIndex;
  UINT8          DcdGroupMax;
  UINT16         ValidCompIndexMask;
  UINT16         ValidCccIndexMask;
  UINT16         ValidDataIndexMask;
  DDRPHY_COMP_CR_DCCCTL4_STRUCT            DccCtl4;
  DDRPHY_COMP_CR_DLLCOMP_VCDLCTRL_STRUCT   DllCompVcdlCtrl;

  Outputs    = &MrcData->Outputs;
  Debug      = &Outputs->Debug;
  Gear4      = Outputs->Gear4;
  GetDccVal  = 0;
  DcdGroupMax = Gear4 ? DCD_GROUP_MAX : (DCD_GROUP_SRZ + 1);                         // Skip SrzPh90 and SrzPhQuad for !Gear4

  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\nDump all DCC codes\n");
  if (Gear4) {
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "SrzQuad Code BIT6 represents txpbd_ph90incr\n");
  }
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "DCD Groups  | DccIndex |");

  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "  Comp0 ||");
  for (CccInst = 0; CccInst < MRC_NUM_CCC_INSTANCES; CccInst++) {
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "  CccC%u |", CccInst);
  } //CccInst
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "|");
  for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
    for (Channel = 0; Channel < Outputs->MaxChannels; Channel++) {
      for (Strobe = 0; Strobe < Outputs->SdramCount; Strobe++) {
        MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, " M%uC%uS%u |", Controller, Channel, Strobe);
      } // Strobe
    } // Channel
  } // Controller
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\n");


  for (DcdGroup = 0; DcdGroup < DcdGroupMax ; DcdGroup++) {

    ValidCompIndexMask = MrcGetDccIndexMask(MrcData, DcdGroup, DccFubComp);
    ValidCccIndexMask  = MrcGetDccIndexMask(MrcData, DcdGroup, DccFubCcc);
    ValidDataIndexMask = MrcGetDccIndexMask(MrcData, DcdGroup, DccFubData);

    for (DccIndex = 0; DccIndex < MAX_DCC_INDEX; DccIndex++) {
      if ((((ValidCompIndexMask >> DccIndex) & 0x1) == 0) && (((ValidCccIndexMask >> DccIndex) & 0x1) == 0) &&
        (((ValidDataIndexMask >> DccIndex) & 0x1) == 0)) {
        continue;
      }

      MRC_DEBUG_MSG(Debug, MSG_LEVEL_NOTE, "%11s |    %02d    |", gDcdGrpNameStr[DcdGroup], DccIndex);

      if (((ValidCompIndexMask >> DccIndex) & 0x1) != 0) {
        if (DcdGroup == DCD_GROUP_REFCLK) {
          DccCtl4.Data = MrcReadCR (MrcData, DDRPHY_COMP_CR_DCCCTL4_REG);
          GetDccVal = DccCtl4.Bits.dcccodeph0_index0;
        } else if (DcdGroup == DCD_GROUP_VCDL) {
          DllCompVcdlCtrl.Data = MrcReadCR (MrcData, DDRPHY_COMP_CR_DLLCOMP_VCDLCTRL_REG);
          GetDccVal = DllCompVcdlCtrl.Bits.dllcomp_cmn_vcdldcccode;
        } else {
          // @todo_adl - Update for other groups
        }
        MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "   %03u  ||", GetDccVal);
      } else {
        MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, " %s ||", gDccStrNull);
      }

      for (CccInst = 0; CccInst < MRC_NUM_CCC_INSTANCES; CccInst++) {
        if (((ValidCccIndexMask >> DccIndex) & 0x1) != 0) {
          switch (DcdGroup) {
            case DCD_GROUP_REFCLK:
            case DCD_GROUP_SRZ:
              MrcGetSetCccInstLane (MrcData, CccInst, DccIndex, GsmIocDccCodePh0IndexCcc, ReadUncached, &GetDccVal);
              break;

            case DCD_GROUP_SRZ_PH90:
              MrcGetSetCccInstLane (MrcData, CccInst, DccIndex, GsmIocDccCodePh90IndexCcc, ReadUncached, &GetDccVal);
              break;

            case DCD_GROUP_SRZ_QUAD:
              MrcGetSetCccInstLane (MrcData, CccInst, DccIndex, GsmIocTxpbddOffsetCcc, ReadUncached, &TxPbdOffset);
              MrcGetSetCccInstLane (MrcData, CccInst, DccIndex, GsmIocTxpbdPh90incrCcc, ReadUncached, &TxPbdSign);
              GetDccVal = (UINT32) TxPbdOffset;
              GetDccVal |= ((UINT32)TxPbdSign << 6);
              break;

            case DCD_GROUP_VCDL:
              MrcGetSetCccInstLane (MrcData, CccInst, DccIndex, GsmIocMdllCmnVcdlDccCodeCcc, ReadUncached, &GetDccVal);
              break;

            default:
              break;
          }
          MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "   %03u  |", GetDccVal);
        } else {
          MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, " %s |", gDccStrNull);
        }
      } //CccInst
      MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "|");

      for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
        for (Channel = 0; Channel < Outputs->MaxChannels; Channel++) {
          for (Strobe = 0; Strobe < Outputs->SdramCount; Strobe++) {
            if (((ValidDataIndexMask >> DccIndex) & 0x1) != 0) {
              switch (DcdGroup) {
                case DCD_GROUP_REFCLK:
                case DCD_GROUP_SRZ:
                  MrcGetSetBit (MrcData, Controller, Channel, MRC_IGNORE_ARG, Strobe, DccIndex, GsmIocDccCodePh0IndexData, ReadUncached, &GetDccVal);
                  break;

                case DCD_GROUP_SRZ_PH90:
                  MrcGetSetBit (MrcData, Controller, Channel, MRC_IGNORE_ARG, Strobe, DccIndex, GsmIocDccCodePh90IndexData, ReadUncached, &GetDccVal);
                  break;

                case DCD_GROUP_SRZ_QUAD:
                  MrcGetSetBit (MrcData, Controller, Channel, MRC_IGNORE_ARG, Strobe, DccIndex, GsmIocTxpbddOffsetData, ReadUncached, &TxPbdOffset);
                  MrcGetSetBit (MrcData, Controller, Channel, MRC_IGNORE_ARG, Strobe, DccIndex, GsmIocTxpbdPh90incrData, ReadUncached, &TxPbdSign);
                  GetDccVal = (UINT32) TxPbdOffset;
                  GetDccVal |= ((UINT32)TxPbdSign << 6);
                  break;

                case DCD_GROUP_VCDL:
                  MrcGetSetBit (MrcData, Controller, Channel, MRC_IGNORE_ARG, Strobe, DccIndex, GsmIocMdllCmnVcdlDccCodeData, ReadUncached, &GetDccVal);
                  break;

                default:
                  break;
              }
              MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "   %03u  |", GetDccVal);
            } else {
              MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, " %s |", gDccStrNull);
            }
          }  // Strobe
        }  // Channel
      }  // Controller
      MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\n");
    }
  }
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\n");
}
#endif

/**
  Run DCC code calibration algorithm for DCD group

  @param[in, out] MrcData        - Include all MRC global data.
  @param[in]      DcdGroup       - DCD_GROUP type.
  @param[in]      CompIndexMask  - COMP DCC Lane mask to run.
  @param[in]      CccIndexMask   - CCC DCC Lane mask to run on all CCC instances.
  @param[in]      DataIndexMask  - DATA DCC Lane mask to run on all DATA instances.

  @retval None
**/
VOID
MrcInitSwFsm (
  IN OUT MrcParameters* const MrcData,
  IN const UINT32             DcdGroup,
  IN UINT16                   CompIndexMask,
  IN UINT16                   CccIndexMask[MAX_CCC_INSTANCES],
  IN UINT16                   DataIndexMask[MAX_CONTROLLER][MAX_CHANNEL][MAX_SDRAM_IN_DIMM]
  )
{
  const MRC_FUNCTION     *MrcCall;
  MrcOutput              *Outputs;
  MrcInput               *Inputs;
  UINT32                 CccInst;
  UINT32                 Controller;
  UINT32                 Channel;
  UINT32                 Strobe;
  INT64                  GetSetVal;
  UINT32                 SavedParams[MAX_DCC_SAVED_PARAMETERS];
  UINT16                 ValidCompIndexMask;
  UINT16                 ValidCccIndexMask;
  UINT16                 ValidDataIndexMask;
  BOOLEAN                ValidCompIndex;
  BOOLEAN                ValidCccIndex;
  BOOLEAN                ValidDataIndex;
  BOOLEAN                CompIndexDone;
  BOOLEAN                CccIndexDone[MRC_NUM_CCC_INSTANCES];
  BOOLEAN                DataIndexDone[MAX_CONTROLLER][MAX_CHANNEL][MAX_SDRAM_IN_DIMM];
  MrcDccCodeSearchState  CompDccCodeSearchData;
  MrcDccCodeSearchState  CccDccCodeSearchData[MRC_NUM_CCC_INSTANCES];
  MrcDccCodeSearchState  DataDccCodeSearchData[MAX_CONTROLLER][MAX_CHANNEL][MAX_SDRAM_IN_DIMM];
  BOOLEAN                CurCompIndexDone;
  BOOLEAN                CurCccIndexDone;
  BOOLEAN                CurDataIndexDone;
  UINT32                 CompDutyCycle;
  UINT32                 CccDutyCycle[MRC_NUM_CCC_INSTANCES];
  UINT32                 DataDutyCycle[MAX_CONTROLLER][MAX_CHANNEL][MAX_SDRAM_IN_DIMM];
  UINT32                 CompSampleCount;
  UINT32                 CccSampleCount[MRC_NUM_CCC_INSTANCES];
  UINT32                 DataSampleCount[MAX_CONTROLLER][MAX_CHANNEL][MAX_SDRAM_IN_DIMM];
  DccFubMask             PrintFubMask;
  UINT8                  DccIndex;
  UINT8                  DccCorrectionCount;
  UINT32                 TotalSampleCounts;
  DDRPHY_COMP_CR_DCCCTL6_STRUCT  CompDccCtl6;

  Outputs     = &MrcData->Outputs;
  Inputs      = &MrcData->Inputs;
  MrcCall     = Inputs->Call.Func;


  ValidCompIndexMask = MrcGetDccIndexMask(MrcData, DcdGroup, DccFubComp);
  ValidCccIndexMask  = MrcGetDccIndexMask(MrcData, DcdGroup, DccFubCcc);
  ValidDataIndexMask = MrcGetDccIndexMask(MrcData, DcdGroup, DccFubData);
  MrcCall->MrcSetMem ((UINT8 *) SavedParams, sizeof (SavedParams), 0);
  MrcCall->MrcSetMem ((UINT8 *) (&CompDccCodeSearchData), sizeof (MrcDccCodeSearchState), 0);
  MrcCall->MrcSetMem ((UINT8 *) CccDccCodeSearchData, (sizeof (MrcDccCodeSearchState) * MRC_NUM_CCC_INSTANCES), 0);
  MrcCall->MrcSetMem ((UINT8 *) DataDccCodeSearchData, (sizeof (MrcDccCodeSearchState) * MAX_CONTROLLER * MAX_CHANNEL * MAX_SDRAM_IN_DIMM), 0);
  CompDutyCycle   = 0;
  MrcCall->MrcSetMem ((UINT8 *) CccDutyCycle, sizeof (CccDutyCycle), 0);
  MrcCall->MrcSetMem ((UINT8 *) DataDutyCycle, sizeof (DataDutyCycle), 0);
  CompSampleCount = 0;
  MrcCall->MrcSetMem ((UINT8 *) CccSampleCount, sizeof (CccSampleCount), 0);
  MrcCall->MrcSetMem ((UINT8 *) DataSampleCount, sizeof (DataSampleCount), 0);
  TotalSampleCounts = TOTAL_SAMPLE_COUNTS_VALUE;

  MRC_DEBUG_MSG (&MrcData->Outputs.Debug, MSG_LEVEL_NOTE, "%s Calibration Start\n", gDcdGrpNameStr[DcdGroup]);

  // setup DCC FSM for accumulator mode
  SetupDccAccComp (MrcData, TRUE, TotalSampleCounts, SavedParams);
  SetupDccAccCmd (MrcData, TRUE, TotalSampleCounts, DcdGroup, TRUE, SavedParams);
  SetupDccAccData (MrcData, TRUE, TotalSampleCounts, DcdGroup, SavedParams);

  // Start MDLLs
  MrcOvrMdllEn (MrcData, TRUE, DccFubAll, FALSE);

  for (DccIndex = 0; DccIndex < MAX_DCC_INDEX; DccIndex++) {

    ValidCompIndex = FALSE;
    ValidCccIndex  = FALSE;
    ValidDataIndex = FALSE;
    CompIndexDone    = TRUE;
    MrcCall->MrcSetMem ((UINT8 *) CccIndexDone, sizeof (CccIndexDone), TRUE);
    MrcCall->MrcSetMem ((UINT8 *) DataIndexDone, sizeof (DataIndexDone), TRUE);

    // SET DCD rosample buffer select for current DCC index
    if ((((CompIndexMask & ValidCompIndexMask) >> DccIndex) & 0x1) != 0) {
      ValidCompIndex = TRUE;
      CompIndexDone  = FALSE;
      MrcDccCodeSearchDataInit (MrcData, DcdGroup, &CompDccCodeSearchData);
      CompDccCtl6.Data = MrcReadCR (MrcData, DDRPHY_COMP_CR_DCCCTL6_REG);
      CompDccCtl6.Bits.dcdsamplesel = DccIndex;
      MrcWriteCR (MrcData, DDRPHY_COMP_CR_DCCCTL6_REG, CompDccCtl6.Data);
    }

    for (CccInst = 0; CccInst < MRC_NUM_CCC_INSTANCES; CccInst++) {
      DdrIoTranslateCccToMcChannel (MrcData, CccInst, &Controller, &Channel);
      if (!MrcChannelExist (MrcData, Controller, Channel)) {
        continue;
      }
      if ((((CccIndexMask[CccInst] & ValidCccIndexMask) >> DccIndex) & MRC_BIT0) != 0) {
        ValidCccIndex         = TRUE;
        CccIndexDone[CccInst] = FALSE;
        MrcDccCodeSearchDataInit (MrcData, DcdGroup, &CccDccCodeSearchData[CccInst]);
        GetSetVal = DccIndex;
        MrcGetSetCccInst (MrcData, CccInst, GsmIocDccDcdSampleSelCcc, WriteNoCache, &GetSetVal);
      }
    } //CccInst

    for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
      for (Channel = 0; Channel < Outputs->MaxChannels; Channel++) {
        if (!MrcChannelExist (MrcData, Controller, Channel)) {
          continue;
        }
        for (Strobe = 0; Strobe < Outputs->SdramCount; Strobe++) {
          if ((((DataIndexMask[Controller][Channel][Strobe] & ValidDataIndexMask) >> DccIndex) & MRC_BIT0) != 0) {
            ValidDataIndex = TRUE;
            DataIndexDone[Controller][Channel][Strobe] = FALSE;
            MrcDccCodeSearchDataInit (MrcData, DcdGroup, &DataDccCodeSearchData[Controller][Channel][Strobe]);
            GetSetVal = DccIndex;
            MrcGetSetChStrb (MrcData, Controller, Channel, Strobe, GsmIocDccDcdSampleSelData, WriteNoCache, &GetSetVal);
          }
        } // Strobe
      } // Channel
    } // Controller

    // Run the calibration step until DCC codes are converged or max correction count is reached.
    for (DccCorrectionCount = 0; DccCorrectionCount < DCC_CORRECTION_LIMIT_SWFSM; DccCorrectionCount++) {
      // Assume current DCC index is done.
      CurCompIndexDone = TRUE;
      CurCccIndexDone  = TRUE;
      CurDataIndexDone = TRUE;
      PrintFubMask     = 0;

      /*
        Algorithm at high level
        1. Set the DCC code.
        2. Run the Accumulator for all COMP/CCC/DATA instances.
        3. Parse and update DCC code search data using results from accumulator.
        4. Set DCC index done variables according to duty cycle value.
        5. Run steps 1 to 4 until duty cycle is converged within limits or maximum correction limit is reached.
      */
      if (ValidCompIndex) {
        MrcSetDccCodeComp (MrcData, DcdGroup, CompDccCodeSearchData, CompIndexDone);
        RunDccAccComp (MrcData, TotalSampleCounts, &CompDutyCycle, &CompSampleCount);
        if ((CompIndexDone == FALSE) && (CompDccCodeSearchData.Done == FALSE)) {
          CompDccCodeSearchData.FbValuePrev = CompDccCodeSearchData.FbValue;
          CompDccCodeSearchData.FbValue     = CompDutyCycle;
          MrcParseDccCodeSearchData (MrcData, DcdGroup, &CompDccCodeSearchData);
          PrintFubMask |= DccFubComp;
          if (CompDccCodeSearchData.Done == FALSE) {
            CurCompIndexDone = FALSE;
          } else {
            CompIndexDone = TRUE;
          }
        }
      }

      if (ValidCccIndex) {
        MrcSetDccCodeCcc (MrcData, DcdGroup, DccIndex, CccDccCodeSearchData, CccIndexDone);
        RunDccAccCmd (MrcData, TotalSampleCounts, CccDutyCycle, CccSampleCount);

        for (CccInst = 0; CccInst < MRC_NUM_CCC_INSTANCES; CccInst++) {
          DdrIoTranslateCccToMcChannel (MrcData, CccInst, &Controller, &Channel);
          if (!MrcChannelExist (MrcData, Controller, Channel)) {
            continue;
          }
          if ((CccIndexDone[CccInst] == FALSE) && (CccDccCodeSearchData[CccInst].Done == FALSE)) {
            CccDccCodeSearchData[CccInst].FbValuePrev = CccDccCodeSearchData[CccInst].FbValue;
            CccDccCodeSearchData[CccInst].FbValue     = CccDutyCycle[CccInst];
            MrcParseDccCodeSearchData (MrcData, DcdGroup, &CccDccCodeSearchData[CccInst]);
            PrintFubMask |= DccFubCcc;
            if (CccDccCodeSearchData[CccInst].Done == FALSE) {
              CurCccIndexDone = FALSE;
            } else {
              CccIndexDone[CccInst] = TRUE;
            }
          }
        } //CccInst
      }

      if (ValidDataIndex) {
        MrcSetDccCodeData (MrcData, DcdGroup, DccIndex, DataDccCodeSearchData, DataIndexDone);
        RunDccAccData (MrcData, TotalSampleCounts, DataDutyCycle, DataSampleCount);

        for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
          for (Channel = 0; Channel < Outputs->MaxChannels; Channel++) {
            if (!MrcChannelExist (MrcData, Controller, Channel)) {
              continue;
            }
            for (Strobe = 0; Strobe < Outputs->SdramCount; Strobe++) {
              if ((DataIndexDone[Controller][Channel][Strobe] == FALSE) &&
                  (DataDccCodeSearchData[Controller][Channel][Strobe].Done == FALSE)) {
                DataDccCodeSearchData[Controller][Channel][Strobe].FbValuePrev = DataDccCodeSearchData[Controller][Channel][Strobe].FbValue;
                DataDccCodeSearchData[Controller][Channel][Strobe].FbValue     = DataDutyCycle[Controller][Channel][Strobe];
                MrcParseDccCodeSearchData (MrcData, DcdGroup, &DataDccCodeSearchData[Controller][Channel][Strobe]);
                PrintFubMask |= DccFubData;
                if (DataDccCodeSearchData[Controller][Channel][Strobe].Done == FALSE) {
                  CurDataIndexDone = FALSE;
                } else {
                  DataIndexDone[Controller][Channel][Strobe] = TRUE;
                }
              }
            } // Strobe
          } // Channel
        } // Controller
      }

#ifdef MRC_DEBUG_PRINT
      PrintDccIter (MrcData, DccIndex, DccCorrectionCount, PrintFubMask, CompDccCodeSearchData, CccDccCodeSearchData, DataDccCodeSearchData);
#endif

      if ((CurCompIndexDone == TRUE) && (CurCccIndexDone == TRUE) && (CurDataIndexDone == TRUE)) {
        break;
      }
    }
  }

  // Cleanup
  // setup DCC FSM for accumulator mode
  SetupDccAccComp (MrcData, FALSE, TotalSampleCounts, SavedParams);
  SetupDccAccCmd (MrcData, FALSE, TotalSampleCounts, DcdGroup, TRUE, SavedParams);
  SetupDccAccData (MrcData, FALSE, TotalSampleCounts, DcdGroup, SavedParams);

  // Disable MDLLs
  MrcOvrMdllEn (MrcData, FALSE, DccFubAll, FALSE);

  if (Inputs->Q0 || Inputs->G0) {
    DqsDccWaQ0 (MrcData, DccFubAll);
  }
  MRC_DEBUG_MSG (&MrcData->Outputs.Debug, MSG_LEVEL_NOTE, "%s Calibration End\n", gDcdGrpNameStr[DcdGroup]);
}

/**
  Run DCC code calibration algorithm for DCD Groups/Lanes to optimize duty cycles within Spec Limits

  @param[in, out] MrcData        - Include all MRC global data.

  @retval None
**/
VOID
DccOptimize (
  IN OUT MrcParameters* const MrcData
  )
{
  const MRC_FUNCTION *MrcCall;
  MrcOutput          *Outputs;
  MrcInput           *Inputs;
  MrcDebug           *Debug;
  UINT32             CccInst;
  UINT32             Controller;
  UINT32             Channel;
  UINT32             Strobe;
  BOOLEAN            Gear4;
  UINT8              DccCheckCounter;
  UINT8              DcdGroup;
  UINT8              DccIndex;
  BOOLEAN            GlobalPass;
  DccFubMask         FubMask;
  UINT32             DutyCycle;
  UINT8              DcdGroupMax;
  BOOLEAN            DcLimitFail;
  CHAR8              PartitionStr[8];
  DutyCycleGrpData   DutyCycleGrpData[DutyCycleMaxGrp];
  UINT16             CompSaturationMask;
  UINT16             CccSaturationMask[MAX_CCC_INSTANCES];
  UINT16             DataSaturationMask[MAX_CONTROLLER][MAX_CHANNEL][MAX_SDRAM_IN_DIMM];
  UINT16             CompFailMask[DCD_GROUP_MAX];
  UINT16             CccFailMask[DCD_GROUP_MAX][MAX_CCC_INSTANCES];
  UINT16             DataFailMask[DCD_GROUP_MAX][MAX_CONTROLLER][MAX_CHANNEL][MAX_SDRAM_IN_DIMM];
  UINT16             CompIndexResult[DCD_GROUP_MAX];
  UINT16             CccIndexResult[DCD_GROUP_MAX][MAX_CCC_INSTANCES];
  UINT16             DataIndexResult[DCD_GROUP_MAX][MAX_CONTROLLER][MAX_CHANNEL][MAX_SDRAM_IN_DIMM];
  UINT32             CompDutyCycle;
  UINT32             CccDutyCycle[MRC_NUM_CCC_INSTANCES];
  UINT32             DataDutyCycle[MAX_CONTROLLER][MAX_CHANNEL][MAX_SDRAM_IN_DIMM];
  UINT32             FinalCompDutyCycle[DCD_GROUP_MAX][MAX_DCC_INDEX];
  UINT32             FinalCccDutyCycle[DCD_GROUP_MAX][MAX_DCC_INDEX][MRC_NUM_CCC_INSTANCES];
  UINT32             FinalDataDutyCycle[DCD_GROUP_MAX][MAX_DCC_INDEX][MAX_CONTROLLER][MAX_CHANNEL][MAX_SDRAM_IN_DIMM];
  UINT16             ValidCompIndexMask;
  UINT16             ValidCccIndexMask;
  UINT16             ValidDataIndexMask;
  BOOLEAN            ValidCompIndex;
  BOOLEAN            ValidCccIndex;
  BOOLEAN            ValidDataIndex;
  UINT32             DcdRoLfsr;
  INT64              GetSetVal;
  BOOLEAN            PrintHeader;

  DDRPHY_COMP_CR_DCCCTL2_STRUCT   CompDccCtl2;
  CH2CCC_CR_DCCCTL2_STRUCT        CccDccCtl2;

  Outputs      = &MrcData->Outputs;
  Inputs       = &MrcData->Inputs;
  Gear4        = Outputs->Gear4;
  MrcCall      = Inputs->Call.Func;
  Debug        = &Outputs->Debug;

  DccCheckCounter = 0;
  GlobalPass      = TRUE;
  DcdGroup        = DCD_GROUP_MAX;
  DccIndex        = MAX_DCC_INDEX;
  DcdGroupMax = Gear4 ? DCD_GROUP_MAX : (DCD_GROUP_SRZ + 1);                                            // Skip SrzPh90 and SrzPhQuad for !Gear4
  CompSaturationMask = 0;
  MrcCall->MrcSetMem ((UINT8 *) CccSaturationMask, sizeof (CccSaturationMask), 0);
  MrcCall->MrcSetMem ((UINT8 *) DataSaturationMask, sizeof (DataSaturationMask), 0);
  MrcCall->MrcSetMem ((UINT8 *) DutyCycleGrpData, sizeof (DutyCycleGrpData), 0);
  MrcCall->MrcSetMemWord ((UINT16 *) CompFailMask, sizeof (CompFailMask), 0x3);                         // Assume all Lanes are fail
  MrcCall->MrcSetMemWord ((UINT16 *) CccFailMask, sizeof (CccFailMask) / sizeof (UINT16), 0xFFFF);      // Assume all Lanes are fail
  MrcCall->MrcSetMemWord ((UINT16 *) DataFailMask, sizeof (DataFailMask) / sizeof (UINT16), 0x03FF);    // Assume all Lanes are fail
  CompDutyCycle   = 0;
  MrcCall->MrcSetMem ((UINT8 *) CccDutyCycle, sizeof (CccDutyCycle), 0);
  MrcCall->MrcSetMem ((UINT8 *) DataDutyCycle, sizeof (DataDutyCycle), 0);
  MrcCall->MrcSetMem ((UINT8 *) FinalCompDutyCycle, sizeof (FinalCompDutyCycle), 0);
  MrcCall->MrcSetMem ((UINT8 *) FinalCccDutyCycle, sizeof (FinalCccDutyCycle), 0);
  MrcCall->MrcSetMem ((UINT8 *) FinalDataDutyCycle, sizeof (FinalDataDutyCycle), 0);
  MrcSPrintf (MrcData, PartitionStr, sizeof (PartitionStr), "None");

  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\n----- DCC Optimize Start -----\n");

  if (Gear4) {                                     //@ todo_adl - need stepping check for future steppings?
    // Check for CCC SRZ phase 0 saturation and run Ph90 and Quad id Phase0 codes are saturated.
    CheckForCccDccSaturation (MrcData, CccSaturationMask);
    MrcInitSwFsm (MrcData, DCD_GROUP_SRZ_PH90, CompSaturationMask, CccSaturationMask, DataSaturationMask);
    MrcInitSwFsm (MrcData, DCD_GROUP_SRZ_QUAD, CompSaturationMask, CccSaturationMask, DataSaturationMask);
  }

  // Run the SwFsm algorithm on failing Lanes until all Lanes falls within limits or maximum correction limit is reached.
  for (DccCheckCounter = 0; DccCheckCounter < MAX_DCC_CHECK_COUNT; DccCheckCounter++) {

    GlobalPass = TRUE;
    PrintHeader = FALSE;
    // COMP/CCC/DATA Variable to update results from current iteration
    MrcCall->MrcSetMem ((UINT8 *) CompIndexResult, sizeof (CompIndexResult), 0x0);
    MrcCall->MrcSetMem ((UINT8 *) CccIndexResult, sizeof (CccIndexResult), 0x0);
    MrcCall->MrcSetMem ((UINT8 *) DataIndexResult, sizeof (DataIndexResult), 0x0);
    // Init DutyCycle group data to defaults for current iteration.
    MrcCall->MrcSetMem ((UINT8 *) DutyCycleGrpData, sizeof (DutyCycleGrpData), 0);
    MrcDutyCycleGrpDataInit(MrcData, DutyCycleGrpData);

    for (DcdGroup = 0; DcdGroup < DcdGroupMax; DcdGroup++) {

      ValidCompIndexMask = MrcGetDccIndexMask(MrcData, DcdGroup, DccFubComp);
      ValidCccIndexMask  = MrcGetDccIndexMask(MrcData, DcdGroup, DccFubCcc);
      ValidDataIndexMask = MrcGetDccIndexMask(MrcData, DcdGroup, DccFubData);

      for (DccIndex = 0; DccIndex < MAX_DCC_INDEX; DccIndex++) {
        // Find all failing DCD groups, Lanes, partitions.
        ValidCompIndex = FALSE;
        ValidCccIndex  = FALSE;
        ValidDataIndex = FALSE;
        FubMask        = 0;

        if ((((CompFailMask[DcdGroup] & ValidCompIndexMask) >> DccIndex) & MRC_BIT0) != 0) {
          ValidCompIndex  = TRUE;
          GlobalPass      = FALSE;
          FubMask         |= DccFubComp;
        }

        for (CccInst = 0; CccInst < MRC_NUM_CCC_INSTANCES; CccInst++) {
          DdrIoTranslateCccToMcChannel (MrcData, CccInst, &Controller, &Channel);
          if (!MrcChannelExist (MrcData, Controller, Channel)) {
            continue;
          }
          if ((((CccFailMask[DcdGroup][CccInst] & ValidCccIndexMask) >> DccIndex) & MRC_BIT0) != 0) {
            ValidCccIndex = TRUE;
            GlobalPass    = FALSE;
            FubMask       |= DccFubCcc;
          }
        }

        for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
          for (Channel = 0; Channel < Outputs->MaxChannels; Channel++) {
            if (!MrcChannelExist (MrcData, Controller, Channel)) {
              continue;
            }
            for (Strobe = 0; Strobe < Outputs->SdramCount; Strobe++) {
              if ((((DataFailMask[DcdGroup][Controller][Channel][Strobe] & ValidDataIndexMask) >> DccIndex) & MRC_BIT0) != 0) {
                ValidDataIndex = TRUE;
                GlobalPass     = FALSE;
                FubMask       |= DccFubData;
              }
            } // Strobe
          } // Channel
        } // Controller

        // Measure duty cycles of all failing instances until current iteration.
        if ((ValidCompIndex == TRUE) || (ValidCccIndex == TRUE) || (ValidDataIndex == TRUE)) {
          CompDutyCycle = 0;
          MrcCall->MrcSetMem ((UINT8 *) CccDutyCycle, sizeof (CccDutyCycle), 0);
          MrcCall->MrcSetMem ((UINT8 *) DataDutyCycle, sizeof (DataDutyCycle), 0);
          MrcGetDccIndexDutyCycle (MrcData, DcdGroup, FubMask, DccIndex, &CompDutyCycle, CccDutyCycle, DataDutyCycle);
        }

        // Parse the duty cycle values check if values are within limits for all COMP/CCC/DATA partitions.
        if (ValidCompIndex) {
          if ((((CompFailMask[DcdGroup] & ValidCompIndexMask) >> DccIndex) & MRC_BIT0) != 0) {
            DutyCycle   = CompDutyCycle;
            FinalCompDutyCycle[DcdGroup][DccIndex] = DutyCycle;
            DcLimitFail = MrcFindDcLimits (MrcData, DcdGroup, DccFubComp, DccIndex, "Comp0", DutyCycle, DutyCycleGrpData);
            if (DcLimitFail == TRUE) {
              CompIndexResult[DcdGroup] |= (MRC_BIT0 << DccIndex);
            }
          }
        }

        if (ValidCccIndex) {
          for (CccInst = 0; CccInst < MRC_NUM_CCC_INSTANCES; CccInst++) {
            DdrIoTranslateCccToMcChannel (MrcData, CccInst, &Controller, &Channel);
            if (!MrcChannelExist (MrcData, Controller, Channel)) {
              continue;
            }
            if ((((CccFailMask[DcdGroup][CccInst] & ValidCccIndexMask) >> DccIndex) & MRC_BIT0) != 0) {
              DutyCycle = CccDutyCycle[CccInst];
              FinalCccDutyCycle[DcdGroup][DccIndex][CccInst] = DutyCycle;
              MrcSPrintf (MrcData, PartitionStr, sizeof (PartitionStr), "CccC%u", CccInst);
              DcLimitFail = MrcFindDcLimits (MrcData, DcdGroup, DccFubCcc, DccIndex, PartitionStr, DutyCycle, DutyCycleGrpData);
              if (DcLimitFail == TRUE) {
                CccIndexResult[DcdGroup][CccInst] |= (MRC_BIT0 << DccIndex);
              }
            }
          }
        }

        if (ValidDataIndex) {
          for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
            for (Channel = 0; Channel < Outputs->MaxChannels; Channel++) {
              if (!MrcChannelExist (MrcData, Controller, Channel)) {
                continue;
              }
              for (Strobe = 0; Strobe < Outputs->SdramCount; Strobe++) {
                if ((((DataFailMask[DcdGroup][Controller][Channel][Strobe] & ValidDataIndexMask) >> DccIndex) & MRC_BIT0) != 0) {
                  DutyCycle = DataDutyCycle[Controller][Channel][Strobe];
                  FinalDataDutyCycle[DcdGroup][DccIndex][Controller][Channel][Strobe] = DutyCycle;
                  MrcSPrintf (MrcData, PartitionStr, sizeof (PartitionStr), "M%uC%uS%u", Controller, Channel, Strobe);
                  DcLimitFail = MrcFindDcLimits (MrcData, DcdGroup, DccFubData, DccIndex, PartitionStr, DutyCycle, DutyCycleGrpData);
                  if (DcLimitFail == TRUE) {
                    DataIndexResult[DcdGroup][Controller][Channel][Strobe] |= (MRC_BIT0 << DccIndex);
                  }
                }
              } // Strobe
            } // Channel
          } // Controller
        }

#ifdef MRC_DEBUG_PRINT
        // Print DutyCycle for current DCD Group/Lane.
        if ((ValidCccIndex == TRUE) || (ValidDataIndex == TRUE) || (ValidCompIndex == TRUE)) {
          if (PrintHeader == FALSE) {
            PrintHeader = TRUE;
            PrintDutyCycle (MrcData, DcdGroup, DccIndex, CompDutyCycle, CccDutyCycle, DataDutyCycle,  PrintHeader);
          }
          PrintDutyCycle (MrcData, DcdGroup, DccIndex, CompDutyCycle, CccDutyCycle, DataDutyCycle, FALSE);
        }
#endif
      }
    }

    // Exit loop if all DCD Group/Lane are passed.
    if (GlobalPass == TRUE) {
      break;
    }

#ifdef MRC_DEBUG_PRINT
    // Print Duty Cycles group data.
    MrcPrintDutyCycleGrpData (MrcData, DutyCycleGrpData);
#endif

    // Find DCC Lane masks of all DCD Groups which needs rerun DCC calibration algorithm.
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "DCC Instances to be rerun\n");
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "DCD Groups  |");

    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "  Comp0 ||");
    for (CccInst = 0; CccInst < MRC_NUM_CCC_INSTANCES; CccInst++) {
      MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "  CccC%u |", CccInst);
    }

    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "|");

    for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
      for (Channel = 0; Channel < Outputs->MaxChannels; Channel++) {
        for (Strobe = 0; Strobe < Outputs->SdramCount; Strobe++) {
          MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, " M%uC%uS%u |", Controller, Channel, Strobe);
        } // Strobe
      } // Channel
    } // Controller
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\n");

    for (DcdGroup = 0; DcdGroup < DcdGroupMax; DcdGroup++) {
      MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "%11s |", gDcdGrpNameStr[DcdGroup]);

      // Update the FailMask from the results from current run.
      // Rerun SRZ Phase0, Phase90 and Quad if Phase0/Phase90 are not converged.
      CompIndexResult[DcdGroup] &= CompFailMask[DcdGroup];
      CompFailMask[DcdGroup]     = CompIndexResult[DcdGroup];
      MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, " 0x%04X ||", CompIndexResult[DcdGroup]);

      for (CccInst = 0; CccInst < MRC_NUM_CCC_INSTANCES; CccInst++) {
        CccIndexResult[DcdGroup][CccInst] &= CccFailMask[DcdGroup][CccInst];
        CccFailMask[DcdGroup][CccInst]     = CccIndexResult[DcdGroup][CccInst];
        if (((DcdGroup == DCD_GROUP_SRZ) || (DcdGroup == DCD_GROUP_SRZ_PH90)) && Gear4) {
          CccFailMask[DCD_GROUP_SRZ][CccInst]         |= CccIndexResult[DcdGroup][CccInst];
          CccIndexResult[DCD_GROUP_SRZ][CccInst]      |= CccIndexResult[DcdGroup][CccInst];
          CccFailMask[DCD_GROUP_SRZ_PH90][CccInst]    |= CccIndexResult[DcdGroup][CccInst];
          CccIndexResult[DCD_GROUP_SRZ_PH90][CccInst] |= CccIndexResult[DcdGroup][CccInst];
          CccFailMask[DCD_GROUP_SRZ_QUAD][CccInst]    |= CccIndexResult[DcdGroup][CccInst];
          CccIndexResult[DCD_GROUP_SRZ_QUAD][CccInst] |= CccIndexResult[DcdGroup][CccInst];
        }
        MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, " 0x%04X |", CccIndexResult[DcdGroup][CccInst]);
      }
      MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "|");

      for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
        for (Channel = 0; Channel < Outputs->MaxChannels; Channel++) {
          for (Strobe = 0; Strobe < Outputs->SdramCount; Strobe++) {
            DataIndexResult[DcdGroup][Controller][Channel][Strobe] &= DataFailMask[DcdGroup][Controller][Channel][Strobe];
            DataFailMask[DcdGroup][Controller][Channel][Strobe]     = DataIndexResult[DcdGroup][Controller][Channel][Strobe];
            if (((DcdGroup == DCD_GROUP_SRZ) || (DcdGroup == DCD_GROUP_SRZ_PH90)) && Gear4) {
              DataFailMask[DCD_GROUP_SRZ][Controller][Channel][Strobe]         |= DataIndexResult[DcdGroup][Controller][Channel][Strobe];
              DataIndexResult[DCD_GROUP_SRZ][Controller][Channel][Strobe]      |= DataIndexResult[DcdGroup][Controller][Channel][Strobe];
              DataFailMask[DCD_GROUP_SRZ_PH90][Controller][Channel][Strobe]    |= DataIndexResult[DcdGroup][Controller][Channel][Strobe];
              DataIndexResult[DCD_GROUP_SRZ_PH90][Controller][Channel][Strobe] |= DataIndexResult[DcdGroup][Controller][Channel][Strobe];
              DataFailMask[DCD_GROUP_SRZ_QUAD][Controller][Channel][Strobe]    |= DataIndexResult[DcdGroup][Controller][Channel][Strobe];
              DataIndexResult[DCD_GROUP_SRZ_QUAD][Controller][Channel][Strobe] |= DataIndexResult[DcdGroup][Controller][Channel][Strobe];
            }
            MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, " 0x%04X |",  DataIndexResult[DcdGroup][Controller][Channel][Strobe]);
          } // Strobe
        } // Channel
      } // Controller

      MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\n");
    }

    // Update AFE LFSR setting to a new value for next run.
    MrcCall->MrcGetRandomNumber (&DcdRoLfsr);
    DcdRoLfsr &= 0x3FF;                                           // dcdrolfsr is 10 bit
    CccDccCtl2.Data = MrcReadCR (MrcData, CH0CCC_CR_DCCCTL2_REG);
    CccDccCtl2.Bits.dcdrolfsr = DcdRoLfsr;
    MrcWriteCrMulticast (MrcData, CCC_CR_DCCCTL2_REG, CccDccCtl2.Data);

    GetSetVal = DcdRoLfsr;
    MrcGetSetChStrb (MrcData, MAX_CONTROLLER, MAX_CHANNEL, MAX_SDRAM_IN_DIMM, GsmIocDcdRoLfsrData, WriteNoCache, &GetSetVal);

    CompDccCtl2.Data = MrcReadCR (MrcData, DDRPHY_COMP_CR_DCCCTL2_REG);
    CompDccCtl2.Bits.dcdrolfsr = DcdRoLfsr;
    MrcWriteCR (MrcData, DDRPHY_COMP_CR_DCCCTL2_REG, CompDccCtl2.Data);

    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\nIteration %u\n", DccCheckCounter + 1);
    // Run DCC calibration algorithm for all failing Groups/Lanes/Instances
    for (DcdGroup = 0; DcdGroup < DcdGroupMax; DcdGroup++) {
      MrcInitSwFsm (MrcData, DcdGroup, CompIndexResult[DcdGroup], CccIndexResult[DcdGroup], DataIndexResult[DcdGroup] );
    }
  }

#ifdef MRC_DEBUG_PRINT
  // Print final Duty cycle values, corresponding DCC codes, DCC cycle group data.
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\n-------------------------------------------------------------------------------------\n");
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Final DCC Duty Cycles after SwFsm is done\n");

  MrcCall->MrcSetMem ((UINT8 *) DutyCycleGrpData, sizeof (DutyCycleGrpData), 0);
  MrcDutyCycleGrpDataInit(MrcData, DutyCycleGrpData);
  PrintHeader = FALSE;
  for (DcdGroup = 0; DcdGroup < DcdGroupMax; DcdGroup++) {
    ValidCompIndexMask = MrcGetDccIndexMask(MrcData, DcdGroup, DccFubComp);
    ValidCccIndexMask  = MrcGetDccIndexMask(MrcData, DcdGroup, DccFubCcc);
    ValidDataIndexMask = MrcGetDccIndexMask(MrcData, DcdGroup, DccFubData);

    for (DccIndex = 0; DccIndex < MAX_DCC_INDEX; DccIndex++) {
      if (((ValidCompIndexMask >> DccIndex) & MRC_BIT0) || ((ValidCccIndexMask >> DccIndex) & MRC_BIT0) ||
          ((ValidDataIndexMask >> DccIndex) & MRC_BIT0)) {
        if (PrintHeader == FALSE) {
          PrintHeader = TRUE;
          PrintDutyCycle (MrcData, DcdGroup, DccIndex, FinalCompDutyCycle[0][0], FinalCccDutyCycle[0][0], FinalDataDutyCycle[0][0], PrintHeader);
        }
        PrintDutyCycle (MrcData, DcdGroup, DccIndex, FinalCompDutyCycle[DcdGroup][DccIndex], FinalCccDutyCycle[DcdGroup][DccIndex], FinalDataDutyCycle[DcdGroup][DccIndex], FALSE);
      }

      if (((ValidCompIndexMask >> DccIndex) & MRC_BIT0) != 0) {
        DutyCycle   = FinalCompDutyCycle[DcdGroup][DccIndex];
        DcLimitFail = MrcFindDcLimits (MrcData, DcdGroup, DccFubComp, DccIndex, "Comp0", DutyCycle, DutyCycleGrpData);
      }

      if (((ValidCccIndexMask >> DccIndex) & MRC_BIT0) != 0) {
        for (CccInst = 0; CccInst < MRC_NUM_CCC_INSTANCES; CccInst++) {
          DdrIoTranslateCccToMcChannel (MrcData, CccInst, &Controller, &Channel);
          if (!MrcChannelExist (MrcData, Controller, Channel)) {
            continue;
          }
          DutyCycle = FinalCccDutyCycle[DcdGroup][DccIndex][CccInst];
          MrcSPrintf (MrcData, PartitionStr, sizeof (PartitionStr), "CccC%u", CccInst);
          DcLimitFail = MrcFindDcLimits (MrcData, DcdGroup, DccFubCcc, DccIndex, PartitionStr, DutyCycle, DutyCycleGrpData);
        }
      }

      if (((ValidDataIndexMask >> DccIndex) & MRC_BIT0) != 0) {
        for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
          for (Channel = 0; Channel < Outputs->MaxChannels; Channel++) {
            if (!MrcChannelExist (MrcData, Controller, Channel)) {
              continue;
            }
            for (Strobe = 0; Strobe < Outputs->SdramCount; Strobe++) {
              DutyCycle = FinalDataDutyCycle[DcdGroup][DccIndex][Controller][Channel][Strobe];
              MrcSPrintf (MrcData, PartitionStr, sizeof (PartitionStr), "M%uC%uS%u", Controller, Channel, Strobe);
              DcLimitFail = MrcFindDcLimits (MrcData, DcdGroup, DccFubData, DccIndex, PartitionStr, DutyCycle, DutyCycleGrpData);
            } // Strobe
          } // Channel
        } // Controller
      }
    }
  }
  DumpDccCodes (MrcData);
  MrcPrintDutyCycleGrpData (MrcData, DutyCycleGrpData);
  MRC_DEBUG_MSG(Debug, MSG_LEVEL_NOTE, "-------------------------------------------------------------------------------------\n");
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "----- DCC Optimize End -----\n");
#endif
}
