#ifndef __MrcMcRegisterStructAdl3xxx_h__
#define __MrcMcRegisterStructAdl3xxx_h__
/** @file
  This file was automatically generated. Modify at your own risk.
  Note that no error checking is done in these functions so ensure that the correct values are passed.

@copyright
  Copyright (c) 2010 - 2021 Intel Corporation. All rights reserved
  This software and associated documentation (if any) is furnished
  under a license and may only be used or copied in accordance
  with the terms of the license. Except as permitted by the
  license, no part of this software or documentation may be
  reproduced, stored in a retrieval system, or transmitted in any
  form or by any means without the express written consent of
  Intel Corporation.
  This file contains an 'Intel Peripheral Driver' and is uniquely
  identified as "Intel Reference Module" and is licensed for Intel
  CPUs and chipsets under the terms of your license agreement with
  Intel or your vendor. This file may be modified by the user, subject
  to additional terms of the license agreement.

@par Specification Reference:
**/

#pragma pack(push, 1)

typedef DDRVTT0_CR_DDREARLY_RSTRDONE_STRUCT DATA6_GLOBAL_CR_DDREARLYCR_STRUCT;

typedef DATA0_GLOBAL_CR_IOLVRCTL0_STRUCT DATA6_GLOBAL_CR_IOLVRCTL0_STRUCT;

typedef DATA0_GLOBAL_CR_PGATE_PGCOMBOCTRL_STRUCT DATA6_GLOBAL_CR_PGATE_PGCOMBOCTRL_STRUCT;

typedef DATA0_GLOBAL_CR_VTTDRVSEGCTL_STRUCT DATA6_GLOBAL_CR_VTTDRVSEGCTL_STRUCT;

typedef DATA0_GLOBAL_CR_VTTDRVSEGCTL1_STRUCT DATA6_GLOBAL_CR_VTTDRVSEGCTL1_STRUCT;

typedef DATA0_GLOBAL_CR_VIEWCTL_STRUCT DATA6_GLOBAL_CR_VIEWCTL_STRUCT;

typedef DATA0_GLOBAL_CR_VIEWMUX_PGCOMBOCTRL_STRUCT DATA6_GLOBAL_CR_VIEWMUX_PGCOMBOCTRL_STRUCT;

typedef DATA0_GLOBAL_CR_PMTIMER0_STRUCT DATA6_GLOBAL_CR_PMTIMER0_STRUCT;

typedef DATA0_GLOBAL_CR_PMTIMER1_STRUCT DATA6_GLOBAL_CR_PMTIMER1_STRUCT;

typedef DDRVTT0_CR_DDREARLY_RSTRDONE_STRUCT DATA7_GLOBAL_CR_DDREARLYCR_STRUCT;

typedef DATA0_GLOBAL_CR_IOLVRCTL0_STRUCT DATA7_GLOBAL_CR_IOLVRCTL0_STRUCT;

typedef DATA0_GLOBAL_CR_PGATE_PGCOMBOCTRL_STRUCT DATA7_GLOBAL_CR_PGATE_PGCOMBOCTRL_STRUCT;

typedef DATA0_GLOBAL_CR_VTTDRVSEGCTL_STRUCT DATA7_GLOBAL_CR_VTTDRVSEGCTL_STRUCT;

typedef DATA0_GLOBAL_CR_VTTDRVSEGCTL1_STRUCT DATA7_GLOBAL_CR_VTTDRVSEGCTL1_STRUCT;

typedef DATA0_GLOBAL_CR_VIEWCTL_STRUCT DATA7_GLOBAL_CR_VIEWCTL_STRUCT;

typedef DATA0_GLOBAL_CR_VIEWMUX_PGCOMBOCTRL_STRUCT DATA7_GLOBAL_CR_VIEWMUX_PGCOMBOCTRL_STRUCT;

typedef DATA0_GLOBAL_CR_PMTIMER0_STRUCT DATA7_GLOBAL_CR_PMTIMER0_STRUCT;

typedef DATA0_GLOBAL_CR_PMTIMER1_STRUCT DATA7_GLOBAL_CR_PMTIMER1_STRUCT;
typedef union {
  struct {
    UINT32 iolvrseg_cmn_lvrbypass                  :  1;  // Bits 0:0
    UINT32 iolvrseg_cmn_gndsel                     :  1;  // Bits 1:1
    UINT32 iolvrseg_cmn_codelvrleg                 :  8;  // Bits 9:2
    UINT32 iolvrseg_cmn_codeffleg                  :  8;  // Bits 17:10
    UINT32 iolvrseg_cmn_enlvrleg                   :  1;  // Bits 18:18
    UINT32 iolvrseg_cmn_enffleg                    :  1;  // Bits 19:19
    UINT32 iolvr_global_bonus                      :  12;  // Bits 31:20
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} CCC0_GLOBAL_IOLVRCTL_STRUCT;
typedef union {
  struct {
    UINT32 viewmxdig_partobsen                     :  2;  // Bits 1:0
    UINT32 viewmxdig_cmn_viewdigcbbselport1        :  4;  // Bits 5:2
    UINT32 viewmxdig_cmn_viewdigcbbselport0        :  4;  // Bits 9:6
    UINT32 viewmxana_cmn_viewanasel                :  3;  // Bits 12:10
    UINT32 viewmxana_cmn_viewanaen                 :  1;  // Bits 13:13
    UINT32 reserved0                               :  18;  // Bits 31:14
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} CCC0_GLOBAL_CR_VIEWCT_STRUCT;
typedef union {
  struct {
    UINT32 reserved2                               :  16;  // Bits 15:0
    UINT32 reserved1                               :  16;  // Bits 31:16
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} CCC0_GLOBAL_IOLVRSLV_CTRL_STRUCT;

typedef DDRVTT0_CR_DDREARLY_RSTRDONE_STRUCT CCC0_GLOBAL_CR_DDREARLYCR_STRUCT;
typedef union {
  struct {
    UINT32 viewdig_cmn_viewdigport0sel_top         :  4;  // Bits 3:0
    UINT32 viewdig_cmn_viewdigport1sel_top         :  4;  // Bits 7:4
    UINT32 viewmxdig_cmn_viewdigcbbselport0_top    :  4;  // Bits 11:8
    UINT32 viewmxdig_cmn_viewdigcbbselport1_top    :  4;  // Bits 15:12
    UINT32 rxbias_cmn_viewanabiassel_top           :  3;  // Bits 18:16
    UINT32 mdll_cmn_viewanaen_top                  :  1;  // Bits 19:19
    UINT32 viewmxana_cmn_viewanasel_top            :  3;  // Bits 22:20
    UINT32 rxvref_cmn_viewanaen_top                :  1;  // Bits 23:23
    UINT32 rxbias_cmn_viewanaen_top                :  1;  // Bits 24:24
    UINT32 viewmxana_cmn_viewanaen_top1            :  1;  // Bits 25:25
    UINT32 viewmxana_cmn_viewanaen_top0            :  1;  // Bits 26:26
    UINT32 iolvrseg_cmn_viewanaen_top              :  1;  // Bits 27:27
    UINT32 rxbias_cmn_vrefsel_top                  :  4;  // Bits 31:28
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} CCC0_GLOBAL_CR_VIEWCTL_STRUCT;
typedef union {
  struct {
    UINT32 rxvref_cmn_ddrenvrefgenweakres_top      :  1;  // Bits 0:0
    UINT32 rxvref_cmn_ddrlevshftbypasspmos_top     :  1;  // Bits 1:1
    UINT32 rxvref_cmn_ddrvrefgenenable_top         :  1;  // Bits 2:2
    UINT32 rxvref_cmn_selvddsupply_top             :  1;  // Bits 3:3
    UINT32 rxvref_cmn_selvsxhi_top                 :  1;  // Bits 4:4
    UINT32 rxvref_cmn_ddrvrefctrl_top1             :  9;  // Bits 13:5
    UINT32 rxvref_cmn_ddrvrefctrl_top0             :  9;  // Bits 22:14
    UINT32 rxall_cmn_rxlpddrmode_top               :  2;  // Bits 24:23
    UINT32 cccrxflops_cmn_cccrxreset_top           :  1;  // Bits 25:25
    UINT32 rxbias_cmn_biasicomp_top                :  4;  // Bits 29:26
    UINT32 rxbias_cmn_tailctl_top                  :  2;  // Bits 31:30
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} CCC0_GLOBAL_CR_CCCDRXVREF_STRUCT;
typedef union {
  struct {
    UINT32 cccglobal_cmn_bonus_0                   :  32;  // Bits 31:0
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} CCC0_GLOBAL_BONUS_0_STRUCT;
typedef union {
  struct {
    UINT32 cccglobal_cmn_bonus_1                   :  32;  // Bits 31:0
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} CCC0_GLOBAL_BONUS_1_STRUCT;
typedef union {
  struct {
    UINT32 pmclkgatedisable                        :  1;  // Bits 0:0
    UINT32 rampenable_ovren                        :  1;  // Bits 1:1
    UINT32 rampenable_ovrval                       :  1;  // Bits 2:2
    UINT32 initdone_ovren                          :  1;  // Bits 3:3
    UINT32 init_done_ovrval                        :  1;  // Bits 4:4
    UINT32 iobufact_ovren                          :  1;  // Bits 5:5
    UINT32 iobufact_ovrval                         :  1;  // Bits 6:6
    UINT32 clkalign_ovren                          :  1;  // Bits 7:7
    UINT32 start_clkalign_ovrval                   :  1;  // Bits 8:8
    UINT32 clkalignrst_ovrval                      :  1;  // Bits 9:9
    UINT32 func_rst_ovren                          :  1;  // Bits 10:10
    UINT32 func_rst_ovrval                         :  1;  // Bits 11:11
    UINT32 pien_ovren                              :  1;  // Bits 12:12
    UINT32 pien_ovrval                             :  1;  // Bits 13:13
    UINT32 dllen_ovren                             :  1;  // Bits 14:14
    UINT32 dllen_ovrval                            :  1;  // Bits 15:15
    UINT32 ldoen_ovrval                            :  1;  // Bits 16:16
    UINT32 DdrxxClkGoodQnnnH_ovren                 :  1;  // Bits 17:17
    UINT32 DdrxxClkGoodQnnnH_ovrval                :  1;  // Bits 18:18
    UINT32 Vsshipowergood_ovren                    :  1;  // Bits 19:19
    UINT32 Vsshipowergood_ovrval                   :  1;  // Bits 20:20
    UINT32 clkalign_complete_ovren                 :  1;  // Bits 21:21
    UINT32 clkalign_complete_ovrval                :  1;  // Bits 22:22
    UINT32 mdlllock_ovren                          :  1;  // Bits 23:23
    UINT32 mdlllock_ovrval                         :  1;  // Bits 24:24
    UINT32 compupdatedone_ovren                    :  1;  // Bits 25:25
    UINT32 compupdatedone_ovrval                   :  1;  // Bits 26:26
    UINT32 scr_gvblock_ovrride                     :  1;  // Bits 27:27
    UINT32 Spare                                   :  4;  // Bits 31:28
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} CCC0_GLOBAL_CR_PM_FSM_OVRD_0_STRUCT;

typedef CCC0_GLOBAL_CR_PM_FSM_OVRD_0_STRUCT CCC0_GLOBAL_CR_PM_FSM_OVRD_1_STRUCT;
typedef union {
  struct {
    UINT32 Spare                                   :  14;  // Bits 13:0
    UINT32 ccctop0_dllcodepi_offset                :  4;  // Bits 17:14
    UINT32 ccctop0_dllcodewl_offset                :  4;  // Bits 21:18
    UINT32 ccctop0_dllbwsel_offset                 :  4;  // Bits 25:22
    UINT32 ccctop0_vdlllock_offset                 :  6;  // Bits 31:26
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} CCC0_GLOBAL_CR_COMP_OFFSET_0_STRUCT;
typedef union {
  struct {
    UINT32 Spare                                   :  14;  // Bits 13:0
    UINT32 ccctop1_dllcodepi_offset                :  4;  // Bits 17:14
    UINT32 ccctop1_dllcodewl_offset                :  4;  // Bits 21:18
    UINT32 ccctop1_dllbwsel_offset                 :  4;  // Bits 25:22
    UINT32 ccctop1_vdlllock_offset                 :  6;  // Bits 31:26
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} CCC0_GLOBAL_CR_COMP_OFFSET_1_STRUCT;
typedef union {
  struct {
    UINT32 tcocomp_bonus_0                         :  2;  // Bits 1:0
    UINT32 ccctx_ccc7_txtcocompp_top0              :  5;  // Bits 6:2
    UINT32 ccctx_ccc7_txtcocompn_top0              :  5;  // Bits 11:7
    UINT32 ccctx_ccc6_txtcocompp_top0              :  5;  // Bits 16:12
    UINT32 ccctx_ccc6_txtcocompn_top0              :  5;  // Bits 21:17
    UINT32 ccctx_ccc5_txtcocompp_top0              :  5;  // Bits 26:22
    UINT32 ccctx_ccc5_txtcocompn_top0              :  5;  // Bits 31:27
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} CCC0_GLOBAL_CR_TCOCOMP_0_STRUCT;
typedef union {
  struct {
    UINT32 tcocomp_bonus_1                         :  2;  // Bits 1:0
    UINT32 ccctx_ccc7_txtcocompp_top1              :  5;  // Bits 6:2
    UINT32 ccctx_ccc7_txtcocompn_top1              :  5;  // Bits 11:7
    UINT32 ccctx_ccc6_txtcocompp_top1              :  5;  // Bits 16:12
    UINT32 ccctx_ccc6_txtcocompn_top1              :  5;  // Bits 21:17
    UINT32 ccctx_ccc5_txtcocompp_top1              :  5;  // Bits 26:22
    UINT32 ccctx_ccc5_txtcocompn_top1              :  5;  // Bits 31:27
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} CCC0_GLOBAL_CR_TCOCOMP_1_STRUCT;
typedef union {
  struct {
    UINT32 tcocomp_bonus_2                         :  12;  // Bits 11:0
    UINT32 ccctx_ccc8_txtcocompp_top1              :  5;  // Bits 16:12
    UINT32 ccctx_ccc8_txtcocompn_top1              :  5;  // Bits 21:17
    UINT32 ccctx_ccc8_txtcocompp_top0              :  5;  // Bits 26:22
    UINT32 ccctx_ccc8_txtcocompn_top0              :  5;  // Bits 31:27
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} CCC0_GLOBAL_CR_TCOCOMP_2_STRUCT;
typedef union {
  struct {
    UINT32 scr_init_done_timer_val                 :  4;  // Bits 3:0
    UINT32 scr_iobufacten_timer_val                :  4;  // Bits 7:4
    UINT32 scr_start_clkalign_timer_val            :  2;  // Bits 9:8
    UINT32 scr_clkalignrsten_timer_val             :  3;  // Bits 12:10
    UINT32 scr_dll_en_timer_val                    :  10;  // Bits 22:13
    UINT32 scr_ldoen_timer_val                     :  3;  // Bits 25:23
    UINT32 scr_funcrst_timer_val                   :  4;  // Bits 29:26
    UINT32 Spare                                   :  2;  // Bits 31:30
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} CCC0_GLOBAL_CR_PMTIMER0_STRUCT;
typedef union {
  struct {
    UINT32 scr_initdone_dis_timer_val              :  2;  // Bits 1:0
    UINT32 scr_ldo_dis_timer_val                   :  2;  // Bits 3:2
    UINT32 scr_dll_dis_timer_val                   :  2;  // Bits 5:4
    UINT32 scr_pi_dis_timer_val                    :  2;  // Bits 7:6
    UINT32 scr_clkalignrst_dis_timer_val           :  2;  // Bits 9:8
    UINT32 scr_start_clkalign_dis_timer_val        :  2;  // Bits 11:10
    UINT32 scr_funcrst_dis_timer_val               :  2;  // Bits 13:12
    UINT32 scr_iobuf_dis_timer_val                 :  4;  // Bits 17:14
    UINT32 scr_pien_timer_val                      :  11;  // Bits 28:18
    UINT32 spare                                   :  3;  // Bits 31:29
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} CCC0_GLOBAL_CR_PMTIMER1_STRUCT;
typedef union {
  struct {
    UINT32 cs_setupdly                             :  3;  // Bits 2:0
    UINT32 cs_updtwidth                            :  5;  // Bits 7:3
    UINT32 cs_holddly                              :  6;  // Bits 13:8
    UINT32 ca_setupdly                             :  3;  // Bits 16:14
    UINT32 ca_updtwidth                            :  3;  // Bits 19:17
    UINT32 ca_holddly                              :  4;  // Bits 23:20
    UINT32 spare                                   :  8;  // Bits 31:24
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} CCC0_GLOBAL_CR_COMPUPDT_DLY0_STRUCT;
typedef union {
  struct {
    UINT32 ck_setupdly                             :  3;  // Bits 2:0
    UINT32 ck_updtwidth                            :  5;  // Bits 7:3
    UINT32 ck_holddly                              :  6;  // Bits 13:8
    UINT32 compupdtovr_sel                         :  15;  // Bits 28:14
    UINT32 spare                                   :  3;  // Bits 31:29
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} CCC0_GLOBAL_CR_COMPUPDT_DLY1_STRUCT;

typedef CCC0_GLOBAL_IOLVRCTL_STRUCT CCC1_GLOBAL_IOLVRCTL_STRUCT;

typedef CCC0_GLOBAL_CR_VIEWCT_STRUCT CCC1_GLOBAL_CR_VIEWCT_STRUCT;

typedef CCC0_GLOBAL_IOLVRSLV_CTRL_STRUCT CCC1_GLOBAL_IOLVRSLV_CTRL_STRUCT;

typedef DDRVTT0_CR_DDREARLY_RSTRDONE_STRUCT CCC1_GLOBAL_CR_DDREARLYCR_STRUCT;

typedef CCC0_GLOBAL_CR_VIEWCTL_STRUCT CCC1_GLOBAL_CR_VIEWCTL_STRUCT;

typedef CCC0_GLOBAL_CR_CCCDRXVREF_STRUCT CCC1_GLOBAL_CR_CCCDRXVREF_STRUCT;

typedef CCC0_GLOBAL_BONUS_0_STRUCT CCC1_GLOBAL_BONUS_0_STRUCT;

typedef CCC0_GLOBAL_BONUS_1_STRUCT CCC1_GLOBAL_BONUS_1_STRUCT;

typedef CCC0_GLOBAL_CR_PM_FSM_OVRD_0_STRUCT CCC1_GLOBAL_CR_PM_FSM_OVRD_0_STRUCT;

typedef CCC0_GLOBAL_CR_PM_FSM_OVRD_0_STRUCT CCC1_GLOBAL_CR_PM_FSM_OVRD_1_STRUCT;

typedef CCC0_GLOBAL_CR_COMP_OFFSET_0_STRUCT CCC1_GLOBAL_CR_COMP_OFFSET_0_STRUCT;

typedef CCC0_GLOBAL_CR_COMP_OFFSET_1_STRUCT CCC1_GLOBAL_CR_COMP_OFFSET_1_STRUCT;

typedef CCC0_GLOBAL_CR_TCOCOMP_0_STRUCT CCC1_GLOBAL_CR_TCOCOMP_0_STRUCT;

typedef CCC0_GLOBAL_CR_TCOCOMP_1_STRUCT CCC1_GLOBAL_CR_TCOCOMP_1_STRUCT;

typedef CCC0_GLOBAL_CR_TCOCOMP_2_STRUCT CCC1_GLOBAL_CR_TCOCOMP_2_STRUCT;

typedef CCC0_GLOBAL_CR_PMTIMER0_STRUCT CCC1_GLOBAL_CR_PMTIMER0_STRUCT;

typedef CCC0_GLOBAL_CR_PMTIMER1_STRUCT CCC1_GLOBAL_CR_PMTIMER1_STRUCT;

typedef CCC0_GLOBAL_CR_COMPUPDT_DLY0_STRUCT CCC1_GLOBAL_CR_COMPUPDT_DLY0_STRUCT;

typedef CCC0_GLOBAL_CR_COMPUPDT_DLY1_STRUCT CCC1_GLOBAL_CR_COMPUPDT_DLY1_STRUCT;

typedef CCC0_GLOBAL_IOLVRCTL_STRUCT CCC2_GLOBAL_IOLVRCTL_STRUCT;

typedef CCC0_GLOBAL_CR_VIEWCT_STRUCT CCC2_GLOBAL_CR_VIEWCT_STRUCT;

typedef CCC0_GLOBAL_IOLVRSLV_CTRL_STRUCT CCC2_GLOBAL_IOLVRSLV_CTRL_STRUCT;

typedef DDRVTT0_CR_DDREARLY_RSTRDONE_STRUCT CCC2_GLOBAL_CR_DDREARLYCR_STRUCT;

typedef CCC0_GLOBAL_CR_VIEWCTL_STRUCT CCC2_GLOBAL_CR_VIEWCTL_STRUCT;

typedef CCC0_GLOBAL_CR_CCCDRXVREF_STRUCT CCC2_GLOBAL_CR_CCCDRXVREF_STRUCT;

typedef CCC0_GLOBAL_BONUS_0_STRUCT CCC2_GLOBAL_BONUS_0_STRUCT;

typedef CCC0_GLOBAL_BONUS_1_STRUCT CCC2_GLOBAL_BONUS_1_STRUCT;

typedef CCC0_GLOBAL_CR_PM_FSM_OVRD_0_STRUCT CCC2_GLOBAL_CR_PM_FSM_OVRD_0_STRUCT;

typedef CCC0_GLOBAL_CR_PM_FSM_OVRD_0_STRUCT CCC2_GLOBAL_CR_PM_FSM_OVRD_1_STRUCT;

typedef CCC0_GLOBAL_CR_COMP_OFFSET_0_STRUCT CCC2_GLOBAL_CR_COMP_OFFSET_0_STRUCT;

typedef CCC0_GLOBAL_CR_COMP_OFFSET_1_STRUCT CCC2_GLOBAL_CR_COMP_OFFSET_1_STRUCT;

typedef CCC0_GLOBAL_CR_TCOCOMP_0_STRUCT CCC2_GLOBAL_CR_TCOCOMP_0_STRUCT;

typedef CCC0_GLOBAL_CR_TCOCOMP_1_STRUCT CCC2_GLOBAL_CR_TCOCOMP_1_STRUCT;

typedef CCC0_GLOBAL_CR_TCOCOMP_2_STRUCT CCC2_GLOBAL_CR_TCOCOMP_2_STRUCT;

typedef CCC0_GLOBAL_CR_PMTIMER0_STRUCT CCC2_GLOBAL_CR_PMTIMER0_STRUCT;

typedef CCC0_GLOBAL_CR_PMTIMER1_STRUCT CCC2_GLOBAL_CR_PMTIMER1_STRUCT;

typedef CCC0_GLOBAL_CR_COMPUPDT_DLY0_STRUCT CCC2_GLOBAL_CR_COMPUPDT_DLY0_STRUCT;

typedef CCC0_GLOBAL_CR_COMPUPDT_DLY1_STRUCT CCC2_GLOBAL_CR_COMPUPDT_DLY1_STRUCT;

typedef CCC0_GLOBAL_IOLVRCTL_STRUCT CCC3_GLOBAL_IOLVRCTL_STRUCT;

typedef CCC0_GLOBAL_CR_VIEWCT_STRUCT CCC3_GLOBAL_CR_VIEWCT_STRUCT;

typedef CCC0_GLOBAL_IOLVRSLV_CTRL_STRUCT CCC3_GLOBAL_IOLVRSLV_CTRL_STRUCT;

typedef DDRVTT0_CR_DDREARLY_RSTRDONE_STRUCT CCC3_GLOBAL_CR_DDREARLYCR_STRUCT;

typedef CCC0_GLOBAL_CR_VIEWCTL_STRUCT CCC3_GLOBAL_CR_VIEWCTL_STRUCT;

typedef CCC0_GLOBAL_CR_CCCDRXVREF_STRUCT CCC3_GLOBAL_CR_CCCDRXVREF_STRUCT;

typedef CCC0_GLOBAL_BONUS_0_STRUCT CCC3_GLOBAL_BONUS_0_STRUCT;

typedef CCC0_GLOBAL_BONUS_1_STRUCT CCC3_GLOBAL_BONUS_1_STRUCT;

typedef CCC0_GLOBAL_CR_PM_FSM_OVRD_0_STRUCT CCC3_GLOBAL_CR_PM_FSM_OVRD_0_STRUCT;

typedef CCC0_GLOBAL_CR_PM_FSM_OVRD_0_STRUCT CCC3_GLOBAL_CR_PM_FSM_OVRD_1_STRUCT;

typedef CCC0_GLOBAL_CR_COMP_OFFSET_0_STRUCT CCC3_GLOBAL_CR_COMP_OFFSET_0_STRUCT;

typedef CCC0_GLOBAL_CR_COMP_OFFSET_1_STRUCT CCC3_GLOBAL_CR_COMP_OFFSET_1_STRUCT;

typedef CCC0_GLOBAL_CR_TCOCOMP_0_STRUCT CCC3_GLOBAL_CR_TCOCOMP_0_STRUCT;

typedef CCC0_GLOBAL_CR_TCOCOMP_1_STRUCT CCC3_GLOBAL_CR_TCOCOMP_1_STRUCT;

typedef CCC0_GLOBAL_CR_TCOCOMP_2_STRUCT CCC3_GLOBAL_CR_TCOCOMP_2_STRUCT;

typedef CCC0_GLOBAL_CR_PMTIMER0_STRUCT CCC3_GLOBAL_CR_PMTIMER0_STRUCT;

typedef CCC0_GLOBAL_CR_PMTIMER1_STRUCT CCC3_GLOBAL_CR_PMTIMER1_STRUCT;

typedef CCC0_GLOBAL_CR_COMPUPDT_DLY0_STRUCT CCC3_GLOBAL_CR_COMPUPDT_DLY0_STRUCT;

typedef CCC0_GLOBAL_CR_COMPUPDT_DLY1_STRUCT CCC3_GLOBAL_CR_COMPUPDT_DLY1_STRUCT;
typedef union {
  struct {
    UINT32 FLL_RATIO                               :  8;  // Bits 7:0
    UINT32 FREQ_CHANGE_REQ                         :  1;  // Bits 8:8
    UINT32 FLL_OUT_CLK_REQ                         :  1;  // Bits 9:9
    UINT32 FLL_ENABLE                              :  1;  // Bits 10:10
    UINT32 FLL_LDO_ENABLE                          :  1;  // Bits 11:11
    UINT32 BYPASS_AMPREF_FLT                       :  1;  // Bits 12:12
    UINT32 FLLVR_BYPASS                            :  1;  // Bits 13:13
    UINT32 DCO_EN_HR                               :  2;  // Bits 15:14
    UINT32 DCO_CB                                  :  3;  // Bits 18:16
    UINT32 DCO_IREFTUNE                            :  4;  // Bits 22:19
    UINT32 FASTRAMP_EN                             :  1;  // Bits 23:23
    UINT32 LKR_ALWAYS_ON                           :  1;  // Bits 24:24
    UINT32 FLL_OUT_CLK_REQ_OVRD_EN                 :  1;  // Bits 25:25
    UINT32 LDO_ENABLE_DLY_SEL                      :  2;  // Bits 27:26
    UINT32 FREQ_CHANGE_REQ_OVRD_EN                 :  1;  // Bits 28:28
    UINT32 LKR_STRENGTH_CFG                        :  1;  // Bits 29:29
    UINT32 FLL_LDO_ENABLE_OVRD_EN                  :  1;  // Bits 30:30
    UINT32 SELEXTREF                               :  1;  // Bits 31:31
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} FLL_CMD_CFG_REG_STRUCT;
typedef union {
  struct {
    UINT32 FAST_CAL_WINDOW_VAL                     :  3;  // Bits 2:0
    UINT32 SLOW_CAL_WINDOW_VAL                     :  3;  // Bits 5:3
    UINT32 WAIT2FINALLOCK_SEL                      :  2;  // Bits 7:6
    UINT32 SKIP_COARSE_CAL                         :  1;  // Bits 8:8
    UINT32 FINE_CAL_ENABLE                         :  1;  // Bits 9:9
    UINT32 RUNTIME_CAL                             :  1;  // Bits 10:10
    UINT32 CAL_THRESH_HI                           :  6;  // Bits 16:11
    UINT32 CAL_THRESH_LO                           :  6;  // Bits 22:17
    UINT32 DCO_ON_IN_OFF_STATE                     :  1;  // Bits 23:23
    UINT32 COARSE_CAL_TIME_SEL                     :  3;  // Bits 26:24
    UINT32 ISOLATION_MODE_ON                       :  1;  // Bits 27:27
    UINT32 LDO_VREFSEL                             :  4;  // Bits 31:28
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} FLL_STATIC_CFG_0_REG_STRUCT;
typedef union {
  struct {
    UINT32 COARSECAL_CNTR_EN                       :  4;  // Bits 3:0
    UINT32 FINECAL_CNTR_EN                         :  4;  // Bits 7:4
    UINT32 DELAY_FLLENABLE                         :  3;  // Bits 10:8
    UINT32 RCOMPENSATION_CFG                       :  2;  // Bits 12:11
    UINT32 REFCLK_DIVIDE_RATIO_SEL                 :  2;  // Bits 14:13
    UINT32 COMPUTE_LENGTH_SEL                      :  2;  // Bits 16:15
    UINT32 DAC_SETTLE_LENGTH_SEL                   :  2;  // Bits 18:17
    UINT32 MISC_CFG                                :  3;  // Bits 21:19
    UINT32 VREFSEL_FASTRAMP                        :  4;  // Bits 25:22
    UINT32 VSUPPLY_CFG                             :  2;  // Bits 27:26
    UINT32 REFCLK_INPUT_CLKGATE_OVRD               :  1;  // Bits 28:28
    UINT32 SEQUENCE_FSM_CLKGATE_OVRD               :  1;  // Bits 29:29
    UINT32 REQACK_FSM_CLKGATE_OVRD                 :  1;  // Bits 30:30
    UINT32 DCO_CODE_UPDATE_CLKGATE_OVRD            :  1;  // Bits 31:31
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} FLL_STATIC_CFG_1_REG_STRUCT;
typedef union {
  struct {
    UINT32 FLL_CORE_EN_OVRD                        :  1;  // Bits 0:0
    UINT32 REG_WR_DONE_OVRD_EN                     :  1;  // Bits 1:1
    UINT32 HV_PWR_GOOD_OVRD_EN                     :  1;  // Bits 2:2
    UINT32 DLY_COUNTERS_BYPASS_OVRD_EN             :  1;  // Bits 3:3
    UINT32 NODELAY_FLL_EN_OVRD_EN                  :  1;  // Bits 4:4
    UINT32 VIEWDIG_DFX_EN_CH0                      :  1;  // Bits 5:5
    UINT32 VIEWDIG_DFX_EN_CH1                      :  1;  // Bits 6:6
    UINT32 VIEWANA_SEL                             :  4;  // Bits 10:7
    UINT32 VIEWDIG_SEL_CH0                         :  4;  // Bits 14:11
    UINT32 VIEWDIG_SEL_CH1                         :  4;  // Bits 18:15
    UINT32 SINGLE_STEP_MODE_EN                     :  1;  // Bits 19:19
    UINT32 SINGLE_STEP_MODE_TRIGGER                :  1;  // Bits 20:20
    UINT32 RESERVED_DBG_0                          :  3;  // Bits 23:21
    UINT32 FLL_CORE_EN_OVRD_EN                     :  1;  // Bits 24:24
    UINT32 REG_WR_DONE_OVRD                        :  1;  // Bits 25:25
    UINT32 HV_PWR_GOOD_OVRD                        :  1;  // Bits 26:26
    UINT32 DLY_COUNTERS_BYPASS_OVRD                :  1;  // Bits 27:27
    UINT32 NODELAY_FLL_EN_OVRD                     :  1;  // Bits 28:28
    UINT32 RESERVED_DBG_1                          :  3;  // Bits 31:29
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} FLL_DEBUG_CFG_REG_STRUCT;
typedef union {
  struct {
    UINT32 VBIAS_CODE_2FLLCORE_OVRD                :  16;  // Bits 15:0
    UINT32 FORCE_OUTCLK_ON                         :  1;  // Bits 16:16
    UINT32 FORCE_OUTCLK_OFF                        :  1;  // Bits 17:17
    UINT32 RESERVED_DYN_0                          :  4;  // Bits 21:18
    UINT32 CAL_CODE_VALID_REG                      :  1;  // Bits 22:22
    UINT32 RESERVED_DYN_2                          :  1;  // Bits 23:23
    UINT32 RESERVED_DYN_3                          :  1;  // Bits 24:24
    UINT32 RESERVED_DYN_1                          :  7;  // Bits 31:25
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} FLL_DYNAMIC_CFG_REG_STRUCT;
typedef union {
  struct {
    UINT32 FLL_ENABLE                              :  1;  // Bits 0:0
    UINT32 DIST_RESET_B                            :  1;  // Bits 1:1
    UINT32 FLL_EARLY_LOCK                          :  1;  // Bits 2:2
    UINT32 FLL_FINAL_LOCK                          :  1;  // Bits 3:3
    UINT32 RAMP_DONE                               :  1;  // Bits 4:4
    UINT32 FREQ_CHANGE_REQ                         :  1;  // Bits 5:5
    UINT32 FREQ_CHANGE_DONE                        :  1;  // Bits 6:6
    UINT32 REG_WRITES_DONE                         :  1;  // Bits 7:7
    UINT32 VBIAS_CODE_2FLLCORE                     :  16;  // Bits 23:8
    UINT32 FLL_RATIO                               :  8;  // Bits 31:24
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} FLL_EXTIP_STAT_REG_STRUCT;
typedef union {
  struct {
    UINT32 FLL_REFCLK_REQ                          :  1;  // Bits 0:0
    UINT32 FLL_REFCLK_ACK                          :  1;  // Bits 1:1
    UINT32 FLL_OUTCLK_REQ                          :  1;  // Bits 2:2
    UINT32 FLL_OUTCLK_ACK                          :  1;  // Bits 3:3
    INT32  FLL_COUNTER_ERR                         :  12;  // Bits 15:4
    UINT32 FLL_COUNTER_SUM                         :  12;  // Bits 27:16
    UINT32 HV_PWR_GOOD                             :  1;  // Bits 28:28
    UINT32 LDOEN_DLY_CNTR_DONE                     :  1;  // Bits 29:29
    UINT32 FASTRAMPDONE_CNTR_DONE                  :  1;  // Bits 30:30
    UINT32 LDO_RAMP_TIMER_DONE                     :  1;  // Bits 31:31
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} FLL_DIAG_STAT_REG_STRUCT;
typedef union {
  struct {
    UINT32 FLL_COUNTER_OVF                         :  10;  // Bits 9:0
    UINT32 COUNTER_OVERFLOW_STICKY                 :  1;  // Bits 10:10
    UINT32 MEASUREMENT_ENABLE_DLY_VER              :  1;  // Bits 11:11
    UINT32 RESERVED_DIAG_STS_1                     :  20;  // Bits 31:12
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} FLL_DIAG_STAT_1_REG_STRUCT;
typedef union {
  struct {
    UINT32 LDO_FAST_RAMP_TIME_SEL                  :  2;  // Bits 1:0
    UINT32 RESERVED_STATIC_CFG_2                   :  30;  // Bits 31:2
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} FLL_STATIC_CFG_2_REG_STRUCT;
typedef union {
  struct {
    UINT32 RESERVED_DIAG_STS_2                     :  32;  // Bits 31:0
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} FLL_DIAG_STAT_2_REG_STRUCT;

typedef DDRVTT0_CR_DDREARLY_RSTRDONE_STRUCT DATA3_ECC_GLOBAL_CR_DDREARLYCR_STRUCT;

typedef DATA0_GLOBAL_CR_IOLVRCTL0_STRUCT DATA3_ECC_GLOBAL_CR_IOLVRCTL0_STRUCT;

typedef DATA0_GLOBAL_CR_PGATE_PGCOMBOCTRL_STRUCT DATA3_ECC_GLOBAL_CR_PGATE_PGCOMBOCTRL_STRUCT;

typedef DATA0_GLOBAL_CR_VTTDRVSEGCTL_STRUCT DATA3_ECC_GLOBAL_CR_VTTDRVSEGCTL_STRUCT;

typedef DATA0_GLOBAL_CR_VTTDRVSEGCTL1_STRUCT DATA3_ECC_GLOBAL_CR_VTTDRVSEGCTL1_STRUCT;

typedef DATA0_GLOBAL_CR_VIEWCTL_STRUCT DATA3_ECC_GLOBAL_CR_VIEWCTL_STRUCT;

typedef DATA0_GLOBAL_CR_VIEWMUX_PGCOMBOCTRL_STRUCT DATA3_ECC_GLOBAL_CR_VIEWMUX_PGCOMBOCTRL_STRUCT;

typedef DATA0_GLOBAL_CR_PMTIMER0_STRUCT DATA3_ECC_GLOBAL_CR_PMTIMER0_STRUCT;

typedef DATA0_GLOBAL_CR_PMTIMER1_STRUCT DATA3_ECC_GLOBAL_CR_PMTIMER1_STRUCT;

typedef DDRVTT0_CR_DDREARLY_RSTRDONE_STRUCT DATA4_ECC_GLOBAL_CR_DDREARLYCR_STRUCT;

typedef DATA0_GLOBAL_CR_IOLVRCTL0_STRUCT DATA4_ECC_GLOBAL_CR_IOLVRCTL0_STRUCT;

typedef DATA0_GLOBAL_CR_PGATE_PGCOMBOCTRL_STRUCT DATA4_ECC_GLOBAL_CR_PGATE_PGCOMBOCTRL_STRUCT;

typedef DATA0_GLOBAL_CR_VTTDRVSEGCTL_STRUCT DATA4_ECC_GLOBAL_CR_VTTDRVSEGCTL_STRUCT;

typedef DATA0_GLOBAL_CR_VTTDRVSEGCTL1_STRUCT DATA4_ECC_GLOBAL_CR_VTTDRVSEGCTL1_STRUCT;

typedef DATA0_GLOBAL_CR_VIEWCTL_STRUCT DATA4_ECC_GLOBAL_CR_VIEWCTL_STRUCT;

typedef DATA0_GLOBAL_CR_VIEWMUX_PGCOMBOCTRL_STRUCT DATA4_ECC_GLOBAL_CR_VIEWMUX_PGCOMBOCTRL_STRUCT;

typedef DATA0_GLOBAL_CR_PMTIMER0_STRUCT DATA4_ECC_GLOBAL_CR_PMTIMER0_STRUCT;

typedef DATA0_GLOBAL_CR_PMTIMER1_STRUCT DATA4_ECC_GLOBAL_CR_PMTIMER1_STRUCT;
typedef union {
  struct {
    UINT32 DdrCaSlwDlyBypass                       :  1;  // Bits 23:23
    UINT32 DdrCtlSlwDlyBypass                      :  1;  // Bits 24:24
    UINT32 DdrClkSlwDlyBypass                      :  1;  // Bits 25:25
  } BitsA0;
  struct {
    UINT32 TxEn                                    :  15;  // Bits 14:0
    UINT32 CCCMuxSelect                            :  2;  // Bits 16:15
    UINT32 PiEn                                    :  5;  // Bits 21:17
    UINT32 PiEnOvrd                                :  1;  // Bits 22:22
    UINT32 AltMuxLp5DEn                            :  1;  // Bits 23:23
    UINT32 Spare                                   :  2;  // Bits 25:24
    UINT32 Gear1                                   :  1;  // Bits 26:26
    UINT32 TimerXXClk                              :  3;  // Bits 29:27
    UINT32 KeepXXClkOn                             :  1;  // Bits 30:30
    UINT32 Gear4                                   :  1;  // Bits 31:31
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} CH2CCC_CR_DDRCRPINSUSED_STRUCT;
typedef union {
  struct {
    UINT32 IamclkP                                 :  4;  // Bits 3:0
    UINT32 ccc_bonus                               :  27;  // Bits 30:4
    UINT32 cccrst                                  :  1;  // Bits 31:31
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} CH2CCC_CR_CCCRSTCTL_STRUCT;
typedef union {
  struct {
    UINT32 mdll_cmn_pi0code                        :  8;  // Bits 7:0
    UINT32 reserved1                               :  8;  // Bits 15:8
    UINT32 mdll_cmn_pi1code                        :  10;  // Bits 25:16
    UINT32 reserved0                               :  6;  // Bits 31:26
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} CH2CCC_CR_PICODE0_STRUCT;
typedef union {
  struct {
    UINT32 mdll_cmn_pi2code                        :  10;  // Bits 9:0
    UINT32 reserved3                               :  6;  // Bits 15:10
    UINT32 mdll_cmn_pi3code                        :  10;  // Bits 25:16
    UINT32 reserved2                               :  6;  // Bits 31:26
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} CH2CCC_CR_PICODE1_STRUCT;
typedef union {
  struct {
    UINT32 mdll_cmn_pi4code                        :  10;  // Bits 9:0
    UINT32 reserved5                               :  6;  // Bits 15:10
    UINT32 mdll_cmn_pi5code                        :  8;  // Bits 23:16
    UINT32 reserved4                               :  8;  // Bits 31:24
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} CH2CCC_CR_PICODE2_STRUCT;
typedef union {
  struct {
    UINT32 mdll_cmn_pi6code                        :  10;  // Bits 9:0
    UINT32 reserved7                               :  6;  // Bits 15:10
    UINT32 mdll_cmn_pi7code                        :  8;  // Bits 23:16
    UINT32 reserved6                               :  8;  // Bits 31:24
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} CH2CCC_CR_PICODE3_STRUCT;
typedef union {
  struct {
    UINT32 mdll_cmn_turbocaptrim                   :  2;  // Bits 1:0
    UINT32 mdll_cmn_turboonstartup                 :  1;  // Bits 2:2
    UINT32 mdll_cmn_ldoffcodepi                    :  6;  // Bits 8:3
    UINT32 reserved8                               :  1;  // Bits 9:9
    UINT32 mdll_cmn_wlphgenin                      :  1;  // Bits 10:10
    UINT32 mdll_cmn_wlrdacholden                   :  1;  // Bits 11:11
    UINT32 mdll_cmn_weaklocken                     :  1;  // Bits 12:12
    UINT32 mdll_cmn_dllbypassen                    :  1;  // Bits 13:13
    UINT32 mdll_cmn_mdllengthsel                   :  2;  // Bits 15:14
    UINT32 mdll_cmn_phdeterr                       :  1;  // Bits 16:16
    UINT32 mdll_cmn_mdlllock                       :  1;  // Bits 17:17
    UINT32 mdll_cmn_pien                           :  8;  // Bits 25:18
    UINT32 mdll_cmn_mdllen                         :  1;  // Bits 26:26
    UINT32 mdll_cmn_extwlval                       :  1;  // Bits 27:27
    UINT32 mdll_cmn_extwlerr                       :  1;  // Bits 28:28
    UINT32 mdll_cmn_extwlen                        :  1;  // Bits 29:29
    UINT32 mdll_cmn_extfreezeval                   :  1;  // Bits 30:30
    UINT32 mdll_cmn_extfreezeen                    :  1;  // Bits 31:31
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} CH2CCC_CR_MDLLCTL0_STRUCT;
typedef union {
  struct {
    UINT32 mdll_cmn_picapsel                       :  3;  // Bits 2:0
    UINT32 mdll_cmn_phsdrvprocsel                  :  5;  // Bits 7:3
    UINT32 mdll_cmn_openloopbias                   :  1;  // Bits 8:8
    UINT32 mdll_cmn_vctlcaptrim                    :  2;  // Bits 10:9
    UINT32 mdll_cmn_vcdlbwsel                      :  6;  // Bits 16:11
    UINT32 mdll_cmn_vctldacforcen                  :  1;  // Bits 17:17
    UINT32 mdll_cmn_vctldaccode                    :  9;  // Bits 26:18
    UINT32 mdll_cmn_vctldaccompareen               :  1;  // Bits 27:27
    UINT32 Spare                                   :  4;  // Bits 31:28
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} CH2CCC_CR_MDLLCTL1_STRUCT;
typedef union {
  struct {
    UINT32 mdll_cmn_dcdtargclksel                  :  1;  // Bits 0:0
    UINT32 mdll_cmn_dccen                          :  1;  // Bits 1:1
    UINT32 mdll_cmn_dllldoen                       :  1;  // Bits 2:2
    UINT32 bonus_1                                 :  2;  // Bits 4:3
    UINT32 mdll_cmn_vctlcompoffsetcal              :  5;  // Bits 9:5
    UINT32 mdll_cmn_vcdldcccode                    :  4;  // Bits 13:10
    UINT32 mdll_cmn_ldoffcodelock                  :  8;  // Bits 21:14
    UINT32 mdll_cmn_ldoffcodewl                    :  6;  // Bits 27:22
    UINT32 spare                                   :  3;  // Bits 30:28
    UINT32 mdll_cmn_ldofbdivsel                    :  1;  // Bits 31:31
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} CH2CCC_CR_MDLLCTL2_STRUCT;
typedef union {
  struct {
    UINT32 mdll_cmn_ldopbiasctrl                   :  3;  // Bits 2:0
    UINT32 mdll_cmn_ldolocalvsshibypass            :  1;  // Bits 3:3
    UINT32 mdll_cmn_ldoforceen                     :  1;  // Bits 4:4
    UINT32 mdll_cmn_ldonbiasvrefcode               :  5;  // Bits 9:5
    UINT32 mdll_cmn_ldonbiasctrl                   :  4;  // Bits 13:10
    UINT32 mdll_cmn_rloadcomp                      :  6;  // Bits 19:14
    UINT32 mdll_cmn_vctldaccompareout              :  1;  // Bits 20:20
    UINT32 spare                                   :  9;  // Bits 29:21
    UINT32 mdll_cmn_vcdlcben                       :  2;  // Bits 31:30
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} CH2CCC_CR_MDLLCTL3_STRUCT;
typedef union {
  struct {
    UINT32                                         :  4;  // Bits 3:0
    UINT32 ccctx_cmn_txslewdlybypass               :  15;  // Bits 18:4
    UINT32 ccctx_cmn_txslewpreupdt                 :  1;  // Bits 19:19
    UINT32 ccctx_rgrp3_compupdtsyncen              :  1;  // Bits 20:20
    UINT32 ccctx_rgrp4_compupdtsyncen              :  1;  // Bits 21:21
    UINT32 ccctx_cmn_pwrdwncompupdt                :  1;  // Bits 22:22
    UINT32 afetx_cmn_txlocalvsshibyp               :  1;  // Bits 23:23
    UINT32 ccctx_cmn_ennmospup                     :  1;  // Bits 24:24
  } BitsA0;
  struct {
    UINT32 cccrx_cmn_rxmtailctl                    :  2;  // Bits 1:0
    UINT32 ccctx_cmn_extupdateen                   :  1;  // Bits 2:2
    UINT32 ccctx_cmn_extupdateenclk                :  1;  // Bits 3:3
    UINT32 ccctx_cmn_txslewpreupdt                 :  1;  // Bits 4:4
    UINT32 ccctx_rgrp3_compupdtsyncen              :  1;  // Bits 5:5
    UINT32 ccctx_rgrp4_compupdtsyncen              :  1;  // Bits 6:6
    UINT32 ccctx_cmn_pwrdwncompupdt                :  1;  // Bits 7:7
    UINT32 afetx_cmn_txlocalvsshibyp               :  1;  // Bits 8:8
    UINT32 ccctx_cmn_ennmospup                     :  1;  // Bits 9:9
    UINT32 ccctx_rgrp3_compupdtsyncen_ovr_sel      :  1;  // Bits 10:10
    UINT32 ccctx_rgrp4_compupdtsyncen_ovr_sel      :  1;  // Bits 11:11
    UINT32 Spare1                                  :  15; // Bits 26:12
    UINT32 ccctx_cmn_txeqcomp                      :  3;  // Bits 29:27
    UINT32 ccctx_cmn_txcompupdten                  :  1;  // Bits 30:30
    UINT32 ccctx_cmn_txcompupdtclken               :  1;  // Bits 31:31
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} CH2CCC_CR_COMP2_STRUCT;
typedef union {
  struct {
    UINT32 ccctx_cmn_compupdten                    :  15;  // Bits 14:0
    UINT32 ccctx_cmn_compupdtclken                 :  1;  // Bits 15:15
    UINT32 ccctx_cmn_compsyncreset                 :  1;  // Bits 16:16
    UINT32 imod_comp_ccctx_cmn_txtcocompp          :  5;  // Bits 21:17
    UINT32 imod_comp_ccctx_cmn_txtcocompn          :  5;  // Bits 26:22
    UINT32 DdrCaSlwDlyBypass                       :  1;  // Bits 27:27
    UINT32 DdrCtlSlwDlyBypass                      :  1;  // Bits 28:28
    UINT32 DdrClkSlwDlyBypass                      :  1;  // Bits 29:29
    UINT32 Spare                                   :  2;  // Bits 31:30
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} CH2CCC_CR_COMP3_STRUCT;
typedef union {
  struct {
    UINT32 ccctx_cmn_txpdnusevcciog                :  1;  // Bits 0:0
    UINT32 CtlVoltageSelect                        :  1;  // Bits 1:1
    UINT32 ClkVoltageSelect                        :  1;  // Bits 2:2
    UINT32 CaVoltageSelect                         :  1;  // Bits 3:3
    UINT32 CaPDPreDrvVccddq                        :  1;  // Bits 4:4
    UINT32 CtlPDPreDrvVccddq                       :  1;  // Bits 5:5
    UINT32 ClkPDPreDrvVccddq                       :  1;  // Bits 6:6
    UINT32 ccctx_cmn_txvsshiffstlegen              :  1;  // Bits 7:7
    UINT32 rxbias_cmn_biasen                       :  1;  // Bits 8:8
    UINT32 ccctx_cmn_ennbiasboost                  :  1;  // Bits 9:9
    UINT32 VssHiBypassVddqMode                     :  1;  // Bits 10:10
    UINT32 ccctx_cmn_txpupusevcciog                :  1;  // Bits 11:11
    UINT32 CkUseCtlComp                            :  1;  // Bits 12:12
    UINT32 CsUseCaComp                             :  1;  // Bits 13:13
    UINT32 ckecstxeqconstz                         :  1;  // Bits 14:14  // Adl_qo
    UINT32 ctltxeqconstz                           :  1;  // Bits 15:15  // Adl_qo
    UINT32 clktxeqconstz                           :  1;  // Bits 16:16  // Adl_qo
    UINT32 catxeqconstz                            :  1;  // Bits 17:17  // Adl_qo
    UINT32 Spare                                   :  14;  // Bits 31:18
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} CH2CCC_CR_DDRCRCCCVOLTAGEUSED_STRUCT;
typedef union {
  struct {
    UINT32 CtlScompOffset                          :  4;  // Bits 3:0
    UINT32 Spare1                                  :  4;  // Bits 7:4
    UINT32 CtlRcompDrvUpOffset                     :  4;  // Bits 11:8
    UINT32 CtlRcompDrvDownOffset                   :  4;  // Bits 15:12
    UINT32 CaScompOffset                           :  4;  // Bits 19:16
    UINT32 Spare2                                  :  4;  // Bits 23:20
    UINT32 CaRcompDrvUpOffset                      :  4;  // Bits 27:24
    UINT32 CaRcompDrvDownOffset                    :  4;  // Bits 31:28
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} CH2CCC_CR_DDRCRCTLCACOMPOFFSET_STRUCT;
typedef union {
  struct {
    UINT32 ClkScompOffset                          :  4;  // Bits 3:0
    UINT32 Spare1                                  :  4;  // Bits 7:4
    UINT32 ClkRcompDrvUpOffset                     :  4;  // Bits 11:8
    UINT32 ClkRcompDrvDownOffset                   :  4;  // Bits 15:12
    UINT32 CtlFFOffset                             :  4;  // Bits 19:16
    UINT32 CaFFOffset                              :  4;  // Bits 23:20
    UINT32 ClkFFOffset                             :  4;  // Bits 27:24
    UINT32 Spare                                   :  4;  // Bits 31:28
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} CH2CCC_CR_DDRCRVSSHICLKCOMPOFFSET_STRUCT;
typedef union {
  struct {
    UINT32 ccctx_ccc5_iamclkp                      :  1;  // Bits 0:0
    UINT32 ccctx_ccc6_iamclkp                      :  1;  // Bits 1:1
    UINT32 ccctx_ccc7_iamclkp                      :  1;  // Bits 2:2
    UINT32 ccctx_ccc8_iamclkp                      :  1;  // Bits 3:3
    UINT32 ccctx_rgrp3_gear4enclkdata              :  1;  // Bits 4:4
    UINT32 ccctx_rgrp4_gear4enclkdata              :  1;  // Bits 5:5
    UINT32 rxbias_cmn_rloadcomp                    :  6;  // Bits 11:6
    UINT32 alltx_cmn_tcoslewstatlegen              :  1;  // Bits 12:12
    UINT32 cccrx_cmn_rxdiffampen                   :  1;  // Bits 13:13
    UINT32 omod_func_cktop_cmn_bonus               :  4;  // Bits 17:14
    UINT32 omod_func_cccrxflops_cmn_bonus          :  1;  // Bits 18:18
    UINT32 cccrxflops_cmn_bonus                    :  2;  // Bits 20:19
    UINT32 pghub_cmn_iobufact                      :  1;  // Bits 21:21
    UINT32 pghub_cmn_iobufactclk                   :  1;  // Bits 22:22
    UINT32 ccctx_cmn_txvsshileakeren               :  1;  // Bits 23:23
    UINT32 ccctx_cmn_txvsshileakercomp             :  6;  // Bits 29:24
    UINT32 safemodeenable                          :  1;  // Bits 30:30
    UINT32 Spare                                   :  1;  // Bits 31:31
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} CH2CCC_CR_CTL0_STRUCT;
typedef union {
  struct {
    UINT32 cktop_cmn_bonus                         :  8;  // Bits 7:0
    UINT32 dcdsrz_cmn_dcdselsecondaryclk           :  1;  // Bits 8:8
    UINT32 dcdsrz_cmn_gear1en                      :  1;  // Bits 9:9
    UINT32 dcdsrz_cmn_gear2en                      :  1;  // Bits 10:10
    UINT32 dcdsrz_cmn_gear4en                      :  1;  // Bits 11:11
    UINT32 cccbuf_cmn_bonus                        :  2;  // Bits 13:12
    UINT32 pghub_cmn_pwrdwncomp                    :  6;  // Bits 19:14
    UINT32 ccctx_ccc0_dcdsampsecondary             :  1;  // Bits 20:20
    UINT32 Spare4                                  :  2;  // Bits 22:21
    UINT32 clktx_rgrp4_txclkpden                   :  1;  // Bits 23:23
    UINT32 clktx_rgrp3_txclkpden                   :  1;  // Bits 24:24
    UINT32 dcdsrz_cmn_dcdtailctl                   :  2;  // Bits 26:25
    UINT32 dcdsrz_cmn_dcdsrzsampclksel             :  2;  // Bits 28:27
    UINT32 dcdsrz_cmn_dcdenvcciog                  :  1;  // Bits 29:29
    UINT32 dcdsrz_cmn_dcdenrxpadpsal               :  1;  // Bits 30:30
    UINT32 dcdsrz_cmn_dcdenrxpadnsal               :  1;  // Bits 31:31
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} CH2CCC_CR_CTL3_STRUCT;

typedef DATA0CH0_CR_MDLLCTL4_STRUCT CH2CCC_CR_MDLLCTL4_STRUCT;
typedef union {
  struct {
    UINT32 afeshared_cmn_parkval                   :  1;  // Bits 0:0
    UINT32 reserved9                               :  3;  // Bits 3:1
    UINT32 cccrx_cmn_rxmvcmres                     :  2;  // Bits 5:4
    UINT32 ccctx_cmn_txeqconstz                    :  1;  // Bits 6:6
    UINT32 ccctx_cmn_txeqenntap                    :  15;  // Bits 21:7
    UINT32 ccctx_cmn_txeqenpreset                  :  1;  // Bits 22:22
    UINT32 bonus_3                                 :  1;  // Bits 23:23
    UINT32 bonus_2                                 :  1;  // Bits 24:24
    UINT32 pghub_cmn_ckes3segsel                   :  1;  // Bits 25:25
    UINT32 ccctx_cmn_txsegen                       :  2;  // Bits 27:26
    UINT32 txshim_cmn_txeqdataovrdval              :  1;  // Bits 28:28
    UINT32 txshim_cmn_txeqdataovrden               :  1;  // Bits 29:29
    UINT32 rxsdl_cmn_parken                        :  1;  // Bits 30:30
    UINT32 ccctx_cmn_parken                        :  1;  // Bits 31:31
  } Bits;
  struct {
    UINT32 parkval_ovr_val                         :  1;  // Bits 0:0
    UINT32 parkval_ovr_sel                         :  1;  // Bits 1:1
    UINT32 cccrx_cmn_rxmvcmres                     :  2;  // Bits 5:4
    UINT32 bonus_4                                 :  1;  // Bits 6:6
  } BitsQ0;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} CH2CCC_CR_CTL2_STRUCT;
typedef union {
  struct {
    UINT32 txpbd_ccc0_txpbddlycode                 :  8;  // Bits 7:0
    UINT32 txpbd_ccc10_txpbddlycode                :  8;  // Bits 15:8
    UINT32 txpbd_ccc11_txpbddlycode                :  8;  // Bits 23:16
    UINT32 txpbd_ccc12_txpbddlycode                :  8;  // Bits 31:24
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} CH2CCC_CR_TXPBDCODE0_STRUCT;
typedef union {
  struct {
    UINT32 txpbd_ccc1_txpbddlycode                 :  8;  // Bits 7:0
    UINT32 txpbd_ccc2_txpbddlycode                 :  8;  // Bits 15:8
    UINT32 txpbd_ccc3_txpbddlycode                 :  8;  // Bits 23:16
    UINT32 txpbd_ccc4_txpbddlycode                 :  8;  // Bits 31:24
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} CH2CCC_CR_TXPBDCODE1_STRUCT;
typedef union {
  struct {
    UINT32 txpbd_ccc5_txpbddlycode                 :  8;  // Bits 7:0
    UINT32 txpbd_ccc6_txpbddlycode                 :  8;  // Bits 15:8
    UINT32 txpbd_ccc7_txpbddlycode                 :  8;  // Bits 23:16
    UINT32 txpbd_ccc8_txpbddlycode                 :  8;  // Bits 31:24
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} CH2CCC_CR_TXPBDCODE2_STRUCT;
typedef union {
  struct {
    UINT32 txpbd_ccc9_txpbddlycode                 :  8;  // Bits 7:0
    UINT32 txpbd_ccc13_txpbddlycode                :  8;  // Bits 15:8
    UINT32 txpbd_ccc14_txpbddlycode                :  8;  // Bits 23:16
    UINT32 txpbd_cmn_txdccrangesel                 :  2;  // Bits 25:24
    UINT32 mdll_cmn_dccrangesel                    :  2;  // Bits 27:26
    UINT32 Spare                                   :  4;  // Bits 31:28
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} CH2CCC_CR_TXPBDCODE3_STRUCT;
typedef union {
  struct {
    UINT32 txpbd_ccc8_txpbddlycodeph90             :  8;  // Bits 7:0
    UINT32 txpbd_ccc7_txpbddlycodeph90             :  8;  // Bits 15:8
    UINT32 txpbd_ccc6_txpbddlycodeph90             :  8;  // Bits 23:16
    UINT32 txpbd_ccc5_txpbddlycodeph90             :  8;  // Bits 31:24
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} CH2CCC_CR_TXPBDCODE4_STRUCT;

typedef DATA0CH0_CR_DCCCTL5_STRUCT CH2CCC_CR_DCCCTL6_STRUCT;

typedef DATA0CH0_CR_DCCCTL6_STRUCT CH2CCC_CR_DCCCTL7_STRUCT;
typedef union {
  struct {
    UINT32 dcccodeph0_index7                       :  8;  // Bits 7:0
    UINT32 dcccodeph0_index8                       :  8;  // Bits 15:8
    UINT32 dcccodeph0_index9                       :  8;  // Bits 23:16
    UINT32 dcccodeph0_index10                      :  8;  // Bits 31:24
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} CH2CCC_CR_DCCCTL8_STRUCT;
typedef union {
  struct {
    UINT32                                         :  24;  // Bits 23:0
    UINT32 dcdrocal_en                             :  1;  // Bits 24:24
    UINT32 mdll_cmn_enabledcd                      :  1;  // Bits 25:25
    UINT32 dcdsrz_cmn_dcdenable                    :  1;  // Bits 26:26
    UINT32 dccfsm_rst_b                            :  1;  // Bits 27:27
    UINT32 fatal_cal                               :  1;  // Bits 28:28
    UINT32 reserved10                              :  3;  // Bits 31:29
  } BitsA0;
  struct {
    UINT32 dcccodeph0_index11                      :  8;  // Bits 7:0
    UINT32 dcccodeph0_index12                      :  8;  // Bits 15:8
    UINT32 dcccodeph0_index13                      :  8;  // Bits 23:16
    UINT32 dccfsm_rst_b                            :  1;  // Bits 24:24
    UINT32 maxdccsteps                             :  7;  // Bits 31:25
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} CH2CCC_CR_DCCCTL9_STRUCT;

typedef DATA0CH0_CR_DCCCTL9_STRUCT CH2CCC_CR_DCCCTL11_STRUCT;
typedef union {
  struct {
    UINT32 dcdrocal_en                             :  1;  // Bits 0:0
    UINT32 mdll_cmn_enabledcd                      :  1;  // Bits 1:1
    UINT32 dcdsrz_cmn_dcdenable                    :  1;  // Bits 2:2
    UINT32 fatal_cal                               :  1;  // Bits 3:3
    UINT32 ccctx_ccc8_txpfirstonrise               :  1;  // Bits 4:4
    UINT32 ccctx_ccc8_txpfirstonfall               :  1;  // Bits 5:5
    UINT32 ccctx_ccc7_txpfirstonrise               :  1;  // Bits 6:6
    UINT32 ccctx_ccc7_txpfirstonfall               :  1;  // Bits 7:7
    UINT32 ccctx_ccc6_txpfirstonrise               :  1;  // Bits 8:8
    UINT32 ccctx_ccc6_txpfirstonfall               :  1;  // Bits 9:9
    UINT32 ccctx_ccc5_txpfirstonrise               :  1;  // Bits 10:10
    UINT32 ccctx_ccc5_txpfirstonfall               :  1;  // Bits 11:11
    UINT32 dcdrocalsamplecountstart                :  1;  // Bits 12:12
    UINT32 dcdrocalsamplecountrst_b                :  1;  // Bits 13:13
    UINT32 dcdrocal_start                          :  1;  // Bits 14:14
    UINT32 dcc_start                               :  1;  // Bits 15:15
    UINT32 dcccodeph0_index15                      :  8;  // Bits 23:16
    UINT32 dcccodeph0_index14                      :  8;  // Bits 31:24
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} CH2CCC_CR_DCCCTL20_STRUCT;
typedef union {
  struct {
    UINT32 Pi0DivEn                                :  1;  // Bits 0:0
    UINT32 Pi0Inc                                  :  2;  // Bits 2:1
    UINT32 Pi1DivEn                                :  1;  // Bits 3:3
    UINT32 Pi1Inc                                  :  2;  // Bits 5:4
    UINT32 Pi2DivEn                                :  1;  // Bits 6:6
    UINT32 Pi2Inc                                  :  2;  // Bits 8:7
    UINT32 Pi3DivEn                                :  1;  // Bits 9:9
    UINT32 Pi3Inc                                  :  2;  // Bits 11:10
    UINT32 Pi4DivEn                                :  1;  // Bits 12:12
    UINT32 Pi4Inc                                  :  2;  // Bits 14:13
    UINT32 Pi4DivEnPreamble                        :  1;  // Bits 15:15
    UINT32 Pi4IncPreamble                          :  2;  // Bits 17:16
    UINT32 PiSyncDivider                           :  2;  // Bits 19:18
    UINT32 WckHalfPreamble                         :  1;  // Bits 20:20
    UINT32 PiClkDuration                           :  3;  // Bits 23:21
    UINT32 wck2cktrainen                           :  1;  // Bits 24:24
    UINT32 ccc_Spare1                              :  7;  // Bits 31:25
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} CH2CCC_CR_DDRCRCCCPIDIVIDER_STRUCT;

typedef DATA0CH0_CR_CLKALIGNCTL0_STRUCT CH2CCC_CR_CLKALIGNCTL0_STRUCT;

typedef DATA0CH0_CR_CLKALIGNCTL1_STRUCT CH2CCC_CR_CLKALIGNCTL1_STRUCT;
typedef union {
  struct {
    UINT32 pdoffset_ovr                            :  1;  // Bits 0:0
    UINT32 txdll_cmdd0_pdoffset                    :  8;  // Bits 8:1
    UINT32 d0tosig_offset                          :  8;  // Bits 16:9
    UINT32 reserved10                              :  15;  // Bits 31:17
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} CH2CCC_CR_CLKALIGNCTL2_STRUCT;
typedef union {
  struct {
    UINT32 wlslowclken_dly                         :  5;  // Bits 4:0
    UINT32 weaklocken_dly                          :  5;  // Bits 9:5
    UINT32 spare                                   :  4;  // Bits 13:10
    UINT32 lp_weaklocken                           :  1;  // Bits 14:14
    UINT32 weaklocken_ovr_sel                      :  1;  // Bits 15:15
    UINT32 wlcomp_samplewait                       :  5;  // Bits 20:16
    UINT32 wlcomp_per_stepsize                     :  3;  // Bits 23:21
    UINT32 wlcomp_init_stepsize                    :  3;  // Bits 26:24
    UINT32 weaklockcomp_err                        :  1;  // Bits 27:27
    UINT32 weaklocken_ovr                          :  1;  // Bits 28:28
    UINT32 wlcomp_clkgatedis                       :  1;  // Bits 29:29
    UINT32 weaklocken                              :  1;  // Bits 30:30
    UINT32 weaklockcomp_en                         :  1;  // Bits 31:31
  } Bits;
  struct {
    UINT32 initdlllock_dly                         :  4;  // Bits 3:0
    UINT32 wlrelock_dly                            :  4;  // Bits 7:4
    UINT32 weaklocken_dly                          :  5;  // Bits 12:8
  } BitsQ0;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} CH2CCC_CR_WLCTRL_STRUCT;
typedef union {
  struct {
    UINT32 ClkGateDisable                          :  1;  // Bits 0:0
    UINT32 DefDrvEnLow                             :  2;  // Bits 2:1
    UINT32 RTO                                     :  1;  // Bits 3:3
    UINT32 IntCkOn                                 :  1;  // Bits 4:4
    UINT32 CkeIdlePiGateDisable                    :  1;  // Bits 5:5
    UINT32 CaValidPiGateDisable                    :  1;  // Bits 6:6
    UINT32 CaTxEq                                  :  5;  // Bits 11:7
    UINT32 CtlTxEq                                 :  5;  // Bits 16:12
    UINT32 ClkTxEq                                 :  5;  // Bits 21:17
    UINT32 CtlSRDrv                                :  2;  // Bits 23:22
    UINT32 c3segsel_b_for_cke                      :  1;  // Bits 24:24
    UINT32 RxVref                                  :  6;  // Bits 30:25
    UINT32 BlockTrainRst                           :  1;  // Bits 31:31
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} CH2CCC_CR_DDRCRCCCCLKCONTROLS_STRUCT;
typedef union {
  struct {
    UINT32 Reserved11                              :  4;  // Bits 3:0
    UINT32 txpbd_ccc8_txpbd_ph90incr               :  1;  // Bits 4:4
    UINT32 txpbd_ccc7_txpbd_ph90incr               :  1;  // Bits 5:5
    UINT32 txpbd_ccc6_txpbd_ph90incr               :  1;  // Bits 6:6
    UINT32 txpbd_ccc5_txpbd_ph90incr               :  1;  // Bits 7:7
    UINT32 txpbd_ccc8_txpbddoffset                 :  6;  // Bits 13:8
    UINT32 txpbd_ccc7_txpbddoffset                 :  6;  // Bits 19:14
    UINT32 txpbd_ccc6_txpbddoffset                 :  6;  // Bits 25:20
    UINT32 txpbd_ccc5_txpbddoffset                 :  6;  // Bits 31:26
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} CH2CCC_CR_TXPBDOFFSET_STRUCT;

typedef DATA0CH0_CR_DCCCTL0_STRUCT CH2CCC_CR_DCCCTL0_STRUCT;

typedef DATA0CH0_CR_DCCCTL1_STRUCT CH2CCC_CR_DCCCTL1_STRUCT;
typedef union {
  struct {
    UINT32 Reserved12                              :  2;  // Bits 1:0
    UINT32 dcc_clken_ovr                           :  1;  // Bits 2:2
    UINT32 dcdsamplecountstart                     :  1;  // Bits 3:3
    UINT32 dcdsamplecountrst_b                     :  1;  // Bits 4:4
    UINT32 dcc_serdata_ovren                       :  1;  // Bits 5:5
    UINT32 dcdrovcoprog                            :  3;  // Bits 8:6
    UINT32 dcdrolfsr                               :  10;  // Bits 18:9
    UINT32 scr_fsm_dccctrlcodestatus_sel           :  4;  // Bits 22:19
    UINT32 compare_dly                             :  4;  // Bits 26:23
    UINT32 dcdreset_dly                            :  3;  // Bits 29:27
    UINT32 clknclkpsample_avg_en                   :  1;  // Bits 30:30
    UINT32 threeloop_en                            :  1;  // Bits 31:31
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} CH2CCC_CR_DCCCTL2_STRUCT;

typedef DATA0CH0_CR_DCCCTL3_STRUCT CH2CCC_CR_DCCCTL3_STRUCT;
typedef union {
  struct {
    UINT32 dcc_step3pattern_enable                 :  16;  // Bits 15:0
    UINT32 dccctrlcodeph0_status                   :  8;  // Bits 23:16
    UINT32 dccctrlcodeph90_status                  :  8;  // Bits 31:24
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} CH2CCC_CR_DCCCTL5_STRUCT;

typedef DATA0CH0_CR_DCCCTL10_STRUCT CH2CCC_CR_DCCCTL13_STRUCT;
typedef union {
  struct {
    UINT32 spare                                   :  25;  // Bits 24:0
    UINT32 initdone_status                         :  1;  // Bits 25:25
    UINT32 scr_gvblock_ovrride                     :  1;  // Bits 26:26
  } BitsA0;
  struct {
    UINT32 spare                                   :  20;  // Bits 19:0
    UINT32 DdrxxClkGoodQnnnH                       :  1;  // Bits 20:20
    UINT32 current_pmctrlfsm_state                 :  4;  // Bits 24:21
    UINT32 Vsshipwrgood                            :  1;  // Bits 25:25
    UINT32 initdone_status                         :  1;  // Bits 26:26
    UINT32 pmctrlfsm_ovrden                        :  1;  // Bits 27:27
    UINT32 pmctrlfsm_ovrdval                       :  4;  // Bits 31:28
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} CH2CCC_CR_PMFSM_STRUCT;
typedef union {
  struct {
    UINT32 RcompDrvUp                              :  6;  // Bits 5:0
    UINT32 RcompDrvDown                            :  6;  // Bits 11:6
    UINT32 ScompCmd                                :  8;  // Bits 19:12
    UINT32 VssHiFF_cmd                             :  6;  // Bits 25:20
    UINT32 reserved13                              :  6;  // Bits 31:26
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} CH2CCC_CR_DDRCRCMDCOMP_STRUCT;
typedef union {
  struct {
    UINT32 RcompDrvUp                              :  6;  // Bits 5:0
    UINT32 RcompDrvDown                            :  6;  // Bits 11:6
    UINT32 ScompClk                                :  8;  // Bits 19:12
    UINT32 VssHiFF_ctl                             :  6;  // Bits 25:20
    UINT32 reserved14                              :  6;  // Bits 31:26
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} CH2CCC_CR_DDRCRCLKCOMP_STRUCT;
typedef union {
  struct {
    UINT32 RcompDrvUp                              :  6;  // Bits 5:0
    UINT32 RcompDrvDown                            :  6;  // Bits 11:6
    UINT32 ScompCtl                                :  8;  // Bits 19:12
    UINT32 VssHiFF_clk                             :  6;  // Bits 25:20
    UINT32 CkeCsUp                                 :  6;  // Bits 31:26
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} CH2CCC_CR_DDRCRCTLCOMP_STRUCT;
typedef union {
  struct {
    UINT32 Dll_codepi                              :  6;  // Bits 5:0
    UINT32 Dll_codewl                              :  6;  // Bits 11:6
    UINT32 Dll_bwsel                               :  6;  // Bits 17:12
    UINT32 Dll_vdlllock                            :  8;  // Bits 25:18
    UINT32 Offsetnui                               :  2;  // Bits 27:26
    UINT32 Targetnui                               :  2;  // Bits 29:28
    UINT32 Spare                                   :  2;  // Bits 31:30
  } Bits;
  struct {
    UINT32                                         :  18;  // Bits 17:0
    UINT32 rloaddqs                                :  6;  // Bits 23:18
    UINT32 cben                                    :  2;  // Bits 25:24
  } BitsQ0;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} CH2CCC_CR_VCCDLLCOMPDATACCC_STRUCT;
typedef union {
  struct {
    UINT32 dccoffset                               :  8;  // Bits 7:0
    UINT32 dccoffsetsign                           :  1;  // Bits 8:8
    UINT32 reserved15                              :  23;  // Bits 31:9
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} CH2CCC_CR_DCSOFFSET_STRUCT;
typedef union {
  struct {
    UINT32 DdrBscanEnable                          :  1;  // Bits 0:0
    UINT32 DataValue                               :  15;  // Bits 15:1
    UINT32 BiasRloadVref                           :  3;  // Bits 18:16
    UINT32 BiasIrefAdj                             :  4;  // Bits 22:19
    UINT32 BiasCasAdj                              :  2;  // Bits 24:23
    UINT32 Spare                                   :  7;  // Bits 31:25
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} CH2CCC_CR_DDRCRBSCANDATA_STRUCT;
typedef union {
  struct {
    UINT32 enonfuncen                              :  1;  // Bits 0:0
    UINT32 startonfuncen                           :  1;  // Bits 1:1
    UINT32 enlfsr                                  :  1;  // Bits 2:2
    UINT32 masknondeta                             :  1;  // Bits 3:3
    UINT32 Spare                                   :  1;  // Bits 4:4
    UINT32 encycles                                :  3;  // Bits 7:5
    UINT32 maskbits                                :  8;  // Bits 15:8
    UINT32 misrdata                                :  16;  // Bits 31:16
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} CH2CCC_CR_DDRCRMISR_STRUCT;
typedef union {
  struct {
    UINT32                                         :  4;  // Bits 4:0
    UINT32 minval                                  :  8;  // Bits 11:4
    UINT32 maxval                                  :  8;  // Bits 19:12
    UINT32 iolb_cpgcmode                           :  1;  // Bits 20:20
    UINT32 CB1                                     :  1;  // Bits 21:21
    UINT32 calccenter                              :  1;  // Bits 22:22
    UINT32 iolbcycles                              :  4;  // Bits 26:23
    UINT32                                         :  3;  // Bits 29:27
    UINT32 data0_ph1                               :  1;  // Bits 30:30
    UINT32 data1_ph1                               :  1;  // Bits 31:31
  } BitsA0;
  struct {
    UINT32 runtest                                 :  1;  // Bits 0:0
    UINT32 param                                   :  3;  // Bits 3:1
    UINT32 minval                                  :  10;  // Bits 13:4
    UINT32 maxval                                  :  10;  // Bits 23:14
    UINT32 iolb_cpgcmode                           :  1;  // Bits 24:24
    UINT32 isolation                               :  1;  // Bits 25:25
    UINT32 calccenter                              :  1;  // Bits 26:26
    UINT32 laneresult                              :  1;  // Bits 27:27
    UINT32 data0                                   :  1;  // Bits 28:28
    UINT32 data1                                   :  1;  // Bits 29:29
    UINT32 ddrdfxdataovrd                          :  1;  // Bits 30:30
    UINT32 Spare                                   :  1;  // Bits 31:31
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} CH2CCC_CR_DDRCRMARGINMODECONTROL_STRUCT;

typedef DATA0CH0_CR_DDRCRMARGINMODEDEBUGMSB0_STRUCT CH2CCC_CR_DDRCRMARGINMODEDEBUGMSB_STRUCT;

typedef DATA0CH0_CR_DDRCRMARGINMODEDEBUGMSB0_STRUCT CH2CCC_CR_DDRCRMARGINMODEDEBUGLSB_STRUCT;
typedef union {
  struct {
    UINT32 srzdataovrden                           :  1;  // Bits 0:0
    UINT32 srzdrvenovrden                          :  1;  // Bits 1:1
    UINT32 txeqdataovrden                          :  1;  // Bits 2:2
    UINT32 txeqdataovrdval                         :  1;  // Bits 3:3
    UINT32 ccc14_srzdataovrdval                    :  1;  // Bits 4:4
    UINT32 ccc13_srzdataovrdval                    :  1;  // Bits 5:5
    UINT32 ccc12_srzdataovrdval                    :  1;  // Bits 6:6
    UINT32 ccc11_srzdataovrdval                    :  1;  // Bits 7:7
    UINT32 ccc10_srzdataovrdval                    :  1;  // Bits 8:8
    UINT32 ccc9_srzdataovrdval                     :  1;  // Bits 9:9
    UINT32 ccc8_srzdataovrdval                     :  1;  // Bits 10:10
    UINT32 ccc7_srzdataovrdval                     :  1;  // Bits 11:11
    UINT32 ccc6_srzdataovrdval                     :  1;  // Bits 12:12
    UINT32 ccc5_srzdataovrdval                     :  1;  // Bits 13:13
    UINT32 ccc4_srzdataovrdval                     :  1;  // Bits 14:14
    UINT32 ccc3_srzdataovrdval                     :  1;  // Bits 15:15
    UINT32 ccc2_srzdataovrdval                     :  1;  // Bits 16:16
    UINT32 ccc1_srzdataovrdval                     :  1;  // Bits 17:17
    UINT32 ccc0_srzdataovrdval                     :  1;  // Bits 18:18
    UINT32 Spare                                   :  13;  // Bits 31:19
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} CH2CCC_CR_SRZBYPASSCTL_STRUCT;
typedef union {
  struct {
    UINT32 ccc14_srzdrvenovrden                    :  1;  // Bits 0:0
    UINT32 ccc13_srzdrvenovrden                    :  1;  // Bits 1:1
    UINT32 ccc12_srzdrvenovrden                    :  1;  // Bits 2:2
    UINT32 ccc11_srzdrvenovrden                    :  1;  // Bits 3:3
    UINT32 ccc10_srzdrvenovrden                    :  1;  // Bits 4:4
    UINT32 ccc9_srzdrvenovrden                     :  1;  // Bits 5:5
    UINT32 ccc8_srzdrvenovrden                     :  1;  // Bits 6:6
    UINT32 ccc7_srzdrvenovrden                     :  1;  // Bits 7:7
    UINT32 ccc6_srzdrvenovrden                     :  1;  // Bits 8:8
    UINT32 ccc5_srzdrvenovrden                     :  1;  // Bits 9:9
    UINT32 ccc4_srzdrvenovrden                     :  1;  // Bits 10:10
    UINT32 ccc3_srzdrvenovrden                     :  1;  // Bits 11:11
    UINT32 ccc2_srzdrvenovrden                     :  1;  // Bits 12:12
    UINT32 ccc1_srzdrvenovrden                     :  1;  // Bits 13:13
    UINT32 ccc0_srzdrvenovrden                     :  1;  // Bits 14:14
    UINT32 ccc14_srzdrvenovrdval                   :  1;  // Bits 15:15
    UINT32 ccc13_srzdrvenovrdval                   :  1;  // Bits 16:16
    UINT32 ccc12_srzdrvenovrdval                   :  1;  // Bits 17:17
    UINT32 ccc11_srzdrvenovrdval                   :  1;  // Bits 18:18
    UINT32 ccc10_srzdrvenovrdval                   :  1;  // Bits 19:19
    UINT32 ccc9_srzdrvenovrdval                    :  1;  // Bits 20:20
    UINT32 ccc8_srzdrvenovrdval                    :  1;  // Bits 21:21
    UINT32 ccc7_srzdrvenovrdval                    :  1;  // Bits 22:22
    UINT32 ccc6_srzdrvenovrdval                    :  1;  // Bits 23:23
    UINT32 ccc5_srzdrvenovrdval                    :  1;  // Bits 24:24
    UINT32 ccc4_srzdrvenovrdval                    :  1;  // Bits 25:25
    UINT32 ccc3_srzdrvenovrdval                    :  1;  // Bits 26:26
    UINT32 ccc2_srzdrvenovrdval                    :  1;  // Bits 27:27
    UINT32 ccc1_srzdrvenovrdval                    :  1;  // Bits 28:28
    UINT32 ccc0_srzdrvenovrdval                    :  1;  // Bits 29:29
    UINT32 reserved16                              :  2;  // Bits 31:30
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} CH2CCC_CR_SRZDRVENBYPASSCTL_STRUCT;

typedef DATA0CH0_CR_DFXCCMON_STRUCT CH2CCC_CR_DFXCCMON_STRUCT;
typedef union {
  struct {
    UINT32 dcc_bits_en                             :  16;  // Bits 15:0
    UINT32 dcc_step2pattern_enable                 :  16;  // Bits 31:16
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} CH2CCC_CR_DCCCTL4_STRUCT;
typedef union {
  struct {
    UINT32 fatal_live                              :  16;  // Bits 15:0
    UINT32 fatal_sticky                            :  16;  // Bits 31:16
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} CH2CCC_CR_DCCCTL17_STRUCT;
typedef union {
  struct {
    UINT32 dccctrlcodeph0overflow                  :  16;  // Bits 15:0
    UINT32 dccctrlcodeph90overflow                 :  16;  // Bits 31:16
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} CH2CCC_CR_DCCCTL18_STRUCT;

typedef DATA0CH0_CR_DCCCTL13_STRUCT CH2CCC_CR_DCCCTL19_STRUCT;
typedef union {
  struct {
    UINT32 clkalign_sample_pd_output               :  1;  // Bits 0:0
    UINT32 clkalign_complete_level                 :  1;  // Bits 1:1
    UINT32 clkalignstatus_err                      :  1;  // Bits 2:2
    UINT32 clkalignstatus_one_count                :  5;  // Bits 7:3
    UINT32 clkalignstatus_zero_count               :  5;  // Bits 12:8
    UINT32 clkalignstatus_sample_done              :  1;  // Bits 13:13
    UINT32 clkalignstatus_safe1                    :  1;  // Bits 14:14
    UINT32 clkalignstatus_safe0                    :  1;  // Bits 15:15
    UINT32 ref2xclkpicode                          :  8;  // Bits 23:16
    UINT32 reserved17                              :  8;  // Bits 31:24
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} CH2CCC_CR_CLKALIGNSTATUS_STRUCT;

typedef CH2CCC_CR_DDRCRPINSUSED_STRUCT CH0CCC_CR_DDRCRPINSUSED_STRUCT;

typedef CH2CCC_CR_CCCRSTCTL_STRUCT CH0CCC_CR_CCCRSTCTL_STRUCT;

typedef CH2CCC_CR_PICODE0_STRUCT CH0CCC_CR_PICODE0_STRUCT;

typedef CH2CCC_CR_PICODE1_STRUCT CH0CCC_CR_PICODE1_STRUCT;

typedef CH2CCC_CR_PICODE2_STRUCT CH0CCC_CR_PICODE2_STRUCT;

typedef CH2CCC_CR_PICODE3_STRUCT CH0CCC_CR_PICODE3_STRUCT;

typedef CH2CCC_CR_MDLLCTL0_STRUCT CH0CCC_CR_MDLLCTL0_STRUCT;

typedef CH2CCC_CR_MDLLCTL1_STRUCT CH0CCC_CR_MDLLCTL1_STRUCT;

typedef CH2CCC_CR_MDLLCTL2_STRUCT CH0CCC_CR_MDLLCTL2_STRUCT;

typedef CH2CCC_CR_MDLLCTL3_STRUCT CH0CCC_CR_MDLLCTL3_STRUCT;

typedef CH2CCC_CR_COMP2_STRUCT CH0CCC_CR_COMP2_STRUCT;

typedef CH2CCC_CR_COMP3_STRUCT CH0CCC_CR_COMP3_STRUCT;

typedef CH2CCC_CR_DDRCRCCCVOLTAGEUSED_STRUCT CH0CCC_CR_DDRCRCCCVOLTAGEUSED_STRUCT;

typedef CH2CCC_CR_DDRCRCTLCACOMPOFFSET_STRUCT CH0CCC_CR_DDRCRCTLCACOMPOFFSET_STRUCT;

typedef CH2CCC_CR_DDRCRVSSHICLKCOMPOFFSET_STRUCT CH0CCC_CR_DDRCRVSSHICLKCOMPOFFSET_STRUCT;

typedef CH2CCC_CR_CTL0_STRUCT CH0CCC_CR_CTL0_STRUCT;

typedef CH2CCC_CR_CTL3_STRUCT CH0CCC_CR_CTL3_STRUCT;

typedef DATA0CH0_CR_MDLLCTL4_STRUCT CH0CCC_CR_MDLLCTL4_STRUCT;

typedef CH2CCC_CR_CTL2_STRUCT CH0CCC_CR_CTL2_STRUCT;

typedef CH2CCC_CR_TXPBDCODE0_STRUCT CH0CCC_CR_TXPBDCODE0_STRUCT;

typedef CH2CCC_CR_TXPBDCODE1_STRUCT CH0CCC_CR_TXPBDCODE1_STRUCT;

typedef CH2CCC_CR_TXPBDCODE2_STRUCT CH0CCC_CR_TXPBDCODE2_STRUCT;

typedef CH2CCC_CR_TXPBDCODE3_STRUCT CH0CCC_CR_TXPBDCODE3_STRUCT;

typedef CH2CCC_CR_TXPBDCODE4_STRUCT CH0CCC_CR_TXPBDCODE4_STRUCT;

typedef DATA0CH0_CR_DCCCTL5_STRUCT CH0CCC_CR_DCCCTL6_STRUCT;

typedef DATA0CH0_CR_DCCCTL6_STRUCT CH0CCC_CR_DCCCTL7_STRUCT;

typedef CH2CCC_CR_DCCCTL8_STRUCT CH0CCC_CR_DCCCTL8_STRUCT;

typedef CH2CCC_CR_DCCCTL9_STRUCT CH0CCC_CR_DCCCTL9_STRUCT;

typedef DATA0CH0_CR_DCCCTL9_STRUCT CH0CCC_CR_DCCCTL11_STRUCT;

typedef CH2CCC_CR_DCCCTL20_STRUCT CH0CCC_CR_DCCCTL20_STRUCT;

typedef CH2CCC_CR_DDRCRCCCPIDIVIDER_STRUCT CH0CCC_CR_DDRCRCCCPIDIVIDER_STRUCT;

typedef DATA0CH0_CR_CLKALIGNCTL0_STRUCT CH0CCC_CR_CLKALIGNCTL0_STRUCT;

typedef DATA0CH0_CR_CLKALIGNCTL1_STRUCT CH0CCC_CR_CLKALIGNCTL1_STRUCT;

typedef CH2CCC_CR_CLKALIGNCTL2_STRUCT CH0CCC_CR_CLKALIGNCTL2_STRUCT;

typedef CH2CCC_CR_WLCTRL_STRUCT CH0CCC_CR_WLCTRL_STRUCT;

typedef CH2CCC_CR_DDRCRCCCCLKCONTROLS_STRUCT CH0CCC_CR_DDRCRCCCCLKCONTROLS_STRUCT;

typedef CH2CCC_CR_TXPBDOFFSET_STRUCT CH0CCC_CR_TXPBDOFFSET_STRUCT;

typedef DATA0CH0_CR_DCCCTL0_STRUCT CH0CCC_CR_DCCCTL0_STRUCT;

typedef DATA0CH0_CR_DCCCTL1_STRUCT CH0CCC_CR_DCCCTL1_STRUCT;

typedef CH2CCC_CR_DCCCTL2_STRUCT CH0CCC_CR_DCCCTL2_STRUCT;

typedef DATA0CH0_CR_DCCCTL3_STRUCT CH0CCC_CR_DCCCTL3_STRUCT;

typedef CH2CCC_CR_DCCCTL5_STRUCT CH0CCC_CR_DCCCTL5_STRUCT;

typedef DATA0CH0_CR_DCCCTL10_STRUCT CH0CCC_CR_DCCCTL13_STRUCT;

typedef CH2CCC_CR_PMFSM_STRUCT CH0CCC_CR_PMFSM_STRUCT;

typedef CH2CCC_CR_DDRCRCMDCOMP_STRUCT CH0CCC_CR_DDRCRCMDCOMP_STRUCT;

typedef CH2CCC_CR_DDRCRCLKCOMP_STRUCT CH0CCC_CR_DDRCRCLKCOMP_STRUCT;

typedef CH2CCC_CR_DDRCRCTLCOMP_STRUCT CH0CCC_CR_DDRCRCTLCOMP_STRUCT;

typedef CH2CCC_CR_VCCDLLCOMPDATACCC_STRUCT CH0CCC_CR_VCCDLLCOMPDATACCC_STRUCT;

typedef CH2CCC_CR_DCSOFFSET_STRUCT CH0CCC_CR_DCSOFFSET_STRUCT;

typedef CH2CCC_CR_DDRCRBSCANDATA_STRUCT CH0CCC_CR_DDRCRBSCANDATA_STRUCT;

typedef CH2CCC_CR_DDRCRMISR_STRUCT CH0CCC_CR_DDRCRMISR_STRUCT;

typedef CH2CCC_CR_DDRCRMARGINMODECONTROL_STRUCT CH0CCC_CR_DDRCRMARGINMODECONTROL_STRUCT;

typedef DATA0CH0_CR_DDRCRMARGINMODEDEBUGMSB0_STRUCT CH0CCC_CR_DDRCRMARGINMODEDEBUGMSB_STRUCT;

typedef DATA0CH0_CR_DDRCRMARGINMODEDEBUGMSB0_STRUCT CH0CCC_CR_DDRCRMARGINMODEDEBUGLSB_STRUCT;

typedef CH2CCC_CR_SRZBYPASSCTL_STRUCT CH0CCC_CR_SRZBYPASSCTL_STRUCT;

typedef CH2CCC_CR_SRZDRVENBYPASSCTL_STRUCT CH0CCC_CR_SRZDRVENBYPASSCTL_STRUCT;

typedef DATA0CH0_CR_DFXCCMON_STRUCT CH0CCC_CR_DFXCCMON_STRUCT;

typedef CH2CCC_CR_DCCCTL4_STRUCT CH0CCC_CR_DCCCTL4_STRUCT;

typedef CH2CCC_CR_DCCCTL17_STRUCT CH0CCC_CR_DCCCTL17_STRUCT;

typedef CH2CCC_CR_DCCCTL18_STRUCT CH0CCC_CR_DCCCTL18_STRUCT;

typedef DATA0CH0_CR_DCCCTL13_STRUCT CH0CCC_CR_DCCCTL19_STRUCT;

typedef CH2CCC_CR_CLKALIGNSTATUS_STRUCT CH0CCC_CR_CLKALIGNSTATUS_STRUCT;

typedef CH2CCC_CR_DDRCRPINSUSED_STRUCT CH3CCC_CR_DDRCRPINSUSED_STRUCT;

typedef CH2CCC_CR_CCCRSTCTL_STRUCT CH3CCC_CR_CCCRSTCTL_STRUCT;

typedef CH2CCC_CR_PICODE0_STRUCT CH3CCC_CR_PICODE0_STRUCT;

typedef CH2CCC_CR_PICODE1_STRUCT CH3CCC_CR_PICODE1_STRUCT;

typedef CH2CCC_CR_PICODE2_STRUCT CH3CCC_CR_PICODE2_STRUCT;

typedef CH2CCC_CR_PICODE3_STRUCT CH3CCC_CR_PICODE3_STRUCT;

typedef CH2CCC_CR_MDLLCTL0_STRUCT CH3CCC_CR_MDLLCTL0_STRUCT;

typedef CH2CCC_CR_MDLLCTL1_STRUCT CH3CCC_CR_MDLLCTL1_STRUCT;

typedef CH2CCC_CR_MDLLCTL2_STRUCT CH3CCC_CR_MDLLCTL2_STRUCT;

typedef CH2CCC_CR_MDLLCTL3_STRUCT CH3CCC_CR_MDLLCTL3_STRUCT;

typedef CH2CCC_CR_COMP2_STRUCT CH3CCC_CR_COMP2_STRUCT;

typedef CH2CCC_CR_COMP3_STRUCT CH3CCC_CR_COMP3_STRUCT;

typedef CH2CCC_CR_DDRCRCCCVOLTAGEUSED_STRUCT CH3CCC_CR_DDRCRCCCVOLTAGEUSED_STRUCT;

typedef CH2CCC_CR_DDRCRCTLCACOMPOFFSET_STRUCT CH3CCC_CR_DDRCRCTLCACOMPOFFSET_STRUCT;

typedef CH2CCC_CR_DDRCRVSSHICLKCOMPOFFSET_STRUCT CH3CCC_CR_DDRCRVSSHICLKCOMPOFFSET_STRUCT;

typedef CH2CCC_CR_CTL0_STRUCT CH3CCC_CR_CTL0_STRUCT;

typedef CH2CCC_CR_CTL3_STRUCT CH3CCC_CR_CTL3_STRUCT;

typedef DATA0CH0_CR_MDLLCTL4_STRUCT CH3CCC_CR_MDLLCTL4_STRUCT;

typedef CH2CCC_CR_CTL2_STRUCT CH3CCC_CR_CTL2_STRUCT;

typedef CH2CCC_CR_TXPBDCODE0_STRUCT CH3CCC_CR_TXPBDCODE0_STRUCT;

typedef CH2CCC_CR_TXPBDCODE1_STRUCT CH3CCC_CR_TXPBDCODE1_STRUCT;

typedef CH2CCC_CR_TXPBDCODE2_STRUCT CH3CCC_CR_TXPBDCODE2_STRUCT;

typedef CH2CCC_CR_TXPBDCODE3_STRUCT CH3CCC_CR_TXPBDCODE3_STRUCT;

typedef CH2CCC_CR_TXPBDCODE4_STRUCT CH3CCC_CR_TXPBDCODE4_STRUCT;

typedef DATA0CH0_CR_DCCCTL5_STRUCT CH3CCC_CR_DCCCTL6_STRUCT;

typedef DATA0CH0_CR_DCCCTL6_STRUCT CH3CCC_CR_DCCCTL7_STRUCT;

typedef CH2CCC_CR_DCCCTL8_STRUCT CH3CCC_CR_DCCCTL8_STRUCT;

typedef CH2CCC_CR_DCCCTL9_STRUCT CH3CCC_CR_DCCCTL9_STRUCT;

typedef DATA0CH0_CR_DCCCTL9_STRUCT CH3CCC_CR_DCCCTL11_STRUCT;

typedef CH2CCC_CR_DCCCTL20_STRUCT CH3CCC_CR_DCCCTL20_STRUCT;

typedef CH2CCC_CR_DDRCRCCCPIDIVIDER_STRUCT CH3CCC_CR_DDRCRCCCPIDIVIDER_STRUCT;

typedef DATA0CH0_CR_CLKALIGNCTL0_STRUCT CH3CCC_CR_CLKALIGNCTL0_STRUCT;

typedef DATA0CH0_CR_CLKALIGNCTL1_STRUCT CH3CCC_CR_CLKALIGNCTL1_STRUCT;

typedef CH2CCC_CR_CLKALIGNCTL2_STRUCT CH3CCC_CR_CLKALIGNCTL2_STRUCT;

typedef CH2CCC_CR_WLCTRL_STRUCT CH3CCC_CR_WLCTRL_STRUCT;

typedef CH2CCC_CR_DDRCRCCCCLKCONTROLS_STRUCT CH3CCC_CR_DDRCRCCCCLKCONTROLS_STRUCT;

typedef CH2CCC_CR_TXPBDOFFSET_STRUCT CH3CCC_CR_TXPBDOFFSET_STRUCT;

typedef DATA0CH0_CR_DCCCTL0_STRUCT CH3CCC_CR_DCCCTL0_STRUCT;

typedef DATA0CH0_CR_DCCCTL1_STRUCT CH3CCC_CR_DCCCTL1_STRUCT;

typedef CH2CCC_CR_DCCCTL2_STRUCT CH3CCC_CR_DCCCTL2_STRUCT;

typedef DATA0CH0_CR_DCCCTL3_STRUCT CH3CCC_CR_DCCCTL3_STRUCT;

typedef CH2CCC_CR_DCCCTL5_STRUCT CH3CCC_CR_DCCCTL5_STRUCT;

typedef DATA0CH0_CR_DCCCTL10_STRUCT CH3CCC_CR_DCCCTL13_STRUCT;

typedef CH2CCC_CR_PMFSM_STRUCT CH3CCC_CR_PMFSM_STRUCT;

typedef CH2CCC_CR_DDRCRCMDCOMP_STRUCT CH3CCC_CR_DDRCRCMDCOMP_STRUCT;

typedef CH2CCC_CR_DDRCRCLKCOMP_STRUCT CH3CCC_CR_DDRCRCLKCOMP_STRUCT;

typedef CH2CCC_CR_DDRCRCTLCOMP_STRUCT CH3CCC_CR_DDRCRCTLCOMP_STRUCT;

typedef CH2CCC_CR_VCCDLLCOMPDATACCC_STRUCT CH3CCC_CR_VCCDLLCOMPDATACCC_STRUCT;

typedef CH2CCC_CR_DCSOFFSET_STRUCT CH3CCC_CR_DCSOFFSET_STRUCT;

typedef CH2CCC_CR_DDRCRBSCANDATA_STRUCT CH3CCC_CR_DDRCRBSCANDATA_STRUCT;

typedef CH2CCC_CR_DDRCRMISR_STRUCT CH3CCC_CR_DDRCRMISR_STRUCT;

typedef CH2CCC_CR_DDRCRMARGINMODECONTROL_STRUCT CH3CCC_CR_DDRCRMARGINMODECONTROL_STRUCT;

typedef DATA0CH0_CR_DDRCRMARGINMODEDEBUGMSB0_STRUCT CH3CCC_CR_DDRCRMARGINMODEDEBUGMSB_STRUCT;

typedef DATA0CH0_CR_DDRCRMARGINMODEDEBUGMSB0_STRUCT CH3CCC_CR_DDRCRMARGINMODEDEBUGLSB_STRUCT;

typedef CH2CCC_CR_SRZBYPASSCTL_STRUCT CH3CCC_CR_SRZBYPASSCTL_STRUCT;

typedef CH2CCC_CR_SRZDRVENBYPASSCTL_STRUCT CH3CCC_CR_SRZDRVENBYPASSCTL_STRUCT;

typedef DATA0CH0_CR_DFXCCMON_STRUCT CH3CCC_CR_DFXCCMON_STRUCT;

typedef CH2CCC_CR_DCCCTL4_STRUCT CH3CCC_CR_DCCCTL4_STRUCT;

typedef CH2CCC_CR_DCCCTL17_STRUCT CH3CCC_CR_DCCCTL17_STRUCT;

typedef CH2CCC_CR_DCCCTL18_STRUCT CH3CCC_CR_DCCCTL18_STRUCT;

typedef DATA0CH0_CR_DCCCTL13_STRUCT CH3CCC_CR_DCCCTL19_STRUCT;

typedef CH2CCC_CR_CLKALIGNSTATUS_STRUCT CH3CCC_CR_CLKALIGNSTATUS_STRUCT;

typedef CH2CCC_CR_DDRCRPINSUSED_STRUCT CH1CCC_CR_DDRCRPINSUSED_STRUCT;

typedef CH2CCC_CR_CCCRSTCTL_STRUCT CH1CCC_CR_CCCRSTCTL_STRUCT;

typedef CH2CCC_CR_PICODE0_STRUCT CH1CCC_CR_PICODE0_STRUCT;

typedef CH2CCC_CR_PICODE1_STRUCT CH1CCC_CR_PICODE1_STRUCT;

typedef CH2CCC_CR_PICODE2_STRUCT CH1CCC_CR_PICODE2_STRUCT;

typedef CH2CCC_CR_PICODE3_STRUCT CH1CCC_CR_PICODE3_STRUCT;

typedef CH2CCC_CR_MDLLCTL0_STRUCT CH1CCC_CR_MDLLCTL0_STRUCT;

typedef CH2CCC_CR_MDLLCTL1_STRUCT CH1CCC_CR_MDLLCTL1_STRUCT;

typedef CH2CCC_CR_MDLLCTL2_STRUCT CH1CCC_CR_MDLLCTL2_STRUCT;

typedef CH2CCC_CR_MDLLCTL3_STRUCT CH1CCC_CR_MDLLCTL3_STRUCT;

typedef CH2CCC_CR_COMP2_STRUCT CH1CCC_CR_COMP2_STRUCT;

typedef CH2CCC_CR_COMP3_STRUCT CH1CCC_CR_COMP3_STRUCT;

typedef CH2CCC_CR_DDRCRCCCVOLTAGEUSED_STRUCT CH1CCC_CR_DDRCRCCCVOLTAGEUSED_STRUCT;

typedef CH2CCC_CR_DDRCRCTLCACOMPOFFSET_STRUCT CH1CCC_CR_DDRCRCTLCACOMPOFFSET_STRUCT;

typedef CH2CCC_CR_DDRCRVSSHICLKCOMPOFFSET_STRUCT CH1CCC_CR_DDRCRVSSHICLKCOMPOFFSET_STRUCT;

typedef CH2CCC_CR_CTL0_STRUCT CH1CCC_CR_CTL0_STRUCT;

typedef CH2CCC_CR_CTL3_STRUCT CH1CCC_CR_CTL3_STRUCT;

typedef DATA0CH0_CR_MDLLCTL4_STRUCT CH1CCC_CR_MDLLCTL4_STRUCT;

typedef CH2CCC_CR_CTL2_STRUCT CH1CCC_CR_CTL2_STRUCT;

typedef CH2CCC_CR_TXPBDCODE0_STRUCT CH1CCC_CR_TXPBDCODE0_STRUCT;

typedef CH2CCC_CR_TXPBDCODE1_STRUCT CH1CCC_CR_TXPBDCODE1_STRUCT;

typedef CH2CCC_CR_TXPBDCODE2_STRUCT CH1CCC_CR_TXPBDCODE2_STRUCT;

typedef CH2CCC_CR_TXPBDCODE3_STRUCT CH1CCC_CR_TXPBDCODE3_STRUCT;

typedef CH2CCC_CR_TXPBDCODE4_STRUCT CH1CCC_CR_TXPBDCODE4_STRUCT;

typedef DATA0CH0_CR_DCCCTL5_STRUCT CH1CCC_CR_DCCCTL6_STRUCT;

typedef DATA0CH0_CR_DCCCTL6_STRUCT CH1CCC_CR_DCCCTL7_STRUCT;

typedef CH2CCC_CR_DCCCTL8_STRUCT CH1CCC_CR_DCCCTL8_STRUCT;

typedef CH2CCC_CR_DCCCTL9_STRUCT CH1CCC_CR_DCCCTL9_STRUCT;

typedef DATA0CH0_CR_DCCCTL9_STRUCT CH1CCC_CR_DCCCTL11_STRUCT;

typedef CH2CCC_CR_DCCCTL20_STRUCT CH1CCC_CR_DCCCTL20_STRUCT;

typedef CH2CCC_CR_DDRCRCCCPIDIVIDER_STRUCT CH1CCC_CR_DDRCRCCCPIDIVIDER_STRUCT;

typedef DATA0CH0_CR_CLKALIGNCTL0_STRUCT CH1CCC_CR_CLKALIGNCTL0_STRUCT;

typedef DATA0CH0_CR_CLKALIGNCTL1_STRUCT CH1CCC_CR_CLKALIGNCTL1_STRUCT;

typedef CH2CCC_CR_CLKALIGNCTL2_STRUCT CH1CCC_CR_CLKALIGNCTL2_STRUCT;

typedef CH2CCC_CR_WLCTRL_STRUCT CH1CCC_CR_WLCTRL_STRUCT;

typedef CH2CCC_CR_DDRCRCCCCLKCONTROLS_STRUCT CH1CCC_CR_DDRCRCCCCLKCONTROLS_STRUCT;

typedef CH2CCC_CR_TXPBDOFFSET_STRUCT CH1CCC_CR_TXPBDOFFSET_STRUCT;

typedef DATA0CH0_CR_DCCCTL0_STRUCT CH1CCC_CR_DCCCTL0_STRUCT;

typedef DATA0CH0_CR_DCCCTL1_STRUCT CH1CCC_CR_DCCCTL1_STRUCT;

typedef CH2CCC_CR_DCCCTL2_STRUCT CH1CCC_CR_DCCCTL2_STRUCT;

typedef DATA0CH0_CR_DCCCTL3_STRUCT CH1CCC_CR_DCCCTL3_STRUCT;

typedef CH2CCC_CR_DCCCTL5_STRUCT CH1CCC_CR_DCCCTL5_STRUCT;

typedef DATA0CH0_CR_DCCCTL10_STRUCT CH1CCC_CR_DCCCTL13_STRUCT;

typedef CH2CCC_CR_PMFSM_STRUCT CH1CCC_CR_PMFSM_STRUCT;

typedef CH2CCC_CR_DDRCRCMDCOMP_STRUCT CH1CCC_CR_DDRCRCMDCOMP_STRUCT;

typedef CH2CCC_CR_DDRCRCLKCOMP_STRUCT CH1CCC_CR_DDRCRCLKCOMP_STRUCT;

typedef CH2CCC_CR_DDRCRCTLCOMP_STRUCT CH1CCC_CR_DDRCRCTLCOMP_STRUCT;

typedef CH2CCC_CR_VCCDLLCOMPDATACCC_STRUCT CH1CCC_CR_VCCDLLCOMPDATACCC_STRUCT;

typedef CH2CCC_CR_DCSOFFSET_STRUCT CH1CCC_CR_DCSOFFSET_STRUCT;

typedef CH2CCC_CR_DDRCRBSCANDATA_STRUCT CH1CCC_CR_DDRCRBSCANDATA_STRUCT;

typedef CH2CCC_CR_DDRCRMISR_STRUCT CH1CCC_CR_DDRCRMISR_STRUCT;

typedef CH2CCC_CR_DDRCRMARGINMODECONTROL_STRUCT CH1CCC_CR_DDRCRMARGINMODECONTROL_STRUCT;

typedef DATA0CH0_CR_DDRCRMARGINMODEDEBUGMSB0_STRUCT CH1CCC_CR_DDRCRMARGINMODEDEBUGMSB_STRUCT;

typedef DATA0CH0_CR_DDRCRMARGINMODEDEBUGMSB0_STRUCT CH1CCC_CR_DDRCRMARGINMODEDEBUGLSB_STRUCT;

typedef CH2CCC_CR_SRZBYPASSCTL_STRUCT CH1CCC_CR_SRZBYPASSCTL_STRUCT;

typedef CH2CCC_CR_SRZDRVENBYPASSCTL_STRUCT CH1CCC_CR_SRZDRVENBYPASSCTL_STRUCT;

typedef DATA0CH0_CR_DFXCCMON_STRUCT CH1CCC_CR_DFXCCMON_STRUCT;

typedef CH2CCC_CR_DCCCTL4_STRUCT CH1CCC_CR_DCCCTL4_STRUCT;

typedef CH2CCC_CR_DCCCTL17_STRUCT CH1CCC_CR_DCCCTL17_STRUCT;

typedef CH2CCC_CR_DCCCTL18_STRUCT CH1CCC_CR_DCCCTL18_STRUCT;

typedef DATA0CH0_CR_DCCCTL13_STRUCT CH1CCC_CR_DCCCTL19_STRUCT;

typedef CH2CCC_CR_CLKALIGNSTATUS_STRUCT CH1CCC_CR_CLKALIGNSTATUS_STRUCT;

typedef CH2CCC_CR_DDRCRPINSUSED_STRUCT CH5CCC_CR_DDRCRPINSUSED_STRUCT;

typedef CH2CCC_CR_CCCRSTCTL_STRUCT CH5CCC_CR_CCCRSTCTL_STRUCT;

typedef CH2CCC_CR_PICODE0_STRUCT CH5CCC_CR_PICODE0_STRUCT;

typedef CH2CCC_CR_PICODE1_STRUCT CH5CCC_CR_PICODE1_STRUCT;

typedef CH2CCC_CR_PICODE2_STRUCT CH5CCC_CR_PICODE2_STRUCT;

typedef CH2CCC_CR_PICODE3_STRUCT CH5CCC_CR_PICODE3_STRUCT;

typedef CH2CCC_CR_MDLLCTL0_STRUCT CH5CCC_CR_MDLLCTL0_STRUCT;

typedef CH2CCC_CR_MDLLCTL1_STRUCT CH5CCC_CR_MDLLCTL1_STRUCT;

typedef CH2CCC_CR_MDLLCTL2_STRUCT CH5CCC_CR_MDLLCTL2_STRUCT;

typedef CH2CCC_CR_MDLLCTL3_STRUCT CH5CCC_CR_MDLLCTL3_STRUCT;

typedef CH2CCC_CR_COMP2_STRUCT CH5CCC_CR_COMP2_STRUCT;

typedef CH2CCC_CR_COMP3_STRUCT CH5CCC_CR_COMP3_STRUCT;

typedef CH2CCC_CR_DDRCRCCCVOLTAGEUSED_STRUCT CH5CCC_CR_DDRCRCCCVOLTAGEUSED_STRUCT;

typedef CH2CCC_CR_DDRCRCTLCACOMPOFFSET_STRUCT CH5CCC_CR_DDRCRCTLCACOMPOFFSET_STRUCT;

typedef CH2CCC_CR_DDRCRVSSHICLKCOMPOFFSET_STRUCT CH5CCC_CR_DDRCRVSSHICLKCOMPOFFSET_STRUCT;

typedef CH2CCC_CR_CTL0_STRUCT CH5CCC_CR_CTL0_STRUCT;

typedef CH2CCC_CR_CTL3_STRUCT CH5CCC_CR_CTL3_STRUCT;

typedef DATA0CH0_CR_MDLLCTL4_STRUCT CH5CCC_CR_MDLLCTL4_STRUCT;

typedef CH2CCC_CR_CTL2_STRUCT CH5CCC_CR_CTL2_STRUCT;

typedef CH2CCC_CR_TXPBDCODE0_STRUCT CH5CCC_CR_TXPBDCODE0_STRUCT;

typedef CH2CCC_CR_TXPBDCODE1_STRUCT CH5CCC_CR_TXPBDCODE1_STRUCT;

typedef CH2CCC_CR_TXPBDCODE2_STRUCT CH5CCC_CR_TXPBDCODE2_STRUCT;

typedef CH2CCC_CR_TXPBDCODE3_STRUCT CH5CCC_CR_TXPBDCODE3_STRUCT;

typedef CH2CCC_CR_TXPBDCODE4_STRUCT CH5CCC_CR_TXPBDCODE4_STRUCT;

typedef DATA0CH0_CR_DCCCTL5_STRUCT CH5CCC_CR_DCCCTL6_STRUCT;

typedef DATA0CH0_CR_DCCCTL6_STRUCT CH5CCC_CR_DCCCTL7_STRUCT;

typedef CH2CCC_CR_DCCCTL8_STRUCT CH5CCC_CR_DCCCTL8_STRUCT;

typedef CH2CCC_CR_DCCCTL9_STRUCT CH5CCC_CR_DCCCTL9_STRUCT;

typedef DATA0CH0_CR_DCCCTL9_STRUCT CH5CCC_CR_DCCCTL11_STRUCT;

typedef CH2CCC_CR_DCCCTL20_STRUCT CH5CCC_CR_DCCCTL20_STRUCT;

typedef CH2CCC_CR_DDRCRCCCPIDIVIDER_STRUCT CH5CCC_CR_DDRCRCCCPIDIVIDER_STRUCT;

typedef DATA0CH0_CR_CLKALIGNCTL0_STRUCT CH5CCC_CR_CLKALIGNCTL0_STRUCT;

typedef DATA0CH0_CR_CLKALIGNCTL1_STRUCT CH5CCC_CR_CLKALIGNCTL1_STRUCT;

typedef CH2CCC_CR_CLKALIGNCTL2_STRUCT CH5CCC_CR_CLKALIGNCTL2_STRUCT;

typedef CH2CCC_CR_WLCTRL_STRUCT CH5CCC_CR_WLCTRL_STRUCT;

typedef CH2CCC_CR_DDRCRCCCCLKCONTROLS_STRUCT CH5CCC_CR_DDRCRCCCCLKCONTROLS_STRUCT;

typedef CH2CCC_CR_TXPBDOFFSET_STRUCT CH5CCC_CR_TXPBDOFFSET_STRUCT;

typedef DATA0CH0_CR_DCCCTL0_STRUCT CH5CCC_CR_DCCCTL0_STRUCT;

typedef DATA0CH0_CR_DCCCTL1_STRUCT CH5CCC_CR_DCCCTL1_STRUCT;

typedef CH2CCC_CR_DCCCTL2_STRUCT CH5CCC_CR_DCCCTL2_STRUCT;

typedef DATA0CH0_CR_DCCCTL3_STRUCT CH5CCC_CR_DCCCTL3_STRUCT;

typedef CH2CCC_CR_DCCCTL5_STRUCT CH5CCC_CR_DCCCTL5_STRUCT;

typedef DATA0CH0_CR_DCCCTL10_STRUCT CH5CCC_CR_DCCCTL13_STRUCT;

typedef CH2CCC_CR_PMFSM_STRUCT CH5CCC_CR_PMFSM_STRUCT;

typedef CH2CCC_CR_DDRCRCMDCOMP_STRUCT CH5CCC_CR_DDRCRCMDCOMP_STRUCT;

typedef CH2CCC_CR_DDRCRCLKCOMP_STRUCT CH5CCC_CR_DDRCRCLKCOMP_STRUCT;

typedef CH2CCC_CR_DDRCRCTLCOMP_STRUCT CH5CCC_CR_DDRCRCTLCOMP_STRUCT;

typedef CH2CCC_CR_VCCDLLCOMPDATACCC_STRUCT CH5CCC_CR_VCCDLLCOMPDATACCC_STRUCT;

typedef CH2CCC_CR_DCSOFFSET_STRUCT CH5CCC_CR_DCSOFFSET_STRUCT;

typedef CH2CCC_CR_DDRCRBSCANDATA_STRUCT CH5CCC_CR_DDRCRBSCANDATA_STRUCT;

typedef CH2CCC_CR_DDRCRMISR_STRUCT CH5CCC_CR_DDRCRMISR_STRUCT;

typedef CH2CCC_CR_DDRCRMARGINMODECONTROL_STRUCT CH5CCC_CR_DDRCRMARGINMODECONTROL_STRUCT;

typedef DATA0CH0_CR_DDRCRMARGINMODEDEBUGMSB0_STRUCT CH5CCC_CR_DDRCRMARGINMODEDEBUGMSB_STRUCT;

typedef DATA0CH0_CR_DDRCRMARGINMODEDEBUGMSB0_STRUCT CH5CCC_CR_DDRCRMARGINMODEDEBUGLSB_STRUCT;

typedef CH2CCC_CR_SRZBYPASSCTL_STRUCT CH5CCC_CR_SRZBYPASSCTL_STRUCT;

typedef CH2CCC_CR_SRZDRVENBYPASSCTL_STRUCT CH5CCC_CR_SRZDRVENBYPASSCTL_STRUCT;

typedef DATA0CH0_CR_DFXCCMON_STRUCT CH5CCC_CR_DFXCCMON_STRUCT;

typedef CH2CCC_CR_DCCCTL4_STRUCT CH5CCC_CR_DCCCTL4_STRUCT;

typedef CH2CCC_CR_DCCCTL17_STRUCT CH5CCC_CR_DCCCTL17_STRUCT;

typedef CH2CCC_CR_DCCCTL18_STRUCT CH5CCC_CR_DCCCTL18_STRUCT;

typedef DATA0CH0_CR_DCCCTL13_STRUCT CH5CCC_CR_DCCCTL19_STRUCT;

typedef CH2CCC_CR_CLKALIGNSTATUS_STRUCT CH5CCC_CR_CLKALIGNSTATUS_STRUCT;

typedef CH2CCC_CR_DDRCRPINSUSED_STRUCT CH7CCC_CR_DDRCRPINSUSED_STRUCT;

typedef CH2CCC_CR_CCCRSTCTL_STRUCT CH7CCC_CR_CCCRSTCTL_STRUCT;

typedef CH2CCC_CR_PICODE0_STRUCT CH7CCC_CR_PICODE0_STRUCT;

typedef CH2CCC_CR_PICODE1_STRUCT CH7CCC_CR_PICODE1_STRUCT;

typedef CH2CCC_CR_PICODE2_STRUCT CH7CCC_CR_PICODE2_STRUCT;

typedef CH2CCC_CR_PICODE3_STRUCT CH7CCC_CR_PICODE3_STRUCT;

typedef CH2CCC_CR_MDLLCTL0_STRUCT CH7CCC_CR_MDLLCTL0_STRUCT;

typedef CH2CCC_CR_MDLLCTL1_STRUCT CH7CCC_CR_MDLLCTL1_STRUCT;

typedef CH2CCC_CR_MDLLCTL2_STRUCT CH7CCC_CR_MDLLCTL2_STRUCT;

typedef CH2CCC_CR_MDLLCTL3_STRUCT CH7CCC_CR_MDLLCTL3_STRUCT;

typedef CH2CCC_CR_COMP2_STRUCT CH7CCC_CR_COMP2_STRUCT;

typedef CH2CCC_CR_COMP3_STRUCT CH7CCC_CR_COMP3_STRUCT;

typedef CH2CCC_CR_DDRCRCCCVOLTAGEUSED_STRUCT CH7CCC_CR_DDRCRCCCVOLTAGEUSED_STRUCT;

typedef CH2CCC_CR_DDRCRCTLCACOMPOFFSET_STRUCT CH7CCC_CR_DDRCRCTLCACOMPOFFSET_STRUCT;

typedef CH2CCC_CR_DDRCRVSSHICLKCOMPOFFSET_STRUCT CH7CCC_CR_DDRCRVSSHICLKCOMPOFFSET_STRUCT;

typedef CH2CCC_CR_CTL0_STRUCT CH7CCC_CR_CTL0_STRUCT;

typedef CH2CCC_CR_CTL3_STRUCT CH7CCC_CR_CTL3_STRUCT;

typedef DATA0CH0_CR_MDLLCTL4_STRUCT CH7CCC_CR_MDLLCTL4_STRUCT;

typedef CH2CCC_CR_CTL2_STRUCT CH7CCC_CR_CTL2_STRUCT;

typedef CH2CCC_CR_TXPBDCODE0_STRUCT CH7CCC_CR_TXPBDCODE0_STRUCT;

typedef CH2CCC_CR_TXPBDCODE1_STRUCT CH7CCC_CR_TXPBDCODE1_STRUCT;

typedef CH2CCC_CR_TXPBDCODE2_STRUCT CH7CCC_CR_TXPBDCODE2_STRUCT;

typedef CH2CCC_CR_TXPBDCODE3_STRUCT CH7CCC_CR_TXPBDCODE3_STRUCT;

typedef CH2CCC_CR_TXPBDCODE4_STRUCT CH7CCC_CR_TXPBDCODE4_STRUCT;

typedef DATA0CH0_CR_DCCCTL5_STRUCT CH7CCC_CR_DCCCTL6_STRUCT;

typedef DATA0CH0_CR_DCCCTL6_STRUCT CH7CCC_CR_DCCCTL7_STRUCT;

typedef CH2CCC_CR_DCCCTL8_STRUCT CH7CCC_CR_DCCCTL8_STRUCT;

typedef CH2CCC_CR_DCCCTL9_STRUCT CH7CCC_CR_DCCCTL9_STRUCT;

typedef DATA0CH0_CR_DCCCTL9_STRUCT CH7CCC_CR_DCCCTL11_STRUCT;

typedef CH2CCC_CR_DCCCTL20_STRUCT CH7CCC_CR_DCCCTL20_STRUCT;

typedef CH2CCC_CR_DDRCRCCCPIDIVIDER_STRUCT CH7CCC_CR_DDRCRCCCPIDIVIDER_STRUCT;

typedef DATA0CH0_CR_CLKALIGNCTL0_STRUCT CH7CCC_CR_CLKALIGNCTL0_STRUCT;

typedef DATA0CH0_CR_CLKALIGNCTL1_STRUCT CH7CCC_CR_CLKALIGNCTL1_STRUCT;

typedef CH2CCC_CR_CLKALIGNCTL2_STRUCT CH7CCC_CR_CLKALIGNCTL2_STRUCT;

typedef CH2CCC_CR_WLCTRL_STRUCT CH7CCC_CR_WLCTRL_STRUCT;

typedef CH2CCC_CR_DDRCRCCCCLKCONTROLS_STRUCT CH7CCC_CR_DDRCRCCCCLKCONTROLS_STRUCT;

typedef CH2CCC_CR_TXPBDOFFSET_STRUCT CH7CCC_CR_TXPBDOFFSET_STRUCT;

typedef DATA0CH0_CR_DCCCTL0_STRUCT CH7CCC_CR_DCCCTL0_STRUCT;

typedef DATA0CH0_CR_DCCCTL1_STRUCT CH7CCC_CR_DCCCTL1_STRUCT;

typedef CH2CCC_CR_DCCCTL2_STRUCT CH7CCC_CR_DCCCTL2_STRUCT;

typedef DATA0CH0_CR_DCCCTL3_STRUCT CH7CCC_CR_DCCCTL3_STRUCT;

typedef CH2CCC_CR_DCCCTL5_STRUCT CH7CCC_CR_DCCCTL5_STRUCT;

typedef DATA0CH0_CR_DCCCTL10_STRUCT CH7CCC_CR_DCCCTL13_STRUCT;

typedef CH2CCC_CR_PMFSM_STRUCT CH7CCC_CR_PMFSM_STRUCT;

typedef CH2CCC_CR_DDRCRCMDCOMP_STRUCT CH7CCC_CR_DDRCRCMDCOMP_STRUCT;

typedef CH2CCC_CR_DDRCRCLKCOMP_STRUCT CH7CCC_CR_DDRCRCLKCOMP_STRUCT;

typedef CH2CCC_CR_DDRCRCTLCOMP_STRUCT CH7CCC_CR_DDRCRCTLCOMP_STRUCT;

typedef CH2CCC_CR_VCCDLLCOMPDATACCC_STRUCT CH7CCC_CR_VCCDLLCOMPDATACCC_STRUCT;

typedef CH2CCC_CR_DCSOFFSET_STRUCT CH7CCC_CR_DCSOFFSET_STRUCT;

typedef CH2CCC_CR_DDRCRBSCANDATA_STRUCT CH7CCC_CR_DDRCRBSCANDATA_STRUCT;

typedef CH2CCC_CR_DDRCRMISR_STRUCT CH7CCC_CR_DDRCRMISR_STRUCT;

typedef CH2CCC_CR_DDRCRMARGINMODECONTROL_STRUCT CH7CCC_CR_DDRCRMARGINMODECONTROL_STRUCT;

typedef DATA0CH0_CR_DDRCRMARGINMODEDEBUGMSB0_STRUCT CH7CCC_CR_DDRCRMARGINMODEDEBUGMSB_STRUCT;

typedef DATA0CH0_CR_DDRCRMARGINMODEDEBUGMSB0_STRUCT CH7CCC_CR_DDRCRMARGINMODEDEBUGLSB_STRUCT;

typedef CH2CCC_CR_SRZBYPASSCTL_STRUCT CH7CCC_CR_SRZBYPASSCTL_STRUCT;

typedef CH2CCC_CR_SRZDRVENBYPASSCTL_STRUCT CH7CCC_CR_SRZDRVENBYPASSCTL_STRUCT;

typedef DATA0CH0_CR_DFXCCMON_STRUCT CH7CCC_CR_DFXCCMON_STRUCT;

typedef CH2CCC_CR_DCCCTL4_STRUCT CH7CCC_CR_DCCCTL4_STRUCT;

typedef CH2CCC_CR_DCCCTL17_STRUCT CH7CCC_CR_DCCCTL17_STRUCT;

typedef CH2CCC_CR_DCCCTL18_STRUCT CH7CCC_CR_DCCCTL18_STRUCT;

typedef DATA0CH0_CR_DCCCTL13_STRUCT CH7CCC_CR_DCCCTL19_STRUCT;

typedef CH2CCC_CR_CLKALIGNSTATUS_STRUCT CH7CCC_CR_CLKALIGNSTATUS_STRUCT;

typedef CH2CCC_CR_DDRCRPINSUSED_STRUCT CH4CCC_CR_DDRCRPINSUSED_STRUCT;

typedef CH2CCC_CR_CCCRSTCTL_STRUCT CH4CCC_CR_CCCRSTCTL_STRUCT;

typedef CH2CCC_CR_PICODE0_STRUCT CH4CCC_CR_PICODE0_STRUCT;

typedef CH2CCC_CR_PICODE1_STRUCT CH4CCC_CR_PICODE1_STRUCT;

typedef CH2CCC_CR_PICODE2_STRUCT CH4CCC_CR_PICODE2_STRUCT;

typedef CH2CCC_CR_PICODE3_STRUCT CH4CCC_CR_PICODE3_STRUCT;

typedef CH2CCC_CR_MDLLCTL0_STRUCT CH4CCC_CR_MDLLCTL0_STRUCT;

typedef CH2CCC_CR_MDLLCTL1_STRUCT CH4CCC_CR_MDLLCTL1_STRUCT;

typedef CH2CCC_CR_MDLLCTL2_STRUCT CH4CCC_CR_MDLLCTL2_STRUCT;

typedef CH2CCC_CR_MDLLCTL3_STRUCT CH4CCC_CR_MDLLCTL3_STRUCT;

typedef CH2CCC_CR_COMP2_STRUCT CH4CCC_CR_COMP2_STRUCT;

typedef CH2CCC_CR_COMP3_STRUCT CH4CCC_CR_COMP3_STRUCT;

typedef CH2CCC_CR_DDRCRCCCVOLTAGEUSED_STRUCT CH4CCC_CR_DDRCRCCCVOLTAGEUSED_STRUCT;

typedef CH2CCC_CR_DDRCRCTLCACOMPOFFSET_STRUCT CH4CCC_CR_DDRCRCTLCACOMPOFFSET_STRUCT;

typedef CH2CCC_CR_DDRCRVSSHICLKCOMPOFFSET_STRUCT CH4CCC_CR_DDRCRVSSHICLKCOMPOFFSET_STRUCT;

typedef CH2CCC_CR_CTL0_STRUCT CH4CCC_CR_CTL0_STRUCT;

typedef CH2CCC_CR_CTL3_STRUCT CH4CCC_CR_CTL3_STRUCT;

typedef DATA0CH0_CR_MDLLCTL4_STRUCT CH4CCC_CR_MDLLCTL4_STRUCT;

typedef CH2CCC_CR_CTL2_STRUCT CH4CCC_CR_CTL2_STRUCT;

typedef CH2CCC_CR_TXPBDCODE0_STRUCT CH4CCC_CR_TXPBDCODE0_STRUCT;

typedef CH2CCC_CR_TXPBDCODE1_STRUCT CH4CCC_CR_TXPBDCODE1_STRUCT;

typedef CH2CCC_CR_TXPBDCODE2_STRUCT CH4CCC_CR_TXPBDCODE2_STRUCT;

typedef CH2CCC_CR_TXPBDCODE3_STRUCT CH4CCC_CR_TXPBDCODE3_STRUCT;

typedef CH2CCC_CR_TXPBDCODE4_STRUCT CH4CCC_CR_TXPBDCODE4_STRUCT;

typedef DATA0CH0_CR_DCCCTL5_STRUCT CH4CCC_CR_DCCCTL6_STRUCT;

typedef DATA0CH0_CR_DCCCTL6_STRUCT CH4CCC_CR_DCCCTL7_STRUCT;

typedef CH2CCC_CR_DCCCTL8_STRUCT CH4CCC_CR_DCCCTL8_STRUCT;

typedef CH2CCC_CR_DCCCTL9_STRUCT CH4CCC_CR_DCCCTL9_STRUCT;

typedef DATA0CH0_CR_DCCCTL9_STRUCT CH4CCC_CR_DCCCTL11_STRUCT;

typedef CH2CCC_CR_DCCCTL20_STRUCT CH4CCC_CR_DCCCTL20_STRUCT;

typedef CH2CCC_CR_DDRCRCCCPIDIVIDER_STRUCT CH4CCC_CR_DDRCRCCCPIDIVIDER_STRUCT;

typedef DATA0CH0_CR_CLKALIGNCTL0_STRUCT CH4CCC_CR_CLKALIGNCTL0_STRUCT;

typedef DATA0CH0_CR_CLKALIGNCTL1_STRUCT CH4CCC_CR_CLKALIGNCTL1_STRUCT;

typedef CH2CCC_CR_CLKALIGNCTL2_STRUCT CH4CCC_CR_CLKALIGNCTL2_STRUCT;

typedef CH2CCC_CR_WLCTRL_STRUCT CH4CCC_CR_WLCTRL_STRUCT;

typedef CH2CCC_CR_DDRCRCCCCLKCONTROLS_STRUCT CH4CCC_CR_DDRCRCCCCLKCONTROLS_STRUCT;

typedef CH2CCC_CR_TXPBDOFFSET_STRUCT CH4CCC_CR_TXPBDOFFSET_STRUCT;

typedef DATA0CH0_CR_DCCCTL0_STRUCT CH4CCC_CR_DCCCTL0_STRUCT;

typedef DATA0CH0_CR_DCCCTL1_STRUCT CH4CCC_CR_DCCCTL1_STRUCT;

typedef CH2CCC_CR_DCCCTL2_STRUCT CH4CCC_CR_DCCCTL2_STRUCT;

typedef DATA0CH0_CR_DCCCTL3_STRUCT CH4CCC_CR_DCCCTL3_STRUCT;

typedef CH2CCC_CR_DCCCTL5_STRUCT CH4CCC_CR_DCCCTL5_STRUCT;

typedef DATA0CH0_CR_DCCCTL10_STRUCT CH4CCC_CR_DCCCTL13_STRUCT;

typedef CH2CCC_CR_PMFSM_STRUCT CH4CCC_CR_PMFSM_STRUCT;

typedef CH2CCC_CR_DDRCRCMDCOMP_STRUCT CH4CCC_CR_DDRCRCMDCOMP_STRUCT;

typedef CH2CCC_CR_DDRCRCLKCOMP_STRUCT CH4CCC_CR_DDRCRCLKCOMP_STRUCT;

typedef CH2CCC_CR_DDRCRCTLCOMP_STRUCT CH4CCC_CR_DDRCRCTLCOMP_STRUCT;

typedef CH2CCC_CR_VCCDLLCOMPDATACCC_STRUCT CH4CCC_CR_VCCDLLCOMPDATACCC_STRUCT;

typedef CH2CCC_CR_DCSOFFSET_STRUCT CH4CCC_CR_DCSOFFSET_STRUCT;

typedef CH2CCC_CR_DDRCRBSCANDATA_STRUCT CH4CCC_CR_DDRCRBSCANDATA_STRUCT;

typedef CH2CCC_CR_DDRCRMISR_STRUCT CH4CCC_CR_DDRCRMISR_STRUCT;

typedef CH2CCC_CR_DDRCRMARGINMODECONTROL_STRUCT CH4CCC_CR_DDRCRMARGINMODECONTROL_STRUCT;

typedef DATA0CH0_CR_DDRCRMARGINMODEDEBUGMSB0_STRUCT CH4CCC_CR_DDRCRMARGINMODEDEBUGMSB_STRUCT;

typedef DATA0CH0_CR_DDRCRMARGINMODEDEBUGMSB0_STRUCT CH4CCC_CR_DDRCRMARGINMODEDEBUGLSB_STRUCT;

typedef CH2CCC_CR_SRZBYPASSCTL_STRUCT CH4CCC_CR_SRZBYPASSCTL_STRUCT;

typedef CH2CCC_CR_SRZDRVENBYPASSCTL_STRUCT CH4CCC_CR_SRZDRVENBYPASSCTL_STRUCT;

typedef DATA0CH0_CR_DFXCCMON_STRUCT CH4CCC_CR_DFXCCMON_STRUCT;

typedef CH2CCC_CR_DCCCTL4_STRUCT CH4CCC_CR_DCCCTL4_STRUCT;

typedef CH2CCC_CR_DCCCTL17_STRUCT CH4CCC_CR_DCCCTL17_STRUCT;

typedef CH2CCC_CR_DCCCTL18_STRUCT CH4CCC_CR_DCCCTL18_STRUCT;

typedef DATA0CH0_CR_DCCCTL13_STRUCT CH4CCC_CR_DCCCTL19_STRUCT;

typedef CH2CCC_CR_CLKALIGNSTATUS_STRUCT CH4CCC_CR_CLKALIGNSTATUS_STRUCT;

typedef CH2CCC_CR_DDRCRPINSUSED_STRUCT CH6CCC_CR_DDRCRPINSUSED_STRUCT;

typedef CH2CCC_CR_CCCRSTCTL_STRUCT CH6CCC_CR_CCCRSTCTL_STRUCT;

typedef CH2CCC_CR_PICODE0_STRUCT CH6CCC_CR_PICODE0_STRUCT;

typedef CH2CCC_CR_PICODE1_STRUCT CH6CCC_CR_PICODE1_STRUCT;

typedef CH2CCC_CR_PICODE2_STRUCT CH6CCC_CR_PICODE2_STRUCT;

typedef CH2CCC_CR_PICODE3_STRUCT CH6CCC_CR_PICODE3_STRUCT;

typedef CH2CCC_CR_MDLLCTL0_STRUCT CH6CCC_CR_MDLLCTL0_STRUCT;

typedef CH2CCC_CR_MDLLCTL1_STRUCT CH6CCC_CR_MDLLCTL1_STRUCT;

typedef CH2CCC_CR_MDLLCTL2_STRUCT CH6CCC_CR_MDLLCTL2_STRUCT;

typedef CH2CCC_CR_MDLLCTL3_STRUCT CH6CCC_CR_MDLLCTL3_STRUCT;

typedef CH2CCC_CR_COMP2_STRUCT CH6CCC_CR_COMP2_STRUCT;

typedef CH2CCC_CR_COMP3_STRUCT CH6CCC_CR_COMP3_STRUCT;

typedef CH2CCC_CR_DDRCRCCCVOLTAGEUSED_STRUCT CH6CCC_CR_DDRCRCCCVOLTAGEUSED_STRUCT;

typedef CH2CCC_CR_DDRCRCTLCACOMPOFFSET_STRUCT CH6CCC_CR_DDRCRCTLCACOMPOFFSET_STRUCT;

typedef CH2CCC_CR_DDRCRVSSHICLKCOMPOFFSET_STRUCT CH6CCC_CR_DDRCRVSSHICLKCOMPOFFSET_STRUCT;

typedef CH2CCC_CR_CTL0_STRUCT CH6CCC_CR_CTL0_STRUCT;

typedef CH2CCC_CR_CTL3_STRUCT CH6CCC_CR_CTL3_STRUCT;

typedef DATA0CH0_CR_MDLLCTL4_STRUCT CH6CCC_CR_MDLLCTL4_STRUCT;

typedef CH2CCC_CR_CTL2_STRUCT CH6CCC_CR_CTL2_STRUCT;

typedef CH2CCC_CR_TXPBDCODE0_STRUCT CH6CCC_CR_TXPBDCODE0_STRUCT;

typedef CH2CCC_CR_TXPBDCODE1_STRUCT CH6CCC_CR_TXPBDCODE1_STRUCT;

typedef CH2CCC_CR_TXPBDCODE2_STRUCT CH6CCC_CR_TXPBDCODE2_STRUCT;

typedef CH2CCC_CR_TXPBDCODE3_STRUCT CH6CCC_CR_TXPBDCODE3_STRUCT;

typedef CH2CCC_CR_TXPBDCODE4_STRUCT CH6CCC_CR_TXPBDCODE4_STRUCT;

typedef DATA0CH0_CR_DCCCTL5_STRUCT CH6CCC_CR_DCCCTL6_STRUCT;

typedef DATA0CH0_CR_DCCCTL6_STRUCT CH6CCC_CR_DCCCTL7_STRUCT;

typedef CH2CCC_CR_DCCCTL8_STRUCT CH6CCC_CR_DCCCTL8_STRUCT;

typedef CH2CCC_CR_DCCCTL9_STRUCT CH6CCC_CR_DCCCTL9_STRUCT;

typedef DATA0CH0_CR_DCCCTL9_STRUCT CH6CCC_CR_DCCCTL11_STRUCT;

typedef CH2CCC_CR_DCCCTL20_STRUCT CH6CCC_CR_DCCCTL20_STRUCT;

typedef CH2CCC_CR_DDRCRCCCPIDIVIDER_STRUCT CH6CCC_CR_DDRCRCCCPIDIVIDER_STRUCT;

typedef DATA0CH0_CR_CLKALIGNCTL0_STRUCT CH6CCC_CR_CLKALIGNCTL0_STRUCT;

typedef DATA0CH0_CR_CLKALIGNCTL1_STRUCT CH6CCC_CR_CLKALIGNCTL1_STRUCT;

typedef CH2CCC_CR_CLKALIGNCTL2_STRUCT CH6CCC_CR_CLKALIGNCTL2_STRUCT;

typedef CH2CCC_CR_WLCTRL_STRUCT CH6CCC_CR_WLCTRL_STRUCT;

typedef CH2CCC_CR_DDRCRCCCCLKCONTROLS_STRUCT CH6CCC_CR_DDRCRCCCCLKCONTROLS_STRUCT;

typedef CH2CCC_CR_TXPBDOFFSET_STRUCT CH6CCC_CR_TXPBDOFFSET_STRUCT;

typedef DATA0CH0_CR_DCCCTL0_STRUCT CH6CCC_CR_DCCCTL0_STRUCT;

typedef DATA0CH0_CR_DCCCTL1_STRUCT CH6CCC_CR_DCCCTL1_STRUCT;

typedef CH2CCC_CR_DCCCTL2_STRUCT CH6CCC_CR_DCCCTL2_STRUCT;

typedef DATA0CH0_CR_DCCCTL3_STRUCT CH6CCC_CR_DCCCTL3_STRUCT;

typedef CH2CCC_CR_DCCCTL5_STRUCT CH6CCC_CR_DCCCTL5_STRUCT;

typedef DATA0CH0_CR_DCCCTL10_STRUCT CH6CCC_CR_DCCCTL13_STRUCT;

typedef CH2CCC_CR_PMFSM_STRUCT CH6CCC_CR_PMFSM_STRUCT;

typedef CH2CCC_CR_DDRCRCMDCOMP_STRUCT CH6CCC_CR_DDRCRCMDCOMP_STRUCT;

typedef CH2CCC_CR_DDRCRCLKCOMP_STRUCT CH6CCC_CR_DDRCRCLKCOMP_STRUCT;

typedef CH2CCC_CR_DDRCRCTLCOMP_STRUCT CH6CCC_CR_DDRCRCTLCOMP_STRUCT;

typedef CH2CCC_CR_VCCDLLCOMPDATACCC_STRUCT CH6CCC_CR_VCCDLLCOMPDATACCC_STRUCT;

typedef CH2CCC_CR_DCSOFFSET_STRUCT CH6CCC_CR_DCSOFFSET_STRUCT;

typedef CH2CCC_CR_DDRCRBSCANDATA_STRUCT CH6CCC_CR_DDRCRBSCANDATA_STRUCT;

typedef CH2CCC_CR_DDRCRMISR_STRUCT CH6CCC_CR_DDRCRMISR_STRUCT;

typedef CH2CCC_CR_DDRCRMARGINMODECONTROL_STRUCT CH6CCC_CR_DDRCRMARGINMODECONTROL_STRUCT;

typedef DATA0CH0_CR_DDRCRMARGINMODEDEBUGMSB0_STRUCT CH6CCC_CR_DDRCRMARGINMODEDEBUGMSB_STRUCT;

typedef DATA0CH0_CR_DDRCRMARGINMODEDEBUGMSB0_STRUCT CH6CCC_CR_DDRCRMARGINMODEDEBUGLSB_STRUCT;

typedef CH2CCC_CR_SRZBYPASSCTL_STRUCT CH6CCC_CR_SRZBYPASSCTL_STRUCT;

typedef CH2CCC_CR_SRZDRVENBYPASSCTL_STRUCT CH6CCC_CR_SRZDRVENBYPASSCTL_STRUCT;

typedef DATA0CH0_CR_DFXCCMON_STRUCT CH6CCC_CR_DFXCCMON_STRUCT;

typedef CH2CCC_CR_DCCCTL4_STRUCT CH6CCC_CR_DCCCTL4_STRUCT;

typedef CH2CCC_CR_DCCCTL17_STRUCT CH6CCC_CR_DCCCTL17_STRUCT;

typedef CH2CCC_CR_DCCCTL18_STRUCT CH6CCC_CR_DCCCTL18_STRUCT;

typedef DATA0CH0_CR_DCCCTL13_STRUCT CH6CCC_CR_DCCCTL19_STRUCT;

typedef CH2CCC_CR_CLKALIGNSTATUS_STRUCT CH6CCC_CR_CLKALIGNSTATUS_STRUCT;
typedef union {
  struct {
    UINT32 pnccomp_vttpanicup_vrefcode             :  8;  // Bits 7:0
    UINT32 pnccomp_vttpanicdn_vrefcode             :  8;  // Bits 15:8
    UINT32 pnccomp_cmddn200_vrefcode               :  8;  // Bits 23:16
    UINT32 pnccomp_rloaddqs_vrefcode               :  8;  // Bits 31:24
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} DDRPHY_COMP_NEW_CR_PNCCOMP_CTRL3_STRUCT;
typedef union {
  struct {
    UINT32 Vsshiff_rx                              :  6;  // Bits 5:0
    UINT32 Dqs_fwdclkn                             :  7;  // Bits 12:6
    UINT32 Dqs_fwdclkp                             :  7;  // Bits 19:13
    UINT32 Dll_coderead                            :  7;  // Bits 26:20
    UINT32 Dqsntargetnui                           :  3;  // Bits 29:27
    UINT32 Dqsnoffsetnui                           :  2;  // Bits 31:30
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} DDRPHY_COMP_NEW_CR_DDRCRDATACOMP1_STRUCT;
typedef union {
  struct {
    UINT32 panicvttdn0                             :  8;  // Bits 7:0
    UINT32 panicvttdn1                             :  8;  // Bits 15:8
    UINT32 panicvttup0                             :  8;  // Bits 23:16
    UINT32 panicvttup1                             :  8;  // Bits 31:24
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} DDRPHY_COMP_NEW_CR_DDRCRDATACOMPVTT_STRUCT;
typedef union {
  struct {
    UINT32 Dll_codepi                              :  6;  // Bits 5:0
    UINT32 Dll_codewl                              :  6;  // Bits 11:6
    UINT32 Dll_bwsel                               :  6;  // Bits 17:12
    UINT32 Dll_vdlllock                            :  8;  // Bits 25:18
    UINT32 Dqspoffsetnui                           :  2;  // Bits 27:26
    UINT32 Dqsptargetnui                           :  3;  // Bits 30:28
    UINT32 Spare                                   :  1;  // Bits 31:31
  } Bits;
  struct {
    UINT32                                         :  18;  // Bits 17:0
    UINT32 rloaddqs                                :  6;  // Bits 23:18
    UINT32 cben                                    :  2;  // Bits 25:24
  } BitsQ0;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} DDRPHY_COMP_NEW_CR_VCCDLLCOMPDATACCC_STRUCT;
typedef union {
  struct {
    UINT32 hibwdivider                             :  2;  // Bits 1:0
    UINT32 lobwdivider                             :  2;  // Bits 3:2
    UINT32 sampledivider                           :  2;  // Bits 5:4
    UINT32 openloop                                :  1;  // Bits 6:6
    UINT32 slowbwerror                             :  2;  // Bits 8:7
    UINT32 hibwenable                              :  1;  // Bits 9:9
    UINT32 vtslope                                 :  5;  // Bits 14:10
    INT32  vtoffset                                :  5;  // Bits 19:15
    UINT32 selcode                                 :  4;  // Bits 23:20
    UINT32 outputcode                              :  8;  // Bits 31:24
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} DDRPHY_COMP_NEW_CR_DIMMVREF_VREFCONTROL_STRUCT;
typedef union {
  struct {
    UINT32 ca0vref                                 :  9;  // Bits 8:0
    UINT32 ca1vref                                 :  9;  // Bits 17:9
    UINT32 qxcount                                 :  6;  // Bits 23:18
    UINT32 Reserved0                               :  3;  // Bits 26:24
    UINT32 enca0vref                               :  1;  // Bits 27:27
    UINT32 enca1vref                               :  1;  // Bits 28:28
    UINT32 sagvopenloopen                          :  1;  // Bits 29:29
    UINT32 sagvlockcodectl                         :  1;  // Bits 30:30
    UINT32 sagvfastbwdisable                       :  1;  // Bits 31:31
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} DDRPHY_COMP_NEW_CR_DIMMVREF_VREFCH0_STRUCT;
typedef union {
  struct {
    UINT32 ca0vref                                 :  9;  // Bits 8:0
    UINT32 ca1vref                                 :  9;  // Bits 17:9
    UINT32 Reserved02                              :  9;  // Bits 26:18
    UINT32 enca0vref                               :  1;  // Bits 27:27
    UINT32 enca1vref                               :  1;  // Bits 28:28
    UINT32 Reserved11                              :  3;  // Bits 31:29
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} DDRPHY_COMP_NEW_CR_DIMMVREF_VREFCH1_STRUCT;
typedef union {
  struct {
    UINT32 hiztimerctrl                            :  2;  // Bits 1:0
    UINT32 ca00slowbw                              :  1;  // Bits 2:2
    UINT32 ca01slowbw                              :  1;  // Bits 3:3
    UINT32 lockovrd                                :  2;  // Bits 5:4
    UINT32 locktimer                               :  3;  // Bits 8:6
    UINT32 vtslopesagv                             :  5;  // Bits 13:9
    UINT32 vtoffsetsagv                            :  5;  // Bits 18:14
    UINT32 gateicindvfs                            :  1;  // Bits 19:19
    UINT32 ca10slowbw                              :  1;  // Bits 20:20
    UINT32 ca11slowbw                              :  1;  // Bits 21:21
    UINT32 sagvvtctl                               :  1;  // Bits 22:22
    UINT32 vtcompovrden                            :  1;  // Bits 23:23
    UINT32 vtcompovrd                              :  8;  // Bits 31:24
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} DDRPHY_COMP_NEW_CR_DIMMVREF_VREFADJUST2_STRUCT;
typedef union {
  struct {
    UINT32 vsshicompcr_dqvrefcode                  :  8;  // Bits 7:0
    UINT32 vsshicompcr_cmdvrefcode                 :  8;  // Bits 15:8
    UINT32 vsshicompcr_rxvrefcode                  :  8;  // Bits 23:16
    UINT32 vsshicompcr_leakvrefcode                :  8;  // Bits 31:24
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} DDRPHY_COMP_NEW_CR_VSSHICOMP_CTRL2_STRUCT;
typedef union {
  struct {
    UINT32 vsshicompcr_clkvrefcode                 :  8;  // Bits 7:0
    UINT32 vsshicompcr_ctlvrefcode                 :  8;  // Bits 15:8
    UINT32 bwsel_lo_threshold                      :  6;  // Bits 21:16
    UINT32 Spare                                   :  10;  // Bits 31:22
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} DDRPHY_COMP_NEW_CR_VSSHICOMP_CTRL3_STRUCT;
typedef union {
  struct {
    UINT32 dcs_start_dl_val                        :  10;  // Bits 9:0
    UINT32 dcs_calibrate_en                        :  1;  // Bits 10:10
    UINT32 dcs_jitter_meas_en                      :  1;  // Bits 11:11
    UINT32 dcs_thresh                              :  2;  // Bits 13:12
    UINT32 dcs_samples                             :  2;  // Bits 15:14
    UINT32 dcs_cb_tune                             :  2;  // Bits 17:16
    UINT32 dcs_timer_cfg                           :  2;  // Bits 19:18
    UINT32 dcs_halt_mode                           :  2;  // Bits 21:20
    UINT32 dcs_recycle_mode                        :  2;  // Bits 23:22
    UINT32 dcs_adaptive_filt_en                    :  1;  // Bits 24:24
    UINT32 dcs_clkdivsel                           :  1;  // Bits 25:25
    UINT32 dcs_num_clk                             :  2;  // Bits 27:26
    UINT32 Reserved3                               :  4;  // Bits 31:28
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} DCSCTL0_STRUCT;
typedef union {
  struct {
    UINT32 dcabypass                               :  6;  // Bits 5:0
    UINT32 dcs_cnt_load                            :  6;  // Bits 11:6
    UINT32 dcs_dcastaticcode                       :  8;  // Bits 19:12
    UINT32 dcs_clk_sel_ovrsel                      :  1;  // Bits 20:20
    UINT32 dcs_clk_sel_ovr                         :  2;  // Bits 22:21
    UINT32 dcs_mrcinitloop_en                      :  1;  // Bits 23:23
    UINT32 dcs_exthalt                             :  1;  // Bits 24:24
    UINT32 dcs_dl_val_ovrsel                       :  1;  // Bits 25:25
    UINT32 dca_fallxriseb_ovrsel                   :  1;  // Bits 26:26
    UINT32 dca_fallxriseb_ovr                      :  1;  // Bits 27:27
    UINT32 Reserved4                               :  3;  // Bits 30:28
    UINT32 dca_hvm_ovrsel                          :  1;  // Bits 31:31
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} DCSCTL1_STRUCT;
typedef union {
  struct {
    UINT32 dcacode_ovrsel                          :  1;  // Bits 0:0
    UINT32 dcacode_ovr                             :  8;  // Bits 8:1
    UINT32 Reserved5                               :  23;  // Bits 31:9
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} DCSCTL2_STRUCT;
typedef union {
  struct {
    UINT32 dcddlycben                              :  1;  // Bits 0:0
    UINT32 parkvalue                               :  1;  // Bits 1:1
    UINT32 txdccrangesel                           :  2;  // Bits 3:2
    UINT32 serializerbypen                         :  1;  // Bits 4:4
    UINT32 enperiodicdcc                           :  1;  // Bits 5:5
    UINT32 enperiodicdccondvfs                     :  1;  // Bits 6:6
    UINT32 dcdjitmode_sel                          :  1;  // Bits 7:7
    UINT32 dcdjitmode                              :  1;  // Bits 8:8
    UINT32 dcddlycode_sel                          :  1;  // Bits 9:9
    UINT32 dcddlycode                              :  10;  // Bits 19:10
    UINT32 dcdclken_sel                            :  1;  // Bits 20:20
    UINT32 dcdclken                                :  1;  // Bits 21:21
    UINT32 dcdcalen_sel                            :  1;  // Bits 22:22
    UINT32 dcdcalen                                :  1;  // Bits 23:23
    UINT32 bonus                                   :  2;  // Bits 25:24
    UINT32 dcdclocksel                             :  1;  // Bits 26:26
    UINT32 dcsfsm_rst_b                            :  1;  // Bits 27:27
    UINT32 reserved6                               :  4;  // Bits 31:28
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} DCSCTL3_STRUCT;
typedef union {
  struct {
    UINT32 dcs_init                                :  1;  // Bits 0:0
    UINT32 dcs_init_ovrd                           :  1;  // Bits 1:1
    UINT32 Reserved7                               :  30;  // Bits 31:2
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} DCSINIT_STRUCT;
typedef union {
  struct {
    UINT32 dcs_dyndcsclken                         :  1;  // Bits 0:0
    UINT32 Reserved8                               :  31;  // Bits 31:1
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} DCSDIGCLKGATE_STRUCT;
typedef union {
  struct {
    UINT32 dcs_hi                                  :  1;  // Bits 0:0
    UINT32 dcs_lo                                  :  1;  // Bits 1:1
    UINT32 dcs_dl_code                             :  10;  // Bits 11:2
    UINT32 dcs_frozen                              :  1;  // Bits 12:12
    UINT32 dcs_started                             :  1;  // Bits 13:13
    UINT32 dcs_view0                               :  1;  // Bits 14:14
    UINT32 dcs_view1                               :  1;  // Bits 15:15
    UINT32 Reserved9                               :  16;  // Bits 31:16
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} DCSSTATUS1_STRUCT;
typedef union {
  struct {
    UINT32 plllock                                 :  1;  // Bits 0:0
    UINT32 Reserved10                              :  31;  // Bits 31:1
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} PLLSTATUS_STRUCT;
typedef union {
  struct {
    UINT32 dcs_viewsel1                            :  3;  // Bits 2:0
    UINT32 dcs_viewsel0                            :  3;  // Bits 5:3
    UINT32 Reserved12                              :  2;  // Bits 7:6
    UINT32 dcdcomp_bonus                           :  2;  // Bits 9:8
    UINT32 Reserved11                              :  22;  // Bits 31:10
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} DCCDIGOBS_STRUCT;
typedef union {
  struct {
    UINT32 dcs_run_mode                            :  2;  // Bits 1:0
    UINT32 Reserved13                              :  30;  // Bits 31:2
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} DCSRUNMODE_STRUCT;
typedef union {
  struct {
    UINT32 periodic_init_dcccode                   :  8;  // Bits 7:0
    UINT32 Reserved14                              :  24;  // Bits 31:8
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} TXDLLSIGGRPDCCCTL34_STRUCT;
typedef union {
  struct {
    UINT32 periodic_percode_status                 :  8;  // Bits 7:0
    UINT32 periodic_initcode_status                :  8;  // Bits 15:8
    UINT32 Reserved15                              :  16;  // Bits 31:16
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} TXDLLSIGGRPDCCCTL35_STRUCT;
typedef union {
  struct {
    UINT32 dccoffset                               :  8;  // Bits 7:0
    UINT32 dccoffsetsign                           :  1;  // Bits 8:8
    UINT32 reserved16                              :  23;  // Bits 31:9
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} DCSOFFSET_STRUCT;
typedef union {
  struct {
    UINT32 dcsdfx_offset                           :  10;  // Bits 9:0
    UINT32 dcsdfx_limit                            :  10;  // Bits 19:10
    UINT32 reserved17                              :  12;  // Bits 31:20
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} DFXDCS_CTL_STRUCT;
typedef union {
  struct {
    UINT32 dcsdfx_start                            :  1;  // Bits 0:0
    UINT32 reserved18                              :  31;  // Bits 31:1
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} DFXDCS_RUN_STRUCT;
typedef union {
  struct {
    UINT32 dcsdfx_done                             :  1;  // Bits 0:0
    UINT32 dcsdfx_pass                             :  1;  // Bits 1:1
    UINT32 reserved19                              :  30;  // Bits 31:2
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} DFXDCS_STATUS_STRUCT;
typedef union {
  struct {
    UINT32 cal_code_src_sel                        :  1;  // Bits 0:0
    UINT32 fll_ratio_src_sel                       :  1;  // Bits 1:1
    UINT32 forceclkreq                             :  1;  // Bits 2:2
    UINT32 fll_enable_src_sel                      :  1;  // Bits 3:3
    UINT32 fllwireratio                            :  8;  // Bits 11:4
    UINT32 fllwirecalcode                          :  16;  // Bits 27:12
    UINT32 fllcalcodevalid                         :  1;  // Bits 28:28
    UINT32 spare                                   :  3;  // Bits 31:29
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} DDRPHY_COMP_NEW_CR_DDRCRFLLWIRED_STRUCT;
typedef union {
  struct {
    UINT32 dimmvref_visa_sel                       :  2;  // Bits 1:0
    UINT32 swcapfsm_visa_sel                       :  3;  // Bits 4:2
    UINT32 binfsm_visa_sel                         :  3;  // Bits 7:5
    UINT32 swcapoutput_visa_sel                    :  4;  // Bits 11:8
    UINT32 init_vs_ngdcc_visa_sel                  :  1;  // Bits 12:12
    UINT32 reserved20                              :  19;  // Bits 31:13
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} DDRPHY_COMP_NEW_CR_DDRCOMPVISAPREMUX_STRUCT;
typedef union {
  struct {
    UINT32 Dll_rxbwsel                             :  6;  // Bits 5:0
    UINT32 Spare                                   :  26;  // Bits 31:6
  } BitsA0;
  struct {
    UINT32 Binfsm4_offsetsearch                    :  2;  // Bits 1:0
    UINT32 Binfsm4_codedelay                       :  9;  // Bits 10:2
    UINT32 rcomp_cmn_cmdupextstatlegen             :  1;  // Bits 11:11
    UINT32 rcomp_cmn_dqupextstatlegen              :  1;  // Bits 12:12
    UINT32 rcomp_cmn_clkupextstatlegen             :  1;  // Bits 13:13
    UINT32 rcomp_cmn_ctlupextstatlegen             :  1;  // Bits 14:14
    UINT32 rcomp_cmn_odtupextstatlegen             :  1;  // Bits 15:15
    UINT32 rcomp_cmn_ckecsupextstatlegen           :  1;  // Bits 16:16
    UINT32 rcomp_cmn_cmddnrelpdnstatlegen          :  1;  // Bits 17:17
    UINT32 rcomp_cmn_dqdnrelpdnstatlegen           :  1;  // Bits 18:18
    UINT32 rcomp_cmn_clkdnrelpdnstatlegen          :  1;  // Bits 19:19
    UINT32 rcomp_cmn_ctldnrelpdnstatlegen          :  1;  // Bits 20:20
    UINT32 rcomp_cmn_odtdnrelpdnstatlegen          :  1;  // Bits 21:21
    UINT32 rcomp_cmn_cmddnrelpupstatlegen          :  1;  // Bits 22:22
    UINT32 rcomp_cmn_dqdnrelpupstatlegen           :  1;  // Bits 23:23
    UINT32 rcomp_cmn_clkdnrelpupstatlegen          :  1;  // Bits 24:24
    UINT32 rcomp_cmn_ctldnrelpupstatlegen          :  1;  // Bits 25:25
    UINT32 rcomp_cmn_odtdnrelpupstatlegen          :  1;  // Bits 26:26
    UINT32 Spare                                   :  5;  // Bits 31:27
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} DDRPHY_COMP_NEW_CR_VCCDLLCOMPDATACCC1_STRUCT;
typedef union {
  struct {
    UINT32 scramen                                 :  1;  // Bits 0:0
    UINT32 scramkey                                :  16;  // Bits 16:1
    UINT32 clockgateab                             :  2;  // Bits 18:17
    UINT32 clockgatec                              :  2;  // Bits 20:19
    UINT32 enabledbiab                             :  1;  // Bits 21:21
    UINT32 ca_mirrored                             :  4;  // Bits 25:22
    UINT32 dis_cmdanalogen                         :  1;  // Bits 26:26
    UINT32 dis_cmdanalogen_at_idle                 :  1;  // Bits 27:27
    UINT32 spare                                   :  1;  // Bits 28:28
    UINT32 forcecompdistdisable                    :  1;  // Bits 29:29
    UINT32 earlyrankrdvalidswitch                  :  1;  // Bits 30:30
    UINT32 forcecompdist                           :  1;  // Bits 31:31
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} DDRSCRAM_CR_DDRSCRAMBLECH0_STRUCT;
typedef union {
  struct {
    UINT32 scramen                                 :  1;  // Bits 0:0
    UINT32 scramkey                                :  16;  // Bits 16:1
    UINT32 clockgateab                             :  2;  // Bits 18:17
    UINT32 clockgatec                              :  2;  // Bits 20:19
    UINT32 enabledbiab                             :  1;  // Bits 21:21
    UINT32 ca_mirrored                             :  4;  // Bits 25:22
    UINT32 spare                                   :  6;  // Bits 31:26
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} DDRSCRAM_CR_DDRSCRAMBLECH1_STRUCT;

typedef DDRSCRAM_CR_DDRSCRAMBLECH1_STRUCT DDRSCRAM_CR_DDRSCRAMBLECH2_STRUCT;

typedef DDRSCRAM_CR_DDRSCRAMBLECH1_STRUCT DDRSCRAM_CR_DDRSCRAMBLECH3_STRUCT;

typedef DDRSCRAM_CR_DDRSCRAMBLECH1_STRUCT DDRSCRAM_CR_DDRSCRAMBLECH4_STRUCT;

typedef DDRSCRAM_CR_DDRSCRAMBLECH1_STRUCT DDRSCRAM_CR_DDRSCRAMBLECH5_STRUCT;

typedef DDRSCRAM_CR_DDRSCRAMBLECH1_STRUCT DDRSCRAM_CR_DDRSCRAMBLECH6_STRUCT;

typedef DDRSCRAM_CR_DDRSCRAMBLECH1_STRUCT DDRSCRAM_CR_DDRSCRAMBLECH7_STRUCT;
typedef union {
  struct {
    UINT32 spare0                                  :  1;  // Bits 0:0
    UINT32 OvrdPeriodicToDvfsComp                  :  1;  // Bits 1:1
    UINT32 eccpresent                              :  1;  // Bits 2:2
    UINT32 write0_enable                           :  1;  // Bits 3:3
    UINT32 gear1cmdondclkfall                      :  1;  // Bits 4:4
    UINT32 forcecompupdate                         :  1;  // Bits 5:5
    UINT32 rank_4_mode                             :  1;  // Bits 6:6
    UINT32 wck2ck_4_1                              :  1;  // Bits 7:7
    UINT32 wrp_d5_wr_0                             :  1;  // Bits 8:8
    UINT32 ddr5_mode                               :  1;  // Bits 9:9
    UINT32 ddrnochinterleave                       :  1;  // Bits 10:10
    UINT32 lpddr_mode                              :  1;  // Bits 11:11
    UINT32 gear1                                   :  1;  // Bits 12:12
    UINT32 rcven_extension                         :  4;  // Bits 16:13
    UINT32 ddr4_mode                               :  1;  // Bits 17:17
    UINT32 clkgatedisable                          :  1;  // Bits 18:18
    UINT32 dataclkgatedisatidle                    :  1;  // Bits 19:19
    UINT32 Gear4                                   :  1;  // Bits 20:20
    UINT32 lpddr4mode                              :  1;  // Bits 21:21
    UINT32 channel_not_populated                   :  8;  // Bits 29:22
    UINT32 dis_iosf_sb_clk_gate                    :  1;  // Bits 30:30
    UINT32 forcedeltadqsupdate                     :  1;  // Bits 31:31
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} DDRSCRAM_CR_DDRMISCCONTROL0_STRUCT;
typedef union {
  struct {
    UINT32 csodtmapping_cs0                        :  4;  // Bits 3:0
    UINT32 csodtmapping_cs1                        :  4;  // Bits 7:4
    UINT32 csodtmapping_cs2                        :  4;  // Bits 11:8
    UINT32 csodtmapping_cs3                        :  4;  // Bits 15:12
    UINT32 csodtmapping_odt0                       :  4;  // Bits 19:16
    UINT32 csodtmapping_odt1                       :  4;  // Bits 23:20
    UINT32 io_train_rst                            :  1;  // Bits 24:24
    UINT32 io_train_rst_quiet_time                 :  1;  // Bits 25:25
    UINT32 io_train_rst_duration                   :  4;  // Bits 29:26
    UINT32 companddeltadqsupdateclkgatedisable     :  1;  // Bits 30:30
    UINT32 spare                                   :  1;  // Bits 31:31
  } Bits;
  struct {
    UINT32 pgatevddq_cmn_vsxhivddqgndsel           :  1;  // Bits 0:0
    UINT32 pgatevddq_cmn_vsxhivddqglben            :  1;  // Bits 1:1
    UINT32 pgatehv_cmn_vsxhivdd2gndsel             :  1;  // Bits 2:2
    UINT32 pgatehv_cmn_vsxhivdd2glben              :  1;  // Bits 3:3
    UINT32 pgate_cmn_vsxhisel                      :  1;  // Bits 4:4
    UINT32 csodtmapping_cs1                        :  3;  // Bits 7:5
  } BitsQ0;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} DDRSCRAM_CR_DDRMISCCONTROL1_STRUCT;
typedef union {
  struct {
    UINT32 csodtmapping_cs0                        :  4;  // Bits 3:0
    UINT32 csodtmapping_cs1                        :  4;  // Bits 7:4
    UINT32 csodtmapping_cs2                        :  4;  // Bits 11:8
    UINT32 csodtmapping_cs3                        :  4;  // Bits 15:12
    UINT32 csodtmapping_odt0                       :  4;  // Bits 19:16
    UINT32 csodtmapping_odt1                       :  4;  // Bits 23:20
    UINT32 lpdeltadqstrainmode                     :  1;  // Bits 24:24
    UINT32 rx_analogen_grace_cnt                   :  7;  // Bits 31:25
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} DDRSCRAM_CR_DDRMISCCONTROL2_STRUCT;
typedef union {
  struct {
    UINT32                                         :  14;  // Bits 13:0
    UINT32 spare                                   :  5;  // Bits 18:14
    UINT32 rptchdqtxclkon                          :  1;  // Bits 19:19
    UINT32 rptchdqrxclkon                          :  1;  // Bits 20:20
    UINT32 rptchrepclkon                           :  1;  // Bits 21:21
    UINT32 cmdanlgengracecnt                       :  3;  // Bits 24:22
    UINT32 txanlgengracecnt                        :  6;  // Bits 30:25
    UINT32                                         :  1;  // Bits 31:31
  } BitsA0;
  struct {
    UINT32 tcwl4txdqfifowren                       :  7;  // Bits 6:0
    UINT32 tcwl4txdqfiforden                       :  7;  // Bits 13:7
    UINT32 spare                                   :  4;  // Bits 17:14
    UINT32 rptchdqtxclkon                          :  1;  // Bits 18:18
    UINT32 rptchdqrxclkon                          :  1;  // Bits 19:19
    UINT32 rptchrepclkon                           :  1;  // Bits 20:20
    UINT32 cmdanlgengracecnt                       :  3;  // Bits 23:21
    UINT32 txanlgengracecnt                        :  7;  // Bits 30:24
    UINT32 txdqfifordenperrankdeldis               :  1;  // Bits 31:31
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} MCMISCS_WRITECFGCH0_STRUCT;

typedef MCMISCS_WRITECFGCH0_STRUCT MCMISCS_WRITECFGCH1_STRUCT;

typedef MCMISCS_WRITECFGCH0_STRUCT MCMISCS_WRITECFGCH2_STRUCT;

typedef MCMISCS_WRITECFGCH0_STRUCT MCMISCS_WRITECFGCH3_STRUCT;

typedef MCMISCS_WRITECFGCH0_STRUCT MCMISCS_WRITECFGCH4_STRUCT;

typedef MCMISCS_WRITECFGCH0_STRUCT MCMISCS_WRITECFGCH5_STRUCT;

typedef MCMISCS_WRITECFGCH0_STRUCT MCMISCS_WRITECFGCH6_STRUCT;

typedef MCMISCS_WRITECFGCH0_STRUCT MCMISCS_WRITECFGCH7_STRUCT;
typedef union {
  struct {
    UINT32 spare                                   :  12;  // Bits 11:0
    UINT32 tcl4rcven                               :  7;  // Bits 18:12
    UINT32 tcl4rxdqfiforden                        :  7;  // Bits 25:19
    UINT32 rxdqdatavaliddclkdel                    :  5;  // Bits 30:26
    UINT32 rxdqdatavalidqclkdel                    :  1;  // Bits 31:31
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} MCMISCS_READCFGCH0_STRUCT;

typedef MCMISCS_READCFGCH0_STRUCT MCMISCS_READCFGCH1_STRUCT;

typedef MCMISCS_READCFGCH0_STRUCT MCMISCS_READCFGCH2_STRUCT;

typedef MCMISCS_READCFGCH0_STRUCT MCMISCS_READCFGCH3_STRUCT;

typedef MCMISCS_READCFGCH0_STRUCT MCMISCS_READCFGCH4_STRUCT;

typedef MCMISCS_READCFGCH0_STRUCT MCMISCS_READCFGCH5_STRUCT;

typedef MCMISCS_READCFGCH0_STRUCT MCMISCS_READCFGCH6_STRUCT;

typedef MCMISCS_READCFGCH0_STRUCT MCMISCS_READCFGCH7_STRUCT;
typedef union {
  struct {
    UINT32 txdqfifordenrank0chadel                 :  3;  // Bits 2:0
    UINT32 txdqfifordenrank1chadel                 :  3;  // Bits 5:3
    UINT32 txdqfifordenrank2chadel                 :  3;  // Bits 8:6
    UINT32 txdqfifordenrank3chadel                 :  3;  // Bits 11:9
    UINT32 txdqfifordenrank0chbdel                 :  3;  // Bits 14:12
    UINT32 txdqfifordenrank1chbdel                 :  3;  // Bits 17:15
    UINT32 txdqfifordenrank2chbdel                 :  3;  // Bits 20:18
    UINT32 txdqfifordenrank3chbdel                 :  3;  // Bits 23:21
    UINT32 tx_analog_clk_gate_dis_cha              :  1;  // Bits 24:24
    UINT32 tx_analog_clk_gate_dis_chb              :  1;  // Bits 25:25
    UINT32 spare                                   :  6;  // Bits 31:26
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} MCMISCS_WRITECFGCH01_STRUCT;

typedef MCMISCS_WRITECFGCH01_STRUCT MCMISCS_WRITECFGCH23_STRUCT;

typedef MCMISCS_WRITECFGCH01_STRUCT MCMISCS_WRITECFGCH45_STRUCT;

typedef MCMISCS_WRITECFGCH01_STRUCT MCMISCS_WRITECFGCH67_STRUCT;
typedef union {
  struct {
    UINT32 rcvenrank0chadel                        :  3;  // Bits 2:0
    UINT32 rcvenrank1chadel                        :  3;  // Bits 5:3
    UINT32 rcvenrank2chadel                        :  3;  // Bits 8:6
    UINT32 rcvenrank3chadel                        :  3;  // Bits 11:9
    UINT32 rcvenrank0chbdel                        :  3;  // Bits 14:12
    UINT32 rcvenrank1chbdel                        :  3;  // Bits 17:15
    UINT32 rcvenrank2chbdel                        :  3;  // Bits 20:18
    UINT32 rcvenrank3chbdel                        :  3;  // Bits 23:21
    UINT32 rx_analog_clk_gate_dis_cha              :  1;  // Bits 24:24
    UINT32 rx_analog_clk_gate_dis_chb              :  1;  // Bits 25:25
    UINT32 spare                                   :  6;  // Bits 31:26
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} MCMISCS_READCFGCH01_STRUCT;

typedef MCMISCS_READCFGCH01_STRUCT MCMISCS_READCFGCH23_STRUCT;

typedef MCMISCS_READCFGCH01_STRUCT MCMISCS_READCFGCH45_STRUCT;

typedef MCMISCS_READCFGCH01_STRUCT MCMISCS_READCFGCH67_STRUCT;
typedef union {
  struct {
    UINT32                                         :  13;  // Bits 12:0
    UINT32 datainvertnibble                        :  2;  // Bits 14:13
    UINT32 run1stcomp                              :  1;  // Bits 15:15
    UINT32 ddrcrobsvisasel1_8to1                   :  3;  // Bits 18:16
    UINT32 ddrcrobsvisasel0_8to1                   :  3;  // Bits 21:19
    UINT32 ddrcrobsvisasel1_32to8                  :  2;  // Bits 23:22
    UINT32 ddrcrobsvisasel0_32to8                  :  2;  // Bits 25:24
    UINT32 ddrvisaorpll                            :  1;  // Bits 26:26
    UINT32 dclkrst_rptr_plus1                      :  1;  // Bits 27:27
    UINT32 spare                                   :  4;  // Bits 31:28
  } BitsA0;
  struct {
    UINT32 txburstlen                              :  5;  // Bits 4:0
    UINT32 rxburstlen                              :  5;  // Bits 9:5
    UINT32 cmdduration                             :  3;  // Bits 12:10
    UINT32 datainvertnibble                        :  3;  // Bits 15:13
    UINT32 run1stcomp                              :  1;  // Bits 16:16
    UINT32 ddrcrobsvisasel1_8to1                   :  3;  // Bits 19:17
    UINT32 ddrcrobsvisasel0_8to1                   :  3;  // Bits 22:20
    UINT32 ddrcrobsvisasel1_32to8                  :  2;  // Bits 24:23
    UINT32 ddrcrobsvisasel0_32to8                  :  2;  // Bits 26:25
    UINT32 ddrvisaorpll                            :  1;  // Bits 27:27
    UINT32 dclkrst_rptr_plus1                      :  1;  // Bits 28:28
    UINT32 spare                                   :  3;  // Bits 31:29
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} DDRSCRAM_CR_DDRMISCCONTROL7_STRUCT;
typedef union {
  struct {
    UINT32 forceautotrdy                           :  1;  // Bits 0:0
    UINT32 spare                                   :  31;  // Bits 31:1
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} MCMISCS_SYNAUTOTRDYSTART_STRUCT;
typedef union {
  struct {
    UINT32 disableautotrdy                         :  1;  // Bits 0:0
    UINT32 spare                                   :  31;  // Bits 31:1
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} MCMISCS_SYNAUTOTRDYEND_STRUCT;
typedef union {
  struct {
    UINT32 rxdqfifordenrank0chadel                 :  4;  // Bits 3:0
    UINT32 rxdqfifordenrank1chadel                 :  4;  // Bits 7:4
    UINT32 rxdqfifordenrank2chadel                 :  4;  // Bits 11:8
    UINT32 rxdqfifordenrank3chadel                 :  4;  // Bits 15:12
    UINT32 rxdqfifordenrank0chbdel                 :  4;  // Bits 19:16
    UINT32 rxdqfifordenrank1chbdel                 :  4;  // Bits 23:20
    UINT32 rxdqfifordenrank2chbdel                 :  4;  // Bits 27:24
    UINT32 rxdqfifordenrank3chbdel                 :  4;  // Bits 31:28
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} MCMISCS_RXDQFIFORDENCH01_STRUCT;

typedef MCMISCS_RXDQFIFORDENCH01_STRUCT MCMISCS_RXDQFIFORDENCH23_STRUCT;

typedef MCMISCS_RXDQFIFORDENCH01_STRUCT MCMISCS_RXDQFIFORDENCH45_STRUCT;

typedef MCMISCS_RXDQFIFORDENCH01_STRUCT MCMISCS_RXDQFIFORDENCH67_STRUCT;
typedef union {
  struct {
    UINT32 enablespinegate                         :  1;  // Bits 0:0
    UINT32 sleepcycles                             :  3;  // Bits 3:1
    UINT32 awakecycles                             :  2;  // Bits 5:4
    UINT32 enhiphase                               :  2;  // Bits 7:6
    UINT32 gracelimitentry                         :  3;  // Bits 10:8
    UINT32 dopgatingdis                            :  1;  // Bits 11:11  // [ADL L0] Manual Edit
    UINT32 lpmode2gatingdis                        :  1;  // Bits 12:12  // [ADL L0] Manual Edit
    UINT32 spare                                   :  19; // Bits 31:13
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} MCMISCS_SPINEGATING_STRUCT;
typedef union {
  struct {
    UINT32 cas2rdwck                               :  7;  // Bits 6:0
    UINT32 cas2wrwck                               :  7;  // Bits 13:7
    UINT32 tWckHalfRate                            :  3;  // Bits 16:14
    UINT32 tWckPre                                 :  5;  // Bits 21:17
    UINT32 TrainWCkPulse                           :  1;  // Bits 22:22
    UINT32 Lp5Mode                                 :  1;  // Bits 23:23
    UINT32 WCkDiffLowInIdle                        :  1;  // Bits 24:24
    UINT32 TrainWCkBL                              :  4;  // Bits 28:25
    UINT32 TrainWCkMask                            :  3;  // Bits 31:29
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} MCMISCS_DDRWCKCONTROL_STRUCT;
typedef union {
  struct {
    UINT32 cas2fswck                               :  7;  // Bits 6:0
    UINT32 TrainWCkSyncRatio                       :  2;  // Bits 8:7
    UINT32 TrainWCkEn                              :  1;  // Bits 9:9
    UINT32 ddr_2n_mode_en                          :  1;  // Bits 10:10
    UINT32 ddr_2n_mode_type_g4                     :  1;  // Bits 11:11
    UINT32 scramCheckEcc_scramClkGateOvrd          :  2;  // Bits 13:12  //Adl_qo
    UINT32 lp_bank_mode                            :  2;  // Bits 15:14
    UINT32 wrcmd2dummy                             :  4;  // Bits 19:16
    UINT32 wrdummy2wrdummy                         :  4;  // Bits 23:20
    UINT32 rdcmd2dummy                             :  4;  // Bits 27:24
    UINT32 rddummy2rddummy                         :  4;  // Bits 31:28
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} MCMISCS_DDRWCKCONTROL1_STRUCT;
typedef union {
  struct {
    UINT32 EnInitComplete                          :  1;   // Bits 0:0
    UINT32 StaticSouthSpineGateEn                  :  1;   // Bits 1:1
    UINT32 spare                                   :  30;  // Bits 31:2
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} DDRSCRAM_CR_DDRLASTCR_STRUCT;

typedef DATA0CH0_CR_DFXCCMON_STRUCT DDRMCMISCS_CR_DFXCCMON_STRUCT;
typedef union {
  struct {
    UINT32 dfx_clk_gate_disable                    :  1;  // Bits 0:0
    UINT32 reserved0                               :  31;  // Bits 31:1
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} DDRMCMISCS_CR_DFX_CLKCTRL_STRUCT;

typedef DDRVTT0_CR_DDREARLY_RSTRDONE_STRUCT DDRSCRAM_CR_DDREARLYCR_STRUCT;
typedef union {
  struct {
    UINT32 spare                                   :  6;  // Bits 5:0
    UINT32 skipcompupdate                          :  10;  // Bits 15:6
    UINT32 compupdatecntval                        :  8;  // Bits 23:16
    UINT32 compupdateovrdcntval                    :  8;  // Bits 31:24
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} MCMISCS_COMPUPDATE_CTRL_STRUCT;

#pragma pack(pop)
#endif
