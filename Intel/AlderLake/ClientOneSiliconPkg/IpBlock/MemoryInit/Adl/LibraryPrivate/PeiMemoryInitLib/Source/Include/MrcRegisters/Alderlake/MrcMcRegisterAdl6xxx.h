#ifndef __MrcMcRegisterAdl6xxx_h__
#define __MrcMcRegisterAdl6xxx_h__
/** @file
  This file was automatically generated. Modify at your own risk.
  Note that no error checking is done in these functions so ensure that the correct values are passed.

@copyright
  Copyright (c) 2010 - 2020 Intel Corporation. All rights reserved
  This software and associated documentation (if any) is furnished
  under a license and may only be used or copied in accordance
  with the terms of the license. Except as permitted by the
  license, no part of this software or documentation may be
  reproduced, stored in a retrieval system, or transmitted in any
  form or by any means without the express written consent of
  Intel Corporation.
  This file contains an 'Intel Peripheral Driver' and is uniquely
  identified as "Intel Reference Module" and is licensed for Intel
  CPUs and chipsets under the terms of your license agreement with
  Intel or your vendor. This file may be modified by the user, subject
  to additional terms of the license agreement.

@par Specification Reference:
**/

#pragma pack(push, 1)


#define CCF_IDP0_CR_CHANNEL_HASH_REG                                   (0x00006040)

  #define CCF_IDP0_CR_CHANNEL_HASH_HASH_MASK_OFF                       ( 6)
  #define CCF_IDP0_CR_CHANNEL_HASH_HASH_MASK_WID                       (14)
  #define CCF_IDP0_CR_CHANNEL_HASH_HASH_MASK_MSK                       (0x000FFFC0)
  #define CCF_IDP0_CR_CHANNEL_HASH_HASH_MASK_MIN                       (0)
  #define CCF_IDP0_CR_CHANNEL_HASH_HASH_MASK_MAX                       (16383) // 0x00003FFF
  #define CCF_IDP0_CR_CHANNEL_HASH_HASH_MASK_DEF                       (0x00000000)
  #define CCF_IDP0_CR_CHANNEL_HASH_HASH_MASK_HSH                       (0x0E0C6040)

  #define CCF_IDP0_CR_CHANNEL_HASH_HASH_LSB_MASK_BIT_OFF               (24)
  #define CCF_IDP0_CR_CHANNEL_HASH_HASH_LSB_MASK_BIT_WID               ( 3)
  #define CCF_IDP0_CR_CHANNEL_HASH_HASH_LSB_MASK_BIT_MSK               (0x07000000)
  #define CCF_IDP0_CR_CHANNEL_HASH_HASH_LSB_MASK_BIT_MIN               (0)
  #define CCF_IDP0_CR_CHANNEL_HASH_HASH_LSB_MASK_BIT_MAX               (7) // 0x00000007
  #define CCF_IDP0_CR_CHANNEL_HASH_HASH_LSB_MASK_BIT_DEF               (0x00000000)
  #define CCF_IDP0_CR_CHANNEL_HASH_HASH_LSB_MASK_BIT_HSH               (0x03306040)

  #define CCF_IDP0_CR_CHANNEL_HASH_HASH_MODE_OFF                       (28)
  #define CCF_IDP0_CR_CHANNEL_HASH_HASH_MODE_WID                       ( 1)
  #define CCF_IDP0_CR_CHANNEL_HASH_HASH_MODE_MSK                       (0x10000000)
  #define CCF_IDP0_CR_CHANNEL_HASH_HASH_MODE_MIN                       (0)
  #define CCF_IDP0_CR_CHANNEL_HASH_HASH_MODE_MAX                       (1) // 0x00000001
  #define CCF_IDP0_CR_CHANNEL_HASH_HASH_MODE_DEF                       (0x00000000)
  #define CCF_IDP0_CR_CHANNEL_HASH_HASH_MODE_HSH                       (0x01386040)

#define CCF_IDP0_CR_CHANNEL_EHASH_REG                                  (0x00006044)

  #define CCF_IDP0_CR_CHANNEL_EHASH_EHASH_MASK_OFF                     ( 6)
  #define CCF_IDP0_CR_CHANNEL_EHASH_EHASH_MASK_WID                     (14)
  #define CCF_IDP0_CR_CHANNEL_EHASH_EHASH_MASK_MSK                     (0x000FFFC0)
  #define CCF_IDP0_CR_CHANNEL_EHASH_EHASH_MASK_MIN                     (0)
  #define CCF_IDP0_CR_CHANNEL_EHASH_EHASH_MASK_MAX                     (16383) // 0x00003FFF
  #define CCF_IDP0_CR_CHANNEL_EHASH_EHASH_MASK_DEF                     (0x00000000)
  #define CCF_IDP0_CR_CHANNEL_EHASH_EHASH_MASK_HSH                     (0x0E0C6044)

  #define CCF_IDP0_CR_CHANNEL_EHASH_EHASH_LSB_MASK_BIT_OFF             (24)
  #define CCF_IDP0_CR_CHANNEL_EHASH_EHASH_LSB_MASK_BIT_WID             ( 3)
  #define CCF_IDP0_CR_CHANNEL_EHASH_EHASH_LSB_MASK_BIT_MSK             (0x07000000)
  #define CCF_IDP0_CR_CHANNEL_EHASH_EHASH_LSB_MASK_BIT_MIN             (0)
  #define CCF_IDP0_CR_CHANNEL_EHASH_EHASH_LSB_MASK_BIT_MAX             (7) // 0x00000007
  #define CCF_IDP0_CR_CHANNEL_EHASH_EHASH_LSB_MASK_BIT_DEF             (0x00000000)
  #define CCF_IDP0_CR_CHANNEL_EHASH_EHASH_LSB_MASK_BIT_HSH             (0x03306044)

  #define CCF_IDP0_CR_CHANNEL_EHASH_EHASH_MODE_OFF                     (28)
  #define CCF_IDP0_CR_CHANNEL_EHASH_EHASH_MODE_WID                     ( 1)
  #define CCF_IDP0_CR_CHANNEL_EHASH_EHASH_MODE_MSK                     (0x10000000)
  #define CCF_IDP0_CR_CHANNEL_EHASH_EHASH_MODE_MIN                     (0)
  #define CCF_IDP0_CR_CHANNEL_EHASH_EHASH_MODE_MAX                     (1) // 0x00000001
  #define CCF_IDP0_CR_CHANNEL_EHASH_EHASH_MODE_DEF                     (0x00000000)
  #define CCF_IDP0_CR_CHANNEL_EHASH_EHASH_MODE_HSH                     (0x01386044)

  #define CCF_IDP0_CR_CHANNEL_EHASH_MLMC_IS_BA0_OFF                    (29)
  #define CCF_IDP0_CR_CHANNEL_EHASH_MLMC_IS_BA0_WID                    ( 1)
  #define CCF_IDP0_CR_CHANNEL_EHASH_MLMC_IS_BA0_MSK                    (0x20000000)
  #define CCF_IDP0_CR_CHANNEL_EHASH_MLMC_IS_BA0_MIN                    (0)
  #define CCF_IDP0_CR_CHANNEL_EHASH_MLMC_IS_BA0_MAX                    (1) // 0x00000001
  #define CCF_IDP0_CR_CHANNEL_EHASH_MLMC_IS_BA0_DEF                    (0x00000000)
  #define CCF_IDP0_CR_CHANNEL_EHASH_MLMC_IS_BA0_HSH                    (0x013A6044)

#define CCF_IDP0_CR_MAD_INTER_CHANNEL_REG                              (0x00006048)

  #define CCF_IDP0_CR_MAD_INTER_CHANNEL_DDR_TYPE_OFF                   ( 0)
  #define CCF_IDP0_CR_MAD_INTER_CHANNEL_DDR_TYPE_WID                   ( 3)
  #define CCF_IDP0_CR_MAD_INTER_CHANNEL_DDR_TYPE_MSK                   (0x00000007)
  #define CCF_IDP0_CR_MAD_INTER_CHANNEL_DDR_TYPE_MIN                   (0)
  #define CCF_IDP0_CR_MAD_INTER_CHANNEL_DDR_TYPE_MAX                   (7) // 0x00000007
  #define CCF_IDP0_CR_MAD_INTER_CHANNEL_DDR_TYPE_DEF                   (0x00000000)
  #define CCF_IDP0_CR_MAD_INTER_CHANNEL_DDR_TYPE_HSH                   (0x03006048)

  #define CCF_IDP0_CR_MAD_INTER_CHANNEL_ECHM_OFF                       ( 3)
  #define CCF_IDP0_CR_MAD_INTER_CHANNEL_ECHM_WID                       ( 1)
  #define CCF_IDP0_CR_MAD_INTER_CHANNEL_ECHM_MSK                       (0x00000008)
  #define CCF_IDP0_CR_MAD_INTER_CHANNEL_ECHM_MIN                       (0)
  #define CCF_IDP0_CR_MAD_INTER_CHANNEL_ECHM_MAX                       (1) // 0x00000001
  #define CCF_IDP0_CR_MAD_INTER_CHANNEL_ECHM_DEF                       (0x00000000)
  #define CCF_IDP0_CR_MAD_INTER_CHANNEL_ECHM_HSH                       (0x01066048)

  #define CCF_IDP0_CR_MAD_INTER_CHANNEL_CH_L_MAP_OFF                   ( 4)
  #define CCF_IDP0_CR_MAD_INTER_CHANNEL_CH_L_MAP_WID                   ( 1)
  #define CCF_IDP0_CR_MAD_INTER_CHANNEL_CH_L_MAP_MSK                   (0x00000010)
  #define CCF_IDP0_CR_MAD_INTER_CHANNEL_CH_L_MAP_MIN                   (0)
  #define CCF_IDP0_CR_MAD_INTER_CHANNEL_CH_L_MAP_MAX                   (1) // 0x00000001
  #define CCF_IDP0_CR_MAD_INTER_CHANNEL_CH_L_MAP_DEF                   (0x00000000)
  #define CCF_IDP0_CR_MAD_INTER_CHANNEL_CH_L_MAP_HSH                   (0x01086048)

  #define CCF_IDP0_CR_MAD_INTER_CHANNEL_STKD_MODE_OFF                  ( 8)
  #define CCF_IDP0_CR_MAD_INTER_CHANNEL_STKD_MODE_WID                  ( 1)
  #define CCF_IDP0_CR_MAD_INTER_CHANNEL_STKD_MODE_MSK                  (0x00000100)
  #define CCF_IDP0_CR_MAD_INTER_CHANNEL_STKD_MODE_MIN                  (0)
  #define CCF_IDP0_CR_MAD_INTER_CHANNEL_STKD_MODE_MAX                  (1) // 0x00000001
  #define CCF_IDP0_CR_MAD_INTER_CHANNEL_STKD_MODE_DEF                  (0x00000000)
  #define CCF_IDP0_CR_MAD_INTER_CHANNEL_STKD_MODE_HSH                  (0x01106048)

  #define CCF_IDP0_CR_MAD_INTER_CHANNEL_STKD_MODE_CH1_OFF              ( 9)
  #define CCF_IDP0_CR_MAD_INTER_CHANNEL_STKD_MODE_CH1_WID              ( 1)
  #define CCF_IDP0_CR_MAD_INTER_CHANNEL_STKD_MODE_CH1_MSK              (0x00000200)
  #define CCF_IDP0_CR_MAD_INTER_CHANNEL_STKD_MODE_CH1_MIN              (0)
  #define CCF_IDP0_CR_MAD_INTER_CHANNEL_STKD_MODE_CH1_MAX              (1) // 0x00000001
  #define CCF_IDP0_CR_MAD_INTER_CHANNEL_STKD_MODE_CH1_DEF              (0x00000000)
  #define CCF_IDP0_CR_MAD_INTER_CHANNEL_STKD_MODE_CH1_HSH              (0x01126048)

  #define CCF_IDP0_CR_MAD_INTER_CHANNEL_CH_S_SIZE_OFF                  (12)
  #define CCF_IDP0_CR_MAD_INTER_CHANNEL_CH_S_SIZE_WID                  ( 8)
  #define CCF_IDP0_CR_MAD_INTER_CHANNEL_CH_S_SIZE_MSK                  (0x000FF000)
  #define CCF_IDP0_CR_MAD_INTER_CHANNEL_CH_S_SIZE_MIN                  (0)
  #define CCF_IDP0_CR_MAD_INTER_CHANNEL_CH_S_SIZE_MAX                  (255) // 0x000000FF
  #define CCF_IDP0_CR_MAD_INTER_CHANNEL_CH_S_SIZE_DEF                  (0x00000000)
  #define CCF_IDP0_CR_MAD_INTER_CHANNEL_CH_S_SIZE_HSH                  (0x08186048)

  #define CCF_IDP0_CR_MAD_INTER_CHANNEL_STKD_MODE_CH_BITS_OFF          (24)
  #define CCF_IDP0_CR_MAD_INTER_CHANNEL_STKD_MODE_CH_BITS_WID          ( 3)
  #define CCF_IDP0_CR_MAD_INTER_CHANNEL_STKD_MODE_CH_BITS_MSK          (0x07000000)
  #define CCF_IDP0_CR_MAD_INTER_CHANNEL_STKD_MODE_CH_BITS_MIN          (0)
  #define CCF_IDP0_CR_MAD_INTER_CHANNEL_STKD_MODE_CH_BITS_MAX          (7) // 0x00000007
  #define CCF_IDP0_CR_MAD_INTER_CHANNEL_STKD_MODE_CH_BITS_DEF          (0x00000000)
  #define CCF_IDP0_CR_MAD_INTER_CHANNEL_STKD_MODE_CH_BITS_HSH          (0x03306048)

#define CCF_IDP0_CR_MAD_INTRA_CH0_REG                                  (0x0000604C)

  #define CCF_IDP0_CR_MAD_INTRA_CH0_DIMM_L_MAP_OFF                     ( 0)
  #define CCF_IDP0_CR_MAD_INTRA_CH0_DIMM_L_MAP_WID                     ( 1)
  #define CCF_IDP0_CR_MAD_INTRA_CH0_DIMM_L_MAP_MSK                     (0x00000001)
  #define CCF_IDP0_CR_MAD_INTRA_CH0_DIMM_L_MAP_MIN                     (0)
  #define CCF_IDP0_CR_MAD_INTRA_CH0_DIMM_L_MAP_MAX                     (1) // 0x00000001
  #define CCF_IDP0_CR_MAD_INTRA_CH0_DIMM_L_MAP_DEF                     (0x00000000)
  #define CCF_IDP0_CR_MAD_INTRA_CH0_DIMM_L_MAP_HSH                     (0x0100604C)

  #define CCF_IDP0_CR_MAD_INTRA_CH0_RI_OFF                             ( 4)
  #define CCF_IDP0_CR_MAD_INTRA_CH0_RI_WID                             ( 1)
  #define CCF_IDP0_CR_MAD_INTRA_CH0_RI_MSK                             (0x00000010)
  #define CCF_IDP0_CR_MAD_INTRA_CH0_RI_MIN                             (0)
  #define CCF_IDP0_CR_MAD_INTRA_CH0_RI_MAX                             (1) // 0x00000001
  #define CCF_IDP0_CR_MAD_INTRA_CH0_RI_DEF                             (0x00000000)
  #define CCF_IDP0_CR_MAD_INTRA_CH0_RI_HSH                             (0x0108604C)

  #define CCF_IDP0_CR_MAD_INTRA_CH0_EIM_OFF                            ( 8)
  #define CCF_IDP0_CR_MAD_INTRA_CH0_EIM_WID                            ( 1)
  #define CCF_IDP0_CR_MAD_INTRA_CH0_EIM_MSK                            (0x00000100)
  #define CCF_IDP0_CR_MAD_INTRA_CH0_EIM_MIN                            (0)
  #define CCF_IDP0_CR_MAD_INTRA_CH0_EIM_MAX                            (1) // 0x00000001
  #define CCF_IDP0_CR_MAD_INTRA_CH0_EIM_DEF                            (0x00000000)
  #define CCF_IDP0_CR_MAD_INTRA_CH0_EIM_HSH                            (0x0110604C)

  #define CCF_IDP0_CR_MAD_INTRA_CH0_ECC_OFF                            (12)
  #define CCF_IDP0_CR_MAD_INTRA_CH0_ECC_WID                            ( 2)
  #define CCF_IDP0_CR_MAD_INTRA_CH0_ECC_MSK                            (0x00003000)
  #define CCF_IDP0_CR_MAD_INTRA_CH0_ECC_MIN                            (0)
  #define CCF_IDP0_CR_MAD_INTRA_CH0_ECC_MAX                            (3) // 0x00000003
  #define CCF_IDP0_CR_MAD_INTRA_CH0_ECC_DEF                            (0x00000000)
  #define CCF_IDP0_CR_MAD_INTRA_CH0_ECC_HSH                            (0x0218604C)

#define CCF_IDP0_CR_MAD_INTRA_CH1_REG                                  (0x00006050)
//Duplicate of CCF_IDP0_CR_MAD_INTRA_CH0_REG

#define CCF_IDP0_CR_MAD_DIMM_CH0_REG                                   (0x00006054)

  #define CCF_IDP0_CR_MAD_DIMM_CH0_DIMM_L_SIZE_OFF                     ( 0)
  #define CCF_IDP0_CR_MAD_DIMM_CH0_DIMM_L_SIZE_WID                     ( 7)
  #define CCF_IDP0_CR_MAD_DIMM_CH0_DIMM_L_SIZE_MSK                     (0x0000007F)
  #define CCF_IDP0_CR_MAD_DIMM_CH0_DIMM_L_SIZE_MIN                     (0)
  #define CCF_IDP0_CR_MAD_DIMM_CH0_DIMM_L_SIZE_MAX                     (127) // 0x0000007F
  #define CCF_IDP0_CR_MAD_DIMM_CH0_DIMM_L_SIZE_DEF                     (0x00000000)
  #define CCF_IDP0_CR_MAD_DIMM_CH0_DIMM_L_SIZE_HSH                     (0x07006054)

  #define CCF_IDP0_CR_MAD_DIMM_CH0_DLW_OFF                             ( 7)
  #define CCF_IDP0_CR_MAD_DIMM_CH0_DLW_WID                             ( 2)
  #define CCF_IDP0_CR_MAD_DIMM_CH0_DLW_MSK                             (0x00000180)
  #define CCF_IDP0_CR_MAD_DIMM_CH0_DLW_MIN                             (0)
  #define CCF_IDP0_CR_MAD_DIMM_CH0_DLW_MAX                             (3) // 0x00000003
  #define CCF_IDP0_CR_MAD_DIMM_CH0_DLW_DEF                             (0x00000000)
  #define CCF_IDP0_CR_MAD_DIMM_CH0_DLW_HSH                             (0x020E6054)

  #define CCF_IDP0_CR_MAD_DIMM_CH0_DLNOR_OFF                           ( 9)
  #define CCF_IDP0_CR_MAD_DIMM_CH0_DLNOR_WID                           ( 2)
  #define CCF_IDP0_CR_MAD_DIMM_CH0_DLNOR_MSK                           (0x00000600)
  #define CCF_IDP0_CR_MAD_DIMM_CH0_DLNOR_MIN                           (0)
  #define CCF_IDP0_CR_MAD_DIMM_CH0_DLNOR_MAX                           (3) // 0x00000003
  #define CCF_IDP0_CR_MAD_DIMM_CH0_DLNOR_DEF                           (0x00000000)
  #define CCF_IDP0_CR_MAD_DIMM_CH0_DLNOR_HSH                           (0x02126054)

  #define CCF_IDP0_CR_MAD_DIMM_CH0_ddr5_device_8Gb_OFF                 (11)
  #define CCF_IDP0_CR_MAD_DIMM_CH0_ddr5_device_8Gb_WID                 ( 1)
  #define CCF_IDP0_CR_MAD_DIMM_CH0_ddr5_device_8Gb_MSK                 (0x00000800)
  #define CCF_IDP0_CR_MAD_DIMM_CH0_ddr5_device_8Gb_MIN                 (0)
  #define CCF_IDP0_CR_MAD_DIMM_CH0_ddr5_device_8Gb_MAX                 (1) // 0x00000001
  #define CCF_IDP0_CR_MAD_DIMM_CH0_ddr5_device_8Gb_DEF                 (0x00000001)
  #define CCF_IDP0_CR_MAD_DIMM_CH0_ddr5_device_8Gb_HSH                 (0x01166054)

  #define CCF_IDP0_CR_MAD_DIMM_CH0_DIMM_S_SIZE_OFF                     (16)
  #define CCF_IDP0_CR_MAD_DIMM_CH0_DIMM_S_SIZE_WID                     ( 7)
  #define CCF_IDP0_CR_MAD_DIMM_CH0_DIMM_S_SIZE_MSK                     (0x007F0000)
  #define CCF_IDP0_CR_MAD_DIMM_CH0_DIMM_S_SIZE_MIN                     (0)
  #define CCF_IDP0_CR_MAD_DIMM_CH0_DIMM_S_SIZE_MAX                     (127) // 0x0000007F
  #define CCF_IDP0_CR_MAD_DIMM_CH0_DIMM_S_SIZE_DEF                     (0x00000000)
  #define CCF_IDP0_CR_MAD_DIMM_CH0_DIMM_S_SIZE_HSH                     (0x07206054)

  #define CCF_IDP0_CR_MAD_DIMM_CH0_DSW_OFF                             (24)
  #define CCF_IDP0_CR_MAD_DIMM_CH0_DSW_WID                             ( 2)
  #define CCF_IDP0_CR_MAD_DIMM_CH0_DSW_MSK                             (0x03000000)
  #define CCF_IDP0_CR_MAD_DIMM_CH0_DSW_MIN                             (0)
  #define CCF_IDP0_CR_MAD_DIMM_CH0_DSW_MAX                             (3) // 0x00000003
  #define CCF_IDP0_CR_MAD_DIMM_CH0_DSW_DEF                             (0x00000000)
  #define CCF_IDP0_CR_MAD_DIMM_CH0_DSW_HSH                             (0x02306054)

  #define CCF_IDP0_CR_MAD_DIMM_CH0_DSNOR_OFF                           (26)
  #define CCF_IDP0_CR_MAD_DIMM_CH0_DSNOR_WID                           ( 2)
  #define CCF_IDP0_CR_MAD_DIMM_CH0_DSNOR_MSK                           (0x0C000000)
  #define CCF_IDP0_CR_MAD_DIMM_CH0_DSNOR_MIN                           (0)
  #define CCF_IDP0_CR_MAD_DIMM_CH0_DSNOR_MAX                           (3) // 0x00000003
  #define CCF_IDP0_CR_MAD_DIMM_CH0_DSNOR_DEF                           (0x00000000)
  #define CCF_IDP0_CR_MAD_DIMM_CH0_DSNOR_HSH                           (0x02346054)

  #define CCF_IDP0_CR_MAD_DIMM_CH0_BG0_bit_options_OFF                 (28)
  #define CCF_IDP0_CR_MAD_DIMM_CH0_BG0_bit_options_WID                 ( 2)
  #define CCF_IDP0_CR_MAD_DIMM_CH0_BG0_bit_options_MSK                 (0x30000000)
  #define CCF_IDP0_CR_MAD_DIMM_CH0_BG0_bit_options_MIN                 (0)
  #define CCF_IDP0_CR_MAD_DIMM_CH0_BG0_bit_options_MAX                 (3) // 0x00000003
  #define CCF_IDP0_CR_MAD_DIMM_CH0_BG0_bit_options_DEF                 (0x00000000)
  #define CCF_IDP0_CR_MAD_DIMM_CH0_BG0_bit_options_HSH                 (0x02386054)

  #define CCF_IDP0_CR_MAD_DIMM_CH0_Decoder_EBH_OFF                     (30)
  #define CCF_IDP0_CR_MAD_DIMM_CH0_Decoder_EBH_WID                     ( 2)
  #define CCF_IDP0_CR_MAD_DIMM_CH0_Decoder_EBH_MSK                     (0xC0000000)
  #define CCF_IDP0_CR_MAD_DIMM_CH0_Decoder_EBH_MIN                     (0)
  #define CCF_IDP0_CR_MAD_DIMM_CH0_Decoder_EBH_MAX                     (3) // 0x00000003
  #define CCF_IDP0_CR_MAD_DIMM_CH0_Decoder_EBH_DEF                     (0x00000000)
  #define CCF_IDP0_CR_MAD_DIMM_CH0_Decoder_EBH_HSH                     (0x023C6054)

#define CCF_IDP0_CR_MAD_DIMM_CH1_REG                                   (0x00006058)
//Duplicate of CCF_IDP0_CR_MAD_DIMM_CH0_REG

#define CCF_IDP0_CR_MARS_ENABLE_REG                                    (0x0000605C)

  #define CCF_IDP0_CR_MARS_ENABLE_MARS_Enable_OFF                      ( 0)
  #define CCF_IDP0_CR_MARS_ENABLE_MARS_Enable_WID                      ( 1)
  #define CCF_IDP0_CR_MARS_ENABLE_MARS_Enable_MSK                      (0x00000001)
  #define CCF_IDP0_CR_MARS_ENABLE_MARS_Enable_MIN                      (0)
  #define CCF_IDP0_CR_MARS_ENABLE_MARS_Enable_MAX                      (1) // 0x00000001
  #define CCF_IDP0_CR_MARS_ENABLE_MARS_Enable_DEF                      (0x00000000)
  #define CCF_IDP0_CR_MARS_ENABLE_MARS_Enable_HSH                      (0x0100605C)

#define CCF_IDP0_CR_CHANNEL_HASH_MC1_REG                               (0x000061B0)
//Duplicate of CCF_IDP0_CR_CHANNEL_HASH_REG

#define CCF_IDP0_CR_CHANNEL_EHASH_MC1_REG                              (0x000061B4)
//Duplicate of CCF_IDP0_CR_CHANNEL_EHASH_REG

#define CCF_IDP0_CR_MAD_INTER_CHANNEL_MC1_REG                          (0x000061B8)
//Duplicate of CCF_IDP0_CR_MAD_INTER_CHANNEL_REG

#define CCF_IDP0_CR_MAD_INTRA_CH0_MC1_REG                              (0x000061BC)
//Duplicate of CCF_IDP0_CR_MAD_INTRA_CH0_REG

#define CCF_IDP0_CR_MAD_INTRA_CH1_MC1_REG                              (0x000061C0)
//Duplicate of CCF_IDP0_CR_MAD_INTRA_CH0_REG

#define CCF_IDP0_CR_MAD_DIMM_CH0_MC1_REG                               (0x000061C4)
//Duplicate of CCF_IDP0_CR_MAD_DIMM_CH0_REG

#define CCF_IDP0_CR_MAD_DIMM_CH1_MC1_REG                               (0x000061C8)
//Duplicate of CCF_IDP0_CR_MAD_DIMM_CH0_REG

#define CCF_IDP0_CR_SLICE_HASH_REG                                     (0x000061CC)
//Duplicate of CCF_IDP0_CR_CHANNEL_HASH_REG

#define CCF_IDP0_CR_MAD_INTER_SLICE_REG                                (0x000061D0)

  #define CCF_IDP0_CR_MAD_INTER_SLICE_MS_L_MAP_OFF                     ( 4)
  #define CCF_IDP0_CR_MAD_INTER_SLICE_MS_L_MAP_WID                     ( 1)
  #define CCF_IDP0_CR_MAD_INTER_SLICE_MS_L_MAP_MSK                     (0x00000010)
  #define CCF_IDP0_CR_MAD_INTER_SLICE_MS_L_MAP_MIN                     (0)
  #define CCF_IDP0_CR_MAD_INTER_SLICE_MS_L_MAP_MAX                     (1) // 0x00000001
  #define CCF_IDP0_CR_MAD_INTER_SLICE_MS_L_MAP_DEF                     (0x00000000)
  #define CCF_IDP0_CR_MAD_INTER_SLICE_MS_L_MAP_HSH                     (0x010861D0)

  #define CCF_IDP0_CR_MAD_INTER_SLICE_STKD_MODE_OFF                    ( 8)
  #define CCF_IDP0_CR_MAD_INTER_SLICE_STKD_MODE_WID                    ( 1)
  #define CCF_IDP0_CR_MAD_INTER_SLICE_STKD_MODE_MSK                    (0x00000100)
  #define CCF_IDP0_CR_MAD_INTER_SLICE_STKD_MODE_MIN                    (0)
  #define CCF_IDP0_CR_MAD_INTER_SLICE_STKD_MODE_MAX                    (1) // 0x00000001
  #define CCF_IDP0_CR_MAD_INTER_SLICE_STKD_MODE_DEF                    (0x00000000)
  #define CCF_IDP0_CR_MAD_INTER_SLICE_STKD_MODE_HSH                    (0x011061D0)

  #define CCF_IDP0_CR_MAD_INTER_SLICE_STKD_MODE_MS1_OFF                ( 9)
  #define CCF_IDP0_CR_MAD_INTER_SLICE_STKD_MODE_MS1_WID                ( 1)
  #define CCF_IDP0_CR_MAD_INTER_SLICE_STKD_MODE_MS1_MSK                (0x00000200)
  #define CCF_IDP0_CR_MAD_INTER_SLICE_STKD_MODE_MS1_MIN                (0)
  #define CCF_IDP0_CR_MAD_INTER_SLICE_STKD_MODE_MS1_MAX                (1) // 0x00000001
  #define CCF_IDP0_CR_MAD_INTER_SLICE_STKD_MODE_MS1_DEF                (0x00000000)
  #define CCF_IDP0_CR_MAD_INTER_SLICE_STKD_MODE_MS1_HSH                (0x011261D0)

  #define CCF_IDP0_CR_MAD_INTER_SLICE_MS_S_SIZE_OFF                    (12)
  #define CCF_IDP0_CR_MAD_INTER_SLICE_MS_S_SIZE_WID                    (11)
  #define CCF_IDP0_CR_MAD_INTER_SLICE_MS_S_SIZE_MSK                    (0x007FF000)
  #define CCF_IDP0_CR_MAD_INTER_SLICE_MS_S_SIZE_MIN                    (0)
  #define CCF_IDP0_CR_MAD_INTER_SLICE_MS_S_SIZE_MAX                    (2047) // 0x000007FF
  #define CCF_IDP0_CR_MAD_INTER_SLICE_MS_S_SIZE_DEF                    (0x00000000)
  #define CCF_IDP0_CR_MAD_INTER_SLICE_MS_S_SIZE_HSH                    (0x0B1861D0)

  #define CCF_IDP0_CR_MAD_INTER_SLICE_STKD_MODE_MS_BITS_OFF            (24)
  #define CCF_IDP0_CR_MAD_INTER_SLICE_STKD_MODE_MS_BITS_WID            ( 4)
  #define CCF_IDP0_CR_MAD_INTER_SLICE_STKD_MODE_MS_BITS_MSK            (0x0F000000)
  #define CCF_IDP0_CR_MAD_INTER_SLICE_STKD_MODE_MS_BITS_MIN            (0)
  #define CCF_IDP0_CR_MAD_INTER_SLICE_STKD_MODE_MS_BITS_MAX            (15) // 0x0000000F
  #define CCF_IDP0_CR_MAD_INTER_SLICE_STKD_MODE_MS_BITS_DEF            (0x00000000)
  #define CCF_IDP0_CR_MAD_INTER_SLICE_STKD_MODE_MS_BITS_HSH            (0x043061D0)

#define CCF_IDP1_CR_CHANNEL_HASH_REG                                   (0x00006240)
//Duplicate of CCF_IDP0_CR_CHANNEL_HASH_REG

#define CCF_IDP1_CR_CHANNEL_EHASH_REG                                  (0x00006244)
//Duplicate of CCF_IDP0_CR_CHANNEL_EHASH_REG

#define CCF_IDP1_CR_MAD_INTER_CHANNEL_REG                              (0x00006248)
//Duplicate of CCF_IDP0_CR_MAD_INTER_CHANNEL_REG

#define CCF_IDP1_CR_MAD_INTRA_CH0_REG                                  (0x0000624C)
//Duplicate of CCF_IDP0_CR_MAD_INTRA_CH0_REG

#define CCF_IDP1_CR_MAD_INTRA_CH1_REG                                  (0x00006250)
//Duplicate of CCF_IDP0_CR_MAD_INTRA_CH0_REG

#define CCF_IDP1_CR_MAD_DIMM_CH0_REG                                   (0x00006254)
//Duplicate of CCF_IDP0_CR_MAD_DIMM_CH0_REG

#define CCF_IDP1_CR_MAD_DIMM_CH1_REG                                   (0x00006258)
//Duplicate of CCF_IDP0_CR_MAD_DIMM_CH0_REG

#define CCF_IDP1_CR_MARS_ENABLE_REG                                    (0x0000625C)
//Duplicate of CCF_IDP0_CR_MARS_ENABLE_REG

#define CCF_IDP1_CR_CHANNEL_HASH_MC1_REG                               (0x000063B0)
//Duplicate of CCF_IDP0_CR_CHANNEL_HASH_REG

#define CCF_IDP1_CR_CHANNEL_EHASH_MC1_REG                              (0x000063B4)
//Duplicate of CCF_IDP0_CR_CHANNEL_EHASH_REG

#define CCF_IDP1_CR_MAD_INTER_CHANNEL_MC1_REG                          (0x000063B8)
//Duplicate of CCF_IDP0_CR_MAD_INTER_CHANNEL_REG

#define CCF_IDP1_CR_MAD_INTRA_CH0_MC1_REG                              (0x000063BC)
//Duplicate of CCF_IDP0_CR_MAD_INTRA_CH0_REG

#define CCF_IDP1_CR_MAD_INTRA_CH1_MC1_REG                              (0x000063C0)
//Duplicate of CCF_IDP0_CR_MAD_INTRA_CH0_REG

#define CCF_IDP1_CR_MAD_DIMM_CH0_MC1_REG                               (0x000063C4)
//Duplicate of CCF_IDP0_CR_MAD_DIMM_CH0_REG

#define CCF_IDP1_CR_MAD_DIMM_CH1_MC1_REG                               (0x000063C8)
//Duplicate of CCF_IDP0_CR_MAD_DIMM_CH0_REG

#define CCF_IDP1_CR_SLICE_HASH_REG                                     (0x000063CC)
//Duplicate of CCF_IDP0_CR_CHANNEL_HASH_REG

#define CCF_IDP1_CR_MAD_INTER_SLICE_REG                                (0x000063D0)
//Duplicate of CCF_IDP0_CR_MAD_INTER_SLICE_REG
#pragma pack(pop)
#endif
