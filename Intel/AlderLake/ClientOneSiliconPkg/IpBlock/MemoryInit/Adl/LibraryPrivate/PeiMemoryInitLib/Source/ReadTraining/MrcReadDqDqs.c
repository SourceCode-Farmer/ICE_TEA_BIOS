/** @file
  Once DQS is aligned against the clock in the receive enable training flow,
  the second stage of the read training is the DQ/DQS training, aligning each
  strobe with it's byte of data. The DQ/DQS training is once again using the
  DDR read synchronization mode, in this mode a predetermined pattern is read
  out of the DDR. The following algorithm is used to align the data sampling
  to the best sampling point.

@copyright
  INTEL CONFIDENTIAL
  Copyright 1999 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains an 'Intel Peripheral Driver' and is uniquely identified as
  "Intel Reference Module" and is licensed for Intel CPUs and chipsets under
  the terms of your license agreement with Intel or your vendor. This file may
  be modified by the user, subject to additional terms of the license agreement.

@par Specification Reference:
**/
//
// Include files
//
#include "MrcReadDqDqs.h"
#include "MrcCrosser.h"
#include "MrcCpgcApi.h"
#include "Cpgc20TestCtl.h"
#include "MrcCommon.h"
#include "MrcDdr5Registers.h"
#include "MrcLpddr5Registers.h"
#include "MrcDdr5.h"
#include "MrcChipApi.h"
#include "MrcMaintenance.h"
#include "MrcMemoryApi.h"
#include "MrcCommandTraining.h"

// For MPR pattern "00000000"
MRC_BG_BANK_PAIR Ddr4RdMprBankL2pBank3[2] = { {0, 3}, {1, 3} };

/**
  Perform Read MPR Training using normal reads.
  Centers read DQ-DQS with MPR pattern.

  @param[in, out] MrcData  - Pointer to MRC global data.
  @param[in] Param  - Indication to optimize P/N or both.
  @param[in] PerBit - Indication to do Per Bit step.
  @param[in] Print  - Print debug messages.

  @retval MrcStatus - mrcSuccess or reason for failure.
**/
MrcStatus
MrcReadMprTrainingNormal (
  IN OUT MrcParameters *const MrcData,
  IN     UINT8                Param,
  IN     BOOLEAN              PerBit,
  IN     BOOLEAN              Print
  )
{
  const MrcInput      *Inputs;
  MrcDebug            *Debug;
  const MRC_FUNCTION  *MrcCall;
  MrcOutput           *Outputs;
  MrcSaveData         *SaveOutputs;
  MrcChannelOut       *ChannelOut;
  MrcStatus           Status;
  MrcStatus           MprStatus;
  UINT8               MaxChannel;
  UINT8               Controller;
  UINT8               Channel;
  UINT32              IpChannel;
  UINT8               Rank;
  UINT8               Byte;
  UINT8               Bit;
  UINT8               RankMask;  // RankBitMask for both channels
  UINT8               LoopCount;
  UINT8               McChBitMask;
  UINT8               EccByte;
  UINT64              ErrStatus;
  UINT64              EccStatus;
  UINT64              ChunkStatus;
  UINT16              *Margin;
  UINT16              ChunkResult[MAX_CONTROLLER][MAX_CHANNEL];
  INT8                LeftEdge;
  INT8                DqsDelay;
  INT8                DqsStart;
  INT8                DqsStop;
  INT16               CurrentPassingStart[MAX_CONTROLLER][MAX_CHANNEL][MAX_SDRAM_IN_DIMM];
  INT16               CurrentPassingEnd[MAX_CONTROLLER][MAX_CHANNEL][MAX_SDRAM_IN_DIMM];
  INT16               LargestPassingStart[MAX_CONTROLLER][MAX_CHANNEL][MAX_SDRAM_IN_DIMM];
  INT16               LargestPassingEnd[MAX_CONTROLLER][MAX_CHANNEL][MAX_SDRAM_IN_DIMM];
  INT16               FinalDqs[MAX_CONTROLLER][MAX_CHANNEL][MAX_RANK_IN_CHANNEL][MAX_SDRAM_IN_DIMM];
  INT16               cWidth;
  INT16               lWidth;
  INT16               Center;
  INT16               Left;
  INT16               Right;
  INT64               GetSetDis;
  INT64               GetSetEn;
  INT64               GetSetVal;
  BOOLEAN             Pass;
  BOOLEAN             Lpddr4;
  BOOLEAN             Lpddr5;
  BOOLEAN             Lpddr;
  BOOLEAN             Ddr4;
  BOOLEAN             Ddr5;
  UINT8               RdDataPtn;    //Read Data Pattern in 8 bit
  UINT8               MrDqCalPatA;
  UINT8               MrDqCalPatB;
  UINT8               MrDqCalLow;
  UINT8               MrDqCalHigh;
  UINT8               MrData;
  BOOLEAN             Gear4;
  UINT32              McRdbCreditsSave;
  UINT32              Offset;
  MC0_CH0_CR_MC_RDB_CREDITS_STRUCT  McRdbCredits;

  DDR4_MODE_REGISTER_3_STRUCT   Ddr4Mr3;
  INT8                MarginBit[MAX_RANK_IN_CHANNEL][MAX_CONTROLLER][MAX_CHANNEL][MAX_SDRAM_IN_DIMM][MAX_BITS][MAX_EDGES]; // For Rx PBD
  UINT8               L2RBitFlag[MAX_RANK_IN_CHANNEL][MAX_CONTROLLER][MAX_CHANNEL][MAX_SDRAM_IN_DIMM][MAX_BITS];
  UINT8               MinWidth;
  UINT8               BitGroupErr[MAX_CONTROLLER][MAX_CHANNEL][MAX_SDRAM_IN_DIMM];
  UINT32              UiMask;
  INT64               SaveRxDqsP[MAX_RANK_IN_CHANNEL][MAX_CONTROLLER][MAX_CHANNEL][MAX_SDRAM_IN_DIMM]; // For Rx PBD P delay
  INT64               SaveRxDqsN[MAX_RANK_IN_CHANNEL][MAX_CONTROLLER][MAX_CHANNEL][MAX_SDRAM_IN_DIMM]; // For Rx PBD N delay
  INT64               GetSetValN;
  INT64               GetSetValP;
  GSM_GT              Group;
  MC_MPR_CONFIG_SAVE  SaveData;

  Inputs      = &MrcData->Inputs;
  MrcCall     = Inputs->Call.Func;
  Outputs     = &MrcData->Outputs;
  SaveOutputs = &MrcData->Save.Data;
  Debug       = Print ? &Outputs->Debug : NULL;
  Status      = mrcSuccess;
  MprStatus   = mrcSuccess;
  Lpddr4      = (Outputs->DdrType == MRC_DDR_TYPE_LPDDR4);
  Lpddr5      = (Outputs->DdrType == MRC_DDR_TYPE_LPDDR5);
  Ddr4        = (Outputs->DdrType == MRC_DDR_TYPE_DDR4);
  Ddr5        = (Outputs->DdrType == MRC_DDR_TYPE_DDR5);
  Lpddr       = Outputs->Lpddr;
  RankMask    = Outputs->ValidRankMask;
  GetSetDis   = 0;
  GetSetEn    = 1;
  GetSetValN  = 0;
  GetSetValP  = 0;
  MaxChannel  = Outputs->MaxChannels;
  Gear4       = Outputs->Gear4;
  MinWidth    = 10; // To avoid speckling
  MrcCall->MrcSetMem ((UINT8 *) L2RBitFlag, sizeof (L2RBitFlag), 0);
  MrcCall->MrcSetMem ((UINT8 *) MarginBit,  sizeof (MarginBit),  0);
  MrcCall->MrcSetMem ((UINT8 *) SaveRxDqsP, sizeof (SaveRxDqsP), 0);
  MrcCall->MrcSetMem ((UINT8 *) SaveRxDqsN, sizeof (SaveRxDqsN), 0);
  SaveOutputs->MprLeftEdge = 0;

  // Setup the Read Data Pattern.  This depends on technology. DDR5 uses Galois LFSR so this value is not used.
  //          Lpddr           Ddr4
  // Pattern  01011010        10101010
  RdDataPtn   = (Lpddr) ? 0x5A : 0xAA;
  EccByte  = Ddr5 ? MRC_DDR5_ECC_BYTE : MRC_DDR4_ECC_BYTE;

  McRdbCreditsSave = 0;
  if (Gear4 && Lpddr) {
    for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
      for (Channel = 0; Channel < MaxChannel; Channel++) {
        if ((!MrcChannelExist (MrcData, Controller, Channel)) || IS_MC_SUB_CH (Lpddr, Channel)) {
          continue;
        }
        IpChannel = LP_IP_CH (Lpddr, Channel);
        Offset = OFFSET_CALC_MC_CH (MC0_CH0_CR_MC_RDB_CREDITS_REG, MC1_CH0_CR_MC_RDB_CREDITS_REG, Controller, MC0_CH1_CR_MC_RDB_CREDITS_REG, IpChannel);
        McRdbCredits.Data = MrcReadCR (MrcData, Offset);
        McRdbCreditsSave = McRdbCredits.Data;             // Back up the original value, should be the same on all channels
        McRdbCredits.Bits.VC0_reads_entries   = 29;
        McRdbCredits.Bits.VC1_reads_entries   = 3;
        McRdbCredits.Bits.Total_reads_entries = 32;
        MrcWriteCR (MrcData, Offset, McRdbCredits.Data);
      }
    }
  }

  DqsStart = RMPR_DQS_START;
  DqsStop  = RMPR_DQS_STOP;

  LeftEdge = MRC_INT8_MAX;

  if (Ddr5 && Inputs->ReadMprVA) {
    // Loop Count is decreased to 7 to compensate for running each IOTest 8 times. 2^7 * 2^3 = 2^10
    // DDR5 does not use a predefined RdDataPtn since it uses Galois
    LoopCount = 7;
  } else {
    LoopCount = 10;
  }

  if (Lpddr5 && Gear4 && (Outputs->Frequency >= f5200)) {
    LoopCount = 4;
  }

  SetupMcMprConfig (MrcData, &SaveData, MRC_ENABLE);

  if (Lpddr4) {
    MrDqCalPatA = 32;
    MrDqCalPatB = 40;
    MrDqCalLow  = 15;
    MrDqCalHigh = 20;
  } else if (Lpddr5) {
    MrDqCalPatA = 33;
    MrDqCalPatB = 34;
    MrDqCalLow  = 31;
    MrDqCalHigh = 32;
  } else {
    MrDqCalPatA = 0;
    MrDqCalPatB = 0;
    MrDqCalLow  = 0;
    MrDqCalHigh = 0;
  }

  // Setup REUT Engine
  SetupIOTestCpgcRead (MrcData, Outputs->McChBitMask, LoopCount, NSOE, RdDataPtn);

  // Program chunk mask for RxDqsP/N.
  // This is set to all chunks in SetupIOTest, so needs to be done after calling SetupIOTest.
  if ((Param == RdTP) || (Param == RdTN)) {
    // For RxDqsP check only even chunks
    UiMask = 0x5555;
    if (Param == RdTN) {
      // For RxDqsN check only odd chunks
      UiMask = UiMask << 1;
    }
    MrcSetChunkAndClErrMsk(MrcData, 0xFF, UiMask);
  }

  // Set DQS Delay to 32
  // If training RdTN or RdTP, save the other strobe delay to be restored at the end
  GetSetVal = 32;
  for (Rank = 0; Rank < MAX_RANK_IN_CHANNEL; Rank++) {
    for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
      for (Channel = 0; Channel < MaxChannel; Channel++) {
        if (MrcRankExist (MrcData, Controller, Channel, Rank)) {
          for (Byte = 0; Byte < Outputs->SdramCount; Byte++) {
            // Set DQS Delay to 32
            if (Param == RdTN) {
              // Save RxDqsP value
              MrcGetSetStrobe (MrcData, Controller, Channel, Rank, Byte, RxDqsPDelay, ReadCached,   &SaveRxDqsP[Rank][Controller][Channel][Byte]);
              MrcGetSetStrobe (MrcData, Controller, Channel, Rank, Byte, RxDqsNDelay, WriteToCache, &GetSetVal);
            } else if (Param == RdTP) {
              // Save RxDqsN value
              MrcGetSetStrobe (MrcData, Controller, Channel, Rank, Byte, RxDqsNDelay, ReadCached,   &SaveRxDqsN[Rank][Controller][Channel][Byte]);
              MrcGetSetStrobe (MrcData, Controller, Channel, Rank, Byte, RxDqsPDelay, WriteToCache, &GetSetVal);
            } else {
              MrcGetSetStrobe (MrcData, Controller, Channel, Rank, Byte, RxDqsNDelay, WriteToCache, &GetSetVal);
              MrcGetSetStrobe (MrcData, Controller, Channel, Rank, Byte, RxDqsPDelay, WriteToCache, &GetSetVal);
            }
          } //Byte
        } //Rank
      } // Channel
    } // Controller
  } // Rank

  MrcFlushRegisterCachedData (MrcData);

  for (Rank = 0; Rank < MAX_RANK_IN_CHANNEL; Rank++) {
    if (!((MRC_BIT0 << Rank) & RankMask)) {
      continue; // Skip if both channels empty
    }
    McChBitMask = 0;
    // Program MR3 and Mask RAS/WE to prevent scheduler for issuing non-Read commands
    for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
      for (Channel = 0; Channel < MaxChannel; Channel++) {
        McChBitMask |= SelectReutRanks (MrcData, Controller, Channel, 1 << Rank, FALSE, 0); //Save particular mask to use with runIOTest later
        if (!(MrcRankExist (MrcData, Controller, Channel, Rank))) {
          continue;
        }
        ChannelOut = &Outputs->Controller[Controller].Channel[Channel];

        // Enter MPR mode on DRAM for DDR4
        if (Ddr4) {
          Ddr4Mr3.Data = ChannelOut->Dimm[Rank / 2].Rank[Rank % 2].MR[mrMR3];
          Ddr4Mr3.Bits.MprOperation = 1;
          Status = MrcWriteMRS (MrcData, Controller, Channel, (1 << Rank), mrMR3, Ddr4Mr3.Data);
        } else if (Lpddr) {
          MrcIssueMrw (MrcData, Controller, Channel, Rank, MrDqCalPatA, RdDataPtn, TRUE); // DQ Calibration Pattern "A"
          MrcIssueMrw (MrcData, Controller, Channel, Rank, MrDqCalPatB, RdDataPtn, TRUE); // DQ Calibration Pattern "B"
          MrcIssueMrw (MrcData, Controller, Channel, Rank, MrDqCalLow,  0x00, TRUE); // Lower Byte Invert for DQ Calibration
          MrcIssueMrw (MrcData, Controller, Channel, Rank, MrDqCalHigh, 0x00, TRUE); // Upper Byte Invert for DQ Calibration
        } else if (Ddr5) {
          // Initial setup of DRAM
          MrcIssueMrw (MrcData, Controller, Channel, Rank, mrMR25, 0x01, TRUE); // Read Training Mode Settings
          MrcIssueMrw (MrcData, Controller, Channel, Rank, mrMR26, 0x5A, TRUE); // LFSR0 Seed
          MrcIssueMrw (MrcData, Controller, Channel, Rank, mrMR27, 0x3C, TRUE); // LFSR1 Seed
          MrcIssueMrw (MrcData, Controller, Channel, Rank, mrMR28, 0x00, TRUE); // Read Pattern Invert DQ(7:0)
          MrcIssueMrw (MrcData, Controller, Channel, Rank, mrMR29, 0x00, TRUE); // Read Pattern Invert DQ(15:8)
          MrData = (Inputs->ReadMprVA) ? 0xFE : 0xFF;                           // LFSR Assignment (0-LFSR0, 1-LFSR1)
          MrcIssueMrw (MrcData, Controller, Channel, Rank, mrMR30, MrData, TRUE);
         }
      } // for Channel
    } // for Controller
    MrcGetSetMcCh (MrcData, MAX_CONTROLLER, MAX_CHANNEL, GsmMccMprTrainDdrOn, WriteNoCache, &GetSetEn);
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "--- CPGC-based RDMPR ---\n");
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Rank = %u\n", Rank);
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Channel\t0                1\nByte\t");
    MRC_DEBUG_MSG (
      Debug,
      MSG_LEVEL_NOTE, (
      Outputs->SdramCount == (MAX_SDRAM_IN_DIMM)
      ) ? "0 1 2 3 4 5 6 7 8 0 1 2 3 4 5 6 7 8" : "0 1 2 3 4 5 6 7 0 1 2 3 4 5 6 7"
      );

    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\n%s",(Param == RdTN) ? "DqsNDelay" : (Param == RdTP) ? "DqsPDelay" : "DqsDelay" );
    for (DqsDelay = DqsStart; DqsDelay < DqsStop; DqsDelay += RMPR_DQS_STEP) {
      MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\n% 5d\t", DqsDelay);
      // Program DQS Delays and download the Reg File for the current rank.

      Status = ChangeMargin (MrcData, RdT, DqsDelay, 0, 1, 0, 0, 0, 0, 0, 0, 0);

      for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
        for (Channel = 0; Channel < MaxChannel; Channel++) {
          if (!(MrcRankExist (MrcData, Controller, Channel, Rank))) {
            MRC_DEBUG_MSG (
              Debug,
              MSG_LEVEL_NOTE,
              (Channel != 0) ? "" : ((Outputs->SdramCount == MAX_SDRAM_IN_DIMM) ? "                  " : "                ")
              );
          }
        }
      }

      if (Ddr4) {
        // Clean the Rx FIFO using another MPR sequence - Bank 3 (all zeroes)
        for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
          for (Channel = 0; Channel < MaxChannel; Channel++) {
            if (MrcRankExist (MrcData, Controller, Channel, Rank)) {
              MrcGetSetBankSequence (MrcData, Controller, Channel, Ddr4RdMprBankL2pBank3, 2, MRC_SET);
            }
          } // for Channel
        } // for Controller
      }
      if (Lpddr) {
        // Clean the Rx FIFO using another MPR sequence - all zeroes
        MrcGetSetMcCh (MrcData, MAX_CONTROLLER, MAX_CHANNEL, GsmMccMprTrainDdrOn, WriteNoCache, &GetSetDis);
        for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
          for (Channel = 0; Channel < MaxChannel; Channel++) {
            if (!(MrcRankExist (MrcData, Controller, Channel, Rank))) {
              continue;
            }
            MrcIssueMrw (MrcData, Controller, Channel, Rank, MrDqCalPatA, 0, TRUE); // DQ Calibration Pattern "A"
            MrcIssueMrw (MrcData, Controller, Channel, Rank, MrDqCalPatB, 0, TRUE); // DQ Calibration Pattern "B"
          } // for Channel
        } // for Controller
        MrcGetSetMcCh (MrcData, MAX_CONTROLLER, MAX_CHANNEL, GsmMccMprTrainDdrOn, WriteNoCache, &GetSetEn);
      }

      // No need to clean RxFIFO in DDR5 as we use LFSR pattern
      if (!Ddr5) {
        SetupIOTestCpgcRead (MrcData, McChBitMask, 4, NSOE, RdDataPtn);
        RunIOTest (MrcData, McChBitMask, Outputs->DQPat, 1, 0);
      }

      // Go back to the original MPR pattern
      if (Ddr4) {
        for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
          for (Channel = 0; Channel < MaxChannel; Channel++) {
            if (MrcRankExist (MrcData, Controller, Channel, Rank)) {
              MrcGetSetBankSequence (MrcData, Controller, Channel, Ddr4RdMprBankL2p, 2, MRC_SET);
            }
          } // for Channel
        } // for Controller
      }
      if (Lpddr) {
        MrcGetSetMcCh (MrcData, MAX_CONTROLLER, MAX_CHANNEL, GsmMccMprTrainDdrOn, WriteNoCache, &GetSetDis);
        for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
          for (Channel = 0; Channel < MaxChannel; Channel++) {
            if (!(MrcRankExist (MrcData, Controller, Channel, Rank))) {
              continue;
            }
            MrcIssueMrw (MrcData, Controller, Channel, Rank, MrDqCalPatA, RdDataPtn, TRUE); // DQ Calibration Pattern "A"
            MrcIssueMrw (MrcData, Controller, Channel, Rank, MrDqCalPatB, RdDataPtn, TRUE); // DQ Calibration Pattern "B"
          } // for Channel
        } // for Controller
        MrcGetSetMcCh (MrcData, MAX_CONTROLLER, MAX_CHANNEL, GsmMccMprTrainDdrOn, WriteNoCache, &GetSetEn);
      }

      if (!Ddr5) { // For DDR5 we do this once in the beginning
        SetupIOTestCpgcRead (MrcData, McChBitMask, LoopCount, NSOE, RdDataPtn);

        // Program chunk mask for RxDqsP/N.
        // This is set to all chunks in SetupIOTest, so needs to be done after calling SetupIOTest.
        if ((Param == RdTP) || (Param == RdTN)) {
          // For RxDqsP check only even chunks
          UiMask = 0x5555;
          if (Param == RdTN) {
            // For RxDqsN check only odd chunks
            UiMask = UiMask << 1;
          }
          MrcSetChunkAndClErrMsk(MrcData, 0xFF, UiMask);
        }

      }
      RunIOTest (MrcData, McChBitMask, Outputs->DQPat, 1, 0);

      // Update results for all ch/bytes
      MrcCall->MrcSetMem ((UINT8 *) BitGroupErr, sizeof (BitGroupErr), 0);
      for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
        for (Channel = 0; Channel < MaxChannel; Channel++) {
          if (!(MrcRankExist (MrcData, Controller, Channel, Rank))) {
            continue;
          }
          ErrStatus = 0;
          EccStatus = 0;
          ChunkStatus = 0;
          ChunkResult[Controller][Channel] = 0;
          // Read out per byte error results and check for any byte error
          MrcGetMiscErrStatus (MrcData, Controller, Channel, ByteGroupErrStatus, &ErrStatus);
          if (Outputs->EccSupport) { //Read out per byte ecc status
            MrcGetMiscErrStatus (MrcData, Controller, Channel, EccLaneErrStatus, &EccStatus);
          }
          // Read per chunk error status
          MrcGetMiscErrStatus (MrcData, Controller, Channel, ChunkErrStatus, &ChunkStatus);
          ChunkResult[Controller][Channel] = (UINT16) ChunkStatus;
          MrcGetBitGroupErrStatus (MrcData, Controller, Channel, BitGroupErr[Controller][Channel]);
          for (Byte = 0; Byte < Outputs->SdramCount; Byte++) {
            if (!MrcByteExist (MrcData, Controller, Channel, Byte)) {
              MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "  ");
              continue;
            }
            if (Byte < EccByte) {
              Pass = ((((UINT32) ErrStatus) & (1 << Byte)) == 0);
            } else {
              Pass = (((UINT32) EccStatus) == 0);
            }
            MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, Pass ? ". " : "# ");

            // Track the earliest left edge across all bytes / ranks
            if ((Pass && (DqsDelay < LeftEdge))) {
              LeftEdge = DqsDelay;
            }

            if (DqsDelay == DqsStart) {
              if (Pass) {
                CurrentPassingStart[Controller][Channel][Byte] = CurrentPassingEnd[Controller][Channel][Byte] = DqsDelay;
                LargestPassingStart[Controller][Channel][Byte] = LargestPassingEnd[Controller][Channel][Byte] = DqsDelay;
              } else {
                CurrentPassingStart[Controller][Channel][Byte] = CurrentPassingEnd[Controller][Channel][Byte] = -DqsStart - 1;
                LargestPassingStart[Controller][Channel][Byte] = LargestPassingEnd[Controller][Channel][Byte] = -DqsStart - 1;
              }
            } else {
              if (Pass) {
                if (CurrentPassingEnd[Controller][Channel][Byte] == (DqsDelay - RMPR_DQS_STEP)) {
                  CurrentPassingEnd[Controller][Channel][Byte] = DqsDelay;
                } else {
                  CurrentPassingStart[Controller][Channel][Byte] = CurrentPassingEnd[Controller][Channel][Byte] = DqsDelay;
                }
                // Update Largest variables

                cWidth  = CurrentPassingEnd[Controller][Channel][Byte] - CurrentPassingStart[Controller][Channel][Byte];
                lWidth  = LargestPassingEnd[Controller][Channel][Byte] - LargestPassingStart[Controller][Channel][Byte];
                if (cWidth > lWidth) {
                  LargestPassingStart[Controller][Channel][Byte]  = CurrentPassingStart[Controller][Channel][Byte];
                  LargestPassingEnd[Controller][Channel][Byte]    = CurrentPassingEnd[Controller][Channel][Byte];
                }
              }
            }
          } // for Byte
        } // for Channel
      } // for Controller

      for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
        for (Channel = 0; Channel < MaxChannel; Channel++) {
          ErrStatus = 0;
          for (Byte = 0; Byte < Outputs->SdramCount; Byte++) {
            if (Byte < EccByte) {
              ErrStatus |= MrcCall->MrcLeftShift64 (BitGroupErr[Controller][Channel][Byte], Byte * 8);
            }
            // Update Margin per bit
            if (DqsDelay == DqsStart) {
              for (Bit = 0; Bit < MAX_BITS; Bit++) {
                MarginBit[Rank][Controller][Channel][Byte][Bit][0] = DqsStart;
                MarginBit[Rank][Controller][Channel][Byte][Bit][1] = DqsStop;
              }
            } else {
              for (Bit = 0; Bit < MAX_BITS; Bit++) {
                if (L2RBitFlag[Rank][Controller][Channel][Byte][Bit] == 0) { // Update Left Edge
                  //MRC_DEBUG_MSG(Debug, MSG_LEVEL_NOTE, "\n Update Left Edge : ErrMaskMC%dC%dB%d 0x%02X\n", Controller, Channel, Byte, BitGroupErr[Controller][Channel][Byte]);
                  MarginBit[Rank][Controller][Channel][Byte][Bit][0] = DqsDelay;
                  if (!(BitGroupErr[Controller][Channel][Byte] & (1 << Bit))) { // Fail to Pass transition (1-> 0)
                    //MRC_DEBUG_MSG(Debug, MSG_LEVEL_NOTE, "\n Update Left Edge last time\n");
                    L2RBitFlag[Rank][Controller][Channel][Byte][Bit] = 1; // not update left edge anymore
                    MarginBit[Rank][Controller][Channel][Byte][Bit][0] -= RMPR_DQS_STEP;
                  }
                } else if (L2RBitFlag[Rank][Controller][Channel][Byte][Bit] == 1) { // Update Right Edge
                  //MRC_DEBUG_MSG(Debug, MSG_LEVEL_NOTE, "\n Update Right Edge : ErrMaskMC%dC%dB%d 0x%02X\n", Controller, Channel, Byte, BitGroupErr[Controller][Channel][Byte]);
                  MarginBit[Rank][Controller][Channel][Byte][Bit][1] = DqsDelay;
                  if ((BitGroupErr[Controller][Channel][Byte] & (1 << Bit)) && (ABS(MarginBit[Rank][Controller][Channel][Byte][Bit][1] - MarginBit[Rank][Controller][Channel][Byte][Bit][0]) > MinWidth)) { // Pass to Fail transition (0-> 1), minWidth 10 ticks
                    L2RBitFlag[Rank][Controller][Channel][Byte][Bit] = 2; // not update right edge anymore
                    //MRC_DEBUG_MSG(Debug, MSG_LEVEL_NOTE, "\n Update Right Edge last time\n");
                  }
                }
              } // Bit
            } // else
          } // Byte
          if (MrcRankExist (MrcData, Controller, Channel, Rank)) {
            if (Outputs->EccSupport) {
              MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "0x%02X_", BitGroupErr[Controller][Channel][EccByte]);
            }
            MRC_DEBUG_MSG (
              Debug,
              MSG_LEVEL_NOTE,
              "%s%016llX 0x%04X ",
              Outputs->EccSupport ? "" : "0x",
              ErrStatus,
              ChunkResult[Controller][Channel]
              );
          }
        }
      }

      Status = IoReset (MrcData);
    } // for DqsDelay

    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\n");

    // Clean Up registers.
    // MPR_TRAIN_DDR_ON bit will force a special command so clear it before MRS command
    MrcGetSetMcCh (MrcData, MAX_CONTROLLER, MAX_CHANNEL, GsmMccMprTrainDdrOn, WriteNoCache, &GetSetDis);
    for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
      for (Channel = 0; Channel < MaxChannel; Channel++) {
        if (MrcRankExist (MrcData, Controller, Channel, Rank)) {
          ChannelOut = &Outputs->Controller[Controller].Channel[Channel];


          // Exit MPR mode on DRAM for DDR4
          if (Ddr4) {
            Ddr4Mr3.Data = ChannelOut->Dimm[Rank / 2].Rank[Rank % 2].MR[mrMR3];
            Status = MrcWriteMRS (MrcData, Controller, Channel, (MRC_BIT0 << Rank), mrMR3, Ddr4Mr3.Data);
          }

          MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "MC%u.C%u.R%u: \tLeft\tRight\tWidth\tCenter\tRxDqs%s\n", Controller, Channel, Rank, (Param == RdTN) ? "N" : (Param == RdTP) ? "P" : "PN");
          for (Byte = 0; Byte < Outputs->SdramCount; Byte++) {
            if (!MrcByteExist (MrcData, Controller, Channel, Byte)) {
              continue;
            }
            Left  = LargestPassingStart[Controller][Channel][Byte];
            Right = LargestPassingEnd[Controller][Channel][Byte];
            lWidth = Right - Left;
            Center = Left + (lWidth / 2);

            // Error Handler if eye not found for all bytes
            if (lWidth == 0) {
              MRC_DEBUG_MSG (
                Debug,
                MSG_LEVEL_ERROR,
                "\nERROR!! NO EYE found for Mc: %u Channel: %u Rank: %u Byte: %u \n",
                Controller,
                Channel,
                Rank,
                Byte
                );
              // Mark it as zero margin in MarginResult
              Left   = 0;
              Right  = 0;
              Center = 0;
              MprStatus = mrcReadMPRErr;
            } else if (lWidth <= RMPR_MIN_WIDTH) {
              MRC_DEBUG_MSG (
                Debug,
                MSG_LEVEL_WARNING,
                "WARNING!! lWidth <= %u for Mc: %u Channel: %u Rank: %u Byte: %u\n",
                RMPR_MIN_WIDTH,
                Controller,
                Channel,
                Rank,
                Byte
                );
            }
            Group = (Param == RdTN) ? RxDqsNDelay : RxDqsPDelay;
            MrcGetSetStrobe (MrcData, Controller, Channel, Rank, Byte, Group, ReadFromCache, &GetSetVal);
            FinalDqs[Controller][Channel][Rank][Byte] = Center + (INT16) GetSetVal;
            MRC_DEBUG_MSG (
              Debug,
              MSG_LEVEL_NOTE,
              "  B%u:\t\t%d\t%d\t%d\t%d\t%d\n",
              Byte,
              Left,
              Right,
              lWidth,
              Center,
              FinalDqs[Controller][Channel][Rank][Byte]
              );
            // Update the MarginResult struct for future tests
            Margin    = &Outputs->MarginResult[LastRxT][Rank][Controller][Channel][Byte][0];
            Margin[0] = ABS (10 * (Left  - Center));
            Margin[1] = ABS (10 * (Right - Center));

            // For PBD results
            for (Bit = 0; Bit < MAX_BITS; Bit++) {
              MarginBit[Rank][Controller][Channel][Byte][Bit][1] -= (INT8) Center; // Normalize margins
              MarginBit[Rank][Controller][Channel][Byte][Bit][0] -= (INT8) Center;
            }
          } // for Byte
        }
      } // for Channel
    } // for Controller

    for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
      for (Channel = 0; Channel < MaxChannel; Channel++) {
        if (MrcRankExist (MrcData, Controller, Channel, Rank)) {
          MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\nMC%u.C%u.R%u:", Controller, Channel, Rank);
          for (Byte = 0; Byte < Outputs->SdramCount; Byte++) {
            if (!MrcByteExist(MrcData, Controller, Channel, Byte)) {
              continue;
            }
            MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\nB%u:", Byte);
            for (Bit = 0; Bit < MAX_BITS; Bit++) {
              MRC_DEBUG_MSG(Debug, MSG_LEVEL_NOTE, "\t[%3d %3d]", MarginBit[Rank][Controller][Channel][Byte][Bit][0], MarginBit[Rank][Controller][Channel][Byte][Bit][1]);
            }
          }
        }
      }
    }
  } // for Rank
  MRC_DEBUG_MSG(Debug, MSG_LEVEL_NOTE, "\n");

  // Now program the DQS center values on populated ranks.
  // Need to do it after all ranks are trained, because we need to keep the same DQS value on all ranks
  // during the training.
  for (Rank = 0; Rank < MAX_RANK_IN_CHANNEL; Rank++) {
    for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
      for (Channel = 0; Channel < MaxChannel; Channel++) {
        if (MrcRankExist (MrcData, Controller, Channel, Rank)) {
          for (Byte = 0; Byte < Outputs->SdramCount; Byte++) {
            GetSetVal = (INT64) FinalDqs[Controller][Channel][Rank][Byte];
            if (Param == RdTN) {
              GetSetValN = GetSetVal;
              GetSetValP = SaveRxDqsP[Rank][Controller][Channel][Byte]; // Restore RxDqsP
            } else if (Param == RdTP) {
              GetSetValN = SaveRxDqsN[Rank][Controller][Channel][Byte]; // Restore RxDqsN
              GetSetValP = GetSetVal;
            } else {
              GetSetValN = GetSetVal;
              GetSetValP = GetSetVal;
            }
            MrcGetSetStrobe (MrcData, Controller, Channel, Rank, Byte, RxDqsPDelay, WriteToCache, &GetSetValP);
            MrcGetSetStrobe (MrcData, Controller, Channel, Rank, Byte, RxDqsNDelay, WriteToCache, &GetSetValN);
          }
        }
      }
    }
  }
  MrcFlushRegisterCachedData (MrcData);

  // Call for RX PBD centering
  if (((Param == RdTN) || (Param == RdTP)) && PerBit) {
    PerBit1DCentering (MrcData, Param, 10, 1, MRC_PRINTS_ON, MarginBit); // TestType = MPR
  }

  // Clean up after Test.
  SetupMcMprConfig (MrcData, &SaveData, MRC_DISABLE);

  // Restore MC credits
  if (Gear4 && Lpddr) {
    for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
      for (Channel = 0; Channel < MaxChannel; Channel++) {
        if ((!MrcChannelExist (MrcData, Controller, Channel)) || IS_MC_SUB_CH (Lpddr, Channel)) {
          continue;
        }
        IpChannel = LP_IP_CH (Lpddr, Channel);
        Offset = OFFSET_CALC_MC_CH (MC0_CH0_CR_MC_RDB_CREDITS_REG, MC1_CH0_CR_MC_RDB_CREDITS_REG, Controller, MC0_CH1_CR_MC_RDB_CREDITS_REG, IpChannel);
        MrcWriteCR (MrcData, Offset, McRdbCreditsSave);
      }
    }
  }

  Status  = ChangeMargin (MrcData, RdT, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0);
  Status  = IoReset (MrcData);

  if ((MprStatus != mrcSuccess) && (Inputs->ExitOnFailure == 1)) {
    Status = MprStatus;
  }

  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "LeftEdge: %d\n", LeftEdge);
  if (Print && (LeftEdge < SaveOutputs->MprLeftEdge)) {
    SaveOutputs->MprLeftEdge = LeftEdge;
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "MprLeftEdge: %d\n", SaveOutputs->MprLeftEdge);
  }

  return Status;
}

/**
  Perform Read MPR Training.
  Center read DQ-DQS with MPR pattern.

  @param[in, out] MrcData   - Include all MRC global data.

  @retval MrcStatus - mrcSuccess or reason for failure.
**/
MrcStatus
MrcReadMprTraining (
  IN OUT MrcParameters *const MrcData
  )
{
  return MrcReadMprTrainingNormal (MrcData, RdT, FALSE, MRC_PRINTS_ON);  // CPGC-based
}

#if 0
// @todo_adl can we enable this ?
/**
  Perform Read MPR Training.
  Center read DQ-DQS with MPR pattern.
  This is the HW method of the Read MPR algorithm.

  @param[in, out] MrcData   - Include all MRC global data.

  @retval MrcStatus - mrcSuccess or reason for failure.
**/
MrcStatus
ReadMprTraining (
  IN OUT MrcParameters *const MrcData,
  IN     BOOLEAN              Print
  )
{
  const MrcInput   *Inputs;
  MrcDebug         *Debug;
  MrcOutput        *Outputs;
  MrcChannelOut    *ChannelOut;
  MrcStatus        Status;
  MrcStatus        MprStatus;
  MrcDdrType       DdrType;
  UINT8            MaxChannel;
  UINT8            Controller;
  UINT8            Channel;
  UINT8            IpChannel;
  UINT8            Rank;
  UINT8            Byte;
  UINT8            McChBitMask;
  UINT8            RankMask;  // RankBitMask for both channels
  UINT8            LoopCount;
  UINT8            TestDoneStatus;
  INT8             DqsDelay;
  INT16            CurrentPassingStart[MAX_CONTROLLER][MAX_CHANNEL][MAX_SDRAM_IN_DIMM];
  INT16            CurrentPassingEnd[MAX_CONTROLLER][MAX_CHANNEL][MAX_SDRAM_IN_DIMM];
  INT16            LargestPassingStart[MAX_CONTROLLER][MAX_CHANNEL][MAX_SDRAM_IN_DIMM];
  INT16            LargestPassingEnd[MAX_CONTROLLER][MAX_CHANNEL][MAX_SDRAM_IN_DIMM];
  INT16            FinalDqs[MAX_CONTROLLER][MAX_CHANNEL][MAX_RANK_IN_CHANNEL][MAX_SDRAM_IN_DIMM];
  INT16            cWidth;
  INT16            lWidth;
  INT16            Center;
  INT16            Left;
  INT16            Right;
  INT64            GroupOffset;
  INT64            GetSetDis;
  INT64            GetSetEn;
  INT64            GetSetVal;
  UINT8            RDRDsgPrevData;
  UINT8            RDRDdgPrevData;
  INT64            DataTrainFeedbackField;
  BOOLEAN          Pass;
  BOOLEAN          Lpddr;
  BOOLEAN          Lpddr4;
  BOOLEAN          Lpddr5;
  BOOLEAN          Ddr4;
  UINT16          *Margin;
  UINT32           Offset;
  UINT8            MrDqCalPatA;
  UINT8            MrDqCalPatB;
  UINT8            MrDqCalLow;
  UINT8            MrDqCalHigh;
  INT64                              GetSetValue;
  MRC_BG_BANK_PAIR                   BankMapping[MAX_CONTROLLER][MAX_CHANNEL][MAX_BANKS];
  MC0_REQ0_CR_CPGC_SEQ_CTL_STRUCT    CpgcSeqCtl;
  MC0_REQ0_CR_CPGC_SEQ_STATUS_STRUCT CpgcSeqStatus;
  DDR4_MODE_REGISTER_3_STRUCT        Ddr4Mr3;
  UINT32                             CpgcChickenPrevData[MAX_CONTROLLER];
  MC0_CR_CPGC2_V_CHICKEN_STRUCT      CpgcChickenStruct;

  Inputs         = &MrcData->Inputs;
  Outputs        = &MrcData->Outputs;
  Debug          = Print ? &Outputs->Debug : NULL;
  Status         = mrcSuccess;
  MprStatus      = mrcSuccess;
  DdrType        = Outputs->DdrType;
  Lpddr4         = (DdrType == MRC_DDR_TYPE_LPDDR4);
  Lpddr5         = (DdrType == MRC_DDR_TYPE_LPDDR5);
  Lpddr          = Outputs->Lpddr;
  Ddr4           = (DdrType == MRC_DDR_TYPE_DDR4);
  GetSetDis      = 0;
  GetSetEn       = 1;
  MaxChannel     = Outputs->MaxChannels;
  RDRDsgPrevData = 0;
  RDRDdgPrevData = 0;

  LoopCount = 10;

  if (Lpddr) {
    // Set GsmMctRDRDsg and GsmMctRDRDdg to 8 (BL 16)
    for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
      CpgcChickenPrevData[Controller] = 0;
      if (MrcControllerExist (MrcData, Controller)) {
        Offset = OFFSET_CALC_CH (MC0_CR_CPGC2_V_CHICKEN_REG, MC1_CR_CPGC2_V_CHICKEN_REG, Controller);
        CpgcChickenPrevData[Controller] = MrcReadCR (MrcData, Offset);
        CpgcChickenStruct.Data = CpgcChickenPrevData[Controller];
        CpgcChickenStruct.Bits.MPR_TEST_REQ_DBLR = 1;
        MrcWriteCR (MrcData, Offset, CpgcChickenStruct.Data);
        MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Setting MPR_TEST_REQ_DBLR.\n");
        GetSetVal = 1;
        MrcGetSetMc (MrcData, Controller, GsmMccHalfCachelineMode, WriteToCache | PrintValue, &GetSetVal);
        Channel = Outputs->Controller[Controller].FirstPopCh;
        MrcGetSetMcCh (MrcData, Controller, Channel, GsmMctRDRDsg, ReadFromCache | PrintValue, &GetSetVal);
        RDRDsgPrevData = (UINT8) GetSetVal;
        MrcGetSetMcCh (MrcData, Controller, Channel, GsmMctRDRDdg, ReadFromCache | PrintValue, &GetSetVal);
        RDRDdgPrevData = (UINT8) GetSetVal;
      }
    }
    GetSetVal = 8;
    for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
      for (Channel = 0; Channel < MAX_CHANNEL; Channel++) {
        if ((!MrcChannelExist (MrcData, Controller, Channel)) || IS_MC_SUB_CH (Lpddr, Channel)) {
          continue;
        }
        MrcGetSetMcCh (MrcData, Controller, Channel, GsmMctRDRDsg, WriteToCache | PrintValue, &GetSetVal);
        MrcGetSetMcCh (MrcData, Controller, Channel, GsmMctRDRDdg, WriteToCache | PrintValue, &GetSetVal);
        MrcFlushRegisterCachedData (MrcData);
        // Must update the XARB bubble injector when TAT values change
        SetTcBubbleInjector (MrcData, Controller, Channel);
      }
    }
  }

  if (Lpddr4) {
    MrDqCalPatA = 32;
    MrDqCalPatB = 40;
    MrDqCalLow = 15;
    MrDqCalHigh = 20;
  } else if (Lpddr5) {
    MrDqCalPatA = 33;
    MrDqCalPatB = 34;
    MrDqCalLow = 31;
    MrDqCalHigh = 32;
  } else {
    MrDqCalPatA = 0;
    MrDqCalPatB = 0;
    MrDqCalLow = 0;
    MrDqCalHigh = 0;
  }

  // Use basic addressing mode (open a page on a rank and keep writing/reading to it)
  // Rotate through all 8 logical ranks
  // LFSR and LMN disabled.
  McChBitMask = Outputs->McChBitMask;
  RankMask  = Outputs->ValidRankMask;

  GroupOffset = 32; // RxDqs PI Center
  for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
    for (Channel = 0; Channel < MaxChannel; Channel++) {
      if (MrcChannelExist (MrcData, Controller, Channel)) {
        // Set DQS Delay to 32
        // Update RxDqsP & RxDqsN - leave other parameter the same; can we update in the next loop or do it per channel
        MrcGetSetStrobe (MrcData, Controller, Channel, MAX_RANK_IN_CHANNEL, MAX_SDRAM_IN_DIMM, RxDqsNDelay, WriteToCache, &GroupOffset);
        MrcGetSetStrobe (MrcData, Controller, Channel, MAX_RANK_IN_CHANNEL, MAX_SDRAM_IN_DIMM, RxDqsPDelay, WriteToCache, &GroupOffset);
        if (Ddr4) {
          // Save Bank Mapping
          MrcGetSetBankSequence (MrcData, Controller, Channel, &BankMapping[Controller][Channel][0], 2, MRC_GET);
          MrcGetSetBankSequence (MrcData, Controller, Channel, Ddr4RdMprBankL2p, 2, MRC_SET);
        }
      }
    } // Channel
  } // Controller
  MrcFlushRegisterCachedData (MrcData);

  //
  // Setup REUT Engine
  //
  SetupIOTestMPR (MrcData, McChBitMask, LoopCount, NSOE, 0, 0);

  // Enable Rank Mux Override
  MrcGetSetChStrb (MrcData, MAX_CONTROLLER, MAX_CHANNEL, MAX_SDRAM_IN_DIMM, GsmIocRankOverrideEn, WriteNoCache, &GetSetEn);

  for (Rank = 0; Rank < MAX_RANK_IN_CHANNEL; Rank++) {
    if (!((MRC_BIT0 << Rank) & RankMask)) {
      continue; // Skip for empty channels
    }

    // Update Rank Mux Override for the rank under test
    GetSetVal = Rank;
    MrcGetSetChStrb (MrcData, MAX_CONTROLLER, MAX_CHANNEL, MAX_SDRAM_IN_DIMM, GsmIocRankOverrideVal, WriteNoCache, &GetSetVal);
    //
    // Program MR3 and Mask RAS/WE to prevent scheduler for issuing non-Read commands
    //
    McChBitMask = 0;
    for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
      for (Channel = 0; Channel < MaxChannel; Channel++) {
        McChBitMask |= SelectReutRanks (MrcData, Controller, Channel, 1 << Rank, FALSE, 0);
        if (!(MrcRankExist (MrcData, Controller, Channel, Rank))) {
          continue;
        }
        ChannelOut = &Outputs->Controller[Controller].Channel[Channel];
        //
        // Enter MPR mode on DRAM for DDR3/4
        //
        if (Ddr4) {
          Ddr4Mr3.Data = ChannelOut->Dimm[Rank / 2].Rank[Rank % 2].MR[mrMR3];
          Ddr4Mr3.Bits.MprOperation = 1;
          Status = MrcWriteMRS (MrcData, Controller, Channel, (1 << Rank), mrMR3, Ddr4Mr3.Data);
        } else if (Lpddr) {
          // Pattern 0xAA implemented due to LSB bit being sent first -- only supporting 1010 pattern.
          MrcIssueMrw (MrcData, Controller, Channel, Rank, MrDqCalPatA, 0xAA, TRUE); // DQ Calibration Pattern "A"
          MrcIssueMrw (MrcData, Controller, Channel, Rank, MrDqCalPatB, 0xAA, TRUE); // DQ Calibration Pattern "B"
          MrcIssueMrw (MrcData, Controller, Channel, Rank, MrDqCalLow, 0x00, TRUE); // Lower Byte Invert for DQ Calibration
          MrcIssueMrw (MrcData, Controller, Channel, Rank, MrDqCalHigh, 0x00, TRUE); // Upper Byte Invert for DQ Calibration
        }
      } // for Channel
    } // for Controller
    MrcGetSetMcCh (MrcData, MAX_CONTROLLER, MAX_CHANNEL, GsmMccMprTrainDdrOn, WriteNoCache, &GetSetEn);

    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Rank = %u\n", Rank);
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Channel\t0                1\nByte\t");
    MRC_DEBUG_MSG (
      Debug,
      MSG_LEVEL_NOTE, (
      Outputs->SdramCount == (MAX_SDRAM_IN_DIMM)
      ) ? "0 1 2 3 4 5 6 7 8 0 1 2 3 4 5 6 7 8" : "0 1 2 3 4 5 6 7 0 1 2 3 4 5 6 7"
      );

    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\nDqsDelay");
    for (DqsDelay = RMPR_DQS_START; DqsDelay < RMPR_DQS_STOP; DqsDelay += RMPR_DQS_STEP) {
      MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\n% 5d\t", DqsDelay);

      //
      // Program DQS Delays and download the Reg File for the current rank.
      //
      Status = ChangeMargin (MrcData, RdT, DqsDelay, 0, 1, 0, 0, 0, 0, 0, 0, 0);

      for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
        for (Channel = 0; Channel < MaxChannel; Channel++) {
          if (!(MrcRankExist (MrcData, Controller, Channel, Rank))) {
            MRC_DEBUG_MSG (
              Debug,
              MSG_LEVEL_NOTE,
              (Channel != 0) ? "" : ((Outputs->SdramCount == MAX_SDRAM_IN_DIMM) ? "                  " : "                ")
              );
          } else {
            // Force on SenseAmp
            // Enable RX Training mode. Turn on the ODT.
            MrcGetSetChStrb (MrcData, Controller, Channel, MAX_SDRAM_IN_DIMM, GsmIocForceRxAmpOn, WriteToCache, &GetSetEn);
            MrcGetSetChStrb (MrcData, Controller, Channel, MAX_SDRAM_IN_DIMM, GsmIocReadDqDqsMode, WriteNoCache, &GetSetEn);
            MrcGetSetChStrb (MrcData, Controller, Channel, MAX_SDRAM_IN_DIMM, GsmIocForceOdtOn, WriteToCache, &GetSetEn);
            MrcFlushRegisterCachedData (MrcData);
          }
        } // for Channel
      } // for Controller

      Status = IoReset (MrcData);

      // Start CPGC and run for 1uS
      CpgcSeqCtl.Data = 0;
      CpgcSeqCtl.Bits.START_TEST = 1;
      Cpgc20ControlRegWrite (MrcData, McChBitMask, CpgcSeqCtl);

      // Wait for test to start clearing errors.
      MrcWait (MrcData, START_TEST_DELAY);

      // Clear Results for Prior Test and wait to obtain results
      Status = IoReset (MrcData);
      MrcWait (MrcData, IO_RESET_DELAY);

      // Stop CPGC
      CpgcSeqCtl.Data = 0;
      CpgcSeqCtl.Bits.STOP_TEST = 1;
      Cpgc20ControlRegWrite (MrcData, McChBitMask, CpgcSeqCtl);

      // Wait till CPGC test is done on all participating channels
      do {
        TestDoneStatus = 0;
        for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
          for (Channel = 0; Channel < MaxChannel; Channel++) {
            if ((MrcRankExist (MrcData, Controller, Channel, Rank)) && (!(IS_MC_SUB_CH (Lpddr, Channel)))) {
              IpChannel = LP_IP_CH (Lpddr, Channel);
              ChannelOut = &Outputs->Controller[Controller].Channel[IpChannel];
              Offset = OFFSET_CALC_MC_CH (MC0_REQ0_CR_CPGC_SEQ_STATUS_REG, MC1_REQ0_CR_CPGC_SEQ_STATUS_REG, Controller, MC0_REQ1_CR_CPGC_SEQ_STATUS_REG, IpChannel);
              CpgcSeqStatus.Data = MrcReadCR (MrcData, Offset);
              if (CpgcSeqStatus.Bits.TEST_DONE) {
                TestDoneStatus |= ChannelOut->CpgcChAssign << (Controller * MaxChannel);
              }
            }
          }
        }
      } while ((TestDoneStatus & McChBitMask) != McChBitMask);


      // Update results for all ch/bytes
      for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
        for (Channel = 0; Channel < MaxChannel; Channel++) {
          if (!(MrcRankExist (MrcData, Controller, Channel, Rank))) {
            continue;
          }
          for (Byte = 0; Byte < Outputs->SdramCount; Byte++) {
            if (!MrcByteExist (MrcData, Controller, Channel, Byte)) {
              MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "  ");
              continue;
            }
            MrcGetSetChStrb (MrcData, Controller, Channel, Byte, GsmIocDataTrainFeedback, ReadUncached, &DataTrainFeedbackField);
            Pass = (DataTrainFeedbackField == 1);
            MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, Pass ? ". " : "# ");

            if (DqsDelay == RMPR_DQS_START) {
              if (Pass) {
                CurrentPassingStart[Controller][Channel][Byte] = CurrentPassingEnd[Controller][Channel][Byte] = DqsDelay;
                LargestPassingStart[Controller][Channel][Byte] = LargestPassingEnd[Controller][Channel][Byte] = DqsDelay;
              } else {
                CurrentPassingStart[Controller][Channel][Byte] = CurrentPassingEnd[Controller][Channel][Byte] = -33;
                LargestPassingStart[Controller][Channel][Byte] = LargestPassingEnd[Controller][Channel][Byte] = -33;
              }
            } else {
              if (Pass) {
                if (CurrentPassingEnd[Controller][Channel][Byte] == (DqsDelay - RMPR_DQS_STEP)) {
                  CurrentPassingEnd[Controller][Channel][Byte] = DqsDelay;
                } else {
                  CurrentPassingStart[Controller][Channel][Byte] = CurrentPassingEnd[Controller][Channel][Byte] = DqsDelay;
                }
                //
                // Update Largest variables
                //
                cWidth  = CurrentPassingEnd[Controller][Channel][Byte] - CurrentPassingStart[Controller][Channel][Byte];
                lWidth  = LargestPassingEnd[Controller][Channel][Byte] - LargestPassingStart[Controller][Channel][Byte];
                if (cWidth > lWidth) {
                  LargestPassingStart[Controller][Channel][Byte]  = CurrentPassingStart[Controller][Channel][Byte];
                  LargestPassingEnd[Controller][Channel][Byte]    = CurrentPassingEnd[Controller][Channel][Byte];
                }
              }
            } // if (DqsDelay == RMPR_DQS_START)
          } // for Byte
        } // for Channel
      } // for Controller

      // Clear RxAmp and RxTrainingMode. Restore DataControl0
      MrcGetSetChStrb (MrcData, MAX_CONTROLLER, MAX_CHANNEL, MAX_SDRAM_IN_DIMM, GsmIocForceOdtOn, WriteToCache, &GetSetDis);
      MrcGetSetChStrb (MrcData, MAX_CONTROLLER, MAX_CHANNEL, MAX_SDRAM_IN_DIMM, GsmIocReadDqDqsMode, WriteNoCache, &GetSetDis);
      MrcGetSetChStrb (MrcData, MAX_CONTROLLER, MAX_CHANNEL, MAX_SDRAM_IN_DIMM, GsmIocForceRxAmpOn, WriteToCache, &GetSetDis);
      MrcFlushRegisterCachedData (MrcData);
      Status = IoReset (MrcData);
    } // for DqsDelay

    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\n");

    // Clean Up registers.
    // MPR_TRAIN_DDR_ON bit will force a special command so clear it before MRS command
    MrcGetSetMcCh (MrcData, MAX_CONTROLLER, MAX_CHANNEL, GsmMccMprTrainDdrOn, WriteNoCache, &GetSetDis);
    for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
      for (Channel = 0; Channel < MaxChannel; Channel++) {
        if (MrcRankExist (MrcData, Controller, Channel, Rank)) {
          ChannelOut = &Outputs->Controller[Controller].Channel[Channel];

          // Exit MPR mode on DRAM for DDR3/4
          if (Ddr4) {
            Ddr4Mr3.Data = ChannelOut->Dimm[Rank / 2].Rank[Rank % 2].MR[mrMR3];
            Status = MrcWriteMRS (MrcData, Controller, Channel, (MRC_BIT0 << Rank), mrMR3, Ddr4Mr3.Data);
          }

          MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "MC%u.C%u.R%u: \tLeft\tRight\tWidth\tCenter\tRxDqsPN\n", Controller, Channel, Rank);
          for (Byte = 0; Byte < Outputs->SdramCount; Byte++) {
            if (!MrcByteExist (MrcData, Controller, Channel, Byte)) {
              continue;
            }
            Left  = LargestPassingStart[Controller][Channel][Byte];
            Right = LargestPassingEnd[Controller][Channel][Byte];
            lWidth = Right - Left;
            Center = Left + (lWidth / 2);
            //
            // Error Handler if eye not found for all bytes
            //
            if (lWidth == 0) {
              MRC_DEBUG_MSG (
                Debug,
                MSG_LEVEL_ERROR,
                "ERROR!! NO EYE found for Mc: %u Channel: %u Rank: %u Byte: %u\n",
                Controller,
                Channel,
                Rank,
                Byte
                );
              //
              // Mark it as zero margin in MarginResult
              //
              Left   = 0;
              Right  = 0;
              Center = 0;
              MprStatus = mrcReadMPRErr;
            } else if (lWidth <= RMPR_MIN_WIDTH) {
              MRC_DEBUG_MSG (
                Debug,
                MSG_LEVEL_WARNING,
                "WARNING!! lWidth <= %u for Mc: %u Channel: %u Rank: %u Byte: %u\n",
                RMPR_MIN_WIDTH,
                Controller,
                Channel,
                Rank,
                Byte
                );
            }
            MrcGetSetStrobe (MrcData, Controller, Channel, Rank, Byte, RxDqsPDelay, ReadFromCache, &GetSetValue);
            FinalDqs[Controller][Channel][Rank][Byte] = Center + (INT16) GetSetValue;
            MRC_DEBUG_MSG (
              Debug,
              MSG_LEVEL_NOTE,
              "  B%u: \t%d\t%d\t%d\t%d\t%d\n",
              Byte,
              Left,
              Right,
              lWidth,
              Center,
              FinalDqs[Controller][Channel][Rank][Byte]
              );
            //
            // Update the MarginResult struct for future tests
            // Store the new margins relative to the center
            //
            Margin    = &Outputs->MarginResult[LastRxT][Rank][Controller][Channel][Byte][0];
            Margin[0] = ABS (10 * (Left  - Center));
            Margin[1] = ABS (10 * (Right - Center));
          } // for Byte
        } // for MrcRankExist
      } // for Channel
    } // for Controller
  } // for Rank

  //
  // Now program the DQS center values on populated ranks.
  // Need to do it after all ranks are trained, because we need to keep the same DQS value on all ranks
  // during the training.
  //
  for (Rank = 0; Rank < MAX_RANK_IN_CHANNEL; Rank++) {
    for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
      for (Channel = 0; Channel < MaxChannel; Channel++) {
        if (MrcRankExist (MrcData, Controller, Channel, Rank)) {
          for (Byte = 0; Byte < Outputs->SdramCount; Byte++) {
            if (!MrcByteExist (MrcData, Controller, Channel, Byte)) {
              continue;
            }
            GetSetValue = (INT64) FinalDqs[Controller][Channel][Rank][Byte];
            MrcGetSetStrobe (MrcData, Controller, Channel, Rank, Byte, RxDqsPDelay, WriteToCache, &GetSetValue);
            MrcGetSetStrobe (MrcData, Controller, Channel, Rank, Byte, RxDqsNDelay, WriteToCache, &GetSetValue);
          }
        }
      }
    }
  }
  MrcFlushRegisterCachedData (MrcData);

  //
  // Restore DataControl0, and RxTrainingMode
  // Restore the bank mapping for DDR4
  //
  for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
    for (Channel = 0; Channel < MaxChannel; Channel++) {
      if (!MrcChannelExist (MrcData, Controller, Channel)) {
        continue;
      }
      if (Ddr4) {
        MrcGetSetBankSequence (MrcData, Controller, Channel, &BankMapping[Controller][Channel][0], 2, MRC_SET);
      }
    } // for Channel
  } // for Controller

  // Clean up after Test.
  if (Lpddr) {
    for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
      if (MrcControllerExist (MrcData, Controller)) {
        Offset = OFFSET_CALC_CH (MC0_CR_CPGC2_V_CHICKEN_REG, MC1_CR_CPGC2_V_CHICKEN_REG, Controller);
        MrcWriteCR (MrcData, Offset, CpgcChickenPrevData[Controller]);
        MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Revert MPR_TEST_REQ_DBLR.\n");
        GetSetVal = 0;
        MrcGetSetMc (MrcData, Controller, GsmMccHalfCachelineMode, WriteToCache | PrintValue, &GetSetVal);
        for (Channel = 0; Channel < MAX_CHANNEL; Channel++) {
          if ((!MrcChannelExist (MrcData, Controller, Channel)) || IS_MC_SUB_CH (Lpddr, Channel)) {
            continue;
          }
          GetSetVal = RDRDsgPrevData;
          MrcGetSetMcCh (MrcData, Controller, Channel, GsmMctRDRDsg, WriteToCache | PrintValue, &GetSetVal);
          GetSetVal = RDRDdgPrevData;
          MrcGetSetMcCh (MrcData, Controller, Channel, GsmMctRDRDdg, WriteToCache | PrintValue, &GetSetVal);
          MrcFlushRegisterCachedData (MrcData);
          // Must update the XARB bubble injector when TAT values change
          SetTcBubbleInjector (MrcData, Controller, Channel);
        }
      }
    }
  }
  MrcGetSetChStrb (MrcData, MAX_CONTROLLER, MAX_CHANNEL, MAX_SDRAM_IN_DIMM, GsmIocRankOverrideEn, WriteNoCache, &GetSetDis);
  MrcFlushRegisterCachedData (MrcData);
  Status  = ChangeMargin (MrcData, RdT, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0);
  Status  = IoReset (MrcData);

  if ((MprStatus != mrcSuccess) && (Inputs->ExitOnFailure == 1)) {
    Status = MprStatus;
  }

  return Status;
}
#endif

/**
  Perform Read Timing Centering.
  Center Rx DQS-DQ using moderate pattern with 1D eye

  @param[in] MrcData         - Include all MRC global data.

  @retval MrcStatus       - if it succeded returns mrcSuccess
**/
MrcStatus
MrcReadTimingCentering (
  IN     MrcParameters *const MrcData
  )
{
  UINT8     ResetPerBit;
  UINT8     LoopCount;
  MrcStatus Status;

  ResetPerBit = 0;

  LoopCount   = 10;

  Status =  DQTimeCentering1D (MrcData, RdT, ResetPerBit, LoopCount, TRUE, FALSE, 1);

  return Status;
}

/**
  Perform Read Timing Centering.
  Center Rx DQS-DQ using moderate pattern with 1D eye

  @param[in] MrcData         - Include all MRC global data.

  @retval MrcStatus       - if it succeded returns mrcSuccess
**/
MrcStatus
MrcWeakReadTimingCentering (
  IN     MrcParameters *const MrcData
  )
{
  UINT8     ResetPerBit;
  UINT8     LoopCount;
  MrcStatus Status;

  ResetPerBit = 0;

  LoopCount   = 8;

  Status =  DQTimeCentering1D (MrcData, RdT, ResetPerBit, LoopCount, TRUE, FALSE, 1);

  return Status;
}

/**
  Perform Read Timing Centering with JR, center according to worst case results.
  Center Rx DQS-DQ using moderate pattern with 2D eye

  @param[in] MrcData      - Include all MRC global data.

  @retval MrcStatus       - if it succeded returns mrcSuccess
**/
MrcStatus
MrcReadTimingCenteringJR (
  IN     MrcParameters *const MrcData
  )
{
  MrcDebug            *Debug;
  MrcOutput           *Outputs;
  UINT8               LoopCount;
  UINT8               Iteration;
  UINT16              RxDqsPNMin[MAX_CONTROLLER][MAX_CHANNEL][MAX_RANK_IN_CHANNEL][MAX_BYTE_IN_DDR5_CHANNEL][2]; // the last one for N/P
  UINT8               Controller;
  UINT8               Channel;
  UINT8               Rank;
  UINT8               Byte;
  INT64               GetSetVal;

  Outputs = &MrcData->Outputs;
  Debug   = &Outputs->Debug;

  LoopCount = 17;

  if (Outputs->Any2Dpc) {
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Skip MrcReadTimingCenteringJR in 2DPC configuration\n");
    return mrcSuccess;
  }
  // Get initial values
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Initial RxDqsP/N Values: RxDqsP\tRxDqsN\n");
  for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
    for (Channel = 0; Channel < MAX_CHANNEL; Channel++) {
      for (Rank = 0; Rank < MAX_RANK_IN_CHANNEL; Rank++) {
        if (!MrcRankExist (MrcData, Controller, Channel, Rank)) {
          continue;
        }
        for (Byte = 0; Byte < Outputs->SdramCount; Byte++) {
          MrcGetSetStrobe (MrcData, Controller, Channel, Rank, Byte, RxDqsPDelay, ReadCached, &GetSetVal);
          RxDqsPNMin[Controller][Channel][Rank][Byte][DqsP] = (UINT16) GetSetVal;
          MrcGetSetStrobe (MrcData, Controller, Channel, Rank, Byte, RxDqsNDelay, ReadCached, &GetSetVal);
          RxDqsPNMin[Controller][Channel][Rank][Byte][DqsN] = (UINT16) GetSetVal;
          MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "MC%dC%dR%dB%d:\t%3d \t%3d\n", Controller, Channel, Rank, Byte, RxDqsPNMin[Controller][Channel][Rank][Byte][DqsP], RxDqsPNMin[Controller][Channel][Rank][Byte][DqsN]);
        } // Byte
      } // Rank
    } // Channel
  } // Controller

  for (Iteration = 0; Iteration < IterationMAX; Iteration++) {
    // Center RdT
    //DQTimeCentering1D(MrcData, RdT, ResetPerBit, LoopCount, FALSE, FALSE, 1);
    DataTimeCentering2D (
      MrcData,
      Outputs->MarginResult,
      Outputs->McChBitMask,
      RdT,
      0,
      1,
      0,
      LoopCount,
      1
      );
    // Store results
    for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
      for (Channel = 0; Channel < MAX_CHANNEL; Channel++) {
        for (Rank = 0; Rank < MAX_RANK_IN_CHANNEL; Rank++) {
          if (!MrcRankExist (MrcData, Controller, Channel, Rank)) {
            continue;
          }
          for (Byte = 0; Byte < Outputs->SdramCount; Byte++) {
            MrcGetSetStrobe (MrcData, Controller, Channel, Rank, Byte, RxDqsPDelay, ReadCached, &GetSetVal);
            if (RxDqsPNMin[Controller][Channel][Rank][Byte][DqsP] > (UINT16) GetSetVal) {
              RxDqsPNMin[Controller][Channel][Rank][Byte][DqsP] = (UINT16) GetSetVal;
            }
            MrcGetSetStrobe (MrcData, Controller, Channel, Rank, Byte, RxDqsNDelay, ReadCached, &GetSetVal);
            if (RxDqsPNMin[Controller][Channel][Rank][Byte][DqsN] > (UINT16) GetSetVal) {
              RxDqsPNMin[Controller][Channel][Rank][Byte][DqsN] = (UINT16) GetSetVal;
            }
          } // Byte
        } // Rank
      } // Channel
    } // Controller

    // jedec reset
    MrcResetSequence (MrcData);
  } // Iteration

  // Set wc center results
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Final RxDqsP/N Values: RxDqsP\tRxDqsN\n");
  for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
    for (Channel = 0; Channel < MAX_CHANNEL; Channel++) {
      for (Rank = 0; Rank < MAX_RANK_IN_CHANNEL; Rank++) {
        if (!MrcRankExist (MrcData, Controller, Channel, Rank)) {
          continue;
        }
        for (Byte = 0; Byte < Outputs->SdramCount; Byte++) {

          GetSetVal = (INT64) RxDqsPNMin[Controller][Channel][Rank][Byte][DqsP];
          MrcGetSetStrobe (MrcData, Controller, Channel, Rank, Byte, RxDqsPDelay, WriteToCache, &GetSetVal);

          GetSetVal = (INT64) RxDqsPNMin[Controller][Channel][Rank][Byte][DqsN];
          MrcGetSetStrobe (MrcData, Controller, Channel, Rank, Byte, RxDqsNDelay, WriteToCache, &GetSetVal);
          MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "MC%dC%dR%dB%d:\t%3d \t%3d\n", Controller, Channel, Rank, Byte, RxDqsPNMin[Controller][Channel][Rank][Byte][DqsP], RxDqsPNMin[Controller][Channel][Rank][Byte][DqsN]);
        } // Byte
      } // Rank
    } // Channel
  } // Controller
  MrcFlushRegisterCachedData (MrcData);

  return mrcSuccess;
}

/**
  This procedure is meant to find the calibrated step size for Per Bit DeSkew.

  @param[in] MrcData         - Include all MRC global data.

  @retval MrcStatus       - if it succeded returns mrcSuccess
**/
MrcStatus
MrcPerBitDeskewCalibration (
  IN     MrcParameters *const MrcData
  )
{
  //MrcInput  *Inputs;
  MrcOutput *Outputs;
  MrcStatus Status;
//  INT64     GetSetVal;
  UINT8     LoopCount;

  Outputs = &MrcData->Outputs;
  //Inputs  = &MrcData->Inputs;

  LoopCount   = 10;

  // If Read Per Bit Deskew is requested to be disabled, enable it temporarily to do calibration.
  //if (Inputs->RdTPbdDis) {
    //@todo: To disable Deskew set RxDqPerBitDeskew/RxDqPerBitDeskewOffset to 0
  //}

  Status = GetPerBitDeSkewStep (
            MrcData,
            Outputs->MarginResult,
            Outputs->ValidChBitMask,
            RdT,
            LoopCount
            );

  // Disable before continuing with training
  //if (Inputs->RdTPbdDis) {
    //@todo: To disable Deskew set RxDqPerBitDeskew/RxDqPerBitDeskewOffset to 0
  //}

  return Status;
}

/**
  This function executes the read Voltage centering.
  Center Rx DQ Vref using moderate pattern with 1D eye.

  @param[in] MrcData         - Include all MRC global data.

  @retval MrcStatus      - if it succeded returns mrcSuccess
**/
MrcStatus
MrcReadVoltageCentering (
  IN     MrcParameters *const MrcData
  )
{
  UINT8     LoopCount;

  LoopCount   = 10;

  return DQTimeCentering1D (MrcData, RdV, 0, LoopCount, MRC_PRINTS_ON, FALSE, 1);
}

/**
  Perform Read Timing Centering in 2D.
  Final read timing centering using 2D algorithm and per bit optimization

  @param[in] MrcData         - Include all MRC global data.

  @retval MrcStatus       - if it succeded returns mrcSuccess
**/
MrcStatus
MrcReadTimingCentering2D (
  IN     MrcParameters *const MrcData
  )
{
  const MrcInput  *Inputs;
  MrcDebug        *Debug;
  MrcOutput       *Outputs;
  MrcStatus       Status;
  UINT8           EnPerBit;
  UINT8           EnRxDutyCycle;
  UINT8           ResetPerBit;
  UINT8           LoopCount;
  UINT8           En2D;

  Inputs  = &MrcData->Inputs;
  Outputs = &MrcData->Outputs;
  Debug   = &Outputs->Debug;
  Status  = mrcSuccess;
  LoopCount     = 15;

  if (!Inputs->RdTPbdDis) {
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Running PerBit RdTP/RdTN with LC = %u\n", LoopCount);
    Status = PerBit1DCentering (MrcData, RdTP, LoopCount, 0, MRC_PRINTS_ON, NULL);
    Status = PerBit1DCentering (MrcData, RdTN, LoopCount, 0, MRC_PRINTS_ON, NULL);
  }

  if (mrcSuccess == Status) {
    EnPerBit      = 0;
    EnRxDutyCycle = 1;
    ResetPerBit   = 0;
    En2D          = 1;
    MRC_DEBUG_MSG (
      Debug,
      MSG_LEVEL_NOTE,
      "\nCalling with EnRxDutyCycle = %d, EnPerBit = %d, ResetPerBit = %d En2D = %d\n",
      EnRxDutyCycle,
      EnPerBit,
      ResetPerBit,
      En2D
    );

    Status = DataTimeCentering2D (
      MrcData,
      Outputs->MarginResult,
      Outputs->McChBitMask,
      RdT,
      EnPerBit,
      EnRxDutyCycle,
      ResetPerBit,
      LoopCount,
      En2D
    );
  }

  return Status;
}

/**
  Perform Read Timing Centering after DCA training.
  Final read timing centering using 1D algorithm and per bit optimization

  @param[in] MrcData         - Include all MRC global data.

  @retval MrcStatus       - if it succeded returns mrcSuccess
**/
MrcStatus
MrcReadTimingCenteringAfterDca (
  IN     MrcParameters *const MrcData
  )
{
  const MrcInput  *Inputs;
  MrcDebug        *Debug;
  MrcOutput       *Outputs;
  MrcStatus       Status;
  UINT8           EnPerBit;
  UINT8           EnRxDutyCycle;
  UINT8           ResetPerBit;
  UINT8           LoopCount;
  UINT8           En2D;

  Inputs  = &MrcData->Inputs;
  Outputs = &MrcData->Outputs;
  Debug   = &Outputs->Debug;
  Status  = mrcSuccess;
  LoopCount     = 15;

  if (!Inputs->RdTPbdDis) {
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Running PerBit RdTP/RdTN with LC = %u\n", LoopCount);
    Status = PerBit1DCentering (MrcData, RdTP, LoopCount, 0, MRC_PRINTS_ON, NULL);
    Status = PerBit1DCentering (MrcData, RdTN, LoopCount, 0, MRC_PRINTS_ON, NULL);
  }

  if (mrcSuccess == Status) {
    EnPerBit      = 0;
    EnRxDutyCycle = 1;
    ResetPerBit   = 0;
    En2D          = 0;
    MRC_DEBUG_MSG (
      Debug,
      MSG_LEVEL_NOTE,
      "\nCalling with EnRxDutyCycle = %d, EnPerBit = %d, ResetPerBit = %d En2D = %d\n",
      EnRxDutyCycle,
      EnPerBit,
      ResetPerBit,
      En2D
    );

    Status = DataTimeCentering2D (
      MrcData,
      Outputs->MarginResult,
      Outputs->McChBitMask,
      RdT,
      EnPerBit,
      EnRxDutyCycle,
      ResetPerBit,
      LoopCount,
      En2D
    );
  }

  return Status;
}

#ifdef MRC_DEBUG_PRINT
/**
  Print current RxVref values for all channels/bytes.

  @param[in,out] MrcData     - Include all MRC global data.
  @param[in]     McChBitMask - Channel bit mask.
**/
VOID
MrcPrintRdVoltagePerByte (
  IN OUT MrcParameters *const MrcData,
  IN     UINT8                McChBitMask
  )
{
  MrcOutput     *Outputs;
  MrcDebug      *Debug;
  UINT32        Controller;
  UINT32        Channel;
  UINT32        Byte;
  INT64         GetSetVal;

  Outputs = &MrcData->Outputs;
  Debug   = &Outputs->Debug;

  for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\t");
    for (Channel = 0; Channel < Outputs->MaxChannels; Channel++) {
      if (!(MC_CH_MASK_CHECK (McChBitMask, Controller, Channel, Outputs->MaxChannels))) {
        continue;
      }
      for (Byte = 0; Byte < Outputs->SdramCount; Byte++) {
        MrcGetSetChStrb (MrcData, Controller, Channel, Byte, RxVref, ReadFromCache, &GetSetVal);
        MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "%d\t", (UINT32) GetSetVal);
      }
    }
  }
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\n");
}
#endif // MRC_DEBUG_PRINT

/**
  Perform Read Voltage Centering in 2D.
  Note: This function currently only supports param = RdV

  @param[in,out] MrcData     - Include all MRC global data.
  @param[in,out] MarginByte  - Pointer to Marging Results data structure
  @param[in]     McChBitMask - MC Channel BIT mask for MC Channel(s) to work on
  @param[in]     Param       - {0:RcvEn, 1:RdT, 2:WrT, 3: WrDqsT, 4:RdV, 5:WrV, 6:WrLevel,
                                8:WrTBit, 9:RdTBit, 10:RdVBit,
                                16:RdFan2, 17:WrFan2, 32:RdFan3, 32:WrFan3}
                                ONLY RdV is allowed in this function
  @param[in]     EnPerBit    - Option to enable per bit margining
  @param[in]     ResetPerBit - Option to Reset PerBit Deskew to middle value before byte training
  @param[in]     LoopCount   - Loop count
  @param[in]     En2D        - Option to only run center at nominal Vref point

  @retval MrcStatus - If it succeded return mrcSuccess
**/
MrcStatus
ReadVoltageCentering2D (
  IN OUT MrcParameters *const MrcData,
  IN OUT UINT16               MarginByte[MAX_RESULT_TYPE][MAX_RANK_IN_CHANNEL][MAX_CONTROLLER][MAX_CHANNEL][MAX_SDRAM_IN_DIMM][MAX_EDGES],
  IN     UINT8                McChBitMask,
  IN     UINT8                Param,
  IN     UINT8                EnPerBit,
  IN     UINT8                ResetPerBit,
  IN     UINT8                LoopCount,
  IN     UINT8                En2D
  )
{
  static const INT8   TimePoints[] = { 0, -8, 8 };
  static const UINT8  EHWeights[sizeof (TimePoints)] = { 1, 1, 1 };
  const MrcInput      *Inputs;
  MrcDebug            *Debug;
  const MRC_FUNCTION  *MrcCall;
  MrcOutput           *Outputs;
  MrcChannelOut       *ChannelOut;
  MrcStatus           Status;
  UINT8               ResultType;
  UINT8               Controller;
  UINT8               Channel;
  UINT8               Byte;
  UINT8               Rank;
#ifdef MRC_DEBUG_PRINT
  UINT8               Edge;
#endif
  UINT8               RankResult;
  UINT8               RankMask;
  UINT8               bit;
  UINT8               lcloop;
  UINT8               tim;
  UINT8               paramB;
  UINT8               paramT;
  UINT8               BMap[MAX_SDRAM_IN_DIMM];
  INT8                SumEH;
  INT8                SumEHSign;
  UINT8               MaxTscale;
  UINT8               SaveLC;
  UINT16              mode;
  INT16               center[MAX_CONTROLLER][MAX_CHANNEL][MAX_RANK_IN_CHANNEL][MAX_SDRAM_IN_DIMM];
  INT32               value0[MAX_CONTROLLER][MAX_CHANNEL][MAX_SDRAM_IN_DIMM];
  UINT32              BERStats[4];
  UINT32              TimScale[MAX_CONTROLLER][MAX_CHANNEL][MAX_SDRAM_IN_DIMM];
  INT16               CenterSumByte[MAX_CONTROLLER][MAX_CHANNEL][MAX_SDRAM_IN_DIMM];
  INT32               CenterSumBit[MAX_CONTROLLER][MAX_CHANNEL][MAX_RANK_IN_CHANNEL][MAX_SDRAM_IN_DIMM][MAX_BITS];
  UINT16              marginbit[MAX_CONTROLLER][MAX_CHANNEL][MAX_SDRAM_IN_DIMM][MAX_BITS][MAX_EDGES];
  UINT16              EyeShape[3][MAX_CONTROLLER][MAX_CHANNEL][MAX_SDRAM_IN_DIMM][MAX_EDGES];
  UINT32              RxOffsetVdq;
  INT64               GetSetVal;
  UINT8               MaxVrefOffset;
  UINT8               MaxChannel;

  Inputs    = &MrcData->Inputs;
  MrcCall   = Inputs->Call.Func;
  Outputs   = &MrcData->Outputs;
  Debug     = &Outputs->Debug;
  MaxChannel = Outputs->MaxChannels;
  Status    = mrcSuccess;
  SumEH     = 0;
  MaxTscale = 12;
  RankResult = 0;
  MrcCall->MrcSetMem ((UINT8 *) BERStats, sizeof (BERStats), 0);
  for (lcloop = 0; lcloop < (sizeof (BMap) / sizeof (BMap[0])); lcloop++) {
    BMap[lcloop] = lcloop;
  }
  //
  // Assume rank0 is always popuplated
  //
  if (Param == RdV) {
    paramB = RdVBit;
    paramT = RdT;
  } else {
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_ERROR, "Error Handler: Unknown Margin Parameter\n");
    Status = mrcReadVoltage2DError;
    return Status;
  }

  SetupIOTestBasicVA (MrcData, Outputs->McChBitMask, LoopCount, ABGSOE, 0, 0, 8, PatWrRd, 0, 0);
  //
  // Calculate SumEH for use in weighting equations
  //
  for (tim = 0; tim < sizeof (TimePoints); tim++) {
    SumEH += EHWeights[tim];

    //
    // Loop once at nominal Vref point
    //
    if (En2D == 0) {
      tim = sizeof (TimePoints);
    }
  }
  //
  // SumEH is used as divisor, make sure is never 0
  //
  if (SumEH == 0) {
    SumEH = 1;
  }

  for (Rank = 0; Rank < MAX_RANK_IN_CHANNEL; Rank++) {
    if (EnPerBit == 1) {
      // For Per bit run centering per Rank to get
      RankMask  = 1 << Rank;
    } else {
      RankMask = 0xF;
    }
    //
    // Reset PerBit Deskew to middle value before Byte training
    // Amplifier voltage offset for bit[x] of the DQ Byte.
    // Matched = {0: Most negative offset,... 16: 0 offset, ... 31: Most postive offset}.
    //
    if (ResetPerBit == 1) {
      // Matched = {0: Most negative offset,... 16: 0 offset, ... 31: Most postive offset}.
      // EnMultiCast=1, 0,0,0,0, UpdateHost=1, SkipWait=0
      Status = ChangeMargin (MrcData, paramB, 0xFFFFFFFF, 0, 1, 0, 0, Rank, 0, 0, 1, 0);
    }
    //
    // Select rank for REUT test
    //
    McChBitMask = 0;
    for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
      for (Channel = 0; Channel < MaxChannel; Channel++) {
        McChBitMask |= SelectReutRanks(MrcData, Controller, Channel, RankMask, FALSE, 0);
      }
    } // Controller

    //
    // Continue with next rank if this rank is not present on any channel
    //
    if (!(McChBitMask)) {
      continue;
    }
    //
    // ####################################################
    // ################  Initialize EW/EH variables  ######
    // ####################################################
    //
    Status      = GetMarginByte (MrcData, Outputs->MarginResult, paramT, 0, 0xF);
    ResultType  = GetMarginResultType (paramT);

#ifdef MRC_DEBUG_PRINT
    if (En2D) {
      MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\n### Measure Eye Height, per BYTE, at %s RankMask = 0x%X\n", "ALL (2D) Timing Points", RankMask);
    } else {
      MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\n### Measure Eye Height, per BYTE, at %s RankMask = 0x%X\n", "NOMINAL Timing", RankMask);
    }

    for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
      MRC_DEBUG_MSG(Debug, MSG_LEVEL_NOTE, "\t\t");
      for (Channel = 0; Channel < MaxChannel; Channel++) {
        if (!MC_CH_MASK_CHECK (McChBitMask, Controller, Channel, MaxChannel)) {
          continue;
        }
        MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "MC%u.Ch%u", Controller, Channel);
        if (Channel == 0) {
          for (Byte = 0; Byte < Outputs->SdramCount - 1; Byte++) {
            MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\t");
          }
        }
      }
    } // Controller

    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\nByte\t");
    for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
      MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\t");
      for (Channel = 0; Channel < MaxChannel; Channel++) {
        if (!MC_CH_MASK_CHECK (McChBitMask, Controller, Channel, MaxChannel)) {
          continue;
        }
        for (Byte = 0; Byte < Outputs->SdramCount; Byte++) {
          MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "%d\t", Byte);
        }
      }
    } // Controller

    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\nTScale\t");
#endif // MRC_DEBUG_PRINT
      //
      // Update TimScale to have the appropriate eye width (read from last saved parameters)
      //
    for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
      MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\t");
      for (Channel = 0; Channel < MaxChannel; Channel++) {
        if (!MC_CH_MASK_CHECK (McChBitMask, Controller, Channel, MaxChannel)) {
          continue;
        }

        ChannelOut = &Outputs->Controller[Controller].Channel[Channel];

        for (Byte = 0; Byte < Outputs->SdramCount; Byte++) {
          if (En2D > 0) {
            TimScale[Controller][Channel][Byte] =
               (
                MarginByte[ResultType][0][Controller][Channel][Byte][0] +
                MarginByte[ResultType][0][Controller][Channel][Byte][1]
                ) /
               20;
          } else {
            TimScale[Controller][Channel][Byte] = 1;
          }
          //
          // It is possible sumT is 0.
          //
          if (!(TimScale[Controller][Channel][Byte]) || (TimScale[Controller][Channel][Byte] > MaxTscale)) {
            TimScale[Controller][Channel][Byte] = MaxTscale;
          }

          MRC_DEBUG_MSG(Debug, MSG_LEVEL_NOTE, "%d\t", TimScale[Controller][Channel][Byte]);
        } // Byte
      } // Channel
    } // Controller

#ifdef MRC_DEBUG_PRINT
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\nInitial RdVref");
    MrcPrintRdVoltagePerByte (MrcData, McChBitMask);
#endif // MRC_DEBUG_PRINT

    Status      = GetMarginByte (MrcData, Outputs->MarginResult, Param, RankResult, 0xF);
    ResultType  = GetMarginResultType (Param);

    //
    // ####################################################
    // ######   Measure Eye Height at all Timing Points  #####
    // ####################################################
    //
    //
    // Loop through all the Time Points to Test
    //
    for (tim = 0; tim < sizeof (TimePoints); tim++) {
      MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\n\nRdTime\t");

      //
      // Setup Timing Offset for this point
      //
      for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
        MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\t");
        for (Channel = 0; Channel < MaxChannel; Channel++) {
          if (!MC_CH_MASK_CHECK (McChBitMask, Controller, Channel, MaxChannel)) {
            continue;
          }

          for (Byte = 0; Byte < Outputs->SdramCount; Byte++) {
            value0[Controller][Channel][Byte] = (INT32)(TimePoints[tim] * TimScale[Controller][Channel][Byte]) / MaxTscale;
            Status = ChangeMargin (MrcData, paramT, value0[Controller][Channel][Byte], 0, 0, Controller, Channel, Rank, Byte, 0, 1, 0);
            MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "%d\t", value0[Controller][Channel][Byte]);
          }
        }
      } // Controller

      //
      // Run Margin Test
      //
      mode = 0;
      MaxVrefOffset = GetVrefOffsetLimits (MrcData, Param);
      Status = MrcGetBERMarginByte (
                MrcData,
                Outputs->MarginResult,
                McChBitMask,
                RankMask,
                RankMask,
                Param,
                mode,
                BMap,
                1,
                MaxVrefOffset,
                0,
                BERStats
                );
      RankResult = GetRankToStoreResults(MrcData, RankMask);
#ifdef MRC_DEBUG_PRINT
      for (Edge = 0; Edge < MAX_EDGES; Edge++) {
        MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, (Edge) ? "\nHigh\t" : "\nLo\t");
        for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
          MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\t");
          for (Channel = 0; Channel < MaxChannel; Channel++) {
            if (!MC_CH_MASK_CHECK (McChBitMask, Controller, Channel, MaxChannel)) {
              continue;
            }
            for (Byte = 0; Byte < Outputs->SdramCount; Byte++) {
              MRC_DEBUG_MSG(
                 Debug,
                 MSG_LEVEL_NOTE,
                 "%d\t",
                 MarginByte[ResultType][RankResult][Controller][Channel][Byte][Edge]
                 );
            } // Byte
          } // Channel
        } // Controller
      } // Edge

      MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\nCenter\t");
#endif // MRC_DEBUG_PRINT
        //
        // Store Results
        //
      for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
        MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\t");
        for (Channel = 0; Channel < MaxChannel; Channel++) {
          if (MC_CH_MASK_CHECK (McChBitMask, Controller, Channel, MaxChannel)) {

            for (Byte = 0; Byte < Outputs->SdramCount; Byte++) {
              center[Controller][Channel][RankResult][Byte] = (INT32) (MarginByte[ResultType][RankResult][Controller][Channel][Byte][1] -
                                                                       MarginByte[ResultType][RankResult][Controller][Channel][Byte][0]);
              if (tim == 0) {
                CenterSumByte[Controller][Channel][Byte] = 0;
              }
              //
              // Calculate weight for this point
              //
              CenterSumByte[Controller][Channel][Byte] += EHWeights[tim] * center[Controller][Channel][RankResult][Byte];

              MRC_DEBUG_MSG(Debug, MSG_LEVEL_NOTE, "%d\t", center[Controller][Channel][RankResult][Byte] / 20);

              //
              // Record edges for use in per bit margining
              //
              EyeShape[tim][Controller][Channel][Byte][0] = MarginByte[ResultType][RankResult][Controller][Channel][Byte][0];
              EyeShape[tim][Controller][Channel][Byte][1] = MarginByte[ResultType][RankResult][Controller][Channel][Byte][1];
            } // Byte
          } // MC_CH_MASK_CHECK
        } // Channel
      } // Controller

      //
      // Loop once at nominal Vref point
      //
      if (En2D == 0) {
        tim = sizeof (TimePoints);
      }
    } // for tim

    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\n\nWtdCntr\t");
    //
    // ####################################################
    // ###########   Center Results per Byte   ############
    // ####################################################
    //
    for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
      MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\t");
      for (Channel = 0; Channel < MaxChannel; Channel++) {
        if (!MC_CH_MASK_CHECK (McChBitMask, Controller, Channel, MaxChannel)) {
          continue;
        }

        ChannelOut = &Outputs->Controller[Controller].Channel[Channel];

        //
        // Calculate CenterPoint.  Round to Nearest Int
        //
        for (Byte = 0; Byte < Outputs->SdramCount; Byte++) {
          SumEHSign = (CenterSumByte[Controller][Channel][Byte] < 0) ? (-1) : 1;

          CenterSumByte[Controller][Channel][Byte] = (CenterSumByte[Controller][Channel][Byte] + 10 * (SumEHSign * SumEH)) / (20 * SumEH);
          MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE,"%d\t", CenterSumByte[Controller][Channel][Byte]);

          //
          // Apply new center point
          //
          GetSetVal = CenterSumByte[Controller][Channel][Byte];
          MrcGetSetChStrb (MrcData, Controller, Channel, Byte, RxVref, WriteOffsetCached, &GetSetVal);

          //
          // Update the Eye Edges
          //
          for (tim = 0; tim < sizeof(TimePoints); tim++) {
            EyeShape[tim][Controller][Channel][Byte][0] = EyeShape[tim][Controller][Channel][Byte][0] + (10 * CenterSumByte[Controller][Channel][Byte]);
            EyeShape[tim][Controller][Channel][Byte][1] = EyeShape[tim][Controller][Channel][Byte][1] - (10 * CenterSumByte[Controller][Channel][Byte]);

            //
            // Loop once at nominal Vref point
            //
            if (En2D == 0) {
              tim = sizeof(TimePoints);
            }
          }
          //
          // Update MrcData for future tests (MarginResult points back to MrcData)
          // EyeShape for Vref 0 is assumed to have the best shape for future tests.
          //
          MarginByte[ResultType][RankResult][Controller][Channel][Byte][0] = (UINT16)EyeShape[0][Controller][Channel][Byte][0];
          MarginByte[ResultType][RankResult][Controller][Channel][Byte][1] = (UINT16)EyeShape[0][Controller][Channel][Byte][1];
        } // for Byte
      } // for Channel
    } // Controller


#ifdef MRC_DEBUG_PRINT
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\nRdVref\t");
    MrcPrintRdVoltagePerByte (MrcData, McChBitMask);
#endif // MRC_DEBUG_PRINT
    //
    // ####################################################
    // ############    Measure Eye Height Per BIT   ########
    // ####################################################
    //
    if (EnPerBit) {
      MRC_DEBUG_MSG (
        Debug,
        MSG_LEVEL_NOTE, (
        En2D
        ) ? "\n\n### Measure Eye Height, per BIT, at ALL (2D) Timing Points\n" :
        "\n\n### Measure Eye Height, per BIT, at NOMINAL Timing\n"
        );

      //
      // Stop on all lane fail
      //
      for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
        for (Channel = 0; Channel < MaxChannel; Channel++) {
          if (MC_CH_MASK_CHECK (McChBitMask, Controller, Channel, MaxChannel)) {
#ifdef MRC_DEBUG_PRINT
            MRC_DEBUG_MSG(Debug, MSG_LEVEL_NOTE, "Mc%u.Ch%u\t\t\t\t\t",Controller, Channel);
            for (Byte = 0; Byte < Outputs->SdramCount - 1; Byte++) {
              MRC_DEBUG_MSG(Debug, MSG_LEVEL_NOTE, "\t\t\t\t\t\t\t");
            }
#endif // MRC_DEBUG_PRINT
            // We enable error checking on all Cachelines and UI's of the Cacheline.
            //MrcSetChunkAndClErrMsk (MrcData, 0xFF, 0xFFFF);
            MrcSetupTestErrCtl (MrcData, ALSOE, 1);
            MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\t");
          } // MC_CH_MASK_CHECK
        } // Channel
      } // Controller


#ifdef MRC_DEBUG_PRINT
      MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\n");
      for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
        for (Channel = 0; Channel < MaxChannel; Channel++) {
          if (MC_CH_MASK_CHECK (McChBitMask, Controller, Channel, MaxChannel)) {
            for (Byte = 0; Byte < Outputs->SdramCount; Byte++) {
              MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Byte % 12d\t\t\t", Byte);
            }
          }
        }
      } // Controller

      MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\n");
#endif // MRC_DEBUG_PRINT
      //
      // Loop through all the Vref Points to Test
      //
      SaveLC = Outputs->DQPatLC;
      for (tim = 0; tim < sizeof (TimePoints); tim++) {
        //
        // Setup Timing Offset for this point
        //
        for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
          for (Channel = 0; Channel < MaxChannel; Channel++) {
            if (MC_CH_MASK_CHECK (McChBitMask, Controller, Channel, MaxChannel)) {

              for (Byte = 0; Byte < Outputs->SdramCount; Byte++) {
                value0[Controller][Channel][Byte] = (INT32)(TimePoints[tim] * TimScale[Controller][Channel][Byte]) / MaxTscale;
                Status = ChangeMargin(MrcData, paramT, value0[Controller][Channel][Byte], 0, 0, Controller, Channel, Rank, Byte, 0, 1, 0);

                //
                // Amplifier voltage offset for bit[x] of the DQ Byte.
                // Matched = {0: Most negative offset,... 16: 0 offset, ... 31: Most postive offset}.
                //
                for (bit = 0; bit < MAX_BITS; bit++) {
                  marginbit[Controller][Channel][Byte][bit][0] = marginbit[Controller][Channel][Byte][bit][1] = 16;
                }
              }
            }
          }
        } // Controller


        MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Rank %d \n", Rank);
        //
        // Run Margin Test
        // Loop through 2 times.  Once at low loop count and Once at high loopcount
        // Improves runtime
        // @TODO: Need 2 loops below if not using BASICVA
        //
        for (lcloop = 0; lcloop < 1; lcloop++) {
          Outputs->DQPatLC  = (lcloop == 0) ? 1 : SaveLC;
          mode              = 0;
          Status            = MrcGetMarginBit (MrcData, McChBitMask, Rank, marginbit, EyeShape[tim], paramB, mode, 15, MRC_PRINTS_ON);
        }
        //
        // Store Results
        //
        for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
          for (Channel = 0; Channel < MaxChannel; Channel++) {
            if (MC_CH_MASK_CHECK (McChBitMask, Controller, Channel, MaxChannel)) {

              for (Byte = 0; Byte < Outputs->SdramCount; Byte++) {
                for (bit = 0; bit < MAX_BITS; bit++) {
                  center[Controller][Channel][RankResult][Byte] = ((marginbit[Controller][Channel][Byte][bit][1] - 16) - (16 - marginbit[Controller][Channel][Byte][bit][0]));
                  if (tim == 0) {
                    CenterSumBit[Controller][Channel][RankResult][Byte][bit] = 0;
                  }
                  //
                  // Calculate weight for this point
                  //
                  CenterSumBit[Controller][Channel][RankResult][Byte][bit] += EHWeights[tim] * center[Controller][Channel][RankResult][Byte];
                } // Bit
              } // Byte
            } // MC_CH_MASK_CHECK
          } // Channel
        } // Controller

        //
        // Loop once at nominal Vref point
        //
        if (En2D == 0) {
          tim = sizeof (TimePoints);
        }
      } // for tim
      //
      // ####################################################
      // ############   Center Result Per BIT  ##############
      // ####################################################
      //
      MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\nWgted Center\n\t\t\t");
      for (Byte = 0; Byte < Outputs->SdramCount; Byte++) {
        MRC_DEBUG_MSG(Debug, MSG_LEVEL_NOTE, "Byte%u", Byte);
        for (bit = 0; bit < MAX_BITS; bit++) {
          MRC_DEBUG_MSG(Debug, MSG_LEVEL_NOTE, "\t");
        }
      }
      MRC_DEBUG_MSG(Debug, MSG_LEVEL_NOTE, "\n");

      for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
        for (Channel = 0; Channel < MaxChannel; Channel++) {
          if (!MC_CH_MASK_CHECK (McChBitMask, Controller, Channel, MaxChannel)) {
            continue;
          }
          MRC_DEBUG_MSG(Debug, MSG_LEVEL_NOTE, "Mc%u.Ch%u\t\t",Controller, Channel);
          ChannelOut = &Outputs->Controller[Controller].Channel[Channel];

          //
          // Calculate and apply CenterPoint.  Round to Nearest Int
          //
          for (Byte = 0; Byte < Outputs->SdramCount; Byte++) {
            for (bit = 0; bit < MAX_BITS; bit++) {
              SumEHSign = (CenterSumBit[Controller][Channel][Rank][Byte][bit] < 0) ? (-1) : 1;

              CenterSumBit[Controller][Channel][Rank][Byte][bit] = (CenterSumBit[Controller][Channel][Rank][Byte][bit] + (SumEHSign * SumEH)) / (2 * SumEH);

              //
              // Centerpoint needs to be added to starting DqPb value
              //
              CenterSumBit[Controller][Channel][Rank][Byte][bit] += (INT32)ChannelOut->RxDqVrefPb[Rank][Byte][bit].Center;

              //
              // Check for saturation
              //
              if (CenterSumBit[Controller][Channel][Rank][Byte][bit] > 15) {
                CenterSumBit[Controller][Channel][Rank][Byte][bit] = 15;
              } else if (CenterSumBit[Controller][Channel][Rank][Byte][bit] < 0) {
                CenterSumBit[Controller][Channel][Rank][Byte][bit] = 0;
              }

              MRC_DEBUG_MSG(Debug, MSG_LEVEL_NOTE, "%x\t", CenterSumBit[Controller][Channel][Rank][Byte][bit]);

              //
              // Update MrcData
              //
              ChannelOut->RxDqVrefPb[Rank][Byte][bit].Center = (UINT8)CenterSumBit[Controller][Channel][Rank][Byte][bit];

              RxOffsetVdq = CenterSumBit[Controller][Channel][Rank][Byte][bit];
              //
              // Apply and propagate new centerpoint
              //
              Status = ChangeMargin (MrcData, RdVBit, RxOffsetVdq, 0, 0, Controller, Channel, Rank, Byte, 0, 1, 0);
            } // for bit
            MRC_DEBUG_MSG(Debug, MSG_LEVEL_NOTE, "\t");
          } // for Byte
          MRC_DEBUG_MSG(Debug, MSG_LEVEL_NOTE, "\n");
        } // for Channel
      } // Controller

    } // if EnPerBit

    if (EnPerBit == 0) { // We use RankMask = 0x0F in this case
      break;
    }
  } // for Rank

/// @attention - This is used to determine if the PerBit routines are correct.  Left for sanity.
#ifdef MRC_DEBUG_PRINT
/*
  for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
    for (Channel = 0; Channel < MaxChannel; Channel++) {
      if (MC_CH_MASK_CHECK (McChBitMask, Controller, Channel, MaxChannel)) {
        ChannelOut = &Outputs->Controller[Controller].Channel[Channel];
        for (Rank = 0; Rank < MaxRank; Rank++) {
          for (Byte = 0; Byte < Outputs->SdramCount; Byte++) {
            Offset = DDRDATA0CH0_CR_RXCONTROL2RANK0_REG +
               ((DDRDATA0CH1_CR_RXCONTROL2RANK0_REG - DDRDATA0CH0_CR_RXCONTROL2RANK0_REG) * Channel) +
               ((DDRDATA1CH0_CR_RXCONTROL2RANK0_REG - DDRDATA0CH0_CR_RXCONTROL2RANK0_REG) * Byte) +
               (DDRDATA0CH0_CR_RXCONTROL2RANK1_REG - DDRDATA0CH0_CR_RXCONTROL2RANK0_REG) * Rank;
            MRC_DEBUG_MSG(Debug, MSG_LEVEL_NOTE, "\nRdVref = % 3d   RdVBit = 0x%08X", (INT8)ChannelOut->RxVref[Byte],
                          MrcReadCR(MrcData, Offset));
          }
        }
      }
    }
  } // Controller
*/
#endif // MRC_DEBUG_PRINT

  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\n");
  //
  // Clean up after test
  //
  Status = ChangeMargin (MrcData, paramT, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0);

  return Status;
}

/**
  Perform Read Voltage Centering in 2D.
  Note: This function currently only supports param = RdV

  @param[in] MrcData       - Include all MRC global data.

  @retval MrcStatus       - if it succeded return mrcSuccess
**/
MrcStatus
MrcReadVoltageCentering2D (
  IN     MrcParameters *const MrcData
  )
{
  MrcOutput *Outputs;
  MrcStatus Status;
  UINT8     EnPerBit;
  UINT8     ResetPerBit;
  UINT8     LoopCount1;
  UINT8     LoopCount2;
  UINT8     En2D;
  BOOLEAN   Ddr4;

  Outputs    = &MrcData->Outputs;
  Ddr4       = (Outputs->DdrType == MRC_DDR_TYPE_DDR4);
  Status     = mrcSuccess;

  LoopCount1  = 10;  // Lower loopcount for the first 2D pass on DDR4 2DPC
  LoopCount2  = 15;

  if (Outputs->RxMode == MrcRxModeMatchedN || Outputs->RxMode == MrcRxModeMatchedP || Outputs->RxMode == MrcRxModeUnmatchedP) {
    Status = PerBit1DCentering (MrcData, RdV, LoopCount2, 0, MRC_PRINTS_ON, NULL);
    IoReset (MrcData);        // The IoReset will also cover the logical domain.
    MrcWait (MrcData, 100);
  }
  if (mrcSuccess == Status) {
    EnPerBit    = 0;
    ResetPerBit = 0;
    En2D        = 1;

    if (Ddr4 && Outputs->Any2Dpc) {
      Status = ReadVoltageCentering2D (
        MrcData,
        Outputs->MarginResult,
        Outputs->ValidChBitMask,
        RdV,
        EnPerBit,
        ResetPerBit,
        LoopCount1,
        En2D
        );
    }

    if (Status == mrcSuccess) {
      Status = ReadVoltageCentering2D (
        MrcData,
        Outputs->MarginResult,
        Outputs->ValidChBitMask,
        RdV,
        EnPerBit,
        ResetPerBit,
        LoopCount2,
        En2D
        );
    }
  }

  return Status;
}

/**
  Perform Early Read Timing 2D centering Training.
  Center read DQ-DQS timing while testing differents RxVref points.

  @param[in]  MrcData   - Include all MRC global data.
  @param[in]  TestType  - 0: Normal Traffic, 1: Read MPR.
  @param[in]  Prints    - Debug prints enable/disable

  @retval MrcStatus - mrcSuccess or reason for failure.
**/
MrcStatus
EarlyReadTimingCentering2D (
  IN  MrcParameters *const  MrcData,
  IN  UINT8                 TestType,
  IN  BOOLEAN               Prints
  )
{
  MrcInput    *Inputs;
  MrcOutput   *Outputs;
  MrcStatus   Status;
  MrcDebug    *Debug;
  MrcDdrType  DdrType;
  MrcSaveData *SaveOutputs;
  INT64       Min;
  INT64       Max;
  UINT32      LowVref;
  UINT32      HighVref;
  INT16       Points2DMin;
  INT16       Points2DMax;
  UINT8       LoopCount;
  UINT8       StepSize;
  BOOLEAN     Ddr5;
  BOOLEAN     Lpddr5;

  Inputs  = &MrcData->Inputs;
  Outputs = &MrcData->Outputs;
  SaveOutputs = &MrcData->Save.Data;
  DdrType = Outputs->DdrType;
  Ddr5    = (DdrType == MRC_DDR_TYPE_DDR5);
  Lpddr5  = (DdrType == MRC_DDR_TYPE_LPDDR5);
  Debug   = (MRC_PRINTS_ON == Prints) ? &Outputs->Debug : NULL;

  if (DdrType == MRC_DDR_TYPE_LPDDR4) {
    if (ERTC2D_MPR == TestType) {
      LowVref  = 70 * 2; // We're currently seeing the RxV eye shifted towards VOH.  No need to go to 0.
      HighVref = 100 * 2; // LP4 Pull-Up Calibrated Voltage for VOH
      StepSize = 2;
    } else {
      LowVref  = 30 * 2; // We're currently seeing the RxV eye shifted towards VOH.  No need to go to 0.
      HighVref = 100 * 2; // LP4 Pull-Up Calibrated Voltage for VOH
      StepSize = 8;
    }
  } else if (DdrType == MRC_DDR_TYPE_LPDDR5){
    if (ERTC2D_MPR == TestType) {
      LowVref  = 30 * 2;
      HighVref = 100 * 2;
      StepSize = 8;
    } else {
      LowVref  = 30 * 2;
      HighVref = 100 * 2;
      StepSize = 8;
    }
  } else {
    LowVref   = Outputs->VddVoltage[Inputs->MemoryProfile];
    HighVref  = LowVref;
    if (Outputs->OdtMode == MrcOdtModeVddq) {
      // Sweep from 1/2 VDDq to VDDq
      LowVref *= 2;
      LowVref   = UDIVIDEROUND (LowVref, 4);
    } else {
      // Sweep from 1/5 of VDDq to 2/3 of VDDq
      HighVref *= 2;
      LowVref   = UDIVIDEROUND (LowVref, 5);
      HighVref  = UDIVIDEROUND (HighVref, 3);
    }
    StepSize = 8;
  }
  StepSize = (Inputs->MemoryProfile != STD_PROFILE) ? 1 : StepSize;

  MrcEncodeRxVref (MrcData, LowVref, &Min);
  MrcEncodeRxVref (MrcData, HighVref, &Max);

  Points2DMin = (INT16) Min;
  Points2DMax = (INT16) Max;

  MrcGetSetLimits (
    MrcData,
    RxVref,
    &Min,
    &Max,
    NULL
    );

  Points2DMin = MAX (Points2DMin, (INT16) Min);

  LoopCount   = 10;
  Points2DMax = MIN (Points2DMax, (INT16) Max);
  Status = EarlyLinearCentering2D (
            MrcData,
            Outputs->McChBitMask,
            Outputs->ValidRankMask,
            RdT,
            RdV,
            Points2DMin,
            Points2DMax,
            StepSize,
            LoopCount,
            TestType,
            Prints
            );

  if ((TestType == ERTC2D_MPR)) {
    if ((Outputs->RxMode == MrcRxModeUnmatchedN) || (Outputs->RxMode == MrcRxModeUnmatchedP)) {
      INT32 PiOffset;
      UINT8 UiLockOrig;
      UINT8 CompOffOrig;
      UINT8 ReadDqsOffsetDdr5Orig;
      UINT32 Offset;
      DATA0CH0_CR_SDLCTL1_STRUCT              DataSdlCtl1;
      UINT32                                  LocalController;
      UINT32                                  Channel;
      UINT32                                  Rank;
      UINT32                                  TransChannel;
      UINT32                                  TransStrobe;
      UINT32                                  Strobe;
      MrcChannelOut                           *ChannelOut;
      UINT16                                  *MrPtr;
      DDR5_MODE_REGISTER_40_TYPE              *Mr40;
      LPDDR5_MODE_REGISTER_10_TYPE            Mr10;
      INT32                                   OffsetGuardband;

       MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Initial FwdClkOffsetTrained: %d, MprLeftEdge: %d\n", Outputs->FwdClkOffsetTrained, SaveOutputs->MprLeftEdge);
      if ((Outputs->FwdClkOffsetTrained == 0) && (SaveOutputs->MprLeftEdge > -31)) {
        OffsetGuardband = 4; // PI ticks
        PiOffset = 31 - OffsetGuardband + SaveOutputs->MprLeftEdge;
        Outputs->FwdClkOffsetTrained = -(PiOffset * Outputs->UIps) / 64; // Convert to [ps]
        MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "PiOffset: %d, FwdClkOffsetTrained: %d\n", PiOffset, Outputs->FwdClkOffsetTrained);
        UiLockOrig = SaveOutputs->UiLock;
        CompOffOrig = SaveOutputs->CompOffset;
        ReadDqsOffsetDdr5Orig = SaveOutputs->ReadDqsOffsetDdr5;
        MrcFlushRegisterCachedData (MrcData);
        InitializeRegisterCache (MrcData);    // Phy init uses a mix of direct CR access and GetSet, so invalidate CR cache at the end

        Status = MrcLockUiCalibration (MrcData);
        if (Status != mrcSuccess) {
          return Status;
        }
        ForceRcomp (MrcData);

        if ((Ddr5 || Lpddr5) && (((SaveOutputs->UiLock - SaveOutputs->CompOffset) != (UiLockOrig - CompOffOrig)) || (SaveOutputs->ReadDqsOffsetDdr5 != ReadDqsOffsetDdr5Orig))) {
          MRC_DEBUG_MSG (Debug, MSG_LEVEL_WARNING, "---- LOCKUI HAS CHANGED ----\n");
          // DDR5 Read Preamble length depends on UiLock, hence need to recalculate the TAT values after Lock UI calibration
          for (LocalController = 0; LocalController < MAX_CONTROLLER; LocalController++) {
            for (Channel = 0; Channel < Outputs->MaxChannels; Channel++) {
              if ((!MrcChannelExist (MrcData, LocalController, Channel))) {
                continue;
              }
              SetTcTurnAround (MrcData, LocalController, Channel);
              for (Strobe = 0; Strobe < Outputs->SdramCount; Strobe++) {
                TransChannel = Channel;
                TransStrobe  = Strobe;
                Rank         = 0; // Rank is not used
                MrcTranslateSystemToIp (MrcData, &LocalController, &TransChannel, &Rank, &TransStrobe, GsmIocTxOn);

                Offset = DATA0CH0_CR_SDLCTL1_REG +
                  (DATA0CH1_CR_SDLCTL1_REG - DATA0CH0_CR_SDLCTL1_REG) * TransChannel +
                  (DATA1CH0_CR_SDLCTL1_REG - DATA0CH0_CR_SDLCTL1_REG) * TransStrobe;
                DataSdlCtl1.Data = MrcReadCR (MrcData, Offset);
                DataSdlCtl1.Bits.rxsdl_cmn_passrcvenondqsfall   = ((SaveOutputs->UiLock - SaveOutputs->CompOffset) + 1 - SaveOutputs->ReadDqsOffsetDdr5 * 2 == 1) ? 0 : 1; // <-- depends on UiLock calibration results
                DataSdlCtl1.Bits.rxsdl_cmn_qualifysdlwithrcven  = !(DataSdlCtl1.Bits.rxsdl_cmn_passrcvenondqsfall);                            // <-- depends on UiLock calibration results
                MrcWriteCR (MrcData, Offset, DataSdlCtl1.Data);
              }
              // Update MR40

              ChannelOut = &Outputs->Controller[LocalController].Channel[Channel];
              for (Rank = 0; Rank < MAX_RANK_IN_CHANNEL; Rank++) {
                if (!MrcRankExist (MrcData, LocalController, Channel, Rank)) {
                  continue;
                }
                if (Ddr5) {
                  MrPtr = ChannelOut->Dimm[Rank / 2].Rank[Rank % 2].MR;
                  Mr40 = (DDR5_MODE_REGISTER_40_TYPE *) &MrPtr[mrIndexMR40];
                  Mr40->Bits.ReadDqsOffsetTiming = SaveOutputs->ReadDqsOffsetDdr5 & 7; // [2:0]
                  MrcIssueMrw (MrcData, LocalController, Channel, Rank, mrMR40, MrPtr[mrIndexMR40], TRUE);
                } else if (Lpddr5) {
                  //MR10 - RDQS Post Amble Mode = 1 (Static), RDQS PST = 0 (0.5tWCK), WCK PST = 0 (2.5tWCK)
                  Mr10.Data8 = (UINT8) (ChannelOut->Dimm[RANK_TO_DIMM_NUMBER(Rank)].Rank[(Rank % MAX_RANK_IN_DIMM)].MR[mrIndexMR10]);
                  Mr10.Bits.RdqsPreLen  = (MrcGetRpre(MrcData) >= 3) ? 3 : 1;
                  // MRC_DEBUG_MSG (Debug, MSG_LEVEL_ERROR, "Mr10 RdqsPrelen is %d\n", Mr10.Bits.RdqsPreLen);
                  Status = MrcIssueMrw (MrcData, LocalController, Channel, Rank, mrMR10, Mr10.Data8, MRC_PRINTS_ON);
                  ChannelOut->Dimm[RANK_TO_DIMM_NUMBER(Rank)].Rank[(Rank % MAX_RANK_IN_DIMM)].MR[mrIndexMR10] = Mr10.Data8;
                  // MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "MC%u Ch%u R%u: Read Dqs Preamble = %d status %d\n", LocalController, Channel, Rank, Mr10.Bits.RdqsPreLen, Status);
                }
              }
            }
          }
          ForceRcomp (MrcData);
          MrcReadLevelingTraining (MrcData);
          MrcRoundTripMatch (MrcData);
        } // if LockUi changed

        // Re-run RDMPR 2D (previously trained RxVref might be not good)
         Status = EarlyLinearCentering2D (
                    MrcData,
                    Outputs->McChBitMask,
                    Outputs->ValidRankMask,
                    RdT,
                    RdV,
                    Points2DMin,
                    Points2DMax,
                    StepSize,
                    LoopCount,
                    TestType,
                    Prints
                    );
      } else {
        MRC_DEBUG_MSG (Debug, MSG_LEVEL_WARNING, "%s MPR left edge is saturated!\n", gWarnString);
      }
    } // Unmatched Rx
  } // RDMPR

  return Status;
}

/**
  Perform Read Timing Linear Centering in different Read Vref points using MPR mode.
  (Does not require Writes to be functional)

  @param[in] MrcData       - Include all MRC global data.

  @retval MrcStatus       - if it success return mrcSuccess
**/
MrcStatus
MrcEarlyReadMprTimingCentering2D (
  IN     MrcParameters *const MrcData
  )
{
  return EarlyReadTimingCentering2D (MrcData, ERTC2D_MPR, MRC_PRINTS_ON);
}

/**
  Perform Early Read Timing 2D centering Training.
  Center read DQ-DQS timing while testing different RxVref points.

  @param[in, out] MrcData   - Include all MRC global data.

  @retval MrcStatus - mrcSuccess or reason for failure.
**/
MrcStatus
MrcEarlyReadTimingCentering2D (
  IN     MrcParameters *const MrcData
  )
{
  return EarlyReadTimingCentering2D (MrcData, ERTC2D_NORMAL, MRC_PRINTS_ON);
}
