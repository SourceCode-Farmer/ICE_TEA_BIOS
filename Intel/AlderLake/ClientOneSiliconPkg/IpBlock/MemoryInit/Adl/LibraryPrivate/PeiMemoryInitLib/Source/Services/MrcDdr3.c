/** @file
  This file includes all the DDR3 specific characteristic functions.

@copyright
  INTEL CONFIDENTIAL
  Copyright 1999 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains an 'Intel Peripheral Driver' and is uniquely identified as
  "Intel Reference Module" and is licensed for Intel CPUs and chipsets under
  the terms of your license agreement with Intel or your vendor. This file may
  be modified by the user, subject to additional terms of the license agreement.

@par Specification Reference:
**/
// Include files
#include "MrcDdr3.h"
#include "MrcDdrCommon.h"
#include "MrcRefreshConfiguration.h"
#include "MrcPowerModes.h"
#include "MrcMemoryScrub.h"
#include "MrcMcOffsets.h"
#include "MrcCpgcApi.h"
#include "MrcMemoryApi.h"

// Module external functions
/**
  This function sends the proper MRS command for specific ranks as indicated by RankMask

  @param[in] MrcData    - include all the MRC data
  @param[in] Controller - Controller to send MRS.
  @param[in] Channel    - Channel to send command to
  @param[in] RankMask   - Rank mask for which command will be sent to
  @param[in] MR         - MRS command to be sent
  @param[in] DimmValue  - Dimm Values to be sent

  @retval MrcStatus
**/
MrcStatus
MrcWriteMRSAll (
  IN MrcParameters *const MrcData,
  IN const UINT32         Controller,
  IN const UINT32         Channel,
  IN       UINT8          RankMask,
  IN const UINT8          MR,
  IN const UINT16 *const  DimmValue
  )
{
  MrcStatus             Status;
  MrcChannelOut         *ChannelOut;
  UINT32                Rank;
  UINT16                MrhData;
  MRC_MRH_ADDRESS_TYPE  MrhAddr;
  MrhAddr.Data16 = 0;

  ChannelOut  = &MrcData->Outputs.Controller[Controller].Channel[Channel];
  RankMask    &= ChannelOut->ValidRankBitMask;
  Status      = mrcSuccess;

  for (Rank = 0; Rank < MAX_RANK_IN_CHANNEL; Rank++) {
    if (((1 << Rank) & RankMask) == 0) {
      continue;
    }
    MrhAddr.Ddr3Ddr4Mrs.MrNum   = MR;
    MrhAddr.Ddr3Ddr4Mrs.MA13To8 = (DimmValue[Rank / 2] >> 8) & 0x3F;
    MrhData = DimmValue[Rank / 2] & 0xFF;
    // MRC_DEBUG_MSG (&MrcData->Outputs.Debug, MSG_LEVEL_ERROR, "MrcWriteMRSAll: Mc %d, Ch %d, Rank %d, MR%d = 0x%04X\n", Controller, Channel, Rank, MR, MrhData);
    Status = MrcRunMrh (MrcData, Controller, Channel, Rank, MrhAddr.Data16, MrhData, MRC_MRH_CMD_MRS, FALSE);
    if (Status != mrcSuccess) {
      break;
    }
  }

  return Status;
}

/**
  This function sends the proper MRS command for specific ranks as indicated by RankMask

  @param[in] MrcData    - Include all the MRC data
  @param[in] Controller - Controller to send MRS.
  @param[in] Channel    - Channel to send MRS.
  @param[in] RankMask   - Rank mask for which command will be sent to
  @param[in] MR         - MRS command to be sent
  @param[in] Value      - Value to be sent

  @retval MrcStatus
**/
MrcStatus
MrcWriteMRS (
  IN MrcParameters  *const MrcData,
  IN       UINT32          Controller,
  IN const UINT32          Channel,
  IN UINT8                 RankMask,
  IN const UINT8           MR,
  IN const UINT16          Value
  )
{
  MrcChannelOut *ChannelOut;
  MrcStatus     Status;
  UINT32        Rank;
  UINT16        MrhData;
  MRC_MRH_ADDRESS_TYPE  MrhAddr;
  MrhAddr.Data16 = 0;

  ChannelOut  = &MrcData->Outputs.Controller[Controller].Channel[Channel];
  RankMask    &= ChannelOut->ValidRankBitMask;
  Status      = mrcSuccess;

  MrhAddr.Ddr3Ddr4Mrs.MrNum   = MR;
  MrhAddr.Ddr3Ddr4Mrs.MA13To8 = (Value >> 8) & 0x3F;
  MrhData = Value & 0xFF;
  for (Rank = 0; Rank < MAX_RANK_IN_CHANNEL; Rank++) {
    if (((1 << Rank) & RankMask) == 0) {
      continue;
    }
    //MRC_DEBUG_MSG (&MrcData->Outputs.Debug, MSG_LEVEL_ERROR, "MrcWriteMRS: Mc %d, Ch %d,Rank %d, MR%d = 0x%04X\n", Controller, Channel, Rank, MR, Value);
    Status = MrcRunMrh (MrcData, Controller, Channel, Rank, MrhAddr.Data16, MrhData, MRC_MRH_CMD_MRS, FALSE);
    if (Status != mrcSuccess) {
      break;
    }
  }

  return Status;
}

/**
  Use MRS Burst FSM to program all relevant MR registers in one quick operation.
  Can only be used on ULT/ULX (up to 2 ranks, no ECC).
  Runs on all populated controllers, channels, and ranks.

  @param[in]  MrcData - Include all MRC global data.

  @retval mrcSuccess    - MRS FSM was executed successfully
  @retval mrcDeviceBusy - timed out waiting for MRH or MRS FSM
**/
MrcStatus
MrcProgramMrsFsm (
  IN MrcParameters *const MrcData
  )
{
  const MrcInput      *Inputs;
  MrcDebug            *Debug;
  const MRC_FUNCTION  *MrcCall;
  MrcOutput           *Outputs;
  MrcStatus           Status;
  INT64               GetSetVal;
  UINT64              Timeout;
  UINT32              OffsetMrCommand;
  UINT32              OffsetMrsFsm;
  UINT32              Channel;
  UINT32              Controller;
  UINT32              IpChannel;
  BOOLEAN             Busy;
  BOOLEAN             Lpddr;
  MC0_CH0_CR_DDR_MR_COMMAND_STRUCT  MrCommand;
  MC0_CH0_CR_MRS_FSM_RUN_STRUCT     MrsFsm;

  Inputs  = &MrcData->Inputs;
  Outputs = &MrcData->Outputs;
  MrcCall = Inputs->Call.Func;
  Debug   = &Outputs->Debug;
  Status  = mrcSuccess;
  Lpddr   = Outputs->Lpddr;

  // Disable Self Refresh when using MRS FSM.
  // Function will wait for MC to exit self refresh.
  Status = MrcSelfRefreshState (MrcData, MRC_SR_EXIT);
  if (Status != mrcSuccess) {
    return Status;
  }

  // Make sure both MRH and MRS FSM are not busy
  for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
    for (Channel = 0; Channel < MAX_CHANNEL; Channel++) {
      if ((!MrcChannelExist (MrcData, Controller, Channel)) || IS_MC_SUB_CH (Lpddr, Channel)) {
        continue;
      }
      IpChannel = LP_IP_CH (Lpddr, Channel);
      OffsetMrCommand = OFFSET_CALC_MC_CH (MC0_CH0_CR_DDR_MR_COMMAND_REG, MC1_CH0_CR_DDR_MR_COMMAND_REG, Controller, MC0_CH1_CR_DDR_MR_COMMAND_REG, IpChannel);
      OffsetMrsFsm = MrcGetMrsRunFsmOffset (MrcData, Controller, IpChannel);

      Timeout = MrcCall->MrcGetCpuTime () + MRC_WAIT_TIMEOUT;   // 10 seconds timeout
      do {
        MrCommand.Data = MrcReadCR (MrcData, OffsetMrCommand);
        MrsFsm.Data    = MrcReadCR (MrcData, OffsetMrsFsm);
        Busy = (MrCommand.Bits.Busy == 1) || (MrsFsm.Bits.Run == 1);
      } while (Busy && (MrcCall->MrcGetCpuTime () < Timeout));
      if (Busy) {
        MRC_DEBUG_MSG (Debug, MSG_LEVEL_ERROR, "Timed out waiting for previous MRH command to finish!\n");
        return mrcDeviceBusy;
      }
    } // Channel
  } // Controller

  // Start MRS FSM on both channels in parallel
  MrsFsm.Data = 0;
  MrsFsm.Bits.Run = 1;

  for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
    for (Channel = 0; Channel < MAX_CHANNEL; Channel++) {
      if ((MrcChannelExist (MrcData, Controller, Channel)) && (!IS_MC_SUB_CH (Lpddr, Channel))) {
        IpChannel = LP_IP_CH (Lpddr, Channel);
        OffsetMrsFsm = MrcGetMrsRunFsmOffset (MrcData, Controller, IpChannel);
        MrcWriteCR (MrcData, OffsetMrsFsm, MrsFsm.Data);
      }
    } // Channel
  } // Controller

  // Poll for completion
  for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
    for (Channel = 0; Channel < MAX_CHANNEL; Channel++) {
      if ((!MrcChannelExist (MrcData, Controller, Channel)) || IS_MC_SUB_CH (Lpddr, Channel)) {
        continue;
      }
      IpChannel = LP_IP_CH (Lpddr, Channel);
      OffsetMrsFsm = MrcGetMrsRunFsmOffset (MrcData, Controller, IpChannel);

      Timeout = MrcCall->MrcGetCpuTime () + MRC_WAIT_TIMEOUT;   // 10 seconds timeout
      do {
        MrsFsm.Data = MrcReadCR (MrcData, OffsetMrsFsm);
        Busy = (MrsFsm.Bits.Run == 1);
      } while (Busy && (MrcCall->MrcGetCpuTime () < Timeout));
      if (Busy) {
        MRC_DEBUG_MSG (Debug, MSG_LEVEL_ERROR, "Timed out waiting for MRS FSM finish!\n");
        return mrcDeviceBusy;
      }
    } // Channel
  } // Controller

  // Re-enable Self Refresh when done using MRS FSM
  GetSetVal = 1;
  MrcGetSetMc (MrcData, MAX_CONTROLLER, GsmMccEnableSr, WriteNoCache, &GetSetVal);

  return Status;
}

