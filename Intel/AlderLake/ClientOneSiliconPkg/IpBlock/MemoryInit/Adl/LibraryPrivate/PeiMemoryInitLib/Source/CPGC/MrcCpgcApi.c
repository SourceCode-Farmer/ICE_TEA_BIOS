/** @file
  This file implements an interface between the MRC and the different
  versions of CPGC.

@copyright
  INTEL CONFIDENTIAL
  Copyright 2015 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains an 'Intel Peripheral Driver' and is uniquely identified as
  "Intel Reference Module" and is licensed for Intel CPUs and chipsets under
  the terms of your license agreement with Intel or your vendor. This file may
  be modified by the user, subject to additional terms of the license agreement.

@par Specification Reference:
**/
#include "MrcInterface.h"
#include "MrcCpgcOffsets.h"
#include "MrcCommon.h"
#include "MrcCpgcApi.h"
#include "Cpgc20.h"
#include "Cpgc20TestCtl.h"
#include "Cpgc20Patterns.h"
#include "MrcMemoryApi.h"
#include "MrcPpr.h"

///
/// External Function Definitions
///

/**
  This function will program the patterns passed in into the Pattern Generators.

  @param[in]  MrcData     - Pointer to MRC global data.
  @param[in]  McChBitMask - Memory Controller Channel Bit mask for which test should be setup for.
  @param[in]  Pattern     - Array of 64-bit patterns.
  @param[in]  PatLen      - Number of Patterns to program.
  @param[in]  Start       - Starting Chunk of pattern programming.

  @retval MrcStatus - mrcSuccess if TestEngine is supported, else mrcFail.
**/
MrcStatus
MrcSetCadbPgPattern (
  IN  MrcParameters *const  MrcData,
  IN  UINT8                 McChBitMask,
  IN  UINT64_STRUCT         Pattern[CADB_20_MAX_CHUNKS],
  IN  UINT32                PatLen,
  IN  UINT32                Start
  )
{
  switch (MrcData->Inputs.TestEngine) {
    case MrcTeCpgc20:
      Cadb20ProgramPatGen (MrcData, McChBitMask, Start, PatLen, Pattern);
      break;

    default:
      return mrcFail;
      break;
  }

  return mrcSuccess;
}

/**
  This function will configure the pattern rotation for dynamic and static patterns.

  @param[in]  MrcData    - Pointer to global MRC data.
  @param[in]  PatCtlPtr  - Pointer to rotation configuration.

  @retval MrcStatus - mrcSuccess if TestEngine is supported, else mrcFail.
**/
MrcStatus
MrcProgramDataPatternRotation (
  IN  MrcParameters   *const  MrcData,
  IN  MRC_PATTERN_CTL *const  PatCtlPtr
  )
{
  switch (MrcData->Inputs.TestEngine) {
    case MrcTeCpgc20:
      // Program the data rotation -- IncRate if Dynamic, 0 if Static.
      Cpgc20ConfigPgRotation (MrcData, ((PatCtlPtr->PatSource == MrcPatSrcDynamic) ? PatCtlPtr->IncRate : 0));  // No data rotation
      if (PatCtlPtr->PatSource == MrcPatSrcAllZeroes) {
        Cpgc20SetPgInvDcEn (MrcData, 0xFFFFFFFFFFFFFFFFULL, 0xFF);  // Enable DC on all lanes, including ECC
      } else {
      // Disable DC/Invert
      Cpgc20SetPgInvDcEn (MrcData, 0, 0);
      }
      Cpgc20SetPgInvDcCfg (MrcData, ((PatCtlPtr->PatSource == MrcPatSrcAllZeroes) ? Cpgc20DcMode : Cpgc20InvertMode), 0, FALSE, 0, 0xAA);
      break;

    default:
      return mrcFail;
      break;
  }

  return mrcSuccess;
}

/**
  This function programs the Error Mask for Cacheline and UI comparisons.
  A value of 1 enables error checking.

  @param[in]  MrcData       - Pointer to global MRC data.
  @param[in]  CachelineMask - Bit mask of cachelines to enable.
  @param[in]  ChunkMask     - Bit mask of chunks to enable.

  @retval MrcStatus - mrcSuccess if TestEngine is supported, else mrcFail.
**/
MrcStatus
MrcSetChunkAndClErrMsk (
  IN  MrcParameters *const  MrcData,
  IN  UINT32                CachelineMask,
  IN  UINT32                ChunkMask
  )
{
  switch (MrcData->Inputs.TestEngine) {
    case MrcTeCpgc20:
      Cpgc20SetChunkClErrMsk (MrcData, CachelineMask, ChunkMask);
      break;

    default:
      return mrcFail;
  }

  return mrcSuccess;
}

/**
  This function programs the Error Mask for Data and ECC bit lanes.
  A value of 1 enables error checking for that bit.

  @param[in]  MrcData    - Pointer to global MRC data.
  @param[in]  DataMask   - Bit mask of Data bits to check.
  @param[in]  EccMask    - Bit mask of ECC bits to check.

  @retval MrcStatus - mrcSuccess if TestEngine is supported, else mrcFail.
**/
MrcStatus
MrcSetDataAndEccErrMsk (
  IN  MrcParameters *const  MrcData,
  IN  UINT64                DataMask,
  IN  UINT8                 EccMask
  )
{
  MrcStatus     Status;
  UINT64_STRUCT SubChErrMsk;
  UINT64        DataValue;
  UINT8         EccValue;

  Status = mrcSuccess;
  // Using bitwise complement for Masks as the register fields are enabled when 0 and disabled when 1
  DataValue = ~DataMask;
  EccValue = ~EccMask;

  switch (MrcData->Inputs.TestEngine) {
    case MrcTeCpgc20:
      // We set the entire x64 channel.
      SubChErrMsk.Data = DataValue;
      Cpgc20SetDataErrMsk (MrcData, 0x1, SubChErrMsk.Data32.Low);
      Cpgc20SetDataErrMsk (MrcData, 0x2, SubChErrMsk.Data32.High);
      if (MrcData->Outputs.EccSupport) {
        Cpgc20SetEccErrMsk (MrcData, EccValue);
      }
      break;

    default:
      Status = mrcFail;
      break;
  }

  return Status;
}

/**
  This function programs the test loop count.

  @param[in]  MrcData    - Pointer to MRC global data.
  @param[in]  LoopCount  - Number of sequence iterations. 0 means infinite test.

  @retval MrcStatus - mrcSuccess if TestEngine is supported, else mrcFail.
**/
MrcStatus
MrcSetLoopcount (
  IN MrcParameters *const  MrcData,
  IN UINT32                LoopCount
  )
{
  switch (MrcData->Inputs.TestEngine) {
    case MrcTeCpgc20:
      Cpgc20SetLoopCount (MrcData, &LoopCount);
      break;

    default:
      return mrcFail;
      break;
  }

  return mrcSuccess;
}

/**
  This function programs the test engine stop condition.

  @param[in]  MrcData         - Pointer to MRC global data.
  @param[in]  StopType        - MRC_TEST_STOP_TYPE: NSOE, NTHSOE, ABGSOE, ALSOE.
  @param[in]  NumberOfErrors  - Number of errors required to stop the test when StopType is NTHSOE.

  @retval MrcStatus - mrcSuccess if TestEngine is supported, else mrcFail.
**/
MrcStatus
MrcSetupTestErrCtl (
  IN  MrcParameters       *const  MrcData,
  IN  MRC_TEST_STOP_TYPE          StopType,
  IN  UINT32                      NumberOfErrors
  )
{
  MrcStatus Status;

  Status = mrcSuccess;
  MrcData->Outputs.ReutStopType = StopType;

  switch (MrcData->Inputs.TestEngine) {
    case MrcTeCpgc20:
      Cpgc20SetupTestErrCtl (MrcData, StopType, NumberOfErrors);
      break;

    default:
      Status = mrcFail;
  }

  return Status;
}

/**
  This function will Setup REUT Error Counters to count errors for specified type.

  @param[in]  MrcData           - Include all MRC global data.
  @param[in]  CounterPointer    - Specifies in register which Counter to setup. Valid for ErrControl values:
                                  ErrCounterCtlPerLane, ErrCounterCtlPerByte, ErrCounterCtlPerNibble, or ErrCounterCtlPerUI.
  @param[in]  ErrControl        - Specifies which type of error counter read will be executed.
  @param[in]  CounterUI         - Specifies which UI will be considered when counting errors.
                                  00 - All UI; 01 - Even UI; 10 - Odd UI; 11 - Particular UI (COUNTER_CONTROL_SEL = ErrCounterCtlPerUI)

  @retval mrcWrongInputParameter if ErrControlSel is an incorrect value, otherwise mrcSuccess.
**/
MrcStatus
MrcSetupErrCounterCtl (
  IN  MrcParameters *const      MrcData,
  IN  UINT8                     CounterPointer,
  IN  MRC_ERR_COUNTER_CTL_TYPE  ErrControlSel,
  IN  UINT8                     CounterUI
  )
{
  MrcStatus Status;

  Status = mrcSuccess;

  switch (MrcData->Inputs.TestEngine) {
    case MrcTeCpgc20:
      switch (ErrControlSel) {
        case ErrCounterCtlAllLanes:
          Status = Cpgc20SetupErrCounterCtl (MrcData, 0, ErrControlSel, 0, 0, 0);
          break;

        case ErrCounterCtlPerByte:
//        case ErrCounterCtlPerNibble:
        case ErrCounterCtlPerLane:
        case ErrCounterCtlPerUI:
          Status = Cpgc20SetupErrCounterCtl (MrcData, CounterPointer, ErrControlSel, CounterPointer, 1, CounterUI);
          break;

        default:
          Status = mrcWrongInputParameter;
          break;
        }
      break;

    default:
      Status = mrcFail;
      break;
  }
  return Status;
}

/**
  This function returns the Bit Group Error status results.

  @param[in]  MrcData     - Include all MRC global data.
  @param[in]  Controller  - Desired controller to get ErrStatus
  @param[in]  Channel     - Desired channel to get ErrStatus
  @param[out] Status      - Pointer to array where the lane error status values will be stored.

  @retval Nothing.
**/
void
MrcGetBitGroupErrStatus (
  IN  MrcParameters   *const  MrcData,
  IN  UINT32                  Controller,
  IN  UINT32                  Channel,
  OUT UINT8                   *Status
  )
{
  switch (MrcData->Inputs.TestEngine) {
    case MrcTeCpgc20:
      Cpgc20GetBitGroupErrStatus (MrcData, Controller, Channel, Status);
      break;

    default:
      break;
  }
}

/**
  This function returns the Error status results of the specified Error Type.

  @param[in]  MrcData     - Include all MRC global data.
  @param[in]  Controller  - Desired Controller.
  @param[in]  Channel     - Desired Channel.
  @param[in]  Param       - Specifies which type of error status read will be executed.
  @param[out] Buffer      - Pointer to buffer which register values will be read into.
                              Error status bits will be returned starting with bit zero. Logical shifting will not be handled by this function.

  @retval Returns mrcWrongInputParameter if Param value is not supported by this function, otherwise mrcSuccess.
**/
MrcStatus
MrcGetMiscErrStatus (
  IN  MrcParameters   *const  MrcData,
  IN  UINT32                  Controller,
  IN  UINT32                  Channel,
  IN  MRC_ERR_STATUS_TYPE     Param,
  OUT UINT64          *const  Buffer
  )
{
  MrcStatus Status;

  switch (Param) {
    case ByteGroupErrStatus:
    case EccLaneErrStatus:
    case ChunkErrStatus:
    case RankErrStatus:
    case NthErrStatus:
    case NthErrOverflow:
    case AlertErrStatus:
    case WdbRdChunkNumStatus:
      switch (MrcData->Inputs.TestEngine) {
        case MrcTeCpgc20:
          Status = Cpgc20GetErrEccChunkRankByteStatus (MrcData, Controller, Channel, Param, Buffer);
          break;
        default:
          Status = mrcWrongInputParameter;
          break;
      }

    default:
      Status = mrcWrongInputParameter;
      break;
  }
  return Status;
}

/**
  This function sets or returns the Logical-to-Physical mapping of Banks.  The index to the array
  is the logical address, and the value at that index is the physical address.  This function
  operates on a linear definition of Banks, even though there may be a hierarchy as BankGroup->Bank.
  For system with X Bank Groups and Y Banks per group, the Banks are indexed in the array as:
  (X * Y + Z) where X is the bank group, Y is the total number of banks per group, and Z is the
  target bank belonging to bank group X.

  Example:
    Bank Group 3, Bank 5, 8 Banks per Bank Group -> Index position (3 * 8) + 5 == 29.

  The function will act upon the array bounded by the param Length.
  The Function always starts at Bank 0.

  @param[in]  MrcData     - Pointer to MRC global data.
  @param[in]  Controller  - Desired Controller.
  @param[in]  Channel     - Desired Channel.
  @param[in]  L2pBankList - Array of logical-to-physical Bank Mapping.
  @param[in]  Length      - Array length of L2pBankList buffer.
  @param[in]  IsGet       - Boolean; if TRUE, request is a Get, otherwise Set.

  @retval MrcStatus.
**/
MrcStatus
MrcGetSetBankSequence (
  IN  MrcParameters *const      MrcData,
  IN  UINT32                    Controller,
  IN  UINT32                    Channel,
  IN  MRC_BG_BANK_PAIR  *const  L2pBankList,
  IN  UINT32                    Length,
  IN  BOOLEAN                   IsGet
  )
{
  MrcInput  *Inputs;
  MrcStatus Status;
  UINT8     BankList[CPGC20_MAX_BANKS_PER_CHANNEL];
  UINT8     Index;
  UINT8     BgOffset;
  UINT8     BankMask;

  Status  = mrcSuccess;
  Inputs  = &MrcData->Inputs;

  switch (Inputs->TestEngine) {
    case MrcTeCpgc20:
      if (Length > CPGC20_MAX_BANKS_PER_CHANNEL) {
        Status = mrcWrongInputParameter;
      } else {
        BgOffset = (MrcData->Outputs.DdrType == MRC_DDR_TYPE_DDR4) ? CPGC20_BANK_GROUP_FIELD_OFFSET_DDR4 : CPGC20_BANK_GROUP_FIELD_OFFSET_DDR5;
        BankMask = (1 << BgOffset) - 1;
        if (!IsGet) {
          for (Index = 0; Index < Length; Index++) {
            // L2pBankList[Index].Bank = bank from software loop
            // Index = CPGC logical bank
            BankList[Index] = (L2pBankList[Index].BankGroup << BgOffset) | (L2pBankList[Index].Bank);
          }
        }
        Cpgc20GetSetBankMap (MrcData, Controller, Channel, BankList, Length, IsGet);
        if (IsGet) {
          for (Index = 0; Index < Length; Index++) {
            L2pBankList[Index].BankGroup = BankList[Index] >> BgOffset;
            L2pBankList[Index].Bank = BankList[Index] & (BankMask);
          }
        }
      }
      break;

    default:
      Status = mrcFail;
      break;
  }

  return Status;
}

/**
  This function returns the Error Counter status.

  @param[in]  MrcData       - Include all MRC global data.
  @param[in]  Controller    - Desired Controller.
  @param[in]  Channel       - Desired Channel.
  @param[in]  CounterSelect - Specifies in register which Counter to setup. Valid for ErrControl values:
                                ErrCounterCtlPerLane, ErrCounterCtlPerByte, or ErrCounterCtlPerChunk.
  @param[in]  ErrControl    - Specifies which type of error counter read will be executed.
  @param[in]  Status        - Pointer to buffer where counter status will be held.
  @param[out] Overflow      - Pointer to variable indicating if Overflow has been reached.

  @retval Nothing.
**/
void
MrcGetErrCounterStatus (
  IN  MrcParameters   *const    MrcData,
  IN  UINT32                    Controller,
  IN  UINT32                    Channel,
  IN  UINT8                     CounterSelect,
  IN  MRC_ERR_COUNTER_CTL_TYPE  ErrControl,
  OUT UINT32          *const    Status,
  OUT BOOLEAN         *const    Overflow
  )
{
  switch (MrcData->Inputs.TestEngine) {
    case MrcTeCpgc20:
      switch (ErrControl) {
        case ErrCounterCtlAllLanes:
          Cpgc20GetErrCounterStatus (MrcData, Controller, Channel, 0, Status, Overflow);
          break;

        case ErrCounterCtlPerByte:
//        case ErrCounterCtlPerNibble:
        case ErrCounterCtlPerLane:
        case ErrCounterCtlPerUI:
          Cpgc20GetErrCounterStatus (MrcData, Controller, Channel, CounterSelect, Status, Overflow);
          break;

        default:
          break;
      }
      break;

    default:
      *Status = 0xFFFFFFFF;
      break;
  }
}

/**
CADB Select Override Mapping:


CADB_BUF bit    CADB_BUF.field bit    DDR4            LPDDR4 Select   LPDDR4 DeSelect   DDR5 Select     DDR5 DeSelect
==============================================================================================================================
[46]                                                                                    CA_2nd[13]
[45]                                                                                    CA_2nd[12]
[44]                                                                                    CA_2nd[11]
[43]                                                                                    CA_2nd[10]
[42]                                                                                    CA_2nd[9]
[41]            CADB_BUF.CA [23]                      CA_4th[5]                         CA_2nd[8]
[40]            CADB_BUF.CA [22]      ACT_n           CA_4th[4]                         CA_2nd[7]
[39]            CADB_BUF.CA [21]      BG1             CA_4th[3]                         CA_2nd[6]
[38]            CADB_BUF.CA [20]      BG0             CA_4th[2]                         CA_2nd[5]
[37]            CADB_BUF.CA [19]      BA1             CA_4th[1]                         CA_2nd[4]
[36]            CADB_BUF.CA [18]      BA0             CA_4th[0]                         CA_2nd[3]
[35]            CADB_BUF.CA [17]                      CA_3rd[5]                         CA_2nd[2]
[34]            CADB_BUF.CA [16]      MA[16]/RAS_n    CA_3rd[4]                         CA_2nd[1]
[33]            CADB_BUF.CA [15]      MA[15]/CAS_n    CA_3rd[3]                         CA_2nd[0]
[32]            CADB_BUF.CA [14]      MA[14]/WE_n     CA_3rd[2]
[31]            CADB_BUF.CA [13]      MA[13]          CA_3rd[1]                         CA_1st[13]      CA[13]
[30]            CADB_BUF.CA [12]      MA[12]          CA_3rd[0]                         CA_1st[12]      CA[12]
[29]            CADB_BUF.CA [11]      MA[11]          CA_2nd[5]                         CA_1st[11]      CA[11]
[28]            CADB_BUF.CA [10]      MA[10]          CA_2nd[4]                         CA_1st[10]      CA[10]
[27]            CADB_BUF.CA [9]       MA[9]           CA_2nd[3]                         CA_1st[9]       CA[9]
[26]            CADB_BUF.CA [8]       MA[8]           CA_2nd[2]                         CA_1st[8]       CA[8]
[25]            CADB_BUF.CA [7]       MA[7]           CA_2nd[1]                         CA_1st[7]       CA[7]
[24]            CADB_BUF.CA [6]       MA[6]           CA_2nd[0]                         CA_1st[6]       CA[6]
[23]            CADB_BUF.CA [5]       MA[5]           CA_1st[5]       CA[5]_subch0      CA_1st[5]       CA[5]
[22]            CADB_BUF.CA [4]       MA[4]           CA_1st[4]       CA[4]_subch0      CA_1st[4]       CA[4]
[21]            CADB_BUF.CA [3]       MA[3]           CA_1st[3]       CA[3]_subch0      CA_1st[3]       CA[3]
[20]            CADB_BUF.CA [2]       MA[2]           CA_1st[2]       CA[2]_subch0      CA_1st[2]       CA[2]
[19]            CADB_BUF.CA [1]       MA[1]           CA_1st[1]       CA[1]_subch0      CA_1st[1]       CA[1]
[18]            CADB_BUF.CA [0]       MA[0]           CA_1st[0]       CA[0]_subch0      CA_1st[0]       CA[0]
[17]            CADB_BUF.PAR          PAR
[16]            CADB_BUF.VAL          Set for all the non-tri-stated CADB chunks!
[15]
[14]            CADB_BUF.CKE [3]      CKE[3]                          CKE[1]_subch1
[13]            CADB_BUF.CKE [2]      CKE[2]                          CKE[0]_subch1
[12]            CADB_BUF.CKE [1]      CKE[1]                          CKE[1]_subch0
[11]            CADB_BUF.CKE [0]      CKE[0]                          CKE[0]_subch0
[10]            CADB_BUF.ODT [3]      ODT[3]                          CS[3]_subch1      CS_nontarget[3]
[9]             CADB_BUF.ODT [2]      ODT[2]                          CS[2]_subch1      CS_nontarget[2]
[8]             CADB_BUF.ODT [1]      ODT[1]                          CS[3]_subch0      CS_nontarget[1]
[7]             CADB_BUF.ODT [0]      ODT[0]                          CS[2]_subch0      CS_nontarget[0]
[6]
[5]
[4]
[3]             CADB_BUF.CS [3]       CSb[3]                          CS[1]_subch1      CS[3]           CS[3]
[2]             CADB_BUF.CS [2]       CSb[2]                          CS[0]_subch1      CS[2]           CS[2]
[1]             CADB_BUF.CS [1]       CSb[1]                          CS[1]_subch0      CS[1]           CS[1]
[0]             CADB_BUF.CS [0]       CSb[0]                          CS[0]_subch0      CS[0]           CS[0]
**/
/**
  This function takes in a generic mapping of CS, CKE, ODT, and Command/Address
  signals and converts them to the expected layout of the hardware engine.

  @param[in]  MrcData - Pointer to global data structure.
  @param[in]  CmdAddr - Union of Command/Address signals per each supported memory technology
  @param[in]  Cke     - Bit Mask of Clock Enable signals per sub-channel.
  @param[in]  Cs      - Bit Mask of Chip Select signals per sub-channel.
  @param[in]  Odt     - Bit Mask of On Die Termination signals.

  @retval - MrcStatus: mrcSuccess if successful, otherwise an error status.
**/
MrcStatus
CpgcConvertDdrToCadb (
  IN  MrcParameters     *const  MrcData,
  IN  MRC_CA_MAP_TYPE   *const  CmdAddr,
  IN  UINT8                     Cke[2],
  IN  UINT8                     Cs[2],
  IN  UINT8                     Odt,
  OUT UINT64_STRUCT     *const  CadbVal
  )
{
  MRC_FUNCTION  *MrcCall;
  MrcStatus     Status;
  MrcDdrType    DdrType;
  UINT64        CadbBits;
  CADB_BUF_FIELDS CadbBuf;

  DdrType = MrcData->Outputs.DdrType;
  MrcCall = MrcData->Inputs.Call.Func;
  Status = mrcSuccess;
  CadbBits = 0;

  if (DdrType == MRC_DDR_TYPE_DDR4) {
    CadbBits = MrcCall->MrcLeftShift64 (CmdAddr->Ddr4.Ma, CADB_DDR4_MA_0_16_OFF);
    CadbBits |= MrcCall->MrcLeftShift64 (CmdAddr->Ddr4.Ba, CADB_DDR4_BA_OFF);
    CadbBits |= MrcCall->MrcLeftShift64 (CmdAddr->Ddr4.Bg, CADB_DDR4_BG_OFF);
    CadbBits |= MrcCall->MrcLeftShift64 (CmdAddr->Ddr4.Act_n, CADB_DDR4_ACT_N_OFF);
    // We don't use CA Parity pin, no need to stress it
    // CadbBits |= CmdAddr->Ddr4.Par << CADB_DDR4_PAR_OFF;
    CadbBits |= MrcCall->MrcLeftShift64 ((Cke[0] & CADB_DDR4_CKE_CTL_MSK), CADB_DDR4_CKE_OFF);
    CadbBits |= MrcCall->MrcLeftShift64 ((Cs[0] & CADB_DDR4_CKE_CTL_MSK), CADB_DDR4_CS_B_OFF);
    CadbBits |= MrcCall->MrcLeftShift64 ((Odt & CADB_DDR4_CKE_CTL_MSK), CADB_DDR4_ODT_OFF);
  } else if (MrcData->Outputs.Lpddr) {
    if (DdrType == MRC_DDR_TYPE_LPDDR4) {
      CadbBits = CmdAddr->Lpddr4.Ca1 << CADB_LPDDR_CA_OFF;
    } else {
      CadbBits = CmdAddr->Lpddr5.Ca1 << CADB_LPDDR_CA_OFF;
    }
    //    CadbBits |= CmdAddr->Lpddr4.Ca2 << CADB_LPDDR4_CA_2_OFF;
    //    CadbBits |= MrcCall->MrcLeftShift64 (CmdAddr->Lpddr4.Ca3, CADB_LPDDR4_CA_3_OFF);
    //    CadbBits |= MrcCall->MrcLeftShift64 (CmdAddr->Lpddr4.Ca4, CADB_LPDDR4_CA_4_OFF);
    CadbBits |= MrcCall->MrcLeftShift64 ((Cke[0] & CADB_LPDDR_CS_CKE_MSK), CADB_LPDDR_CKE_SC0_OFF);
    CadbBits |= MrcCall->MrcLeftShift64 ((Cke[1] & CADB_LPDDR_CS_CKE_MSK), CADB_LPDDR_CKE_SC1_OFF);
    CadbBits |= MrcCall->MrcLeftShift64 ((Cs[0] & CADB_LPDDR_CS_CKE_MSK), CADB_LPDDR_CS_0_1_SC0_OFF);
    CadbBits |= MrcCall->MrcLeftShift64 ((Cs[1] & CADB_LPDDR_CS_CKE_MSK), CADB_LPDDR_CS_0_1_SC1_OFF);
    CadbBits |= MrcCall->MrcLeftShift64 (((Cs[0] >> 2) & CADB_LPDDR_CS_CKE_MSK), CADB_LPDDR_CS_2_3_SC0_OFF);
    CadbBits |= MrcCall->MrcLeftShift64 (((Cs[1] >> 2) & CADB_LPDDR_CS_CKE_MSK), CADB_LPDDR_CS_2_3_SC1_OFF);
  } else if (DdrType == MRC_DDR_TYPE_DDR5) {
    CadbBuf.Data = CadbBits;
    CadbBuf.BitsDdr5Deselect.Cs = Cs[0];
    CadbBuf.BitsDdr5Deselect.Ca = CmdAddr->Ddr5.Ca;
    CadbBits = CadbBuf.Data;
  } else {
    Status = mrcDimmNotSupport;
  }
  CadbBits |= MrcCall->MrcLeftShift64 (1, CADB_BUF_VAL_OFF);    // Set VAL bit, otherwise it may be tri-stated

  CadbVal->Data = CadbBits;

  return Status;
}


/**
  This function will configure and program the CPGC DPAT Unisequencers.  The PGs must be selected before this call.

  @param[in]  MrcData    - Global MRC data.
  @param[in]  LfsrPoly   - LFSR Polynominal applied to all UNISEQs
  @param[in]  PatCtlPtr  - Structure that stores start, Stop, IncRate and Dqpat for pattern.
  @param[in]  Start      - Zero based index which Unisequencer to start programming.
  @param[in]  UsqCount   - Number of Unisequencers to program.

  @retval - Nothing
**/
void
MrcInitUnisequencer (
  IN MrcParameters *const    MrcData,
  IN  MRC_PG_LFSR_TYPE       LfsrPoly,
  IN  MRC_PATTERN_CTL *const PatCtlPtr,
  IN const UINT8             Start,
  IN const UINT8             UsqCount
  )
{
  UINT8   Index;
  UINT8   Stop;

  Stop = Start + UsqCount;
  for (Index = Start; Index < Stop; Index++) {
    switch (MrcData->Inputs.TestEngine) {
    case MrcTeCpgc20:
      Cpgc20DPatUsqCfg (MrcData, LfsrPoly, PatCtlPtr, Index);
      break;
    default:
      break;
    }
  }
}
/**
  This function will program the CPGC PGs Mux Seeds.  The PGs must be selected before this call.

  @param[in] MrcData    - Global MRC data.
  @param[in] Seeds      - Array of seeds programmed into PAT_WDB_CL_MUX_PB_RD/WR.
  @param[in] Start      - Zero based index which Unisequencer to start programming.
  @param[in] SeedCount  - Number of seeds in the array.

  @retval - Nothing
**/
void
MrcInitCpgcPgMux (
  IN MrcParameters *const MrcData,
  IN const UINT32  *const Seeds,
  IN const UINT8          Start,
  IN const UINT8          SeedCount
  )
{
  UINT8   Index;
  UINT8   Stop;

  Stop = Start + SeedCount;
  for (Index = Start; Index < Stop; Index++) {
    switch (MrcData->Inputs.TestEngine) {
      case MrcTeCpgc20:
        Cpgc20LfsrSeed (MrcData, Seeds[Index], Index);
        break;
      default:
        break;
    }
  }
}

/**
  This function will program the CADB PGs Mux Seeds.  The PGs must be selected before this call.

  @param[in] MrcData   - Global MRC data.
  @param[in] Seeds     - Array of seeds programmed into PAT_WDB_CL_MUX_PB_RD/WR.
  @param[in] Start     - Zero based index which Unisequencer to start programming.
  @param[in] SeedCount - Number of seeds in the array.

  @retval - Nothing
**/
void
MrcInitCadbPgMux (
  IN MrcParameters *const MrcData,
  IN const UINT32  *const Seeds,
  IN const UINT8          Start,
  IN const UINT8          SeedCount
  )
{
  UINT8   Index;
  UINT8   Stop;

  Stop = Start + SeedCount;
  for (Index = Start; Index < Stop; Index++) {
    switch (MrcData->Inputs.TestEngine) {
      case MrcTeCpgc20:
        Cadb20LfsrSeed (MrcData, Seeds[Index], Index);
        break;

      default:
        break;
    }
  }
}

/**
  This function will enable or disable CPGC engines on all channels.
  @todo: Move to GetSet infrastructure.

  @param[in] MrcData    - Global MRC data structure
  @param[in] ActiveMode - If true, enable CPGC engines. If false, set to idle mode.

  @retval - Nothing
**/
void
MrcSetCpgcInitMode (
  IN MrcParameters *const MrcData,
  IN BOOLEAN              ActiveMode
  )
{
  MrcOutput   *Outputs;
  UINT32      Offset;
  UINT32      Controller;
  UINT32      Channel;
  UINT32      IpChannel;
  BOOLEAN     Lpddr;
  MC0_REQ0_CR_CPGC_SEQ_CFG_A_STRUCT CpgcSeqCfgA;

  Outputs = &MrcData->Outputs;
  Lpddr = Outputs->Lpddr;

  for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
    for (Channel = 0; Channel < Outputs->MaxChannels; Channel++) {
      if ((MrcChannelExist (MrcData, Controller, Channel)) && (!(IS_MC_SUB_CH (Lpddr, Channel)))) {
        IpChannel = LP_IP_CH (Lpddr, Channel);
        Offset = MrcGetCpgcSeqCfgOffset (MrcData, Controller, IpChannel);
        CpgcSeqCfgA.Data =  MrcReadCR (MrcData, Offset);
        CpgcSeqCfgA.Bits.INITIALIZATION_MODE = ActiveMode ? CPGC20_ACTIVE_MODE : CPGC20_IDLE_MODE;
        MrcWriteCR (MrcData, Offset, CpgcSeqCfgA.Data);
      }
    }
  }
}

/**
  This function will enable or disable the Global Start/Stop Bind feature for CPGC engines.

  @param[in] MrcData    - Global MRC data structure
  @param[in] GlobalStartBind - If true, the CPGC Enginees are bound to Start together. If false, the Global Start feature is disabled.
  @param[in] GlobalStopBind  - If true, the CPGC Enginees are bound to Stop together. If false, the Global Stop feature is disabled.

  @retval - Nothing
**/
VOID
MrcSetCpgcBindMode (
  IN MrcParameters *const MrcData,
  IN BOOLEAN              GlobalStartBind,
  IN BOOLEAN              GlobalStopBind
  )
{
  MrcOutput   *Outputs;
  UINT32      Offset;
  UINT32      Controller;
  UINT32      Channel;
  UINT32      IpChannel;
  BOOLEAN     Lpddr;
  MC0_REQ0_CR_CPGC_SEQ_CFG_A_STRUCT CpgcSeqCfgA;

  Outputs = &MrcData->Outputs;
  Lpddr = Outputs->Lpddr;

  for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
    for (Channel = 0; Channel < Outputs->MaxChannels; Channel++) {
      if ((MrcChannelExist (MrcData, Controller, Channel)) && (!(IS_MC_SUB_CH (Lpddr, Channel)))) {
        IpChannel = LP_IP_CH (Lpddr, Channel);
        Offset = MrcGetCpgcSeqCfgOffset (MrcData, Controller, IpChannel);
        CpgcSeqCfgA.Data =  MrcReadCR (MrcData, Offset);
        CpgcSeqCfgA.Bits.GLOBAL_START_BIND = GlobalStartBind;
        CpgcSeqCfgA.Bits.GLOBAL_STOP_BIND  = GlobalStopBind;
        MrcWriteCR (MrcData, Offset, CpgcSeqCfgA.Data);
      }
    }
  }
}

/**
  This function will convert the CPGC Bank address into the DRAM Bank Address and Bank Group bits based on DDR technology

  @param[in]  MrcData     - Global MRC data structure
  @param[in]  LogicalBank - CPGC Bank Number
  @param[out] BankAddress - Pointer to BankAddress value
  @param[out] BankGroup   - Pointer to BankGroup value

  @retval - Nothing
**/
VOID
MrcConvertCpgcBanktoCpgcAddress (
  IN  MrcParameters *const MrcData,
  IN  UINT32               LogicalBank,
  OUT UINT32               *BankAddress,
  OUT UINT32               *BankGroup
  )
{
  MrcOutput         *Outputs;
  UINT8             DdrType;

  Outputs = &MrcData->Outputs;
  DdrType = Outputs->DdrType;

  switch (DdrType) {
    case MRC_DDR_TYPE_DDR4:
      if ((BankAddress != NULL) && (BankGroup != NULL)) {
        *BankGroup = (LogicalBank >> CPGC20_BANK_GROUP_FIELD_OFFSET_DDR4) & 0x3;
        *BankAddress = LogicalBank & 0x3;
      }
      break;
    case MRC_DDR_TYPE_DDR5:
      if ((BankAddress != NULL) && (BankGroup != NULL)) {
        *BankGroup = (LogicalBank >> CPGC20_BANK_GROUP_FIELD_OFFSET_DDR5) & 0x7;
        *BankAddress = LogicalBank & 0x3;
      }
      break;
    case MRC_DDR_TYPE_LPDDR4:
    case MRC_DDR_TYPE_LPDDR5: // @todo: LPDDR5 BG Mode
    default:
      if (BankAddress != NULL) {
        *BankAddress = LogicalBank;
      }
      break;
  }
}

/**
  This function will convert the DRAM Bank Address and Bank Group bits into the CPGC Bank address based on DDR technology

  @param[in]  MrcData     - Global MRC data structure
  @param[in]  BankAddress - Pointer to BankAddress value
  @param[in]  BankGroup   - Pointer to BankGroup value
  @param[out] LogicalBank - CPGC Bank Number

  @retval - Nothing
**/
VOID
MrcConvertBankAddresstoCpgcBank (
  IN  MrcParameters *const MrcData,
  IN  UINT32               BankAddress,
  IN  UINT32               BankGroup,
  OUT UINT32               *LogicalBank
  )
{
  MrcOutput         *Outputs;
  UINT32            FirstController;
  UINT32            FirstChannel;
  UINT8             Rank;
  UINT8             FirstDimm;

  Outputs         = &MrcData->Outputs;
  FirstController = Outputs->FirstPopController;
  FirstChannel    = Outputs->Controller[FirstController].FirstPopCh;
  FirstDimm       = 0;

  switch (Outputs->DdrType) {
    default:
      return;
    case MRC_DDR_TYPE_DDR4:
      if (LogicalBank != NULL) {
        *LogicalBank = ((BankGroup & 0x3) << CPGC20_BANK_GROUP_FIELD_OFFSET_DDR4) | (BankAddress & 0x3);
      }
      break;
    case MRC_DDR_TYPE_DDR5:
      if (LogicalBank != NULL) {
        *LogicalBank = ((BankGroup & 0x7) << CPGC20_BANK_GROUP_FIELD_OFFSET_DDR5) | (BankAddress & 0x3);
      }
      break;
    case MRC_DDR_TYPE_LPDDR4:
      if (LogicalBank != NULL) {
        *LogicalBank = (BankAddress & 0x7);
      }
      break;
    case MRC_DDR_TYPE_LPDDR5:
      for (Rank = 0; Rank < MAX_RANK_IN_CHANNEL; Rank++) {
        if (MrcRankExist (MrcData, FirstController, FirstChannel, Rank)) {
          FirstDimm = RANK_TO_DIMM_NUMBER (Rank);
        }
      }
      if (Outputs->Controller[FirstController].Channel[FirstChannel].Dimm[FirstDimm].BankGroups > 1) { // 4 Banks/4 Bank Groups Configuration
        if (LogicalBank != NULL) {
          *LogicalBank = ((BankGroup & 0x3) << CPGC20_BANK_GROUP_FIELD_OFFSET_DDR5) | (BankAddress & 0x3);
        }
      } else {
        switch (Outputs->Controller[FirstController].Channel[FirstChannel].Dimm[FirstDimm].Banks) {
          case 8:
            *LogicalBank = (BankAddress & 0x7);
            break;
          case 16:
            *LogicalBank = (BankAddress & 0xF);
            break;
        } // Switch (Banks)
      }
      break;
  } // Switch (DdrType)
}

/**
  This function will set the Clear Raster Repo indication bit in Register

  @param[in] MrcData     - Global MRC data structure
  @param[in] McChBitMask - Bit masks of MC Channels to program the register for.

  @retval - Nothing
**/
VOID
MrcClearRasterRepo (
  IN  MrcParameters *const MrcData,
  IN  UINT8                McChBitMask
  )
{
  BOOLEAN RasterRepoClear = TRUE;
  Cpgc20RasterRepoConfig (MrcData, McChBitMask, NULL, NULL, NULL, &RasterRepoClear, NULL); // Clear Raster Repo
}

/**
  This function programs the specified 64-bit Pattern provided, into CPGC ExtBuf registers.
  Data Pattern is shifted across each UI, if non-zero

  @param[in]  MrcData                 - Global MRC data structure
  @param[in]  McChBitMask             - Memory Controller Channel Bit mask to read status for.
  @param[in]  PatternQW               - Array of 64-bit Data Pattern to write
  @param[in]  UiShl                   - Array of Bit-shift values per UI
  @param[in]  ErrInjMask16            - Bitmask of DQ lanes to inject error

  @retval   mrcSuccess / mrcFail
**/
MrcStatus
MrcProgramMATSPattern (
  IN  MrcParameters *const MrcData,
  IN  UINT32               McChBitMask,
  IN  UINT64               PatternQW[],
  IN  UINT8                UiShl[16],
  IN  UINT16               ErrInjMask16
  )
{
  MRC_FUNCTION    *MrcCall;
  UINT32          Index;
  UINT32          Temphi;
  UINT32          CpgcExtBuf[8];
  UINT64_STRUCT   PatternTemp;

  MrcCall = MrcData->Inputs.Call.Func;
  MrcCall->MrcSetMem ((UINT8 *) CpgcExtBuf, sizeof (CpgcExtBuf), 0);

  PatternTemp.Data = PatternQW[0];
  for (Index = 0; Index < 8; Index++) {
    CpgcExtBuf[Index] = ((PatternTemp.Data32.High & ~ErrInjMask16) | (~PatternTemp.Data32.High & ErrInjMask16));
    // Rotate pattern left for each UI
    PatternTemp.Data = MrcCall->MrcLeftShift64 (PatternTemp.Data, UiShl[0]);
  } // i loop
  for (Index = 0; Index < 8; Index++) {
    Temphi = CpgcExtBuf[Index];
    MRC_DEBUG_MSG (&MrcData->Outputs.Debug, MSG_LEVEL_NOTE, "ExtBuf[%d]: 0x%08x\n", Index, Temphi);
  }
  WriteMATSFixedPattern (MrcData, CpgcExtBuf);
  return mrcSuccess;
}

/**
  Collect DDR advanced command CPGC error status

  cpgcErrDat0S                       - Repo Active   - Channel data lane [31:0] UI error status.
  cpgcErrDat1S                       - Repo Active   - Channel data lane [63:32] UI error status (for DDR4 only).
  cpgcErrDat2S                       - Repo Active   -
  cpgcErrDat3S                       - Repo Active   -
  cpgcErrEccS
  - Repo Active
  - [7:0]    - Ecc lane [7:0] UI error status.
  - [15:8]   - Same to [7:0]
  cpgcErrChunk                       - [15:0]   - Chunk error status
  cpgcErrColumn                      - Column address that error happens on
  cpgcErrRow                         - Row address that error happens on
  cpgcErrBank                        - [4:0]   Bank address that error happens on
  cpgcErrRank                        - [2:0]   Rank address that error happens on
  cpgcSubrank                        - [2:0]   Subrank address that error happens on

  @param[in]  MrcData                 - Global MRC data structure
  @param[in]  McChBitMask             - Memory Controller Channel Bit mask to read status for.
  @param[out] CpgcErrorStatusMats     - Array of error status

  @retval   mrcSucess / mrcFail
**/
MrcStatus
CpgcAdvTrainingErrorStatusMATS (
  IN  MrcParameters    *const  MrcData,
  IN  UINT32                   McChBitMask,
  OUT CPGC_ERROR_STATUS_MATS   CpgcErrorStatusMats[MAX_CONTROLLER][MAX_CHANNEL]
  )
{
  MrcStatus                                       Status = mrcSuccess;
  MrcOutput                                       *Outputs;
  MRC_FUNCTION                                    *MrcCall;
  UINT8                                           Controller;
  UINT8                                           Channel;
  UINT8                                           Byte;
  UINT8                                           MaxChDdr;
  UINT8                                           BitGroupErr[MAX_SDRAM_IN_DIMM];
  CPGC2_ERR_ADDR_INFO_LO                          ErrInfoLo;
  CPGC2_ERR_ADDR_INFO_HI                          ErrInfoHi;
  UINT32                                          ErrorSummary[3];
  UINT32                                          Data32;
  UINT32                                          Data32Upper;
  UINT64                                          ErrStatus;
  UINT8                                           EccByte;
  BOOLEAN                                         Ddr4;

  Outputs = &MrcData->Outputs;
  MrcCall = MrcData->Inputs.Call.Func;
  MaxChDdr = Outputs->MaxChannels;
  EccByte  = (Outputs->DdrType == MRC_DDR_TYPE_DDR5) ? NUM_ECC_BITS_DDR5 : NUM_ECC_BITS_DDR4;
  Ddr4 = (Outputs->DdrType == MRC_DDR_TYPE_DDR4);

  MrcCall->MrcSetMem ((UINT8 *) CpgcErrorStatusMats, MAX_CONTROLLER * MAX_CHANNEL * sizeof (CPGC_ERROR_STATUS_MATS), 0);

  for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
    for (Channel = 0; Channel < MaxChDdr; Channel++) {
      // Skip inactive channels
      if (MC_CH_MASK_CHECK (McChBitMask, Controller, Channel, Outputs->MaxChannels) == 0) {
        continue;
      }
      Cpgc20GetBitGroupErrStatus (MrcData, Controller, Channel, BitGroupErr);
      Data32 = 0;
      Data32Upper = 0;
      for (Byte = 0; Byte < Outputs->SdramCount; Byte++) {
        if (Outputs->EccSupport && (Byte == EccByte)) {
          CpgcErrorStatusMats[Controller][Channel].cpgcErrEccS  = BitGroupErr[Byte];
        } else if (Ddr4 && Byte >= 4) {
          Data32Upper |= BitGroupErr[Byte] << ((Byte - 4) * 8);
        } else {
          Data32 |= BitGroupErr[Byte] << Byte * 8;
        }
      }

      CpgcErrorStatusMats[Controller][Channel].cpgcErrDat0S = Data32;
      CpgcErrorStatusMats[Controller][Channel].cpgcErrDat2S = Data32;
      if (Ddr4) {
        // Data lane [63:32] error status
        CpgcErrorStatusMats[Controller][Channel].cpgcErrDat1S = Data32Upper;
        CpgcErrorStatusMats[Controller][Channel].cpgcErrDat3S = Data32Upper;
      }

      // TODO: Verify Ddr4 support for the other error status fields (chunk err, error summary)


      // 16 chucks per cacheline for DDR5
      MrcGetMiscErrStatus (MrcData, Controller, Channel, ChunkErrStatus, &ErrStatus);
      CpgcErrorStatusMats[Controller][Channel].cpgcErrChunk = (UINT32) ErrStatus;

      // Physical address info
      Cpgc20ReadErrSummary (MrcData, Controller, Channel, ErrorSummary);
      ErrInfoLo.Data = ErrorSummary[1];
      ErrInfoHi.Data = ErrorSummary[2];

      CpgcErrorStatusMats[Controller][Channel].cpgcErrRow = ErrInfoLo.Bits.Row;
      // Lower bits are cacheline boundary
      CpgcErrorStatusMats[Controller][Channel].cpgcErrColumn = ErrInfoLo.Bits.Column << (MrcLog2 (Outputs->BurstLength) - 1); // MrcLog2 returns (Log2)+1 so subtract 1 from result
      CpgcErrorStatusMats[Controller][Channel].cpgcErrBank = ErrInfoLo.Bits.Bank | ((ErrInfoHi.Bits.Bank & 0x1) << 4);
      CpgcErrorStatusMats[Controller][Channel].cpgcErrRank = ErrInfoHi.Bits.Rank;

#ifdef MRC_DEBUG_PRINT
      if (CpgcErrorStatusMats[Controller][Channel].cpgcErrDat0S |
          CpgcErrorStatusMats[Controller][Channel].cpgcErrDat2S |
          CpgcErrorStatusMats[Controller][Channel].cpgcErrDat1S |
          CpgcErrorStatusMats[Controller][Channel].cpgcErrDat3S)
      {
        MRC_DEBUG_MSG (&MrcData->Outputs.Debug, MSG_LEVEL_ERROR,
          "Error Found: Mc%d.C%d: Row = 0x%06x, Column = 0x%04x, Bank = 0x%02x, Chunk = 0x%02x, Rank%d\n",
          Controller,
          Channel,
          CpgcErrorStatusMats[Controller][Channel].cpgcErrRow,
          CpgcErrorStatusMats[Controller][Channel].cpgcErrColumn,
          CpgcErrorStatusMats[Controller][Channel].cpgcErrBank,
          CpgcErrorStatusMats[Controller][Channel].cpgcErrChunk,
          CpgcErrorStatusMats[Controller][Channel].cpgcErrRank
        );
        if (Ddr4) {
          MRC_DEBUG_MSG(&MrcData->Outputs.Debug, MSG_LEVEL_ERROR,
            "    cpgcErrEven = 0x%08x%08x, cpgcErrOdd = 0x%08x%08x, cpgcErrEccS = 0x%04x\n",
            CpgcErrorStatusMats[Controller][Channel].cpgcErrDat1S,
            CpgcErrorStatusMats[Controller][Channel].cpgcErrDat0S,
            CpgcErrorStatusMats[Controller][Channel].cpgcErrDat3S,
            CpgcErrorStatusMats[Controller][Channel].cpgcErrDat2S,
            CpgcErrorStatusMats[Controller][Channel].cpgcErrEccS
          );
        } else {
          MRC_DEBUG_MSG(&MrcData->Outputs.Debug, MSG_LEVEL_ERROR,
            "    cpgcErrEven = 0x%08x, cpgcErrOdd = 0x%08x, cpgcErrEccS = 0x%04x\n",
            CpgcErrorStatusMats[Controller][Channel].cpgcErrDat0S,
            CpgcErrorStatusMats[Controller][Channel].cpgcErrDat2S,
            CpgcErrorStatusMats[Controller][Channel].cpgcErrEccS
          );
        }
        MRC_DEBUG_MSG (&MrcData->Outputs.Debug, MSG_LEVEL_ERROR,
          "    ErrorSummaryA = 0x%x, ErrorSummaryB = 0x%x, ErrorSummaryC = 0x%x\n",
          ErrorSummary[0],
          ErrorSummary[1],
          ErrorSummary[2]);
      } else {
        MRC_DEBUG_MSG (&MrcData->Outputs.Debug, MSG_LEVEL_ERROR,
          "Mc%d.C%d: No errors found on last test run\n",
          Controller,
          Channel
        );
      }
#endif
    } // Channel
  } // Controller

  return Status;
} // CpgcAdvTrainingErrorStatusMATS

/**
  Checks the results of the preceding advanced memory test on Ddr5

  @param[in]  MrcData                 - Global MRC data structure
  @param[in]  McChBitMask             - Memory Controller Channel Bit mask to check results for.
  @param[in]  Rank                    - Rank to check results for
  @param[in]  RowBits                 - Number of row bits supported by current rank
  @param[in]  BaseBits                - Number of bank bits in SW loop
  @param[out] TestStatus              - Pass/fail status for the test per controller/channel
  @param[in]  Direction               - Sequential address direction MT_ADDR_DIR_UP, MT_ADDR_DIR_DN
  @param[in]  AmtRetryDisabled        - Indicates when the AMT Retry flag is disabled

  @retval status - mrcSuccess / mrcFail
**/
MrcStatus
CheckTestResultsMATS (
  IN  MrcParameters      *const MrcData,
  IN  UINT32                    McChBitMask,
  IN  UINT32                    Rank,
  IN  UINT8                     RowBits[MAX_CONTROLLER][MAX_CHANNEL],
  IN  UINT32                    BaseBits,
  OUT UINT8                     TestStatus[MAX_CONTROLLER][MAX_CHANNEL],
  IN  UINT8                     Direction,
  IN  BOOLEAN                   AmtRetryDisabled
  )
{
  MrcOutput                *Outputs;
  MrcDebug                 *Debug;
  MrcStatus                Status = mrcSuccess;
  MRC_FUNCTION             *MrcCall;
  UINT32                   ErrorStatusDatLo = 0;
  UINT32                   ErrorStatusDatHi = 0;
  UINT32                   ErrorStatusEcc = 0;
  UINT8                    Channel;
  UINT8                    Controller;
  CPGC_ERROR_STATUS_MATS   CpgcErrorStatus[MAX_CONTROLLER][MAX_CHANNEL];
  UINT8                    MaxChDdr;
  INT16                    DimmTemp[MAX_CONTROLLER][MAX_CHANNEL];
  BOOLEAN                  Ddr4;
  UINT32                   Ddr4Mask;
#ifdef MRC_DEBUG_PRINT
  const CHAR8              *DqText;
#endif

  Outputs = &MrcData->Outputs;
  Debug = &Outputs->Debug;
  MaxChDdr = Outputs->MaxChannels;
  MrcCall = MrcData->Inputs.Call.Func;
  MrcCall->MrcSetMem ((UINT8 *) DimmTemp, sizeof (DimmTemp), 0);
  Ddr4 = (Outputs->DdrType == MRC_DDR_TYPE_DDR4);
  if (Ddr4) {
    Ddr4Mask = 0xFFFFFFFF;
  } else {
    Ddr4Mask = 0;
  }
  //
  // Collect test results
  CpgcAdvTrainingErrorStatusMATS (MrcData, McChBitMask, CpgcErrorStatus);

  for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
    for (Channel = 0; Channel < MaxChDdr; Channel++) {
      if ((MC_CH_MASK_CHECK (McChBitMask, Controller, Channel, MaxChDdr)) == 0) {
        continue;
      }
      //InjectErrorTestResultsMATS (Host, Socket, Ch, LogicalRank, CpgcErrorStatus);

      // For DDR4, SubChB is the upper bits [63:32] of SubChA
      // DDR5 should not need SubChB since we represent each subchannel as a separate channel
      ErrorStatusDatLo = CpgcErrorStatus[Controller][Channel].cpgcErrDat0S | CpgcErrorStatus[Controller][Channel].cpgcErrDat2S;
      ErrorStatusDatHi = CpgcErrorStatus[Controller][Channel].cpgcErrDat1S | CpgcErrorStatus[Controller][Channel].cpgcErrDat3S;
      if (Outputs->EccSupport) {
        ErrorStatusEcc = (CpgcErrorStatus[Controller][Channel].cpgcErrEccS & MRC_UINT8_MAX);
      }

      // If any lane has errors, then read DIMM temperature
      DimmTemp[Controller][Channel] = 0;
      //if ((ErrorStatusDatLo | ErrorStatusEcc | (Ddr4Mask & ErrorStatusDatHi)) != 0) {
      //  // Then read current DIMM temperature
      //  // TSOD not present initializes DIMM temperature = 0
      //  GetDimmTemperature (Socket, Channel, DimmRank[LogicalRank][Channel], &DimmTemp[Channel]);
      //}
    }
  }

  // Update Row Range Failure tracking
  Status = UpdateRowFailures (MrcData, McChBitMask, CpgcErrorStatus, DimmTemp, RowBits, BaseBits, TestStatus, Direction, AmtRetryDisabled);

  for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
    for (Channel = 0; Channel < MaxChDdr; Channel++) {

      // Was this channel part of the test group?
      if ((MC_CH_MASK_CHECK (McChBitMask, Controller, Channel, MaxChDdr)) == 0) {
        continue;
      }
      // Evaluate error status
      // Do not log failures on 1st try
      ErrorStatusDatLo = 0;
      ErrorStatusDatHi = 0;
      ErrorStatusEcc = 0;

      if (Outputs->RetryState) {
        ErrorStatusDatLo = CpgcErrorStatus[Controller][Channel].cpgcErrDat0S | CpgcErrorStatus[Controller][Channel].cpgcErrDat2S;
        ErrorStatusDatHi = CpgcErrorStatus[Controller][Channel].cpgcErrDat1S | CpgcErrorStatus[Controller][Channel].cpgcErrDat3S;
        if (Outputs->EccSupport) {
          ErrorStatusEcc = (CpgcErrorStatus[Controller][Channel].cpgcErrEccS & MRC_UINT8_MAX);
        }
      }

#ifdef MRC_DEBUG_PRINT
      if (Ddr4) {
        DqText = "DQ[71:0]";
      } else {
        DqText = "DQ[39:0]";
      }
      //
      // Check for any bit errors on any lane on SubCh-A.
      // If DDR4, SubCh-B contains bits [63:32] of SubCh-A.
      //
      if ((ErrorStatusDatLo | ErrorStatusEcc | (Ddr4Mask & ErrorStatusDatHi)) != 0) {
        // TODO: Review for enabling LP4/LP5
        MRC_DEBUG_MSG (Debug, MSG_LEVEL_ERROR, "Rank = %d, Bank = %d, Row = 0x%x, Column = 0x%x, Chunk = 0x%x\n",
          CpgcErrorStatus[Controller][Channel].cpgcErrRank & MRC_UINT16_MAX,
          CpgcErrorStatus[Controller][Channel].cpgcErrBank & MRC_UINT16_MAX, CpgcErrorStatus[Controller][Channel].cpgcErrRow,
          CpgcErrorStatus[Controller][Channel].cpgcErrColumn, CpgcErrorStatus[Controller][Channel].cpgcErrChunk & MRC_UINT16_MAX);
        MRC_DEBUG_MSG (Debug, MSG_LEVEL_ERROR, "%s Error on rising strobe: 0x%02x",
          DqText,
          CpgcErrorStatus[Controller][Channel].cpgcErrEccS
        );
        if (Ddr4) {
          MRC_DEBUG_MSG(Debug, MSG_LEVEL_ERROR, "%08x", CpgcErrorStatus[Controller][Channel].cpgcErrDat1S);
        }
        MRC_DEBUG_MSG(Debug, MSG_LEVEL_ERROR, "%08x\n", CpgcErrorStatus[Controller][Channel].cpgcErrDat0S);

        MRC_DEBUG_MSG (Debug, MSG_LEVEL_ERROR, "%s Error on falling strobe: 0x%02x",
          DqText,
          CpgcErrorStatus[Controller][Channel].cpgcErrEccS
        );
        if (Ddr4) {
          MRC_DEBUG_MSG(Debug, MSG_LEVEL_ERROR, "%08x", CpgcErrorStatus[Controller][Channel].cpgcErrDat3S);
        }
        MRC_DEBUG_MSG(Debug, MSG_LEVEL_ERROR, "%08x\n", CpgcErrorStatus[Controller][Channel].cpgcErrDat2S);
      } // if ErrorStatus on SubCh-A
#endif
    } // Channel loop
  } // Controller loop

  return Status;
} // CheckTestResultsMATS

/**
  Executes a step of Advanced Memory test on given row address, size, unmasked DQ lanes and logs results.

  @param[in] MrcData                          - Global MRC data structure
  @param[in] McChBitMask                      - Memory Controller Channel Bit mask to read results for.
  @param[in] Rank                             - Pointer to return bitmask of channels to test next time
  @param[in] ColumnBits                       - number of column bits per logical rank
  @param[in] RowBits                          - number of row bits per logical rank
  @param[in] BankBits                         - number of bank bits
  @param[in] CmdPat                           - Type of sequence MT_CPGC_WRITE, MT_CPGC_READ, or MT_CPGC_READ_WRITE
  @param[in] Direction                        - Sequential address direction MT_ADDR_DIR_UP, MT_ADDR_DIR_DOWN
  @param[in] Bank                             - Current bank address
  @param[in] BaseBits                         - Number of least significant bank bits used in SW loop
  @param[in] BaseRow                          - Row address to start test
  @param[in] RangeSize                        - Row range size to test
  @param[in] DeviceMask                       - Bit mask of DQ lanes to exclude from test results
  @param[in] TestStatus                       - Pass/fail status for the test per channel
  @param[in] seqDataInv                       - Specifies whether data pattern should be inverted per subsequence
  @param[in] IsUseInvtPat                     - Info to indicate whether or not patternQW is inverted by comparing original pattern

  @retval status - mrcSuccess / mrcFail
**/
MrcStatus
CpgcMemTestRowRange (
  MrcParameters   *const    MrcData,
  UINT32                    McChBitMask,
  UINT32                    Rank,
  UINT8                     ColumnBits[MAX_RANK_IN_CHANNEL][MAX_CONTROLLER][MAX_CHANNEL],
  UINT8                     RowBits[MAX_RANK_IN_CHANNEL][MAX_CONTROLLER][MAX_CHANNEL],
  UINT8                     BankBits[MAX_RANK_IN_CHANNEL][MAX_CONTROLLER][MAX_CHANNEL],
  UINT8                     CmdPat,
  UINT8                     Direction,
  UINT32                    Bank,
  UINT32                    BaseBits,
  UINT32                    BaseRow[MAX_CONTROLLER][MAX_CHANNEL],
  UINT32                    RangeSize[MAX_CONTROLLER][MAX_CHANNEL],
  UINT32                    DeviceMask[MAX_CONTROLLER][MAX_CHANNEL][3],
  UINT8                     TestStatus[MAX_CONTROLLER][MAX_CHANNEL],
  UINT8                     SeqDataInv[2],
  BOOLEAN                   IsUseInvtPat
  )
{
  MrcOutput       *Outputs;
  MRC_FUNCTION    *MrcCall;
  UINT8           Channel;
  UINT8           Controller;
  INT32           i;
  UINT8           done;
  UINT32          TempRow[MAX_CONTROLLER][MAX_CHANNEL];
  UINT32          TempSize[MAX_CONTROLLER][MAX_CHANNEL];
  UINT32          RemSize[MAX_CONTROLLER][MAX_CHANNEL];
  BOOLEAN         secondPass;
  ROW_FAIL_RANGE  LastFail[MAX_CONTROLLER][MAX_CHANNEL];
  UINT32          TestRow[MAX_CONTROLLER][MAX_CHANNEL];
  UINT32          TestSize[MAX_CONTROLLER][MAX_CHANNEL];
  UINT8           LineMask[MAX_CONTROLLER][MAX_CHANNEL];
  UINT8           StopOnErr[MAX_CONTROLLER][MAX_CHANNEL];
  UINT8           TestMode;
  UINT8           RetryState;
  UINT8           GlobalTestStatus;
  UINT32          TempRankChEnMap;
  UINT8           MaxChannels;
  UINT32          FailingChannelMask = 0;
  UINT32          TempFailingChannelMask = 0;
  UINT32          PassingChannelMask = 0;
  UINT8           BgInterleave;

  MrcCall = MrcData->Inputs.Call.Func;

  MrcCall->MrcSetMem ((UINT8 *)TestSize, sizeof (TestSize), 0);
  MrcCall->MrcSetMem ((UINT8 *)TempRow,  sizeof (TempRow), 0);
  MrcCall->MrcSetMem ((UINT8 *)RemSize,  sizeof (RemSize), 0);
  MrcCall->MrcSetMem ((UINT8 *)TestRow,  sizeof (TestRow), 0);
  MrcCall->MrcSetMem ((UINT8 *)TempSize, sizeof (TempSize), 0);
  MrcCall->MrcSetMem ((UINT8 *)LineMask, sizeof (LineMask), 0);

  secondPass = FALSE;
  TestMode = CmdPat;

  Outputs = &MrcData->Outputs;
  MaxChannels = Outputs->MaxChannels;
  TempRankChEnMap = McChBitMask;

  if (GetDefaultBankGroupInterleave(MrcData, &BgInterleave) != mrcSuccess) {
    BgInterleave = 8;
  }

  for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
    for (Channel = 0; Channel < MaxChannels; Channel++) {
      if ((MC_CH_MASK_CHECK (McChBitMask , Controller, Channel, MaxChannels) == 0)) {
        continue;
      }
      TempRow[Controller][Channel] = BaseRow[Controller][Channel];
      RemSize[Controller][Channel] = RangeSize[Controller][Channel];
      if (RangeSize[Controller][Channel] == 0) {
        TempRankChEnMap &= ~(1 << ((Controller * MaxChannels) + Channel));
      }
    }
  }

  do {
    RetryState = Outputs->RetryState;
    for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
      for (Channel = 0; Channel < MaxChannels; Channel++) {
        if ((MC_CH_MASK_CHECK (TempRankChEnMap, Controller, Channel, MaxChannels) == 0)) {
          continue;
        }
        TempSize[Controller][Channel] = 0;
        for (i = 31; i >= 0; i--) {
          if (RemSize[Controller][Channel] & (1 << i)) {
            TempSize[Controller][Channel] = (1 << i);
            break;
          }
        }
        if (RetryState == 0) {
          if (Direction == MT_ADDR_DIR_DOWN) {
            TempRow[Controller][Channel] -= TempSize[Controller][Channel] - 1;
          }
        }
        // If retry 0, then check error of all interleave bank groups.
        // Else if retry 1, then test LastFail row by doing MT_CPGC_READ mode and masking cacheline associated with bank group 0
        // Else if retry 2, then test LastFail row by doing MT_CPGC_READ mode and masking cacheline associated with bank group 1
        // ...
        // Else if retry 8, then test LastFail row by doing MT_CPGC_READ mode and masking cacheline associated with bank group 7
        LastFail[Controller][Channel] = Outputs->LastFail[Controller][Channel];

        // Legacy Memtest accumulates DQ errors; Advanced Memtest stops on 1st error
        StopOnErr[Controller][Channel] = 1;

        if (RetryState) {
          // RetryState > 0 will do a read of the failing bank pair and attempt to isolate the failure to a specific BG by using cache line mask feature
          // The failing BG will be saved in BankGroupMask bit-field of the ROW_FAIL_RANGE
          // If the reads do not show error, then we assume a transient read error occured and the original failure will not be logged.
          TestRow[Controller][Channel] = LastFail[Controller][Channel].Addr.Bits.Row;
          TestMode = PatRd;
          if ((LastFail[Controller][Channel].Addr.Bits.Valid) && (MC_CH_MASK_CHECK (PassingChannelMask, Controller, Channel, MaxChannels) == 0)) {
            TestSize[Controller][Channel] = 1;
          } else {
            TestSize[Controller][Channel] = 0;
          }

          // Adjust the cacheline mask, so that regardless the direction, the retry 1 always checks the bank group 0,
          // retry 2 checks bank group 1, and so on
          // Direction up:
          // RetryState 1: 0x01
          // RetryState 2: 0x02
          // ...
          // RetryState 8: 0x80
          //
          // Direction down:
          // RetryState 1: 0x80
          // RetryState 2: 0x40
          // ...
          // RetryState 8: 0x01

          LineMask[Controller][Channel] = GetCachelineMask (RetryState, Direction, BgInterleave);
        } else {
          TestRow[Controller][Channel] = TempRow[Controller][Channel];
          TestSize[Controller][Channel] = TempSize[Controller][Channel];
          TestMode = CmdPat;
          LineMask[Controller][Channel] = 0xFF;
        }// RetryState
      } // Channel
    } // Controller

    // Mask off channels that are not being tested
    TempRankChEnMap = McChBitMask;
    for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
      for (Channel = 0; Channel < MaxChannels; Channel++) {
        if (MC_CH_MASK_CHECK (TempRankChEnMap, Controller, Channel, MaxChannels) == 0) {
          continue;
        }
        if (TestSize[Controller][Channel] == 0) {
          TempRankChEnMap &= ~(1 << ((Controller * MaxChannels) + Channel));
          TempRankChEnMap &= ~FailingChannelMask;
          TempRankChEnMap &= ~PassingChannelMask;
        }
      }
    }

    CpgcMemTestRankSetupMATSRowRange (MrcData, TempRankChEnMap, Rank, ColumnBits[Rank], RowBits[Rank], BankBits[Rank],
      TestMode, Direction, Bank, BaseBits, TestRow, TestSize, DeviceMask, LineMask, StopOnErr, SeqDataInv, IsUseInvtPat);

    RunIOTest (MrcData, (UINT8) TempRankChEnMap, StaticPattern, 1, 0);

    // For READ test must check for failures
    if (CmdPat == PatRdWr || CmdPat == PatRd) {
      // Advanced Memtest error handling path
      if (CheckTestResultsMATS (MrcData, TempRankChEnMap, Rank, RowBits[Rank], BaseBits, TestStatus, Direction, FALSE) == mrcFail) {
        // Failure means that row fail range list is full
        Outputs->RetryState = 0;
        return mrcFail;
      }

      GlobalTestStatus = 0;
      for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
        for (Channel = 0; Channel < MaxChannels; Channel++) {
          if ((MC_CH_MASK_CHECK (TempRankChEnMap, Controller, Channel, MaxChannels) == 0) || (TestStatus[Controller][Channel] == 0)) { // Test Status = 0 is a Pass
            continue;
          }
          GlobalTestStatus = 1; // 1 indicates failure happened
          if (Outputs->RetryState) {
            // Retry detected failure. Caller restarts test at LastFail row, so do not continue testing this channel
            RemSize[Controller][Channel] = 0;
          }
        } // channel
      } // controller

      // Read Retry FSM
      // 0 no error -> 0
      // 0 error    -> 1 read retry of 1st BG
      // 1          -> 2 read retry of 2nd BG
      // 2          -> 3 read retry of 2rd BG
      // ...
      // 7          -> 8 read retry of 8th BG
      // 8 error    -> 0, Fail list updated, return and caller to restart at LastFail row
      // 8 no error -> 0, Fail list not updated, return and caller to restart at LastFail row
      //
      if (RetryState > 0) {
        for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
          for (Channel = 0; Channel < MaxChannels; Channel++) {
            if ((MC_CH_MASK_CHECK (TempRankChEnMap, Controller, Channel, MaxChannels) == 0) || (TestStatus[Controller][Channel] == 0)) {
              continue;
            }
            TempFailingChannelMask = (1 << ((Controller * MaxChannels) + Channel));
          } // channel
        } // controller
      } // Retrystate > 0
      if (RetryState == BgInterleave) {
        FailingChannelMask = TempFailingChannelMask;

        // Reset Passing Channel Mask to continue non 2^N range
        PassingChannelMask = 0;

        // Reset retry state
        Outputs->RetryState = 0;
      } else if (RetryState || GlobalTestStatus) {
        // Mask off the passed channel to avoid run into retry FSM
        if ((RetryState == 0) && (GlobalTestStatus != 0)) {
          for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
            for (Channel = 0; Channel < MaxChannels; Channel++) {
              if ((MC_CH_MASK_CHECK (TempRankChEnMap, Controller, Channel, MaxChannels) != 0) && (TestStatus[Controller][Channel] == 0)) {
                PassingChannelMask |= (1 << ((Controller * MaxChannels) + Channel));
              }
            } // channel
          } // controller
        }

        // Move to next retry state
        Outputs->RetryState++;
      } // RetryState
    } // if READ

    if (Outputs->RetryState > 0) {
      done = 0;
    } else {
      done = 1;
      for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
        for (Channel = 0; Channel < MaxChannels; Channel++) {
          if (MC_CH_MASK_CHECK (McChBitMask, Controller, Channel, MaxChannels) == 0) {
            continue;
          }
          if (secondPass == TRUE) {
            // Retry the same range
            done = 0;
          } else if (RemSize[Controller][Channel] > 0) {
            // Move to next 2^n range until done
            if (Direction == MT_ADDR_DIR_UP) {
              TempRow[Controller][Channel] += TempSize[Controller][Channel];
              if (TempRow[Controller][Channel] < BaseRow[Controller][Channel] + RangeSize[Controller][Channel]) {
                done = 0;
              }
            } else {
              TempRow[Controller][Channel]--;       // this could result in -1
              if ((INT32) TempRow[Controller][Channel] > (INT32) (BaseRow[Controller][Channel] - RangeSize[Controller][Channel])) {
                done = 0;
              }
            }
            RemSize[Controller][Channel] &= ~TempSize[Controller][Channel];
          } // secondPass
        } // channel
      } // controller
    }
  } while (!done);

  return mrcSuccess;

} //CpgcMemTestRowRange

/**
  Cpgc command pattern setup up for memory test

  @param[in] MrcData               - Pointer to MRC global data.
  @param[in] Controller            - Controller to setup
  @param[in] Channel               - Channel to setup
  @param[in] RwMode                - Cpgc read and write mode
  @param[in] UseSubSeq1            - Cpgc subseq1 usage
  @param[in] Direction             - Address direction during memory test
  @param[in] SeqDataInv            - Enables pattern inversion per subsequence
  @param[in] IsUseInvtPat          - Info to indicate whether or not patternQW is inverted by comparing original pattern

  @retval n/a
**/
VOID
CpgcMemTestCmdPatSetup (
  IN MrcParameters  *const MrcData,
  IN UINT8                 Controller,
  IN UINT8                 Channel,
  IN UINT8                 RwMode,
  IN BOOLEAN               UseSubSeq1,
  IN UINT8                 Direction,
  IN UINT8                 SeqDataInv[2],
  IN BOOLEAN               IsUseInvtPat
  )
{
  UINT8          CmdPat;
  UINT16         BurstWait;
  TAddressCarry  AddressCarry;

  AddressCarry.AddressDirection = CPGC20_FAST_Y;
  AddressCarry.AddressOrder     = CPGC20_BANK_2_ROW_COL_2_RANK; // Default to set bank interleave for good performance

  BurstWait = 0;

  if (UseSubSeq1 == CPGC_NO_SUBSEQ1) { // Only read or write
    if (RwMode == CPGC_BASE_READ_SUBSEQ) { // Read only
      if (IsUseInvtPat) {
        if (Direction == MT_ADDR_DIR_UP) { // ^(rI)
          CmdPat = PatRdUpInvt;
        } else { // v(rI)
          CmdPat = PatRdDownInvt;
        }
      } else {
        if (Direction == MT_ADDR_DIR_UP) { // ^(rD)
          CmdPat = PatRdUp;
        } else { // v(rD)
          CmdPat = PatRdDown;
        }
      }
    } else { // Write only
      if (IsUseInvtPat) {
        if (Direction == MT_ADDR_DIR_UP) { // ^(wI)
          CmdPat = PatWrUpInvt;
        } else { // v(wI)
          CmdPat = PatWrDownInvt;
        }
      } else {
        if (Direction == MT_ADDR_DIR_UP) { // ^(wD)
          CmdPat = PatWrUp;
        } else { // v(wD)
          CmdPat = PatWrDown;
        }
      }
    }
  } else { // Read and then Write
    if (IsUseInvtPat) {
      if (Direction == MT_ADDR_DIR_UP) {
        if (SeqDataInv[1] == 1) { // ^(rI, wD)
          CmdPat = PatRdWrUpInvtAlt;
        } else { // ^(rI, wI)
          CmdPat = PatRdWrUpInvt;
        }
      } else {
        if (SeqDataInv[1] == 1) { // v(rI, wD)
          CmdPat = PatRdWrDownInvtAlt;
        } else { // v(rI, wI)
          CmdPat = PatRdWrDownInvt;
        }
      }
    } else {
      if (Direction == MT_ADDR_DIR_UP) {
        if (SeqDataInv[1] == 1) { // ^(rD, wI)
          CmdPat = PatRdWrUpAlt;
        } else { // ^(rD, wD)
          CmdPat = PatRdWrUp;
        }
      } else {
        if (SeqDataInv[1] == 1) { // v(rD, wI)
          CmdPat = PatRdWrDownAlt;
        } else { // v(rD, wD)
          CmdPat = PatRdWrDown;
        }
      }
    }
  }
  SetCpgcCmdPat (MrcData, Controller, Channel, CmdPat, &AddressCarry, BurstWait);
}
