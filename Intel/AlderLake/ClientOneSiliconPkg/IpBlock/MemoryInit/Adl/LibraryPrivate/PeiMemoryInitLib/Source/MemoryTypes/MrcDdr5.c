/** @file
  Implementation of DDR5 Specific functions, and constants.

@copyright
  INTEL CONFIDENTIAL
  Copyright 2019 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains an 'Intel Peripheral Driver' and is uniquely identified as
  "Intel Reference Module" and is licensed for Intel CPUs and chipsets under
  the terms of your license agreement with Intel or your vendor. This file may
  be modified by the user, subject to additional terms of the license agreement.

@par Specification Reference:
  DDR5 JEDEC Spec
**/

#include "MrcInterface.h"
#include "MrcDdr5.h"
#include "MrcDdrCommon.h"
#include "MrcHalRegisterAccess.h"
#include "MrcApi.h"
#include "MrcCommon.h"
#include "MrcChipApi.h"
#include "MrcDdr5Registers.h"
#include "MrcMemoryApi.h"
#include "MrcMaintenance.h"
#include "MrcPmic.h"


///
/// Initialization Timing Parameters
///
#define MRC_DDR5_tINIT0_MS   20      ///< Max voltage-ramp time
#define MRC_DDR5_tINIT1_US   200     ///< Min RESET_n LOW time after completion of voltage Ramp
#define MRC_DDR5_tINIT2_NS   10      ///< Min CS_n LOW time before RESET_n HIGH
#define MRC_DDR5_tINIT3_MS   4       ///< Min CS_n LOW time after RESET_n HIGH
#define MRC_DDR5_tINIT4_US   2       ///< Minimum time for DRAM to register EXIT on CS_n HIGH
#define MRC_DDR5_tINIT5_NCK  3       ///< Minimum cycles required after CS_n HIGH

//--------------- DDR5 -----------------------------------------
// DDR5 DQ ODT values are in this order: { RttWr, RTT_NOM_RD, RTT_NOM_WR, RttPark, RttParkDqs }
// Note: RttWr   0xFFFF denotes Hi-Z
//               0      denotes disabled
//
//       RttPark, RttNom may be disabled by setting 0, there is no Hi-Z value.
//
// 1DPC
GLOBAL_REMOVE_IF_UNREFERENCED const TOdtValueDqDdr5 Ddr5DqOdtTable1Dpc[MAX_DIMMS_IN_CHANNEL][2] = {
  /// 1DPC 1R,             1DPC 2R
  {{ 40, 0, 0, 40, 40 }, { 240, 0, 0, 34, 34 }}, // DIMM 0
  {{ 40, 0, 0, 40, 40 }, { 240, 0, 0, 34, 34 }}  // DIMM 1
};

// 2DPC
GLOBAL_REMOVE_IF_UNREFERENCED const TOdtValueDqDdr5 Ddr5OdtTable2Dpc[MAX_DIMMS_IN_CHANNEL][oiNotValid] = {
  /// 2DPC 0R/1R,     2DPC 0R/2R,       2DPC 1R 1R,           2DPC 1R 2R,          2DPC 2R 1R,         2DPC 2R 2R
  { { 40,0,0,40,40 }, { 240,0,0,40,40 },{ 60,34,40,48,48 }, { 120,60,60,60,60 }, { 120,48,48,48,48 }, { 240,48,48,60,60 } }, // DIMM 0
  { { 40,0,0,40,40 }, { 240,0,0,40,40 },{ 60,40,34,34,34 }, { 120,60,60,60,60 }, { 120,48,48,48,48 }, { 240,48,48,60,60 } }  // DIMM 1
};
// DDR5 CCC ODT values are in this order { RttCa, RttCs, RttCk }
// 1DPC
GLOBAL_REMOVE_IF_UNREFERENCED const TOdtValueCccDdr5 Ddr5CccOdtTable1Dpc[MAX_DIMMS_IN_CHANNEL][2][2] = {
  //  1DPC 1R,                      1DPC 2R
  //  Group A,    Group B           Group A,    Group B
  {{{ 0, 0, 0 },{ 40, 40, 40 }}, {{ 0, 0, 0 },{ 80, 40, 40 }}}, // DIMM 0
  {{{ 0, 0, 0 },{ 40, 40, 40 }}, {{ 0, 0, 0 },{ 80, 40, 40 }}}  // DIMM 1
};

// 2DPC
GLOBAL_REMOVE_IF_UNREFERENCED const TOdtValueCccDdr5 Ddr5CccOdtTable2Dpc[MAX_DIMMS_IN_CHANNEL][oiNotValid][2] = {
  //  2DPC 0R/1R,                   2DPC 0R/2R,                   2DPC 1R 1R,                     2DPC 1R 2R,                    2DPC 2R 1R,                    2DPC 2R 2R
  //  Group A,    Group B           Group A,    Group B           Group A,      Group B           Group A,    Group B            Group A,    Group B            Group A,      Group B
  {{{ 0, 0, 0 },{ 40, 40, 40 }}, {{ 0, 0, 0 },{ 80, 40, 40 }}, {{ 240, 0, 0 },{ 40, 40, 40 }}, {{ 0, 0, 0 },{ 60, 40, 40 }}, {{ 0, 0, 0 },{ 60, 40, 40 }}, {{ 0, 0, 0 },{ 60, 40, 40 }}}, // DIMM 0
  {{{ 0, 0, 0 },{ 40, 40, 40 }}, {{ 0, 0, 0 },{ 80, 40, 40 }}, {{ 240, 0, 0 },{ 40, 40, 40 }}, {{ 0, 0, 0 },{ 60, 40, 40 }}, {{ 0, 0, 0 },{ 60, 40, 40 }}, {{ 0, 0, 0 },{ 60, 40, 40 }}}  // DIMM 1
};

GLOBAL_REMOVE_IF_UNREFERENCED const TOdtValueCccDdr5 Ddr5CccOdtTable2DpcG0[MAX_DIMMS_IN_CHANNEL][oiNotValid][2] = {
  //  2DPC 0R/1R,                   2DPC 0R/2R,                   2DPC 1R 1R,                     2DPC 1R 2R,                    2DPC 2R 1R,                    2DPC 2R 2R
  //  Group A,    Group B           Group A,    Group B           Group A,      Group B           Group A,    Group B            Group A,    Group B            Group A,      Group B
  {{{ 0, 0, 0 },{ 40, 40, 40 }}, {{ 0, 0, 0 },{ 80, 40, 40 }}, {{ 480, 0, 0 },{ 40, 40, 40 }}, {{ 0, 0, 0 },{ 120, 40, 40 }}, {{ 0, 0, 0 },{ 120, 40, 40 }}, {{ 480, 0, 0 },{ 80, 40, 40 }}}, // DIMM 0
  {{{ 0, 0, 0 },{ 40, 40, 40 }}, {{ 0, 0, 0 },{ 80, 40, 40 }}, {{ 480, 0, 0 },{ 40, 40, 40 }}, {{ 0, 0, 0 },{ 120, 40, 40 }}, {{ 0, 0, 0 },{ 120, 40, 40 }}, {{ 480, 0, 0 },{ 80, 40, 40 }}}  // DIMM 1
};

typedef enum {
  GenericFsmConfigNonPdaMrs = 0,
  GenericFsmConfigPdaMrs
} GenericFsmConfigMode;

/**
  Override CS to the input CsOverrideVal value.

  @param[in] MrcData       - Pointer to MRC global data.
  @param[in] CsOverrideVal - Value to override CS to. Any non-zero value sets CS high.
  @param[in] CsOverrideEn  - Input specifying whether to enable or disable CS Override.

  @retval N/A
**/
void
OverrideCs (
  IN MrcParameters *const MrcData,
  IN UINT8                CsOverrideVal,
  IN BOOLEAN              CsOverrideEn
  )
{
  MrcOutput     *Outputs;
  INT64         ValidRankBitMask;
  INT64         OverrideValRankBitMask;
  INT64         OverrideEnRankBitMask;
  INT32         Channel;
  UINT32        Controller;

  Outputs = &MrcData->Outputs;

  for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
    for (Channel = 0; Channel < MAX_CHANNEL; Channel++) {
      if (MrcChannelExist (MrcData, Controller, Channel)) {
        ValidRankBitMask = Outputs->Controller[Controller].Channel[Channel].ValidRankBitMask;
        OverrideValRankBitMask = (CsOverrideVal == 0) ? 0 : ValidRankBitMask;
        OverrideEnRankBitMask = (CsOverrideEn == FALSE) ? 0 : ValidRankBitMask;
        MrcGetSetMcCh (MrcData, Controller, Channel, GsmMccCsOverrideVal, WriteToCache, &OverrideValRankBitMask);
        MrcGetSetMcCh (MrcData, Controller, Channel, GsmMccCsOverride, WriteToCache, &OverrideEnRankBitMask);
      }
    } // Channel
  } // Controller
  MrcFlushRegisterCachedData (MrcData);
}


/**
  This function converts from the integer defined Read Latency to the Mode Register (MR0)
  encoding of the timing in DDR5.

  @param[in]  MrcData - Pointer to global MRC data.
  @param[in]  Value   - Requested Read Latency value.
  @param[out] EncVal  - Encoded Mode Register value.

  @retval MrcStatus - mrcSuccess if the latency is supported.  Else mrcWrongInputParameter.
**/
MrcStatus
EncodeReadLatencyDdr5 (
  IN  MrcParameters *MrcData,
  IN  UINT16        Value,
  OUT UINT8         *EncVal
  )
{
  MrcDebug    *Debug;
  MrcStatus   Status;
  DDR5_MR0_RL MrValue;

  Debug = &MrcData->Outputs.Debug;
  Status = mrcSuccess;

  if (Value < 22 ||  Value > 66 || (Value %2 != 0)) {
    MrValue = Ddr5RlMax;
  } else {
    MrValue = (Value - 22) / 2;
  }

  if (MrValue < Ddr5Rl_22 || MrValue >= Ddr5RlMax) {
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_ERROR, "%s Invalid %s Latency Value: %d\n", gErrString, gRdString, Value);
    Status = mrcWrongInputParameter;
  }


  if (EncVal != NULL) {
    *EncVal = MrValue;
  } else {
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_ERROR, "%s %s\n", __FUNCTION__, gNullPtrErrStr);
    Status = mrcWrongInputParameter;
  }

  return Status;
}

/**
  This function converts from the integer defined Write Recovery to the Mode Register (MR6)
  encoding of the timing in DDR5.

  @param[in]  MrcData - Pointer to global MRC data.
  @param[in]  Value   - Requested Write Recovery value.
  @param[out] EncVal  - Encoded Mode Register value.

  @retval MrcStatus - mrcSuccess if the latency is supported.  Else mrcWrongInputParameter.
**/
MrcStatus
EncodeWriteRecoveryDdr5 (
  IN  MrcParameters *MrcData,
  IN  UINT16        Value,
  OUT UINT8         *EncVal
  )
{
  MrcDebug    *Debug;
  MrcStatus   Status;
  DDR5_MR6_WR MrValue;

  Debug = &MrcData->Outputs.Debug;
  Status = mrcSuccess;

  if (Value < 48 || Value > 96 || (Value % 6 != 0)) {
    MrValue = Ddr5WrMax;
  } else {
    MrValue = (Value - 48) / 6;
  }

  if (MrValue < Ddr5Wr_48 || MrValue >= Ddr5WrMax) {
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_ERROR, "%s Unsupported Write Recovery value\n", gErrString);
    Status = mrcWrongInputParameter;
  }

  if (EncVal != NULL) {
    *EncVal = MrValue;
  } else {
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_ERROR, "%s %s\n", __FUNCTION__, gNullPtrErrStr);
    Status = mrcWrongInputParameter;
  }

  return Status;
}


/**
  This function converts from the integer defined tRTP timing to the Mode Register (MR6)
  encoding of the timing in DDR5.

  @param[in]  MrcData - Pointer to global MRC data.
  @param[in]  Value   - Requested tRTP value.
  @param[out] EncVal  - Encoded Mode Register value.

  @retval MrcStatus - mrcSuccess if the timing is supported.  Else mrcWrongInputParameter.
**/
MrcStatus
EncodeTrtpDdr5 (
  IN  MrcParameters *MrcData,
  IN  UINT16        Value,
  OUT UINT8         *EncVal
  )
{
  MrcDebug      *Debug;
  MrcStatus     Status;
  DDR5_MR6_TRTP MrValue;

  Debug = &MrcData->Outputs.Debug;
  Status = mrcSuccess;


  switch (Value) {
    case 12:
      MrValue = Ddr5Trtp_12;
      break;

    case 14:
      MrValue = Ddr5Trtp_14;
      break;

    case 15:
      MrValue = Ddr5Trtp_15;
      break;

    case 17:
      MrValue = Ddr5Trtp_17;
      break;

    case 18:
      MrValue = Ddr5Trtp_18;
      break;

    case 20:
      MrValue = Ddr5Trtp_20;
      break;

    case 21:
      MrValue = Ddr5Trtp_21;
      break;

    case 23:
      MrValue = Ddr5Trtp_23;
      break;

    case 24:
      MrValue = Ddr5Trtp_24;
      break;

    default:
      MrValue = Ddr5TrtpMax;
      Status = mrcWrongInputParameter;
      break;
  }

  if (MrValue < Ddr5TrtpMax) {
    if (EncVal != NULL) {
      *EncVal = MrValue;
    } else {
      MRC_DEBUG_MSG (Debug, MSG_LEVEL_ERROR, "%s %s\n", __FUNCTION__, gNullPtrErrStr);
      Status = mrcWrongInputParameter;
    }
  } else {
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_ERROR, "%s Invalid tRTP Value: %d\n", gErrString, Value);
    Status = mrcWrongInputParameter;
  }

  return Status;
}

/**
  This function returns the tCCD_L/tCCD_L_WR/tDLLK MR13 encoded value
  for the input frequency value.

  @param[in]  MrcData   - Pointer to global MRC data.
  @param[in]  Frequency - Data Rate
  @param[out] EncVal    - Encoded Mode Register value.

  @retval MrcStatus - mrcSuccess if the timing is supported.  Else mrcWrongInputParameter.
**/
MrcStatus
EncodeDdr5TccdlTdllk (
  IN  MrcParameters *MrcData,
  IN  MrcFrequency  Frequency,
  OUT UINT8         *EncVal
  )
{
  MrcStatus Status;
  MrcDebug  *Debug;
  UINT8     MrValue;

  Debug = &MrcData->Outputs.Debug;
  Status = mrcSuccess;

  if (Frequency <= f3200) {
    MrValue = 0;
  } else if ((f3200 < Frequency) && (Frequency <= f3600)) {
    MrValue = 1;
  } else if ((f3600 < Frequency) && (Frequency <= f4000)) {
    MrValue = 2;
  } else if ((f4000 < Frequency) && (Frequency <= f4400)) {
    MrValue = 3;
  } else if ((f4400 < Frequency) && (Frequency <= f4800)) {
    MrValue = 4;
  } else if ((f4800 < Frequency) && (Frequency <= f5200)) {
    MrValue = 5;
  } else if ((f5200 < Frequency) && (Frequency <= f5600)) {
    MrValue = 6;
  } else if ((f5600 < Frequency) && (Frequency <= f6000)) {
    MrValue = 7;
  } else if ((f6000 < Frequency) && (Frequency <= f6400)) {
    MrValue = 8;
  } else {
    MrValue = 0xFF;
  }

  if (MrValue != 0xFF) {
    if (EncVal != NULL) {
      *EncVal = MrValue;
    } else {
      MRC_DEBUG_MSG (Debug, MSG_LEVEL_ERROR, "%s %s\n", __FUNCTION__, gNullPtrErrStr);
      Status = mrcWrongInputParameter;
    }
  } else {
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_ERROR, "%s Invalid Frequency Value: %d\n", gErrString, Frequency);
    Status = mrcWrongInputParameter;
  }

  return Status;
}

/**
  This function returns the Read Preamble Setting MR8 encoded value.

  @param[in]  MrcData   - Pointer to global MRC data.
  @param[out] EncVal    - Encoded Mode Register value.

  @retval MrcStatus - mrcSuccess if the timing is supported.  Else mrcWrongInputParameter.
**/
MrcStatus
MrcDdr5GetReadPreambleSetting (
  IN  MrcParameters  *MrcData,
  OUT DDR5_MR8_TRPRE *EncVal
  )
{
  MrcStatus      Status;
  MrcDebug       *Debug;
  UINT32         tRPRE;
  DDR5_MR8_TRPRE ReadPreambleSettings;

  Debug  = &MrcData->Outputs.Debug;
  Status = mrcSuccess;

  tRPRE = MrcGetRpre (MrcData);
  if (tRPRE == 1) {
    ReadPreambleSettings = Ddr5tRPRE_1tCK_10;
  } else if (tRPRE == 2) {
    ReadPreambleSettings = (MrcData->Outputs.RxMode == MrcRxModeMatchedN) ? Ddr5tRPRE_2tCK_1110 :Ddr5tRPRE_2tCK_0010;
  } else if (tRPRE == 3) {
    ReadPreambleSettings = Ddr5tRPRE_3tCK_000010;
  } else if (tRPRE == 4) {
    ReadPreambleSettings = Ddr5tRPRE_4tCK_00001010;
  } else {
    ReadPreambleSettings = 0;
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_ERROR, "%s Invalid tRPRE %d\n", gErrString, tRPRE);
    Status = mrcWrongInputParameter;
  }

  if (Status == mrcSuccess && EncVal != NULL) {
    *EncVal = ReadPreambleSettings;
  }

  return Status;
}

/**
  This function returns the Write Preamble Setting MR8 encoded value.

  @param[in]  MrcData   - Pointer to global MRC data.
  @param[out] EncVal    - Encoded Mode Register value.

  @retval MrcStatus - mrcSuccess if the timing is supported.  Else mrcWrongInputParameter.
**/
MrcStatus
MrcDdr5GetWritePreambleSetting (
  IN  MrcParameters  *MrcData,
  OUT DDR5_MR8_TWPRE *EncVal
  )
{
  MrcStatus      Status;
  MrcDebug       *Debug;
  UINT32         tWPRE;
  DDR5_MR8_TWPRE WritePreambleSettings;

  Debug = &MrcData->Outputs.Debug;
  Status = mrcSuccess;

  tWPRE = MrcGetWpre (MrcData);
  if (tWPRE == 2) {
    WritePreambleSettings = Ddr5tWPRE_2tCK_0010;
  } else if (tWPRE == 3) {
    WritePreambleSettings = Ddr5tWPRE_3tCK_000010;
  } else if (tWPRE == 4) {
    WritePreambleSettings = Ddr5tWPRE_4tCK_00001010;
  } else {
    WritePreambleSettings = 0;
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_ERROR, "%s Invalid tWPRE %d\n", gErrString, tWPRE);
    Status = mrcWrongInputParameter;
  }

  if (Status == mrcSuccess && EncVal != NULL) {
    *EncVal = WritePreambleSettings;
  }

  return Status;
}

// List of MR array indexes to initialize during DDR5 Jedec Init
MrcModeRegister Ddr5MrInitList [] = {
  mrMR0,
  mrMR2,
  mrMR3,
  mrMR4,
  mrMR5,
  mrMR6,
  mrMR8,
  mrMR10,
  mrMR11,
  mrMR12,
  mrMR13,
  mpcMR13,
  mrMR37,
  mrMR38,
  mrMR39,
  mrMR40,
  mrMR45,
  mrMR48,
  mpcMR32a0,
  mpcMR32a1,
  mpcMR32b0,
  mpcMR32b1,
  mpcMR33a0,
  mpcMR33b0,
  mpcMR33,
  mpcMR34,
  mpcApplyVrefCa,
  mrMR34,
  mrMR35,
  mrMR36,
  mpcDllReset,
  mpcZqCal,
  mpcZqLat,
  mpcEnterCaTrainMode,
  mpcSetCmdTiming,
  mpcSelectAllPDA,
  mrEndOfSequence
};

/**
  This function returns an MR value or MPC command for the input MrRegNum.
  Some MR values (such as timing or vref MRs) will be different depending
  on the system configuration specified in the MrcData variable.

  @param[in]  MrcData      - Pointer to global MRC data.
  @param[in]  MrRegNum     - Requested MrRegiser
  @param[out] MrRegValOut  - The value for the requested MR Register
  @param[in]  Controller   - Index of the requested Controler
  @param[in]  Channel      - Index of the requested Channel

  @retval MrcStatus - mrcSuccess if the MrRegNum is supported. Else mrcWrongInputParameter.
**/
MrcStatus
Ddr5JedecInitVal (
  IN  MrcParameters *const MrcData,
  IN  MrcModeRegister      MrRegNum,
  OUT UINT16               *MrRegValOut,
  IN  UINT32               Controller,
  IN  UINT32               Channel,
  IN  UINT32               Rank
  )
{
  MrcStatus      Status;
  MrcInput       *Inputs;
  MrcOutput      *Outputs;
  MrcSaveData    *SaveOutputs;
  MrcDebug       *Debug;
  MrcChannelOut  *ChannelOut;
  MrcTiming      *TimingPtr;
  UINT8          RetVal;
  UINT8          RlEnc;
  UINT8          WrEnc;
  UINT8          TrtpEnc;
  UINT8          TdllkEnc;
  INT8           RttValue;
  INT8           RttNomWr;
  INT8           RttNomRd;
  UINT32         Profile;
  TOdtValueDqDdr5  DqOdtTableIndex;
  TOdtValueCccDdr5 *CccOdtTableIndex;
  DDR5_MR8_TRPRE RpreVal;
  DDR5_MR8_TWPRE WpreVal;
  DDR5_MODE_REGISTER_0_TYPE *Mr0;
  DDR5_MODE_REGISTER_2_TYPE *Mr2;
  DDR5_MODE_REGISTER_4_TYPE *Mr4;
  DDR5_MODE_REGISTER_6_TYPE *Mr6;
  DDR5_MODE_REGISTER_8_TYPE *Mr8;
  DDR5_MODE_REGISTER_10_TYPE *Mr10;
  DDR5_MODE_REGISTER_11_TYPE *Mr11;
  DDR5_MODE_REGISTER_12_TYPE *Mr12;
  DDR5_MODE_REGISTER_13_TYPE *Mr13;
  DDR5_MODE_REGISTER_34_TYPE *Mr34;
  DDR5_MODE_REGISTER_35_TYPE *Mr35;
  DDR5_MODE_REGISTER_37_TYPE *Mr37;
  DDR5_MODE_REGISTER_38_TYPE *Mr38;
  DDR5_MODE_REGISTER_39_TYPE *Mr39;
  DDR5_MODE_REGISTER_40_TYPE *Mr40;
  DDR5_MODE_REGISTER_45_TYPE *Mr45;
  DDR5_MODE_REGISTER_113_TYPE *Mr129;
  DDR5_MODE_REGISTER_114_TYPE *Mr130;

  Inputs     = &MrcData->Inputs;
  Outputs    = &MrcData->Outputs;
  SaveOutputs = &MrcData->Save.Data;
  Debug      = &Outputs->Debug;
  Profile    = Inputs->MemoryProfile;
  ChannelOut = &Outputs->Controller[Controller].Channel[Channel];
  TimingPtr  = &ChannelOut->Timing[Profile];
  Status     = mrcSuccess;
  RetVal     = 0;
  RttValue   = 0;

  Status  = GetOdtTableIndex (MrcData, Controller, Channel, RANK_TO_DIMM_NUMBER (Rank), &DqOdtTableIndex);
  CccOdtTableIndex = (TOdtValueCccDdr5 *) SelectCccTable_DDR5 (MrcData, Controller, Channel, Rank);
  if ((CccOdtTableIndex == NULL) || (Status != mrcSuccess)) {
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_ERROR, "%s: invalid DQ/CCC ODT index", gErrString);
    return mrcFail;
  }

  // We will only return one value for the input MR Num.
  Mr0  = (DDR5_MODE_REGISTER_0_TYPE *) &RetVal;
  Mr2  = (DDR5_MODE_REGISTER_2_TYPE *) &RetVal;
  Mr4  = (DDR5_MODE_REGISTER_4_TYPE *) &RetVal;
  Mr6  = (DDR5_MODE_REGISTER_6_TYPE *) &RetVal;
  Mr8  = (DDR5_MODE_REGISTER_8_TYPE *) &RetVal;
  Mr10 = (DDR5_MODE_REGISTER_10_TYPE *) &RetVal;
  Mr11 = (DDR5_MODE_REGISTER_11_TYPE *) &RetVal;
  Mr12 = (DDR5_MODE_REGISTER_12_TYPE *) &RetVal;
  Mr13 = (DDR5_MODE_REGISTER_13_TYPE *) &RetVal;
  Mr34 = (DDR5_MODE_REGISTER_34_TYPE *) &RetVal;
  Mr35 = (DDR5_MODE_REGISTER_35_TYPE *) &RetVal;
  Mr37 = (DDR5_MODE_REGISTER_37_TYPE *) &RetVal;
  Mr38 = (DDR5_MODE_REGISTER_38_TYPE *) &RetVal;
  Mr39 = (DDR5_MODE_REGISTER_39_TYPE *) &RetVal;
  Mr40 = (DDR5_MODE_REGISTER_40_TYPE *) &RetVal;
  Mr45 = (DDR5_MODE_REGISTER_45_TYPE *) &RetVal;
  Mr129 = (DDR5_MODE_REGISTER_113_TYPE *) &RetVal;
  Mr130 = (DDR5_MODE_REGISTER_114_TYPE *) &RetVal;

  switch (MrRegNum) {
    case mrMR0:
      // Mr0->Bits.BurstLength = 0 (BL16)
      if (EncodeReadLatencyDdr5 (MrcData, TimingPtr->tCL, &RlEnc) != mrcSuccess) {
        Status = mrcWrongInputParameter;
      }
      Mr0->Bits.CasLatency = RlEnc;
      break;

    case mrMR2:
      // Mr2->Bits.ReadPreambleTraining = 0 (Normal Mode)
      // Mr2->Bits.WriteLevelingTraining = 0 (Normal Mode)
      // Mr2->Bits.Mode1n = 0 (2N Mode) (Read Only)
      // Mr2->Bits.MaxPowerSavingsMode = 0 (Disable);
      Mr2->Bits.CsAssertionDuration = 0; // (Multiple cycles of CS assertion supported)
      // Mr2->Bits.Device15Mpsm = 0 (Disable)
      // Mr2->Bits.InternalWriteTiming = 0 (Disable)
      break;

    case mrMR3:
      // Mr3->Bits.WriteLevelingInternalCycleLowerByte = 0
      // Mr3->Bits.WriteLevelingInternalCycleUpperByte = 0
      break;

    case mrMR4:
      Mr4->Bits.RefreshRate = 0x2; // Read Only, setting to default value
      Mr4->Bits.RefreshTrfcMode = (Inputs->FineGranularityRefresh) ? 1 : 0; // TRUE=FGR, FALSE=Normal
      // Mr4->Bits.Tuf = 0 [Read Only]
      break;

    case mrMR5:
      // Mr5->Bits.DataOutputDisable = 0; (Default)
      // Mr5->Bits.PullUpOutputDriverImpedance = 0; (RZQ/7)
      // Mr5->Bits.TdqsEnable = 0; (Default)
      // Mr5->Bits.DmEnable = 0; (Default)
      // Mr5->Bits.PullDownOutputDriverImpedance = 0; (RZQ/7)
      break;

    case mrMR6:
      if (EncodeWriteRecoveryDdr5 (MrcData, TimingPtr->tWR, &WrEnc) != mrcSuccess) {
        Status = mrcWrongInputParameter;
      }
      if (EncodeTrtpDdr5 (MrcData, TimingPtr->tRTP, &TrtpEnc) != mrcSuccess) {
        Status = mrcWrongInputParameter;
      }
      Mr6->Bits.WriteRecoveryTime = WrEnc;
      Mr6->Bits.tRTP = TrtpEnc;
      break;

    case mrMR8:
      if (MrcDdr5GetReadPreambleSetting (MrcData, &RpreVal) != mrcSuccess) {
        Status = mrcWrongInputParameter;
      }
      Mr8->Bits.ReadPreambleSettings = RpreVal;

      if (MrcDdr5GetWritePreambleSetting (MrcData, &WpreVal) != mrcSuccess) {
        Status = mrcWrongInputParameter;
      }
      Mr8->Bits.WritePreambleSettings = WpreVal;

      // Mr8->Bits.ReadPostambleSettings = 0;  // 0.5 tCK - 0 Pattern
      // Mr8->Bits.WritePostambleSettings = 0; // 0.5 tCK - 0 Pattern
      break;

    case mrMR10:
      if (ChannelOut->Dimm[Rank / 2].RankInDimm == 2) {
        Mr10->Bits.VrefDqCalibrationValue = Ddr5Vref_75p0; // 75% of VDDQ (Default)
      } else { // 1R
        Mr10->Bits.VrefDqCalibrationValue = Ddr5Vref_75p0; // 75% of VDDQ
      }
      ChannelOut->Mr10PdaEnabled = FALSE;
      break;

    case mrMR11:
      // Read Only: Configured via VrefCA Command
      Mr11->Bits.VrefCaCalibrationValue = DDR5_VREFCA (Ddr5Vref_68p0); // 68% of VDDQ (Default)
      ChannelOut->Mr11PdaEnabled = FALSE;
      break;

    case mrMR12:
      // Read Only: Configured via VrefCS Command
      Mr12->Bits.VrefCsCalibrationValue = DDR5_VREFCS (Ddr5Vref_75p0); // 75% of VDDQ (Default)
      break;

    case mrMR13:
      if (EncodeDdr5TccdlTdllk (MrcData, Outputs->HighFrequency, &TdllkEnc) != mrcSuccess) {
        Status = mrcWrongInputParameter;
      }
      Mr13->Bits.tCCD_L_tDLLK = TdllkEnc;
      break;

    case mpcMR13:
      if (EncodeDdr5TccdlTdllk (MrcData, Outputs->HighFrequency, &TdllkEnc) != mrcSuccess) {
        Status = mrcWrongInputParameter;
      }
      RetVal = DDR5_MPC_CFG_TDLLK_TCCD_L (TdllkEnc);
      break;

    case mrMR37:
      Mr37->Bits.OdtlOnWrOffset  = OdtlOnWrOffsetMinus2;
      Mr37->Bits.OdtlOffWrOffset = OdtlOffWrOffsetPlus2;
      break;

    case mrMR38:
      Mr38->Bits.OdtlOnWrNtOffset = OdtlOnWrOffsetMinus2;
      Mr38->Bits.OdtlOffWrNtOffset = OdtlOffWrOffsetPlus2;
      break;

    case mrMR39:
      Mr39->Bits.OdtlOnRdNtOffset = OdtlOnRdOffsetMinus2;
      Mr39->Bits.OdtlOffRdNtOffset = OdtlOffRdOffsetPlus2;
      break;

    case mrMR40:
      Mr40->Bits.ReadDqsOffsetTiming = SaveOutputs->ReadDqsOffsetDdr5 & 7; // [2:0]
      break;

    case mrMR45:
      Mr45->Bits.DqsIntervalTimerRunTime = 4; // Stop after 64 clocks
      break;

    case mrMR48:
      //Mr48->Bits.WritePatternMode = 0 (default)
      break;

    case mpcMR32a0:
      // MR32 Group A RTT_CK
      RttValue = CccOdtEncode_DDR5 (CccOdtTableIndex[0].RttCk);
      RetVal =  DDR5_MPC_GROUP_A_RTT_CK (RttValue);
      break;

    case mpcMR32b0:
      // MR32 Group B RTT_CK
      RttValue = CccOdtEncode_DDR5 (CccOdtTableIndex[1].RttCk);
      RetVal = DDR5_MPC_GROUP_B_RTT_CK (RttValue);
      break;

    case mpcMR32a1:
      // MR32 Group A RTT_CS
      RttValue = CccOdtEncode_DDR5 (CccOdtTableIndex[0].RttCs);
      RetVal = DDR5_MPC_GROUP_A_RTT_CS (RttValue);
      break;

    case mpcMR32b1:
      // MR32 Group B RTT_CS
      RttValue = CccOdtEncode_DDR5 (CccOdtTableIndex[1].RttCs);
      RetVal = DDR5_MPC_GROUP_B_RTT_CS (RttValue);
      break;

    case mpcMR33a0:
      // MR33 Group A RTT_CA
      RttValue = CccOdtEncode_DDR5 (CccOdtTableIndex[0].RttCa);
      RetVal = DDR5_MPC_GROUP_A_RTT_CA (RttValue);
      break;

    case mpcMR33b0:
      // MR33 Group B RTT_CA
      RttValue = CccOdtEncode_DDR5 (CccOdtTableIndex[1].RttCa);
      RetVal = DDR5_MPC_GROUP_B_RTT_CA (RttValue);
      break;

    case mpcMR33:
      // MR33 DQS_RTT_PARK - set via MPC command
      RttValue = OdtEncode (DqOdtTableIndex.RttParkDqs);
      RetVal = DDR5_MPC_SET_DQS_RTT_PARK (RttValue);
      break;

    case mpcMR34:
      // MR34 RTT_PARK
      RttValue = OdtEncode (DqOdtTableIndex.RttPark);
      RetVal = DDR5_MPC_SET_RTT_PARK (RttValue);
      break;

    case mpcApplyVrefCa:
      // Apply Vrefca and RTT_CA/CS/CK
      RetVal = DDR5_MPC_APPLY_VREF_RTT;
      break;

    case mrMR34:
      // RTT_PARK must be configured via MPC command: DDR5_MPC_SET_RTT_PARK, see mpcMR34
      // We still want to populate the RttPark value in MR34 in the host struct as it's used in the code.
      RttValue = OdtEncode (DqOdtTableIndex.RttWr);
      if (RttValue == -1) {
        break;
      }
      Mr34->Bits.RttWr   = (UINT8) RttValue;
      RttValue = OdtEncode (DqOdtTableIndex.RttPark);
      Mr34->Bits.RttPark = (UINT8) RttValue;
      break;

    case mrMR35:
      // MR35 RTT_NOM_WR / RTT_NOM_RD
      RttNomWr = OdtEncode (DqOdtTableIndex.RttNomWr);
      RttNomRd = OdtEncode (DqOdtTableIndex.RttNomRd);
      if (RttNomWr == -1) {
        MRC_DEBUG_MSG (Debug, MSG_LEVEL_ERROR, "%s Bad RTT_NOM_%s Value\n", gErrString, "WR");
        Status = mrcWrongInputParameter;
        break;
      }
      if (RttNomRd == -1) {
        MRC_DEBUG_MSG (Debug, MSG_LEVEL_ERROR, "%s Bad RTT_NOM_%s Value\n", gErrString, "RD");
        Status = mrcWrongInputParameter;
        break;
      }
      Mr35->Bits.RttNomWr = (UINT8) RttNomWr;
      Mr35->Bits.RttNomRd = (UINT8) RttNomRd;
      break;

    case mrMR36:
      // Mr36->Bits.RttLoopback = 0 (RTT_OFF Default)
      break;

    case mrMR129:
    case mrMR137:
    case mrMR145:
    case mrMR153:
    case mrMR161:
    case mrMR169:
    case mrMR177:
    case mrMR185:
    case mrMR193:
    case mrMR201:
    case mrMR209:
    case mrMR217:
    case mrMR225:
    case mrMR233:
    case mrMR241:
    case mrMR249:
      Mr129->Data8 = 0;
      Mr129->Bits.DfeTap1Enable = 1;
      break;

    case mrMR130:
    case mrMR138:
    case mrMR146:
    case mrMR154:
    case mrMR162:
    case mrMR170:
    case mrMR178:
    case mrMR186:
    case mrMR194:
    case mrMR202:
    case mrMR210:
    case mrMR218:
    case mrMR226:
    case mrMR234:
    case mrMR242:
    case mrMR250:
      Mr130->Data8 = 0;
      Mr130->Bits.DfeTap2Enable = 1;
      break;

    case mpcDllReset:
      RetVal = DDR5_MPC_DLL_RESET;
      break;

    case mpcZqCal:
      RetVal = DDR5_MPC_ZQCAL_START;
      break;

    case mpcZqLat:
      RetVal = DDR5_MPC_ZQCAL_LATCH;
      break;

    case mpcEnterCaTrainMode:
      RetVal = DDR5_MPC_ENTER_CA_TRAINING_MODE;
      break;

    case mpcSetCmdTiming:
      RetVal = (TimingPtr->NMode == 2) ? DDR5_MPC_SET_2N_COMMAND_TIMING : DDR5_MPC_SET_1N_COMMAND_TIMING;
      break;

    case mpcSelectAllPDA:
      RetVal =  DDR5_MPC_PDA_SELECT_ID(0xf);
      break;

    default:
      Status = mrcWrongInputParameter;
      break;
  }

  if (RttValue == -1) {
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_ERROR, "%s Invalid ODT Value\n", gErrString);
    Status = mrcWrongInputParameter;
  }


  if (MrRegValOut != NULL) {
    *MrRegValOut = RetVal;
  } else {
    Status = mrcWrongInputParameter;
  }

  if (Status != mrcSuccess) {
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_ERROR, "%s failed for MrRegNum = %d\n", __FUNCTION__, MrRegNum);
  }

  return Status;
}


/**
  This function returns the Generic MRS FSM Command Type and Delay Type associated with the
  input MrcModeRegister value.

  @param[in]  MrcData      - Pointer to global MRC data.
  @param[in]  MrRegNum     - Requested MrRegiser
  @param[out] CmdTypeOut   - The Generic MRS FSM Command Type to use for the input MrRegister
  @param[out] DelayTypeOut - The type of delay to use for the input MrRegister

  @retval MrcStatus - mrcSuccess if the MrRegNum is supported. Else mrcWrongInputParameter.
**/
MrcStatus
MrcDdr5GetGmfAttributes (
  IN  MrcParameters *const MrcData,
  IN  MrcModeRegister      MrRegNum,
  OUT GmfCmdType           *CmdTypeOut,  OPTIONAL
  OUT GmfDdr5DelayType     *DelayTypeOut OPTIONAL
  )
{
  MrcStatus     Status;
  MrcOutput     *Outputs;
  MrcDebug      *Debug;
  GmfCmdType    CmdType;
  GmfDdr5DelayType  DelayType;

  Outputs    = &MrcData->Outputs;
  Debug      = &Outputs->Debug;
  Status     = mrcSuccess;
  CmdType    = GmfCmdMrw;
  DelayType  = GmfDdr5Delay_tMOD;

  switch (MrRegNum) {
    case mrMR0:
    case mrMR2:
    case mrMR3:
    case mrMR4:
    case mrMR5:
    case mrMR6:
    case mrMR8:
    case mrMR10:
    case mrMR13:
    case mrMR37:
    case mrMR38:
    case mrMR39:
    case mrMR40:
//  case mrMR43:
//  case mrMR44:
    case mrMR45:
    case mrMR48:
    case mrMR34:
    case mrMR35:
    case mrMR36:
      CmdType    = GmfCmdMrw;
      DelayType  = GmfDdr5Delay_tMOD;
      break;

    case mrMR129:
    case mrMR130:
    case mrMR131:
    case mrMR132:
    case mrMR137:
    case mrMR138:
    case mrMR139:
    case mrMR140:
    case mrMR145:
    case mrMR146:
    case mrMR147:
    case mrMR148:
    case mrMR153:
    case mrMR154:
    case mrMR155:
    case mrMR156:
    case mrMR161:
    case mrMR162:
    case mrMR163:
    case mrMR164:
    case mrMR169:
    case mrMR170:
    case mrMR171:
    case mrMR172:
    case mrMR177:
    case mrMR178:
    case mrMR179:
    case mrMR180:
    case mrMR185:
    case mrMR186:
    case mrMR187:
    case mrMR188:
    case mrMR193:
    case mrMR194:
    case mrMR195:
    case mrMR196:
    case mrMR201:
    case mrMR202:
    case mrMR203:
    case mrMR204:
    case mrMR209:
    case mrMR210:
    case mrMR211:
    case mrMR212:
    case mrMR217:
    case mrMR218:
    case mrMR219:
    case mrMR220:
    case mrMR225:
    case mrMR226:
    case mrMR227:
    case mrMR228:
    case mrMR233:
    case mrMR234:
    case mrMR235:
    case mrMR236:
    case mrMR241:
    case mrMR242:
    case mrMR243:
    case mrMR244:
    case mrMR249:
    case mrMR250:
    case mrMR251:
    case mrMR252:
      CmdType   = GmfCmdMrw;
      DelayType = GmfDdr5Delay_tDFE;
      break;

    case mrMR11:
    case mrMR12:
      CmdType   = GmfCmdVref;
      DelayType = GmfDdr5Delay_tVREFCA;
      break;

    case mpcApplyVrefCa:
    case mpcMR13:
    case mpcMR32a0:
    case mpcMR32a1:
    case mpcMR32b0:
    case mpcMR32b1:
    case mpcMR33a0:
    case mpcMR33b0:
    case mpcMR33:
    case mpcMR34:
    case mpcDllReset:
    case mpcSetCmdTiming:
    case mpcSelectAllPDA:
      CmdType   = GmfCmdMpc;
      DelayType = GmfDdr5Delay_tMOD;
      break;

    case mpcZqCal:
      CmdType = GmfCmdMpc;
      DelayType = GmfDdr5Delay_tZQCAL;
      break;

    case mpcZqLat:
    case mpcEnterCaTrainMode:
      CmdType = GmfCmdMpc;
      DelayType = GmfDdr5Delay_tZQLAT;
      break;

    default:
      Status = mrcWrongInputParameter;
      break;
  }

  if (CmdTypeOut != NULL) {
    *CmdTypeOut = CmdType;
  }

  if (DelayTypeOut != NULL) {
    *DelayTypeOut = DelayType;
  }

  if (Status != mrcSuccess) {
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_ERROR, "%s failed for MrRegNum = %d\n", __FUNCTION__, MrRegNum);
  }

  return Status;
}

/**
  This function returns the requested DelayType timing in nCK units.

  @param[in]  MrcData      - Pointer to global MRC data.
  @param[in]  DelayType    - Requested delay type
  @param[out] TimingNckOut - Output variable for the requested delay timing in nCK units

  @retval MrcStatus - mrcSuccess if the DelayType is supported.
                      mrcWrongInputParameter if TimingNckOut is NULL.
                      mrcTimingError if the timing value overflows the output pointer.
**/
MrcStatus
Ddr5GmfDelayTimings (
  IN  MrcParameters *const MrcData,
  IN  GmfTimingIndex       DelayType,
  OUT UINT16               *TimingNckOut
  )
{
  UINT32        TimingNck;
  UINT32        tCK;
#ifdef MRC_DEBUG_PRINT
  MrcDebug  *Debug;

  Debug = &MrcData->Outputs.Debug;
#endif

  if (TimingNckOut == NULL) {
    return mrcWrongInputParameter;
  }

  tCK = MrcData->Outputs.MemoryClock;
  switch ((GmfDdr5DelayType) DelayType)
  {
    case GmfDdr5Delay_tVREFCA:
    case GmfDdr5Delay_tMOD:
      // DDR5 "tMRD"
      // max(14ns, 16nCK)
      TimingNck = tMODGet (MrcData, tCK);
      break;

    case GmfDdr5Delay_tZQCAL:
      TimingNck = tZQCALGet (MrcData, tCK);
      break;

    case GmfDdr5Delay_tZQLAT:
      TimingNck = tZQCSGet (MrcData, tCK);
      break;

    case GmfDdr5Delay_tDFE:
      // DDR5 DFE Mode Register Write Update Delay Time
      // Min 80ns
      TimingNck = DIVIDECEIL (80*1000*1000, tCK);
      break;

    default:
      TimingNck = 0;
      break;
  }

  MRC_DEBUG_ASSERT (TimingNck <= MRC_UINT16_MAX, Debug, "%s: TimingNckOut Overflow\n", gErrString);
  if (TimingNck > MRC_UINT16_MAX) {
    return mrcTimingError;
  }

  *TimingNckOut = (UINT16) TimingNck;
  return mrcSuccess;
}

/**
  This function will setup the default MR values for DDR5 based on
  DRAM Timings and Frequency in MRC global data.
  Only populated Channels and Ranks are initialized.

  @param[in, out]  MrcData   -  Pointer to MRC global data

  @retval MrcStatus - mrcSuccess if successful, else an error status.
**/
MrcStatus
InitMrwDdr5 (
  IN OUT MrcParameters *const MrcData
  )
{
  MrcDebug      *Debug;
  MrcOutput     *Outputs;
  MrcChannelOut *ChannelOut;
  UINT16        *MrPtr;
  UINT32        MrIndex;
  MrcStatus     Status;
  UINT32        Controller;
  UINT32        Channel;
  UINT32        Dimm;
  UINT32        DimmRank;
  UINT32        Rank;
  UINT32        Index;
  INT16         Value1;
  INT16         Value2;
  MrcModeRegister CurMrAddr;
  DDR5_MODE_REGISTER_34_TYPE *Mr34;

  Outputs = &MrcData->Outputs;
  Debug   = &Outputs->Debug;
  Status = mrcSuccess;

  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "InitMrwDdr5:\n");

  for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
    for (Channel = 0; Channel < Outputs->MaxChannels; Channel++) {
      for (Rank = 0; Rank < MAX_RANK_IN_CHANNEL; Rank++) {
        if (!MrcRankExist (MrcData, Controller, Channel, Rank)) {
          continue;
        }
        Dimm = RANK_TO_DIMM_NUMBER (Rank);
        DimmRank = Rank % MAX_RANK_IN_DIMM;
        for (Index = 0; Ddr5MrInitList[Index] != mrEndOfSequence; Index++) {
          CurMrAddr = Ddr5MrInitList[Index];
          ChannelOut = &Outputs->Controller[Controller].Channel[Channel];
          MrPtr = ChannelOut->Dimm[Dimm].Rank[DimmRank].MR;
          MrIndex = MrcMrAddrToIndex (MrcData, &CurMrAddr);
          // Fetch the init value
          Status = Ddr5JedecInitVal (MrcData, CurMrAddr, &MrPtr[MrIndex], Controller, Channel, Rank);
          if (Status != mrcSuccess) {
            return Status;
          }
          MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, " MC%u C%u R%u MrAddr = %3u MrIndex = %2u Value = 0x%02X ", Controller, Channel, Rank, CurMrAddr, MrIndex, MrPtr[MrIndex]);
          switch (CurMrAddr) {
            case mrMR34:
              Mr34 = (DDR5_MODE_REGISTER_34_TYPE *) &MrPtr[MrIndex];
              Value1 = OdtDecode (Mr34->Bits.RttWr);
              Value2 = OdtDecode (Mr34->Bits.RttPark);
              MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "RttWr=%d RttPark=%d", Value1, Value2);
              break;
            default:
              break;
          }
          MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\n");
        } // Index
      } // Rank
    } // Channel
  } // Controller

  return Status;
}


/**
  Perform JEDEC Init sequence for DDR5.

  @param[in] MrcData  - Pointer to MRC global data.
  @param[in] Sequence - Array of MrcModeRegister values defining the sequence
                        of MRW and MPC commands to perform. The entire array
                        is index until the sentinel value mrEndOfSequence
                        is reached.
  @param[in] SagvConfig  - Configure the Generic FSM for SAGV transition (don't run the FSM and don't Clean it)

  @retval MrcStatus
**/
MrcStatus
PerformGenericMrsFsmSequence (
  IN MrcParameters *const MrcData,
  IN const MrcModeRegister  *const Sequence,
  IN BOOLEAN              SagvConfig
  )
{
  const MRC_FUNCTION  *MrcCall;
  MrcStatus     Status;
  MrcInput      *Inputs;
  MrcOutput     *Outputs;
  MrcChannelOut *ChannelOut;
  MrcDebug      *Debug;
  UINT32        Channel;
  UINT32        Controller;
  UINT32        Dimm;
  UINT32        DimmRank;
  UINT32        Rank;
  UINT32        OutIdx;
  UINT8         Data;
  UINT32        MrIndex;
  UINT32        Index;
  UINT16        *MrPtr;
  GmfCmdType    CmdType;
  GmfDdr5DelayType DelayType;
  MrcModeRegister  CurMrAddr;
  const MrcModeRegister   *MrPerRank;
  BOOLEAN                 IsPdaMr;
  GenericFsmConfigMode    ConfigSeq;
  MRC_GEN_MRS_FSM_MR_TYPE *GenMrsFsmMr;
  MRC_GEN_MRS_FSM_MR_TYPE MrData[MAX_CONTROLLER][MAX_CHANNEL][MAX_RANK_IN_CHANNEL][MAX_MR_GEN_FSM];

  // These MR's can be trained per rank and hence should be marked per Rank in Gen MRS FSM for runtime
  static const MrcModeRegister SagvMrPerRankDdr5[] = {
    mpcMR32a0,  // RTT_CK Group A
    mpcMR32a1,  // RTT_CS Group A
    mpcMR33a0,  // RTT_CA Group A
    mpcMR32b0,  // RTT_CK Group B
    mpcMR32b1,  // RTT_CS Group B
    mpcMR33b0,  // RTT_CA Group B
    mpcMR33,    // DQS_RTT_PARK
    mpcMR34,    // RTT_PARK
//  mrMR43,
//  mrMR44,
    mrMR3,      // Write Leveling Internal Cycle Alignment
//  mrMR5,      // Ron pull-up / pull-down  @todo_adl Include this if DIMM Ron will be trained
    mrMR10,     // VREF_DQ
    mrMR11,     // VREF_CA
    mrMR12,     // VREF_CS
    mrMR34,     // RTT_WR
    mrMR35,     // RTT_NOM_WR, RTT_NOM_RD
//  mrMR37,     // ODTLon_WR_Offset,    ODTLoff_WR_Offset
//  mrMR38,     // ODTLon_WR_NT_Offset, ODTLoff_WR_NT_Offset
//  mrMR39,     // ODTLon_RD_NT_Offset, ODTLoff_RD_NT_Offset

//  mrMR129 - mrMR252 // DFE Tap1..4 - checked directly in IsMrPerRank, to reduce size of this array
    mrEndOfSequence
  };

  Inputs  = &MrcData->Inputs;
  Outputs = &MrcData->Outputs;
  Debug   = &Outputs->Debug;
  MrcCall = Inputs->Call.Func;
  Status  = mrcSuccess;
  if (!Outputs->RestoreMRs && !Outputs->Ddr5MrInitDone) {
    // Setup Init MR's to a default value, and update MrcData.
    Status = InitMrwDdr5 (MrcData);
    if (Status != mrcSuccess) {
      return Status;
    }
    Outputs->Ddr5MrInitDone = TRUE;
  }

  // Clear out our array
  MrcCall->MrcSetMem ((UINT8 *) MrData, sizeof (MrData), 0);

  for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
    for (Channel = 0; Channel < Outputs->MaxChannels; Channel++) {
      ChannelOut = &Outputs->Controller[Controller].Channel[Channel];
      for (Rank = 0; Rank < MAX_RANK_IN_CHANNEL; Rank++) {
        if (!MrcRankExist (MrcData, Controller, Channel, Rank)) {
          continue;
        }
        Dimm = RANK_TO_DIMM_NUMBER (Rank);
        DimmRank = Rank % MAX_RANK_IN_DIMM;
        MrPtr = ChannelOut->Dimm[Dimm].Rank[DimmRank].MR;
        OutIdx = 0;
        // Pass on the list twice. First pass: non-PDA, second pass: PDA
        for (ConfigSeq = GenericFsmConfigNonPdaMrs; ConfigSeq <= GenericFsmConfigPdaMrs; ConfigSeq++) {
          for (Index = 0; Sequence[Index] != mrEndOfSequence; Index++) {
            CurMrAddr = Sequence[Index];
            MrIndex = MrcMrAddrToIndex (MrcData, &CurMrAddr);
            if (MrIndex <  MAX_MR_IN_DIMM) {
              IsPdaMr = MrcMrIsPda (MrcData, Controller, Channel, CurMrAddr);
              if (ConfigSeq == GenericFsmConfigNonPdaMrs) {
                // In non-PDA section discard PDA MR's and also mpcSelectAllPDA
                if (IsPdaMr || (CurMrAddr == mpcSelectAllPDA)) {
                  continue;
                }
              } else { // GenericFsmConfigPdaMrs
                // In PDA section include only PDA MR's and also mpcSelectAllPDA
                if (!IsPdaMr && (CurMrAddr != mpcSelectAllPDA)) {
                  continue;
                }
              }

              // Fetch the Command and Delay type
              Status = MrcDdr5GetGmfAttributes (MrcData, CurMrAddr, &CmdType, &DelayType);
              if (Status != mrcSuccess) {
                return Status;
              }

              // Fetch the data
              Data = (UINT8) MrPtr[MrIndex];

              // Populate the Generic MRS FSM entry
              GenMrsFsmMr = &MrData[Controller][Channel][Rank][OutIdx];
              // In PDA Mr data will be extracted in the inner functions
              GenMrsFsmMr->PdaMr = IsPdaMr;

              // Fetch the data
              GenMrsFsmMr->MrData  = Data;

              if (CurMrAddr > MRC_UINT8_MAX) {
                // Special case for MPC commands
                GenMrsFsmMr->MrAddr = 0;
              } else {
                GenMrsFsmMr->MrAddr = (UINT8) CurMrAddr;
                if (SagvConfig && (CurMrAddr == mrMR12)) {
                  GenMrsFsmMr->FreqSwitchPoint = TRUE;
                }
              }
              GenMrsFsmMr->Valid   = TRUE;
              GenMrsFsmMr->DelayIndex = (GmfTimingIndex) DelayType;
              GenMrsFsmMr->CmdType = CmdType;
            } else {
              MRC_DEBUG_MSG (Debug, MSG_LEVEL_ERROR, "MR index(%d) exceeded MR array length(%d)\n", MrIndex, MAX_MR_IN_DIMM);
              Status = mrcWrongInputParameter;
              return Status;
            }
            OutIdx++;
          } // Index
        } // ConfigSeq

      } // Rank
    } // Channel
  } // Controller

  MrPerRank = SagvConfig ? SagvMrPerRankDdr5 : NULL;

  // Program Generic MRS FSM Per Controller/Channel
  Status = MrcGenMrsFsmConfig (MrcData, MrData, MrPerRank);
  if (Status != mrcSuccess) {
    return Status;
  }

  if (!SagvConfig) {
    // Run Generic FSM
    // Since we use the MRS FSM, we've configured it to send ZQ at the end if the sequence.
    // DDR5 ZQ is skipped if the sequence includes a ZQCAL MPC command.
    Status = MrcGenMrsFsmRun (MrcData);
    if (Status != mrcSuccess) {
      return Status;
    }

    // Clean The Generic FSM used  entries
    // Entries need to be invalidated for next run
    Status = MrcGenMrsFsmClean (MrcData, MrData, FALSE);
    if (Status != mrcSuccess) {
      return Status;
    }
  }
  return Status;
}

/**
  Perform JEDEC Init sequence for DDR5.

  @param[in] MrcData - Pointer to MRC global data.

  @retval MrcStatus
**/
MrcStatus
MrcJedecInitDdr5 (
  IN MrcParameters *const MrcData
  )
{
  MrcStatus Status;
  MrcInput  *Inputs;
  MrcOutput *Outputs;
  UINT32    Controller;
  UINT32    Channel;
  UINT32    Offset;
  UINT64    MrsFsmCtlSave[MAX_CONTROLLER][MAX_CHANNEL];
  MC0_CH0_CR_MRS_FSM_CONTROL_STRUCT MrsFsmCtl;
  const MRC_FUNCTION    *MrcCall;
  // DDR5 Single Cycle MPC commands
  // These commands can be issued prior to EctComplete
  static const MrcModeRegister JedecInitSequenceSingleCycle[] = {
    mpcSetCmdTiming,
    mpcMR32a0,
    mpcMR32a1,
    mpcMR33a0,
    mpcMR32b0,
    mpcMR32b1,
    mpcMR33b0,
    mrMR11,
    mrMR12,
    mpcMR33,
    mpcMR34,
    mpcApplyVrefCa,
    mrEndOfSequence
    };

  // DDR5 Multi Cycle MRW commands
  // These commands can only be issued after EctComplete
  static const MrcModeRegister JedecInitSequenceMultiCycle[] = {
    mrMR0,
    mrMR2,
    mrMR4,
    mrMR5,
    mrMR6,
    mrMR8,
    mrMR13,
    mrMR129,
    mrMR130,
    mrMR131,
    mrMR132,
    mrMR137,
    mrMR138,
    mrMR139,
    mrMR140,
    mrMR145,
    mrMR146,
    mrMR147,
    mrMR148,
    mrMR153,
    mrMR154,
    mrMR155,
    mrMR156,
    mrMR161,
    mrMR162,
    mrMR163,
    mrMR164,
    mrMR169,
    mrMR170,
    mrMR171,
    mrMR172,
    mrMR177,
    mrMR178,
    mrMR179,
    mrMR180,
    mrMR185,
    mrMR186,
    mrMR187,
    mrMR188,
    mrMR193,
    mrMR194,
    mrMR195,
    mrMR196,
    mrMR201,
    mrMR202,
    mrMR203,
    mrMR204,
    mrMR209,
    mrMR210,
    mrMR211,
    mrMR212,
    mrMR217,
    mrMR218,
    mrMR219,
    mrMR220,
    mrMR225,
    mrMR226,
    mrMR227,
    mrMR228,
    mrMR233,
    mrMR234,
    mrMR235,
    mrMR236,
    mrMR241,
    mrMR242,
    mrMR243,
    mrMR244,
    mrMR249,
    mrMR250,
    mrMR251,
    mrMR252,
    mrMR34,
    mrMR35,
    mrMR36,
    mrMR37,
    mrMR38,
    mrMR39,
    mrMR40,
    mrMR45,
    // PDA MR's start here
    // IMPORTANT note: the order of the MR's here should be opposite to their training order in MRC call table
    // For example, MR3 is trained as PDA before MR10.
    mrMR10,
    mrMR3,
//  mrMR44,
//  mrMR43,
    mpcSelectAllPDA,
    mrEndOfSequence
    };

  Inputs = &MrcData->Inputs;
  Outputs = &MrcData->Outputs;
  Status  = mrcSuccess;
  MrcCall = Inputs->Call.Func;

  if (Inputs->BootMode == bmFast) {
    // Disable do_dq_osc_start for the JEDEC Init FSM run
    //zero out MrsFsmCtlSave
    MrcCall->MrcSetMem ((UINT8 *) MrsFsmCtlSave, sizeof(MrsFsmCtlSave), 0);
    for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
      for (Channel = 0; Channel < Outputs->MaxChannels; Channel++) {
        if (MrcChannelExist (MrcData, Controller, Channel)) {
          Offset = OFFSET_CALC_MC_CH (MC0_CH0_CR_MRS_FSM_CONTROL_REG, MC1_CH0_CR_MRS_FSM_CONTROL_REG, Controller, MC0_CH1_CR_MRS_FSM_CONTROL_REG, Channel);
          MrsFsmCtl.Data = MrcReadCR64 (MrcData, Offset);
          MrsFsmCtlSave[Controller][Channel] = MrsFsmCtl.Data; // Save MRS_FSM_CONTROL per controller and per channel
          MrsFsmCtl.Bits.do_dq_osc_start = 0;
          MrcWriteCR64 (MrcData, Offset, MrsFsmCtl.Data);
        }
      }
    }

    // Generic MRS FSM registers are already restored, just run the FSM
    Status = MrcGenMrsFsmRun (MrcData);
    if (Status != mrcSuccess) {
      return Status;
    }

    // Enumerate DRAM devices for PDA use
    if (Inputs->EnablePda) {
      // Program average WrV in non-PDA mode, before PDA enumeration
      MrcUpdateVref (
        MrcData,
        0xF,
        0xF,
        1,     //non_PDA mode
        WrV,
        0,
        FALSE,
        FALSE,
        FALSE,
        TRUE
      );

      Status = MrcPdaEnumeration (MrcData);
      if (Status != mrcSuccess) {
        return Status;
      }
    }

    // Disable multicycle MPC command on MC
    // DRAM will be configured to 1tCK MPC via MR2
    DisableMcMulticycleCs (MrcData, TRUE);

    // Run the FSM second time, after PDA enumeration and multicycle MPC config
    Status = MrcGenMrsFsmRun (MrcData);
    if (Status != mrcSuccess) {
      return Status;
    }

    // Restore do_dq_osc_start
    for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
      for (Channel = 0; Channel < Outputs->MaxChannels; Channel++) {
        if (MrcChannelExist (MrcData, Controller, Channel)) {
          Offset = OFFSET_CALC_MC_CH (MC0_CH0_CR_MRS_FSM_CONTROL_REG, MC1_CH0_CR_MRS_FSM_CONTROL_REG, Controller, MC0_CH1_CR_MRS_FSM_CONTROL_REG, Channel);
          MrcWriteCR64 (MrcData, Offset, MrsFsmCtlSave[Controller][Channel]);
        }
      }
    }
  } else {
    if (PerformGenericMrsFsmSequence (MrcData, JedecInitSequenceSingleCycle, FALSE) != mrcSuccess) {
      Status = mrcFail;
    }

    if (Outputs->EctDone) {
      // Enumerate DRAM devices for PDA use
      if (Inputs->EnablePda) {
        // Program average WrV in non-PDA mode, before PDA enumeration
        MrcUpdateVref (
          MrcData,
          0xF,
          0xF,
          1,     //non_PDA mode
          WrV,
          0,
          FALSE,
          FALSE,
          FALSE,
          TRUE
        );

        Status = MrcPdaEnumeration (MrcData);
        if (Status != mrcSuccess) {
          return Status;
        }
      }

      // Once ECT is done, we can send out multi-cycle MRW commands (first non-PDA, then PDA)
      if (PerformGenericMrsFsmSequence (MrcData, JedecInitSequenceMultiCycle, FALSE) != mrcSuccess) {
        Status = mrcFail;
      }
    }
  }

/*if (Outputs->PdaEnable) {
    // @todo: Program PDA TxVref for all Controllers/Channels if enabled.
    for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
      for (Channel = 0; Channel < Outputs->MaxChannels; Channel++) {
        if (MrcChannelExist (MrcData, Controller, Channel)) {
          RankMask = Outputs->Controller[Controller].Channel[Channel].ValidRankBitMask;
          //ChangeMargin (MrcData, WrV, 0, 0, TRUE, Controller, Channel, RankMask, 0x1F, 0, FALSE, TRUE);
          Ddr5SetDramVref (MrcData, Controller, Channel, RankMask, 0x1F, WrV, 0, FALSE, TRUE, TRUE);
        }
      }
    }
  }*/

  return Status;
}

/**
  This function changes the DDR5 DIMM Voltages using PMIC.
  OEM-specific function is used if implemented and returns SUCCESS;
  otherwise default MRC function is used.

  @param[in, out] MrcData - The MRC "global data" area.
**/
VOID
MrcDdr5VoltageCheckAndSwitch (
  IN OUT MrcParameters      *MrcData
  )
{
  MrcInput          *Inputs;
  MRC_FUNCTION      *MrcCall;
  MrcOutput         *Outputs;
  MrcDebug          *Debug;
  MrcControllerIn   *ControllerIn;
  MrcControllerOut  *ControllerOut;
  MrcChannelOut     *ChannelOut;
  MrcChannelIn      *ChannelIn;
  const MrcDimmIn   *DimmIn;
  MrcDimmOut        *DimmOut;
  MrcProfile        MemoryProfile;
  MrcVddSelect      Vdd;
  MrcVddSelect      Vddq;
  MrcVddSelect      Vpp;
  UINT8             Controller;
  UINT8             Channel;
  UINT8             Dimm;
  UINT8             SpdAddress;
  UINT8             Value;
  UINT32            Offset;
  UINT32            Status;
  /*
   * Below are used to check duplicate configuration as
   * one Ddr5 dimm is plugged into 2 channels
   */
  UINT8             DimmConfigured[MAX_CONTROLLER * MAX_DDR5_CHANNEL * MAX_DIMMS_IN_CHANNEL];
  UINT8             DimmNextFree;

  Inputs  = &MrcData->Inputs;
  Outputs = &MrcData->Outputs;
  MrcCall = Inputs->Call.Func;
  Debug   = &Outputs->Debug;
  MemoryProfile = Inputs->MemoryProfile;

  if ((Outputs->VddVoltage[MemoryProfile]  == VDD_INVALID) ||
      (Outputs->VddqVoltage[MemoryProfile] == VDD_INVALID) ||
      (Outputs->VppVoltage[MemoryProfile]  == VDD_INVALID)) {
    return;
  }

  MrcCall->MrcSetMem (DimmConfigured, sizeof (DimmConfigured), 0);

  for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
    ControllerIn  = &Inputs->Controller[Controller];
    ControllerOut = &Outputs->Controller[Controller];
    for (Channel = 0; Channel < MAX_CHANNEL; Channel++) {
      if (!MrcChannelExist (MrcData, Controller, Channel)) {
        continue;
      }
      ChannelIn   = &ControllerIn->Channel[Channel];
      ChannelOut  = &ControllerOut->Channel[Channel];
      for (Dimm = 0; Dimm < MAX_DIMMS_IN_CHANNEL; Dimm++) {
        if (ChannelOut->Dimm[Dimm].Status != DIMM_PRESENT) {
          continue;
        }
        DimmIn  = &ChannelIn->Dimm[Dimm];
        DimmOut = &ChannelOut->Dimm[Dimm];

        SpdAddress = DimmIn->SpdAddress;
        if (SpdAddress == 0) {
          // In case of fixed spd file
          continue;
        }

        /* Check if it's already configured */
        for (DimmNextFree = 0; DimmNextFree < sizeof(DimmConfigured); DimmNextFree++) {
          if (DimmConfigured[DimmNextFree] == 0) {
            break;
          }

          if (DimmConfigured[DimmNextFree] == SpdAddress) {
            /* Already configured */
            DimmNextFree = 0xFF;
            break;
          }
        }

        if (DimmNextFree >= sizeof (DimmConfigured)) {
          continue;
        }

        Offset = SPD5_MR5;
        Value = MrcCall->MrcSmbusRead8 (SpdAddress | (Offset << 8), &Status);
        if ((Value & SPD5_MR5_HUB_SUPPORT) == 0) {
          MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Dimm SpdAddress 0x%x: Doesn't support HUB\n", SpdAddress);
          continue;
        }

        Vdd  = DimmOut->VddVoltage[MemoryProfile];
        Vddq = DimmOut->VddqVoltage[MemoryProfile];
        Vpp  = DimmOut->VppVoltage[MemoryProfile];
        Status = EFI_UNSUPPORTED;
        if (MrcCall->MrcSetMemoryPmicVoltage != NULL) {
          // Callback to deal with Vendor specific PMICs
          Status = MrcCall->MrcSetMemoryPmicVoltage (MrcData, SpdAddress, Vdd, Vddq, Vpp);
        }
        if ((Status != EFI_SUCCESS) && DimmOut->PmicProgrammable) {
          // Call default config function which assumes only one PMIC
          MrcDefaultSetMemoryPmicVoltage (MrcData, SpdAddress, DimmOut->IsPmicSupport10MVStep, Vdd, Vddq, Vpp);
        }
        DimmConfigured[DimmNextFree] = SpdAddress;
      } // For Dimm
    } // For Channel
  } // For Controller

  Outputs->VoltageDone = TRUE;
}

/**
  Perform JEDEC reset sequence for DDR5.

  @param[in] MrcData - Include all MRC global data.

  @retval - mrcSuccess if no errors encountered, mrcFail otherwise
**/
MrcStatus
MrcJedecResetDdr5 (
  IN MrcParameters *const MrcData
  )
{
  MrcStatus         Status;
  MrcInput          *Inputs;
  MrcOutput         *Outputs;
  MRC_FUNCTION      *MrcCall;
  MrcTiming         *Timing;
  UINT32            PciEBar;
  INT64             GetSetVal;
  UINT32            tInit1;
  UINT32            tInit3;
  UINT32            tInit4;
  UINT32            tInit5;
  UINT32            CsOverrideDelay;
  UINT32            tXS;
  UINT32            tXPR;
  UINT32            FirstController;
  UINT8             FirstChannel;
  MrcProfile        Profile;

  static const MrcModeRegister JedecResetExitSequence[] = {
    mpcMR13,
    mpcDllReset,
    mpcZqCal,
    mpcZqLat,
    mrEndOfSequence
    };

  Inputs            = &MrcData->Inputs;
  Outputs           = &MrcData->Outputs;
  MrcCall           = Inputs->Call.Func;
  PciEBar           = Inputs->PciEBaseAddress;
  FirstController   = (MrcControllerExist(MrcData, cCONTROLLER0)) ? 0 : 1;
  FirstChannel      = MrcData->Outputs.Controller[FirstController].FirstPopCh;
  Profile           = Inputs->MemoryProfile;
  Timing            = &Outputs->Controller[FirstController].Channel[FirstChannel].Timing[Profile];
  tXS               = tXSRGet (MrcData, Timing);
  Status            = mrcSuccess;

  // Init Timings in Nanoseconds
  tInit1 = MRC_DDR5_tINIT1_US * MRC_TIMER_1US;
  tInit3 = MRC_DDR5_tINIT3_MS * MRC_TIMER_1MS;
  tInit4 = MRC_DDR5_tINIT4_US * MRC_TIMER_1US;
  // Convert nCK to FS
  tInit5 = (MRC_DDR5_tINIT5_NCK * Outputs->MemoryClock);
  tInit5 = DIVIDECEIL (tInit5, FEMTOSECONDS_PER_NANOSECOND);
  tXPR   = (tXS * Outputs->MemoryClock);
  tXPR   = DIVIDECEIL (tXPR, FEMTOSECONDS_PER_NANOSECOND);
  CsOverrideDelay = (3 * Outputs->MemoryClock); //3tCK
  CsOverrideDelay = DIVIDECEIL (CsOverrideDelay, FEMTOSECONDS_PER_NANOSECOND);

  // Enable Multicycle CS on the MC which is the DRAM's default MPC mode after DRAM Reset
  EnableMcMulticycleCs (MrcData);

  // Force CKE_ON before DRAM Reset
  // Even though there is no CKE pin on DDR5, CKE override is used
  // to prevent power down entry in all technologies.
  MrcCkeOnProgramming (MrcData);
  GetSetVal = MC0_CH0_CR_REUT_CH_MISC_CKE_CS_CTRL_CKE_Override_MAX;
  MrcGetSetMcCh (MrcData, MAX_CONTROLLER, MAX_CHANNEL, GsmMccCkeOverride, WriteNoCache, &GetSetVal);

  // CA Tristate is disabled in MrcMcConfiguration
  // Disabling CA forces CA[4:0] high when the CA bus is inactive.
  // This allows us to issue multiple cycles of NOP commands by simply
  // driving CS_N low using OverrideCs();

  // Check if this is the first time we are performing JEDEC RESET
  if (Outputs->JedecInitDone){
    // Reset Initialization with Stable Power:
    // Only need to assert RESET for 1us (tPW_RESET)
    tInit1 = MRC_TIMER_1US;
  }

  // Override CS_N to go LOW
  OverrideCs (MrcData, 0, TRUE);
  // Override Vdd / Vddq / Vpp using PMIC
  if (Outputs->VoltageDone == FALSE) {
    MrcEnableDimmPmic (MrcData);
    MrcDdr5VoltageCheckAndSwitch (MrcData);
  }
  // Assert DIMM reset signal
  MrcCall->MrcDramReset (PciEBar, 0);

  // After tINIT1, bring Dram out of Reset
  MrcWait (MrcData, tInit1);
  MrcCall->MrcDramReset (PciEBar, 1);

  // After tINIT3, override CS_N to go HIGH
  MrcWait (MrcData, tInit3);
  OverrideCs (MrcData, 1, TRUE);

  // Wait for tINIT4 time for CS_N to remain HIGH
  MrcWait (MrcData, tInit4);

  // At tINIT4, issue NOP command by driving CS_N low for tINIT5
  OverrideCs (MrcData, 0, TRUE);
  MrcWait (MrcData, tInit5);

  // After tINIT5, override CS_N to go HIGH
  OverrideCs (MrcData, 1, TRUE);

  // Disabling the CS Override causes CA[5:13] to go low.
  // Wait at least 3tCK after setting CS_N high
  // to avoid CA transitioning during the NOP.
  MrcWait (MrcData, CsOverrideDelay);
  OverrideCs (MrcData, 1, FALSE);

  // Step8: Wait for tXPR timings before sending any command
  MrcWait (MrcData, tXPR);

  if (Inputs->BootMode == bmCold) {
    if (PerformGenericMrsFsmSequence (MrcData, JedecResetExitSequence, FALSE) != mrcSuccess) {
      Status = mrcFail;
    }
  }

  return Status;
}

/**
  This function is used to get the timing parameter tDQSCK.

  @param[in]  MrcData    - Pointer to MRC global data.

  @retval UINT32 - The requested parameter in Picoseconds.
**/
UINT32
Ddr5GetTdqsck (
  IN  MrcParameters   *const MrcData
  )
{
  MrcOutput   *Outputs;
  UINT32      tCKPs;
  UINT32      tDqsCk;

  Outputs = &MrcData->Outputs;
  tCKPs   = Outputs->tCKps;

  // tDQSCK is in 1000nCK units
  if (Outputs->Frequency <= 3200) {
    tDqsCk = MRC_DDR5_tDQSCK_3200;
  } else if (Outputs->Frequency <= 3600) {
    tDqsCk = MRC_DDR5_tDQSCK_3600;
  } else if (Outputs->Frequency <= 4000) {
    tDqsCk = MRC_DDR5_tDQSCK_4000;
  } else if (Outputs->Frequency <= 4400) {
    tDqsCk = MRC_DDR5_tDQSCK_4400;
  } else { // Frequency > 4400
    tDqsCk = MRC_DDR5_tDQSCK_4800;
  }
  // Convert to PS
  tDqsCk *= tCKPs;

  // Divide by 1000 due to 1000nCK factor
  tDqsCk /= 1000;

  return (tDqsCk); // return units of Picoseconds
}

/**
This function is used to get the timing parameter tDQSCKi.

@param[in]  MrcData    - Pointer to MRC global data.

@retval UINT32 - The requested parameter in Picoseconds.
**/
UINT32
Ddr5GetTdqscki (
  IN  MrcParameters   *const MrcData
  )
{
  MrcOutput   *Outputs;
  UINT32      tCKPs;
  UINT32      tDqsCki;

  Outputs = &MrcData->Outputs;
  tCKPs   = Outputs->tCKps;

  // tDQSCK is in 1000nCK units
  if (Outputs->Frequency <= 3200) {
    tDqsCki = MRC_DDR5_tDQSCKi_3200;
  } else if (Outputs->Frequency <= 3600) {
    tDqsCki = MRC_DDR5_tDQSCKi_3600;
  } else if (Outputs->Frequency <= 4000) {
    tDqsCki = MRC_DDR5_tDQSCKi_4000;
  } else if (Outputs->Frequency <= 4400) {
    tDqsCki = MRC_DDR5_tDQSCKi_4400;
  } else { // Frequency > 4400
    tDqsCki = MRC_DDR5_tDQSCKi_4800;
  }
  // Convert to PS
  tDqsCki *= tCKPs;

  // Divide by 1000 due to 1000nCK factor
  tDqsCki /= 1000;

  return (tDqsCki); // return units of Picoseconds
}

/**
  This function is used to get the timing parameter tRX_DQS2DQ Min or Max.

  @param[in]  MrcData    - Pointer to MRC global data.
  @param[in]  IsMin      - TRUE returns the minimum value, FALSE returns the maximum value.

  @retval UINT32 - The requested parameter in Femptoseconds.
**/
UINT32
Ddr5GetTdqs2dq (
  IN  MrcParameters   *const MrcData,
  IN  BOOLEAN         IsMin
  )
{
  MrcOutput   *Outputs;
  UINT32      UIPs;
  UINT32      Tdqs2dqPs;

  Outputs = &MrcData->Outputs;
  UIPs   = Outputs->UIps;

  // tRX_DQS2DQ is in 100UI units
  if (IsMin) {
    Tdqs2dqPs = MRC_DDR5_tRX_DQS2DQ_MIN;
  } else {
    if (Outputs->Frequency < 4400) {
      Tdqs2dqPs = MRC_DDR5_tRX_DQS2DQ_3200;
    } else if (Outputs->Frequency < 4800) {
      Tdqs2dqPs = MRC_DDR5_tRX_DQS2DQ_4400;
    } else { // Frequency >= 4800
      Tdqs2dqPs = MRC_DDR5_tRX_DQS2DQ_4800;
    }
  }
  // Convert to PS
  Tdqs2dqPs *= UIPs;

  // Divice by 100 due to 100UI factor
  Tdqs2dqPs /= 100;

  return (Tdqs2dqPs * 1000); // Convert Picoseconds to Femptoseconds
}

/**
  This function issues the PDA Enumeration ID MPC. This will first send a MPC to enter PDA Enumeration Mode.
  It will then iterate through all DRAM devices and send an MPC to assign it an ID number, starting at 0.
  Once the iteration is done, a MPC will be sent to exit PDA Mode. This will wait tMPC Delay between PDA Entry
  and the first PDA Enumerate ID MPC's. This will wait tPDA_DELAY between the PDA Enumerate ID MPC's.

  @param[in]  MrcData       - Pointer to global MRC data

  @retval MrcStatus - mrcTimeout if the FSM does not complete after 1s
  @retval MrcStatus - mrcSuccess if the MPC is sent successfuly
  @retval MrcStatus - mrcFail for unexepected failures
**/
MrcStatus
MrcPdaEnumeration (
  IN MrcParameters *const MrcData
  )
{
  const MRC_FUNCTION  *MrcCall;
  MrcStatus     Status;
  MrcOutput     *Outputs;
  MrcInput      *Inputs;
  MrcChannelIn  *ChannelIn;
  MrcChannelOut *ChannelOut;
  MrcDimmOut    *DimmOut;
  MrcDebug      *Debug;
  INT64         GetSetVal;
  INT64         GetSetDis;
  INT64         tCWL4TxDqFifoWrEnSave[MAX_CONTROLLER][MAX_CHANNEL];
  INT64         tCWL4TxDqFifoRdEnSave[MAX_CONTROLLER][MAX_CHANNEL];
  INT64         tCWL4TxDqFifoWrEn;
  INT64         tCWL4TxDqFifoRdEn;
  INT64         TcwlSave;
  INT64         TcwlDecSave[MAX_CONTROLLER][MAX_CHANNEL];
  INT64         TcwlAddSave[MAX_CONTROLLER][MAX_CHANNEL];
  INT64         TxDqsSave[MAX_CONTROLLER][MAX_CHANNEL][MAX_RANK_IN_CHANNEL][MAX_BYTE_IN_DDR5_CHANNEL];
  INT64         OdtParkModeSave;
  UINT32        DecTcwl;
  UINT32        AddTcwl;
  UINT32        Controller;
  UINT32        Channel;
  UINT32        TcwlCalc;
  UINT32        FirstController;
  UINT32        TckPs;
  UINT32        tpda_delay;
  UINT32        Strobe;
  UINT32        Offset;
  UINT32        DataSrzCtlSave;
  UINT32        Rank;
  UINT8         Byte;
  UINT8         DramByte;
  UINT8         NumDevices;
  UINT8         Device;
  UINT8         DimmIdx;
  UINT8         FirstChannel;
  UINT8         BytesPerDevice;
  BOOLEAN       Ddr5;
  UINT8         CpuByteList[MAX_BYTE_IN_DDR5_CHANNEL];
  UINT8         PdaIdList[MAX_BYTE_IN_DDR5_CHANNEL];
  DATA_CR_SRZCTL_STRUCT DataSrzCtl;

  Outputs           = &MrcData->Outputs;
  Inputs            = &MrcData->Inputs;
  MrcCall           = Inputs->Call.Func;
  Status            = mrcSuccess;
  Debug             = &Outputs->Debug;
  FirstController   = Outputs->FirstPopController;
  FirstChannel      = Outputs->Controller[FirstController].FirstPopCh;
  Ddr5              = (Outputs->DdrType == MRC_DDR_TYPE_DDR5);
  TckPs             = Outputs->tCKps;
  GetSetDis         = 0;

  // Only run PDA Enumeration on DDR5
  if (!Ddr5) {
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Skip PDA Enumeration, only supported for DDR5 \n");
    return Status;
  }

  // Initialize arrays
  MrcCall->MrcSetMem ((UINT8 *) CpuByteList, sizeof (CpuByteList), 0);
  MrcCall->MrcSetMem ((UINT8 *) PdaIdList, sizeof (PdaIdList), 0);
  MrcCall->MrcSetMem ((UINT8 *) tCWL4TxDqFifoWrEnSave, sizeof (tCWL4TxDqFifoWrEnSave), 0);
  MrcCall->MrcSetMem ((UINT8 *) tCWL4TxDqFifoRdEnSave, sizeof (tCWL4TxDqFifoRdEnSave), 0);
  MrcCall->MrcSetMem ((UINT8 *) TcwlDecSave, sizeof (TcwlDecSave), 0);
  MrcCall->MrcSetMem ((UINT8 *) TcwlAddSave, sizeof (TcwlAddSave), 0);
  MrcCall->MrcSetMem ((UINT8 *) TxDqsSave, sizeof (TxDqsSave), 0);

  MrcGetSetChStrb(MrcData, FirstController, FirstChannel, 0, GsmIocDataDqsOdtParkMode, ReadNoCache, &OdtParkModeSave);
  // Drive DQS low
  GetSetVal = 1;
  MrcGetSetChStrb (MrcData, MAX_CONTROLLER, MAX_CHANNEL, MAX_SDRAM_IN_DIMM, GsmIocDataDqsOdtParkMode, WriteToCache, &GetSetVal);

  Controller  = FirstController;
  Channel     = FirstChannel;
  Rank        = 0;  // Rank is not used here
  Strobe      = 0;
  MrcTranslateSystemToIp (MrcData, &Controller, &Channel, &Rank, &Strobe, GsmIocTxOn); // No GetSet for this register, so use another per-byte PHY CR

  // No GetSet for this register, so use another per-byte PHY CR from above for Channel/Strobe translation
  Offset = DATA0CH0_CR_SRZCTL_REG +
          (DATA0CH1_CR_SRZCTL_REG - DATA0CH0_CR_SRZCTL_REG) * Channel +
          (DATA1CH0_CR_SRZCTL_REG - DATA0CH0_CR_SRZCTL_REG) * Strobe;

  DataSrzCtl.Data = MrcReadCR (MrcData, Offset);
  DataSrzCtlSave = DataSrzCtl.Data;
  DataSrzCtl.Bits.dqsbuf_cmn_bonus = 2;
  MrcWriteCrMulticast (MrcData, DATA_CR_SRZCTL_REG, DataSrzCtl.Data);

  // Set DQ Pins HIGH, to a known state
  GetSetVal = 0xFF;
  MrcGetSetChStrb (MrcData, MAX_CONTROLLER, MAX_CHANNEL, MAX_SDRAM_IN_DIMM, GsmIocDqOverrideData, WriteToCache, &GetSetVal);
  MrcGetSetChStrb (MrcData, MAX_CONTROLLER, MAX_CHANNEL, MAX_SDRAM_IN_DIMM, GsmIocDqOverrideEn,   WriteToCache, &GetSetVal);

  // Save all tCWL and related settings
  MrcGetSetMcCh (MrcData, FirstController, FirstChannel, GsmMctCWL, ReadNoCache, &TcwlSave);

  // Set custom tCWL settings for PDA Enumeration
  // tCWL = 11.5ns - This is the middle of the tPDA_DQS_Delay range
  TcwlCalc = DIVIDECEIL (11500, TckPs);

  // Add 3ns to adjust for internal delays
  TcwlCalc += DIVIDECEIL (3000, TckPs);
  DecTcwl = 0;
  AddTcwl = 0;
  for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
    for (Channel = 0; Channel < Outputs->MaxChannels; Channel++) {
      if (!MrcChannelExist (MrcData, Controller, Channel)) {
        continue;
      }
      MrcGetSetMcCh (MrcData, Controller, Channel, GsmMctCWLDec, ReadNoCache, &TcwlDecSave[Controller][Channel]);
      MrcGetSetMcCh (MrcData, Controller, Channel, GsmMctCWLAdd, ReadNoCache, &TcwlAddSave[Controller][Channel]);
      MrcGetSetMcCh (MrcData, Controller, Channel, TxDqFifoWrEnTcwlDelay, ReadNoCache, &tCWL4TxDqFifoWrEnSave[Controller][Channel]);
      MrcGetSetMcCh (MrcData, Controller, Channel, TxDqFifoRdEnTcwlDelay, ReadNoCache, &tCWL4TxDqFifoRdEnSave[Controller][Channel]);

      MrcGetTxDqFifoDelays (MrcData, Controller, Channel, TcwlCalc, AddTcwl, DecTcwl, &tCWL4TxDqFifoWrEn, &tCWL4TxDqFifoRdEn);

      MrcGetSetMcCh (MrcData, Controller, Channel, GsmMctCWLDec, WriteToCache, &GetSetDis);
      MrcGetSetMcCh (MrcData, Controller, Channel, GsmMctCWLAdd, WriteToCache, &GetSetDis);
      MrcGetSetMcCh (MrcData, Controller, Channel, TxDqFifoWrEnTcwlDelay, WriteToCache, &tCWL4TxDqFifoWrEn);
      MrcGetSetMcCh (MrcData, Controller, Channel, TxDqFifoRdEnTcwlDelay, WriteToCache, &tCWL4TxDqFifoRdEn);
      for (Rank = 0; Rank < MAX_RANK_IN_CHANNEL; Rank++) {
        if (MrcRankExist (MrcData, Controller, Channel, Rank)) {
          for (Byte = 0; Byte < Outputs->SdramCount; Byte++) {
            MrcGetSetStrobe (MrcData, Controller, Channel, Rank, Byte, TxDqsDelay, ReadFromCache, &TxDqsSave[Controller][Channel][Rank][Byte]);
          }
        }
      }
    }
  }

  MrcGetSetStrobe (MrcData, MAX_CONTROLLER, MAX_CHANNEL, MAX_RANK_IN_CHANNEL, MAX_SDRAM_IN_DIMM, TxDqsDelay, WriteToCache, &GetSetDis);
  GetSetVal = TcwlCalc;
  MrcGetSetMcCh (MrcData, MAX_CONTROLLER, MAX_CHANNEL, GsmMctCWL, WriteToCache, &GetSetVal);

  // Force ON TX in the Data bytes
  GetSetVal = 1;
  MrcGetSetChStrb (MrcData, MAX_CONTROLLER, MAX_CHANNEL, MAX_SDRAM_IN_DIMM, GsmIocTxOn, WriteToCache, &GetSetVal);
  MrcGetSetChStrb (MrcData, MAX_CONTROLLER, MAX_CHANNEL, MAX_SDRAM_IN_DIMM, GsmIocTxPiPwrDnDis, WriteToCache, &GetSetVal);

  MrcFlushRegisterCachedData (MrcData);

  // Prepare GetSetVal for use below
  GetSetVal = 0xFF;
  for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
    for (Channel = 0; Channel < Outputs->MaxChannels; Channel++) {
      for (Rank = 0; Rank < MAX_RANK_IN_CHANNEL; Rank++) {
        if (!MrcRankExist (MrcData, Controller, Channel, Rank)) {
          continue;
        }
        ChannelOut = &Outputs->Controller[Controller].Channel[Channel];
        ChannelIn  = &Inputs->Controller[Controller].Channel[Channel];
        DimmIdx = (UINT8) RANK_TO_DIMM_NUMBER (Rank);
        DimmOut = &ChannelOut->Dimm[DimmIdx];

        // Channel Width (ddr5 = 32 Bytes) / SdramWidth (x8 or x16)
        NumDevices = DimmOut->PrimaryBusWidth / DimmOut->SdramWidth;
        BytesPerDevice = DimmOut->SdramWidth / 8;
        if ((DimmOut->SdramWidth == 8) && DimmOut->EccSupport) {
          NumDevices += 1;
        }

        if (Outputs->BitByteSwizzleFound) {
          DramByte = 0;
          for (Device = 0; Device < NumDevices; Device++) {
            for (Byte = 0; Byte < Outputs->SdramCount; Byte++) {
              if (!MrcByteExist (MrcData, Controller, Channel, Byte)) {
                continue;
              }
              if (ChannelIn->DqsMapCpu2Dram[DimmIdx][Byte] == DramByte) {
                CpuByteList[Device] = Byte;
                PdaIdList[Device] = Device;
                DramByte = DramByte + BytesPerDevice;
                break;
              }
            } // Byte
          } // DevId
        } else { // PDA Enumeration before bit/byte swizzle is found
          for (Byte = 0; Byte < Outputs->SdramCount; Byte++) {
            if (!MrcByteExist (MrcData, Controller, Channel, Byte)) {
              continue;
            }
            // Initialize CpuByteList and PdaIdList for first PDA Enumeration
            CpuByteList[Byte] = Byte;
            PdaIdList[Byte] = Byte;

            // Set Inital values for Byte mapping
            ChannelIn->DqsMapCpu2Dram[DimmIdx][Byte] = Byte;
            // Use max for NumDevices, until the Byte Swizzle detection is completed.
            NumDevices = Outputs->SdramCount;
          } // Byte
        }

        // Enter PDA Enumerate ID
        Status = MrcIssueMpc (MrcData, Controller, Channel, Rank, DDR5_MPC_ENTER_PDA_ENUM_PROG_MODE, MRC_PRINTS_OFF);

        // Set up each device
        for (Device = 0; Device < NumDevices; Device++) {

          // Set DQ Pins LOW
          MrcGetSetChStrb (MrcData, Controller, Channel, CpuByteList[Device], GsmIocDqOverrideData, WriteToCache, &GetSetDis);
          MrcFlushRegisterCachedData (MrcData);

          // Send PDA Enumerate ID
          Status = MrcIssueMpc (MrcData, Controller, Channel, Rank, DDR5_MPC_PDA_ENUMERATE_ID (PdaIdList[Device]), MRC_PRINTS_OFF);

          // Set DQ Pins back to HIGH
          MrcGetSetChStrb (MrcData, Controller, Channel, CpuByteList[Device], GsmIocDqOverrideData, WriteToCache, &GetSetVal);
          MrcFlushRegisterCachedData (MrcData);

          // wait tpda_delay
          tpda_delay = MrcGetPdaDelay (MrcData);
          MrcWait (MrcData, tpda_delay * MRC_TIMER_1NS);
        }
        //Exit PDA Enumerate ID
        Status = MrcIssueMpc (MrcData, Controller, Channel, Rank, DDR5_MPC_EXIT_PDA_ENUM_PROG_MODE, MRC_PRINTS_OFF);
      }
    }
  }

  // Disable DQ override
  MrcGetSetChStrb (MrcData, MAX_CONTROLLER, MAX_CHANNEL, MAX_SDRAM_IN_DIMM, GsmIocDqOverrideEn,   WriteToCache, &GetSetDis);
  MrcGetSetChStrb (MrcData, MAX_CONTROLLER, MAX_CHANNEL, MAX_SDRAM_IN_DIMM, GsmIocDqOverrideData, WriteToCache, &GetSetDis);

  MrcWriteCrMulticast (MrcData, DATA_CR_SRZCTL_REG, DataSrzCtlSave);

  // Restore ODTParkMode
  MrcGetSetChStrb (MrcData, MAX_CONTROLLER, MAX_CHANNEL, MAX_SDRAM_IN_DIMM, GsmIocDataDqsOdtParkMode, WriteToCache, &OdtParkModeSave);

  // Restore all tCWL settings
  MrcGetSetMcCh (MrcData, MAX_CONTROLLER, MAX_CHANNEL, GsmMctCWL, WriteToCache, &TcwlSave);
  for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
    for (Channel = 0; Channel < Outputs->MaxChannels; Channel++) {
      if (!MrcChannelExist (MrcData, Controller, Channel)) {
        continue;
      }
      MrcGetSetMcCh (MrcData, Controller, Channel, GsmMctCWLDec, WriteToCache, &TcwlDecSave[Controller][Channel]);
      MrcGetSetMcCh (MrcData, Controller, Channel, GsmMctCWLAdd, WriteToCache, &TcwlAddSave[Controller][Channel]);
      MrcGetSetMcCh (MrcData, Controller, Channel, TxDqFifoWrEnTcwlDelay, WriteToCache, &tCWL4TxDqFifoWrEnSave[Controller][Channel]);
      MrcGetSetMcCh (MrcData, Controller, Channel, TxDqFifoRdEnTcwlDelay, WriteToCache, &tCWL4TxDqFifoRdEnSave[Controller][Channel]);
      for (Rank = 0; Rank < MAX_RANK_IN_CHANNEL; Rank++) {
        if (MrcRankExist (MrcData, Controller, Channel, Rank)) {
          for (Byte = 0; Byte < Outputs->SdramCount; Byte++) {
            MrcGetSetStrobe (MrcData, Controller, Channel, Rank, Byte, TxDqsDelay, WriteToCache, &TxDqsSave[Controller][Channel][Rank][Byte]);
          }
        }
      }
    }
  }

  MrcGetSetChStrb (MrcData, MAX_CONTROLLER, MAX_CHANNEL, MAX_SDRAM_IN_DIMM, GsmIocTxOn, WriteToCache, &GetSetDis);
  MrcGetSetChStrb (MrcData, MAX_CONTROLLER, MAX_CHANNEL, MAX_SDRAM_IN_DIMM, GsmIocTxPiPwrDnDis, WriteToCache, &GetSetDis);

  MrcFlushRegisterCachedData (MrcData);

  return Status;
}

/**
  Enable/Disable DDR5 Write Leveling mode on DRAM

  @param[in] MrcData     - Include all MRC global data.
  @param[in] Rank        - Rank to work on
  @param[in] WriteLvMode - Write leveling mode to enable (Internal/External)
                            Ddr5WrLvlModeDisable,   // MR2[1]: Write Leveling = 0, MR2[7]: Internal Write Timing = 0
                            Ddr5WrLvlModeExternal,  // MR2[1]: Write Leveling = 1, MR2[7]: Internal Write Timing = 0
                            Ddr5WrLvlModeInternal,  // MR2[1]: Write Leveling = 1, MR2[7]: Internal Write Timing = 1
                            Ddr5WrLvlModeFinal      // MR2[1]: Write Leveling = 0, MR2[7]: Internal Write Timing = 1

  @retval - mrcSuccess if no errors encountered, mrcFail otherwise
**/
MrcStatus
MrcDdr5SetDramWrLvlMode (
  IN MrcParameters  *const MrcData,
  IN UINT32                Rank,
  IN Ddr5WrLvlMode   const WrLevelingMode
  )
{
  UINT32                      Controller;
  UINT32                      Channel;
  UINT8                       RankMask;
  UINT32                      RankHalf;
  UINT32                      RankMod2;
  UINT32                      ValidRankMask;
  MrcStatus                   Status;
  MrcStatus                   MrsStatus;
  MrcChannelOut               *ChannelOut;
  MrcOutput                   *Outputs;
  DDR5_MODE_REGISTER_2_TYPE   MR2;

  Outputs       = &MrcData->Outputs;
  ValidRankMask = Outputs->ValidRankMask;
  Status        = mrcSuccess;

  RankMask = 1 << Rank;
  if ((RankMask & ValidRankMask) == 0) {
    return Status;
  }
  RankHalf = Rank / MAX_RANK_IN_DIMM;
  RankMod2 = Rank % MAX_RANK_IN_DIMM;

  for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
    for (Channel = 0; Channel < MAX_CHANNEL; Channel++) {
      if (!(MrcRankExist (MrcData, Controller, Channel, Rank))) {
        continue;
      }
      ChannelOut = &Outputs->Controller[Controller].Channel[Channel];
      MR2.Data8 = (UINT8) ChannelOut->Dimm[RankHalf].Rank[RankMod2].MR[mrMR2];
      MR2.Bits.WriteLevelingTraining = ((WrLevelingMode == Ddr5WrLvlModeDisable) || (WrLevelingMode == Ddr5WrLvlModeFinal))    ? 0 : 1;
      MR2.Bits.InternalWriteTiming   = ((WrLevelingMode == Ddr5WrLvlModeDisable) || (WrLevelingMode == Ddr5WrLvlModeExternal)) ? 0 : 1;
      MrsStatus = MrcWriteMRS (MrcData, Controller, Channel, RankMask, mrMR2, (UINT16) MR2.Data8);
      if (MrsStatus != mrcSuccess) {
        Status |= MrsStatus;
      }
      ChannelOut->Dimm[RankHalf].Rank[RankMod2].MR[mrMR2] = MR2.Data8;
    }  // for Channel
  } // Controller

  return Status;
}

/**
  Set CAS latency on on All DRAMs in the system

  @param[in] MrcData     - Include all MRC global data.
  @param[in] CasLatency  - CAS latency value:
                            00000B: 22
                            00001B: 24
                            00010B: 26
                            00011B: 28
                            ...
                            10011B: 60
                            10100B: 62
                            10101B: 64
                            10110B: 66
                            All other encodings reserved.

  @retval - mrcSuccess if no errors encountered, mrcFail otherwise
**/
MrcStatus
MrcDdr5SetCasLatency (
  IN MrcParameters  *const MrcData,
  UINT8                    CasLatency
  )
{
  UINT32                      Controller;
  UINT32                      Channel;
  UINT8                       Rank;
  UINT8                       RankHalf;
  UINT8                       RankMod2;
  UINT8                       ValidRankMask;
  UINT8                       RankMask;
  MrcStatus                   Status;
  MrcStatus                   MrsStatus;
  MrcChannelOut               *ChannelOut;
  MrcOutput                   *Outputs;
  DDR5_MODE_REGISTER_0_TYPE   MR0;

  Outputs       = &MrcData->Outputs;
  ValidRankMask = Outputs->ValidRankMask;
  Status        = mrcSuccess;


  for (Rank = 0; Rank < MAX_RANK_IN_CHANNEL; Rank++) {
    RankMask = (1 << Rank);
    if (!(RankMask & ValidRankMask)) {
      // Skip if all channels empty
      continue;
    }

    RankHalf      = Rank / MAX_RANK_IN_DIMM;
    RankMod2      = Rank % MAX_RANK_IN_DIMM;

    for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
      for (Channel = 0; Channel < MAX_CHANNEL; Channel++) {
        if (!(MrcRankExist (MrcData, Controller, Channel, Rank))) {
          continue;
        }
        ChannelOut = &Outputs->Controller[Controller].Channel[Channel];
        MR0.Data8 = (UINT8) ChannelOut->Dimm[RankHalf].Rank[RankMod2].MR[mrMR0];
        MR0.Bits.CasLatency = CasLatency;
        MrsStatus = MrcWriteMRS (MrcData, Controller, Channel, 1 << Rank, mrMR0, (UINT16) MR0.Data8);
        if (MrsStatus != mrcSuccess) {
          Status |= MrsStatus;
        }
      }  // for Channel
    } // Controller
  }

  return Status;
}

/**
  This function sets Write Leveling Internal Cycle delay through MR3 commands
  to the provided controller/channels

  @param[in] MrcData          - Include all MRC global data.
  @param[in] Controller       - Index of the requested Controler
  @param[in] Channel          - Index of the requested Channel
  @param[in] Rank             - Current working rank
  @param[in] IntCycleAlignLow - Write Leveling Internal Cycle Alignment (Lower Byte)
  @param[in] IntCycleAlignUp  - Write Leveling Internal Cycle Alignment (Upper Byte)
                                0000B: Disable (Default)
                                0001B: -1 tCK
                                0010B: -2 tCK
                                0011B: -3 tCK
                                0100B: -4 tCK
                                0101B: -5 tCK
                                0110B: -6 tCK
                                0111B: -7 tCK
                                1000B~ 1111B: RFU
  @param[in]  DebugPrint - When TRUE, will print debugging information

  @retval - mrcSuccess if no errors encountered, mrcFail otherwise
**/
MrcStatus
MrcSetMR3_DDR5 (
  IN MrcParameters  *const MrcData,
  IN UINT32                Controller,
  IN UINT32                Channel,
  IN UINT32                Rank,
  IN UINT8                 IntCycleAlignLow,
  IN UINT8                 IntCycleAlignUp,
  IN BOOLEAN               DebugPrint
  )
{
  UINT32                      ControllerStart;
  UINT32                      ControllerEnd;
  UINT32                      ControllerLoop;
  UINT32                      ChannelStart;
  UINT32                      ChannelEnd;
  UINT32                      ChannelLoop;
  UINT32                      RankHalf;
  UINT32                      RankMod2;
  MrcStatus                   Status;
  MrcStatus                   MrsStatus;
  MrcChannelOut               *ChannelOut;
  MrcOutput                   *Outputs;
  DDR5_MODE_REGISTER_3_TYPE   MR3;
  MrcDebug                    *Debug;

  Outputs       = &MrcData->Outputs;
  RankHalf      = Rank / MAX_RANK_IN_DIMM;
  RankMod2      = Rank % MAX_RANK_IN_DIMM;
  Status        = mrcSuccess;
  Debug         = &Outputs->Debug;

  if (Controller >= MAX_CONTROLLER) {
    ControllerStart = 0;
    ControllerEnd = MAX_CONTROLLER;
  } else {
    ControllerStart = Controller;
    ControllerEnd = Controller + 1;
  }
  if (Channel >= Outputs->MaxChannels) {
    ChannelStart = 0;
    ChannelEnd = (UINT32) Outputs->MaxChannels;
  } else {
    ChannelStart = Channel;
    ChannelEnd = Channel + 1;
  }

  for (ControllerLoop = ControllerStart; ControllerLoop < ControllerEnd; ControllerLoop++) {
    for (ChannelLoop = ChannelStart; ChannelLoop < ChannelEnd; ChannelLoop++) {
      if (!(MrcRankExist (MrcData, ControllerLoop, ChannelLoop, Rank))) {
        continue;
      }
      ChannelOut = &Outputs->Controller[ControllerLoop].Channel[ChannelLoop];
      MR3.Data8 = (UINT8) ChannelOut->Dimm[RankHalf].Rank[RankMod2].MR[mrMR3];
      // Lower Byte WL Internal Cycle Alignment is intended for x4, x8, and x16 configurations.
      // Upper Byte WL Internal Cycle Alignment is intended for x16 configuration only.
      MR3.Bits.WriteLevelingInternalCycleLowerByte = IntCycleAlignLow;
      MR3.Bits.WriteLevelingInternalCycleUpperByte = IntCycleAlignUp;
      if (DebugPrint) {
        MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE,
          "MrcSetMR3_DDR5: MC%d, C%d, R%d, Lower = 0x%02X, Upper = 0x%02X\n",
          ControllerLoop, ChannelLoop, Rank, IntCycleAlignLow, IntCycleAlignUp);
      }
      MrsStatus = MrcWriteMRS (MrcData, ControllerLoop, ChannelLoop, 1 << Rank, mrMR3, (UINT16) MR3.Data8);
      if (MrsStatus != mrcSuccess) {
        Status |= MrsStatus;
      }
      ChannelOut->Dimm[RankHalf].Rank[RankMod2].MR[mrMR3] = MR3.Data8;
    }  // for ChannelLoop
  } // ControllerLoop

  return Status;
}

/**
  This function selects the DQ ODT table according to DIMM/rank population.

  @param[in] MrcData         - Include all the MRC general data.
  @param[in] Dimm            - selected DIMM.
  @param[in] OdtIndex        - selected ODT index.

  @retval TOdtValueDqDdr5 * - Pointer to the relevant table or NULL if the table was not found.
**/
TOdtValueDqDdr5 *
SelectTable_DDR5 (
  IN MrcParameters *const MrcData,
  IN const UINT32         Dimm,
  IN const TOdtIndex      OdtIndex
  )
{
  TOdtValueDqDdr5  *OdtTable;

  OdtTable = NULL;
  if ((OdtIndex > oi2DPC2R2R) || (Dimm >= MAX_DIMMS_IN_CHANNEL)) {
    MRC_DEBUG_MSG (&MrcData->Outputs.Debug, MSG_LEVEL_ERROR, "%s SelectTable_DDR5: array out of bounds! OdtIndex: %u, Dimm: %u\n", gErrString, OdtIndex, Dimm);
    return NULL;
  }

  if (OdtIndex < oi2DPC1R1R) {
    OdtTable = (TOdtValueDqDdr5 *) &Ddr5DqOdtTable1Dpc[Dimm][OdtIndex];
  } else { // 2DPC
      OdtTable = (TOdtValueDqDdr5 *) &Ddr5OdtTable2Dpc[Dimm][OdtIndex];
  }

  return OdtTable;
}

/**
  This function selects the CCC ODT table according to DIMM/Rank population.

  @param[in] MrcData         - Include all the MRC general data.
  @param[in] Controller      - selected Controller.
  @param[in] Channel         - selected Channel.
  @param[in] Rank            - selected Rank.

  @retval TOdtValueCccDdr5 * - Pointer to the relevant table or NULL if the table was not found.
**/
TOdtValueCccDdr5 *
SelectCccTable_DDR5 (
  IN MrcParameters *const MrcData,
  IN const UINT32         Controller,
  IN const UINT32         Channel,
  IN const UINT32         Rank
  )
{
  MrcOutput         *Outputs;
  MrcChannelOut     *ChannelOut;
  MrcDimmOut        *DimmOut;
  UINT32            Dimm;
  UINT32            RanksInDimm0;
  UINT32            RanksInDimm1;
  TOdtIndex         OdtIndex;
  TOdtValueCccDdr5  *OdtTable;
  MrcInput          *Inputs;

  Inputs       = &MrcData->Inputs;
  Outputs      = &MrcData->Outputs;
  ChannelOut   = &Outputs->Controller[Controller].Channel[Channel];
  Dimm         = RANK_TO_DIMM_NUMBER (Rank);
  DimmOut      = &ChannelOut->Dimm[Dimm];
  RanksInDimm0 = ChannelOut->Dimm[dDIMM0].RankInDimm;
  RanksInDimm1 = ChannelOut->Dimm[dDIMM1].RankInDimm;
  OdtIndex     = oiNotValid;
  OdtTable     = NULL;

  switch (ChannelOut->DimmCount) {
    case 1: // 1DPC
      OdtIndex = (DimmOut->RankInDimm == 2) ? oi1DPC2R : oi1DPC1R;
      break;

    case 2: // 2DPC
      if ((RanksInDimm0 == 1) && (RanksInDimm1 == 1)) {
        OdtIndex = oi2DPC1R1R;
      } else if ((RanksInDimm0 == 1) && (RanksInDimm1 == 2)) {
        OdtIndex = oi2DPC1R2R;
      } else if ((RanksInDimm0 == 2) && (RanksInDimm1 == 1)) {
        OdtIndex = oi2DPC2R1R;
      } else if ((RanksInDimm0 == 2) && (RanksInDimm1 == 2)) {
        OdtIndex = oi2DPC2R2R;
      } else {
        MRC_DEBUG_MSG (&Outputs->Debug, MSG_LEVEL_ERROR, "%s: Invalid %uDPC rank mode: RanksInDimm0=%u, RanksInDimm1=%u\n", gErrString, 2, RanksInDimm0, RanksInDimm1);
      }
      break;

    default:
      break;
  }

  if (OdtIndex < oi2DPC1R1R) {
    OdtTable = (TOdtValueCccDdr5 *) &Ddr5CccOdtTable1Dpc[Dimm][OdtIndex][0];
  } else { // 2DPC
    if (Inputs->G0) {
      OdtTable = (TOdtValueCccDdr5 *) &Ddr5CccOdtTable2DpcG0[Dimm][OdtIndex][0];
    } else {
      OdtTable = (TOdtValueCccDdr5 *) &Ddr5CccOdtTable2Dpc[Dimm][OdtIndex][0];
    }
  }

  return OdtTable;
}

/**
  This function converts from Ohms to DDR5 MR5 PU/PD Drive Strength encoding.

  @param[in]  DrvStrValue  - Ron Value in Ohms.

  @retval INT8 - Encoding if valid Ron value.  Else, -1.
**/
INT8
Ddr5DrvStrEncode (
  IN  UINT16  DrvStrValue
  )
{
  INT8      EncodeVal;

  switch (DrvStrValue) {
    case 48:
      EncodeVal = DDR5_ODIC_48;
      break;

    case 40:
      EncodeVal = DDR5_ODIC_40;
      break;

    case 34:
      EncodeVal = DDR5_ODIC_34;
      break;

    default:
      EncodeVal = -1;
      break;
  }

  return EncodeVal;
}

/**
  This function converts from DDR5 MR5 PU/PD Drive Strength encoding to Ohms.

  @param[in]  DecodeVal - Decoded Ron value.

  @retval INT16 - Ron Value in Ohms if valid. Else, -1.
**/
INT16
DdrDrvStrDecode (
  IN  UINT16 DecodeVal
  )
{
  INT16      RonValue;

  switch (DecodeVal) {
    case DDR5_ODIC_48:
      RonValue = 48;
      break;

    case DDR5_ODIC_40:
      RonValue = 40;
      break;

    case DDR5_ODIC_34:
      RonValue = 34;
      break;

    default:
      RonValue = -1;
      break;
  }

  return RonValue;
}

/**
  This function converts from Ohms to MR ODT encoding.

  @param[in]  OdtValue  - ODT Value in Ohms.

  @retval INT8 - Encoding if valid ODT value.  Else, -1.
**/
INT8
CccOdtEncode_DDR5 (
  IN  UINT32  OdtValue
  )
{
  INT8      EncodeVal;

  switch (OdtValue) {
    case 0xFFFF:
    case 0:
      EncodeVal = CkCsCaOdt_RTT_OFF;
      break;

    case 480:
      EncodeVal = CkCsCaOdt_RZQ_0p5_480;
      break;

    case 240:
      EncodeVal = CkCsCaOdt_RZQ_1_240;
      break;

    case 120:
      EncodeVal = CkCsCaOdt_RZQ_2_120;
      break;

    case 80:
      EncodeVal = CkCsCaOdt_RZQ_3_80;
      break;

    case 60:
      EncodeVal = CkCsCaOdt_RZQ_4_60;
      break;

    case 40:
      EncodeVal = CkCsCaOdt_RZQ_6_40;
      break;

    default:
      EncodeVal = -1;
      break;
  }

  return EncodeVal;
}

/**
  Used to update TxVref for DDR5.
  Uses input offset value to increment/decrement current setting.

  @param[in,out] MrcData        - Include all MRC global data.
  @param[in]     Controller     - Selecting which Controller to talk to.
  @param[in]     Channel        - Selecting which Channel to talk to.
  @param[in]     RankMask       - Selecting which Ranks to talk to.
  @param[in]     DeviceMask     - Selecting which Devices to talk to (only valid for DDR5 and adjusting VrefDQ).
  @param[in]     VrefType       - Determines the Vref type to change, only CmdV and TxVref are valid.
  @param[in]     Offset         - Vref offset value.
  @param[in]     UpdateMrcData  - Used to decide if Mrc host must be updated.
  @param[in]     PdaMode        - Selecting if we are using DDR5 PDA.
  @param[in]     IsCachedOffset - Determines if the paramter is an offset (relative to cache) or absolute value.

  @retval MrcStatus - mrcWrongInputParameter if unsupported OptParam,  mrcSuccess otherwise
**/
MrcStatus
Ddr5SetDramVref (
  IN OUT MrcParameters *const MrcData,
  IN     UINT8                Controller,
  IN     UINT8                Channel,
  IN     UINT8                RankMask,
  IN     UINT16               DeviceMask,
  IN     UINT8                VrefType,
  IN     INT32                Offset,
  IN     BOOLEAN              UpdateMrcData,
  IN     BOOLEAN              PdaMode,
  IN     BOOLEAN              IsCachedOffset
  )
{
  MrcDebug      *Debug;
  MrcOutput     *Outputs;
  MrcChannelOut *ChannelOut;
  MrcRankOut    *RankOut;
  UINT16        *MrPtr;
  UINT8         Rank;
  UINT8         RankIdx;
  UINT8         MrIndex;
  UINT8         Device;
  INT32         CurrentOffset;
  UINT8         VrefCode;
  MrcDimmOut    *DimmOut;
  UINT8         NumDevices;
  UINT8         DimmIdx;
  BOOLEAN       ProgramPda;

  Outputs = &MrcData->Outputs;
  Debug = &Outputs->Debug;
  ChannelOut = &Outputs->Controller[Controller].Channel[Channel];
  MrIndex       = 0;
  CurrentOffset = 0;
  VrefCode      = 0;
  ProgramPda    = FALSE;

  switch (VrefType) {
  case CmdV:
    MrIndex = mrIndexMR11;
    ProgramPda = (PdaMode && ChannelOut->Mr11PdaEnabled);
    break;
  case WrV:
    MrIndex = mrIndexMR10;
    ProgramPda = (PdaMode && ChannelOut->Mr10PdaEnabled);
    break;
  default:
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_ERROR, "%s %s %u\n", gErrString, gWrongInputParam, VrefType);
    return mrcWrongInputParameter;
    break;
  }

  DeviceMask = (ProgramPda) ? DeviceMask : 1;
  //MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Ddr5SetDramVref: MC%u C%u RankMask 0x%X DeviceMask 0x%X -> Offset: %d PdaMode: %u UpdateMrcData: %u IsCachedOffset: %u\n",
  //  Controller, Channel, RankMask, DeviceMask, Offset, PdaMode, UpdateMrcData, IsCachedOffset);

  for (Rank = 0; Rank < MAX_RANK_IN_CHANNEL; Rank++) {
    if ((MrcRankExist(MrcData, Controller, Channel, Rank)) && (RankMask & (1 << Rank))) {
      DimmIdx = Rank / MAX_RANK_IN_DIMM;
      RankIdx = Rank % 2;
      DimmOut = &ChannelOut->Dimm[DimmIdx];
      RankOut = &DimmOut->Rank[RankIdx];
      MrPtr   = RankOut->MR;

      // Channel Width (ddr5 = 32 Bytes) / SdramWidth (x8 or x16)
      if (ProgramPda) {
        NumDevices = 32 / DimmOut->SdramWidth;
        if ((DimmOut->SdramWidth == 8) && DimmOut->EccSupport) {
          NumDevices += 1;
        }
      } else {
        NumDevices = 1;
      }
      for (Device = 0; Device < NumDevices; Device++) {
        if (DeviceMask & (1 << Device)) {
          if (IsCachedOffset) {
            // Offset value -  calculate the new Vref
            if (ProgramPda) {
              if (VrefType == CmdV) {
                VrefCode = RankOut->DdrPdaVrefCmd[Device];
              } else {
                VrefCode = RankOut->DdrPdaVrefDq[Device];
              }
            } else {
              VrefCode = (UINT8) MrPtr[MrIndex];
            }
            CurrentOffset = MrcVrefToOffsetDdr5 (VrefCode);
            CurrentOffset += Offset;
            // MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\nDdr5SetDramVref: MC%u C%u Dev: %u, VrefCode: %u, CurrentOffset: %d + %d = %d\n", Controller, Channel, Device, VrefCode, MrcVrefToOffsetDdr5 (VrefCode), Offset, CurrentOffset);
          } else {
            // If !IsCachedOffset
            CurrentOffset = Offset;
          }
          MrcSetTxVrefDdr5 (MrcData, Controller, Channel, Rank, Device, VrefType, CurrentOffset, UpdateMrcData, ProgramPda);
        }
      }
      // Apply CA Vref after it is programmed above
      if (VrefType == CmdV) {
        MrcIssueMpc (MrcData, Controller, Channel, Rank, DDR5_MPC_APPLY_VREF_RTT, MRC_PRINTS_OFF);
      }
      if (ProgramPda) {
        // Send PDA command of Index 15 to resume normal rank operation mode for MR/CAVREF/MPC
        MrcPdaSelect (MrcData, Controller, Channel, Rank, 15, MRC_PRINTS_OFF);
      }
    }
  }
  return mrcSuccess;
}

/**
  Set Tx Dimm Vref absolute Value for DDR5.
  Use custom MRC range [-52:+53] :: (45% - 97.5%) * 1.1V where offset zero is 71.0%

  @param[in, out] MrcData       - Include all MRC global data.
  @param[in]      Controller    - Memory Controller Number within the processor (0-based).
  @param[in]      Channel       - Selecting which Channel to talk to
  @param[in]      Rank          - Selecting which Rank to talk to
  @param[in]      Device        - Selecting which Device to talk to (only valid when PDAmode is TRUE)
  @param[in]      VrefType       - Determines the Vref type to change, only CmdV and TxVref are valid.
  @param[in]      TxVrefOffset  - TxVref Offset to set
  @param[in]      UpdateMrcData - update MRC host struct
  @param[in]      PdaMode       - Selecting to use MPC or old way of MRS

  @retval none
**/
VOID
MrcSetTxVrefDdr5 (
    IN OUT MrcParameters *const MrcData,
    IN     UINT32               Controller,
    IN     UINT8                Channel,
    IN     UINT8                Rank,
    IN     UINT8                Device,
    IN     UINT8                VrefType,
    IN     INT32                TxVrefOffset,
    IN     BOOLEAN              UpdateMrcData,
    IN     BOOLEAN              PdaMode
  )
{
  //MrcDebug                *Debug;
  MrcChannelOut           *ChannelOut;
  MrcOutput               *Outputs;
  MrcRankOut              *RankOut;
  UINT8                   Vref;
  UINT8                   ByteLoop;
  UINT8                   MrAddress;
  UINT32                  Delay;
  BOOLEAN                 ProgramPda;

  Outputs = &MrcData->Outputs;
  //Debug = &Outputs->Debug;
  ChannelOut = &Outputs->Controller[Controller].Channel[Channel];
  RankOut    = &ChannelOut->Dimm[(Rank / 2) % MAX_DIMMS_IN_CHANNEL].Rank[Rank % 2];
  MrAddress  = 0;
  ProgramPda = FALSE;

  switch (VrefType) {
    case CmdV:
      MrAddress = mrMR11;
      ProgramPda = (PdaMode && ChannelOut->Mr11PdaEnabled);
      break;
    case WrV:
      MrAddress = mrMR10;
      ProgramPda = (PdaMode && ChannelOut->Mr10PdaEnabled);
      break;
    default:
      break;
  }

  Vref = MrcOffsetToVrefDdr5 (TxVrefOffset);
  //MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Vref: %u\n", Vref);

  if (ProgramPda) {
    //MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "MrcSetTxVrefDdr5: PDA mode, Device %d\n", Device);
    MrcPdaSelect (MrcData, Controller, Channel, Rank, Device, MRC_PRINTS_OFF);
  }
  if (VrefType == CmdV) {
    MrcIssueVrefCa (MrcData, Controller, Channel, Rank, Vref, MRC_PRINTS_OFF);
  } else {
    // VrefType TxVref
    Delay = (tVREF_DQ_PS_DDR5 * MRC_TIMER_1NS) / 1000;
    MrcIssueMrw (MrcData, Controller, Channel, Rank, MrAddress, Vref, MRC_PRINTS_OFF);
    MrcWait (MrcData, Delay);
  }

  if (UpdateMrcData) {
    if (ProgramPda) {
      if (VrefType == CmdV) {
        RankOut->DdrPdaVrefCmd[Device] = Vref;
      } else {
        RankOut->DdrPdaVrefDq[Device] = Vref;
        // MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "MrcSetTxVrefDdr5: PDA mode, MC%u C%u R%u -> DdrPdaVrefDq[%u] = %d\n", Controller, Channel, Rank, Device, RankOut->DdrPdaVrefDq[Device]);
      }
    } else {
      if (VrefType == CmdV) {
        RankOut->MR[mrIndexMR11] = Vref;
      } else {
        RankOut->MR[mrIndexMR10] = Vref;
      }
      // Update the per byte Vref in case needed later
      for (ByteLoop = 0; ByteLoop < Outputs->SdramCount; ByteLoop++) {
        if (VrefType == CmdV) {
          RankOut->DdrPdaVrefCmd[ByteLoop] = Vref;
        } else {
          RankOut->DdrPdaVrefDq[ByteLoop] = Vref;
        }
      }
    }
  }
}


/**
  Offset to DDR5 VrefDQ Range/Value (MR10)
  Offset [-52:+53] - Vref 0 - 105 :: (45% - 97.5%) * 1.1V
  @param[in]  MrcData  - Include all MRC global data.
  @param[in]  Offset   - Value to be converted to actual VrefDQ Range/Value.

  @retval DDR4 VrefDQ Range/Value
**/
UINT8
MrcOffsetToVrefDdr5 (
  IN INT32                Offset
)
{
  INT32 VrefDQ;

  VrefDQ = 53 - Offset;
  return (UINT8)VrefDQ;
}

/**
  DDR5 VrefDQ Range/Value (MR10) to Offset
  Vref Range 0 - 105 - Offset [-52:+53] :: (45% - 97.5%) * 1.1V where offset zero is 71.0%

  @param[in]  VrefDQ   - VrefDQ Range/Value to be converted back to corresponding Offset value.

  @retval Offset
**/
INT32
MrcVrefToOffsetDdr5 (
  IN UINT8                VrefDQ
)
{
  INT32  Offset;

  Offset = 53 - VrefDQ;
  return Offset;
}

/**
  This function Update the Non PDA DDR5 Vref to match the PDA average
  across all bytes (per rank/ch).
  This will allow the use of Non PDA sweep using GetBerMarginByte

  @param[in]     MrcData         - Include all MRC global data.
  @param[in]     Controller      - Selecting which Controller to talk to.
  @param[in]     Channel         - Selecting which Channel to talk to.
  @param[in]     RankMask        - ranks to work on

  @retval mrcWrongInputParameter if a bad Param is passed in, otherwise mrcSuccess.
**/
MrcStatus
UpdatePdaCenterDdr5 (
  IN     MrcParameters *const MrcData,
  IN     const UINT8          Controller,
  IN     const UINT8          Channel,
  IN     const UINT8          RankMask,
  IN     UINT8                VrefType
)
{
  MrcOutput     *Outputs;
  MrcChannelOut *ChannelOut;
  MrcDimmOut    *DimmOut;
  MrcRankOut    *RankOut;
  UINT32        Rank;
  UINT32        Device;
  UINT32        NumDevices;
  UINT8         Vref;
  INT32         ByteCenterAvg;
  UINT8         MrIndex;
  MrcDebug      *Debug;

  Outputs = &MrcData->Outputs;
  Debug = &Outputs->Debug;
  MrIndex = 0;
  switch (VrefType) {
    case CmdV:
      MrIndex = mrIndexMR11;
      break;
    case WrV:
      MrIndex = mrIndexMR10;
      break;
    default:
      MRC_DEBUG_MSG (Debug, MSG_LEVEL_ERROR, "%s %u ", gWrongInputParam, VrefType);
      return mrcWrongInputParameter;
      break;
  }

  if (MrcChannelExist(MrcData, Controller, Channel)) {
    ChannelOut = &Outputs->Controller[Controller].Channel[Channel];
    for (Rank = 0; Rank < MAX_RANK_IN_CHANNEL; Rank++) {
      if (!MrcRankExist(MrcData, Controller, Channel, Rank)) {
        continue;
      }
      if ((1 << Rank) & RankMask) {
        DimmOut = &ChannelOut->Dimm[RANK_TO_DIMM_NUMBER(Rank)];
        RankOut = &DimmOut->Rank[Rank % 2];
        NumDevices = DimmOut->PrimaryBusWidth / DimmOut->SdramWidth;
        if ((DimmOut->SdramWidth == 8) && DimmOut->EccSupport) {
          NumDevices++;
        }
        ByteCenterAvg = 0;
        for (Device = 0; Device < NumDevices; Device++) {
          if (VrefType == CmdV) {
            ByteCenterAvg += MrcVrefToOffsetDdr5 (RankOut->DdrPdaVrefCmd[Device]);
          } else {
            ByteCenterAvg += MrcVrefToOffsetDdr5 (RankOut->DdrPdaVrefDq[Device]);
          }
        }
        ByteCenterAvg /= (INT32) NumDevices;
        Vref = MrcOffsetToVrefDdr5 (ByteCenterAvg);
        RankOut->MR[MrIndex] = Vref;
        // if (VrefType == WrV) MRC_DEBUG_MSG (Debug, MSG_LEVEL_ERROR, "UpdatePdaCenterDdr5: R%u MR[%u] = %d\n", Rank, MrIndex, RankOut->MR[MrIndex]); //<PK>
      }
    }
  }

  return mrcSuccess;
}

/**
  Get all the data for all Ranks all Devs for specific controller, channel and MR for DDR5

  Example 1: in DDR5 2R x8 with ECC the data will return as:
  MrPdaData[]= {R0D0,R1D0,R0D1,R1D1,R0D2,R1D2,R0D3,R1D3,R0ECC,R1ECC}

  Example 2: in DDR5 2DPC 1R-1R, DIMM0 x8, DIMM1 x16, no ECC - the data will return as:
  MrPdaData[]= {R0D0,R2D0,R0D1,R2D1,R0D2,R2D2,R0D3,R2D3}

  Example 3: in DDR5 2DPC 1R-1R, DIMM0 x16, DIMM1 x16, no ECC - the data will return as:
  MrPdaData[]= {R0D0,R2D0,R0D1,R2D1}

  Important:
  The *NumMrData can be different depending on the MR

  @param[in] MrcData      - Pointer to global MRC data.
  @param[in] Controller   - Controller to work on.
  @param[in] Channel      - channel to work on.
  @param[in] MrAddress    - MR Address
  @param[in] MrPdaData    - Array of Data that will be filled
  @param[in] NumMrData    - pointer that will contain # of data that filled in the array.

  @retval MrcStatus - mrcSuccess, otherwise an error status.
  **/
MrcStatus
MrFillPdaMrsDataDdr5 (
  IN      MrcParameters   *MrcData,
  IN      UINT32          Controller,
  IN      UINT32          Channel,
  IN      MrcModeRegister MrAddress,
  IN OUT  UINT8           MrPdaData[MAX_PDA_MR_IN_CHANNEL],
  IN OUT  UINT8           *NumMrData
  )
{
  UINT32                      RankMod2;
  UINT32                      Rank;
  MrcStatus                   Status;
  MrcChannelOut               *ChannelOut;
  MrcOutput                   *Outputs;
  UINT8                       DevsPerRank[MAX_RANK_IN_CHANNEL];
  UINT32                      DimmIdx;
  MrcDimmOut                  *DimmOut;
  MrcRankOut                  *RankOut;
  UINT32                      DeviceIdx;
  BOOLEAN                     IsAnyRankx8;

  Outputs    = &MrcData->Outputs;
  Status     = mrcSuccess;
  ChannelOut = &Outputs->Controller[Controller].Channel[Channel];
  *NumMrData = 0;

  if ((MrAddress != mrMR3) && (MrAddress != mrMR10) && (MrAddress != mrMR11) /*&& (MrAddress != mrMR43) && (MrAddress != mrMR44)*/) {
    MRC_DEBUG_MSG (&Outputs->Debug, MSG_LEVEL_ERROR, "MrFillPdaMrsDataDdr5 does not support  MR %d \n", MrAddress);
    return mrcWrongInputParameter;
  }

  // First see if any rank is populated with x8
  // In this case Generic MRS FSM will use 4 or 5 devices in a device loop, for all ranks, even if some ranks are x16.
  // Only if all ranks are x16 it will use 2 devices per loop.
  IsAnyRankx8 = FALSE;
  for (Rank = 0; Rank < MAX_RANK_IN_CHANNEL; Rank++) {
    if (!MrcRankExist (MrcData, Controller, Channel, Rank)) {
      continue;
    }
    DimmIdx = RANK_TO_DIMM_NUMBER(Rank);
    DimmOut = &ChannelOut->Dimm[DimmIdx];
    if (DimmOut->SdramWidth == 8) {
      IsAnyRankx8 = TRUE;
    }
  }

  // Fill the dev Rank array
  // some ranks can be x16 some x8
  for (Rank = 0; Rank < MAX_RANK_IN_CHANNEL; Rank++) {
    DevsPerRank[Rank] = 0;
    if (!MrcRankExist (MrcData, Controller, Channel, Rank)) {
      continue;
    }
    DimmIdx = RANK_TO_DIMM_NUMBER(Rank);
    DimmOut = &ChannelOut->Dimm[DimmIdx];
    // Channel Width (ddr5 = 32 Bytes) / SdramWidth (x8 or x16)
    DevsPerRank[Rank] = DimmOut->PrimaryBusWidth / DimmOut->SdramWidth;
    if (IsAnyRankx8) {
      DevsPerRank[Rank] = 4;
    }
    if ((DimmOut->SdramWidth == 8) && DimmOut->EccSupport) {
      DevsPerRank[Rank]++;
    }
  }

  // Get the data for all ranks and devices
  for (DeviceIdx = 0; DeviceIdx < MAX_BYTE_IN_DDR5_CHANNEL; DeviceIdx++) {
    for (Rank = 0; Rank < MAX_RANK_IN_CHANNEL; Rank++) {
      if (!MrcRankExist (MrcData, Controller, Channel, Rank)) {
        continue;
      }
      DimmIdx   = RANK_TO_DIMM_NUMBER (Rank);
      RankMod2  = Rank % MAX_RANK_IN_DIMM;
      RankOut = &ChannelOut->Dimm[DimmIdx].Rank[RankMod2];
      if (DeviceIdx < DevsPerRank[Rank]) {
        switch (MrAddress) {
          case mrMR3:
            MrPdaData[*NumMrData] = RankOut->Ddr5PdaMr3[DeviceIdx];
            break;
          case mrMR10:
            MrPdaData[*NumMrData] = RankOut->DdrPdaVrefDq[DeviceIdx];
            break;
          case mrMR11:
            MrPdaData[*NumMrData] = RankOut->DdrPdaVrefCmd[DeviceIdx];
            break;
          /*case mrMR43:
            MrPdaData[*NumMrData] = RankOut->DdrPdaMr43[DeviceIdx];
            break;
          case mrMR44:
            MrPdaData[*NumMrData] = RankOut->DdrPdaMr44[DeviceIdx];
            break;*/
          default:
            break;
        }
        (*NumMrData)++;
      }
    }
  }

  return Status;
}


/**
  This function completes setting up the Generic MRS FSM configuration to enable SAGV during normal operation.

  @param[in] MrcData  - Pointer to global MRC data.
  @param[in] Print    - Boolean control for debug print messages.

  @retval MrcStatus - mrcSuccess, otherwise an error status.
**/
MrcStatus
MrcFinalizeDdr5MrSeq (
  IN  MrcParameters *const MrcData,
  IN  BOOLEAN              Print
  )
{
  MrcStatus                   Status;
  Status        = mrcSuccess;

  static const MrcModeRegister Ddr5SagvMrOrder[] = {
    mpcMR13,          // tCCD_L is different per speed
    mpcDllReset,
    mpcSetCmdTiming,
    mpcMR32a0,
    mpcMR32a1,
    mpcMR33a0,
    mpcMR32b0,
    mpcMR32b1,
    mpcMR33b0,
    mrMR11,
    mrMR12,
    mpcMR33,
    mpcMR34,
    mpcApplyVrefCa,
    mrMR0,
    mrMR2,
    mrMR4,
    mrMR5,
    mrMR6,
    mrMR8,
    mrMR13,
    mrMR129,
    mrMR130,
    mrMR131,
    mrMR132,
    mrMR137,
    mrMR138,
    mrMR139,
    mrMR140,
    mrMR145,
    mrMR146,
    mrMR147,
    mrMR148,
    mrMR153,
    mrMR154,
    mrMR155,
    mrMR156,
    mrMR161,
    mrMR162,
    mrMR163,
    mrMR164,
    mrMR169,
    mrMR170,
    mrMR171,
    mrMR172,
    mrMR177,
    mrMR178,
    mrMR179,
    mrMR180,
    mrMR185,
    mrMR186,
    mrMR187,
    mrMR188,
    mrMR193,
    mrMR194,
    mrMR195,
    mrMR196,
    mrMR201,
    mrMR202,
    mrMR203,
    mrMR204,
    mrMR209,
    mrMR210,
    mrMR211,
    mrMR212,
    mrMR217,
    mrMR218,
    mrMR219,
    mrMR220,
    mrMR225,
    mrMR226,
    mrMR227,
    mrMR228,
    mrMR233,
    mrMR234,
    mrMR235,
    mrMR236,
    mrMR241,
    mrMR242,
    mrMR243,
    mrMR244,
    mrMR249,
    mrMR250,
    mrMR251,
    mrMR252,
    mrMR34,
    mrMR35,
    mrMR36,
    mrMR37,
    mrMR38,
    mrMR39,
    mrMR40,
    mrMR45,
    // PDA MRs start from here
    mrMR10,
    mrMR3,
//  mrMR44,
//  mrMR43,
    mpcSelectAllPDA,
    mrEndOfSequence
  };

  Status = PerformGenericMrsFsmSequence (MrcData, Ddr5SagvMrOrder, TRUE);

  return Status;
}

/**
  This function sets DDR5 PullUp/Dn Drive Strength through MR5 command
  @param[in]      MrcData     - Pointer to global MRC data.
  @param[in]      Controller       - Index of the requested Controler
  @param[in]      Channel          - Index of the requested Channel
  @param[in]      Rank             - Current working rank
  @param[in]      PuDrvStr    - Pull Up   Drive Strength value to set (in Ohms).  Does not support infinity (0xFFFF) as RFU.
  @param[in]      PdDrvStr    - Pull Down Drive Strength value to set (in Ohms).  Does not support infinity (0xFFFF) as RFU.

  @retval MrcStatus - mrcSuccess if a supported Ron value, else mrcWrongInputParameter.
**/
MrcStatus
MrcSetMR5_DDR5 (
  IN  MrcParameters *const  MrcData,
  IN  UINT32                Controller,
  IN  UINT32                Channel,
  IN  UINT32                Rank,
  IN  UINT16                PuDrvStr,
  IN  UINT16                PdDrvStr
  )
{
  UINT32                      ControllerStart;
  UINT32                      ControllerEnd;
  UINT32                      ControllerLoop;
  UINT32                      ChannelStart;
  UINT32                      ChannelEnd;
  UINT32                      ChannelLoop;
  UINT32                      RankHalf;
  UINT32                      RankMod2;
  MrcStatus                   Status;
  MrcStatus                   MrsStatus;
  MrcChannelOut               *ChannelOut;
  MrcOutput                   *Outputs;
  MrcDebug                    *Debug;
  INT8                        EncodePullUp;
  INT8                        EncodePullDn;
  DDR5_MODE_REGISTER_5_TYPE   MR5;

  Status  = mrcSuccess;
  Outputs = &MrcData->Outputs;
  Debug   = &Outputs->Debug;

  EncodePullUp = EncodePullDn = 1; // Default
  RankHalf = Rank / MAX_RANK_IN_DIMM;
  RankMod2 = Rank % MAX_RANK_IN_DIMM;

  if (Controller >= MAX_CONTROLLER) {
    ControllerStart = 0;
    ControllerEnd = MAX_CONTROLLER;
  } else {
    ControllerStart = Controller;
    ControllerEnd = Controller + 1;
  }
  if (Channel >= Outputs->MaxChannels) {
    ChannelStart = 0;
    ChannelEnd = Outputs->MaxChannels;
  } else {
    ChannelStart = Channel;
    ChannelEnd = Channel + 1;
  }

  // Encode Ron values
  if (PuDrvStr != MRC_IGNORE_ARG_16) {
    EncodePullUp = Ddr5DrvStrEncode (PuDrvStr);
  }
  if (PdDrvStr != MRC_IGNORE_ARG_16) {
    EncodePullDn = Ddr5DrvStrEncode (PdDrvStr);
  }

  if ((EncodePullUp == -1) || (EncodePullDn == -1)) {
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_ERROR, "%s Invalid %s Value: Up %u Dn %u\n", gErrString, gDrvStr, PuDrvStr, PdDrvStr);
    Status = mrcWrongInputParameter;
  }

  for (ControllerLoop = ControllerStart; ControllerLoop < ControllerEnd; ControllerLoop++) {
    for (ChannelLoop = ChannelStart; ChannelLoop < ChannelEnd; ChannelLoop++) {
      if (!(MrcRankExist (MrcData, ControllerLoop, ChannelLoop, Rank))) {
        continue;
      }
      ChannelOut = &Outputs->Controller[ControllerLoop].Channel[ChannelLoop];
      MR5.Data8 = (UINT8) ChannelOut->Dimm[RankHalf].Rank[RankMod2].MR[mrIndexMR5];
      if (PuDrvStr != MRC_IGNORE_ARG_16) {
        MR5.Bits.PullUpOutputDriverImpedance = EncodePullUp;
      }
      if (PdDrvStr != MRC_IGNORE_ARG_16) {
        MR5.Bits.PullDownOutputDriverImpedance = EncodePullDn;
      }
      MrsStatus = MrcWriteMRS (MrcData, ControllerLoop, ChannelLoop, 1 << Rank, mrMR5, (UINT16) MR5.Data8);
      if (MrsStatus != mrcSuccess) {
        Status |= MrsStatus;
      }
      ChannelOut->Dimm[RankHalf].Rank[RankMod2].MR[mrIndexMR5] = MR5.Data8;
      //MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "MrcSetMR5_DDR5: MC%d, C%d, R%d, UP = 0x%02X, DOWN = 0x%02X, MR = 0x%x\n", Controller, Channel, Rank, EncodePullUp, EncodePullDn, MR5.Data8);
    }  // for ChannelLoop
  } // ControllerLoop

  return Status;
}

/**
  This function sets DQ DFE_Tap1/2/3/4 MRs:
  DQL0-7: 129-132, 137-140, 145-148, ..., 185-188
  DQU0-7: 193-196, 201-204, 209-212, ..., 249-252 (only x16 device)

  @param[in] MrcData     - Pointer to global MRC data.
  @param[in] Controller  - Index of the requested Controler
  @param[in] Channel     - Index of the requested Channel
  @param[in] Rank        - Current working rank
  @param[in] Byte        - Current working byte
  @param[in] Value       - DFE value - absolute value  min 0 max 40.
  @param[in] DFETap      - DFE Tap - 1,2,3,4.
  @param[in] PDA         - PDA mode: True, False

  @retval MrcStatus - mrcSuccess if a supported DFE value, else mrcWrongInputParameter.
**/
MrcStatus
MrcSetDfe_DDR5 (
  IN  MrcParameters *const  MrcData,
  IN  UINT32                Controller,
  IN  UINT32                Channel,
  IN  UINT32                Rank,
  IN  UINT32                Byte,
  IN  UINT8                 Value,
  IN  UINT8                 DFETap,
  IN  BOOLEAN               PDA
  )
{
  UINT32                      ControllerStart;
  UINT32                      ControllerEnd;
  UINT32                      ControllerLoop;
  UINT32                      ChannelStart;
  UINT32                      ChannelEnd;
  UINT32                      ChannelLoop;
  //UINT32                      ByteStart;
  //UINT32                      ByteEnd;
  //UINT32                      ByteLoop;
  UINT8                       DFEValue;
  UINT8                       MrIndex;
  MrcModeRegister             MrNum;
  const MrcModeRegister       *FsmSequence;
  UINT32                      RankHalf;
  UINT32                      RankMod2;
  MrcStatus                   Status;
  MrcChannelOut               *ChannelOut;
  //MrcSdramOut                 *SdramOut;
  MrcOutput                   *Outputs;
  MrcDebug                    *Debug;
  //static const UINT8          DeviceMap[] = { 0,1,2,3 }; //@todo_adl get real device map - PDA
  UINT8                       DfeTapStartMR;
  static const MrcModeRegister            DFETap1Arrx16[] = { mrMR129, mrMR137, mrMR145, mrMR153, mrMR161, mrMR169, mrMR177, mrMR185,
                                                              mrMR193, mrMR201, mrMR209, mrMR217, mrMR225, mrMR233, mrMR241, mrMR249,mrEndOfSequence };
  static const MrcModeRegister            DFETap2Arrx16[] = { mrMR130, mrMR138, mrMR146, mrMR154, mrMR162, mrMR170, mrMR178, mrMR186,
                                                              mrMR194, mrMR202, mrMR210, mrMR218, mrMR226, mrMR234, mrMR242, mrMR250,mrEndOfSequence };
  static const MrcModeRegister            DFETap3Arrx16[] = { mrMR131, mrMR139, mrMR147, mrMR155, mrMR163, mrMR171, mrMR179, mrMR187,
                                                               mrMR195, mrMR203, mrMR211, mrMR219, mrMR227, mrMR235, mrMR243, mrMR251,mrEndOfSequence };
  static const MrcModeRegister            DFETap4Arrx16[] = { mrMR132, mrMR140, mrMR148, mrMR156, mrMR164, mrMR172, mrMR180, mrMR188,
                                                              mrMR196, mrMR204, mrMR212, mrMR220, mrMR228, mrMR236, mrMR244, mrMR252,mrEndOfSequence };
  //MRC_GEN_MRS_FSM_MR_TYPE MrData[MAX_CONTROLLER][MAX_CHANNEL][MAX_RANK_IN_CHANNEL][MAX_MR_GEN_FSM]; //@todo_adl check the size and type

  Status    = mrcSuccess;
  Outputs   = &MrcData->Outputs;
  Debug     = &Outputs->Debug;
  MrNum     = 255;

  RankHalf = Rank / MAX_RANK_IN_DIMM;
  RankMod2 = Rank % MAX_RANK_IN_DIMM;

  if (Controller >= MAX_CONTROLLER) {
    ControllerStart = 0;
    ControllerEnd   = MAX_CONTROLLER;
  } else {
    ControllerStart = Controller;
    ControllerEnd   = Controller + 1;
  }
  if (Channel >= Outputs->MaxChannels) {
    ChannelStart = 0;
    ChannelEnd   = (UINT32) Outputs->MaxChannels;
  } else {
    ChannelStart = Channel;
    ChannelEnd   = Channel + 1;
  }

  /*
  // @todo_adl finish PDA for DFE;
  if (Byte >= MAX_BYTE_IN_DDR5_CHANNEL) {
    ByteStart = 0;
    ByteEnd = MAX_BYTE_IN_DDR5_CHANNEL;
  } else {
    ByteStart = Byte;
    ByteEnd = Byte + 1;
  }
  */
  DFEValue  = Value & 0x3F;

  switch (DFETap) {
    case 1:
      DfeTapStartMR = DFETap1Arrx16[0]; // DQL Bit [0]
      break;
    case 2:
      DfeTapStartMR = DFETap2Arrx16[0];
      break;
    case 3:
      DfeTapStartMR = DFETap3Arrx16[0];
      break;
    case 4:
    default:
      DfeTapStartMR = DFETap4Arrx16[0];
      break;
  }
  if ((DFEValue > 40) && (DFETap == 1)) {
    Status = mrcWrongInputParameter;
  }
  if ((DFEValue > 15) && (DFETap == 2)) {
    Status = mrcWrongInputParameter;
  }
  if ((DFEValue > 12) && (DFETap == 3)) {
    Status = mrcWrongInputParameter;
  }
  if ((DFEValue > 9) && (DFETap == 4)) {
    Status = mrcWrongInputParameter;
  }

  if (Status == mrcWrongInputParameter) {
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_ERROR, "%s Invalid DFE Value: %d in Tap %u\n", gErrString, Value, DFETap);
    return Status;
  }

  // Update MR value in the host struct
  for (ControllerLoop = ControllerStart; ControllerLoop < ControllerEnd; ControllerLoop++) {
    for (ChannelLoop = ChannelStart; ChannelLoop < ChannelEnd; ChannelLoop++) {
      if (!(MrcRankExist (MrcData, ControllerLoop, ChannelLoop, Rank))) {
        continue;
      }

      ChannelOut = &Outputs->Controller[ControllerLoop].Channel[ChannelLoop];
      //MRC_DEBUG_MSG(Debug, MSG_LEVEL_ERROR, "\n DeviceType x%d\n", Devicex16 ? 16:8);
      if (PDA) {
        /* @todo_adl finish PDA for DFE; use GenericMrsFsm

        for (ByteLoop = ByteStart; ByteLoop < ByteEnd; ByteLoop++) {
          SdramOut =  &ChannelOut->Dimm[RankHalf].Rank[RankMod2].Ddr5PdaDfe[DeviceMap[ByteLoop]];
          for (BitLoop = 0; BitLoop < MAX_BITS; BitLoop++) {
            MRDFE.Bits.DfeTap1BiasSignBit = DFESign;
            MRDFE.Bits.DfeTap1Bias = DFEValue;
            MRDFE.Bits.DfeTap1Enable = DFEEnable;
            // @todo_adl build MrData
            if (Devicex16 && (DeviceMap[ByteLoop] % 2)) {
              SdramOut->DfeUpper[BitLoop][DFETap] = MRDFE.Data8; // update MrcData DQU0-7
            } else {
              SdramOut->DfeLower[BitLoop][DFETap] = MRDFE.Data8; // update MrcData DQL0-7
            }
          } //BitLoop
        } // ByteLoop
        */
      } else {
        // We use the same DFE value for all bits, store it using MR of DQL bit [0]
        MrNum = DfeTapStartMR;
        MrIndex = MrcMrAddrToIndex (MrcData, &MrNum);
        ChannelOut->Dimm[RankHalf].Rank[RankMod2].MR[MrIndex] = Value;
      } // if not PDA
    }  // for ChannelLoop
  } // ControllerLoop
  //MRC_DEBUG_MSG(&Outputs->Debug, MSG_LEVEL_NOTE, "MrcSetDfeTap_DDR5: MC%dC%dR%d, DFETap%d = 0x%02X\n", Controller, Channel, Rank, DFETap, MRDFE.Data8);

  // Program Generic MRS FSM Per Controller/Channel
  switch (DFETap) {
  case 1:
    FsmSequence = DFETap1Arrx16;
    break;
  case 2:
    FsmSequence = DFETap2Arrx16;
    break;
  case 3:
    FsmSequence = DFETap3Arrx16;
    break;
  case 4:
  default:
    FsmSequence = DFETap4Arrx16;
    break;
  }
  PerformGenericMrsFsmSequence (MrcData, FsmSequence, FALSE);
  MrcWait (MrcData, 80); // DFE settle time tDFE = 80nS

  return Status;
}

#if 0
/**
  This function sets DRAM DCA MRs:

  @param[in] MrcData     - Pointer to global MRC data.
  @param[in] Controller  - Index of the requested Controler
  @param[in] Channel     - Index of the requested Channel
  @param[in] Rank        - Current working rank
  @param[in] Byte        - Current working byte
  @param[in] Value       - DFE value - absolute value  min 0 max 40.
  @param[in] OptParam    - DCA QCLK, IBCLK, QBCLK.
  @param[in] PDA         - PDA mode: True, False

  @retval MrcStatus - mrcSuccess if a supported DFE value, else mrcWrongInputParameter.
**/
MrcStatus
MrcSetDca_DDR5 (
  IN  MrcParameters *const  MrcData,
  IN  UINT32                Controller,
  IN  UINT32                Channel,
  IN  UINT32                Rank,
  IN  UINT32                Byte,
  IN  UINT16                Value,
  IN  UINT8                 OptParam,
  IN  BOOLEAN               PDA
  )
{
  UINT32                      ControllerStart;
  UINT32                      ControllerEnd;
  UINT32                      ControllerLoop;
  UINT32                      ChannelStart;
  UINT32                      ChannelEnd;
  UINT32                      ChannelLoop;
  UINT8                       MrIndex;
  MrcModeRegister             MrNum;
  const MrcModeRegister       *FsmSequence;
  UINT32                      RankHalf;
  UINT32                      RankMod2;
  MrcStatus                   Status;
  MrcChannelOut               *ChannelOut;
  MrcOutput                   *Outputs;
  MrcDebug                    *Debug;
  UINT8                       DfeTapStartMR;
  DDR5_MODE_REGISTER_43_TYPE  Mr43;
  DDR5_MODE_REGISTER_44_TYPE  Mr44;
  UINT8                       SignBit;
  UINT8                       AbsoluteValue;

  static const MrcModeRegister            DcaMr43[] = { mrMR43, mrEndOfSequence };
  static const MrcModeRegister            DcaMr44[] = { mrMR44, mrEndOfSequence };

  Status    = mrcSuccess;
  Outputs   = &MrcData->Outputs;
  Debug     = &Outputs->Debug;
  MrNum     = 255;

  RankHalf = Rank / MAX_RANK_IN_DIMM;
  RankMod2 = Rank % MAX_RANK_IN_DIMM;

  if (Controller >= MAX_CONTROLLER) {
    ControllerStart = 0;
    ControllerEnd   = MAX_CONTROLLER;
  } else {
    ControllerStart = Controller;
    ControllerEnd   = Controller + 1;
  }
  if (Channel >= Outputs->MaxChannels) {
    ChannelStart = 0;
    ChannelEnd   = (UINT32) Outputs->MaxChannels;
  } else {
    ChannelStart = Channel;
    ChannelEnd   = Channel + 1;
  }
  //Bit 15 is a flag of sign bit.
  if ((Value & 0x8000) == 0x8000) {
    SignBit = 1;
  } else {
    SignBit = 0;
  }
  AbsoluteValue = Value & 0xFF;

  switch (OptParam) {
  case OptDimmDcaQclk:
  case OptDimmDcaIBclk:
    DfeTapStartMR = DcaMr43[0];
    FsmSequence = DcaMr43;
    break;
  case OptDimmDcaQBclk:
    DfeTapStartMR = DcaMr44[0];
    FsmSequence = DcaMr44;
    break;
  default:
    DfeTapStartMR = DcaMr43[0];
    FsmSequence = DcaMr43;
    Status = mrcWrongInputParameter;;
    break;
  }
  if (Status == mrcWrongInputParameter) {
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_ERROR, "Invalid DCA MR Addess: %d \n", OptParam);
    return Status;
  }

  // Update MR value in the host struct
  for (ControllerLoop = ControllerStart; ControllerLoop < ControllerEnd; ControllerLoop++) {
    for (ChannelLoop = ChannelStart; ChannelLoop < ChannelEnd; ChannelLoop++) {
      if (!(MrcRankExist (MrcData, ControllerLoop, ChannelLoop, Rank))) {
        continue;
      }

      ChannelOut = &Outputs->Controller[ControllerLoop].Channel[ChannelLoop];
      if (PDA) {
      } else {

        MrNum = DfeTapStartMR;
        MrIndex = MrcMrAddrToIndex (MrcData, &MrNum);
        MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "MrcSetDca_DDR5 absolute value %d sign bit %d  MR%d   ", AbsoluteValue, SignBit, DfeTapStartMR);
        if (OptParam == OptDimmDcaQclk) {
          Mr43.Data8 = (UINT8) ChannelOut->Dimm[RankHalf].Rank[RankMod2].MR[MrIndex];
          Mr43.Bits.DcaForQclkIn4PhaseClk = AbsoluteValue;
          Mr43.Bits.DcaForQclkSignBit = SignBit;
          ChannelOut->Dimm[RankHalf].Rank[RankMod2].MR[MrIndex] = Mr43.Data8;
        } else if (OptParam == OptDimmDcaIBclk) {
          Mr43.Data8 = (UINT8) ChannelOut->Dimm[RankHalf].Rank[RankMod2].MR[MrIndex];
          Mr43.Bits.DcaForIbclkIn4PhaseClk = AbsoluteValue;
          Mr43.Bits.DcaForIclkSignBit = SignBit;
          ChannelOut->Dimm[RankHalf].Rank[RankMod2].MR[MrIndex] = Mr43.Data8;
        } else if (OptParam == OptDimmDcaQBclk) {
          Mr44.Data8 = (UINT8) ChannelOut->Dimm[RankHalf].Rank[RankMod2].MR[MrIndex];
          Mr44.Bits.DcaForQbclkIn4PhaseClk = AbsoluteValue;
          Mr44.Bits.DcaForQbclkSignBit = SignBit;
          ChannelOut->Dimm[RankHalf].Rank[RankMod2].MR[MrIndex] = Mr44.Data8;
        }
        MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Mr final value = 0x%x\n", ChannelOut->Dimm[RankHalf].Rank[RankMod2].MR[MrIndex]);

      } // if not PDA
    }  // for ChannelLoop
  } // ControllerLoop

  // Program Generic MRS FSM Per Controller/Channel
  PerformGenericMrsFsmSequence (MrcData, FsmSequence, FALSE);
  MrcWait (MrcData, 80); // 80nS todo

  return Status;
}
#endif //if 0

/**
  Ddr5 Get the MR value and its corresponding index for a given DIMM Opt Param.
  Value is set by reference to the corresponding pointers.

  @param[in]      MrcData     - Include all MRC global data.
  @param[in]      OptParam    - The Dimm Opt Param (e.g., OptDimmRon, OptDimmOdtWr, OptDimmOdtPark, OptDimmOdtNom)
  @param[out]     *MrIndex    - Updated Pointer to the MR index.
  @param[out]     *MrNum      - Updated Pointer to the MR number.

  @retval MrcStatus - mrcWrongInputParameter if unsupported OptParam, mrcSuccess otherwise
**/
MrcStatus
Ddr5GetOptDimmParamMrIndex (
  IN  MrcParameters *const MrcData,
  IN  UINT8                OptDimmParam,
  OUT UINT8                *MrIndex,
  OUT UINT8                *MrNum
  )
{
  MrcDebug  *Debug;
  MrcStatus Status;

  Status = mrcSuccess;
  Debug = &MrcData->Outputs.Debug;

  switch (OptDimmParam) {
  case OptDimmRon:
  case OptDimmRonUp:
  case OptDimmRonDn:
    // DIMM RON
    *MrIndex  = mrIndexMR5;
    *MrNum    = mrMR5;
    break;

  case OptDimmOdtWr:
  case OptDimmOdtPark:
    *MrIndex  = mrIndexMR34;
    *MrNum    = mrMR34;
    break;

  case OptDimmOdtNomWr:
  case OptDimmOdtNomRd:
    *MrIndex  = mrIndexMR35;
    *MrNum    = mrMR35;
    break;

  case OptDimmOdtParkDqs:
    *MrIndex = mpcMR33 - (256 - mrIndexMpcMr13);
    *MrNum = 0;
    break;

  /*@todo_adl
  case OptDimmOdtCA:
    // DIMM RTT WR or DIMM ODT CA
    *MrIndex = mrIndexMR33;
    *MrNum = mrMR11;
    break;

  case OptDimmSocOdt:
    // SOC ODT
    *MrIndex = mrIndexMR17;
    *MrNum = mrMR17;
    break;
    */

  case OptDimmDFETap1:
    *MrIndex = mrIndexMR129;
    *MrNum   = mrMR129;
    break;

  case OptDimmDFETap2:
    *MrIndex = mrIndexMR130;
    *MrNum   = mrMR130;
    break;

  case OptDimmDFETap3:
    *MrIndex = mrIndexMR131;
    *MrNum   = mrMR131;
    break;

  case OptDimmDFETap4:
    *MrIndex = mrIndexMR132;
    *MrNum   = mrMR132;
    break;

  /*case OptDimmDcaQclk:
  case OptDimmDcaIBclk:
    *MrIndex = mrIndexMR43;
    *MrNum   = mrMR43;
    break;
  case OptDimmDcaQBclk:
    *MrIndex = mrIndexMR44;
    *MrNum   = mrMR44;
    break;*/

  default:
    *MrIndex = 0xFF;
    *MrNum = 0xFF;
    Status = mrcWrongInputParameter;
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_ERROR, "%s %u ", gWrongInputParam, OptDimmParam);
    break;
  }

  return Status;
}

/**
  Ddr5 Set DimmParamValue is responsible for performing the concrete set DIMM parameter to value,
  using  specific MR set functions.
  Parameters supported: OptDimmRon, OptDimmOdtWr

  @param[in,out]  MrcData    - Include all MRC global data.
  @param[in,out]  MrData     - Pointer to the MR data to update.
  @param[in]      Controller - the controller to work on
  @param[in]      Channel    - The channel to work on
  @param[in]      Rank       - The rank to work on
  @param[in]      OptParam   - The Dimm Opt Param (e.g., OptDimmRon, OptDimmOdtWr, OptDimmOdtPark, OptDimmOdtNom)
  @param[in]      ParamValue - The actual values (Typically in Ohms)

  @retval MrcStatus - mrcWrongInputParameter if unsupported OptParam, MrcStatus of the MR set functions otherwise
**/
MrcStatus
Ddr5SetDimmParamValue (
  IN OUT MrcParameters *const MrcData,
  IN OUT UINT16        *const MrData,
  IN     UINT32               Controller,
  IN     UINT32               Channel,
  IN     UINT32               Rank,
  IN     UINT8                OptParam,
  IN     UINT16               ParamValue,
  IN     BOOLEAN              UpdateHost
  )
{
  MrcDebug  *Debug;
  MrcStatus Status;

  Debug = &MrcData->Outputs.Debug;

  switch (OptParam) {
    case OptDimmRon:
    case OptDimmRonUp:
      // DIMM RON Up
      Status = MrcSetMR5_DDR5 (MrcData, Controller, Channel, Rank, ParamValue, MRC_IGNORE_ARG_16);
      break;

    case OptDimmRonDn:
      // DIMM RON Dn
      Status = MrcSetMR5_DDR5 (MrcData, Controller, Channel, Rank, MRC_IGNORE_ARG_16, ParamValue);
      break;

    case OptDimmDFETap1:
    case OptDimmDFETap2:
    case OptDimmDFETap3:
    case OptDimmDFETap4:
      Status = MrcSetDfe_DDR5 (MrcData, Controller, Channel, Rank, MRC_IGNORE_ARG, (UINT8) ParamValue, (UINT8) (OptParam - OptDimmDFETap1 + 1), FALSE); // only per RANK
      break;

    case OptDimmOdtPark:
     Status = MrcSetMR34_DDR5 (MrcData, Controller, Channel, Rank, ParamValue, MRC_IGNORE_ARG_16, MRC_PRINTS_OFF, UpdateHost);
     break;
#if 0
    case OptDimmDcaQclk:
    case OptDimmDcaIBclk:
    case OptDimmDcaQBclk:
      Status = MrcSetDca_DDR5 (MrcData, Controller, Channel, Rank, MRC_IGNORE_ARG, ParamValue, OptParam, FALSE);
      break;
#endif //if 0
    case OptDimmOdtWr:
     Status = MrcSetMR34_DDR5 (MrcData, Controller, Channel, Rank, MRC_IGNORE_ARG_16, ParamValue, MRC_PRINTS_OFF, UpdateHost);
     break;

    case OptDimmOdtNomWr:
     Status = MrcSetMR35_DDR5 (MrcData, Controller, Channel, Rank, ParamValue, MRC_IGNORE_ARG_16, MRC_PRINTS_OFF, UpdateHost);
     break;

    case OptDimmOdtNomRd:
     Status = MrcSetMR35_DDR5 (MrcData, Controller, Channel, Rank, MRC_IGNORE_ARG_16, ParamValue, MRC_PRINTS_OFF, UpdateHost);
     break;

    case OptDimmOdtParkDqs:
     Status = MrcSetMR33_DDR5 (MrcData, Controller, Channel, Rank, ParamValue, MRC_PRINTS_OFF, UpdateHost);
     break;

      /*
    case OptDimmSocOdt:
      // SOC ODT
      Status = MrcLpddr5SetMr17(MrcData, ParamValue, MrData);
      break;
      */
    default:
      Status = mrcWrongInputParameter;
      MRC_DEBUG_MSG(Debug, MSG_LEVEL_ERROR, "%s %u ", gWrongInputParam, OptParam);
      break;
  }

  return Status;
}

/**
Ddr5 Get DimmParamValue is responsible for performing the concrete Get DIMM paramter to value, using
the MRC Cache.
@param[in,out]  MrcData         - Include all MRC global data.
@param[in,out]  MrData          - Pointer to the MR data to update.
@param[in]      OptParam        - The Dimm Opt Param (e.g., OptDimmRon, OptDimmOdtWr, OptDimmOdtPark, OptDimmOdtNom)
@param[in,out]  ParamValue      - The actual values (Typically in Ohms)

@retval MrcStatus - mrcWrongInputParameter if unsupported OptParam
- MrcStatus of the MR set functions otherwise

**/
MrcStatus
Ddr5GetDimmParamValue(
  IN OUT MrcParameters *const MrcData,
  IN OUT UINT16        *const MrData,
  IN     UINT8                OptParam,
  IN OUT UINT16*                ParamValue
)
{
  MrcDebug        *Debug;
  MrcStatus       Status;
  DDR5_MODE_REGISTER_34_TYPE *Mr34;
  DDR5_MODE_REGISTER_35_TYPE *Mr35;
  DDR5_MODE_REGISTER_5_TYPE *Mr5;
  DDR5_MODE_REGISTER_113_TYPE *Mr113;
  DDR5_MODE_REGISTER_114_TYPE *Mr114;
  DDR5_MODE_REGISTER_115_TYPE *Mr115;
  DDR5_MODE_REGISTER_116_TYPE *Mr116;

  Debug = &MrcData->Outputs.Debug;
  Status = mrcSuccess;

  switch (OptParam) {
  case OptDimmDFETap1:
    Mr113 = (DDR5_MODE_REGISTER_113_TYPE *)MrData;
    *ParamValue = Mr113->Data8 & 0x7f;
    break;
  case OptDimmDFETap2:
    Mr114 = (DDR5_MODE_REGISTER_114_TYPE *)MrData;
    *ParamValue = Mr114->Data8 & 0x7f;
    break;
  case OptDimmDFETap3:
    Mr115 = (DDR5_MODE_REGISTER_115_TYPE *)MrData;
    *ParamValue = Mr115->Data8 & 0x7f;
    break;
  case OptDimmDFETap4:
    Mr116 = (DDR5_MODE_REGISTER_116_TYPE *)MrData;
    *ParamValue = Mr116->Data8 & 0x7f;
    break;

  case OptDimmRon:
  case OptDimmRonUp:
    // DIMM RON Up
    Mr5 = (DDR5_MODE_REGISTER_5_TYPE *) MrData;
    *ParamValue = Mr5->Bits.PullUpOutputDriverImpedance;
    break;


  case OptDimmRonDn:
    // DIMM RON Dn
    Mr5 = (DDR5_MODE_REGISTER_5_TYPE *)MrData;
    *ParamValue = Mr5->Bits.PullDownOutputDriverImpedance;
    break;



  case OptDimmOdtWr:
    // DIMM RTT WR
    Mr34 = (DDR5_MODE_REGISTER_34_TYPE *)MrData;
    *ParamValue = Mr34->Bits.RttWr;
    break;

  case OptDimmOdtPark:
  case OptDimmOdtParkNT:
    // DIMM RTT PARK
    Mr34 = (DDR5_MODE_REGISTER_34_TYPE *)MrData;
    *ParamValue = Mr34->Bits.RttPark;
    break;


  case OptDimmOdtNomWr:
    // DIMM RTT NOM
    Mr35 = (DDR5_MODE_REGISTER_35_TYPE *)MrData;
    *ParamValue = Mr35->Bits.RttNomWr;
    break;

  case OptDimmOdtNomRd:
    // DIMM RTT NOM
    Mr35 = (DDR5_MODE_REGISTER_35_TYPE *)MrData;
    *ParamValue = Mr35->Bits.RttNomRd;
    break;

  case OptDimmOdtParkDqs:
    *ParamValue = *MrData & 0x7; //Mask out mpc command bits
    break;


  default:
    // Unsupported paramter
    Status = mrcWrongInputParameter;
    MRC_DEBUG_MSG(Debug, MSG_LEVEL_ERROR, "%s %u ", gWrongInputParam, OptParam);
    break;
  }
  return Status;
}

/**
  Program DQS_RTT_PARK via MPC

  @param[in]      MrcData          - Pointer to global MRC data.
  @param[in]      Controller       - Index of the requested Controler
  @param[in]      Channel          - Index of the requested Channel
  @param[in]      Rank             - Current working rank
  @param[in]      DqsRttPark       - Dimm Dqs Park termination value to set (in Ohms).  Does not support infinity (0xFFFF) as RFU.
  @param[in]      DebugPrint       - When TRUE, will print debugging information
  @param[in]      UpdateHost       - When TRUE, will update MRC host struct

  @retval MrcStatus - mrcSuccess if a supported Ron value, else mrcWrongInputParameter.
**/
MrcStatus
MrcSetMR33_DDR5 (
  IN  MrcParameters *const  MrcData,
  IN  UINT32                Controller,
  IN  UINT32                Channel,
  IN  UINT32                Rank,
  IN  UINT16                DqsRttPark,
  IN  BOOLEAN               DebugPrint,
  IN  BOOLEAN               UpdateHost
  )
{
  UINT32                      RankHalf;
  UINT32                      RankMod2;
  MrcStatus                   Status;
  MrcChannelOut               *ChannelOut;
  MrcOutput                   *Outputs;
  MrcDebug                    *Debug;
  INT8                        EncodeDqsOdtPark;
  UINT32                      MrIndex;
  MrcModeRegister             MpcMr33 = mpcMR33;
  MrcDebugMsgLevel            DebugLevel;

  Status = mrcSuccess;
  Outputs = &MrcData->Outputs;
  Debug = &Outputs->Debug;
  DebugLevel = DebugPrint ? MSG_LEVEL_NOTE : MSG_LEVEL_NEVER;
  RankHalf = Rank / MAX_RANK_IN_DIMM;
  RankMod2 = Rank % MAX_RANK_IN_DIMM;
  EncodeDqsOdtPark = -1;

  if (DqsRttPark != MRC_IGNORE_ARG_16) {
    EncodeDqsOdtPark = (INT8) OdtEncode (DqsRttPark);
    if (EncodeDqsOdtPark == -1) {
      MRC_DEBUG_MSG (Debug, MSG_LEVEL_ERROR, "%s Invalid DQSOdtPark Value:%u\n", gErrString, DqsRttPark);
      Status = mrcWrongInputParameter;
    }
  }

  ChannelOut = &Outputs->Controller[Controller].Channel[Channel];
  if (EncodeDqsOdtPark != -1) {
    MrIndex = MrcMrAddrToIndex (MrcData, &MpcMr33);
    MrcIssueMpc (MrcData, Controller, Channel, Rank, DDR5_MPC_SET_DQS_RTT_PARK (EncodeDqsOdtPark), MRC_PRINTS_OFF); //OdtParkDqs override by MPC command
    MRC_DEBUG_MSG (Debug, DebugLevel, "MrcSetMR33_DDR5: MC%d, C%d, R%d, DQSOdtPark = 0x%02X\n", Controller, Channel, Rank, EncodeDqsOdtPark);
    if (UpdateHost) {
      ChannelOut->Dimm[RankHalf].Rank[RankMod2].MR[MrIndex] = DDR5_MPC_SET_DQS_RTT_PARK (EncodeDqsOdtPark); // mpcMR33 is only use for DQS_RTT_PARK
    }
  }
  return Status;
}

MrcStatus
MrcSetMR34_DDR5 (
  IN  MrcParameters *const  MrcData,
  IN  UINT32                Controller,
  IN  UINT32                Channel,
  IN  UINT32                Rank,
  IN  UINT16                OdtPark,
  IN  UINT16                OdtWr,
  IN  BOOLEAN               DebugPrint,
  IN  BOOLEAN               UpdateHost
  )
{
  UINT32                      RankHalf;
  UINT32                      RankMod2;
  MrcStatus                   Status;
  MrcChannelOut               *ChannelOut;
  MrcOutput                   *Outputs;
  MrcDebug                    *Debug;
  INT8                        EncodeOdtPark;
  INT8                        EncodeOdtWr;
  DDR5_MODE_REGISTER_34_TYPE  MR34;
  UINT32                      MrIndex;
  MrcModeRegister             MrNum = mrMR34;
  MrcModeRegister             MpcMrNum = mpcMR34;
  MrcDebugMsgLevel            DebugLevel;

  Status     = mrcSuccess;
  Outputs    = &MrcData->Outputs;
  Debug      = &Outputs->Debug;
  DebugLevel = DebugPrint ? MSG_LEVEL_NOTE : MSG_LEVEL_NEVER;
  RankHalf   = Rank / MAX_RANK_IN_DIMM;
  RankMod2   = Rank % MAX_RANK_IN_DIMM;

  EncodeOdtPark = -1;
  EncodeOdtWr   = -1;

  //Encode Ron values
  if (OdtPark != MRC_IGNORE_ARG_16) {
    EncodeOdtPark = (INT8) OdtEncode (OdtPark);
    if (EncodeOdtPark == -1) {
      MRC_DEBUG_MSG(Debug, MSG_LEVEL_ERROR, "%s Invalid OdtPark Value:%u\n", gErrString, OdtPark);
      Status = mrcWrongInputParameter;
    }
  }
  if (OdtWr != MRC_IGNORE_ARG_16) {
    EncodeOdtWr = (INT8) OdtEncode (OdtWr);
    if (EncodeOdtWr == -1) {
      MRC_DEBUG_MSG(Debug, MSG_LEVEL_ERROR, "%s Invalid OdtWr Value:%u\n", gErrString, OdtWr);
      Status = mrcWrongInputParameter;
    }
  }

  ChannelOut = &Outputs->Controller[Controller].Channel[Channel];
  if (EncodeOdtPark != -1) {
    MrIndex = MrcMrAddrToIndex (MrcData, &MpcMrNum);
    MrcIssueMpc (MrcData, Controller, Channel, Rank, DDR5_MPC_SET_RTT_PARK (EncodeOdtPark), FALSE);
    MRC_DEBUG_MSG (Debug, DebugLevel, "MrcSetMR34_DDR5: MC%d, C%d, R%d, OdtPark = 0x%02X\n", Controller, Channel, Rank, EncodeOdtPark);
    if (UpdateHost) {
      ChannelOut->Dimm[RankHalf].Rank[RankMod2].MR[MrIndex] = DDR5_MPC_SET_RTT_PARK (EncodeOdtPark);
      //Also update MrMR34 with MpcMR34 value
      MrIndex = MrcMrAddrToIndex(MrcData, &MrNum);
      MR34.Data8 = (UINT8) ChannelOut->Dimm[RankHalf].Rank[RankMod2].MR[MrIndex];
      MR34.Bits.RttPark = EncodeOdtPark;
      ChannelOut->Dimm[RankHalf].Rank[RankMod2].MR[MrIndex] = MR34.Data8;
    }
  }
  if (EncodeOdtWr != -1) {
    MrIndex = MrcMrAddrToIndex(MrcData, &MrNum);
    MR34.Data8 = (UINT8) ChannelOut->Dimm[RankHalf].Rank[RankMod2].MR[MrIndex];
    MR34.Bits.RttWr = EncodeOdtWr;
    MrcIssueMrw (MrcData, Controller, Channel, Rank, mrMR34, MR34.Data8, FALSE);
    MRC_DEBUG_MSG (Debug, DebugLevel, "MrcSetMR34_DDR5: MC%d, C%d, R%d, OdtWr = 0x%02X\n", Controller, Channel, Rank, EncodeOdtWr);
    if (UpdateHost) {
      ChannelOut->Dimm[RankHalf].Rank[RankMod2].MR[MrIndex] = MR34.Data8;
    }
  }

  return Status;
}

/**
  This function sets PuulUp/Dn Drive Stregth through MR5 command

  @param[in]      MrcData         - Pointer to global MRC data.
  @param[in]      Controller      - Index of the requested Controler
  @param[in]      Channel         - Index of the requested Channel
  @param[in]      Rank            - Current working rank
  @param[in]      OdtNomWr        - Dimm park termination value to set (in Ohms).  Does not support infinity (0xFFFF) as RFU.
  @param[in]      OdtNomRd        - Dimm write termination value to set (in Ohms).  Does not support infinity (0xFFFF) as RFU.
  @param[in]      DebugPrint      - When TRUE, will print debugging information

  @retval MrcStatus - mrcSuccess if a supported Ron value, else mrcWrongInputParameter.
**/
MrcStatus
MrcSetMR35_DDR5 (
  IN  MrcParameters *const  MrcData,
  IN  UINT32                Controller,
  IN  UINT32                Channel,
  IN  UINT32                Rank,
  IN  UINT16                OdtNomWr,
  IN  UINT16                OdtNomRd,
  IN  BOOLEAN               DebugPrint,
  IN  BOOLEAN               UpdateHost
  )
{
  UINT32                      RankHalf;
  UINT32                      RankMod2;
  MrcStatus                   Status;
  MrcChannelOut               *ChannelOut;
  MrcOutput                   *Outputs;
  MrcDebug                    *Debug;
  INT8                        EncodeOdtNomWr;
  INT8                        EncodeOdtNomRd;
  DDR5_MODE_REGISTER_35_TYPE  MR35;
  UINT32                      MrIndex;
  MrcModeRegister             MrNum = mrMR35;
  MrcDebugMsgLevel            DebugLevel;

  Status        = mrcSuccess;
  Outputs       = &MrcData->Outputs;
  Debug         = &Outputs->Debug;
  DebugLevel    = DebugPrint ? MSG_LEVEL_NOTE : MSG_LEVEL_NEVER;
  RankHalf      = Rank / MAX_RANK_IN_DIMM;
  RankMod2      = Rank % MAX_RANK_IN_DIMM;

  EncodeOdtNomWr = -1;
  EncodeOdtNomRd = -1;

  //Encode Ron values
  if (OdtNomWr != MRC_IGNORE_ARG_16) {
    EncodeOdtNomWr = (INT8) OdtEncode (OdtNomWr);
    if (EncodeOdtNomWr == -1) {
      MRC_DEBUG_MSG (Debug, MSG_LEVEL_ERROR, "%s Invalid OdtNomWr Value:%u\n", gErrString, OdtNomWr);
      Status = mrcWrongInputParameter;
    }
  }
  if (OdtNomRd != MRC_IGNORE_ARG_16) {
    EncodeOdtNomRd = (INT8) OdtEncode (OdtNomRd);
    if (EncodeOdtNomRd == -1) {
      MRC_DEBUG_MSG (Debug, MSG_LEVEL_ERROR, "%s Invalid OdtNomRd Value:%u\n", gErrString, OdtNomRd);
      Status = mrcWrongInputParameter;
    }
  }

  MrIndex = MrcMrAddrToIndex (MrcData, &MrNum);
  ChannelOut = &Outputs->Controller[Controller].Channel[Channel];
  MR35.Data8 = (UINT8) ChannelOut->Dimm[RankHalf].Rank[RankMod2].MR[MrIndex];
  if (EncodeOdtNomWr != -1) {
    MR35.Bits.RttNomWr = EncodeOdtNomWr;
    MRC_DEBUG_MSG (Debug, DebugLevel, "MrcSetMR35_DDR5: MC%d, C%d, R%d, OdtNomWr = 0x%02X\n", Controller, Channel, Rank, EncodeOdtNomWr);
  }
  if (EncodeOdtNomRd != -1) {
    MR35.Bits.RttNomRd = EncodeOdtNomRd;
    MRC_DEBUG_MSG (Debug, DebugLevel, "MrcSetMR35_DDR5: MC%d, C%d, R%d, OdtNomRd = 0x%02X\n", Controller, Channel, Rank, EncodeOdtNomRd);
  }
  MrcIssueMrw (MrcData, Controller, Channel, Rank, mrMR35, MR35.Data8, FALSE);
  if (UpdateHost) {
    ChannelOut->Dimm[RankHalf].Rank[RankMod2].MR[MrIndex] = MR35.Data8;
  }

  return Status;
}

/**
  DDR5 get available values and the number of possible values of a given DimmOptParam.
  The order of the values returns corresponds to the ascending field values in the MRs as defind by JEDEC

  @param[in]      MrcData               - Include all MRC global data.
  @param[in]      DimmOptParam          - e.g., OptDimmOdtWr, OptDimmOdtNom, OptDimmOdtPark, OptDimmRon
  @param[out]     **DimmOptParamVals    - Reference to the pointer of values.
  @param[out]     *NumDimmOptParamVals  - Reference to the number of values.

  @retval MrcStatus - mrcWrongInputParameter if unsupported OptParam, mrcSuccess otherwise
**/
MrcStatus
Ddr5GetDimmOptParamValues (
  IN MrcParameters *const MrcData,
  IN UINT8                DimmOptParam,
  OUT UINT16              **DimmOptParamVals,
  OUT UINT8               *NumDimmOptParamVals
  )
{
  static const UINT16    Ddr5DimmCaOdtVals[]        = { 0xFFFF, 480, 240, 120, 80, 60, 0, 40 }; // DDR5 ODT CA Values in Ohms - note index 6 is RFU
  static const UINT16    Ddr5DimmRttWrNomParkVals[] = { 0xFFFF, 240, 120, 80, 60, 48, 40, 34 }; // DDR5 RttWr / RttNom / RttPark Values in Ohms
  static const UINT16    Ddr5DimmRonVals[]          = { 34, 40, 48 };                           // DDR5 Ron (Drive Strength) in Ohms
  MrcDebug               *Debug;
  MrcStatus              Status;

  Debug = &MrcData->Outputs.Debug;
  Status = mrcSuccess;

  switch (DimmOptParam) {
    case OptDimmOdtWr:
      // DIMM RTT_WR
      *DimmOptParamVals = (UINT16 *) Ddr5DimmRttWrNomParkVals;
      *NumDimmOptParamVals = ARRAY_COUNT (Ddr5DimmRttWrNomParkVals);
      break;

    case OptDimmOdtCA:
      // DIMM CA_ODT
      *DimmOptParamVals = (UINT16 *) Ddr5DimmCaOdtVals;
      *NumDimmOptParamVals = ARRAY_COUNT (Ddr5DimmCaOdtVals);
      break;

    case OptDimmOdtNom:
    case OptDimmOdtPark:
    case OptDimmOdtNomNT:
    case OptDimmOdtParkNT:
    case OptDimmOdtNomWr:
    case OptDimmOdtNomRd:
      // DIMM RTT_NOM / RTT_PARK
      *DimmOptParamVals = (UINT16 *) Ddr5DimmRttWrNomParkVals;
      *NumDimmOptParamVals = ARRAY_COUNT (Ddr5DimmRttWrNomParkVals);
      break;

    case OptDimmRon:
    case OptDimmRonUp:
    case OptDimmRonDn:
      // DIMM RON
      *DimmOptParamVals = (UINT16 *) Ddr5DimmRonVals;
      *NumDimmOptParamVals = ARRAY_COUNT (Ddr5DimmRonVals);
      break;

    default:
      *DimmOptParamVals = (UINT16 *)NULL;
      *NumDimmOptParamVals = 0;
      Status = mrcWrongInputParameter;
      MRC_DEBUG_MSG(Debug, MSG_LEVEL_ERROR, "%s %u ", gWrongInputParam, DimmOptParam);
      break;
  }

  return Status;
}

/**
  This function returns up to 2 arrays with the Lower/Upper Byte information for each Device within the specified Controller/Channel/Rank

  @param[in]      MrcData           - Include all MRC global data.
  @param[in]      Controller        - Controller to check
  @param[in]      Channel           - Channel to check
  @param[in]      Rank              - Rank to check
  @param[out]     LowerByte         - Array of Lower DRAM Bytes per-Device. Maximum allowed size is MAX_BYTE_IN_DDR5_CHANNEL
  @param[out]     UpperByte         - Optional Array of Upper DRAM Bytes per-Device. Only for use with x16 DRAM devices. Maximum allowed size is MAX_BYTE_IN_DDR5_CHANNEL.

  @retval None
**/
VOID
GetDdr5LowerUpperBytes (
  IN MrcParameters* const MrcData,
  IN UINT32               Controller,
  IN UINT32               Channel,
  IN UINT32               Rank,
  OUT UINT8               LowerByte[MAX_BYTE_IN_DDR5_CHANNEL],
  OUT UINT8               UpperByte[MAX_BYTE_IN_DDR5_CHANNEL] OPTIONAL
  )
{
  MrcOutput     *Outputs;
  MrcInput      *Inputs;
  UINT8         Byte;
  UINT8         DramByte;
  UINT8         NumDevices;
  UINT8         Device;
  UINT8         DimmIdx;
  MrcChannelOut *ChannelOut;
  MrcDimmOut    *DimmOut;
  UINT8 BytesPerDevice;
  MrcChannelIn  *ChannelIn;

  Outputs = &MrcData->Outputs;
  Inputs = &MrcData->Inputs;
  ChannelIn  = &Inputs->Controller[Controller].Channel[Channel];
  ChannelOut = &Outputs->Controller[Controller].Channel[Channel];
  DimmIdx = (UINT8) RANK_TO_DIMM_NUMBER (Rank);
  DimmOut = &ChannelOut->Dimm[DimmIdx];
  NumDevices = DimmOut->PrimaryBusWidth / DimmOut->SdramWidth;
  NumDevices += (Outputs->EccSupport) ? 1 : 0;
  NumDevices = MIN (NumDevices, MAX_BYTE_IN_DDR5_CHANNEL); // Ensure Device loop doesn't exceed max array size
  BytesPerDevice = DimmOut->SdramWidth / 8;

  DramByte = 0;
  for (Device = 0; Device < NumDevices; Device++) {
    for (Byte = 0; Byte < Outputs->SdramCount; Byte++) {
      if ((ChannelIn->DqsMapCpu2Dram[DimmIdx][Byte] == DramByte)) {
        LowerByte[Device] = Byte;
      }
      if (DimmOut->SdramWidth == 16) {
        if (ChannelIn->DqsMapCpu2Dram[DimmIdx][Byte] == (DramByte + 1)) {
          if (UpperByte != NULL) {
            UpperByte[Device] = Byte;
          }
        }
      }
    }
    DramByte += BytesPerDevice;
  }
}

/**
  This function returns a BOOLEAN arrays that indicates if PPR should be executed for each Device within the specified Controller/Channel/Rank

  @param[in]      MrcData            - Include all MRC global data.
  @param[in]      Controller         - Controller to check
  @param[in]      Channel            - Channel to check
  @param[in]      Rank               - Rank to check
  @param[in]      ByteMask           - Mask of bytes to check
  @param[out]     EnabledPprDevices  - Array of devices which should execute the PPR sequence. Maximum size is MAX_BYTE_IN_DDR5_CHANNEL

  @retval None
**/

VOID
GetDdr5PprDevices (
  IN   MrcParameters* const MrcData,
  IN   UINT32               Controller,
  IN   UINT32               Channel,
  IN   UINT32               Rank,
  IN   UINT16               ByteMask,
  OUT  BOOLEAN              EnabledPprDevices[MAX_BYTE_IN_DDR5_CHANNEL]
  )
{
  MrcOutput     *Outputs;
  MrcInput      *Inputs;
  UINT8         Byte;
  UINT8         DramByte;
  UINT8         NumDevices;
  UINT8         Device;
  UINT8         DimmIdx;
  MrcChannelOut *ChannelOut;
  MrcDimmOut    *DimmOut;
  UINT8 BytesPerDevice;
  MrcChannelIn  *ChannelIn;

  Outputs = &MrcData->Outputs;
  Inputs = &MrcData->Inputs;
  ChannelIn  = &Inputs->Controller[Controller].Channel[Channel];
  ChannelOut = &Outputs->Controller[Controller].Channel[Channel];
  DimmIdx = (UINT8) RANK_TO_DIMM_NUMBER (Rank);
  DimmOut = &ChannelOut->Dimm[DimmIdx];
  NumDevices = DimmOut->PrimaryBusWidth / DimmOut->SdramWidth;
  NumDevices += (Outputs->EccSupport) ? 1 : 0;
  NumDevices = MIN (NumDevices, MAX_BYTE_IN_DDR5_CHANNEL); // Ensure Device loop doesn't exceed max array size
  BytesPerDevice = DimmOut->SdramWidth / 8;

  DramByte = 0;
  for (Device = 0; Device < NumDevices; Device++) {
    for (Byte = 0; Byte < Outputs->SdramCount; Byte++) {
      // Only check devices that need repair
      if (((1 << Byte) & ByteMask)) {
        if (DimmOut->SdramWidth == 16) {
          if ((ChannelIn->DqsMapCpu2Dram[DimmIdx][Byte] == DramByte) || (ChannelIn->DqsMapCpu2Dram[DimmIdx][Byte] == DramByte + 1)) {
            EnabledPprDevices[Device] = TRUE;
            break;
          }
        } else {
          if (ChannelIn->DqsMapCpu2Dram[DimmIdx][Byte] == DramByte) {
            EnabledPprDevices[Device] = TRUE;
            break;
          }
        }
      }
    }
    DramByte += BytesPerDevice;
  }
}

/**
Enter Post Package Repair (PPR) to attempt to repair detected failed row.

  @param[in] MrcData     - Pointer to MRC global data.
  @param[in] Controller  - Controller for detected fail row
  @param[in] Channel     - Channel for detected fail row
  @param[in] Rank        - Rank for detected fail row
  @param[in] BankAddress - BankAddress for detected fail row
  @param[in] BankGroup   - BankGroup for detected fail row
  @param[in] Row         - Row for detected fail row
  @param[in] ByteMask    - Byte mask to repair for fail row

  @retval MrcStatus
**/

MrcStatus
Ddr5PostPackageRepair (
  IN MrcParameters* const MrcData,
  IN UINT32               Controller,
  IN UINT32               Channel,
  IN UINT32               Rank,
  IN UINT32               BankGroup,
  IN UINT32               BankAddress,
  IN UINT32               Row,
  IN UINT16               ByteMask
  )
{
  MrcOutput                    *Outputs;
  MrcInput                     *Inputs;
  const MRC_FUNCTION           *MrcCall;
  MrcDebug                     *Debug;
  MrcStatus                    Status   = mrcSuccess;
  UINT8                        Device;
  UINT8                        DimmIdx;
  UINT8                        NumDevices;
  MrcChannelOut                *ChannelOut;
  MrcDimmOut                   *DimmOut;
  UINT8                        LowerByte[MAX_BYTE_IN_DDR5_CHANNEL];
  UINT8                        UpperByte[MAX_BYTE_IN_DDR5_CHANNEL];
  BOOLEAN                      EnablePprDevice[MAX_BYTE_IN_DDR5_CHANNEL];
  UINT8                        PprResource;
  UINT32                       Offset = 0;
  UINT32                       Index;
  UINT8                        MrrResult[4];
  UINT16                       DeviceByteMask;
  Ddr5Command                  MrhCommand;    // Used for MRH generic commands
  MC0_CH0_CR_SCHED_CBIT_STRUCT SchedCbit;
  DDR5_MODE_REGISTER_23_TYPE   MR23;
  MrcModeRegister              MRNumber;

  Outputs = &MrcData->Outputs;
  Inputs = &MrcData->Inputs;
  MrcCall = Inputs->Call.Func;
  Debug = &Outputs->Debug;

  MrcCall->MrcSetMem ((UINT8 *) LowerByte, sizeof (LowerByte), 0x0);
  MrcCall->MrcSetMem ((UINT8 *) UpperByte, sizeof (UpperByte), 0x0);
  MrcCall->MrcSetMem ((UINT8 *) EnablePprDevice, sizeof (EnablePprDevice), 0x0);

  // DDR5 PPR Beginning Sequence
  static const ModeRegisterDataStruct Ddr5PprGuardKeySequence[] = {
    { mrMR24, 0xCF },
    { mrMR24, 0x73 },
    { mrMR24, 0xBB },
    { mrMR24, 0x3B },
    { mrEndOfSequence, 0 }
  };

  MRC_DEBUG_MSG (Debug, MSG_LEVEL_ERROR, "Entering PPR\n");
  ChannelOut = &Outputs->Controller[Controller].Channel[Channel];
  DimmIdx = (UINT8) RANK_TO_DIMM_NUMBER (Rank);
  DimmOut = &ChannelOut->Dimm[DimmIdx];
  NumDevices = DimmOut->PrimaryBusWidth / DimmOut->SdramWidth;
  //NumDevices += (Outputs->EccSupport) ? 1 : 0;
  NumDevices = MIN (NumDevices, MAX_BYTE_IN_DDR5_CHANNEL); // Ensure Device loop doesn't exceed max array sizes

  GetDdr5LowerUpperBytes (MrcData, Controller, Channel, Rank, LowerByte, (DimmOut->SdramWidth == 16) ? UpperByte : NULL);
  GetDdr5PprDevices (MrcData, Controller, Channel, Rank, ByteMask, EnablePprDevice);

  if ((BankGroup == 0x0) || (BankGroup == 0x1)) {
    MRNumber = mrMR54;
  } else if ((BankGroup == 0x2) || (BankGroup == 0x3)) {
    MRNumber = mrMR55;
  } else if ((BankGroup == 0x4) || (BankGroup == 0x5)) {
    MRNumber = mrMR56;
  } else if ((BankGroup == 0x6) || (BankGroup == 0x7)) {
    MRNumber = mrMR57;
  } else {
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_ERROR, "DDR5 PPR Error, incorrect parameters: BankGroup%d, BankAddress%d\n", BankGroup, BankAddress);
    return mrcWrongInputParameter;
  }

  for (Device = 0; Device < NumDevices; Device++) {
    // Only check devices that need repair
    if (!EnablePprDevice[Device]) {
      continue;
    }
    MrcIssueMrr (MrcData, Controller, Channel, Rank, MRNumber, MrrResult); // Check the correct MR for PPR Resources, based on BA/BG
    PprResource = MrrResult[LowerByte[Device]];
    if (BankGroup & 0x1) { // Odd number bank group
      PprResource = (PprResource & (MRC_BIT4 << BankAddress));
    } else { // Even number bank group
      PprResource = (PprResource & (MRC_BIT0 << BankAddress));
    }
    if (PprResource == 0) { // Resource is not available
      MRC_DEBUG_MSG (Debug, MSG_LEVEL_ERROR, "No PPR Resources available: BankGroup%d, BankAddress%d, Device%d\n", BankGroup, BankAddress, Device);
      return mrcPPRBankRowUnavailable;
    }

    // PPR resource for Bank Group/Address pair is available
    Offset = OFFSET_CALC_MC_CH (MC0_CH0_CR_SCHED_CBIT_REG, MC1_CH0_CR_SCHED_CBIT_REG, Controller, MC0_CH1_CR_SCHED_CBIT_REG, Channel);
    SchedCbit.Data = 0x0;
    SchedCbit.Data = MrcReadCR (MrcData, Offset);
    SchedCbit.Bits.dis_pt_it = 0x1;     // Disabling page-table idle timer, to allow custom wait time before PRE issued
    MrcWriteCR (MrcData, Offset, SchedCbit.Data);

    MrhCommand.Bits.CA_0 = 0x000B; // Precharge All command
    MrhCommand.Bits.CA_1 = 0x0;

    Status = MrcRunGenericMrh (MrcData, Controller, Channel, Rank, MrhCommand.Data, FALSE); // Issue PREA using MRH command
    if (Status != mrcSuccess) {
      return Status;
    }

    MrcIssueMrr (MrcData, Controller, Channel, Rank, mrMR23, MrrResult);
    MR23.Data32 = MrrResult[LowerByte[Device]];

    MR23.Bits.Hppr = 1; // Setting bit OP[0] to high for PPR
    Status = MrcIssueMrw (MrcData, Controller, Channel, Rank, mrMR23, MR23.Data32, FALSE);
    if (Status != mrcSuccess) {
      return Status;
    }

    for (Index = 0; Ddr5PprGuardKeySequence[Index].MRnum != mrEndOfSequence; Index++) { // Pass PPR Guard Key
      Status = MrcIssueMrw (MrcData, Controller, Channel, Rank, Ddr5PprGuardKeySequence[Index].MRnum, Ddr5PprGuardKeySequence[Index].Data, TRUE);
      if (Status != mrcSuccess) {
        return Status;
      }
    }

    DeviceByteMask = 1 << LowerByte[Device];
    if (DimmOut->SdramWidth == 16) {
      DeviceByteMask |= 1 << UpperByte[Device];
    }

    Status = MrcPprActAndDqLow (MrcData, Controller, Channel, Rank, Row, BankGroup, BankAddress, DeviceByteMask); // Issue ACT with target row and drive DQ low
    if (Status != mrcSuccess) {
      return Status;
    }

    MrcWait (MrcData, 2000 * MRC_TIMER_1MS);// Wait 2000ms for Repair to complete

    MrhCommand.Bits.CA_0 = 0x001B; // Precharge PB
    MrhCommand.Bits.CA_0 |= (BankGroup & 0x7) << 8;
    MrhCommand.Bits.CA_0 |= (BankAddress & 0x3) << 6;
    MrhCommand.Bits.CA_1 = 0x0;

    Status = MrcRunGenericMrh (MrcData, Controller, Channel, Rank, MrhCommand.Data, FALSE); // Issue PREpb for this BA/BG
    if (Status != mrcSuccess) {
      return Status;
    }

    SchedCbit.Bits.dis_pt_it = 0x0;     // Reenabling page-table idle timer
    MrcWriteCR (MrcData, Offset, SchedCbit.Data);

    MrcWait (MrcData, 50 * MRC_TIMER_1US);// Wait 50us for tPGMPST

    MR23.Bits.Hppr = 0; // Setting bit OP[0] to low to exit PPR
    Status = MrcIssueMrw (MrcData, Controller, Channel, Rank, mrMR23, MR23.Data32, FALSE);
    if (Status != mrcSuccess) {
      return Status;
    }
  } // for Device
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_ERROR, "PPR ended\n");
  return Status;
}

/**
  Check if the given MR is one of the DFE TAP MR's

  @param[in] MrAddr - MR address.

  @retval TRUE if this is one of the DFE TAP MR's
**/
BOOLEAN
Ddr5IsDimmDfeMr (
  IN UINT8 MrAddr
  )
{
  UINT32  Bit;
  UINT32  Tap1;

  for (Bit = 0; Bit < 16; Bit++) { // 8 bits for DQL, 8 bits for DQU
    Tap1 = mrMR129 + (8 * Bit);
    if ((MrAddr >= Tap1) && (MrAddr <= Tap1 + 3)) { // 4 taps for each bit
      return TRUE;
    }
  }
  return FALSE;
}

/**
  Return the DFE TAP of MR according to the MR Address.
  Also check if the MR represents the Bit0 in this TAP.

  @param[in]      MrAddr          - MR address
  @param[in, out] IsDimmDfeMrBit0 - Pointer used to return the Bit0 Indication.

  @retval Tap Index (0-3).
**/
UINT8
Ddr5GetDimmDfeTap (
  IN  UINT8     MrAddr,
  OUT BOOLEAN   *IsDimmDfeMrBit0
  )
{
  UINT8  BitTap;

  BitTap = (MrAddr % mrMR129);
  if (BitTap < MAX_DFE_TAP_DDR5) {
    *IsDimmDfeMrBit0  = TRUE;
    return BitTap;
  } else {
    *IsDimmDfeMrBit0 = FALSE;
    return ((MrAddr - mrMR129) % 4);
  }
}

/*
  Calculate DqioDuration based on frequency for DDR5

  @param[in]  MrcData      - Include all MRC global data
  @param[out] DqioDuration - DqioDuration calculated

  @retval mrcSuccess               - if it success
  @retval mrcUnsupportedTechnology - if the frequency doesn't match
*/
MrcStatus
Ddr5GetDqioDuration (
  IN     MrcParameters *const MrcData,
  OUT    UINT8               *DqioDuration
  )
{
  MrcOutput     *Outputs;
  MrcFrequency  Frequency;

  Outputs   = &MrcData->Outputs;
  Frequency = Outputs->Frequency;

  if (Frequency >= f5333) {
    *DqioDuration = 64;
  } else if (Frequency >= f3467) {
    *DqioDuration = 63;
  } else if (Frequency >= f3200) {
    *DqioDuration = 60;
  } else if (Frequency >= f2933) {
    *DqioDuration = 55;
  } else if (Frequency >= f2667) {
    *DqioDuration = 50;
  } else if (Frequency >= f2400) {
    *DqioDuration = 45;
  } else if (Frequency >= f2133) {
    *DqioDuration = 40;
  } else if (Frequency >= f1867) {
    *DqioDuration = 35;
  } else if (Frequency >= f1600) {
    *DqioDuration = 30;
  } else if (Frequency >= f1333) {
    *DqioDuration = 25;
  } else if (Frequency >= f1067) {
    *DqioDuration = 20;
  } else {
    MRC_DEBUG_MSG (&Outputs->Debug, MSG_LEVEL_ERROR, "%s Frequency unsupported\n", gErrString);
    return mrcUnsupportedTechnology;
  }

  return mrcSuccess;
}
