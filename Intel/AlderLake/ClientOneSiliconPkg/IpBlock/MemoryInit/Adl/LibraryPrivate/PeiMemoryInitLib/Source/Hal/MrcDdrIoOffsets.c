/** @file
  This file contains functions to get DDR IO Data Offsets
  used memory training.

@copyright
  INTEL CONFIDENTIAL
  Copyright 2013 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains an 'Intel Peripheral Driver' and is uniquely identified as
  "Intel Reference Module" and is licensed for Intel CPUs and chipsets under
  the terms of your license agreement with Intel or your vendor. This file may
  be modified by the user, subject to additional terms of the license agreement.

@par Specification Reference:
**/

#include "McAddress.h"
#include "MrcInterface.h"
#include "MrcDdrIoOffsets.h"
#include "MrcHalRegisterAccess.h"
#include "MrcCommon.h"
#include "MrcDdrIoApi.h"

#define DDR4_RANK1_CLK_CHANNEL_OFFSET (3)

///
/// Local Functions
///
/**
  This function returns the offset to access specific Strobe of DllWeakLock.

  @params[in]  Strobe - 0-based index of Strobe to access.

  @retval UINT32 - CR offset
**/
static
UINT32
DllWeakLockOffset (
  IN  UINT32  const   Strobe
  )
{
  UINT32 Offset = 0xFFFFFFFF;

  // @todo_adl Offset  = DLLDDRDATA0_CR_DLLWEAKLOCK_REG;
  // Offset += (DLLDDRDATA1_CR_DLLWEAKLOCK_REG - DLLDDRDATA0_CR_DLLWEAKLOCK_REG) * Strobe;

  return Offset;
}

/*

  This function returns the offset to access specific Channel/Strobe of DdrCrCmdBusTrain.

  @params[in]  Channel - 0-based index of Channel to access.
  @params[in]  Strobe  - 0-based index of Strobe to access.

  @retval UINT32 - CR offset
*/
static
UINT32
CmdBusTrainOffset (
  IN  UINT32  const   Channel,
  IN  UINT32  const   Strobe
  )
{
  UINT32 Offset;

  Offset  = DATA0CH0_CR_DDRCRCMDBUSTRAIN_REG;
  Offset += (DATA0CH1_CR_DDRCRCMDBUSTRAIN_REG - DATA0CH0_CR_DDRCRCMDBUSTRAIN_REG) * Channel +
            (DATA1CH0_CR_DDRCRCMDBUSTRAIN_REG - DATA0CH0_CR_DDRCRCMDBUSTRAIN_REG) * Strobe;

  return Offset;
}

/*

  This function returns the offset to access specific Channel/Strobe of DataTrainFeedback.

  @params[in]  Channel - 0-based index of Channel to access.
  @params[in]  Strobe  - 0-based index of Strobe to access.

  @retval UINT32 - CR offset
*/
static
UINT32
DataTrainFeedbackOffset (
  IN  UINT32  const   Channel,
  IN  UINT32  const   Strobe
  )
{
  UINT32 Offset;

  Offset  = DATA0CH0_CR_DATATRAINFEEDBACK_REG;
  Offset += (DATA0CH1_CR_DATATRAINFEEDBACK_REG - DATA0CH0_CR_DATATRAINFEEDBACK_REG) * Channel +
            (DATA1CH0_CR_DATATRAINFEEDBACK_REG - DATA0CH0_CR_DATATRAINFEEDBACK_REG) * Strobe;

  return Offset;
}

/*
  This function returns the offset to access specific Channel/Strobe/Bit of Data's DccLaneStatus.

  @params[in]  Channel - 0-based index of Channel to access.
  @params[in]  Strobe  - 0-based index of Strobe to access.
  @params[in]  Bit     - 0-based index of Bit to access.

  @retval UINT32 - CR offset
*/
static
UINT32
DataDccLaneStatusOffset (
  IN  UINT32  const Channel,
  IN  UINT32  const Strobe,
  IN  UINT32  const Bit
  )
{
  UINT32 Offset = 0xFFFFFFFF;
  /* todo_adl
  UINT32 BitDivision;
  BitDivision = Bit / 3;  //3 bits per register

  Offset = OFFSET_CALC3 (
    DATA0CH0_CR_DCCLANESTATUS0_REG, DATA0CH1_CR_DCCLANESTATUS0_REG, Channel,
    DATA1CH0_CR_DCCLANESTATUS0_REG, Strobe,
    DATA0CH0_CR_DCCLANESTATUS1_REG, BitDivision
  );
  */
  return Offset;
}

/*
  This function returns the offset to access specific Channel/Strobe/Bit of CCC's DccLaneStatus.

  @params[in]  Channel - 0-based index of Channel to access.
  @params[in]  Bit     - 0-based index of Bit to access.

  @retval UINT32 - CR offset
*/
static
UINT32
CccDccLaneStatusOffset (
  IN  UINT32  const Channel,
  IN  UINT32  const Bit
  )
{
  UINT32 Offset = 0xFFFFFFFF;
  /* todo_adl
  UINT32 BitDivision;
  BitDivision = Bit / 3;  //3 bits per register

  Offset = OFFSET_CALC_MC_CH (
    CH0CCC_CR_DCCLANESTATUS0_REG, CH1CCC_CR_DCCLANESTATUS0_REG, Channel,
    CH0CCC_CR_DCCLANESTATUS1_REG, BitDivision
  );
  */
  return Offset;
}

/*
  This function returns the offset to access specific Channel/Strobe of Data's DCCCALCCONTROL.

  @params[in]  Channel - 0-based index of Channel to access.
  @params[in]  Strobe  - 0-based index of Strobe to access.

  @retval UINT32 - CR offset
*/
static
UINT32
DataDccCalcCtlOffset (
  IN  UINT32  const Channel,
  IN  UINT32  const Strobe
  )
{
  UINT32 Offset = 0xFFFFFFFF;
  /* todo_adl
  Offset = OFFSET_CALC_MC_CH (
    DATA0CH0_CR_DCCCALCCONTROL_REG, DATA0CH1_CR_DCCCALCCONTROL_REG, Channel,
    DATA1CH0_CR_DCCCALCCONTROL_REG, Strobe
  );
  */
  return Offset;
}

/*
  This function returns the offset to access specific Channel/Strobe of Data's DCCFSMCONTROL.

  @params[in]  Channel - 0-based index of Channel to access.
  @params[in]  Strobe  - 0-based index of Strobe to access.

  @retval UINT32 - CR offset
*/
UINT32
DataDccFsmCtlOffset (
  IN  UINT32  const Channel,
  IN  UINT32  const Strobe
  )
{
  UINT32 Offset = 0xFFFFFFFF;
  /* todo_adl
  Offset = OFFSET_CALC_MC_CH (
    DATA0CH0_CR_DCCFSMCONTROL_REG, DATA0CH1_CR_DCCFSMCONTROL_REG, Channel,
    DATA1CH0_CR_DCCFSMCONTROL_REG, Strobe
  );
  */
  return Offset;
}

/**
  This function returns the offset to access specific Channel/Strobe of DataControl0.

  @params[in]  Channel - 0-based index of Channel to access.
  @params[in]  Strobe  - 0-based index of Strobe to access.

  @retval UINT32 - CR offset
**/
UINT32
DataControl0Offset (
  IN  UINT32  const   Channel,
  IN  UINT32  const   Strobe
  )
{
  UINT32 Offset;

  Offset  = DATA0CH0_CR_DDRCRDATACONTROL0_REG;
  Offset += (DATA0CH1_CR_DDRCRDATACONTROL0_REG - DATA0CH0_CR_DDRCRDATACONTROL0_REG) * Channel +
            (DATA1CH0_CR_DDRCRDATACONTROL0_REG - DATA0CH0_CR_DDRCRDATACONTROL0_REG) * Strobe;

  return Offset;
}

/**
  This function returns the offset to access specific Channel/Strobe of DataControl1.

  @params[in]  Channel - 0-based index of Channel to access.
  @params[in]  Strobe  - 0-based index of Strobe to access.

  @retval UINT32 - CR offset
**/
UINT32
DataControl1Offset (
  IN  UINT32  const   Channel,
  IN  UINT32  const   Strobe
  )
{
  UINT32 Offset;

  Offset  = DATA0CH0_CR_DDRCRDATACONTROL1_REG;
  Offset += (DATA0CH1_CR_DDRCRDATACONTROL1_REG - DATA0CH0_CR_DDRCRDATACONTROL1_REG) * Channel +
            (DATA1CH0_CR_DDRCRDATACONTROL1_REG - DATA0CH0_CR_DDRCRDATACONTROL1_REG) * Strobe;

  return Offset;
}

/**
  This function returns the offset to access specific Channel/Strobe of DataControl2.

  @params[in]  Channel - 0-based index of Channel to access.
  @params[in]  Strobe  - 0-based index of Strobe to access.

  @retval UINT32 - CR offset
**/
UINT32
DataControl2Offset (
  IN  UINT32  const Channel,
  IN  UINT32  const Strobe
  )
{
  UINT32 Offset;

  Offset  = DATA0CH0_CR_DDRCRDATACONTROL2_REG;
  Offset += (DATA0CH1_CR_DDRCRDATACONTROL2_REG - DATA0CH0_CR_DDRCRDATACONTROL2_REG) * Channel +
            (DATA1CH0_CR_DDRCRDATACONTROL2_REG - DATA0CH0_CR_DDRCRDATACONTROL2_REG) * Strobe;

  return Offset;
}

/**
  This function returns the offset to access specific Channel/Strobe of DataControl3.

  @params[in]  Channel - 0-based index of Channel to access.
  @params[in]  Strobe  - 0-based index of Strobe to access.

  @retval UINT32 - CR offset
**/
UINT32
DataControl3Offset (
  IN  UINT32  const Channel,
  IN  UINT32  const Strobe
  )
{
  UINT32 Offset;

  Offset = DATA0CH0_CR_DDRCRDATACONTROL3_REG;
  Offset += (DATA0CH1_CR_DDRCRDATACONTROL3_REG - DATA0CH0_CR_DDRCRDATACONTROL3_REG) * Channel +
            (DATA1CH0_CR_DDRCRDATACONTROL3_REG - DATA0CH0_CR_DDRCRDATACONTROL3_REG) * Strobe;

  return Offset;
}

/*
  This function returns the offset to access specific Channel/Rank/Strobe of DataDqs.

  @params[in]  Channel - 0-based index of Channel to access.
  @params[in]  Rank    - 0-based index of Rank to access.
  @params[in]  Strobe  - 0-based index of Strobe to access.

  @retval UINT32 - CR offset
*/

UINT32
DataDqsOffset (
  IN  UINT32  const Channel,
  IN  UINT32  const Rank,
  IN  UINT32  const Strobe
  )
{
  UINT32 Offset = 0xFFFFFFFF;

  /* @todo_adl
  Offset  = DATA0CH0_CR_DDRDATADQSRANK0_REG;
  Offset += (DATA0CH1_CR_DDRDATADQSRANK0_REG - DATA0CH0_CR_DDRDATADQSRANK0_REG) * Channel +
            (DATA0CH0_CR_DDRDATADQSRANK1_REG - DATA0CH0_CR_DDRDATADQSRANK0_REG) * Rank +
            (DATA1CH0_CR_DDRDATADQSRANK0_REG - DATA0CH0_CR_DDRDATADQSRANK0_REG) * Strobe;
            */
  return Offset;
}

/*
  This function returns the offset to access specific Channel/Strobe of DDRDATADQSRANKX.

  @params[in]  Channel - 0-based index of Channel to access.
  @params[in]  Strobe  - 0-based index of Strobe to access.

  @retval UINT32 - CR offset
*/

UINT32
DataTxDqsPbdOffset (
  IN  UINT32  const Channel,
  IN  UINT32  const Strobe
  )
{
  UINT32 Offset;

  Offset  = DATA0CH0_CR_DDRDATADQSRANKX_REG;
  Offset += (DATA0CH1_CR_DDRDATADQSRANKX_REG - DATA0CH0_CR_DDRDATADQSRANKX_REG) * Channel +
            (DATA1CH0_CR_DDRDATADQSRANKX_REG - DATA0CH0_CR_DDRDATADQSRANKX_REG) * Strobe;
  return Offset;
}

/*
  This function returns the offset to access specific Channel/Rank/Strobe/Bit of DataDqRankXLaneY.

  @params[in]  Channel - 0-based index of Channel to access.
  @params[in]  Rank    - 0-based index of Rank to access.
  @params[in]  Strobe  - 0-based index of Strobe to access.
  @params[in]  Bit     - 0-based index of Bit to access.

  @retval UINT32 - CR offset
*/
static
UINT32
DataDqRankXLaneYOffset (
  IN  UINT32  const Channel,
  IN  UINT32  const Rank,
  IN  UINT32  const Strobe,
  IN  UINT32  const Bit
  )
{
  UINT32 Offset;

  if (Channel >= MAX_CHANNEL) {
    // Overall Broadcast
    Offset = DATA_CR_DDRDATADQRANK0LANE0_REG;
    Offset += (DATA_CR_DDRDATADQRANK1LANE0_REG - DATA_CR_DDRDATADQRANK0LANE0_REG) * Rank +
              (DATA_CR_DDRDATADQRANK0LANE1_REG - DATA_CR_DDRDATADQRANK0LANE0_REG) * Bit;
  } else {
    Offset =   DATA0CH0_CR_DDRDATADQRANK0LANE0_REG;
    Offset += (DATA0CH1_CR_DDRDATADQRANK0LANE0_REG - DATA0CH0_CR_DDRDATADQRANK0LANE0_REG) * Channel +
              (DATA0CH0_CR_DDRDATADQRANK1LANE0_REG - DATA0CH0_CR_DDRDATADQRANK0LANE0_REG) * Rank +
              (DATA1CH0_CR_DDRDATADQRANK0LANE0_REG - DATA0CH0_CR_DDRDATADQRANK0LANE0_REG) * Strobe +
              (DATA0CH0_CR_DDRDATADQRANK0LANE1_REG - DATA0CH0_CR_DDRDATADQRANK0LANE0_REG) * Bit;
  }

  return Offset;
}



/*
  This function returns the offset to access specific Channel/Rank/Strobe/Bit of RxMatchedUnmatchedPcalXLaneYOffset.

  @params[in]  Channel - 0-based index of Channel to access.
  @params[in]  Rank    - 0-based index of Rank to access.
  @params[in]  Strobe  - 0-based index of Strobe to access.
  @params[in]  Bit     - 0-based index of Bit to access.

  @retval UINT32 - CR offset
*/
static
UINT32
RxMatchedUnmatchedPcalXLaneYOffset (
  IN  UINT32  const Channel,
  IN  UINT32  const Rank,
  IN  UINT32  const Strobe,
  IN  UINT32  const Bit
  )
{
  UINT32 Offset = 0;

  if (Rank < 2) {  // Rank 0/1
    Offset = DATA0CH0_CR_DQ0RXAMPCTL_REG +
            (DATA0CH1_CR_DQ0RXAMPCTL_REG - DATA0CH0_CR_DQ0RXAMPCTL_REG) * Channel +
            (DATA1CH0_CR_DQ0RXAMPCTL_REG - DATA0CH0_CR_DQ0RXAMPCTL_REG) * Strobe +
            (DATA0CH0_CR_DQ1RXAMPCTL_REG - DATA0CH0_CR_DQ0RXAMPCTL_REG) * Bit ;
  } else {         // Rank 2/3
    Offset = DATA0CH0_CR_DQRXAMPCTL0_REG +
            (DATA0CH1_CR_DQRXAMPCTL0_REG - DATA0CH0_CR_DQRXAMPCTL0_REG) * Channel +
            (DATA1CH0_CR_DQRXAMPCTL0_REG - DATA0CH0_CR_DQRXAMPCTL0_REG) * Strobe +
            (DATA0CH0_CR_DQRXAMPCTL1_REG - DATA0CH0_CR_DQRXAMPCTL0_REG) * (Bit / 3);
  }

  return Offset;
}
/*
  This function returns the offset to access specific Channel/Rank/Strobe/Bit of RxUnmatchedNcalXLaneYOffset.

  @params[in]  Channel - 0-based index of Channel to access.
  @params[in]  Rank    - 0-based index of Rank to access.
  @params[in]  Strobe  - 0-based index of Strobe to access.
  @params[in]  Bit     - 0-based index of Bit to access.

  @retval UINT32 - CR offset
*/
static
UINT32
RxUnmatchedNcalXLaneYOffset (
  IN  UINT32  const Channel,
  IN  UINT32  const Rank,
  IN  UINT32  const Strobe,
  IN  UINT32  const Bit
  )
{
  UINT32 Offset;

  Offset = DATA0CH0_CR_DQ0RXAMPCTL_REG +
          (DATA0CH1_CR_DQ0RXAMPCTL_REG - DATA0CH0_CR_DQ0RXAMPCTL_REG) * Channel +
          (DATA1CH0_CR_DQ0RXAMPCTL_REG - DATA0CH0_CR_DQ0RXAMPCTL_REG) * Strobe +
          (DATA0CH0_CR_DQ1RXAMPCTL_REG - DATA0CH0_CR_DQ0RXAMPCTL_REG) * Bit ;


  return Offset;
}

/**
  This function returns the offset to access specific Channel of MCMISCS_WRITECFGCH[0..7].

  @params[in]  Channel - 0-based index of Channel to access.

  @retval UINT32 - CR offset
**/
UINT32
WriteConfigOffset (
  IN  UINT32  const Channel
  )
{
  UINT32 Offset;

  Offset  = MCMISCS_WRITECFGCH0_REG;
  Offset += (MCMISCS_WRITECFGCH1_REG - MCMISCS_WRITECFGCH0_REG) * Channel;

  return Offset;
}

/**
  Return the offset to specific instance of DDRVTT[0..3] registers.

  @param[in] MrcData    - MRC global data
  @param[in] RegOffset  - Offset of DDRVTT0 register
  @param[in] Index      - VTT FUB index: [0..3]

  @retval UINT32 - CR offset
**/
UINT32
DdrVttOffset (
  IN MrcParameters* const MrcData,
  IN UINT32         const RegOffset,
  IN UINT32         const Index
  )
{
  UINT32 Offset;

  Offset = RegOffset + ((DDRVTT1_CR_AFE_STATUS_REG - DDRVTT0_CR_AFE_STATUS_REG) * Index);

  if (MrcData->Inputs.A0) {
    Offset += (DDRVTT0_CR_AFE_STATUS_A0_REG - DDRVTT0_CR_AFE_STATUS_REG);
  }

  return Offset;
}

#ifdef FULL_EV_MERGE
/**
  Function used to get the CR Offset for DATATCOCONTROL0/1.

  @param[in]  Channel - DDR Channel Number within the processor socket (0-based).
  @param[in]  Strobe  - DQS data group within the channel (0-based).
  @param[in]  Bit     - Bit within the strobe (0-based).

  @retval CR Offset.
**/
static
UINT32
GetDdrIoDataTcoControl(
  IN  UINT32  const   Channel,
  IN  UINT32  const   Strobe,
  IN  UINT32  const   Bit
)
{
  UINT32 Offset = 0xFFFFFFFF;
  /* @todo_adl
  Offset = DATA0CH0_CR_DDRCRDATATCOCONTROL0_REG;
  Offset += (DATA0CH1_CR_DDRCRDATATCOCONTROL0_REG - DATA0CH0_CR_DDRCRDATATCOCONTROL0_REG) * Channel +
    (DATA1CH0_CR_DDRCRDATATCOCONTROL0_REG - DATA0CH0_CR_DDRCRDATATCOCONTROL0_REG) * Strobe;

  if (Bit > 4) {
    Offset += (DATA0CH0_CR_DDRCRDATATCOCONTROL1_REG - DATA0CH0_CR_DDRCRDATATCOCONTROL0_REG);
  }
  */
  return Offset;
}
#endif

/**
  Function used to get the CR Offset for Data Read Groups.

  @param[in]  Group       - DDRIO group being accessed.
  @param[in]  Socket      - Processor socket in the system (0-based).  Not used in Core MRC.
  @param[in]  Controller  - Memory controller in the processor socket (0-based).
  @param[in]  Channel     - DDR Channel Number within the processor socket (0-based).
  @param[in]  Rank        - Rank index in the channel. (0-based).
  @param[in]  Strobe      - Dqs data group within the rank (0-based).
  @param[in]  Bit         - Bit index within the data group (0-based).
  @param[in]  FreqIndex   - Index supporting multiple operating frequencies. (Not used in Client CPU's)

  @retval CR Offset.
**/
UINT32
GetDdrIoDataReadOffsets (
  IN  MrcParameters   *MrcData,
  IN  GSM_GT          Group,
  IN  UINT32          Socket,
  IN  UINT32          Controller,
  IN  UINT32          Channel,
  IN  UINT32          Rank,
  IN  UINT32          Strobe,
  IN  UINT32          Bit,
  IN  UINT32          FreqIndex
  )
{
  MrcOutput *Outputs;
  BOOLEAN   Lpddr;
  BOOLEAN   A0;
  UINT32    IpChannel;
  UINT32    Offset;

  Outputs = &MrcData->Outputs;
  Lpddr   = Outputs->Lpddr;
  Offset  = MRC_UINT32_MAX;
  A0      = MrcData->Inputs.A0;

  switch (Group) {
    case DqsOdtCompOffset:
      Offset = DataControl2Offset (Channel, Strobe);
      break;

    case RxBiasCtl:
    case RxBiasVrefSel:
    case RxBiasTailCtl:
    case RxLpddrMode:
      Offset = DATA0CH0_CR_AFEMISC_REG +
             ((DATA0CH1_CR_AFEMISC_REG - DATA0CH0_CR_AFEMISC_REG) * Channel) +
             ((DATA1CH0_CR_AFEMISC_REG - DATA0CH0_CR_AFEMISC_REG) * Strobe);
      break;

    case RecEnDelay:
    case RxDqsPDelay:
    case RxDqsNDelay:
      Offset = DATA0CH0_CR_RXCONTROL0RANK0_REG +
        ((DATA0CH1_CR_RXCONTROL0RANK0_REG - DATA0CH0_CR_RXCONTROL0RANK0_REG) * Channel) +
        ((DATA0CH0_CR_RXCONTROL0RANK1_REG - DATA0CH0_CR_RXCONTROL0RANK0_REG) * Rank) +
        ((DATA1CH0_CR_RXCONTROL0RANK0_REG - DATA0CH0_CR_RXCONTROL0RANK0_REG) * Strobe);
      break;

    case CompRcompOdtUpPerStrobe:
    case CompRcompOdtDnPerStrobe:
      Offset = DATA0CH0_CR_DDRCRDATACOMP0_REG +
        ((DATA0CH1_CR_DDRCRDATACOMP0_REG - DATA0CH0_CR_DDRCRDATACOMP0_REG) * Channel) +
        ((DATA1CH0_CR_DDRCRDATACOMP0_REG - DATA0CH0_CR_DDRCRDATACOMP0_REG) * Strobe);
      break;

    case RxVref:
//    case RxPerBitDeskewCal:
      Offset = DataControl3Offset(Channel, Strobe);
      break;

    case RxTap1:
    case RxTap2:
    case RxTap3:
    case RxTap4:
    case RxTap1En:
    case RxTap2En:
    case RxTap3En:
    case RxC:
    case RxR:
    case RxEq:
      Offset = DATA0CH0_CR_DQRXCTL0_REG +
        ((DATA0CH1_CR_DQRXCTL0_REG - DATA0CH0_CR_DQRXCTL0_REG) * Channel) +
        ((DATA1CH0_CR_DQRXCTL0_REG - DATA0CH0_CR_DQRXCTL0_REG) * Strobe);
      break;

    case RxDqsEq:
      Offset = DATA0CH0_CR_DQSTXRXCTL_REG +
        ((DATA0CH1_CR_DQSTXRXCTL_REG - DATA0CH0_CR_DQSTXRXCTL_REG) * Channel) +
        ((DATA1CH0_CR_DQSTXRXCTL_REG - DATA0CH0_CR_DQSTXRXCTL_REG) * Strobe);
      break;

    case RxDqsBitDelay:
    case RxDqsBitOffset:
    case RxDqsNBitDelay:
    case RxDqsPBitDelay:
      Offset = DataDqRankXLaneYOffset (Channel, Rank, Strobe, Bit);
      break;

    case RxVoc:
    case RxVocUnmatched:
      Offset = DataDqRankXLaneYOffset (Channel, Rank, Strobe, Bit);
      break;

    case RoundTripDelay:
      IpChannel = LP_IP_CH (Lpddr, Channel);
      Offset = OFFSET_CALC_MC_CH (MC0_CH0_CR_SC_ROUNDTRIP_LATENCY_REG, MC1_CH0_CR_SC_ROUNDTRIP_LATENCY_REG, Controller, MC0_CH1_CR_SC_ROUNDTRIP_LATENCY_REG, IpChannel);
      break;

    case RxFlybyDelay:
      if (A0) {
        Offset  = MCMISCS_READCFGCH01_A0_REG;
        Offset += (MCMISCS_READCFGCH23_A0_REG - MCMISCS_READCFGCH01_A0_REG) * (Channel / 2);
      } else {
        Offset  = MCMISCS_READCFGCH01_REG;
        Offset += (MCMISCS_READCFGCH23_REG - MCMISCS_READCFGCH01_REG) * (Channel / 2);
      }
      break;

    case RxFifoRdEnFlybyDelay:
      if (A0) {
        Offset  = MCMISCS_RXDQFIFORDENCH01_A0_REG;
        Offset += (MCMISCS_RXDQFIFORDENCH23_A0_REG - MCMISCS_RXDQFIFORDENCH01_A0_REG) * (Channel / 2);
      } else {
        Offset  = MCMISCS_RXDQFIFORDENCH01_REG;
        Offset += (MCMISCS_RXDQFIFORDENCH23_REG - MCMISCS_RXDQFIFORDENCH01_REG) * (Channel / 2);
      }
      break;

    case RxIoTclDelay:
    case RxFifoRdEnTclDelay:
    case RxDqDataValidDclkDelay:
    case RxDqDataValidQclkDelay:
      Offset  = MCMISCS_READCFGCH0_REG;
      Offset += (MCMISCS_READCFGCH1_REG - MCMISCS_READCFGCH0_REG) * Channel;
      break;

    case RxRankMuxDelay:
    case SenseAmpDelay:
    case SenseAmpDuration:
    case McOdtDelay:
    case McOdtDuration:
    case DqsOdtDelay:
    case DqsOdtDuration:
      Offset = DataControl1Offset (Channel, Strobe);
      break;

    case RxVrefVttDecap:
    case RxVrefVddqDecap:
      Offset = DATA0CH0_CR_DQRXCTL1_REG +
             ((DATA0CH1_CR_DQRXCTL1_REG - DATA0CH0_CR_DQRXCTL1_REG) * Channel) +
             ((DATA1CH0_CR_DQRXCTL1_REG - DATA0CH0_CR_DQRXCTL1_REG) * Strobe);
      break;

    case ForceDfeDisable:
      if (MrcData->Inputs.Q0Regs) {
        Offset = DATA0CH0_CR_DQRXCTL1_REG +
               ((DATA0CH1_CR_DQRXCTL1_REG - DATA0CH0_CR_DQRXCTL1_REG) * Channel) +
               ((DATA1CH0_CR_DQRXCTL1_REG - DATA0CH0_CR_DQRXCTL1_REG) * Strobe);
      } else {
        Offset = DATA0CH0_CR_DQSTXRXCTL0_REG +
               ((DATA0CH1_CR_DQSTXRXCTL0_REG - DATA0CH0_CR_DQSTXRXCTL0_REG) * Channel) +
               ((DATA1CH0_CR_DQSTXRXCTL0_REG - DATA0CH0_CR_DQSTXRXCTL0_REG) * Strobe);
      }
      break;

    case RxDqsAmpOffset:
    case RxDqsUnmatchedAmpOffset:
      Offset = DataDqsOffset (Channel, Rank, Strobe);
      break;

    case DataRxD0PiCb:
    case DataSDllPiCb:
// @todo_adl      Offset = DataControl6Offset(Channel, Strobe);
      break;

    case VccDllRxD0PiCb:
    case VccDllSDllPiCb:
// @todo_adl      Offset = DDRPHY_COMP_CR_VCCDLLREPLICACTRL2_REG;
      break;

    case RxCben:
// @todo_adl      Offset = DLLDDRDATA0_CR_PITUNE_REG + ((DLLDDRDATA1_CR_PITUNE_REG - DLLDDRDATA0_CR_PITUNE_REG) * Strobe);
      break;

    case RloadDqsDn:
// @todo_adl      Offset = DDRVCCDLL0_CR_DDRCRVCCDLLCOMPDLL_REG;
      break;

    case DqOdtVrefUp:
    case DqOdtVrefDn:
      Offset = DDRPHY_COMP_CR_DDRCRCOMPCTL0_REG;
      break;

    case CompRcompOdtUp:
    case CompRcompOdtDn:
      Offset = DDRPHY_COMP_CR_DDRCRDATACOMP0_REG;
      break;

    case PanicVttDnLp:
// @todo_adl      Offset = DDRPHY_COMP_CR_DDRCRCOMPVTTPANIC2_REG;
      break;

    case VttGenStatusSelCount:
    case VttGenStatusCount:
      if (A0) {
        Offset = DDRVTT0_CR_DDRCRVTTGENSTATUS_A0_REG + ((DDRVTT1_CR_DDRCRVTTGENSTATUS_A0_REG - DDRVTT0_CR_DDRCRVTTGENSTATUS_A0_REG) * Strobe);
      } else {
        Offset = DDRVTT0_CR_DDRCRVTTGENSTATUS_REG + ((DDRVTT1_CR_DDRCRVTTGENSTATUS_REG - DDRVTT0_CR_DDRCRVTTGENSTATUS_REG) * Strobe);
      }
      break;

    default:
      break;
  }

  return Offset;
}

/**
  Function used to get the CR Offset for Write Data Groups.

  @param[in]  Group       - DDRIO group being accessed.
  @param[in]  Socket      - Processor socket in the system (0-based).  Not used in Core MRC.
  @param[in]  Channel     - DDR Channel Number within the processor socket (0-based).
  @param[in]  Rank        - Rank index in the channel. (0-based).
  @param[in]  Strobe      - Dqs data group within the rank (0-based).
  @param[in]  Bit         - Bit index within the data group (0-based).
  @param[in]  FreqIndex   - Index supporting multiple operating frequencies. (Not used in Core MRC)

  @retval CR Offset.
**/
UINT32
GetDdrIoDataWriteOffsets (
  IN  MrcParameters   *MrcData,
  IN  GSM_GT          Group,
  IN  UINT32          Socket,
  IN  UINT32          Channel,
  IN  UINT32          Rank,
  IN  UINT32          Strobe,
  IN  UINT32          Bit,
  IN  UINT32          FreqIndex
  )
{
  UINT32  Offset;
  BOOLEAN A0;

  Offset = MRC_UINT32_MAX;
  A0     = MrcData->Inputs.A0;

  switch (Group) {
    case TxRankMuxDelay:
      Offset = DataControl0Offset (Channel, Strobe);
      break;

    case TxDqsRankMuxDelay:
      Offset = DataControl2Offset (Channel, Strobe);
      break;

    case TxDqsDelay90:
    case TxDqDelay90:
      if (Rank < 2) {
        Offset = DATA0CH0_CR_TXCONTROL0_PH90_REG +
               ((DATA0CH1_CR_TXCONTROL0_PH90_REG - DATA0CH0_CR_TXCONTROL0_PH90_REG) * Channel) +
               ((DATA1CH0_CR_TXCONTROL0_PH90_REG - DATA0CH0_CR_TXCONTROL0_PH90_REG) * Strobe);
      } else {
        Offset = DATA0CH0_CR_TXCONTROL1_PH90_REG +
               ((DATA0CH1_CR_TXCONTROL1_PH90_REG - DATA0CH0_CR_TXCONTROL1_PH90_REG) * Channel) +
               ((DATA1CH0_CR_TXCONTROL1_PH90_REG - DATA0CH0_CR_TXCONTROL1_PH90_REG) * Strobe);
      }
      break;

    case TxDqsDelay:
    case TxDqDelay:
    case TxEq:
      Offset = DATA0CH0_CR_TXCONTROL0RANK0_REG +
             ((DATA0CH1_CR_TXCONTROL0RANK0_REG - DATA0CH0_CR_TXCONTROL0RANK0_REG) * Channel) +
             ((DATA0CH0_CR_TXCONTROL0RANK1_REG - DATA0CH0_CR_TXCONTROL0RANK0_REG) * Rank) +
             ((DATA1CH0_CR_TXCONTROL0RANK0_REG - DATA0CH0_CR_TXCONTROL0RANK0_REG) * Strobe);
      break;

    case TxEqCoeff0:
    case TxEqCoeff1:
    case TxEqCoeff2:
      Offset = DATA0CH0_CR_DQTXCTL0_REG +
             ((DATA0CH1_CR_DQTXCTL0_REG - DATA0CH0_CR_DQTXCTL0_REG) * Channel) +
             ((DATA1CH0_CR_DQTXCTL0_REG - DATA0CH0_CR_DQTXCTL0_REG) * Strobe);
      break;

    case TxDqBitDelay:
    case TxDqBitDelay90:
    case TxDqDccOffset:
      Offset = DataDqRankXLaneYOffset (Channel, Rank, Strobe, Bit);
      break;

    case RxUnmatchedOffNcal:
       Offset = RxUnmatchedNcalXLaneYOffset (Channel, Rank, Strobe, Bit);
      break;

    case RxMatchedUnmatchedOffPcal:
       Offset = RxMatchedUnmatchedPcalXLaneYOffset (Channel, Rank, Strobe, Bit);
      break;

    case TxDqFifoRdEnFlybyDelay:
      if (A0) {
        Offset  = MCMISCS_WRITECFGCH01_A0_REG;
        Offset += (MCMISCS_WRITECFGCH67_A0_REG - MCMISCS_WRITECFGCH45_A0_REG) * (Channel / 2);
        if (Channel > 1) {
          Offset += 8;
        }
      } else {
        Offset  = MCMISCS_WRITECFGCH01_REG;
        Offset += (MCMISCS_WRITECFGCH67_REG - MCMISCS_WRITECFGCH45_REG) * (Channel / 2);
      }
      break;

    case RxRptChDqClkOn:
    case TxRptChDqClkOn:
    case TxDqFifoWrEnTcwlDelay:
    case TxDqFifoRdEnTcwlDelay:
      Offset = WriteConfigOffset (Channel);
      break;

    case TxRonUp:
    case TxRonDn:
    case SCompCodeDq:
      Offset = DDRPHY_COMP_CR_DDRCRDATACOMP0_REG;
      break;

    case TxTco:
      Offset = DATA0CH0_CR_COMPCTL2_REG +
             ((DATA0CH1_CR_COMPCTL2_REG - DATA0CH0_CR_COMPCTL2_REG) * Channel) +
             ((DATA1CH0_CR_COMPCTL2_REG - DATA0CH0_CR_COMPCTL2_REG) * Strobe);
      break;

    case TxDqsTcoP:
    case TxDqsTcoN:
      Offset = DATA0CH0_CR_COMPCTL1_REG +
             ((DATA0CH1_CR_COMPCTL1_REG - DATA0CH0_CR_COMPCTL1_REG) * Channel) +
             ((DATA1CH0_CR_COMPCTL1_REG - DATA0CH0_CR_COMPCTL1_REG) * Strobe);
      break;
    case TxDqsDccOffset:
      Offset = DataDqsOffset (Channel, Rank, Strobe);
      break;

    case TxDqsBitDelay:
      Offset = DataTxDqsPbdOffset (Channel, Strobe);
      break;

    case DqDrvVrefUp:
    case DqDrvVrefDn:
      Offset = DDRPHY_COMP_CR_DDRCRCOMPCTL0_REG;
      break;

//    case TxPerBitDeskewCal:
//    case SCompBypassDq:
//      Offset = DataControl2Offset (Channel, Strobe);
//      break;

    case DqScompPC:
    case TxSlewRate:
// @todo_adl      Offset = DDRPHY_COMP_CR_DDRCRCOMPCTL1_REG;
      break;

    default:
      break;
    }

  return Offset;
}

/**
  Function used to get the CR Offset for training offset and comp offset Groups.

  @param[in]  MrcData   - Global MRC data structure.
  @param[in]  Group     - DDRIO group being accessed.
  @param[in]  Socket    - Processor socket in the system (0-based).  Not used in Core MRC.
  @param[in]  Channel   - DDR Channel Number within the processor socket (0-based).
  @param[in]  Rank      - Rank index in the channel. (0-based).
  @param[in]  Strobe    - Dqs data group within the rank (0-based).
  @param[in]  Lane      - Lane index to GSM_GT Group.
  @param[in]  FreqIndex - Index supporting multiple operating frequencies. (Not used in Client CPU's)

  @retval CR Offset.
**/
UINT32
GetDdrIoDataTrainOffsets (
  IN MrcParameters    *MrcData,
  IN  GSM_GT          Group,
  IN  UINT32          Socket,
  IN  UINT32          Channel,
  IN  UINT32          Rank,
  IN  UINT32          Strobe,
  IN  UINT32          Lane,
  IN  UINT32          FreqIndex
  )
{
  UINT32 Offset;

  Offset = MRC_UINT32_MAX;

  switch (Group) {
    case RecEnOffset:
    case RxDqsOffset:
    case RxVrefOffset:
    case TxDqsOffset:
    case TxDqOffset:
    case RxDqsPiUiOffset:
      Offset = DATA0CH0_CR_DDRCRDATAOFFSETTRAIN_REG +
             ((DATA0CH1_CR_DDRCRDATAOFFSETTRAIN_REG - DATA0CH0_CR_DDRCRDATAOFFSETTRAIN_REG) * Channel) +
             ((DATA1CH0_CR_DDRCRDATAOFFSETTRAIN_REG - DATA0CH0_CR_DDRCRDATAOFFSETTRAIN_REG) * Strobe);
      break;

    case CompOffsetAll:
    case CompOffsetVssHiFF:
      Offset = DATA0CH0_CR_DDRCRDATAOFFSETCOMP_REG +
             ((DATA0CH1_CR_DDRCRDATAOFFSETCOMP_REG - DATA0CH0_CR_DDRCRDATAOFFSETCOMP_REG) * Channel) +
             ((DATA1CH0_CR_DDRCRDATAOFFSETCOMP_REG - DATA0CH0_CR_DDRCRDATAOFFSETCOMP_REG) * Strobe);
      break;

    default:
      break;
  }

  return Offset;
}

/**
  Function used to get the CR Offset for DDR IO DCC Ccc Groups.

  @param[in]  MrcData     - Global MRC data structure.
  @param[in]  Group       - DDRIO group being accessed.
  @param[in]  Socket      - Processor socket in the system (0-based).  Not used in Core MRC.
  @param[in]  Channel     - DDR Channel Number within the processor socket (0-based).
  @param[in]  Rank        - Rank instance of the parameter (0-based).
  @param[in]  Lane        - Lane index to GSM_GT Group.
  @param[in]  FreqIndex   - Index supporting multiple operating frequencies. (Not used in Client CPU's)

  @retval CR Offset.
**/
UINT32
GetDdrIoDccCccOffsets (
  IN MrcParameters    *MrcData,
  IN  GSM_GT          Group,
  IN  UINT32          Socket,
  IN  UINT32          Channel,
  IN  UINT32          Rank,
  IN  UINT32          Lane,
  IN  UINT32          FreqIndex
  )
{
  UINT32      Offset;
  UINT32      CccFub;
  BOOLEAN     DtHalo;

  Offset  = MRC_UINT32_MAX;
  DtHalo  = MrcData->Inputs.DtHalo;

  if (Channel < MRC_NUM_CCC_INSTANCES) {
    if (DtHalo) {
      CccFub = CccIndexToFub[Channel];  // Channel is CCC index for CCC items, see MrcTranslateSystemToIp
    } else {
      CccFub = Channel;
    }
  } else {
    CccFub = 0;  // Channel can be MRC_IGNORE_ARG if it's not used
  }

  switch (Group) {

    case GsmIocDccDcdSampleSelCcc:
    case GsmIocStep1PatternCcc:
      Offset = OFFSET_CALC_CH (CH2CCC_CR_DCCCTL13_REG, CH0CCC_CR_DCCCTL13_REG, CccFub);
      break;

    case GsmIocDccCodePh0IndexCcc:         // Lane is Index[0..15]
      if (Lane <= 2) {
        Offset = OFFSET_CALC_CH (CH2CCC_CR_DCCCTL6_REG, CH0CCC_CR_DCCCTL6_REG, CccFub);
      } else if (Lane <= 6) {
        Offset = OFFSET_CALC_CH (CH2CCC_CR_DCCCTL7_REG, CH0CCC_CR_DCCCTL7_REG, CccFub);
      } else if (Lane <= 10) {
        Offset = OFFSET_CALC_CH (CH2CCC_CR_DCCCTL8_REG, CH0CCC_CR_DCCCTL8_REG, CccFub);
      } else if (Lane <= 13) {
        Offset = OFFSET_CALC_CH (CH2CCC_CR_DCCCTL9_REG, CH0CCC_CR_DCCCTL9_REG, CccFub);
      } else if (Lane <= 15) {
        Offset = OFFSET_CALC_CH (CH2CCC_CR_DCCCTL20_REG, CH0CCC_CR_DCCCTL20_REG, CccFub);
      }
      break;

    case GsmIocDccCodePh90IndexCcc:        // Lane is Index [6..9]
      if ((Lane >= 6) && (Lane <= 9)) {
        Offset = OFFSET_CALC_CH (CH2CCC_CR_DCCCTL11_REG, CH0CCC_CR_DCCCTL11_REG, CccFub);
      }
      break;

    case GsmIocTxpbddOffsetCcc:
    case GsmIocTxpbdPh90incrCcc:           // Lane is Index [6..9]
      if ((Lane >= 6) && (Lane <= 9)) {
        Offset = OFFSET_CALC_CH (CH2CCC_CR_TXPBDOFFSET_REG, CH0CCC_CR_TXPBDOFFSET_REG, CccFub);
      }
      break;

    case GsmIocMdllCmnVcdlDccCodeCcc:
      Offset = OFFSET_CALC_CH (CH2CCC_CR_MDLLCTL2_REG, CH0CCC_CR_MDLLCTL2_REG, CccFub);
      break;

    default:
      break;
  }

  return Offset;
}

/**
  Function used to get the CR Offset for DCC Data Groups.

  @param[in]  Group       - DDRIO group being accessed.
  @param[in]  Socket      - Processor socket in the system (0-based).  Not used in Core MRC.
  @param[in]  Channel     - DDR Channel Number within the processor socket (0-based).
  @param[in]  Rank        - Rank index in the channel. (0-based).
  @param[in]  Strobe      - Dqs data group within the rank (0-based).
  @param[in]  Lane        - Lane index to GSM_GT Group.
  @param[in]  FreqIndex   - Index supporting multiple operating frequencies. (Not used in Client CPU's)

  @retval CR Offset.
**/
UINT32
GetDdrIoDccDataOffsets (
  IN  MrcParameters   *MrcData,
  IN  GSM_GT          Group,
  IN  UINT32          Socket,
  IN  UINT32          Channel,
  IN  UINT32          Rank,
  IN  UINT32          Strobe,
  IN  UINT32          Lane,
  IN  UINT32          FreqIndex
  )
{
  UINT32    Offset;
  UINT32    ChSeperation;
  UINT32    StrbSeperation;
  UINT32    RegOffset;

  Offset         = MRC_UINT32_MAX;
  ChSeperation   = DATA0CH1_CR_DCCCTL10_REG - DATA0CH0_CR_DCCCTL10_REG;    // Channel/strobe separation is same
  StrbSeperation = DATA1CH0_CR_DCCCTL10_REG - DATA0CH0_CR_DCCCTL10_REG;
  RegOffset      = (ChSeperation * Channel) + (StrbSeperation * Strobe);

  switch (Group) {
    case GsmIocDccDcdSampleSelData:
    case GsmIocStep1PatternData:
      Offset = DATA0CH0_CR_DCCCTL10_REG + RegOffset;
      break;

    case GsmIocDccCodePh0IndexData:                      // Lane is Index[0..9]
      if (Lane <= 2) {
        Offset = DATA0CH0_CR_DCCCTL5_REG + RegOffset;
      } else if (Lane <= 6) {
        Offset = DATA0CH0_CR_DCCCTL6_REG + RegOffset;
      } else if (Lane <= 9) {
        Offset = DATA0CH0_CR_DCCCTL7_REG + RegOffset;
      }
      break;

    case GsmIocDccCodePh90IndexData:                     // Lane is Index[1..9]
      if (Lane == 1) {
        Offset = DATA0CH0_CR_DCCCTL7_REG + RegOffset;
      } else if ((Lane >= 2) && (Lane <= 5)) {
        Offset = DATA0CH0_CR_DCCCTL8_REG + RegOffset;
      } else if ((Lane >= 6) && (Lane <= 9)) {
        Offset = DATA0CH0_CR_DCCCTL9_REG + RegOffset;
      }
      break;

    case GsmIocTxpbddOffsetData:                         // Lane is Index[1..9]
      if ((Lane == 1) || ((Lane >= 7) && (Lane <= 9))) {
        Offset = DATA0CH0_CR_TXPBDOFFSET1_REG + RegOffset;
      } else if ((Lane >= 2) && (Lane <= 6)) {
        Offset = DATA0CH0_CR_TXPBDOFFSET0_REG + RegOffset;
      }
      break;

    case GsmIocTxpbdPh90incrData:                        // Lane is Index[1..9]
      if ((Lane == 1) || ((Lane >= 4) && (Lane <= 9))) {
        Offset = DATA0CH0_CR_TXPBDOFFSET1_REG + RegOffset;
      } else if ((Lane == 2) || (Lane == 3)) {
        Offset = DATA0CH0_CR_TXPBDOFFSET0_REG + RegOffset;
      }
      break;

    case GsmIocMdllCmnVcdlDccCodeData:
      Offset = DATA0CH0_CR_MDLLCTL2_REG + RegOffset;
      break;

    default:
      break;
  }
  return Offset;
}

/**
  Function used to get the CR Offset for DDR IO Configuration settings.

  @param[in]  MrcData     - Global MRC data structure.
  @param[in]  Group       - DDRIO group being accessed.
  @param[in]  Socket      - Processor socket in the system (0-based).  Not used in Core MRC.
  @param[in]  Channel     - DDR Channel within the Memory Controller Number(0-based).
  @param[in]  Rank        - Rank index in the channel. (0-based).
  @param[in]  Strobe      - Dqs data group within the rank (0-based).
  @param[in]  Lane        - Lane index to GSM_GT Group.
  @param[in]  FreqIndex   - Index supporting multiple operating frequencies. (Not used in Client CPU's)

  @retval CR Offset.
**/
UINT32
MrcGetDdrIoConfigOffsets (
  IN MrcParameters    *MrcData,
  IN  GSM_GT          Group,
  IN  UINT32          Socket,
  IN  UINT32          Channel,
  IN  UINT32          Rank,
  IN  UINT32          Strobe,
  IN  UINT32          Lane,
  IN  UINT32          FreqIndex
  )
{
  UINT32  Offset;
  BOOLEAN A0;

  Offset = MRC_UINT32_MAX;
  A0 = MrcData->Inputs.A0;

  switch (Group) {
    case GsmIocWlWakeCyc:
    case GsmIocWlSleepCyclesAct:
    case GsmIocWlSleepCyclesLp:
    case GsmIocDllMask:
    case GsmIocDllWeakLock:
    case GsmIocDllWeakLock1:
      Offset = DllWeakLockOffset (Strobe);
      break;

    case GsmIocForceCmpUpdt:
//    case GsmIocWlLatency: // @todo No TGL version found
    case GsmIocNoDqInterleave:
    case GsmIocScramLpMode:
    case GsmIocScramDdr4Mode:
    case GsmIocScramDdr5Mode:
    case GsmIocScramGear1:
    case GsmIocScramGear4:
    case GsmIocDisClkGate:
    case GsmIocDisDataIdlClkGate:
    case GsmIocScramLp4Mode:
// @todo_adl    case GsmIocScramOvrdPeriodicToDvfsComp:
    case GsmIocChNotPop:
    case GsmIocDisIosfSbClkGate:
    case GsmIocLp5Wck2CkRatio:
    case GsmIocEccEn:
    case GsmIocWrite0En:
    case GsmIocScramWrite0En:
      Offset = DDRSCRAM_CR_DDRMISCCONTROL0_REG;
      break;

    case GsmIocScramLp5Mode:
      Offset = A0 ? MCMISCS_DDRWCKCONTROL_A0_REG : MCMISCS_DDRWCKCONTROL_REG;
      break;

    case GsmIocVccDllRxDeskewCal:
    case GsmIocVccDllTxDeskewCal:
    case GsmIocVccDllCccDeskewCal:
// @todo_adl      Offset = DDRPHY_COMP_CR_VCCDLLREPLICACTRL2_REG;
      break;

    case GsmIocVccDllControlBypass_V:
    case GsmIocVccDllControlSelCode_V:
    case GsmIocVccDllControlTarget_V:
    case GsmIocVccDllControlOpenLoop_V:
// @todo_adl      Offset = (Strobe) ? DDRVCCDLL1_CR_DDRCRVCCDLLCONTROL_REG : DDRVCCDLL0_CR_DDRCRVCCDLLCONTROL_REG;
      break;

    case GsmIocVsxHiControlSelCode_V:
    case GsmIocVsxHiControlOpenLoop_V:
// @todo_adl      Offset = DDRVSSHIAFEA_CR_DDRCRVSSHICONTROL_REG;
      break;

    case GsmIocCccPiEn:
    case GsmIocCccPiEnOverride:
      Offset = OFFSET_CALC_CH (CH2CCC_CR_DDRCRPINSUSED_REG, CH0CCC_CR_DDRCRPINSUSED_REG, Channel);
      break;

    case GsmIocClkPFallNRiseMode:
    case GsmIocClkPFallNRiseFeedback:
    case GsmIocClkPFallNRiseCcc56:
    case GsmIocClkPFallNRiseCcc78:
// @todo_adl      Offset = OFFSET_CALC_CH (CH0CCC_CR_DDRCRCCCPERBITDESKEWPFALLNRISE_REG, CH1CCC_CR_DDRCRCCCPERBITDESKEWPFALLNRISE_REG, Channel);
      break;

    case GsmIocClkPRiseNFallMode:
    case GsmIocClkPRiseNFallFeedback:
    case GsmIocClkPRiseNFallCcc56:
    case GsmIocClkPRiseNFallCcc78:
// @todo_adl      Offset = OFFSET_CALC_CH (CH0CCC_CR_DDRCRCCCPERBITDESKEWPRISENFALL_REG, CH1CCC_CR_DDRCRCCCPERBITDESKEWPRISENFALL_REG, Channel);
      break;

    case GsmIocVccDllGear1:
// @todo_adl      Offset = DDRPHY_COMP_CR_VCCDLLDQSDELAY_REG;
      break;

    case GsmIocIoReset:
      Offset = DDRSCRAM_CR_DDRMISCCONTROL1_REG;
      break;

    case GsmIocReadDqDqsMode:
    case GsmIocSenseAmpMode:
    case GsmIocDqsRFMode:
    case GsmIocCaTrainingMode:
    case GsmIocWriteLevelMode:
    case GsmIocReadLevelMode:
    case GsmIocRankOverrideEn:
    case GsmIocRankOverrideVal:
    case GsmIocDataTrainFeedback:
    case GsmIocRxAmpOffsetEn:
    case GsmIocDdrDqRxSdlBypassEn:
    case GsmIocDdrDqDrvEnOvrdData:
    case GsmIocDdrDqDrvEnOvrdModeEn:
      Offset = DataTrainFeedbackOffset (Channel, Strobe);
      break;

    case GsmIocForceRxAmpOn:
    case GsmIocForceOdtOn:
    case GsmIocForceOdtOnWithoutDrvEn:
    case GsmIocTxPiPwrDnDis:
    case GsmIocTxDisable:
    case GsmIocInternalClocksOn:
    case GsmIocTxOn:
    case GsmIocRxDisable:
    case GsmIocWlLongDelEn:
    case GsmIocBiasPMCtrl:
//    case GsmIocDataOdtMode:
//    case GsmIocRxVocMode:
    case GsmIocDataDqsOdtParkMode:
    case GsmIocDataDqOdtParkMode:
//    case GsmIocDataDqsNParkLow:
    case GsmIocLocalGateD0tx:
    case GsmIocDataCtlGear4:
      Offset = DataControl0Offset (Channel, Strobe);
      break;

    case GsmIocDataOdtStaticEn:
    case GsmIocTxEqEn:
    case GsmIocTxEqTapSelect:
    case GsmIocConstZTxEqEn:
      Offset = DATA0CH0_CR_DQTXCTL0_REG +
             ((DATA0CH1_CR_DQTXCTL0_REG - DATA0CH0_CR_DQTXCTL0_REG) * Channel) +
             ((DATA1CH0_CR_DQTXCTL0_REG - DATA0CH0_CR_DQTXCTL0_REG) * Strobe);
      break;

//    case GsmIocSdllSegmentDisable:
// @todo_adl    case GsmIocRXDeskewForceOn:
//      Offset = DataControl1Offset (Channel, Strobe);
//      break;

//    case GsmIocRxSALTailCtrl:
    case GsmIocRxPiPwrDnDis:
    case GsmIocCaParityTrain:
      Offset = DataControl3Offset (Channel, Strobe);
      break;

//    case GsmIocLeakerComp: // @todo No TGL version found
//    case GsmIocRxTypeSelect: // @todo No TGL version found
//    case GsmIocRxPathBiasRcomp: // @todo No TGL version found
//    case GsmIocRxPathBiasSel: // @todo No TGL version found
//      Offset = DataControl4Offset (Channel, Strobe);
//      break;

    case GsmIocDataCtlGear1:
      Offset = A0 ? DataControl2Offset (Channel, Strobe) : DataControl0Offset (Channel, Strobe);
      break;

    case GsmIocDataWrPreamble:
//    case GsmIocDqSlewDlyByPass:
//    case GsmIocRxVrefMFC:
//    case GsmIocVrefPwrDnEn: // @todo No TGL version found
    case GsmIocRxClkStg:
    case GsmIocDataRxBurstLen:
    case GsmIocDataInvertNibble:
      Offset = DataControl2Offset (Channel, Strobe);
      break;

    case GsmIocLdoFFCodeLockData:
      Offset  = DATA0CH0_CR_MDLLCTL2_REG +
               (DATA0CH1_CR_MDLLCTL2_REG - DATA0CH0_CR_MDLLCTL2_REG) * Channel +
               (DATA1CH0_CR_MDLLCTL2_REG - DATA0CH0_CR_MDLLCTL2_REG) * Strobe;
      break;

    case GsmIocDccFsmResetData:
    case GsmIocDcdRoCalSampleCountRstData:
    case GsmIocSrzDcdEnData:
    case GsmIocMdllDcdEnData:
    case GsmIocDcdSampleCountRstData:
    case GsmIocDcdSampleCountStartData:
    case GsmIocDcdRoLfsrData:
      Offset  = DATA0CH0_CR_DCCCTL2_REG +
               (DATA0CH1_CR_DCCCTL2_REG - DATA0CH0_CR_DCCCTL2_REG) * Channel +
               (DATA1CH0_CR_DCCCTL2_REG - DATA0CH0_CR_DCCCTL2_REG) * Strobe;
      break;

    case GsmIocDcdRoCalEnData:
    case GsmIocDcdRoCalStartData:
    case GsmIocDccStartData:
      Offset  = DATA0CH0_CR_DCCCTL11_REG +
               (DATA0CH1_CR_DCCCTL11_REG - DATA0CH0_CR_DCCCTL11_REG) * Channel +
               (DATA1CH0_CR_DCCCTL11_REG - DATA0CH0_CR_DCCCTL11_REG) * Strobe;
      break;

    case GsmIocLdoFFCodeLockCcc:
    case GsmIocDccFsmResetCcc:
    case GsmIocDcdRoCalEnCcc:
    case GsmIocDcdRoCalStartCcc:
    case GsmIocDcdRoCalSampleCountRstCcc:
    case GsmIocDccStartCcc:
    case GsmIocSrzDcdEnCcc:
    case GsmIocMdllDcdEnCcc:
      Offset = GetDdrIoCommandOffsets (MrcData, Group, Socket, Channel, Rank, Strobe, Lane, FreqIndex);
      break;

    case GsmIocStrobeOdtStaticEn:
    case GsmIocEnDqsNRcvEn:
      Offset = DataDqsTxRxOffset (Channel, Strobe);
      break;

    case GsmIocEnDqsNRcvEnGate:
    case GsmIocRxMatchedPathEn:
      Offset = DataSdlCtlOffset (Channel, Strobe);
      break;

    case GsmIocDqsMaskValue:
    case GsmIocDqsMaskPulseCnt:
    case GsmIocDqsPulseCnt:
    case GsmIocDqOverrideData:
    case GsmIocDqOverrideEn:
      Offset = CmdBusTrainOffset (Channel, Strobe);
      break;

    case GsmIocForceOnRcvEn:
      Offset = MarginModeControlOffset (Channel, Strobe);
      break;

    case GsmIocFFCodeIdleOffset:
    case GsmIocFFCodeWeakOffset:
    case GsmIocFFCodePiOffset:
    case GsmIocFFCodeWriteOffset:
    case GsmIocFFCodeReadOffset:
    case GsmIocFFCodePBDOffset:
    case GsmIocFFCodeCCCDistOffset:
        Offset = VccDllDataCCCCompOffset (Channel, Strobe);
        break;

    case GsmIocCapCancelCodeIdle:
    case GsmIocCapCancelCodePBD:
    case GsmIocCapCancelCodeWrite:
    case GsmIocCapCancelCodeRead:
    case GsmIocCapCancelCodePi:
      Offset = CouplingCapOffset (Channel, Strobe);
      break;

    case GsmIocVssHiFFCodeIdle:
    case GsmIocVssHiFFCodeWrite:
    case GsmIocVssHiFFCodeRead:
    case GsmIocVssHiFFCodePBD:
    case GsmIocVssHiFFCodePi:
      Offset = VssHiFFOffset (Channel, Strobe);
      break;

    case GsmIocEnableSpineGate:
      Offset = A0 ? MCMISCS_SPINEGATING_A0_REG : MCMISCS_SPINEGATING_REG;
      break;

    case GsmIocCmdVrefConverge:
      Offset = DDRPHY_COMP_NEW_CR_DIMMVREF_VREFADJUST2_REG;
      break;

    case GsmIocCompVddqOdtEn:
    case GsmIocCompVttOdtEn:
// @todo_adl      Offset = DDRPHY_COMP_CR_DDRCRCOMPCTL0_REG;
      break;

    case GsmIocCompOdtStaticDis:
    case GsmIocDisableQuickComp:
    case GsmIocSinStepAdv:
      // @todo_adl
      break;

    case GsmIocCompClkOn:
    case GsmIocSinStep:
      Offset = DDRPHY_COMP_CR_DDRCRCOMPCTL4_REG;
      break;

    case GsmIocVttPanicCompUpMult:
    case GsmIocVttPanicCompDnMult:
// @todo_adl      Offset = DDRPHY_COMP_CR_DDRCRCOMPVTTPANIC_REG;
      break;

    case GsmIocCmdDrvVref200ohm:
// @todo_adl      Offset = DDRPHY_COMP_CR_DDRCRCOMPCTL2_REG;
      break;

    case GsmIocRptChRepClkOn:
    case GsmIocCmdAnlgEnGraceCnt:
    case GsmIocTxAnlgEnGraceCnt:
    case GsmIocTxDqFifoRdEnPerRankDelDis:
      Offset = WriteConfigOffset (Channel);
      break;

    case GsmIocDataDccLaneStatusResult:
      Offset = DataDccLaneStatusOffset (Channel, Strobe, Lane);
      break;

    case GsmIocCccDccLaneStatusResult:
      Offset = CccDccLaneStatusOffset (Channel, Lane);
      break;

    case GsmIocDataDccStepSize:
    case GsmIocDataDcc2xStep:
      Offset = DataDccCalcCtlOffset (Channel, Strobe);
      break;

    case GsmIocDataDccSaveFullDcc:
    case GsmIocDataDccSkipCRWrite:
    case GsmIocDataDccMeasPoint:
    case GsmIocDataDccRankEn:
    case GsmIocDataDccLaneEn:
      Offset = DataDccFsmCtlOffset (Channel, Strobe);
      break;

    case GsmIocCccDccStepSize:
    case GsmIocCccDcc2xStep:
// @todo_adl      Offset = OFFSET_CALC_CH (CH0CCC_CR_DCCCALCCONTROL_REG, CH1CCC_CR_DCCCALCCONTROL_REG, Channel);
      break;

    case GsmIocCccDccCcc:
// @todo_adl       Offset = OFFSET_CALC_CH (CH0CCC_CR_DDRCRCCCPERBITDESKEW0_REG, CH1CCC_CR_DDRCRCCCPERBITDESKEW0_REG, Channel);
// @todo_adl       Offset += (CH0CCC_CR_DDRCRCCCPERBITDESKEW1_REG - CH0CCC_CR_DDRCRCCCPERBITDESKEW0_REG) * (Lane / 5);
      break;

    case GsmIocRetrainSwizzleCtlByteSel:
    case GsmIocRetrainSwizzleCtlBit:
    case GsmIocRetrainSwizzleCtlRetrainEn:
    case GsmIocRetrainSwizzleCtlSerialMrr:
      Offset = WrRetrainSwizzleControlOffset (Channel, Strobe);
      break;

    case GsmIocRetrainInitPiCode:
    case GsmIocRetrainDeltaPiCode:
    case GsmIocRetrainRoCount:
      Offset = WrRetrainRankOffset (Channel, Rank, Strobe);
      break;

    case GsmIocRetrainCtlInitTrain:
    case GsmIocRetrainCtlDuration:
    case GsmIocRetrainCtlResetStatus:
      Offset = WrRetrainControlStatusOffset (Channel, Strobe);
      break;

    case GsmIocDccDcdSampleSelCcc:
    case GsmIocStep1PatternCcc:
    case GsmIocDccCodePh0IndexCcc:
    case GsmIocDccCodePh90IndexCcc:
    case GsmIocTxpbddOffsetCcc:
    case GsmIocTxpbdPh90incrCcc:
    case GsmIocMdllCmnVcdlDccCodeCcc:
      Offset = GetDdrIoDccCccOffsets (MrcData, Group, Socket, Channel, Rank, Lane, FreqIndex);
      break;

    case GsmIocDccDcdSampleSelData:
    case GsmIocStep1PatternData:
    case GsmIocDccCodePh0IndexData:
    case GsmIocDccCodePh90IndexData:
    case GsmIocTxpbddOffsetData:
    case GsmIocTxpbdPh90incrData:
    case GsmIocMdllCmnVcdlDccCodeData:
      Offset = GetDdrIoDccDataOffsets (MrcData, Group, Socket, Channel, Rank, Strobe, Lane, FreqIndex);
       break;

    default:
      break;
  }

  return Offset;
}

/**
  Function used to get the CR Offset for DDR IO Command Groups.

  @param[in]  MrcData     - Global MRC data structure.
  @param[in]  Group       - DDRIO group being accessed.
  @param[in]  Socket      - Processor socket in the system (0-based).  Not used in Core MRC.
  @param[in]  Channel     - DDR Channel Number within the processor socket (0-based).
  @param[in]  Rank        - Rank instance of the parameter (0-based).
  @param[in]  GroupIndex  - Index for command group types that specify indicies (0-based).
  @param[in]  Lane        - Lane index to GSM_GT Group.
  @param[in]  FreqIndex   - Index supporting multiple operating frequencies. (Not used in Client CPU's)

  @retval CR Offset.
**/
UINT32
GetDdrIoCommandOffsets (
  IN MrcParameters    *MrcData,
  IN  GSM_GT          Group,
  IN  UINT32          Socket,
  IN  UINT32          Channel,
  IN  UINT32          Rank,
  IN  UINT32          GroupIndex,
  IN  UINT32          Lane,
  IN  UINT32          FreqIndex
  )
{
  UINT32      Offset;
  BOOLEAN     Ddr4;
  BOOLEAN     Ddr5;
  BOOLEAN     A0;
  UINT32      CccFub;
  UINT32      CccSeparation;
  MrcDdrType  DdrType;
  BOOLEAN     DtHalo;

  Offset  = MRC_UINT32_MAX;
  DdrType = MrcData->Outputs.DdrType;
  Ddr4    = (DdrType == MRC_DDR_TYPE_DDR4);
  Ddr5    = (DdrType == MRC_DDR_TYPE_DDR5);
  A0      = MrcData->Inputs.A0;
  DtHalo  = MrcData->Inputs.DtHalo;

  if (Channel < MRC_NUM_CCC_INSTANCES) {
    if (DtHalo) {
      CccFub = CccIndexToFub[Channel];  // Channel is CCC index for CCC items, see MrcTranslateSystemToIp
    } else {
      CccFub = Channel;
    }
  } else {
    CccFub = 0;  // Channel can be MRC_IGNORE_ARG if it's not used
  }
  // Note the order of CCC instances is not linear: ch2 is first, ch0 is second etc.
  CccSeparation = CH0CCC_CR_PICODE0_REG - CH2CCC_CR_PICODE0_REG;

  switch (Group) {
    case CmdGrpPi:
      Offset = OFFSET_CALC_CH (CH2CCC_CR_PICODE0_REG, CH0CCC_CR_PICODE0_REG, CccFub);
      break;

    case CtlGrpPi:
      // Select the CCC instance
      Offset = CccFub * CccSeparation;
      // Get the specific register in the CCC instance.
      if (Ddr4) {
        if (DtHalo) {
          Offset += ((Rank == 0) ? CH2CCC_CR_PICODE3_REG : ((Rank == 2) ? CH2CCC_CR_PICODE2_REG : CH2CCC_CR_PICODE1_REG));
        } else {
          Offset += CH2CCC_CR_PICODE1_REG;
        }
      } else if (Ddr5) {
        if (DtHalo) {
          if (Channel < 4) {
            Offset += ((Rank == 1) ? CH2CCC_CR_PICODE3_REG : CH2CCC_CR_PICODE1_REG);
          } else {
            Offset += CH2CCC_CR_PICODE1_REG;
          }
        } else {
          Offset += CH2CCC_CR_PICODE1_REG;
        }
      } else {
        // LP4 / LP5
        Offset += CH2CCC_CR_PICODE1_REG;
      }
      break;

    case CkeGrpPi:
      // Select the CCC instance
      Offset = CccFub * CccSeparation;
      // Get the specific register in the CCC instance.
      if (Ddr4) {
        Offset += ((Rank < 2) ? CH2CCC_CR_PICODE1_REG : ((Rank == 2) ? CH2CCC_CR_PICODE3_REG : CH2CCC_CR_PICODE2_REG));
      } else {
        // LP4; no CKE pin on DDR5/LP5
        Offset += CH2CCC_CR_PICODE3_REG;
      }
      break;

    case ClkGrpPi:
    case ClkGrpG4Pi:
      // Select the CCC instance
      Offset = CccFub * CccSeparation;
      // Get the specific register in the CCC instance.
      if (Ddr4 && DtHalo) {
        Offset += ((Rank % 2) ? CH2CCC_CR_PICODE3_REG : CH2CCC_CR_PICODE2_REG);
      } else { // LP4 / LP5 / DDR5 / UlxUlt-DDR4
        Offset += CH2CCC_CR_PICODE2_REG;
      }
      break;

    case WckGrpPi:
    case WckGrpPi90:
      // Select the CCC instance
      Offset = CccFub * CccSeparation;
      // Get the specific register in the CCC instance.
      Offset += CH2CCC_CR_PICODE3_REG;
      break;

    case CmdVref:
      if(MrcData->Inputs.UlxUlt) {
        Offset = DDRPHY_COMP_NEW_CR_DIMMVREF_VREFCH0_REG;
      } else if (Channel == 0) {
        Offset = DDRPHY_COMP_NEW_CR_DIMMVREF_VREFCH0_REG;
      } else {
        Offset = DDRPHY_COMP_NEW_CR_DIMMVREF_VREFCH1_REG;
      }
      break;

    case GsmIocLdoFFCodeLockCcc:
      Offset = OFFSET_CALC_CH (CH2CCC_CR_MDLLCTL2_REG, CH0CCC_CR_MDLLCTL2_REG, CccFub);
      break;

    case GsmIocDccFsmResetCcc:
      Offset = OFFSET_CALC_CH (CH2CCC_CR_DCCCTL9_REG, CH0CCC_CR_DCCCTL9_REG, CccFub);
      break;

    case GsmIocDcdRoCalEnCcc:
    case GsmIocDcdRoCalStartCcc:
    case GsmIocDcdRoCalSampleCountRstCcc:
    case GsmIocDccStartCcc:
    case GsmIocSrzDcdEnCcc:
    case GsmIocMdllDcdEnCcc:
      Offset = OFFSET_CALC_CH (CH2CCC_CR_DCCCTL20_REG, CH0CCC_CR_DCCCTL20_REG, CccFub);
      break;

    case CmdSCompOffset:
    case CmdRCompDrvDownOffset:
    case CmdRCompDrvUpOffset:
    case CtlSCompOffset:
    case CtlRCompDrvDownOffset:
    case CtlRCompDrvUpOffset:
      Offset = OFFSET_CALC_CH (CH2CCC_CR_DDRCRCTLCACOMPOFFSET_REG, CH0CCC_CR_DDRCRCTLCACOMPOFFSET_REG, CccFub);
      break;

    case CkeSCompOffset:
    case CkeRCompDrvDownOffset:
    case CkeRCompDrvUpOffset:
// @todo_adl      Offset = OFFSET_CALC_CH (CH0CCC_CR_DDRCRCTLCACOMPOFFSET_REG, CH1CCC_CR_DDRCRCTLCACOMPOFFSET_REG, Channel);
      break;

    case VsxHiClkFFOffset:
    case VsxHiCaFFOffset:
    case VsxHiCtlFFOffset:
    case ClkSCompOffset:
    case ClkRCompDrvDownOffset:
    case ClkRCompDrvUpOffset:
// @todo_adl      Offset = OFFSET_CALC_CH (CH0CCC_CR_DDRCRVSSHICLKCOMPOFFSET_REG, CH1CCC_CR_DDRCRVSSHICLKCOMPOFFSET_REG, Channel);
      break;

    case DefDrvEnLow:
    case CmdTxEq:
    case CtlTxEq:
      if (A0) {
        Offset = OFFSET_CALC_CH (CH2CCC_CR_DDRCRCCCCLKCONTROLS_A0_REG, CH0CCC_CR_DDRCRCCCCLKCONTROLS_A0_REG, CccFub);
      } else {
        Offset = OFFSET_CALC_CH (CH2CCC_CR_DDRCRCCCCLKCONTROLS_REG, CH0CCC_CR_DDRCRCCCCLKCONTROLS_REG, CccFub);
      }
      break;

    case WrDSCodeUpCmd:
    case WrDSCodeDnCmd:
    case SCompCodeCmd:
      Offset = DDRPHY_COMP_CR_DDRCRCMDCOMP_REG;
      break;

    case TcoCompCodeCmd:
    case TcoCompCodeCtl:
    case TcoCompCodeClk:
      // @todo_adl
      break;

    case SCompBypassCmd:
    case SCompBypassCtl:
    case SCompBypassClk:
      if (A0) {
        Offset = OFFSET_CALC_CH (CH2CCC_CR_DDRCRPINSUSED_REG, CH0CCC_CR_DDRCRPINSUSED_REG, CccFub);
      } else {
        Offset = OFFSET_CALC_CH (CH2CCC_CR_COMP3_REG, CH0CCC_CR_COMP3_REG, CccFub);
      }
      break;

    case WrDSCodeUpCtl:
    case WrDSCodeDnCtl:
    case SCompCodeCtl:
      Offset = DDRPHY_COMP_CR_DDRCRCTLCOMP_REG;
      break;

    case WrDSCodeUpClk:
    case WrDSCodeDnClk:
    case SCompCodeClk:
      Offset = DDRPHY_COMP_CR_DDRCRCLKCOMP_REG;
      break;

    case CmdDrvVrefUp:
    case CtlDrvVrefUp:
    case ClkDrvVrefUp:
      Offset = DDRPHY_COMP_CR_DDRCRCOMPCTL1_REG;
      break;

    case CmdDrvVrefDn:
    case CtlDrvVrefDn:
    case ClkDrvVrefDn:
      Offset = DDRPHY_COMP_CR_DDRCRCOMPCTL2_REG;
      break;

    case CmdSlewRate:
    case CmdScompPC:
    case CtlSlewRate:
    case CtlScompPC:
    case ClkSlewRate:
    case ClkScompPC:
// @todo_adl      Offset = DDRPHY_COMP_CR_DDRCRCOMPCTL1_REG;
      break;

    case CccPerBitDeskewCal:
// @todo_adl      Offset = OFFSET_CALC_CH (CH0CCC_CR_DDRCRCCCPERBITDESKEW0_REG, CH1CCC_CR_DDRCRCCCPERBITDESKEW0_REG, Channel);
      break;

    case CtlGrp0:
    case CtlGrp1:
    case CtlGrp2:
    case CtlGrp3:
    case CtlGrp4:
    case CmdGrp0:
    case CmdGrp1:
    case CmdGrp2:
    case CkAll:
    case CmdAll:
    case CtlAll:
    default:
      break;
  }

  switch (Group) {
    case CmdGrpPi:
    case CtlGrpPi:
    case CkeGrpPi:
    case ClkGrpPi:
    case ClkGrpG4Pi:
    case WckGrpPi:
    case WckGrpPi90:
      if (A0) {
        Offset -= (CH0CCC_CR_PICODE0_REG - CH0CCC_CR_PICODE0_A0_REG);
      }
      break;

    default:
      break;
  }

  return Offset;
}

/**
  This function handles getting the register offset for the requested parameter.
  It will determine multicast by the parameter exceeding the MAX of the
  Socket/Channel/Rank/Strobe/Bit.

  @param[in]  MrcData     - Pointer to global data.
  @param[in]  Group       - DDRIO group to access.
  @param[in]  Socket      - Processor socket in the system (0-based).  Not used in Core MRC.
  @param[in]  Controller  - Memory controller in the processor socket (0-based).
  @param[in]  Channel     - DDR Channel Number within the memory controller (0-based).
  @param[in]  Rank        - Rank number within a channel (0-based).
  @param[in]  Strobe      - Dqs data group within the rank (0-based).
  @param[in]  Bit         - Bit index within the data group (0-based).
  @param[in]  FreqIndex   - Index supporting multiple operating frequencies. (Not used in Client CPU's)

  @retval CR Offset
**/
UINT32
MrcGetDdrIoRegOffset (
  IN  MrcParameters *const  MrcData,
  IN  GSM_GT  const         Group,
  IN  UINT32  const         Socket,
  IN  UINT32  const         Controller,
  IN  UINT32  const         Channel,
  IN  UINT32  const         Rank,
  IN  UINT32  const         Strobe,
  IN  UINT32  const         Bit,
  IN  UINT32  const         FreqIndex
  )
{
  UINT32 Offset = MRC_UINT32_MAX;

  switch (Group) {
    case PanicVttDnLp:
    case VttGenStatusSelCount:
    case VttGenStatusCount:
    case RxVrefVttDecap:
    case RxVrefVddqDecap:
    case ForceDfeDisable:
    case DqsOdtCompOffset:
    case DqOdtVrefUp:
    case DqOdtVrefDn:
    case CompRcompOdtUp:
    case CompRcompOdtDn:
    case RecEnDelay:
    case RxDqsPDelay:
    case RxDqsNDelay:
    case CompRcompOdtUpPerStrobe:
    case CompRcompOdtDnPerStrobe:
    case RxVref:
    case RxTap1:
    case RxTap2:
    case RxTap3:
    case RxTap4:
    case RxTap1En:
    case RxTap2En:
    case RxTap3En:
    case RxC:
    case RxR:
    case RxEq:
    case RxDqsEq:
    case RxDqsBitDelay:
    case RxDqsBitOffset:
    case RxDqsNBitDelay:
    case RxDqsPBitDelay:
    case RoundTripDelay:
    case RxFlybyDelay:
    case RxIoTclDelay:
    case RxFifoRdEnFlybyDelay:
    case RxFifoRdEnTclDelay:
    case RxDqDataValidDclkDelay:
    case RxDqDataValidQclkDelay:
    case SenseAmpDelay:
    case SenseAmpDuration:
    case McOdtDelay:
    case McOdtDuration:
    case DqsOdtDelay:
    case DqsOdtDuration:
    case RxBiasCtl:
    case RxLpddrMode:
    case RxBiasVrefSel:
    case RxBiasTailCtl:
    case RxDqsAmpOffset:
    case RxDqsUnmatchedAmpOffset:
    case VccDllRxD0PiCb:
    case VccDllSDllPiCb:
    case DataRxD0PiCb:
    case DataSDllPiCb:
    case RxCben:
    case RloadDqsDn:
    case RxVoc:
    case RxVocUnmatched:
    case RxRankMuxDelay:
    case RxPerBitDeskewCal:
      Offset = GetDdrIoDataReadOffsets (MrcData, Group, Socket, Controller, Channel, Rank, Strobe, Bit, FreqIndex);
      break;

    case DqScompPC:
    case TxSlewRate:
    case TxTco:
    case TxDqsTcoP:
    case TxDqsTcoN:
    case TxDqsBitDelay:
    case DqDrvVrefUp:
    case DqDrvVrefDn:
    case SCompCodeDq:
    case SCompBypassDq:
    case TxRonUp:
    case TxRonDn:
    case TxDqsDelay:
    case TxDqsDelay90:
    case TxDqDelay:
    case TxDqDelay90:
    case TxEq:
    case TxEqCoeff0:
    case TxEqCoeff1:
    case TxEqCoeff2:
    case TxRankMuxDelay:
    case TxDqsRankMuxDelay:
    case TxDqBitDelay:
    case TxDqBitDelay90:
    case TxDqDccOffset:
    case TxDqsDccOffset:
    case RxRptChDqClkOn:
    case TxRptChDqClkOn:
    case TxDqFifoWrEnTcwlDelay:
    case TxDqFifoRdEnTcwlDelay:
    case TxDqFifoRdEnFlybyDelay:
    case TxPerBitDeskewCal:
    case RxUnmatchedOffNcal:
    case RxMatchedUnmatchedOffPcal:
      Offset = GetDdrIoDataWriteOffsets (MrcData, Group, Socket, Channel, Rank, Strobe, Bit, FreqIndex);
      break;

    case RecEnOffset:
    case RxDqsOffset:
    case RxVrefOffset:
    case TxDqsOffset:
    case TxDqOffset:
    case CompOffsetVssHiFF:
    case RxDqsPiUiOffset:
      Offset = GetDdrIoDataTrainOffsets (MrcData, Group, Socket, Channel, Rank, Strobe, Bit, FreqIndex);
      break;

    case SCompCodeCmd:
    case TcoCompCodeCmd:
    case SCompBypassCmd:
    case SCompBypassCtl:
    case SCompBypassClk:
    case SCompCodeCtl:
    case TcoCompCodeCtl:
    case SCompCodeClk:
    case TcoCompCodeClk:
    case CmdDrvVrefUp:
    case CmdDrvVrefDn:
    case CtlDrvVrefUp:
    case CtlDrvVrefDn:
    case ClkDrvVrefUp:
    case ClkDrvVrefDn:
    case CmdSlewRate:
    case CmdScompPC:
    case CtlSlewRate:
    case CtlScompPC:
    case ClkSlewRate:
    case ClkScompPC:
    case CtlGrpPi:
    case CkeGrpPi:
    case CmdGrpPi:
    case ClkGrpPi:
    case ClkGrpG4Pi:
    case WckGrpPi:
    case WckGrpPi90:
    case CmdVref:
    case CmdRCompDrvDownOffset:
    case CmdRCompDrvUpOffset:
    case CmdSCompOffset:
    case CtlRCompDrvDownOffset:
    case CtlRCompDrvUpOffset:
    case CtlSCompOffset:
    case ClkRCompDrvDownOffset:
    case ClkRCompDrvUpOffset:
    case ClkSCompOffset:
//    case ClkCompOnTheFlyUpdtEn:
    case CkeRCompDrvDownOffset:
    case CkeRCompDrvUpOffset:
    case CkeSCompOffset:
    case DefDrvEnLow:
    case CmdTxEq:
    case CtlTxEq:
    case VsxHiClkFFOffset:
    case VsxHiCaFFOffset:
    case VsxHiCtlFFOffset:
    case WrDSCodeUpCmd:
    case WrDSCodeDnCmd:
    case WrDSCodeUpCtl:
    case WrDSCodeDnCtl:
    case WrDSCodeUpClk:
    case WrDSCodeDnClk:
    case CccPerBitDeskewCal:
      Offset = GetDdrIoCommandOffsets (MrcData, Group, Socket, Channel, Rank, Strobe, Bit, FreqIndex);
      break;

    default:
      break;
  }

  return Offset;
}

// These functions are called in DdrioInit()

/**
  This function returns the offset to access specific Channel/Strobe of CbtTune0.

  @params[in]  Channel - 0-based index of Channel to access.
  @params[in]  Strobe  - 0-based index of Strobe to access.

  @retval CR Offset
**/

UINT32
CBTune0Offset (
  IN  UINT32  const   DllBlock
  )
{
  UINT32 Offset = 0xFFFFFFFF;
// @todo_adl    Offset = DLLDDRDATA0_CR_CBTUNE0_REG;
// @todo_adl    Offset += (DLLDDRDATA1_CR_CBTUNE0_REG - DLLDDRDATA0_CR_CBTUNE0_REG) * DllBlock;
  return Offset;
}

/**
  This function returns the offset to access specific Channel/Strobe of CbtTune1.

  @params[in]  Channel - 0-based index of Channel to access.
  @params[in]  Strobe  - 0-based index of Strobe to access.

  @retval CR Offset
**/

UINT32
CBTune1Offset (
  IN  UINT32  const   Channel,
  IN  UINT32  const   Strobe
  )
{
  UINT32 Offset = 0xFFFFFFFF;
  /* @todo_adl
  if (Channel >= MAX_CHANNEL) {
  // Overall Broadcast
    Offset = DLLDDR_CR_CBTUNE1_REG;
  } else {
    Offset = DLLDDRDATA0_CR_CBTUNE1_REG;
    Offset += (DLLDDRDATA1_CR_CBTUNE1_REG - DLLDDRDATA0_CR_CBTUNE1_REG) * Strobe;
  }*/
  return Offset;
}

/**
  This function returns the offset to access specific Channel/Strobe of WRRETRAINSWIZZLECONTROL.

  @params[in]  Channel - 0-based index of Channel to access.
  @params[in]  Strobe  - 0-based index of Strobe to access.

  @retval UINT32 - CR offset
**/
UINT32
WrRetrainSwizzleControlOffset (
  IN  UINT32  const Channel,
  IN  UINT32  const Strobe
  )
{
  UINT32 Offset;

  Offset = DATA0CH0_CR_DDRCRWRRETRAINSWIZZLECONTROL_REG +
         ((DATA0CH1_CR_DDRCRWRRETRAINSWIZZLECONTROL_REG - DATA0CH0_CR_DDRCRWRRETRAINSWIZZLECONTROL_REG) * Channel) +
         ((DATA1CH0_CR_DDRCRWRRETRAINSWIZZLECONTROL_REG - DATA0CH0_CR_DDRCRWRRETRAINSWIZZLECONTROL_REG) * Strobe);

  return Offset;
}

/**
  This function returns the offset to access specific Channel/Strobe of WRRETRAINRANK[0..3].

  @params[in]  Channel - 0-based index of Channel to access.
  @params[in]  Rank    - 0-based index of Rank to access.
  @params[in]  Strobe  - 0-based index of Strobe to access.

  @retval UINT32 - CR offset
**/
UINT32
WrRetrainRankOffset (
  IN  UINT32  const Channel,
  IN  UINT32  const Rank,
  IN  UINT32  const Strobe
  )
{
  UINT32 Offset;

  Offset = DATA0CH0_CR_DDRCRWRRETRAINRANK3_REG +
         ((DATA0CH1_CR_DDRCRWRRETRAINRANK3_REG - DATA0CH0_CR_DDRCRWRRETRAINRANK3_REG) * Channel) +
         ((DATA0CH0_CR_DDRCRWRRETRAINRANK0_REG - DATA0CH0_CR_DDRCRWRRETRAINRANK1_REG) * (3 - Rank)) +
         ((DATA1CH0_CR_DDRCRWRRETRAINRANK0_REG - DATA0CH0_CR_DDRCRWRRETRAINRANK0_REG) * Strobe);

  return Offset;
}

/**
  This function returns the offset to access specific Channel/Strobe of WrRetrainControlStatus.

  @params[in]  Channel - 0-based index of Channel to access.
  @params[in]  Strobe  - 0-based index of Strobe to access.

  @retval CR Offset
**/
UINT32
WrRetrainControlStatusOffset (
  IN  UINT32  const   Channel,
  IN  UINT32  const   Strobe
  )
{
  UINT32 Offset;

  Offset = DATA0CH0_CR_DDRCRWRRETRAINCONTROLSTATUS_REG +
         ((DATA0CH1_CR_DDRCRWRRETRAINCONTROLSTATUS_REG - DATA0CH0_CR_DDRCRWRRETRAINCONTROLSTATUS_REG) * Channel) +
         ((DATA1CH0_CR_DDRCRWRRETRAINCONTROLSTATUS_REG - DATA0CH0_CR_DDRCRWRRETRAINCONTROLSTATUS_REG) * Strobe);

  return Offset;
}

/**
  This function returns the offset to access specific Channel/Strobe of MarginModeControl.

  @params[in]  Channel - 0-based index of Channel to access.
  @params[in]  Strobe  - 0-based index of Strobe to access.

  @retval CR Offset
**/

UINT32
MarginModeControlOffset (
  IN  UINT32  const   Channel,
  IN  UINT32  const   Strobe
  )
{
  UINT32 Offset;

  if (Channel >= MAX_CHANNEL) {
    // Overall Broadcast
    Offset = DATA_CR_DDRCRMARGINMODECONTROL0_REG;
  } else {
    Offset =   DATA0CH0_CR_DDRCRMARGINMODECONTROL0_REG;
    Offset += (DATA0CH1_CR_DDRCRMARGINMODECONTROL0_REG - DATA0CH0_CR_DDRCRMARGINMODECONTROL0_REG) * Channel +
              (DATA1CH0_CR_DDRCRMARGINMODECONTROL0_REG - DATA0CH0_CR_DDRCRMARGINMODECONTROL0_REG) * Strobe;
  }
  return Offset;
}

/**
  This function returns the offset to access specific VccDllComp register.
  There are 12 VccDll partitions, 8 for data and 4 for CCC

  @params[in]  Channel - 0-based index of Channel to access.
  @params[in]  Strobe  - 0-based index of Strobe to access.

  @retval CR Offset
**/
UINT32
VccDllDataCCCCompOffset (
  IN  UINT32  const   Channel,
  IN  UINT32  const   Strobe
  )
{
  UINT32 Offset = 0xFFFFFFFF;
  /* @todo_adl
  if (Channel != MRC_IGNORE_ARG) {
    Offset = OFFSET_CALC_CH (DLLDDRCCC0_CR_DDRCRVCCDLLCOMPOFFSET_REG, DLLDDRCCC1_CR_DDRCRVCCDLLCOMPOFFSET_REG, Channel);
  } else {
    Offset = DLLDDRDATA0_CR_DDRCRVCCDLLCOMPOFFSET_REG;
    Offset += (DLLDDRDATA1_CR_DDRCRVCCDLLCOMPOFFSET_REG - DLLDDRDATA0_CR_DDRCRVCCDLLCOMPOFFSET_REG) * Strobe;
  }*/
  return Offset;
}

/**
  This function returns the offset to access specific VccDllCouplingCap registers.
  There are 12 VccDll partitions, 8 for data and 4 for CCC

  @params[in]  Channel - 0-based index of Channel to access.
  @params[in]  Strobe  - 0-based index of Strobe to access.

  @retval CR Offset
**/
UINT32
CouplingCapOffset (
  IN  UINT32  const   Channel,
  IN  UINT32  const   Strobe
  )
{
  UINT32 Offset = 0xFFFFFFFF;
  /* @todo_adl
  if (Channel != MRC_IGNORE_ARG) {
    Offset = OFFSET_CALC_CH (DLLDDRCCC0_CR_DDRCRVCCDLLCOUPLINGCAP_REG, DLLDDRCCC1_CR_DDRCRVCCDLLCOUPLINGCAP_REG, Channel);
  } else {
    Offset = DLLDDRDATA0_CR_DDRCRVCCDLLCOUPLINGCAP_REG;
    Offset += (DLLDDRDATA1_CR_DDRCRVCCDLLCOUPLINGCAP_REG - DLLDDRDATA0_CR_DDRCRVCCDLLCOUPLINGCAP_REG) * Strobe;
  }*/
  return Offset;
}

/**
  This function returns the offset to access specific VccDllVssHiFF (VsxHi is used interchangeably for VssHi) registers.
  There are 12 VccDll partitions, 8 for data and 4 for CCC. Since VccDll injects alot of current to VssHi rail therefore
  MRC needs to run VssHi compensation for feedforward legs.

  @params[in]  Channel - 0-based index of Channel to access.
  @params[in]  Strobe  - 0-based index of Strobe to access.

  @retval CR Offset
**/
UINT32
VssHiFFOffset (
  IN  UINT32  const   Channel,
  IN  UINT32  const   Strobe
  )
{
  UINT32 Offset = 0xFFFFFFFF;
  /* @todo_adl
  if (Channel != MRC_IGNORE_ARG) {
    Offset = OFFSET_CALC_CH (DLLDDRCCC0_CR_DDRCRVCCDLLVSXHIFF_REG, DLLDDRCCC1_CR_DDRCRVCCDLLVSXHIFF_REG, Channel);
  } else {
    Offset = DLLDDRDATA0_CR_DDRCRVCCDLLVSXHIFF_REG;
    Offset += (DLLDDRDATA1_CR_DDRCRVCCDLLVSXHIFF_REG - DLLDDRDATA0_CR_DDRCRVCCDLLVSXHIFF_REG) * Strobe;
  }*/
  return Offset;
}

/**
  This function returns the offset to access specific Channel/Strobe of DataControl1.

  @params[in]  Channel - 0-based index of Channel to access.
  @params[in]  Strobe  - 0-based index of Strobe to access.

  @retval UINT32 - CR offset
**/
UINT32
DataDqsTxRxOffset (
  IN  UINT32  const   Channel,
  IN  UINT32  const   Strobe
  )
{
  UINT32 Offset;

  Offset = DATA0CH0_CR_DQSTXRXCTL_REG;
  Offset += (DATA0CH1_CR_DQSTXRXCTL_REG - DATA0CH0_CR_DQSTXRXCTL_REG) * Channel +
            (DATA1CH0_CR_DQSTXRXCTL_REG - DATA0CH0_CR_DQSTXRXCTL_REG) * Strobe;

  return Offset;
}

/**
  This function returns the offset to access specific Channel/Strobe of DataSdlCtl0.

  @params[in]  Channel - 0-based index of Channel to access.
  @params[in]  Strobe  - 0-based index of Strobe to access.

  @retval UINT32 - CR offset
**/
UINT32
DataSdlCtlOffset (
  IN  UINT32  const   Channel,
  IN  UINT32  const   Strobe
  )
{
  UINT32 Offset;

  Offset = DATA0CH0_CR_SDLCTL0_REG;
  Offset += (DATA0CH1_CR_SDLCTL0_REG - DATA0CH0_CR_SDLCTL0_REG) * Channel +
            (DATA1CH0_CR_SDLCTL0_REG - DATA0CH0_CR_SDLCTL0_REG) * Strobe;

  return Offset;
}

/**
  Return the offset to access specific Channel/Strobe of DataOffsetComp.

  @param[in]   MrcData - The MRC global data.
  @param[in]   Channel - 0-based index of Channel to access. Overall broadcast is triggered on the values starting and above MAX_CHANNEL
  @param[in]   Strobe  - 0-based index of Strobe to access.

  @retval Offset - CR offset
**/
UINT32
MrcGetOffsetDataOffsetComp (
  IN MrcParameters * const MrcData,
  IN const UINT32          Channel,
  IN const UINT32          Strobe
  )
{
  UINT32  Offset;

  if (Channel >= MAX_CHANNEL) {
    // Overall Broadcast
    Offset = DATA_CR_DDRCRDATAOFFSETCOMP_REG;
  } else {
    // Specific Channel and Byte
    Offset = DATA0CH0_CR_DDRCRDATAOFFSETCOMP_REG +
           ((DATA0CH1_CR_DDRCRDATAOFFSETCOMP_REG - DATA0CH0_CR_DDRCRDATAOFFSETCOMP_REG) * Channel) +
           ((DATA1CH0_CR_DDRCRDATAOFFSETCOMP_REG - DATA0CH0_CR_DDRCRDATAOFFSETCOMP_REG) * Strobe);
  }

  return Offset;
}
