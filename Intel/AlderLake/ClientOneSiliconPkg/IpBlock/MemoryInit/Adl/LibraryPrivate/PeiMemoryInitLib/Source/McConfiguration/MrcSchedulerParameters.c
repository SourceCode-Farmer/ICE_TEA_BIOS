/** @file
  This module configures the memory controller scheduler.

@copyright
  INTEL CONFIDENTIAL
  Copyright 1999 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains an 'Intel Peripheral Driver' and is uniquely identified as
  "Intel Reference Module" and is licensed for Intel CPUs and chipsets under
  the terms of your license agreement with Intel or your vendor. This file may
  be modified by the user, subject to additional terms of the license agreement.

@par Specification Reference:
**/

// Include files
#include "MrcSchedulerParameters.h"
#include "MrcChipApi.h"

/// Defines
#define MRC_SC_PH_THROT_CR_COUNT  (4)

/// Enumerations
typedef enum {
  MrcSptLoadSameRank,
  MrcSptUnloadedSameRank,
  MrcSptLoadedDiffRank,
  MrcSptUnloadedDiffRank,
  MrcSptMax
} MRC_SC_PH_THROT_TYPE;

/// Static data
const static UINT8 ScPhThrottlingPor[MRC_SC_PH_THROT_CR_COUNT][MrcSptMax] = {
  {0x10, 0x8, 0x18, 0xC}, // SC_PH_THROTTLING_0
  {0x8,  0x4, 0x10, 0x8}, // SC_PH_THROTTLING_1
  {0x6,  0x2, 0xC,  0x4}, // SC_PH_THROTTLING_2
  {0,    0,   0,    0  }  // SC_PH_THROTTLING_3
};

/**
  This function configures the memory controller MCx_CHx_CR_SPID_LOW_POWER_CTL register.

  @param[in] MrcData    - Include all MRC global data.
**/
void
ConfigSpidLowPowerCtl (
  IN MrcParameters *const MrcData
  )
{
  MrcOutput  *Outputs;
  UINT32     Channel;
  UINT32     Controller;
  UINT32     Offset;
  UINT32     LpMode;
  MC0_CH0_CR_SPID_LOW_POWER_CTL_STRUCT SpidLowPowerCtl;

  Outputs = &MrcData->Outputs;
  LpMode  = MrcData->Inputs.LpMode;

  for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
    for (Channel = 0; Channel < MAX_CHANNEL_SHARE_REGS; Channel++) {
      Offset = OFFSET_CALC_MC_CH (MC0_CH0_CR_SPID_LOW_POWER_CTL_REG, MC1_CH0_CR_SPID_LOW_POWER_CTL_REG, Controller, MC0_CH1_CR_SPID_LOW_POWER_CTL_REG, Channel);
      SpidLowPowerCtl.Data = MrcReadCR (MrcData, Offset);
      if (LpMode == 1) { // 1 = Enabled
        SpidLowPowerCtl.Bits.selfrefresh_enable   = 0;
        SpidLowPowerCtl.Bits.powerdown_enable     = 1;
        SpidLowPowerCtl.Bits.ckevalid_enable      = 1;
        SpidLowPowerCtl.Bits.powerdown_latency    = 7;
      }

      if ((Outputs->DdrType == MRC_DDR_TYPE_DDR4) && SpidLowPowerCtl.Bits.selfrefresh_enable) {
        SpidLowPowerCtl.Bits.raise_cke_after_exit_latency = 1;
      }

      MrcWriteCR (MrcData, Offset, SpidLowPowerCtl.Data);
    }
  }
}

/**
  This function configures the memory controller scheduler.

  @param[in] MrcData - Include all MRC global data.

  @retval Nothing.
**/
void
MrcSchedulerParametersConfig (
  IN MrcParameters *const MrcData
  )
{
  const MrcInput  *Inputs;
  MrcOutput       *Outputs;
  MrcDebug        *Debug;
  MrcIntOutput    *MrcIntData;
  MrcChannelOut   *ChannelOut;
  MrcDimmOut      *DimmOut;
  MrcTiming       *Timing;
  MrcDdrType      DdrType;
  INT64           GetSetVal;
  INT64           Nto1Ratio;
  INT64           FreqPoint;
  INT64           CmdStretch;
  INT64           DramTechnology;
  INT64           tCKCKEH;
  INT64           tCSCKEH;
  INT64           CmdTristateDis;
  INT64           tCPDED;
  INT64           tCKE;
  INT64           tSR;
  UINT32          Offset;
  UINT32          Channel;
  UINT32          IpChannel;
  UINT32          Controller;
  UINT32          tCL;
  UINT32          tCWL;
  UINT32          BurstLength;
  UINT32          NMode;
  UINT32          Data32;
  UINT32          tCSLCK;
  UINT32          tWckStop;
  UINT32          DelayPmAckCycles;
  UINT32          FirstChannel;
  UINT32          tCKToDelayPmAckCycles;
  UINT32          tSRToDelayPmAckCycles;
  UINT32          tWCKOFF;
  UINT8           CmdMirror;
  UINT8           AddressMirror;
  UINT8           X8Device;
  UINT8           Dimm;
  UINT8           GearRatio;
  UINT8           Gear2;
  BOOLEAN         Gear4;
  UINT8           Index;
  const UINT8     *ThrottlingPor;
  BOOLEAN         Lpddr4;
  BOOLEAN         Lpddr5;
  BOOLEAN         Lpddr;
  BOOLEAN         Ddr4;
  BOOLEAN         Ddr5;
  BOOLEAN         EnableMcOdtCtrl;
  BOOLEAN         SafeMode;
  CHAR8           *RefreshTypeStr;
  MC0_CH0_CR_SC_PBR_STRUCT                ScPbr;
  MC0_MCDECS_CBIT_STRUCT                  McdecsCbit;
  MC0_CH0_CR_SCHED_CBIT_STRUCT            SchedCbit;
  MC0_CH0_CR_SCHED_SECOND_CBIT_STRUCT     SchedSecondCbit;
  MC0_CH0_CR_SCHED_THIRD_CBIT_STRUCT      SchedThirdCbit;
  MC0_CH0_CR_SC_WDBWM_STRUCT              ScWdbWm;
  MC0_SC_QOS_STRUCT                       ScQos;
  MC0_SC_QOS2_STRUCT                      ScQos2;
  MC0_CH0_CR_SC_PCIT_STRUCT               ScPcit;
  MC0_CH0_CR_SC_ADAPTIVE_PCIT_STRUCT      ScAdaptivePcit;
  MC0_CH0_CR_WCK_CONFIG_STRUCT            WckConfig;
  MC0_MCDECS_SECOND_CBIT_STRUCT           McdecsSecondCbit;
  MC0_CH0_CR_SC_BLOCKING_RULES_CFG_STRUCT ScBlockingRulesCfg;
  MC0_CH0_CR_SC_PH_THROTTLING_0_STRUCT    ScPhThrottling;
  MC0_CH0_CR_SC_PR_CNT_CONFIG_STRUCT      ScPrCntCfg;
  MC0_CH0_CR_SC_WPQ_THRESHOLD_STRUCT      ScWpqThreshold;
  MC0_CH0_CR_WMM_READ_CONFIG_STRUCT       WmmReadConfig;
  MC0_CH0_CR_XARB_CFG_BUBBLE_INJ_STRUCT   XarbBubbleInjCfg;
  MC0_CH0_CR_PM_PDWN_CONFIG_STRUCT        PmPdwnConfig;
  MC0_CH0_CR_MCMNTS_SPARE_STRUCT          McmntsSpare;
  MC0_CH0_CR_DEADLOCK_BREAKER_STRUCT      DeadlockBreaker;
  MC0_PM_CONTROL_STRUCT                   PmControl;
  MC0_CH0_CR_MCMNTS_GLOBAL_DRIVER_GATE_CFG_STRUCT McMntsGlobalDriverGateCfg;

  MrcIntData    = ((MrcIntOutput *) (MrcData->IntOutputs.Internal));
  Inputs        = &MrcData->Inputs;
  Outputs       = &MrcData->Outputs;
  Debug         = &Outputs->Debug;
  DdrType       = Outputs->DdrType;
  AddressMirror = 0;
  X8Device      = 0;
  Lpddr4        = (DdrType == MRC_DDR_TYPE_LPDDR4);
  Lpddr5        = (DdrType == MRC_DDR_TYPE_LPDDR5);
  Ddr4          = (DdrType == MRC_DDR_TYPE_DDR4);
  Ddr5          = (DdrType == MRC_DDR_TYPE_DDR5);
  Lpddr         = Outputs->Lpddr;
  SafeMode      = (Inputs->SafeMode == TRUE);
  Gear2         = (Outputs->Gear2) ? 1 : 0; // Don't assume TRUE == 1 FALSE == 0.
  Gear4         = Outputs->Gear4;
  NMode         = MrcGetNMode(MrcData);
  CmdMirror     = Inputs->CmdMirror;
  GearRatio     = Gear4 ? 4 : ((Gear2 == 1) ? 2 : 1);
  FirstChannel  = 0;

  for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
    if (!(MrcControllerExist (MrcData, Controller))) {
      continue;
    }
    Offset = OFFSET_CALC_CH (MC0_MCDECS_CBIT_REG, MC1_MCDECS_CBIT_REG, Controller);
    McdecsCbit.Data = MrcReadCR (MrcData, Offset);
    McdecsCbit.Bits.dis_single_ch_sr        = 0;
    McdecsCbit.Bits.dis_other_ch_stolen_ref = 0;
    McdecsCbit.Bits.Dis_EFLOW_fwd_data_hack = 1;
    MrcWriteCR (MrcData, Offset, McdecsCbit.Data);

    Offset = OFFSET_CALC_CH (MC0_BC_CR_SCHED_CBIT_REG, MC1_BC_CR_SCHED_CBIT_REG, Controller);
    SchedCbit.Data = 0;
    SchedCbit.Bits.dis_pt_it = Inputs->DisPgCloseIdleTimeout;

    SchedCbit.Bits.dis_deprd_opt = 1;

    // ZQ Command Serialize Options:
    //   DDR5 do not serialize ZQ commands.
    //   DDR4 do not serialize ZQ commands.
    //   LPDDR4 is always serialized regardless of layout.
    SchedCbit.Bits.serialize_zq = (Lpddr5) ? 0 : (Lpddr4 && Outputs->LpByteMode) ? 1 : Inputs->SharedZqPin;

    // Determine if we are enabling MC control over DRAM ODT pin.  LPDDR4 has no ODT pin,
    // so we do not enable MC control of the ODT pin for this technology.
    if (!Lpddr && Outputs->DramDqOdtEn) {
      EnableMcOdtCtrl = 1;
    } else {
      EnableMcOdtCtrl = 0;
    }
    // Field is a disable.  So use negative logic on the Enable variable.
    SchedCbit.Bits.dis_odt = !EnableMcOdtCtrl;
    SchedCbit.Bits.dis_clk_gate = (SafeMode ? 1 : 0);
    MrcWriteCrMulticast (MrcData, Offset, SchedCbit.Data);
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "SCHED_CBIT = %08Xh\n", SchedCbit.Data);

    Offset = OFFSET_CALC_CH (MC0_BC_CR_SC_WDBWM_REG, MC1_BC_CR_SC_WDBWM_REG, Controller);
    ScWdbWm.Data = 0;
    ScWdbWm.Bits.WMM_entry_wm = 0x38;
    ScWdbWm.Bits.WMM_exit_wm = 0x28;
    ScWdbWm.Bits.WIM_wm = 0x3b;
    ScWdbWm.Bits.Write_Isoc_CAS_count = 0x10;
    ScWdbWm.Bits.Read_CAS_count   = 0x30;
    ScWdbWm.Bits.Write_CAS_count  = 0x40;
    ScWdbWm.Bits.Write_CAS_count_for_VC1  = 0x40;
    ScWdbWm.Bits.Write_threshold_for_lp_read_bklr = 0x16;
    MrcWriteCR64 (MrcData, Offset, ScWdbWm.Data);

    Offset = OFFSET_CALC_CH (MC0_SC_QOS_REG, MC1_SC_QOS_REG, Controller);
    ScQos.Data = 0;
    ScQos.Bits.Isoch_time_window = 0x384;
    ScQos.Bits.Write_starvation_window = 0x5A;
    ScQos.Bits.Read_starvation_window = 0x5A;
    ScQos.Bits.VC0_read_count = 0x16;
    ScQos.Bits.VC1_Read_starvation_en = 1;
    ScQos.Bits.Write_starvation_in_Isoc_en = 1;
    ScQos.Bits.Read_starvation_in_Isoch_en = 1;
    ScQos.Bits.VC0_counter_disable = 0;
    ScQos.Bits.Force_MCVC1Demote = 0;
    ScQos.Bits.MC_Ignore_VC1Demote = 0;
    ScQos.Bits.allow_cross_vc_blocking = 1;
    MrcWriteCR64 (MrcData, Offset, ScQos.Data);

    Offset = OFFSET_CALC_CH (MC0_SC_QOS2_REG, MC1_SC_QOS2_REG, Controller);
    ScQos2.Data = MrcReadCR64 (MrcData, Offset);
    ScQos2.Bits.Isoc_during_demote_window = 0;
    ScQos2.Bits.Isoc_during_demote_period_x8 = 0;
    MrcWriteCR64 (MrcData, Offset, ScQos2.Data);

    Offset = OFFSET_CALC_CH (MC0_BC_CR_SC_PCIT_REG, MC1_BC_CR_SC_PCIT_REG, Controller);
    ScPcit.Data = 0;
    ScPcit.Bits.PCIT_SUBCH0 = 0x40;
    ScPcit.Bits.PCIT_SUBCH1 = 0x40;
    ScPcit.Bits.PCIT_GT     = 0x60;
    MrcWriteCrMulticast (MrcData, Offset, ScPcit.Data);
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "SC_PCIT = %08Xh\n", ScPcit.Data);

    FirstChannel = Outputs->Controller[Controller].FirstPopCh;
    MrcGetSetMcCh (MrcData, Controller, FirstChannel, GsmMctCKE, ReadFromCache, &tCKE);
    MrcGetSetMcCh (MrcData, Controller, FirstChannel, GsmMctSR, ReadFromCache, &tSR);

    tCKToDelayPmAckCycles = ((UINT32)tCKE * (Lpddr ? GearRatio : 1)) + 10 + (10 * (Lpddr ? 1 : 0));
    tSRToDelayPmAckCycles = UDIVIDEROUND ((UINT32)tSR, GearRatio);
    DelayPmAckCycles      = MAX (tCKToDelayPmAckCycles, tSRToDelayPmAckCycles);

    Offset = OFFSET_CALC_CH (MC0_PM_CONTROL_REG, MC1_PM_CONTROL_REG, Controller);
    PmControl.Data = MrcReadCR (MrcData, Offset);
    if (DelayPmAckCycles > PmControl.Bits.delay_PM_Ack_cycles) {
      PmControl.Bits.delay_PM_Ack_cycles = DelayPmAckCycles;
      MrcWriteCR (MrcData, Offset, PmControl.Data);
    }

    for (Channel = 0; Channel < MAX_CHANNEL; Channel++) {
      if ((!MrcChannelExist (MrcData, Controller, Channel)) || IS_MC_SUB_CH (Lpddr, Channel)) {
        // For LPDDR4/5, only program register on even channels.
        continue;
      }
      ChannelOut = &Outputs->Controller[Controller].Channel[Channel];
      Timing = &ChannelOut->Timing[Inputs->MemoryProfile];
      IpChannel = LP_IP_CH (Lpddr, Channel);

      // Disable APD/PPD - it could have been enabled during previous SAGV iteration
      Offset = OFFSET_CALC_MC_CH (MC0_CH0_CR_PM_PDWN_CONFIG_REG, MC1_CH0_CR_PM_PDWN_CONFIG_REG, Controller, MC0_CH1_CR_PM_PDWN_CONFIG_REG, IpChannel);
      PmPdwnConfig.Data = MrcReadCR64 (MrcData, Offset);
      PmPdwnConfig.Bits.APD = 0;
      PmPdwnConfig.Bits.PPD = 0;
      MrcWriteCR64 (MrcData, Offset, PmPdwnConfig.Data);

      Offset = OFFSET_CALC_MC_CH (MC0_CH0_CR_SCHED_SECOND_CBIT_REG, MC1_CH0_CR_SCHED_SECOND_CBIT_REG, Controller, MC0_CH1_CR_SCHED_SECOND_CBIT_REG, IpChannel);
      SchedSecondCbit.Data = MrcReadCR (MrcData, Offset);
      SchedSecondCbit.Bits.DisWrActThrottleOnAnyRead       = 1;
      SchedSecondCbit.Bits.disable_wr_on_SAGV_exit_for_DCC = 1;
      SchedSecondCbit.Bits.dis_ignore_1st_trefi            = 1;
      SchedSecondCbit.Bits.enable_fdata_nak                = 1;
      if (Lpddr5) {
        SchedSecondCbit.Bits.disable_cas_clubbing          = 1;
      }
      MrcWriteCR (MrcData, Offset, SchedSecondCbit.Data);
      MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "MC%u C%u SCHED_SECOND_CBIT = %08Xh\n", Controller, Channel, SchedSecondCbit.Data);

      // Set the Command N mode and Command Rate Limit
      // Command stretch:
      //   00 - 1N
      //   01 - 2N
      //   10 - 3N
      //   11 - N:1
      // DDR4 Gear2: When command stretch is set to 1N or 2N, MC will generate a command "like" 2N and 4N respectively
      Nto1Ratio = SafeMode ? 1 : 3;    // This was called Command Rate Limit before
      if (Ddr5) {
        if (NMode == 1) {
          CmdStretch = 0;                          // 1N
        } else {
          CmdStretch = 1;                          // 2N
        }
      } else if (Lpddr) {
        CmdStretch = 0;                            // LP4 and LP5: 1N
      } else {
        CmdStretch = (Outputs->Gear2) ? 1 : 2;     // DDR4: 3N Mode for Gear1, 2N Mode for Gear2
      }

      DramTechnology = DdrType;  // Matches MRC_DDR_TYPE_xxx encoding
      AddressMirror = 0;
      X8Device      = 0;
      for (Dimm = 0; Dimm < MAX_DIMMS_IN_CHANNEL; Dimm++) {
        if (MrcRankExist (MrcData, Controller, Channel, Dimm * MAX_RANK_IN_DIMM)) {
          DimmOut = &ChannelOut->Dimm[Dimm];
          if (DimmOut->AddressMirrored) {
            AddressMirror |= (1 << Dimm);
          }
          if ((DimmOut->SdramWidth == 8) && (Ddr4)) {
            X8Device |= (1 << Dimm);
          }
        }
      }

      if (Lpddr && CmdMirror) {
        Offset = (CmdMirror >> ((Controller * MAX_CHANNEL) + Channel)) & 0x3;
        AddressMirror = (UINT8) Offset | ((UINT8) Offset << 2); //This is to make sure all ranks on the channels match
      }

      // Use max values to be safe, because we don't know yet the final delta between CS/CK and CKE in DDRIO.
      tCKCKEH = 7;
      tCSCKEH = 7;

      // @todo: Move this to MrcMemoryApi: MrcGetCpded()
      switch (DdrType) {
        case MRC_DDR_TYPE_LPDDR4:
        case MRC_DDR_TYPE_LPDDR5:
          // tCPDED is unused in CNL, MC is using tCKE for this parameter. Use default value (1)
          tCPDED = 1;
          break;

        case MRC_DDR_TYPE_DDR4:
          tCPDED = 4;
          Offset = DIVIDECEIL (10000000, Timing->tCK); //femtoseconds
          tCKCKEH = MAX ((UINT32) tCKCKEH, Offset);
          break;

        case MRC_DDR_TYPE_DDR5:
          // DDR5 tCPDED = MAX (5ns, 8nCK)
          tCPDED = PicoSecondsToClocks (5000, Outputs->MemoryClock);
          tCPDED = MAX (tCPDED, 8);
          // max(3.5ns, 8tCK)
          Offset = DIVIDECEIL (3500000, Timing->tCK); //femtoseconds
          tCKCKEH = MAX ((UINT32) tCKCKEH, Offset);
          break;

        default:
          MRC_DEBUG_ASSERT ((1 == 0), Debug, "Unsupported DDR Type: %u\n", DdrType);
          tCPDED = 0;
          break;
      }

      // Disable command tri state before training.
      CmdTristateDis = 1;

      FreqPoint = MrcIntData->SaGvPoint;

      GetSetVal = AddressMirror;
      MrcGetSetMcCh (MrcData, Controller, Channel, GsmMccAddrMirror,     WriteToCache | PrintValue, &GetSetVal);
      GetSetVal = X8Device;
      MrcGetSetMcCh (MrcData, Controller, Channel, GsmMccX8Device,       WriteToCache | PrintValue, &GetSetVal);
      if (Gear4) {
        GetSetVal = 1;
        MrcGetSetMcCh (MrcData, Controller, Channel, GsmMccGear4, WriteToCache | PrintValue, &GetSetVal);
      } else {
        GetSetVal = Gear2;
        MrcGetSetMcCh (MrcData, Controller, Channel, GsmMccGear2, WriteToCache | PrintValue, &GetSetVal);
      }
      MrcGetSetMcCh (MrcData, Controller, Channel, GsmMccCmdGapRatio,    WriteToCache | PrintValue, &Nto1Ratio);
      MrcGetSetMcCh (MrcData, Controller, Channel, GsmMccDramType,       WriteToCache | PrintValue, &DramTechnology);
      MrcGetSetMcCh (MrcData, Controller, Channel, GsmMccCmdStretch,     WriteToCache | PrintValue, &CmdStretch);
      MrcGetSetMcCh (MrcData, Controller, Channel, GsmMccCmdTriStateDis, WriteToCache | PrintValue, &CmdTristateDis);
      MrcGetSetMcCh (MrcData, Controller, Channel, GsmMccFreqPoint,      WriteToCache | PrintValue, &FreqPoint);
      MrcGetSetMcCh (MrcData, Controller, Channel, GsmMctCPDED,          WriteToCache | PrintValue, &tCPDED);
      MrcGetSetMcCh (MrcData, Controller, Channel, GsmMctCKCKEH,         WriteToCache | PrintValue, &tCKCKEH);
      MrcGetSetMcCh (MrcData, Controller, Channel, GsmMctCSCKEH,         WriteToCache | PrintValue, &tCSCKEH);
      MrcFlushRegisterCachedData (MrcData);

      Timing = &ChannelOut->Timing[Inputs->MemoryProfile];
      // Configure WCK parameters
      if (Lpddr5) {
        tCL = Timing->tCL;
        tCWL = Timing->tCWL;
        BurstLength = Outputs->BurstLength;
          Offset = OFFSET_CALC_MC_CH(MC0_CH0_CR_WCK_CONFIG_REG, MC1_CH0_CR_WCK_CONFIG_REG, Controller, MC0_CH1_CR_WCK_CONFIG_REG, IpChannel);
        WckConfig.Data = MrcReadCR64 (MrcData, Offset);
        // Ranks must be present incrementally.  Thus we can check the bit mask > 1 to determine that we have more than 1 rank present.
        WckConfig.Bits.LP5_WCK_MODE = (SafeMode) ? 0 : (ChannelOut->ValidRankBitMask > 1) ? 1 : 2;
        tWCKOFF = PicoSecondsToClocks (30000, Outputs->MemoryClock/4); // Convert 30ns to WCK
        if((tWCKOFF % 4) != 0) {
          tWCKOFF += (4 - (tWCKOFF % 4));
        }
        WckConfig.Bits.tWCKOFF = tWCKOFF;
        // WckConfig.Bits.WCK_OFF_IDLE_TIMER = 0x100; // This is programmed in MrcPowerDownConfig()
        WckConfig.Bits.WCK_MRR_SHORTER_BL_DIS = 1; // Needs to be active for Lp5

        // MRC should configure wck_config.twckpst to MAX(tWCKPST, tCSLCK)
        // This will postpone PDE and does not have power impact
        // tCSLCK = Max (5ns, 3nCK)
        tCSLCK = PicoSecondsToClocks (5000, Outputs->MemoryClock/4); // Convert 5ns to WCK
        tCSLCK = MAX (tCSLCK, 12);
        // tWCKSTOP = Max(2nCK, 6ns)
        tWckStop = PicoSecondsToClocks (6000, Outputs->MemoryClock/4); // Convert 6ns to WCK
        tWckStop = MAX (tWckStop, 8);

        WckConfig.Bits.tWCKPST = 3;  // To be modified based on the Mr10.Bits.WckPstLen

        if (WckConfig.Bits.LP5_WCK_MODE != 2) {
          // SAFE / MANUAL / OFF
          WckConfig.Bits.tWCKPST = MAX (WckConfig.Bits.tWCKPST, tCSLCK);
          WckConfig.Bits.tWCKPST = MAX (WckConfig.Bits.tWCKPST, tWckStop);
        } else {
          // DYNAMIC
          WckConfig.Bits.tWCKPST = (GearRatio * 2);
        }
        WckConfig.Bits.tCASSTOP_ADDITIONAL_GAP = WckConfig.Bits.tWCKPST;
        // Table 130 - WCK2CK SYNC Off Timing
        // New WCK = RL + BL + RD (tWCKPST / tCK)
        // tWCKPST is 2.5 tWCK
        WckConfig.Bits.RD_WCK_ASYNC_GAP = (tCL + BurstLength) * 4;
        // New WCK = WL + BL + RD (tWCKPST / tCK)
        WckConfig.Bits.WR_WCK_ASYNC_GAP = (tCWL + BurstLength) * 4;
        MrcWriteCR64 (MrcData, Offset, WckConfig.Data);
        MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "MC%u C%u: WckConfig = %08Xh\n", Controller, Channel, WckConfig.Data);
      }
      // Set Opportunistic Read for Write Major Mode
      Offset = OFFSET_CALC_MC_CH (MC0_CH0_CR_WMM_READ_CONFIG_REG, MC1_CH0_CR_WMM_READ_CONFIG_REG, Controller, MC0_CH1_CR_WMM_READ_CONFIG_REG, IpChannel);
      WmmReadConfig.Data = MrcReadCR (MrcData, Offset);
      WmmReadConfig.Bits.MAX_RPQ_CAS = 0x0A;
      MrcWriteCR (MrcData, Offset, WmmReadConfig.Data);

      for (Index = 0; Index < MRC_SC_PH_THROT_CR_COUNT; Index++) {
        ThrottlingPor = ScPhThrottlingPor[Index];
        ScPhThrottling.Data = 0;
        ScPhThrottling.Bits.loaded_same_rank        = ThrottlingPor[MrcSptLoadSameRank];
        ScPhThrottling.Bits.unloaded_same_rank      = ThrottlingPor[MrcSptUnloadedSameRank];
        ScPhThrottling.Bits.loaded_different_rank   = ThrottlingPor[MrcSptLoadedDiffRank];
        ScPhThrottling.Bits.unloaded_different_rank = ThrottlingPor[MrcSptUnloadedDiffRank];
        Offset = OFFSET_CALC_MC_CH (MC0_CH0_CR_SC_PH_THROTTLING_0_REG, MC1_CH0_CR_SC_PH_THROTTLING_0_REG, Controller, MC0_CH1_CR_SC_PH_THROTTLING_0_REG, IpChannel);
        Offset += (MC0_CH0_CR_SC_PH_THROTTLING_1_REG - MC0_CH0_CR_SC_PH_THROTTLING_0_REG) * Index;
        MrcWriteCR (MrcData, Offset, ScPhThrottling.Data);
      }

      Offset = OFFSET_CALC_MC_CH (MC0_CH0_CR_SC_WPQ_THRESHOLD_REG, MC1_CH0_CR_SC_WPQ_THRESHOLD_REG, Controller, MC0_CH1_CR_SC_WPQ_THRESHOLD_REG, IpChannel);
      ScWpqThreshold.Data = MrcReadCR (MrcData, Offset);
      // These settings are recommended for All Memory Techs (DDR-4/5,LP-4/5)
      ScWpqThreshold.Bits.PHs_allowed_under_low_wm  = 2;
      ScWpqThreshold.Bits.PHs_allowed_under_med_wm  = 3;
      ScWpqThreshold.Bits.PHs_allowed_under_high_wm = 5;
      MrcWriteCR (MrcData, Offset, ScWpqThreshold.Data);

      Offset = OFFSET_CALC_MC_CH (MC0_CH0_CR_SC_PR_CNT_CONFIG_REG, MC1_CH0_CR_SC_PR_CNT_CONFIG_REG, Controller, MC0_CH1_CR_SC_PR_CNT_CONFIG_REG, IpChannel);
      ScPrCntCfg.Data = MrcReadCR64 (MrcData, Offset);
      ScPrCntCfg.Bits.VC0_latency_guard_timer_x16     = (VC_LATENCY_GUARD_TIMER_PS * 2) / (Outputs->Dclkps * 16);
      ScPrCntCfg.Bits.VC1_latency_guard_timer_x8      = (VC_LATENCY_GUARD_TIMER_PS * 2) / (Outputs->Dclkps * 8);
      ScPrCntCfg.Bits.VC1_Isoc_latency_guard_timer_x8 = (VC_LATENCY_GUARD_TIMER_PS) / (Outputs->Dclkps * 8);
      MrcWriteCR64 (MrcData, Offset, ScPrCntCfg.Data);

      Offset = OFFSET_CALC_MC_CH (MC0_CH0_CR_XARB_CFG_BUBBLE_INJ_REG, MC1_CH0_CR_XARB_CFG_BUBBLE_INJ_REG, Controller, MC0_CH1_CR_XARB_CFG_BUBBLE_INJ_REG, IpChannel);
      XarbBubbleInjCfg.Data = MrcReadCR (MrcData, Offset);
      XarbBubbleInjCfg.Bits.bubble_inj_enable_any_cas_switch = 0;
      XarbBubbleInjCfg.Bits.bubble_inj_enable_read_switch = 0;
      XarbBubbleInjCfg.Bits.bubble_inj_enable_write_switch = 0;
      MrcWriteCR (MrcData, Offset, XarbBubbleInjCfg.Data);

      Offset = OFFSET_CALC_MC_CH (MC0_CH0_CR_SCHED_THIRD_CBIT_REG, MC1_CH0_CR_SCHED_THIRD_CBIT_REG, Controller, MC0_CH1_CR_SCHED_THIRD_CBIT_REG, IpChannel);
      SchedThirdCbit.Data = MrcReadCR (MrcData, Offset);
      SchedThirdCbit.Bits.dis_lpmode_on_sagv  = 1;
      SchedThirdCbit.Bits.invert_ALERT_n      = 0;
      SchedThirdCbit.Bits.pe_read_for_pe_blkr = 1;
      SchedThirdCbit.Bits.lp_read_blkr        = 1;
      if (Lpddr4) {
        SchedThirdCbit.Bits.dis_cke_off_in_refresh = 1;
      }
      MrcWriteCR (MrcData, Offset, SchedThirdCbit.Data);
      MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "MC%u C%u: SchedThirdCbit = %08Xh\n", Controller, Channel, SchedThirdCbit.Data);

      Offset = OFFSET_CALC_MC_CH (MC0_CH0_CR_SC_BLOCKING_RULES_CFG_REG, MC1_CH0_CR_SC_BLOCKING_RULES_CFG_REG, Controller, MC0_CH1_CR_SC_BLOCKING_RULES_CFG_REG, IpChannel);
      ScBlockingRulesCfg.Data = MrcReadCR (MrcData, Offset);
      if (!SafeMode) {
        ScBlockingRulesCfg.Bits.med_wm_allowed_preempt_priorities = 7;
        ScBlockingRulesCfg.Bits.high_wm_allowed_preempt_priorities = 6;
      }
      // These settings are recommended for All Memory Techs (DDR-4/5,LP-4/5)
      ScBlockingRulesCfg.Bits.high_wm_allowed_preempt_priorities  = 0;
      ScBlockingRulesCfg.Bits.med_wm_allowed_preempt_priorities   = 4;
      ScBlockingRulesCfg.Bits.low_wm_allowed_preempt_priorities   = 4;
      MrcWriteCR (MrcData, Offset, ScBlockingRulesCfg.Data);

      Offset = OFFSET_CALC_MC_CH (MC0_CH0_CR_SC_ADAPTIVE_PCIT_REG, MC1_CH0_CR_SC_ADAPTIVE_PCIT_REG, Controller, MC0_CH1_CR_SC_ADAPTIVE_PCIT_REG, IpChannel);
      ScAdaptivePcit.Data = MrcReadCR64 (MrcData, Offset);
      // ADAPTIVE_PCIT_WINDOW should be set to (tRC * 3) in DCLK units
      // tRC = tRAS + tRP
      // tRC = Timing->tRAS + Timing->tRP;
      Data32 = (Timing->tRAS + Timing->tRCDtRP) * 3;
      if (Outputs->Gear2) {
        Data32 = Data32 / 2;
      } else if (Outputs->Gear4) {
        Data32 = Data32 / 4;
      }
      if (Inputs->A0) {
        Data32 = 0;
      }
      ScAdaptivePcit.Bits.ADAPTIVE_PCIT_WINDOW = Data32;
      ScAdaptivePcit.Bits.PH_WEIGHT            = 0;
      ScAdaptivePcit.Bits.EARLY_WEIGHT         = 2;
      ScAdaptivePcit.Bits.MAX_PCIT             = MC0_CH0_CR_SC_ADAPTIVE_PCIT_MAX_PCIT_MAX;
      if (Inputs->UlxUlt) {
        ScAdaptivePcit.Bits.IGNORE_GT          = 0x1;
        ScAdaptivePcit.Bits.IGNORE_VC1         = 0x1;
        ScAdaptivePcit.Bits.TRANSACTION_FACTOR = 0x3;
        ScAdaptivePcit.Bits.LATE_WEIGHT        = 0x1;
        ScAdaptivePcit.Bits.CHANGE_RANGE       = 0x3;
        ScAdaptivePcit.Bits.MIN_PCIT           = 0x20;
        ScAdaptivePcit.Bits.GOOD_PAGE_CLOSE_WEIGHT = 0x1;

      }
      MrcWriteCR64 (MrcData, Offset, ScAdaptivePcit.Data);

      // Enable Background Zq Cal Mode for Lpddr5
      // For Lpddr5 MC sends Zq Start Command, which is not required for Lp5 Background Zq Mode
      // When set to 1 MC skips sending a ZQstart command and only sends a ZQ latch command
      // MR-28 OP[5] = 1 in JEDEC RESET (Command-Based-Calibration mode)   -> McmntsSpare.Bits.Background_ZQ_Mode = 0
      // MR-28 OP[5] = 0 in JEDEC RESET (Background Calibration (Default)) -> McmntsSpare.Bits.Background_ZQ_Mode = 1
      Offset = OFFSET_CALC_MC_CH (MC0_CH0_CR_MCMNTS_SPARE_REG, MC1_CH0_CR_MCMNTS_SPARE_REG, Controller, MC0_CH1_CR_MCMNTS_SPARE_REG, IpChannel);
      McmntsSpare.Data = MrcReadCR (MrcData, Offset);
      if (Lpddr5) {
        McmntsSpare.Bits.Background_ZQ_Mode = 1;
      }
      // Assume Normal refresh
      McmntsSpare.Bits.ForceX2Ref     = 0;
      McmntsSpare.Bits.DisLowRefRate  = 0;
      McmntsSpare.Bits.ForceX4Ref     = 0;
      McmntsSpare.Bits.ForceX8Ref     = 0;
      RefreshTypeStr = NULL;
      switch (Inputs->McRefreshRate) {
        case RefreshNORMAL:
          RefreshTypeStr = "Normal";
          break;
        case Refresh1x: // force 1X refresh
          McmntsSpare.Bits.DisLowRefRate = 1;
          RefreshTypeStr = "1x";
          break;
        case Refresh2x: // force 2X refresh
          McmntsSpare.Bits.ForceX2Ref = 1;
          RefreshTypeStr = "2x";
          break;
        case Refresh4x: // force 4X refresh
          McmntsSpare.Bits.ForceX4Ref = 1;
          RefreshTypeStr = "4x";
          break;
        default:
          MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "%s Inputs->McRefreshRate = %u is not supported!\n", Inputs->McRefreshRate);
          break;
      }
      if (RefreshTypeStr != NULL) {
        MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Refresh mode: %s\n", RefreshTypeStr);
      }
      MrcWriteCR (MrcData, Offset, McmntsSpare.Data);
      MRC_DEBUG_MSG (Debug, MSG_LEVEL_ERROR, "MC%u C%u: McmntsSpare = %08Xh\n", Controller, Channel, McmntsSpare.Data);

      // Here we assume that tREFI is already programmed, which is true because MrcRefreshConfiguration is called before MrcSchedulerParametersConfig.
      MrcGetSetMcCh (MrcData, Controller, Channel, GsmMctREFI, ReadFromCache, &GetSetVal);
      Offset = OFFSET_CALC_MC_CH (MC0_CH0_CR_DEADLOCK_BREAKER_REG, MC1_CH0_CR_DEADLOCK_BREAKER_REG, Controller, MC0_CH1_CR_DEADLOCK_BREAKER_REG, IpChannel);
      DeadlockBreaker.Data = MrcReadCR (MrcData, Offset);
      // Set No_CAS_threshold to tREFI / 2
      // TC_RFTP.tREFI is 18 bits but No_CAS_threshold is 16 bits. Here guarantee No_CAS_threshold doesn't overflow.
      DeadlockBreaker.Bits.No_CAS_threshold         = MIN ((UINT32) GetSetVal / 2, MC0_CH0_CR_DEADLOCK_BREAKER_No_CAS_threshold_MAX);
      DeadlockBreaker.Bits.No_CAS_threshold_en      = 1;
      DeadlockBreaker.Bits.Preemption_threshold_en  = 1;
      DeadlockBreaker.Bits.Rank_join                = 2;
      MrcWriteCR (MrcData, Offset, DeadlockBreaker.Data);
      MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "MC%u C%u: DEADLOCK_BREAKER = %08Xh\n", Controller, Channel, DeadlockBreaker.Data);

      Offset = OFFSET_CALC_MC_CH (MC0_CH0_CR_MCMNTS_GLOBAL_DRIVER_GATE_CFG_REG, MC1_CH0_CR_MCMNTS_GLOBAL_DRIVER_GATE_CFG_REG, Controller, MC0_CH1_CR_MCMNTS_GLOBAL_DRIVER_GATE_CFG_REG, IpChannel);
      McMntsGlobalDriverGateCfg.Data = MrcReadCR (MrcData, Offset);
      if (Outputs->EccSupport && Inputs->EccDftEn) {
        McMntsGlobalDriverGateCfg.Bits.qclk_global_driver_override_to_dclk = 1;
      }
      MrcWriteCR (MrcData, Offset, McMntsGlobalDriverGateCfg.Data);

      Offset = OFFSET_CALC_MC_CH (MC0_CH0_CR_SC_PBR_REG, MC1_CH0_CR_SC_PBR_REG, Controller, MC0_CH1_CR_SC_PBR_REG, IpChannel);
      ScPbr.Data = MrcReadCR (MrcData, Offset);
      ScPbr.Bits.PBR_Idle_Bank_Condition_WR_dis = 1;
      MrcWriteCR (MrcData, Offset, ScPbr.Data);

    } // Channel

    Offset = OFFSET_CALC_CH (MC0_MCDECS_SECOND_CBIT_REG, MC1_MCDECS_SECOND_CBIT_REG, Controller);
    McdecsSecondCbit.Data = MrcReadCR (MrcData, Offset);
    if (Inputs->A0) {
      McdecsSecondCbit.Bits.dis_spid_cmd_clk_gate = 0;
    }
    McdecsSecondCbit.Bits.Allow_RH_Debt_in_SR = 0;
    MrcWriteCR (MrcData, Offset, McdecsSecondCbit.Data);

  } // Controller

  ConfigSpidLowPowerCtl (MrcData);

  if (Ddr4 || Ddr5) {
    MrcSetOdtMatrix (MrcData, 1);
  }

  return;
}

/**
  Set ODT Logic behavior for DDR4 and DDR5.

  @param[in, out] MrcData - Include all MRC global data.
  @param[in]      Profile - Behavior type. Currently Profile 1 is supported (DDR3-like)

  @retval none
**/
void
MrcSetOdtMatrix (
  IN OUT MrcParameters *const MrcData,
  IN UINT8                    Profile
  )
{
  MrcDebug      *Debug;
  MrcOutput     *Outputs;
  MrcChannelOut *ChannelOut;
  INT64         EnableOdtMatrix;
  UINT32        Controller;
  UINT32        Offset;
  UINT8         Channel;
  UINT8         Dimm;
  BOOLEAN       Ddr5;
  MC0_CH0_CR_SC_ODT_MATRIX_STRUCT  OdtMatrix;

  Outputs       = &MrcData->Outputs;
  Debug         = &Outputs->Debug;
  Ddr5          = (Outputs->DdrType == MRC_DDR_TYPE_DDR5);

  for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
    for (Channel = 0; Channel < MAX_CHANNEL; Channel++) {
      if (!MrcChannelExist (MrcData, Controller, Channel)) {
        continue;
      }
      ChannelOut = &Outputs->Controller[Controller].Channel[Channel];
      OdtMatrix.Data = 0;
      switch (Profile) {
        case 1:
          if (Ddr5) {
            if (ChannelOut->DimmCount == 2) {
              // Assert NomRd / NomWr on both ranks of the non-target DIMM
              OdtMatrix.Bits.Write_Rank_0 = 0xC;
              OdtMatrix.Bits.Write_Rank_1 = 0xC;
              OdtMatrix.Bits.Write_Rank_2 = 0x3;
              OdtMatrix.Bits.Write_Rank_3 = 0x3;
              OdtMatrix.Bits.Read_Rank_0  = 0xC;
              OdtMatrix.Bits.Read_Rank_1  = 0xC;
              OdtMatrix.Bits.Read_Rank_2  = 0x3;
              OdtMatrix.Bits.Read_Rank_3  = 0x3;
            }
          } else { // DDR4
            // DDR3 like behavior: put OdtNom on rank 0 of the other DIMM
            // DDR4 difference is that ODT pin is not asserted on the target rank
            if (ChannelOut->DimmCount == 2) {
              OdtMatrix.Bits.Write_Rank_0 = 0x4;
              OdtMatrix.Bits.Write_Rank_1 = 0x4;
              OdtMatrix.Bits.Write_Rank_2 = 0x1;
              OdtMatrix.Bits.Write_Rank_3 = 0x1;
              OdtMatrix.Bits.Read_Rank_0  = 0x4;
              OdtMatrix.Bits.Read_Rank_1  = 0x4;
              OdtMatrix.Bits.Read_Rank_2  = 0x1;
              OdtMatrix.Bits.Read_Rank_3  = 0x1;
            } else {
              for (Dimm = 0; Dimm < MAX_DIMMS_IN_CHANNEL; Dimm++) {
                if (ChannelOut->Dimm[Dimm].RankInDimm > 1) {
                  if (Dimm == 0) {
                    OdtMatrix.Bits.Write_Rank_0 = 0x1;
                    OdtMatrix.Bits.Write_Rank_1 = 0x2;
                  } else {
                    OdtMatrix.Bits.Write_Rank_2 = 0x4;
                    OdtMatrix.Bits.Write_Rank_3 = 0x8;
                  }
                }
              }
            }
          }
          break;

        case 0:
          // set odt pins to zero.
          break;

        default:
          MRC_DEBUG_MSG (Debug, MSG_LEVEL_ERROR, "MrcSetOdtMatrix : Unknown Odt Matrix Profile\n");
          break;
      } // switch Profile

      Offset = OFFSET_CALC_MC_CH (MC0_CH0_CR_SC_ODT_MATRIX_REG, MC1_CH0_CR_SC_ODT_MATRIX_REG, Controller, MC0_CH1_CR_SC_ODT_MATRIX_REG, Channel);
      MrcWriteCR (MrcData, Offset, OdtMatrix.Data);

      EnableOdtMatrix = 1;
      MrcGetSetMcCh (MrcData, Controller, Channel, GsmMccEnableOdtMatrix, WriteCached, &EnableOdtMatrix);
    } // Channel
  } // Controller
}

/**
  Configure DDR4_1DPC performance feature.

  @param[in] MrcData - Include all MRC global data.

  @retval Nothing.
**/
void
MrcConfigureDdr4OneDpc (
  IN MrcParameters *const MrcData
  )
{
  const MrcInput  *Inputs;
  MrcOutput       *Outputs;
  MrcDebug        *Debug;
  MrcChannelOut   *ChannelOut;
  MrcDimmOut      *DimmOut;
  UINT32          Controller;
  UINT32          Channel;
  UINT32          Dimm;
  UINT32          DimmMask;
  UINT32          Offset;
  INT64           GetSetVal;
  BOOLEAN         Ddr4;
  BOOLEAN         Ddr5;
  MC0_CH0_CR_SC_ODT_MATRIX_STRUCT   OdtMatrix;
  MC0_CH0_CR_MRS_FSM_CONTROL_STRUCT MrsFsmControl;
  MC0_CH0_CR_DDR_MR_PARAMS_STRUCT   DdrMrParams;
  MC0_CH0_CR_DDR4_MR6_VREF_DIMM1_VALUES_0_STRUCT  Mr6VrefValues;

  Inputs   = &MrcData->Inputs;
  Outputs  = &MrcData->Outputs;
  Debug    = &Outputs->Debug;
  Ddr4     = (Outputs->DdrType == MRC_DDR_TYPE_DDR4);
  Ddr5     = (Outputs->DdrType == MRC_DDR_TYPE_DDR5);

  if (!Ddr4 && !Ddr5) { // This is DDR4/DDR5-only feature
    return;
  }
  //MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "MrcConfigureDdr4OneDpc\n");

  for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
    for (Channel = 0; Channel < Outputs->MaxChannels; Channel++) {
      if (!MrcChannelExist (MrcData, Controller, Channel)) {
        continue;
      }
      ChannelOut = &Outputs->Controller[Controller].Channel[Channel];
      if (ChannelOut->DimmCount != 1) {  // Must be 1DPC
        //MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "MC%d C%d DimmCount = %d, skip\n", Controller, Channel, ChannelOut->DimmCount);
        continue;
      }
      for (Dimm = 0; Dimm < MAX_DIMMS_IN_CHANNEL; Dimm++) {
        DimmOut = &ChannelOut->Dimm[Dimm];
        if ((DimmOut->Status != DIMM_PRESENT) || (DimmOut->RankInDimm != 2)) { // Must be 2R DIMM
          //MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "MC%d C%d DIMM%d RankInDimm = %d, skip\n", Controller, Channel, Dimm, DimmOut->RankInDimm);
          continue;
        }
        DimmMask = 1 << Dimm;
        if (Ddr4 && ((Inputs->Ddr4OneDpc & DimmMask) == 0)) {
          // If feature is disabled for this DIMM on DDR4
          // On DDR5 1DPC 2R we should always enable this feature, so input parameter is not used
          //MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Inputs->Ddr4OneDpc = %d, DimmMask = %d\n", Inputs->Ddr4OneDpc, DimmMask);
          continue;
        }
        MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Mc%u C%d DIMM%d: Enabling 1dpc_split_ranks\n", Controller, Channel, Dimm);
        // We have a single 2R DIMM on this channel and going to enable ddr4_1dpc feature on this channel

        // Force CKE on all ranks, to avoid CKE going down during enabling of this feature
        GetSetVal = 0x0F;
        MrcGetSetMcCh (MrcData, Controller, Channel, GsmMccCkeOn, WriteNoCache | PrintValue, &GetSetVal);

        // Enable remapped rank in MC_INIT_STATE.Rank_occupancy, to avoid CKE going down during this flow
        GetSetVal = (Dimm == 0) ? 0x0B : 0x0D; // '1011' or '1101'
        MrcGetSetMcCh (MrcData, Controller, Channel, GsmMccRankOccupancy, WriteCached | PrintValue, &GetSetVal);


        GetSetVal = DimmMask;
        MrcGetSetMcCh (MrcData, Controller, Channel, GsmMccDdr4OneDpc, WriteCached | PrintValue, &GetSetVal);

        // 1. MC_INIT_STATE.Rank_occupancy must be '1001'
        GetSetVal = 9;
        MrcGetSetMcCh (MrcData, Controller, Channel, GsmMccRankOccupancy, WriteCached | PrintValue, &GetSetVal);

        // 2. Rank Interleave must be on - N/A for ADL

        // 3. If Address Mirror enabled on this DIMM, must enable it on the other DIMM as well
        if (DimmOut->AddressMirrored) {
          GetSetVal = 3;
          MrcGetSetMcCh (MrcData, Controller, Channel, GsmMccAddrMirror, WriteToCache | PrintValue, &GetSetVal);
        }

        // 4. In TC_RDRD, TC_RDWR, TC_WRRD and TC_WRWR configure to the *_dd field the same value as to the _dr field.
        MrcGetSetMcCh (MrcData, Controller, Channel, GsmMctRDRDdr, ReadFromCache | PrintValue, &GetSetVal);
        MrcGetSetMcCh (MrcData, Controller, Channel, GsmMctRDRDdd, WriteCached   | PrintValue, &GetSetVal);

        MrcGetSetMcCh (MrcData, Controller, Channel, GsmMctRDWRdr, ReadFromCache | PrintValue, &GetSetVal);
        MrcGetSetMcCh (MrcData, Controller, Channel, GsmMctRDWRdd, WriteCached   | PrintValue, &GetSetVal);

        MrcGetSetMcCh (MrcData, Controller, Channel, GsmMctWRRDdr, ReadFromCache | PrintValue, &GetSetVal);
        MrcGetSetMcCh (MrcData, Controller, Channel, GsmMctWRRDdd, WriteCached   | PrintValue, &GetSetVal);

        MrcGetSetMcCh (MrcData, Controller, Channel, GsmMctWRWRdr, ReadFromCache | PrintValue, &GetSetVal);
        MrcGetSetMcCh (MrcData, Controller, Channel, GsmMctWRWRdd, WriteCached   | PrintValue, &GetSetVal);

        // Must update the XARB bubble injector when TAT values change
        // This will read TAT values from the CRs, hence using WriteCached in the block above, to avoid extra register cache flush
        SetTcBubbleInjector (MrcData, Controller, Channel);

        // 5. In ODT_MATRIX configure the fields according to the rank occupancy (i.e. if ranks 0,1 exist set the matrix fields for ranks 0,3 instead)
        // Note that inside each field you still indicate which ODT pins are required to be set, so set bits 0,1 and not 0,3
        Offset = OFFSET_CALC_MC_CH (MC0_CH0_CR_SC_ODT_MATRIX_REG, MC1_CH0_CR_SC_ODT_MATRIX_REG, Controller, MC0_CH1_CR_SC_ODT_MATRIX_REG, Channel);
        OdtMatrix.Data = MrcReadCR (MrcData, Offset);
        if (Dimm == 0) {
          OdtMatrix.Bits.Write_Rank_3 = OdtMatrix.Bits.Write_Rank_1;
        } else { // DIMM1
          OdtMatrix.Bits.Write_Rank_0 = OdtMatrix.Bits.Write_Rank_2;
        }
        OdtMatrix.Bits.Write_Rank_1 = 0;
        OdtMatrix.Bits.Write_Rank_2 = 0;
        MrcWriteCR (MrcData, Offset, OdtMatrix.Data);

        // 6. If DIMM0: Roundtrip latency of Rank 3 should be set to the roundtrip latency of Rank 1
        //    If DIMM1: Roundtrip latency of Rank 0 should be set to the roundtrip latency of Rank 2
        if (Dimm == 0) {
          MrcGetSetMcChRnk (MrcData, Controller, Channel, 1, RoundTripDelay, ReadFromCache | PrintValue, &GetSetVal);
          MrcGetSetMcChRnk (MrcData, Controller, Channel, 3, RoundTripDelay, WriteToCache  | PrintValue, &GetSetVal);
        } else { // DIMM1
          MrcGetSetMcChRnk (MrcData, Controller, Channel, 2, RoundTripDelay, ReadFromCache | PrintValue, &GetSetVal);
          MrcGetSetMcChRnk (MrcData, Controller, Channel, 0, RoundTripDelay, WriteToCache  | PrintValue, &GetSetVal);
        }

        // 7. When MRH is used (DDR_MR_COMMNAD_0_0_0_MCHBAR), it should be programmed to access either rank 0 or rank 3 (and not 1 or 2).
        // See MrcRunMrh() and MrcDdr4PdaCmd()

        // 8. sc_gs_cfg_training.reset_on_command = 0
        // We don't use this feature, so it's stays zero

        // 9. sc_gs_cfg.x8_device should be the same for both DIMMs
        GetSetVal = (DimmOut->SdramWidth == 8) ? 3 : 0;
        MrcGetSetMcCh (MrcData, Controller, Channel, GsmMccX8Device, WriteToCache | PrintValue, &GetSetVal);

        // Adjust CKE mask to 1001. MC is using ranks 0 and 3 when ddr4_1dpc is enabled.
        GetSetVal = 9;
        MrcGetSetMcCh (MrcData, Controller, Channel, GsmMccCkeOverride, WriteNoCache | PrintValue, &GetSetVal);
        MrcGetSetMcCh (MrcData, Controller, Channel, GsmMccCkeOn,       WriteNoCache | PrintValue, &GetSetVal);

        // Set the sc_gs_cfg_training.reset_on_command to zero.
        GetSetVal = 0;
        MrcGetSetMcCh (MrcData, Controller, Channel, GsmMccResetOnCmd, WriteCached   | PrintValue, &GetSetVal);

        // Set the mc_refresh_stagger.ref_stagger_mode to 1.
        GetSetVal = 1;
        MrcGetSetMcCh (MrcData, Controller, Channel, GsmMccRefStaggerMode, WriteCached   | PrintValue, &GetSetVal);

        // DDR4: Set mrs_fsm_control.twodpc_support to 1.
        Offset = OFFSET_CALC_MC_CH (MC0_CH0_CR_MRS_FSM_CONTROL_REG, MC1_CH0_CR_MRS_FSM_CONTROL_REG, Controller, MC0_CH1_CR_MRS_FSM_CONTROL_REG, Channel);
        MrsFsmControl.Data = MrcReadCR64 (MrcData, Offset);
        MrsFsmControl.Bits.TwoDPC_support = 1;
        MrcWriteCR64 (MrcData, Offset, MrsFsmControl.Data);

        // DDR4: Update legacy MRS FSM to use ranks 0 and 3 for MR6 PDA
        if (Dimm == 0) {
          Mr6VrefValues.Data = MrcReadCR64 (MrcData, (Controller == 0) ? MC0_CH0_CR_DDR4_MR6_VREF_VALUES_1_REG : MC1_CH0_CR_DDR4_MR6_VREF_VALUES_1_REG);              // Rank 1
          MrcWriteCR64 (MrcData, (Controller == 0) ? MC0_CH0_CR_DDR4_MR6_VREF_DIMM1_VALUES_1_REG : MC1_CH0_CR_DDR4_MR6_VREF_DIMM1_VALUES_1_REG, Mr6VrefValues.Data);  // Rank 3
        } else { // DIMM1
          Mr6VrefValues.Data = MrcReadCR64 (MrcData, (Controller == 0) ? MC0_CH0_CR_DDR4_MR6_VREF_DIMM1_VALUES_0_REG : MC1_CH0_CR_DDR4_MR6_VREF_DIMM1_VALUES_0_REG);  // Rank 2
          MrcWriteCR64 (MrcData, (Controller == 0) ? MC0_CH0_CR_DDR4_MR6_VREF_VALUES_0_REG : MC1_CH0_CR_DDR4_MR6_VREF_VALUES_0_REG, Mr6VrefValues.Data);              // Rank 0
        }

        // DDR5: ddr5_dl_8Gb / ddr5_ds_8Gb should be the same for both DIMMs
        // Single DIMM should be DIMM_L, so read from DIMM_L and program DIMM_S to the same value
        MrcGetSetMcCh (MrcData, Controller, Channel, GsmMccDdr5Device8GbDimmL, ReadFromCache | PrintValue, &GetSetVal);
        MrcGetSetMcCh (MrcData, Controller, Channel, GsmMccDdr5Device8GbDimmS, WriteToCache  | PrintValue, &GetSetVal);

        // Update DDR_MR_PARAMS to use ranks 0 and 3
        Offset = OFFSET_CALC_MC_CH (MC0_CH0_CR_DDR_MR_PARAMS_REG, MC1_CH0_CR_DDR_MR_PARAMS_REG, Controller, MC0_CH1_CR_DDR_MR_PARAMS_REG, Channel);
        DdrMrParams.Data = MrcReadCR (MrcData, Offset);
        if (Dimm == 0) {
          DdrMrParams.Bits.Rank_3_width = DdrMrParams.Bits.Rank_1_width;
        } else { // DIMM1
          DdrMrParams.Bits.Rank_0_width = DdrMrParams.Bits.Rank_2_width;
        }
        MrcWriteCR (MrcData, Offset, DdrMrParams.Data);
      } // for Dimm
    } // for Channel
  } // for Controller
  MrcFlushRegisterCachedData (MrcData);
}
