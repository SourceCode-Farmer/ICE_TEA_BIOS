#ifndef __MrcMcRegisterStructAdl7xxx_h__
#define __MrcMcRegisterStructAdl7xxx_h__
/** @file
  This file was automatically generated. Modify at your own risk.
  Note that no error checking is done in these functions so ensure that the correct values are passed.

@copyright
  Copyright (c) 2010 - 2021 Intel Corporation. All rights reserved
  This software and associated documentation (if any) is furnished
  under a license and may only be used or copied in accordance
  with the terms of the license. Except as permitted by the
  license, no part of this software or documentation may be
  reproduced, stored in a retrieval system, or transmitted in any
  form or by any means without the express written consent of
  Intel Corporation.
  This file contains an 'Intel Peripheral Driver' and is uniquely
  identified as "Intel Reference Module" and is licensed for Intel
  CPUs and chipsets under the terms of your license agreement with
  Intel or your vendor. This file may be modified by the user, subject
  to additional terms of the license agreement.

@par Specification Reference:
**/

#pragma pack(push, 1)
typedef union {
  struct {
    UINT32 DIP_ENABLE                              :  1;  // Bits 0:0
    UINT32 MEE0_ENABLE                             :  1;  // Bits 1:1
    UINT32 MEE1_ENABLE                             :  1;  // Bits 2:2
    UINT32 TMECC0_ENABLE                           :  1;  // Bits 3:3
    UINT32 TMECC1_ENABLE                           :  1;  // Bits 4:4
    UINT32 ASTRO_ENABLE                            :  1;  // Bits 5:5
    UINT32 EFLOW0_ENABLE                           :  1;  // Bits 6:6
    UINT32 EFLOW1_ENABLE                           :  1;  // Bits 7:7
    UINT32 MC0_ENABLE                              :  1;  // Bits 8:8
    UINT32 MC1_ENABLE                              :  1;  // Bits 9:9
    UINT32 RSVD0                                   :  22;  // Bits 31:10
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} MEMSS_PM_EN_MCHBAR_IMPH_STRUCT;
typedef union {
  struct {
    UINT32                                         :  3;  // Bits 2:0
    UINT32 DEVNUM                                  :  5;  // Bits 7:3
    UINT32 BUSNUM                                  :  8;  // Bits 15:8
    UINT32                                         :  15;  // Bits 30:16
    UINT32 HDAUD_EN                                :  1;  // Bits 31:31
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} HDAUDRID_IMPH_STRUCT;
typedef union {
  struct {
    UINT32 AGENT_0                                 :  4;  // Bits 3:0
    UINT32 AGENT_1                                 :  4;  // Bits 7:4
    UINT32 AGENT_2                                 :  4;  // Bits 11:8
    UINT32 AGENT_3                                 :  4;  // Bits 15:12
    UINT32 AGENT_4                                 :  4;  // Bits 19:16
    UINT32 AGENT_5                                 :  4;  // Bits 23:20
    UINT32 AGENT_6                                 :  4;  // Bits 27:24
    UINT32 AGENT_7                                 :  4;  // Bits 31:28
    UINT32 AGENT_8                                 :  4;  // Bits 35:32
    UINT32 AGENT_9                                 :  4;  // Bits 39:36
    UINT32 RSVD0                                   :  4;  // Bits 43:40
    UINT32 RSVD1                                   :  4;  // Bits 47:44
    UINT32 RSVD2                                   :  16;  // Bits 63:48
  } Bits;
  UINT64 Data;
  UINT32 Data32[2];
  UINT16 Data16[4];
  UINT8  Data8[8];
} MEMSS_PM_ORDER_MCHBAR_IMPH_STRUCT;
typedef union {
  struct {
    UINT32 DROP_VLW_CTL                            :  1;  // Bits 0:0
    UINT32                                         :  30;  // Bits 30:1
    UINT32 DROP_VLW_STS                            :  1;  // Bits 31:31
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} VLWCTL_STRUCT;
typedef union {
  struct {
    UINT32 FUNNUM                                  :  3;  // Bits 2:0
    UINT32 DEVNUM                                  :  5;  // Bits 7:3
    UINT32 BUSNUM                                  :  8;  // Bits 15:8
    UINT32 BARNUM                                  :  3;  // Bits 18:16
    UINT32                                         :  13;  // Bits 31:19
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} VDMBDFBARKVM_IMPH_STRUCT;

typedef VDMBDFBARKVM_IMPH_STRUCT VDMBDFBARPAVP_IMPH_STRUCT;
typedef union {
  struct {
    UINT32 VC                                      :  4;  // Bits 3:0
    UINT32 FMT_CMDTYPE                             :  6;  // Bits 9:4
    UINT32 TC                                      :  4;  // Bits 13:10
    UINT32 CHAIN                                   :  1;  // Bits 14:14
    UINT32 NS                                      :  1;  // Bits 15:15
    UINT32 RO                                      :  1;  // Bits 16:16
    UINT32 LENGTH                                  :  5;  // Bits 21:17
    UINT32 EP                                      :  1;  // Bits 22:22
    UINT32 AT                                      :  2;  // Bits 24:23
    UINT32                                         :  7;  // Bits 31:25
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} PRIMUP_MASK1_IMPH_STRUCT;
typedef union {
  struct {
    UINT32 RQID                                    :  16;  // Bits 15:0
    UINT32 TAG                                     :  8;  // Bits 23:16
    UINT32 LBEFBE_MSGTYPE                          :  8;  // Bits 31:24
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} PRIMUP_MASK2_IMPH_STRUCT;
typedef union {
  struct {
    UINT32 ADDR_31_0                               :  32;  // Bits 31:0
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} PRIMUP_MASK3_IMPH_STRUCT;
typedef union {
  struct {
    UINT32 ADDR_63_32                              :  32;  // Bits 31:0
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} PRIMUP_MASK4_IMPH_STRUCT;

typedef PRIMUP_MASK1_IMPH_STRUCT PRIMUP_COMP1_IMPH_STRUCT;

typedef PRIMUP_MASK2_IMPH_STRUCT PRIMUP_COMP2_IMPH_STRUCT;

typedef PRIMUP_MASK3_IMPH_STRUCT PRIMUP_COMP3_IMPH_STRUCT;

typedef PRIMUP_MASK4_IMPH_STRUCT PRIMUP_COMP4_IMPH_STRUCT;
typedef union {
  struct {
    UINT32 ENABLE                                  :  1;  // Bits 0:0
    UINT32 TRIGGERED                               :  1;  // Bits 1:1
    UINT32 STALL_DNARB                             :  1;  // Bits 2:2
    UINT32 STALL_UPARB                             :  1;  // Bits 3:3
    UINT32 STALL_SNPARB                            :  1;  // Bits 4:4
    UINT32 STALL_PREVTDARB                         :  1;  // Bits 5:5
    UINT32 STALL_FIFOARB                           :  1;  // Bits 6:6
    UINT32 STALL_P2PARB                            :  1;  // Bits 7:7
    UINT32                                         :  15;  // Bits 22:8
    UINT32 STALL_DELAY                             :  9;  // Bits 31:23
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} PRIMUP_TRIGGER_IMPH_STRUCT;
typedef union {
  struct {
    UINT32 PCIE0_EN                                :  1;  // Bits 0:0
    UINT32 PCIE1_EN                                :  1;  // Bits 1:1
    UINT32 PCIE2_EN                                :  1;  // Bits 2:2
    UINT32 PCIE3_EN                                :  1;  // Bits 3:3
    UINT32 PCIE4_EN                                :  1;  // Bits 4:4
    UINT32 PCIE5_EN                                :  1;  // Bits 5:5
    UINT32 PCIE6_EN                                :  1;  // Bits 6:6
    UINT32 PCIE7_EN                                :  1;  // Bits 7:7
    UINT32 xHCI_EN                                 :  1;  // Bits 8:8
    UINT32 xDCI_EN                                 :  1;  // Bits 9:9
    UINT32 TBT_DMA0_EN                             :  1;  // Bits 10:10
    UINT32 TBT_DMA1_EN                             :  1;  // Bits 11:11
    UINT32 TBT_DMA2_EN                             :  1;  // Bits 12:12
    UINT32 TBT_DMA3_EN                             :  1;  // Bits 13:13
    UINT32 Reserved                                :  18;  // Bits 31:14
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} TCSS_DEVEN_IMPH_STRUCT;
typedef union {
  struct {
    UINT32 TC_PCIE0_DIS                            :  1;  // Bits 0:0
    UINT32 TC_PCIE1_DIS                            :  1;  // Bits 1:1
    UINT32 TC_PCIE2_DIS                            :  1;  // Bits 2:2
    UINT32 TC_PCIE3_DIS                            :  1;  // Bits 3:3
    UINT32 TC_PCIE4_DIS                            :  1;  // Bits 4:4
    UINT32 TC_PCIE5_DIS                            :  1;  // Bits 5:5
    UINT32 TC_PCIE6_DIS                            :  1;  // Bits 6:6
    UINT32 TC_PCIE7_DIS                            :  1;  // Bits 7:7
    UINT32 TC_XHCI_DIS                             :  1;  // Bits 8:8
    UINT32 TC_XDCI_DIS                             :  1;  // Bits 9:9
    UINT32 TC_TBT_DMA0_DIS                         :  1;  // Bits 10:10
    UINT32 TC_TBT_DMA1_DIS                         :  1;  // Bits 11:11
    UINT32 TC_TBT_DMA2_DIS                         :  1;  // Bits 12:12
    UINT32 SPARE_15_13                             :  3;  // Bits 15:13
    UINT32 IOM_DIS                                 :  1;  // Bits 16:16
    UINT32 DPIN_PORT_COUNT                         :  3;  // Bits 19:17
    UINT32 SPARE_31_20                             :  12;  // Bits 31:20
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} CAPID0_D_STRUCT;
typedef union {
  struct {
    UINT32 Max_Data_Rate_At_GEAR1                  :  7;  // Bits 6:0
    UINT32 Reserved                                :  25;  // Bits 31:7
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} CAPID0_F_0_0_0_PCI_STRUCT;
typedef union {
  struct {
    UINT32 PEG2DMIDIS                              :  1;  // Bits 0:0
    UINT32 EOIB                                    :  1;  // Bits 1:1
    UINT32 MSIBYPDIS                               :  1;  // Bits 2:2
    UINT32 TYPEC2DMIDIS                            :  1;  // Bits 3:3
    UINT32 PHLDDIS                                 :  1;  // Bits 4:4
    UINT32 RSVD_8_5                                :  4;  // Bits 8:5
    UINT32 PHLDBLKDIS                              :  1;  // Bits 9:9
    UINT32                                         :  1;  // Bits 10:10
    UINT32 DIS_VLW_PEG                             :  1;  // Bits 11:11
    UINT32 SPECRDDIS                               :  1;  // Bits 12:12
    UINT32                                         :  1;  // Bits 13:13
    UINT32 IMGU2DMIDIS                             :  1;  // Bits 14:14
    UINT32 GMM2DMIDIS                              :  1;  // Bits 15:15
    UINT32 FORCEREDISP                             :  1;  // Bits 16:16
    UINT32 FORCE_WALK_SNP                          :  1;  // Bits 17:17
    UINT32 SNIFREGS_RTLCG_DIS                      :  1;  // Bits 18:18
    UINT32 DETERMINISTIC_BOUNDARIES                :  1;  // Bits 19:19
    UINT32 WRC_DIS                                 :  1;  // Bits 20:20
    UINT32 DUAL_SNP_RSP_DIS                        :  1;  // Bits 21:21
    UINT32 VCRT_WR_PRIO_CNTR_DEPTH                 :  6;  // Bits 27:22
    UINT32 RSVD_28_28                              :  1;  // Bits 28:28
    UINT32 RSVD_29_29                              :  1;  // Bits 29:29
    UINT32 RSVD_31_30                              :  2;  // Bits 31:30
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} HCTL0_IMPH_STRUCT;
typedef union {
  struct {
    UINT32 REGBAREN                                :  1;  // Bits 0:0
    UINT32                                         :  23;  // Bits 23:1
    UINT32 REGFBARLow                              :  8;  // Bits 31:24
    UINT32 REGFBARHigh                             :  10;  // Bits 41:32
  } Bits;
  UINT64 Data;
  UINT32 Data32[2];
  UINT16 Data16[4];
  UINT8  Data8[8];
} REGBAR_IMPH_STRUCT;
typedef union {
  struct {
    UINT32 DPRACK                                  :  1;  // Bits 0:0
    UINT32 RSVD                                    :  31;  // Bits 31:1
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} GUNITACK_IMPH_STRUCT;

typedef GUNITACK_IMPH_STRUCT DIPACK_IMPH_STRUCT;
typedef union {
  struct {
    UINT32 THERMAL_MONITOR_STATUS                  :  1;  // Bits 0:0
    UINT32 THERMAL_MONITOR_LOG                     :  1;  // Bits 1:1
    UINT32 PROCHOT_STATUS                          :  1;  // Bits 2:2
    UINT32 PROCHOT_LOG                             :  1;  // Bits 3:3
    UINT32 OUT_OF_SPEC_STATUS                      :  1;  // Bits 4:4
    UINT32 OUT_OF_SPEC_LOG                         :  1;  // Bits 5:5
    UINT32 THRESHOLD1_STATUS                       :  1;  // Bits 6:6
    UINT32 THRESHOLD1_LOG                          :  1;  // Bits 7:7
    UINT32 THRESHOLD2_STATUS                       :  1;  // Bits 8:8
    UINT32 THRESHOLD2_LOG                          :  1;  // Bits 9:9
    UINT32 POWER_LIMITATION_STATUS                 :  1;  // Bits 10:10
    UINT32 POWER_LIMITATION_LOG                    :  1;  // Bits 11:11
    UINT32                                         :  4;  // Bits 15:12
    UINT32 Temperature                             :  7;  // Bits 22:16
    UINT32                                         :  4;  // Bits 26:23
    UINT32 Resolution                              :  4;  // Bits 30:27
    UINT32 Valid                                   :  1;  // Bits 31:31
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} PKG_THERM_CAMARILLO_STATUS_IMPH_STRUCT;
typedef union {
  struct {
    UINT32                                         :  8;  // Bits 7:0
    UINT32 THRESHOLD1_STATUS                       :  1;  // Bits 8:8
    UINT32 THRESHOLD1_LOG                          :  1;  // Bits 9:9
    UINT32 THRESHOLD2_STATUS                       :  1;  // Bits 10:10
    UINT32 THRESHOLD2_LOG                          :  1;  // Bits 11:11
    UINT32                                         :  20;  // Bits 31:12
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} DDR_THERM_CAMARILLO_STATUS_IMPH_STRUCT;
typedef union {
  struct {
    UINT32 RESERVED1                               :  6;  // Bits 5:0
    UINT32 HASH_MASK                               :  14;  // Bits 19:6
    UINT32 RESERVED                                :  10;  // Bits 29:20
    UINT32 IDP_HASH_DISABLE                        :  1;  // Bits 30:30
    UINT32 CMF_HASH_DISABLE                        :  1;  // Bits 31:31
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} IOP_HASH_CONFIG_IMPH_STRUCT;
typedef union {
  struct {
    UINT32 SEL_2LM_1LM                             :  1;  // Bits 0:0
    UINT32 SEL_TMECC_ON                            :  1;  // Bits 1:1
    UINT32 Reserved_0                              :  6;  // Bits 7:2
    UINT32 SLICE_0_DISABLE                         :  1;  // Bits 8:8
    UINT32 SLICE_1_DISABLE                         :  1;  // Bits 9:9
    UINT32 Reserved_1                              :  22;  // Bits 31:10
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} CMF_TOPOLOGY_GLOBAL_CFG_0_MCHBAR_IMPH_STRUCT;
typedef union {
  struct {
    UINT32 Reserved_0                              :  20;  // Bits 19:0
    UINT32 REMAPBASELow                            :  12;  // Bits 31:20
    UINT32 REMAPBASEHigh                           :  10;  // Bits 41:32
    UINT32 Reserved_1                              :  22;  // Bits 63:42
  } Bits;
  UINT64 Data;
  UINT32 Data32[2];
  UINT16 Data16[4];
  UINT8  Data8[8];
} REMAP_BASE_MCHBAR_IMPH_STRUCT;
typedef union {
  struct {
    UINT32 Reserved_0                              :  20;  // Bits 19:0
    UINT32 REMAPLMTLow                             :  12;  // Bits 31:20
    UINT32 REMAPLMTHigh                            :  10;  // Bits 41:32
    UINT32 Reserved_1                              :  22;  // Bits 63:42
  } Bits;
  UINT64 Data;
  UINT32 Data32[2];
  UINT16 Data16[4];
  UINT8  Data8[8];
} REMAP_LIMIT_MCHBAR_IMPH_STRUCT;
typedef union {
  struct {
    UINT32 MC_org                                  :  4;  // Bits 3:0
    UINT32 Reserved_0                              :  2;  // Bits 5:4
    UINT32 HASH_MASK                               :  14;  // Bits 19:6
    UINT32 Reserved_1                              :  4;  // Bits 23:20
    UINT32 HASH_LSB_MASK_BIT                       :  3;  // Bits 26:24
    UINT32 Reserved_2                              :  1;  // Bits 27:27
    UINT32 L_shape_dec_mask                        :  4;  // Bits 31:28
    UINT32 L_shape_boundary                        :  4;  // Bits 35:32
    UINT32 Reserved_3                              :  28;  // Bits 63:36
  } Bits;
  UINT64 Data;
  UINT32 Data32[2];
  UINT16 Data16[4];
  UINT8  Data8[8];
} MEMORY_SLICE_HASH_MCHBAR_IMPH_STRUCT;
typedef union {
  struct {
    UINT32 Reserved_0                              :  4;  // Bits 3:0
    UINT32 MS_L_MAP                                :  1;  // Bits 4:4
    UINT32 Reserved_1                              :  3;  // Bits 7:5
    UINT32 STKD_MODE                               :  1;  // Bits 8:8
    UINT32 STKD_MODE_MS1                           :  1;  // Bits 9:9
    UINT32 Reserved_2                              :  2;  // Bits 11:10
    UINT32 MS_S_SIZE                               :  11;  // Bits 22:12
    UINT32 Reserved_3                              :  1;  // Bits 23:23
    UINT32 STKD_MODE_MS_BITS                       :  4;  // Bits 27:24
    UINT32 Reserved_4                              :  4;  // Bits 31:28
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} MAD_SLICE_MCHBAR_IMPH_STRUCT;
typedef union {
  struct {
    UINT32 PersistentSpace                         :  24;  // Bits 23:0
    UINT32 reserved                                :  7;  // Bits 30:24
    UINT32 EN_1LM_PMEM                             :  1;  // Bits 31:31
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} PMEM_1LM_MCHBAR_IMPH_STRUCT;
typedef union {
  struct {
    UINT32 VC1PARKLIM                              :  3;  // Bits 2:0
    UINT32                                         :  1;  // Bits 3:3
    UINT32 NON_VC1PARKLIM                          :  3;  // Bits 6:4
    UINT32                                         :  1;  // Bits 7:7
    UINT32 VTDPARKCNTEN                            :  1;  // Bits 8:8
    UINT32                                         :  23;  // Bits 31:9
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} XTM_DEF_VTD1_ARB_IPC_IMPH_STRUCT;
typedef union {
  struct {
    UINT32 LOCK                                    :  1;  // Bits 0:0
    UINT32                                         :  31;  // Bits 31:1
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} VTDTRKLCK_IMPH_STRUCT;
typedef union {
  struct {
    UINT32 SAI_MASKLow                             :  32;  // Bits 31:0
    UINT32 SAI_MASKHigh                            :  32;  // Bits 63:32
  } Bits;
  UINT64 Data;
  UINT32 Data32[2];
  UINT16 Data16[4];
  UINT8  Data8[8];
} CP_RTCMPLX_VTDBYPASS_IMPH_STRUCT;
typedef union {
  struct {
    UINT32 IPUVTD_EN                               :  1;  // Bits 0:0
    UINT32 RSVD                                    :  11;  // Bits 11:1
    UINT32 IPUVTDBAR_LOW                           :  20;  // Bits 31:12
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} IPUVTDBAR_LOW_IMPH_STRUCT;
typedef union {
  struct {
    UINT32 IPUVTDBAR_HIGH                          :  10;  // Bits 9:0
    UINT32 RSVD                                    :  22;  // Bits 31:10
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} IPUVTDBAR_HIGH_IMPH_STRUCT;
typedef union {
  struct {
    UINT32 PCIE_VTDBAREn                           :  1;  // Bits 0:0
    UINT32                                         :  11;  // Bits 11:1
    UINT32 PCIE_VTDBAR_LOW                         :  20;  // Bits 31:12
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} PCIE0VTDBAR_LOW_IMPH_STRUCT;
typedef union {
  struct {
    UINT32 PCIE_VTDBAR_HIGH                        :  10;  // Bits 9:0
    UINT32                                         :  22;  // Bits 31:10
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} PCIE0VTDBAR_HIGH_IMPH_STRUCT;

typedef PCIE0VTDBAR_LOW_IMPH_STRUCT PCIE1VTDBAR_LOW_IMPH_STRUCT;

typedef PCIE0VTDBAR_HIGH_IMPH_STRUCT PCIE1VTDBAR_HIGH_IMPH_STRUCT;

typedef PCIE0VTDBAR_LOW_IMPH_STRUCT PCIE2VTDBAR_LOW_IMPH_STRUCT;

typedef PCIE0VTDBAR_HIGH_IMPH_STRUCT PCIE2VTDBAR_HIGH_IMPH_STRUCT;

typedef PCIE0VTDBAR_LOW_IMPH_STRUCT PCIE3VTDBAR_LOW_IMPH_STRUCT;

typedef PCIE0VTDBAR_HIGH_IMPH_STRUCT PCIE3VTDBAR_HIGH_IMPH_STRUCT;

typedef PCIE0VTDBAR_LOW_IMPH_STRUCT PCIE4VTDBAR_LOW_IMPH_STRUCT;

typedef PCIE0VTDBAR_HIGH_IMPH_STRUCT PCIE4VTDBAR_HIGH_IMPH_STRUCT;

typedef PCIE0VTDBAR_LOW_IMPH_STRUCT PCIE5VTDBAR_LOW_IMPH_STRUCT;

typedef PCIE0VTDBAR_HIGH_IMPH_STRUCT PCIE5VTDBAR_HIGH_IMPH_STRUCT;

#pragma pack(pop)
#endif
