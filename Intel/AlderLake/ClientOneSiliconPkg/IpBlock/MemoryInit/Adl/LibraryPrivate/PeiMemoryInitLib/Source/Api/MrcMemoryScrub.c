/** @file
  This file contains the memory scrubbing and alias checking functions.

@copyright
  INTEL CONFIDENTIAL
  Copyright 1999 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains an 'Intel Peripheral Driver' and is uniquely identified as
  "Intel Reference Module" and is licensed for Intel CPUs and chipsets under
  the terms of your license agreement with Intel or your vendor. This file may
  be modified by the user, subject to additional terms of the license agreement.

@par Specification Reference:
**/

#include "MrcTypes.h"
#include "MrcApi.h"
#include "MrcMemoryScrub.h"
#include "MrcCpgcApi.h"
#include "Cpgc20TestCtl.h"
#include "MrcSpdProcessing.h"
#include "MrcMcConfiguration.h"
#include "MrcGeneral.h"


/**
  Zero out all of the memory.
  This function is used in the following cases:
  - ECC is enabled (to initialize ECC logic)
  - TXT library is asking to scrub the memory (instead of SCLEAN ACM)
  - Platform code is asking to scrub the memory due to MOR bit being set (Memory Override Request)

  @param[in] MrcData - Include all MRC global data.

  @retval mrcSuccess if the clean succeeded, otherwise an error status.
**/
MrcStatus
MrcEccClean (
  IN     MrcParameters *const MrcData
  )
{
  const MrcInput        *Inputs;
  MrcDebug              *Debug;
  const MRC_FUNCTION    *MrcCall;
  MrcOutput             *Outputs;
  MrcControllerOut      *ControllerOut;
  MrcStatus             Status;
  INT64                 GetSetVal;
  UINT32                Offset;
  UINT32                tRFC;
  UINT8                 Rank;
  UINT8                 IpChannel;
  UINT8                 Channel;
  UINT8                 Controller;
  UINT8                 MaxChannels;
  UINT8                 McChBitMask;
  UINT8                 RankFeatureEnable;
  UINT8                 McChError;
  BOOLEAN               EccEnabled;
  BOOLEAN               Ibecc;
  BOOLEAN               TxtClean;
  BOOLEAN               CleanMemory;
  BOOLEAN               WarmReset;
  BOOLEAN               Lpddr;
  MC0_CH0_CR_SC_PCIT_STRUCT ScPcit;
  MC0_CH0_CR_SC_PCIT_STRUCT ScPcitSave[MAX_CONTROLLER][MAX_CHANNEL];
  MC0_CH0_CR_DQS_OSCILLATOR_PARAMS_STRUCT Lp4DqsOsclParams;
  MC0_CH0_CR_DQS_OSCILLATOR_PARAMS_STRUCT Lp4DqsOsclParamsOrg[MAX_CONTROLLER][MAX_CHANNEL];
  DDRSCRAM_CR_DDRMISCCONTROL2_STRUCT      DdrMiscControl2;
  DDRSCRAM_CR_DDRMISCCONTROL2_STRUCT      DdrMiscControl2Org;

#ifdef MRC_DEBUG_PRINT
  static const char *SourceStr[4] = { "ECC", "CleanMemory", "TXT", "IBECC" };
#endif

  Inputs      = &MrcData->Inputs;
  MrcCall     = Inputs->Call.Func;
  Outputs     = &MrcData->Outputs;
  Debug       = &Outputs->Debug;
  Status      = mrcSuccess;
  EccEnabled  = (Outputs->EccSupport == TRUE);
  Lpddr       = Outputs->Lpddr;
  tRFC        = 0;
  TxtClean    = Inputs->TxtClean;
  CleanMemory = (BOOLEAN) Inputs->CleanMemory;
  Ibecc       = (BOOLEAN) ((Inputs->Ibecc == TRUE) && ((Inputs->IbeccOperationMode == IbeccModeProtectedRanges) || (Inputs->IbeccOperationMode == IbeccModeProtectedAll)));
  WarmReset   = (Inputs->BootMode == bmWarm);
  McChError   = 0;
  MaxChannels = Outputs->MaxChannels;
  if (!(((Ibecc || EccEnabled || CleanMemory) && (!WarmReset)) || TxtClean)) {
    // Memory clean is not needed if none of the (Ibecc || EccEnabled || CleanMemory || TxtClean) bit is set
    // In WarmRest, only TxtClean needs to preform this EccClean function, not required for EccEnabled or CleanMemory
    return mrcSuccess;
  }
  if (WarmReset) {
    // If it's a Warm reset, then we are here due to TxtClean.
    // It is not possible to go to CPGC mode on a Warm flow without going to Normal mode first.
    // Hence we cannot use CPGC to do the scrubbing.
    // Invoke a power cycle reset, and we will run the scrubbing in the subsequent Fast boot.
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "TxtClean - doing power cycle to run scrubbing on the next MRC flow..\n");
    MrcCall->MrcIoWrite8 (0xCF9, 0xE);
    MRC_DEADLOOP ();
  }

  MrcCkeOnProgramming (MrcData);

  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Scrubbing Memory due to %s\n", EccEnabled ? SourceStr[0] : (Ibecc ? SourceStr[3] : (TxtClean ? SourceStr[2] : SourceStr[1])));

  for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
    ControllerOut = &Outputs->Controller[Controller];
    for (Channel = 0; Channel < MaxChannels; Channel++) {
      if (!(MrcChannelExist (MrcData, Controller, Channel)) || IS_MC_SUB_CH (Lpddr, Channel)) {
        continue;
      }
      MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "MC%d.CH%d\n", Controller, Channel);
      tRFC = ControllerOut->Channel[Channel].Timing[Inputs->MemoryProfile].tRFC;
      IpChannel = LP_IP_CH (Lpddr, Channel);
      if (Lpddr) {
        // Disable the DQS Osillator for LP4.
        Offset = OFFSET_CALC_MC_CH (MC0_CH0_CR_DQS_OSCILLATOR_PARAMS_REG, MC1_CH0_CR_DQS_OSCILLATOR_PARAMS_REG, Controller, MC0_CH1_CR_DQS_OSCILLATOR_PARAMS_REG, IpChannel);
        Lp4DqsOsclParams.Data = MrcReadCR (MrcData, Offset);
        Lp4DqsOsclParamsOrg[Controller][IpChannel].Data = Lp4DqsOsclParams.Data;
        Lp4DqsOsclParams.Bits.DQSOSCL_PERIOD = 0;
        Lp4DqsOsclParams.Bits.DIS_SRX_DQSOSCL = 1;
        MrcWriteCR (MrcData, Offset, Lp4DqsOsclParams.Data);
      }

      // Change PCIT to 0xFF
      Offset = OFFSET_CALC_MC_CH (MC0_CH0_CR_SC_PCIT_REG, MC1_CH0_CR_SC_PCIT_REG, Controller, MC0_CH1_CR_SC_PCIT_REG, IpChannel);
      ScPcit.Data = MrcReadCR (MrcData, Offset);
      ScPcitSave[Controller][IpChannel] = ScPcit;
      ScPcit.Bits.PCIT_SUBCH0 = MC0_CH0_CR_SC_PCIT_PCIT_SUBCH0_MAX;
      ScPcit.Bits.PCIT_SUBCH1 = MC0_CH0_CR_SC_PCIT_PCIT_SUBCH1_MAX;
      MrcWriteCR (MrcData, Offset, ScPcit.Data);
    }
  }

  DdrMiscControl2Org.Data = 0;
  if (Lpddr) {
    // Disable DqDqs Retraining feature
    DdrMiscControl2.Data = MrcReadCR (MrcData, DDRSCRAM_CR_DDRMISCCONTROL2_REG);
    DdrMiscControl2Org.Data = DdrMiscControl2.Data;
    if (DdrMiscControl2Org.Bits.lpdeltadqstrainmode == 1) {
      DdrMiscControl2.Bits.lpdeltadqstrainmode = 0;
      MrcWriteCR (MrcData, DDRSCRAM_CR_DDRMISCCONTROL2_REG, DdrMiscControl2.Data);
    }
  }

  // Enable refreshes on MC before we start ECC scrubbing.
  // Refresh must be kept enabled from this point on, otherwise we might loose the ECC initialization.
  GetSetVal = 1;
  MrcGetSetMc (MrcData, MAX_CONTROLLER, GsmMccEnableRefresh, WriteNoCache, &GetSetVal);
  // Scrub per rank, on both channels in parallel
  for (Rank = 0; Rank < MAX_RANK_IN_CHANNEL; Rank++) {
    if (((1 << Rank) & Outputs->ValidRankMask) == 0) {
      continue;
    }
    // Determine the Active Channels
    McChBitMask = 0;
    for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
      for (Channel = 0; Channel < MaxChannels; Channel++) {
        RankFeatureEnable = 0x02;  // Enable Refresh during CPGC test
        McChBitMask |= SelectReutRanks (MrcData, Controller, Channel, 1 << Rank, FALSE, RankFeatureEnable);
      }
    }

    SetupIOTestScrub (MrcData, McChBitMask);

    // Update the Bank/Row/Col sizes per current rank population
    Cpgc20AddressSetupScrub (MrcData, Rank);

    // Run the test on both channels, don't check for errors - this is WR only test
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Scrubbing Rank%d: McChBitMask = 0x%X\n", Rank, McChBitMask);
    McChError = RunIOTest (MrcData, McChBitMask, Outputs->DQPat, 1, 0);

    // Wait for MC to drain - CPGC test done indication comes earlier that all the WR's come out
    MrcWait (MrcData, ((Outputs->tCKps * 64 * Outputs->BurstLength) / 1000) * MRC_TIMER_1NS);
    MrcWait (MrcData, ((Outputs->tCKps * tRFC) / 1000) * MRC_TIMER_1NS);
  } // for Rank

  if (TxtClean) {
    // Perform Memory Read Test to Check Zeros per rank, on both channels in parallel
    McChError = 0;
    for (Rank = 0; Rank < MAX_RANK_IN_CHANNEL; Rank++) {
      if (((1 << Rank) & Outputs->ValidRankMask) == 0) {
        continue;
      }
      // Determine the Active Channels
      McChBitMask = 0;
      for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
        for (Channel = 0; Channel < MaxChannels; Channel++) {
          RankFeatureEnable = 0x02;  // Enable Refresh during CPGC test
          McChBitMask |= SelectReutRanks (MrcData, Controller, Channel, 1 << Rank, FALSE, RankFeatureEnable);
        }
      }

      SetupIOTestCpgcRead (MrcData, McChBitMask, 4, NSOE, 0x0);  //RdDataPtn = 0x00000000

      // Update the Bank/Row/Col sizes per current rank population
      Cpgc20AddressSetupScrub (MrcData, Rank);

      // Run the test on both channels, to check if it is all zeros
      MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Checking Zeros on Rank%d: McChBitMask = 0x%X\n", Rank, McChBitMask);
      McChError |= RunIOTest (MrcData, McChBitMask, Outputs->DQPat, 1, 0);

    } // for Rank
  } // for TxtClean

  if (Lpddr && (DdrMiscControl2Org.Bits.lpdeltadqstrainmode == 1)) {
    // Restore DqDqs Retraining value
    MrcWriteCR (MrcData, DDRSCRAM_CR_DDRMISCCONTROL2_REG, DdrMiscControl2Org.Data);
  }

  for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
    for (Channel = 0; Channel < MaxChannels; Channel++) {
      if (!(MrcChannelExist (MrcData, Controller, Channel)) || IS_MC_SUB_CH (Lpddr, Channel)) {
        continue;
      }
      IpChannel = LP_IP_CH (Lpddr, Channel);
      // Restore PCIT value
      Offset = OFFSET_CALC_MC_CH (MC0_CH0_CR_SC_PCIT_REG, MC1_CH0_CR_SC_PCIT_REG, Controller, MC0_CH1_CR_SC_PCIT_REG, IpChannel);
      MrcWriteCR (MrcData, Offset, ScPcitSave[Controller][IpChannel].Data);

      if (Lpddr) {
        // Restore the DQS Oscillator value for LP4.
        Offset = OFFSET_CALC_MC_CH (MC0_CH0_CR_DQS_OSCILLATOR_PARAMS_REG, MC1_CH0_CR_DQS_OSCILLATOR_PARAMS_REG, Controller, MC0_CH1_CR_DQS_OSCILLATOR_PARAMS_REG, IpChannel);
        MrcWriteCR (MrcData, Offset, Lp4DqsOsclParamsOrg[Controller][IpChannel].Data);
      }
    }
  }

  // Check the scrubbing result
  if (McChError != 0) {
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_WARNING, "%s: Memory is not Clean after Scrubbing, McChError = 0x%X\n", gWarnString, McChError);
  }

  if (TxtClean) {
    if (McChError == 0) {
      MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Memory is Clean, McChError = 0x%X, Calling TxtLib ClearSecretsBit()\n", McChError);
      // Go to Normal mode
      MrcSetNormalMode (MrcData, TRUE);
      McRegistersLock (MrcData);
      for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
        if (MrcControllerExist (MrcData, Controller) || (Controller == 0)) {
          MrcGetSetMc (MrcData, Controller, GsmMccMrcDone, WriteNoCache, &GetSetVal);
        }
      }
      MrcCall->MrcClearSecretsBit ();     // Call TxtLib ClearSecretsBit();
    } else { //Failed on scrub
      MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Calling TxtLib SetPowerDownRequest()\n");
      MrcCall->MrcSetPowerDownRequest (MRC_POWER_DOWN_REQUEST);    // Call TxtLib SetPowerDownRequest();
    }
  }

  return Status;
}

/**
  This function performs a memory alias check.

  @param[in] MrcData - The global host structure

  @retval mrcSuccess or error value.
**/
MrcStatus
MrcAliasCheck (
  IN OUT MrcParameters *const MrcData
)
{
  const MRC_FUNCTION            *MrcCall;
  MrcDebug                      *Debug;
  MrcInput                      *Inputs;
  MrcOutput                     *Outputs;
  MrcControllerOut              *ControllerOut;
  MrcDimmOut                    *DimmOut;
  MrcStatus                     Status;
  INT64                         RefreshSave[MAX_CONTROLLER];
  INT64                         GetSetVal;
  UINT32                        SdramAddressingCapacity;
  UINT32                        CrOffset;
  UINT32                        SdramCapacity;
  UINT8                         Rank;
  UINT8                         RankToDimm;
  UINT8                         Controller;
  UINT8                         Channel;
  UINT8                         IpChannel;
  UINT8                         MaxChannels;
  UINT8                         ActiveChBitMask;
  BOOLEAN                       InvalidSpdAddressingCapacity;
  MRC_REUTAddress               ReutAddress;
  MC0_MAD_INTRA_CH0_STRUCT      MadIntraOrig[MAX_CONTROLLER][MAX_CHANNEL];
  MC0_MAD_INTRA_CH0_STRUCT      MadIntra;
  BOOLEAN                       Lpddr;
  MC0_CH0_CR_SC_PCIT_STRUCT     ScPcit;
  // INT64                         IocEccEn;
  MC0_CH0_CR_SC_PCIT_STRUCT     ScPcitOrg[MAX_CONTROLLER][MAX_CHANNEL];
  MC0_CH0_CR_DQS_OSCILLATOR_PARAMS_STRUCT   Lp4DqsOsclParamsOrg[MAX_CONTROLLER][MAX_CHANNEL];
  MC0_CH0_CR_DQS_OSCILLATOR_PARAMS_STRUCT   Lp4DqsOsclParams;
  DDRSCRAM_CR_DDRMISCCONTROL2_STRUCT        DdrMiscControl2;
  DDRSCRAM_CR_DDRMISCCONTROL2_STRUCT        DdrMiscControl2Org;

  Outputs                      = &MrcData->Outputs;
  Inputs                       = &MrcData->Inputs;
  MrcCall                      = Inputs->Call.Func;
  Debug                        = &Outputs->Debug;
  Status                       = mrcSuccess;
  Lpddr                        = Outputs->Lpddr;
  InvalidSpdAddressingCapacity = FALSE;
  MaxChannels                  = Outputs->MaxChannels;

  // Check to see if the SDRAM Addressing * Primary Bus Width == SDRAM capacity.
  // If not, report an alias and exit.
  for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
    ControllerOut = &Outputs->Controller[Controller];
    for (Channel = 0; Channel < MaxChannels; Channel++) {
      for (Rank = 0; Rank < MAX_RANK_IN_CHANNEL; Rank += MAX_RANK_IN_DIMM) {
        if (MrcRankExist (MrcData, Controller, Channel, Rank)) {
          RankToDimm = RANK_TO_DIMM_NUMBER (Rank);
          DimmOut = &ControllerOut->Channel[Channel].Dimm[RankToDimm];
          SdramAddressingCapacity = (DimmOut->ColumnSize * DimmOut->RowSize);
          // Since the minimum number of row and column bits are 12 and 9 respectively,
          // we can shift by 20 to get the result in Mb before multiplying by the bus width.
          SdramAddressingCapacity = SdramAddressingCapacity >> 20;
          SdramAddressingCapacity *= DimmOut->Banks;
          SdramAddressingCapacity *= DimmOut->BankGroups;
          SdramAddressingCapacity *= DimmOut->SdramWidth;
          if (Lpddr) {
            // SPD Density is per die, but SdramWidth is per LP4 channel, so need to multiple by number of channels per die
            SdramAddressingCapacity *= DimmOut->ChannelsPerSdramPackage;
          }
          SdramCapacity = SdramCapacityTable[DimmOut->DensityIndex] * 8;
          if (SdramCapacity != SdramAddressingCapacity) {
            InvalidSpdAddressingCapacity = TRUE;
            MRC_DEBUG_MSG (
              Debug,
              MSG_LEVEL_ERROR,
              "ERROR: MC %d Channel %d Dimm %d SPD SDRAM Adressing Capacity(0x%xMb) does not match SDRAM Capacity(0x%xMb)\nPlease verify:\n",
              Controller,
              Channel,
              RankToDimm,
              SdramAddressingCapacity,
              SdramCapacity
            );
            MRC_DEBUG_MSG (
              Debug,
              MSG_LEVEL_ERROR,
              " Capacity: 0x%x\n RowSize: 0x%x\n ColumnSize: 0x%x\n Banks: 0x%x\n Bank Groups: 0x%x\n Device Width: 0x%x\n",
              SdramCapacity,
              DimmOut->RowSize,
              DimmOut->ColumnSize,
              DimmOut->Banks,
              DimmOut->BankGroups,
              DimmOut->SdramWidth
            );
            break;
          }
        }
      }
    }
  }
  //
  // Since we will not hang the system, signal that an Alias could exist and return mrcSuccess.
  //
  if (TRUE == InvalidSpdAddressingCapacity) {
    Outputs->SpdSecurityStatus = MrcSpdStatusAliased;
    MRC_DEBUG_ASSERT (
      (InvalidSpdAddressingCapacity == FALSE),
      Debug,
      "...Memory Alias detected - Invalid Spd Addressing Capacity...\n"
    );
    return Status;
  }

  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Performing Alias Test\n");
  MrcCall->MrcSetMem ((UINT8 *) &ReutAddress, sizeof (ReutAddress), 0);


  for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
    if (MrcControllerExist (MrcData, Controller)) {
      for (Channel = 0; Channel < MaxChannels; Channel++) {
        if (!(MrcChannelExist (MrcData, Controller, Channel)) || IS_MC_SUB_CH (Lpddr, Channel)) {
          continue;
        }
        IpChannel = LP_IP_CH (Lpddr, Channel);
        // Determine if we are ECC enabled.  If so, disable ECC since the ECC scrub has yet to occur.
        if (Outputs->EccSupport == TRUE) {
          MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "ECC enabled.  Disabling ECC for the test.  Must scrub after this!!!\n");
          CrOffset = OFFSET_CALC_MC_CH (MC0_MAD_INTRA_CH0_REG, MC1_MAD_INTRA_CH0_REG, Controller, MC0_MAD_INTRA_CH1_REG, IpChannel);
          MadIntraOrig[Controller][IpChannel].Data = MrcReadCR (MrcData, CrOffset);
          MadIntra.Data = MadIntraOrig[Controller][IpChannel].Data;
          MadIntra.Bits.ECC = emNoEcc;
          MrcWriteCR (MrcData, CrOffset, MadIntra.Data);
        }
        if (Lpddr) {
          // Disable the DQS Osillator for LP4.
          CrOffset = OFFSET_CALC_MC_CH (MC0_CH0_CR_DQS_OSCILLATOR_PARAMS_REG, MC1_CH0_CR_DQS_OSCILLATOR_PARAMS_REG, Controller, MC0_CH1_CR_DQS_OSCILLATOR_PARAMS_REG, IpChannel);
          Lp4DqsOsclParams.Data = MrcReadCR (MrcData, CrOffset);
          Lp4DqsOsclParamsOrg[Controller][IpChannel].Data = Lp4DqsOsclParams.Data;
          Lp4DqsOsclParams.Bits.DQSOSCL_PERIOD = 0;
          Lp4DqsOsclParams.Bits.DIS_SRX_DQSOSCL = 1;
          MrcWriteCR (MrcData, CrOffset, Lp4DqsOsclParams.Data);
        }
        // Change PCIT to 0xFF
        CrOffset = OFFSET_CALC_MC_CH (MC0_CH0_CR_SC_PCIT_REG, MC1_CH0_CR_SC_PCIT_REG, Controller, MC0_CH1_CR_SC_PCIT_REG, IpChannel);
        ScPcit.Data = MrcReadCR (MrcData, CrOffset);
        ScPcitOrg[Controller][IpChannel] = ScPcit;
        ScPcit.Bits.PCIT_SUBCH0 = MC0_CH0_CR_SC_PCIT_PCIT_SUBCH0_MAX;
        ScPcit.Bits.PCIT_SUBCH1 = MC0_CH0_CR_SC_PCIT_PCIT_SUBCH1_MAX;
        MrcWriteCR (MrcData, CrOffset, ScPcit.Data);
      }
      // Enable Refreshes. Save previous state.
      MrcGetSetMc (MrcData, Controller, GsmMccEnableRefresh, ReadNoCache, &RefreshSave[Controller]);
      GetSetVal = 1;
      MrcGetSetMc (MrcData, Controller, GsmMccEnableRefresh, WriteNoCache, &GetSetVal);
    }
  }
  // @todo No TGL version found
  /*if (Outputs->EccSupport) {
  MrcGetSetDdrIoGroupController0 (MrcData, GsmIocEccEn, ReadFromCache, &IocEccEn);
  GetSetVal = 0;
  MrcGetSetDdrIoGroupController0 (MrcData, GsmIocEccEn, ForceWriteCached, &GetSetVal);
  }*/
  DdrMiscControl2Org.Data = 0;
  if (Lpddr) {
    // Disable DqDqs Retraining feature
    CrOffset = DDRSCRAM_CR_DDRMISCCONTROL2_REG;
    DdrMiscControl2.Data = MrcReadCR (MrcData, CrOffset);
    DdrMiscControl2Org.Data = DdrMiscControl2.Data;
    if (DdrMiscControl2Org.Bits.lpdeltadqstrainmode == 1) {
      DdrMiscControl2.Bits.lpdeltadqstrainmode = 0;
      MrcWriteCR (MrcData, CrOffset, DdrMiscControl2.Data);
    }
  }

  // Run test Per Dimm
  //
  for (Rank = 0; Rank < MAX_RANK_IN_CHANNEL; Rank += MAX_RANK_IN_DIMM) {
    if ((MRC_BIT0 << Rank) & Outputs->ValidRankMask) {
      MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Testing Dimm %d\n", Rank / 2);
      //@todo      MrcMemoryCheckSetup (MrcData, &ReutAddress, Rank, 0, &ReutUninitialized);
      //
      // Determine Active Channels
      //
      ActiveChBitMask = 0;
      for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
        for (Channel = 0; Channel < MAX_CHANNEL; Channel++) {
          ActiveChBitMask |= SelectReutRanks (MrcData, Controller, Channel, MRC_BIT0 << Rank, FALSE, 0);
        }
      }

      //
      // Run the test
      //
      //@todo      Status = MrcRunMemoryScrub (MrcData, ActiveChBitMask, TRUE);
      if (Status != mrcSuccess) {
        break;
      }
    }
  }

  if (Lpddr && (DdrMiscControl2Org.Bits.lpdeltadqstrainmode == 1)) {
    // Restore DqDqs Retraining value
    CrOffset = DDRSCRAM_CR_DDRMISCCONTROL2_REG;
    MrcWriteCR (MrcData, CrOffset, DdrMiscControl2Org.Data);
  }
  // @todo No TGL version found
  /*if (Outputs->EccSupport) {
  MrcGetSetDdrIoGroupController0 (MrcData, GsmIocEccEn, ForceWriteCached, &IocEccEn);
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "ReEnabling ECC Logic.  Must scrub after this!\n");
  }*/

  for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
    if (MrcControllerExist (MrcData, Controller)) {
      for (Channel = 0; Channel < MaxChannels; Channel++) {
        if (!(MrcChannelExist (MrcData, Controller, Channel)) || IS_MC_SUB_CH (Lpddr, Channel)) {
          continue;
        }
        IpChannel = LP_IP_CH (Lpddr, Channel);
        // Restore PCIT value
        CrOffset = OFFSET_CALC_MC_CH (MC0_CH0_CR_SC_PCIT_REG, MC1_CH0_CR_SC_PCIT_REG, Controller, MC0_CH1_CR_SC_PCIT_REG, IpChannel);
        MrcWriteCR (MrcData, CrOffset, ScPcitOrg[Controller][IpChannel].Data);
        if (Lpddr) {
          // Restore the DQS Oscillator value for LP4.
          CrOffset = OFFSET_CALC_MC_CH (MC0_CH0_CR_DQS_OSCILLATOR_PARAMS_REG, MC1_CH0_CR_DQS_OSCILLATOR_PARAMS_REG, Controller, MC0_CH1_CR_DQS_OSCILLATOR_PARAMS_REG, IpChannel);
          MrcWriteCR (MrcData, CrOffset, Lp4DqsOsclParamsOrg[Controller][IpChannel].Data);
        }
        // ReEnable ECC logic.
        if (Outputs->EccSupport == TRUE) {
          CrOffset = OFFSET_CALC_MC_CH (MC0_MAD_INTRA_CH0_REG, MC1_MAD_INTRA_CH0_REG, Controller, MC0_MAD_INTRA_CH1_REG, IpChannel);
          MrcWriteCR (MrcData, CrOffset, MadIntraOrig[Controller][IpChannel].Data);
        }
      }
      // Restore Refreshes.
      MrcGetSetMc (MrcData, Controller, GsmMccEnableRefresh, WriteNoCache, &RefreshSave[Controller]);
    }
  }

  // Wait 4 usec after enabling the ECC IO, needed by HW
  MrcWait (MrcData, 4 * MRC_TIMER_1US);

  if (mrcSuccess != Status) {
    Outputs->SpdSecurityStatus = MrcSpdStatusAliased;
    MRC_DEBUG_ASSERT (
      (Status == mrcSuccess),
      Debug,
      "%s Alias Detected! See REUT Error above. ***\n Error Status : %x \n", gErrString, Status);
    Status = mrcSuccess;
  }

  return Status;
}

/**
  This function provides MRC core hook to call TXT Alias Check before DRAM Init Done.

  @param[in]  MrcData - Pointer to global MRC structure.

  @retval mrcSuccess.
**/
MrcStatus
MrcTxtAliasCheck (
  IN MrcParameters *const MrcData
  )
{
  MrcData->Inputs.Call.Func->MrcTxtAcheck ();
  return mrcSuccess;
}
