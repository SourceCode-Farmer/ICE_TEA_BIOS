#ifndef __MrcMcRegisterStructAdl0xxx_h__
#define __MrcMcRegisterStructAdl0xxx_h__
/** @file
  This file was automatically generated. Modify at your own risk.
  Note that no error checking is done in these functions so ensure that the correct values are passed.

@copyright
  Copyright (c) 2010 - 2021 Intel Corporation. All rights reserved
  This software and associated documentation (if any) is furnished
  under a license and may only be used or copied in accordance
  with the terms of the license. Except as permitted by the
  license, no part of this software or documentation may be
  reproduced, stored in a retrieval system, or transmitted in any
  form or by any means without the express written consent of
  Intel Corporation.
  This file contains an 'Intel Peripheral Driver' and is uniquely
  identified as "Intel Reference Module" and is licensed for Intel
  CPUs and chipsets under the terms of your license agreement with
  Intel or your vendor. This file may be modified by the user, subject
  to additional terms of the license agreement.

@par Specification Reference:
**/

#pragma pack(push, 1)
typedef union {
  struct {
    UINT32 rxpbddlycodep                           :  8;  // Bits 7:0
    UINT32 rxpbddlycoden                           :  8;  // Bits 15:8
    UINT32 txdqperbitdeskew                        :  8;  // Bits 23:16
    UINT32 txdqperbitdeskew90                      :  8;  // Bits 31:24
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} DATA0CH0_CR_DDRDATADQRANK0LANE0_STRUCT;

typedef DATA0CH0_CR_DDRDATADQRANK0LANE0_STRUCT DATA0CH0_CR_DDRDATADQRANK0LANE1_STRUCT;

typedef DATA0CH0_CR_DDRDATADQRANK0LANE0_STRUCT DATA0CH0_CR_DDRDATADQRANK0LANE2_STRUCT;

typedef DATA0CH0_CR_DDRDATADQRANK0LANE0_STRUCT DATA0CH0_CR_DDRDATADQRANK0LANE3_STRUCT;

typedef DATA0CH0_CR_DDRDATADQRANK0LANE0_STRUCT DATA0CH0_CR_DDRDATADQRANK0LANE4_STRUCT;

typedef DATA0CH0_CR_DDRDATADQRANK0LANE0_STRUCT DATA0CH0_CR_DDRDATADQRANK0LANE5_STRUCT;

typedef DATA0CH0_CR_DDRDATADQRANK0LANE0_STRUCT DATA0CH0_CR_DDRDATADQRANK0LANE6_STRUCT;

typedef DATA0CH0_CR_DDRDATADQRANK0LANE0_STRUCT DATA0CH0_CR_DDRDATADQRANK0LANE7_STRUCT;

typedef DATA0CH0_CR_DDRDATADQRANK0LANE0_STRUCT DATA0CH0_CR_DDRDATADQRANK1LANE0_STRUCT;

typedef DATA0CH0_CR_DDRDATADQRANK0LANE0_STRUCT DATA0CH0_CR_DDRDATADQRANK1LANE1_STRUCT;

typedef DATA0CH0_CR_DDRDATADQRANK0LANE0_STRUCT DATA0CH0_CR_DDRDATADQRANK1LANE2_STRUCT;

typedef DATA0CH0_CR_DDRDATADQRANK0LANE0_STRUCT DATA0CH0_CR_DDRDATADQRANK1LANE3_STRUCT;

typedef DATA0CH0_CR_DDRDATADQRANK0LANE0_STRUCT DATA0CH0_CR_DDRDATADQRANK1LANE4_STRUCT;

typedef DATA0CH0_CR_DDRDATADQRANK0LANE0_STRUCT DATA0CH0_CR_DDRDATADQRANK1LANE5_STRUCT;

typedef DATA0CH0_CR_DDRDATADQRANK0LANE0_STRUCT DATA0CH0_CR_DDRDATADQRANK1LANE6_STRUCT;

typedef DATA0CH0_CR_DDRDATADQRANK0LANE0_STRUCT DATA0CH0_CR_DDRDATADQRANK1LANE7_STRUCT;

typedef DATA0CH0_CR_DDRDATADQRANK0LANE0_STRUCT DATA0CH0_CR_DDRDATADQRANK2LANE0_STRUCT;

typedef DATA0CH0_CR_DDRDATADQRANK0LANE0_STRUCT DATA0CH0_CR_DDRDATADQRANK2LANE1_STRUCT;

typedef DATA0CH0_CR_DDRDATADQRANK0LANE0_STRUCT DATA0CH0_CR_DDRDATADQRANK2LANE2_STRUCT;

typedef DATA0CH0_CR_DDRDATADQRANK0LANE0_STRUCT DATA0CH0_CR_DDRDATADQRANK2LANE3_STRUCT;

typedef DATA0CH0_CR_DDRDATADQRANK0LANE0_STRUCT DATA0CH0_CR_DDRDATADQRANK2LANE4_STRUCT;

typedef DATA0CH0_CR_DDRDATADQRANK0LANE0_STRUCT DATA0CH0_CR_DDRDATADQRANK2LANE5_STRUCT;

typedef DATA0CH0_CR_DDRDATADQRANK0LANE0_STRUCT DATA0CH0_CR_DDRDATADQRANK2LANE6_STRUCT;

typedef DATA0CH0_CR_DDRDATADQRANK0LANE0_STRUCT DATA0CH0_CR_DDRDATADQRANK2LANE7_STRUCT;

typedef DATA0CH0_CR_DDRDATADQRANK0LANE0_STRUCT DATA0CH0_CR_DDRDATADQRANK3LANE0_STRUCT;

typedef DATA0CH0_CR_DDRDATADQRANK0LANE0_STRUCT DATA0CH0_CR_DDRDATADQRANK3LANE1_STRUCT;

typedef DATA0CH0_CR_DDRDATADQRANK0LANE0_STRUCT DATA0CH0_CR_DDRDATADQRANK3LANE2_STRUCT;

typedef DATA0CH0_CR_DDRDATADQRANK0LANE0_STRUCT DATA0CH0_CR_DDRDATADQRANK3LANE3_STRUCT;

typedef DATA0CH0_CR_DDRDATADQRANK0LANE0_STRUCT DATA0CH0_CR_DDRDATADQRANK3LANE4_STRUCT;

typedef DATA0CH0_CR_DDRDATADQRANK0LANE0_STRUCT DATA0CH0_CR_DDRDATADQRANK3LANE5_STRUCT;

typedef DATA0CH0_CR_DDRDATADQRANK0LANE0_STRUCT DATA0CH0_CR_DDRDATADQRANK3LANE6_STRUCT;

typedef DATA0CH0_CR_DDRDATADQRANK0LANE0_STRUCT DATA0CH0_CR_DDRDATADQRANK3LANE7_STRUCT;
typedef union {
  struct {
    UINT32 Reserved0                               :  5;  // Bits 4:0
    UINT32 gear1                                   :  1;  // Bits 5:5
    UINT32 tx_clock_on_with_txanalogen             :  1;  // Bits 6:6
    UINT32 local_gate_d0tx                         :  1;  // Bits 7:7
    UINT32 Gear4                                   :  1;  // Bits 8:8
    UINT32 internalclockson                        :  1;  // Bits 9:9
    UINT32 wllongdelen                             :  1;  // Bits 10:10
    UINT32 txpion                                  :  1;  // Bits 11:11
    UINT32 txon                                    :  1;  // Bits 12:12
    UINT32 txdisable                               :  1;  // Bits 13:13
    UINT32 endqodtparkmode                         :  1;  // Bits 14:14
    UINT32 endqsodtparkmode                        :  1;  // Bits 15:15
    UINT32 biaspmctrl                              :  2;  // Bits 17:16
    UINT32 vrefpmctrl                              :  2;  // Bits 19:18
    UINT32 rxdisable                               :  1;  // Bits 20:20
    UINT32 forcerxon                               :  1;  // Bits 21:21
    UINT32 rxrankmuxdelay_2ndstg                   :  2;  // Bits 23:22
    UINT32 datarst                                 :  1;  // Bits 24:24
    UINT32 odtforceqdrven                          :  1;  // Bits 25:25
    UINT32 odtsampoff                              :  1;  // Bits 26:26
    UINT32 ddrcrforceodton                         :  1;  // Bits 27:27
    UINT32 txrankmuxdelay                          :  4;  // Bits 31:28
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} DATA0CH0_CR_DDRCRDATACONTROL0_STRUCT;
typedef union {
  struct {
    UINT32 safemodeenable                          :  1;  // Bits 0:0
    UINT32 rxrankmuxdelay                          :  4;  // Bits 4:1
    UINT32 dqsodtdelay                             :  6;  // Bits 10:5
    UINT32 dqsodtduration                          :  3;  // Bits 13:11
    UINT32 dqodtdelay                              :  5;  // Bits 18:14
    UINT32 dqodtduration                           :  4;  // Bits 22:19
    UINT32 senseampdelay                           :  5;  // Bits 27:23
    UINT32 senseampduration                        :  4;  // Bits 31:28
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} DATA0CH0_CR_DDRCRDATACONTROL1_STRUCT;
typedef union {
  struct {
    UINT32                                         :  7;  // Bits 6:0
    UINT32 gear1                                   :  1;  // Bits 7:7
    UINT32 wrpreamble                              :  3;  // Bits 10:8
    UINT32 dbimode                                 :  1;  // Bits 11:11
    UINT32 compupdate_ovren                        :  1;  // Bits 12:12
    UINT32 txdqsrankmuxdelay_2nd_stage_offset      :  2;  // Bits 14:13
    UINT32 disabletxdqs                            :  1;  // Bits 15:15
    UINT32 txrankmuxdelay_2nd_stage_offset         :  2;  // Bits 17:16
    UINT32 txdqsrankmuxdelay                       :  4;  // Bits 21:18
    UINT32 rxburstlen                              :  3;  // Bits 24:22
    UINT32 Reserved1                               :  1;  // Bits 25:25
    UINT32 compupdatedone_ovrval                   :  1;  // Bits 26:26
    UINT32 rxclkstgnum                             :  5;  // Bits 31:27
  } BitsA0;
  struct {
    UINT32 datainvertnibble                        :  2;  // Bits 1:0
    INT32  dqsodtcompoffset                        :  5;  // Bits 6:2  // manual edit
    UINT32 wrpreamble                              :  3;  // Bits 9:7
    UINT32 dbimode                                 :  1;  // Bits 10:10
    UINT32 compupdate_ovren                        :  1;  // Bits 11:11
    UINT32 txdqsrankmuxdelay_2nd_stage_offset      :  2;  // Bits 13:12
    UINT32 disabletxdqs                            :  1;  // Bits 14:14
    UINT32 txrankmuxdelay_2nd_stage_offset         :  2;  // Bits 16:15
    UINT32 txdqsrankmuxdelay                       :  4;  // Bits 20:17
    UINT32 rxburstlen                              :  3;  // Bits 23:21
    UINT32 Reserved1                               :  1;  // Bits 24:24
    UINT32 compupdatedone_ovrval                   :  1;  // Bits 25:25
    UINT32 rxclkstgnum                             :  6;  // Bits 31:26
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} DATA0CH0_CR_DDRCRDATACONTROL2_STRUCT;
typedef union {
  struct {
    UINT32 usedefaultrdptrcalc                     :  1;  // Bits 0:0
    UINT32 rstnumpre                               :  3;  // Bits 3:1
    UINT32 rxtogglepreamble                        :  2;  // Bits 5:4
    UINT32 rcvenwaveshape                          :  3;  // Bits 8:6
    UINT32 rxreadpointer                           :  5;  // Bits 13:9
    UINT32 rxvref                                  :  9;  // Bits 22:14
    UINT32 wr1p5tckpostamble_toggle                :  1;  // Bits 23:23
    UINT32 wr1p5tckpostamble_enable                :  1;  // Bits 24:24
    UINT32 commandaddrparityodd                    :  1;  // Bits 25:25
    UINT32 caparitytrain                           :  1;  // Bits 26:26
    UINT32 lpddr5                                  :  1;  // Bits 27:27
    UINT32 lpddr                                   :  1;  // Bits 28:28
    UINT32 rxpion                                  :  1;  // Bits 29:29
    UINT32 dram_rddqsoffset                        :  2;  // Bits 31:30
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} DATA0CH0_CR_DDRCRDATACONTROL3_STRUCT;
typedef union {
  struct {
    UINT32 Reserved2                               :  3;  // Bits 2:0
    UINT32 rxvref_cmn_ddrvrefctrl                  :  9;  // Bits 11:3
    UINT32 rxvref_cmn_selvsxhi                     :  1;  // Bits 12:12
    UINT32 rxvref_cmn_selvddsupply                 :  1;  // Bits 13:13
    UINT32 rxvref_cmn_ddrvrefgenenable             :  1;  // Bits 14:14
    UINT32 rxvref_cmn_ddrlevshftbypasspmos         :  1;  // Bits 15:15
    UINT32 rxvref_cmn_ddrenvrefgenweakres          :  1;  // Bits 16:16
    UINT32 rxbias_cmn_vrefsel                      :  4;  // Bits 20:17
    UINT32 rxbias_cmn_tailctl                      :  2;  // Bits 22:21
    UINT32 rxbias_cmn_biasicomp                    :  4;  // Bits 26:23
    UINT32 rxall_cmn_rxlpddrmode                   :  2;  // Bits 28:27
    UINT32 afeshared_cmn_parkval                   :  1;  // Bits 29:29
    UINT32 afeshared_cmn_iobufact                  :  1;  // Bits 30:30
    UINT32 afeshared_cmn_globalvsshibyp            :  1;  // Bits 31:31
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} DATA0CH0_CR_AFEMISC_STRUCT;
typedef union {
  struct {
    UINT32 dqsdcdsrz_cmn_dcdtailctl                :  2;  // Bits 1:0
    UINT32 dqsdcdsrz_cmn_dcdselsecondaryclk        :  1;  // Bits 2:2
    UINT32 dqdcdsrz_cmn_dcdselsecondaryclk         :  1;  // Bits 3:3
    UINT32 dcdsrz_cmn_gear4en                      :  1;  // Bits 4:4
    UINT32 dcdsrz_cmn_gear2en                      :  1;  // Bits 5:5
    UINT32 dcdsrz_cmn_gear1en                      :  1;  // Bits 6:6
    UINT32 cktop_cmn_bonus                         :  8;  // Bits 14:7
    UINT32 pghub_cmn_bonus                         :  2;  // Bits 16:15
    UINT32 dqsbuf_cmn_bonus                        :  4;  // Bits 20:17
    UINT32 dqbuf_cmn_bonus                         :  2;  // Bits 22:21
    UINT32 imod_func_dqstx_cmn_txclkpden           :  1;  // Bits 23:23
    UINT32 imod_func_dqsdcdsrz_cmn_dcdenvcciog     :  1;  // Bits 24:24
    UINT32 imod_func_dqsdcdsrz_cmn_dcdenrxdqspsal  :  1;  // Bits 25:25
    UINT32 imod_func_dqsdcdsrz_cmn_dcdenrxdqsnsal  :  1;  // Bits 26:26
    UINT32 omod_func_dqstx_cmn_txpfirstonrise      :  2;  // Bits 28:27
    UINT32 omod_func_dqstx_cmn_txpfirstonfall      :  2;  // Bits 30:29
    UINT32 Reserved3                               :  1;  // Bits 31:31
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} DATA0CH0_CR_SRZCTL_STRUCT;
typedef union {
  struct {
    UINT32 dqrx_cmn_rxudfetap4                     :  3;  // Bits 2:0
    UINT32 dqrx_cmn_rxudfetap3                     :  4;  // Bits 6:3
    UINT32 dqrx_cmn_rxudfetap2                     :  5;  // Bits 11:7
    UINT32 dqrx_cmn_rxudfetap1                     :  4;  // Bits 15:12
    UINT32 dqrx_cmn_rxudfe3en                      :  1;  // Bits 16:16
    UINT32 dqrx_cmn_rxudfe2en                      :  1;  // Bits 17:17
    UINT32 dqrx_cmn_rxudfe1en                      :  1;  // Bits 18:18
    UINT32 dqrx_cmn_rxpadmuxtrainmodeen            :  1;  // Bits 19:19
    UINT32 dqrx_cmn_rxpadmuxforceen                :  1;  // Bits 20:20
    UINT32 dqrx_cmn_rxmctleres                     :  2;  // Bits 22:21
    UINT32 dqrx_cmn_rxmctleeq                      :  4;  // Bits 26:23
    UINT32 dqrx_cmn_rxmctlecap                     :  2;  // Bits 28:27
    UINT32 dqrx_cmn_rxdiffampen                    :  1;  // Bits 29:29
    UINT32 dqrx_cmn_rxmdqcben                      :  2;  // Bits 31:30
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} DATA0CH0_CR_DQRXCTL0_STRUCT;
typedef union {
  struct {
    UINT32 dqrx_cmn_rxvsshiffstrobestlegen         :  1;  // Bits 0:0
    UINT32 dqrx_cmn_rxvrefvttmfc                   :  3;  // Bits 3:1
    UINT32 dqrx_cmn_rxvrefvssmfc                   :  3;  // Bits 6:4
    UINT32 dqrx_cmn_rxvrefvdd2gmfc                 :  3;  // Bits 9:7
    UINT32 dqrx_cmn_rxmdqbwsel                     :  6;  // Bits 15:10
    UINT32 dxrx_cmn_rxtrainreset                   :  1;  // Bits 16:16
    UINT32 dxrx_cmn_rxmvcmres                      :  2;  // Bits 18:17
    UINT32 dqrx_cmn_rxurangesel                    :  1;  // Bits 19:19
    UINT32 dqrx_cmn_rxupwrmuxsel                   :  1;  // Bits 20:20
    UINT32 dqrx_cmn_rxmpdqdlycode                  :  8;  // Bits 28:21
    UINT32 forcedfedisable                         :  1;  // Bits 29:29
    UINT32 Reserved4                               :  2;  // Bits 31:30
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} DATA0CH0_CR_DQRXCTL1_STRUCT;
typedef union {
  struct {
    UINT32                                         : 14;  // Bits 13:0
    UINT32 dqstx_cmn_txodten                       :  1;  // Bits 14:14
    UINT32 dqstx_cmn_txidlemodedrven               :  1;  // Bits 15:15
    UINT32 bonus_1                                 :  1;  // Bits 16:16
    UINT32 bonus_0                                 :  1;  // Bits 17:17
  } BitsA0;
  struct {
    UINT32 dqstx_cmn_txvttparkendqsp               :  1;  // Bits 0:0
    UINT32 dqstx_cmn_txdqsprnfdelay                :  5;  // Bits 5:1
    UINT32 dqstx_cmn_txdqspfnrdelay                :  5;  // Bits 10:6
    UINT32 dqstx_cmn_txvttparkendqsn               :  1;  // Bits 11:11
    UINT32 dqstx_cmn_txvsshiffstlegen              :  1;  // Bits 12:12
    UINT32 dqstx_cmn_txodtstatlegendqs             :  1;  // Bits 13:13
    UINT32 dqstx_cmn_txidlemodedrven               :  1;  // Bits 14:14
    UINT32 bonus_1                                 :  1;  // Bits 15:15
    UINT32 bonus_0                                 :  2;  // Bits 17:16
    UINT32 dqsrx_cmn_rxsetailctl                   :  2;  // Bits 19:18
    UINT32 dqsrx_cmn_rxmtailctl                    :  2;  // Bits 21:20
    UINT32 dqsrx_cmn_rxmctleres                    :  2;  // Bits 23:22
    UINT32 dqsrx_cmn_rxmctleeq                     :  4;  // Bits 27:24
    UINT32 dqsrx_cmn_rxmctlecap                    :  2;  // Bits 29:28
    UINT32 dqsrx_cmn_rxendqsnrcven                 :  1;  // Bits 30:30
    UINT32 dqsrx_cmn_rxdiffampen                   :  1;  // Bits 31:31
  } Bits;
  struct {
    UINT32                                         :  1;  // Bits 0:0
    UINT32 dqstx_cmn_txdqspfalldelay               :  5;  // Bits 5:1
    UINT32 dqstx_cmn_txdqsprisedelay               :  5;  // Bits 10:6
  } BitsQ0;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} DATA0CH0_CR_DQSTXRXCTL_STRUCT;
typedef union {
  struct {
    UINT32 spare                                   :  9;  // Bits 8:0
    UINT32 txdccrangesel                           :  2;  // Bits 10:9
    UINT32 forcedfedisable                         :  1;  // Bits 11:11
    UINT32 dqsrx_cmn_rxmampoffset_rk3              :  5;  // Bits 16:12
    UINT32 dqsrx_cmn_rxmampoffset_rk2              :  5;  // Bits 21:17
    UINT32 dqsrx_cmn_rxmampoffset_rk1              :  5;  // Bits 26:22
    UINT32 dqsrx_cmn_rxmampoffset_rk0              :  5;  // Bits 31:27
  } Bits;
  struct {
    UINT32 dqstx_cmn_txdqsnfalldelay               :  5;  // Bits 4:0
    UINT32 dqstx_cmn_txdqsnrisedelay               :  5;  // Bits 9:5
    UINT32 txdccrangesel                           :  2;  // Bits 11:10
  } BitsQ0;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} DATA0CH0_CR_DQSTXRXCTL0_STRUCT;
typedef union {
  struct {
    UINT32 rxsdl_cmn_rxsdllengthsel                :  2;  // Bits 1:0
    UINT32 rxsdl_cmn_rxpienable                    :  1;  // Bits 2:2
    UINT32 rxsdl_cmn_rxpicapsel                    :  3;  // Bits 5:3
    UINT32 rxsdl_cmn_rxphsdrvprocsel               :  5;  // Bits 10:6
    UINT32 rxsdl_cmn_readlevel                     :  1;  // Bits 11:11
    UINT32 rxsdl_cmn_qualifysdlwithrcven           :  1;  // Bits 12:12
    UINT32 rxsdl_cmn_pbddlycodedqenptr             :  8;  // Bits 20:13
    UINT32 rxsdl_cmn_passrcvenondqsfall            :  1;  // Bits 21:21
    UINT32 rxsdl_cmn_burstlen                      :  3;  // Bits 24:22
    UINT32 rxsdl_cmn_rxsdlbwsel                    :  6;  // Bits 30:25
    UINT32 rxsdl_cmn_srzclkend0                    :  1;  // Bits 31:31
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} DATA0CH0_CR_SDLCTL1_STRUCT;
typedef union {
  struct {
    UINT32 spare                                   :  17;  // Bits 16:0
    UINT32 rxsdl_cmn_rxamp2pixoverselpip           :  4;  // Bits 20:17
    UINT32 rxsdl_cmn_rxamp2pixoverselpin           :  4;  // Bits 24:21
    UINT32 rxsdl_cmn_rxamp2pixoverovr              :  1;  // Bits 25:25
    UINT32 rxsdl_cmn_mdlllen2gearratio             :  1;  // Bits 26:26
    UINT32 rxsdl_cmn_sdlen                         :  1;  // Bits 27:27
    UINT32 rxsdl_cmn_rxunmatchclkinv               :  1;  // Bits 28:28
    UINT32 rxsdl_cmn_rxmatchedpathen               :  1;  // Bits 29:29
    UINT32 rxsdl_cmn_enabledqsnrcven               :  1;  // Bits 30:30
    UINT32 rxsdl_cmn_srzclkenrcven                 :  1;  // Bits 31:31
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} DATA0CH0_CR_SDLCTL0_STRUCT;
typedef union {
  struct {
    UINT32 mdll_cmn_turbocaptrim                   :  2;  // Bits 1:0
    UINT32 mdll_cmn_turboonstartup                 :  1;  // Bits 2:2
    UINT32 mdll_cmn_ldoffcodepi                    :  6;  // Bits 8:3
    UINT32 mdll_cmn_parkval                        :  1;  // Bits 9:9
    UINT32 mdll_cmn_wlphgenin                      :  1;  // Bits 10:10
    UINT32 mdll_cmn_wlrdacholden                   :  1;  // Bits 11:11
    UINT32 mdll_cmn_weaklocken                     :  1;  // Bits 12:12
    UINT32 mdll_cmn_dllbypassen                    :  1;  // Bits 13:13
    UINT32 mdll_cmn_mdllengthsel                   :  2;  // Bits 15:14
    UINT32 mdll_cmn_phdeterr                       :  1;  // Bits 16:16
    UINT32 mdll_cmn_mdlllock                       :  1;  // Bits 17:17
    UINT32 mdll_cmn_pien                           :  8;  // Bits 25:18
    UINT32 mdll_cmn_mdllen                         :  1;  // Bits 26:26
    UINT32 mdll_cmn_extwlval                       :  1;  // Bits 27:27
    UINT32 mdll_cmn_extwlerr                       :  1;  // Bits 28:28
    UINT32 mdll_cmn_extwlen                        :  1;  // Bits 29:29
    UINT32 mdll_cmn_extfreezeval                   :  1;  // Bits 30:30
    UINT32 mdll_cmn_extfreezeen                    :  1;  // Bits 31:31
  } Bits;
  struct {
    UINT32                                        :  9;  // Bits 8:0
    UINT32 parkval_ovr_val                        :  1;  // Bits 9:9
  } BitsQ0;
  
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} DATA0CH0_CR_MDLLCTL0_STRUCT;
typedef union {
  struct {
    UINT32 mdll_cmn_picapsel                       :  3;  // Bits 2:0
    UINT32 mdll_cmn_phsdrvprocsel                  :  5;  // Bits 7:3
    UINT32 mdll_cmn_openloopbias                   :  1;  // Bits 8:8
    UINT32 mdll_cmn_vctlcaptrim                    :  2;  // Bits 10:9
    UINT32 mdll_cmn_vcdlbwsel                      :  6;  // Bits 16:11
    UINT32 mdll_cmn_vctldacforcen                  :  1;  // Bits 17:17
    UINT32 mdll_cmn_vctldaccode                    :  9;  // Bits 26:18
    UINT32 mdll_cmn_vctldaccompareen               :  1;  // Bits 27:27
    UINT32 spare                                   :  4;  // Bits 31:28
  } Bits;
  struct {
    UINT32                                         :  28;  // Bits 27:0
    UINT32 parkval_ovr_sel                         :  1;   // Bits 28:28
  } BitsQ0;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} DATA0CH0_CR_MDLLCTL1_STRUCT;
typedef union {
  struct {
    UINT32 mdll_cmn_dcdtargclksel                  :  1;  // Bits 0:0
    UINT32 mdll_cmn_dccen                          :  1;  // Bits 1:1
    UINT32 mdll_cmn_dllldoen                       :  1;  // Bits 2:2
    UINT32 mdll_cmn_ldoffreadsegen                 :  2;  // Bits 4:3
    UINT32 mdll_cmn_vctlcompoffsetcal              :  5;  // Bits 9:5
    UINT32 mdll_cmn_vcdldcccode                    :  4;  // Bits 13:10
    UINT32 mdll_cmn_ldoffcodelock                  :  8;  // Bits 21:14
    UINT32 mdll_cmn_ldoffcodewl                    :  6;  // Bits 27:22
    UINT32 spare                                   :  3;  // Bits 30:28
    UINT32 mdll_cmn_ldofbdivsel                    :  1;  // Bits 31:31
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} DATA0CH0_CR_MDLLCTL2_STRUCT;
typedef union {
  struct {
    UINT32 mdll_cmn_ldopbiasctrl                   :  3;  // Bits 2:0
    UINT32 mdll_cmn_ldolocalvsshibypass            :  1;  // Bits 3:3
    UINT32 mdll_cmn_ldoforceen                     :  1;  // Bits 4:4
    UINT32 mdll_cmn_ldonbiasvrefcode               :  5;  // Bits 9:5
    UINT32 mdll_cmn_ldonbiasctrl                   :  4;  // Bits 13:10
    UINT32 mdll_cmn_rloadcomp                      :  6;  // Bits 19:14
    UINT32 mdll_cmn_vctldaccompareout              :  1;  // Bits 20:20
    UINT32 mdll_cmn_ldoffcoderead                  :  7;  // Bits 27:21
    UINT32 rxsdl_cmn_rxvcdlcben                    :  2;  // Bits 29:28
    UINT32 mdll_cmn_vcdlcben                       :  2;  // Bits 31:30
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} DATA0CH0_CR_MDLLCTL3_STRUCT;
typedef union {
  struct {
    UINT32 spare                                   :  32;  // Bits 31:0
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} DATA0CH0_CR_MDLLCTL4_STRUCT;

typedef DATA0CH0_CR_MDLLCTL4_STRUCT DATA0CH0_CR_MDLLCTL5_STRUCT;
typedef union {
  struct {
    UINT32 dqpirk2rkupdate_delay                   :  1;  // Bits 0:0
    UINT32 rxrankmuxdelay_rcvenpicode              :  2;  // Bits 2:1
    UINT32 txrankmuxdelay_dqpicode                 :  2;  // Bits 4:3
    UINT32 txrankmuxdelay_dqspicode                :  2;  // Bits 6:5
    UINT32 cfg_rxdqsdelaysaturated_en              :  1;  // Bits 7:7
    UINT32 RxDqsnDelay_rollover_flag               :  1;  // Bits 8:8
    UINT32 RxDqspDelay_rollover_flag               :  1;  // Bits 9:9
    UINT32 dqspirk2rkupdate_delay                  :  1;  // Bits 10:10
    UINT32 rcvenpirk2rkupdate_delay                :  1;  // Bits 11:11
    UINT32 spare                                   :  2;  // Bits 13:12
    UINT32 Reserved5                               :  18;  // Bits 31:14
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} DATA0CH0_CR_DIGMISCCTRL_STRUCT;
typedef union {
  struct {
    UINT32 spare                                   :  21;  // Bits 20:0
    UINT32 dxtx_cmn_txvsshileakeren                :  1;  // Bits 21:21
    UINT32 dxtx_cmn_txvsshileakercomp              :  6;  // Bits 27:22
    UINT32 dqrx_cmn_dqphsdrvprocsel                :  2;  // Bits 29:28
    UINT32 dcdsrz_cmn_dcdsrzsampclksel             :  2;  // Bits 31:30
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} DATA0CH0_CR_AFEMISCCTRL2_STRUCT;
typedef union {
  struct {
    UINT32 dq1_txpbd_ph90incr                      :  1;  // Bits 0:0
    UINT32 dq0_txpbd_ph90incr                      :  1;  // Bits 1:1
    UINT32 dq4_txpbddoffset                        :  6;  // Bits 7:2
    UINT32 dq3_txpbddoffset                        :  6;  // Bits 13:8
    UINT32 dq2_txpbddoffset                        :  6;  // Bits 19:14
    UINT32 dq1_txpbddoffset                        :  6;  // Bits 25:20
    UINT32 dq0_txpbddoffset                        :  6;  // Bits 31:26
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} DATA0CH0_CR_TXPBDOFFSET0_STRUCT;
typedef union {
  struct {
    UINT32 Reserved5                               :  1;  // Bits 0:0
    UINT32 dqs_txpbd_ph90incr                      :  1;  // Bits 1:1
    UINT32 dq7_txpbd_ph90incr                      :  1;  // Bits 2:2
    UINT32 dq6_txpbd_ph90incr                      :  1;  // Bits 3:3
    UINT32 dq5_txpbd_ph90incr                      :  1;  // Bits 4:4
    UINT32 dq4_txpbd_ph90incr                      :  1;  // Bits 5:5
    UINT32 dq3_txpbd_ph90incr                      :  1;  // Bits 6:6
    UINT32 dq2_txpbd_ph90incr                      :  1;  // Bits 7:7
    UINT32 dqs_txpbddo                             :  6;  // Bits 13:8
    UINT32 dq7_txpbddoffset                        :  6;  // Bits 19:14
    UINT32 dq6_txpbddoffset                        :  6;  // Bits 25:20
    UINT32 dq5_txpbddoffset                        :  6;  // Bits 31:26
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} DATA0CH0_CR_TXPBDOFFSET1_STRUCT;
typedef union {
  struct {
    UINT32 dxtx_cmn_ennbiasboost                   :  1;  // Bits 0:0
    UINT32 dqtx_cmn_txrcompodtupdq                 :  6;  // Bits 6:1
    UINT32 dqtx_cmn_txrcompodtdndq                 :  6;  // Bits 12:7
    UINT32 dqstx_cmn_txrcompodtupdqs               :  6;  // Bits 18:13
    UINT32 dqstx_cmn_txrcompodtdndqs               :  6;  // Bits 24:19
    UINT32 Reserved6                               :  7;  // Bits 31:25
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} DATA0CH0_CR_COMPCTL0_STRUCT;
typedef union {
  struct {
    UINT32 dqstx_cmn_txtcocompdqsp                 :  8;  // Bits 7:0
    UINT32 dqstx_cmn_txtcocompdqsn                 :  8;  // Bits 15:8
    UINT32 dxtx_cmn_txrcompdrvup                   :  6;  // Bits 21:16
    UINT32 dxtx_cmn_txrcompdrvdn                   :  6;  // Bits 27:22
    UINT32 Reserved7                               :  4;  // Bits 31:28
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} DATA0CH0_CR_COMPCTL1_STRUCT;
typedef union {
  struct {
    UINT32 dqrx_cmn_rxvsshiffstrobe                :  6;  // Bits 5:0
    UINT32 dqrx_cmn_rxvsshiffdata                  :  6;  // Bits 11:6
    UINT32 dxtx_cmn_txvsshiff                      :  6;  // Bits 17:12
    UINT32 rxbias_cmn_rloadcomp                    :  6;  // Bits 23:18
    UINT32 dxtx_cmn_txtcocomp                      :  8;  // Bits 31:24
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} DATA0CH0_CR_COMPCTL2_STRUCT;
typedef union {
  struct {
    UINT32 dcccodeph0_index0                       :  8;  // Bits 7:0
    UINT32 dcccodeph0_index1                       :  8;  // Bits 15:8
    UINT32 dcccodeph0_index2                       :  8;  // Bits 23:16
    UINT32 initialdcccode                          :  8;  // Bits 31:24
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} DATA0CH0_CR_DCCCTL5_STRUCT;
typedef union {
  struct {
    UINT32 dcccodeph0_index3                       :  8;  // Bits 7:0
    UINT32 dcccodeph0_index4                       :  8;  // Bits 15:8
    UINT32 dcccodeph0_index5                       :  8;  // Bits 23:16
    UINT32 dcccodeph0_index6                       :  8;  // Bits 31:24
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} DATA0CH0_CR_DCCCTL6_STRUCT;
typedef union {
  struct {
    UINT32 dcccodeph0_index7                       :  8;  // Bits 7:0
    UINT32 dcccodeph0_index8                       :  8;  // Bits 15:8
    UINT32 dcccodeph0_index9                       :  8;  // Bits 23:16
    UINT32 dcccodeph90_index1                      :  8;  // Bits 31:24
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} DATA0CH0_CR_DCCCTL7_STRUCT;
typedef union {
  struct {
    UINT32 dcccodeph90_index2                      :  8;  // Bits 7:0
    UINT32 dcccodeph90_index3                      :  8;  // Bits 15:8
    UINT32 dcccodeph90_index4                      :  8;  // Bits 23:16
    UINT32 dcccodeph90_index5                      :  8;  // Bits 31:24
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} DATA0CH0_CR_DCCCTL8_STRUCT;
typedef union {
  struct {
    UINT32 dcccodeph90_index6                      :  8;  // Bits 7:0
    UINT32 dcccodeph90_index7                      :  8;  // Bits 15:8
    UINT32 dcccodeph90_index8                      :  8;  // Bits 23:16
    UINT32 dcccodeph90_index9                      :  8;  // Bits 31:24
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} DATA0CH0_CR_DCCCTL9_STRUCT;
typedef union {
  struct {
    UINT32 rxrcvenpi                               :  12;  // Bits 11:0
    UINT32 rxsdldqsppicode                         :  7;  // Bits 18:12
    UINT32 rxsdldqsnpicode                         :  7;  // Bits 25:19
    UINT32 spare                                   :  6;  // Bits 31:26
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} DATA0CH0_CR_RXCONTROL0RANK0_STRUCT;

typedef DATA0CH0_CR_RXCONTROL0RANK0_STRUCT DATA0CH0_CR_RXCONTROL0RANK1_STRUCT;

typedef DATA0CH0_CR_RXCONTROL0RANK0_STRUCT DATA0CH0_CR_RXCONTROL0RANK2_STRUCT;

typedef DATA0CH0_CR_RXCONTROL0RANK0_STRUCT DATA0CH0_CR_RXCONTROL0RANK3_STRUCT;
typedef union {
  struct {
    UINT32 txdqdelay_rk1                           :  8;  // Bits 7:0
    UINT32 txdqsdelay_rk1                          :  8;  // Bits 15:8
    UINT32 txdqdelay_rk0                           :  8;  // Bits 23:16
    UINT32 txdqsdelay_rk0                          :  8;  // Bits 31:24
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} DATA0CH0_CR_TXCONTROL0_PH90_STRUCT;
typedef union {
  struct {
    UINT32 txdqdelay                               :  12;  // Bits 11:0
    UINT32 txdqsdelay                              :  12;  // Bits 23:12
    UINT32 spare                                   :  8;  // Bits 31:24
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} DATA0CH0_CR_TXCONTROL0RANK0_STRUCT;

typedef DATA0CH0_CR_TXCONTROL0RANK0_STRUCT DATA0CH0_CR_TXCONTROL0RANK1_STRUCT;

typedef DATA0CH0_CR_TXCONTROL0RANK0_STRUCT DATA0CH0_CR_TXCONTROL0RANK2_STRUCT;

typedef DATA0CH0_CR_TXCONTROL0RANK0_STRUCT DATA0CH0_CR_TXCONTROL0RANK3_STRUCT;
typedef union {
  struct {
    UINT32 txdqsperbitdeskew_rk0                   :  8;  // Bits 7:0
    UINT32 txdqsperbitdeskew_rk1                   :  8;  // Bits 15:8
    UINT32 txdqsperbitdeskew_rk2                   :  8;  // Bits 23:16
    UINT32 txdqsperbitdeskew_rk3                   :  8;  // Bits 31:24
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} DATA0CH0_CR_DDRDATADQSRANKX_STRUCT;
typedef union {
  struct {
    UINT32 txdqsperbitdeskewph90_rk0               :  8;  // Bits 7:0
    UINT32 txdqsperbitdeskewph90_rk1               :  8;  // Bits 15:8
    UINT32 txdqsperbitdeskewph90_rk2               :  8;  // Bits 23:16
    UINT32 txdqsperbitdeskewph90_rk3               :  8;  // Bits 31:24
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} DATA0CH0_CR_DDRDATADQSPH90RANKX_STRUCT;
typedef union {
  struct {
    INT32  dqdrvupcompoffset                       :  6;  // Bits 5:0
    INT32  dqdrvdowncompoffset                     :  6;  // Bits 11:6
    INT32  dqodtcompoffset                         :  5;  // Bits 16:12
    INT32  vsshiffcompoffset                       :  5;  // Bits 21:17
    INT32  dqslewratecompoffset                    :  5;  // Bits 26:22
    INT32  rloadoffset                             :  5;  // Bits 31:27
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} DATA0CH0_CR_DDRCRDATAOFFSETCOMP_STRUCT;
typedef union {
  struct {
    INT32  rcvenoffset                             :  6;  // Bits 5:0
    INT32  rxdqsoffset                             :  6;  // Bits 11:6
    INT32  txdqoffset                              :  6;  // Bits 17:12
    INT32  txdqsoffset                             :  6;  // Bits 23:18
    INT32  vrefoffset                              :  7;  // Bits 30:24
    UINT32 rxdqspiuioffset                         :  1;  // Bits 31:31
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} DATA0CH0_CR_DDRCRDATAOFFSETTRAIN_STRUCT;
typedef union {
  struct {
    UINT32 ddrdqovrdmodeen                         :  11;  // Bits 10:0
    UINT32 ddrdqovrddata                           :  11;  // Bits 21:11
    UINT32 ddrcrdqsmaskvalue                       :  1;  // Bits 22:22
    UINT32 ddrcrnumofpulses                        :  2;  // Bits 24:23
    UINT32 ddrcrmaskcntpulsenumstart               :  4;  // Bits 28:25
    UINT32 rcvenmrgining_samplepulse_width         :  2;  // Bits 30:29
    UINT32 dqstx_cmn_txodten                       :  1;  // Bits 31:31
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} DATA0CH0_CR_DDRCRCMDBUSTRAIN_STRUCT;
typedef union {
  struct {
    UINT32 dataretrain_dq0                         :  3;  // Bits 2:0
    UINT32 dataretrain_dq1                         :  3;  // Bits 5:3
    UINT32 dataretrain_dq2                         :  3;  // Bits 8:6
    UINT32 dataretrain_dq3                         :  3;  // Bits 11:9
    UINT32 dataretrain_dq4                         :  3;  // Bits 14:12
    UINT32 dataretrain_dq5                         :  3;  // Bits 17:15
    UINT32 dataretrain_dq6                         :  3;  // Bits 20:18
    UINT32 dataretrain_dq7                         :  3;  // Bits 23:21
    UINT32 dataretrain_bytesel                     :  1;  // Bits 24:24
    UINT32 dataretrainen                           :  1;  // Bits 25:25
    UINT32 dataserialmrr                           :  1;  // Bits 26:26
    UINT32 datainvertnibble                        :  2;  // Bits 28:27
    UINT32 spare                                   :  3;  // Bits 31:29
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} DATA0CH0_CR_DDRCRWRRETRAINSWIZZLECONTROL_STRUCT;
typedef union {
  struct {
    UINT32 initpicode                              :  10;  // Bits 9:0
    UINT32 deltapicode                             :  7;  // Bits 16:10
    UINT32 rocount                                 :  15;  // Bits 31:17
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} DATA0CH0_CR_DDRCRWRRETRAINRANK3_STRUCT;

typedef DATA0CH0_CR_DDRCRWRRETRAINRANK3_STRUCT DATA0CH0_CR_DDRCRWRRETRAINRANK2_STRUCT;

typedef DATA0CH0_CR_DDRCRWRRETRAINRANK3_STRUCT DATA0CH0_CR_DDRCRWRRETRAINRANK1_STRUCT;

typedef DATA0CH0_CR_DDRCRWRRETRAINRANK3_STRUCT DATA0CH0_CR_DDRCRWRRETRAINRANK0_STRUCT;
typedef union {
  struct {
    UINT32 inittrain                               :  1;  // Bits 0:0
    UINT32 duration                                :  10;  // Bits 10:1
    UINT32 resetstatus                             :  1;  // Bits 11:11
    UINT32 largechangedelta                        :  3;  // Bits 14:12
    UINT32 updonlargechange                        :  1;  // Bits 15:15
    UINT32 stoponlargechange                       :  1;  // Bits 16:16
    UINT32 r0status                                :  2;  // Bits 18:17
    UINT32 r1status                                :  2;  // Bits 20:19
    UINT32 r2status                                :  2;  // Bits 22:21
    UINT32 r3status                                :  2;  // Bits 24:23
    UINT32 fsmstatus                               :  4;  // Bits 28:25
    UINT32 fsmrank                                 :  2;  // Bits 30:29
    UINT32 largechangetrig                         :  1;  // Bits 31:31
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} DATA0CH0_CR_DDRCRWRRETRAINCONTROLSTATUS_STRUCT;
typedef union {
  struct {
    UINT32 txdqdelay_rk3                           :  8;  // Bits 7:0
    UINT32 txdqsdelay_rk3                          :  8;  // Bits 15:8
    UINT32 txdqdelay_rk2                           :  8;  // Bits 23:16
    UINT32 txdqsdelay_rk2                          :  8;  // Bits 31:24
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} DATA0CH0_CR_TXCONTROL1_PH90_STRUCT;
typedef union {
  struct {
    UINT32 dqrx_cmn_rxdqstofifodlysel              :  2;  // Bits 1:0
    UINT32 dqrx_cmn_dlydfenumuip                   :  2;  // Bits 3:2
    UINT32 dqrx_cmn_dlydfenumuin                   :  2;  // Bits 5:4
    UINT32 rxsdl_cmn_srznumpreovrdval              :  2;  // Bits 7:6
    UINT32 rxsdl_cmn_srznumpreovrden               :  1;  // Bits 8:8
    UINT32 rxsdl_cmn_srzrcvenpiovrdval             :  1;  // Bits 9:9
    UINT32 rxsdl_cmn_srzrcvenpiovrden              :  1;  // Bits 10:10
    UINT32 rxsdl_cmn_srzrcvenpimodovrdval          :  1;  // Bits 11:11
    UINT32 rxsdl_cmn_srzrcvenpimodovrden           :  1;  // Bits 12:12
    UINT32 viewmxdig_cmn_viewdigfromdigport1       :  1;  // Bits 13:13
    UINT32 viewmxdig_cmn_viewdigfromdigport0       :  1;  // Bits 14:14
    UINT32 rxsdl_cmn_rxsdlbypassval                :  1;  // Bits 15:15
    UINT32 rxsdl_cmn_rxsdlbypassen                 :  1;  // Bits 16:16
    UINT32 rxsdl_cmn_parken                        :  1;  // Bits 17:17
    UINT32 rxsdl_cmn_enablepiwhenoff               :  1;  // Bits 18:18
    UINT32 pghub_cmn_codeupdatepulse               :  1;  // Bits 19:19
    UINT32 dxtx_cmn_tcoslewstatlegen               :  1;  // Bits 20:20
    UINT32 dxtx_cmn_parken                         :  1;  // Bits 21:21
    UINT32 dqsrx_cmn_rxdiffampdfxen                :  1;  // Bits 22:22
    UINT32 afe2dig_rxsdl_cmn_rcventrainfb          :  1;  // Bits 23:23
    UINT32 spare                                   :  7;  // Bits 30:24
    UINT32 afe2dig_mdll_cmn_vctldaccompareout      :  1;  // Bits 31:31
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} DATA0CH0_CR_AFEMISCCTRL1_STRUCT;
typedef union {
  struct {
    UINT32 dqrx_dq7_rxusalspeed                    :  2;  // Bits 1:0
    UINT32 dqrx_dq7_rxmtailctl                     :  2;  // Bits 3:2
    UINT32 dqrx_dq6_rxusalspeed                    :  2;  // Bits 5:4
    UINT32 dqrx_dq6_rxmtailctl                     :  2;  // Bits 7:6
    UINT32 dqrx_dq5_rxusalspeed                    :  2;  // Bits 9:8
    UINT32 dqrx_dq5_rxmtailctl                     :  2;  // Bits 11:10
    UINT32 dqrx_dq4_rxusalspeed                    :  2;  // Bits 13:12
    UINT32 dqrx_dq4_rxmtailctl                     :  2;  // Bits 15:14
    UINT32 dqrx_dq3_rxusalspeed                    :  2;  // Bits 17:16
    UINT32 dqrx_dq3_rxmtailctl                     :  2;  // Bits 19:18
    UINT32 dqrx_dq2_rxusalspeed                    :  2;  // Bits 21:20
    UINT32 dqrx_dq2_rxmtailctl                     :  2;  // Bits 23:22
    UINT32 dqrx_dq1_rxusalspeed                    :  2;  // Bits 25:24
    UINT32 dqrx_dq1_rxmtailctl                     :  2;  // Bits 27:26
    UINT32 dqrx_dq0_rxusalspeed                    :  2;  // Bits 29:28
    UINT32 dqrx_dq0_rxmtailctl                     :  2;  // Bits 31:30
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} DATA0CH0_CR_DQXRXAMPCTL_STRUCT;
typedef union {
  struct {
    UINT32 dqrx_rk1_dq0_rxupmpoffsetcal            :  5;  // Bits 4:0
    UINT32 dqrx_rk0_dq0_rxupmpoffsetcal            :  5;  // Bits 9:5
    UINT32 dqrx_rk3_dq0_rxunoffsetcal              :  5;  // Bits 14:10
    UINT32 dqrx_rk2_dq0_rxunoffsetcal              :  5;  // Bits 19:15
    UINT32 dqrx_rk1_dq0_rxunoffsetcal              :  5;  // Bits 24:20
    UINT32 dqrx_rk0_dq0_rxunoffsetcal              :  5;  // Bits 29:25
    UINT32 Reserved8                               :  2;  // Bits 31:30
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} DATA0CH0_CR_DQ0RXAMPCTL_STRUCT;
typedef union {
  struct {
    UINT32 dqrx_rk1_dq1_rxupmpoffsetcal            :  5;  // Bits 4:0
    UINT32 dqrx_rk0_dq1_rxupmpoffsetcal            :  5;  // Bits 9:5
    UINT32 dqrx_rk3_dq1_rxunoffsetcal              :  5;  // Bits 14:10
    UINT32 dqrx_rk2_dq1_rxunoffsetcal              :  5;  // Bits 19:15
    UINT32 dqrx_rk1_dq1_rxunoffsetcal              :  5;  // Bits 24:20
    UINT32 dqrx_rk0_dq1_rxunoffsetcal              :  5;  // Bits 29:25
    UINT32 Reserved9                               :  2;  // Bits 31:30
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} DATA0CH0_CR_DQ1RXAMPCTL_STRUCT;
typedef union {
  struct {
    UINT32 dqrx_rk1_dq2_rxupmpoffsetcal            :  5;  // Bits 4:0
    UINT32 dqrx_rk0_dq2_rxupmpoffsetcal            :  5;  // Bits 9:5
    UINT32 dqrx_rk3_dq2_rxunoffsetcal              :  5;  // Bits 14:10
    UINT32 dqrx_rk2_dq2_rxunoffsetcal              :  5;  // Bits 19:15
    UINT32 dqrx_rk1_dq2_rxunoffsetcal              :  5;  // Bits 24:20
    UINT32 dqrx_rk0_dq2_rxunoffsetcal              :  5;  // Bits 29:25
    UINT32 Reserved10                              :  2;  // Bits 31:30
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} DATA0CH0_CR_DQ2RXAMPCTL_STRUCT;
typedef union {
  struct {
    UINT32 dqrx_rk1_dq3_rxupmpoffsetcal            :  5;  // Bits 4:0
    UINT32 dqrx_rk0_dq3_rxupmpoffsetcal            :  5;  // Bits 9:5
    UINT32 dqrx_rk3_dq3_rxunoffsetcal              :  5;  // Bits 14:10
    UINT32 dqrx_rk2_dq3_rxunoffsetcal              :  5;  // Bits 19:15
    UINT32 dqrx_rk1_dq3_rxunoffsetcal              :  5;  // Bits 24:20
    UINT32 dqrx_rk0_dq3_rxunoffsetcal              :  5;  // Bits 29:25
    UINT32 Reserved11                              :  2;  // Bits 31:30
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} DATA0CH0_CR_DQ3RXAMPCTL_STRUCT;
typedef union {
  struct {
    UINT32 dqrx_rk1_dq4_rxupmpoffsetcal            :  5;  // Bits 4:0
    UINT32 dqrx_rk0_dq4_rxupmpoffsetcal            :  5;  // Bits 9:5
    UINT32 dqrx_rk3_dq4_rxunoffsetcal              :  5;  // Bits 14:10
    UINT32 dqrx_rk2_dq4_rxunoffsetcal              :  5;  // Bits 19:15
    UINT32 dqrx_rk1_dq4_rxunoffsetcal              :  5;  // Bits 24:20
    UINT32 dqrx_rk0_dq4_rxunoffsetcal              :  5;  // Bits 29:25
    UINT32 Reserved12                              :  2;  // Bits 31:30
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} DATA0CH0_CR_DQ4RXAMPCTL_STRUCT;
typedef union {
  struct {
    UINT32 dqrx_rk1_dq5_rxupmpoffsetcal            :  5;  // Bits 4:0
    UINT32 dqrx_rk0_dq5_rxupmpoffsetcal            :  5;  // Bits 9:5
    UINT32 dqrx_rk3_dq5_rxunoffsetcal              :  5;  // Bits 14:10
    UINT32 dqrx_rk2_dq5_rxunoffsetcal              :  5;  // Bits 19:15
    UINT32 dqrx_rk1_dq5_rxunoffsetcal              :  5;  // Bits 24:20
    UINT32 dqrx_rk0_dq5_rxunoffsetcal              :  5;  // Bits 29:25
    UINT32 Reserved13                              :  2;  // Bits 31:30
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} DATA0CH0_CR_DQ5RXAMPCTL_STRUCT;
typedef union {
  struct {
    UINT32 dqrx_rk1_dq6_rxupmpoffsetcal            :  5;  // Bits 4:0
    UINT32 dqrx_rk0_dq6_rxupmpoffsetcal            :  5;  // Bits 9:5
    UINT32 dqrx_rk3_dq6_rxunoffsetcal              :  5;  // Bits 14:10
    UINT32 dqrx_rk2_dq6_rxunoffsetcal              :  5;  // Bits 19:15
    UINT32 dqrx_rk1_dq6_rxunoffsetcal              :  5;  // Bits 24:20
    UINT32 dqrx_rk0_dq6_rxunoffsetcal              :  5;  // Bits 29:25
    UINT32 Reserved14                              :  2;  // Bits 31:30
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} DATA0CH0_CR_DQ6RXAMPCTL_STRUCT;
typedef union {
  struct {
    UINT32 dqrx_rk1_dq7_rxupmpoffsetcal            :  5;  // Bits 4:0
    UINT32 dqrx_rk0_dq7_rxupmpoffsetcal            :  5;  // Bits 9:5
    UINT32 dqrx_rk3_dq7_rxunoffsetcal              :  5;  // Bits 14:10
    UINT32 dqrx_rk2_dq7_rxunoffsetcal              :  5;  // Bits 19:15
    UINT32 dqrx_rk1_dq7_rxunoffsetcal              :  5;  // Bits 24:20
    UINT32 dqrx_rk0_dq7_rxunoffsetcal              :  5;  // Bits 29:25
    UINT32 Reserved15                              :  2;  // Bits 31:30
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} DATA0CH0_CR_DQ7RXAMPCTL_STRUCT;
typedef union {
  struct {
    UINT32 dqrx_rk3_dq2_rxupmpoffsetcal            :  5;  // Bits 4:0
    UINT32 dqrx_rk2_dq2_rxupmpoffsetcal            :  5;  // Bits 9:5
    UINT32 dqrx_rk3_dq1_rxupmpoffsetcal            :  5;  // Bits 14:10
    UINT32 dqrx_rk2_dq1_rxupmpoffsetcal            :  5;  // Bits 19:15
    UINT32 dqrx_rk3_dq0_rxupmpoffsetcal            :  5;  // Bits 24:20
    UINT32 dqrx_rk2_dq0_rxupmpoffsetcal            :  5;  // Bits 29:25
    UINT32 Reserved16                              :  2;  // Bits 31:30
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} DATA0CH0_CR_DQRXAMPCTL0_STRUCT;
typedef union {
  struct {
    UINT32 dqrx_rk3_dq5_rxupmpoffsetcal            :  5;  // Bits 4:0
    UINT32 dqrx_rk2_dq5_rxupmpoffsetcal            :  5;  // Bits 9:5
    UINT32 dqrx_rk3_dq4_rxupmpoffsetcal            :  5;  // Bits 14:10
    UINT32 dqrx_rk2_dq4_rxupmpoffsetcal            :  5;  // Bits 19:15
    UINT32 dqrx_rk3_dq3_rxupmpoffsetcal            :  5;  // Bits 24:20
    UINT32 dqrx_rk2_dq3_rxupmpoffsetcal            :  5;  // Bits 29:25
    UINT32 Reserved17                              :  2;  // Bits 31:30
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} DATA0CH0_CR_DQRXAMPCTL1_STRUCT;
typedef union {
  struct {
    UINT32 dqrx_rk3_dq7_rxupmpoffsetcal            :  5;  // Bits 4:0
    UINT32 dqrx_rk2_dq7_rxupmpoffsetcal            :  5;  // Bits 9:5
    UINT32 dqrx_rk3_dq6_rxupmpoffsetcal            :  5;  // Bits 14:10
    UINT32 dqrx_rk2_dq6_rxupmpoffsetcal            :  5;  // Bits 19:15
    UINT32 Reserved18                              :  12;  // Bits 31:20
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} DATA0CH0_CR_DQRXAMPCTL2_STRUCT;
typedef union {
  struct {
    UINT32 dxtx_cmn_txvttodten                     :  1;  // Bits 0:0
    UINT32 dxtx_cmn_txvssodten                     :  1;  // Bits 1:1
    UINT32 dxtx_cmn_txvddqodten                    :  1;  // Bits 2:2
    UINT32 dxtx_cmn_txpupusevcciog                 :  1;  // Bits 3:3
    UINT32 dxtx_cmn_txpdnusevcciog                 :  1;  // Bits 4:4
    UINT32 dxtx_cmn_txlocalvsshibyp                :  1;  // Bits 5:5
    UINT32 dqtx_cmn_txvsshiffstlegen               :  1;  // Bits 6:6
    UINT32 dxtx_cmn_txdyncpadreduxdis              :  1;  // Bits 7:7
    UINT32 dxtx_cmn_ennmospup                      :  1;  // Bits 8:8
    UINT32 dqtx_cmn_txodtstatlegendq               :  1;  // Bits 9:9
    UINT32 dxtx_cmn_forceodton                     :  1;  // Bits 10:10
    UINT32 dxtx_cmn_forceodtoff                    :  1;  // Bits 11:11
    UINT32 dqtx_cmn_txidlemodedrven                :  1;  // Bits 12:12
    UINT32 dqtx_cmn_txidlemodedrvdata              :  1;  // Bits 13:13
    UINT32 dqtx_cmn_txeqenpreset                   :  1;  // Bits 14:14
    UINT32 dqtx_cmn_txeqenntap                     :  2;  // Bits 16:15
    UINT32 dqtx_cmn_txeqconstz                     :  1;  // Bits 17:17
    UINT32 dqtx_cmn_txeqcompcoeff2                 :  3;  // Bits 20:18
    UINT32 dqtx_cmn_txeqcompcoeff1                 :  3;  // Bits 23:21
    UINT32 dqtx_cmn_txeqcompcoeff0                 :  3;  // Bits 26:24
    UINT32 Reserved19                              :  5;  // Bits 31:27
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} DATA0CH0_CR_DQTXCTL0_STRUCT;
typedef union {
  struct {
    UINT32 dccsettle_dly                           :  3;  // Bits 2:0
    UINT32 totalsamplecounts                       :  16;  // Bits 18:3
    UINT32 macrostep                               :  8;  // Bits 26:19
    UINT32 microstep                               :  4;  // Bits 30:27
    UINT32 dcdrovcoen                              :  1;  // Bits 31:31
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} DATA0CH0_CR_DCCCTL0_STRUCT;
typedef union {
  struct {
    UINT32 dcc_high_limit_marker                   :  16;  // Bits 15:0
    UINT32 dcc_low_limit_marker                    :  16;  // Bits 31:16
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} DATA0CH0_CR_DCCCTL1_STRUCT;
typedef union {
  struct {
    UINT32 dcdsamplecountstart                     :  1;  // Bits 0:0
    UINT32 dcdsamplecountrst_b                     :  1;  // Bits 1:1
    UINT32 dcdrovcoprog                            :  3;  // Bits 4:2
    UINT32 dcdrolfsr                               :  10;  // Bits 14:5
    UINT32 srz_dcdenable                           :  1;  // Bits 15:15
    UINT32 mdll_dcdenable                          :  1;  // Bits 16:16
    UINT32 Reserved20                              :  3;  // Bits 19:17
    UINT32 dcdrocalsamplecountstart                :  1;  // Bits 20:20
    UINT32 dcdrocalsamplecountrst_b                :  1;  // Bits 21:21
    UINT32 dccfsm_rst_b                            :  1;  // Bits 22:22
    UINT32 compare_dly                             :  4;  // Bits 26:23
    UINT32 dcdreset_dly                            :  3;  // Bits 29:27
    UINT32 clknclkpsample_avg_en                   :  1;  // Bits 30:30
    UINT32 threeloop_en                            :  1;  // Bits 31:31
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} DATA0CH0_CR_DCCCTL2_STRUCT;
typedef union {
  struct {
    UINT32 dcdrocal_high_freq_marker               :  16;  // Bits 15:0
    UINT32 dcdrocal_low_freq_marker                :  16;  // Bits 31:16
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} DATA0CH0_CR_DCCCTL3_STRUCT;
typedef union {
  struct {
    UINT32 totalrocalsamplecounts                  :  16;  // Bits 15:0
    UINT32 dcdsamplesel                            :  4;  // Bits 19:16
    UINT32 step1pattern                            :  4;  // Bits 23:20
    UINT32 step2pattern                            :  4;  // Bits 27:24
    UINT32 step3pattern                            :  4;  // Bits 31:28
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} DATA0CH0_CR_DCCCTL10_STRUCT;
typedef union {
  struct {
    UINT32 dccctrlcodeph0overflow                  :  10;  // Bits 9:0
    UINT32 dccctrlcodeph90overflow                 :  10;  // Bits 19:10
    UINT32 dccctrlcodeph0_status                   :  8;  // Bits 27:20
    UINT32 scr_fsm_dccctrlcodestatus_sel           :  4;  // Bits 31:28
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} DATA0CH0_CR_DCCCTL12_STRUCT;
typedef union {
  struct {
    UINT32 maxsamplecnt                            :  5;  // Bits 4:0
    UINT32 samplesize                              :  5;  // Bits 9:5
    UINT32 pimicrosteps                            :  2;  // Bits 11:10
    UINT32 pimacrosteps                            :  4;  // Bits 15:12
    UINT32 dllwait_dly                             :  4;  // Bits 19:16
    UINT32 swd0piclkpicodesel                      :  1;  // Bits 20:20
    UINT32 swsample_en                             :  1;  // Bits 21:21
    UINT32 clkalignd0piclkpistep_maxreloopcnt      :  8;  // Bits 29:22
    UINT32 start_clkalign                          :  1;  // Bits 30:30
    UINT32 clkalignrst_b                           :  1;  // Bits 31:31
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} DATA0CH0_CR_CLKALIGNCTL0_STRUCT;
typedef union {
  struct {
    UINT32 swd0piclkpicode                         :  8;  // Bits 7:0
    UINT32 clkalignd0piclkpicodeoffset             :  8;  // Bits 15:8
    UINT32 clkaligninitiald0piclkpicode            :  8;  // Bits 23:16
    UINT32 q2d0align_code                          :  8;  // Bits 31:24
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} DATA0CH0_CR_CLKALIGNCTL1_STRUCT;
typedef union {
  struct {
    UINT32 pdoffset_ovr                            :  1;  // Bits 0:0
    UINT32 txdll_dqd0_pdoffset                     :  8;  // Bits 8:1
    UINT32 d0tosig_offset                          :  8;  // Bits 16:9
    UINT32 Reserved21                              :  15;  // Bits 31:17
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} DATA0CH0_CR_CLKALIGNCTL2_STRUCT;
typedef union {
  struct {
    UINT32 dcc_bits_en                             :  10;  // Bits 9:0
    UINT32 dcc_step2pattern_enable                 :  10;  // Bits 19:10
    UINT32 dcc_step3pattern_enable                 :  10;  // Bits 29:20
    UINT32 dcc_serdata_ovren                       :  1;  // Bits 30:30
    UINT32 Reserved22                              :  1;  // Bits 31:31
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} DATA0CH0_CR_DCCCTL4_STRUCT;
typedef union {
  struct {
    UINT32 Vsshiff_rx_offset                       :  4;  // Bits 3:0
    UINT32 Dll_coderead_offset                     :  4;  // Bits 7:4
    UINT32 Dqs_fwdclkn_offset                      :  4;  // Bits 11:8
    UINT32 Dqs_fwdclkp_offset                      :  4;  // Bits 15:12
    UINT32 Dll_vdlllock_offset                     :  4;  // Bits 19:16
    UINT32 Dll_codepi_offset                       :  4;  // Bits 23:20
    UINT32 Dll_codewl_offset                       :  4;  // Bits 27:24
    UINT32 Dll_bwsel_offset                        :  4;  // Bits 31:28
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} DATA0CH0_CR_DDRCRDATACOMPVTT_STRUCT;
typedef union {
  struct {
    UINT32 wlslowclken_dly                         :  5;  // Bits 4:0
    UINT32 weaklocken_dly                          :  5;  // Bits 9:5
    UINT32 spare                                   :  4;  // Bits 13:10
    UINT32 lp_weaklocken                           :  1;  // Bits 14:14
    UINT32 weaklocken_ovr_sel                      :  1;  // Bits 15:15
    UINT32 wlcomp_samplewait                       :  5;  // Bits 20:16
    UINT32 wlcomp_per_stepsize                     :  3;  // Bits 23:21
    UINT32 wlcomp_init_stepsize                    :  3;  // Bits 26:24
    UINT32 weaklockcomp_err                        :  1;  // Bits 27:27
    UINT32 weaklocken_ovr                          :  1;  // Bits 28:28
    UINT32 wlcomp_clkgatedis                       :  1;  // Bits 29:29
    UINT32 weaklocken                              :  1;  // Bits 30:30
    UINT32 weaklockcomp_en                         :  1;  // Bits 31:31
  } Bits;
  struct {
    UINT32 initdlllock_dly                         :  4;  // Bits 3:0
    UINT32 wlrelock_dly                            :  4;  // Bits 7:4
    UINT32 weaklocken_dly                          :  5;  // Bits 12:8
    UINT32 spare                                   :  1;  // Bits 13:13
  } BitsQ0;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} DATA0CH0_CR_WLCTRL_STRUCT;
typedef union {
  struct {
    UINT32 spare                                   :  4;  // Bits 3:0
    UINT32 scr_skip_lpmode_dll_to_alldisb          :  1;  // Bits 4:4
    UINT32 scr_vrefen2rxbiasen_timer_val           :  8;  // Bits 12:5
    UINT32 lpmode_pien_ovrdval                     :  1;  // Bits 13:13
    UINT32 lpmode_pien_ovrden                      :  1;  // Bits 14:14
    UINT32 lpmode_mdllen_ovrdval                   :  1;  // Bits 15:15
    UINT32 lpmode_mdllen_ovrden                    :  1;  // Bits 16:16
    UINT32 lpmode_ldoen_ovrdval                    :  1;  // Bits 17:17
    UINT32 lpmode_ldoen_ovrden                     :  1;  // Bits 18:18
    UINT32 scr_mdlldisb2ldodisb_timer_val          :  3;  // Bits 21:19
    UINT32 scr_pidisb2mdlldsb_timer_val            :  3;  // Bits 24:22
    UINT32 scr_lpmodentry_timer_val                :  3;  // Bits 27:25
    UINT32 scr_dllen2pien_timer_val                :  3;  // Bits 30:28
    UINT32 scr_skip_lpmode_dly                     :  1;  // Bits 31:31
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} DATA0CH0_CR_LPMODECTL_STRUCT;
typedef union {
  struct {
    UINT32 RcompDrvUp                              :  6;  // Bits 5:0
    UINT32 RcompDrvDown                            :  6;  // Bits 11:6
    UINT32 RcompOdtUp                              :  6;  // Bits 17:12
    UINT32 RcompOdtDown                            :  6;  // Bits 23:18
    UINT32 VssHiFF_dq                              :  6;  // Bits 29:24
    UINT32 Spare                                   :  2;  // Bits 31:30
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} DATA0CH0_CR_DDRCRDATACOMP0_STRUCT;
typedef union {
  struct {
    UINT32 Vsshiff_rx                              :  6;  // Bits 5:0
    UINT32 Dqs_fwdclkn                             :  7;  // Bits 12:6
    UINT32 Dqs_fwdclkp                             :  7;  // Bits 19:13
    UINT32 Dll_coderead                            :  7;  // Bits 26:20
    UINT32 Dqsntargetnui                           :  3;  // Bits 29:27
    UINT32 Dqsnoffsetnui                           :  2;  // Bits 31:30
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} DATA0CH0_CR_DDRCRDATACOMP1_STRUCT;
typedef union {
  struct {
    UINT32 Dll_codepi                              :  6;  // Bits 5:0
    UINT32 Dll_codewl                              :  6;  // Bits 11:6
    UINT32 Dll_bwsel                               :  6;  // Bits 17:12
    UINT32 Dll_vdlllock                            :  8;  // Bits 25:18
    UINT32 Offsetnui                               :  2;  // Bits 27:26
    UINT32 Targetnui                               :  3;  // Bits 30:28
    UINT32 Spare                                   :  1;  // Bits 31:31
  } Bits;
  struct {
    UINT32                                         :  18;  // Bits 17:0
    UINT32 rloaddqs                                :  6;  // Bits 23:18
    UINT32 cben                                    :  2;  // Bits 25:24
  } BitsQ0;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} DATA0CH0_CR_VCCDLLCOMPDATA_STRUCT;
typedef union {
  struct {
    UINT32 dccoffset                               :  8;  // Bits 7:0
    UINT32 dccoffsetsign                           :  1;  // Bits 8:8
    UINT32 reserved23                              :  23;  // Bits 31:9
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} DATA0CH0_CR_DCSOFFSET_STRUCT;
typedef union {
  struct {
    UINT32 ccm_done                                :  1;  // Bits 0:0
    UINT32 ccm_result                              :  10;  // Bits 10:1
    UINT32 ccm_clk_sel                             :  4;  // Bits 14:11
    UINT32 spare1                                  :  1;  // Bits 15:15
    UINT32 ccm_bcnt_ival                           :  8;  // Bits 23:16
    UINT32 spare2                                  :  7;  // Bits 30:24
    UINT32 ccm_start                               :  1;  // Bits 31:31
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} DATA0CH0_CR_DFXCCMON_STRUCT;
typedef union {
  struct {
    UINT32 dqdcdsrz_cmn_txeqdataovrdval            :  16;  // Bits 15:0
    UINT32 dqdcdsrz_cmn_txeqdataovrden             :  1;  // Bits 16:16
    UINT32 afe2dig_dqsbuf_cmn_bonus                :  2;  // Bits 18:17
    UINT32 afe2dig_dqrx_dq7_rxumdataunflopped      :  1;  // Bits 19:19
    UINT32 afe2dig_dqrx_dq6_rxumdataunflopped      :  1;  // Bits 20:20
    UINT32 afe2dig_dqrx_dq5_rxumdataunflopped      :  1;  // Bits 21:21
    UINT32 afe2dig_dqrx_dq4_rxumdataunflopped      :  1;  // Bits 22:22
    UINT32 afe2dig_dqrx_dq3_rxumdataunflopped      :  1;  // Bits 23:23
    UINT32 afe2dig_dqrx_dq2_rxumdataunflopped      :  1;  // Bits 24:24
    UINT32 afe2dig_dqrx_dq1_rxumdataunflopped      :  1;  // Bits 25:25
    UINT32 afe2dig_dqrx_dq0_rxumdataunflopped      :  1;  // Bits 26:26
    UINT32 afe2dig_cktop_cmn_bonus                 :  4;  // Bits 30:27
    UINT32 Reserved24                              :  1;  // Bits 31:31
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} DATA0CH0_CR_AFEMISCCTRL0_STRUCT;
typedef union {
  struct {
    UINT32 rxbias_cmn_viewanaen                    :  1;  // Bits 0:0
    UINT32 rxbias_cmn_viewanabiassel               :  3;  // Bits 3:1
    UINT32 rxvref_cmn_viewanaen                    :  1;  // Bits 4:4
    UINT32 viewmxdig_cmn_viewdigcbbselport1        :  4;  // Bits 8:5
    UINT32 viewmxdig_cmn_viewdigcbbselport0        :  4;  // Bits 12:9
    UINT32 viewmxana_cmn_viewanasel                :  3;  // Bits 15:13
    UINT32 viewmxana_cmn_viewanaen                 :  1;  // Bits 16:16
    UINT32 viewdig_cmn_viewdigport1sel             :  4;  // Bits 20:17
    UINT32 viewdig_cmn_viewdigport0sel             :  4;  // Bits 24:21
    UINT32 mdll_cmn_viewanaen                      :  1;  // Bits 25:25
    UINT32 dqsbuf_dqs_anaviewsel                   :  3;  // Bits 28:26
    UINT32 dqsbuf_dqs_anaviewen                    :  1;  // Bits 29:29
    UINT32 Reserved25                              :  2;  // Bits 31:30
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} DATA0CH0_CR_VIEWCTL_STRUCT;
typedef union {
  struct {
    UINT32 fatal_live                              :  10;  // Bits 9:0
    UINT32 fatal_sticky                            :  10;  // Bits 19:10
    UINT32 fatal_cal                               :  1;  // Bits 20:20
    UINT32 dcdrocal_en                             :  1;  // Bits 21:21
    UINT32 dcdrocal_start                          :  1;  // Bits 22:22
    UINT32 dcc_start                               :  1;  // Bits 23:23
    UINT32 dccctrlcodeph90_status                  :  8;  // Bits 31:24
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} DATA0CH0_CR_DCCCTL11_STRUCT;
typedef union {
  struct {
    UINT32 dcdsamplecount                          :  16;  // Bits 15:0
    UINT32 dcdrocal_count                          :  16;  // Bits 31:16
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} DATA0CH0_CR_DCCCTL13_STRUCT;
typedef union {
  struct {
    UINT32 spare                                   :  25;  // Bits 24:0
    UINT32 initdone_status                         :  1;  // Bits 25:25
    UINT32 scr_gvblock_ovrride                     :  1;  // Bits 26:26
  } BitsA0;
  struct {
    UINT32 spare                                   :  21;  // Bits 20:0
    UINT32 current_pmctrlfsm_state                 :  4;  // Bits 24:21
    UINT32 Vsshipwrgood                            :  1;  // Bits 25:25
    UINT32 initdone_status                         :  1;  // Bits 26:26
    UINT32 pmctrlfsm_ovrden                        :  1;  // Bits 27:27
    UINT32 pmctrlfsm_ovrdval                       :  4;  // Bits 31:28
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} DATA0CH0_CR_PMFSM_STRUCT;
typedef union {
  struct {
    UINT32 pmclkgatedisable                        :  1;  // Bits 0:0
    UINT32 rampenable_ovren                        :  1;  // Bits 1:1
    UINT32 rampenable_ovrval                       :  1;  // Bits 2:2
    UINT32 initdone_ovren                          :  1;  // Bits 3:3
    UINT32 init_done_ovrval                        :  1;  // Bits 4:4
    UINT32 iobufact_ovren                          :  1;  // Bits 5:5
    UINT32 iobufact_ovrval                         :  1;  // Bits 6:6
    UINT32 clkalign_ovren                          :  1;  // Bits 7:7
    UINT32 start_clkalign_ovrval                   :  1;  // Bits 8:8
    UINT32 clkalignrst_ovrval                      :  1;  // Bits 9:9
    UINT32 func_rst_ovren                          :  1;  // Bits 10:10
    UINT32 func_rst_ovrval                         :  1;  // Bits 11:11
    UINT32 pien_ovren                              :  1;  // Bits 12:12
    UINT32 pien_ovrval                             :  1;  // Bits 13:13
    UINT32 dllen_ovren                             :  1;  // Bits 14:14
    UINT32 dllen_ovrval                            :  1;  // Bits 15:15
    UINT32 ldoen_ovrval                            :  1;  // Bits 16:16
    UINT32 Vsshipowergood_ovren                    :  1;  // Bits 17:17
    UINT32 Vsshipowergood_ovrval                   :  1;  // Bits 18:18
    UINT32 clkalign_complete_ovren                 :  1;  // Bits 19:19
    UINT32 clkalign_complete_ovrval                :  1;  // Bits 20:20
    UINT32 mdlllock_ovrval                         :  1;  // Bits 21:21
    UINT32 mdlllock_ovren                          :  1;  // Bits 22:22
    UINT32 compupdatedone_ovren                    :  1;  // Bits 23:23
    UINT32 compupdatedone_ovrval                   :  1;  // Bits 24:24
    UINT32 scr_gvblock_ovrride                     :  1;  // Bits 25:25
    UINT32 spare                                   :  6;  // Bits 31:26
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} DATA0CH0_CR_PMFSM1_STRUCT;
typedef union {
  struct {
    UINT32 clkalign_sample_pd_output               :  1;  // Bits 0:0
    UINT32 clkalign_complete_level                 :  1;  // Bits 1:1
    UINT32 clkalignstatus_err                      :  1;  // Bits 2:2
    UINT32 clkalignstatus_one_count                :  5;  // Bits 7:3
    UINT32 clkalignstatus_zero_count               :  5;  // Bits 12:8
    UINT32 clkalignstatus_sample_done              :  1;  // Bits 13:13
    UINT32 clkalignstatus_safe1                    :  1;  // Bits 14:14
    UINT32 clkalignstatus_safe0                    :  1;  // Bits 15:15
    UINT32 ref2xclkpicode                          :  8;  // Bits 23:16
    UINT32 Reserved26                              :  8;  // Bits 31:24
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} DATA0CH0_CR_CLKALIGNSTATUS_STRUCT;
typedef union {
  struct {
    UINT32 rankovrd                                :  1;  // Bits 0:0
    UINT32 rankvalue                               :  2;  // Bits 2:1
    UINT32 rxampoffseten                           :  2;  // Bits 4:3
    UINT32 rxtrainingmode                          :  1;  // Bits 5:5
    UINT32 wltrainingmode                          :  2;  // Bits 7:6
    UINT32 rltrainingmode                          :  1;  // Bits 8:8
    UINT32 senseamptrainingmode                    :  1;  // Bits 9:9
    UINT32 catrainingmode                          :  1;  // Bits 10:10
    UINT32 datatrainfeedback                       :  10;  // Bits 20:11
    UINT32 spare                                   :  1;  // Bits 21:21
    UINT32 ddrdqdrvenovrddata                      :  8;  // Bits 29:22
    UINT32 ddrdqdrvenovrdmodeen                    :  1;  // Bits 30:30
    UINT32 ddrdqrxsdlbypassen                      :  1;  // Bits 31:31
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} DATA0CH0_CR_DATATRAINFEEDBACK_STRUCT;
typedef union {
  struct {
    UINT32                                         :  4;  // Bits 3:0
    UINT32 minval                                  :  7;  // Bits 10:4
    UINT32 maxval                                  :  7;  // Bits 17:11
    UINT32 Reserved30                              :  1;  // Bits 18:18
    UINT32 Reserved29                              :  1;  // Bits 19:19
    UINT32                                         :  1;  // Bits 20:20
    UINT32 overflow_status                         :  1;  // Bits 21:21
    UINT32 Reserved28                              :  1;  // Bits 22:22
    UINT32 calccenter                              :  1;  // Bits 23:23
    UINT32 iolbcycles                              :  4;  // Bits 27:24
  } BitsA0;
  struct {
    UINT32 runtest                                 :  1;  // Bits 0:0
    UINT32 param                                   :  3;  // Bits 3:1
    UINT32 minval                                  :  8;  // Bits 11:4
    UINT32 maxval                                  :  8;  // Bits 19:12
    UINT32 forceonrcven                            :  1;  // Bits 20:20
    UINT32 Reserved28                              :  1;  // Bits 21:21
    UINT32 settle_time_adj                         :  2;  // Bits 23:22
    UINT32 calccenter                              :  1;  // Bits 24:24
    UINT32 skip_count                              :  3;  // Bits 27:25
    UINT32 laneresult                              :  1;  // Bits 28:28
    UINT32 data0                                   :  1;  // Bits 29:29
    UINT32 data1                                   :  1;  // Bits 30:30
    UINT32 Reserved27                              :  1;  // Bits 31:31
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} DATA0CH0_CR_DDRCRMARGINMODECONTROL0_STRUCT;
typedef union {
  struct {
    UINT32 result                                  :  32;  // Bits 31:0
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} DATA0CH0_CR_DDRCRMARGINMODEDEBUGMSB0_STRUCT;

typedef DATA0CH0_CR_DDRCRMARGINMODEDEBUGMSB0_STRUCT DATA0CH0_CR_DDRCRMARGINMODEDEBUGLSB0_STRUCT;
typedef union {
  struct {
    UINT32 maxdccsteps                             :  7;  // Bits 6:0
    UINT32 reserved29                              :  25;  // Bits 31:7
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} DATA0CH0_CR_DCCCTL14_STRUCT;

typedef DATA0CH0_CR_DDRDATADQRANK0LANE0_STRUCT DATA0CH1_CR_DDRDATADQRANK0LANE0_STRUCT;

typedef DATA0CH0_CR_DDRDATADQRANK0LANE0_STRUCT DATA0CH1_CR_DDRDATADQRANK0LANE1_STRUCT;

typedef DATA0CH0_CR_DDRDATADQRANK0LANE0_STRUCT DATA0CH1_CR_DDRDATADQRANK0LANE2_STRUCT;

typedef DATA0CH0_CR_DDRDATADQRANK0LANE0_STRUCT DATA0CH1_CR_DDRDATADQRANK0LANE3_STRUCT;

typedef DATA0CH0_CR_DDRDATADQRANK0LANE0_STRUCT DATA0CH1_CR_DDRDATADQRANK0LANE4_STRUCT;

typedef DATA0CH0_CR_DDRDATADQRANK0LANE0_STRUCT DATA0CH1_CR_DDRDATADQRANK0LANE5_STRUCT;

typedef DATA0CH0_CR_DDRDATADQRANK0LANE0_STRUCT DATA0CH1_CR_DDRDATADQRANK0LANE6_STRUCT;

typedef DATA0CH0_CR_DDRDATADQRANK0LANE0_STRUCT DATA0CH1_CR_DDRDATADQRANK0LANE7_STRUCT;

typedef DATA0CH0_CR_DDRDATADQRANK0LANE0_STRUCT DATA0CH1_CR_DDRDATADQRANK1LANE0_STRUCT;

typedef DATA0CH0_CR_DDRDATADQRANK0LANE0_STRUCT DATA0CH1_CR_DDRDATADQRANK1LANE1_STRUCT;

typedef DATA0CH0_CR_DDRDATADQRANK0LANE0_STRUCT DATA0CH1_CR_DDRDATADQRANK1LANE2_STRUCT;

typedef DATA0CH0_CR_DDRDATADQRANK0LANE0_STRUCT DATA0CH1_CR_DDRDATADQRANK1LANE3_STRUCT;

typedef DATA0CH0_CR_DDRDATADQRANK0LANE0_STRUCT DATA0CH1_CR_DDRDATADQRANK1LANE4_STRUCT;

typedef DATA0CH0_CR_DDRDATADQRANK0LANE0_STRUCT DATA0CH1_CR_DDRDATADQRANK1LANE5_STRUCT;

typedef DATA0CH0_CR_DDRDATADQRANK0LANE0_STRUCT DATA0CH1_CR_DDRDATADQRANK1LANE6_STRUCT;

typedef DATA0CH0_CR_DDRDATADQRANK0LANE0_STRUCT DATA0CH1_CR_DDRDATADQRANK1LANE7_STRUCT;

typedef DATA0CH0_CR_DDRDATADQRANK0LANE0_STRUCT DATA0CH1_CR_DDRDATADQRANK2LANE0_STRUCT;

typedef DATA0CH0_CR_DDRDATADQRANK0LANE0_STRUCT DATA0CH1_CR_DDRDATADQRANK2LANE1_STRUCT;

typedef DATA0CH0_CR_DDRDATADQRANK0LANE0_STRUCT DATA0CH1_CR_DDRDATADQRANK2LANE2_STRUCT;

typedef DATA0CH0_CR_DDRDATADQRANK0LANE0_STRUCT DATA0CH1_CR_DDRDATADQRANK2LANE3_STRUCT;

typedef DATA0CH0_CR_DDRDATADQRANK0LANE0_STRUCT DATA0CH1_CR_DDRDATADQRANK2LANE4_STRUCT;

typedef DATA0CH0_CR_DDRDATADQRANK0LANE0_STRUCT DATA0CH1_CR_DDRDATADQRANK2LANE5_STRUCT;

typedef DATA0CH0_CR_DDRDATADQRANK0LANE0_STRUCT DATA0CH1_CR_DDRDATADQRANK2LANE6_STRUCT;

typedef DATA0CH0_CR_DDRDATADQRANK0LANE0_STRUCT DATA0CH1_CR_DDRDATADQRANK2LANE7_STRUCT;

typedef DATA0CH0_CR_DDRDATADQRANK0LANE0_STRUCT DATA0CH1_CR_DDRDATADQRANK3LANE0_STRUCT;

typedef DATA0CH0_CR_DDRDATADQRANK0LANE0_STRUCT DATA0CH1_CR_DDRDATADQRANK3LANE1_STRUCT;

typedef DATA0CH0_CR_DDRDATADQRANK0LANE0_STRUCT DATA0CH1_CR_DDRDATADQRANK3LANE2_STRUCT;

typedef DATA0CH0_CR_DDRDATADQRANK0LANE0_STRUCT DATA0CH1_CR_DDRDATADQRANK3LANE3_STRUCT;

typedef DATA0CH0_CR_DDRDATADQRANK0LANE0_STRUCT DATA0CH1_CR_DDRDATADQRANK3LANE4_STRUCT;

typedef DATA0CH0_CR_DDRDATADQRANK0LANE0_STRUCT DATA0CH1_CR_DDRDATADQRANK3LANE5_STRUCT;

typedef DATA0CH0_CR_DDRDATADQRANK0LANE0_STRUCT DATA0CH1_CR_DDRDATADQRANK3LANE6_STRUCT;

typedef DATA0CH0_CR_DDRDATADQRANK0LANE0_STRUCT DATA0CH1_CR_DDRDATADQRANK3LANE7_STRUCT;

typedef DATA0CH0_CR_DDRCRDATACONTROL0_STRUCT DATA0CH1_CR_DDRCRDATACONTROL0_STRUCT;

typedef DATA0CH0_CR_DDRCRDATACONTROL1_STRUCT DATA0CH1_CR_DDRCRDATACONTROL1_STRUCT;

typedef DATA0CH0_CR_DDRCRDATACONTROL2_STRUCT DATA0CH1_CR_DDRCRDATACONTROL2_STRUCT;

typedef DATA0CH0_CR_DDRCRDATACONTROL3_STRUCT DATA0CH1_CR_DDRCRDATACONTROL3_STRUCT;

typedef DATA0CH0_CR_AFEMISC_STRUCT DATA0CH1_CR_AFEMISC_STRUCT;

typedef DATA0CH0_CR_SRZCTL_STRUCT DATA0CH1_CR_SRZCTL_STRUCT;

typedef DATA0CH0_CR_DQRXCTL0_STRUCT DATA0CH1_CR_DQRXCTL0_STRUCT;

typedef DATA0CH0_CR_DQRXCTL1_STRUCT DATA0CH1_CR_DQRXCTL1_STRUCT;

typedef DATA0CH0_CR_DQSTXRXCTL_STRUCT DATA0CH1_CR_DQSTXRXCTL_STRUCT;

typedef DATA0CH0_CR_DQSTXRXCTL0_STRUCT DATA0CH1_CR_DQSTXRXCTL0_STRUCT;

typedef DATA0CH0_CR_SDLCTL1_STRUCT DATA0CH1_CR_SDLCTL1_STRUCT;

typedef DATA0CH0_CR_SDLCTL0_STRUCT DATA0CH1_CR_SDLCTL0_STRUCT;

typedef DATA0CH0_CR_MDLLCTL0_STRUCT DATA0CH1_CR_MDLLCTL0_STRUCT;

typedef DATA0CH0_CR_MDLLCTL1_STRUCT DATA0CH1_CR_MDLLCTL1_STRUCT;

typedef DATA0CH0_CR_MDLLCTL2_STRUCT DATA0CH1_CR_MDLLCTL2_STRUCT;

typedef DATA0CH0_CR_MDLLCTL3_STRUCT DATA0CH1_CR_MDLLCTL3_STRUCT;

typedef DATA0CH0_CR_MDLLCTL4_STRUCT DATA0CH1_CR_MDLLCTL4_STRUCT;

typedef DATA0CH0_CR_MDLLCTL4_STRUCT DATA0CH1_CR_MDLLCTL5_STRUCT;

typedef DATA0CH0_CR_DIGMISCCTRL_STRUCT DATA0CH1_CR_DIGMISCCTRL_STRUCT;

typedef DATA0CH0_CR_AFEMISCCTRL2_STRUCT DATA0CH1_CR_AFEMISCCTRL2_STRUCT;

typedef DATA0CH0_CR_TXPBDOFFSET0_STRUCT DATA0CH1_CR_TXPBDOFFSET0_STRUCT;

typedef DATA0CH0_CR_TXPBDOFFSET1_STRUCT DATA0CH1_CR_TXPBDOFFSET1_STRUCT;

typedef DATA0CH0_CR_COMPCTL0_STRUCT DATA0CH1_CR_COMPCTL0_STRUCT;

typedef DATA0CH0_CR_COMPCTL1_STRUCT DATA0CH1_CR_COMPCTL1_STRUCT;

typedef DATA0CH0_CR_COMPCTL2_STRUCT DATA0CH1_CR_COMPCTL2_STRUCT;

typedef DATA0CH0_CR_DCCCTL5_STRUCT DATA0CH1_CR_DCCCTL5_STRUCT;

typedef DATA0CH0_CR_DCCCTL6_STRUCT DATA0CH1_CR_DCCCTL6_STRUCT;

typedef DATA0CH0_CR_DCCCTL7_STRUCT DATA0CH1_CR_DCCCTL7_STRUCT;

typedef DATA0CH0_CR_DCCCTL8_STRUCT DATA0CH1_CR_DCCCTL8_STRUCT;

typedef DATA0CH0_CR_DCCCTL9_STRUCT DATA0CH1_CR_DCCCTL9_STRUCT;

typedef DATA0CH0_CR_RXCONTROL0RANK0_STRUCT DATA0CH1_CR_RXCONTROL0RANK0_STRUCT;

typedef DATA0CH0_CR_RXCONTROL0RANK0_STRUCT DATA0CH1_CR_RXCONTROL0RANK1_STRUCT;

typedef DATA0CH0_CR_RXCONTROL0RANK0_STRUCT DATA0CH1_CR_RXCONTROL0RANK2_STRUCT;

typedef DATA0CH0_CR_RXCONTROL0RANK0_STRUCT DATA0CH1_CR_RXCONTROL0RANK3_STRUCT;

typedef DATA0CH0_CR_TXCONTROL0_PH90_STRUCT DATA0CH1_CR_TXCONTROL0_PH90_STRUCT;

typedef DATA0CH0_CR_TXCONTROL0RANK0_STRUCT DATA0CH1_CR_TXCONTROL0RANK0_STRUCT;

typedef DATA0CH0_CR_TXCONTROL0RANK0_STRUCT DATA0CH1_CR_TXCONTROL0RANK1_STRUCT;

typedef DATA0CH0_CR_TXCONTROL0RANK0_STRUCT DATA0CH1_CR_TXCONTROL0RANK2_STRUCT;

typedef DATA0CH0_CR_TXCONTROL0RANK0_STRUCT DATA0CH1_CR_TXCONTROL0RANK3_STRUCT;

typedef DATA0CH0_CR_DDRDATADQSRANKX_STRUCT DATA0CH1_CR_DDRDATADQSRANKX_STRUCT;

typedef DATA0CH0_CR_DDRDATADQSPH90RANKX_STRUCT DATA0CH1_CR_DDRDATADQSPH90RANKX_STRUCT;

typedef DATA0CH0_CR_DDRCRDATAOFFSETCOMP_STRUCT DATA0CH1_CR_DDRCRDATAOFFSETCOMP_STRUCT;

typedef DATA0CH0_CR_DDRCRDATAOFFSETTRAIN_STRUCT DATA0CH1_CR_DDRCRDATAOFFSETTRAIN_STRUCT;

typedef DATA0CH0_CR_DDRCRCMDBUSTRAIN_STRUCT DATA0CH1_CR_DDRCRCMDBUSTRAIN_STRUCT;

typedef DATA0CH0_CR_DDRCRWRRETRAINSWIZZLECONTROL_STRUCT DATA0CH1_CR_DDRCRWRRETRAINSWIZZLECONTROL_STRUCT;

typedef DATA0CH0_CR_DDRCRWRRETRAINRANK3_STRUCT DATA0CH1_CR_DDRCRWRRETRAINRANK3_STRUCT;

typedef DATA0CH0_CR_DDRCRWRRETRAINRANK3_STRUCT DATA0CH1_CR_DDRCRWRRETRAINRANK2_STRUCT;

typedef DATA0CH0_CR_DDRCRWRRETRAINRANK3_STRUCT DATA0CH1_CR_DDRCRWRRETRAINRANK1_STRUCT;

typedef DATA0CH0_CR_DDRCRWRRETRAINRANK3_STRUCT DATA0CH1_CR_DDRCRWRRETRAINRANK0_STRUCT;

typedef DATA0CH0_CR_DDRCRWRRETRAINCONTROLSTATUS_STRUCT DATA0CH1_CR_DDRCRWRRETRAINCONTROLSTATUS_STRUCT;

typedef DATA0CH0_CR_TXCONTROL1_PH90_STRUCT DATA0CH1_CR_TXCONTROL1_PH90_STRUCT;

typedef DATA0CH0_CR_AFEMISCCTRL1_STRUCT DATA0CH1_CR_AFEMISCCTRL1_STRUCT;

typedef DATA0CH0_CR_DQXRXAMPCTL_STRUCT DATA0CH1_CR_DQXRXAMPCTL_STRUCT;

typedef DATA0CH0_CR_DQ0RXAMPCTL_STRUCT DATA0CH1_CR_DQ0RXAMPCTL_STRUCT;

typedef DATA0CH0_CR_DQ1RXAMPCTL_STRUCT DATA0CH1_CR_DQ1RXAMPCTL_STRUCT;

typedef DATA0CH0_CR_DQ2RXAMPCTL_STRUCT DATA0CH1_CR_DQ2RXAMPCTL_STRUCT;

typedef DATA0CH0_CR_DQ3RXAMPCTL_STRUCT DATA0CH1_CR_DQ3RXAMPCTL_STRUCT;

typedef DATA0CH0_CR_DQ4RXAMPCTL_STRUCT DATA0CH1_CR_DQ4RXAMPCTL_STRUCT;

typedef DATA0CH0_CR_DQ5RXAMPCTL_STRUCT DATA0CH1_CR_DQ5RXAMPCTL_STRUCT;

typedef DATA0CH0_CR_DQ6RXAMPCTL_STRUCT DATA0CH1_CR_DQ6RXAMPCTL_STRUCT;

typedef DATA0CH0_CR_DQ7RXAMPCTL_STRUCT DATA0CH1_CR_DQ7RXAMPCTL_STRUCT;

typedef DATA0CH0_CR_DQRXAMPCTL0_STRUCT DATA0CH1_CR_DQRXAMPCTL0_STRUCT;

typedef DATA0CH0_CR_DQRXAMPCTL1_STRUCT DATA0CH1_CR_DQRXAMPCTL1_STRUCT;

typedef DATA0CH0_CR_DQRXAMPCTL2_STRUCT DATA0CH1_CR_DQRXAMPCTL2_STRUCT;

typedef DATA0CH0_CR_DQTXCTL0_STRUCT DATA0CH1_CR_DQTXCTL0_STRUCT;

typedef DATA0CH0_CR_DCCCTL0_STRUCT DATA0CH1_CR_DCCCTL0_STRUCT;

typedef DATA0CH0_CR_DCCCTL1_STRUCT DATA0CH1_CR_DCCCTL1_STRUCT;

typedef DATA0CH0_CR_DCCCTL2_STRUCT DATA0CH1_CR_DCCCTL2_STRUCT;

typedef DATA0CH0_CR_DCCCTL3_STRUCT DATA0CH1_CR_DCCCTL3_STRUCT;

typedef DATA0CH0_CR_DCCCTL10_STRUCT DATA0CH1_CR_DCCCTL10_STRUCT;

typedef DATA0CH0_CR_DCCCTL12_STRUCT DATA0CH1_CR_DCCCTL12_STRUCT;

typedef DATA0CH0_CR_CLKALIGNCTL0_STRUCT DATA0CH1_CR_CLKALIGNCTL0_STRUCT;

typedef DATA0CH0_CR_CLKALIGNCTL1_STRUCT DATA0CH1_CR_CLKALIGNCTL1_STRUCT;

typedef DATA0CH0_CR_CLKALIGNCTL2_STRUCT DATA0CH1_CR_CLKALIGNCTL2_STRUCT;

typedef DATA0CH0_CR_DCCCTL4_STRUCT DATA0CH1_CR_DCCCTL4_STRUCT;

typedef DATA0CH0_CR_DDRCRDATACOMPVTT_STRUCT DATA0CH1_CR_DDRCRDATACOMPVTT_STRUCT;

typedef DATA0CH0_CR_WLCTRL_STRUCT DATA0CH1_CR_WLCTRL_STRUCT;

typedef DATA0CH0_CR_LPMODECTL_STRUCT DATA0CH1_CR_LPMODECTL_STRUCT;

typedef DATA0CH0_CR_DDRCRDATACOMP0_STRUCT DATA0CH1_CR_DDRCRDATACOMP0_STRUCT;

typedef DATA0CH0_CR_DDRCRDATACOMP1_STRUCT DATA0CH1_CR_DDRCRDATACOMP1_STRUCT;

typedef DATA0CH0_CR_VCCDLLCOMPDATA_STRUCT DATA0CH1_CR_VCCDLLCOMPDATA_STRUCT;

typedef DATA0CH0_CR_DCSOFFSET_STRUCT DATA0CH1_CR_DCSOFFSET_STRUCT;

typedef DATA0CH0_CR_DFXCCMON_STRUCT DATA0CH1_CR_DFXCCMON_STRUCT;

typedef DATA0CH0_CR_AFEMISCCTRL0_STRUCT DATA0CH1_CR_AFEMISCCTRL0_STRUCT;

typedef DATA0CH0_CR_VIEWCTL_STRUCT DATA0CH1_CR_VIEWCTL_STRUCT;

typedef DATA0CH0_CR_DCCCTL11_STRUCT DATA0CH1_CR_DCCCTL11_STRUCT;

typedef DATA0CH0_CR_DCCCTL13_STRUCT DATA0CH1_CR_DCCCTL13_STRUCT;

typedef DATA0CH0_CR_PMFSM_STRUCT DATA0CH1_CR_PMFSM_STRUCT;

typedef DATA0CH0_CR_PMFSM1_STRUCT DATA0CH1_CR_PMFSM1_STRUCT;

typedef DATA0CH0_CR_CLKALIGNSTATUS_STRUCT DATA0CH1_CR_CLKALIGNSTATUS_STRUCT;

typedef DATA0CH0_CR_DATATRAINFEEDBACK_STRUCT DATA0CH1_CR_DATATRAINFEEDBACK_STRUCT;

typedef DATA0CH0_CR_DDRCRMARGINMODECONTROL0_STRUCT DATA0CH1_CR_DDRCRMARGINMODECONTROL0_STRUCT;

typedef DATA0CH0_CR_DDRCRMARGINMODEDEBUGMSB0_STRUCT DATA0CH1_CR_DDRCRMARGINMODEDEBUGMSB0_STRUCT;

typedef DATA0CH0_CR_DDRCRMARGINMODEDEBUGMSB0_STRUCT DATA0CH1_CR_DDRCRMARGINMODEDEBUGLSB0_STRUCT;

typedef DATA0CH0_CR_DCCCTL14_STRUCT DATA0CH1_CR_DCCCTL14_STRUCT;

typedef DATA0CH0_CR_DDRDATADQRANK0LANE0_STRUCT DATA1CH0_CR_DDRDATADQRANK0LANE0_STRUCT;

typedef DATA0CH0_CR_DDRDATADQRANK0LANE0_STRUCT DATA1CH0_CR_DDRDATADQRANK0LANE1_STRUCT;

typedef DATA0CH0_CR_DDRDATADQRANK0LANE0_STRUCT DATA1CH0_CR_DDRDATADQRANK0LANE2_STRUCT;

typedef DATA0CH0_CR_DDRDATADQRANK0LANE0_STRUCT DATA1CH0_CR_DDRDATADQRANK0LANE3_STRUCT;

typedef DATA0CH0_CR_DDRDATADQRANK0LANE0_STRUCT DATA1CH0_CR_DDRDATADQRANK0LANE4_STRUCT;

typedef DATA0CH0_CR_DDRDATADQRANK0LANE0_STRUCT DATA1CH0_CR_DDRDATADQRANK0LANE5_STRUCT;

typedef DATA0CH0_CR_DDRDATADQRANK0LANE0_STRUCT DATA1CH0_CR_DDRDATADQRANK0LANE6_STRUCT;

typedef DATA0CH0_CR_DDRDATADQRANK0LANE0_STRUCT DATA1CH0_CR_DDRDATADQRANK0LANE7_STRUCT;

typedef DATA0CH0_CR_DDRDATADQRANK0LANE0_STRUCT DATA1CH0_CR_DDRDATADQRANK1LANE0_STRUCT;

typedef DATA0CH0_CR_DDRDATADQRANK0LANE0_STRUCT DATA1CH0_CR_DDRDATADQRANK1LANE1_STRUCT;

typedef DATA0CH0_CR_DDRDATADQRANK0LANE0_STRUCT DATA1CH0_CR_DDRDATADQRANK1LANE2_STRUCT;

typedef DATA0CH0_CR_DDRDATADQRANK0LANE0_STRUCT DATA1CH0_CR_DDRDATADQRANK1LANE3_STRUCT;

typedef DATA0CH0_CR_DDRDATADQRANK0LANE0_STRUCT DATA1CH0_CR_DDRDATADQRANK1LANE4_STRUCT;

typedef DATA0CH0_CR_DDRDATADQRANK0LANE0_STRUCT DATA1CH0_CR_DDRDATADQRANK1LANE5_STRUCT;

typedef DATA0CH0_CR_DDRDATADQRANK0LANE0_STRUCT DATA1CH0_CR_DDRDATADQRANK1LANE6_STRUCT;

typedef DATA0CH0_CR_DDRDATADQRANK0LANE0_STRUCT DATA1CH0_CR_DDRDATADQRANK1LANE7_STRUCT;

typedef DATA0CH0_CR_DDRDATADQRANK0LANE0_STRUCT DATA1CH0_CR_DDRDATADQRANK2LANE0_STRUCT;

typedef DATA0CH0_CR_DDRDATADQRANK0LANE0_STRUCT DATA1CH0_CR_DDRDATADQRANK2LANE1_STRUCT;

typedef DATA0CH0_CR_DDRDATADQRANK0LANE0_STRUCT DATA1CH0_CR_DDRDATADQRANK2LANE2_STRUCT;

typedef DATA0CH0_CR_DDRDATADQRANK0LANE0_STRUCT DATA1CH0_CR_DDRDATADQRANK2LANE3_STRUCT;

typedef DATA0CH0_CR_DDRDATADQRANK0LANE0_STRUCT DATA1CH0_CR_DDRDATADQRANK2LANE4_STRUCT;

typedef DATA0CH0_CR_DDRDATADQRANK0LANE0_STRUCT DATA1CH0_CR_DDRDATADQRANK2LANE5_STRUCT;

typedef DATA0CH0_CR_DDRDATADQRANK0LANE0_STRUCT DATA1CH0_CR_DDRDATADQRANK2LANE6_STRUCT;

typedef DATA0CH0_CR_DDRDATADQRANK0LANE0_STRUCT DATA1CH0_CR_DDRDATADQRANK2LANE7_STRUCT;

typedef DATA0CH0_CR_DDRDATADQRANK0LANE0_STRUCT DATA1CH0_CR_DDRDATADQRANK3LANE0_STRUCT;

typedef DATA0CH0_CR_DDRDATADQRANK0LANE0_STRUCT DATA1CH0_CR_DDRDATADQRANK3LANE1_STRUCT;

typedef DATA0CH0_CR_DDRDATADQRANK0LANE0_STRUCT DATA1CH0_CR_DDRDATADQRANK3LANE2_STRUCT;

typedef DATA0CH0_CR_DDRDATADQRANK0LANE0_STRUCT DATA1CH0_CR_DDRDATADQRANK3LANE3_STRUCT;

typedef DATA0CH0_CR_DDRDATADQRANK0LANE0_STRUCT DATA1CH0_CR_DDRDATADQRANK3LANE4_STRUCT;

typedef DATA0CH0_CR_DDRDATADQRANK0LANE0_STRUCT DATA1CH0_CR_DDRDATADQRANK3LANE5_STRUCT;

typedef DATA0CH0_CR_DDRDATADQRANK0LANE0_STRUCT DATA1CH0_CR_DDRDATADQRANK3LANE6_STRUCT;

typedef DATA0CH0_CR_DDRDATADQRANK0LANE0_STRUCT DATA1CH0_CR_DDRDATADQRANK3LANE7_STRUCT;

typedef DATA0CH0_CR_DDRCRDATACONTROL0_STRUCT DATA1CH0_CR_DDRCRDATACONTROL0_STRUCT;

typedef DATA0CH0_CR_DDRCRDATACONTROL1_STRUCT DATA1CH0_CR_DDRCRDATACONTROL1_STRUCT;

typedef DATA0CH0_CR_DDRCRDATACONTROL2_STRUCT DATA1CH0_CR_DDRCRDATACONTROL2_STRUCT;

typedef DATA0CH0_CR_DDRCRDATACONTROL3_STRUCT DATA1CH0_CR_DDRCRDATACONTROL3_STRUCT;

typedef DATA0CH0_CR_AFEMISC_STRUCT DATA1CH0_CR_AFEMISC_STRUCT;

typedef DATA0CH0_CR_SRZCTL_STRUCT DATA1CH0_CR_SRZCTL_STRUCT;

typedef DATA0CH0_CR_DQRXCTL0_STRUCT DATA1CH0_CR_DQRXCTL0_STRUCT;

typedef DATA0CH0_CR_DQRXCTL1_STRUCT DATA1CH0_CR_DQRXCTL1_STRUCT;

typedef DATA0CH0_CR_DQSTXRXCTL_STRUCT DATA1CH0_CR_DQSTXRXCTL_STRUCT;

typedef DATA0CH0_CR_DQSTXRXCTL0_STRUCT DATA1CH0_CR_DQSTXRXCTL0_STRUCT;

typedef DATA0CH0_CR_SDLCTL1_STRUCT DATA1CH0_CR_SDLCTL1_STRUCT;

typedef DATA0CH0_CR_SDLCTL0_STRUCT DATA1CH0_CR_SDLCTL0_STRUCT;

typedef DATA0CH0_CR_MDLLCTL0_STRUCT DATA1CH0_CR_MDLLCTL0_STRUCT;

typedef DATA0CH0_CR_MDLLCTL1_STRUCT DATA1CH0_CR_MDLLCTL1_STRUCT;

typedef DATA0CH0_CR_MDLLCTL2_STRUCT DATA1CH0_CR_MDLLCTL2_STRUCT;

typedef DATA0CH0_CR_MDLLCTL3_STRUCT DATA1CH0_CR_MDLLCTL3_STRUCT;

typedef DATA0CH0_CR_MDLLCTL4_STRUCT DATA1CH0_CR_MDLLCTL4_STRUCT;

typedef DATA0CH0_CR_MDLLCTL4_STRUCT DATA1CH0_CR_MDLLCTL5_STRUCT;

typedef DATA0CH0_CR_DIGMISCCTRL_STRUCT DATA1CH0_CR_DIGMISCCTRL_STRUCT;

typedef DATA0CH0_CR_AFEMISCCTRL2_STRUCT DATA1CH0_CR_AFEMISCCTRL2_STRUCT;

typedef DATA0CH0_CR_TXPBDOFFSET0_STRUCT DATA1CH0_CR_TXPBDOFFSET0_STRUCT;

typedef DATA0CH0_CR_TXPBDOFFSET1_STRUCT DATA1CH0_CR_TXPBDOFFSET1_STRUCT;

typedef DATA0CH0_CR_COMPCTL0_STRUCT DATA1CH0_CR_COMPCTL0_STRUCT;

typedef DATA0CH0_CR_COMPCTL1_STRUCT DATA1CH0_CR_COMPCTL1_STRUCT;

typedef DATA0CH0_CR_COMPCTL2_STRUCT DATA1CH0_CR_COMPCTL2_STRUCT;

typedef DATA0CH0_CR_DCCCTL5_STRUCT DATA1CH0_CR_DCCCTL5_STRUCT;

typedef DATA0CH0_CR_DCCCTL6_STRUCT DATA1CH0_CR_DCCCTL6_STRUCT;

typedef DATA0CH0_CR_DCCCTL7_STRUCT DATA1CH0_CR_DCCCTL7_STRUCT;

typedef DATA0CH0_CR_DCCCTL8_STRUCT DATA1CH0_CR_DCCCTL8_STRUCT;

typedef DATA0CH0_CR_DCCCTL9_STRUCT DATA1CH0_CR_DCCCTL9_STRUCT;

typedef DATA0CH0_CR_RXCONTROL0RANK0_STRUCT DATA1CH0_CR_RXCONTROL0RANK0_STRUCT;

typedef DATA0CH0_CR_RXCONTROL0RANK0_STRUCT DATA1CH0_CR_RXCONTROL0RANK1_STRUCT;

typedef DATA0CH0_CR_RXCONTROL0RANK0_STRUCT DATA1CH0_CR_RXCONTROL0RANK2_STRUCT;

typedef DATA0CH0_CR_RXCONTROL0RANK0_STRUCT DATA1CH0_CR_RXCONTROL0RANK3_STRUCT;

typedef DATA0CH0_CR_TXCONTROL0_PH90_STRUCT DATA1CH0_CR_TXCONTROL0_PH90_STRUCT;

typedef DATA0CH0_CR_TXCONTROL0RANK0_STRUCT DATA1CH0_CR_TXCONTROL0RANK0_STRUCT;

typedef DATA0CH0_CR_TXCONTROL0RANK0_STRUCT DATA1CH0_CR_TXCONTROL0RANK1_STRUCT;

typedef DATA0CH0_CR_TXCONTROL0RANK0_STRUCT DATA1CH0_CR_TXCONTROL0RANK2_STRUCT;

typedef DATA0CH0_CR_TXCONTROL0RANK0_STRUCT DATA1CH0_CR_TXCONTROL0RANK3_STRUCT;

typedef DATA0CH0_CR_DDRDATADQSRANKX_STRUCT DATA1CH0_CR_DDRDATADQSRANKX_STRUCT;

typedef DATA0CH0_CR_DDRDATADQSPH90RANKX_STRUCT DATA1CH0_CR_DDRDATADQSPH90RANKX_STRUCT;

typedef DATA0CH0_CR_DDRCRDATAOFFSETCOMP_STRUCT DATA1CH0_CR_DDRCRDATAOFFSETCOMP_STRUCT;

typedef DATA0CH0_CR_DDRCRDATAOFFSETTRAIN_STRUCT DATA1CH0_CR_DDRCRDATAOFFSETTRAIN_STRUCT;

typedef DATA0CH0_CR_DDRCRCMDBUSTRAIN_STRUCT DATA1CH0_CR_DDRCRCMDBUSTRAIN_STRUCT;

typedef DATA0CH0_CR_DDRCRWRRETRAINSWIZZLECONTROL_STRUCT DATA1CH0_CR_DDRCRWRRETRAINSWIZZLECONTROL_STRUCT;

typedef DATA0CH0_CR_DDRCRWRRETRAINRANK3_STRUCT DATA1CH0_CR_DDRCRWRRETRAINRANK3_STRUCT;

typedef DATA0CH0_CR_DDRCRWRRETRAINRANK3_STRUCT DATA1CH0_CR_DDRCRWRRETRAINRANK2_STRUCT;

typedef DATA0CH0_CR_DDRCRWRRETRAINRANK3_STRUCT DATA1CH0_CR_DDRCRWRRETRAINRANK1_STRUCT;

typedef DATA0CH0_CR_DDRCRWRRETRAINRANK3_STRUCT DATA1CH0_CR_DDRCRWRRETRAINRANK0_STRUCT;

typedef DATA0CH0_CR_DDRCRWRRETRAINCONTROLSTATUS_STRUCT DATA1CH0_CR_DDRCRWRRETRAINCONTROLSTATUS_STRUCT;

typedef DATA0CH0_CR_TXCONTROL1_PH90_STRUCT DATA1CH0_CR_TXCONTROL1_PH90_STRUCT;

typedef DATA0CH0_CR_AFEMISCCTRL1_STRUCT DATA1CH0_CR_AFEMISCCTRL1_STRUCT;

typedef DATA0CH0_CR_DQXRXAMPCTL_STRUCT DATA1CH0_CR_DQXRXAMPCTL_STRUCT;

typedef DATA0CH0_CR_DQ0RXAMPCTL_STRUCT DATA1CH0_CR_DQ0RXAMPCTL_STRUCT;

typedef DATA0CH0_CR_DQ1RXAMPCTL_STRUCT DATA1CH0_CR_DQ1RXAMPCTL_STRUCT;

typedef DATA0CH0_CR_DQ2RXAMPCTL_STRUCT DATA1CH0_CR_DQ2RXAMPCTL_STRUCT;

typedef DATA0CH0_CR_DQ3RXAMPCTL_STRUCT DATA1CH0_CR_DQ3RXAMPCTL_STRUCT;

typedef DATA0CH0_CR_DQ4RXAMPCTL_STRUCT DATA1CH0_CR_DQ4RXAMPCTL_STRUCT;

typedef DATA0CH0_CR_DQ5RXAMPCTL_STRUCT DATA1CH0_CR_DQ5RXAMPCTL_STRUCT;

typedef DATA0CH0_CR_DQ6RXAMPCTL_STRUCT DATA1CH0_CR_DQ6RXAMPCTL_STRUCT;

typedef DATA0CH0_CR_DQ7RXAMPCTL_STRUCT DATA1CH0_CR_DQ7RXAMPCTL_STRUCT;

typedef DATA0CH0_CR_DQRXAMPCTL0_STRUCT DATA1CH0_CR_DQRXAMPCTL0_STRUCT;

typedef DATA0CH0_CR_DQRXAMPCTL1_STRUCT DATA1CH0_CR_DQRXAMPCTL1_STRUCT;

typedef DATA0CH0_CR_DQRXAMPCTL2_STRUCT DATA1CH0_CR_DQRXAMPCTL2_STRUCT;

typedef DATA0CH0_CR_DQTXCTL0_STRUCT DATA1CH0_CR_DQTXCTL0_STRUCT;

typedef DATA0CH0_CR_DCCCTL0_STRUCT DATA1CH0_CR_DCCCTL0_STRUCT;

typedef DATA0CH0_CR_DCCCTL1_STRUCT DATA1CH0_CR_DCCCTL1_STRUCT;

typedef DATA0CH0_CR_DCCCTL2_STRUCT DATA1CH0_CR_DCCCTL2_STRUCT;

typedef DATA0CH0_CR_DCCCTL3_STRUCT DATA1CH0_CR_DCCCTL3_STRUCT;

typedef DATA0CH0_CR_DCCCTL10_STRUCT DATA1CH0_CR_DCCCTL10_STRUCT;

typedef DATA0CH0_CR_DCCCTL12_STRUCT DATA1CH0_CR_DCCCTL12_STRUCT;

typedef DATA0CH0_CR_CLKALIGNCTL0_STRUCT DATA1CH0_CR_CLKALIGNCTL0_STRUCT;

typedef DATA0CH0_CR_CLKALIGNCTL1_STRUCT DATA1CH0_CR_CLKALIGNCTL1_STRUCT;

typedef DATA0CH0_CR_CLKALIGNCTL2_STRUCT DATA1CH0_CR_CLKALIGNCTL2_STRUCT;

typedef DATA0CH0_CR_DCCCTL4_STRUCT DATA1CH0_CR_DCCCTL4_STRUCT;

typedef DATA0CH0_CR_DDRCRDATACOMPVTT_STRUCT DATA1CH0_CR_DDRCRDATACOMPVTT_STRUCT;

typedef DATA0CH0_CR_WLCTRL_STRUCT DATA1CH0_CR_WLCTRL_STRUCT;

typedef DATA0CH0_CR_LPMODECTL_STRUCT DATA1CH0_CR_LPMODECTL_STRUCT;

typedef DATA0CH0_CR_DDRCRDATACOMP0_STRUCT DATA1CH0_CR_DDRCRDATACOMP0_STRUCT;

typedef DATA0CH0_CR_DDRCRDATACOMP1_STRUCT DATA1CH0_CR_DDRCRDATACOMP1_STRUCT;

typedef DATA0CH0_CR_VCCDLLCOMPDATA_STRUCT DATA1CH0_CR_VCCDLLCOMPDATA_STRUCT;

typedef DATA0CH0_CR_DCSOFFSET_STRUCT DATA1CH0_CR_DCSOFFSET_STRUCT;

typedef DATA0CH0_CR_DFXCCMON_STRUCT DATA1CH0_CR_DFXCCMON_STRUCT;

typedef DATA0CH0_CR_AFEMISCCTRL0_STRUCT DATA1CH0_CR_AFEMISCCTRL0_STRUCT;

typedef DATA0CH0_CR_VIEWCTL_STRUCT DATA1CH0_CR_VIEWCTL_STRUCT;

typedef DATA0CH0_CR_DCCCTL11_STRUCT DATA1CH0_CR_DCCCTL11_STRUCT;

typedef DATA0CH0_CR_DCCCTL13_STRUCT DATA1CH0_CR_DCCCTL13_STRUCT;

typedef DATA0CH0_CR_PMFSM_STRUCT DATA1CH0_CR_PMFSM_STRUCT;

typedef DATA0CH0_CR_PMFSM1_STRUCT DATA1CH0_CR_PMFSM1_STRUCT;

typedef DATA0CH0_CR_CLKALIGNSTATUS_STRUCT DATA1CH0_CR_CLKALIGNSTATUS_STRUCT;

typedef DATA0CH0_CR_DATATRAINFEEDBACK_STRUCT DATA1CH0_CR_DATATRAINFEEDBACK_STRUCT;

typedef DATA0CH0_CR_DDRCRMARGINMODECONTROL0_STRUCT DATA1CH0_CR_DDRCRMARGINMODECONTROL0_STRUCT;

typedef DATA0CH0_CR_DDRCRMARGINMODEDEBUGMSB0_STRUCT DATA1CH0_CR_DDRCRMARGINMODEDEBUGMSB0_STRUCT;

typedef DATA0CH0_CR_DDRCRMARGINMODEDEBUGMSB0_STRUCT DATA1CH0_CR_DDRCRMARGINMODEDEBUGLSB0_STRUCT;

typedef DATA0CH0_CR_DCCCTL14_STRUCT DATA1CH0_CR_DCCCTL14_STRUCT;

typedef DATA0CH0_CR_DDRDATADQRANK0LANE0_STRUCT DATA1CH1_CR_DDRDATADQRANK0LANE0_STRUCT;

typedef DATA0CH0_CR_DDRDATADQRANK0LANE0_STRUCT DATA1CH1_CR_DDRDATADQRANK0LANE1_STRUCT;

typedef DATA0CH0_CR_DDRDATADQRANK0LANE0_STRUCT DATA1CH1_CR_DDRDATADQRANK0LANE2_STRUCT;

typedef DATA0CH0_CR_DDRDATADQRANK0LANE0_STRUCT DATA1CH1_CR_DDRDATADQRANK0LANE3_STRUCT;

typedef DATA0CH0_CR_DDRDATADQRANK0LANE0_STRUCT DATA1CH1_CR_DDRDATADQRANK0LANE4_STRUCT;

typedef DATA0CH0_CR_DDRDATADQRANK0LANE0_STRUCT DATA1CH1_CR_DDRDATADQRANK0LANE5_STRUCT;

typedef DATA0CH0_CR_DDRDATADQRANK0LANE0_STRUCT DATA1CH1_CR_DDRDATADQRANK0LANE6_STRUCT;

typedef DATA0CH0_CR_DDRDATADQRANK0LANE0_STRUCT DATA1CH1_CR_DDRDATADQRANK0LANE7_STRUCT;

typedef DATA0CH0_CR_DDRDATADQRANK0LANE0_STRUCT DATA1CH1_CR_DDRDATADQRANK1LANE0_STRUCT;

typedef DATA0CH0_CR_DDRDATADQRANK0LANE0_STRUCT DATA1CH1_CR_DDRDATADQRANK1LANE1_STRUCT;

typedef DATA0CH0_CR_DDRDATADQRANK0LANE0_STRUCT DATA1CH1_CR_DDRDATADQRANK1LANE2_STRUCT;

typedef DATA0CH0_CR_DDRDATADQRANK0LANE0_STRUCT DATA1CH1_CR_DDRDATADQRANK1LANE3_STRUCT;

typedef DATA0CH0_CR_DDRDATADQRANK0LANE0_STRUCT DATA1CH1_CR_DDRDATADQRANK1LANE4_STRUCT;

typedef DATA0CH0_CR_DDRDATADQRANK0LANE0_STRUCT DATA1CH1_CR_DDRDATADQRANK1LANE5_STRUCT;

typedef DATA0CH0_CR_DDRDATADQRANK0LANE0_STRUCT DATA1CH1_CR_DDRDATADQRANK1LANE6_STRUCT;

typedef DATA0CH0_CR_DDRDATADQRANK0LANE0_STRUCT DATA1CH1_CR_DDRDATADQRANK1LANE7_STRUCT;

typedef DATA0CH0_CR_DDRDATADQRANK0LANE0_STRUCT DATA1CH1_CR_DDRDATADQRANK2LANE0_STRUCT;

typedef DATA0CH0_CR_DDRDATADQRANK0LANE0_STRUCT DATA1CH1_CR_DDRDATADQRANK2LANE1_STRUCT;

typedef DATA0CH0_CR_DDRDATADQRANK0LANE0_STRUCT DATA1CH1_CR_DDRDATADQRANK2LANE2_STRUCT;

typedef DATA0CH0_CR_DDRDATADQRANK0LANE0_STRUCT DATA1CH1_CR_DDRDATADQRANK2LANE3_STRUCT;

typedef DATA0CH0_CR_DDRDATADQRANK0LANE0_STRUCT DATA1CH1_CR_DDRDATADQRANK2LANE4_STRUCT;

typedef DATA0CH0_CR_DDRDATADQRANK0LANE0_STRUCT DATA1CH1_CR_DDRDATADQRANK2LANE5_STRUCT;

typedef DATA0CH0_CR_DDRDATADQRANK0LANE0_STRUCT DATA1CH1_CR_DDRDATADQRANK2LANE6_STRUCT;

typedef DATA0CH0_CR_DDRDATADQRANK0LANE0_STRUCT DATA1CH1_CR_DDRDATADQRANK2LANE7_STRUCT;

typedef DATA0CH0_CR_DDRDATADQRANK0LANE0_STRUCT DATA1CH1_CR_DDRDATADQRANK3LANE0_STRUCT;

typedef DATA0CH0_CR_DDRDATADQRANK0LANE0_STRUCT DATA1CH1_CR_DDRDATADQRANK3LANE1_STRUCT;

typedef DATA0CH0_CR_DDRDATADQRANK0LANE0_STRUCT DATA1CH1_CR_DDRDATADQRANK3LANE2_STRUCT;

typedef DATA0CH0_CR_DDRDATADQRANK0LANE0_STRUCT DATA1CH1_CR_DDRDATADQRANK3LANE3_STRUCT;

typedef DATA0CH0_CR_DDRDATADQRANK0LANE0_STRUCT DATA1CH1_CR_DDRDATADQRANK3LANE4_STRUCT;

typedef DATA0CH0_CR_DDRDATADQRANK0LANE0_STRUCT DATA1CH1_CR_DDRDATADQRANK3LANE5_STRUCT;

typedef DATA0CH0_CR_DDRDATADQRANK0LANE0_STRUCT DATA1CH1_CR_DDRDATADQRANK3LANE6_STRUCT;

typedef DATA0CH0_CR_DDRDATADQRANK0LANE0_STRUCT DATA1CH1_CR_DDRDATADQRANK3LANE7_STRUCT;

typedef DATA0CH0_CR_DDRCRDATACONTROL0_STRUCT DATA1CH1_CR_DDRCRDATACONTROL0_STRUCT;

typedef DATA0CH0_CR_DDRCRDATACONTROL1_STRUCT DATA1CH1_CR_DDRCRDATACONTROL1_STRUCT;

typedef DATA0CH0_CR_DDRCRDATACONTROL2_STRUCT DATA1CH1_CR_DDRCRDATACONTROL2_STRUCT;

typedef DATA0CH0_CR_DDRCRDATACONTROL3_STRUCT DATA1CH1_CR_DDRCRDATACONTROL3_STRUCT;

typedef DATA0CH0_CR_AFEMISC_STRUCT DATA1CH1_CR_AFEMISC_STRUCT;

typedef DATA0CH0_CR_SRZCTL_STRUCT DATA1CH1_CR_SRZCTL_STRUCT;

typedef DATA0CH0_CR_DQRXCTL0_STRUCT DATA1CH1_CR_DQRXCTL0_STRUCT;

typedef DATA0CH0_CR_DQRXCTL1_STRUCT DATA1CH1_CR_DQRXCTL1_STRUCT;

typedef DATA0CH0_CR_DQSTXRXCTL_STRUCT DATA1CH1_CR_DQSTXRXCTL_STRUCT;

typedef DATA0CH0_CR_DQSTXRXCTL0_STRUCT DATA1CH1_CR_DQSTXRXCTL0_STRUCT;

typedef DATA0CH0_CR_SDLCTL1_STRUCT DATA1CH1_CR_SDLCTL1_STRUCT;

typedef DATA0CH0_CR_SDLCTL0_STRUCT DATA1CH1_CR_SDLCTL0_STRUCT;

typedef DATA0CH0_CR_MDLLCTL0_STRUCT DATA1CH1_CR_MDLLCTL0_STRUCT;

typedef DATA0CH0_CR_MDLLCTL1_STRUCT DATA1CH1_CR_MDLLCTL1_STRUCT;

typedef DATA0CH0_CR_MDLLCTL2_STRUCT DATA1CH1_CR_MDLLCTL2_STRUCT;

typedef DATA0CH0_CR_MDLLCTL3_STRUCT DATA1CH1_CR_MDLLCTL3_STRUCT;

typedef DATA0CH0_CR_MDLLCTL4_STRUCT DATA1CH1_CR_MDLLCTL4_STRUCT;

typedef DATA0CH0_CR_MDLLCTL4_STRUCT DATA1CH1_CR_MDLLCTL5_STRUCT;

typedef DATA0CH0_CR_DIGMISCCTRL_STRUCT DATA1CH1_CR_DIGMISCCTRL_STRUCT;

typedef DATA0CH0_CR_AFEMISCCTRL2_STRUCT DATA1CH1_CR_AFEMISCCTRL2_STRUCT;

typedef DATA0CH0_CR_TXPBDOFFSET0_STRUCT DATA1CH1_CR_TXPBDOFFSET0_STRUCT;

typedef DATA0CH0_CR_TXPBDOFFSET1_STRUCT DATA1CH1_CR_TXPBDOFFSET1_STRUCT;

typedef DATA0CH0_CR_COMPCTL0_STRUCT DATA1CH1_CR_COMPCTL0_STRUCT;

typedef DATA0CH0_CR_COMPCTL1_STRUCT DATA1CH1_CR_COMPCTL1_STRUCT;

typedef DATA0CH0_CR_COMPCTL2_STRUCT DATA1CH1_CR_COMPCTL2_STRUCT;

typedef DATA0CH0_CR_DCCCTL5_STRUCT DATA1CH1_CR_DCCCTL5_STRUCT;

typedef DATA0CH0_CR_DCCCTL6_STRUCT DATA1CH1_CR_DCCCTL6_STRUCT;

typedef DATA0CH0_CR_DCCCTL7_STRUCT DATA1CH1_CR_DCCCTL7_STRUCT;

typedef DATA0CH0_CR_DCCCTL8_STRUCT DATA1CH1_CR_DCCCTL8_STRUCT;

typedef DATA0CH0_CR_DCCCTL9_STRUCT DATA1CH1_CR_DCCCTL9_STRUCT;

typedef DATA0CH0_CR_RXCONTROL0RANK0_STRUCT DATA1CH1_CR_RXCONTROL0RANK0_STRUCT;

typedef DATA0CH0_CR_RXCONTROL0RANK0_STRUCT DATA1CH1_CR_RXCONTROL0RANK1_STRUCT;

typedef DATA0CH0_CR_RXCONTROL0RANK0_STRUCT DATA1CH1_CR_RXCONTROL0RANK2_STRUCT;

typedef DATA0CH0_CR_RXCONTROL0RANK0_STRUCT DATA1CH1_CR_RXCONTROL0RANK3_STRUCT;

typedef DATA0CH0_CR_TXCONTROL0_PH90_STRUCT DATA1CH1_CR_TXCONTROL0_PH90_STRUCT;

typedef DATA0CH0_CR_TXCONTROL0RANK0_STRUCT DATA1CH1_CR_TXCONTROL0RANK0_STRUCT;

typedef DATA0CH0_CR_TXCONTROL0RANK0_STRUCT DATA1CH1_CR_TXCONTROL0RANK1_STRUCT;

typedef DATA0CH0_CR_TXCONTROL0RANK0_STRUCT DATA1CH1_CR_TXCONTROL0RANK2_STRUCT;

typedef DATA0CH0_CR_TXCONTROL0RANK0_STRUCT DATA1CH1_CR_TXCONTROL0RANK3_STRUCT;

typedef DATA0CH0_CR_DDRDATADQSRANKX_STRUCT DATA1CH1_CR_DDRDATADQSRANKX_STRUCT;

typedef DATA0CH0_CR_DDRDATADQSPH90RANKX_STRUCT DATA1CH1_CR_DDRDATADQSPH90RANKX_STRUCT;

typedef DATA0CH0_CR_DDRCRDATAOFFSETCOMP_STRUCT DATA1CH1_CR_DDRCRDATAOFFSETCOMP_STRUCT;

typedef DATA0CH0_CR_DDRCRDATAOFFSETTRAIN_STRUCT DATA1CH1_CR_DDRCRDATAOFFSETTRAIN_STRUCT;

typedef DATA0CH0_CR_DDRCRCMDBUSTRAIN_STRUCT DATA1CH1_CR_DDRCRCMDBUSTRAIN_STRUCT;

typedef DATA0CH0_CR_DDRCRWRRETRAINSWIZZLECONTROL_STRUCT DATA1CH1_CR_DDRCRWRRETRAINSWIZZLECONTROL_STRUCT;

typedef DATA0CH0_CR_DDRCRWRRETRAINRANK3_STRUCT DATA1CH1_CR_DDRCRWRRETRAINRANK3_STRUCT;

typedef DATA0CH0_CR_DDRCRWRRETRAINRANK3_STRUCT DATA1CH1_CR_DDRCRWRRETRAINRANK2_STRUCT;

typedef DATA0CH0_CR_DDRCRWRRETRAINRANK3_STRUCT DATA1CH1_CR_DDRCRWRRETRAINRANK1_STRUCT;

typedef DATA0CH0_CR_DDRCRWRRETRAINRANK3_STRUCT DATA1CH1_CR_DDRCRWRRETRAINRANK0_STRUCT;

typedef DATA0CH0_CR_DDRCRWRRETRAINCONTROLSTATUS_STRUCT DATA1CH1_CR_DDRCRWRRETRAINCONTROLSTATUS_STRUCT;

typedef DATA0CH0_CR_TXCONTROL1_PH90_STRUCT DATA1CH1_CR_TXCONTROL1_PH90_STRUCT;

typedef DATA0CH0_CR_AFEMISCCTRL1_STRUCT DATA1CH1_CR_AFEMISCCTRL1_STRUCT;

typedef DATA0CH0_CR_DQXRXAMPCTL_STRUCT DATA1CH1_CR_DQXRXAMPCTL_STRUCT;

typedef DATA0CH0_CR_DQ0RXAMPCTL_STRUCT DATA1CH1_CR_DQ0RXAMPCTL_STRUCT;

typedef DATA0CH0_CR_DQ1RXAMPCTL_STRUCT DATA1CH1_CR_DQ1RXAMPCTL_STRUCT;

typedef DATA0CH0_CR_DQ2RXAMPCTL_STRUCT DATA1CH1_CR_DQ2RXAMPCTL_STRUCT;

typedef DATA0CH0_CR_DQ3RXAMPCTL_STRUCT DATA1CH1_CR_DQ3RXAMPCTL_STRUCT;

typedef DATA0CH0_CR_DQ4RXAMPCTL_STRUCT DATA1CH1_CR_DQ4RXAMPCTL_STRUCT;

typedef DATA0CH0_CR_DQ5RXAMPCTL_STRUCT DATA1CH1_CR_DQ5RXAMPCTL_STRUCT;

typedef DATA0CH0_CR_DQ6RXAMPCTL_STRUCT DATA1CH1_CR_DQ6RXAMPCTL_STRUCT;

typedef DATA0CH0_CR_DQ7RXAMPCTL_STRUCT DATA1CH1_CR_DQ7RXAMPCTL_STRUCT;

typedef DATA0CH0_CR_DQRXAMPCTL0_STRUCT DATA1CH1_CR_DQRXAMPCTL0_STRUCT;

typedef DATA0CH0_CR_DQRXAMPCTL1_STRUCT DATA1CH1_CR_DQRXAMPCTL1_STRUCT;

typedef DATA0CH0_CR_DQRXAMPCTL2_STRUCT DATA1CH1_CR_DQRXAMPCTL2_STRUCT;

typedef DATA0CH0_CR_DQTXCTL0_STRUCT DATA1CH1_CR_DQTXCTL0_STRUCT;

typedef DATA0CH0_CR_DCCCTL0_STRUCT DATA1CH1_CR_DCCCTL0_STRUCT;

typedef DATA0CH0_CR_DCCCTL1_STRUCT DATA1CH1_CR_DCCCTL1_STRUCT;

typedef DATA0CH0_CR_DCCCTL2_STRUCT DATA1CH1_CR_DCCCTL2_STRUCT;

typedef DATA0CH0_CR_DCCCTL3_STRUCT DATA1CH1_CR_DCCCTL3_STRUCT;

typedef DATA0CH0_CR_DCCCTL10_STRUCT DATA1CH1_CR_DCCCTL10_STRUCT;

typedef DATA0CH0_CR_DCCCTL12_STRUCT DATA1CH1_CR_DCCCTL12_STRUCT;

typedef DATA0CH0_CR_CLKALIGNCTL0_STRUCT DATA1CH1_CR_CLKALIGNCTL0_STRUCT;

typedef DATA0CH0_CR_CLKALIGNCTL1_STRUCT DATA1CH1_CR_CLKALIGNCTL1_STRUCT;

typedef DATA0CH0_CR_CLKALIGNCTL2_STRUCT DATA1CH1_CR_CLKALIGNCTL2_STRUCT;

typedef DATA0CH0_CR_DCCCTL4_STRUCT DATA1CH1_CR_DCCCTL4_STRUCT;

typedef DATA0CH0_CR_DDRCRDATACOMPVTT_STRUCT DATA1CH1_CR_DDRCRDATACOMPVTT_STRUCT;

typedef DATA0CH0_CR_WLCTRL_STRUCT DATA1CH1_CR_WLCTRL_STRUCT;

typedef DATA0CH0_CR_LPMODECTL_STRUCT DATA1CH1_CR_LPMODECTL_STRUCT;

typedef DATA0CH0_CR_DDRCRDATACOMP0_STRUCT DATA1CH1_CR_DDRCRDATACOMP0_STRUCT;

typedef DATA0CH0_CR_DDRCRDATACOMP1_STRUCT DATA1CH1_CR_DDRCRDATACOMP1_STRUCT;

typedef DATA0CH0_CR_VCCDLLCOMPDATA_STRUCT DATA1CH1_CR_VCCDLLCOMPDATA_STRUCT;

typedef DATA0CH0_CR_DCSOFFSET_STRUCT DATA1CH1_CR_DCSOFFSET_STRUCT;

typedef DATA0CH0_CR_DFXCCMON_STRUCT DATA1CH1_CR_DFXCCMON_STRUCT;

typedef DATA0CH0_CR_AFEMISCCTRL0_STRUCT DATA1CH1_CR_AFEMISCCTRL0_STRUCT;

typedef DATA0CH0_CR_VIEWCTL_STRUCT DATA1CH1_CR_VIEWCTL_STRUCT;

typedef DATA0CH0_CR_DCCCTL11_STRUCT DATA1CH1_CR_DCCCTL11_STRUCT;

typedef DATA0CH0_CR_DCCCTL13_STRUCT DATA1CH1_CR_DCCCTL13_STRUCT;

typedef DATA0CH0_CR_PMFSM_STRUCT DATA1CH1_CR_PMFSM_STRUCT;

typedef DATA0CH0_CR_PMFSM1_STRUCT DATA1CH1_CR_PMFSM1_STRUCT;

typedef DATA0CH0_CR_CLKALIGNSTATUS_STRUCT DATA1CH1_CR_CLKALIGNSTATUS_STRUCT;

typedef DATA0CH0_CR_DATATRAINFEEDBACK_STRUCT DATA1CH1_CR_DATATRAINFEEDBACK_STRUCT;

typedef DATA0CH0_CR_DDRCRMARGINMODECONTROL0_STRUCT DATA1CH1_CR_DDRCRMARGINMODECONTROL0_STRUCT;

typedef DATA0CH0_CR_DDRCRMARGINMODEDEBUGMSB0_STRUCT DATA1CH1_CR_DDRCRMARGINMODEDEBUGMSB0_STRUCT;

typedef DATA0CH0_CR_DDRCRMARGINMODEDEBUGMSB0_STRUCT DATA1CH1_CR_DDRCRMARGINMODEDEBUGLSB0_STRUCT;

typedef DATA0CH0_CR_DCCCTL14_STRUCT DATA1CH1_CR_DCCCTL14_STRUCT;

typedef DATA0CH0_CR_DDRDATADQRANK0LANE0_STRUCT DATA2CH0_CR_DDRDATADQRANK0LANE0_STRUCT;

typedef DATA0CH0_CR_DDRDATADQRANK0LANE0_STRUCT DATA2CH0_CR_DDRDATADQRANK0LANE1_STRUCT;

typedef DATA0CH0_CR_DDRDATADQRANK0LANE0_STRUCT DATA2CH0_CR_DDRDATADQRANK0LANE2_STRUCT;

typedef DATA0CH0_CR_DDRDATADQRANK0LANE0_STRUCT DATA2CH0_CR_DDRDATADQRANK0LANE3_STRUCT;

typedef DATA0CH0_CR_DDRDATADQRANK0LANE0_STRUCT DATA2CH0_CR_DDRDATADQRANK0LANE4_STRUCT;

typedef DATA0CH0_CR_DDRDATADQRANK0LANE0_STRUCT DATA2CH0_CR_DDRDATADQRANK0LANE5_STRUCT;

typedef DATA0CH0_CR_DDRDATADQRANK0LANE0_STRUCT DATA2CH0_CR_DDRDATADQRANK0LANE6_STRUCT;

typedef DATA0CH0_CR_DDRDATADQRANK0LANE0_STRUCT DATA2CH0_CR_DDRDATADQRANK0LANE7_STRUCT;

typedef DATA0CH0_CR_DDRDATADQRANK0LANE0_STRUCT DATA2CH0_CR_DDRDATADQRANK1LANE0_STRUCT;

typedef DATA0CH0_CR_DDRDATADQRANK0LANE0_STRUCT DATA2CH0_CR_DDRDATADQRANK1LANE1_STRUCT;

typedef DATA0CH0_CR_DDRDATADQRANK0LANE0_STRUCT DATA2CH0_CR_DDRDATADQRANK1LANE2_STRUCT;

typedef DATA0CH0_CR_DDRDATADQRANK0LANE0_STRUCT DATA2CH0_CR_DDRDATADQRANK1LANE3_STRUCT;

typedef DATA0CH0_CR_DDRDATADQRANK0LANE0_STRUCT DATA2CH0_CR_DDRDATADQRANK1LANE4_STRUCT;

typedef DATA0CH0_CR_DDRDATADQRANK0LANE0_STRUCT DATA2CH0_CR_DDRDATADQRANK1LANE5_STRUCT;

typedef DATA0CH0_CR_DDRDATADQRANK0LANE0_STRUCT DATA2CH0_CR_DDRDATADQRANK1LANE6_STRUCT;

typedef DATA0CH0_CR_DDRDATADQRANK0LANE0_STRUCT DATA2CH0_CR_DDRDATADQRANK1LANE7_STRUCT;

typedef DATA0CH0_CR_DDRDATADQRANK0LANE0_STRUCT DATA2CH0_CR_DDRDATADQRANK2LANE0_STRUCT;

typedef DATA0CH0_CR_DDRDATADQRANK0LANE0_STRUCT DATA2CH0_CR_DDRDATADQRANK2LANE1_STRUCT;

typedef DATA0CH0_CR_DDRDATADQRANK0LANE0_STRUCT DATA2CH0_CR_DDRDATADQRANK2LANE2_STRUCT;

typedef DATA0CH0_CR_DDRDATADQRANK0LANE0_STRUCT DATA2CH0_CR_DDRDATADQRANK2LANE3_STRUCT;

typedef DATA0CH0_CR_DDRDATADQRANK0LANE0_STRUCT DATA2CH0_CR_DDRDATADQRANK2LANE4_STRUCT;

typedef DATA0CH0_CR_DDRDATADQRANK0LANE0_STRUCT DATA2CH0_CR_DDRDATADQRANK2LANE5_STRUCT;

typedef DATA0CH0_CR_DDRDATADQRANK0LANE0_STRUCT DATA2CH0_CR_DDRDATADQRANK2LANE6_STRUCT;

typedef DATA0CH0_CR_DDRDATADQRANK0LANE0_STRUCT DATA2CH0_CR_DDRDATADQRANK2LANE7_STRUCT;

typedef DATA0CH0_CR_DDRDATADQRANK0LANE0_STRUCT DATA2CH0_CR_DDRDATADQRANK3LANE0_STRUCT;

typedef DATA0CH0_CR_DDRDATADQRANK0LANE0_STRUCT DATA2CH0_CR_DDRDATADQRANK3LANE1_STRUCT;

typedef DATA0CH0_CR_DDRDATADQRANK0LANE0_STRUCT DATA2CH0_CR_DDRDATADQRANK3LANE2_STRUCT;

typedef DATA0CH0_CR_DDRDATADQRANK0LANE0_STRUCT DATA2CH0_CR_DDRDATADQRANK3LANE3_STRUCT;

typedef DATA0CH0_CR_DDRDATADQRANK0LANE0_STRUCT DATA2CH0_CR_DDRDATADQRANK3LANE4_STRUCT;

typedef DATA0CH0_CR_DDRDATADQRANK0LANE0_STRUCT DATA2CH0_CR_DDRDATADQRANK3LANE5_STRUCT;

typedef DATA0CH0_CR_DDRDATADQRANK0LANE0_STRUCT DATA2CH0_CR_DDRDATADQRANK3LANE6_STRUCT;

typedef DATA0CH0_CR_DDRDATADQRANK0LANE0_STRUCT DATA2CH0_CR_DDRDATADQRANK3LANE7_STRUCT;

typedef DATA0CH0_CR_DDRCRDATACONTROL0_STRUCT DATA2CH0_CR_DDRCRDATACONTROL0_STRUCT;

typedef DATA0CH0_CR_DDRCRDATACONTROL1_STRUCT DATA2CH0_CR_DDRCRDATACONTROL1_STRUCT;

typedef DATA0CH0_CR_DDRCRDATACONTROL2_STRUCT DATA2CH0_CR_DDRCRDATACONTROL2_STRUCT;

typedef DATA0CH0_CR_DDRCRDATACONTROL3_STRUCT DATA2CH0_CR_DDRCRDATACONTROL3_STRUCT;

typedef DATA0CH0_CR_AFEMISC_STRUCT DATA2CH0_CR_AFEMISC_STRUCT;

typedef DATA0CH0_CR_SRZCTL_STRUCT DATA2CH0_CR_SRZCTL_STRUCT;

typedef DATA0CH0_CR_DQRXCTL0_STRUCT DATA2CH0_CR_DQRXCTL0_STRUCT;

typedef DATA0CH0_CR_DQRXCTL1_STRUCT DATA2CH0_CR_DQRXCTL1_STRUCT;

typedef DATA0CH0_CR_DQSTXRXCTL_STRUCT DATA2CH0_CR_DQSTXRXCTL_STRUCT;

typedef DATA0CH0_CR_DQSTXRXCTL0_STRUCT DATA2CH0_CR_DQSTXRXCTL0_STRUCT;

typedef DATA0CH0_CR_SDLCTL1_STRUCT DATA2CH0_CR_SDLCTL1_STRUCT;

typedef DATA0CH0_CR_SDLCTL0_STRUCT DATA2CH0_CR_SDLCTL0_STRUCT;

typedef DATA0CH0_CR_MDLLCTL0_STRUCT DATA2CH0_CR_MDLLCTL0_STRUCT;

typedef DATA0CH0_CR_MDLLCTL1_STRUCT DATA2CH0_CR_MDLLCTL1_STRUCT;

typedef DATA0CH0_CR_MDLLCTL2_STRUCT DATA2CH0_CR_MDLLCTL2_STRUCT;

typedef DATA0CH0_CR_MDLLCTL3_STRUCT DATA2CH0_CR_MDLLCTL3_STRUCT;

typedef DATA0CH0_CR_MDLLCTL4_STRUCT DATA2CH0_CR_MDLLCTL4_STRUCT;

typedef DATA0CH0_CR_MDLLCTL4_STRUCT DATA2CH0_CR_MDLLCTL5_STRUCT;

typedef DATA0CH0_CR_DIGMISCCTRL_STRUCT DATA2CH0_CR_DIGMISCCTRL_STRUCT;

typedef DATA0CH0_CR_AFEMISCCTRL2_STRUCT DATA2CH0_CR_AFEMISCCTRL2_STRUCT;

typedef DATA0CH0_CR_TXPBDOFFSET0_STRUCT DATA2CH0_CR_TXPBDOFFSET0_STRUCT;

typedef DATA0CH0_CR_TXPBDOFFSET1_STRUCT DATA2CH0_CR_TXPBDOFFSET1_STRUCT;

typedef DATA0CH0_CR_COMPCTL0_STRUCT DATA2CH0_CR_COMPCTL0_STRUCT;

typedef DATA0CH0_CR_COMPCTL1_STRUCT DATA2CH0_CR_COMPCTL1_STRUCT;

typedef DATA0CH0_CR_COMPCTL2_STRUCT DATA2CH0_CR_COMPCTL2_STRUCT;

typedef DATA0CH0_CR_DCCCTL5_STRUCT DATA2CH0_CR_DCCCTL5_STRUCT;

typedef DATA0CH0_CR_DCCCTL6_STRUCT DATA2CH0_CR_DCCCTL6_STRUCT;

typedef DATA0CH0_CR_DCCCTL7_STRUCT DATA2CH0_CR_DCCCTL7_STRUCT;

typedef DATA0CH0_CR_DCCCTL8_STRUCT DATA2CH0_CR_DCCCTL8_STRUCT;

typedef DATA0CH0_CR_DCCCTL9_STRUCT DATA2CH0_CR_DCCCTL9_STRUCT;

typedef DATA0CH0_CR_RXCONTROL0RANK0_STRUCT DATA2CH0_CR_RXCONTROL0RANK0_STRUCT;

typedef DATA0CH0_CR_RXCONTROL0RANK0_STRUCT DATA2CH0_CR_RXCONTROL0RANK1_STRUCT;

typedef DATA0CH0_CR_RXCONTROL0RANK0_STRUCT DATA2CH0_CR_RXCONTROL0RANK2_STRUCT;

typedef DATA0CH0_CR_RXCONTROL0RANK0_STRUCT DATA2CH0_CR_RXCONTROL0RANK3_STRUCT;

typedef DATA0CH0_CR_TXCONTROL0_PH90_STRUCT DATA2CH0_CR_TXCONTROL0_PH90_STRUCT;

typedef DATA0CH0_CR_TXCONTROL0RANK0_STRUCT DATA2CH0_CR_TXCONTROL0RANK0_STRUCT;

typedef DATA0CH0_CR_TXCONTROL0RANK0_STRUCT DATA2CH0_CR_TXCONTROL0RANK1_STRUCT;

typedef DATA0CH0_CR_TXCONTROL0RANK0_STRUCT DATA2CH0_CR_TXCONTROL0RANK2_STRUCT;

typedef DATA0CH0_CR_TXCONTROL0RANK0_STRUCT DATA2CH0_CR_TXCONTROL0RANK3_STRUCT;

typedef DATA0CH0_CR_DDRDATADQSRANKX_STRUCT DATA2CH0_CR_DDRDATADQSRANKX_STRUCT;

typedef DATA0CH0_CR_DDRDATADQSPH90RANKX_STRUCT DATA2CH0_CR_DDRDATADQSPH90RANKX_STRUCT;

typedef DATA0CH0_CR_DDRCRDATAOFFSETCOMP_STRUCT DATA2CH0_CR_DDRCRDATAOFFSETCOMP_STRUCT;

typedef DATA0CH0_CR_DDRCRDATAOFFSETTRAIN_STRUCT DATA2CH0_CR_DDRCRDATAOFFSETTRAIN_STRUCT;

typedef DATA0CH0_CR_DDRCRCMDBUSTRAIN_STRUCT DATA2CH0_CR_DDRCRCMDBUSTRAIN_STRUCT;

typedef DATA0CH0_CR_DDRCRWRRETRAINSWIZZLECONTROL_STRUCT DATA2CH0_CR_DDRCRWRRETRAINSWIZZLECONTROL_STRUCT;

typedef DATA0CH0_CR_DDRCRWRRETRAINRANK3_STRUCT DATA2CH0_CR_DDRCRWRRETRAINRANK3_STRUCT;

typedef DATA0CH0_CR_DDRCRWRRETRAINRANK3_STRUCT DATA2CH0_CR_DDRCRWRRETRAINRANK2_STRUCT;

typedef DATA0CH0_CR_DDRCRWRRETRAINRANK3_STRUCT DATA2CH0_CR_DDRCRWRRETRAINRANK1_STRUCT;

typedef DATA0CH0_CR_DDRCRWRRETRAINRANK3_STRUCT DATA2CH0_CR_DDRCRWRRETRAINRANK0_STRUCT;

typedef DATA0CH0_CR_DDRCRWRRETRAINCONTROLSTATUS_STRUCT DATA2CH0_CR_DDRCRWRRETRAINCONTROLSTATUS_STRUCT;

typedef DATA0CH0_CR_TXCONTROL1_PH90_STRUCT DATA2CH0_CR_TXCONTROL1_PH90_STRUCT;

typedef DATA0CH0_CR_AFEMISCCTRL1_STRUCT DATA2CH0_CR_AFEMISCCTRL1_STRUCT;

typedef DATA0CH0_CR_DQXRXAMPCTL_STRUCT DATA2CH0_CR_DQXRXAMPCTL_STRUCT;

typedef DATA0CH0_CR_DQ0RXAMPCTL_STRUCT DATA2CH0_CR_DQ0RXAMPCTL_STRUCT;

typedef DATA0CH0_CR_DQ1RXAMPCTL_STRUCT DATA2CH0_CR_DQ1RXAMPCTL_STRUCT;

typedef DATA0CH0_CR_DQ2RXAMPCTL_STRUCT DATA2CH0_CR_DQ2RXAMPCTL_STRUCT;

typedef DATA0CH0_CR_DQ3RXAMPCTL_STRUCT DATA2CH0_CR_DQ3RXAMPCTL_STRUCT;

typedef DATA0CH0_CR_DQ4RXAMPCTL_STRUCT DATA2CH0_CR_DQ4RXAMPCTL_STRUCT;

typedef DATA0CH0_CR_DQ5RXAMPCTL_STRUCT DATA2CH0_CR_DQ5RXAMPCTL_STRUCT;

typedef DATA0CH0_CR_DQ6RXAMPCTL_STRUCT DATA2CH0_CR_DQ6RXAMPCTL_STRUCT;

typedef DATA0CH0_CR_DQ7RXAMPCTL_STRUCT DATA2CH0_CR_DQ7RXAMPCTL_STRUCT;

typedef DATA0CH0_CR_DQRXAMPCTL0_STRUCT DATA2CH0_CR_DQRXAMPCTL0_STRUCT;

typedef DATA0CH0_CR_DQRXAMPCTL1_STRUCT DATA2CH0_CR_DQRXAMPCTL1_STRUCT;

typedef DATA0CH0_CR_DQRXAMPCTL2_STRUCT DATA2CH0_CR_DQRXAMPCTL2_STRUCT;

typedef DATA0CH0_CR_DQTXCTL0_STRUCT DATA2CH0_CR_DQTXCTL0_STRUCT;

typedef DATA0CH0_CR_DCCCTL0_STRUCT DATA2CH0_CR_DCCCTL0_STRUCT;

typedef DATA0CH0_CR_DCCCTL1_STRUCT DATA2CH0_CR_DCCCTL1_STRUCT;

typedef DATA0CH0_CR_DCCCTL2_STRUCT DATA2CH0_CR_DCCCTL2_STRUCT;

typedef DATA0CH0_CR_DCCCTL3_STRUCT DATA2CH0_CR_DCCCTL3_STRUCT;

typedef DATA0CH0_CR_DCCCTL10_STRUCT DATA2CH0_CR_DCCCTL10_STRUCT;

typedef DATA0CH0_CR_DCCCTL12_STRUCT DATA2CH0_CR_DCCCTL12_STRUCT;

typedef DATA0CH0_CR_CLKALIGNCTL0_STRUCT DATA2CH0_CR_CLKALIGNCTL0_STRUCT;

typedef DATA0CH0_CR_CLKALIGNCTL1_STRUCT DATA2CH0_CR_CLKALIGNCTL1_STRUCT;

typedef DATA0CH0_CR_CLKALIGNCTL2_STRUCT DATA2CH0_CR_CLKALIGNCTL2_STRUCT;

typedef DATA0CH0_CR_DCCCTL4_STRUCT DATA2CH0_CR_DCCCTL4_STRUCT;

typedef DATA0CH0_CR_DDRCRDATACOMPVTT_STRUCT DATA2CH0_CR_DDRCRDATACOMPVTT_STRUCT;

typedef DATA0CH0_CR_WLCTRL_STRUCT DATA2CH0_CR_WLCTRL_STRUCT;

typedef DATA0CH0_CR_LPMODECTL_STRUCT DATA2CH0_CR_LPMODECTL_STRUCT;

typedef DATA0CH0_CR_DDRCRDATACOMP0_STRUCT DATA2CH0_CR_DDRCRDATACOMP0_STRUCT;

typedef DATA0CH0_CR_DDRCRDATACOMP1_STRUCT DATA2CH0_CR_DDRCRDATACOMP1_STRUCT;

typedef DATA0CH0_CR_VCCDLLCOMPDATA_STRUCT DATA2CH0_CR_VCCDLLCOMPDATA_STRUCT;

typedef DATA0CH0_CR_DCSOFFSET_STRUCT DATA2CH0_CR_DCSOFFSET_STRUCT;

typedef DATA0CH0_CR_DFXCCMON_STRUCT DATA2CH0_CR_DFXCCMON_STRUCT;

typedef DATA0CH0_CR_AFEMISCCTRL0_STRUCT DATA2CH0_CR_AFEMISCCTRL0_STRUCT;

typedef DATA0CH0_CR_VIEWCTL_STRUCT DATA2CH0_CR_VIEWCTL_STRUCT;

typedef DATA0CH0_CR_DCCCTL11_STRUCT DATA2CH0_CR_DCCCTL11_STRUCT;

typedef DATA0CH0_CR_DCCCTL13_STRUCT DATA2CH0_CR_DCCCTL13_STRUCT;

typedef DATA0CH0_CR_PMFSM_STRUCT DATA2CH0_CR_PMFSM_STRUCT;

typedef DATA0CH0_CR_PMFSM1_STRUCT DATA2CH0_CR_PMFSM1_STRUCT;

typedef DATA0CH0_CR_CLKALIGNSTATUS_STRUCT DATA2CH0_CR_CLKALIGNSTATUS_STRUCT;

typedef DATA0CH0_CR_DATATRAINFEEDBACK_STRUCT DATA2CH0_CR_DATATRAINFEEDBACK_STRUCT;

typedef DATA0CH0_CR_DDRCRMARGINMODECONTROL0_STRUCT DATA2CH0_CR_DDRCRMARGINMODECONTROL0_STRUCT;

typedef DATA0CH0_CR_DDRCRMARGINMODEDEBUGMSB0_STRUCT DATA2CH0_CR_DDRCRMARGINMODEDEBUGMSB0_STRUCT;

typedef DATA0CH0_CR_DDRCRMARGINMODEDEBUGMSB0_STRUCT DATA2CH0_CR_DDRCRMARGINMODEDEBUGLSB0_STRUCT;

typedef DATA0CH0_CR_DCCCTL14_STRUCT DATA2CH0_CR_DCCCTL14_STRUCT;

typedef DATA0CH0_CR_DDRDATADQRANK0LANE0_STRUCT DATA2CH1_CR_DDRDATADQRANK0LANE0_STRUCT;

typedef DATA0CH0_CR_DDRDATADQRANK0LANE0_STRUCT DATA2CH1_CR_DDRDATADQRANK0LANE1_STRUCT;

typedef DATA0CH0_CR_DDRDATADQRANK0LANE0_STRUCT DATA2CH1_CR_DDRDATADQRANK0LANE2_STRUCT;

typedef DATA0CH0_CR_DDRDATADQRANK0LANE0_STRUCT DATA2CH1_CR_DDRDATADQRANK0LANE3_STRUCT;

typedef DATA0CH0_CR_DDRDATADQRANK0LANE0_STRUCT DATA2CH1_CR_DDRDATADQRANK0LANE4_STRUCT;

typedef DATA0CH0_CR_DDRDATADQRANK0LANE0_STRUCT DATA2CH1_CR_DDRDATADQRANK0LANE5_STRUCT;

typedef DATA0CH0_CR_DDRDATADQRANK0LANE0_STRUCT DATA2CH1_CR_DDRDATADQRANK0LANE6_STRUCT;

typedef DATA0CH0_CR_DDRDATADQRANK0LANE0_STRUCT DATA2CH1_CR_DDRDATADQRANK0LANE7_STRUCT;

typedef DATA0CH0_CR_DDRDATADQRANK0LANE0_STRUCT DATA2CH1_CR_DDRDATADQRANK1LANE0_STRUCT;

typedef DATA0CH0_CR_DDRDATADQRANK0LANE0_STRUCT DATA2CH1_CR_DDRDATADQRANK1LANE1_STRUCT;

typedef DATA0CH0_CR_DDRDATADQRANK0LANE0_STRUCT DATA2CH1_CR_DDRDATADQRANK1LANE2_STRUCT;

typedef DATA0CH0_CR_DDRDATADQRANK0LANE0_STRUCT DATA2CH1_CR_DDRDATADQRANK1LANE3_STRUCT;

typedef DATA0CH0_CR_DDRDATADQRANK0LANE0_STRUCT DATA2CH1_CR_DDRDATADQRANK1LANE4_STRUCT;

typedef DATA0CH0_CR_DDRDATADQRANK0LANE0_STRUCT DATA2CH1_CR_DDRDATADQRANK1LANE5_STRUCT;

typedef DATA0CH0_CR_DDRDATADQRANK0LANE0_STRUCT DATA2CH1_CR_DDRDATADQRANK1LANE6_STRUCT;

typedef DATA0CH0_CR_DDRDATADQRANK0LANE0_STRUCT DATA2CH1_CR_DDRDATADQRANK1LANE7_STRUCT;

typedef DATA0CH0_CR_DDRDATADQRANK0LANE0_STRUCT DATA2CH1_CR_DDRDATADQRANK2LANE0_STRUCT;

typedef DATA0CH0_CR_DDRDATADQRANK0LANE0_STRUCT DATA2CH1_CR_DDRDATADQRANK2LANE1_STRUCT;

typedef DATA0CH0_CR_DDRDATADQRANK0LANE0_STRUCT DATA2CH1_CR_DDRDATADQRANK2LANE2_STRUCT;

typedef DATA0CH0_CR_DDRDATADQRANK0LANE0_STRUCT DATA2CH1_CR_DDRDATADQRANK2LANE3_STRUCT;

typedef DATA0CH0_CR_DDRDATADQRANK0LANE0_STRUCT DATA2CH1_CR_DDRDATADQRANK2LANE4_STRUCT;

typedef DATA0CH0_CR_DDRDATADQRANK0LANE0_STRUCT DATA2CH1_CR_DDRDATADQRANK2LANE5_STRUCT;

typedef DATA0CH0_CR_DDRDATADQRANK0LANE0_STRUCT DATA2CH1_CR_DDRDATADQRANK2LANE6_STRUCT;

typedef DATA0CH0_CR_DDRDATADQRANK0LANE0_STRUCT DATA2CH1_CR_DDRDATADQRANK2LANE7_STRUCT;

typedef DATA0CH0_CR_DDRDATADQRANK0LANE0_STRUCT DATA2CH1_CR_DDRDATADQRANK3LANE0_STRUCT;

typedef DATA0CH0_CR_DDRDATADQRANK0LANE0_STRUCT DATA2CH1_CR_DDRDATADQRANK3LANE1_STRUCT;

typedef DATA0CH0_CR_DDRDATADQRANK0LANE0_STRUCT DATA2CH1_CR_DDRDATADQRANK3LANE2_STRUCT;

typedef DATA0CH0_CR_DDRDATADQRANK0LANE0_STRUCT DATA2CH1_CR_DDRDATADQRANK3LANE3_STRUCT;

typedef DATA0CH0_CR_DDRDATADQRANK0LANE0_STRUCT DATA2CH1_CR_DDRDATADQRANK3LANE4_STRUCT;

typedef DATA0CH0_CR_DDRDATADQRANK0LANE0_STRUCT DATA2CH1_CR_DDRDATADQRANK3LANE5_STRUCT;

typedef DATA0CH0_CR_DDRDATADQRANK0LANE0_STRUCT DATA2CH1_CR_DDRDATADQRANK3LANE6_STRUCT;

typedef DATA0CH0_CR_DDRDATADQRANK0LANE0_STRUCT DATA2CH1_CR_DDRDATADQRANK3LANE7_STRUCT;

typedef DATA0CH0_CR_DDRCRDATACONTROL0_STRUCT DATA2CH1_CR_DDRCRDATACONTROL0_STRUCT;

typedef DATA0CH0_CR_DDRCRDATACONTROL1_STRUCT DATA2CH1_CR_DDRCRDATACONTROL1_STRUCT;

typedef DATA0CH0_CR_DDRCRDATACONTROL2_STRUCT DATA2CH1_CR_DDRCRDATACONTROL2_STRUCT;

typedef DATA0CH0_CR_DDRCRDATACONTROL3_STRUCT DATA2CH1_CR_DDRCRDATACONTROL3_STRUCT;

typedef DATA0CH0_CR_AFEMISC_STRUCT DATA2CH1_CR_AFEMISC_STRUCT;

typedef DATA0CH0_CR_SRZCTL_STRUCT DATA2CH1_CR_SRZCTL_STRUCT;

typedef DATA0CH0_CR_DQRXCTL0_STRUCT DATA2CH1_CR_DQRXCTL0_STRUCT;

typedef DATA0CH0_CR_DQRXCTL1_STRUCT DATA2CH1_CR_DQRXCTL1_STRUCT;

typedef DATA0CH0_CR_DQSTXRXCTL_STRUCT DATA2CH1_CR_DQSTXRXCTL_STRUCT;

typedef DATA0CH0_CR_DQSTXRXCTL0_STRUCT DATA2CH1_CR_DQSTXRXCTL0_STRUCT;

typedef DATA0CH0_CR_SDLCTL1_STRUCT DATA2CH1_CR_SDLCTL1_STRUCT;

typedef DATA0CH0_CR_SDLCTL0_STRUCT DATA2CH1_CR_SDLCTL0_STRUCT;

typedef DATA0CH0_CR_MDLLCTL0_STRUCT DATA2CH1_CR_MDLLCTL0_STRUCT;

typedef DATA0CH0_CR_MDLLCTL1_STRUCT DATA2CH1_CR_MDLLCTL1_STRUCT;

typedef DATA0CH0_CR_MDLLCTL2_STRUCT DATA2CH1_CR_MDLLCTL2_STRUCT;

typedef DATA0CH0_CR_MDLLCTL3_STRUCT DATA2CH1_CR_MDLLCTL3_STRUCT;

typedef DATA0CH0_CR_MDLLCTL4_STRUCT DATA2CH1_CR_MDLLCTL4_STRUCT;

typedef DATA0CH0_CR_MDLLCTL4_STRUCT DATA2CH1_CR_MDLLCTL5_STRUCT;

typedef DATA0CH0_CR_DIGMISCCTRL_STRUCT DATA2CH1_CR_DIGMISCCTRL_STRUCT;

typedef DATA0CH0_CR_AFEMISCCTRL2_STRUCT DATA2CH1_CR_AFEMISCCTRL2_STRUCT;

typedef DATA0CH0_CR_TXPBDOFFSET0_STRUCT DATA2CH1_CR_TXPBDOFFSET0_STRUCT;

typedef DATA0CH0_CR_TXPBDOFFSET1_STRUCT DATA2CH1_CR_TXPBDOFFSET1_STRUCT;

typedef DATA0CH0_CR_COMPCTL0_STRUCT DATA2CH1_CR_COMPCTL0_STRUCT;

typedef DATA0CH0_CR_COMPCTL1_STRUCT DATA2CH1_CR_COMPCTL1_STRUCT;

typedef DATA0CH0_CR_COMPCTL2_STRUCT DATA2CH1_CR_COMPCTL2_STRUCT;

typedef DATA0CH0_CR_DCCCTL5_STRUCT DATA2CH1_CR_DCCCTL5_STRUCT;

typedef DATA0CH0_CR_DCCCTL6_STRUCT DATA2CH1_CR_DCCCTL6_STRUCT;

typedef DATA0CH0_CR_DCCCTL7_STRUCT DATA2CH1_CR_DCCCTL7_STRUCT;

typedef DATA0CH0_CR_DCCCTL8_STRUCT DATA2CH1_CR_DCCCTL8_STRUCT;

typedef DATA0CH0_CR_DCCCTL9_STRUCT DATA2CH1_CR_DCCCTL9_STRUCT;

typedef DATA0CH0_CR_RXCONTROL0RANK0_STRUCT DATA2CH1_CR_RXCONTROL0RANK0_STRUCT;

typedef DATA0CH0_CR_RXCONTROL0RANK0_STRUCT DATA2CH1_CR_RXCONTROL0RANK1_STRUCT;

typedef DATA0CH0_CR_RXCONTROL0RANK0_STRUCT DATA2CH1_CR_RXCONTROL0RANK2_STRUCT;

typedef DATA0CH0_CR_RXCONTROL0RANK0_STRUCT DATA2CH1_CR_RXCONTROL0RANK3_STRUCT;

typedef DATA0CH0_CR_TXCONTROL0_PH90_STRUCT DATA2CH1_CR_TXCONTROL0_PH90_STRUCT;

typedef DATA0CH0_CR_TXCONTROL0RANK0_STRUCT DATA2CH1_CR_TXCONTROL0RANK0_STRUCT;

typedef DATA0CH0_CR_TXCONTROL0RANK0_STRUCT DATA2CH1_CR_TXCONTROL0RANK1_STRUCT;

typedef DATA0CH0_CR_TXCONTROL0RANK0_STRUCT DATA2CH1_CR_TXCONTROL0RANK2_STRUCT;

typedef DATA0CH0_CR_TXCONTROL0RANK0_STRUCT DATA2CH1_CR_TXCONTROL0RANK3_STRUCT;

typedef DATA0CH0_CR_DDRDATADQSRANKX_STRUCT DATA2CH1_CR_DDRDATADQSRANKX_STRUCT;

typedef DATA0CH0_CR_DDRDATADQSPH90RANKX_STRUCT DATA2CH1_CR_DDRDATADQSPH90RANKX_STRUCT;

typedef DATA0CH0_CR_DDRCRDATAOFFSETCOMP_STRUCT DATA2CH1_CR_DDRCRDATAOFFSETCOMP_STRUCT;

typedef DATA0CH0_CR_DDRCRDATAOFFSETTRAIN_STRUCT DATA2CH1_CR_DDRCRDATAOFFSETTRAIN_STRUCT;

typedef DATA0CH0_CR_DDRCRCMDBUSTRAIN_STRUCT DATA2CH1_CR_DDRCRCMDBUSTRAIN_STRUCT;

typedef DATA0CH0_CR_DDRCRWRRETRAINSWIZZLECONTROL_STRUCT DATA2CH1_CR_DDRCRWRRETRAINSWIZZLECONTROL_STRUCT;

typedef DATA0CH0_CR_DDRCRWRRETRAINRANK3_STRUCT DATA2CH1_CR_DDRCRWRRETRAINRANK3_STRUCT;

typedef DATA0CH0_CR_DDRCRWRRETRAINRANK3_STRUCT DATA2CH1_CR_DDRCRWRRETRAINRANK2_STRUCT;

typedef DATA0CH0_CR_DDRCRWRRETRAINRANK3_STRUCT DATA2CH1_CR_DDRCRWRRETRAINRANK1_STRUCT;

typedef DATA0CH0_CR_DDRCRWRRETRAINRANK3_STRUCT DATA2CH1_CR_DDRCRWRRETRAINRANK0_STRUCT;

typedef DATA0CH0_CR_DDRCRWRRETRAINCONTROLSTATUS_STRUCT DATA2CH1_CR_DDRCRWRRETRAINCONTROLSTATUS_STRUCT;

typedef DATA0CH0_CR_TXCONTROL1_PH90_STRUCT DATA2CH1_CR_TXCONTROL1_PH90_STRUCT;

typedef DATA0CH0_CR_AFEMISCCTRL1_STRUCT DATA2CH1_CR_AFEMISCCTRL1_STRUCT;

typedef DATA0CH0_CR_DQXRXAMPCTL_STRUCT DATA2CH1_CR_DQXRXAMPCTL_STRUCT;

typedef DATA0CH0_CR_DQ0RXAMPCTL_STRUCT DATA2CH1_CR_DQ0RXAMPCTL_STRUCT;

typedef DATA0CH0_CR_DQ1RXAMPCTL_STRUCT DATA2CH1_CR_DQ1RXAMPCTL_STRUCT;

typedef DATA0CH0_CR_DQ2RXAMPCTL_STRUCT DATA2CH1_CR_DQ2RXAMPCTL_STRUCT;

typedef DATA0CH0_CR_DQ3RXAMPCTL_STRUCT DATA2CH1_CR_DQ3RXAMPCTL_STRUCT;

typedef DATA0CH0_CR_DQ4RXAMPCTL_STRUCT DATA2CH1_CR_DQ4RXAMPCTL_STRUCT;

typedef DATA0CH0_CR_DQ5RXAMPCTL_STRUCT DATA2CH1_CR_DQ5RXAMPCTL_STRUCT;

typedef DATA0CH0_CR_DQ6RXAMPCTL_STRUCT DATA2CH1_CR_DQ6RXAMPCTL_STRUCT;

typedef DATA0CH0_CR_DQ7RXAMPCTL_STRUCT DATA2CH1_CR_DQ7RXAMPCTL_STRUCT;

typedef DATA0CH0_CR_DQRXAMPCTL0_STRUCT DATA2CH1_CR_DQRXAMPCTL0_STRUCT;

typedef DATA0CH0_CR_DQRXAMPCTL1_STRUCT DATA2CH1_CR_DQRXAMPCTL1_STRUCT;

typedef DATA0CH0_CR_DQRXAMPCTL2_STRUCT DATA2CH1_CR_DQRXAMPCTL2_STRUCT;

typedef DATA0CH0_CR_DQTXCTL0_STRUCT DATA2CH1_CR_DQTXCTL0_STRUCT;

typedef DATA0CH0_CR_DCCCTL0_STRUCT DATA2CH1_CR_DCCCTL0_STRUCT;

typedef DATA0CH0_CR_DCCCTL1_STRUCT DATA2CH1_CR_DCCCTL1_STRUCT;

typedef DATA0CH0_CR_DCCCTL2_STRUCT DATA2CH1_CR_DCCCTL2_STRUCT;

typedef DATA0CH0_CR_DCCCTL3_STRUCT DATA2CH1_CR_DCCCTL3_STRUCT;

typedef DATA0CH0_CR_DCCCTL10_STRUCT DATA2CH1_CR_DCCCTL10_STRUCT;

typedef DATA0CH0_CR_DCCCTL12_STRUCT DATA2CH1_CR_DCCCTL12_STRUCT;

typedef DATA0CH0_CR_CLKALIGNCTL0_STRUCT DATA2CH1_CR_CLKALIGNCTL0_STRUCT;

typedef DATA0CH0_CR_CLKALIGNCTL1_STRUCT DATA2CH1_CR_CLKALIGNCTL1_STRUCT;

typedef DATA0CH0_CR_CLKALIGNCTL2_STRUCT DATA2CH1_CR_CLKALIGNCTL2_STRUCT;

typedef DATA0CH0_CR_DCCCTL4_STRUCT DATA2CH1_CR_DCCCTL4_STRUCT;

typedef DATA0CH0_CR_DDRCRDATACOMPVTT_STRUCT DATA2CH1_CR_DDRCRDATACOMPVTT_STRUCT;

typedef DATA0CH0_CR_WLCTRL_STRUCT DATA2CH1_CR_WLCTRL_STRUCT;

typedef DATA0CH0_CR_LPMODECTL_STRUCT DATA2CH1_CR_LPMODECTL_STRUCT;

typedef DATA0CH0_CR_DDRCRDATACOMP0_STRUCT DATA2CH1_CR_DDRCRDATACOMP0_STRUCT;

typedef DATA0CH0_CR_DDRCRDATACOMP1_STRUCT DATA2CH1_CR_DDRCRDATACOMP1_STRUCT;

typedef DATA0CH0_CR_VCCDLLCOMPDATA_STRUCT DATA2CH1_CR_VCCDLLCOMPDATA_STRUCT;

typedef DATA0CH0_CR_DCSOFFSET_STRUCT DATA2CH1_CR_DCSOFFSET_STRUCT;

typedef DATA0CH0_CR_DFXCCMON_STRUCT DATA2CH1_CR_DFXCCMON_STRUCT;

typedef DATA0CH0_CR_AFEMISCCTRL0_STRUCT DATA2CH1_CR_AFEMISCCTRL0_STRUCT;

typedef DATA0CH0_CR_VIEWCTL_STRUCT DATA2CH1_CR_VIEWCTL_STRUCT;

typedef DATA0CH0_CR_DCCCTL11_STRUCT DATA2CH1_CR_DCCCTL11_STRUCT;

typedef DATA0CH0_CR_DCCCTL13_STRUCT DATA2CH1_CR_DCCCTL13_STRUCT;

typedef DATA0CH0_CR_PMFSM_STRUCT DATA2CH1_CR_PMFSM_STRUCT;

typedef DATA0CH0_CR_PMFSM1_STRUCT DATA2CH1_CR_PMFSM1_STRUCT;

typedef DATA0CH0_CR_CLKALIGNSTATUS_STRUCT DATA2CH1_CR_CLKALIGNSTATUS_STRUCT;

typedef DATA0CH0_CR_DATATRAINFEEDBACK_STRUCT DATA2CH1_CR_DATATRAINFEEDBACK_STRUCT;

typedef DATA0CH0_CR_DDRCRMARGINMODECONTROL0_STRUCT DATA2CH1_CR_DDRCRMARGINMODECONTROL0_STRUCT;

typedef DATA0CH0_CR_DDRCRMARGINMODEDEBUGMSB0_STRUCT DATA2CH1_CR_DDRCRMARGINMODEDEBUGMSB0_STRUCT;

typedef DATA0CH0_CR_DDRCRMARGINMODEDEBUGMSB0_STRUCT DATA2CH1_CR_DDRCRMARGINMODEDEBUGLSB0_STRUCT;

typedef DATA0CH0_CR_DCCCTL14_STRUCT DATA2CH1_CR_DCCCTL14_STRUCT;

typedef DATA0CH0_CR_DDRDATADQRANK0LANE0_STRUCT DATA3CH0_CR_DDRDATADQRANK0LANE0_STRUCT;

typedef DATA0CH0_CR_DDRDATADQRANK0LANE0_STRUCT DATA3CH0_CR_DDRDATADQRANK0LANE1_STRUCT;

typedef DATA0CH0_CR_DDRDATADQRANK0LANE0_STRUCT DATA3CH0_CR_DDRDATADQRANK0LANE2_STRUCT;

typedef DATA0CH0_CR_DDRDATADQRANK0LANE0_STRUCT DATA3CH0_CR_DDRDATADQRANK0LANE3_STRUCT;

typedef DATA0CH0_CR_DDRDATADQRANK0LANE0_STRUCT DATA3CH0_CR_DDRDATADQRANK0LANE4_STRUCT;

typedef DATA0CH0_CR_DDRDATADQRANK0LANE0_STRUCT DATA3CH0_CR_DDRDATADQRANK0LANE5_STRUCT;

typedef DATA0CH0_CR_DDRDATADQRANK0LANE0_STRUCT DATA3CH0_CR_DDRDATADQRANK0LANE6_STRUCT;

typedef DATA0CH0_CR_DDRDATADQRANK0LANE0_STRUCT DATA3CH0_CR_DDRDATADQRANK0LANE7_STRUCT;

typedef DATA0CH0_CR_DDRDATADQRANK0LANE0_STRUCT DATA3CH0_CR_DDRDATADQRANK1LANE0_STRUCT;

typedef DATA0CH0_CR_DDRDATADQRANK0LANE0_STRUCT DATA3CH0_CR_DDRDATADQRANK1LANE1_STRUCT;

typedef DATA0CH0_CR_DDRDATADQRANK0LANE0_STRUCT DATA3CH0_CR_DDRDATADQRANK1LANE2_STRUCT;

typedef DATA0CH0_CR_DDRDATADQRANK0LANE0_STRUCT DATA3CH0_CR_DDRDATADQRANK1LANE3_STRUCT;

typedef DATA0CH0_CR_DDRDATADQRANK0LANE0_STRUCT DATA3CH0_CR_DDRDATADQRANK1LANE4_STRUCT;

typedef DATA0CH0_CR_DDRDATADQRANK0LANE0_STRUCT DATA3CH0_CR_DDRDATADQRANK1LANE5_STRUCT;

typedef DATA0CH0_CR_DDRDATADQRANK0LANE0_STRUCT DATA3CH0_CR_DDRDATADQRANK1LANE6_STRUCT;

typedef DATA0CH0_CR_DDRDATADQRANK0LANE0_STRUCT DATA3CH0_CR_DDRDATADQRANK1LANE7_STRUCT;

typedef DATA0CH0_CR_DDRDATADQRANK0LANE0_STRUCT DATA3CH0_CR_DDRDATADQRANK2LANE0_STRUCT;

typedef DATA0CH0_CR_DDRDATADQRANK0LANE0_STRUCT DATA3CH0_CR_DDRDATADQRANK2LANE1_STRUCT;

typedef DATA0CH0_CR_DDRDATADQRANK0LANE0_STRUCT DATA3CH0_CR_DDRDATADQRANK2LANE2_STRUCT;

typedef DATA0CH0_CR_DDRDATADQRANK0LANE0_STRUCT DATA3CH0_CR_DDRDATADQRANK2LANE3_STRUCT;

typedef DATA0CH0_CR_DDRDATADQRANK0LANE0_STRUCT DATA3CH0_CR_DDRDATADQRANK2LANE4_STRUCT;

typedef DATA0CH0_CR_DDRDATADQRANK0LANE0_STRUCT DATA3CH0_CR_DDRDATADQRANK2LANE5_STRUCT;

typedef DATA0CH0_CR_DDRDATADQRANK0LANE0_STRUCT DATA3CH0_CR_DDRDATADQRANK2LANE6_STRUCT;

typedef DATA0CH0_CR_DDRDATADQRANK0LANE0_STRUCT DATA3CH0_CR_DDRDATADQRANK2LANE7_STRUCT;

typedef DATA0CH0_CR_DDRDATADQRANK0LANE0_STRUCT DATA3CH0_CR_DDRDATADQRANK3LANE0_STRUCT;

typedef DATA0CH0_CR_DDRDATADQRANK0LANE0_STRUCT DATA3CH0_CR_DDRDATADQRANK3LANE1_STRUCT;

typedef DATA0CH0_CR_DDRDATADQRANK0LANE0_STRUCT DATA3CH0_CR_DDRDATADQRANK3LANE2_STRUCT;

typedef DATA0CH0_CR_DDRDATADQRANK0LANE0_STRUCT DATA3CH0_CR_DDRDATADQRANK3LANE3_STRUCT;

typedef DATA0CH0_CR_DDRDATADQRANK0LANE0_STRUCT DATA3CH0_CR_DDRDATADQRANK3LANE4_STRUCT;

typedef DATA0CH0_CR_DDRDATADQRANK0LANE0_STRUCT DATA3CH0_CR_DDRDATADQRANK3LANE5_STRUCT;

typedef DATA0CH0_CR_DDRDATADQRANK0LANE0_STRUCT DATA3CH0_CR_DDRDATADQRANK3LANE6_STRUCT;

typedef DATA0CH0_CR_DDRDATADQRANK0LANE0_STRUCT DATA3CH0_CR_DDRDATADQRANK3LANE7_STRUCT;

typedef DATA0CH0_CR_DDRCRDATACONTROL0_STRUCT DATA3CH0_CR_DDRCRDATACONTROL0_STRUCT;

typedef DATA0CH0_CR_DDRCRDATACONTROL1_STRUCT DATA3CH0_CR_DDRCRDATACONTROL1_STRUCT;

typedef DATA0CH0_CR_DDRCRDATACONTROL2_STRUCT DATA3CH0_CR_DDRCRDATACONTROL2_STRUCT;

typedef DATA0CH0_CR_DDRCRDATACONTROL3_STRUCT DATA3CH0_CR_DDRCRDATACONTROL3_STRUCT;

typedef DATA0CH0_CR_AFEMISC_STRUCT DATA3CH0_CR_AFEMISC_STRUCT;

typedef DATA0CH0_CR_SRZCTL_STRUCT DATA3CH0_CR_SRZCTL_STRUCT;

typedef DATA0CH0_CR_DQRXCTL0_STRUCT DATA3CH0_CR_DQRXCTL0_STRUCT;

typedef DATA0CH0_CR_DQRXCTL1_STRUCT DATA3CH0_CR_DQRXCTL1_STRUCT;

typedef DATA0CH0_CR_DQSTXRXCTL_STRUCT DATA3CH0_CR_DQSTXRXCTL_STRUCT;

typedef DATA0CH0_CR_DQSTXRXCTL0_STRUCT DATA3CH0_CR_DQSTXRXCTL0_STRUCT;

typedef DATA0CH0_CR_SDLCTL1_STRUCT DATA3CH0_CR_SDLCTL1_STRUCT;

typedef DATA0CH0_CR_SDLCTL0_STRUCT DATA3CH0_CR_SDLCTL0_STRUCT;

typedef DATA0CH0_CR_MDLLCTL0_STRUCT DATA3CH0_CR_MDLLCTL0_STRUCT;

typedef DATA0CH0_CR_MDLLCTL1_STRUCT DATA3CH0_CR_MDLLCTL1_STRUCT;

typedef DATA0CH0_CR_MDLLCTL2_STRUCT DATA3CH0_CR_MDLLCTL2_STRUCT;

typedef DATA0CH0_CR_MDLLCTL3_STRUCT DATA3CH0_CR_MDLLCTL3_STRUCT;

typedef DATA0CH0_CR_MDLLCTL4_STRUCT DATA3CH0_CR_MDLLCTL4_STRUCT;

typedef DATA0CH0_CR_MDLLCTL4_STRUCT DATA3CH0_CR_MDLLCTL5_STRUCT;

typedef DATA0CH0_CR_DIGMISCCTRL_STRUCT DATA3CH0_CR_DIGMISCCTRL_STRUCT;

typedef DATA0CH0_CR_AFEMISCCTRL2_STRUCT DATA3CH0_CR_AFEMISCCTRL2_STRUCT;

typedef DATA0CH0_CR_TXPBDOFFSET0_STRUCT DATA3CH0_CR_TXPBDOFFSET0_STRUCT;

typedef DATA0CH0_CR_TXPBDOFFSET1_STRUCT DATA3CH0_CR_TXPBDOFFSET1_STRUCT;

typedef DATA0CH0_CR_COMPCTL0_STRUCT DATA3CH0_CR_COMPCTL0_STRUCT;

typedef DATA0CH0_CR_COMPCTL1_STRUCT DATA3CH0_CR_COMPCTL1_STRUCT;

typedef DATA0CH0_CR_COMPCTL2_STRUCT DATA3CH0_CR_COMPCTL2_STRUCT;

typedef DATA0CH0_CR_DCCCTL5_STRUCT DATA3CH0_CR_DCCCTL5_STRUCT;

typedef DATA0CH0_CR_DCCCTL6_STRUCT DATA3CH0_CR_DCCCTL6_STRUCT;

typedef DATA0CH0_CR_DCCCTL7_STRUCT DATA3CH0_CR_DCCCTL7_STRUCT;

typedef DATA0CH0_CR_DCCCTL8_STRUCT DATA3CH0_CR_DCCCTL8_STRUCT;

typedef DATA0CH0_CR_DCCCTL9_STRUCT DATA3CH0_CR_DCCCTL9_STRUCT;

typedef DATA0CH0_CR_RXCONTROL0RANK0_STRUCT DATA3CH0_CR_RXCONTROL0RANK0_STRUCT;

typedef DATA0CH0_CR_RXCONTROL0RANK0_STRUCT DATA3CH0_CR_RXCONTROL0RANK1_STRUCT;

typedef DATA0CH0_CR_RXCONTROL0RANK0_STRUCT DATA3CH0_CR_RXCONTROL0RANK2_STRUCT;

typedef DATA0CH0_CR_RXCONTROL0RANK0_STRUCT DATA3CH0_CR_RXCONTROL0RANK3_STRUCT;

typedef DATA0CH0_CR_TXCONTROL0_PH90_STRUCT DATA3CH0_CR_TXCONTROL0_PH90_STRUCT;

typedef DATA0CH0_CR_TXCONTROL0RANK0_STRUCT DATA3CH0_CR_TXCONTROL0RANK0_STRUCT;

typedef DATA0CH0_CR_TXCONTROL0RANK0_STRUCT DATA3CH0_CR_TXCONTROL0RANK1_STRUCT;

typedef DATA0CH0_CR_TXCONTROL0RANK0_STRUCT DATA3CH0_CR_TXCONTROL0RANK2_STRUCT;

typedef DATA0CH0_CR_TXCONTROL0RANK0_STRUCT DATA3CH0_CR_TXCONTROL0RANK3_STRUCT;

typedef DATA0CH0_CR_DDRDATADQSRANKX_STRUCT DATA3CH0_CR_DDRDATADQSRANKX_STRUCT;

typedef DATA0CH0_CR_DDRDATADQSPH90RANKX_STRUCT DATA3CH0_CR_DDRDATADQSPH90RANKX_STRUCT;

typedef DATA0CH0_CR_DDRCRDATAOFFSETCOMP_STRUCT DATA3CH0_CR_DDRCRDATAOFFSETCOMP_STRUCT;

typedef DATA0CH0_CR_DDRCRDATAOFFSETTRAIN_STRUCT DATA3CH0_CR_DDRCRDATAOFFSETTRAIN_STRUCT;

typedef DATA0CH0_CR_DDRCRCMDBUSTRAIN_STRUCT DATA3CH0_CR_DDRCRCMDBUSTRAIN_STRUCT;

typedef DATA0CH0_CR_DDRCRWRRETRAINSWIZZLECONTROL_STRUCT DATA3CH0_CR_DDRCRWRRETRAINSWIZZLECONTROL_STRUCT;

typedef DATA0CH0_CR_DDRCRWRRETRAINRANK3_STRUCT DATA3CH0_CR_DDRCRWRRETRAINRANK3_STRUCT;

typedef DATA0CH0_CR_DDRCRWRRETRAINRANK3_STRUCT DATA3CH0_CR_DDRCRWRRETRAINRANK2_STRUCT;

typedef DATA0CH0_CR_DDRCRWRRETRAINRANK3_STRUCT DATA3CH0_CR_DDRCRWRRETRAINRANK1_STRUCT;

typedef DATA0CH0_CR_DDRCRWRRETRAINRANK3_STRUCT DATA3CH0_CR_DDRCRWRRETRAINRANK0_STRUCT;

typedef DATA0CH0_CR_DDRCRWRRETRAINCONTROLSTATUS_STRUCT DATA3CH0_CR_DDRCRWRRETRAINCONTROLSTATUS_STRUCT;

typedef DATA0CH0_CR_TXCONTROL1_PH90_STRUCT DATA3CH0_CR_TXCONTROL1_PH90_STRUCT;

typedef DATA0CH0_CR_AFEMISCCTRL1_STRUCT DATA3CH0_CR_AFEMISCCTRL1_STRUCT;

typedef DATA0CH0_CR_DQXRXAMPCTL_STRUCT DATA3CH0_CR_DQXRXAMPCTL_STRUCT;

typedef DATA0CH0_CR_DQ0RXAMPCTL_STRUCT DATA3CH0_CR_DQ0RXAMPCTL_STRUCT;

typedef DATA0CH0_CR_DQ1RXAMPCTL_STRUCT DATA3CH0_CR_DQ1RXAMPCTL_STRUCT;

typedef DATA0CH0_CR_DQ2RXAMPCTL_STRUCT DATA3CH0_CR_DQ2RXAMPCTL_STRUCT;

typedef DATA0CH0_CR_DQ3RXAMPCTL_STRUCT DATA3CH0_CR_DQ3RXAMPCTL_STRUCT;

typedef DATA0CH0_CR_DQ4RXAMPCTL_STRUCT DATA3CH0_CR_DQ4RXAMPCTL_STRUCT;

typedef DATA0CH0_CR_DQ5RXAMPCTL_STRUCT DATA3CH0_CR_DQ5RXAMPCTL_STRUCT;

typedef DATA0CH0_CR_DQ6RXAMPCTL_STRUCT DATA3CH0_CR_DQ6RXAMPCTL_STRUCT;

typedef DATA0CH0_CR_DQ7RXAMPCTL_STRUCT DATA3CH0_CR_DQ7RXAMPCTL_STRUCT;

typedef DATA0CH0_CR_DQRXAMPCTL0_STRUCT DATA3CH0_CR_DQRXAMPCTL0_STRUCT;

typedef DATA0CH0_CR_DQRXAMPCTL1_STRUCT DATA3CH0_CR_DQRXAMPCTL1_STRUCT;

typedef DATA0CH0_CR_DQRXAMPCTL2_STRUCT DATA3CH0_CR_DQRXAMPCTL2_STRUCT;

typedef DATA0CH0_CR_DQTXCTL0_STRUCT DATA3CH0_CR_DQTXCTL0_STRUCT;

typedef DATA0CH0_CR_DCCCTL0_STRUCT DATA3CH0_CR_DCCCTL0_STRUCT;

typedef DATA0CH0_CR_DCCCTL1_STRUCT DATA3CH0_CR_DCCCTL1_STRUCT;

typedef DATA0CH0_CR_DCCCTL2_STRUCT DATA3CH0_CR_DCCCTL2_STRUCT;

typedef DATA0CH0_CR_DCCCTL3_STRUCT DATA3CH0_CR_DCCCTL3_STRUCT;

typedef DATA0CH0_CR_DCCCTL10_STRUCT DATA3CH0_CR_DCCCTL10_STRUCT;

typedef DATA0CH0_CR_DCCCTL12_STRUCT DATA3CH0_CR_DCCCTL12_STRUCT;

typedef DATA0CH0_CR_CLKALIGNCTL0_STRUCT DATA3CH0_CR_CLKALIGNCTL0_STRUCT;

typedef DATA0CH0_CR_CLKALIGNCTL1_STRUCT DATA3CH0_CR_CLKALIGNCTL1_STRUCT;

typedef DATA0CH0_CR_CLKALIGNCTL2_STRUCT DATA3CH0_CR_CLKALIGNCTL2_STRUCT;

typedef DATA0CH0_CR_DCCCTL4_STRUCT DATA3CH0_CR_DCCCTL4_STRUCT;

typedef DATA0CH0_CR_DDRCRDATACOMPVTT_STRUCT DATA3CH0_CR_DDRCRDATACOMPVTT_STRUCT;

typedef DATA0CH0_CR_WLCTRL_STRUCT DATA3CH0_CR_WLCTRL_STRUCT;

typedef DATA0CH0_CR_LPMODECTL_STRUCT DATA3CH0_CR_LPMODECTL_STRUCT;

typedef DATA0CH0_CR_DDRCRDATACOMP0_STRUCT DATA3CH0_CR_DDRCRDATACOMP0_STRUCT;

typedef DATA0CH0_CR_DDRCRDATACOMP1_STRUCT DATA3CH0_CR_DDRCRDATACOMP1_STRUCT;

typedef DATA0CH0_CR_VCCDLLCOMPDATA_STRUCT DATA3CH0_CR_VCCDLLCOMPDATA_STRUCT;

typedef DATA0CH0_CR_DCSOFFSET_STRUCT DATA3CH0_CR_DCSOFFSET_STRUCT;

typedef DATA0CH0_CR_DFXCCMON_STRUCT DATA3CH0_CR_DFXCCMON_STRUCT;

typedef DATA0CH0_CR_AFEMISCCTRL0_STRUCT DATA3CH0_CR_AFEMISCCTRL0_STRUCT;

typedef DATA0CH0_CR_VIEWCTL_STRUCT DATA3CH0_CR_VIEWCTL_STRUCT;

typedef DATA0CH0_CR_DCCCTL11_STRUCT DATA3CH0_CR_DCCCTL11_STRUCT;

typedef DATA0CH0_CR_DCCCTL13_STRUCT DATA3CH0_CR_DCCCTL13_STRUCT;

typedef DATA0CH0_CR_PMFSM_STRUCT DATA3CH0_CR_PMFSM_STRUCT;

typedef DATA0CH0_CR_PMFSM1_STRUCT DATA3CH0_CR_PMFSM1_STRUCT;

typedef DATA0CH0_CR_CLKALIGNSTATUS_STRUCT DATA3CH0_CR_CLKALIGNSTATUS_STRUCT;

typedef DATA0CH0_CR_DATATRAINFEEDBACK_STRUCT DATA3CH0_CR_DATATRAINFEEDBACK_STRUCT;

typedef DATA0CH0_CR_DDRCRMARGINMODECONTROL0_STRUCT DATA3CH0_CR_DDRCRMARGINMODECONTROL0_STRUCT;

typedef DATA0CH0_CR_DDRCRMARGINMODEDEBUGMSB0_STRUCT DATA3CH0_CR_DDRCRMARGINMODEDEBUGMSB0_STRUCT;

typedef DATA0CH0_CR_DDRCRMARGINMODEDEBUGMSB0_STRUCT DATA3CH0_CR_DDRCRMARGINMODEDEBUGLSB0_STRUCT;

typedef DATA0CH0_CR_DCCCTL14_STRUCT DATA3CH0_CR_DCCCTL14_STRUCT;

typedef DATA0CH0_CR_DDRDATADQRANK0LANE0_STRUCT DATA3CH1_CR_DDRDATADQRANK0LANE0_STRUCT;

typedef DATA0CH0_CR_DDRDATADQRANK0LANE0_STRUCT DATA3CH1_CR_DDRDATADQRANK0LANE1_STRUCT;

typedef DATA0CH0_CR_DDRDATADQRANK0LANE0_STRUCT DATA3CH1_CR_DDRDATADQRANK0LANE2_STRUCT;

typedef DATA0CH0_CR_DDRDATADQRANK0LANE0_STRUCT DATA3CH1_CR_DDRDATADQRANK0LANE3_STRUCT;

typedef DATA0CH0_CR_DDRDATADQRANK0LANE0_STRUCT DATA3CH1_CR_DDRDATADQRANK0LANE4_STRUCT;

typedef DATA0CH0_CR_DDRDATADQRANK0LANE0_STRUCT DATA3CH1_CR_DDRDATADQRANK0LANE5_STRUCT;

typedef DATA0CH0_CR_DDRDATADQRANK0LANE0_STRUCT DATA3CH1_CR_DDRDATADQRANK0LANE6_STRUCT;

typedef DATA0CH0_CR_DDRDATADQRANK0LANE0_STRUCT DATA3CH1_CR_DDRDATADQRANK0LANE7_STRUCT;

typedef DATA0CH0_CR_DDRDATADQRANK0LANE0_STRUCT DATA3CH1_CR_DDRDATADQRANK1LANE0_STRUCT;

typedef DATA0CH0_CR_DDRDATADQRANK0LANE0_STRUCT DATA3CH1_CR_DDRDATADQRANK1LANE1_STRUCT;

typedef DATA0CH0_CR_DDRDATADQRANK0LANE0_STRUCT DATA3CH1_CR_DDRDATADQRANK1LANE2_STRUCT;

typedef DATA0CH0_CR_DDRDATADQRANK0LANE0_STRUCT DATA3CH1_CR_DDRDATADQRANK1LANE3_STRUCT;

typedef DATA0CH0_CR_DDRDATADQRANK0LANE0_STRUCT DATA3CH1_CR_DDRDATADQRANK1LANE4_STRUCT;

typedef DATA0CH0_CR_DDRDATADQRANK0LANE0_STRUCT DATA3CH1_CR_DDRDATADQRANK1LANE5_STRUCT;

typedef DATA0CH0_CR_DDRDATADQRANK0LANE0_STRUCT DATA3CH1_CR_DDRDATADQRANK1LANE6_STRUCT;

typedef DATA0CH0_CR_DDRDATADQRANK0LANE0_STRUCT DATA3CH1_CR_DDRDATADQRANK1LANE7_STRUCT;

typedef DATA0CH0_CR_DDRDATADQRANK0LANE0_STRUCT DATA3CH1_CR_DDRDATADQRANK2LANE0_STRUCT;

typedef DATA0CH0_CR_DDRDATADQRANK0LANE0_STRUCT DATA3CH1_CR_DDRDATADQRANK2LANE1_STRUCT;

typedef DATA0CH0_CR_DDRDATADQRANK0LANE0_STRUCT DATA3CH1_CR_DDRDATADQRANK2LANE2_STRUCT;

typedef DATA0CH0_CR_DDRDATADQRANK0LANE0_STRUCT DATA3CH1_CR_DDRDATADQRANK2LANE3_STRUCT;

typedef DATA0CH0_CR_DDRDATADQRANK0LANE0_STRUCT DATA3CH1_CR_DDRDATADQRANK2LANE4_STRUCT;

typedef DATA0CH0_CR_DDRDATADQRANK0LANE0_STRUCT DATA3CH1_CR_DDRDATADQRANK2LANE5_STRUCT;

typedef DATA0CH0_CR_DDRDATADQRANK0LANE0_STRUCT DATA3CH1_CR_DDRDATADQRANK2LANE6_STRUCT;

typedef DATA0CH0_CR_DDRDATADQRANK0LANE0_STRUCT DATA3CH1_CR_DDRDATADQRANK2LANE7_STRUCT;

typedef DATA0CH0_CR_DDRDATADQRANK0LANE0_STRUCT DATA3CH1_CR_DDRDATADQRANK3LANE0_STRUCT;

typedef DATA0CH0_CR_DDRDATADQRANK0LANE0_STRUCT DATA3CH1_CR_DDRDATADQRANK3LANE1_STRUCT;

typedef DATA0CH0_CR_DDRDATADQRANK0LANE0_STRUCT DATA3CH1_CR_DDRDATADQRANK3LANE2_STRUCT;

typedef DATA0CH0_CR_DDRDATADQRANK0LANE0_STRUCT DATA3CH1_CR_DDRDATADQRANK3LANE3_STRUCT;

typedef DATA0CH0_CR_DDRDATADQRANK0LANE0_STRUCT DATA3CH1_CR_DDRDATADQRANK3LANE4_STRUCT;

typedef DATA0CH0_CR_DDRDATADQRANK0LANE0_STRUCT DATA3CH1_CR_DDRDATADQRANK3LANE5_STRUCT;

typedef DATA0CH0_CR_DDRDATADQRANK0LANE0_STRUCT DATA3CH1_CR_DDRDATADQRANK3LANE6_STRUCT;

typedef DATA0CH0_CR_DDRDATADQRANK0LANE0_STRUCT DATA3CH1_CR_DDRDATADQRANK3LANE7_STRUCT;

typedef DATA0CH0_CR_DDRCRDATACONTROL0_STRUCT DATA3CH1_CR_DDRCRDATACONTROL0_STRUCT;

typedef DATA0CH0_CR_DDRCRDATACONTROL1_STRUCT DATA3CH1_CR_DDRCRDATACONTROL1_STRUCT;

typedef DATA0CH0_CR_DDRCRDATACONTROL2_STRUCT DATA3CH1_CR_DDRCRDATACONTROL2_STRUCT;

typedef DATA0CH0_CR_DDRCRDATACONTROL3_STRUCT DATA3CH1_CR_DDRCRDATACONTROL3_STRUCT;

typedef DATA0CH0_CR_AFEMISC_STRUCT DATA3CH1_CR_AFEMISC_STRUCT;

typedef DATA0CH0_CR_SRZCTL_STRUCT DATA3CH1_CR_SRZCTL_STRUCT;

typedef DATA0CH0_CR_DQRXCTL0_STRUCT DATA3CH1_CR_DQRXCTL0_STRUCT;

typedef DATA0CH0_CR_DQRXCTL1_STRUCT DATA3CH1_CR_DQRXCTL1_STRUCT;

typedef DATA0CH0_CR_DQSTXRXCTL_STRUCT DATA3CH1_CR_DQSTXRXCTL_STRUCT;

typedef DATA0CH0_CR_DQSTXRXCTL0_STRUCT DATA3CH1_CR_DQSTXRXCTL0_STRUCT;

typedef DATA0CH0_CR_SDLCTL1_STRUCT DATA3CH1_CR_SDLCTL1_STRUCT;

typedef DATA0CH0_CR_SDLCTL0_STRUCT DATA3CH1_CR_SDLCTL0_STRUCT;

typedef DATA0CH0_CR_MDLLCTL0_STRUCT DATA3CH1_CR_MDLLCTL0_STRUCT;

typedef DATA0CH0_CR_MDLLCTL1_STRUCT DATA3CH1_CR_MDLLCTL1_STRUCT;

typedef DATA0CH0_CR_MDLLCTL2_STRUCT DATA3CH1_CR_MDLLCTL2_STRUCT;

typedef DATA0CH0_CR_MDLLCTL3_STRUCT DATA3CH1_CR_MDLLCTL3_STRUCT;

typedef DATA0CH0_CR_MDLLCTL4_STRUCT DATA3CH1_CR_MDLLCTL4_STRUCT;

typedef DATA0CH0_CR_MDLLCTL4_STRUCT DATA3CH1_CR_MDLLCTL5_STRUCT;

typedef DATA0CH0_CR_DIGMISCCTRL_STRUCT DATA3CH1_CR_DIGMISCCTRL_STRUCT;

typedef DATA0CH0_CR_AFEMISCCTRL2_STRUCT DATA3CH1_CR_AFEMISCCTRL2_STRUCT;

typedef DATA0CH0_CR_TXPBDOFFSET0_STRUCT DATA3CH1_CR_TXPBDOFFSET0_STRUCT;

typedef DATA0CH0_CR_TXPBDOFFSET1_STRUCT DATA3CH1_CR_TXPBDOFFSET1_STRUCT;

typedef DATA0CH0_CR_COMPCTL0_STRUCT DATA3CH1_CR_COMPCTL0_STRUCT;

typedef DATA0CH0_CR_COMPCTL1_STRUCT DATA3CH1_CR_COMPCTL1_STRUCT;

typedef DATA0CH0_CR_COMPCTL2_STRUCT DATA3CH1_CR_COMPCTL2_STRUCT;

typedef DATA0CH0_CR_DCCCTL5_STRUCT DATA3CH1_CR_DCCCTL5_STRUCT;

typedef DATA0CH0_CR_DCCCTL6_STRUCT DATA3CH1_CR_DCCCTL6_STRUCT;

typedef DATA0CH0_CR_DCCCTL7_STRUCT DATA3CH1_CR_DCCCTL7_STRUCT;

typedef DATA0CH0_CR_DCCCTL8_STRUCT DATA3CH1_CR_DCCCTL8_STRUCT;

typedef DATA0CH0_CR_DCCCTL9_STRUCT DATA3CH1_CR_DCCCTL9_STRUCT;

typedef DATA0CH0_CR_RXCONTROL0RANK0_STRUCT DATA3CH1_CR_RXCONTROL0RANK0_STRUCT;

typedef DATA0CH0_CR_RXCONTROL0RANK0_STRUCT DATA3CH1_CR_RXCONTROL0RANK1_STRUCT;

typedef DATA0CH0_CR_RXCONTROL0RANK0_STRUCT DATA3CH1_CR_RXCONTROL0RANK2_STRUCT;

typedef DATA0CH0_CR_RXCONTROL0RANK0_STRUCT DATA3CH1_CR_RXCONTROL0RANK3_STRUCT;

typedef DATA0CH0_CR_TXCONTROL0_PH90_STRUCT DATA3CH1_CR_TXCONTROL0_PH90_STRUCT;

typedef DATA0CH0_CR_TXCONTROL0RANK0_STRUCT DATA3CH1_CR_TXCONTROL0RANK0_STRUCT;

typedef DATA0CH0_CR_TXCONTROL0RANK0_STRUCT DATA3CH1_CR_TXCONTROL0RANK1_STRUCT;

typedef DATA0CH0_CR_TXCONTROL0RANK0_STRUCT DATA3CH1_CR_TXCONTROL0RANK2_STRUCT;

typedef DATA0CH0_CR_TXCONTROL0RANK0_STRUCT DATA3CH1_CR_TXCONTROL0RANK3_STRUCT;

typedef DATA0CH0_CR_DDRDATADQSRANKX_STRUCT DATA3CH1_CR_DDRDATADQSRANKX_STRUCT;

typedef DATA0CH0_CR_DDRDATADQSPH90RANKX_STRUCT DATA3CH1_CR_DDRDATADQSPH90RANKX_STRUCT;

typedef DATA0CH0_CR_DDRCRDATAOFFSETCOMP_STRUCT DATA3CH1_CR_DDRCRDATAOFFSETCOMP_STRUCT;

typedef DATA0CH0_CR_DDRCRDATAOFFSETTRAIN_STRUCT DATA3CH1_CR_DDRCRDATAOFFSETTRAIN_STRUCT;

typedef DATA0CH0_CR_DDRCRCMDBUSTRAIN_STRUCT DATA3CH1_CR_DDRCRCMDBUSTRAIN_STRUCT;

typedef DATA0CH0_CR_DDRCRWRRETRAINSWIZZLECONTROL_STRUCT DATA3CH1_CR_DDRCRWRRETRAINSWIZZLECONTROL_STRUCT;

typedef DATA0CH0_CR_DDRCRWRRETRAINRANK3_STRUCT DATA3CH1_CR_DDRCRWRRETRAINRANK3_STRUCT;

typedef DATA0CH0_CR_DDRCRWRRETRAINRANK3_STRUCT DATA3CH1_CR_DDRCRWRRETRAINRANK2_STRUCT;

typedef DATA0CH0_CR_DDRCRWRRETRAINRANK3_STRUCT DATA3CH1_CR_DDRCRWRRETRAINRANK1_STRUCT;

typedef DATA0CH0_CR_DDRCRWRRETRAINRANK3_STRUCT DATA3CH1_CR_DDRCRWRRETRAINRANK0_STRUCT;

typedef DATA0CH0_CR_DDRCRWRRETRAINCONTROLSTATUS_STRUCT DATA3CH1_CR_DDRCRWRRETRAINCONTROLSTATUS_STRUCT;

typedef DATA0CH0_CR_TXCONTROL1_PH90_STRUCT DATA3CH1_CR_TXCONTROL1_PH90_STRUCT;

typedef DATA0CH0_CR_AFEMISCCTRL1_STRUCT DATA3CH1_CR_AFEMISCCTRL1_STRUCT;

typedef DATA0CH0_CR_DQXRXAMPCTL_STRUCT DATA3CH1_CR_DQXRXAMPCTL_STRUCT;

typedef DATA0CH0_CR_DQ0RXAMPCTL_STRUCT DATA3CH1_CR_DQ0RXAMPCTL_STRUCT;

typedef DATA0CH0_CR_DQ1RXAMPCTL_STRUCT DATA3CH1_CR_DQ1RXAMPCTL_STRUCT;

typedef DATA0CH0_CR_DQ2RXAMPCTL_STRUCT DATA3CH1_CR_DQ2RXAMPCTL_STRUCT;

typedef DATA0CH0_CR_DQ3RXAMPCTL_STRUCT DATA3CH1_CR_DQ3RXAMPCTL_STRUCT;

typedef DATA0CH0_CR_DQ4RXAMPCTL_STRUCT DATA3CH1_CR_DQ4RXAMPCTL_STRUCT;

typedef DATA0CH0_CR_DQ5RXAMPCTL_STRUCT DATA3CH1_CR_DQ5RXAMPCTL_STRUCT;

typedef DATA0CH0_CR_DQ6RXAMPCTL_STRUCT DATA3CH1_CR_DQ6RXAMPCTL_STRUCT;

typedef DATA0CH0_CR_DQ7RXAMPCTL_STRUCT DATA3CH1_CR_DQ7RXAMPCTL_STRUCT;

typedef DATA0CH0_CR_DQRXAMPCTL0_STRUCT DATA3CH1_CR_DQRXAMPCTL0_STRUCT;

typedef DATA0CH0_CR_DQRXAMPCTL1_STRUCT DATA3CH1_CR_DQRXAMPCTL1_STRUCT;

typedef DATA0CH0_CR_DQRXAMPCTL2_STRUCT DATA3CH1_CR_DQRXAMPCTL2_STRUCT;

typedef DATA0CH0_CR_DQTXCTL0_STRUCT DATA3CH1_CR_DQTXCTL0_STRUCT;

typedef DATA0CH0_CR_DCCCTL0_STRUCT DATA3CH1_CR_DCCCTL0_STRUCT;

typedef DATA0CH0_CR_DCCCTL1_STRUCT DATA3CH1_CR_DCCCTL1_STRUCT;

typedef DATA0CH0_CR_DCCCTL2_STRUCT DATA3CH1_CR_DCCCTL2_STRUCT;

typedef DATA0CH0_CR_DCCCTL3_STRUCT DATA3CH1_CR_DCCCTL3_STRUCT;

typedef DATA0CH0_CR_DCCCTL10_STRUCT DATA3CH1_CR_DCCCTL10_STRUCT;

typedef DATA0CH0_CR_DCCCTL12_STRUCT DATA3CH1_CR_DCCCTL12_STRUCT;

typedef DATA0CH0_CR_CLKALIGNCTL0_STRUCT DATA3CH1_CR_CLKALIGNCTL0_STRUCT;

typedef DATA0CH0_CR_CLKALIGNCTL1_STRUCT DATA3CH1_CR_CLKALIGNCTL1_STRUCT;

typedef DATA0CH0_CR_CLKALIGNCTL2_STRUCT DATA3CH1_CR_CLKALIGNCTL2_STRUCT;

typedef DATA0CH0_CR_DCCCTL4_STRUCT DATA3CH1_CR_DCCCTL4_STRUCT;

typedef DATA0CH0_CR_DDRCRDATACOMPVTT_STRUCT DATA3CH1_CR_DDRCRDATACOMPVTT_STRUCT;

typedef DATA0CH0_CR_WLCTRL_STRUCT DATA3CH1_CR_WLCTRL_STRUCT;

typedef DATA0CH0_CR_LPMODECTL_STRUCT DATA3CH1_CR_LPMODECTL_STRUCT;

typedef DATA0CH0_CR_DDRCRDATACOMP0_STRUCT DATA3CH1_CR_DDRCRDATACOMP0_STRUCT;

typedef DATA0CH0_CR_DDRCRDATACOMP1_STRUCT DATA3CH1_CR_DDRCRDATACOMP1_STRUCT;

typedef DATA0CH0_CR_VCCDLLCOMPDATA_STRUCT DATA3CH1_CR_VCCDLLCOMPDATA_STRUCT;

typedef DATA0CH0_CR_DCSOFFSET_STRUCT DATA3CH1_CR_DCSOFFSET_STRUCT;

typedef DATA0CH0_CR_DFXCCMON_STRUCT DATA3CH1_CR_DFXCCMON_STRUCT;

typedef DATA0CH0_CR_AFEMISCCTRL0_STRUCT DATA3CH1_CR_AFEMISCCTRL0_STRUCT;

typedef DATA0CH0_CR_VIEWCTL_STRUCT DATA3CH1_CR_VIEWCTL_STRUCT;

typedef DATA0CH0_CR_DCCCTL11_STRUCT DATA3CH1_CR_DCCCTL11_STRUCT;

typedef DATA0CH0_CR_DCCCTL13_STRUCT DATA3CH1_CR_DCCCTL13_STRUCT;

typedef DATA0CH0_CR_PMFSM_STRUCT DATA3CH1_CR_PMFSM_STRUCT;

typedef DATA0CH0_CR_PMFSM1_STRUCT DATA3CH1_CR_PMFSM1_STRUCT;

typedef DATA0CH0_CR_CLKALIGNSTATUS_STRUCT DATA3CH1_CR_CLKALIGNSTATUS_STRUCT;

typedef DATA0CH0_CR_DATATRAINFEEDBACK_STRUCT DATA3CH1_CR_DATATRAINFEEDBACK_STRUCT;

typedef DATA0CH0_CR_DDRCRMARGINMODECONTROL0_STRUCT DATA3CH1_CR_DDRCRMARGINMODECONTROL0_STRUCT;

typedef DATA0CH0_CR_DDRCRMARGINMODEDEBUGMSB0_STRUCT DATA3CH1_CR_DDRCRMARGINMODEDEBUGMSB0_STRUCT;

typedef DATA0CH0_CR_DDRCRMARGINMODEDEBUGMSB0_STRUCT DATA3CH1_CR_DDRCRMARGINMODEDEBUGLSB0_STRUCT;

typedef DATA0CH0_CR_DCCCTL14_STRUCT DATA3CH1_CR_DCCCTL14_STRUCT;

#pragma pack(pop)
#endif
