/** @file
  This module contains functions to configure and use Memory Controller
  maintenance features.

@copyright
  INTEL CONFIDENTIAL
  Copyright 2017 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains an 'Intel Peripheral Driver' and is uniquely identified as
  "Intel Reference Module" and is licensed for Intel CPUs and chipsets under
  the terms of your license agreement with Intel or your vendor. This file may
  be modified by the user, subject to additional terms of the license agreement.

@par Specification Reference:
**/
#include "MrcTypes.h"
#include "McAddress.h"
#include "MrcCommon.h"
#include "MrcMaintenance.h"
#include "MrcGeneral.h"
#include "MrcMcOffsets.h"
#include "MrcDdr5.h"
#include "MrcChipApi.h"


/**
  This function will disable the DQS Oscillator maintenance feature in the Memory Controller.

  @params[in] MrcData - Pointer to MRC global data.
**/
VOID
MrcDisableDqsOscillatorMc (
  IN  MrcParameters * const MrcData
  )
{
  UINT32  Controller;
  UINT32  Channel;
  UINT32  Offset;
  UINT32  IpChannel;
  BOOLEAN Lpddr;
  MC0_CH0_CR_DQS_OSCILLATOR_PARAMS_STRUCT Lp4DqsOsclParams;

  Lpddr = MrcData->Outputs.Lpddr;

  for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
    for (Channel = 0; Channel < MAX_CHANNEL; Channel++) {
      if ((!MrcChannelExist (MrcData, Controller, Channel)) || IS_MC_SUB_CH (Lpddr, Channel)) {
        // For LPDDR4/5, only program register on even channels.
        continue;
      }
      IpChannel = LP_IP_CH (Lpddr, Channel);
      Offset = OFFSET_CALC_MC_CH (
                  MC0_CH0_CR_DQS_OSCILLATOR_PARAMS_REG,
                  MC1_CH0_CR_DQS_OSCILLATOR_PARAMS_REG,
                  Controller,
                  MC0_CH1_CR_DQS_OSCILLATOR_PARAMS_REG,
                  IpChannel
                  );
      Lp4DqsOsclParams.Data = MrcReadCR (MrcData, Offset);
      Lp4DqsOsclParams.Bits.DQSOSCL_PERIOD          = 0;
      Lp4DqsOsclParams.Bits.DIS_SRX_DQSOSCL         = 1;

      MrcWriteCR (MrcData, Offset, Lp4DqsOsclParams.Data);
    } // Channel
  } // Controller
}

/**
  This function executes the requested ZQ command.
  This function will wait the appropriate wait time as specified in the JEDEC spec.
  ZQ is sent to all populated controllers, channels, and ranks.

  @param[in]  MrcData   - Pointer to MRC global data.
  @param[in]  ChBitMask - Bit mask of channels to run.

  @retval MrcStatus - mrcSuccess if successful, else an error status.
**/
MrcStatus
MrcSendZq (
  IN  MrcParameters *const  MrcData
  )
{
  MrcStatus   Status;
  UINT32      Offset;
  UINT32      Channel;
  UINT32      Controller;
  UINT32      IpChannel;
  BOOLEAN     Lpddr;
  MC0_CH0_CR_MRS_FSM_CONTROL_STRUCT FsmCtl;
  MC0_CH0_CR_MRS_FSM_CONTROL_STRUCT FsmCtlCurrentData[MAX_CONTROLLER][MAX_CHANNEL];

  Status = mrcSuccess;
  Lpddr = MrcData->Outputs.Lpddr;

  // Use MRS FSM to send ZQ commands on all populated channels / ranks.
  // DDR4:   FSM will issue ZQCL and wait tZQINIT
  // LPDDR4: FSM will issue MPC ZQCAL START and LATCH, and wait tZQCAL / tZQLAT
  // DDR5:   FSM will issue MPC ZQCAL START and LATCH, and wait tZQCAL / tZQLAT
  for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
    for (Channel = 0; Channel < MAX_CHANNEL; Channel++) {
      if ((MrcChannelExist (MrcData, Controller, Channel)) && (!IS_MC_SUB_CH (Lpddr, Channel))) {
        // Save FSM CTRL
        IpChannel = LP_IP_CH (Lpddr, Channel);
        Offset = MrcGetMrsFsmCtlOffset (MrcData, Controller, IpChannel);
        FsmCtlCurrentData[Controller][Channel].Data = MrcReadCR64 (MrcData, Offset);

        FsmCtl.Data = 0;
        FsmCtl.Bits.do_ZQCL = 1;
        FsmCtl.Bits.reset_flow = 1;  // FSM should wait tZQINIT
        MrcWriteCR64 (MrcData, Offset, FsmCtl.Data);
      }
    } // Channel
  } // Controller

  // Will run on both channels in parallel
  Status = MrcProgramMrsFsm (MrcData);

  // Restore FSM CTRL
  for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
    for (Channel = 0; Channel < MAX_CHANNEL; Channel++) {
      if ((MrcChannelExist (MrcData, Controller, Channel)) && (!IS_MC_SUB_CH (Lpddr, Channel))) {
        IpChannel = LP_IP_CH (Lpddr, Channel);
        Offset = MrcGetMrsFsmCtlOffset (MrcData, Controller, IpChannel);
        MrcWriteCR64 (MrcData, Offset, FsmCtlCurrentData[Controller][Channel].Data);
      }
    }
  }

  return Status;
}

/**
  Issue ZQ calibration command on all populated controller, channels and ranks.
  When done, wait appropriate delay depending on the ZQ type.

  @param[in] MrcData  - include all the MRC data
  @param[in] ZqType   - Type of ZQ Calibration: see MrcZqType enum

  @retval MrcStatus - mrcSuccess if passes, otherwise an error status
**/
MrcStatus
MrcIssueZQ (
  IN  MrcParameters *const  MrcData,
  IN  MrcZqType             ZqType
  )
{
  MrcOutput *Outputs;
  MrcStatus Status;
  MrcDebug  *Debug;
  char      *StrZqType;

  Outputs = &MrcData->Outputs;
  Debug   = &Outputs->Debug;

  switch (ZqType) {
  case MRC_ZQ_INIT:
    StrZqType = "INIT";
    break;

  case MRC_ZQ_LONG:
    StrZqType = "LONG";
    break;

  case MRC_ZQ_SHORT:
    StrZqType = "SHORT";
    break;

  case MRC_ZQ_RESET:
    StrZqType = "RESET";
    break;

  case MRC_ZQ_CAL:
    StrZqType = "CAL";
    break;

  default:
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_ERROR, "Wrong ZQ type: %d\n", ZqType);
    return mrcWrongInputParameter;
  }

  if (!Outputs->JedecInitDone) {
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Issue ZQ %s on all channels / ranks\n", StrZqType);
  }

  // We only support:
  // MRC_ZQ_CAL for LPDDR4
  // MRC_ZQ_CAL for DDR5
  // MRC_ZQ_LONG for DDR4
  Status = MrcSendZq (MrcData);

  return Status;
}

/**
  Issue DDR4 MRS command (PDA Mode)

  @param[in]  MrcData   - Include all MRC global data.
  @param[in]  Controller- Memory Controller Number within the processor (0-based).
  @param[in]  Channel   - Channel to send MRS command to.
  @param[in]  Rank      - Rank to send MRS command to.
  @param[in]  Address   - MRS command value.
  @param[in]  MR        - Which MR command to send.
  @param[in]  DeviceMask- Which set(s) of 8 DQs to set to low for PDA mode.

  @retval mrcSuccess    - PDA command was sent successfully
  @retval mrcDeviceBusy - Timed out waiting for MRH
**/
MrcStatus
MrcDdr4PdaCmd (
  IN MrcParameters *const MrcData,
  IN UINT32               Controller,
  IN UINT32               Channel,
  IN UINT32               Rank,
  IN UINT16               Address,
  IN UINT8                MR,
  IN UINT16               DeviceMask
  )
{
  const MRC_FUNCTION                    *MrcCall;
  MrcDebug                              *Debug;
  MrcOutput                             *Outputs;
  INT64                                 Ddr4OneDpc;
  UINT32                                Offset;
  UINT8                                 Data;
  UINT8                                 MrCmdAdd;
  BOOLEAN                               Busy;
  MC0_CH0_CR_DDR_MR_COMMAND_STRUCT      MrCommand;
  UINT64                                Timeout;

  MrcCall = MrcData->Inputs.Call.Func;
  Outputs = &MrcData->Outputs;
  Debug   = &Outputs->Debug;

  Timeout = MrcCall->MrcGetCpuTime () + MRC_WAIT_TIMEOUT;   // 10 seconds timeout

  Offset = OFFSET_CALC_MC_CH (MC0_CH0_CR_DDR_MR_COMMAND_REG, MC1_CH0_CR_DDR_MR_COMMAND_REG, Controller, MC0_CH1_CR_DDR_MR_COMMAND_REG, Channel);

  // Remap target rank if ddr4_1dpc is enabled on this channel
  MrcGetSetMcCh (MrcData, Controller, Channel, GsmMccDdr4OneDpc, ReadFromCache, &Ddr4OneDpc);
  if (Ddr4OneDpc == 1) { // ddr4_1dpc enabled on DIMM0
    if (Rank == 1) {
      Rank = 3;
    }
  } else if (Ddr4OneDpc == 2) { // ddr4_1dpc enabled on DIMM1
    if (Rank == 2) {
      Rank = 0;
    }
  }

  //
  // Make sure MRH is not busy
  //
  do {
    MrCommand.Data = MrcReadCR (MrcData, Offset);
    Busy           = (MrCommand.Bits.Busy == 1);
  } while (Busy && (MrcCall->MrcGetCpuTime () < Timeout));
  if (Busy) {
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_ERROR, "Timed out waiting for previous MRH command to finish!\n");
    return mrcDeviceBusy;
  }

  Data = (Address & 0xFF);
  // MA13 is now in the Address field but this is unused in PDA.
  MrCmdAdd = ((UINT8) ((Address & 0x1F00) >> 5)) | (MR & 0x7);

  //
  // Send the PDA command
  //
  MrCommand.Data                 = 0;
  MrCommand.Bits.Data            = Data;
  MrCommand.Bits.Address         = MrCmdAdd;
  MrCommand.Bits.Rank            = Rank;
  MrCommand.Bits.Busy            = 1;
  MrCommand.Bits.DRAM_mask       = 0x1FF & (~DeviceMask); // 0 - for device enable
  MrCommand.Bits.Command         = 2; // Per DRAM addressability mode (PDA)
  MrCommand.Bits.Assume_idle     = 0;
  MrcWriteCR (MrcData, Offset, MrCommand.Data);

  //
  // Wait till MRH is done sending the command
  //
  Timeout = MrcCall->MrcGetCpuTime () + MRC_WAIT_TIMEOUT;   // 10 seconds timeout
  do {
    MrCommand.Data = MrcReadCR (MrcData, Offset);
    Busy           = (MrCommand.Bits.Busy == 1);
  } while (Busy && (MrcCall->MrcGetCpuTime () < Timeout));

  if (Busy) {
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_ERROR, "Timed out sending MRH command!\n");
    return mrcDeviceBusy;
  }

  return mrcSuccess;
}

VOID
MrcRankOccupancy (
  IN  MrcParameters * const MrcData
  )
{
  MrcOutput     *Outputs;
  MrcChannelOut *ChannelOut;
  INT64         GetSetVal;
  UINT32        Controller;
  UINT32        Channel;
  UINT32        IpChannel;
  UINT8         RankMask;
  BOOLEAN       Lpddr;
  UINT32        Offset;
  MC0_CH0_CR_MC_INIT_STATE_STRUCT McInitState;

  Outputs = &MrcData->Outputs;
  Lpddr = Outputs->Lpddr;

  for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
    if (Outputs->DdrType == MRC_DDR_TYPE_DDR4) {
      // Set the Rank_occupancy to zero for ch1 due to  Outputs->MaxChannels is set to 1 for ddr4.
      Offset = OFFSET_CALC_MC_CH (MC0_CH0_CR_MC_INIT_STATE_REG, MC1_CH0_CR_MC_INIT_STATE_REG, Controller, MC0_CH1_CR_MC_INIT_STATE_REG, 1);
      McInitState.Data = MrcReadCR (MrcData, Offset);
      McInitState.Bits.Rank_occupancy = 0;
      MrcWriteCR (MrcData, Offset, McInitState.Data);
    }

    for (Channel = 0; Channel < Outputs->MaxChannels; Channel++) {
      ChannelOut = &Outputs->Controller[Controller].Channel[Channel];
      RankMask   = ChannelOut->ValidRankBitMask;

      // Set the Rank_occupancy per channel.
      // For LPDDR technologies, the mapping is the following:
      //    Bit     Ch      Rank
      //    0   -   0         0
      //    1   -   0         1
      //    2   -   1         0
      //    3   -   1         1
      //    4   -   0         2
      //    5   -   0         3
      //    6   -   1         2
      //    7   -   1         3
      if (Lpddr) {
        if (((Channel % 2) == 0)) {
          GetSetVal = (RankMask & 0x3); // Bits 0/1
          GetSetVal |= (UINT64) ((RankMask & 0xC) << 2); // Bits 4/5
          // Only write the register on Channel 1/3 for LP.
          continue;
        } else {
          // Channel 1/3 rank pop start at bits 2
          GetSetVal |= (UINT64) ((RankMask & 0x3) << 2); // Bits 2/3
          GetSetVal |= (UINT64) ((RankMask & 0xC) << 4); // Bits 6/7
          //Set the channel to the previous channel as 1/3 do not have registers.
          IpChannel = Channel - 1;
        }
      } else {
        IpChannel = Channel;
        GetSetVal = ChannelOut->ValidRankBitMask;
      }
      MrcGetSetMcCh (MrcData, Controller, IpChannel, GsmMccRankOccupancy, WriteCached | PrintValue, &GetSetVal);
    } // Channel
  } // Controller
}

/**
  This function configures probeless config register.

  @param[in, out] MrcData - All the MRC global data.
**/
void
MrcProbelessConfig (
  IN OUT MrcParameters *const MrcData
  )
{
  const MrcInput  *Inputs;
  MrcOutput       *Outputs;
  MrcDebug        *Debug;
  UINT32          Controller;
  UINT32          Channel;
  UINT32          IpChannel;
  UINT32          Offset;
  BOOLEAN         Lpddr;
  INT64          GetSetVal;
  MC0_DDRPL_CFG_DTF_STRUCT            ProbelessCfg;
  MC0_CH0_CR_PL_AGENT_CFG_DTF_STRUCT  PlAgentCfg;
  Inputs  = &MrcData->Inputs;
  Outputs = &MrcData->Outputs;
  Debug   = &Outputs->Debug;
  Lpddr   = Outputs->Lpddr;

  for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
    if (!MrcControllerExist (MrcData, Controller)) {
      continue;
    }
    Offset = OFFSET_CALC_CH (MC0_DDRPL_CFG_DTF_REG, MC1_DDRPL_CFG_DTF_REG, Controller);
    ProbelessCfg.Data = MrcReadCR (MrcData, Offset);

    //If channel 0 is populated, program MCHTrace to 0. Else program to 1.
    //If DDR4 or LP memory subchannel 0 is populated for channel 0, program SCHTrace to 0.
    //Else if not DDR4 and subchannel 0 is not populated for channel 0, program SCHTrace to 1.
    Channel = (MrcChannelExist (MrcData, Controller, 0)) ? 0 : 1;
    ProbelessCfg.Bits.MCHTrace = Channel;
    ProbelessCfg.Bits.SCHTrace = Channel;

    //If ECC is enabled, set ECC_EN to 1.
    ProbelessCfg.Bits.ECC_EN = Outputs->EccSupport;

    //Set DDRPL_Activate based on setup option ProbelessTrace Enable or Disable. Default is Disabled.
    ProbelessCfg.Bits.DDRPL_Activate = Inputs->ProbelessTrace;

    if (Inputs->SafeMode) {
      ProbelessCfg.Bits.DDRPL_GLB_DRV_GATE_DIS = 1;
    }

    MRC_DEBUG_MSG (
      Debug,
      MSG_LEVEL_NOTE,
      "MCHTrace = 0x%x\nSCHTrace = 0x%x\nECC_EN = 0x%x\nDDRPL_Activate = 0x%x\n",
      ProbelessCfg.Bits.MCHTrace,
      ProbelessCfg.Bits.SCHTrace,
      ProbelessCfg.Bits.ECC_EN,
      ProbelessCfg.Bits.DDRPL_Activate
      );

    GetSetVal = 1;
    MrcGetSetMcCh (MrcData, MAX_CONTROLLER, MAX_CHANNEL, GsmMccBlockCke,  WriteNoCache, &GetSetVal);

    MrcWriteCR (MrcData, Offset, ProbelessCfg.Data);

    GetSetVal = 0;
    MrcGetSetMcCh (MrcData, MAX_CONTROLLER, MAX_CHANNEL, GsmMccBlockCke,  WriteNoCache, &GetSetVal); // Unblock CKE

    for (Channel = 0; Channel < MAX_CHANNEL; Channel++) {
      if (MrcChannelExist (MrcData, Controller, Channel)) {
        IpChannel = LP_IP_CH (Lpddr, Channel);
        Offset = OFFSET_CALC_MC_CH (MC0_CH0_CR_PL_AGENT_CFG_DTF_REG, MC1_CH0_CR_PL_AGENT_CFG_DTF_REG, Controller, MC0_CH1_CR_PL_AGENT_CFG_DTF_REG, IpChannel);
        PlAgentCfg.Data = MrcReadCR (MrcData, Offset);
        PlAgentCfg.Bits.data_trace_mode = 3;
        MrcWriteCR (MrcData, Offset, PlAgentCfg.Data);
      }
    } // Channel
  } // Controller
}

/**
  This function provides the initial configuration of the Memory Controller's
  maintenance services.  Some items are disabled during MRC training, and will need
  to be configured at the end of MRC.

  @params[in] MrcData - Pointer to MRC global data.
**/
VOID
MrcMaintenanceConfig (
  IN  MrcParameters * const MrcData
  )
{
  // Configure Rank Occupancy:
  MrcRankOccupancy (MrcData);
  // Disable Dqs Oscillator at the start of MRC.
  MrcDisableDqsOscillatorMc (MrcData);
  // Configure probeless.
  MrcProbelessConfig (MrcData);
}

/**
  Issue LPDDR & DDR5 MRW (Mode Register Write) command using MRH (Mode Register Handler).

  @param[in] MrcData      - Include all MRC global data.
  @param[in] Controller   - Controller to send MRW.
  @param[in] Channel      - the channel to work on
  @param[in] Rank         - the rank to work on
  @param[in] Address      - MRW address
  @param[in] Data         - MRW Data
  @param[in] DebugPrint   - when TRUE, will print debugging information

  @retval mrcSuccess    - MRW was sent successfully
  @retval mrcDeviceBusy - timed out waiting for MRH
**/
MrcStatus
MrcIssueMrw (
  IN MrcParameters *const MrcData,
  IN UINT32               Controller,
  IN UINT32               Channel,
  IN UINT32               Rank,
  IN UINT32               Address,
  IN UINT32               Data,
  IN BOOLEAN              DebugPrint
)
{
  MrcStatus Status;
  MrcDebug  *Debug;
  MrcOutput *Outputs;

  Outputs = &MrcData->Outputs;
  Debug = &Outputs->Debug;

  // Send the MRW
  Status = MrcRunMrh (
    MrcData,
    Controller,
    Channel,
    Rank,
    Address,
    Data,
    MRC_MRH_CMD_MRW,
    DebugPrint
    );

  if (DebugPrint && !Outputs->JedecInitDone) {
    MRC_DEBUG_MSG (
      Debug, MSG_LEVEL_NOTE,
      "MrcIssueMrw on mc %d ch %d rank %d: MR%d, Opcode=0x%02X\n",
      Controller, Channel, Rank, Address, Data
    );
  }

  return Status;
}

/**
  Issue LPDDR MRR (Mode Register Read) command using MRH (Mode Register Handler).
  Use DQ mapping array to deswizzle the MR data.

  @param[in]  MrcData    - Include all MRC global data.
  @param[in]  Controller - the controller to issue MRR.
  @param[in]  Channel    - the channel to work on
  @param[in]  Rank       - the rank to work on
  @param[in]  Address    - MRR address
  @param[out] Data       - MRR Data array per SDRAM device

  @retval mrcSuccess     - MRR was executed successfully
  @retval mrcDeviceBusy  - timed out waiting for MRH
**/
MrcStatus
MrcIssueMrr (
  IN MrcParameters *const MrcData,
  IN UINT32               Controller,
  IN UINT32               Channel,
  IN UINT32               Rank,
  IN UINT32               Address,
  OUT UINT8               Data[4]
)
{
  const MrcInput        *Inputs;
  MrcDebug              *Debug;
  const MRC_FUNCTION    *MrcCall;
  const MrcControllerIn *ControllerIn;
  const MrcChannelIn    *ChannelIn;
  MrcOutput             *Outputs;
  UINT32                OffsetMrrResult;
  UINT32                CurrCpuBit;
  UINT32                CurrCpuByte;
  UINT32                CpuByteCnt;
  UINT32                DeviceCnt;
  UINT32                DeviceMax;
  UINT32                CurrDramBit;
  UINT32                DramByte;
  UINT32                BitVal;
  UINT32                IpChannel;
  UINT32                DimmIdx;
  BOOLEAN               Lpddr;
  MC0_CH0_CR_DDR_MR_RESULT_0_STRUCT  MrResult;

  Inputs = &MrcData->Inputs;
  MrcCall = Inputs->Call.Func;
  Outputs = &MrcData->Outputs;
  Debug = &Outputs->Debug;
  ControllerIn = &Inputs->Controller[Controller];
  ChannelIn = &ControllerIn->Channel[Channel];
  CurrCpuByte = 0;
  Lpddr = Outputs->Lpddr;
  IpChannel = LP_IP_CH (Lpddr, Channel);
  MrcCall->MrcSetMem (Data, 4 * sizeof(Data[0]), 0);
  DimmIdx = Lpddr ? dDIMM0 : RANK_TO_DIMM_NUMBER (Rank); // Lpddr only uses DIMM0 entry

  if (Lpddr) {
    DeviceMax = (Outputs->LpByteMode) ? 2 : 1;
  } else {
    DeviceMax = 4;
  }

  // Send the MRR
  MrcRunMrh (MrcData, Controller, Channel, Rank, Address, 1, MRC_MRH_CMD_MRR, FALSE);
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "MrcIssueMrr on mc %u ch %u rank %u: MR%d\n", Controller, Channel, Rank, Address);

  OffsetMrrResult = OFFSET_CALC_MC_CH (MC0_CH0_CR_DDR_MR_RESULT_0_REG, MC1_CH0_CR_DDR_MR_RESULT_0_REG, Controller, MC0_CH1_CR_DDR_MR_RESULT_0_REG, IpChannel);
  MrResult.Data = MrcReadCR (MrcData, OffsetMrrResult);
  // MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "MrResult: 0x%08X\n", MrResult.Data);

  // Device now maps per byte with LPDDR.
  for (DeviceCnt = 0; DeviceCnt < DeviceMax; DeviceCnt++) {
    if (Lpddr) {
      DramByte = DeviceCnt;
      // Find which CPU byte is mapped to the relevant DRAM byte
      for (CpuByteCnt = 0; CpuByteCnt < Outputs->SdramCount; CpuByteCnt++) {
        if (DramByte == ChannelIn->DqsMapCpu2Dram[DimmIdx][CpuByteCnt]) {
          CurrCpuByte = CpuByteCnt;
          break;
        }
      }

      for (CurrCpuBit = 0; CurrCpuBit < MAX_BITS; CurrCpuBit++) {
        // The actual DRAM bit that is connected to the current CPU DQ pin
        CurrDramBit = ChannelIn->DqMapCpu2Dram[CurrCpuByte][CurrCpuBit] - 8 * DramByte; // Subtract 8xDramByte
        BitVal = (MrResult.Data8[DeviceCnt] >> CurrCpuBit) & 1; // The 0/1 value that came from the DRAM bit
        Data[DeviceCnt] |= (BitVal << CurrDramBit);             // Putting the value in the correct place
      }
    } else {
      // DDR5 MRR doesn't need DQ deswizzling
      Data[DeviceCnt] = MrResult.Data8[DeviceCnt];
    }
  }  // for DeviceCnt

  return mrcSuccess;
}

/**
  Issue NOP command using MRH (Mode Register Handler).

  @param[in] MrcData    - Include all MRC global data.
  @param[in] Controller - the controller to work on
  @param[in] Channel    - The channel to work on
  @param[in] Rank       - The rank to work on
  @param[in] DebugPrint - When TRUE, will print debugging information

  @retval mrcSuccess               - NOP was sent successfully
  @retval mrcDeviceBusy            - Timed out waiting for MRH
  @retval mrcUnsupportedTechnology - The memory technology is not supported
**/
MrcStatus
MrcIssueNop (
  IN MrcParameters *const MrcData,
  IN UINT32               Controller,
  IN UINT32               Channel,
  IN UINT32               Rank,
  IN BOOLEAN              DebugPrint
)
{
  MrcStatus Status;
  MrcDebug  *Debug;
  MrcOutput *Outputs;

  Outputs = &MrcData->Outputs;
  Debug = &Outputs->Debug;

  if (Outputs->DdrType != MRC_DDR_TYPE_DDR5) {
    return mrcWrongInputParameter;
  }

  // Send the MPC
  Status = MrcRunMrh (
    MrcData,
    Controller,
    Channel,
    Rank,
    0,
    0,
    MRC_MRH_CMD_DDR5_NOP,
    DebugPrint
    );

  if (DebugPrint) {
    MRC_DEBUG_MSG (
      Debug, MSG_LEVEL_NOTE,
      "MrcIssueNop on Mc %d Ch %d Rank %d\n",
      Controller, Channel, Rank
    );
  }

  return Status;
}

/**
  Issue VREFCA command using MRH (Mode Register Handler).

  @param[in] MrcData    - Include all MRC global data.
  @param[in] Controller - the controller to work on
  @param[in] Channel    - The channel to work on
  @param[in] Rank       - The rank to work on
  @param[in] Opcode     - VREFCA Opcode OP[7:0]
  @param[in] DebugPrint - When TRUE, will print debugging information

  @retval mrcSuccess               - VREFCA was sent successfully
  @retval mrcDeviceBusy            - Timed out waiting for MRH
  @retval mrcUnsupportedTechnology - The memory technology is not supported
**/
MrcStatus
MrcIssueVrefCa (
  IN MrcParameters *const MrcData,
  IN UINT32               Controller,
  IN UINT32               Channel,
  IN UINT32               Rank,
  IN UINT32               Opcode,
  IN BOOLEAN              DebugPrint
)
{
  MrcStatus Status;
  MrcDebug  *Debug;
  MrcOutput *Outputs;
  UINT32    tCK;
  UINT32    tMRD;
  UINT32    tMRDns;

  Outputs = &MrcData->Outputs;
  Debug = &Outputs->Debug;

  if (Outputs->DdrType != MRC_DDR_TYPE_DDR5) {
    return mrcUnsupportedTechnology;
  }

  // Send the MPC
  Status = MrcRunMrh (
    MrcData,
    Controller,
    Channel,
    Rank,
    Opcode,
    0,
    MRC_MRH_CMD_DDR5_VREFCA,
    DebugPrint
    );

  if (DebugPrint) {
    MRC_DEBUG_MSG (
      Debug, MSG_LEVEL_NOTE,
      "MrcIssueVrefCa on Mc %d Ch %d Rank %d: Opcode 0x%X\n",
      Controller, Channel, Rank, Opcode
    );
  }

  tCK    = Outputs->MemoryClock;
  tMRD   = tMODGet(MrcData, tCK);
  tMRDns = tMRD * tCK;
  tMRDns = DIVIDECEIL (tMRDns, FEMTOSECONDS_PER_NANOSECOND);

  // Wait tMRD after issuing VrefCA command
  MrcWait (MrcData, tMRDns);

  return Status;
}

/**
  Issue LPDDR4, LPDDR5, or DDR5 MPC command using MRH (Mode Register Handler).

  @param[in] MrcData    - Include all MRC global data.
  @param[in] Controller - the controller to work on
  @param[in] Channel    - The channel to work on
  @param[in] Rank       - The rank to work on
  @param[in] Opcode     - MPC Opcode (LP4: OP[6:0], LP5\DDR5: OP[7:0])
  @param[in] DebugPrint - When TRUE, will print debugging information

  @retval mrcSuccess    - MPC was sent successfully
  @retval mrcDeviceBusy - Timed out waiting for MRH
**/
MrcStatus
MrcIssueMpc (
  IN MrcParameters *const MrcData,
  IN UINT32               Controller,
  IN UINT32               Channel,
  IN UINT32               Rank,
  IN UINT32               Opcode,
  IN BOOLEAN              DebugPrint
)
{
  MrcStatus     Status;
  MrcDebug      *Debug;
  MrcInput      *Inputs;
  MrcOutput     *Outputs;
  MrcChannelOut *ChannelOut;
  MrcTiming     *Timing;
  INT64         GetSetVal;
  INT64         GetSetValSave;
  UINT32        tMpcNck;
  UINT32        tMpcNckFs;
  UINT32        tMpcNs;
  BOOLEAN       Ddr5Cmd2N;

  Inputs     = &MrcData->Inputs;
  Outputs    = &MrcData->Outputs;
  ChannelOut = &Outputs->Controller[Controller].Channel[Channel];
  Debug      = &Outputs->Debug;
  Timing     = &ChannelOut->Timing[Inputs->MemoryProfile];
  Ddr5Cmd2N  = (Outputs->DdrType == MRC_DDR_TYPE_DDR5) && (MrcGetNMode (MrcData) == 2);

  if (Ddr5Cmd2N) {
    GetSetVal = 1;
    MrcGetSetMcCh (MrcData, MAX_CONTROLLER, MAX_CHANNEL, GsmMccBlockXarb, WriteNoCache, &GetSetVal);  // Block XARB when changing cadb_enable
    GetSetVal = 0;
    MrcGetSetMcCh (MrcData, Controller, Channel, GsmMccCadbEnable, ReadNoCache, &GetSetValSave);
    MrcGetSetMcCh (MrcData, Controller, Channel, GsmMccCadbEnable, WriteNoCache, &GetSetVal);
    MrcGetSetMcCh (MrcData, MAX_CONTROLLER, MAX_CHANNEL, GsmMccBlockXarb, WriteNoCache, &GetSetVal); // Unblock XARB
  }

  // Send the MPC
  Status = MrcRunMrh (
    MrcData,
    Controller,
    Channel,
    Rank,
    Opcode,
    0,
    MRC_MRH_CMD_LP4_MPC, // Same as MRC_MRH_CMD_DDR5_MPC
    DebugPrint
    );

  if (DebugPrint) {
    MRC_DEBUG_MSG (
      Debug, MSG_LEVEL_NOTE,
      "MrcIssueMpc on Mc %d Ch %d Rank %d: Opcode 0x%X\n",
      Controller, Channel, Rank, Opcode
    );
  }

  tMpcNck = tMODGet (MrcData, Timing->tCK);
  tMpcNckFs = tMpcNck * Outputs->MemoryClock;
  tMpcNs = DIVIDECEIL (tMpcNckFs, FEMTOSECONDS_PER_NANOSECOND);

  MrcWait (MrcData, tMpcNs);

  if (Ddr5Cmd2N) {
    GetSetVal = 1;
    MrcGetSetMcCh (MrcData, MAX_CONTROLLER, MAX_CHANNEL, GsmMccBlockXarb, WriteNoCache, &GetSetVal);  // Block XARB when changing cadb_enable
    MrcGetSetMcCh (MrcData, Controller, Channel, GsmMccCadbEnable, WriteNoCache, &GetSetValSave);
    GetSetVal = 0;
    MrcGetSetMcCh (MrcData, MAX_CONTROLLER, MAX_CHANNEL, GsmMccBlockXarb, WriteNoCache, &GetSetVal); // Unblock XARB
  }

  return Status;
}

/**
  This function issues the PDA Select ID MPC.  The device ID's selected to listen to following
  MR/CAVref/MPC commands will be defined by Index.
  This will wait tMPC_Delay after sending the PDA Select ID MPC such that the caller does not need to wait
  to send the next command.
  To enable normal Rank operation mode for MR/CAVref/MPC commands, the caller must issue the PDA Select ID MPC
  after finishing the MR/CAVref/MPC updates with a value of 15 in Index.

  @param[in]  MrcData    - Pointer to global MRC data.
  @param[in]  Controller - 0-Based index of the Controller to access.
  @param[in]  Channel    - 0-Based index of the Channel to access.
  @param[in]  Rank       - 0-Based index of the Rank to access.
  @param[in]  Index      - 0-Based number of the PDA ID to select. 15 enables all devices
  @param[in]  DebugPrint - When TRUE, will print debugging information

  @retval MrcStatus - mrcTimeout if the FSM does not complete after 1s.
  @retval MrcStatus - mrcSuccess if the MPC is sent successfuly.
  @retval MrcStatus - mrcFail for unexepected failures.
**/
MrcStatus
MrcPdaSelect (
  IN  MrcParameters * const MrcData,
  IN  UINT32                Controller,
  IN  UINT32                Channel,
  IN  UINT32                Rank,
  IN  UINT8                 Index,
  IN BOOLEAN                DebugPrint
  )
{
  MrcOutput                   *Outputs;
  MrcStatus                   Status;
  MrcDebug                    *Debug;

  Status  = mrcSuccess;
  Outputs = &MrcData->Outputs;
  Debug   = &Outputs->Debug;

  if (DebugPrint) {
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE,
      "PdaSelect on MC%d C%d R%d: Device 0x%X\n",
      Controller, Channel, Rank, Index
    );
  }

  //Send PDA Select ID
  Status = MrcIssueMpc (MrcData, Controller, Channel, Rank, DDR5_MPC_PDA_SELECT_ID (Index), DebugPrint);

  return Status;
}

