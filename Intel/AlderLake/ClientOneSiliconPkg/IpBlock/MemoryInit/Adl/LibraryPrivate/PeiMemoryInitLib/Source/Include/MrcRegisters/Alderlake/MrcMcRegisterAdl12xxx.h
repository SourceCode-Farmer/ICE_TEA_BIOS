#ifndef __MrcMcRegisterAdl12xxx_h__
#define __MrcMcRegisterAdl12xxx_h__
/** @file
  This file was automatically generated. Modify at your own risk.
  Note that no error checking is done in these functions so ensure that the correct values are passed.

@copyright
  Copyright (c) 2010 - 2020 Intel Corporation. All rights reserved
  This software and associated documentation (if any) is furnished
  under a license and may only be used or copied in accordance
  with the terms of the license. Except as permitted by the
  license, no part of this software or documentation may be
  reproduced, stored in a retrieval system, or transmitted in any
  form or by any means without the express written consent of
  Intel Corporation.
  This file contains an 'Intel Peripheral Driver' and is uniquely
  identified as "Intel Reference Module" and is licensed for Intel
  CPUs and chipsets under the terms of your license agreement with
  Intel or your vendor. This file may be modified by the user, subject
  to additional terms of the license agreement.

@par Specification Reference:
**/

#pragma pack(push, 1)


#define CMF_LT_SA_W_PG_ACCESS_POLICY_CP_REG                            (0x00012000)
//Duplicate of CMF_0_LT_SA_W_PG_ACCESS_POLICY_CP_REG

#define CMF_LT_SA_W_PG_ACCESS_POLICY_WAC_REG                           (0x00012008)
//Duplicate of CMF_0_LT_SA_W_PG_ACCESS_POLICY_WAC_REG

#define CMF_LT_SA_W_PG_ACCESS_POLICY_RAC_REG                           (0x00012010)
//Duplicate of CMF_0_LT_SA_W_PG_ACCESS_POLICY_RAC_REG

#define CMF_P_U_CODE_PG_ACCESS_POLICY_CP_REG                           (0x00012018)
//Duplicate of CMF_0_LT_SA_W_PG_ACCESS_POLICY_CP_REG

#define CMF_P_U_CODE_PG_ACCESS_POLICY_WAC_REG                          (0x00012020)
//Duplicate of CMF_0_P_U_CODE_PG_ACCESS_POLICY_WAC_REG

#define CMF_P_U_CODE_PG_ACCESS_POLICY_RAC_REG                          (0x00012028)
//Duplicate of CMF_0_P_U_CODE_PG_ACCESS_POLICY_WAC_REG

#define CMF_LT_MEM_SA_PG_ACCESS_POLICY_CP_REG                          (0x00012030)
//Duplicate of CMF_0_LT_SA_W_PG_ACCESS_POLICY_CP_REG

#define CMF_LT_MEM_SA_PG_ACCESS_POLICY_WAC_REG                         (0x00012038)
//Duplicate of CMF_0_LT_SA_W_PG_ACCESS_POLICY_WAC_REG

#define CMF_LT_MEM_SA_PG_ACCESS_POLICY_RAC_REG                         (0x00012040)
//Duplicate of CMF_0_LT_SA_W_PG_ACCESS_POLICY_RAC_REG

#define CMF_OS_PG_ACCESS_POLICY_CP_REG                                 (0x00012048)
//Duplicate of CMF_0_LT_SA_W_PG_ACCESS_POLICY_CP_REG

#define CMF_OS_PG_ACCESS_POLICY_WAC_REG                                (0x00012050)
//Duplicate of CMF_0_OS_PG_ACCESS_POLICY_WAC_REG

#define CMF_OS_PG_ACCESS_POLICY_RAC_REG                                (0x00012058)
//Duplicate of CMF_0_LT_SA_W_PG_ACCESS_POLICY_RAC_REG

#define CMI_PRIO_THRESHOLD_REG                                         (0x00012060)
//Duplicate of CMI_0_PRIO_THRESHOLD_REG

#define CMI_PRIO_LIM_REG                                               (0x00012064)
//Duplicate of CMI_0_PRIO_LIM_REG

#define CMF_TOPOLOGY_GLOBAL_CFG_0_REG                                  (0x00012068)
//Duplicate of CMF_0_TOPOLOGY_GLOBAL_CFG_0_REG

#define CMF_TOPOLOGY_GLOBAL_CFG_1_REG                                  (0x0001206C)
//Duplicate of CMF_0_TOPOLOGY_GLOBAL_CFG_1_REG

#define CMF_TOPOLOGY_GLOBAL_CFG_2_REG                                  (0x00012070)
//Duplicate of CMF_0_TOPOLOGY_GLOBAL_CFG_2_REG

#define CMF_TOPOLOGY_GLOBAL_CFG_3_REG                                  (0x00012074)
//Duplicate of CMF_0_TOPOLOGY_GLOBAL_CFG_3_REG

#define CMF_TOPOLOGY_GLOBAL_CFG_4_REG                                  (0x00012078)
//Duplicate of CMF_0_TOPOLOGY_GLOBAL_CFG_4_REG

#define CMF_GLOBAL_CFG_1_REG                                           (0x00012080)
//Duplicate of CMF_0_GLOBAL_CFG_1_REG

#define CMI_IOSF_SB_EP_CTRL_REG                                        (0x00012084)
//Duplicate of CMI_0_IOSF_SB_EP_CTRL_REG

#define CMI_CLK_GATE_EN_0_REG                                          (0x00012088)
//Duplicate of CMI_0_CLK_GATE_EN_0_REG

#define CMI_CLK_GATE_EN_1_REG                                          (0x0001208C)
//Duplicate of CMI_0_CLK_GATE_EN_0_REG

#define CMF_RSVD0_REG                                                  (0x00012090)

  #define CMF_RSVD0_RESERVED_OFF                                       ( 0)
  #define CMF_RSVD0_RESERVED_WID                                       ( 6)
  #define CMF_RSVD0_RESERVED_MSK                                       (0x0000003F)
  #define CMF_RSVD0_RESERVED_MIN                                       (0)
  #define CMF_RSVD0_RESERVED_MAX                                       (63) // 0x0000003F
  #define CMF_RSVD0_RESERVED_DEF                                       (0x00000000)
  #define CMF_RSVD0_RESERVED_HSH                                       (0x06012090)

  #define CMF_RSVD0_vc1wr_window_OFF                                   ( 6)
  #define CMF_RSVD0_vc1wr_window_WID                                   ( 8)
  #define CMF_RSVD0_vc1wr_window_MSK                                   (0x00003FC0)
  #define CMF_RSVD0_vc1wr_window_MIN                                   (0)
  #define CMF_RSVD0_vc1wr_window_MAX                                   (255) // 0x000000FF
  #define CMF_RSVD0_vc1wr_window_DEF                                   (0x0000000F)
  #define CMF_RSVD0_vc1wr_window_HSH                                   (0x080D2090)

  #define CMF_RSVD0_update_vc1wr_window_OFF                            (14)
  #define CMF_RSVD0_update_vc1wr_window_WID                            ( 1)
  #define CMF_RSVD0_update_vc1wr_window_MSK                            (0x00004000)
  #define CMF_RSVD0_update_vc1wr_window_MIN                            (0)
  #define CMF_RSVD0_update_vc1wr_window_MAX                            (1) // 0x00000001
  #define CMF_RSVD0_update_vc1wr_window_DEF                            (0x00000000)
  #define CMF_RSVD0_update_vc1wr_window_HSH                            (0x011D2090)

  #define CMF_RSVD0_vc1wr_window_fix_disable_OFF                       (15)
  #define CMF_RSVD0_vc1wr_window_fix_disable_WID                       ( 1)
  #define CMF_RSVD0_vc1wr_window_fix_disable_MSK                       (0x00008000)
  #define CMF_RSVD0_vc1wr_window_fix_disable_MIN                       (0)
  #define CMF_RSVD0_vc1wr_window_fix_disable_MAX                       (1) // 0x00000001
  #define CMF_RSVD0_vc1wr_window_fix_disable_DEF                       (0x00000000)
  #define CMF_RSVD0_vc1wr_window_fix_disable_HSH                       (0x011F2090)

  #define CMF_RSVD0_vc0wr_const_prio_fix_en_OFF                        (16)
  #define CMF_RSVD0_vc0wr_const_prio_fix_en_WID                        ( 1)
  #define CMF_RSVD0_vc0wr_const_prio_fix_en_MSK                        (0x00010000)
  #define CMF_RSVD0_vc0wr_const_prio_fix_en_MIN                        (0)
  #define CMF_RSVD0_vc0wr_const_prio_fix_en_MAX                        (1) // 0x00000001
  #define CMF_RSVD0_vc0wr_const_prio_fix_en_DEF                        (0x00000000)
  #define CMF_RSVD0_vc0wr_const_prio_fix_en_HSH                        (0x01212090)

  #define CMF_RSVD0_gldv_gating_disable_OFF                            (17)
  #define CMF_RSVD0_gldv_gating_disable_WID                            ( 1)
  #define CMF_RSVD0_gldv_gating_disable_MSK                            (0x00020000)
  #define CMF_RSVD0_gldv_gating_disable_MIN                            (0)
  #define CMF_RSVD0_gldv_gating_disable_MAX                            (1) // 0x00000001
  #define CMF_RSVD0_gldv_gating_disable_DEF                            (0x00000000)
  #define CMF_RSVD0_gldv_gating_disable_HSH                            (0x01232090)

  #define CMF_RSVD0_DISABLE_EGRESS_FIFO_FOR_RD_CPL_OFF                 (18)
  #define CMF_RSVD0_DISABLE_EGRESS_FIFO_FOR_RD_CPL_WID                 ( 1)
  #define CMF_RSVD0_DISABLE_EGRESS_FIFO_FOR_RD_CPL_MSK                 (0x00040000)
  #define CMF_RSVD0_DISABLE_EGRESS_FIFO_FOR_RD_CPL_MIN                 (0)
  #define CMF_RSVD0_DISABLE_EGRESS_FIFO_FOR_RD_CPL_MAX                 (1) // 0x00000001
  #define CMF_RSVD0_DISABLE_EGRESS_FIFO_FOR_RD_CPL_DEF                 (0x00000000)
  #define CMF_RSVD0_DISABLE_EGRESS_FIFO_FOR_RD_CPL_HSH                 (0x01252090)

  #define CMF_RSVD0_RESERVED1_OFF                                      (19)
  #define CMF_RSVD0_RESERVED1_WID                                      (13)
  #define CMF_RSVD0_RESERVED1_MSK                                      (0xFFF80000)
  #define CMF_RSVD0_RESERVED1_MIN                                      (0)
  #define CMF_RSVD0_RESERVED1_MAX                                      (8191) // 0x00001FFF
  #define CMF_RSVD0_RESERVED1_DEF                                      (0x00000000)
  #define CMF_RSVD0_RESERVED1_HSH                                      (0x0D272090)

#define CMF_RSVD1_REG                                                  (0x00012094)
//Duplicate of CMF_0_RSVD1_REG

#define CMI_REQUEST_PORT_0_REQ_CONFIG_0_CHANNEL_0_REG                  (0x00012098)
//Duplicate of CMI_0_REQUEST_PORT_0_REQ_CONFIG_0_CHANNEL_0_REG

#define CMI_REQUEST_PORT_0_REQ_CONFIG_0_CHANNEL_1_REG                  (0x0001209C)
//Duplicate of CMI_0_REQUEST_PORT_0_REQ_CONFIG_0_CHANNEL_0_REG

#define CMI_REQUEST_PORT_0_REQ_CONFIG_0_CHANNEL_2_REG                  (0x000120A0)
//Duplicate of CMI_0_REQUEST_PORT_0_REQ_CONFIG_0_CHANNEL_2_REG

#define CMI_REQUEST_PORT_0_REQ_CONFIG_0_CHANNEL_3_REG                  (0x000120A4)
//Duplicate of CMI_0_REQUEST_PORT_0_REQ_CONFIG_0_CHANNEL_2_REG

#define CMI_REQUEST_PORT_0_REQ_CONFIG_1_REG                            (0x000120A8)
//Duplicate of CMI_0_REQUEST_PORT_0_REQ_CONFIG_1_REG

#define CMI_REQUEST_PORT_0_STALL_REG                                   (0x000120AC)
//Duplicate of CMI_0_REQUEST_PORT_0_STALL_REG

#define CMI_REQUEST_PORT_1_REQ_CONFIG_0_CHANNEL_0_REG                  (0x000120B0)
//Duplicate of CMI_0_REQUEST_PORT_0_REQ_CONFIG_0_CHANNEL_0_REG

#define CMI_REQUEST_PORT_1_REQ_CONFIG_0_CHANNEL_1_REG                  (0x000120B4)
//Duplicate of CMI_0_REQUEST_PORT_0_REQ_CONFIG_0_CHANNEL_0_REG

#define CMI_REQUEST_PORT_1_REQ_CONFIG_0_CHANNEL_2_REG                  (0x000120B8)
//Duplicate of CMI_0_REQUEST_PORT_0_REQ_CONFIG_0_CHANNEL_2_REG

#define CMI_REQUEST_PORT_1_REQ_CONFIG_0_CHANNEL_3_REG                  (0x000120BC)
//Duplicate of CMI_0_REQUEST_PORT_0_REQ_CONFIG_0_CHANNEL_2_REG

#define CMI_REQUEST_PORT_1_REQ_CONFIG_1_REG                            (0x000120C0)
//Duplicate of CMI_0_REQUEST_PORT_0_REQ_CONFIG_1_REG

#define CMI_REQUEST_PORT_1_STALL_REG                                   (0x000120C4)
//Duplicate of CMI_0_REQUEST_PORT_0_STALL_REG

#define CMI_REQUEST_PORT_2_REQ_CONFIG_0_CHANNEL_0_REG                  (0x000120C8)
//Duplicate of CMI_0_REQUEST_PORT_0_REQ_CONFIG_0_CHANNEL_0_REG

#define CMI_REQUEST_PORT_2_REQ_CONFIG_0_CHANNEL_1_REG                  (0x000120CC)
//Duplicate of CMI_0_REQUEST_PORT_0_REQ_CONFIG_0_CHANNEL_0_REG

#define CMI_REQUEST_PORT_2_REQ_CONFIG_0_CHANNEL_2_REG                  (0x000120D0)
//Duplicate of CMI_0_REQUEST_PORT_0_REQ_CONFIG_0_CHANNEL_2_REG

#define CMI_REQUEST_PORT_2_REQ_CONFIG_0_CHANNEL_3_REG                  (0x000120D4)
//Duplicate of CMI_0_REQUEST_PORT_0_REQ_CONFIG_0_CHANNEL_2_REG

#define CMI_REQUEST_PORT_2_REQ_CONFIG_1_REG                            (0x000120D8)
//Duplicate of CMI_0_REQUEST_PORT_0_REQ_CONFIG_1_REG

#define CMI_REQUEST_PORT_2_STALL_REG                                   (0x000120DC)
//Duplicate of CMI_0_REQUEST_PORT_0_STALL_REG

#define CMI_REQUEST_PORT_3_REQ_CONFIG_0_CHANNEL_0_REG                  (0x000120E0)
//Duplicate of CMI_0_REQUEST_PORT_0_REQ_CONFIG_0_CHANNEL_0_REG

#define CMI_REQUEST_PORT_3_REQ_CONFIG_0_CHANNEL_1_REG                  (0x000120E4)
//Duplicate of CMI_0_REQUEST_PORT_0_REQ_CONFIG_0_CHANNEL_0_REG

#define CMI_REQUEST_PORT_3_REQ_CONFIG_0_CHANNEL_2_REG                  (0x000120E8)
//Duplicate of CMI_0_REQUEST_PORT_0_REQ_CONFIG_0_CHANNEL_2_REG

#define CMI_REQUEST_PORT_3_REQ_CONFIG_0_CHANNEL_3_REG                  (0x000120EC)
//Duplicate of CMI_0_REQUEST_PORT_0_REQ_CONFIG_0_CHANNEL_2_REG

#define CMI_REQUEST_PORT_3_REQ_CONFIG_1_REG                            (0x000120F0)
//Duplicate of CMI_0_REQUEST_PORT_0_REQ_CONFIG_1_REG

#define CMI_REQUEST_PORT_3_STALL_REG                                   (0x000120F4)
//Duplicate of CMI_0_REQUEST_PORT_0_STALL_REG

#define CMI_RESPOND_PORT_0_CPL_CONFIG_0_CHANNEL_0_REG                  (0x000120F8)
//Duplicate of CMI_0_RESPOND_PORT_0_CPL_CONFIG_0_CHANNEL_0_REG

#define CMI_RESPOND_PORT_0_CPL_CONFIG_0_CHANNEL_1_REG                  (0x000120FC)
//Duplicate of CMI_0_RESPOND_PORT_0_CPL_CONFIG_0_CHANNEL_0_REG

#define CMI_RESPOND_PORT_0_CPL_CONFIG_0_CHANNEL_2_REG                  (0x00012100)
//Duplicate of CMI_0_RESPOND_PORT_0_CPL_CONFIG_0_CHANNEL_0_REG

#define CMI_RESPOND_PORT_0_CPL_CONFIG_0_CHANNEL_3_REG                  (0x00012104)
//Duplicate of CMI_0_RESPOND_PORT_0_CPL_CONFIG_0_CHANNEL_0_REG

#define CMI_RESPOND_PORT_0_CPL_CONFIG_1_REG                            (0x00012108)
//Duplicate of CMI_0_RESPOND_PORT_0_CPL_CONFIG_1_REG

#define CMI_RESPOND_PORT_0_STALL_REG                                   (0x0001210C)
//Duplicate of CMI_0_RESPOND_PORT_0_STALL_REG

#define CMI_RESPOND_PORT_1_CPL_CONFIG_0_CHANNEL_0_REG                  (0x00012110)
//Duplicate of CMI_0_RESPOND_PORT_0_CPL_CONFIG_0_CHANNEL_0_REG

#define CMI_RESPOND_PORT_1_CPL_CONFIG_0_CHANNEL_1_REG                  (0x00012114)
//Duplicate of CMI_0_RESPOND_PORT_0_CPL_CONFIG_0_CHANNEL_0_REG

#define CMI_RESPOND_PORT_1_CPL_CONFIG_0_CHANNEL_2_REG                  (0x00012118)
//Duplicate of CMI_0_RESPOND_PORT_0_CPL_CONFIG_0_CHANNEL_0_REG

#define CMI_RESPOND_PORT_1_CPL_CONFIG_0_CHANNEL_3_REG                  (0x0001211C)
//Duplicate of CMI_0_RESPOND_PORT_0_CPL_CONFIG_0_CHANNEL_0_REG

#define CMI_RESPOND_PORT_1_CPL_CONFIG_1_REG                            (0x00012120)
//Duplicate of CMI_0_RESPOND_PORT_0_CPL_CONFIG_1_REG

#define CMI_RESPOND_PORT_1_STALL_REG                                   (0x00012124)
//Duplicate of CMI_0_RESPOND_PORT_0_STALL_REG

#define CMI_RESPOND_PORT_2_CPL_CONFIG_0_CHANNEL_0_REG                  (0x00012128)
//Duplicate of CMI_0_RESPOND_PORT_0_CPL_CONFIG_0_CHANNEL_0_REG

#define CMI_RESPOND_PORT_2_CPL_CONFIG_0_CHANNEL_1_REG                  (0x0001212C)
//Duplicate of CMI_0_RESPOND_PORT_0_CPL_CONFIG_0_CHANNEL_0_REG

#define CMI_RESPOND_PORT_2_CPL_CONFIG_0_CHANNEL_2_REG                  (0x00012130)
//Duplicate of CMI_0_RESPOND_PORT_0_CPL_CONFIG_0_CHANNEL_0_REG

#define CMI_RESPOND_PORT_2_CPL_CONFIG_0_CHANNEL_3_REG                  (0x00012134)
//Duplicate of CMI_0_RESPOND_PORT_0_CPL_CONFIG_0_CHANNEL_0_REG

#define CMI_RESPOND_PORT_2_CPL_CONFIG_1_REG                            (0x00012138)
//Duplicate of CMI_0_RESPOND_PORT_0_CPL_CONFIG_1_REG

#define CMI_RESPOND_PORT_2_STALL_REG                                   (0x0001213C)
//Duplicate of CMI_0_RESPOND_PORT_0_STALL_REG

#define CMI_RESPOND_PORT_3_CPL_CONFIG_0_CHANNEL_0_REG                  (0x00012140)
//Duplicate of CMI_0_RESPOND_PORT_0_CPL_CONFIG_0_CHANNEL_0_REG

#define CMI_RESPOND_PORT_3_CPL_CONFIG_0_CHANNEL_1_REG                  (0x00012144)
//Duplicate of CMI_0_RESPOND_PORT_0_CPL_CONFIG_0_CHANNEL_0_REG

#define CMI_RESPOND_PORT_3_CPL_CONFIG_0_CHANNEL_2_REG                  (0x00012148)
//Duplicate of CMI_0_RESPOND_PORT_0_CPL_CONFIG_0_CHANNEL_0_REG

#define CMI_RESPOND_PORT_3_CPL_CONFIG_0_CHANNEL_3_REG                  (0x0001214C)
//Duplicate of CMI_0_RESPOND_PORT_0_CPL_CONFIG_0_CHANNEL_0_REG

#define CMI_RESPOND_PORT_3_CPL_CONFIG_1_REG                            (0x00012150)
//Duplicate of CMI_0_RESPOND_PORT_0_CPL_CONFIG_1_REG

#define CMI_RESPOND_PORT_3_STALL_REG                                   (0x00012154)
//Duplicate of CMI_0_RESPOND_PORT_0_STALL_REG

#define CMI_MAD_SLICE_REG                                              (0x00012158)

  #define CMI_MAD_SLICE_Reserved_0_OFF                                 ( 0)
  #define CMI_MAD_SLICE_Reserved_0_WID                                 ( 4)
  #define CMI_MAD_SLICE_Reserved_0_MSK                                 (0x0000000F)
  #define CMI_MAD_SLICE_Reserved_0_MIN                                 (0)
  #define CMI_MAD_SLICE_Reserved_0_MAX                                 (15) // 0x0000000F
  #define CMI_MAD_SLICE_Reserved_0_DEF                                 (0x00000000)
  #define CMI_MAD_SLICE_Reserved_0_HSH                                 (0x04012158)

  #define CMI_MAD_SLICE_MS_L_MAP_OFF                                   ( 4)
  #define CMI_MAD_SLICE_MS_L_MAP_WID                                   ( 1)
  #define CMI_MAD_SLICE_MS_L_MAP_MSK                                   (0x00000010)
  #define CMI_MAD_SLICE_MS_L_MAP_MIN                                   (0)
  #define CMI_MAD_SLICE_MS_L_MAP_MAX                                   (1) // 0x00000001
  #define CMI_MAD_SLICE_MS_L_MAP_DEF                                   (0x00000000)
  #define CMI_MAD_SLICE_MS_L_MAP_HSH                                   (0x01092158)

  #define CMI_MAD_SLICE_Reserved_1_OFF                                 ( 5)
  #define CMI_MAD_SLICE_Reserved_1_WID                                 ( 3)
  #define CMI_MAD_SLICE_Reserved_1_MSK                                 (0x000000E0)
  #define CMI_MAD_SLICE_Reserved_1_MIN                                 (0)
  #define CMI_MAD_SLICE_Reserved_1_MAX                                 (7) // 0x00000007
  #define CMI_MAD_SLICE_Reserved_1_DEF                                 (0x00000000)
  #define CMI_MAD_SLICE_Reserved_1_HSH                                 (0x030B2158)

  #define CMI_MAD_SLICE_STKD_MODE_OFF                                  ( 8)
  #define CMI_MAD_SLICE_STKD_MODE_WID                                  ( 1)
  #define CMI_MAD_SLICE_STKD_MODE_MSK                                  (0x00000100)
  #define CMI_MAD_SLICE_STKD_MODE_MIN                                  (0)
  #define CMI_MAD_SLICE_STKD_MODE_MAX                                  (1) // 0x00000001
  #define CMI_MAD_SLICE_STKD_MODE_DEF                                  (0x00000000)
  #define CMI_MAD_SLICE_STKD_MODE_HSH                                  (0x01112158)

  #define CMI_MAD_SLICE_STKD_MODE_MS1_OFF                              ( 9)
  #define CMI_MAD_SLICE_STKD_MODE_MS1_WID                              ( 1)
  #define CMI_MAD_SLICE_STKD_MODE_MS1_MSK                              (0x00000200)
  #define CMI_MAD_SLICE_STKD_MODE_MS1_MIN                              (0)
  #define CMI_MAD_SLICE_STKD_MODE_MS1_MAX                              (1) // 0x00000001
  #define CMI_MAD_SLICE_STKD_MODE_MS1_DEF                              (0x00000000)
  #define CMI_MAD_SLICE_STKD_MODE_MS1_HSH                              (0x01132158)

  #define CMI_MAD_SLICE_Reserved_2_OFF                                 (10)
  #define CMI_MAD_SLICE_Reserved_2_WID                                 ( 2)
  #define CMI_MAD_SLICE_Reserved_2_MSK                                 (0x00000C00)
  #define CMI_MAD_SLICE_Reserved_2_MIN                                 (0)
  #define CMI_MAD_SLICE_Reserved_2_MAX                                 (3) // 0x00000003
  #define CMI_MAD_SLICE_Reserved_2_DEF                                 (0x00000000)
  #define CMI_MAD_SLICE_Reserved_2_HSH                                 (0x02152158)

  #define CMI_MAD_SLICE_MS_S_SIZE_OFF                                  (12)
  #define CMI_MAD_SLICE_MS_S_SIZE_WID                                  (11)
  #define CMI_MAD_SLICE_MS_S_SIZE_MSK                                  (0x007FF000)
  #define CMI_MAD_SLICE_MS_S_SIZE_MIN                                  (0)
  #define CMI_MAD_SLICE_MS_S_SIZE_MAX                                  (2047) // 0x000007FF
  #define CMI_MAD_SLICE_MS_S_SIZE_DEF                                  (0x00000000)
  #define CMI_MAD_SLICE_MS_S_SIZE_HSH                                  (0x0B192158)

  #define CMI_MAD_SLICE_Reserved_3_OFF                                 (23)
  #define CMI_MAD_SLICE_Reserved_3_WID                                 ( 1)
  #define CMI_MAD_SLICE_Reserved_3_MSK                                 (0x00800000)
  #define CMI_MAD_SLICE_Reserved_3_MIN                                 (0)
  #define CMI_MAD_SLICE_Reserved_3_MAX                                 (1) // 0x00000001
  #define CMI_MAD_SLICE_Reserved_3_DEF                                 (0x00000000)
  #define CMI_MAD_SLICE_Reserved_3_HSH                                 (0x012F2158)

  #define CMI_MAD_SLICE_STKD_MODE_MS_BITS_OFF                          (24)
  #define CMI_MAD_SLICE_STKD_MODE_MS_BITS_WID                          ( 4)
  #define CMI_MAD_SLICE_STKD_MODE_MS_BITS_MSK                          (0x0F000000)
  #define CMI_MAD_SLICE_STKD_MODE_MS_BITS_MIN                          (0)
  #define CMI_MAD_SLICE_STKD_MODE_MS_BITS_MAX                          (15) // 0x0000000F
  #define CMI_MAD_SLICE_STKD_MODE_MS_BITS_DEF                          (0x00000000)
  #define CMI_MAD_SLICE_STKD_MODE_MS_BITS_HSH                          (0x04312158)

  #define CMI_MAD_SLICE_Reserved_4_OFF                                 (28)
  #define CMI_MAD_SLICE_Reserved_4_WID                                 ( 4)
  #define CMI_MAD_SLICE_Reserved_4_MSK                                 (0xF0000000)
  #define CMI_MAD_SLICE_Reserved_4_MIN                                 (0)
  #define CMI_MAD_SLICE_Reserved_4_MAX                                 (15) // 0x0000000F
  #define CMI_MAD_SLICE_Reserved_4_DEF                                 (0x00000000)
  #define CMI_MAD_SLICE_Reserved_4_HSH                                 (0x04392158)

#define CMI_TOLUD_REG                                                  (0x0001215C)

  #define CMI_TOLUD_Reserved_OFF                                       ( 0)
  #define CMI_TOLUD_Reserved_WID                                       (20)
  #define CMI_TOLUD_Reserved_MSK                                       (0x000FFFFF)
  #define CMI_TOLUD_Reserved_MIN                                       (0)
  #define CMI_TOLUD_Reserved_MAX                                       (1048575) // 0x000FFFFF
  #define CMI_TOLUD_Reserved_DEF                                       (0x00000000)
  #define CMI_TOLUD_Reserved_HSH                                       (0x1401215C)

  #define CMI_TOLUD_TOLUD_OFF                                          (20)
  #define CMI_TOLUD_TOLUD_WID                                          (12)
  #define CMI_TOLUD_TOLUD_MSK                                          (0xFFF00000)
  #define CMI_TOLUD_TOLUD_MIN                                          (0)
  #define CMI_TOLUD_TOLUD_MAX                                          (4095) // 0x00000FFF
  #define CMI_TOLUD_TOLUD_DEF                                          (0x00000001)
  #define CMI_TOLUD_TOLUD_HSH                                          (0x0C29215C)

#define CMI_MEMORY_SLICE_HASH_REG                                      (0x00012160)

  #define CMI_MEMORY_SLICE_HASH_MC_org_OFF                             ( 0)
  #define CMI_MEMORY_SLICE_HASH_MC_org_WID                             ( 4)
  #define CMI_MEMORY_SLICE_HASH_MC_org_MSK                             (0x0000000F)
  #define CMI_MEMORY_SLICE_HASH_MC_org_MIN                             (0)
  #define CMI_MEMORY_SLICE_HASH_MC_org_MAX                             (15) // 0x0000000F
  #define CMI_MEMORY_SLICE_HASH_MC_org_DEF                             (0x00000004)
  #define CMI_MEMORY_SLICE_HASH_MC_org_HSH                             (0x44012160)

  #define CMI_MEMORY_SLICE_HASH_Reserved_0_OFF                         ( 4)
  #define CMI_MEMORY_SLICE_HASH_Reserved_0_WID                         ( 2)
  #define CMI_MEMORY_SLICE_HASH_Reserved_0_MSK                         (0x00000030)
  #define CMI_MEMORY_SLICE_HASH_Reserved_0_MIN                         (0)
  #define CMI_MEMORY_SLICE_HASH_Reserved_0_MAX                         (3) // 0x00000003
  #define CMI_MEMORY_SLICE_HASH_Reserved_0_DEF                         (0x00000000)
  #define CMI_MEMORY_SLICE_HASH_Reserved_0_HSH                         (0x42092160)

  #define CMI_MEMORY_SLICE_HASH_HASH_MASK_OFF                          ( 6)
  #define CMI_MEMORY_SLICE_HASH_HASH_MASK_WID                          (14)
  #define CMI_MEMORY_SLICE_HASH_HASH_MASK_MSK                          (0x000FFFC0)
  #define CMI_MEMORY_SLICE_HASH_HASH_MASK_MIN                          (0)
  #define CMI_MEMORY_SLICE_HASH_HASH_MASK_MAX                          (16383) // 0x00003FFF
  #define CMI_MEMORY_SLICE_HASH_HASH_MASK_DEF                          (0x00000001)
  #define CMI_MEMORY_SLICE_HASH_HASH_MASK_HSH                          (0x4E0D2160)

  #define CMI_MEMORY_SLICE_HASH_Reserved_1_OFF                         (20)
  #define CMI_MEMORY_SLICE_HASH_Reserved_1_WID                         ( 4)
  #define CMI_MEMORY_SLICE_HASH_Reserved_1_MSK                         (0x00F00000)
  #define CMI_MEMORY_SLICE_HASH_Reserved_1_MIN                         (0)
  #define CMI_MEMORY_SLICE_HASH_Reserved_1_MAX                         (15) // 0x0000000F
  #define CMI_MEMORY_SLICE_HASH_Reserved_1_DEF                         (0x00000000)
  #define CMI_MEMORY_SLICE_HASH_Reserved_1_HSH                         (0x44292160)

  #define CMI_MEMORY_SLICE_HASH_HASH_LSB_MASK_BIT_OFF                  (24)
  #define CMI_MEMORY_SLICE_HASH_HASH_LSB_MASK_BIT_WID                  ( 3)
  #define CMI_MEMORY_SLICE_HASH_HASH_LSB_MASK_BIT_MSK                  (0x07000000)
  #define CMI_MEMORY_SLICE_HASH_HASH_LSB_MASK_BIT_MIN                  (0)
  #define CMI_MEMORY_SLICE_HASH_HASH_LSB_MASK_BIT_MAX                  (7) // 0x00000007
  #define CMI_MEMORY_SLICE_HASH_HASH_LSB_MASK_BIT_DEF                  (0x00000000)
  #define CMI_MEMORY_SLICE_HASH_HASH_LSB_MASK_BIT_HSH                  (0x43312160)

  #define CMI_MEMORY_SLICE_HASH_Reserved_2_OFF                         (27)
  #define CMI_MEMORY_SLICE_HASH_Reserved_2_WID                         ( 1)
  #define CMI_MEMORY_SLICE_HASH_Reserved_2_MSK                         (0x08000000)
  #define CMI_MEMORY_SLICE_HASH_Reserved_2_MIN                         (0)
  #define CMI_MEMORY_SLICE_HASH_Reserved_2_MAX                         (1) // 0x00000001
  #define CMI_MEMORY_SLICE_HASH_Reserved_2_DEF                         (0x00000000)
  #define CMI_MEMORY_SLICE_HASH_Reserved_2_HSH                         (0x41372160)

  #define CMI_MEMORY_SLICE_HASH_L_shape_dec_mask_OFF                   (28)
  #define CMI_MEMORY_SLICE_HASH_L_shape_dec_mask_WID                   ( 4)
  #define CMI_MEMORY_SLICE_HASH_L_shape_dec_mask_MSK                   (0xF0000000)
  #define CMI_MEMORY_SLICE_HASH_L_shape_dec_mask_MIN                   (0)
  #define CMI_MEMORY_SLICE_HASH_L_shape_dec_mask_MAX                   (15) // 0x0000000F
  #define CMI_MEMORY_SLICE_HASH_L_shape_dec_mask_DEF                   (0x00000000)
  #define CMI_MEMORY_SLICE_HASH_L_shape_dec_mask_HSH                   (0x44392160)

  #define CMI_MEMORY_SLICE_HASH_L_shape_boundary_OFF                   (32)
  #define CMI_MEMORY_SLICE_HASH_L_shape_boundary_WID                   ( 4)
  #define CMI_MEMORY_SLICE_HASH_L_shape_boundary_MSK                   (0x0000000F00000000ULL)
  #define CMI_MEMORY_SLICE_HASH_L_shape_boundary_MIN                   (0)
  #define CMI_MEMORY_SLICE_HASH_L_shape_boundary_MAX                   (15) // 0x0000000F
  #define CMI_MEMORY_SLICE_HASH_L_shape_boundary_DEF                   (0x00000000)
  #define CMI_MEMORY_SLICE_HASH_L_shape_boundary_HSH                   (0x44412160)

  #define CMI_MEMORY_SLICE_HASH_Reserved_3_OFF                         (36)
  #define CMI_MEMORY_SLICE_HASH_Reserved_3_WID                         (28)
  #define CMI_MEMORY_SLICE_HASH_Reserved_3_MSK                         (0xFFFFFFF000000000ULL)
  #define CMI_MEMORY_SLICE_HASH_Reserved_3_MIN                         (0)
  #define CMI_MEMORY_SLICE_HASH_Reserved_3_MAX                         (268435455) // 0x0FFFFFFF
  #define CMI_MEMORY_SLICE_HASH_Reserved_3_DEF                         (0x00000000)
  #define CMI_MEMORY_SLICE_HASH_Reserved_3_HSH                         (0x5C492160)

#define CMI_REMAP_BASE_REG                                             (0x00012168)

  #define CMI_REMAP_BASE_Reserved_0_OFF                                ( 0)
  #define CMI_REMAP_BASE_Reserved_0_WID                                (20)
  #define CMI_REMAP_BASE_Reserved_0_MSK                                (0x000FFFFF)
  #define CMI_REMAP_BASE_Reserved_0_MIN                                (0)
  #define CMI_REMAP_BASE_Reserved_0_MAX                                (1048575) // 0x000FFFFF
  #define CMI_REMAP_BASE_Reserved_0_DEF                                (0x00000000)
  #define CMI_REMAP_BASE_Reserved_0_HSH                                (0x54012168)

  #define CMI_REMAP_BASE_REMAPBASE_OFF                                 (20)
  #define CMI_REMAP_BASE_REMAPBASE_WID                                 (22)
  #define CMI_REMAP_BASE_REMAPBASE_MSK                                 (0x000003FFFFF00000ULL)
  #define CMI_REMAP_BASE_REMAPBASE_MIN                                 (0)
  #define CMI_REMAP_BASE_REMAPBASE_MAX                                 (4194303) // 0x003FFFFF
  #define CMI_REMAP_BASE_REMAPBASE_DEF                                 (0x0007FFFF)
  #define CMI_REMAP_BASE_REMAPBASE_HSH                                 (0x56292168)

  #define CMI_REMAP_BASE_Reserved_1_OFF                                (42)
  #define CMI_REMAP_BASE_Reserved_1_WID                                (22)
  #define CMI_REMAP_BASE_Reserved_1_MSK                                (0xFFFFFC0000000000ULL)
  #define CMI_REMAP_BASE_Reserved_1_MIN                                (0)
  #define CMI_REMAP_BASE_Reserved_1_MAX                                (4194303) // 0x003FFFFF
  #define CMI_REMAP_BASE_Reserved_1_DEF                                (0x00000000)
  #define CMI_REMAP_BASE_Reserved_1_HSH                                (0x56552168)

#define CMI_REMAP_LIMIT_REG                                            (0x00012170)

  #define CMI_REMAP_LIMIT_Reserved_0_OFF                               ( 0)
  #define CMI_REMAP_LIMIT_Reserved_0_WID                               (20)
  #define CMI_REMAP_LIMIT_Reserved_0_MSK                               (0x000FFFFF)
  #define CMI_REMAP_LIMIT_Reserved_0_MIN                               (0)
  #define CMI_REMAP_LIMIT_Reserved_0_MAX                               (1048575) // 0x000FFFFF
  #define CMI_REMAP_LIMIT_Reserved_0_DEF                               (0x000FFFFF)
  #define CMI_REMAP_LIMIT_Reserved_0_HSH                               (0x54012170)

  #define CMI_REMAP_LIMIT_REMAPLMT_OFF                                 (20)
  #define CMI_REMAP_LIMIT_REMAPLMT_WID                                 (22)
  #define CMI_REMAP_LIMIT_REMAPLMT_MSK                                 (0x000003FFFFF00000ULL)
  #define CMI_REMAP_LIMIT_REMAPLMT_MIN                                 (0)
  #define CMI_REMAP_LIMIT_REMAPLMT_MAX                                 (4194303) // 0x003FFFFF
  #define CMI_REMAP_LIMIT_REMAPLMT_DEF                                 (0x00000000)
  #define CMI_REMAP_LIMIT_REMAPLMT_HSH                                 (0x56292170)

  #define CMI_REMAP_LIMIT_Reserved_1_OFF                               (42)
  #define CMI_REMAP_LIMIT_Reserved_1_WID                               (22)
  #define CMI_REMAP_LIMIT_Reserved_1_MSK                               (0xFFFFFC0000000000ULL)
  #define CMI_REMAP_LIMIT_Reserved_1_MIN                               (0)
  #define CMI_REMAP_LIMIT_Reserved_1_MAX                               (4194303) // 0x003FFFFF
  #define CMI_REMAP_LIMIT_Reserved_1_DEF                               (0x00000000)
  #define CMI_REMAP_LIMIT_Reserved_1_HSH                               (0x56552170)

#define CMI_PMEM_1LM_REG                                               (0x00012178)

  #define CMI_PMEM_1LM_PersistentSpace_OFF                             ( 0)
  #define CMI_PMEM_1LM_PersistentSpace_WID                             (24)
  #define CMI_PMEM_1LM_PersistentSpace_MSK                             (0x00FFFFFF)
  #define CMI_PMEM_1LM_PersistentSpace_MIN                             (0)
  #define CMI_PMEM_1LM_PersistentSpace_MAX                             (16777215) // 0x00FFFFFF
  #define CMI_PMEM_1LM_PersistentSpace_DEF                             (0x00000001)
  #define CMI_PMEM_1LM_PersistentSpace_HSH                             (0x18012178)

  #define CMI_PMEM_1LM_reserved_OFF                                    (24)
  #define CMI_PMEM_1LM_reserved_WID                                    ( 7)
  #define CMI_PMEM_1LM_reserved_MSK                                    (0x7F000000)
  #define CMI_PMEM_1LM_reserved_MIN                                    (0)
  #define CMI_PMEM_1LM_reserved_MAX                                    (127) // 0x0000007F
  #define CMI_PMEM_1LM_reserved_DEF                                    (0x00000000)
  #define CMI_PMEM_1LM_reserved_HSH                                    (0x07312178)

  #define CMI_PMEM_1LM_EN_1LM_PMEM_OFF                                 (31)
  #define CMI_PMEM_1LM_EN_1LM_PMEM_WID                                 ( 1)
  #define CMI_PMEM_1LM_EN_1LM_PMEM_MSK                                 (0x80000000)
  #define CMI_PMEM_1LM_EN_1LM_PMEM_MIN                                 (0)
  #define CMI_PMEM_1LM_EN_1LM_PMEM_MAX                                 (1) // 0x00000001
  #define CMI_PMEM_1LM_EN_1LM_PMEM_DEF                                 (0x00000000)
  #define CMI_PMEM_1LM_EN_1LM_PMEM_HSH                                 (0x013F2178)

#define CMI_RESPOND_PORT_0_RD_COUNTER_CHANNEL_0_REG                    (0x00012180)
//Duplicate of CMI_0_RESPOND_PORT_0_RD_COUNTER_CHANNEL_0_REG

#define CMI_RESPOND_PORT_0_RD_COUNTER_CHANNEL_1_REG                    (0x00012188)
//Duplicate of CMI_0_RESPOND_PORT_0_RD_COUNTER_CHANNEL_0_REG

#define CMI_RESPOND_PORT_0_RD_COUNTER_CHANNEL_2_REG                    (0x00012190)
//Duplicate of CMI_0_RESPOND_PORT_0_RD_COUNTER_CHANNEL_0_REG

#define CMI_RESPOND_PORT_0_RD_COUNTER_CHANNEL_3_REG                    (0x00012198)
//Duplicate of CMI_0_RESPOND_PORT_0_RD_COUNTER_CHANNEL_0_REG

#define CMI_RESPOND_PORT_0_WR_COUNTER_CHANNEL_0_REG                    (0x000121A0)
//Duplicate of CMI_0_RESPOND_PORT_0_WR_COUNTER_CHANNEL_0_REG

#define CMI_RESPOND_PORT_0_WR_COUNTER_CHANNEL_1_REG                    (0x000121A8)
//Duplicate of CMI_0_RESPOND_PORT_0_WR_COUNTER_CHANNEL_0_REG

#define CMI_RESPOND_PORT_0_WR_COUNTER_CHANNEL_2_REG                    (0x000121B0)
//Duplicate of CMI_0_RESPOND_PORT_0_WR_COUNTER_CHANNEL_0_REG

#define CMI_RESPOND_PORT_0_WR_COUNTER_CHANNEL_3_REG                    (0x000121B8)
//Duplicate of CMI_0_RESPOND_PORT_0_WR_COUNTER_CHANNEL_0_REG

#define CMI_RESPOND_PORT_1_RD_COUNTER_CHANNEL_0_REG                    (0x000121C0)
//Duplicate of CMI_0_RESPOND_PORT_0_RD_COUNTER_CHANNEL_0_REG

#define CMI_RESPOND_PORT_1_RD_COUNTER_CHANNEL_1_REG                    (0x000121C8)
//Duplicate of CMI_0_RESPOND_PORT_0_RD_COUNTER_CHANNEL_0_REG

#define CMI_RESPOND_PORT_1_RD_COUNTER_CHANNEL_2_REG                    (0x000121D0)
//Duplicate of CMI_0_RESPOND_PORT_0_RD_COUNTER_CHANNEL_0_REG

#define CMI_RESPOND_PORT_1_RD_COUNTER_CHANNEL_3_REG                    (0x000121D8)
//Duplicate of CMI_0_RESPOND_PORT_0_RD_COUNTER_CHANNEL_0_REG

#define CMI_RESPOND_PORT_1_WR_COUNTER_CHANNEL_0_REG                    (0x000121E0)
//Duplicate of CMI_0_RESPOND_PORT_0_WR_COUNTER_CHANNEL_0_REG

#define CMI_RESPOND_PORT_1_WR_COUNTER_CHANNEL_1_REG                    (0x000121E8)
//Duplicate of CMI_0_RESPOND_PORT_0_WR_COUNTER_CHANNEL_0_REG

#define CMI_RESPOND_PORT_1_WR_COUNTER_CHANNEL_2_REG                    (0x000121F0)
//Duplicate of CMI_0_RESPOND_PORT_0_WR_COUNTER_CHANNEL_0_REG

#define CMI_RESPOND_PORT_1_WR_COUNTER_CHANNEL_3_REG                    (0x000121F8)
//Duplicate of CMI_0_RESPOND_PORT_0_WR_COUNTER_CHANNEL_0_REG

#define CMI_RESPOND_PORT_2_RD_COUNTER_CHANNEL_0_REG                    (0x00012200)
//Duplicate of CMI_0_RESPOND_PORT_0_RD_COUNTER_CHANNEL_0_REG

#define CMI_RESPOND_PORT_2_RD_COUNTER_CHANNEL_1_REG                    (0x00012208)
//Duplicate of CMI_0_RESPOND_PORT_0_RD_COUNTER_CHANNEL_0_REG

#define CMI_RESPOND_PORT_2_RD_COUNTER_CHANNEL_2_REG                    (0x00012210)
//Duplicate of CMI_0_RESPOND_PORT_0_RD_COUNTER_CHANNEL_0_REG

#define CMI_RESPOND_PORT_2_RD_COUNTER_CHANNEL_3_REG                    (0x00012218)
//Duplicate of CMI_0_RESPOND_PORT_0_RD_COUNTER_CHANNEL_0_REG

#define CMI_RESPOND_PORT_2_WR_COUNTER_CHANNEL_0_REG                    (0x00012220)
//Duplicate of CMI_0_RESPOND_PORT_0_WR_COUNTER_CHANNEL_0_REG

#define CMI_RESPOND_PORT_2_WR_COUNTER_CHANNEL_1_REG                    (0x00012228)
//Duplicate of CMI_0_RESPOND_PORT_0_WR_COUNTER_CHANNEL_0_REG

#define CMI_RESPOND_PORT_2_WR_COUNTER_CHANNEL_2_REG                    (0x00012230)
//Duplicate of CMI_0_RESPOND_PORT_0_WR_COUNTER_CHANNEL_0_REG

#define CMI_RESPOND_PORT_2_WR_COUNTER_CHANNEL_3_REG                    (0x00012238)
//Duplicate of CMI_0_RESPOND_PORT_0_WR_COUNTER_CHANNEL_0_REG

#define CMI_RESPOND_PORT_3_RD_COUNTER_CHANNEL_0_REG                    (0x00012240)
//Duplicate of CMI_0_RESPOND_PORT_0_RD_COUNTER_CHANNEL_0_REG

#define CMI_RESPOND_PORT_3_RD_COUNTER_CHANNEL_1_REG                    (0x00012248)
//Duplicate of CMI_0_RESPOND_PORT_0_RD_COUNTER_CHANNEL_0_REG

#define CMI_RESPOND_PORT_3_RD_COUNTER_CHANNEL_2_REG                    (0x00012250)
//Duplicate of CMI_0_RESPOND_PORT_0_RD_COUNTER_CHANNEL_0_REG

#define CMI_RESPOND_PORT_3_RD_COUNTER_CHANNEL_3_REG                    (0x00012258)
//Duplicate of CMI_0_RESPOND_PORT_0_RD_COUNTER_CHANNEL_0_REG

#define CMI_RESPOND_PORT_3_WR_COUNTER_CHANNEL_0_REG                    (0x00012260)
//Duplicate of CMI_0_RESPOND_PORT_0_WR_COUNTER_CHANNEL_0_REG

#define CMI_RESPOND_PORT_3_WR_COUNTER_CHANNEL_1_REG                    (0x00012268)
//Duplicate of CMI_0_RESPOND_PORT_0_WR_COUNTER_CHANNEL_0_REG

#define CMI_RESPOND_PORT_3_WR_COUNTER_CHANNEL_2_REG                    (0x00012270)
//Duplicate of CMI_0_RESPOND_PORT_0_WR_COUNTER_CHANNEL_0_REG

#define CMI_RESPOND_PORT_3_WR_COUNTER_CHANNEL_3_REG                    (0x00012278)
//Duplicate of CMI_0_RESPOND_PORT_0_WR_COUNTER_CHANNEL_0_REG

#define CMI_REQ_L3_ARB_CONFIG_REG                                      (0x00012280)
//Duplicate of CMI_0_REQ_L3_ARB_CONFIG_REG

#define CMI_PARITY_CONTROL_REG                                         (0x00012284)
//Duplicate of CMI_0_PARITY_CONTROL_REG

#define CMI_PARITY_ERR_LOG_REG                                         (0x00012288)
//Duplicate of CMI_0_PARITY_ERR_LOG_REG

#define CMI_PARITY_ERR_INJ_REG                                         (0x00012290)
//Duplicate of CMI_0_PARITY_ERR_INJ_REG

#define CMI_REQUEST_PORT_0_RESERVED_0_REG                              (0x00012320)
//Duplicate of CMI_0_REQUEST_PORT_0_RESERVED_0_REG

#define CMI_REQUEST_PORT_0_RESERVED_1_REG                              (0x00012324)
//Duplicate of CMI_0_REQUEST_PORT_0_RESERVED_0_REG

#define CMI_REQUEST_PORT_0_RESERVED_2_REG                              (0x00012328)
//Duplicate of CMI_0_REQUEST_PORT_0_RESERVED_0_REG

#define CMI_REQUEST_PORT_0_REQ_IN_DFX_INSTR_DBG_REG                    (0x0001232C)
//Duplicate of CMI_0_REQUEST_PORT_0_REQ_IN_DFX_INSTR_DBG_REG

#define CMI_REQUEST_PORT_0_RD_CPL_OUT_DFX_INSTR_DBG_REG                (0x00012330)
//Duplicate of CMI_0_REQUEST_PORT_0_REQ_IN_DFX_INSTR_DBG_REG

#define CMI_REQUEST_PORT_0_RSP_OUT_DFX_INSTR_DBG_REG                   (0x00012334)
//Duplicate of CMI_0_REQUEST_PORT_0_REQ_IN_DFX_INSTR_DBG_REG

#define CMI_REQUEST_PORT_1_RESERVED_0_REG                              (0x00012338)
//Duplicate of CMI_0_REQUEST_PORT_0_RESERVED_0_REG

#define CMI_REQUEST_PORT_1_RESERVED_1_REG                              (0x0001233C)
//Duplicate of CMI_0_REQUEST_PORT_0_RESERVED_0_REG

#define CMI_REQUEST_PORT_1_RESERVED_2_REG                              (0x00012340)
//Duplicate of CMI_0_REQUEST_PORT_0_RESERVED_0_REG

#define CMI_REQUEST_PORT_1_REQ_IN_DFX_INSTR_DBG_REG                    (0x00012344)
//Duplicate of CMI_0_REQUEST_PORT_0_REQ_IN_DFX_INSTR_DBG_REG

#define CMI_REQUEST_PORT_1_RD_CPL_OUT_DFX_INSTR_DBG_REG                (0x00012348)
//Duplicate of CMI_0_REQUEST_PORT_0_REQ_IN_DFX_INSTR_DBG_REG

#define CMI_REQUEST_PORT_1_RSP_OUT_DFX_INSTR_DBG_REG                   (0x0001234C)
//Duplicate of CMI_0_REQUEST_PORT_0_REQ_IN_DFX_INSTR_DBG_REG

#define CMI_REQUEST_PORT_2_RESERVED_0_REG                              (0x00012350)
//Duplicate of CMI_0_REQUEST_PORT_0_RESERVED_0_REG

#define CMI_REQUEST_PORT_2_RESERVED_1_REG                              (0x00012354)
//Duplicate of CMI_0_REQUEST_PORT_0_RESERVED_0_REG

#define CMI_REQUEST_PORT_2_RESERVED_2_REG                              (0x00012358)
//Duplicate of CMI_0_REQUEST_PORT_0_RESERVED_0_REG

#define CMI_REQUEST_PORT_2_REQ_IN_DFX_INSTR_DBG_REG                    (0x0001235C)
//Duplicate of CMI_0_REQUEST_PORT_0_REQ_IN_DFX_INSTR_DBG_REG

#define CMI_REQUEST_PORT_2_RD_CPL_OUT_DFX_INSTR_DBG_REG                (0x00012360)
//Duplicate of CMI_0_REQUEST_PORT_0_REQ_IN_DFX_INSTR_DBG_REG

#define CMI_REQUEST_PORT_2_RSP_OUT_DFX_INSTR_DBG_REG                   (0x00012364)
//Duplicate of CMI_0_REQUEST_PORT_0_REQ_IN_DFX_INSTR_DBG_REG

#define CMI_REQUEST_PORT_3_RESERVED_0_REG                              (0x00012368)
//Duplicate of CMI_0_REQUEST_PORT_0_RESERVED_0_REG

#define CMI_REQUEST_PORT_3_RESERVED_1_REG                              (0x0001236C)
//Duplicate of CMI_0_REQUEST_PORT_0_RESERVED_0_REG

#define CMI_REQUEST_PORT_3_RESERVED_2_REG                              (0x00012370)
//Duplicate of CMI_0_REQUEST_PORT_0_RESERVED_0_REG

#define CMI_REQUEST_PORT_3_REQ_IN_DFX_INSTR_DBG_REG                    (0x00012374)
//Duplicate of CMI_0_REQUEST_PORT_0_REQ_IN_DFX_INSTR_DBG_REG

#define CMI_REQUEST_PORT_3_RD_CPL_OUT_DFX_INSTR_DBG_REG                (0x00012378)
//Duplicate of CMI_0_REQUEST_PORT_0_REQ_IN_DFX_INSTR_DBG_REG

#define CMI_REQUEST_PORT_3_RSP_OUT_DFX_INSTR_DBG_REG                   (0x0001237C)
//Duplicate of CMI_0_REQUEST_PORT_0_REQ_IN_DFX_INSTR_DBG_REG

#define CMI_RESPOND_PORT_0_RESERVED_0_REG                              (0x00012380)
//Duplicate of CMI_0_REQUEST_PORT_0_RESERVED_0_REG

#define CMI_RESPOND_PORT_0_RESERVED_1_REG                              (0x00012384)
//Duplicate of CMI_0_REQUEST_PORT_0_RESERVED_0_REG

#define CMI_RESPOND_PORT_0_RESERVED_2_REG                              (0x00012388)
//Duplicate of CMI_0_REQUEST_PORT_0_RESERVED_0_REG

#define CMI_RESPOND_PORT_0_REQ_OUT_DFX_INSTR_DBG_REG                   (0x0001238C)
//Duplicate of CMI_0_REQUEST_PORT_0_REQ_IN_DFX_INSTR_DBG_REG

#define CMI_RESPOND_PORT_0_RD_CPL_IN_DFX_INSTR_DBG_REG                 (0x00012390)
//Duplicate of CMI_0_REQUEST_PORT_0_REQ_IN_DFX_INSTR_DBG_REG

#define CMI_RESPOND_PORT_0_RSP_IN_DFX_INSTR_DBG_REG                    (0x00012394)
//Duplicate of CMI_0_REQUEST_PORT_0_REQ_IN_DFX_INSTR_DBG_REG

#define CMI_RESPOND_PORT_1_RESERVED_0_REG                              (0x00012398)
//Duplicate of CMI_0_REQUEST_PORT_0_RESERVED_0_REG

#define CMI_RESPOND_PORT_1_RESERVED_1_REG                              (0x0001239C)
//Duplicate of CMI_0_REQUEST_PORT_0_RESERVED_0_REG

#define CMI_RESPOND_PORT_1_RESERVED_2_REG                              (0x000123A0)
//Duplicate of CMI_0_REQUEST_PORT_0_RESERVED_0_REG

#define CMI_RESPOND_PORT_1_REQ_OUT_DFX_INSTR_DBG_REG                   (0x000123A4)
//Duplicate of CMI_0_REQUEST_PORT_0_REQ_IN_DFX_INSTR_DBG_REG

#define CMI_RESPOND_PORT_1_RD_CPL_IN_DFX_INSTR_DBG_REG                 (0x000123A8)
//Duplicate of CMI_0_REQUEST_PORT_0_REQ_IN_DFX_INSTR_DBG_REG

#define CMI_RESPOND_PORT_1_RSP_IN_DFX_INSTR_DBG_REG                    (0x000123AC)
//Duplicate of CMI_0_REQUEST_PORT_0_REQ_IN_DFX_INSTR_DBG_REG

#define CMI_RESPOND_PORT_2_RESERVED_0_REG                              (0x000123B0)
//Duplicate of CMI_0_REQUEST_PORT_0_RESERVED_0_REG

#define CMI_RESPOND_PORT_2_RESERVED_1_REG                              (0x000123B4)
//Duplicate of CMI_0_REQUEST_PORT_0_RESERVED_0_REG

#define CMI_RESPOND_PORT_2_RESERVED_2_REG                              (0x000123B8)
//Duplicate of CMI_0_REQUEST_PORT_0_RESERVED_0_REG

#define CMI_RESPOND_PORT_2_REQ_OUT_DFX_INSTR_DBG_REG                   (0x000123BC)
//Duplicate of CMI_0_REQUEST_PORT_0_REQ_IN_DFX_INSTR_DBG_REG

#define CMI_RESPOND_PORT_2_RD_CPL_IN_DFX_INSTR_DBG_REG                 (0x000123C0)
//Duplicate of CMI_0_REQUEST_PORT_0_REQ_IN_DFX_INSTR_DBG_REG

#define CMI_RESPOND_PORT_2_RSP_IN_DFX_INSTR_DBG_REG                    (0x000123C4)
//Duplicate of CMI_0_REQUEST_PORT_0_REQ_IN_DFX_INSTR_DBG_REG

#define CMI_RESPOND_PORT_3_RESERVED_0_REG                              (0x000123C8)
//Duplicate of CMI_0_REQUEST_PORT_0_RESERVED_0_REG

#define CMI_RESPOND_PORT_3_RESERVED_1_REG                              (0x000123CC)
//Duplicate of CMI_0_REQUEST_PORT_0_RESERVED_0_REG

#define CMI_RESPOND_PORT_3_RESERVED_2_REG                              (0x000123D0)
//Duplicate of CMI_0_REQUEST_PORT_0_RESERVED_0_REG

#define CMI_RESPOND_PORT_3_REQ_OUT_DFX_INSTR_DBG_REG                   (0x000123D4)
//Duplicate of CMI_0_REQUEST_PORT_0_REQ_IN_DFX_INSTR_DBG_REG

#define CMI_RESPOND_PORT_3_RD_CPL_IN_DFX_INSTR_DBG_REG                 (0x000123D8)
//Duplicate of CMI_0_REQUEST_PORT_0_REQ_IN_DFX_INSTR_DBG_REG

#define CMI_RESPOND_PORT_3_RSP_IN_DFX_INSTR_DBG_REG                    (0x000123DC)
//Duplicate of CMI_0_REQUEST_PORT_0_REQ_IN_DFX_INSTR_DBG_REG

#define CMI_REQUEST_PORT_0_REQ_DATA_IN_DFX_INSTR_DBG_REG               (0x00012420)
//Duplicate of CMI_0_REQUEST_PORT_0_REQ_IN_DFX_INSTR_DBG_REG

#define CMI_REQUEST_PORT_0_RD_CPL_DATA_OUT_DFX_INSTR_DBG_REG           (0x00012424)
//Duplicate of CMI_0_REQUEST_PORT_0_REQ_IN_DFX_INSTR_DBG_REG

#define CMI_REQUEST_PORT_1_REQ_DATA_IN_DFX_INSTR_DBG_REG               (0x00012428)
//Duplicate of CMI_0_REQUEST_PORT_0_REQ_IN_DFX_INSTR_DBG_REG

#define CMI_REQUEST_PORT_1_RD_CPL_DATA_OUT_DFX_INSTR_DBG_REG           (0x0001242C)
//Duplicate of CMI_0_REQUEST_PORT_0_REQ_IN_DFX_INSTR_DBG_REG

#define CMI_REQUEST_PORT_2_REQ_DATA_IN_DFX_INSTR_DBG_REG               (0x00012430)
//Duplicate of CMI_0_REQUEST_PORT_0_REQ_IN_DFX_INSTR_DBG_REG

#define CMI_REQUEST_PORT_2_RD_CPL_DATA_OUT_DFX_INSTR_DBG_REG           (0x00012434)
//Duplicate of CMI_0_REQUEST_PORT_0_REQ_IN_DFX_INSTR_DBG_REG

#define CMI_REQUEST_PORT_3_REQ_DATA_IN_DFX_INSTR_DBG_REG               (0x00012438)
//Duplicate of CMI_0_REQUEST_PORT_0_REQ_IN_DFX_INSTR_DBG_REG

#define CMI_REQUEST_PORT_3_RD_CPL_DATA_OUT_DFX_INSTR_DBG_REG           (0x0001243C)
//Duplicate of CMI_0_REQUEST_PORT_0_REQ_IN_DFX_INSTR_DBG_REG

#define CMI_RESPOND_PORT_0_REQ_DATA_OUT_DFX_INSTR_DBG_REG              (0x00012440)
//Duplicate of CMI_0_REQUEST_PORT_0_REQ_IN_DFX_INSTR_DBG_REG

#define CMI_RESPOND_PORT_0_RD_CPL_DATA_IN_DFX_INSTR_DBG_REG            (0x00012444)
//Duplicate of CMI_0_REQUEST_PORT_0_REQ_IN_DFX_INSTR_DBG_REG

#define CMI_RESPOND_PORT_1_REQ_DATA_OUT_DFX_INSTR_DBG_REG              (0x00012448)
//Duplicate of CMI_0_REQUEST_PORT_0_REQ_IN_DFX_INSTR_DBG_REG

#define CMI_RESPOND_PORT_1_RD_CPL_DATA_IN_DFX_INSTR_DBG_REG            (0x0001244C)
//Duplicate of CMI_0_REQUEST_PORT_0_REQ_IN_DFX_INSTR_DBG_REG

#define CMI_RESPOND_PORT_2_REQ_DATA_OUT_DFX_INSTR_DBG_REG              (0x00012450)
//Duplicate of CMI_0_REQUEST_PORT_0_REQ_IN_DFX_INSTR_DBG_REG

#define CMI_RESPOND_PORT_2_RD_CPL_DATA_IN_DFX_INSTR_DBG_REG            (0x00012454)
//Duplicate of CMI_0_REQUEST_PORT_0_REQ_IN_DFX_INSTR_DBG_REG

#define CMI_RESPOND_PORT_3_REQ_DATA_OUT_DFX_INSTR_DBG_REG              (0x00012458)
//Duplicate of CMI_0_REQUEST_PORT_0_REQ_IN_DFX_INSTR_DBG_REG

#define CMI_RESPOND_PORT_3_RD_CPL_DATA_IN_DFX_INSTR_DBG_REG            (0x0001245C)
//Duplicate of CMI_0_REQUEST_PORT_0_REQ_IN_DFX_INSTR_DBG_REG

#define CMI_DFX_OBSERVE_REG                                            (0x00012460)
//Duplicate of CMI_0_DFX_OBSERVE_REG

#define CMF_GLOBAL_STATUS_REG                                          (0x00012468)
//Duplicate of CMF_0_GLOBAL_STATUS_REG

#define CMF_GLOBAL_CFG_REG                                             (0x0001246C)
//Duplicate of CMF_0_GLOBAL_CFG_REG
#pragma pack(pop)
#endif
