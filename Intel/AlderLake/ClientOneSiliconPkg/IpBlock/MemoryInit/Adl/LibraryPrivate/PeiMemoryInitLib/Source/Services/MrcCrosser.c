/** @file
  These functions implement the crosser training algorithm.

@copyright
  INTEL CONFIDENTIAL
  Copyright 1999 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains an 'Intel Peripheral Driver' and is uniquely identified as
  "Intel Reference Module" and is licensed for Intel CPUs and chipsets under
  the terms of your license agreement with Intel or your vendor. This file may
  be modified by the user, subject to additional terms of the license agreement.

@par Specification Reference:
**/
// Include files
#include "MrcCrosser.h"
#include "MrcWriteDqDqs.h"
#include "MrcReadDqDqs.h"
#include "MrcCommon.h"
#include "MrcMcConfiguration.h"
#include "MrcCpgcApi.h"
#include "MrcChipApi.h"
#include "MrcMemoryApi.h"
#include "MrcLpddr4.h"
#include "MrcDdr5.h"
#include "MrcDdr4.h"
#include "MrcMemoryApi.h"
#include "MrcCpgcOffsets.h"
#include "Cpgc20TestCtl.h"
#include "MrcDdrIoOffsets.h"
#include "MrcPpr.h"

#if SUPPORT_SODIMM == SUPPORT
#include "MrcSpdProcessing.h"
#endif //SUPPORT_SODIMM == SUPPORT


#if (defined (MRC_POWER_TRAINING_DEBUG) && (MRC_POWER_TRAINING_DEBUG == SUPPORT))
#define MRC_PWR_DBG_PRINT (TRUE)
#else
#define MRC_PWR_DBG_PRINT (FALSE)
#endif

/// Module Definitions
#define DIMM_ODT_DIMM_MASK_SHIFT  (4)

/// Power optimization loop count
#define OPT_PARAM_LOOP_COUNT (15)

/// Power optimization re-centering loop count
#define RECENTER_LOOP_COUNT  (10)

/// Power optimization re-centering step size
#define T_RECENTERING_STEP_SIZE (2)
#define V_RECENTERING_STEP_SIZE (3)

/// Power optimization final centering step size
#define V_FINAL_STEP_SIZE (2)

/// Power optimization cmd timing margining step size
#define CMD_T_MARGIN_STEP_SIZE (3)

/// UPM/PWR increment value if margins are at or below the retrain limit.
#define MRC_UPM_PWR_INC_VAL (80)

/// Vswing value = Vindiff_Dqs/2 * 1.1 (Guardband) = 216mV
#define VSWING (216)

/// Zo
#define Z_O (40)
/// Vref step size for the following: DqOdt, DqDrv, CmdDrv, CtlDrv, ClkDrv
#define MRC_COMP_VREF_STEP_SIZE   (192)

/// Number of VTT regs
#define MAX_VTT_REGS (4)

/// SenseAmplifier Wake-Up Time in pS
#define MRC_SENSE_AMP_WAKEUP_TIME (2000)

/// SOT Spacing Macro
///   GroupIdx | Spaces | Trained
///   ---------|--------|-------------
///      0     |   9    | 8 DQs, 1 DQS
///      1     |   8    | 8 DQs
///      2     |   1    | 1 DQS
#define MRC_SOT_BIT_SPACE(x) (((x) == 0) ? ("         ") : (((x) == 1) ? ("        ") : (" ")))

/// We need to make one of the bits in the combined Scomp value the PC bit value. It can be any bit as long as we always use the same one everywhere, and don't overwrite any bits used to store the SComp values.
/// The combined Scomp value is split back into individual PC and Scomp values before being written to registers using the get/set methods.
#define SCOMP_PC_STORAGE_BIT_OFFSET (4)

// Param step sizes. IF YOU CHANGE THESE, YOU MUST ADJUST THE SWEEP RANGE TABLE TO MATCH!!
#define DQ_TCO_COMP_STEP (4)
#define DQS_TCO_COMP_STEP (7)
#define CLK_TCO_COMP_STEP (2)
#define VDDQ_STEP (4)
#define WR_DS_STEP (2)
#define WR_TXEQ_STEP (1)
#define WR_DS_COARSE_STEP (4)
#define CCC_DS_STEP (2)
#define CCC_TXEQ_STEP (2)
#define CCC_DS_COARSE_STEP (3)
#define RXDQ_ODT_STEP (3)
#define RXDQS_ODT_STEP (3)
#define RXEQ_STEP (2)
#define DFE_STEP_PER_CHANNEL (3)
#define DFE_STEP (1)
#define RX_LOAD_STEP (2)
#define RX_BIAS_STEP (2)
#define RX_CB_DATA_STEP (2)
#define RX_CB_COMP_STEP (2)

// Adjustments to line up 0 ticks with 0 volts
#define VDDQ_BASE_TICKS (0) // Supposedly zero, documentation does not say for certain.
#define RXVREF_BASE_TICKS (0) // Supposedly zero, documentation does not say for certain. 0-381. Step size is Vddxx/382
#define DDR4_CMDVREF_BASE_TICKS (0) // Supposedly zero, documentation does not say for certain. 0-381. Step size is Vddxx/382
#define DDR4_TXVREF_BASE_TICKS (69) // 45% at 0.65% per tick
#define LP4_CMDVREF_BASE_TICKS (25) // 15% at 0.6% per tick
#define LP4_TXVREF_BASE_TICKS (25) // 15% at 0.6% per tick
// TGL_POWER_TRAINING_VDDQ - Update for DDR5. Also check if DDR5 ODT types, value ranges, and RZQ value has changed at all.
//#define DDR5_CMDVREF_BASE_TICKS (?)
//#define DDR5_TXVREF_BASE_TICKS (?)
// TGL_POWER_TRAINING_VDDQ - Update for LPDDR5 depending on manufacturer spec
#define LP5_CMDVREF_BASE_TICKS (20) // Samsung has 15% at 0.5% per tick, JEDEC spec has 10%
#define LP5_TXVREF_BASE_TICKS (20) // Samsung has 15% at 0.5% per tick, JEDEC spec has 10%

// Slew rate constants
#define SLEW_RATE_ENABLED (0)
#define CYCLE_LOCK (1)
// Number of read-write command margin failures.
#define NUM_RW_CMD_MARGIN_FAILURES (3)

// CCC margins are twice as large.
#define MAX_CCC_POSSIBLE_TIME (MAX_POSSIBLE_TIME * 2)

// 8 values can be set in each decap register
#define MAX_DECAP_VALUES (8)

// Max CCC TxEq limit
#define CCC_TXEQ_MAX (12)

// Max TxEq Limit
#define TXEQ_MAX (7)

// Min LPDDR CCC TxEq limit
#define LP_CCC_TXEQ_LOW_LIMIT (8)

// Up and Down
#define MAX_COMP_DIRECTIONS (2)
#define COMP_UP (0)
#define COMP_DN (1)

// The max number of comps/comp vrefs we ever optimize at once
#define MAX_COMPS_OPTIMIZED (3)

// The max number of comp offsets we ever optimize at once
#define MAX_COMP_OFFSETS_OPTIMIZED (4)

// The array index in which we store the name of the optimized comp vref during comp optimization
#define OPTIMIZED_COMP_VREF_INDEX (0)
// The max number of comps we ever check for saturation at once
#define MAX_COMPS_CHECKED (3)

// The number of comp codes we reserve to prevent saturation during training
#define RESERVED_COMP_CODES (3)
// Segment numbers for comp vrefs
#define ONE_SEGMENT  (1)
#define TWO_SEGMENTS (2)
#define THREE_SEGMENTS (3)

// Enable FIVR power Measuremnt
#define FIVR_POWER_MEASUREMENT_ENABLED (0)

// Right now, we can only run 1D and 2D optimizations
#define MAX_OPT_PARAM_LENGTH (2)

// 2 places of decimal accuracy
#define TWO_DECIMAL_PLACES (100)

// Scale for linear normalized power
#define LINEAR_NORMALIZED_POWER_SCALE (1000)

// Margins are multiplied by this when returned from the margining functions
#define MARGIN_MULTIPLIER (10)

// Less than this amount of margin is considered a failure
#define MARGIN_FAIL (2 * (MARGIN_MULTIPLIER))

#define VOC_RCOMP_MAX             63 //rcomp odt max value
#define VOC_VREF_MAX              511 //max rxvref value
#define VOC_VREF_STEP_SIZE        1 // can be tuned
#define VOC_MAX                   31 //max voc code
#define VOC_MID                   ((VOC_MAX+1) /2) //middle voc code
#define VOC_KNOB_COUNT            2 ///voc knob count. 0 for VocP, 1 for VocN
#define EARLY_DIMM_DFE_LOOP_COUNT 8
#define DIMM_DFE_LOOP_COUNT       17
/// Module Globals - TGL_POWER_TRAINING - Fill this in with actual numbers for everything.
/// Retraining limits are per eye-side. UPM/PWR limits are total eye width/height. Both are in offset register ticks, not raw register ticks!
GLOBAL_REMOVE_IF_UNREFERENCED const MrcUpmPwrRetrainLimits  InitialLimits[MRC_NUMBER_UPM_PWR_RETRAIN_MARGINS] = {
  //           UPM,          PWR       Retrain
  {RdT,       {300,          1280,     90 }},
  {WrT,       {300,          1280,     90 }},
  {RdV,       {400,          1280,     160}},
  // For ULT DDR3L rcF the values are increased by 20 ticks, see MrcGetUpmPwrLimit()
  {WrV,       {600,          1280,     160}},
  {RdFan2,    {240,          1280,       0}},
  {WrFan2,    {240,          1280,       0}},
  {RdFan3,    {(240*4)/5,    1280,       0}},
  {WrFan3,    {(240*4)/5,    1280,       0}},
  // {650ps,750ps} * 64 pi ticks * 2 (for width) = 134 PI Ticks ->  ~1.3nsec for UPM, 154 PI Ticks -> ~1.5nsec for PWR
  // Margin function works in steps of 4, so we divide the margin by 4.
  // Margin numbers are scaled by 10.
  {RcvEnaX,   {((134*10)/4), 1280,       0}},
  {CmdT,       {600,         1280,     180}}, // CCC retrain limit is around twice what data is, as margins are +-64 instead of +-32 (Data is DDR, CLK is not)
  {CmdV,       {800,         1280,     160}}
};

/// Taken from ADL RMT guidelines for max frequency
GLOBAL_REMOVE_IF_UNREFERENCED  const MrcAdlRMTBaseLine  AdlDDR4RMTBaseLine[MAX_MARGINS_TRADEOFF] =
{//Test, Ticks PerSide, UPM PerSide
  {RdT,       10,        49 },
  {WrT,       10,        49 },
  {RdV,       22,        52 },
  {WrV,        8,        62 }
};

GLOBAL_REMOVE_IF_UNREFERENCED  const MrcAdlRMTBaseLine  AdlLPDDR5RMTBaseLine[MAX_MARGINS_TRADEOFF] =
{//Test, Ticks PerSide, UPM PerSide
  {RdT,       13,        39 },
  {WrT,       13,        39 },
  {RdV,       25,        51 },
  {WrV,       26,        65 }
};

GLOBAL_REMOVE_IF_UNREFERENCED  const MrcAdlRMTBaseLine  AdlDDR5RMTBaseLine[MAX_MARGINS_TRADEOFF] =
{//Test, Ticks PerSide, UPM PerSide
  {RdT,       12,        39 },
  {WrT,       12,        39 },
  {RdV,       23,        49 },
  {WrV,        9,        50 }
};

GLOBAL_REMOVE_IF_UNREFERENCED const MrcOptParamsLimits  MrcOptParamLimit[MRC_NUMBER_OPT_PARAMS_TRAIN] = {
  //            Normal/SAGV(high), SAGV (low), DT/Halo (max perf.)
  // Use full range for now, optimize postsilicon
  {OptRdDqOdt,        {-8,    8}, {-8,    8}, {-8,    8}}, // This is a comp offset on a side effect comp vref, so needs at least 3 codes of guardband to account for comp offset drift during comp offset optimization
  {OptRdDqsOdt,       {-5,    5}, {-5,    5}, {-5,    5}},
  {OptRxBias,         {0,     7}, {0,     7}, {0,     7}},
  {OptRxBiasVrefSel,  {0,     7}, {0,     7}, {0,     7}},
  {OptRxBiasTailCtl,  {0,     3}, {0,     3}, {0,     3}},
  {OptRxEq,           {0,     7}, {0,     7}, {0,     7}},
  {OptTxEqCoeff0,     {0,     3}, {0,     3}, {0,     3}},
  {OptTxEqCoeff1,     {0,     3}, {0,     3}, {0,     3}},
  {OptTxEqCoeff2,     {0,     3}, {0,     3}, {0,     3}},
  {OptDimmDFETap1,    {-40,  10}, {-40,  10}, {-40,  10}},
  {OptDimmDFETap2,    {-13,  13}, {-13,  13}, {-13,  13}},
  {OptDimmDFETap3,    {-12,  12}, {-12,  12}, {-12,  12}},
  {OptDimmDFETap4,    {-9,    9}, {-9,    9}, {-9,    9}},
  {OptWrDS,           {-6,    6}, {-6,    6}, {-6,    6}},
  {OptWrDSDnCoarse,   {-7,    7}, {-7,    7}, {-7,    7}}, // This is a comp offset on a side effect comp vref, so needs at least 3 codes of guardband to account for comp offset drift during comp offset optimization
  {OptWrDSUpCoarse,   {-8,    7}, {-8,    7}, {-8,    7}},
  {OptSComp,          {-16,  15}, {-16,  15}, {-16,  15}},
  {OptCCCTco,         {0,    31}, {0,    31}, {0,    31}},
  {OptTxDqTco,        {0,     9}, {0,     9}, {0,     9}},
  {OptTxDqsPTco,       {0,    10}, {0,    10}, {0,    10}},
  {OptVddq,           {0,    30}, {0,    30}, {0,    30}},
  {OptCCCTxEq,        {0,     6}, {0,     6}, {0,     6}},
  {OptCCCDS,          {-4,    3}, {-4,    3}, {-4,    3}},
  {OptCCCDSDnCoarse,  {-1,    1}, {-1,    1}, {-1,    1}}, // This is a comp offset on a side effect comp vref, so needs at least 3 codes of guardband to account for comp offset drift during comp offset optimization
  {OptCCCDSUpCoarse,  {-2,    2}, {-2,    2}, {-2,    2}},
  {OptCCCSComp,       {-8,    7}, {-8,    7}, {-8,    7}},
  {OptVccDLLBypass,   {0,     1}, {0,     1}, {0,     1}},
  {OptRxVrefVttDecap, {0,     7}, {0,     7}, {0,     7}},
  {OptRxVrefVddqDecap,{0,     7}, {0,     7}, {0,     7}},
  {OptDFETap1,        {0,     5}, {0,     5}, {0,     5}},
  {OptDFETap2,        {0,     5}, {0,     5}, {0,     5}},
  {OptRxC,            {0,     3}, {0,     3}, {0,     3}},
  {OptRxR,            {0,     3}, {0,     3}, {0,     3}},
  {OptPanicVttDnLp,   {0,    15}, {0,    15}, {0,    15}}
};

UINT8 LPFARR55[5][5] = {
  { 1, 1, 1, 1, 1 },
  { 1, 2, 3, 2, 1 },
  { 1, 3, 9, 3, 1 },
  { 1, 2, 3, 2, 1 },
  { 1, 1, 1, 1, 1 }
};

// DDR Params encoding
#ifdef MRC_DEBUG_PRINT
GLOBAL_REMOVE_IF_UNREFERENCED const char  *TOptParamOffsetString[] = {
  "WrDS",
  "Vddq",
  "SComp",
  "TxDqTCoComp",
  "TxDqsPTCoComp",
  "TxDqsNTCoComp",
  "CCCTCoComp",
  "TxTCoCompoff",
  "TxEq",
  "TxEqCoeff0",
  "TxEqCoeff1",
  "TxEqCoeff2",
  "RdOdt",
  "RdDqOdt",
  "RdDqsOdt",
  "RxEq",
  "RxBias",
  "RxLoad",
  "RxCbData",
  "RxBiasVrefSel",
  "RxBiasTailCtl",
  "DimmOdt",
  "DimmOdtWr",
  "DimmOdtNom",
  "DimmOdtNomNT",
  "DimmOdtNomWr",
  "DimmOdtNomRd",
  "OptOdtPark",
  "OptOdtParkNT",
  "OptDimmOdtParkDqs",
  "OptDimmOdtComb",
  "DimmOdtCa",
  "DimmRon",
  "DimmRonUp",
  "DimmRonDn",
  "DimmDFETap1",
  "DimmDFETap2",
  "DimmDFETap3",
  "DimmDFETap4",
  "WrDSUp",
  "WrDSDn",
  "WrDSUpCoarse",
  "WrDSDnCoarse",
  "RdOdtUp",
  "RdOdtDn",
  "CCCDS",
  "CCCDSUp",
  "CCCDSDn",
  "CmdDS",
  "CtlDS",
  "ClkDS",
  "RxBiasCb",
  "TxEqWrDS",
  "DimmSocOdt",
  "CCCTxEq",
  "CCCDSDnCoarse",
  "CCCDSUpCoarse",
  "CmdDSDnCoarse",
  "CmdDSUpCoarse",
  "CtlDSDnCoarse",
  "CtlDSUpCoarse",
  "ClkDSDnCoarse",
  "ClkDSUpCoarse",
  "CCCTxEqCCCDS",
  "CCCSComp",
  "CmdSComp",
  "CtlSComp",
  "ClkSComp",
  "VccDLLBypass",
  "RxVrefVttDecap",
  "RxVrefVddqDecap",
  "DFETap1",
  "DFETap2",
  "DFETap3",
  "DFETap4",
  "RxC",
  "RxR",
  "PanicVttDnLp",
  "OptDefault",
  "Default"
};

/// These strings match the OptResultPerByteDbgStr enum for indexing
/// the switch PrintCalcResultTableCh and PrintODTResultTable.
const char *OptResultDbgStrings[] = {
  "Best",
  "GrdBnd",
  "OffSel",
  "Scale",
  "MaxPost",
  "MinPost"
};

// Strings for TGlobalCompOffset decoding
const char *GlobalCompOffsetStr[] = {
  "RdOdt",
  "WrDS",
  "WrDSCmd",
  "WrDSCtl",
  "WrDSClk"
};

// Strings for CompGlobalOffsetParam decoding
const char *CompGlobalOffsetParamStr[] = {
  "RdOdtUp",
  "RdOdtDn",
  "WrDSUp",
  "WrDSDn",
  "WrDSCmdUp",
  "WrDSCmdDn",
  "WrDSCtlUp",
  "WrDSCtlDn",
  "WrDSClkUp",
  "WrDSClkDn",
  "SCompDq",
  "SCompCmd",
  "SCompCtl",
  "SCompClk",
  "DisOdtStatic"
};

#define ALIGN_TO_STRING ((UINT8) OptDimmOdtWr)
const char *OdtTypeString[] = {
  "OdtWr",
  "Nom",
  "NomNT",
  "NomWr",
  "NomRd",
  "Park",
  "ParkNT"
  "RttMaxType"
};

const char  *CmdIterTypesString[] = {
  "MrcIterationClock",
  "MrcIterationCmd",
  "MrcIterationCtl"
};
#endif // MRC_DEBUG_PRINT

/**
  This function approximates a non linear heaviside function using 5 linear curves.
  The curves are defined by 3 parameters:
    a - The curve's slope
    b - Intercept with y axis.
    x_max - The maximal x value to which curve parameters apply. i.e., for x < x1 a = a1, b = b1.
  It should select a balanced wr/rd operating point with respect to UPM

  @param[in]  MrcData          - MRC host structure
  @param[in]  PostMar          - Margin after training
  @param[in]  GoodPwrLimitPost - UPM limits for margins

  @retval  linear approximation function output
**/
UINT64
UPMFilter (
  IN MrcParameters *const MrcData,
  IN UINT32               PostMar,
  IN UINT16               GoodPwrLimitPost
  )
{
  const  MRC_FUNCTION *MrcCall;
  static const UINT32  a[]      = { 10,    50,   200,   50,    10,          1 };
  static const INT32   b[]      = { 0,   -100, -1225,  650,  1450,       1810 };
  static const UINT32  x_max[]  = { 250,  750,  1250, 2000,  4000, 0xFFFFFFFF };
  UINT64    LocalResult;
  MrcInput  *Inputs;
  UINT32    a_LocalResult;
  INT32     b_LocalResult;
  UINT32    i;
  UINT64    Value;

  Inputs  = &MrcData->Inputs;
  MrcCall = Inputs->Call.Func;
  Value   = PostMar;
  // Five Linear approximation optimization function value, y1=a1x+b1, 1000*y1=1000*a1*(LocalResult/UPM)+1000*b1, 1000*y1 = a1*((LocalResults*1000)/UPM)+1000*b1
  // UPM normalized with three digits after zero
  LocalResult   = MrcCall->MrcDivU64x64 (MrcCall->MrcMultU64x32 (Value, 1000), GoodPwrLimitPost, NULL);
  b_LocalResult = 0;
  a_LocalResult = 0;
  for (i = 0; i < ARRAY_COUNT (x_max); i++) {
    if (LocalResult <= x_max[i]) {
      a_LocalResult = a[i];
      b_LocalResult = b[i];
      break;
    }
  }
  LocalResult = MrcCall->MrcDivU64x64 (MrcCall->MrcMultU64x32 (LocalResult, a_LocalResult), 100, NULL) + b_LocalResult;
  // MRC_DEBUG_MSG(&MrcData->Outputs.Debug, MSG_LEVEL_NOTE, "LinearApproximationTradeOff return value=%d \n", LocalResult);

  return (LocalResult);
}
/**
  Force Rcomp to update if need be

  @param[in, out] MrcData   - MRC global data.
  @param[in]      OptParam  - Param type
  @param[in]      ForceComp - Force the comp to run regardless of the param type

  @retval BOOLEAN - Whether a comp was performed or not
**/
BOOLEAN
ForceSystemRComp (
  IN OUT MrcParameters *const MrcData,
  IN     UINT8                OptParam,
  IN     BOOLEAN              ForceComp
  )
{
  UINT8 Comp;

  Comp = 0;

  if (!ForceComp) {
    switch (OptParam) { // Re-run comps after any comp or voltage change
    case OptTxDqTco:
    case OptCCCTco:
    case OptWrDS:
    case OptWrDSUpCoarse:
    case OptWrDSDnCoarse:
    case DqDrvUpCompOffset:
    case DqDrvDnCompOffset:
    case OptRdDqOdt:
    case OptRdDqsOdt:
    case DqsOdtCompOffset:
    case DqOdtCompOffset:
    case OptRxLoad:
    case RloadCompOffset:
    case OptSComp:
    case DqSCompOffset:
    case OptCCCDS:
    case OptCCCDSUp:
    case OptCCCDSDn:
    case OptCCCDSUpCoarse:
    case OptCCCDSDnCoarse:
    case CmdRCompDrvUpOffset:
    case CmdRCompDrvDownOffset:
    case CtlRCompDrvUpOffset:
    case CtlRCompDrvDownOffset:
    case OptCCCSComp:
    case CmdSCompOffset:
    case CtlSCompOffset:
    case OptVddq:
    case OptVccDLLBypass:
    case OptRxVrefVttDecap:
    case OptRxVrefVddqDecap:
      Comp = 1;
      break;
    default:
      Comp = 0;
    }
  }

  if (Comp || ForceComp) {
    ForceRcomp (MrcData);
    return TRUE;
  }
  return FALSE;
}

/**
  This function looks at the margin values stored in the global data structure and checks
  WrT, WrV, RdT, and RdV to see if they are above the minimum margin required.

  @param[in, out] MrcData - MRC global data.

  @retval mrcSuccess - If margins are acceptable.
  @retval mrcRetrain - If margins are not acceptable.
**/
MrcStatus
MrcRetrainMarginCheck (
  IN OUT MrcParameters *const MrcData
  )
{
  MrcDebug                *Debug;
  MrcOutput               *Outputs;
  MRC_FUNCTION            *MrcCall;
  MRC_MarginTypes         MarginParam;
  MrcMarginResult         LastResultParam;
  MrcStatus               Status;
  MRC_MARGIN_LIMIT_TYPE   MarginLimitType;
  UINT16                  (*LastMargins)[MAX_RANK_IN_CHANNEL][MAX_CONTROLLER][MAX_CHANNEL][MAX_SDRAM_IN_DIMM][MAX_EDGES];
  UINT32                  BERStats[4];
  UINT16                  MinEdgeMargin[MAX_EDGES];
  UINT16                  RetrainMarginLimit;
  UINT16                  CurrentMargin;
  UINT8                   Controller;
  UINT8                   Channel;
  UINT8                   McChMask;
  UINT8                   Rank;
  UINT8                   RankMask;
  UINT8                   Edge;
  UINT8                   Loopcount;
  UINT8                   MaxMargin;
  BOOLEAN                 RdWrCmdMarginFail[NUM_RW_CMD_MARGIN_FAILURES];

  MrcCall             = MrcData->Inputs.Call.Func;
  Outputs             = &MrcData->Outputs;
  Debug               = &Outputs->Debug;
  LastMargins         = Outputs->MarginResult;
  Status              = mrcSuccess;
  Loopcount           = 17;
  MrcCall->MrcSetMem ((UINT8 *) BERStats, sizeof (BERStats), 0);
  MrcCall->MrcSetMem ((UINT8 *) RdWrCmdMarginFail, sizeof (RdWrCmdMarginFail), FALSE);
  MaxMargin         = 0;
  // Loop is dependent on the order of MRC_MarginTypes.  If this changes, please ensure functionality
  // stays the same.
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Loopcount: %d\n", Loopcount);
  // @todo Update with McChBitMask
  SetupIOTestBasicVA (MrcData, Outputs->ValidChBitMask, Loopcount, NSOE, 0, 0, 8, PatWrRd, 0, 0);
  for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
    for (Rank = 0; Rank < MAX_RANK_IN_CHANNEL; Rank++) {
      RankMask    = MRC_BIT0 << Rank;
      McChMask = 0;
      for (Channel = 0; Channel < MAX_CHANNEL; Channel++) {
        McChMask |= SelectReutRanks (MrcData, Controller, Channel, RankMask, FALSE, 0);
      }

      if (McChMask == 0) {
        continue;
      }

      for (MarginParam = RdT; MarginParam <= CmdV; MarginParam++) {
        switch (MarginParam) {
          case CmdT:
          case RdT:
          case WrT:
            Outputs->DQPat = RdRdTA;
            MaxMargin = (CmdT == MarginParam) ? MAX_CCC_POSSIBLE_TIME : MAX_POSSIBLE_TIME;
            break;

          case CmdV:
          case RdV:
          case WrV:
            Outputs->DQPat = BasicVA;
            MaxMargin = GetVrefOffsetLimits (MrcData, MarginParam);
            break;

          default:
            // Skip margin parameter.
            continue;
        }

        Status = MrcGetBERMarginCh (
          MrcData,
          LastMargins,
          McChMask,
          RankMask,
          RankMask,
          MarginParam,
          0,
          1,
          MaxMargin,
          0,
          BERStats
          );
      }
    }
  }

  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Margins\nParams: RdT\tWrT\tRdV\tWrV\tCmdT\tCmdV\n\tLft Rgt Lft Rgt Low Hi  Low Hi  Lft Rgt Low Hi");
  for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
    for (Channel = 0; Channel < MAX_CHANNEL; Channel++) {
      if (!MrcChannelExist (MrcData, Controller, Channel)) {
        continue;
      }

      for (Rank = 0; Rank < MAX_RANK_IN_CHANNEL; Rank++) {
        if (!MrcRankExist (MrcData, Controller, Channel, Rank)) {
          continue;
        }

        MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\nCo%uCh%uRa%u\t", Controller, Channel, Rank);
        for (MarginParam = RdT; MarginParam <= CmdV; MarginParam++) {
          if (MarginParam != RdT && MarginParam != RdV && MarginParam != WrT && MarginParam != WrV && MarginParam != CmdT && MarginParam != CmdV) {
            continue;
          }

          LastResultParam     = GetMarginResultType (MarginParam);
          RetrainMarginLimit  = MrcGetUpmPwrLimit (MrcData, MarginParam, RetrainLimit) / 10;
          MrcCall->MrcSetMemWord (MinEdgeMargin, MAX_EDGES, (UINT16) (~0));

          for (Edge = 0; Edge < MAX_EDGES; Edge++) {
            CurrentMargin       = LastMargins[LastResultParam][Rank][Controller][Channel][0][Edge] / 10;
            MinEdgeMargin[Edge] = MIN (MinEdgeMargin[Edge], CurrentMargin);
            if ((CurrentMargin <= RetrainMarginLimit)) {
              Status =  mrcRetrain;
              if ((MarginParam == RdT) || (MarginParam == RdV)) {
                RdWrCmdMarginFail[0] = TRUE;
              } else if ((MarginParam == WrT) || (MarginParam == WrV)) {
                RdWrCmdMarginFail[1] = TRUE;
              } else if ((MarginParam == CmdT) || (MarginParam == CmdV)) {
                RdWrCmdMarginFail[2] = TRUE;
              }
            }
          }
          MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "%2d  %2d\t", MinEdgeMargin[0], MinEdgeMargin[1]);
          if ((RdWrCmdMarginFail[0] == TRUE) && (RdWrCmdMarginFail[1] == TRUE) && (RdWrCmdMarginFail[2] == TRUE)) {
            Rank    = MAX_RANK_IN_CHANNEL;
            Channel = MAX_CHANNEL;
            break;
          }
        }
      }
    }
  }
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\n");  // End of table

  if (Status == mrcRetrain) {
    // Loop is dependent on the order of MRC_MarginTypes.  If this changes, please ensure functionality
    // stays the same.
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "*** Margin Limit Check Failed! ***\nNew Limits:\nParam\tUPM\tPWR");
    for (MarginParam = RdT; MarginParam <= CmdV; MarginParam++) {
      if (((RdWrCmdMarginFail[0] == FALSE) && ((MarginParam == RdT) || (MarginParam == RdV))) ||
          ((RdWrCmdMarginFail[1] == FALSE) && ((MarginParam == WrT) || (MarginParam == WrV))) ||
          ((RdWrCmdMarginFail[2] == FALSE) && ((MarginParam == CmdT) || (MarginParam == CmdV))) ||
          (MarginParam != RdT && MarginParam != RdV && MarginParam != WrT && MarginParam != WrV && MarginParam != CmdT && MarginParam != CmdV)) {
        continue;
      }
      MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\n%s", gMarginTypesStr[MarginParam]);
      for (MarginLimitType = UpmLimit; MarginLimitType < RetrainLimit; MarginLimitType++) {
        RetrainMarginLimit = MrcUpdateUpmPwrLimits (MrcData, MarginParam, MarginLimitType, MRC_UPM_PWR_INC_VAL);
        MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\t%d", RetrainMarginLimit);
      }
    }
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\n");  // End of table.
  }

  return Status;
}

/**
  Force OLTM (Open Loop Thermal Management) on Hynix DDR4 DRAMs before ww45'2015.
  Only applied for 2DPC.

  @param[in] MrcData  - Include all MRC global data.

  @retval MrcStatus - if it succeed return mrcSuccess
**/
extern
MrcStatus
MrcForceOltm (
  IN MrcParameters *const MrcData
  )
{
  MrcOutput         *Outputs;
  MrcControllerOut  *ControllerOut;
  MrcChannelOut     *ChannelOut;
  MrcInput          *Inputs;
  MrcDebug          *Debug;
  MrcSpd            *SpdIn;
  MrcStatus         Status;
  UINT16            DateCode;
  UINT8             Controller;
  UINT8             Channel;
  UINT8             Dimm;
  BOOLEAN           UpdateNeeded;
  UINT32            Offset;
  SPD4_MANUFACTURING_DATA              *ManufactureData;
  MC0_CH0_CR_SCHED_SECOND_CBIT_STRUCT  SchedSecondCbit;
  MC0_CH0_CR_MCMNTS_SPARE_STRUCT       McmntsSpare;
  MC0_CH0_CR_DDR_MR_PARAMS_STRUCT      DdrMrParams;

  Inputs        = &MrcData->Inputs;
  Outputs       = &MrcData->Outputs;
  Debug         = &Outputs->Debug;
  Status        = mrcSuccess;

  UpdateNeeded = FALSE;
  for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
    ControllerOut = &Outputs->Controller[Controller];
    for (Channel = 0; Channel < MAX_CHANNEL; Channel++) {
      if (((1 << Channel) & Outputs->ValidChBitMask) == 0) {
        continue;
      }
      ChannelOut = &ControllerOut->Channel[Channel];
      if (ChannelOut->DimmCount != 2) {
        continue;  // Not 2DPC
      }

      for (Dimm = 0; Dimm < MAX_DIMMS_IN_CHANNEL; Dimm++) {
        SpdIn = &Inputs->Controller[Controller].Channel[Channel].Dimm[Dimm].Spd.Data;
        ManufactureData = &SpdIn->Ddr4.ManufactureInfo;
        if ((ManufactureData->DramIdCode.Data == 0xAD80) ||
            (ManufactureData->ModuleId.IdCode.Data == 0xAD80)) { // Hynix
          DateCode = (ManufactureData->ModuleId.Date.Year << 8) | ManufactureData->ModuleId.Date.Week;
          if (DateCode < 0x1545) {  // Before ww45'2015
            UpdateNeeded = TRUE;
            break;
          }
        }
      }
    } // for Channel
  } // for Controller

  if (UpdateNeeded) {
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Forcing 2x refresh...\n");
    SchedSecondCbit.Data = MrcReadCR (MrcData, MC0_CH0_CR_SCHED_SECOND_CBIT_REG);
    SchedSecondCbit.Bits.dis_srx_mr4 = 1;
    MrcWriteCrMulticast (MrcData, MC0_BC_CR_SCHED_SECOND_CBIT_REG, SchedSecondCbit.Data);
    for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
      for (Channel = 0; Channel < MAX_CHANNEL; Channel++) {
        Offset = OFFSET_CALC_MC_CH (MC0_CH0_CR_DDR_MR_PARAMS_REG, MC1_CH0_CR_DDR_MR_PARAMS_REG, Controller, MC0_CH1_CR_DDR_MR_PARAMS_REG, Channel);
        DdrMrParams.Data = MrcReadCR (MrcData, Offset);
        DdrMrParams.Bits.MR4_PERIOD         = 0;
        DdrMrParams.Bits.DDR4_TS_readout_en = 0;
        MrcWriteCR (MrcData, Offset, DdrMrParams.Data);

        Offset = OFFSET_CALC_MC_CH (MC0_CH0_CR_MCMNTS_SPARE_REG, MC1_CH0_CR_MCMNTS_SPARE_REG, Controller, MC0_CH1_CR_MCMNTS_SPARE_REG, Channel);
        McmntsSpare.Data = MrcReadCR (MrcData, Offset);
        McmntsSpare.Bits.ForceX2Ref = 1;  // force 2X refresh
        MrcWriteCR (MrcData, Offset, McmntsSpare.Data);
      }
    }
  } else {
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Force OLTM is not needed in the current DRAM configuration\n");
  }

  return Status;
}


/**
  Calculate the VDDQ limits allowed by the LPDDR overshoot equations by sweep index
  @param[in] MrcData  - Include all MRC global data.
  @retval MrcStatus - if it succeed return mrcSuccess
**/
extern
MrcStatus
CalcVDDQLimits(
  IN MrcParameters *const MrcData,
  OUT UINT16       *const Limits
)
{
  Limits[0] = 0;
  Limits[1] = 0xFFFF;
  // TGL_POWER_TRAINING_VDDQ - Need to fill this out if VDDQ training is implemented. No guardband is required here.
  return mrcSuccess;
}

#ifdef FULL_EV_MERGE
/**
  Calculate the RTT write limits allowed by the LPDDR overshoot equations in ohms.
  We can ignore PHY ODT digital offsets here if DRAM ODT is trained before PHY ODT.
  @param[in] MrcData  - Include all MRC global data.
  @retval MrcStatus - if it succeed return mrcSuccess
**/
extern
MrcStatus
CalcRttWriteLimits(
  IN MrcParameters *const MrcData,
  OUT UINT16       *const Limits
)
{
  MrcInput            *Inputs;
  MrcOutput           *Outputs;
  INT64               GetSetVal;
  UINT32              FirstController;
  UINT32              FirstChannel;
  UINT16              RTT_WR;
  UINT16              Temp;
  UINT16              VLimit;
  Inputs = &MrcData->Inputs;
  Outputs = &MrcData->Outputs;
  FirstController = (MrcControllerExist(MrcData, cCONTROLLER0)) ? 0 : 1;
  FirstChannel = MrcData->Outputs.Controller[FirstController].FirstPopCh;
  Limits[0] = 0;
  Limits[1] = 0xFFFF;
  if (!Outputs->Lpddr) {
    return mrcSuccess;
  }
  MrcGetSetChStrb(MrcData, FirstController, FirstChannel, 0, GsmIocDataDqsOdtParkMode, ReadFromCache | (MRC_PWR_DBG_PRINT ? PrintValue : 0), &GetSetVal);
  if (GetSetVal == 1 || GetSetVal == 2) {
    MrcGetSetChStrb(MrcData, FirstController, FirstChannel, 0, GsmIocDataDqsNParkLow, ReadFromCache | (MRC_PWR_DBG_PRINT ? PrintValue : 0), &GetSetVal);
    if (GetSetVal == 1) {
      Temp = (7 * ((UINT16)Outputs->VccddqVoltage * TWO_DECIMAL_PLACES / VSWING - (UINT16)((3 * TWO_DECIMAL_PLACES) / 2))) / 10;
      RTT_WR = (Outputs->RcompTarget[RdOdt] * TWO_DECIMAL_PLACES) / Temp;
      MRC_POWER_DEBUG_MSG(&Outputs->Debug, MSG_LEVEL_NOTE, "\nCalcRttWriteLimits lower limit Temp = %d, RTT_WR = %d\n", Temp, RTT_WR);

      if ((Temp * RTT_WR) / TWO_DECIMAL_PLACES < 250) {
        Limits[0] = RTT_WR; // No guardband is required here
      }
    } else {
      VLimit = MrcGetVLimit(MrcData);
      Limits[1] = (Outputs->RcompTarget[RdOdt] * VLimit - (UINT16)Outputs->VccddqVoltage * Z_O) / ((UINT16)Outputs->VccddqVoltage - VLimit); // No guardband is required here
      Temp = (7 * ((UINT16)Outputs->VccddqVoltage * TWO_DECIMAL_PLACES / VSWING - (UINT16)(1 * TWO_DECIMAL_PLACES))) / 10;
      RTT_WR = (Outputs->RcompTarget[RdOdt] * TWO_DECIMAL_PLACES) / Temp;

      MRC_POWER_DEBUG_MSG(&Outputs->Debug, MSG_LEVEL_NOTE, "\nCalcRttWriteLimits lower limit Temp = %d, RTT_WR = %d\n", Temp, RTT_WR);

      if ((Temp * RTT_WR) / TWO_DECIMAL_PLACES < 250) {
        Limits[0] = RTT_WR; // No guardband is required here
      }
    }

    MRC_POWER_DEBUG_MSG(&Outputs->Debug, MSG_LEVEL_NOTE, "\nCalcRttWriteLimits before flipped limit check Limits[0] = %d, Limits[1] = %d\n", Limits[0], Limits[1]);

    if (Limits[1] < Limits[0]) {
      Limits[0] = (Limits[0] + Limits[1]) / 2;
      Limits[1] = Limits[0];
      MRC_DEBUG_MSG(&Outputs->Debug, MSG_LEVEL_NOTE, "\nCalcRttWriteLimits calculated a lower limit higher than the upper limit! LPDDR Overshoot violated!");
    }
    MRC_POWER_DEBUG_MSG(&Outputs->Debug, MSG_LEVEL_NOTE, "\nCalcRttWriteLimits final Limits[0] = %d, Limits[1] = %d\n", Limits[0], Limits[1]);

  }

  return mrcSuccess;
}

/**
  Calculate the Rx comp vref limits allowed by the LPDDR overshoot equations in ohms

  @param[in] MrcData  - Include all MRC global data.

  @retval MrcStatus - if it succeed return mrcSuccess
**/
extern
MrcStatus
CalcRxCompVrefLimits(
  IN MrcParameters *const MrcData,
  IN BOOLEAN       UseTable,
  OUT UINT16       *const Limits
)
{
  MrcOutput           *Outputs;
  MrcChannelOut       *ChannelOut;
  INT64               GetSetVal;
  UINT32              FirstController;
  UINT32              FirstChannel;
  UINT16              RTT_WR_Min = 0xFFFF;
  UINT16              RTT_WR_Max = 0;
  INT16               RTT_WR_Temp;
  UINT16              RxOdt;
  UINT16              RxOdt2;
  UINT16              VLimit;
  UINT8               Controller;
  UINT8               Channel;

  Outputs = &MrcData->Outputs;
  FirstController = (MrcControllerExist(MrcData, cCONTROLLER0)) ? 0 : 1;
  FirstChannel = MrcData->Outputs.Controller[FirstController].FirstPopCh;
  Limits[0] = 0;
  Limits[1] = 0xFFFF;

  if (!Outputs->Lpddr) {
    return mrcSuccess;
  }

  MrcGetSetChStrb(MrcData, FirstController, FirstChannel, 0, GsmIocDataDqsOdtParkMode, ReadFromCache | (MRC_PWR_DBG_PRINT ? PrintValue : 0), &GetSetVal);

  if (GetSetVal == 1 || GetSetVal == 2) {
    if (UseTable) {
      RTT_WR_Min = MrcGetLpddrDqOdtTableValue(MrcData);
      RTT_WR_Max = RTT_WR_Min;

      MRC_POWER_DEBUG_MSG(&Outputs->Debug, MSG_LEVEL_NOTE, "\nCalcRxCompVrefLimits UseTable = 1, RTT_WR_Max & RTT_WR_Min = %d\n", RTT_WR_Max);

    } else {
      for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
        for (Channel = 0; Channel < MAX_CHANNEL; Channel++) {
          if (!(MrcChannelExist(MrcData, Controller, Channel))) {
            continue;
          }
          ChannelOut = &Outputs->Controller[Controller].Channel[Channel];
          RTT_WR_Temp = LpddrOdtDecode(0x7 & (ChannelOut->Dimm[dDIMM0].Rank[0].MR[MrcGetDqOdtMrIndex(MrcData)]));
          if (RTT_WR_Temp == -2) {
            MRC_DEBUG_MSG(&Outputs->Debug, MSG_LEVEL_ERROR, "Invalid DIMM 0 RANK 0 LPDDR Dq ODT MR value! %d\n");
            return mrcFail;
          }
          MRC_POWER_DEBUG_MSG(&Outputs->Debug, MSG_LEVEL_NOTE, "\nCalcRxCompVrefLimits MR11 = %d\n", ChannelOut->Dimm[dDIMM0].Rank[0].MR[MrcGetDqOdtMrIndex(MrcData)]);
          MRC_POWER_DEBUG_MSG(&Outputs->Debug, MSG_LEVEL_NOTE, "\nCalcRxCompVrefLimits RTT_WR_Temp = %d\n", RTT_WR_Temp);

          if (RTT_WR_Temp > RTT_WR_Max) {
            RTT_WR_Max = RTT_WR_Temp;
          }
          if (RTT_WR_Temp < RTT_WR_Min) {
            RTT_WR_Min = RTT_WR_Temp;
          }
        }
      }

      MRC_POWER_DEBUG_MSG(&Outputs->Debug, MSG_LEVEL_NOTE, "\nCalcRxCompVrefLimits RTT_WR_Max = %d, RTT_WR_Min: %d\n", RTT_WR_Max, RTT_WR_Min);

    }

    MrcGetSetChStrb(MrcData, FirstController, FirstChannel, 0, GsmIocDataDqsNParkLow, ReadFromCache | (MRC_PWR_DBG_PRINT ? PrintValue : 0), &GetSetVal);

    if (GetSetVal == 1) {
      RxOdt = (7 * RTT_WR_Min * ((UINT16)Outputs->VccddqVoltage * TWO_DECIMAL_PLACES / VSWING - (UINT16)((3 * TWO_DECIMAL_PLACES) / 2))) / (TWO_DECIMAL_PLACES * 10);

      MRC_POWER_DEBUG_MSG(&Outputs->Debug, MSG_LEVEL_NOTE, "\nCalcRxCompVrefLimits upper limit RxOdt = %d\n", RxOdt);

      Limits[1] = MIN(250, RxOdt); // No guardband is required here
    } else {
      VLimit = MrcGetVLimit(MrcData);
      RxOdt = ((((UINT16)Outputs->VccddqVoltage * TWO_DECIMAL_PLACES) / VLimit * (RTT_WR_Max + Z_O)) - RTT_WR_Max * TWO_DECIMAL_PLACES) / TWO_DECIMAL_PLACES;
      RxOdt2 = (Z_O * ((2 * (UINT16)Outputs->VccddqVoltage * TWO_DECIMAL_PLACES) / VLimit - 1 * TWO_DECIMAL_PLACES)) / TWO_DECIMAL_PLACES;

      MRC_POWER_DEBUG_MSG(&Outputs->Debug, MSG_LEVEL_NOTE, "\nCalcRxCompVrefLimits lower limit RxOdt = %d, RxOdt2 = %d\n", RxOdt, RxOdt2);

      Limits[0] = MAX(RxOdt, RxOdt2);

      RxOdt = (7 * RTT_WR_Min * ((UINT16)Outputs->VccddqVoltage * TWO_DECIMAL_PLACES / VSWING - (UINT16)(1 * TWO_DECIMAL_PLACES))) / (TWO_DECIMAL_PLACES * 10);
      MRC_POWER_DEBUG_MSG(&Outputs->Debug, MSG_LEVEL_NOTE, "\nCalcRxCompVrefLimits upper limit RxOdt = %d\n", RxOdt);

      Limits[1] = MIN(250, RxOdt); // No guardband is required here
    }

    if (Limits[1] < Limits[0]) {
      Limits[0] = (Limits[0] + Limits[1]) / 2;
      Limits[1] = Limits[0];
      MRC_POWER_DEBUG_MSG(&Outputs->Debug, MSG_LEVEL_NOTE, "\nCalcRxCompVrefLimits calculated a lower limit higher than the upper limit! LPDDR Overshoot violated!");
    }

    MRC_POWER_DEBUG_MSG(&Outputs->Debug, MSG_LEVEL_NOTE, "\nCalcRxCompVrefLimits Limits[0] = %d, Limits[1] = %d\n", Limits[0], Limits[1]);

  }

  return mrcSuccess;
}

/**
  Calculate the RxDqs comp offset limits allowed by the LPDDR overshoot equations by sweep index

  @param[in] MrcData  - Include all MRC global data.

  @retval MrcStatus - if it succeed return mrcSuccess
**/
extern
MrcStatus
CalcRxDqsCompOffsetLimits(
  IN MrcParameters *const MrcData,
  OUT INT8         *const Limits
  )
{
  MrcInput            *Inputs;
  MrcOutput           *Outputs;
  UINT32              FirstController;
  UINT32              FirstChannel;
  UINT32              Rleg;
  INT64               StrobeOdtStaticEn;
  INT64               RcompOdt;
  UINT16              RxCompVrefLimits[2];

  Inputs = &MrcData->Inputs;
  Outputs = &MrcData->Outputs;
  FirstController = (MrcControllerExist(MrcData, cCONTROLLER0)) ? 0 : 1;
  FirstChannel = MrcData->Outputs.Controller[FirstController].FirstPopCh;

  if (!Outputs->Lpddr) {
    return mrcSuccess;
  }

  CalcRxCompVrefLimits(MrcData, FALSE, RxCompVrefLimits); // Find limits in Ohms

  MRC_POWER_DEBUG_MSG(&Outputs->Debug, MSG_LEVEL_NOTE, "\nCalcRxDqsCompOffsetLimits Limits[0] = %d, Limits[1] = %d\n", RxCompVrefLimits[0], RxCompVrefLimits[1]);
  MRC_POWER_DEBUG_MSG(&(MrcData->Outputs.Debug), MSG_LEVEL_NOTE, "\nCalcRxDqsCompOffsetLimits RcompTarget[RdOdt] = %d\n", Outputs->RcompTarget[RdOdt]);


  MrcGetSetChStrb(MrcData, FirstController, FirstChannel, 0, GsmIocStrobeOdtStaticEn, ReadFromCache | (MRC_PWR_DBG_PRINT ? PrintValue : 0), &StrobeOdtStaticEn);
  MrcGetSetNoScope(MrcData, (Outputs->OdtMode == MrcOdtModeVss) ? CompRcompOdtDn : CompRcompOdtUp, ReadUncached | (MRC_PWR_DBG_PRINT ? PrintValue : 0), &RcompOdt);

  Rleg = Outputs->RcompTarget[RdOdt] * (23 * (UINT32) (StrobeOdtStaticEn) + (UINT32) RcompOdt);

  MRC_POWER_DEBUG_MSG(&(MrcData->Outputs.Debug), MSG_LEVEL_NOTE, "\nCalcRxDqsCompOffsetLimits Rleg = %d\n", Rleg);


  // High PHY impedance limit dictates low digital offset limit and vice versa
  if (RxCompVrefLimits[1] == 0) {
    Limits[0] = 0x7F;
  } else {
    Limits[0] = (INT8)(((INT32)Rleg - (23 + (INT32)RcompOdt) * (INT32)RxCompVrefLimits[1]) / (INT32)RxCompVrefLimits[1]); // No guardband is required here
  }
  if (RxCompVrefLimits[0] == 0) {
    Limits[1] = 0x7F;
  } else {
    Limits[1] = (INT8)(((INT32)Rleg - (23 + (INT32)RcompOdt) * (INT32)RxCompVrefLimits[0]) / (INT32)RxCompVrefLimits[0]); // No guardband is required here
  }

  if (Limits[1] < Limits[0]) {
    Limits[0] = 0;
    Limits[1] = 0;
    MRC_DEBUG_MSG(&Outputs->Debug, MSG_LEVEL_NOTE, "\nCalcRxDqsCompOffsetLimits calculated a lower limit higher than the upper limit! LPDDR Overshoot violated!");
  }

  MRC_POWER_DEBUG_MSG(&Outputs->Debug, MSG_LEVEL_NOTE, "\nCalcRxDqsCompOffsetLimits Limits[0] = %d, Limits[1] = %d\n", Limits[0], Limits[1]);


  return mrcSuccess;
}

#endif

/**
  This function calculates the average Rx ODT impedance for the given channel (in ohms).

  @param[in]  MrcData       - Pointer to global MRC data.
  @param[in]  Controller    - 0-based index to controller
  @param[in]  Channel       - 0-based index to legacy channel (x64 bit)
  @param[out] ImpedanceOhms - Pointer to impedance return value

  @retval mrcSuccess Successfully calculated impedance
**/
MrcStatus
CalcRxDqOdtAverageByteImpedance (
  IN  MrcParameters *const MrcData,
  IN  UINT8                Controller,
  IN  UINT8                Channel,
  OUT UINT16        *const ImpedanceOhms
)
{
  MrcOutput          *Outputs;
  MrcInput           *Inputs;
  MrcChannelOut      *ChannelOut;
  const MRC_FUNCTION *MrcCall;
  INT64          GetSetVal;
  INT16          AverageRxDqOdtCompOffset = 0;
  UINT8          CompVref;
  INT64          MinCompVref;
  INT64          MaxCompVref;
  UINT8          AdjustedCompVref;
  UINT32         AdjustedRxDqOdtTarget;
  UINT8          Byte;
  UINT8          Bytes = 0;
  UINT8          Rank;
  INT8           NewCompUp;
  INT8           NewCompDn;
  INT8           NewComp = 0;
  INT8           CurrentComp;
  INT8           MinDelta;
  INT8           CurrentDelta;
  INT64          MaxComp;
  INT64          MinComp;
  UINT8          NewCompVref;
  INT8           Sign;
  UINT8          Offset = 1;

  UINT64         Timeout;
  UINT32         Delay;
  DATA0CH0_CR_DDRCRDATAOFFSETCOMP_STRUCT DdrCrDataOffsetComp;
  MrcStatus      Status;
  UINT32         NewCompValue;

  Outputs      = &MrcData->Outputs;
  Inputs       = &MrcData->Inputs;
  MrcCall      = Inputs->Call.Func;
  ChannelOut   = &Outputs->Controller[Controller].Channel[Channel];

  if (NULL == ImpedanceOhms) {
    MRC_DEBUG_MSG (&Outputs->Debug, MSG_LEVEL_ERROR, "%s: ImpedanceOhms argument\n", gNullPtrErrStr);
    return mrcWrongInputParameter;
  }

  MrcGetSetLimits(MrcData, DqOdtVrefUp, &MinCompVref, &MaxCompVref, &Delay);
  MrcGetSetLimits(MrcData, CompRcompOdtUp, &MinComp, &MaxComp, &Delay);

  MrcGetSetNoScope(MrcData, DqOdtVrefUp, ReadFromCache, &GetSetVal); // Only OdtVrefUp matter
  CompVref = (UINT8)GetSetVal;

  MRC_POWER_DEBUG_MSG(&Outputs->Debug, MSG_LEVEL_NOTE, "\nCalcRxDqOdtAverageByteImpedance initial CompVref: %d\n", CompVref);


  for (Rank = 0; Rank < MAX_RANK_IN_CHANNEL; Rank++) {
    if (!(MrcRankExist(MrcData, Controller, Channel, Rank))) {
      continue;
    }
    for (Byte = 0; Byte < MAX_SDRAM_IN_DIMM; Byte++) {
      if (!MrcByteExist(MrcData, Controller, Channel, Byte)) {
        continue;
      }
      DdrCrDataOffsetComp.Data = ChannelOut->DataCompOffset[Byte];
      AverageRxDqOdtCompOffset += (INT16)MrcSE((UINT16)DdrCrDataOffsetComp.Bits.dqodtcompoffset, DATA0CH0_CR_DDRCRDATAOFFSETCOMP_dqodtcompoffset_WID, 16);
      Bytes++;
    }
  }

  MRC_POWER_DEBUG_MSG(&Outputs->Debug, MSG_LEVEL_NOTE, "CalcRxDqOdtAverageByteImpedance total AverageRxDqOdtCompOffset: %d\n", AverageRxDqOdtCompOffset);

  AverageRxDqOdtCompOffset /= Bytes;

  MRC_POWER_DEBUG_MSG(&Outputs->Debug, MSG_LEVEL_NOTE, "CalcRxDqOdtAverageByteImpedance average AverageRxDqOdtCompOffset: %d\n", AverageRxDqOdtCompOffset);


  // Now, we need to know how much the base impedance value should be adjusted due to the average RxDq ODT Comp Offset
  Sign = (AverageRxDqOdtCompOffset < 0) ? -1 : 1;
  Sign *= 1; // Using ODT Comp Vref Up always, Vref Dn always set to 96 (1/2 VrefUp)

  AdjustedCompVref = CompVref;
  MinDelta = (INT8)ABS(AverageRxDqOdtCompOffset);
  CurrentComp = GetCompCode(MrcData, OptRdDqOdt, COMP_UP);

  MRC_POWER_DEBUG_MSG(&Outputs->Debug, MSG_LEVEL_NOTE, "CalcRxDqOdtAverageByteImpedance current comp up: %d\n", CurrentComp);


  if(AverageRxDqOdtCompOffset != 0) { // Loop until we find a break condition as long as the average comp offset is not 0
    Timeout = MrcCall->MrcGetCpuTime() + 10000; // 10 seconds timeout
    while (1) {
      if ((MrcCall->MrcGetCpuTime() >= Timeout)) {
        MRC_DEBUG_MSG(&Outputs->Debug, MSG_LEVEL_ERROR, "\nTimeout in CalcRxDqOdtAverageByteImpedance! \n");
        return mrcFail;
      }
      NewCompVref = CompVref + (Sign * Offset);

      MRC_POWER_DEBUG_MSG(&Outputs->Debug, MSG_LEVEL_NOTE, "CalcRxDqOdtAverageByteImpedance new CompVref: %d\n", NewCompVref);

      if ((MinCompVref > NewCompVref) || (NewCompVref > MaxCompVref)) {
        MRC_DEBUG_MSG(&Outputs->Debug, MSG_LEVEL_WARNING, "Warning! We saturated the RxDq ODT comp vref before we could shift it enough to make the comp shift match the average comp offset. Impedance will only be partially accurate! \n");
        break;
      }

      Status = UpdateCompGlobalOffset(MrcData, RdOdtUp, NewCompVref, FALSE, FALSE, &NewCompValue); // No need to update VrefDn, it always 96

      if (mrcSuccess != Status) {
        MRC_DEBUG_MSG (&Outputs->Debug, MSG_LEVEL_ERROR, "UpdateCompGlobalOffset() error %d\n", Status);
        return mrcFail;
      }
      NewCompUp = GetCompCode(MrcData, OptRdDqOdt, COMP_UP);
      NewCompDn = GetCompCode(MrcData, OptRdDqOdt, COMP_DN);
      MRC_POWER_DEBUG_MSG(&Outputs->Debug, MSG_LEVEL_NOTE, "CalcRxDqOdtAverageByteImpedance new comp up: %d\n", NewCompUp);
      MRC_POWER_DEBUG_MSG(&Outputs->Debug, MSG_LEVEL_NOTE, "CalcRxDqOdtAverageByteImpedance new comp down: %d\n", NewCompDn);

      if (NewCompUp != -1) {
        if ((RESERVED_COMP_CODES > NewCompUp) || (NewCompUp > (MaxComp - RESERVED_COMP_CODES))) {
          MRC_DEBUG_MSG(&Outputs->Debug, MSG_LEVEL_WARNING, "Warning! We saturated the RxDq ODT comp up before we could shift it as far as the average comp offset. Impedance will only be partially accurate! \n");
          break;
        }
        NewComp = NewCompUp;
      }
      if (NewCompDn != -1) {
        if ((RESERVED_COMP_CODES > NewCompDn) || (NewCompDn > (MaxComp - RESERVED_COMP_CODES))) {
          MRC_DEBUG_MSG(&Outputs->Debug, MSG_LEVEL_WARNING, "Warning! We saturated the RxDq ODT comp down before we could shift it as far as the average comp offset. Impedance will only be partially accurate! \n");
          break;
        }
        NewComp = NewCompDn;
      }

      // Move the Comp Vref until the comp output changes an amount equivalent to the average comp offset
      CurrentDelta = ABS(CurrentComp + (INT8)AverageRxDqOdtCompOffset - NewComp);
      if (CurrentDelta <= MinDelta) {
        MinDelta = CurrentDelta;
        AdjustedCompVref = NewCompVref;
        if (MinDelta == 0) {
          MRC_POWER_DEBUG_MSG(&Outputs->Debug, MSG_LEVEL_NOTE, "CalcRxDqOdtAverageByteImpedance (MinDelta == 0) \n");
          break; // We're done
        }
      } else {
        MRC_POWER_DEBUG_MSG(&Outputs->Debug, MSG_LEVEL_NOTE, "CalcRxDqOdtAverageByteImpedance (CurrentDelta > MinDelta) CurrentDelta = %d, MinDelta = %d \n", CurrentDelta, MinDelta);
        break; // We overshot, so we're done
      }

      Offset++;
    }

    // Restore original comp vref value
    Status = UpdateCompGlobalOffset(MrcData, RdOdtUp, CompVref, FALSE, TRUE, &NewCompValue);
    if (mrcSuccess != Status) {
      MRC_DEBUG_MSG (&Outputs->Debug, MSG_LEVEL_ERROR, "UpdateCompGlobalOffset() error %d\n", Status);
      return mrcFail;
    }
  }

  MRC_POWER_DEBUG_MSG(&Outputs->Debug, MSG_LEVEL_NOTE, "CalcRxDqOdtAverageByteImpedance AdjustedCompVref: %d\n", AdjustedCompVref);
  MRC_POWER_DEBUG_MSG(&Outputs->Debug, MSG_LEVEL_NOTE, "CalcRxDqOdtAverageByteImpedance ODTSingleSegEn: %d\n", ODTSingleSegEn);

  // Vref_pupcode = 192 * Rext / (Rext + Rtarg_pup)
  // Since the pulldown strength is relative to the pullup strength, the Vref / Vsupply ratio is always 0.5.
  // Vref_pdncode = 192 * 0.5 = 96
  AdjustedRxDqOdtTarget = DIVIDEROUND(((MRC_COMP_VREF_STEP_SIZE - AdjustedCompVref) * Inputs->RcompResistor), AdjustedCompVref);

  MRC_POWER_DEBUG_MSG(&Outputs->Debug, MSG_LEVEL_NOTE, "CalcRxDqOdtAverageByteImpedance AdjustedRxDqOdtTarget: %d\n", AdjustedRxDqOdtTarget);

  *ImpedanceOhms = (UINT16) AdjustedRxDqOdtTarget; // Actual RxDqOdt impedance value in ohms
  return mrcSuccess;
}

/**
  This function is responsible for updating the SOC ODT value in LPDDR4/5 memory based on the CPU ODT settings.
  This reads the CPU ODT using the current comp settings, find the closest SOC ODT settings and programs it to the corresponding MR.
  Note - calculating CPU impedance assumes the cached value holds the current settings.

  @param[in]  MrcData - Pointer to global MRC data.

  @retval -Nothing.
**/
void
LpddrUpdateSocOdt(
  IN MrcParameters *const MrcData
  )
{
  UINT8           Channel;
  UINT8           Controller;
  UINT16          CpuImpedance;
  UINT16          SocOdt;
  MrcStatus       Status;

  for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
    for (Channel = 0; Channel < MAX_CHANNEL; Channel++) {
      if (!(MrcChannelExist(MrcData, Controller, Channel))) {
        continue;
      }

      Status = CalcRxDqOdtAverageByteImpedance(MrcData, Controller, Channel, &CpuImpedance);
      if (mrcSuccess != Status) {
        MRC_DEBUG_MSG (&MrcData->Outputs.Debug, MSG_LEVEL_ERROR, "CalcRxDqOdtAverageByteImpedance() error %d\n", Status);
      }
      SocOdt = MrcCheckForSocOdtEnc(MrcData, CpuImpedance);

      MRC_POWER_DEBUG_MSG(&(MrcData->Outputs.Debug), MSG_LEVEL_NOTE, "LpddrUpdateSocOdt: CpuImpedance = %d, SocOdt = %d \n", CpuImpedance, SocOdt);

      SetDimmParamValue(MrcData, Controller, Channel, 0xF, OptDimmSocOdt, SocOdt, TRUE);
    }
  }
}

/**
  Print DIMM ODT / Ron values per DIMM.
  Also print CPU ODT / Ron.

  @param[in] MrcData  - Include all MRC global data.

  @retval none
**/
extern
void
MrcPrintDimmOdtValues (
  IN MrcParameters *const MrcData
  )
{
#ifdef MRC_DEBUG_PRINT
  MrcOutput     *Outputs;
  MrcChannelOut *ChannelOut;
  MrcDebug      *Debug;
  UINT32        Controller;
  UINT32        Channel;
  UINT8         RankMask;
  UINT8         Rank;
  UINT8         Dimm;
  BOOLEAN       Ddr4;
  BOOLEAN       Ddr5;
  UINT16        OdtWrite;
  UINT16        OdtNom;
  UINT16        OdtNomWr;
  UINT16        OdtNomRd;
  UINT16        OdtPark;
  UINT16        OdtCA;
  UINT16        DimmRon;
  UINT16        CpuOdt[MAX_CHANNEL];
  //UINT16        CpuRon[MAX_CHANNEL];
  MrcStatus     Status;

  Outputs = &MrcData->Outputs;
  Debug   = &Outputs->Debug;
  Ddr4    = (Outputs->DdrType == MRC_DDR_TYPE_DDR4);
  Ddr5    = (Outputs->DdrType == MRC_DDR_TYPE_DDR5);
  OdtNom = 0;
  OdtNomWr = 0;
  OdtNomRd = 0;
  OdtPark = 0;
  OdtCA = 0;

//   DIMM ODT summary:
//
//   (DDR4)     Ron     OdtWr   OdtNom    OdtPark
//   (DDR5)     Ron     OdtWr   OdtNomWr  OdtNomRd  OdtPark   OdtCA
//   (LPDDR4)   Ron     OdtDQ   OdtCA
//   ---------------------------------------
//   Mc0C0D0:   48      Hi-Z    48      80
//   Mc0C0D1:   48      240     60      120
//   ---------------------------------------
//   Mc1C0D0:   48      48      34      34
//   Mc1C0D1:   48      48      34      34
//   ---------------------------------------
//
//   CPU Summary: Ron = 56, Read ODT = 109
//
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\nDIMM ODT summary:\n\t\tRon\t%s\n", Ddr4 ? "OdtWr\tOdtNom\tOdtPark" : Ddr5 ? "OdtWr\tNomWr\tNomRd\tOdtPark" : "OdtDQ\tOdtCA");
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "--------------------------------------------------------\n");
  for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
    for (Channel = 0; Channel < Outputs->MaxChannels; Channel++) {
      CpuOdt[Channel] = 0;
      if (!(MC_CH_MASK_CHECK(Outputs->ValidChBitMask, Controller, Channel, Outputs->MaxChannels))) {
        continue;
      }
      ChannelOut = &Outputs->Controller[Controller].Channel[Channel];
      for (Dimm = 0; Dimm < MAX_DIMMS_IN_CHANNEL; Dimm++) {
        RankMask = DIMM_TO_RANK_MASK (Dimm);
        Rank = Dimm * 2;
        if ((RankMask & ChannelOut->ValidRankBitMask) == 0) {
          continue;
        }
        if (Ddr4) {
          OdtNom   = CalcDimmImpedance (MrcData, Controller, Channel, Rank, OptDimmOdtNom,  FALSE, 0, FALSE);
        }
        if (Ddr4 || Ddr5) {
          OdtPark  = CalcDimmImpedance (MrcData, Controller, Channel, Rank, OptDimmOdtPark, FALSE, 0, FALSE);
        }
        if (Ddr5) {
          OdtNomRd = CalcDimmImpedance (MrcData, Controller, Channel, Rank, OptDimmOdtNomRd, FALSE, 0, FALSE);
          OdtNomWr = CalcDimmImpedance (MrcData, Controller, Channel, Rank, OptDimmOdtNomWr, FALSE, 0, FALSE);
        }
        if (Outputs->Lpddr) { // @todo add DDR5
          OdtCA    = CalcDimmImpedance (MrcData, Controller, Channel, Rank, OptDimmOdtCA,   FALSE, 0, FALSE);
        }
        if (Outputs->Lpddr) {
          OdtCA *= 2;
        }
        OdtWrite = CalcDimmImpedance (MrcData, Controller, Channel, Rank, OptDimmOdtWr,   FALSE, 0, FALSE);
        DimmRon  = CalcDimmImpedance (MrcData, Controller, Channel, Rank, OptDimmRon,     FALSE, 0, FALSE);
        MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Mc%dC%dD%d:\t%d\t", Controller, Channel, Dimm, DimmRon);
        if ((OdtWrite == 0xFFFF) || (OdtWrite == 0)) {
          MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, ((OdtWrite == 0xFFFF) && Ddr4) ? "Hi-Z\t" : "Off\t");
        } else {
          MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "%d\t", OdtWrite);
        }
        if (Ddr4) {
          MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, (OdtNom == 0xFFFF) ? "Hi-Z\t" : "%d\t", OdtNom);
          MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, (OdtPark == 0xFFFF) ? "Hi-Z\n" : "%d\n", OdtPark);
        } else if (Outputs->Lpddr){
          MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, (OdtCA == 0xFFFF) ? "Off\t" : "%d\t", OdtCA);
          MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\n");
        } else if (Ddr5) {
          MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, (OdtNomWr == 0xFFFF) ? "Off\t" : "%d\t", OdtNomWr);
          MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, (OdtNomRd == 0xFFFF) ? "Off\t" : "%d\t", OdtNomRd);
          MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, (OdtPark == 0xFFFF) ? "Off\n" : "%d\n", OdtPark);
        }
      } // for Dimm
      MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "--------------------------------------------------------\n");
      Status = CalcRxDqOdtAverageByteImpedance (MrcData, (UINT8)Controller, (UINT8)Channel, &CpuOdt[Channel]);
      if (mrcSuccess != Status) {
        MRC_DEBUG_MSG (Debug, MSG_LEVEL_ERROR, "CalcRxDqOdtAverageByteImpedance() error %d\n", Status);
      }
    } // for Channel

    for (Channel = 0; Channel < Outputs->MaxChannels; Channel++) {
      if (!(MC_CH_MASK_CHECK(Outputs->ValidChBitMask, Controller, Channel, Outputs->MaxChannels))) {
        continue;
      }
      MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "CPU Summary: MC = %d Channel = %d Read ODT = %d\n", Controller, Channel, CpuOdt[Channel]);
    }
  } // for Controller

#endif // MRC_DEBUG_PRINT
}

/**
  Wrapper for DQTimeCentering1D taking duty cycle into account.

  @param[in,out] MrcData        - Include all MRC global data.
  @param[in]     ResetPerBit    - Option to Reset PerBit Deskew to middle value before byte training
  @param[in]     StepSize       - Step size
  @param[in]     loopcount      - loop count
  @param[in]     MsgPrint       - Show debug prints
  @param[in]     EarlyCentering - Execute as early centering routine

  @retval MrcStatus -  If succeeded, return mrcSuccess
**/
MrcStatus
DQTimeCentering1D_RxDC (
  IN OUT MrcParameters *const MrcData,
  IN     const UINT8          ResetPerBit,
  IN     const UINT8          StepSize,
  IN     const UINT8          loopcount,
  IN     BOOLEAN              MsgPrint,
  IN     BOOLEAN              EarlyCentering
  )
{
 // DQTimeCentering1D (MrcData, RdTP, ResetPerBit, StepSize, loopcount, MsgPrint, EarlyCentering, 1);
 //       MrcSetDefaultRxVrefDdr4 (MrcData,TRUE, FALSE, FALSE);
 // DQTimeCentering1D (MrcData, RdTN, ResetPerBit, StepSize, loopcount, MsgPrint, EarlyCentering, 1);
 //
 //     MrcSetDefaultRxVrefDdr4 (MrcData, TRUE, FALSE, FALSE);
 //   DQTimeCentering1D (MrcData, WrV, 0,0, 10, FALSE, FALSE, 1);
 //   DQTimeCentering1D (MrcData, RdV, 0,0, 10, FALSE, FALSE, 1);
 //     DQTimeCentering1D (MrcData, WrTLp4, 0,0, 10, FALSE, FALSE, 1);
 //     DQTimeCentering1D (MrcData, WrT, 0,0, 10, FALSE, FALSE, 1);
 //   DQTimeCentering1D (MrcData, RdT, 0,0, 10, FALSE, FALSE, 1);
  return mrcSuccess;
}

/**
This function made for Matrix Convolution, Get an Matrix,
perform Convolution using Convolution matrix and update result matrix

@param[in]  arr       - Input array
@param[out] newarr    - Output array
@param[in]  numRows   - Number of rows in input/output arrays
@param[in]  numCols   - Number of columns in input/output arrays
@param[in]  colsUsed  - Number of columns that are actually used
@param[in]  lpfarr    - Convolution kernel matrix array
@param[in]  size      - Number of columns and rows in convolution kernel matrix

@retval None
**/
void
MrcConvolution2D (
  UINT32    *arr,
  UINT32    *newarr,
  UINT16    numRows,
  UINT16    numCols,
  UINT16    colsUsed,
  UINT8     *lpfarr,
  UINT8     size
  )
{
  INT16   outRow;
  INT16   outCol;
  INT16   kernelRow;
  INT16   kernelCol;
  INT16   inRow;
  INT16   inCol;
  INT16   center;
  UINT32  weight;

  center = (INT16) ((size - 1) / 2);
  for (outRow = 0; outRow < numRows; outRow++) {
    for (outCol = 0; outCol < colsUsed; outCol++) {
      weight = 0;
      newarr[outCol + (outRow * numCols)] = 0;
      for (kernelRow = 0; kernelRow < size; kernelRow++) {
        for (kernelCol = 0; kernelCol < size; kernelCol++) {
          inRow = outRow - center + kernelRow;
          inCol = outCol - center + kernelCol;
          if ((inRow < 0) || (inRow >= numRows) || (inCol < 0) || (inCol >= colsUsed))
          {
            continue;
          }
          weight += lpfarr[kernelCol + (kernelRow * size)];
          newarr[outCol + (outRow * numCols)] += (arr[inCol + (inRow * numCols)] * lpfarr[kernelCol + (kernelRow * size)]);
        }
      }
      newarr[outCol + (outRow * numCols)] = newarr[outCol + (outRow * numCols)] / weight;
    }
  }}

/**
  This function wrap DimmODTCATraining routine.
  This step is for LPDDR only.
  We don't train 2D with CA DS because the training time would be too long, and we don't need the margin that badly.

  @param[in] MrcData  - Include all MRC global data.

  @retval MrcStatus - if it succeed return mrcSuccess
**/
MrcStatus
MrcDimmOdtCaTraining (
  IN MrcParameters *const MrcData
  )
{
  UINT16              SaveMarginsArray[2][MAX_OPT_POINTS][MAX_CONTROLLER][MAX_BYTE_IN_DDR5_CHANNEL * MAX_DIMMS_IN_CHANNEL]; // Must match test list size
  static const UINT8  TestList[] = { CmdV, CmdT };
  static const UINT8  OptParam[] = { OptDimmOdtCA };
  UINT8               Scale[] = { 1, 1, 0, 0, 0 };
  MrcOutput           *Outputs;
  UINT16              *DimmCaOdtVals;
  OptOffsetChByte     BestOff;
  INT8                Start;
  INT8                Stop;
  INT8                Off;

  Outputs = &MrcData->Outputs;

  if (!Outputs->Lpddr) {
    return mrcSuccess;
  }

  GetDimmOptParamValues (MrcData, OptParam[0], &DimmCaOdtVals, (UINT8 *) &Stop);
  Start = 0;
  Stop  = Stop - 1;

  MRC_DEBUG_MSG (&Outputs->Debug, MSG_LEVEL_NOTE, "\nDIMM CaOdt Offset - Value mapping \nOffset\t Value\n");
  for (Off = Start; Off < Stop + 1; Off++) {
    MRC_DEBUG_MSG (&Outputs->Debug, MSG_LEVEL_NOTE, "%d:\t %u \n", Off, DimmCaOdtVals[(UINT8)Off]);
  }

  // ADL_POWER_TRAINING_DDR5 - if DDR5 is run in 2DPC, you may want to call this per DIMM and set CaVref per DIMM
  // Train CA ODT only
  TrainDDROptParam (
    MrcData,
    &BestOff,
    MrcData->Outputs.ValidChBitMask,
    MrcData->Outputs.ValidRankMask,
    OptParam,
    ARRAY_COUNT(OptParam),
    FullGrid,
    TestList,
    ARRAY_COUNT(TestList),
    Scale,
    NULL,
    &Start,  // Start
    &Stop,  // Stop
    OPT_PARAM_LOOP_COUNT,
    1,      // Repeats
    0,      // NoPrint
    0,      // SkipOdtUpdate
    0,      // GuardBand
    0,       // PatternType
    SaveMarginsArray,
    FALSE, // UPMFilterEnable
    NULL,  // TestUpmArray
    FALSE  // CliffsFilterEnable
    );

  MRC_DEBUG_MSG (&Outputs->Debug, MSG_LEVEL_NOTE, "Re-center Cmd Vref\n");
  CmdVoltageCentering(MrcData, OPT_PARAM_LOOP_COUNT, V_FINAL_STEP_SIZE, MRC_PWR_DBG_PRINT);
  MrcResetSequence(MrcData);

  return mrcSuccess;
}

/**
  DIMM ODT training for DDR5

  @param[in] MrcData  - Include all MRC global data.

  @retval MrcStatus - if it succeed return mrcSuccess
**/
MrcStatus
DimmOdtTrainingDDR5 (
  IN MrcParameters *const MrcData,
  IN UINT8                OdtValidChannelMask
  )
{
  const MRC_FUNCTION          *MrcCall;
  MrcOutput                   *Outputs;
  MrcChannelOut               *ChannelOut;
  MrcInput                    *Inputs;
  MrcDebug                    *Debug;
  MrcDebugMsgLevel            DbgLevel;
  MrcStatus                   Status;
  UINT8                       Controller;
  UINT8                       Channel;
  UINT8                       Dimm;
  UINT8                       ParkIndex;
  static const UINT8          OdtPark40002R2RVals[MAX_ODT_PARK_VALS]    = {60, 48, 40};  //Both 2R2R/1R1R have same number of values per parameter to be able to run two controllers in 2R2R/1R1R mix mode
  static const UINT8          OdtPark40002R2RValsDoubleSize[MAX_ODT_PARK_VALS_DOUBLE_SIZE]    = {48, 60, 80, 120,240};
  static const UINT8          OdtPark40001R1RVals[MAX_ODT_PARK_VALS]    = {48, 40, 34};
  static const UINT8          OdtWr40002R2RVals[MAX_ODT_WR_VALS]        = {240, 120};
  static const UINT8          OdtWr40002R2RValsDoubleSize[MAX_ODT_WR_VALS_DOUBLE_SIZE]        = {240, 120, 80, 60};

  static const UINT8          OdtWr40001R1RVals[MAX_ODT_WR_VALS]        = {60 ,  48};
  static const UINT8          OdtNomWr40002R2RVals[MAX_ODT_NOM_WR_VALS] = {80, 60, 48, 40};
  static const UINT8          OdtNomWr40002R2RValsDoubleSize[MAX_ODT_NOM_WR_VALS_DOUBLE_SIZE] = {48, 60, 80, 120};

  static const UINT8          OdtNomWr40001R1RVals[MAX_ODT_NOM_WR_VALS] = {60, 48, 40, 34};
  static const UINT8          OdtNomRd40002R2RVals[MAX_ODT_NOM_RD_VALS] = {60, 48, 40};
  static const UINT8          OdtNomRd40002R2RValsDoubleSize[MAX_ODT_NOM_RD_VALS_DOUBLE_SIZE] = {48, 60, 80, 120};
  static const UINT8          OdtNomRd40001R1RVals[MAX_ODT_NOM_RD_VALS] = {48, 40, 34};

  static const UINT8          OdtPark20002R2RVals[MAX_ODT_PARK_VALS]    = {240, 120, 80};
  static const UINT8          OdtPark20001R1RVals[MAX_ODT_PARK_VALS]    = { 48,  48, 48};
  static const UINT8          OdtWr20002R2RVals[MAX_ODT_WR_VALS]        = {80, 60};
  static const UINT8          OdtWr20001R1RVals[MAX_ODT_WR_VALS]        = {80, 60};
  static const UINT8          OdtNomWr20002R2RVals[MAX_ODT_NOM_WR_VALS] = {120, 80, 60, 48};
  static const UINT8          OdtNomWr20001R1RVals[MAX_ODT_NOM_WR_VALS] = {60, 48, 40, 34};
  static const UINT8          OdtNomRd20002R2RVals[MAX_ODT_NOM_RD_VALS] = {60, 48, 40};
  static const UINT8          OdtNomRd20001R1RVals[MAX_ODT_NOM_RD_VALS] = {48, 40, 34};

  UINT8                       OdtPark2R2RVals[MAX_ODT_PARK_VALS];
  UINT8                       OdtPark2R2RValsDoubleSize[MAX_ODT_PARK_VALS_DOUBLE_SIZE];
  UINT8                       *OdtPark2R2RValsPointer  = NULL;
  UINT8                       OdtPark1R1RVals[MAX_ODT_PARK_VALS];
  UINT8                       OdtWr2R2RVals[MAX_ODT_WR_VALS];
  UINT8                       OdtWr2R2RValsDoubleSize[MAX_ODT_WR_VALS_DOUBLE_SIZE];
  UINT8                       *OdtWr2R2RValsPointer = NULL;
  UINT8                       OdtWr1R1RVals[MAX_ODT_WR_VALS];
  UINT8                       OdtNomWr2R2RVals[MAX_ODT_NOM_WR_VALS];
  UINT8                       OdtNomWr2R2RValsDoubleSize[MAX_ODT_NOM_WR_VALS_DOUBLE_SIZE];
  UINT8                       *OdtNomWr2R2RValsPointer = NULL;
  UINT8                       OdtNomWr1R1RVals[MAX_ODT_NOM_WR_VALS];
  UINT8                       OdtNomRd2R2RVals[MAX_ODT_NOM_RD_VALS];
  UINT8                       OdtNomRd2R2RValsDoubleSize[MAX_ODT_NOM_RD_VALS_DOUBLE_SIZE];
  UINT8                       *OdtNomRd2R2RValsPointer = NULL;
  UINT8                       OdtNomRd1R1RVals[MAX_ODT_NOM_RD_VALS];

  UINT16                      WrTestResults[MAX_CONTROLLER][MAX_CHANNEL][MAX_ODT_PARK_VALS + DEFAULT_TEST][WR_TABLE_COLS];
  UINT16                      RdTestResults[MAX_CONTROLLER][MAX_CHANNEL][MAX_ODT_PARK_VALS + DEFAULT_TEST][RD_TABLE_COLS];
  UINT16                      WrRdTestResults[MAX_CONTROLLER][MAX_CHANNEL][MAX_ODT_PARK_VALS + DEFAULT_TEST][FINAL_TABLE_COLS];
  UINT16                      BestScore[MAX_CONTROLLER][MAX_CHANNEL];
  UINT8                       BestIndex[MAX_CONTROLLER][MAX_CHANNEL];
  UINT8                       FinalIndex;
  UINT8                       LC = 10;
  UINT8                       ResetPerBit;
  UINT8                       RepeatFactor;
  UINT32                      TestScore;
  UINT8                       ParkValue;
  BOOLEAN                     DebugPrints;
  DDR5_MODE_REGISTER_33_TYPE  MR33;
  DDR5_MODE_REGISTER_34_TYPE  MR34;
  DDR5_MODE_REGISTER_35_TYPE  MR35;
  MrcModeRegister             MpcMr33Num;
  MrcModeRegister             Mr34Num;
  MrcModeRegister             Mr35Num;
  UINT32                      MrIndex;
  UINT16                      *MrReg;
  BOOLEAN                     TestDefaultValue;
  BOOLEAN                     Is2R2R;
  UINT16                      DefaultRttPark[MAX_CONTROLLER][MAX_CHANNEL][MAX_DIMMS_IN_CHANNEL];
  UINT16                      DefaultRttParkDqs[MAX_CONTROLLER][MAX_CHANNEL][MAX_DIMMS_IN_CHANNEL];
  UINT16                      DefaultRttWr[MAX_CONTROLLER][MAX_CHANNEL][MAX_DIMMS_IN_CHANNEL];
  UINT16                      DefaultRttNomWr[MAX_CONTROLLER][MAX_CHANNEL][MAX_DIMMS_IN_CHANNEL];
  UINT16                      DefaultRttNomRd[MAX_CONTROLLER][MAX_CHANNEL][MAX_DIMMS_IN_CHANNEL];
  UINT16                      TotalScoresResult[MAX_CONTROLLER][MAX_CHANNEL][MAX_SDRAM_IN_DIMM][MAX_EDGES];
  BOOLEAN                     DoubleSizeDimm;
  UINT8                       MaxOdtParkVals = 0;
  BOOLEAN                     OdtRangeChange = FALSE;
  UINT8                       MaxOdtNomRdVals = 0;
  UINT8                       MaxOdtWrVals = 0;
  UINT8                       MaxOdtNomWrVals = 0;


  Status  = mrcSuccess;
  Outputs = &MrcData->Outputs;
  Inputs  = &MrcData->Inputs;
  MrcCall = Inputs->Call.Func;
  Debug   = &Outputs->Debug;
  DebugPrints = TRUE;
  DbgLevel    = DebugPrints ? MSG_LEVEL_NOTE : MSG_LEVEL_NEVER;
  MpcMr33Num  = mpcMR33;
  Mr34Num     = mrMR34;
  Mr35Num     = mrMR35;
  ResetPerBit = 0;
  DoubleSizeDimm = Inputs->Ddr5DoubleSize2Dpc;

  if (DebugPrints) {
    MrcPrintDimmOdtValues (MrcData);
  }

  //Store default values in local array
  for (Dimm = 0; Dimm < MAX_DIMMS_IN_CHANNEL; Dimm++) {
    for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
      for (Channel = 0; Channel < MAX_CHANNEL; Channel++) {
        if (!MrcChannelExist (MrcData, Controller, Channel) || !MC_CH_MASK_CHECK (OdtValidChannelMask, Controller, Channel, MAX_CHANNEL)) {
          continue;
        }
        // MR values below are per DIMM, take them from Rank 0
        ChannelOut = &Outputs->Controller[Controller].Channel[Channel];
        MrReg = &ChannelOut->Dimm[Dimm].Rank[0].MR[0];

        MrIndex = MrcMrAddrToIndex (MrcData, &MpcMr33Num);
        MR33.Data8 = (UINT8) MrReg[MrIndex];
        DefaultRttParkDqs[Controller][Channel][Dimm] = (UINT16) OdtDecode (MR33.Data8 & 0x7);  // Host struct value is after DDR5_MPC_SET_DQS_RTT_PARK macro
        // MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "D%u MC%u C%u MR33.Data8 = 0x%x, DefaultRttParkDqs = %d\n", Dimm, Controller, Channel, MR33.Data8, DefaultRttParkDqs[Controller][Channel][Dimm]);

        MrIndex = MrcMrAddrToIndex (MrcData, &Mr34Num);
        MR34.Data8 = (UINT8) MrReg[MrIndex];
        DefaultRttWr[Controller][Channel][Dimm]   = (UINT16) OdtDecode (MR34.Bits.RttWr);
        DefaultRttPark[Controller][Channel][Dimm] = (UINT16) OdtDecode (MR34.Bits.RttPark);
        // MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "D%u MC%u C%u MR34.Data8 = 0x%x, DefaultRttWr = %d, DefaultRttPark = %d\n", Dimm, Controller, Channel, MR34.Data8, DefaultRttWr[Controller][Channel][Dimm], DefaultRttPark[Controller][Channel][Dimm]);

        MrIndex = MrcMrAddrToIndex (MrcData, &Mr35Num);
        MR35.Data8 = (UINT8) MrReg[MrIndex];
        DefaultRttNomWr[Controller][Channel][Dimm] = (UINT16) OdtDecode (MR35.Bits.RttNomWr);
        DefaultRttNomRd[Controller][Channel][Dimm] = (UINT16) OdtDecode (MR35.Bits.RttNomRd);
        // MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "D%u MC%u C%u MR35.Data8 = 0x%x, DefaultRttNomWr = %d, DefaultRttNomRd = %d\n", Dimm, Controller, Channel, MR34.Data8, DefaultRttNomWr[Controller][Channel][Dimm], DefaultRttNomRd[Controller][Channel][Dimm]);
      } // Channel
    } // Controller
  } // Dimm

  if (Outputs->Frequency >= 3200) { // High frequency odt scan values
    RepeatFactor = 103;
    if (DoubleSizeDimm) {
      MrcCall->MrcCopyMem ((UINT8 *) &OdtPark2R2RValsDoubleSize,  (UINT8 *) &OdtPark40002R2RValsDoubleSize,  sizeof (OdtPark40002R2RValsDoubleSize));
      OdtPark2R2RValsPointer = OdtPark2R2RValsDoubleSize;
      OdtRangeChange = TRUE;
    } else {
      MrcCall->MrcCopyMem ((UINT8 *) &OdtPark2R2RVals,  (UINT8 *) &OdtPark40002R2RVals,  sizeof (OdtPark40002R2RVals));
      OdtPark2R2RValsPointer = OdtPark2R2RVals;
    }

    MrcCall->MrcCopyMem ((UINT8 *) &OdtPark1R1RVals,  (UINT8 *) &OdtPark40001R1RVals,  sizeof (OdtPark40001R1RVals));
    if (DoubleSizeDimm) {
      MrcCall->MrcCopyMem ((UINT8 *) &OdtWr2R2RValsDoubleSize,    (UINT8 *) &OdtWr40002R2RValsDoubleSize,    sizeof (OdtWr40002R2RValsDoubleSize));
      OdtWr2R2RValsPointer = OdtWr2R2RValsDoubleSize;
      OdtRangeChange = TRUE;
    } else {
      MrcCall->MrcCopyMem ((UINT8 *) &OdtWr2R2RVals,    (UINT8 *) &OdtWr40002R2RVals,    sizeof (OdtWr40002R2RVals));
      OdtWr2R2RValsPointer = OdtWr2R2RVals;
    }

    MrcCall->MrcCopyMem ((UINT8 *) &OdtWr1R1RVals,    (UINT8 *) &OdtWr40001R1RVals,    sizeof (OdtWr40001R1RVals));

    if (DoubleSizeDimm) {
      MrcCall->MrcCopyMem ((UINT8 *) &OdtNomWr2R2RValsDoubleSize, (UINT8 *) &OdtNomWr40002R2RValsDoubleSize, sizeof (OdtNomWr40002R2RValsDoubleSize));
      OdtNomWr2R2RValsPointer = OdtNomWr2R2RValsDoubleSize;
      OdtRangeChange = TRUE;
    } else {
      MrcCall->MrcCopyMem ((UINT8 *) &OdtNomWr2R2RVals, (UINT8 *) &OdtNomWr40002R2RVals, sizeof (OdtNomWr40002R2RVals));
      OdtNomWr2R2RValsPointer = OdtNomWr2R2RVals;
    }

    MrcCall->MrcCopyMem ((UINT8 *) &OdtNomWr1R1RVals, (UINT8 *) &OdtNomWr40001R1RVals, sizeof (OdtNomWr40001R1RVals));

    if (DoubleSizeDimm) {
      MrcCall->MrcCopyMem ((UINT8 *) &OdtNomRd2R2RValsDoubleSize, (UINT8 *) &OdtNomRd40002R2RValsDoubleSize, sizeof (OdtNomRd40002R2RValsDoubleSize));
      OdtNomRd2R2RValsPointer = OdtNomRd2R2RValsDoubleSize;
      OdtRangeChange = TRUE;
    } else {
      MrcCall->MrcCopyMem ((UINT8 *) &OdtNomRd2R2RVals, (UINT8 *) &OdtNomRd40002R2RVals, sizeof (OdtNomRd40002R2RVals));
      OdtNomRd2R2RValsPointer = OdtNomRd2R2RVals;
    }

    MrcCall->MrcCopyMem ((UINT8 *) &OdtNomRd1R1RVals, (UINT8 *) &OdtNomRd40001R1RVals, sizeof (OdtNomRd40001R1RVals));
  } else { // Low frequency odt scan values
    RepeatFactor = 100; // Defaults are for high frequency
    MrcCall->MrcCopyMem ((UINT8 *) &OdtPark2R2RVals,  (UINT8 *) &OdtPark20002R2RVals,  sizeof (OdtPark20002R2RVals));
    OdtPark2R2RValsPointer = OdtPark2R2RVals;

    MrcCall->MrcCopyMem ((UINT8 *) &OdtPark1R1RVals,  (UINT8 *) &OdtPark20001R1RVals,  sizeof (OdtPark20001R1RVals));
    MrcCall->MrcCopyMem ((UINT8 *) &OdtWr2R2RVals,    (UINT8 *) &OdtWr20002R2RVals,    sizeof (OdtWr20002R2RVals));
    OdtWr2R2RValsPointer = OdtWr2R2RVals;
    MrcCall->MrcCopyMem ((UINT8 *) &OdtWr1R1RVals,    (UINT8 *) &OdtWr20001R1RVals,    sizeof (OdtWr20001R1RVals));
    MrcCall->MrcCopyMem ((UINT8 *) &OdtNomWr2R2RVals, (UINT8 *) &OdtNomWr20002R2RVals, sizeof (OdtNomWr20002R2RVals));
    OdtNomWr2R2RValsPointer = OdtNomWr2R2RVals;
    MrcCall->MrcCopyMem ((UINT8 *) &OdtNomWr1R1RVals, (UINT8 *) &OdtNomWr20001R1RVals, sizeof (OdtNomWr20001R1RVals));
    MrcCall->MrcCopyMem ((UINT8 *) &OdtNomRd2R2RVals, (UINT8 *) &OdtNomRd20002R2RVals, sizeof (OdtNomRd20002R2RVals));
    OdtNomRd2R2RValsPointer = OdtNomRd2R2RVals;

    MrcCall->MrcCopyMem ((UINT8 *) &OdtNomRd1R1RVals, (UINT8 *) &OdtNomRd20001R1RVals, sizeof (OdtNomRd20001R1RVals));
  }
  MRC_DEBUG_MSG (Debug, DbgLevel, "OdtRangeChange: %d\n", OdtRangeChange);

  // First train DIMM1, then DIMM0, as DIMM1 is the far DIMM in daisy chain layout and has worse margins
  for (Dimm = 1; Dimm != 0xFF; Dimm--) {
    //WRITE TEST

    //Print Wr full grid headlines
    if (DebugPrints) {
      MRC_DEBUG_MSG (Debug, DbgLevel, "<BEGIN PARSE> Dimm%d %s flow\n", Dimm, "Write");
      MRC_DEBUG_MSG (Debug, DbgLevel, "<METADATA> Headings_Level1: ");
      for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
        for (Channel = 0; Channel < MAX_CHANNEL; Channel++) {
          if (!MrcChannelExist (MrcData, Controller, Channel) || !MC_CH_MASK_CHECK (OdtValidChannelMask, Controller, Channel, MAX_CHANNEL) ) {
            continue;
          }
          MRC_DEBUG_MSG (Debug, DbgLevel, " MC%dC%d,", Controller, Channel);
        }
      }
      MRC_DEBUG_MSG (Debug, DbgLevel, "\n<METADATA> Heading_Level2: Park, Wr, NomWr, WrT, WrV, Score\n");
      MRC_DEBUG_MSG (Debug, DbgLevel, "<METADATA> Heading_Level2_Aliases: Park_DIMM%d, Wr_Dimm%d, NomWr_Dimm%d, WrT, WrV, Score\n", Dimm, Dimm, (Dimm + 1) % 2);
      MRC_DEBUG_MSG (Debug, DbgLevel, "\nDIMM%d Wr Full Grid: Park_DIMM%d,Wr_DIMM%d,NomWr_Dimm%d\n", Dimm, Dimm, Dimm, (Dimm + 1) % 2);
      for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
        for (Channel = 0; Channel < MAX_CHANNEL; Channel++) {
          if (!MrcChannelExist (MrcData, Controller, Channel) || !MC_CH_MASK_CHECK (OdtValidChannelMask, Controller, Channel, MAX_CHANNEL)) {
            continue;
          }
          MRC_DEBUG_MSG (Debug, DbgLevel, "Mc%d C%d\t\t\t\t\t\t\t", Controller, Channel);
        }
      }
      MRC_DEBUG_MSG (Debug, DbgLevel, "\n");
      for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
        for (Channel = 0; Channel < MAX_CHANNEL; Channel++) {
          if (!MrcChannelExist (MrcData, Controller, Channel) || !MC_CH_MASK_CHECK (OdtValidChannelMask, Controller, Channel, MAX_CHANNEL)) {
            continue;
          }
          MRC_DEBUG_MSG (Debug, DbgLevel, "Park\tWr\tNomWr\tWrT\tWrV\tScore\t\t");
        }
      }
      MRC_DEBUG_MSG (Debug, DbgLevel, "\n");
    }

    MrcCall->MrcSetMem ((UINT8 *)WrTestResults, sizeof (WrTestResults), 0);
    if (OdtRangeChange) {
      MaxOdtParkVals = MAX_ODT_PARK_VALS_DOUBLE_SIZE;
    } else {
      MaxOdtParkVals = MAX_ODT_PARK_VALS;
    }
    for (ParkIndex = 0; ParkIndex < (MaxOdtParkVals + DEFAULT_TEST); ParkIndex++) {
      TestDefaultValue = (MaxOdtParkVals == ParkIndex);

      if (!TestDefaultValue) {
        for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
          for (Channel = 0; Channel < MAX_CHANNEL; Channel++) {
            if (!MrcChannelExist (MrcData, Controller, Channel) || !MC_CH_MASK_CHECK (OdtValidChannelMask, Controller, Channel, MAX_CHANNEL)) {
              continue;
            }
            ChannelOut = &Outputs->Controller[Controller].Channel[Channel];
            //Set 1st odt value
            ParkValue = (ChannelOut->ValidRankBitMask == R0R1R2R3_MASK) ? OdtPark2R2RValsPointer[ParkIndex] : OdtPark1R1RVals[ParkIndex];
            SetDimmParamValue (MrcData, Controller, Channel, (R0R1_MASK << (Dimm * 2)), OptDimmOdtPark,    ParkValue, FALSE);
            SetDimmParamValue (MrcData, Controller, Channel, (R0R1_MASK << (Dimm * 2)), OptDimmOdtParkDqs, ParkValue, FALSE);
          }// Channel
        }// Controller

        //Center RD/WR voltage per odt Park change
        MrcSetDefaultRxVrefDdr5 (MrcData, FALSE, MRC_PRINTS_OFF, FALSE);//Safe vref point for read
        DQTimeCentering1D (MrcData, WrV, ResetPerBit, LC, MRC_PRINTS_OFF, FALSE, 2);
        DQTimeCentering1D (MrcData, RdV, ResetPerBit, LC, MRC_PRINTS_OFF, FALSE, 3);
      }// if (!TestDefaultValue)

      if (OdtRangeChange) {
        MaxOdtWrVals = MAX_ODT_WR_VALS_DOUBLE_SIZE;
      } else {
        MaxOdtWrVals = MAX_ODT_WR_VALS;
      }

      for (UINT8 WrIndex = 0; WrIndex < MaxOdtWrVals; WrIndex++) {
        if (!TestDefaultValue) {
          for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
            for (Channel = 0; Channel < MAX_CHANNEL; Channel++) {
              if (!MrcChannelExist (MrcData, Controller, Channel) || !MC_CH_MASK_CHECK (OdtValidChannelMask, Controller, Channel, MAX_CHANNEL)) {
                continue;
              }
              ChannelOut = &Outputs->Controller[Controller].Channel[Channel];
              //Set 2st odt value
              SetDimmParamValue (MrcData, Controller, Channel, (R0R1_MASK << (Dimm * 2)), OptDimmOdtWr,
                (ChannelOut->ValidRankBitMask == R0R1R2R3_MASK) ? OdtWr2R2RValsPointer[WrIndex] : OdtWr1R1RVals[WrIndex], FALSE);
            }// Channel
          }// Controller
        }// if (!TestDefaultValue)

        if (OdtRangeChange) {
          MaxOdtNomWrVals = MAX_ODT_NOM_WR_VALS_DOUBLE_SIZE;
        } else {
          MaxOdtNomWrVals = MAX_ODT_NOM_WR_VALS;
        }

        for (UINT8 NomWrIndex = 0; NomWrIndex < MaxOdtNomWrVals; NomWrIndex++) {
          if (!TestDefaultValue) {
            for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
              for (Channel = 0; Channel < MAX_CHANNEL; Channel++) {
                if (!MrcChannelExist (MrcData, Controller, Channel) || !MC_CH_MASK_CHECK (OdtValidChannelMask, Controller, Channel, MAX_CHANNEL)) {
                  continue;
                }
                ChannelOut = &Outputs->Controller[Controller].Channel[Channel];
                //Set 3rd odt value
                SetDimmParamValue (MrcData, Controller, Channel, (R2R3_MASK >> (Dimm * 2)), OptDimmOdtNomWr,
                  (ChannelOut->ValidRankBitMask == R0R1R2R3_MASK) ? OdtNomWr2R2RValsPointer[NomWrIndex] : OdtNomWr1R1RVals[NomWrIndex], FALSE);
              }// Channel
            }// Controller
            //Center WR voltage per Wr change
            DQTimeCentering1D (MrcData, WrV, ResetPerBit, LC, MRC_PRINTS_OFF, FALSE, 2);
          } else {// !TestDefaultValue: Override values to host defaults
            for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
              for (Channel = 0; Channel < MAX_CHANNEL; Channel++) {
                if (!MrcChannelExist (MrcData, Controller, Channel) || !MC_CH_MASK_CHECK (OdtValidChannelMask, Controller, Channel, MAX_CHANNEL)) {
                  continue;
                }
                SetDimmParamValue (MrcData, Controller, Channel, (R0R1_MASK << (Dimm * 2)), OptDimmOdtPark,    DefaultRttPark[Controller][Channel][Dimm],    FALSE);
                SetDimmParamValue (MrcData, Controller, Channel, (R0R1_MASK << (Dimm * 2)), OptDimmOdtParkDqs, DefaultRttParkDqs[Controller][Channel][Dimm], FALSE);
                SetDimmParamValue (MrcData, Controller, Channel, (R0R1_MASK << (Dimm * 2)), OptDimmOdtWr,      DefaultRttWr[Controller][Channel][Dimm],      FALSE);
                SetDimmParamValue (MrcData, Controller, Channel, (R2R3_MASK >> (Dimm * 2)), OptDimmOdtNomWr,   DefaultRttNomWr[Controller][Channel][Dimm],   FALSE);
              }// Channel
            }// Controller
            //RD/WR Voltage Centering after restoring host ctl/ch/dimm default values for OptDimmOdtPark/OptDimmOdtWr/OptDimmOdtNomWr
            MrcSetDefaultRxVrefDdr5 (MrcData, FALSE, MRC_PRINTS_OFF, FALSE);
            DQTimeCentering1D (MrcData, WrV, ResetPerBit, LC, MRC_PRINTS_OFF, FALSE, 2);
            DQTimeCentering1D (MrcData, RdV, ResetPerBit, LC, MRC_PRINTS_OFF, FALSE, 3);
          } // if (TestDefaultValue)

          //Run test
          DimmOdtRunTest (MrcData, OdtValidChannelMask, 1 << Dimm, TEST_WR, TotalScoresResult);

          for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
            for (Channel = 0; Channel < MAX_CHANNEL; Channel++) {
              if (!MrcChannelExist (MrcData, Controller, Channel) || !MC_CH_MASK_CHECK (OdtValidChannelMask, Controller, Channel, MAX_CHANNEL)) {
                continue;
              }
              ChannelOut = &Outputs->Controller[Controller].Channel[Channel];
              Is2R2R = (ChannelOut->ValidRankBitMask == R0R1R2R3_MASK);
              //Print Wr full grid results tables
              MRC_DEBUG_MSG (Debug, DbgLevel, "%d\t%d\t%d\t%d\t%d\t%d\t\t",
                (TestDefaultValue) ? DefaultRttPark[Controller][Channel][Dimm] : (Is2R2R ? OdtPark2R2RValsPointer[ParkIndex]   : OdtPark1R1RVals[ParkIndex]),
                (TestDefaultValue) ? DefaultRttWr[Controller][Channel][Dimm]   : (Is2R2R ? OdtWr2R2RValsPointer[WrIndex]       : OdtWr1R1RVals[WrIndex]),
                (TestDefaultValue) ? DefaultRttNomWr[Controller][Channel][Dimm]: (Is2R2R ? OdtNomWr2R2RValsPointer[NomWrIndex] : OdtNomWr1R1RVals[NomWrIndex]),
                Outputs->MarginResult[LastTxT][0][Controller][Channel][0][0],
                Outputs->MarginResult[LastTxV][0][Controller][Channel][0][0],
                TotalScoresResult[Controller][Channel][0][0]
              );

              TestScore = TotalScoresResult[Controller][Channel][0][0];

              if (TestDefaultValue) {
                WrTestResults[Controller][Channel][ParkIndex][0] = DefaultRttPark[Controller][Channel][Dimm];
                WrTestResults[Controller][Channel][ParkIndex][1] = DefaultRttWr[Controller][Channel][Dimm] ;
                WrTestResults[Controller][Channel][ParkIndex][2] = DefaultRttNomWr[Controller][Channel][Dimm];
                WrTestResults[Controller][Channel][ParkIndex][3] = Outputs->MarginResult[LastTxT][0][Controller][Channel][0][0];
                WrTestResults[Controller][Channel][ParkIndex][4] = Outputs->MarginResult[LastTxV][0][Controller][Channel][0][0];
                WrTestResults[Controller][Channel][ParkIndex][5] = (UINT16)(TestScore * RepeatFactor / 100 );//+3% score for default value
                //Set indexes to max to end test
                WrIndex = MaxOdtWrVals;
                NomWrIndex = MaxOdtNomWrVals;
              } else if ((UINT16)TestScore >= WrTestResults[Controller][Channel][ParkIndex][5]) {//Save best value per Park odt
                WrTestResults[Controller][Channel][ParkIndex][0] = Is2R2R ? OdtPark2R2RValsPointer[ParkIndex]   : OdtPark1R1RVals[ParkIndex];
                WrTestResults[Controller][Channel][ParkIndex][1] = Is2R2R ? OdtWr2R2RValsPointer[WrIndex]       : OdtWr1R1RVals[WrIndex];
                WrTestResults[Controller][Channel][ParkIndex][2] = Is2R2R ? OdtNomWr2R2RValsPointer[NomWrIndex] : OdtNomWr1R1RVals[NomWrIndex];
                WrTestResults[Controller][Channel][ParkIndex][3] = Outputs->MarginResult[LastTxT][0][Controller][Channel][0][0];
                WrTestResults[Controller][Channel][ParkIndex][4] = Outputs->MarginResult[LastTxV][0][Controller][Channel][0][0];
                WrTestResults[Controller][Channel][ParkIndex][5] = (UINT16)TestScore;
              }// TestDefaultValue
            }// Channel
          }// Controller
          MRC_DEBUG_MSG (Debug, DbgLevel, "\n");
        }// Odt2Index
      }// Odt1Index
      //Restore OptDimmOdtWr/OptDimmOdtNomWr to default/good value so tx centering will pass/fail based on park value only
      //Resulting same rx centering value for all odt values under same odtpark
      for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
        for (Channel = 0; Channel < MAX_CHANNEL; Channel++) {
          if (!MrcChannelExist (MrcData, Controller, Channel) || !MC_CH_MASK_CHECK (OdtValidChannelMask, Controller, Channel, MAX_CHANNEL)) {
            continue;
          }
          SetDimmParamValue (MrcData, Controller, Channel, (R0R1_MASK << (Dimm * 2)), OptDimmOdtWr,    DefaultRttWr[Controller][Channel][Dimm],    FALSE);
          SetDimmParamValue (MrcData, Controller, Channel, (R2R3_MASK >> (Dimm * 2)), OptDimmOdtNomWr, DefaultRttNomWr[Controller][Channel][Dimm], FALSE);
        } // Channel
      } // Controller
    } // ParkIndex
    MRC_DEBUG_MSG (Debug, DbgLevel, "<END PARSE> Dimm%d %s flow\n", Dimm, "Write");

    //Print Wr summary results
    if (DebugPrints) {
      MRC_DEBUG_MSG (Debug, DbgLevel, "\nDIMM%d Wr Summary: Park_DIMM%d,Wr_DIMM%d,NomWr_Dimm%d\n", Dimm, Dimm, Dimm, (Dimm + 1) % 2);
      for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
        for (Channel = 0; Channel < MAX_CHANNEL; Channel++) {
          if (!MrcChannelExist (MrcData, Controller, Channel) || !MC_CH_MASK_CHECK (OdtValidChannelMask, Controller, Channel, MAX_CHANNEL)) {
            continue;
          }
          MRC_DEBUG_MSG (Debug, DbgLevel, "Mc%d C%d\t\t\t\t\t\t\t", Controller, Channel);
        }// Channel
      }// Controller
      MRC_DEBUG_MSG (Debug, DbgLevel, "\n");
      for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
        for (Channel = 0; Channel < MAX_CHANNEL; Channel++) {
          if (!MrcChannelExist (MrcData, Controller, Channel) || !MC_CH_MASK_CHECK (OdtValidChannelMask, Controller, Channel, MAX_CHANNEL)) {
            continue;
          }
          MRC_DEBUG_MSG (Debug, DbgLevel, "Park\tWr\tNomWr\tWrT\tWrV\tScore\t\t");
        }// Channel
      }// Controller
      MRC_DEBUG_MSG (Debug, DbgLevel, "\n");
      for (ParkIndex = 0; ParkIndex < (MaxOdtParkVals + DEFAULT_TEST); ParkIndex++) {
        for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
          for (Channel = 0; Channel < MAX_CHANNEL; Channel++) {
            if (!MrcChannelExist (MrcData, Controller, Channel) || !MC_CH_MASK_CHECK (OdtValidChannelMask, Controller, Channel, MAX_CHANNEL)) {
              continue;
            }
            for (UINT8 ColIndex = 0; ColIndex < WR_TABLE_COLS; ColIndex++) {
              MRC_DEBUG_MSG (Debug, DbgLevel, "%d\t", WrTestResults[Controller][Channel][ParkIndex][ColIndex]);
            }// ColIndex
            MRC_DEBUG_MSG (Debug, DbgLevel, "\t");
          }// Channel
        }// Controller
        MRC_DEBUG_MSG (Debug, DbgLevel, "\n");
      } // ParkIndex
    } // DebugPrints

    //READ TEST

    //Print Rd full grid headlines
    if (DebugPrints) {
      MRC_DEBUG_MSG (Debug, DbgLevel, "<BEGIN PARSE> Dimm%d %s flow\n", Dimm, "Read");
      MRC_DEBUG_MSG (Debug, DbgLevel, "<METADATA> Headings_Level1: ");
      for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
        for (Channel = 0; Channel < MAX_CHANNEL; Channel++) {
          if (!MrcChannelExist (MrcData, Controller, Channel) || !MC_CH_MASK_CHECK (OdtValidChannelMask, Controller, Channel, MAX_CHANNEL) ) {
            continue;
          }
          MRC_DEBUG_MSG (Debug, DbgLevel, " MC%dC%d,", Controller, Channel);
        }
      }
      MRC_DEBUG_MSG (Debug, DbgLevel, "\n<METADATA> Heading_Level2: Park, NomRd, RdT, RdV, Score\n");
      MRC_DEBUG_MSG (Debug, DbgLevel, "<METADATA> Heading_Level2_Aliases: Park_DIMM%d, NomRd_Dimm%d, RdT, RdV, Score\n", Dimm, (Dimm + 1) % 2);
      MRC_DEBUG_MSG (Debug, DbgLevel, "\nDIMM%d Rd Full Grid: OdtPark_DIMM%d,OdtNomRd_Dimm%d\n", Dimm, Dimm, (Dimm + 1) % 2);
      for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
        for (Channel = 0; Channel < MAX_CHANNEL; Channel++) {
          if (!MrcChannelExist (MrcData, Controller, Channel) || !MC_CH_MASK_CHECK (OdtValidChannelMask, Controller, Channel, MAX_CHANNEL)) {
            continue;
          }
          MRC_DEBUG_MSG (Debug, DbgLevel, "Mc%d C%d\t\t\t\t\t\t", Controller, Channel);
        }// Channel
      }// Controller
      MRC_DEBUG_MSG (Debug, DbgLevel, "\n");
      for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
        for (Channel = 0; Channel < MAX_CHANNEL; Channel++) {
          if (!MrcChannelExist (MrcData, Controller, Channel) || !MC_CH_MASK_CHECK (OdtValidChannelMask, Controller, Channel, MAX_CHANNEL)) {
            continue;
          }
          MRC_DEBUG_MSG (Debug, DbgLevel, "Park\tNomRd\tRdT\tRdV\tScore\t\t");
        }// Channel
      }// Controller
      MRC_DEBUG_MSG (Debug, DbgLevel, "\n");
    }// if (DebugPrints)

    MrcCall->MrcSetMem ((UINT8 *)RdTestResults, sizeof (RdTestResults), 0);
    for (ParkIndex = 0; ParkIndex < (MaxOdtParkVals + DEFAULT_TEST); ParkIndex++) {
      TestDefaultValue = (MaxOdtParkVals == ParkIndex);

      if (!TestDefaultValue) {
        for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
          for (Channel = 0; Channel < MAX_CHANNEL; Channel++) {
            if (!MrcChannelExist (MrcData, Controller, Channel) || !MC_CH_MASK_CHECK (OdtValidChannelMask, Controller, Channel, MAX_CHANNEL)) {
              continue;
            }
            ChannelOut = &Outputs->Controller[Controller].Channel[Channel];
            //Set 1st odt value
            ParkValue = (ChannelOut->ValidRankBitMask == R0R1R2R3_MASK) ? OdtPark2R2RValsPointer[ParkIndex] : OdtPark1R1RVals[ParkIndex];
            SetDimmParamValue (MrcData, Controller, Channel, (R0R1_MASK << (Dimm * 2)), OptDimmOdtPark,    ParkValue, FALSE);
            SetDimmParamValue (MrcData, Controller, Channel, (R0R1_MASK << (Dimm * 2)), OptDimmOdtParkDqs, ParkValue, FALSE);
          }// Channel
        }// Controller

        //Center RD/WR voltage per park change
        MrcSetDefaultRxVrefDdr5 (MrcData, FALSE, MRC_PRINTS_OFF, FALSE);
        DQTimeCentering1D (MrcData, WrV, ResetPerBit, LC, MRC_PRINTS_OFF, FALSE, 2);
        DQTimeCentering1D (MrcData, RdV, ResetPerBit, LC, MRC_PRINTS_OFF, FALSE, 3);
      }// if (!TestDefaultValue)

      if (OdtRangeChange) {
        MaxOdtNomRdVals = MAX_ODT_NOM_RD_VALS_DOUBLE_SIZE;
      } else {
        MaxOdtNomRdVals = MAX_ODT_NOM_RD_VALS;
      }

      for (UINT8 NomRdIndex = 0; NomRdIndex < MaxOdtNomRdVals; NomRdIndex++) {
        if (!TestDefaultValue) {
          for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
            for (Channel = 0; Channel < MAX_CHANNEL; Channel++) {
              if (!MrcChannelExist (MrcData, Controller, Channel) || !MC_CH_MASK_CHECK (OdtValidChannelMask, Controller, Channel, MAX_CHANNEL)) {
                continue;
              }
              ChannelOut = &Outputs->Controller[Controller].Channel[Channel];
              //Set 2nd odt value
              SetDimmParamValue (MrcData, Controller, Channel, (R2R3_MASK >> (Dimm * 2)), OptDimmOdtNomRd,
                (ChannelOut->ValidRankBitMask == R0R1R2R3_MASK) ? OdtNomRd2R2RValsPointer[NomRdIndex] : OdtNomRd1R1RVals[NomRdIndex], FALSE);
            }// Channel
          }// Controller
          //Center RD voltage per rd change
          DQTimeCentering1D (MrcData, RdV, ResetPerBit, LC, MRC_PRINTS_OFF, FALSE, 3);
        } else {// if (!TestDefaultValue) : Override values to host defaults
          for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
            for (Channel = 0; Channel < MAX_CHANNEL; Channel++) {
              if (!MrcChannelExist (MrcData, Controller, Channel) || !MC_CH_MASK_CHECK (OdtValidChannelMask, Controller, Channel, MAX_CHANNEL)) {
                continue;
              }
              SetDimmParamValue (MrcData, Controller, Channel, (R0R1_MASK << (Dimm * 2)), OptDimmOdtPark,    DefaultRttPark[Controller][Channel][Dimm], FALSE);
              SetDimmParamValue (MrcData, Controller, Channel, (R0R1_MASK << (Dimm * 2)), OptDimmOdtParkDqs, DefaultRttParkDqs[Controller][Channel][Dimm], FALSE);
              SetDimmParamValue (MrcData, Controller, Channel, (R2R3_MASK >> (Dimm * 2)), OptDimmOdtNomRd,   DefaultRttNomRd[Controller][Channel][Dimm], FALSE);
            }// Channel
          }// Controller
          //RD/WR Voltage Centering after restoring host ctl/ch/dimm default values for OptDimmOdtPark/OptDimmOdtWr/OptDimmOdtNomWr
          MrcSetDefaultRxVrefDdr5 (MrcData, FALSE, MRC_PRINTS_OFF, FALSE);
          DQTimeCentering1D (MrcData, WrV, ResetPerBit, LC, MRC_PRINTS_OFF, FALSE, 2);
          DQTimeCentering1D (MrcData, RdV, ResetPerBit, LC, MRC_PRINTS_OFF, FALSE, 3);
        }

        //Run test
        DimmOdtRunTest (MrcData, OdtValidChannelMask, (1 == Dimm) ? (1 << Dimm) : TWO_DIMMS_MASK, TEST_RD, TotalScoresResult);

        for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
          for (Channel = 0; Channel < MAX_CHANNEL; Channel++) {
            if (!MrcChannelExist (MrcData, Controller, Channel)|| !MC_CH_MASK_CHECK (OdtValidChannelMask, Controller, Channel, MAX_CHANNEL)) {
              continue;
            }
            ChannelOut = &Outputs->Controller[Controller].Channel[Channel];
            Is2R2R = (ChannelOut->ValidRankBitMask == R0R1R2R3_MASK);
            // print Rd full grid results tables
            MRC_DEBUG_MSG (Debug, DbgLevel, "%d\t%d\t%d\t%d\t%d\t\t",
              (TestDefaultValue) ? DefaultRttPark[Controller][Channel][Dimm]  : (Is2R2R ? OdtPark2R2RValsPointer[ParkIndex]  : OdtPark1R1RVals[ParkIndex]),
              (TestDefaultValue) ? DefaultRttNomRd[Controller][Channel][Dimm] : (Is2R2R ? OdtNomRd2R2RValsPointer[NomRdIndex]: OdtNomRd1R1RVals[NomRdIndex]),
              Outputs->MarginResult[LastRxT][0][Controller][Channel][0][0],
              Outputs->MarginResult[LastRxV][0][Controller][Channel][0][0],
              TotalScoresResult[Controller][Channel][0][0]
              );

            TestScore = TotalScoresResult[Controller][Channel][0][0];

            if (TestDefaultValue) {
              RdTestResults[Controller][Channel][ParkIndex][0] = DefaultRttPark[Controller][Channel][Dimm];
              RdTestResults[Controller][Channel][ParkIndex][1] = DefaultRttNomRd[Controller][Channel][Dimm];
              RdTestResults[Controller][Channel][ParkIndex][2] = Outputs->MarginResult[LastRxT][0][Controller][Channel][0][0];
              RdTestResults[Controller][Channel][ParkIndex][3] = Outputs->MarginResult[LastRxV][0][Controller][Channel][0][0];
              RdTestResults[Controller][Channel][ParkIndex][4] = (UINT16)(TestScore * RepeatFactor / 100);//+3% score for default value
              //Set index to max to end test
              NomRdIndex = MaxOdtNomRdVals;
            } else if ((UINT16)TestScore >= RdTestResults[Controller][Channel][ParkIndex][4]) {//Save best score values per Park
              RdTestResults[Controller][Channel][ParkIndex][0] = Is2R2R ? OdtPark2R2RValsPointer[ParkIndex]   : OdtPark1R1RVals[ParkIndex];
              RdTestResults[Controller][Channel][ParkIndex][1] = Is2R2R ? OdtNomRd2R2RValsPointer[NomRdIndex] : OdtNomRd1R1RVals[NomRdIndex];
              RdTestResults[Controller][Channel][ParkIndex][2] = Outputs->MarginResult[LastRxT][0][Controller][Channel][0][0];
              RdTestResults[Controller][Channel][ParkIndex][3] = Outputs->MarginResult[LastRxV][0][Controller][Channel][0][0];
              RdTestResults[Controller][Channel][ParkIndex][4] = (UINT16)TestScore;
            }
          }// Channel
        }// Controller
        MRC_DEBUG_MSG (Debug, DbgLevel, "\n");
      }// NomRdIndex
      //Restore OptDimmOdtNomRd to default/good value so rx centering will pass/fail based on park value only;
      //Resulting same tx centering value for all odt values under same odtpark
      for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
        for (Channel = 0; Channel < MAX_CHANNEL; Channel++) {
          if (!MrcChannelExist (MrcData, Controller, Channel) || !MC_CH_MASK_CHECK (OdtValidChannelMask, Controller, Channel, MAX_CHANNEL)) {
            continue;
          }
          SetDimmParamValue (MrcData, Controller, Channel, (R2R3_MASK >> (Dimm * 2)), OptDimmOdtNomRd, DefaultRttNomRd[Controller][Channel][Dimm], FALSE);
        }
      }
    } // ParkIndex
    MRC_DEBUG_MSG (Debug, DbgLevel, "<END PARSE> Dimm%d %s flow\n", Dimm, "Read");

    //Print Rd summary results
    if (DebugPrints) {
      MRC_DEBUG_MSG (Debug, DbgLevel, "\nDIMM%d Rd Summary: OdtPark_DIMM%d,OdtNomRd_Dimm%d\n", Dimm, Dimm, (Dimm + 1) % 2);
      for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {//Print Rd summary results - All CTRL/CH
        for (Channel = 0; Channel < MAX_CHANNEL; Channel++) {
          if (!MrcChannelExist (MrcData, Controller, Channel) || !MC_CH_MASK_CHECK (OdtValidChannelMask, Controller, Channel, MAX_CHANNEL)) {
            continue;
          }
          MRC_DEBUG_MSG (Debug, DbgLevel, "Mc%d C%d\t\t\t\t\t\t", Controller, Channel);
        }// Channel
      }// Controller
      MRC_DEBUG_MSG (Debug, DbgLevel, "\n");
      for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {//Print Rd summary results - All CTRL/CH
        for (Channel = 0; Channel < MAX_CHANNEL; Channel++) {
          if (!MrcChannelExist (MrcData, Controller, Channel) || !MC_CH_MASK_CHECK (OdtValidChannelMask, Controller, Channel, MAX_CHANNEL)) {
            continue;
          }
          MRC_DEBUG_MSG (Debug, DbgLevel, "Park\tNomRd\tRdT\tRdV\tScore\t\t");
        }// Channel
      }// Controller
      MRC_DEBUG_MSG (Debug, DbgLevel, "\n");
      for (ParkIndex = 0; ParkIndex < (MaxOdtParkVals + DEFAULT_TEST); ParkIndex++) {
        for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {//Print Rd summary results - All CTRL/CH
          for (Channel = 0; Channel < MAX_CHANNEL; Channel++) {
            if (!MrcChannelExist (MrcData, Controller, Channel) || !MC_CH_MASK_CHECK (OdtValidChannelMask, Controller, Channel, MAX_CHANNEL)) {
              continue;
            }
            for (UINT8 ColIndex = 0; ColIndex < RD_TABLE_COLS; ColIndex++) {
              MRC_DEBUG_MSG (Debug, DbgLevel, "%d\t", RdTestResults[Controller][Channel][ParkIndex][ColIndex]);
            }// ColIndex
            MRC_DEBUG_MSG (Debug, DbgLevel, "\t");
          }// Channel
        }// Controller
        MRC_DEBUG_MSG (Debug, DbgLevel, "\n");
      } // ParkIndex
    }

    //COMBINE WR & RD Results

    //Print combine Wr & Rd summary headlines
    MRC_DEBUG_MSG (Debug, DbgLevel, "\nDIMM%d WrRd Summary:\n", Dimm);
    for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {//Print Wr & Rd headlines
      for (Channel = 0; Channel < MAX_CHANNEL; Channel++) {
        if (!MrcChannelExist (MrcData, Controller, Channel) || !MC_CH_MASK_CHECK (OdtValidChannelMask, Controller, Channel, MAX_CHANNEL)) {
          continue;
        }
        MRC_DEBUG_MSG (Debug, DbgLevel, "Mc%d C%d\t\t\t\t\t\t\t\t\t\t", Controller, Channel);
      }// Channel
    }// Controller
    MRC_DEBUG_MSG (Debug, DbgLevel, "\n");
    for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {//Print Rd summary results - All CTRL/CH
      for (Channel = 0; Channel < MAX_CHANNEL; Channel++) {
        if (!MrcChannelExist (MrcData, Controller, Channel) || !MC_CH_MASK_CHECK (OdtValidChannelMask, Controller, Channel, MAX_CHANNEL)) {
          continue;
        }
        MRC_DEBUG_MSG (Debug, DbgLevel, "Park\tWr\tNomWr\tNomRd\tWrT\tWrV\tRdT\tRdV\tScore\t\t", Dimm);
      }// Channel
    }// Controller
    MRC_DEBUG_MSG (Debug, DbgLevel, "\n");
    //Print combine Wr & Rd summary headlines - End

    MrcCall->MrcSetMem ((UINT8 *)WrRdTestResults, sizeof (WrRdTestResults), 0);
    MrcCall->MrcSetMem ((UINT8 *)BestScore, sizeof (BestScore), 0);
    MrcCall->MrcSetMem ((UINT8 *)BestIndex, sizeof (BestIndex), 0);

    for (ParkIndex = 0; ParkIndex < (MaxOdtParkVals + DEFAULT_TEST); ParkIndex++) {
      for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
        for (Channel = 0; Channel < MAX_CHANNEL; Channel++) {
          if (!MrcChannelExist (MrcData, Controller, Channel) || !MC_CH_MASK_CHECK (OdtValidChannelMask, Controller, Channel, MAX_CHANNEL)) {
            continue;
          }
          //Combine Wr & Rd values per Park
          for (UINT8 ColIndex = 0; ColIndex < FINAL_TABLE_COLS; ColIndex++) {
            if (3 > ColIndex) {
              WrRdTestResults[Controller][Channel][ParkIndex][ColIndex] = WrTestResults[Controller][Channel][ParkIndex][ColIndex];
            } else if (3 == ColIndex) {
              WrRdTestResults[Controller][Channel][ParkIndex][ColIndex] = RdTestResults[Controller][Channel][ParkIndex][1];
            } else if (4 == ColIndex) {
              WrRdTestResults[Controller][Channel][ParkIndex][ColIndex] = WrTestResults[Controller][Channel][ParkIndex][3];
            } else if (5 == ColIndex) {
              WrRdTestResults[Controller][Channel][ParkIndex][ColIndex] = WrTestResults[Controller][Channel][ParkIndex][4];
            } else if (6 == ColIndex) {
              WrRdTestResults[Controller][Channel][ParkIndex][ColIndex] = RdTestResults[Controller][Channel][ParkIndex][2];
            } else if (7 == ColIndex) {
              WrRdTestResults[Controller][Channel][ParkIndex][ColIndex] = RdTestResults[Controller][Channel][ParkIndex][3];
            } else if (8 == ColIndex) {
              WrRdTestResults[Controller][Channel][ParkIndex][ColIndex] =
              RdTestResults[Controller][Channel][ParkIndex][4] + WrTestResults[Controller][Channel][ParkIndex][5];//RD/WR priority hook
              //Find and store best results
              if (WrRdTestResults[Controller][Channel][ParkIndex][ColIndex] > BestScore[Controller][Channel]) {
                BestScore[Controller][Channel] = WrRdTestResults[Controller][Channel][ParkIndex][ColIndex];
                BestIndex[Controller][Channel] = ParkIndex;
              }
            }// if (3 > ColIndex)
            MRC_DEBUG_MSG (Debug, DbgLevel, "%d\t", WrRdTestResults[Controller][Channel][ParkIndex][ColIndex]);
          }// ColIndex
          MRC_DEBUG_MSG (Debug, DbgLevel, "\t");
        }// Channel
      }// Controller
      MRC_DEBUG_MSG (Debug, DbgLevel, "\n");
    } // ParkIndex

    // Setting best ODT values
    for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
      for (Channel = 0; Channel < MAX_CHANNEL; Channel++) {
        if (!MrcChannelExist (MrcData, Controller, Channel) || !MC_CH_MASK_CHECK (OdtValidChannelMask, Controller, Channel, MAX_CHANNEL)) {
          continue;
        }
        FinalIndex = BestIndex[Controller][Channel];
        MRC_DEBUG_MSG (Debug, DbgLevel, "\nMc%d C%d BestIndex%d is set per Dimm%d traffic\n", Controller, Channel, FinalIndex, Dimm);
        SetDimmParamValue (MrcData, Controller, Channel, (R0R1_MASK << (Dimm * 2)), OptDimmOdtPark,    WrRdTestResults[Controller][Channel][FinalIndex][0], TRUE);
        SetDimmParamValue (MrcData, Controller, Channel, (R0R1_MASK << (Dimm * 2)), OptDimmOdtParkDqs, WrRdTestResults[Controller][Channel][FinalIndex][0], TRUE);
        SetDimmParamValue (MrcData, Controller, Channel, (R0R1_MASK << (Dimm * 2)), OptDimmOdtWr,      WrRdTestResults[Controller][Channel][FinalIndex][1], TRUE);
        SetDimmParamValue (MrcData, Controller, Channel, (R2R3_MASK >> (Dimm * 2)), OptDimmOdtNomWr,   WrRdTestResults[Controller][Channel][FinalIndex][2], TRUE);
        SetDimmParamValue (MrcData, Controller, Channel, (R2R3_MASK >> (Dimm * 2)), OptDimmOdtNomRd,   WrRdTestResults[Controller][Channel][FinalIndex][3], TRUE);
      }// Channel
    }// Controller

    //RD/WR Voltage Centering after final setting
    MrcSetDefaultRxVrefDdr5 (MrcData, FALSE, MRC_PRINTS_OFF, FALSE);
    DQTimeCentering1D (MrcData, WrV, ResetPerBit, LC, MRC_PRINTS_OFF, FALSE, 2);
    DQTimeCentering1D (MrcData, RdV, ResetPerBit, LC, MRC_PRINTS_OFF, FALSE, 3);
  } // Dimm

  //RD/WR Timing 2D Centering after last setting
  MrcWriteTimingCentering2D (MrcData);
  MrcReadTimingCentering2D (MrcData);

  if (DebugPrints) {
    MrcPrintDimmOdtValues (MrcData);
  }

  return Status;
}

/**
  This function construct the up, test array based on test parameters

  @param[in] MrcData   - Pointer to MRC global data.
  @param[in] TestList  - Margin type
  @param[in] RMTBaseLIne - Taken from excel per technology; worst case per frequency
  @param[out] TestUpmArray - Array that includes per test parameter, margin, scaling factor

  @retval None
**/
void
MrcCreateUpmArray (
  IN  MrcParameters *const MrcData,
  IN  UINT8*               TestList,
  IN  UINT8                TestListSize,
  IN  MrcAdlRMTBaseLine*   RMTBaseLine,
  OUT MrcTestUpmArray*     TestUpmArray
)
{
  MrcOutput            *Outputs;
  const MrcInput       *Inputs;
  MrcDebug             *Debug;
  const MRC_FUNCTION   *MrcCall;
  UINT8                 Test;
  UINT8                 MarginType;
  UINT8                 MinTickValue;
  UINT8                 TickValue;
  UINT8                 ScaleFactor;
  UINT16                TotalTicks;

  Outputs = &MrcData->Outputs;
  Debug = &Outputs->Debug;
  Inputs = &MrcData->Inputs;
  MrcCall = Inputs->Call.Func;
  MinTickValue = 255;
  ScaleFactor = 0;

  //Get minimum per tick value form all test relevant parameters
  for (Test = 0; Test < TestListSize; Test++) {
    for (MarginType = 0; MarginType < MAX_MARGINS_TRADEOFF; MarginType++) {
      if (TestList[Test] == RMTBaseLine[MarginType].Test) {
        TickValue = (UINT8)(RMTBaseLine[MarginType].MarginPerSide / RMTBaseLine[MarginType].TicksPerSide);
        if (MinTickValue > TickValue) {
          MinTickValue = TickValue;
        }
        //MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\nRMTBaseLine[%d].MarginPerSide = %d, RMTBaseLine[%d].TicksPerSide = %d,MinTickValue = %d\n",
        //  MarginType, RMTBaseLine[MarginType].MarginPerSide, MarginType, RMTBaseLine[MarginType].TicksPerSide, MinTickValue);
        break;
      }
      else {
        continue;
      }
    }
  }

  //Create TestUpmArray
  for (Test = 0; Test < TestListSize; Test++) {
    for (MarginType = 0; MarginType < MAX_MARGINS_TRADEOFF; MarginType++) {
      if (TestList[Test] == RMTBaseLine[MarginType].Test) {
        //*Dest, *Src, NumBytes
        MrcCall->MrcCopyMem (&TestUpmArray[Test].Test, &TestList[Test], 1);//Type
        TotalTicks = RMTBaseLine[MarginType].TicksPerSide * 2 * 10;//ticksPerSide * 2sides * 10 (All margins in ticks are mul by 10)
        MrcCall->MrcCopyMem ((UINT8*)&TestUpmArray[Test].TotalTicks, (UINT8*)&TotalTicks, 2);//Margin
        ScaleFactor = (UINT8)((RMTBaseLine[MarginType].MarginPerSide / RMTBaseLine[MarginType].TicksPerSide) / MinTickValue);
        //MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\nRMTBaseLine[%d].MarginPerSide = %d, RMTBaseLine[%d].TicksPerSide = %d,ScaleFactor = %d\n",
        //  MarginType, RMTBaseLine[MarginType].MarginPerSide, MarginType, RMTBaseLine[MarginType].TicksPerSide, ScaleFactor);
        MrcCall->MrcCopyMem (&TestUpmArray[Test].ScaleFactor, &ScaleFactor, 1);//ScaleFactor
        break;
      }
      else {
        continue;
      }
    }
  }

  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Printing TestUpmArray:\n");
  for (Test = 0; Test < TestListSize; Test++) {//Print test list
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "%d\t%d\t%d\n", TestUpmArray[Test].Test, TestUpmArray[Test].TotalTicks, TestUpmArray[Test].ScaleFactor);
  }
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\n");
}

/**
This function is the DimmODTTraining routine.

@param[in] MrcData  - Include all MRC global data.

@retval MrcStatus - if it succeed return mrcSuccess
**/
MrcStatus
MrcDimmOdtTraining(
  IN MrcParameters *const MrcData
)
{
  UINT16              SaveMarginsArray[MAX_MARGINS_TRADEOFF][MAX_OPT_POINTS][MAX_CONTROLLER][MAX_BYTE_IN_DDR5_CHANNEL * MAX_DIMMS_IN_CHANNEL]; // Must match test list size
  static       UINT8  TestList[] = {WrV, WrT, RdT, RdV};
  static       UINT8  TestListWr[] = {WrV, WrT};
  MrcTestUpmArray     TestUpmArray[MAX_MARGINS_TRADEOFF];
  static const UINT8  OptParam[2][2] = {{ OptDimmOdtPark, OptDimmOdtNom }, { OptDimmOdtWr, OptWrDS }};
  UINT8               Scale[] = { 1, 1, 1, 1, 0 };
  MrcOutput           *Outputs;
  MrcInput            *Inputs;
  MrcDebug            *Debug;
  UINT16              *DimmOdtParkVals;
  UINT16              *DimmOdtNomVals;
  UINT16              *DimmWrOdtVals;
  OptOffsetChByte     BestOff;
  INT8                Start[2];
  INT8                Stop[2];
  INT8                Off;
  BOOLEAN             Ddr5;
  BOOLEAN             Ddr4;
  BOOLEAN             Lpddr5;
  UINT8               Channel;
  UINT8               Controller;
  MrcChannelOut       *ChannelOut;
  UINT8               OdtValidChannelMask;
  UINT8               OdtValidRankMask;

  Outputs = &MrcData->Outputs;
  Inputs = &MrcData->Inputs;
  Debug = &Outputs->Debug;

  Ddr5 = (Outputs->DdrType == MRC_DDR_TYPE_DDR5);
  Ddr4 = (Outputs->DdrType == MRC_DDR_TYPE_DDR4);
  Lpddr5 = (Outputs->DdrType == MRC_DDR_TYPE_LPDDR5);
  OdtValidChannelMask = 0;
  OdtValidRankMask = 0;

  for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
    for (Channel = 0; Channel < MAX_CHANNEL; Channel++) {
      if (!MrcChannelExist (MrcData, Controller, Channel)) {
        continue;
      }
      ChannelOut = &Outputs->Controller[Controller].Channel[Channel];
      if ((ChannelOut->ValidRankBitMask == R0R2_MASK) || (ChannelOut->ValidRankBitMask == R0R1R2R3_MASK)) {
        if (Ddr5) {
          // Select channels that have 2DPC populated (1R-1R or 2R-2R)
          // bits[0:3] are for MC0 and bits [4:7] are for MC1
          OdtValidChannelMask |= (1 << MC_CH_IDX(Controller, Channel, MAX_CHANNEL));
        } else if (Ddr4) {
          OdtValidChannelMask |= (1 << MC_CH_IDX (Controller, Channel, Outputs->MaxChannels));
          OdtValidRankMask |= ChannelOut->ValidRankBitMask;
        }
      }
    }
  }

  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "OdtValidChannelMask: 0x%x, OdtValidRankMask: 0x%x\n", OdtValidChannelMask, OdtValidRankMask);

  if ((Ddr5 || Ddr4) && (0 == OdtValidChannelMask)) {
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Skip DIMMODTT DDR4/5 for non 1R-1R/2R-2R 2DPC on any channel\n");
    return mrcSuccess;
  } else if (OdtValidChannelMask != 0) {// 2DPC 1R1R/2R2R on at least one channel
    if (Ddr5) {
      DimmOdtTrainingDDR5 (MrcData, OdtValidChannelMask);
    } else if (Ddr4) {
      MrcPrintDimmOdtValues(MrcData);  // Print DIMM ODT table
      GetDimmOptParamValues (MrcData, OptParam[0][0], &DimmOdtParkVals, (UINT8 *)&Stop[0]);
      Start[0] = 1;//Skip Hi-Z value
      Stop[0] -= 1;
      MRC_DEBUG_MSG (&Outputs->Debug, MSG_LEVEL_NOTE, "\nDIMM Park Odt Offset - Value mapping \nOffset\t Value\n");
      for (Off = Start[0]; Off < Stop[0]+1; Off++) {
        MRC_DEBUG_MSG (&Outputs->Debug, MSG_LEVEL_NOTE, "%d:\t %u \n", Off, DimmOdtParkVals[(UINT8)Off]);
      }

      GetDimmOptParamValues (MrcData, OptParam[0][1], &DimmOdtNomVals, (UINT8 *)&Stop[1]);
      Start[1] = 1;//Skip Hi-Z value
      Stop[1] -= 1;
      MRC_DEBUG_MSG (&Outputs->Debug, MSG_LEVEL_NOTE, "\nDIMM Nom Odt Offset - Value mapping \nOffset\t Value\n");
      for (Off = Start[1]; Off < Stop[1] + 1; Off++) {
        MRC_DEBUG_MSG (&Outputs->Debug, MSG_LEVEL_NOTE, "%d:\t %u \n", Off, DimmOdtNomVals[(UINT8)Off]);
      }

      MrcCreateUpmArray (MrcData, TestList, ARRAY_COUNT(TestList), (MrcAdlRMTBaseLine*) AdlDDR4RMTBaseLine, TestUpmArray);

      TrainDDROptParam (
        MrcData,
        &BestOff,
        OdtValidChannelMask,
        OdtValidRankMask,
        OptParam[0],
        ARRAY_COUNT (OptParam[0]),
        FullGrid,
        TestList,
        ARRAY_COUNT (TestList),
        Scale,
        NULL,
        Start,  // Start
        Stop,  // Stop
        OPT_PARAM_LOOP_COUNT + ((Inputs->DtHalo) ? 2 : 0),
        FALSE,   // Repeats
        0,      // NoPrint
        0,      // SkipOdtUpdate
        0,      // GuardBand
        0,       // PatternType
        SaveMarginsArray,
        TRUE, // UPMFilterEnable
        TestUpmArray,  // TestUpmArray
        FALSE  // CliffsFilterEnable
      );

      MRC_DEBUG_MSG (&Outputs->Debug, MSG_LEVEL_NOTE, "Re-center Write Vref\n");
      DQTimeCentering1D (MrcData, WrV, 0, RECENTER_LOOP_COUNT, MRC_POWER_TRAINING_DEBUG, FALSE, V_RECENTERING_STEP_SIZE);

      MRC_DEBUG_MSG (&Outputs->Debug, MSG_LEVEL_NOTE, "Re-center Write Timing\n");
      DQTimeCentering1D (MrcData, WrT, 0, RECENTER_LOOP_COUNT, MRC_POWER_TRAINING_DEBUG, FALSE, T_RECENTERING_STEP_SIZE);

      MRC_DEBUG_MSG (&Outputs->Debug, MSG_LEVEL_NOTE, "Re-center Read Vref\n");
      DQTimeCentering1D (MrcData, RdV, 0, RECENTER_LOOP_COUNT, MRC_POWER_TRAINING_DEBUG, FALSE, V_RECENTERING_STEP_SIZE);

      MRC_DEBUG_MSG (&Outputs->Debug, MSG_LEVEL_NOTE, "Re-center Read Timing\n");
      DQTimeCentering1D (MrcData, RdT, 0, RECENTER_LOOP_COUNT, MRC_POWER_TRAINING_DEBUG, FALSE, T_RECENTERING_STEP_SIZE);
      MrcReadTimingCentering2D (MrcData);
    }// else if (Ddr4)
  }// else if (OdtValidChannelMask != 0)

  if ((Lpddr5) || (Ddr4 && (OdtValidChannelMask != 0))) {// OdtWr training is common for both DDR4 2dpc & lpddr5
    MrcPrintDimmOdtValues(MrcData);  // Print DIMM ODT table
    GetDimmOptParamValues (MrcData, OptParam[1][0], &DimmWrOdtVals, (UINT8 *)&Stop[0]);
    if (DimmWrOdtVals == NULL) {
      MRC_DEBUG_MSG(&Outputs->Debug, MSG_LEVEL_ERROR, "Invalid pointer values\n");
      return mrcFail;
    }

    Start[0] = 1;//Skip Hi-Z value
    Stop[0] -= 1;

    MRC_DEBUG_MSG(&Outputs->Debug, MSG_LEVEL_NOTE, "\nDIMM WrOdt Offset - Value mapping \nOffset\t Value\n");
    for (Off = Start[0]; Off < Stop[0] + 1; Off++) {
      MRC_DEBUG_MSG(&Outputs->Debug, MSG_LEVEL_NOTE, "%d:\t %u \n", Off, DimmWrOdtVals[(UINT8)Off]);
    }
    Start[1] = OptParamLimitValue (MrcData, OptParam[1][1], 0);
    Stop[1] = OptParamLimitValue (MrcData, OptParam[1][1], 1);

    MrcCreateUpmArray (MrcData, TestListWr, ARRAY_COUNT(TestListWr), (MrcAdlRMTBaseLine*) (Ddr4 ?  AdlDDR4RMTBaseLine : AdlLPDDR5RMTBaseLine), TestUpmArray);

    TrainDDROptParam (// Train Wr ODT only
      MrcData,
      &BestOff,
      (Lpddr5)? MrcData->Outputs.ValidChBitMask: OdtValidChannelMask,
      (Lpddr5)? MrcData->Outputs.ValidRankMask : OdtValidRankMask,
      OptParam[1],
      ARRAY_COUNT (OptParam[1]),
      FullGrid,
      TestListWr,
      ARRAY_COUNT(TestListWr),
      Scale,
      NULL,
      Start,  // Start
      Stop,  // Stop
      OPT_PARAM_LOOP_COUNT + ((Inputs->DtHalo) ? 2 : 0),
      1,      // Repeats
      0,      // NoPrint
      0,      // SkipOdtUpdate
      0,      // GuardBand
      0,       // PatternType
      SaveMarginsArray,
      TRUE,
      TestUpmArray,  // TestUpmArray
      FALSE  // CliffsFilterEnable
    );

    // Re-Center Write voltage and update Host Struct with new center
    MRC_DEBUG_MSG(&Outputs->Debug, MSG_LEVEL_NOTE, "Re-center Write Vref\n");
    DQTimeCentering1D(MrcData, WrV, 0, RECENTER_LOOP_COUNT, MRC_POWER_TRAINING_DEBUG, FALSE, V_RECENTERING_STEP_SIZE);
    MRC_DEBUG_MSG(&Outputs->Debug, MSG_LEVEL_NOTE, "Re-center Write Timing\n");
    DQTimeCentering1D(MrcData, WrT, 0, RECENTER_LOOP_COUNT, MRC_POWER_TRAINING_DEBUG, FALSE, T_RECENTERING_STEP_SIZE);
    MrcPrintDimmOdtValues(MrcData);  // Print DIMM ODT table
    MrcWriteTimingCentering2D (MrcData);
  }

  return mrcSuccess;
}

/**
  Run DIMM ODT test and measure margins

  @param[in]     MrcData            - Include all MRC global data.
  @param[in]     ChannelMask        - Channel mask to run on
  @param[in]     DimmMask           - DIMM mask to run on
  @param[in]     Test               - Test to run: TEST_WR / TEST_RD / TEST_RDWR
  @param[out]    TotalScoresResult  - Margin results
**/
void
DimmOdtRunTest (
  IN  MrcParameters  *const MrcData,
  IN  UINT8                 ChannelMask,
  IN  UINT8                 DimmMask,
  IN  UINT8                 Test,
  OUT UINT16                TotalScoresResult[MAX_CONTROLLER][MAX_CHANNEL][MAX_SDRAM_IN_DIMM][MAX_EDGES]
  )
{
   const MRC_FUNCTION *MrcCall;
   MrcOutput          *Outputs;
   MrcChannelOut      *ChannelOut;
   MrcInput           *Inputs;
   UINT8              *TestList;
   static const UINT8 TestListWr[] = { WrT, WrV };
   static const UINT8 TestListRd[] = { RdT, RdV };
   static const UINT8 TestListRdWr[] = { WrT, WrV, RdT, RdV };
   UINT8              NumTests;
   UINT16             PosEdgeMargin;
   UINT16             NegEdgeMargin;
   MRC_MarginTypes    Param;
   UINT8              TestIndex;
   UINT8              MaxMargin;
   UINT8              BMap[9];
   UINT32             BERStats[4];
   UINT8              Controller;
   UINT8              Channel;
   UINT8              Rank;
   UINT8              Byte;
   UINT8              ResultType;
   UINT8              McChBitMask;
   UINT8              LC = 15;
   UINT8              LocalRankMask;
   UINT16             NormalizedResult;

   Outputs = &MrcData->Outputs;
   Inputs = &MrcData->Inputs;
   MrcCall = Inputs->Call.Func;

   for (Byte = 0; Byte < ARRAY_COUNT (BMap); Byte++) {
     BMap[Byte] = Byte;
   }

   switch (Test) {
    case TEST_RD:
      TestList = (UINT8*)TestListRd;
      NumTests = sizeof (TestListRd);
      break;

    case TEST_RDWR:
      TestList = (UINT8*)TestListRdWr;
      NumTests = sizeof (TestListRdWr);
      break;

    case TEST_WR:
    default:
      TestList = (UINT8*)TestListWr;
      NumTests = sizeof (TestListWr);
      break;
   }

   SetupIOTestBasicVA (MrcData, Outputs->McChBitMask, LC, NSOE, 0, 0, 8, PatWrRd, 0, 0);
   // Init test results for the new ODT set
   MrcCall->MrcSetMemWord (&TotalScoresResult[0][0][0][0],  MAX_CONTROLLER * MAX_CHANNEL * MAX_SDRAM_IN_DIMM * MAX_EDGES, 0);

   for (TestIndex = 0; TestIndex < NumTests; TestIndex++) {
     Param = TestList[TestIndex];
     ResultType = GetMarginResultType (Param);

     if ((Param == WrV) || (Param == RdV)) {
       MaxMargin = GetVrefOffsetLimits (MrcData, Param);
     } else {
       MaxMargin = MAX_POSSIBLE_TIME;
     }

     for (Rank = 0; Rank < MAX_RANK_IN_CHANNEL; Rank++) {//MultiRank implemented in fw
       LocalRankMask = 1 << Rank;// Select rank for REUT test
       McChBitMask = 0;
       for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
         for (Channel = 0; Channel < MAX_CHANNEL; Channel++) {
           if (!MrcChannelExist (MrcData, Controller, Channel) || !MC_CH_MASK_CHECK (ChannelMask, Controller, Channel, MAX_CHANNEL)) {
             continue;
           }
           ChannelOut = &Outputs->Controller[Controller].Channel[Channel];
           if (0 == (ChannelOut->ValidRankBitMask & LocalRankMask & ((DimmMask < TWO_DIMMS_MASK) ? (R0R1_MASK << (2*(DimmMask-1))) : R0R1R2R3_MASK))) {
             continue;
           }
           McChBitMask |= SelectReutRanks (MrcData, Controller, Channel, LocalRankMask, FALSE, 0);
         }
       }

       MrcCall->MrcSetMem ((UINT8 *)BERStats, sizeof (BERStats), 0);
       //Run margin per rank per byte for McChBitMask
       MrcGetBERMarginByte (
         MrcData,
         Outputs->MarginResult,
         McChBitMask,
         LocalRankMask,
         LocalRankMask,
         Param,
         0,
         BMap,
         0,
         MaxMargin,
         0,
         BERStats
         );
     } // Rank

     //Collect channel results
     for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
       for (Channel = 0; Channel < MAX_CHANNEL; Channel++) {
         if (!MrcChannelExist (MrcData, Controller, Channel) || !MC_CH_MASK_CHECK (ChannelMask, Controller, Channel, MAX_CHANNEL)) {
           continue;
         }
         ChannelOut = &Outputs->Controller[Controller].Channel[Channel];
         if (!(ChannelOut->ValidRankBitMask)) {
           continue;
         }

         PosEdgeMargin = 999; // Init big number
         NegEdgeMargin = 999; // Init big number
         for (Rank = 0; Rank < MAX_RANK_IN_CHANNEL; Rank++) {//MultiRank implemented in fw
           LocalRankMask = 1 << Rank;// Select rank for REUT test
           if (0 == (ChannelOut->ValidRankBitMask & LocalRankMask & ((DimmMask < TWO_DIMMS_MASK) ? (R0R1_MASK << (2*(DimmMask-1))) : R0R1R2R3_MASK))) {
             continue;
           }
           //Set minimum rank/byte value as channel margin
           for (Byte = 0; Byte < Outputs->SdramCount; Byte++) {
             PosEdgeMargin = MIN (PosEdgeMargin, Outputs->MarginResult[ResultType][Rank][Controller][Channel][Byte][0]);
             NegEdgeMargin = MIN (NegEdgeMargin, Outputs->MarginResult[ResultType][Rank][Controller][Channel][Byte][1]);
           }
         }// Rank
         //Saving channel summary margin in Rank0/Byte0/Edge0
         NormalizedResult = Outputs->MarginResult[ResultType][0][Controller][Channel][0][0] = PosEdgeMargin + NegEdgeMargin;
         if (Param == RdV) {
           NormalizedResult /= 3;//Reduce RdV value to be less dominant
         }
         //Saving all tests summary result at LastTotalScore Rank0/Byte0/Edge0
         TotalScoresResult[Controller][Channel][0][0] +=
           (UINT16) UPMFilter (MrcData, NormalizedResult, (Param == WrT) ? 250 : (Param == RdT) ? 200 : 150);//WrV/RdV
       }// Channel
     }// Controller
   }// TestIndex
}


/**
  This function implements Read Equalization training.

  @param[in] MrcData        - Include all MRC global data.
  @param[in] MsgPrint       - Control debug prints on/off.
  @param[in] FinalRecenter  - Switch to disable final re-centering.
  @param[in] PerChannel     - Switch to per-channel training.

  @retval MrcStatus - if it succeeds return mrcSuccess
**/
MrcStatus
ReadEQTraining (
  IN MrcParameters *const MrcData,
  IN BOOLEAN              MsgPrint,
  IN BOOLEAN              FinalRecenter,
  IN BOOLEAN              PerChannel
  )
{
  UINT16              SaveMarginsArray[2][MAX_OPT_POINTS][MAX_CONTROLLER][MAX_BYTE_IN_DDR5_CHANNEL * MAX_DIMMS_IN_CHANNEL]; // Must match test list size
  static const UINT8  TestList[]  = { RdV, RdT };
  UINT8               *OptParam1;
  UINT8               *OptParam2;
  UINT8               OptParamCTLE1[] = { OptRxEq }; // May set RxEq constant and sweep R/C 2D
  UINT8               OptParamCTLE2[] = { OptRxC, OptRxR };
  UINT8               OptParamDFE1[]  = { OptDFETap1 };
  UINT8               OptParamDFE2[]  = { OptDFETap2 };
  UINT8               Scale[]         = { 1, 1, 255, 0, 0 }; // Ignore power optimization, need a seperate scale array for each param
  UINT8               ScaleLpddr[]    = { 1, 2, 0, 0, 0 };
  OptOffsetChByte     BestOff;
  UINT8               GridMode1;
  UINT8               GridMode2;
  UINT8               OptParamLength1;
  UINT8               OptParamLength2;
  INT8                StartCTLE1[1];
  INT8                StopCTLE1[1];
  INT8                StartCTLE2[2];
  INT8                StopCTLE2[2];
  INT8                StartDFE1[1];
  INT8                StopDFE1[1];
  INT8                StartDFE2[1];
  INT8                StopDFE2[1];
  INT8                *Start1;
  INT8                *Stop1;
  INT8                *Start2;
  INT8                *Stop2;
  INT8                DfeStep;
  INT64               GetSetVal;
  INT64               GetSetEn;
  MrcOutput           *Outputs;
  MrcDebug            *Debug;
  MrcDebugMsgLevel    DebugLevel;
  BOOLEAN             RxModeMatched;
  BOOLEAN             Lpddr;

  Outputs       = &MrcData->Outputs;
  Debug         = &Outputs->Debug;
  DebugLevel    = MsgPrint ? MSG_LEVEL_NOTE : MSG_LEVEL_NEVER;
  RxModeMatched = (Outputs->RxMode == MrcRxModeMatchedN || Outputs->RxMode == MrcRxModeMatchedP);
  Lpddr         = Outputs->Lpddr;
  OptParam1     = NULL;
  OptParam2     = NULL;
  GetSetEn      = 1;

  if (RxModeMatched) {
    // CTLE
    OptParamLength1 = 1;
    OptParamLength2 = 2;
    GridMode1 = FullGrid;
    GridMode2 = FullGrid;
    OptParam1 = OptParamCTLE1;
    OptParam2 = OptParamCTLE2;

    StartCTLE1[0] = OptParamLimitValue (MrcData, OptParamCTLE1[0], 0);
    StopCTLE1[0]  = OptParamLimitValue (MrcData, OptParamCTLE1[0], 1);
    StartCTLE2[0] = OptParamLimitValue (MrcData, OptParamCTLE2[0], 0);
    StopCTLE2[0]  = OptParamLimitValue (MrcData, OptParamCTLE2[0], 1);
    StartCTLE2[1] = OptParamLimitValue (MrcData, OptParamCTLE2[1], 0);
    StopCTLE2[1]  = OptParamLimitValue (MrcData, OptParamCTLE2[1], 1);
    Start1  = StartCTLE1;
    Stop1   = StopCTLE1;
    Start2  = StartCTLE2;
    Stop2   = StopCTLE2;
  } else {
    // DFE
    OptParamLength1 = 1;
    OptParamLength2 = 1;
    GridMode1 = FullGrid;
    GridMode2 = FullGrid;
    OptParam1 = OptParamDFE1;
    OptParam2 = OptParamDFE2;
    DfeStep   = PerChannel ? 1 : DFE_STEP_PER_CHANNEL;

    StartDFE1[0]  = OptParamLimitValue (MrcData, OptParamDFE1[0], 0) * DfeStep;
    StopDFE1[0]   = OptParamLimitValue (MrcData, OptParamDFE1[0], 1) * DfeStep;
    StartDFE2[0]  = OptParamLimitValue (MrcData, OptParamDFE2[0], 0) * DfeStep;
    StopDFE2[0]   = OptParamLimitValue (MrcData, OptParamDFE2[0], 1) * DfeStep;
    Start1  = StartDFE1;
    Stop1   = StopDFE1;
    Start2  = StartDFE2;
    Stop2   = StopDFE2;
  }

  // Function Call for RxEQ Training
  Outputs->PowerTrainingPerChannel = PerChannel;
  if (!RxModeMatched) {
    // Enable RxDFETap1
    GetSetVal = 0;
    MrcGetSetChStrb (MrcData, MAX_CONTROLLER, MAX_CHANNEL, MAX_SDRAM_IN_DIMM, ForceDfeDisable, WriteNoCache, &GetSetVal);
    MrcGetSetChStrb (MrcData, MAX_CONTROLLER, MAX_CHANNEL, MAX_SDRAM_IN_DIMM, RxTap1En,        WriteCached,  &GetSetEn);
  }

  Scale[ARRAY_COUNT(TestList)] = 255;
  TrainDDROptParam (
    MrcData,
    &BestOff,
    Outputs->ValidChBitMask,
    Outputs->ValidRankMask,
    OptParam1,
    OptParamLength1,
    GridMode1,
    TestList,
    ARRAY_COUNT (TestList),
    Lpddr ? ScaleLpddr : Scale,
    NULL,
    Start1,
    Stop1,
    OPT_PARAM_LOOP_COUNT,
    1,          // Repeats
    !MsgPrint,  // NoPrint
    0,          // SkipOptUpdate
    0,          // GuardBand
    0,          // PatType
    SaveMarginsArray,
    FALSE, // UPMFilterEnable
    NULL,  // TestUpmArray
    FALSE  // CliffsFilterEnable
    );

  if (!RxModeMatched && Lpddr) { // LP5
    MRC_DEBUG_MSG (Debug, DebugLevel, "Re-center RdV\n");
    DQTimeCentering1D (MrcData, RdV, 0, OPT_PARAM_LOOP_COUNT, MRC_PWR_DBG_PRINT, FALSE, 1);

    MRC_DEBUG_MSG (Debug, DebugLevel, "Re-center RdT\n");
    PerBit1DCentering (MrcData, RdTP, OPT_PARAM_LOOP_COUNT + 2, 0, MRC_PRINTS_OFF, NULL);
    PerBit1DCentering (MrcData, RdTN, OPT_PARAM_LOOP_COUNT + 2, 0, MRC_PRINTS_OFF, NULL);
    DataTimeCentering2D (
      MrcData,
      Outputs->MarginResult,
      Outputs->McChBitMask,
      RdT,
      0,
      1,
      0,
      OPT_PARAM_LOOP_COUNT + 2,
      1
    );
    MrcGetSetChStrb (MrcData, MAX_CONTROLLER, MAX_CHANNEL, MAX_SDRAM_IN_DIMM, RxTap2En, WriteCached, &GetSetEn);

    Scale[ARRAY_COUNT(TestList)] = 255;
    TrainDDROptParam (
      MrcData,
      &BestOff,
      Outputs->ValidChBitMask,
      Outputs->ValidRankMask,
      OptParam2,
      OptParamLength2,
      GridMode2,
      TestList,
      ARRAY_COUNT (TestList),
      Scale,
      NULL,
      Start2,
      Stop2,
      OPT_PARAM_LOOP_COUNT + 2,
      1,          // Repeats
      !MsgPrint,  // NoPrint
      0,          // SkipOptUpdate
      0,          // GuardBand
      0,          // PatType
      SaveMarginsArray,
      FALSE, // UPMFilterEnable
      NULL,  // TestUpmArray
      FALSE  // CliffsFilterEnable
      );
  }

  if (FinalRecenter) {
    MRC_DEBUG_MSG (Debug, DebugLevel, "Re-center RdV\n");
    DQTimeCentering1D (MrcData, RdV, 0, OPT_PARAM_LOOP_COUNT, MRC_PWR_DBG_PRINT, FALSE, 1);

    MRC_DEBUG_MSG (Debug, DebugLevel, "Re-center RdT\n");
    PerBit1DCentering (MrcData, RdTP, OPT_PARAM_LOOP_COUNT + 2, 0, MRC_PRINTS_OFF, NULL);
    PerBit1DCentering (MrcData, RdTN, OPT_PARAM_LOOP_COUNT + 2, 0, MRC_PRINTS_OFF, NULL);
    DataTimeCentering2D (
      MrcData,
      Outputs->MarginResult,
      Outputs->McChBitMask,
      RdT,
      0,
      1,
      0,
      OPT_PARAM_LOOP_COUNT + 2,
      1
    );
  }

  return mrcSuccess;
}

/**
  This function implements Read Equalization training.

  @param[in] MrcData - Include all MRC global data.

  @retval MrcStatus - if it succeeds return mrcSuccess
**/
MrcStatus
MrcReadEQTraining (
  IN MrcParameters *const MrcData
  )
{
  return ReadEQTraining (MrcData, MRC_PRINTS_ON, TRUE, FALSE);
}

/**
  Training the Cmd/CTL TxEq and Ron for best margins.
  Steps:
  1. Find the minimal Vref (Ron) for which Comp is not saturated (Start).
  2. Find the maximal Vref (Ron) for which Comp is not saturated (Stop).
  3. Train CCC Ron (Vref) & TxEq in the region [Start, Stop].
  4. Center Cmd Timing.
  5. Center Cmd Voltage.

  @param[in] MrcData - Include all MRC global data.

  @retval MrcStatus - if it succeeds return mrcSuccess
**/
MrcStatus
MrcCmdEqDsTraining (
  IN MrcParameters *const MrcData
  )
{
  UINT16              SaveMarginsArray[2][MAX_OPT_POINTS][MAX_CONTROLLER][MAX_BYTE_IN_DDR5_CHANNEL * MAX_DIMMS_IN_CHANNEL]; // Must match test list size
  MrcOutput           *Outputs;
  static const UINT8  TestList[] = { CmdV, CmdT };
  UINT8               Scale[] = { 1, 1, 0, 0, 0 }; // Must specify scale = 0 to unpopulate slots!!
  static const UINT8  OptParam[] = { OptCCCTxEq, OptCCCDS }; // Param affecting vref must be second
  //MrcOutput           *Outputs;
  MrcStatus           Status;
  OptOffsetChByte     BestOff;
  INT8                Start[2];
  INT8                Stop[2];

  Outputs = &MrcData->Outputs;

  Start[0] = OptParamLimitValue (MrcData, OptParam[0], 0);
  Stop[0] = OptParamLimitValue (MrcData, OptParam[0], 1);
  Start[1] = OptParamLimitValue (MrcData, OptParam[1], 0);
  Stop[1] = OptParamLimitValue (MrcData, OptParam[1], 1);

  if (Outputs->Lpddr) {
    if ((CCC_TXEQ_MAX - Stop[0] * CCC_TXEQ_STEP) < LP_CCC_TXEQ_LOW_LIMIT) {
      Stop[0] = (CCC_TXEQ_MAX - LP_CCC_TXEQ_LOW_LIMIT) / CCC_TXEQ_STEP;
    }
  }

  TrainDDROptParam (
    MrcData,
    &BestOff,
    MrcData->Outputs.ValidChBitMask,
    MrcData->Outputs.ValidRankMask,
    OptParam,
    ARRAY_COUNT (OptParam),
    ChessEven,
    TestList,
    ARRAY_COUNT (TestList),
    Scale,
    NULL,
    Start,
    Stop,
    OPT_PARAM_LOOP_COUNT, // Loopcount
    1,                    // Repeats
    0,                    // NoPrint
    0,                    // SkipOptUpdate
    0,                    // GuardBand
    0,                     // PatType
    SaveMarginsArray,
    FALSE, // UPMFilterEnable
    NULL,  // TestUpmArray
    FALSE  // CliffsFilterEnable
    );

  // Re-Center CMD Timing and voltage and update Host Struct with new center (may be required, but probably vref centering will be sufficient as timing effects should be pretty symmetrical).
  MRC_DEBUG_MSG(&Outputs->Debug, MSG_LEVEL_NOTE, "Re-center Cmd Vref\n");
  Status = CmdVoltageCentering(MrcData, OPT_PARAM_LOOP_COUNT, V_FINAL_STEP_SIZE, MRC_PWR_DBG_PRINT);

  return Status;
}

/**
  This function implements CMD/CTL Drive Strength Up/Dn 2D.

  @param[in] MrcData - Include all MRC global data.

  @retval MrcStatus - if it succeeds return mrcSuccess
**/
MrcStatus
MrcCmdDsUpDnTraining (
  IN MrcParameters *const MrcData
  )
{
  UINT16              SaveMarginsArray[2][MAX_OPT_POINTS][MAX_CONTROLLER][MAX_BYTE_IN_DDR5_CHANNEL * MAX_DIMMS_IN_CHANNEL]; // Must match test list size
  MrcInput            *Inputs;
  MrcOutput           *Outputs;
  static const UINT8  TestList[] = { CmdV, CmdT };
  UINT8               Scale[] = { 1, 1, 0, 0, 0 }; // Must specify scale = 0 to unpopulated slots!!
  static const UINT8  OptParam[] = { OptCCCDSUpCoarse, OptCCCDSDnCoarse };
  OptOffsetChByte     BestOff;
  //UINT8               UPMOptimize[MAX_TRADEOFF_TYPES] = { 0, 1 };
  INT8                Start[2];
  INT8                Stop[2];
  MrcStatus           Status;

  Inputs  = &MrcData->Inputs;
  Outputs = &MrcData->Outputs;

  Start[0] = OptParamLimitValue (MrcData, OptParam[0], 0);
  Stop[0] = OptParamLimitValue (MrcData, OptParam[0], 1);
  Start[1] = OptParamLimitValue (MrcData, OptParam[1], 0);
  Stop[1] = OptParamLimitValue (MrcData, OptParam[1], 1);

  TrainDDROptParam (
    MrcData,
    &BestOff,
    MrcData->Outputs.ValidChBitMask,
    MrcData->Outputs.ValidRankMask,
    OptParam,
    ARRAY_COUNT (OptParam),
    UpDnCompOffsetSweepRange1,
    TestList,
    ARRAY_COUNT (TestList),
    Scale,
    NULL,
    Start,
    Stop,
    OPT_PARAM_LOOP_COUNT + 1,
    1,                    // Repeats
    0,                    // NoPrint
    0,                    // SkipOptUpdate
    0,                    // GuardBand
    0,                     // PatType
    SaveMarginsArray,
    FALSE, // UPMFilterEnable
    NULL,  // TestUpmArray
    FALSE  // CliffsFilterEnable
    );

  // Re-Center CMD Timing and voltage and update Host Struct with new center (may be required, but probably vref centering will be sufficient as timing effects should be pretty symmetrical).
  MRC_DEBUG_MSG (&Outputs->Debug, MSG_LEVEL_NOTE, "Re-center Cmd Vref\n");
  Status = CmdVoltageCentering (MrcData, OPT_PARAM_LOOP_COUNT, V_FINAL_STEP_SIZE, MRC_PWR_DBG_PRINT);

  if (!(Inputs->TrainingEnables2.CMDSR) || Outputs->Lpddr) { // We need to Re-center Cmd/Ctl timings if CMD SR and CAODT training were not run
    MRC_DEBUG_MSG (&Outputs->Debug, MSG_LEVEL_NOTE, "Re-center Cmd/Ctl Timings\n");
    Status = MrcCmdTimingCentering (MrcData, Outputs->ValidChBitMask, OPT_PARAM_LOOP_COUNT, TRUE, MRC_PWR_DBG_PRINT, 1);
    if (Status != mrcSuccess) {
      return Status;
    }

    MRC_DEBUG_MSG (&Outputs->Debug, MSG_LEVEL_NOTE, "Re-center Write Timing \n");
    //DQTimeCentering1D (MrcData, WrT, 0, T_RECENTERING_STEP_SIZE, OPT_PARAM_LOOP_COUNT, MRC_PWR_DBG_PRINT, FALSE, 1); // No need to use WrTLp4 after basic training
    MRC_DEBUG_MSG (&Outputs->Debug, MSG_LEVEL_NOTE, "Re-center Read Timing \n");
    //DQTimeCentering1D_RxDC (MrcData, 0, T_RECENTERING_STEP_SIZE, OPT_PARAM_LOOP_COUNT, MRC_PWR_DBG_PRINT, FALSE, 1);
  }

  return Status;
}

/**
  Training the Cmd/CTL/CLK slew rate for best margins.
  Steps:
  1. Get Min/Max Stage Number from DDR Scomp config step (Start).
  2. Train CCC Slew rate stages in the region [Start, Stop].
  3. Re-center Cmd Timing.
  4. Re-center Cmd Voltage.

  @param[in] MrcData - Include all MRC global data.

  @retval MrcStatus - if it succeeds return mrcSuccess
**/
MrcStatus
MrcCmdSlewRate (
  IN MrcParameters *const MrcData
  )
{
  UINT16              SaveMarginsArray[2][MAX_OPT_POINTS][MAX_CONTROLLER][MAX_BYTE_IN_DDR5_CHANNEL * MAX_DIMMS_IN_CHANNEL]; // Must match test list size
  static const UINT8  TestList[] = { CmdV, CmdT };
  static const UINT8  OptParam[] = { OptCCCSComp };
  UINT8               Scale[] = { 1, 1, 0, 0, 0 };
  MrcDebug            *Debug;
  MrcOutput           *Outputs;
  MrcIntOutput        *IntOutputs;
  MrcStatus           Status = mrcSuccess;
  OptOffsetChByte     BestOff;
  INT8                MaxNumStages;
  INT8                MinNumStages;

  Outputs = &MrcData->Outputs;
  IntOutputs = (MrcIntOutput *) (MrcData->IntOutputs.Internal);
  Debug = &Outputs->Debug;

  // Can run this on LPDDR4 if need be, but probably we should skip it
  if (Outputs->Lpddr) {
    return mrcSuccess;
  }

  MinNumStages = IntOutputs->CmdSRData.MinChainLengthCmd + OptParamLimitValue(MrcData, OptParam[0], 0); // We have to convert the 0->15 index into a -8->7 index.
  MaxNumStages = IntOutputs->CmdSRData.MaxChainLengthCmd + OptParamLimitValue(MrcData, OptParam[0], 0); // We have to convert the 0->15 index into a -8->7 index.
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\nCmd SComp Stages: Min %d, Max %d\n", MinNumStages, MaxNumStages);

  // Train Slew Rate for best margins
  TrainDDROptParam (
    MrcData,
    &BestOff,
    MrcData->Outputs.ValidChBitMask,
    MrcData->Outputs.ValidRankMask,
    OptParam,
    ARRAY_COUNT (OptParam),
    CustomSR,
    TestList,
    ARRAY_COUNT (TestList),
    Scale,
    NULL,
    &MinNumStages,  // Start
    &MaxNumStages,  // Stop
    OPT_PARAM_LOOP_COUNT + 1,
    1,      // Repeats
    0,      // NoPrint
    0,      // SkipOdtUpdate
    0,      // GuardBand
    0,       // PatType
    SaveMarginsArray,
    FALSE, // UPMFilterEnable
    NULL,  // TestUpmArray
    FALSE  // CliffsFilterEnable
    );

  // Re-Center CMD Timing and voltage and update Host Struct with new center
  MRC_DEBUG_MSG(Debug, MSG_LEVEL_NOTE, "Re-center Cmd/Ctl Timings\n");
  Status = MrcCmdTimingCentering (MrcData, Outputs->ValidChBitMask, OPT_PARAM_LOOP_COUNT, TRUE, MRC_PWR_DBG_PRINT, 1);
  if (Status != mrcSuccess) {
    return Status;
  }

  MRC_DEBUG_MSG(&Outputs->Debug, MSG_LEVEL_NOTE, "Re-center Write Timing \n");
  //DQTimeCentering1D(MrcData, WrT, 0, T_RECENTERING_STEP_SIZE, OPT_PARAM_LOOP_COUNT, MRC_PWR_DBG_PRINT, FALSE, 1); // No need to use WrTLp4 after basic training
  MRC_DEBUG_MSG(&Outputs->Debug, MSG_LEVEL_NOTE, "Re-center Read Timing \n");
  //DQTimeCentering1D_RxDC (MrcData, 0, T_RECENTERING_STEP_SIZE, OPT_PARAM_LOOP_COUNT, MRC_PWR_DBG_PRINT, FALSE, 1);

  MRC_DEBUG_MSG(Debug, MSG_LEVEL_NOTE, "Re-center Cmd Vref\n");
  Status = CmdVoltageCentering (MrcData, OPT_PARAM_LOOP_COUNT, V_FINAL_STEP_SIZE, MRC_PWR_DBG_PRINT);
  return Status;
}

/**
  This function implements Write (Transmitter) Equalization and Drive Strength training.

  @param[in] MrcData - Include all MRC global data.

  @retval MrcStatus - if it succeeds return mrcSuccess
**/
MrcStatus
MrcWriteEqDsTraining (
  IN MrcParameters *const MrcData
  )
{
  UINT16              SaveMarginsArray[2][MAX_OPT_POINTS][MAX_CONTROLLER][MAX_BYTE_IN_DDR5_CHANNEL * MAX_DIMMS_IN_CHANNEL]; // Must match test list size
  MrcOutput           *Outputs;
  static const UINT8  TestList[]  = { WrV, WrT };
  UINT8               Scale[]     = { 1, 1, 0, 0, 0 }; // Must specify scale = 0 to unpopulate slots!!
  static const UINT8  OptParam0[] = { OptTxEqCoeff0 }; // Param affecting vref must be second  , OptWrDS
  static const UINT8  OptParam1[] = { OptTxEqCoeff1 }; // Param affecting vref must be second  , OptWrDS
  static const UINT8  OptParam2[] = { OptTxEqCoeff2 }; // Param affecting vref must be second  , OptWrDS
  OptOffsetChByte     BestOff;
  INT8                Start; // [ARRAY_COUNT(OptParam0)];
  INT8                Stop; // [ARRAY_COUNT(OptParam0)];
  INT64               GetSetVal;
  UINT8               ValidChBitMask;
  UINT8               ValidRankMask;
  BOOLEAN             Ddr5;
  UINT32              Controller;
  UINT32              Channel;
  MrcChannelOut       *ChannelOut;
  MrcControllerOut    *ControllerOut;

  Outputs         = &MrcData->Outputs;
  ValidChBitMask  = Outputs->ValidChBitMask;
  ValidRankMask   = Outputs->ValidRankMask;
  Ddr5            = (Outputs->DdrType == MRC_DDR_TYPE_DDR5);

  Start = OptParamLimitValue (MrcData, OptParam0[0], 0);
  Stop  = OptParamLimitValue (MrcData, OptParam0[0], 1);
  //Start[1] = OptParamLimitValue (MrcData, OptParam0[1], 0);
  //Stop[1]  = OptParamLimitValue (MrcData, OptParam0[1], 1);

  // On DDR5 train only channels with 1DPC populated
  if (Ddr5) {
    ValidChBitMask = 0;
    for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
      ControllerOut = &Outputs->Controller[Controller];
      for (Channel = 0; Channel < Outputs->MaxChannels; Channel++) {
        if (!MrcChannelExist (MrcData, Controller, Channel)) {
          continue;
        }
        ChannelOut = &Outputs->Controller[Controller].Channel[Channel];
        if (ChannelOut->DimmCount == 1) {
          ValidChBitMask |= (ControllerOut->ValidChBitMask << (Controller * Outputs->MaxChannels));
          // Enable 2-Tap TxEq
          GetSetVal = 2;
          MrcGetSetChStrb (MrcData, Controller, Channel, MAX_SDRAM_IN_DIMM, GsmIocTxEqTapSelect, WriteCached | PrintValue, &GetSetVal);
          // Initialize Coef values
          GetSetVal = 3;
          MrcGetSetChStrb (MrcData, Controller, Channel, MAX_SDRAM_IN_DIMM, TxEqCoeff0, WriteCached | PrintValue, &GetSetVal);
          MrcGetSetChStrb (MrcData, Controller, Channel, MAX_SDRAM_IN_DIMM, TxEqCoeff1, WriteCached | PrintValue, &GetSetVal);
          MrcGetSetChStrb (MrcData, Controller, Channel, MAX_SDRAM_IN_DIMM, TxEqCoeff2, WriteCached | PrintValue, &GetSetVal);
        }
      }
    }
  } else {
    // Enable 2-Tap TxEq
    GetSetVal = 2;
    MrcGetSetChStrb (MrcData, MAX_CONTROLLER, MAX_CHANNEL, MAX_SDRAM_IN_DIMM, GsmIocTxEqTapSelect, WriteCached, &GetSetVal);
  }

  if (ValidChBitMask == 0) {
    MRC_DEBUG_MSG (&Outputs->Debug, MSG_LEVEL_NOTE,  "Skipping WRDSEQT on 2DPC\n");
    return mrcSuccess;
  }

  // Run TxEqCoeff0/DS 2D
  TrainDDROptParam (
    MrcData,
    &BestOff,
    ValidChBitMask,
    ValidRankMask,
    OptParam0,
    ARRAY_COUNT (OptParam0),
    FullGrid,
    TestList,
    ARRAY_COUNT (TestList),
    Scale,
    NULL,
    &Start,
    &Stop,
    OPT_PARAM_LOOP_COUNT,
    1,            // Repeats
    0,
    0,
    0,           // GuardBand
    0,            // PatType
    SaveMarginsArray,
    FALSE, // UPMFilterEnable
    NULL,  // TestUpmArray
    FALSE  // CliffsFilterEnable
    );

  // Run TxEqCoeff1/DS 2D
  TrainDDROptParam (
    MrcData,
    &BestOff,
    ValidChBitMask,
    ValidRankMask,
    OptParam1,
    ARRAY_COUNT (OptParam1),
    FullGrid,
    TestList,
    ARRAY_COUNT (TestList),
    Scale,
    NULL,
    &Start,
    &Stop,
    OPT_PARAM_LOOP_COUNT,
    1,            // Repeats
    0,
    0,
    0,           // GuardBand
    0,            // PatType
    SaveMarginsArray,
    FALSE, // UPMFilterEnable
    NULL,  // TestUpmArray
    FALSE  // CliffsFilterEnable
  );

  // Run TxEqCoeff2/DS 2D
  TrainDDROptParam (
    MrcData,
    &BestOff,
    ValidChBitMask,
    ValidRankMask,
    OptParam2,
    ARRAY_COUNT (OptParam2),
    FullGrid,
    TestList,
    ARRAY_COUNT (TestList),
    Scale,
    NULL,
    &Start,
    &Stop,
    OPT_PARAM_LOOP_COUNT,
    1,            // Repeats
    0,
    0,
    0,           // GuardBand
    0,            // PatType
    SaveMarginsArray,
    FALSE, // UPMFilterEnable
    NULL,  // TestUpmArray
    FALSE  // CliffsFilterEnable
  );

  // Re-Center Write Timing and voltage and update Host Struct with new center (may be required, but probably vref centering will be sufficient as timing effects should be pretty symmetrical).
  MRC_DEBUG_MSG (&Outputs->Debug, MSG_LEVEL_NOTE, "Re-center WrV\n");
  if (Ddr5) {
    DQTimeCentering1D (MrcData, WrV, 0, RECENTER_LOOP_COUNT, MRC_PWR_DBG_PRINT, FALSE, 1);
  } else {
    EarlyWriteTimingCentering2D (MrcData, MRC_PWR_DBG_PRINT);
    DQTimeCentering1D (MrcData, WrV, 0, RECENTER_LOOP_COUNT, MRC_PWR_DBG_PRINT, FALSE, 1);
  }

  MRC_DEBUG_MSG (&Outputs->Debug, MSG_LEVEL_NOTE, "Re-center WrT\n");
  PerBit1DCentering (MrcData, WrT, OPT_PARAM_LOOP_COUNT + 2, 0, MRC_PWR_DBG_PRINT, NULL);
  DQTimeCentering1D (MrcData, WrT, 0, OPT_PARAM_LOOP_COUNT, MRC_PWR_DBG_PRINT, FALSE, 1); // No need to use WrTLp4 / WrTDdr5 after basic training

  return mrcSuccess;
}

/**
This function implements Write (Transmitter) Drive Strength training.

@param[in] MrcData - Include all MRC global data.

@retval MrcStatus - if it succeeds return mrcSuccess
**/
MrcStatus
MrcWriteDsTraining(
  IN MrcParameters *const MrcData
)
{
  UINT16              SaveMarginsArray[2][MAX_OPT_POINTS][MAX_CONTROLLER][MAX_BYTE_IN_DDR5_CHANNEL * MAX_DIMMS_IN_CHANNEL]; // Must match test list size
  MrcOutput           *Outputs;
  static UINT8        TestList[] = { WrV, WrT };
  MrcTestUpmArray     TestUpmArray[MAX_MARGINS_TRADEOFF];
  UINT8               Scale[] = { 1, 1, 0, 0, 0 }; // Must specify scale = 0 to unpopulate slots!!
  static const UINT8  OptParam[] = { OptWrDS };
  OptOffsetChByte     BestOff;
  INT8                Start;
  INT8                Stop;
  UINT8               ValidChBitMask;
  UINT8               ValidRankMask;

  Outputs = &MrcData->Outputs;
  ValidChBitMask = Outputs->ValidChBitMask;
  ValidRankMask = Outputs->ValidRankMask;

  Start = OptParamLimitValue(MrcData, OptParam[0], 0);
  Stop = OptParamLimitValue(MrcData, OptParam[0], 1);

  MrcCreateUpmArray (MrcData, TestList, ARRAY_COUNT(TestList), (MrcAdlRMTBaseLine*) AdlDDR4RMTBaseLine, TestUpmArray);

  TrainDDROptParam(
    MrcData,
    &BestOff,
    ValidChBitMask,
    ValidRankMask,
    OptParam,
    ARRAY_COUNT(OptParam),
    FullGrid,
    TestList,
    ARRAY_COUNT(TestList),
    Scale,
    NULL,
    &Start,
    &Stop,
    OPT_PARAM_LOOP_COUNT,
    1,            // Repeats
    0,
    0,
    0,           // GuardBand
    0,            // PatType
    SaveMarginsArray,
    TRUE, // UPMFilterEnable
    TestUpmArray,  // TestUpmArray
    FALSE  // CliffsFilterEnable
  );

  // Re-Center Write Timing and voltage and update Host Struct with new center (may be required, but probably vref centering will be sufficient as timing effects should be pretty symmetrical).
  MRC_DEBUG_MSG(&Outputs->Debug, MSG_LEVEL_NOTE, "Re-center WrV\n");
  //EarlyWriteTimingCentering2D(MrcData, MRC_PWR_DBG_PRINT);
  DQTimeCentering1D(MrcData, WrV, 0, RECENTER_LOOP_COUNT, MRC_PWR_DBG_PRINT, FALSE, 1);
  MRC_DEBUG_MSG(&Outputs->Debug, MSG_LEVEL_NOTE, "Re-center WrT\n");
  //PerBit1DCentering(MrcData, WrT, OPT_PARAM_LOOP_COUNT + 2, 0, MRC_PWR_DBG_PRINT, NULL);
  DQTimeCentering1D(MrcData, WrT, 0, OPT_PARAM_LOOP_COUNT, MRC_PWR_DBG_PRINT, FALSE, 1); // No need to use WrTLp4 / WrTDdr5 after basic training

  return mrcSuccess;
}

/**
  This function implements Write (Transmitter) Drive Strength Up / Down training.

  @param[in] MrcData - Include all MRC global data.

  @retval MrcStatus - if it succeeds return mrcSuccess
**/
MrcStatus
MrcWriteDsUpDnTraining (
  IN MrcParameters *const MrcData
  )
{
  UINT16              SaveMarginsArray[2][MAX_OPT_POINTS][MAX_CONTROLLER][MAX_BYTE_IN_DDR5_CHANNEL * MAX_DIMMS_IN_CHANNEL]; // Must match test list size
  MrcDebug            *Debug;
  MrcInput            *Inputs;
  MrcOutput           *Outputs;
  static const UINT8  TestList[]  = { WrV, WrT };
  UINT8               Scale[]     = { 1, 1, 0, 0, 0 }; // Must specify scale = 0 to unpopulated slots!!
  static const UINT8  OptParam[]  = { OptWrDSUpCoarse, OptWrDSDnCoarse };
  static const UINT8  OptParam2[] = { OptTxEq };
  OptOffsetChByte     BestOff;
  UINT8               RankMask;
  INT8                Start[2];
  INT8                Stop[2];
  INT8                StartTxEq;
  INT8                StopTxEq;

  Inputs  = &MrcData->Inputs;
  Outputs = &MrcData->Outputs;
  Debug   = &Outputs->Debug;

  Start[0] = OptParamLimitValue (MrcData, OptParam[0], 0);
  Stop[0]  = OptParamLimitValue (MrcData, OptParam[0], 1);
  Start[1] = OptParamLimitValue (MrcData, OptParam[1], 0);
  Stop[1]  = OptParamLimitValue (MrcData, OptParam[1], 1);

  TrainDDROptParam (
    MrcData,
    &BestOff,
    MrcData->Outputs.ValidChBitMask,
    MrcData->Outputs.ValidRankMask,
    OptParam,
    ARRAY_COUNT (OptParam),
    UpDnCompOffsetSweepRange8,
    TestList,
    ARRAY_COUNT (TestList),
    Scale,
    NULL,
    Start,
    Stop,
    OPT_PARAM_LOOP_COUNT + 1,
    1,                    // Repeats
    0,                    // NoPrint
    0,                    // SkipOptUpdate
    0,                    // GuardBand
    0,                     // PatType
    SaveMarginsArray,
    FALSE, // UPMFilterEnable
    NULL,  // TestUpmArray
    FALSE  // CliffsFilterEnable
    );

  MRC_DEBUG_MSG (&Outputs->Debug, MSG_LEVEL_NOTE, "Re-center Write Vref\n");
  //DQTimeCentering1D (MrcData, WrV, 0, V_RECENTERING_STEP_SIZE, OPT_PARAM_LOOP_COUNT, MRC_PWR_DBG_PRINT, FALSE, 1);

  StartTxEq = OptParamLimitValue (MrcData, OptParam2[0], 0);
  StopTxEq = OptParamLimitValue (MrcData, OptParam2[0], 1);

  // Run TxEq 1D
  for (RankMask = 1; RankMask < (0x1 << MAX_RANK_IN_CHANNEL); RankMask <<= 1) {
    if (RankMask & MrcData->Outputs.ValidRankMask) {
      MRC_DEBUG_MSG (&Outputs->Debug, MSG_LEVEL_NOTE, "\n *** Optimizing RankMask %x *** \n", RankMask);
      Scale[ARRAY_COUNT (TestList)] = 0;
      TrainDDROptParam (
        MrcData,
        &BestOff,
        MrcData->Outputs.ValidChBitMask,
        RankMask,
        OptParam2,
        ARRAY_COUNT (OptParam2),
        FullGrid,
        TestList,
        ARRAY_COUNT (TestList),
        Scale,
        NULL,
        &StartTxEq,
        &StopTxEq,
        OPT_PARAM_LOOP_COUNT,
        1,                    // Repeats
        0,                    // NoPrint
        0,                    // SkipOptUpdate
        0,                    // GuardBand
        0,                     // PatType
        SaveMarginsArray,
        FALSE, // UPMFilterEnable
        NULL,  // TestUpmArray
        FALSE  // CliffsFilterEnable
        );
    }
  }

  // Re-Center Write Timing and voltage and update Host Struct with new center (may be required, but probably vref centering will be sufficient as timing effects should be pretty symmetrical).
  MRC_DEBUG_MSG(Debug, MSG_LEVEL_NOTE, "Re-center Write Vref\n");
  //DQTimeCentering1D(MrcData, WrV, 0, V_RECENTERING_STEP_SIZE, OPT_PARAM_LOOP_COUNT, MRC_PWR_DBG_PRINT, FALSE, 1);

  if (!(Inputs->TrainingEnables.WRSRT) || Outputs->Lpddr) { // We need to Re-center Wr timings if Wr SR training was not run
    MRC_DEBUG_MSG (&Outputs->Debug, MSG_LEVEL_NOTE, "Re-center Write Timing \n");
    //DQTimeCentering1D (MrcData, WrT, 0, T_RECENTERING_STEP_SIZE, OPT_PARAM_LOOP_COUNT, MRC_PWR_DBG_PRINT, FALSE, 1); // No need to use WrTLp4 after basic training
  }

  return mrcSuccess;
}

/**
  This function implements RxVref Decap training.

  @param[in] MrcData - Include all MRC global data.

  @retval MrcStatus - if it succeeds return mrcSuccess
**/
MrcStatus
MrcRxVrefDecapTraining (
  IN MrcParameters *const MrcData
  )
{
    UINT16              SaveMarginsArray[2][MAX_OPT_POINTS][MAX_CONTROLLER][MAX_BYTE_IN_DDR5_CHANNEL * MAX_DIMMS_IN_CHANNEL]; // Must match test list size
    MrcDebug            *Debug;
    MrcOutput           *Outputs;
    static const UINT8  TestList[] = { RdT, RdV };
    UINT8               *OptParam = 0x0;
    UINT8               OptParam1[] = { OptRxVrefVddqDecap };
    UINT8               OptParam2[] = { OptRxVrefVttDecap, OptRxVrefVddqDecap };
    UINT8               OptParam3[] = { OptRxVrefVttDecap };
    UINT8               Scale[] = { 2, 1, 255, 0, 0 }; // Ignore Power Optimization
    OptOffsetChByte     BestOff;
    UINT8               OptParamLength;
    UINT8               GridMode;
    INT8                *Start;
    INT8                *Stop;
    INT8                Start1;
    INT8                Stop1;
    INT8                Start2[2];
    INT8                Stop2[2];

    Outputs = &MrcData->Outputs;
    Debug = &Outputs->Debug;

    switch (Outputs->OdtMode) {
        case MrcOdtModeVss:
        case MrcOdtModeVddq:
          if (Outputs->Lpddr) {
            return mrcSuccess;
          }
          Start1 = OptParamLimitValue (MrcData, OptParam1[0], 0);
          Stop1 = OptParamLimitValue (MrcData, OptParam1[0], 1);
          Start = &Start1;
          Stop = &Stop1;
          OptParam = OptParam1;
          OptParamLength = 1;
          GridMode = FullGrid;
          break;

        case MrcOdtModeVtt:
          if (Outputs->Lpddr) {
            Start1 = OptParamLimitValue(MrcData, OptParam3[0], 0);
            Stop1 = OptParamLimitValue(MrcData, OptParam3[0], 1);
            Start = &Start1;
            Stop = &Stop1;
            OptParam = OptParam3;
            OptParamLength = 1;
            GridMode = FullGrid;
          } else {
            Start2[0] = OptParamLimitValue(MrcData, OptParam2[0], 0);
            Stop2[0] = OptParamLimitValue(MrcData, OptParam2[0], 1);
            Start2[1] = OptParamLimitValue(MrcData, OptParam2[1], 0);
            Stop2[1] = OptParamLimitValue(MrcData, OptParam2[1], 1);
            Start = Start2;
            Stop = Stop2;
            OptParam = OptParam2;
            OptParamLength = 2;
            GridMode = DecapSweep;
          }
          break;

        case MrcOdtModeDefault:
        default:
          return mrcSuccess;
    }

    TrainDDROptParam (
        MrcData,
        &BestOff,
        MrcData->Outputs.ValidChBitMask,
        MrcData->Outputs.ValidRankMask,
        OptParam,
        OptParamLength,
        GridMode,
        TestList,
        ARRAY_COUNT (TestList),
        Scale,
        NULL,
        Start,
        Stop,
        OPT_PARAM_LOOP_COUNT,
        1,                    // Repeats
        0,                    // NoPrint
        0,                    // SkipOptUpdate
        0,                    // GuardBand
        0,                     // PatType
        SaveMarginsArray,
        FALSE, // UPMFilterEnable
        NULL,  // TestUpmArray
        FALSE  // CliffsFilterEnable
        );

    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Re-center Rd Vref\n");
    //DQTimeCentering1D (MrcData, RdV, 0, V_RECENTERING_STEP_SIZE, OPT_PARAM_LOOP_COUNT, MRC_PWR_DBG_PRINT, FALSE, 1);

    MRC_DEBUG_MSG(&Outputs->Debug, MSG_LEVEL_NOTE, "Re-center Read Timing \n");
    //DQTimeCentering1D_RxDC (MrcData, 0, T_RECENTERING_STEP_SIZE, OPT_PARAM_LOOP_COUNT, MRC_PWR_DBG_PRINT, FALSE, 1);

    return mrcSuccess;
}

/**
  This function implements PanicVttDnLp Training.

  @param[in] MrcData - Include all MRC global data.

  @retval MrcStatus - if it succeeds return mrcSuccess
**/
MrcStatus
MrcPanicVttDnLpTraining (
  IN MrcParameters *const MrcData
  )
{
    UINT16              SaveMarginsArray[1][MAX_OPT_POINTS][MAX_CONTROLLER][MAX_BYTE_IN_DDR5_CHANNEL * MAX_DIMMS_IN_CHANNEL]; // Must match test list size
    MrcDebug            *Debug;
    MrcOutput           *Outputs;
    static const UINT8  TestList[] = { RdV };
    static const UINT8  OptParam[] = { OptPanicVttDnLp };
    UINT8               Scale[] = { 1, 0, 0, 0, 0 }; // Must specify scale = 0 to unpopulated slots!!
    OptOffsetChByte     BestOff;
    UINT8               VTTIndex;
    //INT64               GetSetVal;
    INT8                Start;
    INT8                Stop;

    Outputs = &MrcData->Outputs;
    Debug = &Outputs->Debug;

    if ((!Outputs->Lpddr) || (Outputs->OdtMode != MrcOdtModeVtt)) {
        return mrcSuccess;
    }

    Start = OptParamLimitValue (MrcData, OptParam[0], 0);
    Stop = OptParamLimitValue (MrcData, OptParam[0], 1);

    // Enable the panic event counter
    for (VTTIndex = 0; VTTIndex < MAX_VTT_REGS; VTTIndex++) {
      //GetSetVal = 1;
      //MrcGetSetChStrb(MrcData, MRC_IGNORE_ARG, MRC_IGNORE_ARG, VTTIndex, VttGenStatusEnCount, WriteCached, &GetSetVal);
    }

    TrainDDROptParam (
        MrcData,
        &BestOff,
        MrcData->Outputs.ValidChBitMask,
        MrcData->Outputs.ValidRankMask,
        OptParam,
        ARRAY_COUNT (OptParam),
        FullGrid,
        TestList,
        ARRAY_COUNT (TestList),
        Scale,
        NULL,
        &Start,
        &Stop,
        OPT_PARAM_LOOP_COUNT,
        1,                    // Repeats
        0,                    // NoPrint
        0,                    // SkipOptUpdate
        0,                    // GuardBand
        0,                     // PatType
        SaveMarginsArray,
        FALSE, // UPMFilterEnable
        NULL,  // TestUpmArray
        FALSE  // CliffsFilterEnable
        );

    // Disable the panic event counter
    //for (VTTIndex = 0; VTTIndex < MAX_VTT_REGS; VTTIndex++) {
    //  GetSetVal = 0;
    //  MrcGetSetChStrb(MrcData, MRC_IGNORE_ARG, MRC_IGNORE_ARG, VTTIndex, VttGenStatusEnCount, WriteCached, &GetSetVal);
    //}

    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Re-center Rd Vref\n");
    //DQTimeCentering1D (MrcData, RdV, 0, V_RECENTERING_STEP_SIZE, OPT_PARAM_LOOP_COUNT, MRC_PWR_DBG_PRINT, FALSE, 1);

    return mrcSuccess;
}

/**
  This function implements Vddq power training.

  @param[in] MrcData - Include all MRC global data.

  @retval MrcStatus - If it succeeds return mrcSuccess
**/
MrcStatus
MrcVddqTraining (
  IN MrcParameters *const MrcData
  )
{
    UINT16              SaveMarginsArray[4][MAX_OPT_POINTS][MAX_CONTROLLER][MAX_BYTE_IN_DDR5_CHANNEL * MAX_DIMMS_IN_CHANNEL]; // Must match test list size
    MrcStatus           Status;
    MrcOutput           *Outputs;
    MrcDebug            *Debug;
    static const UINT8  TestList[] = { CmdV, CmdT, WrV, WrT };
    static const UINT8  OptParam[] = { OptVddq };
    UINT8               Scale[] = { 2, 2, 1, 1 };
    OptOffsetChByte     BestOff;
    UINT16              LpddrOvershootLimits[] = { 0, 0 };
    INT8                Start;
    INT8                Stop;
    INT8                GuardBand;
    BOOLEAN             Lpddr;

    // TGL_POWER_TRAINING_VDDQ: This algorithm can potentially (but shouldn't under operational conditions) overflow vref values during VDDQ adjustments. Make sure postsilicon that the VDDQ sweep range doesn't cause Vref overflows.
    Status = mrcSuccess;
    Outputs = &MrcData->Outputs;
    Debug = &Outputs->Debug;
    Lpddr = (Outputs->DdrType == MRC_DDR_TYPE_LPDDR4) || (Outputs->DdrType == MRC_DDR_TYPE_LPDDR5);

    if (!Lpddr) {
        return mrcSuccess;
    }

    Start = OptParamLimitValue (MrcData, OptParam[0], 0);
    Stop = OptParamLimitValue (MrcData, OptParam[0], 1);

    if (Outputs->Lpddr) {
      CalcVDDQLimits(MrcData, LpddrOvershootLimits);

      if (LpddrOvershootLimits[0] > Start) {
        MRC_DEBUG_MSG(Debug, MSG_LEVEL_NOTE, "Changing VDDQ sweep limits due to LPDDR overshoot limits. Original %d, Final %d \n", Start, LpddrOvershootLimits[0]);
        Start = (INT8)LpddrOvershootLimits[0];
      }
      if (LpddrOvershootLimits[1] < Stop) {
        MRC_DEBUG_MSG(Debug, MSG_LEVEL_NOTE, "Changing VDDQ sweep limits due to LPDDR overshoot limits. Original %d, Final %d \n", Stop, LpddrOvershootLimits[1]);
        Stop = (INT8)LpddrOvershootLimits[1];
      }
    }

    if (Stop > Start) { // Don't run if VDDQ can't be changed
      GuardBand = 0; // Margins increases are monotonic with param changes, so does not require a param gaurdband

    TrainDDROptParam (
        MrcData,
        &BestOff,
        MrcData->Outputs.ValidChBitMask,
        MrcData->Outputs.ValidRankMask,
        OptParam,
        ARRAY_COUNT (OptParam),
        FullGrid,
        TestList,
        ARRAY_COUNT (TestList),
        Scale,
        NULL,
        &Start,           // Start
        &Stop,            // Stop
        OPT_PARAM_LOOP_COUNT,
        1,                // Repeats
        0,                // NoPrint
        0,                // SkipOdtUpdate
        GuardBand,
        0,                 // PatType
        SaveMarginsArray,
        FALSE, // UPMFilterEnable
        NULL,  // TestUpmArray
        FALSE  // CliffsFilterEnable
      );

      /*if (Outputs->Lpddr) {
        MrcLpddrOvershoot(MrcData);
      }*/

      // JEDEC init in case the last Vddq setting messed up the DRAMs (only needed if running data margins blindly after command margins).
      // MrcResetSequence (MrcData);

      // May not be neccesary if we can adjust in the get/set Vddq method
      // Timings need not be re-centered since we are using widths for power training after this, we just need to make sure the Vrefs aren't really far off.
      MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Re-center Cmd Vref\n");
      Status = CmdVoltageCentering (MrcData, OPT_PARAM_LOOP_COUNT, V_FINAL_STEP_SIZE, MRC_PWR_DBG_PRINT);
      MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Re-center Write Vref\n");
      //DQTimeCentering1D (MrcData, WrV, 0, V_RECENTERING_STEP_SIZE, OPT_PARAM_LOOP_COUNT, MRC_PWR_DBG_PRINT, FALSE, 1);
      MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Re-center Rd Vref\n");
      //DQTimeCentering1D (MrcData, RdV, 0, V_RECENTERING_STEP_SIZE, OPT_PARAM_LOOP_COUNT, MRC_PWR_DBG_PRINT, FALSE, 1);
      // May need to re-center CMD/CTL-CLK if this disturbs the CMD/CTL to CLK alignment by more than a few ticks.
      // Re-Center CMD Timing and voltage and update Host Struct with new center
      //MRC_DEBUG_MSG(Debug, MSG_LEVEL_NOTE, "Re-center Cmd/Ctl Timings\n");
//#ifndef LOCAL_STUB_FLAG
      //Status = MrcCmdTimingCentering(MrcData, Outputs->ValidChBitMask, OPT_PARAM_LOOP_COUNT, TRUE, MRC_PWR_DBG_PRINT, 1);
//#endif
      //if (Status != mrcSuccess) {
      //  return Status;
      //}
      // May need to re-center Write Leveling if this disturbs the TxDqs to CLK alignment by more than a few ticks. DQTimeCentering1D does not currently support 'WrDqsT'.
      // MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Re-center TxDqs (Write Leveling Fine)\n");
      // DQTimeCentering1D (MrcData, WrDqsT, 0, T_RECENTERING_STEP_SIZE, OPT_PARAM_LOOP_COUNT, MRC_PWR_DBG_PRINT, FALSE, 1);
    }
    return Status;
}

/**
  This function implements Read Amplifier Power training.

  @param[in] MrcData - Include all MRC global data.

  @retval MrcStatus - If it succeeds return mrcSuccess
**/
MrcStatus
MrcReadAmplifierPower (
  IN MrcParameters *const MrcData
  )
{
  UINT16              SaveMarginsArray[2][MAX_OPT_POINTS][MAX_CONTROLLER][MAX_BYTE_IN_DDR5_CHANNEL * MAX_DIMMS_IN_CHANNEL]; // Must match test list size
  MrcStatus           Status;
  MrcOutput           *Outputs;
  static const UINT8  TestList[]  = { RdV, RdT };
  UINT8               Scale0[]    = { 1, 1, 0,   0, 0 }; // Need seperate scale arrays for each param in case power is optimized differently for each one
  UINT8               Scale1[]    = { 1, 1, 255, 0, 0 }; // Need seperate scale arrays for each param in case power is optimized differently for each one
  UINT8               OptParam0[] = { OptRxBias };
  UINT8               OptParam1[] = { OptRxBiasTailCtl, OptRxBiasVrefSel };
  OptOffsetChByte     BestOff;
  INT8                Start0;
  INT8                Stop0;
  INT8                Start1[ARRAY_COUNT(OptParam1)];
  INT8                Stop1[ARRAY_COUNT(OptParam1)];
  INT8                GuardBand;
  UINT8               ValidChBitMask;
  UINT8               ValidRankMask;

  Status          = mrcSuccess;
  Outputs         = &MrcData->Outputs;
  ValidChBitMask  = Outputs->ValidChBitMask;
  ValidRankMask   = Outputs->ValidRankMask;

  Start0 = OptParamLimitValue (MrcData, OptParam0[0], 0);
  Stop0  = OptParamLimitValue (MrcData, OptParam0[0], 1);
  GuardBand = 0;

  // Function Call for RxBias/RxLoad
  TrainDDROptParam (
    MrcData,
    &BestOff,
    ValidChBitMask,
    ValidRankMask,
    OptParam0,
    ARRAY_COUNT (OptParam0),
    FullGrid,
    TestList,
    ARRAY_COUNT (TestList),
    Scale0,
    NULL,
    &Start0,           // Start
    &Stop0,            // Stop
    OPT_PARAM_LOOP_COUNT,
    1,                // Repeats
    0,                // NoPrint
    0,                // SkipOdtUpdate
    GuardBand,
    0,                 // PatType
    SaveMarginsArray,
    FALSE, // UPMFilterEnable
    NULL,  // TestUpmArray
    FALSE  // CliffsFilterEnable
  );

    Start1[0] = OptParamLimitValue (MrcData, OptParam1[0], 0);
    Stop1[0]  = OptParamLimitValue (MrcData, OptParam1[0], 1);
    Start1[1] = OptParamLimitValue (MrcData, OptParam1[1], 0);
    Stop1[1]  = OptParamLimitValue (MrcData, OptParam1[1], 1);

    // Function Call for RxBiasVrefSel and TailCtl
    TrainDDROptParam (
      MrcData,
      &BestOff,
      ValidChBitMask,
      ValidRankMask,
      OptParam1,
      ARRAY_COUNT (OptParam1),
      ChessEven,
      TestList,
      ARRAY_COUNT (TestList),
      Scale1,
      NULL,
      Start1,           // Start
      Stop1,            // Stop
      OPT_PARAM_LOOP_COUNT,
      1,                // Repeats
      0,                // NoPrint
      0,                // SkipOdtUpdate
      GuardBand,
      0,                 // PatType
      SaveMarginsArray,
      FALSE, // UPMFilterEnable
      NULL,  // TestUpmArray
      FALSE  // CliffsFilterEnable
    );

  //MRC_DEBUG_MSG(&Outputs->Debug, MSG_LEVEL_NOTE, "Re-center Read Vref \n");
  //DQTimeCentering1D(MrcData, RdV, 0, V_RECENTERING_STEP_SIZE, OPT_PARAM_LOOP_COUNT, MRC_PWR_DBG_PRINT, FALSE, 1);
  //
  //MRC_DEBUG_MSG(&Outputs->Debug, MSG_LEVEL_NOTE, "Re-center Read Timing \n");
  //DQTimeCentering1D_RxDC (MrcData, 0, T_RECENTERING_STEP_SIZE, OPT_PARAM_LOOP_COUNT, MRC_PWR_DBG_PRINT, FALSE, 1);
  MRC_DEBUG_MSG (&Outputs->Debug, MSG_LEVEL_NOTE, "Re-center Read 2D\n");
  PerBit1DCentering (MrcData, RdTP, OPT_PARAM_LOOP_COUNT + 2, 0, MRC_PRINTS_OFF, NULL);
  PerBit1DCentering (MrcData, RdTN, OPT_PARAM_LOOP_COUNT + 2, 0, MRC_PRINTS_OFF, NULL);
  Status = DataTimeCentering2D (
    MrcData,
    Outputs->MarginResult,
    Outputs->McChBitMask,
    RdT,
    0,
    1,
    0,
    OPT_PARAM_LOOP_COUNT + 2,
    1
  );
  //DQTimeCentering1D(MrcData, RdT, 0, OPT_PARAM_LOOP_COUNT, MRC_PWR_DBG_PRINT, FALSE, 1);
  //EarlyReadTimingCentering2D (MrcData, ERTC2D_NORMAL, MRC_PRINTS_OFF);

  return Status;
}

#define DfeTap1Num   (51) // Max possible value

/**
  DIMM DFE traning per Rank.

  @param[in] MrcData  - Include all MRC global data.
  @param[in] DfeTap   - DfeTap number 1, 2, 3, 4

  @retval MrcStatus - if it succeeds return mrcSuccess
**/
MrcStatus
MrcDimmDfeRankTraining (
  IN MrcParameters *const MrcData,
  IN const UINT32         DfeTap,
  IN UINT8                LoopCountInput
  )
{
  const MrcInput      *Inputs;
  const MRC_FUNCTION  *MrcCall;
  MrcDebug            *Debug;
  MrcOutput           *Outputs;
  INT16               DfeTap1Val[DfeTap1Num];
  UINT8               Controller;
  UINT8               Channel;
  UINT8               Rank;
  static const UINT8  OptParam[] = { OptDimmDFETap1, OptDimmDFETap2, OptDimmDFETap3, OptDimmDFETap4 };
  UINT8               Param;
  UINT8               RankMask;
  UINT8               MaxMargin;
  UINT32              BERStats[4];
  UINT16              MarginTotal[MAX_CONTROLLER][MAX_CHANNEL][MAX_RANK_IN_CHANNEL][DfeTap1Num][1]; // Last dimension is for  WrV only
  UINT32              ResultsSum[MAX_CONTROLLER][MAX_CHANNEL][MAX_RANK_IN_CHANNEL];
  UINT8               ResCount[MAX_CONTROLLER][MAX_CHANNEL][MAX_RANK_IN_CHANNEL];
  UINT32              SumRes;
  UINT8               Tap1ind;
  UINT8               LoopCount;
  UINT16              GetSetVal;
  UINT16              Sign;
  UINT8               McChBitMask;
  UINT8               MaxChannels;
  INT8                StartDfe;
  INT8                StopDfe;
  UINT8               DfeSteps;

  Inputs      = &MrcData->Inputs;
  Outputs     = &MrcData->Outputs;
  Debug       = &Outputs->Debug;
  MrcCall     = Inputs->Call.Func;
  McChBitMask = Outputs->ValidChBitMask;
  RankMask    = Outputs->ValidRankMask;
  MaxChannels = Outputs->MaxChannels;
  LoopCount   = LoopCountInput;

  if (DfeTap > ARRAY_COUNT (OptParam)) {
    return mrcWrongInputParameter;
  }
  Param = OptParam[DfeTap - 1];

  MrcCall->MrcSetMem ((UINT8 *) BERStats, sizeof (BERStats), 0);
  MrcCall->MrcSetMem ((UINT8 *) ResCount, sizeof (ResCount), 0);
  MrcCall->MrcSetMemDword ((UINT32 *) ResultsSum, sizeof (ResultsSum) / sizeof (UINT32), 0);

  StartDfe = OptParamLimitValue (MrcData, Param, 0);
  StopDfe  = OptParamLimitValue (MrcData, Param, 1);
  DfeSteps = 1 + ABS (StartDfe) + ABS (StopDfe);

  // Fill values for dimm dfetap1
  for (Tap1ind = 0; Tap1ind < DfeSteps; Tap1ind++) {
    DfeTap1Val[Tap1ind] = StartDfe + Tap1ind;
  }

  // Setup stress
  SetupIOTestBasicVA (MrcData, McChBitMask, LoopCount, NSOE, 0, 0, 8, PatWrRd, 0, 0);

  // Training per Rank
  for (Tap1ind = 0; Tap1ind < DfeSteps; Tap1ind++) { // DfeTap values
    for (Rank = 0; Rank < MAX_RANK_IN_CHANNEL; Rank++) {
      RankMask = 1 << Rank;
      for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
        for (Channel = 0; Channel < MAX_CHANNEL; Channel++) {
          if (!(MrcRankExist(MrcData, Controller, Channel, Rank))) {
            continue;
          }
          Sign = (DfeTap1Val[Tap1ind] < 0) ? MRC_BIT6 : 0;  // direction
          GetSetVal = ABS (DfeTap1Val[Tap1ind]);            // value
          GetSetVal |= Sign;
          GetSetVal |= MRC_BIT7;                            // DFE Enable
          SetDimmParamValue (MrcData, Controller, Channel, RankMask, Param, GetSetVal, 1);
        } // Channel
      }  // Controller
    } // Rank

    // Perform centering
    // Maybe WrV, in case of fail continue to next values.

    // Collect margin per rank
    for (Rank = 0; Rank < MAX_RANK_IN_CHANNEL; Rank++) {
      // Select rank for REUT test
      RankMask = 1 << Rank;
      McChBitMask = 0;
      for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
        for (Channel = 0; Channel < MaxChannels; Channel++) {
          McChBitMask |= SelectReutRanks (MrcData, Controller, Channel, RankMask, FALSE, 0);
        }
      }
      // Continue with next rank if this rank is not present on any channel
      if (McChBitMask == 0) {
        continue;
      }

      // Measure WrV margin
      MaxMargin = GetVrefOffsetLimits (MrcData, WrV);
      MrcGetBERMarginCh (
        MrcData,
        Outputs->MarginResult,
        McChBitMask,
        0xFF, // No Lpddr support for now
        RankMask,
        WrV,
        0,
        0,
        MaxMargin,
        0,
        BERStats
      );

      // Measure WrT margin - skipped, because DFE mostly impacts WrV
      // MaxMargin = MAX_POSSIBLE_TIME;
      // MrcGetBERMarginCh (
      //   MrcData,
      //   Outputs->MarginResult,
      //   McChBitMask,
      //   0xFF, // No Lpddr support for now
      //   RankMask,
      //   WrT,
      //   0,
      //   0,
      //   MaxMargin,
      //   0,
      //   BERStats
      // );
    } // Rank

    // Save results
    for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
      for (Rank = 0; Rank < MAX_RANK_IN_CHANNEL; Rank++) {
        for (Channel = 0; Channel < MaxChannels; Channel++) {
          SumRes = 0;
          if (MrcRankExist(MrcData, Controller, Channel, Rank)) {
            //MarginTotal[Controller][Channel][Rank][Tap1ind][0] =  (UINT8)(Outputs->MarginResult[LastTxT][Rank][Controller][Channel][0][0] / 10);
            //MarginTotal[Controller][Channel][Rank][Tap1ind][0] += (UINT8)(Outputs->MarginResult[LastTxT][Rank][Controller][Channel][0][1] / 10);
            //SumRes += (UINT32) LinearApproximationTradeOff (MrcData, MarginTotal[Controller][Channel][Rank][Tap1ind][0] * 10, 400);

            MarginTotal[Controller][Channel][Rank][Tap1ind][0] =  (UINT8)(Outputs->MarginResult[LastTxV][Rank][Controller][Channel][0][0] / 10);
            MarginTotal[Controller][Channel][Rank][Tap1ind][0] += (UINT8)(Outputs->MarginResult[LastTxV][Rank][Controller][Channel][0][1] / 10);
            SumRes += (UINT32) UPMFilter (MrcData, MarginTotal[Controller][Channel][Rank][Tap1ind][0] * 10, 600);
          }
          if (ResultsSum[Controller][Channel][Rank] < SumRes) { // Save max results
            ResultsSum[Controller][Channel][Rank] = SumRes;
            ResCount[Controller][Channel][Rank] = Tap1ind;
          }
        } // Channel
      } //  Rank
    } // Controller
  } // DFE Tap 1

  // Print results
  MRC_DEBUG_MSG(Debug, MSG_LEVEL_NOTE, "\nResults for DFETap%d: WrV:\nDFETap%d:\t\t", DfeTap, DfeTap);
  for (Tap1ind = 0; Tap1ind < DfeSteps; Tap1ind++) { // DfeTap values
    MRC_DEBUG_MSG(Debug, MSG_LEVEL_NOTE, "%d\t", DfeTap1Val[Tap1ind]);
  }

  for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
    for (Rank = 0; Rank < MAX_RANK_IN_CHANNEL; Rank++) {
      for (Channel = 0; Channel < MaxChannels; Channel++) {
        if (MrcRankExist(MrcData, Controller, Channel, Rank)) {
          MRC_DEBUG_MSG(Debug, MSG_LEVEL_NOTE, "\nMc%u.C%u.R%u:  %4d\t", Controller, Channel, Rank, ResultsSum[Controller][Channel][Rank]);
          for (Tap1ind = 0; Tap1ind < DfeSteps; Tap1ind++) { // DfeTap values
            MRC_DEBUG_MSG(Debug, MSG_LEVEL_NOTE, "%3d\t",
              MarginTotal[Controller][Channel][Rank][Tap1ind][0]
              );
          } // DFE Tap 1
        }
      } // Channel
    } //  Rank
  } // Controller


  // Find Optimal config per Rank
  MRC_DEBUG_MSG(Debug, MSG_LEVEL_NOTE, "\nFinal Settings: \n");
  for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
    for (Rank = 0; Rank < MAX_RANK_IN_CHANNEL; Rank++) {
      RankMask = 1 << Rank;
      for (Channel = 0; Channel < MAX_CHANNEL; Channel++) {
        if (!(MrcRankExist(MrcData, Controller, Channel, Rank))) {
          continue;
        }
        Tap1ind = ResCount[Controller][Channel][Rank];
        Sign = (DfeTap1Val[Tap1ind] < 0) ? MRC_BIT6 : 0;  // direction
        GetSetVal = ABS (DfeTap1Val[Tap1ind]);            // value
        GetSetVal |= Sign;
        GetSetVal |= MRC_BIT7;                            // DFE Enable
        SetDimmParamValue (MrcData, Controller, Channel, RankMask, Param, GetSetVal, 1);
        MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Mc%u.C%u.R%u: DfeTap%d: %3d\n", Controller, Channel, Rank, DfeTap, DfeTap1Val[Tap1ind]);
      } // Channel
    }  // Rank
  }  // Controller

  MRC_DEBUG_MSG(&Outputs->Debug, MSG_LEVEL_NOTE, "Re-center WrV/WrT\n");
  DQTimeCentering1D (MrcData, WrV, 0, OPT_PARAM_LOOP_COUNT, MRC_PWR_DBG_PRINT, FALSE, 1);
  PerBit1DCentering (MrcData, WrT, OPT_PARAM_LOOP_COUNT + 2, 0, MRC_PRINTS_OFF, NULL);
  DataTimeCentering2D (
    MrcData,
    Outputs->MarginResult,
    Outputs->McChBitMask,
    WrT,
    0,
    1,
    0,
    OPT_PARAM_LOOP_COUNT + 2,
    1
  );

  return mrcSuccess;
}

/**
  DIMM DFE training for DDR5 / LPDDR5

  @param[in] MrcData - Include all MRC global data.

  @retval MrcStatus -  if it succeeds return mrcSuccess
**/
MrcStatus
MrcDimmDFETraining (
  IN MrcParameters *const MrcData
  )
{
  MrcStatus Status;
  MrcOutput *Outputs;
  UINT32    DfeTap;
  UINT32    MaxDfeTap;
  BOOLEAN   Ddr5;

  Status  = mrcSuccess;
  Outputs = &MrcData->Outputs;
  Ddr5 = (Outputs->DdrType == MRC_DDR_TYPE_DDR5);

  MaxDfeTap = Ddr5 ? MAX_DFE_TAP_DDR5 : MAX_DFE_TAP_LPDDR5; // DDR5: 4 taps, LP5: 2 taps

  for (DfeTap = 0; DfeTap < MaxDfeTap; DfeTap++) {
    Status |= MrcDimmDfeRankTraining (MrcData, DfeTap + 1, DIMM_DFE_LOOP_COUNT);
  }
  return Status;
}

/**
  Early DIMM DFE training for DDR5.

  @param[in] MrcData - Include all MRC global data.

  @retval MrcStatus -  if it succeeds return mrcSuccess
**/
MrcStatus
MrcDimmDFETrainingEarly (
  IN MrcParameters *const MrcData
  )
{
  MrcStatus Status;
  MrcOutput *Outputs;
  UINT32    DfeTap;
  UINT32    MaxDfeTap;
  BOOLEAN   Ddr5;

  Status  = mrcSuccess;
  Outputs = &MrcData->Outputs;
  Ddr5 = (Outputs->DdrType == MRC_DDR_TYPE_DDR5);

  if (!Ddr5) {
    return Status;
  }
  MaxDfeTap = 1;

  for (DfeTap = 0; DfeTap < MaxDfeTap; DfeTap++) {
    Status |= MrcDimmDfeRankTraining (MrcData, DfeTap + 1, EARLY_DIMM_DFE_LOOP_COUNT);
  }
  return Status;
}
/**
  This function implements Dimm Ron training.

  @param[in] MrcData - Include all MRC global data.

  @retval MrcStatus -  if it succeeds return mrcSuccess
**/
MrcStatus
MrcDimmRonTraining (
  IN MrcParameters *const MrcData
  )
{
  UINT16              SaveMarginsArray[2][MAX_OPT_POINTS][MAX_CONTROLLER][MAX_BYTE_IN_DDR5_CHANNEL * MAX_DIMMS_IN_CHANNEL]; // Must match test list size
  OptOffsetChByte     BestOff;
  MrcOutput           *Outputs;
  MrcDebug            *Debug;
  static const UINT8  TestList[]  = { RdV, RdT };
  UINT8               Scale[]     = { 1, 1, 0, 0, 0 }; // Must specify scale = 0 to unpopulate slots!!
  static const UINT8  OptParam[]  = { OptDimmRon };
  static const UINT8  OptParamDdr5[]  = { OptDimmRonUp, OptDimmRonDn };
  UINT16              *DimmRonVals;
  INT8                Start;
  INT8                Stop;
  INT8                Start1[ARRAY_COUNT (OptParamDdr5)];
  INT8                Stop1[ARRAY_COUNT (OptParamDdr5)];
  INT8                Off;
  BOOLEAN             Ddr5;
  UINT8               ValidChBitMask;
  UINT8               ValidRankMask;

  Outputs = &MrcData->Outputs;
  Debug   = &Outputs->Debug;
  Ddr5    = (Outputs->DdrType == MRC_DDR_TYPE_DDR5);
  ValidChBitMask = Outputs->ValidChBitMask;
  ValidRankMask  = Outputs->ValidRankMask;

  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\nDIMM Ron Offset - Value mapping \nOffset\t Value\n");
  if (!Ddr5) {
    GetDimmOptParamValues (MrcData, OptParam[0], &DimmRonVals, (UINT8 *) &Stop);
    Start = 0;
    for (Off = Start; Off < Stop; Off++) {
      MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "%d:\t %u \n", Off, DimmRonVals[(UINT8)Off]);
    }
    Stop -= 1; // Subtract 1 because GetDimmOptParamValues returns size of parameter array, but stop should be set to ending Index (Size - 1)
  } else {
    GetDimmOptParamValues (MrcData, OptParamDdr5[0], &DimmRonVals, (UINT8 *) &Stop1[0]);
    Start1[0] = Start1[1] = 0;
    for (Off = Start1[0]; Off < Stop1[0]; Off++) {
      MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "%d:\t %u \n", Off, DimmRonVals[(UINT8)Off]);
    }
    Stop1[0] -= 1; // Subtract 1 because GetDimmOptParamValues returns size of parameter array, but stop should be set to ending Index (Size - 1)
    Stop1[1] = Stop1[0];
  }

  // Need to run per channel so the optimal RxVref is the same for all ranks
  if (!Ddr5) {
    TrainDDROptParam (
      MrcData,
      &BestOff,
      ValidChBitMask, // Channels
      ValidRankMask,  // Ranks
      OptParam,
      ARRAY_COUNT (OptParam),
      FullGrid,
      TestList,
      ARRAY_COUNT (TestList),
      Scale,
      NULL,
      &Start,
      &Stop,
      OPT_PARAM_LOOP_COUNT, // Loopcount
      1,                    // Repeats
      0,                    // NoPrint
      0,                    // SkipOdtUpdate
      0,                    // GuardBand
      0,                    // PatType
      SaveMarginsArray,
      FALSE, // UPMFilterEnable
      NULL,  // TestUpmArray
      FALSE  // CliffsFilterEnable
    );
  } else {
    TrainDDROptParam (
      MrcData,
      &BestOff,
      ValidChBitMask, // Channels
      ValidRankMask,  // Ranks
      OptParamDdr5,
      ARRAY_COUNT (OptParamDdr5),
      FullGrid,
      TestList,
      ARRAY_COUNT (TestList),
      Scale,
      NULL,
      Start1,
      Stop1,
      OPT_PARAM_LOOP_COUNT, // Loopcount
      1,                    // Repeats
      0,                    // NoPrint
      0,                    // SkipOdtUpdate
      0,                    // GuardBand
      0,                    // PatType
      SaveMarginsArray,
      FALSE, // UPMFilterEnable
      NULL,  // TestUpmArray
      FALSE  // CliffsFilterEnable
    );
  }

  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Re-center Read Vref \n");
  DQTimeCentering1D (MrcData, RdV, 0, OPT_PARAM_LOOP_COUNT, MRC_PRINTS_OFF, FALSE, 1);

  return mrcSuccess;
}

/**
  This function implements the general training algorithm for DDR and IO parameters
  that impact margin and power.

  This function can train for power or for margin, and the function determines the mode as follows:
    PowerTraining: (NumTests <= MAX_TRADEOFF_TYPES) && (Scale[NumTests] != 0)
    else MarginTraining.

  The Parameters that are supported:
    [0: WrDS, 1: RdODT, 2: SComp, 3: TComp, 3: TxEq, 4: RxEq, 5: RxBias, 6: DimmOdt, 7: DimmOdtWr]

  @param[in,out] MrcData           - Include all MRC global data.
  @param[in,out] BestOff           - Structure containg the best offest and margins (values) for th Opt param.
  @param[in]     McChannelMask     - MCs and Channels to train
  @param[in]     RankMask          - Condenses down the results from multiple ranks
  @param[in]     OptParam          - Defines the OptParam Offsets (e.g. OptRdDqOdt, OptSComp..etc)
  @param[in]     OptParamLen       - Defines the size of OptParam[].
  @param[in]     GridMode          - Selects the way to sweep the params
  @param[in]     TestList          - List of margin params that will be tested (up to 4)
  @param[in]     NumTests          - The length of TestList[].
  @param[in]     Scale             - List of the relative importance between the 4 tests
  @param[in]     UPMOptimize       - Optimize in FindOptimalTradeOff only for UPM limit for selected params, so if they pass UPM they do not affect the score.
  @param[in]     Start             - Start point of sweeping the Comp values
  @param[in]     Stop              - Stop point of sweeping the Comp values
  @param[in]     LoopCount         - The number of loops to run in IO tests.
  @param[in]     CPGCAllRanks      - Run CPGC on all the specified ranks at once. Else, one rank at a time.
  @param[in]     NoPrint           - Switch to disable printing.
  @param[in]     SkipOptUpdate     - Switch to train but not update Opt settings. Not really supported any longer
  @param[in]     GuardBand         - Signed offset to apply to the Opt param best value.
  @param[in]     PatType           - Type of pattern the will be applied for optimization, trying to keep MrcDqPat definitions. If not specified, LFSR VA is used. Allowed values: [StaticPattern (Simple Pattern)]
  @param[in]     SaveMargin        - The array for saving margins in. Can be declared to be only as large as needed to save memory.
  @param[in]     UPMFilterEnable   - Enables UPMFilter if disabled runs in legacy mode
  @param[in]     TestUpmArray      - Relevant UPM array pointer
  @param[in]     CliffsFilterEnable - Enables Cliffs filter (Convolution 2D)

  @retval Nothing
**/
void
TrainDDROptParam (
  IN OUT MrcParameters *const MrcData,
  IN OUT OptOffsetChByte      *BestOff,
  IN     UINT8                McChannelMask,
  IN     UINT8                RankMask,
  IN     const UINT8          OptParam[],
  IN     UINT8                OptParamLen,
  IN     UINT8                GridMode,
  IN     const UINT8          *TestList,
  IN     UINT8                NumTests,
  IN     UINT8                *Scale,
  IN     UINT8                UPMOptimize[MAX_TRADEOFF_TYPES],
  IN     INT8                 Start[],
  IN     INT8                 Stop[],
  IN     UINT8                LoopCount,
  IN     BOOLEAN              CPGCAllRanks,
  IN     BOOLEAN              NoPrint,
  IN     BOOLEAN              SkipOptUpdate, // Not really supported any longer
  IN     INT8                 GuardBand,
  IN     UINT8                PatType,
  IN     UINT16               SaveMargin[][MAX_OPT_POINTS][MAX_CONTROLLER][MAX_BYTE_IN_DDR5_CHANNEL * MAX_DIMMS_IN_CHANNEL],
  IN     BOOLEAN              UPMFilterEnable,
  IN     MrcTestUpmArray*     TestUpmArray,
  IN     BOOLEAN              CliffsFilterEnable
  )
{
  const MrcInput      *Inputs;
  MrcDebug            *Debug;
  const MRC_FUNCTION  *MrcCall;
  MrcOutput           *Outputs;
  MrcChannelOut       *ChannelOut;
  MrcControllerOut    *ControllerOut;
  UINT16              (*MarginByte)[MAX_RESULT_TYPE][MAX_RANK_IN_CHANNEL][MAX_CONTROLLER][MAX_CHANNEL][MAX_SDRAM_IN_DIMM][MAX_EDGES];
  UINT32              BERStats[4];
  INT32               NewVddq;
  INT32               DefaultVddq;
  INT32               TxVrefBaseTicks;
  INT32               CmdVrefBaseTicks;
  INT32               DefaultCmdVref[MAX_CONTROLLER][MAX_CHANNEL][MAX_DIMMS_IN_CHANNEL];
  INT32               DefaultTxVref[MAX_CONTROLLER][MAX_CHANNEL][MAX_RANK_IN_CHANNEL];
  INT32               DefaultRxVref[MAX_CONTROLLER][MAX_CHANNEL][MAX_SDRAM_IN_DIMM];
  UINT16              PostMargin[MAX_MARGINS_TRADEOFF][MAX_CONTROLLER][MAX_CHANNEL][MAX_SDRAM_IN_DIMM];
  UINT8               Test;
  UINT16              MinEye[MAX_CONTROLLER][MAX_CHANNEL];
  UINT16              Margins[MAX_TRADEOFF_TYPES][MAX_OPT_POINTS]; // TestParam X 24 Comp Points
  UINT16              UpmLimits[MAX_TRADEOFF_TYPES];
  UINT16              PwrLimits[MAX_TRADEOFF_TYPES];
  UINT16              MarginResult;
  UINT16              RowNum;
  UINT16              ColNum;
  INT16               Best;
  INT64               GetSetValue = 0;
  UINT8               UPMOptimizeDefaults[MAX_TRADEOFF_TYPES];
  UINT8               OptimizationMode;
  UINT8               ResultType;
  UINT8               AveN;
  UINT8               McChBitMask;
  UINT8               Controller;
  UINT8               Channel;
  UINT8               ChDimm;
  UINT8               Byte;
  UINT8               Rank;
  UINT8               Edge;
  UINT8               FirstRank;
  UINT8               NumBytes;
  UINT8               BMap[MAX_SDRAM_IN_DIMM]; // Need by GetBERMarginByte
  UINT8               Param;
  UINT8               MaxMargin;
  UINT8               localR[MAX_CONTROLLER][MAX_CHANNEL];
  UINT8               FirstRankPerCh[MAX_CONTROLLER][MAX_CHANNEL];
  UINT8               RankRepIndex;
  UINT8               RankRep;
  UINT8               AdjustedLoopCount;
  INT8                CurrentComp;
  UINT8               StepSize = 1;
  UINT8               NumChannels = 0;
  UINT8               CompChecks = 0;
  UINT8               OptCompParams[MAX_COMPS_CHECKED];
  UINT8               RdRd2Test;
  INT8                MaxComp;
  UINT16              OptPower[MAX_CONTROLLER][MAX_CHANNEL][MAX_OPT_POINTS];
  UINT16              Power;
  INT8                Delta;
  UINT8               Index;
  UINT8               OffLen[MAX_GRID_DIM];
  INT8                ParamOff[MAX_GRID_DIM];
  UINT8               LenMargin;
  UINT8               MaxOptPoints;
  UINT8               MinRanksPerChannel;
  UINT8               MaxRanksPerChannel;
  UINT8               NumRanksInChannel;
  UINT8               BitShift;
  BOOLEAN             IncEnds;
  BOOLEAN             IncEndsForPrint;
  BOOLEAN             CPUComp;
  BOOLEAN             printPerCh;
  BOOLEAN             BusFailure = FALSE;
  BOOLEAN             GlobalSetDone = FALSE;
  BOOLEAN             TxVrefJedecReset = FALSE;
  UINT8               OptIdx;
  UINT16              Margin;
  BOOLEAN             IgnoreUpmPwrLimit;
  UINT16              MinChByte;
  UINT8               ChangeRxVrefIndex = 0;
  UINT8               ChangeTxVrefIndex = 0;
  UINT8               ChangeCmdVrefIndex = 0;
  UINT8               MaxRankReps;
  UINT8               BytesToSet[MAX_OPT_PARAM_LENGTH];
  BOOLEAN             GlobalParams[MAX_OPT_PARAM_LENGTH];
  BOOLEAN             PerRankParam;
  BOOLEAN             CCCParam;
  BOOLEAN             AllGlobalParam;
  BOOLEAN             AnyPerRankParam;
  BOOLEAN             AnyGlobalParam;
  BOOLEAN             AnyCCCParam;
  BOOLEAN             UlxUlt;
  BOOLEAN             ChangeRxVref;
  BOOLEAN             ChangeTxVref;
  BOOLEAN             ChangeCmdVref;
  BOOLEAN             ChangeRxVrefNow;
  BOOLEAN             ChangeTxVrefNow;
  BOOLEAN             ChangeCmdVrefNow;
  MC0_CH0_CR_CADB_CFG_STRUCT  CadbConfig;
  BOOLEAN             EnBer;
#ifdef LB_STUB_FLAG
  UINT16                ParamVector[3];
  UINT16                SimMargin;
  MRC_POWER_SYS_CONFIG  SysConfig;
#endif
  OptResultsPerByte calcResultSummary; // Result print summary: 5 columns per byte
  static UINT32 Pattern[8][2] = {
    { 0xF0F0F0F0, 0xF0F0F0F0 },
    { 0xCCCCCCCC, 0xCCCCCCCC },
    { 0x0F0F0F0F, 0x0F0F0F0F },
    { 0x55555555, 0x55555555 },
    { 0xCCCCCCCC, 0xCCCCCCCC },
    { 0xF0F0F0F0, 0xF0F0F0F0 },
    { 0x55555555, 0x55555555 },
    { 0x0F0F0F0F, 0x0F0F0F0F }
  };

  static UINT32 Pattern2[8][2] = {
    { 0xAAAAAAAA, 0x55555555 },
    { 0xAAAAAAAA, 0x55555555 },
    { 0xAAAAAAAA, 0x55555555 },
    { 0xAAAAAAAA, 0x55555555 },
    { 0xAAAAAAAA, 0x55555555 },
    { 0xAAAAAAAA, 0x55555555 },
    { 0xAAAAAAAA, 0x55555555 },
    { 0xAAAAAAAA, 0x55555555 }
  };

  MinRanksPerChannel  = 0xFF;
  MaxRanksPerChannel  = 1;
  NumBytes            = 1;
  DefaultVddq         = 0;
  AllGlobalParam      = TRUE;
  AnyPerRankParam     = FALSE;
  AnyGlobalParam      = FALSE;
  AnyCCCParam         = FALSE;
  PerRankParam        = FALSE;
  CCCParam            = FALSE;
  ResultType          = 0;
  CurrentComp         = 0;
  IncEnds             = 1;
  IncEndsForPrint     = 1;
  printPerCh          = 0;
  ChangeRxVref        = FALSE;
  ChangeTxVref        = FALSE;
  ChangeCmdVref       = FALSE;
  ChangeRxVrefNow     = FALSE;
  ChangeTxVrefNow     = FALSE;
  ChangeCmdVrefNow    = FALSE;
  Inputs              = &MrcData->Inputs;
  MrcCall             = Inputs->Call.Func;
  Outputs             = &MrcData->Outputs;
  Debug               = &Outputs->Debug;
  UlxUlt              = Inputs->UlxUlt;
  MarginByte          = &Outputs->MarginResult;
  McChannelMask      &= Outputs->ValidChBitMask;
  RankMask           &= Outputs->ValidRankMask;
  if (Outputs->Lpddr) {
    TxVrefBaseTicks  = (Outputs->DdrType == MRC_DDR_TYPE_LPDDR4) ? LP4_TXVREF_BASE_TICKS : LP5_TXVREF_BASE_TICKS;
    CmdVrefBaseTicks = (Outputs->DdrType == MRC_DDR_TYPE_LPDDR4) ? LP4_CMDVREF_BASE_TICKS : LP5_CMDVREF_BASE_TICKS;
  } else {
    TxVrefBaseTicks  = DDR4_TXVREF_BASE_TICKS;
    CmdVrefBaseTicks = DDR4_CMDVREF_BASE_TICKS;
  }
  IgnoreUpmPwrLimit = TRUE;
  MrcCall->MrcSetMem ((UINT8 *) &calcResultSummary, sizeof (calcResultSummary), 0);
  MrcCall->MrcSetMem ((UINT8 *) BestOff, sizeof (OptOffsetChByte), 0xFF); // @todo: cleanup multiple clears.
  MrcCall->MrcSetMem ((UINT8 *) Margins, sizeof (Margins), 0);
  MrcCall->MrcSetMem ((UINT8 *) PostMargin, sizeof (PostMargin), 0);
  MrcCall->MrcSetMem ((UINT8 *) OptPower, sizeof (OptPower), 0);
  MrcCall->MrcSetMem ((UINT8 *) localR, sizeof (localR), 0);
  MrcCall->MrcSetMem ((UINT8 *) FirstRankPerCh, sizeof (FirstRankPerCh), 0);
  MrcCall->MrcSetMem ((UINT8 *) BERStats, sizeof (BERStats), 0);
  MrcCall->MrcSetMem ((UINT8 *) OffLen, sizeof (OffLen), 0);
  MrcCall->MrcSetMem ((UINT8 *) GlobalParams, sizeof (GlobalParams), 0);
  MrcCall->MrcSetMem ((UINT8 *) BytesToSet, sizeof (BytesToSet), 1);
  MrcCall->MrcSetMemWord (UpmLimits, MAX_TRADEOFF_TYPES, 0);
  MrcCall->MrcSetMemWord (PwrLimits, MAX_TRADEOFF_TYPES, 0);

  // Set to zero
  for (Test = 0; Test < NumTests; Test++) {
    for (Index = 0; Index < MAX_OPT_POINTS; Index++) {
      for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
        for (Byte = 0; Byte < (MAX_BYTE_IN_DDR5_CHANNEL * MAX_DIMMS_IN_CHANNEL); Byte++) {
          SaveMargin[Test][Index][Controller][Byte] = 0;
        }
      }
    }
  }

  for (Byte = 0; Byte < ARRAY_COUNT (BMap); Byte++) {
    BMap[Byte] = Byte;
  }

  if (Inputs->PowerTrainingMode == MrcTmMargin) {
    if (OptParam[0] == OptPanicVttDnLp) {
      Scale[NumTests] = 1; // Pay attention to power in tradeoff evaluation
      if (!NoPrint) {
        MRC_DEBUG_MSG(Debug, MSG_LEVEL_NOTE, "Optimizing for maximum margins (ignoring UPMs), considering power in optimization\n");
      }
    } else {
      Scale[NumTests] = 0; // If we have this param set, we are optimizing for margins only, so set 'Scale[NumTests]' to 0 to scale power to zero and thus ignore it
      if (!NoPrint) {
        MRC_DEBUG_MSG(Debug, MSG_LEVEL_NOTE, "Optimizing for maximum margins (ignoring UPMs), ignoring power in optimization\n");
      }
    }
    UPMOptimize = NULL; // We want max margins, so don't optimize for UPMs
  } else if (Inputs->PowerTrainingMode == MrcTmPower) {
    if (Scale[NumTests] == 0 && !(OptParam[0] == OptRdDqOdt && Outputs->OdtMode == MrcOdtModeVtt)) { // no power considuration in Vtt when training RxDq ODT
      Scale[NumTests] = 1; // Pay attention to power in tradeoff evaluation
    }
    if (Scale[NumTests] != 0 && Scale[NumTests] != 255) { // 255 specifically ignores power
      if (UPMOptimize == NULL) {
        UPMOptimize = UPMOptimizeDefaults;
        MrcCall->MrcSetMem((UINT8 *) UPMOptimize, sizeof (UPMOptimizeDefaults), 1); // UPM optimize everything unless a specific setup was given
      }
      if (!NoPrint) {
        MRC_DEBUG_MSG(Debug, MSG_LEVEL_NOTE, "Optimizing for minimum power (driving to UPMs), considering power in optimization\n");
      }
    } else {
      Scale[NumTests] = 0;
      UPMOptimize = NULL; // We want max margins, so don't optimize for UPMs
      if (!NoPrint) {
        MRC_DEBUG_MSG(Debug, MSG_LEVEL_NOTE, "Optimizing for maximum margins (ignoring UPMs), ignoring power in optimization\n");
      }
    }
  }

  // Select All Ranks for REUT test
  McChBitMask = 0;
  for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
    ControllerOut = &Outputs->Controller[Controller];
    for (Channel = 0; Channel < Outputs->MaxChannels; Channel++) {
      if (!(MC_CH_MASK_CHECK(McChannelMask, Controller, Channel, Outputs->MaxChannels))) {
        continue;
      }

      ChannelOut      = &ControllerOut->Channel[Channel];
      localR[Controller][Channel] = ChannelOut->ValidRankBitMask & RankMask;

      NumRanksInChannel = 0;
      for (BitShift = 0; BitShift < MAX_BITS * sizeof(localR[Controller][Channel]); BitShift++) {
        if (localR[Controller][Channel] & 0x1 << BitShift) {
          NumRanksInChannel++;
        }
      }
      if (NumRanksInChannel < MinRanksPerChannel) {
        MinRanksPerChannel = NumRanksInChannel;
      }
      if ((NumRanksInChannel > MaxRanksPerChannel) && CPGCAllRanks == FALSE) {
        MaxRanksPerChannel = NumRanksInChannel;
      }

      if (localR[Controller][Channel] != 0x0) {
        NumChannels++;
      }

      // Use McChBitMask from here down - if ch is set that mean at least 1 rank for testing, also remove ch w/o active ranks
      McChBitMask |= SelectReutRanks (MrcData, Controller, Channel, localR[Controller][Channel], FALSE, 0);
      // MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "reut ranks McChBitMask %x Local ranks=%x\n", McChBitMask,localR[Channel]);
      for (Rank = 0; Rank < MAX_RANK_IN_CHANNEL; Rank++) {
        if ((0x1 << Rank) & localR[Controller][Channel]) {
          FirstRankPerCh[Controller][Channel] = Rank;
          break;
        }
      }
    }
  }

  if (McChBitMask == 0) {
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_ERROR, "Error! TrainDDROptParam() McChBitMask == 0 \n");
    return;
  }

  // Find the first selected rank
  FirstRank = GetRankToStoreResults(MrcData, RankMask);

  // Store margin results (per byte or ch)
  for (OptIdx = 0; OptIdx < OptParamLen; OptIdx++) {
    // A param may only be one of these three types, never more.
    // GlobalParams = Train all channels to the same result based on the worst case results across all channels
    GlobalParams[OptIdx] = (OptParam[OptIdx] == OptCCCTco) || (OptParam[OptIdx] == OptVccDLLBypass) || (OptParam[OptIdx] == OptPanicVttDnLp) || (OptParam[OptIdx] == OptVddq);
    // PerRankParam - Train all bytes to the same result based on the worst case across all bytes on all ranks in the rank mask
    PerRankParam = (OptParam[OptIdx] == OptDimmOdt) || (OptParam[OptIdx] == OptDimmOdtWr) || (OptParam[OptIdx] == OptDimmRon) || (OptParam[OptIdx] == OptDimmRonUp) || (OptParam[OptIdx] == OptDimmRonDn) || (OptParam[OptIdx] == OptDimmOdtCA) || (OptParam[OptIdx] == OptDimmOdtPark) || (OptParam[OptIdx] == OptDimmDFETap1) || (OptParam[OptIdx] == OptDimmDFETap2) ||
      (OptParam[OptIdx] == OptDimmDFETap3) || (OptParam[OptIdx] == OptDimmDFETap4) ||
      (OptParam[OptIdx] == OptDimmOdtNom) || (OptParam[OptIdx] == OptDimmOdtParkNT) || (OptParam[OptIdx] == OptDimmOdtNomNT) || ((OptParam[OptIdx] == OptRdDqOdt) && Outputs->Lpddr);
    // CCCParam - CCC params take a different code path. These params only margin CmdT and CmdV.
    CCCParam = (OptParam[OptIdx] == OptCCCDSDnCoarse) || (OptParam[OptIdx] == OptCCCDSUpCoarse) || (OptParam[OptIdx] == OptCCCDS) || (OptParam[OptIdx] == OptCCCTxEq) || (OptParam[OptIdx] == OptCCCSComp);

    AllGlobalParam &= GlobalParams[OptIdx];
    AnyGlobalParam |= GlobalParams[OptIdx];
    AnyPerRankParam |= PerRankParam;
    AnyCCCParam |= CCCParam;
    if ((GlobalParams[OptIdx] == TRUE) || (AnyCCCParam == TRUE)) {
      CPGCAllRanks = TRUE;
    }

    // If there is a 2D sweep with 1 byte param and 1 non-byte param, make them both non-byte
    if (!AnyCCCParam && !AnyPerRankParam && !AnyGlobalParam) {
      NumBytes = (UINT8)Outputs->SdramCount;
    } else {
      NumBytes = 1;
    }

    // Calculate Start/Stop Point for Comp Optimization
    // For TcoComp avoid Reserved comps as it isn't really compensated
    CPUComp = ((OptParam[OptIdx] == OptWrDS) || (OptParam[OptIdx] == OptCCCDS) || (OptParam[OptIdx] == OptWrDSUpCoarse) || (OptParam[OptIdx] == OptWrDSDnCoarse) ||
               (OptParam[OptIdx] == OptCCCDSUpCoarse) || (OptParam[OptIdx] == OptCCCDSDnCoarse) || (OptParam[OptIdx] == OptRdDqOdt) || (OptParam[OptIdx] == OptRdDqsOdt) ||
               (OptParam[OptIdx] == OptSComp) || (OptParam[OptIdx] == OptCCCSComp) || (OptParam[OptIdx] == OptRxLoad));
    if (CPUComp) {
      if (OptParam[OptIdx] == OptSComp) {
        MaxComp = 31;
      } else {
        MaxComp = 63;
      }

      switch(OptParam[OptIdx]) {
        case OptWrDSUpCoarse:
        case OptWrDSDnCoarse:
          StepSize = WR_DS_COARSE_STEP;
          break;
        case OptCCCDSUpCoarse:
        case OptCCCDSDnCoarse:
          StepSize = CCC_DS_COARSE_STEP;
          break;
        case OptWrDS:
          StepSize = WR_DS_STEP;
          break;
        case OptCCCDS:
          StepSize = CCC_DS_STEP;
          break;
        case OptTxEq:
        case OptTxEqCoeff0:
        case OptTxEqCoeff1:
        case OptTxEqCoeff2:
          StepSize = WR_TXEQ_STEP;
          break;
        case OptCCCTxEq:
          StepSize = CCC_TXEQ_STEP;
          break;
        case OptDimmDFETap1:
        case OptDimmDFETap2:
        case OptDimmDFETap3:
        case OptDimmDFETap4:
          if (Outputs->PowerTrainingPerChannel == TRUE) { // Per channel
            StepSize = DFE_STEP_PER_CHANNEL;
          } else {
            StepSize = DFE_STEP;
          }
          break;
        case OptRdDqOdt:
          StepSize = RXDQ_ODT_STEP;
          break;
        case OptRdDqsOdt:
          StepSize = RXDQS_ODT_STEP;
          break;
        case OptRxEq:
          StepSize = RXEQ_STEP;
          break;
        case OptDFETap1:
        case OptDFETap2:
          if (Outputs->PowerTrainingPerChannel == TRUE) { // Per channel
            StepSize = DFE_STEP_PER_CHANNEL;
          } else {
            StepSize = DFE_STEP;
          }
          break;
        case OptRxLoad:
          StepSize = RX_LOAD_STEP;
          break;
        case OptRxBias:
        case OptRxBiasVrefSel:
        case OptRxBiasTailCtl:
          StepSize = RX_BIAS_STEP;
          break;
      }

      // First param is CMD, second is CTL, third is CLK
      switch (OptParam[OptIdx]) {
        case OptCCCDS:
          OptCompParams[0] = OptCmdDS;
          OptCompParams[1] = OptCtlDS;
          CompChecks = 2;
          break;
        case OptCCCDSDnCoarse:
          OptCompParams[0] = OptCmdDSDnCoarse;
          OptCompParams[1] = OptCtlDSDnCoarse;
          CompChecks = 2;
          break;
        case OptCCCDSUpCoarse:
          OptCompParams[0] = OptCmdDSUpCoarse;
          OptCompParams[1] = OptCtlDSUpCoarse;
          CompChecks = 2;
          break;
        case OptCCCSComp:
          OptCompParams[0] = OptCmdSComp;
          OptCompParams[1] = OptCtlSComp;
          CompChecks = 2;
          break;
        default:
          OptCompParams[0] = OptParam[OptIdx];
          CompChecks = 1;
      }
      for (Index = 0; Index < CompChecks * MAX_COMP_DIRECTIONS; Index++) {
        CurrentComp = GetCompCode(MrcData, OptCompParams[Index / MAX_COMP_DIRECTIONS], Index % MAX_COMP_DIRECTIONS);

        if (CurrentComp >= 0) { // Comp is valid
          Delta = CurrentComp - RESERVED_COMP_CODES + Start[OptIdx] * StepSize;
          if (Delta < 0) {
            MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Comp eval %s: Start of sweep range for comp %s is clipped by %d to prevent saturation\n", (Index % MAX_COMP_DIRECTIONS == COMP_UP) ? "Up" : "Down", TOptParamOffsetString[OptCompParams[Index / MAX_COMP_DIRECTIONS]], Delta);
            Start[OptIdx] -= (Delta / (INT8)StepSize + (Delta % (INT8)StepSize == 0 ? 0 : -1)); // Value delta, not total steps, so compute steps here
          }

          Delta = MaxComp - CurrentComp - RESERVED_COMP_CODES - Stop[OptIdx] * StepSize;
          if (Delta < 0) {
            MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Comp eval %s: End of sweep range for comp %s is clipped by %d to prevent saturation\n", (Index % MAX_COMP_DIRECTIONS == COMP_UP) ? "Up" : "Down", TOptParamOffsetString[OptCompParams[Index / MAX_COMP_DIRECTIONS]], Delta);
            Stop[OptIdx] += (Delta / (INT8)StepSize + (Delta % (INT8)StepSize == 0 ? 0 : -1)); // Value delta, not total steps, so compute steps here
          }

          if (OptParam[OptIdx] == OptSComp || OptParam[OptIdx] == OptCCCSComp) {
            Start[OptIdx] -= 1; // Start one index earlier on slew rate training to account for needing a point to test slew rate being disabled. This doesn't violate comp ranges, as the slew rate is set to 0 when disabled.
          }

          if (!SkipOptUpdate && !NoPrint) {
            // print is irrelevant in this case
            MRC_DEBUG_MSG(
              Debug,
              MSG_LEVEL_NOTE,
              "Comp %s, comp eval %s: CurrentComp = %d, CurrentStart = %d, CurrentStop = %d\n",
              TOptParamOffsetString[OptCompParams[Index / MAX_COMP_DIRECTIONS]],
              (Index % MAX_COMP_DIRECTIONS == COMP_UP) ? "Up" : "Down",
              CurrentComp,
              Start[OptIdx],
              Stop[OptIdx]
            );
          }
        }
      }

      if (Stop[OptIdx] < Start[OptIdx]) {
        Stop[OptIdx] = Start[OptIdx];
      }
    }

    if ((OptParam[OptIdx] == OptDimmOdt) || (OptParam[OptIdx] == OptRdDqOdt) || (OptParam[OptIdx] == OptDimmRon) || (OptParam[OptIdx] == OptDimmRonUp) || (OptParam[OptIdx] == OptDimmRonDn) || (OptParam[OptIdx] == OptVccDLLBypass) ||
      (OptParam[OptIdx] == OptDimmOdtPark) || (OptParam[OptIdx] == OptDimmOdtNom) || (OptParam[OptIdx] == OptDimmOdtParkNT) || (OptParam[OptIdx] == OptDimmOdtNomNT)) {
      ChangeRxVref = TRUE;
      if (OptIdx == 1 && ChangeRxVrefIndex != 1) {
        ChangeRxVrefIndex = Stop[0] - Start[0] + 1; // If only one param affects vref, it should always be the second one due to the way indexing works in this function. The range is dictated by the first param.
      } else {
        ChangeRxVrefIndex = 1; // Both params affect vref
      }
    }
    if ((OptParam[OptIdx] == OptDimmOdt) || (OptParam[OptIdx] == OptDimmOdtWr) || (OptParam[OptIdx] == OptWrDS) || (OptParam[OptIdx] == OptWrDSDnCoarse) || (OptParam[OptIdx] == OptWrDSUpCoarse) || (OptParam[OptIdx] == OptDimmDFETap1) || (OptParam[OptIdx] == OptDimmDFETap2) || (OptParam[OptIdx] == OptDimmDFETap3) || (OptParam[OptIdx] == OptDimmDFETap4) ||
      (OptParam[OptIdx] == OptVccDLLBypass) || (OptParam[OptIdx] == OptDimmOdtPark) || (OptParam[OptIdx] == OptDimmOdtNom) || (OptParam[OptIdx] == OptDimmOdtParkNT) || (OptParam[OptIdx] == OptDimmOdtNomNT) || (OptParam[OptIdx] == OptTxEqCoeff0) || (OptParam[OptIdx] == OptTxEqCoeff1) || (OptParam[OptIdx] == OptTxEqCoeff2)) {
      ChangeTxVref = TRUE;
      if (OptIdx == 1 && ChangeTxVrefIndex != 1) {
        ChangeTxVrefIndex = Stop[0] - Start[0] + 1; // If only one param affects vref, it should always be the second one due to the way indexing works in this function. The range is dictated by the first param.
      } else {
        ChangeTxVrefIndex = 1; // Both params affect vref
      }
    }
    if ((OptParam[OptIdx] == OptDimmOdtCA) || (OptParam[OptIdx] == OptCCCDS) || (OptParam[OptIdx] == OptCCCDSDnCoarse) || (OptParam[OptIdx] == OptCCCDSUpCoarse) || (OptParam[OptIdx] == OptVccDLLBypass)) {
      ChangeCmdVref = TRUE;
      if (OptIdx == 1 && ChangeCmdVrefIndex != 1) {
        ChangeCmdVrefIndex = Stop[0] - Start[0] + 1; // If only one param affects vref, it should always be the second one due to the way indexing works in this function. The range is dictated by the first param.
      } else {
        ChangeCmdVrefIndex = 1; // Both params affect vref
      }
    }
  } // End OptIdx loop to store margin results

  // Determine where the power column is, or if we're training for best margin.
  OptimizationMode = ((NumTests < MAX_TRADEOFF_TYPES) && (Scale[NumTests] != 0)) ? NumTests : 100;

  // Loop through all test params and measure margin.
  // Calc total num of points for full grid.
  LenMargin = 1;
  ColNum = 1;
  RowNum = 1;
  for (OptIdx = 0; OptIdx < OptParamLen; OptIdx++) {
    OffLen[OptIdx] = (Stop[OptIdx] - Start[OptIdx] + 1);
    LenMargin *= OffLen[OptIdx];
    BestOff->GridDataSet.Start[OptIdx] = Start[OptIdx];
    BestOff->GridDataSet.OffLen[OptIdx] = OffLen[OptIdx];
    if (0 == OptIdx) {
      ColNum = OffLen[OptIdx];
    } else if (1 == OptIdx) {
      RowNum = OffLen[OptIdx];
    }
  }
  if (SkipOptUpdate) {
    // Just run Margins test
    LenMargin = 1;
    GridMode = FullGrid;
  }

  BestOff->GridDataSet.GridMode = GridMode;
  BestOff->GridDataSet.OptParamLen = OptParamLen;
  MaxOptPoints = MAX_OPT_POINTS;
  if (LenMargin > MaxOptPoints) { // This case will never be hit, as LenMargin is 8 bits. Should make LenMargin 16 bits eventually.
    LenMargin = MaxOptPoints;
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_WARNING, "------------> warning : LenMargin exceed max: %d length is clipped\n", MAX_OPT_POINTS);
  }
  if (!NoPrint) {
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "LenMargin = %d, Test LoopCount = %d\n", LenMargin, LoopCount);
  }

  // Loop Through all Comp Codes
  for (Index = 0; Index < LenMargin; Index++) {
    if (OptParam[0] == OptVddq && BusFailure) { // If the CAC and/or data bus failed for a previous Vddq setting, don't run any more Vddq settings as they should all fail.
      continue;
    } else if (OptParam[0] == OptVddq && Index == 0) {
      // TGL_POWER_TRAINING_VDDQ Replace this when Vddq get/set is complete.
      DefaultVddq = 440 + VDDQ_BASE_TICKS; // MrcGetSetDdrIoGroupController0 (MrcData, Vddq, ReadFromCache, &DefaultVddq);
    }

    if (ChangeRxVref == TRUE && Index % ChangeRxVrefIndex == 0) {
      ChangeRxVrefNow = TRUE;
    }
    if (ChangeTxVref == TRUE && Index % ChangeTxVrefIndex == 0) {
      ChangeTxVrefNow = TRUE;
    }
    if (ChangeCmdVref == TRUE && Index % ChangeCmdVrefIndex == 0) {
      ChangeCmdVrefNow = TRUE;
    }
    // Continue accordingly to GridMode
    if (GetParamsXYZ (MrcData, ParamOff, OptParamLen, GridMode, Index, Start, OffLen)) { // return ParamOff[param]
      continue;
    }
    for (OptIdx = 0; OptIdx < OptParamLen; OptIdx++) {
      // These params are trained per rank(s) at times, but all bytes must still be set anyways.
      if (((OptParam[OptIdx] == OptRdDqOdt) || (OptParam[OptIdx] == OptWrDS)) && AnyPerRankParam) {
        BytesToSet[OptIdx] = Outputs->SdramCount;
      } else {
        BytesToSet[OptIdx] = NumBytes;
      }

      GlobalSetDone = 0;
      for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
        ControllerOut = &Outputs->Controller[Controller];
        for (Channel = 0; Channel < Outputs->MaxChannels; Channel++) {
          if (!(MC_CH_MASK_CHECK(McChBitMask, Controller, Channel, Outputs->MaxChannels))) {
            continue;
          }
          ChannelOut = &ControllerOut->Channel[Channel];

          for (Byte = 0; Byte < BytesToSet[OptIdx]; Byte++) {
            if (!SkipOptUpdate) {
              // Change OpParam offset for all ch/byte/LocalR
              // Note: When using multi OptParams need to take care that one is not overwritten
              //       by the other in UpdateOptParamOffset routine (because UpdateHost=0).
              //       Changed function to always read CSRs to avoid this.
              // Note: Some are limited in range inside e.g: RdOdt +15:-16
              if (GlobalSetDone != 1) {
                UpdateOptParamOffset (MrcData, Controller, Channel, localR[Controller][Channel], Byte, OptParam[OptIdx], ParamOff[OptIdx], TRUE);
              }
              if (GlobalParams[OptIdx]) {
                GlobalSetDone = 1;
              }
            }
            // MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\n--Channel=%d, localR[Controller][Channel]=%x Byte=%d OffsetComp=%d Off=%d\n",Channel,localR[Controller][Channel],Byte,OffsetComp,Off);
          }
        }
      }
    /*
      // If OptParamLen != 1, then the ODT is being run per-rank with another param and we don't want to waste time doing this
      // Setting RxDqODT invalidates the current CTLE/DFE settings
      // @ADL Consider if this step is necessary after ODT
      if (Outputs->Lpddr && (OptParam[OptIdx] == OptRdDqOdt) && (OptParamLen == 1)) {
        LpddrUpdateSocOdt (MrcData);
        ReadEQTraining (MrcData, (MRC_PWR_DBG_PRINT) ? FALSE : TRUE, FALSE, TRUE);
      } else if ((Outputs->DdrType == MRC_DDR_TYPE_DDR4) && (OptParam[OptIdx] == OptRdDqOdt) && (OptParamLen == 1)) {
        //ReadEQTraining(MrcData, (MRC_PWR_DBG_PRINT) ? FALSE : TRUE, FALSE, TRUE);
        //MRC_DEBUG_MSG(Debug, MSG_LEVEL_NOTE, "Nice Try ;)\n");
      }
    */
      GlobalSetDone = 0;

      // Re-center any margins required as a result of updating the optimization param
      if (OptParam[OptIdx] == OptVddq) {
        // Run a JEDEC init in case the last Vddq setting messed up the DRAMs (only needed if running data margins blindly after command margins).
        //MrcResetSequence (MrcData);

        // Let's just re-center the vrefs. The timings will be by width, and should be accurate if the Vrefs are good. This will save time and limit JEDEC inits.
        if (!NoPrint) {
          MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Re-center Cmd Vref, Tx Vref, and Rx Vref by calculation\n");
        }

        NewVddq = DefaultVddq - VDDQ_STEP * ParamOff[OptIdx];

        for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
          for (Channel = 0; Channel < Outputs->MaxChannels; Channel++) {
            if (!(MC_CH_MASK_CHECK(McChBitMask, Controller, Channel, Outputs->MaxChannels))) {
              continue;
            }

            if (Index == 0) { // Cache
              for (ChDimm = 0; ChDimm < MAX_DIMMS_IN_CHANNEL; ChDimm++) {
                MrcGetSetMcChRnk (MrcData, Controller, Channel, MAX_RANK_IN_DIMM * ChDimm, CmdVref, ReadFromCache, &GetSetValue);
                DefaultCmdVref[Controller][Channel][ChDimm] = (INT32) GetSetValue;
                if (UlxUlt) {
                  // Rank parameter is not used
                  break;
                }
              }
            }
            for (ChDimm = 0; ChDimm < MAX_DIMMS_IN_CHANNEL; ChDimm++) {
              GetSetValue = (INT64) ((NewVddq * (DefaultCmdVref[Controller][Channel][ChDimm] + CmdVrefBaseTicks)) / DefaultVddq - CmdVrefBaseTicks);
              MrcGetSetMcChRnk (MrcData, Controller, Channel, MAX_RANK_IN_DIMM * ChDimm, CmdVref, WriteCached, &GetSetValue);
              if (UlxUlt) {
                // Rank parameter is not used
                break;
              }
            }

            for (Rank = 0; Rank < MAX_RANK_IN_CHANNEL; Rank++) {
              if ((0x1 << Rank) & RankMask) {
                continue;
              }

              if (Index == 0) { // Cache
                // TGL_POWER_TRAINING_VDDQ - Add TxVref to Get/Set
                // MrcGetSetDdrIoGroupStrobe (MrcData, Channel, Rank, MRC_IGNORE_ARG, TxVref, ReadFromCache, &DefaultTxVref[Controller][Channel][Rank]);
              }
              GetSetValue = (INT64) ((NewVddq * (DefaultTxVref[Controller][Channel][Rank] + TxVrefBaseTicks)) / DefaultVddq - TxVrefBaseTicks);
              // TGL_POWER_TRAINING_VDDQ - Add TxVref to Get/Set
              // MrcGetSetDdrIoGroupStrobe (MrcData, Channel, Rank, MRC_IGNORE_ARG, TxVref, WriteCached, &GetSetValue);
            }

            for (Byte = 0; Byte < Outputs->SdramCount; Byte++) {
              if (!MrcByteExist (MrcData, Controller, Channel, Byte)) {
                continue;
              }

              if (Index == 0) { // Cache
                MrcGetSetChStrb (MrcData, Controller, Channel, Byte, RxVref, ReadFromCache, &GetSetValue);
                DefaultRxVref[Controller][Channel][Byte] = (INT32) GetSetValue;
              }
              GetSetValue = (INT64) ((DefaultVddq * (DefaultRxVref[Controller][Channel][Byte] + RXVREF_BASE_TICKS)) / NewVddq - RXVREF_BASE_TICKS);
              MrcGetSetChStrb (MrcData, Controller, Channel, Byte, RxVref, WriteCached, &GetSetValue);
            }
          }
        }
      } // If Vddq
    }

    for (OptIdx = 0; OptIdx < OptParamLen; OptIdx++) {
      if (ForceSystemRComp(MrcData, OptParam[OptIdx], FALSE) == TRUE) {
        break;
      }
    }

    if (ChangeRxVrefNow == TRUE) {
      // Set vref accordingly to odt's / Ron's
      MrcSetDefaultRxVref(MrcData, FALSE, MRC_PWR_DBG_PRINT);
    }
    if (ChangeTxVrefNow == TRUE) {
      DQTimeCentering1D (MrcData, WrV, 0, RECENTER_LOOP_COUNT, MRC_PWR_DBG_PRINT, FALSE, 1);
      //DQTimeCentering1D(MrcData, WrV, 0, V_RECENTERING_STEP_SIZE, OPT_PARAM_LOOP_COUNT, MRC_PWR_DBG_PRINT, FALSE, 1);
      ChangeTxVrefNow = FALSE;
    }
    if (ChangeRxVrefNow == TRUE) {
      DQTimeCentering1D (MrcData, RdV, 0, RECENTER_LOOP_COUNT, MRC_PWR_DBG_PRINT, FALSE, 1);
      //DQTimeCentering1D(MrcData, RdV, 0, V_RECENTERING_STEP_SIZE, OPT_PARAM_LOOP_COUNT, MRC_PWR_DBG_PRINT, FALSE, 1);
      ChangeRxVrefNow = FALSE;
    }
    if (ChangeCmdVrefNow == TRUE) {
      CmdVoltageCentering(MrcData, OPT_PARAM_LOOP_COUNT, V_RECENTERING_STEP_SIZE, MRC_PWR_DBG_PRINT);
      ChangeCmdVrefNow = FALSE;
    }

    for (Test = 0; Test < NumTests; Test++) {
      if (OptParam[0] == OptVddq && BusFailure) { // If the CAC and/or data bus failed for this Vddq setting, don't run any more tests.
        continue;
      }

      Param = TestList[Test]; // tl[0]=4 tl[1]=1
      if (PatType == StaticPattern) {
        if (OptParam[0] == OptTxDqTco) {
          SetupIOTestStatic(MrcData, McChBitMask, LoopCount, NSOE, 0, 0, 1, Pattern2);
        } else {
          SetupIOTestStatic(MrcData, McChBitMask, LoopCount, NSOE, 0, 0, 4, Pattern);
        }
        IgnoreUpmPwrLimit = TRUE;
      } else { // LFSR
        if (CPGCAllRanks == FALSE) {
          AdjustedLoopCount = 0;
          while (MinRanksPerChannel > 1) {
            MinRanksPerChannel /= 2;
            AdjustedLoopCount++;
          }
          if (LoopCount > AdjustedLoopCount) {
            AdjustedLoopCount = LoopCount - AdjustedLoopCount; // Adjust loopcount per min number of ranks per channel
          } else {
            AdjustedLoopCount = 1;
          }
        } else {
          AdjustedLoopCount = LoopCount;
        }

        if ((Param == RdT) || (Param == RcvEnaX)) {
          RdRd2Test = Outputs->Lpddr ? RdRdTA_All : RdRdTA;
          if (AdjustedLoopCount > ((Outputs->Lpddr) ? 3 : 1)) {
            AdjustedLoopCount = (Outputs->Lpddr) ? (AdjustedLoopCount - 3) : (AdjustedLoopCount - 1);
          } else {
            AdjustedLoopCount = 1;
          }
        } else {
          RdRd2Test = FALSE;
        }

        if ((Param == CmdT) || (Param == CmdV)) {
          SetupIOTestCADB (MrcData, McChBitMask, AdjustedLoopCount, NSOE, 1, 0);
        } else {
          SetupIOTestBasicVA (MrcData, McChBitMask, AdjustedLoopCount, NSOE, 0, 0, 8, PatWrRd, 0, 0); // Set test to all channels.
        }

        if (RdRd2Test != FALSE) {
          Outputs->DQPat = RdRd2Test;
        }
      }
      ResultType = GetMarginResultType (Param); // rxv=0 rxt=1
                                                // Assign to last pass margin results by reference
                                                // get lowest margin from all ch/rankS/byte save in FirstRank
      GetMarginByte (MrcData, Outputs->MarginResult, Param, FirstRank, RankMask);
      // MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\n--- FirstRank = %d ResultType=%d Param=%d ranks=0x%x\n", FirstRank,ResultType,Param,RankMask);
      // Calculate the MaxMargin for this test
      if ((Param == WrV) || (Param == WrFan3) || (Param == WrFan2) ||
        (Param == RdV) || (Param == RdFan3) || (Param == RdFan2) || (Param == CmdV)) {
        MaxMargin = GetVrefOffsetLimits (MrcData, Param);
      } else {
        MaxMargin = MAX_POSSIBLE_TIME;
      }
      if (IgnoreUpmPwrLimit) {
        // Set Upm / Power limits to maximal search region
        UpmLimits[Test] = 20 * MaxMargin;
        PwrLimits[Test] = 20 * MaxMargin;
      } else {
        UpmLimits[Test] = MrcGetUpmPwrLimit (MrcData, Param, UpmLimit);
        PwrLimits[Test] = MrcGetUpmPwrLimit (MrcData, Param, PowerLimit);
      }
      MaxMargin = MIN (MaxMargin, (UINT8)(PwrLimits[Test] / 20));

      MaxRankReps = ((CPGCAllRanks == TRUE) ? 1 : MAX_RANK_IN_CHANNEL);
      RankRep = 0;
      for (RankRepIndex = 0; RankRepIndex < MaxRankReps; RankRepIndex++) {
        if (!(Outputs->ValidRankMask & ((CPGCAllRanks == TRUE) ? RankMask : (1 << RankRepIndex)))) { // If the rank(s) don't exist, move on
          continue;
        }
        for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
          ControllerOut = &Outputs->Controller[Controller];
          for (Channel = 0; Channel < Outputs->MaxChannels; Channel++) {
            if (!(MC_CH_MASK_CHECK(McChBitMask, Controller, Channel, Outputs->MaxChannels))) {
              continue;
            }
            ChannelOut = &ControllerOut->Channel[Channel];
            if (CPGCAllRanks == TRUE) {
              // We have to call this after SetupIOTestBasicVA, so call it again
              SelectReutRanks(MrcData, Controller, Channel, localR[Controller][Channel], FALSE, 0);
            } else {
              SelectReutRanks(MrcData, Controller, Channel, ChannelOut->ValidRankBitMask & (1 << RankRepIndex), FALSE, 0);
            }
          }
        }

        // Run Margin Test - margin_1d with chosen param
        // run on all ranks but change param only for firstRank??
        EnBer = 1;

#ifndef LB_STUB_FLAG
        if (Param == CmdT) {
          MrcResetSequence(MrcData); // Run this in case the CCC bus was dysfunctional for the last param value, it will reset it for the new param value
          MrcBlockTrainResetToggle(MrcData, FALSE);
          if (Outputs->Lpddr) {
            if (OptParam[0] == OptVddq) {
              // TGL_POWER_TRAINING_VDDQ Margin CMD timings here with ECT patterns. If using LCT patterns, re-enable MrcResetSequence() call further down.
            } else {
              CmdLinearFindEdgesLpddr (MrcData, MrcIterationClock, McChBitMask, RankMask, 0x1F,  MRC_PRINTS_OFF); // 1F means all CMD groups
            }
          } else {
            CmdLinearFindEdges (MrcData, MrcIterationClock, McChBitMask, RankMask, 3, -64, 64, CMD_T_MARGIN_STEP_SIZE, 1, (INT8 *)NULL, TRUE, TRUE);
          }
          // Restore centered value
          for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
            for (Channel = 0; Channel < Outputs->MaxChannels; Channel++) {
              if (!(MC_CH_MASK_CHECK(McChBitMask, Controller, Channel, Outputs->MaxChannels))) {
                continue;
              }
              for (Rank = 0; Rank < MAX_RANK_IN_CHANNEL; Rank++) {
                if (MrcRankExist (MrcData, Controller, Channel, Rank)) {
                  // UpdateHost = 1 so that we reset the IntClkAlignedMargins->Valid flag in ShiftDQPIs
                  ShiftPIforCmdTraining (MrcData, Controller, Channel, MrcIterationClock, 1 << Rank, MRC_IGNORE_ARG_8, 0, 1);
                }
              }
            }
          }
          MrcResetSequence(MrcData);
          MrcBlockTrainResetToggle(MrcData, TRUE);
        } else if (Param == CmdV) {
          MrcResetSequence(MrcData); // Run this in case the CCC bus was dysfunctional for the last param value, it will reset it for the new param value
          if (OptParam[0] == OptVddq) {
            // TGL_POWER_TRAINING_VDDQ Margin CMD voltage here with ECT patterns. If using LCT patterns, re-enable MrcResetSequence() call further down.
          } else {
            MrcGetBERMarginCh (
              MrcData,
              Outputs->MarginResult,
              McChBitMask,
              0xFF,
              RankMask,
              Param,
              0,  // Mode
              0,
              MaxMargin,
              0,
              BERStats
              );
          }
          MrcResetSequence(MrcData);
        } else {
          if (OptParam[0] == OptVccDLLBypass) {
            MrcResetSequence(MrcData); // Run this in case the CCC bus was dysfunctional for the last param value, it will reset it for the new param value
          }
          //MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "MrcGetBERMarginByte McChBitMask=%x FirstRank=%d Param=%d\n",McChBitMask, FirstRank, Param);
          MrcGetBERMarginByte (
            MrcData,
            Outputs->MarginResult,
            McChBitMask,
            RankMask,
            RankMask,
            Param,
            0,  // Mode
            BMap,
            EnBer,
            MaxMargin,
            0,
            BERStats
            );
          if (OptParam[0] == OptVccDLLBypass) {
            MrcResetSequence(MrcData); // Run this in case the CCC bus was dysfunctional for the last param value, it will reset it for the new param value
          }
        }

#endif
        // MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, " finish MrcGetBERMarginByte \n");
        // Record Test Results
        MinChByte = MRC_UINT16_MAX;
        for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
          ControllerOut = &Outputs->Controller[Controller];
          for (Channel = 0; Channel < Outputs->MaxChannels; Channel++) {
            if (!(MC_CH_MASK_CHECK(McChBitMask, Controller, Channel, Outputs->MaxChannels))) {
              continue;
            }
            ChannelOut = &ControllerOut->Channel[Channel];

            if (RankRep == 0) {
              for (Byte = 0; Byte < Outputs->SdramCount; Byte++) {
                SaveMargin[Test][Index][Controller][Channel * Outputs->SdramCount + Byte] = 0;

                if ((Param == CmdT) || (Param == CmdV)) {
                  break;  // Exit per-byte loop
                }
              }

              MinEye[Controller][Channel] = 0xFFFF;
            }

            if (CPGCAllRanks == TRUE) {
              if (!(ChannelOut->ValidRankBitMask & RankMask)) { // If the rank(s) don't exist on this controller/channel, move on
                continue;
              }
            } else {
              if (!(ChannelOut->ValidRankBitMask & (1 << RankRepIndex))) { // If the rank(s) don't exist on this controller/channel, move on
                continue;
              }
            }

            for (Byte = 0; Byte < Outputs->SdramCount; Byte++) {
              if (Param != CmdT) {
                MarginResult = 0;
                for (Edge = 0; Edge < MAX_EDGES; Edge++) {
                  // CmdT margins are stored according to a slightly different convention - Save in first populated rank per channel
                  // All other margins are saved to first populated rank among all channels
                  Margin = (*MarginByte)[ResultType][FirstRank][Controller][Channel][Byte][Edge];
                  MarginResult += Margin;

                  if (Param == WrV && Outputs->DdrType == MRC_DDR_TYPE_DDR4) {
                    // We margin TxVref in PDA mode. If there was no margin at all, we may not have been able to exit PDA mode correctly, so need a JEDEC reset.
                    if (Margin < MARGIN_FAIL) {
                      TxVrefJedecReset = TRUE;
                    }
                  }
                }
              } else {
                MarginResult = (*MarginByte)[ResultType][FirstRankPerCh[Controller][Channel]][Controller][Channel][Byte][0] +
                               (*MarginByte)[ResultType][FirstRankPerCh[Controller][Channel]][Controller][Channel][Byte][1];
              }

              if (OptParam[0] == OptVddq) {
                if (SaveMargin[Test][Index][Controller][Channel * Outputs->SdramCount + Byte] < UpmLimits[Test]) {
                  // This indicates a failure of the CAC or data bus, so set a flag that skips any further tests (and leaves all margins at 0).
                  // This will save time and cause the algorithm to decide this Vddq setting doesn't work.
                  BusFailure = 1;
                }
              }
              if (RankRep == 0 || SaveMargin[Test][Index][Controller][Channel * Outputs->SdramCount + Byte] > MarginResult) {
                SaveMargin[Test][Index][Controller][Channel * Outputs->SdramCount + Byte] = MarginResult;
              }
              if (MinEye[Controller][Channel] > SaveMargin[Test][Index][Controller][Channel * Outputs->SdramCount + Byte]) {
                MinEye[Controller][Channel] = SaveMargin[Test][Index][Controller][Channel * Outputs->SdramCount + Byte];
              }
              if ((Param == CmdT) || (Param == CmdV)) {
                break;  // Exit per-byte loop
              }
            }
            if (AllGlobalParam || (AnyPerRankParam && (RankRep == MaxRanksPerChannel - 1))) {
              SaveMargin[Test][Index][Controller][Channel * Outputs->SdramCount + 0] = MinEye[Controller][Channel];
            }
            if ((Param == CmdT) || (Param == CmdV) || (AnyPerRankParam && (RankRep == MaxRanksPerChannel - 1))) {
              for (Byte = 0; Byte < Outputs->SdramCount; Byte++) { // Copy the first byte results to all the other bytes
                SaveMargin[Test][Index][Controller][Channel * Outputs->SdramCount + Byte] = SaveMargin[Test][Index][Controller][Channel * Outputs->SdramCount + 0];
              }
            }
          }
        }
        if (TxVrefJedecReset == TRUE) {
          MrcResetSequence(MrcData);
          TxVrefJedecReset = FALSE;
        }
        if (AllGlobalParam) {
          // All bytes already collapsed into the first byte for any global param (per channel), so just look at first byte
          for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
            for (Channel = 0; Channel < Outputs->MaxChannels; Channel++) {
              if (!(MC_CH_MASK_CHECK(McChBitMask, Controller, Channel, Outputs->MaxChannels))) {
                continue;
              }
              // MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Ch%u Byte%u MinChByte = %u\n", Channel, Byte, MinChByte);
              MinChByte = MIN (MinChByte, SaveMargin[Test][Index][Controller][Channel * Outputs->SdramCount + 0]);
            }
          }
          // MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "MinChByte = %u\n", MinChByte);
          for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
            // All bytes already collapsed into the first byte for any global param (per channel), so just look at first byte
            for (Channel = 0; Channel < Outputs->MaxChannels; Channel++) {
              if (!(MC_CH_MASK_CHECK(McChBitMask, Controller, Channel, Outputs->MaxChannels))) {
                continue;
              }
              for (Byte = 0; Byte < Outputs->SdramCount; Byte++) {
                SaveMargin[Test][Index][Controller][Channel * Outputs->SdramCount + Byte] = MinChByte;
              }
            }
          }
        }
        RankRep++;
      }

      if ((Param == CmdT) || (Param == CmdV)) {
        // Disable CADB Deselects
        CadbConfig.Data = 0;
        CadbConfig.Bits.CADB_MODE = 0;
        CadbConfig.Bits.CADB_TO_CPGC_BIND = 1;
        Cadb20ConfigRegWrite (MrcData, CadbConfig);
      }
    } // end of test list

    // Use one of the Margin Arrays for fine grain power tradeoffs. This is only used if Scale[NumTests] is not 0, so skip it to save time if possible.
    if (Scale[NumTests] != 0) {
      CalcSysPower(MrcData, (OptParam[0] == OptPanicVttDnLp), 10, OptParam, ParamOff, OptParamLen); // Get the power for the entire system (for however many ranks are being run)
      Power = Outputs->OdtPowerSavingData.MrcSavingWr;
      Margins[NumTests][Index] = Power / (NumBytes * NumChannels); // Average power per byte per channel in the system (for however many ranks are being run)
      for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
        for (Channel = 0; Channel < Outputs->MaxChannels; Channel++) {
          if (!(MC_CH_MASK_CHECK(McChBitMask, Controller, Channel, Outputs->MaxChannels))) {
            continue;
          }
          OptPower[Controller][Channel][Index] = Power / (NumChannels); // Average power per channel in the system (for however many ranks are being run)
        }
      }
    } else {
      for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
        for (Channel = 0; Channel < Outputs->MaxChannels; Channel++) {
          OptPower[Controller][Channel][Index] = 0;
        }
      }
    }
  } // end of offset

  if (AllGlobalParam && !NoPrint) {
    MRC_DEBUG_MSG(Debug, MSG_LEVEL_NOTE, "\n Optimization params are all global. \n Margins shown are the composite worst case for the entire system. \n");
  }
  if (AnyPerRankParam && !NoPrint) {
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\n Optimization params are per rank mask. \n Margins shown are the composite worst case for the entire rank mask. \n");
  }
  if (AnyCCCParam && !NoPrint) {
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\n Optimization params are per channel. \n Margins shown are the composite worst case for the entire channel. \n");
  }

  for (Test = 0; Test < NumTests; Test++) {
#ifdef MRC_DEBUG_PRINT
    if (MRC_PWR_DBG_PRINT) {
      PrintResultTableByte4by24 (
        MrcData,
        McChBitMask,
        SaveMargin,
        Test,
        LenMargin,
        0,
        OptParam[0],
        TestList[Test],
        PwrLimits,
        NoPrint
        );
    }
#endif // MRC_DEBUG_PRINT
  } // end of test list

  // Calculate the best value for every byte
  for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
    ControllerOut = &Outputs->Controller[Controller];
    for (Channel = 0; Channel < Outputs->MaxChannels; Channel++) {
      if (!(MC_CH_MASK_CHECK(McChBitMask, Controller, Channel, Outputs->MaxChannels)) || GlobalSetDone) {
        continue;
      }
       ChannelOut = &ControllerOut->Channel[Channel];
      // no need to run on channel with no selected ranks
      if (!(ChannelOut->ValidRankBitMask & localR[Controller][Channel]) || GlobalSetDone) {
        continue;
      }
      for (Byte = 0; Byte < NumBytes && !GlobalSetDone; Byte++) {
        // Populate Margins array and asymmetric penalty
        for (Test = 0; Test < NumTests; Test++) {
          for (Index = 0; Index < LenMargin; Index++) {
            Margins[Test][Index] = SaveMargin[Test][Index][Controller][Channel * Outputs->SdramCount + Byte];
          }
        }

        // Special Cases for Running Average Filter. Curve smoothing can help us run less points and extrapolate the right settings (assuming non-piecewise sweeps)
        if (OptParamLen == 1) { // 1D case
          if ((OptParam[0] == OptDimmOdt) || (OptParam[0] == OptDimmOdtWr) || (OptParam[0] == OptDimmRon) || (OptParam[0] == OptDimmOdtCA) || (OptParam[0] == OptVccDLLBypass) ||
              (OptParam[0] == OptDimmOdtNom) || (OptParam[0] == OptDimmOdtPark) || (OptParam[0] == OptDimmOdtParkNT) || (OptParam[0] == OptDimmOdtNomNT) ||
              (OptParam[0] == OptPanicVttDnLp) || (OptParam[0] == OptVddq) || (OptParam[0] == OptRxVrefVttDecap)|| (OptParam[0] == OptRxVrefVddqDecap) || (OptParam[0] == OptCCCSComp) ||
              (OptParam[0] == OptSComp) || (OptParam[0] == OptDFETap1) || (OptParam[0] == OptDFETap2)) {
            AveN = 1;
          } else {
            AveN = 3;
          }

          if (LenMargin < AveN) {
            AveN = LenMargin - 1;
          }
        } else {
          // 2D case (for now)
          if ((OptParam[0] == OptTxEq) || (OptParam[0] == OptTxEqCoeff0) || (OptParam[0] == OptTxEqCoeff1) || (OptParam[0] == OptTxEqCoeff2) || (OptParam[0] == OptWrDS) || (OptParam[0] == OptCCCTxEq) || (OptParam[0] == OptCCCDS) || (OptParam[0] == OptRxC) || (OptParam[0] == OptRxR) || (OptParam[0] == OptDimmRonUp) || (OptParam[0] == OptDimmRonDn)) {
            AveN = 3;
          } else {
            AveN = 1;
          }

          for (Test = 0; Test < NumTests; Test++) {
            if (GridMode < FullGrid) {
              Fill2DAverage (MrcData, Margins, Test, LenMargin, OffLen[0], 0, 1);
              for (Index = 0; Index < LenMargin; Index++) { // refill the SaveMargin array after filling the gaps
                SaveMargin[Test][Index][Controller][Channel * Outputs->SdramCount + Byte] = Margins[Test][Index];
              }
            }
            if (AveN != 1) {
              RunningAverage2D(&Margins[Test][0], LenMargin, OffLen[0], 0, 1);
            }
          }
        }

        // need to provide set of power numbers depending on the OffsetComp codes (per byte)for trend line.
        NormalizePowerToMargins (MrcData, Margins, MAX_OPT_POINTS, LenMargin, NumTests);

        // Use that value to create Margin Results based on power.
        // Creates a smooth, linear function that goes from MaxSum to N/(N-1)*MaxSum
        // RatioNum = FinePwrRatio[OptParam] * LenMargin; //e.g FinePwrRatio[RdOdt]=5
        // Find the Best Overall Setting
        // senSq=0,caleM=1,powerOpHigh=0
        FindOptimalTradeOff (
          MrcData,
          &calcResultSummary,
          &Margins[0][0],
          MAX_OPT_POINTS,
          LenMargin,
          Scale,
          UPMOptimize,
          0,
          AveN,
          IncEnds,
          1,
          UpmLimits,
          OptimizationMode,
          GuardBand,
          UPMFilterEnable,
          TestUpmArray,
          CliffsFilterEnable,
          RowNum,
          ColNum
          );

        // Get the best index considering the GuardBand
        Best = calcResultSummary.Best + calcResultSummary.GuardBand;
        for (Test = 0; Test < NumTests; Test++) {
          // PostMargin will be reported back to DimmOdt
          // in old method you can get non coherency between result here and what reported out
          PostMargin[Test][Controller][Channel][Byte] = calcResultSummary.Margins[Test][Best].EW;
        }

        // Best is Index, need to convert to OptParam Offset (ParamOff)
        //Best -= Shift  // update take offset look like bug

        GetParamsXYZ (MrcData, ParamOff, OptParamLen, GridMode, (UINT8) Best, Start, OffLen);
        for (OptIdx = 0; OptIdx < OptParamLen; OptIdx++) {
          if (NumBytes == 1 && BytesToSet[OptIdx] == Outputs->SdramCount) {
            for (Byte = 0; Byte < BytesToSet[OptIdx]; Byte++) {
              UpdateOptParamOffset(MrcData, Controller, Channel, localR[Controller][Channel], Byte, OptParam[OptIdx], ParamOff[OptIdx], TRUE);
            }
            Byte = 0; // Reset for rest of loop
          } else {
            UpdateOptParamOffset(MrcData, Controller, Channel, localR[Controller][Channel], Byte, OptParam[OptIdx], ParamOff[OptIdx], TRUE);
          }
        }

        // MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, " localR[Channel]=%x Best =%d ch=%d byte=%d \n",localR[Controller][Channel],(UINT8) Best,Channel,Byte);
        BestOff->Offset[Controller][Channel * Outputs->SdramCount + Byte] = Best;
#ifdef MRC_DEBUG_PRINT
        for (Test = 0; Test < NumTests; Test++) {
          Print2DResultTableChByte (
            MrcData,
            Controller,
            Channel,
            Byte,
            &calcResultSummary,
            BestOff,
            TestList[Test],
            LenMargin,
            Start,
            Stop,
            OptParam,
            OffLen,
            PwrLimits,
            OptParamLen,
            Test,
            NumTests,
            NoPrint
            );
        }
#endif // MRC_DEBUG_PRINT

        if (AllGlobalParam) {
          GlobalSetDone = 1;
        }
      } // end byte

      // Print After Calc (Stack Size Reduction)
      // Printing the results
#ifdef MRC_DEBUG_PRINT
      if (0) {
        // printing the results
        // @todo: make some loop here
        IncEndsForPrint =
          (
          OptParam[0] == OptDimmOdt ||
          OptParam[0] == OptDimmOdtWr ||
          OptParam[0] == OptDimmRon ||
          OptParam[0] == OptRxEq ||
          IncEnds
          );
        printPerCh = (OptParam[0] == OptDimmOdt || OptParam[0] == OptDimmOdtWr || OptParam[0] == OptDimmRon);

        // lower bytes
        PrintCalcResultTableCh (
          MrcData,
          &calcResultSummary,
          TestList,
          NumTests,
          LenMargin,
          0,
          IncEndsForPrint,
          OptParam[0],
          OptPower[Controller][Channel],
          Controller,
          Channel,
          localR[Controller][Channel],
          Scale[NumTests],
          0,
          printPerCh,
          NoPrint
          );

        // higher bytes
        if (!printPerCh) {
          PrintCalcResultTableCh (
            MrcData,
            &calcResultSummary,
            TestList,
            NumTests,
            LenMargin,
            0,
            IncEndsForPrint,
            OptParam[0],
            OptPower[Controller][Channel],
            Controller,
            Channel,
            localR[Controller][Channel],
            Scale[NumTests],
            1,
            printPerCh,
            NoPrint
            );
          }
        } // if (0)
#endif // MRC_DEBUG_PRINT
    } // End of Calculating best value (ch)
  }

  for (OptIdx = 0; OptIdx < OptParamLen; OptIdx++) {
    if (ForceSystemRComp(MrcData, OptParam[OptIdx], FALSE) == TRUE) {
      break;
    }
  }

  // ** Disabled SOC update - for better margin
  //for (OptIdx = 0; OptIdx < OptParamLen; OptIdx++) {
  //  if (Outputs->Lpddr && OptParam[OptIdx] == OptRdDqOdt) {
  //    LpddrUpdateSocOdt(MrcData);
  //    break;
  //  }
  //}

  // Update the LastPass points in host
  for (Test = 0; Test < NumTests; Test++) {
    for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
      for (Channel = 0; Channel < Outputs->MaxChannels; Channel++) {
        if (!(MC_CH_MASK_CHECK(McChBitMask, Controller, Channel, Outputs->MaxChannels))) {
          continue;
        }

        ResultType = GetMarginResultType(TestList[Test]);
        for (Byte = 0; Byte < NumBytes; Byte++) {
          // save the margins in best offset point for each byte/ch in  rank 0/1
          Best = (UINT8)(BestOff->Offset[Controller][Channel * Outputs->SdramCount + Byte]);

          for (Rank = 0; Rank < MAX_RANK_IN_CHANNEL; Rank++) {
            if (((0x1 << Rank) & localR[Controller][Channel]) == 0x0 || !(MrcRankExist(MrcData, Controller, Channel, Rank))) {
              continue;
            }
            (*MarginByte)[ResultType][Rank][Controller][Channel][Byte][0] = SaveMargin[Test][Best][Controller][Channel * Outputs->SdramCount + Byte] / 2;
            (*MarginByte)[ResultType][Rank][Controller][Channel][Byte][1] = SaveMargin[Test][Best][Controller][Channel * Outputs->SdramCount + Byte] / 2;
          }
          // MRC_DEBUG_MSG (Debug, MSG_LEVEL_ERROR, "best offset= %d ;byte=%d ;(*MarginByte)[ResultType][0][Controller][Channel][Byte][0] -%d (*MarginByte)[ResultType][0][Controller][Channel][Byte][1] -%d add=%d\n",(UINT8) (BestOff->Offset[Channel][Byte] - Start),Byte,(UINT16) (*MarginByte)[ResultType][0][/**Controller**/ CONTROLLER_0][Channel][Byte][0] , (*MarginByte)[ResultType][0][/**Controller**/ CONTROLLER_0][Channel][Byte][1],((UINT16) (*MarginByte)[ResultType][0][/**Controller**/ CONTROLLER_0][Channel][Byte][0] + (UINT16)(*MarginByte)[ResultType][0][/**Controller**/ CONTROLLER_0][Channel][Byte][1]));
        }
      }
    }
    ScaleMarginByte (MrcData, Outputs->MarginResult, TestList[Test], 0);
  }

  // Save the BestOff Values
  BestOff->NumTests = NumTests;
  for (Test = 0; Test < NumTests; Test++) {
    for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
      for (Channel = 0; Channel < Outputs->MaxChannels; Channel++) {
        if (!(MC_CH_MASK_CHECK(McChBitMask, Controller, Channel, Outputs->MaxChannels))) {
          continue;
        }
        // Track minimum eye width per ch
        for (Byte = 0; Byte < NumBytes; Byte++) {
          if (Byte == 0) {
            BestOff->Margins[Test][Controller][Channel] = PostMargin[Test][Controller][Channel][0];
          } else if (BestOff->Margins[Test][Controller][Channel] > PostMargin[Test][Controller][Channel][Byte]) {
            BestOff->Margins[Test][Controller][Channel] = PostMargin[Test][Controller][Channel][Byte];
          }
        }
        BestOff->TestList[Test][Controller][Channel] = TestList[Test];
        // @todo : add OptParam list
      }
    }
  }

  return;
}

/**
  This function calculates the average Rx ODT impedance for the given channel/subchannel.

  @param[in]  MrcData - Pointer to global MRC data.
  @param[in]  Channel - 0-based index to legacy channel (x64 bit)
  @param[in]  SubCh   - 0-based index to enhanced channel (x32 bit).

  @retval - UINT16: Impedance in Ohms.
**/
UINT16
CalcAverageByteImpedance (
  IN MrcParameters *const MrcData,
  IN UINT8                Controller,
  IN UINT8                Channel
  )
{
  MrcOutput      *Outputs;
  UINT8           Byte;
  UINT8           Bytes;
  UINT8           Rank;
  UINT16          ByteImpedance;
  MrcDebug       *Debug;

  Outputs = &MrcData->Outputs;
  Debug = &Outputs->Debug;
  ByteImpedance = 0;
  Bytes = 0;

  for (Rank = 0; Rank < MAX_RANK_IN_CHANNEL; Rank++) {
    if (!(MrcRankExist (MrcData, Controller, Channel, Rank))) {
      continue;
    }

    for (Byte = 0; Byte < MAX_SDRAM_IN_DIMM; Byte++) {
      if (!MrcByteExist (MrcData, Controller, Channel, Byte)) {
        continue;
      }

      ByteImpedance += CalcCpuImpedance (MrcData, Controller, Channel, Rank, Byte, OptRdOdt, FALSE, FALSE, FALSE, FALSE, 0);
      MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Ctrl%u.Ch%u.B%u - ByteImpedance %u\n", Controller, Channel, Byte, ByteImpedance);
      Bytes++;
    }
  }

  ByteImpedance /= Bytes;

  return ByteImpedance ;
}

/**
  This function is responsible for updating the SOC ODT value in LPDDR4 memory based on the CPU ODT settings.
  This reads the CPU ODT using the current comp settings, find the closest SOC ODT settings and programs it to the corresponding MR.
  Note - calculating CPU impedance assumes the cached value holds the current settings.

  @param[in]  MrcData - Pointer to global MRC data.

  @retval -Nothing.
**/
void
Lpddr4UpdateSocOdt (
  IN MrcParameters *const MrcData
  )
{
  // MrcDebug        *Debug;
  // MrcOutput       *Outputs;
  UINT8           Channel;
  UINT8           Controller;
  UINT16          CpuImpedance;
  UINT16          SocOdt;

  // Outputs = &MrcData->Outputs;
  // Debug   = &Outputs->Debug;

  for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
    for (Channel = 0; Channel < MAX_CHANNEL; Channel++) {
      if (!(MrcChannelExist (MrcData, Controller, Channel))) {
        continue;
      }

      CpuImpedance = CalcAverageByteImpedance (MrcData, Controller, Channel);
      SocOdt = MrcCheckForSocOdtEnc (MrcData, CpuImpedance);
      SetDimmParamValue (MrcData, Controller, Channel, 0xF, OptDimmSocOdt, SocOdt, TRUE);
    }
  }
}


/**
This function implements Read ODT training.
Optimize Read ODT strength for performance & power

@param[in] MrcData - Include all MRC global data.

@retval MrcStatus - If it succeeded return mrcSuccess
**/
MrcStatus
MrcReadODTTraining(
  IN MrcParameters *const MrcData
)
{
  UINT16          SaveMarginsArray[3][MAX_OPT_POINTS][MAX_CONTROLLER][MAX_BYTE_IN_DDR5_CHANNEL * MAX_DIMMS_IN_CHANNEL]; // Must match test list size
  MrcStatus       Status;
  MrcOutput       *Outputs;
  MrcDebug        *Debug;
  INT8            StartDq;
  INT8            StopDq;
  OptOffsetChByte BestOff;
  UINT8           OptParamDq[]  = { OptRdDqOdt };
  UINT8           TestListDq[]  = { RdV, RdT };
  UINT8           ScaleDq[]     = { 1, 1, 0, 0, 0 };
  UINT8           CurrentCompDn;
  UINT8           CurrentCompUp;
  UINT8           OdtCodeMin;
  UINT8           OdtCodeMax;
  UINT8           GuardBand;
  INT8            Delta;
  INT64           GetSetVal;

  Outputs = &MrcData->Outputs;
  Debug   = &Outputs->Debug;
  Status  = mrcSuccess;
  GuardBand = 6; // GB from Comp code edges

  StartDq  = OptParamLimitValue (MrcData, OptParamDq[0], 0);
  StopDq   = OptParamLimitValue (MrcData, OptParamDq[0], 1);

  MrcGetSetNoScope (MrcData, CompRcompOdtUp, ReadUncached, &GetSetVal);
  CurrentCompUp = (INT8) GetSetVal;
  MrcGetSetNoScope (MrcData, CompRcompOdtDn, ReadUncached, &GetSetVal);
  CurrentCompDn = (INT8) GetSetVal;
  OdtCodeMin = MIN (CurrentCompUp, CurrentCompDn);
  OdtCodeMax = MAX (CurrentCompUp, CurrentCompDn);

  // Check Comp code limit on start and stop
  Delta = OdtCodeMin + StartDq;
  if (Delta < GuardBand) {
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "------------> warning offset range is clipped by %d\n", Delta);
    StartDq -= Delta;
  }

  Delta = (63 - GuardBand) - OdtCodeMax - StopDq;
  if (Delta < GuardBand) {
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "------------> warning offset range is clipped by %d\n", Delta);
    StopDq += Delta;
  }

  TrainDDROptParam (
    MrcData,
    &BestOff,
    Outputs->ValidChBitMask,
    Outputs->ValidRankMask,
    OptParamDq,
    ARRAY_COUNT (OptParamDq),
    FullGrid,
    TestListDq,
    ARRAY_COUNT(TestListDq),
    ScaleDq,
    NULL,
    &StartDq,
    &StopDq,
    OPT_PARAM_LOOP_COUNT + 2,
    1,              // Repeats
    0,              // NoPrint
    0,              // SkipOptUpdate
    0,              // GuardBand
    0,              // PatType
    SaveMarginsArray,
    FALSE, // UPMFilterEnable
    NULL,  // TestUpmArray
    FALSE  // CliffsFilterEnable
  );

  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Re-center RdV\n");
  DQTimeCentering1D (MrcData, RdV, 0, OPT_PARAM_LOOP_COUNT, MRC_PWR_DBG_PRINT, FALSE, 1);

  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Re-center RdT\n");
  PerBit1DCentering (MrcData, RdTP, OPT_PARAM_LOOP_COUNT, 0, MRC_PRINTS_OFF, NULL);
  PerBit1DCentering (MrcData, RdTN, OPT_PARAM_LOOP_COUNT, 0, MRC_PRINTS_OFF, NULL);
  Status = DataTimeCentering2D (
    MrcData,
    Outputs->MarginResult,
    Outputs->McChBitMask,
    RdT,
    0,
    1,
    0,
    OPT_PARAM_LOOP_COUNT,
    1
  );

  return Status;
}

/**
  This function implements Write Slew Rate training.
  Optimize Write Slew Rate for performance & power

  @param[in] MrcData - Include all MRC global data.

  @retval mrcSuccess
**/
MrcStatus
MrcWriteSlewRate (
  IN MrcParameters *const MrcData
  )
{
  UINT16              SaveMarginsArray[2][MAX_OPT_POINTS][MAX_CONTROLLER][MAX_BYTE_IN_DDR5_CHANNEL * MAX_DIMMS_IN_CHANNEL]; // Must match test list size
  static const UINT8  TestList[] = { WrV, WrT };
  static const UINT8  OptParam[] = { OptSComp };
  UINT8               Scale[] = { 1, 1, 0, 0, 0 };
  OptOffsetChByte     BestOff;
  INT8                Start;
  INT8                Stop;
  MrcOutput          *Outputs;

  Outputs = &MrcData->Outputs;

  if (Outputs->Lpddr) {
      return mrcSuccess;
  }

  Start = OptParamLimitValue (MrcData, OptParam[0], 0);
  Stop  = OptParamLimitValue (MrcData, OptParam[0], 1);

  TrainDDROptParam (
    MrcData,
    &BestOff,
    MrcData->Outputs.ValidChBitMask,
    MrcData->Outputs.ValidRankMask,
    OptParam,
    ARRAY_COUNT (OptParam),
    CustomSR,
    TestList,
    ARRAY_COUNT (TestList),
    Scale,
    NULL,
    &Start,            // Start
    &Stop,             // Stop
    OPT_PARAM_LOOP_COUNT + 1,
    1,                // Repeats
    0,                // NoPrint
    0,                // SkipOdtUpdate
    0,                // GuardBand
    0,                 // PatType
    SaveMarginsArray,
    FALSE, // UPMFilterEnable
    NULL,  // TestUpmArray
    FALSE  // CliffsFilterEnable
    );

  MRC_DEBUG_MSG(&Outputs->Debug, MSG_LEVEL_NOTE, "Re-center Write Vref\n");
  //DQTimeCentering1D(MrcData, WrV, 0, V_RECENTERING_STEP_SIZE, OPT_PARAM_LOOP_COUNT, MRC_PWR_DBG_PRINT, FALSE, 1);

  MRC_DEBUG_MSG(&Outputs->Debug, MSG_LEVEL_NOTE, "Re-center Write Timing \n");
  //DQTimeCentering1D(MrcData, WrT, 0, T_RECENTERING_STEP_SIZE, OPT_PARAM_LOOP_COUNT, MRC_PWR_DBG_PRINT, FALSE, 1); // No need to use WrTLp4 after basic training

  return mrcSuccess;
}

/**
  Updates a given ch/Rank/byte combination with a new value for OptParam
  OptParam can be: WrDS, RdOdt, TComp, SComp, RxEq, TxEq, RxBias or DimmOdt
  OptParam == OptDefault restore values from Host except Dimms Odt's
  @param[in,out] MrcData         - Include all MRC global data.
  @param[in]     Controller      - Controller index to work on.
  @param[in]     Channel         - Channel index to work on.
  @param[in]     Ranks           - Condenses down the results from multiple ranks
  @param[in]     Byte            - Byte index to work on.
  @param[in]     OptParam        - Defines the OptParam Offsets.
                                   Supported OptParam = [0: WrDS, 1: RdODT, 2: SComp, 3: TComp, 4: TxEq,
                                                         5: RxEq, 6: RxBias, 7: DimmOdt, 8: DimmOdtWr]
  @param[in]     Off             - Offset
  @param[in]     UpdateHost      - Desides if MrcData has to be updated. Should always use TRUE for now,
                                   since many multi-D sweeps are setting params within the same registers.

  @retval Nothing
**/
void
UpdateOptParamOffset (
  IN OUT MrcParameters *const MrcData,
  IN     const UINT8          Controller,
  IN     const UINT8          Channel,
  IN           UINT8          Ranks,
  IN     const UINT8          Byte,
  IN           UINT8          OptParam,
  IN           INT32          Off,
  IN     const UINT8          UpdateHost
  )
{

  UINT16         *DimmOptParamVals;
  UINT8           NumDimmOptParamVals;

  MrcOutput       *Outputs;
  MrcChannelOut   *ChannelOut;
  INT64           GetSetVal;
  UINT8           GsmMode;
  BOOLEAN         Lpddr;
  BOOLEAN         Ddr5;
  UINT8           RankMask;
  UINT8           NumRanksInDimm = 0;
  UINT8           NumRanksInCurrentDimm;
  UINT8           Dimm;
  UINT32          Rank;
  UINT8           Index;
  UINT8           TotalIndex;
  UINT32          Offset;
  UINT32          TransController;
  UINT32          TransChannel;
  UINT32          TransStrobe;
  UINT8           Sign;
  INT16           OffMin = 0;
  INT16           OffMax = 0;
  //INT64           Min;
  //INT64           Max;
  //UINT32          Delay;
  MrcStatus       Status;
  DATA0CH0_CR_DDRCRDATAOFFSETCOMP_STRUCT DdrCrDataOffsetComp;
  CH0CCC_CR_DDRCRCTLCACOMPOFFSET_STRUCT DdrCrCmdOffsetComp;

  Outputs       = &MrcData->Outputs;
  Lpddr         = Outputs->Lpddr;
  ChannelOut    = &Outputs->Controller[Controller].Channel[Channel];
  Ddr5          = (Outputs->DdrType == MRC_DDR_TYPE_DDR5);

  //Rank = 0;
  if (OptParam == OptVddq) {
    GsmMode = (UpdateHost) ? ForceWriteOffsetCached : ForceWriteOffsetUncached;
  } else {
    GsmMode = (UpdateHost) ? ForceWriteCached : ForceWriteUncached;
  }
  GsmMode |= GSM_READ_CSR; // We are updating multiple params in the same register without updating the cache, so we are forced to read the CSRs now.

  if (MRC_PWR_DBG_PRINT) {
    GsmMode |= GSM_PRINT_VAL;
  }

  switch (OptParam) {
    case OptTxDqTco:
      GetSetVal = DQ_TCO_COMP_STEP * Off;
      MrcGetSetStrobe(MrcData, Controller, Channel, MRC_IGNORE_ARG, Byte, TxTco, GsmMode, &GetSetVal);
      break;

    case OptTxDqsPTco:
      GetSetVal = DQS_TCO_COMP_STEP * Off;
      MrcGetSetStrobe(MrcData, Controller, Channel, MRC_IGNORE_ARG, Byte, TxDqsTcoP, GsmMode, &GetSetVal);
      break;

    case OptTxDqsNTco:
      GetSetVal = DQS_TCO_COMP_STEP * Off;
      MrcGetSetStrobe(MrcData, Controller, Channel, MRC_IGNORE_ARG, Byte, TxDqsTcoN, GsmMode, &GetSetVal);
      break;
      break;

    case OptCCCTco:
      GetSetVal = CLK_TCO_COMP_STEP * Off;
      MrcGetSetNoScope(MrcData, TcoCompCodeCmd, GsmMode, &GetSetVal);
      MrcGetSetNoScope(MrcData, TcoCompCodeCtl, GsmMode, &GetSetVal);
      break;

    case OptRdDqsOdt:
      //GetSetVal = (Off < 0) ? Off * RXDQS_ODT_STEP + MRC_BIT5 : Off * RXDQS_ODT_STEP; // Convert to 5-bit 2's complement
      GetSetVal = Off * RXDQS_ODT_STEP;
      MrcGetSetChStrb(MrcData, Controller, Channel, Byte, DqsOdtCompOffset, GsmMode, &GetSetVal);
      break;

    case DqsOdtCompOffset:
      //GetSetVal = (Off < 0) ? Off + MRC_BIT5 : Off; // Convert to 5-bit 2's complement
      GetSetVal = Off;
      MrcGetSetChStrb(MrcData, Controller, Channel, Byte, DqsOdtCompOffset, GsmMode, &GetSetVal);
      break;

    case OptWrDS:
    case OptWrDSUpCoarse:
    case OptWrDSDnCoarse:
    case DqDrvUpCompOffset:
    case DqDrvDnCompOffset:
    case OptRdDqOdt:
    case DqOdtCompOffset:
    case OptRxLoad:
    case RloadCompOffset:
    case OptSComp:
    case DqSCompOffset:
      DdrCrDataOffsetComp.Data = ChannelOut->DataCompOffset[Byte];
      switch(OptParam) {
        case OptWrDS:
          OffMin = -32 / WR_DS_STEP;
          OffMax = 31 / WR_DS_STEP;
          break;
        case OptWrDSUpCoarse:
        case OptWrDSDnCoarse:
          OffMin = -32 / WR_DS_COARSE_STEP;
          OffMax = 31 / WR_DS_COARSE_STEP;
          break;
        case DqDrvUpCompOffset:
        case DqDrvDnCompOffset:
          OffMin = -32;
          OffMax = 31;
          break;
        case OptRdDqOdt:
          OffMin = -16 / RXDQ_ODT_STEP;
          OffMax = 15 / RXDQ_ODT_STEP;
          break;
        case OptRxLoad:
          OffMin = -16 / RX_LOAD_STEP;
          OffMax = 15 / RX_LOAD_STEP;
          break;
        case DqOdtCompOffset:
         case RloadCompOffset:
          OffMin = -16;
          OffMax = 15;
          break;
        case OptSComp:
        case DqSCompOffset:
          OffMin = -17; // -17 for SR dis.
          OffMax = 15;
          break;
      }

      if (Off > OffMax) {
        Off = OffMax;
      } else if (Off < OffMin) {
        Off = OffMin;
      }

      switch (OptParam) {
        case OptWrDS:
          DdrCrDataOffsetComp.Bits.dqdrvupcompoffset = Off * WR_DS_STEP;
          DdrCrDataOffsetComp.Bits.dqdrvdowncompoffset = Off * WR_DS_STEP;
          break;

        case OptWrDSUpCoarse:
          DdrCrDataOffsetComp.Bits.dqdrvupcompoffset  = Off * WR_DS_COARSE_STEP;
          break;

        case OptWrDSDnCoarse:
          DdrCrDataOffsetComp.Bits.dqdrvdowncompoffset = Off * WR_DS_COARSE_STEP;
          break;

        case DqDrvUpCompOffset:
          DdrCrDataOffsetComp.Bits.dqdrvupcompoffset = Off;
          break;

        case DqDrvDnCompOffset:
          DdrCrDataOffsetComp.Bits.dqdrvdowncompoffset = Off;
          break;

        case OptRdDqOdt:
          //GetSetVal = (Off < 0) ? Off * RXDQ_ODT_STEP + MRC_BIT5 : Off * RXDQ_ODT_STEP; // Convert to 5-bit 2's complement
          GetSetVal = Off * RXDQ_ODT_STEP;
          DdrCrDataOffsetComp.Bits.dqodtcompoffset = (INT32)GetSetVal;
          break;
        case DqOdtCompOffset:
          //GetSetVal = (Off < 0) ? Off + MRC_BIT5 : Off; // Convert to 5-bit 2's complement
          GetSetVal = Off;
          DdrCrDataOffsetComp.Bits.dqodtcompoffset = (INT32)GetSetVal;
          break;

        case OptRxLoad:
          /*@todo_adl
          DdrCrDataOffsetComp.Bits.RloadOffset = Off * RX_LOAD_STEP;
          */
          break;
        case RloadCompOffset:
          /*@todo_adl
          DdrCrDataOffsetComp.Bits.RloadOffset = Off;
          */
          break;
        case OptSComp:
        case DqSCompOffset:
          /*@todo_adl
          if (Off == -17) { // Just picked value outside SR range.
            DdrCrDataOffsetComp.Bits.DqSlewRateCompOffset = 0;
            GetSetVal = 1;
          } else {
            DdrCrDataOffsetComp.Bits.DqSlewRateCompOffset = Off;
            GetSetVal = 0;
          }
          MrcGetSetChStrb(MrcData, Controller, Channel, Byte, GsmIocDqSlewDlyByPass, GsmMode, &GetSetVal);
          */
          break;
      }

      TransController = Controller;
      TransChannel    = Channel;
      TransStrobe     = Byte;
      Rank            = 0; // Not used for DdrCrDataOffsetComp
      MrcTranslateSystemToIp (MrcData, &TransController, &TransChannel, &Rank, &TransStrobe, GsmIocTxOn); // No GetSet for this register, so use another per-byte PHY CR
      Offset = MrcGetOffsetDataOffsetComp (MrcData, TransChannel, TransStrobe);
      MrcWriteCR (MrcData, Offset, DdrCrDataOffsetComp.Data);
      if (UpdateHost) {
        ChannelOut->DataCompOffset[Byte] = DdrCrDataOffsetComp.Data;
      }
      break;
    case OptCCCDS:
    case OptCCCDSUp:
    case OptCCCDSDn:
    case OptCCCDSUpCoarse:
    case OptCCCDSDnCoarse:
    case CmdRCompDrvUpOffset:
    case CmdRCompDrvDownOffset:
    case CtlRCompDrvUpOffset:
    case CtlRCompDrvDownOffset:
    case OptCCCSComp:
    case CmdSCompOffset:
    case CtlSCompOffset:
      DdrCrCmdOffsetComp.Data = ChannelOut->CmdCompOffset;
      switch (OptParam) {
        case OptCCCDS:
        case OptCCCDSUp:
        case OptCCCDSDn:
          OffMin = -8 / CCC_DS_STEP;
          OffMax = 7 / CCC_DS_STEP;
          break;
        case CmdRCompDrvUpOffset:
        case CmdRCompDrvDownOffset:
        case CtlRCompDrvUpOffset:
        case CtlRCompDrvDownOffset:
          OffMin = -8;
          OffMax = 7;
          break;
        case OptCCCDSUpCoarse:
        case OptCCCDSDnCoarse:
          OffMin = -8 / CCC_DS_COARSE_STEP;
          OffMax = 7 / CCC_DS_COARSE_STEP;
          break;
        case OptCCCSComp:
        case CmdSCompOffset:
        case CtlSCompOffset:
          OffMin = -9; // -9 for SR dis
          OffMax = 7;
          break;
      }

      if (Off > OffMax) {
        Off = OffMax;
      } else if (Off < OffMin) {
        Off = OffMin;
      }

      switch (OptParam) {
        case OptCCCDS:
          DdrCrCmdOffsetComp.Bits.CaRcompDrvUpOffset = Off * CCC_DS_STEP;
          DdrCrCmdOffsetComp.Bits.CtlRcompDrvUpOffset = Off * CCC_DS_STEP;
          DdrCrCmdOffsetComp.Bits.CaRcompDrvDownOffset = Off * CCC_DS_STEP;
          DdrCrCmdOffsetComp.Bits.CtlRcompDrvDownOffset = Off * CCC_DS_STEP;
          break;
        case OptCCCDSUp:
          DdrCrCmdOffsetComp.Bits.CaRcompDrvUpOffset = Off * CCC_DS_STEP;
          DdrCrCmdOffsetComp.Bits.CtlRcompDrvUpOffset = Off * CCC_DS_STEP;
          break;
        case OptCCCDSDn:
          DdrCrCmdOffsetComp.Bits.CaRcompDrvDownOffset = Off * CCC_DS_STEP;
          DdrCrCmdOffsetComp.Bits.CtlRcompDrvDownOffset = Off * CCC_DS_STEP;
          break;
        case OptCCCDSUpCoarse:
          DdrCrCmdOffsetComp.Bits.CaRcompDrvUpOffset = Off * CCC_DS_COARSE_STEP;
          DdrCrCmdOffsetComp.Bits.CtlRcompDrvUpOffset = Off * CCC_DS_COARSE_STEP;
          break;
        case OptCCCDSDnCoarse:
          DdrCrCmdOffsetComp.Bits.CaRcompDrvDownOffset = Off * CCC_DS_COARSE_STEP;
          DdrCrCmdOffsetComp.Bits.CtlRcompDrvDownOffset = Off * CCC_DS_COARSE_STEP;
          break;
        case CmdRCompDrvUpOffset:
          DdrCrCmdOffsetComp.Bits.CaRcompDrvUpOffset = Off;
          break;
        case CmdRCompDrvDownOffset:
          DdrCrCmdOffsetComp.Bits.CaRcompDrvDownOffset = Off;
          break;
        case CtlRCompDrvUpOffset:
          DdrCrCmdOffsetComp.Bits.CtlRcompDrvUpOffset = Off;
          break;
        case CtlRCompDrvDownOffset:
          DdrCrCmdOffsetComp.Bits.CtlRcompDrvDownOffset = Off;
          break;
        case OptCCCSComp:
          if (Off == -9) { // Just picked value outside SR range.
            DdrCrCmdOffsetComp.Bits.CaScompOffset = 0;
            //DdrCrCmdOffsetComp.Bits.CtlScompOffset = 0;
            GetSetVal = 1;
          } else {
            DdrCrCmdOffsetComp.Bits.CaScompOffset = Off;
            //DdrCrCmdOffsetComp.Bits.CtlScompOffset = Off;
            GetSetVal = 0;
          }
          //MrcGetSetMcCh(MrcData, Controller, Channel, SCompBypassCtl, GsmMode, &GetSetVal);
          MrcGetSetMcCh(MrcData, Controller, Channel, SCompBypassCmd, GsmMode, &GetSetVal);
          break;
        case CmdSCompOffset:
          DdrCrCmdOffsetComp.Bits.CaScompOffset = Off;
          //DdrCrCmdOffsetComp.Bits.CtlScompOffset = Off;
          break;
        case CtlSCompOffset:
          //DdrCrCmdOffsetComp.Bits.CaScompOffset = Off;
          DdrCrCmdOffsetComp.Bits.CtlScompOffset = Off;
          break;
      }

      Offset = GetDdrIoCommandOffsets(MrcData, CmdSCompOffset, 0, (Controller * MAX_CHANNEL) + (Channel * (MAX_CHANNEL / Outputs->MaxChannels)), 0, 0, 0, 0);
      MrcWriteCR(MrcData, Offset, DdrCrCmdOffsetComp.Data);
      if (MRC_PWR_DBG_PRINT) {
        if (OptParam == CmdSCompOffset || OptParam == CtlSCompOffset || OptParam == CtlRCompDrvUpOffset || OptParam == CtlRCompDrvDownOffset || OptParam == CmdRCompDrvUpOffset || OptParam == CmdRCompDrvDownOffset) {
          MRC_DEBUG_MSG(&Outputs->Debug, MSG_LEVEL_NOTE, "UpdateOptParamOffset DdrCrCmdOffsetComp raw: Controller = %d, Channel = %d, OptParam = %d, Off = %d \n", Controller, Channel, OptParam, Off);
          MRC_DEBUG_MSG(&Outputs->Debug, MSG_LEVEL_NOTE, "UpdateOptParamOffset DdrCrCmdOffsetComp translated: Channel = %d, OptParam = %d, Off = %d \n", (Controller * MAX_CHANNEL) + (Channel * (MAX_CHANNEL / Outputs->MaxChannels)), OptParam, Off);
        } else {
          MRC_DEBUG_MSG(&Outputs->Debug, MSG_LEVEL_NOTE, "UpdateOptParamOffset DdrCrCmdOffsetComp raw: Controller = %d, Channel = %d, OptParam = %s, Off = %d \n", Controller, Channel, TOptParamOffsetString[OptParam], Off);
          MRC_DEBUG_MSG(&Outputs->Debug, MSG_LEVEL_NOTE, "UpdateOptParamOffset DdrCrCmdOffsetComp translated: Channel = %d, OptParam = %s, Off = %d \n", (Controller * MAX_CHANNEL) + (Channel * (MAX_CHANNEL / Outputs->MaxChannels)), TOptParamOffsetString[OptParam], Off);
        }
      }
      if (UpdateHost) {
        ChannelOut->CmdCompOffset = DdrCrCmdOffsetComp.Data;
      }
      break;
    case OptTxEq:
    case OptTxEqCoeff0:
    case OptTxEqCoeff1:
    case OptTxEqCoeff2:
    case OptRxEq:
    case OptRxC:
    case OptRxR:
    case OptDFETap1:
    case OptDFETap2:
      switch (OptParam) {
        case OptTxEq:
        case OptTxEqCoeff0:
        case OptTxEqCoeff1:
        case OptTxEqCoeff2:
          GetSetVal = Off + (Ddr5 ? 1 : 0); // Sweep DDR5: [1..4], otherwise [0..3]
          break;
        case OptRxEq:
          GetSetVal = Off * RXEQ_STEP;
          break;
        case OptRxC:
        case OptRxR:
          GetSetVal = Off;
          break;
        case OptDFETap1:
        case OptDFETap2:
          if (Outputs->PowerTrainingPerChannel == TRUE) { // Per channel
            GetSetVal = Off * DFE_STEP_PER_CHANNEL;
          } else {
            GetSetVal = Off * DFE_STEP;
          }
          break;
        //case OptDFETap2:
        //  GetSetVal = (Off < 16) ? (15 - Off) : Off;  // sign/magnitude encoding
        //  break;
      }

      switch (OptParam) {
        case OptTxEq:
        case OptTxEqCoeff0:
          MrcGetSetChStrb(MrcData, Controller, Channel, Byte, TxEqCoeff0, GsmMode, &GetSetVal);
          break;
        case OptTxEqCoeff1:
          MrcGetSetChStrb(MrcData, Controller, Channel, Byte, TxEqCoeff1, GsmMode, &GetSetVal);
          break;
        case OptTxEqCoeff2:
          MrcGetSetChStrb(MrcData, Controller, Channel, Byte, TxEqCoeff2, GsmMode, &GetSetVal);
          break;
        case OptRxEq:
          MrcGetSetChStrb(MrcData, Controller, Channel, Byte, RxEq, GsmMode, &GetSetVal);
          break;
        case OptRxC:
          MrcGetSetChStrb(MrcData, Controller, Channel, Byte, RxC, GsmMode, &GetSetVal);
          break;
        case OptRxR:
          MrcGetSetChStrb(MrcData, Controller, Channel, Byte, RxR, GsmMode, &GetSetVal);
          break;
        case OptDFETap1:
          MrcGetSetChStrb(MrcData, Controller, Channel, Byte, RxTap1, GsmMode, &GetSetVal);
          break;
        case OptDFETap2:
          MrcGetSetChStrb(MrcData, Controller, Channel, Byte, RxTap2, GsmMode, &GetSetVal);
          break;
      }

      break;
    case OptCCCTxEq:
      GetSetVal = CCC_TXEQ_MAX - Off * CCC_TXEQ_STEP; // Sweep from high (12) to low
      if (Lpddr) {
        if (GetSetVal < LP_CCC_TXEQ_LOW_LIMIT) {
          GetSetVal = LP_CCC_TXEQ_LOW_LIMIT; // Sweep from high (12) to 0x8
        }
      }
      // For CCC, the 5th bit TxEq[4] = !(DDR4). If DDR4, set to 0, else 1. High equalizations save power for DDR4 but cost power for other modes.
      if (Lpddr) {
        GetSetVal |= 0x10;
      }
      //MrcGetSetMcCh(MrcData, Controller, Channel, CmdTxEq, GsmMode, &GetSetVal); // @todo_adl should use CCC instance
      //MrcGetSetMcCh(MrcData, Controller, Channel, CtlTxEq, GsmMode, &GetSetVal);
      break;
    case OptRxBias:
      GetSetVal = Off * RX_BIAS_STEP;
      MrcGetSetChStrb(MrcData, Controller, Channel, Byte, RxBiasCtl, GsmMode, &GetSetVal);
      break;
    case OptRxBiasVrefSel:
      GetSetVal = Off * RX_BIAS_STEP;
      MrcGetSetChStrb(MrcData, Controller, Channel, Byte, RxBiasVrefSel, GsmMode, &GetSetVal);
      break;
    case OptRxBiasTailCtl:
      GetSetVal = Off;
      MrcGetSetChStrb(MrcData, Controller, Channel, Byte, RxBiasTailCtl, GsmMode, &GetSetVal);
      break;
    case OptRxVrefVttDecap:
      GetSetVal = Off;
      MrcGetSetChStrb(MrcData, Controller, Channel, Byte, RxVrefVttDecap, GsmMode, &GetSetVal);
      break;
    case OptRxVrefVddqDecap:
      GetSetVal = Off;
      MrcGetSetChStrb(MrcData, Controller, Channel, Byte, RxVrefVddqDecap, GsmMode, &GetSetVal);
      break;
    case OptVccDLLBypass:
      /*@todo_adl
      GetSetVal = Off;
      MrcGetSetChStrb(MrcData, Controller, Channel, Byte, GsmIocVccDllFFControlBypass_V, GsmMode, &GetSetVal);
      MrcGetSetChStrb(MrcData, Controller, Channel, Byte, GsmIocVccDllControlBypass_V, GsmMode, &GetSetVal);
      */
      break;
    case OptPanicVttDnLp:
      GetSetVal = Off;
      MrcGetSetNoScope(MrcData, PanicVttDnLp, GsmMode, &GetSetVal);
      break;
    case OptVddq:
      // Sweep Vddq from high to low
      GetSetVal = -VDDQ_STEP * Off;
      // TGL_POWER_TRAINING_VDDQ Need to set Vddq via FIVR register:
      // MC_BIOS_REQ_PCU.REQ_VOLTAGE_DATA -> Set by MRC, read by Pcode (11 bits, 2.5 mV per tick)
      // MC_BIOS_DATA.VDDQ_TX_VOLTAGE -> Set by Pcode to what was actually requested from FIVR (may not be the same as what MRC requested)(11 bits, 2.5 mV per tick)
      // MAILBOX_BIOS_CMD_READ_BIOS_MC_REQ_ERROR -> Set by Pcode, read by MRC to see if there are any error codes indicating problems during setting Vddq. BIOS should retry or throw an error if this occurs.
      // Details for how to set Vddq in BIOS are here: https://sharepoint.amr.ith.intel.com/sites/tgldoc/tglhas/Shared%20Documents/Power%20Management%20and%20Delivery/Features/Vddq_tx%20FIVR/Vddq_tx%20FIVR.html#active-power-management
      // TGL_POWER_TRAINING_VDDQ There are some PHY init settings that depend on the Vddq setting, and those need to be copied from the PHY init and added to a function that is called as a side effects of the Vddq get/set.
      // MrcGetSetDdrIoGroupController0 (MrcData, Vddq, GsmMode, &GetSetVal);
      break;
    case OptDimmRon:
    case OptDimmRonUp:
    case OptDimmRonDn:
    case OptDimmOdtWr:
    case OptDimmOdtNom:
    case OptDimmOdtPark:
    case OptDimmOdtParkNT:
    case OptDimmOdtNomNT:
    case OptDimmOdtCA:
      if (OptParam == OptDimmOdtNomNT) {
        OptParam = OptDimmOdtNom;
      } else if (OptParam == OptDimmOdtParkNT) {
        OptParam = OptDimmOdtPark;
      }
      Status = GetDimmOptParamValues (MrcData, OptParam, &DimmOptParamVals, &NumDimmOptParamVals);
      if (Status == mrcSuccess) {
        Index = (UINT8) Off;
        if (DimmOptParamVals != NULL) {
          if (MRC_PWR_DBG_PRINT) {
            MRC_DEBUG_MSG(&Outputs->Debug, MSG_LEVEL_NOTE, "UpdateOptParamOffset DRAM Param: Controller = %d, Channel = %d, RankMask = %d, OptParam = %s, DimmOptParamVal = %d \n", Controller, Channel, Ranks & ChannelOut->ValidRankBitMask, TOptParamOffsetString[OptParam], DimmOptParamVals[Index]);
          }

          if (ChannelOut->DimmCount == 2) {
            for (Dimm = 0; Dimm < MAX_DIMMS_IN_CHANNEL; Dimm++) {
              NumRanksInCurrentDimm = MrcGetRankInDimm (MrcData, Channel, Dimm);
              if (NumRanksInCurrentDimm != 0) {
                if (NumRanksInDimm != 0 && NumRanksInCurrentDimm != NumRanksInDimm) {
                  break;
                }
                NumRanksInDimm = NumRanksInCurrentDimm;
              }
            }
          }

          for (Dimm = 0; Dimm < MAX_DIMMS_IN_CHANNEL; Dimm++) {
            NumRanksInDimm = MrcGetRankInDimm (MrcData, Channel, Dimm);
            RankMask = Ranks & ((NumRanksInDimm == 1) ? DIMM_TO_VARIABLE_RANK_MASK (Dimm, 0x1) : DIMM_TO_VARIABLE_RANK_MASK (Dimm, 0x3));

            if (RankMask) {
              TotalIndex = Index;
              if (TotalIndex < 0) {
                TotalIndex = 0;
              } else if (TotalIndex > NumDimmOptParamVals - 1) {
                TotalIndex = NumDimmOptParamVals - 1;
              }
              SetDimmParamValue (MrcData, Controller, Channel, RankMask, OptParam, DimmOptParamVals[TotalIndex], UpdateHost);
            }
          }
        }
      }
      break;
    case OptDimmDFETap1:
    case OptDimmDFETap2:
    case OptDimmDFETap3:
    case OptDimmDFETap4:
      //@todo_adl  SetDimmParam for Multiple MRs
      if (MRC_PWR_DBG_PRINT) {
        MRC_DEBUG_MSG(&Outputs->Debug, MSG_LEVEL_NOTE, "UpdateOptParamOffset DRAM Param: Controller = %d, Channel = %d, RankMask = %d, OptParam = %s, DimmOptParamVal = %d \n", Controller, Channel, Ranks & ChannelOut->ValidRankBitMask, TOptParamOffsetString[OptParam], Off);
      }

      if (ChannelOut->DimmCount == 2) {
        for (Dimm = 0; Dimm < MAX_DIMMS_IN_CHANNEL; Dimm++) {
          NumRanksInCurrentDimm = MrcGetRankInDimm(MrcData, Channel, Dimm);
          if (NumRanksInCurrentDimm != 0) {
            if (NumRanksInDimm != 0 && NumRanksInCurrentDimm != NumRanksInDimm) {
              break;
            }
            NumRanksInDimm = NumRanksInCurrentDimm;
          }
        }
      }

      for (Dimm = 0; Dimm < MAX_DIMMS_IN_CHANNEL; Dimm++) {
        NumRanksInDimm = MrcGetRankInDimm(MrcData, Channel, Dimm);
        RankMask = Ranks & ((NumRanksInDimm == 1) ? DIMM_TO_VARIABLE_RANK_MASK(Dimm, 0x1) : DIMM_TO_VARIABLE_RANK_MASK(Dimm, 0x3));

        if (RankMask) {
          Sign = (Off < 0) ? 1<<6 : 0; // direction
          Offset = ABS(Off); // value
          Offset |= Sign;
          Offset |= 1 << 7; // DFE Enable
          SetDimmParamValue(MrcData, Controller, Channel, RankMask, OptParam, (UINT16)Offset, UpdateHost);
        }
      }


      break;
  }
}

/**
  Slightly penalize any Asymmetry in margin

  @param[in] NegEdge - Negative edge of the margin
  @param[in] PosEdge - Positive edge of the margin

  @retval p2p - Width/Height reduced by the asymmetric difference in margin.
**/
UINT16
EffectiveMargin (
  IN const UINT16 NegEdge,
  IN const UINT16 PosEdge
  )
{
  INT16 p2p;
  UINT16 p2pDiff;

  p2p     = 2 * (PosEdge + NegEdge);
  p2pDiff = PosEdge - NegEdge;

  if (PosEdge > NegEdge) {
    p2p -= p2pDiff;
  } else {
    p2p += p2pDiff;
  }

  return p2p / 2;
}

/**
  This function does a running average on Margins in two dimentional fashion.

  @param[in,out] Margins - Margins to average in a 1D array.
  @param[in]     MLen    - Determines the Y-Dimension lengths
  @param[in]     XDim    - Determines the X-Dimension lengths
  @param[in]     XMin    - Used to skip the first elements in the Margin when averaging.
  @param[in]     CScale  - Used to place more weight on the center point.

  @retval Nothing
**/
void
RunningAverage2D (
  IN OUT UINT16       Margins[MAX_OPT_POINTS],
  IN     const UINT8  MLen,
  IN     const UINT8  XDim,
  IN     const UINT8  XMin,
  IN     const UINT8  CScale
)

{
  UINT8  XMax;
  UINT8  YMax;
  UINT16 TMargins[MAX_OPT_POINTS];
  UINT8  i;
  UINT8  x;
  UINT8  y;
  UINT8  xo;
  UINT8  yo;
  UINT8  XOff;
  INT8   YOff;

  if (MLen == 1) {
    return;
  }
  XMax  = XDim - 1;
  YMax  = ((MLen + XDim - 1) / XDim) - 1; // Ceiling to int in case the matrix is not fully populated

  for (i = 0; i < MLen; i++) {
    x = (i % XDim);
    y = (i / XDim);

    // Center Point
    TMargins[i] = Margins[i] * (CScale - 1); // Also add margin at the centerpoint below
    // Sum up surrounding results
    for (xo = 0; xo < 3; xo++) {
      XOff = x + xo - 1;
      // Avoid negative numbers on XOff
      if ((x == 0) && (xo == 0)) {
        XOff = 0;
      }
      // (x < XMin) allows averaging across points (1;0) and (2;0)
      if ((XOff < XMin) && (x < XMin)) {
        XOff = x; // RxEq special case.  Skip averaging on Col0/Col1
      }

      if (XOff > XMax) {
        XOff = XMax;
      }

      for (yo = 0; yo < 3; yo++) {
        YOff = y + yo - 1;
        if (YOff < 0) {
          YOff = 0;
        }

        if (YOff > YMax) {
          YOff = YMax;
        }
        // Avoid averaging with unpopulated matrix elements when dealing with partially populated matrices
        if ((XDim * YOff + XOff) > (MLen - 1)) {
          YOff = YOff - 1;
        }

        TMargins[i] += Margins[XDim * YOff + XOff];
      }
    }
  }
  // Copy TempMargins back over to real margins
  for (i = 0; i < MLen; i++) {
    Margins[i] = TMargins[i] / (8 + CScale); // Added div to maintain margin scaling
  }

  return;
}

/**
  This function does a running average on Margins in two dimentional fashion.

  @param[in,out] Margins - Margins to average
  @param[in]     Test    - Selects the Margins to average
  @param[in]     MLen    - Determines the Y-Dimension lengths
  @param[in]     XDim    - Determines the X-Dimension lengths
  @param[in]     XMin    - Used to skip the first elements in the Margin when averaging.
  @param[in]     CScale  - Used to place more weight on the center point.

  @retval Nothing
**/
void
Fill2DAverage (
  IN     MrcParameters *const MrcData,
  IN OUT UINT16               Margins[2][MAX_OPT_POINTS],
  IN     const UINT8          Test,
  IN     const UINT8          MLen,
  IN     const UINT8          XDim,
  IN     const UINT8          XMin,
  IN     const UINT8          CScale
)
{
  UINT8  XMax;
  UINT8  YMax;
  UINT16 TMargins[MAX_OPT_POINTS];
  UINT8  i;
  UINT8  x;
  UINT8  y;
  UINT8  xo;
  UINT8  yo;
  UINT8  XOff;
  INT8   YOff;
  UINT8  Edge;
  INT16  Gradient;
  INT16  MaxGradient;

  XMax  = XDim - 1;
  YMax  = ((MLen + XDim - 1) / XDim) - 1; // Ceiling to int in case the matrix is not fully populated

  for (i = 0; i < MLen; i++) {
    if (Margins[Test][i]) { // skip already populated entries
    continue;
    }

    x = (i % XDim);
    y = (i / XDim);
    Edge = 0;

//     if (i == 14)
//       MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "x=%d y=%d Margins[%d][%d]=%d\n", x, y, Test, i, Margins[Test][i]);

    // Center Point
    TMargins[i] = Margins[Test][i] * (CScale - 1); // Also add margin at the centerpoint below
    // Sum up surrounding results
    for (xo = 0; xo < 3; xo += 2) {
      XOff = x + xo - 1;
      // Avoid negative numbers on XOff
      if (((INT8) XOff) < 0) {
        //XOff = 0;
        Edge++;
        continue;
      }
      // (x < XMin) allows averaging across points (1;0) and (2;0)
      if ((XOff < XMin) && (x < XMin)) {
        XOff = x; // RxEq special case.  Skip averaging on Col0/Col1
      }

      if (XOff > XMax) {
        //XOff = XMax;
        Edge++;
        continue;
      }
      // Avoid averaging with unpopulated matrix elements when dealing with partially populated matrices
      if ((XDim * y + XOff) > (MLen - 1)) {
        Edge++;
        continue;
      }
      TMargins[i] += Margins[Test][XDim * y + XOff];
//       if (i == 14)
//         MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "x=%d y=%d TMargins[%d]=%d Edge=%d\n", x, y, i, TMargins[i],Edge);
    }
    for (yo = 0; yo < 3; yo += 2) {
      YOff = y + yo - 1;
      if (YOff < 0) {
        // YOff = 0;
        Edge++;
        continue;
      }

      if (YOff > YMax) {
        // YOff = YMax;
        Edge++;
        continue;
      }

      TMargins[i] += Margins[Test][XDim * YOff + x];
//       if (i == 14)
//         MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "x=%d y=%d TMargins[%d]=%d Edge=%d\n", x, y, i, TMargins[i],Edge);
    }
    // Copy TempMargins back over to real margins
//     if (Edge > 0) {
//       Margins[Test][i] = Margins[Test][i] * 8 / 10; // Penalize the edges by decreaseing margin by 20%
//     }
    Margins[Test][i] = TMargins[i] / (4 - Edge + CScale - 1); // Added div to maintain margin scaling
      //MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "x=%d y=%d TMargins[%d]=%d Margins[%d][i]=%d Edge=%d\n", x, y, i, TMargins[i], Test, Margins[Test][i], Edge);
  }

  // penalize for high margin gradient
  for (i = 0; i < MLen; i++) {
    x = (i % XDim);
    y = (i / XDim);
    MaxGradient = 0;
    // Sum up surrounding results
    for (xo = 0; xo < 3; xo += 2) {
      XOff = x + xo - 1;
      // Avoid negative numbers on XOff
      if ((x == 0) && (xo == 0)) {
        XOff = 0;
      }
      if (XOff > XMax) {
        XOff = XMax;
      }
      // Avoid averaging with unpopulated matrix elements when dealing with partially populated matrices
      if ((XDim * y + XOff) > (MLen - 1)) {
        XOff = XOff - 1;
      }
      Gradient = Margins[Test][XDim * y + x] - Margins[Test][XDim * y + XOff];
      //MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "x=%d y=%d Gradient=%d Margins[Test][XDim * y + x] = %d Margins[Test][XDim * y + XOff] = %d XOff = %d\n",x, y, Gradient, Margins[Test][XDim * y + x], Margins[Test][XDim * y + XOff], XOff);
      if (Gradient > MaxGradient) {
        // if we loose margin update MaxGradient
        MaxGradient = Gradient;
      }
    }
    for (yo = 0; yo < 3; yo += 2) {
      YOff = y + yo - 1;
      if (YOff < 0) {
        YOff = 0;
      }

      if (YOff > YMax) {
        YOff = YMax;
      }

      // Avoid averaging with unpopulated matrix elements when dealing with partially populated matrices
      if ((XDim * YOff + x) > (MLen - 1)) {
        YOff = YOff - 1;
      }

      Gradient = (Margins[Test][XDim * y + x] - Margins[Test][XDim * YOff + x]);
      //MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "x=%d y=%d Gradient=%d Margins[Test][XDim * y + x] = %d Margins[Test][XDim * y + XOff] = %d YOff = %d\n",x, y, Gradient, Margins[Test][XDim * y + x], Margins[Test][XDim * y + YOff], YOff);
      if (Gradient > MaxGradient) {
        MaxGradient = Gradient;
      }
      // MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "x=%d y=%d TMargins[%d]=%d Edge=%d\n", x, y, i, TMargins[i],Edge);
    }
    // save MaxGradient in Temp array and clip for max margin.
    if (MaxGradient > Margins[Test][i]) {
      MaxGradient = Margins[Test][i];
    }
    TMargins[i] = MaxGradient;
    //MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "x=%d y=%d  Margins[%d][%d]=%d MaxGradient=%d \n", x, y, i, Test, Margins[Test][i], TMargins[i]);
  }
  // apply the MaxGradient to Original array
  for (i = 0; i < MLen; i++) {
    Margins[Test][i] -= TMargins[i];
  }
  return;
}

/**
  This function takes in 2D array of Margins: MarginType / Parameter Index.
  This index to the array represents some arbitrary parameter value that we are optimizing.
  The function will look for up to MAX_TRADEOFF_TYPES entries to optimize on.
  OptResultByte will store the results of the optimization, and various other data.

  In addition to optimizing for margin, this function can also optimize for power.
  GoodPowerLimit is an array that sets level where power is more important than margin.
    i.e. Any points where ((Margin[0]>GoodPowerLimit[0]) & (Margin[1]>GoodPowerLimit[1]) & ... )
  To avoid overflow, this function will automatic scale margins to fit in UINT64

  @param[in]     MrcData          - The global MRC data structure.
  @param[in,out] OptResultByte    - Structure containing the optimized results.
  @param[in]     InputMargins     - Margins we are optimizing
  @param[in]     MarginsLength    - The length of InputMargins
  @param[in]     LenMargin        - The length of InputMargins we are optimizing (0 - LenMargin -1).
  @param[in]     Scale            - Controls the relative importance on Margins[0] vs. [1] ...
                                      ex: To make Margins[0] twice as important, set Scale = [1, 2, 2, 2, 2].
                                      Since the search optimizes the lowest margin, increasing [1:4] makes 0 more important.
                                      This function can be used to optimize only Margin[0] by setting Scale = [1, 0, 0, 0, 0].
  @param[in]     UPMOptimize      - Optimize only for UPM limit for selected params, so if they pass UPM they do not affect the score.
  @param[in]     EnSq             - Enables the square root term in the optimization functions to make the tradeoff steeper.
  @param[in]     AveN             - The number of points used for the averaging filter.
  @param[in]     IncEnds          - Controls if the endpoints are to be included.
  @param[in]     ScaleM           - Controls the scaling of the middle point in 1-D average filter.
  @param[in]     GoodPowerLimit   - The power limit above which we only trade-off for power and not margin.
  @param[in]     OptimizationMode - 0:    Returns first good margin limit point.
                                    1-4:  Return the first index that meets GoodPowerLimit and lowest power.
                                            OptimizationMode is power column index.
                                    5-99: Return the index that meets GoodPowerLimit and >= % of the Max Optimization result.
                                    >100: Returns the highest Optimization Result.
  @param[in]     GuardBand        - Signed offest to check if margin drop is acceptable.  Save good guardband
                                    in OptResultByte.

  @retval Nothing.
**/
void
FindOptimalTradeOff (
  IN     MrcParameters      *const  MrcData,
  IN OUT OptResultsPerByte          *OptResultByte,
  IN     UINT16                     *InputMargins,
  IN     UINT8                      MarginsLength,
  IN     UINT8                      LenMargin,
  IN     const UINT8                Scale[MAX_TRADEOFF_TYPES],
  IN     const UINT8                UPMOptimize[MAX_TRADEOFF_TYPES],
  IN     UINT8                      EnSq,
  IN     UINT8                      AveN,
  IN     UINT8                      IncEnds,
  IN     UINT8                      ScaleM,
  IN     UINT16                     GoodPowerLimit[MAX_TRADEOFF_TYPES],
  IN     UINT8                      OptimizationMode,
  IN     INT8                       GuardBand,
  IN     BOOLEAN                    UPMFilterEnable,
  IN     MrcTestUpmArray*           TestUpmArray,
  IN     BOOLEAN                    CliffsFilterEnable,
  IN     UINT16                     CliffsFilterRowNum,
  IN     UINT16                     CliffsFilterColNum
  )
{
  const MrcInput      *Inputs;
  MrcDebug            *Debug;
  const MRC_FUNCTION  *MrcCall;
  UINT32               PostMar[MAX_TRADEOFF_TYPES][MAX_OPT_POINTS];
  UINT32               SMaxPost[MAX_TRADEOFF_TYPES];
  UINT32               MaxPost[MAX_TRADEOFF_TYPES];
  UINT32               MinPost[MAX_TRADEOFF_TYPES];
  UINT16               GoodPwrLimitPost[MAX_TRADEOFF_TYPES];
  UINT32               CumulativeMarginsArray[MAX_OPT_POINTS];
  UINT32               ResultArray[MAX_OPT_POINTS];
  UINT32               ScaleMin;
  UINT8                Nby2;
  UINT8                EqOrder;
  UINT8                MarginArray;
  UINT8                yArr;
  UINT8                x;
  UINT8                i;
  UINT8                Off;
  INT16                xEff;
  UINT8                NumBits;
  UINT32               localY;
  UINT8                Shift;
  UINT8                Adder;
  UINT8                Start;
  UINT8                Stop;
  UINT64               Result;
  UINT64               LocalResult;
  UINT64               MaxR;
  UINT64               MarginLimit;
  UINT8                BestX;
  UINT8                PowerX;
  UINT8                FoundPwrOpt;
  UINT8                NumCalcArr;
  INT8                 StepSize;
  UINT8                MarginDropPercent;
  UINT32               MinPost1;
  BOOLEAN              GoodPower;

  Inputs            = &MrcData->Inputs;
  MrcCall           = Inputs->Call.Func;
  Debug             = &MrcData->Outputs.Debug;
  MarginDropPercent = 10;  // 10% loss of margin is a bad guardband offset.
  EqOrder           = 0; // Is the optimization equation: X^1, X^2, X^5
  Result            = 0;
  MaxR              = 0;
  BestX             = 0;
  PowerX            = 0;
  FoundPwrOpt       = 0;
  Start = 0;
  Stop  = LenMargin;
  MrcCall->MrcSetMemDword (MaxPost, sizeof (MaxPost) / sizeof (UINT32), 1);
  MrcCall->MrcSetMemDword (SMaxPost, sizeof (SMaxPost) / sizeof (UINT32), 1);
  MrcCall->MrcSetMemDword (MinPost, sizeof (MinPost) / sizeof (UINT32), 0xFFFFFFFF);
  MrcCall->MrcSetMemWord (GoodPwrLimitPost, sizeof (GoodPwrLimitPost) / sizeof (UINT16), 0);
  MrcCall->MrcSetMem ((UINT8 *) PostMar, sizeof (PostMar), 0);
  MrcCall->MrcSetMem ((UINT8 *) OptResultByte, sizeof (OptResultsPerByte), 0);
  MrcCall->MrcSetMem ((UINT8 *) CumulativeMarginsArray, sizeof (CumulativeMarginsArray), 0);

  if (AveN <= 0) {
    AveN = 1;
  }
  Nby2    = (AveN >> 1);

  // Process Raw Margins Results with a running average filter of AveN
  for (MarginArray = 0; MarginArray < MAX_TRADEOFF_TYPES; MarginArray++) {
    // Scale GoodPowerLimit to match PostMar results
    GoodPwrLimitPost[MarginArray] = GoodPowerLimit[MarginArray] * (AveN + ScaleM - 1) * Scale[MarginArray];

    for (x = 0; x < LenMargin; x++) {
      if (UPMFilterEnable) {
        PostMar[MarginArray][x] = InputMargins[x + MarginArray * MarginsLength]; // Raw margins are copied into uin32 type array for calculations
        OptResultByte->Margins[MarginArray][x].EW = (UINT16)PostMar[MarginArray][x]; //Save raw data results for prints
      } else {
        if (Scale[MarginArray] == 0) {
          // Not in the game
          MinPost[MarginArray] = PostMar[MarginArray][x] = 1;
        } else {
          if (x == 0) {
            // Update EqOrder once for each MarginArray value with a non-zero scale factor.
            //   e.g.:so for {RdT,RdV,0,0} it will be = 2
            EqOrder += 1;
          }

          for (Off = 0; Off < AveN; Off++) {
            xEff = x + Off - Nby2;
            if (xEff < 0) {
              PostMar[MarginArray][x] += *(InputMargins + MarginArray * MarginsLength + 0);  // InputMargins[MarginArray][0];
            } else if (xEff >= LenMargin) {
              PostMar[MarginArray][x] += *(InputMargins + MarginArray * MarginsLength + LenMargin - 1);
            } else if (x == xEff) {
              PostMar[MarginArray][x] += ScaleM * *(InputMargins + MarginArray * MarginsLength + xEff);
            } else {
              PostMar[MarginArray][x] += *(InputMargins + MarginArray * MarginsLength + xEff);
            }
          }
          // save none scaled margins after avg filtering
          OptResultByte->Margins[MarginArray][x].EW = (UINT16) PostMar[MarginArray][x] / (AveN + ScaleM - 1);
          PostMar[MarginArray][x] *= Scale[MarginArray];
          if (MaxPost[MarginArray] < PostMar[MarginArray][x]) {
            MaxPost[MarginArray] = PostMar[MarginArray][x];
          }

          if (MinPost[MarginArray] > PostMar[MarginArray][x]) {
            MinPost[MarginArray] = PostMar[MarginArray][x];
          }
        }
      }
    }

    if (!UPMFilterEnable) {
      if (Scale[MarginArray] == 0) {
        continue;
      }

      SMaxPost[MarginArray]               = MaxPost[MarginArray];
      OptResultByte->Scale[MarginArray]   = Scale[MarginArray];
      OptResultByte->MaxPost[MarginArray] = MaxPost[MarginArray] / (AveN + ScaleM - 1);
      OptResultByte->MinPost[MarginArray] = MinPost[MarginArray] / (AveN + ScaleM - 1);
    }
  }

  if (!UPMFilterEnable) {
    // Sort Array
    MrcBsort (SMaxPost, MAX_TRADEOFF_TYPES);
    // Calculate Number of Bits Required to represent this number. Make sure to handle care of EnSq
    NumBits = 0;

    for (MarginArray = 0; MarginArray < MAX_TRADEOFF_TYPES; MarginArray++) {
      if (MarginArray < (MAX_TRADEOFF_TYPES - 1)) {
        // if EnSq we do Max^2 so the num get twice the bits...
        localY = SMaxPost[MarginArray];
        if (EnSq) {
          localY = (localY * localY);
        }

        NumBits += MrcLog2 (localY);
      } else {
        NumBits += MrcLog2 (SMaxPost[MarginArray]);
      }
    }

    NumBits += 11; // reserved another 10 bits for division in order to format for printing ; 3 for adding - up to 8
    // EqOrder for square terms
    if (EnSq) {
      EqOrder = (EqOrder + (EqOrder - 1));
    }
    // Handle Potential Saturation
    if (NumBits > 64) {
      MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Warning number of bits exceeds 64 bit : %d \n", NumBits);
      // Shift all numbers to reduce final result to be less than 32 bits.  Round Up
      Shift = (NumBits - 64 + EqOrder - 1) / EqOrder;
      // RoundUp Adder
      Adder = (1 << (Shift - 1));
      // Divide by (1<<Shift) and Round Up
      for (MarginArray = 0; MarginArray < MAX_TRADEOFF_TYPES; MarginArray++) {
        MaxPost[MarginArray]       = (MaxPost[MarginArray] + Adder) >> Shift;
        GoodPwrLimitPost[MarginArray]  = (GoodPwrLimitPost[MarginArray] + Adder) >> Shift;
        for (x = 0; x < LenMargin; x++) {
          PostMar[MarginArray][x] = (PostMar[MarginArray][x] + Adder) >> Shift;
        }
      }
    }
    // Calculate Square terms:
    if (EnSq) {
      for (MarginArray = 0; MarginArray < MAX_TRADEOFF_TYPES; MarginArray++) {
        MaxPost[MarginArray] = MaxPost[MarginArray] * MaxPost[MarginArray];
      }
    }
    // Set Limits for Search
    Start = 0;
    Stop  = LenMargin;
    if ((IncEnds == 0) && (LenMargin > AveN)) {
      if (Nby2 > 0) {
        Start++;
        Stop--;
      }
    }
    if (UPMOptimize != NULL) {
      for (x = Start; x < Stop; x++) {
        for (MarginArray = 0; MarginArray < MAX_TRADEOFF_TYPES; MarginArray++) {
          if (UPMOptimize[MarginArray] == 0) {
            continue; // not need to calculate those
          }
          if (PostMar[MarginArray][x] > GoodPwrLimitPost[MarginArray]) {
            PostMar[MarginArray][x] = GoodPwrLimitPost[MarginArray];
            OptResultByte->Margins[MarginArray][x].EW = (UINT16) PostMar[MarginArray][x] / (AveN + ScaleM - 1) / Scale[MarginArray];
          }
        }
      }
    }
  }

  // Find Optimal Point from Margin Point of View
  // Combine the points using the formula:
  //   Max0*Max1*Max2*Post3 + Max1*Max2*Max3*Post0 + Max2*Max3*Max0*Post1 +
  //   Max3*Max0*Max1*Post2 + Scale*min(Post0,Post1,Post2,Post3)^EqOrder
  //   Scale = 1 + (10*(SMaxPost[0]-SMaxPost[1]))/SMaxPost[MAX_TRADEOFF_TYPES-1]
  for (x = Start; x < Stop; x++) {
    Result      = 0;
    MinPost1    = 0xFFFFFFFF;
    GoodPower   = TRUE;
    NumCalcArr  = 0;
    for (MarginArray = 0; MarginArray < MAX_TRADEOFF_TYPES; MarginArray++) {
      if (Scale[MarginArray] == 0) {
        continue; // not need to calculate those
      }
      if (UPMFilterEnable) {
        Result += MrcCall->MrcMultU64x32(UPMFilter (MrcData, PostMar[MarginArray][x], TestUpmArray[MarginArray].TotalTicks), TestUpmArray[MarginArray].ScaleFactor);
      } else {
        NumCalcArr++; // Count the number of MarginTypes in the optimization function.
        // Find Min of all PostMar at offset x
        // Does this point meet the min power Margin requirements?
        if (Scale[MarginArray] > 0) {
          if (MinPost1 > PostMar[MarginArray][x]) {
            MinPost1 = PostMar[MarginArray][x];
          }

          if (PostMar[MarginArray][x] < GoodPwrLimitPost[MarginArray]) {
            GoodPower = FALSE;
          }
        }
        // Calculate this portion of result
        LocalResult = 1;
        for (yArr = 0; yArr < MAX_TRADEOFF_TYPES; yArr++) {
          if (Scale[yArr] == 0) {
            continue; // not need to calculate those
          }

          if (MarginArray == yArr) {
            continue;
          } else {
            LocalResult = MrcCall->MrcMultU64x32 (LocalResult, MaxPost[yArr]);
          }
        }

        Result += MrcCall->MrcMultU64x32 (LocalResult, PostMar[MarginArray][x]);
      }
    }

    if (!UPMFilterEnable) {
      // Add in (MinPost ^ EqOrder)
      // If NumCalcArr is 0, set it to 1 so that it still in the range of array size.
      //  This will cause PowerCalcIndex to underflow.  Set to 1 in this case.
      if (NumCalcArr == 0) {
        MRC_DEBUG_MSG (
          Debug,
          MSG_LEVEL_NOTE,
          "Error: wrong input parameter caused NumCalcArr = 0 when calling FindOptimalTradeOff()\n"
          );
        NumCalcArr = 1;
      }

      ScaleMin = 1 + (10 * (SMaxPost[0] - SMaxPost[1])) / SMaxPost[NumCalcArr - 1];
      if (ScaleMin > 5) {
        ScaleMin = 5;
      }

      ScaleMin  = 1;
      LocalResult    = ScaleMin;
      for (i = 0; i < EqOrder; i++) {
        LocalResult = MrcCall->MrcMultU64x32 (LocalResult, MinPost1);
      }

      Result += LocalResult;
    } else {// (!UPMFilterEnable)
      CumulativeMarginsArray[x] = (UINT32)Result;// Filling up array for cliffs filter usage
    }

    if (Result > MaxR) {
      MaxR  = Result;
      BestX = x; // save first highest function result offset
    }

    OptResultByte->Result[x] = Result;

    if (!UPMFilterEnable) {
      // Find Optimal Point from Power Point of View.
      // All the margin types must be >= GoodPowerLimit
      if (GoodPower) {
        if (FoundPwrOpt == 0) {
          FoundPwrOpt = 1;
          PowerX      = x;
        } else {
          // Find the first lowest power point.
          // PostMar[Power] is inverted: higher number is lower power.
          if ((OptimizationMode > 0) && (OptimizationMode < MAX_TRADEOFF_TYPES)) {
            if (PostMar[OptimizationMode][x] > PostMar[OptimizationMode][PowerX]) {
              PowerX = x;
            }
          }
        }
      }
    }
  } // end shmoo offsets

  if (!UPMFilterEnable) {
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, (MaxR == 0) ? "Warning : MaxR is Zero !!!\n" : "");
    // At this point, BestX will have the highest Margin Result index.  PowerX will have
    // the highest or lowest index that is marked good based on MarginLimits.
    if (FoundPwrOpt) {
      if (OptimizationMode < MAX_TRADEOFF_TYPES) {
        BestX = PowerX;
      } else if (OptimizationMode < 100) {
        for (x = PowerX; x < Stop; x++) {
          Result = (UINT32) (MrcCall->MrcDivU64x64 (MrcCall->MrcMultU64x32 (OptResultByte->Result[x], 100), MaxR, NULL));
          if (Result >= OptimizationMode) {
            BestX = x;
            break;
          }
        }
      }
    }
  }

  OptResultByte->Best     = BestX;
  OptResultByte->MaxR     = MaxR;
  if (!UPMFilterEnable) {
    // Apply a guard band to the best setting, clamped at edges of the search.
    if (GuardBand != 0) {
      // Determine step direction and limit to the search edge.
      if (GuardBand < 0) {
        StepSize = 1;
        Off = ((BestX + GuardBand) < Start) ? Start : (BestX + GuardBand);
      } else {
        StepSize = -1;
        Off = ((BestX + GuardBand) >= Stop) ? (Stop - 1) : (BestX + GuardBand);
      }
      // Check each test for margin drop of MarginDropPercent.
      // If any test fails, we step towards the original selection.
      MarginLimit = MrcCall->MrcMultU64x32 (OptResultByte->Result[BestX], (100 - MarginDropPercent));
      MarginLimit = MrcCall->MrcDivU64x64 (MarginLimit, 100, NULL);
      for(; (Off != BestX); Off += StepSize) {
        if (OptResultByte->Result[Off] > MarginLimit) {
          break;
        }
      }
      OptResultByte->GuardBand = Off - (INT8) BestX;
    }
  } else {// (!UPMFilterEnable)
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Print all margins summary table\n");
    for (UINT8 z = 0; z < CliffsFilterRowNum; z++) {// Select highest score result
      for (UINT8 y = 0; y < CliffsFilterColNum; y++) {// Select highest score result
        MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "%d\t", CumulativeMarginsArray[y + z*CliffsFilterColNum]);
      }
      MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\n");
    }
  }
  if (CliffsFilterEnable) {// Over writes best results after cliff detection filter
    MrcConvolution2D (// Run cliff detection filter on result
      CumulativeMarginsArray,
      ResultArray,
      CliffsFilterRowNum,
      CliffsFilterColNum,
      CliffsFilterColNum,
      &LPFARR55[0][0],
      5
    );

    MaxR = 0;
    BestX = 0;
    for (x = Start; x < Stop; x++) {// Select highest score result
      OptResultByte->Result[x] = ResultArray[x];//Update byte results in host struct
      if (ResultArray[x] > MaxR) {
        MaxR = ResultArray[x];
        BestX = x; // save first highest function result offset
      }
    }

    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Print all margins summary table after cliff filter\n");
    for (UINT8 z = 0; z < CliffsFilterRowNum; z++) {// Select highest score result
      for (UINT8 y = 0; y < CliffsFilterColNum; y++) {// Select highest score result
        MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "%d\t", ResultArray[y + z*CliffsFilterColNum]);
      }
      MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\n");
    }
    //Update byte results in host struct
    OptResultByte->Best     = BestX;
    OptResultByte->MaxR     = MaxR;
  }

  return;
}

/**
  This function implements Turn Around Timing training.
  Optimize TA ODT Delay and Duration

  @param[in] MrcData - Include all MRC global data.

  @retval MrcStatus - If it succeeds return mrcSuccess.
**/
MrcStatus
MrcTurnAroundTiming (
  IN MrcParameters *const MrcData
  )
{
  MrcDebug            *Debug;
  MrcOutput           *Outputs;
  MrcChannelOut       *ChannelOut;
  MrcStatus           Status;
  INT64               GetSetVal;
  INT64               MaxVal;
  UINT32              Controller;
  UINT32              Channel;
  UINT32              IpChannel;
  UINT32              ChannelIncrement;
  UINT8               MaxChannel;
  UINT8               RankMaskCh;
  UINT8               RankMask;
  BOOLEAN             RunDD;
  BOOLEAN             RunDR;
  BOOLEAN             Lpddr;
  static const UINT8  ParamListX[6]  = {RdV, RdT, WrV, WrT, RcvEnaX, WrLevel};
  static const UINT8  TestListRdX[3] = {RdV, RdT, RcvEnaX};
  static const UINT8  TestListWr[2]  = {WrV, WrT};
  static const INT8   ClkShifts[2]   = {-7, 7};
  INT8                Range;
  UINT8               GuardBand;
  UINT8               Update;
  UINT8               LoopCount;
  UINT32              RelaxBy;
  UINT32              MinNomWR2WR_dr;
  UINT32              NomWR2RD_dr;
  UINT32              NomWR2RD_dd;
  UINT32              NomRD2WR_dd;
  UINT32              NomRD2WR_dr;
  UINT32              NomRD2RD_dr;
  UINT32              NomRD2RD_dd;
  UINT32              NomWR2WR_dr;
  UINT32              NomWR2WR_dd;
  UINT32              Offset;
  MC0_CH0_CR_SC_PCIT_STRUCT  ScPcit;
  MC0_CH0_CR_SC_PCIT_STRUCT  ScPcitSave[MAX_CONTROLLER][MAX_CHANNEL];

#ifdef MRC_DEBUG_PRINT
  UINT8 GroupIdx;
  UINT8 TaTypeIdx;
  static const GSM_GT PrintGroups [4][2] = {{GsmMctRDRDdr, GsmMctRDRDdd},
                                            {GsmMctRDWRdr, GsmMctRDWRdd},
                                            {GsmMctWRRDdr, GsmMctWRRDdd},
                                            {GsmMctWRWRdr, GsmMctWRWRdd}};
  static const char *TaString[] = {"RdRd", "RdWr", "WrRd", "WrWr"};
  static const char *TypeStr[] = {"Dr", "Dd"};
#endif

  Status        = mrcSuccess;
  RankMaskCh    = 0;
  Update        = 1;
  LoopCount     = 10;  //1024 per Rank
  RelaxBy       = 4;
  RunDD         = FALSE;
  RunDR         = FALSE;
  NomWR2RD_dr   = 0;
  NomWR2RD_dd   = 0;
  NomRD2WR_dd   = 0;
  NomRD2WR_dr   = 0;
  NomRD2RD_dr   = 0;
  NomRD2RD_dd   = 0;
  NomWR2WR_dr   = 0;
  NomWR2WR_dd   = 0;
  MaxVal        = 0;
  RankMask      = MrcData->Outputs.ValidRankMask;
  Range         = 1;

  Outputs       = &MrcData->Outputs;
  Debug         = &Outputs->Debug;
  MaxChannel    = Outputs->MaxChannels;
  Lpddr         = Outputs->Lpddr;

  ChannelIncrement = Lpddr ? 2 : 1;

  for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
    for (Channel = 0; Channel < MaxChannel; Channel += ChannelIncrement) {
      if (!MrcChannelExist (MrcData, Controller, Channel)) {
        continue;
      }
      ChannelOut = &Outputs->Controller[Controller].Channel[Channel];

      RankMaskCh  = ChannelOut->ValidRankBitMask;
      RunDD       = RunDD || (ChannelOut->DimmCount == 2);
      RunDR       = RunDR || ((RankMaskCh & 0xC) == 0xC) || ((RankMaskCh & 0x3) == 0x3);
      RunDD       = 0;
      MRC_DEBUG_MSG (
        Debug,
        MSG_LEVEL_NOTE,
        "Mc %d, Channel %d: RunDR = 0x%x, RunDD = 0x%x, RankMaskCh = 0x%x\n",
        Controller,
        Channel,
        RunDR,
        RunDD,
        RankMaskCh
      );

      // Use nominal values (previously programmed) +1 and -1 to test for Gear1.  Gear2 will need to do +2 and -2 with steps of 2.
      // Taking worst case of both channels.
      // Ideally the Cliff routine should support offset per channel or better make the param a real offset (not the abs value)
      MrcGetSetMcCh (MrcData, Controller, Channel, GsmMctWRRDdr, ReadFromCache, &GetSetVal);
      NomWR2RD_dr = MAX (NomWR2RD_dr, (UINT8) GetSetVal);
      MrcGetSetMcCh (MrcData, Controller, Channel, GsmMctWRRDdd, ReadFromCache, &GetSetVal);
      NomWR2RD_dd = MAX (NomWR2RD_dd, (UINT8) GetSetVal);

      MrcGetSetMcCh (MrcData, Controller, Channel, GsmMctRDWRdr, ReadFromCache, &GetSetVal);
      NomRD2WR_dr = MAX (NomRD2WR_dr, (UINT8) GetSetVal);
      MrcGetSetMcCh (MrcData, Controller, Channel, GsmMctRDWRdd, ReadFromCache, &GetSetVal);
      NomRD2WR_dd = MAX (NomRD2WR_dd, (UINT8) GetSetVal);
      MrcGetSetMcCh (MrcData, Controller, Channel, GsmMctRDRDdr, ReadFromCache, &GetSetVal);
      NomRD2RD_dr = MAX (NomRD2RD_dr, (UINT8) GetSetVal);
      MrcGetSetMcCh (MrcData, Controller, Channel, GsmMctRDRDdd, ReadFromCache, &GetSetVal);
      NomRD2RD_dd = MAX (NomRD2RD_dd, (UINT8) GetSetVal);
      MrcGetSetMcCh (MrcData, Controller, Channel, GsmMctWRWRdr, ReadFromCache, &GetSetVal);
      NomWR2WR_dr = MAX (NomWR2WR_dr, (UINT8) GetSetVal);
      MrcGetSetMcCh (MrcData, Controller, Channel, GsmMctWRWRdd, ReadFromCache, &GetSetVal);
      NomWR2WR_dd = MAX (NomWR2WR_dd, (UINT8) GetSetVal);

    // Change PCIT to MAX
    // This allows proper tRDWR_dg stress without extra PRE/ACT cycles.
      IpChannel   = LP_IP_CH (Lpddr, Channel);
      Offset = OFFSET_CALC_MC_CH (MC0_CH0_CR_SC_PCIT_REG, MC1_CH0_CR_SC_PCIT_REG, Controller, MC0_CH1_CR_SC_PCIT_REG, IpChannel);
      ScPcit.Data = MrcReadCR (MrcData, Offset);
      ScPcitSave[Controller][IpChannel] = ScPcit;
      ScPcit.Bits.PCIT_SUBCH0 = MC0_CH0_CR_SC_PCIT_PCIT_SUBCH0_MAX;
      ScPcit.Bits.PCIT_SUBCH1 = MC0_CH0_CR_SC_PCIT_PCIT_SUBCH1_MAX;
      MrcWriteCR (MrcData, Offset, ScPcit.Data);
    } // for Channel
  } // for Controller

  if (Outputs->Gear2) {
    Range = 2;
    MinNomWR2WR_dr = MAX (NomWR2WR_dr - Range, 7); // 7 QClks
    MinNomWR2WR_dr = 2 * (DIVIDECEIL (MinNomWR2WR_dr, 2));
  } else {
    MinNomWR2WR_dr = MAX (NomWR2WR_dr - Range, DIVIDECEIL (7, 2)); // 7 QClks in DClks
  }

  for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
    for (Channel = 0; Channel < MaxChannel; Channel += ChannelIncrement) {
      if (!MrcChannelExist (MrcData, Controller, Channel)) {
        continue;
      }
      // Adjust initial values to be more relaxed so any detected failures are due to the parameter being tested.
      if (RunDR) {
        // Different Rank, Same DIMM
        MrcGetSetLimits (MrcData, GsmMctRDRDdr, NULL, &MaxVal, NULL);
        GetSetVal = MIN ((UINT32) MaxVal, NomRD2RD_dr + RelaxBy);
        MrcGetSetMcCh (MrcData, Controller, Channel, GsmMctRDRDdr, WriteToCache, &GetSetVal);
        MrcGetSetLimits (MrcData, GsmMctWRWRdr, NULL, &MaxVal, NULL);
        GetSetVal = MIN ((UINT32) MaxVal, NomWR2WR_dr + RelaxBy);
        MrcGetSetMcCh (MrcData, Controller, Channel, GsmMctWRWRdr, WriteToCache, &GetSetVal);

        MrcGetSetLimits (MrcData, GsmMctWRRDdr, NULL, &MaxVal, NULL);
        GetSetVal = MIN ((UINT32) MaxVal, NomWR2RD_dr + RelaxBy);
        MrcGetSetMcCh (MrcData, Controller, Channel, GsmMctWRRDdr, WriteToCache, &GetSetVal);
        MrcGetSetLimits (MrcData, GsmMctRDWRdr, NULL, &MaxVal, NULL);
        GetSetVal = MIN ((UINT32) MaxVal, NomRD2WR_dr + RelaxBy);
        MrcGetSetMcCh (MrcData, Controller, Channel, GsmMctRDWRdr, WriteToCache, &GetSetVal);
      }
      if (RunDD) {
        // Different DIMMs
        MrcGetSetLimits (MrcData, GsmMctWRRDdd, NULL, &MaxVal, NULL);
        GetSetVal = MIN ((UINT32) MaxVal, NomWR2RD_dd + RelaxBy);
        MrcGetSetMcCh (MrcData, Controller, Channel, GsmMctWRRDdd, WriteToCache, &GetSetVal);
        MrcGetSetLimits (MrcData, GsmMctRDWRdd, NULL, &MaxVal, NULL);
        GetSetVal = MIN ((UINT32) MaxVal, NomRD2WR_dd + RelaxBy);
        MrcGetSetMcCh (MrcData, Controller, Channel, GsmMctRDWRdd, WriteToCache, &GetSetVal);
        MrcGetSetLimits (MrcData, GsmMctRDRDdd, NULL, &MaxVal, NULL);
        GetSetVal = MIN ((UINT32) MaxVal, NomRD2RD_dd + RelaxBy);
        MrcGetSetMcCh (MrcData, Controller, Channel, GsmMctRDRDdd, WriteToCache, &GetSetVal);
        MrcGetSetLimits (MrcData, GsmMctWRWRdd, NULL, &MaxVal, NULL);
        GetSetVal = MIN ((UINT32) MaxVal, NomWR2WR_dd + RelaxBy);
        MrcGetSetMcCh (MrcData, Controller, Channel, GsmMctWRWRdd, WriteToCache, &GetSetVal);
      }
      MrcFlushRegisterCachedData (MrcData);
      // Must update the XARB bubble injector when TAT values change
      SetTcBubbleInjector (MrcData, Controller, Channel);
    } // for Channel
  } // for Controller

  // Different DIMM turnarounds
  if (RunDD) {
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "%s DDRD2RD\n", "\n ##### Running");
    Status = TrainDDROptParamCliff (
              MrcData,
              ddrd2rd,
              TestListRdX,
              ARRAY_COUNT (TestListRdX),
              (INT8) NomRD2RD_dd,
              (INT8) NomRD2RD_dd + Range,
              LoopCount,
              Update,
              Outputs->MarginResult,
              ClkShifts,
              ARRAY_COUNT (ClkShifts),
              0,
              RankMask,
              0
              );
    if (Status != mrcSuccess) {
      return mrcFail;
    }
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "%s DDWR2WR\n", "\n ##### Running");
    Status = TrainDDROptParamCliff (
              MrcData,
              ddwr2wr,
              TestListWr,
              ARRAY_COUNT (TestListWr),
              (INT8) NomWR2WR_dd - Range,
              (INT8) NomWR2WR_dd + Range,
              LoopCount,
              Update,
              Outputs->MarginResult,
              ClkShifts,
              ARRAY_COUNT (ClkShifts),
              0,
              RankMask,
              0
              );
    if (Status != mrcSuccess) {
      return mrcFail;
    }
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "%s DDWR2RD\n", "\n ##### Running");
    Status = TrainDDROptParamCliff (
              MrcData,
              ddwr2rd,
              ParamListX,
              ARRAY_COUNT (ParamListX),
              MAX ((INT8) NomWR2RD_dd - Range, 4),
              (INT8) NomWR2RD_dd + Range,
              LoopCount,
              Update,
              Outputs->MarginResult,
              ClkShifts,
              ARRAY_COUNT (ClkShifts),
              0,
              RankMask,
              0
              );
    if (Status != mrcSuccess) {
      return mrcFail;
    }
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "%s DDRD2WR\n", "\n ##### Running");
    Status = TrainDDROptParamCliff (
              MrcData,
              ddrd2wr,
              ParamListX,
              ARRAY_COUNT (ParamListX),
              (INT8) NomRD2WR_dd - Range,
              (INT8) NomRD2WR_dd + Range,
              LoopCount,
              Update,
              Outputs->MarginResult,
              ClkShifts,
              ARRAY_COUNT (ClkShifts),
              0,
              RankMask,
              0
              );
    if (Status != mrcSuccess) {
      return mrcFail;
    }
  }
  // Different Rank turnarounds
  if (RunDR) {
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "%s DRRD2RD\n", "\n ##### Running");
    Status = TrainDDROptParamCliff (
              MrcData,
              drrd2rd,
              TestListRdX,
              ARRAY_COUNT (TestListRdX),
              (INT8) NomRD2RD_dr,
              (INT8) NomRD2RD_dr + Range,
              LoopCount,
              Update,
              Outputs->MarginResult,
              ClkShifts,
              ARRAY_COUNT (ClkShifts),
              0,
              RankMask,
              0
              );
    if (Status != mrcSuccess) {
      return mrcFail;
    }
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "%s DRWR2WR\n", "\n ##### Running");
    Status = TrainDDROptParamCliff (
              MrcData,
              drwr2wr,
              TestListWr,
              ARRAY_COUNT (TestListWr),
              (INT8) MinNomWR2WR_dr,
              (INT8) NomWR2WR_dr + Range,
              LoopCount,
              Update,
              Outputs->MarginResult,
              ClkShifts,
              ARRAY_COUNT (ClkShifts),
              0,
              RankMask,
              0
              );
    if (Status != mrcSuccess) {
      return mrcFail;
    }
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "%s DRWR2RD\n", "\n ##### Running");
    Status = TrainDDROptParamCliff (
              MrcData,
              drwr2rd,
              ParamListX,
              ARRAY_COUNT (ParamListX),
              MAX ((INT8) NomWR2RD_dr - Range, 4),
              (INT8) NomWR2RD_dr + Range,
              LoopCount,
              Update,
              Outputs->MarginResult,
              ClkShifts,
              ARRAY_COUNT (ClkShifts),
              0,
              RankMask,
              0
              );
    if (Status != mrcSuccess) {
      return mrcFail;
    }
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "%s DRRD2WR\n", "\n ##### Running");
    Status = TrainDDROptParamCliff (
              MrcData,
              drrd2wr,
              ParamListX,
              ARRAY_COUNT (ParamListX),
              (INT8) NomRD2WR_dr - Range,
              (INT8) NomRD2WR_dr + Range,
              LoopCount,
              Update,
              Outputs->MarginResult,
              ClkShifts,
              ARRAY_COUNT (ClkShifts),
              0,
              RankMask,
              0
              );
    if (Status != mrcSuccess) {
      return mrcFail;
    }
  }

  // Program SAFE values for ODT and SAmp
  GuardBand = 1;
  if (Outputs->Gear2) {
    GuardBand *= 2;
  }
  UpdateSampOdtTiming (MrcData, GuardBand);
  // ODT Delay (start) / Duration
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "%s mcodts\n", "\n ##### Running");
  Status = TrainDDROptParamCliff (
            MrcData,
            mcodts,
            TestListRdX,          // Including RcvEnaX to the test list
            ARRAY_COUNT (TestListRdX),
            0,
            2 + GuardBand,
            LoopCount,
            Update,
            Outputs->MarginResult,
            ClkShifts,
            ARRAY_COUNT (ClkShifts),
            0,
            RankMask,
            GuardBand
            );
  if (Status != mrcSuccess) {
    return mrcFail;
  }
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "%s mcodtd\n", "\n ##### Running");
  Status = TrainDDROptParamCliff (
            MrcData,
            mcodtd,
            TestListRdX,
            ARRAY_COUNT (TestListRdX),
            (-1 - GuardBand),
            0,
            LoopCount,
            Update,
            Outputs->MarginResult,
            ClkShifts,
            ARRAY_COUNT (ClkShifts),
            0,
            RankMask,
            GuardBand
            );
  if (Status != mrcSuccess) {
    return mrcFail;
  }

  // Restore PCIT value
  for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
    for (Channel = 0; Channel < MaxChannel; Channel += ChannelIncrement) {
      if (!MrcChannelExist (MrcData, Controller, Channel)) {
        continue;
      }
      IpChannel = LP_IP_CH (Lpddr, Channel);
      Offset = OFFSET_CALC_MC_CH (MC0_CH0_CR_SC_PCIT_REG, MC1_CH0_CR_SC_PCIT_REG, Controller, MC0_CH1_CR_SC_PCIT_REG, IpChannel);
      MrcWriteCR (MrcData, Offset, ScPcitSave[Controller][IpChannel].Data);
    }
  }

  // Print out the end results of the training step in Table Format
#ifdef MRC_DEBUG_PRINT
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\n--- Final TA values ---\n");
  for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
    for (Channel = 0; Channel < MaxChannel; Channel += ChannelIncrement) {
      MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\tMc%d.C%d", Controller, Channel);
    }
  }
  for (TaTypeIdx = 0; TaTypeIdx < (sizeof (PrintGroups) / (sizeof (PrintGroups[0]))); TaTypeIdx++) {
    for (GroupIdx = 0; GroupIdx < (sizeof (PrintGroups[0]) / (sizeof (PrintGroups[0][0]))); GroupIdx++) {
      MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\n%s%s", TaString[TaTypeIdx], TypeStr[GroupIdx]);
      for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
        for (Channel = 0; Channel < MaxChannel; Channel += ChannelIncrement) {
          if (!MrcChannelExist (MrcData, Controller, Channel)) {
            continue;
          }
          MrcGetSetMcCh (MrcData, Controller, Channel, PrintGroups[TaTypeIdx][GroupIdx], ReadFromCache, &GetSetVal);
          MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\t%d", (UINT8) GetSetVal);
        }
      }
    } // GroupIdx
  } // TaTypeIdx
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\n");
#endif
  return Status;
}

/**
  General purpose function to optimize an arbitrary value, OptParam (see list above)
    OptParam is generally some timing number that impacts performance or power
    Expects that as OptParam gets smaller*, margins are flat until we hit a cliff
    This procedure defines a cliff as a reduction of 4 ticks in eye height/width
    * In the case of mcodts, higher values are actually worst
    To stress out the timing, xxDDR_CLK is shifted by +/- 15 PI ticks

  @param[in] MrcData         - Include all MRC global data.
  @param[in] OptParam        - Supports Turnaround Timings and ODT Start / Duration
  @param[in] TestList        - List of margin param to check to make sure timing are okay.
  @param[in] NumTests        - The size of TestList
  @param[in] Start           - Start point for this turn around time setting.
  @param[in] Stop            - Stop point for this turnaround time setting.
                                 Note that the Start/Stop values are the real values, not the encoded value
  @param[in] LoopCount       - Length of a given test (per rank)
  @param[in] Update          - Update the CRs and host structure with ideal values
  @param[in] MarginByte      - Byte level margins
  @param[in] ClkShifts       - Array of PI clocks to be shifted
  @param[in] NumR2RPhases    - Number of PI clock phases
  @param[in] rank            - rank to work on
  @param[in] RankMask        - RankMask to be optimized
  @param[in] GuardBand       - GuardBand to be added to last pass value (to be a bit conservative).

  @retval MrcStatus      - If it succeeds return mrcSuccess
**/
MrcStatus
TrainDDROptParamCliff (
  IN MrcParameters *const MrcData,
  IN UINT8                OptParam,
  IN const UINT8          TestList[],
  IN UINT8                NumTests,
  IN INT8                 Start,
  IN INT8                 Stop,
  IN UINT8                LoopCount,
  IN UINT8                Update,
  IN UINT16               MarginByte[MAX_RESULT_TYPE][MAX_RANK_IN_CHANNEL][MAX_CONTROLLER][MAX_CHANNEL][MAX_SDRAM_IN_DIMM][MAX_EDGES],
  IN const INT8           *ClkShifts,
  IN UINT8                NumR2RPhases,
  IN UINT8                rank,
  IN UINT8                RankMask,
  IN UINT8                GuardBand
  )
{
  static const UINT8 OptParamDDType[] = { // Does this test run dr, dd or both
  //drrd2rd ddrd2rd drwr2wr ddwr2wr drrd2wr ddrd2wr drwr2rd ddwr2rd rdodtd wrodtd mcodts mcodtd rtl
    1,      2,      1,      2,      1,      2,      1,      2,      3,     3,     3,     3,     0};
  static const UINT8 RankMapping[16] = {15, 15, 15, 4, 15, 3, 15, 1, 15, 15, 15, 15, 5, 2, 15, 0};
    // Order of rank turnarounds for dr & dd.
  static const UINT32  RankOrder[2][6] = {
    { 0x32320101, 0x10101010, 0x32323232, 0x20, 0x10, 0x23 }, // RankOrder[0]: drsd - same DIMM
    { 0x21303120, 0x00002120, 0x00003020, 0x20, 0x00, 0x00 }  // RankOrder[1]: drdd - diff DIMM
  };
  static const Cpgc20Address CPGCAddressConst = {
    CPGC20_RANK_2_ROW_COL_2_BANK,
    CPGC20_FAST_Y,
    0,
    0,
    0,
    0,
    8,
    8
  };
  Cpgc20Address     CpgcAddress;
  const MrcInput    *Inputs;
  MrcDebug          *Debug;
  const MRC_FUNCTION *MrcCall;
  MrcOutput         *Outputs;
  MrcChannelOut     *ChannelOut;
  MrcStatus         Status;
  MRC_PATTERN_CTL   PatternCtl; // For 8 bit VA, this walks through each WDB pointer ~ 2X
  INT64             WrRdSg[MAX_CONTROLLER][MAX_CHANNEL];
  INT64             RdWrSg[MAX_CONTROLLER][MAX_CHANNEL];
  UINT32            BERStats[4];  // Track BER results
  UINT32            RankList;
  UINT32            Offset;
  UINT32            Controller;
  UINT32            Channel;
  UINT32            IpChannel;
  UINT16            Margins[MAX_TESTS_OPT_PARAM_CLIFF][2][2][MAX_CONTROLLER][MAX_CHANNEL][MAX_SDRAM_IN_DIMM];  // Tests X DR/DD x ClkPhases x Ch X Byte
  UINT16            NumCL;  // Number of cachelines per SubSeq
  UINT16            m;
  UINT16            ByteMask;
  UINT16            ByteFailMask[MAX_CONTROLLER][MAX_CHANNEL];  // Per ch mask indicating which bytes have failed
  UINT16            ByteDone;
  INT8              Inc;
  INT8              Off;
  INT8              Index;
  INT8              LastPass[MAX_CONTROLLER][MAX_CHANNEL][MAX_SDRAM_IN_DIMM];     // Lass Pass Value for off
  INT8              Begin;
  INT8              End;
  INT8              ChLastPass;
  INT8              ActualGuardBand;
  UINT8             MaxChannel;
  UINT8             McChannelMask;
  UINT8             Byte;
  UINT8             Rank;
  UINT8             McChBitMask;
  UINT8             RankCount;
  UINT8             OrigRankCount;
  UINT8             McChBitMaskdd;
  UINT8             RankMaskCh;
  UINT8             GlobalRankMask;
  UINT8             drddPresent[2]; // [0]: ChBitMask for dr, [1]: ChBitMask for dd
  UINT8             CmdPat;
  UINT8             BMap[9];  // Needed for GetBERMarginByte function
  UINT8             MarginLimit;  // Need to change it to 20%of eye heigth
  UINT8             ResetDDR;
  UINT8             SelfRefresh;
  UINT8             offs[MAX_CONTROLLER][MAX_CHANNEL];
  UINT8             Param;
  UINT8             iparam;
  UINT8             dd;
  UINT8             test0;
  UINT8             v0;
  UINT8             RankOrderIndex;
  UINT8             UpdateHostMargin;
  UINT8             Done;
  UINT8             MaxMargin;
  UINT8             ResultType;
  UINT8             WDBIncRate; // Number of cachelines between incrementing WDB.
  UINT8             LoopEnd;
  UINT8             ResultRank;
  UINT8             ShiftValue;
  BOOLEAN           WriteVrefParam;
  BOOLEAN           DramVref;
  BOOLEAN           Ddr4;
  BOOLEAN           Lpddr;
  BOOLEAN           IsDual;
  BOOLEAN           ODT;
  BOOLEAN           PerByte;
  BOOLEAN           NotRankTraining;
  BOOLEAN           FindFirstPass;
#ifdef MRC_DEBUG_PRINT
  INT64             GetSetVal;
  INT8              ChLastPass1[MAX_CONTROLLER][MAX_CHANNEL];
#endif // MRC_DEBUG_PRINT
  UINT8             RepeatInitialTest;
  MC0_REQ0_CR_CPGC_SEQ_RANK_L2P_MAPPING_A_STRUCT  CpgcChSeqRankL2PMapping;
  MC0_REQ0_CR_CPGC2_ADDRESS_SIZE_STRUCT           Cpgc2AddrSize;
//  REUT_CH_SEQ_CFG_0_STRUCT                         ReutChSeqCfg; // @todo <ICL> Update with CPGC 2.0 implementation
//  REUT_CH_SEQ_BASE_ADDR_WRAP_0_STRUCT ReutChSeqBaseAddrWrap;

  Inputs          = &MrcData->Inputs;
  MrcCall         = Inputs->Call.Func;
  Outputs         = &MrcData->Outputs;
  Debug           = &Outputs->Debug;
  Status          = mrcSuccess;
  Ddr4            = (Outputs->DdrType == MRC_DDR_TYPE_DDR4);
  Lpddr           = Outputs->Lpddr;
  Done            = 0;
  drddPresent[0]  = 0;
  drddPresent[1]  = 0;
  MarginLimit     = 10;  // Drop of X% in margin means failure
  ResetDDR        = 1;
  SelfRefresh     = 0;
  WDBIncRate      = 13;
  NumCL           = (Lpddr) ? 64 : 128;
  MaxChannel      = Outputs->MaxChannels;
  RepeatInitialTest = 4;  // Run initial RTL point 5 times

  MRC_DEBUG_ASSERT (NumTests <= MAX_TESTS_OPT_PARAM_CLIFF, Debug, "ERROR: too many tests passed into TrainDDROptParamCliff()\n");

  PatternCtl.IncRate  = 0;
  PatternCtl.DQPat    = BasicVA;
  PatternCtl.EnableXor = FALSE;
  PatternCtl.PatSource = MrcPatSrcDynamic;
  MrcCall->MrcSetMem ((UINT8 *) ByteFailMask, sizeof (ByteFailMask), 0);
  MrcCall->MrcSetMem ((UINT8 *) offs, sizeof (offs), 0);
  MrcCall->MrcSetMem ((UINT8 *) BERStats, sizeof (BERStats), 0);
  MrcCall->MrcCopyMem ((UINT8 *) &CpgcAddress, (UINT8 *) &CPGCAddressConst, sizeof (CpgcAddress));
  switch (OptParam) {
  case mcodts:
  case mcodtd:
    CpgcAddress.AddrIncOrder = CPGC20_BANK_2_ROW_COL_2_RANK;
    CpgcAddress.ColSizeBits = 3;
    NumCL           = 32;
    break;

  case rtl:
    break;

  default:
  case drwr2rd:
  case drwr2wr:
  case drrd2rd:
  case drrd2wr:
    //Use currently defined CpgcAddress
    break;
  }
  for (Byte = 0; Byte < ARRAY_COUNT (BMap); Byte++) {
    BMap[Byte] = Byte;
  }

  GlobalRankMask = Outputs->ValidRankMask & RankMask;

  NotRankTraining = (OptParam == rtl);
  FindFirstPass   = (OptParam == rtl);  // FindFirstPass logic only works for RTL!
  ODT             = (OptParam == rdodtd) || (OptParam == wrodtd) || (OptParam == mcodtd) || (OptParam == mcodts);
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\nNotRankTraining = %u, ODT = %d\n", NotRankTraining, ODT);

  // Decide which channels need to be run and program NumCachelines and LC
  for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
    for (Channel = 0; Channel < MaxChannel; Channel++) {
      ChannelOut = &Outputs->Controller[Controller].Channel[Channel];
      if (ChannelOut->ValidRankBitMask) {
        McChannelMask = 1 << MC_CH_IDX (Controller, Channel, MaxChannel);
        RankMaskCh  = ChannelOut->ValidRankBitMask;
        IsDual      = ((RankMaskCh & 0xC) == 0xC) || ((RankMaskCh & 0x3) == 0x3);

        // Continue if no ranks in this channel
        if ((RankMaskCh & RankMask) == 0) {
          continue;
        }

        if ((OptParamDDType[OptParam] & 0x2) && (ChannelOut->DimmCount == 2)) {
          drddPresent[1] |= McChannelMask; // dd parameter and channel has 2 DIMMs
        }

        if (((OptParamDDType[OptParam] & 0x1) && IsDual) || NotRankTraining) {
          drddPresent[0] |= McChannelMask; // dr parameter and channel has a dual rank
        }

        if (ODT && ((drddPresent[0] & McChannelMask) == 0)) {
          // ODT matters when Single rank
          // dr parameter and channel has a dual rank
          drddPresent[0] |= McChannelMask;
        }
      }
    }
  }

  McChBitMask = drddPresent[1] | drddPresent[0]; // Channel is present if it has either a dr or dd
  MRC_DEBUG_MSG (
    Debug,
    MSG_LEVEL_NOTE,
    "drddPresent[0] = 0x%x, drddPresent[1] = 0x%x, McChBitMask = 0x%x\n",
    drddPresent[0],
    drddPresent[1],
    McChBitMask
    );

  // There is nothing to optimize for this parameter
  if ((McChBitMask == 0) || (Stop <= Start)) {
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "McChBitMask = %d, Start = 0x%x, Stop = 0x%x\n", McChBitMask, Start, Stop);
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_ERROR, "No need to optimize TA, OptParam = %d\n", OptParam);
    return (Inputs->ExitOnFailure) ? mrcFail : mrcSuccess;
  }
  // Setup the REUT Test
  if ((OptParam == ddwr2rd) || (OptParam == drwr2rd)) {
    CmdPat = PatWrRdTA;
    Outputs->DQPat  = TurnAroundWR;
    // WrRdSg is a long delay.  Extend RdWrSg to cover the remaining WrRdSg delay so we get WrRdDr properly.
    for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
      for (Channel = 0; Channel < MaxChannel; Channel++) {
        if ((!MrcChannelExist (MrcData, Controller, Channel)) || IS_MC_SUB_CH (Lpddr, Channel)) {
            continue;
        }
        MrcGetSetMcCh (MrcData, Controller, Channel, GsmMctWRRDsg, ReadFromCache, &WrRdSg[Controller][Channel]);
        MrcGetSetMcCh (MrcData, Controller, Channel, GsmMctRDWRsg, ReadFromCache, &RdWrSg[Controller][Channel]);
        MrcGetSetMcCh (MrcData, Controller, Channel, GsmMctRDWRsg, WriteCached,   &WrRdSg[Controller][Channel]);
        // Must update the XARB bubble injector when TAT values change
        SetTcBubbleInjector (MrcData, Controller, Channel);
      }
    }
  } else if ((OptParam == ddrd2wr) || (OptParam == drrd2wr)) {
    CmdPat          = PatRdWrTA;
    Outputs->DQPat  = TurnAroundRW;
//    RankInc         = 1;
  } else if (ODT) {
    CmdPat          = PatODTTA;
    Outputs->DQPat  = TurnAroundODT;
//    RankInc         = 1;
  } else if (OptParam == rtl) {
    CmdPat = PatWrRd;
    Outputs->DQPat  = Outputs->Lpddr ? RdRdTA_All : RdRdTA;
    // Less optimistic values since we are updating values and RMT fails
    WDBIncRate  = 16;
    NumCL       = 4;
    LoopCount = Outputs->Lpddr ? LoopCount - 3 : LoopCount - 1;
  } else {
    CmdPat = PatWrRd;
    Outputs->DQPat  = TurnAround;
  }

  PatternCtl.DQPat        = Outputs->DQPat;
  PatternCtl.IncRate      = WDBIncRate;

  // SOE=0, EnCADB=0, EnCKE=0, SubSeqWait=0
  if (OptParam == rtl) {
    SetupIOTestBasicVA (MrcData, McChBitMask, LoopCount, NSOE, 0, 0, 8, PatWrRd, 0, 0);
    for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
      for (Channel = 0; Channel < MaxChannel; Channel++) {
        SelectReutRanks (MrcData, (UINT8) Controller, (UINT8) Channel, RankMask, FALSE, 0);
      }
    }
  } else {
    SetupIOTest (MrcData, McChBitMask, CmdPat, NumCL, LoopCount, &CpgcAddress, NSOE, &PatternCtl, 0, 0, 0);
  }

  Outputs->DQPatLC = MRC_BIT0 << (LoopCount - MrcLog2 ((UINT32) (NumCL - 1)));
  if (Outputs->DQPatLC < 1) {
    Outputs->DQPatLC = 1;
  }
  // Optimize parameter per byte.  Everything else is per channel
  PerByte = (OptParam == mcodts) || (OptParam == mcodtd);

  // Keep track of which bytes have failed and are we done yet
  ByteDone = (1 << Outputs->SdramCount) - 1;

  // ###########################################################
  // ####  Loop through OptParam X DD X ClkPhases X Params and measure margin #####
  // ###########################################################
  if (OptParam == mcodts) {
    // In the case of mcodts, higher values are actually worst.
    Begin = Start;
    End   = Stop;
    Inc   = 1;
  } else {
    Begin = Stop;
    End   = Start;
    Inc   = -1;
  }

  if ((Outputs->Gear2) && (OptParam != mcodts) && (OptParam != mcodtd) && (OptParam != rtl)) {
    Inc *= 2;
  }

  ActualGuardBand = (Inc * GuardBand);

  MRC_DEBUG_MSG (
    Debug,
    MSG_LEVEL_NOTE,
    "Start = %d, Stop = %d, Begin = %d, End = %d, Inc = %d\n",
    Start,
    Stop,
    Begin,
    End,
    Inc
    );
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, (OptParam == rtl) ? "Rank = %d\n" : "", rank);
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Channel 0\t\t\t\t\t\t\t\t1\nByte\t");
  MRC_DEBUG_MSG (
    Debug,
    MSG_LEVEL_NOTE, (
    Outputs->SdramCount == MAX_SDRAM_IN_DIMM
    ) ? "0\t1\t2\t3\t4\t5\t6\t7\t8\t0\t1\t2\t3\t4\t5\t6\t7\t8\n" :
    "0\t1\t2\t3\t4\t5\t6\t7\t0\t1\t2\t3\t4\t5\t6\t7\n"
    );

  // Init Variables
  for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
    for (Channel = 0; Channel < MaxChannel; Channel++) {
      if (MrcChannelExist (MrcData, Controller, Channel)) {
        MrcCall->MrcSetMem ((UINT8 *) &LastPass[Controller][Channel][0], Outputs->SdramCount, (UINT8) (Begin - ActualGuardBand));
        for (Byte = 0; Byte < Outputs->SdramCount; Byte++) {
          for (iparam = 0; iparam < NumTests; iparam++) {
            for (dd = 0; dd < 2; dd++) {
              for (test0 = 0; test0 < NumR2RPhases; test0++) {
                Margins[iparam][dd][test0][Controller][Channel][Byte] = 1280;
              }
            }
          }
        }
      }
    }
  }
  // Walk through different OptParam values
  for (Off = (INT8) Begin; Off != (INT8) (End + Inc); Off += Inc) {
    if (Done) {
      break;
    }
    Index = (Off - Begin) * Inc; // Index = 0, 1, 2..
    if (Index == 1) {
      if ((RepeatInitialTest != 0) && FindFirstPass) {  // Repeat initial measurement of RTL to filter out repetition noise
        Off -= Inc;
        Index = 0;
        RepeatInitialTest--;
      }
    }
    // Inc can only take a value of +/- 1.
    if ((Index == 1) && FindFirstPass) {
      Inc  *= -1;
      Off   = End;
      End   = Begin - Inc;  // One Inc less since we have already done Index 0.
      Begin = Off - Inc;    // One Inc less to get us starting at Index 1
      MRC_DEBUG_MSG (
        Debug,
        MSG_LEVEL_NOTE,
        "Find First Pass - Walking backwards.\n Off = %d, Begin = %d, End = %d, Inc = %d\n",
        Off,
        Begin,
        End,
        Inc
        );
    }
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Param^ Offset-> %d\n   Actl\t", Off);

    for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
      for (Channel = 0; Channel < MaxChannel; Channel++) {
        ChannelOut = &Outputs->Controller[Controller].Channel[Channel];
        RankMaskCh = ChannelOut->ValidRankBitMask & RankMask;
        // if nothing for this channel OR No Ranks in this channel
        if (!(MC_CH_MASK_CHECK (McChBitMask, Controller, Channel, MaxChannel)) || (RankMaskCh == 0)) {
#ifdef MRC_DEBUG_PRINT
          if (Channel == 0) { //@todo need to adjust for MaxChannel printing
            if (Outputs->SdramCount == (MAX_SDRAM_IN_DIMM - 1)) {
              MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\t\t\t\t\t\t\t\t");
            } else {
              MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\t\t\t\t\t\t\t\t\t");
            }
          }
#endif // MRC_DEBUG_PRINT
          continue;
        }

        if (FindFirstPass && (Index == 0)) {
          // We use the current RTL value for the initial test
#ifdef MRC_DEBUG_PRINT
          MrcGetSetMcChRnk (MrcData, Controller, Channel, rank, RoundTripDelay, ReadFromCache, &GetSetVal);
#endif
          MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\n Mc%u C%u initial RTL: %u", Controller, Channel, (UINT32) GetSetVal);
        } else {
          LoopEnd = (UINT8) ((PerByte) ? Outputs->SdramCount : 1);
          for (Byte = 0; Byte < LoopEnd; Byte++) {
            UpdateTAParamOffset (MrcData, Controller, Channel, Byte, OptParam, Off, Update, MRC_PRINTS_ON, RankMaskCh);
          }
          if ((OptParam == wrodtd) || (OptParam == rdodtd)) {
            // Set UpdateHost to TRUE because TA values share the same register
            MrcTatStretch (MrcData, Controller, Channel, OptParam, Off - Start, TRUE);
          }
        }
      }
    }
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\n");

    // Test both: different dr and dd as required
    for (dd = 0; dd < 2; dd++) {
      if (Done) {
        break;
      }
      // Check if this test type should be run
      McChBitMaskdd = drddPresent[dd];
      if (McChBitMaskdd == 0) {
        continue;
      }

      if (OptParam != rtl) {
        MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, (dd == 0) ? "Dual Rank\n" : "Dual Dimm\n");
        // Select Ranks in the correct order based on the test type
        // Need to re-order the ranks based on the value of ddw2r
        for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
          for (Channel = 0; Channel < MaxChannel; Channel++) {
            ChannelOut = &Outputs->Controller[Controller].Channel[Channel];
            RankMaskCh = ChannelOut->ValidRankBitMask & RankMask;
            if ((RankMaskCh == 0) || IS_MC_SUB_CH (Lpddr, Channel)) {
              // No Ranks in this channel
              // For LPDDR4/5, only program register on even channels.
              continue;
            }
            // Initialize variables and read out ordered rank list
            CpgcChSeqRankL2PMapping.Data = 0;
            RankCount = 0;
            IpChannel = LP_IP_CH (Lpddr, Channel);
            if (NotRankTraining) {
              RankList = 0x00003210;
            } else {
              RankOrderIndex = RankMapping[RankMaskCh];
              if (RankOrderIndex == 15) {
                RankList = 0x00003210;
              } else {
                RankList = RankOrder[dd][RankOrderIndex];
              }
            }

            while (RankList > 0) {
              if ((RankCount % 6 == 0) && (RankCount)) {
                // Program the RankMapping register if we exceed 6 ranks that fits within the register width

                Offset = OFFSET_CALC_MC_CH (MC0_REQ0_CR_CPGC_SEQ_RANK_L2P_MAPPING_A_REG,
                                            MC1_REQ0_CR_CPGC_SEQ_RANK_L2P_MAPPING_A_REG, Controller,
                                            MC0_REQ1_CR_CPGC_SEQ_RANK_L2P_MAPPING_A_REG, IpChannel);
                Offset += (RankCount / 6) * (MC0_REQ0_CR_CPGC_SEQ_RANK_L2P_MAPPING_B_REG - MC0_REQ0_CR_CPGC_SEQ_RANK_L2P_MAPPING_A_REG);
                MrcWriteCR (MrcData, Offset, CpgcChSeqRankL2PMapping.Data);

                // Reset RankMapping register
                CpgcChSeqRankL2PMapping.Data = 0;
              }
              Rank = (RankList & 0xF); // Nibble by Nibble
              RankList = (RankList >> 4);
              if (!(RankMaskCh & (1 << Rank))) {
                continue;
              }

              ShiftValue = (RankCount % 6) * MC0_REQ0_CR_CPGC_SEQ_RANK_L2P_MAPPING_A_L2P_RANK0_MAPPING_WID;
              CpgcChSeqRankL2PMapping.Data |= (Rank << ShiftValue);
              RankCount++;
            }

            if (CpgcChSeqRankL2PMapping.Data != 0) {
              // Program the RankMapping register that did not get programmed in the while loop

              Offset = OFFSET_CALC_MC_CH (MC0_REQ0_CR_CPGC_SEQ_RANK_L2P_MAPPING_A_REG,
                                          MC1_REQ0_CR_CPGC_SEQ_RANK_L2P_MAPPING_A_REG, Controller,
                                          MC0_REQ1_CR_CPGC_SEQ_RANK_L2P_MAPPING_A_REG, IpChannel);
              Offset += (RankCount / 6) * (MC0_REQ0_CR_CPGC_SEQ_RANK_L2P_MAPPING_B_REG - MC0_REQ0_CR_CPGC_SEQ_RANK_L2P_MAPPING_A_REG);
              MrcWriteCR (MrcData, Offset, CpgcChSeqRankL2PMapping.Data);
            }

            Offset = OFFSET_CALC_MC_CH (MC0_REQ0_CR_CPGC2_ADDRESS_SIZE_REG,
                                        MC1_REQ0_CR_CPGC2_ADDRESS_SIZE_REG, Controller,
                                        MC0_REQ1_CR_CPGC2_ADDRESS_SIZE_REG, IpChannel);
            Cpgc2AddrSize.Data = MrcReadCR64 (MrcData, Offset);
            OrigRankCount = (UINT8) (Cpgc2AddrSize.Bits.Block_Size_Max_Rank + 1);
            if (OrigRankCount != RankCount) {
              Cpgc2AddrSize.Bits.Block_Size_Max_Rank  = RankCount - 1;
              Cpgc2AddrSize.Bits.Region_Size_Max_Rank = RankCount - 1;
              MrcWriteCR64 (MrcData, Offset, Cpgc2AddrSize.Data);
              Cpgc20AdjustNumOfRanks (MrcData, Controller, IpChannel, OrigRankCount, RankCount);
            }
          } // for Channel
        } // for Controller
      } // if not "rtl" param
      // ###################################################
      // ### Walk through different sets of rank2rank timings  ###
      // ###################################################
      for (test0 = 0; test0 < NumR2RPhases; test0++) {
        if (Done) {
          break;
        }

        v0 = ClkShifts[test0];

        // Program rank offsets differently for dd vs. dr
        if (NotRankTraining) {
          for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
            for (Channel = 0; Channel < MaxChannel; Channel++) {
              if (!(MC_CH_MASK_CHECK (McChBitMaskdd, Controller, Channel, MaxChannel))) {
                offs[Controller][Channel] = 0;
              } else {
                // Shift all signals in the channel(Clk/Ctl/Cmd/Dq) by v0
                offs[Controller][Channel] = v0;
              }
            }
          }
          if (v0 != 0) {
            ShiftCh2Ch (MrcData, RankMask, offs, ResetDDR, SelfRefresh, 0);
          }
        } else if (dd == 1) {
          // For DD
          // Shift Clk/DQ on one DIMM by v0 and Clk/DQ on other DIMM by -v0
          // @todo: CTL picode should be optionally shifted to improve margins
          SetCmdMargin (MrcData, McChBitMaskdd, 0x3, WrT, v0, 0, ResetDDR, SelfRefresh);
          SetCmdMargin (MrcData, McChBitMaskdd, 0xC, WrT, -v0, 0, ResetDDR, SelfRefresh);
        } else {
          // For DR
          // Shift Clk/DQ on front side by v0 and Clk/DQ on backside by -v0
          // @todo: CTL picode should be optionally shifted to improve margins
          SetCmdMargin (MrcData, McChBitMaskdd, 0x5, WrT, v0, 0, ResetDDR, SelfRefresh);
          SetCmdMargin (MrcData, McChBitMaskdd, 0xA, WrT, -v0, 0, ResetDDR, SelfRefresh);
        }
        // Test different margin param
        for (iparam = 0; iparam < NumTests; iparam++) {
          Param = TestList[iparam];
          WriteVrefParam = ((Param == WrV) || (Param == WrFan2) || (Param == WrFan3));
          MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "%s ", gMarginTypesStr[Param]);

          ResultType = GetMarginResultType (Param);

          // MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "%d ", MinMarginLimit);
          // Calculate MaxMargin and Starting Point for margin search
          ResultRank = rank;
          if ((Param == WrV) || (Param == WrFan3) || (Param == WrFan2) ||
              (Param == RdV) || (Param == RdFan3) || (Param == RdFan2) || (Param == CmdV)) {
            MaxMargin = GetVrefOffsetLimits (MrcData, Param);
          } else {
            MaxMargin = MAX_POSSIBLE_TIME;
          }
          DramVref = (Ddr4 || Lpddr) && WriteVrefParam;
          // Are we done yet or should we keep testing?
          Done = 1;
          for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
            for (Channel = 0; Channel < MaxChannel; Channel++) {
              if (!(MC_CH_MASK_CHECK (McChBitMaskdd, Controller, Channel, MaxChannel))) {
                continue;
              }

              ChannelOut  = &Outputs->Controller[Controller].Channel[Channel];
              RankMaskCh  = ChannelOut->ValidRankBitMask & RankMask;
              if (RankMaskCh == 0) {
                continue; // No Ranks in this channel
              }

              // When FindFirstPass is used, all Bytes have to have passed before we stop.
              // We uses ByteFailMask[] to track the passing bytes in this case.
              if (PerByte || FindFirstPass) {
                if (ByteFailMask[Controller][Channel] != ByteDone) {
                  Done = 0;
                }
              } else {
                if (ByteFailMask[Controller][Channel] == 0) {
                  Done = 0;
                }
              }
            }
          }

          if (Done) {
            break;
          }

          Status = GetMarginByte (MrcData, Outputs->MarginResult, Param, 0, 0xF);
          if (Status != mrcSuccess) {
            return Status;
          }

          MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "% 3d\t", (INT8) v0);
          Status = MrcGetBERMarginByte (
                    MrcData,
                    Outputs->MarginResult,
                    McChBitMaskdd,
                    GlobalRankMask,
                    GlobalRankMask,
                    Param,
                    0,  // Mode
                    BMap,
                    1,
                    MaxMargin,
                    0,
                    BERStats
                    );
          if (Status != mrcSuccess) {
            return Status;
          }

          if (DramVref) {
            // We return results on first available rank.
            for (Rank = 0; Rank < MAX_RANK_IN_CHANNEL; Rank++) {
              if ((1 << Rank) & GlobalRankMask) {
                ResultRank = Rank;
                break;
              }
            }
          }
          // Record Results
          UpdateHostMargin = 1;

          for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
            for (Channel = 0; Channel < MaxChannel; Channel++) {
              ChannelOut = &Outputs->Controller[Controller].Channel[Channel];
              RankMaskCh = ChannelOut->ValidRankBitMask & RankMask;
              // if nothing for this channel OR No Ranks in this channel
              if (!(MC_CH_MASK_CHECK (McChBitMaskdd, Controller, Channel, MaxChannel)) || (RankMaskCh == 0)) {
#ifdef MRC_DEBUG_PRINT
                if (Channel == 0) {
                  if (Outputs->SdramCount == (MAX_SDRAM_IN_DIMM - 1)) {
                    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\t\t\t\t\t\t\t\t");
                  } else {
                    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\t\t\t\t\t\t\t\t\t");
                  }
                }
#endif // MRC_DEBUG_PRINT
                continue;
              }

              for (Byte = 0; Byte < Outputs->SdramCount; Byte++) {
                // For this optimization, it makes more sense to look at the full sum
                ByteMask = 1 << Byte;
                m = EffectiveMargin (
                      MarginByte[ResultType][ResultRank][Controller][Channel][Byte][0],
                      MarginByte[ResultType][ResultRank][Controller][Channel][Byte][1]
                      );

                if (m < 20) {
                  m = 20;
                }
                MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "%d", m);

                // If previously failed, this is also a failure unless we are looking for
                // the first passing offset.
                if ((ByteFailMask[Controller][Channel] & ByteMask) && !FindFirstPass) {
                  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "#\t");
                  continue;
                }

                // Check if the first test is failing with zero margin.
                // Stop the test in this case - initial point should be passing.
                if ((Index == 0) && (m == 20)) {
                    ByteFailMask[Controller][Channel] = ByteDone;
                    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "#\t");
                    continue;
                }

                /* @todo: disable this as it make the results very conservative
                // Byte fails if margin is below MinMarginLimit at any time
                if (m < MinMarginLimit) {
                  // If we are looking for pass, continue and do not update LastPass
                  if (TRUE == FindFirstPass) {
                    if (Index == 0) {
                      // When training from the most aggressive setting to the conservative setting,
                      // if we fail the first setting we stop.
                      ByteFailMask[Controller][Channel] = ByteDone;
                    }
                    UpdateHostMargin = 0;
                  } else {
                    ByteFailMask[Controller][Channel] |= ByteMask;
                    LastPass[Controller][Channel][Byte] = Off - Inc - ActualGuardBand;
                  }
                  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "#\t");
                  continue;
                }
                */
                if (Index == 0) {
                  // Get the smallest marging at Index 0
                  if (Margins[iparam][dd][test0][Controller][Channel][Byte] > m) {
                    Margins[iparam][dd][test0][Controller][Channel][Byte] = m;
                  }
                } else {
                  // Check if we dropped more than the percent allowed
                  if (m < ((Margins[iparam][dd][test0][Controller][Channel][Byte] * (100 - MarginLimit)) / 100)) {
                    if (!FindFirstPass) {
                      ByteFailMask[Controller][Channel] |= ByteMask;
                      LastPass[Controller][Channel][Byte] = Off - Inc - ActualGuardBand;
                    }
                    UpdateHostMargin = 0;
                    MRC_DEBUG_MSG (
                      Debug,
                      MSG_LEVEL_NOTE,
                      "#-%d\t",
                      (ABS (m - Margins[iparam][dd][test0][Controller][Channel][Byte]) * 100) / Margins[iparam][dd][test0][Controller][Channel][Byte]
                      );
                    continue;
                  } else {
                    if (FindFirstPass) {
                      if ((ByteFailMask[Controller][Channel] & ByteMask) != ByteMask) {
                        LastPass[Controller][Channel][Byte] = Off - ActualGuardBand;
                        ByteFailMask[Controller][Channel] |= ByteMask;
                      }
                    } else {
                      LastPass[Controller][Channel][Byte] = Off - ActualGuardBand;
                    }
                  }
                }

                MRC_DEBUG_MSG (
                  Debug,
                  MSG_LEVEL_NOTE,
                  ".%c%d\t",
                  (m > Margins[iparam][dd][test0][Controller][Channel][Byte]) ? '+' : '-',
                  (ABS(m - Margins[iparam][dd][test0][Controller][Channel][Byte]) * 100) / Margins[iparam][dd][test0][Controller][Channel][Byte]
                  );
              } // for Byte
            } // for Channel
          } // for Controller

          // Stop the test if we fail on the initial setting
          if (Index == 0) {
            for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
              for (Channel = 0; Channel < MaxChannel; Channel++) {
                ChannelOut = &Outputs->Controller[Controller].Channel[Channel];
                RankMaskCh = ChannelOut->ValidRankBitMask & RankMask;
                // if nothing for this channel OR No Ranks in this channel
                if (!(MC_CH_MASK_CHECK (McChBitMaskdd, Controller, Channel, MaxChannel)) || (RankMaskCh == 0)) {
                  continue;
                }
                if (ByteFailMask[Controller][Channel] != 0) {
                  MRC_DEBUG_MSG (Debug, MSG_LEVEL_ERROR, "\nError! Mc%u.Ch%u failed initial value!\n", Controller, Channel);
                  return mrcFail;
                }
              }
            }
          }

          if (UpdateHostMargin) {
            Status = ScaleMarginByte (MrcData, Outputs->MarginResult, Param, ResultRank);
          }

          MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\n");
        } // for iparam

        MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\n");

        // Clean up
        if (NotRankTraining) {
          MrcCall->MrcSetMem ((UINT8 *) offs, sizeof (offs), 0);
          // UpdateHost = 1 so that we reset the IntClkAlignedMargins->Valid flag in ShiftDQPIs
          if (v0 != 0) {
            ShiftCh2Ch (MrcData, RankMask, offs, ResetDDR, SelfRefresh, 1);
          }
        } else {
          SetCmdMargin (MrcData, McChBitMaskdd, RankMask, WrT, 0, 0, ResetDDR, SelfRefresh);
        }
      } // for test0 in ClkShifts[]

      MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\n");
    } // for dd
    if ((OptParam == wrodtd) || (OptParam == rdodtd)) {
      for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
        for (Channel = 0; Channel < MaxChannel; Channel++) {
          ChannelOut  = &Outputs->Controller[Controller].Channel[Channel];
          RankMaskCh  = ChannelOut->ValidRankBitMask & RankMask;
          // If nothing for this channel OR No Ranks in this channel
          if (!(MC_CH_MASK_CHECK (McChBitMask, Controller, Channel, MaxChannel)) || (RankMaskCh == 0) || (IS_MC_SUB_CH (Lpddr, Channel))) {
            continue;
          }
          // MrcTatStretch was called with UpdateHost so need to clean back to original values
          MrcTatStretch (MrcData, Controller, Channel, OptParam, - (Off - Start), TRUE);
        }
      }
    }
  } // for Off

  // If we are sweeping aggressive settings to conservative settings, we
  // need to restore original Inc, Begin, and End values to select the
  // proper offset if bytes have varying offsets values for a parameter
  // that is NOT specified per Byte.
  if (FindFirstPass) {
    Off   = End;         // Temp storage for swap
    End   = Begin + Inc;
    Begin = Off + Inc;
    Inc  *= -1;
    MRC_DEBUG_MSG (
      Debug,
      MSG_LEVEL_NOTE,
      "Find First Pass - Reverting Inc, Begin, and End\n Begin = %d, End = %d, Inc = %d,\n",
      Begin,
      End,
      Inc
      );
  }

#ifdef MRC_DEBUG_PRINT
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Optimal offset per Byte\n\t");
  // Print optimal value
  for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
    for (Channel = 0; Channel < MaxChannel; Channel++) {
      ChannelOut            = &Outputs->Controller[Controller].Channel[Channel];
      RankMaskCh            = ChannelOut->ValidRankBitMask & RankMask;
      ChLastPass1[Controller][Channel]  = End;
      // if nothing for this channel OR No Ranks in this channel
      if (!(MC_CH_MASK_CHECK (McChBitMask, Controller, Channel, MaxChannel)) || (RankMaskCh == 0)) {
        if (Channel == 0) {
          if (Outputs->SdramCount == (MAX_SDRAM_IN_DIMM - 1)) {
            MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\t\t\t\t\t\t\t\t");
          } else {
            MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\t\t\t\t\t\t\t\t\t");
          }
        }
        continue;
      }

      for (Byte = 0; Byte < Outputs->SdramCount; Byte++) {
        MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "%d\t", LastPass[Controller][Channel][Byte]);
        if ((Inc > 0) && (ChLastPass1[Controller][Channel] > LastPass[Controller][Channel][Byte])) {
          ChLastPass1[Controller][Channel] = LastPass[Controller][Channel][Byte];
        }

        if ((Inc < 0) && (ChLastPass1[Controller][Channel] < LastPass[Controller][Channel][Byte])) {
          ChLastPass1[Controller][Channel] = LastPass[Controller][Channel][Byte];
        }
      }
    }
  }

  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\n");
  for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
    for (Channel = 0; Channel < MaxChannel; Channel++) {
      ChannelOut  = &Outputs->Controller[Controller].Channel[Channel];
      RankMaskCh  = ChannelOut->ValidRankBitMask & RankMask;
      // if nothing for this channel OR No Ranks in this channel
      if (!(MC_CH_MASK_CHECK (McChBitMask, Controller, Channel, MaxChannel)) || (RankMaskCh == 0)) {
        continue;
      }

      MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Optimal offset Mc %d Channel %d = %d\n", Controller, Channel, ChLastPass1[Controller][Channel]);
    }
  }
#endif // MRC_DEBUG_PRINT
  // Program new value
  for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
    for (Channel = 0; Channel < MaxChannel; Channel++) {
      ChannelOut  = &Outputs->Controller[Controller].Channel[Channel];
      RankMaskCh  = ChannelOut->ValidRankBitMask & RankMask;
      // if nothing for this channel OR No Ranks in this channel
      if (!(MC_CH_MASK_CHECK (McChBitMask, Controller, Channel, MaxChannel)) || (RankMaskCh == 0)) {
        continue;
      }

      // Start with the most aggressive setting
      ChLastPass = End;
      for (Byte = 0; Byte < Outputs->SdramCount; Byte++) {
        if (Update == 0) {
          LastPass[Controller][Channel][Byte] = Begin;
        }

        if ((Inc > 0) && (ChLastPass > LastPass[Controller][Channel][Byte])) {
          ChLastPass = LastPass[Controller][Channel][Byte];
        }

        if ((Inc < 0) && (ChLastPass < LastPass[Controller][Channel][Byte])) {
          ChLastPass = LastPass[Controller][Channel][Byte];
        }

        if (PerByte) {
          UpdateTAParamOffset (MrcData, Controller, Channel, Byte, OptParam, LastPass[Controller][Channel][Byte], Update, MRC_PRINTS_OFF, RankMaskCh);
        }
      }

      if (PerByte == FALSE) {
        UpdateTAParamOffset (MrcData, Controller, Channel, 0, OptParam, ChLastPass, Update, MRC_PRINTS_OFF, RankMaskCh);
        if ((OptParam == wrodtd) || (OptParam == rdodtd)) {
          MrcTatStretch (MrcData, Controller, Channel, OptParam, ChLastPass - Start, TRUE);
        }
      }

      MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Selected Offset for Mc %d Channel %d is = %d\n", Controller, Channel, ChLastPass);
    }

    if ((OptParam == ddwr2rd) || (OptParam == drwr2rd)) {
      for (Channel = 0; Channel < MaxChannel; Channel++) {
        if ((!MrcChannelExist (MrcData, Controller, Channel)) || IS_MC_SUB_CH (Lpddr, Channel)) {
          continue;
        }
        MrcGetSetMcCh (MrcData, Controller, Channel, GsmMctRDWRsg, WriteCached,   &RdWrSg[Controller][Channel]);
      }
    }
  }
  return Status;
}

/**
  Sets command margins when moving WrT, WrTBox, or WrV
  NOTE: ONLY one, ResetDDR or SelfRefresh can be set inside this function

  @param[in] MrcData         - Include all MRC global data.
  @param[in] McChBitMask     - Bit mask of populated controllers/channels
  @param[in] Ranks           - Bit Mask of populated ranks
  @param[in] Param           - Input parameter to update
  @param[in] Value0          - value to be added
  @param[in] Value1          - value to be added
  @param[in] ResetDDR        - Do we reset DDR?
  @param[in] SelfRefresh     - Do we perform Self refresh?

  @retval MrcStatus      - If it succeeds return mrcSuccess
**/
void
SetCmdMargin (
  IN MrcParameters *const MrcData,
  IN const UINT8          McChBitMask,
  IN const UINT8          Ranks,
  IN const UINT8          Param,
  IN const UINT8          Value0,
  IN const UINT8          Value1,
  IN UINT8                ResetDDR,
  IN const UINT8          SelfRefresh
  )
{
  MrcOutput         *Outputs;
  INT64             GetSetVal;
  UINT32            Controller;
  UINT32            Channel;
  UINT8             MaxChannel;
  UINT8             RankMaskCh;
  UINT8             Offset;

  Outputs       = &MrcData->Outputs;
  MaxChannel    = Outputs->MaxChannels;
  Offset        = 0;

  if (SelfRefresh && ResetDDR) {
    MRC_DEBUG_MSG (
      &Outputs->Debug,
      MSG_LEVEL_ERROR,
      "WARNING SelfRefresh OR ResetDDR can be set at once...performing SelfRefresh\n"
      );
    ResetDDR = 0;
  }

  if (SelfRefresh) {
    GetSetVal = 1;
    MrcGetSetMc (MrcData, MAX_CONTROLLER, GsmMccEnableRefresh, WriteNoCache, &GetSetVal);
  }

  // Walk though all mcs, chs and ranks
  for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
    for (Channel = 0; Channel < MaxChannel; Channel++) {
      if (McChBitMask & (1 << ((Controller * MaxChannel) + Channel))) {
        // determine which ranks from parameter "Ranks" exist in this channel
        RankMaskCh = Ranks & Outputs->Controller[Controller].Channel[Channel].ValidRankBitMask;
        if (RankMaskCh == 0) {
          continue; // No Ranks in this channel
        }
        switch (Param) {
          case WrTBox:
          case WrT:
            ShiftPIforCmdTraining (MrcData, Controller, Channel, MrcIterationClock, RankMaskCh, MRC_IGNORE_ARG_8, Value0, 0);
            if (Param != WrTBox) {
              break;
            }
            /*FALLTHROUGH*/
          // Fall through to WrV if Param is WrTBox.
          case WrV:
            Offset = (Param == WrTBox) ? ((2 * Value1) - 1) * 8 : Value0;
            ChangeMargin (MrcData, WrV, Offset, 0, 0, (UINT8) Controller, (UINT8) Channel, RankMaskCh, 0, 0, 0, 0);
            break;

          default:
            return;
        }
      }
    }
  }

  if (ResetDDR) {
    MrcResetSequence (MrcData);
  } else if (SelfRefresh) {
    MrcSelfRefreshState (MrcData, MRC_SR_EXIT);
  }

  return;
}

/**
  Updates the value for following OptParamCliff variables:
  drrd2rd=0, ddrd2rd=1, drwr2wr=2, ddwr2wr=3, drrd2wr=4, ddrd2wr=5, drwr2rd=6, ddwr2rd=7,
  rdodtd=8, wrodtd=9, mcodts=10, mcodtd=11, rtl=12}

  @param[in,out] MrcData    - Include all MRC global data.
  @param[in]     Controller - Controller to update the specified parameter.
  @param[in]     Channel    - Channel to update the specified parameter.
  @param[in]     Byte       - Byte to update the specified parameter.
  @param[in]     OptParam   - Parameter to update.
  @param[in]     Off        - Value to offset the current setting.
  @param[in]     UpdateHost - Switch to update the host structure with the new value.
  @param[in]     DebugPrint - Switch to enable debug prints.
  @param[in]     RankMask   - Bit mask of Ranks to update.

  @retval Nothing
**/
void
UpdateTAParamOffset (
  IN OUT MrcParameters *const MrcData,
  IN     const UINT32         Controller,
  IN     const UINT32         Channel,
  IN     const UINT8          Byte,
  IN     const UINT8          OptParam,
  IN     const INT8           Off,
  IN     const UINT8          UpdateHost,
  IN     const UINT8          DebugPrint,
  IN     const UINT8          RankMask
  )
{
  MrcDebug    *Debug;
  MrcOutput   *Outputs;
  INT64       GetSetVal;
  INT64       GetSetValSaved1[MAX_RANK_IN_CHANNEL];
  INT64       GetSetValSaved2[MAX_RANK_IN_CHANNEL];
  INT64       Roundtrip;
  INT64       RtlDelta;
  INT64       FifoDelta;
  INT64       FlybyDelta;
  INT64       CommonFlyby;
  INT64       RxFifoRdEnFlyby;
  UINT8       Rank;
  INT8        New;
  UINT8       GsmModeSingle;  //For parameters that only updates one setting
  UINT8       GsmModeMulti;   //For parameters that updates more than one setting
  UINT32      PrintMode;
  BOOLEAN     TatUpdate;

  Outputs       = &MrcData->Outputs;
  Debug         = &Outputs->Debug;
  GsmModeMulti  = ForceWriteCached;
  GsmModeSingle = (UpdateHost) ? ForceWriteCached : ForceWriteUncached;
  GetSetVal     = Off;
  TatUpdate     = FALSE;
  PrintMode     = 0;
  if (DebugPrint) {
    GsmModeSingle |= PrintValue;
    GsmModeMulti  |= PrintValue;
  }

  switch (OptParam) {
    case drrd2rd:
      // Different Rank RD 2 RD Turn Around offsets
      MrcGetSetMcCh (MrcData, Controller, Channel, GsmMctRDRDdr, GsmModeSingle, &GetSetVal);
      TatUpdate = TRUE;
      break;

    case ddrd2rd:
      // Different DIMM RD 2 RD Turn Around offsets
      MrcGetSetMcCh (MrcData, Controller, Channel, GsmMctRDRDdd, GsmModeSingle, &GetSetVal);
      TatUpdate = TRUE;
      break;

    case drwr2wr:
      // Different Rank WR 2 WR Turn Around offsets
      MrcGetSetMcCh (MrcData, Controller, Channel, GsmMctWRWRdr, GsmModeSingle, &GetSetVal);
      TatUpdate = TRUE;
      break;

    case ddwr2wr:
      // Different DIMM WR 2 WR Turn Around offsets
      MrcGetSetMcCh (MrcData, Controller, Channel, GsmMctWRWRdd, GsmModeSingle, &GetSetVal);
      TatUpdate = TRUE;
      break;

    case drrd2wr:
      // Different Rank RD 2 WR Turn Around offsets
      MrcGetSetMcCh (MrcData, Controller, Channel, GsmMctRDWRdr, GsmModeSingle, &GetSetVal);
      TatUpdate = TRUE;
      break;

    case ddrd2wr:
      // Different DIMM RD 2 WR Turn Around offsets
      MrcGetSetMcCh (MrcData, Controller, Channel, GsmMctRDWRdd, GsmModeSingle, &GetSetVal);
      TatUpdate = TRUE;
      break;

    case drwr2rd:
      // Different Rank WR 2 RD Turn Around offsets
      MrcGetSetMcCh (MrcData, Controller, Channel, GsmMctWRRDdr, GsmModeSingle, &GetSetVal);
      TatUpdate = TRUE;
      break;

    case ddwr2rd:
      // Different DIMM WR 2 RD Turn Around offsets
      MrcGetSetMcCh (MrcData, Controller, Channel, GsmMctWRRDdd, GsmModeSingle, &GetSetVal);
      TatUpdate = TRUE;
      break;

    case rdodtd:
      // Convert into Register values. 2'b00 = BL/2 + 2 (For Enhanced Channels 10 DCLKs, otherwise 6 DCLKs)
      // @todo: <CNL> Enhance Channel switch.
      GetSetVal  = Off - 6;
      MrcGetSetMcCh (MrcData, Controller, Channel, GsmMctOdtRdDuration, GsmModeSingle, &GetSetVal);
      break;

/* // Do not seem like this is being used
    case wrodtd:
      // Convert into Register values. 2'b00 = BL/2 + 2 (For Enhanced Channels 10 DCLKs, otherwise 6 DCLKs)
      // @todo: <CNL> Enhance Channel switch.
      GetSetVal  = Off - 6;
      MrcGetSetDdrIoGroupChannel (MrcData, Channel, GsmMctOdtWrDuration, GsmModeSingle, &GetSetVal);
      break;
*/
    case mcodts:
      // MC ODT delay
      if (!UpdateHost) {
        MrcGetSetChStrb (MrcData, Controller, Channel, Byte, SenseAmpDelay, ReadFromCache, &GetSetValSaved2[0]);
      }
      MrcGetSetChStrb (MrcData, Controller, Channel, Byte, McOdtDelay, ReadFromCache, &GetSetValSaved1[0]);
      New                   = (INT8) GetSetValSaved1[0] + Off;
      if (New < -4) {
        New = -4; // RcvEnPi[8:6] - 5 qclk Min
      } else if (New > 6) {
        New = 6; // RcvEnPi[8:6] + 5 qclk Max
      }

      GetSetVal             = New;
      MrcGetSetChStrb (MrcData, Controller, Channel, Byte, McOdtDelay, GsmModeMulti, &GetSetVal);
      MrcGetSetChStrb (MrcData, Controller, Channel, Byte, SenseAmpDelay, GsmModeMulti, &GetSetVal);
      if (!UpdateHost) {
        MrcGetSetChStrb (MrcData, Controller, Channel, Byte, McOdtDelay, WriteToCache, &GetSetValSaved1[0]);
        MrcGetSetChStrb (MrcData, Controller, Channel, Byte, SenseAmpDelay, WriteToCache, &GetSetValSaved2[0]);
      }

      MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, (DebugPrint) ? "%d\t" : "", New);
      break;

    case mcodtd:
      // Duration
      if (!UpdateHost) {
        MrcGetSetChStrb (MrcData, Controller, Channel, Byte, SenseAmpDuration, ReadFromCache, &GetSetValSaved2[0]);
      }
      MrcGetSetChStrb (MrcData, Controller, Channel, Byte, McOdtDuration, ReadFromCache, &GetSetValSaved1[0]);
      GetSetVal = GetSetValSaved1[0] + Off;

      MrcGetSetChStrb (MrcData, Controller, Channel, Byte, McOdtDuration, GsmModeMulti, &GetSetVal);
      MrcGetSetChStrb (MrcData, Controller, Channel, Byte, SenseAmpDuration, GsmModeMulti, &GetSetVal);
      if (!UpdateHost) {
        MrcGetSetChStrb (MrcData, Controller, Channel, Byte, McOdtDuration, WriteToCache, &GetSetValSaved1[0]);
        MrcGetSetChStrb (MrcData, Controller, Channel, Byte, SenseAmpDuration, WriteToCache, &GetSetValSaved2[0]);
      }
      break;

    case rtl:
      // Note: UpdateHost argument is ignored for "rtl" parameter - we always keep CR cache updated to the HW for the below parameters.
      Roundtrip = 0;
      CommonFlyby = MCMISCS_RXDQFIFORDENCH01_rxdqfifordenrank0chadel_MAX;
      for (Rank = 0; Rank < MAX_RANK_IN_CHANNEL; Rank++) {
        if (RankMask & (1 << Rank)) {
          // When we change Roundtrip, we can compensate with the following:
          // - RxFifoRdEnTclDelay:   READCFGCH[0..7].tCL4RxDqFifoRdEn (tCK)                                      - per Phy Ch
          // - RxFifoRdEnFlybyDelay: RXDQFIFORDENCH[01..67].RxDqFifoRdEnRank0ChADel (G1: UI, G2: tCK, G4: 2tCK)  - per Phy Ch / Rank
          // RoundTripDelay is in QCLK (all gears)
          MrcGetSetMcChRnk (MrcData, Controller, Channel, Rank, RoundTripDelay, ReadFromCache | PrintMode, &Roundtrip);
          RtlDelta = Off - Roundtrip;
          if (Outputs->Gear2) {
            FifoDelta = RtlDelta;
            FlybyDelta = 0;
          } else if (Outputs->Gear4) {
            FifoDelta = 2 * (INT32) RtlDelta;
            FlybyDelta = 0;
          } else { // Gear1
            FifoDelta = (INT32) RtlDelta / 2;
            FlybyDelta = (UINT32) RtlDelta & 1;
            if ((FlybyDelta == 1) && (RtlDelta < 0)) {
              FifoDelta -= 1;
            }
          }
          //MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\n--> [Mc:%d R%d C%d] RtlDelta: %lld, FifoDelta: %lld, FlyByDelta: %lld\n", Controller, Rank, Channel, RtlDelta, FifoDelta, FlybyDelta);
          MrcGetSetMcChRnk (MrcData, Controller, Channel, Rank, RoundTripDelay,       WriteOffsetCached | PrintMode, &RtlDelta);
          MrcGetSetMcChRnk (MrcData, Controller, Channel, Rank, RxFifoRdEnFlybyDelay, WriteOffsetCached | PrintMode, &FlybyDelta);
          MrcGetSetMcCh    (MrcData, Controller, Channel,       RxFifoRdEnTclDelay,   WriteOffsetCached | PrintMode, &FifoDelta);

          MrcGetSetMcChRnk (MrcData, Controller, Channel, Rank, RxFifoRdEnFlybyDelay, ReadFromCache | PrintMode, &RxFifoRdEnFlyby);
          CommonFlyby = MIN (CommonFlyby, RxFifoRdEnFlyby);
        }
      } // for Rank
      // Move the common part of RxFifoFlyby (Subch/Rank) into RxFifo (Subch)
      // During RTL sweep we only use RxFifoFlyby in Gear1; it is not used in Gear2/Gear4
      if ((CommonFlyby > 1) && !Outputs->Gear2 && !Outputs->Gear4) {
        FifoDelta = (INT32) CommonFlyby / 2;
        FlybyDelta = -((INT32) FifoDelta * 2);
        // MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "CommonFlyby: %lld, FifoDelta: %lld, FlyByDelta: %lld\n", CommonFlyby, FifoDelta, FlybyDelta);
        for (Rank = 0; Rank < MAX_RANK_IN_CHANNEL; Rank++) {
          if (RankMask & (1 << Rank)) {
            MrcGetSetMcChRnk (MrcData, Controller, Channel, Rank, RxFifoRdEnFlybyDelay, WriteOffsetCached | PrintMode, &FlybyDelta);
          }
        }
        MrcGetSetMcCh (MrcData, Controller, Channel, RxFifoRdEnTclDelay,   WriteOffsetCached | PrintMode, &FifoDelta);
      }
      break;

    default:
      break;
  }

  MRC_DEBUG_MSG (
    Debug,
    MSG_LEVEL_NOTE,
    ((OptParam != mcodtd) && (OptParam != mcodts) && DebugPrint) ? "%d\t" : "",
    Off
    );

  if (TatUpdate) {
    // Must update the XARB bubble injector when TAT values change
    SetTcBubbleInjector (MrcData, Controller, Channel);
  }
  return;
}

/**
  This function applies an offset to the global compensation logic.
  Reruns Compensation and returns the new comp value

  @param[in,out] MrcData         - Include all MRC global data.
  @param[in]     param           - Parameter defining the desired global compensation logic
  @param[in]     offset          - Value to apply
  @param[in]     AdjustOdtStatic  - Decides if Static ODT will be adjusted for ReadODT param
  @param[in]     UpdateHost      - Decides if MrcData has to be updated
  @param[out]    NewComp         - Pointer to new comp return value

  @retval mrcSuccess Successfully computed new compensation value
**/
extern
MrcStatus
UpdateCompGlobalOffset (
  IN OUT MrcParameters *const MrcData,
  IN     const UINT8          param,
  IN     const INT32          offset,
  IN     const BOOLEAN        AdjustOdtStatic,
  IN     const BOOLEAN        UpdateHost,
  OUT          UINT32  *const NewComp
  )
{
  const MrcInput *Inputs;
  MrcOutput *Outputs;
  MrcDebug  *Debug;
  INT64     RcompOdt;
  INT64     GetSetVal = 0;
  INT64     Min;
  INT64     Max;
//BOOLEAN   EnDqsOdtParkMode;
  BOOLEAN   StaticLegChange;
  UINT32    StaticLegEn;
  GSM_GT    RdOdtGroup;
  GSM_GT    Group;
  UINT32    GsModeFlags;
  UINT32    FirstController;
  UINT32    FirstChannel;
                                       // CompGlobalOffsetParam:   RdOdtUp         RdOdtDn         WrDSUp       WrDSDn       WrDSCmdUp      WrDSCmdDn      WrDSCtlUp      WrDSCtlDn      WrDSClkUp      WrDSClkDn      SCompDq      SCompCmd      SCompCtl      SCompClk      RxLoad      DisOdtStatic
  static const GSM_GT    GetSetWrite[MaxCompGlobalOffsetParam] = { DqOdtVrefUp,    DqOdtVrefDn,    DqDrvVrefUp, DqDrvVrefDn, CmdDrvVrefUp,  CmdDrvVrefDn,  CtlDrvVrefUp,  CtlDrvVrefDn,  ClkDrvVrefUp,  ClkDrvVrefDn,  GsmGtDelim,  GsmGtDelim,   GsmGtDelim,   GsmGtDelim,   GsmGtDelim, GsmGtDelim };
  static const GSM_GT    GetSetRead[MaxCompGlobalOffsetParam]  = { CompRcompOdtUp, CompRcompOdtDn, TxRonUp,     TxRonDn,     WrDSCodeUpCmd, WrDSCodeDnCmd, WrDSCodeUpCtl, WrDSCodeDnCtl, WrDSCodeUpClk, WrDSCodeDnClk, SCompCodeDq, SCompCodeCmd, SCompCodeCtl, SCompCodeClk, GsmGtDelim, /* @todo_adl RloadDqsUp,*/ CompRcompOdtUp};
  DDRPHY_COMP_NEW_CR_VCCDLLCOMPDATACCC1_STRUCT    VccDllCompDataCcc1;

  Inputs = &MrcData->Inputs;
  Outputs = &MrcData->Outputs;
  Debug   = &Outputs->Debug;

  MrcGetSetLimits(MrcData, DqOdtVrefUp, &Min, &Max, NULL);

  if (NULL == NewComp) {
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_ERROR, "%s: NewComp argument\n", gNullPtrErrStr);
    return mrcWrongInputParameter;
  }

  if (param > (UINT8) (MaxCompGlobalOffsetParam - 1)) {
    // param is out of bounds.
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_ERROR, "%s: param %u > %d\n", gWrongInputParam, param, (MaxCompGlobalOffsetParam - 1));
    return mrcWrongInputParameter;
  }

  GsModeFlags = (UpdateHost) ? ForceWriteCached : ForceWriteUncached;
  if (MRC_PWR_DBG_PRINT) {
    GsModeFlags |= PrintValue;
  }
  // Update offset in local CR variable
  if (GetSetWrite[param] != GsmGtDelim) {
    GetSetVal = offset;
    MrcGetSetNoScope (MrcData, GetSetWrite[param], GsModeFlags, &GetSetVal);
  }
  switch (param) {
    case SCompDq:
      // Apply Comp Offset to Scomp-DQ
      GetSetVal = offset & 0xF;
      MrcGetSetNoScope (MrcData, TxSlewRate, GsModeFlags, &GetSetVal);
      GetSetVal = (offset >> SCOMP_PC_STORAGE_BIT_OFFSET) & 0x1; // The value is stored in bit 4 of the value passed in, because bits 0-3 are now used to store the Scomp value
      MrcGetSetNoScope (MrcData, DqScompPC, GsModeFlags, &GetSetVal);
      break;

    case SCompCmd:
      // Apply Comp Offset to Scomp-CMD/CTL/CLK
      GetSetVal = offset & 0xF;
      MrcGetSetNoScope (MrcData, CmdSlewRate, GsModeFlags, &GetSetVal);
      GetSetVal = (offset >> SCOMP_PC_STORAGE_BIT_OFFSET) & 0x1; // The value is stored in bit 4 of the value passed in, because bits 0-3 are now used to store the Scomp value
      MrcGetSetNoScope (MrcData, CmdScompPC, GsModeFlags, &GetSetVal);
      break;

    case SCompCtl:
      // Apply Comp Offset to Scomp-CMD/CTL/CLK
      GetSetVal = offset & 0xF;
      MrcGetSetNoScope(MrcData, CtlSlewRate, GsModeFlags, &GetSetVal);
      GetSetVal = (offset >> SCOMP_PC_STORAGE_BIT_OFFSET) & 0x1; // The value is stored in bit 4 of the value passed in, because bits 0-3 are now used to store the Scomp value
      MrcGetSetNoScope(MrcData, CtlScompPC, GsModeFlags, &GetSetVal);
      break;

    case SCompClk:
      // Apply Comp Offset to Scomp-CMD/CTL/CLK
      GetSetVal = offset & 0xF;
      MrcGetSetNoScope(MrcData, ClkSlewRate, GsModeFlags, &GetSetVal);
      GetSetVal = (offset >> SCOMP_PC_STORAGE_BIT_OFFSET) & 0x1; // The value is stored in bit 4 of the value passed in, because bits 0-3 are now used to store the Scomp value
      MrcGetSetNoScope(MrcData, ClkScompPC, GsModeFlags, &GetSetVal);
      break;

    case RxLoad:
      // Apply Comp Offset to RxLoad
      GetSetVal = offset;
      // @todo_adl MrcGetSetNoScope (MrcData, RxLoadCompVref, GsModeFlags, &GetSetVal);
      break;

    default:
      break;
  }
  // Run Compensation
  // Start Comp Engine
  ForceRcomp (MrcData);
  FirstController = (MrcControllerExist(MrcData, cCONTROLLER0)) ? 0 : 1;
  FirstChannel = Outputs->Controller[FirstController].FirstPopCh;
  MrcGetSetChStrb (MrcData, FirstController, FirstChannel, 0, GsmIocDataDqsOdtParkMode, ReadFromCache, &GetSetVal);
//EnDqsOdtParkMode = (GetSetVal > 0);

  RdOdtGroup = (Outputs->OdtMode == MrcOdtModeVss) ? CompRcompOdtDn : CompRcompOdtUp; // In Vss mode only odt down is valid

  if (((param == RdOdtUp) || (param == RdOdtDn)) && AdjustOdtStatic && !(Inputs->A0)) { // && (!EnDqsOdtParkMode) We set the RdOdtDn after the RdOdtUp, and we want to adjust this once everything has been set.
    VccDllCompDataCcc1.Data = MrcReadCR (MrcData, DDRPHY_COMP_NEW_CR_VCCDLLCOMPDATACCC1_REG);
    // Check if we are close to saturation in either direction
    MrcGetSetNoScope (MrcData, RdOdtGroup, ReadUncached, &RcompOdt);
    MRC_POWER_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "AjustOdtStatic: initial CompRcompOdtUp/Dn: %d\n", (INT32) RcompOdt);
    StaticLegChange = FALSE;
    StaticLegEn     = 0;
    if (RcompOdt > 48) { // Try Enable static ODT legs.
      // Enable static read Odt legs
      // relative + external
      StaticLegChange = TRUE;
      StaticLegEn     = 1;
    } else if (RcompOdt < 16) { // Try disable static read Odt legs
      StaticLegChange = TRUE;
      StaticLegEn     = 0;
    }
    if (StaticLegChange) {
      VccDllCompDataCcc1.Bits.rcomp_cmn_odtdnrelpupstatlegen  = StaticLegEn;
      VccDllCompDataCcc1.Bits.rcomp_cmn_odtdnrelpdnstatlegen  = StaticLegEn;
      VccDllCompDataCcc1.Bits.rcomp_cmn_odtupextstatlegen     = StaticLegEn;
      MrcWriteCR (MrcData, DDRPHY_COMP_NEW_CR_VCCDLLCOMPDATACCC1_REG, VccDllCompDataCcc1.Data);

      GetSetVal = StaticLegEn;
      MrcGetSetChStrb (MrcData, MAX_CONTROLLER, MAX_CHANNEL, MAX_SDRAM_IN_DIMM, GsmIocDataOdtStaticEn,   WriteCached, &GetSetVal);
      MrcGetSetChStrb (MrcData, MAX_CONTROLLER, MAX_CHANNEL, MAX_SDRAM_IN_DIMM, GsmIocStrobeOdtStaticEn, WriteCached, &GetSetVal);
      ForceRcomp (MrcData);
      MrcGetSetNoScope (MrcData, RdOdtGroup, ReadUncached, &RcompOdt);
      MRC_POWER_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "AjustOdtStatic: StaticLegEn: %d, New CompRcompOdtUp/Dn: %d\n", StaticLegEn, (INT32) RcompOdt);
    }
  }

  // Return the new comp code (hardware output)
  Group = GsmGtMax;
  if ((param == RdOdtUp) || (param == RdOdtDn)) {
    Group = RdOdtGroup;
  } else if (GetSetRead[param] != GsmGtDelim) {
    Group = GetSetRead[param];
  }
  if (Group != GsmGtMax) {
    MrcGetSetNoScope (MrcData, Group, ReadUncached, &GetSetVal);
  }

  *NewComp = (UINT32) GetSetVal;
  return mrcSuccess;
}

/**
  Programs Delay/Duration for the SenseAmp and MCODT based on RcvEn timing
  Provide GuardBand > 0 if needed to be more conservative in timing
  Main goal is to optimize power

  @param[in,out] MrcData   - Include all MRC global data.
  @param[in]     GuardBand - QCLK guardband on Duration.

  @retval Nothing
**/
void
UpdateSampOdtTiming (
  IN OUT MrcParameters *const MrcData,
  IN     const UINT8          GuardBand
  )
{
  MrcOutput *Outputs;
  MrcDebug  *Debug;
  MrcSaveData *SaveOutputs;
  MrcDdrType  DdrType;
  INT64       Min;
  INT64       Max;
  INT64       GetSetOn;   // SenseAmp delay
  INT64       GetSetOff;  // SenseAmp / ODT duration
  INT64       DqOdtDelayVal;
  INT64       DqsOdtDelayVal;
  INT64       DqsOdtDurationVal;
  INT64       GetSetVal;
  INT32       On;         // SenseAmp / ODT delay
  INT32       Off;        // SenseAmp / ODT duration
  UINT32      Channel;
  UINT32      Controller;
  UINT32      Byte;
  UINT32      Rank;
  UINT32      Strobe2ClkDriftPs;
  UINT32      WakeUpQclk;
  UINT32      QclkPs;
  INT16       MaxRcvEn;
  INT16       MinRcvEn;
  INT16       CurRcvEn;
  INT16       QclkPi;
  BOOLEAN     Gear2;
  BOOLEAN     Gear4;
  BOOLEAN     Lpddr5;
  BOOLEAN     Lpddr4;
  BOOLEAN     Ddr5;
  BOOLEAN     Ddr4;
  UINT32      PrintMode;
  const MrcInput *Inputs;

  Outputs   = &MrcData->Outputs;
  Debug     = &Outputs->Debug;
  DdrType   = Outputs->DdrType;
  Gear2     = Outputs->Gear2;
  Gear4     = Outputs->Gear4;
  SaveOutputs = &MrcData->Save.Data;
  Lpddr5    = (DdrType == MRC_DDR_TYPE_LPDDR5);
  Lpddr4    = (DdrType == MRC_DDR_TYPE_LPDDR4);
  Ddr5      = (DdrType == MRC_DDR_TYPE_DDR5);
  Ddr4      = (DdrType == MRC_DDR_TYPE_DDR4);
  PrintMode = PrintValue;
  Inputs    = &MrcData->Inputs;

  if (Lpddr5) {
    // tWCK2DQO is drift of DRAM read data to our WCK.
    Strobe2ClkDriftPs = 1600; //ps
    QclkPs = Outputs->Wckps / ((Gear2) ? 1 : 2);
  } else {
    QclkPs = Outputs->Qclkps;
    if (Lpddr4) {
      Strobe2ClkDriftPs = 1200; //ps
    } else if (Ddr5) {
      Strobe2ClkDriftPs = Ddr5GetTdqscki (MrcData);
    } else {
      Strobe2ClkDriftPs = 0;
    }
  }

  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "UpdateSampOdtTiming: GuardBand = %d\n", GuardBand);
  MrcGetSetLimits (MrcData, RecEnDelay, &Min, &Max, NULL);

  for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
    for (Channel = 0; Channel < MAX_CHANNEL; Channel++) {
      if (MrcChannelExist (MrcData, Controller, Channel)) {
        for (Byte = 0; Byte < Outputs->SdramCount; Byte++) {
          MaxRcvEn  = 0;
          MinRcvEn  = (UINT16) Max;

          for (Rank = 0; Rank < MAX_RANK_IN_CHANNEL; Rank++) {
            if (MrcRankExist (MrcData, Controller, Channel, Rank)) {
              MrcGetSetStrobe (MrcData, Controller, Channel, Rank, Byte, RecEnDelay, ReadFromCache, &GetSetVal);
              CurRcvEn = (INT16) GetSetVal;
              if (MaxRcvEn < CurRcvEn) {
                MaxRcvEn = CurRcvEn;
              }

              if (MinRcvEn > CurRcvEn) {
                MinRcvEn = CurRcvEn;
              }
            }
          }
          // Round Max to nearest cycle
          QclkPi = Gear4 ? 256 : (Gear2 ? 128 : 64);
          MaxRcvEn = DIVIDECEIL (MaxRcvEn, QclkPi);
          MinRcvEn = MinRcvEn >> (6 + (Gear4 ? 2 : (Gear2 ? 1 : 0)));

          // SENSE AMP CAN ONLY BE ON WHEN ODT IS ON FOR EOS REASONS.
          // Ideal: On = 4 - MAX (WakeUpNs, 2Qclk) - TdqsckQclk + MinRcvEnQclk
          //WakeUpQclk = (((QclkPi * MRC_SENSE_AMP_WAKEUP_TIME) + TdqsckPs) / Outputs->Qclkps); // Convert to PI codes
          WakeUpQclk = MRC_SENSE_AMP_WAKEUP_TIME + Strobe2ClkDriftPs;
          WakeUpQclk = DIVIDECEIL (WakeUpQclk, QclkPs); // Convert to PI codes
          WakeUpQclk = MAX (WakeUpQclk, 2);
          On = (Gear4 ? 2 : 4) - WakeUpQclk + MinRcvEn;
          On = MAX (On, 0);

          // Turn Off Samp/ODT 1 qclk after postamble
          Off = (MaxRcvEn - MinRcvEn) + WakeUpQclk + GuardBand + (Gear4 ? 1 : (Gear2 ? 2 : 5));
          if (Ddr4) {
            Off += 1;
          } else if (Ddr5) {
            Off += SaveOutputs->ReadDqsOffsetDdr5;
          }

          GetSetOn = On;                 // senseamp still remain in QCLK for all gears
          Off = Gear4 ? (Off * 2) : Off; // Gear4 duration will be in tCK (conversion done in logic) // once HAL max eqn for rcven changes for Gear4 this needs to be reverted.

          GetSetOff = RANGE (Off, 0, DATA0CH0_CR_DDRCRDATACONTROL1_dqodtduration_MAX);  // DqOdtDuration and SenseAmpDuration have the same MAX value

          if (((Inputs->A0) && !Ddr5) || ((Ddr4) && (Inputs->SBGACpu))) {
            //reduce the ADL-S BGA DDR4 ODT delay by 2
            MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "OdtDelay before reduction %d\n", On);
            On -= 2;                  // DQ and DQS ODT needs more delay on A0
            On = MAX (On, 0);         // Make sure we are not going negative
            MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "OdtDelay after reduction %d\n", On);
          }

          DqOdtDelayVal = Gear4 ? (On * 2) : On;

          DqsOdtDelayVal    = On * 2; // DQS ODT Delay is in half QCLK granularity
          DqsOdtDurationVal = RANGE (Off, 0, DATA0CH0_CR_DDRCRDATACONTROL1_dqsodtduration_MAX);

          MrcGetSetChStrb (MrcData, Controller, Channel, Byte, McOdtDelay,       WriteToCache | PrintMode, &DqOdtDelayVal);
          MrcGetSetChStrb (MrcData, Controller, Channel, Byte, McOdtDuration,    WriteToCache | PrintMode, &GetSetOff);
          MrcGetSetChStrb (MrcData, Controller, Channel, Byte, DqsOdtDelay,      WriteToCache | PrintMode, &DqsOdtDelayVal);
          MrcGetSetChStrb (MrcData, Controller, Channel, Byte, DqsOdtDuration,   WriteToCache | PrintMode, &DqsOdtDurationVal);
          MrcGetSetChStrb (MrcData, Controller, Channel, Byte, SenseAmpDelay,    WriteToCache | PrintMode, &GetSetOn);
          if (Outputs->Lpddr && Gear2) {
            GetSetOff += 2;
          }
          MrcGetSetChStrb (MrcData, Controller, Channel, Byte, SenseAmpDuration, WriteToCache | PrintMode, &GetSetOff);
        }
      }
    }
  }
  MrcFlushRegisterCachedData (MrcData);

  return;
}

/**
  Turns off unused portions of the secondary DLL to save power

  @param[in,out] MrcData - Include all MRC global data.

  @retval Nothing
**/
void
UpdateSlaveDLLLength (
  IN OUT MrcParameters *const MrcData
  )
{/* @todo_adl
  MrcOutput           *Outputs;
  UINT8             Controller;
  UINT8             Channel;
  UINT8             MaxChannel;
  UINT8             byte;
  UINT8             rank;
  UINT8             MaxPi;
  INT64             GetSetVal;
  UINT32            Ratio;
  UINT32            TempVar1;
  UINT32            AvgSdllSegmentDisable;
  UINT32            SdllSegDisable;
  UINT32            Average;
  UINT32            Count;
  UINT32            Offset;
  UINT32            VccDllBlock;
  BOOLEAN           BreakOut;
  DLLDDRDATA0_CR_DDRCRVCCDLLFFCONTROL_STRUCT         VccDllFFControl;

  Outputs  = &MrcData->Outputs;
  MaxChannel = Outputs->MaxChannels;
  Ratio    = 0;
  Average  = 0;
  Count    = 0;
  SdllSegDisable = 0;
  BreakOut = FALSE;

  for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
    for (Channel = 0; Channel < MaxChannel; Channel++) {
      if (MrcChannelExist (MrcData, Controller, Channel)) {
        for (byte = 0; byte < Outputs->SdramCount; byte++) {
          MaxPi = 0;
          for (rank = 0; rank < MAX_RANK_IN_CHANNEL; rank++) {
            if (MrcRankExist (MrcData, Controller, Channel, rank)) {
              MrcGetSetStrobe (MrcData, Controller, Channel, rank, byte, RxDqsPDelay, ReadFromCache, &GetSetVal);
              if (MaxPi < GetSetVal) {
                MaxPi = (UINT8) GetSetVal;
              }

              MrcGetSetStrobe (MrcData, Controller, Channel, rank, byte, RxDqsNDelay, ReadFromCache, &GetSetVal);
              if (MaxPi < GetSetVal) {
                MaxPi = (UINT8) GetSetVal;
              }
            }
          }
          // Update SlaveDLL Length for power Savings
          // Calculate which segments to turn off:
          // NEW (OFF: 0, PI<48: 0x2, PI<32: 0x4, PI<16: 0x6)
          // results are:   0, 2 , 4 or 6
          GetSetVal = ((7 - (MaxPi >> 3)) & ~MRC_BIT0);
          MrcGetSetChStrb (MrcData, Controller, Channel, byte, GsmIocSdllSegmentDisable, WriteCached, &GetSetVal);
          SdllSegDisable = (UINT32) GetSetVal;
          Average = Average + (RANGE (SdllSegDisable, 0, 10));
          Count++;
        }
      }
    }
  }//controller

  // Read the first populated VccDllFFControl register
  // @todo : Review this for DDR4 2DPC and DDR5
  for (Controller = 0; (Controller < MAX_CONTROLLER) && (BreakOut == FALSE); Controller++) {
    for (Channel = 0; (Channel < MaxChannel) && (BreakOut == FALSE); Channel++) {
      if (MrcChannelExist (MrcData, Controller, Channel)) {
        VccDllBlock = ((MAX_CCC_PER_CHANNEL * Controller) + Channel);
        Offset = OFFSET_CALC_CH (DLLDDRDATA0_CR_DDRCRVCCDLLFFCONTROL_REG, DLLDDRDATA1_CR_DDRCRVCCDLLFFCONTROL_REG, VccDllBlock) ;
        VccDllFFControl.Data = MrcReadCR (MrcData, Offset);
        BreakOut = TRUE;
      }
    }
  } //controller
  if (Count == 0) {
    Count = 1;
    MRC_DEBUG_MSG (&Outputs->Debug, MSG_LEVEL_WARNING, "SdllSegDisable for each byte is 0 and is forced to be 1 to avoid divide by zero error. Possibly unpopulated channel\n", Count);
  }
  AvgSdllSegmentDisable = Average / Count;

  TempVar1 = 183 / 8;
  Ratio = (((10 - AvgSdllSegmentDisable) * 170) +  TempVar1* 12) + 670;
  //Ratio = DIVIDEROUND (TempVar2, 1000); // Dont divide, instead multiply the RHS by 1000
  TempVar1 = ((Ratio > 900) && (Ratio <= 1000)) ? 0 : ((Ratio > 800) && (Ratio <= 900)) ? 1 : ((Ratio > 700) && (Ratio <= 800)) ? 2 : 3;
  VccDllFFControl.Bits.VccdllReadPwrSave = (4 * (!(Outputs->Gear2))) + TempVar1;
  MrcWriteCrMulticast (MrcData, DLLDDR_CR_DDRCRVCCDLLFFCONTROL_REG, VccDllFFControl.Data);
  */
  return;
}

/**
  This function Shifts the CMD timing.
  NOTE: ONLY one, ResetDDR or SelfRefresh can be set inside this function

  @param[in,out] MrcData     - Include all MRC global data.
  @param[in]     Ranks       - Parameter defining the desired global compensation logic
  @param[in]     offset      - per channel Value to shift picode for
  @param[in]     ResetDDR    - Do we reset DDR?
  @param[in]     SelfRefresh - Do we perform Self refresh?
  @param[in]     UpdateHost  - Determines if MrcData has to be updated

  @retval MrcStatus       - If it succeeds return mrcSuccess
**/
MrcStatus
ShiftCh2Ch (
  IN OUT MrcParameters *const MrcData,
  IN     const UINT8          Ranks,
  IN     UINT8                offset[MAX_CONTROLLER][MAX_CHANNEL],
  IN     UINT8                ResetDDR,
  IN     const UINT8          SelfRefresh,
  IN     const UINT8          UpdateHost
  )
{
  MrcDebug          *Debug;
  MrcOutput         *Outputs;
  MrcIntOutput      *IntOutputs;
  MrcIntCmdTimingOut  *IntCmdTiming;
  MrcStatus         Status;
  INT64             GetSetVal;
  UINT32            Controller;
  UINT32            Channel;
  UINT8             Rank;
  UINT8             RankMaskCh;
  UINT8             CmdGroup;
  INT32             NewValue;
  INT32             Offset;

  Status        = mrcSuccess;
  Outputs       = &MrcData->Outputs;
  Debug         = &Outputs->Debug;
  IntOutputs    = (MrcIntOutput *) (MrcData->IntOutputs.Internal);

  if (SelfRefresh && ResetDDR) {
    MRC_DEBUG_MSG (
      Debug,
      MSG_LEVEL_ERROR,
      "WARNING SelfRefresh OR ResetDDR can be set at once...performing SelfRefresh\n"
      );
    ResetDDR = 0;
  }

  if (SelfRefresh) {
    GetSetVal = 1;
    MrcGetSetMc (MrcData, MAX_CONTROLLER, GsmMccEnableRefresh, WriteNoCache, &GetSetVal);
  }

  if (Outputs->DdrType != MRC_DDR_TYPE_LPDDR5) {
    MrcBlockTrainResetToggle (MrcData, FALSE);
  }

  for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
    for (Channel = 0; Channel < Outputs->MaxChannels; Channel++) {
      if (!MrcChannelExist (MrcData, Controller, Channel)) {
        continue;
      }

      IntCmdTiming = &IntOutputs->Controller[Controller].CmdTiming[Channel];
      RankMaskCh = Ranks & Outputs->Controller[Controller].Channel[Channel].ValidRankBitMask;

      if (RankMaskCh == 0) {
        continue;
      }

      Offset = offset[Controller][Channel];

      // Shift CLK (this will shift DQ PIs as well)
      ShiftPIforCmdTraining (MrcData, Controller, Channel, MrcIterationClock, RankMaskCh, MRC_IGNORE_ARG_8, Offset, UpdateHost);

      // Shift WCK for LP5
      if (Outputs->DdrType == MRC_DDR_TYPE_LPDDR5) {
        GetSetVal = (INT64) (IntCmdTiming->WckPiCode + Offset);
        MrcGetSetCcc (MrcData, Controller, Channel, MRC_IGNORE_ARG, 0, WckGrpPi, ForceWriteCached, &GetSetVal);
      }

      // Shift CTL
      NewValue = 0;
      for (Rank = 0; Rank < MAX_RANK_IN_CHANNEL; Rank++) {
        if (RankMaskCh & (1 << Rank)) {
          NewValue = (INT32) (IntCmdTiming->CtlPiCode[Rank]) + Offset;
          break;
        }
      }

      ShiftPIforCmdTraining (MrcData, Controller, Channel, MrcIterationCtl, RankMaskCh, MRC_IGNORE_ARG_8, NewValue, UpdateHost);

      // Shift Cmd
      for (CmdGroup = 0; CmdGroup < MAX_COMMAND_GROUPS; CmdGroup++) {
        ShiftPIforCmdTraining (
          MrcData,
          Controller,
          Channel,
          MrcIterationCmd,
          MRC_IGNORE_ARG_8,
          1 << CmdGroup,
          (INT32) IntCmdTiming->CmdPiCode[CmdGroup] + Offset,
          UpdateHost
        );
      }
    } // for Channel
  } // for Controller
  IoReset (MrcData);

  // Reset DDR is required
  if (ResetDDR) {
    Status = MrcResetSequence (MrcData);
  } else if (SelfRefresh) {
    Status = MrcSelfRefreshState (MrcData, MRC_SR_EXIT);
  }
  if (Outputs->DdrType != MRC_DDR_TYPE_LPDDR5) {
    MrcBlockTrainResetToggle (MrcData, TRUE);
  }

  return Status;
}

/**
  Returns the index into the array OptResult in the MrcOutput structure.

  @param[in] OptParam - Margin parameter

  @retval One of the following values: RdSAmpOfft(0), WrDSOfft (1), RxEqOfft(2), TxEqOfft (3), RdOdtOfft(4)
**/
UINT8
GetOptResultType (
  IN UINT8 OptParam
  )
{
  switch (OptParam) {
  case OptRxBias:
  case OptRxBiasCb:
    return RdSAmpOfft;

  case OptWrDS:
  case OptTxEqWrDS:
    return WrDSOfft;

  case OptRxEq:
    return RxEqOfft;

  case OptTxEq:
    return TxEqOfft;

  case OptRdDqOdt:
    return RdOdtOfft;

  default:
    return 0; // Return RdSAmpOfft to point to the beginning of the array
  }
}

/**
  Normalizes the Power values to the Margins passed in Points2Calc.
  Assumes that power numbers are represented as lowest number is the lowest power,
  and inverts the scale such that highest number is the lowest power.  This is done
  before normalizing to margins.

  @param[in]     MrcData       - Include all MRC global data.
  @param[in]     Points2calc   - Data normalize power.
  @param[in]     ArrayLength   - Array length of Points2calc.
  @param[in]     LenMargin     - The length of inputMargins we are optimizing (0 - LenMargin -1).
  @param[in]     TestListSize  - Size of TestList/Scale

  @retval Nothing
**/
void
NormalizePowerToMargins (
  IN     MrcParameters   *MrcData,
  IN     void            *Points2calc,
  IN     UINT8           ArrayLength,
  IN     UINT8           LenMargin,
  IN     UINT8           TestListSize
  )
{
#if (defined (MRC_POWER_TRAINING_DEBUG) && (MRC_POWER_TRAINING_DEBUG == SUPPORT))
  MrcDebug          *Debug;
#endif
  const MRC_FUNCTION *MrcCall;
  UINT16          MaxPoints[MAX_MARGINS_TRADEOFF];
  UINT16          MinPoints[MAX_MARGINS_TRADEOFF];
  UINT16          MaxPwr;
  UINT16          MinPwr;
  UINT8           off;
  UINT8           test;
  UINT16          AveOfMax;
  UINT16          X;
  UINT16          *Points;
  UINT16          *PointsElement;

  MrcCall     = MrcData->Inputs.Call.Func;
#if (defined (MRC_POWER_TRAINING_DEBUG) && (MRC_POWER_TRAINING_DEBUG == SUPPORT))
  Debug       = &MrcData->Outputs.Debug;
#endif
  Points      = (UINT16 *) Points2calc;
  MaxPwr      = 0;
  MinPwr      = 0xffff;

  MrcCall->MrcSetMemWord (MaxPoints, sizeof (MaxPoints) / sizeof (UINT16), 0);
  MrcCall->MrcSetMemWord (MinPoints, sizeof (MinPoints) / sizeof (UINT16), 0xFFFF);

  // Sorting the min max margin points for each test
  for (off = 0; off < LenMargin; off++) {
    for (test = 0; test < TestListSize; test++) {
      PointsElement = (Points + ArrayLength * test + off);
      if (MaxPoints[test] < *PointsElement) {
        MaxPoints[test] = *PointsElement;
      }

      if (MinPoints[test] > *PointsElement) {
        MinPoints[test] = *PointsElement;
      }
    }
    PointsElement = (Points + ArrayLength * TestListSize + off);

    if (MaxPwr < *PointsElement) {
      MaxPwr = *PointsElement;
    }

    if (MinPwr > *PointsElement) {
      MinPwr = *PointsElement;
    }

    if (LenMargin == 1) {
      MaxPwr  = *PointsElement;
      MinPwr  = *PointsElement;
    }
  }

  MRC_POWER_DEBUG_MSG (Debug, MSG_LEVEL_ALGO, "MaxPwr=%d MinPwr=%d\n",MaxPwr,MinPwr);
  AveOfMax  = 0;
  TestListSize = MIN (TestListSize, ARRAY_COUNT (MaxPoints));
  for (test = 0; test < TestListSize; test++) {
    AveOfMax += MaxPoints[test];
  }
  AveOfMax = AveOfMax / TestListSize;

  MRC_POWER_DEBUG_MSG (Debug, MSG_LEVEL_ALGO, "Power Values\nBefore\tAfter\n");
  for (off = 0; off < LenMargin; off++) {
    PointsElement = (Points + ArrayLength * TestListSize + off);
    MRC_POWER_DEBUG_MSG (Debug, MSG_LEVEL_ALGO, "%d\t", *PointsElement);
    if (MaxPwr == MinPwr) {
      X = 0;
    } else {
      X = 100 - 100 * (*PointsElement - MinPwr) / (MaxPwr);
    }
    *PointsElement = AveOfMax * X / 100;
    MRC_POWER_DEBUG_MSG (Debug, MSG_LEVEL_ALGO, "%d\n", *PointsElement);
  }
}

#ifdef MRC_DEBUG_PRINT

#if 0 // This function is not used right now
/**
  Prints OptParam values from CRs and Host structure for all ch/Rank/byte as well as
  the Best optimization value (if requested)
  OptWrDS = 0
  OptRdOd = 1
  OptSCom = 2
  OptTCompOff = 3
  OptTxEq = 4
  OptRxEq = 5
  OptRxBias = 6
  OptDimmOdt = 7
  OptDimmOdtWr = 8
  OptDimmRon = 9
  OptDefault = 10

  @param[in] MrcData   - Include all MRC global data.
  @param[in] ChMask    - Channel Mask to print the summary for
  @param[in] RankMask  - Rank Mask to print the summary for (in case Rank is not applicable set RankMask = 0xF)
  @param[in] OptParam  - Defines the OptParam Offsets. OptDefault reports all parameters
  @param[in] OptOff    - Structure containg the best offest and margins for the OptParam.
                         If OptOffsetChByte is not available, NullPtr needs to be passed (void  *NullPtr)
  @param[in] OptResult - True/False: Whether to print the Best optimization value

  @retval Nothing
**/
void
ReadOptParamOffsetSum (
  IN MrcParameters *const MrcData,
  IN UINT8                ChMask,
  IN UINT8                RankMask,
  IN const UINT8          OptParam,
  IN OptOffsetChByte      *OptOff,
  IN BOOLEAN              OptResult
  )
{
  const MrcInput     *Inputs;
  MrcDebug           *Debug;
  const MRC_FUNCTION *MrcCall;
  MrcOutput          *Outputs;
  UINT8              Channel;
  UINT8              Rank;
  UINT8              Byte;
  UINT8              Param;
  UINT8              NumBytes;
  UINT8              ChannelMask;
  INT16              OffArr[2];
  INT16              Best;
  BOOLEAN            PerRank;
  BOOLEAN            SkipByte;

  Outputs     = &MrcData->Outputs;
  Inputs      = &MrcData->Inputs;
  MrcCall     = Inputs->Call.Func;
  Debug       = &Outputs->Debug;
  ChannelMask = Outputs->ValidChBitMask & ChMask;
  NumBytes    = (UINT8) Outputs->SdramCount;
  MrcCall->MrcSetMemWord ((UINT16 *) OffArr, ARRAY_COUNT (OffArr), 0);

  for (Param = OptWrDS; Param < OptDefault; Param++) {
    if (OptParam == Param || OptParam == OptDefault) {

      MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\nOffsets for Optimization Parameter %s\n", TOptParamOffsetString[Param]);
      PerRank =
        (
          Param == OptTxEq ||
          Param == OptRxEq ||
          Param == OptDimmOdt ||
          Param == OptDimmOdtWr ||
          Param == OptDimmRon
        );
      SkipByte = (Param == OptDimmRon || Param == OptDimmOdt || Param == OptDimmOdtWr);

      for (Channel = 0; Channel < MAX_CHANNEL; Channel++) {
        if (!((MRC_BIT0 << Channel) & ChannelMask)) {
          continue;
        }

        MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Channel %d\n", Channel);
        for (Rank = 0; Rank < MAX_RANK_IN_CHANNEL; Rank++) {
          if ((MRC_BIT0 << Rank) & RankMask) {
            if (!(MrcRankExist (MrcData, CONTROLLER_0, Channel, Rank))) {
              continue;
            }

            if (PerRank) {
              MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Rank %d\n", Rank);
            } else if (Rank > 0) {
              continue;
            }

            if (!SkipByte) {
              MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Byte\t");
              for (Byte = 0; Byte < NumBytes; Byte++) {
                MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "%d\t", Byte);
              }

              MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\n");
            }

            if (OptResult) {
              MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Opt/CR/Host\t");
            } else {
              MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "CR/Host\t");
            }

            if (!SkipByte) {
              for (Byte = 0; Byte < NumBytes; Byte++) {
                ReadOptParamOffset (MrcData, &OffArr[0], Channel, Rank, Byte, Param);

                if (OptResult) {
                  Best = OptOff->Offset[Channel][Byte];
                  if (Best != OffArr[0] || Best != OffArr[1]) {
                    MRC_DEBUG_MSG (
                      Debug,
                      MSG_LEVEL_NOTE,
                      "\nError: Mismatch in Param %s in Channel %d Rank %d Byte %d is found: Best=%d CR=%d Host=%d\n",
                      TOptParamOffsetString[Param],
                      Channel,
                      Rank,
                      Byte,
                      Best,
                      OffArr[0],
                      OffArr[1]
                      );
                  }

                  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "%d/%d/%d\t", Best, OffArr[0], OffArr[1]);
                } else {
                  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "%d/%d\t", OffArr[0], OffArr[1]);
                }
              }
            } else {
              ReadOptParamOffset (MrcData, &OffArr[0], Channel, Rank, 0, Param);

              if (Param == OptDimmRon || Param == OptDimmOdtWr) {
                if (OptResult) {
                  Best = OptOff->Offset[Channel][0];
                  if (Best != OffArr[1]) {
                    MRC_DEBUG_MSG (
                      Debug,
                      MSG_LEVEL_NOTE,
                      "\nError: Mismatch in Param %s in Channel %d Rank %d is found: Best=%d Host=%d\n",
                      TOptParamOffsetString[Param],
                      Channel,
                      Rank,
                      Best,
                      OffArr[1]
                      );
                  }

                  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "%d/NA/%d", Best, OffArr[1]);
                } else {
                  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "NA/%d", OffArr[1]);
                }
              } else if (Param == OptDimmOdt) {
                if (OptResult) {
                  Best = OptOff->Offset[Channel][0];
                  if (Best != OffArr[0]) {
                    MRC_DEBUG_MSG (
                      Debug,
                      MSG_LEVEL_NOTE,
                      "\nError: Mismatch in Param %s in Channel %d Rank %d is found: Best=%d Host=%d\n",
                      TOptParamOffsetString[Param],
                      Channel,
                      Rank,
                      Best,
                      OffArr[0]
                      );
                  }

                  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "%d/NA/%d", Best, OffArr[0]);
                } else {
                  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "NA/%d", OffArr[0]);
                }
              }
            }

            MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\n");
          }
        }
      }
    }
  }
}

/**
  Reads OptParam value from CRs and Host structure for a given ch/Rank/byte combination
  OptParam can be: WrDS, RdOdt, TComp, SComp, RxEq, TxEq, RxBias, DIMM Ron, DIMM RttNom or DIMM RttWr

  @param[in]  MrcData  - Include all MRC global data.
  @param[out] FinalVal - Pointer to the array consisting of CR value and Host value for a particular
                         OptParam and given ch/Rank/byte combination.
  @param[in]  Channel  - Channel index to work on.
  @param[in]  Rank     - Rank index to work on (valid only for TxEq and RxEq, for others is ignored)
  @param[in]  Byte     - Byte index to work on.
  @param[in]  OptParam - Defines the OptParam Offsets. Supported OptParam =
                         [0: WrDS, 1: RdODT, 2: SComp, 3: TComp, 3: TxEq,
                          4: RxEq, 5: RxBias, 6: DimmOdt, 7: DimmOdtWr]

  @retval Nothing
**/
void
ReadOptParamOffset (
  IN  MrcParameters *const MrcData,
  OUT INT16                *FinalVal,
  IN  const UINT8          Channel,
  IN  const UINT8          Rank,
  IN  const UINT8          Byte,
  IN  const UINT8          OptParam
  )
{
  const MRC_FUNCTION *MrcCall;
  static const UINT16 RttNomMRSEncodingConst[] = {0x00, 0x10, 0x01, 0x11, 0x81, 0x80}; // RttNom Off,120,60,40,30,20 Ohms
  static const UINT16 RttWrMRSEncodingConst[]  = {0x00, 0x02, 0x01};                // RttWr  RttNom,120,60 Ohms
  MrcDebug         *Debug;
  static const UINT8 LpddrRonEnc[] = {0x1, 0x2, 0x3}; //{34,40,48};
  static const UINT8 LpddrOdtEnc[] = {0x0, 0x2, 0x3}; //{0,120,240};
  MrcOutput       *Outputs;
  MrcChannelOut   *ChannelOut;
  UINT16          *MrReg;
  BOOLEAN         Type;
  UINT8           Index;
  UINT16          MRValue;
  UINT16          RttNomMRSEncoding[ARRAY_COUNT (RttNomMRSEncodingConst)];
  UINT16          RttWrMRSEncoding[ARRAY_COUNT (RttWrMRSEncodingConst)];
  UINT16          RttWr;
  UINT16          RttNom;
  UINT16          RttNomMask;
  UINT16          RttWrMask;
  UINT32          Offset;
  INT16           UpOff;
  INT16           DnOff;
  INT64           GetSetVal;
  DATA0CH0_CR_DDRCRDATAOFFSETCOMP_STRUCT DdrCrDataOffsetCompCr;
  DATA0CH0_CR_DDRCRDATAOFFSETCOMP_STRUCT DdrCrDataOffsetCompHost;
  DDRDATA0CH0_CR_TXTRAINRANK0_STRUCT        CrTxTrainRank;
  DDRDATA0CH0_CR_RXTRAINRANK0_STRUCT        CrRxTrainRank;
  DDRDATA0CH0_CR_DDRCRDATACONTROL1_STRUCT   DdrCrDataControl1Cr;
  DDRDATA0CH0_CR_DDRCRDATACONTROL1_STRUCT   DdrCrDataControl1Host;

  MrcCall     = MrcData->Inputs.Call.Func;
  Outputs     = &MrcData->Outputs;
  Debug       = &Outputs->Debug;
  ChannelOut  = &Outputs->Controller[0].Channel[Channel];
  MrcCall->MrcCopyMem ((UINT8 *) RttNomMRSEncoding, (UINT8 *) RttNomMRSEncodingConst, sizeof (RttNomMRSEncoding));
  MrcCall->MrcCopyMem ((UINT8 *) RttWrMRSEncoding, (UINT8 *) RttWrMRSEncodingConst, sizeof (RttWrMRSEncoding));

  // Compensation Offsets
  Type = ((OptParam == OptWrDS) || (OptParam == OptRdOdt) || (OptParam == OptTCompOff) || (OptParam == OptSComp));
  if (Type) {

    Offset = MrcGetOffsetDataOffsetComp (MrcData, Channel, Byte);
    DdrCrDataOffsetCompCr.Data   = MrcReadCR (MrcData, Offset);
    DdrCrDataOffsetCompHost.Data = ChannelOut->DataCompOffset[Byte];

    if (OptParam == OptWrDS) {
      UpOff = (INT16) (INT32) DdrCrDataOffsetCompCr.Bits.DqDrvUpCompOffset;
      DnOff = (INT16) (INT32) DdrCrDataOffsetCompCr.Bits.DqDrvDownCompOffset;
      if (UpOff != DnOff) {
        MRC_DEBUG_MSG (
          Debug,
          MSG_LEVEL_NOTE,
          "DqDrvUpCompOffset %d is not equal to DqDrvDownCompOffset for Channel=%d, Byte=%d\n",
          UpOff,
          DnOff,
          Channel,
          Byte
          );
      }

      FinalVal[0] = UpOff;
      UpOff = (INT16) (INT32) DdrCrDataOffsetCompHost.Bits.DqDrvUpCompOffset;
      DnOff = (INT16) (INT32) DdrCrDataOffsetCompHost.Bits.DqDrvDownCompOffset;
      if (UpOff != DnOff) {
        MRC_DEBUG_MSG (
          Debug,
          MSG_LEVEL_NOTE,
          "DqDrvUpCompOffset %d is not equal to DqDrvDownCompOffset for Channel=%d, Byte=%d in Host Structure\n",
          UpOff,
          DnOff,
          Channel,
          Byte
          );
      }

      FinalVal[1] = UpOff;

      if (FinalVal[0] & 0x20) {     // 6-bit 2's complement
        FinalVal[0] -= 0x40;
      }
      if (FinalVal[1] & 0x20) {     // 6-bit 2's complement
        FinalVal[1] -= 0x40;
      }
    } else if (OptParam == OptRdOdt) {
      UpOff = (INT16) (INT32) DdrCrDataOffsetCompCr.Bits.DqOdtUpCompOffset;
      DnOff = (INT16) (INT32) DdrCrDataOffsetCompCr.Bits.DqOdtDownCompOffset;
      if (UpOff != DnOff) {
        MRC_DEBUG_MSG (
          Debug,
          MSG_LEVEL_NOTE,
          "DqOdtUpCompOffset %d is not equal to DqOdtDownCompOffset for Channel=%d, Byte=%d\n",
          UpOff,
          DnOff,
          Channel,
          Byte
          );
      }

      FinalVal[0] = UpOff;
      UpOff = (INT16) (INT32) DdrCrDataOffsetCompHost.Bits.DqOdtUpCompOffset;
      DnOff = (INT16) (INT32) DdrCrDataOffsetCompHost.Bits.DqOdtDownCompOffset;
      if (UpOff != DnOff) {
        MRC_DEBUG_MSG (
          Debug,
          MSG_LEVEL_NOTE,
          "DqOdtUpCompOffset %d is not equal to DqOdtDownCompOffset for Channel=%d, Byte=%d in Host Structure\n",
          UpOff,
          DnOff,
          Channel,
          Byte
          );
      }

      FinalVal[1] = UpOff;

      if (FinalVal[0] & 0x10) {     // 5-bit 2's complement
        FinalVal[0] -= 0x20;
      }
      if (FinalVal[1] & 0x10) {     // 5-bit 2's complement
        FinalVal[1] -= 0x20;
      }
    } else if (OptParam == OptTCompOff) {
      FinalVal[0] = (INT16) (INT32) DdrCrDataOffsetCompCr.Bits.DqTcoCompOffset;
      FinalVal[1] = (INT16) (INT32) DdrCrDataOffsetCompHost.Bits.DqTcoCompOffset;

      if (FinalVal[0] & 0x10) {     // 5-bit 2's complement
        FinalVal[0] -= 0x20;
      }
      if (FinalVal[1] & 0x10) {     // 5-bit 2's complement
        FinalVal[1] -= 0x20;
      }
    } else if (OptParam == OptSComp) {
      FinalVal[0] = (INT16) (INT32) DdrCrDataOffsetCompCr.Bits.DqSlewRateCompOffset;
      FinalVal[1] = (INT16) (INT32) DdrCrDataOffsetCompHost.Bits.DqSlewRateCompOffset;

      if (FinalVal[0] & 0x10) {     // 5-bit 2's complement
        FinalVal[0] -= 0x20;
      }
      if (FinalVal[1] & 0x10) {     // 5-bit 2's complement
        FinalVal[1] -= 0x20;
      }
    }

    if (FinalVal[0] != FinalVal[1]) {
      MRC_DEBUG_MSG (
        Debug,
        MSG_LEVEL_NOTE,
        "Error: Param %s: CR value %d doesn't match Host value %d for Channel=%d, Byte=%d\n",
        TOptParamOffsetString[OptParam],
        FinalVal[0],
        FinalVal[1],
        Channel,
        Byte
        );
    }
  }
  // Equalization Settings
  Type = ((OptParam == OptTxEq) || (OptParam == OptRxEq));
  if (Type) {
    // TxEq[5:4] = Emphasize   = [3, 6, 9, 12] legs
    // TxEq[3:0] = Deemphasize = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 4*Rsvd] legs
    if (OptParam == OptTxEq) {

     Offset = DDRDATA0CH0_CR_TXTRAINRANK0_REG +
      ((DDRDATA0CH1_CR_TXTRAINRANK0_REG - DDRDATA0CH0_CR_TXTRAINRANK0_REG) * Channel) +
      ((DDRDATA0CH0_CR_TXTRAINRANK1_REG - DDRDATA0CH0_CR_TXTRAINRANK0_REG) * Rank) +
      ((DDRDATA1CH0_CR_TXTRAINRANK0_REG - DDRDATA0CH0_CR_TXTRAINRANK0_REG) * Byte);

      CrTxTrainRank.Data  = MrcReadCR (MrcData, Offset);
      // @todo: <CNL> Fix this is broken
      FinalVal[0]         = (INT16) (INT32) CrTxTrainRank.Bits.TxEqualization;
      MrcGetSetDdrIoGroupStrobe (MrcData, Channel, Rank, Byte, TxEq, ReadFromCache, &GetSetVal);
      FinalVal[1]         = (INT16) GetSetVal;
      FinalVal[0]        &= 0xF;  // Read Deemphasize portion only
      FinalVal[1]        &= 0xF;  // Read Deemphasize portion only
    }
    // RxEQ[4:0] CR Decoding (pF/kOhm)
    //            [2:0]
    //  [4:3]     0        1        2        3        4        5-7
    //     0      0.5/.02  0.5/1.0  0.5/.50  0.5/.25  0.5/.12  rsvd
    //     1      1.0/.02  1.0/1.0  1.0/.50  1.0/.25  1.0/.12  rsvd
    //     2      1.5/.02  1.5/1.0  1.5/.50  1.5/.25  1.5/.12  rsvd
    //     3      2.0/.02  2.0/1.0  2.0/.50  2.0/.25  2.0/.12  rsvd
    // Sweep = 0-19        [4:3] = (Sweep/5)  [2:0] = (Sweep%5)
    if (OptParam == OptRxEq) {
      Offset = DDRDATA0CH0_CR_RXCONTROL1RANK0_REG +
        ((DDRDATA0CH1_CR_RXCONTROL1RANK0_REG - DDRDATA0CH0_CR_RXCONTROL1RANK0_REG) * Channel) +
        ((DDRDATA0CH0_CR_RXCONTROL1RANK1_REG - DDRDATA0CH0_CR_RXCONTROL1RANK0_REG) * Rank) +
        ((DDRDATA1CH0_CR_RXCONTROL1RANK0_REG - DDRDATA0CH0_CR_RXCONTROL1RANK0_REG) * Byte);

      CrRxTrainRank.Data = MrcReadCR (MrcData, Offset);
      FinalVal[0] = (INT16) (INT32) CrRxTrainRank.Bits.RxEq;
      FinalVal[1] = (INT16) (INT32) ChannelOut->RxEq[Rank][Byte];
      FinalVal[0] = ((FinalVal[0] >> 3) * 5) + (FinalVal[0] & 0x7);  // Multiply Cap portion by 5 and add Res portion
      FinalVal[1] = ((FinalVal[1] >> 3) * 5) + (FinalVal[1] & 0x7);  // Multiply Cap portion by 5 and add Res portion
    }

    if (FinalVal[0] != FinalVal[1]) {
      MRC_DEBUG_MSG (
        Debug,
        MSG_LEVEL_NOTE,
        "Error: Param %s: CR value %d doesn't match Host value %d for Channel=%d, Rank=%d, Byte=%d\n",
        TOptParamOffsetString[OptParam],
        FinalVal[0],
        FinalVal[1],
        Channel,
        Rank,
        Byte
        );
    }
  }
  // RX Amplifier BIAS
  if ((OptParam == OptRxBias)) {
    // Mapping: [0: 0.44, 1: 0.66, 2: 0.88, 3: 1.00, 4: 1.33, 5: 1.66, 6: 2.00, 7: 2.33]
    Offset = MrcGetOffsetDataControl1 (MrcData, Channel, Byte);

    DdrCrDataControl1Cr.Data    = MrcReadCR (MrcData, Offset);
    MrcGetSetChStrb (MrcData, CONTROLLER_0, Channel, Byte, RxBiasIComp, ReadFromCache, &GetSetVal);
    FinalVal[0]                 = (INT16) (INT32) DdrCrDataControl1Cr.Bits.RxBiasCtl;
    FinalVal[1]                 = (INT16) (INT32) GetSetVal;

    if (FinalVal[0] != FinalVal[1]) {
      MRC_DEBUG_MSG (
        Debug,
        MSG_LEVEL_NOTE,
        "Error: Param %s: CR value %d doesn't match Host value %d for Channel=%d, Byte=%d\n",
        TOptParamOffsetString[OptParam],
        FinalVal[0],
        FinalVal[1],
        Channel,
        Byte
        );
    }
  }
  // Dimm Ron value
  if ((OptParam == OptDimmRon)) {
    // DIMM Ron Encoding   DriverImpCtrl[A5,A1]
    if (MrcRankExist (MrcData, CONTROLLER_0, Channel, Rank)) {
      MrReg = &ChannelOut->Dimm[Rank / 2].Rank[(Rank % 2)].MR[mrMR0];
      MRValue     = MrReg[mrMR1];
      FinalVal[1] = (INT16) ((MRValue >> 1) & 0x1);
    }
  }
  // DIMM ODT Values
  if ((OptParam == OptDimmOdt) || (OptParam == OptDimmOdtWr)) {
    // DIMM ODT Encoding   RttNom[A9,A6,A2]   RttWr[A10, A9] LPDDR - No RttNom
    if (MrcRankExist (MrcData, CONTROLLER_0, Channel, Rank)) {
        MrReg       = &ChannelOut->Dimm[Rank / 2].Rank[Rank % 2].MR[mrMR0];
        RttNomMask  = (MRC_BIT9 + MRC_BIT6 + MRC_BIT2);
        RttWrMask   = (MRC_BIT10 + MRC_BIT9);
        RttWr       = (MrReg[mrMR2] & RttWrMask) >> 9;
        RttNom      = (MrReg[mrMR1] & RttNomMask) >> 2;

        for (Index = 0; Index < ARRAY_COUNT (RttNomMRSEncodingConst); Index++) {
          if (RttNom == RttNomMRSEncoding[Index]) {
            FinalVal[0] = (INT16) (INT8) Index;
          }
        }

        for (Index = 0; Index < ARRAY_COUNT (RttWrMRSEncodingConst); Index++) {
          if (RttWr == RttWrMRSEncoding[Index]) {
            FinalVal[1] = (INT16) (INT8) Index;
          }
        }
    }
  }
}

/**
  This function will print out the last margin data collected of the Param passed in.
  It will print both edges of all the requested bytes, Ranks and Channels.
  NOTE: The function will not check to see if the Rank/Channel exists.  It will print out the
  values stored in the margin array regardless of population status.

  @param[in]  MrcData     - Global MRC data.
  @param[in]  Param       - Parameter of MRC_MarginTypes of which to print the margin.
  @param[in]  ChannelMask - Bit mask of channels to print.
  @param[in]  RankMask    - Bit mask of ranks to print.
  @param[in]  ByteMask    - Bit mask of bytes to print.

  @retval Nothing.
**/
void
MrcPrintLastMargins (
  IN MrcParameters *const MrcData,
  IN const UINT8          Param,
  IN const UINT8          ChannelMask,
  IN const UINT8          RankMask,
  IN const UINT16         ByteMask
  )
{
  MrcDebug        *Debug;
  MrcOutput       *Outputs;
  char            *EdgeString;
  MrcMarginResult LastResultParam;
  UINT32          (*LastMargins)[MAX_RANK_IN_CHANNEL][MAX_CHANNEL][MAX_SDRAM_IN_DIMM][MAX_EDGES];
  UINT8           Channel;
  UINT8           Rank;
  UINT8           Byte;
  UINT8           Edge;

  LastResultParam = GetMarginResultType (Param);
  Outputs         = &MrcData->Outputs;
  Debug           = &Outputs->Debug;
  LastMargins     = Outputs->MarginResult;

  MRC_DEBUG_MSG (
    Debug,
    MSG_LEVEL_NOTE,
    "%s Last Margins:\n",
    gMarginTypesStr[Param]
    );

  EdgeString = ((Param == RdV) || (Param == WrV)) ? "H" : "R";

  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Byte\t");
  for (Byte = 0; Byte < Outputs->SdramCount; Byte++) {
    if ((1 << Byte) & ByteMask) {
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "% 10d", Byte);
    }
  }

  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\nEdge\t");
  for (Byte = 0; Byte < Outputs->SdramCount; Byte++) {
    if ((1 << Byte) & ByteMask) {
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "    L    %s", EdgeString);
    }
  }

  for (Rank = 0; Rank < MAX_RANK_IN_CHANNEL; Rank++) {
    if ((1 << Rank) & RankMask) {
      for (Channel = 0; Channel < MAX_CHANNEL; Channel++) {
        if ((1 << Channel) & ChannelMask) {
          MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\nR%d.C%d\t", Rank, Channel);
          for(Byte = 0; Byte < Outputs->SdramCount; Byte++) {
            if ((1 << Byte) & ByteMask) {
              for (Edge = 0; Edge < MAX_EDGES; Edge++) {
                MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "% 5d", LastMargins[LastResultParam][Rank][Channel][Byte][Edge]);
              }
            }
          }
        }
      }
    }
  }

  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\n");  // End of table
}
#endif // #if 0

/**
  This function implements switch to print the correct format and data for the
  OptResultsPerByte struct members.

  @param[in] Debug     - Debug pointer for printing.
  @param[in] Data      - Pointer to OptResultsPerByte struct.
  @param[in] TypeIndex - Member of OptResultsPerByte to print.
  @param[in] TestIndex - Some parameters store multiple test results to be printed.
  @param[in] MidPoint  - Used to convert from zero-based indexing to the selected value

  @retval Nothing.
**/
void
MrcOptResultsPerBytePrint (
  IN MrcDebug *const    Debug,
  IN OptResultsPerByte  *Data,
  IN UINT8              TypeIndex,
  IN UINT8              TestIndex,
  IN INT8               MidPoint
  )
{
  switch (TypeIndex) {
    case (MrcOptResultBest):
      (TestIndex == 0) ? MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "=== %d ===", Data->Best - MidPoint) :
                         MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\t");
      break;

    case (MrcOptResultGrdBnd):
      (TestIndex == 0) ? MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "*** %d ***", Data->GuardBand) :
                         MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\t");
      break;

    case(MrcOptResultOffSel):
      (TestIndex == 0) ? MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "--> %d <--", Data->Best - MidPoint + Data->GuardBand) :
                         MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\t");
      break;

    case (MrcOptResultScale):
      MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "%d\t", Data->Scale[TestIndex]);
      break;

    case (MrcOptResultMaxPost):
      MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "%d\t", Data->MaxPost[TestIndex]);
      break;

    case (MrcOptResultMinPost):
      MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "%d\t", Data->MinPost[TestIndex]);
      break;

    default:
      MRC_DEBUG_MSG (Debug, MSG_LEVEL_ERROR, "OptResultPerByteDbgStr Switch exceeded number of cases defined\n");
  }
}


/**
  This function prints the Optimize margin result table
  e.g: calcResultSummary[MAX_CHANNEL][MAX_SDRAM_IN_DIMM]

  @param[in] MrcData           - MRC data structure
  @param[in] calcResultSummary - The data array [MAX_CHANNEL][MAX_SDRAM_IN_DIMM]
  @param[in] TestList          - Test list
  @param[in] NumTest           - Number of test
  @param[in] NumOffsets        - Number of offsets
  @param[in] MidPoint          - Middle point
  @param[in] IncEnds           - Print ends points
  @param[in] OptParam          - Used to convert to the Opt param string for printing
  @param[in] OptPower          - Opt Power values to be printed
  @param[in] Channel           - Channel to print
  @param[in] Ranks             - Ranks to print
  @param[in] TrendLine         - Switch to print the trend line
  @param[in] Nibble            - take low/high bytes
  @param[in] perCh             - Switch to only print 1 Byte of data
  @param[in] noPrint           - Boolean used to disable printing of results

  @retval Nothing
**/
void
PrintCalcResultTableCh (
  IN MrcParameters *const MrcData,
  IN OptResultsPerByte    calcResultSummary[MAX_SDRAM_IN_DIMM],
  IN const UINT8          *TestList,
  IN UINT8                NumTest,
  IN UINT8                NumOffsets,
  IN INT8                 MidPoint,
  IN BOOLEAN              IncEnds,
  IN UINT8                OptParam,
  IN UINT16               *OptPower,
  IN UINT8                Controller,
  IN UINT8                Channel,
  IN UINT8                Ranks,
  IN BOOLEAN              TrendLine,
  IN UINT8                Nibble,
  IN BOOLEAN              perCh,
  IN BOOLEAN              noPrint
  )
{
  const MRC_FUNCTION *MrcCall;
  MrcDebug          *Debug;
  OptResultsPerByte *data;
  UINT8             Off; // @todo: change for the new alg.
  UINT8             Start;
  UINT8             Stop;
  UINT8             i;
  UINT8             j;
  UINT8             b;
  UINT8             FirstByte;
  UINT8             NumBytes;
  UINT8             NumTestPlus;
  UINT32            Result;
  BOOLEAN           Format64Results;
  UINT8             Param;

  MrcCall         = MrcData->Inputs.Call.Func;
  Format64Results = 1;
  // Display result in %/Delta , 0-displat raw 64bit result in HEX
  Debug = &MrcData->Outputs.Debug;
  Start = (!IncEnds);
  Stop  = NumOffsets - (!IncEnds);
  if (noPrint) {
    return ;

  }

  FirstByte = (Nibble) ? 4 : 0;
  NumBytes  = FirstByte + 4 + Nibble * MrcData->Outputs.SdramCount % 8;
  if (perCh) {
    NumBytes = 1;
  }

  NumTestPlus = (TrendLine) ? NumTest + 1 : NumTest;

  MRC_DEBUG_MSG (
    Debug,
    MSG_LEVEL_NOTE,
    "\n<======== optimize %s ========>Plot results ",
    TOptParamOffsetString[OptParam]
    );
  MRC_DEBUG_MSG (
    Debug,
    MSG_LEVEL_NOTE,
    "<Channel=%d><rank/s=0x%x><Nibble=%s> across settings :(Start=%d,Stop=%d)\n",
    Channel,
    Ranks,
    (Nibble) ? "High" : "Low",
    Start - MidPoint,
    Stop - MidPoint - 1
    );
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Bytes\t");
  for (b = FirstByte; b < NumBytes; b++) {
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "%d\t", b);
    for (i = 0; i < NumTestPlus + 1; i++) {
      MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\t"); // tab insertion
    }
  }

  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\n"); // row end here !
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Offset\t"); // row starts here !
  if (OptPower[Stop - 1] != 0) {//WA: need to add param to enable this print
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "%s\t","TotPwr");
  }

  for (b = FirstByte; b < NumBytes; b++) {
    for (i = 0; i < NumTest; i++) {
      // Test types header
      Param = TestList[i];
      if (Param > CmdV) {
        Param = (Param % 16) + 4;
      }

      MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "%s\t", gMarginTypesStr[Param]);
    }

    if (TrendLine) {
      MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "%s\t", "PwrVal");
    }

    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Opt.func\t"); // more header..
  }

  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\n\n"); // row end here !
  for (Off = Start; Off < Stop; Off++) {
    // row starts here !
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "%d\t", Off - MidPoint);
    if (OptPower[Stop - 1] != 0) {
      MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "%d.%d\t", OptPower[Off]/10, OptPower[Off]%10);
    }

    for (b = FirstByte; b < NumBytes; b++) {
      if (b < MAX_SDRAM_IN_DIMM) {
        data = &calcResultSummary[b];
      } else {
        MRC_DEBUG_MSG (
          Debug,
          MSG_LEVEL_NOTE,
          "Error: calcResultSummary array out of bounds! %d > %d \n",
          b,
          MAX_SDRAM_IN_DIMM - 1
          );
        return;
      }

      for (i = 0; i < NumTestPlus; i++) {
        MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "%d\t", data->Margins[i][Off].EW);
      }

      if (Format64Results) {
        Result = (UINT32) (MrcCall->MrcDivU64x64 (MrcCall->MrcMultU64x32 (data->Result[Off], 200), data->MaxR, NULL));
        Result /= 2;
        MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "%d\t\t", Result);
      }

      if (!Format64Results) {
        MRC_DEBUG_MSG (
          Debug,
          MSG_LEVEL_NOTE,
          "%08x-%08x\t\t",
          (UINT32) MrcCall->MrcRightShift64 (data->Result[Off],
                32),
          (UINT32) (data->Result[Off])
          );
      }
    }

    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\n"); // row end here !
  }

  for (i = 0; i < ARRAY_COUNT (OptResultDbgStrings); i++) {
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "%s\t", OptResultDbgStrings[i]);
    for (b = FirstByte; b < NumBytes; b++) {
      if (b < MAX_SDRAM_IN_DIMM) {
        data = &calcResultSummary[b];
      } else {
        MRC_DEBUG_MSG (
          Debug,
          MSG_LEVEL_NOTE,
          "Error: calcResultSummary array out of bounds! %d > %d \n",
          b,
          MAX_SDRAM_IN_DIMM - 1
          );
        return;
      }

      for (j = 0; j < NumTestPlus; j++) {
        MrcOptResultsPerBytePrint (Debug, data, i, j, MidPoint);
      }

      MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\t\t"); // tab insertion
    }

    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\n");
  } // row end here !
  return;
}

/**
  This function prints the Optimize margin result table
  e.g: MarginResult[Test][Offset][Channel][Byte][sign]

  @param[in] MrcData     - MRC data structure
  @param[in] ChMask      - Channels to print
  @param[in] ResultArray - Array with saved margin results
  @param[in] TestNum     - Test index
  @param[in] OffsetsNum  - number of offsets
  @param[in] MidPoint    - Zero point
  @param[in] OptParam    - Used to convert to the Opt param string for printing
  @param[in] Param       - Margin type to be printed.
  @param[in] PowerLimits - Power limits to print.
  @param[in] noPrint     - Used to skip printing.

  @retval Nothing
**/
void
PrintResultTableByte4by24 (
  IN MrcParameters   *MrcData,
  IN UINT8           McChMask,
  IN UINT16          ResultArray[][MAX_OPT_POINTS][MAX_CONTROLLER][MAX_BYTE_IN_DDR5_CHANNEL * MAX_DIMMS_IN_CHANNEL],
  IN UINT16          TestNum,
  IN UINT8           OffsetsNum,
  IN UINT8           MidPoint,
  IN UINT8           OptParam,
  IN UINT8           Param,
  IN UINT16          *PowerLimits,
  IN BOOLEAN         noPrint
  )
{
  MrcDebug        *Debug;
  MrcOutput       *Outputs;
  UINT8           Channel;
  UINT8           Byte;
  UINT8           Controller;
  UINT8           Off;
  UINT8           Start;
  UINT8           Stop;

  // @todo new alg. change pass start stop...
  Outputs = &MrcData->Outputs;
  Debug   = &Outputs->Debug;
  Start   = -MidPoint;
  Stop    = OffsetsNum - MidPoint - 1;
  if (Param > CmdV) {
    Param = (Param % 16) + 4;
  }

  if (noPrint) {
    return;
  }

  MRC_DEBUG_MSG (
    Debug,
    MSG_LEVEL_NOTE,
    "\nTest number: %d - %s, Plot results across OptParam=%s (Start=%d,Stop=%d)",
    TestNum,
    gMarginTypesStr[Param],
    TOptParamOffsetString[OptParam],
    Start,
    Stop
    );

  MRC_DEBUG_MSG(Debug, MSG_LEVEL_NOTE, "w/power limits(width): %d\nMc.Ch\t", PowerLimits[TestNum]);

  for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
    for (Channel = 0; Channel < Outputs->MaxChannels; Channel++) {
      MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "%d.%d", Controller, Channel);
      for (Byte = 0; Byte < Outputs->SdramCount; Byte++) {
        MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\t");
      }
    }
  }

  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\nByte\t");

  for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
    for (Channel = 0; Channel < Outputs->MaxChannels; Channel++) {
      for (Byte = 0; Byte < Outputs->SdramCount; Byte++) {
        MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "%u\t", Byte);
      }
    }
  }
  // Sweep through OpParam settings
  for (Off = Start; Off < Stop + 1; Off++) {
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\n  %d:\t", Off);
    for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
      for (Channel = 0; Channel < Outputs->MaxChannels; Channel++) {
        // spaces for non populated channel
        if (!(MC_CH_MASK_CHECK(McChMask, Controller, Channel, Outputs->MaxChannels))) {
          if (Controller != MAX_CONTROLLER && Channel != Outputs->MaxChannels) {
            for (Byte = 0; Byte < Outputs->SdramCount; Byte++) {
              MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\t");
            }
          }
          continue;
        }

        for (Byte = 0; Byte < Outputs->SdramCount; Byte++) {
          if (Byte < MAX_SDRAM_IN_DIMM) {
            MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "%d\t", ResultArray[TestNum][Off - Start][Controller][Channel * Outputs->SdramCount + Byte]);
          }
        }
      }
    }
  }
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\n");  // New line after the end of the table

  return;
}

/**
  This function prints the Optimize margin result table

  @param[in] MrcData            - MRC data structure
  @param[in] Controller         - Controller to print
  @param[in] Channel            - Channel to print
  @param[in] Byte               - Byte to print
  @param[in] calcResultSummary  - Array with saved margin results
  @param[in] BestOff            - Pointer to the selected offsets
  @param[in] Param              - Margin type to print.
  @param[in] OffsetsNum         - number of offsets
  @param[in] Start              - Start offsets
  @param[in] Stop               - End offsets
  @param[in] OptParam           - List of optimization parameters
  @param[in] OptParamLen        - Number of optimization parameters
  @param[in] PowerLimits        - Power limits to print.
  @param[in] Dim                - Dimension
  @param[in] TestNum            - Test index
  @param[in] NumTests           - Number of tests
  @param[in] noPrint            - Used to skip printing.

  @retval Nothing
**/
void
Print2DResultTableChByte (
  IN MrcParameters      *MrcData,
  IN UINT8              Controller,
  IN UINT8              Channel,
  IN UINT8              Byte,
  IN OptResultsPerByte  *calcResultSummary,
  IN OptOffsetChByte    *BestOff,
  IN UINT8              Param,
  IN UINT8              OffsetsNum,
  IN INT8               *Start,
  IN INT8               *Stop,
  IN const UINT8        *OptParam,
  IN UINT8              *OptParamLen,
  IN UINT16             *PowerLimits,
  IN UINT8              Dim,
  IN UINT16             TestNum,
  IN UINT8              NumTests,
  IN BOOLEAN            noPrint
  )
{
  MrcDebug          *Debug;
  MrcOutput         *Outputs;
  UINT8             Off;
  UINT8             XOff;
  UINT8             YOff;
  OptResultsPerByte *data;

  Outputs = &MrcData->Outputs;
  Debug   = &Outputs->Debug;
  if (Param > CmdV) {
    Param = (Param % 16) + 4;
  }

  if (noPrint) {
    return;
  }

  if ((Byte == 0) && ((Dim > 1) || (TestNum == 0))) {
    MRC_DEBUG_MSG (
      Debug,
      MSG_LEVEL_NOTE,
      "\nPlot Margin %s results across OptParam=%s Start=%d Stop=%d ",
      (Dim > 1)? gMarginTypesStr[Param] : "",
      TOptParamOffsetString[OptParam[0]],
      Start[0],
      Stop[0]
    );

    if (Dim > 1) {
      MRC_DEBUG_MSG (
        Debug,
        MSG_LEVEL_NOTE,
        "& OptParam=%s Start=%d Stop=%d, power limits=%d\n",
        TOptParamOffsetString[OptParam[1]],
        Start[1],
        Stop[1],
        PowerLimits[TestNum]
      );
    } else {
      MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\n");
    }
  }
  // Sweep through OpParam settings
  data = calcResultSummary;
  // print param0 header and offsets
  if ((Dim > 1) || ((TestNum == 0) && (Byte == 0))) {
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "%s\n\t", TOptParamOffsetString[OptParam[0]]);
  //     MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "MC%dC%dB%d\t", Controller, Channel, Byte);
    for (Off = 0; Off < OptParamLen[0]; Off++) {// print param0 offsets
      MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "%d\t", Off + Start[0]);
    }
  }
  // print param1 header
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\n%s\t", (Dim > 1) ? TOptParamOffsetString[OptParam[1]] : "");

  if ((Dim > 1 ) || (TestNum == 0)) {
  // print Ch Byte and separators, the -1 is result of the 2 tab width of below line
    for (Off = 0; Off < (OptParamLen[0] - 1); Off++) {
      if (Off == (OptParamLen[0] / 2 - 1)) {
        MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, " MC%dC%dB%d %s", Controller, Channel, Byte, (Dim > 1)? gMarginTypesStr[Param] : "---");
      } else {
        MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "--------");
      }
    }
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "-");
  }

  for (Off = 0; Off <  OffsetsNum; Off++) {
    XOff = Off % OptParamLen[0];
    YOff = Off / OptParamLen[0];
    if (XOff == 0) {
      if (Dim > 1) { // print param1 offset
        MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\n%d\t", YOff + Start[1]);
      } else {
        MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\n%s\t", gMarginTypesStr[Param]);
      }
    }
    //(UINT32) (MrcCall->MrcDivU64x64 (MrcCall->MrcMultU64x32 (data->Result[Off], 200), data->MaxR, NULL)/2)
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "%d", data->Margins[TestNum][Off].EW);
    if (BestOff->Offset[Controller][Channel * Outputs->SdramCount + Byte] == Off) { // mark the chosen one
      MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "**");
    }
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\t");
  }

  if ((Dim > 1) || (TestNum == (NumTests - 1))) {
    // print separators between ch or at last test in list
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\n\t");
    for (Off = 0; Off < (OptParamLen[0] - 1); Off++) {
      MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "--------");
    }
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "-----\n");
  }

  return;
}
#endif // MRC_DEBUG_PRINT

/**
  This function returns the UPM or PWR limit value for the specified parameter

  @param[in] MrcData   - Pointer to MRC global data.
  @param[in] Param     - Margin type
  @param[in] LimitType - Type of limit: UpmLimit or PowerLimit

  @retval Returns the UPM or PWR limit
**/
UINT16
MrcGetUpmPwrLimit (
  IN MrcParameters *const MrcData,
  IN UINT8                Param,
  IN UINT8                LimitType
  )
{
  MrcIntOutput           *IntOutputs;
  MrcOutput              *Outputs;
  const MrcInput         *Inputs;
  MrcDdrType             DdrType;
  MrcUpmPwrRetrainLimits *MrcLimits;
  UINT32                 Index;
  UINT16                 Limit;

  Limit      = 0;
  Outputs    = &MrcData->Outputs;
  Inputs     = &MrcData->Inputs;
  IntOutputs = (MrcIntOutput *) (MrcData->IntOutputs.Internal);
  MrcLimits  = IntOutputs->UpmPwrRetrainLimits.Pointer;
  DdrType    = Outputs->DdrType;

  for (Index = 0; Index < MRC_NUMBER_UPM_PWR_RETRAIN_MARGINS; Index++) {
    if (Param == MrcLimits[Index].Param) {
      Limit = MrcLimits[Index].ParamLimit[LimitType];
      break;
    }
  }

  if (LimitType != RetrainLimit) {
    if ((Param == WrV) && (Outputs->Frequency > f1867)) { // TGL_POWER_TRAINING May need to change this speed or eliminate this case (or add others)
      Limit += 160;
    }

    if ((Inputs->PowerTrainingMode == MrcTmMargin) && (LimitType == PowerLimit)) {
      Limit += 120;
    }
  }

  // TGL_POWER_TRAINING - Need to look into how this limit changes with DDR5.
  if (Param == WrV || Param == CmdVref) {
    // Divide by (<memory> vref step size / LP4 vref step size * 10)
    if (DdrType == MRC_DDR_TYPE_DDR4) {
      if (Param == WrV) {
        Limit = ((Limit * 10) / 16); // DDR4 TxVref 0.65% step size
      } else {
        Limit = ((Limit * 10) / 7); // DDR4 CmdVref 0.26% step size
      }
    } else if (DdrType == MRC_DDR_TYPE_LPDDR4 && Outputs->Lp4x) {
      Limit = ((Limit * 10) / 15); // LP4x 0.6% step size
    } else if (DdrType == MRC_DDR_TYPE_LPDDR5) {
      Limit = ((Limit * 10) / 8); // LP5 0.5% step size
    }
  }

  if ((Inputs->PowerTrainingMode == MrcTmPower) && (DdrType == MRC_DDR_TYPE_LPDDR4) && (LimitType == UpmLimit)) {
    if (Param == RdV) {
      Limit += 120;
    } else if (Param == RdT) {
      Limit += 80;
    }
  }

  return Limit;
}


/**
  This function returns the Start/Stop limits value for the specified parameter

  @param[in] MrcData   - Pointer to MRC global data.
  @param[in] Param     - Opt Param type
  @param[in] LimitType - Type of limit: 0 - Start 1 - Stop

  @retval Returns the Start or Stop limit
**/
INT8
OptParamLimitValue (
  IN MrcParameters *const MrcData,
  IN UINT8                Param,
  IN UINT8                LimitType
  )
{
  MrcDebug                 *Debug;
  MrcOutput                *Outputs;
  const MrcInput           *Inputs;
  const MrcOptParamsLimits *MrcLimits;
  UINT32                   Index;
  INT8                     Limit;
  BOOLEAN                  NotFound;

  Limit      = 0;
  NotFound   = TRUE;
  Outputs    = &MrcData->Outputs;
  Debug      = &Outputs->Debug;
  Inputs     = &MrcData->Inputs;
  MrcLimits  = MrcOptParamLimit;

  for (Index = 0; Index < MRC_NUMBER_OPT_PARAMS_TRAIN; Index++) {
    if (Param == MrcLimits[Index].Param) {
      if (Outputs->Frequency < f1600) { // TGL_POWER_TRAINING May need to change this speed
        Limit = MrcLimits[Index].SaGvLimits[LimitType];
      } else if (Inputs->PowerTrainingMode == MrcTmMargin || Inputs->DtHalo) {
        Limit = MrcLimits[Index].MaxPerfLimits[LimitType];
      } else {
        Limit = MrcLimits[Index].Limits[LimitType];
      }
      NotFound =FALSE;
      break;
    }
  }

  if (NotFound) {
    MRC_DEBUG_ASSERT (
      FALSE,
      Debug,
      "Can't find OptParamLimit for param %s",
      TOptParamOffsetString[Param]
      );
  }
  return Limit;
}

/**
  This function will adjust the requested Limit Type of the margin parameter by the signed offset passed in.

  @param[in]  MrcData   - MRC global data.
  @param[in]  Param     - Margin parameter type to adjust.
  @param[in]  LimitType - MRC_MARGIN_LIMIT_TYPE to adjust.
  @param[in]  Offset    - The adjustment value.

  @retval UINT16 - The new value of Param[MRC_MARGIN_LIMIT_TYPE]
**/
UINT16
MrcUpdateUpmPwrLimits (
  IN OUT MrcParameters * const  MrcData,
  IN UINT8                      Param,
  IN UINT8                      LimitType,
  IN INT8                       Offset
  )
{
  MrcIntOutput            *IntOutputs;
  MrcUpmPwrRetrainLimits  *MrcLimits;
  UINT32                  Index;
  INT32                   UpdatedValue;

  IntOutputs    = (MrcIntOutput *) (MrcData->IntOutputs.Internal);
  MrcLimits     = IntOutputs->UpmPwrRetrainLimits.Pointer;
  UpdatedValue  = 0;

  for (Index = 0; Index < MRC_NUMBER_UPM_PWR_RETRAIN_MARGINS; Index++) {
    if (Param == MrcLimits[Index].Param) {
      UpdatedValue = MrcLimits[Index].ParamLimit[LimitType];
      break;
    }
  }

  UpdatedValue += Offset;
  UpdatedValue = MAX (UpdatedValue, 0);
  UpdatedValue = MIN (UpdatedValue, 0xFFFF);

  MrcLimits[Index].ParamLimit[LimitType] = (UINT16) UpdatedValue;

  return (UINT16) UpdatedValue;
}

/**
  Returns the Actual DIMM Driver/Odt Impedance in Ohms.
  Note: host structure calculation based.

  @param[in] MrcData       - Pointer to MRC global data.
  @param[in] Controller    - Zero based controller number.
  @param[in] Channel       - Zero based channel number.
  @param[in] Rank          - Zero based rank number.
  @param[in] OptParam      - Param to read.
  @param[in] Override      - Override host read value.
  @param[in] OverrideValue - Value to override.
  @param[in] GetFromTable  - Get the Values from Odt tables

  @retval Returns the DIMM driver impedance value in Ohms
**/
UINT16
CalcDimmImpedance (
  IN MrcParameters *const MrcData,
  IN UINT32               Controller,
  IN UINT32               Channel,
  IN UINT32               Rank,
  IN UINT8                OptParam,
  IN BOOLEAN              Override,
  IN UINT16               OverrideValue,
  IN BOOLEAN              GetFromTable
  )
{
  MrcOutput                   *Outputs;
  MrcChannelOut               *ChannelOut;
  MrcDebug                    *Debug;
  MrcDdrType                  DdrType;
  MrcStatus                   Status;
  // DDR4 Parameters
  static const UINT16         Ddr4RttNomDic[8] = { 0xFFFF, 60, 120, 40, 240, 48, 80, 34 }; // Same for Rtt_Park
  static const UINT16         Ddr4RttWrDic[5]  = { 0, 120, 240, 0xFFFF, 80 };              // in DDR4 we dont assert odt to target so 0 = off also
  static const UINT16         Ddr4RonDic[2]    = { 34, 48};
  // LPDDR4/LPDDR5 Parameters
  static const UINT16         Lpddr4OdtDic[7]  = { 0xFFFF, 240, 120, 80, 60, 48, 40 };
  static const UINT16         Lpddr4RonDic[6]  = { 240, 120, 80, 60, 48, 40 };             // MR3 PD-DS ; Note: Index starts from 1 (0 field values is RFU)
  DDR4_MODE_REGISTER_1_STRUCT Ddr4ModeRegister1;
  DDR4_MODE_REGISTER_2_STRUCT Ddr4ModeRegister2;
  DDR4_MODE_REGISTER_5_STRUCT Ddr4ModeRegister5;
  DDR5_MODE_REGISTER_5_TYPE   Ddr5ModeRegister5;
  DDR5_MODE_REGISTER_33_TYPE  Ddr5ModeRegister33;
  DDR5_MODE_REGISTER_34_TYPE  Ddr5ModeRegister34;
  DDR5_MODE_REGISTER_35_TYPE  Ddr5ModeRegister35;
  UINT8                       MaxLpddrIndex;
  UINT32                      Dimm;
  BOOLEAN                     Ddr4;
  BOOLEAN                     Ddr5;
  BOOLEAN                     Lpddr;
  BOOLEAN                     Lpddr5;
  UINT16                      LpddrMr;
  UINT16                      *MR;
  UINT16                      *DimmOptParamValues;
  TOdtValueDdr4               Ddr4OdtTableIndex;
  TOdtValueDqDdr5             Ddr5DqOdtTableIndex;
  TOdtValueLpddr              Lpddr4OdtTableIndex;
  UINT16                      Impedance = 0xFFFF; // Odt off
  UINT8                       MrIndex;
  UINT8                       MrNum;
  UINT8                       NumDimmOptParamVals;
  UINT16                      MrValue;

  Outputs     = &MrcData->Outputs;
  ChannelOut  = &Outputs->Controller[Controller].Channel[Channel];
  Debug       = &Outputs->Debug;
  DdrType     = Outputs->DdrType;
  Ddr4        = (DdrType == MRC_DDR_TYPE_DDR4);
  Ddr5        = (DdrType == MRC_DDR_TYPE_DDR5);
  Lpddr5      = (DdrType == MRC_DDR_TYPE_LPDDR5);
  Lpddr       = Outputs->Lpddr;
  Dimm        = RANK_TO_DIMM_NUMBER (Rank);
  NumDimmOptParamVals = 0;
  DimmOptParamValues  = NULL;

  if (Ddr5) {
    Status = GetDimmOptParamValues (MrcData, OptParam, &DimmOptParamValues, &NumDimmOptParamVals);
    if (Status != mrcSuccess || DimmOptParamValues == NULL) {
      MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "%s: DimmOptParamValues is NULL!\n", gErrString);
      return 0;
    }
  }

  if (GetFromTable) {
    if (Ddr4) {
      Status = GetOdtTableIndex (MrcData, Controller, Channel, Dimm, &Ddr4OdtTableIndex);
      if (Status != mrcSuccess) {
        MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "%s: OdtTableIndex is NULL!\n", gErrString);
        return 0;
      }
      if (OptParam == OptDimmOdtWr) {
        Impedance = Ddr4OdtTableIndex.RttWr;
      } else if (OptParam == OptDimmOdtPark) {
        Impedance = (Ddr4OdtTableIndex.RttPark == 0) ? 0xFFFF : Ddr4OdtTableIndex.RttPark;
      } else if (OptParam == OptDimmOdtNom) {
        // Our convention is that ODT Write/Park is symmetric, ODT Nom is not (e.g 1R-2R vs. 2R-1R)
        Impedance = (Ddr4OdtTableIndex.RttNom == 0) ? 0xFFFF : Ddr4OdtTableIndex.RttNom;
      }
    } else if (Lpddr) {
      Status = GetOdtTableIndex (MrcData, Controller, Channel, Dimm, &Lpddr4OdtTableIndex);
      if (Status != mrcSuccess) {
        MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "%s: OdtTableIndex is NULL!\n", gErrString);
        return 0;
      }
      if (OptParam == OptDimmOdtCA) {
        Impedance = Lpddr4OdtTableIndex.RttCa;
      } else if (OptParam == OptDimmOdtWr) {
        Impedance = Lpddr4OdtTableIndex.RttWr;
      }
    } else { // Ddr5
      Status = GetOdtTableIndex (MrcData, Controller, Channel, Dimm, &Ddr5DqOdtTableIndex);
      if (Status != mrcSuccess) {
        MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "%s: OdtTableIndex is NULL!\n", gErrString);
        return 0;
      }
      if (OptParam == OptDimmOdtWr) {
        Impedance = Ddr5DqOdtTableIndex.RttWr;
      } else if (OptParam == OptDimmOdtNomWr) {
        Impedance = Ddr5DqOdtTableIndex.RttNomWr;
      } else if (OptParam == OptDimmOdtNomRd) {
        Impedance = Ddr5DqOdtTableIndex.RttNomRd;
      } else if (OptParam == OptDimmOdtPark) {
        Impedance = Ddr5DqOdtTableIndex.RttPark;
      }
    }
  } else { // else GetFromTable == FALSE
    MR = ChannelOut->Dimm[(Rank / 2) % MAX_DIMMS_IN_CHANNEL].Rank[Rank % 2].MR;
    GetOptDimmParamMrIndex (MrcData, OptParam, &MrIndex, &MrNum);
    MrValue = MR[MrIndex];

    switch (OptParam) {
      case OptDimmOdtWr:
        if (Ddr4) {
          Ddr4ModeRegister2.Data = MrValue;
          Impedance = Override ? OverrideValue : Ddr4RttWrDic[Ddr4ModeRegister2.Bits.DynamicOdt];
        } else if (Lpddr) {
          LpddrMr = MrValue & 0x7;
          MaxLpddrIndex = ARRAY_COUNT (Lpddr4OdtDic) - 1;
          Impedance = Override ? OverrideValue : Lpddr4OdtDic[RANGE (LpddrMr, 0, MaxLpddrIndex)];
        } else if (Ddr5) {
          Ddr5ModeRegister34.Data8 = (UINT8) MrValue;
          Impedance = Override ? OverrideValue : DimmOptParamValues[RANGE (Ddr5ModeRegister34.Bits.RttWr, 0, NumDimmOptParamVals)];
        }
        break;
      case OptDimmOdtNom:
        if (Ddr4) {
          Ddr4ModeRegister1.Data = MrValue;
          Impedance = Override ? OverrideValue : Ddr4RttNomDic[Ddr4ModeRegister1.Bits.OdtRttValue];
        }
        break;
      case OptDimmOdtNomWr:
        if (Ddr5) {
          Ddr5ModeRegister35.Data8 = (UINT8) MrValue;
          Impedance = Override ? OverrideValue : DimmOptParamValues[RANGE (Ddr5ModeRegister35.Bits.RttNomWr, 0, NumDimmOptParamVals)];
        }
        break;
      case OptDimmOdtNomRd:
        if (Ddr5) {
          Ddr5ModeRegister35.Data8 = (UINT8) MrValue;
          Impedance = Override ? OverrideValue : DimmOptParamValues[RANGE (Ddr5ModeRegister35.Bits.RttNomRd, 0, NumDimmOptParamVals)];
        }
        break;
      case OptDimmOdtPark:
        if (Ddr4) {
          Ddr4ModeRegister5.Data = MrValue;
          Impedance = Override ? OverrideValue : Ddr4RttNomDic[Ddr4ModeRegister5.Bits.RttPark];
        } else if (Ddr5) {
          Ddr5ModeRegister34.Data8 = (UINT8) MrValue;
          Impedance = Override ? OverrideValue : DimmOptParamValues[RANGE (Ddr5ModeRegister34.Bits.RttPark, 0, NumDimmOptParamVals)];
        }
        break;
      case OptDimmRon:
        if (Ddr4) {
          Ddr4ModeRegister1.Data = MrValue;
          Impedance = Override ? OverrideValue : Ddr4RonDic[Ddr4ModeRegister1.Bits.ODImpedance];
        } else if (Lpddr) {
          // LP5 ROn is MR3 OP[2:0] -> Shift MrValue by 0
          // LP4 ROn is MR3 OP[5:3] -> Shift MrValue by 3
          LpddrMr = (MrValue >> (Lpddr5 ? 0 : 3)) & 0x7;
          MaxLpddrIndex = ARRAY_COUNT(Lpddr4RonDic) - 1;
          Impedance = Override ? OverrideValue : Lpddr4RonDic[RANGE(LpddrMr - 1, 0, MaxLpddrIndex)];
        } else if (Ddr5) {
         Ddr5ModeRegister5.Data8 = (UINT8) MrValue;
         Impedance = Override ? OverrideValue : DimmOptParamValues[RANGE (Ddr5ModeRegister5.Bits.PullUpOutputDriverImpedance, 0, NumDimmOptParamVals)];
        }
        break;
      case OptDimmOdtCA:
        if (Lpddr) {
          LpddrMr = (MrValue >> 4) & 0x7;
          MaxLpddrIndex = ARRAY_COUNT (Lpddr4OdtDic) - 1;
          Impedance = Override ? OverrideValue : Lpddr4OdtDic[RANGE(LpddrMr, 0, MaxLpddrIndex)];
        } else if (Ddr5) {
          Ddr5ModeRegister33.Data8 = (UINT8) MrValue;
          Impedance = Override ? OverrideValue : DimmOptParamValues[RANGE (Ddr5ModeRegister33.Bits.CaOdt, 0, NumDimmOptParamVals)];
        }
        break;
      default:
        break;
    } // switch OptParam
  } //end else GetFromTable

  if ((OptParam == OptDimmOdtCA) && Lpddr) {
    Impedance /= 2;
  }

  return Impedance;
}

/**
  Check the selected param to see if it supports FIVR power measurement

  @param[in] MrcData     - Include all MRC global data.
  @param[in] Param       - Param to check for FIVR power measurement support

  @retval BOOLEAN If the param supports FIVR power measurement
**/
extern
BOOLEAN
SupportsFivrPower (
  IN MrcParameters   *MrcData,
  IN const UINT8     Param
  )
{
  switch (Param) {
    case OptDimmRon:
    case OptDimmOdtWr:
    case OptDimmOdtNom:
    case OptDimmOdtPark:
    case OptDimmOdtParkNT:
    case OptDimmOdtNomNT:
    case OptDimmOdtCA:
      return FALSE;
    default:
      return TRUE;
  }
}

/**
  This function returns the Actual Cpu Impedance in Ohms for given OptParam.
  The values will be taken from the host structure, unless override is used.

  @param[in] MrcData        - Pointer to MRC global data.
  @param[in] Controller     - Controller to work on.
  @param[in] Channel        - channel to work on.
  @param[in] Rank           - rank to work on.
  @param[in] Byte           - byte to work on.
  @param[in] OptParam       - param to read
  @param[in] Override       - override enable to verf and offset
  @param[in] OffsetOverride - to override host read value
  @param[in] CompOverride   - override enable
  @param[in] VrefOverride   - value to override
  @param[in] CompCode       - value to override

  @retval Returns the DIMM driver impedance value in Ohms
**/
UINT16
CalcCpuImpedance (
  IN MrcParameters *const MrcData,
  IN UINT32               Controller,
  IN UINT8                Channel,
  IN UINT8                Rank,
  IN UINT8                Byte,
  IN UINT8                OptParam,
  IN BOOLEAN              Override,
  IN INT8                 OffsetOverride,
  IN BOOLEAN              CompOverride,
  IN INT8                 VrefOverride,
  IN UINT8                CompCode
  )
{
  const MrcInput *Inputs;
  MrcOutput      *Outputs;
  MrcChannelOut  *ChannelOut;
  UINT32         Result;
  UINT16         Rext;
  UINT8          StepSize;
  UINT8          LegsPerSeg;
  UINT8          ODTSingleSegEn;
  UINT32         Rleg;
  UINT8          Segments;
  UINT8          TermFactor;
  UINT16         Divider;
  UINT8          OdtLegsOn;
  UINT8          CurrentComp;
  INT8           VrefOffset;
  INT16          DigiOffset;
  INT64          GetSetVal;
  //UINT8          UnsignedData8;
  UINT32         Numerator;
  UINT32         Denomenator;
  UINT16         VddVoltage;
  DATA0CH0_CR_DDRCRDATAOFFSETCOMP_STRUCT DdrCrDataOffsetComp;

  Inputs      = &MrcData->Inputs;
  Outputs     = &MrcData->Outputs;
  ChannelOut  = &Outputs->Controller[Controller].Channel[Channel];
  VddVoltage  = Outputs->VddVoltage[Inputs->MemoryProfile];
  Numerator   = 0;
  Denomenator = 0;
  DdrCrDataOffsetComp.Data = ChannelOut->DataCompOffset[Byte];
  ODTSingleSegEn = 1; // TGL_POWER_TRAINING Add once DDRDATA_CR_DDRCrDataControl5_0_0_0_MCHBAR.ODTSingleSegEn is added to the BIOS cache: (Outputs->ODTSingleSegEn == 1);

  if (CompOverride) {
    VrefOffset = VrefOverride;
    CurrentComp = CompCode;
  } else {
    CurrentComp = GetCompCode (MrcData, OptParam, FALSE); // Get comp code up
    if (OptParam == OptRdOdt || OptParam == OptRdDqOdt) {
      MrcGetSetNoScope (MrcData, DqOdtVrefUp, ReadFromCache, &GetSetVal);
      VrefOffset = (INT8) GetSetVal;
    } /*else if (OptParam == OptWrDS || OptParam == OptTxEq) {
      MrcGetSetNoScope (MrcData, DqDrvVrefUp, ReadFromCache, &GetSetVal);
      VrefOffset = (INT8) GetSetVal;
    }*/ else {
      VrefOffset = 0;
    }
  }

  OdtLegsOn = 1;
  StepSize = MRC_COMP_VREF_STEP_SIZE;

  if (OptParam == OptRdOdt || OptParam == OptRdDqOdt) {
    LegsPerSeg = 8;   // Number of Static legs per segment TGL_POWER_TRAINING - Might change for TGL
    if (Outputs->OdtMode == MrcOdtModeVtt) {
      Segments    = ODTSingleSegEn + 1; // in Vtt mode the segment(s) are compensated to the actual ext. resistor
      TermFactor  = 2; // Terminated to VDD/2
    } else {
      Segments    = 2 * (ODTSingleSegEn + 1); // in Odt mode we have up to 2 segments but we treat up & dn as 2 seperate segments
      TermFactor  = 1; // Terminated to VDD
    }
    MrcGetSetNoScope (MrcData, GsmIocCompOdtStaticDis, ReadFromCache, &GetSetVal);
    OdtLegsOn = GetSetVal ? 0 : 1;
    if (Override) {
      DigiOffset = OffsetOverride;
    } else {
      DigiOffset = (INT8) MrcSE ((UINT8) DdrCrDataOffsetComp.Bits.dqodtcompoffset, DATA0CH0_CR_DDRCRDATAOFFSETCOMP_dqodtcompoffset_WID, 8);
    }
  }/* else if ((OptParam == OptWrDS) || (OptParam == OptTxEq)) {
    Segments = 2;
    TermFactor = 1; // @todo: LowSupplyEn calculation factor
    // TxEq full drive bits [4:5]
    MrcGetSetStrobe (MrcData, Controller, Channel, Rank, Byte, TxEq, ReadFromCache, &GetSetVal);
    UnsignedData8 = (UINT8) GetSetVal;
    LegsPerSeg = ((0x3 & (UnsignedData8 >> 4)) + 2 ) * Segments;
    if (Override) {
      DigiOffset = OffsetOverride;
    } else { // from host
      if (OptParam == OptWrDS) {
        DigiOffset = (INT8) MrcSE ((UINT8) DdrCrDataOffsetComp.Bits.DqDrvUpCompOffset, DATA0CH0_CR_DDRCRDATAOFFSETCOMP_DqDrvUpCompOffset_WID, 8);
      } else { // OptTxEq
        DigiOffset =  (Inputs->UlxUlt) ? (((0xF & (UnsignedData8)) * 2 + 2) / 3) : (0xF & (UnsignedData8));// in ulx num legs 0-10
      }
    }
  }*/ else {
    return 0;
  }

  // TermFactor is always non-zero.  VrefOffset can be zero.
  // Protect by divide by zero here.
  if (VrefOffset == 0) {
    return 0;
  } else {
    Rext = Inputs->RcompResistor;
    // Calc global (Comp) value - Normalize according to LowSupEn
    if (((Outputs->DdrType == MRC_DDR_TYPE_LPDDR4) || (Outputs->DdrType == MRC_DDR_TYPE_LPDDR5)) && (Inputs->LowSupplyEnData)) {
      Numerator = Rext * (StepSize * Inputs->VccIomV - VddVoltage * VrefOffset * TermFactor);
      Denomenator = (VddVoltage * VrefOffset * TermFactor);
    } else  {
      Numerator = Rext * (StepSize - (VrefOffset * TermFactor));
      Denomenator = (VrefOffset * TermFactor);
    }
    Denomenator = MAX (Denomenator, 1);
    Result = (UINT16) (Numerator / Denomenator);
    Rleg = Result * (OdtLegsOn * (LegsPerSeg * 10) + CurrentComp * 10);
  }

  /*if (OptParam == OptTxEq) { // we calc here the tx relative to global drv value
    DigiOffset = RatioR2r * DigiOffset; // total number of static legs
    if (DigiOffset == 0) {
      Result = 0xFFFF; // Infinite resistance
    } else {
      Result = DIVIDEROUND (Rleg, DigiOffset); // result is for entire buffer
    }
  } else {*/
    Divider = OdtLegsOn * (LegsPerSeg * 10)  + 10 * (CurrentComp + DigiOffset);
    Divider = MAX (Divider, 1);
    Segments = MAX (Segments, 1);
    Result = Rleg / (Divider * Segments);
  //}
  return (UINT16) Result;
}

/**
  Calculate Power for the selected Opt param based on

  @param[in] MrcData     - Include all MRC global data.
  @param[in] Power       - @todo
  @param[in] Rank        - Rank to work on
  @param[in] OptParam    - Array of parameters to work on.
  @param[in] Offset      - Array of Offsets to work on.
  @param[in] OptParamLen - Length of OptParam[] and Offset[].
  @param[in] CurrentComp - The current Comp code for OptParam
  @param[in] ReadHost    - Switch to read current offset and CompCode from Host structure.
  @param[in] Scale       - @todo

  @retval Calc power in mW
**/
MrcStatus
CalcOptPower (
  IN MrcParameters   *MrcData,
  IN MrcPower        *Power,
  IN UINT8           Rank,
  IN const UINT8     *OptParam,
  IN INT8            *Offset,
  IN UINT8           OptParamLen,
  IN INT8            CurrentComp,
  IN UINT8           Scale
  )
{
  //const MrcInput        *Inputs;
  MrcOutput             *Outputs;
  //MrcIntOutput          *IntOutputs;
  MrcChannelOut         *ChannelOut;
  //static const UINT16   RxPowerConstuW[][2] = {{1288,900}, {1525, 1105}, {1800, 1180}}; // [Vddq current/Vccio Current][ULX/ULT/ULT(DDR3)]  - 100x [mA]  Per Byte
  //static const UINT16   RxPowerScaleSKL[] = {125, 250, 375, 500, 625, 750, 875, 1000, 1125, 1250, 1375, 1500, 1625, 1750, 1875, 2000};
  //UINT8                 RxDefault;
  //UINT32                CPURXPower;
  //UINT16                TxParamVector[MaxTxPowerParam];
  //UINT16                RxParamVector[MaxRxPowerParam];
  BOOLEAN               Override;
  UINT8                 Idx;
  UINT8                 Channel;
  UINT8                 Byte;
  UINT8                 ChBitMask = 0;
  UINT8                 Iterator;
  //MRC_POWER_SYS_CONFIG  SysConfig;
  //DimmRttWr             DimmWrOdt;
  INT8                  LocalOffset;
  //UINT16                BufferStageDelayPS;
  UINT8                 ScaleRdPwr;
  UINT8                 ScaleWrPwr;
  UINT32                FivrPInValueStart;
  UINT32                FivrPInCountStart;
  UINT32                FivrPInValueEnd;
  UINT32                FivrPInCountEnd;
  UINT32                MaxPOutValue;
  UINT32                MaxPOutUnits;
  //UINT16                DimmRon;
  //BOOLEAN               Ddr4;
  //UINT32                VccIO;
  //UINT32                Vdd;
  //UINT8                 BiasConfig;
  INT64                 GetSetVal = 0x0;
  //UINT8                 NumStages;
  //UINT8                 SlewRateCompCells;
  //DATA0CH0_CR_DDRCRDATAOFFSETCOMP_STRUCT DdrCrDataOffsetComp;

  //Inputs                   = &MrcData->Inputs;
  Outputs                  = &MrcData->Outputs;
  //IntOutputs               = (MrcIntOutput *) (MrcData->IntOutputs.Internal);
  //ChannelOut               = &Outputs->Controller[0].Channel[Channel];
  //Ddr4                     = (Outputs->DdrType == MRC_DDR_TYPE_DDR4);
  //VccIO                    = Inputs->VccIomV;
  //Vdd                      = Outputs->VddVoltage[Inputs->MemoryProfile];

  Power->RdPower = 0;
  Power->WrPower = 0;
  Power->TotalPwr = 0;

  // For PanicVttDnLp, don't calculate power; instead, find the number of [panic high + (panic high - panic lo)] panic events that occur (and minimize it while making sure [panic high - panic lo > 0])
  Idx = MrcSearchList (OptPanicVttDnLp, OptParam, OptParamLen, Offset, &Override, &LocalOffset);
  if (Idx != 0) {
    for (Channel = 0; Channel < MAX_CHANNEL; Channel++) {
      if (!((0x1 << Channel) & MrcData->Outputs.ValidChBitMask)) {
        continue;
      }
      ChannelOut = &Outputs->Controller[0].Channel[Channel];
      // no need to run on channel with no selected ranks
      if (!(ChannelOut->ValidRankBitMask & (0x1 << Rank))) {
        continue;
      }
      ChBitMask |= 0x1 << Channel;
      for (Byte = 0; Byte < Outputs->SdramCount; Byte++) {
        if (!MrcByteInChannelExist (MrcData, Channel, Byte)) {
          continue;
        }

        GetSetVal = 0;
        // TGL_POWER_TRAINING DDRVtt_CR_DDRCrVttGenStatus.SelCount
        // MrcGetSetDdrIoGroupChannelStrobe (MrcData, Channel, Byte, VttGenStatusSelCount, WriteCached, &GetSetVal);
      }
    }

    // Run CPGC
    RunIOTest (MrcData, ChBitMask, Outputs->DQPat, 1, 0);

    for (Channel = 0; Channel < MAX_CHANNEL; Channel++) {
      if (!((0x1 << Channel) & MrcData->Outputs.ValidChBitMask)) {
        continue;
      }
      ChannelOut = &Outputs->Controller[0].Channel[Channel];
      // no need to run on channel with no selected ranks
      if (!(ChannelOut->ValidRankBitMask & (0x1 << Rank))) {
        continue;
      }
      ChBitMask |= 0x1 << Channel;
      for (Byte = 0; Byte < Outputs->SdramCount; Byte++) {
        if (!MrcByteInChannelExist (MrcData, Channel, Byte)) {
          continue;
        }

        // TGL_POWER_TRAINING DDRVtt_CR_DDRCrVttGenStatus.Count
        // MrcGetSetDdrIoGroupChannelStrobe (MrcData, Channel, Byte, VttGenStatusCount, ReadUncached, &GetSetVal);
        Power->RdPower += (UINT16)(GetSetVal & 0x7FFF); // Use the absolute value of the 16 bit signed error count, so cut off the MSB sign bit

        GetSetVal = 5;
        // TGL_POWER_TRAINING DDRVtt_CR_DDRCrVttGenStatus.SelCount
        // MrcGetSetDdrIoGroupChannelStrobe (MrcData, Channel, Byte, VttGenStatusSelCount, WriteCached, &GetSetVal);
      }
    }

    // Run CPGC
    RunIOTest (MrcData, ChBitMask, Outputs->DQPat, 1, 0);

    for (Channel = 0; Channel < MAX_CHANNEL; Channel++) {
      if (!((0x1 << Channel) & MrcData->Outputs.ValidChBitMask)) {
        continue;
      }
      ChannelOut = &Outputs->Controller[0].Channel[Channel];
      // no need to run on channel with no selected ranks
      if (!(ChannelOut->ValidRankBitMask & (0x1 << Rank))) {
        continue;
      }
      ChBitMask |= 0x1 << Channel;
      for (Byte = 0; Byte < Outputs->SdramCount; Byte++) {
        if (!MrcByteInChannelExist (MrcData, Channel, Byte)) {
          continue;
        }

        // TGL_POWER_TRAINING DDRVtt_CR_DDRCrVttGenStatus.Count
        // MrcGetSetDdrIoGroupChannelStrobe (MrcData, Channel, Byte, VttGenStatusCount, ReadUncached, &GetSetVal);
        if (GetSetVal < 0) {
          GetSetVal = 0x7FFF; // Set panic count to maximum if SelCount=5 gives a negative result, so that this PanicVttDnLp value is not chosen.
        }
        Power->RdPower += (UINT16)GetSetVal;
      }
    }

    Power->WrPower = 0;
    Power->TotalPwr = 0;
    return mrcSuccess;
  }

  //DdrCrDataOffsetComp.Data = ChannelOut->DataCompOffset[Byte];
  ScaleRdPwr = 60;
  ScaleWrPwr = 40;

  /*
  // Tx
  TxParamVector[MbLengthTx] = 1000; // @todo: Mrc inputs [mil's]
  // keep within the simulation range
  TxParamVector[MbLengthTx] = RANGE (TxParamVector[MbLengthTx], 800, 2800);

  TxParamVector[SoDimmLenTx] = 200;  // @todo: Mrc inputs [mil's]
  // keep within the simulation range
  TxParamVector[SoDimmLenTx] = RANGE (TxParamVector[SoDimmLenTx], 50, 900);

  Idx = MrcSearchList (OptWrDS, OptParam, OptParamLen, Offset, &Override, &LocalOffset);
  MrcGetSetDdrIoGroupController0 (MrcData, DqDrvVrefUp, ReadFromCache, &GetSetVal);
  TxParamVector[CpuRon]  = CalcCpuImpedance (
                          MrcData,
                          cCONTROLLER0, // @todo - 2xMC
                          Channel,
                          Rank,
                          Byte,
                          OptWrDS,
                          Override,
                          LocalOffset,
                          Override,
                          (UINT8) GetSetVal,
                          CurrentComp
                          );
  // keep within the simulation range
  TxParamVector[CpuRon] = RANGE (TxParamVector[CpuRon], 35, 65);

  Idx = MrcSearchList (OptSComp, OptParam, OptParamLen, Offset, &Override, &LocalOffset);
  if (Override) {
    LocalOffset = Offset[Idx - 1];
  } else {
    LocalOffset = (INT8) MrcSE ((UINT8) DdrCrDataOffsetComp.Bits.DqSlewRateCompOffset, DATA0CH0_CR_DDRCRDATAOFFSETCOMP_DqSlewRateCompOffset_WID, 8);
  }

  // read global cell delay for Tx
  MrcGetSetDdrIoGroupController0 (MrcData, TxSlewRate, ReadFromCache, &GetSetVal);
  SlewRateCompCells = (UINT8) GetSetVal;
  MrcGetSetDdrIoGroupController0 (MrcData, DqScompPC, ReadFromCache, &GetSetVal);
  NumStages = (GetSetVal == ScompTypePhase) ? 2 * (SlewRateCompCells + 1) : (SlewRateCompCells + 1);

  BufferStageDelayPS = DIVIDEROUND (Outputs->Qclkps, NumStages);
  TxParamVector[CpuCellDelay] = BufferStageDelayPS * ((UINT16) MrcPercentPower ((LocalOffset < 0) ? 95 : 105, ABS (LocalOffset))) / 100; // simple linear T-line
  // keep within the simulation range
  TxParamVector[CpuCellDelay] = RANGE (TxParamVector[CpuCellDelay], 10, 100);

  Idx = MrcSearchList (OptTxEq, OptParam, OptParamLen, Offset, &Override, &LocalOffset);
  MrcGetSetDdrIoGroupController0 (MrcData, DqDrvVrefUp, ReadFromCache, &GetSetVal);
  TxParamVector[CpuTxEq]  = CalcCpuImpedance (
                            MrcData,
                            cCONTROLLER0, // @todo - 2xMC
                            Channel,
                            Rank,
                            Byte,
                            OptTxEq,
                            Override,
                            LocalOffset,
                            Override,
                            (UINT8) GetSetVal,
                            CurrentComp
                            );
  // x2 to match simulation input for the RSM calc. and clip to range [100 - 700]
  TxParamVector[CpuTxEq] *= 2;
  TxParamVector[CpuTxEq] = RANGE (TxParamVector[CpuTxEq], 100, 700);

  if (Ddr4) {
    Idx = MrcSearchList (DimmWrOdtNT, OptParam, OptParamLen, Offset, &Override, &LocalOffset);
    // @todo - 2xMC
    TxParamVector[DimmWrOdtNT]  = MrcGetEffDimmWriteOdt (MrcData, cCONTROLLER0, Channel, Rank, 1, FALSE);
    // keep within the simulation range
    TxParamVector[DimmWrOdtNT] = RANGE (TxParamVector[DimmWrOdtNT], 100, 500);
  } else {
    // in LPDDR4 we use entry [3] for Mb_len
    TxParamVector[DimmWrOdtNT] = TxParamVector[MbLengthTx];
  }

  Idx = MrcSearchList (OptDimmOdtWr, OptParam, OptParamLen, Offset, &Override, &LocalOffset);
  // @todo - 2xMC
  DimmWrOdt = CalcDimmImpedance (MrcData, cCONTROLLER0, Channel, Rank, OptDimmOdtWr, Override, LocalOffset, FALSE);
  if (DimmWrOdt == 0xFFFF) {// DDR4 HiZ
    DimmWrOdt = 0;
  } else if (DimmWrOdt == 0) {// when dynamic odt off the nom is picked
    // @todo - 2xMC
    DimmWrOdt = CalcDimmImpedance (MrcData, cCONTROLLER0,  Channel, Rank, OptDimmOdtNom, Override, LocalOffset, FALSE);
  }


  // Rx
  RxParamVector[MbLengthRx]   = 1000; //@todo: Mrc inputs [mil's]
  // keep within the simulation range
  RxParamVector[MbLengthRx]   = RANGE (RxParamVector[MbLengthRx], 800, 2800);
  RxParamVector[SoDimmLenRx]  = 200;  //@todo: Mrc inputs [mil's]
  // keep within the simulation range
  RxParamVector[SoDimmLenRx]  = RANGE (RxParamVector[SoDimmLenRx], 50, 900);

  Idx = MrcSearchList (OptRdOdt, OptParam, OptParamLen, Offset, &Override, &LocalOffset);
  MrcGetSetDdrIoGroupController0 (MrcData, DqDrvVrefUp, ReadFromCache, &GetSetVal);
  RxParamVector[CpuOdt]  = CalcCpuImpedance (
                            MrcData,
                            cCONTROLLER0, // @todo - 2xMC
                            Channel,
                            Rank,
                            Byte,
                            OptRdOdt,
                            Override,
                            LocalOffset,
                            Override,
                            (UINT8) GetSetVal,
                            CurrentComp
                            );
  // keep within the simulation range
  RxParamVector[CpuOdt] = RANGE (RxParamVector[CpuOdt], 60, 500);

  Idx = MrcSearchList (DimmRdOdtNT, OptParam, OptParamLen, Offset, &Override, &LocalOffset);
  // @todo - 2xMC
  RxParamVector[DimmRdOdtNT]  = MrcGetEffDimmWriteOdt (MrcData, cCONTROLLER0, Channel, Rank, 1, FALSE);
  // keep within the simulation range
  RxParamVector[DimmRdOdtNT] = RANGE (RxParamVector[DimmRdOdtNT], 60, 500);

  Idx = MrcSearchList (OptDimmRon, OptParam, OptParamLen, Offset, &Override, &LocalOffset);
  // @todo - 2xMC
  DimmRon = CalcDimmImpedance (MrcData, cCONTROLLER0, Channel, Rank, OptDimmRon, Override, LocalOffset, FALSE);


  Idx = MrcSearchList (OptRxBias, OptParam, OptParamLen, Offset, &Override, &LocalOffset);
  // RX BIAS calculations
  if (VccIO < 900) {
    // LPDDR3 UlX - 0.85v
    BiasConfig = 0;
    RxDefault  = 8;
  } else {
    // LPDDR3 Ult - 0.95v
    BiasConfig = 1;
    RxDefault  = 8;
  }

  CPURXPower = Vdd * RxPowerConstuW[BiasConfig][0] / 100 + VccIO * RxPowerConstuW[BiasConfig][1] / 100;

  if (Override) {
    LocalOffset = (UINT8) Offset[Idx - 1];
    LocalOffset = RANGE (LocalOffset, 0, 15);
  } else {
    // TGL_POWER_TRAINING - This register no longer seems to exist in TGL. When we redo power measurement, may need to look at this.
    // MrcGetSetDdrIoGroupChannelStrobe (MrcData, Channel, Byte, RxBiasCtl, ReadFromCache, &GetSetVal);
    LocalOffset = (UINT8) GetSetVal;
  }

  CPURXPower  = (RxPowerScaleSKL[(UINT8) LocalOffset] * CPURXPower) / RxPowerScaleSKL[RxDefault];
  CPURXPower /= 1000; // div by 1000 to get mW (result is per byte)
  Power->RdPower += (UINT16) CPURXPower * Scale;

  // Set the RSM configuration for Tx
  SysConfig.Data = 0;
  SysConfig.Bits.DdrType = Outputs->DdrType;
  SysConfig.Bits.Frequency = Outputs->Frequency;
  SysConfig.Bits.RttWr = DimmWrOdt;
  //MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "DimmWrOdt: %d ch %d  rank %d\n", DimmWrOdt, Channel, Rank);
  GetPowerTable (MrcData, &SysConfig, &(IntOutputs->SysTxPowerFormula),  RSMPowerCalcTableTx, sizeof (RSMPowerCalcTableTx) / sizeof (MRC_RSM_POWER_FORMULA));
  // calc Tx Power
  Power->WrPower += (UINT16) CalcPowerRSM (
                            MrcData,
                            IntOutputs->SysTxPowerFormula,
                            TxParamVector,
                            Ddr4 ? 6 : 4,
                            Scale
                            );

  // Set the RSM configuration for Rx
  SysConfig.Data = 0;
  SysConfig.Bits.DdrType = MRC_DDR_TYPE_DDR4; // we currently have only DDR4 equ. Outputs->DdrType;
  SysConfig.Bits.Frequency = f1333; // we currently have only 1333 equ. Outputs->Frequency;
  SysConfig.Bits.RttWr = DimmRon; //  we currently have only 34 and 48 values
  //MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "DimmWrOdt: %d ch %d  rank %d\n", DimmWrOdt, Channel, Rank);
  GetPowerTable (MrcData, &SysConfig, &(IntOutputs->SysRxPowerFormula),  RSMPowerCalcTableRx, sizeof (RSMPowerCalcTableRx) / sizeof (MRC_RSM_POWER_FORMULA));

  // calc Rx Power
  Power->RdPower += (UINT16) CalcPowerRSM (
                            MrcData,
                            IntOutputs->SysRxPowerFormula,
                            RxParamVector,
                            4,
                            Scale
                            );
  */

  for (Channel = 0; Channel < MAX_CHANNEL; Channel++) {
    if (!((0x1 << Channel) & MrcData->Outputs.ValidChBitMask)) {
      continue;
    }
    ChannelOut = &Outputs->Controller[0].Channel[Channel];
    // no need to run on channel with no selected ranks
    if (!(ChannelOut->ValidRankBitMask & (0x1 << Rank))) {
      continue;
    }
    ChBitMask |= 0x1 << Channel;
  }

  if (ChBitMask == 0x0) {
    return mrcSuccess; // Nothing to do here
  }

  do {
    // TGL_POWER_TRAINING - Read initial FIVR input power accumulator from P_IN_ACCUMULATOR and sample count from P_IN_NUM_SAMPLES_SNAPSHOT
    // MrcGetSetDdrIoGroupController0 (MrcData, FivrPInValue, ReadUncached, &GetSetVal);
    FivrPInValueStart = (UINT16)GetSetVal;
    // MrcGetSetDdrIoGroupController0 (MrcData, FivrPInCount, ReadUncached, &GetSetVal);
    FivrPInCountStart = (UINT16)GetSetVal;

    SetupIOTestBasicVA (MrcData, ChBitMask, OPT_PARAM_LOOP_COUNT, NSOE, 0x4, 0, 8, PatWrRd, 0, 0); // Read-only test all channels. TGL_POWER_TRAINING - LC may need to be optimized
    RunIOTest (MrcData, ChBitMask, Outputs->DQPat, 1, 0); // This is a standard point test

    // TGL_POWER_TRAINING - Read final FIVR input power accumulator from P_IN_ACCUMULATOR and sample count from P_IN_NUM_SAMPLES_SNAPSHOT
    // MrcGetSetDdrIoGroupController0 (MrcData, FivrPInValue, ReadUncached, &GetSetVal);
    FivrPInValueEnd = (UINT16)GetSetVal;
    // MrcGetSetDdrIoGroupController0 (MrcData, FivrPInCount, ReadUncached, &GetSetVal);
    FivrPInCountEnd = (UINT16)GetSetVal;
  } while (FivrPInCountEnd < FivrPInCountStart); // If this is not true, then we overflowed

  FivrPInValueEnd = FivrPInValueEnd - FivrPInValueStart;
  FivrPInCountEnd = FivrPInCountEnd - FivrPInCountStart;
  // TGL_POWER_TRAINING - Result must be in mW, so we have to adjust for this by finding out what the units are in FIVR. Units are (VR_MAX.MAX_P_OUT * 2^VR_MAX.P_OUT_EXP / 255) mW
  // MrcGetSetDdrIoGroupController0 (MrcData, MaxPOutValue, ReadUncached, &GetSetVal);
  MaxPOutValue = (UINT32)GetSetVal;
  // MrcGetSetDdrIoGroupController0 (MrcData, MaxPOutExp, ReadUncached, &GetSetVal);
  MaxPOutUnits = 1;
  for (Iterator = 0; Iterator < GetSetVal; Iterator++) {
    MaxPOutUnits *= 2;
  }
  MaxPOutValue *= MaxPOutUnits;
//#ifdef LOCAL_STUB_FLAG
  FivrPInValueEnd = 12750;
  MaxPOutValue = 1000;
  FivrPInCountEnd = 100;
//#endif
  Power->RdPower = (UINT16)((FivrPInValueEnd * MaxPOutValue) / (FivrPInCountEnd * 255));

  do {
    // TGL_POWER_TRAINING - Read initial FIVR input power accumulator from P_IN_ACCUMULATOR and sample count from P_IN_NUM_SAMPLES_SNAPSHOT
    // MrcGetSetDdrIoGroupController0 (MrcData, FivrPInValue, ReadUncached, &GetSetVal);
    FivrPInValueStart = (UINT16)GetSetVal;
    // MrcGetSetDdrIoGroupController0 (MrcData, FivrPInCount, ReadUncached, &GetSetVal);
    FivrPInCountStart = (UINT16)GetSetVal;

    SetupIOTestBasicVA (MrcData, ChBitMask, OPT_PARAM_LOOP_COUNT, NSOE, 0x2, 0, 8, PatWrRd, 0, 0); // Write-only test all channels. TGL_POWER_TRAINING - LC may need to be optimized
    RunIOTest (MrcData, ChBitMask, Outputs->DQPat, 1, 0); // This is a standard point test

    // TGL_POWER_TRAINING - Read final FIVR input power accumulator from P_IN_ACCUMULATOR and sample count from P_IN_NUM_SAMPLES_SNAPSHOT
    // MrcGetSetDdrIoGroupController0 (MrcData, FivrPInValue, ReadUncached, &GetSetVal);
    FivrPInValueEnd = (UINT16)GetSetVal;
    // MrcGetSetDdrIoGroupController0 (MrcData, FivrPInCount, ReadUncached, &GetSetVal);
    FivrPInCountEnd = (UINT16)GetSetVal;
  } while (FivrPInCountEnd < FivrPInCountStart); // If this is not true, then we overflowed

  FivrPInValueEnd = FivrPInValueEnd - FivrPInValueStart;
  FivrPInCountEnd = FivrPInCountEnd - FivrPInCountStart;
//#ifdef LOCAL_STUB_FLAG
  FivrPInValueEnd = 12750;
  MaxPOutValue = 1000;
  FivrPInCountEnd = 100;
//#endif
  Power->WrPower = (UINT16)((FivrPInValueEnd * MaxPOutValue) / (FivrPInCountEnd * 255));

  //MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Power->RdPower : %d Power->WrPower: %d ParamVector: %d ParamVector: %d ParamVector: %d ParamVector: %d \n", Power->RdPower, Power->WrPower, ParamVector[0], ParamVector[1], ParamVector[2], ParamVector[3]);
  Power->TotalPwr = ((Power->RdPower * ScaleRdPwr ) + (Power->WrPower * ScaleWrPwr )) / 100; // [mW] when Scale=1
  //MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Power->TotalPwr %d\n", Power->TotalPwr);
  return mrcSuccess;
}

/**
Calculate linear normalized power

@param[in] MrcData        - Include all MRC global data.
@param[in] Param          - Param that linear normalized power should be estimated for
@param[in] ParamValue     - Param value that linear normalized power should be estimated for
@param[in] PowerDirection - If TRUE, increasing param values means increasing power. FALSE means increasing param values means decreasing power.

@retval Linear normalized power
**/
UINT16
CalcLinearNormalizedPower(
  IN MrcParameters   *MrcData,
  IN const UINT8     Param,
  IN INT8            ParamValue,
  IN BOOLEAN         PowerDirection
  )
{
  UINT16         *DimmOptParamVals;
  UINT8           NumDimmOptParamVals;
  UINT16          LinearNormalizedPower = 0;
  INT8            MinValue;
  INT8            MaxValue;

  switch (Param) {
    case OptDimmRon:
    case OptDimmOdtWr:
    case OptDimmOdtNom:
    case OptDimmOdtPark:
    case OptDimmOdtParkNT:
    case OptDimmOdtNomNT:
    case OptDimmOdtCA:
      GetDimmOptParamValues (MrcData, Param, &DimmOptParamVals, &NumDimmOptParamVals); // TGL_POWER_TRAINING_DDR5 - Check that the values are in order from most power to least power
      MinValue = 0;
      MaxValue = NumDimmOptParamVals - 1;
      break;
    default:
      MinValue = OptParamLimitValue (MrcData, Param, 0);
      MaxValue = OptParamLimitValue (MrcData, Param, 1);
  }


  if (Param == OptSComp || Param == OptCCCSComp) {
    MinValue -= 1; // Start one index earlier on slew rate training to account for needing a point to test slew rate being disabled
  }

  LinearNormalizedPower = 1 + (LINEAR_NORMALIZED_POWER_SCALE * ((PowerDirection == TRUE) ? (ParamValue - MinValue) : (MaxValue - ParamValue))) / (MaxValue - MinValue);

  return LinearNormalizedPower;
}

#if EYE_MARGIN_SUPPORT
/**
  This function prints out the Margin eye diagram for ParamT/ParamV.

  @param[in] MrcData - Include all MRC global data.
  @param[in] Channel - Channel to margin.
  @param[in] Ranks   - Bit mask of Ranks to margin.
  @param[in] ParamT  - Time parameter to margin.
  @param[in] ParamV  - Voltage parameter to margin.
  @param[in] CmdIteration - Whether doing CLK/CMD/CTL (only used with ParamT == CmdT and ParamV == CmdV)
  @param[in] CmdGroup     - Determines which CmdGrp to use (0 through 4, only used with CmdIteration == MrcIterationCmd)
  @param[in] Start   - Starting point for margining.
  @param[in] Stop    - Stopping point for margining.
  @param[in] Repeats - Number of times to repeat the test to average out any noise.
  @param[in] NoPrint - Switch to skip printing.

  @retval Nothing
**/
#define MRC_EM_MAX_H  (192)
#define MRC_EM_MAX_W  (128)
void
EyeMargin (
  IN MrcParameters *const MrcData,
  IN UINT8                Ranks,
  IN UINT8                ParamT,
  IN UINT8                ParamV,
  IN UINT8                CmdIteration,
  IN UINT8                CmdGroup,
  IN INT8                 Start,
  IN INT8                 Stop,
  IN UINT16               SearchLimits,
  IN UINT8                LoopCount,
  IN UINT8                Repeats
  )
{
  const MrcInput    *Inputs;
  MrcDebug          *Debug;
  const MRC_FUNCTION *MrcCall;
  MrcOutput         *Outputs;
  MrcChannelOut     *ChannelOut;
  MrcControllerOut  *ControllerOut;
  MrcIntOutput      *IntOutputs;
  MrcIntCmdTimingOut *IntCmdTiming;
  MrcStatus         Status;
  UINT32            (*MarginByte)[MAX_RESULT_TYPE][MAX_RANK_IN_CHANNEL][MAX_CHANNEL][MAX_SDRAM_IN_DIMM][MAX_EDGES];
  UINT32            BERStats[4];
  UINT16            SaveMargin[MRC_EM_MAX_W][MAX_CHANNEL][MAX_SDRAM_IN_DIMM][MAX_EDGES];
  BOOLEAN           Eye[MAX_CHANNEL][MRC_EM_MAX_W][MRC_EM_MAX_H];
  BOOLEAN           Lines[MAX_CHANNEL][MRC_EM_MAX_H];
  UINT32            Channel;
  UINT8             i,j;
  UINT16            MinEdge;
  UINT8             ResultTypeV = 0;
  UINT8             ChBitMask;
  UINT8             Byte;
  UINT8             Rank;
  UINT8             Edge;
  UINT8             FirstRank;
  UINT8             NumBytes;
  UINT8             BMap[9];  // Need by GetBERMarginByte
  UINT8             MaxMarginV;
  UINT8             localR[MAX_CHANNEL];
  UINT8             Rep;
  INT8              Index;
  UINT8             IndexOff;
  INT8              Off;
  UINT8             OffStep;
  UINT8             byteMax[MAX_CHANNEL];
  UINT8             sign;
  UINT8             SafeOff[MAX_CHANNEL]={0,0};
  UINT8             CurrValue;
  INT64             GetSetVal;
  BOOLEAN           Ddr4;
  BOOLEAN           Lpddr4;
  BOOLEAN           IsMrVref;
  Inputs        = &MrcData->Inputs;
  MrcCall       = Inputs->Call.Func;
  Outputs       = &MrcData->Outputs;
  Debug         = &Outputs->Debug;
  MarginByte    = &Outputs->MarginResult;
  ControllerOut = &Outputs->Controller[0];
  IndexOff      = 0;
  Ddr4          = (Outputs->DdrType == MRC_DDR_TYPE_DDR4);
  Lpddr4        = (Outputs->DdrType == MRC_DDR_TYPE_LPDDR4);
  IntOutputs    = (MrcIntOutput *) (MrcData->IntOutputs.Internal);

  MrcCall->MrcSetMem ((UINT8 *) localR, sizeof (localR), 0);
  MrcCall->MrcSetMem ((UINT8 *) Eye, sizeof (Eye), 0);
  MrcCall->MrcSetMem ((UINT8 *) Lines, sizeof (Lines), 0);
  MrcCall->MrcSetMem ((UINT8 *) SaveMargin, sizeof (SaveMargin), 0);
  MrcCall->MrcSetMemDword (BERStats, sizeof (BERStats) / sizeof (UINT32), 0);
  for (Byte = 0; Byte < ARRAY_COUNT (BMap); Byte++) {
    BMap[Byte] = Byte;
  }
  ResultTypeV = GetMarginResultType (ParamV);
  IsMrVref = (ParamV == WrV) || (Lpddr4 && (ParamV == CmdV));

  if ((ParamT == CmdT) | (ParamT == CmdV)) {
    // @todo Update with McChBitMask
    SetupIOTestCADB (MrcData, Outputs->ValidChBitMask, LoopCount, NSOE, 1, 0);
  } else {
    // @todo Update with McChBitMask
    SetupIOTestBasicVA (MrcData, Outputs->ValidChBitMask, LoopCount, 0, 0, 0, 8, PatWrRd, 0, 0);  //set test to all channels
  }

  ChBitMask = 0;
  for (Channel = 0; Channel < MAX_CHANNEL; Channel++) {
    if (MrcChannelExist (MrcData, 0, Channel)) {
      ChannelOut      = &ControllerOut->Channel[Channel];
      localR[Channel] = ChannelOut->ValidRankBitMask & Ranks;
      ChBitMask |= SelectReutRanks (MrcData, (UINT8) Channel, localR[Channel], FALSE, 0);
    } // Channel Exists
  } // Channel
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "EM: ChBitMask 0x%X, Ranks 0x%X, LocalR[C0] 0x%X, LocalR[C1] 0x%X\n", ChBitMask, Ranks, localR[0], localR[1]);
  if (ChBitMask == 0) {
    return;
  }

  // Find the first selected rank
  FirstRank = 0;
  for (Channel = 0; Channel < MAX_CHANNEL; Channel++) {
    for (Rank = 0; Rank < MAX_RANK_IN_CHANNEL; Rank++) {
      if ((1 << Rank) & localR[Channel]) {
        FirstRank = Rank;  // could be in any channel
        break;
      }
    } // Rank
  } // Channel
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "EM: FirstRank 0x%X\n", FirstRank);

  // Store margin results for
  NumBytes = (UINT8) Outputs->SdramCount;
  //if (ParamV == CmdV) {
  //  NumBytes = 1;
  //}

  // Ensure all bit are checking for errors.
  MrcSetDataAndEccErrMsk (MrcData, 0xFFFFFFFFFFFFFFFFULL, 0xFF);

  // Find MaxMargin for this channel
  MaxMarginV = GetVrefOffsetLimits (MrcData, ParamV);

  //if (MAX_POSSIBLE_TIME < Stop) {
  //  Stop = MAX_POSSIBLE_TIME;
  //}
  //if (-MAX_POSSIBLE_TIME > Start) {
  //  Start = -MAX_POSSIBLE_TIME;
  //}

  if (ParamT == RdT) {
    for (Channel = 0; Channel < MAX_CHANNEL; Channel++) {
      if (!MrcChannelExist (MrcData, 0, Channel)) {
        continue;
      }
      for (sign = 0; sign < 2; sign++) {
        byteMax[Channel] = (sign) ? ABS(Stop) : ABS(Start);
        for (Byte = 0; Byte < Outputs->SdramCount; Byte++) {
          byteMax[Channel] = MrcCalcMaxRxMargin (MrcData, ParamT, /**Controller**/ CONTROLLER_0, (UINT8) Channel, FirstRank, Byte, 0, byteMax[Channel]);
        }
      }
    }
    Start = - byteMax[0];
    Stop  = byteMax[1];
  }

  IndexOff = MRC_EM_MAX_W / 2 + Start;
  // No need to search too far
  if (MaxMarginV > SearchLimits) {
    MaxMarginV = (UINT8) (SearchLimits);
  }

  OffStep = 1;

  // Loop through all Test Params and Measure Margin
  for (Off = Start; Off < Stop + 1; Off += OffStep) {
    //MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Timing: %d\n", Off);
    Index = Off - Start;

    for (Channel = 0; Channel < MAX_CHANNEL; Channel++) {
      if (!MrcChannelExist (MrcData, 0, Channel)) {
        continue;
      }
      if (ParamT == CmdT) {
        IntCmdTiming  = &IntOutputs->Controller[0].CmdTiming[Channel];
        if (CmdIteration == MrcIterationClock) {
          // the 3 is to select two SubChannels
          //@todo - 2MC
          ShiftPIforCmdTraining (MrcData, CONTROLLER_0, (UINT8) Channel, CmdIteration, localR[Channel], 3, Off, 0);
        }
        if (CmdIteration == MrcIterationCmd) {
          if (CmdGroup >= MAX_COMMAND_GROUPS) {
            MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Error: CmdGroup out of bounds! %d", CmdGroup);
            return;
          }
          //@todo - 2MC
          ShiftPIforCmdTraining (MrcData, CONTROLLER_0, (UINT8) Channel, CmdIteration, localR[Channel], 1 << CmdGroup, (INT32) IntCmdTiming->CmdPiCode[CmdGroup] + Off, 0);
        }
        if (CmdIteration == MrcIterationCtl) {
          CurrValue = 0;
          for (Rank = 0; Rank < MAX_RANK_IN_CHANNEL; Rank++) {
            if (localR[Channel] & (1 << Rank)) {
              MrcGetSetDdrIoGroupChannelStrobe (MrcData, Channel, Rank, CtlGrpPi, ReadFromCache, &GetSetVal);
              CurrValue = (UINT8) IntCmdTiming->CtlPiCode[Rank];
              break;
            }
          }
          //@todo - 2MC
          ShiftPIforCmdTraining (MrcData, CONTROLLER_0, (UINT8) Channel, CmdIteration, localR[Channel], localR[Channel], CurrValue + Off, 0);
        }
        MrcResetSequence (MrcData);
      } else {
        for (Byte = 0; Byte < NumBytes; Byte++) {
          Status = ChangeMargin (MrcData, ParamT, Off, 0, 0, /**Controller**/ 0, (UINT8) Channel, FirstRank, Byte, 0, 1, 0);
        }
      }
      // Assign to last pass margin results by reference
      // get lowest margin from all ch/rankS/byte save in FirstRank
      //if (ParamT != CmdT) {
      //  Status = GetMarginByte (
      //            MrcData,
      //            Outputs->MarginResult,
      //            ParamV,
      //            FirstRank,
      //            localR[Channel]
      //            );
      //}
    } // Channel

    for (Rep = 0; Rep < Repeats; Rep++) {
      // Run Margin Test - margin_1d with chosen param
      Status = MrcGetBERMarginByte (
        MrcData,
        Outputs->MarginResult,
        ChBitMask,
        (IsMrVref) ? Ranks : FirstRank,
        (IsMrVref) ? Ranks : FirstRank,
        ParamV,
        0, // Mode
        BMap,
        0,
        MaxMarginV,
        0,
        BERStats
        );
      //MrcDisplayMarginResultArray (MrcData, ResultTypeV);
      // Record Results
      for (Channel = 0; Channel < MAX_CHANNEL; Channel++) {
        if (!MrcChannelExist (MrcData, 0, Channel)) {
          continue;
        }
        for (Edge = 0; Edge < MAX_EDGES; Edge++) {
          for (Byte = 0; Byte < NumBytes; Byte++) {
            if ((Index > MRC_EM_MAX_W) || (Index < 0)) {
              MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Error: SaveMargin array out of bounds! %d", Index);
              return;
            }

            if (Rep == 0) {
              SaveMargin[Index][Channel][Byte][Edge] = 0;
            }
            SaveMargin[Index][Channel][Byte][Edge] += (UINT16) (*MarginByte)[ResultTypeV][FirstRank][Channel][Byte][Edge];
          } // Byte
        } // Edge
      } // Channel
    } // Repeats

    for (Channel = 0; Channel < MAX_CHANNEL; Channel++) {
      if (!MrcChannelExist (MrcData, 0, Channel)) {
        continue;
      }
      for (Edge = 0; Edge < MAX_EDGES; Edge++) {
        MinEdge = 0xFFFF;
        for (Byte = 0; Byte < NumBytes; Byte++) {
          if ((Index > (MRC_EM_MAX_W - 1)) || (Index < 0)) {
            MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Error: SaveMargin array out of bounds! %d", Index);
            return;
          }

          SaveMargin[Index][Channel][Byte][Edge] /= Repeats;
          if (MinEdge > SaveMargin[Index][Channel][Byte][Edge]) {
            MinEdge = SaveMargin[Index][Channel][Byte][Edge];
          }
        } // Byte

        if ((Index > (MRC_EM_MAX_W - 1)) ||
            (Index < 0) ||
            ((MRC_EM_MAX_H / 2 - (MinEdge - 1) / 10) > (MRC_EM_MAX_H - 1)) ||
            ((MRC_EM_MAX_H / 2 - (MinEdge - 1) / 10) < 0) ||
            ((MRC_EM_MAX_H / 2 + (MinEdge - 1) / 10) > (MRC_EM_MAX_H - 1)) ||
            ((MRC_EM_MAX_H / 2 + (MinEdge - 1) / 10) < 0)
            ) {
          MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Error: Eye or Lines array out of bounds!\n");
          return;
        }

        if (Edge) {
          Eye[Channel][Index + IndexOff][MRC_EM_MAX_H / 2 - (MinEdge) / 10]  = 1;
          Lines[Channel][MRC_EM_MAX_H / 2 - (MinEdge) / 10] = 1;
        } else {
          Eye[Channel][Index + IndexOff][MRC_EM_MAX_H / 2 + (MinEdge) / 10]  = 1;
          Lines[Channel][MRC_EM_MAX_H / 2 + (MinEdge) / 10] = 1;
        }
      } // Edge
    } // Channel
  } // Offset

  // Print the box
  for (Channel = 0; Channel < MAX_CHANNEL; Channel++) {
    if (!MrcChannelExist (MrcData, 0, Channel)) {
      continue;
    }
    MRC_DEBUG_MSG (
      Debug,
      MSG_LEVEL_NOTE,
      "Plot Eye across ParamT = %s ParamV = %s CmdIter = %s CmdGrp = 0x%x settings:(Start=%d,Stop=%d) LC = %d  Channel = %d Ranks = 0x%x\n",
      gMarginTypesStr[ParamT],
      gMarginTypesStr[ParamV],
      CmdIterTypesString[CmdIteration],
      CmdGroup,
      Start,
      Stop,
      LoopCount,
      Channel,
      localR[Channel]
      );
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\t---------------------------------------------------------- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\t66666555555555544444444443333333333222222222211111111110000000000000000000111111111122222222223333333333444444444455555555556666\n");
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Vref\t43210987654321098765432109876543210987654321098765432109876543210123456789012345678901234567890123456789012345678901234567890123\n");
    for (i = 0; i < MRC_EM_MAX_H; i++) {
      if (Lines[Channel][i]) {
        // print only fail lines
        MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "%3d:\t", MRC_EM_MAX_H / 2 - i); // per ch
        for (j = 0; j < MRC_EM_MAX_W; j++) {
          if (Eye[Channel][j][i]) {
            MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "%s", "#"); // per ch
          } else {
            MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "%s", ((j == (MRC_EM_MAX_W) / 2) || (i == (MRC_EM_MAX_H) / 2)) ? "+" : " "); // per ch
          }
        } // j
        MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\n");//per ch
      }
    } // i
  } // Channel
  if (ParamT != CmdT) {
    Status = ChangeMargin (MrcData, ParamT, 0, 0, 1, /**Controller**/ 0, 0, 0, 0, 0, 1, 0);
  } else {
    //restore settings
    ShiftCh2Ch (MrcData, 0xff, SafeOff, 1, 0, 0, 0);
  }
  Status = MrcResetSequence (MrcData);
}
#endif // EYE_MARGIN_SUPPORT

/**
  Calculate Power for the selected Opt param based on:

  @param[in] MrcData     - Include all MRC global data.
  @param[in] PanicVttDnLp- Measures panic comp event rates, not power
  @param[in] Scale       - Multiplication factor for margins and power to preserve decimal accuracy (usually 10 to preserve 1 decimal place)
  @param[in] Params      - Params that power should be estimated for
  @param[in] ParamValues - Param values that power should be estimated for
  @param[in] ParamsCount - Number of params that power should be estimated for

  @retval Calc power in mW if using FIVR, else use linear approximation
**/
MrcStatus
CalcSysPower(
  IN MrcParameters   *MrcData,
  IN BOOLEAN         PanicVttDnLp,
  IN UINT8           Scale,
  IN const UINT8     Params[],
  IN INT8            ParamValues[],
  IN UINT8           ParamsCount
  )
{
  MrcOutput             *Outputs;
  MrcInput              *Inputs;
  MrcChannelOut         *ChannelOut;
  const MRC_FUNCTION    *MrcCall;
  //MrcOdtPowerSaving     *OdtPowerSaving;
  MrcPower              SystemPower;
  UINT8                 Channel;
  UINT8                 Controller;
  UINT8                 McChBitMask = 0;
  UINT8                 Iterator;
  UINT32                FivrPInValueStart;
  UINT32                FivrPInCountStart;
  UINT32                FivrPInValueEnd;
  UINT32                FivrPInCountEnd;
  UINT32                MaxPOutValue;
  UINT32                MaxPOutUnits;
  UINT32                CountAccumulator;
  INT64                 GetSetVal = 0x0;
  UINT64                Timeout;
  UINT8                 VTTIndex;

  Outputs             = &MrcData->Outputs;
  Inputs              = &MrcData->Inputs;
  MrcCall             = Inputs->Call.Func;
  //OdtPowerSaving      = &Outputs->OdtPowerSavingData;

  SystemPower.RdPower = 0;
  SystemPower.WrPower = 0;
  SystemPower.TotalPwr = 0;

  for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
    for (Channel = 0; Channel < Outputs->MaxChannels; Channel++) {
      if (!(MC_CH_MASK_CHECK(MrcData->Outputs.ValidChBitMask, Controller, Channel, Outputs->MaxChannels))) {
        continue;
      }
      ChannelOut = &Outputs->Controller[Controller].Channel[Channel];
      // no need to run on channel with no selected ranks
      if (!(ChannelOut->ValidRankBitMask)) {
        continue;
      }
      McChBitMask |= SelectReutRanks(MrcData, Controller, Channel, 0xFF, FALSE, 0); // Run all ranks
    }
  }

  if (McChBitMask == 0x0) {
    return mrcSuccess; // Nothing to do here
  }

  // For PanicVttDnLp, don't calculate power; instead, find the number of [panic high + (panic high - panic lo)] panic events that occur (and minimize it while making sure [panic high - panic lo > 0])
  if (PanicVttDnLp != FALSE) { // In order to run this, DDRVtt_CR_DDRCrVttGenStatus.EnCount must be set to 1 or it won't work.
    SetupIOTestBasicVA (MrcData, McChBitMask, OPT_PARAM_LOOP_COUNT, NSOE, 0, 0, 8, PatWrRd, 0, 0); // TGL_POWER_TRAINING_FUTURE - LC may need to be optimized based on number of ranks and test type (for Rx Turnarounds)

    for (VTTIndex = 0; VTTIndex < MAX_VTT_REGS; VTTIndex++) {
      GetSetVal = 0;
      MrcGetSetChStrb (MrcData, MRC_IGNORE_ARG, MRC_IGNORE_ARG, VTTIndex, VttGenStatusSelCount, WriteCached, &GetSetVal);
      GetSetVal = 1;
      //MrcGetSetChStrb(MrcData, MRC_IGNORE_ARG, MRC_IGNORE_ARG, VTTIndex, VttGenStatusResetCount, WriteCached, &GetSetVal);
    }

    // Run CPGC
    RunIOTest (MrcData, McChBitMask, Outputs->DQPat, 1, 0);

    CountAccumulator = 0;
    for (VTTIndex = 0; VTTIndex < MAX_VTT_REGS; VTTIndex++) {
      MrcGetSetNoScope(MrcData, VttGenStatusCount, ReadUncached, &GetSetVal);
      CountAccumulator += (UINT32)(GetSetVal & 0x7FFF); // Use the absolute value of the 16 bit signed error count, so cut off the MSB sign bit

      MRC_POWER_DEBUG_MSG(&Outputs->Debug, MSG_LEVEL_NOTE, "VttGenStatusSelCount = 1: CountAccumulator = %d \n", CountAccumulator);

    }
    SystemPower.RdPower += (UINT16)(CountAccumulator / MAX_VTT_REGS);

    for (VTTIndex = 0; VTTIndex < MAX_VTT_REGS; VTTIndex++) {
      GetSetVal = 4;
      MrcGetSetChStrb(MrcData, MRC_IGNORE_ARG, MRC_IGNORE_ARG, VTTIndex, VttGenStatusSelCount, WriteCached, &GetSetVal);
      GetSetVal = 1;
      //MrcGetSetChStrb(MrcData, MRC_IGNORE_ARG, MRC_IGNORE_ARG, VTTIndex, VttGenStatusResetCount, WriteCached, &GetSetVal);
    }

    // Run CPGC
    RunIOTest (MrcData, McChBitMask, Outputs->DQPat, 1, 0);

    CountAccumulator = 0;
    for (VTTIndex = 0; VTTIndex < MAX_VTT_REGS; VTTIndex++) {
      MrcGetSetNoScope(MrcData, VttGenStatusCount, ReadUncached, &GetSetVal);
      if (GetSetVal > 0x7FFF) { // Value is actually negative (sign/magnitude encoding)
        CountAccumulator = 0xFFFF; // Set panic count to maximum if SelCount=4 gives a negative result, so that this PanicVttDnLp value is not chosen.
        MRC_POWER_DEBUG_MSG(&Outputs->Debug, MSG_LEVEL_NOTE, "VttGenStatusSelCount = 4: CountAccumulator = 0xFFFF \n");

        break;
      }
      CountAccumulator += (UINT32)GetSetVal;
      MRC_POWER_DEBUG_MSG(&Outputs->Debug, MSG_LEVEL_NOTE, "VttGenStatusSelCount = 4: CountAccumulator = %d \n", CountAccumulator);

    }
    if (CountAccumulator == 0xFFFF) { // Set panic count to maximum if SelCount=4 gives a negative result, so that this PanicVttDnLp value is not chosen.
      SystemPower.RdPower = 0xFFFF;
    } else {
      SystemPower.RdPower += (UINT16)(CountAccumulator / MAX_VTT_REGS);
    }


    SystemPower.WrPower = 0;
    SystemPower.TotalPwr = 0;
    MRC_POWER_DEBUG_MSG(&Outputs->Debug, MSG_LEVEL_NOTE, "VttGenStatus: Final RdPower = %d \n", SystemPower.RdPower);

    return mrcSuccess;
  }

  // We can use FIVR for DRAM ODT training, as we don't care about power changes related to DRAM ODT changes due to sweeping from low to high power consumption and stopping once UPMs are met.
  // TGL_POWER_TRAINING_FUTURE - As we don't need to set any registers in the PCU with read or write specific power, we may be able to use total power for everything and eliminate the read-only and write-only CPGC tests
  if (FIVR_POWER_MEASUREMENT_ENABLED && SupportsFivrPower(MrcData, Params[0])) {
    //DdrCrDataOffsetComp.Data = ChannelOut->DataCompOffset[Byte];

    SetupIOTestBasicVA(MrcData, McChBitMask, OPT_PARAM_LOOP_COUNT, NSOE, 0x4, 0, 8, PatWrRd, 0, 0); // Read-only test all channels (0x4 used as a flag). TGL_POWER_TRAINING_FUTURE - LC may need to be optimized based on number of ranks and test type (Rx Turnarounds), write-only test may not be setup correctly.
    Timeout = MrcCall->MrcGetCpuTime() + 10000; // 10 seconds timeout
    do {
      // TGL_POWER_TRAINING_FUTURE - Read initial FIVR input power accumulator from P_IN_ACCUMULATOR and sample count from P_IN_NUM_SAMPLES_SNAPSHOT
      // MrcGetSetDdrIoGroupController0 (MrcData, FivrPInValue, ReadUncached, &GetSetVal);
      FivrPInValueStart = (UINT16)GetSetVal;
      // MrcGetSetDdrIoGroupController0 (MrcData, FivrPInCount, ReadUncached, &GetSetVal);
      FivrPInCountStart = (UINT16)GetSetVal;

      RunIOTest(MrcData, McChBitMask, Outputs->DQPat, 1, 0); // This is a standard point test

      // TGL_POWER_TRAINING_FUTURE - Read final FIVR input power accumulator from P_IN_ACCUMULATOR and sample count from P_IN_NUM_SAMPLES_SNAPSHOT
      // MrcGetSetDdrIoGroupController0 (MrcData, FivrPInValue, ReadUncached, &GetSetVal);
      FivrPInValueEnd = (UINT16)GetSetVal;
      // MrcGetSetDdrIoGroupController0 (MrcData, FivrPInCount, ReadUncached, &GetSetVal);
      FivrPInCountEnd = (UINT16)GetSetVal;
    } while (FivrPInCountEnd < FivrPInCountStart); // If this is not true, then we overflowed
    if ((MrcCall->MrcGetCpuTime() >= Timeout) && (MrcCall->MrcGetCpuTime() < Timeout)) {
      MRC_DEBUG_MSG(&Outputs->Debug, MSG_LEVEL_ERROR, "\nTimeout in FIVR read power measurement! \n");
    }

    FivrPInValueEnd = FivrPInValueEnd - FivrPInValueStart;
    FivrPInCountEnd = FivrPInCountEnd - FivrPInCountStart;
    // TGL_POWER_TRAINING_FUTURE - Result must be in mW, so we have to adjust for this by finding out what the units are in FIVR. Units are (VR_MAX.MAX_P_OUT * 2^VR_MAX.P_OUT_EXP / 255) mW
    // MrcGetSetDdrIoGroupController0 (MrcData, MaxPOutValue, ReadUncached, &GetSetVal);
    MaxPOutValue = (UINT32)GetSetVal;
    // MrcGetSetDdrIoGroupController0 (MrcData, MaxPOutExp, ReadUncached, &GetSetVal);
    MaxPOutUnits = 1;
    for (Iterator = 0; Iterator < GetSetVal; Iterator++) {
      MaxPOutUnits *= 2;
    }
    MaxPOutValue *= MaxPOutUnits;
    //#ifdef LOCAL_STUB_FLAG
    FivrPInValueEnd = 12750;
    MaxPOutValue = 0; // 1000; // TGL_POWER_TRAINING_FUTURE Hardcode to 0 for now, enable later
    FivrPInCountEnd = 100;
    //#endif
    //SystemPower.RdPower = (UINT16)((FivrPInValueEnd * MaxPOutValue * Scale) / (FivrPInCountEnd * 255));

    SetupIOTestBasicVA (MrcData, McChBitMask, OPT_PARAM_LOOP_COUNT, NSOE, 0x2, 0, 8, PatWrRd, 0, 0); // Write-only test all channels (0x2 used as a flag). TGL_POWER_TRAINING - LC may need to be optimized based on number of ranks and test type (Rx Turnarounds), write-only test may not be setup correctly.
    Timeout = MrcCall->MrcGetCpuTime() + 10000; // 10 seconds timeout
    do {
      // TGL_POWER_TRAINING_FUTURE - Read initial FIVR input power accumulator from P_IN_ACCUMULATOR and sample count from P_IN_NUM_SAMPLES_SNAPSHOT
      // MrcGetSetDdrIoGroupController0 (MrcData, FivrPInValue, ReadUncached, &GetSetVal);
      FivrPInValueStart = (UINT16)GetSetVal;
      // MrcGetSetDdrIoGroupController0 (MrcData, FivrPInCount, ReadUncached, &GetSetVal);
      FivrPInCountStart = (UINT16)GetSetVal;

      RunIOTest(MrcData, McChBitMask, Outputs->DQPat, 1, 0); // This is a standard point test

      // TGL_POWER_TRAINING_FUTURE - Read final FIVR input power accumulator from P_IN_ACCUMULATOR and sample count from P_IN_NUM_SAMPLES_SNAPSHOT
      // MrcGetSetDdrIoGroupController0 (MrcData, FivrPInValue, ReadUncached, &GetSetVal);
      FivrPInValueEnd = (UINT16)GetSetVal;
      // MrcGetSetDdrIoGroupController0 (MrcData, FivrPInCount, ReadUncached, &GetSetVal);
      FivrPInCountEnd = (UINT16)GetSetVal;
    } while ((FivrPInCountEnd < FivrPInCountStart) && (MrcCall->MrcGetCpuTime() < Timeout)); // If this is not true, then we overflowed
    if (MrcCall->MrcGetCpuTime() >= Timeout) {
      MRC_DEBUG_MSG(&Outputs->Debug, MSG_LEVEL_ERROR, "\nTimeout in FIVR write power measurement! \n");
    }

    FivrPInValueEnd = FivrPInValueEnd - FivrPInValueStart;
    FivrPInCountEnd = FivrPInCountEnd - FivrPInCountStart;
    //#ifdef LOCAL_STUB_FLAG
    FivrPInValueEnd = 12750;
    MaxPOutValue = 0; // 1000; // TGL_POWER_TRAINING_FUTURE Hardcode to 0 for now, enable later
    FivrPInCountEnd = 100;
    //#endif
    //SystemPower.WrPower = (UINT16)((FivrPInValueEnd * MaxPOutValue * Scale) / (FivrPInCountEnd * 255));

    //MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Power->RdPower : %d Power->WrPower: %d ParamVector: %d ParamVector: %d ParamVector: %d ParamVector: %d \n", Power->RdPower, Power->WrPower, ParamVector[0], ParamVector[1], ParamVector[2], ParamVector[3]);
    //SystemPower.TotalPwr = ((SystemPower.RdPower) + (SystemPower.WrPower)) / 2; // [mW] x Scale
    //MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Power->TotalPwr %d\n", Power->TotalPwr);
  } else {
    SystemPower.TotalPwr = 0;

    // The only reason you use power is if every combination is above UPM, otherwise you just drive to the UPM limit.
    // If every single combination is above UPMs, then the only thing we need to know is which of these combinations has the lowest power, nothing else.
    // So two linear approximations added together will do the job, as it will tell us which value set has the smallest power burn.
    // None of the power values for the two params will be accurate relative to each other, but we don't care, we just need the smallest combination.
    // There's only 1 global minimum that satisfies that criteria regardless of whether the power values for the linear approximations are acccurate relative to each other.
    for (Iterator = 0; Iterator < ParamsCount; Iterator++) {
      switch (Params[Iterator]) {
        case OptRxLoad:
        case OptRxBias:
        case OptRdDqOdt:
        case OptRdDqsOdt:
        case OptCCCSComp:
        case OptCCCDS:
        case OptCCCDSDnCoarse:
        case OptCCCDSUpCoarse:
        case OptDimmRon:
        case OptDimmOdtCA:
        case OptDimmOdtNom:
        case OptDimmOdtPark:
        case OptDimmOdtParkNT:
        case OptDimmOdtNomNT:
        case OptDimmOdtWr:
        case OptSComp:
        case OptWrDS:
        case OptWrDSDnCoarse:
        case OptWrDSUpCoarse:
          SystemPower.TotalPwr += CalcLinearNormalizedPower (MrcData, Params[Iterator], ParamValues[Iterator], TRUE);
          break;
        case OptCCCTxEq:
        case OptTxEq:
        case OptVddq:
          SystemPower.TotalPwr += CalcLinearNormalizedPower (MrcData, Params[Iterator], ParamValues[Iterator], FALSE);
          break;
        default:
          MRC_DEBUG_MSG(&Outputs->Debug, MSG_LEVEL_ERROR, "CalcSysPower called with invalid param! %d\n", Params[Iterator]);
          break;
      }
    }

    SystemPower.TotalPwr *= Scale;
  }

  // update Mrc struct with Base line numbers
  //if (OdtPowerSaving->BaseFlag == FALSE) {
  //  OdtPowerSaving->BaseSavingTotal = (UINT16) SystemPower.TotalPwr;
  //} else {
  //  OdtPowerSaving->MrcSavingTotal = (UINT16) SystemPower.TotalPwr;
  //}

  return mrcSuccess;
}

/**
  This function optimize the digital offsets by reducing the digital
  offset and apply the difference to the global one.

  @param[in] MrcData    - Include all MRC global data.
  @param[in] Param      - Parameter defining the desired digital compensation offset.

  @retval MrcStatus - mrcSuccess
**/
MrcStatus
OptimizeCompOffset (
  IN MrcParameters *const MrcData,
  IN const UINT8          Param
  )
{
  MrcStatus          Status;
  MrcDebug           *Debug;
  MrcInput           *Inputs;
  MrcOutput          *Outputs;
  MrcChannelOut      *ChannelOut;
  const MRC_FUNCTION *MrcCall;
  UINT8             GlobalParam[MAX_COMPS_OPTIMIZED];
  UINT8             CurrCompVref[MAX_COMPS_OPTIMIZED];
  UINT8             NewCompVref[MAX_COMPS_OPTIMIZED];
  UINT8             NewCompCode[MAX_COMPS_OPTIMIZED];
  INT8              Sign;
  INT8              RoundSign;
  INT16             AverageCompOffsetPerCompOffset[MAX_COMP_OFFSETS_OPTIMIZED];
  INT16             AverageCompOffsetPerComp[MAX_COMPS_OPTIMIZED];
  UINT8             NumCompOffsetsPerComp[MAX_COMPS_OPTIMIZED];
  UINT16            Offset = 0;
  UINT16            ExtendedCompOffsetValue = 0;
  INT16             MinDelta = 0;
  INT16             TotalCompOffset = 0;
  INT16             CurrentDelta = 0;
  UINT8             CompVrefOffset = 1;
  UINT8             Comp;
  UINT8             CompOffset;
  UINT8             CompOffsetBytes[MAX_COMP_OFFSETS_OPTIMIZED];
  UINT8             CompOffsetCompIndex[MAX_COMP_OFFSETS_OPTIMIZED];
  UINT8             NumTotalCompCodes = 0;
  UINT8             NumTotalSideEffectCompCodes = 0;
  UINT8             NumCompOffsets = 0;
  UINT8             BestCompVref[MAX_COMPS_OPTIMIZED];
  UINT8             MaxCompOffsetBits[MAX_COMP_OFFSETS_OPTIMIZED];
  UINT8             Byte;
  UINT8             Bytes[MAX_COMP_OFFSETS_OPTIMIZED];
  UINT8             Controller = 0;
  UINT8             Channel;
  UINT8             MaxComp[MAX_COMPS_OPTIMIZED];
  UINT8             MaxCompVref[MAX_COMPS_OPTIMIZED];
  UINT8             MinCompVref[MAX_COMPS_OPTIMIZED];
  UINT8             CurrSCompPC[MAX_COMPS_OPTIMIZED];
  INT16             TotalCompCodes[MAX_COMPS_OPTIMIZED][MAX_CONTROLLER][MAX_CHANNEL][MAX_SDRAM_IN_DIMM];
  UINT8             CurrentCompCode[MAX_COMPS_OPTIMIZED];
  UINT32            NewMainCompVref_Target;
  UINT32            OldMainCompVref_Target;
  UINT32            SideEffectCompVref_Target;
  INT64             GetSetVal;
  UINT64            Timeout;
  BOOLEAN           ContinueOptimizing = TRUE;
  BOOLEAN           UlxUlt;
  TOptParamOffset   CompCodeGroup[MAX_COMPS_OPTIMIZED];
  UINT8             CompCodeDirection[MAX_COMPS_OPTIMIZED];
  BOOLEAN           CompCodeSideEffect[MAX_COMPS_OPTIMIZED];
  GSM_GT            CompOffsetGroup[MAX_COMP_OFFSETS_OPTIMIZED];
  DATA0CH0_CR_DDRCRDATAOFFSETCOMP_STRUCT  DdrCrDataOffsetComp;
  CH0CCC_CR_DDRCRCTLCACOMPOFFSET_STRUCT   DdrCrCmdOffsetComp;
  UINT32            NewCompValue;

  Inputs  = &MrcData->Inputs;
  Outputs = &MrcData->Outputs;
  Debug   = &Outputs->Debug;
  MrcCall = Inputs->Call.Func;
  UlxUlt  = (Inputs->UlxUlt);

  DdrCrDataOffsetComp.Data = 0;
  DdrCrCmdOffsetComp.Data = 0;

  MrcCall->MrcSetMem((UINT8 *)&CurrSCompPC, sizeof(CurrSCompPC), 0);
  MrcCall->MrcSetMem((UINT8 *)&AverageCompOffsetPerComp, sizeof(AverageCompOffsetPerComp), 0);
  MrcCall->MrcSetMem((UINT8 *)&NumCompOffsetsPerComp, sizeof(NumCompOffsetsPerComp), 0);
  MrcCall->MrcSetMem((UINT8 *)&CompCodeSideEffect, sizeof(CompCodeSideEffect), FALSE);
  MrcCall->MrcSetMem((UINT8 *)&AverageCompOffsetPerCompOffset, sizeof(AverageCompOffsetPerCompOffset), 0);
  MrcCall->MrcSetMem((UINT8 *)&Bytes, sizeof(Bytes), 0);
  MrcCall->MrcSetMem((UINT8 *)&NewCompVref, sizeof(NewCompVref), 0xFF);

  // When defining arrays here, the first comp vref must always be an optimized comp vref, not a side effect comp vref
  switch (Param) {
  case WrDSUp:
    // Comps
    CompCodeGroup[0]        = OptWrDS; // TxRonUp
    CompCodeDirection[0]    = COMP_UP;

    // Comp Offsets
    CompOffsetGroup[0]      = DqDrvUpCompOffset;
    CompOffsetBytes[0]      = (UINT8) Outputs->SdramCount;
    CompOffsetCompIndex[0]  = 0;
    MaxCompOffsetBits[0]    = DATA0CH0_CR_DDRCRDATAOFFSETCOMP_dqdrvupcompoffset_WID; // Comp offset

    // Comp Vref
    MrcGetSetNoScope (MrcData, DqDrvVrefUp, ReadFromCache, &GetSetVal);
    CurrCompVref[0]   = (UINT8) GetSetVal;
    MaxCompVref[0]    = MRC_COMP_VREF_STEP_SIZE;
    MinCompVref[0]    = 0;
    GlobalParam[0]    = WrDSUp;
    MaxComp[0]        = 63; // Comp
    NumTotalCompCodes = 1;
    NumCompOffsets    = 1;
    break;

  case RdOdtUp:
    if (UlxUlt) {
      // Comps
      CompCodeGroup[0]      = OptRdDqOdt; // CompRcompOdtUp
      CompCodeDirection[0]  = COMP_UP;    // Comp_UP is only valid
      CompOffsetGroup[0]    = DqOdtCompOffset;
    } else {
      switch (Outputs->OdtMode) {
      case MrcOdtModeVss:
        // Comps
        CompCodeGroup[0]      = OptRdDqOdt; // CompRcompOdtDn
        CompCodeDirection[0]  = COMP_DN;    // In Vss mode only odt down is valid
        CompOffsetGroup[0]    = DqOdtCompOffset;
        break;
      case MrcOdtModeVddq:
      case MrcOdtModeVtt:
        // Comps
        CompCodeGroup[0]      = OptRdDqOdt; // CompRcompOdtUp
        CompCodeDirection[0]  = COMP_UP;    // In Vtt/Vddq mode only odt up is valid
        CompOffsetGroup[0]    = DqOdtCompOffset;
        break;
      default:
        MRC_DEBUG_MSG(Debug, MSG_LEVEL_ERROR, "Invalid ODT Mode!");
        return mrcFail;
      }
    }

    // Comps
    NumTotalCompCodes = 1;

    // Comp Offsets
    CompOffsetBytes[0]      = (UINT8) Outputs->SdramCount;
    CompOffsetCompIndex[0]  = 0;
    MaxCompOffsetBits[0]    = DATA0CH0_CR_DDRCRDATAOFFSETCOMP_dqodtcompoffset_WID;
    NumCompOffsets          = 1;

    // Comp Vref
    MrcGetSetNoScope (MrcData, DqOdtVrefUp, ReadFromCache, &GetSetVal);
    CurrCompVref[0] = (UINT8) GetSetVal;
    MaxCompVref[0]  = MRC_COMP_VREF_STEP_SIZE;
    MinCompVref[0]  = 0;
    GlobalParam[0]  = RdOdtUp;
    MaxComp[0]      = 63;
    break;

  default:
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_ERROR, "Invalid Comp Optimization Param : %d", Param);
    return mrcFail;
  }

  // Find initial comp values
  for (Comp = 0; Comp < NumTotalCompCodes; Comp++) {
    CurrentCompCode[Comp] = (UINT8) GetCompCode (MrcData, CompCodeGroup[Comp], CompCodeDirection[Comp]);
    NewCompCode[Comp] = CurrentCompCode[Comp];
  }

  // Get current comp offsets
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\n***** Opt Param:%s *****\nInitial Values", CompGlobalOffsetParamStr[Param]);
  for (CompOffset = 0; CompOffset < NumCompOffsets; CompOffset++) {
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\n Comp Offset Param: %d, side effect = %s", CompOffset, (CompCodeSideEffect[CompOffsetCompIndex[CompOffset]] == TRUE) ? "TRUE" : "FALSE");
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\n  Comp code: %d\n  Comp Vref: %d",
      CurrentCompCode[CompOffsetCompIndex[CompOffset]],
      CurrCompVref[CompOffsetCompIndex[CompOffset]] + (CurrSCompPC[CompOffsetCompIndex[CompOffset]] << SCOMP_PC_STORAGE_BIT_OFFSET));

    for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
      for (Channel = 0; Channel < Outputs->MaxChannels; Channel++) {
        if ((MrcChannelExist(MrcData, Controller, Channel))) {
          MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\n    MC%d.Ch%d\n", Controller, Channel);
          ChannelOut = &Outputs->Controller[Controller].Channel[Channel];
          DdrCrCmdOffsetComp.Data = ChannelOut->CmdCompOffset;

          for (Byte = 0; Byte < CompOffsetBytes[CompOffset]; Byte++) {
            ExtendedCompOffsetValue = 0;
            if (MrcByteExist(MrcData, Controller, Channel, Byte)) {
              DdrCrDataOffsetComp.Data = ChannelOut->DataCompOffset[Byte];

              switch (CompOffsetGroup[CompOffset]) {
              case DqDrvUpCompOffset:
                Offset = (UINT16)DdrCrDataOffsetComp.Bits.dqdrvupcompoffset;
                break;
              case CmdRCompDrvUpOffset:
                Offset = (UINT16)DdrCrCmdOffsetComp.Bits.CaRcompDrvUpOffset;
                break;
              case CtlRCompDrvUpOffset:
                Offset = (UINT16)DdrCrCmdOffsetComp.Bits.CtlRcompDrvUpOffset;
                break;
              case DqsOdtCompOffset:
                MrcGetSetChStrb(MrcData, Controller, Channel, Byte, DqsOdtCompOffset, ReadFromCache, &GetSetVal);
                Offset = (UINT16)GetSetVal;
                break;
              case DqOdtCompOffset:
                Offset = (UINT16)DdrCrDataOffsetComp.Bits.dqodtcompoffset;
                break;
              case DqDrvDnCompOffset:
                Offset = (UINT16)DdrCrDataOffsetComp.Bits.dqdrvdowncompoffset;
                break;
              case CmdRCompDrvDownOffset:
                Offset = (UINT16)DdrCrCmdOffsetComp.Bits.CaRcompDrvDownOffset;
                break;
              case CtlRCompDrvDownOffset:
                Offset = (UINT16)DdrCrCmdOffsetComp.Bits.CtlRcompDrvDownOffset;
                break;
              case DqSCompOffset:
                Offset = (UINT16)DdrCrDataOffsetComp.Bits.dqslewratecompoffset;
                break;
              case CmdSCompOffset:
                Offset = (UINT16)DdrCrCmdOffsetComp.Bits.CaScompOffset;
                break;
              case CtlSCompOffset:
                Offset = (UINT16)DdrCrCmdOffsetComp.Bits.CtlScompOffset;
                break;
              case RloadCompOffset:
                Offset = (UINT16)DdrCrDataOffsetComp.Bits.rloadoffset;
                break;
              default:
                Offset = 0;
              }
              Bytes[CompOffset]++;

              // We store the comp and comp offset values as unsigned integers even though the comp offsets are 2's complement.
              // We just have to make sure all the values are 16 bits long (longer than the longest comp or comp offset field) and the math works out.
              ExtendedCompOffsetValue = MrcSE(Offset, MaxCompOffsetBits[CompOffset], 16);
              MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "      Byte %d , Comp Offset: %d\n", Byte, (INT16)ExtendedCompOffsetValue);
              TotalCompCodes[CompOffset][Controller][Channel][Byte] = CurrentCompCode[CompOffsetCompIndex[CompOffset]] + ExtendedCompOffsetValue;
            }
            if (CompCodeSideEffect[CompOffsetCompIndex[CompOffset]] == FALSE) {
              AverageCompOffsetPerCompOffset[CompOffset] += (INT16)ExtendedCompOffsetValue;
            }
          }
        }
      }
    }
  }

  // Compute average comp offset per comp offset
  for (CompOffset = 0; CompOffset < NumCompOffsets; CompOffset++) {
    if (CompCodeSideEffect[CompOffsetCompIndex[CompOffset]] == TRUE) {
      continue;
    }
    MRC_POWER_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Comp offset param: %d \n", CompOffset);
    MRC_POWER_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Total comp offset for this comp offset param: %d\n", AverageCompOffsetPerCompOffset[CompOffset]);
    MRC_POWER_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Total bytes: %d\n", Bytes[CompOffset]);

    RoundSign = AverageCompOffsetPerCompOffset[CompOffset] > 0 ? 1 : -1;
    if (Bytes[CompOffset] != 0) {
      AverageCompOffsetPerCompOffset[CompOffset] = (AverageCompOffsetPerCompOffset[CompOffset] / Bytes[CompOffset]) + (RoundSign * ((((AverageCompOffsetPerCompOffset[CompOffset] % Bytes[CompOffset]) * RoundSign) > (Bytes[CompOffset] / 2)) ? 1 : 0));
    } else {
      AverageCompOffsetPerCompOffset[CompOffset] = 0;
    }
    MRC_POWER_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "AverageCompOffsetPerCompOffset: %d\n", AverageCompOffsetPerCompOffset[CompOffset]);
  }

  // Compute average comp offset per comp
  for (CompOffset = 0; CompOffset < NumCompOffsets; CompOffset++) { // Comp offset per comp
    if (CompCodeSideEffect[CompOffsetCompIndex[CompOffset]] == TRUE) {
      continue;
    }
    AverageCompOffsetPerComp[CompOffsetCompIndex[CompOffset]] += AverageCompOffsetPerCompOffset[CompOffset];
    NumCompOffsetsPerComp[CompOffsetCompIndex[CompOffset]]++;
  }
  for (Comp = 0; Comp < NumTotalCompCodes; Comp++) {
    if (CompCodeSideEffect[Comp] == TRUE) {
      continue;
    }
    RoundSign = AverageCompOffsetPerComp[Comp] > 0 ? 1 : -1;
    AverageCompOffsetPerComp[Comp] = (AverageCompOffsetPerComp[Comp] / NumCompOffsetsPerComp[Comp]) + (RoundSign * ((((AverageCompOffsetPerComp[Comp] % NumCompOffsetsPerComp[Comp]) * RoundSign) > (NumCompOffsetsPerComp[Comp] / 2)) ? 1 : 0));
    MRC_POWER_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Comp Param: %d, AverageCompOffsetPerComp: %d \n", Comp, AverageCompOffsetPerComp[Comp]);
  }

  // Compute the minimum delta, which is the average of the AverageCompOffsetPerComp values for all non side effect comps
  for (Comp = 0; Comp < NumTotalCompCodes; Comp++) {
    if (CompCodeSideEffect[Comp] == TRUE) {
      continue;
    }
    MinDelta += ABS(AverageCompOffsetPerComp[Comp]);
    // Add together all comp offsets averages for all comps, adjusting for sign differences. This will show which way to sweep the comp vref.
    TotalCompOffset += AverageCompOffsetPerComp[Comp] * ((CompCodeDirection[Comp] == COMP_DN) ? -1 : 1); // If we decrease the offset value for Dn comps, it decreases the comp vref as they are only pulldown comps.
  }
  MinDelta = (MinDelta / (NumTotalCompCodes - NumTotalSideEffectCompCodes)) + (((MinDelta % (NumTotalCompCodes - NumTotalSideEffectCompCodes)) >((NumTotalCompCodes - NumTotalSideEffectCompCodes) / 2)) ? 1 : 0);

  // Now, we need to know how much the base impedance value should be adjusted due to the average comp Offset
  Sign = (TotalCompOffset < 0) ? -1 : 1; // All Comp Vrefs are set to the same value
  MRC_POWER_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "MinDelta: %d, Sign: %d, TotalCompOffset: %d \n", MinDelta, Sign, TotalCompOffset);

  for (Comp = 0; Comp < NumTotalCompCodes; Comp++) {
    // In this code, we assume all associated comp vrefs are set to the same point in the PHY init prior to this optimization.
    if (CurrSCompPC[Comp] < MRC_BIT1) { // Any valid code will be 0 or 1
      BestCompVref[Comp] = (CurrSCompPC[Comp] << SCOMP_PC_STORAGE_BIT_OFFSET) + CurrCompVref[Comp];
    }
    BestCompVref[Comp] = CurrCompVref[Comp];
  }

  // We optimize all comp vrefs together. This assumes all comp vrefs in this optimization have the same value per step size, and start at the same point.
  if (MinDelta != 0) { // Loop until we find a break condition as long as the average comp offset is not 0 (if it's 0, we're already at the optimal comp vref)
    Timeout = MrcCall->MrcGetCpuTime() + 10000; // 10 seconds timeout
    while (1) {
      if ((MrcCall->MrcGetCpuTime() >= Timeout)) {
        MRC_DEBUG_MSG (Debug, MSG_LEVEL_ERROR, "\nTimeout in comp optimization! \n");
        return mrcFail;
      }
      for (Comp = 0; Comp < NumTotalCompCodes; Comp++) {
        MRC_POWER_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Comp Param: %d \n", Comp);

        if (CompCodeSideEffect[Comp] == FALSE) {
          NewCompVref[Comp] = CurrCompVref[Comp] + (Sign * CompVrefOffset);
        } else {
          // All side effect comp vrefs use the same equations:
          // No need to account for segments here, as they cancel out due to the way the equations are used.
          //   #1: NewMainCompVref_Target = (191 * 100 Ohms) / NewMainCompVref - 100 Ohms
          //   #2: OldMainCompVref_Target = (191 * 100 Ohms) / OldMainCompVref - 100 Ohms
          //   #2: SideEffectCompVref_Target = OldMainCompVref_Target * OldSideEffectCompVref / (191 - OldSideEffectCompVref)
          //   #3: NewSideEffectCompVref = 191 * (SideEffectCompVref_Target / (SideEffectCompVref_Target + NewMainCompVref_Target))
          //   We don't take into account Vtt mode here, as only the Rx ODT down comp is a side effect, and it's not used in Vtt mode.
          NewMainCompVref_Target = (MRC_COMP_VREF_STEP_SIZE * Inputs->RcompResistor * TWO_DECIMAL_PLACES) / NewCompVref[OPTIMIZED_COMP_VREF_INDEX] - Inputs->RcompResistor * TWO_DECIMAL_PLACES; // * 100 to preserve 2 decimal places of accuracy
          OldMainCompVref_Target = (MRC_COMP_VREF_STEP_SIZE * Inputs->RcompResistor * TWO_DECIMAL_PLACES) / CurrCompVref[OPTIMIZED_COMP_VREF_INDEX] - Inputs->RcompResistor * TWO_DECIMAL_PLACES; // * 100 to preserve 2 decimal places of accuracy
          SideEffectCompVref_Target = (OldMainCompVref_Target * CurrCompVref[Comp]) / (MRC_COMP_VREF_STEP_SIZE - CurrCompVref[Comp]);
          MRC_POWER_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "NewMainCompVref_Target: %d\n", NewMainCompVref_Target);
          MRC_POWER_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "OldMainCompVref_Target: %d\n", OldMainCompVref_Target);
          MRC_POWER_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "SideEffectCompVref_Target: %d\n", SideEffectCompVref_Target);

          if (GlobalParam[Comp] == WrDSDn) {
            NewCompVref[Comp] = 96; // Since the pulldown strength is relative to the pullup strength, the Vref/Vsupply ratio is always 0.5. - > 191 / 2 = 96
          } else {
            NewCompVref[Comp] = (UINT8)((MRC_COMP_VREF_STEP_SIZE * SideEffectCompVref_Target) / (SideEffectCompVref_Target + NewMainCompVref_Target));
          }
        }
        MRC_POWER_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "new CompVref: %d\n", NewCompVref[Comp]);

        if ((MinCompVref[Comp] > NewCompVref[Comp]) || (NewCompVref[Comp] > MaxCompVref[Comp])) {
          MRC_DEBUG_MSG (Debug, MSG_LEVEL_WARNING, "Warning! We saturated the comp vref before we could shift it enough to make the comp shift match the average comp offset. Impedance will only be partially accurate! \n");
          ContinueOptimizing = FALSE;
          break;
        }
        if (CurrSCompPC[Comp] < MRC_BIT1) { // Any valid code will be 0 or 1
          NewCompVref[Comp] = (CurrSCompPC[Comp] << SCOMP_PC_STORAGE_BIT_OFFSET) + NewCompVref[Comp];
        }
        Status = UpdateCompGlobalOffset(MrcData, GlobalParam[Comp], NewCompVref[Comp], FALSE, TRUE, &NewCompValue);
        if (mrcSuccess != Status) {
          MRC_DEBUG_MSG (Debug, MSG_LEVEL_ERROR, "UpdateCompGlobalOffset() error %d\n", Status);
          return mrcFail;
        }
      }
      if (!ContinueOptimizing) {
        break;
      }

      for (Comp = 0; Comp < NumTotalCompCodes; Comp++) {
        MRC_POWER_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Comp Param: %d \n", Comp);
        NewCompCode[Comp] = (UINT8) GetCompCode (MrcData, CompCodeGroup[Comp], CompCodeDirection[Comp]);
        MRC_POWER_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "New comp code: %d\n", NewCompCode[Comp]);

        if ((RESERVED_COMP_CODES > NewCompCode[Comp]) || (NewCompCode[Comp] > (MaxComp[Comp] - RESERVED_COMP_CODES))) {
          MRC_DEBUG_MSG (Debug, MSG_LEVEL_WARNING, "Warning! We saturated the comp before we could shift it as far as the average comp offset. Impedance will only be partially accurate! \n");
          ContinueOptimizing = FALSE;
        }
      }
      if (!ContinueOptimizing) {
        break;
      }

      // Compute average delta across all comps
      CurrentDelta = 0;
      for (Comp = 0; Comp < NumTotalCompCodes; Comp++) {
        if (CompCodeSideEffect[Comp] == TRUE) {
          continue;
        }
        CurrentDelta += ABS(CurrentCompCode[Comp] + AverageCompOffsetPerComp[Comp] - NewCompCode[Comp]);
      }
      if (!ContinueOptimizing) {
        break;
      }
      CurrentDelta = (CurrentDelta / (NumTotalCompCodes - NumTotalSideEffectCompCodes)) + (((CurrentDelta % (NumTotalCompCodes - NumTotalSideEffectCompCodes)) >((NumTotalCompCodes - NumTotalSideEffectCompCodes) / 2)) ? 1 : 0);

      // Move the Comp Vref until the comp outputs change an amount equivalent to the average comp offset
      if (CurrentDelta <= MinDelta) {
        MinDelta = CurrentDelta;
        MrcCall->MrcCopyMem(BestCompVref, NewCompVref, NumTotalCompCodes);
        if (MRC_POWER_TRAINING_DEBUG) {
          for (Comp = 0; Comp < NumTotalCompCodes; Comp++) {
            MRC_POWER_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Comp = %d, BestCompVref = %d \n", Comp, BestCompVref[Comp]);
          }
        }
        if (MinDelta == 0) {
          MRC_POWER_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "(MinDelta == 0) \n");
          break; // We're done
        }
      } else {
        MRC_POWER_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "(CurrentDelta > MinDelta) CurrentDelta = %d, MinDelta = %d \n", CurrentDelta, MinDelta);
        break; // We overshot, so we're done
      }

      CompVrefOffset++;
    }

    // Update new comp vrefs or revert to the initial values. Run this in a seperate loop to ensure all comp vrefs are updated before comps are read
    // WrDsDn , RdOdtDn Vref = 96 (1/2)
    for (Comp = 0; Comp < NumTotalCompCodes; Comp++) {
      Status = UpdateCompGlobalOffset(MrcData, GlobalParam[Comp], BestCompVref[Comp], FALSE, TRUE, &NewCompValue);
      if (mrcSuccess != Status) {
        MRC_DEBUG_MSG (Debug, MSG_LEVEL_ERROR, "UpdateCompGlobalOffset() error %d\n", Status);
        return mrcFail;
      }
    }

    // Read the new comp values
    for (Comp = 0; Comp < NumTotalCompCodes; Comp++) {
      NewCompCode[Comp] = (UINT8)GetCompCode(MrcData, CompCodeGroup[Comp], CompCodeDirection[Comp]);
    }

    // Set the new comp offset values and print them out
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\nFinal Values");
    for (CompOffset = 0; CompOffset < NumCompOffsets; CompOffset++) {
      MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\n Comp Offset Param: %d, side effect = %s", CompOffset, (CompCodeSideEffect[CompOffsetCompIndex[CompOffset]] == TRUE) ? "TRUE" : "FALSE");
      MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\n  Comp code: %d\n  Comp Vref: %d",
        NewCompCode[CompOffsetCompIndex[CompOffset]],
        BestCompVref[CompOffsetCompIndex[CompOffset]]);

      for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
        for (Channel = 0; Channel < Outputs->MaxChannels; Channel++) {
          if ((MrcChannelExist(MrcData, Controller, Channel))) {
            ChannelOut = &Outputs->Controller[Controller].Channel[Channel];
            MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\n    MC%d.Ch%d\n", Controller, Channel);
            for (Byte = 0; Byte < CompOffsetBytes[CompOffset]; Byte++) {
              if (MrcByteExist(MrcData, Controller, Channel, Byte)) {
                MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "      Byte %d , Comp Offset: %d\n", Byte, TotalCompCodes[CompOffset][Controller][Channel][Byte] - NewCompCode[CompOffsetCompIndex[CompOffset]]);
                UpdateOptParamOffset(MrcData, Controller, Channel, 0, Byte, CompOffsetGroup[CompOffset], TotalCompCodes[CompOffset][Controller][Channel][Byte] - NewCompCode[CompOffsetCompIndex[CompOffset]], TRUE);
                if (CompOffsetGroup[CompOffset] == DqDrvUpCompOffset) { // Update DqDrvDnCompOffset to same value as DqDrvUp
                  UpdateOptParamOffset(MrcData, Controller, Channel, 0, Byte, DqDrvDnCompOffset, TotalCompCodes[CompOffset][Controller][Channel][Byte] - NewCompCode[CompOffsetCompIndex[CompOffset]], TRUE);
                }
              }
            }
          }
        }
      }
    }

    ForceSystemRComp(MrcData, 0, TRUE);
  } else {
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\n MinDelta = 0, so comp offsets are already at optimal values. Final values equal initial values.");
  }

  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\n");

  return mrcSuccess;
}

/**
  This step performs Comp Offset optimization on the param list defined in this function.

  @param[in] MrcData - Include all MRC global data.

  @retval MrcStatus - mrcSuccess if succeeded
**/
MrcStatus
MrcOptimizeComp (
  IN MrcParameters *const MrcData
  )
{
  // Comp vrefs that depend on the values of other comp vrefs must be optimized after their dependencies
  // RloadCompOffset is run with CmdRCompDrvUpOffset, as it depends on the same comp vref value
  // DqsOdtCompOffset is run with DqOdtCompOffset, as it depends on the same comp vref
  static const UINT8 ParamList[] = { WrDSUp, RdOdtUp }; // WrDSDn, SCompCmd, SCompCtl, SCompDq, WrDSCmdUp, WrDSCmdDn, WrDSCtlUp, WrDSCtlDn,
  MrcStatus          Status;
  UINT8              Param;

  Status = mrcSuccess;

  for (Param = 0; Param < ARRAY_COUNT (ParamList); Param++) {
    if (OptimizeCompOffset (MrcData, ParamList[Param]) == mrcFail) {
      return mrcFail;
    }
  }
  return Status;
}

/**
  This function calculates the percent of power saving from the power optimization steps and
  updates the proper registers in the PCU.  To get the correct base line for this calculation,
  this routing needs to run first time early in the training in order to update the MrcStruct
  with the base line.  After the power training steps, it will run again to get the actual
  percent of power saving.

  @param[in] MrcData - Include all MRC global data.

  @retval MrcStatus - mrcSuccess

**/
#ifdef FULL_EV_MERGE
MrcStatus
MrcPowerSavingMeter (
  IN MrcParameters *const MrcData
  )
{
  MrcDebug                                *Debug;
  MrcOutput                               *Outputs;
  MrcOdtPowerSaving                       *OdtPowerSaving;
  //UINT8                                   PercentTotal = 0;

  Outputs = &MrcData->Outputs;
  OdtPowerSaving = &Outputs->OdtPowerSavingData;

  if (FIVR_POWER_MEASUREMENT_ENABLED) {
    Debug = &Outputs->Debug;
    CalcSysPower(MrcData, FALSE, 10, NULL, NULL, 0); // mW x10

    /*if (OdtPowerSaving->BaseFlag) {
      if (OdtPowerSaving->BaseSavingTotal > OdtPowerSaving->MrcSavingTotal) {
        PercentTotal = (UINT8)(((UINT32)(OdtPowerSaving->BaseSavingTotal - OdtPowerSaving->MrcSavingTotal) * 256) / OdtPowerSaving->BaseSavingTotal);
      } else {
        PercentTotal = 0;
      }
    } else {
      OdtPowerSaving->BaseFlag = TRUE;
    }

    MRC_DEBUG_MSG(
      Debug,
      MSG_LEVEL_NOTE,
      "\tBaseLine\tMrcSaving\tSaving\nAvgtotal\t%d\t\t%d\t\t%d%%\n",
      OdtPowerSaving->BaseSavingTotal / 10,
      OdtPowerSaving->MrcSavingTotal / 10,
      ((UINT16)PercentTotal) * 100 / 256
    );
  } else {
    if (!OdtPowerSaving->BaseFlag) {
      OdtPowerSaving->BaseFlag = TRUE;
    }*/
  }

  MrcPrintDimmOdtValues (MrcData);  // Print DIMM ODT table

  return mrcSuccess;
}

#endif

/**
  This function assign the pointer to the relevant power Coefficient table to the MRC structure
  offset and apply the difference to the global one.

  @param[in] MrcData        - Include all MRC global data.
  @param[in] Formula        -
  @param[in] ParamVector[MaxTxPowerParam] - Parameter vector of Rx/TxPowerOptParam enum holding the actual value in ohms
  @param[in] VectoreSize    - This vector hold the input params to the RSM equations, it should match the equations order
  @param[in] Scale          - optional Scale to get more precision in integer math

  @retval Result in 10x [mW]
**/
INT32
CalcPowerRSM (
  IN MrcParameters *const       MrcData,
  const MRC_RSM_POWER_FORMULA   *Formula,
  UINT16                        ParamVector[],
  UINT8                         VectoreSize,
  UINT8                         Scale
  )
{
  INT32                           Result;
  const MRC_RSM_POWER_FORMULA     *RSMCoeff;
  const MRC_POWER_COEFF           *iCoeff;
  UINT8                           i;
  UINT8                           j;

  // b0+sum(BiXi)+sum(BiiXi^2)+sum(sum(BijXiXj)) ; i!=j
  RSMCoeff = Formula;
  Result = RSMCoeff->Intercept0;
  for (i=0; i < VectoreSize; i++) {
    iCoeff = &RSMCoeff->Coeff[i];
    Result += (ParamVector[i]) * iCoeff->FirstOrder;
    Result += (ParamVector[i] - iCoeff->Intercept) * (ParamVector[i] - iCoeff->Intercept) * iCoeff->SecondOrder;
    for (j = 0; (j < i);  j++) {
      Result += (ParamVector[i] - iCoeff->Intercept) * (ParamVector[j] - RSMCoeff->Coeff[j].Intercept) * iCoeff->Interactions[j];
    }
  }

  // coeff are mult by 1e6
  Result /= (1000000 / Scale);
  return Result;
}

#if 0
/**
  This function been use in stub MRC mode to export in CSV format the power calculation for validation purpuse
  offset and apply the difference to the global one.

  @param[in] MrcData        - Include all MRC global data.

  @retval None
**/
MrcStatus
PowerCalcverification (
  MrcParameters *MrcData
  )
{
  MrcDebug     *Debug;
  UINT16       ParamVector[6];
  //UINT16 ParamStart[] = {59, 35, 50, 100, 500, 200};
  //UINT16 ParamStop[] = {80, 56, 150, 500, 2500, 800};
  //UINT16 ParamStep[] = {10, 5, 10, 100, 200, 200};

  UINT16 ParamStart[] = {50,  30,  800,   100};
  UINT16 ParamStop[] =  {200, 1000, 1000, 200};
  UINT16 ParamStep[] =  {10,  100,  100,  20};

  MRC_POWER_SYS_CONFIG SysConfig;
  MrcIntOutput         *IntOutputs;

  IntOutputs  = (MrcIntOutput *) MrcData->IntOutputs.Internal;
  Debug       = &MrcData->Outputs.Debug;

  if(0) {
    SysConfig.Data = 0;
    SysConfig.Bits.DdrType = MRC_DDR_TYPE_DDR4; //Outputs->DdrType;
    SysConfig.Bits.Frequency = 1333; //Outputs->Frequency;
    SysConfig.Bits.RttWr = RttWr120;
    GetPowerTable (MrcData, &SysConfig, &(IntOutputs->SysTxPowerFormula), RSMPowerCalcTableTx, sizeof (RSMPowerCalcTableTx) / sizeof (MRC_RSM_POWER_FORMULA));

    // calc Tx Power
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_CSV, "cpu_cell_delay, cpu_ron, cpu_txle, DimmOdtNom, mb_main_len, SoDimmLen, Power %d  %d \n", SysConfig.Bits.RttWr, SysConfig.Bits.Frequency);
    for (ParamVector[0] = ParamStart[0]; ParamVector[0] < ParamStop[0]; ParamVector[0]+=ParamStep[0]) {
      for (ParamVector[1] = ParamStart[1]; ParamVector[1] < ParamStop[1]; ParamVector[1]+=ParamStep[1]) {
        for (ParamVector[2] = ParamStart[2]; ParamVector[2] < ParamStop[2]; ParamVector[2]+=ParamStep[2]) {
          for (ParamVector[3] = ParamStart[3]; ParamVector[3] < ParamStop[3]; ParamVector[3]+=ParamStep[3]) {
            for (ParamVector[4] = ParamStart[4]; ParamVector[4] < ParamStop[4]; ParamVector[4]+=ParamStep[4]) {
              for (ParamVector[5] = ParamStart[5]; ParamVector[5] < ParamStop[5]; ParamVector[5]+=ParamStep[5]) {
                MRC_DEBUG_MSG (Debug, MSG_LEVEL_CSV, "%d, %d, %d, %d, %d, %d, %d\n", ParamVector[0], ParamVector[1], ParamVector[2], ParamVector[3], ParamVector[4], ParamVector[5], CalcPowerRSM (MrcData, IntOutputs->SysTxPowerFormula, ParamVector, 6, 1));
                //MRC_DEBUG_MSG (Debug, MSG_LEVEL_CSV, "%d, %d, %d, %d, %d\n", ParamVector[0], ParamVector[1], ParamVector[2], ParamVector[3], CalcPowerRSM (MrcData, &SysConfig, RsmMarginCalcTableTx, sizeof (RsmMarginCalcTableTx) / sizeof (MRC_RSM_POWER_FORMULA), ParamVector, 3, 1));
              }
            }
          }
        }
      }
    }
  }

  SysConfig.Data = 0;
  SysConfig.Bits.DdrType = MRC_DDR_TYPE_DDR4;//MrcIntputData->DdrType;
  SysConfig.Bits.Frequency = 1867;//MrcIntputData->Frequency;
  SysConfig.Bits.RttWr = 48;
  GetPowerTable (MrcData, &SysConfig, &(IntOutputs->SysRxPowerFormula), RSMPowerCalcTableRx, sizeof (RSMPowerCalcTableRx) / sizeof (MRC_RSM_POWER_FORMULA));

  // calc Rx Power
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_CSV, "CpuOdt, Dram_Odt, Mb_mail_len, Sodimm_len, DimmRon %d  freq %d \n", SysConfig.Bits.RttWr, SysConfig.Bits.Frequency);
  for (ParamVector[0] = ParamStart[0]; ParamVector[0] < ParamStop[0]; ParamVector[0]+=ParamStep[0]) {
    for (ParamVector[1] = ParamStart[1]; ParamVector[1] < ParamStop[1]; ParamVector[1]+=ParamStep[1]) {
      for (ParamVector[2] = ParamStart[2]; ParamVector[2] < ParamStop[2]; ParamVector[2]+=ParamStep[2]) {
        for (ParamVector[3] = ParamStart[3]; ParamVector[3] < ParamStop[3]; ParamVector[3]+=ParamStep[3]) {
              MRC_DEBUG_MSG (Debug, MSG_LEVEL_CSV, "%d, %d, %d, %d, %d\n", ParamVector[0], ParamVector[1], ParamVector[2], ParamVector[3], CalcPowerRSM (MrcData, IntOutputs->SysRxPowerFormula, ParamVector, 4, 1));
              //MRC_DEBUG_MSG (Debug, MSG_LEVEL_CSV, "%d, %d, %d, %d, %d\n", ParamVector[0], ParamVector[1], ParamVector[2], ParamVector[3], CalcPowerRSM (MrcData, &SysConfig, RsmMarginCalcTableTx, sizeof (RsmMarginCalcTableTx) / sizeof (MRC_RSM_POWER_FORMULA), ParamVector, 3, 1));
        }
      }
    }
  }
  return mrcFail;
  //return mrcSuccess;
}

#endif


/**
  This function assign the pointer to the relevant power Coefficient table to the MRC structure
  offset and apply the difference to the global one.
  The search algorithm:
    - We match the DIMM technology,
    - Power.Frequency <= Config.Frequency,
    - Power.RttWr <= Config.RttWr.
    - This is assuming that we pre-arrange (low to high) the table to match this algorithm.

  This function implements the following config search algorithm:
    We match the DIMM technology
    Power.Frequency <= Config.Frequency
    Power.RttWr <= Config.RttWr
  This is assuming that we pre-arrange (low to high) the table to match this algo.

  @param[in] MrcData          - Include all MRC global data.
  @param[in] SysPwrConfig     - Parameter defining the desired system configuration (frequency, ddr type, dimm termination).
  @param[in] SysPowerFormula  -
  @param[in] Table            - Pointer to the relevant power equations table
  @param[in] TableSize        -

  @retval None
**/
void
GetPowerTable (
  IN MrcParameters *const      MrcData,
  MRC_POWER_SYS_CONFIG         *SysPwrConfig,
  const MRC_RSM_POWER_FORMULA  **SysPowerFormula,
  const MRC_RSM_POWER_FORMULA  *Table,
  UINT32                       TableSize
  )
{
  UINT32          Index;

  const MRC_RSM_POWER_FORMULA *ClosestConfig;

  // init Mrc struct if not populated yet
  if ((*SysPowerFormula) == NULL) {
    (*SysPowerFormula) = Table;
  }

  ClosestConfig = &Table[0];
  if ((*SysPowerFormula)->Config.Data != SysPwrConfig->Data) {
    for (Index = 0; Index < TableSize; Index++) {
      if (SysPwrConfig->Data == Table[Index].Config.Data) {
        (*SysPowerFormula) = &Table[Index];
        return;
      } else {
        if (Table[Index].Config.Bits.DdrType <= SysPwrConfig->Bits.DdrType) {
          if (Table[Index].Config.Bits.Frequency <= SysPwrConfig->Bits.Frequency) {
            if (Table[Index].Config.Bits.RttWr <= SysPwrConfig->Bits.RttWr) {
              ClosestConfig = &Table[Index];
            }
          }
        }
      }
    }
    (*SysPowerFormula) = ClosestConfig;
  }
}

/**
  This function reads the selected comp code.
  In case of comp up/dn we select the value that is closer to saturation (0 or 63).
  Safe assumption is that up/dn codes don't differ by too much.
  So if one of the codes is in the upper range (more than 32), we select MAX of both, otherwise we select MIN of both.

  @param[in] MrcData  - Include all MRC global data.
  @param[in] OptParam - Parameter to read the relevant comp code.
  @param[in] UpDown   - 0 : up only. 1 : down only. 2 : use min/max of both Up and Down codes.

  @retval The selected Comp code
**/
UINT8
GetCompCode (
  IN OUT MrcParameters *const MrcData,
  IN     UINT8                OptParam,
  IN     UINT8                UpDown
  )
{
  MrcDebug                            *Debug;
  UINT32                              CurrentComp;
  UINT32                              UpCode;
  UINT32                              DownCode;
  INT64                               GetSetVal;
  GSM_GT                              GroupUp;
  GSM_GT                              GroupDn;
  BOOLEAN                             ReadReg;
  BOOLEAN                             Read2Regs;

  Debug  = &MrcData->Outputs.Debug;

  CurrentComp = 0;
  UpCode = DownCode = 0;

  if ((OptParam == OptRdOdt || OptParam == OptRdDqOdt || OptParam == OptRdDqsOdt) && (MrcData->Outputs.OdtMode == MrcOdtModeVtt)) {
    // in Vtt mode only odt up is valid
    UpDown = 0;
  }

  ReadReg   = TRUE;
  Read2Regs = TRUE;
  switch (OptParam) {
    case OptCmdDSUpCoarse:
      Read2Regs = FALSE;
      GroupUp = WrDSCodeUpCmd;
      GroupDn = WrDSCodeUpCmd;
      break;

    case OptCmdDSDnCoarse:
      Read2Regs = FALSE;
      GroupUp = WrDSCodeDnCmd;
      GroupDn = WrDSCodeDnCmd;
      break;

    case OptWrDSUpCoarse:
      Read2Regs = FALSE;
      GroupUp = TxRonUp;
      GroupDn = TxRonUp;
      break;

    case OptWrDSDnCoarse:
      Read2Regs = FALSE;
      GroupUp = TxRonDn;
      GroupDn = TxRonDn;
      break;

    case OptWrDS:
    case OptTxEq:
      GroupUp = TxRonUp;
      GroupDn = TxRonDn;
      break;

    case OptCCCTxEq:
    case OptCmdDS:
      GroupUp = WrDSCodeUpCmd;
      GroupDn = WrDSCodeDnCmd;
      break;

    case OptCtlDS:
      GroupUp = WrDSCodeUpCtl;
      GroupDn = WrDSCodeDnCtl;
      break;

    case OptClkDS:
      GroupUp = WrDSCodeUpClk;
      GroupDn = WrDSCodeDnClk;
      break;

    case OptRdDqsOdt:
    case OptRdDqOdt:
    case OptRdOdt:
      GroupUp = CompRcompOdtUp;
      GroupDn = CompRcompOdtDn;
      break;

    case OptRxLoad:
      // TGL_POWER_TRAINING Add this
      Read2Regs = FALSE;
      GroupUp = 0;// GroupUp = RloadDqsDn;
      GroupDn = 0;// GroupDn = RloadDqsDn;
      break;

    case OptSComp:
      Read2Regs = FALSE;
      GroupUp = SCompCodeDq;
      GroupDn = SCompCodeDq;
      break;

    case OptCCCSComp:
        Read2Regs = FALSE;
        GroupUp = SCompCodeCmd;
        GroupDn = SCompCodeCmd;
        break;

    case OptTxDqTco:
      Read2Regs = FALSE;
      GroupUp = TxTco;
      GroupDn = TxTco;
      break;

    case OptTxDqsPTco:
        Read2Regs = FALSE;
        GroupUp = TxDqsTcoP;
        GroupDn = TxDqsTcoP;
        break;

    case OptTxDqsNTco:
      Read2Regs = FALSE;
      GroupUp = TxDqsTcoP;
      GroupDn = TxDqsTcoP;
      break;

    default:
      ReadReg = FALSE;
      GroupUp = 0;
      GroupDn = 0;
      MRC_DEBUG_MSG (Debug, MSG_LEVEL_ERROR, "GetCompCode(): Invalid OptParam %d\n", OptParam);
      break;
  }

  if (ReadReg) {
    // TGL_POWER_TRAINING - Need to support reading from TxDqsTco, WrDSCodeUpCmd/WrDSCodeDnCmd, WrDSCodeUpCtl/WrDSCodeDnCtl, WrDSCodeUpClk/WrDSCodeDnClk, SCompCodeCmd, RloadDqsDn
    // TxDqsTcoP:
    //     DDRDATA0CH0_CR_DDRCRDATATCOCONTROL1.DQSTcocomp
    // WrDSCodeUpCmd/WrDSCodeDnCmd:
    //     DDRCCC_CR_DDRCRCMDCOMP.RcompDrvUp/Down
    // WrDSCodeUpCtl/WrDSCodeDnCtl:
    //     DDRCCC_CR_DDRCRCTLCOMP.RcompDrvUp/Down
    // WrDSCodeUpClk/WrDSCodeDnClk:
    //     DDRCCC_CR_DDRCRCLKCOMP.RcompDrvUp/Down
    // SCompCodeCmd:
    //     DDRCCC_CR_DDRCRCTLCOMP.Scomp
    //     DDRCCC_CR_DDRCRCLKCOMP.Scomp
    //     DDRCCC_CR_DDRCRCMDCOMP.Scomp
    // RloadDqsDn:
    //     DDRDLL_CR_DdrCrVccDllCompDll.RloadDqs
    MrcGetSetNoScope (MrcData, GroupUp, ReadUncached, &GetSetVal);
    UpCode   = (UINT32) GetSetVal;
    if (Read2Regs) {
      MrcGetSetNoScope (MrcData, GroupDn, ReadUncached, &GetSetVal);
      DownCode = (UINT32) GetSetVal;
    } else {
      DownCode = UpCode;
    }
  } else {
    UpCode = 0;
  }

  if (UpDown == 2) {
    CurrentComp = (UpCode > 32) ?  MAX (UpCode, DownCode) : MIN (UpCode, DownCode);
  } else if (UpDown == 1) {
    CurrentComp = DownCode;
  } else {
    CurrentComp = UpCode;
  }

  return (UINT8) CurrentComp;
}

/**
  Program COMP Vref offset according to the passed parameter and target values.

  @param[in] MrcData      - Include all MRC global data.
  @param[in] Param        - COMP Vref parameter (see TGlobalCompOffset).
  @param[in] RcompTarget  - Array of the Rcomp Targets COMP for { DqOdt, DqDrv, CmdDrv, CtlDrv, ClkDrv }
  @param[in] UpdateHost   - Update host struct with the new value or not.

  @retval mrcSuccess  - if Param is a valid COMP Vref parameter
**/
MrcStatus
UpdateCompTargetValue (
  MrcParameters *const MrcData,
  UINT8                Param,
  UINT16               RcompTarget[MAX_RCOMP_TARGETS],
  BOOLEAN              UpdateHost
  )
{
  MrcDebug   *Debug;
  MrcInput   *Inputs;
  MrcOutput  *Outputs;
  UINT32     Numerator;
  UINT32     Denominator;
  INT16      CompVrefUp;
  INT16      CompVrefDn;
  UINT16     ReferenceRUp;
  UINT16     ReferenceRDn;
  UINT16     TargetUpValue;
  UINT16     TargetDnValue;
  UINT8      NumOfSegments;
  UINT8      CompCodeUp;
  UINT8      CompCodeDn;
  BOOLEAN    VttOdt;
  CompGlobalOffsetParam GlobalCompUp;
  CompGlobalOffsetParam GlobalCompDn;
  MrcStatus  Status;
  UINT32     NewCompValue;

  Inputs   = &MrcData->Inputs;
  Outputs  = &MrcData->Outputs;
  Debug    = &Outputs->Debug;
  VttOdt   = (Outputs->OdtMode == MrcOdtModeVtt);
  if (Param >= MAX_RCOMP_TARGETS) {
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_ERROR, "Error! Bad Param : %d\n", Param);
    return mrcFail;
  }

  TargetDnValue = TargetUpValue = RcompTarget[Param];
  ReferenceRDn = RcompTarget[Param];

  // Rext = 100 Ohm
  // DQ, DQS, ODT Ron Groups (DQ Tx Type - 1 segment):
  // Vref_pupcode = 192 * Rext / (Rext + Rtarg_pup)
  // Since the pulldown strength is relative to the pullup strength, the Vref / Vsupply ratio is always 0.5.
  // Vref_pdncode = 192 * 0.5 = 96

  // CMD, CTL, CLK, CKECS Ron Groups (CCC Tx Type - 2 segments) :
  // Vref_pupcode = 192 * Rext / (Rext + 2 * Rtarg_pup)
  // Vref_pdncode = 192 * 0.5 = 96

  //@todo_adl ADL-P add ckecsvrefup
  NumOfSegments = TWO_SEGMENTS;
  switch (Param) {
    case RdOdt:
      NumOfSegments = ONE_SEGMENT;
      GlobalCompUp = RdOdtUp;
      GlobalCompDn = RdOdtDn;
      break;

    case WrDS:
      NumOfSegments = ONE_SEGMENT;
      GlobalCompUp = WrDSUp;
      GlobalCompDn = WrDSDn;
      break;

    case WrDSClk:
      GlobalCompUp = WrDSClkUp;
      GlobalCompDn = WrDSClkDn;
      break;

    case WrDSCmd:
      GlobalCompUp = WrDSCmdUp;
      GlobalCompDn = WrDSCmdDn;
      break;

    case WrDSCtl:
      GlobalCompUp = WrDSCtlUp;
      GlobalCompDn = WrDSCtlDn;
      break;

    default:
      GlobalCompUp = 0;
      GlobalCompDn = 0;
      break;
  }

  ReferenceRUp = DIVIDEROUND (Inputs->RcompResistor, NumOfSegments);
  Numerator    = MRC_COMP_VREF_STEP_SIZE * ReferenceRUp;
  Denominator  = TargetUpValue + ReferenceRUp;
  if (VttOdt && (Param == RdOdt)) {
    //@todo_adl ADL-P VTT : Vref_pupcode = Vref_pdncode = 192 * ((VccDD2 / 2) / VccDDQ) * Rext / (Rext + Rtarg_vtt)
    Denominator *= 2;
  }
  Denominator = MAX (Denominator, 1);
  // Used UINT32 to prevent overflow of multiply with large UINT16 numbers.
  // Result should be less than UINT8 Max as register field is smaller than UINT8.
  CompVrefUp = (UINT16) DIVIDEROUND (Numerator, Denominator);

  Numerator    = MRC_COMP_VREF_STEP_SIZE * ReferenceRDn;
  Denominator  = TargetDnValue + ReferenceRDn;
  if (VttOdt && (Param == RdOdt)) {
    CompVrefDn = CompVrefUp;
  } else {
    Denominator = MAX(Denominator, 1);
    // Used UINT32 to prevent overflow of multiply with large UINT16 numbers.
    // Result should be less than UINT8 Max as register field is smaller than UINT8.
    CompVrefDn = (UINT16)DIVIDEROUND(Numerator, Denominator);
  }

  // Callee handles saturation at Min/Max values.
  Status = UpdateCompGlobalOffset (MrcData, GlobalCompUp, CompVrefUp, TRUE, UpdateHost, &NewCompValue);
  if (mrcSuccess != Status) {
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_ERROR, "UpdateCompGlobalOffset() error %d\n", Status);
    return mrcFail;
  }
  CompCodeUp = (UINT8) NewCompValue;

  Status = UpdateCompGlobalOffset (MrcData, GlobalCompDn, CompVrefDn, TRUE, UpdateHost, &NewCompValue);
  if (mrcSuccess != Status) {
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_ERROR, "UpdateCompGlobalOffset() error %d\n", Status);
    return mrcFail;
  }
  CompCodeDn = (UINT8) NewCompValue;

  MRC_DEBUG_MSG (
    Debug,
    MSG_LEVEL_NOTE,
    "%8s: Target (Up/Dn): %3d/%3d ReferenceR (Up/Dn): %3d/%3d VrefOffset (Up/Dn): %3d/%3d Current Comp code (Up/Dn): %3d/%3d\n",
    GlobalCompOffsetStr[Param],
    TargetUpValue,
    TargetDnValue,
    ReferenceRUp,
    ReferenceRDn,
    CompVrefUp,
    CompVrefDn,
    CompCodeUp,
    CompCodeDn
    );

  if ((CompCodeUp <= 0) || (CompCodeUp >= 63) || (CompCodeDn <= 0) || (CompCodeDn >= 63)) {
    MRC_DEBUG_MSG(Debug, MSG_LEVEL_WARNING, "WARNING: COMP code is saturated !\n");
  }

  return mrcSuccess;
}
/**
  This function returns the params setting accordingly to input.

  @param[in out] ParamOff- Parameters result Offsets
  @param[in]     GridMode- Selects the way we sweep the params
  @param[in]     Index   - Linear Index for all params
  @param[in]     YLen    - Determines the Y-Dimension lengths
  @param[in]     XLen    - Determines the X-Dimension lengths


  @retval if point is valid (tested)
**/
BOOLEAN
GetParamsXYZ (
  IN     MrcParameters      *const MrcData,
  IN OUT INT8              *ParamOff,
  IN     const UINT8        OptParamLen,
  IN     const UINT8        GridMode,
  IN     const UINT8        Index,
  IN     const INT8         *Start,
  IN     const UINT8        *ParamOffLen
)

{
  MrcDebug          *Debug;
  INT8              XOff;
  INT8              YOff;
  BOOLEAN           SkipPoint = FALSE;

  Debug  = &MrcData->Outputs.Debug;

  if (OptParamLen == 1) {
    switch (GridMode) {
      case FullGrid:
        break;
      case CustomSR:
        if (Index == 0) {
          ParamOff[0] = -17; // disable SR for fastest SR
        }
        break;
      default:
        MRC_DEBUG_MSG(Debug, MSG_LEVEL_NOTE, "unknow GridMode %d\t", GridMode);
    }
    if (!((GridMode == CustomSR) && Index == 0)) {
      ParamOff[0] = Index + Start[0];
    }
  } else if (OptParamLen == 2) {
    XOff =  Index % ParamOffLen[0];
    YOff =  Index / ParamOffLen[0];
    switch (GridMode) {
    case FullGrid:
      break;
    case ChessOdd:
      if (XOff % 2) {
        if (YOff % 2) {
          SkipPoint = TRUE;
        }
      } else {
        if (YOff % 2 == 0) {
          SkipPoint = TRUE;
        }
      }
      break;
    case ChessEven:
      if (XOff % 2) {
        if (YOff % 2 == 0) {
          SkipPoint = TRUE;
        }
      } else {
        if (YOff % 2) {
          SkipPoint = TRUE;
        }
      }
      break;
      case DecapSweep:
        if (((Index % MAX_DECAP_VALUES) - (MAX_DECAP_VALUES - Index / MAX_DECAP_VALUES)) > 0) {
          SkipPoint = TRUE;
        }
        break;
      case UpDnCompOffsetSweepRange1:
        if ((XOff + Start[0]) - (YOff + Start[1]) > 1 || (XOff + Start[0]) - (YOff + Start[1]) < -1) {
          SkipPoint = TRUE;
        }
        break;
      case UpDnCompOffsetSweepRange8:
        if ((XOff + Start[0]) - (YOff + Start[1]) > 8 || (XOff + Start[0]) - (YOff + Start[1]) < -8) {
          SkipPoint = TRUE;
        }
        break;
      default:
      MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "unknow GridMode %d\t", GridMode);
    }

    ParamOff[0] = XOff + Start[0];
    ParamOff[1] = YOff + Start[1];
  }

  if (GridMode == Reversed1D) {
    ParamOff[0] =  Start[0] + ParamOffLen[0] - 1 - Index; // reversed ordering for param
  }

    return SkipPoint;
}

/**
  Returns the index into RttType array

  @param[in] OptDimmOdt - DimmOdt type

  @retval One of the following values: RttWr RttNom RttPark

**/
DimmOdtType
GetRttType (
  IN const UINT8 OptDimmOdt
  )
{
  switch (OptDimmOdt) {
    case OptDimmOdtWr:
      return RttWr;

    case OptDimmOdtNom:
    case OptDimmOdtNomNT:
      return RttNom;

    case OptDimmOdtPark:
    case OptDimmOdtParkNT:
      return RttPark;

    default:
      break;
  }

  return RttMaxType;
}

/**
This function is used to train the Tx TCO Comp offset for Dq.
TCO Comp performs training using fixed pattern, in order to avoid optimization of TCO Comp based on ISI / Crosstalk
In order to leverage for very high margins resulting of fixed pattern, we ignore the power / UPM limits.

@param[in] MrcData - Pointer to global MRC data.

@retval - mrcSuccess
**/
MrcStatus
MrcTxDqTcoCompTraining (
  IN MrcParameters *const MrcData
  )
{
  UINT16              SaveMarginsArray[3][MAX_OPT_POINTS][MAX_CONTROLLER][MAX_BYTE_IN_DDR5_CHANNEL * MAX_DIMMS_IN_CHANNEL]; // Must match test list size
  static const UINT8  TestList[] = { WrT, RdT, CmdT }; // @TGL - This affects RxDqs sometimes, need to take that into consideration
  static const UINT8  OptParam[] = { OptTxDqTco };
  UINT8               Scale[] = { 1, 1, 1, 255, 0 }; // Ignore power optimization
  OptOffsetChByte     BestOff;
  INT8                Start;
  INT8                Stop;
  MrcOutput           *Outputs;

  Outputs = &MrcData->Outputs;

  // Coarse search for Global comp
  Start = OptParamLimitValue (MrcData, OptParam[0], 0);
  Stop  = OptParamLimitValue (MrcData, OptParam[0], 1);

  TrainDDROptParam (
    MrcData,
    &BestOff,
    MrcData->Outputs.ValidChBitMask,
    MrcData->Outputs.ValidRankMask,
    OptParam,
    ARRAY_COUNT (OptParam),
    FullGrid,
    TestList,
    ARRAY_COUNT (TestList),
    Scale,
    NULL,
    &Start,
    &Stop,
    OPT_PARAM_LOOP_COUNT + 2,
    1,                    // Repeats
    0,                    // NoPrint
    0,                    // SkipOptUpdate
    0,                    // GuardBand
    0, //StaticPattern         // PatType
    SaveMarginsArray,
    FALSE, // UPMFilterEnable
    NULL,  // TestUpmArray
    FALSE  // CliffsFilterEnable
    );

  MRC_DEBUG_MSG(&Outputs->Debug, MSG_LEVEL_NOTE, "Re-center Read Timing \n");
  //DQTimeCentering1D_RxDC (MrcData, 0, T_RECENTERING_STEP_SIZE, OPT_PARAM_LOOP_COUNT, MRC_PWR_DBG_PRINT, FALSE, 1);

  MRC_DEBUG_MSG(&Outputs->Debug, MSG_LEVEL_NOTE, "Re-center Write Timing \n");
  //DQTimeCentering1D(MrcData, WrT, 0, T_RECENTERING_STEP_SIZE, OPT_PARAM_LOOP_COUNT, MRC_PWR_DBG_PRINT, FALSE, 1); // No need to use WrTLp4 after basic training

  return mrcSuccess;
}

/**
  This function is used to train the Tx TCO Comp offset for Dqs.
  TcoDqs trained per byte.

  @param[in] MrcData - Pointer to global MRC data.

  @retval - mrcSuccess
**/
MrcStatus
MrcTxDqsTcoCompTraining (
  IN MrcParameters *const MrcData
  )
{
  UINT16              SaveMarginsArray[3][MAX_OPT_POINTS][MAX_CONTROLLER][MAX_BYTE_IN_DDR5_CHANNEL * MAX_DIMMS_IN_CHANNEL]; // Must match test list size
  static const UINT8  TestList[] = { WrT, RdT, CmdT };
  UINT8  OptParam[] = { OptTxDqsPTco };
  UINT8               Scale[] = { 1, 1, 1, 255, 0 }; // Ignore power optimization
  OptOffsetChByte     BestOff;
  INT8                Start;
  INT8                Stop;
  MrcOutput           *Outputs;

  Outputs = &MrcData->Outputs;

  // Coarse search for Global comp
  Start = OptParamLimitValue (MrcData, OptParam[0], 0);
  Stop = OptParamLimitValue (MrcData, OptParam[0], 1);

  TrainDDROptParam (
    MrcData,
    &BestOff,
    MrcData->Outputs.ValidChBitMask,
    MrcData->Outputs.ValidRankMask,
    OptParam,
    ARRAY_COUNT (OptParam),
    FullGrid,
    TestList,
    ARRAY_COUNT (TestList),
    Scale,
    NULL,
    &Start,
    &Stop,
    OPT_PARAM_LOOP_COUNT + 2,
    1,                    // Repeats
    0,                    // NoPrint
    0,                    // SkipOptUpdate
    0,                    // GuardBand
    0,                     // PatType
    SaveMarginsArray,
    FALSE, // UPMFilterEnable
    NULL,  // TestUpmArray
    FALSE  // CliffsFilterEnable
    );

  OptParam[0] = OptTxDqsNTco;

  TrainDDROptParam(
    MrcData,
    &BestOff,
    MrcData->Outputs.ValidChBitMask,
    MrcData->Outputs.ValidRankMask,
    OptParam,
    ARRAY_COUNT(OptParam),
    FullGrid,
    TestList,
    ARRAY_COUNT(TestList),
    Scale,
    NULL,
    &Start,
    &Stop,
    OPT_PARAM_LOOP_COUNT + 2,
    1,                    // Repeats
    0,                    // NoPrint
    0,                    // SkipOptUpdate
    0,                    // GuardBand
    0,                     // PatType
    SaveMarginsArray,
    FALSE, // UPMFilterEnable
    NULL,  // TestUpmArray
    FALSE  // CliffsFilterEnable
  );

  MRC_DEBUG_MSG(&Outputs->Debug, MSG_LEVEL_NOTE, "Re-center Write Timing \n");
  //DQTimeCentering1D(MrcData, WrT, 0, T_RECENTERING_STEP_SIZE, OPT_PARAM_LOOP_COUNT, MRC_PWR_DBG_PRINT, FALSE, 1); // No need to use WrTLp4 after basic training

  return mrcSuccess;
}

/**
  This function is used to train the Clk TCO Comp code.

  @param[in] MrcData - Pointer to global MRC data.

  @retval - mrcSuccess
**/
MrcStatus
MrcClkTcoCompTraining (
  IN MrcParameters *const MrcData
)
{
  UINT16              SaveMarginsArray[1][MAX_OPT_POINTS][MAX_CONTROLLER][MAX_BYTE_IN_DDR5_CHANNEL * MAX_DIMMS_IN_CHANNEL]; // Must match test list size
  static const UINT8  TestList[] = { CmdT };
  static const UINT8  OptParam[] = { OptCCCTco };
  UINT8               Scale[] = { 1, 0, 0, 0, 0 };
  OptOffsetChByte     BestOff;
  INT8                Start;
  INT8                Stop;
  MrcStatus           Status = mrcSuccess;

  Start = OptParamLimitValue (MrcData, OptParam[0], 0);
  Stop  = OptParamLimitValue (MrcData, OptParam[0], 1);

  TrainDDROptParam (
    MrcData,
    &BestOff,
    MrcData->Outputs.ValidChBitMask,
    MrcData->Outputs.ValidRankMask,
    OptParam,
    ARRAY_COUNT (OptParam),
    FullGrid,
    TestList,
    ARRAY_COUNT (TestList),
    Scale,
    NULL,
    &Start,
    &Stop,
    OPT_PARAM_LOOP_COUNT,
    1,                    // Repeats
    0,                    // NoPrint
    0,                    // SkipOptUpdate
    0,                    // GuardBand
    StaticPattern,         // PatType
    SaveMarginsArray,
    FALSE, // UPMFilterEnable
    NULL,  // TestUpmArray
    FALSE  // CliffsFilterEnable
    );

  // Re-Center CMD Timing and voltage and update Host Struct with new center
  Status = MrcCmdTimingCentering(MrcData, MrcData->Outputs.ValidChBitMask, OPT_PARAM_LOOP_COUNT, TRUE, MRC_PRINTS_ON, 1);
  if (Status != mrcSuccess) {
    return Status;
  }

  return mrcSuccess;
}

// This is for debug
#if 0
void
GetMarginsByte4Debug (
  IN OUT MrcParameters *const MrcData,
  IN     UINT8                ChannelMask,
  IN     UINT8                RankMask,
  IN     UINT8                Param,
  IN     UINT16               PwrLimit,
  IN     UINT8                LoopCount,
  IN     UINT8                Repeats,
  IN     BOOLEAN              NoPrint
  )

{
  const MRC_FUNCTION *MrcCall;
  MrcDebug          *Debug;
  MrcOutput         *Outputs;
  MrcChannelOut     *ChannelOut;
  MrcControllerOut  *ControllerOut;
  MrcStatus         Status;
  UINT32            (*MarginByte)[MAX_RESULT_TYPE][MAX_RANK_IN_CHANNEL][MAX_CHANNEL][MAX_SDRAM_IN_DIMM][MAX_EDGES];
  UINT32            BERStats[4];
  UINT16            SaveMargin[MAX_MARGINS_TRADEOFF][MAX_OPT_POINTS][MAX_CHANNEL][MAX_SDRAM_IN_DIMM][MAX_EDGES];
  UINT16            MinEdge;
  UINT8             ChBitMask;
  UINT8             Channel;
  UINT8             localR[MAX_CHANNEL];
  UINT8             Byte;
  UINT8             Rank;
  UINT8             Edge;
  UINT8             NumBytes;
  UINT8             BMap[9]; // Need by GetBERMarginByte
  UINT8             MaxMargin;
  UINT8             Rep;
  UINT8             FirstRank;
  UINT8             ResultType;
  BOOLEAN           WriteVrefParam;

  Outputs             = &MrcData->Outputs;
  Debug               = &Outputs->Debug;
  MarginByte          = &Outputs->MarginResult;
  ChannelMask        &= Outputs->ValidChBitMask;
  RankMask           &= Outputs->ValidRankMask;
  ControllerOut       = &Outputs->Controller[0];
  MrcCall             = MrcData->Inputs.Call.Func;
  WriteVrefParam      = ((Param == WrV) || (Param == WrFan2) || (Param == WrFan3));

  MrcCall->MrcSetMem ((UINT8 *) BERStats, sizeof (BERStats), 0);
  for (Byte = 0; Byte < ARRAY_COUNT (BMap); Byte++) {
    BMap[Byte] = Byte;
  }

  // @todo Update with McChBitMask
  SetupIOTestBasicVA (MrcData, ChannelMask, LoopCount, 0, 0, 0, 8, PatWrRd, 0, 0); // set test to all channels

  // Select All Ranks for REUT test
  ChBitMask = 0;
  for (Channel = 0; Channel < MAX_CHANNEL; Channel++) {
    if (!((MRC_BIT0 << Channel) & ChannelMask)) {
      continue;
    }


    ChannelOut      = &ControllerOut->Channel[Channel];
    localR[Channel] = ChannelOut->ValidRankBitMask & RankMask;

    // use ChBitMask from here down - if ch is set that mean at least 1 rank for testing, also remove ch w/o active ranks
    ChBitMask |= SelectReutRanks (MrcData, Channel, localR[Channel], FALSE, 0);
    // MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "reut ranks ChBitMask %x Local ranks=%x\n", ChBitMask,localR[Channel]);
  }

  if (ChBitMask == 0) {
    return ;
  }

  // Find the first selected rank
  FirstRank = 0;
  for (Rank = 0; Rank < MAX_RANK_IN_CHANNEL; Rank++) {
    if ((MRC_BIT0 << Rank) & RankMask) {
      FirstRank = Rank; // could be in any channel
      break;
    }
  }

  ResultType = GetMarginResultType (Param);

  MaxMargin = MAX_POSSIBLE_TIME;
  if ((Param == RdV) ||
      (Param == RdFan2) ||
      (Param == RdFan3) ||
       WriteVrefParam
     ) {
   MaxMargin = GetVrefOffsetLimits (MrcData, Param);
  }

  // No need to search too far
  if (MaxMargin > (PwrLimit / 20)) {
    MaxMargin = (UINT8) (PwrLimit / 20);
  }
  NumBytes = (UINT8) Outputs->SdramCount;
  for (Rep = 0; Rep < Repeats; Rep++) {
    // Run Margin Test - margin_1d with chosen param
    // run on all ranks but change param only for firstRank??
    Outputs->PdaEnable = TRUE;
    Status = MrcGetBERMarginByte (
      MrcData,
      Outputs->MarginResult,
      ChBitMask,
      RankMask,
      RankMask,
      Param,
      0,  // Mode
      BMap,
      1,
      MaxMargin,
      0,
      BERStats
      );

    // MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, " finish MrcGetBERMarginByte \n");
    // Record Results
    for (Channel = 0; Channel < MAX_CHANNEL; Channel++) {
      if (!((MRC_BIT0 << Channel) & ChBitMask)) {
        continue;
      }

      for (Edge = 0; Edge < MAX_EDGES; Edge++) {
        MinEdge = 0xFFFF;
        for (Byte = 0; Byte < Outputs->SdramCount; Byte++) {
          if (Rep == 0) {
            SaveMargin[0][0][Channel][Byte][Edge] = 0;
          }

          SaveMargin[0][0][Channel][Byte][Edge] +=
            (UINT16) (*MarginByte)[ResultType][FirstRank][Channel][Byte][Edge];
          if (MinEdge > SaveMargin[0][0][Channel][Byte][Edge]) {
            MinEdge = SaveMargin[0][0][Channel][Byte][Edge];
          }
        }
        if (NumBytes == 1) {
          SaveMargin[0][0][Channel][0][Edge] = MinEdge; // Todo:change Byte->0
        }
      }
    }
  }
#ifdef MRC_DEBUG_PRINT
  PrintResultTableByte4by24 (
    MrcData,
    ChBitMask,
    SaveMargin,
    0,
    1,
    0,
    2,
    Param,
    Param,
    &PwrLimit,
    NoPrint //NoPrint
    );
#endif
}

#endif // #if 0

// This is for debug
#if 0
void
PointTest (
  IN OUT MrcParameters *const MrcData,
  IN     UINT8                ChannelMask,
  IN     UINT8                RankMask,
  IN     UINT8                LoopCount,
  IN     UINT8                NumCL,
  IN     BOOLEAN              NoPrint
  )

{
  const MRC_FUNCTION *MrcCall;
  MrcDebug           *Debug;
  MrcOutput         *Outputs;
  MrcChannelOut     *ChannelOut;
  MrcControllerOut  *ControllerOut;
  UINT32            BERStats[4];
  UINT32            Offset;
  UINT8             ChBitMask;
  UINT8             Channel;
  UINT8             SubCh;
  UINT8             localR[MAX_CHANNEL];
  UINT8             Byte;
  UINT8             Rank;
  UINT8             BMap[9]; // Need by GetBERMarginByte
  UINT16            Results;
  UINT64            ErrStatus;
  UINT32            ErrCount;
  BOOLEAN           Overflow;
  UINT8             FirstRank;
  MCDFXS_CR_REUT_GLOBAL_CTL_MCMAIN_STRUCT                         ReutGlobalCtl;
  Outputs             = &MrcData->Outputs;
  Debug               = &Outputs->Debug;
  ChannelMask        &= Outputs->ValidChBitMask;
  RankMask           &= Outputs->ValidRankMask;
  ControllerOut       = &Outputs->Controller[0];
  MrcCall             = MrcData->Inputs.Call.Func;
  MrcCall->MrcSetMem ((UINT8 *) BERStats, sizeof (BERStats), 0);
  for (Byte = 0; Byte < ARRAY_COUNT (BMap); Byte++) {
    BMap[Byte] = Byte;
  }
  // @todo Update with McChBitMask
  SetupIOTestBasicVA (MrcData, ChannelMask, LoopCount, 0, 0, 0, 8, PatWrRd, 0, 0); // set test to all channels
  // Select All Ranks for REUT test
  ChBitMask = 0;
  for (Channel = 0; Channel < MAX_CHANNEL; Channel++) {
    ChannelOut      = &ControllerOut->Channel[Channel];
    localR[Channel] = ChannelOut->ValidRankBitMask & RankMask;
    // use ChBitMask from here down - if ch is set that mean at least 1 rank for testing, also remove ch w/o active ranks
    ChBitMask |= SelectReutRanks (MrcData, Channel, localR[Channel], FALSE, 0);
    // MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "reut ranks ChBitMask %x Local ranks=%x\n", ChBitMask,localR[Channel]);
  }
  if (ChBitMask == 0) {
    return;
  }
  // Find the first selected rank
  FirstRank = 0;
  for (Rank = 0; Rank < MAX_RANK_IN_CHANNEL; Rank++) {
    if ((MRC_BIT0 << Rank) & RankMask) {
      FirstRank = Rank; // could be in any channel
      break;
    }
  }
  for (Channel = 0; Channel < MAX_CHANNEL; Channel++) {
    if (!((MRC_BIT0 << Channel) & ChBitMask)) {
      continue;
    }
    for (Byte = 0; Byte < Outputs->SdramCount; Byte++)  {
      // Setup REUT Error Counters to count errors per Byte Group, 1 count per UI with an error across all lanes in the Byte Group, all UI
      MrcSetupErrCounterCtl (MrcData, BMap[Byte], ErrCounterCtlPerByte, 0);
    }
  }
  ReutGlobalCtl.Data = 0;
  ReutGlobalCtl.Bits.Global_Clear_Errors = 1;
  MrcWriteCR (MrcData, MCDFXS_CR_REUT_GLOBAL_CTL_MCMAIN_REG, ReutGlobalCtl.Data); // Clear errors
  // Run Test
  RunIOTest (MrcData, /**McBitMask**/ 1, ChBitMask, Outputs->DQPat, 0, 0);
  for (Channel = 0; Channel < MAX_CHANNEL; Channel++) {
    if (!((MRC_BIT0 << Channel) & ChBitMask)) {
      continue;
    }
    Results = 0;
    for (SubCh = 0; SubCh < Outputs->SubChCount; SubCh++) {
      // Read out per byte error results
      MrcGetMiscErrStatus (MrcData,  /**Controller**/ 0, Channel, ByteGroupErrStatus, &ErrStatus);
      Results |= MrcCall->MrcLeftShift64 (ErrStatus, (SubCh == 0) ? 0 : 4);
    }
  }
  for (Channel = 0; Channel < MAX_CHANNEL; Channel++) {
    if (!((MRC_BIT0 << Channel) & ChBitMask)) {
      continue;
    }
    for (Byte = 0; Byte < Outputs->SdramCount; Byte++) {
      // Read Error Count
      MrcGetErrCounterStatus (MrcData, /*Controller*/ CONTROLLER_0, Channel, Byte, ErrCounterCtlPerByte, &ErrCount, &Overflow);
      if (!NoPrint) {
        MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Ch%d Byte=%d Error Counter:%d\n", Channel, Byte, ErrCount);
      }
    }
  }
}
#endif // #if 0

/**
  This function will handle
  For RxVref:
  1. The Rx VREF sweep training feedback
  For VOC:
  1. Program the VOC value for the rest of the bits (bit 1 to 7)
  2. VOC sweep training feedback.

  @param[in] MrcData         - Include all MRC global data
  @param[in] Group           - Group number
  @param[in] NumberElements  - The strobe numbers
  @param[in OUT] PassFail    - Training feedback result
  @param[in] VocSweep        - VOC Sweep or not

  @retval mrcSuccess    - function runs successfully
**/
MrcStatus
VrefValueToSwitchVOC(
  IN OUT MrcParameters *const  MrcData,
  IN        GSM_GT             Group,
  IN        UINT8              NumberElements,
  IN  OUT   UINT8              PassFail[MAX_CONTROLLER][MAX_CHANNEL][MAX_SDRAM_IN_DIMM][MAX_BITS][3],
  IN        BOOLEAN            VocSweep
  )
{
  UINT8   Strobe;
  INT64   GetSetVal;
  UINT8   Bit;
  UINT32  Channel;
  UINT32  Controller;
  UINT32  SenseAmpTestDelay;

  SenseAmpTestDelay = 10;

  //  If in VOC sweep, copy the VOC value to all the bits.
  if (VocSweep) {
    for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
      for (Channel = 0; Channel < MAX_CHANNEL; Channel++) {
        if (!MrcChannelExist (MrcData, Controller, Channel)) {
          continue;
        }
        for (Strobe = 0; Strobe < NumberElements; Strobe++) {
          MrcGetSetBit (MrcData, Controller, Channel, 0, Strobe, 0, Group, ReadUncached, &GetSetVal);
          for (Bit = 1; Bit < MAX_BITS; Bit++) {
            MrcGetSetBit (MrcData, Controller, Channel, 0, Strobe, Bit, Group, ForceWriteCached, &GetSetVal);
          }
        }
        // Issue RxReset to make the voc code effective
        IssueRxResetMcCh (MrcData, Controller, Channel);
        SalVocPrepareSample (MrcData, Controller, Channel);
      }
    }
  }
  IssueRxReset (MrcData);
  //
  //  Trigger a VOC state Capture
  //
  //  Wait a bit
  MrcWait (MrcData, SenseAmpTestDelay);

  //  Process feedback
  for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
    for (Channel = 0; Channel < MAX_CHANNEL; Channel++) {
      if (!MrcChannelExist (MrcData, Controller, Channel)) {
        continue;
      }
      for (Strobe = 0; Strobe < NumberElements; Strobe++) {
        MrcGetSetChStrb (MrcData, Controller, Channel, Strobe, GsmIocDataTrainFeedback, ReadUncached, &GetSetVal);
        for (Bit = 0; Bit < MAX_BITS; Bit++) {
          if ((GetSetVal & (INT64)(1 << Bit)) != 0) {
            //
            // Feedback bit 0 mapping voc bit 0, bit 1 mapping voc for bit 1, one/one mapping, no swizzle.
            //
            PassFail[Controller][Channel][Strobe][Bit][ONEDS_CURRENT_SAMPLE] = ONEDS_RESULTS_FAIL;
          } else {
            PassFail[Controller][Channel][Strobe][Bit][ONEDS_CURRENT_SAMPLE] = ONEDS_RESULTS_PASS;
          }
        }
      }
    }
  }
  return mrcSuccess;
}


/**
  This function will handle the Rx VREF sweep training feedback.

  @param[in] MrcData         - Include all MRC global data.
  @param[in] Group           - Group number. It's not used in this function
  @param[in] NumberElements  - The strobe numbers
  @param[in OUT] PassFail    - Training feedback result,
  @param[in] VocSweep        - VOC Sweep or not. It's not used in this function

  @retval mrcSuccess    - function runs successfully
**/
MrcStatus
CompSampler (
  IN  MrcParameters* const  MrcData,
  IN        GSM_GT          Group,
  IN        UINT8           NumberElements,
  IN  OUT   UINT8           PassFail[MAX_CONTROLLER][MAX_CHANNEL][MAX_SDRAM_IN_DIMM][MAX_BITS][3],
  IN        BOOLEAN         VocSweep
  )
{
  UINT8  Strobe;
  INT64  GetSetVal;
  UINT8  Bit;
  UINT8  Channel;
  UINT8  Controller;
  UINT32 SenseAmpTestDelay = 10;

  MrcWait(MrcData, SenseAmpTestDelay);
  for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
    for (Channel = 0; Channel < MAX_CHANNEL; Channel++) {
      if (!MrcChannelExist(MrcData, Controller, Channel)) continue;
      for (Strobe = 0; Strobe < NumberElements; Strobe++) {
        MrcGetSetChStrb(MrcData, Controller, Channel, Strobe, GsmIocDataTrainFeedback, ReadUncached, &GetSetVal);
        GetSetVal = GetSetVal & 0xFF;
        for (Bit = 0; Bit < MAX_BITS; Bit++) {
          if (GetSetVal == 0xFF) {
            //
            // All the bits in the byte have the same feedback
            //
            PassFail[Controller][Channel][Strobe][Bit][ONEDS_CURRENT_SAMPLE] = ONEDS_RESULTS_PASS;
          } else {
            PassFail[Controller][Channel][Strobe][Bit][ONEDS_CURRENT_SAMPLE] = ONEDS_RESULTS_FAIL;
          }
        }
      }
    }
  }
  return mrcSuccess;
}


/**
  This function will search RcompOdtUp and RcompOdtDown to find the 1/0 transtion and
  program the result.

  @param[in] MrcData         - Include all MRC global data.
  @param[in] VddqRatio       - Vddq Ratio

  @retval None
**/
void
ProgramPdnPup (
  IN MrcParameters* const MrcData,
  IN UINT32 VddqRatio
  )
{
  MrcOutput*           Outputs;
  UINT8                Channel;
  UINT8                Strobes;
  UINT8                Controller;
  UINT8                Strobe;
  UINT16               TxRCompOdtUp, TxRCompOdtDn, VOdtCode;
  UINT16               GetSetVal;
  UINT16               Dim1StartPoint[MAX_CONTROLLER][MAX_CHANNEL][MAX_SDRAM_IN_DIMM][2];
  UINT16               Low[MAX_CONTROLLER][MAX_CHANNEL][MAX_SDRAM_IN_DIMM];
  UINT16               High[MAX_CONTROLLER][MAX_CHANNEL][MAX_SDRAM_IN_DIMM];
  UINT16               Results[MAX_CONTROLLER][MAX_CHANNEL][MAX_SDRAM_IN_DIMM][MAX_BITS][2];


  Outputs = &MrcData->Outputs;
  Strobes = Outputs->SdramCount;

  if (VddqRatio <= 50) {
    TxRCompOdtUp = (UINT16) ((32 * VddqRatio) / (100 - VddqRatio));
    TxRCompOdtDn = 32;
    //
    // 0 -> 31: txrcompodtdn=32, txrcompodtup from 0 to 31;
    // 32 -> 63: txrcompodtdn from 31 to 0, txrcompodtup=32
    //
    VOdtCode = TxRCompOdtUp;
  } else {
    TxRCompOdtUp = 32;
    TxRCompOdtDn = (UINT16) (((32 * 100) / VddqRatio) - 32);
    //
    // 0 -> 31: txrcompodtdn=32, txrcompodtup from 0 to 31;
    // 32 -> 63: txrcompodtdn from 31 to 0, txrcompodtup=32
    //
    VOdtCode = 63 - TxRCompOdtDn;
  }
  for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
    for (Channel = 0; Channel < MAX_CHANNEL; Channel++) {
      if (!MrcChannelExist(MrcData, Controller, Channel)) {
        continue;
      }
      for (Strobe = 0; Strobe < Strobes; Strobe++) {

        GetSetVal = VOdtCode;
        ProgramOdtUpDnStatic(MrcData, Controller, Channel, Strobe, GetSetVal);
        Dim1StartPoint[Controller][Channel][Strobe][0] = VOdtCode;
        Dim1StartPoint[Controller][Channel][Strobe][1] = 64;
        Low[Controller][Channel][Strobe] = 0;
        High[Controller][Channel][Strobe] = 63;
      }
    }
  }
  //
  // VODT search starts
  //
  MRC_DEBUG_MSG(&(MrcData->Outputs.Debug), MSG_LEVEL_NOTE, "VODT Search Starts\n");
  Create1DSweepLastPass (MrcData, 0, 0, CompRcompOdtUpPerStrobe, FALSE, Dim1StartPoint, Low, High, 1,
                         FALSE, FALSE, FALSE, Strobes, CompSampler, Results, (UINT8*)"S", 0);
  //
  // Search Result Program
  //
  MRC_DEBUG_MSG(&(MrcData->Outputs.Debug), MSG_LEVEL_NOTE, "VODT search result program\n");
  for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
    for (Channel = 0; Channel < MAX_CHANNEL; Channel++) {
      if (!MrcChannelExist(MrcData, Controller, Channel)) {
        continue;
      }
      for (Strobe = 0; Strobe < Strobes; Strobe++) {
        GetSetVal = Results[Controller][Channel][Strobe][0][0];
        ProgramOdtUpDnStatic(MrcData, Controller, Channel, Strobe, GetSetVal);
      }
    }
  }
}

/**
  This function will save the register value for DQTXCTL0 and RxVref

  @param[in] MrcData            - Include all MRC global data.
  @param[in OUT] CsrSave        - Storage to save value for DQTXCTL0
  @param[in OUT] RxVrefCsrSave  - Storage to save value for RxVref

  @retval None
**/
void
SaveCsrBeforeVocTraining (
  IN     MrcParameters* const MrcData,
  IN OUT UINT32               CsrSave[MAX_CONTROLLER][MAX_CHANNEL][MAX_SDRAM_IN_DIMM],
  IN OUT UINT32               RxVrefCsrSave[MAX_CONTROLLER][MAX_CHANNEL][MAX_SDRAM_IN_DIMM]
  )
{
  MrcOutput  *Outputs;
  UINT32      Channel;
  UINT32      Controller;
  UINT32      Offset;
  UINT32      Strobe;
  UINT32      TransChannel;
  UINT32      TransStrobe;
  UINT32      Rank;
  INT64       GetSetValue;

  Outputs = &MrcData->Outputs;
  Rank = 0;

  for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
    for (Channel = 0; Channel < MAX_CHANNEL; Channel++) {
      if (!MrcChannelExist(MrcData, Controller, Channel)) {
        continue;
      }
      for (Strobe = 0; Strobe < Outputs->SdramCount; Strobe++) {
        TransChannel = Channel;
        TransStrobe = Strobe;
        MrcTranslateSystemToIp (MrcData, &Controller, &TransChannel, &Rank, &TransStrobe, TxEqCoeff0);

        Offset = DATA0CH0_CR_DQTXCTL0_REG +
                (DATA0CH1_CR_DQTXCTL0_REG - DATA0CH0_CR_DQTXCTL0_REG) * TransChannel +
                (DATA1CH0_CR_DQTXCTL0_REG - DATA0CH0_CR_DQTXCTL0_REG) * TransStrobe;
        // Store the value for DQTXCTL0
        CsrSave[Controller][Channel][Strobe] = MrcReadCR (MrcData, Offset);

        MrcGetSetChStrb (MrcData, Controller, Channel, Strobe, RxVref, ReadUncached, &GetSetValue);
        // Store the value for RxVref
        RxVrefCsrSave[Controller][Channel][Strobe] = (UINT32) GetSetValue;
      }
    }
  }
}

/**
  This function implements Sense Amp Offset training.
    1. Enter DC VOC training mode and set target Vref for the DC VOC to operate
    2. Search for the ODT codes that match the pad with the target vref
    3. Search for the vref that gives the most centered VOC codes
    4. Search for the VOC code which will find the 1/0 transtion.
    5. Restore values back to ODT mode, RxVref, and Training Mode to pre-VOC

  @param[in,out] MrcData - Include all MRC global data.

  @retval MrcStatus - if it succeeded, return mrcSuccess
**/
MrcStatus
MrcSenseAmpOffsetTraining (
  IN OUT MrcParameters *const MrcData
  )
{
  const MrcInput        *Inputs;
  MrcOutput             *Outputs;
  MrcDebug              *Debug;
  const MRC_FUNCTION    *MrcCall;
  UINT8                 Bit;
  UINT8                 Strobe;
  INT64                 GetSetVal;
  INT64                 RxVrefMax;
  UINT16                Dim1StartPoint[MAX_CONTROLLER][MAX_CHANNEL][MAX_SDRAM_IN_DIMM][2];
  UINT16                Low[MAX_CONTROLLER][MAX_CHANNEL][MAX_SDRAM_IN_DIMM];
  UINT16                High[MAX_CONTROLLER][MAX_CHANNEL][MAX_SDRAM_IN_DIMM];
  UINT16                Results[VOC_KNOB_COUNT][MAX_CONTROLLER][MAX_CHANNEL][MAX_SDRAM_IN_DIMM][MAX_BITS][2];
  UINT16                MinVref[VOC_KNOB_COUNT][MAX_SDRAM_IN_DIMM], MaxVref[VOC_KNOB_COUNT][MAX_SDRAM_IN_DIMM];
  INT64                 MidVref[VOC_KNOB_COUNT][MAX_SDRAM_IN_DIMM];
  UINT32                CsrSave[MAX_CONTROLLER][MAX_CHANNEL][MAX_SDRAM_IN_DIMM];
  UINT32                RxVrefCsrSave[MAX_CONTROLLER][MAX_CHANNEL][MAX_SDRAM_IN_DIMM];
  UINT8                 LoopIteration;
#ifdef DCVOC_DUMB_SWEEP
  UINT8                 i;
  UINT8                 BitState;
#endif // DCVOC_DUMB_SWEEP
  UINT8                 LowLimit;
  UINT8                 HighLimit;
  BOOLEAN               Lpddr;
  UINT16                VrefMin;
  UINT8                 Controller;
  UINT8                 Channel;
  UINT8                 Strobes;
  UINT8                 Rank;
  UINT8                 VocKnobs;
  UINT16                VocKnob[2];
  UINT8                 VocKnobSelect;
  char*                 VocString[2];
  BOOLEAN               MatchedRx;
  UINT8                 CommonmodeTargetPct;
  UINT32                VDDqRatio;
  UINT8                 MaxIteration;
  MRC_RX_MODE_TYPE      RxMode;

  Inputs  = &MrcData->Inputs;
  MrcCall = Inputs->Call.Func;
  Outputs = &MrcData->Outputs;
  Debug   = &Outputs->Debug;
  RxMode  = Outputs->RxMode;
  Strobes = Outputs->SdramCount;
  Lpddr   = Outputs->Lpddr;

  VrefMin   = Lpddr ? 15 : 300;
  LowLimit  = 0;
  HighLimit = 0;
  Rank      = 0;
  CommonmodeTargetPct = 75;

  MatchedRx     = ((RxMode == MrcRxModeMatchedP) || (RxMode == MrcRxModeMatchedN));
  VocKnobs      = MatchedRx ? 1 : 2;
  VocKnob[0]    = RxMatchedUnmatchedOffPcal;              // RxSALVocP;
  VocKnob[1]    = MatchedRx ? 0xFF : RxUnmatchedOffNcal;  // RxSALVocN;
  VocString[0]  = MatchedRx ? "Voc" : "VocSalP";
  VocString[1]  = MatchedRx ? "Bad" : "VocSalN";

#ifdef DCVOC_DUMB_SWEEP
  MaxIteration = 2;   // 2nd iteration is for result confirmation, no functional impact. The dump sweep is disabled by default.
#else
  MaxIteration = 1;
#endif

  MrcCall->MrcSetMem((UINT8 *) CsrSave, sizeof (CsrSave), 0);
  MrcCall->MrcSetMem((UINT8 *) RxVrefCsrSave, sizeof (RxVrefCsrSave), 0);

  MrcGetSetLimits (MrcData, RxVref, NULL, &RxVrefMax, NULL);

  // Save the CSR value before VOC training.
  SaveCsrBeforeVocTraining (MrcData, CsrSave, RxVrefCsrSave);

  for (VocKnobSelect = 0; VocKnobSelect < VocKnobs; VocKnobSelect++) {
    if (VocKnobSelect == 0) {
      CommonmodeTargetPct = Lpddr ? 30 : 75;

      if (Lpddr) {
        VDDqRatio = 50;  // @todo_adl Is this correct for Vss termination ? Should use Vddq / 5 = 20% ?
      } else {
        VDDqRatio = (100 * CommonmodeTargetPct) / 100;
      }
      // Enter the VOC training mode.
      EnterVocTraining (MrcData, VocKnob[VocKnobSelect], TRUE, CsrSave, RxVrefCsrSave);

      // Program initial RxVref according to Rx termination
      GetSetVal = (VDDqRatio * (UINT32) RxVrefMax) / 100;
      MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Voc Rx Vref = %d\n", (UINT32) GetSetVal);
      MrcGetSetChStrb (MrcData, MAX_CONTROLLER, MAX_CHANNEL, MAX_SDRAM_IN_DIMM, RxVref, ForceWriteCached, &GetSetVal);

      // Start the RcompOdtUp and RcompOdtDown search and program
      ProgramPdnPup (MrcData, VDDqRatio);
    }
    // Enter the VOC training mode.
    EnterVocTraining (MrcData, VocKnob[VocKnobSelect], TRUE, CsrSave, RxVrefCsrSave);

    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\nSetting %s to %d\n", VocString[VocKnobSelect], VOC_MID);
    // Park VOC at midpoint
    GetSetVal = VOC_MID;
    MrcGetSetBit (MrcData, MAX_CONTROLLER, MAX_CHANNEL, Rank, MAX_SDRAM_IN_DIMM, MAX_BITS, VocKnob[VocKnobSelect], ForceWriteCached, &GetSetVal);
    IssueRxReset (MrcData);

    // Sweep the Rx Vref code and monitor the o/p of the DQ Rx for all 8-bits.  This will provide a range of Vref which will
    // switch the DQ o/p from 1 -> 0.  If all DQ O/P switch at the same VREF, then VOC sweep can be skipped. Need to make
    // sure the high side is skipped as one direction sweep is enough.
    //
    // voc search
    //
    for (LoopIteration = 0; LoopIteration < MaxIteration; LoopIteration++) {
      MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "LoopIteration %d : \n", LoopIteration);
      //
      // First LoopIteration loop is to do voc search
      // 2nd LoopIteration loop is to do double check.
      // The MaxIteration is 1 by default.
      //
      if (LoopIteration == 0) {
        MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Sweeping Vref for 1->0 transition on VOCSampler holding %s=0\n", VocString[VocKnobSelect]);
      }
      MRC_DEBUG_MSG(Debug, MSG_LEVEL_NOTE, "VREF ");
      for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
        for (Channel = 0; Channel < Outputs->MaxChannels; Channel++) {
          if (!(MrcRankExist (MrcData, Controller, Channel, Rank))) {
            continue;
          }
          for (Strobe = 0; Strobe < Strobes; Strobe++) {
            MrcGetSetStrobe (MrcData, Controller, Channel, 0, Strobe, RxVref, ReadUncached, &GetSetVal);

            Dim1StartPoint[Controller][Channel][Strobe][ONEDS_LOW]  = (UINT16) (GetSetVal - 40);
            Dim1StartPoint[Controller][Channel][Strobe][ONEDS_HIGH] = VOC_VREF_MAX + 1;

            Low[Controller][Channel][Strobe]  = 0;
            High[Controller][Channel][Strobe] = VrefMin + 120;
          } // Strobe
        } // Channel
      } // Controller

      //
      // Rx VREF search centers rx vref to the middle of the high and low point within that byte lane
      //
      MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Rx Vref Search Start \n");
      Create1DSweepLastPass (MrcData, 0, Rank, RxVref, FALSE, Dim1StartPoint, Low, High, VOC_VREF_STEP_SIZE,
                             FALSE, FALSE, TRUE, Strobes, VrefValueToSwitchVOC, Results[VocKnobSelect], (UINT8*) "S", 0);

      MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Rx Vref Search results: \n");
      //
      // Print the results, but also determine the common vref to be programmed for all bits.
      //
      for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
        for (Channel = 0; Channel < Outputs->MaxChannels; Channel++) {
          if (!(MrcRankExist(MrcData, Controller, Channel, Rank))) {
            continue;
          }
          MRC_DEBUG_MSG(Debug, MSG_LEVEL_NOTE, "MC %d Ch%d ", Controller, Channel);
          for (Strobe = 0; Strobe < Strobes; Strobe++) {
            MRC_DEBUG_MSG(Debug, MSG_LEVEL_NOTE, "  SL%d", Strobe);
            MinVref[VocKnobSelect][Strobe] = 0xFFFF;
            MaxVref[VocKnobSelect][Strobe] = 0x0;
            for (Bit = 0; Bit < MAX_BITS; Bit++) {
              if (Results[VocKnobSelect][Controller][Channel][Strobe][Bit][ONEDS_LOW] < MinVref[VocKnobSelect][Strobe]) {
                MinVref[VocKnobSelect][Strobe] = Results[VocKnobSelect][Controller][Channel][Strobe][Bit][ONEDS_LOW];
              }
              if (Results[VocKnobSelect][Controller][Channel][Strobe][Bit][ONEDS_LOW] > MaxVref[VocKnobSelect][Strobe]) {
                MaxVref[VocKnobSelect][Strobe] = Results[VocKnobSelect][Controller][Channel][Strobe][Bit][ONEDS_LOW];
              }
              MRC_DEBUG_MSG(Debug, MSG_LEVEL_NOTE, ":%02d", Results[VocKnobSelect][Controller][Channel][Strobe][Bit][ONEDS_LOW]);
            }
            //
            // Set the minimum  vref any bit has transitioned.. this is where VOC begins its sweeps.
            //
            MidVref[VocKnobSelect][Strobe] = (MinVref[VocKnobSelect][Strobe] + MaxVref[VocKnobSelect][Strobe]) / 2;

            MRC_DEBUG_MSG(Debug, MSG_LEVEL_NOTE, "[%02d]", MidVref[VocKnobSelect][Strobe]);
            MrcGetSetChStrb (MrcData, Controller, Channel, Strobe, RxVref, ForceWriteCached, &MidVref[VocKnobSelect][Strobe]);

            if ((Strobe % 4 == 3) && (Strobe < (Strobes - 1))) {
              MRC_DEBUG_MSG(Debug, MSG_LEVEL_NOTE, "\n     ");
            } else {
              MRC_DEBUG_MSG(Debug, MSG_LEVEL_NOTE, "  ");
            }
          }
          MRC_DEBUG_MSG(Debug, MSG_LEVEL_NOTE, "\n");
        } //channel
      }//controller

      MRC_DEBUG_MSG(Debug, MSG_LEVEL_NOTE, "\n");

      HighLimit = 0;
      LowLimit = 0;
      if (LoopIteration == 0) {

#ifdef DCVOC_DUMB_SWEEP
        MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Dumb Sweep Start \n");
#endif // DCVOC_DUMB_SWEEP
        for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
          for (Channel = 0; Channel < Outputs->MaxChannels; Channel++) {
            if (!(MrcRankExist(MrcData, Controller, Channel, Rank))) {
              continue;
            }
#ifdef DCVOC_DUMB_SWEEP
            //
            // Dumb sweep
            //
            for (i = 0; i < VOC_MAX; i+=1) {
              MRC_DEBUG_MSG(Debug, MSG_LEVEL_NOTE, "%02d:", i);
              for (Strobe = 0; Strobe < Strobes; Strobe++) {
                //Set Voc Value
                GetSetVal = i;
                for (Bit = 0; Bit < MAX_BITS; Bit++) {
                  MrcGetSetBit(MrcData, Controller, Channel, Rank, Strobe, Bit, VocKnob[VocKnobSelect], ForceWriteCached, &GetSetVal);
                }
              }

              IssueRxReset (MrcData);

              SalVocPrepareSample(MrcData, Controller, Channel);
              for (Strobe = 0; Strobe < Strobes; Strobe++) {

                MrcGetSetChStrb(MrcData, Controller, Channel, Strobe, GsmIocDataTrainFeedback, ReadUncached, &GetSetVal);
                for (Bit = 0; Bit < MAX_BITS; Bit++) {
                  BitState = ((GetSetVal & (INT64)(1 << Bit)) == 0) ? 0 : 1;
                  MRC_DEBUG_MSG(Debug, MSG_LEVEL_NOTE, "%01d:", BitState);
                  if (BitState == 0 && i == 0)  LowLimit = 1;
                  if (BitState == 1 && i == VOC_MAX)  HighLimit = 1;
                }
                MRC_DEBUG_MSG(Debug, MSG_LEVEL_NOTE, "  ");
              }
              MRC_DEBUG_MSG(Debug, MSG_LEVEL_NOTE, "\n");
            }
#endif //if DCVOC_DUMB_SWEEP

            IssueRxResetMcCh (MrcData, Controller, Channel);

            if ((LowLimit == 1) && (HighLimit == 1))
              MRC_DEBUG_MSG(Debug, MSG_LEVEL_NOTE, "\nERROR: %s Failure, Bit Voltage offset differences are more than %s range\n",
                            VocString[VocKnobSelect], VocString[VocKnobSelect]);
            //
            //  Sweep with Brains
            //
            MRC_DEBUG_MSG(Debug, MSG_LEVEL_NOTE, "\nSweeping Vref for 1->0 transition on VOCSampler with %s Normalized.\n",
                          VocString[VocKnobSelect]);
            for (Strobe = 0; Strobe < Strobes; Strobe++) {
              Dim1StartPoint[Controller][Channel][Strobe][ONEDS_LOW] = 0;
              Dim1StartPoint[Controller][Channel][Strobe][ONEDS_HIGH] = VOC_MAX + 1;
              Low[Controller][Channel][Strobe] = 0;
              High[Controller][Channel][Strobe] = VOC_MAX;
            } // Strobe loop ...
          } //channel
        }//controller
        //
        // VOC search sweep from 0 to 31, find the 1 to 0 or o to 1 transtion and program the trainsition voc code to the register.
        //
        MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "VOC  %s Search Start \n", VocString[VocKnobSelect]);
        Create1DSweepLastPass (MrcData, 0, Rank, VocKnob[VocKnobSelect], TRUE, Dim1StartPoint, Low, High,
                               1,//step size.
                               FALSE, FALSE, TRUE, Strobes, VrefValueToSwitchVOC, Results[VocKnobSelect], (UINT8*) "S", 0);

        MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Program VOC Search Result\n");
        //
        // Based on the results rom the test, program the VOC final falues.
        //
        MRC_DEBUG_MSG(Debug, MSG_LEVEL_NOTE, "%s  \n", VocString[VocKnobSelect]);
        for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
          for (Channel = 0; Channel < Outputs->MaxChannels; Channel++) {
            if (!(MrcRankExist(MrcData, Controller, Channel, Rank))) {
              continue;
            }
            MRC_DEBUG_MSG(Debug, MSG_LEVEL_NOTE, "MC%d, CH%d", Controller,Channel );
            //
            // program the final result
            //
            for (Strobe = 0; Strobe < Strobes; Strobe++) {
              MRC_DEBUG_MSG(Debug, MSG_LEVEL_NOTE, "  SL%d", Strobe);
              for (Bit = 0; Bit < MAX_BITS; Bit++) {
                GetSetVal = Results[VocKnobSelect][Controller][Channel][Strobe][Bit][ONEDS_LOW];

                for (Rank = 0; Rank < MAX_RANK_IN_CHANNEL; Rank++) {
                  if (!MrcRankExist (MrcData, Controller, Channel, Rank)) {
                    continue;
                  }
                  MrcGetSetBit(MrcData, Controller, Channel, Rank, Strobe, Bit, VocKnob[VocKnobSelect], ForceWriteCached, &GetSetVal);
                }
                MRC_DEBUG_MSG(Debug, MSG_LEVEL_NOTE, ":%02d", GetSetVal);
              }
              if ((Strobe % 4 == 3) && (Strobe < (Strobes - 1))) {
                MRC_DEBUG_MSG(Debug, MSG_LEVEL_NOTE, "\n     ");
              } else {
                MRC_DEBUG_MSG(Debug, MSG_LEVEL_NOTE, "      ");
              }
            }
            Rank = 0;
            MRC_DEBUG_MSG(Debug, MSG_LEVEL_NOTE, "\n");

            IssueRxResetMcCh (MrcData, Controller, Channel);
          }
        }
      }
    } // LoopIteration
  } // VocKnobSelect

  // Exit the DC VOC training.
  EnterVocTraining (MrcData, VocKnob[0], FALSE, CsrSave, RxVrefCsrSave);
  MrcWait(MrcData, 10); // ns
  IssueRxReset (MrcData);   // The RxReset is only for analog domain
  IoReset (MrcData);        // The IoReset will also cover the logical domain.
  MrcWait (MrcData, 100);

  return mrcSuccess;
}

/**
  This function is used to prepare for sampling the training feedback after programing the VOC code.

  @param[in] MrcData    - Include all MRC global data.
  @param[in] Controller - Controller number.
  @param[in] Channel    - Channel number.

  @retval None
**/
VOID
SalVocPrepareSample (
  IN OUT MrcParameters *const  MrcData,
  IN     UINT32                Controller,
  IN     UINT32                Channel
  )
{
  MrcOutput   *Outputs;
  UINT32      Offset;
  UINT32      Strobe;
  UINT32      TransChannel;
  UINT32      TransStrobe;
  UINT32      Rank = 0;
  DATA0CH0_CR_AFEMISC_STRUCT      DataAfeMisc;
  DATA0CH0_CR_AFEMISCCTRL1_STRUCT DataAfeMiscCtrl1;

  Outputs  = &MrcData->Outputs;

  for (Strobe = 0; Strobe < Outputs->SdramCount; Strobe++) {
    TransChannel = Channel;
    TransStrobe = Strobe;
    MrcTranslateSystemToIp (MrcData, &Controller, &TransChannel, &Rank, &TransStrobe, GsmIocTxOn);

    Offset = DATA0CH0_CR_AFEMISCCTRL1_REG +
            (DATA0CH1_CR_AFEMISCCTRL1_REG - DATA0CH0_CR_AFEMISCCTRL1_REG) * TransChannel +
            (DATA1CH0_CR_AFEMISCCTRL1_REG - DATA0CH0_CR_AFEMISCCTRL1_REG) * TransStrobe;
    DataAfeMiscCtrl1.Data = MrcReadCR (MrcData, Offset);
    DataAfeMiscCtrl1.Bits.rxsdl_cmn_srzrcvenpimodovrden   = 1;
    DataAfeMiscCtrl1.Bits.rxsdl_cmn_srzrcvenpimodovrdval  = 0;
    MrcWriteCR (MrcData, Offset, DataAfeMiscCtrl1.Data);
  }

  MrcWait (MrcData, 3); //3 ns

  for (Strobe = 0; Strobe < Outputs->SdramCount; Strobe++) {
    TransChannel = Channel;
    TransStrobe = Strobe;
    MrcTranslateSystemToIp (MrcData, &Controller, &TransChannel, &Rank, &TransStrobe, GsmIocTxOn);

    Offset = DATA0CH0_CR_AFEMISC_REG +
            (DATA0CH1_CR_AFEMISC_REG - DATA0CH0_CR_AFEMISC_REG) * TransChannel +
            (DATA1CH0_CR_AFEMISC_REG - DATA0CH0_CR_AFEMISC_REG) * TransStrobe;
    DataAfeMisc.Data = MrcReadCR (MrcData, Offset);
    DataAfeMisc.Bits.afeshared_cmn_parkval = 1;
    MrcWriteCR (MrcData, Offset, DataAfeMisc.Data);
  }

  MrcWait (MrcData, 3); // 3 ns

  for (Strobe = 0; Strobe < Outputs->SdramCount; Strobe++) {
    TransChannel = Channel;
    TransStrobe = Strobe;
    MrcTranslateSystemToIp (MrcData, &Controller, &TransChannel, &Rank, &TransStrobe, GsmIocTxOn);

    Offset = DATA0CH0_CR_AFEMISC_REG +
            (DATA0CH1_CR_AFEMISC_REG - DATA0CH0_CR_AFEMISC_REG) * TransChannel +
            (DATA1CH0_CR_AFEMISC_REG - DATA0CH0_CR_AFEMISC_REG) * TransStrobe;
    DataAfeMisc.Data = MrcReadCR (MrcData, Offset);
    DataAfeMisc.Bits.afeshared_cmn_parkval = 0;
    MrcWriteCR (MrcData, Offset, DataAfeMisc.Data);
  }

  MrcWait (MrcData, 3); //3 ns

  for (Strobe = 0; Strobe < Outputs->SdramCount; Strobe++) {
    TransChannel = Channel;
    TransStrobe = Strobe;
    MrcTranslateSystemToIp (MrcData, &Controller, &TransChannel, &Rank, &TransStrobe, GsmIocTxOn);

    Offset = DATA0CH0_CR_AFEMISC_REG +
            (DATA0CH1_CR_AFEMISC_REG - DATA0CH0_CR_AFEMISC_REG) * TransChannel +
            (DATA1CH0_CR_AFEMISC_REG - DATA0CH0_CR_AFEMISC_REG) * TransStrobe;
    DataAfeMisc.Data = MrcReadCR (MrcData, Offset);
    DataAfeMisc.Bits.afeshared_cmn_parkval = 1;
    MrcWriteCR (MrcData, Offset, DataAfeMisc.Data);
  }

  MrcWait (MrcData, 3); //3 ns

  for (Strobe = 0; Strobe < Outputs->SdramCount; Strobe++) {
    TransChannel = Channel;
    TransStrobe = Strobe;
    MrcTranslateSystemToIp (MrcData, &Controller, &TransChannel, &Rank, &TransStrobe, GsmIocTxOn);

    Offset = DATA0CH0_CR_AFEMISC_REG +
            (DATA0CH1_CR_AFEMISC_REG - DATA0CH0_CR_AFEMISC_REG) * TransChannel +
            (DATA1CH0_CR_AFEMISC_REG - DATA0CH0_CR_AFEMISC_REG) * TransStrobe;
    DataAfeMisc.Data = MrcReadCR (MrcData, Offset);
    DataAfeMisc.Bits.afeshared_cmn_parkval = 0;
    MrcWriteCR (MrcData, Offset, DataAfeMisc.Data);
  }

  MrcWait (MrcData, 3); // 3 ns

  for (Strobe = 0; Strobe < Outputs->SdramCount; Strobe++) {
    TransChannel = Channel;
    TransStrobe = Strobe;
    MrcTranslateSystemToIp (MrcData, &Controller, &TransChannel, &Rank, &TransStrobe, GsmIocTxOn);


    Offset = DATA0CH0_CR_AFEMISCCTRL1_REG +
            (DATA0CH1_CR_AFEMISCCTRL1_REG - DATA0CH0_CR_AFEMISCCTRL1_REG) * TransChannel +
            (DATA1CH0_CR_AFEMISCCTRL1_REG - DATA0CH0_CR_AFEMISCCTRL1_REG) * TransStrobe;
    DataAfeMiscCtrl1.Data = MrcReadCR (MrcData, Offset);
    DataAfeMiscCtrl1.Bits.rxsdl_cmn_srzrcvenpimodovrden  = 1;
    DataAfeMiscCtrl1.Bits.rxsdl_cmn_srzrcvenpimodovrdval = 1;
    MrcWriteCR (MrcData, Offset, DataAfeMiscCtrl1.Data);
  }

  MrcWait (MrcData, 3); // 3 ns
}

/**
  This function is used to enter or exit the voc training for each controller and each channel

  @param[in] MrcData        - Include all MRC global data.
  @param[in] Controller     - Controller number.
  @param[in] Channel        - Channel number.
  @param[in] SALP           - It's VocP or VocN.
  @param[in] VocEnable      - VOC training enable or disable.
  @param[in] CsrSave        - Storage that stores the value for DQTXCTL0
  @param[in] RxVrefCsrSave  - Storage that stores the value for RxVref

  @retval None
**/

VOID
VocEntry (
  IN OUT MrcParameters *const  MrcData,
  IN     UINT32                Controller,
  IN     UINT32                Channel,
  IN     BOOLEAN               SALP,
  IN     BOOLEAN               VocEnable,
  IN     UINT32                CsrSave[MAX_CONTROLLER][MAX_CHANNEL][MAX_SDRAM_IN_DIMM],
  IN     UINT32                RxVrefCsrSave[MAX_CONTROLLER][MAX_CHANNEL][MAX_SDRAM_IN_DIMM]
  )
{

  MrcOutput                            *Outputs;
  UINT32                               Offset;
  UINT32                               Strobe;
  UINT32                               TransChannel;
  UINT32                               TransStrobe;
  UINT32                               Rank;
  DATA0CH0_CR_DQRXCTL0_STRUCT          DataDqRxCtl0;
  DATA0CH0_CR_DATATRAINFEEDBACK_STRUCT DataTrainFeedback;
  DATA0CH0_CR_DQTXCTL0_STRUCT          DataDqTxCtl0;
  DATA0CH0_CR_DQTXCTL0_STRUCT          DataDqTxCtl0Save;
  DATA0CH0_CR_AFEMISCCTRL1_STRUCT      DataAfeMiscCtrl1;
  INT64                                GetSetValue;

  Outputs = &MrcData->Outputs;
  Rank = 0;

  for (Strobe = 0; Strobe < Outputs->SdramCount; Strobe++) {
    TransChannel = Channel;
    TransStrobe = Strobe;
    MrcTranslateSystemToIp(MrcData, &Controller, &TransChannel, &Rank, &TransStrobe, GsmIocTxOn);

    Offset = DATA0CH0_CR_DQRXCTL0_REG +
             (DATA0CH1_CR_DQRXCTL0_REG - DATA0CH0_CR_DQRXCTL0_REG) * TransChannel +
             (DATA1CH0_CR_DQRXCTL0_REG - DATA0CH0_CR_DQRXCTL0_REG) * TransStrobe;
    DataDqRxCtl0.Data = MrcReadCR (MrcData, Offset);
    DataDqRxCtl0.Bits.dqrx_cmn_rxdiffampen = VocEnable;
    MrcWriteCR (MrcData, Offset, DataDqRxCtl0.Data);
  }

  for (Strobe = 0; Strobe < Outputs->SdramCount; Strobe++) {
    TransChannel = Channel;
    TransStrobe = Strobe;
    MrcTranslateSystemToIp(MrcData, &Controller, &TransChannel, &Rank, &TransStrobe, GsmIocDataTrainFeedback);

    Offset = DATA0CH0_CR_DATATRAINFEEDBACK_REG +
            (DATA0CH1_CR_DATATRAINFEEDBACK_REG - DATA0CH0_CR_DATATRAINFEEDBACK_REG) * TransChannel +
            (DATA1CH0_CR_DATATRAINFEEDBACK_REG - DATA0CH0_CR_DATATRAINFEEDBACK_REG) * TransStrobe;
    DataTrainFeedback.Data = MrcReadCR (MrcData, Offset);
    if (((SALP == TRUE) && (VocEnable == TRUE)) || (VocEnable == FALSE)) {
      DataTrainFeedback.Bits.rxampoffseten = 0;
    } else {
      DataTrainFeedback.Bits.rxampoffseten = 1;
    }
    MrcWriteCR (MrcData, Offset, DataTrainFeedback.Data);
  }

  MrcWait(MrcData, 100); // ns

  GetSetValue = VocEnable;
  MrcGetSetChStrb (MrcData, Controller, Channel, MAX_SDRAM_IN_DIMM, GsmIocSenseAmpMode,   WriteNoCache, &GetSetValue);  // senseamptrainingmode
  MrcGetSetChStrb (MrcData, Controller, Channel, MAX_SDRAM_IN_DIMM, GsmIocRankOverrideEn, WriteNoCache, &GetSetValue);  // rankovrd
  GetSetValue = 0;
  MrcGetSetChStrb (MrcData, Controller, Channel, MAX_SDRAM_IN_DIMM, GsmIocRankOverrideVal, WriteNoCache, &GetSetValue); // rankvalue

  for (Strobe = 0; Strobe < Outputs->SdramCount; Strobe++) {
    TransChannel = Channel;
    TransStrobe = Strobe;
    MrcTranslateSystemToIp (MrcData, &Controller, &TransChannel, &Rank, &TransStrobe, TxEqCoeff0);

    Offset = DATA0CH0_CR_DQTXCTL0_REG +
            (DATA0CH1_CR_DQTXCTL0_REG - DATA0CH0_CR_DQTXCTL0_REG) * TransChannel +
            (DATA1CH0_CR_DQTXCTL0_REG - DATA0CH0_CR_DQTXCTL0_REG) * TransStrobe;
    DataDqTxCtl0.Data = MrcReadCR (MrcData, Offset);
    if (VocEnable == TRUE) {
      DataDqTxCtl0.Bits.dxtx_cmn_txvttodten   = 0;
      DataDqTxCtl0.Bits.dxtx_cmn_txvssodten   = 1;
      DataDqTxCtl0.Bits.dxtx_cmn_txvddqodten  = 1;
      DataDqTxCtl0.Bits.dxtx_cmn_forceodton   = 1;
    } else {
      DataDqTxCtl0Save.Data = CsrSave[Controller][Channel][Strobe];
      DataDqTxCtl0.Bits.dxtx_cmn_txvttodten   = DataDqTxCtl0Save.Bits.dxtx_cmn_txvttodten;
      DataDqTxCtl0.Bits.dxtx_cmn_txvssodten   = DataDqTxCtl0Save.Bits.dxtx_cmn_txvssodten;
      DataDqTxCtl0.Bits.dxtx_cmn_txvddqodten  = DataDqTxCtl0Save.Bits.dxtx_cmn_txvddqodten;
      DataDqTxCtl0.Bits.dxtx_cmn_forceodton   = DataDqTxCtl0Save.Bits.dxtx_cmn_forceodton;
      DataDqTxCtl0.Bits.dqtx_cmn_txodtstatlegendq = 1;
    }
    MrcWriteCR (MrcData, Offset, DataDqTxCtl0.Data);
  }

  if (VocEnable == TRUE) {
    // Rx Reset
    IssueRxResetMcCh (MrcData, Controller, Channel);
    MrcWait (MrcData, 100); // ns
  } else {
    for (Strobe = 0; Strobe < Outputs->SdramCount; Strobe++) {
      // Restore RxVref per byte
      GetSetValue = (INT64) (RxVrefCsrSave[Controller][Channel][Strobe]);
      MrcGetSetChStrb (MrcData, Controller, Channel, Strobe, RxVref, ForceWriteCached, &GetSetValue);

      TransChannel = Channel;
      TransStrobe = Strobe;
      MrcTranslateSystemToIp (MrcData, &Controller, &TransChannel, &Rank, &TransStrobe, GsmIocTxOn);

      Offset = DATA0CH0_CR_AFEMISCCTRL1_REG +
              (DATA0CH1_CR_AFEMISCCTRL1_REG - DATA0CH0_CR_AFEMISCCTRL1_REG) * TransChannel +
              (DATA1CH0_CR_AFEMISCCTRL1_REG - DATA0CH0_CR_AFEMISCCTRL1_REG) * TransStrobe;
      DataAfeMiscCtrl1.Data = MrcReadCR (MrcData, Offset);
      DataAfeMiscCtrl1.Bits.rxsdl_cmn_srzrcvenpimodovrden  = 0;
      DataAfeMiscCtrl1.Bits.rxsdl_cmn_srzrcvenpimodovrdval = 0;
      MrcWriteCR (MrcData, Offset, DataAfeMiscCtrl1.Data);
    }
  }
  return;
}

/**
  This function is used to enter or exit the voc training for RxUnmatchedOffNcal or RxMatchedUnmatchedOffPcal
  for all the populated controllers and channels.

  @param[in] MrcData        - Include all MRC global data.
  @param[in] VocKnob        - It can be RxUnmatchedOffNcal or RxMatchedUnmatchedOffPcal.
  @param[in] VocEnable      - VOC training enable or disable.
  @param[in] CsrSave        - Storage that stores the value for DQTXCTL0
  @param[in] RxVrefCsrSave  - Storage that stores the value for RxVref

  @retval None
**/
VOID
EnterVocTraining (
  IN MrcParameters *const  MrcData,
  IN GSM_GT                VocKnob,
  IN BOOLEAN               VocEnable,
  IN UINT32                CsrSave[MAX_CONTROLLER][MAX_CHANNEL][MAX_SDRAM_IN_DIMM],
  IN UINT32                RxVrefCsrSave[MAX_CONTROLLER][MAX_CHANNEL][MAX_SDRAM_IN_DIMM]
  )
{
  UINT32 Controller = 0;
  UINT32 Channel = 0;
  BOOLEAN SalP = FALSE;

  MRC_DEBUG_MSG(&(MrcData->Outputs.Debug), MSG_LEVEL_NOTE, "Voc Entry\n");
  if (VocEnable == TRUE) {
    if (VocKnob == RxUnmatchedOffNcal) {
      //
      // For "N" VOC, enter the voc training.
      //
      SalP = FALSE;
    }   else if (VocKnob == RxMatchedUnmatchedOffPcal) {
      //
      // For "P" VOC, enter the voc training.
      //
      SalP = TRUE;
    } else {
      MRC_DEBUG_ASSERT (FALSE, &(MrcData->Outputs.Debug), " VocKnob %d is invalid", VocKnob);
      return;
    }
  }
  for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
    for (Channel = 0; Channel < MAX_CHANNEL; Channel++) {
      if (!MrcChannelExist(MrcData, Controller, Channel)) {
        continue;
      }
      VocEntry (MrcData, Controller, Channel, SalP, VocEnable, CsrSave, RxVrefCsrSave);
    }
  }
  if (VocEnable == FALSE) {
    //
    // Restore the Comp codes
    //
    MrcForceDistCompCodes (MrcData);
  }

  return;
}

/**
  This function toggles the dxrx_cmn_rxtrainreset from 1 to 0 to
  make the voc code take effect after programing for the specified controller
  and channel.

  @param[in] MrcData    - Include all MRC global data.
  @param[in] Controller - Controller number.
  @param[in] Channel    - Channel number.

  @retval MrcStatus - return mrcSuccess
**/
MrcStatus
IssueRxResetMcCh (
  IN MrcParameters *const  MrcData,
  IN UINT32                Controller,
  IN UINT32                Channel
  )
{

  DATA0CH0_CR_DQRXCTL1_STRUCT  DqRxCtl1;
  MrcOutput                    *Outputs;
  UINT32                       Offset;
  UINT32                       Strobe;
  UINT32                       TransChannel;
  UINT32                       TransStrobe;
  UINT32                       Rank;

  Outputs = &MrcData->Outputs;
  Rank = 0;

  for (Strobe = 0; Strobe < Outputs->SdramCount; Strobe++) {
    TransChannel = Channel;
    TransStrobe  = Strobe;
    MrcTranslateSystemToIp (MrcData, &Controller, &TransChannel, &Rank, &TransStrobe, GsmIocTxOn);

    Offset = DATA0CH0_CR_DQRXCTL1_REG +
            (DATA0CH1_CR_DQRXCTL1_REG - DATA0CH0_CR_DQRXCTL1_REG) * TransChannel +
            (DATA1CH0_CR_DQRXCTL1_REG - DATA0CH0_CR_DQRXCTL1_REG) * TransStrobe;
    DqRxCtl1.Data = MrcReadCR (MrcData, Offset);
    DqRxCtl1.Bits.dxrx_cmn_rxtrainreset = 1;
    MrcWriteCR (MrcData, Offset, DqRxCtl1.Data);
  }

  MrcWait (MrcData, 2); // Hold it for 2ns

  for (Strobe = 0; Strobe < Outputs->SdramCount; Strobe++) {
    TransChannel = Channel;
    TransStrobe = Strobe;
    MrcTranslateSystemToIp (MrcData, &Controller, &TransChannel, &Rank, &TransStrobe, GsmIocTxOn);

    Offset = DATA0CH0_CR_DQRXCTL1_REG +
            (DATA0CH1_CR_DQRXCTL1_REG - DATA0CH0_CR_DQRXCTL1_REG) * TransChannel +
            (DATA1CH0_CR_DQRXCTL1_REG - DATA0CH0_CR_DQRXCTL1_REG) * TransStrobe;
    DqRxCtl1.Data = MrcReadCR(MrcData, Offset);
    DqRxCtl1.Bits.dxrx_cmn_rxtrainreset = 0;
    MrcWriteCR(MrcData, Offset, DqRxCtl1.Data);
  }

  MrcWait (MrcData, 100);
  return mrcSuccess;
}

/**
  This function toggles the dxrx_cmn_rxtrainreset from 1 to 0 to
  make the voc code take effect after programing for all the controllers and channels.

  @param[in] MrcData    - Include all MRC global data.

  @retval MrcStatus - return mrcSuccess
**/
MrcStatus
IssueRxReset (
  IN MrcParameters *const  MrcData
  )
{
  UINT32  Controller;
  UINT32  Channel;

  for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
    for (Channel = 0; Channel < MAX_CHANNEL; Channel++) {
      if (!MrcChannelExist(MrcData, Controller, Channel)) {
        continue;
      }
      IssueRxResetMcCh (MrcData, Controller, Channel);
    }
  }

  return mrcSuccess;
}


/**
  Read or write the RcompOdtUp or RcompOdtDown .

  @param[in] MrcData      - Pointer to MRC global data.
  @param[in] Controller   - Controller number.
  @param[in] Channel      - Channel number.
  @param[in] Strobe       - Strobe number.
  @param[in] Group        - Group value. It can be CompRcompOdtUpPerStrobe or CompRcompOdtDnPerStrobe.
  @param[in] Mode         - Mode for operations. It can be ReadUncached(read from CSR) or write to CSR.
  @param[in out] Value    - Pointer to the value for read or.

  @retval None.
**/
VOID
ReadWriteRcompRegPerStrobe (
  IN MrcParameters *const MrcData,
  IN UINT32               Controller,
  IN UINT32               Channel,
  IN UINT32               Strobe,
  IN GSM_GT const         Group,
  IN UINT32               Mode,
  IN OUT  UINT32          *Value
  )
{

  UINT32    Rank = 0;

  INT64                             GetSetVal;


  if (Mode == (ReadUncached)) {
    MrcGetSetStrobe(MrcData, Controller, Channel, Rank, Strobe, Group, Mode, &GetSetVal);
    *Value = (GetSetVal) & 0xFFFFFFFF;
  } else if (Mode == (ForceWriteNoCache)) {
    GetSetVal = ((*Value) & 0x3F);
    MrcGetSetStrobe (MrcData, Controller, Channel, Rank, Strobe, Group, Mode, &GetSetVal);

  } else {
    MRC_DEBUG_MSG(&(MrcData->Outputs.Debug), MSG_LEVEL_ERROR, "Mode is not supported in ReadWriteRcompRegPerStrobe\n");
  }

  if (MrcData->Inputs.B0) {
    GetSetVal = 1;
    //
    // DDRSCRAM_CR_DDRMISCCONTROL0.forcecompupdate = 1
    //
    MrcGetSetNoScope (MrcData, GsmIocForceCmpUpdt, WriteNoCache, &GetSetVal);
    //
    // Wait 2us for comp distribution to complete
    //
    MrcWait (MrcData, 2 * MRC_TIMER_1US);
  }
}

/**
  Program the RcompOdtUp or RcompOdtDown .

  @param[in] MrcData      - Pointer to MRC global data.
  @param[in] Controller   - Controller number.
  @param[in] Channel      - Channel number.
  @param[in] Strobe       - Strobe number.
  @param[in] VOdtCode     - VOdtCode value for program.

  @retval None.
**/
VOID
ProgramOdtUpDnStatic (
  IN MrcParameters* const MrcData,
  IN UINT32               Controller,
  IN UINT32               Channel,
  IN UINT32               Strobe,
  IN UINT32               VOdtCode
  )
{
  UINT32  TxRCompOdtUp, TxRCompOdtDn;
  UINT32   OutValue;
  DATA0CH0_CR_DQTXCTL0_STRUCT DataDqTxCtl0;
  DATA0CH0_CR_DDRCRDATACONTROL2_STRUCT DataControl2;
  UINT32    Offset;
  UINT32    TransChannel;
  UINT32    TransStrobe;
  UINT32    Rank = 0;
  BOOLEAN     A0 = MrcData->Inputs.A0;

  if (VOdtCode >= 32) {
    TxRCompOdtUp = 32;
    // Check for saturation
    TxRCompOdtDn = 63 - VOdtCode;
    //
    // 0 -> 31: txrcompodtdn=32, txrcompodtup from 0 to 31; 32 -> 63: txrcompodtdn from 31 to 0, txrcompodtup=32
    //
    if (VOdtCode > 63) {
      MRC_DEBUG_MSG(&(MrcData->Outputs.Debug), MSG_LEVEL_ERROR, "CoChSt=%d,%d,%d VOdtCode=%lx Error VODT has a limit of 63\n",
                    Controller, Channel, Strobe, VOdtCode);
      return;
    }
  } else {
    TxRCompOdtUp = VOdtCode;
    TxRCompOdtDn = 32;
    //
    // 0 -> 31: txrcompodtdn=32, txrcompodtup from 0 to 31; 32 -> 63: txrcompodtdn from 31 to 0, txrcompodtup=32
    //
  }

  OutValue = TxRCompOdtUp;

  ReadWriteRcompRegPerStrobe (MrcData, Controller, Channel, Strobe, CompRcompOdtUpPerStrobe, ForceWriteNoCache,
                              &OutValue );
  OutValue = TxRCompOdtDn;

  ReadWriteRcompRegPerStrobe (MrcData, Controller, Channel, Strobe, CompRcompOdtDnPerStrobe, ForceWriteNoCache,
                              &OutValue );



  TransChannel = Channel;
  TransStrobe = Strobe;
  MrcTranslateSystemToIp(MrcData, &Controller, &TransChannel, &Rank, &TransStrobe,
                         TxEqCoeff0);

  Offset = DATA0CH0_CR_DQTXCTL0_REG +
           (DATA0CH1_CR_DQTXCTL0_REG - DATA0CH0_CR_DQTXCTL0_REG) * TransChannel +
           (DATA1CH0_CR_DQTXCTL0_REG - DATA0CH0_CR_DQTXCTL0_REG) * TransStrobe;
  DataDqTxCtl0.Data = MrcReadCR(MrcData, Offset);
  DataDqTxCtl0.Bits.dqtx_cmn_txodtstatlegendq = 0;
  MrcWriteCR(MrcData, Offset, DataDqTxCtl0.Data);



  TransChannel = Channel;
  TransStrobe = Strobe;
  MrcTranslateSystemToIp(MrcData, &Controller, &TransChannel, &Rank, &TransStrobe,
                         TxEqCoeff0);

  Offset = DATA0CH0_CR_DDRCRDATACONTROL2_REG +
           (DATA0CH1_CR_DDRCRDATACONTROL2_REG - DATA0CH0_CR_DDRCRDATACONTROL2_REG) * TransChannel +
           (DATA1CH0_CR_DDRCRDATACONTROL2_REG - DATA0CH0_CR_DDRCRDATACONTROL2_REG) * TransStrobe;
  DataControl2.Data = MrcReadCR(MrcData, Offset);

  if (A0) {
    DataControl2.BitsA0.compupdate_ovren = 1;
  } else {
    DataControl2.Bits.compupdate_ovren = 1;
  }
  MrcWriteCR(MrcData, Offset, DataControl2.Data);
  MrcWait(MrcData, 1); //1 ns

  if (A0) {
    DataControl2.BitsA0.compupdate_ovren = 0;
  } else {
    DataControl2.Bits.compupdate_ovren = 0;
  }

  MrcWriteCR(MrcData, Offset, DataControl2.Data);
  MrcWait(MrcData, 1); //1 ns

  return;
}

/**
  This implements the MATS Advanced Memory test algorithm, to run over the specified Controller/Channels.

  @param[in] MrcData                          - Global MRC data structure
  @param[in] McChBitMask                      - Memory Controller Channel Bit mask to test.
  @param[in] CmdPat                           - Type of sequence MT_CPGC_WRITE, MT_CPGC_READ, or MT_CPGC_READ_WRITE
  @param[in] SeqDataInv                       - Specifies whether data pattern should be inverted per subsequence
  @param[in] Pattern                          - Array of 64-bit Data Pattern for the test
  @param[in] IsUseInvtPat                     - Info to indicate whether or not patternQW is inverted by comparing original pattern
  @param[in] UiShl                            - Array of Bit-shift values per UI
  @param[in] NumCacheLines                    - Number of cachelines to use in WDB
  @param[in] PatternDepth                     - Number of UI to use in patternQW
  @param[in] Direction                        - Sequential address direction MT_ADDR_DIR_UP, MT_ADDR_DIR_DOWN

  @retval status - mrcSuccess / mrcFail
**/
MrcStatus
MrcMemTestMATS (
  IN MrcParameters* const MrcData,
  IN UINT32         McChBitMask,
  IN UINT8          CmdPat,
  IN UINT8          SeqDataInv[2], // MT_MAX_SUBSEQ = 2
  IN UINT64         Pattern[16], // MT_PATTERN_DEPTH = 16
  IN BOOLEAN        IsUseInvtPat,
  IN UINT8          UiShl[16], // enable Pattern Rotation per UI
  IN UINT8          NumCacheLines,
  IN UINT8          PatternDepth, // number of UI to use from Pattern QW
  IN UINT8          Direction
  )
{
  MrcStatus         Status = mrcSuccess;
  MrcOutput         *Outputs;
  MrcDimmOut        *DimmOut;
  UINT32            LocalMcChBitMask;
  UINT32            Rank;
  UINT8             Channel;
  UINT8             Controller;
  UINT8             ColumnBits[MAX_RANK_IN_CHANNEL][MAX_CONTROLLER][MAX_CHANNEL];
  UINT8             RowBits[MAX_RANK_IN_CHANNEL][MAX_CONTROLLER][MAX_CHANNEL];
  UINT8             BankBits[MAX_RANK_IN_CHANNEL][MAX_CONTROLLER][MAX_CHANNEL];
  UINT8             RankMask;
  UINT32            Bank, BaseBits;
  ROW_FAIL_RANGE    *FailRangePtr;
  ROW_ADDR          CurAddr[MAX_CONTROLLER][MAX_CHANNEL];
  ROW_ADDR          FailAddr[MAX_CONTROLLER][MAX_CHANNEL];
  ROW_FAIL_RANGE    LastFail[MAX_CONTROLLER][MAX_CHANNEL];
  UINT32            FailSize[MAX_CONTROLLER][MAX_CHANNEL];
  INT32             FailIndex;
  UINT32            FailMax;
  UINT32            MaxRow[MAX_CONTROLLER][MAX_CHANNEL];
  UINT32            CurrentRow[MAX_CONTROLLER][MAX_CHANNEL];
  UINT32            TestSize[MAX_CONTROLLER][MAX_CHANNEL];
  UINT32            DeviceMask[MAX_CONTROLLER][MAX_CHANNEL][3];
  UINT8             TestStatus[MAX_CONTROLLER][MAX_CHANNEL];
  UINT32            RunTest;
  UINT32            done;
  UINT32            TempIndex;

  Outputs = &MrcData->Outputs;

  MRC_DEBUG_MSG (&Outputs->Debug, MSG_LEVEL_NOTE, "McChBitMask=0%x, CmdPat=%d, IsUseInvtPat=%d, NumCacheLines=%d, Direction=%d\n", McChBitMask, CmdPat, IsUseInvtPat, NumCacheLines, Direction);

  for (Rank = 0; Rank < MAX_RANK_IN_CHANNEL; Rank++) {
    MRC_DEBUG_MSG (&Outputs->Debug, MSG_LEVEL_NOTE, "///// Testing Rank = %d /////\n", Rank);
    LocalMcChBitMask = 0; // Reset bitmask for new Rank
    for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
      for (Channel = 0; Channel < Outputs->MaxChannels; Channel++) {
        if (!MrcRankExist (MrcData, Controller, Channel, Rank)) {
          continue;
        }
        DimmOut = &Outputs->Controller[Controller].Channel[Channel].Dimm[RANK_TO_DIMM_NUMBER (Rank)];
        // Initialize per-Rank Column Row and Bank sizes
        // The MrcLog2 function returns +1 so we subtract 1
        ColumnBits[Rank][Controller][Channel] = MrcLog2 (DimmOut->ColumnSize) - 1;
        RowBits[Rank][Controller][Channel] = MrcLog2 (DimmOut->RowSize) - 1;
        BankBits[Rank][Controller][Channel] = (MrcLog2 (DimmOut->Banks) - 1) + (MrcLog2 (DimmOut->BankGroups) - 1);


        RankMask = (1 << Rank);
        LocalMcChBitMask |= SelectReutRanks (MrcData, Controller, Channel, RankMask, FALSE, 0);
      }
    }
    MRC_DEBUG_MSG (&Outputs->Debug, MSG_LEVEL_NOTE, "LocalMcChBitMask = %d\n", LocalMcChBitMask);
    // Skip to next rank if no ranks enabled on any channel
    if (LocalMcChBitMask == 0) {
      continue;
    }

    for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
      for (Channel = 0; Channel < Outputs->MaxChannels; Channel++) {
        if (MC_CH_MASK_CHECK (LocalMcChBitMask, Controller, Channel, Outputs->MaxChannels) == 0) {
          continue;
        }
        SetupIOTestRetention (MrcData, Controller, Channel, 1, CmdPat, SeqDataInv, Pattern, IsUseInvtPat, UiShl, NumCacheLines, Direction); // LC = 1
      }
    }

    BaseBits = GetBaseBits (MrcData, LocalMcChBitMask, Rank);

    // For each Bank address
    for (Bank = 0; (Bank < (UINT32) (1 << BaseBits)) && LocalMcChBitMask; Bank++) {
      MRC_DEBUG_MSG (&Outputs->Debug, MSG_LEVEL_NOTE, "*** Testing Bank = %d ***\n", Bank);
      for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
        for (Channel = 0; Channel < Outputs->MaxChannels; Channel++) {
          if (MC_CH_MASK_CHECK (LocalMcChBitMask, Controller, Channel, Outputs->MaxChannels) == 0) {
            continue;
          }
          // Inititalize upper address
          CurAddr[Controller][Channel].Data = 0;
          CurAddr[Controller][Channel].Bits.BankPair = Bank;
          CurAddr[Controller][Channel].Bits.Rank = Rank;
          CurAddr[Controller][Channel].Bits.Valid = 1;

          // Get max row bits
          MaxRow[Controller][Channel] = 0;
          if (RowBits[Rank][Controller][Channel]) {
            MaxRow[Controller][Channel] = 1 << RowBits[Rank][Controller][Channel];
          }


          if (Direction == MT_ADDR_DIR_UP) {
            CurrentRow[Controller][Channel] = 0;
          } else {
            CurrentRow[Controller][Channel] = MaxRow[Controller][Channel] - 1;
          }
          // Search both directions
          if (Direction == MT_ADDR_DIR_UP) {
            for (TempIndex = 0; TempIndex < Outputs->FailMax[Controller][Channel]; TempIndex++) {
              if (CurrentAddrMatch (CurAddr[Controller][Channel], Outputs->FailRange[Controller][Channel][TempIndex].Addr)) {
                break;
              }
            }
            Outputs->FailIndex[Controller][Channel] = TempIndex;
            CurrentRow[Controller][Channel] = 0;
          } else {
            for (TempIndex = Outputs->FailMax[Controller][Channel] - 1; (INT32) TempIndex >= 0; TempIndex--) {
              if (CurrentAddrMatch (CurAddr[Controller][Channel], Outputs->FailRange[Controller][Channel][TempIndex].Addr)) {
                break;
              }
            }
            Outputs->FailIndex[Controller][Channel] = TempIndex;
            CurrentRow[Controller][Channel] = MaxRow[Controller][Channel] - 1;
          }

          // Default test status to passing
          TestStatus[Controller][Channel] = 0;
        } //ch
      } // controller
   do {
        RunTest = 0;
        for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
          for (Channel = 0; Channel < Outputs->MaxChannels; Channel++) {
            if (MC_CH_MASK_CHECK (LocalMcChBitMask, Controller, Channel, Outputs->MaxChannels) == 0) {
              continue;
            }
            // FailIndex and FailMax can be updated when Fail range list changes in subordinate functions
            FailIndex = Outputs->FailIndex[Controller][Channel];
            FailMax = Outputs->FailMax[Controller][Channel];
            if (FailIndex >= 0) {
              FailRangePtr = &(Outputs->FailRange[Controller][Channel][FailIndex]);
            }
            // Initialize to invalid fail address
            FailAddr[Controller][Channel].Data = 0;
            FailSize[Controller][Channel] = 0;

            // Get current fail address
            if ((FailIndex >= 0) && (FailIndex < (INT32) FailMax) &&
              (CmdPat != PatWr)) {
              FailRangePtr = &(Outputs->FailRange[Controller][Channel][FailIndex]);
              FailAddr[Controller][Channel] = FailRangePtr->Addr;
              FailSize[Controller][Channel] = FailRangePtr->Size;
            }

            // Check valid bit and matching upper address
            TestSize[Controller][Channel] = 0;
            if (CurrentAddrMatch (CurAddr[Controller][Channel], FailAddr[Controller][Channel])) {
              //MRC_DEBUG_MSG(&Outputs->Debug, MSG_LEVEL_NOTE, "FailAddr Match\n");
              if (Direction == MT_ADDR_DIR_UP) {
                // In passing range searching upward
                if (CurrentRow[Controller][Channel] < FailAddr[Controller][Channel].Bits.Row) {
                  TestSize[Controller][Channel] = FailAddr[Controller][Channel].Bits.Row - CurrentRow[Controller][Channel];
                  DeviceMask[Controller][Channel][0] = 0;
                  DeviceMask[Controller][Channel][1] = 0;
                  DeviceMask[Controller][Channel][2] = 0;
                } else {
                  //  In failing range searching upward
                  if (TestStatus[Controller][Channel] == 0) {
                    CurrentRow[Controller][Channel] = FailAddr[Controller][Channel].Bits.Row;
                  }
                  // If previous test failed, then current row is last fail row
                  TestSize[Controller][Channel] = FailSize[Controller][Channel] - (CurrentRow[Controller][Channel] - FailAddr[Controller][Channel].Bits.Row);
                  FailRangePtr = &(Outputs->FailRange[Controller][Channel][FailIndex]);
                  DeviceMask[Controller][Channel][0] = FailRangePtr->Mask[0];
                  DeviceMask[Controller][Channel][1] = FailRangePtr->Mask[1];
                  DeviceMask[Controller][Channel][2] = FailRangePtr->Mask[2];
                }
              } else { // direction
                        // In passing range searching downward
                if (CurrentRow[Controller][Channel] > FailAddr[Controller][Channel].Bits.Row + FailSize[Controller][Channel]) {
                  TestSize[Controller][Channel] = CurrentRow[Controller][Channel] - (FailAddr[Controller][Channel].Bits.Row + FailSize[Controller][Channel] - 1);
                  DeviceMask[Controller][Channel][0] = 0;
                  DeviceMask[Controller][Channel][1] = 0;
                  DeviceMask[Controller][Channel][2] = 0;
                } else {
                  //  In failing range searching downward
                  if (TestStatus[Controller][Channel] == 0) {
                    CurrentRow[Controller][Channel] = FailAddr[Controller][Channel].Bits.Row + FailSize[Controller][Channel] - 1;
                  }
                  // If previous test failed, then current row is last fail row
                  TestSize[Controller][Channel] = CurrentRow[Controller][Channel] + 1 - FailAddr[Controller][Channel].Bits.Row;
                  FailRangePtr = &(Outputs->FailRange[Controller][Channel][FailIndex]);
                  DeviceMask[Controller][Channel][0] = FailRangePtr->Mask[0];
                  DeviceMask[Controller][Channel][1] = FailRangePtr->Mask[1];
                  DeviceMask[Controller][Channel][2] = FailRangePtr->Mask[2];
                }
              } // direction
            } else if ((FailIndex >= 0) && (FailIndex < (INT32) FailMax) &&
              (CmdPat != PatWr)) {
              if (Direction == MT_ADDR_DIR_UP) {
                Outputs->FailIndex[Controller][Channel] = FailIndex + 1;
              } else {
                Outputs->FailIndex[Controller][Channel] = FailIndex - 1;
              }

            } else {
              // No more failures to process
              if (Direction == MT_ADDR_DIR_UP) {
                TestSize[Controller][Channel] = MaxRow[Controller][Channel] - CurrentRow[Controller][Channel];
              } else {
                TestSize[Controller][Channel] = CurrentRow[Controller][Channel] + 1;
              }
              DeviceMask[Controller][Channel][0] = 0;
              DeviceMask[Controller][Channel][1] = 0;
              DeviceMask[Controller][Channel][2] = 0;
            }
            if (TestSize[Controller][Channel]) {
              RunTest = 1;
            }
          } //ch
        } // Controller

        if (RunTest) {
          // Execute concurrent ch test on current row, test size and device mask
          // TestStatus is set for failure and LastFail will contain failing address
          Status = CpgcMemTestRowRange (MrcData, LocalMcChBitMask, Rank, ColumnBits, RowBits, BankBits, CmdPat, Direction, Bank, BaseBits,
            CurrentRow, TestSize, DeviceMask, TestStatus, SeqDataInv, IsUseInvtPat);
          // Failure means that FailRange list is full
          if (Status == mrcFail) {
            MRC_DEBUG_MSG (&Outputs->Debug, MSG_LEVEL_ERROR, "CpgcMemTestRowRange returned failure - FailRange List Full!\n");
            return Status;
          }
          // Evaluate test results
          for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
            for (Channel = 0; Channel < Outputs->MaxChannels; Channel++) {
              // Handle rank that was disabled
              if (MC_CH_MASK_CHECK (LocalMcChBitMask, Controller, Channel, Outputs->MaxChannels) == 0) {
                continue;
              }
              // If the test ran on this channel
              if (TestSize[Controller][Channel]) {
                // If test passed
                if (TestStatus[Controller][Channel] == 0) {
                  // If completed testing a fail range
                  if ((CurrentRow[Controller][Channel] >= FailAddr[Controller][Channel].Bits.Row) &&
                      (CurrentRow[Controller][Channel] < FailAddr[Controller][Channel].Bits.Row + FailSize[Controller][Channel])) {
                    // Move to next fail range
                    FailIndex = Outputs->FailIndex[Controller][Channel];
                    if (Direction == MT_ADDR_DIR_UP) {
                      if (FailIndex < 128) {
                        Outputs->FailIndex[Controller][Channel] = FailIndex + 1;
                      }
                    } else {
                      if (FailIndex >= 0) {
                        Outputs->FailIndex[Controller][Channel] = FailIndex - 1;
                      }
                    }
                  }
                  // Test passed so update CurrentRow
                  if (Direction == MT_ADDR_DIR_UP) {
                    //MRC_DEBUG_MSG(&Outputs->Debug, MSG_LEVEL_NOTE, "Test passed: CurrentRow=0x%08x, TestSize=0x%08x, ", CurrentRow[Controller][Channel], TestSize[Controller][Channel]);
                    CurrentRow[Controller][Channel] += TestSize[Controller][Channel];
                    //MRC_DEBUG_MSG(&Outputs->Debug, MSG_LEVEL_NOTE, "new CurrentRow=0x%08x\n", CurrentRow[Controller][Channel]);
                  } else {
                    CurrentRow[Controller][Channel] -= TestSize[Controller][Channel];  // could result in -1, so casting is needed on exit path
                  }
                } else {
                  LastFail[Controller][Channel] = Outputs->LastFail[Controller][Channel];
                  // Assert that the last fail address is within the current test range
                  if (((Direction == MT_ADDR_DIR_UP) && ((LastFail[Controller][Channel].Addr.Bits.Row < CurrentRow[Controller][Channel]) || (LastFail[Controller][Channel].Addr.Bits.Row >= CurrentRow[Controller][Channel] + TestSize[Controller][Channel]))) ||
                    ((Direction == MT_ADDR_DIR_DOWN) && ((LastFail[Controller][Channel].Addr.Bits.Row > CurrentRow[Controller][Channel]) || ((INT32)LastFail[Controller][Channel].Addr.Bits.Row <= (INT32) (CurrentRow[Controller][Channel] - TestSize[Controller][Channel]))))) {
                    // Something went wrong if we got failure on a row, mask that was not part of the current test range
                    MRC_DEBUG_ASSERT (FALSE, &Outputs->Debug, "CpgcMemTestMATS: Failure occurred outside of test range: LastFail row = 0x%08x, CurrentRow = 0x%08x, TestSize = 0x%08x, direction = %d\n",
                      LastFail[Controller][Channel].Addr.Bits.Row, CurrentRow[Controller][Channel], TestSize[Controller][Channel], Direction);
                    return mrcFail;
                  }
                  // Test failed so restart at last fail address!
                  CurrentRow[Controller][Channel] = LastFail[Controller][Channel].Addr.Bits.Row;
                }
              }
            } // Channel
          } // Controller
        } //if RunTest

        // Check if all rows done
        done = 1;
        for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
          for (Channel = 0; Channel < Outputs->MaxChannels; Channel++) {
            if (MC_CH_MASK_CHECK (LocalMcChBitMask, Controller, Channel, Outputs->MaxChannels) == 0) {
              continue;
            }
            if (Direction == MT_ADDR_DIR_UP) {
              // Searching upward
              if (CurrentRow[Controller][Channel] < MaxRow[Controller][Channel]) {
                done = 0;
                break;
              }
            } else {
              // Searching downward
              if ((INT32) CurrentRow[Controller][Channel] >= 0) {
                done = 0;
                break;
              }
            } // direction
          } // ch
        } // Controller
      } while (!done);
    } //for bank
  } // rank

  return Status;
}
