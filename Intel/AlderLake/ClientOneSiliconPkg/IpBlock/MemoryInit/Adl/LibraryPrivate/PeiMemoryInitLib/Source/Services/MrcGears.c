/** @file
  This file include all the MRC Gears related algorithms.

@copyright
  INTEL CONFIDENTIAL
  Copyright 2017 - 2020 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains an 'Intel Peripheral Driver' and is uniquely identified as
  "Intel Reference Module" and is licensed for Intel CPUs and chipsets under
  the terms of your license agreement with Intel or your vendor. This file may
  be modified by the user, subject to additional terms of the license agreement.

@par Specification Reference:
**/
#include "MrcTypes.h"
#include "McAddress.h"
#include "MrcCommon.h"
#include "MrcGears.h"
#include "MrcWriteDqDqs.h"
#include "MrcDdrIoApi.h"
#include "MrcDdrIoApiInt.h"

#define DCC_PRINT  (1)

/**
  This function finalizes the DCC FSM control registers for normal operation.

  @param[in]  MrcData - Pointer to global MRC data.
**/
VOID
MrcDccFsmFinalize (
  IN  MrcParameters * const MrcData
  )
{/* todo_adl
  UINT32 Channel;
  UINT32 Data;
  UINT32 Offset;
  UINT32 FllPs;
  UINT32 DataNumSamples;
  UINT32 CccNumSamples;
  DATA0CH0_CR_DCCLANESTATUS3_STRUCT DataDccLaneStatus3;
  DATA0CH0_CR_DCCFSMCONTROL_STRUCT  DataDccFsmCtrl;
  CCC_CR_DCCFSMCONTROL_STRUCT       CccDccFsmCtrl;

  // FLL Target ratio is 3133 for 133 RefClk and 3100 for 100 RefClk
  FllPs = 1000000 / ((MrcData->Inputs.RefClk == MRC_REF_CLOCK_133) ? 3133 : 3100);

  // RANGE (RoundUpTo2^NandReturnN ((1000nS / tFLL / 2)*(tQCLK / .3125nS)) - 12), 0, 4);
  // Scale to ps on the constants and optimize down to a single divide
  // Lowest QclkPs is 938.  We stay under 1 billion.  No overflow on UINT32.
  DataNumSamples = (1000000 * MrcData->Outputs.Qclkps) / (FllPs * 2 * 313);
  DataNumSamples = MrcLog2 (DataNumSamples);
  DataNumSamples = MAX (DataNumSamples, 12);
  DataNumSamples -= 12;

  // RANGE  (RoundUpTo2^NandReturnN ((2000nS / tFLL / 2)*(tQCLK / .3125nS) )-12), 0, 4);
  // Scale to ps on the constants and optimize down to a single divide
  // Lowest QclkPs is 938.  We stay under 1 billion.  No overflow on UINT32.
  CccNumSamples = (2000000 * MrcData->Outputs.Qclkps) / (FllPs * 2 * 313);
  CccNumSamples = MrcLog2 (CccNumSamples);
  CccNumSamples = MAX (CccNumSamples, 12);
  CccNumSamples -= 12;

  // Finalize DCC configuration
  MrcWriteCrMulticast (MrcData, CCC_CR_DCCFSMSTATUS_REG, 0);
  MrcWriteCrMulticast (MrcData, DATA_CR_DCCFSMSTATUS_REG, 0);

  for (Channel = 0; Channel < MRC_NUM_CCC_INSTANCES; Channel++) {
    Offset = OFFSET_CALC_CH (CH0CCC_CR_DCCFSMCONTROL_REG, CH1CCC_CR_DCCFSMCONTROL_REG, Channel);
    CccDccFsmCtrl.Data = MrcReadCR (MrcData, Offset);
    CccDccFsmCtrl.Bits.UpdateTcoComp = 1;
    CccDccFsmCtrl.Bits.DccSamples = CccNumSamples;
    MrcWriteCR (MrcData, Offset, CccDccFsmCtrl.Data);

    // DDRDATA partition only has 2 groups of 8 instances.
    if (Channel < 2) {
      for (Data = 0; Data < 8; Data++) {
       Offset = OFFSET_CALC_MC_CH (DATA0CH0_CR_DCCLANESTATUS3_REG, DATA0CH1_CR_DCCLANESTATUS3_REG, Channel, DATA1CH0_CR_DCCLANESTATUS3_REG, Data);
       DataDccLaneStatus3.Data = MrcReadCR (MrcData, Offset);
       DataDccLaneStatus3.Bits.ExtremeCount = 0;
       DataDccLaneStatus3.Bits.ExtremeOffset = 0;
       MrcWriteCR (MrcData, Offset, DataDccLaneStatus3.Data);

       Offset = OFFSET_CALC_MC_CH (DATA0CH0_CR_DCCFSMCONTROL_REG, DATA0CH1_CR_DCCFSMCONTROL_REG, Channel, DATA1CH0_CR_DCCFSMCONTROL_REG, Data);
       DataDccFsmCtrl.Data = MrcReadCR (MrcData, Offset);
       DataDccFsmCtrl.Bits.DccSamples = DataNumSamples;
       MrcWriteCR (MrcData, Offset, DataDccFsmCtrl.Data);
      }
    }
  }
  */
}

