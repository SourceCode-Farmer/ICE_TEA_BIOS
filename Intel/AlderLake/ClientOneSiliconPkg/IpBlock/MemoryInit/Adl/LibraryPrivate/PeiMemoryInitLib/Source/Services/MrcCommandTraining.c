/** @file
  Implementation of the command training algorithm.
  The algorithm finds the N mode for the current board and also the correct
  CLK CMD CTL pi setting.

@copyright
  INTEL CONFIDENTIAL
  Copyright 1999 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains an 'Intel Peripheral Driver' and is uniquely identified as
  "Intel Reference Module" and is licensed for Intel CPUs and chipsets under
  the terms of your license agreement with Intel or your vendor. This file may
  be modified by the user, subject to additional terms of the license agreement.

@par Specification Reference:
**/
//
// Include files
//
#include "MrcCommandTraining.h"
#include "MrcCpgcApi.h"
#include "Cpgc20Patterns.h"
#include "Cpgc20TestCtl.h"
#include "MrcCpgcOffsets.h"
#include "MrcMemoryApi.h"
#include "MrcLpddr4.h"
#include "MrcLpddr5.h"
#include "MrcDdr5.h"
#include "MrcDdr5Registers.h"
#include "MrcChipApi.h"
#include "MrcCommon.h"
#include "MrcMaintenance.h"

#define CAPATTERNMASK     (0xFF)
#define FEEDBACKMASK      (0xFF)
#define FEEDBACKMASK_ECC  (0x0F)
#define MAX_CBT_PHASES    (1)

static const UINT16 CsPiTableLP4G1 [] = {64, 80, 96, 112, 128, 144, 160, 176, 192, 208, 224, 240};
static const UINT16 CsPiTableLP4G2 [] = {32, 48, 64, 80, 96, 112, 128, 144, 160, 176, 192, 208};
static const UINT16 CsPiTableLP4G4 [] = {144, 160, 176, 192, 208, 224, 240, 256, 272, 288, 304, 320};
static const UINT16 CsCaPiTableLP5G2 [] = {0, 16, 32, 48, 64, 80, 96, 112, 128, 144, 160, 176, 192, 208, 224, 240, 256, 272};
static const UINT16 CsCaPiTableLP5G4 [] = {0, 16, 32, 48, 64, 80, 96, 112, 128, 144, 160, 176, 192, 208, 224, 240, 256, 272};

// DDR5 ECT local definitions and constants

// Save Data structure for DDR5 CS\CA training IO init
typedef struct {
  INT64 BiasPMCtrl;
  INT64 DefDrvEnLow;
  INT64 ForceRxOn;
  INT64 InternalClocksOn;
  INT64 CmdTriStateDisSave[MAX_CONTROLLER][MAX_CHANNEL];
} DDR5_CA_TRAIN_IO_INIT_SAVE;

// Function Pointer definition for DDR5 CA Training
// CADB Victim \ Agressor pattern generator.
typedef
void
(*MRC_CADB_PATTERN_GENERATOR) (
  IN  UINT8     Victim,
  OUT CADB_LINE CadbLines[8]
  );


// Function Pointer definition for DDR5 CS\CA VREF set function
typedef
void
(*MRC_SET_VREF) (
  IN MrcParameters *const MrcData,
  IN UINT32               Controller,
  IN UINT32               Channel,
  IN UINT32               Rank,
  IN DDR5_MR10_VREF       VrefCa,
  IN BOOLEAN              DebugPrint
  );

// CADB_BUF CA lane values corresponding
// to each mux select line
#define CADB_MUX_S0     (0xAA)
#define CADB_MUX_S0_INV (0xFF & ~CADB_MUX_S0)
#define CADB_MUX_S1     (0xCC)
#define CADB_MUX_S1_INV (0xFF & ~CADB_MUX_S1)
#define CADB_MUX_S2     (0xF0)

// DDR5 CA Training defines
#define DDR5_CA_TRAIN_LMN_PERIOD  64

// DDR5 DataTrainFeedback SW sample loop count
#define DDR5_FEEDBACK_CSTM_NUM_SAMPLES 16
#define DDR5_FEEDBACK_CATM_NUM_SAMPLES 32

// CS Training Parameters
#define MRC_CS_VREF_STEP_SIZE 3
#define MRC_CS_VREF_START     Ddr5Vref_70p0
#define MRC_CS_VREF_END       Ddr5Vref_60p0
#define MRC_CS_VREF_SAMPLES   ((MRC_CS_VREF_END - MRC_CS_VREF_START) / MRC_CS_VREF_STEP_SIZE)

// CA Training Parameters
#define MRC_CA_VREF_STEP_SIZE 4
#define MRC_CA_VREF_START     Ddr5Vref_80p0
#define MRC_CA_VREF_END       Ddr5Vref_65p0
#define MRC_CA_VREF_SAMPLES   ((MRC_CA_VREF_END - MRC_CA_VREF_START) / MRC_CA_VREF_STEP_SIZE)

// Structure used to store limit results
typedef struct {
  UINT32   LeftEdge;
  UINT32   RightEdge;
} EDGE_SAMPLES;


//
// Traning phase enumerator used by training steps that
// will execute with different paramters depending on the
// selected phase. The actual definition depends on the
// training step.
// Example:
//   DDR5 CS Training:
//      Phase X = Don't Care (only one phase implemented)
//   DDR5 CA Training:
//      Phase 0 = Simple CA Training Pattern (Lone 0 \ Lone 1)
//                CMD PI sweep covers complete CMD PI window in coarse steps
//      Phase 1 = Aggressive CA Training Pattern (Victim \ Agressor with deselect stress)
//                CMD PI sweep covers +/- 32 pi ticks from current CmdPiCode
typedef enum {
  MrcTrainingPhase0,
  MrcTrainingPhase1
} MrcTrainingPhase;

/**
  This function performs early command training.
  Center CTL-CLK timing to allow subsequent steps to work

  @param[in] MrcData - Include all MRC global data.
**/
MrcStatus
MrcEarlyCommandTraining (
  IN MrcParameters *const MrcData
  )
{
  MrcOutput        *Outputs;
  MrcStatus        Status;

  Outputs        = &MrcData->Outputs;
  Status         = mrcSuccess;

  // Check if LPDDR memory is used
  if (Outputs->Lpddr) {
    Status = EarlyCommandTrainingLpddr (MrcData);
  } else if (Outputs->DdrType == MRC_DDR_TYPE_DDR5) {
    Status = EarlyCommandTrainingDdr5 (MrcData);
  }
  return Status;
}

/**
  Find a good starting point for DDR3/4 CTL/CMD/CLK, using a quick 2D search.
  Sweep CTL/CMD PI range and look for best CLK margin using linear CLK sweep at each point.

  @param[in] MrcData         - Include all MRC global data.

  @retval MrcStatus
**/
MrcStatus
CtlClockCentering (
  IN MrcParameters *const MrcData
  )
{
  const MrcInput      *Inputs;
  MrcDebug            *Debug;
  const MRC_FUNCTION  *MrcCall;
  MrcOutput           *Outputs;
  MrcStatus           Status;
  INT64               Min;
  INT64               Max;
  UINT32              Left[MAX_CONTROLLER][MAX_CHANNEL];
  UINT32              Right[MAX_CONTROLLER][MAX_CHANNEL];
  UINT32              Width[MAX_CONTROLLER][MAX_CHANNEL];
  UINT32              MaxWidth[MAX_CONTROLLER][MAX_CHANNEL];
  UINT16              BestCtlPi[MAX_CONTROLLER][MAX_CHANNEL];
  UINT16              BestCtlPiLast[MAX_CONTROLLER][MAX_CHANNEL];
  UINT32              Controller;
  UINT32              Channel;
  UINT8               McChBitMask;
  UINT8               RankMask;
  UINT8               ResultRank[MAX_CONTROLLER][MAX_CHANNEL];
  UINT8               Rank;
  UINT16              CtlPiCode;
  UINT16              CtlLow;
  UINT16              CtlHigh;
  UINT8               CtlStep;
  INT16               ClkLow;
  UINT16              ClkHigh;
  UINT8               ClkStep;
  UINT8               MaxChannels;
  BOOLEAN             SkipVref;
  BOOLEAN             SkipPrint;

  Inputs         = &MrcData->Inputs;
  MrcCall        = Inputs->Call.Func;
  Outputs        = &MrcData->Outputs;
  Debug          = &Outputs->Debug;
  Status         = mrcSuccess;
  MaxChannels    = Outputs->MaxChannels;
  MrcCall->MrcSetMem ((UINT8 *) Left, sizeof (Left), 0);
  MrcCall->MrcSetMem ((UINT8 *) Right, sizeof (Right), 0);
  MrcCall->MrcSetMem ((UINT8 *) Width, sizeof (Width), 0);
  MrcCall->MrcSetMem ((UINT8 *) BestCtlPi, sizeof (BestCtlPi), 0);
  MrcCall->MrcSetMem ((UINT8 *) BestCtlPiLast, sizeof (BestCtlPiLast), 0);
  MrcCall->MrcSetMem ((UINT8 *) ResultRank, sizeof (ResultRank), 0);

  MrcGetSetLimits (MrcData, ClkGrpPi, &Min, &Max, NULL);
  ClkLow  = (UINT16) (Min + 64); // No need to sweep full Clk range
  ClkHigh = (UINT16) ClkLow + 128;
  ClkLow  = (UINT16) MAX (ClkLow, Min);
  ClkHigh = (UINT16) MIN (ClkHigh, Max);
  CtlLow  = (UINT16) (Min + 32);  // keep 32 ticks on each side for later margining steps.
  CtlHigh = (UINT16) (CtlLow + 128);
  CtlStep = 16;
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "CtlLow: %u, CtlHigh: %u\n", CtlLow, CtlHigh);

  RankMask = Outputs->ValidRankMask;

  GetFirstRank (MrcData, ResultRank);

  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\n*** Center Clock Timing per rank\n");
  // Setup CPGC Test to iteration through appropriate ranks during test
  McChBitMask = 0;
  for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
    if (MrcControllerExist (MrcData, Controller)) {
      for (Channel = 0; Channel < MaxChannels; Channel++) {
        McChBitMask |= SelectReutRanks (MrcData, (UINT8) Controller, (UINT8) Channel, RankMask, FALSE, 0);
        if (MC_CH_MASK_CHECK (McChBitMask, Controller, Channel, MaxChannels)) {
          MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\tMc%uCh%u\t\t", Controller, Channel);
        }
      }
    }
  }
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\n");
  for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
    if (MrcControllerExist (MrcData, Controller)) {
      for (Channel = 0; Channel < MaxChannels; Channel++) {
        if (MC_CH_MASK_CHECK (McChBitMask, Controller, Channel, MaxChannels)) {
          MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\tLeft\tRight\tWidth", Controller, Channel);
        }
      }
    }
  }
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\n");

  SkipVref  = TRUE;
  SkipPrint = TRUE;
  MrcCall->MrcSetMem ((UINT8 *) MaxWidth, sizeof (MaxWidth), 0);

  for (CtlPiCode = CtlLow; CtlPiCode <= CtlHigh; CtlPiCode += CtlStep) {
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "%u\t", CtlPiCode);
    for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
      for (Channel = 0; Channel < MaxChannels; Channel++) {
        if (MC_CH_MASK_CHECK (McChBitMask, Controller, Channel, MaxChannels)) {
          // Shift both CTL and CMD
          ShiftPIforCmdTraining (MrcData, Controller, Channel, MrcIterationCtl,  RankMask, MRC_IGNORE_ARG_8, CtlPiCode, 1);
          ShiftPIforCmdTraining (MrcData, Controller, Channel, MrcIterationCmd,  MRC_IGNORE_ARG_8, 0xF, CtlPiCode, 1);  //0xF = All 4 Cmd Groups
        }
      }
    }

    ClkStep = 6;
    CmdLinearFindEdges (MrcData, MrcIterationClock, McChBitMask, RankMask, 3, ClkLow, ClkHigh, ClkStep, 1, (INT8 *) NULL, SkipPrint, SkipVref);

    for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
      for (Channel = 0; Channel < MaxChannels; Channel++) {
        if ((MC_CH_MASK_CHECK (McChBitMask, Controller, Channel, MaxChannels)) == 0) {
          if (Channel == 0) {
            MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\t\t\t\t\t");
          }
          continue;
        }
        Rank = ResultRank[Controller][Channel];
        Left[Controller][Channel]  = Outputs->MarginResult[LastCmdT][Rank][Controller][Channel][0][0] / 10;
        Right[Controller][Channel] = Outputs->MarginResult[LastCmdT][Rank][Controller][Channel][0][1] / 10;
        Width[Controller][Channel] = Right[Controller][Channel] - Left[Controller][Channel];
        if (Width[Controller][Channel] > MaxWidth[Controller][Channel]) {
          MaxWidth[Controller][Channel] = Width[Controller][Channel];
          BestCtlPi[Controller][Channel] = BestCtlPiLast[Controller][Channel] = CtlPiCode;
        } else if (Width[Controller][Channel] == MaxWidth[Controller][Channel]) { // Track the last PI which still gives the max width
          BestCtlPiLast[Controller][Channel] = CtlPiCode;
        }
        MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "%u\t%u\t%u\t", Left[Controller][Channel], Right[Controller][Channel], Width[Controller][Channel]);
        // UpdateHost = 1 so that we reset the IntClkAlignedMargins->Valid flag in ShiftDQPIs
        ShiftPIforCmdTraining (MrcData, Controller, Channel, MrcIterationClock, RankMask, MRC_IGNORE_ARG_8, 0, 1); // Reset everything back to 0
      } // channel
    } // Controller
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\n");
  } // for CtlPiCode

  // Apply the best CTL/CMD PI and re-center the CLK
  for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
    for (Channel = 0; Channel < MaxChannels; Channel++) {
      if (MC_CH_MASK_CHECK (McChBitMask, Controller, Channel, MaxChannels)) {
        CtlPiCode = (BestCtlPi[Controller][Channel] + BestCtlPiLast[Controller][Channel]) / 2;
        MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Mc%u: Ch%u: Best CTL/CMD PI is %u, CLK width = %u\n",
                       Controller, Channel, CtlPiCode, MaxWidth[Controller][Channel]);
        ShiftPIforCmdTraining (MrcData, Controller, Channel, MrcIterationCtl, RankMask, MRC_IGNORE_ARG_8, CtlPiCode, 1);
        ShiftPIforCmdTraining (MrcData, Controller, Channel, MrcIterationCmd, MRC_IGNORE_ARG_8, 0xF, CtlPiCode, 1);  //0xF = All 4 Cmd Groups
      }
    } // Channel
  } // Controller
  SkipPrint = FALSE;
  ClkStep = 4;
  Status = CmdLinearFindEdges (MrcData, MrcIterationClock, McChBitMask, RankMask, 3, ClkLow, ClkHigh, ClkStep, 1, (INT8 *) NULL, SkipPrint, SkipVref);

  return Status;
}

/**
  This function performs Late command training.
  Center CMD/CTL-CLK timing using complex patterns.

  @param[in] MrcData - Include all MRC global data.

  @retval MrcStatus - If it's a success return mrcSuccess
**/
MrcStatus
MrcLateCommandTraining (
  MrcParameters *const MrcData
  )
{
  const MrcInput      *Inputs;
  MrcDebug            *Debug;
  MrcOutput           *Outputs;
  MrcChannelOut       *ChannelOut;
  MrcIntOutput        *IntOutputs;
  MrcIntControllerOut *IntControllerOut;
  MrcIntCmdTimingOut  *IntCmdTiming;
  MrcStatus           Status;
  MrcProfile          Profile;
//INT64               GetSetVal;
//UINT32              MinCode;
//UINT8               Byte;
  UINT32              Controller;
  UINT32              Channel;
  UINT16              CmdPiCode[MAX_CONTROLLER][MAX_CHANNEL];
  UINT16              CtlPiCode[MAX_RANK_IN_CHANNEL][MAX_CONTROLLER][MAX_CHANNEL];
  UINT16              MidPointCmd[MAX_COMMAND_GROUPS][MAX_CONTROLLER][MAX_CHANNEL];
  UINT8               Loopcount;
  UINT8               Cmd2N;
  UINT8               McChBitMask;
  UINT8               RankMask;
  UINT8               Rank;
  UINT8               Ranks;
  UINT8               CmdGroup;
  UINT8               CmdGroupMax;
  BOOLEAN             Ddr4;
  BOOLEAN             ExitOnFailure;
  MC0_CH0_CR_CADB_CFG_STRUCT  CadbConfig;

  Inputs        = &MrcData->Inputs;
  Outputs       = &MrcData->Outputs;
  Debug         = &Outputs->Debug;
  Profile       = Inputs->MemoryProfile;
  ExitOnFailure = (Inputs->ExitOnFailure == TRUE);
  IntOutputs    = (MrcIntOutput *) (MrcData->IntOutputs.Internal);
  Ddr4          = (Outputs->DdrType == MRC_DDR_TYPE_DDR4);

  CmdGroupMax = MrcGetCmdGroupMax (MrcData);

  RankMask  = Outputs->ValidRankMask;
  McChBitMask = 0;

  Loopcount = 10;
  if (Outputs->DdrType != MRC_DDR_TYPE_LPDDR5) {
    MrcBlockTrainResetToggle (MrcData, FALSE);
  }

  // Setup CPGC
  // LC = 10, SOE = 1 (NTHSOE), EnCADB = 1, EnCKE = 0
  SetupIOTestCADB (MrcData, Outputs->McChBitMask, Loopcount, NTHSOE, 1, 0);

  // Program default rank mapping
  for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
    if (MrcControllerExist (MrcData, Controller)) {
      for (Channel = 0; Channel < Outputs->MaxChannels; Channel++) {
        McChBitMask |= SelectReutRanks (MrcData, (UINT8)Controller, (UINT8)Channel, RankMask, FALSE, 0);
      }
    }
  } // Controller

  if (Ddr4) {
    Cmd2N = FALSE;
    for (Controller = 0; Controller < MAX_CONTROLLER; Controller++)  {
      // Set initial Pi Values
      for (Channel = 0; Channel < Outputs->MaxChannels; Channel++) {
        ChannelOut = &Outputs->Controller[Controller].Channel[Channel];
        IntCmdTiming = &IntOutputs->Controller[Controller].CmdTiming[Channel];
        if (MrcChannelExist(MrcData, Controller, Channel)) {
          Cmd2N = (ChannelOut->Timing[Profile].NMode == 2) ? TRUE : FALSE;
          CmdPiCode[Controller][Channel] = (Cmd2N == TRUE) ? 149 : 128;
          // all ranks are initialized with the same value so use only Rank 0 here
          CtlPiCode[0][Controller][Channel] = 128;
          MRC_DEBUG_MSG(
             Debug,
             MSG_LEVEL_NOTE,
             "Mc%u, Ch%u, Cmd2N =%d, CmdPiCode = %d, McChBitMask = 0x%x\n",
             Controller,
             Channel,
             Cmd2N,
             CmdPiCode[Controller][Channel],
             McChBitMask
             );
          // Shift everything to the right.  To get DQ timing right, program Clk to 0
          ShiftPIforCmdTraining (
            MrcData,
            Controller,
            Channel,
            MrcIterationClock,
            ChannelOut->ValidRankBitMask,
            1,
            0 - (INT32) (IntCmdTiming->ClkPiCode[0]),
            1
            );
          ShiftPIforCmdTraining (MrcData, Controller, Channel, MrcIterationCmd, MRC_IGNORE_ARG_8, 0xF, CmdPiCode[Controller][Channel], 1);  //0xF = All 4 Cmd Groups
          ShiftPIforCmdTraining (MrcData, Controller, Channel, MrcIterationCtl, ChannelOut->ValidRankBitMask, MRC_IGNORE_ARG_8, CtlPiCode[0][Controller][Channel], 1);
        }
      }
    } // Controller
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\n*** Center CMD/CTL vs. CLK\n");
    // Center CMD/CTL vs. CLK
    Status = CtlClockCentering (MrcData);
    if ((Status != mrcSuccess) && ExitOnFailure) {
      return Status;
    }
  } // Ddr4

  // Get the current midpoint for CMD and CTL
  for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
    IntControllerOut = &IntOutputs->Controller[Controller];
    for (Channel = 0; Channel < Outputs->MaxChannels; Channel++) {
      if (MrcChannelExist(MrcData, Controller, Channel)) {
        IntCmdTiming = &IntControllerOut->CmdTiming[Channel];
        for (Rank = 0; Rank < MAX_RANK_IN_CHANNEL; Rank++) {
          if ((MRC_BIT0 << Rank) & RankMask) {
            CtlPiCode[Rank][Controller][Channel] = IntCmdTiming->CtlPiCode[Rank];
            MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Mc%u C%u R%u CtlPiCode: %u\n", Controller, Channel, Rank, CtlPiCode[Rank][Controller][Channel]);
          }
        } // Rank
        for (CmdGroup = 0; CmdGroup < CmdGroupMax; CmdGroup++) {
          MidPointCmd[CmdGroup][Controller][Channel] = IntCmdTiming->CmdPiCode[CmdGroup];
          MRC_DEBUG_MSG ( Debug, MSG_LEVEL_NOTE, "Mc%u C%u CmdGrp%u PI: %u\n", Controller, Channel, CmdGroup, MidPointCmd[CmdGroup][Controller][Channel]);
        } // CmdGroup
      }
    }
  }

  // Center Command Timing - per CMD group
  for (CmdGroup = 0; CmdGroup < CmdGroupMax; CmdGroup++) {
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\n\n*** Center Cmd Group %u PI Timing\n", CmdGroup);
    Status = CmdTimingCentering (MrcData, MrcIterationCmd, RankMask, (1 << CmdGroup), MidPointCmd[CmdGroup]);
    if ((Status != mrcSuccess) && (ExitOnFailure)) {
      return Status;
    }
  }

  // Center Control Timing - per Rank
  // @todo_adl: LP4/5 have a single CTL PI for ranks 0 & 1
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\n\n*** Center Control Timing.");
  for (Rank = 0; Rank < MAX_RANK_IN_CHANNEL; Rank++) {
    Ranks = (1 << Rank);
    Ranks = Ranks & RankMask;

    if (Ranks) {
      MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\n\n Rank %d\n", Rank);
      Status = CmdTimingCentering (MrcData, MrcIterationCtl, Ranks, MRC_IGNORE_ARG_8, CtlPiCode[Rank]);
      if ((Status != mrcSuccess) && (ExitOnFailure)) {
        return Status;
      }
    }
  }
/*
  // Normalize timing back to 0 to improve performance
  if (Ddr4) {
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\n*** Normalize timing back to 0\n");
    for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
      IntControllerOut = &IntOutputs->Controller[Controller];
      for (Channel = 0; Channel < Outputs->MaxChannels; Channel++) {
        if (!MrcChannelExist (MrcData, Controller, Channel)) {
          continue;
        }
        IntCmdTiming = &IntControllerOut->CmdTiming[Channel];

        // Find the minimum PI Code across all relevant CMD and CTL fubs
        // CLK shift will also change RcvEn / TxDq / TxDqs, so check them as well.
        MinCode     = MRC_UINT32_MAX;
        for (CmdGroup = 0; CmdGroup < CmdGroupMax; CmdGroup++) {
          MinCode = MIN (MinCode, IntCmdTiming->CmdPiCode[CmdGroup]);
        }

        for (Rank = 0; Rank < MAX_RANK_IN_CHANNEL; Rank++) {
          if (MrcRankExist (MrcData, Controller, Channel, Rank)) {
            // Cke PI
            MinCode = MIN (MinCode, IntCmdTiming->CkePiCode[Rank]);
            // Ctl PI
            MinCode = MIN (MinCode, IntCmdTiming->CtlPiCode[Rank]);
            for (Byte = 0; Byte < Outputs->SdramCount; Byte++) {
              MrcGetSetStrobe(MrcData, Controller, Channel, Rank, Byte, RecEnDelay, ReadFromCache, &GetSetVal);
              MinCode = MIN (MinCode, (UINT32) GetSetVal);
              MrcGetSetStrobe(MrcData, Controller, Channel, Rank, Byte, TxDqsDelay, ReadFromCache, &GetSetVal);
              MinCode = MIN (MinCode, (UINT32) GetSetVal);
              MrcGetSetStrobe(MrcData, Controller, Channel, Rank, Byte, TxDqDelay, ReadFromCache, &GetSetVal);
              MinCode = MIN (MinCode, (UINT32) GetSetVal);
            }
          }
        }

        if (MinCode >= 32) {
          MinCode = MIN (MinCode, MinCode - 32);  // Keep at least 32 PI ticks for margining.
        } else {
          MinCode = 0;
        }

        MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Mc%d C%d: shifting all PI settings by Min PI Code = %d\n", Controller, Channel, MinCode);
#ifndef CTE_FLAG
        // Don't do final shifting as CLK cannnot be shifted in CTE
        ShiftChannelTiming (MrcData, Controller, Channel, (-1) * MinCode);
#endif
      } // for Channel
    } // for Controller
  } // Ddr4
*/
  // Disable CADB Deselects after Command Training
  CadbConfig.Data = 0;
  Cadb20ConfigRegWrite (MrcData, CadbConfig);

  // Finish Training with JEDEC Reset / Init
  Status = MrcResetSequence (MrcData);
  if (Outputs->DdrType != MRC_DDR_TYPE_LPDDR5) {
    MrcBlockTrainResetToggle (MrcData, TRUE);
  }

  // OptimizeTxDqs (MrcData);  // @todo_adl Merge from TGL ?

  return Status;
}

/**
  Perform Command Voltage Centering.

  @param[in, out] MrcData   - Include all MRC global data.
  @param[in]      LoopCount - Test Loopcount
  @param[in]      StepSize  - Test step size
  @param[in]      PrintMsg  - Debug prints enabled or not

  @retval MrcStatus - if it succeded returns mrcSuccess
**/
extern
MrcStatus
CmdVoltageCentering (
  IN OUT MrcParameters *const MrcData,
  IN     UINT32               LoopCount,
  IN     UINT32               StepSize,
  IN     BOOLEAN              PrintMsg
  )
{
  const MrcInput *Inputs;
  MrcDebug       *Debug;
  const MRC_FUNCTION *MrcCall;
  MrcOutput      *Outputs;
  MrcChannelOut  *ChannelOut;
  MrcDimmOut     *DimmOut;
  MrcStatus      Status;
  UINT16         *Margin;
  UINT8          Controller;
  UINT8          Channel;
  UINT8          Rank;
  UINT8          Byte;
  UINT8          RankMask;
  UINT8          MaxChannels;
  UINT16         Low[MAX_CONTROLLER][MAX_CHANNEL][MAX_RANK_IN_CHANNEL];
  UINT16         High[MAX_CONTROLLER][MAX_CHANNEL][MAX_RANK_IN_CHANNEL];
  UINT32         Height;
  UINT16         MinHeight;
  INT16          Center[MAX_CONTROLLER][MAX_CHANNEL][MAX_RANK_IN_CHANNEL];
  //INT16          ByteCenter;
  BOOLEAN        GetBerMargin;
//  BOOLEAN        Ddr5;
  MC0_CH0_CR_CADB_CFG_STRUCT  CadbConfig;
  BOOLEAN        Ddr4;
  BOOLEAN        Ddr4VrefCAPerDimm;
  UINT8          FirstRank;
  UINT8          Dimm;

  Inputs      = &MrcData->Inputs;
  MrcCall     = Inputs->Call.Func;
  Outputs     = &MrcData->Outputs;
  Debug       = &Outputs->Debug;
  MaxChannels = Outputs->MaxChannels;
  Status      = mrcSuccess;
//  Ddr5        = (Outputs->DdrType == MRC_DDR_TYPE_DDR5);
  GetBerMargin  = FALSE;
  MrcCall->MrcSetMem ((UINT8 *) Low, sizeof (Low), 0xFF);
  MrcCall->MrcSetMem ((UINT8 *) High, sizeof (High), 0xFF);
  MrcCall->MrcSetMem ((UINT8 *) Center, sizeof (Center), 0);

  Ddr4      = (Outputs->DdrType == MRC_DDR_TYPE_DDR4);
  Ddr4VrefCAPerDimm = Ddr4 && Inputs->IsVrefCAPerDimm;

  if (PrintMsg) {
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Cmd Vref Training with LC = %d\n", LoopCount);
  }
  Status = DQTimeCentering1D (
            MrcData,
            CmdV,
            0,          // ResetPerBit
            (UINT8) LoopCount,
            PrintMsg,
            FALSE,       // EarlyCentering
            1
            );

  // Find center value and program it
  for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
    for (Channel = 0; Channel < MaxChannels; Channel++) {
      ChannelOut = &Outputs->Controller[Controller].Channel[Channel];
      for (Rank = 0; Rank < MAX_RANK_IN_CHANNEL; Rank++) {
        if (MrcRankExist(MrcData, Controller, Channel, Rank)) {
          FirstRank = Ddr4VrefCAPerDimm ? Rank : 0;
          for (Byte = 0; Byte < Outputs->SdramCount; Byte++) {
            Margin = &Outputs->MarginResult[LastCmdV][Rank][Controller][Channel][Byte][0];
            Low[Controller][Channel][FirstRank] = MIN(Low[Controller][Channel][FirstRank], Margin[0]);
            High[Controller][Channel][FirstRank] = MIN(High[Controller][Channel][FirstRank], Margin[1]);
            if (GetBerMargin) {
              break;  // GetBerMarginCh() stores results in rank 0 byte 0
            }
          } // for Byte
        } // MrcRankExist
        if (GetBerMargin) {
          break;  // GetBerMarginCh() stores results in rank 0 byte 0
        }
      } // for Rank
      if (Ddr4VrefCAPerDimm) {
        for (FirstRank = 0; FirstRank < MAX_RANK_IN_CHANNEL; FirstRank += MAX_RANK_IN_DIMM) { // Loop over first rank in each DIMM
          if (!MrcRankExist(MrcData, Controller, Channel, FirstRank)) {
            continue;
          }
          DimmOut = &ChannelOut->Dimm[RANK_TO_DIMM_NUMBER (FirstRank)];
          if (DimmOut->RankInDimm == MAX_RANK_IN_DIMM) {
            // DDR4 CmdV is per DIMM, store per-DIMM margins in Rank0 and Rank2
            Rank = FirstRank + 1;
            Low[Controller][Channel][FirstRank]  = MIN (Low[Controller][Channel][Rank],  Low[Controller][Channel][FirstRank]);
            High[Controller][Channel][FirstRank] = MIN (High[Controller][Channel][Rank], High[Controller][Channel][FirstRank]);
          }
        } // Ddr4
      }
    } // Channel
  } // Controller

  // Calculate center value
  // Don't check for channel/rank presense to save code size
  for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
    for (Channel = 0; Channel < MAX_CHANNEL; Channel++) {
      for (Rank = 0; Rank < MAX_RANK_IN_CHANNEL; Rank++) {
        Center[Controller][Channel][Rank] = ((INT32)High[Controller][Channel][Rank] - Low[Controller][Channel][Rank]) / 20;
      }
    }
  }

  //  if (!Ddr5 || (Ddr5 && !Inputs->EnablePda)) {  // @todo complete DDR5 CmdV PDA programming
  // Print per-channel/DIMM/RANK results summary
  if (PrintMsg) {
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\nCmd Vref Summary:\n\tLow\tHigh\tHeight\tCenter\n");
    for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
      for (Channel = 0; Channel < MaxChannels; Channel++) {
        for (Rank = 0; Rank < MAX_RANK_IN_CHANNEL; Rank++) {
          if (!MrcRankExist(MrcData, Controller, Channel, Rank) ||
             (Ddr4VrefCAPerDimm && ((Rank % MAX_RANK_IN_DIMM) != 0))) { // Skip unpopulated Ranks, or odd ranks for DDR4 Vref CA per-DIMM
            continue;
          }
          Dimm = Rank / MAX_RANK_IN_DIMM;
          MRC_DEBUG_MSG(
              Debug,
              MSG_LEVEL_NOTE,
              "Mc%u.C%u.D%u:\t%u\t%u\t%u\t%d\n",
              Controller,
              Channel,
              Dimm,
              Low[Controller][Channel][Rank] / 10,
              High[Controller][Channel][Rank] / 10,
              (Low[Controller][Channel][Rank] + High[Controller][Channel][Rank]) / 10,
              Center[Controller][Channel][Rank]
              );
            if (!Ddr4 || (Ddr4 && !Inputs->IsVrefCAPerDimm)) {
              // CMD Vref is per-Channel, break out of for-Rank loop
              break;
            }
        } // for Rank
      } // for Channel
    } // Controller
  }

  for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
    for (Channel = 0; Channel < MaxChannels; Channel++) {
      ChannelOut = &Outputs->Controller[Controller].Channel[Channel];
      for (Rank = 0; Rank < MAX_RANK_IN_CHANNEL; Rank++) {
        if (!MrcRankExist(MrcData, Controller, Channel, Rank) ||
           (Ddr4VrefCAPerDimm && ((Rank % MAX_RANK_IN_DIMM) != 0))) {
          // Skip unpopulated Ranks, or odd ranks for DDR4 Vref CA per-DIMM
          continue;
        }

        RankMask = Ddr4VrefCAPerDimm ? (1 << Rank) : ChannelOut->ValidRankBitMask;

        /*if (Ddr5 && Inputs->EnablePda) { // @todo complete DDR5 CmdV PDA programming
          ChannelOut->Mr11PdaEnabled = TRUE;
          // Program CmdV Per Rank and Byte
          for (Rank = 0; Rank < MAX_RANK_IN_CHANNEL; Rank++) {
            if (!MrcRankExist (MrcData, Controller, Channel, Rank)) {
              continue;
            }
            for (Byte = 0; Byte < Outputs->SdramCount; Byte++) {
              Margin = &Outputs->MarginResult[LastCmdV][Rank][Controller][Channel][Byte][0];
              ByteCenter = (Margin[1] - Margin[0]) / 2;
              ChangeMargin (MrcData, CmdV, ByteCenter / 10, 0, 0, Controller, Channel, 1 << Rank, 1 << Byte, 0, 1, 0);
            }
          }
        }*/
        FirstRank = (Ddr4VrefCAPerDimm)? Rank: 0;
        ChangeMargin(MrcData, CmdV, Center[Controller][Channel][FirstRank], 0, 0, Controller, Channel, RankMask, 0, 0, 1, 0);
        if (!(Ddr4VrefCAPerDimm)) {
          //Update once for Per-Channel case
          break;
        }
      }
    } // Channel
  } // Controller

  MrcResetSequence (MrcData);

  // Check UPM limits
  MinHeight = 120;
  if (Inputs->MemoryProfile != STD_PROFILE) {
    MinHeight = 50;
  }
  for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
    for (Channel = 0; Channel < MaxChannels; Channel++) {
      for (Rank = 0; Rank < MAX_RANK_IN_CHANNEL; Rank++) {
        if (!MrcRankExist(MrcData, Controller, Channel, Rank) ||
          (Ddr4VrefCAPerDimm && ((Rank % MAX_RANK_IN_DIMM) != 0))) {
          // Skip unpopulated Ranks, or odd ranks for DDR4 Vref CA per-DIMM
          continue;
        }

        FirstRank = Ddr4VrefCAPerDimm ? Rank & 2 : 0; // Use the First Rank within each DIMM for Per-DIMM VrefCA, or Rank0 otherwise
        Height = Low[Controller][Channel][FirstRank] + High[Controller][Channel][FirstRank];
        if (Height < MinHeight) {
          Dimm = Rank / MAX_RANK_IN_DIMM;
          MRC_DEBUG_MSG(
             Debug,
             MSG_LEVEL_ERROR,
             "\nERROR! MC%u.Ch%u.D%u - Height region (%d) less than expected value (%d)\n",
             Controller,
             Channel,
             Dimm,
             Height / 10,
             MinHeight / 10
             );
          if (Inputs->ExitOnFailure) {
            return mrcMiscTrainingError;
          }
        }
        if (!(Ddr4VrefCAPerDimm)) {
          //Update once for Per-Channel case
          break;
        }
      } // for Rank
    } // for Channel
  } // Controller


  // Update MrcData for future tests
  for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
    for (Channel = 0; Channel < MaxChannels; Channel++) {
      for (Rank = 0; Rank < MAX_RANK_IN_CHANNEL; Rank++) {
        if (!MrcRankExist (MrcData, Controller, Channel, Rank)) {
          continue;  // No such rank
        }
        FirstRank = Ddr4VrefCAPerDimm ? Rank & 2 : 0; // Use the First Rank within each DIMM for Per-DIMM VrefCA, or Rank0 otherwise
        Outputs->MarginResult[LastCmdV][Rank][Controller][Channel][0][0] = Low[Controller][Channel][FirstRank] + Center[Controller][Channel][FirstRank] * 10;
        Outputs->MarginResult[LastCmdV][Rank][Controller][Channel][0][1] = High[Controller][Channel][FirstRank] - Center[Controller][Channel][FirstRank] * 10;
      } // Rank
    } // Channel
  } // Controller

  // Disable CADB Deselects
  CadbConfig.Data = 0;
  Cadb20ConfigRegWrite (MrcData, CadbConfig);

  return Status;
}

/**
  Perform Command Voltage Centering.

  @param[in, out] MrcData - Include all MRC global data.

  @retval MrcStatus -  if it succeeded returns mrcSuccess
**/
MrcStatus
MrcCmdVoltageCentering (
  IN OUT MrcParameters *const MrcData
  )
{
  MrcOutput             *Outputs;
  BOOLEAN               Ddr5;
  MrcDdrType            DdrType;
  UINT8                 LoopCount;
  BOOLEAN               DtHalo;
  const MrcInput        *Inputs;

  LoopCount = 15;
  Inputs = &MrcData->Inputs;
  Outputs = &MrcData->Outputs;
  DdrType = Outputs->DdrType;
  Ddr5 = (DdrType == MRC_DDR_TYPE_DDR5);
  DtHalo = Inputs->DtHalo;

  if ((Ddr5) && (DtHalo)) {
    LoopCount = 10;
  }
  return CmdVoltageCentering (MrcData, LoopCount, 4, MRC_PRINTS_ON);
}

/**
  Perform Early Command Voltage Centering for DDR5 2DPC, using lower loopcount

  @param[in, out] MrcData - Include all MRC global data.

  @retval MrcStatus -  if it succeeded returns mrcSuccess
**/
MrcStatus
MrcEarlyCmdVoltageCentering (
  IN OUT MrcParameters *const MrcData
  )
{
  return CmdVoltageCentering (MrcData, 6, 4, MRC_PRINTS_ON);
}

/**
  Centers Command Timing around a MidPoint

  @param[in] MrcData         - Include all MRC global data.
  @param[in] Iteration       - Determines which PI to shift
  @param[in] Ranks           - Valid Rank bit mask
  @param[in] GroupMask       - which LPDDR groups to work on for CMD/CLK/CKE; not used for DDR3
  @param[in] MidPoint        - The MidPoint to center around (per channel)

  @retval Nothing
**/
MrcStatus
CmdTimingCentering (
  IN MrcParameters *const MrcData,
  IN UINT8                Iteration,
  IN UINT8                Ranks,
  IN UINT8                GroupMask,
  IN UINT16               MidPoint[MAX_CONTROLLER][MAX_CHANNEL]
  )
{
  const MrcInput     *Inputs;
  MrcDebug           *Debug;
  const MRC_FUNCTION *MrcCall;
  MrcOutput          *Outputs;
  MrcDdrType         DdrType;
  MrcStatus          Status;
  INT64              Min;
  INT64              Max;
  UINT16             Ledge[MAX_CONTROLLER][MAX_CHANNEL];
  UINT16             Redge[MAX_CONTROLLER][MAX_CHANNEL];
  UINT16             Mid[MAX_CONTROLLER][MAX_CHANNEL];
  UINT16             Low[MAX_CONTROLLER][MAX_CHANNEL];
  UINT16             High[MAX_CONTROLLER][MAX_CHANNEL];
  UINT16             PiLow;
  UINT16             PiHigh;
  static const INT8  VrefOffsets[2] = {-8, 8};
  UINT16             Center;
  UINT16             Range;
  UINT32             NMode;
  UINT8              McChBitMask;
  UINT8              RankMask;
  UINT8              Controller;
  UINT8              Channel;
  UINT8              MinWidth = 18;
  UINT16             lWidth;
  BOOLEAN            SkipVref;
  UINT16             EyeWidth;

  Inputs         = &MrcData->Inputs;
  MrcCall        = Inputs->Call.Func;
  Outputs        = &MrcData->Outputs;
  Debug          = &Outputs->Debug;
  DdrType        = Outputs->DdrType;
  Status         = mrcSuccess;
  MrcCall->MrcSetMem ((UINT8 *) Ledge, sizeof (Ledge), 0);
  MrcCall->MrcSetMem ((UINT8 *) Redge, sizeof (Redge), 0);

  // get the sweep limits
  MrcGetSetLimits (MrcData, CmdGrpPi, &Min, &Max, NULL);

  // Find the common CMD/CTL PI range for the sweep, using +/- 64 ticks from the MidPoint of all channels.
  // Sweep +/- 128 ticks in 2N mode (DDR4/5 command)
  PiLow  = MRC_UINT16_MAX;
  PiHigh = 0;
  NMode = MrcGetNMode (MrcData);
  Range = 64;
  if (DdrType == MRC_DDR_TYPE_LPDDR5) {
    Range = 256;      // On LP5 CMD bus 4 times slower, sweep wider range to find edges
  }
  if (Iteration == MrcIterationCmd) {
    if (DdrType == MRC_DDR_TYPE_DDR4) {
      if (Outputs->Gear2) {  // In Gear2 we have 2N on the bus, while NMode is 1
        Range = 128;
      } else {
        Range = (NMode == 2) ? 128 : 64;
      }
    } else if (DdrType == MRC_DDR_TYPE_DDR5) {
      Range = (NMode == 2) ? 128 : 64;
    }
  }

  for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
    for (Channel = 0; Channel < MAX_CHANNEL; Channel++) {
      if (MrcChannelExist (MrcData, Controller, Channel)) {
        MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\tMc%uCh%u",Controller, Channel);
        Center = MidPoint[Controller][Channel];
        if ((Center - Range) > 0) {
          PiLow  = MIN (PiLow,  Center - Range);
        } else {
          PiLow = 0;
        }
        PiHigh = MAX (PiHigh, Center + Range);
      }
    }
  }
  // Ensure we sweep the full range if PiLow is clamped at zero
  if ((PiLow == 0) && (PiHigh < (2 * Range - 1))) {
    PiHigh = (2 * Range - 1);
  }
  PiHigh = MIN (PiHigh, (UINT16) Max);
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\n[%u .. %u]\n", PiLow, PiHigh);

  if (NULL == MidPoint) {
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Midpoint[] parameter is NULL!\n");
    return mrcWrongInputParameter;
  }

  // Setup CPGC Test to iteration through appropriate ranks during test
  McChBitMask = 0;
  for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
    if (MrcControllerExist (MrcData, Controller)) {
      for (Channel = 0; Channel < Outputs->MaxChannels; Channel++) {
        if (MrcChannelExist (MrcData, Controller, Channel)) {
          McChBitMask |= SelectReutRanks (MrcData, Controller, Channel, Ranks, FALSE, 0);
          Mid[Controller][Channel] = MidPoint[Controller][Channel];
        }
      } // channel
    }
  } // Controller

  // Binary search will use the full PI range of [0..255(G1) or 511(G2)]
  MrcCall->MrcSetMemWord ((UINT16 *)Low, sizeof (Low) / sizeof (UINT16), PiLow);
  MrcCall->MrcSetMemWord ((UINT16 *)High, sizeof (High) / sizeof (UINT16), PiHigh);

  if (Iteration == MrcIterationClock) {
    // Use a linear search to center clock and Update Clock Delay/Host
    // Allow wrap around since this is clock
    // CmdLinearFindEdges also programs the new values
    SkipVref = FALSE;
    Status = CmdLinearFindEdges (MrcData, Iteration, McChBitMask, Ranks, GroupMask, (INT16) PiLow, (UINT16) PiHigh, 6, 1, (INT8 *) VrefOffsets, FALSE, SkipVref);
  } else {
    CmdBinaryFindEdge (MrcData, Iteration, McChBitMask, Ranks, GroupMask, Low, Mid, 0, (INT8 *) VrefOffsets);
    for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
      if (MrcControllerExist (MrcData, Controller)) {
        for (Channel = 0; Channel < Outputs->MaxChannels; Channel++) {
          Ledge[Controller][Channel] = Mid[Controller][Channel]; // CountUp is 0 so return High.
          Mid[Controller][Channel]   = MidPoint[Controller][Channel]; //Mid Modified by CmdBinaryFindEdge
        }
      }
    }
    CmdBinaryFindEdge (MrcData, Iteration, McChBitMask, Ranks, GroupMask, Mid, High, 1, (INT8 *) VrefOffsets);
    for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
      if (MrcControllerExist (MrcData, Controller)) {
        for (Channel = 0; Channel < Outputs->MaxChannels; Channel++) {
          Redge[Controller][Channel] = Mid[Controller][Channel];  // CountUp is 1 so return Low.
        }
      }
    } // Controller
    // Update Variables:
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\nMc\tCh\tLeft\tRight\tWidth\tCenter\n");

    for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
      for (Channel = 0; Channel < Outputs->MaxChannels; Channel++) {
        if ((MC_CH_MASK_CHECK (McChBitMask, Controller, Channel, Outputs->MaxChannels)) == 0) {
          continue;
        }
        RankMask  = Ranks & Outputs->Controller[Controller].Channel[Channel].ValidRankBitMask;
        lWidth    = Redge[Controller][Channel] - Ledge[Controller][Channel];

        // Apply the 2N centering for DDR4/5
        EyeWidth = Inputs->LctCmdEyeWidth;
        if ((!Outputs->Lpddr) && (Iteration == MrcIterationCmd) && (lWidth > EyeWidth) && (EyeWidth != 0)) {
          Redge[Controller][Channel] = Ledge[Controller][Channel] + EyeWidth;
          lWidth = Redge[Controller][Channel] - Ledge[Controller][Channel];
        }
        Center = (Ledge[Controller][Channel] + Redge[Controller][Channel] + 1) / 2;
        if (lWidth < MinWidth) {
          MRC_DEBUG_MSG (Debug, MSG_LEVEL_WARNING, "Eye < %u for Mc%u C%u!\n", MinWidth, Controller, Channel);
          Status = mrcMiscTrainingError;
        }

        ShiftPIforCmdTraining (MrcData, Controller, Channel, Iteration, RankMask, GroupMask, Center, 1);

        MRC_DEBUG_MSG (
          Debug,
          MSG_LEVEL_NOTE,
          " %d\t%d\t%d\t%d\t%d\t%d\n",
          Controller,
          Channel,
          Ledge[Controller][Channel],
          Redge[Controller][Channel],
          lWidth,
          Center
          );
      } // for Channel
    } // for Controller
  }

  IoReset (MrcData);

  return Status;
}

#if 0
/**
  Plot CmdT (linear CLK shift) vs CmdV, used for debug of DDR3/4.

  @param[in] MrcData         - Include all MRC global data.

  @retval MrcStatus
**/
MrcStatus
CommandPlot2D (
  IN     MrcParameters *const MrcData
  )
{
  const MrcInput       *Inputs;
  MrcDebug             *Debug;
  const MRC_FUNCTION   *MrcCall;
  MrcOutput            *Outputs;
  MrcChannelOut        *ChannelOut;
  MrcControllerOut     *ControllerOut;
  MrcStatus            Status;
  BOOLEAN              Pass;
  UINT8                Channel;
  UINT8                ChannelMask;
  UINT8                RankMask;
  UINT8                Rank;
  INT16                LCTDelay;
  UINT8                LCTStep = 3;
  UINT8                ChError;
  INT8                 Vref;
  INT8                 VrefLow  = -63;
  INT8                 VrefHigh = 63;
  INT8                 VrefStep = 2;
  UINT8                CmdLoopCount;
  INT8                 Low = -64;
  UINT8                High = 64;
  UINT8                chBitMask;
  UINT8                chBitMaskToRun;
  UINT8                ChToPlot;
  UINT8                GroupMask;
  BOOLEAN              Ddr4;
  MC0_CH0_CR_CADB_CFG_STRUCT  CadbConfig;

  Inputs        = &MrcData->Inputs;
  MrcCall       = Inputs->Call.Func;
  Outputs       = &MrcData->Outputs;
  Debug         = &Outputs->Debug;
  ControllerOut = &Outputs->Controller[0];
  Ddr4          = (Outputs->DdrType == MRC_DDR_TYPE_DDR4);
  GroupMask = 3;
  chBitMaskToRun = 0x3;
  CmdLoopCount = 10;

  for (Rank = 0; Rank < MAX_RANK_IN_CHANNEL; Rank++) {
    RankMask  = 1 << Rank;
    chBitMask = 0;
    for (Channel = 0; Channel < MAX_CHANNEL; Channel++) {
      chBitMask |= SelectReutRanks (MrcData, Channel, RankMask, FALSE, 0);
    }
    if (chBitMask == 0) { // Continue with next rank if this rank is not present on any channel
      continue;
    }
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\nRank %u\n", Rank);

    // Use CADB test for Cmd to match Late Command Training
    SetupIOTestCADB (MrcData, Outputs->McChBitMask, CmdLoopCount, NSOE, 1, 0);

    for (ChToPlot = 0; ChToPlot < MAX_CHANNEL; ChToPlot++) {
      if (((1 << ChToPlot) & chBitMask) == 0) {
        continue;
      }
      ChannelOut = &ControllerOut->Channel[Channel];
      ChannelMask = 1 << ChToPlot;
      MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\nChannel: %u\n", ChToPlot);
      MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\nVref");

      for (Vref = VrefHigh; Vref >= VrefLow; Vref -= VrefStep) {
        MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\n%d\t", Vref);

        for (LCTDelay = Low; LCTDelay <= High; LCTDelay += LCTStep) {
          for (Channel = 0; Channel < MAX_CHANNEL; Channel++) {
            if ((1 << Channel) & chBitMask) {
              //@todo - 2MC
              ShiftPIforCmdTraining (MrcData, CONTROLLER_0, Channel, MrcIterationClock, RankMask, GroupMask, LCTDelay, 0);
            }
          }
          Status = MrcResetSequence (MrcData);  // Reset DDR
          ChangeMargin (MrcData, CmdV, Vref, 0, 0, /**Controller**/ 0, ChToPlot, ChannelOut->ValidRankBitMask, 0, 0, 0, 0);
          ChError = RunIOTest (MrcData, /**McBitMask**/ 1, chBitMaskToRun, Outputs->DQPat, 1, 0);

//          FailCount = 0;
//          for (Test = 1; Test <= 3; Test++) {
//            ChError = RunIOTest (MrcData, /**McBitMask**/ 1, ChannelMask, Outputs->DQPat, 1, 0);
          Pass = ((ChError & ChannelMask) == 0);
          MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, Pass ? "." : "#");
//            if (!Pass) MRC_DEADLOOP();
//            FailCount = Pass ? FailCount : FailCount + 1;
//          }
//          MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, (FailCount == 0) ? "." : "%d", FailCount);
        } // for LCTDelay
      } // for Vref
    } // for ChToPlot

    // Restore centered value
    for (Channel = 0; Channel < MAX_CHANNEL; Channel++) {
      ChannelOut = &ControllerOut->Channel[Channel];
      ChangeMargin (MrcData, CmdV, 0, 0, 0, /**Controller**/ 0, Channel, ChannelOut->ValidRankBitMask, 0, 0, 0, 0);
      if (MrcRankExist (MrcData, CONTROLLER_0, Channel, Rank)) {
        //@todo - 2MC
        ShiftPIforCmdTraining (MrcData, CONTROLLER_0, Channel, MrcIterationClock, RankMask, 3, 0, 0);
      }
    }
  } // for Rank
  // Disable CADB deselects
  CadbConfig.Data = 0;
  Cadb20ConfigRegWrite (MrcData, CadbConfig);

  Status = MrcResetSequence (MrcData);
  return Status;
}

#endif // if 0

/**
  Use a linear search to find the edges between Low and High
  if WrapAround = 0: Look for largest passing region between low and high
  if WrapAround = 1: Look for largest passing region, including wrapping from high to low

  @param[in, out] MrcData     - Include all MRC global data.
  @param[in]      Iteration   - Determines which PI to shift
  @param[in]      chBitMask   - Valid Channel bit mask
  @param[in]      Ranks       - Valid Rank bit mask
  @param[in]      GroupMask   - which LPDDR groups to work on for CMD/CLK/CKE; not used for DDR3
  @param[in]      Low         - Low limit
  @param[in]      High        - High limit
  @param[in]      Step        - Step size
  @param[in]      WrapAllowed - Determines the search region
  @param[in]      VrefOffsets - Array of Vref offsets
  @param[in]      SkipPrint   - Switch to enable or disable debug printing
  @param[in]      SkipVref    - Skip changing CMD Vref offsets, only run test once at the current Vref.

  @retval MrcStatus -  If it succeeds return mrcSuccess
**/
MrcStatus
CmdLinearFindEdges (
  IN OUT MrcParameters *const MrcData,
  IN     UINT8                Iteration,
  IN     UINT8                McChBitMask,
  IN     UINT8                Ranks,
  IN     UINT8                GroupMask,
  IN     INT16                Low,
  IN     UINT16               High,
  IN     UINT8                Step,
  IN     UINT8                WrapAllowed,
  IN     INT8                 *VrefOffsets,
  IN     BOOLEAN              SkipPrint,
  IN     BOOLEAN              SkipVref
  )
{
  const MrcInput    *Inputs;
  MrcDebug          *Debug;
  MrcOutput         *Outputs;
  MrcDebugMsgLevel   DebugLevel;
  INT32             *IPStart;
  INT32             *IPEnd;
  INT32             *CPStart;
  INT32             *CPEnd;
  INT32             *LPStart;
  INT32             *LPEnd;
  MrcStatus         Status;
  BOOLEAN           Pass;
  BOOLEAN           ExitOnFailure;
  INT32             InitialPassingStart[MAX_CONTROLLER][MAX_CHANNEL];
  INT32             InitialPassingEnd[MAX_CONTROLLER][MAX_CHANNEL];
  INT32             CurrentPassingStart[MAX_CONTROLLER][MAX_CHANNEL];
  INT32             CurrentPassingEnd[MAX_CONTROLLER][MAX_CHANNEL];
  INT32             LargestPassingStart[MAX_CONTROLLER][MAX_CHANNEL];
  INT32             LargestPassingEnd[MAX_CONTROLLER][MAX_CHANNEL];
  INT32             lWidth;
  INT32             iWidth;
  INT32             cWidth;
  INT32             Center;
  INT16             LCTDelay;
  UINT8             Controller;
  UINT8             Channel;
  UINT8             CurMcChMask;
  UINT8             RankMask;
  UINT8             Rank;
  UINT16            LCTStep;
  UINT16            LastStep;
  UINT8             Vloop;
  UINT8             VoltageLoopCount;
  UINT8             McChError;
  INT8              Vref;
  Inputs        = &MrcData->Inputs;
  Outputs       = &MrcData->Outputs;
  Debug         = &Outputs->Debug;
  lWidth        = 0;
  iWidth        = 0;
  cWidth        = 0;
  DebugLevel = SkipPrint ? MSG_LEVEL_NEVER : MSG_LEVEL_NOTE;
  ExitOnFailure = FALSE;

  LCTStep = Step;
  VoltageLoopCount  = 2;

  MRC_DEBUG_MSG (Debug, DebugLevel, "\nCLkDlay\t");

  for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
    for (Channel = 0; Channel < Outputs->MaxChannels; Channel++) {
      if (MrcChannelExist (MrcData, Controller, Channel)) {
        MRC_DEBUG_MSG (Debug, DebugLevel, "Mc%dCh%d\t", Controller, Channel);
      }
    } // channel
  } // Controller

  for (LCTDelay = Low; LCTDelay <= High; LCTDelay += LCTStep) {
    // Update Timing
    for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
      for (Channel = 0; Channel < Outputs->MaxChannels; Channel++) {
        if (MC_CH_MASK_CHECK (McChBitMask, Controller, Channel, Outputs->MaxChannels)) {
          RankMask = Ranks & Outputs->Controller[Controller].Channel[Channel].ValidRankBitMask;
          ShiftPIforCmdTraining(MrcData, Controller, Channel, Iteration, RankMask, GroupMask, LCTDelay, 0);
        }
      } // channel
    } // Controller

    // Reset DDR
    Status = MrcResetSequence (MrcData);

    // Run CPGC until both channels fail or we finish all Vref points
    if (SkipVref) {
      McChError = RunIOTest (MrcData, McChBitMask, Outputs->DQPat, 1, 0);
    } else {
      McChError = 0;
      for (Vloop = 0; Vloop < VoltageLoopCount; Vloop++) {
        Vref = VrefOffsets[Vloop];
        ChangeMargin (MrcData, CmdV, Vref, 0, 1, 0, 0, 0, 0, 0, 0, 0);

        McChError |= RunIOTest (MrcData, McChBitMask, Outputs->DQPat, 1, 0);

        if (McChError == McChBitMask) {
          break;
        }
      } // Vloop
    }

    MRC_DEBUG_MSG (Debug, DebugLevel, "\n %d\t", LCTDelay);

    // Update Passing Variables
    for (Controller = 0; Controller < MAX_CONTROLLER; Controller++){
      if (MrcControllerExist (MrcData, Controller)) {
        for (Channel = 0; Channel < Outputs->MaxChannels; Channel++) {
          CurMcChMask = (MRC_BIT0 << ((Controller * Outputs->MaxChannels) + Channel));
          if (!(CurMcChMask & McChBitMask)) {
            MRC_DEBUG_MSG (Debug, DebugLevel, (Channel == 0) ? "  " : "");
            continue;
          }

          Pass = !(McChError & CurMcChMask);

          MRC_DEBUG_MSG (Debug, DebugLevel, Pass ? "  .\t" : "  #\t");

          IPStart = &InitialPassingStart[Controller][Channel];
          IPEnd   = &InitialPassingEnd[Controller][Channel];
          CPStart = &CurrentPassingStart[Controller][Channel];
          CPEnd   = &CurrentPassingEnd[Controller][Channel];
          LPStart = &LargestPassingStart[Controller][Channel];
          LPEnd   = &LargestPassingEnd[Controller][Channel];

          if (LCTDelay == (INT16) Low) {
            if (Pass) {
              *IPStart = *IPEnd = *CPStart = *CPEnd = *LPStart = *LPEnd = Low;
            } else {
              *IPStart = *IPEnd = *CPStart = *CPEnd = *LPStart = *LPEnd = Low - LCTStep;
            }
          } else {
            if (Pass) {
              // Update Initial variables
              if (*IPEnd == (LCTDelay - LCTStep)) {
                *IPEnd = LCTDelay; // In passing region
              }

              // Update Current variables
              if (*CPEnd == (LCTDelay - LCTStep)) {
                *CPEnd = LCTDelay; // In passing region
              } else {
                *CPStart = *CPEnd = LCTDelay;
              }

              // Special case for last step: Append Initial Passing Region
              // LCTDelay should be considered a continuous range that wraps around 0
              LastStep = High - LCTStep;
              if ((LCTDelay >= LastStep) && (*IPStart == Low) && WrapAllowed) {
                iWidth = *IPEnd -*IPStart;
                *CPEnd += (LCTStep + iWidth);
              }
              // Update Largest variables
              cWidth = *CPEnd - *CPStart;
              lWidth = *LPEnd - *LPStart;
              if (cWidth > lWidth) {
                *LPStart = *CPStart;
                *LPEnd   = *CPEnd;
              }
            }
          }
        } // for Channel
      }
    } // for Controller
  } // for LCTDelay

  MRC_DEBUG_MSG (Debug, DebugLevel, "\nMC\tCH\tLeft\tRight\tWidth\tCenter\n");

  for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
    for (Channel = 0; Channel < Outputs->MaxChannels; Channel++) {
      if ((MRC_BIT0 << ((Controller * Outputs->MaxChannels) + Channel)) & McChBitMask) {
        LPStart = &LargestPassingStart[Controller][Channel];
        LPEnd   = &LargestPassingEnd[Controller][Channel];
        lWidth  = *LPEnd - *LPStart;
        Center = (*LPEnd + *LPStart) / 2;
        if ((lWidth < (3 * LCTStep)) || (lWidth >= (High - Low))) {
          MRC_DEBUG_MSG (Debug, DebugLevel, "\nERROR! Bad command eye width: %u\n", lWidth);
          if (lWidth == 0) {
            *LPStart = *LPEnd = 0;
          }
          if (Inputs->ExitOnFailure) {
            ExitOnFailure = TRUE;
          }
        }
        RankMask = Ranks & Outputs->Controller[Controller].Channel[Channel].ValidRankBitMask;
        if (!SkipPrint) {
          // Shift Timing
          ShiftPIforCmdTraining (MrcData, Controller, Channel, Iteration, RankMask, GroupMask, Center, 1);
          MRC_DEBUG_MSG (
            Debug,
            DebugLevel,
            " %d\t%d\t%d\t%d\t%d\t%d\n",
            Controller,
            Channel,
            *LPStart,
            *LPEnd,
            lWidth,
            Center
            );
        }

        // Determine in which rank to save the margins...
        for (Rank = 0; Rank < MAX_RANK_IN_CHANNEL; Rank++) {
          if ((1 << Rank) & RankMask) {
            Outputs->MarginResult[LastCmdT][Rank][Controller][Channel][0][0] = (UINT16) (10 * ABS (*LPStart));
            Outputs->MarginResult[LastCmdT][Rank][Controller][Channel][0][1] = (UINT16) (10 * ABS (*LPEnd));
          }
        } // Rank loop
      }
    } // Channel loop
  } // Controller loop

  if (ExitOnFailure) {
    return mrcMiscTrainingError;
  }

  // Clean Up
  if (!SkipVref) {
    ChangeMargin (MrcData, CmdV, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0);
  }

  Status = MrcResetSequence (MrcData);
  return Status;
}

/**
  Use a binary search to find the edge between Low and High
  High and Low track passing points
  if CountUp: Low is a passing point and need to count up to find a failing point
  if CountDn: High is a passing point and need to count dn to find a failing point

  @param[in]      MrcData     - Include all MRC global data.
  @param[in]      Iteration   - Determines which PI to shift
  @param[in]      ChBitMask   - Valid Channel bit mask
  @param[in]      Ranks       - Valid Rank bit mask
  @param[in]      GroupMask   - which LPDDR groups to work on for CMD/CLK/CKE; not used for DDR3
  @param[in, out] Low         - Low limit
  @param[in, out] High        - High limit
  @param[in]      CountUp     - The direction to search
  @param[in]      VrefOffsets - Array of Vref offsets

  @retval Nothing
**/
void
CmdBinaryFindEdge (
  IN     MrcParameters *const MrcData,
  IN     UINT8                Iteration,
  IN     UINT8                McChBitMask,
  IN     UINT8                Ranks,
  IN     UINT8                GroupMask,
  IN OUT UINT16               (*Low)[MAX_CHANNEL],
  IN OUT UINT16               (*High)[MAX_CHANNEL],
  IN     UINT8                CountUp,
  IN     INT8                 *VrefOffsets
  )
{
  const MrcInput    *Inputs;
  MrcDebug          *Debug;
  const MRC_FUNCTION *MrcCall;
  MrcOutput         *Outputs;
  UINT16            Target[MAX_CONTROLLER][MAX_CHANNEL];
  UINT8             Done;
  UINT8             McChError;
  INT8              Vref;
  UINT8             Controller;
  UINT8             Channel;
  UINT8             MaxChannels;
  UINT8             CurMcChMask;
  UINT8             RankMask;
  UINT8             Group;
  UINT8             Fail;
  UINT8             Vloop;
  UINT8             VoltageLoopCount;
  BOOLEAN           SkipWait;

  Inputs        = &MrcData->Inputs;
  MrcCall       = Inputs->Call.Func;
  Outputs       = &MrcData->Outputs;
  Debug         = &Outputs->Debug;
  MaxChannels   = Outputs->MaxChannels;
  Done          = 0;
  McChError     = 0;
  MrcCall->MrcSetMem ((UINT8 *) Target, sizeof (Target), 0);

  SkipWait          = FALSE;
  VoltageLoopCount  = 2;

  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Pi Delay\n");
  while (!Done) {
    // Update Timing
    for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
      for (Channel = 0; Channel < MaxChannels; Channel++) {
        if (MC_CH_MASK_CHECK (McChBitMask, Controller, Channel, MaxChannels)) {
          Target[Controller][Channel] = (High[Controller][Channel] + Low[Controller][Channel] + CountUp) / 2;    // CountUp gets rounding correct
          RankMask = Ranks & Outputs->Controller[Controller].Channel[Channel].ValidRankBitMask;
          for (Group = 0; Group < MAX_COMMAND_GROUPS; Group++) {
            if ((1 << Group) & GroupMask) {
              ShiftPIforCmdTraining (MrcData, Controller, Channel, Iteration, RankMask, 1 << Group, Target[Controller][Channel], 0);
            }
          } // Group
        }
      } // Channel
    } // Controller

    if ((McChError & McChBitMask) != 0) {
      // Perform JEDEC Reset/Init if at least one of the channels failed on previous PI iteration
      // MRC_DEBUG_MSG (Debug, MSG_LEVEL_WARNING, "Failed, doing JEDEC Reset..\n");
      MrcResetSequence (MrcData);
    } else {
      IoReset (MrcData);
    }

    // Run CPGC until both channels fail or we finish all Vref points
    McChError = 0;
    for (Vloop = 0; Vloop < VoltageLoopCount; Vloop++) {
      Vref = VrefOffsets[Vloop];
      ChangeMargin (MrcData, CmdV, Vref, 0, 1, 0, 0, 0, 0, 0, 0, SkipWait);
      McChError |= RunIOTest (MrcData, McChBitMask, Outputs->DQPat, 1, 0);
      if (McChError == McChBitMask) {
        break;
      }
    }

    // Update High/Low
    for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
      for (Channel = 0; Channel < MaxChannels; Channel++) {
        CurMcChMask = (1 << ((Controller * MaxChannels) + Channel));
        if (CurMcChMask & McChBitMask) {
          Fail = (McChError & CurMcChMask);
          MRC_DEBUG_MSG(Debug, MSG_LEVEL_NOTE, "\t%3d %c", Target[Controller][Channel], Fail ? '#' : '.');
          // Skip if this channel is done
          if (High[Controller][Channel] > Low[Controller][Channel]) {
            if (CountUp) {
              if (Fail) {
                High[Controller][Channel] = Target[Controller][Channel] - 1;
              } else {
                Low[Controller][Channel] = Target[Controller][Channel];
              }
            } else {
              if (Fail) {
                Low[Controller][Channel] = Target[Controller][Channel] + 1;
              } else {
                High[Controller][Channel] = Target[Controller][Channel];
              }
            }
          }
        }
      } // Channel
    } // Controller

    // Update Done
    Done = 1;
    for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
      for (Channel = 0; Channel < MaxChannels; Channel++) {
        if (MC_CH_MASK_CHECK (McChBitMask, Controller, Channel, MaxChannels)) {
          if (High[Controller][Channel] > Low[Controller][Channel]) {
            Done = 0;
          }
        }
      }
    }

    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\n");
  }

  // Clean Up
  ChangeMargin (MrcData, CmdV, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0);
  MrcResetSequence (MrcData);
  return;
}

/**
  Shift the CLK/CMD/CTL Timing by the given PI setting value
  CLK shift will also change RcvEn / TxDq / TxDqs.
  @param[in] MrcData    - Include all MRC global data.
  @param[in] Controller - Controller to update.
  @param[in] Channel    - Channel to shift
  @param[in] Offset     - Offset to shift by

  @retval Nothing
**/
void
ShiftChannelTiming (
  IN MrcParameters *const MrcData,
  IN UINT32               Controller,
  IN UINT32               Channel,
  IN INT32                Offset
  )
{
  MrcDebug            *Debug;
  MrcOutput           *Outputs;
  MrcIntOutput        *IntOutputs;
  MrcIntCmdTimingOut  *IntCmdTiming;
  INT32               NewCode;
  UINT8               Rank;
  UINT8               RankBit;
  UINT8               Group;
  UINT8               MaxGroup;
  BOOLEAN             Ddr;

  Outputs       = &MrcData->Outputs;
  Debug         = &Outputs->Debug;
  IntOutputs    = (MrcIntOutput *) (MrcData->IntOutputs.Internal);
  IntCmdTiming  = &IntOutputs->Controller[Controller].CmdTiming[Channel];
  Ddr           = !MrcData->Outputs.Lpddr;

  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Mc%d Channel %d new values:\n", Controller, Channel);
  // Shift the CLK/CTL Timing
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\tCTL\tCLK");
  for (Rank = 0; Rank < MAX_RANK_IN_CHANNEL; Rank++) {
    if (MrcRankExist (MrcData, Controller, Channel, Rank)) {
      MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\n R%d", Rank);
      RankBit = 1 << Rank;

      NewCode = (INT32) (IntCmdTiming->CtlPiCode[Rank]) + Offset;
      ShiftPIforCmdTraining (MrcData, Controller, Channel, MrcIterationCtl, RankBit, MRC_IGNORE_ARG_8, NewCode, 1);
      MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\t%d", NewCode);

      if ((Rank == 0) || (Ddr)) {
        // CLK is per Rank in DDR, and only 1 Clock per Channel in LPDDR
        NewCode = (INT32) (IntCmdTiming->ClkPiCode[Rank]) + Offset;
        ShiftPIforCmdTraining (MrcData, Controller, Channel, MrcIterationClock, RankBit, MRC_IGNORE_ARG_8, Offset, 1);
        MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\t%d", NewCode);
      }
      MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\n");
    }
  }

  // Shift the CMD Timing
  MaxGroup = (Ddr) ? MAX_COMMAND_GROUPS : 1;
  for (Group = 0; Group < MaxGroup; Group++) {
    NewCode = (INT32) IntCmdTiming->CmdPiCode[Group] + Offset;
    ShiftPIforCmdTraining (MrcData, Controller, Channel, MrcIterationCmd, MRC_IGNORE_ARG_8, (1 << Group), NewCode, 1);
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, " New CMD[%d] value = %d\n", Group, NewCode);
  }

  return;
}

/**
  This function updates Command Mode register, tXP and Roundtrip latency.
  This should only be called for DDR3/4.

  @param[in, out] MrcData     - Include all MRC global data.
  @param[in]      Controller  - Controller to update.
  @param[in]      Channel     - Channel to update.
  @param[in]      OldN        - Old N Mode value.
  @param[in]      NewN        - New N mode value.

  @retval Nothing
**/
void
UpdateCmdNTiming (
  IN OUT MrcParameters *const MrcData,
  IN     UINT32               Controller,
  IN     UINT32               Channel,
  IN     UINT8                OldN,
  IN     UINT8                NewN
  )
{
  MrcOutput     *Outputs;
  MrcTiming     *Timing;
  MrcDdrType    DdrType;
  INT64         CmdStretch;
  INT64         tXP;
  INT64         Diff;
  BOOLEAN       Lpddr;
  INT64         GetSetVal;

  Outputs = &MrcData->Outputs;
  Timing  = &Outputs->Controller[Controller].Channel[Channel].Timing[MrcData->Inputs.MemoryProfile];
  DdrType = Outputs->DdrType;
  Lpddr   = Outputs->Lpddr;

  // If LPDDR, only update MC parameters on Even Channels.
  if ((Lpddr && ((Channel % 2) == 0)) || !Lpddr) {
    // Update Cmd NMode, till now we ran DDR4 at 3N (Gear1) or 2N (Gear2)
    // Command stretch:
    //   00 - 1N
    //   01 - 2N
    //   10 - 3N
    //   11 - N:1
    if (Timing->NMode == 1) {
      CmdStretch = Outputs->Gear2 ? 0 : 3;  // Use N:1 Mode in Gear1 and 1N mode in Gear2
    } else {
      CmdStretch = Timing->NMode - 1;
    }
    MrcGetSetMcCh (MrcData, Controller, Channel, GsmMccCmdStretch,  WriteCached | PrintValue, &CmdStretch);
    if (CmdStretch == 3) {
      // Set bus_retain_on_n_to_1_bubble when N:1 is used.
      GetSetVal = 1;
      MrcGetSetMcCh (MrcData, Controller, Channel, GsmMccBusRetainOnBubble, WriteNoCache | PrintValue, &GetSetVal);
    }

    // Adjust tXP value - it depends on NMode
    // No frequency switching in DDR4 so we use Outputs->Frequency always.
    tXP     = tXPValue (DdrType, Timing->tCK);
    tXP     = RANGE (tXP, tXP_MIN, tXP_MAX);
    MrcGetSetMcCh (MrcData, Controller, Channel, GsmMctXP, WriteCached | GSM_PRINT_VAL, &tXP);
  }

  // Adjust Roundtrip Latency and RxFIFO - it depends on NMode
  Diff = (NewN - OldN);
  // Pass system level channel to this parameter as it is treated like DDRIO training parameter.
  MrcGetSetMcChRnk (MrcData, Controller, Channel, MAX_RANK_IN_CHANNEL, RoundTripDelay, WriteOffsetCached | PrintValue, &Diff);
}

/**
  Force CKE values per rank.

  @param[in] MrcData      - MRC global data.
  @param[in] McChBitMask  - Bitmask of Controllers and Channels to work on.
  @param[in] CkeValue     - Bitmask of CKE values per rank. Bit0 = CKE0, Bit1 = CKE1 etc.

**/
VOID
MrcForceCkeValue (
  IN MrcParameters *const MrcData,
  IN UINT32               McChBitMask,
  IN INT64                CkeValue
  )
{
  MrcOutput           *Outputs;
  INT64   ValidCkeMask;
  UINT32  Controller;
  UINT32  Channel;
  UINT8   MaxChannel;
  BOOLEAN Lpddr;

  Outputs = &MrcData->Outputs;
  MaxChannel = Outputs->MaxChannels;
  Lpddr = Outputs->Lpddr;

  for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
    for (Channel = 0; Channel < MaxChannel; Channel++) {
      if (((1 << ((Controller * MaxChannel) + Channel)) & McChBitMask) && !(IS_MC_SUB_CH (Lpddr, Channel))) {
        ValidCkeMask = MrcData->Outputs.Controller[Controller].Channel[Channel].ValidRankBitMask;
        ValidCkeMask |= (MrcData->Outputs.Controller[Controller].Channel[Channel+1].ValidRankBitMask) << 2;
        MrcGetSetMcCh (MrcData, Controller, Channel, GsmMccCkeOverride, WriteNoCache, &ValidCkeMask);
        MrcGetSetMcCh (MrcData, Controller, Channel, GsmMccCkeOn, WriteNoCache, &CkeValue);
      }
    }
  }
}

//
//  Sets up 4 CADB lines that will be used to send out a CS / CA pattern for LPDDR4.
//
//  -----------------------
//  CADB  Phase  Phase  CS
//  Line  High   Low
//        (Sch1) (Sch0)
//  -----------------------
//   0    0x00   0x00  Off
//   1    0x3F   0x3F  Off
//   2    0x2A   0x2A  On
//   3    0x15   0x15  On
//
//  The CS pattern uses Pattern Buffer and has 32 lines on ICL (CADB 2.1), with CS active for one line only.
//  This will send a command every 32 DCLKs.
//
//  Pattern Buffer details:
//  The line order is:    0, 0, 0, 0, 2, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
//  or different command: 0, 0, 0, 0, 3, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
//
//  000
//  000
//  000
//  000
//
//  010 or 011
//  001
//  001
//  001

//  001
//  000
//  000
//  000
//
//  000
//  000
//  000
//  000
//
//  000
//  000
//  000
//  000
//
//  000
//  000
//  000
//  000
//
//  000
//  000
//  000
//  000
//
//  000
//  000
//  000
//  000

//  ----
//  000        --> PB[0] = 0x000001E0 or 0x000001F0
//  01E or 01F     PB[1] = 0x00000010
//  001            PB[2] = 0x00000000
//  000
//  000
//  000
//  000
//  000
//


CADB_LINE CadbLinesLp4[] = {
  { 0x00, 0x00, 0 },
  { 0x3F, 0x3F, 0 },
  { 0x2A, 0x2A, 1 },
  { 0x15, 0x15, 1 }
};

// CADB Lines here corrospond to CADB_BUF e.g. Line0 is programmed in CADB_BUF0
CADB_LINE CadbLinesLp5[] = {
  { 0x00, 0x00, 0 },
  { 0x7F, 0x7F, 0 },
  { 0x2A, 0x2A, 1 },
  { 0x55, 0x55, 1 },
  { 0x7F, 0x7F, 1 },
  { 0x00, 0x00, 1 }
};

UINT32 CadbMuxLp4Pattern2[] = {
  0x000001E0,
  0x00000010,
  0x00000000
};

UINT32 CadbMuxLp4Pattern3[] = {
  0x000001F0,
  0x00000010,
  0x00000000
};

// This is order of pattern we expect to see. Each value is 1/2 tCK wide
// 0,0,0,0,2A,7F,7F,7F,7F,7F,0,0 - Pattern
// 0,0,0,0, 2, 4, 1, 1, 1, 1,0,0 - CadbLines/CADB_BUF
UINT32 CadbMuxLp5RisingPattern2[] = {
  0x000003C0,
  0x00000010,
  0x00000020
};

// 0,0,0,0,55,7F,7F,7F,7F,7F,0,0 - Pattern
// 0,0,0,0, 3, 4, 1, 1, 1, 1,0,0 - CadbLines/CADB_BUF
UINT32 CadbMuxLp5RisingPattern3[] = {
  0x000003D0,
  0x00000010,
  0x00000020
};

// This is the order of pattern we expect to see. Each value is 1/2 tCK wide
// 0,0,0,0,0,2A,7F,7F,7F,7F,7F,0,0 - Pattern
// 0,0,0,0,5, 2, 1, 1, 1, 1, 1,0,0 - CadbLines/CADB_BUF
UINT32 CadbMuxLp5FallingPattern2[] = {
  0x000007D0,
  0x00000020,
  0x00000010
};

// 0,0,0,0,0,55,7F,7F,7F,7F,7F,0,0 - Pattern
// 0,0,0,0,5, 3, 1, 1, 1, 1, 1,0,0 - CadbLines/CADB_BUF
UINT32 CadbMuxLp5FallingPattern3[] = {
  0x000007F0,
  0x00000020,
  0x00000010
};

// DDR5 CS Training Pattern
// Use a NOP command on CA[13:0] = 0x1F
// Drive CS_N low on the second phase.
CADB_LINE CadbLinesDdr5CsTrain[] = {
// Not Used,  CA[13:0], CS
  {    0x00,      0x1F, 1 },
  {    0x00,      0x1F, 0 }
};

// Switch between CADB_BUF[0] and CADB_BUF[1] which drives CS_N low on SPID.
// This will put CS_N in a toggling 0101 pattern.
UINT32 CadbMuxDdr5CsTrainPattern[] = {
  0xAAAAAAAA,
  0x00000000,
  0x00000000
};

// Switch between CADB_BUF[0] and CADB_BUF[1] which drives CS_N low on SPID.
// This will put CS_N low for 4 cycles
UINT32 CadbMuxDdr5CaTrainExit[3] = {
  0x000000F0,
  0x00000000,
  0x00000000
};

// DDR5 CA Training Pattern (LMN - Stressful)
// Advanced DDR5 CA Training pattern.
// This table defines the seeds to use
// for CAD Sequencers [2:0] in LFSR mode.
// The S0 seed is not used when S0 is in LMN mode.
UINT32 CadbLinesDdr5CaTrainLmnSeeds[] = {
  0x00000000, // S0 LFSR Seed (don't care in LMN mode)
  0x1625e3f2, // S1 LFSR Seed
  0xA738c746  // S2 LFSR seed
};

// DDR5 CA Training Pattern (LMN - Simple - Lone 0)
// Advanced DDR5 CA Training pattern.
// This table defines the select pattern to use
// for CAD Sequencer [2:0] in Pattern mode.
// The S0 pattern is not used when S0 is in LMN mode
// S1 = 0, S2 = 0 limits CADB_BUF selection to entries 0 and 1
UINT32 CadbLinesDdr5CaTrainLmnLoneZero[] = {
  0x00000000, // S0 pattern (don't care in LMN mode)
  0x00000000, // S1 pattern (Lone 0)
  0x00000000  // S2 pattern
};

// DDR5 CA Training Pattern (LMN - Simple - Lone 1)
// Advanced DDR5 CA Training pattern.
// This table defines the select pattern to use
// for CAD Sequencer [2:0] in Pattern mode.
// The S0 pattern is not used when S0 is in LMN mode
// S1 = 1, S2 = 0 limits CADB_BUF selection to entries 2 and 3
UINT32 CadbLinesDdr5CaTrainLmnLoneOne[] = {
  0x00000000, // S0 pattern (don't care in LMN mode)
  0xFFFFFFFF, // S1 pattern (Lone 1)
  0x00000000  // S2 pattern
};

// UniSequencer mode to use for Sequencers [2:0]
// This config places all sequencers in Pattern Buffer mode where
// the input pattern is rotated and used as the CADB_BUF[7:0]
// mux select
MRC_PG_UNISEQ_TYPE  UniseqModePatBuffer[CADB_20_NUM_DSEL_UNISEQ] = {
  MrcPgMuxPatBuffer, // S0
  MrcPgMuxPatBuffer, // S1
  MrcPgMuxPatBuffer  // S2
};

// UniSequencer mode to use for Sequencers [2:0]
// This config places Sequencer 0 in LMN mode and
// sequencers [1:0] in LFSR mode.
MRC_PG_UNISEQ_TYPE  UniseqModeLmnLfsr[CADB_20_NUM_DSEL_UNISEQ] = {
  MrcPgMuxLmn,  // S0
  MrcPgMuxLfsr, // S1
  MrcPgMuxLfsr  // S2
};

// UniSequencer mode to use for Sequencers [2:0]
// This config places Sequencer 0 in LMN mode and
// sequencers [1:0] in Pattern Buffer mode
MRC_PG_UNISEQ_TYPE  UniseqModeLmnPatBuffer[CADB_20_NUM_DSEL_UNISEQ] = {
  MrcPgMuxLmn,       // S0
  MrcPgMuxPatBuffer, // S1
  MrcPgMuxPatBuffer  // S2
};

//
//  Sets up 8 CADB lines that will be used to send out a CS / CA pattern.
//
//  -----------------------
//  CADB  Phase  Phase  CS
//  Line  High   Low
//  -----------------------
//   0    0x111  0x111  Off
//   1    0x222  0x222  On
//   2    0x333  0x333  Off
//   3    0x444  0x444  Off
//   4    0x555  0x555  Off
//   5    0x666  0x666  Off
//   6    0x777  0x777  Off
//   7    0x888  0x888  Off
//
//
//  The CS pattern uses Pattern Buffer and hence has 24 lines, with CS active for one line only.
//  This will send a command every 24 DCLKs.
//
//  Pattern Buffer details:
//  The line order is:    0, 1, 2, 3, 4, 5, 6, 7, 0, 1, 2, 3, 4, 5, 6, 7, 0, 1, 2, 3, 4, 5, 6, 7, 0, 1, 2, 3, 4, 5, 6, 7
//  or different command: 7, 6, 5, 4, 3, 2, 1, 0, 7, 6, 5, 4, 3, 2, 1, 0, 7, 6, 5, 4, 3, 2, 1, 0
//
//  000
//  001
//  010
//  011
//
//  100
//  101
//  110
//  111
//
//  000
//  001
//  010
//  011
//
//  100
//  101
//  110
//  111
//
//  000
//  001
//  010
//  011
//
//  100
//  101
//  110
//  111
//
//  000
//  001
//  010
//  011
//
//  100
//  101
//  110
//  111
//  ----
//  0CA  --> PB[0] = 0xAAAAAAAA
//  FCA      PB[1] = 0xCCCCCCCC
//  0CA      PB[2] = 0xF0F0F0F0
//  FCA
//  0CA
//  FCA
//  0CA
//  FCA
//----------------------------------------------
//
//  111
//  110
//  101
//  100
//
//  011
//  010
//  001
//  000
//
//  111
//  110
//  101
//  100
//
//  011
//  010
//  001
//  000
//
//  111
//  110
//  101
//  100
//
//  011
//  010
//  001
//  000
//-----
//  F35  --> PB[0] = 0x555555
//  035      PB[1] = 0x333333
//  F35      PB[2] = 0x0F0F0F
//  035
//  F35
//  035
/*
CADB_LINE CadbLinesTest[] = {
  { 0x11, 0x11, 0 },
  { 0x22, 0x22, 1 },
  { 0x33, 0x33, 0 },
  { 0x44, 0x44, 0 },
  { 0x55, 0x55, 0 },
  { 0x66, 0x66, 0 },
  { 0x77, 0x77, 0 },
  { 0x88, 0x88, 0 }
};

UINT32 CadbMuxPattern2Test[] = {
  0xAAAAAAAA,
  0xCCCCCCCC,
  0xF0F0F0F0
};

UINT32 CadbMuxPattern3Test[] = {
  0x555555,
  0x333333,
  0x0F0F0F
};
*/

/**
  Setup the CADB for CS or CA training.

  @param[in] MrcData    - The MRC global data.
  @param[in] Rank       - rank to work on
  @param[in] CadbLines  - CADB lines to program
  @param[in] CadbCount  - Number of CADB lines to program
  @param[in] PatBuf     - Pattern buffer array.
  @param[in] UniseqMode - Array specifying the mode to use for each Unisequencer
**/
void
SetupCaTrainingCadb (
  IN MrcParameters  *const  MrcData,
  IN UINT32                 Rank,
  IN CADB_LINE              *CadbLines,
  IN UINT32                 CadbCount,
  IN UINT32                 PatBuf[MRC_NUM_MUX_SEEDS],
  IN MRC_PG_UNISEQ_TYPE     UniseqMode[CADB_20_NUM_DSEL_UNISEQ]
  )
{
  MrcInput            *Inputs;
  MrcOutput           *Outputs;
  const MRC_FUNCTION  *MrcCall;
  MrcDdrType          DdrType;
  UINT32              i;
  UINT32              RankMask;
  UINT8               Cke[2];
  UINT8               Cs[2];
  UINT8               Odt;
  MRC_CA_MAP_TYPE     CmdAddr;
  UINT64_STRUCT       CadbPatternChunks[CADB_20_MAX_CHUNKS];
  static const MRC_PG_LFSR_TYPE CadbLfsrPoly[CADB_20_NUM_DSEL_UNISEQ] = {MrcLfsr32, MrcLfsr32, MrcLfsr32};

  Inputs  = &MrcData->Inputs;
  Outputs = &MrcData->Outputs;
  MrcCall = Inputs->Call.Func;
  DdrType = Outputs->DdrType;

  Odt = 0;
  MrcCall->MrcSetMem ((UINT8 *) Cke, sizeof (Cke), 0);
  MrcCall->MrcSetMem ((UINT8 *) Cs, sizeof (Cs), 0);
  MrcCall->MrcSetMem ((UINT8 *) CadbPatternChunks, sizeof (CadbPatternChunks), 0);

  for (i = 0; i < CadbCount; i++) {
    CmdAddr.Data32 = 0;
    if (DdrType == MRC_DDR_TYPE_DDR5) {
      RankMask = ((1 << MAX_RANK_IN_CHANNEL) - 1);
      RankMask = MrcBitSwap (RankMask, (CadbLines[i].ChipSelect), (UINT8) Rank, 1);
      Cs[0] = Cs[1] = (UINT8) RankMask;
    } else {
      Cs[0] = Cs[1] = (CadbLines[i].ChipSelect << Rank);
    }
    if (DdrType == MRC_DDR_TYPE_LPDDR4) {
      CmdAddr.Lpddr4.Ca1 = CadbLines[i].CaLow;   // Ch 0
      CmdAddr.Lpddr4.Ca2 = CadbLines[i].CaHigh;  // Ch 1
    } else if (DdrType == MRC_DDR_TYPE_LPDDR5) {
      CmdAddr.Lpddr5.Ca1 = CadbLines[i].CaLow;   // Ch 0
      CmdAddr.Lpddr5.Ca2 = CadbLines[i].CaHigh;  // Ch 1
    } else { // MRC_DDR_TYPE_DDR5
      CmdAddr.Ddr5.Ca = CadbLines[i].CaLow;
    }
    CpgcConvertDdrToCadb (MrcData, &CmdAddr, Cke, Cs, Odt, &CadbPatternChunks[i]);
  }

  // Program CADB Pattern Generator
  MrcSetCadbPgPattern (MrcData, Outputs->McChBitMask, CadbPatternChunks, CadbCount, 0);

  // Set Mux0/1/2 to Unisequencer mode specified in function inputs (LFSR size is ignored when unisequncer is configured as MrcPgMuxPatBuffer)
  Cadb20UniseqCfg (MrcData, UniseqMode, CadbLfsrPoly);

  // Program Pattern Buffers for a specific progression over CADB,
  // according to the given Pattern Buffer values
  MrcInitCadbPgMux (MrcData, PatBuf, 0, MRC_NUM_MUX_SEEDS);
}

/**
  Program DESWIZZLE_HIGH/LOW registers for MR4 decoding on LP4/LP5
  Program WRRETRAINSWIZZLECONTROL registers for MR18/MR19 decoding on LP4/LP5/DDR5

  @param[in] MrcData - The MRC global data.
**/
void
ProgramDeswizzleRegisters (
  IN MrcParameters *const MrcData
  )
{
  const MRC_FUNCTION  *MrcCall;
  MrcInput      *Inputs;
  MrcDebug      *Debug;
  MrcOutput     *Outputs;
  MrcChannelIn  *ChannelIn;
  MrcChannelOut *ChannelOut;
  UINT8         Controller;
  UINT8         Channel;
  UINT8         Rank;
  UINT8         RankIncrement;
  UINT32        Byte;
  UINT32        LpByte;
  UINT32        ByteForDqMap;
  UINT8         Bit;
  UINT8         DramBit;
  UINT8         DramByte;
  INT64         GetSetVal;
  INT64         ByteSel;
  BOOLEAN       Lpddr;
  BOOLEAN       Ddr5;
  BOOLEAN       Dimm;
  BOOLEAN       Width16;
  UINT8         FirstRank[MAX_CONTROLLER][MAX_CHANNEL];
#ifdef MRC_DEBUG_PRINT
  UINT32        Offset;
  UINT64        Data64;
  UINT64        ErmData64;
#endif

  Inputs    = &MrcData->Inputs;
  Outputs   = &MrcData->Outputs;
  Debug     = &Outputs->Debug;
  MrcCall   = Inputs->Call.Func;
  Lpddr     = Outputs->Lpddr;
  Ddr5      = (Outputs->DdrType == MRC_DDR_TYPE_DDR5);

  if (Outputs->DdrType == MRC_DDR_TYPE_DDR4) {
    // Nothing to do here for DDR4
    return;
  }

  MrcCall->MrcSetMem ((UINT8 *) FirstRank, sizeof (FirstRank), 0);
  GetFirstRank (MrcData, FirstRank);

  for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
    for (Channel = 0; Channel < Outputs->MaxChannels; Channel++) {
      if (!(MrcChannelExist (MrcData, Controller, Channel))) {
        continue;
      }
      ChannelIn  = &Inputs->Controller[Controller].Channel[Channel];
      ChannelOut = &Outputs->Controller[Controller].Channel[Channel];

      // DESWIZZLE_LOW/HIGH ERM registers are used on Rank1 in LP4/5, and on DIMM1 on DDR5
      // Hence run the loop below on Rank0/1 for LP4/5 and DIMM0/1 for DDR5
      RankIncrement = Ddr5 ? 2 : 1;
      for (Rank = 0; Rank < MAX_RANK_IN_CHANNEL; Rank += RankIncrement) {
        Dimm = RANK_TO_DIMM_NUMBER (Rank);
        if (!MrcRankExist (MrcData, Controller, Channel, Rank)) {
          continue;
        }
        for (Byte = 0; Byte < Outputs->SdramCount; Byte++) {
          if (Ddr5 && (Byte == (MAX_BYTE_IN_DDR5_CHANNEL - 1))) {
            continue;   // DDR5 ECC byte does not have DESWIZZLE register in the MC, it's always routed 1:1
          }
          DramByte = ChannelIn->DqsMapCpu2Dram[Dimm][Byte];
          if (Lpddr && ((Channel == 1) || (Channel == 3))) {
            GetSetVal = Byte + 4;
            LpByte    = DramByte + 2;  // This is used for GetSet of GsmMccDeswizzleBit, which expects bytes [0..3]
          } else {
            GetSetVal = Byte;
            LpByte    = DramByte;
          }
          // DqsMapCpu2Dram maps CPU bytes to DRAM, we need to find the reverse mapping here
          MrcGetSetStrobe (MrcData, Controller, Channel, Rank, DramByte, GsmMccDeswizzleByte, WriteToCache | PrintValue, &GetSetVal);

          if (Lpddr) {  // Bit-level deswizzle is not used in DDR5, keeping reset values
            for (Bit = 0; Bit < MAX_BITS; Bit++) {
              // DqMapCpu2Dram maps CPU bits to DRAM, we need to find the reverse mapping here
              GetSetVal = Bit;
              DramBit = ChannelIn->DqMapCpu2Dram[Byte][Bit] % 8;
              if (DramBit < 5) {  // MC needs to know how to deswizzle only DRAM bits [0..4] for temperature read
                MrcGetSetBit (MrcData, Controller, Channel, Rank, LpByte, DramBit, GsmMccDeswizzleBit, WriteToCache | PrintValue, &GetSetVal);
              }
            }
          }
        } // Byte
      } // Dimm

      Rank = FirstRank[Controller][Channel];
      if (!MrcRankExist (MrcData, Controller, Channel, Rank)) {
        continue;
      }
      Dimm = RANK_TO_DIMM_NUMBER (Rank);
      Width16 = (ChannelOut->Dimm[Dimm].SdramWidth == 16);

      // Map the CPU bytes to the DRAM devices by programming the byte/bit deswizzle information in the DDRIO
      for (Byte = 0; Byte < Outputs->SdramCount; Byte++) {
        DramByte = ChannelIn->DqsMapCpu2Dram[Dimm][Byte];
        MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "MC%u C%u DqsMapCpu2Dram[%d]: %d\n", Controller, Channel, Byte, DramByte); // Lpddr only uses DIMM0 entry
        // ByteSel should always point to DRAM Byte 0 for x16 DRAM devices
        /*
        DRAM Byte0 --- SOC Byte0
        DRAM Byte1 --- SOC Byte1
                          SOC Byte0        SOC Byte1
        ByteSel          0 (Local)        1 (Neighbor)
        Retrain_DQ       SOC Byte0        SOC Byte0

        DRAM Byte0 --- SOC Byte1
        DRAM Byte1 --- SOC Byte0
                          SOC Byte0        SOC Byte1
        ByteSel          1 (Neighbor)     0 (Local)
        Retrain_DQ       SOC Byte1        SOC Byte1
        */
        // Assume x8 devices first
        ByteSel = 0;          // LP4/5 byte mode or DDR5 x8 - every byte uses "Local" ByteSel, because all devices are x8
        ByteForDqMap = Byte;  // Program DQ mapping below using this byte
        if ((Lpddr && !Outputs->LpByteMode) || (Ddr5 && Width16)) {
          // LP4/5 x16 or DDR5 x16
          // Use Local for DRAM byte 0 or 2, and Neighbor for DRAM byte 1 and 3
          if ((DramByte == 1) || (DramByte == 3)) {
            ByteSel = 1;
            ByteForDqMap = Byte ^ 1;    // Program DQ mapping below using the neighbor byte (goes to DRAM byte 0 / 2)
          }
        }
        if (Ddr5 && Inputs->A0 && (Byte == MRC_DDR5_ECC_BYTE)) {
          ByteSel = 1;
        }
        MrcGetSetChStrb (MrcData, Controller, Channel, Byte, GsmIocRetrainSwizzleCtlByteSel, WriteToCache | PrintValue, &ByteSel);
        for (Bit = 0; Bit < MAX_BITS; Bit++) {
          // DQ bit mapping should always be deswizzling DRAM Byte0 DQ bits for x16 DRAM devices
          // DqMapCpu2Dram maps CPU DQ pins to DRAM, we need to find the reverse mapping here
          GetSetVal = Bit;
          MrcGetSetBit (MrcData, Controller, Channel, MRC_IGNORE_ARG, Byte, ChannelIn->DqMapCpu2Dram[ByteForDqMap][Bit] % 8, GsmIocRetrainSwizzleCtlBit, WriteToCache | PrintValue, &GetSetVal); // Rank is not used here
        }
      } // Byte
    } // Channel
  } // Controller
  MrcFlushRegisterCachedData (MrcData);

#ifdef MRC_DEBUG_PRINT
    for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
      for (Channel = 0; Channel < MAX_CHANNEL_SHARE_REGS; Channel++) {
        if (MrcChannelExist (MrcData, Controller, Channel)) {
          Offset = OFFSET_CALC_MC_CH (MC0_CH0_CR_DESWIZZLE_LOW_REG, MC1_CH0_CR_DESWIZZLE_LOW_REG, Controller, MC0_CH1_CR_DESWIZZLE_LOW_REG, Channel);
          Data64 = MrcReadCR64 (MrcData, Offset);
          Offset = OFFSET_CALC_MC_CH (MC0_CH0_CR_DESWIZZLE_LOW_ERM_REG, MC1_CH0_CR_DESWIZZLE_LOW_ERM_REG, Controller, MC0_CH1_CR_DESWIZZLE_LOW_ERM_REG, Channel);
          ErmData64 = MrcReadCR64 (MrcData, Offset);
          MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Mc%u.Ch%u DESWIZZLE_LOW=0x%llx, DESWIZZLE_LOW_ERM=0x%llx\n", Controller, Channel, Data64, ErmData64);

          Offset = OFFSET_CALC_MC_CH (MC0_CH0_CR_DESWIZZLE_HIGH_REG, MC1_CH0_CR_DESWIZZLE_HIGH_REG, Controller, MC0_CH1_CR_DESWIZZLE_HIGH_REG, Channel);
          Data64 = MrcReadCR64 (MrcData, Offset);
          Offset = OFFSET_CALC_MC_CH (MC0_CH0_CR_DESWIZZLE_HIGH_ERM_REG, MC1_CH0_CR_DESWIZZLE_HIGH_ERM_REG, Controller, MC0_CH1_CR_DESWIZZLE_HIGH_ERM_REG, Channel);
          ErmData64 = MrcReadCR64 (MrcData, Offset);
          MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Mc%u.Ch%u DESWIZZLE_HIGH=0x%llx, DESWIZZLE_HIGH_ERM=0x%llx\n", Controller, Channel, Data64, ErmData64);
        }
      }
    }
#endif // MRC_DEBUG_PRINT
}

/**
This function configures the terminating ranks to FSP-OP 1 to enable their CA ODT
termination for the non-terminating ranks if enabled.  Otherwise, they are set
to FSP-OP 0, which is the reset default configuration (off).
This is done across all LPDDR4 and LPDDR5 channels.

@param[in]  MrcData - Pointer to the MRC global data.
@param[in]  Enable  - Boolean parameter to enable(TRUE) or disable(FALSE) CA termination.

@retval MrcStatus - mrcSuccess, otherwise an error status.
**/
MrcStatus
MrcLpddrSetCbtCaOdtEn (
  IN  MrcParameters *const  MrcData,
  IN  BOOLEAN               Enable
)
{
  MrcStatus           Status;
  MrcStatus           CurStatus;
  MrcSaveData         *SaveData;
  MrcDdrType          DdrType;
  MrcOutput           *Outputs;
  INT32               CtlPi;
  INT32               CmdPi;
  UINT8               Controller;
  UINT8               Channel;
  UINT8               MaxChannel;
  UINT8               Rank;
  UINT8               RankMask;
  UINT8               CmdRanksTermMask;
  UINT32              CurCtlPiCode[MAX_CONTROLLER][MAX_CHANNEL][MAX_RANK_IN_CHANNEL];
  UINT32              CurCmdPiCode[MAX_CONTROLLER][MAX_CHANNEL];
  INT64               GetSetVal;
  BOOLEAN             Lpddr4;
  LPDDR4_FSP_OP_TYPE  FspOpPoint;

  SaveData = &MrcData->Save.Data;
  Outputs = &MrcData->Outputs;
  DdrType = Outputs->DdrType;
  Lpddr4  = (DdrType == MRC_DDR_TYPE_LPDDR4);
  Status  = mrcSuccess;

  CmdRanksTermMask = MrcData->Inputs.CmdRanksTerminated;
  MaxChannel = Outputs->MaxChannels;

  FspOpPoint = (Enable) ? LpFspOpPoint1 : LpFspOpPoint0;

  CtlPi = (Lpddr4) ? MRC_SAFE_LP4_CTL_PI : MRC_SAFE_LP5_CTL_PI;
  CmdPi = (Lpddr4) ? MRC_SAFE_LP4_CMD_PI : MRC_SAFE_LP5_CMD_PI;

  for (Rank = 0; Rank < MAX_RANK_IN_CHANNEL; Rank++) {
    RankMask = 1 << Rank;
    if ((RankMask & Outputs->ValidRankMask) == 0) {
      continue;
    }

    if ((CmdRanksTermMask & RankMask) != 0) {
      for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
        for (Channel = 0; Channel < MaxChannel; Channel++) {
          if (!MrcChannelExist (MrcData, Controller, Channel)) {
            continue;
          }
          // Save/Restore from CR. Values from host structure may be trained values at higher gears.
          // Using values from host structure at lowfreq forces wrong gear ratio multiplication in GetSet
          MrcGetSetCcc (MrcData, Controller, Channel, Rank, 0, CtlGrpPi, ReadFromCache, &GetSetVal);
          CurCtlPiCode[Controller][Channel][Rank] = (UINT32) GetSetVal;
          MrcGetSetCcc (MrcData, Controller, Channel, MRC_IGNORE_ARG, 0, CmdGrpPi, ReadFromCache, &GetSetVal);
          CurCmdPiCode[Controller][Channel] = (UINT32) GetSetVal;

          if (!Lpddr4) { // LP5
            CtlPi = (INT32) SaveData->LowFCtlPiCode[Controller][Channel][Rank];
            CmdPi = (INT32) SaveData->LowFCmdPiCode[Controller][Channel];
          }
          // Use safe CS/CA PI settings, otherwise this MRW may get sampled as invalid command (or SRE etc.)
          ShiftPIforCmdTraining (MrcData, Controller, Channel, MrcIterationCtl, RankMask, MRC_IGNORE_ARG_8, CtlPi, 0);
          ShiftPIforCmdTraining (MrcData, Controller, Channel, MrcIterationCmd, MRC_IGNORE_ARG_8, 0xF, CmdPi, 0);
        }
      }
      IoReset (MrcData);

      CurStatus = MrcSetFspVrcg (MrcData, RankMask, MRC_IGNORE_ARG_8, MRC_IGNORE_ARG_8, FspOpPoint);
      if (Status == mrcSuccess) {
        Status = CurStatus;
      }
      for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
        for (Channel = 0; Channel < MaxChannel; Channel++) {
          if (!MrcChannelExist (MrcData, Controller, Channel)) {
            continue;
          }
          // Restore CS/CA PI settings from the host struct
          // CS is per subch
          ShiftPIforCmdTraining (MrcData, Controller, Channel, MrcIterationCtl, RankMask, MRC_IGNORE_ARG_8, CurCtlPiCode[Controller][Channel][Rank], 0);
          ShiftPIforCmdTraining (MrcData, Controller, Channel, MrcIterationCmd, MRC_IGNORE_ARG_8, 0xF, CurCmdPiCode[Controller][Channel], 0);
        }
      }
      IoReset (MrcData);
    }
  } // Rank

  return Status;
}

/**
  Run the CADB test on all channels / ranks at the current CS/CMD PIs.
  For LP5 ECT training, JEDEC allows Commands to be sampled at rising edge of CK and falling edge of CK. MR16.CbtPhase = 0 is for the former.
  This function runs the ShortCADB test twice, collects results and changes the MR16.CbtPhase for that channel.
  In the end all channels have to pass both CBT Phases for the 2D sweep of CS,CMD w.r.t CK training to pass.

  @param[in] MrcData - The MRC global data.
  @retval Bitmask of passing ranks
**/
void
RunEarlyCsCmdTestLpddr (
  IN  MrcParameters * const MrcData,
  OUT UINT8                 RankPassMask[MAX_CONTROLLER][MAX_CHANNEL]
  )
{
  MRC_FUNCTION        *MrcCall;
  MrcDebug            *Debug;
  MrcInput            *Inputs;
  MrcOutput           *Outputs;
  MrcChannelIn        *ChannelIn;
  MrcStatus           Status;
  MrcStatus           CurStatus;
  BOOLEAN             IgnoreByte;
  CADB_LINE           *CadbLines;
  UINT32              CadbCount;
  UINT32              *PatBuf;
  const UINT32        *Seeds;
  UINT8               Controller;
  UINT8               Channel;
  UINT8               MaxChannel;
  UINT8               McChannelMask;
  UINT16              MaskCheck;
  UINT16              PhaseMask;
  UINT16              PhasePassMask;
  UINT8               Rank;
  UINT8               RankBit;
  UINT8               TermRankMask;
  UINT8               Byte;
  UINT8               ByteMask;
  UINT8               DramByte;
  UINT8               NumOfOnes;
  UINT8               TotalNumOfOnes;
  UINT8               FeedbackMask;
  UINT8               CbtPhase;
  UINT32              CaPattern;
  UINT32              McChannelPassMask;
  UINT32              WckControlSave;
  UINT32              WckControl1Save;
  UINT32              DataRcompDataSave[MAX_CONTROLLER][MAX_SDRAM_IN_DIMM];
  UINT8               BytePassMaskCheck;
  UINT8               BytePassMask[MAX_CHANNEL];
  UINT32              Feedback[MAX_CONTROLLER][MAX_CHANNEL][MAX_SDRAM_IN_DIMM];
  UINT32              DelayCadb;
  UINT32              Pattern;
  CHAR8               PassFail;
  INT64               DataTrainFeedbackField;
  UINT32              DataTrainFeedback;
  MrcDdrType          DdrType;
  BOOLEAN             Lpddr4;
  BOOLEAN             Lpddr5;

  Inputs        = &MrcData->Inputs;
  Outputs       = &MrcData->Outputs;
  Debug         = &Outputs->Debug;
  MrcCall       = Inputs->Call.Func;
  TermRankMask  = Inputs->CmdRanksTerminated;
  MaxChannel    = Outputs->MaxChannels;
  DdrType       = Outputs->DdrType;
  Lpddr4        = (DdrType == MRC_DDR_TYPE_LPDDR4);
  Lpddr5        = (DdrType == MRC_DDR_TYPE_LPDDR5);
  FeedbackMask  = FEEDBACKMASK;
  DataTrainFeedback = 0;

  DelayCadb = (Lpddr4) ? (1 * MRC_TIMER_1US) : (MRC_LP5_tADR_PS / 1000);

  BytePassMaskCheck = (1 << Outputs->SdramCount) - 1;
  MrcCall->MrcSetMem ((UINT8 *) BytePassMask, sizeof (BytePassMask), 0);

  CadbLines = Lpddr4 ? CadbLinesLp4 : CadbLinesLp5;
  CadbCount = Lpddr4 ? ARRAY_COUNT (CadbLinesLp4) : ARRAY_COUNT (CadbLinesLp5);

  PatBuf    = Lpddr4 ? CadbMuxLp4Pattern2 : CadbMuxLp5RisingPattern2;
  // LP5 patterns with CS are 0x2a and 0x55. 0x2a has 3 ones but 0x55 has 4 ones. LP4 patterns has 3 ones for both.
  CaPattern = Lpddr4 ? (CadbLines[2].CaHigh & CAPATTERNMASK) : (CadbLines[3].CaHigh & CAPATTERNMASK);
  NumOfOnes = MrcCountBitsEqOne (CaPattern);
  TotalNumOfOnes = Lpddr4 ? (2 * NumOfOnes) : NumOfOnes + MrcCountBitsEqOne (CadbLinesLp5[2].CaHigh & CAPATTERNMASK);
  if (Lpddr5) {
    NumOfOnes += 1;
  }
  Status = mrcSuccess; // Initialize Status otherwise MiniBios fails

  for (Rank = 0; Rank < MAX_RANK_IN_CHANNEL; Rank++) {
    RankBit = 1 << Rank;
    if ((RankBit & Outputs->ValidRankMask) == 0) {
      continue;
    }

    if ((RankBit & TermRankMask) == 0) {
      // Enable Termination on the terminated rank via FSP-High
      Status = MrcLpddrSetCbtCaOdtEn (MrcData, TRUE);
      if (Status != mrcSuccess) {
        return;
      }
    }


    // PhaseMask will be packed as below
    // Each Channel has 2 consecutive bits for PassFail results
    // Channel 0 through 7 for both MCs will occupy all even positions for CbtPhase0
    // Bit 15  14  13  12  11  10  9  8  7  6  5  4  3  2  1  0
    // Ph0      1       1       1     1     1     1     1     1
    // Ph1  1       1       1      1     1     1     1     1
    // if Ph1 isnt run for LP5 then PhaseMask will be 0xFF. Each bit represent a pass for the channel
    PhasePassMask = 0;
    PhaseMask = 0;
    McChannelPassMask = 0; // Bitmask of passed channels on the current rank
    McChannelMask = 0;     // Bitmask of channels that have current rank populated
    for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
      for (Channel = 0; Channel < MaxChannel; Channel++) {
        if (!MrcRankExist (MrcData, Controller, Channel, Rank)) {
          continue;
        }
        McChannelMask |= (1 << ((Controller * MaxChannel) + Channel));
        for (CbtPhase = 0; CbtPhase < MAX_CBT_PHASES; CbtPhase++) {
          PhaseMask |= (MRC_BIT0 << ((((Controller * MaxChannel) + (Channel)) * MAX_CBT_PHASES) + CbtPhase));
        }
      }  // for Channel
    } // for Controller

    for (CbtPhase = 0; CbtPhase < MAX_CBT_PHASES; CbtPhase++) {
      if (Lpddr4 && (CbtPhase > 0)) { // Run this loop only once for Lp4
        continue;
      }
      // Setup and Enter CBT Mode for this Rank.
      SetupCaTrainingCadb (MrcData, Rank, CadbLines, CadbCount, PatBuf, UniseqModePatBuffer);
      Status = LpddrCommandTrainingMode (MrcData, McChannelMask, RankBit, 1, CbtPhase, &WckControlSave, &WckControl1Save, DataRcompDataSave);
      if (Status != mrcSuccess) {
        return;
      }

      if (Lpddr4) {
        // Send VrefCA on DQ[6:0]
        MrcSendCaVrefOnDq (MrcData, Rank);
      }

      // Try two different patterns (0x2A or 0x55), to see if the command is still decoded correctly
      for (Pattern = 0; Pattern <= 1; Pattern++) {
        if (Lpddr4) {
          Seeds = (Pattern == 0) ? CadbMuxLp4Pattern2 : CadbMuxLp4Pattern3;
        } else {
          if (CbtPhase == 0) {
            Seeds = (Pattern == 0) ? CadbMuxLp5RisingPattern2 : CadbMuxLp5RisingPattern3;
          } else {
            Seeds = (Pattern == 0) ? CadbMuxLp5FallingPattern2 : CadbMuxLp5FallingPattern3;
          }
        }
        MrcInitCadbPgMux (MrcData, Seeds, 0, MRC_NUM_MUX_SEEDS);

        // Run CADB pattern on selected channels at the same time
        ShortRunCADB (MrcData, McChannelMask);
        MrcWait (MrcData, DelayCadb);

        // Read and process the results
        for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
          for (Channel = 0; Channel < MaxChannel; Channel++) {
            if (!MrcRankExist (MrcData, Controller, Channel, Rank)) {
              if (Pattern == 1) {
                MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\t");
              }
              continue;
            }
            ChannelIn = &Inputs->Controller[Controller].Channel[Channel];
            BytePassMask[Channel] = 0;
            for (Byte = 0; Byte < Outputs->SdramCount; Byte++) {
              ByteMask = (1 << Byte);
              DramByte = ChannelIn->DqsMapCpu2Dram[dDIMM0][Byte]; // Lpddr only uses DIMM0 entry
              IgnoreByte = FALSE;
              if (!MrcByteExist (MrcData, Controller, Channel, Byte)) {
                IgnoreByte = TRUE;
              }
              if (Lpddr4) {
                if (((DramByte & 1) == 0) && !Outputs->LpByteMode) {
                  // Ignore DRAM bytes 0, 2, 4 and 6 - they don't provide feedback
                  // Byte mode parts provide feedback on all bytes
                  IgnoreByte = TRUE;
                }
              } else {
                if ((DramByte & 1) == 1) { // Ignore DRAM bytes 1, 3, 5 and 7 - they don't provide feedback
                  IgnoreByte = TRUE;
                }
              }
              if (IgnoreByte) {
                BytePassMask[Channel] |= ByteMask;
                continue;
              }
              MrcGetSetChStrb (MrcData, Controller, Channel, Byte, GsmIocDataTrainFeedback, ReadUncached, &DataTrainFeedbackField);
              DataTrainFeedback = (UINT32) DataTrainFeedbackField & FeedbackMask;  // Get only DQ bits, not DQS
              if (Pattern == 0) {
                Feedback[Controller][Channel][Byte] = DataTrainFeedback; // Save the first pattern
              } else {
                // Second Pattern
                // If still read the same data, then DRAM was not able to decode the new command
                if ((Feedback[Controller][Channel][Byte] != DataTrainFeedback) &&
                  (MrcCountBitsEqOne (DataTrainFeedback) == NumOfOnes) &&
                  (MrcCountBitsEqOne (DataTrainFeedback ^ Feedback[Controller][Channel][Byte]) == TotalNumOfOnes)) {
                  BytePassMask[Channel] |= ByteMask;
                }
              }
            }  // for Byte
            if (Pattern == 1) {
              if (BytePassMask[Channel] == BytePassMaskCheck) { // If all bytes on a channel passed, mark this channel as passed.
                McChannelPassMask |= (MRC_BIT0 << ((Controller * MaxChannel) + Channel));
                PhasePassMask |= (MRC_BIT0 << ((((Controller * MaxChannel) + (Channel)) * MAX_CBT_PHASES) + CbtPhase));
                PassFail = '.';
              } else {
                PassFail = '#';
              }
              MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\t%c(0x%02X)", PassFail, DataTrainFeedback);
            }
          }  // for Channel
        } // for Controller
      } // for Pattern

      PhasePassMask = (Lpddr4) ? (UINT16) McChannelPassMask : PhasePassMask;

      for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
        for (Channel = 0; Channel < MaxChannel; Channel++) {
          if (!MrcRankExist (MrcData, Controller, Channel, Rank)) {
            continue;
          }
          MaskCheck = (Lpddr4) ? MRC_BIT0 : (MRC_BIT0 << MAX_CBT_PHASES) - 1; //0x1 if 1 phase, 0x3 if 2 phase
          MaskCheck = (MaskCheck << ((Controller * MaxChannel) + Channel));
          if ((PhasePassMask & MaskCheck) == (PhaseMask & MaskCheck)) { // If both CbtPhases pass (LP5)
            RankPassMask[Controller][Channel] |= RankBit; // Current MCxCHy passing, mark this rank as passed
          }
        }
      }

      // Exit CA training mode on the current rank
      Status = LpddrCommandTrainingMode (MrcData, McChannelMask, RankBit, 0, CbtPhase, &WckControlSave, &WckControl1Save, DataRcompDataSave);
      if ((RankBit & TermRankMask) == 0) {
        // Disable Termination on the terminated rank via FSP-High
        CurStatus = MrcLpddrSetCbtCaOdtEn (MrcData, FALSE);
        if (Status == mrcSuccess) {
          Status = CurStatus;
        }
      }
    } // CbtPhase
    if (Status != mrcSuccess) {
      return;
    }
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\t");
  } // for Rank

  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\n");
}

/**
  Find a good starting point for LPDDR3/LPDDR4 CMD/CS, using a quick 2D search.
  CLK is being kept at default (64).
  Start from ideal theoretical point: CS=64, CMD=96 (LPDDR3) / CMD=64 (LPDDR4).
  Sweep CS with a step of 16 PI ticks.
  Sweep CMD with a step of 16 PI ticks, and look for 3 consecutive passing points.
  The CMD eye in ECT stress is about 60 ticks, so we can always find such 3 points.
  Run CS training CADB pattern to robustly catch CS/CMD errors.
  Stop when a PASS found on all channels/ranks.
  Select the middle of the three CMD passing points.

  @param[in] MrcData - The MRC global data.

  @retval mrcSuccess if succeeded
**/
MrcStatus
EarlyCsCmdLpddr (
  IN MrcParameters *const MrcData
  )
{
  MRC_FUNCTION        *MrcCall;
  MrcInput            *Inputs;
  MrcDebug            *Debug;
  MrcOutput           *Outputs;
  MrcSaveData         *SaveData;
  MrcStatus           Status;
  INT64               ForceRxOn;
  INT64               TxPiOn;
  UINT32              Controller;
  UINT32              Channel;
  UINT8               Rank;
  UINT8               RankMaskBit;
  UINT8               MaxChannel;
  UINT8               RankMask;
  UINT8               RankPassMask[MAX_CONTROLLER][MAX_CHANNEL];
  UINT32              CaPiCode;
  UINT32              CsPiCode;
  UINT32              CaPiCodePass[MAX_CONTROLLER][MAX_CHANNEL][ARRAY_COUNT (CsCaPiTableLP5G4)];
  UINT32              CsPiCodePass[MAX_CONTROLLER][MAX_CHANNEL][ARRAY_COUNT (CsCaPiTableLP5G4)];
  UINT32              FirstController;
  UINT32              FirstChannel;
  UINT8               PassRange[MAX_CONTROLLER][MAX_CHANNEL];
  UINT8               CurConsecutivePass[MAX_CONTROLLER][MAX_CHANNEL][MAX_RANK_IN_CHANNEL];
  UINT8               LargestConsecutivePass[MAX_CONTROLLER][MAX_CHANNEL][MAX_RANK_IN_CHANNEL];
  UINT8               ConPassCount[ARRAY_COUNT (CsCaPiTableLP5G4)];
  UINT8               LargestPassCount;
  UINT8               CaPassIndex;
  UINT8               CsPassIndex;
  UINT8               CsPiIndexMax;
  UINT8               CaPiIndexMax;
  UINT8               NumCsPassIndex;
  UINT8               MidCsPassIndex;
  UINT32              StartCsPiIndex;
  UINT32              CsPiIndex;
  UINT32              CaPiIndex;
  BOOLEAN             Done;
  BOOLEAN             Lpddr4;
  BOOLEAN             Lpddr5;
  BOOLEAN             LowFreqCsCmd2DSweepDone;
  const UINT16        *CsPiTable;
  const UINT16        *CaPiTable;
  UINT8               HighGearRatio;
  UINT8               CurrGear;
  UINT8               HighGear;
  MrcIntOutput        *IntOutputs;
  MrcIntCmdTimingOut  *IntCmdTiming;

  Status  = mrcSuccess;
  Inputs  = &MrcData->Inputs;
  Outputs = &MrcData->Outputs;
  Debug   = &Outputs->Debug;
  MrcCall = Inputs->Call.Func;
  RankMask = Outputs->ValidRankMask;
  MaxChannel = Outputs->MaxChannels;
  SaveData   = &MrcData->Save.Data;
  CsPiCode = 0;
  CaPiCode = 0;
  CaPassIndex = 0;
  CsPassIndex = 0;
  LargestPassCount = 0;
  Lpddr4 = (Outputs->DdrType == MRC_DDR_TYPE_LPDDR4);
  Lpddr5 = (Outputs->DdrType == MRC_DDR_TYPE_LPDDR5);
  NumCsPassIndex = Lpddr4 ? 4 : 8;
  MidCsPassIndex          = NumCsPassIndex / 2;
  CurrGear                = (Outputs->Gear4 ? 4 : (Outputs->Gear2 ? 2 : 1));
  HighGear                = Outputs->HighGear;
  LowFreqCsCmd2DSweepDone = Outputs->LowFreqCsCmd2DSweepDone;
  HighGearRatio    = (LowFreqCsCmd2DSweepDone == FALSE) ? 1 : (HighGear / CurrGear);
  IntOutputs       = (MrcIntOutput *) (MrcData->IntOutputs.Internal);

  Done = TRUE;
  MrcCall->MrcSetMem ((UINT8 *) CaPiCodePass, sizeof (CaPiCodePass), 0);
  MrcCall->MrcSetMem ((UINT8 *) CsPiCodePass, sizeof (CsPiCodePass), 0);
  MrcCall->MrcSetMem ((UINT8 *) RankPassMask, sizeof (RankPassMask), 0); // Bitmask of passed ranks
  MrcCall->MrcSetMem ((UINT8 *) ConPassCount, sizeof (ConPassCount), 0xFF);

  // Init DDR IO for CA training
  if (Lpddr5) {
    CsPiTable    = ((LowFreqCsCmd2DSweepDone == FALSE) || (HighGear == 2)) ? CsCaPiTableLP5G2 : CsCaPiTableLP5G4;
    CaPiTable    = ((LowFreqCsCmd2DSweepDone == FALSE) || (HighGear == 2)) ? CsCaPiTableLP5G2 : CsCaPiTableLP5G4;
    CsPiIndexMax = ((LowFreqCsCmd2DSweepDone == FALSE) || (HighGear == 2)) ? ARRAY_COUNT (CsCaPiTableLP5G2) : ARRAY_COUNT (CsCaPiTableLP5G4);
    CaPiIndexMax = ((LowFreqCsCmd2DSweepDone == FALSE) || (HighGear == 2)) ? ARRAY_COUNT (CsCaPiTableLP5G2) : ARRAY_COUNT (CsCaPiTableLP5G4);
  } else { //Lpddr4
    CsPiTable    = (HighGear == 4) ? CsPiTableLP4G4 : ((HighGear == 2) ? CsPiTableLP4G2 : CsPiTableLP4G1);
    CaPiTable    = (HighGear == 4) ? CsPiTableLP4G4 : ((HighGear == 2) ? CsPiTableLP4G2 : CsPiTableLP4G1);
    CsPiIndexMax = (HighGear == 4) ? ARRAY_COUNT (CsPiTableLP4G4) : ((HighGear == 2) ? ARRAY_COUNT (CsPiTableLP4G2) : ARRAY_COUNT (CsPiTableLP4G1));
    CaPiIndexMax = (HighGear == 4) ? ARRAY_COUNT (CsPiTableLP4G4) : ((HighGear == 2) ? ARRAY_COUNT (CsPiTableLP4G2) : ARRAY_COUNT (CsPiTableLP4G1));
  }
  // Read the first populated channel
  FirstController = (MrcControllerExist (MrcData, cCONTROLLER0)) ? 0 : 1;
  FirstChannel = Outputs->Controller[FirstController].FirstPopCh;
  // Following fields are from datacontrol regs, channel is equivalent to controller for offset calc.
  // But these enums go through MrcTranslateSystemToIp translates channel and strobe to corresponding memtech
  // Read the initialized value of ForceRxAmpOn and TxPwrDnDisable for first populated byte
  MrcGetSetChStrb (MrcData, FirstController, FirstChannel, 0, GsmIocForceRxAmpOn, ReadFromCache, &ForceRxOn);
  MrcGetSetChStrb (MrcData, FirstController, FirstChannel, 0, GsmIocTxPiPwrDnDis, ReadFromCache, &TxPiOn);

  LpddrCaTrainingInitIo (MrcData);

#ifdef MRC_DEBUG_PRINT
  for (Rank = 0; Rank < MAX_RANK_IN_CHANNEL; Rank++) {
    RankMaskBit = RankMask >> Rank;
    if (RankMaskBit == 0) {
      break;
    }
    for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
      for (Channel = 0; Channel < MaxChannel; Channel++) {
        MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\tMc%uC%uR%u", Controller, Channel, Rank);
      }
    }
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\t");
  }
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\n");
#endif
  for (CsPiIndex = 0; CsPiIndex < CsPiIndexMax; CsPiIndex++) {
    CsPiCode = CsPiTable[CsPiIndex];
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "CS: %d\n", CsPiCode);
    for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
      for (Channel = 0; Channel < MaxChannel; Channel++) {
        if (!MrcChannelExist (MrcData, Controller, Channel)) {
          continue;
        }
        // Shift the CS PI on all ranks, keep the value in the host struct.
        ShiftPIforCmdTraining (MrcData, Controller, Channel, MrcIterationCtl, RankMask, MRC_IGNORE_ARG_8, CsPiCode / HighGearRatio, 0);
      }
    }

    MrcCall->MrcSetMem ((UINT8 *) PassRange, sizeof (PassRange), 0);
    MrcCall->MrcSetMem ((UINT8 *) LargestConsecutivePass, sizeof (LargestConsecutivePass), 0);
    MrcCall->MrcSetMem ((UINT8 *) CurConsecutivePass, sizeof (CurConsecutivePass), 0);
    CaPassIndex = 0;            // Restart search for 3 passing CMD points
    for (CaPiIndex = 0; CaPiIndex < CaPiIndexMax; CaPiIndex++) {
      CaPiCode = CaPiTable[CaPiIndex];
      MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "CMD:%d\t", CaPiCode);
      for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
        for (Channel = 0; Channel < MaxChannel; Channel++) {
          if (!MrcChannelExist (MrcData, Controller, Channel)) {
            continue;
          }
          // Shift the Command PI on both CAA and CAB groups; RankMask is not relevant for CMD/CKE.
          // Keep the value in the host struct.
          ShiftPIforCmdTraining (MrcData, Controller, Channel, MrcIterationCmd, MRC_IGNORE_ARG_8, 0xF, CaPiCode / HighGearRatio, 0);
        }
      }
      MrcCall->MrcSetMem ((UINT8 *) RankPassMask, sizeof (RankPassMask), 0); // Bitmask of passed ranks
      RunEarlyCsCmdTestLpddr (MrcData, RankPassMask); // Run the test on all channels / ranks at the current CS/CMD PIs

      for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
        for (Channel = 0; Channel < MaxChannel; Channel++) {
          for (Rank = 0; Rank < MAX_RANK_IN_CHANNEL; Rank++) {
            if (!MrcRankExist (MrcData, Controller, Channel, Rank)) {
              continue;
            }
            RankMaskBit = 1 << Rank;
            if (RankPassMask[Controller][Channel] & RankMaskBit) {
              CurConsecutivePass[Controller][Channel][Rank]++;
              LargestConsecutivePass[Controller][Channel][Rank] = MAX (LargestConsecutivePass[Controller][Channel][Rank], CurConsecutivePass[Controller][Channel][Rank]);
            } else {
              CurConsecutivePass[Controller][Channel][Rank] = 0;
            }
          }
        }
      }

    } // for CMD PI
    for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
      for (Channel = 0; Channel < MaxChannel; Channel++) {
        for (Rank = 0; Rank < MAX_RANK_IN_CHANNEL; Rank++) {
          if (!MrcRankExist (MrcData, Controller, Channel, Rank)) {
            continue;
          }
          ConPassCount[CsPiIndex] = MIN (ConPassCount[CsPiIndex], LargestConsecutivePass[Controller][Channel][Rank]);
        }
      }
    }
    LargestPassCount = MAX (LargestPassCount, ConPassCount[CsPiIndex]);
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Max Consecutive for CS(%d): %d\n\n", CsPiCode, ConPassCount[CsPiIndex]);
  }  // for CS PI
  StartCsPiIndex = 0;
  if (LargestPassCount > 2) {
    LargestPassCount -= 2;
  } else {
    LargestPassCount = 2;
  }
  for (CsPiIndex = 0; CsPiIndex < CsPiIndexMax; CsPiIndex++) {
    if (ConPassCount[CsPiIndex] >= LargestPassCount) {
      StartCsPiIndex = CsPiIndex;
      break;
    }
  }

  for (CsPiIndex = StartCsPiIndex; CsPiIndex < CsPiIndexMax; CsPiIndex++) {
    CsPiCode = CsPiTable[CsPiIndex];
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "CS: %d\n", CsPiCode);
    for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
      for (Channel = 0; Channel < MaxChannel; Channel++) {
        if (!MrcChannelExist (MrcData, Controller, Channel)) {
          continue;
        }
        // Shift the CS PI on all ranks, keep the value in the host struct.
        ShiftPIforCmdTraining (MrcData, Controller, Channel, MrcIterationCtl, RankMask, MRC_IGNORE_ARG_8, CsPiCode / HighGearRatio, 0);
      }
    }

    CaPassIndex = 0;            // Restart search for 3 passing CMD points
    MrcCall->MrcSetMem ((UINT8 *) PassRange, sizeof (PassRange), 0);

    for (CaPiIndex = 0; CaPiIndex < CaPiIndexMax; CaPiIndex++) {
      CaPiCode = CaPiTable[CaPiIndex];
      MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "CMD:%d\t", CaPiCode);
      for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
        for (Channel = 0; Channel < MaxChannel; Channel++) {
          if (!MrcChannelExist (MrcData, Controller, Channel)) {
            continue;
          }
          // Shift the Command PI on both CAA and CAB groups; RankMask is not relevant for CMD/CKE.
          // Keep the value in the host struct.
          ShiftPIforCmdTraining (MrcData, Controller, Channel, MrcIterationCmd, MRC_IGNORE_ARG_8, 0xF, CaPiCode / HighGearRatio, 0);
        }
      }
      MrcCall->MrcSetMem ((UINT8 *) RankPassMask, sizeof (RankPassMask), 0); // Bitmask of passed ranks
      RunEarlyCsCmdTestLpddr (MrcData, RankPassMask); // Run the test on all channels / ranks at the current CS/CMD PIs

      Done = TRUE;
      for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
        for (Channel = 0; Channel < MaxChannel; Channel++) {
          if (!MrcChannelExist (MrcData, Controller, Channel)) {
            continue;
          }
          CaPassIndex = PassRange[Controller][Channel];
          if (RankPassMask[Controller][Channel] == RankMask) {
            PassRange[Controller][Channel]++;
            if (CaPassIndex > LargestPassCount) {
              continue;
            }
            CaPiCodePass[Controller][Channel][CaPassIndex] = CaPiCode;
          }
          if (CaPassIndex < LargestPassCount) {
            Done = FALSE;
          }
        }
      }

      if (Done) {
        break;
      }
    } // for CMD PI
    if (Done) {
      break;
    }
  }  // for CS PI

  if (!Done) {
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_ERROR, "%s: cannot find three consecutive points working starting point for CS/CMD for all channels/ranks\n", gErrString);
    return mrcFail;
  }

  for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
    for (Channel = 0; Channel < MaxChannel; Channel++) {
      if (!MrcChannelExist (MrcData, Controller, Channel)) {
        continue;
      }
      MrcBsort (CaPiCodePass[Controller][Channel], LargestPassCount);  // Sort the array, because the passing range might wrap around
      CaPiCode = CaPiCodePass[Controller][Channel][LargestPassCount / 2];

      if ((Outputs->LowFreqCsCmd2DSweepDone == FALSE) && !Lpddr4) {
        SaveData->LowFCmdPiCode[Controller][Channel] = CaPiCode;
        for (Rank = 0; Rank < MAX_RANK_IN_CHANNEL; Rank++) {
          if (!MrcRankExist (MrcData, Controller, Channel, Rank)) {
            continue;
          }
          // Temporary LowFCtlPiCode used to sweep CS with LowFCmdPiCode fixed in next step
          SaveData->LowFCtlPiCode[Controller][Channel][Rank] = CsPiCode;
        }
      }
      // Shift the Command PI and keep the value in the host struct.
      ShiftPIforCmdTraining (MrcData, Controller, Channel, MrcIterationCmd, MRC_IGNORE_ARG_8, 0xF, CaPiCode / HighGearRatio, 0);
      IntCmdTiming  = &IntOutputs->Controller[Controller].CmdTiming[Channel];
      IntCmdTiming->CmdPiCode[0] = (UINT16) CaPiCode;
      MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Selected values Mc%dC%d: CS = %d, CMD = %d\n", Controller, Channel, CsPiCode, CaPiCode);
    }
  }

#ifdef MRC_DEBUG_PRINT
  for (Rank = 0; Rank < MAX_RANK_IN_CHANNEL; Rank++) {
    RankMaskBit = RankMask >> Rank;
    if (RankMaskBit == 0) {
      break;
    }
    for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
      for (Channel = 0; Channel < MaxChannel; Channel++) {
        MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\tMc%uC%uR%u", Controller, Channel, Rank);
      }
    }
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\t");
  }
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\n");
#endif
  // Now let's find NumCsPassIndex passing CS points at this CMD point
  MrcCall->MrcSetMem ((UINT8 *) PassRange, sizeof (PassRange), 0);
  for (CsPiIndex = 0; CsPiIndex < CsPiIndexMax; CsPiIndex++) {
    CsPiCode = CsPiTable[CsPiIndex];
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "CS: %d\t", CsPiCode);
    for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
      for (Channel = 0; Channel < MaxChannel; Channel++) {
        if (!MrcChannelExist (MrcData, Controller, Channel)) {
          continue;
        }
        // Shift the CS PI on all ranks, keep the value in the host struct.
        ShiftPIforCmdTraining (MrcData, Controller, Channel, MrcIterationCtl, RankMask, MRC_IGNORE_ARG_8, CsPiCode / HighGearRatio, 0);
      }
    }

    MrcCall->MrcSetMem ((UINT8 *) RankPassMask, sizeof (RankPassMask), 0); // Bitmask of passed ranks
    RunEarlyCsCmdTestLpddr (MrcData, RankPassMask); // Run the test on all channels / ranks at the current CS/CMD PIs

    Done = TRUE;
    for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
      for (Channel = 0; Channel < MaxChannel; Channel++) {
        if (!MrcChannelExist (MrcData, Controller, Channel)) {
          continue;
        }
        CsPassIndex = 0;
        if (RankPassMask[Controller][Channel] == RankMask) {
          CsPassIndex = PassRange[Controller][Channel]++;
          if (CsPassIndex > NumCsPassIndex) {
            continue;
          }
          CsPiCodePass[Controller][Channel][CsPassIndex] = CsPiCode;
        }
        if (PassRange[Controller][Channel] < NumCsPassIndex) {
          Done = FALSE;
        }
      }
    }
    if (Done) {
      // All Channels have found NumCsPassIndex number of passing CS points
      break;
    }
  } // for CS PI

  if (!Done) {
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_ERROR, "%s: cannot find a working starting point for CS/CMD for all channels/ranks\n", gErrString);
    return mrcFail;
  }

  for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
    for (Channel = 0; Channel < MaxChannel; Channel++) {
      if (!MrcChannelExist (MrcData, Controller, Channel)) {
        continue;
      }
      MrcBsort (CsPiCodePass[Controller][Channel], ARRAY_COUNT (CsPiCodePass[Controller][Channel]));  // Sort the array, because the passing range might wrap around

      if ((NumCsPassIndex % 2) == 0) { // NumCsPassIndex is even
        CsPiCode = (CsPiCodePass[Controller][Channel][MidCsPassIndex - 1] + CsPiCodePass[Controller][Channel][MidCsPassIndex]) / 2;   // Mid of NumCsPassIndex
      } else {  // NumCsPassIndex is odd
        CsPiCode = CsPiCodePass[Controller][Channel][MidCsPassIndex];  // Mid of NumCsPassIndex
      }
      if (Lpddr5) {
        if (Outputs->LowFreqCsCmd2DSweepDone == FALSE) {
          for (Rank = 0; Rank < MAX_RANK_IN_CHANNEL; Rank++) {
            if (!MrcRankExist (MrcData, Controller, Channel, Rank)) {
              continue;
            }
            SaveData->LowFCtlPiCode[Controller][Channel][Rank] = CsPiCode;
          }
        }
      }
      // Shift the CS PI on all ranks, keep the value in the host struct.
      ShiftPIforCmdTraining (MrcData, Controller, Channel, MrcIterationCtl, RankMask, MRC_IGNORE_ARG_8, CsPiCode / HighGearRatio, 0);
      for (Rank = 0; Rank < MAX_RANK_IN_CHANNEL; Rank++) {
        if (!MrcRankExist (MrcData, Controller, Channel, Rank)) {
          continue;
        }
        IntCmdTiming  = &IntOutputs->Controller[Controller].CmdTiming[Channel];
        IntCmdTiming->CtlPiCode[Rank] = (UINT16) CsPiCode;
        IntCmdTiming->CkePiCode[Rank] = (UINT16) CsPiCode;
      }
      MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Final selected values Mc%dC%d: CS = %d, CMD = %d\n", Controller, Channel, CsPiCode, CaPiCode);
    }
  }

  // Restore DDR IO values that were used for CA training
  LpddrCaTrainingRestoreIo (MrcData);
  // The following are to ensure that the phyinit values are restored
  MrcGetSetChStrb (MrcData, MAX_CONTROLLER, MAX_CHANNEL, MAX_SDRAM_IN_DIMM, GsmIocForceRxAmpOn, WriteToCache, &ForceRxOn);
  MrcGetSetChStrb (MrcData, MAX_CONTROLLER, MAX_CHANNEL, MAX_SDRAM_IN_DIMM, GsmIocTxPiPwrDnDis, WriteToCache, &TxPiOn);
  MrcFlushRegisterCachedData (MrcData);

  return Status;
}

/**
  Sweep CMD PI linearly and find edges for all bytes.
  For LP5 ECT training, JEDEC allows Commands to be sampled at rising edge of CK and falling edge of CK. MR16.CbtPhase = 0 is for the former.
  This function runs the ShortCADB test twice, collects results and changes the MR16.CbtPhase for that channel.
  In the end all channels have to pass both CBT Phases for the CA sweep w.r.t CK to pass.

  @param[in] MrcData - The MRC global data.
  @retval mrcSuccess if succeeded.

**/
MrcStatus
EarlyCaFindEdges (
  IN MrcParameters *const   MrcData,
  UINT32                    Rank,
  UINT16                    Start,
  UINT16                    Stop,
  INT16                     Step,
  UINT8                     CbtPhase,
  UINT16                    LeftLimit[MAX_CONTROLLER][MAX_CHANNEL][MAX_RANK_IN_CHANNEL][MAX_SDRAM_IN_DIMM][MAX_CBT_PHASES],
  UINT16                    RightLimit[MAX_CONTROLLER][MAX_CHANNEL][MAX_RANK_IN_CHANNEL][MAX_SDRAM_IN_DIMM][MAX_CBT_PHASES]
  )
{
  MrcInput          *Inputs;
  MRC_FUNCTION      *MrcCall;
  MrcDebug          *Debug;
  MrcOutput         *Outputs;
  MrcStatus         Status;
  MrcChannelIn      *ChannelIn;
  CADB_LINE         *CadbLines;
  UINT32            Controller;
  UINT32            Channel;
  UINT32            Byte;
  UINT32            CaPattern;
  UINT8             MaxChannel;
  UINT8             McChannelMask;
  UINT8             ByteMask;
  UINT8             DramByte;
  UINT8             ByteDoneMask[MAX_CONTROLLER][MAX_CHANNEL];
  UINT8             McChannelFail;
  UINT8             ByteDoneMaskCheck;
  UINT8             FeedbackMask;
  UINT8             PassCount[MAX_CONTROLLER][MAX_CHANNEL][MAX_SDRAM_IN_DIMM];
  UINT16            PiCode;
  UINT16            LeftLimitValue;
  UINT16            RightLimitValue;
  UINT8             NumOfOnes;
  UINT32            DelayCadb;
  UINT32            DataTrainFeedback;
  INT64             DataTrainFeedbackField;
  BOOLEAN           Done;
  BOOLEAN           IgnoreByte;
  BOOLEAN           BreakOut;
  BOOLEAN           Lpddr4;
  BOOLEAN           Lpddr5;
  char              *BytesHeader;
  char              PassFail;

  Status        = mrcSuccess;
  Inputs        = &MrcData->Inputs;
  MrcCall       = Inputs->Call.Func;
  Outputs       = &MrcData->Outputs;
  Debug         = &Outputs->Debug;
  MaxChannel    = Outputs->MaxChannels;
  Lpddr4        = (Outputs->DdrType == MRC_DDR_TYPE_LPDDR4);
  Lpddr5        = (Outputs->DdrType == MRC_DDR_TYPE_LPDDR5);
  FeedbackMask  = FEEDBACKMASK;

  DelayCadb = (Lpddr4) ? (1 * MRC_TIMER_1US) : (MRC_LP5_tADR_PS / 1000);

  MrcCall->MrcSetMem ((UINT8 *) ByteDoneMask, sizeof (ByteDoneMask), 0);
  MrcCall->MrcSetMem ((UINT8 *) PassCount, sizeof (PassCount), 0);

  PiCode  = Start;
  Done    = FALSE;
  ByteDoneMaskCheck = 0;
  McChannelFail = 0;
  CadbLines = (Lpddr4) ? CadbLinesLp4 : CadbLinesLp5;
  CaPattern = CadbLines[2].CaHigh & CAPATTERNMASK;
  NumOfOnes = MrcCountBitsEqOne (CaPattern);
  if (Lpddr5) {
    NumOfOnes += 1;
  }

  for (Byte = 0; Byte < Outputs->SdramCount; Byte++) {
    ByteDoneMaskCheck |= (1 << Byte);
  }

  McChannelMask = 0;
  for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
    for (Channel = 0; Channel < MaxChannel; Channel++) {
      if (MrcRankExist (MrcData, Controller, Channel, Rank)) {
        McChannelMask |= (1 << ((Controller * MaxChannel) + Channel));
      }
    }
  }

  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\t Mc0Ch0    Mc0Ch1    Mc0Ch2    Mc0Ch3    Mc1Ch0    Mc1Ch1    Mc1Ch2    Mc1Ch3\n");
  BytesHeader = "0   1     ";
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "CMD PI\t %s%s", BytesHeader, BytesHeader);
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "%s%s", BytesHeader, BytesHeader);
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "%s%s", BytesHeader, BytesHeader);
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "%s%s\n", BytesHeader, BytesHeader);

  while (!Done) {
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "%d: \t", PiCode);
    for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
      for (Channel = 0; Channel < MaxChannel; Channel++) {
        if (!MrcRankExist (MrcData, Controller, Channel, Rank)) {
          continue;
        }
        // Shift the Command PI
        ShiftPIforCmdTraining (MrcData, Controller, Channel, MrcIterationCmd, MRC_IGNORE_ARG_8, 0xF, PiCode, 0);
      }
    }
    IoReset (MrcData);
    // Run CADB pattern on selected controller/channels at the same time
    ShortRunCADB (MrcData, McChannelMask);
    MrcWait (MrcData, DelayCadb);

    // Read and process the results
    for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
      for (Channel = 0; Channel < MaxChannel; Channel++) {
        if (!MrcRankExist (MrcData, Controller, Channel, Rank)) {
          ByteDoneMask[Controller][Channel] = ByteDoneMaskCheck;
          MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "          ");
          continue;
        }
        if (ByteDoneMask[Controller][Channel] == ByteDoneMaskCheck) { // All bytes failed on this channel, no need to sweep anymore
          MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "          ");
          for (Byte = 0; Byte < Outputs->SdramCount; Byte++) {
            if (PassCount[Controller][Channel][Byte] == 0) { // Each channel must pass atleast for one CmdPi value
              McChannelFail |= (1 << ((Controller * MaxChannel) + Channel));  //Track if any channels failed in the Controller
            }
          }
          continue;
        }
        ChannelIn = &Inputs->Controller[Controller].Channel[Channel];
        for (Byte = 0; Byte < Outputs->SdramCount; Byte++) {
          ByteMask = (1 << Byte);
          DramByte = ChannelIn->DqsMapCpu2Dram[dDIMM0][Byte]; // Lpddr only uses DIMM0 entry
          IgnoreByte = FALSE;
          if (!MrcByteExist (MrcData, Controller, Channel, Byte)) {
            IgnoreByte = TRUE;
          }
          if (Lpddr4) {
            if (((DramByte & 1) == 0) && !Outputs->LpByteMode) {
              // Ignore DRAM bytes 0, 2, 4 and 6 - they don't provide feedback
              // Byte mode parts provide feedback on all bytes
              IgnoreByte = TRUE;
              PassCount[Controller][Channel][Byte] = 1; // Set a non zero value if the byte is ignored
            }
          } else {
            if ((DramByte & 1) == 1) { // Ignore DRAM bytes 1, 3, 5 and 7 - they don't provide feedback
              IgnoreByte = TRUE;
              PassCount[Controller][Channel][Byte] = 1; // Set a non zero value if the byte is ignored
            }
          }
          if (IgnoreByte) {
            ByteDoneMask[Controller][Channel] |= ByteMask;
            MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "    ");
            continue;
          }
          MrcGetSetChStrb (MrcData, Controller, Channel, Byte, GsmIocDataTrainFeedback, ReadUncached, &DataTrainFeedbackField);
          DataTrainFeedback = (UINT32) DataTrainFeedbackField & FeedbackMask;  // Get only DQ bits, not DQS
          PassFail = '#';
          if ((ByteDoneMask[Controller][Channel] & ByteMask) == 0) {
            LeftLimitValue = LeftLimit[Controller][Channel][Rank][Byte][CbtPhase];
            RightLimitValue = RightLimit[Controller][Channel][Rank][Byte][CbtPhase];
            // If we don't see 4 ones in the byte for Lp5 and 3 ones in a byte for Lp4, then the command was not aligned properly
            if (MrcCountBitsEqOne (DataTrainFeedback) != NumOfOnes) {
              if (LeftLimitValue == RightLimitValue) {
                LeftLimitValue = RightLimitValue = PiCode;
              } else {
                RightLimitValue = PiCode;
                if ((RightLimitValue - (3 * Step)) > LeftLimitValue) {
                  ByteDoneMask[Controller][Channel] |= ByteMask;
                }
              }
            } else {
              PassFail = '.';
              PassCount[Controller][Channel][Byte] += 1;
              RightLimitValue = PiCode;
            }
            LeftLimit[Controller][Channel][Rank][Byte][CbtPhase] = LeftLimitValue;
            RightLimit[Controller][Channel][Rank][Byte][CbtPhase] = RightLimitValue;
          }
          MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "%c%02X ", PassFail, DataTrainFeedback);
        }  // for Byte
        MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "  ");
      }  // for Channel
      MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "  ");
    }  // for Controller
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\n");

    BreakOut = TRUE;
    for (Controller = 0; Controller < MAX_CONTROLLER && BreakOut; Controller++) {
      for (Channel = 0; Channel < MaxChannel && BreakOut; Channel++) {
        if (ByteDoneMask[Controller][Channel] != ByteDoneMaskCheck) {
          BreakOut = FALSE;
        }
      }
    }
    if (BreakOut) {
      // Found the limit on all bytes on all channels on all mc - no need to sweep Pi any longer
      break;
    }

    PiCode += Step;
    if (Step > 0) {
      // Sweep up
      Done = (PiCode > Stop);
    } else {
      // Sweep down
      Done = (((INT16) PiCode) < Stop);
    }
  }  // while not done
  if (((McChannelFail & McChannelMask) != 0) && !Inputs->ExitOnFailure) {
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\nERROR:One or more channels could not find a CMD eye\n");
    Status = mrcFail;
  }
  return Status;
}

/**
  Process the results of the early LPDDR3/LPDDR4 CMD training and find the best PI settings for CAA/CAB.
  Flow:
   1. Find the worst case Right and Left limits for each channel
   2. Find the Center for each channel

  @param[in]  MrcData     - The MRC global data.
  @param[in]  MaxValue    - Value to start RightLimit array at
  @param[in]  LeftLimit   - array of left edge values per channel, rank and CPU byte
  @param[in]  RightLimit  - array of right edge values per channel, rank and CPU byte
  @param[out] BestCs      - array of best CMD PI settings, per channel

  @retval mrcSuccess if succeeded
**/
MrcStatus
FindBestCmdPi (
  IN  MrcParameters *const  MrcData,
  IN  UINT16                MaxValue,
  IN  UINT16                LeftLimit[MAX_CONTROLLER][MAX_CHANNEL][MAX_RANK_IN_CHANNEL][MAX_SDRAM_IN_DIMM][MAX_CBT_PHASES],
  IN  UINT16                RightLimit[MAX_CONTROLLER][MAX_CHANNEL][MAX_RANK_IN_CHANNEL][MAX_SDRAM_IN_DIMM][MAX_CBT_PHASES],
  OUT UINT16                BestCmd[MAX_CONTROLLER][MAX_CHANNEL]
  )
{
  MrcInput          *Inputs;
  MRC_FUNCTION      *MrcCall;
  MrcDebug          *Debug;
  MrcOutput         *Outputs;
  MrcChannelIn      *ChannelIn;
  MrcStatus         Status;
  BOOLEAN           Lpddr4;
  BOOLEAN           Lpddr5;
  BOOLEAN           Dimm;
  UINT32            Controller;
  UINT32            Channel;
  UINT32            Rank;
  UINT8             MaxChannel;
  UINT8             Byte;
  UINT8             DramByte;
  UINT16            CmdLeftLimit[MAX_CONTROLLER][MAX_CHANNEL];
  UINT16            CmdRightLimit[MAX_CONTROLLER][MAX_CHANNEL];

  Inputs        = &MrcData->Inputs;
  MrcCall       = Inputs->Call.Func;
  Outputs       = &MrcData->Outputs;
  Debug         = &Outputs->Debug;
  MaxChannel    = Outputs->MaxChannels;
  Lpddr4        = (Outputs->DdrType == MRC_DDR_TYPE_LPDDR4);
  Lpddr5        = (Outputs->DdrType == MRC_DDR_TYPE_LPDDR5);

  Status = mrcSuccess;

  MrcCall->MrcSetMemWord ((UINT16 *) CmdRightLimit, sizeof (CmdRightLimit) / sizeof (UINT16), MaxValue);
  MrcCall->MrcSetMemWord ((UINT16 *) CmdLeftLimit, sizeof (CmdLeftLimit) / sizeof (UINT16), 0);

  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Finding best CMD PIs:\n");
  for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
    for (Channel = 0; Channel < MaxChannel; Channel++) {
      if (!MrcChannelExist (MrcData, Controller, Channel)) {
        continue;
      }
      MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Mc%dC%d\tLeft\tRight\tCenter\n", Controller, Channel);
      for (Rank = 0; Rank < MAX_RANK_IN_CHANNEL; Rank++) {
        if (!MrcRankExist (MrcData, Controller, Channel, Rank)) {
          continue;
        }
        ChannelIn = &Inputs->Controller[Controller].Channel[Channel];
        Dimm = (BOOLEAN) RANK_TO_DIMM_NUMBER (Rank);
        // Find the worst case Right and Left limits for all ranks, for bytes from the particular CA group
        for (Byte = 0; Byte < Outputs->SdramCount; Byte++) {
          DramByte = ChannelIn->DqsMapCpu2Dram[Dimm][Byte];
          if (!Outputs->LpByteMode) {
            if (Lpddr4) {
              if ((DramByte & 1) == 0) {  // Ignore DRAM even bytes.
                continue;
              }
            } else {
              if ((DramByte & 1) == 1) {  // Ignore DRAM odd bytes.
                continue;
              }
            }
          }

          //  @todo_adl: Lp5 dram will have feedback on all bytes , we should not skip it during training or result processing
          if (Lpddr5 && ((DramByte & 1) == 1)) {  // Ignore DRAM odd bytes.
            continue;
          }
          CmdRightLimit[Controller][Channel] = MIN (CmdRightLimit[Controller][Channel], RightLimit[Controller][Channel][Rank][Byte][0]);
          CmdLeftLimit[Controller][Channel]  = MAX (CmdLeftLimit[Controller][Channel], LeftLimit[Controller][Channel][Rank][Byte][0]);
        }
      } // for Rank
      // Find the Center for each group, worst case of all ranks
      BestCmd[Controller][Channel] = (CmdRightLimit[Controller][Channel] + CmdLeftLimit[Controller][Channel]) / 2;

      MRC_DEBUG_MSG (
        Debug,
        MSG_LEVEL_NOTE,
        "CAA\t\t\t\t%d\t\t%d\t\t%d\n",
        CmdLeftLimit[Controller][Channel],
        CmdRightLimit[Controller][Channel],
        BestCmd[Controller][Channel]
      );
    } // for Channel
  } // for Controller

  return Status;
}



/**
  Print final results of CPU-to-DRAM DQ mapping.

  @param[in] MrcData - The MRC global data.

  @retval mrcSuccess if succeeded
**/
VOID
MrcPrintDqMapResults (
  IN MrcParameters *const MrcData
  )
{
#ifdef MRC_DEBUG_PRINT
  MrcOutput *Outputs;
  MrcDebug  *Debug;
  UINT32    Controller;
  UINT32    Channel;
  UINT8     Byte;
  UINT8     Bit;
  UINT8     DramBit;

  Outputs = &MrcData->Outputs;
  Debug = &Outputs->Debug;


  for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
    for (Channel = 0; Channel < Outputs->MaxChannels; Channel++) {
      if (!MrcRankExist (MrcData, Controller, Channel, 0)) {
        continue;
      }
      MRC_DEBUG_MSG (
        Debug,
        MSG_LEVEL_NOTE,
        "\nMapCA2DQPins Results for Mc%d.Ch%d (\"-1\" for skipped Bytes, DRAM DQ pins offsets):\n",
        Controller,
        Channel
        );
      MRC_DEBUG_MSG (
        Debug,
        MSG_LEVEL_NOTE,
        "CPU Bit: \t[0]\t[1]\t[2]\t[3]\t[4]\t[5]\t[6]\t[7]\n"
        );
      for (Byte = 0; Byte < Outputs->SdramCount; Byte++) {
        MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "CPU Byte%d:", Byte);
        for (Bit = 0; Bit <  MAX_BITS; Bit++) {
          DramBit = MrcData->Inputs.Controller[Controller].Channel[Channel].DqMapCpu2Dram[Byte][Bit];
          MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\t%d", (DramBit == 255) ? -1 : DramBit);
        }
        MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\n");
      }
    }
  }
#endif //MRC_DEBUG_PRINT
}

/**
  Sweep the given PI up or down and find the edge.

  @param[in]  MrcData     - The MRC global data.
  @param[in]  Iteration   - Determines which PI to shift
  @param[in]  McChBitMask - Valid Channel bit mask flat across all controllers.
  @param[in]  RankMask    - Valid Rank bit mask
  @param[in]  GroupMask   - Valid Group bit mask
  @param[in]  Stop        - End of the PI range
  @param[in]  Step        - PI step for the sweep
  @param[out] Limit       - array of edge values (per channel), filled by this function
  @param[in]  DebugPrint  - Print debug messages or not

**/
void
CaFindEdge (
  IN  MrcParameters *const   MrcData,
  IN  UINT8                  Iteration,
  IN  UINT8                  McChBitMask,
  IN  UINT8                  RankMask,
  IN  UINT8                  GroupMask,
  IN  INT16                  Stop[MAX_CONTROLLER][MAX_CHANNEL],
  IN  INT16                  Step,
  OUT UINT8                  Limit[MAX_CONTROLLER][MAX_CHANNEL],
  IN  BOOLEAN                DebugPrint
  )
{
  const MRC_FUNCTION  *MrcCall;
  MrcDebug            *Debug;
  MrcOutput           *Outputs;
  UINT32              Controller;
  UINT32              Channel;
  INT16               PiOffset;
  UINT8               McChBit;
  UINT8               McChError;
  UINT8               MaxChannels;
  BOOLEAN             Pass;
  BOOLEAN             Done;
  BOOLEAN             RightLimitNotReached;
  BOOLEAN             LeftLimitNotReached;
  BOOLEAN             LimitDone;
  BOOLEAN             ChannelDone[MAX_CONTROLLER][MAX_CHANNEL];
#ifdef MRC_DEBUG_PRINT
  MrcDebugMsgLevel    DbgLvl;

  DbgLvl = (DebugPrint) ? MSG_LEVEL_NOTE : MSG_LEVEL_NEVER;
#endif

  Outputs     = &MrcData->Outputs;
  Debug       = &Outputs->Debug;
  MaxChannels = Outputs->MaxChannels;
  MrcCall     = MrcData->Inputs.Call.Func;
  PiOffset    = 0;
  Done        = FALSE;
  RightLimitNotReached = TRUE;
  LeftLimitNotReached = TRUE;
  MrcCall->MrcSetMem ((UINT8 *) ChannelDone, ARRAY_COUNT2D (ChannelDone), FALSE);

  MRC_DEBUG_MSG (Debug, DbgLvl, "\t0 1\n");
  while (!Done) {
    MRC_DEBUG_MSG (Debug, DbgLvl, "%d:\t", PiOffset);
    // Update Timing
    for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
      for (Channel = 0; Channel < MaxChannels; Channel++) {
        if (MC_CH_MASK_CHECK (McChBitMask, Controller, Channel, MaxChannels)) {
          if (!ChannelDone[Controller][Channel]) {
            ShiftPIforCmdTraining (MrcData, Controller, Channel, Iteration, RankMask, GroupMask, PiOffset, 0);
          }
        }
      }
    }
    // Reset DDR after changing the CLK PI
    MrcResetSequence (MrcData);

    // Run CPGC test on both channels
    McChError = RunIOTest (MrcData, McChBitMask, Outputs->DQPat, 1, 0);
    for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
      for (Channel = 0; Channel < MaxChannels; Channel++) {
        McChBit = MC_CH_MASK_CHECK (McChBitMask, Controller, Channel, MaxChannels);
        if (((McChBit) == 0) || (ChannelDone[Controller][Channel])) {
          MRC_DEBUG_MSG (Debug, DbgLvl, (Channel == 0) ? "  " : "");
          continue;
        }

        Pass = !(McChError & McChBit);

        MRC_DEBUG_MSG (Debug, DbgLvl, (Pass ? ". " : "# "));

        RightLimitNotReached = (PiOffset < Stop[Controller][Channel]) && (PiOffset >= 0); //Sweep up for positive PiOffset
        LeftLimitNotReached = (PiOffset > Stop[Controller][Channel]) && (PiOffset <= 0);  //Sweep down for negative PiOffset
        if ((Pass) && (RightLimitNotReached || LeftLimitNotReached)) {
          Limit[Controller][Channel] = (UINT8) (ABS (PiOffset));
        } else {
          ChannelDone[Controller][Channel] = TRUE;
        }
      } // Channel
    } // Controller
    MRC_DEBUG_MSG (Debug, DbgLvl, "\n");
    PiOffset += Step;

    // Check if we have finished all channels
    LimitDone = TRUE;
    for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
      for (Channel = 0; Channel < MaxChannels; Channel++) {
        if (MC_CH_MASK_CHECK (McChBitMask, Controller, Channel, MaxChannels)) {
          if (ChannelDone[Controller][Channel] == FALSE) {
            LimitDone = FALSE;
          }
        }
      }
    }
    // If we found all the edges, break out of the while loop
    if (LimitDone) {
      Done = TRUE;
      for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
        for (Channel = 0; Channel < MaxChannels; Channel++) {
          if (MC_CH_MASK_CHECK (McChBitMask, Controller, Channel, MaxChannels)) {
            MRC_DEBUG_MSG (Debug, DbgLvl, "MC.%dC.%d Limit = %d\n", Controller, Channel, Limit[Controller][Channel]);
          }
        }
      }
    }
  }  // while not done
}

/**
  This function calculate High and Low sweep borders for a given paramter.
  This function is now used for LPDDR5 only
  @param[in]  MrcData   - Pointer to global data.
  @param[in]  Group     - DDRIO group to access.
  @param[in]  Value     - Current trained value.
  @param[out] PiHigh    - Return pointer for High limit.
  @param[out] PiLow     - Return pointer for Low limit.

**/
void
MrcUpdateRange (
  IN  MrcParameters *const MrcData,
  IN  GSM_GT  const   Group,
  IN  INT64   Value,
  OUT INT16   *PiLow,
  OUT INT16   *PiHigh
  )
{
  INT64              Min;
  INT64              Max;
  MrcGetSetLimits (MrcData, Group, &Min, &Max, NULL);
  if ((Value + (INT64)(*PiHigh)) > Max) {
    *PiHigh = (INT16)(Max - Value);
  }
  if ((Value + (INT64)(*PiLow)) < Min) {
    *PiLow = (INT16)(Min - Value);
  }
}

/**
  This function calculate global High and Low borders for sweep range based in limits for WCK, CLK, RecEn and TxDq.
  This function is used for LPDDR5 only

  @param[in]  MrcData   - Pointer to global data.
  @param[out] PiHigh    - Return pointer for High limit.
  @param[out] PiLow     - Return pointer for Low limit.
  @param[in] Print      - flag to enable debug print.
**/
void
CalculateMaxSweepRange (
  IN  MrcParameters *const MrcData,
  OUT INT16                PiLow[MAX_CONTROLLER][MAX_CHANNEL],
  OUT INT16                PiHigh[MAX_CONTROLLER][MAX_CHANNEL],
  BOOLEAN Print
  )
{
  MrcDebug            *Debug;
  MrcOutput           *Outputs;
  UINT32              Controller;
  UINT32              Channel;
  UINT32              Rank;
  UINT8               Byte;
  INT64               WckDelayValue;
  INT64               ClkDelayValue;
  INT64               RcvEnDelayValue;
  INT64               TxDqDelayValue;
  Outputs    = &MrcData->Outputs;
  Debug      = Print ? &Outputs->Debug : NULL;

  for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
    for (Channel = 0; Channel < MAX_CHANNEL; Channel++) {
      MrcGetSetCcc (MrcData, Controller, Channel, MRC_IGNORE_ARG, 0, WckGrpPi, ReadFromCache, &WckDelayValue);
      MrcUpdateRange (MrcData, WckGrpPi, WckDelayValue, &PiLow[Controller][Channel], &PiHigh[Controller][Channel]);

      for (Rank = 0; Rank < MAX_RANK_IN_CHANNEL; Rank++) {
        if (MrcRankExist (MrcData, Controller, Channel, Rank)) {
          MrcGetSetCcc (MrcData, Controller, Channel, MRC_IGNORE_ARG, 0, ClkGrpPi, ReadFromCache, &ClkDelayValue);
          MrcUpdateRange (MrcData, ClkGrpPi, ClkDelayValue, &PiLow[Controller][Channel], &PiHigh[Controller][Channel]);

          for (Byte = 0; Byte < Outputs->SdramCount; Byte++) {
            MrcGetSetStrobe (MrcData, Controller, Channel, Rank, Byte, RecEnDelay, ReadFromCache, &RcvEnDelayValue);
            MrcGetSetStrobe (MrcData, Controller, Channel, Rank, Byte, TxDqDelay, ReadFromCache, &TxDqDelayValue);
            MrcUpdateRange (MrcData, RecEnDelay, RcvEnDelayValue, &PiLow[Controller][Channel], &PiHigh[Controller][Channel]);
            MrcUpdateRange (MrcData, TxDqDelay, TxDqDelayValue, &PiLow[Controller][Channel], &PiHigh[Controller][Channel]);
          } //Byte
        }
      } //Rank
      MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Mc.%d.C:%d Sweep range [%d;%d]\n", Controller, Channel, PiLow[Controller][Channel], PiHigh[Controller][Channel]);
    } // channel
  } // Controller
}

/**
  Sweep right and left from the current point to find the margins.

  @param[in, out] MrcData     - Include all MRC global data.
  @param[in]      Iteration   - Determines which PI to shift
  @param[in]      McChBitMask - Valid Channel bit mask flat across all controllers.
  @param[in]      RankMask    - Valid Rank bit mask
  @param[in]      GroupMask   - Valid Group bit mask
  @param[in]      DebugPrint  - Print debug messages or not

  @retval MrcStatus -  If it succeeds return mrcSuccess
**/
void
CmdLinearFindEdgesLpddr (
  IN OUT MrcParameters *const MrcData,
  IN     UINT8                Iteration,
  IN     UINT8                McChBitMask,
  IN     UINT8                RankMask,
  IN     UINT8                GroupMask,
  IN     BOOLEAN              DebugPrint
  )
{
  const MrcInput    *Inputs;
  const MRC_FUNCTION *MrcCall;
  MrcOutput         *Outputs;
  UINT32            Controller;
  UINT32            Channel;
  UINT32            Rank;
  INT16             PiStep;
  UINT8             MaxChannels;
  UINT8             RightLimit[MAX_CONTROLLER][MAX_CHANNEL];
  UINT8             LeftLimit[MAX_CONTROLLER][MAX_CHANNEL];
  INT16             PiHigh[MAX_CONTROLLER][MAX_CHANNEL];
  INT16             PiLow[MAX_CONTROLLER][MAX_CHANNEL];
  BOOLEAN           ClockIteration;

  Inputs  = &MrcData->Inputs;
  MrcCall = Inputs->Call.Func;
  Outputs = &MrcData->Outputs;
  MaxChannels = Outputs->MaxChannels;
  ClockIteration = (Iteration == MrcIterationClock);

  // We are going to sweep clock 64 PI ticks to the left and to the right
  //@todo adjustment needed for extra sweep range of TGL and Gear2
  for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
    for (Channel = 0; Channel < Outputs->MaxChannels; Channel++) {
      if (ClockIteration && (MrcData->Outputs.DdrType == MRC_DDR_TYPE_LPDDR5)) {
        PiLow[Controller][Channel] = PI_LOW_LP5;
        PiHigh[Controller][Channel] = PI_HIGH_LP5;
      } else {
        PiLow[Controller][Channel] = PI_LOW_LP4;
        PiHigh[Controller][Channel] = PI_HIGH_LP4;
      }
    }
  }
  if (ClockIteration && (MrcData->Outputs.DdrType == MRC_DDR_TYPE_LPDDR5)) {
    CalculateMaxSweepRange (MrcData, PiLow, PiHigh, TRUE);
  }

  PiStep = 1;


  // Initialize to zero margin
  MrcCall->MrcSetMem ((UINT8 *) RightLimit, sizeof (RightLimit), 0);
  MrcCall->MrcSetMem ((UINT8 *) LeftLimit, sizeof (LeftLimit), 0);

  // Find right and left margins
  CaFindEdge (MrcData, Iteration, McChBitMask, RankMask, GroupMask, PiHigh, PiStep, ClockIteration ? RightLimit : LeftLimit, DebugPrint);
  CaFindEdge (MrcData, Iteration, McChBitMask, RankMask, GroupMask, PiLow, -PiStep, ClockIteration ? LeftLimit : RightLimit, DebugPrint);

  for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
    for (Channel = 0; Channel < MaxChannels; Channel++) {
      if (MC_CH_MASK_CHECK (McChBitMask, Controller, Channel, MaxChannels)) {
        // Save margins for RMT
        for (Rank = 0; Rank < MAX_RANK_IN_CHANNEL; Rank++) {
          if ((1 << Rank) & RankMask) {
            Outputs->MarginResult[LastCmdT][Rank][Controller][Channel][0][0] = 10 * LeftLimit[Controller][Channel];
            Outputs->MarginResult[LastCmdT][Rank][Controller][Channel][0][1] = 10 * RightLimit[Controller][Channel];
          }
        }
      }
    }
  }
}

/**
  Early CMD / CLK training for LPDDR3 / LPDDR4.
  Main LPDDR4 flow:
  For Rank:
    1) IF Rank != Terminating:
      a) Enable Termination on the terminated rank via FSP-High
    2) Enter CBT Mode for this Rank.
    3) Drive Rank CKE Low: DIMM switches to FSP-High (1), wait tCKELCK
    4) PLL Frequency switch from low to High
    5) Run Training on all channels / subch in parallel, find passing CA region per ch/subch.
    6) IF Rank != Terminating:
      a) Disable Termination on the terminated rank via FSP-Low
    7) PLL Frequency Switch from High to Low
    8) Drive CKE High: DIMM switches to FSP Low (0)
    9) Exit CBT Mode (In Low Frequency)
    10) Set Vref MR to FSP-High.

  Lp5 Flow :
    Same as above except
    3) Drive DQ7 high to switch to FSP high. LpddrCommandTrainingMode () takes care of it.
    5) Run training on all channels in parallel per CbtPhase and average the PiLimit from both the phases.
    8) Drive DQ7 low


  @param[in] MrcData - The MRC global data.

  @retval mrcSuccess if succeeded
**/
MrcStatus
EarlyCaTraining (
  IN MrcParameters *const MrcData
  )
{
  MrcInput              *Inputs;
  MrcDebug              *Debug;
  MRC_FUNCTION          *MrcCall;
  MrcOutput             *Outputs;
  MrcIntOutput          *IntOutputs;
  MrcIntCmdTimingOut    *IntCmdTiming;
  MrcStatus             Status;
  CADB_LINE             *CadbLines;
  MrcDdrType            DdrType;
  INT64                 Min;
  INT64                 Max;
  INT64                 ForceRxOn;
  INT64                 TxPiOn;
  UINT32                CadbCount;
  UINT32                *PatBuf;
  UINT32                Controller;
  UINT32                Channel;
  UINT32                Rank;
  UINT32                WckControlSave;
  UINT32                WckControl1Save;
  UINT32                DataRcompDataSave[MAX_CONTROLLER][MAX_SDRAM_IN_DIMM];
  UINT32                FirstController;
  UINT32                FirstChannel;
  UINT8                 MaxChannel;
  UINT8                 CbtPhase;
  UINT8                 McChannelMask;
  UINT8                 RankBit;
  UINT8                 RankMask;
  UINT8                 TermRankMask;
  UINT16                PiLow;
  UINT16                PiHigh;
  UINT16                PiStep;
  UINT16                RightLimit[MAX_CONTROLLER][MAX_CHANNEL][MAX_RANK_IN_CHANNEL][MAX_SDRAM_IN_DIMM][MAX_CBT_PHASES];
  UINT16                LeftLimit[MAX_CONTROLLER][MAX_CHANNEL][MAX_RANK_IN_CHANNEL][MAX_SDRAM_IN_DIMM][MAX_CBT_PHASES];
  UINT16                BestCmd[MAX_CONTROLLER][MAX_CHANNEL]; // per Channel and per group (CAA and CAB)
  BOOLEAN               Lpddr4;
  MrcChannelIn          *ChannelIn;
  UINT16                LeftPhase0;
  //UINT16                LeftPhase1;
  UINT16                RightPhase0;
  //UINT16                RightPhase1;
  UINT16                Left;
  UINT16                Right;
  UINT8                 Byte;
  UINT8                 DramByte;
  UINT8                 HighGearRatio;
  UINT8                 CurrGear;

  Inputs           = &MrcData->Inputs;
  MrcCall          = Inputs->Call.Func;
  Outputs          = &MrcData->Outputs;
  Debug            = &Outputs->Debug;
  IntOutputs       = (MrcIntOutput *) (MrcData->IntOutputs.Internal);
  TermRankMask     = Inputs->CmdRanksTerminated;
  MaxChannel       = Outputs->MaxChannels;
  DdrType          = Outputs->DdrType;
  Lpddr4           = (DdrType == MRC_DDR_TYPE_LPDDR4);
  CurrGear         = (Outputs->Gear4 ? 4 : (Outputs->Gear2 ? 2 : 1));
  HighGearRatio    =  Outputs->HighGear / CurrGear;

  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "EarlyCaTraining started\n");

  Status = mrcSuccess;
  RankMask  = Outputs->ValidRankMask;

  MrcGetSetLimits (MrcData, CmdGrpPi, &Min, &Max, NULL);
  PiLow     = (UINT16) Min;
  PiHigh    = (UINT16) Max;
  PiStep = (PiHigh + 1) / 64;



  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "PiLow = %d, PiHigh = %d, PiStep = %d\n", PiLow, PiHigh, PiStep);
  MrcCall->MrcSetMemWord ((UINT16 *) RightLimit, sizeof (RightLimit) / sizeof (UINT16), PiLow);
  MrcCall->MrcSetMemWord ((UINT16 *) LeftLimit, sizeof (LeftLimit) / sizeof (UINT16), PiLow);

  // Init DDR IO for CA training
  CadbLines = Lpddr4 ? CadbLinesLp4 : CadbLinesLp5;
  CadbCount = Lpddr4 ? ARRAY_COUNT (CadbLinesLp4) : ARRAY_COUNT (CadbLinesLp5);
  PatBuf    = Lpddr4 ? CadbMuxLp4Pattern2 : CadbMuxLp5RisingPattern2;

  // Read the first populated channel
  FirstController = (MrcControllerExist (MrcData, cCONTROLLER0)) ? 0 : 1;
  FirstChannel = Outputs->Controller[FirstController].FirstPopCh;
  // Following fields are from datacontrol regs, channel is equivalent to controller for offset calc.
  // But these enums go through MrcTranslateSystemToIp translates channel and strobe to corresponding memtech
  // Read the initialized value of ForceRxAmpOn and TxPwrDnDisable for first populated byte
  MrcGetSetChStrb (MrcData, FirstController, FirstChannel, 0, GsmIocForceRxAmpOn, ReadFromCache, &ForceRxOn);
  MrcGetSetChStrb (MrcData, FirstController, FirstChannel, 0, GsmIocTxPiPwrDnDis, ReadFromCache, &TxPiOn);

  MrcFlushRegisterCachedData (MrcData);
  LpddrCaTrainingInitIo (MrcData);

  for (Rank = 0; Rank < MAX_RANK_IN_CHANNEL; Rank++) {
    RankBit = 1 << Rank;
    if ((RankBit & RankMask) == 0) {
      continue;
    }
    if ((RankBit & TermRankMask) == 0) {
      // Enable Termination on the terminated rank via FSP-High
      Status = MrcLpddrSetCbtCaOdtEn (MrcData, TRUE);
      if (Status != mrcSuccess) {
        return Status;
      }
    }


    McChannelMask = 0;     // Bitmask of Controller channels that have current rank populated
    for (CbtPhase = 0; CbtPhase < MAX_CBT_PHASES; CbtPhase++) {
      if (Lpddr4 && (CbtPhase > 0)) { // Run this loop only once for Lp4
        continue;
      }
      for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
        for (Channel = 0; Channel < MaxChannel; Channel++) {
          if (!MrcRankExist (MrcData, Controller, Channel, Rank)) {
            continue;
          }
          McChannelMask |= (1 << ((Controller * MaxChannel) + Channel));
        }  // for Channel
      }
      // Setup and Enter CBT Mode for this Rank.
      if (!Lpddr4) {
        PatBuf = (CbtPhase == 0) ? CadbMuxLp5RisingPattern2 : CadbMuxLp5FallingPattern2;
      }
      SetupCaTrainingCadb (MrcData, Rank, CadbLines, CadbCount, PatBuf, UniseqModePatBuffer);
      Status = LpddrCommandTrainingMode (MrcData, McChannelMask, RankBit, 1, CbtPhase, &WckControlSave, &WckControl1Save, DataRcompDataSave);
      if (Status != mrcSuccess) {
        return Status;
      }

      if (Lpddr4) {
        // Send VrefCA on DQ[6:0]
        MrcSendCaVrefOnDq (MrcData, Rank);
        MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "R%d: MrcSendCaVrefOnDq done\n", Rank);

      }

      // Sweep CMD PI linearly, on all channels at the same time
      Status = EarlyCaFindEdges (MrcData, Rank, PiLow, PiHigh, PiStep, CbtPhase, LeftLimit, RightLimit);
      if (Status != mrcSuccess) {
        return Status;
      }

      // Exit CA training mode on the current rank
      Status = LpddrCommandTrainingMode (MrcData, McChannelMask, RankBit, 0, CbtPhase, &WckControlSave, &WckControl1Save, DataRcompDataSave);
      if (Status != mrcSuccess) {
        return Status;
      }
    } // CbtPhase
    for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
      for (Channel = 0; Channel < MaxChannel; Channel++) {
        if (!MrcRankExist (MrcData, Controller, Channel, Rank)) {
          continue;
        }
        ChannelIn = &Inputs->Controller[Controller].Channel[Channel];
        MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "CA training data Mc%d Ch%d Rank%d\nCPU Byte\tLeft\tRight\tWidth\tCbtPhase\n", Controller, Channel, Rank, CbtPhase);
        for (Byte = 0; Byte < Outputs->SdramCount; Byte++) {
          if (!MrcByteExist (MrcData, Controller, Channel, Byte)) {
            continue;
          }

          DramByte = ChannelIn->DqsMapCpu2Dram[dDIMM0][Byte]; // Lpddr only uses DIMM0 entry
          if (Lpddr4) {
            if ((DramByte & 1) == 0) {  // Ignore DRAM even bytes.
              continue;
            }
          } else {
            if ((DramByte & 1) == 1) {  // Ignore DRAM odd bytes.
              continue;
            }
          }
          LeftPhase0 = LeftLimit[Controller][Channel][Rank][Byte][0];
          RightPhase0 = RightLimit[Controller][Channel][Rank][Byte][0];
          //LeftPhase1 = LeftLimit[Controller][Channel][Rank][Byte][1];
          //RightPhase1 = RightLimit[Controller][Channel][Rank][Byte][1];
          //if (Lpddr4) {
            Left  = LeftPhase0;
            Right = RightPhase0;
          //} else {
            // Larger Pi value for Left and smaller Pi value for the right
            //Left = MAX (LeftPhase0, LeftPhase1);
            //Right = MIN (RightPhase0, RightPhase1);
          //}
          MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "%d\t\t%d\t%d\t%d\n", Byte, Left, Right, Right - Left);
          // average of PiLimits for both phases are stored in [CbtPhase] = 0
          LeftLimit[Controller][Channel][Rank][Byte][0] = Left;
          RightLimit[Controller][Channel][Rank][Byte][0] = Right;
        } // for Byte
      } // for Channel
    } // for Controller

    if ((RankBit & TermRankMask) == 0) {
      // Disable Termination on the terminated rank via FSP-High
      Status = MrcLpddrSetCbtCaOdtEn (MrcData, FALSE);
    }

  }  // for Rank
  // Restore DDR IO values that were used for CA training
  LpddrCaTrainingRestoreIo (MrcData);

  // The following are to ensure that the phyinit values are restored
  MrcGetSetChStrb (MrcData, MAX_CONTROLLER, MAX_CHANNEL, MAX_SDRAM_IN_DIMM, GsmIocForceRxAmpOn, WriteToCache, &ForceRxOn);
  MrcGetSetChStrb (MrcData, MAX_CONTROLLER, MAX_CHANNEL, MAX_SDRAM_IN_DIMM, GsmIocTxPiPwrDnDis, WriteToCache, &TxPiOn);
  MrcFlushRegisterCachedData (MrcData);

  // Select optimal CMD timings for both channels
  FindBestCmdPi (MrcData, PiHigh, LeftLimit, RightLimit, BestCmd);

  // Apply the new CMD PI settings
  for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
    for (Channel = 0; Channel < MaxChannel; Channel++) {
      if (!(MrcChannelExist (MrcData, Controller, Channel))) {
        continue;
      }
      // CAA is controlled by CmdGrp0 & CmdGrp1
      // Devide PiCodes by GearRatio before programming since 1-D sweep PiCodes may be from higher gear.
      ShiftPIforCmdTraining (MrcData, Controller, Channel, MrcIterationCmd, MRC_IGNORE_ARG_8, 0xF, BestCmd[Controller][Channel] / HighGearRatio, 0);
      IntCmdTiming  = &IntOutputs->Controller[Controller].CmdTiming[Channel];
      IntCmdTiming->CmdPiCode[0] = BestCmd[Controller][Channel];
    } // for Channel
  } // for Controller

  return Status;
}

/**
  Sweep CS Pi up and find edges for all bytes.

  @param[in]  MrcData - The MRC global data.
  @param[out] Limit   - array of edge PI values per channel, rank and CPU byte

**/
void
ChipSelectFindEdge (
  IN MrcParameters *const   MrcData,
  UINT8                     Rank,
  UINT16                    Start,
  UINT16                    Stop,
  INT16                     Step,
  OUT UINT16                LeftLimit[MAX_CONTROLLER][MAX_CHANNEL][MAX_RANK_IN_CHANNEL][MAX_SDRAM_IN_DIMM],
  OUT UINT16                RightLimit[MAX_CONTROLLER][MAX_CHANNEL][MAX_RANK_IN_CHANNEL][MAX_SDRAM_IN_DIMM]
)
{
  MrcInput          *Inputs;
  MRC_FUNCTION      *MrcCall;
  MrcDebug          *Debug;
  MrcOutput         *Outputs;
  MrcChannelIn      *ChannelIn;
  CADB_LINE         *CadbLines;
  MrcDdrType        DdrType;
  UINT16            *LeftLimitPtr;
  UINT16            *RightLimitPtr;
  UINT8             Controller;
  UINT8             Channel;
  UINT8             McChannelMask;
  UINT8             RankMask;
  UINT8             Byte;
  UINT8             ByteMask;
  UINT8             DramByte;
  UINT8             ByteDoneMask[MAX_CONTROLLER][MAX_CHANNEL];
  UINT8             ByteDoneMaskCheck;
  UINT16            PiCode;
  UINT8             NumOfOnes;
  UINT8             TotalNumOfOnes;
  UINT8             Feedback[MAX_CONTROLLER][MAX_CHANNEL][MAX_SDRAM_IN_DIMM];
  UINT8             MaxChannel;
  UINT8             FeedbackMask;
  UINT16            CurrentPassingStart[MAX_CONTROLLER][MAX_CHANNEL][MAX_SDRAM_IN_DIMM];
  UINT16            CurrentPassingEnd[MAX_CONTROLLER][MAX_CHANNEL][MAX_SDRAM_IN_DIMM];
  UINT16            LargestPassingStart[MAX_CONTROLLER][MAX_CHANNEL][MAX_SDRAM_IN_DIMM];
  UINT16            LargestPassingEnd[MAX_CONTROLLER][MAX_CHANNEL][MAX_SDRAM_IN_DIMM];
  UINT32            DataTrainFeedback;
  UINT32            CaPattern;
  UINT32            Pattern;
  UINT32            DelayChipSelectCadb;
  const UINT32      *Seeds;
  INT64             DataTrainFeedbackField;
  BOOLEAN           Done;
  BOOLEAN           Failed;
  BOOLEAN           IgnoreByte;
  BOOLEAN           Lpddr4;
  BOOLEAN           Dimm;
  char              *BytesHeader;
  char              PassFail;

  Inputs        = &MrcData->Inputs;
  MrcCall       = Inputs->Call.Func;
  Outputs       = &MrcData->Outputs;
  Debug         = &Outputs->Debug;
  McChannelMask = 0;
  MaxChannel    = Outputs->MaxChannels;
  DdrType       = Outputs->DdrType;
  Lpddr4        = (DdrType == MRC_DDR_TYPE_LPDDR4);
  FeedbackMask  = FEEDBACKMASK;

  DelayChipSelectCadb = 1 * MRC_TIMER_1US;

  MrcCall->MrcSetMem ((UINT8 *) ByteDoneMask, sizeof (ByteDoneMask), 0);
  MrcCall->MrcSetMem ((UINT8 *) CurrentPassingStart, sizeof (CurrentPassingStart), 0xFF);
  MrcCall->MrcSetMem ((UINT8 *) CurrentPassingEnd, sizeof (CurrentPassingEnd), 0xFF);
  MrcCall->MrcSetMem ((UINT8 *) LargestPassingStart, sizeof (LargestPassingStart), 0xFF);
  MrcCall->MrcSetMem ((UINT8 *) LargestPassingEnd, sizeof (LargestPassingEnd), 0xFF);

  PiCode = Start;
  Done = FALSE;
  ByteDoneMaskCheck = 0;

  RankMask  = 1 << Rank;
  CadbLines = Lpddr4 ? CadbLinesLp4 : CadbLinesLp5;
  // LP5 patterns with CS are 0x2a and 0x55. 0x2a has 3 ones but 0x55 has 4 ones. LP4 patterns has 3 ones for both.
  CaPattern = Lpddr4 ? (CadbLines[2].CaHigh & CAPATTERNMASK) : (CadbLines[3].CaHigh & CAPATTERNMASK);
  NumOfOnes = MrcCountBitsEqOne (CaPattern);
  TotalNumOfOnes = 2 * NumOfOnes;

  for (Byte = 0; Byte < Outputs->SdramCount; Byte++) {
    ByteDoneMaskCheck |= (1 << Byte);
  }

  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\t Mc0Ch0 p1 Mc0Ch1 p1 Mc0Ch2 p1 Mc0Ch3 p1 Mc1Ch0 p1 Mc1Ch1 p1 Mc1Ch2 p1 Mc1Ch3 p1 ");
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Mc0Ch0 p2 Mc0Ch1 p2 Mc0Ch2 p2 Mc0Ch3 p2 Mc1Ch0 p2 Mc1Ch1 p2 Mc1Ch2 p2 Mc1Ch3 p2\n");
  BytesHeader = "0   1     ";
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "CTL PI\t %s%s", BytesHeader, BytesHeader);
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "%s%s", BytesHeader, BytesHeader);
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "%s%s", BytesHeader, BytesHeader);
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "%s%s", BytesHeader, BytesHeader);
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "%s%s", BytesHeader, BytesHeader);
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "%s%s", BytesHeader, BytesHeader);
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "%s%s", BytesHeader, BytesHeader);
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "%s%s\n", BytesHeader, BytesHeader);

  while (!Done) {
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "%d:\t", PiCode);
    for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
      for (Channel = 0; Channel < MaxChannel; Channel++) {
        if (MrcRankExist (MrcData, Controller, Channel, Rank)) {
          ShiftPIforCmdTraining (MrcData, Controller, Channel, MrcIterationCtl, RankMask, MRC_IGNORE_ARG_8, PiCode, 0);
        }
      }
    }

    // Try two different paterns (0x2AA or 0x155), to see if the command is still decoded correctly
    for (Pattern = 0; Pattern <= 1; Pattern++) {
      if (Lpddr4) {
        Seeds = (Pattern == 0) ? CadbMuxLp4Pattern2 : CadbMuxLp4Pattern3;
      } else {
        Seeds = (Pattern == 0) ? CadbMuxLp5RisingPattern2 : CadbMuxLp5RisingPattern3;
      }
      MrcInitCadbPgMux (MrcData, Seeds, 0, MRC_NUM_MUX_SEEDS);

      for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
        for (Channel = 0; Channel < MaxChannel; Channel++) {
          if (!MrcRankExist (MrcData, Controller, Channel, Rank)) {
            continue;
          }
          McChannelMask |= (1 << ((Controller * MaxChannel) + Channel));
        }
      }
      IoReset (MrcData);
      // Run CADB pattern on selected channels at the same time
      ShortRunCADB (MrcData, McChannelMask);
      MrcWait (MrcData, DelayChipSelectCadb);

      // Read and process the results
      for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
        for (Channel = 0; Channel < MaxChannel; Channel++) {
          if (!MrcRankExist (MrcData, Controller, Channel, Rank)) {
            ByteDoneMask[Controller][Channel] = ByteDoneMaskCheck;
            MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "          ");
            continue;
          }
          if (ByteDoneMask[Controller][Channel] == ByteDoneMaskCheck) { // All bytes failed on this channel, no need to sweep anymore
            MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "          ");
            continue;
          }
          LeftLimitPtr = CurrentPassingStart[Controller][Channel];
          RightLimitPtr = CurrentPassingEnd[Controller][Channel];
          ChannelIn  = &Inputs->Controller[Controller].Channel[Channel];
          for (Byte = 0; Byte < Outputs->SdramCount; Byte++) {
            ByteMask = (1 << Byte);
            DramByte = ChannelIn->DqsMapCpu2Dram[dDIMM0][Byte]; // Lpddr only uses DIMM0
            IgnoreByte = FALSE;
            if (!MrcByteExist (MrcData, Controller, Channel, Byte)) {
              IgnoreByte = TRUE;
            }
            if (((DramByte & 1) == 0) && !Outputs->LpByteMode) {
              // Ignore DRAM bytes 0, 2, 4 and 6 - they don't provide feedback
              // Byte mode parts provide feedback on all bytes
              IgnoreByte = TRUE;
            }
            if (IgnoreByte) {
              ByteDoneMask[Controller][Channel] |= ByteMask;
              MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "    ");
              continue;
            }
            MrcGetSetChStrb (MrcData, Controller, Channel, Byte, GsmIocDataTrainFeedback, ReadUncached, &DataTrainFeedbackField);
            DataTrainFeedback = (UINT32) DataTrainFeedbackField & FeedbackMask;  // Get only DQ bits, not DQS
            PassFail = '#';
            if ((ByteDoneMask[Controller][Channel] & ByteMask) == 0) {
              if (Pattern == 0) {
                // First pattern
                Feedback[Controller][Channel][Byte] = (UINT8) DataTrainFeedback;
                PassFail = ' ';
              } else {
                // Second Pattern
                // If still read the same data, then DRAM was not able to decode the new command
                Failed = FALSE;
                if (Feedback[Controller][Channel][Byte] == (UINT8) DataTrainFeedback) {
                  Failed = TRUE;
                }
                if (MrcCountBitsEqOne (DataTrainFeedback) != NumOfOnes) {
                  Failed = TRUE;
                }
                if (MrcCountBitsEqOne (DataTrainFeedback ^ Feedback[Controller][Channel][Byte]) != TotalNumOfOnes) {
                  Failed = TRUE;
                }

                if (Failed) {
                  if (LeftLimitPtr[Byte] == RightLimitPtr[Byte]) {
                    LeftLimitPtr[Byte] = RightLimitPtr[Byte] = PiCode;
                  } else {
                    if (Step > 0) {
                      RightLimitPtr[Byte] = PiCode;
                    } else {
                      LeftLimitPtr[Byte] = PiCode;
                    }
                    //Check against Largest Passing
                    if ((RightLimitPtr[Byte] - LeftLimitPtr[Byte]) > (LargestPassingEnd[Controller][Channel][Byte] - LargestPassingStart[Controller][Channel][Byte])) {
                      LargestPassingStart[Controller][Channel][Byte] = LeftLimitPtr[Byte];
                      LargestPassingEnd[Controller][Channel][Byte] = RightLimitPtr[Byte];
                    }
                    LeftLimitPtr[Byte] = RightLimitPtr[Byte] = PiCode;
                  }
                } else {
                  PassFail = '.';
                  if (PiCode == Start) {
                    if (Step > 0) {
                      LeftLimitPtr[Byte] = PiCode;
                    } else {
                      RightLimitPtr[Byte] = PiCode;
                    }
                  } else {
                    if (Step > 0) {
                      RightLimitPtr[Byte] = PiCode;
                    } else {
                      LeftLimitPtr[Byte] = PiCode;
                    }
                  }
                }
              }
            }
            MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "%c%02X ", PassFail, DataTrainFeedback);
          }  // for Byte
          MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "  ");
        }  // for Channel
      }  // for Controller
    } // for Pattern
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\n");

    PiCode += Step;
    if (Step > 0) {
      // Sweep up
      Done = (PiCode > Stop);
    } else {
      // Sweep down
      Done = (((INT16) PiCode) < Stop);
    }
  }  // while not done

  for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
    for (Channel = 0; Channel < MaxChannel; Channel++) {
      if (!MrcRankExist (MrcData, Controller, Channel, Rank)) {
        continue;
      }
      ChannelIn = &Inputs->Controller[Controller].Channel[Channel];
      Dimm = RANK_TO_DIMM_NUMBER (Rank);
      for (Byte = 0; Byte < Outputs->SdramCount; Byte++) {
        ByteMask = (1 << Byte);
        DramByte = ChannelIn->DqsMapCpu2Dram[Dimm][Byte];
        IgnoreByte = FALSE;
        if (!MrcByteExist (MrcData, Controller, Channel, Byte)) {
          IgnoreByte = TRUE;
        }
        if (((DramByte & 1) == 0) && !Outputs->LpByteMode) {
          // Ignore DRAM bytes 0, 2, 4 and 6 - they don't provide feedback
          // Byte mode parts provide feedback on all bytes
          IgnoreByte = TRUE;
        }
        if (IgnoreByte) {
          continue;
        }
        LeftLimitPtr = CurrentPassingStart[Controller][Channel];
        RightLimitPtr = CurrentPassingEnd[Controller][Channel];
        if ((RightLimitPtr[Byte] - LeftLimitPtr[Byte]) > (LargestPassingEnd[Controller][Channel][Byte] - LargestPassingStart[Controller][Channel][Byte])) {
          LargestPassingStart[Controller][Channel][Byte] = LeftLimitPtr[Byte];
          LargestPassingEnd[Controller][Channel][Byte] = RightLimitPtr[Byte];
        }
        LeftLimit[Controller][Channel][Rank][Byte] = LargestPassingStart[Controller][Channel][Byte];
        RightLimit[Controller][Channel][Rank][Byte] = LargestPassingEnd[Controller][Channel][Byte];
      }
    }
  }

}

/**
  This routine is used throughout LPDDR4  and LP5 ECT code to initialize DDR IO for CA training.
  For LP4 we do the following
  On "Odd" DRAM bytes: enable the following bits because we will use DATATRAINFEEDBACK to read back CA values on DQ[13:8] pins:
    DataControl0.ForceRxOn and ForceBiasOn (must be set first)
    DataControl0.SenseampTrainingMode, ForceOdtOn and TxDisable

  On "Even" DRAM bytes we will send VrefCA values on DQ[6:0] pins:
    DataControl5.DdrCRMaskCntPulseNumStart[16:13] = 6 (number of masked DQS pulses)
    DataControl5.DdrCRNumOfPulses[12:11] = 2 (number of DQS pulses sent)
    DataControl5.Ddrcrdqsmaskcnten[10] = 1 (enables the special CBT VrefCA programming mode)
    DataControl0.TxOn (must be after DataControl5)

  Since we use CBT1 for LP5 , we do not send CAVref on DQ and hence we do not need to Mask Dqs Pulses.
  CA feedback is always recieved on CA [6:0]

  @param[in] MrcData - The MRC global data.

  @retval none
**/
VOID
LpddrCaTrainingInitIo (
  IN MrcParameters *const MrcData
  )
{
  MrcOutput                   *Outputs;
  MrcChannelIn                *ChannelIn;
  MrcDdrType                  DdrType;
  INT64                       GetSetVal;
  INT64                       GetSetEn;
  INT64                       NumMaskedPulses;
  INT64                       NumOfPulses;
  UINT32                      Controller;
  UINT32                      Channel;
  UINT8                       Byte;
  UINT32                      DramBit;
  UINT32                      Bit;
  UINT8                       CpuValue;

  Outputs         = &MrcData->Outputs;
  DdrType         = Outputs->DdrType;
  GetSetVal       = 1;
  GetSetEn        = 1;
  NumMaskedPulses = 6;
  NumOfPulses     = 2;

  IoReset (MrcData); // To avoid ODT segment shift while in ForceOdtOn mode

  if (DdrType == MRC_DDR_TYPE_LPDDR4) {
    for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
      for (Channel = 0; Channel < Outputs->MaxChannels; Channel++) {
        if (!MrcChannelExist (MrcData, Controller, Channel)) {
          continue;
        }
        ChannelIn = &MrcData->Inputs.Controller[Controller].Channel[Channel];
         for (Byte = 0; Byte < Outputs->SdramCount; Byte++) {
          if (ChannelIn->DqsMapCpu2Dram[dDIMM0][Byte]) { // Lpddr only uses DIMM0 entry
            MrcGetSetChStrb (MrcData, Controller, Channel, Byte, GsmIocForceRxAmpOn, WriteToCache, &GetSetEn);
          }
        }
        // Set the following on all bytes for x8 Lp4x devices
        if (Outputs->LpByteMode) {
          MrcGetSetChStrb (MrcData, Controller, Channel, MAX_SDRAM_IN_DIMM, GsmIocForceRxAmpOn, WriteToCache, &GetSetEn);
        }
        MrcFlushRegisterCachedData (MrcData);

        for (Byte = 0; Byte < Outputs->SdramCount; Byte++) {
          // For ByteMode set all the fields in the if condition for both bytes
          if (ChannelIn->DqsMapCpu2Dram[dDIMM0][Byte] || Outputs->LpByteMode) { // Lpddr only uses DIMM0 entry
            MrcGetSetChStrb (MrcData, Controller, Channel, Byte, GsmIocForceOdtOn, WriteToCache, &GetSetEn);
            MrcGetSetChStrb (MrcData, Controller, Channel, Byte, GsmIocTxDisable, WriteToCache, &GetSetEn);
            GetSetVal = 0;
            MrcGetSetChStrb (MrcData, Controller, Channel, Byte, GsmIocRxMatchedPathEn, WriteToCache, &GetSetVal);
            GetSetVal = MrcRxModeUnmatchedP;
            MrcGetSetChStrb(MrcData, Controller, Channel, Byte, RxLpddrMode, WriteToCache, &GetSetVal);
            MrcFlushRegisterCachedData (MrcData);
            MrcGetSetChStrb (MrcData, Controller, Channel, Byte, GsmIocCaTrainingMode, WriteNoCache, &GetSetEn);
          } else {
            MrcGetSetChStrb (MrcData, Controller, Channel, Byte, GsmIocDqsMaskPulseCnt, WriteToCache, &NumMaskedPulses);
            MrcGetSetChStrb (MrcData, Controller, Channel, Byte, GsmIocDqsPulseCnt, WriteToCache, &NumOfPulses);
            // @todo - Configure strobe toggle to get pattern.
            MrcFlushRegisterCachedData (MrcData);  // Must enable Pulse config before turning on Tx.
            MrcGetSetChStrb (MrcData, Controller, Channel, Byte, GsmIocTxPiPwrDnDis, WriteCached, &GetSetEn);
            MrcGetSetChStrb (MrcData, Controller, Channel, Byte, GsmIocTxOn, WriteCached, &GetSetEn);
          }
        } //Byte
        MrcGetSetStrobe (MrcData, Controller, Channel, MAX_RANK_IN_CHANNEL, MAX_SDRAM_IN_DIMM, GsmIocDdrDqRxSdlBypassEn, WriteNoCache, &GetSetEn);
      } //Channel
    } //Controller
  } else { //Lpddr5
    for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
      for (Channel = 0; Channel < Outputs->MaxChannels; Channel++) {
        if (!MrcChannelExist (MrcData, Controller, Channel)) {
          continue;
        }
        ChannelIn = &MrcData->Inputs.Controller[Controller].Channel[Channel];
        for (Byte = 0; Byte < Outputs->SdramCount; Byte++) {
          CpuValue = 0;
          MrcGetSetChStrb (MrcData, Controller, Channel, Byte, GsmIocForceRxAmpOn, WriteToCache, &GetSetEn);
          MrcGetSetChStrb (MrcData, Controller, Channel, Byte, GsmIocForceOdtOn, WriteToCache, &GetSetEn);
          //Need to Program DataTrainFeedBack Register to Toggle Dq7 Correctly
          for (Bit = 0; Bit < MAX_BITS; Bit++) {
            // Find DRAM DQ pin that is connected to the current CPU DQ pin
            DramBit = ChannelIn->DqMapCpu2Dram[Byte][Bit];
            if (DramBit == 7) {
              CpuValue |= 1 << Bit;  // Should go on this CPU DQ pin
            }
          }
          GetSetVal = CpuValue;
          MrcGetSetChStrb (MrcData, Controller, Channel, Byte, GsmIocDdrDqDrvEnOvrdData,   WriteToCache , &GetSetVal);
          MrcGetSetChStrb (MrcData, Controller, Channel, Byte, GsmIocDdrDqDrvEnOvrdModeEn,   WriteToCache , &GetSetEn);
          MrcFlushRegisterCachedData (MrcData);
          MrcGetSetChStrb (MrcData, Controller, Channel, Byte, GsmIocCaTrainingMode, WriteNoCache, &GetSetEn);
        } //Byte
        MrcGetSetStrobe (MrcData, Controller, Channel, MAX_RANK_IN_CHANNEL, MAX_SDRAM_IN_DIMM, GsmIocDdrDqRxSdlBypassEn, WriteNoCache, &GetSetEn);
      } //Channel
    } //Controller
    CadbStartDelay (MrcData, 4);
  } //Lp5

}

/**
  This routine is used throughout LPDDR4 and LPDDR5 ECT code to restore DDR IO from CA training.
  For LP4 we do the following :
  On "Odd" DRAM bytes: disable the following bits:
    DataControl0.ForceRxOn and ForceBiasOn (must be cleared last)
    DataControl0.SenseampTrainingMode, ForceOdtOn and TxDisable

  On "Even" DRAM bytes: disable the following bits:
    DataControl0.TxOn (must be cleared first)

  Since we use CBT1 for LP5 , we do not send CAVref on DQ and hence we do not need to Mask Dqs Pulses.
  CA feedback is always recieved on CA [6:0]

  The order of HW writes is opposite from LpddrCaTrainingInitIo.

  @param[in] MrcData - The MRC global data.

  @retval none
**/
VOID
LpddrCaTrainingRestoreIo (
  IN MrcParameters *const  MrcData
  )
{
  MrcOutput                   *Outputs;
  MrcChannelIn                *ChannelIn;
  MrcDdrType                  DdrType;
  INT64                       GetSetVal;
  INT64                       GetSetDis;
  UINT32                      Controller;
  UINT32                      Channel;
  UINT8                       Byte;
  UINT8                       RxMode;

  Outputs   = &MrcData->Outputs;
  DdrType   = Outputs->DdrType;
  GetSetVal = 0;
  GetSetDis = 0;
  RxMode    = Outputs->RxMode;

  if (DdrType == MRC_DDR_TYPE_LPDDR4) {
    for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
      for (Channel = 0; Channel < Outputs->MaxChannels; Channel++) {
        if (!MrcChannelExist (MrcData, Controller, Channel)) {
          continue;
        }
        ChannelIn = &MrcData->Inputs.Controller[Controller].Channel[Channel];
        for (Byte = 0; Byte < Outputs->SdramCount; Byte++) {
           // For ByteMode set all the fields in the if condition for both bytes
          if (ChannelIn->DqsMapCpu2Dram[dDIMM0][Byte] || Outputs->LpByteMode) { // Lpddr only uses DIMM0 entry
            MrcGetSetChStrb (MrcData, Controller, Channel, Byte, GsmIocCaTrainingMode, WriteNoCache, &GetSetDis);
            MrcGetSetChStrb (MrcData, Controller, Channel, Byte, GsmIocForceOdtOn, WriteToCache, &GetSetDis);
            MrcGetSetChStrb (MrcData, Controller, Channel, Byte, GsmIocTxDisable, WriteToCache, &GetSetDis);
            GetSetVal = ((RxMode == MrcRxModeUnmatchedN) || (RxMode == MrcRxModeUnmatchedP)) ? 0 : 1;;
            MrcGetSetChStrb (MrcData, Controller, Channel, Byte, GsmIocRxMatchedPathEn, WriteToCache, &GetSetVal);
            GetSetVal = RxMode;
            MrcGetSetChStrb(MrcData, Controller, Channel, Byte, RxLpddrMode, WriteToCache, &GetSetVal);
          } else {
            MrcGetSetChStrb (MrcData, Controller, Channel, Byte, GsmIocTxOn, WriteToCache, &GetSetDis);
            MrcGetSetChStrb (MrcData, Controller, Channel, Byte, GsmIocTxPiPwrDnDis, WriteToCache, &GetSetDis);
          }
        }
        MrcFlushRegisterCachedData (MrcData);

        for (Byte = 0; Byte < Outputs->SdramCount; Byte++) {
           if (ChannelIn->DqsMapCpu2Dram[dDIMM0][Byte]) { // Lpddr only uses DIMM0 entry
            MrcGetSetChStrb (MrcData, Controller, Channel, Byte, GsmIocForceRxAmpOn, WriteToCache, &GetSetDis);
          }
        }
        // Set the following in both bytes
        if (Outputs->LpByteMode) {
           MrcGetSetChStrb (MrcData, Controller, Channel, MAX_SDRAM_IN_DIMM, GsmIocForceRxAmpOn, WriteToCache, &GetSetDis);
        }
        MrcFlushRegisterCachedData (MrcData);
        MrcGetSetStrobe (MrcData, Controller, Channel, MAX_RANK_IN_CHANNEL, MAX_SDRAM_IN_DIMM, GsmIocDdrDqRxSdlBypassEn, WriteNoCache, &GetSetDis);
      }
    } //Controller
  } else {
    for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
      for (Channel = 0; Channel < Outputs->MaxChannels; Channel++) {
        if (!MrcChannelExist (MrcData, Controller, Channel)) {
          continue;
        }
        for (Byte = 0; Byte < Outputs->SdramCount; Byte++) {
          MrcGetSetChStrb (MrcData, Controller, Channel, Byte, GsmIocCaTrainingMode, WriteNoCache, &GetSetDis);
          MrcGetSetChStrb (MrcData, Controller, Channel, Byte, GsmIocForceOdtOn, WriteToCache, &GetSetDis);
          MrcGetSetChStrb (MrcData, Controller, Channel, Byte, GsmIocForceRxAmpOn, WriteToCache, &GetSetDis);
          MrcGetSetChStrb (MrcData, Controller, Channel, Byte, GsmIocDdrDqDrvEnOvrdData,   WriteToCache , &GetSetDis);
          MrcGetSetChStrb (MrcData, Controller, Channel, Byte, GsmIocDdrDqDrvEnOvrdModeEn,   WriteToCache , &GetSetDis);
        }
        MrcFlushRegisterCachedData (MrcData);
        MrcGetSetStrobe (MrcData, Controller, Channel, MAX_RANK_IN_CHANNEL, MAX_SDRAM_IN_DIMM, GsmIocDdrDqRxSdlBypassEn, WriteNoCache, &GetSetDis);
      } //Channel
    } //Controller
    CadbStartDelay (MrcData, 0);
  } //Lp5
}

/**
  This routine is used throughout LPDDR4 ECT code to initialize DDR IO for CA training.
  On all DRAM bytes we will send VrefCA values on DQ[6:0] pins:
    DataControl5.DdrCRMaskCntPulseNumStart[16:13] = 6 (number of masked DQS pulses)
    DataControl5.DdrCRNumOfPulses[12:11] = 2 (number of DQS pulses sent)
    DataControl5.Ddrcrdqsmaskcnten[10] = 1 (enables the special CBT VrefCA programming mode)
    DataControl0.TxOn (must be after DataControl5)

  @param[in] MrcData - The MRC global data.

  @retval none
**/
VOID
Lpddr4CaTrainingInitIoTx (
  IN MrcParameters *const MrcData
  )
{
  INT64               GetSetVal;
  INT64               NumMaskedPulses;
  INT64               NumOfPulses;

  GetSetVal       = 1;
  NumMaskedPulses = 6;
  NumOfPulses     = 2;

  MrcGetSetChStrb (MrcData, MAX_CONTROLLER, MAX_CHANNEL, MAX_SDRAM_IN_DIMM, GsmIocDqsMaskPulseCnt, WriteToCache, &NumMaskedPulses);
  MrcGetSetChStrb (MrcData, MAX_CONTROLLER, MAX_CHANNEL, MAX_SDRAM_IN_DIMM, GsmIocDqsPulseCnt,     WriteToCache, &NumOfPulses);
  // @todo - Configure strobe toggle to get pattern.
  MrcFlushRegisterCachedData (MrcData);  // Must enable Pulse config before turning on Tx.

  MrcGetSetChStrb (MrcData, MAX_CONTROLLER, MAX_CHANNEL, MAX_SDRAM_IN_DIMM, GsmIocTxPiPwrDnDis,       WriteCached, &GetSetVal);
  MrcGetSetChStrb (MrcData, MAX_CONTROLLER, MAX_CHANNEL, MAX_SDRAM_IN_DIMM, GsmIocTxOn,               WriteCached, &GetSetVal);
}

/**
  This routine is used throughout LPDDR4 ECT code to restore DDR IO from CA training.
  On all DRAM bytes: disable the following bits:
    DataControl0.TxOn (must be cleared first)
    DataControl5.Ddrcrdqsmaskcnten[10] = 0 (disable the special CBT VrefCA programming mode)

  The order of HW writes is opposite from Lpddr4CaTrainingInitIoTx.

  @param[in] MrcData - The MRC global data.

  @retval none
**/
VOID
Lpddr4CaTrainingRestoreIoTx (
  IN MrcParameters *const MrcData
  )
{
  INT64 GetSetVal;

  GetSetVal = 0;

  MrcGetSetChStrb (MrcData, MAX_CONTROLLER, MAX_CHANNEL, MAX_SDRAM_IN_DIMM, GsmIocTxOn,        WriteToCache, &GetSetVal);
  MrcGetSetChStrb (MrcData, MAX_CONTROLLER, MAX_CHANNEL, MAX_SDRAM_IN_DIMM, GsmIocTxPiPwrDnDis,       WriteToCache, &GetSetVal);
  MrcFlushRegisterCachedData (MrcData);
}

/**
  Enter / exit LPDDR4 and LPDDR5 CA training mode:
  If ENABLE:
    - Send MRW to MR13 OP[0] = 1, Wait tMRD
    - Force CKE low
    - Switch PLL to High frequency
  Else // DISABLE
    - Switch PLL to Low frequency
    - Force CKE high, Wait tFC
    - Send MRW to MR13 OP[0] = 0.

  NOTE: this has to be called at once on all participating channels using ChBitMask argument, because we switch to high frequency when enabling CBT.

  @param[in] MrcData                 - The MRC global data.
  @param[in] McChBitMask             - McChannels to work on.
  @param[in] RankBitMask             - Ranks to work on.
  @param[in] Enable                  - Enable or disable CBT.
  @param[in] CbtPhase                - CbtPhase in MR13 LP5
  @param[in,out] WckControlSave      - Save WckControl register.
  @param[in,out] WckControl1Save     - Save WckControl1 register.
  @param[in,out] DataRcompDataSave   - Save RcompData register.
  @retval mrcSuccess if succeeded
**/
MrcStatus
LpddrCommandTrainingMode (
  IN MrcParameters *const  MrcData,
  IN UINT8                 McChBitMask,
  IN UINT8                 RankBitMask,
  IN UINT8                 Enable,
  IN UINT8                 CbtPhase,
  IN OUT UINT32    *const  WckControlSave,
  IN OUT UINT32    *const  WckControl1Save,
  IN OUT UINT32            DataRcompDataSave[MAX_CONTROLLER][MAX_SDRAM_IN_DIMM]
  )
{
  MrcStatus           Status;
  MrcOutput           *Outputs;
  MrcChannelOut       *ChannelOut;
  MrcSaveData         *SaveData;
  INT32               CtlPi;
  INT32               CmdPi;
  INT64               CurrCtlPi;
  INT64               CurrCmdPi;
  UINT16              Data;
  UINT8               MaxChannel;
  UINT8               Controller;
  UINT8               Channel;
  UINT8               Rank;
  UINT8               RankBit;
  UINT8               CbtModeForFsp;
  BOOLEAN             Lpddr4;
  UINT8               CbtMode;
  UINT8               Gear;
  UINT8               FspOp;
  BOOLEAN             Set;

  Outputs       = &MrcData->Outputs;
  SaveData      = &MrcData->Save.Data;
  MaxChannel    = Outputs->MaxChannels;
  Lpddr4        = (Outputs->DdrType == MRC_DDR_TYPE_LPDDR4);
  Status        = mrcSuccess;
  CbtMode       = Lpddr4 ? Enable : 0; //LP5 MR13 OP[6] = 0 - CBT1 mode, thats the default
  FspOp = (Outputs->LowFreqCsCmd2DSweepDone == TRUE) ? Enable : 0; // Keep DRAM state at FSP-OP = 0 for the low freq 2D sweep

  CtlPi = (Lpddr4) ? MRC_SAFE_LP4_CTL_PI : MRC_SAFE_LP5_CTL_PI;
  CmdPi = (Lpddr4) ? MRC_SAFE_LP4_CMD_PI : MRC_SAFE_LP5_CMD_PI;

  Set           = Enable ? 1 : 0;
  Gear          = Lpddr4 ? MrcGear1 : MrcGear2;

  if (!Enable) { // Exit CBT mode
    // Set Low frequency, unless we're already there
    if ((Outputs->Frequency != Outputs->LowFrequency) && (MrcData->Inputs.LpFreqSwitch == TRUE)) {
      Status = MrcFrequencySwitch (MrcData, Outputs->LowFrequency, Gear, MRC_PRINTS_OFF);
      if (Status != mrcSuccess) {
        return Status;
      }
    }
    // Force CKE high, Wait tFC_Long = 250ns
    if (Lpddr4) {
      MrcForceCkeValue (MrcData, McChBitMask, 0xF);
    } else {
      // Set TxOn and change Tx termination i.e RcompDrvDn to look like Rx termination
      MrcSetRcompData (MrcData, Set, DataRcompDataSave);
      // Drive DQ7 low
      MrcDriveDq7 (MrcData, Set, WckControlSave, WckControl1Save);
    }
    // lp5 this needs to be tCAENT = MRC_LP4_tFC_LONG_NS = 250ns
    MrcWait (MrcData, MRC_LP4_tFC_LONG_NS * MRC_TIMER_1NS);
  }

  for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
    for (Channel = 0; Channel < MaxChannel; Channel++) {
      if (!MrcChannelExist (MrcData, Controller, Channel)) {
        continue;
      }
      if (MC_CH_MASK_CHECK (McChBitMask, Controller, Channel, MaxChannel) == 0) {
        continue;
      }
      ChannelOut = &Outputs->Controller[Controller].Channel[Channel];
      for (Rank = 0; Rank < MAX_RANK_IN_CHANNEL; Rank ++) {
        if (!MrcRankExist (MrcData, Controller, Channel, Rank)) {
          continue;
        }
        RankBit = (1 << Rank);
        if ((RankBit & RankBitMask) != 0) {
          // Use safe CS/CA PI settings, otherwise this MRW may get sampled as invalid command (or SRE etc.)
          if (!Lpddr4) { // LP5
            CtlPi = (INT32) SaveData->LowFCtlPiCode[Controller][Channel][Rank];
            CmdPi = (INT32) SaveData->LowFCmdPiCode[Controller][Channel];
          }
          // Save/Restore from CR. Values from host structure may be trained values at higher gears.
          // Using values from host structure at lowfreq forces wrong gear ratio multiplication in GetSet
          MrcGetSetCcc (MrcData, Controller, Channel, Rank, 0, CtlGrpPi, ReadFromCache, &CurrCtlPi);
          MrcGetSetCcc (MrcData, Controller, Channel, MRC_IGNORE_ARG, 0, CmdGrpPi, ReadFromCache, &CurrCmdPi);

          ShiftPIforCmdTraining (MrcData, Controller, Channel, MrcIterationCtl, RankBit, MRC_IGNORE_ARG_8, CtlPi, 0);
          ShiftPIforCmdTraining (MrcData, Controller, Channel, MrcIterationCmd, MRC_IGNORE_ARG_8, 0xF, CmdPi, 0);
          IoReset (MrcData);
          Data = ChannelOut->Dimm[Rank / MAX_RANK_IN_DIMM].Rank[Rank % MAX_RANK_IN_DIMM].MR[mrIndexMR13];
          if (Lpddr4) {
            MrcLpddr4SetMr13 (MrcData, CbtMode, MRC_IGNORE_ARG_8, MRC_IGNORE_ARG_8, MRC_IGNORE_ARG_8, MRC_IGNORE_ARG_8, MRC_IGNORE_ARG_8, &Data);
            Status = MrcIssueMrw (MrcData, Controller, Channel, Rank, mrMR13, Data, FALSE);
          } else {
            // No need to program CBT mode for LP5, Since ECT always uses CBT Mode - 1 (Default)
            // In CTE Command gets dropped with Denali Errors while trying to Issue MRW to MR-13
            if (CbtMode != 0) {
              MrcLpddr5SetMr13 (MrcData, CbtMode, &Data); //Set OP[6] = 0 - CBT1 mode
              Status = MrcIssueMrw (MrcData, Controller, Channel, Rank, mrMR13, Data, FALSE);
            }
            Data = ChannelOut->Dimm[Rank / MAX_RANK_IN_DIMM].Rank[Rank % MAX_RANK_IN_DIMM].MR[mrIndexMR16];
            CbtModeForFsp = Enable ? 2 : 0;
            //MrcLpddr5Mr16 - Set OP[3:2] - FSP-OP[1] , OP[5:4]  - CBT for FSP, OP[7] - CBT-Phase before starting patterns on CA bus
            // FSP-OP should be 0 during this early 2D low freq training to match the jedec reset dram state
            MrcLpddr5SetMr16 (MrcData, MRC_IGNORE_ARG_8, FspOp, CbtModeForFsp, MRC_IGNORE_ARG_8, CbtPhase, &Data);
            Status = MrcIssueMrw (MrcData, Controller, Channel, Rank, mrMR16, Data, FALSE);
          }

          // Restore CS/CA PI settings from the host struct, CS is per SubCh
          ShiftPIforCmdTraining (MrcData, Controller, Channel, MrcIterationCtl, RankBit, MRC_IGNORE_ARG_8, (INT32) CurrCtlPi, 0);
          ShiftPIforCmdTraining (MrcData, Controller, Channel, MrcIterationCmd, MRC_IGNORE_ARG_8, 0xF, (INT32) CurrCmdPi, 0);
          IoReset (MrcData);

          if (Status != mrcSuccess) {
            return Status;
          }
        } // if
      } // Rank
    } // Channel
  } // Controller

  if (Enable) {
    // Wait tMRD - guaranteed by MRH
    if (Lpddr4) {
      // Force CKE low
      MrcForceCkeValue (MrcData, McChBitMask, 0);
    } else {
      MrcSetRcompData (MrcData, Set, DataRcompDataSave);
      // Drive DQ7 high
      MrcDriveDq7 (MrcData, Set, WckControlSave, WckControl1Save); // This will switch FSP_OP[0] to FSP_OP[1]
    }

    // Wait tCAENT = 250ns
    MrcWait (MrcData, MRC_LP_tCAENT_NS * MRC_TIMER_1NS);

    // Set High frequency, unless we're already there
    if (Outputs->LowFreqCsCmd2DSweepDone == TRUE) {
      if ((Outputs->Frequency != Outputs->HighFrequency) && (MrcData->Inputs.LpFreqSwitch == TRUE)) {
        Status = MrcFrequencySwitch (MrcData, Outputs->HighFrequency, Outputs->HighGear, MRC_PRINTS_OFF);
      }
    }
  }
  return Status;
}


/**
  Enter / exit Ddr5 CS Training mode:
  If ENABLE:
    - Send DDR5 "Enter CS Training Mode" MPC
  Else // DISABLE
    - Send DDR5 "Exit CS Training Mode" MPC

  @param[in] MrcData                 - The MRC global data.
  @param[in] McChBitMask             - McChannels to work on.
  @param[in] Rank                    - Ranks to work on.
  @param[in] Enable                  - Enable or disable CBT.
  @retval mrcSuccess if succeeded
**/
MrcStatus
Ddr5CsTrainMode (
  IN MrcParameters *const  MrcData,
  IN UINT8                 McChBitMask,
  IN UINT32                Rank,
  IN UINT8                 Enable
  )
{
  MrcStatus           Status;
  MrcOutput           *Outputs;
  UINT8               MaxChannel;
  UINT8               Controller;
  UINT8               Channel;


  Outputs             = &MrcData->Outputs;
  MaxChannel          = Outputs->MaxChannels;
  Status              = mrcSuccess;


  if (Enable == MRC_DISABLE) {
    MrcWaitClk (MrcData, MRC_DDR5_tCSTM_MIN_TO_MPC_EXIT_NCK);
  }

  for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
    for (Channel = 0; Channel < MaxChannel; Channel++) {
      if (MC_CH_MASK_CHECK (McChBitMask, Controller, Channel, MaxChannel) == 0) {
        continue;
      }
      if (!MrcRankExist (MrcData, Controller, Channel, Rank)) {
        continue;
      }
      // Enable CS Training Mode on DRAM
      if (Enable == MRC_ENABLE) {
        Status = MrcIssueMpc (MrcData, Controller, Channel, Rank, DDR5_MPC_ENTER_CS_TRAINING_MODE, MRC_PRINTS_ON);
      } else {
        Status = MrcIssueMpc (MrcData, Controller, Channel, Rank, DDR5_MPC_EXIT_CS_TRAINING_MODE, MRC_PRINTS_ON);
      }
      if (Status != mrcSuccess) {
        return Status;
      }
    } // Channel
  } // Controller

  MrcWait (MrcData, MRC_DDR5_tCSTM_ENTRY_EXIT_DELAY_NS);
  return mrcSuccess;
}

/**
  Enter / exit DDR5 CA Training mode:
  If ENABLE:
    - Send DDR5 "Enter CA Training Mode" MPC
  Else // DISABLE
    - Send two NOPs to exit CATM

  @param[in] MrcData      - The MRC global data.
  @param[in] McChBitMask  - McChannels to work on.
  @param[in] Rank         - Rank to work on.
  @param[in] Enable       - Enable or disable CBT.

  @retval mrcSuccess if succeeded
**/
MrcStatus
Ddr5CaTrainMode (
  IN MrcParameters *const  MrcData,
  IN UINT8                 McChBitMask,
  IN UINT32                Rank,
  IN UINT8                 Enable
  )
{
  MrcStatus           Status;
  MrcOutput           *Outputs;
  UINT8               MaxChannel;
  UINT8               Controller;
  UINT8               Channel;
  CADB_LINE           *CadbLines;
  UINT32              CadbCount;
  UINT32              *PatBuf;
  MRC_PG_UNISEQ_TYPE  *UniseqMode;
  UINT8               CadbLength;
  UINT32              NMode;

  Outputs             = &MrcData->Outputs;
  MaxChannel          = Outputs->MaxChannels;
  Status              = mrcSuccess;
  NMode               = MrcGetNMode(MrcData);

  for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
    for (Channel = 0; Channel < MaxChannel; Channel++) {
      if (MC_CH_MASK_CHECK (McChBitMask, Controller, Channel, MaxChannel) == 0) {
        continue;
      }
      // Enable CS Training Mode on DRAM
      if (Enable == MRC_ENABLE) {
        Status = MrcIssueMpc (MrcData, Controller, Channel, Rank, DDR5_MPC_ENTER_CA_TRAINING_MODE, MRC_PRINTS_ON);
      } else {
        // Issue 2 to 8 cycles of NOP to exit CA Training mode (tCATM_CS_Exit)
        // The NOP command will be issued using CADB
        CadbLines = CadbLinesDdr5CsTrain;
        CadbCount = ARRAY_COUNT(CadbLinesDdr5CsTrain);
        PatBuf = CadbMuxDdr5CaTrainExit; // CS low for 4 cycles
        UniseqMode = UniseqModePatBuffer;
        CadbLength = 10 * (UINT8) NMode; // Multiply by NMode for correct Unisequencer output
        SetupCaTrainingCadb(MrcData, Rank, CadbLines, CadbCount, PatBuf, UniseqMode);
        MrcCustomRunCADB(MrcData, Outputs->McChBitMask, CadbCustomLengthRun, CadbLength);
      }
      if (Status != mrcSuccess) {
        return Status;
      }
    } // Channel
  } // Controller

  MrcWait (MrcData, MRC_DDR5_tCATM_ENTRY_EXIT_DELAY_NS);
  return mrcSuccess;
}

/**
  Enter / exit DDR5 CSTM or CATM, depending on Iteration.

  @param[in] MrcData     - The MRC global data.
  @param[in] Iteration   - Command training type: MrcIterationCtl for CSTM and MrcIterationCmd for CATM.
  @param[in] McChBitMask - McChannels to work on.
  @param[in] Rank        - Rank to work on
  @param[in] Enable      - Enable or disable the training mode.

  @retval mrcSuccess if succeeded
**/
MrcStatus
MrcDdr5DramEctTrainMode (
  IN MrcParameters *const  MrcData,
  IN MrcIterationType      Iteration,
  IN UINT8                 McChBitMask,
  IN UINT32                Rank,
  IN UINT8                 Enable
  )
{
  if (Iteration == MrcIterationCtl) {
    return Ddr5CsTrainMode (MrcData, McChBitMask, Rank, Enable);
  } else {
    return Ddr5CaTrainMode (MrcData, McChBitMask, Rank, Enable);
  }
}

/**
  Process the results of the early LPDDR4 CS training and find the best PI settings for CS per sub-channel.
  Flow:
   1. Find the worst case Right and Left limits for each group
   2. Find the Center for each group

  @param[in]  MrcData     - The MRC global data.
  @param[in]  MaxValue    - Value to start RightLimit array at
  @param[in]  LeftLimit   - Array of left edge values per channel, rank and CPU byte
  @param[in]  RightLimit  - Array of right edge values per channel, rank and CPU byte
  @param[out] BestCs      - Array of best CS PI settings, per channel and group

  @retval mrcSuccess if succeeded
**/
MrcStatus
FindBestCsPi (
  IN  MrcParameters *const  MrcData,
  IN  UINT16                MaxValue,
  IN  UINT16                LeftLimit[MAX_CONTROLLER][MAX_CHANNEL][MAX_RANK_IN_CHANNEL][MAX_SDRAM_IN_DIMM],
  IN  UINT16                RightLimit[MAX_CONTROLLER][MAX_CHANNEL][MAX_RANK_IN_CHANNEL][MAX_SDRAM_IN_DIMM],
  OUT UINT16                BestCs[MAX_CONTROLLER][MAX_CHANNEL][MAX_RANK_IN_CHANNEL]
  )
{
  MrcInput          *Inputs;
  MRC_FUNCTION      *MrcCall;
  MrcDebug          *Debug;
  MrcOutput         *Outputs;
  MrcChannelIn      *ChannelIn;
  MrcStatus         Status;
  UINT32            Controller;
  UINT32            Channel;
  UINT8             DramByte;
  UINT8             Rank;
  UINT8             Byte;
  UINT16            CsLeftLimit[MAX_CONTROLLER][MAX_CHANNEL][MAX_RANK_IN_CHANNEL];
  UINT16            CsRightLimit[MAX_CONTROLLER][MAX_CHANNEL][MAX_RANK_IN_CHANNEL];
  BOOLEAN           Dimm;

  Inputs  = &MrcData->Inputs;
  MrcCall = Inputs->Call.Func;
  Outputs = &MrcData->Outputs;
  Debug   = &Outputs->Debug;
  Status  = mrcSuccess;

  MrcCall->MrcSetMemWord ((UINT16 *) CsRightLimit, sizeof (CsRightLimit) / sizeof (UINT16), MaxValue);
  MrcCall->MrcSetMemWord ((UINT16 *) CsLeftLimit, sizeof (CsLeftLimit) / sizeof (UINT16), 0);

  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Finding best CS PIs:\n");
  for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
    for (Channel = 0; Channel < Outputs->MaxChannels; Channel++) {
    ChannelIn = &Inputs->Controller[Controller].Channel[Channel];
      if (!MrcChannelExist (MrcData, Controller, Channel)) {
        continue;
      }
      MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Mc%d Ch%d\t\tLeft\tRight\tCenter\n", Controller, Channel);

      for (Rank = 0; Rank < MAX_RANK_IN_CHANNEL; Rank++) {
        if (!MrcRankExist (MrcData, Controller, Channel, Rank)) {
          continue;
        }
        Dimm = RANK_TO_DIMM_NUMBER(Rank);
        for (Byte = 0; Byte < Outputs->SdramCount; Byte++) {
          DramByte = ChannelIn->DqsMapCpu2Dram[Dimm][Byte];
          if (((DramByte & 1) == 0) && !Outputs->LpByteMode) {
            // Ignore DRAM bytes 0, 2, 4 and 6 - they don't provide feedback
            continue;
          }
          // Find the worst case Right and Left limits across ranks
          CsRightLimit[Controller][Channel][Rank] = MIN (CsRightLimit[Controller][Channel][Rank], RightLimit[Controller][Channel][Rank][Byte]);
          CsLeftLimit[Controller][Channel][Rank]  = MAX (CsLeftLimit[Controller][Channel][Rank], LeftLimit[Controller][Channel][Rank][Byte]);
        } // for Byte
        // Find the Center for each Channel
        BestCs[Controller][Channel][Rank] = (CsRightLimit[Controller][Channel][Rank] + CsLeftLimit[Controller][Channel][Rank]) / 2;

        MRC_DEBUG_MSG (
          Debug,
          MSG_LEVEL_NOTE,
          " Rank%d\t\t%d\t%d\t%d\n",
          Rank,
          CsLeftLimit[Controller][Channel][Rank],
          CsRightLimit[Controller][Channel][Rank],
          BestCs[Controller][Channel][Rank]
        );
      } // for Rank
    } // for Channel
  } // for Controller

  return Status;
}

/**
  Send VrefCA on DQ[6:0] during LPDDR4 CBT.

  @param[in] MrcData  - The MRC global data.
  @param[in] Rank     - The rank to work on.

  @retval none
**/
VOID
MrcSendCaVrefOnDq (
  IN MrcParameters *const   MrcData,
  IN UINT32                 Rank
  )
{
  MrcInput                      *Inputs;
  MRC_FUNCTION                  *MrcCall;
  MrcOutput                     *Outputs;
  MrcIntOutput                  *IntOutputs;
  MrcChannelIn                  *ChannelIn;
  INT64                         GetSetVal;
  UINT32                        Controller;
  UINT32                        Channel;
  UINT32                        DramByte;
  UINT32                        DramBit;
  UINT32                        CpuByte;
  UINT32                        CpuBit;
  UINT32                        BitValue;
  UINT8                         MaxChannel;
  UINT8                         McChBitMask;
  UINT8                         DramValue;
  UINT8                         CpuValue;
  BOOLEAN                       Lpddr;
  MRC_PATTERN_CTL               WDBPattern;
  UINT64_STRUCT                 VrefPattern[CADB_20_MAX_CHUNKS];

  static const Cpgc20Address  CPGCAddress = {
    CPGC20_ROW_COL_2_BANK_2_RANK,
    0,
    0,
    0,
    0,
    0,
    3,
    1
  };

  Inputs     = &MrcData->Inputs;
  MrcCall    = Inputs->Call.Func;
  Outputs    = &MrcData->Outputs;
  IntOutputs = (MrcIntOutput *) (MrcData->IntOutputs.Internal);
  MaxChannel = Outputs->MaxChannels;
  Lpddr      = Outputs->Lpddr;

  WDBPattern.IncRate    = 0;
  WDBPattern.Start      = 0;
  WDBPattern.Stop       = 1;
  WDBPattern.DQPat      = StaticPattern;
  WDBPattern.PatSource  = MrcPatSrcDynamic; // Don't use WDB
  WDBPattern.EnableXor  = FALSE;

  // Default VrefCA is 27.2% of VDD = 0.3v
  // Encoding: Range 1, 001101 --> 0x4D
  DramValue = 0x4D;

  for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
    for (Channel = 0; Channel < MaxChannel; Channel++) {
      if (!MrcChannelExist (MrcData, Controller, Channel)) {
        continue;
      }
      ChannelIn  = &Inputs->Controller[Controller].Channel[Channel];
      if (Outputs->LpByteMode) {
        // skip for x8 devices
        return;
      }
      MrcCall->MrcSetMem ((UINT8 *) VrefPattern, sizeof (VrefPattern), 0);
      for (CpuByte = 0; CpuByte < Outputs->SdramCount; CpuByte++) {
        // Find which DRAM byte is mapped to this CPU byte
        DramByte = ChannelIn->DqsMapCpu2Dram[dDIMM0][CpuByte]; // Lpddr only uses DIMM0 entry
        // CTE has x8 connection, so need to send VREF on all bytes
        if ((DramByte & 1) == 1) {
          continue; // VrefCA should be sent on even bytes only - DQ[6:0]
        }
        CpuValue = 0;
        for (CpuBit = 0; CpuBit < MAX_BITS; CpuBit++) {
          // Find DRAM DQ pin that is connected to the current CPU DQ pin
          DramBit = ChannelIn->DqMapCpu2Dram[CpuByte][CpuBit] - 8 * DramByte; // Subtract 8xDramByte
          BitValue = (DramValue >> DramBit) & 1;    // DRAM DQ value
          CpuValue |= (BitValue << CpuBit);         // Should go on this CPU DQ pin
        }
        GetSetVal = CpuValue;
        MrcGetSetChStrb (MrcData, Controller, Channel, CpuByte, GsmIocDqOverrideData, WriteToCache, &GetSetVal);
        GetSetVal = 0xFF;   // Send all the bits
        MrcGetSetChStrb (MrcData, Controller, Channel, CpuByte, GsmIocDqOverrideEn,   WriteToCache, &GetSetVal);
        VrefPattern[0].Data |= MrcCall->MrcLeftShift64 (CpuValue, CpuByte * 8);
      }
    } // for Channel
  } // for Controller
  MrcFlushRegisterCachedData (MrcData);

  // Setup IO test: CmdPat=PatWr, NumCL=2, LC=1, NSOE, EnCADB=0, EnCKE=0, SubSeqWait=0
  IntOutputs->SkipZq = TRUE;
  SetupIOTest (MrcData, Outputs->McChBitMask, PatWr, 2, 1, &CPGCAddress, NSOE, &WDBPattern, 0, 0, 0);
  IntOutputs->SkipZq = FALSE;


  GetSetVal = 1;
  McChBitMask = 0;
  for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
    for (Channel = 0; Channel < MaxChannel; Channel++) {
      McChBitMask |= SelectReutRanks (MrcData, (UINT8) Controller, (UINT8) Channel, 1 << Rank, FALSE, 0);
      if ((!MrcChannelExist (MrcData, Controller, Channel)) || IS_MC_SUB_CH (Lpddr, Channel)) {
        continue;
      }

      // Set SC_GS_CFG_TRAINING.mask_cs to mask CS while sending CPGC commands.
      // Set SC_GS_CFG_TRAINING.ignore_cke to send CPGC commands even if CKE is LOW.
      MrcGetSetMcCh (MrcData, Controller, Channel, GsmMccMaskCs,    WriteNoCache, &GetSetVal);
      MrcGetSetMcCh (MrcData, Controller, Channel, GsmMccIgnoreCke, WriteNoCache, &GetSetVal);
    }
  }
  MrcSetLoopcount (MrcData, 0); // Set BLOCK_REPEATS = 0 --> 1 iteration
  Cpgc20BaseRepeats (MrcData, 8, 1); // Set BASE_REPEATS to 7 (8 iterations)

  // Run Test: DQPat = StaticPattern, ClearErrors = 0, ResetMode = 0
  RunIOTest (MrcData, McChBitMask, WDBPattern.DQPat, 0, 0);

  // Wait tVrefCA_Long = 250ns
  MrcWait (MrcData, MRC_LP4_tVREFCA_LONG_NS * MRC_TIMER_1NS);

  GetSetVal = 0;
  MrcGetSetChStrb (MrcData, MAX_CONTROLLER, MAX_CHANNEL, MAX_SDRAM_IN_DIMM, GsmIocDqOverrideEn, WriteToCache, &GetSetVal);
  MrcFlushRegisterCachedData (MrcData);

  // Restore MC registers
  GetSetVal = 0;
  MrcGetSetMcCh (MrcData, MAX_CONTROLLER, MAX_CHANNEL, GsmMccMaskCs,    WriteNoCache, &GetSetVal);
  MrcGetSetMcCh (MrcData, MAX_CONTROLLER, MAX_CHANNEL, GsmMccIgnoreCke, WriteNoCache, &GetSetVal);
}

/**
  Early CS / CLK training for LPDDR4.
  Main flow:
  For Rank:
    1) IF Rank != Terminating:
      a) Enable Termination on the terminated rank via FSP-High
    2) Enter CBT Mode for this Rank.
    3) Drive Rank CKE Low: DIMM switches to FSP-High (1), wait tCKELCK
    4) PLL Frequency switch from low to High
    5) Run Training on all channels / subch in parallel, find passing CS region per ch/subch.
    6) IF Rank != Terminating:
      a) Disable Termination on the terminated rank via FSP-Low
    7) PLL Frequency Switch from High to Low
    8) Drive CKE High: DIMM switches to FSP Low (0)
    9) Exit CBT Mode (In Low Frequency)
    10) Set Vref MR to FSP-High.

  @param[in] MrcData - The MRC global data.

  @retval mrcSuccess if succeeded
**/
MrcStatus
EarlyChipSelectTrainingLp4 (
  IN MrcParameters *const MrcData
  )
{
  MrcInput              *Inputs;
  MRC_FUNCTION          *MrcCall;
  MrcDebug              *Debug;
  MrcOutput             *Outputs;
  MrcIntOutput          *IntOutputs;
  MrcIntCmdTimingOut    *IntCmdTiming;
  MrcDdrType            DdrType;
  MrcStatus             Status;
  CADB_LINE             *CadbLines;
  UINT32                CadbCount;
  INT64                 Min;
  INT64                 Max;
  INT64                 ForceRxOn;
  INT64                 TxPiOn;
  UINT32                *CadbMuxPattern;
  UINT32                Controller;
  UINT32                Channel;
  UINT32                WckControlSave;
  UINT32                WckControl1Save;
  UINT32                DataRcompDataSave[MAX_CONTROLLER][MAX_SDRAM_IN_DIMM];
  UINT8                 MaxChannel;
  UINT8                 McChannelMask;
  UINT8                 Rank;
  UINT8                 RankBit;
  UINT8                 RankMask;
  UINT8                 TermRankMask;
  UINT16                PiLow;
  UINT16                PiHigh;
  UINT16                PiMiddle;
  UINT16                PiStep;
  UINT16                RightLimit[MAX_CONTROLLER][MAX_CHANNEL][MAX_RANK_IN_CHANNEL][MAX_SDRAM_IN_DIMM];
  UINT16                LeftLimit[MAX_CONTROLLER][MAX_CHANNEL][MAX_RANK_IN_CHANNEL][MAX_SDRAM_IN_DIMM];
  UINT16                BestCs[MAX_CONTROLLER][MAX_CHANNEL][MAX_RANK_IN_CHANNEL];
  UINT32                FirstController;
  UINT32                FirstChannel;
  BOOLEAN               Lpddr4;
  UINT8                 HighGearRatio;
  UINT8                 CurrGear;
#ifdef MRC_DEBUG_PRINT
  MrcChannelIn          *ChannelIn;
  UINT16                Left;
  UINT16                Right;
  UINT8                 Byte;
  UINT8                 DramByte;
#endif // MRC_DEBUG_PRINT

  Inputs           = &MrcData->Inputs;
  MrcCall          = Inputs->Call.Func;
  Outputs          = &MrcData->Outputs;
  Debug            = &Outputs->Debug;
  IntOutputs       = (MrcIntOutput *) (MrcData->IntOutputs.Internal);
  TermRankMask     = Inputs->CmdRanksTerminated;
  MaxChannel       = Outputs->MaxChannels;
  DdrType          = Outputs->DdrType;
  Lpddr4           = (DdrType == MRC_DDR_TYPE_LPDDR4);
  CurrGear         = (Outputs->Gear4 ? 4 : (Outputs->Gear2 ? 2 : 1));
  HighGearRatio    =  Outputs->HighGear / CurrGear;

  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "EarlyChipSelectTrainingLp4 started\n");

  Status = mrcSuccess;
  RankMask  = Outputs->ValidRankMask;

  MrcGetSetLimits (MrcData, CtlGrpPi, &Min, &Max, NULL);
  PiLow     = (UINT16) Min;
  PiHigh    = (UINT16) MIN (Max, 511);
  PiMiddle  = (PiHigh - PiLow + 1) / 2; // // Will be updated below from the host struct
  PiStep = (PiHigh + 1) / 64;
  CadbLines = Lpddr4 ? CadbLinesLp4 : CadbLinesLp5;
  CadbCount = Lpddr4 ? ARRAY_COUNT (CadbLinesLp4) : ARRAY_COUNT (CadbLinesLp5);
  CadbMuxPattern = Lpddr4 ? CadbMuxLp4Pattern2 : CadbMuxLp5RisingPattern2;



  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "PiLow = %d, PiHigh = %d PiStep = %d\n", PiLow, PiHigh, PiStep);
  MrcCall->MrcSetMemWord ((UINT16 *) RightLimit, sizeof (RightLimit) / sizeof (UINT16), PiLow);
  MrcCall->MrcSetMemWord ((UINT16 *) LeftLimit, sizeof (LeftLimit) / sizeof (UINT16), PiLow);

  // Read the first populated channel
  FirstController = (MrcControllerExist (MrcData, cCONTROLLER0)) ? 0 : 1;
  FirstChannel = Outputs->Controller[FirstController].FirstPopCh;
  // Following fields are from datacontrol regs, channel is equivalent to controller for offset calc.
  // But these enums go through MrcTranslateSystemToIp translates channel and strobe to corresponding memtech
  // Read the initialized value of ForceRxAmpOn and TxPwrDnDisable for first populated byte
  MrcGetSetChStrb (MrcData, FirstController, FirstChannel, 0, GsmIocForceRxAmpOn, ReadFromCache, &ForceRxOn);
  MrcGetSetChStrb (MrcData, FirstController, FirstChannel, 0, GsmIocTxPiPwrDnDis, ReadFromCache, &TxPiOn);

  // Init DDR IO for CA training
  LpddrCaTrainingInitIo (MrcData);

  for (Rank = 0; Rank < MAX_RANK_IN_CHANNEL; Rank++) {
    RankBit = 1 << Rank;
    if ((RankBit & RankMask) == 0) {
      continue;
    }

    if ((RankBit & TermRankMask) == 0) {
      // Enable Termination on the terminated rank via FSP-High
      MrcLpddrSetCbtCaOdtEn (MrcData, TRUE);
    }


    McChannelMask = 0;     // Bitmask of channels that have current rank populated
    for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
      for (Channel = 0; Channel < MaxChannel; Channel++) {
        if (!MrcRankExist (MrcData, Controller, Channel, Rank)) {
          continue;
        }
        McChannelMask |= (1 << ((Controller * MaxChannel) + Channel));
        IntCmdTiming = &IntOutputs->Controller[Controller].CmdTiming[Channel];
        PiMiddle = IntCmdTiming->CtlPiCode[Rank];  // Found in EarlyCsCmdLpddr()
        MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "IntCmdTiming->CtlPiCode[%d] = %d\n", Rank, PiMiddle);
      }  // for Channel
    } // for Controller

    // Setup and Enter CBT Mode for this Rank.
    SetupCaTrainingCadb (MrcData, Rank, CadbLines, CadbCount, CadbMuxPattern, UniseqModePatBuffer);
    Status = LpddrCommandTrainingMode (MrcData, McChannelMask, RankBit, 1, 0, &WckControlSave, &WckControl1Save, DataRcompDataSave);
    if (Status != mrcSuccess) {
      return Status;
    }

    if (Lpddr4) {
      // Send VrefCA on DQ[6:0]
      MrcSendCaVrefOnDq (MrcData, Rank);
      MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "R%d: MrcSendCaVrefOnDq done\n", Rank);
    }

    // Sweep CS Pi from Low to High, on all channels at the same time
    ChipSelectFindEdge (MrcData, Rank, PiLow, PiHigh, PiStep, LeftLimit, RightLimit);

#ifdef MRC_DEBUG_PRINT
    for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
      for (Channel = 0; Channel < MaxChannel; Channel++) {
        if (!MrcRankExist (MrcData, Controller, Channel, Rank)) {
          continue;
        }
        ChannelIn  = &Inputs->Controller[Controller].Channel[Channel];
        MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "CS training data Mc%d Ch%d Rank%d\nCPU Byte\tLeft\tRight\tWidth\n", Controller, Channel, Rank);
        for (Byte = 0; Byte < Outputs->SdramCount; Byte++) {
          if (!MrcByteExist (MrcData, Controller, Channel, Byte)) {
            continue;
          }
          DramByte = ChannelIn->DqsMapCpu2Dram[dDIMM0][Byte]; // Lpddr only uses DIMM0 entry
          if (((DramByte & 1) == 0) && !Outputs->LpByteMode) {
            // Ignore DRAM bytes 0, 2, 4 and 6 - they don't provide feedback
            continue;
          }
          Left  = LeftLimit[Controller][Channel][Rank][Byte];
          Right = RightLimit[Controller][Channel][Rank][Byte];
          MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "%d\t\t%d\t%d\t%d\n", Byte, Left, Right, Right - Left);
        }
      }
    } // for Controller
#endif // MRC_DEBUG_PRINT

    // Exit CA training mode on the current rank
    Status = LpddrCommandTrainingMode (MrcData, McChannelMask, RankBit, 0, 0, &WckControlSave, &WckControl1Save, DataRcompDataSave);
    if (Status != mrcSuccess) {
      return Status;
    }

    if ((RankBit & TermRankMask) == 0) {
      // Disable Termination on the terminated rank via FSP-High
      Status = MrcLpddrSetCbtCaOdtEn (MrcData, FALSE);
    }
  }  // for Rank
  // Restore DDR IO values that were used for CMD training
  LpddrCaTrainingRestoreIo (MrcData);
  // The following are to ensure that the phyinit values are restored
  MrcGetSetChStrb (MrcData, MAX_CONTROLLER, MAX_CHANNEL, MAX_SDRAM_IN_DIMM, GsmIocForceRxAmpOn, WriteToCache, &ForceRxOn);
  MrcGetSetChStrb (MrcData, MAX_CONTROLLER, MAX_CHANNEL, MAX_SDRAM_IN_DIMM, GsmIocTxPiPwrDnDis, WriteToCache, &TxPiOn);
  MrcFlushRegisterCachedData (MrcData);

  // Select optimal CS timings for all populated controllers/channels
  FindBestCsPi (MrcData, PiHigh, LeftLimit, RightLimit, BestCs);

  // Apply the new CS settings
  // Program BestCs value per rank in the respective CsPi register.
  for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
    for (Channel = 0; Channel < MaxChannel; Channel++) {
      for (Rank = 0; Rank < MAX_RANK_IN_CHANNEL; Rank++) {
        if (!MrcRankExist (MrcData, Controller, Channel, Rank)) {
          continue;
        }
        RankMask = (1 << Rank);
        // Devide PiCodes by GearRatio before programming since 1-D sweep PiCodes may be from higher gear.
        ShiftPIforCmdTraining (MrcData, Controller, Channel, MrcIterationCtl, RankMask, MRC_IGNORE_ARG_8, BestCs[Controller][Channel][Rank] / HighGearRatio, 0);
        IntCmdTiming  = &IntOutputs->Controller[Controller].CmdTiming[Channel];
        IntCmdTiming->CtlPiCode[Rank] = BestCs[Controller][Channel][Rank];
        IntCmdTiming->CkePiCode[Rank] = BestCs[Controller][Channel][Rank];
      } //Rank
    } //Channel
  } //Controller

  return Status;
}

/**
  Early CA / CS training for LPDDR4/5.
  Main flow:
  1. Run CPU-to-DRAM DQ Mapping (map DQ pins according to the board swizzling).
  2. Run Early 2D CS/CMD training
  3. Run CS vs. CLK training.
  4. Run CA vs. CLK training.
  5. Program DESWIZZLE_HIGH/LOW registers.
  6. Set DRAM to FSP-High for all Ranks; lock PLL to High Frequency.

  @param[in] MrcData - The MRC global data.

  @retval mrcSuccess if succeeded
**/
MrcStatus
EarlyCommandTrainingLpddr (
  IN MrcParameters *const MrcData
  )
{
  MrcOutput       *Outputs;
  MrcInput        *Inputs;
  MrcDebug        *Debug;
  MrcStatus       Status;
  MrcDdrType      DdrType;
  UINT8           BiasPMCtrl;
  UINT8           Gear;
  UINT32          Controller;
  UINT32          Channel;
  UINT32          Byte;
  INT64           GetSetVal;
  BOOLEAN         Lpddr4;
  BOOLEAN         Lpddr;
  BOOLEAN         Q0;
  UINT32          IpChannel;
  UINT32          Offset;
  UINT32          AdaptivePcitWindow[MAX_CONTROLLER][MAX_CHANNEL];

  MC0_CH0_CR_SC_ADAPTIVE_PCIT_STRUCT      ScAdaptivePcit;

  Outputs    = &MrcData->Outputs;
  Inputs     = &MrcData->Inputs;
  Debug      = &Outputs->Debug;
  Status     = mrcSuccess;
  BiasPMCtrl = 0xFF;
  DdrType    = Outputs->DdrType;
  Lpddr      = Outputs->Lpddr;
  Lpddr4     = (DdrType == MRC_DDR_TYPE_LPDDR4);
  Gear       = Lpddr4 ? MrcGear1 : MrcGear2;
  Q0         = Inputs->Q0;

  // Set Low frequency, unless we're already there
  if ((Outputs->Frequency != Outputs->LowFrequency) && (Inputs->LpFreqSwitch == TRUE)) {
    Status = MrcFrequencySwitch (MrcData, Outputs->LowFrequency, Gear, !Outputs->RestoreMRs);
    if (Status != mrcSuccess) {
      return Status;
    }
  }
  // Only for Lp4, identify and map bit swizzle
  if (Lpddr4) {
    MapDqDqsSwizzle (MrcData);
  } else { //LP5
    PrintDqDqsTable (MrcData);
  }
  // @todo_adl LP5: Remove once ECT is clean with Low VDDQ
  if ((Outputs->VccddqVoltage == VDD_0_40) && (Outputs->HighGear == 4)) {
    // Program DESWIZZLE_HIGH/LOW registers
    ProgramDeswizzleRegisters (MrcData);
    Outputs->EctDone = TRUE;
    // Set FSP-OP = 1
    Status = MrcLpddrSwitchToHigh (MrcData, MRC_PRINTS_ON);
    return Status;
  }

  MrcBlockTrainResetToggle (MrcData, FALSE);
  for (Controller = 0; (Controller < MAX_CONTROLLER) && (BiasPMCtrl == 0xFF); Controller++) {
    for (Channel = 0; (Channel < Outputs->MaxChannels) && (BiasPMCtrl == 0xFF); Channel++) {
      for (Byte = 0; Byte < Outputs->SdramCount; Byte++) {
        if (MrcByteExist (MrcData, Controller, Channel, Byte)) {
          MrcGetSetChStrb (MrcData, Controller, Channel, Byte, GsmIocBiasPMCtrl, ReadFromCache, &GetSetVal);
          BiasPMCtrl = (UINT8) GetSetVal;
          break;
        }
      }
    }
  }

  GetSetVal = 0;
  MrcGetSetChStrb (MrcData, MAX_CONTROLLER, MAX_CHANNEL, MAX_SDRAM_IN_DIMM, GsmIocBiasPMCtrl, WriteToCache, &GetSetVal);
  MrcFlushRegisterCachedData (MrcData);

  // Program adaptive_pcit_window to 0 to prevent Precharge command being sent during ECT sweep time
  for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
    for (Channel = 0; Channel < Outputs->MaxChannels ; Channel++) {
      if ((!MrcChannelExist (MrcData, Controller, Channel)) || IS_MC_SUB_CH (Lpddr, Channel)) {
        // For LPDDR4/5, only program register on even channels.
        continue;
      }
      IpChannel = LP_IP_CH (Lpddr, Channel);
      Offset = OFFSET_CALC_MC_CH (MC0_CH0_CR_SC_ADAPTIVE_PCIT_REG, MC1_CH0_CR_SC_ADAPTIVE_PCIT_REG, Controller, MC0_CH1_CR_SC_ADAPTIVE_PCIT_REG, IpChannel);
      ScAdaptivePcit.Data = MrcReadCR64 (MrcData, Offset);
      AdaptivePcitWindow[Controller][IpChannel] = ScAdaptivePcit.Bits.ADAPTIVE_PCIT_WINDOW;
      ScAdaptivePcit.Bits.ADAPTIVE_PCIT_WINDOW = 0;
      MrcWriteCR64 (MrcData, Offset, ScAdaptivePcit.Data);
    }
  }

  // Early 2D CS/CMD training - find the good starting point for the rest of the ECT steps.
  if (Lpddr4 || (Inputs->LpFreqSwitch == FALSE)) {
    Outputs->LowFreqCsCmd2DSweepDone = TRUE;
  }
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\nLowFreqCsCmdSweepDone %d\n", Outputs->LowFreqCsCmd2DSweepDone);
  if ((Outputs->LowFreqCsCmd2DSweepDone == FALSE) && !Lpddr4) { // Run 2D sweep at low frequency for starting values
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Begin Early CS/CMD 2D sweep at %d freq\n", Outputs->LowFrequency);
    Status = EarlyCsCmdLpddr (MrcData);
    if (Status != mrcSuccess) {
      return Status;
    }
    Outputs->LowFreqCsCmd2DSweepDone = TRUE;
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\nEnd of Early CS/CMD 2D sweep at %d freq\n", Outputs->LowFrequency);
  }

  Status = EarlyCsCmdLpddr (MrcData);
  if (Status != mrcSuccess) {
    return Status;
  }
  //No IoReset due to CBT entry on next training

  // CS is single pumped in LP5 and 4 times slower than WCK so we dont need to train it.
  // Run CS vs. CLK training
  if (Lpddr4) {
    Status = EarlyChipSelectTrainingLp4 (MrcData);
    if (Status != mrcSuccess) {
      return Status;
    }
  }
  //No IoReset due to CBT entry on next training

  // Run CA vs. CLK training
  Status = EarlyCaTraining (MrcData);
  if (Status != mrcSuccess) {
    return Status;
  }
  IoReset (MrcData);

  GetSetVal = BiasPMCtrl;
  MrcGetSetChStrb (MrcData, MAX_CONTROLLER, MAX_CHANNEL, MAX_SDRAM_IN_DIMM, GsmIocBiasPMCtrl, WriteToCache, &GetSetVal);
  MrcFlushRegisterCachedData (MrcData);
  if (Lpddr4) {
    MrcSetWritePreamble (MrcData);
  }

  // Program DESWIZZLE_HIGH/LOW registers
  ProgramDeswizzleRegisters (MrcData);

  MrcBlockTrainResetToggle (MrcData, TRUE);

  if (Lpddr4 && Q0) {
    // FIXME: have to program Pcit with high frequency.
    Status = MrcLpddrSwitchToHigh (MrcData, MRC_PRINTS_ON);
    if (Status != mrcSuccess) {
      return Status;
    }
  }

  // Restore Adaptive_pcit_window value after ECT is complete
  for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
    for (Channel = 0; Channel < Outputs->MaxChannels ; Channel++) {
      if ((!MrcChannelExist (MrcData, Controller, Channel)) || IS_MC_SUB_CH (Lpddr, Channel)) {
        // For LPDDR4/5, only program register on even channels.
        continue;
      }
      IpChannel = LP_IP_CH (Lpddr, Channel);
      Offset = OFFSET_CALC_MC_CH (MC0_CH0_CR_SC_ADAPTIVE_PCIT_REG, MC1_CH0_CR_SC_ADAPTIVE_PCIT_REG, Controller, MC0_CH1_CR_SC_ADAPTIVE_PCIT_REG, IpChannel);
      ScAdaptivePcit.Data = MrcReadCR64 (MrcData, Offset);
      ScAdaptivePcit.Bits.ADAPTIVE_PCIT_WINDOW = AdaptivePcitWindow[Controller][IpChannel];
      MrcWriteCR64 (MrcData, Offset, ScAdaptivePcit.Data);
    }
  }
  Outputs->EctDone = TRUE;

  if (!Lpddr4) {
    Status = MrcResetSequence (MrcData);
  } else {
    // Set FSP-OP = 1
    Status = MrcLpddrSwitchToHigh (MrcData, MRC_PRINTS_ON);
  }

  return Status;
}

/**
  Center CMD with respect to CLK & CTL.

  @param[in,out] MrcData        - Include all MRC global data.
  @param[in]     ChannelMask    - Channels to train
  @param[in]     LoopCount      - Loop count for CMD stress.
  @param[in]     SetupCadb      - Indicates if SetupCadb should be called and disabled at the end.
  @param[in]     DebugPrint     - Enable / disable debug printing
  @param[in]     UpdateHost     - Determines if MrcData structure is updated

  @retval MrcStatus
**/
MrcStatus
MrcCmdTimingCentering (
  IN OUT MrcParameters *const MrcData,
  IN UINT8                ChannelMask,
  IN UINT8                LoopCount,
  IN BOOLEAN              SetupCadb,
  IN BOOLEAN              DebugPrint,
  IN const UINT8          UpdateHost
  )
{
  const MrcInput      *Inputs;
  MrcDebug            *Debug;
  MrcOutput           *Outputs;
  MrcIntOutput        *IntOutputs;
  MrcIntControllerOut *IntControllerOut;
  MrcIntCmdTimingOut  *IntCmdTiming;
  MrcStatus           Status;
  UINT8               Channel;
  UINT8               Controller;
  UINT8               ChBitMask;
  UINT8               RankMask;
  UINT16              MidPointCmd[MAX_COMMAND_GROUPS][MAX_CONTROLLER][MAX_CHANNEL];
  UINT8               CmdGroup;
  UINT8               CmdGroupMax;
  BOOLEAN             ExitOnFailure;
  BOOLEAN             Ddr4;
  MC0_CH0_CR_CADB_CFG_STRUCT          CadbConfig;

  Inputs            = &MrcData->Inputs;
  Outputs           = &MrcData->Outputs;
  Debug             = &Outputs->Debug;
  IntOutputs        = (MrcIntOutput *) (MrcData->IntOutputs.Internal);

  ExitOnFailure = (BOOLEAN) Inputs->ExitOnFailure;
  Ddr4          = (Outputs->DdrType == MRC_DDR_TYPE_DDR4);
  ChBitMask     = Outputs->ValidChBitMask & ChannelMask;
  RankMask      = Outputs->ValidRankMask;

  CmdGroupMax = (Ddr4) ? MAX_COMMAND_GROUPS : 1;

  // Program default rank mapping
  for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
    for (Channel = 0; Channel < MAX_CHANNEL; Channel++) {
      SelectReutRanks (MrcData, Controller, Channel, RankMask, FALSE, 0);
    }
  }

  if (SetupCadb) {
    // SetupIOTest is already called in TrainDDROptParms and LCT.
    SetupIOTestCADB (MrcData, ChBitMask, LoopCount, NTHSOE, 1, 0);
  }

  // Get the midpoint for CMD and CTL
  for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
    IntControllerOut = &IntOutputs->Controller[Controller];
    for (Channel = 0; Channel < MAX_CHANNEL; Channel++) {
      IntCmdTiming = &IntControllerOut->CmdTiming[Channel];
      for (CmdGroup = 0; CmdGroup < CmdGroupMax; CmdGroup++) {
        MidPointCmd[CmdGroup][Controller][Channel] = IntCmdTiming->CmdPiCode[CmdGroup];
      }
    }
  }

  // Center Command Timing
  for (CmdGroup = 0; CmdGroup < CmdGroupMax; CmdGroup++) {
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\n\n*** Center CMD_Pi%u Code Timing\n", CmdGroup);
    Status = CmdTimingCentering (MrcData, MrcIterationCmd, RankMask, (1 << CmdGroup), MidPointCmd[CmdGroup]);
    if ((Status != mrcSuccess) && (ExitOnFailure)) {
      return Status;
    }
  }

  // Disable CADB Deselects after Command Training
  if (SetupCadb) {
    CadbConfig.Data = 0;
    Cadb20ConfigRegWrite (MrcData, CadbConfig);
  }

  // Finish Training with JEDEC Reset / Init
  Status = MrcResetSequence (MrcData);
  return Status;
}

/**
  DDRIO Init for DDR5 CS/CA Training

  @param[in] MrcData   - The MRC global data.
  @param[in] Iteration - Current CS\CA Training Phase
  @param[in] Enable    - Enable or disable the DDR5 CS\CA Training mode
  @param[in] SaveData  - Register data saved on enable and restored on disable

  @retval none
**/
VOID
Ddr5CsCaTrainingInitIo (
  IN MrcParameters *const       MrcData,
  IN MrcIterationType           Iteration,
  IN UINT8                      McChannelMask,
  IN UINT8                      Rank,
  IN UINT8                      Enable,
  IN DDR5_CA_TRAIN_IO_INIT_SAVE *SaveData
  )
{
  MrcInput  *Inputs;
  MrcOutput *Outputs;
  UINT32    FirstController;
  UINT32    FirstChannel;
  UINT32    FirstByte;
  UINT8     CadbDelay;
  BOOLEAN   BlockTrainReset;
  INT64     GetSetBiasPMCtrlVal;
  INT64     GetSetForceOdtOnVal;
  INT64     GetSetDefDrvEnLowVal;
  INT64     GetSetCaTrainingModeVal;
  INT64     GetSetCaTrainParityModeVal;
  INT64     GetSetForceRxAmpOnVal;
  INT64     GetSetDdrDqRxSdlBypassEn;
  INT64     GetSetInternalClocksOnVal;
  INT64     DqOverrideEn;
  INT64     DqOverrideData;
  INT64     DdrDqDrvEnOvrdData;
  INT64     DdrDqDrvEnOvrdModeEn;

  Inputs  = &MrcData->Inputs;
  Outputs = &MrcData->Outputs;
  FirstController = Outputs->FirstPopController;
  FirstChannel    = Outputs->Controller[FirstController].FirstPopCh;
  FirstByte = 0;

  if (Enable) {
    GetSetCaTrainingModeVal     = 1;
    GetSetCaTrainParityModeVal  = ((Iteration == MrcIterationCmd) && (!Inputs->A0)) ? 1 : 0;
    if ((Outputs->DdrType == MRC_DDR_TYPE_DDR5) && Outputs->Gear4) {
      GetSetCaTrainParityModeVal = 0;
    }
    GetSetForceOdtOnVal         = 1;
    GetSetBiasPMCtrlVal         = 0;
    GetSetDefDrvEnLowVal        = 0;
    GetSetForceRxAmpOnVal       = 1;
    GetSetInternalClocksOnVal   = 1;
    GetSetDdrDqRxSdlBypassEn    = 1;  // Needed for feedback propagation into DataTrainFeedback
    CadbDelay = 1;
    BlockTrainReset = TRUE;  // @todo_adl Should be FALSE ?

    // Save the current PHY settings
    MrcGetSetChStrb (MrcData, FirstController, FirstChannel, FirstByte, GsmIocBiasPMCtrl,       ReadFromCache, &SaveData->BiasPMCtrl);
    MrcGetSetCccInst (MrcData, 0, DefDrvEnLow, ReadFromCache, &SaveData->DefDrvEnLow);
    MrcGetSetChStrb (MrcData, FirstController, FirstChannel, FirstByte, GsmIocForceRxAmpOn,     ReadFromCache, &SaveData->ForceRxOn);
    MrcGetSetChStrb (MrcData, FirstController, FirstChannel, FirstByte, GsmIocInternalClocksOn, ReadFromCache, &SaveData->InternalClocksOn);
  } else {
    GetSetCaTrainingModeVal     = 0;
    GetSetCaTrainParityModeVal  = 0;
    GetSetForceOdtOnVal         = 0;
    GetSetBiasPMCtrlVal         = SaveData->BiasPMCtrl;
    GetSetDefDrvEnLowVal        = SaveData->DefDrvEnLow;
    GetSetForceRxAmpOnVal       = SaveData->ForceRxOn;
    GetSetInternalClocksOnVal   = SaveData->InternalClocksOn;
    GetSetDdrDqRxSdlBypassEn    = 0;
    CadbDelay = 0;
    BlockTrainReset = TRUE;
  }

  MrcGetSetChStrb (MrcData, MAX_CONTROLLER, MAX_CHANNEL, MAX_SDRAM_IN_DIMM, GsmIocBiasPMCtrl,               WriteToCache, &GetSetBiasPMCtrlVal);
  MrcGetSetChStrb (MrcData, MAX_CONTROLLER, MAX_CHANNEL, MAX_SDRAM_IN_DIMM, GsmIocForceOdtOnWithoutDrvEn,   WriteToCache, &GetSetForceOdtOnVal);
  MrcGetSetChStrb (MrcData, MAX_CONTROLLER, MAX_CHANNEL, MAX_SDRAM_IN_DIMM, GsmIocForceRxAmpOn,             WriteToCache, &GetSetForceRxAmpOnVal);
  // DefDrvEnLow = 0 = Force On
  MrcGetSetCccInst (MrcData, MAX_CCC_INSTANCES, DefDrvEnLow, WriteToCache, &GetSetDefDrvEnLowVal);
  MrcGetSetChStrb (MrcData, MAX_CONTROLLER, MAX_CHANNEL, MAX_SDRAM_IN_DIMM, GsmIocCaTrainingMode,           WriteToCache, &GetSetCaTrainingModeVal);
  MrcGetSetChStrb (MrcData, MAX_CONTROLLER, MAX_CHANNEL, MAX_SDRAM_IN_DIMM, GsmIocCaParityTrain,            WriteToCache, &GetSetCaTrainParityModeVal);
  MrcGetSetChStrb (MrcData, MAX_CONTROLLER, MAX_CHANNEL, MAX_SDRAM_IN_DIMM, GsmIocInternalClocksOn,         WriteToCache, &GetSetInternalClocksOnVal);
  MrcGetSetStrobe (MrcData, MAX_CONTROLLER, MAX_CHANNEL, Rank, MAX_SDRAM_IN_DIMM, GsmIocDdrDqRxSdlBypassEn, WriteToCache, &GetSetDdrDqRxSdlBypassEn);
  MrcFlushRegisterCachedData (MrcData);

  if (Outputs->EccSupport && (Iteration == MrcIterationCmd)) {
    if (Enable) {
      DqOverrideEn          = 0xFF;
      DqOverrideData        = 0x00;
      DdrDqDrvEnOvrdData    = 0xF0; // Bits [7:4]
      DdrDqDrvEnOvrdModeEn  = 1;
    } else {
      DqOverrideEn          = 0;
      DqOverrideData        = 0;
      DdrDqDrvEnOvrdData    = 0;
      DdrDqDrvEnOvrdModeEn  = 0;
    }
    // The ECC Byte is Byte4 for DDR5
    MrcGetSetChStrb (MrcData, MAX_CONTROLLER, MAX_CHANNEL, 4, GsmIocDqOverrideEn,         WriteToCache, &DqOverrideEn);
    MrcGetSetChStrb (MrcData, MAX_CONTROLLER, MAX_CHANNEL, 4, GsmIocDqOverrideData,       WriteToCache, &DqOverrideData);
    MrcGetSetChStrb (MrcData, MAX_CONTROLLER, MAX_CHANNEL, 4, GsmIocDdrDqDrvEnOvrdData,   WriteToCache, &DdrDqDrvEnOvrdData);
    MrcGetSetChStrb (MrcData, MAX_CONTROLLER, MAX_CHANNEL, 4, GsmIocDdrDqDrvEnOvrdModeEn, WriteToCache, &DdrDqDrvEnOvrdModeEn);
    MrcFlushRegisterCachedData (MrcData);
  }

  Outputs->CaParityMode = (GetSetCaTrainParityModeVal == 1);

  // DDR5 ECT utilizes CADB for CS\CA test patterns
  MrcCadbIoInit (MrcData, McChannelMask, Enable, SaveData->CmdTriStateDisSave);

  MrcBlockTrainResetToggle (MrcData, BlockTrainReset);
  CadbStartDelay (MrcData, CadbDelay);
}

/**
  Apply an offset to the CS and CA PI codes relative to the current PI code.

  @param[in] MrcData   - The MRC global data.
  @param[in] CsOffset  - Signed offset to apply to the CtlGrpPi on all channels
  @param[in] CaOffset  - Signed offset to apply to the CmdGrpPi on all channels

  @retval none
**/
VOID
MrcOffsetCsCaTiming (
  IN  MrcParameters *const MrcData,
  IN  INT32                CsOffset,
  IN  INT32                CaOffset
  )
{
  MrcOutput             *Outputs;
  MrcIntOutput          *IntOutputs;
  UINT32                Controller;
  UINT32                Channel;
  UINT32                Rank;
  UINT8                 MaxChannel;
  UINT8                 CmdGroupMax;
  UINT8                 CmdGroup;
  UINT8                 GroupMask;
  UINT8                 RankMask;
  UINT32                PiCode;
  INT32                 NewPiCode;

  Outputs          = &MrcData->Outputs;
  MaxChannel       = Outputs->MaxChannels;
  CmdGroupMax      = MrcGetCmdGroupMax (MrcData);
  IntOutputs       = (MrcIntOutput *) (MrcData->IntOutputs.Internal);

  for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
    for (Channel = 0; Channel < MaxChannel; Channel++) {
      // Restore CS
      for (Rank = 0; Rank < MAX_RANK_IN_CHANNEL; Rank++) {
        if (!MrcRankExist (MrcData, Controller, Channel, Rank)) {
          continue;
        }
        PiCode      = IntOutputs->Controller[Controller].CmdTiming[Channel].CtlPiCode[Rank];
        NewPiCode   = PiCode + CsOffset;
        // If the new PI code is negative, turncate it to 0
        NewPiCode   = (NewPiCode < 0) ? 0 : NewPiCode;
        GroupMask   = MRC_IGNORE_ARG_8;
        RankMask    = (1 << Rank);
        ShiftPIforCmdTraining (MrcData, Controller, Channel, MrcIterationCtl, RankMask, GroupMask, (UINT32) NewPiCode, 0);
      } // Rank

      // Restore CA
      for (CmdGroup = 0; CmdGroup < CmdGroupMax; CmdGroup++) {
        PiCode      = IntOutputs->Controller[Controller].CmdTiming[Channel].CmdPiCode[CmdGroup];
        NewPiCode   = PiCode + CaOffset;
        // If the new PI code is negative, turncate it to 0
        NewPiCode   = (NewPiCode < 0) ? 0 : NewPiCode;
        GroupMask   = (1 << CmdGroup);
        RankMask    = MRC_IGNORE_ARG_8;
        ShiftPIforCmdTraining (MrcData, Controller, Channel, MrcIterationCmd, RankMask, GroupMask, (UINT32) NewPiCode, 0);
      }
    }  // Channel
  } // Controller
}

/**
  Find the Min and Max values of the currently programmed CmdPiCode accross the input channels.

  @param[in] MrcData     - The MRC global data.
  @param[in] McChBitMask - Bitmask of channels to iterate
  @param[out] MinValue   - The minimum value of CmdPiCode for the input channels
  @param[out] MaxValue   - The maximum value of CmdPiCode for the input channels

  @retval none
**/
void
MrcGetCmdPiCodeRange (
  IN     MrcParameters *const MrcData,
  IN     UINT8         McChBitMask,
     OUT UINT16        *MinValue,
     OUT UINT16        *MaxValue
  )
{
  MrcOutput             *Outputs;
  MrcIntOutput          *IntOutputs;
  UINT32                Controller;
  UINT32                Channel;
  UINT8                 MaxChannel;
  UINT8                 CmdGroupMax;
  UINT8                 CmdGroup;
  UINT16                PiCode;
  INT64                 CmdGrpPiMin;
  INT64                 CmdGrpPiMax;
  UINT16                MinPiCode;
  UINT16                MaxPiCode;

  MrcGetSetLimits(MrcData, CmdGrpPi, &CmdGrpPiMin, &CmdGrpPiMax, NULL);
  // Use the Min\Max as starting values to find actual min\max in the CmdTiming array
  MinPiCode = (UINT16) CmdGrpPiMax;
  MaxPiCode = (UINT16) CmdGrpPiMin;

  Outputs          = &MrcData->Outputs;
  MaxChannel       = Outputs->MaxChannels;
  CmdGroupMax      = MrcGetCmdGroupMax (MrcData);
  IntOutputs       = (MrcIntOutput *) (MrcData->IntOutputs.Internal);

  for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
    for (Channel = 0; Channel < MaxChannel; Channel++) {
      if (MC_CH_MASK_CHECK (McChBitMask, Controller, Channel, MaxChannel) == 0) {
        continue;
      }
      for (CmdGroup = 0; CmdGroup < CmdGroupMax; CmdGroup++) {
        PiCode      = IntOutputs->Controller[Controller].CmdTiming[Channel].CmdPiCode[CmdGroup];
        MinPiCode   = MIN (MinPiCode, PiCode);
        MaxPiCode   = MAX (MaxPiCode, PiCode);
      }
    }  // Channel
  } // Controller

  if (MinValue != NULL) {
    *MinValue = MinPiCode;
  }
  if (MaxValue != NULL) {
    *MaxValue = MaxPiCode;
  }
}

/**
  Find the Min and Max values of the currently programmed CtlPiCode accross the input channels, for the given rank.

  @param[in]  MrcData     - The MRC global data.
  @param[in]  McChBitMask - Bitmask of channels to iterate
  @param[in]  Rank        - Rank to work on
  @param[out] MinValue    - The minimum value of CmdPiCode for the input channels
  @param[out] MaxValue    - The maximum value of CmdPiCode for the input channels

  @retval none
**/
void
MrcGetCtlPiCodeRange (
  IN     MrcParameters *const MrcData,
  IN     UINT8         McChBitMask,
  IN     UINT32        Rank,
     OUT UINT16        *MinValue,
     OUT UINT16        *MaxValue
  )
{
  MrcOutput             *Outputs;
  MrcIntOutput          *IntOutputs;
  UINT32                Controller;
  UINT32                Channel;
  UINT8                 MaxChannel;
  UINT16                PiCode;
  INT64                 CtlGrpPiMin;
  INT64                 CtlGrpPiMax;
  UINT16                MinPiCode;
  UINT16                MaxPiCode;

  MrcGetSetLimits (MrcData, CtlGrpPi, &CtlGrpPiMin, &CtlGrpPiMax, NULL);
  // Use the Min/Max as starting values to find actual min\max in the CmdTiming array
  MinPiCode = (UINT16) CtlGrpPiMax;
  MaxPiCode = (UINT16) CtlGrpPiMin;

  Outputs    = &MrcData->Outputs;
  MaxChannel = Outputs->MaxChannels;
  IntOutputs = (MrcIntOutput *) (MrcData->IntOutputs.Internal);

  for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
    for (Channel = 0; Channel < MaxChannel; Channel++) {
      if (MC_CH_MASK_CHECK (McChBitMask, Controller, Channel, MaxChannel) == 0) {
        continue;
      }
      PiCode    = IntOutputs->Controller[Controller].CmdTiming[Channel].CtlPiCode[Rank];
      MinPiCode = MIN (MinPiCode, PiCode);
      MaxPiCode = MAX (MaxPiCode, PiCode);
    }  // Channel
  } // Controller

  if (MinValue != NULL) {
    *MinValue = MinPiCode;
  }
  if (MaxValue != NULL) {
    *MaxValue = MaxPiCode;
  }
}

/**
  CADB_BUF[7:0] Pattern Generator for DDR5 CA Parity Training.

  Generates a stressful victim-agressor pattern that can provide a much more
  accurate centering that fully stresses crosstalk and ISI.

  Each pattern targets two victim lanes.

  @param[in] Victim     - The target victim CA lane
  @param[out] CadbLines - The CADB_BUF[7:0] pattern configured to stress the input victim lane

  @retval none
**/
void
ConfigCadbLmnPatternStressful (
  IN  UINT8     Victim,
  OUT CADB_LINE CadbLines[8]
  )
{
  UINT32 PatGenerator;
  UINT32 EntryBit;
  UINT32 Lane;
  UINT32 Entry;

  // Initialize the table
  for (Entry = 0; Entry < 8; Entry++) {
    CadbLines[Entry].CaHigh = 0;
    CadbLines[Entry].CaLow = 0;
    // CS uses !S0
    EntryBit = 1 & (CADB_MUX_S0_INV >> Entry);
    CadbLines[Entry].ChipSelect = (UINT8) EntryBit;
  }

  for (Lane = 0; Lane < 13; Lane++) {
    if (Lane == (UINT32) ((Victim + 3) % 13)) {
      // One lane will use the inverse patter from Sequencer 0 (!S0)
      // This lane ensures even parity match
      PatGenerator = CADB_MUX_S0_INV;
    } else if (Lane == Victim || Lane == (UINT32) ((Victim + 7) % 13)) {
      // Two victim lanes will use the pattern from Sequencer 1 (S1)
      PatGenerator = CADB_MUX_S1;
    } else {
      // All other lanes use the aggressor pattern from Sequencer 2 (S2)
      PatGenerator = CADB_MUX_S2;
    }

    // Set the current lane in the Pattern Buffer table
    for (Entry = 0; Entry < 8; Entry++) {
      EntryBit = 1 & (PatGenerator >> Entry);
      CadbLines[Entry].CaLow &= ~(1 << Lane);
      CadbLines[Entry].CaLow |= EntryBit << Lane;
    }
  }
}

/**
  CADB_BUF[7:0] Pattern Generator for DDR5 CA Parity Training.

  Generates a CA Training pattern used for the initial training, which ensures
  all CA pins align to the correct cycle and deskews the lanes; this is done using
  a lone bit one and lone bit zero pattern on each lane where all the other lanes are
  quiet. This provides a very rough center with the main goal being getting all lanes
  to align to the same cycle with the same phase.

  Each pattern targets one victim lane.

  @param[in] Victim     - The target victim CA lane
  @param[out] CadbLines - The CADB_BUF[7:0] pattern configured to stress the input victim lane

  @retval none
**/
void
ConfigCadbLmnPatternSimple (
  IN  UINT8     Victim,
  OUT CADB_LINE CadbLines[8]
  )
{
  UINT32 PatGenerator;
  UINT32 EntryBit;
  UINT32 Lane;
  UINT32 Entry;

  // Initialize the table
  for (Entry = 0; Entry < 8; Entry++) {
    CadbLines[Entry].CaHigh = 0;
    CadbLines[Entry].CaLow = 0x1FFF; // Set CA[12:0] to all ones
    // CS uses !S0
    EntryBit = 1 & (CADB_MUX_S0_INV >> Entry);
    CadbLines[Entry].ChipSelect = (UINT8) EntryBit;
  }

  for (Lane = 0; Lane < 13; Lane++) {
    if (Lane == (UINT32) ((Victim + 1) % 13)) {
      // One lane will use the inverse patter from Sequencer 1 (!S1)
      // This lane ensures even parity match
      PatGenerator = CADB_MUX_S1_INV;
    } else if (Lane == Victim) {
      // One victim lane will use the pattern (!S0 ^ S1)
      PatGenerator = (CADB_MUX_S0_INV ^ CADB_MUX_S1);
    } else {
      // All other lanes ouput a "1"
      PatGenerator = 0xFF;
    }

    // Set the current lane in the Pattern Buffer table
    for (Entry = 0; Entry < 4; Entry++) {
      // Only CADB_BUF[3:0] entries are used
      EntryBit = 1 & (PatGenerator >> Entry);
      CadbLines[Entry].CaLow &= ~(1 << Lane);
      CadbLines[Entry].CaLow |= EntryBit << Lane;
    }
  }
}


/**
  Execute a CADB test for the input target bus and return the results.

  @param[in] MrcData          - The MRC global data.
  @param[in] Iteration        - The bus to target (CMD or CTL)
  @param[in] Phase            - The training phase, only used when Iteration == MrcIterationCmd
  @param[in] McChannelMask    - The channels to include in the CADB test
  @param[in] Rank             - The rank to run the CADB test
  @param[out] CadbTestRunning - Flag to indicate that the CADB test is still running
  @param[out] TestResult      - The results for each channel byte

  @retval none
**/
void
MrcDdr5EctRunCadbTest (
  IN     MrcParameters *const MrcData,
  IN     MrcIterationType     Iteration,
  IN     MrcTrainingPhase     Phase,
  IN     UINT8                McChannelMask,
  IN     UINT32               Rank,
  IN OUT BOOLEAN              *CadbTestRunning,
     OUT UINT8                TestResult[MAX_CONTROLLER][MAX_CHANNEL][MAX_SDRAM_IN_DIMM]
  )
{
  MRC_FUNCTION       *MrcCall;
  MrcInput           *Inputs;
  MrcOutput          *Outputs;
  UINT32             Controller;
  UINT32             Channel;
  UINT32             Byte;
  UINT32             Index;
  UINT32             MaxVictim;
  UINT32             FeedbackMask;
  UINT16             CadbLength;
  UINT8              VictimLane;
  UINT8              MaxChannel;
  UINT8              TestLoop;
  UINT8              MaxTestLoops;
  CADB_LINE          *CadbLines;
  CADB_LINE          CadbLinesBuffer[8];
  UINT32             CadbCount;
  UINT32             *PatBuf;
  MRC_PG_UNISEQ_TYPE *UniseqMode;
  UINT32             DataTrainFeedback;
  INT64              DataTrainFeedbackGetSet;
  UINT32             DataTrainFeedbackNumSamplesPerPiCode;
  BOOLEAN            CaParityMode;
  MRC_CADB_PATTERN_GENERATOR PatternGenerator;

  Inputs           = &MrcData->Inputs;
  Outputs          = &MrcData->Outputs;
  MrcCall          = Inputs->Call.Func;
  MaxChannel       = Outputs->MaxChannels;
  CaParityMode = (!Inputs->A0);
  if ((Outputs->DdrType == MRC_DDR_TYPE_DDR5) && Outputs->Gear4) {
    CaParityMode = Outputs->CaParityMode;
  }

  DataTrainFeedbackNumSamplesPerPiCode = (CaParityMode && (Iteration == MrcIterationCmd) ? 1 : DDR5_FEEDBACK_CSTM_NUM_SAMPLES);
  if (!CaParityMode && (Iteration == MrcIterationCmd)) {
    DataTrainFeedbackNumSamplesPerPiCode = 8;
  }

  // Initialize the TestResult output array to the "fail" value (0)
  MrcCall->MrcSetMem ((UINT8 *) TestResult, (MAX_CONTROLLER * MAX_CHANNEL * MAX_SDRAM_IN_DIMM) / sizeof(UINT8),  0);

  if (Iteration == MrcIterationCtl) {
    // CS Training CADB Test Pattern
    if (*CadbTestRunning == FALSE) {
      // Init DDR IO for CS training
      CadbLines  = CadbLinesDdr5CsTrain;
      CadbCount  = ARRAY_COUNT (CadbLinesDdr5CsTrain);
      PatBuf     = CadbMuxDdr5CsTrainPattern;
      UniseqMode = UniseqModePatBuffer;
      // The CADB pattern will continue until STOP_TEST is asserted
      SetupCaTrainingCadb (MrcData, Rank, CadbLines, CadbCount, PatBuf, UniseqMode);
      MrcCustomRunCADB (MrcData, McChannelMask, CadbEndlessTest, 0);
      *CadbTestRunning = TRUE;
    }

    // Wait for DRAM to output result at the current PI code before sampling DataTrainFeedback
    MrcWait (MrcData, MRC_DDR5_tCSTM_VALID_NS);

    // Reset the DataTrainFeedback (and start sampling)
    IoReset (MrcData);

    // Get the result on all channel bytes:
    for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
      for (Channel = 0; Channel < MaxChannel; Channel++) {
        if (!MrcRankExist (MrcData, Controller, Channel, Rank)) {
          continue;
        }
        for (Byte = 0; Byte < Outputs->SdramCount; Byte++) {
          if (!MrcByteExist (MrcData, Controller, Channel, Byte)) {
            continue;
          }
          DataTrainFeedback = 0;
          FeedbackMask = (Byte == (MAX_BYTE_IN_DDR5_CHANNEL - 1)) ? FEEDBACKMASK_ECC : FEEDBACKMASK;
          for (Index = 0; Index < DataTrainFeedbackNumSamplesPerPiCode; Index++) {
            MrcGetSetChStrb (MrcData, Controller, Channel, Byte, GsmIocDataTrainFeedback, ReadUncached, &DataTrainFeedbackGetSet);
            DataTrainFeedback |= (UINT8) DataTrainFeedbackGetSet & FeedbackMask;  // Get only DQ bits, not DQS
          }
          // A counter value of 0 indicates a pass (no 1's seen on DQ)
          TestResult[Controller][Channel][Byte] = (UINT8) DataTrainFeedback;
        } // Byte
      } // Channel
    } // Controller
  } else { // Iteration == MrcIterationCmd

    // CA Training CADB Test Pattern
    if (Phase == MrcTrainingPhase0) {
      MaxVictim = MAX_DDR5_CMD_PINS;
      PatternGenerator = ConfigCadbLmnPatternSimple;
      UniseqMode = UniseqModeLmnPatBuffer;
      PatBuf     = CadbLinesDdr5CaTrainLmnLoneZero;
      // Do two loops with the simple pattern to cover the Lone 0 and Lone 1 pattern
      MaxTestLoops = 2;
    } else { // Phase == MrcTrainingPhase1
      // The agressive pattern uses two victim lanes at a time,
      // only half the number of victim indexes are needed.
      MaxVictim = DIVIDECEIL (MAX_DDR5_CMD_PINS, 2);
      PatternGenerator = ConfigCadbLmnPatternStressful;
      UniseqMode = UniseqModeLmnLfsr;
      PatBuf     = CadbLinesDdr5CaTrainLmnSeeds;
      MaxTestLoops = 1;
    }

    // Configure LMN FSM to generate 1 cycle high and 63 cycles low
    // LCount = 7 :
    //   - Odd number to place CS_N low pattern on second phase
    //   - 17 ensures that CADB will output some deselect cycles before the
    //     first CS_N low is output.
    // MCount = 1 :
    //   Drive CS_N low with even parity CA pattern for only one cycle
    // NCount = 63 :
    //   Drive CS_N high with odd parity CA pattern for 63 cycles
    // InitialPolarity = 0 :
    //   LMN state machine drives:
    //     S0 = (InitialPolarity) during L state
    //     S0 = (~InitialPolarity) during M state
    //     S0 = (InitialPolarity) during N state
    Cadb20LmnCfg (MrcData, 17, 1, DDR5_CA_TRAIN_LMN_PERIOD - 1, 0);

    // Populate all of the CADB_BUF entries
    CadbCount  = ARRAY_COUNT (CadbLinesBuffer);
    CadbLength = CaParityMode ? (DDR5_CA_TRAIN_LMN_PERIOD * DDR5_FEEDBACK_CATM_NUM_SAMPLES) : DDR5_CA_TRAIN_LMN_PERIOD;

    for (TestLoop = 0; TestLoop < MaxTestLoops; TestLoop++) {
      // Run a test pattern for each victim lane
      for (VictimLane = 0; VictimLane < MaxVictim; VictimLane++) {
        // Populate CadbLinesBuffer with the pattern for the current victim lane
        PatternGenerator (VictimLane, CadbLinesBuffer);

        // Configure CADB with the new test pattern
        SetupCaTrainingCadb (MrcData, Rank, CadbLinesBuffer, CadbCount, PatBuf, UniseqMode);

        for (Index = 0; Index < DataTrainFeedbackNumSamplesPerPiCode; Index++) {
          // Reset the DataTrainFeedback (and start sampling)
          IoReset (MrcData);
          // Run the CADB test pattern and wait for it to complete
          MrcCustomRunCADB (MrcData, McChannelMask, CadbCustomLengthRun, CadbLength);
          // Wait for the DRAM to output the results from the last test
          MrcWait (MrcData, MRC_DDR5_tCATM_VALID_NS);

          // Get the result on all channel bytes:
          for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
            for (Channel = 0; Channel < MaxChannel; Channel++) {
              if (!MrcRankExist (MrcData, Controller, Channel, Rank)) {
                continue;
              }
              for (Byte = 0; Byte < Outputs->SdramCount; Byte++) {
                if (!MrcByteExist (MrcData, Controller, Channel, Byte)) {
                  continue;
                }
                MrcGetSetChStrb (MrcData, Controller, Channel, Byte, GsmIocDataTrainFeedback, ReadUncached, &DataTrainFeedbackGetSet);
                FeedbackMask = (Byte == (MAX_BYTE_IN_DDR5_CHANNEL - 1)) ? FEEDBACKMASK_ECC : FEEDBACKMASK;
                if (!CaParityMode) {
                  DataTrainFeedback = (UINT8) DataTrainFeedbackGetSet & FeedbackMask;  // Get only DQ bits, not DQS
                  // A value of 0 indicates a pass (no 1's seen on DQ)
                  TestResult[Controller][Channel][Byte] |= (UINT8) DataTrainFeedback;  // OR'ing the results in case SW polling is used
                } else {
                  DataTrainFeedback = (UINT8) DataTrainFeedbackGetSet;
                  TestResult[Controller][Channel][Byte] = MAX (TestResult[Controller][Channel][Byte], (UINT8) DataTrainFeedback); // Keep the largest counter value across test loops / victim lanes
                }
              } // Byte
            } // Channel
          } // Controller
        } // SW feedback loop
      } // VictimLane

      if (Phase == MrcTrainingPhase0) {
        // Setup the next loop to use the Lone 1 pattern
        PatBuf = CadbLinesDdr5CaTrainLmnLoneOne;
      }
    } // TestLoop
  }
}

/**
  Sweep CS/CMD PI in the given range and find the largest passing eye for all bytes.

  @param[in] MrcData       - The MRC global data.
  @param[in] Iteration     - The command bus to target (CMD or CTL)
  @param[in] Phase         - The sub training step for the current bus
  @param[in] McChannelMask - The channels to include in the 1D sweep
  @param[in] Rank          - The rank to incldue in the 1D sweep
  @param[in] Start         - The starting PI code value
  @param[in] Stop          - The ending PI code value
  @param[in] Step          - The increments between PI code values
  @param[out] Left         - Return buffer for the left passing limit for each byte
  @param[out] Right        - Return buffer for the right passing limit for each byte

  @retval mrcSuccess if succeeded.
**/
MrcStatus
FindCsTrainingLimits (
  IN  MrcParameters *const  MrcData,
  IN  MrcIterationType      Iteration,
  IN  MrcTrainingPhase      Phase,
  IN  UINT8                 McChannelMask,
  IN  UINT32                Rank,
  IN  UINT32                Start,
  IN  UINT32                Stop,
  IN  UINT32                Step,
  OUT UINT32                Left[MAX_CONTROLLER][MAX_CHANNEL][MAX_RANK_IN_CHANNEL][MAX_SDRAM_IN_DIMM],
  OUT UINT32                Right[MAX_CONTROLLER][MAX_CHANNEL][MAX_RANK_IN_CHANNEL][MAX_SDRAM_IN_DIMM]
  )
{
  MrcDebug           *Debug;
  MrcOutput          *Outputs;
  MrcStatus          Status;
  UINT32             Controller;
  UINT32             Channel;
  UINT32             Byte;
  UINT8              MaxChannel;
  UINT32             PiCode;
  BOOLEAN            Pass;
  UINT32             InitialPassingStart[MAX_CONTROLLER][MAX_CHANNEL][MAX_SDRAM_IN_DIMM];
  UINT32             InitialPassingEnd[MAX_CONTROLLER][MAX_CHANNEL][MAX_SDRAM_IN_DIMM];
  UINT32             CurrentPassingStart[MAX_CONTROLLER][MAX_CHANNEL][MAX_SDRAM_IN_DIMM];
  UINT32             CurrentPassingEnd[MAX_CONTROLLER][MAX_CHANNEL][MAX_SDRAM_IN_DIMM];
  UINT8              TestResult[MAX_CONTROLLER][MAX_CHANNEL][MAX_SDRAM_IN_DIMM];
  UINT32             iWidth;
  UINT32             cWidth;
  UINT32             lWidth;
  BOOLEAN            WrapAround;
  BOOLEAN            CadbTestRunning;

  Status        = mrcSuccess;
  Outputs       = &MrcData->Outputs;
  Debug         = &Outputs->Debug;
  MaxChannel    = Outputs->MaxChannels;
  CadbTestRunning = FALSE;
  // CS and Clock are cyclical, allow the result to wrap around
  WrapAround    = (Iteration == MrcIterationCtl);

  // Print the headers
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "    Mc0Ch0  Mc0Ch1  Mc1Ch0  Mc1Ch1\n PI ");
  for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
    for (Channel = 0; Channel < MaxChannel; Channel++) {
      for (Byte = 0; Byte < Outputs->SdramCount; Byte++) {
        MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, " %d   ", Byte);
      }
      MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "  ");
    }
  }
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\n");

  if (Iteration == MrcIterationCmd) {
    // Delay CS by 1tCK to workaround CADB CA\CS_N phase alignment
    MrcOffsetCsCaTiming (MrcData, 128, 0);
  }

  // Start the sweep
  for (PiCode = Start; PiCode < Stop; PiCode += Step) {
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "%3d:", PiCode);
    for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
      for (Channel = 0; Channel < MaxChannel; Channel++) {
        if (!MrcRankExist (MrcData, Controller, Channel, Rank)) {
          continue;
        }
        // Shift the CS/CA PI
        ShiftPIforCmdTraining (MrcData, Controller, Channel, Iteration, MRC_BIT0 << Rank, 0x3, PiCode, 0);
      }
    }

    // Run the CADB Test Pattern and sample the results at the current PI code
    MrcDdr5EctRunCadbTest (MrcData, Iteration, Phase, McChannelMask, Rank, &CadbTestRunning, TestResult);

    // Read and process the results
    for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
      for (Channel = 0; Channel < MaxChannel; Channel++) {
        if (!MrcRankExist (MrcData, Controller, Channel, Rank)) {
          MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "          ");
          continue;
        }
        for (Byte = 0; Byte < Outputs->SdramCount; Byte++) {
          if (!MrcByteExist (MrcData, Controller, Channel, Byte)) {
            MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "    ");
            continue;
          }

          Pass = (TestResult[Controller][Channel][Byte] == 0);
          MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, " %c %2x", Pass ? '.' : '#', TestResult[Controller][Channel][Byte]);

          if (PiCode == Start) {
            if (Pass) {
              InitialPassingStart[Controller][Channel][Byte] = InitialPassingEnd[Controller][Channel][Byte] = Start;
              CurrentPassingStart[Controller][Channel][Byte] = CurrentPassingEnd[Controller][Channel][Byte] = Start;
              Left[Controller][Channel][Rank][Byte] = Right[Controller][Channel][Rank][Byte] = Start;
            } else {
              InitialPassingStart[Controller][Channel][Byte] = InitialPassingEnd[Controller][Channel][Byte] = MRC_UINT32_MAX;
              CurrentPassingStart[Controller][Channel][Byte] = CurrentPassingEnd[Controller][Channel][Byte] = MRC_UINT32_MAX;
              Left[Controller][Channel][Rank][Byte] = Right[Controller][Channel][Rank][Byte] = MRC_UINT32_MAX;
            }
          } else {
            if (Pass) {
              if (InitialPassingEnd[Controller][Channel][Byte] == (PiCode - Step)) {
                InitialPassingEnd[Controller][Channel][Byte] = PiCode;
              }

              if (CurrentPassingEnd[Controller][Channel][Byte] == (PiCode - Step)) {
                CurrentPassingEnd[Controller][Channel][Byte] = PiCode;
              } else {
                CurrentPassingStart[Controller][Channel][Byte] = CurrentPassingEnd[Controller][Channel][Byte] = PiCode;
              }
              // Special case for last step: Append Initial Passing Region
              // PiCode should be considered a continuous range that wraps around 0
              if (WrapAround && (PiCode >= (Stop - Step)) && (InitialPassingStart[Controller][Channel][Byte] == Start)) {
                iWidth = (InitialPassingEnd[Controller][Channel][Byte] - InitialPassingStart[Controller][Channel][Byte]);
                CurrentPassingEnd[Controller][Channel][Byte] += (Step + iWidth);
              }
              // Update Largest variables
              cWidth = CurrentPassingEnd[Controller][Channel][Byte] - CurrentPassingStart[Controller][Channel][Byte];
              lWidth = Right[Controller][Channel][Rank][Byte] - Left[Controller][Channel][Rank][Byte];
              if (cWidth > lWidth) {
                Left[Controller][Channel][Rank][Byte] = CurrentPassingStart[Controller][Channel][Byte];
                Right[Controller][Channel][Rank][Byte] = CurrentPassingEnd[Controller][Channel][Byte];
              }
            }
          }
        }  // for Byte
        MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "  ");
      }  // for Channel
    }  // for Controller
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\n");
  } // for PiCode

  if (Iteration == MrcIterationCtl) {
    // Shift CS PI to 0 after the sweep, and before CADB stop.
    // This is to avoid DRAM latching a wrong command because CS PI is shifted to the right and CA pins will change value during CADB stop.
    for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
      for (Channel = 0; Channel < MaxChannel; Channel++) {
        if (MrcRankExist (MrcData, Controller, Channel, Rank)) {
          ShiftPIforCmdTraining (MrcData, Controller, Channel, MrcIterationCtl, 1 << Rank, 0x3, 0, 0);
        }
      }
    }
  }

  if (CadbTestRunning) {
    MrcCadbStopTest (MrcData, McChannelMask);
  }

  if (Iteration == MrcIterationCmd) {
    // Restore original Timing
    MrcOffsetCsCaTiming (MrcData, 0, 0);
  }

  return Status;
}

/**
  Calculate the eye width for the input sample

  @param[in] Sample       - Sample containing a left and right limit

  @return The eye width for the input sample
**/
UINT32
SampleEyeWidth (
  EDGE_SAMPLES Sample
  )
{
  return Sample.RightEdge - Sample.LeftEdge;
}

/**
  Early CS Training 1D Sweep

  @param[in] MrcData - The MRC global data.

  @retval mrcSuccess if succeeded
**/
MrcStatus
EarlyCsTrainSweep1D (
  IN  MrcParameters *const MrcData,
  OUT EDGE_SAMPLES         Samples[MAX_CONTROLLER][MAX_CHANNEL][MAX_RANK_IN_CHANNEL],
  IN  MrcIterationType     Iteration,
  IN  MrcTrainingPhase     Phase
  )
{
  MrcInput              *Inputs;
  MrcDebug              *Debug;
  MRC_FUNCTION          *MrcCall;
  MrcOutput             *Outputs;
  MrcStatus             Status;
  UINT32                Controller;
  UINT32                Channel;
  UINT32                Rank;
  UINT8                 MaxChannel;
  UINT8                 McChannelMask;
  UINT8                 RankBit;
  UINT8                 RankMask;
  UINT32                PiLow;
  UINT32                PiHigh;
  UINT32                PiStep;
  UINT32                LeftEdge[MAX_CONTROLLER][MAX_CHANNEL][MAX_RANK_IN_CHANNEL][MAX_SDRAM_IN_DIMM];
  UINT32                RightEdge[MAX_CONTROLLER][MAX_CHANNEL][MAX_RANK_IN_CHANNEL][MAX_SDRAM_IN_DIMM];
  UINT32                *Left;
  UINT32                *Right;
  INT32                 Center;
  INT32                 CenterByte0;
  UINT8                 Byte;
  EDGE_SAMPLES          Sample;
  EDGE_SAMPLES          *CompositeSample;
  UINT8                 FirstRank[MAX_CONTROLLER][MAX_CHANNEL];
  UINT8                 FirstRankIndex;
  DDR5_CA_TRAIN_IO_INIT_SAVE SaveData;
  UINT16                MinPiCode;
  UINT16                MaxPiCode;

  Inputs           = &MrcData->Inputs;
  MrcCall          = Inputs->Call.Func;
  Outputs          = &MrcData->Outputs;
  Debug            = &Outputs->Debug;
  MaxChannel       = Outputs->MaxChannels;

  Status    = mrcSuccess;
  RankMask  = Outputs->ValidRankMask;
  MrcCall->MrcSetMem ((UINT8 *) LeftEdge,  sizeof (LeftEdge),  0);
  MrcCall->MrcSetMem ((UINT8 *) RightEdge, sizeof (RightEdge), 0);

  // Get the first rank index for each channel
  // per-channel results will be stored at this index
  GetFirstRank (MrcData, FirstRank);

  for (Rank = 0; Rank < MAX_RANK_IN_CHANNEL; Rank++) {
    RankBit = 1 << Rank;
    if ((RankBit & RankMask) == 0) {
      continue;
    }
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\nRank%u: %s sweep\n", Rank, (Iteration == MrcIterationCmd) ? "Cmd" : "Ctl");

    McChannelMask = 0;     // Bitmask of Controller channels that have current rank populated
    for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
      for (Channel = 0; Channel < MaxChannel; Channel++) {
        if (!MrcRankExist (MrcData, Controller, Channel, Rank)) {
          continue;
        }
        McChannelMask |= (1 << ((Controller * MaxChannel) + Channel));
      }  // for Channel
    }


    if ((Iteration == MrcIterationCmd) && (Phase == MrcTrainingPhase0)) {
      // Simple Pattern \ Coarse Step phase

      // Find min and max CTL PI accross all channels for the current rank
      MrcGetCtlPiCodeRange (MrcData, McChannelMask, Rank, &MinPiCode, &MaxPiCode);

      // Calculate the sweep range: -96 PI from (min + 128), +96 PI from (max + 128)
      PiLow =  (MinPiCode > (512 - 32))  ? 512 : MinPiCode + 32;
      PiHigh = (MaxPiCode > (512 - 224)) ? 512 : MaxPiCode + 224;

      PiStep = (Outputs->Frequency > f3600) ? 2 : 6;
      if ((Outputs->DdrType == MRC_DDR_TYPE_DDR5) && Outputs->Gear4) {
        PiStep = 4;
      }
    } else if ((Iteration == MrcIterationCmd) && (Phase == MrcTrainingPhase1)) {
      // Aggressive Pattern \ Fine Step phase
      // Cover +/- 64 PI from current center

      // Find min and max CMD PI accross all channels
      MrcGetCmdPiCodeRange (MrcData, McChannelMask, &MinPiCode, &MaxPiCode);

      // Calculate the sweep range: From -64 PI from min, +64 PI from max
      PiLow =  (MinPiCode < 64) ? 0 : MinPiCode - 64;
      PiHigh = (MaxPiCode > (512 - 64)) ? 512 : MaxPiCode + 64;
      PiStep = 4;
    } else { // Iteration == MrcIterationCtl
      // CLK\CTL pattern is cyclical, only cover 2tCK of CTL PI
      PiLow  = 0;
      PiHigh = 256;
      PiStep = 4;
    }

    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "PiLow = %d, PiHigh = %d, PiStep = %d\n", PiLow, PiHigh, PiStep);

    // Initialize the PHY for ECT
    Ddr5CsCaTrainingInitIo (MrcData, Iteration, McChannelMask, (UINT8) Rank, MRC_ENABLE, &SaveData);

    Status = MrcDdr5DramEctTrainMode (MrcData, Iteration, McChannelMask, Rank, MRC_ENABLE);
    if (Status != mrcSuccess) {
      return Status;
    }

    FindCsTrainingLimits (MrcData, Iteration, Phase, McChannelMask, Rank, PiLow, PiHigh, PiStep, LeftEdge, RightEdge);

    // Exit CA training mode on the current rank
    Status = MrcDdr5DramEctTrainMode (MrcData, Iteration, McChannelMask, Rank, MRC_DISABLE);
    if (Status != mrcSuccess) {
      return Status;
    }

    // Process results from this rank
    for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
      for (Channel = 0; Channel < MaxChannel; Channel++) {
        if (!MrcRankExist (MrcData, Controller, Channel, Rank)) {
          continue;
        }
        MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Training results Mc%d C%d R%d\nByte Left Right\n", Controller, Channel, Rank);
        for (Byte = 0; Byte < Outputs->SdramCount; Byte++) {
          MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "%4d %4d %5d\n",
            Byte,
            LeftEdge[Controller][Channel][Rank][Byte],
            RightEdge[Controller][Channel][Rank][Byte]
            );
        }
        MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\nByte Left Right Width Center\n");

        // Initialize the Sample variable
        MrcCall->MrcSetMem ((UINT8 *)&Sample, sizeof (Sample), 0);
        CenterByte0 = 0;
        for (Byte = 0; Byte < Outputs->SdramCount; Byte++) {
          Left  = &LeftEdge[Controller][Channel][Rank][Byte];
          Right = &RightEdge[Controller][Channel][Rank][Byte];

          // Check that a passing region was found.
          if (*Right - *Left == 0) {
            // No Eye Found
            MrcCall->MrcSetMem ((UINT8 *)&Sample, sizeof (Sample), 0);
            MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "%4d (No Eye Found)\n", Byte);
            break;
          } else {
            if (Iteration == MrcIterationCtl) {
              // Align all byte results to the same cycle, using byte0 as a reference for other bytes
              Center = (*Left + *Right) / 2;
              if (Byte == 0) {
                CenterByte0 = Center;
              } else {
                if (Center > CenterByte0) {
                  while ((Center - CenterByte0) > 96) { // This byte is a cycle or more later, pull it in
                    *Left   -= ((*Left >= 128) ? 128 : *Left);
                    *Right  -= ((*Right >= 128) ? 128 : *Right);
                    Center = (*Left + *Right) / 2;
                  }
                } else if (Center < CenterByte0) {
                  while ((CenterByte0 - Center) > 96) {  // This byte is a cycle or more earlier, push it out
                    *Left  += 128;
                    *Right += 128;
                    Center = (*Left + *Right) / 2;
                  }
                }
              }
            }

            // Create a composite result of all bytes
            if (Byte == 0) {
              Sample.RightEdge = *Right;
              Sample.LeftEdge  = *Left;
            } else {
              Sample.RightEdge = MIN (Sample.RightEdge, *Right);
              Sample.LeftEdge  = MAX (Sample.LeftEdge, *Left);
              if ((INT32) SampleEyeWidth(Sample) <= 0) {
                // If the eye width ever goes negative, the the composite result has no eye.
                MrcCall->MrcSetMem ((UINT8 *)&Sample, sizeof (Sample), 0);
                MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "%4d (No Eye Found)\n", Byte);
              }
            }

            MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "%4d %4d %5d %5d %6d\n",
              Byte,
              *Left,
              *Right,
              *Right - *Left,
              (*Left + *Right) / 2
              );
          }
        } // Byte
        Samples[Controller][Channel][Rank] = Sample;
        MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "---> %4d %5d %5d %6d\n",
          Sample.LeftEdge,
          Sample.RightEdge,
          Sample.RightEdge - Sample.LeftEdge,
          (Sample.LeftEdge + Sample.RightEdge) / 2
        );
      } // for Channel
    } // for Controller
    // Restore the PHY settings for the current rank
    Ddr5CsCaTrainingInitIo (MrcData, Iteration, McChannelMask, (UINT8) Rank, MRC_DISABLE, &SaveData);
  } // for Rank

  if (Iteration == MrcIterationCmd) {
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Per Channel Composite Results\n");
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "MC CH Left Right Width Center\n");
    // Create composite eye for Channel, store the data on the FirstRank
    for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
      for (Channel = 0; Channel < MaxChannel; Channel++) {
        if (!MrcChannelExist (MrcData, Controller, Channel)) {
          continue;
        }
        FirstRankIndex = FirstRank[Controller][Channel];
        CompositeSample = &Samples[Controller][Channel][FirstRankIndex];
        for (Rank = 0; Rank < MAX_RANK_IN_CHANNEL; Rank++) {
          if (!MrcRankExist (MrcData, Controller, Channel, Rank)) {
            continue;
          }
          if ((Samples[Controller][Channel][Rank].LeftEdge == 0) && (Samples[Controller][Channel][Rank].RightEdge == 0)) {
            CompositeSample->LeftEdge = CompositeSample->RightEdge = 0;
          } else {
            CompositeSample->LeftEdge  = MAX (CompositeSample->LeftEdge,  Samples[Controller][Channel][Rank].LeftEdge);
            CompositeSample->RightEdge = MIN (CompositeSample->RightEdge, Samples[Controller][Channel][Rank].RightEdge);
            if (CompositeSample->LeftEdge > CompositeSample->RightEdge) {
              // Eyes do not intersect, hence composite eye is empty
              CompositeSample->LeftEdge = CompositeSample->RightEdge = 0;
            }
          }
        }

        MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "%2d %2d %4d %5d %5d %6d\n",
          Controller,
          Channel,
          CompositeSample->LeftEdge,
          CompositeSample->RightEdge,
          CompositeSample->RightEdge - CompositeSample->LeftEdge,
          (CompositeSample->LeftEdge + CompositeSample->RightEdge) / 2
        );
      } // Rank
    } // Channel
  } // Controller

  return Status;
}

/**
  Issue VREFCA command using MRH (Mode Register Handler).

  @param[in] MrcData    - Include all MRC global data.
  @param[in] Controller - the controller to work on
  @param[in] Channel    - The channel to work on
  @param[in] Rank       - The rank to work on
  @param[in] Opcode     - VREFCA Opcode OP[7:0]
  @param[in] DebugPrint - When TRUE, will print debugging information

  @retval mrcSuccess               - VREFCA was sent successfully
  @retval mrcDeviceBusy            - Timed out waiting for MRH
  @retval mrcUnsupportedTechnology - The memory technology is not supported
**/
void
MrcDdr5SetVrefCs (
  IN MrcParameters *const MrcData,
  IN UINT32               Controller,
  IN UINT32               Channel,
  IN UINT32               Rank,
  IN DDR5_MR10_VREF       VrefCs,
  IN BOOLEAN              DebugPrint
  )
{
  MrcRankOut *RankOut;

  // Save current VrefCs in the host struct
  RankOut = &MrcData->Outputs.Controller[Controller].Channel[Channel].Dimm[Rank / 2].Rank[Rank % 2];
  RankOut->MR[mrIndexMR12] = DDR5_VREFCS (VrefCs);

  MrcIssueVrefCa (MrcData, Controller, Channel, Rank, DDR5_VREFCS (VrefCs), DebugPrint);
  MrcIssueMpc (MrcData, Controller, Channel, Rank, DDR5_MPC_APPLY_VREF_RTT, DebugPrint);
}


/**
  Issue VREFCA command using MRH (Mode Register Handler).

  @param[in] MrcData    - Include all MRC global data.
  @param[in] Controller - the controller to work on
  @param[in] Channel    - The channel to work on
  @param[in] Rank       - The rank to work on
  @param[in] Opcode     - VREFCA Opcode OP[7:0]
  @param[in] DebugPrint - When TRUE, will print debugging information

  @retval mrcSuccess               - VREFCA was sent successfully
  @retval mrcDeviceBusy            - Timed out waiting for MRH
  @retval mrcUnsupportedTechnology - The memory technology is not supported
**/
void
MrcDdr5SetVrefCa (
  IN MrcParameters *const MrcData,
  IN UINT32               Controller,
  IN UINT32               Channel,
  IN UINT32               Rank,
  IN DDR5_MR10_VREF       VrefCa,
  IN BOOLEAN              DebugPrint
  )
{
  MrcInput   *Inputs;
  MrcOutput  *Outputs;
  MrcRankOut *RankOut;
  UINT32 RankHalf;
  UINT32 RankMod2;
  UINT32 Byte;

  Inputs   = &MrcData->Inputs;
  Outputs  = &MrcData->Outputs;
  RankHalf = Rank / 2;
  RankMod2 = Rank % 2;

  RankOut = &Outputs->Controller[Controller].Channel[Channel].Dimm[RankHalf].Rank[RankMod2];

  if (Inputs->EnablePda) {
    // Save CMD Per-byte for PDA use
    for (Byte = 0; Byte < Outputs->SdramCount; Byte++) {
      RankOut->DdrPdaVrefCmd[Byte] = VrefCa;
    }
  }

  // Save global CMD Vref
  RankOut->MR[mrIndexMR11] = VrefCa;

  MrcIssueVrefCa (MrcData, Controller, Channel, Rank, VrefCa, DebugPrint);
  MrcIssueMpc (MrcData, Controller, Channel, Rank, DDR5_MPC_APPLY_VREF_RTT, DebugPrint);
}

void
Ddr5SetVrefAllRanks (
  IN MrcParameters *const MrcData,
  IN MRC_SET_VREF         SetVref,
  IN DDR5_MR10_VREF       VrefCa
  )
{
  UINT32 Controller;
  UINT32 Channel;
  UINT32 Rank;

  // Test every rank on every channel on each vref
  for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
    for (Channel = 0; Channel < MAX_CHANNEL; Channel++) {
      for (Rank = 0; Rank < MAX_RANK_IN_CHANNEL; Rank++) {
        if (!MrcRankExist (MrcData, Controller, Channel, Rank)) {
          continue;
        }
        SetVref (MrcData, Controller, Channel, Rank, VrefCa, MRC_PRINTS_OFF);
      } // Rank
    } // Channel
  } // Controller
}


/**
  Early CS / CLK Training for DDR5

  @param[in] MrcData - The MRC global data.

  @retval mrcSuccess if succeeded
**/
MrcStatus
Sweep2D (
  IN MrcParameters *const MrcData,
  IN EDGE_SAMPLES     Samples[][MAX_CONTROLLER][MAX_CHANNEL][MAX_RANK_IN_CHANNEL],
  IN UINT32           VrefSamples,
  IN UINT8            VrefStepSize,
  IN DDR5_MR10_VREF   VrefStart,
  IN MRC_SET_VREF     SetVref,
  IN MrcIterationType Iteration,
  IN MrcTrainingPhase Phase
  )
{
  MRC_FUNCTION    *MrcCall;
  MrcStatus       Status;
  MrcDebug        *Debug;
  MrcOutput       *Outputs;
  MrcChannelOut   *ChannelOut;
  UINT32          Controller;
  UINT32          Channel;
  UINT32          Rank;
  UINT32          DimmRank0;
  UINT32          DimmRank1;
  DDR5_MR10_VREF  Vref;
  UINT8           VrefIndex;
  UINT8           NextVrefIndex;
  UINT8           PrevVrefIndex;
  UINT8           BestVrefIndex;
  UINT32          EyeWidthAverage;
  UINT32          EyeWidth;
  UINT32          BestEyeWidth;
  UINT32          BestEyeWidthAverage;
  UINT32          LeftEdge;
  UINT32          RightEdge;
  EDGE_SAMPLES    CurrentSample;
  EDGE_SAMPLES    PrevSample;
  EDGE_SAMPLES    NextSample;
  UINT8           RankMask;
  INT32           BestSampleOffset;
  INT32           CenterFirstRank[MAX_CONTROLLER][MAX_CHANNEL];
  INT32           CenterFirst;
  BOOLEAN         CenterFirstDone;
  BOOLEAN         CatmPhase1;
  BOOLEAN         Is2DPC;
  BOOLEAN         PullIn;
  BOOLEAN         ShiftThisRank[MAX_RANK_IN_CHANNEL];
  INT32           Center;
  UINT8           FirstRank[MAX_CONTROLLER][MAX_CHANNEL];
  INT64           GetSetVal;
  UINT32          CsPi[MAX_RANK_IN_CHANNEL];
  INT32           CsDelta[MAX_RANK_IN_CHANNEL];
  UINT32          MaxCsDelay;
  UINT32          MinCsDelay;
  UINT32          CsTarget;
  INT32           ShiftValue;

  Outputs        = &MrcData->Outputs;
  Debug          = &Outputs->Debug;
  MrcCall        = MrcData->Inputs.Call.Func;
  BestSampleOffset = 0;
  MrcCall->MrcSetMem ((UINT8 *) CenterFirstRank, sizeof (CenterFirstRank), 0);
  MrcCall->MrcSetMem ((UINT8 *) CsDelta, sizeof (CsDelta), 0);
  MrcCall->MrcSetMem ((UINT8 *) CsPi, sizeof (CsPi), 0);
  Status = mrcSuccess;
  CatmPhase1 = (Iteration == MrcIterationCmd) && (Phase == MrcTrainingPhase1);

  for (VrefIndex = 0; VrefIndex < VrefSamples; VrefIndex++) {
    // Calculate the Vref for the current index
    Vref = VrefStart + (VrefStepSize * VrefIndex);
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Vref: %u\n", Vref);

    if (!CatmPhase1) {
      // Set the Vref on all DRAM devices
      Ddr5SetVrefAllRanks (MrcData, SetVref, Vref);
    }

    // Run a 1D Sweep on all ranks at the current VREF
    EarlyCsTrainSweep1D (MrcData, Samples[VrefIndex], Iteration, Phase);
    if (CatmPhase1) {
      break;
    }
  } // VrefIndex

  // Print VREF header
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "VREF:     ");
  for (VrefIndex = 0; VrefIndex < VrefSamples; VrefIndex++) {
    // Calculate the Vref for the current index
    Vref = VrefStart + (VrefStepSize * VrefIndex);

    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, " %5d[%3d] ", Vref, VrefIndex);
  } // VrefIndex
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\n");

  // Get the first rank index for each channel
  // EarlyCsTrainSweep1D returns per-channel results on the first rank index
  GetFirstRank (MrcData, FirstRank);

  // Process the Vref sweep results
  CenterFirstDone = FALSE;
  CenterFirst = 0;
  for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
    for (Channel = 0; Channel < MAX_CHANNEL; Channel++) {
      for (Rank = 0; Rank < MAX_RANK_IN_CHANNEL; Rank++) {
        if (!MrcRankExist (MrcData, Controller, Channel, Rank)) {
          continue;
        }
        MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "MC%dCH%dR%d: ", Controller, Channel, Rank);
        // Check all of the Vref samples for the current rank to find the one with the widest eye

        BestEyeWidthAverage = 0;
        BestEyeWidth = 0;
        BestVrefIndex = 0;

        // Normalize the CTL PI settings across all channels / ranks, to align to the same cycle
        if (Iteration == MrcIterationCtl) {
          for (VrefIndex = 0; VrefIndex < VrefSamples; VrefIndex++) {
            // Align all rank results over all VrefIndex points to the same cycle, using CenterFirst as a reference for other ranks/vrefs
            LeftEdge  = Samples[VrefIndex][Controller][Channel][Rank].LeftEdge;
            RightEdge = Samples[VrefIndex][Controller][Channel][Rank].RightEdge;
            Center = (LeftEdge + RightEdge) / 2;
            if (!CenterFirstDone && (VrefIndex == 0)) {
              CenterFirst = Center;
              while (LeftEdge >= 128) { // Normalize to the lowest possible cycle
                LeftEdge    -= 128;
                RightEdge   -= 128;
                CenterFirst -= 128;
              }
              CenterFirstDone = TRUE;
            } else {
              if (Center > CenterFirst) {
                while ((Center - CenterFirst) > 64) { // This rank is a cycle or more later, pull it in
                  LeftEdge  -= ((LeftEdge >= 128) ? 128 : LeftEdge);
                  RightEdge -= ((RightEdge >= 128) ? 128 : RightEdge);
                  Center = (LeftEdge + RightEdge) / 2;
                }
              } else if (Center < CenterFirst) {
                while ((CenterFirst - Center) > 64) {  // This rank is a cycle or more earlier, push it out
                  LeftEdge  += 128;
                  RightEdge += 128;
                  Center = (LeftEdge + RightEdge) / 2;
                }
              }
            }
            // Save normalized left/right edge
            Samples[VrefIndex][Controller][Channel][Rank].LeftEdge  = LeftEdge;
            Samples[VrefIndex][Controller][Channel][Rank].RightEdge = RightEdge;
          } // for VrefIndex
        } // Iteration == MrcIterationCtl

        for (VrefIndex = 0; VrefIndex < VrefSamples; VrefIndex++) {
          // Use a 3 point moving average to determine the best VREF
          PrevVrefIndex = (VrefIndex == 0) ? VrefIndex : (VrefIndex - 1);
          NextVrefIndex = ((UINT32) (VrefIndex + 1) >= VrefSamples) ? VrefIndex : (VrefIndex + 1);
          PrevSample    = Samples[PrevVrefIndex][Controller][Channel][Rank];
          CurrentSample = Samples[VrefIndex][Controller][Channel][Rank];
          NextSample    = Samples[NextVrefIndex][Controller][Channel][Rank];

          EyeWidthAverage = (SampleEyeWidth (PrevSample) + SampleEyeWidth (CurrentSample) + SampleEyeWidth (NextSample)) / 3;
          EyeWidth = SampleEyeWidth (CurrentSample);
          MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, " %5d(%3d) ", EyeWidthAverage, EyeWidth);
          if ((EyeWidthAverage > BestEyeWidthAverage) && (EyeWidth != 0)) {
            BestEyeWidthAverage = EyeWidthAverage;
            BestEyeWidth = EyeWidth;
            BestSampleOffset = (CurrentSample.LeftEdge + CurrentSample.RightEdge) / 2;
            BestVrefIndex = VrefIndex;
          }
        } // VrefIndex

        if (BestEyeWidth == 0) {
          MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, " (No Eye Found)\n");
          Status = mrcFail;
        } else {
          Vref = VrefStart + (VrefStepSize * BestVrefIndex);
          if ((Iteration == MrcIterationCmd) && (Phase == MrcTrainingPhase1)) {
            // CATM is done using a shifted CS, so shift the final CA PI back
            BestSampleOffset -= 64;
            BestSampleOffset = MAX (0, BestSampleOffset);  // Protect against negative values
          }
          MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, " (Vref(Index):%4d(%d), PI:%3d)\n", Vref, BestVrefIndex, BestSampleOffset);
          // CSTM: apply the best CS VREF and CS PI for each rank
          // CATM: apply the best CA VREF for each rank, and best composite CA PI for the channel
          RankMask = (MRC_BIT0 << Rank);
          if (!CatmPhase1) {
            SetVref (MrcData, Controller, Channel, Rank, Vref, MRC_PRINTS_OFF);
          }
          if ((Iteration != MrcIterationCmd) || (Rank == FirstRank[Controller][Channel])) {
            if (Iteration == MrcIterationCtl) {
              CsPi[Rank] = BestSampleOffset;
            }
            ShiftPIforCmdTraining (MrcData, Controller, Channel, Iteration, RankMask, MRC_IGNORE_ARG_8, BestSampleOffset, TRUE);
          }
        }
      } // Rank

      if (!MrcChannelExist (MrcData, Controller, Channel)) {
        continue;
      }

      // Shift CLK/CTL across two DIMMs in 2DPC case to enlarge the composite CMD eye
      // This is done after CSTM and before CATM
      ChannelOut = &Outputs->Controller[Controller].Channel[Channel];
      Is2DPC = (ChannelOut->DimmCount == 2);
      if (Is2DPC && (Iteration == MrcIterationCtl)) {
        // In 2DPC case Rank0 must be always present, so initialize Min/Max from Rank0 and then find Mix/Max across all ranks
        MaxCsDelay = CsPi[0];
        MinCsDelay = CsPi[0];
        for (Rank = 0; Rank < MAX_RANK_IN_CHANNEL; Rank++) {
          if (MrcRankExist (MrcData, Controller, Channel, Rank)) {
            MaxCsDelay = MAX (CsPi[Rank], MaxCsDelay);
            MinCsDelay = MIN (CsPi[Rank], MinCsDelay);
          }
        }
        MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "[Mc%u.C%u] MaxCsDelay: %d  MinCsDelay: %d\n", Controller, Channel, MaxCsDelay, MinCsDelay);

        if (MinCsDelay > 128) {
          PullIn    = TRUE;
          CsTarget  = MinCsDelay;
        } else {
          PullIn    = FALSE;
          CsTarget  = MaxCsDelay;
        }
        MRC_DEBUG_MSG(Debug, MSG_LEVEL_NOTE, " PullIn: %u, CsTarget: %u\n", PullIn, CsTarget);

        MrcCall->MrcSetMem ((UINT8 *) ShiftThisRank, sizeof (ShiftThisRank), 0);

        // Find out which DIMMs will have CLK/CTL shifted
        // This has do be done on a DIMM level, because CLK PI is per DIMM in DDR5
        for (Rank = 0; Rank < MAX_RANK_IN_CHANNEL; Rank++) {
          if (!MrcRankExist (MrcData, Controller, Channel, Rank)) {
            continue;
          }
          CsDelta[Rank] = (INT32) CsTarget - (INT32) CsPi[Rank];
          MRC_DEBUG_MSG(Debug, MSG_LEVEL_NOTE, " R%u CsPi: %u, CsDelta: %d\n", Rank, CsPi[Rank], CsDelta[Rank]);

          if (ABS (CsDelta[Rank]) >= 24) {
            DimmRank0 = Rank & 0x2;           // Rank0 of this DIMM
            DimmRank1 = Rank | 0x1;           // Rank1 of this DIMM
            ShiftThisRank[DimmRank0] = TRUE;
            ShiftThisRank[DimmRank1] = TRUE;
            if (ChannelOut->Dimm[Rank / 2].RankInDimm == 2) {
              // On a 2R DIMM, shift by the average value of both ranks
              CsDelta[DimmRank0] = (INT32) CsTarget - (INT32) (CsPi[DimmRank0] + CsPi[DimmRank1]) / 2;
            }
          }
        }

        MrcBlockTrainResetToggle (MrcData, FALSE); // We are going to change CLK/CTL PI values below

        for (Rank = 0; Rank < MAX_RANK_IN_CHANNEL; Rank++) {
          if (!MrcRankExist (MrcData, Controller, Channel, Rank)) {
            continue;
          }
          if (!ShiftThisRank[Rank]) {
            continue;
          }
          RankMask = 1 << Rank;
          // CLK PI is per DIMM in DDR5, hence shift both ranks in a DIMM by the same value (CLK and CS)
          if ((Rank == 0) || (Rank == 1)) {
            ShiftValue = CsDelta[0];
          } else { // R2 or R3
            ShiftValue = CsDelta[2];
          }

          CsPi[Rank] = CsPi[Rank] + ShiftValue;
          MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, " R%u CS PI shift by %d, new CS PI: %u\n", Rank, ShiftValue, CsPi[Rank]);
          ShiftPIforCmdTraining (MrcData, Controller, Channel, MrcIterationCtl, RankMask, MRC_IGNORE_ARG_8, CsPi[Rank], TRUE);

          MrcGetSetCcc (MrcData, Controller, Channel, Rank, 0, ClkGrpPi, ReadFromCache, &GetSetVal);
          MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, " R%u CLK PI: %u, shift by %d\n", Rank, (UINT32) GetSetVal, ShiftValue);
          ShiftPIforCmdTraining (MrcData, Controller, Channel, MrcIterationClock, RankMask, MRC_IGNORE_ARG_8, ShiftValue, TRUE);
        } // Rank
        MrcBlockTrainResetToggle (MrcData, TRUE);
        MrcGetSetCcc (MrcData, Controller, Channel, 0, 0, ClkGrpPi, ReadFromCache | PrintValue, &GetSetVal);
        MrcGetSetCcc (MrcData, Controller, Channel, 1, 0, ClkGrpPi, ReadFromCache | PrintValue, &GetSetVal);
        MrcGetSetCcc (MrcData, Controller, Channel, 2, 0, ClkGrpPi, ReadFromCache | PrintValue, &GetSetVal);
        MrcGetSetCcc (MrcData, Controller, Channel, 3, 0, ClkGrpPi, ReadFromCache | PrintValue, &GetSetVal);
      } // 2DPC and CTL
    } // Channel
  } // Controller
  return Status;
}

/**
  Early CS Training for DDR5

  Trains for optimal CtlGrpPi and DRAM VrefCS.

  @param[in] MrcData - The MRC global data.

  @retval mrcSuccess if succeeded
**/
extern
MrcStatus
MrcDdr5EarlyCsTraining (
  IN MrcParameters *const MrcData
  )
{
  MrcStatus     Status;
  MrcInput      *Inputs;
  MrcOutput     *Outputs;
  MrcDebug      *Debug;
  MRC_FUNCTION  *MrcCall;
  EDGE_SAMPLES Samples[MRC_CS_VREF_SAMPLES][MAX_CONTROLLER][MAX_CHANNEL][MAX_RANK_IN_CHANNEL];

  Inputs         = &MrcData->Inputs;
  Outputs        = &MrcData->Outputs;
  MrcCall        = Inputs->Call.Func;
  Debug          = &Outputs->Debug;

  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\n%s %s %s\n", "----", __FUNCTION__, "----");

  // Init Sample array
  MrcCall->MrcSetMem ((UINT8 *) Samples, sizeof (Samples), 0);

  Status = Sweep2D (
             MrcData,
             Samples,
             MRC_CS_VREF_SAMPLES,
             MRC_CS_VREF_STEP_SIZE,
             MRC_CS_VREF_START,
             MrcDdr5SetVrefCs,
             MrcIterationCtl,
             MrcTrainingPhase0
             );
  return Status;
}

/**
  Early CA Training for DDR5

  Trains for optimal CmdGrpPi and DRAM VrefCA.

  @param[in] MrcData - The MRC global data.

  @retval mrcSuccess if succeeded
**/
extern
MrcStatus
MrcDdr5EarlyCaTraining (
  IN MrcParameters *const MrcData
  )
{
  MrcStatus     PhaseStatus;
  MrcStatus     Status;
  MrcInput      *Inputs;
  MrcOutput     *Outputs;
  MrcDebug      *Debug;
  MRC_FUNCTION  *MrcCall;
  EDGE_SAMPLES Samples[MRC_CA_VREF_SAMPLES][MAX_CONTROLLER][MAX_CHANNEL][MAX_RANK_IN_CHANNEL];
  MrcTrainingPhase Phase;

  Inputs         = &MrcData->Inputs;
  Outputs        = &MrcData->Outputs;
  MrcCall        = Inputs->Call.Func;
  Debug          = &Outputs->Debug;
  Status         = mrcSuccess;

  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\n%s %s %s\n", "----", __FUNCTION__, "----");

  for (Phase = 0; Phase <= MrcTrainingPhase1; Phase++) {
    // Init Sample array
    MrcCall->MrcSetMem ((UINT8 *) Samples, sizeof (Samples), 0);
    PhaseStatus = Sweep2D (
                    MrcData,
                    Samples,
                    MRC_CA_VREF_SAMPLES,
                    MRC_CA_VREF_STEP_SIZE,
                    MRC_CA_VREF_START,
                    MrcDdr5SetVrefCa,
                    MrcIterationCmd,
                    Phase
                    );
    if (PhaseStatus != mrcSuccess) {
      Status = mrcFail;
    }
  }
  return Status;
}

/**
  Early Command Training for DDR5

  Executes DDR5 CS and CA training.

  @param[in] MrcData - The MRC global data.

  @retval mrcSuccess if succeeded
**/
extern
MrcStatus
EarlyCommandTrainingDdr5 (
  IN MrcParameters *const MrcData
  )
{
  MrcStatus Status;
  MrcOutput *Outputs;

  Outputs = &MrcData->Outputs;
  Status = mrcSuccess;

  if (MrcDdr5EarlyCsTraining (MrcData) != mrcSuccess) {
    Status = mrcFail;
  }
  if (MrcDdr5EarlyCaTraining (MrcData) != mrcSuccess) {
    Status = mrcFail;
  }

  // Setting EctDone will alow the JedecInit sequence to issue
  // MRW commands which require a trained CMD\CTL bus.
  Outputs->EctDone = TRUE;

  // Issue JEDEC reset/init before DQ mapping, this will program RL (MR0) which is needed for DQ mapping
  if (MrcResetSequence (MrcData) != mrcSuccess) {
    Status = mrcFail;
  }

  // Map DQ/DQS Swizzle for DDR5, can only run after CATM/CSTM have completed
  if (Outputs->BitByteSwizzleFound == FALSE) {
    if (MapDqDqsSwizzle (MrcData) != mrcSuccess) {
      Status = mrcFail;
    } else {
      // Program DESWIZZLE_HIGH/LOW registers
      ProgramDeswizzleRegisters (MrcData);
    }
  }

  // Issue JEDEC reset/init after DQ mapping, so that PDA enumeration will be correct
  if (MrcResetSequence (MrcData) != mrcSuccess) {
    Status = mrcFail;
  }

  return Status;
}
