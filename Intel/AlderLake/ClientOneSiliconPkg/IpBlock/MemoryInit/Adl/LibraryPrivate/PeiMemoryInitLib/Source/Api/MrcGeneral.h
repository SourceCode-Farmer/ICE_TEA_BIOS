/** @file
  MRC Common / Generic functions

@copyright
  INTEL CONFIDENTIAL
  Copyright 2011 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains an 'Intel Peripheral Driver' and is uniquely identified as
  "Intel Reference Module" and is licensed for Intel CPUs and chipsets under
  the terms of your license agreement with Intel or your vendor. This file may
  be modified by the user, subject to additional terms of the license agreement.

@par Specification Reference:
**/
#ifndef _MrcGeneral_h_
#define _MrcGeneral_h_


#include "MrcTypes.h"
#include "MrcApi.h"
#include "MrcAddressDecodeConfiguration.h"
#include "MrcCommon.h"
#include "MrcCrosser.h"
#include "MrcDebugHook.h"
#include "MrcPowerModes.h"
#include "MrcSpdProcessing.h"
#include "MrcMemoryMap.h"
#include "MrcGlobal.h"


#define PCH_PWRM_BASE_ADDRESS                       0xFE000000     ///< PMC MBAR MMIO base address
#define R_PCH_PWRM_GEN_PMCON_A                      0x1020
#define B_PCH_PWRM_GEN_PMCON_A_MEM_SR_MRC           MRC_BIT21
#define B_PCH_PWRM_GEN_PMCON_A_DISB_MRC             MRC_BIT23

///
/// Define the total memory size of a channel, see CAPID0_A.DDRSZ
///
typedef enum {
  tcs64GB,  ///< 64 GB per channel (Unlimited)
  tcs8GB,   ///< 8 GB
  tcs4GB,   ///< 4 GB
  tcs2GB    ///< 2 GB
} MrcTotalChannelSize;

///
/// IBECC OPERATION MODE, see MC0_IBECC_CONTROL_STRUCT.OPERATION_MODE
///
typedef enum {
  IbeccModeProtectedRanges  = 0,   ///< Functional Mode. (performs range checks)
  IbeccModeNotProtected     = 1,   ///< Makes all requests Non protected and ignore range checks
  IbeccModeProtectedAll     = 2,   ///< Makes all requests protected and ignore range checks
} MrcIbeccMode;

///
/// IBECC ERROR INJECTION MODE, see MC0_IBECC_INJ_CONTROL_STRUCT.ECC_Inj
///
typedef enum {
  IbeccErrInjNoErrorInjection     = 0, ///< No Error Injection
  IbeccErrInjCorrAddressMatch     = 1, ///< Inject Correctable Error Address match
  IbeccErrInjCorrCountInsertion   = 3, ///< Inject Correctable Error on insertion counter
  IbeccErrInjUncorrAddressMatch   = 5, ///< Inject Uncorrectable Error Address match
  IbeccErrInjUncorrCountInsertion = 7  ///< Inject Uncorrectable Error on insertion counter
} MrcIbeccErrInjMode;

extern const MrcFrequency SagvFreqPor[MAX_MRC_DDR_TYPE - 1][MAX_SAGV_POINTS];
extern const UINT8 SaGvGearPor[MAX_MRC_DDR_TYPE - 1][MAX_SAGV_POINTS];


/**
  This function returns the current configuration of Frequency and Gear ratio based on the
  current SAGV point, DDR type, and SAGV input parameters: SaGvFreq, SaGvGear.

  It will update FreqOut and GearOut with the result.

  @param[in]  MrcData   - Pointer to global MRC data.
  @param[in]  SaGvPoint - Current operating SAGV point.
  @param[out] FreqOut   - Pointer to return the SAGV point Frequency.
  @param[out] GearOut   - Pointer to return the SAGV point Gear.
**/
VOID
MrcGetSagvConfig (
  IN OUT  MrcParameters *const  MrcData,
  IN      MrcSaGvPoint          SaGvPoint,
  OUT     MrcFrequency          *FreqOut,
  OUT     BOOLEAN               *GearOut
  );

/**
  This function changes the MC to normal mode, enables the ECC if needed, lock configuration and set PU_MRC_Done.
  If the ECC is enabled, this function should be called after memory is cleaned.

  @param[in, out] MrcData - Include all MRC global data.

  @retval Always returns mrcSuccess.
**/
extern
MrcStatus
MrcMcActivate (
  IN OUT MrcParameters *const MrcData
  );

/**
  Program MC/CPGC engines to either Normal mode of operation
  or CPGC training mode.

  @param[in] MrcData  - The MRC general data.
  @param[in] Mode     - TRUE for Normal mode, FALSE for CPGC mode

  @retval Always returns mrcSuccess.
**/
void
MrcSetNormalMode (
  IN MrcParameters *const MrcData,
  IN BOOLEAN              Mode
  );

/**
  This function enables Normal Mode and configures the Power Down Modes
  for the boot flows other than Cold Boot.

  @param[in] MrcData - The MRC general data.

  @retval Always returns mrcSuccess.
**/
extern
MrcStatus
MrcNormalMode (
  IN MrcParameters *const MrcData
  );

/**
  Clear Delta DQS before switching SA GV point

  @param[in] MrcData - include all the MRC general data.
void
MrcClearDeltaDqs (
  IN     MrcParameters *const MrcData
  );
**/
/**
  Early MRC overrides of input parameters.
  This is called at the very beginning of MRC, so DDR type is not known yet.

  @param[in]  MrcData - Pointer to global MRC data.

  @retval - mrcSuccess
**/
MrcStatus
MrcEarlyOverrides (
  IN  MrcParameters *const  MrcData
  );

/**
  This function will override MRC Inputs based on current safe configuration.
  Called if MrcSafeConfig is TRUE.
  These overrides don't depend on DDR type because we don't know it yet.
  Overrides that depend on DDR type will be done during SPD Processing in MrcMcCapabilityPreSpd() below in this file.

  @param[in]  MrcData - Pointer to global MRC data.

  @retval - mrcSuccess
**/
MrcStatus
MrcSafeMode (
  IN  MrcParameters *const  MrcData
  );

/**
  SA GV flow

  @param[in] MrcData - include all the MRC general data.

  @retval mrcStatus if succeeded.
**/
MrcStatus
MrcSaGvSwitch (
  IN     MrcParameters *const MrcData
  );

/**
  SA GV flow for the Fixed mode.

  @param[in] MrcData  - include all the MRC general data.
  @param[in] SaGvMode - The SAGV mode to be fixed to.

  @retval mrcStatus if succeeded.
**/
MrcStatus
MrcSetSaGvFixed (
  IN  MrcParameters *const MrcData,
  IN  MrcSaGv              SaGvMode
  );

/**
  SA GV flow: configure SAGV switch factor and heuristics.

  @param[in] MrcData  - include all the MRC general data.

  @retval mrcStatus if succeeded.
**/
MrcStatus
MrcSetSaGvThreshold (
  IN  MrcParameters *const MrcData
  );

/**
  Energy Performance Gain

  @param[in] MrcData - include all the MRC general data.

  @retval mrcStatus if succeeded.
**/
MrcStatus
MrcEnergyPerfGain (
  IN     MrcParameters *const MrcData
  );

/**
  This function is the last function that call from the MRC core.
  The function set DISB and set the MRC_Done.

  @param[in, out] MrcData - include all the MRC general data.

  @retval Always returns mrcSuccess.
**/
extern
MrcStatus
MrcDone (
  IN OUT MrcParameters *const MrcData
  );

/**
  Check for symmetric dimm/channel/mc population

  @param[in] *MrcData - Pointer to the MRC Debug structure.

  @retval Returns TRUE if symmetric
  @retval Returns FALSE if not symmetric
**/
BOOLEAN
MrcIsSymmetric (
  IN MrcParameters *MrcData
);

/**
  Enables IBECC if supported by CPU, and if dimm/channel/mc population is symmetric

  @param[in] *MrcData - Pointer to the MRC Debug structure.

  @retval Returns mrcSuccess
**/
MrcStatus
MrcIbecc (
  IN MrcParameters *MrcData
  );

/**
  This function set the MRC vertion to MCDECS_SPARE register.
  The function need to be call by the wrapper after MrcStartMemoryConfiguration function where the MC CLK enable.
  The function write:
  - Major number to bits 16-23
  - Minor number to bits 8-15
  - Build number to bits 0 - 7

  @param[in] MrcData - Include all MRC global data.

  @retval mrcSuccess - Return success.
**/
extern
MrcStatus
MrcSetMrcVersion (
  IN     MrcParameters *const MrcData
  );

/**
  This function locks the memory controller and memory map registers.

  @param[in] MrcData - Include all MRC global data.
**/
extern
void
McRegistersLock (
  IN     MrcParameters *const MrcData
  );

/**
  This function sets the DISB bit in General PM Configuration 2 B:D:F 0,31,0 offset 0xA2.
**/
extern
void
MrcSetDISB (
  IN MrcParameters * const MrcData
  );

/**
  This function reads the CAPID0 register and sets the memory controller's capability.

  @param[in, out] MrcData - All the MRC global data.

  @retval Returns mrcSuccess if the memory controller's capability has been determined, otherwise returns mrcFail.
**/
extern
MrcStatus
MrcMcCapability (
  IN OUT MrcParameters *const MrcData
  );

/**
  This function reads the CAPID0 register and sets the memory controller's capability.

  @param[in, out] MrcData - All the MRC global data.

  @retval Returns mrcSuccess if the memory controller's capability has been determined, otherwise returns mrcFail.
**/
MrcStatus
MrcMcCapabilityPreSpd (
  IN OUT MrcParameters *const MrcData
  );

/**
  This function reads the input data structure and sets the appropriate timing overrides in the output structure.

  @param[in, out] MrcData - All the MRC global data.

  @retval Returns mrcSuccess if the timing overrides have been conpleted.
**/
extern
MrcStatus
MrcSetOverrides (
  IN OUT MrcParameters *const MrcData
  );

/**
  This function sets DRAM to enable Write0 if it is supported by the device

  @param[in] MrcData - The global host structure

  @retval Returns True if feature is enabled, otherwise - False.
**/
BOOLEAN
MrcLpddr5Write0En (
  IN MrcParameters *const MrcData
  );

/**
  This function enables Write0 feature on LP5/DDR5

  @param[in] MrcData - The global host structure

  @retval mrcSuccess.
**/
MrcStatus
MrcWrite0 (
  IN MrcParameters *const MrcData
  );

/**
  This function fills in the MRS FSM to finalize the SAGV configuration for normal operation.

  @param[in] MrcData - The global host structure

  @retval mrcSuccess.
**/
MrcStatus
MrcSaGvFinal (
  IN     MrcParameters *const MrcData
  );

/**
  Find the vendor-specific swizzling configuration

  @param[in]  MrcSpd       - Pointer to Spd data
  @param[in]  DimmOut      - Pointer to DimmOut
  @param[in]  DimmMap      - Dimm Bit Map of the dimm
  @param[in][out] RhTrrCtl - Pointer to RH_TRR_CONTROL
**/
VOID
MrcGetVendorSwizzling (
  IN MrcSpd                                *Spd,
  IN MrcDimmOut                            *DimmOut,
  IN UINT8                                  DimmMap,
  IN OUT MC0_CH0_CR_RH_TRR_CONTROL_STRUCT  *RhTrrCtl
  );

/**
  Configure Row Hammer pTRR on the given controller / channel

  @param[in]  MrcData      - Pointer to global MRC data.
  @param[in]  Controller   - MC index
  @param[in]  Channel      - Channel index
  @param[in]  DimmMap      - Present dimms of the Channel

  @retval - TRUE if pTRR is configured
**/
BOOLEAN
MrcRhConfigPTRR (
  IN MrcParameters *const MrcData,
  IN UINT32            Controller,
  IN UINT32            Channel,
  IN UINT8             DimmMap
  );

/**
  Check if RFM is required by DRAM. If it requires, save 4 parameters into vars

   @param[in]  MrcData        - Pointer to global MRC data.
   @param[in]  Controller     - MC number
   @param[in]  Channel        - Channel number
   @param[in]  Dimm           - Dimm number
   @param[out] RAAIMT         - RFM RAAIMT
   @param[out] RAAMMT         - RFM RAAMMT
   @param[out] REF_SUB        - RFM REF_SUB
   @param[out] NORMAL_REF_SUB - RFM NORMAL_REF_SUB

   @retval - TRUE means RFM is required and RAAIMT and other outputs save the parameter values
**/
BOOLEAN
MrcRhCheckRFMRequired (
  IN MrcParameters *const MrcData,
  IN UINT32            Controller,
  IN UINT32            Channel,
  IN UINT32            Dimm,
  OUT UINT32           *RAAIMT,
  OUT UINT32           *RAAMMT,
  OUT UINT32           *REF_SUB,
  OUT UINT32           *NORMAL_REF_SUB
  );

/**
  Program Row Hammer mitigation if enabled

  @param[in] MrcData - The MRC general data.
**/
MrcStatus
MrcRhPrevention (
  IN MrcParameters *const MrcData
  );

/**
  Enable/Disable DLL WeakLock if needed.
  Note: We don't enable it in McConfig because CKE is still low during that step.

  @param[in] MrcData - The MRC general data.
  @param[in]  Enable - BOOLEAN control to enable (if TRUE), or disable (if FALSE) WeakLock.

  @retval None
**/
void
MrcWeaklockEnDis (
  IN MrcParameters *const MrcData,
  IN BOOLEAN              Enable
  );

#ifdef MRC_DEBUG_PRINT
/**
  Print the input parameters to the debug message output port.

  @param[in] MrcData - The MRC global data.

  @retval mrcSuccess
**/
extern
MrcStatus
MrcPrintInputParameters (
  MrcParameters * const MrcData
  );

/**
  Print the specified memory to the serial message debug port.

  @param[in] Debug - Serial message debug structure.
  @param[in] Start - The starting address to dump.
  @param[in] Size  - The amount of data in bytes to dump.
**/
extern
void
MrcPrintMemory (
  IN MrcDebug *const    Debug,
  IN const UINT8 *const Start,
  IN const UINT32       Size
  );
#endif


/**
  Sets CpuModel and CpuStepping in MrcData based on CpuModelStep.

  @param[out] MrcData     - The Mrc Host data structure
  @param[in]  CpuModel    - The CPU Family Model.
  @param[in]  CpuStepping - The CPU Stepping.

  @retval mrcSuccess if the model and stepping is found.  Otherwise mrcFail
**/
MrcStatus
MrcSetCpuInformation (
  OUT MrcParameters  *MrcData,
  IN  MrcCpuModel    CpuModel,
  IN  MrcCpuStepping CpuStepping
  );

#endif
