#ifndef __MrcMcRegisterStructAdl_h__
#define __MrcMcRegisterStructAdl_h__
/** @file
  This file was automatically generated. Modify at your own risk.
  Note that no error checking is done in these functions so ensure that the correct values are passed.

@copyright
  INTEL CONFIDENTIAL
  Copyright 2019 - 2020 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains an 'Intel Peripheral Driver' and is uniquely identified as
  "Intel Reference Module" and is licensed for Intel CPUs and chipsets under
  the terms of your license agreement with Intel or your vendor. This file may
  be modified by the user, subject to additional terms of the license agreement.

@par Specification Reference:
**/

#pragma pack(push, 1)
#include "MrcMcRegisterStructAdl0xxx.h"
#include "MrcMcRegisterStructAdl1xxx.h"
#include "MrcMcRegisterStructAdl2xxx.h"
#include "MrcMcRegisterStructAdl3xxx.h"
#include "MrcMcRegisterStructAdl5xxx.h"
#include "MrcMcRegisterStructAdl6xxx.h"
#include "MrcMcRegisterStructAdl7xxx.h"
#include "MrcMcRegisterStructAdl8xxx.h"
#include "MrcMcRegisterStructAdl9xxx.h"
#include "MrcMcRegisterStructAdlCxxx.h"
#include "MrcMcRegisterStructAdlDxxx.h"
#include "MrcMcRegisterStructAdlExxx.h"
#include "MrcMcRegisterStructAdlFxxx.h"
#include "MrcMcRegisterStructAdl11xxx.h"
#include "MrcMcRegisterStructAdl12xxx.h"
#include "MrcMcRegisterStructAdl1Cxxx.h"
#include "MrcMcRegisterStructAdl1Dxxx.h"
#include "MrcMcRegisterStructAdl1Exxx.h"
#include "MrcMcRegisterStructAdl1Fxxx.h"
#pragma pack(pop)
#endif
