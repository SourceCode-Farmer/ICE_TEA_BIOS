#ifndef __MrcMcRegisterAdlExxx_h__
#define __MrcMcRegisterAdlExxx_h__
/** @file
  This file was automatically generated. Modify at your own risk.
  Note that no error checking is done in these functions so ensure that the correct values are passed.

@copyright
  Copyright (c) 2010 - 2021 Intel Corporation. All rights reserved
  This software and associated documentation (if any) is furnished
  under a license and may only be used or copied in accordance
  with the terms of the license. Except as permitted by the
  license, no part of this software or documentation may be
  reproduced, stored in a retrieval system, or transmitted in any
  form or by any means without the express written consent of
  Intel Corporation.
  This file contains an 'Intel Peripheral Driver' and is uniquely
  identified as "Intel Reference Module" and is licensed for Intel
  CPUs and chipsets under the terms of your license agreement with
  Intel or your vendor. This file may be modified by the user, subject
  to additional terms of the license agreement.

@par Specification Reference:
**/

#pragma pack(push, 1)


#define MC0_CH0_CR_TC_PRE_REG                                          (0x0000E000)

  #define MC0_CH0_CR_TC_PRE_tRP_OFF                                    ( 0)
  #define MC0_CH0_CR_TC_PRE_tRP_WID                                    ( 8)
  #define MC0_CH0_CR_TC_PRE_tRP_MSK                                    (0x000000FF)
  #define MC0_CH0_CR_TC_PRE_tRP_MIN                                    (0)
  #define MC0_CH0_CR_TC_PRE_tRP_MAX                                    (255) // 0x000000FF
  #define MC0_CH0_CR_TC_PRE_tRP_DEF                                    (0x00000008)
  #define MC0_CH0_CR_TC_PRE_tRP_HSH                                    (0x4800E000)

  #define MC0_CH0_CR_TC_PRE_tRPab_ext_OFF                              ( 8)
  #define MC0_CH0_CR_TC_PRE_tRPab_ext_WID                              ( 5)
  #define MC0_CH0_CR_TC_PRE_tRPab_ext_MSK                              (0x00001F00)
  #define MC0_CH0_CR_TC_PRE_tRPab_ext_MIN                              (0)
  #define MC0_CH0_CR_TC_PRE_tRPab_ext_MAX                              (31) // 0x0000001F
  #define MC0_CH0_CR_TC_PRE_tRPab_ext_DEF                              (0x00000000)
  #define MC0_CH0_CR_TC_PRE_tRPab_ext_HSH                              (0x4510E000)

  #define MC0_CH0_CR_TC_PRE_tRDPRE_OFF                                 (13)
  #define MC0_CH0_CR_TC_PRE_tRDPRE_WID                                 ( 7)
  #define MC0_CH0_CR_TC_PRE_tRDPRE_MSK                                 (0x000FE000)
  #define MC0_CH0_CR_TC_PRE_tRDPRE_MIN                                 (0)
  #define MC0_CH0_CR_TC_PRE_tRDPRE_MAX                                 (127) // 0x0000007F
  #define MC0_CH0_CR_TC_PRE_tRDPRE_DEF                                 (0x00000006)
  #define MC0_CH0_CR_TC_PRE_tRDPRE_HSH                                 (0x471AE000)

  #define MC0_CH0_CR_TC_PRE_tPPD_OFF                                   (20)
  #define MC0_CH0_CR_TC_PRE_tPPD_WID                                   ( 4)
  #define MC0_CH0_CR_TC_PRE_tPPD_MSK                                   (0x00F00000)
  #define MC0_CH0_CR_TC_PRE_tPPD_MIN                                   (0)
  #define MC0_CH0_CR_TC_PRE_tPPD_MAX                                   (15) // 0x0000000F
  #define MC0_CH0_CR_TC_PRE_tPPD_DEF                                   (0x00000004)
  #define MC0_CH0_CR_TC_PRE_tPPD_HSH                                   (0x4428E000)
  #define MC0_CH0_CR_TC_PRE_tWRPRE_OFF                                 (32)
  #define MC0_CH0_CR_TC_PRE_tWRPRE_WID                                 (10)
  #define MC0_CH0_CR_TC_PRE_tWRPRE_MSK                                 (0x000003FF00000000ULL)
  #define MC0_CH0_CR_TC_PRE_tWRPRE_MIN                                 (0)
  #define MC0_CH0_CR_TC_PRE_tWRPRE_MAX                                 (1023) // 0x000003FF
  #define MC0_CH0_CR_TC_PRE_tWRPRE_DEF                                 (0x00000018)
  #define MC0_CH0_CR_TC_PRE_tWRPRE_HSH                                 (0x4A40E000)

  #define MC0_CH0_CR_TC_PRE_tRAS_OFF                                   (42)
  #define MC0_CH0_CR_TC_PRE_tRAS_WID                                   ( 9)
  #define MC0_CH0_CR_TC_PRE_tRAS_MSK                                   (0x0007FC0000000000ULL)
  #define MC0_CH0_CR_TC_PRE_tRAS_MIN                                   (0)
  #define MC0_CH0_CR_TC_PRE_tRAS_MAX                                   (511) // 0x000001FF
  #define MC0_CH0_CR_TC_PRE_tRAS_DEF                                   (0x0000001C)
  #define MC0_CH0_CR_TC_PRE_tRAS_HSH                                   (0x4954E000)

  #define MC0_CH0_CR_TC_PRE_tRCD_OFF                                   (51)
  #define MC0_CH0_CR_TC_PRE_tRCD_WID                                   ( 8)
  #define MC0_CH0_CR_TC_PRE_tRCD_MSK                                   (0x07F8000000000000ULL)
  #define MC0_CH0_CR_TC_PRE_tRCD_MIN                                   (0)
  #define MC0_CH0_CR_TC_PRE_tRCD_MAX                                   (255) // 0x000000FF
  #define MC0_CH0_CR_TC_PRE_tRCD_DEF                                   (0x00000008)
  #define MC0_CH0_CR_TC_PRE_tRCD_HSH                                   (0x4866E000)

  #define MC0_CH0_CR_TC_PRE_derating_ext_OFF                           (59)
  #define MC0_CH0_CR_TC_PRE_derating_ext_WID                           ( 4)
  #define MC0_CH0_CR_TC_PRE_derating_ext_MSK                           (0x7800000000000000ULL)
  #define MC0_CH0_CR_TC_PRE_derating_ext_MIN                           (0)
  #define MC0_CH0_CR_TC_PRE_derating_ext_MAX                           (15) // 0x0000000F
  #define MC0_CH0_CR_TC_PRE_derating_ext_DEF                           (0x00000002)
  #define MC0_CH0_CR_TC_PRE_derating_ext_HSH                           (0x4476E000)

#define MC0_CH0_CR_TC_ACT_REG                                          (0x0000E008)

  #define MC0_CH0_CR_TC_ACT_tFAW_OFF                                   ( 0)
  #define MC0_CH0_CR_TC_ACT_tFAW_WID                                   ( 9)
  #define MC0_CH0_CR_TC_ACT_tFAW_MSK                                   (0x000001FF)
  #define MC0_CH0_CR_TC_ACT_tFAW_MIN                                   (0)
  #define MC0_CH0_CR_TC_ACT_tFAW_MAX                                   (511) // 0x000001FF
  #define MC0_CH0_CR_TC_ACT_tFAW_DEF                                   (0x00000010)
  #define MC0_CH0_CR_TC_ACT_tFAW_HSH                                   (0x0900E008)

  #define MC0_CH0_CR_TC_ACT_tRRD_sg_OFF                                ( 9)
  #define MC0_CH0_CR_TC_ACT_tRRD_sg_WID                                ( 6)
  #define MC0_CH0_CR_TC_ACT_tRRD_sg_MSK                                (0x00007E00)
  #define MC0_CH0_CR_TC_ACT_tRRD_sg_MIN                                (0)
  #define MC0_CH0_CR_TC_ACT_tRRD_sg_MAX                                (63) // 0x0000003F
  #define MC0_CH0_CR_TC_ACT_tRRD_sg_DEF                                (0x00000004)
  #define MC0_CH0_CR_TC_ACT_tRRD_sg_HSH                                (0x0612E008)

  #define MC0_CH0_CR_TC_ACT_tRRD_dg_OFF                                (15)
  #define MC0_CH0_CR_TC_ACT_tRRD_dg_WID                                ( 7)
  #define MC0_CH0_CR_TC_ACT_tRRD_dg_MSK                                (0x003F8000)
  #define MC0_CH0_CR_TC_ACT_tRRD_dg_MIN                                (0)
  #define MC0_CH0_CR_TC_ACT_tRRD_dg_MAX                                (127) // 0x0000007F
  #define MC0_CH0_CR_TC_ACT_tRRD_dg_DEF                                (0x00000004)
  #define MC0_CH0_CR_TC_ACT_tRRD_dg_HSH                                (0x071EE008)

  #define MC0_CH0_CR_TC_ACT_trefsbrd_A0_OFF                            (25)
  #define MC0_CH0_CR_TC_ACT_trefsbrd_A0_WID                            ( 7)
  #define MC0_CH0_CR_TC_ACT_trefsbrd_A0_MSK                            (0xFE000000)
  #define MC0_CH0_CR_TC_ACT_trefsbrd_A0_MIN                            (0)
  #define MC0_CH0_CR_TC_ACT_trefsbrd_A0_MAX                            (127) // 0x0000007F
  #define MC0_CH0_CR_TC_ACT_trefsbrd_A0_DEF                            (0x00000018)
  #define MC0_CH0_CR_TC_ACT_trefsbrd_A0_HSH                            (0x0732E008)

  #define MC0_CH0_CR_TC_ACT_trefsbrd_OFF                               (24)
  #define MC0_CH0_CR_TC_ACT_trefsbrd_WID                               ( 8)
  #define MC0_CH0_CR_TC_ACT_trefsbrd_MSK                               (0xFF000000)
  #define MC0_CH0_CR_TC_ACT_trefsbrd_MIN                               (0)
  #define MC0_CH0_CR_TC_ACT_trefsbrd_MAX                               (255) // 0x000000FF
  #define MC0_CH0_CR_TC_ACT_trefsbrd_DEF                               (0x00000018)
  #define MC0_CH0_CR_TC_ACT_trefsbrd_HSH                               (0x0830E008)

#define MC0_CH0_CR_TC_RDRD_REG                                         (0x0000E00C)

  #define MC0_CH0_CR_TC_RDRD_tRDRD_sg_OFF                              ( 0)
  #define MC0_CH0_CR_TC_RDRD_tRDRD_sg_WID                              ( 7)
  #define MC0_CH0_CR_TC_RDRD_tRDRD_sg_MSK                              (0x0000007F)
  #define MC0_CH0_CR_TC_RDRD_tRDRD_sg_MIN                              (0)
  #define MC0_CH0_CR_TC_RDRD_tRDRD_sg_MAX                              (127) // 0x0000007F
  #define MC0_CH0_CR_TC_RDRD_tRDRD_sg_DEF                              (0x00000004)
  #define MC0_CH0_CR_TC_RDRD_tRDRD_sg_HSH                              (0x0700E00C)

  #define MC0_CH0_CR_TC_RDRD_Allow_2cyc_B2B_LPDDR_OFF                  ( 7)
  #define MC0_CH0_CR_TC_RDRD_Allow_2cyc_B2B_LPDDR_WID                  ( 1)
  #define MC0_CH0_CR_TC_RDRD_Allow_2cyc_B2B_LPDDR_MSK                  (0x00000080)
  #define MC0_CH0_CR_TC_RDRD_Allow_2cyc_B2B_LPDDR_MIN                  (0)
  #define MC0_CH0_CR_TC_RDRD_Allow_2cyc_B2B_LPDDR_MAX                  (1) // 0x00000001
  #define MC0_CH0_CR_TC_RDRD_Allow_2cyc_B2B_LPDDR_DEF                  (0x00000001)
  #define MC0_CH0_CR_TC_RDRD_Allow_2cyc_B2B_LPDDR_HSH                  (0x010EE00C)

  #define MC0_CH0_CR_TC_RDRD_tRDRD_dg_OFF                              ( 8)
  #define MC0_CH0_CR_TC_RDRD_tRDRD_dg_WID                              ( 7)
  #define MC0_CH0_CR_TC_RDRD_tRDRD_dg_MSK                              (0x00007F00)
  #define MC0_CH0_CR_TC_RDRD_tRDRD_dg_MIN                              (0)
  #define MC0_CH0_CR_TC_RDRD_tRDRD_dg_MAX                              (127) // 0x0000007F
  #define MC0_CH0_CR_TC_RDRD_tRDRD_dg_DEF                              (0x00000004)
  #define MC0_CH0_CR_TC_RDRD_tRDRD_dg_HSH                              (0x0710E00C)

  #define MC0_CH0_CR_TC_RDRD_tRDRD_dr_OFF                              (16)
  #define MC0_CH0_CR_TC_RDRD_tRDRD_dr_WID                              ( 8)
  #define MC0_CH0_CR_TC_RDRD_tRDRD_dr_MSK                              (0x00FF0000)
  #define MC0_CH0_CR_TC_RDRD_tRDRD_dr_MIN                              (0)
  #define MC0_CH0_CR_TC_RDRD_tRDRD_dr_MAX                              (255) // 0x000000FF
  #define MC0_CH0_CR_TC_RDRD_tRDRD_dr_DEF                              (0x00000004)
  #define MC0_CH0_CR_TC_RDRD_tRDRD_dr_HSH                              (0x0820E00C)

  #define MC0_CH0_CR_TC_RDRD_tRDRD_dd_OFF                              (24)
  #define MC0_CH0_CR_TC_RDRD_tRDRD_dd_WID                              ( 8)
  #define MC0_CH0_CR_TC_RDRD_tRDRD_dd_MSK                              (0xFF000000)
  #define MC0_CH0_CR_TC_RDRD_tRDRD_dd_MIN                              (0)
  #define MC0_CH0_CR_TC_RDRD_tRDRD_dd_MAX                              (255) // 0x000000FF
  #define MC0_CH0_CR_TC_RDRD_tRDRD_dd_DEF                              (0x00000004)
  #define MC0_CH0_CR_TC_RDRD_tRDRD_dd_HSH                              (0x0830E00C)

#define MC0_CH0_CR_TC_RDWR_REG                                         (0x0000E010)

  #define MC0_CH0_CR_TC_RDWR_tRDWR_sg_OFF                              ( 0)
  #define MC0_CH0_CR_TC_RDWR_tRDWR_sg_WID                              ( 8)
  #define MC0_CH0_CR_TC_RDWR_tRDWR_sg_MSK                              (0x000000FF)
  #define MC0_CH0_CR_TC_RDWR_tRDWR_sg_MIN                              (0)
  #define MC0_CH0_CR_TC_RDWR_tRDWR_sg_MAX                              (255) // 0x000000FF
  #define MC0_CH0_CR_TC_RDWR_tRDWR_sg_DEF                              (0x00000004)
  #define MC0_CH0_CR_TC_RDWR_tRDWR_sg_HSH                              (0x0800E010)

  #define MC0_CH0_CR_TC_RDWR_tRDWR_dg_OFF                              ( 8)
  #define MC0_CH0_CR_TC_RDWR_tRDWR_dg_WID                              ( 8)
  #define MC0_CH0_CR_TC_RDWR_tRDWR_dg_MSK                              (0x0000FF00)
  #define MC0_CH0_CR_TC_RDWR_tRDWR_dg_MIN                              (0)
  #define MC0_CH0_CR_TC_RDWR_tRDWR_dg_MAX                              (255) // 0x000000FF
  #define MC0_CH0_CR_TC_RDWR_tRDWR_dg_DEF                              (0x00000004)
  #define MC0_CH0_CR_TC_RDWR_tRDWR_dg_HSH                              (0x0810E010)

  #define MC0_CH0_CR_TC_RDWR_tRDWR_dr_OFF                              (16)
  #define MC0_CH0_CR_TC_RDWR_tRDWR_dr_WID                              ( 8)
  #define MC0_CH0_CR_TC_RDWR_tRDWR_dr_MSK                              (0x00FF0000)
  #define MC0_CH0_CR_TC_RDWR_tRDWR_dr_MIN                              (0)
  #define MC0_CH0_CR_TC_RDWR_tRDWR_dr_MAX                              (255) // 0x000000FF
  #define MC0_CH0_CR_TC_RDWR_tRDWR_dr_DEF                              (0x00000004)
  #define MC0_CH0_CR_TC_RDWR_tRDWR_dr_HSH                              (0x0820E010)

  #define MC0_CH0_CR_TC_RDWR_tRDWR_dd_OFF                              (24)
  #define MC0_CH0_CR_TC_RDWR_tRDWR_dd_WID                              ( 8)
  #define MC0_CH0_CR_TC_RDWR_tRDWR_dd_MSK                              (0xFF000000)
  #define MC0_CH0_CR_TC_RDWR_tRDWR_dd_MIN                              (0)
  #define MC0_CH0_CR_TC_RDWR_tRDWR_dd_MAX                              (255) // 0x000000FF
  #define MC0_CH0_CR_TC_RDWR_tRDWR_dd_DEF                              (0x00000004)
  #define MC0_CH0_CR_TC_RDWR_tRDWR_dd_HSH                              (0x0830E010)

#define MC0_CH0_CR_TC_WRRD_REG                                         (0x0000E014)

  #define MC0_CH0_CR_TC_WRRD_tWRRD_sg_OFF                              ( 0)
  #define MC0_CH0_CR_TC_WRRD_tWRRD_sg_WID                              ( 9)
  #define MC0_CH0_CR_TC_WRRD_tWRRD_sg_MSK                              (0x000001FF)
  #define MC0_CH0_CR_TC_WRRD_tWRRD_sg_MIN                              (0)
  #define MC0_CH0_CR_TC_WRRD_tWRRD_sg_MAX                              (511) // 0x000001FF
  #define MC0_CH0_CR_TC_WRRD_tWRRD_sg_DEF                              (0x00000004)
  #define MC0_CH0_CR_TC_WRRD_tWRRD_sg_HSH                              (0x0900E014)

  #define MC0_CH0_CR_TC_WRRD_tWRRD_dg_OFF                              ( 9)
  #define MC0_CH0_CR_TC_WRRD_tWRRD_dg_WID                              ( 9)
  #define MC0_CH0_CR_TC_WRRD_tWRRD_dg_MSK                              (0x0003FE00)
  #define MC0_CH0_CR_TC_WRRD_tWRRD_dg_MIN                              (0)
  #define MC0_CH0_CR_TC_WRRD_tWRRD_dg_MAX                              (511) // 0x000001FF
  #define MC0_CH0_CR_TC_WRRD_tWRRD_dg_DEF                              (0x00000004)
  #define MC0_CH0_CR_TC_WRRD_tWRRD_dg_HSH                              (0x0912E014)

  #define MC0_CH0_CR_TC_WRRD_tWRRD_dr_OFF                              (18)
  #define MC0_CH0_CR_TC_WRRD_tWRRD_dr_WID                              ( 7)
  #define MC0_CH0_CR_TC_WRRD_tWRRD_dr_MSK                              (0x01FC0000)
  #define MC0_CH0_CR_TC_WRRD_tWRRD_dr_MIN                              (0)
  #define MC0_CH0_CR_TC_WRRD_tWRRD_dr_MAX                              (127) // 0x0000007F
  #define MC0_CH0_CR_TC_WRRD_tWRRD_dr_DEF                              (0x00000004)
  #define MC0_CH0_CR_TC_WRRD_tWRRD_dr_HSH                              (0x0724E014)

  #define MC0_CH0_CR_TC_WRRD_tWRRD_dd_OFF                              (25)
  #define MC0_CH0_CR_TC_WRRD_tWRRD_dd_WID                              ( 7)
  #define MC0_CH0_CR_TC_WRRD_tWRRD_dd_MSK                              (0xFE000000)
  #define MC0_CH0_CR_TC_WRRD_tWRRD_dd_MIN                              (0)
  #define MC0_CH0_CR_TC_WRRD_tWRRD_dd_MAX                              (127) // 0x0000007F
  #define MC0_CH0_CR_TC_WRRD_tWRRD_dd_DEF                              (0x00000004)
  #define MC0_CH0_CR_TC_WRRD_tWRRD_dd_HSH                              (0x0732E014)

#define MC0_CH0_CR_TC_WRWR_REG                                         (0x0000E018)

  #define MC0_CH0_CR_TC_WRWR_tWRWR_sg_OFF                              ( 0)
  #define MC0_CH0_CR_TC_WRWR_tWRWR_sg_WID                              ( 7)
  #define MC0_CH0_CR_TC_WRWR_tWRWR_sg_MSK                              (0x0000007F)
  #define MC0_CH0_CR_TC_WRWR_tWRWR_sg_MIN                              (0)
  #define MC0_CH0_CR_TC_WRWR_tWRWR_sg_MAX                              (127) // 0x0000007F
  #define MC0_CH0_CR_TC_WRWR_tWRWR_sg_DEF                              (0x00000004)
  #define MC0_CH0_CR_TC_WRWR_tWRWR_sg_HSH                              (0x0700E018)

  #define MC0_CH0_CR_TC_WRWR_tWRWR_dg_OFF                              ( 8)
  #define MC0_CH0_CR_TC_WRWR_tWRWR_dg_WID                              ( 7)
  #define MC0_CH0_CR_TC_WRWR_tWRWR_dg_MSK                              (0x00007F00)
  #define MC0_CH0_CR_TC_WRWR_tWRWR_dg_MIN                              (0)
  #define MC0_CH0_CR_TC_WRWR_tWRWR_dg_MAX                              (127) // 0x0000007F
  #define MC0_CH0_CR_TC_WRWR_tWRWR_dg_DEF                              (0x00000004)
  #define MC0_CH0_CR_TC_WRWR_tWRWR_dg_HSH                              (0x0710E018)

  #define MC0_CH0_CR_TC_WRWR_tWRWR_dr_OFF                              (16)
  #define MC0_CH0_CR_TC_WRWR_tWRWR_dr_WID                              ( 7)
  #define MC0_CH0_CR_TC_WRWR_tWRWR_dr_MSK                              (0x007F0000)
  #define MC0_CH0_CR_TC_WRWR_tWRWR_dr_MIN                              (0)
  #define MC0_CH0_CR_TC_WRWR_tWRWR_dr_MAX                              (127) // 0x0000007F
  #define MC0_CH0_CR_TC_WRWR_tWRWR_dr_DEF                              (0x00000004)
  #define MC0_CH0_CR_TC_WRWR_tWRWR_dr_HSH                              (0x0720E018)

  #define MC0_CH0_CR_TC_WRWR_tWRWR_dd_OFF                              (24)
  #define MC0_CH0_CR_TC_WRWR_tWRWR_dd_WID                              ( 8)
  #define MC0_CH0_CR_TC_WRWR_tWRWR_dd_MSK                              (0xFF000000)
  #define MC0_CH0_CR_TC_WRWR_tWRWR_dd_MIN                              (0)
  #define MC0_CH0_CR_TC_WRWR_tWRWR_dd_MAX                              (255) // 0x000000FF
  #define MC0_CH0_CR_TC_WRWR_tWRWR_dd_DEF                              (0x00000004)
  #define MC0_CH0_CR_TC_WRWR_tWRWR_dd_HSH                              (0x0830E018)

#define MC0_CH0_CR_PM_ADAPTIVE_CKE_REG                                 (0x0000E01C)

  #define MC0_CH0_CR_PM_ADAPTIVE_CKE_MIN_PDWN_IDLE_COUNTER_OFF         ( 0)
  #define MC0_CH0_CR_PM_ADAPTIVE_CKE_MIN_PDWN_IDLE_COUNTER_WID         (12)
  #define MC0_CH0_CR_PM_ADAPTIVE_CKE_MIN_PDWN_IDLE_COUNTER_MSK         (0x00000FFF)
  #define MC0_CH0_CR_PM_ADAPTIVE_CKE_MIN_PDWN_IDLE_COUNTER_MIN         (0)
  #define MC0_CH0_CR_PM_ADAPTIVE_CKE_MIN_PDWN_IDLE_COUNTER_MAX         (4095) // 0x00000FFF
  #define MC0_CH0_CR_PM_ADAPTIVE_CKE_MIN_PDWN_IDLE_COUNTER_DEF         (0x00000010)
  #define MC0_CH0_CR_PM_ADAPTIVE_CKE_MIN_PDWN_IDLE_COUNTER_HSH         (0x0C00E01C)

  #define MC0_CH0_CR_PM_ADAPTIVE_CKE_MAX_PDWN_IDLE_COUNTER_OFF         (12)
  #define MC0_CH0_CR_PM_ADAPTIVE_CKE_MAX_PDWN_IDLE_COUNTER_WID         (12)
  #define MC0_CH0_CR_PM_ADAPTIVE_CKE_MAX_PDWN_IDLE_COUNTER_MSK         (0x00FFF000)
  #define MC0_CH0_CR_PM_ADAPTIVE_CKE_MAX_PDWN_IDLE_COUNTER_MIN         (0)
  #define MC0_CH0_CR_PM_ADAPTIVE_CKE_MAX_PDWN_IDLE_COUNTER_MAX         (4095) // 0x00000FFF
  #define MC0_CH0_CR_PM_ADAPTIVE_CKE_MAX_PDWN_IDLE_COUNTER_DEF         (0x00000100)
  #define MC0_CH0_CR_PM_ADAPTIVE_CKE_MAX_PDWN_IDLE_COUNTER_HSH         (0x0C18E01C)

#define MC0_CH0_CR_SC_ROUNDTRIP_LATENCY_REG                            (0x0000E020)

  #define MC0_CH0_CR_SC_ROUNDTRIP_LATENCY_Rank_0_latency_OFF           ( 0)
  #define MC0_CH0_CR_SC_ROUNDTRIP_LATENCY_Rank_0_latency_WID           ( 8)
  #define MC0_CH0_CR_SC_ROUNDTRIP_LATENCY_Rank_0_latency_MSK           (0x000000FF)
  #define MC0_CH0_CR_SC_ROUNDTRIP_LATENCY_Rank_0_latency_MIN           (0)
  #define MC0_CH0_CR_SC_ROUNDTRIP_LATENCY_Rank_0_latency_MAX           (255) // 0x000000FF
  #define MC0_CH0_CR_SC_ROUNDTRIP_LATENCY_Rank_0_latency_DEF           (0x00000019)
  #define MC0_CH0_CR_SC_ROUNDTRIP_LATENCY_Rank_0_latency_HSH           (0x4800E020)

  #define MC0_CH0_CR_SC_ROUNDTRIP_LATENCY_Rank_1_latency_OFF           ( 8)
  #define MC0_CH0_CR_SC_ROUNDTRIP_LATENCY_Rank_1_latency_WID           ( 8)
  #define MC0_CH0_CR_SC_ROUNDTRIP_LATENCY_Rank_1_latency_MSK           (0x0000FF00)
  #define MC0_CH0_CR_SC_ROUNDTRIP_LATENCY_Rank_1_latency_MIN           (0)
  #define MC0_CH0_CR_SC_ROUNDTRIP_LATENCY_Rank_1_latency_MAX           (255) // 0x000000FF
  #define MC0_CH0_CR_SC_ROUNDTRIP_LATENCY_Rank_1_latency_DEF           (0x00000019)
  #define MC0_CH0_CR_SC_ROUNDTRIP_LATENCY_Rank_1_latency_HSH           (0x4810E020)

  #define MC0_CH0_CR_SC_ROUNDTRIP_LATENCY_Rank_2_latency_OFF           (16)
  #define MC0_CH0_CR_SC_ROUNDTRIP_LATENCY_Rank_2_latency_WID           ( 8)
  #define MC0_CH0_CR_SC_ROUNDTRIP_LATENCY_Rank_2_latency_MSK           (0x00FF0000)
  #define MC0_CH0_CR_SC_ROUNDTRIP_LATENCY_Rank_2_latency_MIN           (0)
  #define MC0_CH0_CR_SC_ROUNDTRIP_LATENCY_Rank_2_latency_MAX           (255) // 0x000000FF
  #define MC0_CH0_CR_SC_ROUNDTRIP_LATENCY_Rank_2_latency_DEF           (0x00000019)
  #define MC0_CH0_CR_SC_ROUNDTRIP_LATENCY_Rank_2_latency_HSH           (0x4820E020)

  #define MC0_CH0_CR_SC_ROUNDTRIP_LATENCY_Rank_3_latency_OFF           (24)
  #define MC0_CH0_CR_SC_ROUNDTRIP_LATENCY_Rank_3_latency_WID           ( 8)
  #define MC0_CH0_CR_SC_ROUNDTRIP_LATENCY_Rank_3_latency_MSK           (0xFF000000)
  #define MC0_CH0_CR_SC_ROUNDTRIP_LATENCY_Rank_3_latency_MIN           (0)
  #define MC0_CH0_CR_SC_ROUNDTRIP_LATENCY_Rank_3_latency_MAX           (255) // 0x000000FF
  #define MC0_CH0_CR_SC_ROUNDTRIP_LATENCY_Rank_3_latency_DEF           (0x00000019)
  #define MC0_CH0_CR_SC_ROUNDTRIP_LATENCY_Rank_3_latency_HSH           (0x4830E020)

  #define MC0_CH0_CR_SC_ROUNDTRIP_LATENCY_Rank_4_latency_OFF           (32)
  #define MC0_CH0_CR_SC_ROUNDTRIP_LATENCY_Rank_4_latency_WID           ( 8)
  #define MC0_CH0_CR_SC_ROUNDTRIP_LATENCY_Rank_4_latency_MSK           (0x000000FF00000000ULL)
  #define MC0_CH0_CR_SC_ROUNDTRIP_LATENCY_Rank_4_latency_MIN           (0)
  #define MC0_CH0_CR_SC_ROUNDTRIP_LATENCY_Rank_4_latency_MAX           (255) // 0x000000FF
  #define MC0_CH0_CR_SC_ROUNDTRIP_LATENCY_Rank_4_latency_DEF           (0x00000019)
  #define MC0_CH0_CR_SC_ROUNDTRIP_LATENCY_Rank_4_latency_HSH           (0x4840E020)

  #define MC0_CH0_CR_SC_ROUNDTRIP_LATENCY_Rank_5_latency_OFF           (40)
  #define MC0_CH0_CR_SC_ROUNDTRIP_LATENCY_Rank_5_latency_WID           ( 8)
  #define MC0_CH0_CR_SC_ROUNDTRIP_LATENCY_Rank_5_latency_MSK           (0x0000FF0000000000ULL)
  #define MC0_CH0_CR_SC_ROUNDTRIP_LATENCY_Rank_5_latency_MIN           (0)
  #define MC0_CH0_CR_SC_ROUNDTRIP_LATENCY_Rank_5_latency_MAX           (255) // 0x000000FF
  #define MC0_CH0_CR_SC_ROUNDTRIP_LATENCY_Rank_5_latency_DEF           (0x00000019)
  #define MC0_CH0_CR_SC_ROUNDTRIP_LATENCY_Rank_5_latency_HSH           (0x4850E020)

  #define MC0_CH0_CR_SC_ROUNDTRIP_LATENCY_Rank_6_latency_OFF           (48)
  #define MC0_CH0_CR_SC_ROUNDTRIP_LATENCY_Rank_6_latency_WID           ( 8)
  #define MC0_CH0_CR_SC_ROUNDTRIP_LATENCY_Rank_6_latency_MSK           (0x00FF000000000000ULL)
  #define MC0_CH0_CR_SC_ROUNDTRIP_LATENCY_Rank_6_latency_MIN           (0)
  #define MC0_CH0_CR_SC_ROUNDTRIP_LATENCY_Rank_6_latency_MAX           (255) // 0x000000FF
  #define MC0_CH0_CR_SC_ROUNDTRIP_LATENCY_Rank_6_latency_DEF           (0x00000019)
  #define MC0_CH0_CR_SC_ROUNDTRIP_LATENCY_Rank_6_latency_HSH           (0x4860E020)

  #define MC0_CH0_CR_SC_ROUNDTRIP_LATENCY_Rank_7_latency_OFF           (56)
  #define MC0_CH0_CR_SC_ROUNDTRIP_LATENCY_Rank_7_latency_WID           ( 8)
  #define MC0_CH0_CR_SC_ROUNDTRIP_LATENCY_Rank_7_latency_MSK           (0xFF00000000000000ULL)
  #define MC0_CH0_CR_SC_ROUNDTRIP_LATENCY_Rank_7_latency_MIN           (0)
  #define MC0_CH0_CR_SC_ROUNDTRIP_LATENCY_Rank_7_latency_MAX           (255) // 0x000000FF
  #define MC0_CH0_CR_SC_ROUNDTRIP_LATENCY_Rank_7_latency_DEF           (0x00000019)
  #define MC0_CH0_CR_SC_ROUNDTRIP_LATENCY_Rank_7_latency_HSH           (0x4870E020)

#define MC0_CH0_CR_SCHED_CBIT_REG                                      (0x0000E028)

  #define MC0_CH0_CR_SCHED_CBIT_dis_opp_cas_OFF                        ( 0)
  #define MC0_CH0_CR_SCHED_CBIT_dis_opp_cas_WID                        ( 1)
  #define MC0_CH0_CR_SCHED_CBIT_dis_opp_cas_MSK                        (0x00000001)
  #define MC0_CH0_CR_SCHED_CBIT_dis_opp_cas_MIN                        (0)
  #define MC0_CH0_CR_SCHED_CBIT_dis_opp_cas_MAX                        (1) // 0x00000001
  #define MC0_CH0_CR_SCHED_CBIT_dis_opp_cas_DEF                        (0x00000000)
  #define MC0_CH0_CR_SCHED_CBIT_dis_opp_cas_HSH                        (0x0100E028)

  #define MC0_CH0_CR_SCHED_CBIT_dis_opp_is_cas_OFF                     ( 1)
  #define MC0_CH0_CR_SCHED_CBIT_dis_opp_is_cas_WID                     ( 1)
  #define MC0_CH0_CR_SCHED_CBIT_dis_opp_is_cas_MSK                     (0x00000002)
  #define MC0_CH0_CR_SCHED_CBIT_dis_opp_is_cas_MIN                     (0)
  #define MC0_CH0_CR_SCHED_CBIT_dis_opp_is_cas_MAX                     (1) // 0x00000001
  #define MC0_CH0_CR_SCHED_CBIT_dis_opp_is_cas_DEF                     (0x00000000)
  #define MC0_CH0_CR_SCHED_CBIT_dis_opp_is_cas_HSH                     (0x0102E028)

  #define MC0_CH0_CR_SCHED_CBIT_dis_opp_ras_OFF                        ( 2)
  #define MC0_CH0_CR_SCHED_CBIT_dis_opp_ras_WID                        ( 1)
  #define MC0_CH0_CR_SCHED_CBIT_dis_opp_ras_MSK                        (0x00000004)
  #define MC0_CH0_CR_SCHED_CBIT_dis_opp_ras_MIN                        (0)
  #define MC0_CH0_CR_SCHED_CBIT_dis_opp_ras_MAX                        (1) // 0x00000001
  #define MC0_CH0_CR_SCHED_CBIT_dis_opp_ras_DEF                        (0x00000000)
  #define MC0_CH0_CR_SCHED_CBIT_dis_opp_ras_HSH                        (0x0104E028)

  #define MC0_CH0_CR_SCHED_CBIT_dis_opp_is_ras_OFF                     ( 3)
  #define MC0_CH0_CR_SCHED_CBIT_dis_opp_is_ras_WID                     ( 1)
  #define MC0_CH0_CR_SCHED_CBIT_dis_opp_is_ras_MSK                     (0x00000008)
  #define MC0_CH0_CR_SCHED_CBIT_dis_opp_is_ras_MIN                     (0)
  #define MC0_CH0_CR_SCHED_CBIT_dis_opp_is_ras_MAX                     (1) // 0x00000001
  #define MC0_CH0_CR_SCHED_CBIT_dis_opp_is_ras_DEF                     (0x00000000)
  #define MC0_CH0_CR_SCHED_CBIT_dis_opp_is_ras_HSH                     (0x0106E028)

  #define MC0_CH0_CR_SCHED_CBIT_dis_2c_byp_OFF                         ( 4)
  #define MC0_CH0_CR_SCHED_CBIT_dis_2c_byp_WID                         ( 1)
  #define MC0_CH0_CR_SCHED_CBIT_dis_2c_byp_MSK                         (0x00000010)
  #define MC0_CH0_CR_SCHED_CBIT_dis_2c_byp_MIN                         (0)
  #define MC0_CH0_CR_SCHED_CBIT_dis_2c_byp_MAX                         (1) // 0x00000001
  #define MC0_CH0_CR_SCHED_CBIT_dis_2c_byp_DEF                         (0x00000000)
  #define MC0_CH0_CR_SCHED_CBIT_dis_2c_byp_HSH                         (0x0108E028)

  #define MC0_CH0_CR_SCHED_CBIT_dis_deprd_opt_OFF                      ( 5)
  #define MC0_CH0_CR_SCHED_CBIT_dis_deprd_opt_WID                      ( 1)
  #define MC0_CH0_CR_SCHED_CBIT_dis_deprd_opt_MSK                      (0x00000020)
  #define MC0_CH0_CR_SCHED_CBIT_dis_deprd_opt_MIN                      (0)
  #define MC0_CH0_CR_SCHED_CBIT_dis_deprd_opt_MAX                      (1) // 0x00000001
  #define MC0_CH0_CR_SCHED_CBIT_dis_deprd_opt_DEF                      (0x00000000)
  #define MC0_CH0_CR_SCHED_CBIT_dis_deprd_opt_HSH                      (0x010AE028)

  #define MC0_CH0_CR_SCHED_CBIT_dis_pt_it_OFF                          ( 6)
  #define MC0_CH0_CR_SCHED_CBIT_dis_pt_it_WID                          ( 1)
  #define MC0_CH0_CR_SCHED_CBIT_dis_pt_it_MSK                          (0x00000040)
  #define MC0_CH0_CR_SCHED_CBIT_dis_pt_it_MIN                          (0)
  #define MC0_CH0_CR_SCHED_CBIT_dis_pt_it_MAX                          (1) // 0x00000001
  #define MC0_CH0_CR_SCHED_CBIT_dis_pt_it_DEF                          (0x00000000)
  #define MC0_CH0_CR_SCHED_CBIT_dis_pt_it_HSH                          (0x010CE028)

  #define MC0_CH0_CR_SCHED_CBIT_dis_prcnt_ring_OFF                     ( 7)
  #define MC0_CH0_CR_SCHED_CBIT_dis_prcnt_ring_WID                     ( 1)
  #define MC0_CH0_CR_SCHED_CBIT_dis_prcnt_ring_MSK                     (0x00000080)
  #define MC0_CH0_CR_SCHED_CBIT_dis_prcnt_ring_MIN                     (0)
  #define MC0_CH0_CR_SCHED_CBIT_dis_prcnt_ring_MAX                     (1) // 0x00000001
  #define MC0_CH0_CR_SCHED_CBIT_dis_prcnt_ring_DEF                     (0x00000000)
  #define MC0_CH0_CR_SCHED_CBIT_dis_prcnt_ring_HSH                     (0x010EE028)

  #define MC0_CH0_CR_SCHED_CBIT_dis_prcnt_sa_OFF                       ( 8)
  #define MC0_CH0_CR_SCHED_CBIT_dis_prcnt_sa_WID                       ( 1)
  #define MC0_CH0_CR_SCHED_CBIT_dis_prcnt_sa_MSK                       (0x00000100)
  #define MC0_CH0_CR_SCHED_CBIT_dis_prcnt_sa_MIN                       (0)
  #define MC0_CH0_CR_SCHED_CBIT_dis_prcnt_sa_MAX                       (1) // 0x00000001
  #define MC0_CH0_CR_SCHED_CBIT_dis_prcnt_sa_DEF                       (0x00000000)
  #define MC0_CH0_CR_SCHED_CBIT_dis_prcnt_sa_HSH                       (0x0110E028)

  #define MC0_CH0_CR_SCHED_CBIT_dis_blkr_ph_OFF                        ( 9)
  #define MC0_CH0_CR_SCHED_CBIT_dis_blkr_ph_WID                        ( 1)
  #define MC0_CH0_CR_SCHED_CBIT_dis_blkr_ph_MSK                        (0x00000200)
  #define MC0_CH0_CR_SCHED_CBIT_dis_blkr_ph_MIN                        (0)
  #define MC0_CH0_CR_SCHED_CBIT_dis_blkr_ph_MAX                        (1) // 0x00000001
  #define MC0_CH0_CR_SCHED_CBIT_dis_blkr_ph_DEF                        (0x00000000)
  #define MC0_CH0_CR_SCHED_CBIT_dis_blkr_ph_HSH                        (0x0112E028)

  #define MC0_CH0_CR_SCHED_CBIT_dis_blkr_pe_OFF                        (10)
  #define MC0_CH0_CR_SCHED_CBIT_dis_blkr_pe_WID                        ( 1)
  #define MC0_CH0_CR_SCHED_CBIT_dis_blkr_pe_MSK                        (0x00000400)
  #define MC0_CH0_CR_SCHED_CBIT_dis_blkr_pe_MIN                        (0)
  #define MC0_CH0_CR_SCHED_CBIT_dis_blkr_pe_MAX                        (1) // 0x00000001
  #define MC0_CH0_CR_SCHED_CBIT_dis_blkr_pe_DEF                        (0x00000000)
  #define MC0_CH0_CR_SCHED_CBIT_dis_blkr_pe_HSH                        (0x0114E028)

  #define MC0_CH0_CR_SCHED_CBIT_dis_blkr_pm_OFF                        (11)
  #define MC0_CH0_CR_SCHED_CBIT_dis_blkr_pm_WID                        ( 1)
  #define MC0_CH0_CR_SCHED_CBIT_dis_blkr_pm_MSK                        (0x00000800)
  #define MC0_CH0_CR_SCHED_CBIT_dis_blkr_pm_MIN                        (0)
  #define MC0_CH0_CR_SCHED_CBIT_dis_blkr_pm_MAX                        (1) // 0x00000001
  #define MC0_CH0_CR_SCHED_CBIT_dis_blkr_pm_DEF                        (0x00000000)
  #define MC0_CH0_CR_SCHED_CBIT_dis_blkr_pm_HSH                        (0x0116E028)

  #define MC0_CH0_CR_SCHED_CBIT_dis_odt_OFF                            (12)
  #define MC0_CH0_CR_SCHED_CBIT_dis_odt_WID                            ( 1)
  #define MC0_CH0_CR_SCHED_CBIT_dis_odt_MSK                            (0x00001000)
  #define MC0_CH0_CR_SCHED_CBIT_dis_odt_MIN                            (0)
  #define MC0_CH0_CR_SCHED_CBIT_dis_odt_MAX                            (1) // 0x00000001
  #define MC0_CH0_CR_SCHED_CBIT_dis_odt_DEF                            (0x00000000)
  #define MC0_CH0_CR_SCHED_CBIT_dis_odt_HSH                            (0x0118E028)

  #define MC0_CH0_CR_SCHED_CBIT_OE_alw_off_OFF                         (13)
  #define MC0_CH0_CR_SCHED_CBIT_OE_alw_off_WID                         ( 1)
  #define MC0_CH0_CR_SCHED_CBIT_OE_alw_off_MSK                         (0x00002000)
  #define MC0_CH0_CR_SCHED_CBIT_OE_alw_off_MIN                         (0)
  #define MC0_CH0_CR_SCHED_CBIT_OE_alw_off_MAX                         (1) // 0x00000001
  #define MC0_CH0_CR_SCHED_CBIT_OE_alw_off_DEF                         (0x00000000)
  #define MC0_CH0_CR_SCHED_CBIT_OE_alw_off_HSH                         (0x011AE028)

  #define MC0_CH0_CR_SCHED_CBIT_block_rpq_OFF                          (14)
  #define MC0_CH0_CR_SCHED_CBIT_block_rpq_WID                          ( 1)
  #define MC0_CH0_CR_SCHED_CBIT_block_rpq_MSK                          (0x00004000)
  #define MC0_CH0_CR_SCHED_CBIT_block_rpq_MIN                          (0)
  #define MC0_CH0_CR_SCHED_CBIT_block_rpq_MAX                          (1) // 0x00000001
  #define MC0_CH0_CR_SCHED_CBIT_block_rpq_DEF                          (0x00000000)
  #define MC0_CH0_CR_SCHED_CBIT_block_rpq_HSH                          (0x011CE028)

  #define MC0_CH0_CR_SCHED_CBIT_block_ipq_OFF                          (15)
  #define MC0_CH0_CR_SCHED_CBIT_block_ipq_WID                          ( 1)
  #define MC0_CH0_CR_SCHED_CBIT_block_ipq_MSK                          (0x00008000)
  #define MC0_CH0_CR_SCHED_CBIT_block_ipq_MIN                          (0)
  #define MC0_CH0_CR_SCHED_CBIT_block_ipq_MAX                          (1) // 0x00000001
  #define MC0_CH0_CR_SCHED_CBIT_block_ipq_DEF                          (0x00000000)
  #define MC0_CH0_CR_SCHED_CBIT_block_ipq_HSH                          (0x011EE028)

  #define MC0_CH0_CR_SCHED_CBIT_block_wpq_OFF                          (16)
  #define MC0_CH0_CR_SCHED_CBIT_block_wpq_WID                          ( 1)
  #define MC0_CH0_CR_SCHED_CBIT_block_wpq_MSK                          (0x00010000)
  #define MC0_CH0_CR_SCHED_CBIT_block_wpq_MIN                          (0)
  #define MC0_CH0_CR_SCHED_CBIT_block_wpq_MAX                          (1) // 0x00000001
  #define MC0_CH0_CR_SCHED_CBIT_block_wpq_DEF                          (0x00000000)
  #define MC0_CH0_CR_SCHED_CBIT_block_wpq_HSH                          (0x0120E028)

  #define MC0_CH0_CR_SCHED_CBIT_dis_zq_OFF                             (17)
  #define MC0_CH0_CR_SCHED_CBIT_dis_zq_WID                             ( 1)
  #define MC0_CH0_CR_SCHED_CBIT_dis_zq_MSK                             (0x00020000)
  #define MC0_CH0_CR_SCHED_CBIT_dis_zq_MIN                             (0)
  #define MC0_CH0_CR_SCHED_CBIT_dis_zq_MAX                             (1) // 0x00000001
  #define MC0_CH0_CR_SCHED_CBIT_dis_zq_DEF                             (0x00000000)
  #define MC0_CH0_CR_SCHED_CBIT_dis_zq_HSH                             (0x0122E028)

  #define MC0_CH0_CR_SCHED_CBIT_dis_tt_OFF                             (18)
  #define MC0_CH0_CR_SCHED_CBIT_dis_tt_WID                             ( 1)
  #define MC0_CH0_CR_SCHED_CBIT_dis_tt_MSK                             (0x00040000)
  #define MC0_CH0_CR_SCHED_CBIT_dis_tt_MIN                             (0)
  #define MC0_CH0_CR_SCHED_CBIT_dis_tt_MAX                             (1) // 0x00000001
  #define MC0_CH0_CR_SCHED_CBIT_dis_tt_DEF                             (0x00000000)
  #define MC0_CH0_CR_SCHED_CBIT_dis_tt_HSH                             (0x0124E028)

  #define MC0_CH0_CR_SCHED_CBIT_dis_opp_ref_OFF                        (19)
  #define MC0_CH0_CR_SCHED_CBIT_dis_opp_ref_WID                        ( 1)
  #define MC0_CH0_CR_SCHED_CBIT_dis_opp_ref_MSK                        (0x00080000)
  #define MC0_CH0_CR_SCHED_CBIT_dis_opp_ref_MIN                        (0)
  #define MC0_CH0_CR_SCHED_CBIT_dis_opp_ref_MAX                        (1) // 0x00000001
  #define MC0_CH0_CR_SCHED_CBIT_dis_opp_ref_DEF                        (0x00000000)
  #define MC0_CH0_CR_SCHED_CBIT_dis_opp_ref_HSH                        (0x0126E028)

  #define MC0_CH0_CR_SCHED_CBIT_long_zq_OFF                            (20)
  #define MC0_CH0_CR_SCHED_CBIT_long_zq_WID                            ( 1)
  #define MC0_CH0_CR_SCHED_CBIT_long_zq_MSK                            (0x00100000)
  #define MC0_CH0_CR_SCHED_CBIT_long_zq_MIN                            (0)
  #define MC0_CH0_CR_SCHED_CBIT_long_zq_MAX                            (1) // 0x00000001
  #define MC0_CH0_CR_SCHED_CBIT_long_zq_DEF                            (0x00000000)
  #define MC0_CH0_CR_SCHED_CBIT_long_zq_HSH                            (0x0128E028)

  #define MC0_CH0_CR_SCHED_CBIT_dis_srx_zq_OFF                         (21)
  #define MC0_CH0_CR_SCHED_CBIT_dis_srx_zq_WID                         ( 1)
  #define MC0_CH0_CR_SCHED_CBIT_dis_srx_zq_MSK                         (0x00200000)
  #define MC0_CH0_CR_SCHED_CBIT_dis_srx_zq_MIN                         (0)
  #define MC0_CH0_CR_SCHED_CBIT_dis_srx_zq_MAX                         (1) // 0x00000001
  #define MC0_CH0_CR_SCHED_CBIT_dis_srx_zq_DEF                         (0x00000000)
  #define MC0_CH0_CR_SCHED_CBIT_dis_srx_zq_HSH                         (0x012AE028)

  #define MC0_CH0_CR_SCHED_CBIT_serialize_zq_OFF                       (22)
  #define MC0_CH0_CR_SCHED_CBIT_serialize_zq_WID                       ( 1)
  #define MC0_CH0_CR_SCHED_CBIT_serialize_zq_MSK                       (0x00400000)
  #define MC0_CH0_CR_SCHED_CBIT_serialize_zq_MIN                       (0)
  #define MC0_CH0_CR_SCHED_CBIT_serialize_zq_MAX                       (1) // 0x00000001
  #define MC0_CH0_CR_SCHED_CBIT_serialize_zq_DEF                       (0x00000000)
  #define MC0_CH0_CR_SCHED_CBIT_serialize_zq_HSH                       (0x012CE028)

  #define MC0_CH0_CR_SCHED_CBIT_zq_fast_exec_OFF                       (23)
  #define MC0_CH0_CR_SCHED_CBIT_zq_fast_exec_WID                       ( 1)
  #define MC0_CH0_CR_SCHED_CBIT_zq_fast_exec_MSK                       (0x00800000)
  #define MC0_CH0_CR_SCHED_CBIT_zq_fast_exec_MIN                       (0)
  #define MC0_CH0_CR_SCHED_CBIT_zq_fast_exec_MAX                       (1) // 0x00000001
  #define MC0_CH0_CR_SCHED_CBIT_zq_fast_exec_DEF                       (0x00000000)
  #define MC0_CH0_CR_SCHED_CBIT_zq_fast_exec_HSH                       (0x012EE028)

  #define MC0_CH0_CR_SCHED_CBIT_dis_starved_read_rank_block_OFF        (24)
  #define MC0_CH0_CR_SCHED_CBIT_dis_starved_read_rank_block_WID        ( 1)
  #define MC0_CH0_CR_SCHED_CBIT_dis_starved_read_rank_block_MSK        (0x01000000)
  #define MC0_CH0_CR_SCHED_CBIT_dis_starved_read_rank_block_MIN        (0)
  #define MC0_CH0_CR_SCHED_CBIT_dis_starved_read_rank_block_MAX        (1) // 0x00000001
  #define MC0_CH0_CR_SCHED_CBIT_dis_starved_read_rank_block_DEF        (0x00000000)
  #define MC0_CH0_CR_SCHED_CBIT_dis_starved_read_rank_block_HSH        (0x0130E028)

  #define MC0_CH0_CR_SCHED_CBIT_dis_idle_preempt_OFF                   (25)
  #define MC0_CH0_CR_SCHED_CBIT_dis_idle_preempt_WID                   ( 1)
  #define MC0_CH0_CR_SCHED_CBIT_dis_idle_preempt_MSK                   (0x02000000)
  #define MC0_CH0_CR_SCHED_CBIT_dis_idle_preempt_MIN                   (0)
  #define MC0_CH0_CR_SCHED_CBIT_dis_idle_preempt_MAX                   (1) // 0x00000001
  #define MC0_CH0_CR_SCHED_CBIT_dis_idle_preempt_DEF                   (0x00000000)
  #define MC0_CH0_CR_SCHED_CBIT_dis_idle_preempt_HSH                   (0x0132E028)

  #define MC0_CH0_CR_SCHED_CBIT_find_first_allocation_OFF              (26)
  #define MC0_CH0_CR_SCHED_CBIT_find_first_allocation_WID              ( 1)
  #define MC0_CH0_CR_SCHED_CBIT_find_first_allocation_MSK              (0x04000000)
  #define MC0_CH0_CR_SCHED_CBIT_find_first_allocation_MIN              (0)
  #define MC0_CH0_CR_SCHED_CBIT_find_first_allocation_MAX              (1) // 0x00000001
  #define MC0_CH0_CR_SCHED_CBIT_find_first_allocation_DEF              (0x00000000)
  #define MC0_CH0_CR_SCHED_CBIT_find_first_allocation_HSH              (0x0134E028)

  #define MC0_CH0_CR_SCHED_CBIT_duplicate_spid_rank_OFF                (27)
  #define MC0_CH0_CR_SCHED_CBIT_duplicate_spid_rank_WID                ( 1)
  #define MC0_CH0_CR_SCHED_CBIT_duplicate_spid_rank_MSK                (0x08000000)
  #define MC0_CH0_CR_SCHED_CBIT_duplicate_spid_rank_MIN                (0)
  #define MC0_CH0_CR_SCHED_CBIT_duplicate_spid_rank_MAX                (1) // 0x00000001
  #define MC0_CH0_CR_SCHED_CBIT_duplicate_spid_rank_DEF                (0x00000000)
  #define MC0_CH0_CR_SCHED_CBIT_duplicate_spid_rank_HSH                (0x0136E028)

  #define MC0_CH0_CR_SCHED_CBIT_wio_merge_lpmode_2_3_OFF               (28)
  #define MC0_CH0_CR_SCHED_CBIT_wio_merge_lpmode_2_3_WID               ( 1)
  #define MC0_CH0_CR_SCHED_CBIT_wio_merge_lpmode_2_3_MSK               (0x10000000)
  #define MC0_CH0_CR_SCHED_CBIT_wio_merge_lpmode_2_3_MIN               (0)
  #define MC0_CH0_CR_SCHED_CBIT_wio_merge_lpmode_2_3_MAX               (1) // 0x00000001
  #define MC0_CH0_CR_SCHED_CBIT_wio_merge_lpmode_2_3_DEF               (0x00000000)
  #define MC0_CH0_CR_SCHED_CBIT_wio_merge_lpmode_2_3_HSH               (0x0138E028)

  #define MC0_CH0_CR_SCHED_CBIT_dis_5cyc_write_gap_OFF                 (29)
  #define MC0_CH0_CR_SCHED_CBIT_dis_5cyc_write_gap_WID                 ( 1)
  #define MC0_CH0_CR_SCHED_CBIT_dis_5cyc_write_gap_MSK                 (0x20000000)
  #define MC0_CH0_CR_SCHED_CBIT_dis_5cyc_write_gap_MIN                 (0)
  #define MC0_CH0_CR_SCHED_CBIT_dis_5cyc_write_gap_MAX                 (1) // 0x00000001
  #define MC0_CH0_CR_SCHED_CBIT_dis_5cyc_write_gap_DEF                 (0x00000000)
  #define MC0_CH0_CR_SCHED_CBIT_dis_5cyc_write_gap_HSH                 (0x013AE028)

  #define MC0_CH0_CR_SCHED_CBIT_dis_5cyc_read_gap_OFF                  (30)
  #define MC0_CH0_CR_SCHED_CBIT_dis_5cyc_read_gap_WID                  ( 1)
  #define MC0_CH0_CR_SCHED_CBIT_dis_5cyc_read_gap_MSK                  (0x40000000)
  #define MC0_CH0_CR_SCHED_CBIT_dis_5cyc_read_gap_MIN                  (0)
  #define MC0_CH0_CR_SCHED_CBIT_dis_5cyc_read_gap_MAX                  (1) // 0x00000001
  #define MC0_CH0_CR_SCHED_CBIT_dis_5cyc_read_gap_DEF                  (0x00000000)
  #define MC0_CH0_CR_SCHED_CBIT_dis_5cyc_read_gap_HSH                  (0x013CE028)

  #define MC0_CH0_CR_SCHED_CBIT_dis_clk_gate_OFF                       (31)
  #define MC0_CH0_CR_SCHED_CBIT_dis_clk_gate_WID                       ( 1)
  #define MC0_CH0_CR_SCHED_CBIT_dis_clk_gate_MSK                       (0x80000000)
  #define MC0_CH0_CR_SCHED_CBIT_dis_clk_gate_MIN                       (0)
  #define MC0_CH0_CR_SCHED_CBIT_dis_clk_gate_MAX                       (1) // 0x00000001
  #define MC0_CH0_CR_SCHED_CBIT_dis_clk_gate_DEF                       (0x00000000)
  #define MC0_CH0_CR_SCHED_CBIT_dis_clk_gate_HSH                       (0x013EE028)

#define MC0_CH0_CR_SCHED_SECOND_CBIT_REG                               (0x0000E02C)

  #define MC0_CH0_CR_SCHED_SECOND_CBIT_dis_srx_mr4_OFF                 ( 0)
  #define MC0_CH0_CR_SCHED_SECOND_CBIT_dis_srx_mr4_WID                 ( 1)
  #define MC0_CH0_CR_SCHED_SECOND_CBIT_dis_srx_mr4_MSK                 (0x00000001)
  #define MC0_CH0_CR_SCHED_SECOND_CBIT_dis_srx_mr4_MIN                 (0)
  #define MC0_CH0_CR_SCHED_SECOND_CBIT_dis_srx_mr4_MAX                 (1) // 0x00000001
  #define MC0_CH0_CR_SCHED_SECOND_CBIT_dis_srx_mr4_DEF                 (0x00000000)
  #define MC0_CH0_CR_SCHED_SECOND_CBIT_dis_srx_mr4_HSH                 (0x0100E02C)

  #define MC0_CH0_CR_SCHED_SECOND_CBIT_dis_ck_tristate_OFF             ( 1)
  #define MC0_CH0_CR_SCHED_SECOND_CBIT_dis_ck_tristate_WID             ( 1)
  #define MC0_CH0_CR_SCHED_SECOND_CBIT_dis_ck_tristate_MSK             (0x00000002)
  #define MC0_CH0_CR_SCHED_SECOND_CBIT_dis_ck_tristate_MIN             (0)
  #define MC0_CH0_CR_SCHED_SECOND_CBIT_dis_ck_tristate_MAX             (1) // 0x00000001
  #define MC0_CH0_CR_SCHED_SECOND_CBIT_dis_ck_tristate_DEF             (0x00000000)
  #define MC0_CH0_CR_SCHED_SECOND_CBIT_dis_ck_tristate_HSH             (0x0102E02C)

  #define MC0_CH0_CR_SCHED_SECOND_CBIT_revert_to_pre_gear4_cancel_OFF  ( 2)
  #define MC0_CH0_CR_SCHED_SECOND_CBIT_revert_to_pre_gear4_cancel_WID  ( 1)
  #define MC0_CH0_CR_SCHED_SECOND_CBIT_revert_to_pre_gear4_cancel_MSK  (0x00000004)
  #define MC0_CH0_CR_SCHED_SECOND_CBIT_revert_to_pre_gear4_cancel_MIN  (0)
  #define MC0_CH0_CR_SCHED_SECOND_CBIT_revert_to_pre_gear4_cancel_MAX  (1) // 0x00000001
  #define MC0_CH0_CR_SCHED_SECOND_CBIT_revert_to_pre_gear4_cancel_DEF  (0x00000000)
  #define MC0_CH0_CR_SCHED_SECOND_CBIT_revert_to_pre_gear4_cancel_HSH  (0x0104E02C)

  #define MC0_CH0_CR_SCHED_SECOND_CBIT_dis_bus_retain_OFF              ( 3)
  #define MC0_CH0_CR_SCHED_SECOND_CBIT_dis_bus_retain_WID              ( 1)
  #define MC0_CH0_CR_SCHED_SECOND_CBIT_dis_bus_retain_MSK              (0x00000008)
  #define MC0_CH0_CR_SCHED_SECOND_CBIT_dis_bus_retain_MIN              (0)
  #define MC0_CH0_CR_SCHED_SECOND_CBIT_dis_bus_retain_MAX              (1) // 0x00000001
  #define MC0_CH0_CR_SCHED_SECOND_CBIT_dis_bus_retain_DEF              (0x00000000)
  #define MC0_CH0_CR_SCHED_SECOND_CBIT_dis_bus_retain_HSH              (0x0106E02C)

  #define MC0_CH0_CR_SCHED_SECOND_CBIT_dis_async_odt_OFF               ( 4)
  #define MC0_CH0_CR_SCHED_SECOND_CBIT_dis_async_odt_WID               ( 1)
  #define MC0_CH0_CR_SCHED_SECOND_CBIT_dis_async_odt_MSK               (0x00000010)
  #define MC0_CH0_CR_SCHED_SECOND_CBIT_dis_async_odt_MIN               (0)
  #define MC0_CH0_CR_SCHED_SECOND_CBIT_dis_async_odt_MAX               (1) // 0x00000001
  #define MC0_CH0_CR_SCHED_SECOND_CBIT_dis_async_odt_DEF               (0x00000000)
  #define MC0_CH0_CR_SCHED_SECOND_CBIT_dis_async_odt_HSH               (0x0108E02C)

  #define MC0_CH0_CR_SCHED_SECOND_CBIT_dis_SRX_reset_OFF               ( 5)
  #define MC0_CH0_CR_SCHED_SECOND_CBIT_dis_SRX_reset_WID               ( 1)
  #define MC0_CH0_CR_SCHED_SECOND_CBIT_dis_SRX_reset_MSK               (0x00000020)
  #define MC0_CH0_CR_SCHED_SECOND_CBIT_dis_SRX_reset_MIN               (0)
  #define MC0_CH0_CR_SCHED_SECOND_CBIT_dis_SRX_reset_MAX               (1) // 0x00000001
  #define MC0_CH0_CR_SCHED_SECOND_CBIT_dis_SRX_reset_DEF               (0x00000000)
  #define MC0_CH0_CR_SCHED_SECOND_CBIT_dis_SRX_reset_HSH               (0x010AE02C)

  #define MC0_CH0_CR_SCHED_SECOND_CBIT_disable_starved_prio_on_new_req_OFF ( 6)
  #define MC0_CH0_CR_SCHED_SECOND_CBIT_disable_starved_prio_on_new_req_WID ( 1)
  #define MC0_CH0_CR_SCHED_SECOND_CBIT_disable_starved_prio_on_new_req_MSK (0x00000040)
  #define MC0_CH0_CR_SCHED_SECOND_CBIT_disable_starved_prio_on_new_req_MIN (0)
  #define MC0_CH0_CR_SCHED_SECOND_CBIT_disable_starved_prio_on_new_req_MAX (1) // 0x00000001
  #define MC0_CH0_CR_SCHED_SECOND_CBIT_disable_starved_prio_on_new_req_DEF (0x00000000)
  #define MC0_CH0_CR_SCHED_SECOND_CBIT_disable_starved_prio_on_new_req_HSH (0x010CE02C)

  #define MC0_CH0_CR_SCHED_SECOND_CBIT_dis_SRX_MRS_MR4_OFF             ( 7)
  #define MC0_CH0_CR_SCHED_SECOND_CBIT_dis_SRX_MRS_MR4_WID             ( 1)
  #define MC0_CH0_CR_SCHED_SECOND_CBIT_dis_SRX_MRS_MR4_MSK             (0x00000080)
  #define MC0_CH0_CR_SCHED_SECOND_CBIT_dis_SRX_MRS_MR4_MIN             (0)
  #define MC0_CH0_CR_SCHED_SECOND_CBIT_dis_SRX_MRS_MR4_MAX             (1) // 0x00000001
  #define MC0_CH0_CR_SCHED_SECOND_CBIT_dis_SRX_MRS_MR4_DEF             (0x00000000)
  #define MC0_CH0_CR_SCHED_SECOND_CBIT_dis_SRX_MRS_MR4_HSH             (0x010EE02C)

  #define MC0_CH0_CR_SCHED_SECOND_CBIT_dis_opp_ref_idle_delay_OFF      ( 8)
  #define MC0_CH0_CR_SCHED_SECOND_CBIT_dis_opp_ref_idle_delay_WID      ( 1)
  #define MC0_CH0_CR_SCHED_SECOND_CBIT_dis_opp_ref_idle_delay_MSK      (0x00000100)
  #define MC0_CH0_CR_SCHED_SECOND_CBIT_dis_opp_ref_idle_delay_MIN      (0)
  #define MC0_CH0_CR_SCHED_SECOND_CBIT_dis_opp_ref_idle_delay_MAX      (1) // 0x00000001
  #define MC0_CH0_CR_SCHED_SECOND_CBIT_dis_opp_ref_idle_delay_DEF      (0x00000000)
  #define MC0_CH0_CR_SCHED_SECOND_CBIT_dis_opp_ref_idle_delay_HSH      (0x0110E02C)

  #define MC0_CH0_CR_SCHED_SECOND_CBIT_dis_ignore_1st_trefi_OFF        ( 9)
  #define MC0_CH0_CR_SCHED_SECOND_CBIT_dis_ignore_1st_trefi_WID        ( 1)
  #define MC0_CH0_CR_SCHED_SECOND_CBIT_dis_ignore_1st_trefi_MSK        (0x00000200)
  #define MC0_CH0_CR_SCHED_SECOND_CBIT_dis_ignore_1st_trefi_MIN        (0)
  #define MC0_CH0_CR_SCHED_SECOND_CBIT_dis_ignore_1st_trefi_MAX        (1) // 0x00000001
  #define MC0_CH0_CR_SCHED_SECOND_CBIT_dis_ignore_1st_trefi_DEF        (0x00000000)
  #define MC0_CH0_CR_SCHED_SECOND_CBIT_dis_ignore_1st_trefi_HSH        (0x0112E02C)

  #define MC0_CH0_CR_SCHED_SECOND_CBIT_echo_mask_OFF                   (10)
  #define MC0_CH0_CR_SCHED_SECOND_CBIT_echo_mask_WID                   ( 9)
  #define MC0_CH0_CR_SCHED_SECOND_CBIT_echo_mask_MSK                   (0x0007FC00)
  #define MC0_CH0_CR_SCHED_SECOND_CBIT_echo_mask_MIN                   (0)
  #define MC0_CH0_CR_SCHED_SECOND_CBIT_echo_mask_MAX                   (511) // 0x000001FF
  #define MC0_CH0_CR_SCHED_SECOND_CBIT_echo_mask_DEF                   (0x00000000)
  #define MC0_CH0_CR_SCHED_SECOND_CBIT_echo_mask_HSH                   (0x0914E02C)

  #define MC0_CH0_CR_SCHED_SECOND_CBIT_dis_early_go_OFF                (19)
  #define MC0_CH0_CR_SCHED_SECOND_CBIT_dis_early_go_WID                ( 1)
  #define MC0_CH0_CR_SCHED_SECOND_CBIT_dis_early_go_MSK                (0x00080000)
  #define MC0_CH0_CR_SCHED_SECOND_CBIT_dis_early_go_MIN                (0)
  #define MC0_CH0_CR_SCHED_SECOND_CBIT_dis_early_go_MAX                (1) // 0x00000001
  #define MC0_CH0_CR_SCHED_SECOND_CBIT_dis_early_go_DEF                (0x00000000)
  #define MC0_CH0_CR_SCHED_SECOND_CBIT_dis_early_go_HSH                (0x0126E02C)

  #define MC0_CH0_CR_SCHED_SECOND_CBIT_dis_new_wr_to_full_OFF          (20)
  #define MC0_CH0_CR_SCHED_SECOND_CBIT_dis_new_wr_to_full_WID          ( 1)
  #define MC0_CH0_CR_SCHED_SECOND_CBIT_dis_new_wr_to_full_MSK          (0x00100000)
  #define MC0_CH0_CR_SCHED_SECOND_CBIT_dis_new_wr_to_full_MIN          (0)
  #define MC0_CH0_CR_SCHED_SECOND_CBIT_dis_new_wr_to_full_MAX          (1) // 0x00000001
  #define MC0_CH0_CR_SCHED_SECOND_CBIT_dis_new_wr_to_full_DEF          (0x00000000)
  #define MC0_CH0_CR_SCHED_SECOND_CBIT_dis_new_wr_to_full_HSH          (0x0128E02C)

  #define MC0_CH0_CR_SCHED_SECOND_CBIT_cmd_bgf_always_on_OFF           (21)
  #define MC0_CH0_CR_SCHED_SECOND_CBIT_cmd_bgf_always_on_WID           ( 1)
  #define MC0_CH0_CR_SCHED_SECOND_CBIT_cmd_bgf_always_on_MSK           (0x00200000)
  #define MC0_CH0_CR_SCHED_SECOND_CBIT_cmd_bgf_always_on_MIN           (0)
  #define MC0_CH0_CR_SCHED_SECOND_CBIT_cmd_bgf_always_on_MAX           (1) // 0x00000001
  #define MC0_CH0_CR_SCHED_SECOND_CBIT_cmd_bgf_always_on_DEF           (0x00000000)
  #define MC0_CH0_CR_SCHED_SECOND_CBIT_cmd_bgf_always_on_HSH           (0x012AE02C)

  #define MC0_CH0_CR_SCHED_SECOND_CBIT_drainless_SAGV_do_zq_OFF        (22)
  #define MC0_CH0_CR_SCHED_SECOND_CBIT_drainless_SAGV_do_zq_WID        ( 1)
  #define MC0_CH0_CR_SCHED_SECOND_CBIT_drainless_SAGV_do_zq_MSK        (0x00400000)
  #define MC0_CH0_CR_SCHED_SECOND_CBIT_drainless_SAGV_do_zq_MIN        (0)
  #define MC0_CH0_CR_SCHED_SECOND_CBIT_drainless_SAGV_do_zq_MAX        (1) // 0x00000001
  #define MC0_CH0_CR_SCHED_SECOND_CBIT_drainless_SAGV_do_zq_DEF        (0x00000000)
  #define MC0_CH0_CR_SCHED_SECOND_CBIT_drainless_SAGV_do_zq_HSH        (0x012CE02C)

  #define MC0_CH0_CR_SCHED_SECOND_CBIT_do_temp_reading_after_drainless_SAGV_OFF (23)
  #define MC0_CH0_CR_SCHED_SECOND_CBIT_do_temp_reading_after_drainless_SAGV_WID ( 1)
  #define MC0_CH0_CR_SCHED_SECOND_CBIT_do_temp_reading_after_drainless_SAGV_MSK (0x00800000)
  #define MC0_CH0_CR_SCHED_SECOND_CBIT_do_temp_reading_after_drainless_SAGV_MIN (0)
  #define MC0_CH0_CR_SCHED_SECOND_CBIT_do_temp_reading_after_drainless_SAGV_MAX (1) // 0x00000001
  #define MC0_CH0_CR_SCHED_SECOND_CBIT_do_temp_reading_after_drainless_SAGV_DEF (0x00000000)
  #define MC0_CH0_CR_SCHED_SECOND_CBIT_do_temp_reading_after_drainless_SAGV_HSH (0x012EE02C)

  #define MC0_CH0_CR_SCHED_SECOND_CBIT_ignore_refresh_between_SR_OFF   (24)
  #define MC0_CH0_CR_SCHED_SECOND_CBIT_ignore_refresh_between_SR_WID   ( 1)
  #define MC0_CH0_CR_SCHED_SECOND_CBIT_ignore_refresh_between_SR_MSK   (0x01000000)
  #define MC0_CH0_CR_SCHED_SECOND_CBIT_ignore_refresh_between_SR_MIN   (0)
  #define MC0_CH0_CR_SCHED_SECOND_CBIT_ignore_refresh_between_SR_MAX   (1) // 0x00000001
  #define MC0_CH0_CR_SCHED_SECOND_CBIT_ignore_refresh_between_SR_DEF   (0x00000000)
  #define MC0_CH0_CR_SCHED_SECOND_CBIT_ignore_refresh_between_SR_HSH   (0x0130E02C)

  #define MC0_CH0_CR_SCHED_SECOND_CBIT_DisWrActThrottleOnAnyRead_OFF   (25)
  #define MC0_CH0_CR_SCHED_SECOND_CBIT_DisWrActThrottleOnAnyRead_WID   ( 1)
  #define MC0_CH0_CR_SCHED_SECOND_CBIT_DisWrActThrottleOnAnyRead_MSK   (0x02000000)
  #define MC0_CH0_CR_SCHED_SECOND_CBIT_DisWrActThrottleOnAnyRead_MIN   (0)
  #define MC0_CH0_CR_SCHED_SECOND_CBIT_DisWrActThrottleOnAnyRead_MAX   (1) // 0x00000001
  #define MC0_CH0_CR_SCHED_SECOND_CBIT_DisWrActThrottleOnAnyRead_DEF   (0x00000001)
  #define MC0_CH0_CR_SCHED_SECOND_CBIT_DisWrActThrottleOnAnyRead_HSH   (0x0132E02C)

  #define MC0_CH0_CR_SCHED_SECOND_CBIT_enable_fdata_nak_OFF            (26)
  #define MC0_CH0_CR_SCHED_SECOND_CBIT_enable_fdata_nak_WID            ( 1)
  #define MC0_CH0_CR_SCHED_SECOND_CBIT_enable_fdata_nak_MSK            (0x04000000)
  #define MC0_CH0_CR_SCHED_SECOND_CBIT_enable_fdata_nak_MIN            (0)
  #define MC0_CH0_CR_SCHED_SECOND_CBIT_enable_fdata_nak_MAX            (1) // 0x00000001
  #define MC0_CH0_CR_SCHED_SECOND_CBIT_enable_fdata_nak_DEF            (0x00000001)
  #define MC0_CH0_CR_SCHED_SECOND_CBIT_enable_fdata_nak_HSH            (0x0134E02C)

  #define MC0_CH0_CR_SCHED_SECOND_CBIT_disable_wr_on_SAGV_exit_for_DCC_OFF (27)
  #define MC0_CH0_CR_SCHED_SECOND_CBIT_disable_wr_on_SAGV_exit_for_DCC_WID ( 1)
  #define MC0_CH0_CR_SCHED_SECOND_CBIT_disable_wr_on_SAGV_exit_for_DCC_MSK (0x08000000)
  #define MC0_CH0_CR_SCHED_SECOND_CBIT_disable_wr_on_SAGV_exit_for_DCC_MIN (0)
  #define MC0_CH0_CR_SCHED_SECOND_CBIT_disable_wr_on_SAGV_exit_for_DCC_MAX (1) // 0x00000001
  #define MC0_CH0_CR_SCHED_SECOND_CBIT_disable_wr_on_SAGV_exit_for_DCC_DEF (0x00000000)
  #define MC0_CH0_CR_SCHED_SECOND_CBIT_disable_wr_on_SAGV_exit_for_DCC_HSH (0x0136E02C)

  #define MC0_CH0_CR_SCHED_SECOND_CBIT_disable_tCWL_offset_in_wrdata_OFF (28)
  #define MC0_CH0_CR_SCHED_SECOND_CBIT_disable_tCWL_offset_in_wrdata_WID ( 1)
  #define MC0_CH0_CR_SCHED_SECOND_CBIT_disable_tCWL_offset_in_wrdata_MSK (0x10000000)
  #define MC0_CH0_CR_SCHED_SECOND_CBIT_disable_tCWL_offset_in_wrdata_MIN (0)
  #define MC0_CH0_CR_SCHED_SECOND_CBIT_disable_tCWL_offset_in_wrdata_MAX (1) // 0x00000001
  #define MC0_CH0_CR_SCHED_SECOND_CBIT_disable_tCWL_offset_in_wrdata_DEF (0x00000000)
  #define MC0_CH0_CR_SCHED_SECOND_CBIT_disable_tCWL_offset_in_wrdata_HSH (0x0138E02C)

  #define MC0_CH0_CR_SCHED_SECOND_CBIT_disable_cas_clubbing_OFF        (29)
  #define MC0_CH0_CR_SCHED_SECOND_CBIT_disable_cas_clubbing_WID        ( 1)
  #define MC0_CH0_CR_SCHED_SECOND_CBIT_disable_cas_clubbing_MSK        (0x20000000)
  #define MC0_CH0_CR_SCHED_SECOND_CBIT_disable_cas_clubbing_MIN        (0)
  #define MC0_CH0_CR_SCHED_SECOND_CBIT_disable_cas_clubbing_MAX        (1) // 0x00000001
  #define MC0_CH0_CR_SCHED_SECOND_CBIT_disable_cas_clubbing_DEF        (0x00000000)
  #define MC0_CH0_CR_SCHED_SECOND_CBIT_disable_cas_clubbing_HSH        (0x013AE02C)

  #define MC0_CH0_CR_SCHED_SECOND_CBIT_two_cyc_early_ca_valid_OFF      (30)
  #define MC0_CH0_CR_SCHED_SECOND_CBIT_two_cyc_early_ca_valid_WID      ( 1)
  #define MC0_CH0_CR_SCHED_SECOND_CBIT_two_cyc_early_ca_valid_MSK      (0x40000000)
  #define MC0_CH0_CR_SCHED_SECOND_CBIT_two_cyc_early_ca_valid_MIN      (0)
  #define MC0_CH0_CR_SCHED_SECOND_CBIT_two_cyc_early_ca_valid_MAX      (1) // 0x00000001
  #define MC0_CH0_CR_SCHED_SECOND_CBIT_two_cyc_early_ca_valid_DEF      (0x00000001)
  #define MC0_CH0_CR_SCHED_SECOND_CBIT_two_cyc_early_ca_valid_HSH      (0x013CE02C)

  #define MC0_CH0_CR_SCHED_SECOND_CBIT_disable_starv2med_prio_on_new_req_OFF (31)
  #define MC0_CH0_CR_SCHED_SECOND_CBIT_disable_starv2med_prio_on_new_req_WID ( 1)
  #define MC0_CH0_CR_SCHED_SECOND_CBIT_disable_starv2med_prio_on_new_req_MSK (0x80000000)
  #define MC0_CH0_CR_SCHED_SECOND_CBIT_disable_starv2med_prio_on_new_req_MIN (0)
  #define MC0_CH0_CR_SCHED_SECOND_CBIT_disable_starv2med_prio_on_new_req_MAX (1) // 0x00000001
  #define MC0_CH0_CR_SCHED_SECOND_CBIT_disable_starv2med_prio_on_new_req_DEF (0x00000000)
  #define MC0_CH0_CR_SCHED_SECOND_CBIT_disable_starv2med_prio_on_new_req_HSH (0x013EE02C)

#define MC0_CH0_CR_DFT_MISC_REG                                        (0x0000E030)

  #define MC0_CH0_CR_DFT_MISC_WDAR_OFF                                 ( 0)
  #define MC0_CH0_CR_DFT_MISC_WDAR_WID                                 ( 1)
  #define MC0_CH0_CR_DFT_MISC_WDAR_MSK                                 (0x00000001)
  #define MC0_CH0_CR_DFT_MISC_WDAR_MIN                                 (0)
  #define MC0_CH0_CR_DFT_MISC_WDAR_MAX                                 (1) // 0x00000001
  #define MC0_CH0_CR_DFT_MISC_WDAR_DEF                                 (0x00000000)
  #define MC0_CH0_CR_DFT_MISC_WDAR_HSH                                 (0x0100E030)

  #define MC0_CH0_CR_DFT_MISC_WDB_Block_En_OFF                         ( 1)
  #define MC0_CH0_CR_DFT_MISC_WDB_Block_En_WID                         ( 1)
  #define MC0_CH0_CR_DFT_MISC_WDB_Block_En_MSK                         (0x00000002)
  #define MC0_CH0_CR_DFT_MISC_WDB_Block_En_MIN                         (0)
  #define MC0_CH0_CR_DFT_MISC_WDB_Block_En_MAX                         (1) // 0x00000001
  #define MC0_CH0_CR_DFT_MISC_WDB_Block_En_DEF                         (0x00000000)
  #define MC0_CH0_CR_DFT_MISC_WDB_Block_En_HSH                         (0x0102E030)

  #define MC0_CH0_CR_DFT_MISC_isolate_data_return_OFF                  ( 4)
  #define MC0_CH0_CR_DFT_MISC_isolate_data_return_WID                  ( 1)
  #define MC0_CH0_CR_DFT_MISC_isolate_data_return_MSK                  (0x00000010)
  #define MC0_CH0_CR_DFT_MISC_isolate_data_return_MIN                  (0)
  #define MC0_CH0_CR_DFT_MISC_isolate_data_return_MAX                  (1) // 0x00000001
  #define MC0_CH0_CR_DFT_MISC_isolate_data_return_DEF                  (0x00000000)
  #define MC0_CH0_CR_DFT_MISC_isolate_data_return_HSH                  (0x0108E030)

  #define MC0_CH0_CR_DFT_MISC_dft_block_enable_OFF                     (17)
  #define MC0_CH0_CR_DFT_MISC_dft_block_enable_WID                     ( 1)
  #define MC0_CH0_CR_DFT_MISC_dft_block_enable_MSK                     (0x00020000)
  #define MC0_CH0_CR_DFT_MISC_dft_block_enable_MIN                     (0)
  #define MC0_CH0_CR_DFT_MISC_dft_block_enable_MAX                     (1) // 0x00000001
  #define MC0_CH0_CR_DFT_MISC_dft_block_enable_DEF                     (0x00000000)
  #define MC0_CH0_CR_DFT_MISC_dft_block_enable_HSH                     (0x0122E030)

  #define MC0_CH0_CR_DFT_MISC_SAM_overloading_OFF                      (18)
  #define MC0_CH0_CR_DFT_MISC_SAM_overloading_WID                      ( 3)
  #define MC0_CH0_CR_DFT_MISC_SAM_overloading_MSK                      (0x001C0000)
  #define MC0_CH0_CR_DFT_MISC_SAM_overloading_MIN                      (0)
  #define MC0_CH0_CR_DFT_MISC_SAM_overloading_MAX                      (7) // 0x00000007
  #define MC0_CH0_CR_DFT_MISC_SAM_overloading_DEF                      (0x00000000)
  #define MC0_CH0_CR_DFT_MISC_SAM_overloading_HSH                      (0x0324E030)

  #define MC0_CH0_CR_DFT_MISC_Stretch_mode_en_OFF                      (24)
  #define MC0_CH0_CR_DFT_MISC_Stretch_mode_en_WID                      ( 1)
  #define MC0_CH0_CR_DFT_MISC_Stretch_mode_en_MSK                      (0x01000000)
  #define MC0_CH0_CR_DFT_MISC_Stretch_mode_en_MIN                      (0)
  #define MC0_CH0_CR_DFT_MISC_Stretch_mode_en_MAX                      (1) // 0x00000001
  #define MC0_CH0_CR_DFT_MISC_Stretch_mode_en_DEF                      (0x00000000)
  #define MC0_CH0_CR_DFT_MISC_Stretch_mode_en_HSH                      (0x0130E030)

  #define MC0_CH0_CR_DFT_MISC_STF_OFF                                  (25)
  #define MC0_CH0_CR_DFT_MISC_STF_WID                                  ( 5)
  #define MC0_CH0_CR_DFT_MISC_STF_MSK                                  (0x3E000000)
  #define MC0_CH0_CR_DFT_MISC_STF_MIN                                  (0)
  #define MC0_CH0_CR_DFT_MISC_STF_MAX                                  (31) // 0x0000001F
  #define MC0_CH0_CR_DFT_MISC_STF_DEF                                  (0x00000000)
  #define MC0_CH0_CR_DFT_MISC_STF_HSH                                  (0x0532E030)

  #define MC0_CH0_CR_DFT_MISC_Stretch_delay_from_write_OFF             (30)
  #define MC0_CH0_CR_DFT_MISC_Stretch_delay_from_write_WID             ( 1)
  #define MC0_CH0_CR_DFT_MISC_Stretch_delay_from_write_MSK             (0x40000000)
  #define MC0_CH0_CR_DFT_MISC_Stretch_delay_from_write_MIN             (0)
  #define MC0_CH0_CR_DFT_MISC_Stretch_delay_from_write_MAX             (1) // 0x00000001
  #define MC0_CH0_CR_DFT_MISC_Stretch_delay_from_write_DEF             (0x00000000)
  #define MC0_CH0_CR_DFT_MISC_Stretch_delay_from_write_HSH             (0x013CE030)

#define MC0_CH0_CR_SC_PCIT_REG                                         (0x0000E034)

  #define MC0_CH0_CR_SC_PCIT_PCIT_SUBCH0_OFF                           ( 0)
  #define MC0_CH0_CR_SC_PCIT_PCIT_SUBCH0_WID                           (10)
  #define MC0_CH0_CR_SC_PCIT_PCIT_SUBCH0_MSK                           (0x000003FF)
  #define MC0_CH0_CR_SC_PCIT_PCIT_SUBCH0_MIN                           (0)
  #define MC0_CH0_CR_SC_PCIT_PCIT_SUBCH0_MAX                           (1023) // 0x000003FF
  #define MC0_CH0_CR_SC_PCIT_PCIT_SUBCH0_DEF                           (0x00000040)
  #define MC0_CH0_CR_SC_PCIT_PCIT_SUBCH0_HSH                           (0x0A00E034)

  #define MC0_CH0_CR_SC_PCIT_PCIT_SUBCH1_OFF                           (10)
  #define MC0_CH0_CR_SC_PCIT_PCIT_SUBCH1_WID                           (10)
  #define MC0_CH0_CR_SC_PCIT_PCIT_SUBCH1_MSK                           (0x000FFC00)
  #define MC0_CH0_CR_SC_PCIT_PCIT_SUBCH1_MIN                           (0)
  #define MC0_CH0_CR_SC_PCIT_PCIT_SUBCH1_MAX                           (1023) // 0x000003FF
  #define MC0_CH0_CR_SC_PCIT_PCIT_SUBCH1_DEF                           (0x00000040)
  #define MC0_CH0_CR_SC_PCIT_PCIT_SUBCH1_HSH                           (0x0A14E034)

  #define MC0_CH0_CR_SC_PCIT_PCIT_GT_OFF                               (20)
  #define MC0_CH0_CR_SC_PCIT_PCIT_GT_WID                               (10)
  #define MC0_CH0_CR_SC_PCIT_PCIT_GT_MSK                               (0x3FF00000)
  #define MC0_CH0_CR_SC_PCIT_PCIT_GT_MIN                               (0)
  #define MC0_CH0_CR_SC_PCIT_PCIT_GT_MAX                               (1023) // 0x000003FF
  #define MC0_CH0_CR_SC_PCIT_PCIT_GT_DEF                               (0x00000040)
  #define MC0_CH0_CR_SC_PCIT_PCIT_GT_HSH                               (0x0A28E034)

#define MC0_CH0_CR_ECC_DFT_REG                                         (0x0000E038)

  #define MC0_CH0_CR_ECC_DFT_ECC_Inject_OFF                            ( 0)
  #define MC0_CH0_CR_ECC_DFT_ECC_Inject_WID                            ( 3)
  #define MC0_CH0_CR_ECC_DFT_ECC_Inject_MSK                            (0x00000007)
  #define MC0_CH0_CR_ECC_DFT_ECC_Inject_MIN                            (0)
  #define MC0_CH0_CR_ECC_DFT_ECC_Inject_MAX                            (7) // 0x00000007
  #define MC0_CH0_CR_ECC_DFT_ECC_Inject_DEF                            (0x00000000)
  #define MC0_CH0_CR_ECC_DFT_ECC_Inject_HSH                            (0x0300E038)

  #define MC0_CH0_CR_ECC_DFT_ECC_correction_disable_OFF                ( 4)
  #define MC0_CH0_CR_ECC_DFT_ECC_correction_disable_WID                ( 1)
  #define MC0_CH0_CR_ECC_DFT_ECC_correction_disable_MSK                (0x00000010)
  #define MC0_CH0_CR_ECC_DFT_ECC_correction_disable_MIN                (0)
  #define MC0_CH0_CR_ECC_DFT_ECC_correction_disable_MAX                (1) // 0x00000001
  #define MC0_CH0_CR_ECC_DFT_ECC_correction_disable_DEF                (0x00000000)
  #define MC0_CH0_CR_ECC_DFT_ECC_correction_disable_HSH                (0x0108E038)

  #define MC0_CH0_CR_ECC_DFT_DIS_MCA_LOG_OFF                           ( 8)
  #define MC0_CH0_CR_ECC_DFT_DIS_MCA_LOG_WID                           ( 1)
  #define MC0_CH0_CR_ECC_DFT_DIS_MCA_LOG_MSK                           (0x00000100)
  #define MC0_CH0_CR_ECC_DFT_DIS_MCA_LOG_MIN                           (0)
  #define MC0_CH0_CR_ECC_DFT_DIS_MCA_LOG_MAX                           (1) // 0x00000001
  #define MC0_CH0_CR_ECC_DFT_DIS_MCA_LOG_DEF                           (0x00000000)
  #define MC0_CH0_CR_ECC_DFT_DIS_MCA_LOG_HSH                           (0x0110E038)

  #define MC0_CH0_CR_ECC_DFT_DIS_PCIE_ERR_OFF                          ( 9)
  #define MC0_CH0_CR_ECC_DFT_DIS_PCIE_ERR_WID                          ( 1)
  #define MC0_CH0_CR_ECC_DFT_DIS_PCIE_ERR_MSK                          (0x00000200)
  #define MC0_CH0_CR_ECC_DFT_DIS_PCIE_ERR_MIN                          (0)
  #define MC0_CH0_CR_ECC_DFT_DIS_PCIE_ERR_MAX                          (1) // 0x00000001
  #define MC0_CH0_CR_ECC_DFT_DIS_PCIE_ERR_DEF                          (0x00000000)
  #define MC0_CH0_CR_ECC_DFT_DIS_PCIE_ERR_HSH                          (0x0112E038)

  #define MC0_CH0_CR_ECC_DFT_DIS_RCH_POISON_OFF                        (10)
  #define MC0_CH0_CR_ECC_DFT_DIS_RCH_POISON_WID                        ( 1)
  #define MC0_CH0_CR_ECC_DFT_DIS_RCH_POISON_MSK                        (0x00000400)
  #define MC0_CH0_CR_ECC_DFT_DIS_RCH_POISON_MIN                        (0)
  #define MC0_CH0_CR_ECC_DFT_DIS_RCH_POISON_MAX                        (1) // 0x00000001
  #define MC0_CH0_CR_ECC_DFT_DIS_RCH_POISON_DEF                        (0x00000001)
  #define MC0_CH0_CR_ECC_DFT_DIS_RCH_POISON_HSH                        (0x0114E038)

  #define MC0_CH0_CR_ECC_DFT_DIS_RCH_ERROR_OFF                         (11)
  #define MC0_CH0_CR_ECC_DFT_DIS_RCH_ERROR_WID                         ( 1)
  #define MC0_CH0_CR_ECC_DFT_DIS_RCH_ERROR_MSK                         (0x00000800)
  #define MC0_CH0_CR_ECC_DFT_DIS_RCH_ERROR_MIN                         (0)
  #define MC0_CH0_CR_ECC_DFT_DIS_RCH_ERROR_MAX                         (1) // 0x00000001
  #define MC0_CH0_CR_ECC_DFT_DIS_RCH_ERROR_DEF                         (0x00000001)
  #define MC0_CH0_CR_ECC_DFT_DIS_RCH_ERROR_HSH                         (0x0116E038)

  #define MC0_CH0_CR_ECC_DFT_ECC_trigger_OFF                           (16)
  #define MC0_CH0_CR_ECC_DFT_ECC_trigger_WID                           ( 2)
  #define MC0_CH0_CR_ECC_DFT_ECC_trigger_MSK                           (0x00030000)
  #define MC0_CH0_CR_ECC_DFT_ECC_trigger_MIN                           (0)
  #define MC0_CH0_CR_ECC_DFT_ECC_trigger_MAX                           (3) // 0x00000003
  #define MC0_CH0_CR_ECC_DFT_ECC_trigger_DEF                           (0x00000000)
  #define MC0_CH0_CR_ECC_DFT_ECC_trigger_HSH                           (0x0220E038)

#define MC0_CH0_CR_PM_PDWN_CONFIG_REG                                  (0x0000E040)

  #define MC0_CH0_CR_PM_PDWN_CONFIG_PDWN_idle_counter_subch0_OFF       ( 0)
  #define MC0_CH0_CR_PM_PDWN_CONFIG_PDWN_idle_counter_subch0_WID       (12)
  #define MC0_CH0_CR_PM_PDWN_CONFIG_PDWN_idle_counter_subch0_MSK       (0x00000FFF)
  #define MC0_CH0_CR_PM_PDWN_CONFIG_PDWN_idle_counter_subch0_MIN       (0)
  #define MC0_CH0_CR_PM_PDWN_CONFIG_PDWN_idle_counter_subch0_MAX       (4095) // 0x00000FFF
  #define MC0_CH0_CR_PM_PDWN_CONFIG_PDWN_idle_counter_subch0_DEF       (0x00000000)
  #define MC0_CH0_CR_PM_PDWN_CONFIG_PDWN_idle_counter_subch0_HSH       (0x4C00E040)

  #define MC0_CH0_CR_PM_PDWN_CONFIG_PDWN_idle_counter_subch1_OFF       (12)
  #define MC0_CH0_CR_PM_PDWN_CONFIG_PDWN_idle_counter_subch1_WID       (12)
  #define MC0_CH0_CR_PM_PDWN_CONFIG_PDWN_idle_counter_subch1_MSK       (0x00FFF000)
  #define MC0_CH0_CR_PM_PDWN_CONFIG_PDWN_idle_counter_subch1_MIN       (0)
  #define MC0_CH0_CR_PM_PDWN_CONFIG_PDWN_idle_counter_subch1_MAX       (4095) // 0x00000FFF
  #define MC0_CH0_CR_PM_PDWN_CONFIG_PDWN_idle_counter_subch1_DEF       (0x00000000)
  #define MC0_CH0_CR_PM_PDWN_CONFIG_PDWN_idle_counter_subch1_HSH       (0x4C18E040)

  #define MC0_CH0_CR_PM_PDWN_CONFIG_APD_OFF                            (24)
  #define MC0_CH0_CR_PM_PDWN_CONFIG_APD_WID                            ( 1)
  #define MC0_CH0_CR_PM_PDWN_CONFIG_APD_MSK                            (0x01000000)
  #define MC0_CH0_CR_PM_PDWN_CONFIG_APD_MIN                            (0)
  #define MC0_CH0_CR_PM_PDWN_CONFIG_APD_MAX                            (1) // 0x00000001
  #define MC0_CH0_CR_PM_PDWN_CONFIG_APD_DEF                            (0x00000000)
  #define MC0_CH0_CR_PM_PDWN_CONFIG_APD_HSH                            (0x4130E040)

  #define MC0_CH0_CR_PM_PDWN_CONFIG_PPD_OFF                            (25)
  #define MC0_CH0_CR_PM_PDWN_CONFIG_PPD_WID                            ( 1)
  #define MC0_CH0_CR_PM_PDWN_CONFIG_PPD_MSK                            (0x02000000)
  #define MC0_CH0_CR_PM_PDWN_CONFIG_PPD_MIN                            (0)
  #define MC0_CH0_CR_PM_PDWN_CONFIG_PPD_MAX                            (1) // 0x00000001
  #define MC0_CH0_CR_PM_PDWN_CONFIG_PPD_DEF                            (0x00000000)
  #define MC0_CH0_CR_PM_PDWN_CONFIG_PPD_HSH                            (0x4132E040)

  #define MC0_CH0_CR_PM_PDWN_CONFIG_write_threshold_OFF                (26)
  #define MC0_CH0_CR_PM_PDWN_CONFIG_write_threshold_WID                ( 6)
  #define MC0_CH0_CR_PM_PDWN_CONFIG_write_threshold_MSK                (0xFC000000)
  #define MC0_CH0_CR_PM_PDWN_CONFIG_write_threshold_MIN                (0)
  #define MC0_CH0_CR_PM_PDWN_CONFIG_write_threshold_MAX                (63) // 0x0000003F
  #define MC0_CH0_CR_PM_PDWN_CONFIG_write_threshold_DEF                (0x00000000)
  #define MC0_CH0_CR_PM_PDWN_CONFIG_write_threshold_HSH                (0x4634E040)

  #define MC0_CH0_CR_PM_PDWN_CONFIG_TT_idle_counter_OFF                (32)
  #define MC0_CH0_CR_PM_PDWN_CONFIG_TT_idle_counter_WID                ( 8)
  #define MC0_CH0_CR_PM_PDWN_CONFIG_TT_idle_counter_MSK                (0x000000FF00000000ULL)
  #define MC0_CH0_CR_PM_PDWN_CONFIG_TT_idle_counter_MIN                (0)
  #define MC0_CH0_CR_PM_PDWN_CONFIG_TT_idle_counter_MAX                (255) // 0x000000FF
  #define MC0_CH0_CR_PM_PDWN_CONFIG_TT_idle_counter_DEF                (0x00000000)
  #define MC0_CH0_CR_PM_PDWN_CONFIG_TT_idle_counter_HSH                (0x4840E040)

  #define MC0_CH0_CR_PM_PDWN_CONFIG_Global_PD_OFF                      (40)
  #define MC0_CH0_CR_PM_PDWN_CONFIG_Global_PD_WID                      ( 1)
  #define MC0_CH0_CR_PM_PDWN_CONFIG_Global_PD_MSK                      (0x0000010000000000ULL)
  #define MC0_CH0_CR_PM_PDWN_CONFIG_Global_PD_MIN                      (0)
  #define MC0_CH0_CR_PM_PDWN_CONFIG_Global_PD_MAX                      (1) // 0x00000001
  #define MC0_CH0_CR_PM_PDWN_CONFIG_Global_PD_DEF                      (0x00000000)
  #define MC0_CH0_CR_PM_PDWN_CONFIG_Global_PD_HSH                      (0x4150E040)

  #define MC0_CH0_CR_PM_PDWN_CONFIG_dis_cke_tt_OFF                     (41)
  #define MC0_CH0_CR_PM_PDWN_CONFIG_dis_cke_tt_WID                     ( 1)
  #define MC0_CH0_CR_PM_PDWN_CONFIG_dis_cke_tt_MSK                     (0x0000020000000000ULL)
  #define MC0_CH0_CR_PM_PDWN_CONFIG_dis_cke_tt_MIN                     (0)
  #define MC0_CH0_CR_PM_PDWN_CONFIG_dis_cke_tt_MAX                     (1) // 0x00000001
  #define MC0_CH0_CR_PM_PDWN_CONFIG_dis_cke_tt_DEF                     (0x00000000)
  #define MC0_CH0_CR_PM_PDWN_CONFIG_dis_cke_tt_HSH                     (0x4152E040)

  #define MC0_CH0_CR_PM_PDWN_CONFIG_allow_opp_ref_below_write_threhold_OFF (42)
  #define MC0_CH0_CR_PM_PDWN_CONFIG_allow_opp_ref_below_write_threhold_WID ( 1)
  #define MC0_CH0_CR_PM_PDWN_CONFIG_allow_opp_ref_below_write_threhold_MSK (0x0000040000000000ULL)
  #define MC0_CH0_CR_PM_PDWN_CONFIG_allow_opp_ref_below_write_threhold_MIN (0)
  #define MC0_CH0_CR_PM_PDWN_CONFIG_allow_opp_ref_below_write_threhold_MAX (1) // 0x00000001
  #define MC0_CH0_CR_PM_PDWN_CONFIG_allow_opp_ref_below_write_threhold_DEF (0x00000000)
  #define MC0_CH0_CR_PM_PDWN_CONFIG_allow_opp_ref_below_write_threhold_HSH (0x4154E040)

#define MC0_CH0_CR_ECCERRLOG0_REG                                      (0x0000E048)

  #define MC0_CH0_CR_ECCERRLOG0_CERRSTS_OFF                            ( 0)
  #define MC0_CH0_CR_ECCERRLOG0_CERRSTS_WID                            ( 1)
  #define MC0_CH0_CR_ECCERRLOG0_CERRSTS_MSK                            (0x00000001)
  #define MC0_CH0_CR_ECCERRLOG0_CERRSTS_MIN                            (0)
  #define MC0_CH0_CR_ECCERRLOG0_CERRSTS_MAX                            (1) // 0x00000001
  #define MC0_CH0_CR_ECCERRLOG0_CERRSTS_DEF                            (0x00000000)
  #define MC0_CH0_CR_ECCERRLOG0_CERRSTS_HSH                            (0x0100E048)

  #define MC0_CH0_CR_ECCERRLOG0_CERR_OVERFLOW_OFF                      ( 1)
  #define MC0_CH0_CR_ECCERRLOG0_CERR_OVERFLOW_WID                      ( 1)
  #define MC0_CH0_CR_ECCERRLOG0_CERR_OVERFLOW_MSK                      (0x00000002)
  #define MC0_CH0_CR_ECCERRLOG0_CERR_OVERFLOW_MIN                      (0)
  #define MC0_CH0_CR_ECCERRLOG0_CERR_OVERFLOW_MAX                      (1) // 0x00000001
  #define MC0_CH0_CR_ECCERRLOG0_CERR_OVERFLOW_DEF                      (0x00000000)
  #define MC0_CH0_CR_ECCERRLOG0_CERR_OVERFLOW_HSH                      (0x0102E048)

  #define MC0_CH0_CR_ECCERRLOG0_MERRSTS_OFF                            ( 2)
  #define MC0_CH0_CR_ECCERRLOG0_MERRSTS_WID                            ( 1)
  #define MC0_CH0_CR_ECCERRLOG0_MERRSTS_MSK                            (0x00000004)
  #define MC0_CH0_CR_ECCERRLOG0_MERRSTS_MIN                            (0)
  #define MC0_CH0_CR_ECCERRLOG0_MERRSTS_MAX                            (1) // 0x00000001
  #define MC0_CH0_CR_ECCERRLOG0_MERRSTS_DEF                            (0x00000000)
  #define MC0_CH0_CR_ECCERRLOG0_MERRSTS_HSH                            (0x0104E048)

  #define MC0_CH0_CR_ECCERRLOG0_MERR_OVERFLOW_OFF                      ( 3)
  #define MC0_CH0_CR_ECCERRLOG0_MERR_OVERFLOW_WID                      ( 1)
  #define MC0_CH0_CR_ECCERRLOG0_MERR_OVERFLOW_MSK                      (0x00000008)
  #define MC0_CH0_CR_ECCERRLOG0_MERR_OVERFLOW_MIN                      (0)
  #define MC0_CH0_CR_ECCERRLOG0_MERR_OVERFLOW_MAX                      (1) // 0x00000001
  #define MC0_CH0_CR_ECCERRLOG0_MERR_OVERFLOW_DEF                      (0x00000000)
  #define MC0_CH0_CR_ECCERRLOG0_MERR_OVERFLOW_HSH                      (0x0106E048)

  #define MC0_CH0_CR_ECCERRLOG0_ERRSYND_OFF                            (16)
  #define MC0_CH0_CR_ECCERRLOG0_ERRSYND_WID                            ( 8)
  #define MC0_CH0_CR_ECCERRLOG0_ERRSYND_MSK                            (0x00FF0000)
  #define MC0_CH0_CR_ECCERRLOG0_ERRSYND_MIN                            (0)
  #define MC0_CH0_CR_ECCERRLOG0_ERRSYND_MAX                            (255) // 0x000000FF
  #define MC0_CH0_CR_ECCERRLOG0_ERRSYND_DEF                            (0x00000000)
  #define MC0_CH0_CR_ECCERRLOG0_ERRSYND_HSH                            (0x0820E048)

  #define MC0_CH0_CR_ECCERRLOG0_ERRCHUNK_OFF                           (24)
  #define MC0_CH0_CR_ECCERRLOG0_ERRCHUNK_WID                           ( 3)
  #define MC0_CH0_CR_ECCERRLOG0_ERRCHUNK_MSK                           (0x07000000)
  #define MC0_CH0_CR_ECCERRLOG0_ERRCHUNK_MIN                           (0)
  #define MC0_CH0_CR_ECCERRLOG0_ERRCHUNK_MAX                           (7) // 0x00000007
  #define MC0_CH0_CR_ECCERRLOG0_ERRCHUNK_DEF                           (0x00000000)
  #define MC0_CH0_CR_ECCERRLOG0_ERRCHUNK_HSH                           (0x0330E048)

  #define MC0_CH0_CR_ECCERRLOG0_ERRRANK_OFF                            (27)
  #define MC0_CH0_CR_ECCERRLOG0_ERRRANK_WID                            ( 2)
  #define MC0_CH0_CR_ECCERRLOG0_ERRRANK_MSK                            (0x18000000)
  #define MC0_CH0_CR_ECCERRLOG0_ERRRANK_MIN                            (0)
  #define MC0_CH0_CR_ECCERRLOG0_ERRRANK_MAX                            (3) // 0x00000003
  #define MC0_CH0_CR_ECCERRLOG0_ERRRANK_DEF                            (0x00000000)
  #define MC0_CH0_CR_ECCERRLOG0_ERRRANK_HSH                            (0x0236E048)

  #define MC0_CH0_CR_ECCERRLOG0_ERRBANK_OFF                            (29)
  #define MC0_CH0_CR_ECCERRLOG0_ERRBANK_WID                            ( 3)
  #define MC0_CH0_CR_ECCERRLOG0_ERRBANK_MSK                            (0xE0000000)
  #define MC0_CH0_CR_ECCERRLOG0_ERRBANK_MIN                            (0)
  #define MC0_CH0_CR_ECCERRLOG0_ERRBANK_MAX                            (7) // 0x00000007
  #define MC0_CH0_CR_ECCERRLOG0_ERRBANK_DEF                            (0x00000000)
  #define MC0_CH0_CR_ECCERRLOG0_ERRBANK_HSH                            (0x033AE048)

#define MC0_CH0_CR_ECCERRLOG1_REG                                      (0x0000E04C)

  #define MC0_CH0_CR_ECCERRLOG1_ERRROW_OFF                             ( 0)
  #define MC0_CH0_CR_ECCERRLOG1_ERRROW_WID                             (18)
  #define MC0_CH0_CR_ECCERRLOG1_ERRROW_MSK                             (0x0003FFFF)
  #define MC0_CH0_CR_ECCERRLOG1_ERRROW_MIN                             (0)
  #define MC0_CH0_CR_ECCERRLOG1_ERRROW_MAX                             (262143) // 0x0003FFFF
  #define MC0_CH0_CR_ECCERRLOG1_ERRROW_DEF                             (0x00000000)
  #define MC0_CH0_CR_ECCERRLOG1_ERRROW_HSH                             (0x1200E04C)

  #define MC0_CH0_CR_ECCERRLOG1_ERRCOL_OFF                             (18)
  #define MC0_CH0_CR_ECCERRLOG1_ERRCOL_WID                             (11)
  #define MC0_CH0_CR_ECCERRLOG1_ERRCOL_MSK                             (0x1FFC0000)
  #define MC0_CH0_CR_ECCERRLOG1_ERRCOL_MIN                             (0)
  #define MC0_CH0_CR_ECCERRLOG1_ERRCOL_MAX                             (2047) // 0x000007FF
  #define MC0_CH0_CR_ECCERRLOG1_ERRCOL_DEF                             (0x00000000)
  #define MC0_CH0_CR_ECCERRLOG1_ERRCOL_HSH                             (0x0B24E04C)

  #define MC0_CH0_CR_ECCERRLOG1_ERRBANKGROUP_OFF                       (29)
  #define MC0_CH0_CR_ECCERRLOG1_ERRBANKGROUP_WID                       ( 3)
  #define MC0_CH0_CR_ECCERRLOG1_ERRBANKGROUP_MSK                       (0xE0000000)
  #define MC0_CH0_CR_ECCERRLOG1_ERRBANKGROUP_MIN                       (0)
  #define MC0_CH0_CR_ECCERRLOG1_ERRBANKGROUP_MAX                       (7) // 0x00000007
  #define MC0_CH0_CR_ECCERRLOG1_ERRBANKGROUP_DEF                       (0x00000000)
  #define MC0_CH0_CR_ECCERRLOG1_ERRBANKGROUP_HSH                       (0x033AE04C)

#define MC0_CH0_CR_TC_PWRDN_REG                                        (0x0000E050)

  #define MC0_CH0_CR_TC_PWRDN_tCKE_OFF                                 ( 0)
  #define MC0_CH0_CR_TC_PWRDN_tCKE_WID                                 ( 7)
  #define MC0_CH0_CR_TC_PWRDN_tCKE_MSK                                 (0x0000007F)
  #define MC0_CH0_CR_TC_PWRDN_tCKE_MIN                                 (0)
  #define MC0_CH0_CR_TC_PWRDN_tCKE_MAX                                 (127) // 0x0000007F
  #define MC0_CH0_CR_TC_PWRDN_tCKE_DEF                                 (0x00000004)
  #define MC0_CH0_CR_TC_PWRDN_tCKE_HSH                                 (0x4700E050)

  #define MC0_CH0_CR_TC_PWRDN_tXP_OFF                                  ( 7)
  #define MC0_CH0_CR_TC_PWRDN_tXP_WID                                  ( 7)
  #define MC0_CH0_CR_TC_PWRDN_tXP_MSK                                  (0x00003F80)
  #define MC0_CH0_CR_TC_PWRDN_tXP_MIN                                  (0)
  #define MC0_CH0_CR_TC_PWRDN_tXP_MAX                                  (127) // 0x0000007F
  #define MC0_CH0_CR_TC_PWRDN_tXP_DEF                                  (0x00000004)
  #define MC0_CH0_CR_TC_PWRDN_tXP_HSH                                  (0x470EE050)

  #define MC0_CH0_CR_TC_PWRDN_tXPDLL_OFF                               (14)
  #define MC0_CH0_CR_TC_PWRDN_tXPDLL_WID                               ( 7)
  #define MC0_CH0_CR_TC_PWRDN_tXPDLL_MSK                               (0x001FC000)
  #define MC0_CH0_CR_TC_PWRDN_tXPDLL_MIN                               (0)
  #define MC0_CH0_CR_TC_PWRDN_tXPDLL_MAX                               (127) // 0x0000007F
  #define MC0_CH0_CR_TC_PWRDN_tXPDLL_DEF                               (0x00000004)
  #define MC0_CH0_CR_TC_PWRDN_tXPDLL_HSH                               (0x471CE050)

  #define MC0_CH0_CR_TC_PWRDN_tRDPDEN_OFF                              (21)
  #define MC0_CH0_CR_TC_PWRDN_tRDPDEN_WID                              ( 8)
  #define MC0_CH0_CR_TC_PWRDN_tRDPDEN_MSK                              (0x1FE00000)
  #define MC0_CH0_CR_TC_PWRDN_tRDPDEN_MIN                              (0)
  #define MC0_CH0_CR_TC_PWRDN_tRDPDEN_MAX                              (255) // 0x000000FF
  #define MC0_CH0_CR_TC_PWRDN_tRDPDEN_DEF                              (0x00000004)
  #define MC0_CH0_CR_TC_PWRDN_tRDPDEN_HSH                              (0x482AE050)

  #define MC0_CH0_CR_TC_PWRDN_tWRPDEN_OFF                              (32)
  #define MC0_CH0_CR_TC_PWRDN_tWRPDEN_WID                              (10)
  #define MC0_CH0_CR_TC_PWRDN_tWRPDEN_MSK                              (0x000003FF00000000ULL)
  #define MC0_CH0_CR_TC_PWRDN_tWRPDEN_MIN                              (0)
  #define MC0_CH0_CR_TC_PWRDN_tWRPDEN_MAX                              (1023) // 0x000003FF
  #define MC0_CH0_CR_TC_PWRDN_tWRPDEN_DEF                              (0x00000004)
  #define MC0_CH0_CR_TC_PWRDN_tWRPDEN_HSH                              (0x4A40E050)

  #define MC0_CH0_CR_TC_PWRDN_tCSH_OFF                                 (42)
  #define MC0_CH0_CR_TC_PWRDN_tCSH_WID                                 ( 6)
  #define MC0_CH0_CR_TC_PWRDN_tCSH_MSK                                 (0x0000FC0000000000ULL)
  #define MC0_CH0_CR_TC_PWRDN_tCSH_MIN                                 (0)
  #define MC0_CH0_CR_TC_PWRDN_tCSH_MAX                                 (63) // 0x0000003F
  #define MC0_CH0_CR_TC_PWRDN_tCSH_DEF                                 (0x00000004)
  #define MC0_CH0_CR_TC_PWRDN_tCSH_HSH                                 (0x4654E050)

  #define MC0_CH0_CR_TC_PWRDN_tCSL_OFF                                 (48)
  #define MC0_CH0_CR_TC_PWRDN_tCSL_WID                                 ( 6)
  #define MC0_CH0_CR_TC_PWRDN_tCSL_MSK                                 (0x003F000000000000ULL)
  #define MC0_CH0_CR_TC_PWRDN_tCSL_MIN                                 (0)
  #define MC0_CH0_CR_TC_PWRDN_tCSL_MAX                                 (63) // 0x0000003F
  #define MC0_CH0_CR_TC_PWRDN_tCSL_DEF                                 (0x00000004)
  #define MC0_CH0_CR_TC_PWRDN_tCSL_HSH                                 (0x4660E050)

  #define MC0_CH0_CR_TC_PWRDN_tCA2CS_OFF                               (54)
  #define MC0_CH0_CR_TC_PWRDN_tCA2CS_WID                               ( 5)
  #define MC0_CH0_CR_TC_PWRDN_tCA2CS_MSK                               (0x07C0000000000000ULL)
  #define MC0_CH0_CR_TC_PWRDN_tCA2CS_MIN                               (0)
  #define MC0_CH0_CR_TC_PWRDN_tCA2CS_MAX                               (31) // 0x0000001F
  #define MC0_CH0_CR_TC_PWRDN_tCA2CS_DEF                               (0x00000004)
  #define MC0_CH0_CR_TC_PWRDN_tCA2CS_HSH                               (0x456CE050)

  #define MC0_CH0_CR_TC_PWRDN_tPRPDEN_OFF                              (59)
  #define MC0_CH0_CR_TC_PWRDN_tPRPDEN_WID                              ( 5)
  #define MC0_CH0_CR_TC_PWRDN_tPRPDEN_MSK                              (0xF800000000000000ULL)
  #define MC0_CH0_CR_TC_PWRDN_tPRPDEN_MIN                              (0)
  #define MC0_CH0_CR_TC_PWRDN_tPRPDEN_MAX                              (31) // 0x0000001F
  #define MC0_CH0_CR_TC_PWRDN_tPRPDEN_DEF                              (0x00000001)
  #define MC0_CH0_CR_TC_PWRDN_tPRPDEN_HSH                              (0x4576E050)

#define MC0_CH0_CR_QUEUE_ENTRY_DISABLE_RPQ_REG                         (0x0000E058)

  #define MC0_CH0_CR_QUEUE_ENTRY_DISABLE_RPQ_RPQ_disable_OFF           ( 0)
  #define MC0_CH0_CR_QUEUE_ENTRY_DISABLE_RPQ_RPQ_disable_WID           (32)

  #define MC0_CH0_CR_QUEUE_ENTRY_DISABLE_RPQ_RPQ_disable_MSK           (0xFFFFFFFF)

  #define MC0_CH0_CR_QUEUE_ENTRY_DISABLE_RPQ_RPQ_disable_MIN           (0)
  #define MC0_CH0_CR_QUEUE_ENTRY_DISABLE_RPQ_RPQ_disable_MAX           (4294967295) // 0xFFFFFFFF

  #define MC0_CH0_CR_QUEUE_ENTRY_DISABLE_RPQ_RPQ_disable_DEF           (0x00000000)
  #define MC0_CH0_CR_QUEUE_ENTRY_DISABLE_RPQ_RPQ_disable_HSH           (0x2000E058)

#define MC0_CH0_CR_QUEUE_ENTRY_DISABLE_IPQ_REG                         (0x0000E05C)

  #define MC0_CH0_CR_QUEUE_ENTRY_DISABLE_IPQ_IPQ_disable_OFF           ( 0)
  #define MC0_CH0_CR_QUEUE_ENTRY_DISABLE_IPQ_IPQ_disable_WID           (16)
  #define MC0_CH0_CR_QUEUE_ENTRY_DISABLE_IPQ_IPQ_disable_MSK           (0x0000FFFF)
  #define MC0_CH0_CR_QUEUE_ENTRY_DISABLE_IPQ_IPQ_disable_MIN           (0)
  #define MC0_CH0_CR_QUEUE_ENTRY_DISABLE_IPQ_IPQ_disable_MAX           (65535) // 0x0000FFFF
  #define MC0_CH0_CR_QUEUE_ENTRY_DISABLE_IPQ_IPQ_disable_DEF           (0x00000000)
  #define MC0_CH0_CR_QUEUE_ENTRY_DISABLE_IPQ_IPQ_disable_HSH           (0x1000E05C)

#define MC0_CH0_CR_QUEUE_ENTRY_DISABLE_WPQ_REG                         (0x0000E060)

  #define MC0_CH0_CR_QUEUE_ENTRY_DISABLE_WPQ_WPQ_disable_OFF           ( 0)
  #define MC0_CH0_CR_QUEUE_ENTRY_DISABLE_WPQ_WPQ_disable_WID           (64)
  #define MC0_CH0_CR_QUEUE_ENTRY_DISABLE_WPQ_WPQ_disable_MSK           (0xFFFFFFFFFFFFFFFFULL)
  #define MC0_CH0_CR_QUEUE_ENTRY_DISABLE_WPQ_WPQ_disable_MIN           (0)
  #define MC0_CH0_CR_QUEUE_ENTRY_DISABLE_WPQ_WPQ_disable_MAX           (18446744073709551615ULL) // 0xFFFFFFFFFFFFFFFF
  #define MC0_CH0_CR_QUEUE_ENTRY_DISABLE_WPQ_WPQ_disable_DEF           (0x00000000)
  #define MC0_CH0_CR_QUEUE_ENTRY_DISABLE_WPQ_WPQ_disable_HSH           (0x4000E060)

#define MC0_CH0_CR_SC_WDBWM_REG                                        (0x0000E068)

  #define MC0_CH0_CR_SC_WDBWM_WMM_entry_wm_OFF                         ( 0)
  #define MC0_CH0_CR_SC_WDBWM_WMM_entry_wm_WID                         ( 7)
  #define MC0_CH0_CR_SC_WDBWM_WMM_entry_wm_MSK                         (0x0000007F)
  #define MC0_CH0_CR_SC_WDBWM_WMM_entry_wm_MIN                         (0)
  #define MC0_CH0_CR_SC_WDBWM_WMM_entry_wm_MAX                         (127) // 0x0000007F
  #define MC0_CH0_CR_SC_WDBWM_WMM_entry_wm_DEF                         (0x00000038)
  #define MC0_CH0_CR_SC_WDBWM_WMM_entry_wm_HSH                         (0x4700E068)

  #define MC0_CH0_CR_SC_WDBWM_WMM_exit_wm_OFF                          ( 8)
  #define MC0_CH0_CR_SC_WDBWM_WMM_exit_wm_WID                          ( 7)
  #define MC0_CH0_CR_SC_WDBWM_WMM_exit_wm_MSK                          (0x00007F00)
  #define MC0_CH0_CR_SC_WDBWM_WMM_exit_wm_MIN                          (0)
  #define MC0_CH0_CR_SC_WDBWM_WMM_exit_wm_MAX                          (127) // 0x0000007F
  #define MC0_CH0_CR_SC_WDBWM_WMM_exit_wm_DEF                          (0x00000030)
  #define MC0_CH0_CR_SC_WDBWM_WMM_exit_wm_HSH                          (0x4710E068)

  #define MC0_CH0_CR_SC_WDBWM_WIM_wm_OFF                               (16)
  #define MC0_CH0_CR_SC_WDBWM_WIM_wm_WID                               ( 7)
  #define MC0_CH0_CR_SC_WDBWM_WIM_wm_MSK                               (0x007F0000)
  #define MC0_CH0_CR_SC_WDBWM_WIM_wm_MIN                               (0)
  #define MC0_CH0_CR_SC_WDBWM_WIM_wm_MAX                               (127) // 0x0000007F
  #define MC0_CH0_CR_SC_WDBWM_WIM_wm_DEF                               (0x0000003C)
  #define MC0_CH0_CR_SC_WDBWM_WIM_wm_HSH                               (0x4720E068)

  #define MC0_CH0_CR_SC_WDBWM_Write_Isoc_CAS_count_OFF                 (24)
  #define MC0_CH0_CR_SC_WDBWM_Write_Isoc_CAS_count_WID                 ( 8)
  #define MC0_CH0_CR_SC_WDBWM_Write_Isoc_CAS_count_MSK                 (0xFF000000)
  #define MC0_CH0_CR_SC_WDBWM_Write_Isoc_CAS_count_MIN                 (0)
  #define MC0_CH0_CR_SC_WDBWM_Write_Isoc_CAS_count_MAX                 (255) // 0x000000FF
  #define MC0_CH0_CR_SC_WDBWM_Write_Isoc_CAS_count_DEF                 (0x00000040)
  #define MC0_CH0_CR_SC_WDBWM_Write_Isoc_CAS_count_HSH                 (0x4830E068)

  #define MC0_CH0_CR_SC_WDBWM_Read_CAS_count_OFF                       (32)
  #define MC0_CH0_CR_SC_WDBWM_Read_CAS_count_WID                       ( 8)
  #define MC0_CH0_CR_SC_WDBWM_Read_CAS_count_MSK                       (0x000000FF00000000ULL)
  #define MC0_CH0_CR_SC_WDBWM_Read_CAS_count_MIN                       (0)
  #define MC0_CH0_CR_SC_WDBWM_Read_CAS_count_MAX                       (255) // 0x000000FF
  #define MC0_CH0_CR_SC_WDBWM_Read_CAS_count_DEF                       (0x00000040)
  #define MC0_CH0_CR_SC_WDBWM_Read_CAS_count_HSH                       (0x4840E068)

  #define MC0_CH0_CR_SC_WDBWM_Write_CAS_count_OFF                      (40)
  #define MC0_CH0_CR_SC_WDBWM_Write_CAS_count_WID                      ( 8)
  #define MC0_CH0_CR_SC_WDBWM_Write_CAS_count_MSK                      (0x0000FF0000000000ULL)
  #define MC0_CH0_CR_SC_WDBWM_Write_CAS_count_MIN                      (0)
  #define MC0_CH0_CR_SC_WDBWM_Write_CAS_count_MAX                      (255) // 0x000000FF
  #define MC0_CH0_CR_SC_WDBWM_Write_CAS_count_DEF                      (0x00000040)
  #define MC0_CH0_CR_SC_WDBWM_Write_CAS_count_HSH                      (0x4850E068)

  #define MC0_CH0_CR_SC_WDBWM_Write_CAS_count_for_VC1_OFF              (48)
  #define MC0_CH0_CR_SC_WDBWM_Write_CAS_count_for_VC1_WID              ( 8)
  #define MC0_CH0_CR_SC_WDBWM_Write_CAS_count_for_VC1_MSK              (0x00FF000000000000ULL)
  #define MC0_CH0_CR_SC_WDBWM_Write_CAS_count_for_VC1_MIN              (0)
  #define MC0_CH0_CR_SC_WDBWM_Write_CAS_count_for_VC1_MAX              (255) // 0x000000FF
  #define MC0_CH0_CR_SC_WDBWM_Write_CAS_count_for_VC1_DEF              (0x00000040)
  #define MC0_CH0_CR_SC_WDBWM_Write_CAS_count_for_VC1_HSH              (0x4860E068)

  #define MC0_CH0_CR_SC_WDBWM_Write_threshold_for_lp_read_bklr_OFF     (56)
  #define MC0_CH0_CR_SC_WDBWM_Write_threshold_for_lp_read_bklr_WID     ( 7)
  #define MC0_CH0_CR_SC_WDBWM_Write_threshold_for_lp_read_bklr_MSK     (0x7F00000000000000ULL)
  #define MC0_CH0_CR_SC_WDBWM_Write_threshold_for_lp_read_bklr_MIN     (0)
  #define MC0_CH0_CR_SC_WDBWM_Write_threshold_for_lp_read_bklr_MAX     (127) // 0x0000007F
  #define MC0_CH0_CR_SC_WDBWM_Write_threshold_for_lp_read_bklr_DEF     (0x00000016)
  #define MC0_CH0_CR_SC_WDBWM_Write_threshold_for_lp_read_bklr_HSH     (0x4770E068)

  #define MC0_CH0_CR_SC_WDBWM_En_Wr_in_WIM_during_TT_OFF               (63)
  #define MC0_CH0_CR_SC_WDBWM_En_Wr_in_WIM_during_TT_WID               ( 1)
  #define MC0_CH0_CR_SC_WDBWM_En_Wr_in_WIM_during_TT_MSK               (0x8000000000000000ULL)
  #define MC0_CH0_CR_SC_WDBWM_En_Wr_in_WIM_during_TT_MIN               (0)
  #define MC0_CH0_CR_SC_WDBWM_En_Wr_in_WIM_during_TT_MAX               (1) // 0x00000001
  #define MC0_CH0_CR_SC_WDBWM_En_Wr_in_WIM_during_TT_DEF               (0x00000000)
  #define MC0_CH0_CR_SC_WDBWM_En_Wr_in_WIM_during_TT_HSH               (0x417EE068)

#define MC0_CH0_CR_TC_ODT_REG                                          (0x0000E070)

  #define MC0_CH0_CR_TC_ODT_ODT_read_duration_OFF                      ( 0)
  #define MC0_CH0_CR_TC_ODT_ODT_read_duration_WID                      ( 4)
  #define MC0_CH0_CR_TC_ODT_ODT_read_duration_MSK                      (0x0000000F)
  #define MC0_CH0_CR_TC_ODT_ODT_read_duration_MIN                      (0)
  #define MC0_CH0_CR_TC_ODT_ODT_read_duration_MAX                      (15) // 0x0000000F
  #define MC0_CH0_CR_TC_ODT_ODT_read_duration_DEF                      (0x00000000)
  #define MC0_CH0_CR_TC_ODT_ODT_read_duration_HSH                      (0x4400E070)

  #define MC0_CH0_CR_TC_ODT_ODT_Read_Delay_OFF                         ( 4)
  #define MC0_CH0_CR_TC_ODT_ODT_Read_Delay_WID                         ( 4)
  #define MC0_CH0_CR_TC_ODT_ODT_Read_Delay_MSK                         (0x000000F0)
  #define MC0_CH0_CR_TC_ODT_ODT_Read_Delay_MIN                         (0)
  #define MC0_CH0_CR_TC_ODT_ODT_Read_Delay_MAX                         (15) // 0x0000000F
  #define MC0_CH0_CR_TC_ODT_ODT_Read_Delay_DEF                         (0x00000000)
  #define MC0_CH0_CR_TC_ODT_ODT_Read_Delay_HSH                         (0x4408E070)

  #define MC0_CH0_CR_TC_ODT_ODT_write_duration_OFF                     ( 8)
  #define MC0_CH0_CR_TC_ODT_ODT_write_duration_WID                     ( 4)
  #define MC0_CH0_CR_TC_ODT_ODT_write_duration_MSK                     (0x00000F00)
  #define MC0_CH0_CR_TC_ODT_ODT_write_duration_MIN                     (0)
  #define MC0_CH0_CR_TC_ODT_ODT_write_duration_MAX                     (15) // 0x0000000F
  #define MC0_CH0_CR_TC_ODT_ODT_write_duration_DEF                     (0x00000000)
  #define MC0_CH0_CR_TC_ODT_ODT_write_duration_HSH                     (0x4410E070)

  #define MC0_CH0_CR_TC_ODT_ODT_Write_Delay_OFF                        (12)
  #define MC0_CH0_CR_TC_ODT_ODT_Write_Delay_WID                        ( 4)
  #define MC0_CH0_CR_TC_ODT_ODT_Write_Delay_MSK                        (0x0000F000)
  #define MC0_CH0_CR_TC_ODT_ODT_Write_Delay_MIN                        (0)
  #define MC0_CH0_CR_TC_ODT_ODT_Write_Delay_MAX                        (15) // 0x0000000F
  #define MC0_CH0_CR_TC_ODT_ODT_Write_Delay_DEF                        (0x00000000)
  #define MC0_CH0_CR_TC_ODT_ODT_Write_Delay_HSH                        (0x4418E070)

  #define MC0_CH0_CR_TC_ODT_tCL_OFF                                    (16)
  #define MC0_CH0_CR_TC_ODT_tCL_WID                                    ( 7)
  #define MC0_CH0_CR_TC_ODT_tCL_MSK                                    (0x007F0000)
  #define MC0_CH0_CR_TC_ODT_tCL_MIN                                    (0)
  #define MC0_CH0_CR_TC_ODT_tCL_MAX                                    (127) // 0x0000007F
  #define MC0_CH0_CR_TC_ODT_tCL_DEF                                    (0x00000005)
  #define MC0_CH0_CR_TC_ODT_tCL_HSH                                    (0x4720E070)

  #define MC0_CH0_CR_TC_ODT_tCWL_OFF                                   (24)
  #define MC0_CH0_CR_TC_ODT_tCWL_WID                                   ( 8)
  #define MC0_CH0_CR_TC_ODT_tCWL_MSK                                   (0xFF000000)
  #define MC0_CH0_CR_TC_ODT_tCWL_MIN                                   (0)
  #define MC0_CH0_CR_TC_ODT_tCWL_MAX                                   (255) // 0x000000FF
  #define MC0_CH0_CR_TC_ODT_tCWL_DEF                                   (0x00000006)
  #define MC0_CH0_CR_TC_ODT_tCWL_HSH                                   (0x4830E070)

  #define MC0_CH0_CR_TC_ODT_tAONPD_OFF                                 (32)
  #define MC0_CH0_CR_TC_ODT_tAONPD_WID                                 ( 6)
  #define MC0_CH0_CR_TC_ODT_tAONPD_MSK                                 (0x0000003F00000000ULL)
  #define MC0_CH0_CR_TC_ODT_tAONPD_MIN                                 (0)
  #define MC0_CH0_CR_TC_ODT_tAONPD_MAX                                 (63) // 0x0000003F
  #define MC0_CH0_CR_TC_ODT_tAONPD_DEF                                 (0x00000004)
  #define MC0_CH0_CR_TC_ODT_tAONPD_HSH                                 (0x4640E070)

  #define MC0_CH0_CR_TC_ODT_Write_Early_ODT_OFF                        (38)
  #define MC0_CH0_CR_TC_ODT_Write_Early_ODT_WID                        ( 1)
  #define MC0_CH0_CR_TC_ODT_Write_Early_ODT_MSK                        (0x0000004000000000ULL)
  #define MC0_CH0_CR_TC_ODT_Write_Early_ODT_MIN                        (0)
  #define MC0_CH0_CR_TC_ODT_Write_Early_ODT_MAX                        (1) // 0x00000001
  #define MC0_CH0_CR_TC_ODT_Write_Early_ODT_DEF                        (0x00000000)
  #define MC0_CH0_CR_TC_ODT_Write_Early_ODT_HSH                        (0x414CE070)

  #define MC0_CH0_CR_TC_ODT_PtrSep_OFF                                 (39)
  #define MC0_CH0_CR_TC_ODT_PtrSep_WID                                 ( 2)
  #define MC0_CH0_CR_TC_ODT_PtrSep_MSK                                 (0x0000018000000000ULL)
  #define MC0_CH0_CR_TC_ODT_PtrSep_MIN                                 (0)
  #define MC0_CH0_CR_TC_ODT_PtrSep_MAX                                 (3) // 0x00000003
  #define MC0_CH0_CR_TC_ODT_PtrSep_DEF                                 (0x00000001)
  #define MC0_CH0_CR_TC_ODT_PtrSep_HSH                                 (0x424EE070)

#define MC0_CH0_CR_MCSCHEDS_SPARE_REG                                  (0x0000E078)

  #define MC0_CH0_CR_MCSCHEDS_SPARE_Spare_RW_OFF                       ( 0)
  #define MC0_CH0_CR_MCSCHEDS_SPARE_Spare_RW_WID                       (15)
  #define MC0_CH0_CR_MCSCHEDS_SPARE_Spare_RW_MSK                       (0x00007FFF)
  #define MC0_CH0_CR_MCSCHEDS_SPARE_Spare_RW_MIN                       (0)
  #define MC0_CH0_CR_MCSCHEDS_SPARE_Spare_RW_MAX                       (32767) // 0x00007FFF
  #define MC0_CH0_CR_MCSCHEDS_SPARE_Spare_RW_DEF                       (0x00000000)
  #define MC0_CH0_CR_MCSCHEDS_SPARE_Spare_RW_HSH                       (0x0F00E078)
  #define MC0_CH0_CR_MCSCHEDS_SPARE_EnableB2BPreemption_OFF            (15)
  #define MC0_CH0_CR_MCSCHEDS_SPARE_EnableB2BPreemption_WID            ( 1)
  #define MC0_CH0_CR_MCSCHEDS_SPARE_EnableB2BPreemption_MSK            (0x00008000)
  #define MC0_CH0_CR_MCSCHEDS_SPARE_EnableB2BPreemption_MIN            (0)
  #define MC0_CH0_CR_MCSCHEDS_SPARE_EnableB2BPreemption_MAX            (1) // 0x00000001
  #define MC0_CH0_CR_MCSCHEDS_SPARE_EnableB2BPreemption_DEF            (0x00000000)
  #define MC0_CH0_CR_MCSCHEDS_SPARE_EnableB2BPreemption_HSH            (0x011EE078)

  #define MC0_CH0_CR_MCSCHEDS_SPARE_WMM_exit_wm_for_FBRMM_OFF          (16)
  #define MC0_CH0_CR_MCSCHEDS_SPARE_WMM_exit_wm_for_FBRMM_WID          ( 7)
  #define MC0_CH0_CR_MCSCHEDS_SPARE_WMM_exit_wm_for_FBRMM_MSK          (0x007F0000)
  #define MC0_CH0_CR_MCSCHEDS_SPARE_WMM_exit_wm_for_FBRMM_MIN          (0)
  #define MC0_CH0_CR_MCSCHEDS_SPARE_WMM_exit_wm_for_FBRMM_MAX          (127) // 0x0000007F
  #define MC0_CH0_CR_MCSCHEDS_SPARE_WMM_exit_wm_for_FBRMM_DEF          (0x00000030)
  #define MC0_CH0_CR_MCSCHEDS_SPARE_WMM_exit_wm_for_FBRMM_HSH          (0x0720E078)

  #define MC0_CH0_CR_MCSCHEDS_SPARE_Spare_RW_V_OFF                     (27)
  #define MC0_CH0_CR_MCSCHEDS_SPARE_Spare_RW_V_WID                     ( 5)
  #define MC0_CH0_CR_MCSCHEDS_SPARE_Spare_RW_V_MSK                     (0xF8000000)
  #define MC0_CH0_CR_MCSCHEDS_SPARE_Spare_RW_V_MIN                     (0)
  #define MC0_CH0_CR_MCSCHEDS_SPARE_Spare_RW_V_MAX                     (31) // 0x0000001F
  #define MC0_CH0_CR_MCSCHEDS_SPARE_Spare_RW_V_DEF                     (0x00000000)
  #define MC0_CH0_CR_MCSCHEDS_SPARE_Spare_RW_V_HSH                     (0x0536E078)

#define MC0_CH0_CR_TC_MPC_REG                                          (0x0000E07C)

  #define MC0_CH0_CR_TC_MPC_MPC_Setup_OFF                              ( 0)
  #define MC0_CH0_CR_TC_MPC_MPC_Setup_WID                              ( 5)
  #define MC0_CH0_CR_TC_MPC_MPC_Setup_MSK                              (0x0000001F)
  #define MC0_CH0_CR_TC_MPC_MPC_Setup_MIN                              (0)
  #define MC0_CH0_CR_TC_MPC_MPC_Setup_MAX                              (31) // 0x0000001F
  #define MC0_CH0_CR_TC_MPC_MPC_Setup_DEF                              (0x00000000)
  #define MC0_CH0_CR_TC_MPC_MPC_Setup_HSH                              (0x0500E07C)

  #define MC0_CH0_CR_TC_MPC_MPC_Hold_OFF                               ( 5)
  #define MC0_CH0_CR_TC_MPC_MPC_Hold_WID                               ( 5)
  #define MC0_CH0_CR_TC_MPC_MPC_Hold_MSK                               (0x000003E0)
  #define MC0_CH0_CR_TC_MPC_MPC_Hold_MIN                               (0)
  #define MC0_CH0_CR_TC_MPC_MPC_Hold_MAX                               (31) // 0x0000001F
  #define MC0_CH0_CR_TC_MPC_MPC_Hold_DEF                               (0x00000000)
  #define MC0_CH0_CR_TC_MPC_MPC_Hold_HSH                               (0x050AE07C)

  #define MC0_CH0_CR_TC_MPC_MultiCycCS_OFF                             (10)
  #define MC0_CH0_CR_TC_MPC_MultiCycCS_WID                             ( 5)
  #define MC0_CH0_CR_TC_MPC_MultiCycCS_MSK                             (0x00007C00)
  #define MC0_CH0_CR_TC_MPC_MultiCycCS_MIN                             (0)
  #define MC0_CH0_CR_TC_MPC_MultiCycCS_MAX                             (31) // 0x0000001F
  #define MC0_CH0_CR_TC_MPC_MultiCycCS_DEF                             (0x00000000)
  #define MC0_CH0_CR_TC_MPC_MultiCycCS_HSH                             (0x0514E07C)

#define MC0_CH0_CR_SC_ODT_MATRIX_REG                                   (0x0000E080)

  #define MC0_CH0_CR_SC_ODT_MATRIX_Read_Rank_0_OFF                     ( 0)
  #define MC0_CH0_CR_SC_ODT_MATRIX_Read_Rank_0_WID                     ( 4)
  #define MC0_CH0_CR_SC_ODT_MATRIX_Read_Rank_0_MSK                     (0x0000000F)
  #define MC0_CH0_CR_SC_ODT_MATRIX_Read_Rank_0_MIN                     (0)
  #define MC0_CH0_CR_SC_ODT_MATRIX_Read_Rank_0_MAX                     (15) // 0x0000000F
  #define MC0_CH0_CR_SC_ODT_MATRIX_Read_Rank_0_DEF                     (0x00000000)
  #define MC0_CH0_CR_SC_ODT_MATRIX_Read_Rank_0_HSH                     (0x0400E080)

  #define MC0_CH0_CR_SC_ODT_MATRIX_Read_Rank_1_OFF                     ( 4)
  #define MC0_CH0_CR_SC_ODT_MATRIX_Read_Rank_1_WID                     ( 4)
  #define MC0_CH0_CR_SC_ODT_MATRIX_Read_Rank_1_MSK                     (0x000000F0)
  #define MC0_CH0_CR_SC_ODT_MATRIX_Read_Rank_1_MIN                     (0)
  #define MC0_CH0_CR_SC_ODT_MATRIX_Read_Rank_1_MAX                     (15) // 0x0000000F
  #define MC0_CH0_CR_SC_ODT_MATRIX_Read_Rank_1_DEF                     (0x00000000)
  #define MC0_CH0_CR_SC_ODT_MATRIX_Read_Rank_1_HSH                     (0x0408E080)

  #define MC0_CH0_CR_SC_ODT_MATRIX_Read_Rank_2_OFF                     ( 8)
  #define MC0_CH0_CR_SC_ODT_MATRIX_Read_Rank_2_WID                     ( 4)
  #define MC0_CH0_CR_SC_ODT_MATRIX_Read_Rank_2_MSK                     (0x00000F00)
  #define MC0_CH0_CR_SC_ODT_MATRIX_Read_Rank_2_MIN                     (0)
  #define MC0_CH0_CR_SC_ODT_MATRIX_Read_Rank_2_MAX                     (15) // 0x0000000F
  #define MC0_CH0_CR_SC_ODT_MATRIX_Read_Rank_2_DEF                     (0x00000000)
  #define MC0_CH0_CR_SC_ODT_MATRIX_Read_Rank_2_HSH                     (0x0410E080)

  #define MC0_CH0_CR_SC_ODT_MATRIX_Read_Rank_3_OFF                     (12)
  #define MC0_CH0_CR_SC_ODT_MATRIX_Read_Rank_3_WID                     ( 4)
  #define MC0_CH0_CR_SC_ODT_MATRIX_Read_Rank_3_MSK                     (0x0000F000)
  #define MC0_CH0_CR_SC_ODT_MATRIX_Read_Rank_3_MIN                     (0)
  #define MC0_CH0_CR_SC_ODT_MATRIX_Read_Rank_3_MAX                     (15) // 0x0000000F
  #define MC0_CH0_CR_SC_ODT_MATRIX_Read_Rank_3_DEF                     (0x00000000)
  #define MC0_CH0_CR_SC_ODT_MATRIX_Read_Rank_3_HSH                     (0x0418E080)

  #define MC0_CH0_CR_SC_ODT_MATRIX_Write_Rank_0_OFF                    (16)
  #define MC0_CH0_CR_SC_ODT_MATRIX_Write_Rank_0_WID                    ( 4)
  #define MC0_CH0_CR_SC_ODT_MATRIX_Write_Rank_0_MSK                    (0x000F0000)
  #define MC0_CH0_CR_SC_ODT_MATRIX_Write_Rank_0_MIN                    (0)
  #define MC0_CH0_CR_SC_ODT_MATRIX_Write_Rank_0_MAX                    (15) // 0x0000000F
  #define MC0_CH0_CR_SC_ODT_MATRIX_Write_Rank_0_DEF                    (0x00000000)
  #define MC0_CH0_CR_SC_ODT_MATRIX_Write_Rank_0_HSH                    (0x0420E080)

  #define MC0_CH0_CR_SC_ODT_MATRIX_Write_Rank_1_OFF                    (20)
  #define MC0_CH0_CR_SC_ODT_MATRIX_Write_Rank_1_WID                    ( 4)
  #define MC0_CH0_CR_SC_ODT_MATRIX_Write_Rank_1_MSK                    (0x00F00000)
  #define MC0_CH0_CR_SC_ODT_MATRIX_Write_Rank_1_MIN                    (0)
  #define MC0_CH0_CR_SC_ODT_MATRIX_Write_Rank_1_MAX                    (15) // 0x0000000F
  #define MC0_CH0_CR_SC_ODT_MATRIX_Write_Rank_1_DEF                    (0x00000000)
  #define MC0_CH0_CR_SC_ODT_MATRIX_Write_Rank_1_HSH                    (0x0428E080)

  #define MC0_CH0_CR_SC_ODT_MATRIX_Write_Rank_2_OFF                    (24)
  #define MC0_CH0_CR_SC_ODT_MATRIX_Write_Rank_2_WID                    ( 4)
  #define MC0_CH0_CR_SC_ODT_MATRIX_Write_Rank_2_MSK                    (0x0F000000)
  #define MC0_CH0_CR_SC_ODT_MATRIX_Write_Rank_2_MIN                    (0)
  #define MC0_CH0_CR_SC_ODT_MATRIX_Write_Rank_2_MAX                    (15) // 0x0000000F
  #define MC0_CH0_CR_SC_ODT_MATRIX_Write_Rank_2_DEF                    (0x00000000)
  #define MC0_CH0_CR_SC_ODT_MATRIX_Write_Rank_2_HSH                    (0x0430E080)

  #define MC0_CH0_CR_SC_ODT_MATRIX_Write_Rank_3_OFF                    (28)
  #define MC0_CH0_CR_SC_ODT_MATRIX_Write_Rank_3_WID                    ( 4)
  #define MC0_CH0_CR_SC_ODT_MATRIX_Write_Rank_3_MSK                    (0xF0000000)
  #define MC0_CH0_CR_SC_ODT_MATRIX_Write_Rank_3_MIN                    (0)
  #define MC0_CH0_CR_SC_ODT_MATRIX_Write_Rank_3_MAX                    (15) // 0x0000000F
  #define MC0_CH0_CR_SC_ODT_MATRIX_Write_Rank_3_DEF                    (0x00000000)
  #define MC0_CH0_CR_SC_ODT_MATRIX_Write_Rank_3_HSH                    (0x0438E080)

#define MC0_CH0_CR_DFT_BLOCK_REG                                       (0x0000E084)

  #define MC0_CH0_CR_DFT_BLOCK_dft_block_cycles_OFF                    ( 0)
  #define MC0_CH0_CR_DFT_BLOCK_dft_block_cycles_WID                    (16)
  #define MC0_CH0_CR_DFT_BLOCK_dft_block_cycles_MSK                    (0x0000FFFF)
  #define MC0_CH0_CR_DFT_BLOCK_dft_block_cycles_MIN                    (0)
  #define MC0_CH0_CR_DFT_BLOCK_dft_block_cycles_MAX                    (65535) // 0x0000FFFF
  #define MC0_CH0_CR_DFT_BLOCK_dft_block_cycles_DEF                    (0x00000000)
  #define MC0_CH0_CR_DFT_BLOCK_dft_block_cycles_HSH                    (0x1000E084)

  #define MC0_CH0_CR_DFT_BLOCK_dft_nonblock_cycles_OFF                 (16)
  #define MC0_CH0_CR_DFT_BLOCK_dft_nonblock_cycles_WID                 (16)
  #define MC0_CH0_CR_DFT_BLOCK_dft_nonblock_cycles_MSK                 (0xFFFF0000)
  #define MC0_CH0_CR_DFT_BLOCK_dft_nonblock_cycles_MIN                 (0)
  #define MC0_CH0_CR_DFT_BLOCK_dft_nonblock_cycles_MAX                 (65535) // 0x0000FFFF
  #define MC0_CH0_CR_DFT_BLOCK_dft_nonblock_cycles_DEF                 (0x00000000)
  #define MC0_CH0_CR_DFT_BLOCK_dft_nonblock_cycles_HSH                 (0x1020E084)

#define MC0_CH0_CR_SC_GS_CFG_REG                                       (0x0000E088)

  #define MC0_CH0_CR_SC_GS_CFG_DRAM_technology_OFF                     ( 0)
  #define MC0_CH0_CR_SC_GS_CFG_DRAM_technology_WID                     ( 3)
  #define MC0_CH0_CR_SC_GS_CFG_DRAM_technology_MSK                     (0x00000007)
  #define MC0_CH0_CR_SC_GS_CFG_DRAM_technology_MIN                     (0)
  #define MC0_CH0_CR_SC_GS_CFG_DRAM_technology_MAX                     (7) // 0x00000007
  #define MC0_CH0_CR_SC_GS_CFG_DRAM_technology_DEF                     (0x00000000)
  #define MC0_CH0_CR_SC_GS_CFG_DRAM_technology_HSH                     (0x4300E088)

  #define MC0_CH0_CR_SC_GS_CFG_CMD_stretch_OFF                         ( 3)
  #define MC0_CH0_CR_SC_GS_CFG_CMD_stretch_WID                         ( 2)
  #define MC0_CH0_CR_SC_GS_CFG_CMD_stretch_MSK                         (0x00000018)
  #define MC0_CH0_CR_SC_GS_CFG_CMD_stretch_MIN                         (0)
  #define MC0_CH0_CR_SC_GS_CFG_CMD_stretch_MAX                         (3) // 0x00000003
  #define MC0_CH0_CR_SC_GS_CFG_CMD_stretch_DEF                         (0x00000000)
  #define MC0_CH0_CR_SC_GS_CFG_CMD_stretch_HSH                         (0x4206E088)

  #define MC0_CH0_CR_SC_GS_CFG_N_to_1_ratio_OFF                        ( 5)
  #define MC0_CH0_CR_SC_GS_CFG_N_to_1_ratio_WID                        ( 3)
  #define MC0_CH0_CR_SC_GS_CFG_N_to_1_ratio_MSK                        (0x000000E0)
  #define MC0_CH0_CR_SC_GS_CFG_N_to_1_ratio_MIN                        (0)
  #define MC0_CH0_CR_SC_GS_CFG_N_to_1_ratio_MAX                        (7) // 0x00000007
  #define MC0_CH0_CR_SC_GS_CFG_N_to_1_ratio_DEF                        (0x00000001)
  #define MC0_CH0_CR_SC_GS_CFG_N_to_1_ratio_HSH                        (0x430AE088)

  #define MC0_CH0_CR_SC_GS_CFG_Address_mirror_OFF                      ( 8)
  #define MC0_CH0_CR_SC_GS_CFG_Address_mirror_WID                      ( 4)
  #define MC0_CH0_CR_SC_GS_CFG_Address_mirror_MSK                      (0x00000F00)
  #define MC0_CH0_CR_SC_GS_CFG_Address_mirror_MIN                      (0)
  #define MC0_CH0_CR_SC_GS_CFG_Address_mirror_MAX                      (15) // 0x0000000F
  #define MC0_CH0_CR_SC_GS_CFG_Address_mirror_DEF                      (0x00000000)
  #define MC0_CH0_CR_SC_GS_CFG_Address_mirror_HSH                      (0x4410E088)

  #define MC0_CH0_CR_SC_GS_CFG_disable_odt_tristate_A0_OFF                (13)
  #define MC0_CH0_CR_SC_GS_CFG_disable_odt_tristate_A0_WID                ( 1)
  #define MC0_CH0_CR_SC_GS_CFG_disable_odt_tristate_A0_MSK                (0x00002000)
  #define MC0_CH0_CR_SC_GS_CFG_disable_odt_tristate_A0_MIN                (0)
  #define MC0_CH0_CR_SC_GS_CFG_disable_odt_tristate_A0_MAX                (1) // 0x00000001
  #define MC0_CH0_CR_SC_GS_CFG_disable_odt_tristate_A0_DEF                (0x00000000)
  #define MC0_CH0_CR_SC_GS_CFG_disable_odt_tristate_A0_HSH                (0x411AE088)

  #define MC0_CH0_CR_SC_GS_CFG_disable_cs_tristate_A0_OFF                 (14)
  #define MC0_CH0_CR_SC_GS_CFG_disable_cs_tristate_A0_WID                 ( 1)
  #define MC0_CH0_CR_SC_GS_CFG_disable_cs_tristate_A0_MSK                 (0x00004000)
  #define MC0_CH0_CR_SC_GS_CFG_disable_cs_tristate_A0_MIN                 (0)
  #define MC0_CH0_CR_SC_GS_CFG_disable_cs_tristate_A0_MAX                 (1) // 0x00000001
  #define MC0_CH0_CR_SC_GS_CFG_disable_cs_tristate_A0_DEF                 (0x00000000)
  #define MC0_CH0_CR_SC_GS_CFG_disable_cs_tristate_A0_HSH                 (0x411CE088)

  #define MC0_CH0_CR_SC_GS_CFG_disable_tristate_A0_OFF                    (15)
  #define MC0_CH0_CR_SC_GS_CFG_disable_tristate_A0_WID                    ( 1)
  #define MC0_CH0_CR_SC_GS_CFG_disable_tristate_A0_MSK                    (0x00008000)
  #define MC0_CH0_CR_SC_GS_CFG_disable_tristate_A0_MIN                    (0)
  #define MC0_CH0_CR_SC_GS_CFG_disable_tristate_A0_MAX                    (1) // 0x00000001
  #define MC0_CH0_CR_SC_GS_CFG_disable_tristate_A0_DEF                    (0x00000000)
  #define MC0_CH0_CR_SC_GS_CFG_disable_tristate_A0_HSH                    (0x411EE088)

  #define MC0_CH0_CR_SC_GS_CFG_disable_odt_tristate_OFF                (12)
  #define MC0_CH0_CR_SC_GS_CFG_disable_odt_tristate_WID                ( 1)
  #define MC0_CH0_CR_SC_GS_CFG_disable_odt_tristate_MSK                (0x00001000)
  #define MC0_CH0_CR_SC_GS_CFG_disable_odt_tristate_MIN                (0)
  #define MC0_CH0_CR_SC_GS_CFG_disable_odt_tristate_MAX                (1) // 0x00000001
  #define MC0_CH0_CR_SC_GS_CFG_disable_odt_tristate_DEF                (0x00000000)
  #define MC0_CH0_CR_SC_GS_CFG_disable_odt_tristate_HSH                (0x4118E088)

  #define MC0_CH0_CR_SC_GS_CFG_disable_cs_tristate_OFF                 (13)
  #define MC0_CH0_CR_SC_GS_CFG_disable_cs_tristate_WID                 ( 1)
  #define MC0_CH0_CR_SC_GS_CFG_disable_cs_tristate_MSK                 (0x00002000)
  #define MC0_CH0_CR_SC_GS_CFG_disable_cs_tristate_MIN                 (0)
  #define MC0_CH0_CR_SC_GS_CFG_disable_cs_tristate_MAX                 (1) // 0x00000001
  #define MC0_CH0_CR_SC_GS_CFG_disable_cs_tristate_DEF                 (0x00000000)
  #define MC0_CH0_CR_SC_GS_CFG_disable_cs_tristate_HSH                 (0x411AE088)

  #define MC0_CH0_CR_SC_GS_CFG_disable_tristate_OFF                    (14)
  #define MC0_CH0_CR_SC_GS_CFG_disable_tristate_WID                    ( 1)
  #define MC0_CH0_CR_SC_GS_CFG_disable_tristate_MSK                    (0x00004000)
  #define MC0_CH0_CR_SC_GS_CFG_disable_tristate_MIN                    (0)
  #define MC0_CH0_CR_SC_GS_CFG_disable_tristate_MAX                    (1) // 0x00000001
  #define MC0_CH0_CR_SC_GS_CFG_disable_tristate_DEF                    (0x00000000)
  #define MC0_CH0_CR_SC_GS_CFG_disable_tristate_HSH                    (0x411CE088)

  #define MC0_CH0_CR_SC_GS_CFG_gear4_OFF                               (15)
  #define MC0_CH0_CR_SC_GS_CFG_gear4_WID                               ( 1)
  #define MC0_CH0_CR_SC_GS_CFG_gear4_MSK                               (0x00008000)
  #define MC0_CH0_CR_SC_GS_CFG_gear4_MIN                               (0)
  #define MC0_CH0_CR_SC_GS_CFG_gear4_MAX                               (1) // 0x00000001
  #define MC0_CH0_CR_SC_GS_CFG_gear4_DEF                               (0x00000000)
  #define MC0_CH0_CR_SC_GS_CFG_gear4_HSH                               (0x411EE088)

  #define MC0_CH0_CR_SC_GS_CFG_no_gear4_param_divide_OFF               (16)
  #define MC0_CH0_CR_SC_GS_CFG_no_gear4_param_divide_WID               ( 1)
  #define MC0_CH0_CR_SC_GS_CFG_no_gear4_param_divide_MSK               (0x00010000)
  #define MC0_CH0_CR_SC_GS_CFG_no_gear4_param_divide_MIN               (0)
  #define MC0_CH0_CR_SC_GS_CFG_no_gear4_param_divide_MAX               (1) // 0x00000001
  #define MC0_CH0_CR_SC_GS_CFG_no_gear4_param_divide_DEF               (0x00000000)
  #define MC0_CH0_CR_SC_GS_CFG_no_gear4_param_divide_HSH               (0x4120E088)

  #define MC0_CH0_CR_SC_GS_CFG_tCAL_OFF                                (19)
  #define MC0_CH0_CR_SC_GS_CFG_tCAL_WID                                ( 3)
  #define MC0_CH0_CR_SC_GS_CFG_tCAL_MSK                                (0x00380000)
  #define MC0_CH0_CR_SC_GS_CFG_tCAL_MIN                                (0)
  #define MC0_CH0_CR_SC_GS_CFG_tCAL_MAX                                (7) // 0x00000007
  #define MC0_CH0_CR_SC_GS_CFG_tCAL_DEF                                (0x00000000)
  #define MC0_CH0_CR_SC_GS_CFG_tCAL_HSH                                (0x4326E088)

  #define MC0_CH0_CR_SC_GS_CFG_frequency_point_OFF                     (22)
  #define MC0_CH0_CR_SC_GS_CFG_frequency_point_WID                     ( 2)
  #define MC0_CH0_CR_SC_GS_CFG_frequency_point_MSK                     (0x00C00000)
  #define MC0_CH0_CR_SC_GS_CFG_frequency_point_MIN                     (0)
  #define MC0_CH0_CR_SC_GS_CFG_frequency_point_MAX                     (3) // 0x00000003
  #define MC0_CH0_CR_SC_GS_CFG_frequency_point_DEF                     (0x00000000)
  #define MC0_CH0_CR_SC_GS_CFG_frequency_point_HSH                     (0x422CE088)

  #define MC0_CH0_CR_SC_GS_CFG_enable_odt_matrix_OFF                   (24)
  #define MC0_CH0_CR_SC_GS_CFG_enable_odt_matrix_WID                   ( 1)
  #define MC0_CH0_CR_SC_GS_CFG_enable_odt_matrix_MSK                   (0x01000000)
  #define MC0_CH0_CR_SC_GS_CFG_enable_odt_matrix_MIN                   (0)
  #define MC0_CH0_CR_SC_GS_CFG_enable_odt_matrix_MAX                   (1) // 0x00000001
  #define MC0_CH0_CR_SC_GS_CFG_enable_odt_matrix_DEF                   (0x00000000)
  #define MC0_CH0_CR_SC_GS_CFG_enable_odt_matrix_HSH                   (0x4130E088)

  #define MC0_CH0_CR_SC_GS_CFG_cs_to_cke_OFF                           (25)
  #define MC0_CH0_CR_SC_GS_CFG_cs_to_cke_WID                           ( 3)
  #define MC0_CH0_CR_SC_GS_CFG_cs_to_cke_MSK                           (0x0E000000)
  #define MC0_CH0_CR_SC_GS_CFG_cs_to_cke_MIN                           (0)
  #define MC0_CH0_CR_SC_GS_CFG_cs_to_cke_MAX                           (7) // 0x00000007
  #define MC0_CH0_CR_SC_GS_CFG_cs_to_cke_DEF                           (0x00000002)
  #define MC0_CH0_CR_SC_GS_CFG_cs_to_cke_HSH                           (0x4332E088)

  #define MC0_CH0_CR_SC_GS_CFG_x8_device_OFF                           (28)
  #define MC0_CH0_CR_SC_GS_CFG_x8_device_WID                           ( 2)
  #define MC0_CH0_CR_SC_GS_CFG_x8_device_MSK                           (0x30000000)
  #define MC0_CH0_CR_SC_GS_CFG_x8_device_MIN                           (0)
  #define MC0_CH0_CR_SC_GS_CFG_x8_device_MAX                           (3) // 0x00000003
  #define MC0_CH0_CR_SC_GS_CFG_x8_device_DEF                           (0x00000000)
  #define MC0_CH0_CR_SC_GS_CFG_x8_device_HSH                           (0x4238E088)

  #define MC0_CH0_CR_SC_GS_CFG_no_gear2_param_divide_OFF               (30)
  #define MC0_CH0_CR_SC_GS_CFG_no_gear2_param_divide_WID               ( 1)
  #define MC0_CH0_CR_SC_GS_CFG_no_gear2_param_divide_MSK               (0x40000000)
  #define MC0_CH0_CR_SC_GS_CFG_no_gear2_param_divide_MIN               (0)
  #define MC0_CH0_CR_SC_GS_CFG_no_gear2_param_divide_MAX               (1) // 0x00000001
  #define MC0_CH0_CR_SC_GS_CFG_no_gear2_param_divide_DEF               (0x00000000)
  #define MC0_CH0_CR_SC_GS_CFG_no_gear2_param_divide_HSH               (0x413CE088)

  #define MC0_CH0_CR_SC_GS_CFG_gear2_OFF                               (31)
  #define MC0_CH0_CR_SC_GS_CFG_gear2_WID                               ( 1)
  #define MC0_CH0_CR_SC_GS_CFG_gear2_MSK                               (0x80000000)
  #define MC0_CH0_CR_SC_GS_CFG_gear2_MIN                               (0)
  #define MC0_CH0_CR_SC_GS_CFG_gear2_MAX                               (1) // 0x00000001
  #define MC0_CH0_CR_SC_GS_CFG_gear2_DEF                               (0x00000000)
  #define MC0_CH0_CR_SC_GS_CFG_gear2_HSH                               (0x413EE088)

  #define MC0_CH0_CR_SC_GS_CFG_ddr_1dpc_split_ranks_on_subch_OFF       (32)
  #define MC0_CH0_CR_SC_GS_CFG_ddr_1dpc_split_ranks_on_subch_WID       ( 2)
  #define MC0_CH0_CR_SC_GS_CFG_ddr_1dpc_split_ranks_on_subch_MSK       (0x0000000300000000ULL)
  #define MC0_CH0_CR_SC_GS_CFG_ddr_1dpc_split_ranks_on_subch_MIN       (0)
  #define MC0_CH0_CR_SC_GS_CFG_ddr_1dpc_split_ranks_on_subch_MAX       (3) // 0x00000003
  #define MC0_CH0_CR_SC_GS_CFG_ddr_1dpc_split_ranks_on_subch_DEF       (0x00000000)
  #define MC0_CH0_CR_SC_GS_CFG_ddr_1dpc_split_ranks_on_subch_HSH       (0x4240E088)

  #define MC0_CH0_CR_SC_GS_CFG_Limit_MM_Transitions_OFF                (34)
  #define MC0_CH0_CR_SC_GS_CFG_Limit_MM_Transitions_WID                ( 1)
  #define MC0_CH0_CR_SC_GS_CFG_Limit_MM_Transitions_MSK                (0x0000000400000000ULL)
  #define MC0_CH0_CR_SC_GS_CFG_Limit_MM_Transitions_MIN                (0)
  #define MC0_CH0_CR_SC_GS_CFG_Limit_MM_Transitions_MAX                (1) // 0x00000001
  #define MC0_CH0_CR_SC_GS_CFG_Limit_MM_Transitions_DEF                (0x00000001)
  #define MC0_CH0_CR_SC_GS_CFG_Limit_MM_Transitions_HSH                (0x4144E088)

  #define MC0_CH0_CR_SC_GS_CFG_pl_trace_only_cs_cmd_OFF                (35)
  #define MC0_CH0_CR_SC_GS_CFG_pl_trace_only_cs_cmd_WID                ( 1)
  #define MC0_CH0_CR_SC_GS_CFG_pl_trace_only_cs_cmd_MSK                (0x0000000800000000ULL)
  #define MC0_CH0_CR_SC_GS_CFG_pl_trace_only_cs_cmd_MIN                (0)
  #define MC0_CH0_CR_SC_GS_CFG_pl_trace_only_cs_cmd_MAX                (1) // 0x00000001
  #define MC0_CH0_CR_SC_GS_CFG_pl_trace_only_cs_cmd_DEF                (0x00000000)
  #define MC0_CH0_CR_SC_GS_CFG_pl_trace_only_cs_cmd_HSH                (0x4146E088)

  #define MC0_CH0_CR_SC_GS_CFG_two_cycle_cke_idle_OFF                  (36)
  #define MC0_CH0_CR_SC_GS_CFG_two_cycle_cke_idle_WID                  ( 1)
  #define MC0_CH0_CR_SC_GS_CFG_two_cycle_cke_idle_MSK                  (0x0000001000000000ULL)
  #define MC0_CH0_CR_SC_GS_CFG_two_cycle_cke_idle_MIN                  (0)
  #define MC0_CH0_CR_SC_GS_CFG_two_cycle_cke_idle_MAX                  (1) // 0x00000001
  #define MC0_CH0_CR_SC_GS_CFG_two_cycle_cke_idle_DEF                  (0x00000000)
  #define MC0_CH0_CR_SC_GS_CFG_two_cycle_cke_idle_HSH                  (0x4148E088)

  #define MC0_CH0_CR_SC_GS_CFG_disable_ca_tristate_OFF                 (37)
  #define MC0_CH0_CR_SC_GS_CFG_disable_ca_tristate_WID                 ( 1)
  #define MC0_CH0_CR_SC_GS_CFG_disable_ca_tristate_MSK                 (0x0000002000000000ULL)
  #define MC0_CH0_CR_SC_GS_CFG_disable_ca_tristate_MIN                 (0)
  #define MC0_CH0_CR_SC_GS_CFG_disable_ca_tristate_MAX                 (1) // 0x00000001
  #define MC0_CH0_CR_SC_GS_CFG_disable_ca_tristate_DEF                 (0x00000000)
  #define MC0_CH0_CR_SC_GS_CFG_disable_ca_tristate_HSH                 (0x414AE088)

  #define MC0_CH0_CR_SC_GS_CFG_disable_ck_tristate_OFF                 (38)
  #define MC0_CH0_CR_SC_GS_CFG_disable_ck_tristate_WID                 ( 1)
  #define MC0_CH0_CR_SC_GS_CFG_disable_ck_tristate_MSK                 (0x0000004000000000ULL)
  #define MC0_CH0_CR_SC_GS_CFG_disable_ck_tristate_MIN                 (0)
  #define MC0_CH0_CR_SC_GS_CFG_disable_ck_tristate_MAX                 (1) // 0x00000001
  #define MC0_CH0_CR_SC_GS_CFG_disable_ck_tristate_DEF                 (0x00000000)
  #define MC0_CH0_CR_SC_GS_CFG_disable_ck_tristate_HSH                 (0x414CE088)

  #define MC0_CH0_CR_SC_GS_CFG_ck_to_cke_OFF                           (39)
  #define MC0_CH0_CR_SC_GS_CFG_ck_to_cke_WID                           ( 6)
  #define MC0_CH0_CR_SC_GS_CFG_ck_to_cke_MSK                           (0x00001F8000000000ULL)
  #define MC0_CH0_CR_SC_GS_CFG_ck_to_cke_MIN                           (0)
  #define MC0_CH0_CR_SC_GS_CFG_ck_to_cke_MAX                           (63) // 0x0000003F
  #define MC0_CH0_CR_SC_GS_CFG_ck_to_cke_DEF                           (0x00000002)
  #define MC0_CH0_CR_SC_GS_CFG_ck_to_cke_HSH                           (0x464EE088)

  #define MC0_CH0_CR_SC_GS_CFG_PDE_CA11_OFF                            (45)
  #define MC0_CH0_CR_SC_GS_CFG_PDE_CA11_WID                            ( 1)
  #define MC0_CH0_CR_SC_GS_CFG_PDE_CA11_MSK                            (0x0000200000000000ULL)
  #define MC0_CH0_CR_SC_GS_CFG_PDE_CA11_MIN                            (0)
  #define MC0_CH0_CR_SC_GS_CFG_PDE_CA11_MAX                            (1) // 0x00000001
  #define MC0_CH0_CR_SC_GS_CFG_PDE_CA11_DEF                            (0x00000000)
  #define MC0_CH0_CR_SC_GS_CFG_PDE_CA11_HSH                            (0x415AE088)

  #define MC0_CH0_CR_SC_GS_CFG_lp5_wck_tck_ratio_OFF                   (46)
  #define MC0_CH0_CR_SC_GS_CFG_lp5_wck_tck_ratio_WID                   ( 1)
  #define MC0_CH0_CR_SC_GS_CFG_lp5_wck_tck_ratio_MSK                   (0x0000400000000000ULL)
  #define MC0_CH0_CR_SC_GS_CFG_lp5_wck_tck_ratio_MIN                   (0)
  #define MC0_CH0_CR_SC_GS_CFG_lp5_wck_tck_ratio_MAX                   (1) // 0x00000001
  #define MC0_CH0_CR_SC_GS_CFG_lp5_wck_tck_ratio_DEF                   (0x00000000)
  #define MC0_CH0_CR_SC_GS_CFG_lp5_wck_tck_ratio_HSH                   (0x415CE088)

  #define MC0_CH0_CR_SC_GS_CFG_gear4_A0_OFF                               (47)
  #define MC0_CH0_CR_SC_GS_CFG_gear4_A0_WID                               ( 1)
  #define MC0_CH0_CR_SC_GS_CFG_gear4_A0_MSK                               (0x0000800000000000ULL)
  #define MC0_CH0_CR_SC_GS_CFG_gear4_A0_MIN                               (0)
  #define MC0_CH0_CR_SC_GS_CFG_gear4_A0_MAX                               (1) // 0x00000001
  #define MC0_CH0_CR_SC_GS_CFG_gear4_A0_DEF                               (0x00000000)
  #define MC0_CH0_CR_SC_GS_CFG_gear4_A0_HSH                               (0x415EE088)

  #define MC0_CH0_CR_SC_GS_CFG_no_gear4_param_divide_A0_OFF               (48)
  #define MC0_CH0_CR_SC_GS_CFG_no_gear4_param_divide_A0_WID               ( 1)
  #define MC0_CH0_CR_SC_GS_CFG_no_gear4_param_divide_A0_MSK               (0x0001000000000000ULL)
  #define MC0_CH0_CR_SC_GS_CFG_no_gear4_param_divide_A0_MIN               (0)
  #define MC0_CH0_CR_SC_GS_CFG_no_gear4_param_divide_A0_MAX               (1) // 0x00000001
  #define MC0_CH0_CR_SC_GS_CFG_no_gear4_param_divide_A0_DEF               (0x00000000)
  #define MC0_CH0_CR_SC_GS_CFG_no_gear4_param_divide_A0_HSH               (0x4160E088)

  #define MC0_CH0_CR_SC_GS_CFG_lpddr5_bg_mode_OFF                      (47)
  #define MC0_CH0_CR_SC_GS_CFG_lpddr5_bg_mode_WID                      ( 2)
  #define MC0_CH0_CR_SC_GS_CFG_lpddr5_bg_mode_MSK                      (0x0001800000000000ULL)
  #define MC0_CH0_CR_SC_GS_CFG_lpddr5_bg_mode_MIN                      (0)
  #define MC0_CH0_CR_SC_GS_CFG_lpddr5_bg_mode_MAX                      (3) // 0x00000003
  #define MC0_CH0_CR_SC_GS_CFG_lpddr5_bg_mode_DEF                      (0x00000001)
  #define MC0_CH0_CR_SC_GS_CFG_lpddr5_bg_mode_HSH                      (0x425EE088)

  #define MC0_CH0_CR_SC_GS_CFG_write0_enable_OFF                       (49)
  #define MC0_CH0_CR_SC_GS_CFG_write0_enable_WID                       ( 1)
  #define MC0_CH0_CR_SC_GS_CFG_write0_enable_MSK                       (0x0002000000000000ULL)
  #define MC0_CH0_CR_SC_GS_CFG_write0_enable_MIN                       (0)
  #define MC0_CH0_CR_SC_GS_CFG_write0_enable_MAX                       (1) // 0x00000001
  #define MC0_CH0_CR_SC_GS_CFG_write0_enable_DEF                       (0x00000000)
  #define MC0_CH0_CR_SC_GS_CFG_write0_enable_HSH                       (0x4162E088)

  #define MC0_CH0_CR_SC_GS_CFG_force_pre_after_cas_OFF                 (50)
  #define MC0_CH0_CR_SC_GS_CFG_force_pre_after_cas_WID                 ( 1)
  #define MC0_CH0_CR_SC_GS_CFG_force_pre_after_cas_MSK                 (0x0004000000000000ULL)
  #define MC0_CH0_CR_SC_GS_CFG_force_pre_after_cas_MIN                 (0)
  #define MC0_CH0_CR_SC_GS_CFG_force_pre_after_cas_MAX                 (1) // 0x00000001
  #define MC0_CH0_CR_SC_GS_CFG_force_pre_after_cas_DEF                 (0x00000000)
  #define MC0_CH0_CR_SC_GS_CFG_force_pre_after_cas_HSH                 (0x4164E088)

  #define MC0_CH0_CR_SC_GS_CFG_MultiCycCmd_OFF                         (51)
  #define MC0_CH0_CR_SC_GS_CFG_MultiCycCmd_WID                         ( 1)
  #define MC0_CH0_CR_SC_GS_CFG_MultiCycCmd_MSK                         (0x0008000000000000ULL)
  #define MC0_CH0_CR_SC_GS_CFG_MultiCycCmd_MIN                         (0)
  #define MC0_CH0_CR_SC_GS_CFG_MultiCycCmd_MAX                         (1) // 0x00000001
  #define MC0_CH0_CR_SC_GS_CFG_MultiCycCmd_DEF                         (0x00000000)
  #define MC0_CH0_CR_SC_GS_CFG_MultiCycCmd_HSH                         (0x4166E088)

  #define MC0_CH0_CR_SC_GS_CFG_row_msb_OFF                             (52)
  #define MC0_CH0_CR_SC_GS_CFG_row_msb_WID                             ( 2)
  #define MC0_CH0_CR_SC_GS_CFG_row_msb_MSK                             (0x0030000000000000ULL)
  #define MC0_CH0_CR_SC_GS_CFG_row_msb_MIN                             (0)
  #define MC0_CH0_CR_SC_GS_CFG_row_msb_MAX                             (3) // 0x00000003
  #define MC0_CH0_CR_SC_GS_CFG_row_msb_DEF                             (0x00000000)
  #define MC0_CH0_CR_SC_GS_CFG_row_msb_HSH                             (0x4268E088)

  #define MC0_CH0_CR_SC_GS_CFG_WCKDiffLowInIdle_OFF                    (54)
  #define MC0_CH0_CR_SC_GS_CFG_WCKDiffLowInIdle_WID                    ( 1)
  #define MC0_CH0_CR_SC_GS_CFG_WCKDiffLowInIdle_MSK                    (0x0040000000000000ULL)
  #define MC0_CH0_CR_SC_GS_CFG_WCKDiffLowInIdle_MIN                    (0)
  #define MC0_CH0_CR_SC_GS_CFG_WCKDiffLowInIdle_MAX                    (1) // 0x00000001
  #define MC0_CH0_CR_SC_GS_CFG_WCKDiffLowInIdle_DEF                    (0x00000000)
  #define MC0_CH0_CR_SC_GS_CFG_WCKDiffLowInIdle_HSH                    (0x416CE088)

  #define MC0_CH0_CR_SC_GS_CFG_tCPDED_OFF                              (56)
  #define MC0_CH0_CR_SC_GS_CFG_tCPDED_WID                              ( 5)
  #define MC0_CH0_CR_SC_GS_CFG_tCPDED_MSK                              (0x1F00000000000000ULL)
  #define MC0_CH0_CR_SC_GS_CFG_tCPDED_MIN                              (0)
  #define MC0_CH0_CR_SC_GS_CFG_tCPDED_MAX                              (31) // 0x0000001F
  #define MC0_CH0_CR_SC_GS_CFG_tCPDED_DEF                              (0x00000001)
  #define MC0_CH0_CR_SC_GS_CFG_tCPDED_HSH                              (0x4570E088)

#define MC0_CH0_CR_SC_PH_THROTTLING_0_REG                              (0x0000E090)

  #define MC0_CH0_CR_SC_PH_THROTTLING_0_loaded_same_rank_OFF           ( 0)
  #define MC0_CH0_CR_SC_PH_THROTTLING_0_loaded_same_rank_WID           ( 6)
  #define MC0_CH0_CR_SC_PH_THROTTLING_0_loaded_same_rank_MSK           (0x0000003F)
  #define MC0_CH0_CR_SC_PH_THROTTLING_0_loaded_same_rank_MIN           (0)
  #define MC0_CH0_CR_SC_PH_THROTTLING_0_loaded_same_rank_MAX           (63) // 0x0000003F
  #define MC0_CH0_CR_SC_PH_THROTTLING_0_loaded_same_rank_DEF           (0x00000008)
  #define MC0_CH0_CR_SC_PH_THROTTLING_0_loaded_same_rank_HSH           (0x0600E090)

  #define MC0_CH0_CR_SC_PH_THROTTLING_0_unloaded_same_rank_OFF         ( 8)
  #define MC0_CH0_CR_SC_PH_THROTTLING_0_unloaded_same_rank_WID         ( 6)
  #define MC0_CH0_CR_SC_PH_THROTTLING_0_unloaded_same_rank_MSK         (0x00003F00)
  #define MC0_CH0_CR_SC_PH_THROTTLING_0_unloaded_same_rank_MIN         (0)
  #define MC0_CH0_CR_SC_PH_THROTTLING_0_unloaded_same_rank_MAX         (63) // 0x0000003F
  #define MC0_CH0_CR_SC_PH_THROTTLING_0_unloaded_same_rank_DEF         (0x00000001)
  #define MC0_CH0_CR_SC_PH_THROTTLING_0_unloaded_same_rank_HSH         (0x0610E090)

  #define MC0_CH0_CR_SC_PH_THROTTLING_0_loaded_different_rank_OFF      (16)
  #define MC0_CH0_CR_SC_PH_THROTTLING_0_loaded_different_rank_WID      ( 6)
  #define MC0_CH0_CR_SC_PH_THROTTLING_0_loaded_different_rank_MSK      (0x003F0000)
  #define MC0_CH0_CR_SC_PH_THROTTLING_0_loaded_different_rank_MIN      (0)
  #define MC0_CH0_CR_SC_PH_THROTTLING_0_loaded_different_rank_MAX      (63) // 0x0000003F
  #define MC0_CH0_CR_SC_PH_THROTTLING_0_loaded_different_rank_DEF      (0x0000000C)
  #define MC0_CH0_CR_SC_PH_THROTTLING_0_loaded_different_rank_HSH      (0x0620E090)

  #define MC0_CH0_CR_SC_PH_THROTTLING_0_unloaded_different_rank_OFF    (24)
  #define MC0_CH0_CR_SC_PH_THROTTLING_0_unloaded_different_rank_WID    ( 6)
  #define MC0_CH0_CR_SC_PH_THROTTLING_0_unloaded_different_rank_MSK    (0x3F000000)
  #define MC0_CH0_CR_SC_PH_THROTTLING_0_unloaded_different_rank_MIN    (0)
  #define MC0_CH0_CR_SC_PH_THROTTLING_0_unloaded_different_rank_MAX    (63) // 0x0000003F
  #define MC0_CH0_CR_SC_PH_THROTTLING_0_unloaded_different_rank_DEF    (0x00000003)
  #define MC0_CH0_CR_SC_PH_THROTTLING_0_unloaded_different_rank_HSH    (0x0630E090)

#define MC0_CH0_CR_SC_PH_THROTTLING_1_REG                              (0x0000E094)

  #define MC0_CH0_CR_SC_PH_THROTTLING_1_loaded_same_rank_OFF           ( 0)
  #define MC0_CH0_CR_SC_PH_THROTTLING_1_loaded_same_rank_WID           ( 6)
  #define MC0_CH0_CR_SC_PH_THROTTLING_1_loaded_same_rank_MSK           (0x0000003F)
  #define MC0_CH0_CR_SC_PH_THROTTLING_1_loaded_same_rank_MIN           (0)
  #define MC0_CH0_CR_SC_PH_THROTTLING_1_loaded_same_rank_MAX           (63) // 0x0000003F
  #define MC0_CH0_CR_SC_PH_THROTTLING_1_loaded_same_rank_DEF           (0x00000004)
  #define MC0_CH0_CR_SC_PH_THROTTLING_1_loaded_same_rank_HSH           (0x0600E094)

  #define MC0_CH0_CR_SC_PH_THROTTLING_1_unloaded_same_rank_OFF         ( 8)
  #define MC0_CH0_CR_SC_PH_THROTTLING_1_unloaded_same_rank_WID         ( 6)
  #define MC0_CH0_CR_SC_PH_THROTTLING_1_unloaded_same_rank_MSK         (0x00003F00)
  #define MC0_CH0_CR_SC_PH_THROTTLING_1_unloaded_same_rank_MIN         (0)
  #define MC0_CH0_CR_SC_PH_THROTTLING_1_unloaded_same_rank_MAX         (63) // 0x0000003F
  #define MC0_CH0_CR_SC_PH_THROTTLING_1_unloaded_same_rank_DEF         (0x00000000)
  #define MC0_CH0_CR_SC_PH_THROTTLING_1_unloaded_same_rank_HSH         (0x0610E094)

  #define MC0_CH0_CR_SC_PH_THROTTLING_1_loaded_different_rank_OFF      (16)
  #define MC0_CH0_CR_SC_PH_THROTTLING_1_loaded_different_rank_WID      ( 6)
  #define MC0_CH0_CR_SC_PH_THROTTLING_1_loaded_different_rank_MSK      (0x003F0000)
  #define MC0_CH0_CR_SC_PH_THROTTLING_1_loaded_different_rank_MIN      (0)
  #define MC0_CH0_CR_SC_PH_THROTTLING_1_loaded_different_rank_MAX      (63) // 0x0000003F
  #define MC0_CH0_CR_SC_PH_THROTTLING_1_loaded_different_rank_DEF      (0x00000008)
  #define MC0_CH0_CR_SC_PH_THROTTLING_1_loaded_different_rank_HSH      (0x0620E094)

  #define MC0_CH0_CR_SC_PH_THROTTLING_1_unloaded_different_rank_OFF    (24)
  #define MC0_CH0_CR_SC_PH_THROTTLING_1_unloaded_different_rank_WID    ( 6)
  #define MC0_CH0_CR_SC_PH_THROTTLING_1_unloaded_different_rank_MSK    (0x3F000000)
  #define MC0_CH0_CR_SC_PH_THROTTLING_1_unloaded_different_rank_MIN    (0)
  #define MC0_CH0_CR_SC_PH_THROTTLING_1_unloaded_different_rank_MAX    (63) // 0x0000003F
  #define MC0_CH0_CR_SC_PH_THROTTLING_1_unloaded_different_rank_DEF    (0x00000002)
  #define MC0_CH0_CR_SC_PH_THROTTLING_1_unloaded_different_rank_HSH    (0x0630E094)

#define MC0_CH0_CR_SC_PH_THROTTLING_2_REG                              (0x0000E098)

  #define MC0_CH0_CR_SC_PH_THROTTLING_2_loaded_same_rank_OFF           ( 0)
  #define MC0_CH0_CR_SC_PH_THROTTLING_2_loaded_same_rank_WID           ( 6)
  #define MC0_CH0_CR_SC_PH_THROTTLING_2_loaded_same_rank_MSK           (0x0000003F)
  #define MC0_CH0_CR_SC_PH_THROTTLING_2_loaded_same_rank_MIN           (0)
  #define MC0_CH0_CR_SC_PH_THROTTLING_2_loaded_same_rank_MAX           (63) // 0x0000003F
  #define MC0_CH0_CR_SC_PH_THROTTLING_2_loaded_same_rank_DEF           (0x00000002)
  #define MC0_CH0_CR_SC_PH_THROTTLING_2_loaded_same_rank_HSH           (0x0600E098)

  #define MC0_CH0_CR_SC_PH_THROTTLING_2_unloaded_same_rank_OFF         ( 8)
  #define MC0_CH0_CR_SC_PH_THROTTLING_2_unloaded_same_rank_WID         ( 6)
  #define MC0_CH0_CR_SC_PH_THROTTLING_2_unloaded_same_rank_MSK         (0x00003F00)
  #define MC0_CH0_CR_SC_PH_THROTTLING_2_unloaded_same_rank_MIN         (0)
  #define MC0_CH0_CR_SC_PH_THROTTLING_2_unloaded_same_rank_MAX         (63) // 0x0000003F
  #define MC0_CH0_CR_SC_PH_THROTTLING_2_unloaded_same_rank_DEF         (0x00000000)
  #define MC0_CH0_CR_SC_PH_THROTTLING_2_unloaded_same_rank_HSH         (0x0610E098)

  #define MC0_CH0_CR_SC_PH_THROTTLING_2_loaded_different_rank_OFF      (16)
  #define MC0_CH0_CR_SC_PH_THROTTLING_2_loaded_different_rank_WID      ( 6)
  #define MC0_CH0_CR_SC_PH_THROTTLING_2_loaded_different_rank_MSK      (0x003F0000)
  #define MC0_CH0_CR_SC_PH_THROTTLING_2_loaded_different_rank_MIN      (0)
  #define MC0_CH0_CR_SC_PH_THROTTLING_2_loaded_different_rank_MAX      (63) // 0x0000003F
  #define MC0_CH0_CR_SC_PH_THROTTLING_2_loaded_different_rank_DEF      (0x00000006)
  #define MC0_CH0_CR_SC_PH_THROTTLING_2_loaded_different_rank_HSH      (0x0620E098)

  #define MC0_CH0_CR_SC_PH_THROTTLING_2_unloaded_different_rank_OFF    (24)
  #define MC0_CH0_CR_SC_PH_THROTTLING_2_unloaded_different_rank_WID    ( 6)
  #define MC0_CH0_CR_SC_PH_THROTTLING_2_unloaded_different_rank_MSK    (0x3F000000)
  #define MC0_CH0_CR_SC_PH_THROTTLING_2_unloaded_different_rank_MIN    (0)
  #define MC0_CH0_CR_SC_PH_THROTTLING_2_unloaded_different_rank_MAX    (63) // 0x0000003F
  #define MC0_CH0_CR_SC_PH_THROTTLING_2_unloaded_different_rank_DEF    (0x00000000)
  #define MC0_CH0_CR_SC_PH_THROTTLING_2_unloaded_different_rank_HSH    (0x0630E098)

#define MC0_CH0_CR_SC_PH_THROTTLING_3_REG                              (0x0000E09C)

  #define MC0_CH0_CR_SC_PH_THROTTLING_3_loaded_same_rank_OFF           ( 0)
  #define MC0_CH0_CR_SC_PH_THROTTLING_3_loaded_same_rank_WID           ( 6)
  #define MC0_CH0_CR_SC_PH_THROTTLING_3_loaded_same_rank_MSK           (0x0000003F)
  #define MC0_CH0_CR_SC_PH_THROTTLING_3_loaded_same_rank_MIN           (0)
  #define MC0_CH0_CR_SC_PH_THROTTLING_3_loaded_same_rank_MAX           (63) // 0x0000003F
  #define MC0_CH0_CR_SC_PH_THROTTLING_3_loaded_same_rank_DEF           (0x00000000)
  #define MC0_CH0_CR_SC_PH_THROTTLING_3_loaded_same_rank_HSH           (0x0600E09C)

  #define MC0_CH0_CR_SC_PH_THROTTLING_3_unloaded_same_rank_OFF         ( 8)
  #define MC0_CH0_CR_SC_PH_THROTTLING_3_unloaded_same_rank_WID         ( 6)
  #define MC0_CH0_CR_SC_PH_THROTTLING_3_unloaded_same_rank_MSK         (0x00003F00)
  #define MC0_CH0_CR_SC_PH_THROTTLING_3_unloaded_same_rank_MIN         (0)
  #define MC0_CH0_CR_SC_PH_THROTTLING_3_unloaded_same_rank_MAX         (63) // 0x0000003F
  #define MC0_CH0_CR_SC_PH_THROTTLING_3_unloaded_same_rank_DEF         (0x00000000)
  #define MC0_CH0_CR_SC_PH_THROTTLING_3_unloaded_same_rank_HSH         (0x0610E09C)

  #define MC0_CH0_CR_SC_PH_THROTTLING_3_loaded_different_rank_OFF      (16)
  #define MC0_CH0_CR_SC_PH_THROTTLING_3_loaded_different_rank_WID      ( 6)
  #define MC0_CH0_CR_SC_PH_THROTTLING_3_loaded_different_rank_MSK      (0x003F0000)
  #define MC0_CH0_CR_SC_PH_THROTTLING_3_loaded_different_rank_MIN      (0)
  #define MC0_CH0_CR_SC_PH_THROTTLING_3_loaded_different_rank_MAX      (63) // 0x0000003F
  #define MC0_CH0_CR_SC_PH_THROTTLING_3_loaded_different_rank_DEF      (0x00000000)
  #define MC0_CH0_CR_SC_PH_THROTTLING_3_loaded_different_rank_HSH      (0x0620E09C)

  #define MC0_CH0_CR_SC_PH_THROTTLING_3_unloaded_different_rank_OFF    (24)
  #define MC0_CH0_CR_SC_PH_THROTTLING_3_unloaded_different_rank_WID    ( 6)
  #define MC0_CH0_CR_SC_PH_THROTTLING_3_unloaded_different_rank_MSK    (0x3F000000)
  #define MC0_CH0_CR_SC_PH_THROTTLING_3_unloaded_different_rank_MIN    (0)
  #define MC0_CH0_CR_SC_PH_THROTTLING_3_unloaded_different_rank_MAX    (63) // 0x0000003F
  #define MC0_CH0_CR_SC_PH_THROTTLING_3_unloaded_different_rank_DEF    (0x00000000)
  #define MC0_CH0_CR_SC_PH_THROTTLING_3_unloaded_different_rank_HSH    (0x0630E09C)

#define MC0_CH0_CR_SC_WPQ_THRESHOLD_REG                                (0x0000E0A0)

  #define MC0_CH0_CR_SC_WPQ_THRESHOLD_Low_WM_OFF                       ( 0)
  #define MC0_CH0_CR_SC_WPQ_THRESHOLD_Low_WM_WID                       ( 6)
  #define MC0_CH0_CR_SC_WPQ_THRESHOLD_Low_WM_MSK                       (0x0000003F)
  #define MC0_CH0_CR_SC_WPQ_THRESHOLD_Low_WM_MIN                       (0)
  #define MC0_CH0_CR_SC_WPQ_THRESHOLD_Low_WM_MAX                       (63) // 0x0000003F
  #define MC0_CH0_CR_SC_WPQ_THRESHOLD_Low_WM_DEF                       (0x0000000A)
  #define MC0_CH0_CR_SC_WPQ_THRESHOLD_Low_WM_HSH                       (0x0600E0A0)

  #define MC0_CH0_CR_SC_WPQ_THRESHOLD_Med_WM_OFF                       ( 6)
  #define MC0_CH0_CR_SC_WPQ_THRESHOLD_Med_WM_WID                       ( 6)
  #define MC0_CH0_CR_SC_WPQ_THRESHOLD_Med_WM_MSK                       (0x00000FC0)
  #define MC0_CH0_CR_SC_WPQ_THRESHOLD_Med_WM_MIN                       (0)
  #define MC0_CH0_CR_SC_WPQ_THRESHOLD_Med_WM_MAX                       (63) // 0x0000003F
  #define MC0_CH0_CR_SC_WPQ_THRESHOLD_Med_WM_DEF                       (0x00000014)
  #define MC0_CH0_CR_SC_WPQ_THRESHOLD_Med_WM_HSH                       (0x060CE0A0)

  #define MC0_CH0_CR_SC_WPQ_THRESHOLD_High_WM_OFF                      (12)
  #define MC0_CH0_CR_SC_WPQ_THRESHOLD_High_WM_WID                      ( 6)
  #define MC0_CH0_CR_SC_WPQ_THRESHOLD_High_WM_MSK                      (0x0003F000)
  #define MC0_CH0_CR_SC_WPQ_THRESHOLD_High_WM_MIN                      (0)
  #define MC0_CH0_CR_SC_WPQ_THRESHOLD_High_WM_MAX                      (63) // 0x0000003F
  #define MC0_CH0_CR_SC_WPQ_THRESHOLD_High_WM_DEF                      (0x00000024)
  #define MC0_CH0_CR_SC_WPQ_THRESHOLD_High_WM_HSH                      (0x0618E0A0)

  #define MC0_CH0_CR_SC_WPQ_THRESHOLD_PHs_allowed_under_low_wm_OFF     (18)
  #define MC0_CH0_CR_SC_WPQ_THRESHOLD_PHs_allowed_under_low_wm_WID     ( 4)
  #define MC0_CH0_CR_SC_WPQ_THRESHOLD_PHs_allowed_under_low_wm_MSK     (0x003C0000)
  #define MC0_CH0_CR_SC_WPQ_THRESHOLD_PHs_allowed_under_low_wm_MIN     (0)
  #define MC0_CH0_CR_SC_WPQ_THRESHOLD_PHs_allowed_under_low_wm_MAX     (15) // 0x0000000F
  #define MC0_CH0_CR_SC_WPQ_THRESHOLD_PHs_allowed_under_low_wm_DEF     (0x00000001)
  #define MC0_CH0_CR_SC_WPQ_THRESHOLD_PHs_allowed_under_low_wm_HSH     (0x0424E0A0)

  #define MC0_CH0_CR_SC_WPQ_THRESHOLD_PHs_allowed_under_med_wm_OFF     (22)
  #define MC0_CH0_CR_SC_WPQ_THRESHOLD_PHs_allowed_under_med_wm_WID     ( 4)
  #define MC0_CH0_CR_SC_WPQ_THRESHOLD_PHs_allowed_under_med_wm_MSK     (0x03C00000)
  #define MC0_CH0_CR_SC_WPQ_THRESHOLD_PHs_allowed_under_med_wm_MIN     (0)
  #define MC0_CH0_CR_SC_WPQ_THRESHOLD_PHs_allowed_under_med_wm_MAX     (15) // 0x0000000F
  #define MC0_CH0_CR_SC_WPQ_THRESHOLD_PHs_allowed_under_med_wm_DEF     (0x00000002)
  #define MC0_CH0_CR_SC_WPQ_THRESHOLD_PHs_allowed_under_med_wm_HSH     (0x042CE0A0)

  #define MC0_CH0_CR_SC_WPQ_THRESHOLD_PHs_allowed_under_high_wm_OFF    (26)
  #define MC0_CH0_CR_SC_WPQ_THRESHOLD_PHs_allowed_under_high_wm_WID    ( 4)
  #define MC0_CH0_CR_SC_WPQ_THRESHOLD_PHs_allowed_under_high_wm_MSK    (0x3C000000)
  #define MC0_CH0_CR_SC_WPQ_THRESHOLD_PHs_allowed_under_high_wm_MIN    (0)
  #define MC0_CH0_CR_SC_WPQ_THRESHOLD_PHs_allowed_under_high_wm_MAX    (15) // 0x0000000F
  #define MC0_CH0_CR_SC_WPQ_THRESHOLD_PHs_allowed_under_high_wm_DEF    (0x00000004)
  #define MC0_CH0_CR_SC_WPQ_THRESHOLD_PHs_allowed_under_high_wm_HSH    (0x0434E0A0)

  #define MC0_CH0_CR_SC_WPQ_THRESHOLD_RPQ_PHs_weight_OFF               (30)
  #define MC0_CH0_CR_SC_WPQ_THRESHOLD_RPQ_PHs_weight_WID               ( 2)
  #define MC0_CH0_CR_SC_WPQ_THRESHOLD_RPQ_PHs_weight_MSK               (0xC0000000)
  #define MC0_CH0_CR_SC_WPQ_THRESHOLD_RPQ_PHs_weight_MIN               (0)
  #define MC0_CH0_CR_SC_WPQ_THRESHOLD_RPQ_PHs_weight_MAX               (3) // 0x00000003
  #define MC0_CH0_CR_SC_WPQ_THRESHOLD_RPQ_PHs_weight_DEF               (0x00000003)
  #define MC0_CH0_CR_SC_WPQ_THRESHOLD_RPQ_PHs_weight_HSH               (0x023CE0A0)

#define MC0_CH0_CR_SC_PR_CNT_CONFIG_REG                                (0x0000E0A8)

  #define MC0_CH0_CR_SC_PR_CNT_CONFIG_Ring_OFF                         ( 0)
  #define MC0_CH0_CR_SC_PR_CNT_CONFIG_Ring_WID                         (10)
  #define MC0_CH0_CR_SC_PR_CNT_CONFIG_Ring_MSK                         (0x000003FF)
  #define MC0_CH0_CR_SC_PR_CNT_CONFIG_Ring_MIN                         (0)
  #define MC0_CH0_CR_SC_PR_CNT_CONFIG_Ring_MAX                         (1023) // 0x000003FF
  #define MC0_CH0_CR_SC_PR_CNT_CONFIG_Ring_DEF                         (0x00000040)
  #define MC0_CH0_CR_SC_PR_CNT_CONFIG_Ring_HSH                         (0x4A00E0A8)

  #define MC0_CH0_CR_SC_PR_CNT_CONFIG_SA_OFF                           (10)
  #define MC0_CH0_CR_SC_PR_CNT_CONFIG_SA_WID                           (10)
  #define MC0_CH0_CR_SC_PR_CNT_CONFIG_SA_MSK                           (0x000FFC00)
  #define MC0_CH0_CR_SC_PR_CNT_CONFIG_SA_MIN                           (0)
  #define MC0_CH0_CR_SC_PR_CNT_CONFIG_SA_MAX                           (1023) // 0x000003FF
  #define MC0_CH0_CR_SC_PR_CNT_CONFIG_SA_DEF                           (0x00000100)
  #define MC0_CH0_CR_SC_PR_CNT_CONFIG_SA_HSH                           (0x4A14E0A8)

  #define MC0_CH0_CR_SC_PR_CNT_CONFIG_VC0_latency_guard_timer_x16_OFF  (20)
  #define MC0_CH0_CR_SC_PR_CNT_CONFIG_VC0_latency_guard_timer_x16_WID  ( 9)
  #define MC0_CH0_CR_SC_PR_CNT_CONFIG_VC0_latency_guard_timer_x16_MSK  (0x1FF00000)
  #define MC0_CH0_CR_SC_PR_CNT_CONFIG_VC0_latency_guard_timer_x16_MIN  (0)
  #define MC0_CH0_CR_SC_PR_CNT_CONFIG_VC0_latency_guard_timer_x16_MAX  (511) // 0x000001FF
  #define MC0_CH0_CR_SC_PR_CNT_CONFIG_VC0_latency_guard_timer_x16_DEF  (0x00000000)
  #define MC0_CH0_CR_SC_PR_CNT_CONFIG_VC0_latency_guard_timer_x16_HSH  (0x4928E0A8)

  #define MC0_CH0_CR_SC_PR_CNT_CONFIG_VC1_latency_guard_timer_x8_OFF   (32)
  #define MC0_CH0_CR_SC_PR_CNT_CONFIG_VC1_latency_guard_timer_x8_WID   (11)
  #define MC0_CH0_CR_SC_PR_CNT_CONFIG_VC1_latency_guard_timer_x8_MSK   (0x000007FF00000000ULL)
  #define MC0_CH0_CR_SC_PR_CNT_CONFIG_VC1_latency_guard_timer_x8_MIN   (0)
  #define MC0_CH0_CR_SC_PR_CNT_CONFIG_VC1_latency_guard_timer_x8_MAX   (2047) // 0x000007FF
  #define MC0_CH0_CR_SC_PR_CNT_CONFIG_VC1_latency_guard_timer_x8_DEF   (0x00000010)
  #define MC0_CH0_CR_SC_PR_CNT_CONFIG_VC1_latency_guard_timer_x8_HSH   (0x4B40E0A8)

  #define MC0_CH0_CR_SC_PR_CNT_CONFIG_VC1_Isoc_latency_guard_timer_x8_OFF (43)
  #define MC0_CH0_CR_SC_PR_CNT_CONFIG_VC1_Isoc_latency_guard_timer_x8_WID (11)
  #define MC0_CH0_CR_SC_PR_CNT_CONFIG_VC1_Isoc_latency_guard_timer_x8_MSK (0x003FF80000000000ULL)
  #define MC0_CH0_CR_SC_PR_CNT_CONFIG_VC1_Isoc_latency_guard_timer_x8_MIN (0)
  #define MC0_CH0_CR_SC_PR_CNT_CONFIG_VC1_Isoc_latency_guard_timer_x8_MAX (2047) // 0x000007FF
  #define MC0_CH0_CR_SC_PR_CNT_CONFIG_VC1_Isoc_latency_guard_timer_x8_DEF (0x00000008)
  #define MC0_CH0_CR_SC_PR_CNT_CONFIG_VC1_Isoc_latency_guard_timer_x8_HSH (0x4B56E0A8)

  #define MC0_CH0_CR_SC_PR_CNT_CONFIG_VC0_RT_Latency_guard_timer_x16_OFF (54)
  #define MC0_CH0_CR_SC_PR_CNT_CONFIG_VC0_RT_Latency_guard_timer_x16_WID ( 9)
  #define MC0_CH0_CR_SC_PR_CNT_CONFIG_VC0_RT_Latency_guard_timer_x16_MSK (0x7FC0000000000000ULL)
  #define MC0_CH0_CR_SC_PR_CNT_CONFIG_VC0_RT_Latency_guard_timer_x16_MIN (0)
  #define MC0_CH0_CR_SC_PR_CNT_CONFIG_VC0_RT_Latency_guard_timer_x16_MAX (511) // 0x000001FF
  #define MC0_CH0_CR_SC_PR_CNT_CONFIG_VC0_RT_Latency_guard_timer_x16_DEF (0x0000000A)
  #define MC0_CH0_CR_SC_PR_CNT_CONFIG_VC0_RT_Latency_guard_timer_x16_HSH (0x496CE0A8)

#define MC0_CH0_CR_REUT_CH_MISC_CKE_CS_CTRL_REG                        (0x0000E0B0)

  #define MC0_CH0_CR_REUT_CH_MISC_CKE_CS_CTRL_CKE_Override_OFF         ( 0)
  #define MC0_CH0_CR_REUT_CH_MISC_CKE_CS_CTRL_CKE_Override_WID         ( 4)
  #define MC0_CH0_CR_REUT_CH_MISC_CKE_CS_CTRL_CKE_Override_MSK         (0x0000000F)
  #define MC0_CH0_CR_REUT_CH_MISC_CKE_CS_CTRL_CKE_Override_MIN         (0)
  #define MC0_CH0_CR_REUT_CH_MISC_CKE_CS_CTRL_CKE_Override_MAX         (15) // 0x0000000F
  #define MC0_CH0_CR_REUT_CH_MISC_CKE_CS_CTRL_CKE_Override_DEF         (0x0000000F)
  #define MC0_CH0_CR_REUT_CH_MISC_CKE_CS_CTRL_CKE_Override_HSH         (0x0400E0B0)

  #define MC0_CH0_CR_REUT_CH_MISC_CKE_CS_CTRL_CS_Override_0_OFF        ( 4)
  #define MC0_CH0_CR_REUT_CH_MISC_CKE_CS_CTRL_CS_Override_0_WID        ( 4)
  #define MC0_CH0_CR_REUT_CH_MISC_CKE_CS_CTRL_CS_Override_0_MSK        (0x000000F0)
  #define MC0_CH0_CR_REUT_CH_MISC_CKE_CS_CTRL_CS_Override_0_MIN        (0)
  #define MC0_CH0_CR_REUT_CH_MISC_CKE_CS_CTRL_CS_Override_0_MAX        (15) // 0x0000000F
  #define MC0_CH0_CR_REUT_CH_MISC_CKE_CS_CTRL_CS_Override_0_DEF        (0x00000000)
  #define MC0_CH0_CR_REUT_CH_MISC_CKE_CS_CTRL_CS_Override_0_HSH        (0x0408E0B0)

  #define MC0_CH0_CR_REUT_CH_MISC_CKE_CS_CTRL_CS_Override_1_OFF        ( 8)
  #define MC0_CH0_CR_REUT_CH_MISC_CKE_CS_CTRL_CS_Override_1_WID        ( 4)
  #define MC0_CH0_CR_REUT_CH_MISC_CKE_CS_CTRL_CS_Override_1_MSK        (0x00000F00)
  #define MC0_CH0_CR_REUT_CH_MISC_CKE_CS_CTRL_CS_Override_1_MIN        (0)
  #define MC0_CH0_CR_REUT_CH_MISC_CKE_CS_CTRL_CS_Override_1_MAX        (15) // 0x0000000F
  #define MC0_CH0_CR_REUT_CH_MISC_CKE_CS_CTRL_CS_Override_1_DEF        (0x00000000)
  #define MC0_CH0_CR_REUT_CH_MISC_CKE_CS_CTRL_CS_Override_1_HSH        (0x0410E0B0)

  #define MC0_CH0_CR_REUT_CH_MISC_CKE_CS_CTRL_CKE_On_OFF               (16)
  #define MC0_CH0_CR_REUT_CH_MISC_CKE_CS_CTRL_CKE_On_WID               ( 4)
  #define MC0_CH0_CR_REUT_CH_MISC_CKE_CS_CTRL_CKE_On_MSK               (0x000F0000)
  #define MC0_CH0_CR_REUT_CH_MISC_CKE_CS_CTRL_CKE_On_MIN               (0)
  #define MC0_CH0_CR_REUT_CH_MISC_CKE_CS_CTRL_CKE_On_MAX               (15) // 0x0000000F
  #define MC0_CH0_CR_REUT_CH_MISC_CKE_CS_CTRL_CKE_On_DEF               (0x00000000)
  #define MC0_CH0_CR_REUT_CH_MISC_CKE_CS_CTRL_CKE_On_HSH               (0x0420E0B0)

  #define MC0_CH0_CR_REUT_CH_MISC_CKE_CS_CTRL_CS_Override_Val_0_OFF    (20)
  #define MC0_CH0_CR_REUT_CH_MISC_CKE_CS_CTRL_CS_Override_Val_0_WID    ( 4)
  #define MC0_CH0_CR_REUT_CH_MISC_CKE_CS_CTRL_CS_Override_Val_0_MSK    (0x00F00000)
  #define MC0_CH0_CR_REUT_CH_MISC_CKE_CS_CTRL_CS_Override_Val_0_MIN    (0)
  #define MC0_CH0_CR_REUT_CH_MISC_CKE_CS_CTRL_CS_Override_Val_0_MAX    (15) // 0x0000000F
  #define MC0_CH0_CR_REUT_CH_MISC_CKE_CS_CTRL_CS_Override_Val_0_DEF    (0x00000000)
  #define MC0_CH0_CR_REUT_CH_MISC_CKE_CS_CTRL_CS_Override_Val_0_HSH    (0x0428E0B0)

  #define MC0_CH0_CR_REUT_CH_MISC_CKE_CS_CTRL_CS_Override_Val_1_OFF    (24)
  #define MC0_CH0_CR_REUT_CH_MISC_CKE_CS_CTRL_CS_Override_Val_1_WID    ( 4)
  #define MC0_CH0_CR_REUT_CH_MISC_CKE_CS_CTRL_CS_Override_Val_1_MSK    (0x0F000000)
  #define MC0_CH0_CR_REUT_CH_MISC_CKE_CS_CTRL_CS_Override_Val_1_MIN    (0)
  #define MC0_CH0_CR_REUT_CH_MISC_CKE_CS_CTRL_CS_Override_Val_1_MAX    (15) // 0x0000000F
  #define MC0_CH0_CR_REUT_CH_MISC_CKE_CS_CTRL_CS_Override_Val_1_DEF    (0x00000000)
  #define MC0_CH0_CR_REUT_CH_MISC_CKE_CS_CTRL_CS_Override_Val_1_HSH    (0x0430E0B0)

#define MC0_CH0_CR_REUT_CH_MISC_ODT_CTRL_REG                           (0x0000E0B4)

  #define MC0_CH0_CR_REUT_CH_MISC_ODT_CTRL_ODT_Override_OFF            ( 0)
  #define MC0_CH0_CR_REUT_CH_MISC_ODT_CTRL_ODT_Override_WID            ( 4)
  #define MC0_CH0_CR_REUT_CH_MISC_ODT_CTRL_ODT_Override_MSK            (0x0000000F)
  #define MC0_CH0_CR_REUT_CH_MISC_ODT_CTRL_ODT_Override_MIN            (0)
  #define MC0_CH0_CR_REUT_CH_MISC_ODT_CTRL_ODT_Override_MAX            (15) // 0x0000000F
  #define MC0_CH0_CR_REUT_CH_MISC_ODT_CTRL_ODT_Override_DEF            (0x00000000)
  #define MC0_CH0_CR_REUT_CH_MISC_ODT_CTRL_ODT_Override_HSH            (0x0400E0B4)

  #define MC0_CH0_CR_REUT_CH_MISC_ODT_CTRL_ODT_On_OFF                  (16)
  #define MC0_CH0_CR_REUT_CH_MISC_ODT_CTRL_ODT_On_WID                  ( 4)
  #define MC0_CH0_CR_REUT_CH_MISC_ODT_CTRL_ODT_On_MSK                  (0x000F0000)
  #define MC0_CH0_CR_REUT_CH_MISC_ODT_CTRL_ODT_On_MIN                  (0)
  #define MC0_CH0_CR_REUT_CH_MISC_ODT_CTRL_ODT_On_MAX                  (15) // 0x0000000F
  #define MC0_CH0_CR_REUT_CH_MISC_ODT_CTRL_ODT_On_DEF                  (0x00000000)
  #define MC0_CH0_CR_REUT_CH_MISC_ODT_CTRL_ODT_On_HSH                  (0x0420E0B4)

  #define MC0_CH0_CR_REUT_CH_MISC_ODT_CTRL_MPR_Train_DDR_On_OFF        (31)
  #define MC0_CH0_CR_REUT_CH_MISC_ODT_CTRL_MPR_Train_DDR_On_WID        ( 1)
  #define MC0_CH0_CR_REUT_CH_MISC_ODT_CTRL_MPR_Train_DDR_On_MSK        (0x80000000)
  #define MC0_CH0_CR_REUT_CH_MISC_ODT_CTRL_MPR_Train_DDR_On_MIN        (0)
  #define MC0_CH0_CR_REUT_CH_MISC_ODT_CTRL_MPR_Train_DDR_On_MAX        (1) // 0x00000001
  #define MC0_CH0_CR_REUT_CH_MISC_ODT_CTRL_MPR_Train_DDR_On_DEF        (0x00000000)
  #define MC0_CH0_CR_REUT_CH_MISC_ODT_CTRL_MPR_Train_DDR_On_HSH        (0x013EE0B4)

#define MC0_CH0_CR_SPID_LOW_POWER_CTL_REG                              (0x0000E0B8)

  #define MC0_CH0_CR_SPID_LOW_POWER_CTL_raise_cke_after_exit_latency_OFF ( 0)
  #define MC0_CH0_CR_SPID_LOW_POWER_CTL_raise_cke_after_exit_latency_WID ( 1)
  #define MC0_CH0_CR_SPID_LOW_POWER_CTL_raise_cke_after_exit_latency_MSK (0x00000001)
  #define MC0_CH0_CR_SPID_LOW_POWER_CTL_raise_cke_after_exit_latency_MIN (0)
  #define MC0_CH0_CR_SPID_LOW_POWER_CTL_raise_cke_after_exit_latency_MAX (1) // 0x00000001
  #define MC0_CH0_CR_SPID_LOW_POWER_CTL_raise_cke_after_exit_latency_DEF (0x00000000)
  #define MC0_CH0_CR_SPID_LOW_POWER_CTL_raise_cke_after_exit_latency_HSH (0x0100E0B8)

  #define MC0_CH0_CR_SPID_LOW_POWER_CTL_idle_length_OFF                ( 1)
  #define MC0_CH0_CR_SPID_LOW_POWER_CTL_idle_length_WID                ( 4)
  #define MC0_CH0_CR_SPID_LOW_POWER_CTL_idle_length_MSK                (0x0000001E)
  #define MC0_CH0_CR_SPID_LOW_POWER_CTL_idle_length_MIN                (0)
  #define MC0_CH0_CR_SPID_LOW_POWER_CTL_idle_length_MAX                (15) // 0x0000000F
  #define MC0_CH0_CR_SPID_LOW_POWER_CTL_idle_length_DEF                (0x00000003)
  #define MC0_CH0_CR_SPID_LOW_POWER_CTL_idle_length_HSH                (0x0402E0B8)

  #define MC0_CH0_CR_SPID_LOW_POWER_CTL_powerdown_latency_OFF          ( 5)
  #define MC0_CH0_CR_SPID_LOW_POWER_CTL_powerdown_latency_WID          ( 5)
  #define MC0_CH0_CR_SPID_LOW_POWER_CTL_powerdown_latency_MSK          (0x000003E0)
  #define MC0_CH0_CR_SPID_LOW_POWER_CTL_powerdown_latency_MIN          (0)
  #define MC0_CH0_CR_SPID_LOW_POWER_CTL_powerdown_latency_MAX          (31) // 0x0000001F
  #define MC0_CH0_CR_SPID_LOW_POWER_CTL_powerdown_latency_DEF          (0x00000001)
  #define MC0_CH0_CR_SPID_LOW_POWER_CTL_powerdown_latency_HSH          (0x050AE0B8)

  #define MC0_CH0_CR_SPID_LOW_POWER_CTL_powerdown_length_OFF           (10)
  #define MC0_CH0_CR_SPID_LOW_POWER_CTL_powerdown_length_WID           ( 4)
  #define MC0_CH0_CR_SPID_LOW_POWER_CTL_powerdown_length_MSK           (0x00003C00)
  #define MC0_CH0_CR_SPID_LOW_POWER_CTL_powerdown_length_MIN           (0)
  #define MC0_CH0_CR_SPID_LOW_POWER_CTL_powerdown_length_MAX           (15) // 0x0000000F
  #define MC0_CH0_CR_SPID_LOW_POWER_CTL_powerdown_length_DEF           (0x00000001)
  #define MC0_CH0_CR_SPID_LOW_POWER_CTL_powerdown_length_HSH           (0x0414E0B8)

  #define MC0_CH0_CR_SPID_LOW_POWER_CTL_selfrefresh_latency_OFF        (14)
  #define MC0_CH0_CR_SPID_LOW_POWER_CTL_selfrefresh_latency_WID        ( 6)
  #define MC0_CH0_CR_SPID_LOW_POWER_CTL_selfrefresh_latency_MSK        (0x000FC000)
  #define MC0_CH0_CR_SPID_LOW_POWER_CTL_selfrefresh_latency_MIN        (0)
  #define MC0_CH0_CR_SPID_LOW_POWER_CTL_selfrefresh_latency_MAX        (63) // 0x0000003F
  #define MC0_CH0_CR_SPID_LOW_POWER_CTL_selfrefresh_latency_DEF        (0x00000001)
  #define MC0_CH0_CR_SPID_LOW_POWER_CTL_selfrefresh_latency_HSH        (0x061CE0B8)

  #define MC0_CH0_CR_SPID_LOW_POWER_CTL_selfrefresh_length_OFF         (20)
  #define MC0_CH0_CR_SPID_LOW_POWER_CTL_selfrefresh_length_WID         ( 4)
  #define MC0_CH0_CR_SPID_LOW_POWER_CTL_selfrefresh_length_MSK         (0x00F00000)
  #define MC0_CH0_CR_SPID_LOW_POWER_CTL_selfrefresh_length_MIN         (0)
  #define MC0_CH0_CR_SPID_LOW_POWER_CTL_selfrefresh_length_MAX         (15) // 0x0000000F
  #define MC0_CH0_CR_SPID_LOW_POWER_CTL_selfrefresh_length_DEF         (0x00000001)
  #define MC0_CH0_CR_SPID_LOW_POWER_CTL_selfrefresh_length_HSH         (0x0428E0B8)

  #define MC0_CH0_CR_SPID_LOW_POWER_CTL_ckevalid_length_OFF            (24)
  #define MC0_CH0_CR_SPID_LOW_POWER_CTL_ckevalid_length_WID            ( 4)
  #define MC0_CH0_CR_SPID_LOW_POWER_CTL_ckevalid_length_MSK            (0x0F000000)
  #define MC0_CH0_CR_SPID_LOW_POWER_CTL_ckevalid_length_MIN            (0)
  #define MC0_CH0_CR_SPID_LOW_POWER_CTL_ckevalid_length_MAX            (15) // 0x0000000F
  #define MC0_CH0_CR_SPID_LOW_POWER_CTL_ckevalid_length_DEF            (0x00000008)
  #define MC0_CH0_CR_SPID_LOW_POWER_CTL_ckevalid_length_HSH            (0x0430E0B8)

  #define MC0_CH0_CR_SPID_LOW_POWER_CTL_ckevalid_enable_OFF            (28)
  #define MC0_CH0_CR_SPID_LOW_POWER_CTL_ckevalid_enable_WID            ( 1)
  #define MC0_CH0_CR_SPID_LOW_POWER_CTL_ckevalid_enable_MSK            (0x10000000)
  #define MC0_CH0_CR_SPID_LOW_POWER_CTL_ckevalid_enable_MIN            (0)
  #define MC0_CH0_CR_SPID_LOW_POWER_CTL_ckevalid_enable_MAX            (1) // 0x00000001
  #define MC0_CH0_CR_SPID_LOW_POWER_CTL_ckevalid_enable_DEF            (0x00000000)
  #define MC0_CH0_CR_SPID_LOW_POWER_CTL_ckevalid_enable_HSH            (0x0138E0B8)

  #define MC0_CH0_CR_SPID_LOW_POWER_CTL_idle_enable_OFF                (29)
  #define MC0_CH0_CR_SPID_LOW_POWER_CTL_idle_enable_WID                ( 1)
  #define MC0_CH0_CR_SPID_LOW_POWER_CTL_idle_enable_MSK                (0x20000000)
  #define MC0_CH0_CR_SPID_LOW_POWER_CTL_idle_enable_MIN                (0)
  #define MC0_CH0_CR_SPID_LOW_POWER_CTL_idle_enable_MAX                (1) // 0x00000001
  #define MC0_CH0_CR_SPID_LOW_POWER_CTL_idle_enable_DEF                (0x00000000)
  #define MC0_CH0_CR_SPID_LOW_POWER_CTL_idle_enable_HSH                (0x013AE0B8)

  #define MC0_CH0_CR_SPID_LOW_POWER_CTL_powerdown_enable_OFF           (30)
  #define MC0_CH0_CR_SPID_LOW_POWER_CTL_powerdown_enable_WID           ( 1)
  #define MC0_CH0_CR_SPID_LOW_POWER_CTL_powerdown_enable_MSK           (0x40000000)
  #define MC0_CH0_CR_SPID_LOW_POWER_CTL_powerdown_enable_MIN           (0)
  #define MC0_CH0_CR_SPID_LOW_POWER_CTL_powerdown_enable_MAX           (1) // 0x00000001
  #define MC0_CH0_CR_SPID_LOW_POWER_CTL_powerdown_enable_DEF           (0x00000000)
  #define MC0_CH0_CR_SPID_LOW_POWER_CTL_powerdown_enable_HSH           (0x013CE0B8)

  #define MC0_CH0_CR_SPID_LOW_POWER_CTL_selfrefresh_enable_OFF         (31)
  #define MC0_CH0_CR_SPID_LOW_POWER_CTL_selfrefresh_enable_WID         ( 1)
  #define MC0_CH0_CR_SPID_LOW_POWER_CTL_selfrefresh_enable_MSK         (0x80000000)
  #define MC0_CH0_CR_SPID_LOW_POWER_CTL_selfrefresh_enable_MIN         (0)
  #define MC0_CH0_CR_SPID_LOW_POWER_CTL_selfrefresh_enable_MAX         (1) // 0x00000001
  #define MC0_CH0_CR_SPID_LOW_POWER_CTL_selfrefresh_enable_DEF         (0x00000000)
  #define MC0_CH0_CR_SPID_LOW_POWER_CTL_selfrefresh_enable_HSH         (0x013EE0B8)

#define MC0_CH0_CR_SC_GS_CFG_TRAINING_REG                              (0x0000E0BC)

  #define MC0_CH0_CR_SC_GS_CFG_TRAINING_reset_on_command_OFF           ( 1)
  #define MC0_CH0_CR_SC_GS_CFG_TRAINING_reset_on_command_WID           ( 4)
  #define MC0_CH0_CR_SC_GS_CFG_TRAINING_reset_on_command_MSK           (0x0000001E)
  #define MC0_CH0_CR_SC_GS_CFG_TRAINING_reset_on_command_MIN           (0)
  #define MC0_CH0_CR_SC_GS_CFG_TRAINING_reset_on_command_MAX           (15) // 0x0000000F
  #define MC0_CH0_CR_SC_GS_CFG_TRAINING_reset_on_command_DEF           (0x00000000)
  #define MC0_CH0_CR_SC_GS_CFG_TRAINING_reset_on_command_HSH           (0x0402E0BC)

  #define MC0_CH0_CR_SC_GS_CFG_TRAINING_reset_delay_OFF                ( 5)
  #define MC0_CH0_CR_SC_GS_CFG_TRAINING_reset_delay_WID                ( 3)
  #define MC0_CH0_CR_SC_GS_CFG_TRAINING_reset_delay_MSK                (0x000000E0)
  #define MC0_CH0_CR_SC_GS_CFG_TRAINING_reset_delay_MIN                (0)
  #define MC0_CH0_CR_SC_GS_CFG_TRAINING_reset_delay_MAX                (7) // 0x00000007
  #define MC0_CH0_CR_SC_GS_CFG_TRAINING_reset_delay_DEF                (0x00000000)
  #define MC0_CH0_CR_SC_GS_CFG_TRAINING_reset_delay_HSH                (0x030AE0BC)

  #define MC0_CH0_CR_SC_GS_CFG_TRAINING_ignore_cke_OFF                 ( 8)
  #define MC0_CH0_CR_SC_GS_CFG_TRAINING_ignore_cke_WID                 ( 1)
  #define MC0_CH0_CR_SC_GS_CFG_TRAINING_ignore_cke_MSK                 (0x00000100)
  #define MC0_CH0_CR_SC_GS_CFG_TRAINING_ignore_cke_MIN                 (0)
  #define MC0_CH0_CR_SC_GS_CFG_TRAINING_ignore_cke_MAX                 (1) // 0x00000001
  #define MC0_CH0_CR_SC_GS_CFG_TRAINING_ignore_cke_DEF                 (0x00000000)
  #define MC0_CH0_CR_SC_GS_CFG_TRAINING_ignore_cke_HSH                 (0x0110E0BC)

  #define MC0_CH0_CR_SC_GS_CFG_TRAINING_mask_cs_OFF                    ( 9)
  #define MC0_CH0_CR_SC_GS_CFG_TRAINING_mask_cs_WID                    ( 1)
  #define MC0_CH0_CR_SC_GS_CFG_TRAINING_mask_cs_MSK                    (0x00000200)
  #define MC0_CH0_CR_SC_GS_CFG_TRAINING_mask_cs_MIN                    (0)
  #define MC0_CH0_CR_SC_GS_CFG_TRAINING_mask_cs_MAX                    (1) // 0x00000001
  #define MC0_CH0_CR_SC_GS_CFG_TRAINING_mask_cs_DEF                    (0x00000000)
  #define MC0_CH0_CR_SC_GS_CFG_TRAINING_mask_cs_HSH                    (0x0112E0BC)

  #define MC0_CH0_CR_SC_GS_CFG_TRAINING_lp4_fifo_rd_wr_OFF             (10)
  #define MC0_CH0_CR_SC_GS_CFG_TRAINING_lp4_fifo_rd_wr_WID             ( 1)
  #define MC0_CH0_CR_SC_GS_CFG_TRAINING_lp4_fifo_rd_wr_MSK             (0x00000400)
  #define MC0_CH0_CR_SC_GS_CFG_TRAINING_lp4_fifo_rd_wr_MIN             (0)
  #define MC0_CH0_CR_SC_GS_CFG_TRAINING_lp4_fifo_rd_wr_MAX             (1) // 0x00000001
  #define MC0_CH0_CR_SC_GS_CFG_TRAINING_lp4_fifo_rd_wr_DEF             (0x00000000)
  #define MC0_CH0_CR_SC_GS_CFG_TRAINING_lp4_fifo_rd_wr_HSH             (0x0114E0BC)

  #define MC0_CH0_CR_SC_GS_CFG_TRAINING_disable_tristate_OFF           (11)
  #define MC0_CH0_CR_SC_GS_CFG_TRAINING_disable_tristate_WID           ( 1)
  #define MC0_CH0_CR_SC_GS_CFG_TRAINING_disable_tristate_MSK           (0x00000800)
  #define MC0_CH0_CR_SC_GS_CFG_TRAINING_disable_tristate_MIN           (0)
  #define MC0_CH0_CR_SC_GS_CFG_TRAINING_disable_tristate_MAX           (1) // 0x00000001
  #define MC0_CH0_CR_SC_GS_CFG_TRAINING_disable_tristate_DEF           (0x00000000)
  #define MC0_CH0_CR_SC_GS_CFG_TRAINING_disable_tristate_HSH           (0x0116E0BC)

  #define MC0_CH0_CR_SC_GS_CFG_TRAINING_cpgc_in_order_OFF              (12)
  #define MC0_CH0_CR_SC_GS_CFG_TRAINING_cpgc_in_order_WID              ( 1)
  #define MC0_CH0_CR_SC_GS_CFG_TRAINING_cpgc_in_order_MSK              (0x00001000)
  #define MC0_CH0_CR_SC_GS_CFG_TRAINING_cpgc_in_order_MIN              (0)
  #define MC0_CH0_CR_SC_GS_CFG_TRAINING_cpgc_in_order_MAX              (1) // 0x00000001
  #define MC0_CH0_CR_SC_GS_CFG_TRAINING_cpgc_in_order_DEF              (0x00000000)
  #define MC0_CH0_CR_SC_GS_CFG_TRAINING_cpgc_in_order_HSH              (0x0118E0BC)

  #define MC0_CH0_CR_SC_GS_CFG_TRAINING_cadb_enable_OFF                (13)
  #define MC0_CH0_CR_SC_GS_CFG_TRAINING_cadb_enable_WID                ( 1)
  #define MC0_CH0_CR_SC_GS_CFG_TRAINING_cadb_enable_MSK                (0x00002000)
  #define MC0_CH0_CR_SC_GS_CFG_TRAINING_cadb_enable_MIN                (0)
  #define MC0_CH0_CR_SC_GS_CFG_TRAINING_cadb_enable_MAX                (1) // 0x00000001
  #define MC0_CH0_CR_SC_GS_CFG_TRAINING_cadb_enable_DEF                (0x00000000)
  #define MC0_CH0_CR_SC_GS_CFG_TRAINING_cadb_enable_HSH                (0x011AE0BC)

  #define MC0_CH0_CR_SC_GS_CFG_TRAINING_deselect_enable_OFF            (14)
  #define MC0_CH0_CR_SC_GS_CFG_TRAINING_deselect_enable_WID            ( 1)
  #define MC0_CH0_CR_SC_GS_CFG_TRAINING_deselect_enable_MSK            (0x00004000)
  #define MC0_CH0_CR_SC_GS_CFG_TRAINING_deselect_enable_MIN            (0)
  #define MC0_CH0_CR_SC_GS_CFG_TRAINING_deselect_enable_MAX            (1) // 0x00000001
  #define MC0_CH0_CR_SC_GS_CFG_TRAINING_deselect_enable_DEF            (0x00000000)
  #define MC0_CH0_CR_SC_GS_CFG_TRAINING_deselect_enable_HSH            (0x011CE0BC)

  #define MC0_CH0_CR_SC_GS_CFG_TRAINING_Reset_CADB_OFF                 (15)
  #define MC0_CH0_CR_SC_GS_CFG_TRAINING_Reset_CADB_WID                 ( 1)
  #define MC0_CH0_CR_SC_GS_CFG_TRAINING_Reset_CADB_MSK                 (0x00008000)
  #define MC0_CH0_CR_SC_GS_CFG_TRAINING_Reset_CADB_MIN                 (0)
  #define MC0_CH0_CR_SC_GS_CFG_TRAINING_Reset_CADB_MAX                 (1) // 0x00000001
  #define MC0_CH0_CR_SC_GS_CFG_TRAINING_Reset_CADB_DEF                 (0x00000000)
  #define MC0_CH0_CR_SC_GS_CFG_TRAINING_Reset_CADB_HSH                 (0x011EE0BC)

  #define MC0_CH0_CR_SC_GS_CFG_TRAINING_bus_retain_on_n_to_1_bubble_OFF (16)
  #define MC0_CH0_CR_SC_GS_CFG_TRAINING_bus_retain_on_n_to_1_bubble_WID ( 1)
  #define MC0_CH0_CR_SC_GS_CFG_TRAINING_bus_retain_on_n_to_1_bubble_MSK (0x00010000)
  #define MC0_CH0_CR_SC_GS_CFG_TRAINING_bus_retain_on_n_to_1_bubble_MIN (0)
  #define MC0_CH0_CR_SC_GS_CFG_TRAINING_bus_retain_on_n_to_1_bubble_MAX (1) // 0x00000001
  #define MC0_CH0_CR_SC_GS_CFG_TRAINING_bus_retain_on_n_to_1_bubble_DEF (0x00000000)
  #define MC0_CH0_CR_SC_GS_CFG_TRAINING_bus_retain_on_n_to_1_bubble_HSH (0x0120E0BC)

  #define MC0_CH0_CR_SC_GS_CFG_TRAINING_Block_xarb_OFF                 (17)
  #define MC0_CH0_CR_SC_GS_CFG_TRAINING_Block_xarb_WID                 ( 1)
  #define MC0_CH0_CR_SC_GS_CFG_TRAINING_Block_xarb_MSK                 (0x00020000)
  #define MC0_CH0_CR_SC_GS_CFG_TRAINING_Block_xarb_MIN                 (0)
  #define MC0_CH0_CR_SC_GS_CFG_TRAINING_Block_xarb_MAX                 (1) // 0x00000001
  #define MC0_CH0_CR_SC_GS_CFG_TRAINING_Block_xarb_DEF                 (0x00000000)
  #define MC0_CH0_CR_SC_GS_CFG_TRAINING_Block_xarb_HSH                 (0x0122E0BC)

  #define MC0_CH0_CR_SC_GS_CFG_TRAINING_Block_cke_OFF                  (18)
  #define MC0_CH0_CR_SC_GS_CFG_TRAINING_Block_cke_WID                  ( 1)
  #define MC0_CH0_CR_SC_GS_CFG_TRAINING_Block_cke_MSK                  (0x00040000)
  #define MC0_CH0_CR_SC_GS_CFG_TRAINING_Block_cke_MIN                  (0)
  #define MC0_CH0_CR_SC_GS_CFG_TRAINING_Block_cke_MAX                  (1) // 0x00000001
  #define MC0_CH0_CR_SC_GS_CFG_TRAINING_Block_cke_DEF                  (0x00000000)
  #define MC0_CH0_CR_SC_GS_CFG_TRAINING_Block_cke_HSH                  (0x0124E0BC)

#define MC0_CH0_CR_SCHED_THIRD_CBIT_REG                                (0x0000E0C0)

  #define MC0_CH0_CR_SCHED_THIRD_CBIT_dis_isoc_block_relaxation_OFF    ( 0)
  #define MC0_CH0_CR_SCHED_THIRD_CBIT_dis_isoc_block_relaxation_WID    ( 1)
  #define MC0_CH0_CR_SCHED_THIRD_CBIT_dis_isoc_block_relaxation_MSK    (0x00000001)
  #define MC0_CH0_CR_SCHED_THIRD_CBIT_dis_isoc_block_relaxation_MIN    (0)
  #define MC0_CH0_CR_SCHED_THIRD_CBIT_dis_isoc_block_relaxation_MAX    (1) // 0x00000001
  #define MC0_CH0_CR_SCHED_THIRD_CBIT_dis_isoc_block_relaxation_DEF    (0x00000000)
  #define MC0_CH0_CR_SCHED_THIRD_CBIT_dis_isoc_block_relaxation_HSH    (0x0100E0C0)

  #define MC0_CH0_CR_SCHED_THIRD_CBIT_lp_read_blkr_OFF                 ( 1)
  #define MC0_CH0_CR_SCHED_THIRD_CBIT_lp_read_blkr_WID                 ( 1)
  #define MC0_CH0_CR_SCHED_THIRD_CBIT_lp_read_blkr_MSK                 (0x00000002)
  #define MC0_CH0_CR_SCHED_THIRD_CBIT_lp_read_blkr_MIN                 (0)
  #define MC0_CH0_CR_SCHED_THIRD_CBIT_lp_read_blkr_MAX                 (1) // 0x00000001
  #define MC0_CH0_CR_SCHED_THIRD_CBIT_lp_read_blkr_DEF                 (0x00000000)
  #define MC0_CH0_CR_SCHED_THIRD_CBIT_lp_read_blkr_HSH                 (0x0102E0C0)

  #define MC0_CH0_CR_SCHED_THIRD_CBIT_pe_read_for_pe_blkr_OFF          ( 2)
  #define MC0_CH0_CR_SCHED_THIRD_CBIT_pe_read_for_pe_blkr_WID          ( 1)
  #define MC0_CH0_CR_SCHED_THIRD_CBIT_pe_read_for_pe_blkr_MSK          (0x00000004)
  #define MC0_CH0_CR_SCHED_THIRD_CBIT_pe_read_for_pe_blkr_MIN          (0)
  #define MC0_CH0_CR_SCHED_THIRD_CBIT_pe_read_for_pe_blkr_MAX          (1) // 0x00000001
  #define MC0_CH0_CR_SCHED_THIRD_CBIT_pe_read_for_pe_blkr_DEF          (0x00000000)
  #define MC0_CH0_CR_SCHED_THIRD_CBIT_pe_read_for_pe_blkr_HSH          (0x0104E0C0)

  #define MC0_CH0_CR_SCHED_THIRD_CBIT_write_ph_blocks_read_pre_OFF     ( 3)
  #define MC0_CH0_CR_SCHED_THIRD_CBIT_write_ph_blocks_read_pre_WID     ( 1)
  #define MC0_CH0_CR_SCHED_THIRD_CBIT_write_ph_blocks_read_pre_MSK     (0x00000008)
  #define MC0_CH0_CR_SCHED_THIRD_CBIT_write_ph_blocks_read_pre_MIN     (0)
  #define MC0_CH0_CR_SCHED_THIRD_CBIT_write_ph_blocks_read_pre_MAX     (1) // 0x00000001
  #define MC0_CH0_CR_SCHED_THIRD_CBIT_write_ph_blocks_read_pre_DEF     (0x00000000)
  #define MC0_CH0_CR_SCHED_THIRD_CBIT_write_ph_blocks_read_pre_HSH     (0x0106E0C0)

  #define MC0_CH0_CR_SCHED_THIRD_CBIT_Read_threshold_for_lp_read_bklr_OFF ( 4)
  #define MC0_CH0_CR_SCHED_THIRD_CBIT_Read_threshold_for_lp_read_bklr_WID ( 6)
  #define MC0_CH0_CR_SCHED_THIRD_CBIT_Read_threshold_for_lp_read_bklr_MSK (0x000003F0)

  #define MC0_CH0_CR_SCHED_THIRD_CBIT_Read_threshold_for_lp_read_bklr_MIN (0)
  #define MC0_CH0_CR_SCHED_THIRD_CBIT_Read_threshold_for_lp_read_bklr_MAX (63) // 0x0000003F

  #define MC0_CH0_CR_SCHED_THIRD_CBIT_Read_threshold_for_lp_read_bklr_DEF (0x00000008)
  #define MC0_CH0_CR_SCHED_THIRD_CBIT_Read_threshold_for_lp_read_bklr_HSH (0x0608E0C0)


  #define MC0_CH0_CR_SCHED_THIRD_CBIT_dis_ignore_1st_zqcs_OFF          (10)
  #define MC0_CH0_CR_SCHED_THIRD_CBIT_dis_ignore_1st_zqcs_WID          ( 1)
  #define MC0_CH0_CR_SCHED_THIRD_CBIT_dis_ignore_1st_zqcs_MSK          (0x00000400)
  #define MC0_CH0_CR_SCHED_THIRD_CBIT_dis_ignore_1st_zqcs_MIN          (0)
  #define MC0_CH0_CR_SCHED_THIRD_CBIT_dis_ignore_1st_zqcs_MAX          (1) // 0x00000001
  #define MC0_CH0_CR_SCHED_THIRD_CBIT_dis_ignore_1st_zqcs_DEF          (0x00000000)
  #define MC0_CH0_CR_SCHED_THIRD_CBIT_dis_ignore_1st_zqcs_HSH          (0x0114E0C0)

  #define MC0_CH0_CR_SCHED_THIRD_CBIT_preempt_vc1_during_demote_OFF    (11)
  #define MC0_CH0_CR_SCHED_THIRD_CBIT_preempt_vc1_during_demote_WID    ( 1)
  #define MC0_CH0_CR_SCHED_THIRD_CBIT_preempt_vc1_during_demote_MSK    (0x00000800)
  #define MC0_CH0_CR_SCHED_THIRD_CBIT_preempt_vc1_during_demote_MIN    (0)
  #define MC0_CH0_CR_SCHED_THIRD_CBIT_preempt_vc1_during_demote_MAX    (1) // 0x00000001
  #define MC0_CH0_CR_SCHED_THIRD_CBIT_preempt_vc1_during_demote_DEF    (0x00000000)
  #define MC0_CH0_CR_SCHED_THIRD_CBIT_preempt_vc1_during_demote_HSH    (0x0116E0C0)

  #define MC0_CH0_CR_SCHED_THIRD_CBIT_dis_idle_ref_start_OFF           (12)
  #define MC0_CH0_CR_SCHED_THIRD_CBIT_dis_idle_ref_start_WID           ( 1)
  #define MC0_CH0_CR_SCHED_THIRD_CBIT_dis_idle_ref_start_MSK           (0x00001000)
  #define MC0_CH0_CR_SCHED_THIRD_CBIT_dis_idle_ref_start_MIN           (0)
  #define MC0_CH0_CR_SCHED_THIRD_CBIT_dis_idle_ref_start_MAX           (1) // 0x00000001
  #define MC0_CH0_CR_SCHED_THIRD_CBIT_dis_idle_ref_start_DEF           (0x00000000)
  #define MC0_CH0_CR_SCHED_THIRD_CBIT_dis_idle_ref_start_HSH           (0x0118E0C0)

  #define MC0_CH0_CR_SCHED_THIRD_CBIT_invert_ALERT_n_OFF               (13)
  #define MC0_CH0_CR_SCHED_THIRD_CBIT_invert_ALERT_n_WID               ( 1)
  #define MC0_CH0_CR_SCHED_THIRD_CBIT_invert_ALERT_n_MSK               (0x00002000)
  #define MC0_CH0_CR_SCHED_THIRD_CBIT_invert_ALERT_n_MIN               (0)
  #define MC0_CH0_CR_SCHED_THIRD_CBIT_invert_ALERT_n_MAX               (1) // 0x00000001
  #define MC0_CH0_CR_SCHED_THIRD_CBIT_invert_ALERT_n_DEF               (0x00000001)
  #define MC0_CH0_CR_SCHED_THIRD_CBIT_invert_ALERT_n_HSH               (0x011AE0C0)

  #define MC0_CH0_CR_SCHED_THIRD_CBIT_dis_oldest_isoch_pre_over_ph_OFF (14)
  #define MC0_CH0_CR_SCHED_THIRD_CBIT_dis_oldest_isoch_pre_over_ph_WID ( 1)
  #define MC0_CH0_CR_SCHED_THIRD_CBIT_dis_oldest_isoch_pre_over_ph_MSK (0x00004000)
  #define MC0_CH0_CR_SCHED_THIRD_CBIT_dis_oldest_isoch_pre_over_ph_MIN (0)
  #define MC0_CH0_CR_SCHED_THIRD_CBIT_dis_oldest_isoch_pre_over_ph_MAX (1) // 0x00000001
  #define MC0_CH0_CR_SCHED_THIRD_CBIT_dis_oldest_isoch_pre_over_ph_DEF (0x00000000)
  #define MC0_CH0_CR_SCHED_THIRD_CBIT_dis_oldest_isoch_pre_over_ph_HSH (0x011CE0C0)

  #define MC0_CH0_CR_SCHED_THIRD_CBIT_dis_seperate_zq_block_OFF        (15)
  #define MC0_CH0_CR_SCHED_THIRD_CBIT_dis_seperate_zq_block_WID        ( 1)
  #define MC0_CH0_CR_SCHED_THIRD_CBIT_dis_seperate_zq_block_MSK        (0x00008000)
  #define MC0_CH0_CR_SCHED_THIRD_CBIT_dis_seperate_zq_block_MIN        (0)
  #define MC0_CH0_CR_SCHED_THIRD_CBIT_dis_seperate_zq_block_MAX        (1) // 0x00000001
  #define MC0_CH0_CR_SCHED_THIRD_CBIT_dis_seperate_zq_block_DEF        (0x00000000)
  #define MC0_CH0_CR_SCHED_THIRD_CBIT_dis_seperate_zq_block_HSH        (0x011EE0C0)

  #define MC0_CH0_CR_SCHED_THIRD_CBIT_dis_cke_off_in_refresh_OFF       (16)
  #define MC0_CH0_CR_SCHED_THIRD_CBIT_dis_cke_off_in_refresh_WID       ( 1)
  #define MC0_CH0_CR_SCHED_THIRD_CBIT_dis_cke_off_in_refresh_MSK       (0x00010000)
  #define MC0_CH0_CR_SCHED_THIRD_CBIT_dis_cke_off_in_refresh_MIN       (0)
  #define MC0_CH0_CR_SCHED_THIRD_CBIT_dis_cke_off_in_refresh_MAX       (1) // 0x00000001
  #define MC0_CH0_CR_SCHED_THIRD_CBIT_dis_cke_off_in_refresh_DEF       (0x00000000)
  #define MC0_CH0_CR_SCHED_THIRD_CBIT_dis_cke_off_in_refresh_HSH       (0x0120E0C0)

  #define MC0_CH0_CR_SCHED_THIRD_CBIT_allow_ecc_dft_overrides_OFF      (17)
  #define MC0_CH0_CR_SCHED_THIRD_CBIT_allow_ecc_dft_overrides_WID      ( 1)
  #define MC0_CH0_CR_SCHED_THIRD_CBIT_allow_ecc_dft_overrides_MSK      (0x00020000)
  #define MC0_CH0_CR_SCHED_THIRD_CBIT_allow_ecc_dft_overrides_MIN      (0)
  #define MC0_CH0_CR_SCHED_THIRD_CBIT_allow_ecc_dft_overrides_MAX      (1) // 0x00000001
  #define MC0_CH0_CR_SCHED_THIRD_CBIT_allow_ecc_dft_overrides_DEF      (0x00000000)
  #define MC0_CH0_CR_SCHED_THIRD_CBIT_allow_ecc_dft_overrides_HSH      (0x0122E0C0)

  #define MC0_CH0_CR_SCHED_THIRD_CBIT_dis_wdb_clk_gate_OFF             (18)
  #define MC0_CH0_CR_SCHED_THIRD_CBIT_dis_wdb_clk_gate_WID             ( 1)
  #define MC0_CH0_CR_SCHED_THIRD_CBIT_dis_wdb_clk_gate_MSK             (0x00040000)
  #define MC0_CH0_CR_SCHED_THIRD_CBIT_dis_wdb_clk_gate_MIN             (0)
  #define MC0_CH0_CR_SCHED_THIRD_CBIT_dis_wdb_clk_gate_MAX             (1) // 0x00000001
  #define MC0_CH0_CR_SCHED_THIRD_CBIT_dis_wdb_clk_gate_DEF             (0x00000000)
  #define MC0_CH0_CR_SCHED_THIRD_CBIT_dis_wdb_clk_gate_HSH             (0x0124E0C0)

  #define MC0_CH0_CR_SCHED_THIRD_CBIT_dis_mnt_clk_gate_OFF             (19)
  #define MC0_CH0_CR_SCHED_THIRD_CBIT_dis_mnt_clk_gate_WID             ( 1)
  #define MC0_CH0_CR_SCHED_THIRD_CBIT_dis_mnt_clk_gate_MSK             (0x00080000)
  #define MC0_CH0_CR_SCHED_THIRD_CBIT_dis_mnt_clk_gate_MIN             (0)
  #define MC0_CH0_CR_SCHED_THIRD_CBIT_dis_mnt_clk_gate_MAX             (1) // 0x00000001
  #define MC0_CH0_CR_SCHED_THIRD_CBIT_dis_mnt_clk_gate_DEF             (0x00000000)
  #define MC0_CH0_CR_SCHED_THIRD_CBIT_dis_mnt_clk_gate_HSH             (0x0126E0C0)

  #define MC0_CH0_CR_SCHED_THIRD_CBIT_dis_scheds_clk_gate_OFF          (20)
  #define MC0_CH0_CR_SCHED_THIRD_CBIT_dis_scheds_clk_gate_WID          ( 1)
  #define MC0_CH0_CR_SCHED_THIRD_CBIT_dis_scheds_clk_gate_MSK          (0x00100000)
  #define MC0_CH0_CR_SCHED_THIRD_CBIT_dis_scheds_clk_gate_MIN          (0)
  #define MC0_CH0_CR_SCHED_THIRD_CBIT_dis_scheds_clk_gate_MAX          (1) // 0x00000001
  #define MC0_CH0_CR_SCHED_THIRD_CBIT_dis_scheds_clk_gate_DEF          (0x00000000)
  #define MC0_CH0_CR_SCHED_THIRD_CBIT_dis_scheds_clk_gate_HSH          (0x0128E0C0)

  #define MC0_CH0_CR_SCHED_THIRD_CBIT_dis_lpmode_on_sagv_OFF           (21)
  #define MC0_CH0_CR_SCHED_THIRD_CBIT_dis_lpmode_on_sagv_WID           ( 1)
  #define MC0_CH0_CR_SCHED_THIRD_CBIT_dis_lpmode_on_sagv_MSK           (0x00200000)
  #define MC0_CH0_CR_SCHED_THIRD_CBIT_dis_lpmode_on_sagv_MIN           (0)
  #define MC0_CH0_CR_SCHED_THIRD_CBIT_dis_lpmode_on_sagv_MAX           (1) // 0x00000001
  #define MC0_CH0_CR_SCHED_THIRD_CBIT_dis_lpmode_on_sagv_DEF           (0x00000000)
  #define MC0_CH0_CR_SCHED_THIRD_CBIT_dis_lpmode_on_sagv_HSH           (0x012AE0C0)

  #define MC0_CH0_CR_SCHED_THIRD_CBIT_ignore_fragment_safe_in_ref_OFF  (22)
  #define MC0_CH0_CR_SCHED_THIRD_CBIT_ignore_fragment_safe_in_ref_WID  ( 1)
  #define MC0_CH0_CR_SCHED_THIRD_CBIT_ignore_fragment_safe_in_ref_MSK  (0x00400000)
  #define MC0_CH0_CR_SCHED_THIRD_CBIT_ignore_fragment_safe_in_ref_MIN  (0)
  #define MC0_CH0_CR_SCHED_THIRD_CBIT_ignore_fragment_safe_in_ref_MAX  (1) // 0x00000001
  #define MC0_CH0_CR_SCHED_THIRD_CBIT_ignore_fragment_safe_in_ref_DEF  (0x00000000)
  #define MC0_CH0_CR_SCHED_THIRD_CBIT_ignore_fragment_safe_in_ref_HSH  (0x012CE0C0)

  #define MC0_CH0_CR_SCHED_THIRD_CBIT_cmi_short_rmw_stall_OFF          (23)
  #define MC0_CH0_CR_SCHED_THIRD_CBIT_cmi_short_rmw_stall_WID          ( 1)
  #define MC0_CH0_CR_SCHED_THIRD_CBIT_cmi_short_rmw_stall_MSK          (0x00800000)
  #define MC0_CH0_CR_SCHED_THIRD_CBIT_cmi_short_rmw_stall_MIN          (0)
  #define MC0_CH0_CR_SCHED_THIRD_CBIT_cmi_short_rmw_stall_MAX          (1) // 0x00000001
  #define MC0_CH0_CR_SCHED_THIRD_CBIT_cmi_short_rmw_stall_DEF          (0x00000000)
  #define MC0_CH0_CR_SCHED_THIRD_CBIT_cmi_short_rmw_stall_HSH          (0x012EE0C0)

  #define MC0_CH0_CR_SCHED_THIRD_CBIT_ipq_ignore_write_ph_OFF          (24)
  #define MC0_CH0_CR_SCHED_THIRD_CBIT_ipq_ignore_write_ph_WID          ( 1)
  #define MC0_CH0_CR_SCHED_THIRD_CBIT_ipq_ignore_write_ph_MSK          (0x01000000)
  #define MC0_CH0_CR_SCHED_THIRD_CBIT_ipq_ignore_write_ph_MIN          (0)
  #define MC0_CH0_CR_SCHED_THIRD_CBIT_ipq_ignore_write_ph_MAX          (1) // 0x00000001
  #define MC0_CH0_CR_SCHED_THIRD_CBIT_ipq_ignore_write_ph_DEF          (0x00000000)
  #define MC0_CH0_CR_SCHED_THIRD_CBIT_ipq_ignore_write_ph_HSH          (0x0130E0C0)

  #define MC0_CH0_CR_SCHED_THIRD_CBIT_cksynccounteroverride_OFF        (25)
  #define MC0_CH0_CR_SCHED_THIRD_CBIT_cksynccounteroverride_WID        ( 1)
  #define MC0_CH0_CR_SCHED_THIRD_CBIT_cksynccounteroverride_MSK        (0x02000000)
  #define MC0_CH0_CR_SCHED_THIRD_CBIT_cksynccounteroverride_MIN        (0)
  #define MC0_CH0_CR_SCHED_THIRD_CBIT_cksynccounteroverride_MAX        (1) // 0x00000001
  #define MC0_CH0_CR_SCHED_THIRD_CBIT_cksynccounteroverride_DEF        (0x00000000)
  #define MC0_CH0_CR_SCHED_THIRD_CBIT_cksynccounteroverride_HSH        (0x0132E0C0)

  #define MC0_CH0_CR_SCHED_THIRD_CBIT_cksynccounteroverridevalue_OFF   (26)
  #define MC0_CH0_CR_SCHED_THIRD_CBIT_cksynccounteroverridevalue_WID   ( 2)
  #define MC0_CH0_CR_SCHED_THIRD_CBIT_cksynccounteroverridevalue_MSK   (0x0C000000)
  #define MC0_CH0_CR_SCHED_THIRD_CBIT_cksynccounteroverridevalue_MIN   (0)
  #define MC0_CH0_CR_SCHED_THIRD_CBIT_cksynccounteroverridevalue_MAX   (3) // 0x00000003
  #define MC0_CH0_CR_SCHED_THIRD_CBIT_cksynccounteroverridevalue_DEF   (0x00000000)
  #define MC0_CH0_CR_SCHED_THIRD_CBIT_cksynccounteroverridevalue_HSH   (0x0234E0C0)

  #define MC0_CH0_CR_SCHED_THIRD_CBIT_asdynamicmodeinsertbubble_OFF    (28)
  #define MC0_CH0_CR_SCHED_THIRD_CBIT_asdynamicmodeinsertbubble_WID    ( 1)
  #define MC0_CH0_CR_SCHED_THIRD_CBIT_asdynamicmodeinsertbubble_MSK    (0x10000000)
  #define MC0_CH0_CR_SCHED_THIRD_CBIT_asdynamicmodeinsertbubble_MIN    (0)
  #define MC0_CH0_CR_SCHED_THIRD_CBIT_asdynamicmodeinsertbubble_MAX    (1) // 0x00000001
  #define MC0_CH0_CR_SCHED_THIRD_CBIT_asdynamicmodeinsertbubble_DEF    (0x00000000)
  #define MC0_CH0_CR_SCHED_THIRD_CBIT_asdynamicmodeinsertbubble_HSH    (0x0138E0C0)

  #define MC0_CH0_CR_SCHED_THIRD_CBIT_dis_read_pre_in_vc0_read_starvation_OFF (29)
  #define MC0_CH0_CR_SCHED_THIRD_CBIT_dis_read_pre_in_vc0_read_starvation_WID ( 1)
  #define MC0_CH0_CR_SCHED_THIRD_CBIT_dis_read_pre_in_vc0_read_starvation_MSK (0x20000000)
  #define MC0_CH0_CR_SCHED_THIRD_CBIT_dis_read_pre_in_vc0_read_starvation_MIN (0)
  #define MC0_CH0_CR_SCHED_THIRD_CBIT_dis_read_pre_in_vc0_read_starvation_MAX (1) // 0x00000001
  #define MC0_CH0_CR_SCHED_THIRD_CBIT_dis_read_pre_in_vc0_read_starvation_DEF (0x00000000)
  #define MC0_CH0_CR_SCHED_THIRD_CBIT_dis_read_pre_in_vc0_read_starvation_HSH (0x013AE0C0)

  #define MC0_CH0_CR_SCHED_THIRD_CBIT_spare_OFF                        (30)
  #define MC0_CH0_CR_SCHED_THIRD_CBIT_spare_WID                        ( 2)
  #define MC0_CH0_CR_SCHED_THIRD_CBIT_spare_MSK                        (0xC0000000)
  #define MC0_CH0_CR_SCHED_THIRD_CBIT_spare_MIN                        (0)
  #define MC0_CH0_CR_SCHED_THIRD_CBIT_spare_MAX                        (3) // 0x00000003
  #define MC0_CH0_CR_SCHED_THIRD_CBIT_spare_DEF                        (0x00000000)
  #define MC0_CH0_CR_SCHED_THIRD_CBIT_spare_HSH                        (0x023CE0C0)

#define MC0_CH0_CR_DEADLOCK_BREAKER_REG                                (0x0000E0C4)

  #define MC0_CH0_CR_DEADLOCK_BREAKER_No_CAS_threshold_OFF             ( 0)
  #define MC0_CH0_CR_DEADLOCK_BREAKER_No_CAS_threshold_WID             (16)
  #define MC0_CH0_CR_DEADLOCK_BREAKER_No_CAS_threshold_MSK             (0x0000FFFF)
  #define MC0_CH0_CR_DEADLOCK_BREAKER_No_CAS_threshold_MIN             (0)
  #define MC0_CH0_CR_DEADLOCK_BREAKER_No_CAS_threshold_MAX             (65535) // 0x0000FFFF
  #define MC0_CH0_CR_DEADLOCK_BREAKER_No_CAS_threshold_DEF             (0x00000000)
  #define MC0_CH0_CR_DEADLOCK_BREAKER_No_CAS_threshold_HSH             (0x1000E0C4)

  #define MC0_CH0_CR_DEADLOCK_BREAKER_No_CAS_threshold_en_OFF          (16)
  #define MC0_CH0_CR_DEADLOCK_BREAKER_No_CAS_threshold_en_WID          ( 1)
  #define MC0_CH0_CR_DEADLOCK_BREAKER_No_CAS_threshold_en_MSK          (0x00010000)
  #define MC0_CH0_CR_DEADLOCK_BREAKER_No_CAS_threshold_en_MIN          (0)
  #define MC0_CH0_CR_DEADLOCK_BREAKER_No_CAS_threshold_en_MAX          (1) // 0x00000001
  #define MC0_CH0_CR_DEADLOCK_BREAKER_No_CAS_threshold_en_DEF          (0x00000000)
  #define MC0_CH0_CR_DEADLOCK_BREAKER_No_CAS_threshold_en_HSH          (0x0120E0C4)

  #define MC0_CH0_CR_DEADLOCK_BREAKER_Preemption_threshold_en_OFF      (17)
  #define MC0_CH0_CR_DEADLOCK_BREAKER_Preemption_threshold_en_WID      ( 1)
  #define MC0_CH0_CR_DEADLOCK_BREAKER_Preemption_threshold_en_MSK      (0x00020000)
  #define MC0_CH0_CR_DEADLOCK_BREAKER_Preemption_threshold_en_MIN      (0)
  #define MC0_CH0_CR_DEADLOCK_BREAKER_Preemption_threshold_en_MAX      (1) // 0x00000001
  #define MC0_CH0_CR_DEADLOCK_BREAKER_Preemption_threshold_en_DEF      (0x00000000)
  #define MC0_CH0_CR_DEADLOCK_BREAKER_Preemption_threshold_en_HSH      (0x0122E0C4)

  #define MC0_CH0_CR_DEADLOCK_BREAKER_Stop_count_during_tt_OFF         (18)
  #define MC0_CH0_CR_DEADLOCK_BREAKER_Stop_count_during_tt_WID         ( 1)
  #define MC0_CH0_CR_DEADLOCK_BREAKER_Stop_count_during_tt_MSK         (0x00040000)
  #define MC0_CH0_CR_DEADLOCK_BREAKER_Stop_count_during_tt_MIN         (0)
  #define MC0_CH0_CR_DEADLOCK_BREAKER_Stop_count_during_tt_MAX         (1) // 0x00000001
  #define MC0_CH0_CR_DEADLOCK_BREAKER_Stop_count_during_tt_DEF         (0x00000000)
  #define MC0_CH0_CR_DEADLOCK_BREAKER_Stop_count_during_tt_HSH         (0x0124E0C4)

  #define MC0_CH0_CR_DEADLOCK_BREAKER_Attempt_refresh_on_deadlock_OFF  (19)
  #define MC0_CH0_CR_DEADLOCK_BREAKER_Attempt_refresh_on_deadlock_WID  ( 1)
  #define MC0_CH0_CR_DEADLOCK_BREAKER_Attempt_refresh_on_deadlock_MSK  (0x00080000)
  #define MC0_CH0_CR_DEADLOCK_BREAKER_Attempt_refresh_on_deadlock_MIN  (0)
  #define MC0_CH0_CR_DEADLOCK_BREAKER_Attempt_refresh_on_deadlock_MAX  (1) // 0x00000001
  #define MC0_CH0_CR_DEADLOCK_BREAKER_Attempt_refresh_on_deadlock_DEF  (0x00000000)
  #define MC0_CH0_CR_DEADLOCK_BREAKER_Attempt_refresh_on_deadlock_HSH  (0x0126E0C4)

  #define MC0_CH0_CR_DEADLOCK_BREAKER_MajorMode_force_duration_OFF     (20)
  #define MC0_CH0_CR_DEADLOCK_BREAKER_MajorMode_force_duration_WID     ( 8)
  #define MC0_CH0_CR_DEADLOCK_BREAKER_MajorMode_force_duration_MSK     (0x0FF00000)
  #define MC0_CH0_CR_DEADLOCK_BREAKER_MajorMode_force_duration_MIN     (0)
  #define MC0_CH0_CR_DEADLOCK_BREAKER_MajorMode_force_duration_MAX     (255) // 0x000000FF
  #define MC0_CH0_CR_DEADLOCK_BREAKER_MajorMode_force_duration_DEF     (0x00000000)
  #define MC0_CH0_CR_DEADLOCK_BREAKER_MajorMode_force_duration_HSH     (0x0828E0C4)

  #define MC0_CH0_CR_DEADLOCK_BREAKER_MajorMode_force_en_OFF           (28)
  #define MC0_CH0_CR_DEADLOCK_BREAKER_MajorMode_force_en_WID           ( 1)
  #define MC0_CH0_CR_DEADLOCK_BREAKER_MajorMode_force_en_MSK           (0x10000000)
  #define MC0_CH0_CR_DEADLOCK_BREAKER_MajorMode_force_en_MIN           (0)
  #define MC0_CH0_CR_DEADLOCK_BREAKER_MajorMode_force_en_MAX           (1) // 0x00000001
  #define MC0_CH0_CR_DEADLOCK_BREAKER_MajorMode_force_en_DEF           (0x00000000)
  #define MC0_CH0_CR_DEADLOCK_BREAKER_MajorMode_force_en_HSH           (0x0138E0C4)

  #define MC0_CH0_CR_DEADLOCK_BREAKER_Disable_blocking_rules_OFF       (29)
  #define MC0_CH0_CR_DEADLOCK_BREAKER_Disable_blocking_rules_WID       ( 1)
  #define MC0_CH0_CR_DEADLOCK_BREAKER_Disable_blocking_rules_MSK       (0x20000000)
  #define MC0_CH0_CR_DEADLOCK_BREAKER_Disable_blocking_rules_MIN       (0)
  #define MC0_CH0_CR_DEADLOCK_BREAKER_Disable_blocking_rules_MAX       (1) // 0x00000001
  #define MC0_CH0_CR_DEADLOCK_BREAKER_Disable_blocking_rules_DEF       (0x00000000)
  #define MC0_CH0_CR_DEADLOCK_BREAKER_Disable_blocking_rules_HSH       (0x013AE0C4)

  #define MC0_CH0_CR_DEADLOCK_BREAKER_Rank_join_OFF                    (30)
  #define MC0_CH0_CR_DEADLOCK_BREAKER_Rank_join_WID                    ( 2)
  #define MC0_CH0_CR_DEADLOCK_BREAKER_Rank_join_MSK                    (0xC0000000)
  #define MC0_CH0_CR_DEADLOCK_BREAKER_Rank_join_MIN                    (0)
  #define MC0_CH0_CR_DEADLOCK_BREAKER_Rank_join_MAX                    (3) // 0x00000003
  #define MC0_CH0_CR_DEADLOCK_BREAKER_Rank_join_DEF                    (0x00000000)
  #define MC0_CH0_CR_DEADLOCK_BREAKER_Rank_join_HSH                    (0x023CE0C4)

#define MC0_CH0_CR_XARB_TC_BUBBLE_INJ_REG                              (0x0000E0C8)

  #define MC0_CH0_CR_XARB_TC_BUBBLE_INJ_max_tRDWR_OFF                  ( 0)
  #define MC0_CH0_CR_XARB_TC_BUBBLE_INJ_max_tRDWR_WID                  ( 7)
  #define MC0_CH0_CR_XARB_TC_BUBBLE_INJ_max_tRDWR_MSK                  (0x0000007F)
  #define MC0_CH0_CR_XARB_TC_BUBBLE_INJ_max_tRDWR_MIN                  (0)
  #define MC0_CH0_CR_XARB_TC_BUBBLE_INJ_max_tRDWR_MAX                  (127) // 0x0000007F
  #define MC0_CH0_CR_XARB_TC_BUBBLE_INJ_max_tRDWR_DEF                  (0x00000004)
  #define MC0_CH0_CR_XARB_TC_BUBBLE_INJ_max_tRDWR_HSH                  (0x4700E0C8)

  #define MC0_CH0_CR_XARB_TC_BUBBLE_INJ_max_tWRWR_OFF                  ( 8)
  #define MC0_CH0_CR_XARB_TC_BUBBLE_INJ_max_tWRWR_WID                  ( 7)
  #define MC0_CH0_CR_XARB_TC_BUBBLE_INJ_max_tWRWR_MSK                  (0x00007F00)
  #define MC0_CH0_CR_XARB_TC_BUBBLE_INJ_max_tWRWR_MIN                  (0)
  #define MC0_CH0_CR_XARB_TC_BUBBLE_INJ_max_tWRWR_MAX                  (127) // 0x0000007F
  #define MC0_CH0_CR_XARB_TC_BUBBLE_INJ_max_tWRWR_DEF                  (0x00000004)
  #define MC0_CH0_CR_XARB_TC_BUBBLE_INJ_max_tWRWR_HSH                  (0x4710E0C8)

  #define MC0_CH0_CR_XARB_TC_BUBBLE_INJ_max_tWRRD_OFF                  (16)
  #define MC0_CH0_CR_XARB_TC_BUBBLE_INJ_max_tWRRD_WID                  ( 9)
  #define MC0_CH0_CR_XARB_TC_BUBBLE_INJ_max_tWRRD_MSK                  (0x01FF0000)
  #define MC0_CH0_CR_XARB_TC_BUBBLE_INJ_max_tWRRD_MIN                  (0)
  #define MC0_CH0_CR_XARB_TC_BUBBLE_INJ_max_tWRRD_MAX                  (511) // 0x000001FF
  #define MC0_CH0_CR_XARB_TC_BUBBLE_INJ_max_tWRRD_DEF                  (0x00000004)
  #define MC0_CH0_CR_XARB_TC_BUBBLE_INJ_max_tWRRD_HSH                  (0x4920E0C8)

  #define MC0_CH0_CR_XARB_TC_BUBBLE_INJ_max_tRDRD_OFF                  (25)
  #define MC0_CH0_CR_XARB_TC_BUBBLE_INJ_max_tRDRD_WID                  ( 7)
  #define MC0_CH0_CR_XARB_TC_BUBBLE_INJ_max_tRDRD_MSK                  (0xFE000000)
  #define MC0_CH0_CR_XARB_TC_BUBBLE_INJ_max_tRDRD_MIN                  (0)
  #define MC0_CH0_CR_XARB_TC_BUBBLE_INJ_max_tRDRD_MAX                  (127) // 0x0000007F
  #define MC0_CH0_CR_XARB_TC_BUBBLE_INJ_max_tRDRD_DEF                  (0x00000004)
  #define MC0_CH0_CR_XARB_TC_BUBBLE_INJ_max_tRDRD_HSH                  (0x4732E0C8)

  #define MC0_CH0_CR_XARB_TC_BUBBLE_INJ_bubble_cnt_visa_out_bits01_OFF (61)
  #define MC0_CH0_CR_XARB_TC_BUBBLE_INJ_bubble_cnt_visa_out_bits01_WID ( 2)
  #define MC0_CH0_CR_XARB_TC_BUBBLE_INJ_bubble_cnt_visa_out_bits01_MSK (0x6000000000000000ULL)
  #define MC0_CH0_CR_XARB_TC_BUBBLE_INJ_bubble_cnt_visa_out_bits01_MIN (0)
  #define MC0_CH0_CR_XARB_TC_BUBBLE_INJ_bubble_cnt_visa_out_bits01_MAX (3) // 0x00000003
  #define MC0_CH0_CR_XARB_TC_BUBBLE_INJ_bubble_cnt_visa_out_bits01_DEF (0x00000000)
  #define MC0_CH0_CR_XARB_TC_BUBBLE_INJ_bubble_cnt_visa_out_bits01_HSH (0x427AE0C8)

  #define MC0_CH0_CR_XARB_TC_BUBBLE_INJ_bubble_cnt_visa_out_bit2_OFF   (63)
  #define MC0_CH0_CR_XARB_TC_BUBBLE_INJ_bubble_cnt_visa_out_bit2_WID   ( 1)
  #define MC0_CH0_CR_XARB_TC_BUBBLE_INJ_bubble_cnt_visa_out_bit2_MSK   (0x8000000000000000ULL)
  #define MC0_CH0_CR_XARB_TC_BUBBLE_INJ_bubble_cnt_visa_out_bit2_MIN   (0)
  #define MC0_CH0_CR_XARB_TC_BUBBLE_INJ_bubble_cnt_visa_out_bit2_MAX   (1) // 0x00000001
  #define MC0_CH0_CR_XARB_TC_BUBBLE_INJ_bubble_cnt_visa_out_bit2_DEF   (0x00000000)
  #define MC0_CH0_CR_XARB_TC_BUBBLE_INJ_bubble_cnt_visa_out_bit2_HSH   (0x417EE0C8)

#define MC0_CH0_CR_MCSCHEDS_GLOBAL_DRIVER_GATE_CFG_REG                 (0x0000E0D0)
//Duplicate of MC0_MCMAINS_GLOBAL_DRIVER_GATE_CFG_REG

#define MC0_CH0_CR_SC_BLOCKING_RULES_CFG_REG                           (0x0000E0D4)

  #define MC0_CH0_CR_SC_BLOCKING_RULES_CFG_dis_legacy_ph_blkr_OFF      ( 0)
  #define MC0_CH0_CR_SC_BLOCKING_RULES_CFG_dis_legacy_ph_blkr_WID      ( 1)
  #define MC0_CH0_CR_SC_BLOCKING_RULES_CFG_dis_legacy_ph_blkr_MSK      (0x00000001)
  #define MC0_CH0_CR_SC_BLOCKING_RULES_CFG_dis_legacy_ph_blkr_MIN      (0)
  #define MC0_CH0_CR_SC_BLOCKING_RULES_CFG_dis_legacy_ph_blkr_MAX      (1) // 0x00000001
  #define MC0_CH0_CR_SC_BLOCKING_RULES_CFG_dis_legacy_ph_blkr_DEF      (0x00000001)
  #define MC0_CH0_CR_SC_BLOCKING_RULES_CFG_dis_legacy_ph_blkr_HSH      (0x0100E0D4)

  #define MC0_CH0_CR_SC_BLOCKING_RULES_CFG_dis_legacy_pe_blkr_OFF      ( 1)
  #define MC0_CH0_CR_SC_BLOCKING_RULES_CFG_dis_legacy_pe_blkr_WID      ( 1)
  #define MC0_CH0_CR_SC_BLOCKING_RULES_CFG_dis_legacy_pe_blkr_MSK      (0x00000002)
  #define MC0_CH0_CR_SC_BLOCKING_RULES_CFG_dis_legacy_pe_blkr_MIN      (0)
  #define MC0_CH0_CR_SC_BLOCKING_RULES_CFG_dis_legacy_pe_blkr_MAX      (1) // 0x00000001
  #define MC0_CH0_CR_SC_BLOCKING_RULES_CFG_dis_legacy_pe_blkr_DEF      (0x00000001)
  #define MC0_CH0_CR_SC_BLOCKING_RULES_CFG_dis_legacy_pe_blkr_HSH      (0x0102E0D4)

  #define MC0_CH0_CR_SC_BLOCKING_RULES_CFG_low_wm_allowed_preempt_priorities_OFF ( 2)
  #define MC0_CH0_CR_SC_BLOCKING_RULES_CFG_low_wm_allowed_preempt_priorities_WID ( 3)
  #define MC0_CH0_CR_SC_BLOCKING_RULES_CFG_low_wm_allowed_preempt_priorities_MSK (0x0000001C)
  #define MC0_CH0_CR_SC_BLOCKING_RULES_CFG_low_wm_allowed_preempt_priorities_MIN (0)
  #define MC0_CH0_CR_SC_BLOCKING_RULES_CFG_low_wm_allowed_preempt_priorities_MAX (7) // 0x00000007
  #define MC0_CH0_CR_SC_BLOCKING_RULES_CFG_low_wm_allowed_preempt_priorities_DEF (0x00000007)
  #define MC0_CH0_CR_SC_BLOCKING_RULES_CFG_low_wm_allowed_preempt_priorities_HSH (0x0304E0D4)

  #define MC0_CH0_CR_SC_BLOCKING_RULES_CFG_med_wm_allowed_preempt_priorities_OFF ( 5)
  #define MC0_CH0_CR_SC_BLOCKING_RULES_CFG_med_wm_allowed_preempt_priorities_WID ( 3)
  #define MC0_CH0_CR_SC_BLOCKING_RULES_CFG_med_wm_allowed_preempt_priorities_MSK (0x000000E0)
  #define MC0_CH0_CR_SC_BLOCKING_RULES_CFG_med_wm_allowed_preempt_priorities_MIN (0)
  #define MC0_CH0_CR_SC_BLOCKING_RULES_CFG_med_wm_allowed_preempt_priorities_MAX (7) // 0x00000007
  #define MC0_CH0_CR_SC_BLOCKING_RULES_CFG_med_wm_allowed_preempt_priorities_DEF (0x00000006)
  #define MC0_CH0_CR_SC_BLOCKING_RULES_CFG_med_wm_allowed_preempt_priorities_HSH (0x030AE0D4)

  #define MC0_CH0_CR_SC_BLOCKING_RULES_CFG_high_wm_allowed_preempt_priorities_OFF ( 8)
  #define MC0_CH0_CR_SC_BLOCKING_RULES_CFG_high_wm_allowed_preempt_priorities_WID ( 3)
  #define MC0_CH0_CR_SC_BLOCKING_RULES_CFG_high_wm_allowed_preempt_priorities_MSK (0x00000700)
  #define MC0_CH0_CR_SC_BLOCKING_RULES_CFG_high_wm_allowed_preempt_priorities_MIN (0)
  #define MC0_CH0_CR_SC_BLOCKING_RULES_CFG_high_wm_allowed_preempt_priorities_MAX (7) // 0x00000007
  #define MC0_CH0_CR_SC_BLOCKING_RULES_CFG_high_wm_allowed_preempt_priorities_DEF (0x00000004)
  #define MC0_CH0_CR_SC_BLOCKING_RULES_CFG_high_wm_allowed_preempt_priorities_HSH (0x0310E0D4)

  #define MC0_CH0_CR_SC_BLOCKING_RULES_CFG_ph_block_pe_OFF             (11)
  #define MC0_CH0_CR_SC_BLOCKING_RULES_CFG_ph_block_pe_WID             ( 1)
  #define MC0_CH0_CR_SC_BLOCKING_RULES_CFG_ph_block_pe_MSK             (0x00000800)
  #define MC0_CH0_CR_SC_BLOCKING_RULES_CFG_ph_block_pe_MIN             (0)
  #define MC0_CH0_CR_SC_BLOCKING_RULES_CFG_ph_block_pe_MAX             (1) // 0x00000001
  #define MC0_CH0_CR_SC_BLOCKING_RULES_CFG_ph_block_pe_DEF             (0x00000001)
  #define MC0_CH0_CR_SC_BLOCKING_RULES_CFG_ph_block_pe_HSH             (0x0116E0D4)

  #define MC0_CH0_CR_SC_BLOCKING_RULES_CFG_blkr_effect_major_mode_OFF  (12)
  #define MC0_CH0_CR_SC_BLOCKING_RULES_CFG_blkr_effect_major_mode_WID  ( 1)
  #define MC0_CH0_CR_SC_BLOCKING_RULES_CFG_blkr_effect_major_mode_MSK  (0x00001000)
  #define MC0_CH0_CR_SC_BLOCKING_RULES_CFG_blkr_effect_major_mode_MIN  (0)
  #define MC0_CH0_CR_SC_BLOCKING_RULES_CFG_blkr_effect_major_mode_MAX  (1) // 0x00000001
  #define MC0_CH0_CR_SC_BLOCKING_RULES_CFG_blkr_effect_major_mode_DEF  (0x00000000)
  #define MC0_CH0_CR_SC_BLOCKING_RULES_CFG_blkr_effect_major_mode_HSH  (0x0118E0D4)

  #define MC0_CH0_CR_SC_BLOCKING_RULES_CFG_RIM_BW_optimization_OFF     (13)
  #define MC0_CH0_CR_SC_BLOCKING_RULES_CFG_RIM_BW_optimization_WID     ( 1)
  #define MC0_CH0_CR_SC_BLOCKING_RULES_CFG_RIM_BW_optimization_MSK     (0x00002000)
  #define MC0_CH0_CR_SC_BLOCKING_RULES_CFG_RIM_BW_optimization_MIN     (0)
  #define MC0_CH0_CR_SC_BLOCKING_RULES_CFG_RIM_BW_optimization_MAX     (1) // 0x00000001
  #define MC0_CH0_CR_SC_BLOCKING_RULES_CFG_RIM_BW_optimization_DEF     (0x00000001)
  #define MC0_CH0_CR_SC_BLOCKING_RULES_CFG_RIM_BW_optimization_HSH     (0x011AE0D4)

  #define MC0_CH0_CR_SC_BLOCKING_RULES_CFG_RIM_BW_prefer_VC1_OFF       (14)
  #define MC0_CH0_CR_SC_BLOCKING_RULES_CFG_RIM_BW_prefer_VC1_WID       ( 1)
  #define MC0_CH0_CR_SC_BLOCKING_RULES_CFG_RIM_BW_prefer_VC1_MSK       (0x00004000)
  #define MC0_CH0_CR_SC_BLOCKING_RULES_CFG_RIM_BW_prefer_VC1_MIN       (0)
  #define MC0_CH0_CR_SC_BLOCKING_RULES_CFG_RIM_BW_prefer_VC1_MAX       (1) // 0x00000001
  #define MC0_CH0_CR_SC_BLOCKING_RULES_CFG_RIM_BW_prefer_VC1_DEF       (0x00000001)
  #define MC0_CH0_CR_SC_BLOCKING_RULES_CFG_RIM_BW_prefer_VC1_HSH       (0x011CE0D4)
#define MC0_CH0_CR_PWM_DDR_SUBCH0_ACT_COUNTER_REG                      (0x0000E0D8)

  #define MC0_CH0_CR_PWM_DDR_SUBCH0_ACT_COUNTER_ACT_COUNTER_OFF        ( 0)
  #define MC0_CH0_CR_PWM_DDR_SUBCH0_ACT_COUNTER_ACT_COUNTER_WID        (64)
  #define MC0_CH0_CR_PWM_DDR_SUBCH0_ACT_COUNTER_ACT_COUNTER_MSK        (0xFFFFFFFFFFFFFFFFULL)
  #define MC0_CH0_CR_PWM_DDR_SUBCH0_ACT_COUNTER_ACT_COUNTER_MIN        (0)
  #define MC0_CH0_CR_PWM_DDR_SUBCH0_ACT_COUNTER_ACT_COUNTER_MAX        (18446744073709551615ULL) // 0xFFFFFFFFFFFFFFFF
  #define MC0_CH0_CR_PWM_DDR_SUBCH0_ACT_COUNTER_ACT_COUNTER_DEF        (0x00000000)
  #define MC0_CH0_CR_PWM_DDR_SUBCH0_ACT_COUNTER_ACT_COUNTER_HSH        (0x4000E0D8)

#define MC0_CH0_CR_PWM_DDR_SUBCH1_ACT_COUNTER_REG                      (0x0000E0E0)
//Duplicate of MC0_CH0_CR_PWM_DDR_SUBCH0_ACT_COUNTER_REG

#define MC0_CH0_CR_PWM_DDR_SUBCH0_REQ_OCCUPANCY_COUNTER_REG            (0x0000E0E8)

  #define MC0_CH0_CR_PWM_DDR_SUBCH0_REQ_OCCUPANCY_COUNTER_REQ_OCCUPANCY_COUNTER_OFF ( 0)
  #define MC0_CH0_CR_PWM_DDR_SUBCH0_REQ_OCCUPANCY_COUNTER_REQ_OCCUPANCY_COUNTER_WID (64)
  #define MC0_CH0_CR_PWM_DDR_SUBCH0_REQ_OCCUPANCY_COUNTER_REQ_OCCUPANCY_COUNTER_MSK (0xFFFFFFFFFFFFFFFFULL)
  #define MC0_CH0_CR_PWM_DDR_SUBCH0_REQ_OCCUPANCY_COUNTER_REQ_OCCUPANCY_COUNTER_MIN (0)
  #define MC0_CH0_CR_PWM_DDR_SUBCH0_REQ_OCCUPANCY_COUNTER_REQ_OCCUPANCY_COUNTER_MAX (18446744073709551615ULL) // 0xFFFFFFFFFFFFFFFF
  #define MC0_CH0_CR_PWM_DDR_SUBCH0_REQ_OCCUPANCY_COUNTER_REQ_OCCUPANCY_COUNTER_DEF (0x00000000)
  #define MC0_CH0_CR_PWM_DDR_SUBCH0_REQ_OCCUPANCY_COUNTER_REQ_OCCUPANCY_COUNTER_HSH (0x4000E0E8)

#define MC0_CH0_CR_PWM_DDR_SUBCH1_REQ_OCCUPANCY_COUNTER_REG            (0x0000E0F0)
//Duplicate of MC0_CH0_CR_PWM_DDR_SUBCH0_REQ_OCCUPANCY_COUNTER_REG

#define MC0_CH0_CR_WCK_CONFIG_REG                                      (0x0000E0F8)

  #define MC0_CH0_CR_WCK_CONFIG_LP5_WCK_MODE_OFF                       ( 0)
  #define MC0_CH0_CR_WCK_CONFIG_LP5_WCK_MODE_WID                       ( 2)
  #define MC0_CH0_CR_WCK_CONFIG_LP5_WCK_MODE_MSK                       (0x00000003)
  #define MC0_CH0_CR_WCK_CONFIG_LP5_WCK_MODE_MIN                       (0)
  #define MC0_CH0_CR_WCK_CONFIG_LP5_WCK_MODE_MAX                       (3) // 0x00000003
  #define MC0_CH0_CR_WCK_CONFIG_LP5_WCK_MODE_DEF                       (0x00000000)
  #define MC0_CH0_CR_WCK_CONFIG_LP5_WCK_MODE_HSH                       (0x4200E0F8)


  #define MC0_CH0_CR_WCK_CONFIG_RD_WCK_ASYNC_GAP_OFF                   ( 2)
  #define MC0_CH0_CR_WCK_CONFIG_RD_WCK_ASYNC_GAP_WID                   (10)
  #define MC0_CH0_CR_WCK_CONFIG_RD_WCK_ASYNC_GAP_MSK                   (0x00000FFC)
  #define MC0_CH0_CR_WCK_CONFIG_RD_WCK_ASYNC_GAP_MIN                   (0)
  #define MC0_CH0_CR_WCK_CONFIG_RD_WCK_ASYNC_GAP_MAX                   (1023) // 0x000003FF
  #define MC0_CH0_CR_WCK_CONFIG_RD_WCK_ASYNC_GAP_DEF                   (0x00000000)
  #define MC0_CH0_CR_WCK_CONFIG_RD_WCK_ASYNC_GAP_HSH                   (0x4A04E0F8)


  #define MC0_CH0_CR_WCK_CONFIG_WR_WCK_ASYNC_GAP_OFF                   (12)
  #define MC0_CH0_CR_WCK_CONFIG_WR_WCK_ASYNC_GAP_WID                   (10)
  #define MC0_CH0_CR_WCK_CONFIG_WR_WCK_ASYNC_GAP_MSK                   (0x003FF000)
  #define MC0_CH0_CR_WCK_CONFIG_WR_WCK_ASYNC_GAP_MIN                   (0)
  #define MC0_CH0_CR_WCK_CONFIG_WR_WCK_ASYNC_GAP_MAX                   (1023) // 0x000003FF
  #define MC0_CH0_CR_WCK_CONFIG_WR_WCK_ASYNC_GAP_DEF                   (0x00000000)
  #define MC0_CH0_CR_WCK_CONFIG_WR_WCK_ASYNC_GAP_HSH                   (0x4A18E0F8)


  #define MC0_CH0_CR_WCK_CONFIG_tCASSTOP_ADDITIONAL_GAP_OFF            (22)
  #define MC0_CH0_CR_WCK_CONFIG_tCASSTOP_ADDITIONAL_GAP_WID            ( 6)
  #define MC0_CH0_CR_WCK_CONFIG_tCASSTOP_ADDITIONAL_GAP_MSK            (0x0FC00000)
  #define MC0_CH0_CR_WCK_CONFIG_tCASSTOP_ADDITIONAL_GAP_MIN            (0)
  #define MC0_CH0_CR_WCK_CONFIG_tCASSTOP_ADDITIONAL_GAP_MAX            (63) // 0x0000003F
  #define MC0_CH0_CR_WCK_CONFIG_tCASSTOP_ADDITIONAL_GAP_DEF            (0x00000000)
  #define MC0_CH0_CR_WCK_CONFIG_tCASSTOP_ADDITIONAL_GAP_HSH            (0x462CE0F8)


  #define MC0_CH0_CR_WCK_CONFIG_WCK_MRR_SHORTER_BL_DIS_OFF             (28)
  #define MC0_CH0_CR_WCK_CONFIG_WCK_MRR_SHORTER_BL_DIS_WID             ( 1)
  #define MC0_CH0_CR_WCK_CONFIG_WCK_MRR_SHORTER_BL_DIS_MSK             (0x10000000)
  #define MC0_CH0_CR_WCK_CONFIG_WCK_MRR_SHORTER_BL_DIS_MIN             (0)
  #define MC0_CH0_CR_WCK_CONFIG_WCK_MRR_SHORTER_BL_DIS_MAX             (1) // 0x00000001
  #define MC0_CH0_CR_WCK_CONFIG_WCK_MRR_SHORTER_BL_DIS_DEF             (0x00000000)
  #define MC0_CH0_CR_WCK_CONFIG_WCK_MRR_SHORTER_BL_DIS_HSH             (0x4138E0F8)


  #define MC0_CH0_CR_WCK_CONFIG_WCK_FAST_MODE_OFF                      (29)
  #define MC0_CH0_CR_WCK_CONFIG_WCK_FAST_MODE_WID                      ( 1)
  #define MC0_CH0_CR_WCK_CONFIG_WCK_FAST_MODE_MSK                      (0x20000000)
  #define MC0_CH0_CR_WCK_CONFIG_WCK_FAST_MODE_MIN                      (0)
  #define MC0_CH0_CR_WCK_CONFIG_WCK_FAST_MODE_MAX                      (1) // 0x00000001
  #define MC0_CH0_CR_WCK_CONFIG_WCK_FAST_MODE_DEF                      (0x00000001)
  #define MC0_CH0_CR_WCK_CONFIG_WCK_FAST_MODE_HSH                      (0x413AE0F8)


  #define MC0_CH0_CR_WCK_CONFIG_tWCKPST_OFF                            (32)
  #define MC0_CH0_CR_WCK_CONFIG_tWCKPST_WID                            ( 5)
  #define MC0_CH0_CR_WCK_CONFIG_tWCKPST_MSK                            (0x0000001F00000000ULL)
  #define MC0_CH0_CR_WCK_CONFIG_tWCKPST_MIN                            (0)
  #define MC0_CH0_CR_WCK_CONFIG_tWCKPST_MAX                            (31) // 0x0000001F
  #define MC0_CH0_CR_WCK_CONFIG_tWCKPST_DEF                            (0x00000000)

  #define MC0_CH0_CR_WCK_CONFIG_tWCKPST_HSH                            (0x4540E0F8)


  #define MC0_CH0_CR_WCK_CONFIG_tWCKOFF_OFF                            (37)
  #define MC0_CH0_CR_WCK_CONFIG_tWCKOFF_WID                            ( 9)
  #define MC0_CH0_CR_WCK_CONFIG_tWCKOFF_MSK                            (0x00003FE000000000ULL)
  #define MC0_CH0_CR_WCK_CONFIG_tWCKOFF_MIN                            (0)
  #define MC0_CH0_CR_WCK_CONFIG_tWCKOFF_MAX                            (511) // 0x000001FF
  #define MC0_CH0_CR_WCK_CONFIG_tWCKOFF_DEF                            (0x00000000)
  #define MC0_CH0_CR_WCK_CONFIG_tWCKOFF_HSH                            (0x494AE0F8)


  #define MC0_CH0_CR_WCK_CONFIG_WCK_OFF_IDLE_TIMER_OFF                 (46)
  #define MC0_CH0_CR_WCK_CONFIG_WCK_OFF_IDLE_TIMER_WID                 (13)
  #define MC0_CH0_CR_WCK_CONFIG_WCK_OFF_IDLE_TIMER_MSK                 (0x07FFC00000000000ULL)
  #define MC0_CH0_CR_WCK_CONFIG_WCK_OFF_IDLE_TIMER_MIN                 (0)
  #define MC0_CH0_CR_WCK_CONFIG_WCK_OFF_IDLE_TIMER_MAX                 (8191) // 0x00001FFF
  #define MC0_CH0_CR_WCK_CONFIG_WCK_OFF_IDLE_TIMER_DEF                 (0x00000000)
  #define MC0_CH0_CR_WCK_CONFIG_WCK_OFF_IDLE_TIMER_HSH                 (0x4D5CE0F8)


  #define MC0_CH0_CR_WCK_CONFIG_WXSA_OFF                               (59)
  #define MC0_CH0_CR_WCK_CONFIG_WXSA_WID                               ( 2)
  #define MC0_CH0_CR_WCK_CONFIG_WXSA_MSK                               (0x1800000000000000ULL)
  #define MC0_CH0_CR_WCK_CONFIG_WXSA_MIN                               (0)
  #define MC0_CH0_CR_WCK_CONFIG_WXSA_MAX                               (3) // 0x00000003
  #define MC0_CH0_CR_WCK_CONFIG_WXSA_DEF                               (0x00000000)
  #define MC0_CH0_CR_WCK_CONFIG_WXSA_HSH                               (0x4276E0F8)


  #define MC0_CH0_CR_WCK_CONFIG_WXSB_OFF                               (61)
  #define MC0_CH0_CR_WCK_CONFIG_WXSB_WID                               ( 2)
  #define MC0_CH0_CR_WCK_CONFIG_WXSB_MSK                               (0x6000000000000000ULL)
  #define MC0_CH0_CR_WCK_CONFIG_WXSB_MIN                               (0)
  #define MC0_CH0_CR_WCK_CONFIG_WXSB_MAX                               (3) // 0x00000003
  #define MC0_CH0_CR_WCK_CONFIG_WXSB_DEF                               (0x00000000)
  #define MC0_CH0_CR_WCK_CONFIG_WXSB_HSH                               (0x427AE0F8)


#define MC0_CH0_CR_XARB_CFG_BUBBLE_INJ_REG                             (0x0000E100)

  #define MC0_CH0_CR_XARB_CFG_BUBBLE_INJ_bubble_inj_enable_read_switch_OFF ( 0)
  #define MC0_CH0_CR_XARB_CFG_BUBBLE_INJ_bubble_inj_enable_read_switch_WID ( 1)
  #define MC0_CH0_CR_XARB_CFG_BUBBLE_INJ_bubble_inj_enable_read_switch_MSK (0x00000001)
  #define MC0_CH0_CR_XARB_CFG_BUBBLE_INJ_bubble_inj_enable_read_switch_MIN (0)
  #define MC0_CH0_CR_XARB_CFG_BUBBLE_INJ_bubble_inj_enable_read_switch_MAX (1) // 0x00000001
  #define MC0_CH0_CR_XARB_CFG_BUBBLE_INJ_bubble_inj_enable_read_switch_DEF (0x00000001)
  #define MC0_CH0_CR_XARB_CFG_BUBBLE_INJ_bubble_inj_enable_read_switch_HSH (0x0100E100)

  #define MC0_CH0_CR_XARB_CFG_BUBBLE_INJ_bubble_inj_enable_write_switch_OFF ( 1)
  #define MC0_CH0_CR_XARB_CFG_BUBBLE_INJ_bubble_inj_enable_write_switch_WID ( 1)
  #define MC0_CH0_CR_XARB_CFG_BUBBLE_INJ_bubble_inj_enable_write_switch_MSK (0x00000002)
  #define MC0_CH0_CR_XARB_CFG_BUBBLE_INJ_bubble_inj_enable_write_switch_MIN (0)
  #define MC0_CH0_CR_XARB_CFG_BUBBLE_INJ_bubble_inj_enable_write_switch_MAX (1) // 0x00000001
  #define MC0_CH0_CR_XARB_CFG_BUBBLE_INJ_bubble_inj_enable_write_switch_DEF (0x00000001)
  #define MC0_CH0_CR_XARB_CFG_BUBBLE_INJ_bubble_inj_enable_write_switch_HSH (0x0102E100)

  #define MC0_CH0_CR_XARB_CFG_BUBBLE_INJ_bubble_inj_enable_any_cas_switch_OFF ( 2)
  #define MC0_CH0_CR_XARB_CFG_BUBBLE_INJ_bubble_inj_enable_any_cas_switch_WID ( 1)
  #define MC0_CH0_CR_XARB_CFG_BUBBLE_INJ_bubble_inj_enable_any_cas_switch_MSK (0x00000004)
  #define MC0_CH0_CR_XARB_CFG_BUBBLE_INJ_bubble_inj_enable_any_cas_switch_MIN (0)
  #define MC0_CH0_CR_XARB_CFG_BUBBLE_INJ_bubble_inj_enable_any_cas_switch_MAX (1) // 0x00000001
  #define MC0_CH0_CR_XARB_CFG_BUBBLE_INJ_bubble_inj_enable_any_cas_switch_DEF (0x00000001)
  #define MC0_CH0_CR_XARB_CFG_BUBBLE_INJ_bubble_inj_enable_any_cas_switch_HSH (0x0104E100)

  #define MC0_CH0_CR_XARB_CFG_BUBBLE_INJ_Rank_mode_OFF                 ( 3)
  #define MC0_CH0_CR_XARB_CFG_BUBBLE_INJ_Rank_mode_WID                 ( 1)
  #define MC0_CH0_CR_XARB_CFG_BUBBLE_INJ_Rank_mode_MSK                 (0x00000008)
  #define MC0_CH0_CR_XARB_CFG_BUBBLE_INJ_Rank_mode_MIN                 (0)
  #define MC0_CH0_CR_XARB_CFG_BUBBLE_INJ_Rank_mode_MAX                 (1) // 0x00000001
  #define MC0_CH0_CR_XARB_CFG_BUBBLE_INJ_Rank_mode_DEF                 (0x00000001)
  #define MC0_CH0_CR_XARB_CFG_BUBBLE_INJ_Rank_mode_HSH                 (0x0106E100)

  #define MC0_CH0_CR_XARB_CFG_BUBBLE_INJ_DIMM_mode_OFF                 ( 4)
  #define MC0_CH0_CR_XARB_CFG_BUBBLE_INJ_DIMM_mode_WID                 ( 1)
  #define MC0_CH0_CR_XARB_CFG_BUBBLE_INJ_DIMM_mode_MSK                 (0x00000010)
  #define MC0_CH0_CR_XARB_CFG_BUBBLE_INJ_DIMM_mode_MIN                 (0)
  #define MC0_CH0_CR_XARB_CFG_BUBBLE_INJ_DIMM_mode_MAX                 (1) // 0x00000001
  #define MC0_CH0_CR_XARB_CFG_BUBBLE_INJ_DIMM_mode_DEF                 (0x00000000)
  #define MC0_CH0_CR_XARB_CFG_BUBBLE_INJ_DIMM_mode_HSH                 (0x0108E100)

  #define MC0_CH0_CR_XARB_CFG_BUBBLE_INJ_DIMM_mode_multiplier_OFF      ( 5)
  #define MC0_CH0_CR_XARB_CFG_BUBBLE_INJ_DIMM_mode_multiplier_WID      ( 2)
  #define MC0_CH0_CR_XARB_CFG_BUBBLE_INJ_DIMM_mode_multiplier_MSK      (0x00000060)
  #define MC0_CH0_CR_XARB_CFG_BUBBLE_INJ_DIMM_mode_multiplier_MIN      (0)
  #define MC0_CH0_CR_XARB_CFG_BUBBLE_INJ_DIMM_mode_multiplier_MAX      (3) // 0x00000003
  #define MC0_CH0_CR_XARB_CFG_BUBBLE_INJ_DIMM_mode_multiplier_DEF      (0x00000001)
  #define MC0_CH0_CR_XARB_CFG_BUBBLE_INJ_DIMM_mode_multiplier_HSH      (0x020AE100)

  #define MC0_CH0_CR_XARB_CFG_BUBBLE_INJ_Dis_MRR_Bubble_OFF            ( 7)
  #define MC0_CH0_CR_XARB_CFG_BUBBLE_INJ_Dis_MRR_Bubble_WID            ( 1)
  #define MC0_CH0_CR_XARB_CFG_BUBBLE_INJ_Dis_MRR_Bubble_MSK            (0x00000080)
  #define MC0_CH0_CR_XARB_CFG_BUBBLE_INJ_Dis_MRR_Bubble_MIN            (0)
  #define MC0_CH0_CR_XARB_CFG_BUBBLE_INJ_Dis_MRR_Bubble_MAX            (1) // 0x00000001
  #define MC0_CH0_CR_XARB_CFG_BUBBLE_INJ_Dis_MRR_Bubble_DEF            (0x00000000)
  #define MC0_CH0_CR_XARB_CFG_BUBBLE_INJ_Dis_MRR_Bubble_HSH            (0x010EE100)

  #define MC0_CH0_CR_XARB_CFG_BUBBLE_INJ_bubble_inj_read_switch_num_cas_OFF ( 8)
  #define MC0_CH0_CR_XARB_CFG_BUBBLE_INJ_bubble_inj_read_switch_num_cas_WID ( 8)
  #define MC0_CH0_CR_XARB_CFG_BUBBLE_INJ_bubble_inj_read_switch_num_cas_MSK (0x0000FF00)
  #define MC0_CH0_CR_XARB_CFG_BUBBLE_INJ_bubble_inj_read_switch_num_cas_MIN (0)
  #define MC0_CH0_CR_XARB_CFG_BUBBLE_INJ_bubble_inj_read_switch_num_cas_MAX (255) // 0x000000FF
  #define MC0_CH0_CR_XARB_CFG_BUBBLE_INJ_bubble_inj_read_switch_num_cas_DEF (0x00000020)
  #define MC0_CH0_CR_XARB_CFG_BUBBLE_INJ_bubble_inj_read_switch_num_cas_HSH (0x0810E100)

  #define MC0_CH0_CR_XARB_CFG_BUBBLE_INJ_bubble_inj_write_switch_num_cas_OFF (16)
  #define MC0_CH0_CR_XARB_CFG_BUBBLE_INJ_bubble_inj_write_switch_num_cas_WID ( 8)
  #define MC0_CH0_CR_XARB_CFG_BUBBLE_INJ_bubble_inj_write_switch_num_cas_MSK (0x00FF0000)
  #define MC0_CH0_CR_XARB_CFG_BUBBLE_INJ_bubble_inj_write_switch_num_cas_MIN (0)
  #define MC0_CH0_CR_XARB_CFG_BUBBLE_INJ_bubble_inj_write_switch_num_cas_MAX (255) // 0x000000FF
  #define MC0_CH0_CR_XARB_CFG_BUBBLE_INJ_bubble_inj_write_switch_num_cas_DEF (0x00000020)
  #define MC0_CH0_CR_XARB_CFG_BUBBLE_INJ_bubble_inj_write_switch_num_cas_HSH (0x0820E100)

  #define MC0_CH0_CR_XARB_CFG_BUBBLE_INJ_bubble_inj_any_cas_switch_num_cas_OFF (24)
  #define MC0_CH0_CR_XARB_CFG_BUBBLE_INJ_bubble_inj_any_cas_switch_num_cas_WID ( 8)
  #define MC0_CH0_CR_XARB_CFG_BUBBLE_INJ_bubble_inj_any_cas_switch_num_cas_MSK (0xFF000000)
  #define MC0_CH0_CR_XARB_CFG_BUBBLE_INJ_bubble_inj_any_cas_switch_num_cas_MIN (0)
  #define MC0_CH0_CR_XARB_CFG_BUBBLE_INJ_bubble_inj_any_cas_switch_num_cas_MAX (255) // 0x000000FF
  #define MC0_CH0_CR_XARB_CFG_BUBBLE_INJ_bubble_inj_any_cas_switch_num_cas_DEF (0x00000080)
  #define MC0_CH0_CR_XARB_CFG_BUBBLE_INJ_bubble_inj_any_cas_switch_num_cas_HSH (0x0830E100)

#define MC0_CH0_CR_TR_RRDVALID_CTRL_REG                                (0x0000E104)

  #define MC0_CH0_CR_TR_RRDVALID_CTRL_Rank_0_trigger_OFF               ( 0)
  #define MC0_CH0_CR_TR_RRDVALID_CTRL_Rank_0_trigger_WID               ( 1)
  #define MC0_CH0_CR_TR_RRDVALID_CTRL_Rank_0_trigger_MSK               (0x00000001)
  #define MC0_CH0_CR_TR_RRDVALID_CTRL_Rank_0_trigger_MIN               (0)
  #define MC0_CH0_CR_TR_RRDVALID_CTRL_Rank_0_trigger_MAX               (1) // 0x00000001
  #define MC0_CH0_CR_TR_RRDVALID_CTRL_Rank_0_trigger_DEF               (0x00000000)
  #define MC0_CH0_CR_TR_RRDVALID_CTRL_Rank_0_trigger_HSH               (0x0100E104)

  #define MC0_CH0_CR_TR_RRDVALID_CTRL_Rank_0_wr_en_OFF                 ( 1)
  #define MC0_CH0_CR_TR_RRDVALID_CTRL_Rank_0_wr_en_WID                 ( 1)
  #define MC0_CH0_CR_TR_RRDVALID_CTRL_Rank_0_wr_en_MSK                 (0x00000002)
  #define MC0_CH0_CR_TR_RRDVALID_CTRL_Rank_0_wr_en_MIN                 (0)
  #define MC0_CH0_CR_TR_RRDVALID_CTRL_Rank_0_wr_en_MAX                 (1) // 0x00000001
  #define MC0_CH0_CR_TR_RRDVALID_CTRL_Rank_0_wr_en_DEF                 (0x00000000)
  #define MC0_CH0_CR_TR_RRDVALID_CTRL_Rank_0_wr_en_HSH                 (0x0102E104)

  #define MC0_CH0_CR_TR_RRDVALID_CTRL_Rank_0_overflow_OFF              ( 2)
  #define MC0_CH0_CR_TR_RRDVALID_CTRL_Rank_0_overflow_WID              ( 1)
  #define MC0_CH0_CR_TR_RRDVALID_CTRL_Rank_0_overflow_MSK              (0x00000004)
  #define MC0_CH0_CR_TR_RRDVALID_CTRL_Rank_0_overflow_MIN              (0)
  #define MC0_CH0_CR_TR_RRDVALID_CTRL_Rank_0_overflow_MAX              (1) // 0x00000001
  #define MC0_CH0_CR_TR_RRDVALID_CTRL_Rank_0_overflow_DEF              (0x00000000)
  #define MC0_CH0_CR_TR_RRDVALID_CTRL_Rank_0_overflow_HSH              (0x0104E104)

  #define MC0_CH0_CR_TR_RRDVALID_CTRL_Rank_1_trigger_OFF               ( 3)
  #define MC0_CH0_CR_TR_RRDVALID_CTRL_Rank_1_trigger_WID               ( 1)
  #define MC0_CH0_CR_TR_RRDVALID_CTRL_Rank_1_trigger_MSK               (0x00000008)
  #define MC0_CH0_CR_TR_RRDVALID_CTRL_Rank_1_trigger_MIN               (0)
  #define MC0_CH0_CR_TR_RRDVALID_CTRL_Rank_1_trigger_MAX               (1) // 0x00000001
  #define MC0_CH0_CR_TR_RRDVALID_CTRL_Rank_1_trigger_DEF               (0x00000000)
  #define MC0_CH0_CR_TR_RRDVALID_CTRL_Rank_1_trigger_HSH               (0x0106E104)

  #define MC0_CH0_CR_TR_RRDVALID_CTRL_Rank_1_wr_en_OFF                 ( 4)
  #define MC0_CH0_CR_TR_RRDVALID_CTRL_Rank_1_wr_en_WID                 ( 1)
  #define MC0_CH0_CR_TR_RRDVALID_CTRL_Rank_1_wr_en_MSK                 (0x00000010)
  #define MC0_CH0_CR_TR_RRDVALID_CTRL_Rank_1_wr_en_MIN                 (0)
  #define MC0_CH0_CR_TR_RRDVALID_CTRL_Rank_1_wr_en_MAX                 (1) // 0x00000001
  #define MC0_CH0_CR_TR_RRDVALID_CTRL_Rank_1_wr_en_DEF                 (0x00000000)
  #define MC0_CH0_CR_TR_RRDVALID_CTRL_Rank_1_wr_en_HSH                 (0x0108E104)

  #define MC0_CH0_CR_TR_RRDVALID_CTRL_Rank_1_overflow_OFF              ( 5)
  #define MC0_CH0_CR_TR_RRDVALID_CTRL_Rank_1_overflow_WID              ( 1)
  #define MC0_CH0_CR_TR_RRDVALID_CTRL_Rank_1_overflow_MSK              (0x00000020)
  #define MC0_CH0_CR_TR_RRDVALID_CTRL_Rank_1_overflow_MIN              (0)
  #define MC0_CH0_CR_TR_RRDVALID_CTRL_Rank_1_overflow_MAX              (1) // 0x00000001
  #define MC0_CH0_CR_TR_RRDVALID_CTRL_Rank_1_overflow_DEF              (0x00000000)
  #define MC0_CH0_CR_TR_RRDVALID_CTRL_Rank_1_overflow_HSH              (0x010AE104)

  #define MC0_CH0_CR_TR_RRDVALID_CTRL_Rank_2_trigger_OFF               ( 6)
  #define MC0_CH0_CR_TR_RRDVALID_CTRL_Rank_2_trigger_WID               ( 1)
  #define MC0_CH0_CR_TR_RRDVALID_CTRL_Rank_2_trigger_MSK               (0x00000040)
  #define MC0_CH0_CR_TR_RRDVALID_CTRL_Rank_2_trigger_MIN               (0)
  #define MC0_CH0_CR_TR_RRDVALID_CTRL_Rank_2_trigger_MAX               (1) // 0x00000001
  #define MC0_CH0_CR_TR_RRDVALID_CTRL_Rank_2_trigger_DEF               (0x00000000)
  #define MC0_CH0_CR_TR_RRDVALID_CTRL_Rank_2_trigger_HSH               (0x010CE104)

  #define MC0_CH0_CR_TR_RRDVALID_CTRL_Rank_2_wr_en_OFF                 ( 7)
  #define MC0_CH0_CR_TR_RRDVALID_CTRL_Rank_2_wr_en_WID                 ( 1)
  #define MC0_CH0_CR_TR_RRDVALID_CTRL_Rank_2_wr_en_MSK                 (0x00000080)
  #define MC0_CH0_CR_TR_RRDVALID_CTRL_Rank_2_wr_en_MIN                 (0)
  #define MC0_CH0_CR_TR_RRDVALID_CTRL_Rank_2_wr_en_MAX                 (1) // 0x00000001
  #define MC0_CH0_CR_TR_RRDVALID_CTRL_Rank_2_wr_en_DEF                 (0x00000000)
  #define MC0_CH0_CR_TR_RRDVALID_CTRL_Rank_2_wr_en_HSH                 (0x010EE104)

  #define MC0_CH0_CR_TR_RRDVALID_CTRL_Rank_2_overflow_OFF              ( 8)
  #define MC0_CH0_CR_TR_RRDVALID_CTRL_Rank_2_overflow_WID              ( 1)
  #define MC0_CH0_CR_TR_RRDVALID_CTRL_Rank_2_overflow_MSK              (0x00000100)
  #define MC0_CH0_CR_TR_RRDVALID_CTRL_Rank_2_overflow_MIN              (0)
  #define MC0_CH0_CR_TR_RRDVALID_CTRL_Rank_2_overflow_MAX              (1) // 0x00000001
  #define MC0_CH0_CR_TR_RRDVALID_CTRL_Rank_2_overflow_DEF              (0x00000000)
  #define MC0_CH0_CR_TR_RRDVALID_CTRL_Rank_2_overflow_HSH              (0x0110E104)

  #define MC0_CH0_CR_TR_RRDVALID_CTRL_Rank_3_trigger_OFF               ( 9)
  #define MC0_CH0_CR_TR_RRDVALID_CTRL_Rank_3_trigger_WID               ( 1)
  #define MC0_CH0_CR_TR_RRDVALID_CTRL_Rank_3_trigger_MSK               (0x00000200)
  #define MC0_CH0_CR_TR_RRDVALID_CTRL_Rank_3_trigger_MIN               (0)
  #define MC0_CH0_CR_TR_RRDVALID_CTRL_Rank_3_trigger_MAX               (1) // 0x00000001
  #define MC0_CH0_CR_TR_RRDVALID_CTRL_Rank_3_trigger_DEF               (0x00000000)
  #define MC0_CH0_CR_TR_RRDVALID_CTRL_Rank_3_trigger_HSH               (0x0112E104)

  #define MC0_CH0_CR_TR_RRDVALID_CTRL_Rank_3_wr_en_OFF                 (10)
  #define MC0_CH0_CR_TR_RRDVALID_CTRL_Rank_3_wr_en_WID                 ( 1)
  #define MC0_CH0_CR_TR_RRDVALID_CTRL_Rank_3_wr_en_MSK                 (0x00000400)
  #define MC0_CH0_CR_TR_RRDVALID_CTRL_Rank_3_wr_en_MIN                 (0)
  #define MC0_CH0_CR_TR_RRDVALID_CTRL_Rank_3_wr_en_MAX                 (1) // 0x00000001
  #define MC0_CH0_CR_TR_RRDVALID_CTRL_Rank_3_wr_en_DEF                 (0x00000000)
  #define MC0_CH0_CR_TR_RRDVALID_CTRL_Rank_3_wr_en_HSH                 (0x0114E104)

  #define MC0_CH0_CR_TR_RRDVALID_CTRL_Rank_3_overflow_OFF              (11)
  #define MC0_CH0_CR_TR_RRDVALID_CTRL_Rank_3_overflow_WID              ( 1)
  #define MC0_CH0_CR_TR_RRDVALID_CTRL_Rank_3_overflow_MSK              (0x00000800)
  #define MC0_CH0_CR_TR_RRDVALID_CTRL_Rank_3_overflow_MIN              (0)
  #define MC0_CH0_CR_TR_RRDVALID_CTRL_Rank_3_overflow_MAX              (1) // 0x00000001
  #define MC0_CH0_CR_TR_RRDVALID_CTRL_Rank_3_overflow_DEF              (0x00000000)
  #define MC0_CH0_CR_TR_RRDVALID_CTRL_Rank_3_overflow_HSH              (0x0116E104)

  #define MC0_CH0_CR_TR_RRDVALID_CTRL_Rank_4_trigger_OFF               (12)
  #define MC0_CH0_CR_TR_RRDVALID_CTRL_Rank_4_trigger_WID               ( 1)
  #define MC0_CH0_CR_TR_RRDVALID_CTRL_Rank_4_trigger_MSK               (0x00001000)
  #define MC0_CH0_CR_TR_RRDVALID_CTRL_Rank_4_trigger_MIN               (0)
  #define MC0_CH0_CR_TR_RRDVALID_CTRL_Rank_4_trigger_MAX               (1) // 0x00000001
  #define MC0_CH0_CR_TR_RRDVALID_CTRL_Rank_4_trigger_DEF               (0x00000000)
  #define MC0_CH0_CR_TR_RRDVALID_CTRL_Rank_4_trigger_HSH               (0x0118E104)

  #define MC0_CH0_CR_TR_RRDVALID_CTRL_Rank_4_wr_en_OFF                 (13)
  #define MC0_CH0_CR_TR_RRDVALID_CTRL_Rank_4_wr_en_WID                 ( 1)
  #define MC0_CH0_CR_TR_RRDVALID_CTRL_Rank_4_wr_en_MSK                 (0x00002000)
  #define MC0_CH0_CR_TR_RRDVALID_CTRL_Rank_4_wr_en_MIN                 (0)
  #define MC0_CH0_CR_TR_RRDVALID_CTRL_Rank_4_wr_en_MAX                 (1) // 0x00000001
  #define MC0_CH0_CR_TR_RRDVALID_CTRL_Rank_4_wr_en_DEF                 (0x00000000)
  #define MC0_CH0_CR_TR_RRDVALID_CTRL_Rank_4_wr_en_HSH                 (0x011AE104)

  #define MC0_CH0_CR_TR_RRDVALID_CTRL_Rank_4_overflow_OFF              (14)
  #define MC0_CH0_CR_TR_RRDVALID_CTRL_Rank_4_overflow_WID              ( 1)
  #define MC0_CH0_CR_TR_RRDVALID_CTRL_Rank_4_overflow_MSK              (0x00004000)
  #define MC0_CH0_CR_TR_RRDVALID_CTRL_Rank_4_overflow_MIN              (0)
  #define MC0_CH0_CR_TR_RRDVALID_CTRL_Rank_4_overflow_MAX              (1) // 0x00000001
  #define MC0_CH0_CR_TR_RRDVALID_CTRL_Rank_4_overflow_DEF              (0x00000000)
  #define MC0_CH0_CR_TR_RRDVALID_CTRL_Rank_4_overflow_HSH              (0x011CE104)

  #define MC0_CH0_CR_TR_RRDVALID_CTRL_Rank_5_trigger_OFF               (15)
  #define MC0_CH0_CR_TR_RRDVALID_CTRL_Rank_5_trigger_WID               ( 1)
  #define MC0_CH0_CR_TR_RRDVALID_CTRL_Rank_5_trigger_MSK               (0x00008000)
  #define MC0_CH0_CR_TR_RRDVALID_CTRL_Rank_5_trigger_MIN               (0)
  #define MC0_CH0_CR_TR_RRDVALID_CTRL_Rank_5_trigger_MAX               (1) // 0x00000001
  #define MC0_CH0_CR_TR_RRDVALID_CTRL_Rank_5_trigger_DEF               (0x00000000)
  #define MC0_CH0_CR_TR_RRDVALID_CTRL_Rank_5_trigger_HSH               (0x011EE104)

  #define MC0_CH0_CR_TR_RRDVALID_CTRL_Rank_5_wr_en_OFF                 (16)
  #define MC0_CH0_CR_TR_RRDVALID_CTRL_Rank_5_wr_en_WID                 ( 1)
  #define MC0_CH0_CR_TR_RRDVALID_CTRL_Rank_5_wr_en_MSK                 (0x00010000)
  #define MC0_CH0_CR_TR_RRDVALID_CTRL_Rank_5_wr_en_MIN                 (0)
  #define MC0_CH0_CR_TR_RRDVALID_CTRL_Rank_5_wr_en_MAX                 (1) // 0x00000001
  #define MC0_CH0_CR_TR_RRDVALID_CTRL_Rank_5_wr_en_DEF                 (0x00000000)
  #define MC0_CH0_CR_TR_RRDVALID_CTRL_Rank_5_wr_en_HSH                 (0x0120E104)

  #define MC0_CH0_CR_TR_RRDVALID_CTRL_Rank_5_overflow_OFF              (17)
  #define MC0_CH0_CR_TR_RRDVALID_CTRL_Rank_5_overflow_WID              ( 1)
  #define MC0_CH0_CR_TR_RRDVALID_CTRL_Rank_5_overflow_MSK              (0x00020000)
  #define MC0_CH0_CR_TR_RRDVALID_CTRL_Rank_5_overflow_MIN              (0)
  #define MC0_CH0_CR_TR_RRDVALID_CTRL_Rank_5_overflow_MAX              (1) // 0x00000001
  #define MC0_CH0_CR_TR_RRDVALID_CTRL_Rank_5_overflow_DEF              (0x00000000)
  #define MC0_CH0_CR_TR_RRDVALID_CTRL_Rank_5_overflow_HSH              (0x0122E104)

  #define MC0_CH0_CR_TR_RRDVALID_CTRL_Rank_6_trigger_OFF               (18)
  #define MC0_CH0_CR_TR_RRDVALID_CTRL_Rank_6_trigger_WID               ( 1)
  #define MC0_CH0_CR_TR_RRDVALID_CTRL_Rank_6_trigger_MSK               (0x00040000)
  #define MC0_CH0_CR_TR_RRDVALID_CTRL_Rank_6_trigger_MIN               (0)
  #define MC0_CH0_CR_TR_RRDVALID_CTRL_Rank_6_trigger_MAX               (1) // 0x00000001
  #define MC0_CH0_CR_TR_RRDVALID_CTRL_Rank_6_trigger_DEF               (0x00000000)
  #define MC0_CH0_CR_TR_RRDVALID_CTRL_Rank_6_trigger_HSH               (0x0124E104)

  #define MC0_CH0_CR_TR_RRDVALID_CTRL_Rank_6_wr_en_OFF                 (19)
  #define MC0_CH0_CR_TR_RRDVALID_CTRL_Rank_6_wr_en_WID                 ( 1)
  #define MC0_CH0_CR_TR_RRDVALID_CTRL_Rank_6_wr_en_MSK                 (0x00080000)
  #define MC0_CH0_CR_TR_RRDVALID_CTRL_Rank_6_wr_en_MIN                 (0)
  #define MC0_CH0_CR_TR_RRDVALID_CTRL_Rank_6_wr_en_MAX                 (1) // 0x00000001
  #define MC0_CH0_CR_TR_RRDVALID_CTRL_Rank_6_wr_en_DEF                 (0x00000000)
  #define MC0_CH0_CR_TR_RRDVALID_CTRL_Rank_6_wr_en_HSH                 (0x0126E104)

  #define MC0_CH0_CR_TR_RRDVALID_CTRL_Rank_6_overflow_OFF              (20)
  #define MC0_CH0_CR_TR_RRDVALID_CTRL_Rank_6_overflow_WID              ( 1)
  #define MC0_CH0_CR_TR_RRDVALID_CTRL_Rank_6_overflow_MSK              (0x00100000)
  #define MC0_CH0_CR_TR_RRDVALID_CTRL_Rank_6_overflow_MIN              (0)
  #define MC0_CH0_CR_TR_RRDVALID_CTRL_Rank_6_overflow_MAX              (1) // 0x00000001
  #define MC0_CH0_CR_TR_RRDVALID_CTRL_Rank_6_overflow_DEF              (0x00000000)
  #define MC0_CH0_CR_TR_RRDVALID_CTRL_Rank_6_overflow_HSH              (0x0128E104)

  #define MC0_CH0_CR_TR_RRDVALID_CTRL_Rank_7_trigger_OFF               (21)
  #define MC0_CH0_CR_TR_RRDVALID_CTRL_Rank_7_trigger_WID               ( 1)
  #define MC0_CH0_CR_TR_RRDVALID_CTRL_Rank_7_trigger_MSK               (0x00200000)
  #define MC0_CH0_CR_TR_RRDVALID_CTRL_Rank_7_trigger_MIN               (0)
  #define MC0_CH0_CR_TR_RRDVALID_CTRL_Rank_7_trigger_MAX               (1) // 0x00000001
  #define MC0_CH0_CR_TR_RRDVALID_CTRL_Rank_7_trigger_DEF               (0x00000000)
  #define MC0_CH0_CR_TR_RRDVALID_CTRL_Rank_7_trigger_HSH               (0x012AE104)

  #define MC0_CH0_CR_TR_RRDVALID_CTRL_Rank_7_wr_en_OFF                 (22)
  #define MC0_CH0_CR_TR_RRDVALID_CTRL_Rank_7_wr_en_WID                 ( 1)
  #define MC0_CH0_CR_TR_RRDVALID_CTRL_Rank_7_wr_en_MSK                 (0x00400000)
  #define MC0_CH0_CR_TR_RRDVALID_CTRL_Rank_7_wr_en_MIN                 (0)
  #define MC0_CH0_CR_TR_RRDVALID_CTRL_Rank_7_wr_en_MAX                 (1) // 0x00000001
  #define MC0_CH0_CR_TR_RRDVALID_CTRL_Rank_7_wr_en_DEF                 (0x00000000)
  #define MC0_CH0_CR_TR_RRDVALID_CTRL_Rank_7_wr_en_HSH                 (0x012CE104)

  #define MC0_CH0_CR_TR_RRDVALID_CTRL_Rank_7_overflow_OFF              (23)
  #define MC0_CH0_CR_TR_RRDVALID_CTRL_Rank_7_overflow_WID              ( 1)
  #define MC0_CH0_CR_TR_RRDVALID_CTRL_Rank_7_overflow_MSK              (0x00800000)
  #define MC0_CH0_CR_TR_RRDVALID_CTRL_Rank_7_overflow_MIN              (0)
  #define MC0_CH0_CR_TR_RRDVALID_CTRL_Rank_7_overflow_MAX              (1) // 0x00000001
  #define MC0_CH0_CR_TR_RRDVALID_CTRL_Rank_7_overflow_DEF              (0x00000000)
  #define MC0_CH0_CR_TR_RRDVALID_CTRL_Rank_7_overflow_HSH              (0x012EE104)

#define MC0_CH0_CR_TR_RRDVALID_DATA_REG                                (0x0000E108)

  #define MC0_CH0_CR_TR_RRDVALID_DATA_Rank_0_sign_OFF                  ( 0)
  #define MC0_CH0_CR_TR_RRDVALID_DATA_Rank_0_sign_WID                  ( 1)
  #define MC0_CH0_CR_TR_RRDVALID_DATA_Rank_0_sign_MSK                  (0x00000001)
  #define MC0_CH0_CR_TR_RRDVALID_DATA_Rank_0_sign_MIN                  (0)
  #define MC0_CH0_CR_TR_RRDVALID_DATA_Rank_0_sign_MAX                  (1) // 0x00000001
  #define MC0_CH0_CR_TR_RRDVALID_DATA_Rank_0_sign_DEF                  (0x00000000)
  #define MC0_CH0_CR_TR_RRDVALID_DATA_Rank_0_sign_HSH                  (0x4100E108)

  #define MC0_CH0_CR_TR_RRDVALID_DATA_Rank_0_value_OFF                 ( 1)
  #define MC0_CH0_CR_TR_RRDVALID_DATA_Rank_0_value_WID                 ( 7)
  #define MC0_CH0_CR_TR_RRDVALID_DATA_Rank_0_value_MSK                 (0x000000FE)
  #define MC0_CH0_CR_TR_RRDVALID_DATA_Rank_0_value_MIN                 (0)
  #define MC0_CH0_CR_TR_RRDVALID_DATA_Rank_0_value_MAX                 (127) // 0x0000007F
  #define MC0_CH0_CR_TR_RRDVALID_DATA_Rank_0_value_DEF                 (0x00000000)
  #define MC0_CH0_CR_TR_RRDVALID_DATA_Rank_0_value_HSH                 (0x4702E108)

  #define MC0_CH0_CR_TR_RRDVALID_DATA_Rank_1_sign_OFF                  ( 8)
  #define MC0_CH0_CR_TR_RRDVALID_DATA_Rank_1_sign_WID                  ( 1)
  #define MC0_CH0_CR_TR_RRDVALID_DATA_Rank_1_sign_MSK                  (0x00000100)
  #define MC0_CH0_CR_TR_RRDVALID_DATA_Rank_1_sign_MIN                  (0)
  #define MC0_CH0_CR_TR_RRDVALID_DATA_Rank_1_sign_MAX                  (1) // 0x00000001
  #define MC0_CH0_CR_TR_RRDVALID_DATA_Rank_1_sign_DEF                  (0x00000000)
  #define MC0_CH0_CR_TR_RRDVALID_DATA_Rank_1_sign_HSH                  (0x4110E108)

  #define MC0_CH0_CR_TR_RRDVALID_DATA_Rank_1_value_OFF                 ( 9)
  #define MC0_CH0_CR_TR_RRDVALID_DATA_Rank_1_value_WID                 ( 7)
  #define MC0_CH0_CR_TR_RRDVALID_DATA_Rank_1_value_MSK                 (0x0000FE00)
  #define MC0_CH0_CR_TR_RRDVALID_DATA_Rank_1_value_MIN                 (0)
  #define MC0_CH0_CR_TR_RRDVALID_DATA_Rank_1_value_MAX                 (127) // 0x0000007F
  #define MC0_CH0_CR_TR_RRDVALID_DATA_Rank_1_value_DEF                 (0x00000000)
  #define MC0_CH0_CR_TR_RRDVALID_DATA_Rank_1_value_HSH                 (0x4712E108)

  #define MC0_CH0_CR_TR_RRDVALID_DATA_Rank_2_sign_OFF                  (16)
  #define MC0_CH0_CR_TR_RRDVALID_DATA_Rank_2_sign_WID                  ( 1)
  #define MC0_CH0_CR_TR_RRDVALID_DATA_Rank_2_sign_MSK                  (0x00010000)
  #define MC0_CH0_CR_TR_RRDVALID_DATA_Rank_2_sign_MIN                  (0)
  #define MC0_CH0_CR_TR_RRDVALID_DATA_Rank_2_sign_MAX                  (1) // 0x00000001
  #define MC0_CH0_CR_TR_RRDVALID_DATA_Rank_2_sign_DEF                  (0x00000000)
  #define MC0_CH0_CR_TR_RRDVALID_DATA_Rank_2_sign_HSH                  (0x4120E108)

  #define MC0_CH0_CR_TR_RRDVALID_DATA_Rank_2_value_OFF                 (17)
  #define MC0_CH0_CR_TR_RRDVALID_DATA_Rank_2_value_WID                 ( 7)
  #define MC0_CH0_CR_TR_RRDVALID_DATA_Rank_2_value_MSK                 (0x00FE0000)
  #define MC0_CH0_CR_TR_RRDVALID_DATA_Rank_2_value_MIN                 (0)
  #define MC0_CH0_CR_TR_RRDVALID_DATA_Rank_2_value_MAX                 (127) // 0x0000007F
  #define MC0_CH0_CR_TR_RRDVALID_DATA_Rank_2_value_DEF                 (0x00000000)
  #define MC0_CH0_CR_TR_RRDVALID_DATA_Rank_2_value_HSH                 (0x4722E108)

  #define MC0_CH0_CR_TR_RRDVALID_DATA_Rank_3_sign_OFF                  (24)
  #define MC0_CH0_CR_TR_RRDVALID_DATA_Rank_3_sign_WID                  ( 1)
  #define MC0_CH0_CR_TR_RRDVALID_DATA_Rank_3_sign_MSK                  (0x01000000)
  #define MC0_CH0_CR_TR_RRDVALID_DATA_Rank_3_sign_MIN                  (0)
  #define MC0_CH0_CR_TR_RRDVALID_DATA_Rank_3_sign_MAX                  (1) // 0x00000001
  #define MC0_CH0_CR_TR_RRDVALID_DATA_Rank_3_sign_DEF                  (0x00000000)
  #define MC0_CH0_CR_TR_RRDVALID_DATA_Rank_3_sign_HSH                  (0x4130E108)

  #define MC0_CH0_CR_TR_RRDVALID_DATA_Rank_3_value_OFF                 (25)
  #define MC0_CH0_CR_TR_RRDVALID_DATA_Rank_3_value_WID                 ( 7)
  #define MC0_CH0_CR_TR_RRDVALID_DATA_Rank_3_value_MSK                 (0xFE000000)
  #define MC0_CH0_CR_TR_RRDVALID_DATA_Rank_3_value_MIN                 (0)
  #define MC0_CH0_CR_TR_RRDVALID_DATA_Rank_3_value_MAX                 (127) // 0x0000007F
  #define MC0_CH0_CR_TR_RRDVALID_DATA_Rank_3_value_DEF                 (0x00000000)
  #define MC0_CH0_CR_TR_RRDVALID_DATA_Rank_3_value_HSH                 (0x4732E108)

  #define MC0_CH0_CR_TR_RRDVALID_DATA_Rank_4_sign_OFF                  (32)
  #define MC0_CH0_CR_TR_RRDVALID_DATA_Rank_4_sign_WID                  ( 1)
  #define MC0_CH0_CR_TR_RRDVALID_DATA_Rank_4_sign_MSK                  (0x0000000100000000ULL)
  #define MC0_CH0_CR_TR_RRDVALID_DATA_Rank_4_sign_MIN                  (0)
  #define MC0_CH0_CR_TR_RRDVALID_DATA_Rank_4_sign_MAX                  (1) // 0x00000001
  #define MC0_CH0_CR_TR_RRDVALID_DATA_Rank_4_sign_DEF                  (0x00000000)
  #define MC0_CH0_CR_TR_RRDVALID_DATA_Rank_4_sign_HSH                  (0x4140E108)

  #define MC0_CH0_CR_TR_RRDVALID_DATA_Rank_4_value_OFF                 (33)
  #define MC0_CH0_CR_TR_RRDVALID_DATA_Rank_4_value_WID                 ( 7)
  #define MC0_CH0_CR_TR_RRDVALID_DATA_Rank_4_value_MSK                 (0x000000FE00000000ULL)
  #define MC0_CH0_CR_TR_RRDVALID_DATA_Rank_4_value_MIN                 (0)
  #define MC0_CH0_CR_TR_RRDVALID_DATA_Rank_4_value_MAX                 (127) // 0x0000007F
  #define MC0_CH0_CR_TR_RRDVALID_DATA_Rank_4_value_DEF                 (0x00000000)
  #define MC0_CH0_CR_TR_RRDVALID_DATA_Rank_4_value_HSH                 (0x4742E108)

  #define MC0_CH0_CR_TR_RRDVALID_DATA_Rank_5_sign_OFF                  (40)
  #define MC0_CH0_CR_TR_RRDVALID_DATA_Rank_5_sign_WID                  ( 1)
  #define MC0_CH0_CR_TR_RRDVALID_DATA_Rank_5_sign_MSK                  (0x0000010000000000ULL)
  #define MC0_CH0_CR_TR_RRDVALID_DATA_Rank_5_sign_MIN                  (0)
  #define MC0_CH0_CR_TR_RRDVALID_DATA_Rank_5_sign_MAX                  (1) // 0x00000001
  #define MC0_CH0_CR_TR_RRDVALID_DATA_Rank_5_sign_DEF                  (0x00000000)
  #define MC0_CH0_CR_TR_RRDVALID_DATA_Rank_5_sign_HSH                  (0x4150E108)

  #define MC0_CH0_CR_TR_RRDVALID_DATA_Rank_5_value_OFF                 (41)
  #define MC0_CH0_CR_TR_RRDVALID_DATA_Rank_5_value_WID                 ( 7)
  #define MC0_CH0_CR_TR_RRDVALID_DATA_Rank_5_value_MSK                 (0x0000FE0000000000ULL)
  #define MC0_CH0_CR_TR_RRDVALID_DATA_Rank_5_value_MIN                 (0)
  #define MC0_CH0_CR_TR_RRDVALID_DATA_Rank_5_value_MAX                 (127) // 0x0000007F
  #define MC0_CH0_CR_TR_RRDVALID_DATA_Rank_5_value_DEF                 (0x00000000)
  #define MC0_CH0_CR_TR_RRDVALID_DATA_Rank_5_value_HSH                 (0x4752E108)

  #define MC0_CH0_CR_TR_RRDVALID_DATA_Rank_6_sign_OFF                  (48)
  #define MC0_CH0_CR_TR_RRDVALID_DATA_Rank_6_sign_WID                  ( 1)
  #define MC0_CH0_CR_TR_RRDVALID_DATA_Rank_6_sign_MSK                  (0x0001000000000000ULL)
  #define MC0_CH0_CR_TR_RRDVALID_DATA_Rank_6_sign_MIN                  (0)
  #define MC0_CH0_CR_TR_RRDVALID_DATA_Rank_6_sign_MAX                  (1) // 0x00000001
  #define MC0_CH0_CR_TR_RRDVALID_DATA_Rank_6_sign_DEF                  (0x00000000)
  #define MC0_CH0_CR_TR_RRDVALID_DATA_Rank_6_sign_HSH                  (0x4160E108)

  #define MC0_CH0_CR_TR_RRDVALID_DATA_Rank_6_value_OFF                 (49)
  #define MC0_CH0_CR_TR_RRDVALID_DATA_Rank_6_value_WID                 ( 7)
  #define MC0_CH0_CR_TR_RRDVALID_DATA_Rank_6_value_MSK                 (0x00FE000000000000ULL)
  #define MC0_CH0_CR_TR_RRDVALID_DATA_Rank_6_value_MIN                 (0)
  #define MC0_CH0_CR_TR_RRDVALID_DATA_Rank_6_value_MAX                 (127) // 0x0000007F
  #define MC0_CH0_CR_TR_RRDVALID_DATA_Rank_6_value_DEF                 (0x00000000)
  #define MC0_CH0_CR_TR_RRDVALID_DATA_Rank_6_value_HSH                 (0x4762E108)

  #define MC0_CH0_CR_TR_RRDVALID_DATA_Rank_7_sign_OFF                  (56)
  #define MC0_CH0_CR_TR_RRDVALID_DATA_Rank_7_sign_WID                  ( 1)
  #define MC0_CH0_CR_TR_RRDVALID_DATA_Rank_7_sign_MSK                  (0x0100000000000000ULL)
  #define MC0_CH0_CR_TR_RRDVALID_DATA_Rank_7_sign_MIN                  (0)
  #define MC0_CH0_CR_TR_RRDVALID_DATA_Rank_7_sign_MAX                  (1) // 0x00000001
  #define MC0_CH0_CR_TR_RRDVALID_DATA_Rank_7_sign_DEF                  (0x00000000)
  #define MC0_CH0_CR_TR_RRDVALID_DATA_Rank_7_sign_HSH                  (0x4170E108)

  #define MC0_CH0_CR_TR_RRDVALID_DATA_Rank_7_value_OFF                 (57)
  #define MC0_CH0_CR_TR_RRDVALID_DATA_Rank_7_value_WID                 ( 7)
  #define MC0_CH0_CR_TR_RRDVALID_DATA_Rank_7_value_MSK                 (0xFE00000000000000ULL)
  #define MC0_CH0_CR_TR_RRDVALID_DATA_Rank_7_value_MIN                 (0)
  #define MC0_CH0_CR_TR_RRDVALID_DATA_Rank_7_value_MAX                 (127) // 0x0000007F
  #define MC0_CH0_CR_TR_RRDVALID_DATA_Rank_7_value_DEF                 (0x00000000)
  #define MC0_CH0_CR_TR_RRDVALID_DATA_Rank_7_value_HSH                 (0x4772E108)

#define MC0_CH0_CR_WMM_READ_CONFIG_REG                                 (0x0000E110)

  #define MC0_CH0_CR_WMM_READ_CONFIG_Dis_Opp_rd_OFF                    ( 0)
  #define MC0_CH0_CR_WMM_READ_CONFIG_Dis_Opp_rd_WID                    ( 1)
  #define MC0_CH0_CR_WMM_READ_CONFIG_Dis_Opp_rd_MSK                    (0x00000001)
  #define MC0_CH0_CR_WMM_READ_CONFIG_Dis_Opp_rd_MIN                    (0)
  #define MC0_CH0_CR_WMM_READ_CONFIG_Dis_Opp_rd_MAX                    (1) // 0x00000001
  #define MC0_CH0_CR_WMM_READ_CONFIG_Dis_Opp_rd_DEF                    (0x00000000)
  #define MC0_CH0_CR_WMM_READ_CONFIG_Dis_Opp_rd_HSH                    (0x0100E110)

  #define MC0_CH0_CR_WMM_READ_CONFIG_ACT_Enable_OFF                    ( 1)
  #define MC0_CH0_CR_WMM_READ_CONFIG_ACT_Enable_WID                    ( 1)
  #define MC0_CH0_CR_WMM_READ_CONFIG_ACT_Enable_MSK                    (0x00000002)
  #define MC0_CH0_CR_WMM_READ_CONFIG_ACT_Enable_MIN                    (0)
  #define MC0_CH0_CR_WMM_READ_CONFIG_ACT_Enable_MAX                    (1) // 0x00000001
  #define MC0_CH0_CR_WMM_READ_CONFIG_ACT_Enable_DEF                    (0x00000001)
  #define MC0_CH0_CR_WMM_READ_CONFIG_ACT_Enable_HSH                    (0x0102E110)

  #define MC0_CH0_CR_WMM_READ_CONFIG_PRE_Enable_OFF                    ( 2)
  #define MC0_CH0_CR_WMM_READ_CONFIG_PRE_Enable_WID                    ( 1)
  #define MC0_CH0_CR_WMM_READ_CONFIG_PRE_Enable_MSK                    (0x00000004)
  #define MC0_CH0_CR_WMM_READ_CONFIG_PRE_Enable_MIN                    (0)
  #define MC0_CH0_CR_WMM_READ_CONFIG_PRE_Enable_MAX                    (1) // 0x00000001
  #define MC0_CH0_CR_WMM_READ_CONFIG_PRE_Enable_DEF                    (0x00000001)
  #define MC0_CH0_CR_WMM_READ_CONFIG_PRE_Enable_HSH                    (0x0104E110)

  #define MC0_CH0_CR_WMM_READ_CONFIG_MAX_RPQ_CAS_OFF                   ( 3)
  #define MC0_CH0_CR_WMM_READ_CONFIG_MAX_RPQ_CAS_WID                   ( 4)
  #define MC0_CH0_CR_WMM_READ_CONFIG_MAX_RPQ_CAS_MSK                   (0x00000078)
  #define MC0_CH0_CR_WMM_READ_CONFIG_MAX_RPQ_CAS_MIN                   (0)
  #define MC0_CH0_CR_WMM_READ_CONFIG_MAX_RPQ_CAS_MAX                   (15) // 0x0000000F
  #define MC0_CH0_CR_WMM_READ_CONFIG_MAX_RPQ_CAS_DEF                   (0x00000008)
  #define MC0_CH0_CR_WMM_READ_CONFIG_MAX_RPQ_CAS_HSH                   (0x0406E110)

#define MC0_CH0_CR_MC2PHY_BGF_CTRL_REG                                 (0x0000E114)

  #define MC0_CH0_CR_MC2PHY_BGF_CTRL_PtrSep_ispid_OFF                  ( 0)
  #define MC0_CH0_CR_MC2PHY_BGF_CTRL_PtrSep_ispid_WID                  ( 2)
  #define MC0_CH0_CR_MC2PHY_BGF_CTRL_PtrSep_ispid_MSK                  (0x00000003)
  #define MC0_CH0_CR_MC2PHY_BGF_CTRL_PtrSep_ispid_MIN                  (0)
  #define MC0_CH0_CR_MC2PHY_BGF_CTRL_PtrSep_ispid_MAX                  (3) // 0x00000003
  #define MC0_CH0_CR_MC2PHY_BGF_CTRL_PtrSep_ispid_DEF                  (0x00000001)
  #define MC0_CH0_CR_MC2PHY_BGF_CTRL_PtrSep_ispid_HSH                  (0x0200E114)

  #define MC0_CH0_CR_MC2PHY_BGF_CTRL_BgfRun_Ovrd_En_OFF                ( 2)
  #define MC0_CH0_CR_MC2PHY_BGF_CTRL_BgfRun_Ovrd_En_WID                ( 1)
  #define MC0_CH0_CR_MC2PHY_BGF_CTRL_BgfRun_Ovrd_En_MSK                (0x00000004)
  #define MC0_CH0_CR_MC2PHY_BGF_CTRL_BgfRun_Ovrd_En_MIN                (0)
  #define MC0_CH0_CR_MC2PHY_BGF_CTRL_BgfRun_Ovrd_En_MAX                (1) // 0x00000001
  #define MC0_CH0_CR_MC2PHY_BGF_CTRL_BgfRun_Ovrd_En_DEF                (0x00000000)
  #define MC0_CH0_CR_MC2PHY_BGF_CTRL_BgfRun_Ovrd_En_HSH                (0x0104E114)

  #define MC0_CH0_CR_MC2PHY_BGF_CTRL_BgfRun_Ovrd_Val_OFF               ( 3)
  #define MC0_CH0_CR_MC2PHY_BGF_CTRL_BgfRun_Ovrd_Val_WID               ( 1)
  #define MC0_CH0_CR_MC2PHY_BGF_CTRL_BgfRun_Ovrd_Val_MSK               (0x00000008)
  #define MC0_CH0_CR_MC2PHY_BGF_CTRL_BgfRun_Ovrd_Val_MIN               (0)
  #define MC0_CH0_CR_MC2PHY_BGF_CTRL_BgfRun_Ovrd_Val_MAX               (1) // 0x00000001
  #define MC0_CH0_CR_MC2PHY_BGF_CTRL_BgfRun_Ovrd_Val_DEF               (0x00000000)
  #define MC0_CH0_CR_MC2PHY_BGF_CTRL_BgfRun_Ovrd_Val_HSH               (0x0106E114)

  #define MC0_CH0_CR_MC2PHY_BGF_CTRL_BgfRun_Delay_OFF                  ( 4)
  #define MC0_CH0_CR_MC2PHY_BGF_CTRL_BgfRun_Delay_WID                  ( 5)
  #define MC0_CH0_CR_MC2PHY_BGF_CTRL_BgfRun_Delay_MSK                  (0x000001F0)
  #define MC0_CH0_CR_MC2PHY_BGF_CTRL_BgfRun_Delay_MIN                  (0)
  #define MC0_CH0_CR_MC2PHY_BGF_CTRL_BgfRun_Delay_MAX                  (31) // 0x0000001F
  #define MC0_CH0_CR_MC2PHY_BGF_CTRL_BgfRun_Delay_DEF                  (0x00000001)
  #define MC0_CH0_CR_MC2PHY_BGF_CTRL_BgfRun_Delay_HSH                  (0x0508E114)

  #define MC0_CH0_CR_MC2PHY_BGF_CTRL_clkStop_override_en_on_BGF_Off_OFF ( 9)
  #define MC0_CH0_CR_MC2PHY_BGF_CTRL_clkStop_override_en_on_BGF_Off_WID ( 1)
  #define MC0_CH0_CR_MC2PHY_BGF_CTRL_clkStop_override_en_on_BGF_Off_MSK (0x00000200)
  #define MC0_CH0_CR_MC2PHY_BGF_CTRL_clkStop_override_en_on_BGF_Off_MIN (0)
  #define MC0_CH0_CR_MC2PHY_BGF_CTRL_clkStop_override_en_on_BGF_Off_MAX (1) // 0x00000001
  #define MC0_CH0_CR_MC2PHY_BGF_CTRL_clkStop_override_en_on_BGF_Off_DEF (0x00000001)
  #define MC0_CH0_CR_MC2PHY_BGF_CTRL_clkStop_override_en_on_BGF_Off_HSH (0x0112E114)

  #define MC0_CH0_CR_MC2PHY_BGF_CTRL_clkStop_override_val_on_BGF_Off_OFF (10)
  #define MC0_CH0_CR_MC2PHY_BGF_CTRL_clkStop_override_val_on_BGF_Off_WID ( 1)
  #define MC0_CH0_CR_MC2PHY_BGF_CTRL_clkStop_override_val_on_BGF_Off_MSK (0x00000400)
  #define MC0_CH0_CR_MC2PHY_BGF_CTRL_clkStop_override_val_on_BGF_Off_MIN (0)
  #define MC0_CH0_CR_MC2PHY_BGF_CTRL_clkStop_override_val_on_BGF_Off_MAX (1) // 0x00000001
  #define MC0_CH0_CR_MC2PHY_BGF_CTRL_clkStop_override_val_on_BGF_Off_DEF (0x00000001)
  #define MC0_CH0_CR_MC2PHY_BGF_CTRL_clkStop_override_val_on_BGF_Off_HSH (0x0114E114)

  #define MC0_CH0_CR_MC2PHY_BGF_CTRL_Enable_osc_override_OFF           (11)
  #define MC0_CH0_CR_MC2PHY_BGF_CTRL_Enable_osc_override_WID           ( 1)
  #define MC0_CH0_CR_MC2PHY_BGF_CTRL_Enable_osc_override_MSK           (0x00000800)
  #define MC0_CH0_CR_MC2PHY_BGF_CTRL_Enable_osc_override_MIN           (0)
  #define MC0_CH0_CR_MC2PHY_BGF_CTRL_Enable_osc_override_MAX           (1) // 0x00000001
  #define MC0_CH0_CR_MC2PHY_BGF_CTRL_Enable_osc_override_DEF           (0x00000000)
  #define MC0_CH0_CR_MC2PHY_BGF_CTRL_Enable_osc_override_HSH           (0x0116E114)

  #define MC0_CH0_CR_MC2PHY_BGF_CTRL_Enable_osc_check_OFF              (12)
  #define MC0_CH0_CR_MC2PHY_BGF_CTRL_Enable_osc_check_WID              ( 1)
  #define MC0_CH0_CR_MC2PHY_BGF_CTRL_Enable_osc_check_MSK              (0x00001000)
  #define MC0_CH0_CR_MC2PHY_BGF_CTRL_Enable_osc_check_MIN              (0)
  #define MC0_CH0_CR_MC2PHY_BGF_CTRL_Enable_osc_check_MAX              (1) // 0x00000001
  #define MC0_CH0_CR_MC2PHY_BGF_CTRL_Enable_osc_check_DEF              (0x00000000)
  #define MC0_CH0_CR_MC2PHY_BGF_CTRL_Enable_osc_check_HSH              (0x0118E114)

  #define MC0_CH0_CR_MC2PHY_BGF_CTRL_MC_2_PHY_mismatch_OFF             (13)
  #define MC0_CH0_CR_MC2PHY_BGF_CTRL_MC_2_PHY_mismatch_WID             ( 1)
  #define MC0_CH0_CR_MC2PHY_BGF_CTRL_MC_2_PHY_mismatch_MSK             (0x00002000)
  #define MC0_CH0_CR_MC2PHY_BGF_CTRL_MC_2_PHY_mismatch_MIN             (0)
  #define MC0_CH0_CR_MC2PHY_BGF_CTRL_MC_2_PHY_mismatch_MAX             (1) // 0x00000001
  #define MC0_CH0_CR_MC2PHY_BGF_CTRL_MC_2_PHY_mismatch_DEF             (0x00000000)
  #define MC0_CH0_CR_MC2PHY_BGF_CTRL_MC_2_PHY_mismatch_HSH             (0x011AE114)

  #define MC0_CH0_CR_MC2PHY_BGF_CTRL_PHY_2_MC_mismatch_OFF             (14)
  #define MC0_CH0_CR_MC2PHY_BGF_CTRL_PHY_2_MC_mismatch_WID             ( 1)
  #define MC0_CH0_CR_MC2PHY_BGF_CTRL_PHY_2_MC_mismatch_MSK             (0x00004000)
  #define MC0_CH0_CR_MC2PHY_BGF_CTRL_PHY_2_MC_mismatch_MIN             (0)
  #define MC0_CH0_CR_MC2PHY_BGF_CTRL_PHY_2_MC_mismatch_MAX             (1) // 0x00000001
  #define MC0_CH0_CR_MC2PHY_BGF_CTRL_PHY_2_MC_mismatch_DEF             (0x00000000)
  #define MC0_CH0_CR_MC2PHY_BGF_CTRL_PHY_2_MC_mismatch_HSH             (0x011CE114)

  #define MC0_CH0_CR_MC2PHY_BGF_CTRL_tCWL_offset_OFF                   (15)
  #define MC0_CH0_CR_MC2PHY_BGF_CTRL_tCWL_offset_WID                   ( 2)
  #define MC0_CH0_CR_MC2PHY_BGF_CTRL_tCWL_offset_MSK                   (0x00018000)
  #define MC0_CH0_CR_MC2PHY_BGF_CTRL_tCWL_offset_MIN                   (0)
  #define MC0_CH0_CR_MC2PHY_BGF_CTRL_tCWL_offset_MAX                   (3) // 0x00000003
  #define MC0_CH0_CR_MC2PHY_BGF_CTRL_tCWL_offset_DEF                   (0x00000000)
  #define MC0_CH0_CR_MC2PHY_BGF_CTRL_tCWL_offset_HSH                   (0x021EE114)

  #define MC0_CH0_CR_MC2PHY_BGF_CTRL_PtrSep_ospid_OFF                  (17)
  #define MC0_CH0_CR_MC2PHY_BGF_CTRL_PtrSep_ospid_WID                  ( 2)
  #define MC0_CH0_CR_MC2PHY_BGF_CTRL_PtrSep_ospid_MSK                  (0x00060000)
  #define MC0_CH0_CR_MC2PHY_BGF_CTRL_PtrSep_ospid_MIN                  (0)
  #define MC0_CH0_CR_MC2PHY_BGF_CTRL_PtrSep_ospid_MAX                  (3) // 0x00000003
  #define MC0_CH0_CR_MC2PHY_BGF_CTRL_PtrSep_ospid_DEF                  (0x00000001)
  #define MC0_CH0_CR_MC2PHY_BGF_CTRL_PtrSep_ospid_HSH                  (0x0222E114)

#define MC0_CH0_CR_SC_ADAPTIVE_PCIT_REG                                (0x0000E118)

  #define MC0_CH0_CR_SC_ADAPTIVE_PCIT_ADAPTIVE_PCIT_WINDOW_OFF         ( 0)
  #define MC0_CH0_CR_SC_ADAPTIVE_PCIT_ADAPTIVE_PCIT_WINDOW_WID         (10)
  #define MC0_CH0_CR_SC_ADAPTIVE_PCIT_ADAPTIVE_PCIT_WINDOW_MSK         (0x000003FF)
  #define MC0_CH0_CR_SC_ADAPTIVE_PCIT_ADAPTIVE_PCIT_WINDOW_MIN         (0)
  #define MC0_CH0_CR_SC_ADAPTIVE_PCIT_ADAPTIVE_PCIT_WINDOW_MAX         (1023) // 0x000003FF
  #define MC0_CH0_CR_SC_ADAPTIVE_PCIT_ADAPTIVE_PCIT_WINDOW_DEF         (0x000000D8)
  #define MC0_CH0_CR_SC_ADAPTIVE_PCIT_ADAPTIVE_PCIT_WINDOW_HSH         (0x4A00E118)

  #define MC0_CH0_CR_SC_ADAPTIVE_PCIT_MIN_PCIT_OFF                     (10)
  #define MC0_CH0_CR_SC_ADAPTIVE_PCIT_MIN_PCIT_WID                     (10)
  #define MC0_CH0_CR_SC_ADAPTIVE_PCIT_MIN_PCIT_MSK                     (0x000FFC00)
  #define MC0_CH0_CR_SC_ADAPTIVE_PCIT_MIN_PCIT_MIN                     (0)
  #define MC0_CH0_CR_SC_ADAPTIVE_PCIT_MIN_PCIT_MAX                     (1023) // 0x000003FF
  #define MC0_CH0_CR_SC_ADAPTIVE_PCIT_MIN_PCIT_DEF                     (0x00000020)
  #define MC0_CH0_CR_SC_ADAPTIVE_PCIT_MIN_PCIT_HSH                     (0x4A14E118)

  #define MC0_CH0_CR_SC_ADAPTIVE_PCIT_MAX_PCIT_OFF                     (20)
  #define MC0_CH0_CR_SC_ADAPTIVE_PCIT_MAX_PCIT_WID                     (10)
  #define MC0_CH0_CR_SC_ADAPTIVE_PCIT_MAX_PCIT_MSK                     (0x3FF00000)
  #define MC0_CH0_CR_SC_ADAPTIVE_PCIT_MAX_PCIT_MIN                     (0)
  #define MC0_CH0_CR_SC_ADAPTIVE_PCIT_MAX_PCIT_MAX                     (1023) // 0x000003FF
  #define MC0_CH0_CR_SC_ADAPTIVE_PCIT_MAX_PCIT_DEF                     (0x00000180)
  #define MC0_CH0_CR_SC_ADAPTIVE_PCIT_MAX_PCIT_HSH                     (0x4A28E118)

  #define MC0_CH0_CR_SC_ADAPTIVE_PCIT_CHANGE_RANGE_OFF                 (30)
  #define MC0_CH0_CR_SC_ADAPTIVE_PCIT_CHANGE_RANGE_WID                 ( 2)
  #define MC0_CH0_CR_SC_ADAPTIVE_PCIT_CHANGE_RANGE_MSK                 (0xC0000000)
  #define MC0_CH0_CR_SC_ADAPTIVE_PCIT_CHANGE_RANGE_MIN                 (0)
  #define MC0_CH0_CR_SC_ADAPTIVE_PCIT_CHANGE_RANGE_MAX                 (3) // 0x00000003
  #define MC0_CH0_CR_SC_ADAPTIVE_PCIT_CHANGE_RANGE_DEF                 (0x00000003)
  #define MC0_CH0_CR_SC_ADAPTIVE_PCIT_CHANGE_RANGE_HSH                 (0x423CE118)

  #define MC0_CH0_CR_SC_ADAPTIVE_PCIT_EARLY_WEIGHT_OFF                 (32)
  #define MC0_CH0_CR_SC_ADAPTIVE_PCIT_EARLY_WEIGHT_WID                 ( 3)
  #define MC0_CH0_CR_SC_ADAPTIVE_PCIT_EARLY_WEIGHT_MSK                 (0x0000000700000000ULL)
  #define MC0_CH0_CR_SC_ADAPTIVE_PCIT_EARLY_WEIGHT_MIN                 (0)
  #define MC0_CH0_CR_SC_ADAPTIVE_PCIT_EARLY_WEIGHT_MAX                 (7) // 0x00000007
  #define MC0_CH0_CR_SC_ADAPTIVE_PCIT_EARLY_WEIGHT_DEF                 (0x00000003)
  #define MC0_CH0_CR_SC_ADAPTIVE_PCIT_EARLY_WEIGHT_HSH                 (0x4340E118)

  #define MC0_CH0_CR_SC_ADAPTIVE_PCIT_LATE_WEIGHT_OFF                  (35)
  #define MC0_CH0_CR_SC_ADAPTIVE_PCIT_LATE_WEIGHT_WID                  ( 3)
  #define MC0_CH0_CR_SC_ADAPTIVE_PCIT_LATE_WEIGHT_MSK                  (0x0000003800000000ULL)
  #define MC0_CH0_CR_SC_ADAPTIVE_PCIT_LATE_WEIGHT_MIN                  (0)
  #define MC0_CH0_CR_SC_ADAPTIVE_PCIT_LATE_WEIGHT_MAX                  (7) // 0x00000007
  #define MC0_CH0_CR_SC_ADAPTIVE_PCIT_LATE_WEIGHT_DEF                  (0x00000001)
  #define MC0_CH0_CR_SC_ADAPTIVE_PCIT_LATE_WEIGHT_HSH                  (0x4346E118)

  #define MC0_CH0_CR_SC_ADAPTIVE_PCIT_PH_WEIGHT_OFF                    (38)
  #define MC0_CH0_CR_SC_ADAPTIVE_PCIT_PH_WEIGHT_WID                    ( 3)
  #define MC0_CH0_CR_SC_ADAPTIVE_PCIT_PH_WEIGHT_MSK                    (0x000001C000000000ULL)
  #define MC0_CH0_CR_SC_ADAPTIVE_PCIT_PH_WEIGHT_MIN                    (0)
  #define MC0_CH0_CR_SC_ADAPTIVE_PCIT_PH_WEIGHT_MAX                    (7) // 0x00000007
  #define MC0_CH0_CR_SC_ADAPTIVE_PCIT_PH_WEIGHT_DEF                    (0x00000000)
  #define MC0_CH0_CR_SC_ADAPTIVE_PCIT_PH_WEIGHT_HSH                    (0x434CE118)

  #define MC0_CH0_CR_SC_ADAPTIVE_PCIT_GOOD_PAGE_CLOSE_WEIGHT_OFF       (41)
  #define MC0_CH0_CR_SC_ADAPTIVE_PCIT_GOOD_PAGE_CLOSE_WEIGHT_WID       ( 3)
  #define MC0_CH0_CR_SC_ADAPTIVE_PCIT_GOOD_PAGE_CLOSE_WEIGHT_MSK       (0x00000E0000000000ULL)
  #define MC0_CH0_CR_SC_ADAPTIVE_PCIT_GOOD_PAGE_CLOSE_WEIGHT_MIN       (0)
  #define MC0_CH0_CR_SC_ADAPTIVE_PCIT_GOOD_PAGE_CLOSE_WEIGHT_MAX       (7) // 0x00000007
  #define MC0_CH0_CR_SC_ADAPTIVE_PCIT_GOOD_PAGE_CLOSE_WEIGHT_DEF       (0x00000001)
  #define MC0_CH0_CR_SC_ADAPTIVE_PCIT_GOOD_PAGE_CLOSE_WEIGHT_HSH       (0x4352E118)

  #define MC0_CH0_CR_SC_ADAPTIVE_PCIT_TRANSACTION_FACTOR_OFF           (44)
  #define MC0_CH0_CR_SC_ADAPTIVE_PCIT_TRANSACTION_FACTOR_WID           ( 3)
  #define MC0_CH0_CR_SC_ADAPTIVE_PCIT_TRANSACTION_FACTOR_MSK           (0x0000700000000000ULL)
  #define MC0_CH0_CR_SC_ADAPTIVE_PCIT_TRANSACTION_FACTOR_MIN           (0)
  #define MC0_CH0_CR_SC_ADAPTIVE_PCIT_TRANSACTION_FACTOR_MAX           (7) // 0x00000007
  #define MC0_CH0_CR_SC_ADAPTIVE_PCIT_TRANSACTION_FACTOR_DEF           (0x00000003)
  #define MC0_CH0_CR_SC_ADAPTIVE_PCIT_TRANSACTION_FACTOR_HSH           (0x4358E118)

  #define MC0_CH0_CR_SC_ADAPTIVE_PCIT_IGNORE_VC1_OFF                   (47)
  #define MC0_CH0_CR_SC_ADAPTIVE_PCIT_IGNORE_VC1_WID                   ( 1)
  #define MC0_CH0_CR_SC_ADAPTIVE_PCIT_IGNORE_VC1_MSK                   (0x0000800000000000ULL)
  #define MC0_CH0_CR_SC_ADAPTIVE_PCIT_IGNORE_VC1_MIN                   (0)
  #define MC0_CH0_CR_SC_ADAPTIVE_PCIT_IGNORE_VC1_MAX                   (1) // 0x00000001
  #define MC0_CH0_CR_SC_ADAPTIVE_PCIT_IGNORE_VC1_DEF                   (0x00000001)
  #define MC0_CH0_CR_SC_ADAPTIVE_PCIT_IGNORE_VC1_HSH                   (0x415EE118)

  #define MC0_CH0_CR_SC_ADAPTIVE_PCIT_IGNORE_GT_OFF                    (48)
  #define MC0_CH0_CR_SC_ADAPTIVE_PCIT_IGNORE_GT_WID                    ( 1)
  #define MC0_CH0_CR_SC_ADAPTIVE_PCIT_IGNORE_GT_MSK                    (0x0001000000000000ULL)
  #define MC0_CH0_CR_SC_ADAPTIVE_PCIT_IGNORE_GT_MIN                    (0)
  #define MC0_CH0_CR_SC_ADAPTIVE_PCIT_IGNORE_GT_MAX                    (1) // 0x00000001
  #define MC0_CH0_CR_SC_ADAPTIVE_PCIT_IGNORE_GT_DEF                    (0x00000001)
  #define MC0_CH0_CR_SC_ADAPTIVE_PCIT_IGNORE_GT_HSH                    (0x4160E118)

#define MC0_CH0_CR_MERGE_REQ_READS_PQ_REG                              (0x0000E120)

  #define MC0_CH0_CR_MERGE_REQ_READS_PQ_disp_srcid_A0_OFF                 ( 0)
  #define MC0_CH0_CR_MERGE_REQ_READS_PQ_disp_srcid_A0_WID                 ( 5)
  #define MC0_CH0_CR_MERGE_REQ_READS_PQ_disp_srcid_A0_MSK                 (0x0000001F)
  #define MC0_CH0_CR_MERGE_REQ_READS_PQ_disp_srcid_A0_MIN                 (0)
  #define MC0_CH0_CR_MERGE_REQ_READS_PQ_disp_srcid_A0_MAX                 (31) // 0x0000001F
  #define MC0_CH0_CR_MERGE_REQ_READS_PQ_disp_srcid_A0_DEF                 (0x00000002)
  #define MC0_CH0_CR_MERGE_REQ_READS_PQ_disp_srcid_A0_HSH                 (0x0500E120)

  #define MC0_CH0_CR_MERGE_REQ_READS_PQ_enable_ipq_req_merge_OFF       ( 5)
  #define MC0_CH0_CR_MERGE_REQ_READS_PQ_enable_ipq_req_merge_WID       ( 1)
  #define MC0_CH0_CR_MERGE_REQ_READS_PQ_enable_ipq_req_merge_MSK       (0x00000020)
  #define MC0_CH0_CR_MERGE_REQ_READS_PQ_enable_ipq_req_merge_MIN       (0)
  #define MC0_CH0_CR_MERGE_REQ_READS_PQ_enable_ipq_req_merge_MAX       (1) // 0x00000001
  #define MC0_CH0_CR_MERGE_REQ_READS_PQ_enable_ipq_req_merge_DEF       (0x00000001)
  #define MC0_CH0_CR_MERGE_REQ_READS_PQ_enable_ipq_req_merge_HSH       (0x010AE120)

  #define MC0_CH0_CR_MERGE_REQ_READS_PQ_enable_merge_req_vc1_low_prio_OFF ( 6)
  #define MC0_CH0_CR_MERGE_REQ_READS_PQ_enable_merge_req_vc1_low_prio_WID ( 1)
  #define MC0_CH0_CR_MERGE_REQ_READS_PQ_enable_merge_req_vc1_low_prio_MSK (0x00000040)
  #define MC0_CH0_CR_MERGE_REQ_READS_PQ_enable_merge_req_vc1_low_prio_MIN (0)
  #define MC0_CH0_CR_MERGE_REQ_READS_PQ_enable_merge_req_vc1_low_prio_MAX (1) // 0x00000001
  #define MC0_CH0_CR_MERGE_REQ_READS_PQ_enable_merge_req_vc1_low_prio_DEF (0x00000001)
  #define MC0_CH0_CR_MERGE_REQ_READS_PQ_enable_merge_req_vc1_low_prio_HSH (0x010CE120)

  #define MC0_CH0_CR_MERGE_REQ_READS_PQ_enable_rpq_req_merge_OFF       ( 7)
  #define MC0_CH0_CR_MERGE_REQ_READS_PQ_enable_rpq_req_merge_WID       ( 1)
  #define MC0_CH0_CR_MERGE_REQ_READS_PQ_enable_rpq_req_merge_MSK       (0x00000080)
  #define MC0_CH0_CR_MERGE_REQ_READS_PQ_enable_rpq_req_merge_MIN       (0)
  #define MC0_CH0_CR_MERGE_REQ_READS_PQ_enable_rpq_req_merge_MAX       (1) // 0x00000001
  #define MC0_CH0_CR_MERGE_REQ_READS_PQ_enable_rpq_req_merge_DEF       (0x00000001)
  #define MC0_CH0_CR_MERGE_REQ_READS_PQ_enable_rpq_req_merge_HSH       (0x010EE120)
#define MC0_CH0_CR_ROWHAMMER_CTL_REG                                   (0x0000E128)

  #define MC0_CH0_CR_ROWHAMMER_CTL_RH_ENABLED_DIMM0_OFF                ( 0)
  #define MC0_CH0_CR_ROWHAMMER_CTL_RH_ENABLED_DIMM0_WID                ( 1)
  #define MC0_CH0_CR_ROWHAMMER_CTL_RH_ENABLED_DIMM0_MSK                (0x00000001)
  #define MC0_CH0_CR_ROWHAMMER_CTL_RH_ENABLED_DIMM0_MIN                (0)
  #define MC0_CH0_CR_ROWHAMMER_CTL_RH_ENABLED_DIMM0_MAX                (1) // 0x00000001
  #define MC0_CH0_CR_ROWHAMMER_CTL_RH_ENABLED_DIMM0_DEF                (0x00000000)
  #define MC0_CH0_CR_ROWHAMMER_CTL_RH_ENABLED_DIMM0_HSH                (0x4100E128)

  #define MC0_CH0_CR_ROWHAMMER_CTL_RH_ENABLED_DIMM1_OFF                ( 1)
  #define MC0_CH0_CR_ROWHAMMER_CTL_RH_ENABLED_DIMM1_WID                ( 1)
  #define MC0_CH0_CR_ROWHAMMER_CTL_RH_ENABLED_DIMM1_MSK                (0x00000002)
  #define MC0_CH0_CR_ROWHAMMER_CTL_RH_ENABLED_DIMM1_MIN                (0)
  #define MC0_CH0_CR_ROWHAMMER_CTL_RH_ENABLED_DIMM1_MAX                (1) // 0x00000001
  #define MC0_CH0_CR_ROWHAMMER_CTL_RH_ENABLED_DIMM1_DEF                (0x00000000)
  #define MC0_CH0_CR_ROWHAMMER_CTL_RH_ENABLED_DIMM1_HSH                (0x4102E128)

  #define MC0_CH0_CR_ROWHAMMER_CTL_RH_HIGH_WM_OFF                      ( 2)
  #define MC0_CH0_CR_ROWHAMMER_CTL_RH_HIGH_WM_WID                      (12)

  #define MC0_CH0_CR_ROWHAMMER_CTL_RH_HIGH_WM_MSK                      (0x00003FFC)

  #define MC0_CH0_CR_ROWHAMMER_CTL_RH_HIGH_WM_MIN                      (0)
  #define MC0_CH0_CR_ROWHAMMER_CTL_RH_HIGH_WM_MAX                      (4095) // 0x00000FFF

  #define MC0_CH0_CR_ROWHAMMER_CTL_RH_HIGH_WM_DEF                      (0x00000060)
  #define MC0_CH0_CR_ROWHAMMER_CTL_RH_HIGH_WM_HSH                      (0x4C04E128)

  #define MC0_CH0_CR_ROWHAMMER_CTL_RH_LOW_WM_OFF                       (14)
  #define MC0_CH0_CR_ROWHAMMER_CTL_RH_LOW_WM_WID                       (11)
  #define MC0_CH0_CR_ROWHAMMER_CTL_RH_LOW_WM_MSK                       (0x01FFC000)
  #define MC0_CH0_CR_ROWHAMMER_CTL_RH_LOW_WM_MIN                       (0)
  #define MC0_CH0_CR_ROWHAMMER_CTL_RH_LOW_WM_MAX                       (2047) // 0x000007FF
  #define MC0_CH0_CR_ROWHAMMER_CTL_RH_LOW_WM_DEF                       (0x00000010)
  #define MC0_CH0_CR_ROWHAMMER_CTL_RH_LOW_WM_HSH                       (0x4B1CE128)

  #define MC0_CH0_CR_ROWHAMMER_CTL_NORMAL_REF_SUB_OFF                  (32)
  #define MC0_CH0_CR_ROWHAMMER_CTL_NORMAL_REF_SUB_WID                  (12)

  #define MC0_CH0_CR_ROWHAMMER_CTL_NORMAL_REF_SUB_MSK                  (0x00000FFF00000000ULL)

  #define MC0_CH0_CR_ROWHAMMER_CTL_NORMAL_REF_SUB_MIN                  (0)
  #define MC0_CH0_CR_ROWHAMMER_CTL_NORMAL_REF_SUB_MAX                  (4095) // 0x00000FFF

  #define MC0_CH0_CR_ROWHAMMER_CTL_NORMAL_REF_SUB_DEF                  (0x00000008)
  #define MC0_CH0_CR_ROWHAMMER_CTL_NORMAL_REF_SUB_HSH                  (0x4C40E128)


  #define MC0_CH0_CR_ROWHAMMER_CTL_REFM_SUB_OFF                        (44)
  #define MC0_CH0_CR_ROWHAMMER_CTL_REFM_SUB_WID                        (12)

  #define MC0_CH0_CR_ROWHAMMER_CTL_REFM_SUB_MSK                        (0x00FFF00000000000ULL)

  #define MC0_CH0_CR_ROWHAMMER_CTL_REFM_SUB_MIN                        (0)
  #define MC0_CH0_CR_ROWHAMMER_CTL_REFM_SUB_MAX                        (4095) // 0x00000FFF

  #define MC0_CH0_CR_ROWHAMMER_CTL_REFM_SUB_DEF                        (0x00000016)
  #define MC0_CH0_CR_ROWHAMMER_CTL_REFM_SUB_HSH                        (0x4C58E128)


  #define MC0_CH0_CR_ROWHAMMER_CTL_REFm_EN_OFF                         (59)
  #define MC0_CH0_CR_ROWHAMMER_CTL_REFm_EN_WID                         ( 1)
  #define MC0_CH0_CR_ROWHAMMER_CTL_REFm_EN_MSK                         (0x0800000000000000ULL)
  #define MC0_CH0_CR_ROWHAMMER_CTL_REFm_EN_MIN                         (0)
  #define MC0_CH0_CR_ROWHAMMER_CTL_REFm_EN_MAX                         (1) // 0x00000001
  #define MC0_CH0_CR_ROWHAMMER_CTL_REFm_EN_DEF                         (0x00000001)
  #define MC0_CH0_CR_ROWHAMMER_CTL_REFm_EN_HSH                         (0x4176E128)


  #define MC0_CH0_CR_ROWHAMMER_CTL_RH_ACC_REF_EN_OFF                   (60)
  #define MC0_CH0_CR_ROWHAMMER_CTL_RH_ACC_REF_EN_WID                   ( 1)
  #define MC0_CH0_CR_ROWHAMMER_CTL_RH_ACC_REF_EN_MSK                   (0x1000000000000000ULL)
  #define MC0_CH0_CR_ROWHAMMER_CTL_RH_ACC_REF_EN_MIN                   (0)
  #define MC0_CH0_CR_ROWHAMMER_CTL_RH_ACC_REF_EN_MAX                   (1) // 0x00000001
  #define MC0_CH0_CR_ROWHAMMER_CTL_RH_ACC_REF_EN_DEF                   (0x00000000)
  #define MC0_CH0_CR_ROWHAMMER_CTL_RH_ACC_REF_EN_HSH                   (0x4178E128)


  #define MC0_CH0_CR_ROWHAMMER_CTL_RH_ACC_REF_RATE_OFF                 (61)
  #define MC0_CH0_CR_ROWHAMMER_CTL_RH_ACC_REF_RATE_WID                 ( 1)
  #define MC0_CH0_CR_ROWHAMMER_CTL_RH_ACC_REF_RATE_MSK                 (0x2000000000000000ULL)
  #define MC0_CH0_CR_ROWHAMMER_CTL_RH_ACC_REF_RATE_MIN                 (0)
  #define MC0_CH0_CR_ROWHAMMER_CTL_RH_ACC_REF_RATE_MAX                 (1) // 0x00000001
  #define MC0_CH0_CR_ROWHAMMER_CTL_RH_ACC_REF_RATE_DEF                 (0x00000000)
  #define MC0_CH0_CR_ROWHAMMER_CTL_RH_ACC_REF_RATE_HSH                 (0x417AE128)


  #define MC0_CH0_CR_ROWHAMMER_CTL_IGNORE_HIGH_WM_BLOCK_OFF            (62)
  #define MC0_CH0_CR_ROWHAMMER_CTL_IGNORE_HIGH_WM_BLOCK_WID            ( 1)
  #define MC0_CH0_CR_ROWHAMMER_CTL_IGNORE_HIGH_WM_BLOCK_MSK            (0x4000000000000000ULL)
  #define MC0_CH0_CR_ROWHAMMER_CTL_IGNORE_HIGH_WM_BLOCK_MIN            (0)
  #define MC0_CH0_CR_ROWHAMMER_CTL_IGNORE_HIGH_WM_BLOCK_MAX            (1) // 0x00000001
  #define MC0_CH0_CR_ROWHAMMER_CTL_IGNORE_HIGH_WM_BLOCK_DEF            (0x00000000)
  #define MC0_CH0_CR_ROWHAMMER_CTL_IGNORE_HIGH_WM_BLOCK_HSH            (0x417CE128)


  #define MC0_CH0_CR_ROWHAMMER_CTL_REFm_Disable_on_hot_OFF             (63)
  #define MC0_CH0_CR_ROWHAMMER_CTL_REFm_Disable_on_hot_WID             ( 1)
  #define MC0_CH0_CR_ROWHAMMER_CTL_REFm_Disable_on_hot_MSK             (0x8000000000000000ULL)
  #define MC0_CH0_CR_ROWHAMMER_CTL_REFm_Disable_on_hot_MIN             (0)
  #define MC0_CH0_CR_ROWHAMMER_CTL_REFm_Disable_on_hot_MAX             (1) // 0x00000001
  #define MC0_CH0_CR_ROWHAMMER_CTL_REFm_Disable_on_hot_DEF             (0x00000000)
  #define MC0_CH0_CR_ROWHAMMER_CTL_REFm_Disable_on_hot_HSH             (0x417EE128)
#define MC0_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_0_REG                (0x0000E200)

  #define MC0_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_0_BYTE_0_OFF       ( 0)
  #define MC0_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_0_BYTE_0_WID       ( 8)
  #define MC0_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_0_BYTE_0_MSK       (0x000000FF)
  #define MC0_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_0_BYTE_0_MIN       (0)
  #define MC0_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_0_BYTE_0_MAX       (255) // 0x000000FF
  #define MC0_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_0_BYTE_0_DEF       (0x00000000)
  #define MC0_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_0_BYTE_0_HSH       (0x0800E200)

  #define MC0_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_0_BYTE_1_OFF       ( 8)
  #define MC0_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_0_BYTE_1_WID       ( 8)
  #define MC0_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_0_BYTE_1_MSK       (0x0000FF00)
  #define MC0_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_0_BYTE_1_MIN       (0)
  #define MC0_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_0_BYTE_1_MAX       (255) // 0x000000FF
  #define MC0_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_0_BYTE_1_DEF       (0x00000000)
  #define MC0_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_0_BYTE_1_HSH       (0x0810E200)

  #define MC0_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_0_BYTE_2_OFF       (16)
  #define MC0_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_0_BYTE_2_WID       ( 8)
  #define MC0_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_0_BYTE_2_MSK       (0x00FF0000)
  #define MC0_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_0_BYTE_2_MIN       (0)
  #define MC0_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_0_BYTE_2_MAX       (255) // 0x000000FF
  #define MC0_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_0_BYTE_2_DEF       (0x00000000)
  #define MC0_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_0_BYTE_2_HSH       (0x0820E200)

  #define MC0_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_0_BYTE_3_OFF       (24)
  #define MC0_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_0_BYTE_3_WID       ( 8)
  #define MC0_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_0_BYTE_3_MSK       (0xFF000000)
  #define MC0_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_0_BYTE_3_MIN       (0)
  #define MC0_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_0_BYTE_3_MAX       (255) // 0x000000FF
  #define MC0_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_0_BYTE_3_DEF       (0x00000000)
  #define MC0_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_0_BYTE_3_HSH       (0x0830E200)

#define MC0_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_1_REG                (0x0000E204)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_0_REG

#define MC0_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_2_REG                (0x0000E208)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_0_REG

#define MC0_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_3_REG                (0x0000E20C)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_0_REG

#define MC0_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_4_REG                (0x0000E210)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_0_REG

#define MC0_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_5_REG                (0x0000E214)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_0_REG

#define MC0_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_6_REG                (0x0000E218)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_0_REG

#define MC0_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_7_REG                (0x0000E21C)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_0_REG

#define MC0_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_8_REG                (0x0000E220)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_0_REG

#define MC0_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_9_REG                (0x0000E224)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_0_REG

#define MC0_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_10_REG               (0x0000E228)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_0_REG

#define MC0_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_11_REG               (0x0000E22C)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_0_REG

#define MC0_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_12_REG               (0x0000E230)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_0_REG

#define MC0_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_13_REG               (0x0000E234)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_0_REG

#define MC0_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_14_REG               (0x0000E238)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_0_REG

#define MC0_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_15_REG               (0x0000E23C)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_0_REG

#define MC0_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_16_REG               (0x0000E240)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_0_REG

#define MC0_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_17_REG               (0x0000E244)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_0_REG

#define MC0_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_18_REG               (0x0000E248)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_0_REG

#define MC0_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_19_REG               (0x0000E24C)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_0_REG

#define MC0_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_20_REG               (0x0000E250)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_0_REG

#define MC0_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_21_REG               (0x0000E254)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_0_REG

#define MC0_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_22_REG               (0x0000E258)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_0_REG

#define MC0_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_23_REG               (0x0000E25C)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_0_REG

#define MC0_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_24_REG               (0x0000E260)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_0_REG

#define MC0_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_25_REG               (0x0000E264)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_0_REG

#define MC0_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_26_REG               (0x0000E268)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_0_REG

#define MC0_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_27_REG               (0x0000E26C)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_0_REG

#define MC0_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_28_REG               (0x0000E270)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_0_REG

#define MC0_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_29_REG               (0x0000E274)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_0_REG

#define MC0_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_30_REG               (0x0000E278)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_0_REG

#define MC0_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_31_REG               (0x0000E27C)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_0_REG

#define MC0_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_32_REG               (0x0000E280)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_0_REG

#define MC0_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_33_REG               (0x0000E284)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_0_REG

#define MC0_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_34_REG               (0x0000E288)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_0_REG

#define MC0_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_35_REG               (0x0000E28C)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_0_REG

#define MC0_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_36_REG               (0x0000E290)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_0_REG

#define MC0_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_37_REG               (0x0000E294)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_0_REG

#define MC0_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_38_REG               (0x0000E298)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_0_REG

#define MC0_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_39_REG               (0x0000E29C)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_0_REG

#define MC0_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_40_REG               (0x0000E2A0)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_0_REG

#define MC0_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_41_REG               (0x0000E2A4)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_0_REG

#define MC0_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_42_REG               (0x0000E2A8)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_0_REG

#define MC0_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_43_REG               (0x0000E2AC)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_0_REG

#define MC0_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_44_REG               (0x0000E2B0)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_0_REG

#define MC0_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_45_REG               (0x0000E2B4)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_0_REG

#define MC0_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_46_REG               (0x0000E2B8)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_0_REG

#define MC0_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_47_REG               (0x0000E2BC)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_0_REG

#define MC0_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_48_REG               (0x0000E2C0)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_0_REG

#define MC0_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_49_REG               (0x0000E2C4)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_0_REG

#define MC0_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_50_REG               (0x0000E2C8)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_0_REG

#define MC0_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_51_REG               (0x0000E2CC)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_0_REG

#define MC0_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_52_REG               (0x0000E2D0)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_0_REG

#define MC0_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_53_REG               (0x0000E2D4)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_0_REG

#define MC0_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_54_REG               (0x0000E2D8)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_0_REG

#define MC0_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_55_REG               (0x0000E2DC)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_0_REG

#define MC0_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_56_REG               (0x0000E2E0)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_0_REG

#define MC0_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_57_REG               (0x0000E2E4)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_0_REG

#define MC0_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_58_REG               (0x0000E2E8)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_0_REG

#define MC0_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_59_REG               (0x0000E2EC)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_0_REG

#define MC0_CH0_CR_GENERIC_MRS_FSM_TIMING_STORAGE_0_REG                (0x0000E3E0)

  #define MC0_CH0_CR_GENERIC_MRS_FSM_TIMING_STORAGE_0_TIMING_FIELD_0_OFF ( 0)
  #define MC0_CH0_CR_GENERIC_MRS_FSM_TIMING_STORAGE_0_TIMING_FIELD_0_WID ( 9)
  #define MC0_CH0_CR_GENERIC_MRS_FSM_TIMING_STORAGE_0_TIMING_FIELD_0_MSK (0x000001FF)
  #define MC0_CH0_CR_GENERIC_MRS_FSM_TIMING_STORAGE_0_TIMING_FIELD_0_MIN (0)
  #define MC0_CH0_CR_GENERIC_MRS_FSM_TIMING_STORAGE_0_TIMING_FIELD_0_MAX (511) // 0x000001FF
  #define MC0_CH0_CR_GENERIC_MRS_FSM_TIMING_STORAGE_0_TIMING_FIELD_0_DEF (0x00000000)
  #define MC0_CH0_CR_GENERIC_MRS_FSM_TIMING_STORAGE_0_TIMING_FIELD_0_HSH (0x0900E3E0)

  #define MC0_CH0_CR_GENERIC_MRS_FSM_TIMING_STORAGE_0_TIMING_FIELD_1_OFF ( 9)
  #define MC0_CH0_CR_GENERIC_MRS_FSM_TIMING_STORAGE_0_TIMING_FIELD_1_WID ( 9)
  #define MC0_CH0_CR_GENERIC_MRS_FSM_TIMING_STORAGE_0_TIMING_FIELD_1_MSK (0x0003FE00)
  #define MC0_CH0_CR_GENERIC_MRS_FSM_TIMING_STORAGE_0_TIMING_FIELD_1_MIN (0)
  #define MC0_CH0_CR_GENERIC_MRS_FSM_TIMING_STORAGE_0_TIMING_FIELD_1_MAX (511) // 0x000001FF
  #define MC0_CH0_CR_GENERIC_MRS_FSM_TIMING_STORAGE_0_TIMING_FIELD_1_DEF (0x00000000)
  #define MC0_CH0_CR_GENERIC_MRS_FSM_TIMING_STORAGE_0_TIMING_FIELD_1_HSH (0x0912E3E0)

  #define MC0_CH0_CR_GENERIC_MRS_FSM_TIMING_STORAGE_0_TIMING_FIELD_2_OFF (18)
  #define MC0_CH0_CR_GENERIC_MRS_FSM_TIMING_STORAGE_0_TIMING_FIELD_2_WID ( 9)
  #define MC0_CH0_CR_GENERIC_MRS_FSM_TIMING_STORAGE_0_TIMING_FIELD_2_MSK (0x07FC0000)
  #define MC0_CH0_CR_GENERIC_MRS_FSM_TIMING_STORAGE_0_TIMING_FIELD_2_MIN (0)
  #define MC0_CH0_CR_GENERIC_MRS_FSM_TIMING_STORAGE_0_TIMING_FIELD_2_MAX (511) // 0x000001FF
  #define MC0_CH0_CR_GENERIC_MRS_FSM_TIMING_STORAGE_0_TIMING_FIELD_2_DEF (0x00000000)
  #define MC0_CH0_CR_GENERIC_MRS_FSM_TIMING_STORAGE_0_TIMING_FIELD_2_HSH (0x0924E3E0)

#define MC0_CH0_CR_GENERIC_MRS_FSM_TIMING_STORAGE_1_REG                (0x0000E3E4)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_TIMING_STORAGE_0_REG

#define MC0_CH0_CR_MNTS_CBIT_REG                                       (0x0000E3EC)

  #define MC0_CH0_CR_MNTS_CBIT_AlwaysRefOnMRS_OFF                      ( 0)
  #define MC0_CH0_CR_MNTS_CBIT_AlwaysRefOnMRS_WID                      ( 1)
  #define MC0_CH0_CR_MNTS_CBIT_AlwaysRefOnMRS_MSK                      (0x00000001)
  #define MC0_CH0_CR_MNTS_CBIT_AlwaysRefOnMRS_MIN                      (0)
  #define MC0_CH0_CR_MNTS_CBIT_AlwaysRefOnMRS_MAX                      (1) // 0x00000001
  #define MC0_CH0_CR_MNTS_CBIT_AlwaysRefOnMRS_DEF                      (0x00000000)
  #define MC0_CH0_CR_MNTS_CBIT_AlwaysRefOnMRS_HSH                      (0x0100E3EC)

  #define MC0_CH0_CR_MNTS_CBIT_CounttREFIWhileRefEnOff_A0_OFF             ( 1)
  #define MC0_CH0_CR_MNTS_CBIT_CounttREFIWhileRefEnOff_A0_WID             ( 1)
  #define MC0_CH0_CR_MNTS_CBIT_CounttREFIWhileRefEnOff_A0_MSK             (0x00000002)
  #define MC0_CH0_CR_MNTS_CBIT_CounttREFIWhileRefEnOff_A0_MIN             (0)
  #define MC0_CH0_CR_MNTS_CBIT_CounttREFIWhileRefEnOff_A0_MAX             (1) // 0x00000001
  #define MC0_CH0_CR_MNTS_CBIT_CounttREFIWhileRefEnOff_A0_DEF             (0x00000000)
  #define MC0_CH0_CR_MNTS_CBIT_CounttREFIWhileRefEnOff_A0_HSH             (0x0102E3EC)

  #define MC0_CH0_CR_MNTS_CBIT_GENERIC_MRS_FSM_Breakpoint_Address_MSB_OFF ( 2)
  #define MC0_CH0_CR_MNTS_CBIT_GENERIC_MRS_FSM_Breakpoint_Address_MSB_WID ( 2)
  #define MC0_CH0_CR_MNTS_CBIT_GENERIC_MRS_FSM_Breakpoint_Address_MSB_MSK (0x0000000C)
  #define MC0_CH0_CR_MNTS_CBIT_GENERIC_MRS_FSM_Breakpoint_Address_MSB_MIN (0)
  #define MC0_CH0_CR_MNTS_CBIT_GENERIC_MRS_FSM_Breakpoint_Address_MSB_MAX (3) // 0x00000003
  #define MC0_CH0_CR_MNTS_CBIT_GENERIC_MRS_FSM_Breakpoint_Address_MSB_DEF (0x00000000)
  #define MC0_CH0_CR_MNTS_CBIT_GENERIC_MRS_FSM_Breakpoint_Address_MSB_HSH (0x0204E3EC)

  #define MC0_CH0_CR_MNTS_CBIT_DisCrossRankZqBlock_OFF                 ( 4)
  #define MC0_CH0_CR_MNTS_CBIT_DisCrossRankZqBlock_WID                 ( 1)
  #define MC0_CH0_CR_MNTS_CBIT_DisCrossRankZqBlock_MSK                 (0x00000010)
  #define MC0_CH0_CR_MNTS_CBIT_DisCrossRankZqBlock_MIN                 (0)
  #define MC0_CH0_CR_MNTS_CBIT_DisCrossRankZqBlock_MAX                 (1) // 0x00000001
  #define MC0_CH0_CR_MNTS_CBIT_DisCrossRankZqBlock_DEF                 (0x00000000)
  #define MC0_CH0_CR_MNTS_CBIT_DisCrossRankZqBlock_HSH                 (0x0108E3EC)

  #define MC0_CH0_CR_MNTS_CBIT_cbit_MRS_ignore_RH_block_OFF            ( 5)
  #define MC0_CH0_CR_MNTS_CBIT_cbit_MRS_ignore_RH_block_WID            ( 1)
  #define MC0_CH0_CR_MNTS_CBIT_cbit_MRS_ignore_RH_block_MSK            (0x00000020)
  #define MC0_CH0_CR_MNTS_CBIT_cbit_MRS_ignore_RH_block_MIN            (0)
  #define MC0_CH0_CR_MNTS_CBIT_cbit_MRS_ignore_RH_block_MAX            (1) // 0x00000001
  #define MC0_CH0_CR_MNTS_CBIT_cbit_MRS_ignore_RH_block_DEF            (0x00000000)
  #define MC0_CH0_CR_MNTS_CBIT_cbit_MRS_ignore_RH_block_HSH            (0x010AE3EC)
#define MC0_CH0_CR_RH_TRR_LFSR_REG                                     (0x0000E3F0)

  #define MC0_CH0_CR_RH_TRR_LFSR_LFSR_0_OFF                            ( 0)
  #define MC0_CH0_CR_RH_TRR_LFSR_LFSR_0_WID                            (32)
  #define MC0_CH0_CR_RH_TRR_LFSR_LFSR_0_MSK                            (0xFFFFFFFF)
  #define MC0_CH0_CR_RH_TRR_LFSR_LFSR_0_MIN                            (0)
  #define MC0_CH0_CR_RH_TRR_LFSR_LFSR_0_MAX                            (4294967295) // 0xFFFFFFFF
  #define MC0_CH0_CR_RH_TRR_LFSR_LFSR_0_DEF                            (0x00000000)
  #define MC0_CH0_CR_RH_TRR_LFSR_LFSR_0_HSH                            (0x6000E3F0)

  #define MC0_CH0_CR_RH_TRR_LFSR_LFSR_1_OFF                            (32)
  #define MC0_CH0_CR_RH_TRR_LFSR_LFSR_1_WID                            (32)
  #define MC0_CH0_CR_RH_TRR_LFSR_LFSR_1_MSK                            (0xFFFFFFFF00000000ULL)
  #define MC0_CH0_CR_RH_TRR_LFSR_LFSR_1_MIN                            (0)
  #define MC0_CH0_CR_RH_TRR_LFSR_LFSR_1_MAX                            (4294967295) // 0xFFFFFFFF
  #define MC0_CH0_CR_RH_TRR_LFSR_LFSR_1_DEF                            (0x00000000)
  #define MC0_CH0_CR_RH_TRR_LFSR_LFSR_1_HSH                            (0x6040E3F0)

#define MC0_CH0_CR_WDB_CAPTURE_CTL_REG                                 (0x0000E3F8)

  #define MC0_CH0_CR_WDB_CAPTURE_CTL_Enable_WDB_Capture_OFF            ( 0)
  #define MC0_CH0_CR_WDB_CAPTURE_CTL_Enable_WDB_Capture_WID            ( 1)
  #define MC0_CH0_CR_WDB_CAPTURE_CTL_Enable_WDB_Capture_MSK            (0x00000001)
  #define MC0_CH0_CR_WDB_CAPTURE_CTL_Enable_WDB_Capture_MIN            (0)
  #define MC0_CH0_CR_WDB_CAPTURE_CTL_Enable_WDB_Capture_MAX            (1) // 0x00000001
  #define MC0_CH0_CR_WDB_CAPTURE_CTL_Enable_WDB_Capture_DEF            (0x00000000)
  #define MC0_CH0_CR_WDB_CAPTURE_CTL_Enable_WDB_Capture_HSH            (0x0100E3F8)

  #define MC0_CH0_CR_WDB_CAPTURE_CTL_WDB_Starting_Capture_Pointer_OFF  ( 8)
  #define MC0_CH0_CR_WDB_CAPTURE_CTL_WDB_Starting_Capture_Pointer_WID  ( 6)
  #define MC0_CH0_CR_WDB_CAPTURE_CTL_WDB_Starting_Capture_Pointer_MSK  (0x00003F00)
  #define MC0_CH0_CR_WDB_CAPTURE_CTL_WDB_Starting_Capture_Pointer_MIN  (0)
  #define MC0_CH0_CR_WDB_CAPTURE_CTL_WDB_Starting_Capture_Pointer_MAX  (63) // 0x0000003F
  #define MC0_CH0_CR_WDB_CAPTURE_CTL_WDB_Starting_Capture_Pointer_DEF  (0x00000000)
  #define MC0_CH0_CR_WDB_CAPTURE_CTL_WDB_Starting_Capture_Pointer_HSH  (0x0610E3F8)

  #define MC0_CH0_CR_WDB_CAPTURE_CTL_WDB_Ending_Capture_Pointer_OFF    (16)
  #define MC0_CH0_CR_WDB_CAPTURE_CTL_WDB_Ending_Capture_Pointer_WID    ( 6)
  #define MC0_CH0_CR_WDB_CAPTURE_CTL_WDB_Ending_Capture_Pointer_MSK    (0x003F0000)
  #define MC0_CH0_CR_WDB_CAPTURE_CTL_WDB_Ending_Capture_Pointer_MIN    (0)
  #define MC0_CH0_CR_WDB_CAPTURE_CTL_WDB_Ending_Capture_Pointer_MAX    (63) // 0x0000003F
  #define MC0_CH0_CR_WDB_CAPTURE_CTL_WDB_Ending_Capture_Pointer_DEF    (0x00000000)
  #define MC0_CH0_CR_WDB_CAPTURE_CTL_WDB_Ending_Capture_Pointer_HSH    (0x0620E3F8)

  #define MC0_CH0_CR_WDB_CAPTURE_CTL_WDB_Capture_Block_SubCh_OFF       (22)
  #define MC0_CH0_CR_WDB_CAPTURE_CTL_WDB_Capture_Block_SubCh_WID       ( 2)
  #define MC0_CH0_CR_WDB_CAPTURE_CTL_WDB_Capture_Block_SubCh_MSK       (0x00C00000)
  #define MC0_CH0_CR_WDB_CAPTURE_CTL_WDB_Capture_Block_SubCh_MIN       (0)
  #define MC0_CH0_CR_WDB_CAPTURE_CTL_WDB_Capture_Block_SubCh_MAX       (3) // 0x00000003
  #define MC0_CH0_CR_WDB_CAPTURE_CTL_WDB_Capture_Block_SubCh_DEF       (0x00000000)
  #define MC0_CH0_CR_WDB_CAPTURE_CTL_WDB_Capture_Block_SubCh_HSH       (0x022CE3F8)

#define MC0_CH0_CR_WDB_CAPTURE_STATUS_REG                              (0x0000E3FC)

  #define MC0_CH0_CR_WDB_CAPTURE_STATUS_WDB_SC0_Current_Capture_Pointer_OFF ( 0)
  #define MC0_CH0_CR_WDB_CAPTURE_STATUS_WDB_SC0_Current_Capture_Pointer_WID ( 6)
  #define MC0_CH0_CR_WDB_CAPTURE_STATUS_WDB_SC0_Current_Capture_Pointer_MSK (0x0000003F)
  #define MC0_CH0_CR_WDB_CAPTURE_STATUS_WDB_SC0_Current_Capture_Pointer_MIN (0)
  #define MC0_CH0_CR_WDB_CAPTURE_STATUS_WDB_SC0_Current_Capture_Pointer_MAX (63) // 0x0000003F
  #define MC0_CH0_CR_WDB_CAPTURE_STATUS_WDB_SC0_Current_Capture_Pointer_DEF (0x00000000)
  #define MC0_CH0_CR_WDB_CAPTURE_STATUS_WDB_SC0_Current_Capture_Pointer_HSH (0x0600E3FC)

  #define MC0_CH0_CR_WDB_CAPTURE_STATUS_WDB_SC1_Current_Capture_Pointer_OFF ( 8)
  #define MC0_CH0_CR_WDB_CAPTURE_STATUS_WDB_SC1_Current_Capture_Pointer_WID ( 6)
  #define MC0_CH0_CR_WDB_CAPTURE_STATUS_WDB_SC1_Current_Capture_Pointer_MSK (0x00003F00)
  #define MC0_CH0_CR_WDB_CAPTURE_STATUS_WDB_SC1_Current_Capture_Pointer_MIN (0)
  #define MC0_CH0_CR_WDB_CAPTURE_STATUS_WDB_SC1_Current_Capture_Pointer_MAX (63) // 0x0000003F
  #define MC0_CH0_CR_WDB_CAPTURE_STATUS_WDB_SC1_Current_Capture_Pointer_DEF (0x00000000)
  #define MC0_CH0_CR_WDB_CAPTURE_STATUS_WDB_SC1_Current_Capture_Pointer_HSH (0x0610E3FC)

#define MC0_CH0_CR_RH_TRR_CONTROL_REG                                  (0x0000E400)

  #define MC0_CH0_CR_RH_TRR_CONTROL_TRR_Dimm_Enabled_OFF               ( 0)
  #define MC0_CH0_CR_RH_TRR_CONTROL_TRR_Dimm_Enabled_WID               ( 2)
  #define MC0_CH0_CR_RH_TRR_CONTROL_TRR_Dimm_Enabled_MSK               (0x00000003)
  #define MC0_CH0_CR_RH_TRR_CONTROL_TRR_Dimm_Enabled_MIN               (0)
  #define MC0_CH0_CR_RH_TRR_CONTROL_TRR_Dimm_Enabled_MAX               (3) // 0x00000003
  #define MC0_CH0_CR_RH_TRR_CONTROL_TRR_Dimm_Enabled_DEF               (0x00000000)
  #define MC0_CH0_CR_RH_TRR_CONTROL_TRR_Dimm_Enabled_HSH               (0x0200E400)

  #define MC0_CH0_CR_RH_TRR_CONTROL_LFSR_0_MASK_OFF                    ( 6)
  #define MC0_CH0_CR_RH_TRR_CONTROL_LFSR_0_MASK_WID                    ( 4)
  #define MC0_CH0_CR_RH_TRR_CONTROL_LFSR_0_MASK_MSK                    (0x000003C0)
  #define MC0_CH0_CR_RH_TRR_CONTROL_LFSR_0_MASK_MIN                    (0)
  #define MC0_CH0_CR_RH_TRR_CONTROL_LFSR_0_MASK_MAX                    (15) // 0x0000000F
  #define MC0_CH0_CR_RH_TRR_CONTROL_LFSR_0_MASK_DEF                    (0x00000000)
  #define MC0_CH0_CR_RH_TRR_CONTROL_LFSR_0_MASK_HSH                    (0x040CE400)

  #define MC0_CH0_CR_RH_TRR_CONTROL_LFSR_1_MASK_OFF                    (10)
  #define MC0_CH0_CR_RH_TRR_CONTROL_LFSR_1_MASK_WID                    ( 4)
  #define MC0_CH0_CR_RH_TRR_CONTROL_LFSR_1_MASK_MSK                    (0x00003C00)
  #define MC0_CH0_CR_RH_TRR_CONTROL_LFSR_1_MASK_MIN                    (0)
  #define MC0_CH0_CR_RH_TRR_CONTROL_LFSR_1_MASK_MAX                    (15) // 0x0000000F
  #define MC0_CH0_CR_RH_TRR_CONTROL_LFSR_1_MASK_DEF                    (0x00000000)
  #define MC0_CH0_CR_RH_TRR_CONTROL_LFSR_1_MASK_HSH                    (0x0414E400)

  #define MC0_CH0_CR_RH_TRR_CONTROL_MA0_Swizzling_OFF                  (14)
  #define MC0_CH0_CR_RH_TRR_CONTROL_MA0_Swizzling_WID                  ( 2)
  #define MC0_CH0_CR_RH_TRR_CONTROL_MA0_Swizzling_MSK                  (0x0000C000)
  #define MC0_CH0_CR_RH_TRR_CONTROL_MA0_Swizzling_MIN                  (0)
  #define MC0_CH0_CR_RH_TRR_CONTROL_MA0_Swizzling_MAX                  (3) // 0x00000003
  #define MC0_CH0_CR_RH_TRR_CONTROL_MA0_Swizzling_DEF                  (0x00000000)
  #define MC0_CH0_CR_RH_TRR_CONTROL_MA0_Swizzling_HSH                  (0x021CE400)

  #define MC0_CH0_CR_RH_TRR_CONTROL_MA3_Swizzling_OFF                  (16)
  #define MC0_CH0_CR_RH_TRR_CONTROL_MA3_Swizzling_WID                  ( 2)
  #define MC0_CH0_CR_RH_TRR_CONTROL_MA3_Swizzling_MSK                  (0x00030000)
  #define MC0_CH0_CR_RH_TRR_CONTROL_MA3_Swizzling_MIN                  (0)
  #define MC0_CH0_CR_RH_TRR_CONTROL_MA3_Swizzling_MAX                  (3) // 0x00000003
  #define MC0_CH0_CR_RH_TRR_CONTROL_MA3_Swizzling_DEF                  (0x00000000)
  #define MC0_CH0_CR_RH_TRR_CONTROL_MA3_Swizzling_HSH                  (0x0220E400)

  #define MC0_CH0_CR_RH_TRR_CONTROL_Micron_R_Swizzling_OFF             (18)
  #define MC0_CH0_CR_RH_TRR_CONTROL_Micron_R_Swizzling_WID             ( 2)
  #define MC0_CH0_CR_RH_TRR_CONTROL_Micron_R_Swizzling_MSK             (0x000C0000)
  #define MC0_CH0_CR_RH_TRR_CONTROL_Micron_R_Swizzling_MIN             (0)
  #define MC0_CH0_CR_RH_TRR_CONTROL_Micron_R_Swizzling_MAX             (3) // 0x00000003
  #define MC0_CH0_CR_RH_TRR_CONTROL_Micron_R_Swizzling_DEF             (0x00000000)
  #define MC0_CH0_CR_RH_TRR_CONTROL_Micron_R_Swizzling_HSH             (0x0224E400)

  #define MC0_CH0_CR_RH_TRR_CONTROL_Micron_F_Swizzling_OFF             (20)
  #define MC0_CH0_CR_RH_TRR_CONTROL_Micron_F_Swizzling_WID             ( 2)
  #define MC0_CH0_CR_RH_TRR_CONTROL_Micron_F_Swizzling_MSK             (0x00300000)
  #define MC0_CH0_CR_RH_TRR_CONTROL_Micron_F_Swizzling_MIN             (0)
  #define MC0_CH0_CR_RH_TRR_CONTROL_Micron_F_Swizzling_MAX             (3) // 0x00000003
  #define MC0_CH0_CR_RH_TRR_CONTROL_Micron_F_Swizzling_DEF             (0x00000000)
  #define MC0_CH0_CR_RH_TRR_CONTROL_Micron_F_Swizzling_HSH             (0x0228E400)

  #define MC0_CH0_CR_RH_TRR_CONTROL_BlockState_Delay_OFF               (22)
  #define MC0_CH0_CR_RH_TRR_CONTROL_BlockState_Delay_WID               ( 5)
  #define MC0_CH0_CR_RH_TRR_CONTROL_BlockState_Delay_MSK               (0x07C00000)
  #define MC0_CH0_CR_RH_TRR_CONTROL_BlockState_Delay_MIN               (0)
  #define MC0_CH0_CR_RH_TRR_CONTROL_BlockState_Delay_MAX               (31) // 0x0000001F
  #define MC0_CH0_CR_RH_TRR_CONTROL_BlockState_Delay_DEF               (0x00000000)
  #define MC0_CH0_CR_RH_TRR_CONTROL_BlockState_Delay_HSH               (0x052CE400)
#define MC0_CH0_CR_REUT_CH_MISC_REFRESH_CTRL_REG                       (0x0000E404)

  #define MC0_CH0_CR_REUT_CH_MISC_REFRESH_CTRL_Refresh_Rank_Mask_OFF   ( 0)
  #define MC0_CH0_CR_REUT_CH_MISC_REFRESH_CTRL_Refresh_Rank_Mask_WID   ( 4)
  #define MC0_CH0_CR_REUT_CH_MISC_REFRESH_CTRL_Refresh_Rank_Mask_MSK   (0x0000000F)
  #define MC0_CH0_CR_REUT_CH_MISC_REFRESH_CTRL_Refresh_Rank_Mask_MIN   (0)
  #define MC0_CH0_CR_REUT_CH_MISC_REFRESH_CTRL_Refresh_Rank_Mask_MAX   (15) // 0x0000000F
  #define MC0_CH0_CR_REUT_CH_MISC_REFRESH_CTRL_Refresh_Rank_Mask_DEF   (0x0000000F)
  #define MC0_CH0_CR_REUT_CH_MISC_REFRESH_CTRL_Refresh_Rank_Mask_HSH   (0x0400E404)

  #define MC0_CH0_CR_REUT_CH_MISC_REFRESH_CTRL_RefZQ_En_Start_Test_Sync_OFF ( 8)
  #define MC0_CH0_CR_REUT_CH_MISC_REFRESH_CTRL_RefZQ_En_Start_Test_Sync_WID ( 1)
  #define MC0_CH0_CR_REUT_CH_MISC_REFRESH_CTRL_RefZQ_En_Start_Test_Sync_MSK (0x00000100)
  #define MC0_CH0_CR_REUT_CH_MISC_REFRESH_CTRL_RefZQ_En_Start_Test_Sync_MIN (0)
  #define MC0_CH0_CR_REUT_CH_MISC_REFRESH_CTRL_RefZQ_En_Start_Test_Sync_MAX (1) // 0x00000001
  #define MC0_CH0_CR_REUT_CH_MISC_REFRESH_CTRL_RefZQ_En_Start_Test_Sync_DEF (0x00000000)
  #define MC0_CH0_CR_REUT_CH_MISC_REFRESH_CTRL_RefZQ_En_Start_Test_Sync_HSH (0x0110E404)

  #define MC0_CH0_CR_REUT_CH_MISC_REFRESH_CTRL_Panic_Refresh_Only_OFF  (31)
  #define MC0_CH0_CR_REUT_CH_MISC_REFRESH_CTRL_Panic_Refresh_Only_WID  ( 1)
  #define MC0_CH0_CR_REUT_CH_MISC_REFRESH_CTRL_Panic_Refresh_Only_MSK  (0x80000000)
  #define MC0_CH0_CR_REUT_CH_MISC_REFRESH_CTRL_Panic_Refresh_Only_MIN  (0)
  #define MC0_CH0_CR_REUT_CH_MISC_REFRESH_CTRL_Panic_Refresh_Only_MAX  (1) // 0x00000001
  #define MC0_CH0_CR_REUT_CH_MISC_REFRESH_CTRL_Panic_Refresh_Only_DEF  (0x00000000)
  #define MC0_CH0_CR_REUT_CH_MISC_REFRESH_CTRL_Panic_Refresh_Only_HSH  (0x013EE404)

#define MC0_CH0_CR_REUT_CH_MISC_ZQ_CTRL_REG                            (0x0000E408)

  #define MC0_CH0_CR_REUT_CH_MISC_ZQ_CTRL_ZQ_Rank_Mask_OFF             ( 0)
  #define MC0_CH0_CR_REUT_CH_MISC_ZQ_CTRL_ZQ_Rank_Mask_WID             ( 4)
  #define MC0_CH0_CR_REUT_CH_MISC_ZQ_CTRL_ZQ_Rank_Mask_MSK             (0x0000000F)
  #define MC0_CH0_CR_REUT_CH_MISC_ZQ_CTRL_ZQ_Rank_Mask_MIN             (0)
  #define MC0_CH0_CR_REUT_CH_MISC_ZQ_CTRL_ZQ_Rank_Mask_MAX             (15) // 0x0000000F
  #define MC0_CH0_CR_REUT_CH_MISC_ZQ_CTRL_ZQ_Rank_Mask_DEF             (0x00000000)
  #define MC0_CH0_CR_REUT_CH_MISC_ZQ_CTRL_ZQ_Rank_Mask_HSH             (0x0400E408)

  #define MC0_CH0_CR_REUT_CH_MISC_ZQ_CTRL_Always_Do_ZQ_OFF             (31)
  #define MC0_CH0_CR_REUT_CH_MISC_ZQ_CTRL_Always_Do_ZQ_WID             ( 1)
  #define MC0_CH0_CR_REUT_CH_MISC_ZQ_CTRL_Always_Do_ZQ_MSK             (0x80000000)
  #define MC0_CH0_CR_REUT_CH_MISC_ZQ_CTRL_Always_Do_ZQ_MIN             (0)
  #define MC0_CH0_CR_REUT_CH_MISC_ZQ_CTRL_Always_Do_ZQ_MAX             (1) // 0x00000001
  #define MC0_CH0_CR_REUT_CH_MISC_ZQ_CTRL_Always_Do_ZQ_DEF             (0x00000000)
  #define MC0_CH0_CR_REUT_CH_MISC_ZQ_CTRL_Always_Do_ZQ_HSH             (0x013EE408)

#define MC0_CH0_CR_TC_REFM_REG                                         (0x0000E40C)

  #define MC0_CH0_CR_TC_REFM_tRFM_OFF                                  ( 0)
  #define MC0_CH0_CR_TC_REFM_tRFM_WID                                  (11)
  #define MC0_CH0_CR_TC_REFM_tRFM_MSK                                  (0x000007FF)
  #define MC0_CH0_CR_TC_REFM_tRFM_MIN                                  (0)
  #define MC0_CH0_CR_TC_REFM_tRFM_MAX                                  (2047) // 0x000007FF
  #define MC0_CH0_CR_TC_REFM_tRFM_DEF                                  (0x0000003C)
  #define MC0_CH0_CR_TC_REFM_tRFM_HSH                                  (0x0B00E40C)

#define MC0_CH0_CR_DDR_MR_PARAMS_REG                                   (0x0000E410)

  #define MC0_CH0_CR_DDR_MR_PARAMS_Rank_0_width_OFF                    ( 0)
  #define MC0_CH0_CR_DDR_MR_PARAMS_Rank_0_width_WID                    ( 2)
  #define MC0_CH0_CR_DDR_MR_PARAMS_Rank_0_width_MSK                    (0x00000003)
  #define MC0_CH0_CR_DDR_MR_PARAMS_Rank_0_width_MIN                    (0)
  #define MC0_CH0_CR_DDR_MR_PARAMS_Rank_0_width_MAX                    (3) // 0x00000003
  #define MC0_CH0_CR_DDR_MR_PARAMS_Rank_0_width_DEF                    (0x00000000)
  #define MC0_CH0_CR_DDR_MR_PARAMS_Rank_0_width_HSH                    (0x0200E410)

  #define MC0_CH0_CR_DDR_MR_PARAMS_Rank_1_width_OFF                    ( 2)
  #define MC0_CH0_CR_DDR_MR_PARAMS_Rank_1_width_WID                    ( 2)
  #define MC0_CH0_CR_DDR_MR_PARAMS_Rank_1_width_MSK                    (0x0000000C)
  #define MC0_CH0_CR_DDR_MR_PARAMS_Rank_1_width_MIN                    (0)
  #define MC0_CH0_CR_DDR_MR_PARAMS_Rank_1_width_MAX                    (3) // 0x00000003
  #define MC0_CH0_CR_DDR_MR_PARAMS_Rank_1_width_DEF                    (0x00000000)
  #define MC0_CH0_CR_DDR_MR_PARAMS_Rank_1_width_HSH                    (0x0204E410)

  #define MC0_CH0_CR_DDR_MR_PARAMS_Rank_2_width_OFF                    ( 4)
  #define MC0_CH0_CR_DDR_MR_PARAMS_Rank_2_width_WID                    ( 2)
  #define MC0_CH0_CR_DDR_MR_PARAMS_Rank_2_width_MSK                    (0x00000030)
  #define MC0_CH0_CR_DDR_MR_PARAMS_Rank_2_width_MIN                    (0)
  #define MC0_CH0_CR_DDR_MR_PARAMS_Rank_2_width_MAX                    (3) // 0x00000003
  #define MC0_CH0_CR_DDR_MR_PARAMS_Rank_2_width_DEF                    (0x00000000)
  #define MC0_CH0_CR_DDR_MR_PARAMS_Rank_2_width_HSH                    (0x0208E410)

  #define MC0_CH0_CR_DDR_MR_PARAMS_Rank_3_width_OFF                    ( 6)
  #define MC0_CH0_CR_DDR_MR_PARAMS_Rank_3_width_WID                    ( 2)
  #define MC0_CH0_CR_DDR_MR_PARAMS_Rank_3_width_MSK                    (0x000000C0)
  #define MC0_CH0_CR_DDR_MR_PARAMS_Rank_3_width_MIN                    (0)
  #define MC0_CH0_CR_DDR_MR_PARAMS_Rank_3_width_MAX                    (3) // 0x00000003
  #define MC0_CH0_CR_DDR_MR_PARAMS_Rank_3_width_DEF                    (0x00000000)
  #define MC0_CH0_CR_DDR_MR_PARAMS_Rank_3_width_HSH                    (0x020CE410)

  #define MC0_CH0_CR_DDR_MR_PARAMS_MR4_PERIOD_OFF                      ( 8)
  #define MC0_CH0_CR_DDR_MR_PARAMS_MR4_PERIOD_WID                      (16)
  #define MC0_CH0_CR_DDR_MR_PARAMS_MR4_PERIOD_MSK                      (0x00FFFF00)
  #define MC0_CH0_CR_DDR_MR_PARAMS_MR4_PERIOD_MIN                      (0)
  #define MC0_CH0_CR_DDR_MR_PARAMS_MR4_PERIOD_MAX                      (65535) // 0x0000FFFF
  #define MC0_CH0_CR_DDR_MR_PARAMS_MR4_PERIOD_DEF                      (0x00000000)
  #define MC0_CH0_CR_DDR_MR_PARAMS_MR4_PERIOD_HSH                      (0x1010E410)

  #define MC0_CH0_CR_DDR_MR_PARAMS_DDR4_TS_readout_en_OFF              (24)
  #define MC0_CH0_CR_DDR_MR_PARAMS_DDR4_TS_readout_en_WID              ( 1)
  #define MC0_CH0_CR_DDR_MR_PARAMS_DDR4_TS_readout_en_MSK              (0x01000000)
  #define MC0_CH0_CR_DDR_MR_PARAMS_DDR4_TS_readout_en_MIN              (0)
  #define MC0_CH0_CR_DDR_MR_PARAMS_DDR4_TS_readout_en_MAX              (1) // 0x00000001
  #define MC0_CH0_CR_DDR_MR_PARAMS_DDR4_TS_readout_en_DEF              (0x00000001)
  #define MC0_CH0_CR_DDR_MR_PARAMS_DDR4_TS_readout_en_HSH              (0x0130E410)

#define MC0_CH0_CR_DDR_MR_COMMAND_REG                                  (0x0000E414)

  #define MC0_CH0_CR_DDR_MR_COMMAND_Address_OFF                        ( 0)
  #define MC0_CH0_CR_DDR_MR_COMMAND_Address_WID                        ( 9)
  #define MC0_CH0_CR_DDR_MR_COMMAND_Address_MSK                        (0x000001FF)
  #define MC0_CH0_CR_DDR_MR_COMMAND_Address_MIN                        (0)
  #define MC0_CH0_CR_DDR_MR_COMMAND_Address_MAX                        (511) // 0x000001FF
  #define MC0_CH0_CR_DDR_MR_COMMAND_Address_DEF                        (0x00000000)
  #define MC0_CH0_CR_DDR_MR_COMMAND_Address_HSH                        (0x0900E414)

  #define MC0_CH0_CR_DDR_MR_COMMAND_Data_OFF                           ( 9)
  #define MC0_CH0_CR_DDR_MR_COMMAND_Data_WID                           ( 8)
  #define MC0_CH0_CR_DDR_MR_COMMAND_Data_MSK                           (0x0001FE00)
  #define MC0_CH0_CR_DDR_MR_COMMAND_Data_MIN                           (0)
  #define MC0_CH0_CR_DDR_MR_COMMAND_Data_MAX                           (255) // 0x000000FF
  #define MC0_CH0_CR_DDR_MR_COMMAND_Data_DEF                           (0x00000000)
  #define MC0_CH0_CR_DDR_MR_COMMAND_Data_HSH                           (0x0812E414)

  #define MC0_CH0_CR_DDR_MR_COMMAND_Rank_OFF                           (17)
  #define MC0_CH0_CR_DDR_MR_COMMAND_Rank_WID                           ( 2)
  #define MC0_CH0_CR_DDR_MR_COMMAND_Rank_MSK                           (0x00060000)
  #define MC0_CH0_CR_DDR_MR_COMMAND_Rank_MIN                           (0)
  #define MC0_CH0_CR_DDR_MR_COMMAND_Rank_MAX                           (3) // 0x00000003
  #define MC0_CH0_CR_DDR_MR_COMMAND_Rank_DEF                           (0x00000000)
  #define MC0_CH0_CR_DDR_MR_COMMAND_Rank_HSH                           (0x0222E414)

  #define MC0_CH0_CR_DDR_MR_COMMAND_Command_OFF                        (19)
  #define MC0_CH0_CR_DDR_MR_COMMAND_Command_WID                        ( 2)
  #define MC0_CH0_CR_DDR_MR_COMMAND_Command_MSK                        (0x00180000)
  #define MC0_CH0_CR_DDR_MR_COMMAND_Command_MIN                        (0)
  #define MC0_CH0_CR_DDR_MR_COMMAND_Command_MAX                        (3) // 0x00000003
  #define MC0_CH0_CR_DDR_MR_COMMAND_Command_DEF                        (0x00000000)
  #define MC0_CH0_CR_DDR_MR_COMMAND_Command_HSH                        (0x0226E414)

  #define MC0_CH0_CR_DDR_MR_COMMAND_DRAM_mask_OFF                      (21)
  #define MC0_CH0_CR_DDR_MR_COMMAND_DRAM_mask_WID                      ( 9)
  #define MC0_CH0_CR_DDR_MR_COMMAND_DRAM_mask_MSK                      (0x3FE00000)
  #define MC0_CH0_CR_DDR_MR_COMMAND_DRAM_mask_MIN                      (0)
  #define MC0_CH0_CR_DDR_MR_COMMAND_DRAM_mask_MAX                      (511) // 0x000001FF
  #define MC0_CH0_CR_DDR_MR_COMMAND_DRAM_mask_DEF                      (0x00000000)
  #define MC0_CH0_CR_DDR_MR_COMMAND_DRAM_mask_HSH                      (0x092AE414)

  #define MC0_CH0_CR_DDR_MR_COMMAND_Assume_idle_OFF                    (30)
  #define MC0_CH0_CR_DDR_MR_COMMAND_Assume_idle_WID                    ( 1)
  #define MC0_CH0_CR_DDR_MR_COMMAND_Assume_idle_MSK                    (0x40000000)
  #define MC0_CH0_CR_DDR_MR_COMMAND_Assume_idle_MIN                    (0)
  #define MC0_CH0_CR_DDR_MR_COMMAND_Assume_idle_MAX                    (1) // 0x00000001
  #define MC0_CH0_CR_DDR_MR_COMMAND_Assume_idle_DEF                    (0x00000000)
  #define MC0_CH0_CR_DDR_MR_COMMAND_Assume_idle_HSH                    (0x013CE414)

  #define MC0_CH0_CR_DDR_MR_COMMAND_Busy_OFF                           (31)
  #define MC0_CH0_CR_DDR_MR_COMMAND_Busy_WID                           ( 1)
  #define MC0_CH0_CR_DDR_MR_COMMAND_Busy_MSK                           (0x80000000)
  #define MC0_CH0_CR_DDR_MR_COMMAND_Busy_MIN                           (0)
  #define MC0_CH0_CR_DDR_MR_COMMAND_Busy_MAX                           (1) // 0x00000001
  #define MC0_CH0_CR_DDR_MR_COMMAND_Busy_DEF                           (0x00000000)
  #define MC0_CH0_CR_DDR_MR_COMMAND_Busy_HSH                           (0x013EE414)

#define MC0_CH0_CR_DDR_MR_RESULT_0_REG                                 (0x0000E418)

  #define MC0_CH0_CR_DDR_MR_RESULT_0_Device_0_OFF                      ( 0)
  #define MC0_CH0_CR_DDR_MR_RESULT_0_Device_0_WID                      ( 8)
  #define MC0_CH0_CR_DDR_MR_RESULT_0_Device_0_MSK                      (0x000000FF)
  #define MC0_CH0_CR_DDR_MR_RESULT_0_Device_0_MIN                      (0)
  #define MC0_CH0_CR_DDR_MR_RESULT_0_Device_0_MAX                      (255) // 0x000000FF
  #define MC0_CH0_CR_DDR_MR_RESULT_0_Device_0_DEF                      (0x00000000)
  #define MC0_CH0_CR_DDR_MR_RESULT_0_Device_0_HSH                      (0x0800E418)

  #define MC0_CH0_CR_DDR_MR_RESULT_0_Device_1_OFF                      ( 8)
  #define MC0_CH0_CR_DDR_MR_RESULT_0_Device_1_WID                      ( 8)
  #define MC0_CH0_CR_DDR_MR_RESULT_0_Device_1_MSK                      (0x0000FF00)
  #define MC0_CH0_CR_DDR_MR_RESULT_0_Device_1_MIN                      (0)
  #define MC0_CH0_CR_DDR_MR_RESULT_0_Device_1_MAX                      (255) // 0x000000FF
  #define MC0_CH0_CR_DDR_MR_RESULT_0_Device_1_DEF                      (0x00000000)
  #define MC0_CH0_CR_DDR_MR_RESULT_0_Device_1_HSH                      (0x0810E418)

  #define MC0_CH0_CR_DDR_MR_RESULT_0_Device_2_OFF                      (16)
  #define MC0_CH0_CR_DDR_MR_RESULT_0_Device_2_WID                      ( 8)
  #define MC0_CH0_CR_DDR_MR_RESULT_0_Device_2_MSK                      (0x00FF0000)
  #define MC0_CH0_CR_DDR_MR_RESULT_0_Device_2_MIN                      (0)
  #define MC0_CH0_CR_DDR_MR_RESULT_0_Device_2_MAX                      (255) // 0x000000FF
  #define MC0_CH0_CR_DDR_MR_RESULT_0_Device_2_DEF                      (0x00000000)
  #define MC0_CH0_CR_DDR_MR_RESULT_0_Device_2_HSH                      (0x0820E418)

  #define MC0_CH0_CR_DDR_MR_RESULT_0_Device_3_OFF                      (24)
  #define MC0_CH0_CR_DDR_MR_RESULT_0_Device_3_WID                      ( 8)
  #define MC0_CH0_CR_DDR_MR_RESULT_0_Device_3_MSK                      (0xFF000000)
  #define MC0_CH0_CR_DDR_MR_RESULT_0_Device_3_MIN                      (0)
  #define MC0_CH0_CR_DDR_MR_RESULT_0_Device_3_MAX                      (255) // 0x000000FF
  #define MC0_CH0_CR_DDR_MR_RESULT_0_Device_3_DEF                      (0x00000000)
  #define MC0_CH0_CR_DDR_MR_RESULT_0_Device_3_HSH                      (0x0830E418)

#define MC0_CH0_CR_DDR_MR_RESULT_1_REG                                 (0x0000E41C)

  #define MC0_CH0_CR_DDR_MR_RESULT_1_Device_4_OFF                      ( 0)
  #define MC0_CH0_CR_DDR_MR_RESULT_1_Device_4_WID                      ( 8)
  #define MC0_CH0_CR_DDR_MR_RESULT_1_Device_4_MSK                      (0x000000FF)
  #define MC0_CH0_CR_DDR_MR_RESULT_1_Device_4_MIN                      (0)
  #define MC0_CH0_CR_DDR_MR_RESULT_1_Device_4_MAX                      (255) // 0x000000FF
  #define MC0_CH0_CR_DDR_MR_RESULT_1_Device_4_DEF                      (0x00000000)
  #define MC0_CH0_CR_DDR_MR_RESULT_1_Device_4_HSH                      (0x0800E41C)

  #define MC0_CH0_CR_DDR_MR_RESULT_1_Device_5_OFF                      ( 8)
  #define MC0_CH0_CR_DDR_MR_RESULT_1_Device_5_WID                      ( 8)
  #define MC0_CH0_CR_DDR_MR_RESULT_1_Device_5_MSK                      (0x0000FF00)
  #define MC0_CH0_CR_DDR_MR_RESULT_1_Device_5_MIN                      (0)
  #define MC0_CH0_CR_DDR_MR_RESULT_1_Device_5_MAX                      (255) // 0x000000FF
  #define MC0_CH0_CR_DDR_MR_RESULT_1_Device_5_DEF                      (0x00000000)
  #define MC0_CH0_CR_DDR_MR_RESULT_1_Device_5_HSH                      (0x0810E41C)

  #define MC0_CH0_CR_DDR_MR_RESULT_1_Device_6_OFF                      (16)
  #define MC0_CH0_CR_DDR_MR_RESULT_1_Device_6_WID                      ( 8)
  #define MC0_CH0_CR_DDR_MR_RESULT_1_Device_6_MSK                      (0x00FF0000)
  #define MC0_CH0_CR_DDR_MR_RESULT_1_Device_6_MIN                      (0)
  #define MC0_CH0_CR_DDR_MR_RESULT_1_Device_6_MAX                      (255) // 0x000000FF
  #define MC0_CH0_CR_DDR_MR_RESULT_1_Device_6_DEF                      (0x00000000)
  #define MC0_CH0_CR_DDR_MR_RESULT_1_Device_6_HSH                      (0x0820E41C)

  #define MC0_CH0_CR_DDR_MR_RESULT_1_Device_7_OFF                      (24)
  #define MC0_CH0_CR_DDR_MR_RESULT_1_Device_7_WID                      ( 8)
  #define MC0_CH0_CR_DDR_MR_RESULT_1_Device_7_MSK                      (0xFF000000)
  #define MC0_CH0_CR_DDR_MR_RESULT_1_Device_7_MIN                      (0)
  #define MC0_CH0_CR_DDR_MR_RESULT_1_Device_7_MAX                      (255) // 0x000000FF
  #define MC0_CH0_CR_DDR_MR_RESULT_1_Device_7_DEF                      (0x00000000)
  #define MC0_CH0_CR_DDR_MR_RESULT_1_Device_7_HSH                      (0x0830E41C)

#define MC0_CH0_CR_DDR_MR_RESULT_2_REG                                 (0x0000E420)

  #define MC0_CH0_CR_DDR_MR_RESULT_2_Device_8_OFF                      ( 0)
  #define MC0_CH0_CR_DDR_MR_RESULT_2_Device_8_WID                      ( 8)
  #define MC0_CH0_CR_DDR_MR_RESULT_2_Device_8_MSK                      (0x000000FF)
  #define MC0_CH0_CR_DDR_MR_RESULT_2_Device_8_MIN                      (0)
  #define MC0_CH0_CR_DDR_MR_RESULT_2_Device_8_MAX                      (255) // 0x000000FF
  #define MC0_CH0_CR_DDR_MR_RESULT_2_Device_8_DEF                      (0x00000000)
  #define MC0_CH0_CR_DDR_MR_RESULT_2_Device_8_HSH                      (0x0800E420)

#define MC0_CH0_CR_MR4_RANK_TEMPERATURE_REG                            (0x0000E424)

  #define MC0_CH0_CR_MR4_RANK_TEMPERATURE_Rank_0_OFF                   ( 0)
  #define MC0_CH0_CR_MR4_RANK_TEMPERATURE_Rank_0_WID                   ( 5)
  #define MC0_CH0_CR_MR4_RANK_TEMPERATURE_Rank_0_MSK                   (0x0000001F)
  #define MC0_CH0_CR_MR4_RANK_TEMPERATURE_Rank_0_MIN                   (0)
  #define MC0_CH0_CR_MR4_RANK_TEMPERATURE_Rank_0_MAX                   (31) // 0x0000001F
  #define MC0_CH0_CR_MR4_RANK_TEMPERATURE_Rank_0_DEF                   (0x00000003)
  #define MC0_CH0_CR_MR4_RANK_TEMPERATURE_Rank_0_HSH                   (0x0500E424)

  #define MC0_CH0_CR_MR4_RANK_TEMPERATURE_Rank_1_OFF                   ( 8)
  #define MC0_CH0_CR_MR4_RANK_TEMPERATURE_Rank_1_WID                   ( 5)
  #define MC0_CH0_CR_MR4_RANK_TEMPERATURE_Rank_1_MSK                   (0x00001F00)
  #define MC0_CH0_CR_MR4_RANK_TEMPERATURE_Rank_1_MIN                   (0)
  #define MC0_CH0_CR_MR4_RANK_TEMPERATURE_Rank_1_MAX                   (31) // 0x0000001F
  #define MC0_CH0_CR_MR4_RANK_TEMPERATURE_Rank_1_DEF                   (0x00000003)
  #define MC0_CH0_CR_MR4_RANK_TEMPERATURE_Rank_1_HSH                   (0x0510E424)

  #define MC0_CH0_CR_MR4_RANK_TEMPERATURE_Rank_2_OFF                   (16)
  #define MC0_CH0_CR_MR4_RANK_TEMPERATURE_Rank_2_WID                   ( 5)
  #define MC0_CH0_CR_MR4_RANK_TEMPERATURE_Rank_2_MSK                   (0x001F0000)
  #define MC0_CH0_CR_MR4_RANK_TEMPERATURE_Rank_2_MIN                   (0)
  #define MC0_CH0_CR_MR4_RANK_TEMPERATURE_Rank_2_MAX                   (31) // 0x0000001F
  #define MC0_CH0_CR_MR4_RANK_TEMPERATURE_Rank_2_DEF                   (0x00000003)
  #define MC0_CH0_CR_MR4_RANK_TEMPERATURE_Rank_2_HSH                   (0x0520E424)

  #define MC0_CH0_CR_MR4_RANK_TEMPERATURE_Rank_3_OFF                   (24)
  #define MC0_CH0_CR_MR4_RANK_TEMPERATURE_Rank_3_WID                   ( 5)
  #define MC0_CH0_CR_MR4_RANK_TEMPERATURE_Rank_3_MSK                   (0x1F000000)
  #define MC0_CH0_CR_MR4_RANK_TEMPERATURE_Rank_3_MIN                   (0)
  #define MC0_CH0_CR_MR4_RANK_TEMPERATURE_Rank_3_MAX                   (31) // 0x0000001F
  #define MC0_CH0_CR_MR4_RANK_TEMPERATURE_Rank_3_DEF                   (0x00000003)
  #define MC0_CH0_CR_MR4_RANK_TEMPERATURE_Rank_3_HSH                   (0x0530E424)

#define MC0_CH0_CR_DDR4_MPR_RANK_TEMPERATURE_REG                       (0x0000E428)

  #define MC0_CH0_CR_DDR4_MPR_RANK_TEMPERATURE_Rank_0_OFF              ( 0)
  #define MC0_CH0_CR_DDR4_MPR_RANK_TEMPERATURE_Rank_0_WID              ( 2)
  #define MC0_CH0_CR_DDR4_MPR_RANK_TEMPERATURE_Rank_0_MSK              (0x00000003)
  #define MC0_CH0_CR_DDR4_MPR_RANK_TEMPERATURE_Rank_0_MIN              (0)
  #define MC0_CH0_CR_DDR4_MPR_RANK_TEMPERATURE_Rank_0_MAX              (3) // 0x00000003
  #define MC0_CH0_CR_DDR4_MPR_RANK_TEMPERATURE_Rank_0_DEF              (0x00000001)
  #define MC0_CH0_CR_DDR4_MPR_RANK_TEMPERATURE_Rank_0_HSH              (0x0200E428)

  #define MC0_CH0_CR_DDR4_MPR_RANK_TEMPERATURE_Rank_1_OFF              ( 8)
  #define MC0_CH0_CR_DDR4_MPR_RANK_TEMPERATURE_Rank_1_WID              ( 2)
  #define MC0_CH0_CR_DDR4_MPR_RANK_TEMPERATURE_Rank_1_MSK              (0x00000300)
  #define MC0_CH0_CR_DDR4_MPR_RANK_TEMPERATURE_Rank_1_MIN              (0)
  #define MC0_CH0_CR_DDR4_MPR_RANK_TEMPERATURE_Rank_1_MAX              (3) // 0x00000003
  #define MC0_CH0_CR_DDR4_MPR_RANK_TEMPERATURE_Rank_1_DEF              (0x00000001)
  #define MC0_CH0_CR_DDR4_MPR_RANK_TEMPERATURE_Rank_1_HSH              (0x0210E428)

  #define MC0_CH0_CR_DDR4_MPR_RANK_TEMPERATURE_Rank_2_OFF              (16)
  #define MC0_CH0_CR_DDR4_MPR_RANK_TEMPERATURE_Rank_2_WID              ( 2)
  #define MC0_CH0_CR_DDR4_MPR_RANK_TEMPERATURE_Rank_2_MSK              (0x00030000)
  #define MC0_CH0_CR_DDR4_MPR_RANK_TEMPERATURE_Rank_2_MIN              (0)
  #define MC0_CH0_CR_DDR4_MPR_RANK_TEMPERATURE_Rank_2_MAX              (3) // 0x00000003
  #define MC0_CH0_CR_DDR4_MPR_RANK_TEMPERATURE_Rank_2_DEF              (0x00000001)
  #define MC0_CH0_CR_DDR4_MPR_RANK_TEMPERATURE_Rank_2_HSH              (0x0220E428)

  #define MC0_CH0_CR_DDR4_MPR_RANK_TEMPERATURE_Rank_3_OFF              (24)
  #define MC0_CH0_CR_DDR4_MPR_RANK_TEMPERATURE_Rank_3_WID              ( 2)
  #define MC0_CH0_CR_DDR4_MPR_RANK_TEMPERATURE_Rank_3_MSK              (0x03000000)
  #define MC0_CH0_CR_DDR4_MPR_RANK_TEMPERATURE_Rank_3_MIN              (0)
  #define MC0_CH0_CR_DDR4_MPR_RANK_TEMPERATURE_Rank_3_MAX              (3) // 0x00000003
  #define MC0_CH0_CR_DDR4_MPR_RANK_TEMPERATURE_Rank_3_DEF              (0x00000001)
  #define MC0_CH0_CR_DDR4_MPR_RANK_TEMPERATURE_Rank_3_HSH              (0x0230E428)

#define MC0_CH0_CR_MC_RDB_CREDITS_REG                                  (0x0000E42C)

  #define MC0_CH0_CR_MC_RDB_CREDITS_Total_reads_entries_OFF            ( 0)
  #define MC0_CH0_CR_MC_RDB_CREDITS_Total_reads_entries_WID            ( 6)
  #define MC0_CH0_CR_MC_RDB_CREDITS_Total_reads_entries_MSK            (0x0000003F)
  #define MC0_CH0_CR_MC_RDB_CREDITS_Total_reads_entries_MIN            (0)
  #define MC0_CH0_CR_MC_RDB_CREDITS_Total_reads_entries_MAX            (63) // 0x0000003F
  #define MC0_CH0_CR_MC_RDB_CREDITS_Total_reads_entries_DEF            (0x00000020)
  #define MC0_CH0_CR_MC_RDB_CREDITS_Total_reads_entries_HSH            (0x0600E42C)

  #define MC0_CH0_CR_MC_RDB_CREDITS_VC0_reads_entries_OFF              ( 8)
  #define MC0_CH0_CR_MC_RDB_CREDITS_VC0_reads_entries_WID              ( 6)
  #define MC0_CH0_CR_MC_RDB_CREDITS_VC0_reads_entries_MSK              (0x00003F00)
  #define MC0_CH0_CR_MC_RDB_CREDITS_VC0_reads_entries_MIN              (0)
  #define MC0_CH0_CR_MC_RDB_CREDITS_VC0_reads_entries_MAX              (63) // 0x0000003F
  #define MC0_CH0_CR_MC_RDB_CREDITS_VC0_reads_entries_DEF              (0x00000010)
  #define MC0_CH0_CR_MC_RDB_CREDITS_VC0_reads_entries_HSH              (0x0610E42C)

  #define MC0_CH0_CR_MC_RDB_CREDITS_VC1_reads_entries_OFF              (16)
  #define MC0_CH0_CR_MC_RDB_CREDITS_VC1_reads_entries_WID              ( 6)
  #define MC0_CH0_CR_MC_RDB_CREDITS_VC1_reads_entries_MSK              (0x003F0000)
  #define MC0_CH0_CR_MC_RDB_CREDITS_VC1_reads_entries_MIN              (0)
  #define MC0_CH0_CR_MC_RDB_CREDITS_VC1_reads_entries_MAX              (63) // 0x0000003F
  #define MC0_CH0_CR_MC_RDB_CREDITS_VC1_reads_entries_DEF              (0x00000010)
  #define MC0_CH0_CR_MC_RDB_CREDITS_VC1_reads_entries_HSH              (0x0620E42C)

#define MC0_CH0_CR_DESWIZZLE_LOW_ERM_REG                               (0x0000E430)

  #define MC0_CH0_CR_DESWIZZLE_LOW_ERM_Bit_0_OFF                       ( 0)
  #define MC0_CH0_CR_DESWIZZLE_LOW_ERM_Bit_0_WID                       ( 3)
  #define MC0_CH0_CR_DESWIZZLE_LOW_ERM_Bit_0_MSK                       (0x00000007)
  #define MC0_CH0_CR_DESWIZZLE_LOW_ERM_Bit_0_MIN                       (0)
  #define MC0_CH0_CR_DESWIZZLE_LOW_ERM_Bit_0_MAX                       (7) // 0x00000007
  #define MC0_CH0_CR_DESWIZZLE_LOW_ERM_Bit_0_DEF                       (0x00000000)
  #define MC0_CH0_CR_DESWIZZLE_LOW_ERM_Bit_0_HSH                       (0x4300E430)

  #define MC0_CH0_CR_DESWIZZLE_LOW_ERM_Bit_1_OFF                       ( 3)
  #define MC0_CH0_CR_DESWIZZLE_LOW_ERM_Bit_1_WID                       ( 3)
  #define MC0_CH0_CR_DESWIZZLE_LOW_ERM_Bit_1_MSK                       (0x00000038)
  #define MC0_CH0_CR_DESWIZZLE_LOW_ERM_Bit_1_MIN                       (0)
  #define MC0_CH0_CR_DESWIZZLE_LOW_ERM_Bit_1_MAX                       (7) // 0x00000007
  #define MC0_CH0_CR_DESWIZZLE_LOW_ERM_Bit_1_DEF                       (0x00000001)
  #define MC0_CH0_CR_DESWIZZLE_LOW_ERM_Bit_1_HSH                       (0x4306E430)

  #define MC0_CH0_CR_DESWIZZLE_LOW_ERM_Bit_2_OFF                       ( 6)
  #define MC0_CH0_CR_DESWIZZLE_LOW_ERM_Bit_2_WID                       ( 3)
  #define MC0_CH0_CR_DESWIZZLE_LOW_ERM_Bit_2_MSK                       (0x000001C0)
  #define MC0_CH0_CR_DESWIZZLE_LOW_ERM_Bit_2_MIN                       (0)
  #define MC0_CH0_CR_DESWIZZLE_LOW_ERM_Bit_2_MAX                       (7) // 0x00000007
  #define MC0_CH0_CR_DESWIZZLE_LOW_ERM_Bit_2_DEF                       (0x00000002)
  #define MC0_CH0_CR_DESWIZZLE_LOW_ERM_Bit_2_HSH                       (0x430CE430)

  #define MC0_CH0_CR_DESWIZZLE_LOW_ERM_Bit_3_OFF                       ( 9)
  #define MC0_CH0_CR_DESWIZZLE_LOW_ERM_Bit_3_WID                       ( 3)
  #define MC0_CH0_CR_DESWIZZLE_LOW_ERM_Bit_3_MSK                       (0x00000E00)
  #define MC0_CH0_CR_DESWIZZLE_LOW_ERM_Bit_3_MIN                       (0)
  #define MC0_CH0_CR_DESWIZZLE_LOW_ERM_Bit_3_MAX                       (7) // 0x00000007
  #define MC0_CH0_CR_DESWIZZLE_LOW_ERM_Bit_3_DEF                       (0x00000003)
  #define MC0_CH0_CR_DESWIZZLE_LOW_ERM_Bit_3_HSH                       (0x4312E430)

  #define MC0_CH0_CR_DESWIZZLE_LOW_ERM_Bit_4_OFF                       (12)
  #define MC0_CH0_CR_DESWIZZLE_LOW_ERM_Bit_4_WID                       ( 3)
  #define MC0_CH0_CR_DESWIZZLE_LOW_ERM_Bit_4_MSK                       (0x00007000)
  #define MC0_CH0_CR_DESWIZZLE_LOW_ERM_Bit_4_MIN                       (0)
  #define MC0_CH0_CR_DESWIZZLE_LOW_ERM_Bit_4_MAX                       (7) // 0x00000007
  #define MC0_CH0_CR_DESWIZZLE_LOW_ERM_Bit_4_DEF                       (0x00000004)
  #define MC0_CH0_CR_DESWIZZLE_LOW_ERM_Bit_4_HSH                       (0x4318E430)

  #define MC0_CH0_CR_DESWIZZLE_LOW_ERM_Bit_8_OFF                       (15)
  #define MC0_CH0_CR_DESWIZZLE_LOW_ERM_Bit_8_WID                       ( 3)
  #define MC0_CH0_CR_DESWIZZLE_LOW_ERM_Bit_8_MSK                       (0x00038000)
  #define MC0_CH0_CR_DESWIZZLE_LOW_ERM_Bit_8_MIN                       (0)
  #define MC0_CH0_CR_DESWIZZLE_LOW_ERM_Bit_8_MAX                       (7) // 0x00000007
  #define MC0_CH0_CR_DESWIZZLE_LOW_ERM_Bit_8_DEF                       (0x00000000)
  #define MC0_CH0_CR_DESWIZZLE_LOW_ERM_Bit_8_HSH                       (0x431EE430)

  #define MC0_CH0_CR_DESWIZZLE_LOW_ERM_Bit_9_OFF                       (18)
  #define MC0_CH0_CR_DESWIZZLE_LOW_ERM_Bit_9_WID                       ( 3)
  #define MC0_CH0_CR_DESWIZZLE_LOW_ERM_Bit_9_MSK                       (0x001C0000)
  #define MC0_CH0_CR_DESWIZZLE_LOW_ERM_Bit_9_MIN                       (0)
  #define MC0_CH0_CR_DESWIZZLE_LOW_ERM_Bit_9_MAX                       (7) // 0x00000007
  #define MC0_CH0_CR_DESWIZZLE_LOW_ERM_Bit_9_DEF                       (0x00000001)
  #define MC0_CH0_CR_DESWIZZLE_LOW_ERM_Bit_9_HSH                       (0x4324E430)

  #define MC0_CH0_CR_DESWIZZLE_LOW_ERM_Bit_10_OFF                      (21)
  #define MC0_CH0_CR_DESWIZZLE_LOW_ERM_Bit_10_WID                      ( 3)
  #define MC0_CH0_CR_DESWIZZLE_LOW_ERM_Bit_10_MSK                      (0x00E00000)
  #define MC0_CH0_CR_DESWIZZLE_LOW_ERM_Bit_10_MIN                      (0)
  #define MC0_CH0_CR_DESWIZZLE_LOW_ERM_Bit_10_MAX                      (7) // 0x00000007
  #define MC0_CH0_CR_DESWIZZLE_LOW_ERM_Bit_10_DEF                      (0x00000002)
  #define MC0_CH0_CR_DESWIZZLE_LOW_ERM_Bit_10_HSH                      (0x432AE430)

  #define MC0_CH0_CR_DESWIZZLE_LOW_ERM_Bit_11_OFF                      (24)
  #define MC0_CH0_CR_DESWIZZLE_LOW_ERM_Bit_11_WID                      ( 3)
  #define MC0_CH0_CR_DESWIZZLE_LOW_ERM_Bit_11_MSK                      (0x07000000)
  #define MC0_CH0_CR_DESWIZZLE_LOW_ERM_Bit_11_MIN                      (0)
  #define MC0_CH0_CR_DESWIZZLE_LOW_ERM_Bit_11_MAX                      (7) // 0x00000007
  #define MC0_CH0_CR_DESWIZZLE_LOW_ERM_Bit_11_DEF                      (0x00000003)
  #define MC0_CH0_CR_DESWIZZLE_LOW_ERM_Bit_11_HSH                      (0x4330E430)

  #define MC0_CH0_CR_DESWIZZLE_LOW_ERM_Bit_12_OFF                      (27)
  #define MC0_CH0_CR_DESWIZZLE_LOW_ERM_Bit_12_WID                      ( 3)
  #define MC0_CH0_CR_DESWIZZLE_LOW_ERM_Bit_12_MSK                      (0x38000000)
  #define MC0_CH0_CR_DESWIZZLE_LOW_ERM_Bit_12_MIN                      (0)
  #define MC0_CH0_CR_DESWIZZLE_LOW_ERM_Bit_12_MAX                      (7) // 0x00000007
  #define MC0_CH0_CR_DESWIZZLE_LOW_ERM_Bit_12_DEF                      (0x00000004)
  #define MC0_CH0_CR_DESWIZZLE_LOW_ERM_Bit_12_HSH                      (0x4336E430)

  #define MC0_CH0_CR_DESWIZZLE_LOW_ERM_Byte_0_OFF                      (32)
  #define MC0_CH0_CR_DESWIZZLE_LOW_ERM_Byte_0_WID                      ( 3)
  #define MC0_CH0_CR_DESWIZZLE_LOW_ERM_Byte_0_MSK                      (0x0000000700000000ULL)
  #define MC0_CH0_CR_DESWIZZLE_LOW_ERM_Byte_0_MIN                      (0)
  #define MC0_CH0_CR_DESWIZZLE_LOW_ERM_Byte_0_MAX                      (7) // 0x00000007
  #define MC0_CH0_CR_DESWIZZLE_LOW_ERM_Byte_0_DEF                      (0x00000000)
  #define MC0_CH0_CR_DESWIZZLE_LOW_ERM_Byte_0_HSH                      (0x4340E430)

  #define MC0_CH0_CR_DESWIZZLE_LOW_ERM_Byte_1_OFF                      (35)
  #define MC0_CH0_CR_DESWIZZLE_LOW_ERM_Byte_1_WID                      ( 3)
  #define MC0_CH0_CR_DESWIZZLE_LOW_ERM_Byte_1_MSK                      (0x0000003800000000ULL)
  #define MC0_CH0_CR_DESWIZZLE_LOW_ERM_Byte_1_MIN                      (0)
  #define MC0_CH0_CR_DESWIZZLE_LOW_ERM_Byte_1_MAX                      (7) // 0x00000007
  #define MC0_CH0_CR_DESWIZZLE_LOW_ERM_Byte_1_DEF                      (0x00000001)
  #define MC0_CH0_CR_DESWIZZLE_LOW_ERM_Byte_1_HSH                      (0x4346E430)

#define MC0_CH0_CR_TC_RFP_REG                                          (0x0000E438)

  #define MC0_CH0_CR_TC_RFP_OREF_RI_OFF                                ( 0)
  #define MC0_CH0_CR_TC_RFP_OREF_RI_WID                                ( 8)
  #define MC0_CH0_CR_TC_RFP_OREF_RI_MSK                                (0x000000FF)
  #define MC0_CH0_CR_TC_RFP_OREF_RI_MIN                                (0)
  #define MC0_CH0_CR_TC_RFP_OREF_RI_MAX                                (255) // 0x000000FF
  #define MC0_CH0_CR_TC_RFP_OREF_RI_DEF                                (0x0000000F)
  #define MC0_CH0_CR_TC_RFP_OREF_RI_HSH                                (0x0800E438)

  #define MC0_CH0_CR_TC_RFP_Refresh_HP_WM_OFF                          ( 8)
  #define MC0_CH0_CR_TC_RFP_Refresh_HP_WM_WID                          ( 4)
  #define MC0_CH0_CR_TC_RFP_Refresh_HP_WM_MSK                          (0x00000F00)
  #define MC0_CH0_CR_TC_RFP_Refresh_HP_WM_MIN                          (0)
  #define MC0_CH0_CR_TC_RFP_Refresh_HP_WM_MAX                          (15) // 0x0000000F
  #define MC0_CH0_CR_TC_RFP_Refresh_HP_WM_DEF                          (0x00000008)
  #define MC0_CH0_CR_TC_RFP_Refresh_HP_WM_HSH                          (0x0410E438)

  #define MC0_CH0_CR_TC_RFP_Refresh_panic_wm_OFF                       (12)
  #define MC0_CH0_CR_TC_RFP_Refresh_panic_wm_WID                       ( 4)
  #define MC0_CH0_CR_TC_RFP_Refresh_panic_wm_MSK                       (0x0000F000)
  #define MC0_CH0_CR_TC_RFP_Refresh_panic_wm_MIN                       (0)
  #define MC0_CH0_CR_TC_RFP_Refresh_panic_wm_MAX                       (15) // 0x0000000F
  #define MC0_CH0_CR_TC_RFP_Refresh_panic_wm_DEF                       (0x00000009)
  #define MC0_CH0_CR_TC_RFP_Refresh_panic_wm_HSH                       (0x0418E438)

  #define MC0_CH0_CR_TC_RFP_CounttREFIWhileRefEnOff_OFF                (16)
  #define MC0_CH0_CR_TC_RFP_CounttREFIWhileRefEnOff_WID                ( 1)
  #define MC0_CH0_CR_TC_RFP_CounttREFIWhileRefEnOff_MSK                (0x00010000)
  #define MC0_CH0_CR_TC_RFP_CounttREFIWhileRefEnOff_MIN                (0)
  #define MC0_CH0_CR_TC_RFP_CounttREFIWhileRefEnOff_MAX                (1) // 0x00000001
  #define MC0_CH0_CR_TC_RFP_CounttREFIWhileRefEnOff_DEF                (0x00000000)
  #define MC0_CH0_CR_TC_RFP_CounttREFIWhileRefEnOff_HSH                (0x0120E438)

  #define MC0_CH0_CR_TC_RFP_HPRefOnMRS_OFF                             (17)
  #define MC0_CH0_CR_TC_RFP_HPRefOnMRS_WID                             ( 1)
  #define MC0_CH0_CR_TC_RFP_HPRefOnMRS_MSK                             (0x00020000)
  #define MC0_CH0_CR_TC_RFP_HPRefOnMRS_MIN                             (0)
  #define MC0_CH0_CR_TC_RFP_HPRefOnMRS_MAX                             (1) // 0x00000001
  #define MC0_CH0_CR_TC_RFP_HPRefOnMRS_DEF                             (0x00000001)
  #define MC0_CH0_CR_TC_RFP_HPRefOnMRS_HSH                             (0x0122E438)

  #define MC0_CH0_CR_TC_RFP_SRX_Ref_Debits_OFF                         (18)
  #define MC0_CH0_CR_TC_RFP_SRX_Ref_Debits_WID                         ( 2)
  #define MC0_CH0_CR_TC_RFP_SRX_Ref_Debits_MSK                         (0x000C0000)
  #define MC0_CH0_CR_TC_RFP_SRX_Ref_Debits_MIN                         (0)
  #define MC0_CH0_CR_TC_RFP_SRX_Ref_Debits_MAX                         (3) // 0x00000003
  #define MC0_CH0_CR_TC_RFP_SRX_Ref_Debits_DEF                         (0x00000001)
  #define MC0_CH0_CR_TC_RFP_SRX_Ref_Debits_HSH                         (0x0224E438)

  #define MC0_CH0_CR_TC_RFP_RAISE_BLK_WAIT_OFF                         (20)
  #define MC0_CH0_CR_TC_RFP_RAISE_BLK_WAIT_WID                         ( 4)
  #define MC0_CH0_CR_TC_RFP_RAISE_BLK_WAIT_MSK                         (0x00F00000)
  #define MC0_CH0_CR_TC_RFP_RAISE_BLK_WAIT_MIN                         (0)
  #define MC0_CH0_CR_TC_RFP_RAISE_BLK_WAIT_MAX                         (15) // 0x0000000F
  #define MC0_CH0_CR_TC_RFP_RAISE_BLK_WAIT_DEF                         (0x00000005)
  #define MC0_CH0_CR_TC_RFP_RAISE_BLK_WAIT_HSH                         (0x0428E438)

  #define MC0_CH0_CR_TC_RFP_tREFIx9_OFF                                (24)
  #define MC0_CH0_CR_TC_RFP_tREFIx9_WID                                ( 8)
  #define MC0_CH0_CR_TC_RFP_tREFIx9_MSK                                (0xFF000000)
  #define MC0_CH0_CR_TC_RFP_tREFIx9_MIN                                (0)
  #define MC0_CH0_CR_TC_RFP_tREFIx9_MAX                                (255) // 0x000000FF
  #define MC0_CH0_CR_TC_RFP_tREFIx9_DEF                                (0x00000023)
  #define MC0_CH0_CR_TC_RFP_tREFIx9_HSH                                (0x0830E438)

#define MC0_CH0_CR_TC_RFTP_REG                                         (0x0000E43C)

  #define MC0_CH0_CR_TC_RFTP_tREFI_OFF                                 ( 0)
  #define MC0_CH0_CR_TC_RFTP_tREFI_WID                                 (18)
  #define MC0_CH0_CR_TC_RFTP_tREFI_MSK                                 (0x0003FFFF)
  #define MC0_CH0_CR_TC_RFTP_tREFI_MIN                                 (0)
  #define MC0_CH0_CR_TC_RFTP_tREFI_MAX                                 (262143) // 0x0003FFFF
  #define MC0_CH0_CR_TC_RFTP_tREFI_DEF                                 (0x00001004)
  #define MC0_CH0_CR_TC_RFTP_tREFI_HSH                                 (0x1200E43C)

  #define MC0_CH0_CR_TC_RFTP_tRFC_OFF                                  (18)
  #define MC0_CH0_CR_TC_RFTP_tRFC_WID                                  (13)
  #define MC0_CH0_CR_TC_RFTP_tRFC_MSK                                  (0x7FFC0000)
  #define MC0_CH0_CR_TC_RFTP_tRFC_MIN                                  (0)
  #define MC0_CH0_CR_TC_RFTP_tRFC_MAX                                  (8191) // 0x00001FFF
  #define MC0_CH0_CR_TC_RFTP_tRFC_DEF                                  (0x000000B4)
  #define MC0_CH0_CR_TC_RFTP_tRFC_HSH                                  (0x0D24E43C)

#define MC0_CH0_CR_TC_SRFTP_REG                                        (0x0000E440)

  #define MC0_CH0_CR_TC_SRFTP_tXSDLL_OFF                               ( 0)
  #define MC0_CH0_CR_TC_SRFTP_tXSDLL_WID                               (13)
  #define MC0_CH0_CR_TC_SRFTP_tXSDLL_MSK                               (0x00001FFF)
  #define MC0_CH0_CR_TC_SRFTP_tXSDLL_MIN                               (0)
  #define MC0_CH0_CR_TC_SRFTP_tXSDLL_MAX                               (8191) // 0x00001FFF
  #define MC0_CH0_CR_TC_SRFTP_tXSDLL_DEF                               (0x00000200)
  #define MC0_CH0_CR_TC_SRFTP_tXSDLL_HSH                               (0x0D00E440)

  #define MC0_CH0_CR_TC_SRFTP_tZQOPER_OFF                              (13)
  #define MC0_CH0_CR_TC_SRFTP_tZQOPER_WID                              (11)
  #define MC0_CH0_CR_TC_SRFTP_tZQOPER_MSK                              (0x00FFE000)
  #define MC0_CH0_CR_TC_SRFTP_tZQOPER_MIN                              (0)
  #define MC0_CH0_CR_TC_SRFTP_tZQOPER_MAX                              (2047) // 0x000007FF
  #define MC0_CH0_CR_TC_SRFTP_tZQOPER_DEF                              (0x00000100)
  #define MC0_CH0_CR_TC_SRFTP_tZQOPER_HSH                              (0x0B1AE440)

  #define MC0_CH0_CR_TC_SRFTP_tMOD_OFF                                 (24)
  #define MC0_CH0_CR_TC_SRFTP_tMOD_WID                                 ( 7)
  #define MC0_CH0_CR_TC_SRFTP_tMOD_MSK                                 (0x7F000000)
  #define MC0_CH0_CR_TC_SRFTP_tMOD_MIN                                 (0)
  #define MC0_CH0_CR_TC_SRFTP_tMOD_MAX                                 (127) // 0x0000007F
  #define MC0_CH0_CR_TC_SRFTP_tMOD_DEF                                 (0x00000000)
  #define MC0_CH0_CR_TC_SRFTP_tMOD_HSH                                 (0x0730E440)

#define MC0_CH0_CR_MC_REFRESH_STAGGER_REG                              (0x0000E444)

  #define MC0_CH0_CR_MC_REFRESH_STAGGER_Ref_Interval_OFF               ( 0)
  #define MC0_CH0_CR_MC_REFRESH_STAGGER_Ref_Interval_WID               (13)
  #define MC0_CH0_CR_MC_REFRESH_STAGGER_Ref_Interval_MSK               (0x00001FFF)
  #define MC0_CH0_CR_MC_REFRESH_STAGGER_Ref_Interval_MIN               (0)
  #define MC0_CH0_CR_MC_REFRESH_STAGGER_Ref_Interval_MAX               (8191) // 0x00001FFF
  #define MC0_CH0_CR_MC_REFRESH_STAGGER_Ref_Interval_DEF               (0x00000000)
  #define MC0_CH0_CR_MC_REFRESH_STAGGER_Ref_Interval_HSH               (0x0D00E444)

  #define MC0_CH0_CR_MC_REFRESH_STAGGER_Disable_Stolen_Refresh_OFF     (13)
  #define MC0_CH0_CR_MC_REFRESH_STAGGER_Disable_Stolen_Refresh_WID     ( 1)
  #define MC0_CH0_CR_MC_REFRESH_STAGGER_Disable_Stolen_Refresh_MSK     (0x00002000)
  #define MC0_CH0_CR_MC_REFRESH_STAGGER_Disable_Stolen_Refresh_MIN     (0)
  #define MC0_CH0_CR_MC_REFRESH_STAGGER_Disable_Stolen_Refresh_MAX     (1) // 0x00000001
  #define MC0_CH0_CR_MC_REFRESH_STAGGER_Disable_Stolen_Refresh_DEF     (0x00000000)
  #define MC0_CH0_CR_MC_REFRESH_STAGGER_Disable_Stolen_Refresh_HSH     (0x011AE444)

  #define MC0_CH0_CR_MC_REFRESH_STAGGER_En_Ref_Type_Display_OFF        (14)
  #define MC0_CH0_CR_MC_REFRESH_STAGGER_En_Ref_Type_Display_WID        ( 1)
  #define MC0_CH0_CR_MC_REFRESH_STAGGER_En_Ref_Type_Display_MSK        (0x00004000)
  #define MC0_CH0_CR_MC_REFRESH_STAGGER_En_Ref_Type_Display_MIN        (0)
  #define MC0_CH0_CR_MC_REFRESH_STAGGER_En_Ref_Type_Display_MAX        (1) // 0x00000001
  #define MC0_CH0_CR_MC_REFRESH_STAGGER_En_Ref_Type_Display_DEF        (0x00000000)
  #define MC0_CH0_CR_MC_REFRESH_STAGGER_En_Ref_Type_Display_HSH        (0x011CE444)

  #define MC0_CH0_CR_MC_REFRESH_STAGGER_Ref_Stagger_En_OFF             (15)
  #define MC0_CH0_CR_MC_REFRESH_STAGGER_Ref_Stagger_En_WID             ( 1)
  #define MC0_CH0_CR_MC_REFRESH_STAGGER_Ref_Stagger_En_MSK             (0x00008000)
  #define MC0_CH0_CR_MC_REFRESH_STAGGER_Ref_Stagger_En_MIN             (0)
  #define MC0_CH0_CR_MC_REFRESH_STAGGER_Ref_Stagger_En_MAX             (1) // 0x00000001
  #define MC0_CH0_CR_MC_REFRESH_STAGGER_Ref_Stagger_En_DEF             (0x00000000)
  #define MC0_CH0_CR_MC_REFRESH_STAGGER_Ref_Stagger_En_HSH             (0x011EE444)

  #define MC0_CH0_CR_MC_REFRESH_STAGGER_Ref_Stagger_Mode_OFF           (16)
  #define MC0_CH0_CR_MC_REFRESH_STAGGER_Ref_Stagger_Mode_WID           ( 1)
  #define MC0_CH0_CR_MC_REFRESH_STAGGER_Ref_Stagger_Mode_MSK           (0x00010000)
  #define MC0_CH0_CR_MC_REFRESH_STAGGER_Ref_Stagger_Mode_MIN           (0)
  #define MC0_CH0_CR_MC_REFRESH_STAGGER_Ref_Stagger_Mode_MAX           (1) // 0x00000001
  #define MC0_CH0_CR_MC_REFRESH_STAGGER_Ref_Stagger_Mode_DEF           (0x00000000)
  #define MC0_CH0_CR_MC_REFRESH_STAGGER_Ref_Stagger_Mode_HSH           (0x0120E444)

  #define MC0_CH0_CR_MC_REFRESH_STAGGER_Trefipulse_Stagger_Disable_OFF (17)
  #define MC0_CH0_CR_MC_REFRESH_STAGGER_Trefipulse_Stagger_Disable_WID ( 1)
  #define MC0_CH0_CR_MC_REFRESH_STAGGER_Trefipulse_Stagger_Disable_MSK (0x00020000)
  #define MC0_CH0_CR_MC_REFRESH_STAGGER_Trefipulse_Stagger_Disable_MIN (0)
  #define MC0_CH0_CR_MC_REFRESH_STAGGER_Trefipulse_Stagger_Disable_MAX (1) // 0x00000001
  #define MC0_CH0_CR_MC_REFRESH_STAGGER_Trefipulse_Stagger_Disable_DEF (0x00000000)
  #define MC0_CH0_CR_MC_REFRESH_STAGGER_Trefipulse_Stagger_Disable_HSH (0x0122E444)

  #define MC0_CH0_CR_MC_REFRESH_STAGGER_WakeUpOnHPM_OFF                (18)
  #define MC0_CH0_CR_MC_REFRESH_STAGGER_WakeUpOnHPM_WID                ( 1)
  #define MC0_CH0_CR_MC_REFRESH_STAGGER_WakeUpOnHPM_MSK                (0x00040000)
  #define MC0_CH0_CR_MC_REFRESH_STAGGER_WakeUpOnHPM_MIN                (0)
  #define MC0_CH0_CR_MC_REFRESH_STAGGER_WakeUpOnHPM_MAX                (1) // 0x00000001
  #define MC0_CH0_CR_MC_REFRESH_STAGGER_WakeUpOnHPM_DEF                (0x00000000)
  #define MC0_CH0_CR_MC_REFRESH_STAGGER_WakeUpOnHPM_HSH                (0x0124E444)

#define MC0_CH0_CR_TC_ZQCAL_REG                                        (0x0000E448)

  #define MC0_CH0_CR_TC_ZQCAL_ZQCS_period_OFF                          ( 0)
  #define MC0_CH0_CR_TC_ZQCAL_ZQCS_period_WID                          (10)
  #define MC0_CH0_CR_TC_ZQCAL_ZQCS_period_MSK                          (0x000003FF)
  #define MC0_CH0_CR_TC_ZQCAL_ZQCS_period_MIN                          (0)
  #define MC0_CH0_CR_TC_ZQCAL_ZQCS_period_MAX                          (1023) // 0x000003FF
  #define MC0_CH0_CR_TC_ZQCAL_ZQCS_period_DEF                          (0x00000080)
  #define MC0_CH0_CR_TC_ZQCAL_ZQCS_period_HSH                          (0x4A00E448)

  #define MC0_CH0_CR_TC_ZQCAL_tZQCS_OFF                                (10)
  #define MC0_CH0_CR_TC_ZQCAL_tZQCS_WID                                (11)
  #define MC0_CH0_CR_TC_ZQCAL_tZQCS_MSK                                (0x001FFC00)
  #define MC0_CH0_CR_TC_ZQCAL_tZQCS_MIN                                (0)
  #define MC0_CH0_CR_TC_ZQCAL_tZQCS_MAX                                (2047) // 0x000007FF
  #define MC0_CH0_CR_TC_ZQCAL_tZQCS_DEF                                (0x00000040)
  #define MC0_CH0_CR_TC_ZQCAL_tZQCS_HSH                                (0x4B14E448)

  #define MC0_CH0_CR_TC_ZQCAL_tZQCAL_OFF                               (32)
  #define MC0_CH0_CR_TC_ZQCAL_tZQCAL_WID                               (13)
  #define MC0_CH0_CR_TC_ZQCAL_tZQCAL_MSK                               (0x00001FFF00000000ULL)
  #define MC0_CH0_CR_TC_ZQCAL_tZQCAL_MIN                               (0)
  #define MC0_CH0_CR_TC_ZQCAL_tZQCAL_MAX                               (8191) // 0x00001FFF
  #define MC0_CH0_CR_TC_ZQCAL_tZQCAL_DEF                               (0x00000320)
  #define MC0_CH0_CR_TC_ZQCAL_tZQCAL_HSH                               (0x4D40E448)

#define MC0_CH0_CR_TC_MR4_SHADDOW_REG                                  (0x0000E450)

  #define MC0_CH0_CR_TC_MR4_SHADDOW_MR4_sh_low_OFF                     ( 0)
  #define MC0_CH0_CR_TC_MR4_SHADDOW_MR4_sh_low_WID                     ( 2)
  #define MC0_CH0_CR_TC_MR4_SHADDOW_MR4_sh_low_MSK                     (0x00000003)
  #define MC0_CH0_CR_TC_MR4_SHADDOW_MR4_sh_low_MIN                     (0)
  #define MC0_CH0_CR_TC_MR4_SHADDOW_MR4_sh_low_MAX                     (3) // 0x00000003
  #define MC0_CH0_CR_TC_MR4_SHADDOW_MR4_sh_low_DEF                     (0x00000000)
  #define MC0_CH0_CR_TC_MR4_SHADDOW_MR4_sh_low_HSH                     (0x0200E450)

  #define MC0_CH0_CR_TC_MR4_SHADDOW_MR4_sh_high_OFF                    ( 4)
  #define MC0_CH0_CR_TC_MR4_SHADDOW_MR4_sh_high_WID                    (10)
  #define MC0_CH0_CR_TC_MR4_SHADDOW_MR4_sh_high_MSK                    (0x00003FF0)
  #define MC0_CH0_CR_TC_MR4_SHADDOW_MR4_sh_high_MIN                    (0)
  #define MC0_CH0_CR_TC_MR4_SHADDOW_MR4_sh_high_MAX                    (1023) // 0x000003FF
  #define MC0_CH0_CR_TC_MR4_SHADDOW_MR4_sh_high_DEF                    (0x00000000)
  #define MC0_CH0_CR_TC_MR4_SHADDOW_MR4_sh_high_HSH                    (0x0A08E450)

#define MC0_CH0_CR_MC_INIT_STATE_REG                                   (0x0000E454)

  #define MC0_CH0_CR_MC_INIT_STATE_Rank_occupancy_OFF                  ( 0)
  #define MC0_CH0_CR_MC_INIT_STATE_Rank_occupancy_WID                  ( 8)
  #define MC0_CH0_CR_MC_INIT_STATE_Rank_occupancy_MSK                  (0x000000FF)
  #define MC0_CH0_CR_MC_INIT_STATE_Rank_occupancy_MIN                  (0)
  #define MC0_CH0_CR_MC_INIT_STATE_Rank_occupancy_MAX                  (255) // 0x000000FF
  #define MC0_CH0_CR_MC_INIT_STATE_Rank_occupancy_DEF                  (0x0000000F)
  #define MC0_CH0_CR_MC_INIT_STATE_Rank_occupancy_HSH                  (0x0800E454)

  #define MC0_CH0_CR_MC_INIT_STATE_SRX_reset_OFF                       ( 8)
  #define MC0_CH0_CR_MC_INIT_STATE_SRX_reset_WID                       ( 1)
  #define MC0_CH0_CR_MC_INIT_STATE_SRX_reset_MSK                       (0x00000100)
  #define MC0_CH0_CR_MC_INIT_STATE_SRX_reset_MIN                       (0)
  #define MC0_CH0_CR_MC_INIT_STATE_SRX_reset_MAX                       (1) // 0x00000001
  #define MC0_CH0_CR_MC_INIT_STATE_SRX_reset_DEF                       (0x00000000)
  #define MC0_CH0_CR_MC_INIT_STATE_SRX_reset_HSH                       (0x0110E454)

  #define MC0_CH0_CR_MC_INIT_STATE_LPDDR4_current_FSP_OFF              ( 9)
  #define MC0_CH0_CR_MC_INIT_STATE_LPDDR4_current_FSP_WID              ( 1)
  #define MC0_CH0_CR_MC_INIT_STATE_LPDDR4_current_FSP_MSK              (0x00000200)
  #define MC0_CH0_CR_MC_INIT_STATE_LPDDR4_current_FSP_MIN              (0)
  #define MC0_CH0_CR_MC_INIT_STATE_LPDDR4_current_FSP_MAX              (1) // 0x00000001
  #define MC0_CH0_CR_MC_INIT_STATE_LPDDR4_current_FSP_DEF              (0x00000000)
  #define MC0_CH0_CR_MC_INIT_STATE_LPDDR4_current_FSP_HSH              (0x0112E454)

  #define MC0_CH0_CR_MC_INIT_STATE_LPDDR4_current_FSP_tracking_OFF     (10)
  #define MC0_CH0_CR_MC_INIT_STATE_LPDDR4_current_FSP_tracking_WID     ( 1)
  #define MC0_CH0_CR_MC_INIT_STATE_LPDDR4_current_FSP_tracking_MSK     (0x00000400)
  #define MC0_CH0_CR_MC_INIT_STATE_LPDDR4_current_FSP_tracking_MIN     (0)
  #define MC0_CH0_CR_MC_INIT_STATE_LPDDR4_current_FSP_tracking_MAX     (1) // 0x00000001
  #define MC0_CH0_CR_MC_INIT_STATE_LPDDR4_current_FSP_tracking_DEF     (0x00000000)
  #define MC0_CH0_CR_MC_INIT_STATE_LPDDR4_current_FSP_tracking_HSH     (0x0114E454)

  #define MC0_CH0_CR_MC_INIT_STATE_Reset_refresh_debt_OFF              (11)
  #define MC0_CH0_CR_MC_INIT_STATE_Reset_refresh_debt_WID              ( 1)
  #define MC0_CH0_CR_MC_INIT_STATE_Reset_refresh_debt_MSK              (0x00000800)
  #define MC0_CH0_CR_MC_INIT_STATE_Reset_refresh_debt_MIN              (0)
  #define MC0_CH0_CR_MC_INIT_STATE_Reset_refresh_debt_MAX              (1) // 0x00000001
  #define MC0_CH0_CR_MC_INIT_STATE_Reset_refresh_debt_DEF              (0x00000000)
  #define MC0_CH0_CR_MC_INIT_STATE_Reset_refresh_debt_HSH              (0x0116E454)

  #define MC0_CH0_CR_MC_INIT_STATE_Deep_SRX_reset_OFF                  (12)
  #define MC0_CH0_CR_MC_INIT_STATE_Deep_SRX_reset_WID                  ( 1)
  #define MC0_CH0_CR_MC_INIT_STATE_Deep_SRX_reset_MSK                  (0x00001000)
  #define MC0_CH0_CR_MC_INIT_STATE_Deep_SRX_reset_MIN                  (0)
  #define MC0_CH0_CR_MC_INIT_STATE_Deep_SRX_reset_MAX                  (1) // 0x00000001
  #define MC0_CH0_CR_MC_INIT_STATE_Deep_SRX_reset_DEF                  (0x00000000)
  #define MC0_CH0_CR_MC_INIT_STATE_Deep_SRX_reset_HSH                  (0x0118E454)

#define MC0_CH0_CR_WDB_VISA_SEL_REG                                    (0x0000E458)

  #define MC0_CH0_CR_WDB_VISA_SEL_VISAByteSel_OFF                      ( 0)
  #define MC0_CH0_CR_WDB_VISA_SEL_VISAByteSel_WID                      ( 3)
  #define MC0_CH0_CR_WDB_VISA_SEL_VISAByteSel_MSK                      (0x00000007)
  #define MC0_CH0_CR_WDB_VISA_SEL_VISAByteSel_MIN                      (0)
  #define MC0_CH0_CR_WDB_VISA_SEL_VISAByteSel_MAX                      (7) // 0x00000007
  #define MC0_CH0_CR_WDB_VISA_SEL_VISAByteSel_DEF                      (0x00000000)
  #define MC0_CH0_CR_WDB_VISA_SEL_VISAByteSel_HSH                      (0x0300E458)

  #define MC0_CH0_CR_WDB_VISA_SEL_RefFSMRankSel_OFF                    ( 8)
  #define MC0_CH0_CR_WDB_VISA_SEL_RefFSMRankSel_WID                    ( 2)
  #define MC0_CH0_CR_WDB_VISA_SEL_RefFSMRankSel_MSK                    (0x00000300)
  #define MC0_CH0_CR_WDB_VISA_SEL_RefFSMRankSel_MIN                    (0)
  #define MC0_CH0_CR_WDB_VISA_SEL_RefFSMRankSel_MAX                    (3) // 0x00000003
  #define MC0_CH0_CR_WDB_VISA_SEL_RefFSMRankSel_DEF                    (0x00000000)
  #define MC0_CH0_CR_WDB_VISA_SEL_RefFSMRankSel_HSH                    (0x0210E458)

  #define MC0_CH0_CR_WDB_VISA_SEL_Ref_counterSel_OFF                   (10)
  #define MC0_CH0_CR_WDB_VISA_SEL_Ref_counterSel_WID                   ( 3)
  #define MC0_CH0_CR_WDB_VISA_SEL_Ref_counterSel_MSK                   (0x00001C00)
  #define MC0_CH0_CR_WDB_VISA_SEL_Ref_counterSel_MIN                   (0)
  #define MC0_CH0_CR_WDB_VISA_SEL_Ref_counterSel_MAX                   (7) // 0x00000007
  #define MC0_CH0_CR_WDB_VISA_SEL_Ref_counterSel_DEF                   (0x00000007)
  #define MC0_CH0_CR_WDB_VISA_SEL_Ref_counterSel_HSH                   (0x0314E458)

  #define MC0_CH0_CR_WDB_VISA_SEL_Enable_Ref_Status_Sample_OFF         (13)
  #define MC0_CH0_CR_WDB_VISA_SEL_Enable_Ref_Status_Sample_WID         ( 1)
  #define MC0_CH0_CR_WDB_VISA_SEL_Enable_Ref_Status_Sample_MSK         (0x00002000)
  #define MC0_CH0_CR_WDB_VISA_SEL_Enable_Ref_Status_Sample_MIN         (0)
  #define MC0_CH0_CR_WDB_VISA_SEL_Enable_Ref_Status_Sample_MAX         (1) // 0x00000001
  #define MC0_CH0_CR_WDB_VISA_SEL_Enable_Ref_Status_Sample_DEF         (0x00000000)
  #define MC0_CH0_CR_WDB_VISA_SEL_Enable_Ref_Status_Sample_HSH         (0x011AE458)

  #define MC0_CH0_CR_WDB_VISA_SEL_RHFSMRankSel_OFF                     (14)
  #define MC0_CH0_CR_WDB_VISA_SEL_RHFSMRankSel_WID                     ( 2)
  #define MC0_CH0_CR_WDB_VISA_SEL_RHFSMRankSel_MSK                     (0x0000C000)
  #define MC0_CH0_CR_WDB_VISA_SEL_RHFSMRankSel_MIN                     (0)
  #define MC0_CH0_CR_WDB_VISA_SEL_RHFSMRankSel_MAX                     (3) // 0x00000003
  #define MC0_CH0_CR_WDB_VISA_SEL_RHFSMRankSel_DEF                     (0x00000000)
  #define MC0_CH0_CR_WDB_VISA_SEL_RHFSMRankSel_HSH                     (0x021CE458)

#define MC0_CH0_CR_PASR_CTL_REG                                        (0x0000E45C)

  #define MC0_CH0_CR_PASR_CTL_PASR_regions_OFF                         ( 0)
  #define MC0_CH0_CR_PASR_CTL_PASR_regions_WID                         ( 8)
  #define MC0_CH0_CR_PASR_CTL_PASR_regions_MSK                         (0x000000FF)
  #define MC0_CH0_CR_PASR_CTL_PASR_regions_MIN                         (0)
  #define MC0_CH0_CR_PASR_CTL_PASR_regions_MAX                         (255) // 0x000000FF
  #define MC0_CH0_CR_PASR_CTL_PASR_regions_DEF                         (0x00000000)
  #define MC0_CH0_CR_PASR_CTL_PASR_regions_HSH                         (0x0800E45C)

  #define MC0_CH0_CR_PASR_CTL_Written_regions_OFF                      ( 8)
  #define MC0_CH0_CR_PASR_CTL_Written_regions_WID                      ( 8)
  #define MC0_CH0_CR_PASR_CTL_Written_regions_MSK                      (0x0000FF00)
  #define MC0_CH0_CR_PASR_CTL_Written_regions_MIN                      (0)
  #define MC0_CH0_CR_PASR_CTL_Written_regions_MAX                      (255) // 0x000000FF
  #define MC0_CH0_CR_PASR_CTL_Written_regions_DEF                      (0x00000000)
  #define MC0_CH0_CR_PASR_CTL_Written_regions_HSH                      (0x0810E45C)

  #define MC0_CH0_CR_PASR_CTL_Monitor_writes_OFF                       (16)
  #define MC0_CH0_CR_PASR_CTL_Monitor_writes_WID                       ( 1)
  #define MC0_CH0_CR_PASR_CTL_Monitor_writes_MSK                       (0x00010000)
  #define MC0_CH0_CR_PASR_CTL_Monitor_writes_MIN                       (0)
  #define MC0_CH0_CR_PASR_CTL_Monitor_writes_MAX                       (1) // 0x00000001
  #define MC0_CH0_CR_PASR_CTL_Monitor_writes_DEF                       (0x00000000)
  #define MC0_CH0_CR_PASR_CTL_Monitor_writes_HSH                       (0x0120E45C)

  #define MC0_CH0_CR_PASR_CTL_Send_command_OFF                         (17)
  #define MC0_CH0_CR_PASR_CTL_Send_command_WID                         ( 1)
  #define MC0_CH0_CR_PASR_CTL_Send_command_MSK                         (0x00020000)
  #define MC0_CH0_CR_PASR_CTL_Send_command_MIN                         (0)
  #define MC0_CH0_CR_PASR_CTL_Send_command_MAX                         (1) // 0x00000001
  #define MC0_CH0_CR_PASR_CTL_Send_command_DEF                         (0x00000000)
  #define MC0_CH0_CR_PASR_CTL_Send_command_HSH                         (0x0122E45C)

  #define MC0_CH0_CR_PASR_CTL_PASR_enabled_OFF                         (18)
  #define MC0_CH0_CR_PASR_CTL_PASR_enabled_WID                         ( 1)
  #define MC0_CH0_CR_PASR_CTL_PASR_enabled_MSK                         (0x00040000)
  #define MC0_CH0_CR_PASR_CTL_PASR_enabled_MIN                         (0)
  #define MC0_CH0_CR_PASR_CTL_PASR_enabled_MAX                         (1) // 0x00000001
  #define MC0_CH0_CR_PASR_CTL_PASR_enabled_DEF                         (0x00000000)
  #define MC0_CH0_CR_PASR_CTL_PASR_enabled_HSH                         (0x0124E45C)

  #define MC0_CH0_CR_PASR_CTL_Row_MSB_bit_OFF                          (19)
  #define MC0_CH0_CR_PASR_CTL_Row_MSB_bit_WID                          ( 2)
  #define MC0_CH0_CR_PASR_CTL_Row_MSB_bit_MSK                          (0x00180000)
  #define MC0_CH0_CR_PASR_CTL_Row_MSB_bit_MIN                          (0)
  #define MC0_CH0_CR_PASR_CTL_Row_MSB_bit_MAX                          (3) // 0x00000003
  #define MC0_CH0_CR_PASR_CTL_Row_MSB_bit_DEF                          (0x00000000)
  #define MC0_CH0_CR_PASR_CTL_Row_MSB_bit_HSH                          (0x0226E45C)

#define MC0_CH0_CR_PM_DIMM_IDLE_ENERGY_REG                             (0x0000E460)

  #define MC0_CH0_CR_PM_DIMM_IDLE_ENERGY_DIMM0_IDLE_ENERGY_OFF         ( 0)
  #define MC0_CH0_CR_PM_DIMM_IDLE_ENERGY_DIMM0_IDLE_ENERGY_WID         ( 6)
  #define MC0_CH0_CR_PM_DIMM_IDLE_ENERGY_DIMM0_IDLE_ENERGY_MSK         (0x0000003F)
  #define MC0_CH0_CR_PM_DIMM_IDLE_ENERGY_DIMM0_IDLE_ENERGY_MIN         (0)
  #define MC0_CH0_CR_PM_DIMM_IDLE_ENERGY_DIMM0_IDLE_ENERGY_MAX         (63) // 0x0000003F
  #define MC0_CH0_CR_PM_DIMM_IDLE_ENERGY_DIMM0_IDLE_ENERGY_DEF         (0x00000000)
  #define MC0_CH0_CR_PM_DIMM_IDLE_ENERGY_DIMM0_IDLE_ENERGY_HSH         (0x0600E460)

  #define MC0_CH0_CR_PM_DIMM_IDLE_ENERGY_DIMM1_IDLE_ENERGY_OFF         ( 8)
  #define MC0_CH0_CR_PM_DIMM_IDLE_ENERGY_DIMM1_IDLE_ENERGY_WID         ( 6)
  #define MC0_CH0_CR_PM_DIMM_IDLE_ENERGY_DIMM1_IDLE_ENERGY_MSK         (0x00003F00)
  #define MC0_CH0_CR_PM_DIMM_IDLE_ENERGY_DIMM1_IDLE_ENERGY_MIN         (0)
  #define MC0_CH0_CR_PM_DIMM_IDLE_ENERGY_DIMM1_IDLE_ENERGY_MAX         (63) // 0x0000003F
  #define MC0_CH0_CR_PM_DIMM_IDLE_ENERGY_DIMM1_IDLE_ENERGY_DEF         (0x00000000)
  #define MC0_CH0_CR_PM_DIMM_IDLE_ENERGY_DIMM1_IDLE_ENERGY_HSH         (0x0610E460)

#define MC0_CH0_CR_PM_DIMM_PD_ENERGY_REG                               (0x0000E464)

  #define MC0_CH0_CR_PM_DIMM_PD_ENERGY_DIMM0_PD_ENERGY_OFF             ( 0)
  #define MC0_CH0_CR_PM_DIMM_PD_ENERGY_DIMM0_PD_ENERGY_WID             ( 6)
  #define MC0_CH0_CR_PM_DIMM_PD_ENERGY_DIMM0_PD_ENERGY_MSK             (0x0000003F)
  #define MC0_CH0_CR_PM_DIMM_PD_ENERGY_DIMM0_PD_ENERGY_MIN             (0)
  #define MC0_CH0_CR_PM_DIMM_PD_ENERGY_DIMM0_PD_ENERGY_MAX             (63) // 0x0000003F
  #define MC0_CH0_CR_PM_DIMM_PD_ENERGY_DIMM0_PD_ENERGY_DEF             (0x00000000)
  #define MC0_CH0_CR_PM_DIMM_PD_ENERGY_DIMM0_PD_ENERGY_HSH             (0x0600E464)

  #define MC0_CH0_CR_PM_DIMM_PD_ENERGY_DIMM1_PD_ENERGY_OFF             ( 8)
  #define MC0_CH0_CR_PM_DIMM_PD_ENERGY_DIMM1_PD_ENERGY_WID             ( 6)
  #define MC0_CH0_CR_PM_DIMM_PD_ENERGY_DIMM1_PD_ENERGY_MSK             (0x00003F00)
  #define MC0_CH0_CR_PM_DIMM_PD_ENERGY_DIMM1_PD_ENERGY_MIN             (0)
  #define MC0_CH0_CR_PM_DIMM_PD_ENERGY_DIMM1_PD_ENERGY_MAX             (63) // 0x0000003F
  #define MC0_CH0_CR_PM_DIMM_PD_ENERGY_DIMM1_PD_ENERGY_DEF             (0x00000000)
  #define MC0_CH0_CR_PM_DIMM_PD_ENERGY_DIMM1_PD_ENERGY_HSH             (0x0610E464)

#define MC0_CH0_CR_PM_DIMM_ACT_ENERGY_REG                              (0x0000E468)

  #define MC0_CH0_CR_PM_DIMM_ACT_ENERGY_DIMM0_ACT_ENERGY_OFF           ( 0)
  #define MC0_CH0_CR_PM_DIMM_ACT_ENERGY_DIMM0_ACT_ENERGY_WID           ( 8)
  #define MC0_CH0_CR_PM_DIMM_ACT_ENERGY_DIMM0_ACT_ENERGY_MSK           (0x000000FF)
  #define MC0_CH0_CR_PM_DIMM_ACT_ENERGY_DIMM0_ACT_ENERGY_MIN           (0)
  #define MC0_CH0_CR_PM_DIMM_ACT_ENERGY_DIMM0_ACT_ENERGY_MAX           (255) // 0x000000FF
  #define MC0_CH0_CR_PM_DIMM_ACT_ENERGY_DIMM0_ACT_ENERGY_DEF           (0x00000000)
  #define MC0_CH0_CR_PM_DIMM_ACT_ENERGY_DIMM0_ACT_ENERGY_HSH           (0x0800E468)

  #define MC0_CH0_CR_PM_DIMM_ACT_ENERGY_DIMM1_ACT_ENERGY_OFF           ( 8)
  #define MC0_CH0_CR_PM_DIMM_ACT_ENERGY_DIMM1_ACT_ENERGY_WID           ( 8)
  #define MC0_CH0_CR_PM_DIMM_ACT_ENERGY_DIMM1_ACT_ENERGY_MSK           (0x0000FF00)
  #define MC0_CH0_CR_PM_DIMM_ACT_ENERGY_DIMM1_ACT_ENERGY_MIN           (0)
  #define MC0_CH0_CR_PM_DIMM_ACT_ENERGY_DIMM1_ACT_ENERGY_MAX           (255) // 0x000000FF
  #define MC0_CH0_CR_PM_DIMM_ACT_ENERGY_DIMM1_ACT_ENERGY_DEF           (0x00000000)
  #define MC0_CH0_CR_PM_DIMM_ACT_ENERGY_DIMM1_ACT_ENERGY_HSH           (0x0810E468)

#define MC0_CH0_CR_PM_DIMM_RD_ENERGY_REG                               (0x0000E46C)

  #define MC0_CH0_CR_PM_DIMM_RD_ENERGY_DIMM0_RD_ENERGY_OFF             ( 0)
  #define MC0_CH0_CR_PM_DIMM_RD_ENERGY_DIMM0_RD_ENERGY_WID             ( 8)
  #define MC0_CH0_CR_PM_DIMM_RD_ENERGY_DIMM0_RD_ENERGY_MSK             (0x000000FF)
  #define MC0_CH0_CR_PM_DIMM_RD_ENERGY_DIMM0_RD_ENERGY_MIN             (0)
  #define MC0_CH0_CR_PM_DIMM_RD_ENERGY_DIMM0_RD_ENERGY_MAX             (255) // 0x000000FF
  #define MC0_CH0_CR_PM_DIMM_RD_ENERGY_DIMM0_RD_ENERGY_DEF             (0x00000000)
  #define MC0_CH0_CR_PM_DIMM_RD_ENERGY_DIMM0_RD_ENERGY_HSH             (0x0800E46C)

  #define MC0_CH0_CR_PM_DIMM_RD_ENERGY_DIMM1_RD_ENERGY_OFF             ( 8)
  #define MC0_CH0_CR_PM_DIMM_RD_ENERGY_DIMM1_RD_ENERGY_WID             ( 8)
  #define MC0_CH0_CR_PM_DIMM_RD_ENERGY_DIMM1_RD_ENERGY_MSK             (0x0000FF00)
  #define MC0_CH0_CR_PM_DIMM_RD_ENERGY_DIMM1_RD_ENERGY_MIN             (0)
  #define MC0_CH0_CR_PM_DIMM_RD_ENERGY_DIMM1_RD_ENERGY_MAX             (255) // 0x000000FF
  #define MC0_CH0_CR_PM_DIMM_RD_ENERGY_DIMM1_RD_ENERGY_DEF             (0x00000000)
  #define MC0_CH0_CR_PM_DIMM_RD_ENERGY_DIMM1_RD_ENERGY_HSH             (0x0810E46C)

#define MC0_CH0_CR_PM_DIMM_WR_ENERGY_REG                               (0x0000E470)

  #define MC0_CH0_CR_PM_DIMM_WR_ENERGY_DIMM0_WR_ENERGY_OFF             ( 0)
  #define MC0_CH0_CR_PM_DIMM_WR_ENERGY_DIMM0_WR_ENERGY_WID             ( 8)
  #define MC0_CH0_CR_PM_DIMM_WR_ENERGY_DIMM0_WR_ENERGY_MSK             (0x000000FF)
  #define MC0_CH0_CR_PM_DIMM_WR_ENERGY_DIMM0_WR_ENERGY_MIN             (0)
  #define MC0_CH0_CR_PM_DIMM_WR_ENERGY_DIMM0_WR_ENERGY_MAX             (255) // 0x000000FF
  #define MC0_CH0_CR_PM_DIMM_WR_ENERGY_DIMM0_WR_ENERGY_DEF             (0x00000000)
  #define MC0_CH0_CR_PM_DIMM_WR_ENERGY_DIMM0_WR_ENERGY_HSH             (0x0800E470)

  #define MC0_CH0_CR_PM_DIMM_WR_ENERGY_DIMM1_WR_ENERGY_OFF             ( 8)
  #define MC0_CH0_CR_PM_DIMM_WR_ENERGY_DIMM1_WR_ENERGY_WID             ( 8)
  #define MC0_CH0_CR_PM_DIMM_WR_ENERGY_DIMM1_WR_ENERGY_MSK             (0x0000FF00)
  #define MC0_CH0_CR_PM_DIMM_WR_ENERGY_DIMM1_WR_ENERGY_MIN             (0)
  #define MC0_CH0_CR_PM_DIMM_WR_ENERGY_DIMM1_WR_ENERGY_MAX             (255) // 0x000000FF
  #define MC0_CH0_CR_PM_DIMM_WR_ENERGY_DIMM1_WR_ENERGY_DEF             (0x00000000)
  #define MC0_CH0_CR_PM_DIMM_WR_ENERGY_DIMM1_WR_ENERGY_HSH             (0x0810E470)

#define MC0_CH0_CR_RH_TRR_ADDRESS_REG                                  (0x0000E474)

  #define MC0_CH0_CR_RH_TRR_ADDRESS_Valid_OFF                          ( 0)
  #define MC0_CH0_CR_RH_TRR_ADDRESS_Valid_WID                          ( 1)
  #define MC0_CH0_CR_RH_TRR_ADDRESS_Valid_MSK                          (0x00000001)
  #define MC0_CH0_CR_RH_TRR_ADDRESS_Valid_MIN                          (0)
  #define MC0_CH0_CR_RH_TRR_ADDRESS_Valid_MAX                          (1) // 0x00000001
  #define MC0_CH0_CR_RH_TRR_ADDRESS_Valid_DEF                          (0x00000000)
  #define MC0_CH0_CR_RH_TRR_ADDRESS_Valid_HSH                          (0x0100E474)

  #define MC0_CH0_CR_RH_TRR_ADDRESS_Rank_OFF                           ( 1)
  #define MC0_CH0_CR_RH_TRR_ADDRESS_Rank_WID                           ( 2)
  #define MC0_CH0_CR_RH_TRR_ADDRESS_Rank_MSK                           (0x00000006)
  #define MC0_CH0_CR_RH_TRR_ADDRESS_Rank_MIN                           (0)
  #define MC0_CH0_CR_RH_TRR_ADDRESS_Rank_MAX                           (3) // 0x00000003
  #define MC0_CH0_CR_RH_TRR_ADDRESS_Rank_DEF                           (0x00000000)
  #define MC0_CH0_CR_RH_TRR_ADDRESS_Rank_HSH                           (0x0202E474)

  #define MC0_CH0_CR_RH_TRR_ADDRESS_Group_OFF                          ( 3)
  #define MC0_CH0_CR_RH_TRR_ADDRESS_Group_WID                          ( 2)
  #define MC0_CH0_CR_RH_TRR_ADDRESS_Group_MSK                          (0x00000018)
  #define MC0_CH0_CR_RH_TRR_ADDRESS_Group_MIN                          (0)
  #define MC0_CH0_CR_RH_TRR_ADDRESS_Group_MAX                          (3) // 0x00000003
  #define MC0_CH0_CR_RH_TRR_ADDRESS_Group_DEF                          (0x00000000)
  #define MC0_CH0_CR_RH_TRR_ADDRESS_Group_HSH                          (0x0206E474)

  #define MC0_CH0_CR_RH_TRR_ADDRESS_Bank_OFF                           ( 5)
  #define MC0_CH0_CR_RH_TRR_ADDRESS_Bank_WID                           ( 3)
  #define MC0_CH0_CR_RH_TRR_ADDRESS_Bank_MSK                           (0x000000E0)
  #define MC0_CH0_CR_RH_TRR_ADDRESS_Bank_MIN                           (0)
  #define MC0_CH0_CR_RH_TRR_ADDRESS_Bank_MAX                           (7) // 0x00000007
  #define MC0_CH0_CR_RH_TRR_ADDRESS_Bank_DEF                           (0x00000000)
  #define MC0_CH0_CR_RH_TRR_ADDRESS_Bank_HSH                           (0x030AE474)

  #define MC0_CH0_CR_RH_TRR_ADDRESS_Row_OFF                            ( 8)
  #define MC0_CH0_CR_RH_TRR_ADDRESS_Row_WID                            (18)
  #define MC0_CH0_CR_RH_TRR_ADDRESS_Row_MSK                            (0x03FFFF00)
  #define MC0_CH0_CR_RH_TRR_ADDRESS_Row_MIN                            (0)
  #define MC0_CH0_CR_RH_TRR_ADDRESS_Row_MAX                            (262143) // 0x0003FFFF
  #define MC0_CH0_CR_RH_TRR_ADDRESS_Row_DEF                            (0x00000000)
  #define MC0_CH0_CR_RH_TRR_ADDRESS_Row_HSH                            (0x1210E474)

#define MC0_CH0_CR_SC_WR_DELAY_REG                                     (0x0000E478)

  #define MC0_CH0_CR_SC_WR_DELAY_Dec_tCWL_OFF                          ( 0)
  #define MC0_CH0_CR_SC_WR_DELAY_Dec_tCWL_WID                          ( 6)
  #define MC0_CH0_CR_SC_WR_DELAY_Dec_tCWL_MSK                          (0x0000003F)
  #define MC0_CH0_CR_SC_WR_DELAY_Dec_tCWL_MIN                          (0)
  #define MC0_CH0_CR_SC_WR_DELAY_Dec_tCWL_MAX                          (63) // 0x0000003F
  #define MC0_CH0_CR_SC_WR_DELAY_Dec_tCWL_DEF                          (0x00000003)
  #define MC0_CH0_CR_SC_WR_DELAY_Dec_tCWL_HSH                          (0x0600E478)

  #define MC0_CH0_CR_SC_WR_DELAY_Add_tCWL_OFF                          ( 6)
  #define MC0_CH0_CR_SC_WR_DELAY_Add_tCWL_WID                          ( 6)
  #define MC0_CH0_CR_SC_WR_DELAY_Add_tCWL_MSK                          (0x00000FC0)
  #define MC0_CH0_CR_SC_WR_DELAY_Add_tCWL_MIN                          (0)
  #define MC0_CH0_CR_SC_WR_DELAY_Add_tCWL_MAX                          (63) // 0x0000003F
  #define MC0_CH0_CR_SC_WR_DELAY_Add_tCWL_DEF                          (0x00000000)
  #define MC0_CH0_CR_SC_WR_DELAY_Add_tCWL_HSH                          (0x060CE478)

  #define MC0_CH0_CR_SC_WR_DELAY_Add_1Qclk_delay_OFF                   (12)
  #define MC0_CH0_CR_SC_WR_DELAY_Add_1Qclk_delay_WID                   ( 1)
  #define MC0_CH0_CR_SC_WR_DELAY_Add_1Qclk_delay_MSK                   (0x00001000)
  #define MC0_CH0_CR_SC_WR_DELAY_Add_1Qclk_delay_MIN                   (0)
  #define MC0_CH0_CR_SC_WR_DELAY_Add_1Qclk_delay_MAX                   (1) // 0x00000001
  #define MC0_CH0_CR_SC_WR_DELAY_Add_1Qclk_delay_DEF                   (0x00000000)
  #define MC0_CH0_CR_SC_WR_DELAY_Add_1Qclk_delay_HSH                   (0x0118E478)

#define MC0_CH0_CR_READ_RETURN_DFT_REG                                 (0x0000E47C)

  #define MC0_CH0_CR_READ_RETURN_DFT_ECC_OFF                           ( 0)
  #define MC0_CH0_CR_READ_RETURN_DFT_ECC_WID                           ( 8)
  #define MC0_CH0_CR_READ_RETURN_DFT_ECC_MSK                           (0x000000FF)
  #define MC0_CH0_CR_READ_RETURN_DFT_ECC_MIN                           (0)
  #define MC0_CH0_CR_READ_RETURN_DFT_ECC_MAX                           (255) // 0x000000FF
  #define MC0_CH0_CR_READ_RETURN_DFT_ECC_DEF                           (0x00000000)
  #define MC0_CH0_CR_READ_RETURN_DFT_ECC_HSH                           (0x0800E47C)

  #define MC0_CH0_CR_READ_RETURN_DFT_RRD_DFT_Mode_OFF                  ( 8)
  #define MC0_CH0_CR_READ_RETURN_DFT_RRD_DFT_Mode_WID                  ( 2)
  #define MC0_CH0_CR_READ_RETURN_DFT_RRD_DFT_Mode_MSK                  (0x00000300)
  #define MC0_CH0_CR_READ_RETURN_DFT_RRD_DFT_Mode_MIN                  (0)
  #define MC0_CH0_CR_READ_RETURN_DFT_RRD_DFT_Mode_MAX                  (3) // 0x00000003
  #define MC0_CH0_CR_READ_RETURN_DFT_RRD_DFT_Mode_DEF                  (0x00000000)
  #define MC0_CH0_CR_READ_RETURN_DFT_RRD_DFT_Mode_HSH                  (0x0210E47C)

  #define MC0_CH0_CR_READ_RETURN_DFT_LFSR_Seed_Index_OFF               (10)
  #define MC0_CH0_CR_READ_RETURN_DFT_LFSR_Seed_Index_WID               ( 5)
  #define MC0_CH0_CR_READ_RETURN_DFT_LFSR_Seed_Index_MSK               (0x00007C00)
  #define MC0_CH0_CR_READ_RETURN_DFT_LFSR_Seed_Index_MIN               (0)
  #define MC0_CH0_CR_READ_RETURN_DFT_LFSR_Seed_Index_MAX               (31) // 0x0000001F
  #define MC0_CH0_CR_READ_RETURN_DFT_LFSR_Seed_Index_DEF               (0x00000000)
  #define MC0_CH0_CR_READ_RETURN_DFT_LFSR_Seed_Index_HSH               (0x0514E47C)

  #define MC0_CH0_CR_READ_RETURN_DFT_Inversion_Mode_OFF                (15)
  #define MC0_CH0_CR_READ_RETURN_DFT_Inversion_Mode_WID                ( 1)
  #define MC0_CH0_CR_READ_RETURN_DFT_Inversion_Mode_MSK                (0x00008000)
  #define MC0_CH0_CR_READ_RETURN_DFT_Inversion_Mode_MIN                (0)
  #define MC0_CH0_CR_READ_RETURN_DFT_Inversion_Mode_MAX                (1) // 0x00000001
  #define MC0_CH0_CR_READ_RETURN_DFT_Inversion_Mode_DEF                (0x00000000)
  #define MC0_CH0_CR_READ_RETURN_DFT_Inversion_Mode_HSH                (0x011EE47C)

  #define MC0_CH0_CR_READ_RETURN_DFT_AddressBased_OFF                  (16)
  #define MC0_CH0_CR_READ_RETURN_DFT_AddressBased_WID                  ( 1)
  #define MC0_CH0_CR_READ_RETURN_DFT_AddressBased_MSK                  (0x00010000)
  #define MC0_CH0_CR_READ_RETURN_DFT_AddressBased_MIN                  (0)
  #define MC0_CH0_CR_READ_RETURN_DFT_AddressBased_MAX                  (1) // 0x00000001
  #define MC0_CH0_CR_READ_RETURN_DFT_AddressBased_DEF                  (0x00000000)
  #define MC0_CH0_CR_READ_RETURN_DFT_AddressBased_HSH                  (0x0120E47C)

  #define MC0_CH0_CR_READ_RETURN_DFT_AddressBasedRRD_channel_OFF       (17)
  #define MC0_CH0_CR_READ_RETURN_DFT_AddressBasedRRD_channel_WID       ( 1)
  #define MC0_CH0_CR_READ_RETURN_DFT_AddressBasedRRD_channel_MSK       (0x00020000)
  #define MC0_CH0_CR_READ_RETURN_DFT_AddressBasedRRD_channel_MIN       (0)
  #define MC0_CH0_CR_READ_RETURN_DFT_AddressBasedRRD_channel_MAX       (1) // 0x00000001
  #define MC0_CH0_CR_READ_RETURN_DFT_AddressBasedRRD_channel_DEF       (0x00000000)
  #define MC0_CH0_CR_READ_RETURN_DFT_AddressBasedRRD_channel_HSH       (0x0122E47C)

#define MC0_CH0_CR_DESWIZZLE_LOW_REG                                   (0x0000E480)
//Duplicate of MC0_CH0_CR_DESWIZZLE_LOW_ERM_REG

#define MC0_CH0_CR_SC_PBR_REG                                          (0x0000E488)

  #define MC0_CH0_CR_SC_PBR_PBR_Disable_OFF                            ( 0)
  #define MC0_CH0_CR_SC_PBR_PBR_Disable_WID                            ( 1)
  #define MC0_CH0_CR_SC_PBR_PBR_Disable_MSK                            (0x00000001)
  #define MC0_CH0_CR_SC_PBR_PBR_Disable_MIN                            (0)
  #define MC0_CH0_CR_SC_PBR_PBR_Disable_MAX                            (1) // 0x00000001
  #define MC0_CH0_CR_SC_PBR_PBR_Disable_DEF                            (0x00000001)
  #define MC0_CH0_CR_SC_PBR_PBR_Disable_HSH                            (0x0100E488)

  #define MC0_CH0_CR_SC_PBR_PBR_OOO_Dis_OFF                            ( 1)
  #define MC0_CH0_CR_SC_PBR_PBR_OOO_Dis_WID                            ( 1)
  #define MC0_CH0_CR_SC_PBR_PBR_OOO_Dis_MSK                            (0x00000002)
  #define MC0_CH0_CR_SC_PBR_PBR_OOO_Dis_MIN                            (0)
  #define MC0_CH0_CR_SC_PBR_PBR_OOO_Dis_MAX                            (1) // 0x00000001
  #define MC0_CH0_CR_SC_PBR_PBR_OOO_Dis_DEF                            (0x00000000)
  #define MC0_CH0_CR_SC_PBR_PBR_OOO_Dis_HSH                            (0x0102E488)

  #define MC0_CH0_CR_SC_PBR_PBR_Issue_NOP_OFF                          ( 2)
  #define MC0_CH0_CR_SC_PBR_PBR_Issue_NOP_WID                          ( 1)
  #define MC0_CH0_CR_SC_PBR_PBR_Issue_NOP_MSK                          (0x00000004)
  #define MC0_CH0_CR_SC_PBR_PBR_Issue_NOP_MIN                          (0)
  #define MC0_CH0_CR_SC_PBR_PBR_Issue_NOP_MAX                          (1) // 0x00000001
  #define MC0_CH0_CR_SC_PBR_PBR_Issue_NOP_DEF                          (0x00000000)
  #define MC0_CH0_CR_SC_PBR_PBR_Issue_NOP_HSH                          (0x0104E488)

  #define MC0_CH0_CR_SC_PBR_PBR_Disable_on_hot_OFF                     ( 3)
  #define MC0_CH0_CR_SC_PBR_PBR_Disable_on_hot_WID                     ( 1)
  #define MC0_CH0_CR_SC_PBR_PBR_Disable_on_hot_MSK                     (0x00000008)
  #define MC0_CH0_CR_SC_PBR_PBR_Disable_on_hot_MIN                     (0)
  #define MC0_CH0_CR_SC_PBR_PBR_Disable_on_hot_MAX                     (1) // 0x00000001
  #define MC0_CH0_CR_SC_PBR_PBR_Disable_on_hot_DEF                     (0x00000000)
  #define MC0_CH0_CR_SC_PBR_PBR_Disable_on_hot_HSH                     (0x0106E488)

  #define MC0_CH0_CR_SC_PBR_PBR_Exit_on_Idle_Cnt_OFF                   ( 4)
  #define MC0_CH0_CR_SC_PBR_PBR_Exit_on_Idle_Cnt_WID                   ( 6)
  #define MC0_CH0_CR_SC_PBR_PBR_Exit_on_Idle_Cnt_MSK                   (0x000003F0)
  #define MC0_CH0_CR_SC_PBR_PBR_Exit_on_Idle_Cnt_MIN                   (0)
  #define MC0_CH0_CR_SC_PBR_PBR_Exit_on_Idle_Cnt_MAX                   (63) // 0x0000003F
  #define MC0_CH0_CR_SC_PBR_PBR_Exit_on_Idle_Cnt_DEF                   (0x00000001)
  #define MC0_CH0_CR_SC_PBR_PBR_Exit_on_Idle_Cnt_HSH                   (0x0608E488)

  #define MC0_CH0_CR_SC_PBR_tRFCpb_OFF                                 (10)
  #define MC0_CH0_CR_SC_PBR_tRFCpb_WID                                 (11)
  #define MC0_CH0_CR_SC_PBR_tRFCpb_MSK                                 (0x001FFC00)
  #define MC0_CH0_CR_SC_PBR_tRFCpb_MIN                                 (0)
  #define MC0_CH0_CR_SC_PBR_tRFCpb_MAX                                 (2047) // 0x000007FF
  #define MC0_CH0_CR_SC_PBR_tRFCpb_DEF                                 (0x0000003C)
  #define MC0_CH0_CR_SC_PBR_tRFCpb_HSH                                 (0x0B14E488)

  #define MC0_CH0_CR_SC_PBR_Refresh_ABR_release_OFF                    (21)
  #define MC0_CH0_CR_SC_PBR_Refresh_ABR_release_WID                    ( 4)
  #define MC0_CH0_CR_SC_PBR_Refresh_ABR_release_MSK                    (0x01E00000)
  #define MC0_CH0_CR_SC_PBR_Refresh_ABR_release_MIN                    (0)
  #define MC0_CH0_CR_SC_PBR_Refresh_ABR_release_MAX                    (15) // 0x0000000F
  #define MC0_CH0_CR_SC_PBR_Refresh_ABR_release_DEF                    (0x00000007)
  #define MC0_CH0_CR_SC_PBR_Refresh_ABR_release_HSH                    (0x042AE488)

  #define MC0_CH0_CR_SC_PBR_PBR_extended_disable_while_DQS_OSC_OFF     (25)
  #define MC0_CH0_CR_SC_PBR_PBR_extended_disable_while_DQS_OSC_WID     ( 1)
  #define MC0_CH0_CR_SC_PBR_PBR_extended_disable_while_DQS_OSC_MSK     (0x02000000)
  #define MC0_CH0_CR_SC_PBR_PBR_extended_disable_while_DQS_OSC_MIN     (0)
  #define MC0_CH0_CR_SC_PBR_PBR_extended_disable_while_DQS_OSC_MAX     (1) // 0x00000001
  #define MC0_CH0_CR_SC_PBR_PBR_extended_disable_while_DQS_OSC_DEF     (0x00000001)
  #define MC0_CH0_CR_SC_PBR_PBR_extended_disable_while_DQS_OSC_HSH     (0x0132E488)

  #define MC0_CH0_CR_SC_PBR_Disable_NoPanic_PBR_Cbit_OFF               (26)
  #define MC0_CH0_CR_SC_PBR_Disable_NoPanic_PBR_Cbit_WID               ( 1)
  #define MC0_CH0_CR_SC_PBR_Disable_NoPanic_PBR_Cbit_MSK               (0x04000000)
  #define MC0_CH0_CR_SC_PBR_Disable_NoPanic_PBR_Cbit_MIN               (0)
  #define MC0_CH0_CR_SC_PBR_Disable_NoPanic_PBR_Cbit_MAX               (1) // 0x00000001
  #define MC0_CH0_CR_SC_PBR_Disable_NoPanic_PBR_Cbit_DEF               (0x00000000)
  #define MC0_CH0_CR_SC_PBR_Disable_NoPanic_PBR_Cbit_HSH               (0x0134E488)

  #define MC0_CH0_CR_SC_PBR_PBR_Idle_Bank_Condition_WR_dis_OFF         (27)
  #define MC0_CH0_CR_SC_PBR_PBR_Idle_Bank_Condition_WR_dis_WID         ( 1)
  #define MC0_CH0_CR_SC_PBR_PBR_Idle_Bank_Condition_WR_dis_MSK         (0x08000000)
  #define MC0_CH0_CR_SC_PBR_PBR_Idle_Bank_Condition_WR_dis_MIN         (0)
  #define MC0_CH0_CR_SC_PBR_PBR_Idle_Bank_Condition_WR_dis_MAX         (1) // 0x00000001
  #define MC0_CH0_CR_SC_PBR_PBR_Idle_Bank_Condition_WR_dis_DEF         (0x00000001)
  #define MC0_CH0_CR_SC_PBR_PBR_Idle_Bank_Condition_WR_dis_HSH         (0x0136E488)

  #define MC0_CH0_CR_SC_PBR_PBR_High_WM_A0_OFF                            (28)
  #define MC0_CH0_CR_SC_PBR_PBR_High_WM_A0_WID                            ( 4)
  #define MC0_CH0_CR_SC_PBR_PBR_High_WM_A0_MSK                            (0xF0000000)
  #define MC0_CH0_CR_SC_PBR_PBR_High_WM_A0_MIN                            (0)
  #define MC0_CH0_CR_SC_PBR_PBR_High_WM_A0_MAX                            (15) // 0x0000000F
  #define MC0_CH0_CR_SC_PBR_PBR_High_WM_A0_DEF                            (0x00000006)
  #define MC0_CH0_CR_SC_PBR_PBR_High_WM_A0_HSH                            (0x0438E488)

#define MC0_CH0_CR_TC_LPDDR4_MISC_REG                                  (0x0000E494)

  #define MC0_CH0_CR_TC_LPDDR4_MISC_tOSCO_OFF                          ( 0)
  #define MC0_CH0_CR_TC_LPDDR4_MISC_tOSCO_WID                          ( 8)
  #define MC0_CH0_CR_TC_LPDDR4_MISC_tOSCO_MSK                          (0x000000FF)
  #define MC0_CH0_CR_TC_LPDDR4_MISC_tOSCO_MIN                          (0)
  #define MC0_CH0_CR_TC_LPDDR4_MISC_tOSCO_MAX                          (255) // 0x000000FF
  #define MC0_CH0_CR_TC_LPDDR4_MISC_tOSCO_DEF                          (0x00000056)
  #define MC0_CH0_CR_TC_LPDDR4_MISC_tOSCO_HSH                          (0x0800E494)

  #define MC0_CH0_CR_TC_LPDDR4_MISC_tPREMRR_OFF                        ( 8)
  #define MC0_CH0_CR_TC_LPDDR4_MISC_tPREMRR_WID                        ( 7)

  #define MC0_CH0_CR_TC_LPDDR4_MISC_tPREMRR_MSK                        (0x00007F00)

  #define MC0_CH0_CR_TC_LPDDR4_MISC_tPREMRR_MIN                        (0)
  #define MC0_CH0_CR_TC_LPDDR4_MISC_tPREMRR_MAX                        (127) // 0x0000007F

  #define MC0_CH0_CR_TC_LPDDR4_MISC_tPREMRR_DEF                        (0x00000010)
  #define MC0_CH0_CR_TC_LPDDR4_MISC_tPREMRR_HSH                        (0x0710E494)


  #define MC0_CH0_CR_TC_LPDDR4_MISC_tMRRMRW_OFF                        (15)

  #define MC0_CH0_CR_TC_LPDDR4_MISC_tMRRMRW_WID                        ( 7)
  #define MC0_CH0_CR_TC_LPDDR4_MISC_tMRRMRW_MSK                        (0x003F8000)

  #define MC0_CH0_CR_TC_LPDDR4_MISC_tMRRMRW_MIN                        (0)
  #define MC0_CH0_CR_TC_LPDDR4_MISC_tMRRMRW_MAX                        (127) // 0x0000007F
  #define MC0_CH0_CR_TC_LPDDR4_MISC_tMRRMRW_DEF                        (0x00000010)
  #define MC0_CH0_CR_TC_LPDDR4_MISC_tMRRMRW_HSH                        (0x071EE494)


  #define MC0_CH0_CR_TC_LPDDR4_MISC_tMRR_OFF                           (22)

  #define MC0_CH0_CR_TC_LPDDR4_MISC_tMRR_WID                           ( 7)

  #define MC0_CH0_CR_TC_LPDDR4_MISC_tMRR_MSK                           (0x1FC00000)

  #define MC0_CH0_CR_TC_LPDDR4_MISC_tMRR_MIN                           (0)
  #define MC0_CH0_CR_TC_LPDDR4_MISC_tMRR_MAX                           (127) // 0x0000007F

  #define MC0_CH0_CR_TC_LPDDR4_MISC_tMRR_DEF                           (0x00000010)
  #define MC0_CH0_CR_TC_LPDDR4_MISC_tMRR_HSH                           (0x072CE494)


  #define MC0_CH0_CR_TC_LPDDR4_MISC_Manual_DQS_MR_READ_OFF             (29)

  #define MC0_CH0_CR_TC_LPDDR4_MISC_Manual_DQS_MR_READ_WID             ( 1)
  #define MC0_CH0_CR_TC_LPDDR4_MISC_Manual_DQS_MR_READ_MSK             (0x20000000)

  #define MC0_CH0_CR_TC_LPDDR4_MISC_Manual_DQS_MR_READ_MIN             (0)
  #define MC0_CH0_CR_TC_LPDDR4_MISC_Manual_DQS_MR_READ_MAX             (1) // 0x00000001
  #define MC0_CH0_CR_TC_LPDDR4_MISC_Manual_DQS_MR_READ_DEF             (0x00000000)
  #define MC0_CH0_CR_TC_LPDDR4_MISC_Manual_DQS_MR_READ_HSH             (0x013AE494)


#define MC0_CH0_CR_DESWIZZLE_HIGH_ERM_REG                              (0x0000E498)

  #define MC0_CH0_CR_DESWIZZLE_HIGH_ERM_Bit_16_OFF                     ( 0)
  #define MC0_CH0_CR_DESWIZZLE_HIGH_ERM_Bit_16_WID                     ( 3)
  #define MC0_CH0_CR_DESWIZZLE_HIGH_ERM_Bit_16_MSK                     (0x00000007)
  #define MC0_CH0_CR_DESWIZZLE_HIGH_ERM_Bit_16_MIN                     (0)
  #define MC0_CH0_CR_DESWIZZLE_HIGH_ERM_Bit_16_MAX                     (7) // 0x00000007
  #define MC0_CH0_CR_DESWIZZLE_HIGH_ERM_Bit_16_DEF                     (0x00000000)
  #define MC0_CH0_CR_DESWIZZLE_HIGH_ERM_Bit_16_HSH                     (0x4300E498)

  #define MC0_CH0_CR_DESWIZZLE_HIGH_ERM_Bit_17_OFF                     ( 3)
  #define MC0_CH0_CR_DESWIZZLE_HIGH_ERM_Bit_17_WID                     ( 3)
  #define MC0_CH0_CR_DESWIZZLE_HIGH_ERM_Bit_17_MSK                     (0x00000038)
  #define MC0_CH0_CR_DESWIZZLE_HIGH_ERM_Bit_17_MIN                     (0)
  #define MC0_CH0_CR_DESWIZZLE_HIGH_ERM_Bit_17_MAX                     (7) // 0x00000007
  #define MC0_CH0_CR_DESWIZZLE_HIGH_ERM_Bit_17_DEF                     (0x00000001)
  #define MC0_CH0_CR_DESWIZZLE_HIGH_ERM_Bit_17_HSH                     (0x4306E498)

  #define MC0_CH0_CR_DESWIZZLE_HIGH_ERM_Bit_18_OFF                     ( 6)
  #define MC0_CH0_CR_DESWIZZLE_HIGH_ERM_Bit_18_WID                     ( 3)
  #define MC0_CH0_CR_DESWIZZLE_HIGH_ERM_Bit_18_MSK                     (0x000001C0)
  #define MC0_CH0_CR_DESWIZZLE_HIGH_ERM_Bit_18_MIN                     (0)
  #define MC0_CH0_CR_DESWIZZLE_HIGH_ERM_Bit_18_MAX                     (7) // 0x00000007
  #define MC0_CH0_CR_DESWIZZLE_HIGH_ERM_Bit_18_DEF                     (0x00000002)
  #define MC0_CH0_CR_DESWIZZLE_HIGH_ERM_Bit_18_HSH                     (0x430CE498)

  #define MC0_CH0_CR_DESWIZZLE_HIGH_ERM_Bit_19_OFF                     ( 9)
  #define MC0_CH0_CR_DESWIZZLE_HIGH_ERM_Bit_19_WID                     ( 3)
  #define MC0_CH0_CR_DESWIZZLE_HIGH_ERM_Bit_19_MSK                     (0x00000E00)
  #define MC0_CH0_CR_DESWIZZLE_HIGH_ERM_Bit_19_MIN                     (0)
  #define MC0_CH0_CR_DESWIZZLE_HIGH_ERM_Bit_19_MAX                     (7) // 0x00000007
  #define MC0_CH0_CR_DESWIZZLE_HIGH_ERM_Bit_19_DEF                     (0x00000003)
  #define MC0_CH0_CR_DESWIZZLE_HIGH_ERM_Bit_19_HSH                     (0x4312E498)

  #define MC0_CH0_CR_DESWIZZLE_HIGH_ERM_Bit_20_OFF                     (12)
  #define MC0_CH0_CR_DESWIZZLE_HIGH_ERM_Bit_20_WID                     ( 3)
  #define MC0_CH0_CR_DESWIZZLE_HIGH_ERM_Bit_20_MSK                     (0x00007000)
  #define MC0_CH0_CR_DESWIZZLE_HIGH_ERM_Bit_20_MIN                     (0)
  #define MC0_CH0_CR_DESWIZZLE_HIGH_ERM_Bit_20_MAX                     (7) // 0x00000007
  #define MC0_CH0_CR_DESWIZZLE_HIGH_ERM_Bit_20_DEF                     (0x00000004)
  #define MC0_CH0_CR_DESWIZZLE_HIGH_ERM_Bit_20_HSH                     (0x4318E498)

  #define MC0_CH0_CR_DESWIZZLE_HIGH_ERM_Bit_24_OFF                     (15)
  #define MC0_CH0_CR_DESWIZZLE_HIGH_ERM_Bit_24_WID                     ( 3)
  #define MC0_CH0_CR_DESWIZZLE_HIGH_ERM_Bit_24_MSK                     (0x00038000)
  #define MC0_CH0_CR_DESWIZZLE_HIGH_ERM_Bit_24_MIN                     (0)
  #define MC0_CH0_CR_DESWIZZLE_HIGH_ERM_Bit_24_MAX                     (7) // 0x00000007
  #define MC0_CH0_CR_DESWIZZLE_HIGH_ERM_Bit_24_DEF                     (0x00000000)
  #define MC0_CH0_CR_DESWIZZLE_HIGH_ERM_Bit_24_HSH                     (0x431EE498)

  #define MC0_CH0_CR_DESWIZZLE_HIGH_ERM_Bit_25_OFF                     (18)
  #define MC0_CH0_CR_DESWIZZLE_HIGH_ERM_Bit_25_WID                     ( 3)
  #define MC0_CH0_CR_DESWIZZLE_HIGH_ERM_Bit_25_MSK                     (0x001C0000)
  #define MC0_CH0_CR_DESWIZZLE_HIGH_ERM_Bit_25_MIN                     (0)
  #define MC0_CH0_CR_DESWIZZLE_HIGH_ERM_Bit_25_MAX                     (7) // 0x00000007
  #define MC0_CH0_CR_DESWIZZLE_HIGH_ERM_Bit_25_DEF                     (0x00000001)
  #define MC0_CH0_CR_DESWIZZLE_HIGH_ERM_Bit_25_HSH                     (0x4324E498)

  #define MC0_CH0_CR_DESWIZZLE_HIGH_ERM_Bit_26_OFF                     (21)
  #define MC0_CH0_CR_DESWIZZLE_HIGH_ERM_Bit_26_WID                     ( 3)
  #define MC0_CH0_CR_DESWIZZLE_HIGH_ERM_Bit_26_MSK                     (0x00E00000)
  #define MC0_CH0_CR_DESWIZZLE_HIGH_ERM_Bit_26_MIN                     (0)
  #define MC0_CH0_CR_DESWIZZLE_HIGH_ERM_Bit_26_MAX                     (7) // 0x00000007
  #define MC0_CH0_CR_DESWIZZLE_HIGH_ERM_Bit_26_DEF                     (0x00000002)
  #define MC0_CH0_CR_DESWIZZLE_HIGH_ERM_Bit_26_HSH                     (0x432AE498)

  #define MC0_CH0_CR_DESWIZZLE_HIGH_ERM_Bit_27_OFF                     (24)
  #define MC0_CH0_CR_DESWIZZLE_HIGH_ERM_Bit_27_WID                     ( 3)
  #define MC0_CH0_CR_DESWIZZLE_HIGH_ERM_Bit_27_MSK                     (0x07000000)
  #define MC0_CH0_CR_DESWIZZLE_HIGH_ERM_Bit_27_MIN                     (0)
  #define MC0_CH0_CR_DESWIZZLE_HIGH_ERM_Bit_27_MAX                     (7) // 0x00000007
  #define MC0_CH0_CR_DESWIZZLE_HIGH_ERM_Bit_27_DEF                     (0x00000003)
  #define MC0_CH0_CR_DESWIZZLE_HIGH_ERM_Bit_27_HSH                     (0x4330E498)

  #define MC0_CH0_CR_DESWIZZLE_HIGH_ERM_Bit_28_OFF                     (27)
  #define MC0_CH0_CR_DESWIZZLE_HIGH_ERM_Bit_28_WID                     ( 3)
  #define MC0_CH0_CR_DESWIZZLE_HIGH_ERM_Bit_28_MSK                     (0x38000000)
  #define MC0_CH0_CR_DESWIZZLE_HIGH_ERM_Bit_28_MIN                     (0)
  #define MC0_CH0_CR_DESWIZZLE_HIGH_ERM_Bit_28_MAX                     (7) // 0x00000007
  #define MC0_CH0_CR_DESWIZZLE_HIGH_ERM_Bit_28_DEF                     (0x00000004)
  #define MC0_CH0_CR_DESWIZZLE_HIGH_ERM_Bit_28_HSH                     (0x4336E498)

  #define MC0_CH0_CR_DESWIZZLE_HIGH_ERM_Byte_0_OFF                     (32)
  #define MC0_CH0_CR_DESWIZZLE_HIGH_ERM_Byte_0_WID                     ( 3)
  #define MC0_CH0_CR_DESWIZZLE_HIGH_ERM_Byte_0_MSK                     (0x0000000700000000ULL)
  #define MC0_CH0_CR_DESWIZZLE_HIGH_ERM_Byte_0_MIN                     (0)
  #define MC0_CH0_CR_DESWIZZLE_HIGH_ERM_Byte_0_MAX                     (7) // 0x00000007
  #define MC0_CH0_CR_DESWIZZLE_HIGH_ERM_Byte_0_DEF                     (0x00000000)
  #define MC0_CH0_CR_DESWIZZLE_HIGH_ERM_Byte_0_HSH                     (0x4340E498)

  #define MC0_CH0_CR_DESWIZZLE_HIGH_ERM_Byte_1_OFF                     (35)
  #define MC0_CH0_CR_DESWIZZLE_HIGH_ERM_Byte_1_WID                     ( 3)
  #define MC0_CH0_CR_DESWIZZLE_HIGH_ERM_Byte_1_MSK                     (0x0000003800000000ULL)
  #define MC0_CH0_CR_DESWIZZLE_HIGH_ERM_Byte_1_MIN                     (0)
  #define MC0_CH0_CR_DESWIZZLE_HIGH_ERM_Byte_1_MAX                     (7) // 0x00000007
  #define MC0_CH0_CR_DESWIZZLE_HIGH_ERM_Byte_1_DEF                     (0x00000001)
  #define MC0_CH0_CR_DESWIZZLE_HIGH_ERM_Byte_1_HSH                     (0x4346E498)

#define MC0_CH0_CR_PM_ALL_RANKS_CKE_LOW_COUNT_REG                      (0x0000E4B0)
//Duplicate of MC0_PWM_COUNTERS_DURATION_REG

#define MC0_CH0_CR_DESWIZZLE_HIGH_REG                                  (0x0000E4B8)
//Duplicate of MC0_CH0_CR_DESWIZZLE_HIGH_ERM_REG

#define MC0_CH0_CR_TC_SREXITTP_REG                                     (0x0000E4C0)

  #define MC0_CH0_CR_TC_SREXITTP_tXSR_OFF                              ( 0)
  #define MC0_CH0_CR_TC_SREXITTP_tXSR_WID                              (13)
  #define MC0_CH0_CR_TC_SREXITTP_tXSR_MSK                              (0x00001FFF)
  #define MC0_CH0_CR_TC_SREXITTP_tXSR_MIN                              (0)
  #define MC0_CH0_CR_TC_SREXITTP_tXSR_MAX                              (8191) // 0x00001FFF
  #define MC0_CH0_CR_TC_SREXITTP_tXSR_DEF                              (0x00000000)
  #define MC0_CH0_CR_TC_SREXITTP_tXSR_HSH                              (0x4D00E4C0)

  #define MC0_CH0_CR_TC_SREXITTP_tXSR_DS_OFF                           (13)
  #define MC0_CH0_CR_TC_SREXITTP_tXSR_DS_WID                           (15)
  #define MC0_CH0_CR_TC_SREXITTP_tXSR_DS_MSK                           (0x0FFFE000)
  #define MC0_CH0_CR_TC_SREXITTP_tXSR_DS_MIN                           (0)
  #define MC0_CH0_CR_TC_SREXITTP_tXSR_DS_MAX                           (32767) // 0x00007FFF
  #define MC0_CH0_CR_TC_SREXITTP_tXSR_DS_DEF                           (0x00000000)
  #define MC0_CH0_CR_TC_SREXITTP_tXSR_DS_HSH                           (0x4F1AE4C0)

  #define MC0_CH0_CR_TC_SREXITTP_serial_zq_between_sub_ch_OFF          (35)
  #define MC0_CH0_CR_TC_SREXITTP_serial_zq_between_sub_ch_WID          ( 1)
  #define MC0_CH0_CR_TC_SREXITTP_serial_zq_between_sub_ch_MSK          (0x0000000800000000ULL)
  #define MC0_CH0_CR_TC_SREXITTP_serial_zq_between_sub_ch_MIN          (0)
  #define MC0_CH0_CR_TC_SREXITTP_serial_zq_between_sub_ch_MAX          (1) // 0x00000001
  #define MC0_CH0_CR_TC_SREXITTP_serial_zq_between_sub_ch_DEF          (0x00000000)
  #define MC0_CH0_CR_TC_SREXITTP_serial_zq_between_sub_ch_HSH          (0x4146E4C0)

  #define MC0_CH0_CR_TC_SREXITTP_count_block_wr_on_srx_OFF             (36)
  #define MC0_CH0_CR_TC_SREXITTP_count_block_wr_on_srx_WID             (16)
  #define MC0_CH0_CR_TC_SREXITTP_count_block_wr_on_srx_MSK             (0x000FFFF000000000ULL)
  #define MC0_CH0_CR_TC_SREXITTP_count_block_wr_on_srx_MIN             (0)
  #define MC0_CH0_CR_TC_SREXITTP_count_block_wr_on_srx_MAX             (65535) // 0x0000FFFF
  #define MC0_CH0_CR_TC_SREXITTP_count_block_wr_on_srx_DEF             (0x00000014)
  #define MC0_CH0_CR_TC_SREXITTP_count_block_wr_on_srx_HSH             (0x5048E4C0)

  #define MC0_CH0_CR_TC_SREXITTP_tSR_OFF                               (52)
  #define MC0_CH0_CR_TC_SREXITTP_tSR_WID                               ( 6)
  #define MC0_CH0_CR_TC_SREXITTP_tSR_MSK                               (0x03F0000000000000ULL)
  #define MC0_CH0_CR_TC_SREXITTP_tSR_MIN                               (0)
  #define MC0_CH0_CR_TC_SREXITTP_tSR_MAX                               (63) // 0x0000003F
  #define MC0_CH0_CR_TC_SREXITTP_tSR_DEF                               (0x00000020)
  #define MC0_CH0_CR_TC_SREXITTP_tSR_HSH                               (0x4668E4C0)

#define MC0_CH0_CR_DQS_OSCILLATOR_PARAMS_REG                           (0x0000E4C8)

  #define MC0_CH0_CR_DQS_OSCILLATOR_PARAMS_DQSOSCL_PERIOD_OFF          ( 0)
  #define MC0_CH0_CR_DQS_OSCILLATOR_PARAMS_DQSOSCL_PERIOD_WID          (16)
  #define MC0_CH0_CR_DQS_OSCILLATOR_PARAMS_DQSOSCL_PERIOD_MSK          (0x0000FFFF)
  #define MC0_CH0_CR_DQS_OSCILLATOR_PARAMS_DQSOSCL_PERIOD_MIN          (0)
  #define MC0_CH0_CR_DQS_OSCILLATOR_PARAMS_DQSOSCL_PERIOD_MAX          (65535) // 0x0000FFFF
  #define MC0_CH0_CR_DQS_OSCILLATOR_PARAMS_DQSOSCL_PERIOD_DEF          (0x00000100)
  #define MC0_CH0_CR_DQS_OSCILLATOR_PARAMS_DQSOSCL_PERIOD_HSH          (0x1000E4C8)

  #define MC0_CH0_CR_DQS_OSCILLATOR_PARAMS_MR19toBlockAckPeriod_OFF    (16)
  #define MC0_CH0_CR_DQS_OSCILLATOR_PARAMS_MR19toBlockAckPeriod_WID    ( 8)
  #define MC0_CH0_CR_DQS_OSCILLATOR_PARAMS_MR19toBlockAckPeriod_MSK    (0x00FF0000)
  #define MC0_CH0_CR_DQS_OSCILLATOR_PARAMS_MR19toBlockAckPeriod_MIN    (0)
  #define MC0_CH0_CR_DQS_OSCILLATOR_PARAMS_MR19toBlockAckPeriod_MAX    (255) // 0x000000FF
  #define MC0_CH0_CR_DQS_OSCILLATOR_PARAMS_MR19toBlockAckPeriod_DEF    (0x00000007)
  #define MC0_CH0_CR_DQS_OSCILLATOR_PARAMS_MR19toBlockAckPeriod_HSH    (0x0820E4C8)

  #define MC0_CH0_CR_DQS_OSCILLATOR_PARAMS_DIS_SRX_DQSOSCL_OFF         (24)
  #define MC0_CH0_CR_DQS_OSCILLATOR_PARAMS_DIS_SRX_DQSOSCL_WID         ( 1)
  #define MC0_CH0_CR_DQS_OSCILLATOR_PARAMS_DIS_SRX_DQSOSCL_MSK         (0x01000000)
  #define MC0_CH0_CR_DQS_OSCILLATOR_PARAMS_DIS_SRX_DQSOSCL_MIN         (0)
  #define MC0_CH0_CR_DQS_OSCILLATOR_PARAMS_DIS_SRX_DQSOSCL_MAX         (1) // 0x00000001
  #define MC0_CH0_CR_DQS_OSCILLATOR_PARAMS_DIS_SRX_DQSOSCL_DEF         (0x00000000)
  #define MC0_CH0_CR_DQS_OSCILLATOR_PARAMS_DIS_SRX_DQSOSCL_HSH         (0x0130E4C8)

  #define MC0_CH0_CR_DQS_OSCILLATOR_PARAMS_disable_wr_on_pkgc_exit_OFF (26)
  #define MC0_CH0_CR_DQS_OSCILLATOR_PARAMS_disable_wr_on_pkgc_exit_WID ( 1)
  #define MC0_CH0_CR_DQS_OSCILLATOR_PARAMS_disable_wr_on_pkgc_exit_MSK (0x04000000)
  #define MC0_CH0_CR_DQS_OSCILLATOR_PARAMS_disable_wr_on_pkgc_exit_MIN (0)
  #define MC0_CH0_CR_DQS_OSCILLATOR_PARAMS_disable_wr_on_pkgc_exit_MAX (1) // 0x00000001
  #define MC0_CH0_CR_DQS_OSCILLATOR_PARAMS_disable_wr_on_pkgc_exit_DEF (0x00000000)
  #define MC0_CH0_CR_DQS_OSCILLATOR_PARAMS_disable_wr_on_pkgc_exit_HSH (0x0134E4C8)

  #define MC0_CH0_CR_DQS_OSCILLATOR_PARAMS_disable_wr_on_SR_exit_OFF   (27)
  #define MC0_CH0_CR_DQS_OSCILLATOR_PARAMS_disable_wr_on_SR_exit_WID   ( 1)
  #define MC0_CH0_CR_DQS_OSCILLATOR_PARAMS_disable_wr_on_SR_exit_MSK   (0x08000000)
  #define MC0_CH0_CR_DQS_OSCILLATOR_PARAMS_disable_wr_on_SR_exit_MIN   (0)
  #define MC0_CH0_CR_DQS_OSCILLATOR_PARAMS_disable_wr_on_SR_exit_MAX   (1) // 0x00000001
  #define MC0_CH0_CR_DQS_OSCILLATOR_PARAMS_disable_wr_on_SR_exit_DEF   (0x00000000)
  #define MC0_CH0_CR_DQS_OSCILLATOR_PARAMS_disable_wr_on_SR_exit_HSH   (0x0136E4C8)

  #define MC0_CH0_CR_DQS_OSCILLATOR_PARAMS_disable_PMreqWaitForMR19_OFF (28)
  #define MC0_CH0_CR_DQS_OSCILLATOR_PARAMS_disable_PMreqWaitForMR19_WID ( 1)
  #define MC0_CH0_CR_DQS_OSCILLATOR_PARAMS_disable_PMreqWaitForMR19_MSK (0x10000000)
  #define MC0_CH0_CR_DQS_OSCILLATOR_PARAMS_disable_PMreqWaitForMR19_MIN (0)
  #define MC0_CH0_CR_DQS_OSCILLATOR_PARAMS_disable_PMreqWaitForMR19_MAX (1) // 0x00000001
  #define MC0_CH0_CR_DQS_OSCILLATOR_PARAMS_disable_PMreqWaitForMR19_DEF (0x00000000)
  #define MC0_CH0_CR_DQS_OSCILLATOR_PARAMS_disable_PMreqWaitForMR19_HSH (0x0138E4C8)

  #define MC0_CH0_CR_DQS_OSCILLATOR_PARAMS_dqs_oscillator_resolution_is_64_OFF (29)
  #define MC0_CH0_CR_DQS_OSCILLATOR_PARAMS_dqs_oscillator_resolution_is_64_WID ( 1)
  #define MC0_CH0_CR_DQS_OSCILLATOR_PARAMS_dqs_oscillator_resolution_is_64_MSK (0x20000000)
  #define MC0_CH0_CR_DQS_OSCILLATOR_PARAMS_dqs_oscillator_resolution_is_64_MIN (0)
  #define MC0_CH0_CR_DQS_OSCILLATOR_PARAMS_dqs_oscillator_resolution_is_64_MAX (1) // 0x00000001
  #define MC0_CH0_CR_DQS_OSCILLATOR_PARAMS_dqs_oscillator_resolution_is_64_DEF (0x00000000)
  #define MC0_CH0_CR_DQS_OSCILLATOR_PARAMS_dqs_oscillator_resolution_is_64_HSH (0x013AE4C8)

#define MC0_CH0_CR_MRH_GENERIC_COMMAND_REG                             (0x0000E4CC)

  #define MC0_CH0_CR_MRH_GENERIC_COMMAND_ca_bus_OFF                    ( 0)
  #define MC0_CH0_CR_MRH_GENERIC_COMMAND_ca_bus_WID                    (28)
  #define MC0_CH0_CR_MRH_GENERIC_COMMAND_ca_bus_MSK                    (0x0FFFFFFF)
  #define MC0_CH0_CR_MRH_GENERIC_COMMAND_ca_bus_MIN                    (0)
  #define MC0_CH0_CR_MRH_GENERIC_COMMAND_ca_bus_MAX                    (268435455) // 0x0FFFFFFF
  #define MC0_CH0_CR_MRH_GENERIC_COMMAND_ca_bus_DEF                    (0x00000000)
  #define MC0_CH0_CR_MRH_GENERIC_COMMAND_ca_bus_HSH                    (0x1C00E4CC)

  #define MC0_CH0_CR_MRH_GENERIC_COMMAND_two_cyc_command_OFF           (28)
  #define MC0_CH0_CR_MRH_GENERIC_COMMAND_two_cyc_command_WID           ( 1)
  #define MC0_CH0_CR_MRH_GENERIC_COMMAND_two_cyc_command_MSK           (0x10000000)
  #define MC0_CH0_CR_MRH_GENERIC_COMMAND_two_cyc_command_MIN           (0)
  #define MC0_CH0_CR_MRH_GENERIC_COMMAND_two_cyc_command_MAX           (1) // 0x00000001
  #define MC0_CH0_CR_MRH_GENERIC_COMMAND_two_cyc_command_DEF           (0x00000000)
  #define MC0_CH0_CR_MRH_GENERIC_COMMAND_two_cyc_command_HSH           (0x0138E4CC)

  #define MC0_CH0_CR_MRH_GENERIC_COMMAND_signal_mr18_19_OFF            (29)
  #define MC0_CH0_CR_MRH_GENERIC_COMMAND_signal_mr18_19_WID            ( 1)
  #define MC0_CH0_CR_MRH_GENERIC_COMMAND_signal_mr18_19_MSK            (0x20000000)
  #define MC0_CH0_CR_MRH_GENERIC_COMMAND_signal_mr18_19_MIN            (0)
  #define MC0_CH0_CR_MRH_GENERIC_COMMAND_signal_mr18_19_MAX            (1) // 0x00000001
  #define MC0_CH0_CR_MRH_GENERIC_COMMAND_signal_mr18_19_DEF            (0x00000000)
  #define MC0_CH0_CR_MRH_GENERIC_COMMAND_signal_mr18_19_HSH            (0x013AE4CC)

  #define MC0_CH0_CR_MRH_GENERIC_COMMAND_Generic_MRH_override_OFF      (31)
  #define MC0_CH0_CR_MRH_GENERIC_COMMAND_Generic_MRH_override_WID      ( 1)
  #define MC0_CH0_CR_MRH_GENERIC_COMMAND_Generic_MRH_override_MSK      (0x80000000)
  #define MC0_CH0_CR_MRH_GENERIC_COMMAND_Generic_MRH_override_MIN      (0)
  #define MC0_CH0_CR_MRH_GENERIC_COMMAND_Generic_MRH_override_MAX      (1) // 0x00000001
  #define MC0_CH0_CR_MRH_GENERIC_COMMAND_Generic_MRH_override_DEF      (0x00000000)
  #define MC0_CH0_CR_MRH_GENERIC_COMMAND_Generic_MRH_override_HSH      (0x013EE4CC)

#define MC0_CH0_CR_WDB_RD_WR_DFX_DATA_REG                              (0x0000E4D8)

  #define MC0_CH0_CR_WDB_RD_WR_DFX_DATA_DATA_OFF                       ( 0)
  #define MC0_CH0_CR_WDB_RD_WR_DFX_DATA_DATA_WID                       (64)
  #define MC0_CH0_CR_WDB_RD_WR_DFX_DATA_DATA_MSK                       (0xFFFFFFFFFFFFFFFFULL)
  #define MC0_CH0_CR_WDB_RD_WR_DFX_DATA_DATA_MIN                       (0)
  #define MC0_CH0_CR_WDB_RD_WR_DFX_DATA_DATA_MAX                       (18446744073709551615ULL) // 0xFFFFFFFFFFFFFFFF
  #define MC0_CH0_CR_WDB_RD_WR_DFX_DATA_DATA_DEF                       (0x00000000)
  #define MC0_CH0_CR_WDB_RD_WR_DFX_DATA_DATA_HSH                       (0x4000E4D8)

#define MC0_CH0_CR_WDB_RD_WR_DFX_CTL_REG                               (0x0000E4E0)

  #define MC0_CH0_CR_WDB_RD_WR_DFX_CTL_GO_OFF                          ( 0)
  #define MC0_CH0_CR_WDB_RD_WR_DFX_CTL_GO_WID                          ( 1)
  #define MC0_CH0_CR_WDB_RD_WR_DFX_CTL_GO_MSK                          (0x00000001)
  #define MC0_CH0_CR_WDB_RD_WR_DFX_CTL_GO_MIN                          (0)
  #define MC0_CH0_CR_WDB_RD_WR_DFX_CTL_GO_MAX                          (1) // 0x00000001
  #define MC0_CH0_CR_WDB_RD_WR_DFX_CTL_GO_DEF                          (0x00000000)
  #define MC0_CH0_CR_WDB_RD_WR_DFX_CTL_GO_HSH                          (0x0100E4E0)

  #define MC0_CH0_CR_WDB_RD_WR_DFX_CTL_Chunk_Sel_OFF                   ( 1)
  #define MC0_CH0_CR_WDB_RD_WR_DFX_CTL_Chunk_Sel_WID                   ( 8)
  #define MC0_CH0_CR_WDB_RD_WR_DFX_CTL_Chunk_Sel_MSK                   (0x000001FE)
  #define MC0_CH0_CR_WDB_RD_WR_DFX_CTL_Chunk_Sel_MIN                   (0)
  #define MC0_CH0_CR_WDB_RD_WR_DFX_CTL_Chunk_Sel_MAX                   (255) // 0x000000FF
  #define MC0_CH0_CR_WDB_RD_WR_DFX_CTL_Chunk_Sel_DEF                   (0x00000000)
  #define MC0_CH0_CR_WDB_RD_WR_DFX_CTL_Chunk_Sel_HSH                   (0x0802E4E0)

  #define MC0_CH0_CR_WDB_RD_WR_DFX_CTL_Op_OFF                          ( 9)
  #define MC0_CH0_CR_WDB_RD_WR_DFX_CTL_Op_WID                          ( 1)
  #define MC0_CH0_CR_WDB_RD_WR_DFX_CTL_Op_MSK                          (0x00000200)
  #define MC0_CH0_CR_WDB_RD_WR_DFX_CTL_Op_MIN                          (0)
  #define MC0_CH0_CR_WDB_RD_WR_DFX_CTL_Op_MAX                          (1) // 0x00000001
  #define MC0_CH0_CR_WDB_RD_WR_DFX_CTL_Op_DEF                          (0x00000000)
  #define MC0_CH0_CR_WDB_RD_WR_DFX_CTL_Op_HSH                          (0x0112E4E0)

  #define MC0_CH0_CR_WDB_RD_WR_DFX_CTL_SubCh_OFF                       (10)
  #define MC0_CH0_CR_WDB_RD_WR_DFX_CTL_SubCh_WID                       ( 1)
  #define MC0_CH0_CR_WDB_RD_WR_DFX_CTL_SubCh_MSK                       (0x00000400)
  #define MC0_CH0_CR_WDB_RD_WR_DFX_CTL_SubCh_MIN                       (0)
  #define MC0_CH0_CR_WDB_RD_WR_DFX_CTL_SubCh_MAX                       (1) // 0x00000001
  #define MC0_CH0_CR_WDB_RD_WR_DFX_CTL_SubCh_DEF                       (0x00000000)
  #define MC0_CH0_CR_WDB_RD_WR_DFX_CTL_SubCh_HSH                       (0x0114E4E0)

  #define MC0_CH0_CR_WDB_RD_WR_DFX_CTL_WID_OFF                         (11)
  #define MC0_CH0_CR_WDB_RD_WR_DFX_CTL_WID_WID                         ( 6)
  #define MC0_CH0_CR_WDB_RD_WR_DFX_CTL_WID_MSK                         (0x0001F800)
  #define MC0_CH0_CR_WDB_RD_WR_DFX_CTL_WID_MIN                         (0)
  #define MC0_CH0_CR_WDB_RD_WR_DFX_CTL_WID_MAX                         (63) // 0x0000003F
  #define MC0_CH0_CR_WDB_RD_WR_DFX_CTL_WID_DEF                         (0x00000000)
  #define MC0_CH0_CR_WDB_RD_WR_DFX_CTL_WID_HSH                         (0x0616E4E0)

#define MC0_CH0_CR_REF_FSM_STATUS_REG                                  (0x0000E4E4)

  #define MC0_CH0_CR_REF_FSM_STATUS_DQS_INTERVAL_FSM_status_OFF        ( 0)
  #define MC0_CH0_CR_REF_FSM_STATUS_DQS_INTERVAL_FSM_status_WID        ( 4)
  #define MC0_CH0_CR_REF_FSM_STATUS_DQS_INTERVAL_FSM_status_MSK        (0x0000000F)
  #define MC0_CH0_CR_REF_FSM_STATUS_DQS_INTERVAL_FSM_status_MIN        (0)
  #define MC0_CH0_CR_REF_FSM_STATUS_DQS_INTERVAL_FSM_status_MAX        (15) // 0x0000000F
  #define MC0_CH0_CR_REF_FSM_STATUS_DQS_INTERVAL_FSM_status_DEF        (0x00000000)
  #define MC0_CH0_CR_REF_FSM_STATUS_DQS_INTERVAL_FSM_status_HSH        (0x0400E4E4)

  #define MC0_CH0_CR_REF_FSM_STATUS_TEMP_RD_FSM_status_OFF             ( 4)
  #define MC0_CH0_CR_REF_FSM_STATUS_TEMP_RD_FSM_status_WID             ( 5)
  #define MC0_CH0_CR_REF_FSM_STATUS_TEMP_RD_FSM_status_MSK             (0x000001F0)
  #define MC0_CH0_CR_REF_FSM_STATUS_TEMP_RD_FSM_status_MIN             (0)
  #define MC0_CH0_CR_REF_FSM_STATUS_TEMP_RD_FSM_status_MAX             (31) // 0x0000001F
  #define MC0_CH0_CR_REF_FSM_STATUS_TEMP_RD_FSM_status_DEF             (0x00000000)
  #define MC0_CH0_CR_REF_FSM_STATUS_TEMP_RD_FSM_status_HSH             (0x0508E4E4)

  #define MC0_CH0_CR_REF_FSM_STATUS_REFRATE_CHNG_FSM_status_OFF        ( 9)
  #define MC0_CH0_CR_REF_FSM_STATUS_REFRATE_CHNG_FSM_status_WID        ( 2)
  #define MC0_CH0_CR_REF_FSM_STATUS_REFRATE_CHNG_FSM_status_MSK        (0x00000600)
  #define MC0_CH0_CR_REF_FSM_STATUS_REFRATE_CHNG_FSM_status_MIN        (0)
  #define MC0_CH0_CR_REF_FSM_STATUS_REFRATE_CHNG_FSM_status_MAX        (3) // 0x00000003
  #define MC0_CH0_CR_REF_FSM_STATUS_REFRATE_CHNG_FSM_status_DEF        (0x00000000)
  #define MC0_CH0_CR_REF_FSM_STATUS_REFRATE_CHNG_FSM_status_HSH        (0x0212E4E4)

  #define MC0_CH0_CR_REF_FSM_STATUS_ZQCS_FSM_status_OFF                (11)
  #define MC0_CH0_CR_REF_FSM_STATUS_ZQCS_FSM_status_WID                ( 3)
  #define MC0_CH0_CR_REF_FSM_STATUS_ZQCS_FSM_status_MSK                (0x00003800)
  #define MC0_CH0_CR_REF_FSM_STATUS_ZQCS_FSM_status_MIN                (0)
  #define MC0_CH0_CR_REF_FSM_STATUS_ZQCS_FSM_status_MAX                (7) // 0x00000007
  #define MC0_CH0_CR_REF_FSM_STATUS_ZQCS_FSM_status_DEF                (0x00000000)
  #define MC0_CH0_CR_REF_FSM_STATUS_ZQCS_FSM_status_HSH                (0x0316E4E4)

  #define MC0_CH0_CR_REF_FSM_STATUS_EXE_REF_FSM_status_OFF             (14)
  #define MC0_CH0_CR_REF_FSM_STATUS_EXE_REF_FSM_status_WID             ( 3)
  #define MC0_CH0_CR_REF_FSM_STATUS_EXE_REF_FSM_status_MSK             (0x0001C000)
  #define MC0_CH0_CR_REF_FSM_STATUS_EXE_REF_FSM_status_MIN             (0)
  #define MC0_CH0_CR_REF_FSM_STATUS_EXE_REF_FSM_status_MAX             (7) // 0x00000007
  #define MC0_CH0_CR_REF_FSM_STATUS_EXE_REF_FSM_status_DEF             (0x00000000)
  #define MC0_CH0_CR_REF_FSM_STATUS_EXE_REF_FSM_status_HSH             (0x031CE4E4)

  #define MC0_CH0_CR_REF_FSM_STATUS_MAIN_REF_FSM_status_OFF            (17)
  #define MC0_CH0_CR_REF_FSM_STATUS_MAIN_REF_FSM_status_WID            ( 3)
  #define MC0_CH0_CR_REF_FSM_STATUS_MAIN_REF_FSM_status_MSK            (0x000E0000)
  #define MC0_CH0_CR_REF_FSM_STATUS_MAIN_REF_FSM_status_MIN            (0)
  #define MC0_CH0_CR_REF_FSM_STATUS_MAIN_REF_FSM_status_MAX            (7) // 0x00000007
  #define MC0_CH0_CR_REF_FSM_STATUS_MAIN_REF_FSM_status_DEF            (0x00000000)
  #define MC0_CH0_CR_REF_FSM_STATUS_MAIN_REF_FSM_status_HSH            (0x0322E4E4)

  #define MC0_CH0_CR_REF_FSM_STATUS_MAIN_SR_FSM_status_OFF             (20)
  #define MC0_CH0_CR_REF_FSM_STATUS_MAIN_SR_FSM_status_WID             ( 4)
  #define MC0_CH0_CR_REF_FSM_STATUS_MAIN_SR_FSM_status_MSK             (0x00F00000)
  #define MC0_CH0_CR_REF_FSM_STATUS_MAIN_SR_FSM_status_MIN             (0)
  #define MC0_CH0_CR_REF_FSM_STATUS_MAIN_SR_FSM_status_MAX             (15) // 0x0000000F
  #define MC0_CH0_CR_REF_FSM_STATUS_MAIN_SR_FSM_status_DEF             (0x00000000)
  #define MC0_CH0_CR_REF_FSM_STATUS_MAIN_SR_FSM_status_HSH             (0x0428E4E4)

  #define MC0_CH0_CR_REF_FSM_STATUS_counter_status_OFF                 (24)
  #define MC0_CH0_CR_REF_FSM_STATUS_counter_status_WID                 ( 8)
  #define MC0_CH0_CR_REF_FSM_STATUS_counter_status_MSK                 (0xFF000000)
  #define MC0_CH0_CR_REF_FSM_STATUS_counter_status_MIN                 (0)
  #define MC0_CH0_CR_REF_FSM_STATUS_counter_status_MAX                 (255) // 0x000000FF
  #define MC0_CH0_CR_REF_FSM_STATUS_counter_status_DEF                 (0x00000000)
  #define MC0_CH0_CR_REF_FSM_STATUS_counter_status_HSH                 (0x0830E4E4)

#define MC0_CH0_CR_WDB_MBIST_0_REG                                     (0x0000E4E8)

  #define MC0_CH0_CR_WDB_MBIST_0_PASS_OFF                              ( 0)
  #define MC0_CH0_CR_WDB_MBIST_0_PASS_WID                              ( 1)
  #define MC0_CH0_CR_WDB_MBIST_0_PASS_MSK                              (0x00000001)
  #define MC0_CH0_CR_WDB_MBIST_0_PASS_MIN                              (0)
  #define MC0_CH0_CR_WDB_MBIST_0_PASS_MAX                              (1) // 0x00000001
  #define MC0_CH0_CR_WDB_MBIST_0_PASS_DEF                              (0x00000000)
  #define MC0_CH0_CR_WDB_MBIST_0_PASS_HSH                              (0x0100E4E8)

  #define MC0_CH0_CR_WDB_MBIST_0_Complete_OFF                          ( 1)
  #define MC0_CH0_CR_WDB_MBIST_0_Complete_WID                          ( 1)
  #define MC0_CH0_CR_WDB_MBIST_0_Complete_MSK                          (0x00000002)
  #define MC0_CH0_CR_WDB_MBIST_0_Complete_MIN                          (0)
  #define MC0_CH0_CR_WDB_MBIST_0_Complete_MAX                          (1) // 0x00000001
  #define MC0_CH0_CR_WDB_MBIST_0_Complete_DEF                          (0x00000000)
  #define MC0_CH0_CR_WDB_MBIST_0_Complete_HSH                          (0x0102E4E8)

  #define MC0_CH0_CR_WDB_MBIST_0_Inject_Failure_OFF                    ( 8)
  #define MC0_CH0_CR_WDB_MBIST_0_Inject_Failure_WID                    ( 1)
  #define MC0_CH0_CR_WDB_MBIST_0_Inject_Failure_MSK                    (0x00000100)
  #define MC0_CH0_CR_WDB_MBIST_0_Inject_Failure_MIN                    (0)
  #define MC0_CH0_CR_WDB_MBIST_0_Inject_Failure_MAX                    (1) // 0x00000001
  #define MC0_CH0_CR_WDB_MBIST_0_Inject_Failure_DEF                    (0x00000000)
  #define MC0_CH0_CR_WDB_MBIST_0_Inject_Failure_HSH                    (0x0110E4E8)

  #define MC0_CH0_CR_WDB_MBIST_0_RUN_BUSY_OFF                          (31)
  #define MC0_CH0_CR_WDB_MBIST_0_RUN_BUSY_WID                          ( 1)
  #define MC0_CH0_CR_WDB_MBIST_0_RUN_BUSY_MSK                          (0x80000000)
  #define MC0_CH0_CR_WDB_MBIST_0_RUN_BUSY_MIN                          (0)
  #define MC0_CH0_CR_WDB_MBIST_0_RUN_BUSY_MAX                          (1) // 0x00000001
  #define MC0_CH0_CR_WDB_MBIST_0_RUN_BUSY_DEF                          (0x00000000)
  #define MC0_CH0_CR_WDB_MBIST_0_RUN_BUSY_HSH                          (0x013EE4E8)

#define MC0_CH0_CR_WDB_MBIST_1_REG                                     (0x0000E4EC)
//Duplicate of MC0_CH0_CR_WDB_MBIST_0_REG

#define MC0_CH0_CR_RDB_MBIST_REG                                       (0x0000E4F8)
//Duplicate of MC0_CH0_CR_WDB_MBIST_0_REG

#define MC0_CH0_CR_ECC_INJECT_COUNT_REG                                (0x0000E4FC)

  #define MC0_CH0_CR_ECC_INJECT_COUNT_Count_OFF                        ( 0)
  #define MC0_CH0_CR_ECC_INJECT_COUNT_Count_WID                        (32)
  #define MC0_CH0_CR_ECC_INJECT_COUNT_Count_MSK                        (0xFFFFFFFF)
  #define MC0_CH0_CR_ECC_INJECT_COUNT_Count_MIN                        (0)
  #define MC0_CH0_CR_ECC_INJECT_COUNT_Count_MAX                        (4294967295) // 0xFFFFFFFF
  #define MC0_CH0_CR_ECC_INJECT_COUNT_Count_DEF                        (0xFFFFFFFF)
  #define MC0_CH0_CR_ECC_INJECT_COUNT_Count_HSH                        (0x2000E4FC)

#define MC0_CH0_CR_PWM_DDR_SUBCH0_RDDATA_COUNTER_REG                   (0x0000E500)

  #define MC0_CH0_CR_PWM_DDR_SUBCH0_RDDATA_COUNTER_RDDATA_COUNTER_OFF  ( 0)
  #define MC0_CH0_CR_PWM_DDR_SUBCH0_RDDATA_COUNTER_RDDATA_COUNTER_WID  (64)
  #define MC0_CH0_CR_PWM_DDR_SUBCH0_RDDATA_COUNTER_RDDATA_COUNTER_MSK  (0xFFFFFFFFFFFFFFFFULL)
  #define MC0_CH0_CR_PWM_DDR_SUBCH0_RDDATA_COUNTER_RDDATA_COUNTER_MIN  (0)
  #define MC0_CH0_CR_PWM_DDR_SUBCH0_RDDATA_COUNTER_RDDATA_COUNTER_MAX  (18446744073709551615ULL) // 0xFFFFFFFFFFFFFFFF
  #define MC0_CH0_CR_PWM_DDR_SUBCH0_RDDATA_COUNTER_RDDATA_COUNTER_DEF  (0x00000000)
  #define MC0_CH0_CR_PWM_DDR_SUBCH0_RDDATA_COUNTER_RDDATA_COUNTER_HSH  (0x4000E500)

#define MC0_CH0_CR_PWM_DDR_SUBCH1_RDDATA_COUNTER_REG                   (0x0000E508)
//Duplicate of MC0_CH0_CR_PWM_DDR_SUBCH0_RDDATA_COUNTER_REG

#define MC0_CH0_CR_PWM_DDR_SUBCH0_WRDATA_COUNTER_REG                   (0x0000E510)

  #define MC0_CH0_CR_PWM_DDR_SUBCH0_WRDATA_COUNTER_WRDATA_COUNTER_OFF  ( 0)
  #define MC0_CH0_CR_PWM_DDR_SUBCH0_WRDATA_COUNTER_WRDATA_COUNTER_WID  (64)
  #define MC0_CH0_CR_PWM_DDR_SUBCH0_WRDATA_COUNTER_WRDATA_COUNTER_MSK  (0xFFFFFFFFFFFFFFFFULL)
  #define MC0_CH0_CR_PWM_DDR_SUBCH0_WRDATA_COUNTER_WRDATA_COUNTER_MIN  (0)
  #define MC0_CH0_CR_PWM_DDR_SUBCH0_WRDATA_COUNTER_WRDATA_COUNTER_MAX  (18446744073709551615ULL) // 0xFFFFFFFFFFFFFFFF
  #define MC0_CH0_CR_PWM_DDR_SUBCH0_WRDATA_COUNTER_WRDATA_COUNTER_DEF  (0x00000000)
  #define MC0_CH0_CR_PWM_DDR_SUBCH0_WRDATA_COUNTER_WRDATA_COUNTER_HSH  (0x4000E510)

#define MC0_CH0_CR_PWM_DDR_SUBCH1_WRDATA_COUNTER_REG                   (0x0000E518)
//Duplicate of MC0_CH0_CR_PWM_DDR_SUBCH0_WRDATA_COUNTER_REG

#define MC0_CH0_CR_DDR4_MR1_RTT_NOM_DIMM1_VALUES_REG                   (0x0000E520)

  #define MC0_CH0_CR_DDR4_MR1_RTT_NOM_DIMM1_VALUES_Rank0_Byte0_OFF     ( 0)
  #define MC0_CH0_CR_DDR4_MR1_RTT_NOM_DIMM1_VALUES_Rank0_Byte0_WID     ( 3)
  #define MC0_CH0_CR_DDR4_MR1_RTT_NOM_DIMM1_VALUES_Rank0_Byte0_MSK     (0x00000007)
  #define MC0_CH0_CR_DDR4_MR1_RTT_NOM_DIMM1_VALUES_Rank0_Byte0_MIN     (0)
  #define MC0_CH0_CR_DDR4_MR1_RTT_NOM_DIMM1_VALUES_Rank0_Byte0_MAX     (7) // 0x00000007
  #define MC0_CH0_CR_DDR4_MR1_RTT_NOM_DIMM1_VALUES_Rank0_Byte0_DEF     (0x00000000)
  #define MC0_CH0_CR_DDR4_MR1_RTT_NOM_DIMM1_VALUES_Rank0_Byte0_HSH     (0x4300E520)

  #define MC0_CH0_CR_DDR4_MR1_RTT_NOM_DIMM1_VALUES_Rank0_Byte1_OFF     ( 4)
  #define MC0_CH0_CR_DDR4_MR1_RTT_NOM_DIMM1_VALUES_Rank0_Byte1_WID     ( 3)
  #define MC0_CH0_CR_DDR4_MR1_RTT_NOM_DIMM1_VALUES_Rank0_Byte1_MSK     (0x00000070)
  #define MC0_CH0_CR_DDR4_MR1_RTT_NOM_DIMM1_VALUES_Rank0_Byte1_MIN     (0)
  #define MC0_CH0_CR_DDR4_MR1_RTT_NOM_DIMM1_VALUES_Rank0_Byte1_MAX     (7) // 0x00000007
  #define MC0_CH0_CR_DDR4_MR1_RTT_NOM_DIMM1_VALUES_Rank0_Byte1_DEF     (0x00000000)
  #define MC0_CH0_CR_DDR4_MR1_RTT_NOM_DIMM1_VALUES_Rank0_Byte1_HSH     (0x4308E520)

  #define MC0_CH0_CR_DDR4_MR1_RTT_NOM_DIMM1_VALUES_Rank0_Byte2_OFF     ( 8)
  #define MC0_CH0_CR_DDR4_MR1_RTT_NOM_DIMM1_VALUES_Rank0_Byte2_WID     ( 3)
  #define MC0_CH0_CR_DDR4_MR1_RTT_NOM_DIMM1_VALUES_Rank0_Byte2_MSK     (0x00000700)
  #define MC0_CH0_CR_DDR4_MR1_RTT_NOM_DIMM1_VALUES_Rank0_Byte2_MIN     (0)
  #define MC0_CH0_CR_DDR4_MR1_RTT_NOM_DIMM1_VALUES_Rank0_Byte2_MAX     (7) // 0x00000007
  #define MC0_CH0_CR_DDR4_MR1_RTT_NOM_DIMM1_VALUES_Rank0_Byte2_DEF     (0x00000000)
  #define MC0_CH0_CR_DDR4_MR1_RTT_NOM_DIMM1_VALUES_Rank0_Byte2_HSH     (0x4310E520)

  #define MC0_CH0_CR_DDR4_MR1_RTT_NOM_DIMM1_VALUES_Rank0_Byte3_OFF     (12)
  #define MC0_CH0_CR_DDR4_MR1_RTT_NOM_DIMM1_VALUES_Rank0_Byte3_WID     ( 3)
  #define MC0_CH0_CR_DDR4_MR1_RTT_NOM_DIMM1_VALUES_Rank0_Byte3_MSK     (0x00007000)
  #define MC0_CH0_CR_DDR4_MR1_RTT_NOM_DIMM1_VALUES_Rank0_Byte3_MIN     (0)
  #define MC0_CH0_CR_DDR4_MR1_RTT_NOM_DIMM1_VALUES_Rank0_Byte3_MAX     (7) // 0x00000007
  #define MC0_CH0_CR_DDR4_MR1_RTT_NOM_DIMM1_VALUES_Rank0_Byte3_DEF     (0x00000000)
  #define MC0_CH0_CR_DDR4_MR1_RTT_NOM_DIMM1_VALUES_Rank0_Byte3_HSH     (0x4318E520)

  #define MC0_CH0_CR_DDR4_MR1_RTT_NOM_DIMM1_VALUES_Rank0_Byte4_OFF     (16)
  #define MC0_CH0_CR_DDR4_MR1_RTT_NOM_DIMM1_VALUES_Rank0_Byte4_WID     ( 3)
  #define MC0_CH0_CR_DDR4_MR1_RTT_NOM_DIMM1_VALUES_Rank0_Byte4_MSK     (0x00070000)
  #define MC0_CH0_CR_DDR4_MR1_RTT_NOM_DIMM1_VALUES_Rank0_Byte4_MIN     (0)
  #define MC0_CH0_CR_DDR4_MR1_RTT_NOM_DIMM1_VALUES_Rank0_Byte4_MAX     (7) // 0x00000007
  #define MC0_CH0_CR_DDR4_MR1_RTT_NOM_DIMM1_VALUES_Rank0_Byte4_DEF     (0x00000000)
  #define MC0_CH0_CR_DDR4_MR1_RTT_NOM_DIMM1_VALUES_Rank0_Byte4_HSH     (0x4320E520)

  #define MC0_CH0_CR_DDR4_MR1_RTT_NOM_DIMM1_VALUES_Rank0_Byte5_OFF     (20)
  #define MC0_CH0_CR_DDR4_MR1_RTT_NOM_DIMM1_VALUES_Rank0_Byte5_WID     ( 3)
  #define MC0_CH0_CR_DDR4_MR1_RTT_NOM_DIMM1_VALUES_Rank0_Byte5_MSK     (0x00700000)
  #define MC0_CH0_CR_DDR4_MR1_RTT_NOM_DIMM1_VALUES_Rank0_Byte5_MIN     (0)
  #define MC0_CH0_CR_DDR4_MR1_RTT_NOM_DIMM1_VALUES_Rank0_Byte5_MAX     (7) // 0x00000007
  #define MC0_CH0_CR_DDR4_MR1_RTT_NOM_DIMM1_VALUES_Rank0_Byte5_DEF     (0x00000000)
  #define MC0_CH0_CR_DDR4_MR1_RTT_NOM_DIMM1_VALUES_Rank0_Byte5_HSH     (0x4328E520)

  #define MC0_CH0_CR_DDR4_MR1_RTT_NOM_DIMM1_VALUES_Rank0_Byte6_OFF     (24)
  #define MC0_CH0_CR_DDR4_MR1_RTT_NOM_DIMM1_VALUES_Rank0_Byte6_WID     ( 3)
  #define MC0_CH0_CR_DDR4_MR1_RTT_NOM_DIMM1_VALUES_Rank0_Byte6_MSK     (0x07000000)
  #define MC0_CH0_CR_DDR4_MR1_RTT_NOM_DIMM1_VALUES_Rank0_Byte6_MIN     (0)
  #define MC0_CH0_CR_DDR4_MR1_RTT_NOM_DIMM1_VALUES_Rank0_Byte6_MAX     (7) // 0x00000007
  #define MC0_CH0_CR_DDR4_MR1_RTT_NOM_DIMM1_VALUES_Rank0_Byte6_DEF     (0x00000000)
  #define MC0_CH0_CR_DDR4_MR1_RTT_NOM_DIMM1_VALUES_Rank0_Byte6_HSH     (0x4330E520)

  #define MC0_CH0_CR_DDR4_MR1_RTT_NOM_DIMM1_VALUES_Rank0_Byte7_OFF     (28)
  #define MC0_CH0_CR_DDR4_MR1_RTT_NOM_DIMM1_VALUES_Rank0_Byte7_WID     ( 3)
  #define MC0_CH0_CR_DDR4_MR1_RTT_NOM_DIMM1_VALUES_Rank0_Byte7_MSK     (0x70000000)
  #define MC0_CH0_CR_DDR4_MR1_RTT_NOM_DIMM1_VALUES_Rank0_Byte7_MIN     (0)
  #define MC0_CH0_CR_DDR4_MR1_RTT_NOM_DIMM1_VALUES_Rank0_Byte7_MAX     (7) // 0x00000007
  #define MC0_CH0_CR_DDR4_MR1_RTT_NOM_DIMM1_VALUES_Rank0_Byte7_DEF     (0x00000000)
  #define MC0_CH0_CR_DDR4_MR1_RTT_NOM_DIMM1_VALUES_Rank0_Byte7_HSH     (0x4338E520)

  #define MC0_CH0_CR_DDR4_MR1_RTT_NOM_DIMM1_VALUES_Rank1_Byte0_OFF     (32)
  #define MC0_CH0_CR_DDR4_MR1_RTT_NOM_DIMM1_VALUES_Rank1_Byte0_WID     ( 3)
  #define MC0_CH0_CR_DDR4_MR1_RTT_NOM_DIMM1_VALUES_Rank1_Byte0_MSK     (0x0000000700000000ULL)
  #define MC0_CH0_CR_DDR4_MR1_RTT_NOM_DIMM1_VALUES_Rank1_Byte0_MIN     (0)
  #define MC0_CH0_CR_DDR4_MR1_RTT_NOM_DIMM1_VALUES_Rank1_Byte0_MAX     (7) // 0x00000007
  #define MC0_CH0_CR_DDR4_MR1_RTT_NOM_DIMM1_VALUES_Rank1_Byte0_DEF     (0x00000000)
  #define MC0_CH0_CR_DDR4_MR1_RTT_NOM_DIMM1_VALUES_Rank1_Byte0_HSH     (0x4340E520)

  #define MC0_CH0_CR_DDR4_MR1_RTT_NOM_DIMM1_VALUES_Rank1_Byte1_OFF     (36)
  #define MC0_CH0_CR_DDR4_MR1_RTT_NOM_DIMM1_VALUES_Rank1_Byte1_WID     ( 3)
  #define MC0_CH0_CR_DDR4_MR1_RTT_NOM_DIMM1_VALUES_Rank1_Byte1_MSK     (0x0000007000000000ULL)
  #define MC0_CH0_CR_DDR4_MR1_RTT_NOM_DIMM1_VALUES_Rank1_Byte1_MIN     (0)
  #define MC0_CH0_CR_DDR4_MR1_RTT_NOM_DIMM1_VALUES_Rank1_Byte1_MAX     (7) // 0x00000007
  #define MC0_CH0_CR_DDR4_MR1_RTT_NOM_DIMM1_VALUES_Rank1_Byte1_DEF     (0x00000000)
  #define MC0_CH0_CR_DDR4_MR1_RTT_NOM_DIMM1_VALUES_Rank1_Byte1_HSH     (0x4348E520)

  #define MC0_CH0_CR_DDR4_MR1_RTT_NOM_DIMM1_VALUES_Rank1_Byte2_OFF     (40)
  #define MC0_CH0_CR_DDR4_MR1_RTT_NOM_DIMM1_VALUES_Rank1_Byte2_WID     ( 3)
  #define MC0_CH0_CR_DDR4_MR1_RTT_NOM_DIMM1_VALUES_Rank1_Byte2_MSK     (0x0000070000000000ULL)
  #define MC0_CH0_CR_DDR4_MR1_RTT_NOM_DIMM1_VALUES_Rank1_Byte2_MIN     (0)
  #define MC0_CH0_CR_DDR4_MR1_RTT_NOM_DIMM1_VALUES_Rank1_Byte2_MAX     (7) // 0x00000007
  #define MC0_CH0_CR_DDR4_MR1_RTT_NOM_DIMM1_VALUES_Rank1_Byte2_DEF     (0x00000000)
  #define MC0_CH0_CR_DDR4_MR1_RTT_NOM_DIMM1_VALUES_Rank1_Byte2_HSH     (0x4350E520)

  #define MC0_CH0_CR_DDR4_MR1_RTT_NOM_DIMM1_VALUES_Rank1_Byte3_OFF     (44)
  #define MC0_CH0_CR_DDR4_MR1_RTT_NOM_DIMM1_VALUES_Rank1_Byte3_WID     ( 3)
  #define MC0_CH0_CR_DDR4_MR1_RTT_NOM_DIMM1_VALUES_Rank1_Byte3_MSK     (0x0000700000000000ULL)
  #define MC0_CH0_CR_DDR4_MR1_RTT_NOM_DIMM1_VALUES_Rank1_Byte3_MIN     (0)
  #define MC0_CH0_CR_DDR4_MR1_RTT_NOM_DIMM1_VALUES_Rank1_Byte3_MAX     (7) // 0x00000007
  #define MC0_CH0_CR_DDR4_MR1_RTT_NOM_DIMM1_VALUES_Rank1_Byte3_DEF     (0x00000000)
  #define MC0_CH0_CR_DDR4_MR1_RTT_NOM_DIMM1_VALUES_Rank1_Byte3_HSH     (0x4358E520)

  #define MC0_CH0_CR_DDR4_MR1_RTT_NOM_DIMM1_VALUES_Rank1_Byte4_OFF     (48)
  #define MC0_CH0_CR_DDR4_MR1_RTT_NOM_DIMM1_VALUES_Rank1_Byte4_WID     ( 3)
  #define MC0_CH0_CR_DDR4_MR1_RTT_NOM_DIMM1_VALUES_Rank1_Byte4_MSK     (0x0007000000000000ULL)
  #define MC0_CH0_CR_DDR4_MR1_RTT_NOM_DIMM1_VALUES_Rank1_Byte4_MIN     (0)
  #define MC0_CH0_CR_DDR4_MR1_RTT_NOM_DIMM1_VALUES_Rank1_Byte4_MAX     (7) // 0x00000007
  #define MC0_CH0_CR_DDR4_MR1_RTT_NOM_DIMM1_VALUES_Rank1_Byte4_DEF     (0x00000000)
  #define MC0_CH0_CR_DDR4_MR1_RTT_NOM_DIMM1_VALUES_Rank1_Byte4_HSH     (0x4360E520)

  #define MC0_CH0_CR_DDR4_MR1_RTT_NOM_DIMM1_VALUES_Rank1_Byte5_OFF     (52)
  #define MC0_CH0_CR_DDR4_MR1_RTT_NOM_DIMM1_VALUES_Rank1_Byte5_WID     ( 3)
  #define MC0_CH0_CR_DDR4_MR1_RTT_NOM_DIMM1_VALUES_Rank1_Byte5_MSK     (0x0070000000000000ULL)
  #define MC0_CH0_CR_DDR4_MR1_RTT_NOM_DIMM1_VALUES_Rank1_Byte5_MIN     (0)
  #define MC0_CH0_CR_DDR4_MR1_RTT_NOM_DIMM1_VALUES_Rank1_Byte5_MAX     (7) // 0x00000007
  #define MC0_CH0_CR_DDR4_MR1_RTT_NOM_DIMM1_VALUES_Rank1_Byte5_DEF     (0x00000000)
  #define MC0_CH0_CR_DDR4_MR1_RTT_NOM_DIMM1_VALUES_Rank1_Byte5_HSH     (0x4368E520)

  #define MC0_CH0_CR_DDR4_MR1_RTT_NOM_DIMM1_VALUES_Rank1_Byte6_OFF     (56)
  #define MC0_CH0_CR_DDR4_MR1_RTT_NOM_DIMM1_VALUES_Rank1_Byte6_WID     ( 3)
  #define MC0_CH0_CR_DDR4_MR1_RTT_NOM_DIMM1_VALUES_Rank1_Byte6_MSK     (0x0700000000000000ULL)
  #define MC0_CH0_CR_DDR4_MR1_RTT_NOM_DIMM1_VALUES_Rank1_Byte6_MIN     (0)
  #define MC0_CH0_CR_DDR4_MR1_RTT_NOM_DIMM1_VALUES_Rank1_Byte6_MAX     (7) // 0x00000007
  #define MC0_CH0_CR_DDR4_MR1_RTT_NOM_DIMM1_VALUES_Rank1_Byte6_DEF     (0x00000000)
  #define MC0_CH0_CR_DDR4_MR1_RTT_NOM_DIMM1_VALUES_Rank1_Byte6_HSH     (0x4370E520)

  #define MC0_CH0_CR_DDR4_MR1_RTT_NOM_DIMM1_VALUES_Rank1_Byte7_OFF     (60)
  #define MC0_CH0_CR_DDR4_MR1_RTT_NOM_DIMM1_VALUES_Rank1_Byte7_WID     ( 3)
  #define MC0_CH0_CR_DDR4_MR1_RTT_NOM_DIMM1_VALUES_Rank1_Byte7_MSK     (0x7000000000000000ULL)
  #define MC0_CH0_CR_DDR4_MR1_RTT_NOM_DIMM1_VALUES_Rank1_Byte7_MIN     (0)
  #define MC0_CH0_CR_DDR4_MR1_RTT_NOM_DIMM1_VALUES_Rank1_Byte7_MAX     (7) // 0x00000007
  #define MC0_CH0_CR_DDR4_MR1_RTT_NOM_DIMM1_VALUES_Rank1_Byte7_DEF     (0x00000000)
  #define MC0_CH0_CR_DDR4_MR1_RTT_NOM_DIMM1_VALUES_Rank1_Byte7_HSH     (0x4378E520)

#define MC0_CH0_CR_DDR4_MR2_RTT_WR_DIMM1_VALUES_REG                    (0x0000E528)
//Duplicate of MC0_CH0_CR_DDR4_MR1_RTT_NOM_DIMM1_VALUES_REG

#define MC0_CH0_CR_DDR4_MR6_VREF_DIMM1_VALUES_0_REG                    (0x0000E530)

  #define MC0_CH0_CR_DDR4_MR6_VREF_DIMM1_VALUES_0_Byte0_OFF            ( 0)
  #define MC0_CH0_CR_DDR4_MR6_VREF_DIMM1_VALUES_0_Byte0_WID            ( 7)
  #define MC0_CH0_CR_DDR4_MR6_VREF_DIMM1_VALUES_0_Byte0_MSK            (0x0000007F)
  #define MC0_CH0_CR_DDR4_MR6_VREF_DIMM1_VALUES_0_Byte0_MIN            (0)
  #define MC0_CH0_CR_DDR4_MR6_VREF_DIMM1_VALUES_0_Byte0_MAX            (127) // 0x0000007F
  #define MC0_CH0_CR_DDR4_MR6_VREF_DIMM1_VALUES_0_Byte0_DEF            (0x00000000)
  #define MC0_CH0_CR_DDR4_MR6_VREF_DIMM1_VALUES_0_Byte0_HSH            (0x4700E530)

  #define MC0_CH0_CR_DDR4_MR6_VREF_DIMM1_VALUES_0_Byte1_OFF            ( 8)
  #define MC0_CH0_CR_DDR4_MR6_VREF_DIMM1_VALUES_0_Byte1_WID            ( 7)
  #define MC0_CH0_CR_DDR4_MR6_VREF_DIMM1_VALUES_0_Byte1_MSK            (0x00007F00)
  #define MC0_CH0_CR_DDR4_MR6_VREF_DIMM1_VALUES_0_Byte1_MIN            (0)
  #define MC0_CH0_CR_DDR4_MR6_VREF_DIMM1_VALUES_0_Byte1_MAX            (127) // 0x0000007F
  #define MC0_CH0_CR_DDR4_MR6_VREF_DIMM1_VALUES_0_Byte1_DEF            (0x00000000)
  #define MC0_CH0_CR_DDR4_MR6_VREF_DIMM1_VALUES_0_Byte1_HSH            (0x4710E530)

  #define MC0_CH0_CR_DDR4_MR6_VREF_DIMM1_VALUES_0_Byte2_OFF            (16)
  #define MC0_CH0_CR_DDR4_MR6_VREF_DIMM1_VALUES_0_Byte2_WID            ( 7)
  #define MC0_CH0_CR_DDR4_MR6_VREF_DIMM1_VALUES_0_Byte2_MSK            (0x007F0000)
  #define MC0_CH0_CR_DDR4_MR6_VREF_DIMM1_VALUES_0_Byte2_MIN            (0)
  #define MC0_CH0_CR_DDR4_MR6_VREF_DIMM1_VALUES_0_Byte2_MAX            (127) // 0x0000007F
  #define MC0_CH0_CR_DDR4_MR6_VREF_DIMM1_VALUES_0_Byte2_DEF            (0x00000000)
  #define MC0_CH0_CR_DDR4_MR6_VREF_DIMM1_VALUES_0_Byte2_HSH            (0x4720E530)

  #define MC0_CH0_CR_DDR4_MR6_VREF_DIMM1_VALUES_0_Byte3_OFF            (24)
  #define MC0_CH0_CR_DDR4_MR6_VREF_DIMM1_VALUES_0_Byte3_WID            ( 7)
  #define MC0_CH0_CR_DDR4_MR6_VREF_DIMM1_VALUES_0_Byte3_MSK            (0x7F000000)
  #define MC0_CH0_CR_DDR4_MR6_VREF_DIMM1_VALUES_0_Byte3_MIN            (0)
  #define MC0_CH0_CR_DDR4_MR6_VREF_DIMM1_VALUES_0_Byte3_MAX            (127) // 0x0000007F
  #define MC0_CH0_CR_DDR4_MR6_VREF_DIMM1_VALUES_0_Byte3_DEF            (0x00000000)
  #define MC0_CH0_CR_DDR4_MR6_VREF_DIMM1_VALUES_0_Byte3_HSH            (0x4730E530)

  #define MC0_CH0_CR_DDR4_MR6_VREF_DIMM1_VALUES_0_Byte4_OFF            (32)
  #define MC0_CH0_CR_DDR4_MR6_VREF_DIMM1_VALUES_0_Byte4_WID            ( 7)
  #define MC0_CH0_CR_DDR4_MR6_VREF_DIMM1_VALUES_0_Byte4_MSK            (0x0000007F00000000ULL)
  #define MC0_CH0_CR_DDR4_MR6_VREF_DIMM1_VALUES_0_Byte4_MIN            (0)
  #define MC0_CH0_CR_DDR4_MR6_VREF_DIMM1_VALUES_0_Byte4_MAX            (127) // 0x0000007F
  #define MC0_CH0_CR_DDR4_MR6_VREF_DIMM1_VALUES_0_Byte4_DEF            (0x00000000)
  #define MC0_CH0_CR_DDR4_MR6_VREF_DIMM1_VALUES_0_Byte4_HSH            (0x4740E530)

  #define MC0_CH0_CR_DDR4_MR6_VREF_DIMM1_VALUES_0_Byte5_OFF            (40)
  #define MC0_CH0_CR_DDR4_MR6_VREF_DIMM1_VALUES_0_Byte5_WID            ( 7)
  #define MC0_CH0_CR_DDR4_MR6_VREF_DIMM1_VALUES_0_Byte5_MSK            (0x00007F0000000000ULL)
  #define MC0_CH0_CR_DDR4_MR6_VREF_DIMM1_VALUES_0_Byte5_MIN            (0)
  #define MC0_CH0_CR_DDR4_MR6_VREF_DIMM1_VALUES_0_Byte5_MAX            (127) // 0x0000007F
  #define MC0_CH0_CR_DDR4_MR6_VREF_DIMM1_VALUES_0_Byte5_DEF            (0x00000000)
  #define MC0_CH0_CR_DDR4_MR6_VREF_DIMM1_VALUES_0_Byte5_HSH            (0x4750E530)

  #define MC0_CH0_CR_DDR4_MR6_VREF_DIMM1_VALUES_0_Byte6_OFF            (48)
  #define MC0_CH0_CR_DDR4_MR6_VREF_DIMM1_VALUES_0_Byte6_WID            ( 7)
  #define MC0_CH0_CR_DDR4_MR6_VREF_DIMM1_VALUES_0_Byte6_MSK            (0x007F000000000000ULL)
  #define MC0_CH0_CR_DDR4_MR6_VREF_DIMM1_VALUES_0_Byte6_MIN            (0)
  #define MC0_CH0_CR_DDR4_MR6_VREF_DIMM1_VALUES_0_Byte6_MAX            (127) // 0x0000007F
  #define MC0_CH0_CR_DDR4_MR6_VREF_DIMM1_VALUES_0_Byte6_DEF            (0x00000000)
  #define MC0_CH0_CR_DDR4_MR6_VREF_DIMM1_VALUES_0_Byte6_HSH            (0x4760E530)

  #define MC0_CH0_CR_DDR4_MR6_VREF_DIMM1_VALUES_0_Byte7_OFF            (56)
  #define MC0_CH0_CR_DDR4_MR6_VREF_DIMM1_VALUES_0_Byte7_WID            ( 7)
  #define MC0_CH0_CR_DDR4_MR6_VREF_DIMM1_VALUES_0_Byte7_MSK            (0x7F00000000000000ULL)
  #define MC0_CH0_CR_DDR4_MR6_VREF_DIMM1_VALUES_0_Byte7_MIN            (0)
  #define MC0_CH0_CR_DDR4_MR6_VREF_DIMM1_VALUES_0_Byte7_MAX            (127) // 0x0000007F
  #define MC0_CH0_CR_DDR4_MR6_VREF_DIMM1_VALUES_0_Byte7_DEF            (0x00000000)
  #define MC0_CH0_CR_DDR4_MR6_VREF_DIMM1_VALUES_0_Byte7_HSH            (0x4770E530)

#define MC0_CH0_CR_DDR4_MR6_VREF_DIMM1_VALUES_1_REG                    (0x0000E538)
//Duplicate of MC0_CH0_CR_DDR4_MR6_VREF_DIMM1_VALUES_0_REG

#define MC0_CH0_CR_DDR4_MR1_ODIC_DIMM1_VALUES_REG                      (0x0000E544)

  #define MC0_CH0_CR_DDR4_MR1_ODIC_DIMM1_VALUES_Rank0_Byte0_OFF        ( 0)
  #define MC0_CH0_CR_DDR4_MR1_ODIC_DIMM1_VALUES_Rank0_Byte0_WID        ( 2)
  #define MC0_CH0_CR_DDR4_MR1_ODIC_DIMM1_VALUES_Rank0_Byte0_MSK        (0x00000003)
  #define MC0_CH0_CR_DDR4_MR1_ODIC_DIMM1_VALUES_Rank0_Byte0_MIN        (0)
  #define MC0_CH0_CR_DDR4_MR1_ODIC_DIMM1_VALUES_Rank0_Byte0_MAX        (3) // 0x00000003
  #define MC0_CH0_CR_DDR4_MR1_ODIC_DIMM1_VALUES_Rank0_Byte0_DEF        (0x00000000)
  #define MC0_CH0_CR_DDR4_MR1_ODIC_DIMM1_VALUES_Rank0_Byte0_HSH        (0x0200E544)

  #define MC0_CH0_CR_DDR4_MR1_ODIC_DIMM1_VALUES_Rank0_Byte1_OFF        ( 2)
  #define MC0_CH0_CR_DDR4_MR1_ODIC_DIMM1_VALUES_Rank0_Byte1_WID        ( 2)
  #define MC0_CH0_CR_DDR4_MR1_ODIC_DIMM1_VALUES_Rank0_Byte1_MSK        (0x0000000C)
  #define MC0_CH0_CR_DDR4_MR1_ODIC_DIMM1_VALUES_Rank0_Byte1_MIN        (0)
  #define MC0_CH0_CR_DDR4_MR1_ODIC_DIMM1_VALUES_Rank0_Byte1_MAX        (3) // 0x00000003
  #define MC0_CH0_CR_DDR4_MR1_ODIC_DIMM1_VALUES_Rank0_Byte1_DEF        (0x00000000)
  #define MC0_CH0_CR_DDR4_MR1_ODIC_DIMM1_VALUES_Rank0_Byte1_HSH        (0x0204E544)

  #define MC0_CH0_CR_DDR4_MR1_ODIC_DIMM1_VALUES_Rank0_Byte2_OFF        ( 4)
  #define MC0_CH0_CR_DDR4_MR1_ODIC_DIMM1_VALUES_Rank0_Byte2_WID        ( 2)
  #define MC0_CH0_CR_DDR4_MR1_ODIC_DIMM1_VALUES_Rank0_Byte2_MSK        (0x00000030)
  #define MC0_CH0_CR_DDR4_MR1_ODIC_DIMM1_VALUES_Rank0_Byte2_MIN        (0)
  #define MC0_CH0_CR_DDR4_MR1_ODIC_DIMM1_VALUES_Rank0_Byte2_MAX        (3) // 0x00000003
  #define MC0_CH0_CR_DDR4_MR1_ODIC_DIMM1_VALUES_Rank0_Byte2_DEF        (0x00000000)
  #define MC0_CH0_CR_DDR4_MR1_ODIC_DIMM1_VALUES_Rank0_Byte2_HSH        (0x0208E544)

  #define MC0_CH0_CR_DDR4_MR1_ODIC_DIMM1_VALUES_Rank0_Byte3_OFF        ( 6)
  #define MC0_CH0_CR_DDR4_MR1_ODIC_DIMM1_VALUES_Rank0_Byte3_WID        ( 2)
  #define MC0_CH0_CR_DDR4_MR1_ODIC_DIMM1_VALUES_Rank0_Byte3_MSK        (0x000000C0)
  #define MC0_CH0_CR_DDR4_MR1_ODIC_DIMM1_VALUES_Rank0_Byte3_MIN        (0)
  #define MC0_CH0_CR_DDR4_MR1_ODIC_DIMM1_VALUES_Rank0_Byte3_MAX        (3) // 0x00000003
  #define MC0_CH0_CR_DDR4_MR1_ODIC_DIMM1_VALUES_Rank0_Byte3_DEF        (0x00000000)
  #define MC0_CH0_CR_DDR4_MR1_ODIC_DIMM1_VALUES_Rank0_Byte3_HSH        (0x020CE544)

  #define MC0_CH0_CR_DDR4_MR1_ODIC_DIMM1_VALUES_Rank0_Byte4_OFF        ( 8)
  #define MC0_CH0_CR_DDR4_MR1_ODIC_DIMM1_VALUES_Rank0_Byte4_WID        ( 2)
  #define MC0_CH0_CR_DDR4_MR1_ODIC_DIMM1_VALUES_Rank0_Byte4_MSK        (0x00000300)
  #define MC0_CH0_CR_DDR4_MR1_ODIC_DIMM1_VALUES_Rank0_Byte4_MIN        (0)
  #define MC0_CH0_CR_DDR4_MR1_ODIC_DIMM1_VALUES_Rank0_Byte4_MAX        (3) // 0x00000003
  #define MC0_CH0_CR_DDR4_MR1_ODIC_DIMM1_VALUES_Rank0_Byte4_DEF        (0x00000000)
  #define MC0_CH0_CR_DDR4_MR1_ODIC_DIMM1_VALUES_Rank0_Byte4_HSH        (0x0210E544)

  #define MC0_CH0_CR_DDR4_MR1_ODIC_DIMM1_VALUES_Rank0_Byte5_OFF        (10)
  #define MC0_CH0_CR_DDR4_MR1_ODIC_DIMM1_VALUES_Rank0_Byte5_WID        ( 2)
  #define MC0_CH0_CR_DDR4_MR1_ODIC_DIMM1_VALUES_Rank0_Byte5_MSK        (0x00000C00)
  #define MC0_CH0_CR_DDR4_MR1_ODIC_DIMM1_VALUES_Rank0_Byte5_MIN        (0)
  #define MC0_CH0_CR_DDR4_MR1_ODIC_DIMM1_VALUES_Rank0_Byte5_MAX        (3) // 0x00000003
  #define MC0_CH0_CR_DDR4_MR1_ODIC_DIMM1_VALUES_Rank0_Byte5_DEF        (0x00000000)
  #define MC0_CH0_CR_DDR4_MR1_ODIC_DIMM1_VALUES_Rank0_Byte5_HSH        (0x0214E544)

  #define MC0_CH0_CR_DDR4_MR1_ODIC_DIMM1_VALUES_Rank0_Byte6_OFF        (12)
  #define MC0_CH0_CR_DDR4_MR1_ODIC_DIMM1_VALUES_Rank0_Byte6_WID        ( 2)
  #define MC0_CH0_CR_DDR4_MR1_ODIC_DIMM1_VALUES_Rank0_Byte6_MSK        (0x00003000)
  #define MC0_CH0_CR_DDR4_MR1_ODIC_DIMM1_VALUES_Rank0_Byte6_MIN        (0)
  #define MC0_CH0_CR_DDR4_MR1_ODIC_DIMM1_VALUES_Rank0_Byte6_MAX        (3) // 0x00000003
  #define MC0_CH0_CR_DDR4_MR1_ODIC_DIMM1_VALUES_Rank0_Byte6_DEF        (0x00000000)
  #define MC0_CH0_CR_DDR4_MR1_ODIC_DIMM1_VALUES_Rank0_Byte6_HSH        (0x0218E544)

  #define MC0_CH0_CR_DDR4_MR1_ODIC_DIMM1_VALUES_Rank0_Byte7_OFF        (14)
  #define MC0_CH0_CR_DDR4_MR1_ODIC_DIMM1_VALUES_Rank0_Byte7_WID        ( 2)
  #define MC0_CH0_CR_DDR4_MR1_ODIC_DIMM1_VALUES_Rank0_Byte7_MSK        (0x0000C000)
  #define MC0_CH0_CR_DDR4_MR1_ODIC_DIMM1_VALUES_Rank0_Byte7_MIN        (0)
  #define MC0_CH0_CR_DDR4_MR1_ODIC_DIMM1_VALUES_Rank0_Byte7_MAX        (3) // 0x00000003
  #define MC0_CH0_CR_DDR4_MR1_ODIC_DIMM1_VALUES_Rank0_Byte7_DEF        (0x00000000)
  #define MC0_CH0_CR_DDR4_MR1_ODIC_DIMM1_VALUES_Rank0_Byte7_HSH        (0x021CE544)

  #define MC0_CH0_CR_DDR4_MR1_ODIC_DIMM1_VALUES_Rank1_Byte0_OFF        (16)
  #define MC0_CH0_CR_DDR4_MR1_ODIC_DIMM1_VALUES_Rank1_Byte0_WID        ( 2)
  #define MC0_CH0_CR_DDR4_MR1_ODIC_DIMM1_VALUES_Rank1_Byte0_MSK        (0x00030000)
  #define MC0_CH0_CR_DDR4_MR1_ODIC_DIMM1_VALUES_Rank1_Byte0_MIN        (0)
  #define MC0_CH0_CR_DDR4_MR1_ODIC_DIMM1_VALUES_Rank1_Byte0_MAX        (3) // 0x00000003
  #define MC0_CH0_CR_DDR4_MR1_ODIC_DIMM1_VALUES_Rank1_Byte0_DEF        (0x00000000)
  #define MC0_CH0_CR_DDR4_MR1_ODIC_DIMM1_VALUES_Rank1_Byte0_HSH        (0x0220E544)

  #define MC0_CH0_CR_DDR4_MR1_ODIC_DIMM1_VALUES_Rank1_Byte1_OFF        (18)
  #define MC0_CH0_CR_DDR4_MR1_ODIC_DIMM1_VALUES_Rank1_Byte1_WID        ( 2)
  #define MC0_CH0_CR_DDR4_MR1_ODIC_DIMM1_VALUES_Rank1_Byte1_MSK        (0x000C0000)
  #define MC0_CH0_CR_DDR4_MR1_ODIC_DIMM1_VALUES_Rank1_Byte1_MIN        (0)
  #define MC0_CH0_CR_DDR4_MR1_ODIC_DIMM1_VALUES_Rank1_Byte1_MAX        (3) // 0x00000003
  #define MC0_CH0_CR_DDR4_MR1_ODIC_DIMM1_VALUES_Rank1_Byte1_DEF        (0x00000000)
  #define MC0_CH0_CR_DDR4_MR1_ODIC_DIMM1_VALUES_Rank1_Byte1_HSH        (0x0224E544)

  #define MC0_CH0_CR_DDR4_MR1_ODIC_DIMM1_VALUES_Rank1_Byte2_OFF        (20)
  #define MC0_CH0_CR_DDR4_MR1_ODIC_DIMM1_VALUES_Rank1_Byte2_WID        ( 2)
  #define MC0_CH0_CR_DDR4_MR1_ODIC_DIMM1_VALUES_Rank1_Byte2_MSK        (0x00300000)
  #define MC0_CH0_CR_DDR4_MR1_ODIC_DIMM1_VALUES_Rank1_Byte2_MIN        (0)
  #define MC0_CH0_CR_DDR4_MR1_ODIC_DIMM1_VALUES_Rank1_Byte2_MAX        (3) // 0x00000003
  #define MC0_CH0_CR_DDR4_MR1_ODIC_DIMM1_VALUES_Rank1_Byte2_DEF        (0x00000000)
  #define MC0_CH0_CR_DDR4_MR1_ODIC_DIMM1_VALUES_Rank1_Byte2_HSH        (0x0228E544)

  #define MC0_CH0_CR_DDR4_MR1_ODIC_DIMM1_VALUES_Rank1_Byte3_OFF        (22)
  #define MC0_CH0_CR_DDR4_MR1_ODIC_DIMM1_VALUES_Rank1_Byte3_WID        ( 2)
  #define MC0_CH0_CR_DDR4_MR1_ODIC_DIMM1_VALUES_Rank1_Byte3_MSK        (0x00C00000)
  #define MC0_CH0_CR_DDR4_MR1_ODIC_DIMM1_VALUES_Rank1_Byte3_MIN        (0)
  #define MC0_CH0_CR_DDR4_MR1_ODIC_DIMM1_VALUES_Rank1_Byte3_MAX        (3) // 0x00000003
  #define MC0_CH0_CR_DDR4_MR1_ODIC_DIMM1_VALUES_Rank1_Byte3_DEF        (0x00000000)
  #define MC0_CH0_CR_DDR4_MR1_ODIC_DIMM1_VALUES_Rank1_Byte3_HSH        (0x022CE544)

  #define MC0_CH0_CR_DDR4_MR1_ODIC_DIMM1_VALUES_Rank1_Byte4_OFF        (24)
  #define MC0_CH0_CR_DDR4_MR1_ODIC_DIMM1_VALUES_Rank1_Byte4_WID        ( 2)
  #define MC0_CH0_CR_DDR4_MR1_ODIC_DIMM1_VALUES_Rank1_Byte4_MSK        (0x03000000)
  #define MC0_CH0_CR_DDR4_MR1_ODIC_DIMM1_VALUES_Rank1_Byte4_MIN        (0)
  #define MC0_CH0_CR_DDR4_MR1_ODIC_DIMM1_VALUES_Rank1_Byte4_MAX        (3) // 0x00000003
  #define MC0_CH0_CR_DDR4_MR1_ODIC_DIMM1_VALUES_Rank1_Byte4_DEF        (0x00000000)
  #define MC0_CH0_CR_DDR4_MR1_ODIC_DIMM1_VALUES_Rank1_Byte4_HSH        (0x0230E544)

  #define MC0_CH0_CR_DDR4_MR1_ODIC_DIMM1_VALUES_Rank1_Byte5_OFF        (26)
  #define MC0_CH0_CR_DDR4_MR1_ODIC_DIMM1_VALUES_Rank1_Byte5_WID        ( 2)
  #define MC0_CH0_CR_DDR4_MR1_ODIC_DIMM1_VALUES_Rank1_Byte5_MSK        (0x0C000000)
  #define MC0_CH0_CR_DDR4_MR1_ODIC_DIMM1_VALUES_Rank1_Byte5_MIN        (0)
  #define MC0_CH0_CR_DDR4_MR1_ODIC_DIMM1_VALUES_Rank1_Byte5_MAX        (3) // 0x00000003
  #define MC0_CH0_CR_DDR4_MR1_ODIC_DIMM1_VALUES_Rank1_Byte5_DEF        (0x00000000)
  #define MC0_CH0_CR_DDR4_MR1_ODIC_DIMM1_VALUES_Rank1_Byte5_HSH        (0x0234E544)

  #define MC0_CH0_CR_DDR4_MR1_ODIC_DIMM1_VALUES_Rank1_Byte6_OFF        (28)
  #define MC0_CH0_CR_DDR4_MR1_ODIC_DIMM1_VALUES_Rank1_Byte6_WID        ( 2)
  #define MC0_CH0_CR_DDR4_MR1_ODIC_DIMM1_VALUES_Rank1_Byte6_MSK        (0x30000000)
  #define MC0_CH0_CR_DDR4_MR1_ODIC_DIMM1_VALUES_Rank1_Byte6_MIN        (0)
  #define MC0_CH0_CR_DDR4_MR1_ODIC_DIMM1_VALUES_Rank1_Byte6_MAX        (3) // 0x00000003
  #define MC0_CH0_CR_DDR4_MR1_ODIC_DIMM1_VALUES_Rank1_Byte6_DEF        (0x00000000)
  #define MC0_CH0_CR_DDR4_MR1_ODIC_DIMM1_VALUES_Rank1_Byte6_HSH        (0x0238E544)

  #define MC0_CH0_CR_DDR4_MR1_ODIC_DIMM1_VALUES_Rank1_Byte7_OFF        (30)
  #define MC0_CH0_CR_DDR4_MR1_ODIC_DIMM1_VALUES_Rank1_Byte7_WID        ( 2)
  #define MC0_CH0_CR_DDR4_MR1_ODIC_DIMM1_VALUES_Rank1_Byte7_MSK        (0xC0000000)
  #define MC0_CH0_CR_DDR4_MR1_ODIC_DIMM1_VALUES_Rank1_Byte7_MIN        (0)
  #define MC0_CH0_CR_DDR4_MR1_ODIC_DIMM1_VALUES_Rank1_Byte7_MAX        (3) // 0x00000003
  #define MC0_CH0_CR_DDR4_MR1_ODIC_DIMM1_VALUES_Rank1_Byte7_DEF        (0x00000000)
  #define MC0_CH0_CR_DDR4_MR1_ODIC_DIMM1_VALUES_Rank1_Byte7_HSH        (0x023CE544)

#define MC0_CH0_CR_DDR4_MR5_RTT_PARK_VALUES_REG                        (0x0000E548)
//Duplicate of MC0_CH0_CR_DDR4_MR1_RTT_NOM_DIMM1_VALUES_REG

#define MC0_CH0_CR_DDR4_MR5_RTT_PARK_DIMM1_VALUES_REG                  (0x0000E550)
//Duplicate of MC0_CH0_CR_DDR4_MR1_RTT_NOM_DIMM1_VALUES_REG

#define MC0_CH0_CR_DDR4_MR1_RTT_NOM_VALUES_REG                         (0x0000E558)
//Duplicate of MC0_CH0_CR_DDR4_MR1_RTT_NOM_DIMM1_VALUES_REG

#define MC0_CH0_CR_LPDDR4_DISCRETE_MR_VALUES_0_REG                     (0x0000E560)

  #define MC0_CH0_CR_LPDDR4_DISCRETE_MR_VALUES_0_PDDS_OFF              ( 0)
  #define MC0_CH0_CR_LPDDR4_DISCRETE_MR_VALUES_0_PDDS_WID              ( 3)
  #define MC0_CH0_CR_LPDDR4_DISCRETE_MR_VALUES_0_PDDS_MSK              (0x00000007)
  #define MC0_CH0_CR_LPDDR4_DISCRETE_MR_VALUES_0_PDDS_MIN              (0)
  #define MC0_CH0_CR_LPDDR4_DISCRETE_MR_VALUES_0_PDDS_MAX              (7) // 0x00000007
  #define MC0_CH0_CR_LPDDR4_DISCRETE_MR_VALUES_0_PDDS_DEF              (0x00000000)
  #define MC0_CH0_CR_LPDDR4_DISCRETE_MR_VALUES_0_PDDS_HSH              (0x4300E560)

  #define MC0_CH0_CR_LPDDR4_DISCRETE_MR_VALUES_0_DQ_ODT_OFF            ( 3)
  #define MC0_CH0_CR_LPDDR4_DISCRETE_MR_VALUES_0_DQ_ODT_WID            ( 3)
  #define MC0_CH0_CR_LPDDR4_DISCRETE_MR_VALUES_0_DQ_ODT_MSK            (0x00000038)
  #define MC0_CH0_CR_LPDDR4_DISCRETE_MR_VALUES_0_DQ_ODT_MIN            (0)
  #define MC0_CH0_CR_LPDDR4_DISCRETE_MR_VALUES_0_DQ_ODT_MAX            (7) // 0x00000007
  #define MC0_CH0_CR_LPDDR4_DISCRETE_MR_VALUES_0_DQ_ODT_DEF            (0x00000000)
  #define MC0_CH0_CR_LPDDR4_DISCRETE_MR_VALUES_0_DQ_ODT_HSH            (0x4306E560)

  #define MC0_CH0_CR_LPDDR4_DISCRETE_MR_VALUES_0_CA_ODT_OFF            ( 6)
  #define MC0_CH0_CR_LPDDR4_DISCRETE_MR_VALUES_0_CA_ODT_WID            ( 3)
  #define MC0_CH0_CR_LPDDR4_DISCRETE_MR_VALUES_0_CA_ODT_MSK            (0x000001C0)
  #define MC0_CH0_CR_LPDDR4_DISCRETE_MR_VALUES_0_CA_ODT_MIN            (0)
  #define MC0_CH0_CR_LPDDR4_DISCRETE_MR_VALUES_0_CA_ODT_MAX            (7) // 0x00000007
  #define MC0_CH0_CR_LPDDR4_DISCRETE_MR_VALUES_0_CA_ODT_DEF            (0x00000000)
  #define MC0_CH0_CR_LPDDR4_DISCRETE_MR_VALUES_0_CA_ODT_HSH            (0x430CE560)

  #define MC0_CH0_CR_LPDDR4_DISCRETE_MR_VALUES_0_CA_VREF_OFF           ( 9)
  #define MC0_CH0_CR_LPDDR4_DISCRETE_MR_VALUES_0_CA_VREF_WID           ( 7)
  #define MC0_CH0_CR_LPDDR4_DISCRETE_MR_VALUES_0_CA_VREF_MSK           (0x0000FE00)
  #define MC0_CH0_CR_LPDDR4_DISCRETE_MR_VALUES_0_CA_VREF_MIN           (0)
  #define MC0_CH0_CR_LPDDR4_DISCRETE_MR_VALUES_0_CA_VREF_MAX           (127) // 0x0000007F
  #define MC0_CH0_CR_LPDDR4_DISCRETE_MR_VALUES_0_CA_VREF_DEF           (0x00000000)
  #define MC0_CH0_CR_LPDDR4_DISCRETE_MR_VALUES_0_CA_VREF_HSH           (0x4712E560)

  #define MC0_CH0_CR_LPDDR4_DISCRETE_MR_VALUES_0_DQ_VREF_OFF           (16)
  #define MC0_CH0_CR_LPDDR4_DISCRETE_MR_VALUES_0_DQ_VREF_WID           ( 7)
  #define MC0_CH0_CR_LPDDR4_DISCRETE_MR_VALUES_0_DQ_VREF_MSK           (0x007F0000)
  #define MC0_CH0_CR_LPDDR4_DISCRETE_MR_VALUES_0_DQ_VREF_MIN           (0)
  #define MC0_CH0_CR_LPDDR4_DISCRETE_MR_VALUES_0_DQ_VREF_MAX           (127) // 0x0000007F
  #define MC0_CH0_CR_LPDDR4_DISCRETE_MR_VALUES_0_DQ_VREF_DEF           (0x00000000)
  #define MC0_CH0_CR_LPDDR4_DISCRETE_MR_VALUES_0_DQ_VREF_HSH           (0x4720E560)

  #define MC0_CH0_CR_LPDDR4_DISCRETE_MR_VALUES_0_CODT_OFF              (23)
  #define MC0_CH0_CR_LPDDR4_DISCRETE_MR_VALUES_0_CODT_WID              ( 6)
  #define MC0_CH0_CR_LPDDR4_DISCRETE_MR_VALUES_0_CODT_MSK              (0x1F800000)
  #define MC0_CH0_CR_LPDDR4_DISCRETE_MR_VALUES_0_CODT_MIN              (0)
  #define MC0_CH0_CR_LPDDR4_DISCRETE_MR_VALUES_0_CODT_MAX              (63) // 0x0000003F
  #define MC0_CH0_CR_LPDDR4_DISCRETE_MR_VALUES_0_CODT_DEF              (0x00000000)
  #define MC0_CH0_CR_LPDDR4_DISCRETE_MR_VALUES_0_CODT_HSH              (0x462EE560)

  #define MC0_CH0_CR_LPDDR4_DISCRETE_MR_VALUES_0_CA_VREF_High_OFF      (32)
  #define MC0_CH0_CR_LPDDR4_DISCRETE_MR_VALUES_0_CA_VREF_High_WID      ( 7)
  #define MC0_CH0_CR_LPDDR4_DISCRETE_MR_VALUES_0_CA_VREF_High_MSK      (0x0000007F00000000ULL)
  #define MC0_CH0_CR_LPDDR4_DISCRETE_MR_VALUES_0_CA_VREF_High_MIN      (0)
  #define MC0_CH0_CR_LPDDR4_DISCRETE_MR_VALUES_0_CA_VREF_High_MAX      (127) // 0x0000007F
  #define MC0_CH0_CR_LPDDR4_DISCRETE_MR_VALUES_0_CA_VREF_High_DEF      (0x00000000)
  #define MC0_CH0_CR_LPDDR4_DISCRETE_MR_VALUES_0_CA_VREF_High_HSH      (0x4740E560)

  #define MC0_CH0_CR_LPDDR4_DISCRETE_MR_VALUES_0_DQ_VREF_High_OFF      (39)
  #define MC0_CH0_CR_LPDDR4_DISCRETE_MR_VALUES_0_DQ_VREF_High_WID      ( 7)
  #define MC0_CH0_CR_LPDDR4_DISCRETE_MR_VALUES_0_DQ_VREF_High_MSK      (0x00003F8000000000ULL)
  #define MC0_CH0_CR_LPDDR4_DISCRETE_MR_VALUES_0_DQ_VREF_High_MIN      (0)
  #define MC0_CH0_CR_LPDDR4_DISCRETE_MR_VALUES_0_DQ_VREF_High_MAX      (127) // 0x0000007F
  #define MC0_CH0_CR_LPDDR4_DISCRETE_MR_VALUES_0_DQ_VREF_High_DEF      (0x00000000)
  #define MC0_CH0_CR_LPDDR4_DISCRETE_MR_VALUES_0_DQ_VREF_High_HSH      (0x474EE560)

#define MC0_CH0_CR_LPDDR4_DISCRETE_MR_VALUES_1_REG                     (0x0000E568)
//Duplicate of MC0_CH0_CR_LPDDR4_DISCRETE_MR_VALUES_0_REG

#define MC0_CH0_CR_LPDDR4_DISCRETE_MR_VALUES_2_REG                     (0x0000E570)
//Duplicate of MC0_CH0_CR_LPDDR4_DISCRETE_MR_VALUES_0_REG

#define MC0_CH0_CR_LPDDR4_DISCRETE_MR_VALUES_3_REG                     (0x0000E578)
//Duplicate of MC0_CH0_CR_LPDDR4_DISCRETE_MR_VALUES_0_REG

#define MC0_CH0_CR_LPDDR4_DISCRETE_MR_VALUES_4_REG                     (0x0000E580)
//Duplicate of MC0_CH0_CR_LPDDR4_DISCRETE_MR_VALUES_0_REG

#define MC0_CH0_CR_LPDDR4_DISCRETE_MR_VALUES_5_REG                     (0x0000E588)
//Duplicate of MC0_CH0_CR_LPDDR4_DISCRETE_MR_VALUES_0_REG

#define MC0_CH0_CR_LPDDR4_DISCRETE_MR_VALUES_6_REG                     (0x0000E590)
//Duplicate of MC0_CH0_CR_LPDDR4_DISCRETE_MR_VALUES_0_REG

#define MC0_CH0_CR_LPDDR4_DISCRETE_MR_VALUES_7_REG                     (0x0000E598)
//Duplicate of MC0_CH0_CR_LPDDR4_DISCRETE_MR_VALUES_0_REG

#define MC0_CH0_CR_DDR4_MR0_MR1_CONTENT_REG                            (0x0000E5A0)

  #define MC0_CH0_CR_DDR4_MR0_MR1_CONTENT_MR0_OFF                      ( 0)
  #define MC0_CH0_CR_DDR4_MR0_MR1_CONTENT_MR0_WID                      (14)
  #define MC0_CH0_CR_DDR4_MR0_MR1_CONTENT_MR0_MSK                      (0x00003FFF)
  #define MC0_CH0_CR_DDR4_MR0_MR1_CONTENT_MR0_MIN                      (0)
  #define MC0_CH0_CR_DDR4_MR0_MR1_CONTENT_MR0_MAX                      (16383) // 0x00003FFF
  #define MC0_CH0_CR_DDR4_MR0_MR1_CONTENT_MR0_DEF                      (0x00000000)
  #define MC0_CH0_CR_DDR4_MR0_MR1_CONTENT_MR0_HSH                      (0x0E00E5A0)

  #define MC0_CH0_CR_DDR4_MR0_MR1_CONTENT_MR1_OFF                      (16)
  #define MC0_CH0_CR_DDR4_MR0_MR1_CONTENT_MR1_WID                      (14)
  #define MC0_CH0_CR_DDR4_MR0_MR1_CONTENT_MR1_MSK                      (0x3FFF0000)
  #define MC0_CH0_CR_DDR4_MR0_MR1_CONTENT_MR1_MIN                      (0)
  #define MC0_CH0_CR_DDR4_MR0_MR1_CONTENT_MR1_MAX                      (16383) // 0x00003FFF
  #define MC0_CH0_CR_DDR4_MR0_MR1_CONTENT_MR1_DEF                      (0x00000000)
  #define MC0_CH0_CR_DDR4_MR0_MR1_CONTENT_MR1_HSH                      (0x0E20E5A0)

#define MC0_CH0_CR_DDR4_MR2_MR3_CONTENT_REG                            (0x0000E5A4)

  #define MC0_CH0_CR_DDR4_MR2_MR3_CONTENT_MR2_OFF                      ( 0)
  #define MC0_CH0_CR_DDR4_MR2_MR3_CONTENT_MR2_WID                      (14)
  #define MC0_CH0_CR_DDR4_MR2_MR3_CONTENT_MR2_MSK                      (0x00003FFF)
  #define MC0_CH0_CR_DDR4_MR2_MR3_CONTENT_MR2_MIN                      (0)
  #define MC0_CH0_CR_DDR4_MR2_MR3_CONTENT_MR2_MAX                      (16383) // 0x00003FFF
  #define MC0_CH0_CR_DDR4_MR2_MR3_CONTENT_MR2_DEF                      (0x00000000)
  #define MC0_CH0_CR_DDR4_MR2_MR3_CONTENT_MR2_HSH                      (0x0E00E5A4)

  #define MC0_CH0_CR_DDR4_MR2_MR3_CONTENT_MR3_OFF                      (16)
  #define MC0_CH0_CR_DDR4_MR2_MR3_CONTENT_MR3_WID                      (14)
  #define MC0_CH0_CR_DDR4_MR2_MR3_CONTENT_MR3_MSK                      (0x3FFF0000)
  #define MC0_CH0_CR_DDR4_MR2_MR3_CONTENT_MR3_MIN                      (0)
  #define MC0_CH0_CR_DDR4_MR2_MR3_CONTENT_MR3_MAX                      (16383) // 0x00003FFF
  #define MC0_CH0_CR_DDR4_MR2_MR3_CONTENT_MR3_DEF                      (0x00000000)
  #define MC0_CH0_CR_DDR4_MR2_MR3_CONTENT_MR3_HSH                      (0x0E20E5A4)

#define MC0_CH0_CR_DDR4_MR4_MR5_CONTENT_REG                            (0x0000E5A8)

  #define MC0_CH0_CR_DDR4_MR4_MR5_CONTENT_MR4_OFF                      ( 0)
  #define MC0_CH0_CR_DDR4_MR4_MR5_CONTENT_MR4_WID                      (14)
  #define MC0_CH0_CR_DDR4_MR4_MR5_CONTENT_MR4_MSK                      (0x00003FFF)
  #define MC0_CH0_CR_DDR4_MR4_MR5_CONTENT_MR4_MIN                      (0)
  #define MC0_CH0_CR_DDR4_MR4_MR5_CONTENT_MR4_MAX                      (16383) // 0x00003FFF
  #define MC0_CH0_CR_DDR4_MR4_MR5_CONTENT_MR4_DEF                      (0x00000000)
  #define MC0_CH0_CR_DDR4_MR4_MR5_CONTENT_MR4_HSH                      (0x0E00E5A8)

  #define MC0_CH0_CR_DDR4_MR4_MR5_CONTENT_MR5_OFF                      (16)
  #define MC0_CH0_CR_DDR4_MR4_MR5_CONTENT_MR5_WID                      (14)
  #define MC0_CH0_CR_DDR4_MR4_MR5_CONTENT_MR5_MSK                      (0x3FFF0000)
  #define MC0_CH0_CR_DDR4_MR4_MR5_CONTENT_MR5_MIN                      (0)
  #define MC0_CH0_CR_DDR4_MR4_MR5_CONTENT_MR5_MAX                      (16383) // 0x00003FFF
  #define MC0_CH0_CR_DDR4_MR4_MR5_CONTENT_MR5_DEF                      (0x00000000)
  #define MC0_CH0_CR_DDR4_MR4_MR5_CONTENT_MR5_HSH                      (0x0E20E5A8)

#define MC0_CH0_CR_DDR4_MR6_MR7_CONTENT_REG                            (0x0000E5AC)

  #define MC0_CH0_CR_DDR4_MR6_MR7_CONTENT_MR6_OFF                      ( 0)
  #define MC0_CH0_CR_DDR4_MR6_MR7_CONTENT_MR6_WID                      (14)
  #define MC0_CH0_CR_DDR4_MR6_MR7_CONTENT_MR6_MSK                      (0x00003FFF)
  #define MC0_CH0_CR_DDR4_MR6_MR7_CONTENT_MR6_MIN                      (0)
  #define MC0_CH0_CR_DDR4_MR6_MR7_CONTENT_MR6_MAX                      (16383) // 0x00003FFF
  #define MC0_CH0_CR_DDR4_MR6_MR7_CONTENT_MR6_DEF                      (0x00000000)
  #define MC0_CH0_CR_DDR4_MR6_MR7_CONTENT_MR6_HSH                      (0x0E00E5AC)

  #define MC0_CH0_CR_DDR4_MR6_MR7_CONTENT_MR7_OFF                      (16)
  #define MC0_CH0_CR_DDR4_MR6_MR7_CONTENT_MR7_WID                      (14)
  #define MC0_CH0_CR_DDR4_MR6_MR7_CONTENT_MR7_MSK                      (0x3FFF0000)
  #define MC0_CH0_CR_DDR4_MR6_MR7_CONTENT_MR7_MIN                      (0)
  #define MC0_CH0_CR_DDR4_MR6_MR7_CONTENT_MR7_MAX                      (16383) // 0x00003FFF
  #define MC0_CH0_CR_DDR4_MR6_MR7_CONTENT_MR7_DEF                      (0x00000000)
  #define MC0_CH0_CR_DDR4_MR6_MR7_CONTENT_MR7_HSH                      (0x0E20E5AC)

#define MC0_CH0_CR_DDR4_MR2_RTT_WR_VALUES_REG                          (0x0000E5B0)
//Duplicate of MC0_CH0_CR_DDR4_MR1_RTT_NOM_DIMM1_VALUES_REG

#define MC0_CH0_CR_DDR4_MR6_VREF_VALUES_0_REG                          (0x0000E5B8)
//Duplicate of MC0_CH0_CR_DDR4_MR6_VREF_DIMM1_VALUES_0_REG

#define MC0_CH0_CR_DDR4_MR6_VREF_VALUES_1_REG                          (0x0000E5C0)
//Duplicate of MC0_CH0_CR_DDR4_MR6_VREF_DIMM1_VALUES_0_REG

#define MC0_CH0_CR_LPDDR_MR_CONTENT_REG                                (0x0000E5C8)

  #define MC0_CH0_CR_LPDDR_MR_CONTENT_MR1_OFF                          ( 0)
  #define MC0_CH0_CR_LPDDR_MR_CONTENT_MR1_WID                          ( 8)
  #define MC0_CH0_CR_LPDDR_MR_CONTENT_MR1_MSK                          (0x000000FF)
  #define MC0_CH0_CR_LPDDR_MR_CONTENT_MR1_MIN                          (0)
  #define MC0_CH0_CR_LPDDR_MR_CONTENT_MR1_MAX                          (255) // 0x000000FF
  #define MC0_CH0_CR_LPDDR_MR_CONTENT_MR1_DEF                          (0x00000000)
  #define MC0_CH0_CR_LPDDR_MR_CONTENT_MR1_HSH                          (0x4800E5C8)

  #define MC0_CH0_CR_LPDDR_MR_CONTENT_MR2_OFF                          ( 8)
  #define MC0_CH0_CR_LPDDR_MR_CONTENT_MR2_WID                          ( 8)
  #define MC0_CH0_CR_LPDDR_MR_CONTENT_MR2_MSK                          (0x0000FF00)
  #define MC0_CH0_CR_LPDDR_MR_CONTENT_MR2_MIN                          (0)
  #define MC0_CH0_CR_LPDDR_MR_CONTENT_MR2_MAX                          (255) // 0x000000FF
  #define MC0_CH0_CR_LPDDR_MR_CONTENT_MR2_DEF                          (0x00000000)
  #define MC0_CH0_CR_LPDDR_MR_CONTENT_MR2_HSH                          (0x4810E5C8)

  #define MC0_CH0_CR_LPDDR_MR_CONTENT_MR3_OFF                          (16)
  #define MC0_CH0_CR_LPDDR_MR_CONTENT_MR3_WID                          ( 8)
  #define MC0_CH0_CR_LPDDR_MR_CONTENT_MR3_MSK                          (0x00FF0000)
  #define MC0_CH0_CR_LPDDR_MR_CONTENT_MR3_MIN                          (0)
  #define MC0_CH0_CR_LPDDR_MR_CONTENT_MR3_MAX                          (255) // 0x000000FF
  #define MC0_CH0_CR_LPDDR_MR_CONTENT_MR3_DEF                          (0x00000000)
  #define MC0_CH0_CR_LPDDR_MR_CONTENT_MR3_HSH                          (0x4820E5C8)

  #define MC0_CH0_CR_LPDDR_MR_CONTENT_MR11_OFF                         (24)
  #define MC0_CH0_CR_LPDDR_MR_CONTENT_MR11_WID                         ( 8)
  #define MC0_CH0_CR_LPDDR_MR_CONTENT_MR11_MSK                         (0xFF000000)
  #define MC0_CH0_CR_LPDDR_MR_CONTENT_MR11_MIN                         (0)
  #define MC0_CH0_CR_LPDDR_MR_CONTENT_MR11_MAX                         (255) // 0x000000FF
  #define MC0_CH0_CR_LPDDR_MR_CONTENT_MR11_DEF                         (0x00000000)
  #define MC0_CH0_CR_LPDDR_MR_CONTENT_MR11_HSH                         (0x4830E5C8)

  #define MC0_CH0_CR_LPDDR_MR_CONTENT_MR12_OFF                         (32)
  #define MC0_CH0_CR_LPDDR_MR_CONTENT_MR12_WID                         ( 8)
  #define MC0_CH0_CR_LPDDR_MR_CONTENT_MR12_MSK                         (0x000000FF00000000ULL)
  #define MC0_CH0_CR_LPDDR_MR_CONTENT_MR12_MIN                         (0)
  #define MC0_CH0_CR_LPDDR_MR_CONTENT_MR12_MAX                         (255) // 0x000000FF
  #define MC0_CH0_CR_LPDDR_MR_CONTENT_MR12_DEF                         (0x00000000)
  #define MC0_CH0_CR_LPDDR_MR_CONTENT_MR12_HSH                         (0x4840E5C8)

  #define MC0_CH0_CR_LPDDR_MR_CONTENT_MR13_OFF                         (40)
  #define MC0_CH0_CR_LPDDR_MR_CONTENT_MR13_WID                         ( 8)
  #define MC0_CH0_CR_LPDDR_MR_CONTENT_MR13_MSK                         (0x0000FF0000000000ULL)
  #define MC0_CH0_CR_LPDDR_MR_CONTENT_MR13_MIN                         (0)
  #define MC0_CH0_CR_LPDDR_MR_CONTENT_MR13_MAX                         (255) // 0x000000FF
  #define MC0_CH0_CR_LPDDR_MR_CONTENT_MR13_DEF                         (0x00000000)
  #define MC0_CH0_CR_LPDDR_MR_CONTENT_MR13_HSH                         (0x4850E5C8)

  #define MC0_CH0_CR_LPDDR_MR_CONTENT_MR22_OFF                         (48)
  #define MC0_CH0_CR_LPDDR_MR_CONTENT_MR22_WID                         ( 8)
  #define MC0_CH0_CR_LPDDR_MR_CONTENT_MR22_MSK                         (0x00FF000000000000ULL)
  #define MC0_CH0_CR_LPDDR_MR_CONTENT_MR22_MIN                         (0)
  #define MC0_CH0_CR_LPDDR_MR_CONTENT_MR22_MAX                         (255) // 0x000000FF
  #define MC0_CH0_CR_LPDDR_MR_CONTENT_MR22_DEF                         (0x00000000)
  #define MC0_CH0_CR_LPDDR_MR_CONTENT_MR22_HSH                         (0x4860E5C8)

  #define MC0_CH0_CR_LPDDR_MR_CONTENT_MR23_OFF                         (56)
  #define MC0_CH0_CR_LPDDR_MR_CONTENT_MR23_WID                         ( 8)
  #define MC0_CH0_CR_LPDDR_MR_CONTENT_MR23_MSK                         (0xFF00000000000000ULL)
  #define MC0_CH0_CR_LPDDR_MR_CONTENT_MR23_MIN                         (0)
  #define MC0_CH0_CR_LPDDR_MR_CONTENT_MR23_MAX                         (255) // 0x000000FF
  #define MC0_CH0_CR_LPDDR_MR_CONTENT_MR23_DEF                         (0x0000003F)
  #define MC0_CH0_CR_LPDDR_MR_CONTENT_MR23_HSH                         (0x4870E5C8)

#define MC0_CH0_CR_MRS_FSM_CONTROL_REG                                 (0x0000E5D0)

  #define MC0_CH0_CR_MRS_FSM_CONTROL_DDR4_Restore_MR_OFF               ( 0)
  #define MC0_CH0_CR_MRS_FSM_CONTROL_DDR4_Restore_MR_WID               ( 8)
  #define MC0_CH0_CR_MRS_FSM_CONTROL_DDR4_Restore_MR_MSK               (0x000000FF)
  #define MC0_CH0_CR_MRS_FSM_CONTROL_DDR4_Restore_MR_MIN               (0)
  #define MC0_CH0_CR_MRS_FSM_CONTROL_DDR4_Restore_MR_MAX               (255) // 0x000000FF
  #define MC0_CH0_CR_MRS_FSM_CONTROL_DDR4_Restore_MR_DEF               (0x00000000)
  #define MC0_CH0_CR_MRS_FSM_CONTROL_DDR4_Restore_MR_HSH               (0x4800E5D0)

  #define MC0_CH0_CR_MRS_FSM_CONTROL_DDR4_Restore_MR1_ODIC_Per_Device_OFF ( 8)
  #define MC0_CH0_CR_MRS_FSM_CONTROL_DDR4_Restore_MR1_ODIC_Per_Device_WID ( 1)
  #define MC0_CH0_CR_MRS_FSM_CONTROL_DDR4_Restore_MR1_ODIC_Per_Device_MSK (0x00000100)
  #define MC0_CH0_CR_MRS_FSM_CONTROL_DDR4_Restore_MR1_ODIC_Per_Device_MIN (0)
  #define MC0_CH0_CR_MRS_FSM_CONTROL_DDR4_Restore_MR1_ODIC_Per_Device_MAX (1) // 0x00000001
  #define MC0_CH0_CR_MRS_FSM_CONTROL_DDR4_Restore_MR1_ODIC_Per_Device_DEF (0x00000000)
  #define MC0_CH0_CR_MRS_FSM_CONTROL_DDR4_Restore_MR1_ODIC_Per_Device_HSH (0x4110E5D0)

  #define MC0_CH0_CR_MRS_FSM_CONTROL_DDR4_Restore_MR2_Per_Device_OFF   ( 9)
  #define MC0_CH0_CR_MRS_FSM_CONTROL_DDR4_Restore_MR2_Per_Device_WID   ( 1)
  #define MC0_CH0_CR_MRS_FSM_CONTROL_DDR4_Restore_MR2_Per_Device_MSK   (0x00000200)
  #define MC0_CH0_CR_MRS_FSM_CONTROL_DDR4_Restore_MR2_Per_Device_MIN   (0)
  #define MC0_CH0_CR_MRS_FSM_CONTROL_DDR4_Restore_MR2_Per_Device_MAX   (1) // 0x00000001
  #define MC0_CH0_CR_MRS_FSM_CONTROL_DDR4_Restore_MR2_Per_Device_DEF   (0x00000000)
  #define MC0_CH0_CR_MRS_FSM_CONTROL_DDR4_Restore_MR2_Per_Device_HSH   (0x4112E5D0)

  #define MC0_CH0_CR_MRS_FSM_CONTROL_DDR4_Restore_MR6_Per_Device_OFF   (10)
  #define MC0_CH0_CR_MRS_FSM_CONTROL_DDR4_Restore_MR6_Per_Device_WID   ( 1)
  #define MC0_CH0_CR_MRS_FSM_CONTROL_DDR4_Restore_MR6_Per_Device_MSK   (0x00000400)
  #define MC0_CH0_CR_MRS_FSM_CONTROL_DDR4_Restore_MR6_Per_Device_MIN   (0)
  #define MC0_CH0_CR_MRS_FSM_CONTROL_DDR4_Restore_MR6_Per_Device_MAX   (1) // 0x00000001
  #define MC0_CH0_CR_MRS_FSM_CONTROL_DDR4_Restore_MR6_Per_Device_DEF   (0x00000000)
  #define MC0_CH0_CR_MRS_FSM_CONTROL_DDR4_Restore_MR6_Per_Device_HSH   (0x4114E5D0)

  #define MC0_CH0_CR_MRS_FSM_CONTROL_DDR4_PDA_VREF_Initial_OFF         (11)
  #define MC0_CH0_CR_MRS_FSM_CONTROL_DDR4_PDA_VREF_Initial_WID         ( 1)
  #define MC0_CH0_CR_MRS_FSM_CONTROL_DDR4_PDA_VREF_Initial_MSK         (0x00000800)
  #define MC0_CH0_CR_MRS_FSM_CONTROL_DDR4_PDA_VREF_Initial_MIN         (0)
  #define MC0_CH0_CR_MRS_FSM_CONTROL_DDR4_PDA_VREF_Initial_MAX         (1) // 0x00000001
  #define MC0_CH0_CR_MRS_FSM_CONTROL_DDR4_PDA_VREF_Initial_DEF         (0x00000000)
  #define MC0_CH0_CR_MRS_FSM_CONTROL_DDR4_PDA_VREF_Initial_HSH         (0x4116E5D0)

  #define MC0_CH0_CR_MRS_FSM_CONTROL_DDR4_Short_VREF_Exit_OFF          (12)
  #define MC0_CH0_CR_MRS_FSM_CONTROL_DDR4_Short_VREF_Exit_WID          ( 1)
  #define MC0_CH0_CR_MRS_FSM_CONTROL_DDR4_Short_VREF_Exit_MSK          (0x00001000)
  #define MC0_CH0_CR_MRS_FSM_CONTROL_DDR4_Short_VREF_Exit_MIN          (0)
  #define MC0_CH0_CR_MRS_FSM_CONTROL_DDR4_Short_VREF_Exit_MAX          (1) // 0x00000001
  #define MC0_CH0_CR_MRS_FSM_CONTROL_DDR4_Short_VREF_Exit_DEF          (0x00000000)
  #define MC0_CH0_CR_MRS_FSM_CONTROL_DDR4_Short_VREF_Exit_HSH          (0x4118E5D0)

  #define MC0_CH0_CR_MRS_FSM_CONTROL_LPDDR_Restore_MR_OFF              (13)
  #define MC0_CH0_CR_MRS_FSM_CONTROL_LPDDR_Restore_MR_WID              ( 9)
  #define MC0_CH0_CR_MRS_FSM_CONTROL_LPDDR_Restore_MR_MSK              (0x003FE000)
  #define MC0_CH0_CR_MRS_FSM_CONTROL_LPDDR_Restore_MR_MIN              (0)
  #define MC0_CH0_CR_MRS_FSM_CONTROL_LPDDR_Restore_MR_MAX              (511) // 0x000001FF
  #define MC0_CH0_CR_MRS_FSM_CONTROL_LPDDR_Restore_MR_DEF              (0x00000000)
  #define MC0_CH0_CR_MRS_FSM_CONTROL_LPDDR_Restore_MR_HSH              (0x491AE5D0)

  #define MC0_CH0_CR_MRS_FSM_CONTROL_do_ZQCL_OFF                       (22)
  #define MC0_CH0_CR_MRS_FSM_CONTROL_do_ZQCL_WID                       ( 1)
  #define MC0_CH0_CR_MRS_FSM_CONTROL_do_ZQCL_MSK                       (0x00400000)
  #define MC0_CH0_CR_MRS_FSM_CONTROL_do_ZQCL_MIN                       (0)
  #define MC0_CH0_CR_MRS_FSM_CONTROL_do_ZQCL_MAX                       (1) // 0x00000001
  #define MC0_CH0_CR_MRS_FSM_CONTROL_do_ZQCL_DEF                       (0x00000000)
  #define MC0_CH0_CR_MRS_FSM_CONTROL_do_ZQCL_HSH                       (0x412CE5D0)

  #define MC0_CH0_CR_MRS_FSM_CONTROL_zero_rank1_MR11_OFF               (23)
  #define MC0_CH0_CR_MRS_FSM_CONTROL_zero_rank1_MR11_WID               ( 1)
  #define MC0_CH0_CR_MRS_FSM_CONTROL_zero_rank1_MR11_MSK               (0x00800000)
  #define MC0_CH0_CR_MRS_FSM_CONTROL_zero_rank1_MR11_MIN               (0)
  #define MC0_CH0_CR_MRS_FSM_CONTROL_zero_rank1_MR11_MAX               (1) // 0x00000001
  #define MC0_CH0_CR_MRS_FSM_CONTROL_zero_rank1_MR11_DEF               (0x00000000)
  #define MC0_CH0_CR_MRS_FSM_CONTROL_zero_rank1_MR11_HSH               (0x412EE5D0)

  #define MC0_CH0_CR_MRS_FSM_CONTROL_reset_flow_OFF                    (24)
  #define MC0_CH0_CR_MRS_FSM_CONTROL_reset_flow_WID                    ( 1)
  #define MC0_CH0_CR_MRS_FSM_CONTROL_reset_flow_MSK                    (0x01000000)
  #define MC0_CH0_CR_MRS_FSM_CONTROL_reset_flow_MIN                    (0)
  #define MC0_CH0_CR_MRS_FSM_CONTROL_reset_flow_MAX                    (1) // 0x00000001
  #define MC0_CH0_CR_MRS_FSM_CONTROL_reset_flow_DEF                    (0x00000000)
  #define MC0_CH0_CR_MRS_FSM_CONTROL_reset_flow_HSH                    (0x4130E5D0)

  #define MC0_CH0_CR_MRS_FSM_CONTROL_vref_time_per_byte_OFF            (25)
  #define MC0_CH0_CR_MRS_FSM_CONTROL_vref_time_per_byte_WID            ( 1)
  #define MC0_CH0_CR_MRS_FSM_CONTROL_vref_time_per_byte_MSK            (0x02000000)
  #define MC0_CH0_CR_MRS_FSM_CONTROL_vref_time_per_byte_MIN            (0)
  #define MC0_CH0_CR_MRS_FSM_CONTROL_vref_time_per_byte_MAX            (1) // 0x00000001
  #define MC0_CH0_CR_MRS_FSM_CONTROL_vref_time_per_byte_DEF            (0x00000000)
  #define MC0_CH0_CR_MRS_FSM_CONTROL_vref_time_per_byte_HSH            (0x4132E5D0)

  #define MC0_CH0_CR_MRS_FSM_CONTROL_do_pda_for_1R1R_OFF               (26)
  #define MC0_CH0_CR_MRS_FSM_CONTROL_do_pda_for_1R1R_WID               ( 1)
  #define MC0_CH0_CR_MRS_FSM_CONTROL_do_pda_for_1R1R_MSK               (0x04000000)
  #define MC0_CH0_CR_MRS_FSM_CONTROL_do_pda_for_1R1R_MIN               (0)
  #define MC0_CH0_CR_MRS_FSM_CONTROL_do_pda_for_1R1R_MAX               (1) // 0x00000001
  #define MC0_CH0_CR_MRS_FSM_CONTROL_do_pda_for_1R1R_DEF               (0x00000000)
  #define MC0_CH0_CR_MRS_FSM_CONTROL_do_pda_for_1R1R_HSH               (0x4134E5D0)

  #define MC0_CH0_CR_MRS_FSM_CONTROL_add_initial_vref_OFF              (27)
  #define MC0_CH0_CR_MRS_FSM_CONTROL_add_initial_vref_WID              ( 1)
  #define MC0_CH0_CR_MRS_FSM_CONTROL_add_initial_vref_MSK              (0x08000000)
  #define MC0_CH0_CR_MRS_FSM_CONTROL_add_initial_vref_MIN              (0)
  #define MC0_CH0_CR_MRS_FSM_CONTROL_add_initial_vref_MAX              (1) // 0x00000001
  #define MC0_CH0_CR_MRS_FSM_CONTROL_add_initial_vref_DEF              (0x00000000)
  #define MC0_CH0_CR_MRS_FSM_CONTROL_add_initial_vref_HSH              (0x4136E5D0)

  #define MC0_CH0_CR_MRS_FSM_CONTROL_LPDDR4_switch_FSP_OFF             (28)
  #define MC0_CH0_CR_MRS_FSM_CONTROL_LPDDR4_switch_FSP_WID             ( 1)
  #define MC0_CH0_CR_MRS_FSM_CONTROL_LPDDR4_switch_FSP_MSK             (0x10000000)
  #define MC0_CH0_CR_MRS_FSM_CONTROL_LPDDR4_switch_FSP_MIN             (0)
  #define MC0_CH0_CR_MRS_FSM_CONTROL_LPDDR4_switch_FSP_MAX             (1) // 0x00000001
  #define MC0_CH0_CR_MRS_FSM_CONTROL_LPDDR4_switch_FSP_DEF             (0x00000000)
  #define MC0_CH0_CR_MRS_FSM_CONTROL_LPDDR4_switch_FSP_HSH             (0x4138E5D0)

  #define MC0_CH0_CR_MRS_FSM_CONTROL_do_dq_osc_start_OFF               (29)
  #define MC0_CH0_CR_MRS_FSM_CONTROL_do_dq_osc_start_WID               ( 1)
  #define MC0_CH0_CR_MRS_FSM_CONTROL_do_dq_osc_start_MSK               (0x20000000)
  #define MC0_CH0_CR_MRS_FSM_CONTROL_do_dq_osc_start_MIN               (0)
  #define MC0_CH0_CR_MRS_FSM_CONTROL_do_dq_osc_start_MAX               (1) // 0x00000001
  #define MC0_CH0_CR_MRS_FSM_CONTROL_do_dq_osc_start_DEF               (0x00000000)
  #define MC0_CH0_CR_MRS_FSM_CONTROL_do_dq_osc_start_HSH               (0x413AE5D0)

  #define MC0_CH0_CR_MRS_FSM_CONTROL_GV_auto_enable_OFF                (30)
  #define MC0_CH0_CR_MRS_FSM_CONTROL_GV_auto_enable_WID                ( 1)
  #define MC0_CH0_CR_MRS_FSM_CONTROL_GV_auto_enable_MSK                (0x40000000)
  #define MC0_CH0_CR_MRS_FSM_CONTROL_GV_auto_enable_MIN                (0)
  #define MC0_CH0_CR_MRS_FSM_CONTROL_GV_auto_enable_MAX                (1) // 0x00000001
  #define MC0_CH0_CR_MRS_FSM_CONTROL_GV_auto_enable_DEF                (0x00000000)
  #define MC0_CH0_CR_MRS_FSM_CONTROL_GV_auto_enable_HSH                (0x413CE5D0)

  #define MC0_CH0_CR_MRS_FSM_CONTROL_LPDDR4_split_transition_OFF       (31)
  #define MC0_CH0_CR_MRS_FSM_CONTROL_LPDDR4_split_transition_WID       ( 1)
  #define MC0_CH0_CR_MRS_FSM_CONTROL_LPDDR4_split_transition_MSK       (0x80000000)
  #define MC0_CH0_CR_MRS_FSM_CONTROL_LPDDR4_split_transition_MIN       (0)
  #define MC0_CH0_CR_MRS_FSM_CONTROL_LPDDR4_split_transition_MAX       (1) // 0x00000001
  #define MC0_CH0_CR_MRS_FSM_CONTROL_LPDDR4_split_transition_DEF       (0x00000000)
  #define MC0_CH0_CR_MRS_FSM_CONTROL_LPDDR4_split_transition_HSH       (0x413EE5D0)

  #define MC0_CH0_CR_MRS_FSM_CONTROL_tVREFDQ_OFF                       (32)
  #define MC0_CH0_CR_MRS_FSM_CONTROL_tVREFDQ_WID                       (10)
  #define MC0_CH0_CR_MRS_FSM_CONTROL_tVREFDQ_MSK                       (0x000003FF00000000ULL)
  #define MC0_CH0_CR_MRS_FSM_CONTROL_tVREFDQ_MIN                       (0)
  #define MC0_CH0_CR_MRS_FSM_CONTROL_tVREFDQ_MAX                       (1023) // 0x000003FF
  #define MC0_CH0_CR_MRS_FSM_CONTROL_tVREFDQ_DEF                       (0x00000000)
  #define MC0_CH0_CR_MRS_FSM_CONTROL_tVREFDQ_HSH                       (0x4A40E5D0)

  #define MC0_CH0_CR_MRS_FSM_CONTROL_GENERIC_MRS_FSM_Device_Loop_Enable_OFF (42)
  #define MC0_CH0_CR_MRS_FSM_CONTROL_GENERIC_MRS_FSM_Device_Loop_Enable_WID ( 1)
  #define MC0_CH0_CR_MRS_FSM_CONTROL_GENERIC_MRS_FSM_Device_Loop_Enable_MSK (0x0000040000000000ULL)
  #define MC0_CH0_CR_MRS_FSM_CONTROL_GENERIC_MRS_FSM_Device_Loop_Enable_MIN (0)
  #define MC0_CH0_CR_MRS_FSM_CONTROL_GENERIC_MRS_FSM_Device_Loop_Enable_MAX (1) // 0x00000001
  #define MC0_CH0_CR_MRS_FSM_CONTROL_GENERIC_MRS_FSM_Device_Loop_Enable_DEF (0x00000000)
  #define MC0_CH0_CR_MRS_FSM_CONTROL_GENERIC_MRS_FSM_Device_Loop_Enable_HSH (0x4154E5D0)

  #define MC0_CH0_CR_MRS_FSM_CONTROL_do_PDA_for_ECC_OFF                (43)
  #define MC0_CH0_CR_MRS_FSM_CONTROL_do_PDA_for_ECC_WID                ( 1)
  #define MC0_CH0_CR_MRS_FSM_CONTROL_do_PDA_for_ECC_MSK                (0x0000080000000000ULL)
  #define MC0_CH0_CR_MRS_FSM_CONTROL_do_PDA_for_ECC_MIN                (0)
  #define MC0_CH0_CR_MRS_FSM_CONTROL_do_PDA_for_ECC_MAX                (1) // 0x00000001
  #define MC0_CH0_CR_MRS_FSM_CONTROL_do_PDA_for_ECC_DEF                (0x00000000)
  #define MC0_CH0_CR_MRS_FSM_CONTROL_do_PDA_for_ECC_HSH                (0x4156E5D0)

  #define MC0_CH0_CR_MRS_FSM_CONTROL_DDR4_Restore_MR5_Per_Device_OFF   (44)
  #define MC0_CH0_CR_MRS_FSM_CONTROL_DDR4_Restore_MR5_Per_Device_WID   ( 1)
  #define MC0_CH0_CR_MRS_FSM_CONTROL_DDR4_Restore_MR5_Per_Device_MSK   (0x0000100000000000ULL)
  #define MC0_CH0_CR_MRS_FSM_CONTROL_DDR4_Restore_MR5_Per_Device_MIN   (0)
  #define MC0_CH0_CR_MRS_FSM_CONTROL_DDR4_Restore_MR5_Per_Device_MAX   (1) // 0x00000001
  #define MC0_CH0_CR_MRS_FSM_CONTROL_DDR4_Restore_MR5_Per_Device_DEF   (0x00000000)
  #define MC0_CH0_CR_MRS_FSM_CONTROL_DDR4_Restore_MR5_Per_Device_HSH   (0x4158E5D0)

  #define MC0_CH0_CR_MRS_FSM_CONTROL_TwoDPC_support_OFF                (45)
  #define MC0_CH0_CR_MRS_FSM_CONTROL_TwoDPC_support_WID                ( 1)
  #define MC0_CH0_CR_MRS_FSM_CONTROL_TwoDPC_support_MSK                (0x0000200000000000ULL)
  #define MC0_CH0_CR_MRS_FSM_CONTROL_TwoDPC_support_MIN                (0)
  #define MC0_CH0_CR_MRS_FSM_CONTROL_TwoDPC_support_MAX                (1) // 0x00000001
  #define MC0_CH0_CR_MRS_FSM_CONTROL_TwoDPC_support_DEF                (0x00000000)
  #define MC0_CH0_CR_MRS_FSM_CONTROL_TwoDPC_support_HSH                (0x415AE5D0)

  #define MC0_CH0_CR_MRS_FSM_CONTROL_DDR4_Restore_MR1_RTTNOM_Per_Device_OFF (46)
  #define MC0_CH0_CR_MRS_FSM_CONTROL_DDR4_Restore_MR1_RTTNOM_Per_Device_WID ( 1)
  #define MC0_CH0_CR_MRS_FSM_CONTROL_DDR4_Restore_MR1_RTTNOM_Per_Device_MSK (0x0000400000000000ULL)
  #define MC0_CH0_CR_MRS_FSM_CONTROL_DDR4_Restore_MR1_RTTNOM_Per_Device_MIN (0)
  #define MC0_CH0_CR_MRS_FSM_CONTROL_DDR4_Restore_MR1_RTTNOM_Per_Device_MAX (1) // 0x00000001
  #define MC0_CH0_CR_MRS_FSM_CONTROL_DDR4_Restore_MR1_RTTNOM_Per_Device_DEF (0x00000000)
  #define MC0_CH0_CR_MRS_FSM_CONTROL_DDR4_Restore_MR1_RTTNOM_Per_Device_HSH (0x415CE5D0)

  #define MC0_CH0_CR_MRS_FSM_CONTROL_Fast_PDA_OFF                      (47)
  #define MC0_CH0_CR_MRS_FSM_CONTROL_Fast_PDA_WID                      ( 1)
  #define MC0_CH0_CR_MRS_FSM_CONTROL_Fast_PDA_MSK                      (0x0000800000000000ULL)
  #define MC0_CH0_CR_MRS_FSM_CONTROL_Fast_PDA_MIN                      (0)
  #define MC0_CH0_CR_MRS_FSM_CONTROL_Fast_PDA_MAX                      (1) // 0x00000001
  #define MC0_CH0_CR_MRS_FSM_CONTROL_Fast_PDA_DEF                      (0x00000000)
  #define MC0_CH0_CR_MRS_FSM_CONTROL_Fast_PDA_HSH                      (0x415EE5D0)

  #define MC0_CH0_CR_MRS_FSM_CONTROL_always_count_tFC_OFF              (48)
  #define MC0_CH0_CR_MRS_FSM_CONTROL_always_count_tFC_WID              ( 1)
  #define MC0_CH0_CR_MRS_FSM_CONTROL_always_count_tFC_MSK              (0x0001000000000000ULL)
  #define MC0_CH0_CR_MRS_FSM_CONTROL_always_count_tFC_MIN              (0)
  #define MC0_CH0_CR_MRS_FSM_CONTROL_always_count_tFC_MAX              (1) // 0x00000001
  #define MC0_CH0_CR_MRS_FSM_CONTROL_always_count_tFC_DEF              (0x00000000)
  #define MC0_CH0_CR_MRS_FSM_CONTROL_always_count_tFC_HSH              (0x4160E5D0)

  #define MC0_CH0_CR_MRS_FSM_CONTROL_PDA_wr_delay_OFF                  (49)
  #define MC0_CH0_CR_MRS_FSM_CONTROL_PDA_wr_delay_WID                  ( 6)
  #define MC0_CH0_CR_MRS_FSM_CONTROL_PDA_wr_delay_MSK                  (0x007E000000000000ULL)
  #define MC0_CH0_CR_MRS_FSM_CONTROL_PDA_wr_delay_MIN                  (0)
  #define MC0_CH0_CR_MRS_FSM_CONTROL_PDA_wr_delay_MAX                  (63) // 0x0000003F
  #define MC0_CH0_CR_MRS_FSM_CONTROL_PDA_wr_delay_DEF                  (0x00000000)
  #define MC0_CH0_CR_MRS_FSM_CONTROL_PDA_wr_delay_HSH                  (0x4662E5D0)

  #define MC0_CH0_CR_MRS_FSM_CONTROL_LPDDR4_per_byte_vref_OFF          (55)
  #define MC0_CH0_CR_MRS_FSM_CONTROL_LPDDR4_per_byte_vref_WID          ( 1)
  #define MC0_CH0_CR_MRS_FSM_CONTROL_LPDDR4_per_byte_vref_MSK          (0x0080000000000000ULL)
  #define MC0_CH0_CR_MRS_FSM_CONTROL_LPDDR4_per_byte_vref_MIN          (0)
  #define MC0_CH0_CR_MRS_FSM_CONTROL_LPDDR4_per_byte_vref_MAX          (1) // 0x00000001
  #define MC0_CH0_CR_MRS_FSM_CONTROL_LPDDR4_per_byte_vref_DEF          (0x00000000)
  #define MC0_CH0_CR_MRS_FSM_CONTROL_LPDDR4_per_byte_vref_HSH          (0x416EE5D0)

  #define MC0_CH0_CR_MRS_FSM_CONTROL_Clear_VRCG_OFF                    (56)
  #define MC0_CH0_CR_MRS_FSM_CONTROL_Clear_VRCG_WID                    ( 1)
  #define MC0_CH0_CR_MRS_FSM_CONTROL_Clear_VRCG_MSK                    (0x0100000000000000ULL)
  #define MC0_CH0_CR_MRS_FSM_CONTROL_Clear_VRCG_MIN                    (0)
  #define MC0_CH0_CR_MRS_FSM_CONTROL_Clear_VRCG_MAX                    (1) // 0x00000001
  #define MC0_CH0_CR_MRS_FSM_CONTROL_Clear_VRCG_DEF                    (0x00000000)
  #define MC0_CH0_CR_MRS_FSM_CONTROL_Clear_VRCG_HSH                    (0x4170E5D0)

  #define MC0_CH0_CR_MRS_FSM_CONTROL_GENERIC_MRS_FSM_Enable_OFF        (57)
  #define MC0_CH0_CR_MRS_FSM_CONTROL_GENERIC_MRS_FSM_Enable_WID        ( 1)
  #define MC0_CH0_CR_MRS_FSM_CONTROL_GENERIC_MRS_FSM_Enable_MSK        (0x0200000000000000ULL)
  #define MC0_CH0_CR_MRS_FSM_CONTROL_GENERIC_MRS_FSM_Enable_MIN        (0)
  #define MC0_CH0_CR_MRS_FSM_CONTROL_GENERIC_MRS_FSM_Enable_MAX        (1) // 0x00000001
  #define MC0_CH0_CR_MRS_FSM_CONTROL_GENERIC_MRS_FSM_Enable_DEF        (0x00000000)
  #define MC0_CH0_CR_MRS_FSM_CONTROL_GENERIC_MRS_FSM_Enable_HSH        (0x4172E5D0)

  #define MC0_CH0_CR_MRS_FSM_CONTROL_GENERIC_MRS_FSM_Breakpoint_Address_OFF (58)
  #define MC0_CH0_CR_MRS_FSM_CONTROL_GENERIC_MRS_FSM_Breakpoint_Address_WID ( 5)
  #define MC0_CH0_CR_MRS_FSM_CONTROL_GENERIC_MRS_FSM_Breakpoint_Address_MSK (0x7C00000000000000ULL)
  #define MC0_CH0_CR_MRS_FSM_CONTROL_GENERIC_MRS_FSM_Breakpoint_Address_MIN (0)
  #define MC0_CH0_CR_MRS_FSM_CONTROL_GENERIC_MRS_FSM_Breakpoint_Address_MAX (31) // 0x0000001F
  #define MC0_CH0_CR_MRS_FSM_CONTROL_GENERIC_MRS_FSM_Breakpoint_Address_DEF (0x00000000)
  #define MC0_CH0_CR_MRS_FSM_CONTROL_GENERIC_MRS_FSM_Breakpoint_Address_HSH (0x4574E5D0)

  #define MC0_CH0_CR_MRS_FSM_CONTROL_cbit_count_each_rank_OFF          (63)
  #define MC0_CH0_CR_MRS_FSM_CONTROL_cbit_count_each_rank_WID          ( 1)
  #define MC0_CH0_CR_MRS_FSM_CONTROL_cbit_count_each_rank_MSK          (0x8000000000000000ULL)
  #define MC0_CH0_CR_MRS_FSM_CONTROL_cbit_count_each_rank_MIN          (0)
  #define MC0_CH0_CR_MRS_FSM_CONTROL_cbit_count_each_rank_MAX          (1) // 0x00000001
  #define MC0_CH0_CR_MRS_FSM_CONTROL_cbit_count_each_rank_DEF          (0x00000000)
  #define MC0_CH0_CR_MRS_FSM_CONTROL_cbit_count_each_rank_HSH          (0x417EE5D0)

#define MC0_CH0_CR_MRS_FSM_RUN_REG                                     (0x0000E5D8)

  #define MC0_CH0_CR_MRS_FSM_RUN_Run_OFF                               ( 0)
  #define MC0_CH0_CR_MRS_FSM_RUN_Run_WID                               ( 1)
  #define MC0_CH0_CR_MRS_FSM_RUN_Run_MSK                               (0x00000001)
  #define MC0_CH0_CR_MRS_FSM_RUN_Run_MIN                               (0)
  #define MC0_CH0_CR_MRS_FSM_RUN_Run_MAX                               (1) // 0x00000001
  #define MC0_CH0_CR_MRS_FSM_RUN_Run_DEF                               (0x00000000)
  #define MC0_CH0_CR_MRS_FSM_RUN_Run_HSH                               (0x0100E5D8)

#define MC0_CH0_CR_DDR4_MR1_ODIC_VALUES_REG                            (0x0000E5DC)
//Duplicate of MC0_CH0_CR_DDR4_MR1_ODIC_DIMM1_VALUES_REG

#define MC0_CH0_CR_PL_AGENT_CFG_DTF_REG                                (0x0000E5E0)

  #define MC0_CH0_CR_PL_AGENT_CFG_DTF_data_trace_mode_OFF              ( 0)
  #define MC0_CH0_CR_PL_AGENT_CFG_DTF_data_trace_mode_WID              ( 2)
  #define MC0_CH0_CR_PL_AGENT_CFG_DTF_data_trace_mode_MSK              (0x00000003)
  #define MC0_CH0_CR_PL_AGENT_CFG_DTF_data_trace_mode_MIN              (0)
  #define MC0_CH0_CR_PL_AGENT_CFG_DTF_data_trace_mode_MAX              (3) // 0x00000003
  #define MC0_CH0_CR_PL_AGENT_CFG_DTF_data_trace_mode_DEF              (0x00000000)
  #define MC0_CH0_CR_PL_AGENT_CFG_DTF_data_trace_mode_HSH              (0x0200E5E0)

  #define MC0_CH0_CR_PL_AGENT_CFG_DTF_UI_to_trace_OFF                  ( 2)
  #define MC0_CH0_CR_PL_AGENT_CFG_DTF_UI_to_trace_WID                  ( 3)
  #define MC0_CH0_CR_PL_AGENT_CFG_DTF_UI_to_trace_MSK                  (0x0000001C)
  #define MC0_CH0_CR_PL_AGENT_CFG_DTF_UI_to_trace_MIN                  (0)
  #define MC0_CH0_CR_PL_AGENT_CFG_DTF_UI_to_trace_MAX                  (7) // 0x00000007
  #define MC0_CH0_CR_PL_AGENT_CFG_DTF_UI_to_trace_DEF                  (0x00000000)
  #define MC0_CH0_CR_PL_AGENT_CFG_DTF_UI_to_trace_HSH                  (0x0304E5E0)

  #define MC0_CH0_CR_PL_AGENT_CFG_DTF_byte_to_trace_OFF                ( 5)
  #define MC0_CH0_CR_PL_AGENT_CFG_DTF_byte_to_trace_WID                ( 3)
  #define MC0_CH0_CR_PL_AGENT_CFG_DTF_byte_to_trace_MSK                (0x000000E0)
  #define MC0_CH0_CR_PL_AGENT_CFG_DTF_byte_to_trace_MIN                (0)
  #define MC0_CH0_CR_PL_AGENT_CFG_DTF_byte_to_trace_MAX                (7) // 0x00000007
  #define MC0_CH0_CR_PL_AGENT_CFG_DTF_byte_to_trace_DEF                (0x00000000)
  #define MC0_CH0_CR_PL_AGENT_CFG_DTF_byte_to_trace_HSH                (0x030AE5E0)

  #define MC0_CH0_CR_PL_AGENT_CFG_DTF_ECC_EN_OFF                       ( 8)
  #define MC0_CH0_CR_PL_AGENT_CFG_DTF_ECC_EN_WID                       ( 1)
  #define MC0_CH0_CR_PL_AGENT_CFG_DTF_ECC_EN_MSK                       (0x00000100)
  #define MC0_CH0_CR_PL_AGENT_CFG_DTF_ECC_EN_MIN                       (0)
  #define MC0_CH0_CR_PL_AGENT_CFG_DTF_ECC_EN_MAX                       (1) // 0x00000001
  #define MC0_CH0_CR_PL_AGENT_CFG_DTF_ECC_EN_DEF                       (0x00000000)
  #define MC0_CH0_CR_PL_AGENT_CFG_DTF_ECC_EN_HSH                       (0x0110E5E0)

  #define MC0_CH0_CR_PL_AGENT_CFG_DTF_ECC_BYTE_replace_OFF             ( 9)
  #define MC0_CH0_CR_PL_AGENT_CFG_DTF_ECC_BYTE_replace_WID             ( 3)
  #define MC0_CH0_CR_PL_AGENT_CFG_DTF_ECC_BYTE_replace_MSK             (0x00000E00)
  #define MC0_CH0_CR_PL_AGENT_CFG_DTF_ECC_BYTE_replace_MIN             (0)
  #define MC0_CH0_CR_PL_AGENT_CFG_DTF_ECC_BYTE_replace_MAX             (7) // 0x00000007
  #define MC0_CH0_CR_PL_AGENT_CFG_DTF_ECC_BYTE_replace_DEF             (0x00000000)
  #define MC0_CH0_CR_PL_AGENT_CFG_DTF_ECC_BYTE_replace_HSH             (0x0312E5E0)

  #define MC0_CH0_CR_PL_AGENT_CFG_DTF_trace_read_data_OFF              (12)
  #define MC0_CH0_CR_PL_AGENT_CFG_DTF_trace_read_data_WID              ( 1)
  #define MC0_CH0_CR_PL_AGENT_CFG_DTF_trace_read_data_MSK              (0x00001000)
  #define MC0_CH0_CR_PL_AGENT_CFG_DTF_trace_read_data_MIN              (0)
  #define MC0_CH0_CR_PL_AGENT_CFG_DTF_trace_read_data_MAX              (1) // 0x00000001
  #define MC0_CH0_CR_PL_AGENT_CFG_DTF_trace_read_data_DEF              (0x00000000)
  #define MC0_CH0_CR_PL_AGENT_CFG_DTF_trace_read_data_HSH              (0x0118E5E0)

  #define MC0_CH0_CR_PL_AGENT_CFG_DTF_trace_write_data_OFF             (13)
  #define MC0_CH0_CR_PL_AGENT_CFG_DTF_trace_write_data_WID             ( 1)
  #define MC0_CH0_CR_PL_AGENT_CFG_DTF_trace_write_data_MSK             (0x00002000)
  #define MC0_CH0_CR_PL_AGENT_CFG_DTF_trace_write_data_MIN             (0)
  #define MC0_CH0_CR_PL_AGENT_CFG_DTF_trace_write_data_MAX             (1) // 0x00000001
  #define MC0_CH0_CR_PL_AGENT_CFG_DTF_trace_write_data_DEF             (0x00000001)
  #define MC0_CH0_CR_PL_AGENT_CFG_DTF_trace_write_data_HSH             (0x011AE5E0)

  #define MC0_CH0_CR_PL_AGENT_CFG_DTF_DDRPL_Activate_OFF               (14)
  #define MC0_CH0_CR_PL_AGENT_CFG_DTF_DDRPL_Activate_WID               ( 1)
  #define MC0_CH0_CR_PL_AGENT_CFG_DTF_DDRPL_Activate_MSK               (0x00004000)
  #define MC0_CH0_CR_PL_AGENT_CFG_DTF_DDRPL_Activate_MIN               (0)
  #define MC0_CH0_CR_PL_AGENT_CFG_DTF_DDRPL_Activate_MAX               (1) // 0x00000001
  #define MC0_CH0_CR_PL_AGENT_CFG_DTF_DDRPL_Activate_DEF               (0x00000000)
  #define MC0_CH0_CR_PL_AGENT_CFG_DTF_DDRPL_Activate_HSH               (0x011CE5E0)

  #define MC0_CH0_CR_PL_AGENT_CFG_DTF_SCHTrace_OFF                     (15)
  #define MC0_CH0_CR_PL_AGENT_CFG_DTF_SCHTrace_WID                     ( 1)
  #define MC0_CH0_CR_PL_AGENT_CFG_DTF_SCHTrace_MSK                     (0x00008000)
  #define MC0_CH0_CR_PL_AGENT_CFG_DTF_SCHTrace_MIN                     (0)
  #define MC0_CH0_CR_PL_AGENT_CFG_DTF_SCHTrace_MAX                     (1) // 0x00000001
  #define MC0_CH0_CR_PL_AGENT_CFG_DTF_SCHTrace_DEF                     (0x00000000)
  #define MC0_CH0_CR_PL_AGENT_CFG_DTF_SCHTrace_HSH                     (0x011EE5E0)

  #define MC0_CH0_CR_PL_AGENT_CFG_DTF_dis_early_ind_override_OFF       (16)
  #define MC0_CH0_CR_PL_AGENT_CFG_DTF_dis_early_ind_override_WID       ( 1)
  #define MC0_CH0_CR_PL_AGENT_CFG_DTF_dis_early_ind_override_MSK       (0x00010000)
  #define MC0_CH0_CR_PL_AGENT_CFG_DTF_dis_early_ind_override_MIN       (0)
  #define MC0_CH0_CR_PL_AGENT_CFG_DTF_dis_early_ind_override_MAX       (1) // 0x00000001
  #define MC0_CH0_CR_PL_AGENT_CFG_DTF_dis_early_ind_override_DEF       (0x00000000)
  #define MC0_CH0_CR_PL_AGENT_CFG_DTF_dis_early_ind_override_HSH       (0x0120E5E0)

  #define MC0_CH0_CR_PL_AGENT_CFG_DTF_ddrpl_data_trace_width_OFF       (17)
  #define MC0_CH0_CR_PL_AGENT_CFG_DTF_ddrpl_data_trace_width_WID       ( 2)
  #define MC0_CH0_CR_PL_AGENT_CFG_DTF_ddrpl_data_trace_width_MSK       (0x00060000)
  #define MC0_CH0_CR_PL_AGENT_CFG_DTF_ddrpl_data_trace_width_MIN       (0)
  #define MC0_CH0_CR_PL_AGENT_CFG_DTF_ddrpl_data_trace_width_MAX       (3) // 0x00000003
  #define MC0_CH0_CR_PL_AGENT_CFG_DTF_ddrpl_data_trace_width_DEF       (0x00000000)
  #define MC0_CH0_CR_PL_AGENT_CFG_DTF_ddrpl_data_trace_width_HSH       (0x0222E5E0)

  #define MC0_CH0_CR_PL_AGENT_CFG_DTF_data_quarter_mode_byte_select_OFF (19)
  #define MC0_CH0_CR_PL_AGENT_CFG_DTF_data_quarter_mode_byte_select_WID ( 2)
  #define MC0_CH0_CR_PL_AGENT_CFG_DTF_data_quarter_mode_byte_select_MSK (0x00180000)
  #define MC0_CH0_CR_PL_AGENT_CFG_DTF_data_quarter_mode_byte_select_MIN (0)
  #define MC0_CH0_CR_PL_AGENT_CFG_DTF_data_quarter_mode_byte_select_MAX (3) // 0x00000003
  #define MC0_CH0_CR_PL_AGENT_CFG_DTF_data_quarter_mode_byte_select_DEF (0x00000000)
  #define MC0_CH0_CR_PL_AGENT_CFG_DTF_data_quarter_mode_byte_select_HSH (0x0226E5E0)

  #define MC0_CH0_CR_PL_AGENT_CFG_DTF_data_half_mode_byte_select_OFF   (21)
  #define MC0_CH0_CR_PL_AGENT_CFG_DTF_data_half_mode_byte_select_WID   ( 1)
  #define MC0_CH0_CR_PL_AGENT_CFG_DTF_data_half_mode_byte_select_MSK   (0x00200000)
  #define MC0_CH0_CR_PL_AGENT_CFG_DTF_data_half_mode_byte_select_MIN   (0)
  #define MC0_CH0_CR_PL_AGENT_CFG_DTF_data_half_mode_byte_select_MAX   (1) // 0x00000001
  #define MC0_CH0_CR_PL_AGENT_CFG_DTF_data_half_mode_byte_select_DEF   (0x00000000)
  #define MC0_CH0_CR_PL_AGENT_CFG_DTF_data_half_mode_byte_select_HSH   (0x012AE5E0)

#define MC0_CH0_CR_MCMNTS_GLOBAL_DRIVER_GATE_CFG_REG                   (0x0000E5E4)
//Duplicate of MC0_MCMAINS_GLOBAL_DRIVER_GATE_CFG_REG

#define MC0_CH0_CR_DDR4_ECC_DEVICE_VALUES_REG                          (0x0000E5E8)

  #define MC0_CH0_CR_DDR4_ECC_DEVICE_VALUES_ODIC_Rank0_OFF             ( 0)
  #define MC0_CH0_CR_DDR4_ECC_DEVICE_VALUES_ODIC_Rank0_WID             ( 2)
  #define MC0_CH0_CR_DDR4_ECC_DEVICE_VALUES_ODIC_Rank0_MSK             (0x00000003)
  #define MC0_CH0_CR_DDR4_ECC_DEVICE_VALUES_ODIC_Rank0_MIN             (0)
  #define MC0_CH0_CR_DDR4_ECC_DEVICE_VALUES_ODIC_Rank0_MAX             (3) // 0x00000003
  #define MC0_CH0_CR_DDR4_ECC_DEVICE_VALUES_ODIC_Rank0_DEF             (0x00000000)
  #define MC0_CH0_CR_DDR4_ECC_DEVICE_VALUES_ODIC_Rank0_HSH             (0x4200E5E8)

  #define MC0_CH0_CR_DDR4_ECC_DEVICE_VALUES_ODIC_Rank1_OFF             ( 2)
  #define MC0_CH0_CR_DDR4_ECC_DEVICE_VALUES_ODIC_Rank1_WID             ( 2)
  #define MC0_CH0_CR_DDR4_ECC_DEVICE_VALUES_ODIC_Rank1_MSK             (0x0000000C)
  #define MC0_CH0_CR_DDR4_ECC_DEVICE_VALUES_ODIC_Rank1_MIN             (0)
  #define MC0_CH0_CR_DDR4_ECC_DEVICE_VALUES_ODIC_Rank1_MAX             (3) // 0x00000003
  #define MC0_CH0_CR_DDR4_ECC_DEVICE_VALUES_ODIC_Rank1_DEF             (0x00000000)
  #define MC0_CH0_CR_DDR4_ECC_DEVICE_VALUES_ODIC_Rank1_HSH             (0x4204E5E8)

  #define MC0_CH0_CR_DDR4_ECC_DEVICE_VALUES_RTT_WR_Rank0_OFF           ( 4)
  #define MC0_CH0_CR_DDR4_ECC_DEVICE_VALUES_RTT_WR_Rank0_WID           ( 3)
  #define MC0_CH0_CR_DDR4_ECC_DEVICE_VALUES_RTT_WR_Rank0_MSK           (0x00000070)
  #define MC0_CH0_CR_DDR4_ECC_DEVICE_VALUES_RTT_WR_Rank0_MIN           (0)
  #define MC0_CH0_CR_DDR4_ECC_DEVICE_VALUES_RTT_WR_Rank0_MAX           (7) // 0x00000007
  #define MC0_CH0_CR_DDR4_ECC_DEVICE_VALUES_RTT_WR_Rank0_DEF           (0x00000000)
  #define MC0_CH0_CR_DDR4_ECC_DEVICE_VALUES_RTT_WR_Rank0_HSH           (0x4308E5E8)

  #define MC0_CH0_CR_DDR4_ECC_DEVICE_VALUES_RTT_WR_Rank1_OFF           ( 7)
  #define MC0_CH0_CR_DDR4_ECC_DEVICE_VALUES_RTT_WR_Rank1_WID           ( 3)
  #define MC0_CH0_CR_DDR4_ECC_DEVICE_VALUES_RTT_WR_Rank1_MSK           (0x00000380)
  #define MC0_CH0_CR_DDR4_ECC_DEVICE_VALUES_RTT_WR_Rank1_MIN           (0)
  #define MC0_CH0_CR_DDR4_ECC_DEVICE_VALUES_RTT_WR_Rank1_MAX           (7) // 0x00000007
  #define MC0_CH0_CR_DDR4_ECC_DEVICE_VALUES_RTT_WR_Rank1_DEF           (0x00000000)
  #define MC0_CH0_CR_DDR4_ECC_DEVICE_VALUES_RTT_WR_Rank1_HSH           (0x430EE5E8)

  #define MC0_CH0_CR_DDR4_ECC_DEVICE_VALUES_VREF_Rank0_OFF             (10)
  #define MC0_CH0_CR_DDR4_ECC_DEVICE_VALUES_VREF_Rank0_WID             ( 7)
  #define MC0_CH0_CR_DDR4_ECC_DEVICE_VALUES_VREF_Rank0_MSK             (0x0001FC00)
  #define MC0_CH0_CR_DDR4_ECC_DEVICE_VALUES_VREF_Rank0_MIN             (0)
  #define MC0_CH0_CR_DDR4_ECC_DEVICE_VALUES_VREF_Rank0_MAX             (127) // 0x0000007F
  #define MC0_CH0_CR_DDR4_ECC_DEVICE_VALUES_VREF_Rank0_DEF             (0x00000000)
  #define MC0_CH0_CR_DDR4_ECC_DEVICE_VALUES_VREF_Rank0_HSH             (0x4714E5E8)

  #define MC0_CH0_CR_DDR4_ECC_DEVICE_VALUES_VREF_Rank1_OFF             (17)
  #define MC0_CH0_CR_DDR4_ECC_DEVICE_VALUES_VREF_Rank1_WID             ( 7)
  #define MC0_CH0_CR_DDR4_ECC_DEVICE_VALUES_VREF_Rank1_MSK             (0x00FE0000)
  #define MC0_CH0_CR_DDR4_ECC_DEVICE_VALUES_VREF_Rank1_MIN             (0)
  #define MC0_CH0_CR_DDR4_ECC_DEVICE_VALUES_VREF_Rank1_MAX             (127) // 0x0000007F
  #define MC0_CH0_CR_DDR4_ECC_DEVICE_VALUES_VREF_Rank1_DEF             (0x00000000)
  #define MC0_CH0_CR_DDR4_ECC_DEVICE_VALUES_VREF_Rank1_HSH             (0x4722E5E8)

  #define MC0_CH0_CR_DDR4_ECC_DEVICE_VALUES_RTT_NOM_Rank0_OFF          (24)
  #define MC0_CH0_CR_DDR4_ECC_DEVICE_VALUES_RTT_NOM_Rank0_WID          ( 3)
  #define MC0_CH0_CR_DDR4_ECC_DEVICE_VALUES_RTT_NOM_Rank0_MSK          (0x07000000)
  #define MC0_CH0_CR_DDR4_ECC_DEVICE_VALUES_RTT_NOM_Rank0_MIN          (0)
  #define MC0_CH0_CR_DDR4_ECC_DEVICE_VALUES_RTT_NOM_Rank0_MAX          (7) // 0x00000007
  #define MC0_CH0_CR_DDR4_ECC_DEVICE_VALUES_RTT_NOM_Rank0_DEF          (0x00000000)
  #define MC0_CH0_CR_DDR4_ECC_DEVICE_VALUES_RTT_NOM_Rank0_HSH          (0x4330E5E8)

  #define MC0_CH0_CR_DDR4_ECC_DEVICE_VALUES_RTT_NOM_Rank1_OFF          (27)
  #define MC0_CH0_CR_DDR4_ECC_DEVICE_VALUES_RTT_NOM_Rank1_WID          ( 3)
  #define MC0_CH0_CR_DDR4_ECC_DEVICE_VALUES_RTT_NOM_Rank1_MSK          (0x38000000)
  #define MC0_CH0_CR_DDR4_ECC_DEVICE_VALUES_RTT_NOM_Rank1_MIN          (0)
  #define MC0_CH0_CR_DDR4_ECC_DEVICE_VALUES_RTT_NOM_Rank1_MAX          (7) // 0x00000007
  #define MC0_CH0_CR_DDR4_ECC_DEVICE_VALUES_RTT_NOM_Rank1_DEF          (0x00000000)
  #define MC0_CH0_CR_DDR4_ECC_DEVICE_VALUES_RTT_NOM_Rank1_HSH          (0x4336E5E8)

  #define MC0_CH0_CR_DDR4_ECC_DEVICE_VALUES_RTT_PARK_Rank0_OFF         (32)
  #define MC0_CH0_CR_DDR4_ECC_DEVICE_VALUES_RTT_PARK_Rank0_WID         ( 3)
  #define MC0_CH0_CR_DDR4_ECC_DEVICE_VALUES_RTT_PARK_Rank0_MSK         (0x0000000700000000ULL)
  #define MC0_CH0_CR_DDR4_ECC_DEVICE_VALUES_RTT_PARK_Rank0_MIN         (0)
  #define MC0_CH0_CR_DDR4_ECC_DEVICE_VALUES_RTT_PARK_Rank0_MAX         (7) // 0x00000007
  #define MC0_CH0_CR_DDR4_ECC_DEVICE_VALUES_RTT_PARK_Rank0_DEF         (0x00000000)
  #define MC0_CH0_CR_DDR4_ECC_DEVICE_VALUES_RTT_PARK_Rank0_HSH         (0x4340E5E8)

  #define MC0_CH0_CR_DDR4_ECC_DEVICE_VALUES_RTT_PARK_Rank1_OFF         (35)
  #define MC0_CH0_CR_DDR4_ECC_DEVICE_VALUES_RTT_PARK_Rank1_WID         ( 3)
  #define MC0_CH0_CR_DDR4_ECC_DEVICE_VALUES_RTT_PARK_Rank1_MSK         (0x0000003800000000ULL)
  #define MC0_CH0_CR_DDR4_ECC_DEVICE_VALUES_RTT_PARK_Rank1_MIN         (0)
  #define MC0_CH0_CR_DDR4_ECC_DEVICE_VALUES_RTT_PARK_Rank1_MAX         (7) // 0x00000007
  #define MC0_CH0_CR_DDR4_ECC_DEVICE_VALUES_RTT_PARK_Rank1_DEF         (0x00000000)
  #define MC0_CH0_CR_DDR4_ECC_DEVICE_VALUES_RTT_PARK_Rank1_HSH         (0x4346E5E8)

#define MC0_CH0_CR_DDR4_ECC_DEVICE_DIMM1_VALUES_REG                    (0x0000E5F0)
//Duplicate of MC0_CH0_CR_DDR4_ECC_DEVICE_VALUES_REG

#define MC0_CH0_CR_MCMNTS_SPARE2_REG                                   (0x0000E5F8)

  #define MC0_CH0_CR_MCMNTS_SPARE2_Spare_RW_OFF                        ( 0)
  #define MC0_CH0_CR_MCMNTS_SPARE2_Spare_RW_WID                        (16)
  #define MC0_CH0_CR_MCMNTS_SPARE2_Spare_RW_MSK                        (0x0000FFFF)
  #define MC0_CH0_CR_MCMNTS_SPARE2_Spare_RW_MIN                        (0)
  #define MC0_CH0_CR_MCMNTS_SPARE2_Spare_RW_MAX                        (65535) // 0x0000FFFF
  #define MC0_CH0_CR_MCMNTS_SPARE2_Spare_RW_DEF                        (0x00000000)
  #define MC0_CH0_CR_MCMNTS_SPARE2_Spare_RW_HSH                        (0x1000E5F8)

  #define MC0_CH0_CR_MCMNTS_SPARE2_Spare_RW_V_OFF                      (16)
  #define MC0_CH0_CR_MCMNTS_SPARE2_Spare_RW_V_WID                      (16)
  #define MC0_CH0_CR_MCMNTS_SPARE2_Spare_RW_V_MSK                      (0xFFFF0000)
  #define MC0_CH0_CR_MCMNTS_SPARE2_Spare_RW_V_MIN                      (0)
  #define MC0_CH0_CR_MCMNTS_SPARE2_Spare_RW_V_MAX                      (65535) // 0x0000FFFF
  #define MC0_CH0_CR_MCMNTS_SPARE2_Spare_RW_V_DEF                      (0x00000000)
  #define MC0_CH0_CR_MCMNTS_SPARE2_Spare_RW_V_HSH                      (0x1020E5F8)

#define MC0_CH0_CR_MCMNTS_SPARE_REG                                    (0x0000E5FC)

  #define MC0_CH0_CR_MCMNTS_SPARE_Spare_RW_OFF                         ( 0)
  #define MC0_CH0_CR_MCMNTS_SPARE_Spare_RW_WID                         ( 8)
  #define MC0_CH0_CR_MCMNTS_SPARE_Spare_RW_MSK                         (0x000000FF)
  #define MC0_CH0_CR_MCMNTS_SPARE_Spare_RW_MIN                         (0)
  #define MC0_CH0_CR_MCMNTS_SPARE_Spare_RW_MAX                         (255) // 0x000000FF
  #define MC0_CH0_CR_MCMNTS_SPARE_Spare_RW_DEF                         (0x00000000)
  #define MC0_CH0_CR_MCMNTS_SPARE_Spare_RW_HSH                         (0x0800E5FC)

  #define MC0_CH0_CR_MCMNTS_SPARE_ForceX2Ref_OFF                       ( 8)
  #define MC0_CH0_CR_MCMNTS_SPARE_ForceX2Ref_WID                       ( 1)
  #define MC0_CH0_CR_MCMNTS_SPARE_ForceX2Ref_MSK                       (0x00000100)
  #define MC0_CH0_CR_MCMNTS_SPARE_ForceX2Ref_MIN                       (0)
  #define MC0_CH0_CR_MCMNTS_SPARE_ForceX2Ref_MAX                       (1) // 0x00000001
  #define MC0_CH0_CR_MCMNTS_SPARE_ForceX2Ref_DEF                       (0x00000000)
  #define MC0_CH0_CR_MCMNTS_SPARE_ForceX2Ref_HSH                       (0x0110E5FC)

  #define MC0_CH0_CR_MCMNTS_SPARE_ForceX4Ref_OFF                       ( 9)
  #define MC0_CH0_CR_MCMNTS_SPARE_ForceX4Ref_WID                       ( 1)
  #define MC0_CH0_CR_MCMNTS_SPARE_ForceX4Ref_MSK                       (0x00000200)
  #define MC0_CH0_CR_MCMNTS_SPARE_ForceX4Ref_MIN                       (0)
  #define MC0_CH0_CR_MCMNTS_SPARE_ForceX4Ref_MAX                       (1) // 0x00000001
  #define MC0_CH0_CR_MCMNTS_SPARE_ForceX4Ref_DEF                       (0x00000000)
  #define MC0_CH0_CR_MCMNTS_SPARE_ForceX4Ref_HSH                       (0x0112E5FC)

  #define MC0_CH0_CR_MCMNTS_SPARE_DisLowRefRate_OFF                    (10)
  #define MC0_CH0_CR_MCMNTS_SPARE_DisLowRefRate_WID                    ( 1)
  #define MC0_CH0_CR_MCMNTS_SPARE_DisLowRefRate_MSK                    (0x00000400)
  #define MC0_CH0_CR_MCMNTS_SPARE_DisLowRefRate_MIN                    (0)
  #define MC0_CH0_CR_MCMNTS_SPARE_DisLowRefRate_MAX                    (1) // 0x00000001
  #define MC0_CH0_CR_MCMNTS_SPARE_DisLowRefRate_DEF                    (0x00000000)
  #define MC0_CH0_CR_MCMNTS_SPARE_DisLowRefRate_HSH                    (0x0114E5FC)

  #define MC0_CH0_CR_MCMNTS_SPARE_disRegardZQonSRtime_OFF              (11)
  #define MC0_CH0_CR_MCMNTS_SPARE_disRegardZQonSRtime_WID              ( 1)
  #define MC0_CH0_CR_MCMNTS_SPARE_disRegardZQonSRtime_MSK              (0x00000800)
  #define MC0_CH0_CR_MCMNTS_SPARE_disRegardZQonSRtime_MIN              (0)
  #define MC0_CH0_CR_MCMNTS_SPARE_disRegardZQonSRtime_MAX              (1) // 0x00000001
  #define MC0_CH0_CR_MCMNTS_SPARE_disRegardZQonSRtime_DEF              (0x00000000)
  #define MC0_CH0_CR_MCMNTS_SPARE_disRegardZQonSRtime_HSH              (0x0116E5FC)

  #define MC0_CH0_CR_MCMNTS_SPARE_DDR_alert_OFF                        (12)
  #define MC0_CH0_CR_MCMNTS_SPARE_DDR_alert_WID                        ( 1)
  #define MC0_CH0_CR_MCMNTS_SPARE_DDR_alert_MSK                        (0x00001000)
  #define MC0_CH0_CR_MCMNTS_SPARE_DDR_alert_MIN                        (0)
  #define MC0_CH0_CR_MCMNTS_SPARE_DDR_alert_MAX                        (1) // 0x00000001
  #define MC0_CH0_CR_MCMNTS_SPARE_DDR_alert_DEF                        (0x00000000)
  #define MC0_CH0_CR_MCMNTS_SPARE_DDR_alert_HSH                        (0x0118E5FC)

  #define MC0_CH0_CR_MCMNTS_SPARE_DisSREXcnt_OFF                       (13)
  #define MC0_CH0_CR_MCMNTS_SPARE_DisSREXcnt_WID                       ( 1)
  #define MC0_CH0_CR_MCMNTS_SPARE_DisSREXcnt_MSK                       (0x00002000)
  #define MC0_CH0_CR_MCMNTS_SPARE_DisSREXcnt_MIN                       (0)
  #define MC0_CH0_CR_MCMNTS_SPARE_DisSREXcnt_MAX                       (1) // 0x00000001
  #define MC0_CH0_CR_MCMNTS_SPARE_DisSREXcnt_DEF                       (0x00000000)
  #define MC0_CH0_CR_MCMNTS_SPARE_DisSREXcnt_HSH                       (0x011AE5FC)

  #define MC0_CH0_CR_MCMNTS_SPARE_pattern_match_enable_OFF             (14)
  #define MC0_CH0_CR_MCMNTS_SPARE_pattern_match_enable_WID             ( 1)
  #define MC0_CH0_CR_MCMNTS_SPARE_pattern_match_enable_MSK             (0x00004000)
  #define MC0_CH0_CR_MCMNTS_SPARE_pattern_match_enable_MIN             (0)
  #define MC0_CH0_CR_MCMNTS_SPARE_pattern_match_enable_MAX             (1) // 0x00000001
  #define MC0_CH0_CR_MCMNTS_SPARE_pattern_match_enable_DEF             (0x00000000)
  #define MC0_CH0_CR_MCMNTS_SPARE_pattern_match_enable_HSH             (0x011CE5FC)

  #define MC0_CH0_CR_MCMNTS_SPARE_ForceX8Ref_OFF                       (16)
  #define MC0_CH0_CR_MCMNTS_SPARE_ForceX8Ref_WID                       ( 1)
  #define MC0_CH0_CR_MCMNTS_SPARE_ForceX8Ref_MSK                       (0x00010000)
  #define MC0_CH0_CR_MCMNTS_SPARE_ForceX8Ref_MIN                       (0)
  #define MC0_CH0_CR_MCMNTS_SPARE_ForceX8Ref_MAX                       (1) // 0x00000001
  #define MC0_CH0_CR_MCMNTS_SPARE_ForceX8Ref_DEF                       (0x00000000)
  #define MC0_CH0_CR_MCMNTS_SPARE_ForceX8Ref_HSH                       (0x0120E5FC)

  #define MC0_CH0_CR_MCMNTS_SPARE_Background_ZQ_Mode_OFF               (17)
  #define MC0_CH0_CR_MCMNTS_SPARE_Background_ZQ_Mode_WID               ( 1)
  #define MC0_CH0_CR_MCMNTS_SPARE_Background_ZQ_Mode_MSK               (0x00020000)
  #define MC0_CH0_CR_MCMNTS_SPARE_Background_ZQ_Mode_MIN               (0)
  #define MC0_CH0_CR_MCMNTS_SPARE_Background_ZQ_Mode_MAX               (1) // 0x00000001
  #define MC0_CH0_CR_MCMNTS_SPARE_Background_ZQ_Mode_DEF               (0x00000000)
  #define MC0_CH0_CR_MCMNTS_SPARE_Background_ZQ_Mode_HSH               (0x0122E5FC)

  #define MC0_CH0_CR_MCMNTS_SPARE_global_driver_wake_without_ref_OFF   (18)
  #define MC0_CH0_CR_MCMNTS_SPARE_global_driver_wake_without_ref_WID   ( 1)
  #define MC0_CH0_CR_MCMNTS_SPARE_global_driver_wake_without_ref_MSK   (0x00040000)
  #define MC0_CH0_CR_MCMNTS_SPARE_global_driver_wake_without_ref_MIN   (0)
  #define MC0_CH0_CR_MCMNTS_SPARE_global_driver_wake_without_ref_MAX   (1) // 0x00000001
  #define MC0_CH0_CR_MCMNTS_SPARE_global_driver_wake_without_ref_DEF   (0x00000001)
  #define MC0_CH0_CR_MCMNTS_SPARE_global_driver_wake_without_ref_HSH   (0x0124E5FC)

  #define MC0_CH0_CR_MCMNTS_SPARE_DisMaintenanceRoundRobinArb_OFF      (19)
  #define MC0_CH0_CR_MCMNTS_SPARE_DisMaintenanceRoundRobinArb_WID      ( 1)
  #define MC0_CH0_CR_MCMNTS_SPARE_DisMaintenanceRoundRobinArb_MSK      (0x00080000)
  #define MC0_CH0_CR_MCMNTS_SPARE_DisMaintenanceRoundRobinArb_MIN      (0)
  #define MC0_CH0_CR_MCMNTS_SPARE_DisMaintenanceRoundRobinArb_MAX      (1) // 0x00000001
  #define MC0_CH0_CR_MCMNTS_SPARE_DisMaintenanceRoundRobinArb_DEF      (0x00000000)
  #define MC0_CH0_CR_MCMNTS_SPARE_DisMaintenanceRoundRobinArb_HSH      (0x0126E5FC)

  #define MC0_CH0_CR_MCMNTS_SPARE_DisBypass2_A0_OFF                       (20)
  #define MC0_CH0_CR_MCMNTS_SPARE_DisBypass2_A0_WID                       ( 1)
  #define MC0_CH0_CR_MCMNTS_SPARE_DisBypass2_A0_MSK                       (0x00100000)
  #define MC0_CH0_CR_MCMNTS_SPARE_DisBypass2_A0_MIN                       (0)
  #define MC0_CH0_CR_MCMNTS_SPARE_DisBypass2_A0_MAX                       (1) // 0x00000001
  #define MC0_CH0_CR_MCMNTS_SPARE_DisBypass2_A0_DEF                       (0x00000000)
  #define MC0_CH0_CR_MCMNTS_SPARE_DisBypass2_A0_HSH                       (0x0128E5FC)

  #define MC0_CH0_CR_MCMNTS_SPARE_DisBypass1_A0_OFF                       (21)
  #define MC0_CH0_CR_MCMNTS_SPARE_DisBypass1_A0_WID                       ( 1)
  #define MC0_CH0_CR_MCMNTS_SPARE_DisBypass1_A0_MSK                       (0x00200000)
  #define MC0_CH0_CR_MCMNTS_SPARE_DisBypass1_A0_MIN                       (0)
  #define MC0_CH0_CR_MCMNTS_SPARE_DisBypass1_A0_MAX                       (1) // 0x00000001
  #define MC0_CH0_CR_MCMNTS_SPARE_DisBypass1_A0_DEF                       (0x00000000)
  #define MC0_CH0_CR_MCMNTS_SPARE_DisBypass1_A0_HSH                       (0x012AE5FC)

  #define MC0_CH0_CR_MCMNTS_SPARE_DisBypass0_A0_OFF                       (22)
  #define MC0_CH0_CR_MCMNTS_SPARE_DisBypass0_A0_WID                       ( 1)
  #define MC0_CH0_CR_MCMNTS_SPARE_DisBypass0_A0_MSK                       (0x00400000)
  #define MC0_CH0_CR_MCMNTS_SPARE_DisBypass0_A0_MIN                       (0)
  #define MC0_CH0_CR_MCMNTS_SPARE_DisBypass0_A0_MAX                       (1) // 0x00000001
  #define MC0_CH0_CR_MCMNTS_SPARE_DisBypass0_A0_DEF                       (0x00000000)
  #define MC0_CH0_CR_MCMNTS_SPARE_DisBypass0_A0_HSH                       (0x012CE5FC)

  #define MC0_CH0_CR_MCMNTS_SPARE_Run_Delay_OFF                        (23)
  #define MC0_CH0_CR_MCMNTS_SPARE_Run_Delay_WID                        ( 5)
  #define MC0_CH0_CR_MCMNTS_SPARE_Run_Delay_MSK                        (0x0F800000)
  #define MC0_CH0_CR_MCMNTS_SPARE_Run_Delay_MIN                        (0)
  #define MC0_CH0_CR_MCMNTS_SPARE_Run_Delay_MAX                        (31) // 0x0000001F
  #define MC0_CH0_CR_MCMNTS_SPARE_Run_Delay_DEF                        (0x00000010)
  #define MC0_CH0_CR_MCMNTS_SPARE_Run_Delay_HSH                        (0x052EE5FC)

  #define MC0_CH0_CR_MCMNTS_SPARE_Spare_RW_V_OFF                       (29)
  #define MC0_CH0_CR_MCMNTS_SPARE_Spare_RW_V_WID                       ( 3)
  #define MC0_CH0_CR_MCMNTS_SPARE_Spare_RW_V_MSK                       (0xE0000000)
  #define MC0_CH0_CR_MCMNTS_SPARE_Spare_RW_V_MIN                       (0)
  #define MC0_CH0_CR_MCMNTS_SPARE_Spare_RW_V_MAX                       (7) // 0x00000007
  #define MC0_CH0_CR_MCMNTS_SPARE_Spare_RW_V_DEF                       (0x00000000)
  #define MC0_CH0_CR_MCMNTS_SPARE_Spare_RW_V_HSH                       (0x033AE5FC)

#define MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG                       (0x0000E600)

  #define MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_ADDRESS_OFF             ( 0)
  #define MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_ADDRESS_WID             ( 8)
  #define MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_ADDRESS_MSK             (0x000000FF)
  #define MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_ADDRESS_MIN             (0)
  #define MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_ADDRESS_MAX             (255) // 0x000000FF
  #define MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_ADDRESS_DEF             (0x00000000)
  #define MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_ADDRESS_HSH             (0x0800E600)

  #define MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_GENERIC_MRS_STORAGE_POINTER_OFF ( 8)
  #define MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_GENERIC_MRS_STORAGE_POINTER_WID ( 9)
  #define MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_GENERIC_MRS_STORAGE_POINTER_MSK (0x0001FF00)
  #define MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_GENERIC_MRS_STORAGE_POINTER_MIN (0)
  #define MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_GENERIC_MRS_STORAGE_POINTER_MAX (511) // 0x000001FF
  #define MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_GENERIC_MRS_STORAGE_POINTER_DEF (0x00000000)
  #define MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_GENERIC_MRS_STORAGE_POINTER_HSH (0x0910E600)

  #define MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_COMMAND_TYPE_OFF        (22)
  #define MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_COMMAND_TYPE_WID        ( 2)
  #define MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_COMMAND_TYPE_MSK        (0x00C00000)
  #define MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_COMMAND_TYPE_MIN        (0)
  #define MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_COMMAND_TYPE_MAX        (3) // 0x00000003
  #define MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_COMMAND_TYPE_DEF        (0x00000000)
  #define MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_COMMAND_TYPE_HSH        (0x022CE600)

  #define MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_TIMING_VALUE_POINTER_OFF (24)
  #define MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_TIMING_VALUE_POINTER_WID ( 3)
  #define MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_TIMING_VALUE_POINTER_MSK (0x07000000)
  #define MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_TIMING_VALUE_POINTER_MIN (0)
  #define MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_TIMING_VALUE_POINTER_MAX (7) // 0x00000007
  #define MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_TIMING_VALUE_POINTER_DEF (0x00000000)
  #define MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_TIMING_VALUE_POINTER_HSH (0x0330E600)

  #define MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_PER_DEVICE_OFF          (27)
  #define MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_PER_DEVICE_WID          ( 1)
  #define MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_PER_DEVICE_MSK          (0x08000000)
  #define MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_PER_DEVICE_MIN          (0)
  #define MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_PER_DEVICE_MAX          (1) // 0x00000001
  #define MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_PER_DEVICE_DEF          (0x00000000)
  #define MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_PER_DEVICE_HSH          (0x0136E600)

  #define MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_FSP_CONTROL_OFF         (28)
  #define MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_FSP_CONTROL_WID         ( 2)
  #define MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_FSP_CONTROL_MSK         (0x30000000)
  #define MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_FSP_CONTROL_MIN         (0)
  #define MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_FSP_CONTROL_MAX         (3) // 0x00000003
  #define MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_FSP_CONTROL_DEF         (0x00000000)
  #define MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_FSP_CONTROL_HSH         (0x0238E600)

  #define MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_PER_RANK_OFF            (30)
  #define MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_PER_RANK_WID            ( 1)
  #define MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_PER_RANK_MSK            (0x40000000)
  #define MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_PER_RANK_MIN            (0)
  #define MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_PER_RANK_MAX            (1) // 0x00000001
  #define MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_PER_RANK_DEF            (0x00000000)
  #define MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_PER_RANK_HSH            (0x013CE600)

  #define MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_ACTIVE_OFF              (31)
  #define MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_ACTIVE_WID              ( 1)
  #define MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_ACTIVE_MSK              (0x80000000)
  #define MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_ACTIVE_MIN              (0)
  #define MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_ACTIVE_MAX              (1) // 0x00000001
  #define MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_ACTIVE_DEF              (0x00000000)
  #define MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_ACTIVE_HSH              (0x013EE600)

#define MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_1_REG                       (0x0000E604)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_2_REG                       (0x0000E608)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_3_REG                       (0x0000E60C)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_4_REG                       (0x0000E610)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_5_REG                       (0x0000E614)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_6_REG                       (0x0000E618)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_7_REG                       (0x0000E61C)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_8_REG                       (0x0000E620)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_9_REG                       (0x0000E624)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_10_REG                      (0x0000E628)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_11_REG                      (0x0000E62C)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_12_REG                      (0x0000E630)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_13_REG                      (0x0000E634)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_14_REG                      (0x0000E638)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_15_REG                      (0x0000E63C)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_16_REG                      (0x0000E640)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_17_REG                      (0x0000E644)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_18_REG                      (0x0000E648)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_19_REG                      (0x0000E64C)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_20_REG                      (0x0000E650)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_21_REG                      (0x0000E654)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_22_REG                      (0x0000E658)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_23_REG                      (0x0000E65C)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_24_REG                      (0x0000E660)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_25_REG                      (0x0000E664)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_26_REG                      (0x0000E668)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_27_REG                      (0x0000E66C)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_28_REG                      (0x0000E670)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_29_REG                      (0x0000E674)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_30_REG                      (0x0000E678)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_31_REG                      (0x0000E67C)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_32_REG                      (0x0000E680)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_33_REG                      (0x0000E684)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_34_REG                      (0x0000E688)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_35_REG                      (0x0000E68C)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_36_REG                      (0x0000E690)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_37_REG                      (0x0000E694)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_38_REG                      (0x0000E698)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_39_REG                      (0x0000E69C)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_40_REG                      (0x0000E6A0)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_41_REG                      (0x0000E6A4)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_42_REG                      (0x0000E6A8)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_43_REG                      (0x0000E6AC)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_44_REG                      (0x0000E6B0)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_45_REG                      (0x0000E6B4)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_46_REG                      (0x0000E6B8)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_47_REG                      (0x0000E6BC)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_48_REG                      (0x0000E6C0)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_49_REG                      (0x0000E6C4)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_50_REG                      (0x0000E6C8)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_51_REG                      (0x0000E6CC)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_52_REG                      (0x0000E6D0)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_53_REG                      (0x0000E6D4)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_54_REG                      (0x0000E6D8)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_55_REG                      (0x0000E6DC)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_56_REG                      (0x0000E6E0)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_57_REG                      (0x0000E6E4)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_58_REG                      (0x0000E6E8)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_59_REG                      (0x0000E6EC)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_60_REG                      (0x0000E6F0)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_61_REG                      (0x0000E6F4)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_62_REG                      (0x0000E6F8)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_63_REG                      (0x0000E6FC)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_64_REG                      (0x0000E700)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_65_REG                      (0x0000E704)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_66_REG                      (0x0000E708)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_67_REG                      (0x0000E70C)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_68_REG                      (0x0000E710)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_69_REG                      (0x0000E714)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_70_REG                      (0x0000E718)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_71_REG                      (0x0000E71C)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_72_REG                      (0x0000E720)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_73_REG                      (0x0000E724)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_74_REG                      (0x0000E728)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_75_REG                      (0x0000E72C)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_76_REG                      (0x0000E730)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_77_REG                      (0x0000E734)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_78_REG                      (0x0000E738)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_79_REG                      (0x0000E73C)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_80_REG                      (0x0000E740)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_81_REG                      (0x0000E744)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_82_REG                      (0x0000E748)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_83_REG                      (0x0000E74C)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_84_REG                      (0x0000E750)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_85_REG                      (0x0000E754)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_86_REG                      (0x0000E758)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_87_REG                      (0x0000E75C)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_88_REG                      (0x0000E760)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_89_REG                      (0x0000E764)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_90_REG                      (0x0000E768)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_91_REG                      (0x0000E76C)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_92_REG                      (0x0000E770)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_93_REG                      (0x0000E774)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_94_REG                      (0x0000E778)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_95_REG                      (0x0000E77C)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_96_REG                      (0x0000E780)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_97_REG                      (0x0000E784)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_98_REG                      (0x0000E788)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_99_REG                      (0x0000E78C)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_100_REG                     (0x0000E790)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_101_REG                     (0x0000E794)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_102_REG                     (0x0000E798)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_103_REG                     (0x0000E79C)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_104_REG                     (0x0000E7A0)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_105_REG                     (0x0000E7A4)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_106_REG                     (0x0000E7A8)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_107_REG                     (0x0000E7AC)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_CH0_CR_MRH_CONFIG_REG                                      (0x0000E7FC)

  #define MC0_CH0_CR_MRH_CONFIG_mrh_quiet_time_before_command_OFF      ( 0)
  #define MC0_CH0_CR_MRH_CONFIG_mrh_quiet_time_before_command_WID      (10)
  #define MC0_CH0_CR_MRH_CONFIG_mrh_quiet_time_before_command_MSK      (0x000003FF)
  #define MC0_CH0_CR_MRH_CONFIG_mrh_quiet_time_before_command_MIN      (0)
  #define MC0_CH0_CR_MRH_CONFIG_mrh_quiet_time_before_command_MAX      (1023) // 0x000003FF
  #define MC0_CH0_CR_MRH_CONFIG_mrh_quiet_time_before_command_DEF      (0x00000000)
  #define MC0_CH0_CR_MRH_CONFIG_mrh_quiet_time_before_command_HSH      (0x0A00E7FC)

  #define MC0_CH0_CR_MRH_CONFIG_mrh_quiet_time_after_command_OFF       (10)
  #define MC0_CH0_CR_MRH_CONFIG_mrh_quiet_time_after_command_WID       (10)
  #define MC0_CH0_CR_MRH_CONFIG_mrh_quiet_time_after_command_MSK       (0x000FFC00)
  #define MC0_CH0_CR_MRH_CONFIG_mrh_quiet_time_after_command_MIN       (0)
  #define MC0_CH0_CR_MRH_CONFIG_mrh_quiet_time_after_command_MAX       (1023) // 0x000003FF
  #define MC0_CH0_CR_MRH_CONFIG_mrh_quiet_time_after_command_DEF       (0x00000000)
  #define MC0_CH0_CR_MRH_CONFIG_mrh_quiet_time_after_command_HSH       (0x0A14E7FC)

#define MC0_CH1_CR_TC_PRE_REG                                          (0x0000E800)
//Duplicate of MC0_CH0_CR_TC_PRE_REG

#define MC0_CH1_CR_TC_ACT_REG                                          (0x0000E808)
//Duplicate of MC0_CH0_CR_TC_ACT_REG

#define MC0_CH1_CR_TC_RDRD_REG                                         (0x0000E80C)
//Duplicate of MC0_CH0_CR_TC_RDRD_REG

#define MC0_CH1_CR_TC_RDWR_REG                                         (0x0000E810)
//Duplicate of MC0_CH0_CR_TC_RDWR_REG

#define MC0_CH1_CR_TC_WRRD_REG                                         (0x0000E814)
//Duplicate of MC0_CH0_CR_TC_WRRD_REG

#define MC0_CH1_CR_TC_WRWR_REG                                         (0x0000E818)
//Duplicate of MC0_CH0_CR_TC_WRWR_REG

#define MC0_CH1_CR_PM_ADAPTIVE_CKE_REG                                 (0x0000E81C)
//Duplicate of MC0_CH0_CR_PM_ADAPTIVE_CKE_REG

#define MC0_CH1_CR_SC_ROUNDTRIP_LATENCY_REG                            (0x0000E820)
//Duplicate of MC0_CH0_CR_SC_ROUNDTRIP_LATENCY_REG

#define MC0_CH1_CR_SCHED_CBIT_REG                                      (0x0000E828)
//Duplicate of MC0_CH0_CR_SCHED_CBIT_REG

#define MC0_CH1_CR_SCHED_SECOND_CBIT_REG                               (0x0000E82C)
//Duplicate of MC0_CH0_CR_SCHED_SECOND_CBIT_REG

#define MC0_CH1_CR_DFT_MISC_REG                                        (0x0000E830)
//Duplicate of MC0_CH0_CR_DFT_MISC_REG

#define MC0_CH1_CR_SC_PCIT_REG                                         (0x0000E834)
//Duplicate of MC0_CH0_CR_SC_PCIT_REG

#define MC0_CH1_CR_ECC_DFT_REG                                         (0x0000E838)
//Duplicate of MC0_CH0_CR_ECC_DFT_REG

#define MC0_CH1_CR_PM_PDWN_CONFIG_REG                                  (0x0000E840)
//Duplicate of MC0_CH0_CR_PM_PDWN_CONFIG_REG

#define MC0_CH1_CR_ECCERRLOG0_REG                                      (0x0000E848)
//Duplicate of MC0_CH0_CR_ECCERRLOG0_REG

#define MC0_CH1_CR_ECCERRLOG1_REG                                      (0x0000E84C)
//Duplicate of MC0_CH0_CR_ECCERRLOG1_REG

#define MC0_CH1_CR_TC_PWRDN_REG                                        (0x0000E850)
//Duplicate of MC0_CH0_CR_TC_PWRDN_REG

#define MC0_CH1_CR_QUEUE_ENTRY_DISABLE_RPQ_REG                         (0x0000E858)
//Duplicate of MC0_CH0_CR_QUEUE_ENTRY_DISABLE_RPQ_REG

#define MC0_CH1_CR_QUEUE_ENTRY_DISABLE_IPQ_REG                         (0x0000E85C)
//Duplicate of MC0_CH0_CR_QUEUE_ENTRY_DISABLE_IPQ_REG

#define MC0_CH1_CR_QUEUE_ENTRY_DISABLE_WPQ_REG                         (0x0000E860)
//Duplicate of MC0_CH0_CR_QUEUE_ENTRY_DISABLE_WPQ_REG

#define MC0_CH1_CR_SC_WDBWM_REG                                        (0x0000E868)
//Duplicate of MC0_CH0_CR_SC_WDBWM_REG

#define MC0_CH1_CR_TC_ODT_REG                                          (0x0000E870)
//Duplicate of MC0_CH0_CR_TC_ODT_REG

#define MC0_CH1_CR_MCSCHEDS_SPARE_REG                                  (0x0000E878)
//Duplicate of MC0_CH0_CR_MCSCHEDS_SPARE_REG

#define MC0_CH1_CR_TC_MPC_REG                                          (0x0000E87C)
//Duplicate of MC0_CH0_CR_TC_MPC_REG

#define MC0_CH1_CR_SC_ODT_MATRIX_REG                                   (0x0000E880)
//Duplicate of MC0_CH0_CR_SC_ODT_MATRIX_REG

#define MC0_CH1_CR_DFT_BLOCK_REG                                       (0x0000E884)
//Duplicate of MC0_CH0_CR_DFT_BLOCK_REG

#define MC0_CH1_CR_SC_GS_CFG_REG                                       (0x0000E888)
//Duplicate of MC0_CH0_CR_SC_GS_CFG_REG

#define MC0_CH1_CR_SC_PH_THROTTLING_0_REG                              (0x0000E890)
//Duplicate of MC0_CH0_CR_SC_PH_THROTTLING_0_REG

#define MC0_CH1_CR_SC_PH_THROTTLING_1_REG                              (0x0000E894)
//Duplicate of MC0_CH0_CR_SC_PH_THROTTLING_1_REG

#define MC0_CH1_CR_SC_PH_THROTTLING_2_REG                              (0x0000E898)
//Duplicate of MC0_CH0_CR_SC_PH_THROTTLING_2_REG

#define MC0_CH1_CR_SC_PH_THROTTLING_3_REG                              (0x0000E89C)
//Duplicate of MC0_CH0_CR_SC_PH_THROTTLING_3_REG

#define MC0_CH1_CR_SC_WPQ_THRESHOLD_REG                                (0x0000E8A0)
//Duplicate of MC0_CH0_CR_SC_WPQ_THRESHOLD_REG

#define MC0_CH1_CR_SC_PR_CNT_CONFIG_REG                                (0x0000E8A8)
//Duplicate of MC0_CH0_CR_SC_PR_CNT_CONFIG_REG

#define MC0_CH1_CR_REUT_CH_MISC_CKE_CS_CTRL_REG                        (0x0000E8B0)
//Duplicate of MC0_CH0_CR_REUT_CH_MISC_CKE_CS_CTRL_REG

#define MC0_CH1_CR_REUT_CH_MISC_ODT_CTRL_REG                           (0x0000E8B4)
//Duplicate of MC0_CH0_CR_REUT_CH_MISC_ODT_CTRL_REG

#define MC0_CH1_CR_SPID_LOW_POWER_CTL_REG                              (0x0000E8B8)
//Duplicate of MC0_CH0_CR_SPID_LOW_POWER_CTL_REG

#define MC0_CH1_CR_SC_GS_CFG_TRAINING_REG                              (0x0000E8BC)
//Duplicate of MC0_CH0_CR_SC_GS_CFG_TRAINING_REG

#define MC0_CH1_CR_SCHED_THIRD_CBIT_REG                                (0x0000E8C0)
//Duplicate of MC0_CH0_CR_SCHED_THIRD_CBIT_REG

#define MC0_CH1_CR_DEADLOCK_BREAKER_REG                                (0x0000E8C4)
//Duplicate of MC0_CH0_CR_DEADLOCK_BREAKER_REG

#define MC0_CH1_CR_XARB_TC_BUBBLE_INJ_REG                              (0x0000E8C8)
//Duplicate of MC0_CH0_CR_XARB_TC_BUBBLE_INJ_REG

#define MC0_CH1_CR_MCSCHEDS_GLOBAL_DRIVER_GATE_CFG_REG                 (0x0000E8D0)
//Duplicate of MC0_MCMAINS_GLOBAL_DRIVER_GATE_CFG_REG

#define MC0_CH1_CR_SC_BLOCKING_RULES_CFG_REG                           (0x0000E8D4)
//Duplicate of MC0_CH0_CR_SC_BLOCKING_RULES_CFG_REG

#define MC0_CH1_CR_PWM_DDR_SUBCH0_ACT_COUNTER_REG                      (0x0000E8D8)
//Duplicate of MC0_CH0_CR_PWM_DDR_SUBCH0_ACT_COUNTER_REG

#define MC0_CH1_CR_PWM_DDR_SUBCH1_ACT_COUNTER_REG                      (0x0000E8E0)
//Duplicate of MC0_CH0_CR_PWM_DDR_SUBCH0_ACT_COUNTER_REG

#define MC0_CH1_CR_PWM_DDR_SUBCH0_REQ_OCCUPANCY_COUNTER_REG            (0x0000E8E8)
//Duplicate of MC0_CH0_CR_PWM_DDR_SUBCH0_REQ_OCCUPANCY_COUNTER_REG

#define MC0_CH1_CR_PWM_DDR_SUBCH1_REQ_OCCUPANCY_COUNTER_REG            (0x0000E8F0)
//Duplicate of MC0_CH0_CR_PWM_DDR_SUBCH0_REQ_OCCUPANCY_COUNTER_REG

#define MC0_CH1_CR_WCK_CONFIG_REG                                      (0x0000E8F8)
//Duplicate of MC0_CH0_CR_WCK_CONFIG_REG

#define MC0_CH1_CR_XARB_CFG_BUBBLE_INJ_REG                             (0x0000E900)
//Duplicate of MC0_CH0_CR_XARB_CFG_BUBBLE_INJ_REG

#define MC0_CH1_CR_TR_RRDVALID_CTRL_REG                                (0x0000E904)
//Duplicate of MC0_CH0_CR_TR_RRDVALID_CTRL_REG

#define MC0_CH1_CR_TR_RRDVALID_DATA_REG                                (0x0000E908)
//Duplicate of MC0_CH0_CR_TR_RRDVALID_DATA_REG

#define MC0_CH1_CR_WMM_READ_CONFIG_REG                                 (0x0000E910)
//Duplicate of MC0_CH0_CR_WMM_READ_CONFIG_REG

#define MC0_CH1_CR_MC2PHY_BGF_CTRL_REG                                 (0x0000E914)
//Duplicate of MC0_CH0_CR_MC2PHY_BGF_CTRL_REG

#define MC0_CH1_CR_SC_ADAPTIVE_PCIT_REG                                (0x0000E918)
//Duplicate of MC0_CH0_CR_SC_ADAPTIVE_PCIT_REG

#define MC0_CH1_CR_MERGE_REQ_READS_PQ_REG                              (0x0000E920)
//Duplicate of MC0_CH0_CR_MERGE_REQ_READS_PQ_REG
#define MC0_CH1_CR_ROWHAMMER_CTL_REG                                   (0x0000E928)
//Duplicate of MC0_CH0_CR_ROWHAMMER_CTL_REG

#define MC0_CH1_CR_GENERIC_MRS_FSM_STORAGE_VALUES_0_REG                (0x0000EA00)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_0_REG

#define MC0_CH1_CR_GENERIC_MRS_FSM_STORAGE_VALUES_1_REG                (0x0000EA04)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_0_REG

#define MC0_CH1_CR_GENERIC_MRS_FSM_STORAGE_VALUES_2_REG                (0x0000EA08)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_0_REG

#define MC0_CH1_CR_GENERIC_MRS_FSM_STORAGE_VALUES_3_REG                (0x0000EA0C)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_0_REG

#define MC0_CH1_CR_GENERIC_MRS_FSM_STORAGE_VALUES_4_REG                (0x0000EA10)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_0_REG

#define MC0_CH1_CR_GENERIC_MRS_FSM_STORAGE_VALUES_5_REG                (0x0000EA14)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_0_REG

#define MC0_CH1_CR_GENERIC_MRS_FSM_STORAGE_VALUES_6_REG                (0x0000EA18)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_0_REG

#define MC0_CH1_CR_GENERIC_MRS_FSM_STORAGE_VALUES_7_REG                (0x0000EA1C)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_0_REG

#define MC0_CH1_CR_GENERIC_MRS_FSM_STORAGE_VALUES_8_REG                (0x0000EA20)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_0_REG

#define MC0_CH1_CR_GENERIC_MRS_FSM_STORAGE_VALUES_9_REG                (0x0000EA24)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_0_REG

#define MC0_CH1_CR_GENERIC_MRS_FSM_STORAGE_VALUES_10_REG               (0x0000EA28)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_0_REG

#define MC0_CH1_CR_GENERIC_MRS_FSM_STORAGE_VALUES_11_REG               (0x0000EA2C)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_0_REG

#define MC0_CH1_CR_GENERIC_MRS_FSM_STORAGE_VALUES_12_REG               (0x0000EA30)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_0_REG

#define MC0_CH1_CR_GENERIC_MRS_FSM_STORAGE_VALUES_13_REG               (0x0000EA34)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_0_REG

#define MC0_CH1_CR_GENERIC_MRS_FSM_STORAGE_VALUES_14_REG               (0x0000EA38)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_0_REG

#define MC0_CH1_CR_GENERIC_MRS_FSM_STORAGE_VALUES_15_REG               (0x0000EA3C)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_0_REG

#define MC0_CH1_CR_GENERIC_MRS_FSM_STORAGE_VALUES_16_REG               (0x0000EA40)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_0_REG

#define MC0_CH1_CR_GENERIC_MRS_FSM_STORAGE_VALUES_17_REG               (0x0000EA44)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_0_REG

#define MC0_CH1_CR_GENERIC_MRS_FSM_STORAGE_VALUES_18_REG               (0x0000EA48)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_0_REG

#define MC0_CH1_CR_GENERIC_MRS_FSM_STORAGE_VALUES_19_REG               (0x0000EA4C)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_0_REG

#define MC0_CH1_CR_GENERIC_MRS_FSM_STORAGE_VALUES_20_REG               (0x0000EA50)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_0_REG

#define MC0_CH1_CR_GENERIC_MRS_FSM_STORAGE_VALUES_21_REG               (0x0000EA54)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_0_REG

#define MC0_CH1_CR_GENERIC_MRS_FSM_STORAGE_VALUES_22_REG               (0x0000EA58)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_0_REG

#define MC0_CH1_CR_GENERIC_MRS_FSM_STORAGE_VALUES_23_REG               (0x0000EA5C)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_0_REG

#define MC0_CH1_CR_GENERIC_MRS_FSM_STORAGE_VALUES_24_REG               (0x0000EA60)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_0_REG

#define MC0_CH1_CR_GENERIC_MRS_FSM_STORAGE_VALUES_25_REG               (0x0000EA64)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_0_REG

#define MC0_CH1_CR_GENERIC_MRS_FSM_STORAGE_VALUES_26_REG               (0x0000EA68)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_0_REG

#define MC0_CH1_CR_GENERIC_MRS_FSM_STORAGE_VALUES_27_REG               (0x0000EA6C)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_0_REG

#define MC0_CH1_CR_GENERIC_MRS_FSM_STORAGE_VALUES_28_REG               (0x0000EA70)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_0_REG

#define MC0_CH1_CR_GENERIC_MRS_FSM_STORAGE_VALUES_29_REG               (0x0000EA74)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_0_REG

#define MC0_CH1_CR_GENERIC_MRS_FSM_STORAGE_VALUES_30_REG               (0x0000EA78)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_0_REG

#define MC0_CH1_CR_GENERIC_MRS_FSM_STORAGE_VALUES_31_REG               (0x0000EA7C)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_0_REG

#define MC0_CH1_CR_GENERIC_MRS_FSM_STORAGE_VALUES_32_REG               (0x0000EA80)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_0_REG

#define MC0_CH1_CR_GENERIC_MRS_FSM_STORAGE_VALUES_33_REG               (0x0000EA84)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_0_REG

#define MC0_CH1_CR_GENERIC_MRS_FSM_STORAGE_VALUES_34_REG               (0x0000EA88)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_0_REG

#define MC0_CH1_CR_GENERIC_MRS_FSM_STORAGE_VALUES_35_REG               (0x0000EA8C)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_0_REG

#define MC0_CH1_CR_GENERIC_MRS_FSM_STORAGE_VALUES_36_REG               (0x0000EA90)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_0_REG

#define MC0_CH1_CR_GENERIC_MRS_FSM_STORAGE_VALUES_37_REG               (0x0000EA94)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_0_REG

#define MC0_CH1_CR_GENERIC_MRS_FSM_STORAGE_VALUES_38_REG               (0x0000EA98)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_0_REG

#define MC0_CH1_CR_GENERIC_MRS_FSM_STORAGE_VALUES_39_REG               (0x0000EA9C)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_0_REG

#define MC0_CH1_CR_GENERIC_MRS_FSM_STORAGE_VALUES_40_REG               (0x0000EAA0)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_0_REG

#define MC0_CH1_CR_GENERIC_MRS_FSM_STORAGE_VALUES_41_REG               (0x0000EAA4)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_0_REG

#define MC0_CH1_CR_GENERIC_MRS_FSM_STORAGE_VALUES_42_REG               (0x0000EAA8)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_0_REG

#define MC0_CH1_CR_GENERIC_MRS_FSM_STORAGE_VALUES_43_REG               (0x0000EAAC)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_0_REG

#define MC0_CH1_CR_GENERIC_MRS_FSM_STORAGE_VALUES_44_REG               (0x0000EAB0)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_0_REG

#define MC0_CH1_CR_GENERIC_MRS_FSM_STORAGE_VALUES_45_REG               (0x0000EAB4)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_0_REG

#define MC0_CH1_CR_GENERIC_MRS_FSM_STORAGE_VALUES_46_REG               (0x0000EAB8)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_0_REG

#define MC0_CH1_CR_GENERIC_MRS_FSM_STORAGE_VALUES_47_REG               (0x0000EABC)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_0_REG

#define MC0_CH1_CR_GENERIC_MRS_FSM_STORAGE_VALUES_48_REG               (0x0000EAC0)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_0_REG

#define MC0_CH1_CR_GENERIC_MRS_FSM_STORAGE_VALUES_49_REG               (0x0000EAC4)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_0_REG

#define MC0_CH1_CR_GENERIC_MRS_FSM_STORAGE_VALUES_50_REG               (0x0000EAC8)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_0_REG

#define MC0_CH1_CR_GENERIC_MRS_FSM_STORAGE_VALUES_51_REG               (0x0000EACC)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_0_REG

#define MC0_CH1_CR_GENERIC_MRS_FSM_STORAGE_VALUES_52_REG               (0x0000EAD0)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_0_REG

#define MC0_CH1_CR_GENERIC_MRS_FSM_STORAGE_VALUES_53_REG               (0x0000EAD4)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_0_REG

#define MC0_CH1_CR_GENERIC_MRS_FSM_STORAGE_VALUES_54_REG               (0x0000EAD8)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_0_REG

#define MC0_CH1_CR_GENERIC_MRS_FSM_STORAGE_VALUES_55_REG               (0x0000EADC)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_0_REG

#define MC0_CH1_CR_GENERIC_MRS_FSM_STORAGE_VALUES_56_REG               (0x0000EAE0)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_0_REG

#define MC0_CH1_CR_GENERIC_MRS_FSM_STORAGE_VALUES_57_REG               (0x0000EAE4)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_0_REG

#define MC0_CH1_CR_GENERIC_MRS_FSM_STORAGE_VALUES_58_REG               (0x0000EAE8)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_0_REG

#define MC0_CH1_CR_GENERIC_MRS_FSM_STORAGE_VALUES_59_REG               (0x0000EAEC)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_STORAGE_VALUES_0_REG

#define MC0_CH1_CR_GENERIC_MRS_FSM_TIMING_STORAGE_0_REG                (0x0000EBE0)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_TIMING_STORAGE_0_REG

#define MC0_CH1_CR_GENERIC_MRS_FSM_TIMING_STORAGE_1_REG                (0x0000EBE4)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_TIMING_STORAGE_0_REG

#define MC0_CH1_CR_MNTS_CBIT_REG                                       (0x0000EBEC)
//Duplicate of MC0_CH0_CR_MNTS_CBIT_REG

#define MC0_CH1_CR_RH_TRR_LFSR_REG                                     (0x0000EBF0)
//Duplicate of MC0_CH0_CR_RH_TRR_LFSR_REG

#define MC0_CH1_CR_WDB_CAPTURE_CTL_REG                                 (0x0000EBF8)
//Duplicate of MC0_CH0_CR_WDB_CAPTURE_CTL_REG

#define MC0_CH1_CR_WDB_CAPTURE_STATUS_REG                              (0x0000EBFC)
//Duplicate of MC0_CH0_CR_WDB_CAPTURE_STATUS_REG

#define MC0_CH1_CR_RH_TRR_CONTROL_REG                                  (0x0000EC00)
//Duplicate of MC0_CH0_CR_RH_TRR_CONTROL_REG

#define MC0_CH1_CR_REUT_CH_MISC_REFRESH_CTRL_REG                       (0x0000EC04)
//Duplicate of MC0_CH0_CR_REUT_CH_MISC_REFRESH_CTRL_REG

#define MC0_CH1_CR_REUT_CH_MISC_ZQ_CTRL_REG                            (0x0000EC08)
//Duplicate of MC0_CH0_CR_REUT_CH_MISC_ZQ_CTRL_REG

#define MC0_CH1_CR_TC_REFM_REG                                         (0x0000EC0C)
//Duplicate of MC0_CH0_CR_TC_REFM_REG

#define MC0_CH1_CR_DDR_MR_PARAMS_REG                                   (0x0000EC10)
//Duplicate of MC0_CH0_CR_DDR_MR_PARAMS_REG

#define MC0_CH1_CR_DDR_MR_COMMAND_REG                                  (0x0000EC14)
//Duplicate of MC0_CH0_CR_DDR_MR_COMMAND_REG

#define MC0_CH1_CR_DDR_MR_RESULT_0_REG                                 (0x0000EC18)
//Duplicate of MC0_CH0_CR_DDR_MR_RESULT_0_REG

#define MC0_CH1_CR_DDR_MR_RESULT_1_REG                                 (0x0000EC1C)
//Duplicate of MC0_CH0_CR_DDR_MR_RESULT_1_REG

#define MC0_CH1_CR_DDR_MR_RESULT_2_REG                                 (0x0000EC20)
//Duplicate of MC0_CH0_CR_DDR_MR_RESULT_2_REG

#define MC0_CH1_CR_MR4_RANK_TEMPERATURE_REG                            (0x0000EC24)
//Duplicate of MC0_CH0_CR_MR4_RANK_TEMPERATURE_REG

#define MC0_CH1_CR_DDR4_MPR_RANK_TEMPERATURE_REG                       (0x0000EC28)
//Duplicate of MC0_CH0_CR_DDR4_MPR_RANK_TEMPERATURE_REG

#define MC0_CH1_CR_MC_RDB_CREDITS_REG                                  (0x0000EC2C)
//Duplicate of MC0_CH0_CR_MC_RDB_CREDITS_REG

#define MC0_CH1_CR_DESWIZZLE_LOW_ERM_REG                               (0x0000EC30)
//Duplicate of MC0_CH0_CR_DESWIZZLE_LOW_ERM_REG

#define MC0_CH1_CR_TC_RFP_REG                                          (0x0000EC38)
//Duplicate of MC0_CH0_CR_TC_RFP_REG

#define MC0_CH1_CR_TC_RFTP_REG                                         (0x0000EC3C)
//Duplicate of MC0_CH0_CR_TC_RFTP_REG

#define MC0_CH1_CR_TC_SRFTP_REG                                        (0x0000EC40)
//Duplicate of MC0_CH0_CR_TC_SRFTP_REG

#define MC0_CH1_CR_MC_REFRESH_STAGGER_REG                              (0x0000EC44)
//Duplicate of MC0_CH0_CR_MC_REFRESH_STAGGER_REG

#define MC0_CH1_CR_TC_ZQCAL_REG                                        (0x0000EC48)
//Duplicate of MC0_CH0_CR_TC_ZQCAL_REG

#define MC0_CH1_CR_TC_MR4_SHADDOW_REG                                  (0x0000EC50)
//Duplicate of MC0_CH0_CR_TC_MR4_SHADDOW_REG

#define MC0_CH1_CR_MC_INIT_STATE_REG                                   (0x0000EC54)
//Duplicate of MC0_CH0_CR_MC_INIT_STATE_REG

#define MC0_CH1_CR_WDB_VISA_SEL_REG                                    (0x0000EC58)
//Duplicate of MC0_CH0_CR_WDB_VISA_SEL_REG

#define MC0_CH1_CR_PASR_CTL_REG                                        (0x0000EC5C)
//Duplicate of MC0_CH0_CR_PASR_CTL_REG

#define MC0_CH1_CR_PM_DIMM_IDLE_ENERGY_REG                             (0x0000EC60)
//Duplicate of MC0_CH0_CR_PM_DIMM_IDLE_ENERGY_REG

#define MC0_CH1_CR_PM_DIMM_PD_ENERGY_REG                               (0x0000EC64)
//Duplicate of MC0_CH0_CR_PM_DIMM_PD_ENERGY_REG

#define MC0_CH1_CR_PM_DIMM_ACT_ENERGY_REG                              (0x0000EC68)
//Duplicate of MC0_CH0_CR_PM_DIMM_ACT_ENERGY_REG

#define MC0_CH1_CR_PM_DIMM_RD_ENERGY_REG                               (0x0000EC6C)
//Duplicate of MC0_CH0_CR_PM_DIMM_RD_ENERGY_REG

#define MC0_CH1_CR_PM_DIMM_WR_ENERGY_REG                               (0x0000EC70)
//Duplicate of MC0_CH0_CR_PM_DIMM_WR_ENERGY_REG

#define MC0_CH1_CR_RH_TRR_ADDRESS_REG                                  (0x0000EC74)
//Duplicate of MC0_CH0_CR_RH_TRR_ADDRESS_REG

#define MC0_CH1_CR_SC_WR_DELAY_REG                                     (0x0000EC78)
//Duplicate of MC0_CH0_CR_SC_WR_DELAY_REG

#define MC0_CH1_CR_READ_RETURN_DFT_REG                                 (0x0000EC7C)
//Duplicate of MC0_CH0_CR_READ_RETURN_DFT_REG

#define MC0_CH1_CR_DESWIZZLE_LOW_REG                                   (0x0000EC80)
//Duplicate of MC0_CH0_CR_DESWIZZLE_LOW_ERM_REG

#define MC0_CH1_CR_SC_PBR_REG                                          (0x0000EC88)
//Duplicate of MC0_CH0_CR_SC_PBR_REG

#define MC0_CH1_CR_TC_LPDDR4_MISC_REG                                  (0x0000EC94)
//Duplicate of MC0_CH0_CR_TC_LPDDR4_MISC_REG

#define MC0_CH1_CR_DESWIZZLE_HIGH_ERM_REG                              (0x0000EC98)
//Duplicate of MC0_CH0_CR_DESWIZZLE_HIGH_ERM_REG

#define MC0_CH1_CR_PM_ALL_RANKS_CKE_LOW_COUNT_REG                      (0x0000ECB0)
//Duplicate of MC0_PWM_COUNTERS_DURATION_REG

#define MC0_CH1_CR_DESWIZZLE_HIGH_REG                                  (0x0000ECB8)
//Duplicate of MC0_CH0_CR_DESWIZZLE_HIGH_ERM_REG

#define MC0_CH1_CR_TC_SREXITTP_REG                                     (0x0000ECC0)
//Duplicate of MC0_CH0_CR_TC_SREXITTP_REG

#define MC0_CH1_CR_DQS_OSCILLATOR_PARAMS_REG                           (0x0000ECC8)
//Duplicate of MC0_CH0_CR_DQS_OSCILLATOR_PARAMS_REG

#define MC0_CH1_CR_MRH_GENERIC_COMMAND_REG                             (0x0000ECCC)
//Duplicate of MC0_CH0_CR_MRH_GENERIC_COMMAND_REG

#define MC0_CH1_CR_WDB_RD_WR_DFX_DATA_REG                              (0x0000ECD8)
//Duplicate of MC0_CH0_CR_WDB_RD_WR_DFX_DATA_REG

#define MC0_CH1_CR_WDB_RD_WR_DFX_CTL_REG                               (0x0000ECE0)
//Duplicate of MC0_CH0_CR_WDB_RD_WR_DFX_CTL_REG

#define MC0_CH1_CR_REF_FSM_STATUS_REG                                  (0x0000ECE4)
//Duplicate of MC0_CH0_CR_REF_FSM_STATUS_REG

#define MC0_CH1_CR_WDB_MBIST_0_REG                                     (0x0000ECE8)
//Duplicate of MC0_CH0_CR_WDB_MBIST_0_REG

#define MC0_CH1_CR_WDB_MBIST_1_REG                                     (0x0000ECEC)
//Duplicate of MC0_CH0_CR_WDB_MBIST_0_REG

#define MC0_CH1_CR_RDB_MBIST_REG                                       (0x0000ECF8)
//Duplicate of MC0_CH0_CR_WDB_MBIST_0_REG

#define MC0_CH1_CR_ECC_INJECT_COUNT_REG                                (0x0000ECFC)
//Duplicate of MC0_CH0_CR_ECC_INJECT_COUNT_REG

#define MC0_CH1_CR_PWM_DDR_SUBCH0_RDDATA_COUNTER_REG                   (0x0000ED00)
//Duplicate of MC0_CH0_CR_PWM_DDR_SUBCH0_RDDATA_COUNTER_REG

#define MC0_CH1_CR_PWM_DDR_SUBCH1_RDDATA_COUNTER_REG                   (0x0000ED08)
//Duplicate of MC0_CH0_CR_PWM_DDR_SUBCH0_RDDATA_COUNTER_REG

#define MC0_CH1_CR_PWM_DDR_SUBCH0_WRDATA_COUNTER_REG                   (0x0000ED10)
//Duplicate of MC0_CH0_CR_PWM_DDR_SUBCH0_WRDATA_COUNTER_REG

#define MC0_CH1_CR_PWM_DDR_SUBCH1_WRDATA_COUNTER_REG                   (0x0000ED18)
//Duplicate of MC0_CH0_CR_PWM_DDR_SUBCH0_WRDATA_COUNTER_REG

#define MC0_CH1_CR_DDR4_MR1_RTT_NOM_DIMM1_VALUES_REG                   (0x0000ED20)
//Duplicate of MC0_CH0_CR_DDR4_MR1_RTT_NOM_DIMM1_VALUES_REG

#define MC0_CH1_CR_DDR4_MR2_RTT_WR_DIMM1_VALUES_REG                    (0x0000ED28)
//Duplicate of MC0_CH0_CR_DDR4_MR1_RTT_NOM_DIMM1_VALUES_REG

#define MC0_CH1_CR_DDR4_MR6_VREF_DIMM1_VALUES_0_REG                    (0x0000ED30)
//Duplicate of MC0_CH0_CR_DDR4_MR6_VREF_DIMM1_VALUES_0_REG

#define MC0_CH1_CR_DDR4_MR6_VREF_DIMM1_VALUES_1_REG                    (0x0000ED38)
//Duplicate of MC0_CH0_CR_DDR4_MR6_VREF_DIMM1_VALUES_0_REG

#define MC0_CH1_CR_DDR4_MR1_ODIC_DIMM1_VALUES_REG                      (0x0000ED44)
//Duplicate of MC0_CH0_CR_DDR4_MR1_ODIC_DIMM1_VALUES_REG

#define MC0_CH1_CR_DDR4_MR5_RTT_PARK_VALUES_REG                        (0x0000ED48)
//Duplicate of MC0_CH0_CR_DDR4_MR1_RTT_NOM_DIMM1_VALUES_REG

#define MC0_CH1_CR_DDR4_MR5_RTT_PARK_DIMM1_VALUES_REG                  (0x0000ED50)
//Duplicate of MC0_CH0_CR_DDR4_MR1_RTT_NOM_DIMM1_VALUES_REG

#define MC0_CH1_CR_DDR4_MR1_RTT_NOM_VALUES_REG                         (0x0000ED58)
//Duplicate of MC0_CH0_CR_DDR4_MR1_RTT_NOM_DIMM1_VALUES_REG

#define MC0_CH1_CR_LPDDR4_DISCRETE_MR_VALUES_0_REG                     (0x0000ED60)
//Duplicate of MC0_CH0_CR_LPDDR4_DISCRETE_MR_VALUES_0_REG

#define MC0_CH1_CR_LPDDR4_DISCRETE_MR_VALUES_1_REG                     (0x0000ED68)
//Duplicate of MC0_CH0_CR_LPDDR4_DISCRETE_MR_VALUES_0_REG

#define MC0_CH1_CR_LPDDR4_DISCRETE_MR_VALUES_2_REG                     (0x0000ED70)
//Duplicate of MC0_CH0_CR_LPDDR4_DISCRETE_MR_VALUES_0_REG

#define MC0_CH1_CR_LPDDR4_DISCRETE_MR_VALUES_3_REG                     (0x0000ED78)
//Duplicate of MC0_CH0_CR_LPDDR4_DISCRETE_MR_VALUES_0_REG

#define MC0_CH1_CR_LPDDR4_DISCRETE_MR_VALUES_4_REG                     (0x0000ED80)
//Duplicate of MC0_CH0_CR_LPDDR4_DISCRETE_MR_VALUES_0_REG

#define MC0_CH1_CR_LPDDR4_DISCRETE_MR_VALUES_5_REG                     (0x0000ED88)
//Duplicate of MC0_CH0_CR_LPDDR4_DISCRETE_MR_VALUES_0_REG

#define MC0_CH1_CR_LPDDR4_DISCRETE_MR_VALUES_6_REG                     (0x0000ED90)
//Duplicate of MC0_CH0_CR_LPDDR4_DISCRETE_MR_VALUES_0_REG

#define MC0_CH1_CR_LPDDR4_DISCRETE_MR_VALUES_7_REG                     (0x0000ED98)
//Duplicate of MC0_CH0_CR_LPDDR4_DISCRETE_MR_VALUES_0_REG

#define MC0_CH1_CR_DDR4_MR0_MR1_CONTENT_REG                            (0x0000EDA0)
//Duplicate of MC0_CH0_CR_DDR4_MR0_MR1_CONTENT_REG

#define MC0_CH1_CR_DDR4_MR2_MR3_CONTENT_REG                            (0x0000EDA4)
//Duplicate of MC0_CH0_CR_DDR4_MR2_MR3_CONTENT_REG

#define MC0_CH1_CR_DDR4_MR4_MR5_CONTENT_REG                            (0x0000EDA8)
//Duplicate of MC0_CH0_CR_DDR4_MR4_MR5_CONTENT_REG

#define MC0_CH1_CR_DDR4_MR6_MR7_CONTENT_REG                            (0x0000EDAC)
//Duplicate of MC0_CH0_CR_DDR4_MR6_MR7_CONTENT_REG

#define MC0_CH1_CR_DDR4_MR2_RTT_WR_VALUES_REG                          (0x0000EDB0)
//Duplicate of MC0_CH0_CR_DDR4_MR1_RTT_NOM_DIMM1_VALUES_REG

#define MC0_CH1_CR_DDR4_MR6_VREF_VALUES_0_REG                          (0x0000EDB8)
//Duplicate of MC0_CH0_CR_DDR4_MR6_VREF_DIMM1_VALUES_0_REG

#define MC0_CH1_CR_DDR4_MR6_VREF_VALUES_1_REG                          (0x0000EDC0)
//Duplicate of MC0_CH0_CR_DDR4_MR6_VREF_DIMM1_VALUES_0_REG

#define MC0_CH1_CR_LPDDR_MR_CONTENT_REG                                (0x0000EDC8)
//Duplicate of MC0_CH0_CR_LPDDR_MR_CONTENT_REG

#define MC0_CH1_CR_MRS_FSM_CONTROL_REG                                 (0x0000EDD0)
//Duplicate of MC0_CH0_CR_MRS_FSM_CONTROL_REG

#define MC0_CH1_CR_MRS_FSM_RUN_REG                                     (0x0000EDD8)
//Duplicate of MC0_CH0_CR_MRS_FSM_RUN_REG

#define MC0_CH1_CR_DDR4_MR1_ODIC_VALUES_REG                            (0x0000EDDC)
//Duplicate of MC0_CH0_CR_DDR4_MR1_ODIC_DIMM1_VALUES_REG

#define MC0_CH1_CR_PL_AGENT_CFG_DTF_REG                                (0x0000EDE0)
//Duplicate of MC0_CH0_CR_PL_AGENT_CFG_DTF_REG

#define MC0_CH1_CR_MCMNTS_GLOBAL_DRIVER_GATE_CFG_REG                   (0x0000EDE4)
//Duplicate of MC0_MCMAINS_GLOBAL_DRIVER_GATE_CFG_REG

#define MC0_CH1_CR_DDR4_ECC_DEVICE_VALUES_REG                          (0x0000EDE8)
//Duplicate of MC0_CH0_CR_DDR4_ECC_DEVICE_VALUES_REG

#define MC0_CH1_CR_DDR4_ECC_DEVICE_DIMM1_VALUES_REG                    (0x0000EDF0)
//Duplicate of MC0_CH0_CR_DDR4_ECC_DEVICE_VALUES_REG

#define MC0_CH1_CR_MCMNTS_SPARE2_REG                                   (0x0000EDF8)
//Duplicate of MC0_CH0_CR_MCMNTS_SPARE2_REG

#define MC0_CH1_CR_MCMNTS_SPARE_REG                                    (0x0000EDFC)
//Duplicate of MC0_CH0_CR_MCMNTS_SPARE_REG

#define MC0_CH1_CR_GENERIC_MRS_FSM_CONTROL_0_REG                       (0x0000EE00)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_CH1_CR_GENERIC_MRS_FSM_CONTROL_1_REG                       (0x0000EE04)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_CH1_CR_GENERIC_MRS_FSM_CONTROL_2_REG                       (0x0000EE08)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_CH1_CR_GENERIC_MRS_FSM_CONTROL_3_REG                       (0x0000EE0C)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_CH1_CR_GENERIC_MRS_FSM_CONTROL_4_REG                       (0x0000EE10)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_CH1_CR_GENERIC_MRS_FSM_CONTROL_5_REG                       (0x0000EE14)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_CH1_CR_GENERIC_MRS_FSM_CONTROL_6_REG                       (0x0000EE18)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_CH1_CR_GENERIC_MRS_FSM_CONTROL_7_REG                       (0x0000EE1C)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_CH1_CR_GENERIC_MRS_FSM_CONTROL_8_REG                       (0x0000EE20)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_CH1_CR_GENERIC_MRS_FSM_CONTROL_9_REG                       (0x0000EE24)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_CH1_CR_GENERIC_MRS_FSM_CONTROL_10_REG                      (0x0000EE28)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_CH1_CR_GENERIC_MRS_FSM_CONTROL_11_REG                      (0x0000EE2C)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_CH1_CR_GENERIC_MRS_FSM_CONTROL_12_REG                      (0x0000EE30)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_CH1_CR_GENERIC_MRS_FSM_CONTROL_13_REG                      (0x0000EE34)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_CH1_CR_GENERIC_MRS_FSM_CONTROL_14_REG                      (0x0000EE38)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_CH1_CR_GENERIC_MRS_FSM_CONTROL_15_REG                      (0x0000EE3C)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_CH1_CR_GENERIC_MRS_FSM_CONTROL_16_REG                      (0x0000EE40)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_CH1_CR_GENERIC_MRS_FSM_CONTROL_17_REG                      (0x0000EE44)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_CH1_CR_GENERIC_MRS_FSM_CONTROL_18_REG                      (0x0000EE48)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_CH1_CR_GENERIC_MRS_FSM_CONTROL_19_REG                      (0x0000EE4C)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_CH1_CR_GENERIC_MRS_FSM_CONTROL_20_REG                      (0x0000EE50)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_CH1_CR_GENERIC_MRS_FSM_CONTROL_21_REG                      (0x0000EE54)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_CH1_CR_GENERIC_MRS_FSM_CONTROL_22_REG                      (0x0000EE58)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_CH1_CR_GENERIC_MRS_FSM_CONTROL_23_REG                      (0x0000EE5C)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_CH1_CR_GENERIC_MRS_FSM_CONTROL_24_REG                      (0x0000EE60)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_CH1_CR_GENERIC_MRS_FSM_CONTROL_25_REG                      (0x0000EE64)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_CH1_CR_GENERIC_MRS_FSM_CONTROL_26_REG                      (0x0000EE68)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_CH1_CR_GENERIC_MRS_FSM_CONTROL_27_REG                      (0x0000EE6C)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_CH1_CR_GENERIC_MRS_FSM_CONTROL_28_REG                      (0x0000EE70)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_CH1_CR_GENERIC_MRS_FSM_CONTROL_29_REG                      (0x0000EE74)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_CH1_CR_GENERIC_MRS_FSM_CONTROL_30_REG                      (0x0000EE78)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_CH1_CR_GENERIC_MRS_FSM_CONTROL_31_REG                      (0x0000EE7C)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_CH1_CR_GENERIC_MRS_FSM_CONTROL_32_REG                      (0x0000EE80)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_CH1_CR_GENERIC_MRS_FSM_CONTROL_33_REG                      (0x0000EE84)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_CH1_CR_GENERIC_MRS_FSM_CONTROL_34_REG                      (0x0000EE88)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_CH1_CR_GENERIC_MRS_FSM_CONTROL_35_REG                      (0x0000EE8C)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_CH1_CR_GENERIC_MRS_FSM_CONTROL_36_REG                      (0x0000EE90)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_CH1_CR_GENERIC_MRS_FSM_CONTROL_37_REG                      (0x0000EE94)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_CH1_CR_GENERIC_MRS_FSM_CONTROL_38_REG                      (0x0000EE98)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_CH1_CR_GENERIC_MRS_FSM_CONTROL_39_REG                      (0x0000EE9C)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_CH1_CR_GENERIC_MRS_FSM_CONTROL_40_REG                      (0x0000EEA0)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_CH1_CR_GENERIC_MRS_FSM_CONTROL_41_REG                      (0x0000EEA4)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_CH1_CR_GENERIC_MRS_FSM_CONTROL_42_REG                      (0x0000EEA8)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_CH1_CR_GENERIC_MRS_FSM_CONTROL_43_REG                      (0x0000EEAC)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_CH1_CR_GENERIC_MRS_FSM_CONTROL_44_REG                      (0x0000EEB0)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_CH1_CR_GENERIC_MRS_FSM_CONTROL_45_REG                      (0x0000EEB4)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_CH1_CR_GENERIC_MRS_FSM_CONTROL_46_REG                      (0x0000EEB8)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_CH1_CR_GENERIC_MRS_FSM_CONTROL_47_REG                      (0x0000EEBC)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_CH1_CR_GENERIC_MRS_FSM_CONTROL_48_REG                      (0x0000EEC0)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_CH1_CR_GENERIC_MRS_FSM_CONTROL_49_REG                      (0x0000EEC4)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_CH1_CR_GENERIC_MRS_FSM_CONTROL_50_REG                      (0x0000EEC8)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_CH1_CR_GENERIC_MRS_FSM_CONTROL_51_REG                      (0x0000EECC)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_CH1_CR_GENERIC_MRS_FSM_CONTROL_52_REG                      (0x0000EED0)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_CH1_CR_GENERIC_MRS_FSM_CONTROL_53_REG                      (0x0000EED4)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_CH1_CR_GENERIC_MRS_FSM_CONTROL_54_REG                      (0x0000EED8)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_CH1_CR_GENERIC_MRS_FSM_CONTROL_55_REG                      (0x0000EEDC)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_CH1_CR_GENERIC_MRS_FSM_CONTROL_56_REG                      (0x0000EEE0)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_CH1_CR_GENERIC_MRS_FSM_CONTROL_57_REG                      (0x0000EEE4)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_CH1_CR_GENERIC_MRS_FSM_CONTROL_58_REG                      (0x0000EEE8)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_CH1_CR_GENERIC_MRS_FSM_CONTROL_59_REG                      (0x0000EEEC)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_CH1_CR_GENERIC_MRS_FSM_CONTROL_60_REG                      (0x0000EEF0)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_CH1_CR_GENERIC_MRS_FSM_CONTROL_61_REG                      (0x0000EEF4)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_CH1_CR_GENERIC_MRS_FSM_CONTROL_62_REG                      (0x0000EEF8)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_CH1_CR_GENERIC_MRS_FSM_CONTROL_63_REG                      (0x0000EEFC)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_CH1_CR_GENERIC_MRS_FSM_CONTROL_64_REG                      (0x0000EF00)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_CH1_CR_GENERIC_MRS_FSM_CONTROL_65_REG                      (0x0000EF04)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_CH1_CR_GENERIC_MRS_FSM_CONTROL_66_REG                      (0x0000EF08)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_CH1_CR_GENERIC_MRS_FSM_CONTROL_67_REG                      (0x0000EF0C)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_CH1_CR_GENERIC_MRS_FSM_CONTROL_68_REG                      (0x0000EF10)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_CH1_CR_GENERIC_MRS_FSM_CONTROL_69_REG                      (0x0000EF14)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_CH1_CR_GENERIC_MRS_FSM_CONTROL_70_REG                      (0x0000EF18)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_CH1_CR_GENERIC_MRS_FSM_CONTROL_71_REG                      (0x0000EF1C)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_CH1_CR_GENERIC_MRS_FSM_CONTROL_72_REG                      (0x0000EF20)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_CH1_CR_GENERIC_MRS_FSM_CONTROL_73_REG                      (0x0000EF24)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_CH1_CR_GENERIC_MRS_FSM_CONTROL_74_REG                      (0x0000EF28)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_CH1_CR_GENERIC_MRS_FSM_CONTROL_75_REG                      (0x0000EF2C)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_CH1_CR_GENERIC_MRS_FSM_CONTROL_76_REG                      (0x0000EF30)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_CH1_CR_GENERIC_MRS_FSM_CONTROL_77_REG                      (0x0000EF34)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_CH1_CR_GENERIC_MRS_FSM_CONTROL_78_REG                      (0x0000EF38)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_CH1_CR_GENERIC_MRS_FSM_CONTROL_79_REG                      (0x0000EF3C)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_CH1_CR_GENERIC_MRS_FSM_CONTROL_80_REG                      (0x0000EF40)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_CH1_CR_GENERIC_MRS_FSM_CONTROL_81_REG                      (0x0000EF44)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_CH1_CR_GENERIC_MRS_FSM_CONTROL_82_REG                      (0x0000EF48)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_CH1_CR_GENERIC_MRS_FSM_CONTROL_83_REG                      (0x0000EF4C)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_CH1_CR_GENERIC_MRS_FSM_CONTROL_84_REG                      (0x0000EF50)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_CH1_CR_GENERIC_MRS_FSM_CONTROL_85_REG                      (0x0000EF54)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_CH1_CR_GENERIC_MRS_FSM_CONTROL_86_REG                      (0x0000EF58)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_CH1_CR_GENERIC_MRS_FSM_CONTROL_87_REG                      (0x0000EF5C)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_CH1_CR_GENERIC_MRS_FSM_CONTROL_88_REG                      (0x0000EF60)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_CH1_CR_GENERIC_MRS_FSM_CONTROL_89_REG                      (0x0000EF64)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_CH1_CR_GENERIC_MRS_FSM_CONTROL_90_REG                      (0x0000EF68)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_CH1_CR_GENERIC_MRS_FSM_CONTROL_91_REG                      (0x0000EF6C)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_CH1_CR_GENERIC_MRS_FSM_CONTROL_92_REG                      (0x0000EF70)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_CH1_CR_GENERIC_MRS_FSM_CONTROL_93_REG                      (0x0000EF74)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_CH1_CR_GENERIC_MRS_FSM_CONTROL_94_REG                      (0x0000EF78)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_CH1_CR_GENERIC_MRS_FSM_CONTROL_95_REG                      (0x0000EF7C)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_CH1_CR_GENERIC_MRS_FSM_CONTROL_96_REG                      (0x0000EF80)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_CH1_CR_GENERIC_MRS_FSM_CONTROL_97_REG                      (0x0000EF84)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_CH1_CR_GENERIC_MRS_FSM_CONTROL_98_REG                      (0x0000EF88)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_CH1_CR_GENERIC_MRS_FSM_CONTROL_99_REG                      (0x0000EF8C)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_CH1_CR_GENERIC_MRS_FSM_CONTROL_100_REG                     (0x0000EF90)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_CH1_CR_GENERIC_MRS_FSM_CONTROL_101_REG                     (0x0000EF94)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_CH1_CR_GENERIC_MRS_FSM_CONTROL_102_REG                     (0x0000EF98)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_CH1_CR_GENERIC_MRS_FSM_CONTROL_103_REG                     (0x0000EF9C)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_CH1_CR_GENERIC_MRS_FSM_CONTROL_104_REG                     (0x0000EFA0)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_CH1_CR_GENERIC_MRS_FSM_CONTROL_105_REG                     (0x0000EFA4)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_CH1_CR_GENERIC_MRS_FSM_CONTROL_106_REG                     (0x0000EFA8)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_CH1_CR_GENERIC_MRS_FSM_CONTROL_107_REG                     (0x0000EFAC)
//Duplicate of MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_REG

#define MC0_CH1_CR_MRH_CONFIG_REG                                      (0x0000EFFC)
//Duplicate of MC0_CH0_CR_MRH_CONFIG_REG
#pragma pack(pop)
#endif
