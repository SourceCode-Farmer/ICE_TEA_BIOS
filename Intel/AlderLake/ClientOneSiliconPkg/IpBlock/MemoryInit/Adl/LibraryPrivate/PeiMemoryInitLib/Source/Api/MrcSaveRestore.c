/** @file

  Power state and boot mode save and restore data functions.

@copyright
  INTEL CONFIDENTIAL
  Copyright 1999 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains an 'Intel Peripheral Driver' and is uniquely identified as
  "Intel Reference Module" and is licensed for Intel CPUs and chipsets under
  the terms of your license agreement with Intel or your vendor. This file may
  be modified by the user, subject to additional terms of the license agreement.

@par Specification Reference:
**/

//
// Include files
//

#include "MrcTypes.h"
#include "MrcApi.h"
#include "MrcCommon.h"
#include "MrcGeneral.h"
#include "MrcGlobal.h"
#include "MrcSaveRestore.h"
#include "MrcSpdProcessing.h"
#include "MrcChipApi.h"

#define MRC_NUM_BYTE_GROUPS     (8)
#define MRC_NUM_BYTE_GROUPS_ECC (10)
#define R64BIT                  (0xFFFF)

//
// ------- IMPORTANT NOTE --------
// MRC_REGISTER_COUNT_COMMON and MRC_REGISTER_COUNT_SAGV in MrcInterface.h should match these tables.
// Update these define's whenever you add/remove registers from the tables below.

// Total Save/Restore size:
//
// A0: 2556 + 12128 * 4 (per SAGV point) = 51068 bytes
// B0: 2580 + 12104 * 4 (per SAGV point) = 50996 bytes
// MAX (A0, B0)       = 2580 + 12128 * 4 = 51092 bytes (common for A0/B0)
//-------------------------------------
// These registers must be saved only once, they don't depend on SA GV point.
// A0: 80 + 2128 + 168 + 180 = 2556
// B0: 80 + 2128 + 176 + 196 = 2580
// MAX (A0, B0) = 2580
//-------------------------------------

const SaveDataControl SaveDataCommonPerByte[] = {
  // DATA[0:9]CH[0:1]  4 * 10 * 2 = 80 bytes
  {0x12C, 0x12C}, // 4 WRRETRAINSWIZZLECONTROL - DATA0CH0
};

// "R64BIT" indicates the 64bit field instead of the end offset used in 32 bit registers.
const SaveDataControl SaveDataCommonPerCh[] = {
  // MC[0-1] CH[0-1]  532 * 2 * 2 = 2128
  {0xE068, R64BIT},  //  8  ScWdbwm
  {0xE0BC, 0xE0BC},  //  4  ScGsCfgTraining
  {0xE0D0, 0xE0D0},  //  4  GlobalDriverGateCfg
  {0xE110, 0xE110},  //  4  WmmReadConfig
  {0xE120, 0xE120},  //  4  MERGE_REQ_READS_PQ
  {0xE128, R64BIT},  //  8  ROWHAMMER_CTL
  {0xE3F0, R64BIT},  //  8  RH_TRR_LFSR
  {0xE400, 0xE400},  //  4  RH_TRR_CONTROL
  {0xE410, 0xE410},  //  4  DdrMrParams
  {0xE424, 0xE428},  //  8  LpddrMr4, Ddr4MprRankTemperature
  {0xE430, R64BIT},  //  8  DeswizzleLowErm
  {0xE454, 0xE454},  //  4  McInitState
  {0xE480, R64BIT},  //  8  Deswizzlelow
  {0xE498, R64BIT},  //  8  DeswizzleHighErm
  {0xE4B8, R64BIT},  //  8  DeswizzleHigh
  {0xE5E0, 0xE5E4},  //  8  PL_AGENT_CFG_DTF, MCMNTS_GLOBAL_DRIVER_GATE_CFG
  {0xE600, 0xE7AC},  //  432 GenericMrcFsmControl[0:107]
                     // Total per channel: (4 * 7) + (8 * 9) + 432 = 532
};

const SaveDataControl SaveDataCommonPerMc[] = {
  // MC[0-1] : 84 * 2 = 168
  {0xD800, 0xD810},  //  20 MadInterChannel, MadIntraCh[0-1], MadDimmCh[0-1]
  {0xD818, 0xD81C},  //  8  McdecsMisc, McdecsCbit
  {0xD824, 0xD828},  //  8  ChannelHash, ChannelEHash
//{0xD830, 0xD830},  //  8  McInitStateG
  {0xD880, 0xD880},  //  4  QueueCreditC
  {0xD938, 0xD938},  //  4  McMainsGlobalDriverGateCfg
  {0xD954, 0xD954},  //  4  McdecsSecondCbit
  {0xD9B8, 0xD9B8},  //  4  MadMcHash
  {0xDF00, 0xDF00},  //  4  DdrplCfgDtf
  {0xD860, 0xD860},  //  4  PmSrefConfig
  {0xD930, R64BIT},  //  8  ScQos
  {0xD93C, 0xD93C},  //  4  PmControl
  {0xD960, R64BIT},  //  8  ScQos2
  {0xD968, 0xD968},  //  4  ScQos3
                     // Total per MC: (4 * 8) + (8 * 4) + 20 = 84
};
 const SaveDataControl SaveDataCommonA0[] = {
  // PHY global / PCU: 180
  {0x01AC, 0x01AC},  //  4 DATA0CH0_CR_WLCTRL - Restore will read this one and multicast to all DATA fubs
  {0x01DC, 0x01DC},  //  4 DATA0CH0_CR_PMFSM1 - Restore will read this one and multicast to all DATA fubs
  {0x291C, 0x2924},  //  12 VTTCOMPOFFSET, VTTCOMPOFFSET2, EARLY_RSTRDONE  - VTT0
  {0x299C, 0x29A4},  //  12 VTTCOMPOFFSET, VTTCOMPOFFSET2, EARLY_RSTRDONE  - VTT1
  {0x2A1C, 0x2A24},  //  12 VTTCOMPOFFSET, VTTCOMPOFFSET2, EARLY_RSTRDONE  - VTT2
  {0x2A9C, 0x2AA4},  //  12 VTTCOMPOFFSET, VTTCOMPOFFSET2, EARLY_RSTRDONE  - VTT3
  {0x2C5C, 0x2C5C},  //  4  EARLYCR - DDRPHY_COMP
  {0x2C88, 0x2C94},  //  16 VIEWDIGTX_CTRL, VIEWDIGTX_SEL, VIEWANA_CTRL, VIEWANA_CBBSEL - DDRPHY COMP
  {0x2CCC, 0x2CCC},  //  4  MISCCOMPCODES   - DDRPHY COMP
  {0x2CDC, 0x2CE4},  //  12 DDRCRCMDCOMP, DDRCRCLKCOMP, DDRCRCTLCOMP   - DDRPHY COMP
  {0x2D1C, 0x2D20},  //  8 DATA0_GLOBAL_CR_PMTIMER0/1   - Restore will read these and multicast to all DATA_GLOBAL fubs
  {0x3120, 0x3124},  //  8 CCC0_GLOBAL_CR_PM_FSM_OVRD_0/1
  {0x31A0, 0x31A4},  //  8 CCC1_GLOBAL_CR_PM_FSM_OVRD_0/1
  {0x3220, 0x3224},  //  8 CCC2_GLOBAL_CR_PM_FSM_OVRD_0/1
  {0x32A0, 0x32A4},  //  8 CCC3_GLOBAL_CR_PM_FSM_OVRD_0/1
  {0x313C, 0x3140},  //  8 CCC0_GLOBAL_CR_PMTIMER0/1    - Restore will read these and multicast to all CCC_GLOBAL fubs
  {0x3304, 0x3310},  //  16 FLL_STATIC_CFG0/1, FLL_DEBUG_CFG, FLL_DYNAMIC_CFG
  {0x3320, 0x3320},  //  4  FLL_STATIC_CFG2
  {0x3630, 0x3630},  //  4 CH0CCC_CR_WLCTRL - Restore will read this one and multicast to all CCC fubs
  {0x58A4, 0x58A4},  //  4 PCU DdrVoltage
  {0x59B8, 0x59B8},  //  4 PCU PowerSaving
  {0x5D10, R64BIT},  //  8 PCU Sskpd
                     // Total for PHY global / PCU: (4 * 8) + (8 * 7) + (12 * 5) + (16 * 2) = 180
};

 const SaveDataControl SaveDataCommon[] = {
  // PHY global / PCU: 196
  {0x01AC, 0x01AC},  //  4 DATA0CH0_CR_WLCTRL - Restore will read this one and multicast to all DATA fubs
  {0x01DC, 0x01DC},  //  4 DATA0CH0_CR_PMFSM1 - Restore will read this one and multicast to all DATA fubs
  {0x2910, 0x2918},  //  12 VTTCOMPOFFSET, VTTCOMPOFFSET2, EARLY_RSTRDONE  - VTT0
  {0x2990, 0x2998},  //  12 VTTCOMPOFFSET, VTTCOMPOFFSET2, EARLY_RSTRDONE  - VTT1
  {0x2A10, 0x2A18},  //  12 VTTCOMPOFFSET, VTTCOMPOFFSET2, EARLY_RSTRDONE  - VTT2
  {0x2A90, 0x2A98},  //  12 VTTCOMPOFFSET, VTTCOMPOFFSET2, EARLY_RSTRDONE  - VTT3
  {0x2C5C, 0x2C5C},  //  4  EARLYCR - DDRPHY_COMP
  {0x2C88, 0x2C94},  //  16 VIEWDIGTX_CTRL, VIEWDIGTX_SEL, VIEWANA_CTRL, VIEWANA_CBBSEL - DDRPHY COMP
  {0x2CCC, 0x2CCC},  //  4  MISCCOMPCODES   - DDRPHY COMP
  {0x2CDC, 0x2CE4},  //  12 DDRCRCMDCOMP, DDRCRCLKCOMP, DDRCRCTLCOMP   - DDRPHY COMP
  {0x2D1C, 0x2D20},  //  8 DATA0_GLOBAL_CR_PMTIMER0/1   - Restore will read these and multicast to all DATA_GLOBAL fubs
  {0x3120, 0x3124},  //  8 CCC0_GLOBAL_CR_PM_FSM_OVRD_0/1
  {0x31A0, 0x31A4},  //  8 CCC1_GLOBAL_CR_PM_FSM_OVRD_0/1
  {0x3220, 0x3224},  //  8 CCC2_GLOBAL_CR_PM_FSM_OVRD_0/1
  {0x32A0, 0x32A4},  //  8 CCC3_GLOBAL_CR_PM_FSM_OVRD_0/1
  {0x313C, 0x3148},  //  16 CCC0_GLOBAL_CR_PMTIMER0/1, CCC0_GLOBAL_CR_COMPUPDT_DLY0/1 - Restore will read these and multicast to all CCC_GLOBAL fubs
  {0x3304, 0x3310},  //  16 FLL_STATIC_CFG0/1, FLL_DEBUG_CFG, FLL_DYNAMIC_CFG
  {0x3320, 0x3320},  //  4  FLL_STATIC_CFG2
  {0x3610, 0x3610},  //  4 CH0CCC_CR_WLCTRL - Restore will read this one and multicast to all CCC fubs
  {0x3CF0, 0x3CF0},  //  4 DDRPHY_COMP_NEW_CR_VCCDLLCOMPDATACCC1
  {0x3EBC, 0x3EBC},  //  4 MCMISCS_SPINEGATING
  {0x58A4, 0x58A4},  //  4 PCU DdrVoltage
  {0x59B8, 0x59B8},  //  4 PCU PowerSaving
  {0x5D10, R64BIT},  //  8 PCU Sskpd
                     // Total for PHY global / PCU: (4 * 10) + (8 * 6) + (12 * 5) + (16 * 3) = 196
};
//-------------------------------------
// These registers must be saved for each SA GV point.
// Total bytes to save:
// A0:  7760 + 1088 + 48 + 2736 + 496 = 12128 bytes.
// B0:  7760 + 1088 + 48 + 2736 + 472 = 12104 bytes.
//-------------------------------------

const SaveDataControl SaveDataSaGvPerByte[] = {
  // DATA[0-9]CH[0-1] : 388 * 10 * 2 = 7760
  {0x000, 0x08c}, // 144 DDRDATADQ[RANK0LANE0:RANK3LANE7], DataControl0:3
  {0x090, 0x0BC}, //  48 AFEMISC, SRZCTL,DQRXCTL[0-1], DQSTXRXCTL, DQSTXRXCTL0, SDLCTL0, SDLCTL1, MDLLCTL[0-3]
  {0x0C8, 0x0F0}, //  44 AFEMISCCTRL2, TXPBDOFFSET[0:1], COMPCTL[0:2], DCCCTL[5:9]
  {0x0F4, 0x128}, //  56 RXCONTROL0RANK[0-3], TXCONTROL0_PH90, TXCONTROL0RANK[0-3], DQSRANKX, DQSPH90RANKX, OFFSETCOMP, OFFSETTRAIN, CMDBUSTRAIN
  {0x130, 0x17C}, //  80 WRRETRAINRANK[3-0], WRRETRAINCONTROLSTATUS, TXCONTROL1_PH90, AFEMISCCTRL1, DQXRXAMPCTL, DQ[0-7]RXAMPCTL, DQRXAMPCTL[0-2], DQTXCTL0
  {0x198, 0x1A0}, //  12 CLKALIGNCTL[0-2]
  {0x1A8, 0x1A8}, //   4 DDRCRDATACOMPVTT
                  // 144 + 48 + 44 + 56 + 80 + 12 + 4 = 388
};

const SaveDataControl SaveDataSaGvCccA0[] = {
  // CH[0-7] : 136 * 8 = 1088
  {0x3480, 0x34C0},  //  68  DDRCRPINSUSED, PICODE[0:3], MDLLCTL[0:3], COMP2, COMP3, DDRCRCCCVOLTAGEUSED, DDRCRCTLCACOMPOFFSET, DDRCRVSSHICLKCOMPOFFSET, CCCRSTCTL, CTL0, CTL3
  {0x34C8, 0x34F4},  //  48  CTL2, TXPBDCODE[0:4], DCCCTL6, DCCCTL7, DCCCTL8, DCCCTL9, DCCCTL11, DCCCTL20
  {0x3500, 0x3500},  //  4   DDRCRCCCPIDIVIDER
  {0x3508, 0x3510},  //  12  CLKALIGNCTL1, CLKALIGNCTL2, DDRCRCCCCLKCONTROLS
  {0x352C, 0x352C},  //  4   TXPBDOFFSET
                     // (4 * 2) + 12 + 48 + 68 = 136
};

const SaveDataControl SaveDataSaGvCcc[] = {
  // CH[0-7] : 136 * 8 = 1088
  {0x3480, 0x34C0},  //  68  DDRCRPINSUSED, CCCRSTCTL, PICODE[0:3], MDLLCTL[0:3], COMP2, COMP3, DDRCRCCCVOLTAGEUSED, DDRCRCTLCACOMPOFFSET, DDRCRVSSHICLKCOMPOFFSET, CTL0, CTL3
  {0x34C8, 0x34F4},  //  48  CTL2, TXPBDCODE[0:4], DCCCTL6, DCCCTL7, DCCCTL8, DCCCTL9, DCCCTL11, DCCCTL20
  {0x3500, 0x3500},  //  4   DDRCRCCCPIDIVIDER
  {0x3508, 0x350C},  //  8   CLKALIGNCTL1, CLKALIGNCTL2
  {0x3514, 0x3518},  //  8   DDRCRCCCCLKCONTROLS, TXPBDOFFSET
                     // (4 * 1) + (8 * 2) + 48 + 68 = 136
};

const SaveDataControl SaveDataSaGvCccGlobal[] = {
  // CCC[0-3] : 12 * 4 = 48
  {0x3130, 0x3138},  //  12  CCC0_GLOBAL_CR_TCOCOMP_0, CCC0_GLOBAL_CR_TCOCOMP_1, CCC0_GLOBAL_CR_TCOCOMP_2
};

const SaveDataControl SaveDataSaGvPerCh[] = {
  // MC[0-1]CH[0-1] : 684 * 2 * 2 = 2736
  {0xE000, R64BIT},  //  8  TcPre(64bit)
  {0xE008, 0xE01C},  //  24 TcAct, TcRdRd, TcRdWr, TcWrRd, TcWrWr, AdaptiveCke
  {0xE020, R64BIT},  //  8  RoundTrip latency(64bit)
  {0xE028, 0xE02C},  //  8  SchedCbit, SchedSecondCbit
  {0xE034, 0xE034},  //  4  ScPcit
  {0xE040, R64BIT},  //  8  PmPdwnCfg
  {0xE050, R64BIT},  //  8  TcPwrdn
  {0xE060, R64BIT},  //  8  QueueEntryDisableWpq
  {0xE070, R64BIT},  //  8  TcOdt
  {0xE078, 0xE078},  //  4  McSchedsSpare
  {0xE080, 0xE080},  //  4  ScOdtMatrix
  {0xE088, R64BIT},  //  8  ScGsCfg
  {0xE090, 0xE0A0},  //  20 ScPhThrottling[0:3], ScWpqThreshold
  {0xE0A8, R64BIT},  //  8  ScPrCntCfg
  {0xE0B8, 0xE0B8},  //  4  SpidLowPowerCtl
  {0xE0C0, 0xE0C4},  //  8  SchedThirdCbit, DeadLockBreaker
  {0xE0C8, R64BIT},  //  8  TcBubbleInj
  {0xE0D4, 0xE0D4},  //  4  ScBlockingRulesCfg
  {0xE0F8, R64BIT},  //  8  WckConfig
  {0xE100, 0xE100},  //  4  CfgBubbleInj
  {0xE114, 0xE114},  //  4  Mc2phybgfctrl
  {0xE118, R64BIT},  //  8  AdaptivePcit
  {0xE200, 0xE2EC},  //  240 GenericMrsFsmStorageValues[0:59]
  {0xE3E0, 0xE3E4},  //  8   GenericMrsFsmTimingStorage0, GenericMrsFsmTimingStorage1
  {0xE40C, 0xE40C},  //  4  TcRefm
  {0xE438, 0xE444},  //  16 TcRfp, TcRftp, TcSrftp, McRefreshStagger
  {0xE448, R64BIT},  //  8  TcZqcal
  {0xE450, 0xE450},  //  4  TcMr4Shaddow
  {0xE478, 0xE478},  //  4  ScWrDelay
  {0xE488, 0xE488},  //  4  ScPbr
  {0xE494, 0xE494},  //  4  TcLpddr4Misc
  {0xE4C0, R64BIT},  //  8  TcSrExitTp
  {0xE4C8, 0xE4C8},  //  4  Lp4DqsOscillatorParams
  {0XE520, R64BIT},  //  8  Mr1RttNomDimm1Values
  {0XE528, R64BIT},  //  8  Mr2RttNomDimm1Values
  {0XE530, R64BIT},  //  8  Mr6VrefDimm1Values0
  {0XE538, R64BIT},  //  8  Mr6VrefDimm1Values1
  {0XE544, 0XE544},  //  4  Mr1OdicDimm1Values
  {0XE548, R64BIT},  //  8  Mr5RttParkValues
  {0XE550, R64BIT},  //  8  Mr5RttParkDimm1Values
  {0XE558, R64BIT},  //  8  Mr5RttNonValues
  {0XE560, R64BIT},  //  8  DiscreteMrValues0
  {0XE568, R64BIT},  //  8  DiscreteMrValues1
  {0XE570, R64BIT},  //  8  DiscreteMrValues2
  {0XE578, R64BIT},  //  8  DiscreteMrValues3
  {0XE580, R64BIT},  //  8  DiscreteMrValues4
  {0XE588, R64BIT},  //  8  DiscreteMrValues5
  {0XE590, R64BIT},  //  8  DiscreteMrValues6
  {0XE598, R64BIT},  //  8  DiscreteMrValues7
  {0xE5A0, 0xE5AC},  //  16 Ddr4Mr0Mr1Content, Ddr4Mr2Mr3Content, Ddr4Mr4Mr5Content, Ddr4Mr6Mr7Content
  {0XE5B0, R64BIT},  //  8  Mr2RttWrValues
  {0XE5B8, R64BIT},  //  8  Mr6VrefValues0
  {0XE5C0, R64BIT},  //  8  Mr6VrefValues1
  {0xE5C8, R64BIT},  //  8  MpddrMrContent
  {0xE5D0, R64BIT},  //  8  MrsFsmControl
  {0xE5DC, 0xE5DC},  //  4  Mr1OdicValue
  {0xE5E8, R64BIT},  //  8  EccDevice
  {0xE5F0, R64BIT},  //  8  EccDeviceDimm1
  {0xE5FC, 0xE5FC},  //  4  McMntsspare
                     // (4 * 16) + (8 * 38) + (16 * 2) + (20 * 1) + 24 + 240 = 684
};

 const SaveDataControl SaveDataSaGvA0[] = {
  // 496
  {0x2800, 0x2808},  //  12 AFE_CTRL0, AFE_CTRL_NEW, AFE_CTRL2 - VCC DLL0
  {0x2880, 0x2888},  //  12 AFE_CTRL0, AFE_CTRL_NEW, AFE_CTRL2 - VCC DLL1
  {0x2900, 0x2908},  //  12 AFE_CTRL1, AFE_CTRL2_NEW , AFE_CTRL3_NEW - VTT0
  {0x2980, 0x2988},  //  12 AFE_CTRL1, AFE_CTRL2_NEW , AFE_CTRL3_NEW - VTT1
  {0x2A00, 0x2A08},  //  12 AFE_CTRL1, AFE_CTRL2_NEW , AFE_CTRL3_NEW - VTT2
  {0x2A80, 0x2A88},  //  12 AFE_CTRL1, AFE_CTRL2_NEW , AFE_CTRL3_NEW - VTT3
  {0x2914, 0x2914},  //  4  VTTGENCONTROL  - VTT0
  {0x2994, 0x2994},  //  4  VTTGENCONTROL  - VTT1
  {0x2A14, 0x2A14},  //  4  VTTGENCONTROL  - VTT2
  {0x2A94, 0x2A94},  //  4  VTTGENCONTROL  - VTT3
  {0x2C08, 0x2C24},  //  32 COMP: RCOMPCTRL1, RCOMPCTRL, RCOMP_EXTCOMP, RCOMP_RELCOMP, PNCCOMP_CTRL0, PNCCOMP_CTRL1, PNCCOMP, SCOMP
  {0x2C28, 0x2C3C},  //  24 COMP: RXDQSCOMP_RXCODE, RXDQSCOMP_CMN_CTRL0, RXDQSCOMP_CMN_CTRL1_NEW, RXDQSCOMP, DLLCOMP_DCCCTRL, DLLCOMP_LDOCTRL0
  {0x2C40, 0x2C58},  //  28 COMP: DLLCOMP_LDOCTRL1_NEW, DLLCOMP_PIDELAY, DLLCOMP_PICTRL, DLLCOMP_DLLCTRL, DLLCOMP_VCDLCTRL, DLLCOMP_VDLLCTRL, DLLCOMP_SWCAPCTRL
  {0x2C60, 0x2C74},  //  24 COMP: VSSHICOMP_CTRL0, VSSHICOMP_CTRL1, VSSHICOMP_FFCOMP, COMPOVERRIDE, VSXHIOPAMPCH1_CTRL, VSXHIOPAMPCH0_CTRL
  {0x2C80, 0x2C84},  //  8  COMP: COMPDLLWL, TXCONTROL
  {0x2CA8, 0x2CA8},  //  4  COMP: DCCCTL4
  {0x2CC0, 0x2CC8},  //  12 COMP: IOLVRFFCTL, RXDQSCOMP_CMN_CTRL1, LDOCTRL1
  {0x2CD0, 0x2CD4},  //  8  COMP: COMPDLLSTRTUP, FSMSKIPCTRL
  {0x2CE8, 0x2CFC},  //  24 COMP: COMPCTL[0-4], SCOMPCTL
  {0x3100, 0x3100},  //  4  IOLVRCTL - CCC0 GLOBAL
  {0x3180, 0x3180},  //  4  IOLVRCTL - CCC1 GLOBAL
  {0x3200, 0x3200},  //  4  IOLVRCTL - CCC2 GLOBAL
  {0x3280, 0x3280},  //  4  IOLVRCTL - CCC3 GLOBAL
  {0x3114, 0x3114},  //  4  CCCDRXVREF - CCC0 GLOBAL
  {0x3194, 0x3194},  //  4  CCCDRXVREF - CCC1 GLOBAL
  {0x3214, 0x3214},  //  4  CCCDRXVREF - CCC2 GLOBAL
  {0x3294, 0x3294},  //  4  CCCDRXVREF - CCC3 GLOBAL
  {0x3300, 0x3300},  //  4  FLL_CMD_CFG
  {0x3C80, 0x3CA4},  //  40 COMP_NEW: PNCCOMP_CTRL3, DDRCRDATACOMP1, DDRCRDATACOMPVTT, VCCDLLCOMPDATACCC, DIMMVREF_VREFCONTROL, DIMMVREF_VREFCH[0-1], VREFADJUST2, VSSHICOMP_CTRL[2-3]
  {0x3E00, 0x3E1C},  //  32 DDRSCRAMBLECH[0-7]
  {0x3E20, 0x3E28},  //  12 DDRMISCCONTROL[0-2]
  {0x3E2C, 0x3E68},  //  64 WRITECFGCH[0:7], READCFGCH[0:7]
  {0x3E74, 0x3E74},  //  4  WRITECFGCH01
  {0x3E80, 0x3E9C},  //  32 WRITECFGCH23, WRITECFGCH45, WRITECFGCH67, READCFGCH01, READCFGCH23, READCFGCH45, READCFGCH67, DDRMISCCONTROL7
  {0x3EA8, 0x3EB4},  //  16 RXDQFIFORDENCH01, RXDQFIFORDENCH23, RXDQFIFORDENCH45, RXDQFIFORDENCH67
  {0x3EBC, 0x3EC0},  //  8  DDRWCKCONTROL, DDRWCKCONTROL1
                     // (4 * 15) + (8 * 3) + (12 * 8) + (16 * 1) + (24 * 3) + 28 + (32 * 3) + 40 + 64 = 496
};

 const SaveDataControl SaveDataSaGv[] = {
  // 472
  {0x2800, 0x2808},  //  12 AFE_CTRL0, AFE_CTRL_NEW, AFE_CTRL2 - VCC DLL0
  {0x2880, 0x2888},  //  12 AFE_CTRL0, AFE_CTRL_NEW, AFE_CTRL2 - VCC DLL1
  {0x2908, 0x2908},  //  4  VTTGENCONTROL  - VTT0
  {0x2988, 0x2988},  //  4  VTTGENCONTROL  - VTT1
  {0x2A08, 0x2A08},  //  4  VTTGENCONTROL  - VTT2
  {0x2A88, 0x2A88},  //  4  VTTGENCONTROL  - VTT3
  {0x2C08, 0x2C24},  //  32 COMP: RCOMPCTRL1, RCOMPCTRL, RCOMP_EXTCOMP, RCOMP_RELCOMP, PNCCOMP_CTRL0, PNCCOMP_CTRL1, PNCCOMP, SCOMP
  {0x2C28, 0x2C3C},  //  24 COMP: RXDQSCOMP_RXCODE, RXDQSCOMP_CMN_CTRL0, RXDQSCOMP_CMN_CTRL1_NEW (B0) / VCCDDQLDO_DVFSCOMP_CTRL0 (Q0), RXDQSCOMP_ENABLE, DLLCOMP_DCCCTRL, DLLCOMP_LDOCTRL0
  {0x2C40, 0x2C58},  //  28 COMP: DLLCOMP_LDOCTRL1_NEW, DLLCOMP_PIDELAY, DLLCOMP_PICTRL, DLLCOMP_DLLCTRL, DLLCOMP_VCDLCTRL, DLLCOMP_VDLLCTRL, DLLCOMP_SWCAPCTRL
  {0x2C60, 0x2C74},  //  24 COMP: VSSHICOMP_CTRL0, VSSHICOMP_CTRL1, VSSHICOMP_FFCOMP, COMPOVERRIDE, VSXHIOPAMPCH1_CTRL, VSXHIOPAMPCH0_CTRL
  {0x2C80, 0x2C84},  //  8  COMP: COMPDLLWL, TXCONTROL
  {0x2CA8, 0x2CA8},  //  4  COMP: DCCCTL4
  {0x2CC0, 0x2CC8},  //  12 COMP: IOLVRFFCTL, RXDQSCOMP_CMN_CTRL1, LDOCTRL1
  {0x2CD0, 0x2CD4},  //  8  COMP: COMPDLLSTRTUP, FSMSKIPCTRL
  {0x2CE8, 0x2CFC},  //  24 COMP: COMPCTL[0-4], SCOMPCTL
  {0x3100, 0x3100},  //  4  IOLVRCTL - CCC0 GLOBAL
  {0x3180, 0x3180},  //  4  IOLVRCTL - CCC1 GLOBAL
  {0x3200, 0x3200},  //  4  IOLVRCTL - CCC2 GLOBAL
  {0x3280, 0x3280},  //  4  IOLVRCTL - CCC3 GLOBAL
  {0x3114, 0x3114},  //  4  CCCDRXVREF - CCC0 GLOBAL
  {0x3194, 0x3194},  //  4  CCCDRXVREF - CCC1 GLOBAL
  {0x3214, 0x3214},  //  4  CCCDRXVREF - CCC2 GLOBAL
  {0x3294, 0x3294},  //  4  CCCDRXVREF - CCC3 GLOBAL
  {0x3300, 0x3300},  //  4  FLL_CMD_CFG
  {0x3C80, 0x3CAC},  //  48 COMP_NEW: PNCCOMP_CTRL3, DDRCRDATACOMP1, DDRCRDATACOMPVTT, VCCDLLCOMPDATACCC, DIMMVREF_VREFCONTROL, DIMMVREF_VREFCH[0-1], VREFADJUST2, VSSHICOMP_CTRL[2-3], DCSCTL0, DCSCTL1
  {0x3CB4, 0x3CB4},  //  4  COMP_NEW: DCSCTL3
  {0x3CCC, 0x3CD4},  //  12 COMP_NEW: DCSRUNMODE, TXDLLSIGGRPDCCCTL34, TXDLLSIGGRPDCCCTL35
  {0x3E00, 0x3E1C},  //  32 DDRSCRAMBLECH[0-7]
  {0x3E20, 0x3E28},  //  12 DDRMISCCONTROL[0-2]
  {0x3E2C, 0x3E68},  //  64 WRITECFGCH[0:7], READCFGCH[0:7]
  {0x3E80, 0x3EA0},  //  36 WRITECFGCH01, WRITECFGCH23, WRITECFGCH45, WRITECFGCH67, READCFGCH01, READCFGCH23, READCFGCH45, READCFGCH67, DDRMISCCONTROL7
  {0x3EAC, 0x3EB8},  //  16 RXDQFIFORDENCH01, RXDQFIFORDENCH23, RXDQFIFORDENCH45, RXDQFIFORDENCH67
  {0x3EC0, 0x3EC4},  //  8  DDRWCKCONTROL, DDRWCKCONTROL1
                     // (4 * 15) + (8 * 3) + (12 * 5) + (16 * 1) + (24 * 3) + 28 + (32 * 2) + 36 + 48 + 64 = 472
};
 typedef enum {
   SaveReg,
   RestoreReg
 } SaveRestore;

/**
  This function verifies that neither CPU fuses or DIMMs have changed.

  @param[in] MrcData - Include all MRC global data.

  @retval mrcSuccess if fast boot is allowed, otherwise mrcColdBootRequired.
**/
MrcStatus
MrcFastBootPermitted (
  IN     MrcParameters *const MrcData
  )
{
  const MrcInput            *Inputs;
  const MRC_FUNCTION        *MrcCall;
  const MrcControllerIn     *ControllerIn;
  const MrcChannelIn        *ChannelIn;
  const MrcDimmIn           *DimmIn;
  const UINT8               *CrcStart;
  MrcOutput                 *Outputs;
  MrcDebug                  *Debug;
  MrcSaveData               *Save;
  MrcContSave               *ControllerSave;
  MrcChannelSave            *ChannelSave;
  MrcDimmOut                *DimmSave;
  MrcCapabilityId           Capid0Reg;
  UINT32                    CrcSize;
  UINT32                    Offset;
  UINT16                    DimmCrc;
  UINT8                     Controller;
  UINT8                     Channel;
  UINT8                     Dimm;

  CrcStart = NULL;
  CrcSize  = 0;
  Inputs   = &MrcData->Inputs;
  MrcCall = Inputs->Call.Func;
  Save     = &MrcData->Save.Data;
  Outputs  = &MrcData->Outputs;
  Debug    = &Outputs->Debug;

  // Obtain the capabilities of the memory controller and see if they have changed.
  Offset = Inputs->PciEBaseAddress + MrcCall->MrcGetPcieDeviceAddress (0, 0, 0, CAPID0_A_0_0_0_PCI_REG);
  Capid0Reg.A = MrcCall->MrcMmioRead32 (Offset);
  Offset = Inputs->PciEBaseAddress + MrcCall->MrcGetPcieDeviceAddress (0, 0, 0, CAPID0_B_0_0_0_PCI_REG);
  Capid0Reg.B = MrcCall->MrcMmioRead32 (Offset);
  Offset = Inputs->PciEBaseAddress + MrcCall->MrcGetPcieDeviceAddress (0, 0, 0, CAPID0_C_0_0_0_PCI_REG);
  Capid0Reg.C = MrcCall->MrcMmioRead32 (Offset);
  Offset = Inputs->PciEBaseAddress + MrcCall->MrcGetPcieDeviceAddress (0, 0, 0, CAPID0_E_0_0_0_PCI_REG);
  Capid0Reg.E = MrcCall->MrcMmioRead32 (Offset);
  if ((Capid0Reg.A != Save->McCapId.A) ||
      (Capid0Reg.B != Save->McCapId.B) ||
      (Capid0Reg.C != Save->McCapId.C) ||
      (Capid0Reg.E != Save->McCapId.E)) {
    MRC_DEBUG_MSG (
      Debug,
      MSG_LEVEL_NOTE,
      "Capabilities have changed, cold boot required\n '%X_%X_%X_%X' --> '%X_%X_%X_%X'\n",
      Save->McCapId.A,
      Save->McCapId.B,
      Save->McCapId.C,
      Save->McCapId.E,
      Capid0Reg.A,
      Capid0Reg.B,
      Capid0Reg.C,
      Capid0Reg.E
      );
    return mrcColdBootRequired;
  }
  // See if any of the DIMMs have changed.
  for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
    ControllerIn   = &Inputs->Controller[Controller];
    ControllerSave = &Save->Controller[Controller];
    for (Channel = 0; Channel < MAX_CHANNEL; Channel++) {
      ChannelIn   = &ControllerIn->Channel[Channel];
      ChannelSave = &ControllerSave->Channel[Channel];
      for (Dimm = 0; Dimm < MAX_DIMMS_IN_CHANNEL; Dimm++) {
        DimmIn   = &ChannelIn->Dimm[Dimm];
        DimmSave = &ChannelSave->Dimm[Dimm];
        if (DimmIn->Status == DIMM_DISABLED) {
          DimmCrc = 0;
        } else {
          CrcStart = MrcSpdCrcArea (MrcData, Controller, Channel, Dimm, &CrcSize);
          GetDimmCrc ((const UINT8 *const) CrcStart, CrcSize, &DimmCrc);
        }

        MRC_DEBUG_MSG (
          Debug,
          MSG_LEVEL_NOTE,
          "Channel %u Dimm %u DimmCrc %Xh, DimmSave->Crc %Xh\n",
          Channel,
          Dimm,
          DimmCrc,
          DimmSave->Crc
          );
        if (DimmCrc != DimmSave->Crc) {
          MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Dimm has changed, cold boot required\n");
          return mrcColdBootRequired;
        }
      }
    }
  }
  // Set RestoreMRs flag to use trained Opt Param Values for Power Savings.
  Outputs->RestoreMRs = TRUE;
  Outputs->Ddr5MrInitDone = TRUE;

  return mrcSuccess;
}

/**
  This function saves or restores register values based on the SaveRestore flag.

  @param[in, out] MrcData         - Include all the MRC global data.
  @param[in]      SaveRestoreDataCtrlPtr
                                  - Registers data group to be saved or restored.
  @param[in]      SizeoOfData     - Total size of the register group.
  @param[in]      RegOffset       - Register offset increment of the register group.
  @param[in, out] McRegister      - Save or Restore registers value in mrc save data.
  @param[in]      SaveRestore     - Control flag to save registers or to restore registers.
  @param[in]      SkipPrint       - Debug prints control flag.

  @retval         LocalByteCount  - Returns number of bytes saved/restored.
**/
UINT32
SaveRestoreRegisters (
  IN OUT  MrcParameters   *const MrcData,
  IN      SaveDataControl const *SaveRestoreDataCtrlPtr,
  IN      UINT32  SizeoOfData,
  IN      UINT32  RegOffset,
  IN OUT  UINT32  **McRegister,
  IN      UINT8   SaveRestore,
  IN      BOOLEAN SkipPrint
)
{
  UINT32                Offset;
  UINT32                Index;
  UINT32                Value;
  UINT64                Value64;
  UINT32                LocalByteCount;
  MrcDebug              *Debug;
  MrcDebugMsgLevel      DebugLevel;

  Debug = &MrcData->Outputs.Debug;
  DebugLevel = SkipPrint ? MSG_LEVEL_NEVER : MSG_LEVEL_NOTE;
  Value64 = 0;
  Value = 0;

  LocalByteCount = 0;
  for (Index = 0; Index < SizeoOfData / sizeof (SaveDataControl); Index++) {
    for (Offset = SaveRestoreDataCtrlPtr->StartMchbarOffset; Offset <= SaveRestoreDataCtrlPtr->EndMchbarOffset; Offset += sizeof (UINT32)) {
      if (SaveRestore == SaveReg) {
        if (SaveRestoreDataCtrlPtr->EndMchbarOffset == R64BIT) {
          Value64 = MrcReadCR64 (MrcData, RegOffset + Offset);
          MRC_DEBUG_MSG (Debug, DebugLevel, "Index: %02d, Offset: %08x, Value: 0x%016llx\n", Index, Offset + RegOffset, Value64);
          *(UINT64 *)(*McRegister)++ = Value64;
          (*McRegister)++;
          LocalByteCount += 8;
          break;
        } else {
          Value = MrcReadCR (MrcData, RegOffset + Offset);
          MRC_DEBUG_MSG (Debug, DebugLevel, "Index: %02d, Offset: %08x, Value: 0x%08x\n", Index, Offset + RegOffset, Value);
          *(*McRegister)++ = Value;
          LocalByteCount += 4;
        }
      } else {
        if (SaveRestoreDataCtrlPtr->EndMchbarOffset == R64BIT) {
          Value64 = *(UINT64 *)(*McRegister)++;
          MrcWriteCR64 (MrcData, RegOffset + Offset, Value64);
          MRC_DEBUG_MSG (Debug, DebugLevel, "Index: %02d, Offset: %08x, Value: 0x%016llx\n", Index, Offset + RegOffset, Value64);
          (*McRegister)++;
          LocalByteCount += 8;
          break;
        } else {
          Value = *(*McRegister)++;
          LocalByteCount += 4;
          MrcWriteCR (MrcData, RegOffset + Offset, Value);
          MRC_DEBUG_MSG (Debug, DebugLevel, "Index: %02d, Offset: %08x, Value: 0x%08x\n", Index, Offset + RegOffset, Value);
        }
      }
    }
    SaveRestoreDataCtrlPtr++;
  }
  return LocalByteCount;
}

/**
  This function saves any values that need to be used during non-cold boots.

  @param[in, out] MrcData - Include all the MRC global data.

  @retval mrcSuccess if the save occurred with no errors, otherwise returns an error code.
**/
MrcStatus
MrcSaveMCValues (
  IN OUT MrcParameters *const MrcData
  )
{
  const MrcInput        *Inputs;
  const MrcControllerIn *ControllerIn;
  const MrcChannelIn    *ChannelIn;
  const MrcSpd          *SpdIn;
  const MRC_FUNCTION    *MrcCall;
  MrcIntOutput          *MrcIntData;
  MrcOutput             *Outputs;
  MrcDebug              *Debug;
  MrcSaveData           *SaveData;
  MrcSaveHeader         *SaveHeader;
  MrcControllerOut      *ControllerOut;
  MrcChannelOut         *ChannelOut;
  MrcContSave           *ControllerSave;
  MrcChannelSave        *ChannelSave;
  MrcProfile            Profile;
  UINT32                *McRegister;
  UINT32                *McRegisterStart;
  UINT32                RegOffset;
  UINT32                Byte;
  UINT8                 *SpdBegin;
  UINT8                 Controller;
  UINT8                 Channel;
  UINT8                 Dimm;
  UINT8                 SaveRestore;  // @todo used for future simplification of single save/restore function call.
  UINT32                LocalByteCount;
  UINT32                TotalByteCount;
  BOOLEAN               SkipPrint;
  UINT32                NumBytesGroup;
  BOOLEAN               A0;

  // Copy channel and DIMM information to the data area that will be saved.
  Inputs      = &MrcData->Inputs;
  MrcCall     = Inputs->Call.Func;
  Outputs     = &MrcData->Outputs;
  SaveData    = &MrcData->Save.Data;
  SaveHeader  = &MrcData->Save.Header;
  Debug       = &Outputs->Debug;
  SaveRestore = SaveReg; // @todo used for future simplification of single save/restore function call. Filled from input parameter.
  SkipPrint   = TRUE;
  A0          = Inputs->A0;

  MrcFlushRegisterCachedData (MrcData);
  MrcIntData  = ((MrcIntOutput *) (MrcData->IntOutputs.Internal));
  TotalByteCount = 0;
  NumBytesGroup = Outputs->EccSupport ? MRC_NUM_BYTE_GROUPS_ECC : MRC_NUM_BYTE_GROUPS;

  for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
    ControllerIn                  = &Inputs->Controller[Controller];
    ControllerOut                 = &Outputs->Controller[Controller];
    ControllerSave                = &SaveData->Controller[Controller];
    ControllerSave->ChannelCount  = ControllerOut->ChannelCount;
    ControllerSave->Status        = ControllerOut->Status;
    ControllerSave->ValidChBitMask = ControllerOut->ValidChBitMask;
    ControllerSave->FirstPopCh     = ControllerOut->FirstPopCh;

    for (Channel = 0; Channel < MAX_CHANNEL; Channel++) {
      ChannelIn   = &ControllerIn->Channel[Channel];
      ChannelOut  = &ControllerOut->Channel[Channel];
      ChannelSave                   = &ControllerSave->Channel[Channel];
      ChannelSave->DimmCount        = ChannelOut->DimmCount;
      ChannelSave->ValidRankBitMask = ChannelOut->ValidRankBitMask;
      ChannelSave->ValidSubChBitMask= ChannelOut->ValidSubChBitMask;
      ChannelSave->ValidByteMask    = ChannelOut->ValidByteMask;
      ChannelSave->Status           = ChannelOut->Status;
      if ((Inputs->SaGv != MrcSaGvEnabled) ||
          (MrcIntData->SaGvPoint == MrcSaGvPoint1) ||
          (Outputs->FreqMax >= SaveData->Frequency[MrcIntData->SaGvPoint - 1])
         ) {
        // Save Timings when current frequency is higher than previous SAGV point, or SAGV is disabled
        if (MrcChannelExist (MrcData, Controller, Channel)) {
          // Check for channel presense, as SaveData->Timing[] is global (not per channel). All populated channels have the same timings.
          for (Profile = STD_PROFILE; Profile < MAX_PROFILE; Profile++) {
            MrcCall->MrcCopyMem ((UINT8 *) &SaveData->Timing[Profile], (UINT8 *) &ChannelOut->Timing[Profile], sizeof (MrcTiming));
          }
        }
      }
      SaveData->Mr10PdaEnabled |= (ChannelOut->Mr10PdaEnabled);
      SaveData->Mr11PdaEnabled |= (ChannelOut->Mr11PdaEnabled);
      MrcCall->MrcCopyMem ((UINT8 *) ChannelSave->DqsMapCpu2Dram, (UINT8 *) ChannelIn->DqsMapCpu2Dram, sizeof (ChannelIn->DqsMapCpu2Dram));
      for (Dimm = 0; Dimm < MAX_DIMMS_IN_CHANNEL; Dimm++) {
        MrcCall->MrcCopyMem ((UINT8 *) &ChannelSave->Dimm[Dimm], (UINT8 *) &ChannelOut->Dimm[Dimm], sizeof (MrcDimmOut));
        SpdIn = &ChannelIn->Dimm[Dimm].Spd.Data;
        if (Outputs->DdrType == MRC_DDR_TYPE_DDR5) {
          SpdBegin = (UINT8 *) &SpdIn->Ddr5.ManufactureInfo;
          ChannelSave->DimmSpdSave [Dimm].SpdDramDeviceType        = SpdIn->Ddr5.Base.DramDeviceType.Data;
          ChannelSave->DimmSpdSave [Dimm].SpdModuleType            = SpdIn->Ddr5.Base.ModuleType.Data;
          ChannelSave->DimmSpdSave [Dimm].SpdModuleMemoryBusWidth  = SpdIn->Ddr5.ModuleCommon.ModuleMemoryBusWidth.Data;
        } else { // DDR4 / LP4 / LP5
          SpdBegin = (UINT8 *) &SpdIn->Ddr4.ManufactureInfo;
          ChannelSave->DimmSpdSave [Dimm].SpdDramDeviceType        = SpdIn->Ddr4.Base.DramDeviceType.Data;
          ChannelSave->DimmSpdSave [Dimm].SpdModuleType            = SpdIn->Ddr4.Base.ModuleType.Data;
          ChannelSave->DimmSpdSave [Dimm].SpdModuleMemoryBusWidth  = SpdIn->Ddr4.Base.ModuleMemoryBusWidth.Data;
        }
        // Save just enough SPD information so it can be restored during non-cold boot.
        MrcCall->MrcCopyMem ((UINT8 *) &ChannelSave->DimmSpdSave[Dimm].SpdSave[0], SpdBegin, sizeof (ChannelSave->DimmSpdSave[Dimm].SpdSave));
      } // for Dimm
    } // for Channel
  } // for Controller

  for (Profile = STD_PROFILE; Profile < MAX_PROFILE; Profile++) {
    SaveData->VddVoltage[Profile]  = Outputs->VddVoltage[Profile];
    SaveData->VddqVoltage[Profile] = Outputs->VddqVoltage[Profile];
    SaveData->VppVoltage[Profile]  = Outputs->VppVoltage[Profile];
  }
  SaveData->ValidRankMask  = Outputs->ValidRankMask;
  SaveData->ValidChBitMask = Outputs->ValidChBitMask;
  SaveData->ValidMcBitMask = Outputs->ValidMcBitMask;

  // Copy specified memory controller MMIO registers to the data area that will be saved.
  // Start with the common section.
  if ((Inputs->SaGv == MrcSaGvEnabled) && (MrcIntData->SaGvPoint != MrcSaGvPoint4)) {
     // If SA GV is enabled, only save the Common registers at the last point (currently High).
  } else {

    McRegister = SaveData->RegSaveCommon;
    RegOffset  = 0;
    if (A0) {
      LocalByteCount = SaveRestoreRegisters (MrcData, SaveDataCommonA0, sizeof (SaveDataCommonA0), RegOffset, (void *)&McRegister, SaveRestore, SkipPrint);
    } else {
      LocalByteCount = SaveRestoreRegisters (MrcData, SaveDataCommon, sizeof (SaveDataCommon), RegOffset, (void *)&McRegister, SaveRestore, SkipPrint);
    }

    TotalByteCount += LocalByteCount;
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\t%s ByteCount: %d, TotalByteCount: %d\n", "SaveDataCommon", LocalByteCount, TotalByteCount);

    for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
      // Common per-MC registers
      RegOffset = INC_OFFSET_CALC_CH (MC0_MAD_INTER_CHANNEL_REG, MC1_MAD_INTER_CHANNEL_REG, Controller);
      {
        LocalByteCount = SaveRestoreRegisters (MrcData, SaveDataCommonPerMc, sizeof (SaveDataCommonPerMc), RegOffset, (void *)&McRegister, SaveRestore, SkipPrint);
      }
      TotalByteCount += LocalByteCount;
      MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "MC%d\t%s ByteCount: %d, TotalByteCount: %d\n", Controller, "SaveDataCommonPerMc", LocalByteCount, TotalByteCount);

      // Common per-Ch registers
      for (Channel = 0; Channel < MAX_CHANNEL / 2; Channel++) {
        RegOffset = INC_OFFSET_CALC_MC_CH (MC0_CH0_CR_WMM_READ_CONFIG_REG, MC1_CH0_CR_WMM_READ_CONFIG_REG, Controller, MC0_CH1_CR_WMM_READ_CONFIG_REG, Channel);
        LocalByteCount = SaveRestoreRegisters (MrcData, SaveDataCommonPerCh, sizeof (SaveDataCommonPerCh), RegOffset, (void *)&McRegister, SaveRestore, SkipPrint);
        TotalByteCount += LocalByteCount;
        MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "MC%d C%d\t%s ByteCount: %d, TotalByteCount: %d\n", Controller, Channel, "SaveDataCommonPerCh", LocalByteCount, TotalByteCount);
      }

      // Common per-byte registers
      for (Byte = 0; Byte < NumBytesGroup; Byte++) {
        RegOffset = INC_OFFSET_CALC_MC_CH (DATA0CH0_CR_DDRCRWRRETRAINSWIZZLECONTROL_REG, DATA0CH1_CR_DDRCRWRRETRAINSWIZZLECONTROL_REG, Controller, DATA1CH0_CR_DDRCRWRRETRAINSWIZZLECONTROL_REG, Byte);
        LocalByteCount = SaveRestoreRegisters (MrcData, SaveDataCommonPerByte, sizeof (SaveDataCommonPerByte), RegOffset, (void *)&McRegister, SaveRestore, SkipPrint);
        TotalByteCount += LocalByteCount;
        MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "MC%d B%d\t%s ByteCount: %d, TotalByteCount: %d\n", Controller, Byte, "SaveDataCommonPerByte", LocalByteCount, TotalByteCount);
      }
    }

    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Common section: saved %d bytes\n\n", (McRegister - SaveData->RegSaveCommon) * 4);
    if ((UINT32) (McRegister - SaveData->RegSaveCommon) > MRC_REGISTER_COUNT_COMMON) {
      MRC_DEBUG_MSG (Debug, MSG_LEVEL_ERROR, "\n%s RegSaveCommon overflow!\n", gErrString);
      return mrcFail;
    }
  } // if SAGV and Low point
  // Sagv save registers total count
  TotalByteCount = 0;
  McRegister = SaveData->SaGvRegSave[MrcIntData->SaGvPoint];
  McRegisterStart = McRegister;

  // PHY global per SAGV
  RegOffset       = 0;
  if (A0) {
    LocalByteCount = SaveRestoreRegisters (MrcData, SaveDataSaGvA0, sizeof (SaveDataSaGvA0), RegOffset, (void *)&McRegister, SaveRestore, SkipPrint);
  } else {
    LocalByteCount = SaveRestoreRegisters (MrcData, SaveDataSaGv, sizeof (SaveDataSaGv), RegOffset, (void *)&McRegister, SaveRestore, SkipPrint);
  }
  TotalByteCount += LocalByteCount;

  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\t%s ByteCount: %d, TotalByteCount: %d\n", "SaveDataSaGv", LocalByteCount, TotalByteCount);

  // SAGV MC and PHY registers
  for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
    for (Channel = 0; Channel < MAX_CHANNEL / 2; Channel++) {
      RegOffset = INC_OFFSET_CALC_MC_CH (MC0_CH0_CR_TC_PRE_REG, MC1_CH0_CR_TC_PRE_REG, Controller, MC0_CH1_CR_TC_PRE_REG, Channel);
      LocalByteCount = SaveRestoreRegisters (MrcData, SaveDataSaGvPerCh, sizeof (SaveDataSaGvPerCh), RegOffset, (void *)&McRegister, SaveRestore, SkipPrint);
      TotalByteCount += LocalByteCount;
      MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "MC%d C%d\t%s ByteCount: %d, TotalByteCount: %d\n", Controller, Channel, "SaveDataSaGvPerCh", LocalByteCount, TotalByteCount);
    }

    // Per-byte registers
    for (Byte = 0; Byte < NumBytesGroup; Byte++) {
      RegOffset = INC_OFFSET_CALC_MC_CH (DATA0CH0_CR_DDRDATADQRANK0LANE0_REG, DATA0CH1_CR_DDRDATADQRANK0LANE0_REG, Controller, DATA1CH0_CR_DDRDATADQRANK0LANE0_REG, Byte);
      LocalByteCount = SaveRestoreRegisters (MrcData, SaveDataSaGvPerByte, sizeof (SaveDataSaGvPerByte), RegOffset, (void *)&McRegister, SaveRestore, SkipPrint);
      TotalByteCount += LocalByteCount;
      MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "MC%d B%d\t%s ByteCount: %d, TotalByteCount: %d\n", Controller, Byte, "SaveDataSaGvPerByte", LocalByteCount, TotalByteCount);
    }
  }

  // SAGV per-CCC registers
  for (Channel = 0; Channel < (MAX_CONTROLLER * MAX_CCC_PER_CHANNEL); Channel++) {
    RegOffset = INC_OFFSET_CALC_CH (CH2CCC_CR_MDLLCTL0_REG, CH0CCC_CR_MDLLCTL0_REG, Channel);
    if (A0) {
      LocalByteCount = SaveRestoreRegisters (MrcData, SaveDataSaGvCccA0, sizeof (SaveDataSaGvCccA0), RegOffset, (void *)&McRegister, SaveRestore, SkipPrint);
    } else {
      LocalByteCount = SaveRestoreRegisters (MrcData, SaveDataSaGvCcc, sizeof (SaveDataSaGvCcc), RegOffset, (void *)&McRegister, SaveRestore, SkipPrint);
    }
    TotalByteCount += LocalByteCount;
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "CCC%d\t%s ByteCount: %d, TotalByteCount: %d\n", Channel, "SaveDataSaGvCcc", LocalByteCount, TotalByteCount);
  }

  // SAGV per-CCC_GLOBAL registers
  for (Channel = 0; Channel < (MAX_CCC_GLOBAL_INSTANCES); Channel++) {
    RegOffset = INC_OFFSET_CALC_CH (CCC0_GLOBAL_CR_TCOCOMP_0_REG, CCC1_GLOBAL_CR_TCOCOMP_0_REG, Channel);
    LocalByteCount = SaveRestoreRegisters (MrcData, SaveDataSaGvCccGlobal, sizeof (SaveDataSaGvCccGlobal), RegOffset, (void *)&McRegister, SaveRestore, SkipPrint);
    TotalByteCount += LocalByteCount;
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "CCC_GLOBAL%u\t%s ByteCount: %u, TotalByteCount: %u\n", Channel, "SaveDataSaGvCccGlobal", LocalByteCount, TotalByteCount);
  }

  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, " SAGV section: saved %d bytes\n", (McRegister - McRegisterStart) * 4);
  if ((UINT32) (McRegister - McRegisterStart) > MRC_REGISTER_COUNT_SAGV) {
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_ERROR, "\n%s SaGvRegSave overflow!\n", gErrString);
    return mrcFail;
  }

  MrcVersionGet (MrcData, &SaveData->Version);
  SaveData->CpuModel               = Inputs->CpuModel;
  SaveData->CpuStepping            = Inputs->CpuStepping;
  SaveData->CpuFamily              = Inputs->CpuFamily;
  SaveData->Frequency[MrcIntData->SaGvPoint] = Outputs->Frequency;
  SaveData->BurstLength            = Outputs->BurstLength;
  SaveData->MemoryClock            = Outputs->MemoryClock;
  SaveData->Ratio                  = Outputs->Ratio;
  SaveData->RefClk                 = Outputs->RefClk;
  SaveData->EccSupport             = Outputs->EccSupport;
  SaveData->DdrType                = Outputs->DdrType;
  SaveData->Lpddr                  = Outputs->Lpddr;
  SaveData->Lp4x                   = Outputs->Lp4x;
  SaveData->LpByteMode             = Outputs->LpByteMode;
  SaveData->PdaEnable              = Outputs->PdaEnable;
  SaveData->EnhancedChannelMode    = Outputs->EnhancedChannelMode;
  SaveData->TCRSensitiveHynixDDR4  = Outputs->TCRSensitiveHynixDDR4;
  SaveData->TCRSensitiveMicronDDR4 = Outputs->TCRSensitiveMicronDDR4;
  SaveData->XmpProfileEnable       = Outputs->XmpProfileEnable;
  SaveData->XmpConfigWarning       = (SaveData->XmpConfigWarning || Outputs->XmpConfigWarning);
  SaveData->MaxChannels            = Outputs->MaxChannels;
  SaveData->FirstPopController     = Outputs->FirstPopController;
  SaveData->Vdd2Mv                 = Outputs->Vdd2Mv;
  SaveData->MeStolenSize           = Inputs->MeStolenSize;
  SaveData->ImrAlignment           = Inputs->ImrAlignment;
  SaveData->HighGear[MrcIntData->SaGvPoint] = Outputs->HighGear;
  SaveData->FreqMax                = Outputs->FreqMax;
#ifdef UP_SERVER_FLAG
  if(Inputs->BoardType == btUpServer) {
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Output UP CLTM TSOD Offset\nValue = c0d0:%xh\tcod1:%xh\tc1d0:%xh\tc1d1:%xh\n", Outputs->ThermOffset[0][0],Outputs->ThermOffset[0][1], Outputs->ThermOffset[1][0], Outputs->ThermOffset[1][1] );
    for (Channel = 0; Channel < MAX_CHANNEL; Channel++) {
      for (Dimm = 0; Dimm < MAX_DIMMS_IN_CHANNEL; Dimm++) {
        SaveData->ThermOffset[Channel][Dimm] = Outputs->ThermOffset[Channel][Dimm];                        ///TSOD Thermal Offset
      }
    }
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Save UP CLTM TSOD Offset  \nValue = c0d0:%xh\tcod1:%xh\tc1d0:%xh\tc1d1:%xh\n", SaveData->ThermOffset[0][0],SaveData->ThermOffset[0][1], SaveData->ThermOffset[1][0], SaveData->ThermOffset[1][1] );
  }
#endif

  SaveData->SaMemCfgCrc = MrcCalculateCrc32 ((UINT8 *) Inputs->SaMemCfgAddress.Ptr, Inputs->SaMemCfgSize);
  SaveHeader->Crc       = MrcCalculateCrc32 ((UINT8 *) SaveData, sizeof (MrcSaveData));
  MrcData->Save.Size    = sizeof (MrcSave);
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Saved data CRC = %xh\n", SaveHeader->Crc);

  return mrcSuccess;
}

/**
  This function saves any updates to values that need to be used during non-cold boots.

  @param[in, out] MrcData - Include all the MRC global data.

  @retval mrcSuccess
**/
MrcStatus
MrcUpdateSavedMCValues (
  IN OUT MrcParameters *const MrcData
  )
{
  const MrcInput        *Inputs;
  MrcSaveData           *SaveData;
  MrcSaveHeader         *SaveHeader;
  MrcStatus             Status;

  Inputs      = &MrcData->Inputs;
  SaveData    = &MrcData->Save.Data;
  SaveHeader  = &MrcData->Save.Header;
  Status      = mrcSuccess;

  // In Fast Boot, MeStolenSize may have changed. This should be updated within Save Data structure.
  SaveData->MeStolenSize           = Inputs->MeStolenSize;
  SaveData->ImrAlignment           = Inputs->ImrAlignment;

  SaveData->SaMemCfgCrc = MrcCalculateCrc32 ((UINT8 *) Inputs->SaMemCfgAddress.Ptr, Inputs->SaMemCfgSize);
  SaveHeader->Crc       = MrcCalculateCrc32 ((UINT8 *) SaveData, sizeof (MrcSaveData));
  MRC_DEBUG_MSG (&MrcData->Outputs.Debug, MSG_LEVEL_NOTE, "Saved data CRC = %xh\n", SaveHeader->Crc);

  return Status;
}

/**
  This function copies the non-training information that needs to be restored
  from the 'save' data structure to the 'Output' data structure.

  @param[in, out] MrcData - include all the MRC global data.

  @retval mrcSuccess if the copy completed with no errors, otherwise returns an error code.
**/
MrcStatus
MrcRestoreNonTrainingValues (
  IN OUT MrcParameters *const MrcData
  )
{
  MRC_FUNCTION      *MrcCall;
  MrcInput          *Inputs;
  MrcControllerIn   *ControllerIn;
  MrcChannelIn      *ChannelIn;
  MrcSaveData       *SaveData;
  MrcContSave       *ControllerSave;
  MrcChannelSave    *ChannelSave;
  MrcDimmOut        *DimmSave;
  MrcOutput         *Outputs;
  MrcDebug          *Debug;
  MrcIntOutput      *MrcIntData;
  MrcControllerOut  *ControllerOut;
  MrcChannelOut     *ChannelOut;
  MrcDimmOut        *DimmOut;
  MrcSpd            *SpdIn;
  UINT8             *SpdBegin;
  MrcProfile        Profile;
  MrcSaGvPoint      SaGvPoint;
  UINT8             Controller;
  UINT8             Channel;
  UINT8             Dimm;

  SaveData    = &MrcData->Save.Data;
  Outputs     = &MrcData->Outputs;
  Debug       = &Outputs->Debug;
  Inputs      = &MrcData->Inputs;
  MrcCall     = Inputs->Call.Func;
  MrcIntData  = ((MrcIntOutput *) (MrcData->IntOutputs.Internal));
  SaGvPoint   = MrcIntData->SaGvPoint;

  for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
    ControllerIn                = &Inputs->Controller[Controller];
    ControllerSave              = &SaveData->Controller[Controller];
    ControllerOut               = &Outputs->Controller[Controller];
    ControllerOut->ChannelCount = ControllerSave->ChannelCount;
    ControllerOut->ValidChBitMask = ControllerSave->ValidChBitMask;
    ControllerOut->Status       = ControllerSave->Status;
    ControllerOut->FirstPopCh   = ControllerSave->FirstPopCh;
    for (Channel = 0; Channel < MAX_CHANNEL; Channel++) {
      ChannelIn                     = &ControllerIn->Channel[Channel];
      ChannelSave                   = &ControllerSave->Channel[Channel];
      ChannelOut                    = &ControllerOut->Channel[Channel];
      ChannelOut->DimmCount         = ChannelSave->DimmCount;
      ChannelOut->ValidRankBitMask  = ChannelSave->ValidRankBitMask;
      ChannelOut->Status            = ChannelSave->Status;
      ChannelOut->ValidSubChBitMask = ChannelSave->ValidSubChBitMask;
      ChannelOut->ValidByteMask     = ChannelSave->ValidByteMask;
      for (Profile = STD_PROFILE; Profile < MAX_PROFILE; Profile++) {
        MrcCall->MrcCopyMem ((UINT8 *) &ChannelOut->Timing[Profile], (UINT8 *) &SaveData->Timing[Profile], sizeof (MrcTiming));
      }
      if (Inputs->EnablePda) {
        ChannelOut->Mr3PdaEnabled    = TRUE;
      //ChannelOut->Mr43PdaEnabled   = TRUE;
      //ChannelOut->Mr44PdaEnabled   = TRUE;
        Outputs->BitByteSwizzleFound = TRUE;
        ChannelOut->Mr10PdaEnabled   = SaveData->Mr10PdaEnabled;
        ChannelOut->Mr11PdaEnabled   = SaveData->Mr11PdaEnabled;
      }
      MrcCall->MrcCopyMem ((UINT8 *) ChannelIn->DqsMapCpu2Dram, (UINT8 *) ChannelSave->DqsMapCpu2Dram, sizeof (ChannelSave->DqsMapCpu2Dram));
      for (Dimm = 0; Dimm < MAX_DIMMS_IN_CHANNEL; Dimm++) {
        DimmSave = &ChannelSave->Dimm[Dimm];
        DimmOut  = &ChannelOut->Dimm[Dimm];
        if (DimmSave->Status == DIMM_PRESENT || DimmSave->Status == DIMM_DISABLED) {
          SpdIn   = &ChannelIn->Dimm[Dimm].Spd.Data;
          MrcCall->MrcCopyMem ((UINT8 *) DimmOut, (UINT8 *) DimmSave, sizeof (MrcDimmOut));
          if (SaveData->DdrType == MRC_DDR_TYPE_DDR5) {
            SpdBegin = (UINT8 *) &SpdIn->Ddr5.ManufactureInfo;
            SpdIn->Ddr5.Base.DramDeviceType.Data = ChannelSave->DimmSpdSave[Dimm].SpdDramDeviceType;
            SpdIn->Ddr5.Base.ModuleType.Data     = ChannelSave->DimmSpdSave[Dimm].SpdModuleType;
            SpdIn->Ddr5.ModuleCommon.ModuleMemoryBusWidth.Bits.PrimaryBusWidth = ChannelSave->DimmSpdSave[Dimm].SpdModuleMemoryBusWidth;
          } else { // DDR4 / LP4 / LP5
            SpdBegin = (UINT8 *) &SpdIn->Ddr4.ManufactureInfo;
            SpdIn->Ddr4.Base.DramDeviceType.Data       = ChannelSave->DimmSpdSave[Dimm].SpdDramDeviceType;
            SpdIn->Ddr4.Base.ModuleType.Data           = ChannelSave->DimmSpdSave[Dimm].SpdModuleType;
            SpdIn->Ddr4.Base.ModuleMemoryBusWidth.Data = ChannelSave->DimmSpdSave[Dimm].SpdModuleMemoryBusWidth;
          }
          // Restore just enough SPD information so it can be passed out in the HOB.
          // If SAGV enabled, only do this on the last pass, due to LPDDR VendorId patching.
          if ((Inputs->SaGv != MrcSaGvEnabled) || (SaGvPoint == MrcSaGvPoint4)) {
            MrcCall->MrcCopyMem (SpdBegin, (UINT8 *) &ChannelSave->DimmSpdSave[Dimm].SpdSave[0], sizeof (ChannelSave->DimmSpdSave[Dimm].SpdSave));
          }
        } else {
          DimmOut->Status = DimmSave->Status;
        }
      } // for Dimm
    } // for Channel
  } // for Controller

  for (Profile = STD_PROFILE; Profile < MAX_PROFILE; Profile++) {
    Outputs->VddVoltage[Profile]  = SaveData->VddVoltage[Profile];
    Outputs->VddqVoltage[Profile] = SaveData->VddqVoltage[Profile];
    Outputs->VppVoltage[Profile]  = SaveData->VppVoltage[Profile];
  }
  Outputs->ValidRankMask = SaveData->ValidRankMask;
  Outputs->ValidChBitMask = SaveData->ValidChBitMask;
  Outputs->ValidMcBitMask = SaveData->ValidMcBitMask;

// ------- IMPORTANT NOTE --------
// MeStolenSize should not be saved/restored (except on S3).  There is no rule stating that ME FW cannot request
// a different amount of ME UMA space from one boot to the next.  Also, if ME FW is updated/changed, the UMA
// Size requested from the previous version should not be restored.
//
  Inputs->CpuModel                = SaveData->CpuModel;
  Inputs->CpuStepping             = SaveData->CpuStepping;
  Inputs->CpuFamily               = SaveData->CpuFamily;
  Outputs->FreqMax                = SaveData->FreqMax;
  Outputs->MemoryClock            = SaveData->MemoryClock;
  Outputs->BurstLength            = SaveData->BurstLength;
  Outputs->Ratio                  = SaveData->Ratio;
  Outputs->RefClk                 = SaveData->RefClk;
  Outputs->EccSupport             = SaveData->EccSupport;
  Outputs->DdrType                = SaveData->DdrType;
  Outputs->Lpddr                  = SaveData->Lpddr;
  Outputs->EnhancedChannelMode    = SaveData->EnhancedChannelMode;
  Outputs->Lp4x                   = SaveData->Lp4x;
  Outputs->LpByteMode             = SaveData->LpByteMode;
  Outputs->PdaEnable              = SaveData->PdaEnable;
  Outputs->TCRSensitiveHynixDDR4  = SaveData->TCRSensitiveHynixDDR4;
  Outputs->TCRSensitiveMicronDDR4 = SaveData->TCRSensitiveMicronDDR4;
  Outputs->XmpProfileEnable       = SaveData->XmpProfileEnable;
  Outputs->XmpConfigWarning       = SaveData->XmpConfigWarning;
  Outputs->MaxChannels            = SaveData->MaxChannels;
  Outputs->FirstPopController     = SaveData->FirstPopController;
  Outputs->Vdd2Mv                 = SaveData->Vdd2Mv;
  Outputs->Frequency              = SaveData->Frequency[SaGvPoint];
  Outputs->HighFrequency          = Outputs->Frequency;
  Outputs->HighGear               = SaveData->HighGear[SaGvPoint];
  Outputs->Gear2                  = (Outputs->HighGear == 2);
  Outputs->Gear4                  = (Outputs->HighGear == 4);

  Outputs->MemoryClock      = ConvertFreq2Clock (MrcData, Outputs->Frequency);
  Outputs->Ratio            = MrcFrequencyToRatio (MrcData, Outputs->Frequency, Outputs->RefClk, Inputs->BClkFrequency);
  MRC_DEBUG_MSG (
    Debug,
    MSG_LEVEL_NOTE,
    "SAGV point %u: Frequency=%u, tCK=%ufs, Ratio=%u, Gear%d, Inputs->BClkFrequency=%x,Outputs->RefClk=%d\n",
    SaGvPoint,
    Outputs->Frequency,
    Outputs->MemoryClock,
    Outputs->Ratio,
    Outputs->HighGear,
    Inputs->BClkFrequency,
    Outputs->RefClk
  );

  Outputs->EctDone                  = TRUE;
  Outputs->LowFreqCsCmd2DSweepDone  = TRUE;

  Outputs->RestoreMRs     = TRUE;              // JEDEC Init should use trained MR values from the host struct
  Outputs->Ddr5MrInitDone = TRUE;

  if (Inputs->BootMode == bmS3) {
    Inputs->MeStolenSize      = SaveData->MeStolenSize;
    Inputs->ImrAlignment      = SaveData->ImrAlignment;
  }

#ifdef UP_SERVER_FLAG
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Save UP CLTM TSOD Offset  \nValue = c0d0:%xh\tcod1:%xh\tc1d0:%xh\tc1d1:%xh\n", SaveData->ThermOffset[0][0],SaveData->ThermOffset[0][1], SaveData->ThermOffset[1][0], SaveData->ThermOffset[1][1] );
  for (Channel = 0; Channel < MAX_CHANNEL; Channel++) {
    for (Dimm = 0; Dimm < MAX_DIMMS_IN_CHANNEL; Dimm++) {
      Outputs->ThermOffset[Channel][Dimm] = SaveData->ThermOffset[Channel][Dimm];                        ///TSOD Thermal Offset
    }
  }
MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Output UP CLTM TSOD Offset\nValue = c0d0:%xh\tcod1:%xh\tc1d0:%xh\tc1d1:%xh\n", Outputs->ThermOffset[0][0],Outputs->ThermOffset[0][1], Outputs->ThermOffset[1][0], Outputs->ThermOffset[1][1] );
#endif

  return mrcSuccess;
}

/**
  This function writes the previously determined training values back to the memory controller,
  for the SAGV section

  @param[in] MrcData    - Include all the MRC global data.
  @param[in] McRegister - Data array to restore the values from.

  @retval mrcSuccess if the memory controller write back completed with no errors, otherwise returns an error code.
**/
MrcStatus
MrcRestoreTrainingSaGv (
  IN MrcParameters *const MrcData,
  IN UINT32               *McRegister
  )
{
  const MrcInput        *Inputs;
  MrcOutput             *Outputs;
  MrcDebug              *Debug;
  UINT32                *McRegisterStart;
  UINT32                RegOffset;
  UINT32                Controller;
  UINT32                Channel;
  UINT32                Byte;
  BOOLEAN               SkipPrint;
  UINT32                NumBytesGroup;
  BOOLEAN               A0;

  Inputs   =  &MrcData->Inputs;
  Outputs   = &MrcData->Outputs;
  Debug     = &Outputs->Debug;
  SkipPrint = TRUE;
  McRegisterStart = McRegister;
  NumBytesGroup = Outputs->EccSupport ? MRC_NUM_BYTE_GROUPS_ECC : MRC_NUM_BYTE_GROUPS;
  RegOffset = 0;
  A0        = Inputs->A0;
  if (A0) {
    SaveRestoreRegisters (MrcData, SaveDataSaGvA0, sizeof (SaveDataSaGvA0), RegOffset, (void *)&McRegister, RestoreReg, SkipPrint);
  } else {
    SaveRestoreRegisters (MrcData, SaveDataSaGv, sizeof (SaveDataSaGv), RegOffset, (void *)&McRegister, RestoreReg, SkipPrint);
  }
  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Restored SaveDataSaGv\n");

  for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
    // per channel registers
    for (Channel = 0; Channel < MAX_CHANNEL / 2; Channel++) {
      RegOffset = INC_OFFSET_CALC_MC_CH (MC0_CH0_CR_TC_PRE_REG, MC1_CH0_CR_TC_PRE_REG, Controller, MC0_CH1_CR_TC_PRE_REG, Channel);
      SaveRestoreRegisters (MrcData, SaveDataSaGvPerCh, sizeof (SaveDataSaGvPerCh), RegOffset, (void *)&McRegister, RestoreReg, SkipPrint);
      MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "MC%u C%u: Restored SaveDataSaGvPerCh\n", Controller, Channel);
    }
    // Per-byte registers
    for (Byte = 0; Byte < NumBytesGroup; Byte++) {
      RegOffset = INC_OFFSET_CALC_MC_CH (DATA0CH0_CR_DDRDATADQRANK0LANE0_REG, DATA0CH1_CR_DDRDATADQRANK0LANE0_REG, Controller, DATA1CH0_CR_DDRDATADQRANK0LANE0_REG, Byte);
      SaveRestoreRegisters (MrcData, SaveDataSaGvPerByte, sizeof (SaveDataSaGvPerByte), RegOffset, (void *)&McRegister, RestoreReg, SkipPrint);
      MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "MC%u B%u: Restored SaveDataSaGvPerByte\n", Controller, Byte);
    }
  }
  // Common per-CCC registers
  for (Channel = 0; Channel < (MAX_CONTROLLER * MAX_CCC_PER_CHANNEL); Channel++) {
    RegOffset = INC_OFFSET_CALC_CH (CH2CCC_CR_MDLLCTL0_REG, CH0CCC_CR_MDLLCTL0_REG, Channel);
  if (A0) {
    SaveRestoreRegisters (MrcData, SaveDataSaGvCccA0, sizeof (SaveDataSaGvCccA0), RegOffset, (void *)&McRegister, RestoreReg, SkipPrint);
  } else {
    SaveRestoreRegisters (MrcData, SaveDataSaGvCcc, sizeof (SaveDataSaGvCcc), RegOffset, (void *)&McRegister, RestoreReg, SkipPrint);
  }
  }

  // CCC_GLOBAL registers
  for (Channel = 0; Channel < (MAX_CCC_GLOBAL_INSTANCES); Channel++) {
    RegOffset = INC_OFFSET_CALC_CH (CCC0_GLOBAL_CR_TCOCOMP_0_REG, CCC1_GLOBAL_CR_TCOCOMP_0_REG, Channel);
    SaveRestoreRegisters (MrcData, SaveDataSaGvCccGlobal, sizeof (SaveDataSaGvCccGlobal), RegOffset, (void *)&McRegister, RestoreReg, SkipPrint);
  }

  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "SAGV section: Restored %d bytes\n", (McRegister - McRegisterStart) * 4);
  if ((UINT32) (McRegister - McRegisterStart) > MRC_REGISTER_COUNT_SAGV) {
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_ERROR, "ERROR: SaGvRegSave overflow!\n");
    return mrcFail;
  }

  return mrcSuccess;
}

/**
  This function restores host structure data for CmdPiCode/CtlPiCode and update cache for RcvEn, TxDqs, TxDq
  This is needed to do CmdT and CmdV margining in RMT on Fast Flow and for LP5 since we are using Low/POR frequency switch during JEDEC reset.

  @param[in] MrcData - Include all the MRC global data.

  @retval mrcSuccess if the memory controller write back completed with no errors, otherwise returns an error code.
**/
MrcStatus
MrcRestoreCCCHostStructure (
  IN     MrcParameters *const MrcData
  )
{
  UINT32                Controller;
  UINT32                Channel;
  UINT32                Byte;
  UINT32                Rank;
  UINT8                 CmdGroup;
  UINT8                 CmdGroupMax;
  INT64                 GetSetVal;
  MrcIntCmdTimingOut    *IntCmdTiming;
  MrcIntOutput          *MrcIntData;
  MrcOutput             *Outputs;

  Outputs     = &MrcData->Outputs;
  MrcIntData  = ((MrcIntOutput *) (MrcData->IntOutputs.Internal));
  CmdGroupMax = MrcGetCmdGroupMax (MrcData);

  for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
    for (Channel = 0; Channel < Outputs->MaxChannels; Channel++) {
      if (!MrcChannelExist (MrcData, Controller, Channel)) {
        continue;
      }
      IntCmdTiming = &MrcIntData->Controller[Controller].CmdTiming[Channel];
      // Restore CMD/CTL PI settings, for Margin Limit Check
      for (CmdGroup = 0; CmdGroup < CmdGroupMax; CmdGroup++) {
        MrcGetSetCcc (MrcData, Controller, Channel, MRC_IGNORE_ARG, CmdGroup, CmdGrpPi, ReadCached, &GetSetVal);
        IntCmdTiming->CmdPiCode[CmdGroup] = (UINT16) GetSetVal;
      }
      for (Rank = 0; Rank < MAX_RANK_IN_CHANNEL; Rank++) {
        if (!MrcRankExist (MrcData, Controller, Channel, Rank)) {
          continue;
        }
        /*
         * Member CaVref is used only for DDR4.
         * Restore them only for Rank 0 per Dimm, and such saving
         * works for per-channel, too.
         */
        if (((Outputs->DdrType == MRC_DDR_TYPE_DDR4) && (Rank % MAX_RANK_IN_DIMM) == 0)) {
          MrcGetSetMcChRnk (MrcData, Controller, Channel, Rank, CmdVref, ReadCached, &GetSetVal);
          MrcIntData->Controller[Controller].CaVref[Channel][RANK_TO_DIMM_NUMBER(Rank)] = (UINT16) GetSetVal;
        }
        MrcGetSetCcc (MrcData, Controller, Channel, Rank, 0, CtlGrpPi, ReadCached, &GetSetVal);
        IntCmdTiming->CtlPiCode[Rank] = (UINT16) GetSetVal;
        for (Byte = 0; Byte < Outputs->SdramCount; Byte++) {
          // Bring RcvEn,TxDqs,TxDq  to CR cache, we shift CmdT for Margin check
          MrcGetSetStrobe (MrcData, Controller, Channel, Rank, Byte, RecEnDelay, ReadCached, &GetSetVal);
          MrcGetSetStrobe (MrcData, Controller, Channel, Rank, Byte, TxDqsDelay, ReadCached, &GetSetVal);
          MrcGetSetStrobe (MrcData, Controller, Channel, Rank, Byte, TxDqDelay,  ReadCached, &GetSetVal);
        } // Byte
      } // for Rank
    } // for Channel
  } // for Controller
  return mrcSuccess;
}
/**
  This function writes the previously determined training values back to the memory controller.
  We also have SAGV flow for S3/Warm/Fast boot here.

  @param[in] MrcData - Include all the MRC global data.

  @retval mrcSuccess if the memory controller write back completed with no errors, otherwise returns an error code.
**/
MrcStatus
MrcRestoreTrainingValues (
  IN     MrcParameters *const MrcData
  )
{
  const MrcInput        *Inputs;
  MrcDebug              *Debug;
  MrcIntOutput          *MrcIntData;
  MrcOutput             *Outputs;
  MrcSaveData           *SaveData;
  MrcStatus             Status;
  UINT32                *McRegister;
  UINT32                RegOffset;
  UINT32                Controller;
  UINT32                Channel;
  UINT32                Byte;
  UINT32                Offset;
  BOOLEAN               SkipPrint;
  BOOLEAN               A0;
  BOOLEAN               UlxUlt;
  UINT32                NumBytesGroup;
  UINT32                Data;
  DDRSCRAM_CR_DDRLASTCR_STRUCT            LastCr;
  DDRSCRAM_CR_DDRMISCCONTROL0_STRUCT      MiscControl0;

  Inputs   = &MrcData->Inputs;
  Outputs  = &MrcData->Outputs;
  Debug    = &Outputs->Debug;
  SaveData = &MrcData->Save.Data;
  A0       = Inputs->A0;
  UlxUlt   = Inputs->UlxUlt;
  MrcIntData  = ((MrcIntOutput *) (MrcData->IntOutputs.Internal));
  SkipPrint = TRUE;
  NumBytesGroup = Outputs->EccSupport ? MRC_NUM_BYTE_GROUPS_ECC : MRC_NUM_BYTE_GROUPS;

  // First restore the Common section
  McRegister = SaveData->RegSaveCommon;
  RegOffset   = 0;
  if (A0) {
    SaveRestoreRegisters (MrcData, SaveDataCommonA0, sizeof (SaveDataCommonA0), RegOffset, (void *)&McRegister, RestoreReg, SkipPrint);
  } else {
    SaveRestoreRegisters (MrcData, SaveDataCommon, sizeof (SaveDataCommon), RegOffset, (void *)&McRegister, RestoreReg, SkipPrint);
  }

  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Restored SaveDataCommon\n");

  for (Controller = 0; Controller < MAX_CONTROLLER; Controller++) {
    // Common per-Mc registers
    RegOffset = INC_OFFSET_CALC_CH (MC0_MAD_INTER_CHANNEL_REG, MC1_MAD_INTER_CHANNEL_REG, Controller);
    {
      SaveRestoreRegisters (MrcData, SaveDataCommonPerMc, sizeof (SaveDataCommonPerMc), RegOffset, (void *)&McRegister, RestoreReg, SkipPrint);
    }

    // Common per-Ch registers
    for (Channel = 0; Channel < MAX_CHANNEL / 2; Channel++) {
      RegOffset = INC_OFFSET_CALC_MC_CH (MC0_CH0_CR_WMM_READ_CONFIG_REG, MC1_CH0_CR_WMM_READ_CONFIG_REG, Controller, MC0_CH1_CR_WMM_READ_CONFIG_REG, Channel);
      SaveRestoreRegisters (MrcData, SaveDataCommonPerCh, sizeof (SaveDataCommonPerCh), RegOffset, (void *)&McRegister, RestoreReg, SkipPrint);
    }

    // Common per-byte registers
    for (Byte = 0; Byte < NumBytesGroup; Byte++) {
      RegOffset = INC_OFFSET_CALC_MC_CH (DATA0CH0_CR_DDRCRWRRETRAINSWIZZLECONTROL_REG, DATA0CH1_CR_DDRCRWRRETRAINSWIZZLECONTROL_REG, Controller, DATA1CH0_CR_DDRCRWRRETRAINSWIZZLECONTROL_REG, Byte);
      SaveRestoreRegisters (MrcData, SaveDataCommonPerByte, sizeof (SaveDataCommonPerByte), RegOffset, (void *)&McRegister, RestoreReg, SkipPrint);
    }
  }

  MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "\nCommon section: Restored %d bytes\n", (McRegister - SaveData->RegSaveCommon) * 4);
  if ((UINT32) (McRegister - SaveData->RegSaveCommon) > MRC_REGISTER_COUNT_COMMON) {
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_ERROR, "\nERROR: RegSaveCommon overflow!\n");
    return mrcFail;
  }

  // Now restore the SAGV section, RegSaveHigh is used when SAGV is disabled
  McRegister = SaveData->SaGvRegSave[MrcIntData->SaGvPoint];

  Status = MrcRestoreTrainingSaGv (MrcData, McRegister);
  if (Status != mrcSuccess) {
    return Status;
  }

  // Invalidate CR cache, in case it got some registers cached before they were restored from MrcSave
  InitializeRegisterCache (MrcData);

  // Restore Host structure data for CmdPiCode/CtlPiCode and update cache for RcvEn, TxDqs, TxDq
  // This is needed for MarginLimitCheck / RMT on Fast flow.
  if (Inputs->BootMode == bmFast) {
    MrcRestoreCCCHostStructure (MrcData);
  }

  // Sync CCC PI.
  MrcBlockTrainResetToggle (MrcData, FALSE);
  IoReset (MrcData);
  MrcBlockTrainResetToggle (MrcData, TRUE);

  // Multicast those here, instead of adding to S/R table
  MrcWriteCrMulticast (MrcData, DATA_GLOBAL_CR_DDREARLYCR_REG, 1);
  MrcWriteCrMulticast (MrcData, CCC_GLOBAL_CR_DDREARLYCR_REG, 1);
  Offset = A0 ? DDRSCRAM_CR_DDREARLYCR_A0_REG : DDRSCRAM_CR_DDREARLYCR_REG;
  MrcWriteCR (MrcData, Offset, 1);

  // Read from the first instance, these CR's are the same across all FUBs
  // Then program all FUBs using multicast

  Data = MrcReadCR (MrcData, DATA0CH0_CR_PMFSM1_REG);
  MrcWriteCrMulticast (MrcData, DATA_CR_PMFSM1_REG, Data);

  Data = MrcReadCR (MrcData, DATA0CH0_CR_WLCTRL_REG);
  MrcWriteCrMulticast (MrcData, DATA_CR_WLCTRL_REG, Data);

  Offset = A0 ? CH0CCC_CR_WLCTRL_A0_REG : CH0CCC_CR_WLCTRL_REG;
  Data = MrcReadCR (MrcData, Offset);
  Offset = A0 ? CCC_CR_WLCTRL_A0_REG : CCC_CR_WLCTRL_REG;
  MrcWriteCrMulticast (MrcData, Offset, Data);

  Data = MrcReadCR (MrcData, CCC0_GLOBAL_CR_PMTIMER0_REG);
  MrcWriteCrMulticast (MrcData, CCC_GLOBAL_CR_PMTIMER0_REG, Data);

  Data = MrcReadCR (MrcData, CCC0_GLOBAL_CR_PMTIMER1_REG);
  MrcWriteCrMulticast (MrcData, CCC_GLOBAL_CR_PMTIMER1_REG, Data);

  Data = MrcReadCR (MrcData, DATA0_GLOBAL_CR_PMTIMER0_REG);
  MrcWriteCrMulticast (MrcData, DATA_GLOBAL_CR_PMTIMER0_REG, Data);

  Data = MrcReadCR (MrcData, DATA0_GLOBAL_CR_PMTIMER1_REG);
  MrcWriteCrMulticast (MrcData, DATA_GLOBAL_CR_PMTIMER1_REG, Data);

  if (!A0) {
    Data = MrcReadCR (MrcData, CCC0_GLOBAL_CR_COMPUPDT_DLY0_REG);
    MrcWriteCrMulticast (MrcData, CCC_GLOBAL_CR_COMPUPDT_DLY0_REG, Data);

    Data = MrcReadCR (MrcData, CCC0_GLOBAL_CR_COMPUPDT_DLY1_REG);
    MrcWriteCrMulticast (MrcData, CCC_GLOBAL_CR_COMPUPDT_DLY1_REG, Data);
  }

  // Must be the last register written for basic init (Must be after MC Init).
  LastCr.Data = 0;
  LastCr.Bits.EnInitComplete = 1;
  MrcWriteCR(MrcData, DDRSCRAM_CR_DDRLASTCR_REG, LastCr.Data);

  if ((Outputs->DdrType == MRC_DDR_TYPE_DDR5) && (Inputs->BootMode != bmFast)) {
    // Let MC know that it should maintain SR via McInitState.SRX_reset
    // Otherwise CS rises during first COMP below
    MrcSelfRefreshExit (MrcData);
  }

  ForceRcomp (MrcData);

  // CCF IDP registers for MARS feature are not in Save/Restore table, initialize them here and reduce S/R size.
  MrcMarsConfig (MrcData);

  // Restore Static Spine Gating, Need to be set after EnInitComplete
  if (UlxUlt && (!(Inputs->J0) && !(Inputs->K0) && !(Inputs->Q0))) {
    LastCr.Data = MrcReadCR (MrcData, DDRSCRAM_CR_DDRLASTCR_REG);
    MiscControl0.Data = MrcReadCR (MrcData, DDRSCRAM_CR_DDRMISCCONTROL0_REG);
    // Enable Static Spine gating
    if(MiscControl0.Bits.ddrnochinterleave == 1) {
      if (!(MrcControllerExist (MrcData, cCONTROLLER0))) {
        LastCr.Bits.StaticSouthSpineGateEn = 1;
        MrcWriteCR(MrcData, DDRSCRAM_CR_DDRLASTCR_REG, LastCr.Data);
      }
    }
  }
  return mrcSuccess;
}

/**
  Calculates a CRC-32 of the specified data buffer.

  @param[in] Data     - Pointer to the data buffer.
  @param[in] DataSize - Size of the data buffer, in bytes.

  @retval The CRC-32 value.
**/
UINT32
MrcCalculateCrc32 (
  IN     const UINT8       *const Data,
  IN     const UINT32      DataSize
  )
{
  UINT32 i;
  UINT32 j;
  UINT32 crc;
  UINT32 CrcTable[256];

  crc = (UINT32) (-1);

  // Initialize the CRC base table.
  for (i = 0; i < 256; i++) {
    CrcTable[i] = i;
    for (j = 8; j > 0; j--) {
      CrcTable[i] = (CrcTable[i] & 1) ? (CrcTable[i] >> 1) ^ 0xEDB88320 : CrcTable[i] >> 1;
    }
  }
  // Calculate the CRC.
  for (i = 0; i < DataSize; i++) {
    crc = (crc >> 8) ^ CrcTable[(UINT8) crc ^ (Data)[i]];
  }

  return ~crc;
}


#ifdef UP_SERVER_FLAG
#ifdef MRC_DEBUG_PRINT
/**
  This function Print the CLTM related registers.

  @param MrcData - Include all the MRC global data.

  @retval None.
**/
void
MrcCltmPrintMchRegisters (
  MrcParameters          *MrcData
  )
{
  MrcOutput               *Outputs;
  MrcDebug                *Debug;

  Outputs                 = &MrcData->Outputs;
  Debug                   = &Outputs->Debug;

    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "UP Power weight Energy registers...\n");
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "DDR_ENERGY_SCALEFACTOR %Xh: %Xh \n", PCU_CR_DDR_ENERGY_SCALEFACTOR_PCU_REG, MrcReadCR (MrcData, PCU_CR_DDR_ENERGY_SCALEFACTOR_PCU_REG));
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "MCHBAR_CH0_CR_PM_DIMM_RD_ENERGY_REG %Xh: %Xh \n", MCHBAR_CH0_CR_PM_DIMM_RD_ENERGY_REG, MrcReadCR (MrcData, MCHBAR_CH0_CR_PM_DIMM_RD_ENERGY_REG));
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "MCHBAR_CH1_CR_PM_DIMM_RD_ENERGY_REG %Xh: %Xh \n", MCHBAR_CH1_CR_PM_DIMM_RD_ENERGY_REG, MrcReadCR (MrcData, MCHBAR_CH1_CR_PM_DIMM_RD_ENERGY_REG));
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "MCHBAR_CH0_CR_PM_DIMM_WR_ENERGY_REG %Xh: %Xh \n", MCHBAR_CH0_CR_PM_DIMM_WR_ENERGY_REG, MrcReadCR (MrcData, MCHBAR_CH0_CR_PM_DIMM_WR_ENERGY_REG));
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "MCHBAR_CH1_CR_PM_DIMM_WR_ENERGY_REG %Xh: %Xh \n", MCHBAR_CH1_CR_PM_DIMM_WR_ENERGY_REG, MrcReadCR (MrcData, MCHBAR_CH1_CR_PM_DIMM_WR_ENERGY_REG));
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "MCHBAR_CH0_CR_PM_DIMM_ACT_ENERGY_REG %Xh: %Xh \n", MCHBAR_CH0_CR_PM_DIMM_ACT_ENERGY_REG, MrcReadCR (MrcData, MCHBAR_CH0_CR_PM_DIMM_ACT_ENERGY_REG));
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "MCHBAR_CH1_CR_PM_DIMM_ACT_ENERGY_REG %Xh: %Xh \n", MCHBAR_CH1_CR_PM_DIMM_ACT_ENERGY_REG, MrcReadCR (MrcData, MCHBAR_CH1_CR_PM_DIMM_ACT_ENERGY_REG));
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "MCHBAR_CH0_CR_PM_DIMM_IDLE_ENERGY_REG %Xh: %Xh \n", MCHBAR_CH0_CR_PM_DIMM_IDLE_ENERGY_REG, MrcReadCR (MrcData, MCHBAR_CH0_CR_PM_DIMM_IDLE_ENERGY_REG));
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "MCHBAR_CH1_CR_PM_DIMM_IDLE_ENERGY_REG %Xh: %Xh \n", MCHBAR_CH1_CR_PM_DIMM_IDLE_ENERGY_REG, MrcReadCR (MrcData, MCHBAR_CH1_CR_PM_DIMM_IDLE_ENERGY_REG));
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "MCHBAR_CH0_CR_PM_DIMM_PD_ENERGY_REG %Xh: %Xh \n", MCHBAR_CH0_CR_PM_DIMM_PD_ENERGY_REG, MrcReadCR (MrcData, MCHBAR_CH0_CR_PM_DIMM_PD_ENERGY_REG));
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "MCHBAR_CH1_CR_PM_DIMM_PD_ENERGY_REG %Xh: %Xh \n", MCHBAR_CH1_CR_PM_DIMM_PD_ENERGY_REG, MrcReadCR (MrcData, MCHBAR_CH1_CR_PM_DIMM_PD_ENERGY_REG));

    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Power budget registers ...\n");

    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "DDR_WARM_BUDGET_CH0 %Xh: %Xh \n", PCU_CR_DDR_WARM_BUDGET_CH0_PCU_REG, MrcReadCR (MrcData, PCU_CR_DDR_WARM_BUDGET_CH0_PCU_REG));
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "DDR_WARM_BUDGET_CH1 %Xh: %Xh \n", PCU_CR_DDR_WARM_BUDGET_CH1_PCU_REG, MrcReadCR (MrcData, PCU_CR_DDR_WARM_BUDGET_CH1_PCU_REG));
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "DDR_HOT_BUDGET_CH0 %Xh: %Xh \n", PCU_CR_DDR_HOT_BUDGET_CH0_PCU_REG, MrcReadCR (MrcData, PCU_CR_DDR_HOT_BUDGET_CH0_PCU_REG));
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "DDR_HOT_BUDGET_CH1 %Xh: %Xh \n", PCU_CR_DDR_HOT_BUDGET_CH1_PCU_REG, MrcReadCR (MrcData, PCU_CR_DDR_HOT_BUDGET_CH1_PCU_REG));


    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "Thermal Thresholds registers...\n");
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "DDR_WARM_THRESHOLD_CH0 %Xh: %Xh \n", PCU_CR_DDR_WARM_THRESHOLD_CH0_PCU_REG, MrcReadCR (MrcData, PCU_CR_DDR_WARM_THRESHOLD_CH0_PCU_REG));
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "DDR_WARM_THRESHOLD_CH1 %Xh: %Xh \n", PCU_CR_DDR_WARM_THRESHOLD_CH1_PCU_REG, MrcReadCR (MrcData, PCU_CR_DDR_WARM_THRESHOLD_CH1_PCU_REG));
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "DDR_HOT_THRESHOLD_CH0 %Xh: %Xh \n", PCU_CR_DDR_HOT_THRESHOLD_CH0_PCU_REG, MrcReadCR (MrcData, PCU_CR_DDR_HOT_THRESHOLD_CH0_PCU_REG));
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "DDR_HOT_THRESHOLD_CH1 %Xh: %Xh \n", PCU_CR_DDR_HOT_THRESHOLD_CH1_PCU_REG, MrcReadCR (MrcData, PCU_CR_DDR_HOT_THRESHOLD_CH1_PCU_REG));

    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "CLTM Configuration registers...\n");
    MRC_DEBUG_MSG (Debug, MSG_LEVEL_NOTE, "DDR_PTM_CTL %Xh: %Xh \n", PCU_CR_DDR_PTM_CTL_PCU_REG, MrcReadCR (MrcData, PCU_CR_DDR_PTM_CTL_PCU_REG));
}
#endif //MRC_DEBUG_PRINT
#endif
