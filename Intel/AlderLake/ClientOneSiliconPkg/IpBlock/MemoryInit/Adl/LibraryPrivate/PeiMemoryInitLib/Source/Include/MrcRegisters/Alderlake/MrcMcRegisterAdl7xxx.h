#ifndef __MrcMcRegisterAdl7xxx_h__
#define __MrcMcRegisterAdl7xxx_h__
/** @file
  This file was automatically generated. Modify at your own risk.
  Note that no error checking is done in these functions so ensure that the correct values are passed.

@copyright
  Copyright (c) 2010 - 2020 Intel Corporation. All rights reserved
  This software and associated documentation (if any) is furnished
  under a license and may only be used or copied in accordance
  with the terms of the license. Except as permitted by the
  license, no part of this software or documentation may be
  reproduced, stored in a retrieval system, or transmitted in any
  form or by any means without the express written consent of
  Intel Corporation.
  This file contains an 'Intel Peripheral Driver' and is uniquely
  identified as "Intel Reference Module" and is licensed for Intel
  CPUs and chipsets under the terms of your license agreement with
  Intel or your vendor. This file may be modified by the user, subject
  to additional terms of the license agreement.

@par Specification Reference:
**/

#pragma pack(push, 1)


#define MEMSS_PM_EN_MCHBAR_IMPH_REG                                    (0x00007004)

  #define MEMSS_PM_EN_MCHBAR_IMPH_DIP_ENABLE_OFF                       ( 0)
  #define MEMSS_PM_EN_MCHBAR_IMPH_DIP_ENABLE_WID                       ( 1)
  #define MEMSS_PM_EN_MCHBAR_IMPH_DIP_ENABLE_MSK                       (0x00000001)
  #define MEMSS_PM_EN_MCHBAR_IMPH_DIP_ENABLE_MIN                       (0)
  #define MEMSS_PM_EN_MCHBAR_IMPH_DIP_ENABLE_MAX                       (1) // 0x00000001
  #define MEMSS_PM_EN_MCHBAR_IMPH_DIP_ENABLE_DEF                       (0x00000000)
  #define MEMSS_PM_EN_MCHBAR_IMPH_DIP_ENABLE_HSH                       (0x01007004)

  #define MEMSS_PM_EN_MCHBAR_IMPH_MEE0_ENABLE_OFF                      ( 1)
  #define MEMSS_PM_EN_MCHBAR_IMPH_MEE0_ENABLE_WID                      ( 1)
  #define MEMSS_PM_EN_MCHBAR_IMPH_MEE0_ENABLE_MSK                      (0x00000002)
  #define MEMSS_PM_EN_MCHBAR_IMPH_MEE0_ENABLE_MIN                      (0)
  #define MEMSS_PM_EN_MCHBAR_IMPH_MEE0_ENABLE_MAX                      (1) // 0x00000001
  #define MEMSS_PM_EN_MCHBAR_IMPH_MEE0_ENABLE_DEF                      (0x00000000)
  #define MEMSS_PM_EN_MCHBAR_IMPH_MEE0_ENABLE_HSH                      (0x01027004)

  #define MEMSS_PM_EN_MCHBAR_IMPH_MEE1_ENABLE_OFF                      ( 2)
  #define MEMSS_PM_EN_MCHBAR_IMPH_MEE1_ENABLE_WID                      ( 1)
  #define MEMSS_PM_EN_MCHBAR_IMPH_MEE1_ENABLE_MSK                      (0x00000004)
  #define MEMSS_PM_EN_MCHBAR_IMPH_MEE1_ENABLE_MIN                      (0)
  #define MEMSS_PM_EN_MCHBAR_IMPH_MEE1_ENABLE_MAX                      (1) // 0x00000001
  #define MEMSS_PM_EN_MCHBAR_IMPH_MEE1_ENABLE_DEF                      (0x00000000)
  #define MEMSS_PM_EN_MCHBAR_IMPH_MEE1_ENABLE_HSH                      (0x01047004)

  #define MEMSS_PM_EN_MCHBAR_IMPH_TMECC0_ENABLE_OFF                    ( 3)
  #define MEMSS_PM_EN_MCHBAR_IMPH_TMECC0_ENABLE_WID                    ( 1)
  #define MEMSS_PM_EN_MCHBAR_IMPH_TMECC0_ENABLE_MSK                    (0x00000008)
  #define MEMSS_PM_EN_MCHBAR_IMPH_TMECC0_ENABLE_MIN                    (0)
  #define MEMSS_PM_EN_MCHBAR_IMPH_TMECC0_ENABLE_MAX                    (1) // 0x00000001
  #define MEMSS_PM_EN_MCHBAR_IMPH_TMECC0_ENABLE_DEF                    (0x00000000)
  #define MEMSS_PM_EN_MCHBAR_IMPH_TMECC0_ENABLE_HSH                    (0x01067004)

  #define MEMSS_PM_EN_MCHBAR_IMPH_TMECC1_ENABLE_OFF                    ( 4)
  #define MEMSS_PM_EN_MCHBAR_IMPH_TMECC1_ENABLE_WID                    ( 1)
  #define MEMSS_PM_EN_MCHBAR_IMPH_TMECC1_ENABLE_MSK                    (0x00000010)
  #define MEMSS_PM_EN_MCHBAR_IMPH_TMECC1_ENABLE_MIN                    (0)
  #define MEMSS_PM_EN_MCHBAR_IMPH_TMECC1_ENABLE_MAX                    (1) // 0x00000001
  #define MEMSS_PM_EN_MCHBAR_IMPH_TMECC1_ENABLE_DEF                    (0x00000000)
  #define MEMSS_PM_EN_MCHBAR_IMPH_TMECC1_ENABLE_HSH                    (0x01087004)

  #define MEMSS_PM_EN_MCHBAR_IMPH_ASTRO_ENABLE_OFF                     ( 5)
  #define MEMSS_PM_EN_MCHBAR_IMPH_ASTRO_ENABLE_WID                     ( 1)
  #define MEMSS_PM_EN_MCHBAR_IMPH_ASTRO_ENABLE_MSK                     (0x00000020)
  #define MEMSS_PM_EN_MCHBAR_IMPH_ASTRO_ENABLE_MIN                     (0)
  #define MEMSS_PM_EN_MCHBAR_IMPH_ASTRO_ENABLE_MAX                     (1) // 0x00000001
  #define MEMSS_PM_EN_MCHBAR_IMPH_ASTRO_ENABLE_DEF                     (0x00000000)
  #define MEMSS_PM_EN_MCHBAR_IMPH_ASTRO_ENABLE_HSH                     (0x010A7004)

  #define MEMSS_PM_EN_MCHBAR_IMPH_EFLOW0_ENABLE_OFF                    ( 6)
  #define MEMSS_PM_EN_MCHBAR_IMPH_EFLOW0_ENABLE_WID                    ( 1)
  #define MEMSS_PM_EN_MCHBAR_IMPH_EFLOW0_ENABLE_MSK                    (0x00000040)
  #define MEMSS_PM_EN_MCHBAR_IMPH_EFLOW0_ENABLE_MIN                    (0)
  #define MEMSS_PM_EN_MCHBAR_IMPH_EFLOW0_ENABLE_MAX                    (1) // 0x00000001
  #define MEMSS_PM_EN_MCHBAR_IMPH_EFLOW0_ENABLE_DEF                    (0x00000000)
  #define MEMSS_PM_EN_MCHBAR_IMPH_EFLOW0_ENABLE_HSH                    (0x010C7004)

  #define MEMSS_PM_EN_MCHBAR_IMPH_EFLOW1_ENABLE_OFF                    ( 7)
  #define MEMSS_PM_EN_MCHBAR_IMPH_EFLOW1_ENABLE_WID                    ( 1)
  #define MEMSS_PM_EN_MCHBAR_IMPH_EFLOW1_ENABLE_MSK                    (0x00000080)
  #define MEMSS_PM_EN_MCHBAR_IMPH_EFLOW1_ENABLE_MIN                    (0)
  #define MEMSS_PM_EN_MCHBAR_IMPH_EFLOW1_ENABLE_MAX                    (1) // 0x00000001
  #define MEMSS_PM_EN_MCHBAR_IMPH_EFLOW1_ENABLE_DEF                    (0x00000000)
  #define MEMSS_PM_EN_MCHBAR_IMPH_EFLOW1_ENABLE_HSH                    (0x010E7004)

  #define MEMSS_PM_EN_MCHBAR_IMPH_MC0_ENABLE_OFF                       ( 8)
  #define MEMSS_PM_EN_MCHBAR_IMPH_MC0_ENABLE_WID                       ( 1)
  #define MEMSS_PM_EN_MCHBAR_IMPH_MC0_ENABLE_MSK                       (0x00000100)
  #define MEMSS_PM_EN_MCHBAR_IMPH_MC0_ENABLE_MIN                       (0)
  #define MEMSS_PM_EN_MCHBAR_IMPH_MC0_ENABLE_MAX                       (1) // 0x00000001
  #define MEMSS_PM_EN_MCHBAR_IMPH_MC0_ENABLE_DEF                       (0x00000000)
  #define MEMSS_PM_EN_MCHBAR_IMPH_MC0_ENABLE_HSH                       (0x01107004)

  #define MEMSS_PM_EN_MCHBAR_IMPH_MC1_ENABLE_OFF                       ( 9)
  #define MEMSS_PM_EN_MCHBAR_IMPH_MC1_ENABLE_WID                       ( 1)
  #define MEMSS_PM_EN_MCHBAR_IMPH_MC1_ENABLE_MSK                       (0x00000200)
  #define MEMSS_PM_EN_MCHBAR_IMPH_MC1_ENABLE_MIN                       (0)
  #define MEMSS_PM_EN_MCHBAR_IMPH_MC1_ENABLE_MAX                       (1) // 0x00000001
  #define MEMSS_PM_EN_MCHBAR_IMPH_MC1_ENABLE_DEF                       (0x00000000)
  #define MEMSS_PM_EN_MCHBAR_IMPH_MC1_ENABLE_HSH                       (0x01127004)

  #define MEMSS_PM_EN_MCHBAR_IMPH_RSVD0_OFF                            (10)
  #define MEMSS_PM_EN_MCHBAR_IMPH_RSVD0_WID                            (22)
  #define MEMSS_PM_EN_MCHBAR_IMPH_RSVD0_MSK                            (0xFFFFFC00)
  #define MEMSS_PM_EN_MCHBAR_IMPH_RSVD0_MIN                            (0)
  #define MEMSS_PM_EN_MCHBAR_IMPH_RSVD0_MAX                            (4194303) // 0x003FFFFF
  #define MEMSS_PM_EN_MCHBAR_IMPH_RSVD0_DEF                            (0x00000000)
  #define MEMSS_PM_EN_MCHBAR_IMPH_RSVD0_HSH                            (0x16147004)

#define HDAUDRID_IMPH_REG                                              (0x00007008)

  #define HDAUDRID_IMPH_DEVNUM_OFF                                     ( 3)
  #define HDAUDRID_IMPH_DEVNUM_WID                                     ( 5)
  #define HDAUDRID_IMPH_DEVNUM_MSK                                     (0x000000F8)
  #define HDAUDRID_IMPH_DEVNUM_MIN                                     (0)
  #define HDAUDRID_IMPH_DEVNUM_MAX                                     (31) // 0x0000001F
  #define HDAUDRID_IMPH_DEVNUM_DEF                                     (0x0000001B)
  #define HDAUDRID_IMPH_DEVNUM_HSH                                     (0x05067008)

  #define HDAUDRID_IMPH_BUSNUM_OFF                                     ( 8)
  #define HDAUDRID_IMPH_BUSNUM_WID                                     ( 8)
  #define HDAUDRID_IMPH_BUSNUM_MSK                                     (0x0000FF00)
  #define HDAUDRID_IMPH_BUSNUM_MIN                                     (0)
  #define HDAUDRID_IMPH_BUSNUM_MAX                                     (255) // 0x000000FF
  #define HDAUDRID_IMPH_BUSNUM_DEF                                     (0x00000000)
  #define HDAUDRID_IMPH_BUSNUM_HSH                                     (0x08107008)

  #define HDAUDRID_IMPH_HDAUD_EN_OFF                                   (31)
  #define HDAUDRID_IMPH_HDAUD_EN_WID                                   ( 1)
  #define HDAUDRID_IMPH_HDAUD_EN_MSK                                   (0x80000000)
  #define HDAUDRID_IMPH_HDAUD_EN_MIN                                   (0)
  #define HDAUDRID_IMPH_HDAUD_EN_MAX                                   (1) // 0x00000001
  #define HDAUDRID_IMPH_HDAUD_EN_DEF                                   (0x00000001)
  #define HDAUDRID_IMPH_HDAUD_EN_HSH                                   (0x013E7008)

#define MEMSS_PM_ORDER_MCHBAR_IMPH_REG                                 (0x00007010)

  #define MEMSS_PM_ORDER_MCHBAR_IMPH_AGENT_0_OFF                       ( 0)
  #define MEMSS_PM_ORDER_MCHBAR_IMPH_AGENT_0_WID                       ( 4)
  #define MEMSS_PM_ORDER_MCHBAR_IMPH_AGENT_0_MSK                       (0x0000000F)
  #define MEMSS_PM_ORDER_MCHBAR_IMPH_AGENT_0_MIN                       (0)
  #define MEMSS_PM_ORDER_MCHBAR_IMPH_AGENT_0_MAX                       (15) // 0x0000000F
  #define MEMSS_PM_ORDER_MCHBAR_IMPH_AGENT_0_DEF                       (0x00000000)
  #define MEMSS_PM_ORDER_MCHBAR_IMPH_AGENT_0_HSH                       (0x44007010)

  #define MEMSS_PM_ORDER_MCHBAR_IMPH_AGENT_1_OFF                       ( 4)
  #define MEMSS_PM_ORDER_MCHBAR_IMPH_AGENT_1_WID                       ( 4)
  #define MEMSS_PM_ORDER_MCHBAR_IMPH_AGENT_1_MSK                       (0x000000F0)
  #define MEMSS_PM_ORDER_MCHBAR_IMPH_AGENT_1_MIN                       (0)
  #define MEMSS_PM_ORDER_MCHBAR_IMPH_AGENT_1_MAX                       (15) // 0x0000000F
  #define MEMSS_PM_ORDER_MCHBAR_IMPH_AGENT_1_DEF                       (0x00000000)
  #define MEMSS_PM_ORDER_MCHBAR_IMPH_AGENT_1_HSH                       (0x44087010)

  #define MEMSS_PM_ORDER_MCHBAR_IMPH_AGENT_2_OFF                       ( 8)
  #define MEMSS_PM_ORDER_MCHBAR_IMPH_AGENT_2_WID                       ( 4)
  #define MEMSS_PM_ORDER_MCHBAR_IMPH_AGENT_2_MSK                       (0x00000F00)
  #define MEMSS_PM_ORDER_MCHBAR_IMPH_AGENT_2_MIN                       (0)
  #define MEMSS_PM_ORDER_MCHBAR_IMPH_AGENT_2_MAX                       (15) // 0x0000000F
  #define MEMSS_PM_ORDER_MCHBAR_IMPH_AGENT_2_DEF                       (0x00000000)
  #define MEMSS_PM_ORDER_MCHBAR_IMPH_AGENT_2_HSH                       (0x44107010)

  #define MEMSS_PM_ORDER_MCHBAR_IMPH_AGENT_3_OFF                       (12)
  #define MEMSS_PM_ORDER_MCHBAR_IMPH_AGENT_3_WID                       ( 4)
  #define MEMSS_PM_ORDER_MCHBAR_IMPH_AGENT_3_MSK                       (0x0000F000)
  #define MEMSS_PM_ORDER_MCHBAR_IMPH_AGENT_3_MIN                       (0)
  #define MEMSS_PM_ORDER_MCHBAR_IMPH_AGENT_3_MAX                       (15) // 0x0000000F
  #define MEMSS_PM_ORDER_MCHBAR_IMPH_AGENT_3_DEF                       (0x00000000)
  #define MEMSS_PM_ORDER_MCHBAR_IMPH_AGENT_3_HSH                       (0x44187010)

  #define MEMSS_PM_ORDER_MCHBAR_IMPH_AGENT_4_OFF                       (16)
  #define MEMSS_PM_ORDER_MCHBAR_IMPH_AGENT_4_WID                       ( 4)
  #define MEMSS_PM_ORDER_MCHBAR_IMPH_AGENT_4_MSK                       (0x000F0000)
  #define MEMSS_PM_ORDER_MCHBAR_IMPH_AGENT_4_MIN                       (0)
  #define MEMSS_PM_ORDER_MCHBAR_IMPH_AGENT_4_MAX                       (15) // 0x0000000F
  #define MEMSS_PM_ORDER_MCHBAR_IMPH_AGENT_4_DEF                       (0x00000000)
  #define MEMSS_PM_ORDER_MCHBAR_IMPH_AGENT_4_HSH                       (0x44207010)

  #define MEMSS_PM_ORDER_MCHBAR_IMPH_AGENT_5_OFF                       (20)
  #define MEMSS_PM_ORDER_MCHBAR_IMPH_AGENT_5_WID                       ( 4)
  #define MEMSS_PM_ORDER_MCHBAR_IMPH_AGENT_5_MSK                       (0x00F00000)
  #define MEMSS_PM_ORDER_MCHBAR_IMPH_AGENT_5_MIN                       (0)
  #define MEMSS_PM_ORDER_MCHBAR_IMPH_AGENT_5_MAX                       (15) // 0x0000000F
  #define MEMSS_PM_ORDER_MCHBAR_IMPH_AGENT_5_DEF                       (0x00000000)
  #define MEMSS_PM_ORDER_MCHBAR_IMPH_AGENT_5_HSH                       (0x44287010)

  #define MEMSS_PM_ORDER_MCHBAR_IMPH_AGENT_6_OFF                       (24)
  #define MEMSS_PM_ORDER_MCHBAR_IMPH_AGENT_6_WID                       ( 4)
  #define MEMSS_PM_ORDER_MCHBAR_IMPH_AGENT_6_MSK                       (0x0F000000)
  #define MEMSS_PM_ORDER_MCHBAR_IMPH_AGENT_6_MIN                       (0)
  #define MEMSS_PM_ORDER_MCHBAR_IMPH_AGENT_6_MAX                       (15) // 0x0000000F
  #define MEMSS_PM_ORDER_MCHBAR_IMPH_AGENT_6_DEF                       (0x00000000)
  #define MEMSS_PM_ORDER_MCHBAR_IMPH_AGENT_6_HSH                       (0x44307010)

  #define MEMSS_PM_ORDER_MCHBAR_IMPH_AGENT_7_OFF                       (28)
  #define MEMSS_PM_ORDER_MCHBAR_IMPH_AGENT_7_WID                       ( 4)
  #define MEMSS_PM_ORDER_MCHBAR_IMPH_AGENT_7_MSK                       (0xF0000000)
  #define MEMSS_PM_ORDER_MCHBAR_IMPH_AGENT_7_MIN                       (0)
  #define MEMSS_PM_ORDER_MCHBAR_IMPH_AGENT_7_MAX                       (15) // 0x0000000F
  #define MEMSS_PM_ORDER_MCHBAR_IMPH_AGENT_7_DEF                       (0x00000000)
  #define MEMSS_PM_ORDER_MCHBAR_IMPH_AGENT_7_HSH                       (0x44387010)

  #define MEMSS_PM_ORDER_MCHBAR_IMPH_AGENT_8_OFF                       (32)
  #define MEMSS_PM_ORDER_MCHBAR_IMPH_AGENT_8_WID                       ( 4)
  #define MEMSS_PM_ORDER_MCHBAR_IMPH_AGENT_8_MSK                       (0x0000000F00000000ULL)
  #define MEMSS_PM_ORDER_MCHBAR_IMPH_AGENT_8_MIN                       (0)
  #define MEMSS_PM_ORDER_MCHBAR_IMPH_AGENT_8_MAX                       (15) // 0x0000000F
  #define MEMSS_PM_ORDER_MCHBAR_IMPH_AGENT_8_DEF                       (0x00000000)
  #define MEMSS_PM_ORDER_MCHBAR_IMPH_AGENT_8_HSH                       (0x44407010)

  #define MEMSS_PM_ORDER_MCHBAR_IMPH_AGENT_9_OFF                       (36)
  #define MEMSS_PM_ORDER_MCHBAR_IMPH_AGENT_9_WID                       ( 4)
  #define MEMSS_PM_ORDER_MCHBAR_IMPH_AGENT_9_MSK                       (0x000000F000000000ULL)
  #define MEMSS_PM_ORDER_MCHBAR_IMPH_AGENT_9_MIN                       (0)
  #define MEMSS_PM_ORDER_MCHBAR_IMPH_AGENT_9_MAX                       (15) // 0x0000000F
  #define MEMSS_PM_ORDER_MCHBAR_IMPH_AGENT_9_DEF                       (0x00000000)
  #define MEMSS_PM_ORDER_MCHBAR_IMPH_AGENT_9_HSH                       (0x44487010)

  #define MEMSS_PM_ORDER_MCHBAR_IMPH_RSVD0_OFF                         (40)
  #define MEMSS_PM_ORDER_MCHBAR_IMPH_RSVD0_WID                         ( 4)
  #define MEMSS_PM_ORDER_MCHBAR_IMPH_RSVD0_MSK                         (0x00000F0000000000ULL)
  #define MEMSS_PM_ORDER_MCHBAR_IMPH_RSVD0_MIN                         (0)
  #define MEMSS_PM_ORDER_MCHBAR_IMPH_RSVD0_MAX                         (15) // 0x0000000F
  #define MEMSS_PM_ORDER_MCHBAR_IMPH_RSVD0_DEF                         (0x00000000)
  #define MEMSS_PM_ORDER_MCHBAR_IMPH_RSVD0_HSH                         (0x44507010)

  #define MEMSS_PM_ORDER_MCHBAR_IMPH_RSVD1_OFF                         (44)
  #define MEMSS_PM_ORDER_MCHBAR_IMPH_RSVD1_WID                         ( 4)
  #define MEMSS_PM_ORDER_MCHBAR_IMPH_RSVD1_MSK                         (0x0000F00000000000ULL)
  #define MEMSS_PM_ORDER_MCHBAR_IMPH_RSVD1_MIN                         (0)
  #define MEMSS_PM_ORDER_MCHBAR_IMPH_RSVD1_MAX                         (15) // 0x0000000F
  #define MEMSS_PM_ORDER_MCHBAR_IMPH_RSVD1_DEF                         (0x00000000)
  #define MEMSS_PM_ORDER_MCHBAR_IMPH_RSVD1_HSH                         (0x44587010)

  #define MEMSS_PM_ORDER_MCHBAR_IMPH_RSVD2_OFF                         (48)
  #define MEMSS_PM_ORDER_MCHBAR_IMPH_RSVD2_WID                         (16)
  #define MEMSS_PM_ORDER_MCHBAR_IMPH_RSVD2_MSK                         (0xFFFF000000000000ULL)
  #define MEMSS_PM_ORDER_MCHBAR_IMPH_RSVD2_MIN                         (0)
  #define MEMSS_PM_ORDER_MCHBAR_IMPH_RSVD2_MAX                         (65535) // 0x0000FFFF
  #define MEMSS_PM_ORDER_MCHBAR_IMPH_RSVD2_DEF                         (0x00000000)
  #define MEMSS_PM_ORDER_MCHBAR_IMPH_RSVD2_HSH                         (0x50607010)

#define VLWCTL_REG                                                     (0x00007020)

  #define VLWCTL_DROP_VLW_CTL_OFF                                      ( 0)
  #define VLWCTL_DROP_VLW_CTL_WID                                      ( 1)
  #define VLWCTL_DROP_VLW_CTL_MSK                                      (0x00000001)
  #define VLWCTL_DROP_VLW_CTL_MIN                                      (0)
  #define VLWCTL_DROP_VLW_CTL_MAX                                      (1) // 0x00000001
  #define VLWCTL_DROP_VLW_CTL_DEF                                      (0x00000001)
  #define VLWCTL_DROP_VLW_CTL_HSH                                      (0x01007020)

  #define VLWCTL_DROP_VLW_STS_OFF                                      (31)
  #define VLWCTL_DROP_VLW_STS_WID                                      ( 1)
  #define VLWCTL_DROP_VLW_STS_MSK                                      (0x80000000)
  #define VLWCTL_DROP_VLW_STS_MIN                                      (0)
  #define VLWCTL_DROP_VLW_STS_MAX                                      (1) // 0x00000001
  #define VLWCTL_DROP_VLW_STS_DEF                                      (0x00000000)
  #define VLWCTL_DROP_VLW_STS_HSH                                      (0x013E7020)

#define VDMBDFBARKVM_IMPH_REG                                          (0x00007030)

  #define VDMBDFBARKVM_IMPH_FUNNUM_OFF                                 ( 0)
  #define VDMBDFBARKVM_IMPH_FUNNUM_WID                                 ( 3)
  #define VDMBDFBARKVM_IMPH_FUNNUM_MSK                                 (0x00000007)
  #define VDMBDFBARKVM_IMPH_FUNNUM_MIN                                 (0)
  #define VDMBDFBARKVM_IMPH_FUNNUM_MAX                                 (7) // 0x00000007
  #define VDMBDFBARKVM_IMPH_FUNNUM_DEF                                 (0x00000000)
  #define VDMBDFBARKVM_IMPH_FUNNUM_HSH                                 (0x03007030)

  #define VDMBDFBARKVM_IMPH_DEVNUM_OFF                                 ( 3)
  #define VDMBDFBARKVM_IMPH_DEVNUM_WID                                 ( 5)
  #define VDMBDFBARKVM_IMPH_DEVNUM_MSK                                 (0x000000F8)
  #define VDMBDFBARKVM_IMPH_DEVNUM_MIN                                 (0)
  #define VDMBDFBARKVM_IMPH_DEVNUM_MAX                                 (31) // 0x0000001F
  #define VDMBDFBARKVM_IMPH_DEVNUM_DEF                                 (0x00000016)
  #define VDMBDFBARKVM_IMPH_DEVNUM_HSH                                 (0x05067030)

  #define VDMBDFBARKVM_IMPH_BUSNUM_OFF                                 ( 8)
  #define VDMBDFBARKVM_IMPH_BUSNUM_WID                                 ( 8)
  #define VDMBDFBARKVM_IMPH_BUSNUM_MSK                                 (0x0000FF00)
  #define VDMBDFBARKVM_IMPH_BUSNUM_MIN                                 (0)
  #define VDMBDFBARKVM_IMPH_BUSNUM_MAX                                 (255) // 0x000000FF
  #define VDMBDFBARKVM_IMPH_BUSNUM_DEF                                 (0x00000000)
  #define VDMBDFBARKVM_IMPH_BUSNUM_HSH                                 (0x08107030)

  #define VDMBDFBARKVM_IMPH_BARNUM_OFF                                 (16)
  #define VDMBDFBARKVM_IMPH_BARNUM_WID                                 ( 3)
  #define VDMBDFBARKVM_IMPH_BARNUM_MSK                                 (0x00070000)
  #define VDMBDFBARKVM_IMPH_BARNUM_MIN                                 (0)
  #define VDMBDFBARKVM_IMPH_BARNUM_MAX                                 (7) // 0x00000007
  #define VDMBDFBARKVM_IMPH_BARNUM_DEF                                 (0x00000007)
  #define VDMBDFBARKVM_IMPH_BARNUM_HSH                                 (0x03207030)

#define VDMBDFBARPAVP_IMPH_REG                                         (0x00007034)
//Duplicate of VDMBDFBARKVM_IMPH_REG

#define PRIMUP_MASK1_IMPH_REG                                          (0x00007050)

  #define PRIMUP_MASK1_IMPH_VC_OFF                                     ( 0)
  #define PRIMUP_MASK1_IMPH_VC_WID                                     ( 4)
  #define PRIMUP_MASK1_IMPH_VC_MSK                                     (0x0000000F)
  #define PRIMUP_MASK1_IMPH_VC_MIN                                     (0)
  #define PRIMUP_MASK1_IMPH_VC_MAX                                     (15) // 0x0000000F
  #define PRIMUP_MASK1_IMPH_VC_DEF                                     (0x00000000)
  #define PRIMUP_MASK1_IMPH_VC_HSH                                     (0x04007050)

  #define PRIMUP_MASK1_IMPH_FMT_CMDTYPE_OFF                            ( 4)
  #define PRIMUP_MASK1_IMPH_FMT_CMDTYPE_WID                            ( 6)
  #define PRIMUP_MASK1_IMPH_FMT_CMDTYPE_MSK                            (0x000003F0)
  #define PRIMUP_MASK1_IMPH_FMT_CMDTYPE_MIN                            (0)
  #define PRIMUP_MASK1_IMPH_FMT_CMDTYPE_MAX                            (63) // 0x0000003F
  #define PRIMUP_MASK1_IMPH_FMT_CMDTYPE_DEF                            (0x00000000)
  #define PRIMUP_MASK1_IMPH_FMT_CMDTYPE_HSH                            (0x06087050)

  #define PRIMUP_MASK1_IMPH_TC_OFF                                     (10)
  #define PRIMUP_MASK1_IMPH_TC_WID                                     ( 4)
  #define PRIMUP_MASK1_IMPH_TC_MSK                                     (0x00003C00)
  #define PRIMUP_MASK1_IMPH_TC_MIN                                     (0)
  #define PRIMUP_MASK1_IMPH_TC_MAX                                     (15) // 0x0000000F
  #define PRIMUP_MASK1_IMPH_TC_DEF                                     (0x00000000)
  #define PRIMUP_MASK1_IMPH_TC_HSH                                     (0x04147050)

  #define PRIMUP_MASK1_IMPH_CHAIN_OFF                                  (14)
  #define PRIMUP_MASK1_IMPH_CHAIN_WID                                  ( 1)
  #define PRIMUP_MASK1_IMPH_CHAIN_MSK                                  (0x00004000)
  #define PRIMUP_MASK1_IMPH_CHAIN_MIN                                  (0)
  #define PRIMUP_MASK1_IMPH_CHAIN_MAX                                  (1) // 0x00000001
  #define PRIMUP_MASK1_IMPH_CHAIN_DEF                                  (0x00000000)
  #define PRIMUP_MASK1_IMPH_CHAIN_HSH                                  (0x011C7050)

  #define PRIMUP_MASK1_IMPH_NS_OFF                                     (15)
  #define PRIMUP_MASK1_IMPH_NS_WID                                     ( 1)
  #define PRIMUP_MASK1_IMPH_NS_MSK                                     (0x00008000)
  #define PRIMUP_MASK1_IMPH_NS_MIN                                     (0)
  #define PRIMUP_MASK1_IMPH_NS_MAX                                     (1) // 0x00000001
  #define PRIMUP_MASK1_IMPH_NS_DEF                                     (0x00000000)
  #define PRIMUP_MASK1_IMPH_NS_HSH                                     (0x011E7050)

  #define PRIMUP_MASK1_IMPH_RO_OFF                                     (16)
  #define PRIMUP_MASK1_IMPH_RO_WID                                     ( 1)
  #define PRIMUP_MASK1_IMPH_RO_MSK                                     (0x00010000)
  #define PRIMUP_MASK1_IMPH_RO_MIN                                     (0)
  #define PRIMUP_MASK1_IMPH_RO_MAX                                     (1) // 0x00000001
  #define PRIMUP_MASK1_IMPH_RO_DEF                                     (0x00000000)
  #define PRIMUP_MASK1_IMPH_RO_HSH                                     (0x01207050)

  #define PRIMUP_MASK1_IMPH_LENGTH_OFF                                 (17)
  #define PRIMUP_MASK1_IMPH_LENGTH_WID                                 ( 5)
  #define PRIMUP_MASK1_IMPH_LENGTH_MSK                                 (0x003E0000)
  #define PRIMUP_MASK1_IMPH_LENGTH_MIN                                 (0)
  #define PRIMUP_MASK1_IMPH_LENGTH_MAX                                 (31) // 0x0000001F
  #define PRIMUP_MASK1_IMPH_LENGTH_DEF                                 (0x00000000)
  #define PRIMUP_MASK1_IMPH_LENGTH_HSH                                 (0x05227050)

  #define PRIMUP_MASK1_IMPH_EP_OFF                                     (22)
  #define PRIMUP_MASK1_IMPH_EP_WID                                     ( 1)
  #define PRIMUP_MASK1_IMPH_EP_MSK                                     (0x00400000)
  #define PRIMUP_MASK1_IMPH_EP_MIN                                     (0)
  #define PRIMUP_MASK1_IMPH_EP_MAX                                     (1) // 0x00000001
  #define PRIMUP_MASK1_IMPH_EP_DEF                                     (0x00000000)
  #define PRIMUP_MASK1_IMPH_EP_HSH                                     (0x012C7050)

  #define PRIMUP_MASK1_IMPH_AT_OFF                                     (23)
  #define PRIMUP_MASK1_IMPH_AT_WID                                     ( 2)
  #define PRIMUP_MASK1_IMPH_AT_MSK                                     (0x01800000)
  #define PRIMUP_MASK1_IMPH_AT_MIN                                     (0)
  #define PRIMUP_MASK1_IMPH_AT_MAX                                     (3) // 0x00000003
  #define PRIMUP_MASK1_IMPH_AT_DEF                                     (0x00000000)
  #define PRIMUP_MASK1_IMPH_AT_HSH                                     (0x022E7050)

#define PRIMUP_MASK2_IMPH_REG                                          (0x00007054)

  #define PRIMUP_MASK2_IMPH_RQID_OFF                                   ( 0)
  #define PRIMUP_MASK2_IMPH_RQID_WID                                   (16)
  #define PRIMUP_MASK2_IMPH_RQID_MSK                                   (0x0000FFFF)
  #define PRIMUP_MASK2_IMPH_RQID_MIN                                   (0)
  #define PRIMUP_MASK2_IMPH_RQID_MAX                                   (65535) // 0x0000FFFF
  #define PRIMUP_MASK2_IMPH_RQID_DEF                                   (0x00000000)
  #define PRIMUP_MASK2_IMPH_RQID_HSH                                   (0x10007054)

  #define PRIMUP_MASK2_IMPH_TAG_OFF                                    (16)
  #define PRIMUP_MASK2_IMPH_TAG_WID                                    ( 8)
  #define PRIMUP_MASK2_IMPH_TAG_MSK                                    (0x00FF0000)
  #define PRIMUP_MASK2_IMPH_TAG_MIN                                    (0)
  #define PRIMUP_MASK2_IMPH_TAG_MAX                                    (255) // 0x000000FF
  #define PRIMUP_MASK2_IMPH_TAG_DEF                                    (0x00000000)
  #define PRIMUP_MASK2_IMPH_TAG_HSH                                    (0x08207054)

  #define PRIMUP_MASK2_IMPH_LBEFBE_MSGTYPE_OFF                         (24)
  #define PRIMUP_MASK2_IMPH_LBEFBE_MSGTYPE_WID                         ( 8)
  #define PRIMUP_MASK2_IMPH_LBEFBE_MSGTYPE_MSK                         (0xFF000000)
  #define PRIMUP_MASK2_IMPH_LBEFBE_MSGTYPE_MIN                         (0)
  #define PRIMUP_MASK2_IMPH_LBEFBE_MSGTYPE_MAX                         (255) // 0x000000FF
  #define PRIMUP_MASK2_IMPH_LBEFBE_MSGTYPE_DEF                         (0x00000000)
  #define PRIMUP_MASK2_IMPH_LBEFBE_MSGTYPE_HSH                         (0x08307054)

#define PRIMUP_MASK3_IMPH_REG                                          (0x00007058)

  #define PRIMUP_MASK3_IMPH_ADDR_31_0_OFF                              ( 0)
  #define PRIMUP_MASK3_IMPH_ADDR_31_0_WID                              (32)
  #define PRIMUP_MASK3_IMPH_ADDR_31_0_MSK                              (0xFFFFFFFF)
  #define PRIMUP_MASK3_IMPH_ADDR_31_0_MIN                              (0)
  #define PRIMUP_MASK3_IMPH_ADDR_31_0_MAX                              (4294967295) // 0xFFFFFFFF
  #define PRIMUP_MASK3_IMPH_ADDR_31_0_DEF                              (0x00000000)
  #define PRIMUP_MASK3_IMPH_ADDR_31_0_HSH                              (0x20007058)

#define PRIMUP_MASK4_IMPH_REG                                          (0x0000705C)

  #define PRIMUP_MASK4_IMPH_ADDR_63_32_OFF                             ( 0)
  #define PRIMUP_MASK4_IMPH_ADDR_63_32_WID                             (32)
  #define PRIMUP_MASK4_IMPH_ADDR_63_32_MSK                             (0xFFFFFFFF)
  #define PRIMUP_MASK4_IMPH_ADDR_63_32_MIN                             (0)
  #define PRIMUP_MASK4_IMPH_ADDR_63_32_MAX                             (4294967295) // 0xFFFFFFFF
  #define PRIMUP_MASK4_IMPH_ADDR_63_32_DEF                             (0x00000000)
  #define PRIMUP_MASK4_IMPH_ADDR_63_32_HSH                             (0x2000705C)

#define PRIMUP_COMP1_IMPH_REG                                          (0x00007060)
//Duplicate of PRIMUP_MASK1_IMPH_REG

#define PRIMUP_COMP2_IMPH_REG                                          (0x00007064)
//Duplicate of PRIMUP_MASK2_IMPH_REG

#define PRIMUP_COMP3_IMPH_REG                                          (0x00007068)
//Duplicate of PRIMUP_MASK3_IMPH_REG

#define PRIMUP_COMP4_IMPH_REG                                          (0x0000706C)
//Duplicate of PRIMUP_MASK4_IMPH_REG

#define PRIMUP_TRIGGER_IMPH_REG                                        (0x00007070)

  #define PRIMUP_TRIGGER_IMPH_ENABLE_OFF                               ( 0)
  #define PRIMUP_TRIGGER_IMPH_ENABLE_WID                               ( 1)
  #define PRIMUP_TRIGGER_IMPH_ENABLE_MSK                               (0x00000001)
  #define PRIMUP_TRIGGER_IMPH_ENABLE_MIN                               (0)
  #define PRIMUP_TRIGGER_IMPH_ENABLE_MAX                               (1) // 0x00000001
  #define PRIMUP_TRIGGER_IMPH_ENABLE_DEF                               (0x00000000)
  #define PRIMUP_TRIGGER_IMPH_ENABLE_HSH                               (0x01007070)

  #define PRIMUP_TRIGGER_IMPH_TRIGGERED_OFF                            ( 1)
  #define PRIMUP_TRIGGER_IMPH_TRIGGERED_WID                            ( 1)
  #define PRIMUP_TRIGGER_IMPH_TRIGGERED_MSK                            (0x00000002)
  #define PRIMUP_TRIGGER_IMPH_TRIGGERED_MIN                            (0)
  #define PRIMUP_TRIGGER_IMPH_TRIGGERED_MAX                            (1) // 0x00000001
  #define PRIMUP_TRIGGER_IMPH_TRIGGERED_DEF                            (0x00000000)
  #define PRIMUP_TRIGGER_IMPH_TRIGGERED_HSH                            (0x01027070)

  #define PRIMUP_TRIGGER_IMPH_STALL_DNARB_OFF                          ( 2)
  #define PRIMUP_TRIGGER_IMPH_STALL_DNARB_WID                          ( 1)
  #define PRIMUP_TRIGGER_IMPH_STALL_DNARB_MSK                          (0x00000004)
  #define PRIMUP_TRIGGER_IMPH_STALL_DNARB_MIN                          (0)
  #define PRIMUP_TRIGGER_IMPH_STALL_DNARB_MAX                          (1) // 0x00000001
  #define PRIMUP_TRIGGER_IMPH_STALL_DNARB_DEF                          (0x00000000)
  #define PRIMUP_TRIGGER_IMPH_STALL_DNARB_HSH                          (0x01047070)

  #define PRIMUP_TRIGGER_IMPH_STALL_UPARB_OFF                          ( 3)
  #define PRIMUP_TRIGGER_IMPH_STALL_UPARB_WID                          ( 1)
  #define PRIMUP_TRIGGER_IMPH_STALL_UPARB_MSK                          (0x00000008)
  #define PRIMUP_TRIGGER_IMPH_STALL_UPARB_MIN                          (0)
  #define PRIMUP_TRIGGER_IMPH_STALL_UPARB_MAX                          (1) // 0x00000001
  #define PRIMUP_TRIGGER_IMPH_STALL_UPARB_DEF                          (0x00000000)
  #define PRIMUP_TRIGGER_IMPH_STALL_UPARB_HSH                          (0x01067070)

  #define PRIMUP_TRIGGER_IMPH_STALL_SNPARB_OFF                         ( 4)
  #define PRIMUP_TRIGGER_IMPH_STALL_SNPARB_WID                         ( 1)
  #define PRIMUP_TRIGGER_IMPH_STALL_SNPARB_MSK                         (0x00000010)
  #define PRIMUP_TRIGGER_IMPH_STALL_SNPARB_MIN                         (0)
  #define PRIMUP_TRIGGER_IMPH_STALL_SNPARB_MAX                         (1) // 0x00000001
  #define PRIMUP_TRIGGER_IMPH_STALL_SNPARB_DEF                         (0x00000000)
  #define PRIMUP_TRIGGER_IMPH_STALL_SNPARB_HSH                         (0x01087070)

  #define PRIMUP_TRIGGER_IMPH_STALL_PREVTDARB_OFF                      ( 5)
  #define PRIMUP_TRIGGER_IMPH_STALL_PREVTDARB_WID                      ( 1)
  #define PRIMUP_TRIGGER_IMPH_STALL_PREVTDARB_MSK                      (0x00000020)
  #define PRIMUP_TRIGGER_IMPH_STALL_PREVTDARB_MIN                      (0)
  #define PRIMUP_TRIGGER_IMPH_STALL_PREVTDARB_MAX                      (1) // 0x00000001
  #define PRIMUP_TRIGGER_IMPH_STALL_PREVTDARB_DEF                      (0x00000000)
  #define PRIMUP_TRIGGER_IMPH_STALL_PREVTDARB_HSH                      (0x010A7070)

  #define PRIMUP_TRIGGER_IMPH_STALL_FIFOARB_OFF                        ( 6)
  #define PRIMUP_TRIGGER_IMPH_STALL_FIFOARB_WID                        ( 1)
  #define PRIMUP_TRIGGER_IMPH_STALL_FIFOARB_MSK                        (0x00000040)
  #define PRIMUP_TRIGGER_IMPH_STALL_FIFOARB_MIN                        (0)
  #define PRIMUP_TRIGGER_IMPH_STALL_FIFOARB_MAX                        (1) // 0x00000001
  #define PRIMUP_TRIGGER_IMPH_STALL_FIFOARB_DEF                        (0x00000000)
  #define PRIMUP_TRIGGER_IMPH_STALL_FIFOARB_HSH                        (0x010C7070)

  #define PRIMUP_TRIGGER_IMPH_STALL_P2PARB_OFF                         ( 7)
  #define PRIMUP_TRIGGER_IMPH_STALL_P2PARB_WID                         ( 1)
  #define PRIMUP_TRIGGER_IMPH_STALL_P2PARB_MSK                         (0x00000080)
  #define PRIMUP_TRIGGER_IMPH_STALL_P2PARB_MIN                         (0)
  #define PRIMUP_TRIGGER_IMPH_STALL_P2PARB_MAX                         (1) // 0x00000001
  #define PRIMUP_TRIGGER_IMPH_STALL_P2PARB_DEF                         (0x00000000)
  #define PRIMUP_TRIGGER_IMPH_STALL_P2PARB_HSH                         (0x010E7070)

  #define PRIMUP_TRIGGER_IMPH_STALL_DELAY_OFF                          (23)
  #define PRIMUP_TRIGGER_IMPH_STALL_DELAY_WID                          ( 9)
  #define PRIMUP_TRIGGER_IMPH_STALL_DELAY_MSK                          (0xFF800000)
  #define PRIMUP_TRIGGER_IMPH_STALL_DELAY_MIN                          (0)
  #define PRIMUP_TRIGGER_IMPH_STALL_DELAY_MAX                          (511) // 0x000001FF
  #define PRIMUP_TRIGGER_IMPH_STALL_DELAY_DEF                          (0x00000000)
  #define PRIMUP_TRIGGER_IMPH_STALL_DELAY_HSH                          (0x092E7070)

#define TCSS_DEVEN_IMPH_REG                                            (0x00007090)

  #define TCSS_DEVEN_IMPH_PCIE0_EN_OFF                                 ( 0)
  #define TCSS_DEVEN_IMPH_PCIE0_EN_WID                                 ( 1)
  #define TCSS_DEVEN_IMPH_PCIE0_EN_MSK                                 (0x00000001)
  #define TCSS_DEVEN_IMPH_PCIE0_EN_MIN                                 (0)
  #define TCSS_DEVEN_IMPH_PCIE0_EN_MAX                                 (1) // 0x00000001
  #define TCSS_DEVEN_IMPH_PCIE0_EN_DEF                                 (0x00000001)
  #define TCSS_DEVEN_IMPH_PCIE0_EN_HSH                                 (0x01007090)

  #define TCSS_DEVEN_IMPH_PCIE1_EN_OFF                                 ( 1)
  #define TCSS_DEVEN_IMPH_PCIE1_EN_WID                                 ( 1)
  #define TCSS_DEVEN_IMPH_PCIE1_EN_MSK                                 (0x00000002)
  #define TCSS_DEVEN_IMPH_PCIE1_EN_MIN                                 (0)
  #define TCSS_DEVEN_IMPH_PCIE1_EN_MAX                                 (1) // 0x00000001
  #define TCSS_DEVEN_IMPH_PCIE1_EN_DEF                                 (0x00000001)
  #define TCSS_DEVEN_IMPH_PCIE1_EN_HSH                                 (0x01027090)

  #define TCSS_DEVEN_IMPH_PCIE2_EN_OFF                                 ( 2)
  #define TCSS_DEVEN_IMPH_PCIE2_EN_WID                                 ( 1)
  #define TCSS_DEVEN_IMPH_PCIE2_EN_MSK                                 (0x00000004)
  #define TCSS_DEVEN_IMPH_PCIE2_EN_MIN                                 (0)
  #define TCSS_DEVEN_IMPH_PCIE2_EN_MAX                                 (1) // 0x00000001
  #define TCSS_DEVEN_IMPH_PCIE2_EN_DEF                                 (0x00000001)
  #define TCSS_DEVEN_IMPH_PCIE2_EN_HSH                                 (0x01047090)

  #define TCSS_DEVEN_IMPH_PCIE3_EN_OFF                                 ( 3)
  #define TCSS_DEVEN_IMPH_PCIE3_EN_WID                                 ( 1)
  #define TCSS_DEVEN_IMPH_PCIE3_EN_MSK                                 (0x00000008)
  #define TCSS_DEVEN_IMPH_PCIE3_EN_MIN                                 (0)
  #define TCSS_DEVEN_IMPH_PCIE3_EN_MAX                                 (1) // 0x00000001
  #define TCSS_DEVEN_IMPH_PCIE3_EN_DEF                                 (0x00000001)
  #define TCSS_DEVEN_IMPH_PCIE3_EN_HSH                                 (0x01067090)

  #define TCSS_DEVEN_IMPH_PCIE4_EN_OFF                                 ( 4)
  #define TCSS_DEVEN_IMPH_PCIE4_EN_WID                                 ( 1)
  #define TCSS_DEVEN_IMPH_PCIE4_EN_MSK                                 (0x00000010)
  #define TCSS_DEVEN_IMPH_PCIE4_EN_MIN                                 (0)
  #define TCSS_DEVEN_IMPH_PCIE4_EN_MAX                                 (1) // 0x00000001
  #define TCSS_DEVEN_IMPH_PCIE4_EN_DEF                                 (0x00000001)
  #define TCSS_DEVEN_IMPH_PCIE4_EN_HSH                                 (0x01087090)

  #define TCSS_DEVEN_IMPH_PCIE5_EN_OFF                                 ( 5)
  #define TCSS_DEVEN_IMPH_PCIE5_EN_WID                                 ( 1)
  #define TCSS_DEVEN_IMPH_PCIE5_EN_MSK                                 (0x00000020)
  #define TCSS_DEVEN_IMPH_PCIE5_EN_MIN                                 (0)
  #define TCSS_DEVEN_IMPH_PCIE5_EN_MAX                                 (1) // 0x00000001
  #define TCSS_DEVEN_IMPH_PCIE5_EN_DEF                                 (0x00000001)
  #define TCSS_DEVEN_IMPH_PCIE5_EN_HSH                                 (0x010A7090)

  #define TCSS_DEVEN_IMPH_PCIE6_EN_OFF                                 ( 6)
  #define TCSS_DEVEN_IMPH_PCIE6_EN_WID                                 ( 1)
  #define TCSS_DEVEN_IMPH_PCIE6_EN_MSK                                 (0x00000040)
  #define TCSS_DEVEN_IMPH_PCIE6_EN_MIN                                 (0)
  #define TCSS_DEVEN_IMPH_PCIE6_EN_MAX                                 (1) // 0x00000001
  #define TCSS_DEVEN_IMPH_PCIE6_EN_DEF                                 (0x00000001)
  #define TCSS_DEVEN_IMPH_PCIE6_EN_HSH                                 (0x010C7090)

  #define TCSS_DEVEN_IMPH_PCIE7_EN_OFF                                 ( 7)
  #define TCSS_DEVEN_IMPH_PCIE7_EN_WID                                 ( 1)
  #define TCSS_DEVEN_IMPH_PCIE7_EN_MSK                                 (0x00000080)
  #define TCSS_DEVEN_IMPH_PCIE7_EN_MIN                                 (0)
  #define TCSS_DEVEN_IMPH_PCIE7_EN_MAX                                 (1) // 0x00000001
  #define TCSS_DEVEN_IMPH_PCIE7_EN_DEF                                 (0x00000001)
  #define TCSS_DEVEN_IMPH_PCIE7_EN_HSH                                 (0x010E7090)

  #define TCSS_DEVEN_IMPH_xHCI_EN_OFF                                  ( 8)
  #define TCSS_DEVEN_IMPH_xHCI_EN_WID                                  ( 1)
  #define TCSS_DEVEN_IMPH_xHCI_EN_MSK                                  (0x00000100)
  #define TCSS_DEVEN_IMPH_xHCI_EN_MIN                                  (0)
  #define TCSS_DEVEN_IMPH_xHCI_EN_MAX                                  (1) // 0x00000001
  #define TCSS_DEVEN_IMPH_xHCI_EN_DEF                                  (0x00000001)
  #define TCSS_DEVEN_IMPH_xHCI_EN_HSH                                  (0x01107090)

  #define TCSS_DEVEN_IMPH_xDCI_EN_OFF                                  ( 9)
  #define TCSS_DEVEN_IMPH_xDCI_EN_WID                                  ( 1)
  #define TCSS_DEVEN_IMPH_xDCI_EN_MSK                                  (0x00000200)
  #define TCSS_DEVEN_IMPH_xDCI_EN_MIN                                  (0)
  #define TCSS_DEVEN_IMPH_xDCI_EN_MAX                                  (1) // 0x00000001
  #define TCSS_DEVEN_IMPH_xDCI_EN_DEF                                  (0x00000001)
  #define TCSS_DEVEN_IMPH_xDCI_EN_HSH                                  (0x01127090)

  #define TCSS_DEVEN_IMPH_TBT_DMA0_EN_OFF                              (10)
  #define TCSS_DEVEN_IMPH_TBT_DMA0_EN_WID                              ( 1)
  #define TCSS_DEVEN_IMPH_TBT_DMA0_EN_MSK                              (0x00000400)
  #define TCSS_DEVEN_IMPH_TBT_DMA0_EN_MIN                              (0)
  #define TCSS_DEVEN_IMPH_TBT_DMA0_EN_MAX                              (1) // 0x00000001
  #define TCSS_DEVEN_IMPH_TBT_DMA0_EN_DEF                              (0x00000001)
  #define TCSS_DEVEN_IMPH_TBT_DMA0_EN_HSH                              (0x01147090)

  #define TCSS_DEVEN_IMPH_TBT_DMA1_EN_OFF                              (11)
  #define TCSS_DEVEN_IMPH_TBT_DMA1_EN_WID                              ( 1)
  #define TCSS_DEVEN_IMPH_TBT_DMA1_EN_MSK                              (0x00000800)
  #define TCSS_DEVEN_IMPH_TBT_DMA1_EN_MIN                              (0)
  #define TCSS_DEVEN_IMPH_TBT_DMA1_EN_MAX                              (1) // 0x00000001
  #define TCSS_DEVEN_IMPH_TBT_DMA1_EN_DEF                              (0x00000001)
  #define TCSS_DEVEN_IMPH_TBT_DMA1_EN_HSH                              (0x01167090)

  #define TCSS_DEVEN_IMPH_TBT_DMA2_EN_OFF                              (12)
  #define TCSS_DEVEN_IMPH_TBT_DMA2_EN_WID                              ( 1)
  #define TCSS_DEVEN_IMPH_TBT_DMA2_EN_MSK                              (0x00001000)
  #define TCSS_DEVEN_IMPH_TBT_DMA2_EN_MIN                              (0)
  #define TCSS_DEVEN_IMPH_TBT_DMA2_EN_MAX                              (1) // 0x00000001
  #define TCSS_DEVEN_IMPH_TBT_DMA2_EN_DEF                              (0x00000001)
  #define TCSS_DEVEN_IMPH_TBT_DMA2_EN_HSH                              (0x01187090)

  #define TCSS_DEVEN_IMPH_TBT_DMA3_EN_OFF                              (13)
  #define TCSS_DEVEN_IMPH_TBT_DMA3_EN_WID                              ( 1)
  #define TCSS_DEVEN_IMPH_TBT_DMA3_EN_MSK                              (0x00002000)
  #define TCSS_DEVEN_IMPH_TBT_DMA3_EN_MIN                              (0)
  #define TCSS_DEVEN_IMPH_TBT_DMA3_EN_MAX                              (1) // 0x00000001
  #define TCSS_DEVEN_IMPH_TBT_DMA3_EN_DEF                              (0x00000001)
  #define TCSS_DEVEN_IMPH_TBT_DMA3_EN_HSH                              (0x011A7090)

  #define TCSS_DEVEN_IMPH_Reserved_OFF                                 (14)
  #define TCSS_DEVEN_IMPH_Reserved_WID                                 (18)
  #define TCSS_DEVEN_IMPH_Reserved_MSK                                 (0xFFFFC000)
  #define TCSS_DEVEN_IMPH_Reserved_MIN                                 (0)
  #define TCSS_DEVEN_IMPH_Reserved_MAX                                 (262143) // 0x0003FFFF
  #define TCSS_DEVEN_IMPH_Reserved_DEF                                 (0x00000000)
  #define TCSS_DEVEN_IMPH_Reserved_HSH                                 (0x121C7090)

#define CAPID0_D_REG                                                   (0x00007094)

  #define CAPID0_D_TC_PCIE0_DIS_OFF                                    ( 0)
  #define CAPID0_D_TC_PCIE0_DIS_WID                                    ( 1)
  #define CAPID0_D_TC_PCIE0_DIS_MSK                                    (0x00000001)
  #define CAPID0_D_TC_PCIE0_DIS_MIN                                    (0)
  #define CAPID0_D_TC_PCIE0_DIS_MAX                                    (1) // 0x00000001
  #define CAPID0_D_TC_PCIE0_DIS_DEF                                    (0x00000000)
  #define CAPID0_D_TC_PCIE0_DIS_HSH                                    (0x01007094)

  #define CAPID0_D_TC_PCIE1_DIS_OFF                                    ( 1)
  #define CAPID0_D_TC_PCIE1_DIS_WID                                    ( 1)
  #define CAPID0_D_TC_PCIE1_DIS_MSK                                    (0x00000002)
  #define CAPID0_D_TC_PCIE1_DIS_MIN                                    (0)
  #define CAPID0_D_TC_PCIE1_DIS_MAX                                    (1) // 0x00000001
  #define CAPID0_D_TC_PCIE1_DIS_DEF                                    (0x00000000)
  #define CAPID0_D_TC_PCIE1_DIS_HSH                                    (0x01027094)

  #define CAPID0_D_TC_PCIE2_DIS_OFF                                    ( 2)
  #define CAPID0_D_TC_PCIE2_DIS_WID                                    ( 1)
  #define CAPID0_D_TC_PCIE2_DIS_MSK                                    (0x00000004)
  #define CAPID0_D_TC_PCIE2_DIS_MIN                                    (0)
  #define CAPID0_D_TC_PCIE2_DIS_MAX                                    (1) // 0x00000001
  #define CAPID0_D_TC_PCIE2_DIS_DEF                                    (0x00000000)
  #define CAPID0_D_TC_PCIE2_DIS_HSH                                    (0x01047094)

  #define CAPID0_D_TC_PCIE3_DIS_OFF                                    ( 3)
  #define CAPID0_D_TC_PCIE3_DIS_WID                                    ( 1)
  #define CAPID0_D_TC_PCIE3_DIS_MSK                                    (0x00000008)
  #define CAPID0_D_TC_PCIE3_DIS_MIN                                    (0)
  #define CAPID0_D_TC_PCIE3_DIS_MAX                                    (1) // 0x00000001
  #define CAPID0_D_TC_PCIE3_DIS_DEF                                    (0x00000000)
  #define CAPID0_D_TC_PCIE3_DIS_HSH                                    (0x01067094)

  #define CAPID0_D_TC_PCIE4_DIS_OFF                                    ( 4)
  #define CAPID0_D_TC_PCIE4_DIS_WID                                    ( 1)
  #define CAPID0_D_TC_PCIE4_DIS_MSK                                    (0x00000010)
  #define CAPID0_D_TC_PCIE4_DIS_MIN                                    (0)
  #define CAPID0_D_TC_PCIE4_DIS_MAX                                    (1) // 0x00000001
  #define CAPID0_D_TC_PCIE4_DIS_DEF                                    (0x00000000)
  #define CAPID0_D_TC_PCIE4_DIS_HSH                                    (0x01087094)

  #define CAPID0_D_TC_PCIE5_DIS_OFF                                    ( 5)
  #define CAPID0_D_TC_PCIE5_DIS_WID                                    ( 1)
  #define CAPID0_D_TC_PCIE5_DIS_MSK                                    (0x00000020)
  #define CAPID0_D_TC_PCIE5_DIS_MIN                                    (0)
  #define CAPID0_D_TC_PCIE5_DIS_MAX                                    (1) // 0x00000001
  #define CAPID0_D_TC_PCIE5_DIS_DEF                                    (0x00000000)
  #define CAPID0_D_TC_PCIE5_DIS_HSH                                    (0x010A7094)

  #define CAPID0_D_TC_PCIE6_DIS_OFF                                    ( 6)
  #define CAPID0_D_TC_PCIE6_DIS_WID                                    ( 1)
  #define CAPID0_D_TC_PCIE6_DIS_MSK                                    (0x00000040)
  #define CAPID0_D_TC_PCIE6_DIS_MIN                                    (0)
  #define CAPID0_D_TC_PCIE6_DIS_MAX                                    (1) // 0x00000001
  #define CAPID0_D_TC_PCIE6_DIS_DEF                                    (0x00000000)
  #define CAPID0_D_TC_PCIE6_DIS_HSH                                    (0x010C7094)

  #define CAPID0_D_TC_PCIE7_DIS_OFF                                    ( 7)
  #define CAPID0_D_TC_PCIE7_DIS_WID                                    ( 1)
  #define CAPID0_D_TC_PCIE7_DIS_MSK                                    (0x00000080)
  #define CAPID0_D_TC_PCIE7_DIS_MIN                                    (0)
  #define CAPID0_D_TC_PCIE7_DIS_MAX                                    (1) // 0x00000001
  #define CAPID0_D_TC_PCIE7_DIS_DEF                                    (0x00000000)
  #define CAPID0_D_TC_PCIE7_DIS_HSH                                    (0x010E7094)

  #define CAPID0_D_TC_XHCI_DIS_OFF                                     ( 8)
  #define CAPID0_D_TC_XHCI_DIS_WID                                     ( 1)
  #define CAPID0_D_TC_XHCI_DIS_MSK                                     (0x00000100)
  #define CAPID0_D_TC_XHCI_DIS_MIN                                     (0)
  #define CAPID0_D_TC_XHCI_DIS_MAX                                     (1) // 0x00000001
  #define CAPID0_D_TC_XHCI_DIS_DEF                                     (0x00000000)
  #define CAPID0_D_TC_XHCI_DIS_HSH                                     (0x01107094)

  #define CAPID0_D_TC_XDCI_DIS_OFF                                     ( 9)
  #define CAPID0_D_TC_XDCI_DIS_WID                                     ( 1)
  #define CAPID0_D_TC_XDCI_DIS_MSK                                     (0x00000200)
  #define CAPID0_D_TC_XDCI_DIS_MIN                                     (0)
  #define CAPID0_D_TC_XDCI_DIS_MAX                                     (1) // 0x00000001
  #define CAPID0_D_TC_XDCI_DIS_DEF                                     (0x00000000)
  #define CAPID0_D_TC_XDCI_DIS_HSH                                     (0x01127094)

  #define CAPID0_D_TC_TBT_DMA0_DIS_OFF                                 (10)
  #define CAPID0_D_TC_TBT_DMA0_DIS_WID                                 ( 1)
  #define CAPID0_D_TC_TBT_DMA0_DIS_MSK                                 (0x00000400)
  #define CAPID0_D_TC_TBT_DMA0_DIS_MIN                                 (0)
  #define CAPID0_D_TC_TBT_DMA0_DIS_MAX                                 (1) // 0x00000001
  #define CAPID0_D_TC_TBT_DMA0_DIS_DEF                                 (0x00000000)
  #define CAPID0_D_TC_TBT_DMA0_DIS_HSH                                 (0x01147094)

  #define CAPID0_D_TC_TBT_DMA1_DIS_OFF                                 (11)
  #define CAPID0_D_TC_TBT_DMA1_DIS_WID                                 ( 1)
  #define CAPID0_D_TC_TBT_DMA1_DIS_MSK                                 (0x00000800)
  #define CAPID0_D_TC_TBT_DMA1_DIS_MIN                                 (0)
  #define CAPID0_D_TC_TBT_DMA1_DIS_MAX                                 (1) // 0x00000001
  #define CAPID0_D_TC_TBT_DMA1_DIS_DEF                                 (0x00000000)
  #define CAPID0_D_TC_TBT_DMA1_DIS_HSH                                 (0x01167094)

  #define CAPID0_D_TC_TBT_DMA2_DIS_OFF                                 (12)
  #define CAPID0_D_TC_TBT_DMA2_DIS_WID                                 ( 1)
  #define CAPID0_D_TC_TBT_DMA2_DIS_MSK                                 (0x00001000)
  #define CAPID0_D_TC_TBT_DMA2_DIS_MIN                                 (0)
  #define CAPID0_D_TC_TBT_DMA2_DIS_MAX                                 (1) // 0x00000001
  #define CAPID0_D_TC_TBT_DMA2_DIS_DEF                                 (0x00000000)
  #define CAPID0_D_TC_TBT_DMA2_DIS_HSH                                 (0x01187094)

  #define CAPID0_D_SPARE_15_13_OFF                                     (13)
  #define CAPID0_D_SPARE_15_13_WID                                     ( 3)
  #define CAPID0_D_SPARE_15_13_MSK                                     (0x0000E000)
  #define CAPID0_D_SPARE_15_13_MIN                                     (0)
  #define CAPID0_D_SPARE_15_13_MAX                                     (7) // 0x00000007
  #define CAPID0_D_SPARE_15_13_DEF                                     (0x00000000)
  #define CAPID0_D_SPARE_15_13_HSH                                     (0x031A7094)

  #define CAPID0_D_IOM_DIS_OFF                                         (16)
  #define CAPID0_D_IOM_DIS_WID                                         ( 1)
  #define CAPID0_D_IOM_DIS_MSK                                         (0x00010000)
  #define CAPID0_D_IOM_DIS_MIN                                         (0)
  #define CAPID0_D_IOM_DIS_MAX                                         (1) // 0x00000001
  #define CAPID0_D_IOM_DIS_DEF                                         (0x00000000)
  #define CAPID0_D_IOM_DIS_HSH                                         (0x01207094)

  #define CAPID0_D_DPIN_PORT_COUNT_OFF                                 (17)
  #define CAPID0_D_DPIN_PORT_COUNT_WID                                 ( 3)
  #define CAPID0_D_DPIN_PORT_COUNT_MSK                                 (0x000E0000)
  #define CAPID0_D_DPIN_PORT_COUNT_MIN                                 (0)
  #define CAPID0_D_DPIN_PORT_COUNT_MAX                                 (7) // 0x00000007
  #define CAPID0_D_DPIN_PORT_COUNT_DEF                                 (0x00000000)
  #define CAPID0_D_DPIN_PORT_COUNT_HSH                                 (0x03227094)

  #define CAPID0_D_SPARE_31_20_OFF                                     (20)
  #define CAPID0_D_SPARE_31_20_WID                                     (12)
  #define CAPID0_D_SPARE_31_20_MSK                                     (0xFFF00000)
  #define CAPID0_D_SPARE_31_20_MIN                                     (0)
  #define CAPID0_D_SPARE_31_20_MAX                                     (4095) // 0x00000FFF
  #define CAPID0_D_SPARE_31_20_DEF                                     (0x00000000)
  #define CAPID0_D_SPARE_31_20_HSH                                     (0x0C287094)

#define CAPID0_F_0_0_0_PCI_REG                                         (0x00007098)

  #define CAPID0_F_0_0_0_PCI_Max_Data_Rate_At_GEAR1_OFF                ( 0)
  #define CAPID0_F_0_0_0_PCI_Max_Data_Rate_At_GEAR1_WID                ( 6)
  #define CAPID0_F_0_0_0_PCI_Max_Data_Rate_At_GEAR1_MSK                (0x0000003F)
  #define CAPID0_F_0_0_0_PCI_Max_Data_Rate_At_GEAR1_MIN                (0)
  #define CAPID0_F_0_0_0_PCI_Max_Data_Rate_At_GEAR1_MAX                (63) // 0x0000003F
  #define CAPID0_F_0_0_0_PCI_Max_Data_Rate_At_GEAR1_DEF                (0x00000000)
  #define CAPID0_F_0_0_0_PCI_Max_Data_Rate_At_GEAR1_HSH                (0x06007098)

  #define CAPID0_F_0_0_0_PCI_Reserved_OFF                              ( 6)
  #define CAPID0_F_0_0_0_PCI_Reserved_WID                              (26)
  #define CAPID0_F_0_0_0_PCI_Reserved_MSK                              (0xFFFFFFC0)
  #define CAPID0_F_0_0_0_PCI_Reserved_MIN                              (0)
  #define CAPID0_F_0_0_0_PCI_Reserved_MAX                              (67108863) // 0x03FFFFFF
  #define CAPID0_F_0_0_0_PCI_Reserved_DEF                              (0x00000000)
  #define CAPID0_F_0_0_0_PCI_Reserved_HSH                              (0x1A0C7098)

#define HCTL0_IMPH_REG                                                 (0x00007100)

  #define HCTL0_IMPH_PEG2DMIDIS_OFF                                    ( 0)
  #define HCTL0_IMPH_PEG2DMIDIS_WID                                    ( 1)
  #define HCTL0_IMPH_PEG2DMIDIS_MSK                                    (0x00000001)
  #define HCTL0_IMPH_PEG2DMIDIS_MIN                                    (0)
  #define HCTL0_IMPH_PEG2DMIDIS_MAX                                    (1) // 0x00000001
  #define HCTL0_IMPH_PEG2DMIDIS_DEF                                    (0x00000000)
  #define HCTL0_IMPH_PEG2DMIDIS_HSH                                    (0x01007100)

  #define HCTL0_IMPH_EOIB_OFF                                          ( 1)
  #define HCTL0_IMPH_EOIB_WID                                          ( 1)
  #define HCTL0_IMPH_EOIB_MSK                                          (0x00000002)
  #define HCTL0_IMPH_EOIB_MIN                                          (0)
  #define HCTL0_IMPH_EOIB_MAX                                          (1) // 0x00000001
  #define HCTL0_IMPH_EOIB_DEF                                          (0x00000000)
  #define HCTL0_IMPH_EOIB_HSH                                          (0x01027100)

  #define HCTL0_IMPH_MSIBYPDIS_OFF                                     ( 2)
  #define HCTL0_IMPH_MSIBYPDIS_WID                                     ( 1)
  #define HCTL0_IMPH_MSIBYPDIS_MSK                                     (0x00000004)
  #define HCTL0_IMPH_MSIBYPDIS_MIN                                     (0)
  #define HCTL0_IMPH_MSIBYPDIS_MAX                                     (1) // 0x00000001
  #define HCTL0_IMPH_MSIBYPDIS_DEF                                     (0x00000000)
  #define HCTL0_IMPH_MSIBYPDIS_HSH                                     (0x01047100)

  #define HCTL0_IMPH_TYPEC2DMIDIS_OFF                                  ( 3)
  #define HCTL0_IMPH_TYPEC2DMIDIS_WID                                  ( 1)
  #define HCTL0_IMPH_TYPEC2DMIDIS_MSK                                  (0x00000008)
  #define HCTL0_IMPH_TYPEC2DMIDIS_MIN                                  (0)
  #define HCTL0_IMPH_TYPEC2DMIDIS_MAX                                  (1) // 0x00000001
  #define HCTL0_IMPH_TYPEC2DMIDIS_DEF                                  (0x00000001)
  #define HCTL0_IMPH_TYPEC2DMIDIS_HSH                                  (0x01067100)

  #define HCTL0_IMPH_PHLDDIS_OFF                                       ( 4)
  #define HCTL0_IMPH_PHLDDIS_WID                                       ( 1)
  #define HCTL0_IMPH_PHLDDIS_MSK                                       (0x00000010)
  #define HCTL0_IMPH_PHLDDIS_MIN                                       (0)
  #define HCTL0_IMPH_PHLDDIS_MAX                                       (1) // 0x00000001
  #define HCTL0_IMPH_PHLDDIS_DEF                                       (0x00000000)
  #define HCTL0_IMPH_PHLDDIS_HSH                                       (0x01087100)

  #define HCTL0_IMPH_RSVD_8_5_OFF                                      ( 5)
  #define HCTL0_IMPH_RSVD_8_5_WID                                      ( 4)
  #define HCTL0_IMPH_RSVD_8_5_MSK                                      (0x000001E0)
  #define HCTL0_IMPH_RSVD_8_5_MIN                                      (0)
  #define HCTL0_IMPH_RSVD_8_5_MAX                                      (15) // 0x0000000F
  #define HCTL0_IMPH_RSVD_8_5_DEF                                      (0x00000000)
  #define HCTL0_IMPH_RSVD_8_5_HSH                                      (0x040A7100)

  #define HCTL0_IMPH_PHLDBLKDIS_OFF                                    ( 9)
  #define HCTL0_IMPH_PHLDBLKDIS_WID                                    ( 1)
  #define HCTL0_IMPH_PHLDBLKDIS_MSK                                    (0x00000200)
  #define HCTL0_IMPH_PHLDBLKDIS_MIN                                    (0)
  #define HCTL0_IMPH_PHLDBLKDIS_MAX                                    (1) // 0x00000001
  #define HCTL0_IMPH_PHLDBLKDIS_DEF                                    (0x00000000)
  #define HCTL0_IMPH_PHLDBLKDIS_HSH                                    (0x01127100)

  #define HCTL0_IMPH_DIS_VLW_PEG_OFF                                   (11)
  #define HCTL0_IMPH_DIS_VLW_PEG_WID                                   ( 1)
  #define HCTL0_IMPH_DIS_VLW_PEG_MSK                                   (0x00000800)
  #define HCTL0_IMPH_DIS_VLW_PEG_MIN                                   (0)
  #define HCTL0_IMPH_DIS_VLW_PEG_MAX                                   (1) // 0x00000001
  #define HCTL0_IMPH_DIS_VLW_PEG_DEF                                   (0x00000000)
  #define HCTL0_IMPH_DIS_VLW_PEG_HSH                                   (0x01167100)

  #define HCTL0_IMPH_SPECRDDIS_OFF                                     (12)
  #define HCTL0_IMPH_SPECRDDIS_WID                                     ( 1)
  #define HCTL0_IMPH_SPECRDDIS_MSK                                     (0x00001000)
  #define HCTL0_IMPH_SPECRDDIS_MIN                                     (0)
  #define HCTL0_IMPH_SPECRDDIS_MAX                                     (1) // 0x00000001
  #define HCTL0_IMPH_SPECRDDIS_DEF                                     (0x00000000)
  #define HCTL0_IMPH_SPECRDDIS_HSH                                     (0x01187100)

  #define HCTL0_IMPH_IMGU2DMIDIS_OFF                                   (14)
  #define HCTL0_IMPH_IMGU2DMIDIS_WID                                   ( 1)
  #define HCTL0_IMPH_IMGU2DMIDIS_MSK                                   (0x00004000)
  #define HCTL0_IMPH_IMGU2DMIDIS_MIN                                   (0)
  #define HCTL0_IMPH_IMGU2DMIDIS_MAX                                   (1) // 0x00000001
  #define HCTL0_IMPH_IMGU2DMIDIS_DEF                                   (0x00000001)
  #define HCTL0_IMPH_IMGU2DMIDIS_HSH                                   (0x011C7100)

  #define HCTL0_IMPH_GMM2DMIDIS_OFF                                    (15)
  #define HCTL0_IMPH_GMM2DMIDIS_WID                                    ( 1)
  #define HCTL0_IMPH_GMM2DMIDIS_MSK                                    (0x00008000)
  #define HCTL0_IMPH_GMM2DMIDIS_MIN                                    (0)
  #define HCTL0_IMPH_GMM2DMIDIS_MAX                                    (1) // 0x00000001
  #define HCTL0_IMPH_GMM2DMIDIS_DEF                                    (0x00000001)
  #define HCTL0_IMPH_GMM2DMIDIS_HSH                                    (0x011E7100)

  #define HCTL0_IMPH_FORCEREDISP_OFF                                   (16)
  #define HCTL0_IMPH_FORCEREDISP_WID                                   ( 1)
  #define HCTL0_IMPH_FORCEREDISP_MSK                                   (0x00010000)
  #define HCTL0_IMPH_FORCEREDISP_MIN                                   (0)
  #define HCTL0_IMPH_FORCEREDISP_MAX                                   (1) // 0x00000001
  #define HCTL0_IMPH_FORCEREDISP_DEF                                   (0x00000000)
  #define HCTL0_IMPH_FORCEREDISP_HSH                                   (0x01207100)

  #define HCTL0_IMPH_FORCE_WALK_SNP_OFF                                (17)
  #define HCTL0_IMPH_FORCE_WALK_SNP_WID                                ( 1)
  #define HCTL0_IMPH_FORCE_WALK_SNP_MSK                                (0x00020000)
  #define HCTL0_IMPH_FORCE_WALK_SNP_MIN                                (0)
  #define HCTL0_IMPH_FORCE_WALK_SNP_MAX                                (1) // 0x00000001
  #define HCTL0_IMPH_FORCE_WALK_SNP_DEF                                (0x00000000)
  #define HCTL0_IMPH_FORCE_WALK_SNP_HSH                                (0x01227100)

  #define HCTL0_IMPH_SNIFREGS_RTLCG_DIS_OFF                            (18)
  #define HCTL0_IMPH_SNIFREGS_RTLCG_DIS_WID                            ( 1)
  #define HCTL0_IMPH_SNIFREGS_RTLCG_DIS_MSK                            (0x00040000)
  #define HCTL0_IMPH_SNIFREGS_RTLCG_DIS_MIN                            (0)
  #define HCTL0_IMPH_SNIFREGS_RTLCG_DIS_MAX                            (1) // 0x00000001
  #define HCTL0_IMPH_SNIFREGS_RTLCG_DIS_DEF                            (0x00000000)
  #define HCTL0_IMPH_SNIFREGS_RTLCG_DIS_HSH                            (0x01247100)

  #define HCTL0_IMPH_DETERMINISTIC_BOUNDARIES_OFF                      (19)
  #define HCTL0_IMPH_DETERMINISTIC_BOUNDARIES_WID                      ( 1)
  #define HCTL0_IMPH_DETERMINISTIC_BOUNDARIES_MSK                      (0x00080000)
  #define HCTL0_IMPH_DETERMINISTIC_BOUNDARIES_MIN                      (0)
  #define HCTL0_IMPH_DETERMINISTIC_BOUNDARIES_MAX                      (1) // 0x00000001
  #define HCTL0_IMPH_DETERMINISTIC_BOUNDARIES_DEF                      (0x00000000)
  #define HCTL0_IMPH_DETERMINISTIC_BOUNDARIES_HSH                      (0x01267100)

  #define HCTL0_IMPH_WRC_DIS_OFF                                       (20)
  #define HCTL0_IMPH_WRC_DIS_WID                                       ( 1)
  #define HCTL0_IMPH_WRC_DIS_MSK                                       (0x00100000)
  #define HCTL0_IMPH_WRC_DIS_MIN                                       (0)
  #define HCTL0_IMPH_WRC_DIS_MAX                                       (1) // 0x00000001
  #define HCTL0_IMPH_WRC_DIS_DEF                                       (0x00000000)
  #define HCTL0_IMPH_WRC_DIS_HSH                                       (0x01287100)

  #define HCTL0_IMPH_DUAL_SNP_RSP_DIS_OFF                              (21)
  #define HCTL0_IMPH_DUAL_SNP_RSP_DIS_WID                              ( 1)
  #define HCTL0_IMPH_DUAL_SNP_RSP_DIS_MSK                              (0x00200000)
  #define HCTL0_IMPH_DUAL_SNP_RSP_DIS_MIN                              (0)
  #define HCTL0_IMPH_DUAL_SNP_RSP_DIS_MAX                              (1) // 0x00000001
  #define HCTL0_IMPH_DUAL_SNP_RSP_DIS_DEF                              (0x00000000)
  #define HCTL0_IMPH_DUAL_SNP_RSP_DIS_HSH                              (0x012A7100)

  #define HCTL0_IMPH_VCRT_WR_PRIO_CNTR_DEPTH_OFF                       (22)
  #define HCTL0_IMPH_VCRT_WR_PRIO_CNTR_DEPTH_WID                       ( 6)
  #define HCTL0_IMPH_VCRT_WR_PRIO_CNTR_DEPTH_MSK                       (0x0FC00000)
  #define HCTL0_IMPH_VCRT_WR_PRIO_CNTR_DEPTH_MIN                       (0)
  #define HCTL0_IMPH_VCRT_WR_PRIO_CNTR_DEPTH_MAX                       (63) // 0x0000003F
  #define HCTL0_IMPH_VCRT_WR_PRIO_CNTR_DEPTH_DEF                       (0x00000020)
  #define HCTL0_IMPH_VCRT_WR_PRIO_CNTR_DEPTH_HSH                       (0x062C7100)

  #define HCTL0_IMPH_RSVD_28_28_OFF                                    (28)
  #define HCTL0_IMPH_RSVD_28_28_WID                                    ( 1)
  #define HCTL0_IMPH_RSVD_28_28_MSK                                    (0x10000000)
  #define HCTL0_IMPH_RSVD_28_28_MIN                                    (0)
  #define HCTL0_IMPH_RSVD_28_28_MAX                                    (1) // 0x00000001
  #define HCTL0_IMPH_RSVD_28_28_DEF                                    (0x00000000)
  #define HCTL0_IMPH_RSVD_28_28_HSH                                    (0x01387100)

  #define HCTL0_IMPH_RSVD_29_29_OFF                                    (29)
  #define HCTL0_IMPH_RSVD_29_29_WID                                    ( 1)
  #define HCTL0_IMPH_RSVD_29_29_MSK                                    (0x20000000)
  #define HCTL0_IMPH_RSVD_29_29_MIN                                    (0)
  #define HCTL0_IMPH_RSVD_29_29_MAX                                    (1) // 0x00000001
  #define HCTL0_IMPH_RSVD_29_29_DEF                                    (0x00000000)
  #define HCTL0_IMPH_RSVD_29_29_HSH                                    (0x013A7100)

  #define HCTL0_IMPH_RSVD_31_30_OFF                                    (30)
  #define HCTL0_IMPH_RSVD_31_30_WID                                    ( 2)
  #define HCTL0_IMPH_RSVD_31_30_MSK                                    (0xC0000000)
  #define HCTL0_IMPH_RSVD_31_30_MIN                                    (0)
  #define HCTL0_IMPH_RSVD_31_30_MAX                                    (3) // 0x00000003
  #define HCTL0_IMPH_RSVD_31_30_DEF                                    (0x00000000)
  #define HCTL0_IMPH_RSVD_31_30_HSH                                    (0x023C7100)

#define REGBAR_IMPH_REG                                                (0x00007110)

  #define REGBAR_IMPH_REGBAREN_OFF                                     ( 0)
  #define REGBAR_IMPH_REGBAREN_WID                                     ( 1)
  #define REGBAR_IMPH_REGBAREN_MSK                                     (0x00000001)
  #define REGBAR_IMPH_REGBAREN_MIN                                     (0)
  #define REGBAR_IMPH_REGBAREN_MAX                                     (1) // 0x00000001
  #define REGBAR_IMPH_REGBAREN_DEF                                     (0x00000000)
  #define REGBAR_IMPH_REGBAREN_HSH                                     (0x41007110)

  #define REGBAR_IMPH_REGFBAR_OFF                                      (24)
  #define REGBAR_IMPH_REGFBAR_WID                                      (18)
  #define REGBAR_IMPH_REGFBAR_MSK                                      (0x000003FFFF000000ULL)
  #define REGBAR_IMPH_REGFBAR_MIN                                      (0)
  #define REGBAR_IMPH_REGFBAR_MAX                                      (262143) // 0x0003FFFF
  #define REGBAR_IMPH_REGFBAR_DEF                                      (0x00000000)
  #define REGBAR_IMPH_REGFBAR_HSH                                      (0x52307110)

#define GUNITACK_IMPH_REG                                              (0x0000715C)

  #define GUNITACK_IMPH_DPRACK_OFF                                     ( 0)
  #define GUNITACK_IMPH_DPRACK_WID                                     ( 1)
  #define GUNITACK_IMPH_DPRACK_MSK                                     (0x00000001)
  #define GUNITACK_IMPH_DPRACK_MIN                                     (0)
  #define GUNITACK_IMPH_DPRACK_MAX                                     (1) // 0x00000001
  #define GUNITACK_IMPH_DPRACK_DEF                                     (0x00000000)
  #define GUNITACK_IMPH_DPRACK_HSH                                     (0x0100715C)

  #define GUNITACK_IMPH_RSVD_OFF                                       ( 1)
  #define GUNITACK_IMPH_RSVD_WID                                       (31)
  #define GUNITACK_IMPH_RSVD_MSK                                       (0xFFFFFFFE)
  #define GUNITACK_IMPH_RSVD_MIN                                       (0)
  #define GUNITACK_IMPH_RSVD_MAX                                       (2147483647) // 0x7FFFFFFF
  #define GUNITACK_IMPH_RSVD_DEF                                       (0x00000000)
  #define GUNITACK_IMPH_RSVD_HSH                                       (0x1F02715C)

#define DIPACK_IMPH_REG                                                (0x00007160)
//Duplicate of GUNITACK_IMPH_REG

#define PKG_THERM_CAMARILLO_STATUS_IMPH_REG                            (0x00007200)

  #define PKG_THERM_CAMARILLO_STATUS_IMPH_THERMAL_MONITOR_STATUS_OFF   ( 0)
  #define PKG_THERM_CAMARILLO_STATUS_IMPH_THERMAL_MONITOR_STATUS_WID   ( 1)
  #define PKG_THERM_CAMARILLO_STATUS_IMPH_THERMAL_MONITOR_STATUS_MSK   (0x00000001)
  #define PKG_THERM_CAMARILLO_STATUS_IMPH_THERMAL_MONITOR_STATUS_MIN   (0)
  #define PKG_THERM_CAMARILLO_STATUS_IMPH_THERMAL_MONITOR_STATUS_MAX   (1) // 0x00000001
  #define PKG_THERM_CAMARILLO_STATUS_IMPH_THERMAL_MONITOR_STATUS_DEF   (0x00000000)
  #define PKG_THERM_CAMARILLO_STATUS_IMPH_THERMAL_MONITOR_STATUS_HSH   (0x01007200)

  #define PKG_THERM_CAMARILLO_STATUS_IMPH_THERMAL_MONITOR_LOG_OFF      ( 1)
  #define PKG_THERM_CAMARILLO_STATUS_IMPH_THERMAL_MONITOR_LOG_WID      ( 1)
  #define PKG_THERM_CAMARILLO_STATUS_IMPH_THERMAL_MONITOR_LOG_MSK      (0x00000002)
  #define PKG_THERM_CAMARILLO_STATUS_IMPH_THERMAL_MONITOR_LOG_MIN      (0)
  #define PKG_THERM_CAMARILLO_STATUS_IMPH_THERMAL_MONITOR_LOG_MAX      (1) // 0x00000001
  #define PKG_THERM_CAMARILLO_STATUS_IMPH_THERMAL_MONITOR_LOG_DEF      (0x00000000)
  #define PKG_THERM_CAMARILLO_STATUS_IMPH_THERMAL_MONITOR_LOG_HSH      (0x01027200)

  #define PKG_THERM_CAMARILLO_STATUS_IMPH_PROCHOT_STATUS_OFF           ( 2)
  #define PKG_THERM_CAMARILLO_STATUS_IMPH_PROCHOT_STATUS_WID           ( 1)
  #define PKG_THERM_CAMARILLO_STATUS_IMPH_PROCHOT_STATUS_MSK           (0x00000004)
  #define PKG_THERM_CAMARILLO_STATUS_IMPH_PROCHOT_STATUS_MIN           (0)
  #define PKG_THERM_CAMARILLO_STATUS_IMPH_PROCHOT_STATUS_MAX           (1) // 0x00000001
  #define PKG_THERM_CAMARILLO_STATUS_IMPH_PROCHOT_STATUS_DEF           (0x00000000)
  #define PKG_THERM_CAMARILLO_STATUS_IMPH_PROCHOT_STATUS_HSH           (0x01047200)

  #define PKG_THERM_CAMARILLO_STATUS_IMPH_PROCHOT_LOG_OFF              ( 3)
  #define PKG_THERM_CAMARILLO_STATUS_IMPH_PROCHOT_LOG_WID              ( 1)
  #define PKG_THERM_CAMARILLO_STATUS_IMPH_PROCHOT_LOG_MSK              (0x00000008)
  #define PKG_THERM_CAMARILLO_STATUS_IMPH_PROCHOT_LOG_MIN              (0)
  #define PKG_THERM_CAMARILLO_STATUS_IMPH_PROCHOT_LOG_MAX              (1) // 0x00000001
  #define PKG_THERM_CAMARILLO_STATUS_IMPH_PROCHOT_LOG_DEF              (0x00000000)
  #define PKG_THERM_CAMARILLO_STATUS_IMPH_PROCHOT_LOG_HSH              (0x01067200)

  #define PKG_THERM_CAMARILLO_STATUS_IMPH_OUT_OF_SPEC_STATUS_OFF       ( 4)
  #define PKG_THERM_CAMARILLO_STATUS_IMPH_OUT_OF_SPEC_STATUS_WID       ( 1)
  #define PKG_THERM_CAMARILLO_STATUS_IMPH_OUT_OF_SPEC_STATUS_MSK       (0x00000010)
  #define PKG_THERM_CAMARILLO_STATUS_IMPH_OUT_OF_SPEC_STATUS_MIN       (0)
  #define PKG_THERM_CAMARILLO_STATUS_IMPH_OUT_OF_SPEC_STATUS_MAX       (1) // 0x00000001
  #define PKG_THERM_CAMARILLO_STATUS_IMPH_OUT_OF_SPEC_STATUS_DEF       (0x00000000)
  #define PKG_THERM_CAMARILLO_STATUS_IMPH_OUT_OF_SPEC_STATUS_HSH       (0x01087200)

  #define PKG_THERM_CAMARILLO_STATUS_IMPH_OUT_OF_SPEC_LOG_OFF          ( 5)
  #define PKG_THERM_CAMARILLO_STATUS_IMPH_OUT_OF_SPEC_LOG_WID          ( 1)
  #define PKG_THERM_CAMARILLO_STATUS_IMPH_OUT_OF_SPEC_LOG_MSK          (0x00000020)
  #define PKG_THERM_CAMARILLO_STATUS_IMPH_OUT_OF_SPEC_LOG_MIN          (0)
  #define PKG_THERM_CAMARILLO_STATUS_IMPH_OUT_OF_SPEC_LOG_MAX          (1) // 0x00000001
  #define PKG_THERM_CAMARILLO_STATUS_IMPH_OUT_OF_SPEC_LOG_DEF          (0x00000000)
  #define PKG_THERM_CAMARILLO_STATUS_IMPH_OUT_OF_SPEC_LOG_HSH          (0x010A7200)

  #define PKG_THERM_CAMARILLO_STATUS_IMPH_THRESHOLD1_STATUS_OFF        ( 6)
  #define PKG_THERM_CAMARILLO_STATUS_IMPH_THRESHOLD1_STATUS_WID        ( 1)
  #define PKG_THERM_CAMARILLO_STATUS_IMPH_THRESHOLD1_STATUS_MSK        (0x00000040)
  #define PKG_THERM_CAMARILLO_STATUS_IMPH_THRESHOLD1_STATUS_MIN        (0)
  #define PKG_THERM_CAMARILLO_STATUS_IMPH_THRESHOLD1_STATUS_MAX        (1) // 0x00000001
  #define PKG_THERM_CAMARILLO_STATUS_IMPH_THRESHOLD1_STATUS_DEF        (0x00000000)
  #define PKG_THERM_CAMARILLO_STATUS_IMPH_THRESHOLD1_STATUS_HSH        (0x010C7200)

  #define PKG_THERM_CAMARILLO_STATUS_IMPH_THRESHOLD1_LOG_OFF           ( 7)
  #define PKG_THERM_CAMARILLO_STATUS_IMPH_THRESHOLD1_LOG_WID           ( 1)
  #define PKG_THERM_CAMARILLO_STATUS_IMPH_THRESHOLD1_LOG_MSK           (0x00000080)
  #define PKG_THERM_CAMARILLO_STATUS_IMPH_THRESHOLD1_LOG_MIN           (0)
  #define PKG_THERM_CAMARILLO_STATUS_IMPH_THRESHOLD1_LOG_MAX           (1) // 0x00000001
  #define PKG_THERM_CAMARILLO_STATUS_IMPH_THRESHOLD1_LOG_DEF           (0x00000000)
  #define PKG_THERM_CAMARILLO_STATUS_IMPH_THRESHOLD1_LOG_HSH           (0x010E7200)

  #define PKG_THERM_CAMARILLO_STATUS_IMPH_THRESHOLD2_STATUS_OFF        ( 8)
  #define PKG_THERM_CAMARILLO_STATUS_IMPH_THRESHOLD2_STATUS_WID        ( 1)
  #define PKG_THERM_CAMARILLO_STATUS_IMPH_THRESHOLD2_STATUS_MSK        (0x00000100)
  #define PKG_THERM_CAMARILLO_STATUS_IMPH_THRESHOLD2_STATUS_MIN        (0)
  #define PKG_THERM_CAMARILLO_STATUS_IMPH_THRESHOLD2_STATUS_MAX        (1) // 0x00000001
  #define PKG_THERM_CAMARILLO_STATUS_IMPH_THRESHOLD2_STATUS_DEF        (0x00000000)
  #define PKG_THERM_CAMARILLO_STATUS_IMPH_THRESHOLD2_STATUS_HSH        (0x01107200)

  #define PKG_THERM_CAMARILLO_STATUS_IMPH_THRESHOLD2_LOG_OFF           ( 9)
  #define PKG_THERM_CAMARILLO_STATUS_IMPH_THRESHOLD2_LOG_WID           ( 1)
  #define PKG_THERM_CAMARILLO_STATUS_IMPH_THRESHOLD2_LOG_MSK           (0x00000200)
  #define PKG_THERM_CAMARILLO_STATUS_IMPH_THRESHOLD2_LOG_MIN           (0)
  #define PKG_THERM_CAMARILLO_STATUS_IMPH_THRESHOLD2_LOG_MAX           (1) // 0x00000001
  #define PKG_THERM_CAMARILLO_STATUS_IMPH_THRESHOLD2_LOG_DEF           (0x00000000)
  #define PKG_THERM_CAMARILLO_STATUS_IMPH_THRESHOLD2_LOG_HSH           (0x01127200)

  #define PKG_THERM_CAMARILLO_STATUS_IMPH_POWER_LIMITATION_STATUS_OFF  (10)
  #define PKG_THERM_CAMARILLO_STATUS_IMPH_POWER_LIMITATION_STATUS_WID  ( 1)
  #define PKG_THERM_CAMARILLO_STATUS_IMPH_POWER_LIMITATION_STATUS_MSK  (0x00000400)
  #define PKG_THERM_CAMARILLO_STATUS_IMPH_POWER_LIMITATION_STATUS_MIN  (0)
  #define PKG_THERM_CAMARILLO_STATUS_IMPH_POWER_LIMITATION_STATUS_MAX  (1) // 0x00000001
  #define PKG_THERM_CAMARILLO_STATUS_IMPH_POWER_LIMITATION_STATUS_DEF  (0x00000000)
  #define PKG_THERM_CAMARILLO_STATUS_IMPH_POWER_LIMITATION_STATUS_HSH  (0x01147200)

  #define PKG_THERM_CAMARILLO_STATUS_IMPH_POWER_LIMITATION_LOG_OFF     (11)
  #define PKG_THERM_CAMARILLO_STATUS_IMPH_POWER_LIMITATION_LOG_WID     ( 1)
  #define PKG_THERM_CAMARILLO_STATUS_IMPH_POWER_LIMITATION_LOG_MSK     (0x00000800)
  #define PKG_THERM_CAMARILLO_STATUS_IMPH_POWER_LIMITATION_LOG_MIN     (0)
  #define PKG_THERM_CAMARILLO_STATUS_IMPH_POWER_LIMITATION_LOG_MAX     (1) // 0x00000001
  #define PKG_THERM_CAMARILLO_STATUS_IMPH_POWER_LIMITATION_LOG_DEF     (0x00000000)
  #define PKG_THERM_CAMARILLO_STATUS_IMPH_POWER_LIMITATION_LOG_HSH     (0x01167200)

  #define PKG_THERM_CAMARILLO_STATUS_IMPH_Temperature_OFF              (16)
  #define PKG_THERM_CAMARILLO_STATUS_IMPH_Temperature_WID              ( 7)
  #define PKG_THERM_CAMARILLO_STATUS_IMPH_Temperature_MSK              (0x007F0000)
  #define PKG_THERM_CAMARILLO_STATUS_IMPH_Temperature_MIN              (0)
  #define PKG_THERM_CAMARILLO_STATUS_IMPH_Temperature_MAX              (127) // 0x0000007F
  #define PKG_THERM_CAMARILLO_STATUS_IMPH_Temperature_DEF              (0x00000000)
  #define PKG_THERM_CAMARILLO_STATUS_IMPH_Temperature_HSH              (0x07207200)

  #define PKG_THERM_CAMARILLO_STATUS_IMPH_Resolution_OFF               (27)
  #define PKG_THERM_CAMARILLO_STATUS_IMPH_Resolution_WID               ( 4)
  #define PKG_THERM_CAMARILLO_STATUS_IMPH_Resolution_MSK               (0x78000000)
  #define PKG_THERM_CAMARILLO_STATUS_IMPH_Resolution_MIN               (0)
  #define PKG_THERM_CAMARILLO_STATUS_IMPH_Resolution_MAX               (15) // 0x0000000F
  #define PKG_THERM_CAMARILLO_STATUS_IMPH_Resolution_DEF               (0x00000001)
  #define PKG_THERM_CAMARILLO_STATUS_IMPH_Resolution_HSH               (0x04367200)

  #define PKG_THERM_CAMARILLO_STATUS_IMPH_Valid_OFF                    (31)
  #define PKG_THERM_CAMARILLO_STATUS_IMPH_Valid_WID                    ( 1)
  #define PKG_THERM_CAMARILLO_STATUS_IMPH_Valid_MSK                    (0x80000000)
  #define PKG_THERM_CAMARILLO_STATUS_IMPH_Valid_MIN                    (0)
  #define PKG_THERM_CAMARILLO_STATUS_IMPH_Valid_MAX                    (1) // 0x00000001
  #define PKG_THERM_CAMARILLO_STATUS_IMPH_Valid_DEF                    (0x00000000)
  #define PKG_THERM_CAMARILLO_STATUS_IMPH_Valid_HSH                    (0x013E7200)

#define DDR_THERM_CAMARILLO_STATUS_IMPH_REG                            (0x00007204)

  #define DDR_THERM_CAMARILLO_STATUS_IMPH_THRESHOLD1_STATUS_OFF        ( 8)
  #define DDR_THERM_CAMARILLO_STATUS_IMPH_THRESHOLD1_STATUS_WID        ( 1)
  #define DDR_THERM_CAMARILLO_STATUS_IMPH_THRESHOLD1_STATUS_MSK        (0x00000100)
  #define DDR_THERM_CAMARILLO_STATUS_IMPH_THRESHOLD1_STATUS_MIN        (0)
  #define DDR_THERM_CAMARILLO_STATUS_IMPH_THRESHOLD1_STATUS_MAX        (1) // 0x00000001
  #define DDR_THERM_CAMARILLO_STATUS_IMPH_THRESHOLD1_STATUS_DEF        (0x00000000)
  #define DDR_THERM_CAMARILLO_STATUS_IMPH_THRESHOLD1_STATUS_HSH        (0x01107204)

  #define DDR_THERM_CAMARILLO_STATUS_IMPH_THRESHOLD1_LOG_OFF           ( 9)
  #define DDR_THERM_CAMARILLO_STATUS_IMPH_THRESHOLD1_LOG_WID           ( 1)
  #define DDR_THERM_CAMARILLO_STATUS_IMPH_THRESHOLD1_LOG_MSK           (0x00000200)
  #define DDR_THERM_CAMARILLO_STATUS_IMPH_THRESHOLD1_LOG_MIN           (0)
  #define DDR_THERM_CAMARILLO_STATUS_IMPH_THRESHOLD1_LOG_MAX           (1) // 0x00000001
  #define DDR_THERM_CAMARILLO_STATUS_IMPH_THRESHOLD1_LOG_DEF           (0x00000000)
  #define DDR_THERM_CAMARILLO_STATUS_IMPH_THRESHOLD1_LOG_HSH           (0x01127204)

  #define DDR_THERM_CAMARILLO_STATUS_IMPH_THRESHOLD2_STATUS_OFF        (10)
  #define DDR_THERM_CAMARILLO_STATUS_IMPH_THRESHOLD2_STATUS_WID        ( 1)
  #define DDR_THERM_CAMARILLO_STATUS_IMPH_THRESHOLD2_STATUS_MSK        (0x00000400)
  #define DDR_THERM_CAMARILLO_STATUS_IMPH_THRESHOLD2_STATUS_MIN        (0)
  #define DDR_THERM_CAMARILLO_STATUS_IMPH_THRESHOLD2_STATUS_MAX        (1) // 0x00000001
  #define DDR_THERM_CAMARILLO_STATUS_IMPH_THRESHOLD2_STATUS_DEF        (0x00000000)
  #define DDR_THERM_CAMARILLO_STATUS_IMPH_THRESHOLD2_STATUS_HSH        (0x01147204)

  #define DDR_THERM_CAMARILLO_STATUS_IMPH_THRESHOLD2_LOG_OFF           (11)
  #define DDR_THERM_CAMARILLO_STATUS_IMPH_THRESHOLD2_LOG_WID           ( 1)
  #define DDR_THERM_CAMARILLO_STATUS_IMPH_THRESHOLD2_LOG_MSK           (0x00000800)
  #define DDR_THERM_CAMARILLO_STATUS_IMPH_THRESHOLD2_LOG_MIN           (0)
  #define DDR_THERM_CAMARILLO_STATUS_IMPH_THRESHOLD2_LOG_MAX           (1) // 0x00000001
  #define DDR_THERM_CAMARILLO_STATUS_IMPH_THRESHOLD2_LOG_DEF           (0x00000000)
  #define DDR_THERM_CAMARILLO_STATUS_IMPH_THRESHOLD2_LOG_HSH           (0x01167204)

#define IOP_HASH_CONFIG_IMPH_REG                                       (0x00007250)

  #define IOP_HASH_CONFIG_IMPH_RESERVED1_OFF                           ( 0)
  #define IOP_HASH_CONFIG_IMPH_RESERVED1_WID                           ( 6)
  #define IOP_HASH_CONFIG_IMPH_RESERVED1_MSK                           (0x0000003F)
  #define IOP_HASH_CONFIG_IMPH_RESERVED1_MIN                           (0)
  #define IOP_HASH_CONFIG_IMPH_RESERVED1_MAX                           (63) // 0x0000003F
  #define IOP_HASH_CONFIG_IMPH_RESERVED1_DEF                           (0x00000000)
  #define IOP_HASH_CONFIG_IMPH_RESERVED1_HSH                           (0x06007250)

  #define IOP_HASH_CONFIG_IMPH_HASH_MASK_OFF                           ( 6)
  #define IOP_HASH_CONFIG_IMPH_HASH_MASK_WID                           (14)
  #define IOP_HASH_CONFIG_IMPH_HASH_MASK_MSK                           (0x000FFFC0)
  #define IOP_HASH_CONFIG_IMPH_HASH_MASK_MIN                           (0)
  #define IOP_HASH_CONFIG_IMPH_HASH_MASK_MAX                           (16383) // 0x00003FFF
  #define IOP_HASH_CONFIG_IMPH_HASH_MASK_DEF                           (0x00000050)
  #define IOP_HASH_CONFIG_IMPH_HASH_MASK_HSH                           (0x0E0C7250)

  #define IOP_HASH_CONFIG_IMPH_RESERVED_OFF                            (20)
  #define IOP_HASH_CONFIG_IMPH_RESERVED_WID                            (10)
  #define IOP_HASH_CONFIG_IMPH_RESERVED_MSK                            (0x3FF00000)
  #define IOP_HASH_CONFIG_IMPH_RESERVED_MIN                            (0)
  #define IOP_HASH_CONFIG_IMPH_RESERVED_MAX                            (1023) // 0x000003FF
  #define IOP_HASH_CONFIG_IMPH_RESERVED_DEF                            (0x00000000)
  #define IOP_HASH_CONFIG_IMPH_RESERVED_HSH                            (0x0A287250)

  #define IOP_HASH_CONFIG_IMPH_IDP_HASH_DISABLE_OFF                    (30)
  #define IOP_HASH_CONFIG_IMPH_IDP_HASH_DISABLE_WID                    ( 1)
  #define IOP_HASH_CONFIG_IMPH_IDP_HASH_DISABLE_MSK                    (0x40000000)
  #define IOP_HASH_CONFIG_IMPH_IDP_HASH_DISABLE_MIN                    (0)
  #define IOP_HASH_CONFIG_IMPH_IDP_HASH_DISABLE_MAX                    (1) // 0x00000001
  #define IOP_HASH_CONFIG_IMPH_IDP_HASH_DISABLE_DEF                    (0x00000000)
  #define IOP_HASH_CONFIG_IMPH_IDP_HASH_DISABLE_HSH                    (0x013C7250)

  #define IOP_HASH_CONFIG_IMPH_CMF_HASH_DISABLE_OFF                    (31)
  #define IOP_HASH_CONFIG_IMPH_CMF_HASH_DISABLE_WID                    ( 1)
  #define IOP_HASH_CONFIG_IMPH_CMF_HASH_DISABLE_MSK                    (0x80000000)
  #define IOP_HASH_CONFIG_IMPH_CMF_HASH_DISABLE_MIN                    (0)
  #define IOP_HASH_CONFIG_IMPH_CMF_HASH_DISABLE_MAX                    (1) // 0x00000001
  #define IOP_HASH_CONFIG_IMPH_CMF_HASH_DISABLE_DEF                    (0x00000001)
  #define IOP_HASH_CONFIG_IMPH_CMF_HASH_DISABLE_HSH                    (0x013E7250)

#define CMF_TOPOLOGY_GLOBAL_CFG_0_MCHBAR_IMPH_REG                      (0x00007254)

  #define CMF_TOPOLOGY_GLOBAL_CFG_0_MCHBAR_IMPH_SEL_2LM_1LM_OFF        ( 0)
  #define CMF_TOPOLOGY_GLOBAL_CFG_0_MCHBAR_IMPH_SEL_2LM_1LM_WID        ( 1)
  #define CMF_TOPOLOGY_GLOBAL_CFG_0_MCHBAR_IMPH_SEL_2LM_1LM_MSK        (0x00000001)
  #define CMF_TOPOLOGY_GLOBAL_CFG_0_MCHBAR_IMPH_SEL_2LM_1LM_MIN        (0)
  #define CMF_TOPOLOGY_GLOBAL_CFG_0_MCHBAR_IMPH_SEL_2LM_1LM_MAX        (1) // 0x00000001
  #define CMF_TOPOLOGY_GLOBAL_CFG_0_MCHBAR_IMPH_SEL_2LM_1LM_DEF        (0x00000000)
  #define CMF_TOPOLOGY_GLOBAL_CFG_0_MCHBAR_IMPH_SEL_2LM_1LM_HSH        (0x01007254)

  #define CMF_TOPOLOGY_GLOBAL_CFG_0_MCHBAR_IMPH_SEL_TMECC_ON_OFF       ( 1)
  #define CMF_TOPOLOGY_GLOBAL_CFG_0_MCHBAR_IMPH_SEL_TMECC_ON_WID       ( 1)
  #define CMF_TOPOLOGY_GLOBAL_CFG_0_MCHBAR_IMPH_SEL_TMECC_ON_MSK       (0x00000002)
  #define CMF_TOPOLOGY_GLOBAL_CFG_0_MCHBAR_IMPH_SEL_TMECC_ON_MIN       (0)
  #define CMF_TOPOLOGY_GLOBAL_CFG_0_MCHBAR_IMPH_SEL_TMECC_ON_MAX       (1) // 0x00000001
  #define CMF_TOPOLOGY_GLOBAL_CFG_0_MCHBAR_IMPH_SEL_TMECC_ON_DEF       (0x00000000)
  #define CMF_TOPOLOGY_GLOBAL_CFG_0_MCHBAR_IMPH_SEL_TMECC_ON_HSH       (0x01027254)

  #define CMF_TOPOLOGY_GLOBAL_CFG_0_MCHBAR_IMPH_Reserved_0_OFF         ( 2)
  #define CMF_TOPOLOGY_GLOBAL_CFG_0_MCHBAR_IMPH_Reserved_0_WID         ( 6)
  #define CMF_TOPOLOGY_GLOBAL_CFG_0_MCHBAR_IMPH_Reserved_0_MSK         (0x000000FC)
  #define CMF_TOPOLOGY_GLOBAL_CFG_0_MCHBAR_IMPH_Reserved_0_MIN         (0)
  #define CMF_TOPOLOGY_GLOBAL_CFG_0_MCHBAR_IMPH_Reserved_0_MAX         (63) // 0x0000003F
  #define CMF_TOPOLOGY_GLOBAL_CFG_0_MCHBAR_IMPH_Reserved_0_DEF         (0x00000000)
  #define CMF_TOPOLOGY_GLOBAL_CFG_0_MCHBAR_IMPH_Reserved_0_HSH         (0x06047254)

  #define CMF_TOPOLOGY_GLOBAL_CFG_0_MCHBAR_IMPH_SLICE_0_DISABLE_OFF    ( 8)
  #define CMF_TOPOLOGY_GLOBAL_CFG_0_MCHBAR_IMPH_SLICE_0_DISABLE_WID    ( 1)
  #define CMF_TOPOLOGY_GLOBAL_CFG_0_MCHBAR_IMPH_SLICE_0_DISABLE_MSK    (0x00000100)
  #define CMF_TOPOLOGY_GLOBAL_CFG_0_MCHBAR_IMPH_SLICE_0_DISABLE_MIN    (0)
  #define CMF_TOPOLOGY_GLOBAL_CFG_0_MCHBAR_IMPH_SLICE_0_DISABLE_MAX    (1) // 0x00000001
  #define CMF_TOPOLOGY_GLOBAL_CFG_0_MCHBAR_IMPH_SLICE_0_DISABLE_DEF    (0x00000000)
  #define CMF_TOPOLOGY_GLOBAL_CFG_0_MCHBAR_IMPH_SLICE_0_DISABLE_HSH    (0x01107254)

  #define CMF_TOPOLOGY_GLOBAL_CFG_0_MCHBAR_IMPH_SLICE_1_DISABLE_OFF    ( 9)
  #define CMF_TOPOLOGY_GLOBAL_CFG_0_MCHBAR_IMPH_SLICE_1_DISABLE_WID    ( 1)
  #define CMF_TOPOLOGY_GLOBAL_CFG_0_MCHBAR_IMPH_SLICE_1_DISABLE_MSK    (0x00000200)
  #define CMF_TOPOLOGY_GLOBAL_CFG_0_MCHBAR_IMPH_SLICE_1_DISABLE_MIN    (0)
  #define CMF_TOPOLOGY_GLOBAL_CFG_0_MCHBAR_IMPH_SLICE_1_DISABLE_MAX    (1) // 0x00000001
  #define CMF_TOPOLOGY_GLOBAL_CFG_0_MCHBAR_IMPH_SLICE_1_DISABLE_DEF    (0x00000000)
  #define CMF_TOPOLOGY_GLOBAL_CFG_0_MCHBAR_IMPH_SLICE_1_DISABLE_HSH    (0x01127254)

  #define CMF_TOPOLOGY_GLOBAL_CFG_0_MCHBAR_IMPH_Reserved_1_OFF         (10)
  #define CMF_TOPOLOGY_GLOBAL_CFG_0_MCHBAR_IMPH_Reserved_1_WID         (22)
  #define CMF_TOPOLOGY_GLOBAL_CFG_0_MCHBAR_IMPH_Reserved_1_MSK         (0xFFFFFC00)
  #define CMF_TOPOLOGY_GLOBAL_CFG_0_MCHBAR_IMPH_Reserved_1_MIN         (0)
  #define CMF_TOPOLOGY_GLOBAL_CFG_0_MCHBAR_IMPH_Reserved_1_MAX         (4194303) // 0x003FFFFF
  #define CMF_TOPOLOGY_GLOBAL_CFG_0_MCHBAR_IMPH_Reserved_1_DEF         (0x00000000)
  #define CMF_TOPOLOGY_GLOBAL_CFG_0_MCHBAR_IMPH_Reserved_1_HSH         (0x16147254)

#define REMAP_BASE_MCHBAR_IMPH_REG                                     (0x00007258)

  #define REMAP_BASE_MCHBAR_IMPH_Reserved_0_OFF                        ( 0)
  #define REMAP_BASE_MCHBAR_IMPH_Reserved_0_WID                        (20)
  #define REMAP_BASE_MCHBAR_IMPH_Reserved_0_MSK                        (0x000FFFFF)
  #define REMAP_BASE_MCHBAR_IMPH_Reserved_0_MIN                        (0)
  #define REMAP_BASE_MCHBAR_IMPH_Reserved_0_MAX                        (1048575) // 0x000FFFFF
  #define REMAP_BASE_MCHBAR_IMPH_Reserved_0_DEF                        (0x00000000)
  #define REMAP_BASE_MCHBAR_IMPH_Reserved_0_HSH                        (0x54007258)

  #define REMAP_BASE_MCHBAR_IMPH_REMAPBASE_OFF                         (20)
  #define REMAP_BASE_MCHBAR_IMPH_REMAPBASE_WID                         (22)
  #define REMAP_BASE_MCHBAR_IMPH_REMAPBASE_MSK                         (0x000003FFFFF00000ULL)
  #define REMAP_BASE_MCHBAR_IMPH_REMAPBASE_MIN                         (0)
  #define REMAP_BASE_MCHBAR_IMPH_REMAPBASE_MAX                         (4194303) // 0x003FFFFF
  #define REMAP_BASE_MCHBAR_IMPH_REMAPBASE_DEF                         (0x0007FFFF)
  #define REMAP_BASE_MCHBAR_IMPH_REMAPBASE_HSH                         (0x56287258)

  #define REMAP_BASE_MCHBAR_IMPH_Reserved_1_OFF                        (42)
  #define REMAP_BASE_MCHBAR_IMPH_Reserved_1_WID                        (22)
  #define REMAP_BASE_MCHBAR_IMPH_Reserved_1_MSK                        (0xFFFFFC0000000000ULL)
  #define REMAP_BASE_MCHBAR_IMPH_Reserved_1_MIN                        (0)
  #define REMAP_BASE_MCHBAR_IMPH_Reserved_1_MAX                        (4194303) // 0x003FFFFF
  #define REMAP_BASE_MCHBAR_IMPH_Reserved_1_DEF                        (0x00000000)
  #define REMAP_BASE_MCHBAR_IMPH_Reserved_1_HSH                        (0x56547258)

#define REMAP_LIMIT_MCHBAR_IMPH_REG                                    (0x00007260)

  #define REMAP_LIMIT_MCHBAR_IMPH_Reserved_0_OFF                       ( 0)
  #define REMAP_LIMIT_MCHBAR_IMPH_Reserved_0_WID                       (20)
  #define REMAP_LIMIT_MCHBAR_IMPH_Reserved_0_MSK                       (0x000FFFFF)
  #define REMAP_LIMIT_MCHBAR_IMPH_Reserved_0_MIN                       (0)
  #define REMAP_LIMIT_MCHBAR_IMPH_Reserved_0_MAX                       (1048575) // 0x000FFFFF
  #define REMAP_LIMIT_MCHBAR_IMPH_Reserved_0_DEF                       (0x000FFFFF)
  #define REMAP_LIMIT_MCHBAR_IMPH_Reserved_0_HSH                       (0x54007260)

  #define REMAP_LIMIT_MCHBAR_IMPH_REMAPLMT_OFF                         (20)
  #define REMAP_LIMIT_MCHBAR_IMPH_REMAPLMT_WID                         (22)
  #define REMAP_LIMIT_MCHBAR_IMPH_REMAPLMT_MSK                         (0x000003FFFFF00000ULL)
  #define REMAP_LIMIT_MCHBAR_IMPH_REMAPLMT_MIN                         (0)
  #define REMAP_LIMIT_MCHBAR_IMPH_REMAPLMT_MAX                         (4194303) // 0x003FFFFF
  #define REMAP_LIMIT_MCHBAR_IMPH_REMAPLMT_DEF                         (0x00000000)
  #define REMAP_LIMIT_MCHBAR_IMPH_REMAPLMT_HSH                         (0x56287260)

  #define REMAP_LIMIT_MCHBAR_IMPH_Reserved_1_OFF                       (42)
  #define REMAP_LIMIT_MCHBAR_IMPH_Reserved_1_WID                       (22)
  #define REMAP_LIMIT_MCHBAR_IMPH_Reserved_1_MSK                       (0xFFFFFC0000000000ULL)
  #define REMAP_LIMIT_MCHBAR_IMPH_Reserved_1_MIN                       (0)
  #define REMAP_LIMIT_MCHBAR_IMPH_Reserved_1_MAX                       (4194303) // 0x003FFFFF
  #define REMAP_LIMIT_MCHBAR_IMPH_Reserved_1_DEF                       (0x00000000)
  #define REMAP_LIMIT_MCHBAR_IMPH_Reserved_1_HSH                       (0x56547260)

#define MEMORY_SLICE_HASH_MCHBAR_IMPH_REG                              (0x00007268)

  #define MEMORY_SLICE_HASH_MCHBAR_IMPH_MC_org_OFF                     ( 0)
  #define MEMORY_SLICE_HASH_MCHBAR_IMPH_MC_org_WID                     ( 4)
  #define MEMORY_SLICE_HASH_MCHBAR_IMPH_MC_org_MSK                     (0x0000000F)
  #define MEMORY_SLICE_HASH_MCHBAR_IMPH_MC_org_MIN                     (0)
  #define MEMORY_SLICE_HASH_MCHBAR_IMPH_MC_org_MAX                     (15) // 0x0000000F
  #define MEMORY_SLICE_HASH_MCHBAR_IMPH_MC_org_DEF                     (0x00000004)
  #define MEMORY_SLICE_HASH_MCHBAR_IMPH_MC_org_HSH                     (0x44007268)

  #define MEMORY_SLICE_HASH_MCHBAR_IMPH_Reserved_0_OFF                 ( 4)
  #define MEMORY_SLICE_HASH_MCHBAR_IMPH_Reserved_0_WID                 ( 2)
  #define MEMORY_SLICE_HASH_MCHBAR_IMPH_Reserved_0_MSK                 (0x00000030)
  #define MEMORY_SLICE_HASH_MCHBAR_IMPH_Reserved_0_MIN                 (0)
  #define MEMORY_SLICE_HASH_MCHBAR_IMPH_Reserved_0_MAX                 (3) // 0x00000003
  #define MEMORY_SLICE_HASH_MCHBAR_IMPH_Reserved_0_DEF                 (0x00000000)
  #define MEMORY_SLICE_HASH_MCHBAR_IMPH_Reserved_0_HSH                 (0x42087268)

  #define MEMORY_SLICE_HASH_MCHBAR_IMPH_HASH_MASK_OFF                  ( 6)
  #define MEMORY_SLICE_HASH_MCHBAR_IMPH_HASH_MASK_WID                  (14)
  #define MEMORY_SLICE_HASH_MCHBAR_IMPH_HASH_MASK_MSK                  (0x000FFFC0)
  #define MEMORY_SLICE_HASH_MCHBAR_IMPH_HASH_MASK_MIN                  (0)
  #define MEMORY_SLICE_HASH_MCHBAR_IMPH_HASH_MASK_MAX                  (16383) // 0x00003FFF
  #define MEMORY_SLICE_HASH_MCHBAR_IMPH_HASH_MASK_DEF                  (0x00000001)
  #define MEMORY_SLICE_HASH_MCHBAR_IMPH_HASH_MASK_HSH                  (0x4E0C7268)

  #define MEMORY_SLICE_HASH_MCHBAR_IMPH_Reserved_1_OFF                 (20)
  #define MEMORY_SLICE_HASH_MCHBAR_IMPH_Reserved_1_WID                 ( 4)
  #define MEMORY_SLICE_HASH_MCHBAR_IMPH_Reserved_1_MSK                 (0x00F00000)
  #define MEMORY_SLICE_HASH_MCHBAR_IMPH_Reserved_1_MIN                 (0)
  #define MEMORY_SLICE_HASH_MCHBAR_IMPH_Reserved_1_MAX                 (15) // 0x0000000F
  #define MEMORY_SLICE_HASH_MCHBAR_IMPH_Reserved_1_DEF                 (0x00000000)
  #define MEMORY_SLICE_HASH_MCHBAR_IMPH_Reserved_1_HSH                 (0x44287268)

  #define MEMORY_SLICE_HASH_MCHBAR_IMPH_HASH_LSB_MASK_BIT_OFF          (24)
  #define MEMORY_SLICE_HASH_MCHBAR_IMPH_HASH_LSB_MASK_BIT_WID          ( 3)
  #define MEMORY_SLICE_HASH_MCHBAR_IMPH_HASH_LSB_MASK_BIT_MSK          (0x07000000)
  #define MEMORY_SLICE_HASH_MCHBAR_IMPH_HASH_LSB_MASK_BIT_MIN          (0)
  #define MEMORY_SLICE_HASH_MCHBAR_IMPH_HASH_LSB_MASK_BIT_MAX          (7) // 0x00000007
  #define MEMORY_SLICE_HASH_MCHBAR_IMPH_HASH_LSB_MASK_BIT_DEF          (0x00000000)
  #define MEMORY_SLICE_HASH_MCHBAR_IMPH_HASH_LSB_MASK_BIT_HSH          (0x43307268)

  #define MEMORY_SLICE_HASH_MCHBAR_IMPH_Reserved_2_OFF                 (27)
  #define MEMORY_SLICE_HASH_MCHBAR_IMPH_Reserved_2_WID                 ( 1)
  #define MEMORY_SLICE_HASH_MCHBAR_IMPH_Reserved_2_MSK                 (0x08000000)
  #define MEMORY_SLICE_HASH_MCHBAR_IMPH_Reserved_2_MIN                 (0)
  #define MEMORY_SLICE_HASH_MCHBAR_IMPH_Reserved_2_MAX                 (1) // 0x00000001
  #define MEMORY_SLICE_HASH_MCHBAR_IMPH_Reserved_2_DEF                 (0x00000000)
  #define MEMORY_SLICE_HASH_MCHBAR_IMPH_Reserved_2_HSH                 (0x41367268)

  #define MEMORY_SLICE_HASH_MCHBAR_IMPH_L_shape_dec_mask_OFF           (28)
  #define MEMORY_SLICE_HASH_MCHBAR_IMPH_L_shape_dec_mask_WID           ( 4)
  #define MEMORY_SLICE_HASH_MCHBAR_IMPH_L_shape_dec_mask_MSK           (0xF0000000)
  #define MEMORY_SLICE_HASH_MCHBAR_IMPH_L_shape_dec_mask_MIN           (0)
  #define MEMORY_SLICE_HASH_MCHBAR_IMPH_L_shape_dec_mask_MAX           (15) // 0x0000000F
  #define MEMORY_SLICE_HASH_MCHBAR_IMPH_L_shape_dec_mask_DEF           (0x00000000)
  #define MEMORY_SLICE_HASH_MCHBAR_IMPH_L_shape_dec_mask_HSH           (0x44387268)

  #define MEMORY_SLICE_HASH_MCHBAR_IMPH_L_shape_boundary_OFF           (32)
  #define MEMORY_SLICE_HASH_MCHBAR_IMPH_L_shape_boundary_WID           ( 4)
  #define MEMORY_SLICE_HASH_MCHBAR_IMPH_L_shape_boundary_MSK           (0x0000000F00000000ULL)
  #define MEMORY_SLICE_HASH_MCHBAR_IMPH_L_shape_boundary_MIN           (0)
  #define MEMORY_SLICE_HASH_MCHBAR_IMPH_L_shape_boundary_MAX           (15) // 0x0000000F
  #define MEMORY_SLICE_HASH_MCHBAR_IMPH_L_shape_boundary_DEF           (0x00000000)
  #define MEMORY_SLICE_HASH_MCHBAR_IMPH_L_shape_boundary_HSH           (0x44407268)

  #define MEMORY_SLICE_HASH_MCHBAR_IMPH_Reserved_3_OFF                 (36)
  #define MEMORY_SLICE_HASH_MCHBAR_IMPH_Reserved_3_WID                 (28)
  #define MEMORY_SLICE_HASH_MCHBAR_IMPH_Reserved_3_MSK                 (0xFFFFFFF000000000ULL)
  #define MEMORY_SLICE_HASH_MCHBAR_IMPH_Reserved_3_MIN                 (0)
  #define MEMORY_SLICE_HASH_MCHBAR_IMPH_Reserved_3_MAX                 (268435455) // 0x0FFFFFFF
  #define MEMORY_SLICE_HASH_MCHBAR_IMPH_Reserved_3_DEF                 (0x00000000)
  #define MEMORY_SLICE_HASH_MCHBAR_IMPH_Reserved_3_HSH                 (0x5C487268)

#define MAD_SLICE_MCHBAR_IMPH_REG                                      (0x00007270)

  #define MAD_SLICE_MCHBAR_IMPH_Reserved_0_OFF                         ( 0)
  #define MAD_SLICE_MCHBAR_IMPH_Reserved_0_WID                         ( 4)
  #define MAD_SLICE_MCHBAR_IMPH_Reserved_0_MSK                         (0x0000000F)
  #define MAD_SLICE_MCHBAR_IMPH_Reserved_0_MIN                         (0)
  #define MAD_SLICE_MCHBAR_IMPH_Reserved_0_MAX                         (15) // 0x0000000F
  #define MAD_SLICE_MCHBAR_IMPH_Reserved_0_DEF                         (0x00000000)
  #define MAD_SLICE_MCHBAR_IMPH_Reserved_0_HSH                         (0x04007270)

  #define MAD_SLICE_MCHBAR_IMPH_MS_L_MAP_OFF                           ( 4)
  #define MAD_SLICE_MCHBAR_IMPH_MS_L_MAP_WID                           ( 1)
  #define MAD_SLICE_MCHBAR_IMPH_MS_L_MAP_MSK                           (0x00000010)
  #define MAD_SLICE_MCHBAR_IMPH_MS_L_MAP_MIN                           (0)
  #define MAD_SLICE_MCHBAR_IMPH_MS_L_MAP_MAX                           (1) // 0x00000001
  #define MAD_SLICE_MCHBAR_IMPH_MS_L_MAP_DEF                           (0x00000000)
  #define MAD_SLICE_MCHBAR_IMPH_MS_L_MAP_HSH                           (0x01087270)

  #define MAD_SLICE_MCHBAR_IMPH_Reserved_1_OFF                         ( 5)
  #define MAD_SLICE_MCHBAR_IMPH_Reserved_1_WID                         ( 3)
  #define MAD_SLICE_MCHBAR_IMPH_Reserved_1_MSK                         (0x000000E0)
  #define MAD_SLICE_MCHBAR_IMPH_Reserved_1_MIN                         (0)
  #define MAD_SLICE_MCHBAR_IMPH_Reserved_1_MAX                         (7) // 0x00000007
  #define MAD_SLICE_MCHBAR_IMPH_Reserved_1_DEF                         (0x00000000)
  #define MAD_SLICE_MCHBAR_IMPH_Reserved_1_HSH                         (0x030A7270)

  #define MAD_SLICE_MCHBAR_IMPH_STKD_MODE_OFF                          ( 8)
  #define MAD_SLICE_MCHBAR_IMPH_STKD_MODE_WID                          ( 1)
  #define MAD_SLICE_MCHBAR_IMPH_STKD_MODE_MSK                          (0x00000100)
  #define MAD_SLICE_MCHBAR_IMPH_STKD_MODE_MIN                          (0)
  #define MAD_SLICE_MCHBAR_IMPH_STKD_MODE_MAX                          (1) // 0x00000001
  #define MAD_SLICE_MCHBAR_IMPH_STKD_MODE_DEF                          (0x00000000)
  #define MAD_SLICE_MCHBAR_IMPH_STKD_MODE_HSH                          (0x01107270)

  #define MAD_SLICE_MCHBAR_IMPH_STKD_MODE_MS1_OFF                      ( 9)
  #define MAD_SLICE_MCHBAR_IMPH_STKD_MODE_MS1_WID                      ( 1)
  #define MAD_SLICE_MCHBAR_IMPH_STKD_MODE_MS1_MSK                      (0x00000200)
  #define MAD_SLICE_MCHBAR_IMPH_STKD_MODE_MS1_MIN                      (0)
  #define MAD_SLICE_MCHBAR_IMPH_STKD_MODE_MS1_MAX                      (1) // 0x00000001
  #define MAD_SLICE_MCHBAR_IMPH_STKD_MODE_MS1_DEF                      (0x00000000)
  #define MAD_SLICE_MCHBAR_IMPH_STKD_MODE_MS1_HSH                      (0x01127270)

  #define MAD_SLICE_MCHBAR_IMPH_Reserved_2_OFF                         (10)
  #define MAD_SLICE_MCHBAR_IMPH_Reserved_2_WID                         ( 2)
  #define MAD_SLICE_MCHBAR_IMPH_Reserved_2_MSK                         (0x00000C00)
  #define MAD_SLICE_MCHBAR_IMPH_Reserved_2_MIN                         (0)
  #define MAD_SLICE_MCHBAR_IMPH_Reserved_2_MAX                         (3) // 0x00000003
  #define MAD_SLICE_MCHBAR_IMPH_Reserved_2_DEF                         (0x00000000)
  #define MAD_SLICE_MCHBAR_IMPH_Reserved_2_HSH                         (0x02147270)

  #define MAD_SLICE_MCHBAR_IMPH_MS_S_SIZE_OFF                          (12)
  #define MAD_SLICE_MCHBAR_IMPH_MS_S_SIZE_WID                          (11)
  #define MAD_SLICE_MCHBAR_IMPH_MS_S_SIZE_MSK                          (0x007FF000)
  #define MAD_SLICE_MCHBAR_IMPH_MS_S_SIZE_MIN                          (0)
  #define MAD_SLICE_MCHBAR_IMPH_MS_S_SIZE_MAX                          (2047) // 0x000007FF
  #define MAD_SLICE_MCHBAR_IMPH_MS_S_SIZE_DEF                          (0x00000000)
  #define MAD_SLICE_MCHBAR_IMPH_MS_S_SIZE_HSH                          (0x0B187270)

  #define MAD_SLICE_MCHBAR_IMPH_Reserved_3_OFF                         (23)
  #define MAD_SLICE_MCHBAR_IMPH_Reserved_3_WID                         ( 1)
  #define MAD_SLICE_MCHBAR_IMPH_Reserved_3_MSK                         (0x00800000)
  #define MAD_SLICE_MCHBAR_IMPH_Reserved_3_MIN                         (0)
  #define MAD_SLICE_MCHBAR_IMPH_Reserved_3_MAX                         (1) // 0x00000001
  #define MAD_SLICE_MCHBAR_IMPH_Reserved_3_DEF                         (0x00000000)
  #define MAD_SLICE_MCHBAR_IMPH_Reserved_3_HSH                         (0x012E7270)

  #define MAD_SLICE_MCHBAR_IMPH_STKD_MODE_MS_BITS_OFF                  (24)
  #define MAD_SLICE_MCHBAR_IMPH_STKD_MODE_MS_BITS_WID                  ( 4)
  #define MAD_SLICE_MCHBAR_IMPH_STKD_MODE_MS_BITS_MSK                  (0x0F000000)
  #define MAD_SLICE_MCHBAR_IMPH_STKD_MODE_MS_BITS_MIN                  (0)
  #define MAD_SLICE_MCHBAR_IMPH_STKD_MODE_MS_BITS_MAX                  (15) // 0x0000000F
  #define MAD_SLICE_MCHBAR_IMPH_STKD_MODE_MS_BITS_DEF                  (0x00000000)
  #define MAD_SLICE_MCHBAR_IMPH_STKD_MODE_MS_BITS_HSH                  (0x04307270)

  #define MAD_SLICE_MCHBAR_IMPH_Reserved_4_OFF                         (28)
  #define MAD_SLICE_MCHBAR_IMPH_Reserved_4_WID                         ( 4)
  #define MAD_SLICE_MCHBAR_IMPH_Reserved_4_MSK                         (0xF0000000)
  #define MAD_SLICE_MCHBAR_IMPH_Reserved_4_MIN                         (0)
  #define MAD_SLICE_MCHBAR_IMPH_Reserved_4_MAX                         (15) // 0x0000000F
  #define MAD_SLICE_MCHBAR_IMPH_Reserved_4_DEF                         (0x00000000)
  #define MAD_SLICE_MCHBAR_IMPH_Reserved_4_HSH                         (0x04387270)

#define PMEM_1LM_MCHBAR_IMPH_REG                                       (0x00007278)

  #define PMEM_1LM_MCHBAR_IMPH_PersistentSpace_OFF                     ( 0)
  #define PMEM_1LM_MCHBAR_IMPH_PersistentSpace_WID                     (24)
  #define PMEM_1LM_MCHBAR_IMPH_PersistentSpace_MSK                     (0x00FFFFFF)
  #define PMEM_1LM_MCHBAR_IMPH_PersistentSpace_MIN                     (0)
  #define PMEM_1LM_MCHBAR_IMPH_PersistentSpace_MAX                     (16777215) // 0x00FFFFFF
  #define PMEM_1LM_MCHBAR_IMPH_PersistentSpace_DEF                     (0x00000001)
  #define PMEM_1LM_MCHBAR_IMPH_PersistentSpace_HSH                     (0x18007278)

  #define PMEM_1LM_MCHBAR_IMPH_reserved_OFF                            (24)
  #define PMEM_1LM_MCHBAR_IMPH_reserved_WID                            ( 7)
  #define PMEM_1LM_MCHBAR_IMPH_reserved_MSK                            (0x7F000000)
  #define PMEM_1LM_MCHBAR_IMPH_reserved_MIN                            (0)
  #define PMEM_1LM_MCHBAR_IMPH_reserved_MAX                            (127) // 0x0000007F
  #define PMEM_1LM_MCHBAR_IMPH_reserved_DEF                            (0x00000000)
  #define PMEM_1LM_MCHBAR_IMPH_reserved_HSH                            (0x07307278)

  #define PMEM_1LM_MCHBAR_IMPH_EN_1LM_PMEM_OFF                         (31)
  #define PMEM_1LM_MCHBAR_IMPH_EN_1LM_PMEM_WID                         ( 1)
  #define PMEM_1LM_MCHBAR_IMPH_EN_1LM_PMEM_MSK                         (0x80000000)
  #define PMEM_1LM_MCHBAR_IMPH_EN_1LM_PMEM_MIN                         (0)
  #define PMEM_1LM_MCHBAR_IMPH_EN_1LM_PMEM_MAX                         (1) // 0x00000001
  #define PMEM_1LM_MCHBAR_IMPH_EN_1LM_PMEM_DEF                         (0x00000000)
  #define PMEM_1LM_MCHBAR_IMPH_EN_1LM_PMEM_HSH                         (0x013E7278)

#define XTM_DEF_VTD1_ARB_IPC_IMPH_REG                                  (0x00007300)

  #define XTM_DEF_VTD1_ARB_IPC_IMPH_VC1PARKLIM_OFF                     ( 0)
  #define XTM_DEF_VTD1_ARB_IPC_IMPH_VC1PARKLIM_WID                     ( 3)
  #define XTM_DEF_VTD1_ARB_IPC_IMPH_VC1PARKLIM_MSK                     (0x00000007)
  #define XTM_DEF_VTD1_ARB_IPC_IMPH_VC1PARKLIM_MIN                     (0)
  #define XTM_DEF_VTD1_ARB_IPC_IMPH_VC1PARKLIM_MAX                     (7) // 0x00000007
  #define XTM_DEF_VTD1_ARB_IPC_IMPH_VC1PARKLIM_DEF                     (0x00000003)
  #define XTM_DEF_VTD1_ARB_IPC_IMPH_VC1PARKLIM_HSH                     (0x03007300)

  #define XTM_DEF_VTD1_ARB_IPC_IMPH_NON_VC1PARKLIM_OFF                 ( 4)
  #define XTM_DEF_VTD1_ARB_IPC_IMPH_NON_VC1PARKLIM_WID                 ( 3)
  #define XTM_DEF_VTD1_ARB_IPC_IMPH_NON_VC1PARKLIM_MSK                 (0x00000070)
  #define XTM_DEF_VTD1_ARB_IPC_IMPH_NON_VC1PARKLIM_MIN                 (0)
  #define XTM_DEF_VTD1_ARB_IPC_IMPH_NON_VC1PARKLIM_MAX                 (7) // 0x00000007
  #define XTM_DEF_VTD1_ARB_IPC_IMPH_NON_VC1PARKLIM_DEF                 (0x00000002)
  #define XTM_DEF_VTD1_ARB_IPC_IMPH_NON_VC1PARKLIM_HSH                 (0x03087300)

  #define XTM_DEF_VTD1_ARB_IPC_IMPH_VTDPARKCNTEN_OFF                   ( 8)
  #define XTM_DEF_VTD1_ARB_IPC_IMPH_VTDPARKCNTEN_WID                   ( 1)
  #define XTM_DEF_VTD1_ARB_IPC_IMPH_VTDPARKCNTEN_MSK                   (0x00000100)
  #define XTM_DEF_VTD1_ARB_IPC_IMPH_VTDPARKCNTEN_MIN                   (0)
  #define XTM_DEF_VTD1_ARB_IPC_IMPH_VTDPARKCNTEN_MAX                   (1) // 0x00000001
  #define XTM_DEF_VTD1_ARB_IPC_IMPH_VTDPARKCNTEN_DEF                   (0x00000000)
  #define XTM_DEF_VTD1_ARB_IPC_IMPH_VTDPARKCNTEN_HSH                   (0x01107300)

#define VTDTRKLCK_IMPH_REG                                             (0x000073FC)

  #define VTDTRKLCK_IMPH_LOCK_OFF                                      ( 0)
  #define VTDTRKLCK_IMPH_LOCK_WID                                      ( 1)
  #define VTDTRKLCK_IMPH_LOCK_MSK                                      (0x00000001)
  #define VTDTRKLCK_IMPH_LOCK_MIN                                      (0)
  #define VTDTRKLCK_IMPH_LOCK_MAX                                      (1) // 0x00000001
  #define VTDTRKLCK_IMPH_LOCK_DEF                                      (0x00000000)
  #define VTDTRKLCK_IMPH_LOCK_HSH                                      (0x010073FC)

#define CP_RTCMPLX_VTDBYPASS_IMPH_REG                                  (0x00007860)

  #define CP_RTCMPLX_VTDBYPASS_IMPH_SAI_MASK_OFF                       ( 0)
  #define CP_RTCMPLX_VTDBYPASS_IMPH_SAI_MASK_WID                       (64)
  #define CP_RTCMPLX_VTDBYPASS_IMPH_SAI_MASK_MSK                       (0xFFFFFFFFFFFFFFFFULL)
  #define CP_RTCMPLX_VTDBYPASS_IMPH_SAI_MASK_MIN                       (0)
  #define CP_RTCMPLX_VTDBYPASS_IMPH_SAI_MASK_MAX                       (18446744073709551615ULL) // 0xFFFFFFFFFFFFFFFF
  #define CP_RTCMPLX_VTDBYPASS_IMPH_SAI_MASK_DEF                       (0xC0910000001200)
  #define CP_RTCMPLX_VTDBYPASS_IMPH_SAI_MASK_HSH                       (0x40007860)

#define IPUVTDBAR_LOW_IMPH_REG                                         (0x00007880)

  #define IPUVTDBAR_LOW_IMPH_IPUVTD_EN_OFF                             ( 0)
  #define IPUVTDBAR_LOW_IMPH_IPUVTD_EN_WID                             ( 1)
  #define IPUVTDBAR_LOW_IMPH_IPUVTD_EN_MSK                             (0x00000001)
  #define IPUVTDBAR_LOW_IMPH_IPUVTD_EN_MIN                             (0)
  #define IPUVTDBAR_LOW_IMPH_IPUVTD_EN_MAX                             (1) // 0x00000001
  #define IPUVTDBAR_LOW_IMPH_IPUVTD_EN_DEF                             (0x00000000)
  #define IPUVTDBAR_LOW_IMPH_IPUVTD_EN_HSH                             (0x01007880)

  #define IPUVTDBAR_LOW_IMPH_RSVD_OFF                                  ( 1)
  #define IPUVTDBAR_LOW_IMPH_RSVD_WID                                  (11)
  #define IPUVTDBAR_LOW_IMPH_RSVD_MSK                                  (0x00000FFE)
  #define IPUVTDBAR_LOW_IMPH_RSVD_MIN                                  (0)
  #define IPUVTDBAR_LOW_IMPH_RSVD_MAX                                  (2047) // 0x000007FF
  #define IPUVTDBAR_LOW_IMPH_RSVD_DEF                                  (0x00000000)
  #define IPUVTDBAR_LOW_IMPH_RSVD_HSH                                  (0x0B027880)

  #define IPUVTDBAR_LOW_IMPH_IPUVTDBAR_LOW_OFF                         (12)
  #define IPUVTDBAR_LOW_IMPH_IPUVTDBAR_LOW_WID                         (20)
  #define IPUVTDBAR_LOW_IMPH_IPUVTDBAR_LOW_MSK                         (0xFFFFF000)
  #define IPUVTDBAR_LOW_IMPH_IPUVTDBAR_LOW_MIN                         (0)
  #define IPUVTDBAR_LOW_IMPH_IPUVTDBAR_LOW_MAX                         (1048575) // 0x000FFFFF
  #define IPUVTDBAR_LOW_IMPH_IPUVTDBAR_LOW_DEF                         (0x00000000)
  #define IPUVTDBAR_LOW_IMPH_IPUVTDBAR_LOW_HSH                         (0x14187880)

#define IPUVTDBAR_HIGH_IMPH_REG                                        (0x00007884)

  #define IPUVTDBAR_HIGH_IMPH_IPUVTDBAR_HIGH_OFF                       ( 0)
  #define IPUVTDBAR_HIGH_IMPH_IPUVTDBAR_HIGH_WID                       (10)
  #define IPUVTDBAR_HIGH_IMPH_IPUVTDBAR_HIGH_MSK                       (0x000003FF)
  #define IPUVTDBAR_HIGH_IMPH_IPUVTDBAR_HIGH_MIN                       (0)
  #define IPUVTDBAR_HIGH_IMPH_IPUVTDBAR_HIGH_MAX                       (1023) // 0x000003FF
  #define IPUVTDBAR_HIGH_IMPH_IPUVTDBAR_HIGH_DEF                       (0x00000000)
  #define IPUVTDBAR_HIGH_IMPH_IPUVTDBAR_HIGH_HSH                       (0x0A007884)

  #define IPUVTDBAR_HIGH_IMPH_RSVD_OFF                                 (10)
  #define IPUVTDBAR_HIGH_IMPH_RSVD_WID                                 (22)
  #define IPUVTDBAR_HIGH_IMPH_RSVD_MSK                                 (0xFFFFFC00)
  #define IPUVTDBAR_HIGH_IMPH_RSVD_MIN                                 (0)
  #define IPUVTDBAR_HIGH_IMPH_RSVD_MAX                                 (4194303) // 0x003FFFFF
  #define IPUVTDBAR_HIGH_IMPH_RSVD_DEF                                 (0x00000000)
  #define IPUVTDBAR_HIGH_IMPH_RSVD_HSH                                 (0x16147884)

#define PCIE0VTDBAR_LOW_IMPH_REG                                       (0x00007888)

  #define PCIE0VTDBAR_LOW_IMPH_PCIE_VTDBAREn_OFF                       ( 0)
  #define PCIE0VTDBAR_LOW_IMPH_PCIE_VTDBAREn_WID                       ( 1)
  #define PCIE0VTDBAR_LOW_IMPH_PCIE_VTDBAREn_MSK                       (0x00000001)
  #define PCIE0VTDBAR_LOW_IMPH_PCIE_VTDBAREn_MIN                       (0)
  #define PCIE0VTDBAR_LOW_IMPH_PCIE_VTDBAREn_MAX                       (1) // 0x00000001
  #define PCIE0VTDBAR_LOW_IMPH_PCIE_VTDBAREn_DEF                       (0x00000000)
  #define PCIE0VTDBAR_LOW_IMPH_PCIE_VTDBAREn_HSH                       (0x01007888)

  #define PCIE0VTDBAR_LOW_IMPH_PCIE_VTDBAR_LOW_OFF                     (12)
  #define PCIE0VTDBAR_LOW_IMPH_PCIE_VTDBAR_LOW_WID                     (20)
  #define PCIE0VTDBAR_LOW_IMPH_PCIE_VTDBAR_LOW_MSK                     (0xFFFFF000)
  #define PCIE0VTDBAR_LOW_IMPH_PCIE_VTDBAR_LOW_MIN                     (0)
  #define PCIE0VTDBAR_LOW_IMPH_PCIE_VTDBAR_LOW_MAX                     (1048575) // 0x000FFFFF
  #define PCIE0VTDBAR_LOW_IMPH_PCIE_VTDBAR_LOW_DEF                     (0x00000000)
  #define PCIE0VTDBAR_LOW_IMPH_PCIE_VTDBAR_LOW_HSH                     (0x14187888)

#define PCIE0VTDBAR_HIGH_IMPH_REG                                      (0x0000788C)

  #define PCIE0VTDBAR_HIGH_IMPH_PCIE_VTDBAR_HIGH_OFF                   ( 0)
  #define PCIE0VTDBAR_HIGH_IMPH_PCIE_VTDBAR_HIGH_WID                   (10)
  #define PCIE0VTDBAR_HIGH_IMPH_PCIE_VTDBAR_HIGH_MSK                   (0x000003FF)
  #define PCIE0VTDBAR_HIGH_IMPH_PCIE_VTDBAR_HIGH_MIN                   (0)
  #define PCIE0VTDBAR_HIGH_IMPH_PCIE_VTDBAR_HIGH_MAX                   (1023) // 0x000003FF
  #define PCIE0VTDBAR_HIGH_IMPH_PCIE_VTDBAR_HIGH_DEF                   (0x00000000)
  #define PCIE0VTDBAR_HIGH_IMPH_PCIE_VTDBAR_HIGH_HSH                   (0x0A00788C)

#define PCIE1VTDBAR_LOW_IMPH_REG                                       (0x00007890)
//Duplicate of PCIE0VTDBAR_LOW_IMPH_REG

#define PCIE1VTDBAR_HIGH_IMPH_REG                                      (0x00007894)
//Duplicate of PCIE0VTDBAR_HIGH_IMPH_REG

#define PCIE2VTDBAR_LOW_IMPH_REG                                       (0x00007898)
//Duplicate of PCIE0VTDBAR_LOW_IMPH_REG

#define PCIE2VTDBAR_HIGH_IMPH_REG                                      (0x0000789C)
//Duplicate of PCIE0VTDBAR_HIGH_IMPH_REG

#define PCIE3VTDBAR_LOW_IMPH_REG                                       (0x000078A0)
//Duplicate of PCIE0VTDBAR_LOW_IMPH_REG

#define PCIE3VTDBAR_HIGH_IMPH_REG                                      (0x000078A4)
//Duplicate of PCIE0VTDBAR_HIGH_IMPH_REG

#define PCIE4VTDBAR_LOW_IMPH_REG                                       (0x000078A8)
//Duplicate of PCIE0VTDBAR_LOW_IMPH_REG

#define PCIE4VTDBAR_HIGH_IMPH_REG                                      (0x000078AC)
//Duplicate of PCIE0VTDBAR_HIGH_IMPH_REG

#define PCIE5VTDBAR_LOW_IMPH_REG                                       (0x000078B0)
//Duplicate of PCIE0VTDBAR_LOW_IMPH_REG

#define PCIE5VTDBAR_HIGH_IMPH_REG                                      (0x000078B4)
//Duplicate of PCIE0VTDBAR_HIGH_IMPH_REG
#pragma pack(pop)
#endif
