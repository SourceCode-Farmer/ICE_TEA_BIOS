/** @file
  .

@copyright
  INTEL CONFIDENTIAL
  Copyright 2018 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains an 'Intel Peripheral Driver' and is uniquely identified as
  "Intel Reference Module" and is licensed for Intel CPUs and chipsets under
  the terms of your license agreement with Intel or your vendor. This file may
  be modified by the user, subject to additional terms of the license agreement.

@par Specification Reference:
**/
#ifndef _MrcChipApi_h_
#define _MrcChipApi_h_

#include "MrcDdrIoApi.h"

///
/// Defines and Macros
///
#define MAX_MR_GEN_FSM           (108)  // Maximum number of MRS FSM CONTROL MR Addresses that can be sent.
#define GEN_MRS_FSM_STORAGE_MAX  (60)   // 60 Storage registers
#define GEN_MRS_FSM_BYTE_MAX     (240)  // 60 Registers * 4 Bytes per register

// Separation required by hardware.
#define TX_FIFO_SEPARATION         (-2)

#define MAX_ADD_RANK_DELAY  (7)
#define MAX_ADD_DELAY       (7)
#define MAX_DEC_DELAY       (15)
#define GEN_MRS_FSM_TIMING_SET_MAX         (6)   // 2 registers, 3 settings each

///
/// Struct and Types
///

// Generic MRS FSM CommandTypes
// Based on MC0_CH0_CR_GENERIC_MRS_FSM_CONTROL_0_STRUCT.COMMAND_TYPE
// defintion:
// MRW  :  2'b00
// MPC  :  2'b01
// VREF :  2'b10
typedef enum {
  GmfCmdMrw  = 0,
  GmfCmdMpc  = 1,
  GmfCmdVref = 2
} GmfCmdType;

// Delay Storage Indexes available to Generic MRS FSM
typedef enum {
  GmfTimingIndex0,
  GmfTimingIndex1,
  GmfTimingIndex2,
  GmfTimingIndex3,
  GmfTimingIndex4,
  GmfTimingIndex5,
  GmfTimingIndexMax,
} GmfTimingIndex;

// Delay timing types for Generic MRS FSM
typedef enum {
  GmfDdr5Delay_tMOD    = GmfTimingIndex0,
  GmfDdr5Delay_tZQCAL  = GmfTimingIndex1,
  GmfDdr5Delay_tZQLAT  = GmfTimingIndex2,
  GmfDdr5Delay_tDFE    = GmfTimingIndex3,
  GmfDdr5Delay_tVREFCA = GmfTimingIndex4
} GmfDdr5DelayType;

// Delay timing types for Generic MRS FSM
typedef enum {
  GmfLpddr5Delay_tMRW    = GmfTimingIndex0,
  GmfLpddr5Delay_tODTUP  = GmfTimingIndex1,
  GmfLpddr5Delay_tVREFCA = GmfTimingIndex2,
  GmfLpddr5Delay_tFC     = GmfTimingIndex3,
  GmfLpddr5Delay_tVRCG   = GmfTimingIndex4,
  GmfLpddr5Delay_tMRD    = GmfTimingIndex5
} GmfLpddr5DelayType;

typedef struct {
  GmfTimingIndex DelayIndex;  ///< The unique delay index
  UINT8          MrAddr;
  UINT8          MrData;
  BOOLEAN        Valid;
  BOOLEAN        FspWrToggle;     ///< Defines that this MR write should toggle the FSP state for WR
  BOOLEAN        FspOpToggle;     ///< Defines that this MR write should toggle the FSP state for OP
  BOOLEAN        FreqSwitchPoint; ///< Defines that after this MR, we should execute the Frequency switch.
  BOOLEAN        PdaMr;
  GmfCmdType     CmdType;   ///< Defines the type of command to output. The default (0) is MRW
} MRC_GEN_MRS_FSM_MR_TYPE;

typedef union {
  struct {
    UINT32 MA    : 14; // Bits 13:0
    UINT32 ACT_n : 1;  // Bit 14
    UINT32 WE_n  : 1;  // Bit 15
    UINT32 CAS_n : 1;  // Bit 16
    UINT32 RAS_n : 1;  // Bit 17
    UINT32 MA17  : 1;  // Bit 18
    UINT32 BA    : 2;  // Bits 20:19
    UINT32 BG    : 2;  // Bits 22:21
    UINT32       : 5;  // Bits 27:23
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} Ddr4CaBus;

typedef union {
  struct {
    UINT32 CA_0 : 6;  // Bits 5:0
    UINT32 CA_1 : 6;  // Bit 11:6
    UINT32 CA_2 : 6;  // Bit 17:12
    UINT32 CA_3 : 6;  // Bit 23:18
  } Bits;
  UINT32 Data;
} LpDdr4Command;

typedef union {
  struct {
    UINT32 CA_0 : 14;  // Bits 13:0
    UINT32 CA_1 : 14;  // Bits 27:14
  } Bits;
  UINT32 Data;
} Ddr5Command;

typedef union {
  struct {
    UINT32 CA_0 : 7;  // Bits 6:0
    UINT32 CA_1 : 7;  // Bit 13:7
    UINT32 CA_2 : 7;  // Bit 20:14
    UINT32 CA_3 : 7;  // Bit 27:21
  } Bits;
  UINT32 Data;
} LpDdr5Command;

/**
  This function configures the Generic MRS FSM shadow registers based on the MrData inputs.
  It will determine if it needs to use the per-rank feature if the MR value differs across ranks.

  @param[in] MrcData - Pointer to MRC global data.
  @param[in] MrData  - Pointer to an array of MR data to configure the MRS FSM with.
  @param[out] MrPerRank - Optional mrEndOfSequence terminated array specifying MR addresses that must
                          be sent per-rank, all other registers use the same data for all ranks on the
                          same channel. If this pointer is NULL, then only MRs with different values in
                          each rank are configued as per-rank Generic MRS FSM entries.

  @retval mrcSuccess if successful.
  @retval mrcFail if MrData pointer is null, the timing or per-rank registers are out of free entries.
**/
MrcStatus
MrcGenMrsFsmConfig (
  IN  MrcParameters *MrcData,
  IN  MRC_GEN_MRS_FSM_MR_TYPE MrData[MAX_CONTROLLER][MAX_CHANNEL][MAX_RANK_IN_CHANNEL][MAX_MR_GEN_FSM],
  IN  const MrcModeRegister   *MrPerRank OPTIONAL
  );

/**
  This function executes the MRS FSM and waits for the FSM to complete.
  If the FSM does not complete after 10 seconds, it will return an error message.

  @param[in] MrcData - Pointer to MRC global data.

  @retval mrcFail if the FSM is not idle.
  @retval mrcSuccess otherwise.
**/
MrcStatus
MrcGenMrsFsmRun (
  IN  MrcParameters *MrcData
  );

/**
  This function cleans the MRS FSM valid control on all Controllers Channels.

  @param[in] MrcData - Pointer to MRC global data.
  @param[in] MrData  - Pointer to an array of MR data to configure the MRS FSM with.
  @param[in] CleanAll - If set to TRUE, MrData values will be ignored and all Control Registers will be cleared

  @retval mrcFail if clean failed.
  @retval mrcSuccess otherwise.
**/
MrcStatus
MrcGenMrsFsmClean (
  IN  MrcParameters *MrcData,
  IN  MRC_GEN_MRS_FSM_MR_TYPE MrData[MAX_CONTROLLER][MAX_CHANNEL][MAX_RANK_IN_CHANNEL][MAX_MR_GEN_FSM],
  IN  BOOLEAN       CleanAll
  );

/**
Executes DDR4 generic MRH command

  @param[in] MrcData      - Pointer to global data
  @param[in] Controller   - Targeted controller
  @param[in] Channel      - Targeted channel
  @param[in] Rank         - Targeted rank
  @param[in] Ca_Bus       - Command bus instruction

  @retval mrcStatus
**/
MrcStatus
MrcDdr4RunGenericMrh (
  IN MrcParameters *const MrcData,
  IN UINT32               Controller,
  IN UINT32               Channel,
  IN UINT32               Rank,
  IN Ddr4CaBus     *const Ca_Bus
  );

/**
Executes LPDDR4 generic MRH command

  @param[in] MrcData      - Pointer to global data
  @param[in] Controller   - Targeted controller
  @param[in] Channel      - Targeted channel
  @param[in] Rank         - Targeted rank
  @param[in] Ca_Bus       - Command bus instruction
  @param[in] TwoCycleCommand - Indicates whether MRH command requires two cycles

  @retval mrcStatus
**/
MrcStatus
MrcRunGenericMrh (
  IN MrcParameters *const MrcData,
  IN UINT32               Controller,
  IN UINT32               Channel,
  IN UINT32               Rank,
  IN UINT32 const         Ca_Bus,
  IN BOOLEAN              TwoCycleCommand
  );

UINT32
MrcQclkToTck (
  IN  MrcParameters *const MrcData,
  IN  UINT32               Qclk
  );

UINT32
MrcTckToQclk (
  IN  MrcParameters *const MrcData,
  IN  UINT32               Tck
  );

/**
  Program MC/DDRIO registers to Gear1 / Gear2 / Gear4 mode.
  This only includes Gear mode enable/disable, not other registers that are impacted by gear mode.
  The current Gear mode is set based on Outputs->Gear2 and Outputs->Gear4.

  @param[in] MrcData - The MRC general data.
**/
VOID
MrcSetGear (
  IN MrcParameters *const MrcData
  );

/**
  Programming of CCC_CR_DDRCRCCCCLKCONTROLS_BlockTrainRst

  @param[in] MrcData - The MRC global data.
  @param[in] BlockTrainReset - TRUE to BlockTrainReset for most training algos.  FALSE for specific training algos that need PiDivider sync.

**/
VOID
MrcBlockTrainResetToggle (
  IN MrcParameters *const MrcData,
  IN BOOLEAN              BlockTrainReset
  );

/**
  This function configures the DDRCRCMDBUSTRAIN register to values for normal mode.

  @param[in]  MrcData - Pointer to global MRC data.

  @retval none.
**/
VOID
MrcSetWritePreamble (
  IN  MrcParameters *const  MrcData
  );

/**
  Returns the currently configured DRAM Command Intput Rate NMode
  Note: In DDR4 3N is used during training (in Gear1), but this
  function won't report this.

  @param[in] MrcData    - Include all MRC global data.

  @retval 1 = 1N Mode
  @retval 2 = 2N Mode
**/
UINT32
MrcGetNMode (
  IN MrcParameters *const MrcData
  );

/**
  Configure the MC to issue multicycle CS_n MPC commands.
  The DRAM must be configured separately by either setting
  DDR5 MR2.OP[4] = 0 or by resetting the DRAM.

  @param[in] MrcData    - Include all MRC global data.

  @retval None
**/
void
EnableMcMulticycleCs (
  IN MrcParameters *const MrcData
  );

/**
  Configure the MC and DRAM for single cycle CS_n MPC commands.
  An MRW is issued to the DRAM to configure DDR5 MR2[4] = 1.

  @param[in] MrcData    - Include all MRC global data.
  @param[in] McOnly     - TRUE: program MC side only; FALSE: program both MC and DRAM.

  @retval None
**/
void
DisableMcMulticycleCs (
  IN MrcParameters *const MrcData,
  IN const BOOLEAN        McOnly
  );

/**
  Read the first RCOMPDATA0 Rcomp register on the first available MC channel.

  @param[in] MrcData      - Pointer to MRC global data.

  @retval UINT32 - The data read from RCOMPDATA0 on the first available MC channel.
**/
UINT32
ReadFirstRcompReg (
  IN OUT MrcParameters *const MrcData
  );

/**
  This function returns the afe2dig_dqrx_dq*_rxumdataunflopped fields from AFEMISCCONTROL0 register
  for the specified Controller, Channel and Byte. All 8 DQ fields are combined and returned together.

  @param[in] MrcData    - Pointer to MRC global data.
  @param[in] Controller - Zero based memory controller number
  @param[in] Channel    - Zero based memory channel number
  @param[in] Byte       - Zero based memory byte number

  @retval afe2dig_dqrx_dq[0...7]_rxumdataunflopped
**/
UINT8
ReadAfeMiscControl0Reg (
  IN MrcParameters *const MrcData,
  IN UINT32               Controller,
  IN UINT32               Channel,
  IN UINT32               Byte
  );

/**
  Find the maximum possible cycle increment and decrement values for write leveling trainings

  @param[in] MrcData - Pointer to MRC global data.
  @param[out] MaxAdd  - Max possible increment value for write leveling trainings
  @param[out] MaxDec  - Max possible decrement value for write leveling trainings

  @retval None
**/
MrcStatus
MrcMcTxCycleLimits (
  IN MrcParameters *const MrcData,
  OUT UINT32              *MaxAdd,
  OUT UINT32              *MaxDec
  );

/**
  Find the minimum and maximum PI setting across Tx DQ/DQS on a given Rank, on all channels.
  Determine how far we can use the PI value to shift the Cycle. Min value will use DQS,
  Max value will use DQ (DQ is DQS + 32 (gear1) or DQS + 96 (gear2), and also plus tDQS2DQ for LP4/LP5).

  @param[in]  MrcData         - Pointer to MRC global data.
  @param[in]  Rank            - Current working rank
  @param[in]  MaxAdd          - Max possible increment value for write leveling trainings
  @param[in]  MaxDec          - Max possible decrement value for write leveling trainings
  @param[out] StartOffset     - Starting offset
  @param[out] EndOffset       - End of limit
  @param[out] SavedTxDqsVal   - Pointer to array where current Tx Dqs timings will be stored

  @retval MrcStatus - If it succeeded, return mrcSuccess
**/

MrcStatus
MrcIoTxLimits (
  IN  MrcParameters *const MrcData,
  IN  UINT32               Rank,
  IN  UINT32               MaxAdd,
  IN  UINT32               MaxDec,
  OUT INT32               *StartOffset,
  OUT INT32               *EndOffset,
  OUT UINT16              SavedTxDqsVal[MAX_CONTROLLER][MAX_CHANNEL][MAX_SDRAM_IN_DIMM]
  );

/**
  Programs new delay offsets to Tx DQ/DQS timing

  @param[in]  MrcData       - Pointer to MRC global data.
  @param[in]  Rank          - Current working rank
  @param[in]  MaxAdd        - Max possible increment value for write leveling trainings
  @param[in]  MaxDec        - Max possible decrement value for write leveling trainings
  @param[in]  UiLoop        - The current UiLoop used in loopback mode
  @param[in]  SavedTxDqsVal - array of the initial TX DQS Delays
  @param[in]  TxDelayOffset - Delay to be programmed

  @retval @retval mrcSuccess or mrcTimingError if minimum TX fifo separation is not met
**/

MrcStatus
SetWriteCycleDelay (
  IN  MrcParameters *const  MrcData,
  IN  UINT32                Rank,
  IN  UINT32                MaxAdd,
  IN  UINT32                MaxDec,
  IN  UINT32                UiLoop,
  IN  UINT16                SavedTxDqsVal[MAX_CONTROLLER][MAX_CHANNEL][MAX_SDRAM_IN_DIMM],
  IN  INT32                 TxDelayOffset
  );

/**
  Programming of PM_CONTROL_Dclk_en_reset_bgf_run
  BGF is disabled when Dclk_enable is cleared
  If PM_CONTROL_Dclk_en_reset_bgf_run bit is set to 0, BGF will continue to run even if dclk_enable is cleared

  @param[in] MrcData -    The MRC global data.
  @param[in] SetBgfRun  - FALSE to Keep BgfRun Enabled when EnableDclk is de-asserted.
                          TRUE  to program  PM_CONTROL_Dclk_en_reset_bgf_run = 1 (Negative Logic).

**/
VOID
MrcToggleBgfRun (
  IN MrcParameters *const MrcData,
  IN BOOLEAN              SetBgfRun
);

/**
  Force comp codes distribution

  @param[in, out] MrcData - Include all MRC global data.
**/
VOID
MrcForceDistCompCodes (
  IN OUT MrcParameters *const MrcData
  );

/*
  Save frequency calibration values during LP frequency switch.

  @param[in, out] MrcData - Include all MRC global data.
  @retval MrcStatus       - mrcSuccess if successful, else an error status
*/
MrcStatus
MrcLpFreqCalibrationSave(
  IN OUT MrcParameters *const MrcData
);

/*
  Configures and restores frequency calibration values during LP frequency switch.

  @param[in, out] MrcData - Include all MRC global data.
  @retval MrcStatus       - mrcSuccess if successful, else an error status
*/
MrcStatus
MrcLpFreqCalibrationRestore(
  IN OUT MrcParameters *const MrcData
);

#endif // _MrcChipApi_h_
