#ifndef __MrcMcRegisterAdl1xxx_h__
#define __MrcMcRegisterAdl1xxx_h__
/** @file
  This file was automatically generated. Modify at your own risk.
  Note that no error checking is done in these functions so ensure that the correct values are passed.

@copyright
  Copyright (c) 2010 - 2021 Intel Corporation. All rights reserved
  This software and associated documentation (if any) is furnished
  under a license and may only be used or copied in accordance
  with the terms of the license. Except as permitted by the
  license, no part of this software or documentation may be
  reproduced, stored in a retrieval system, or transmitted in any
  form or by any means without the express written consent of
  Intel Corporation.
  This file contains an 'Intel Peripheral Driver' and is uniquely
  identified as "Intel Reference Module" and is licensed for Intel
  CPUs and chipsets under the terms of your license agreement with
  Intel or your vendor. This file may be modified by the user, subject
  to additional terms of the license agreement.

@par Specification Reference:
**/

#pragma pack(push, 1)


#define DATA4CH0_CR_DDRDATADQRANK0LANE0_REG                            (0x00001000)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA4CH0_CR_DDRDATADQRANK0LANE1_REG                            (0x00001004)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA4CH0_CR_DDRDATADQRANK0LANE2_REG                            (0x00001008)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA4CH0_CR_DDRDATADQRANK0LANE3_REG                            (0x0000100C)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA4CH0_CR_DDRDATADQRANK0LANE4_REG                            (0x00001010)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA4CH0_CR_DDRDATADQRANK0LANE5_REG                            (0x00001014)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA4CH0_CR_DDRDATADQRANK0LANE6_REG                            (0x00001018)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA4CH0_CR_DDRDATADQRANK0LANE7_REG                            (0x0000101C)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA4CH0_CR_DDRDATADQRANK1LANE0_REG                            (0x00001020)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA4CH0_CR_DDRDATADQRANK1LANE1_REG                            (0x00001024)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA4CH0_CR_DDRDATADQRANK1LANE2_REG                            (0x00001028)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA4CH0_CR_DDRDATADQRANK1LANE3_REG                            (0x0000102C)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA4CH0_CR_DDRDATADQRANK1LANE4_REG                            (0x00001030)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA4CH0_CR_DDRDATADQRANK1LANE5_REG                            (0x00001034)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA4CH0_CR_DDRDATADQRANK1LANE6_REG                            (0x00001038)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA4CH0_CR_DDRDATADQRANK1LANE7_REG                            (0x0000103C)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA4CH0_CR_DDRDATADQRANK2LANE0_REG                            (0x00001040)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA4CH0_CR_DDRDATADQRANK2LANE1_REG                            (0x00001044)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA4CH0_CR_DDRDATADQRANK2LANE2_REG                            (0x00001048)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA4CH0_CR_DDRDATADQRANK2LANE3_REG                            (0x0000104C)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA4CH0_CR_DDRDATADQRANK2LANE4_REG                            (0x00001050)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA4CH0_CR_DDRDATADQRANK2LANE5_REG                            (0x00001054)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA4CH0_CR_DDRDATADQRANK2LANE6_REG                            (0x00001058)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA4CH0_CR_DDRDATADQRANK2LANE7_REG                            (0x0000105C)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA4CH0_CR_DDRDATADQRANK3LANE0_REG                            (0x00001060)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA4CH0_CR_DDRDATADQRANK3LANE1_REG                            (0x00001064)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA4CH0_CR_DDRDATADQRANK3LANE2_REG                            (0x00001068)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA4CH0_CR_DDRDATADQRANK3LANE3_REG                            (0x0000106C)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA4CH0_CR_DDRDATADQRANK3LANE4_REG                            (0x00001070)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA4CH0_CR_DDRDATADQRANK3LANE5_REG                            (0x00001074)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA4CH0_CR_DDRDATADQRANK3LANE6_REG                            (0x00001078)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA4CH0_CR_DDRDATADQRANK3LANE7_REG                            (0x0000107C)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA4CH0_CR_DDRCRDATACONTROL0_REG                              (0x00001080)
//Duplicate of DATA0CH0_CR_DDRCRDATACONTROL0_REG

#define DATA4CH0_CR_DDRCRDATACONTROL1_REG                              (0x00001084)
//Duplicate of DATA0CH0_CR_DDRCRDATACONTROL1_REG

#define DATA4CH0_CR_DDRCRDATACONTROL2_REG                              (0x00001088)
//Duplicate of DATA0CH0_CR_DDRCRDATACONTROL2_REG

#define DATA4CH0_CR_DDRCRDATACONTROL3_REG                              (0x0000108C)
//Duplicate of DATA0CH0_CR_DDRCRDATACONTROL3_REG

#define DATA4CH0_CR_AFEMISC_REG                                        (0x00001090)
//Duplicate of DATA0CH0_CR_AFEMISC_REG

#define DATA4CH0_CR_SRZCTL_REG                                         (0x00001094)
//Duplicate of DATA0CH0_CR_SRZCTL_REG

#define DATA4CH0_CR_DQRXCTL0_REG                                       (0x00001098)
//Duplicate of DATA0CH0_CR_DQRXCTL0_REG

#define DATA4CH0_CR_DQRXCTL1_REG                                       (0x0000109C)
//Duplicate of DATA0CH0_CR_DQRXCTL1_REG

#define DATA4CH0_CR_DQSTXRXCTL_REG                                     (0x000010A0)
//Duplicate of DATA0CH0_CR_DQSTXRXCTL_REG

#define DATA4CH0_CR_DQSTXRXCTL0_REG                                    (0x000010A4)
//Duplicate of DATA0CH0_CR_DQSTXRXCTL0_REG

#define DATA4CH0_CR_SDLCTL1_REG                                        (0x000010A8)
//Duplicate of DATA0CH0_CR_SDLCTL1_REG

#define DATA4CH0_CR_SDLCTL0_REG                                        (0x000010AC)
//Duplicate of DATA0CH0_CR_SDLCTL0_REG

#define DATA4CH0_CR_MDLLCTL0_REG                                       (0x000010B0)
//Duplicate of DATA0CH0_CR_MDLLCTL0_REG

#define DATA4CH0_CR_MDLLCTL1_REG                                       (0x000010B4)
//Duplicate of DATA0CH0_CR_MDLLCTL1_REG

#define DATA4CH0_CR_MDLLCTL2_REG                                       (0x000010B8)
//Duplicate of DATA0CH0_CR_MDLLCTL2_REG

#define DATA4CH0_CR_MDLLCTL3_REG                                       (0x000010BC)
//Duplicate of DATA0CH0_CR_MDLLCTL3_REG

#define DATA4CH0_CR_MDLLCTL4_REG                                       (0x000010C0)
//Duplicate of DATA0CH0_CR_MDLLCTL4_REG

#define DATA4CH0_CR_MDLLCTL5_REG                                       (0x000010C4)
//Duplicate of DATA0CH0_CR_MDLLCTL4_REG

#define DATA4CH0_CR_DIGMISCCTRL_REG                                    (0x000010C4)
//Duplicate of DATA0CH0_CR_DIGMISCCTRL_REG

#define DATA4CH0_CR_AFEMISCCTRL2_REG                                   (0x000010C8)
//Duplicate of DATA0CH0_CR_AFEMISCCTRL2_REG

#define DATA4CH0_CR_TXPBDOFFSET0_REG                                   (0x000010CC)
//Duplicate of DATA0CH0_CR_TXPBDOFFSET0_REG

#define DATA4CH0_CR_TXPBDOFFSET1_REG                                   (0x000010D0)
//Duplicate of DATA0CH0_CR_TXPBDOFFSET1_REG

#define DATA4CH0_CR_COMPCTL0_REG                                       (0x000010D4)
//Duplicate of DATA0CH0_CR_COMPCTL0_REG

#define DATA4CH0_CR_COMPCTL1_REG                                       (0x000010D8)
//Duplicate of DATA0CH0_CR_COMPCTL1_REG

#define DATA4CH0_CR_COMPCTL2_REG                                       (0x000010DC)
//Duplicate of DATA0CH0_CR_COMPCTL2_REG

#define DATA4CH0_CR_DCCCTL5_REG                                        (0x000010E0)
//Duplicate of DATA0CH0_CR_DCCCTL5_REG

#define DATA4CH0_CR_DCCCTL6_REG                                        (0x000010E4)
//Duplicate of DATA0CH0_CR_DCCCTL6_REG

#define DATA4CH0_CR_DCCCTL7_REG                                        (0x000010E8)
//Duplicate of DATA0CH0_CR_DCCCTL7_REG

#define DATA4CH0_CR_DCCCTL8_REG                                        (0x000010EC)
//Duplicate of DATA0CH0_CR_DCCCTL8_REG

#define DATA4CH0_CR_DCCCTL9_REG                                        (0x000010F0)
//Duplicate of DATA0CH0_CR_DCCCTL9_REG

#define DATA4CH0_CR_RXCONTROL0RANK0_REG                                (0x000010F4)
//Duplicate of DATA0CH0_CR_RXCONTROL0RANK0_REG

#define DATA4CH0_CR_RXCONTROL0RANK1_REG                                (0x000010F8)
//Duplicate of DATA0CH0_CR_RXCONTROL0RANK0_REG

#define DATA4CH0_CR_RXCONTROL0RANK2_REG                                (0x000010FC)
//Duplicate of DATA0CH0_CR_RXCONTROL0RANK0_REG

#define DATA4CH0_CR_RXCONTROL0RANK3_REG                                (0x00001100)
//Duplicate of DATA0CH0_CR_RXCONTROL0RANK0_REG

#define DATA4CH0_CR_TXCONTROL0_PH90_REG                                (0x00001104)
//Duplicate of DATA0CH0_CR_TXCONTROL0_PH90_REG

#define DATA4CH0_CR_TXCONTROL0RANK0_REG                                (0x00001108)
//Duplicate of DATA0CH0_CR_TXCONTROL0RANK0_REG

#define DATA4CH0_CR_TXCONTROL0RANK1_REG                                (0x0000110C)
//Duplicate of DATA0CH0_CR_TXCONTROL0RANK0_REG

#define DATA4CH0_CR_TXCONTROL0RANK2_REG                                (0x00001110)
//Duplicate of DATA0CH0_CR_TXCONTROL0RANK0_REG

#define DATA4CH0_CR_TXCONTROL0RANK3_REG                                (0x00001114)
//Duplicate of DATA0CH0_CR_TXCONTROL0RANK0_REG

#define DATA4CH0_CR_DDRDATADQSRANKX_REG                                (0x00001118)
//Duplicate of DATA0CH0_CR_DDRDATADQSRANKX_REG

#define DATA4CH0_CR_DDRDATADQSPH90RANKX_REG                            (0x0000111C)
//Duplicate of DATA0CH0_CR_DDRDATADQSPH90RANKX_REG

#define DATA4CH0_CR_DDRCRDATAOFFSETCOMP_REG                            (0x00001120)
//Duplicate of DATA0CH0_CR_DDRCRDATAOFFSETCOMP_REG

#define DATA4CH0_CR_DDRCRDATAOFFSETTRAIN_REG                           (0x00001124)
//Duplicate of DATA0CH0_CR_DDRCRDATAOFFSETTRAIN_REG

#define DATA4CH0_CR_DDRCRCMDBUSTRAIN_REG                               (0x00001128)
//Duplicate of DATA0CH0_CR_DDRCRCMDBUSTRAIN_REG

#define DATA4CH0_CR_DDRCRWRRETRAINSWIZZLECONTROL_REG                   (0x0000112C)
//Duplicate of DATA0CH0_CR_DDRCRWRRETRAINSWIZZLECONTROL_REG

#define DATA4CH0_CR_DDRCRWRRETRAINRANK3_REG                            (0x00001130)
//Duplicate of DATA0CH0_CR_DDRCRWRRETRAINRANK3_REG

#define DATA4CH0_CR_DDRCRWRRETRAINRANK2_REG                            (0x00001134)
//Duplicate of DATA0CH0_CR_DDRCRWRRETRAINRANK3_REG

#define DATA4CH0_CR_DDRCRWRRETRAINRANK1_REG                            (0x00001138)
//Duplicate of DATA0CH0_CR_DDRCRWRRETRAINRANK3_REG

#define DATA4CH0_CR_DDRCRWRRETRAINRANK0_REG                            (0x0000113C)
//Duplicate of DATA0CH0_CR_DDRCRWRRETRAINRANK3_REG

#define DATA4CH0_CR_DDRCRWRRETRAINCONTROLSTATUS_REG                    (0x00001140)
//Duplicate of DATA0CH0_CR_DDRCRWRRETRAINCONTROLSTATUS_REG

#define DATA4CH0_CR_TXCONTROL1_PH90_REG                                (0x00001144)
//Duplicate of DATA0CH0_CR_TXCONTROL1_PH90_REG

#define DATA4CH0_CR_AFEMISCCTRL1_REG                                   (0x00001148)
//Duplicate of DATA0CH0_CR_AFEMISCCTRL1_REG

#define DATA4CH0_CR_DQXRXAMPCTL_REG                                    (0x0000114C)
//Duplicate of DATA0CH0_CR_DQXRXAMPCTL_REG

#define DATA4CH0_CR_DQ0RXAMPCTL_REG                                    (0x00001150)
//Duplicate of DATA0CH0_CR_DQ0RXAMPCTL_REG

#define DATA4CH0_CR_DQ1RXAMPCTL_REG                                    (0x00001154)
//Duplicate of DATA0CH0_CR_DQ1RXAMPCTL_REG

#define DATA4CH0_CR_DQ2RXAMPCTL_REG                                    (0x00001158)
//Duplicate of DATA0CH0_CR_DQ2RXAMPCTL_REG

#define DATA4CH0_CR_DQ3RXAMPCTL_REG                                    (0x0000115C)
//Duplicate of DATA0CH0_CR_DQ3RXAMPCTL_REG

#define DATA4CH0_CR_DQ4RXAMPCTL_REG                                    (0x00001160)
//Duplicate of DATA0CH0_CR_DQ4RXAMPCTL_REG

#define DATA4CH0_CR_DQ5RXAMPCTL_REG                                    (0x00001164)
//Duplicate of DATA0CH0_CR_DQ5RXAMPCTL_REG

#define DATA4CH0_CR_DQ6RXAMPCTL_REG                                    (0x00001168)
//Duplicate of DATA0CH0_CR_DQ6RXAMPCTL_REG

#define DATA4CH0_CR_DQ7RXAMPCTL_REG                                    (0x0000116C)
//Duplicate of DATA0CH0_CR_DQ7RXAMPCTL_REG

#define DATA4CH0_CR_DQRXAMPCTL0_REG                                    (0x00001170)
//Duplicate of DATA0CH0_CR_DQRXAMPCTL0_REG

#define DATA4CH0_CR_DQRXAMPCTL1_REG                                    (0x00001174)
//Duplicate of DATA0CH0_CR_DQRXAMPCTL1_REG

#define DATA4CH0_CR_DQRXAMPCTL2_REG                                    (0x00001178)
//Duplicate of DATA0CH0_CR_DQRXAMPCTL2_REG

#define DATA4CH0_CR_DQTXCTL0_REG                                       (0x0000117C)
//Duplicate of DATA0CH0_CR_DQTXCTL0_REG

#define DATA4CH0_CR_DCCCTL0_REG                                        (0x00001180)
//Duplicate of DATA0CH0_CR_DCCCTL0_REG

#define DATA4CH0_CR_DCCCTL1_REG                                        (0x00001184)
//Duplicate of DATA0CH0_CR_DCCCTL1_REG

#define DATA4CH0_CR_DCCCTL2_REG                                        (0x00001188)
//Duplicate of DATA0CH0_CR_DCCCTL2_REG

#define DATA4CH0_CR_DCCCTL3_REG                                        (0x0000118C)
//Duplicate of DATA0CH0_CR_DCCCTL3_REG

#define DATA4CH0_CR_DCCCTL10_REG                                       (0x00001190)
//Duplicate of DATA0CH0_CR_DCCCTL10_REG

#define DATA4CH0_CR_DCCCTL12_REG                                       (0x00001194)
//Duplicate of DATA0CH0_CR_DCCCTL12_REG

#define DATA4CH0_CR_CLKALIGNCTL0_REG                                   (0x00001198)
//Duplicate of DATA0CH0_CR_CLKALIGNCTL0_REG

#define DATA4CH0_CR_CLKALIGNCTL1_REG                                   (0x0000119C)
//Duplicate of DATA0CH0_CR_CLKALIGNCTL1_REG

#define DATA4CH0_CR_CLKALIGNCTL2_REG                                   (0x000011A0)
//Duplicate of DATA0CH0_CR_CLKALIGNCTL2_REG

#define DATA4CH0_CR_DCCCTL4_REG                                        (0x000011A4)
//Duplicate of DATA0CH0_CR_DCCCTL4_REG

#define DATA4CH0_CR_DDRCRDATACOMPVTT_REG                               (0x000011A8)
//Duplicate of DATA0CH0_CR_DDRCRDATACOMPVTT_REG

#define DATA4CH0_CR_WLCTRL_REG                                         (0x000011AC)
//Duplicate of DATA0CH0_CR_WLCTRL_REG

#define DATA4CH0_CR_LPMODECTL_REG                                      (0x000011B0)
//Duplicate of DATA0CH0_CR_LPMODECTL_REG

#define DATA4CH0_CR_DDRCRDATACOMP0_REG                                 (0x000011B4)
//Duplicate of DATA0CH0_CR_DDRCRDATACOMP0_REG

#define DATA4CH0_CR_DDRCRDATACOMP1_REG                                 (0x000011B8)
//Duplicate of DATA0CH0_CR_DDRCRDATACOMP1_REG

#define DATA4CH0_CR_VCCDLLCOMPDATA_REG                                 (0x000011BC)
//Duplicate of DATA0CH0_CR_VCCDLLCOMPDATA_REG

#define DATA4CH0_CR_DCSOFFSET_REG                                      (0x000011C0)
//Duplicate of DATA0CH0_CR_DCSOFFSET_REG

#define DATA4CH0_CR_DFXCCMON_REG                                       (0x000011C4)
//Duplicate of DATA0CH0_CR_DFXCCMON_REG

#define DATA4CH0_CR_AFEMISCCTRL0_REG                                   (0x000011C8)
//Duplicate of DATA0CH0_CR_AFEMISCCTRL0_REG

#define DATA4CH0_CR_VIEWCTL_REG                                        (0x000011CC)
//Duplicate of DATA0CH0_CR_VIEWCTL_REG

#define DATA4CH0_CR_DCCCTL11_REG                                       (0x000011D0)
//Duplicate of DATA0CH0_CR_DCCCTL11_REG

#define DATA4CH0_CR_DCCCTL13_REG                                       (0x000011D4)
//Duplicate of DATA0CH0_CR_DCCCTL13_REG

#define DATA4CH0_CR_PMFSM_REG                                          (0x000011D8)
//Duplicate of DATA0CH0_CR_PMFSM_REG

#define DATA4CH0_CR_PMFSM1_REG                                         (0x000011DC)
//Duplicate of DATA0CH0_CR_PMFSM1_REG

#define DATA4CH0_CR_CLKALIGNSTATUS_REG                                 (0x000011E0)
//Duplicate of DATA0CH0_CR_CLKALIGNSTATUS_REG

#define DATA4CH0_CR_DATATRAINFEEDBACK_REG                              (0x000011E4)
//Duplicate of DATA0CH0_CR_DATATRAINFEEDBACK_REG

#define DATA4CH0_CR_DDRCRMARGINMODECONTROL0_REG                        (0x000011E8)
//Duplicate of DATA0CH0_CR_DDRCRMARGINMODECONTROL0_REG

#define DATA4CH0_CR_DDRCRMARGINMODEDEBUGMSB0_REG                       (0x000011EC)
//Duplicate of DATA0CH0_CR_DDRCRMARGINMODEDEBUGMSB0_REG

#define DATA4CH0_CR_DDRCRMARGINMODEDEBUGLSB0_REG                       (0x000011F0)
//Duplicate of DATA0CH0_CR_DDRCRMARGINMODEDEBUGMSB0_REG

#define DATA4CH0_CR_DCCCTL14_REG                                       (0x000011F4)
//Duplicate of DATA0CH0_CR_DCCCTL14_REG

#define DATA4CH1_CR_DDRDATADQRANK0LANE0_REG                            (0x00001200)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA4CH1_CR_DDRDATADQRANK0LANE1_REG                            (0x00001204)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA4CH1_CR_DDRDATADQRANK0LANE2_REG                            (0x00001208)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA4CH1_CR_DDRDATADQRANK0LANE3_REG                            (0x0000120C)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA4CH1_CR_DDRDATADQRANK0LANE4_REG                            (0x00001210)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA4CH1_CR_DDRDATADQRANK0LANE5_REG                            (0x00001214)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA4CH1_CR_DDRDATADQRANK0LANE6_REG                            (0x00001218)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA4CH1_CR_DDRDATADQRANK0LANE7_REG                            (0x0000121C)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA4CH1_CR_DDRDATADQRANK1LANE0_REG                            (0x00001220)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA4CH1_CR_DDRDATADQRANK1LANE1_REG                            (0x00001224)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA4CH1_CR_DDRDATADQRANK1LANE2_REG                            (0x00001228)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA4CH1_CR_DDRDATADQRANK1LANE3_REG                            (0x0000122C)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA4CH1_CR_DDRDATADQRANK1LANE4_REG                            (0x00001230)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA4CH1_CR_DDRDATADQRANK1LANE5_REG                            (0x00001234)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA4CH1_CR_DDRDATADQRANK1LANE6_REG                            (0x00001238)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA4CH1_CR_DDRDATADQRANK1LANE7_REG                            (0x0000123C)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA4CH1_CR_DDRDATADQRANK2LANE0_REG                            (0x00001240)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA4CH1_CR_DDRDATADQRANK2LANE1_REG                            (0x00001244)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA4CH1_CR_DDRDATADQRANK2LANE2_REG                            (0x00001248)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA4CH1_CR_DDRDATADQRANK2LANE3_REG                            (0x0000124C)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA4CH1_CR_DDRDATADQRANK2LANE4_REG                            (0x00001250)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA4CH1_CR_DDRDATADQRANK2LANE5_REG                            (0x00001254)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA4CH1_CR_DDRDATADQRANK2LANE6_REG                            (0x00001258)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA4CH1_CR_DDRDATADQRANK2LANE7_REG                            (0x0000125C)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA4CH1_CR_DDRDATADQRANK3LANE0_REG                            (0x00001260)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA4CH1_CR_DDRDATADQRANK3LANE1_REG                            (0x00001264)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA4CH1_CR_DDRDATADQRANK3LANE2_REG                            (0x00001268)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA4CH1_CR_DDRDATADQRANK3LANE3_REG                            (0x0000126C)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA4CH1_CR_DDRDATADQRANK3LANE4_REG                            (0x00001270)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA4CH1_CR_DDRDATADQRANK3LANE5_REG                            (0x00001274)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA4CH1_CR_DDRDATADQRANK3LANE6_REG                            (0x00001278)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA4CH1_CR_DDRDATADQRANK3LANE7_REG                            (0x0000127C)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA4CH1_CR_DDRCRDATACONTROL0_REG                              (0x00001280)
//Duplicate of DATA0CH0_CR_DDRCRDATACONTROL0_REG

#define DATA4CH1_CR_DDRCRDATACONTROL1_REG                              (0x00001284)
//Duplicate of DATA0CH0_CR_DDRCRDATACONTROL1_REG

#define DATA4CH1_CR_DDRCRDATACONTROL2_REG                              (0x00001288)
//Duplicate of DATA0CH0_CR_DDRCRDATACONTROL2_REG

#define DATA4CH1_CR_DDRCRDATACONTROL3_REG                              (0x0000128C)
//Duplicate of DATA0CH0_CR_DDRCRDATACONTROL3_REG

#define DATA4CH1_CR_AFEMISC_REG                                        (0x00001290)
//Duplicate of DATA0CH0_CR_AFEMISC_REG

#define DATA4CH1_CR_SRZCTL_REG                                         (0x00001294)
//Duplicate of DATA0CH0_CR_SRZCTL_REG

#define DATA4CH1_CR_DQRXCTL0_REG                                       (0x00001298)
//Duplicate of DATA0CH0_CR_DQRXCTL0_REG

#define DATA4CH1_CR_DQRXCTL1_REG                                       (0x0000129C)
//Duplicate of DATA0CH0_CR_DQRXCTL1_REG

#define DATA4CH1_CR_DQSTXRXCTL_REG                                     (0x000012A0)
//Duplicate of DATA0CH0_CR_DQSTXRXCTL_REG

#define DATA4CH1_CR_DQSTXRXCTL0_REG                                    (0x000012A4)
//Duplicate of DATA0CH0_CR_DQSTXRXCTL0_REG

#define DATA4CH1_CR_SDLCTL1_REG                                        (0x000012A8)
//Duplicate of DATA0CH0_CR_SDLCTL1_REG

#define DATA4CH1_CR_SDLCTL0_REG                                        (0x000012AC)
//Duplicate of DATA0CH0_CR_SDLCTL0_REG

#define DATA4CH1_CR_MDLLCTL0_REG                                       (0x000012B0)
//Duplicate of DATA0CH0_CR_MDLLCTL0_REG

#define DATA4CH1_CR_MDLLCTL1_REG                                       (0x000012B4)
//Duplicate of DATA0CH0_CR_MDLLCTL1_REG

#define DATA4CH1_CR_MDLLCTL2_REG                                       (0x000012B8)
//Duplicate of DATA0CH0_CR_MDLLCTL2_REG

#define DATA4CH1_CR_MDLLCTL3_REG                                       (0x000012BC)
//Duplicate of DATA0CH0_CR_MDLLCTL3_REG

#define DATA4CH1_CR_MDLLCTL4_REG                                       (0x000012C0)
//Duplicate of DATA0CH0_CR_MDLLCTL4_REG

#define DATA4CH1_CR_MDLLCTL5_REG                                       (0x000012C4)
//Duplicate of DATA0CH0_CR_MDLLCTL4_REG

#define DATA4CH1_CR_DIGMISCCTRL_REG                                    (0x000012C4)
//Duplicate of DATA0CH0_CR_DIGMISCCTRL_REG

#define DATA4CH1_CR_AFEMISCCTRL2_REG                                   (0x000012C8)
//Duplicate of DATA0CH0_CR_AFEMISCCTRL2_REG

#define DATA4CH1_CR_TXPBDOFFSET0_REG                                   (0x000012CC)
//Duplicate of DATA0CH0_CR_TXPBDOFFSET0_REG

#define DATA4CH1_CR_TXPBDOFFSET1_REG                                   (0x000012D0)
//Duplicate of DATA0CH0_CR_TXPBDOFFSET1_REG

#define DATA4CH1_CR_COMPCTL0_REG                                       (0x000012D4)
//Duplicate of DATA0CH0_CR_COMPCTL0_REG

#define DATA4CH1_CR_COMPCTL1_REG                                       (0x000012D8)
//Duplicate of DATA0CH0_CR_COMPCTL1_REG

#define DATA4CH1_CR_COMPCTL2_REG                                       (0x000012DC)
//Duplicate of DATA0CH0_CR_COMPCTL2_REG

#define DATA4CH1_CR_DCCCTL5_REG                                        (0x000012E0)
//Duplicate of DATA0CH0_CR_DCCCTL5_REG

#define DATA4CH1_CR_DCCCTL6_REG                                        (0x000012E4)
//Duplicate of DATA0CH0_CR_DCCCTL6_REG

#define DATA4CH1_CR_DCCCTL7_REG                                        (0x000012E8)
//Duplicate of DATA0CH0_CR_DCCCTL7_REG

#define DATA4CH1_CR_DCCCTL8_REG                                        (0x000012EC)
//Duplicate of DATA0CH0_CR_DCCCTL8_REG

#define DATA4CH1_CR_DCCCTL9_REG                                        (0x000012F0)
//Duplicate of DATA0CH0_CR_DCCCTL9_REG

#define DATA4CH1_CR_RXCONTROL0RANK0_REG                                (0x000012F4)
//Duplicate of DATA0CH0_CR_RXCONTROL0RANK0_REG

#define DATA4CH1_CR_RXCONTROL0RANK1_REG                                (0x000012F8)
//Duplicate of DATA0CH0_CR_RXCONTROL0RANK0_REG

#define DATA4CH1_CR_RXCONTROL0RANK2_REG                                (0x000012FC)
//Duplicate of DATA0CH0_CR_RXCONTROL0RANK0_REG

#define DATA4CH1_CR_RXCONTROL0RANK3_REG                                (0x00001300)
//Duplicate of DATA0CH0_CR_RXCONTROL0RANK0_REG

#define DATA4CH1_CR_TXCONTROL0_PH90_REG                                (0x00001304)
//Duplicate of DATA0CH0_CR_TXCONTROL0_PH90_REG

#define DATA4CH1_CR_TXCONTROL0RANK0_REG                                (0x00001308)
//Duplicate of DATA0CH0_CR_TXCONTROL0RANK0_REG

#define DATA4CH1_CR_TXCONTROL0RANK1_REG                                (0x0000130C)
//Duplicate of DATA0CH0_CR_TXCONTROL0RANK0_REG

#define DATA4CH1_CR_TXCONTROL0RANK2_REG                                (0x00001310)
//Duplicate of DATA0CH0_CR_TXCONTROL0RANK0_REG

#define DATA4CH1_CR_TXCONTROL0RANK3_REG                                (0x00001314)
//Duplicate of DATA0CH0_CR_TXCONTROL0RANK0_REG

#define DATA4CH1_CR_DDRDATADQSRANKX_REG                                (0x00001318)
//Duplicate of DATA0CH0_CR_DDRDATADQSRANKX_REG

#define DATA4CH1_CR_DDRDATADQSPH90RANKX_REG                            (0x0000131C)
//Duplicate of DATA0CH0_CR_DDRDATADQSPH90RANKX_REG

#define DATA4CH1_CR_DDRCRDATAOFFSETCOMP_REG                            (0x00001320)
//Duplicate of DATA0CH0_CR_DDRCRDATAOFFSETCOMP_REG

#define DATA4CH1_CR_DDRCRDATAOFFSETTRAIN_REG                           (0x00001324)
//Duplicate of DATA0CH0_CR_DDRCRDATAOFFSETTRAIN_REG

#define DATA4CH1_CR_DDRCRCMDBUSTRAIN_REG                               (0x00001328)
//Duplicate of DATA0CH0_CR_DDRCRCMDBUSTRAIN_REG

#define DATA4CH1_CR_DDRCRWRRETRAINSWIZZLECONTROL_REG                   (0x0000132C)
//Duplicate of DATA0CH0_CR_DDRCRWRRETRAINSWIZZLECONTROL_REG

#define DATA4CH1_CR_DDRCRWRRETRAINRANK3_REG                            (0x00001330)
//Duplicate of DATA0CH0_CR_DDRCRWRRETRAINRANK3_REG

#define DATA4CH1_CR_DDRCRWRRETRAINRANK2_REG                            (0x00001334)
//Duplicate of DATA0CH0_CR_DDRCRWRRETRAINRANK3_REG

#define DATA4CH1_CR_DDRCRWRRETRAINRANK1_REG                            (0x00001338)
//Duplicate of DATA0CH0_CR_DDRCRWRRETRAINRANK3_REG

#define DATA4CH1_CR_DDRCRWRRETRAINRANK0_REG                            (0x0000133C)
//Duplicate of DATA0CH0_CR_DDRCRWRRETRAINRANK3_REG

#define DATA4CH1_CR_DDRCRWRRETRAINCONTROLSTATUS_REG                    (0x00001340)
//Duplicate of DATA0CH0_CR_DDRCRWRRETRAINCONTROLSTATUS_REG

#define DATA4CH1_CR_TXCONTROL1_PH90_REG                                (0x00001344)
//Duplicate of DATA0CH0_CR_TXCONTROL1_PH90_REG

#define DATA4CH1_CR_AFEMISCCTRL1_REG                                   (0x00001348)
//Duplicate of DATA0CH0_CR_AFEMISCCTRL1_REG

#define DATA4CH1_CR_DQXRXAMPCTL_REG                                    (0x0000134C)
//Duplicate of DATA0CH0_CR_DQXRXAMPCTL_REG

#define DATA4CH1_CR_DQ0RXAMPCTL_REG                                    (0x00001350)
//Duplicate of DATA0CH0_CR_DQ0RXAMPCTL_REG

#define DATA4CH1_CR_DQ1RXAMPCTL_REG                                    (0x00001354)
//Duplicate of DATA0CH0_CR_DQ1RXAMPCTL_REG

#define DATA4CH1_CR_DQ2RXAMPCTL_REG                                    (0x00001358)
//Duplicate of DATA0CH0_CR_DQ2RXAMPCTL_REG

#define DATA4CH1_CR_DQ3RXAMPCTL_REG                                    (0x0000135C)
//Duplicate of DATA0CH0_CR_DQ3RXAMPCTL_REG

#define DATA4CH1_CR_DQ4RXAMPCTL_REG                                    (0x00001360)
//Duplicate of DATA0CH0_CR_DQ4RXAMPCTL_REG

#define DATA4CH1_CR_DQ5RXAMPCTL_REG                                    (0x00001364)
//Duplicate of DATA0CH0_CR_DQ5RXAMPCTL_REG

#define DATA4CH1_CR_DQ6RXAMPCTL_REG                                    (0x00001368)
//Duplicate of DATA0CH0_CR_DQ6RXAMPCTL_REG

#define DATA4CH1_CR_DQ7RXAMPCTL_REG                                    (0x0000136C)
//Duplicate of DATA0CH0_CR_DQ7RXAMPCTL_REG

#define DATA4CH1_CR_DQRXAMPCTL0_REG                                    (0x00001370)
//Duplicate of DATA0CH0_CR_DQRXAMPCTL0_REG

#define DATA4CH1_CR_DQRXAMPCTL1_REG                                    (0x00001374)
//Duplicate of DATA0CH0_CR_DQRXAMPCTL1_REG

#define DATA4CH1_CR_DQRXAMPCTL2_REG                                    (0x00001378)
//Duplicate of DATA0CH0_CR_DQRXAMPCTL2_REG

#define DATA4CH1_CR_DQTXCTL0_REG                                       (0x0000137C)
//Duplicate of DATA0CH0_CR_DQTXCTL0_REG

#define DATA4CH1_CR_DCCCTL0_REG                                        (0x00001380)
//Duplicate of DATA0CH0_CR_DCCCTL0_REG

#define DATA4CH1_CR_DCCCTL1_REG                                        (0x00001384)
//Duplicate of DATA0CH0_CR_DCCCTL1_REG

#define DATA4CH1_CR_DCCCTL2_REG                                        (0x00001388)
//Duplicate of DATA0CH0_CR_DCCCTL2_REG

#define DATA4CH1_CR_DCCCTL3_REG                                        (0x0000138C)
//Duplicate of DATA0CH0_CR_DCCCTL3_REG

#define DATA4CH1_CR_DCCCTL10_REG                                       (0x00001390)
//Duplicate of DATA0CH0_CR_DCCCTL10_REG

#define DATA4CH1_CR_DCCCTL12_REG                                       (0x00001394)
//Duplicate of DATA0CH0_CR_DCCCTL12_REG

#define DATA4CH1_CR_CLKALIGNCTL0_REG                                   (0x00001398)
//Duplicate of DATA0CH0_CR_CLKALIGNCTL0_REG

#define DATA4CH1_CR_CLKALIGNCTL1_REG                                   (0x0000139C)
//Duplicate of DATA0CH0_CR_CLKALIGNCTL1_REG

#define DATA4CH1_CR_CLKALIGNCTL2_REG                                   (0x000013A0)
//Duplicate of DATA0CH0_CR_CLKALIGNCTL2_REG

#define DATA4CH1_CR_DCCCTL4_REG                                        (0x000013A4)
//Duplicate of DATA0CH0_CR_DCCCTL4_REG

#define DATA4CH1_CR_DDRCRDATACOMPVTT_REG                               (0x000013A8)
//Duplicate of DATA0CH0_CR_DDRCRDATACOMPVTT_REG

#define DATA4CH1_CR_WLCTRL_REG                                         (0x000013AC)
//Duplicate of DATA0CH0_CR_WLCTRL_REG

#define DATA4CH1_CR_LPMODECTL_REG                                      (0x000013B0)
//Duplicate of DATA0CH0_CR_LPMODECTL_REG

#define DATA4CH1_CR_DDRCRDATACOMP0_REG                                 (0x000013B4)
//Duplicate of DATA0CH0_CR_DDRCRDATACOMP0_REG

#define DATA4CH1_CR_DDRCRDATACOMP1_REG                                 (0x000013B8)
//Duplicate of DATA0CH0_CR_DDRCRDATACOMP1_REG

#define DATA4CH1_CR_VCCDLLCOMPDATA_REG                                 (0x000013BC)
//Duplicate of DATA0CH0_CR_VCCDLLCOMPDATA_REG

#define DATA4CH1_CR_DCSOFFSET_REG                                      (0x000013C0)
//Duplicate of DATA0CH0_CR_DCSOFFSET_REG

#define DATA4CH1_CR_DFXCCMON_REG                                       (0x000013C4)
//Duplicate of DATA0CH0_CR_DFXCCMON_REG

#define DATA4CH1_CR_AFEMISCCTRL0_REG                                   (0x000013C8)
//Duplicate of DATA0CH0_CR_AFEMISCCTRL0_REG

#define DATA4CH1_CR_VIEWCTL_REG                                        (0x000013CC)
//Duplicate of DATA0CH0_CR_VIEWCTL_REG

#define DATA4CH1_CR_DCCCTL11_REG                                       (0x000013D0)
//Duplicate of DATA0CH0_CR_DCCCTL11_REG

#define DATA4CH1_CR_DCCCTL13_REG                                       (0x000013D4)
//Duplicate of DATA0CH0_CR_DCCCTL13_REG

#define DATA4CH1_CR_PMFSM_REG                                          (0x000013D8)
//Duplicate of DATA0CH0_CR_PMFSM_REG

#define DATA4CH1_CR_PMFSM1_REG                                         (0x000013DC)
//Duplicate of DATA0CH0_CR_PMFSM1_REG

#define DATA4CH1_CR_CLKALIGNSTATUS_REG                                 (0x000013E0)
//Duplicate of DATA0CH0_CR_CLKALIGNSTATUS_REG

#define DATA4CH1_CR_DATATRAINFEEDBACK_REG                              (0x000013E4)
//Duplicate of DATA0CH0_CR_DATATRAINFEEDBACK_REG

#define DATA4CH1_CR_DDRCRMARGINMODECONTROL0_REG                        (0x000013E8)
//Duplicate of DATA0CH0_CR_DDRCRMARGINMODECONTROL0_REG

#define DATA4CH1_CR_DDRCRMARGINMODEDEBUGMSB0_REG                       (0x000013EC)
//Duplicate of DATA0CH0_CR_DDRCRMARGINMODEDEBUGMSB0_REG

#define DATA4CH1_CR_DDRCRMARGINMODEDEBUGLSB0_REG                       (0x000013F0)
//Duplicate of DATA0CH0_CR_DDRCRMARGINMODEDEBUGMSB0_REG

#define DATA4CH1_CR_DCCCTL14_REG                                       (0x000013F4)
//Duplicate of DATA0CH0_CR_DCCCTL14_REG

#define DATA5CH0_CR_DDRDATADQRANK0LANE0_REG                            (0x00001400)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA5CH0_CR_DDRDATADQRANK0LANE1_REG                            (0x00001404)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA5CH0_CR_DDRDATADQRANK0LANE2_REG                            (0x00001408)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA5CH0_CR_DDRDATADQRANK0LANE3_REG                            (0x0000140C)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA5CH0_CR_DDRDATADQRANK0LANE4_REG                            (0x00001410)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA5CH0_CR_DDRDATADQRANK0LANE5_REG                            (0x00001414)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA5CH0_CR_DDRDATADQRANK0LANE6_REG                            (0x00001418)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA5CH0_CR_DDRDATADQRANK0LANE7_REG                            (0x0000141C)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA5CH0_CR_DDRDATADQRANK1LANE0_REG                            (0x00001420)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA5CH0_CR_DDRDATADQRANK1LANE1_REG                            (0x00001424)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA5CH0_CR_DDRDATADQRANK1LANE2_REG                            (0x00001428)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA5CH0_CR_DDRDATADQRANK1LANE3_REG                            (0x0000142C)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA5CH0_CR_DDRDATADQRANK1LANE4_REG                            (0x00001430)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA5CH0_CR_DDRDATADQRANK1LANE5_REG                            (0x00001434)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA5CH0_CR_DDRDATADQRANK1LANE6_REG                            (0x00001438)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA5CH0_CR_DDRDATADQRANK1LANE7_REG                            (0x0000143C)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA5CH0_CR_DDRDATADQRANK2LANE0_REG                            (0x00001440)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA5CH0_CR_DDRDATADQRANK2LANE1_REG                            (0x00001444)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA5CH0_CR_DDRDATADQRANK2LANE2_REG                            (0x00001448)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA5CH0_CR_DDRDATADQRANK2LANE3_REG                            (0x0000144C)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA5CH0_CR_DDRDATADQRANK2LANE4_REG                            (0x00001450)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA5CH0_CR_DDRDATADQRANK2LANE5_REG                            (0x00001454)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA5CH0_CR_DDRDATADQRANK2LANE6_REG                            (0x00001458)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA5CH0_CR_DDRDATADQRANK2LANE7_REG                            (0x0000145C)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA5CH0_CR_DDRDATADQRANK3LANE0_REG                            (0x00001460)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA5CH0_CR_DDRDATADQRANK3LANE1_REG                            (0x00001464)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA5CH0_CR_DDRDATADQRANK3LANE2_REG                            (0x00001468)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA5CH0_CR_DDRDATADQRANK3LANE3_REG                            (0x0000146C)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA5CH0_CR_DDRDATADQRANK3LANE4_REG                            (0x00001470)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA5CH0_CR_DDRDATADQRANK3LANE5_REG                            (0x00001474)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA5CH0_CR_DDRDATADQRANK3LANE6_REG                            (0x00001478)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA5CH0_CR_DDRDATADQRANK3LANE7_REG                            (0x0000147C)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA5CH0_CR_DDRCRDATACONTROL0_REG                              (0x00001480)
//Duplicate of DATA0CH0_CR_DDRCRDATACONTROL0_REG

#define DATA5CH0_CR_DDRCRDATACONTROL1_REG                              (0x00001484)
//Duplicate of DATA0CH0_CR_DDRCRDATACONTROL1_REG

#define DATA5CH0_CR_DDRCRDATACONTROL2_REG                              (0x00001488)
//Duplicate of DATA0CH0_CR_DDRCRDATACONTROL2_REG

#define DATA5CH0_CR_DDRCRDATACONTROL3_REG                              (0x0000148C)
//Duplicate of DATA0CH0_CR_DDRCRDATACONTROL3_REG

#define DATA5CH0_CR_AFEMISC_REG                                        (0x00001490)
//Duplicate of DATA0CH0_CR_AFEMISC_REG

#define DATA5CH0_CR_SRZCTL_REG                                         (0x00001494)
//Duplicate of DATA0CH0_CR_SRZCTL_REG

#define DATA5CH0_CR_DQRXCTL0_REG                                       (0x00001498)
//Duplicate of DATA0CH0_CR_DQRXCTL0_REG

#define DATA5CH0_CR_DQRXCTL1_REG                                       (0x0000149C)
//Duplicate of DATA0CH0_CR_DQRXCTL1_REG

#define DATA5CH0_CR_DQSTXRXCTL_REG                                     (0x000014A0)
//Duplicate of DATA0CH0_CR_DQSTXRXCTL_REG

#define DATA5CH0_CR_DQSTXRXCTL0_REG                                    (0x000014A4)
//Duplicate of DATA0CH0_CR_DQSTXRXCTL0_REG

#define DATA5CH0_CR_SDLCTL1_REG                                        (0x000014A8)
//Duplicate of DATA0CH0_CR_SDLCTL1_REG

#define DATA5CH0_CR_SDLCTL0_REG                                        (0x000014AC)
//Duplicate of DATA0CH0_CR_SDLCTL0_REG

#define DATA5CH0_CR_MDLLCTL0_REG                                       (0x000014B0)
//Duplicate of DATA0CH0_CR_MDLLCTL0_REG

#define DATA5CH0_CR_MDLLCTL1_REG                                       (0x000014B4)
//Duplicate of DATA0CH0_CR_MDLLCTL1_REG

#define DATA5CH0_CR_MDLLCTL2_REG                                       (0x000014B8)
//Duplicate of DATA0CH0_CR_MDLLCTL2_REG

#define DATA5CH0_CR_MDLLCTL3_REG                                       (0x000014BC)
//Duplicate of DATA0CH0_CR_MDLLCTL3_REG

#define DATA5CH0_CR_MDLLCTL4_REG                                       (0x000014C0)
//Duplicate of DATA0CH0_CR_MDLLCTL4_REG

#define DATA5CH0_CR_MDLLCTL5_REG                                       (0x000014C4)
//Duplicate of DATA0CH0_CR_MDLLCTL4_REG

#define DATA5CH0_CR_DIGMISCCTRL_REG                                    (0x000014C4)
//Duplicate of DATA0CH0_CR_DIGMISCCTRL_REG

#define DATA5CH0_CR_AFEMISCCTRL2_REG                                   (0x000014C8)
//Duplicate of DATA0CH0_CR_AFEMISCCTRL2_REG

#define DATA5CH0_CR_TXPBDOFFSET0_REG                                   (0x000014CC)
//Duplicate of DATA0CH0_CR_TXPBDOFFSET0_REG

#define DATA5CH0_CR_TXPBDOFFSET1_REG                                   (0x000014D0)
//Duplicate of DATA0CH0_CR_TXPBDOFFSET1_REG

#define DATA5CH0_CR_COMPCTL0_REG                                       (0x000014D4)
//Duplicate of DATA0CH0_CR_COMPCTL0_REG

#define DATA5CH0_CR_COMPCTL1_REG                                       (0x000014D8)
//Duplicate of DATA0CH0_CR_COMPCTL1_REG

#define DATA5CH0_CR_COMPCTL2_REG                                       (0x000014DC)
//Duplicate of DATA0CH0_CR_COMPCTL2_REG

#define DATA5CH0_CR_DCCCTL5_REG                                        (0x000014E0)
//Duplicate of DATA0CH0_CR_DCCCTL5_REG

#define DATA5CH0_CR_DCCCTL6_REG                                        (0x000014E4)
//Duplicate of DATA0CH0_CR_DCCCTL6_REG

#define DATA5CH0_CR_DCCCTL7_REG                                        (0x000014E8)
//Duplicate of DATA0CH0_CR_DCCCTL7_REG

#define DATA5CH0_CR_DCCCTL8_REG                                        (0x000014EC)
//Duplicate of DATA0CH0_CR_DCCCTL8_REG

#define DATA5CH0_CR_DCCCTL9_REG                                        (0x000014F0)
//Duplicate of DATA0CH0_CR_DCCCTL9_REG

#define DATA5CH0_CR_RXCONTROL0RANK0_REG                                (0x000014F4)
//Duplicate of DATA0CH0_CR_RXCONTROL0RANK0_REG

#define DATA5CH0_CR_RXCONTROL0RANK1_REG                                (0x000014F8)
//Duplicate of DATA0CH0_CR_RXCONTROL0RANK0_REG

#define DATA5CH0_CR_RXCONTROL0RANK2_REG                                (0x000014FC)
//Duplicate of DATA0CH0_CR_RXCONTROL0RANK0_REG

#define DATA5CH0_CR_RXCONTROL0RANK3_REG                                (0x00001500)
//Duplicate of DATA0CH0_CR_RXCONTROL0RANK0_REG

#define DATA5CH0_CR_TXCONTROL0_PH90_REG                                (0x00001504)
//Duplicate of DATA0CH0_CR_TXCONTROL0_PH90_REG

#define DATA5CH0_CR_TXCONTROL0RANK0_REG                                (0x00001508)
//Duplicate of DATA0CH0_CR_TXCONTROL0RANK0_REG

#define DATA5CH0_CR_TXCONTROL0RANK1_REG                                (0x0000150C)
//Duplicate of DATA0CH0_CR_TXCONTROL0RANK0_REG

#define DATA5CH0_CR_TXCONTROL0RANK2_REG                                (0x00001510)
//Duplicate of DATA0CH0_CR_TXCONTROL0RANK0_REG

#define DATA5CH0_CR_TXCONTROL0RANK3_REG                                (0x00001514)
//Duplicate of DATA0CH0_CR_TXCONTROL0RANK0_REG

#define DATA5CH0_CR_DDRDATADQSRANKX_REG                                (0x00001518)
//Duplicate of DATA0CH0_CR_DDRDATADQSRANKX_REG

#define DATA5CH0_CR_DDRDATADQSPH90RANKX_REG                            (0x0000151C)
//Duplicate of DATA0CH0_CR_DDRDATADQSPH90RANKX_REG

#define DATA5CH0_CR_DDRCRDATAOFFSETCOMP_REG                            (0x00001520)
//Duplicate of DATA0CH0_CR_DDRCRDATAOFFSETCOMP_REG

#define DATA5CH0_CR_DDRCRDATAOFFSETTRAIN_REG                           (0x00001524)
//Duplicate of DATA0CH0_CR_DDRCRDATAOFFSETTRAIN_REG

#define DATA5CH0_CR_DDRCRCMDBUSTRAIN_REG                               (0x00001528)
//Duplicate of DATA0CH0_CR_DDRCRCMDBUSTRAIN_REG

#define DATA5CH0_CR_DDRCRWRRETRAINSWIZZLECONTROL_REG                   (0x0000152C)
//Duplicate of DATA0CH0_CR_DDRCRWRRETRAINSWIZZLECONTROL_REG

#define DATA5CH0_CR_DDRCRWRRETRAINRANK3_REG                            (0x00001530)
//Duplicate of DATA0CH0_CR_DDRCRWRRETRAINRANK3_REG

#define DATA5CH0_CR_DDRCRWRRETRAINRANK2_REG                            (0x00001534)
//Duplicate of DATA0CH0_CR_DDRCRWRRETRAINRANK3_REG

#define DATA5CH0_CR_DDRCRWRRETRAINRANK1_REG                            (0x00001538)
//Duplicate of DATA0CH0_CR_DDRCRWRRETRAINRANK3_REG

#define DATA5CH0_CR_DDRCRWRRETRAINRANK0_REG                            (0x0000153C)
//Duplicate of DATA0CH0_CR_DDRCRWRRETRAINRANK3_REG

#define DATA5CH0_CR_DDRCRWRRETRAINCONTROLSTATUS_REG                    (0x00001540)
//Duplicate of DATA0CH0_CR_DDRCRWRRETRAINCONTROLSTATUS_REG

#define DATA5CH0_CR_TXCONTROL1_PH90_REG                                (0x00001544)
//Duplicate of DATA0CH0_CR_TXCONTROL1_PH90_REG

#define DATA5CH0_CR_AFEMISCCTRL1_REG                                   (0x00001548)
//Duplicate of DATA0CH0_CR_AFEMISCCTRL1_REG

#define DATA5CH0_CR_DQXRXAMPCTL_REG                                    (0x0000154C)
//Duplicate of DATA0CH0_CR_DQXRXAMPCTL_REG

#define DATA5CH0_CR_DQ0RXAMPCTL_REG                                    (0x00001550)
//Duplicate of DATA0CH0_CR_DQ0RXAMPCTL_REG

#define DATA5CH0_CR_DQ1RXAMPCTL_REG                                    (0x00001554)
//Duplicate of DATA0CH0_CR_DQ1RXAMPCTL_REG

#define DATA5CH0_CR_DQ2RXAMPCTL_REG                                    (0x00001558)
//Duplicate of DATA0CH0_CR_DQ2RXAMPCTL_REG

#define DATA5CH0_CR_DQ3RXAMPCTL_REG                                    (0x0000155C)
//Duplicate of DATA0CH0_CR_DQ3RXAMPCTL_REG

#define DATA5CH0_CR_DQ4RXAMPCTL_REG                                    (0x00001560)
//Duplicate of DATA0CH0_CR_DQ4RXAMPCTL_REG

#define DATA5CH0_CR_DQ5RXAMPCTL_REG                                    (0x00001564)
//Duplicate of DATA0CH0_CR_DQ5RXAMPCTL_REG

#define DATA5CH0_CR_DQ6RXAMPCTL_REG                                    (0x00001568)
//Duplicate of DATA0CH0_CR_DQ6RXAMPCTL_REG

#define DATA5CH0_CR_DQ7RXAMPCTL_REG                                    (0x0000156C)
//Duplicate of DATA0CH0_CR_DQ7RXAMPCTL_REG

#define DATA5CH0_CR_DQRXAMPCTL0_REG                                    (0x00001570)
//Duplicate of DATA0CH0_CR_DQRXAMPCTL0_REG

#define DATA5CH0_CR_DQRXAMPCTL1_REG                                    (0x00001574)
//Duplicate of DATA0CH0_CR_DQRXAMPCTL1_REG

#define DATA5CH0_CR_DQRXAMPCTL2_REG                                    (0x00001578)
//Duplicate of DATA0CH0_CR_DQRXAMPCTL2_REG

#define DATA5CH0_CR_DQTXCTL0_REG                                       (0x0000157C)
//Duplicate of DATA0CH0_CR_DQTXCTL0_REG

#define DATA5CH0_CR_DCCCTL0_REG                                        (0x00001580)
//Duplicate of DATA0CH0_CR_DCCCTL0_REG

#define DATA5CH0_CR_DCCCTL1_REG                                        (0x00001584)
//Duplicate of DATA0CH0_CR_DCCCTL1_REG

#define DATA5CH0_CR_DCCCTL2_REG                                        (0x00001588)
//Duplicate of DATA0CH0_CR_DCCCTL2_REG

#define DATA5CH0_CR_DCCCTL3_REG                                        (0x0000158C)
//Duplicate of DATA0CH0_CR_DCCCTL3_REG

#define DATA5CH0_CR_DCCCTL10_REG                                       (0x00001590)
//Duplicate of DATA0CH0_CR_DCCCTL10_REG

#define DATA5CH0_CR_DCCCTL12_REG                                       (0x00001594)
//Duplicate of DATA0CH0_CR_DCCCTL12_REG

#define DATA5CH0_CR_CLKALIGNCTL0_REG                                   (0x00001598)
//Duplicate of DATA0CH0_CR_CLKALIGNCTL0_REG

#define DATA5CH0_CR_CLKALIGNCTL1_REG                                   (0x0000159C)
//Duplicate of DATA0CH0_CR_CLKALIGNCTL1_REG

#define DATA5CH0_CR_CLKALIGNCTL2_REG                                   (0x000015A0)
//Duplicate of DATA0CH0_CR_CLKALIGNCTL2_REG

#define DATA5CH0_CR_DCCCTL4_REG                                        (0x000015A4)
//Duplicate of DATA0CH0_CR_DCCCTL4_REG

#define DATA5CH0_CR_DDRCRDATACOMPVTT_REG                               (0x000015A8)
//Duplicate of DATA0CH0_CR_DDRCRDATACOMPVTT_REG

#define DATA5CH0_CR_WLCTRL_REG                                         (0x000015AC)
//Duplicate of DATA0CH0_CR_WLCTRL_REG

#define DATA5CH0_CR_LPMODECTL_REG                                      (0x000015B0)
//Duplicate of DATA0CH0_CR_LPMODECTL_REG

#define DATA5CH0_CR_DDRCRDATACOMP0_REG                                 (0x000015B4)
//Duplicate of DATA0CH0_CR_DDRCRDATACOMP0_REG

#define DATA5CH0_CR_DDRCRDATACOMP1_REG                                 (0x000015B8)
//Duplicate of DATA0CH0_CR_DDRCRDATACOMP1_REG

#define DATA5CH0_CR_VCCDLLCOMPDATA_REG                                 (0x000015BC)
//Duplicate of DATA0CH0_CR_VCCDLLCOMPDATA_REG

#define DATA5CH0_CR_DCSOFFSET_REG                                      (0x000015C0)
//Duplicate of DATA0CH0_CR_DCSOFFSET_REG

#define DATA5CH0_CR_DFXCCMON_REG                                       (0x000015C4)
//Duplicate of DATA0CH0_CR_DFXCCMON_REG

#define DATA5CH0_CR_AFEMISCCTRL0_REG                                   (0x000015C8)
//Duplicate of DATA0CH0_CR_AFEMISCCTRL0_REG

#define DATA5CH0_CR_VIEWCTL_REG                                        (0x000015CC)
//Duplicate of DATA0CH0_CR_VIEWCTL_REG

#define DATA5CH0_CR_DCCCTL11_REG                                       (0x000015D0)
//Duplicate of DATA0CH0_CR_DCCCTL11_REG

#define DATA5CH0_CR_DCCCTL13_REG                                       (0x000015D4)
//Duplicate of DATA0CH0_CR_DCCCTL13_REG

#define DATA5CH0_CR_PMFSM_REG                                          (0x000015D8)
//Duplicate of DATA0CH0_CR_PMFSM_REG

#define DATA5CH0_CR_PMFSM1_REG                                         (0x000015DC)
//Duplicate of DATA0CH0_CR_PMFSM1_REG

#define DATA5CH0_CR_CLKALIGNSTATUS_REG                                 (0x000015E0)
//Duplicate of DATA0CH0_CR_CLKALIGNSTATUS_REG

#define DATA5CH0_CR_DATATRAINFEEDBACK_REG                              (0x000015E4)
//Duplicate of DATA0CH0_CR_DATATRAINFEEDBACK_REG

#define DATA5CH0_CR_DDRCRMARGINMODECONTROL0_REG                        (0x000015E8)
//Duplicate of DATA0CH0_CR_DDRCRMARGINMODECONTROL0_REG

#define DATA5CH0_CR_DDRCRMARGINMODEDEBUGMSB0_REG                       (0x000015EC)
//Duplicate of DATA0CH0_CR_DDRCRMARGINMODEDEBUGMSB0_REG

#define DATA5CH0_CR_DDRCRMARGINMODEDEBUGLSB0_REG                       (0x000015F0)
//Duplicate of DATA0CH0_CR_DDRCRMARGINMODEDEBUGMSB0_REG

#define DATA5CH0_CR_DCCCTL14_REG                                       (0x000015F4)
//Duplicate of DATA0CH0_CR_DCCCTL14_REG

#define DATA5CH1_CR_DDRDATADQRANK0LANE0_REG                            (0x00001600)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA5CH1_CR_DDRDATADQRANK0LANE1_REG                            (0x00001604)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA5CH1_CR_DDRDATADQRANK0LANE2_REG                            (0x00001608)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA5CH1_CR_DDRDATADQRANK0LANE3_REG                            (0x0000160C)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA5CH1_CR_DDRDATADQRANK0LANE4_REG                            (0x00001610)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA5CH1_CR_DDRDATADQRANK0LANE5_REG                            (0x00001614)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA5CH1_CR_DDRDATADQRANK0LANE6_REG                            (0x00001618)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA5CH1_CR_DDRDATADQRANK0LANE7_REG                            (0x0000161C)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA5CH1_CR_DDRDATADQRANK1LANE0_REG                            (0x00001620)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA5CH1_CR_DDRDATADQRANK1LANE1_REG                            (0x00001624)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA5CH1_CR_DDRDATADQRANK1LANE2_REG                            (0x00001628)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA5CH1_CR_DDRDATADQRANK1LANE3_REG                            (0x0000162C)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA5CH1_CR_DDRDATADQRANK1LANE4_REG                            (0x00001630)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA5CH1_CR_DDRDATADQRANK1LANE5_REG                            (0x00001634)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA5CH1_CR_DDRDATADQRANK1LANE6_REG                            (0x00001638)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA5CH1_CR_DDRDATADQRANK1LANE7_REG                            (0x0000163C)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA5CH1_CR_DDRDATADQRANK2LANE0_REG                            (0x00001640)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA5CH1_CR_DDRDATADQRANK2LANE1_REG                            (0x00001644)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA5CH1_CR_DDRDATADQRANK2LANE2_REG                            (0x00001648)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA5CH1_CR_DDRDATADQRANK2LANE3_REG                            (0x0000164C)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA5CH1_CR_DDRDATADQRANK2LANE4_REG                            (0x00001650)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA5CH1_CR_DDRDATADQRANK2LANE5_REG                            (0x00001654)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA5CH1_CR_DDRDATADQRANK2LANE6_REG                            (0x00001658)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA5CH1_CR_DDRDATADQRANK2LANE7_REG                            (0x0000165C)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA5CH1_CR_DDRDATADQRANK3LANE0_REG                            (0x00001660)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA5CH1_CR_DDRDATADQRANK3LANE1_REG                            (0x00001664)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA5CH1_CR_DDRDATADQRANK3LANE2_REG                            (0x00001668)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA5CH1_CR_DDRDATADQRANK3LANE3_REG                            (0x0000166C)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA5CH1_CR_DDRDATADQRANK3LANE4_REG                            (0x00001670)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA5CH1_CR_DDRDATADQRANK3LANE5_REG                            (0x00001674)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA5CH1_CR_DDRDATADQRANK3LANE6_REG                            (0x00001678)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA5CH1_CR_DDRDATADQRANK3LANE7_REG                            (0x0000167C)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA5CH1_CR_DDRCRDATACONTROL0_REG                              (0x00001680)
//Duplicate of DATA0CH0_CR_DDRCRDATACONTROL0_REG

#define DATA5CH1_CR_DDRCRDATACONTROL1_REG                              (0x00001684)
//Duplicate of DATA0CH0_CR_DDRCRDATACONTROL1_REG

#define DATA5CH1_CR_DDRCRDATACONTROL2_REG                              (0x00001688)
//Duplicate of DATA0CH0_CR_DDRCRDATACONTROL2_REG

#define DATA5CH1_CR_DDRCRDATACONTROL3_REG                              (0x0000168C)
//Duplicate of DATA0CH0_CR_DDRCRDATACONTROL3_REG

#define DATA5CH1_CR_AFEMISC_REG                                        (0x00001690)
//Duplicate of DATA0CH0_CR_AFEMISC_REG

#define DATA5CH1_CR_SRZCTL_REG                                         (0x00001694)
//Duplicate of DATA0CH0_CR_SRZCTL_REG

#define DATA5CH1_CR_DQRXCTL0_REG                                       (0x00001698)
//Duplicate of DATA0CH0_CR_DQRXCTL0_REG

#define DATA5CH1_CR_DQRXCTL1_REG                                       (0x0000169C)
//Duplicate of DATA0CH0_CR_DQRXCTL1_REG

#define DATA5CH1_CR_DQSTXRXCTL_REG                                     (0x000016A0)
//Duplicate of DATA0CH0_CR_DQSTXRXCTL_REG

#define DATA5CH1_CR_DQSTXRXCTL0_REG                                    (0x000016A4)
//Duplicate of DATA0CH0_CR_DQSTXRXCTL0_REG

#define DATA5CH1_CR_SDLCTL1_REG                                        (0x000016A8)
//Duplicate of DATA0CH0_CR_SDLCTL1_REG

#define DATA5CH1_CR_SDLCTL0_REG                                        (0x000016AC)
//Duplicate of DATA0CH0_CR_SDLCTL0_REG

#define DATA5CH1_CR_MDLLCTL0_REG                                       (0x000016B0)
//Duplicate of DATA0CH0_CR_MDLLCTL0_REG

#define DATA5CH1_CR_MDLLCTL1_REG                                       (0x000016B4)
//Duplicate of DATA0CH0_CR_MDLLCTL1_REG

#define DATA5CH1_CR_MDLLCTL2_REG                                       (0x000016B8)
//Duplicate of DATA0CH0_CR_MDLLCTL2_REG

#define DATA5CH1_CR_MDLLCTL3_REG                                       (0x000016BC)
//Duplicate of DATA0CH0_CR_MDLLCTL3_REG

#define DATA5CH1_CR_MDLLCTL4_REG                                       (0x000016C0)
//Duplicate of DATA0CH0_CR_MDLLCTL4_REG

#define DATA5CH1_CR_MDLLCTL5_REG                                       (0x000016C4)
//Duplicate of DATA0CH0_CR_MDLLCTL4_REG

#define DATA5CH1_CR_DIGMISCCTRL_REG                                    (0x000016C4)
//Duplicate of DATA0CH0_CR_DIGMISCCTRL_REG

#define DATA5CH1_CR_AFEMISCCTRL2_REG                                   (0x000016C8)
//Duplicate of DATA0CH0_CR_AFEMISCCTRL2_REG

#define DATA5CH1_CR_TXPBDOFFSET0_REG                                   (0x000016CC)
//Duplicate of DATA0CH0_CR_TXPBDOFFSET0_REG

#define DATA5CH1_CR_TXPBDOFFSET1_REG                                   (0x000016D0)
//Duplicate of DATA0CH0_CR_TXPBDOFFSET1_REG

#define DATA5CH1_CR_COMPCTL0_REG                                       (0x000016D4)
//Duplicate of DATA0CH0_CR_COMPCTL0_REG

#define DATA5CH1_CR_COMPCTL1_REG                                       (0x000016D8)
//Duplicate of DATA0CH0_CR_COMPCTL1_REG

#define DATA5CH1_CR_COMPCTL2_REG                                       (0x000016DC)
//Duplicate of DATA0CH0_CR_COMPCTL2_REG

#define DATA5CH1_CR_DCCCTL5_REG                                        (0x000016E0)
//Duplicate of DATA0CH0_CR_DCCCTL5_REG

#define DATA5CH1_CR_DCCCTL6_REG                                        (0x000016E4)
//Duplicate of DATA0CH0_CR_DCCCTL6_REG

#define DATA5CH1_CR_DCCCTL7_REG                                        (0x000016E8)
//Duplicate of DATA0CH0_CR_DCCCTL7_REG

#define DATA5CH1_CR_DCCCTL8_REG                                        (0x000016EC)
//Duplicate of DATA0CH0_CR_DCCCTL8_REG

#define DATA5CH1_CR_DCCCTL9_REG                                        (0x000016F0)
//Duplicate of DATA0CH0_CR_DCCCTL9_REG

#define DATA5CH1_CR_RXCONTROL0RANK0_REG                                (0x000016F4)
//Duplicate of DATA0CH0_CR_RXCONTROL0RANK0_REG

#define DATA5CH1_CR_RXCONTROL0RANK1_REG                                (0x000016F8)
//Duplicate of DATA0CH0_CR_RXCONTROL0RANK0_REG

#define DATA5CH1_CR_RXCONTROL0RANK2_REG                                (0x000016FC)
//Duplicate of DATA0CH0_CR_RXCONTROL0RANK0_REG

#define DATA5CH1_CR_RXCONTROL0RANK3_REG                                (0x00001700)
//Duplicate of DATA0CH0_CR_RXCONTROL0RANK0_REG

#define DATA5CH1_CR_TXCONTROL0_PH90_REG                                (0x00001704)
//Duplicate of DATA0CH0_CR_TXCONTROL0_PH90_REG

#define DATA5CH1_CR_TXCONTROL0RANK0_REG                                (0x00001708)
//Duplicate of DATA0CH0_CR_TXCONTROL0RANK0_REG

#define DATA5CH1_CR_TXCONTROL0RANK1_REG                                (0x0000170C)
//Duplicate of DATA0CH0_CR_TXCONTROL0RANK0_REG

#define DATA5CH1_CR_TXCONTROL0RANK2_REG                                (0x00001710)
//Duplicate of DATA0CH0_CR_TXCONTROL0RANK0_REG

#define DATA5CH1_CR_TXCONTROL0RANK3_REG                                (0x00001714)
//Duplicate of DATA0CH0_CR_TXCONTROL0RANK0_REG

#define DATA5CH1_CR_DDRDATADQSRANKX_REG                                (0x00001718)
//Duplicate of DATA0CH0_CR_DDRDATADQSRANKX_REG

#define DATA5CH1_CR_DDRDATADQSPH90RANKX_REG                            (0x0000171C)
//Duplicate of DATA0CH0_CR_DDRDATADQSPH90RANKX_REG

#define DATA5CH1_CR_DDRCRDATAOFFSETCOMP_REG                            (0x00001720)
//Duplicate of DATA0CH0_CR_DDRCRDATAOFFSETCOMP_REG

#define DATA5CH1_CR_DDRCRDATAOFFSETTRAIN_REG                           (0x00001724)
//Duplicate of DATA0CH0_CR_DDRCRDATAOFFSETTRAIN_REG

#define DATA5CH1_CR_DDRCRCMDBUSTRAIN_REG                               (0x00001728)
//Duplicate of DATA0CH0_CR_DDRCRCMDBUSTRAIN_REG

#define DATA5CH1_CR_DDRCRWRRETRAINSWIZZLECONTROL_REG                   (0x0000172C)
//Duplicate of DATA0CH0_CR_DDRCRWRRETRAINSWIZZLECONTROL_REG

#define DATA5CH1_CR_DDRCRWRRETRAINRANK3_REG                            (0x00001730)
//Duplicate of DATA0CH0_CR_DDRCRWRRETRAINRANK3_REG

#define DATA5CH1_CR_DDRCRWRRETRAINRANK2_REG                            (0x00001734)
//Duplicate of DATA0CH0_CR_DDRCRWRRETRAINRANK3_REG

#define DATA5CH1_CR_DDRCRWRRETRAINRANK1_REG                            (0x00001738)
//Duplicate of DATA0CH0_CR_DDRCRWRRETRAINRANK3_REG

#define DATA5CH1_CR_DDRCRWRRETRAINRANK0_REG                            (0x0000173C)
//Duplicate of DATA0CH0_CR_DDRCRWRRETRAINRANK3_REG

#define DATA5CH1_CR_DDRCRWRRETRAINCONTROLSTATUS_REG                    (0x00001740)
//Duplicate of DATA0CH0_CR_DDRCRWRRETRAINCONTROLSTATUS_REG

#define DATA5CH1_CR_TXCONTROL1_PH90_REG                                (0x00001744)
//Duplicate of DATA0CH0_CR_TXCONTROL1_PH90_REG

#define DATA5CH1_CR_AFEMISCCTRL1_REG                                   (0x00001748)
//Duplicate of DATA0CH0_CR_AFEMISCCTRL1_REG

#define DATA5CH1_CR_DQXRXAMPCTL_REG                                    (0x0000174C)
//Duplicate of DATA0CH0_CR_DQXRXAMPCTL_REG

#define DATA5CH1_CR_DQ0RXAMPCTL_REG                                    (0x00001750)
//Duplicate of DATA0CH0_CR_DQ0RXAMPCTL_REG

#define DATA5CH1_CR_DQ1RXAMPCTL_REG                                    (0x00001754)
//Duplicate of DATA0CH0_CR_DQ1RXAMPCTL_REG

#define DATA5CH1_CR_DQ2RXAMPCTL_REG                                    (0x00001758)
//Duplicate of DATA0CH0_CR_DQ2RXAMPCTL_REG

#define DATA5CH1_CR_DQ3RXAMPCTL_REG                                    (0x0000175C)
//Duplicate of DATA0CH0_CR_DQ3RXAMPCTL_REG

#define DATA5CH1_CR_DQ4RXAMPCTL_REG                                    (0x00001760)
//Duplicate of DATA0CH0_CR_DQ4RXAMPCTL_REG

#define DATA5CH1_CR_DQ5RXAMPCTL_REG                                    (0x00001764)
//Duplicate of DATA0CH0_CR_DQ5RXAMPCTL_REG

#define DATA5CH1_CR_DQ6RXAMPCTL_REG                                    (0x00001768)
//Duplicate of DATA0CH0_CR_DQ6RXAMPCTL_REG

#define DATA5CH1_CR_DQ7RXAMPCTL_REG                                    (0x0000176C)
//Duplicate of DATA0CH0_CR_DQ7RXAMPCTL_REG

#define DATA5CH1_CR_DQRXAMPCTL0_REG                                    (0x00001770)
//Duplicate of DATA0CH0_CR_DQRXAMPCTL0_REG

#define DATA5CH1_CR_DQRXAMPCTL1_REG                                    (0x00001774)
//Duplicate of DATA0CH0_CR_DQRXAMPCTL1_REG

#define DATA5CH1_CR_DQRXAMPCTL2_REG                                    (0x00001778)
//Duplicate of DATA0CH0_CR_DQRXAMPCTL2_REG

#define DATA5CH1_CR_DQTXCTL0_REG                                       (0x0000177C)
//Duplicate of DATA0CH0_CR_DQTXCTL0_REG

#define DATA5CH1_CR_DCCCTL0_REG                                        (0x00001780)
//Duplicate of DATA0CH0_CR_DCCCTL0_REG

#define DATA5CH1_CR_DCCCTL1_REG                                        (0x00001784)
//Duplicate of DATA0CH0_CR_DCCCTL1_REG

#define DATA5CH1_CR_DCCCTL2_REG                                        (0x00001788)
//Duplicate of DATA0CH0_CR_DCCCTL2_REG

#define DATA5CH1_CR_DCCCTL3_REG                                        (0x0000178C)
//Duplicate of DATA0CH0_CR_DCCCTL3_REG

#define DATA5CH1_CR_DCCCTL10_REG                                       (0x00001790)
//Duplicate of DATA0CH0_CR_DCCCTL10_REG

#define DATA5CH1_CR_DCCCTL12_REG                                       (0x00001794)
//Duplicate of DATA0CH0_CR_DCCCTL12_REG

#define DATA5CH1_CR_CLKALIGNCTL0_REG                                   (0x00001798)
//Duplicate of DATA0CH0_CR_CLKALIGNCTL0_REG

#define DATA5CH1_CR_CLKALIGNCTL1_REG                                   (0x0000179C)
//Duplicate of DATA0CH0_CR_CLKALIGNCTL1_REG

#define DATA5CH1_CR_CLKALIGNCTL2_REG                                   (0x000017A0)
//Duplicate of DATA0CH0_CR_CLKALIGNCTL2_REG

#define DATA5CH1_CR_DCCCTL4_REG                                        (0x000017A4)
//Duplicate of DATA0CH0_CR_DCCCTL4_REG

#define DATA5CH1_CR_DDRCRDATACOMPVTT_REG                               (0x000017A8)
//Duplicate of DATA0CH0_CR_DDRCRDATACOMPVTT_REG

#define DATA5CH1_CR_WLCTRL_REG                                         (0x000017AC)
//Duplicate of DATA0CH0_CR_WLCTRL_REG

#define DATA5CH1_CR_LPMODECTL_REG                                      (0x000017B0)
//Duplicate of DATA0CH0_CR_LPMODECTL_REG

#define DATA5CH1_CR_DDRCRDATACOMP0_REG                                 (0x000017B4)
//Duplicate of DATA0CH0_CR_DDRCRDATACOMP0_REG

#define DATA5CH1_CR_DDRCRDATACOMP1_REG                                 (0x000017B8)
//Duplicate of DATA0CH0_CR_DDRCRDATACOMP1_REG

#define DATA5CH1_CR_VCCDLLCOMPDATA_REG                                 (0x000017BC)
//Duplicate of DATA0CH0_CR_VCCDLLCOMPDATA_REG

#define DATA5CH1_CR_DCSOFFSET_REG                                      (0x000017C0)
//Duplicate of DATA0CH0_CR_DCSOFFSET_REG

#define DATA5CH1_CR_DFXCCMON_REG                                       (0x000017C4)
//Duplicate of DATA0CH0_CR_DFXCCMON_REG

#define DATA5CH1_CR_AFEMISCCTRL0_REG                                   (0x000017C8)
//Duplicate of DATA0CH0_CR_AFEMISCCTRL0_REG

#define DATA5CH1_CR_VIEWCTL_REG                                        (0x000017CC)
//Duplicate of DATA0CH0_CR_VIEWCTL_REG

#define DATA5CH1_CR_DCCCTL11_REG                                       (0x000017D0)
//Duplicate of DATA0CH0_CR_DCCCTL11_REG

#define DATA5CH1_CR_DCCCTL13_REG                                       (0x000017D4)
//Duplicate of DATA0CH0_CR_DCCCTL13_REG

#define DATA5CH1_CR_PMFSM_REG                                          (0x000017D8)
//Duplicate of DATA0CH0_CR_PMFSM_REG

#define DATA5CH1_CR_PMFSM1_REG                                         (0x000017DC)
//Duplicate of DATA0CH0_CR_PMFSM1_REG

#define DATA5CH1_CR_CLKALIGNSTATUS_REG                                 (0x000017E0)
//Duplicate of DATA0CH0_CR_CLKALIGNSTATUS_REG

#define DATA5CH1_CR_DATATRAINFEEDBACK_REG                              (0x000017E4)
//Duplicate of DATA0CH0_CR_DATATRAINFEEDBACK_REG

#define DATA5CH1_CR_DDRCRMARGINMODECONTROL0_REG                        (0x000017E8)
//Duplicate of DATA0CH0_CR_DDRCRMARGINMODECONTROL0_REG

#define DATA5CH1_CR_DDRCRMARGINMODEDEBUGMSB0_REG                       (0x000017EC)
//Duplicate of DATA0CH0_CR_DDRCRMARGINMODEDEBUGMSB0_REG

#define DATA5CH1_CR_DDRCRMARGINMODEDEBUGLSB0_REG                       (0x000017F0)
//Duplicate of DATA0CH0_CR_DDRCRMARGINMODEDEBUGMSB0_REG

#define DATA5CH1_CR_DCCCTL14_REG                                       (0x000017F4)
//Duplicate of DATA0CH0_CR_DCCCTL14_REG

#define DATA6CH0_CR_DDRDATADQRANK0LANE0_REG                            (0x00001800)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA6CH0_CR_DDRDATADQRANK0LANE1_REG                            (0x00001804)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA6CH0_CR_DDRDATADQRANK0LANE2_REG                            (0x00001808)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA6CH0_CR_DDRDATADQRANK0LANE3_REG                            (0x0000180C)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA6CH0_CR_DDRDATADQRANK0LANE4_REG                            (0x00001810)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA6CH0_CR_DDRDATADQRANK0LANE5_REG                            (0x00001814)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA6CH0_CR_DDRDATADQRANK0LANE6_REG                            (0x00001818)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA6CH0_CR_DDRDATADQRANK0LANE7_REG                            (0x0000181C)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA6CH0_CR_DDRDATADQRANK1LANE0_REG                            (0x00001820)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA6CH0_CR_DDRDATADQRANK1LANE1_REG                            (0x00001824)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA6CH0_CR_DDRDATADQRANK1LANE2_REG                            (0x00001828)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA6CH0_CR_DDRDATADQRANK1LANE3_REG                            (0x0000182C)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA6CH0_CR_DDRDATADQRANK1LANE4_REG                            (0x00001830)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA6CH0_CR_DDRDATADQRANK1LANE5_REG                            (0x00001834)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA6CH0_CR_DDRDATADQRANK1LANE6_REG                            (0x00001838)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA6CH0_CR_DDRDATADQRANK1LANE7_REG                            (0x0000183C)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA6CH0_CR_DDRDATADQRANK2LANE0_REG                            (0x00001840)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA6CH0_CR_DDRDATADQRANK2LANE1_REG                            (0x00001844)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA6CH0_CR_DDRDATADQRANK2LANE2_REG                            (0x00001848)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA6CH0_CR_DDRDATADQRANK2LANE3_REG                            (0x0000184C)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA6CH0_CR_DDRDATADQRANK2LANE4_REG                            (0x00001850)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA6CH0_CR_DDRDATADQRANK2LANE5_REG                            (0x00001854)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA6CH0_CR_DDRDATADQRANK2LANE6_REG                            (0x00001858)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA6CH0_CR_DDRDATADQRANK2LANE7_REG                            (0x0000185C)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA6CH0_CR_DDRDATADQRANK3LANE0_REG                            (0x00001860)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA6CH0_CR_DDRDATADQRANK3LANE1_REG                            (0x00001864)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA6CH0_CR_DDRDATADQRANK3LANE2_REG                            (0x00001868)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA6CH0_CR_DDRDATADQRANK3LANE3_REG                            (0x0000186C)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA6CH0_CR_DDRDATADQRANK3LANE4_REG                            (0x00001870)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA6CH0_CR_DDRDATADQRANK3LANE5_REG                            (0x00001874)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA6CH0_CR_DDRDATADQRANK3LANE6_REG                            (0x00001878)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA6CH0_CR_DDRDATADQRANK3LANE7_REG                            (0x0000187C)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA6CH0_CR_DDRCRDATACONTROL0_REG                              (0x00001880)
//Duplicate of DATA0CH0_CR_DDRCRDATACONTROL0_REG

#define DATA6CH0_CR_DDRCRDATACONTROL1_REG                              (0x00001884)
//Duplicate of DATA0CH0_CR_DDRCRDATACONTROL1_REG

#define DATA6CH0_CR_DDRCRDATACONTROL2_REG                              (0x00001888)
//Duplicate of DATA0CH0_CR_DDRCRDATACONTROL2_REG

#define DATA6CH0_CR_DDRCRDATACONTROL3_REG                              (0x0000188C)
//Duplicate of DATA0CH0_CR_DDRCRDATACONTROL3_REG

#define DATA6CH0_CR_AFEMISC_REG                                        (0x00001890)
//Duplicate of DATA0CH0_CR_AFEMISC_REG

#define DATA6CH0_CR_SRZCTL_REG                                         (0x00001894)
//Duplicate of DATA0CH0_CR_SRZCTL_REG

#define DATA6CH0_CR_DQRXCTL0_REG                                       (0x00001898)
//Duplicate of DATA0CH0_CR_DQRXCTL0_REG

#define DATA6CH0_CR_DQRXCTL1_REG                                       (0x0000189C)
//Duplicate of DATA0CH0_CR_DQRXCTL1_REG

#define DATA6CH0_CR_DQSTXRXCTL_REG                                     (0x000018A0)
//Duplicate of DATA0CH0_CR_DQSTXRXCTL_REG

#define DATA6CH0_CR_DQSTXRXCTL0_REG                                    (0x000018A4)
//Duplicate of DATA0CH0_CR_DQSTXRXCTL0_REG

#define DATA6CH0_CR_SDLCTL1_REG                                        (0x000018A8)
//Duplicate of DATA0CH0_CR_SDLCTL1_REG

#define DATA6CH0_CR_SDLCTL0_REG                                        (0x000018AC)
//Duplicate of DATA0CH0_CR_SDLCTL0_REG

#define DATA6CH0_CR_MDLLCTL0_REG                                       (0x000018B0)
//Duplicate of DATA0CH0_CR_MDLLCTL0_REG

#define DATA6CH0_CR_MDLLCTL1_REG                                       (0x000018B4)
//Duplicate of DATA0CH0_CR_MDLLCTL1_REG

#define DATA6CH0_CR_MDLLCTL2_REG                                       (0x000018B8)
//Duplicate of DATA0CH0_CR_MDLLCTL2_REG

#define DATA6CH0_CR_MDLLCTL3_REG                                       (0x000018BC)
//Duplicate of DATA0CH0_CR_MDLLCTL3_REG

#define DATA6CH0_CR_MDLLCTL4_REG                                       (0x000018C0)
//Duplicate of DATA0CH0_CR_MDLLCTL4_REG

#define DATA6CH0_CR_MDLLCTL5_REG                                       (0x000018C4)
//Duplicate of DATA0CH0_CR_MDLLCTL4_REG

#define DATA6CH0_CR_DIGMISCCTRL_REG                                    (0x000018C4)
//Duplicate of DATA0CH0_CR_DIGMISCCTRL_REG

#define DATA6CH0_CR_AFEMISCCTRL2_REG                                   (0x000018C8)
//Duplicate of DATA0CH0_CR_AFEMISCCTRL2_REG

#define DATA6CH0_CR_TXPBDOFFSET0_REG                                   (0x000018CC)
//Duplicate of DATA0CH0_CR_TXPBDOFFSET0_REG

#define DATA6CH0_CR_TXPBDOFFSET1_REG                                   (0x000018D0)
//Duplicate of DATA0CH0_CR_TXPBDOFFSET1_REG

#define DATA6CH0_CR_COMPCTL0_REG                                       (0x000018D4)
//Duplicate of DATA0CH0_CR_COMPCTL0_REG

#define DATA6CH0_CR_COMPCTL1_REG                                       (0x000018D8)
//Duplicate of DATA0CH0_CR_COMPCTL1_REG

#define DATA6CH0_CR_COMPCTL2_REG                                       (0x000018DC)
//Duplicate of DATA0CH0_CR_COMPCTL2_REG

#define DATA6CH0_CR_DCCCTL5_REG                                        (0x000018E0)
//Duplicate of DATA0CH0_CR_DCCCTL5_REG

#define DATA6CH0_CR_DCCCTL6_REG                                        (0x000018E4)
//Duplicate of DATA0CH0_CR_DCCCTL6_REG

#define DATA6CH0_CR_DCCCTL7_REG                                        (0x000018E8)
//Duplicate of DATA0CH0_CR_DCCCTL7_REG

#define DATA6CH0_CR_DCCCTL8_REG                                        (0x000018EC)
//Duplicate of DATA0CH0_CR_DCCCTL8_REG

#define DATA6CH0_CR_DCCCTL9_REG                                        (0x000018F0)
//Duplicate of DATA0CH0_CR_DCCCTL9_REG

#define DATA6CH0_CR_RXCONTROL0RANK0_REG                                (0x000018F4)
//Duplicate of DATA0CH0_CR_RXCONTROL0RANK0_REG

#define DATA6CH0_CR_RXCONTROL0RANK1_REG                                (0x000018F8)
//Duplicate of DATA0CH0_CR_RXCONTROL0RANK0_REG

#define DATA6CH0_CR_RXCONTROL0RANK2_REG                                (0x000018FC)
//Duplicate of DATA0CH0_CR_RXCONTROL0RANK0_REG

#define DATA6CH0_CR_RXCONTROL0RANK3_REG                                (0x00001900)
//Duplicate of DATA0CH0_CR_RXCONTROL0RANK0_REG

#define DATA6CH0_CR_TXCONTROL0_PH90_REG                                (0x00001904)
//Duplicate of DATA0CH0_CR_TXCONTROL0_PH90_REG

#define DATA6CH0_CR_TXCONTROL0RANK0_REG                                (0x00001908)
//Duplicate of DATA0CH0_CR_TXCONTROL0RANK0_REG

#define DATA6CH0_CR_TXCONTROL0RANK1_REG                                (0x0000190C)
//Duplicate of DATA0CH0_CR_TXCONTROL0RANK0_REG

#define DATA6CH0_CR_TXCONTROL0RANK2_REG                                (0x00001910)
//Duplicate of DATA0CH0_CR_TXCONTROL0RANK0_REG

#define DATA6CH0_CR_TXCONTROL0RANK3_REG                                (0x00001914)
//Duplicate of DATA0CH0_CR_TXCONTROL0RANK0_REG

#define DATA6CH0_CR_DDRDATADQSRANKX_REG                                (0x00001918)
//Duplicate of DATA0CH0_CR_DDRDATADQSRANKX_REG

#define DATA6CH0_CR_DDRDATADQSPH90RANKX_REG                            (0x0000191C)
//Duplicate of DATA0CH0_CR_DDRDATADQSPH90RANKX_REG

#define DATA6CH0_CR_DDRCRDATAOFFSETCOMP_REG                            (0x00001920)
//Duplicate of DATA0CH0_CR_DDRCRDATAOFFSETCOMP_REG

#define DATA6CH0_CR_DDRCRDATAOFFSETTRAIN_REG                           (0x00001924)
//Duplicate of DATA0CH0_CR_DDRCRDATAOFFSETTRAIN_REG

#define DATA6CH0_CR_DDRCRCMDBUSTRAIN_REG                               (0x00001928)
//Duplicate of DATA0CH0_CR_DDRCRCMDBUSTRAIN_REG

#define DATA6CH0_CR_DDRCRWRRETRAINSWIZZLECONTROL_REG                   (0x0000192C)
//Duplicate of DATA0CH0_CR_DDRCRWRRETRAINSWIZZLECONTROL_REG

#define DATA6CH0_CR_DDRCRWRRETRAINRANK3_REG                            (0x00001930)
//Duplicate of DATA0CH0_CR_DDRCRWRRETRAINRANK3_REG

#define DATA6CH0_CR_DDRCRWRRETRAINRANK2_REG                            (0x00001934)
//Duplicate of DATA0CH0_CR_DDRCRWRRETRAINRANK3_REG

#define DATA6CH0_CR_DDRCRWRRETRAINRANK1_REG                            (0x00001938)
//Duplicate of DATA0CH0_CR_DDRCRWRRETRAINRANK3_REG

#define DATA6CH0_CR_DDRCRWRRETRAINRANK0_REG                            (0x0000193C)
//Duplicate of DATA0CH0_CR_DDRCRWRRETRAINRANK3_REG

#define DATA6CH0_CR_DDRCRWRRETRAINCONTROLSTATUS_REG                    (0x00001940)
//Duplicate of DATA0CH0_CR_DDRCRWRRETRAINCONTROLSTATUS_REG

#define DATA6CH0_CR_TXCONTROL1_PH90_REG                                (0x00001944)
//Duplicate of DATA0CH0_CR_TXCONTROL1_PH90_REG

#define DATA6CH0_CR_AFEMISCCTRL1_REG                                   (0x00001948)
//Duplicate of DATA0CH0_CR_AFEMISCCTRL1_REG

#define DATA6CH0_CR_DQXRXAMPCTL_REG                                    (0x0000194C)
//Duplicate of DATA0CH0_CR_DQXRXAMPCTL_REG

#define DATA6CH0_CR_DQ0RXAMPCTL_REG                                    (0x00001950)
//Duplicate of DATA0CH0_CR_DQ0RXAMPCTL_REG

#define DATA6CH0_CR_DQ1RXAMPCTL_REG                                    (0x00001954)
//Duplicate of DATA0CH0_CR_DQ1RXAMPCTL_REG

#define DATA6CH0_CR_DQ2RXAMPCTL_REG                                    (0x00001958)
//Duplicate of DATA0CH0_CR_DQ2RXAMPCTL_REG

#define DATA6CH0_CR_DQ3RXAMPCTL_REG                                    (0x0000195C)
//Duplicate of DATA0CH0_CR_DQ3RXAMPCTL_REG

#define DATA6CH0_CR_DQ4RXAMPCTL_REG                                    (0x00001960)
//Duplicate of DATA0CH0_CR_DQ4RXAMPCTL_REG

#define DATA6CH0_CR_DQ5RXAMPCTL_REG                                    (0x00001964)
//Duplicate of DATA0CH0_CR_DQ5RXAMPCTL_REG

#define DATA6CH0_CR_DQ6RXAMPCTL_REG                                    (0x00001968)
//Duplicate of DATA0CH0_CR_DQ6RXAMPCTL_REG

#define DATA6CH0_CR_DQ7RXAMPCTL_REG                                    (0x0000196C)
//Duplicate of DATA0CH0_CR_DQ7RXAMPCTL_REG

#define DATA6CH0_CR_DQRXAMPCTL0_REG                                    (0x00001970)
//Duplicate of DATA0CH0_CR_DQRXAMPCTL0_REG

#define DATA6CH0_CR_DQRXAMPCTL1_REG                                    (0x00001974)
//Duplicate of DATA0CH0_CR_DQRXAMPCTL1_REG

#define DATA6CH0_CR_DQRXAMPCTL2_REG                                    (0x00001978)
//Duplicate of DATA0CH0_CR_DQRXAMPCTL2_REG

#define DATA6CH0_CR_DQTXCTL0_REG                                       (0x0000197C)
//Duplicate of DATA0CH0_CR_DQTXCTL0_REG

#define DATA6CH0_CR_DCCCTL0_REG                                        (0x00001980)
//Duplicate of DATA0CH0_CR_DCCCTL0_REG

#define DATA6CH0_CR_DCCCTL1_REG                                        (0x00001984)
//Duplicate of DATA0CH0_CR_DCCCTL1_REG

#define DATA6CH0_CR_DCCCTL2_REG                                        (0x00001988)
//Duplicate of DATA0CH0_CR_DCCCTL2_REG

#define DATA6CH0_CR_DCCCTL3_REG                                        (0x0000198C)
//Duplicate of DATA0CH0_CR_DCCCTL3_REG

#define DATA6CH0_CR_DCCCTL10_REG                                       (0x00001990)
//Duplicate of DATA0CH0_CR_DCCCTL10_REG

#define DATA6CH0_CR_DCCCTL12_REG                                       (0x00001994)
//Duplicate of DATA0CH0_CR_DCCCTL12_REG

#define DATA6CH0_CR_CLKALIGNCTL0_REG                                   (0x00001998)
//Duplicate of DATA0CH0_CR_CLKALIGNCTL0_REG

#define DATA6CH0_CR_CLKALIGNCTL1_REG                                   (0x0000199C)
//Duplicate of DATA0CH0_CR_CLKALIGNCTL1_REG

#define DATA6CH0_CR_CLKALIGNCTL2_REG                                   (0x000019A0)
//Duplicate of DATA0CH0_CR_CLKALIGNCTL2_REG

#define DATA6CH0_CR_DCCCTL4_REG                                        (0x000019A4)
//Duplicate of DATA0CH0_CR_DCCCTL4_REG

#define DATA6CH0_CR_DDRCRDATACOMPVTT_REG                               (0x000019A8)
//Duplicate of DATA0CH0_CR_DDRCRDATACOMPVTT_REG

#define DATA6CH0_CR_WLCTRL_REG                                         (0x000019AC)
//Duplicate of DATA0CH0_CR_WLCTRL_REG

#define DATA6CH0_CR_LPMODECTL_REG                                      (0x000019B0)
//Duplicate of DATA0CH0_CR_LPMODECTL_REG

#define DATA6CH0_CR_DDRCRDATACOMP0_REG                                 (0x000019B4)
//Duplicate of DATA0CH0_CR_DDRCRDATACOMP0_REG

#define DATA6CH0_CR_DDRCRDATACOMP1_REG                                 (0x000019B8)
//Duplicate of DATA0CH0_CR_DDRCRDATACOMP1_REG

#define DATA6CH0_CR_VCCDLLCOMPDATA_REG                                 (0x000019BC)
//Duplicate of DATA0CH0_CR_VCCDLLCOMPDATA_REG

#define DATA6CH0_CR_DCSOFFSET_REG                                      (0x000019C0)
//Duplicate of DATA0CH0_CR_DCSOFFSET_REG

#define DATA6CH0_CR_DFXCCMON_REG                                       (0x000019C4)
//Duplicate of DATA0CH0_CR_DFXCCMON_REG

#define DATA6CH0_CR_AFEMISCCTRL0_REG                                   (0x000019C8)
//Duplicate of DATA0CH0_CR_AFEMISCCTRL0_REG

#define DATA6CH0_CR_VIEWCTL_REG                                        (0x000019CC)
//Duplicate of DATA0CH0_CR_VIEWCTL_REG

#define DATA6CH0_CR_DCCCTL11_REG                                       (0x000019D0)
//Duplicate of DATA0CH0_CR_DCCCTL11_REG

#define DATA6CH0_CR_DCCCTL13_REG                                       (0x000019D4)
//Duplicate of DATA0CH0_CR_DCCCTL13_REG

#define DATA6CH0_CR_PMFSM_REG                                          (0x000019D8)
//Duplicate of DATA0CH0_CR_PMFSM_REG

#define DATA6CH0_CR_PMFSM1_REG                                         (0x000019DC)
//Duplicate of DATA0CH0_CR_PMFSM1_REG

#define DATA6CH0_CR_CLKALIGNSTATUS_REG                                 (0x000019E0)
//Duplicate of DATA0CH0_CR_CLKALIGNSTATUS_REG

#define DATA6CH0_CR_DATATRAINFEEDBACK_REG                              (0x000019E4)
//Duplicate of DATA0CH0_CR_DATATRAINFEEDBACK_REG

#define DATA6CH0_CR_DDRCRMARGINMODECONTROL0_REG                        (0x000019E8)
//Duplicate of DATA0CH0_CR_DDRCRMARGINMODECONTROL0_REG

#define DATA6CH0_CR_DDRCRMARGINMODEDEBUGMSB0_REG                       (0x000019EC)
//Duplicate of DATA0CH0_CR_DDRCRMARGINMODEDEBUGMSB0_REG

#define DATA6CH0_CR_DDRCRMARGINMODEDEBUGLSB0_REG                       (0x000019F0)
//Duplicate of DATA0CH0_CR_DDRCRMARGINMODEDEBUGMSB0_REG

#define DATA6CH0_CR_DCCCTL14_REG                                       (0x000019F4)
//Duplicate of DATA0CH0_CR_DCCCTL14_REG

#define DATA6CH1_CR_DDRDATADQRANK0LANE0_REG                            (0x00001A00)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA6CH1_CR_DDRDATADQRANK0LANE1_REG                            (0x00001A04)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA6CH1_CR_DDRDATADQRANK0LANE2_REG                            (0x00001A08)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA6CH1_CR_DDRDATADQRANK0LANE3_REG                            (0x00001A0C)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA6CH1_CR_DDRDATADQRANK0LANE4_REG                            (0x00001A10)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA6CH1_CR_DDRDATADQRANK0LANE5_REG                            (0x00001A14)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA6CH1_CR_DDRDATADQRANK0LANE6_REG                            (0x00001A18)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA6CH1_CR_DDRDATADQRANK0LANE7_REG                            (0x00001A1C)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA6CH1_CR_DDRDATADQRANK1LANE0_REG                            (0x00001A20)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA6CH1_CR_DDRDATADQRANK1LANE1_REG                            (0x00001A24)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA6CH1_CR_DDRDATADQRANK1LANE2_REG                            (0x00001A28)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA6CH1_CR_DDRDATADQRANK1LANE3_REG                            (0x00001A2C)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA6CH1_CR_DDRDATADQRANK1LANE4_REG                            (0x00001A30)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA6CH1_CR_DDRDATADQRANK1LANE5_REG                            (0x00001A34)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA6CH1_CR_DDRDATADQRANK1LANE6_REG                            (0x00001A38)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA6CH1_CR_DDRDATADQRANK1LANE7_REG                            (0x00001A3C)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA6CH1_CR_DDRDATADQRANK2LANE0_REG                            (0x00001A40)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA6CH1_CR_DDRDATADQRANK2LANE1_REG                            (0x00001A44)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA6CH1_CR_DDRDATADQRANK2LANE2_REG                            (0x00001A48)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA6CH1_CR_DDRDATADQRANK2LANE3_REG                            (0x00001A4C)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA6CH1_CR_DDRDATADQRANK2LANE4_REG                            (0x00001A50)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA6CH1_CR_DDRDATADQRANK2LANE5_REG                            (0x00001A54)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA6CH1_CR_DDRDATADQRANK2LANE6_REG                            (0x00001A58)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA6CH1_CR_DDRDATADQRANK2LANE7_REG                            (0x00001A5C)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA6CH1_CR_DDRDATADQRANK3LANE0_REG                            (0x00001A60)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA6CH1_CR_DDRDATADQRANK3LANE1_REG                            (0x00001A64)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA6CH1_CR_DDRDATADQRANK3LANE2_REG                            (0x00001A68)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA6CH1_CR_DDRDATADQRANK3LANE3_REG                            (0x00001A6C)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA6CH1_CR_DDRDATADQRANK3LANE4_REG                            (0x00001A70)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA6CH1_CR_DDRDATADQRANK3LANE5_REG                            (0x00001A74)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA6CH1_CR_DDRDATADQRANK3LANE6_REG                            (0x00001A78)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA6CH1_CR_DDRDATADQRANK3LANE7_REG                            (0x00001A7C)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA6CH1_CR_DDRCRDATACONTROL0_REG                              (0x00001A80)
//Duplicate of DATA0CH0_CR_DDRCRDATACONTROL0_REG

#define DATA6CH1_CR_DDRCRDATACONTROL1_REG                              (0x00001A84)
//Duplicate of DATA0CH0_CR_DDRCRDATACONTROL1_REG

#define DATA6CH1_CR_DDRCRDATACONTROL2_REG                              (0x00001A88)
//Duplicate of DATA0CH0_CR_DDRCRDATACONTROL2_REG

#define DATA6CH1_CR_DDRCRDATACONTROL3_REG                              (0x00001A8C)
//Duplicate of DATA0CH0_CR_DDRCRDATACONTROL3_REG

#define DATA6CH1_CR_AFEMISC_REG                                        (0x00001A90)
//Duplicate of DATA0CH0_CR_AFEMISC_REG

#define DATA6CH1_CR_SRZCTL_REG                                         (0x00001A94)
//Duplicate of DATA0CH0_CR_SRZCTL_REG

#define DATA6CH1_CR_DQRXCTL0_REG                                       (0x00001A98)
//Duplicate of DATA0CH0_CR_DQRXCTL0_REG

#define DATA6CH1_CR_DQRXCTL1_REG                                       (0x00001A9C)
//Duplicate of DATA0CH0_CR_DQRXCTL1_REG

#define DATA6CH1_CR_DQSTXRXCTL_REG                                     (0x00001AA0)
//Duplicate of DATA0CH0_CR_DQSTXRXCTL_REG

#define DATA6CH1_CR_DQSTXRXCTL0_REG                                    (0x00001AA4)
//Duplicate of DATA0CH0_CR_DQSTXRXCTL0_REG

#define DATA6CH1_CR_SDLCTL1_REG                                        (0x00001AA8)
//Duplicate of DATA0CH0_CR_SDLCTL1_REG

#define DATA6CH1_CR_SDLCTL0_REG                                        (0x00001AAC)
//Duplicate of DATA0CH0_CR_SDLCTL0_REG

#define DATA6CH1_CR_MDLLCTL0_REG                                       (0x00001AB0)
//Duplicate of DATA0CH0_CR_MDLLCTL0_REG

#define DATA6CH1_CR_MDLLCTL1_REG                                       (0x00001AB4)
//Duplicate of DATA0CH0_CR_MDLLCTL1_REG

#define DATA6CH1_CR_MDLLCTL2_REG                                       (0x00001AB8)
//Duplicate of DATA0CH0_CR_MDLLCTL2_REG

#define DATA6CH1_CR_MDLLCTL3_REG                                       (0x00001ABC)
//Duplicate of DATA0CH0_CR_MDLLCTL3_REG

#define DATA6CH1_CR_MDLLCTL4_REG                                       (0x00001AC0)
//Duplicate of DATA0CH0_CR_MDLLCTL4_REG

#define DATA6CH1_CR_MDLLCTL5_REG                                       (0x00001AC4)
//Duplicate of DATA0CH0_CR_MDLLCTL4_REG

#define DATA6CH1_CR_DIGMISCCTRL_REG                                    (0x00001AC4)
//Duplicate of DATA0CH0_CR_DIGMISCCTRL_REG

#define DATA6CH1_CR_AFEMISCCTRL2_REG                                   (0x00001AC8)
//Duplicate of DATA0CH0_CR_AFEMISCCTRL2_REG

#define DATA6CH1_CR_TXPBDOFFSET0_REG                                   (0x00001ACC)
//Duplicate of DATA0CH0_CR_TXPBDOFFSET0_REG

#define DATA6CH1_CR_TXPBDOFFSET1_REG                                   (0x00001AD0)
//Duplicate of DATA0CH0_CR_TXPBDOFFSET1_REG

#define DATA6CH1_CR_COMPCTL0_REG                                       (0x00001AD4)
//Duplicate of DATA0CH0_CR_COMPCTL0_REG

#define DATA6CH1_CR_COMPCTL1_REG                                       (0x00001AD8)
//Duplicate of DATA0CH0_CR_COMPCTL1_REG

#define DATA6CH1_CR_COMPCTL2_REG                                       (0x00001ADC)
//Duplicate of DATA0CH0_CR_COMPCTL2_REG

#define DATA6CH1_CR_DCCCTL5_REG                                        (0x00001AE0)
//Duplicate of DATA0CH0_CR_DCCCTL5_REG

#define DATA6CH1_CR_DCCCTL6_REG                                        (0x00001AE4)
//Duplicate of DATA0CH0_CR_DCCCTL6_REG

#define DATA6CH1_CR_DCCCTL7_REG                                        (0x00001AE8)
//Duplicate of DATA0CH0_CR_DCCCTL7_REG

#define DATA6CH1_CR_DCCCTL8_REG                                        (0x00001AEC)
//Duplicate of DATA0CH0_CR_DCCCTL8_REG

#define DATA6CH1_CR_DCCCTL9_REG                                        (0x00001AF0)
//Duplicate of DATA0CH0_CR_DCCCTL9_REG

#define DATA6CH1_CR_RXCONTROL0RANK0_REG                                (0x00001AF4)
//Duplicate of DATA0CH0_CR_RXCONTROL0RANK0_REG

#define DATA6CH1_CR_RXCONTROL0RANK1_REG                                (0x00001AF8)
//Duplicate of DATA0CH0_CR_RXCONTROL0RANK0_REG

#define DATA6CH1_CR_RXCONTROL0RANK2_REG                                (0x00001AFC)
//Duplicate of DATA0CH0_CR_RXCONTROL0RANK0_REG

#define DATA6CH1_CR_RXCONTROL0RANK3_REG                                (0x00001B00)
//Duplicate of DATA0CH0_CR_RXCONTROL0RANK0_REG

#define DATA6CH1_CR_TXCONTROL0_PH90_REG                                (0x00001B04)
//Duplicate of DATA0CH0_CR_TXCONTROL0_PH90_REG

#define DATA6CH1_CR_TXCONTROL0RANK0_REG                                (0x00001B08)
//Duplicate of DATA0CH0_CR_TXCONTROL0RANK0_REG

#define DATA6CH1_CR_TXCONTROL0RANK1_REG                                (0x00001B0C)
//Duplicate of DATA0CH0_CR_TXCONTROL0RANK0_REG

#define DATA6CH1_CR_TXCONTROL0RANK2_REG                                (0x00001B10)
//Duplicate of DATA0CH0_CR_TXCONTROL0RANK0_REG

#define DATA6CH1_CR_TXCONTROL0RANK3_REG                                (0x00001B14)
//Duplicate of DATA0CH0_CR_TXCONTROL0RANK0_REG

#define DATA6CH1_CR_DDRDATADQSRANKX_REG                                (0x00001B18)
//Duplicate of DATA0CH0_CR_DDRDATADQSRANKX_REG

#define DATA6CH1_CR_DDRDATADQSPH90RANKX_REG                            (0x00001B1C)
//Duplicate of DATA0CH0_CR_DDRDATADQSPH90RANKX_REG

#define DATA6CH1_CR_DDRCRDATAOFFSETCOMP_REG                            (0x00001B20)
//Duplicate of DATA0CH0_CR_DDRCRDATAOFFSETCOMP_REG

#define DATA6CH1_CR_DDRCRDATAOFFSETTRAIN_REG                           (0x00001B24)
//Duplicate of DATA0CH0_CR_DDRCRDATAOFFSETTRAIN_REG

#define DATA6CH1_CR_DDRCRCMDBUSTRAIN_REG                               (0x00001B28)
//Duplicate of DATA0CH0_CR_DDRCRCMDBUSTRAIN_REG

#define DATA6CH1_CR_DDRCRWRRETRAINSWIZZLECONTROL_REG                   (0x00001B2C)
//Duplicate of DATA0CH0_CR_DDRCRWRRETRAINSWIZZLECONTROL_REG

#define DATA6CH1_CR_DDRCRWRRETRAINRANK3_REG                            (0x00001B30)
//Duplicate of DATA0CH0_CR_DDRCRWRRETRAINRANK3_REG

#define DATA6CH1_CR_DDRCRWRRETRAINRANK2_REG                            (0x00001B34)
//Duplicate of DATA0CH0_CR_DDRCRWRRETRAINRANK3_REG

#define DATA6CH1_CR_DDRCRWRRETRAINRANK1_REG                            (0x00001B38)
//Duplicate of DATA0CH0_CR_DDRCRWRRETRAINRANK3_REG

#define DATA6CH1_CR_DDRCRWRRETRAINRANK0_REG                            (0x00001B3C)
//Duplicate of DATA0CH0_CR_DDRCRWRRETRAINRANK3_REG

#define DATA6CH1_CR_DDRCRWRRETRAINCONTROLSTATUS_REG                    (0x00001B40)
//Duplicate of DATA0CH0_CR_DDRCRWRRETRAINCONTROLSTATUS_REG

#define DATA6CH1_CR_TXCONTROL1_PH90_REG                                (0x00001B44)
//Duplicate of DATA0CH0_CR_TXCONTROL1_PH90_REG

#define DATA6CH1_CR_AFEMISCCTRL1_REG                                   (0x00001B48)
//Duplicate of DATA0CH0_CR_AFEMISCCTRL1_REG

#define DATA6CH1_CR_DQXRXAMPCTL_REG                                    (0x00001B4C)
//Duplicate of DATA0CH0_CR_DQXRXAMPCTL_REG

#define DATA6CH1_CR_DQ0RXAMPCTL_REG                                    (0x00001B50)
//Duplicate of DATA0CH0_CR_DQ0RXAMPCTL_REG

#define DATA6CH1_CR_DQ1RXAMPCTL_REG                                    (0x00001B54)
//Duplicate of DATA0CH0_CR_DQ1RXAMPCTL_REG

#define DATA6CH1_CR_DQ2RXAMPCTL_REG                                    (0x00001B58)
//Duplicate of DATA0CH0_CR_DQ2RXAMPCTL_REG

#define DATA6CH1_CR_DQ3RXAMPCTL_REG                                    (0x00001B5C)
//Duplicate of DATA0CH0_CR_DQ3RXAMPCTL_REG

#define DATA6CH1_CR_DQ4RXAMPCTL_REG                                    (0x00001B60)
//Duplicate of DATA0CH0_CR_DQ4RXAMPCTL_REG

#define DATA6CH1_CR_DQ5RXAMPCTL_REG                                    (0x00001B64)
//Duplicate of DATA0CH0_CR_DQ5RXAMPCTL_REG

#define DATA6CH1_CR_DQ6RXAMPCTL_REG                                    (0x00001B68)
//Duplicate of DATA0CH0_CR_DQ6RXAMPCTL_REG

#define DATA6CH1_CR_DQ7RXAMPCTL_REG                                    (0x00001B6C)
//Duplicate of DATA0CH0_CR_DQ7RXAMPCTL_REG

#define DATA6CH1_CR_DQRXAMPCTL0_REG                                    (0x00001B70)
//Duplicate of DATA0CH0_CR_DQRXAMPCTL0_REG

#define DATA6CH1_CR_DQRXAMPCTL1_REG                                    (0x00001B74)
//Duplicate of DATA0CH0_CR_DQRXAMPCTL1_REG

#define DATA6CH1_CR_DQRXAMPCTL2_REG                                    (0x00001B78)
//Duplicate of DATA0CH0_CR_DQRXAMPCTL2_REG

#define DATA6CH1_CR_DQTXCTL0_REG                                       (0x00001B7C)
//Duplicate of DATA0CH0_CR_DQTXCTL0_REG

#define DATA6CH1_CR_DCCCTL0_REG                                        (0x00001B80)
//Duplicate of DATA0CH0_CR_DCCCTL0_REG

#define DATA6CH1_CR_DCCCTL1_REG                                        (0x00001B84)
//Duplicate of DATA0CH0_CR_DCCCTL1_REG

#define DATA6CH1_CR_DCCCTL2_REG                                        (0x00001B88)
//Duplicate of DATA0CH0_CR_DCCCTL2_REG

#define DATA6CH1_CR_DCCCTL3_REG                                        (0x00001B8C)
//Duplicate of DATA0CH0_CR_DCCCTL3_REG

#define DATA6CH1_CR_DCCCTL10_REG                                       (0x00001B90)
//Duplicate of DATA0CH0_CR_DCCCTL10_REG

#define DATA6CH1_CR_DCCCTL12_REG                                       (0x00001B94)
//Duplicate of DATA0CH0_CR_DCCCTL12_REG

#define DATA6CH1_CR_CLKALIGNCTL0_REG                                   (0x00001B98)
//Duplicate of DATA0CH0_CR_CLKALIGNCTL0_REG

#define DATA6CH1_CR_CLKALIGNCTL1_REG                                   (0x00001B9C)
//Duplicate of DATA0CH0_CR_CLKALIGNCTL1_REG

#define DATA6CH1_CR_CLKALIGNCTL2_REG                                   (0x00001BA0)
//Duplicate of DATA0CH0_CR_CLKALIGNCTL2_REG

#define DATA6CH1_CR_DCCCTL4_REG                                        (0x00001BA4)
//Duplicate of DATA0CH0_CR_DCCCTL4_REG

#define DATA6CH1_CR_DDRCRDATACOMPVTT_REG                               (0x00001BA8)
//Duplicate of DATA0CH0_CR_DDRCRDATACOMPVTT_REG

#define DATA6CH1_CR_WLCTRL_REG                                         (0x00001BAC)
//Duplicate of DATA0CH0_CR_WLCTRL_REG

#define DATA6CH1_CR_LPMODECTL_REG                                      (0x00001BB0)
//Duplicate of DATA0CH0_CR_LPMODECTL_REG

#define DATA6CH1_CR_DDRCRDATACOMP0_REG                                 (0x00001BB4)
//Duplicate of DATA0CH0_CR_DDRCRDATACOMP0_REG

#define DATA6CH1_CR_DDRCRDATACOMP1_REG                                 (0x00001BB8)
//Duplicate of DATA0CH0_CR_DDRCRDATACOMP1_REG

#define DATA6CH1_CR_VCCDLLCOMPDATA_REG                                 (0x00001BBC)
//Duplicate of DATA0CH0_CR_VCCDLLCOMPDATA_REG

#define DATA6CH1_CR_DCSOFFSET_REG                                      (0x00001BC0)
//Duplicate of DATA0CH0_CR_DCSOFFSET_REG

#define DATA6CH1_CR_DFXCCMON_REG                                       (0x00001BC4)
//Duplicate of DATA0CH0_CR_DFXCCMON_REG

#define DATA6CH1_CR_AFEMISCCTRL0_REG                                   (0x00001BC8)
//Duplicate of DATA0CH0_CR_AFEMISCCTRL0_REG

#define DATA6CH1_CR_VIEWCTL_REG                                        (0x00001BCC)
//Duplicate of DATA0CH0_CR_VIEWCTL_REG

#define DATA6CH1_CR_DCCCTL11_REG                                       (0x00001BD0)
//Duplicate of DATA0CH0_CR_DCCCTL11_REG

#define DATA6CH1_CR_DCCCTL13_REG                                       (0x00001BD4)
//Duplicate of DATA0CH0_CR_DCCCTL13_REG

#define DATA6CH1_CR_PMFSM_REG                                          (0x00001BD8)
//Duplicate of DATA0CH0_CR_PMFSM_REG

#define DATA6CH1_CR_PMFSM1_REG                                         (0x00001BDC)
//Duplicate of DATA0CH0_CR_PMFSM1_REG

#define DATA6CH1_CR_CLKALIGNSTATUS_REG                                 (0x00001BE0)
//Duplicate of DATA0CH0_CR_CLKALIGNSTATUS_REG

#define DATA6CH1_CR_DATATRAINFEEDBACK_REG                              (0x00001BE4)
//Duplicate of DATA0CH0_CR_DATATRAINFEEDBACK_REG

#define DATA6CH1_CR_DDRCRMARGINMODECONTROL0_REG                        (0x00001BE8)
//Duplicate of DATA0CH0_CR_DDRCRMARGINMODECONTROL0_REG

#define DATA6CH1_CR_DDRCRMARGINMODEDEBUGMSB0_REG                       (0x00001BEC)
//Duplicate of DATA0CH0_CR_DDRCRMARGINMODEDEBUGMSB0_REG

#define DATA6CH1_CR_DDRCRMARGINMODEDEBUGLSB0_REG                       (0x00001BF0)
//Duplicate of DATA0CH0_CR_DDRCRMARGINMODEDEBUGMSB0_REG

#define DATA6CH1_CR_DCCCTL14_REG                                       (0x00001BF4)
//Duplicate of DATA0CH0_CR_DCCCTL14_REG

#define DATA7CH0_CR_DDRDATADQRANK0LANE0_REG                            (0x00001C00)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA7CH0_CR_DDRDATADQRANK0LANE1_REG                            (0x00001C04)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA7CH0_CR_DDRDATADQRANK0LANE2_REG                            (0x00001C08)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA7CH0_CR_DDRDATADQRANK0LANE3_REG                            (0x00001C0C)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA7CH0_CR_DDRDATADQRANK0LANE4_REG                            (0x00001C10)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA7CH0_CR_DDRDATADQRANK0LANE5_REG                            (0x00001C14)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA7CH0_CR_DDRDATADQRANK0LANE6_REG                            (0x00001C18)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA7CH0_CR_DDRDATADQRANK0LANE7_REG                            (0x00001C1C)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA7CH0_CR_DDRDATADQRANK1LANE0_REG                            (0x00001C20)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA7CH0_CR_DDRDATADQRANK1LANE1_REG                            (0x00001C24)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA7CH0_CR_DDRDATADQRANK1LANE2_REG                            (0x00001C28)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA7CH0_CR_DDRDATADQRANK1LANE3_REG                            (0x00001C2C)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA7CH0_CR_DDRDATADQRANK1LANE4_REG                            (0x00001C30)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA7CH0_CR_DDRDATADQRANK1LANE5_REG                            (0x00001C34)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA7CH0_CR_DDRDATADQRANK1LANE6_REG                            (0x00001C38)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA7CH0_CR_DDRDATADQRANK1LANE7_REG                            (0x00001C3C)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA7CH0_CR_DDRDATADQRANK2LANE0_REG                            (0x00001C40)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA7CH0_CR_DDRDATADQRANK2LANE1_REG                            (0x00001C44)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA7CH0_CR_DDRDATADQRANK2LANE2_REG                            (0x00001C48)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA7CH0_CR_DDRDATADQRANK2LANE3_REG                            (0x00001C4C)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA7CH0_CR_DDRDATADQRANK2LANE4_REG                            (0x00001C50)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA7CH0_CR_DDRDATADQRANK2LANE5_REG                            (0x00001C54)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA7CH0_CR_DDRDATADQRANK2LANE6_REG                            (0x00001C58)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA7CH0_CR_DDRDATADQRANK2LANE7_REG                            (0x00001C5C)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA7CH0_CR_DDRDATADQRANK3LANE0_REG                            (0x00001C60)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA7CH0_CR_DDRDATADQRANK3LANE1_REG                            (0x00001C64)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA7CH0_CR_DDRDATADQRANK3LANE2_REG                            (0x00001C68)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA7CH0_CR_DDRDATADQRANK3LANE3_REG                            (0x00001C6C)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA7CH0_CR_DDRDATADQRANK3LANE4_REG                            (0x00001C70)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA7CH0_CR_DDRDATADQRANK3LANE5_REG                            (0x00001C74)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA7CH0_CR_DDRDATADQRANK3LANE6_REG                            (0x00001C78)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA7CH0_CR_DDRDATADQRANK3LANE7_REG                            (0x00001C7C)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA7CH0_CR_DDRCRDATACONTROL0_REG                              (0x00001C80)
//Duplicate of DATA0CH0_CR_DDRCRDATACONTROL0_REG

#define DATA7CH0_CR_DDRCRDATACONTROL1_REG                              (0x00001C84)
//Duplicate of DATA0CH0_CR_DDRCRDATACONTROL1_REG

#define DATA7CH0_CR_DDRCRDATACONTROL2_REG                              (0x00001C88)
//Duplicate of DATA0CH0_CR_DDRCRDATACONTROL2_REG

#define DATA7CH0_CR_DDRCRDATACONTROL3_REG                              (0x00001C8C)
//Duplicate of DATA0CH0_CR_DDRCRDATACONTROL3_REG

#define DATA7CH0_CR_AFEMISC_REG                                        (0x00001C90)
//Duplicate of DATA0CH0_CR_AFEMISC_REG

#define DATA7CH0_CR_SRZCTL_REG                                         (0x00001C94)
//Duplicate of DATA0CH0_CR_SRZCTL_REG

#define DATA7CH0_CR_DQRXCTL0_REG                                       (0x00001C98)
//Duplicate of DATA0CH0_CR_DQRXCTL0_REG

#define DATA7CH0_CR_DQRXCTL1_REG                                       (0x00001C9C)
//Duplicate of DATA0CH0_CR_DQRXCTL1_REG

#define DATA7CH0_CR_DQSTXRXCTL_REG                                     (0x00001CA0)
//Duplicate of DATA0CH0_CR_DQSTXRXCTL_REG

#define DATA7CH0_CR_DQSTXRXCTL0_REG                                    (0x00001CA4)
//Duplicate of DATA0CH0_CR_DQSTXRXCTL0_REG

#define DATA7CH0_CR_SDLCTL1_REG                                        (0x00001CA8)
//Duplicate of DATA0CH0_CR_SDLCTL1_REG

#define DATA7CH0_CR_SDLCTL0_REG                                        (0x00001CAC)
//Duplicate of DATA0CH0_CR_SDLCTL0_REG

#define DATA7CH0_CR_MDLLCTL0_REG                                       (0x00001CB0)
//Duplicate of DATA0CH0_CR_MDLLCTL0_REG

#define DATA7CH0_CR_MDLLCTL1_REG                                       (0x00001CB4)
//Duplicate of DATA0CH0_CR_MDLLCTL1_REG

#define DATA7CH0_CR_MDLLCTL2_REG                                       (0x00001CB8)
//Duplicate of DATA0CH0_CR_MDLLCTL2_REG

#define DATA7CH0_CR_MDLLCTL3_REG                                       (0x00001CBC)
//Duplicate of DATA0CH0_CR_MDLLCTL3_REG

#define DATA7CH0_CR_MDLLCTL4_REG                                       (0x00001CC0)
//Duplicate of DATA0CH0_CR_MDLLCTL4_REG

#define DATA7CH0_CR_MDLLCTL5_REG                                       (0x00001CC4)
//Duplicate of DATA0CH0_CR_MDLLCTL4_REG

#define DATA7CH0_CR_DIGMISCCTRL_REG                                    (0x00001CC4)
//Duplicate of DATA0CH0_CR_DIGMISCCTRL_REG

#define DATA7CH0_CR_AFEMISCCTRL2_REG                                   (0x00001CC8)
//Duplicate of DATA0CH0_CR_AFEMISCCTRL2_REG

#define DATA7CH0_CR_TXPBDOFFSET0_REG                                   (0x00001CCC)
//Duplicate of DATA0CH0_CR_TXPBDOFFSET0_REG

#define DATA7CH0_CR_TXPBDOFFSET1_REG                                   (0x00001CD0)
//Duplicate of DATA0CH0_CR_TXPBDOFFSET1_REG

#define DATA7CH0_CR_COMPCTL0_REG                                       (0x00001CD4)
//Duplicate of DATA0CH0_CR_COMPCTL0_REG

#define DATA7CH0_CR_COMPCTL1_REG                                       (0x00001CD8)
//Duplicate of DATA0CH0_CR_COMPCTL1_REG

#define DATA7CH0_CR_COMPCTL2_REG                                       (0x00001CDC)
//Duplicate of DATA0CH0_CR_COMPCTL2_REG

#define DATA7CH0_CR_DCCCTL5_REG                                        (0x00001CE0)
//Duplicate of DATA0CH0_CR_DCCCTL5_REG

#define DATA7CH0_CR_DCCCTL6_REG                                        (0x00001CE4)
//Duplicate of DATA0CH0_CR_DCCCTL6_REG

#define DATA7CH0_CR_DCCCTL7_REG                                        (0x00001CE8)
//Duplicate of DATA0CH0_CR_DCCCTL7_REG

#define DATA7CH0_CR_DCCCTL8_REG                                        (0x00001CEC)
//Duplicate of DATA0CH0_CR_DCCCTL8_REG

#define DATA7CH0_CR_DCCCTL9_REG                                        (0x00001CF0)
//Duplicate of DATA0CH0_CR_DCCCTL9_REG

#define DATA7CH0_CR_RXCONTROL0RANK0_REG                                (0x00001CF4)
//Duplicate of DATA0CH0_CR_RXCONTROL0RANK0_REG

#define DATA7CH0_CR_RXCONTROL0RANK1_REG                                (0x00001CF8)
//Duplicate of DATA0CH0_CR_RXCONTROL0RANK0_REG

#define DATA7CH0_CR_RXCONTROL0RANK2_REG                                (0x00001CFC)
//Duplicate of DATA0CH0_CR_RXCONTROL0RANK0_REG

#define DATA7CH0_CR_RXCONTROL0RANK3_REG                                (0x00001D00)
//Duplicate of DATA0CH0_CR_RXCONTROL0RANK0_REG

#define DATA7CH0_CR_TXCONTROL0_PH90_REG                                (0x00001D04)
//Duplicate of DATA0CH0_CR_TXCONTROL0_PH90_REG

#define DATA7CH0_CR_TXCONTROL0RANK0_REG                                (0x00001D08)
//Duplicate of DATA0CH0_CR_TXCONTROL0RANK0_REG

#define DATA7CH0_CR_TXCONTROL0RANK1_REG                                (0x00001D0C)
//Duplicate of DATA0CH0_CR_TXCONTROL0RANK0_REG

#define DATA7CH0_CR_TXCONTROL0RANK2_REG                                (0x00001D10)
//Duplicate of DATA0CH0_CR_TXCONTROL0RANK0_REG

#define DATA7CH0_CR_TXCONTROL0RANK3_REG                                (0x00001D14)
//Duplicate of DATA0CH0_CR_TXCONTROL0RANK0_REG

#define DATA7CH0_CR_DDRDATADQSRANKX_REG                                (0x00001D18)
//Duplicate of DATA0CH0_CR_DDRDATADQSRANKX_REG

#define DATA7CH0_CR_DDRDATADQSPH90RANKX_REG                            (0x00001D1C)
//Duplicate of DATA0CH0_CR_DDRDATADQSPH90RANKX_REG

#define DATA7CH0_CR_DDRCRDATAOFFSETCOMP_REG                            (0x00001D20)
//Duplicate of DATA0CH0_CR_DDRCRDATAOFFSETCOMP_REG

#define DATA7CH0_CR_DDRCRDATAOFFSETTRAIN_REG                           (0x00001D24)
//Duplicate of DATA0CH0_CR_DDRCRDATAOFFSETTRAIN_REG

#define DATA7CH0_CR_DDRCRCMDBUSTRAIN_REG                               (0x00001D28)
//Duplicate of DATA0CH0_CR_DDRCRCMDBUSTRAIN_REG

#define DATA7CH0_CR_DDRCRWRRETRAINSWIZZLECONTROL_REG                   (0x00001D2C)
//Duplicate of DATA0CH0_CR_DDRCRWRRETRAINSWIZZLECONTROL_REG

#define DATA7CH0_CR_DDRCRWRRETRAINRANK3_REG                            (0x00001D30)
//Duplicate of DATA0CH0_CR_DDRCRWRRETRAINRANK3_REG

#define DATA7CH0_CR_DDRCRWRRETRAINRANK2_REG                            (0x00001D34)
//Duplicate of DATA0CH0_CR_DDRCRWRRETRAINRANK3_REG

#define DATA7CH0_CR_DDRCRWRRETRAINRANK1_REG                            (0x00001D38)
//Duplicate of DATA0CH0_CR_DDRCRWRRETRAINRANK3_REG

#define DATA7CH0_CR_DDRCRWRRETRAINRANK0_REG                            (0x00001D3C)
//Duplicate of DATA0CH0_CR_DDRCRWRRETRAINRANK3_REG

#define DATA7CH0_CR_DDRCRWRRETRAINCONTROLSTATUS_REG                    (0x00001D40)
//Duplicate of DATA0CH0_CR_DDRCRWRRETRAINCONTROLSTATUS_REG

#define DATA7CH0_CR_TXCONTROL1_PH90_REG                                (0x00001D44)
//Duplicate of DATA0CH0_CR_TXCONTROL1_PH90_REG

#define DATA7CH0_CR_AFEMISCCTRL1_REG                                   (0x00001D48)
//Duplicate of DATA0CH0_CR_AFEMISCCTRL1_REG

#define DATA7CH0_CR_DQXRXAMPCTL_REG                                    (0x00001D4C)
//Duplicate of DATA0CH0_CR_DQXRXAMPCTL_REG

#define DATA7CH0_CR_DQ0RXAMPCTL_REG                                    (0x00001D50)
//Duplicate of DATA0CH0_CR_DQ0RXAMPCTL_REG

#define DATA7CH0_CR_DQ1RXAMPCTL_REG                                    (0x00001D54)
//Duplicate of DATA0CH0_CR_DQ1RXAMPCTL_REG

#define DATA7CH0_CR_DQ2RXAMPCTL_REG                                    (0x00001D58)
//Duplicate of DATA0CH0_CR_DQ2RXAMPCTL_REG

#define DATA7CH0_CR_DQ3RXAMPCTL_REG                                    (0x00001D5C)
//Duplicate of DATA0CH0_CR_DQ3RXAMPCTL_REG

#define DATA7CH0_CR_DQ4RXAMPCTL_REG                                    (0x00001D60)
//Duplicate of DATA0CH0_CR_DQ4RXAMPCTL_REG

#define DATA7CH0_CR_DQ5RXAMPCTL_REG                                    (0x00001D64)
//Duplicate of DATA0CH0_CR_DQ5RXAMPCTL_REG

#define DATA7CH0_CR_DQ6RXAMPCTL_REG                                    (0x00001D68)
//Duplicate of DATA0CH0_CR_DQ6RXAMPCTL_REG

#define DATA7CH0_CR_DQ7RXAMPCTL_REG                                    (0x00001D6C)
//Duplicate of DATA0CH0_CR_DQ7RXAMPCTL_REG

#define DATA7CH0_CR_DQRXAMPCTL0_REG                                    (0x00001D70)
//Duplicate of DATA0CH0_CR_DQRXAMPCTL0_REG

#define DATA7CH0_CR_DQRXAMPCTL1_REG                                    (0x00001D74)
//Duplicate of DATA0CH0_CR_DQRXAMPCTL1_REG

#define DATA7CH0_CR_DQRXAMPCTL2_REG                                    (0x00001D78)
//Duplicate of DATA0CH0_CR_DQRXAMPCTL2_REG

#define DATA7CH0_CR_DQTXCTL0_REG                                       (0x00001D7C)
//Duplicate of DATA0CH0_CR_DQTXCTL0_REG

#define DATA7CH0_CR_DCCCTL0_REG                                        (0x00001D80)
//Duplicate of DATA0CH0_CR_DCCCTL0_REG

#define DATA7CH0_CR_DCCCTL1_REG                                        (0x00001D84)
//Duplicate of DATA0CH0_CR_DCCCTL1_REG

#define DATA7CH0_CR_DCCCTL2_REG                                        (0x00001D88)
//Duplicate of DATA0CH0_CR_DCCCTL2_REG

#define DATA7CH0_CR_DCCCTL3_REG                                        (0x00001D8C)
//Duplicate of DATA0CH0_CR_DCCCTL3_REG

#define DATA7CH0_CR_DCCCTL10_REG                                       (0x00001D90)
//Duplicate of DATA0CH0_CR_DCCCTL10_REG

#define DATA7CH0_CR_DCCCTL12_REG                                       (0x00001D94)
//Duplicate of DATA0CH0_CR_DCCCTL12_REG

#define DATA7CH0_CR_CLKALIGNCTL0_REG                                   (0x00001D98)
//Duplicate of DATA0CH0_CR_CLKALIGNCTL0_REG

#define DATA7CH0_CR_CLKALIGNCTL1_REG                                   (0x00001D9C)
//Duplicate of DATA0CH0_CR_CLKALIGNCTL1_REG

#define DATA7CH0_CR_CLKALIGNCTL2_REG                                   (0x00001DA0)
//Duplicate of DATA0CH0_CR_CLKALIGNCTL2_REG

#define DATA7CH0_CR_DCCCTL4_REG                                        (0x00001DA4)
//Duplicate of DATA0CH0_CR_DCCCTL4_REG

#define DATA7CH0_CR_DDRCRDATACOMPVTT_REG                               (0x00001DA8)
//Duplicate of DATA0CH0_CR_DDRCRDATACOMPVTT_REG

#define DATA7CH0_CR_WLCTRL_REG                                         (0x00001DAC)
//Duplicate of DATA0CH0_CR_WLCTRL_REG

#define DATA7CH0_CR_LPMODECTL_REG                                      (0x00001DB0)
//Duplicate of DATA0CH0_CR_LPMODECTL_REG

#define DATA7CH0_CR_DDRCRDATACOMP0_REG                                 (0x00001DB4)
//Duplicate of DATA0CH0_CR_DDRCRDATACOMP0_REG

#define DATA7CH0_CR_DDRCRDATACOMP1_REG                                 (0x00001DB8)
//Duplicate of DATA0CH0_CR_DDRCRDATACOMP1_REG

#define DATA7CH0_CR_VCCDLLCOMPDATA_REG                                 (0x00001DBC)
//Duplicate of DATA0CH0_CR_VCCDLLCOMPDATA_REG

#define DATA7CH0_CR_DCSOFFSET_REG                                      (0x00001DC0)
//Duplicate of DATA0CH0_CR_DCSOFFSET_REG

#define DATA7CH0_CR_DFXCCMON_REG                                       (0x00001DC4)
//Duplicate of DATA0CH0_CR_DFXCCMON_REG

#define DATA7CH0_CR_AFEMISCCTRL0_REG                                   (0x00001DC8)
//Duplicate of DATA0CH0_CR_AFEMISCCTRL0_REG

#define DATA7CH0_CR_VIEWCTL_REG                                        (0x00001DCC)
//Duplicate of DATA0CH0_CR_VIEWCTL_REG

#define DATA7CH0_CR_DCCCTL11_REG                                       (0x00001DD0)
//Duplicate of DATA0CH0_CR_DCCCTL11_REG

#define DATA7CH0_CR_DCCCTL13_REG                                       (0x00001DD4)
//Duplicate of DATA0CH0_CR_DCCCTL13_REG

#define DATA7CH0_CR_PMFSM_REG                                          (0x00001DD8)
//Duplicate of DATA0CH0_CR_PMFSM_REG

#define DATA7CH0_CR_PMFSM1_REG                                         (0x00001DDC)
//Duplicate of DATA0CH0_CR_PMFSM1_REG

#define DATA7CH0_CR_CLKALIGNSTATUS_REG                                 (0x00001DE0)
//Duplicate of DATA0CH0_CR_CLKALIGNSTATUS_REG

#define DATA7CH0_CR_DATATRAINFEEDBACK_REG                              (0x00001DE4)
//Duplicate of DATA0CH0_CR_DATATRAINFEEDBACK_REG

#define DATA7CH0_CR_DDRCRMARGINMODECONTROL0_REG                        (0x00001DE8)
//Duplicate of DATA0CH0_CR_DDRCRMARGINMODECONTROL0_REG

#define DATA7CH0_CR_DDRCRMARGINMODEDEBUGMSB0_REG                       (0x00001DEC)
//Duplicate of DATA0CH0_CR_DDRCRMARGINMODEDEBUGMSB0_REG

#define DATA7CH0_CR_DDRCRMARGINMODEDEBUGLSB0_REG                       (0x00001DF0)
//Duplicate of DATA0CH0_CR_DDRCRMARGINMODEDEBUGMSB0_REG

#define DATA7CH0_CR_DCCCTL14_REG                                       (0x00001DF4)
//Duplicate of DATA0CH0_CR_DCCCTL14_REG

#define DATA7CH1_CR_DDRDATADQRANK0LANE0_REG                            (0x00001E00)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA7CH1_CR_DDRDATADQRANK0LANE1_REG                            (0x00001E04)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA7CH1_CR_DDRDATADQRANK0LANE2_REG                            (0x00001E08)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA7CH1_CR_DDRDATADQRANK0LANE3_REG                            (0x00001E0C)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA7CH1_CR_DDRDATADQRANK0LANE4_REG                            (0x00001E10)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA7CH1_CR_DDRDATADQRANK0LANE5_REG                            (0x00001E14)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA7CH1_CR_DDRDATADQRANK0LANE6_REG                            (0x00001E18)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA7CH1_CR_DDRDATADQRANK0LANE7_REG                            (0x00001E1C)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA7CH1_CR_DDRDATADQRANK1LANE0_REG                            (0x00001E20)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA7CH1_CR_DDRDATADQRANK1LANE1_REG                            (0x00001E24)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA7CH1_CR_DDRDATADQRANK1LANE2_REG                            (0x00001E28)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA7CH1_CR_DDRDATADQRANK1LANE3_REG                            (0x00001E2C)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA7CH1_CR_DDRDATADQRANK1LANE4_REG                            (0x00001E30)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA7CH1_CR_DDRDATADQRANK1LANE5_REG                            (0x00001E34)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA7CH1_CR_DDRDATADQRANK1LANE6_REG                            (0x00001E38)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA7CH1_CR_DDRDATADQRANK1LANE7_REG                            (0x00001E3C)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA7CH1_CR_DDRDATADQRANK2LANE0_REG                            (0x00001E40)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA7CH1_CR_DDRDATADQRANK2LANE1_REG                            (0x00001E44)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA7CH1_CR_DDRDATADQRANK2LANE2_REG                            (0x00001E48)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA7CH1_CR_DDRDATADQRANK2LANE3_REG                            (0x00001E4C)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA7CH1_CR_DDRDATADQRANK2LANE4_REG                            (0x00001E50)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA7CH1_CR_DDRDATADQRANK2LANE5_REG                            (0x00001E54)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA7CH1_CR_DDRDATADQRANK2LANE6_REG                            (0x00001E58)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA7CH1_CR_DDRDATADQRANK2LANE7_REG                            (0x00001E5C)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA7CH1_CR_DDRDATADQRANK3LANE0_REG                            (0x00001E60)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA7CH1_CR_DDRDATADQRANK3LANE1_REG                            (0x00001E64)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA7CH1_CR_DDRDATADQRANK3LANE2_REG                            (0x00001E68)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA7CH1_CR_DDRDATADQRANK3LANE3_REG                            (0x00001E6C)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA7CH1_CR_DDRDATADQRANK3LANE4_REG                            (0x00001E70)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA7CH1_CR_DDRDATADQRANK3LANE5_REG                            (0x00001E74)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA7CH1_CR_DDRDATADQRANK3LANE6_REG                            (0x00001E78)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA7CH1_CR_DDRDATADQRANK3LANE7_REG                            (0x00001E7C)
//Duplicate of DATA0CH0_CR_DDRDATADQRANK0LANE0_REG

#define DATA7CH1_CR_DDRCRDATACONTROL0_REG                              (0x00001E80)
//Duplicate of DATA0CH0_CR_DDRCRDATACONTROL0_REG

#define DATA7CH1_CR_DDRCRDATACONTROL1_REG                              (0x00001E84)
//Duplicate of DATA0CH0_CR_DDRCRDATACONTROL1_REG

#define DATA7CH1_CR_DDRCRDATACONTROL2_REG                              (0x00001E88)
//Duplicate of DATA0CH0_CR_DDRCRDATACONTROL2_REG

#define DATA7CH1_CR_DDRCRDATACONTROL3_REG                              (0x00001E8C)
//Duplicate of DATA0CH0_CR_DDRCRDATACONTROL3_REG

#define DATA7CH1_CR_AFEMISC_REG                                        (0x00001E90)
//Duplicate of DATA0CH0_CR_AFEMISC_REG

#define DATA7CH1_CR_SRZCTL_REG                                         (0x00001E94)
//Duplicate of DATA0CH0_CR_SRZCTL_REG

#define DATA7CH1_CR_DQRXCTL0_REG                                       (0x00001E98)
//Duplicate of DATA0CH0_CR_DQRXCTL0_REG

#define DATA7CH1_CR_DQRXCTL1_REG                                       (0x00001E9C)
//Duplicate of DATA0CH0_CR_DQRXCTL1_REG

#define DATA7CH1_CR_DQSTXRXCTL_REG                                     (0x00001EA0)
//Duplicate of DATA0CH0_CR_DQSTXRXCTL_REG

#define DATA7CH1_CR_DQSTXRXCTL0_REG                                    (0x00001EA4)
//Duplicate of DATA0CH0_CR_DQSTXRXCTL0_REG

#define DATA7CH1_CR_SDLCTL1_REG                                        (0x00001EA8)
//Duplicate of DATA0CH0_CR_SDLCTL1_REG

#define DATA7CH1_CR_SDLCTL0_REG                                        (0x00001EAC)
//Duplicate of DATA0CH0_CR_SDLCTL0_REG

#define DATA7CH1_CR_MDLLCTL0_REG                                       (0x00001EB0)
//Duplicate of DATA0CH0_CR_MDLLCTL0_REG

#define DATA7CH1_CR_MDLLCTL1_REG                                       (0x00001EB4)
//Duplicate of DATA0CH0_CR_MDLLCTL1_REG

#define DATA7CH1_CR_MDLLCTL2_REG                                       (0x00001EB8)
//Duplicate of DATA0CH0_CR_MDLLCTL2_REG

#define DATA7CH1_CR_MDLLCTL3_REG                                       (0x00001EBC)
//Duplicate of DATA0CH0_CR_MDLLCTL3_REG

#define DATA7CH1_CR_MDLLCTL4_REG                                       (0x00001EC0)
//Duplicate of DATA0CH0_CR_MDLLCTL4_REG

#define DATA7CH1_CR_MDLLCTL5_REG                                       (0x00001EC4)
//Duplicate of DATA0CH0_CR_MDLLCTL4_REG

#define DATA7CH1_CR_DIGMISCCTRL_REG                                    (0x00001EC4)
//Duplicate of DATA0CH0_CR_DIGMISCCTRL_REG

#define DATA7CH1_CR_AFEMISCCTRL2_REG                                   (0x00001EC8)
//Duplicate of DATA0CH0_CR_AFEMISCCTRL2_REG

#define DATA7CH1_CR_TXPBDOFFSET0_REG                                   (0x00001ECC)
//Duplicate of DATA0CH0_CR_TXPBDOFFSET0_REG

#define DATA7CH1_CR_TXPBDOFFSET1_REG                                   (0x00001ED0)
//Duplicate of DATA0CH0_CR_TXPBDOFFSET1_REG

#define DATA7CH1_CR_COMPCTL0_REG                                       (0x00001ED4)
//Duplicate of DATA0CH0_CR_COMPCTL0_REG

#define DATA7CH1_CR_COMPCTL1_REG                                       (0x00001ED8)
//Duplicate of DATA0CH0_CR_COMPCTL1_REG

#define DATA7CH1_CR_COMPCTL2_REG                                       (0x00001EDC)
//Duplicate of DATA0CH0_CR_COMPCTL2_REG

#define DATA7CH1_CR_DCCCTL5_REG                                        (0x00001EE0)
//Duplicate of DATA0CH0_CR_DCCCTL5_REG

#define DATA7CH1_CR_DCCCTL6_REG                                        (0x00001EE4)
//Duplicate of DATA0CH0_CR_DCCCTL6_REG

#define DATA7CH1_CR_DCCCTL7_REG                                        (0x00001EE8)
//Duplicate of DATA0CH0_CR_DCCCTL7_REG

#define DATA7CH1_CR_DCCCTL8_REG                                        (0x00001EEC)
//Duplicate of DATA0CH0_CR_DCCCTL8_REG

#define DATA7CH1_CR_DCCCTL9_REG                                        (0x00001EF0)
//Duplicate of DATA0CH0_CR_DCCCTL9_REG

#define DATA7CH1_CR_RXCONTROL0RANK0_REG                                (0x00001EF4)
//Duplicate of DATA0CH0_CR_RXCONTROL0RANK0_REG

#define DATA7CH1_CR_RXCONTROL0RANK1_REG                                (0x00001EF8)
//Duplicate of DATA0CH0_CR_RXCONTROL0RANK0_REG

#define DATA7CH1_CR_RXCONTROL0RANK2_REG                                (0x00001EFC)
//Duplicate of DATA0CH0_CR_RXCONTROL0RANK0_REG

#define DATA7CH1_CR_RXCONTROL0RANK3_REG                                (0x00001F00)
//Duplicate of DATA0CH0_CR_RXCONTROL0RANK0_REG

#define DATA7CH1_CR_TXCONTROL0_PH90_REG                                (0x00001F04)
//Duplicate of DATA0CH0_CR_TXCONTROL0_PH90_REG

#define DATA7CH1_CR_TXCONTROL0RANK0_REG                                (0x00001F08)
//Duplicate of DATA0CH0_CR_TXCONTROL0RANK0_REG

#define DATA7CH1_CR_TXCONTROL0RANK1_REG                                (0x00001F0C)
//Duplicate of DATA0CH0_CR_TXCONTROL0RANK0_REG

#define DATA7CH1_CR_TXCONTROL0RANK2_REG                                (0x00001F10)
//Duplicate of DATA0CH0_CR_TXCONTROL0RANK0_REG

#define DATA7CH1_CR_TXCONTROL0RANK3_REG                                (0x00001F14)
//Duplicate of DATA0CH0_CR_TXCONTROL0RANK0_REG

#define DATA7CH1_CR_DDRDATADQSRANKX_REG                                (0x00001F18)
//Duplicate of DATA0CH0_CR_DDRDATADQSRANKX_REG

#define DATA7CH1_CR_DDRDATADQSPH90RANKX_REG                            (0x00001F1C)
//Duplicate of DATA0CH0_CR_DDRDATADQSPH90RANKX_REG

#define DATA7CH1_CR_DDRCRDATAOFFSETCOMP_REG                            (0x00001F20)
//Duplicate of DATA0CH0_CR_DDRCRDATAOFFSETCOMP_REG

#define DATA7CH1_CR_DDRCRDATAOFFSETTRAIN_REG                           (0x00001F24)
//Duplicate of DATA0CH0_CR_DDRCRDATAOFFSETTRAIN_REG

#define DATA7CH1_CR_DDRCRCMDBUSTRAIN_REG                               (0x00001F28)
//Duplicate of DATA0CH0_CR_DDRCRCMDBUSTRAIN_REG

#define DATA7CH1_CR_DDRCRWRRETRAINSWIZZLECONTROL_REG                   (0x00001F2C)
//Duplicate of DATA0CH0_CR_DDRCRWRRETRAINSWIZZLECONTROL_REG

#define DATA7CH1_CR_DDRCRWRRETRAINRANK3_REG                            (0x00001F30)
//Duplicate of DATA0CH0_CR_DDRCRWRRETRAINRANK3_REG

#define DATA7CH1_CR_DDRCRWRRETRAINRANK2_REG                            (0x00001F34)
//Duplicate of DATA0CH0_CR_DDRCRWRRETRAINRANK3_REG

#define DATA7CH1_CR_DDRCRWRRETRAINRANK1_REG                            (0x00001F38)
//Duplicate of DATA0CH0_CR_DDRCRWRRETRAINRANK3_REG

#define DATA7CH1_CR_DDRCRWRRETRAINRANK0_REG                            (0x00001F3C)
//Duplicate of DATA0CH0_CR_DDRCRWRRETRAINRANK3_REG

#define DATA7CH1_CR_DDRCRWRRETRAINCONTROLSTATUS_REG                    (0x00001F40)
//Duplicate of DATA0CH0_CR_DDRCRWRRETRAINCONTROLSTATUS_REG

#define DATA7CH1_CR_TXCONTROL1_PH90_REG                                (0x00001F44)
//Duplicate of DATA0CH0_CR_TXCONTROL1_PH90_REG

#define DATA7CH1_CR_AFEMISCCTRL1_REG                                   (0x00001F48)
//Duplicate of DATA0CH0_CR_AFEMISCCTRL1_REG

#define DATA7CH1_CR_DQXRXAMPCTL_REG                                    (0x00001F4C)
//Duplicate of DATA0CH0_CR_DQXRXAMPCTL_REG

#define DATA7CH1_CR_DQ0RXAMPCTL_REG                                    (0x00001F50)
//Duplicate of DATA0CH0_CR_DQ0RXAMPCTL_REG

#define DATA7CH1_CR_DQ1RXAMPCTL_REG                                    (0x00001F54)
//Duplicate of DATA0CH0_CR_DQ1RXAMPCTL_REG

#define DATA7CH1_CR_DQ2RXAMPCTL_REG                                    (0x00001F58)
//Duplicate of DATA0CH0_CR_DQ2RXAMPCTL_REG

#define DATA7CH1_CR_DQ3RXAMPCTL_REG                                    (0x00001F5C)
//Duplicate of DATA0CH0_CR_DQ3RXAMPCTL_REG

#define DATA7CH1_CR_DQ4RXAMPCTL_REG                                    (0x00001F60)
//Duplicate of DATA0CH0_CR_DQ4RXAMPCTL_REG

#define DATA7CH1_CR_DQ5RXAMPCTL_REG                                    (0x00001F64)
//Duplicate of DATA0CH0_CR_DQ5RXAMPCTL_REG

#define DATA7CH1_CR_DQ6RXAMPCTL_REG                                    (0x00001F68)
//Duplicate of DATA0CH0_CR_DQ6RXAMPCTL_REG

#define DATA7CH1_CR_DQ7RXAMPCTL_REG                                    (0x00001F6C)
//Duplicate of DATA0CH0_CR_DQ7RXAMPCTL_REG

#define DATA7CH1_CR_DQRXAMPCTL0_REG                                    (0x00001F70)
//Duplicate of DATA0CH0_CR_DQRXAMPCTL0_REG

#define DATA7CH1_CR_DQRXAMPCTL1_REG                                    (0x00001F74)
//Duplicate of DATA0CH0_CR_DQRXAMPCTL1_REG

#define DATA7CH1_CR_DQRXAMPCTL2_REG                                    (0x00001F78)
//Duplicate of DATA0CH0_CR_DQRXAMPCTL2_REG

#define DATA7CH1_CR_DQTXCTL0_REG                                       (0x00001F7C)
//Duplicate of DATA0CH0_CR_DQTXCTL0_REG

#define DATA7CH1_CR_DCCCTL0_REG                                        (0x00001F80)
//Duplicate of DATA0CH0_CR_DCCCTL0_REG

#define DATA7CH1_CR_DCCCTL1_REG                                        (0x00001F84)
//Duplicate of DATA0CH0_CR_DCCCTL1_REG

#define DATA7CH1_CR_DCCCTL2_REG                                        (0x00001F88)
//Duplicate of DATA0CH0_CR_DCCCTL2_REG

#define DATA7CH1_CR_DCCCTL3_REG                                        (0x00001F8C)
//Duplicate of DATA0CH0_CR_DCCCTL3_REG

#define DATA7CH1_CR_DCCCTL10_REG                                       (0x00001F90)
//Duplicate of DATA0CH0_CR_DCCCTL10_REG

#define DATA7CH1_CR_DCCCTL12_REG                                       (0x00001F94)
//Duplicate of DATA0CH0_CR_DCCCTL12_REG

#define DATA7CH1_CR_CLKALIGNCTL0_REG                                   (0x00001F98)
//Duplicate of DATA0CH0_CR_CLKALIGNCTL0_REG

#define DATA7CH1_CR_CLKALIGNCTL1_REG                                   (0x00001F9C)
//Duplicate of DATA0CH0_CR_CLKALIGNCTL1_REG

#define DATA7CH1_CR_CLKALIGNCTL2_REG                                   (0x00001FA0)
//Duplicate of DATA0CH0_CR_CLKALIGNCTL2_REG

#define DATA7CH1_CR_DCCCTL4_REG                                        (0x00001FA4)
//Duplicate of DATA0CH0_CR_DCCCTL4_REG

#define DATA7CH1_CR_DDRCRDATACOMPVTT_REG                               (0x00001FA8)
//Duplicate of DATA0CH0_CR_DDRCRDATACOMPVTT_REG

#define DATA7CH1_CR_WLCTRL_REG                                         (0x00001FAC)
//Duplicate of DATA0CH0_CR_WLCTRL_REG

#define DATA7CH1_CR_LPMODECTL_REG                                      (0x00001FB0)
//Duplicate of DATA0CH0_CR_LPMODECTL_REG

#define DATA7CH1_CR_DDRCRDATACOMP0_REG                                 (0x00001FB4)
//Duplicate of DATA0CH0_CR_DDRCRDATACOMP0_REG

#define DATA7CH1_CR_DDRCRDATACOMP1_REG                                 (0x00001FB8)
//Duplicate of DATA0CH0_CR_DDRCRDATACOMP1_REG

#define DATA7CH1_CR_VCCDLLCOMPDATA_REG                                 (0x00001FBC)
//Duplicate of DATA0CH0_CR_VCCDLLCOMPDATA_REG

#define DATA7CH1_CR_DCSOFFSET_REG                                      (0x00001FC0)
//Duplicate of DATA0CH0_CR_DCSOFFSET_REG

#define DATA7CH1_CR_DFXCCMON_REG                                       (0x00001FC4)
//Duplicate of DATA0CH0_CR_DFXCCMON_REG

#define DATA7CH1_CR_AFEMISCCTRL0_REG                                   (0x00001FC8)
//Duplicate of DATA0CH0_CR_AFEMISCCTRL0_REG

#define DATA7CH1_CR_VIEWCTL_REG                                        (0x00001FCC)
//Duplicate of DATA0CH0_CR_VIEWCTL_REG

#define DATA7CH1_CR_DCCCTL11_REG                                       (0x00001FD0)
//Duplicate of DATA0CH0_CR_DCCCTL11_REG

#define DATA7CH1_CR_DCCCTL13_REG                                       (0x00001FD4)
//Duplicate of DATA0CH0_CR_DCCCTL13_REG

#define DATA7CH1_CR_PMFSM_REG                                          (0x00001FD8)
//Duplicate of DATA0CH0_CR_PMFSM_REG

#define DATA7CH1_CR_PMFSM1_REG                                         (0x00001FDC)
//Duplicate of DATA0CH0_CR_PMFSM1_REG

#define DATA7CH1_CR_CLKALIGNSTATUS_REG                                 (0x00001FE0)
//Duplicate of DATA0CH0_CR_CLKALIGNSTATUS_REG

#define DATA7CH1_CR_DATATRAINFEEDBACK_REG                              (0x00001FE4)
//Duplicate of DATA0CH0_CR_DATATRAINFEEDBACK_REG

#define DATA7CH1_CR_DDRCRMARGINMODECONTROL0_REG                        (0x00001FE8)
//Duplicate of DATA0CH0_CR_DDRCRMARGINMODECONTROL0_REG

#define DATA7CH1_CR_DDRCRMARGINMODEDEBUGMSB0_REG                       (0x00001FEC)
//Duplicate of DATA0CH0_CR_DDRCRMARGINMODEDEBUGMSB0_REG

#define DATA7CH1_CR_DDRCRMARGINMODEDEBUGLSB0_REG                       (0x00001FF0)
//Duplicate of DATA0CH0_CR_DDRCRMARGINMODEDEBUGMSB0_REG

#define DATA7CH1_CR_DCCCTL14_REG                                       (0x00001FF4)
//Duplicate of DATA0CH0_CR_DCCCTL14_REG
#pragma pack(pop)
#endif
