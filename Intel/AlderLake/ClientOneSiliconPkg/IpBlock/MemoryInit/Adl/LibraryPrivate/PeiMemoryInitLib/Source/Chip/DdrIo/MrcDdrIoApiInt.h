/** @file
  Contains functions that are used within the DdrIo Library.

@copyright
  INTEL CONFIDENTIAL
  Copyright 2019 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains an 'Intel Peripheral Driver' and is uniquely identified as
  "Intel Reference Module" and is licensed for Intel CPUs and chipsets under
  the terms of your license agreement with Intel or your vendor. This file may
  be modified by the user, subject to additional terms of the license agreement.

@par Specification Reference:
**/
#ifndef _MrcDdrIoApiInt_h_
#define _MrcDdrIoApiInt_h_
#include "MrcDdrIoOffsets.h"

/// Defines
#define FLL_REF_CLK            (400) // MHz
#define RX_PRE_SDL_DCC_LOOP     20
typedef enum {
  RxPreSdlDcc  = 0,                 // RX PRE SDL DCC
  RxPostSdlDcc = 1,                 // RX POST SDL DCC
} SDL_DCC;

/// Functions

/**
  This function updates the Gear specific fields in the DDRIO,
  based on Outputs->Gear2 and Outputs->Gear4.

  @param[in]  MrcData - Pointer to global MRC data.
**/
VOID
DdrIoConfigGear (
  IN  MrcParameters *MrcData
  );

/**
  This function configures the DDR IO ODT type to Data and Comp blocks.
  VSS and VTT blocks are one time configured in MrcDdrIoInit.

  @param[in]  MrcData - Pointer to global MRC data.
  @param[in]  NewMode - ODT mode to enable.

  @retval MrcStatus - mrcSuccess if a valid ODT mode is requested, otherwise mrcWrongInputParameter.
**/
MrcStatus
MrcSetIoOdtMode (
  IN  MrcParameters *const  MrcData,
  IN  MRC_ODT_MODE_TYPE     NewMode
  );

/**
  This routine sets the Rx DQS VOC code

  @param[in]  MrcData     - Pointer to MRC global data.
  @param[in]  Controller  - Controller number.
  @param[in]  Channel     - Channel number.
  @param[in]  Rank        - Rank number.
  @param[in]  Strobe      - Strobe number.
  @param[in]  VocCode     - VOC code to program.
  @param[in]  Print       - Print the debug message or not.
**/
VOID
SetRxDqsVoc (
  IN OUT MrcParameters *const MrcData,
  IN UINT32                   Controller,
  IN UINT32                   Channel,
  IN UINT32                   Rank,
  IN UINT32                   Strobe,
  IN UINT8                    VocCode,
  IN BOOLEAN                  Print
  );

/**
  This routine gets the Rx DQS VOC code

  @param[in]  MrcData          - Pointer to MRC global data.
  @param[in]  Controller       - Controller number.
  @param[in]  Channel          - Channel number.
  @param[in]  Rank             - Rank number.
  @param[in]  Strobe           - Strobe number.
  @param[in]  Print            - Print debug message or not.
  @param[in]  RxPreSdlDccLoop  - The loop number to run Rx PRE SDL DCC for averaging.
  @param[in]  VocValue         - The RX DQS VOC value.

  @retval NA.
**/
VOID
GetRxDqsVoc (
  IN OUT MrcParameters *const MrcData,
  IN UINT32                   Controller,
  IN UINT32                   Channel,
  IN UINT32                   Rank,
  IN UINT32                   Strobe,
  IN BOOLEAN                  Print,
  IN UINT8                    RxPreSdlDccLoop,
  IN UINT32                   VocValue[MAX_CONTROLLER][MAX_CHANNEL][MAX_RANK_IN_CHANNEL][MAX_SDRAM_IN_DIMM][RX_PRE_SDL_DCC_LOOP]
  );

/**
  This routine programs the Rx DQS PI

  @param[in]  MrcData     - Pointer to MRC global data.
  @param[in]  PiCode      - Pi Code to program.
  @param[in]  Print       - Print debug message or not.
**/
VOID
SetRxPi (
  IN OUT MrcParameters *const MrcData,
  IN UINT32                   PiCode,
  IN BOOLEAN                  Print
  );

/**
  This routine runs Rx SDL DCC training

  @param[in]  MrcData    - Pointer to MRC global data.
  @param[in]  SdlDccType - SDL DCC training type: Pre SDL DCC or POST SDL DCC

  @retval     MrcStatus - if succeeded, return mrcSuccess
**/
MrcStatus
MrcRxSdlDccTraining (
  IN OUT MrcParameters *const MrcData,
  IN     SDL_DCC              SdlDccType
  );

/**
  This routine will setup or clean up CPGC for Rx POST SDL DCC

  @param[in]  MrcData      - Pointer to MRC global data.
  @param[in]  Enable       - CPGC setup for Rx POST SDL DCC or clean up to exit Rx POST SDL DCC.
  @param[in]  Rank         - Rank number.
  @param[in]  McChBitMask  - Pointer to the Channel bit mask.
  @param[in]  SaveData     - Pointer to the SaveData used for SetupMcMprConfig.

**/
VOID
CpgcSetupPostSdlDcc (
  IN OUT MrcParameters *const MrcData,
  IN BOOLEAN                  Enable,
  IN UINT32                   Rank,
  UINT8                       *McChBitMask,
  MC_MPR_CONFIG_SAVE          *SaveData
  );

#endif //_MrcDdrIoApiInt_h_
