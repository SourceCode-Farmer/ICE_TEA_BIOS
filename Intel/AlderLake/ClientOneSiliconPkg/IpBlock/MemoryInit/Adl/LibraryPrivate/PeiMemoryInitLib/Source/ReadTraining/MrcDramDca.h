/** @file
  DRAM DCA training definitions.

@copyright
  INTEL CONFIDENTIAL
  Copyright 1999 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains an 'Intel Peripheral Driver' and is uniquely identified as
  "Intel Reference Module" and is licensed for Intel CPUs and chipsets under
  the terms of your license agreement with Intel or your vendor. This file may
  be modified by the user, subject to additional terms of the license agreement.

@par Specification Reference:
**/
#ifndef _MrcDramDca_h_
#define _MrcDramDca_h_

//
// DRAM Duty Cycle Adjuster Settings limits for QCLK, IBCLK and QBCLK
//
#define  MIN_DRAM_CLOCK_DCA  (-4)
#define  MAX_DRAM_CLOCK_DCA  (7)

//
// MR42.dca_training_assist_mode definitions
//
#define DCA_TRAINING_ASSIST_DISABLE 0
#define DCA_TRAINING_ASSIST_IBCLK   1
#define DCA_TRAINING_ASSIST_ICLK    2

//
// DCA sweep values for per-device registers MR43 and MR44
//
#define DRAM_DCA_SWEEP_START      MIN_DRAM_CLOCK_DCA
#define DRAM_DCA_SWEEP_MAX        MAX_DRAM_CLOCK_DCA
#define DRAM_DCA_SWEEP_STEP_SIZE  1
#define DRAM_DCA_SWEEP_SIZE       ((DRAM_DCA_SWEEP_MAX - DRAM_DCA_SWEEP_START + 1 + (DRAM_DCA_SWEEP_STEP_SIZE - 1)) / DRAM_DCA_SWEEP_STEP_SIZE)
//
// Convert sweep index to DCA value to be programmed
//
#define SWEEP_INDEX_TO_DCA_VALUE(_Index) ((INT16)_Index + DRAM_DCA_SWEEP_START)

//
// The DRAM is aligned to the phase of ICLK or IBCLK
//
#define ICLK_PHASE_ALIGNED    0
#define IBCLK_PHASE_ALIGNED   1
#define DDR5_BURST_LENGTH     16
#define MAX_EYE_WIDTH         MAX_INT16
#define MAX_UI                DDR5_BURST_LENGTH
#define FILTERING_ONE         1
#define FILTERING_THREE       3
#define RMPR_DQS_START_DCA    0
#define RMPR_DQS_STOP_DCA     128
#define MAX_EW_DIFF_NUM       2
#define DDR5_LFSR0_5A         0x5A
#define DDR5_LFSR1_3C         0x3C
#define LP5_MR30_POSTIVE_OFFSET 8

typedef enum {
  DramDcaSingleorTwoClk = 0,        ///< DCA single/two-phase internal clock
  DramDcaQClk           = 1,        ///< DCA QCLK
  DramDcaIbClk          = 2,        ///< DCA IBCLK.
  DramDcaQbClk          = 3         ///< DCA QBCLK.
} DCA_GT;

typedef enum {
  DcaNotSupported = 0,
  DcaSingleOrTwoPhase = 1,
  DcaFourPhase = 2,
  MaxDcaType
} DCA_TYPE;


typedef struct {
  INT16      EyeWidth[MAX_CONTROLLER][MAX_CHANNEL][MAX_RANK_IN_CHANNEL][MAX_SDRAM_IN_DIMM];
} EYE_WIDTH_STROBE_ALL;

typedef struct {
  //
  // Track eye width for each Channel, Rank, Device, UI(Chunk)
  // Burst length is 16 for DDR5 but upper 8 and lower 8 chunks are measured together.
  // i.e. EyeWidthAllStrobe[0] is the eye width for chunks 0 and 8
  //
  EYE_WIDTH_STROBE_ALL      EyeWidthAllStrobe[MAX_UI / 2];
} EYE_WIDTH_UI_ALL;

typedef struct {
  //
  // Track eye width for each Channel, Rank, Device, UI(Chunk)
  // Burst length is 16 for DDR5 but upper 8 and lower 8 chunks are measured together.
  // For example, EyeWidthAllStrobe[0] is the eye width for chunks 0 and 8
  //
  INT16  EwForScoring[DRAM_DCA_SWEEP_SIZE][MAX_UI / 4];
  UINT16 DcaScore[DRAM_DCA_SWEEP_SIZE][2];
} DCA_SCORE;

//
// Structure to track CLK phase alignment for each DRAM
// ICLK_PHASE_ALIGNED or IBCLK_PHASE_ALIGNED
//
typedef struct {
  UINT8   DcaClkAlignment[MAX_CONTROLLER][MAX_CHANNEL][MAX_RANK_IN_CHANNEL];
} DCA_CLK_PHASE_ALIGNMENT;

//
// Private structure to pass private data around this module using the
// public CommonParameters pointer
//
typedef struct _DRAM_DCA_TRAINING_Data {

  //
  // Type of training to be performed - depends on what the DIMMs support
  //
  DCA_TYPE                    DcaType;
  //
  // Group under test (the DCA control) - DramDcaSingleorTwoClk, DramDcaQClk, DramDcaIbClk or DramDcaQbClk
  //
  DCA_GT                      Group;
  //
  // Mask of channels that support the training (0 = training supported)
  //
  UINT32                      ChannelMask;
  //
  // Mask of ranks that support the training (0 = training supported)
  //
  UINT32                      RankMask[MAX_CONTROLLER][MAX_CHANNEL];
  //
  // Each DRAM is phase aligned to either ICLK or IBCLK
  //
  DCA_CLK_PHASE_ALIGNMENT      ClkPhaseAlignment[MAX_STROBE];
  //
  // The eyewidth is measured for only a single UI(Chunk) per test
  //
  UINT32                      UiIndex;
} DRAM_DCA_TRAINING_Data;

/**

  Read the DRAM DCA capability that it supports DCA or not, two phases or four phases for DDR5.

  @param[in, out] MrcData     - Pointer to MRC global data.
  @param Mr42Data             - The array that strores the DDR5 MR42 data.

  @retval NA

**/
VOID
ReadDcaCapability (
  IN  MrcParameters *const  MrcData,
  IN  UINT8                 Mr42Data[MAX_CONTROLLER][MAX_CHANNEL][MAX_RANK_IN_CHANNEL]
  );

/**

  DRAM DCA Training - Four-Phase Internal Clock flow

  @param[in, out] MrcData  - Pointer to MRC global data.

  @retval mrcSucess  Training succeeds or is unsupported by the DRAM
  @retval mrcFail  Training fails

**/
MrcStatus
DramDcaFourPhaseTraining (
  IN  MrcParameters *const  MrcData
  );

/**
  Perform DCA read margin using normal read mpr pattern.

  @param[in] MrcData              - Pointer to MRC global data.
  @param[in] Param                - Indication to optimize P/N or both.
  @param[in] Print                - Print debug messages.
  @param[in] UiIndex              - UI index.
  @param[in] EyeWidthAllUi        - Pointer to the data struct to save the EW of the training.
  @param[in] MarginOnly           - Margin only or need to return the trained value.
  @param[in] DramDcaTrainingData  - Pointer to DRAM DCA training context
  @param[in] SimpleCheck          - Simple check or not. Simple check is used to check the clock phase alignment.

  @retval MrcStatus - mrcSuccess or reason for failure.
**/
MrcStatus
MrcReadMprTrainingNormalDcaMargin (
  IN OUT MrcParameters    *const MrcData,
  IN     UINT8                   Param,
  IN     BOOLEAN                 PerBit,
  IN     BOOLEAN                 Print,
  IN     UINT32                  UiIndex,
  IN OUT EYE_WIDTH_UI_ALL        *EyeWidthAllUi,
  IN     BOOLEAN                 MarginOnly,
  IN     DRAM_DCA_TRAINING_Data  *DramDcaTrainingData,
  IN     BOOLEAN                 SimpleCheck
  );

/**

  DRAM DCA Training - Single or Two-Phase Internal Clock flow

  @param[in, out] MrcData  - Pointer to MRC global data.

  @retval mrcSucess  Training succeeds or is unsupported by the DRAM
  @retval mrcFail  Training fails

**/
MrcStatus
DramDcaTwoPhaseTraining (
  IN  MrcParameters *const  MrcData
  );

/**
  DRAM DCA Training for DDR5 and LP5

  @param[in] MrcData       - Include all MRC global data.

  @retval MrcStatus       - if it's successful, return mrcSuccess, otherwise return reason for failure.
**/
MrcStatus
MrcDramDcaTraining (
  IN     MrcParameters *const MrcData
  );

/**
  Set the DRAM DCA value for DDR5

  @param[in,out] MrcData             - Include all MRC global data.
  @param[in]     Controller          - Selecting which Controller to talk to.
  @param[in]     Channel             - Selecting which Channel to talk to.
  @param[in]     RankMask            - Selecting which Ranks to talk to.
  @param[in]     DeviceMask          - Selecting which Devices to talk to (only valid for DDR4 and adjusting VrefDQ).
  @param[in]     Offset              - DCA value.
  @param[in]     UpdateMrcData       - Used to decide if Mrc host must be updated.
  @param[in]     PdaMode             - Selecting to use MRH or old method for MRS (only valid for DDR4 and adjusting VrefDQ).
  @param[in]     DcaGroup            - DCA group.

  @retval Nothing.
**/
void
MrcSetDramDca (
  IN OUT MrcParameters *const MrcData,
  IN     UINT8                Controller,
  IN     UINT8                Channel,
  IN     UINT8                RankMask,
  IN     UINT16               DeviceMask,
  IN     INT8                 Offset,
  IN     BOOLEAN              UpdateMrcData,
  IN     BOOLEAN              PdaMode,
  IN     DCA_GT               DcaGroup
  );

#endif // _MrcDramDca_h_
