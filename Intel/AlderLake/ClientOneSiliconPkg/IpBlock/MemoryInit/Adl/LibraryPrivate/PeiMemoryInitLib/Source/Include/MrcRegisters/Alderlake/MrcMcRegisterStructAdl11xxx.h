#ifndef __MrcMcRegisterStructAdl11xxx_h__
#define __MrcMcRegisterStructAdl11xxx_h__
/** @file
  This file was automatically generated. Modify at your own risk.
  Note that no error checking is done in these functions so ensure that the correct values are passed.

@copyright
  Copyright (c) 2010 - 2020 Intel Corporation. All rights reserved
  This software and associated documentation (if any) is furnished
  under a license and may only be used or copied in accordance
  with the terms of the license. Except as permitted by the
  license, no part of this software or documentation may be
  reproduced, stored in a retrieval system, or transmitted in any
  form or by any means without the express written consent of
  Intel Corporation.
  This file contains an 'Intel Peripheral Driver' and is uniquely
  identified as "Intel Reference Module" and is licensed for Intel
  CPUs and chipsets under the terms of your license agreement with
  Intel or your vendor. This file may be modified by the user, subject
  to additional terms of the license agreement.

@par Specification Reference:
**/

#pragma pack(push, 1)
typedef union {
  struct {
    UINT32 SAI_MASKLow                             :  32;  // Bits 31:0
    UINT32 SAI_MASKHigh                            :  32;  // Bits 63:32
  } Bits;
  UINT64 Data;
  UINT32 Data32[2];
  UINT16 Data16[4];
  UINT8  Data8[8];
} CMF_0_LT_SA_W_PG_ACCESS_POLICY_CP_STRUCT;
typedef union {
  struct {
    UINT32 SAI_MASKLow                             :  32;  // Bits 31:0
    UINT32 SAI_MASKHigh                            :  32;  // Bits 63:32
  } Bits;
  UINT64 Data;
  UINT32 Data32[2];
  UINT16 Data16[4];
  UINT8  Data8[8];
} CMF_0_LT_SA_W_PG_ACCESS_POLICY_WAC_STRUCT;
typedef union {
  struct {
    UINT32 SAI_MASKLow                             :  32;  // Bits 31:0
    UINT32 SAI_MASKHigh                            :  32;  // Bits 63:32
  } Bits;
  UINT64 Data;
  UINT32 Data32[2];
  UINT16 Data16[4];
  UINT8  Data8[8];
} CMF_0_LT_SA_W_PG_ACCESS_POLICY_RAC_STRUCT;

typedef CMF_0_LT_SA_W_PG_ACCESS_POLICY_CP_STRUCT CMF_0_P_U_CODE_PG_ACCESS_POLICY_CP_STRUCT;
typedef union {
  struct {
    UINT32 SAI_MASKLow                             :  32;  // Bits 31:0
    UINT32 SAI_MASKHigh                            :  32;  // Bits 63:32
  } Bits;
  UINT64 Data;
  UINT32 Data32[2];
  UINT16 Data16[4];
  UINT8  Data8[8];
} CMF_0_P_U_CODE_PG_ACCESS_POLICY_WAC_STRUCT;

typedef CMF_0_P_U_CODE_PG_ACCESS_POLICY_WAC_STRUCT CMF_0_P_U_CODE_PG_ACCESS_POLICY_RAC_STRUCT;

typedef CMF_0_LT_SA_W_PG_ACCESS_POLICY_CP_STRUCT CMF_0_OS_PG_ACCESS_POLICY_CP_STRUCT;
typedef union {
  struct {
    UINT32 SAI_MASKLow                             :  32;  // Bits 31:0
    UINT32 SAI_MASKHigh                            :  32;  // Bits 63:32
  } Bits;
  UINT64 Data;
  UINT32 Data32[2];
  UINT16 Data16[4];
  UINT8  Data8[8];
} CMF_0_OS_PG_ACCESS_POLICY_WAC_STRUCT;

typedef CMF_0_LT_SA_W_PG_ACCESS_POLICY_RAC_STRUCT CMF_0_OS_PG_ACCESS_POLICY_RAC_STRUCT;
typedef union {
  struct {
    UINT32 NON_VC1_PRIO_THRESHOLD                  :  16;  // Bits 15:0
    UINT32 VC1_RD_PRIO_THRESHOLD                   :  16;  // Bits 31:16
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} CMI_0_PRIO_THRESHOLD_STRUCT;
typedef union {
  struct {
    UINT32 LOW_PRIORITY_LIM                        :  16;  // Bits 15:0
    UINT32 HIGH_PRIORITY_LIM                       :  16;  // Bits 31:16
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} CMI_0_PRIO_LIM_STRUCT;
typedef union {
  struct {
    UINT32 SEL_2LM_1LM                             :  1;  // Bits 0:0
    UINT32 SEL_TMECC_ON                            :  1;  // Bits 1:1
    UINT32 Reserved_0                              :  6;  // Bits 7:2
    UINT32 SLICE_0_DISABLE                         :  1;  // Bits 8:8
    UINT32 SLICE_1_DISABLE                         :  1;  // Bits 9:9
    UINT32 Reserved_1                              :  22;  // Bits 31:10
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} CMF_0_TOPOLOGY_GLOBAL_CFG_0_STRUCT;
typedef union {
  struct {
    UINT32 Reserved_0                              :  10;  // Bits 9:0
    UINT32 AGENT_WR_RSP                            :  5;  // Bits 14:10
    UINT32 Reserved_1                              :  5;  // Bits 19:15
    UINT32 REQUESTOR_MUX_DEMUX_1                   :  2;  // Bits 21:20
    UINT32 REQUESTOR_MUX_DEMUX_0                   :  2;  // Bits 23:22
    UINT32 RESPONDER_MUX_DEMUX_1                   :  2;  // Bits 25:24
    UINT32 RESPONDER_MUX_DEMUX_0                   :  2;  // Bits 27:26
    UINT32 Reserved_2                              :  4;  // Bits 31:28
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} CMF_0_TOPOLOGY_GLOBAL_CFG_1_STRUCT;
typedef union {
  struct {
    UINT32 Reserved_0                              :  10;  // Bits 9:0
    UINT32 AGENT_WR_RSP                            :  5;  // Bits 14:10
    UINT32 Reserved_1                              :  5;  // Bits 19:15
    UINT32 REQUESTOR_MUX_DEMUX_1                   :  2;  // Bits 21:20
    UINT32 REQUESTOR_MUX_DEMUX_0                   :  2;  // Bits 23:22
    UINT32 RESPONDER_MUX_DEMUX_1                   :  2;  // Bits 25:24
    UINT32 RESPONDER_MUX_DEMUX_0                   :  2;  // Bits 27:26
    UINT32 Reserved_2                              :  4;  // Bits 31:28
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} CMF_0_TOPOLOGY_GLOBAL_CFG_2_STRUCT;
typedef union {
  struct {
    UINT32 Reserved_0                              :  10;  // Bits 9:0
    UINT32 AGENT_WR_RSP                            :  5;  // Bits 14:10
    UINT32 Reserved_1                              :  5;  // Bits 19:15
    UINT32 REQUESTOR_MUX_DEMUX_1                   :  2;  // Bits 21:20
    UINT32 REQUESTOR_MUX_DEMUX_0                   :  2;  // Bits 23:22
    UINT32 RESPONDER_MUX_DEMUX_1                   :  2;  // Bits 25:24
    UINT32 RESPONDER_MUX_DEMUX_0                   :  2;  // Bits 27:26
    UINT32 Reserved_2                              :  4;  // Bits 31:28
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} CMF_0_TOPOLOGY_GLOBAL_CFG_3_STRUCT;
typedef union {
  struct {
    UINT32 Reserved_0                              :  10;  // Bits 9:0
    UINT32 AGENT_WR_RSP                            :  5;  // Bits 14:10
    UINT32 Reserved_1                              :  5;  // Bits 19:15
    UINT32 REQUESTOR_MUX_DEMUX_1                   :  2;  // Bits 21:20
    UINT32 REQUESTOR_MUX_DEMUX_0                   :  2;  // Bits 23:22
    UINT32 RESPONDER_MUX_DEMUX_1                   :  2;  // Bits 25:24
    UINT32 RESPONDER_MUX_DEMUX_0                   :  2;  // Bits 27:26
    UINT32 Reserved_2                              :  4;  // Bits 31:28
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} CMF_0_TOPOLOGY_GLOBAL_CFG_4_STRUCT;
typedef union {
  struct {
    UINT32 ISM_IDLE_TIME                           :  8;  // Bits 7:0
    UINT32 FIXED_WINDOW                            :  1;  // Bits 8:8
    UINT32 CLK_GATE_EN                             :  1;  // Bits 9:9
    UINT32 FORCE_ISM_ACTIVE                        :  1;  // Bits 10:10
    UINT32 BYPASS_EN                               :  1;  // Bits 11:11
    UINT32 DIS_PERF_COUNTERS                       :  1;  // Bits 12:12
    UINT32 DIS_GLBDRV_GATING                       :  1;  // Bits 13:13
    UINT32 DIS_RSP_PORT_PRE_VALID                  :  1;  // Bits 14:14
    UINT32 DIS_RSP_PORT_BYPASS_PRE_VALID           :  1;  // Bits 15:15
    UINT32 RESERVED                                :  16;  // Bits 31:16
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} CMF_0_GLOBAL_CFG_1_STRUCT;
typedef union {
  struct {
    UINT32 CGCTRL_CLKGATEDEF                       :  1;  // Bits 0:0
    UINT32 RSVD0                                   :  7;  // Bits 7:1
    UINT32 CGCTRL_CLKGATEN                         :  1;  // Bits 8:8
    UINT32 RSVD1                                   :  7;  // Bits 15:9
    UINT32 CGCTRL_IDLECNT                          :  8;  // Bits 23:16
    UINT32 RSVD2                                   :  8;  // Bits 31:24
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} CMI_0_IOSF_SB_EP_CTRL_STRUCT;
typedef union {
  struct {
    UINT32 CLK_GATE_EN                             :  32;  // Bits 31:0
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} CMI_0_CLK_GATE_EN_0_STRUCT;

typedef CMI_0_CLK_GATE_EN_0_STRUCT CMI_0_CLK_GATE_EN_1_STRUCT;
typedef union {
  struct {
    UINT32 RESERVED                                :  6;  // Bits 5:0
    UINT32 vc1wr_window                            :  8;  // Bits 13:6
    UINT32 update_vc1wr_window                     :  1;  // Bits 14:14
    UINT32 vc1wr_window_fix_disable                :  1;  // Bits 15:15
    UINT32 vc0wr_const_prio_fix_en                 :  1;  // Bits 16:16
    UINT32 gldv_gating_disable                     :  1;  // Bits 17:17
    UINT32 iop_ep_req_ch0_rd_park_size             :  4;  // Bits 21:18
    UINT32 iop_ep_req_ch0_wr_park_size             :  4;  // Bits 25:22
    UINT32 RESERVED1                               :  6;  // Bits 31:26
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} CMF_0_RSVD0_STRUCT;
typedef union {
  struct {
    UINT32 RESERVED                                :  32;  // Bits 31:0
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} CMF_0_RSVD1_STRUCT;
typedef union {
  struct {
    UINT32 REQ_WR_PARK_SIZE                        :  4;  // Bits 3:0
    UINT32 RESERVED0                               :  4;  // Bits 7:4
    UINT32 REQ_RD_PARK_SIZE                        :  4;  // Bits 11:8
    UINT32 RESERVED1                               :  4;  // Bits 15:12
    UINT32 REQ_WR_MAX_CREDIT                       :  8;  // Bits 23:16
    UINT32 REQ_RD_MAX_CREDIT                       :  8;  // Bits 31:24
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} CMI_0_REQUEST_PORT_0_REQ_CONFIG_0_CHANNEL_0_STRUCT;

typedef CMI_0_REQUEST_PORT_0_REQ_CONFIG_0_CHANNEL_0_STRUCT CMI_0_REQUEST_PORT_0_REQ_CONFIG_0_CHANNEL_1_STRUCT;
typedef union {
  struct {
    UINT32 REQ_WR_PARK_SIZE                        :  4;  // Bits 3:0
    UINT32 RESERVED0                               :  4;  // Bits 7:4
    UINT32 REQ_RD_PARK_SIZE                        :  4;  // Bits 11:8
    UINT32 RESERVED1                               :  4;  // Bits 15:12
    UINT32 REQ_WR_MAX_CREDIT                       :  8;  // Bits 23:16
    UINT32 REQ_RD_MAX_CREDIT                       :  8;  // Bits 31:24
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} CMI_0_REQUEST_PORT_0_REQ_CONFIG_0_CHANNEL_2_STRUCT;

typedef CMI_0_REQUEST_PORT_0_REQ_CONFIG_0_CHANNEL_2_STRUCT CMI_0_REQUEST_PORT_0_REQ_CONFIG_0_CHANNEL_3_STRUCT;
typedef union {
  struct {
    UINT32 REQ_WR_QUEUEDEPTH                       :  8;  // Bits 7:0
    UINT32 REQ_RD_QUEUEDEPTH                       :  8;  // Bits 15:8
    UINT32 RESERVED1                               :  16;  // Bits 31:16
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} CMI_0_REQUEST_PORT_0_REQ_CONFIG_1_STRUCT;
typedef union {
  struct {
    UINT32 REQ_STALL                               :  1;  // Bits 0:0
    UINT32 RSVD0                                   :  4;  // Bits 4:1
    UINT32 RSVD1                                   :  3;  // Bits 7:5
    UINT32 UNSTALL_COUNTER_VALUE                   :  16;  // Bits 23:8
    UINT32 RSVD2                                   :  8;  // Bits 31:24
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} CMI_0_REQUEST_PORT_0_STALL_STRUCT;

typedef CMI_0_REQUEST_PORT_0_REQ_CONFIG_0_CHANNEL_0_STRUCT CMI_0_REQUEST_PORT_1_REQ_CONFIG_0_CHANNEL_0_STRUCT;

typedef CMI_0_REQUEST_PORT_0_REQ_CONFIG_0_CHANNEL_0_STRUCT CMI_0_REQUEST_PORT_1_REQ_CONFIG_0_CHANNEL_1_STRUCT;

typedef CMI_0_REQUEST_PORT_0_REQ_CONFIG_0_CHANNEL_2_STRUCT CMI_0_REQUEST_PORT_1_REQ_CONFIG_0_CHANNEL_2_STRUCT;

typedef CMI_0_REQUEST_PORT_0_REQ_CONFIG_0_CHANNEL_2_STRUCT CMI_0_REQUEST_PORT_1_REQ_CONFIG_0_CHANNEL_3_STRUCT;

typedef CMI_0_REQUEST_PORT_0_REQ_CONFIG_1_STRUCT CMI_0_REQUEST_PORT_1_REQ_CONFIG_1_STRUCT;

typedef CMI_0_REQUEST_PORT_0_STALL_STRUCT CMI_0_REQUEST_PORT_1_STALL_STRUCT;

typedef CMI_0_REQUEST_PORT_0_REQ_CONFIG_0_CHANNEL_0_STRUCT CMI_0_REQUEST_PORT_2_REQ_CONFIG_0_CHANNEL_0_STRUCT;

typedef CMI_0_REQUEST_PORT_0_REQ_CONFIG_0_CHANNEL_0_STRUCT CMI_0_REQUEST_PORT_2_REQ_CONFIG_0_CHANNEL_1_STRUCT;

typedef CMI_0_REQUEST_PORT_0_REQ_CONFIG_0_CHANNEL_2_STRUCT CMI_0_REQUEST_PORT_2_REQ_CONFIG_0_CHANNEL_2_STRUCT;

typedef CMI_0_REQUEST_PORT_0_REQ_CONFIG_0_CHANNEL_2_STRUCT CMI_0_REQUEST_PORT_2_REQ_CONFIG_0_CHANNEL_3_STRUCT;

typedef CMI_0_REQUEST_PORT_0_REQ_CONFIG_1_STRUCT CMI_0_REQUEST_PORT_2_REQ_CONFIG_1_STRUCT;

typedef CMI_0_REQUEST_PORT_0_STALL_STRUCT CMI_0_REQUEST_PORT_2_STALL_STRUCT;
typedef union {
  struct {
    UINT32 WR_RSP_PARK_SIZE                        :  4;  // Bits 3:0
    UINT32 RESERVED0                               :  4;  // Bits 7:4
    UINT32 RD_CPL_PARK_SIZE                        :  4;  // Bits 11:8
    UINT32 RESERVED1                               :  4;  // Bits 15:12
    UINT32 RSP_MAX_CREDIT                          :  8;  // Bits 23:16
    UINT32 RD_CPL_MAX_CREDIT                       :  8;  // Bits 31:24
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} CMI_0_RESPOND_PORT_0_CPL_CONFIG_0_CHANNEL_0_STRUCT;

typedef CMI_0_RESPOND_PORT_0_CPL_CONFIG_0_CHANNEL_0_STRUCT CMI_0_RESPOND_PORT_0_CPL_CONFIG_0_CHANNEL_1_STRUCT;

typedef CMI_0_RESPOND_PORT_0_CPL_CONFIG_0_CHANNEL_0_STRUCT CMI_0_RESPOND_PORT_0_CPL_CONFIG_0_CHANNEL_2_STRUCT;

typedef CMI_0_RESPOND_PORT_0_CPL_CONFIG_0_CHANNEL_0_STRUCT CMI_0_RESPOND_PORT_0_CPL_CONFIG_0_CHANNEL_3_STRUCT;
typedef union {
  struct {
    UINT32 RD_CPL_QUEUEDEPTH                       :  8;  // Bits 7:0
    UINT32 RSP_QUEUEDEPTH                          :  8;  // Bits 15:8
    UINT32 REQ_FAST_WAKE                           :  1;  // Bits 16:16
    UINT32 RESERVED1                               :  15;  // Bits 31:17
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} CMI_0_RESPOND_PORT_0_CPL_CONFIG_1_STRUCT;
typedef union {
  struct {
    UINT32 RSVD0                                   :  4;  // Bits 3:0
    UINT32 RSP_RD_CPL_STALL                        :  1;  // Bits 4:4
    UINT32 RSVD1                                   :  3;  // Bits 7:5
    UINT32 UNSTALL_COUNTER_VALUE                   :  16;  // Bits 23:8
    UINT32 RSVD2                                   :  8;  // Bits 31:24
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} CMI_0_RESPOND_PORT_0_STALL_STRUCT;

typedef CMI_0_RESPOND_PORT_0_CPL_CONFIG_0_CHANNEL_0_STRUCT CMI_0_RESPOND_PORT_1_CPL_CONFIG_0_CHANNEL_0_STRUCT;

typedef CMI_0_RESPOND_PORT_0_CPL_CONFIG_0_CHANNEL_0_STRUCT CMI_0_RESPOND_PORT_1_CPL_CONFIG_0_CHANNEL_1_STRUCT;

typedef CMI_0_RESPOND_PORT_0_CPL_CONFIG_0_CHANNEL_0_STRUCT CMI_0_RESPOND_PORT_1_CPL_CONFIG_0_CHANNEL_2_STRUCT;

typedef CMI_0_RESPOND_PORT_0_CPL_CONFIG_0_CHANNEL_0_STRUCT CMI_0_RESPOND_PORT_1_CPL_CONFIG_0_CHANNEL_3_STRUCT;

typedef CMI_0_RESPOND_PORT_0_CPL_CONFIG_1_STRUCT CMI_0_RESPOND_PORT_1_CPL_CONFIG_1_STRUCT;

typedef CMI_0_RESPOND_PORT_0_STALL_STRUCT CMI_0_RESPOND_PORT_1_STALL_STRUCT;
typedef union {
  struct {
    UINT32 READ_COUNTERLow                         :  32;  // Bits 31:0
    UINT32 READ_COUNTERHigh                        :  32;  // Bits 63:32
  } Bits;
  UINT64 Data;
  UINT32 Data32[2];
  UINT16 Data16[4];
  UINT8  Data8[8];
} CMI_0_RESPOND_PORT_0_RD_COUNTER_CHANNEL_0_STRUCT;

typedef CMI_0_RESPOND_PORT_0_RD_COUNTER_CHANNEL_0_STRUCT CMI_0_RESPOND_PORT_0_RD_COUNTER_CHANNEL_1_STRUCT;

typedef CMI_0_RESPOND_PORT_0_RD_COUNTER_CHANNEL_0_STRUCT CMI_0_RESPOND_PORT_0_RD_COUNTER_CHANNEL_2_STRUCT;

typedef CMI_0_RESPOND_PORT_0_RD_COUNTER_CHANNEL_0_STRUCT CMI_0_RESPOND_PORT_0_RD_COUNTER_CHANNEL_3_STRUCT;
typedef union {
  struct {
    UINT32 WRITE_COUNTERLow                        :  32;  // Bits 31:0
    UINT32 WRITE_COUNTERHigh                       :  32;  // Bits 63:32
  } Bits;
  UINT64 Data;
  UINT32 Data32[2];
  UINT16 Data16[4];
  UINT8  Data8[8];
} CMI_0_RESPOND_PORT_0_WR_COUNTER_CHANNEL_0_STRUCT;

typedef CMI_0_RESPOND_PORT_0_WR_COUNTER_CHANNEL_0_STRUCT CMI_0_RESPOND_PORT_0_WR_COUNTER_CHANNEL_1_STRUCT;

typedef CMI_0_RESPOND_PORT_0_WR_COUNTER_CHANNEL_0_STRUCT CMI_0_RESPOND_PORT_0_WR_COUNTER_CHANNEL_2_STRUCT;

typedef CMI_0_RESPOND_PORT_0_WR_COUNTER_CHANNEL_0_STRUCT CMI_0_RESPOND_PORT_0_WR_COUNTER_CHANNEL_3_STRUCT;

typedef CMI_0_RESPOND_PORT_0_RD_COUNTER_CHANNEL_0_STRUCT CMI_0_RESPOND_PORT_1_RD_COUNTER_CHANNEL_0_STRUCT;

typedef CMI_0_RESPOND_PORT_0_RD_COUNTER_CHANNEL_0_STRUCT CMI_0_RESPOND_PORT_1_RD_COUNTER_CHANNEL_1_STRUCT;

typedef CMI_0_RESPOND_PORT_0_RD_COUNTER_CHANNEL_0_STRUCT CMI_0_RESPOND_PORT_1_RD_COUNTER_CHANNEL_2_STRUCT;

typedef CMI_0_RESPOND_PORT_0_RD_COUNTER_CHANNEL_0_STRUCT CMI_0_RESPOND_PORT_1_RD_COUNTER_CHANNEL_3_STRUCT;

typedef CMI_0_RESPOND_PORT_0_WR_COUNTER_CHANNEL_0_STRUCT CMI_0_RESPOND_PORT_1_WR_COUNTER_CHANNEL_0_STRUCT;

typedef CMI_0_RESPOND_PORT_0_WR_COUNTER_CHANNEL_0_STRUCT CMI_0_RESPOND_PORT_1_WR_COUNTER_CHANNEL_1_STRUCT;

typedef CMI_0_RESPOND_PORT_0_WR_COUNTER_CHANNEL_0_STRUCT CMI_0_RESPOND_PORT_1_WR_COUNTER_CHANNEL_2_STRUCT;

typedef CMI_0_RESPOND_PORT_0_WR_COUNTER_CHANNEL_0_STRUCT CMI_0_RESPOND_PORT_1_WR_COUNTER_CHANNEL_3_STRUCT;
typedef union {
  struct {
    UINT32 NonVC1Wr_HiPri_Park_Size                :  4;  // Bits 3:0
    UINT32 NonVC1Wr_LoPri_Park_Size                :  4;  // Bits 7:4
    UINT32 VC1Wr_HiPri_Park_Size                   :  4;  // Bits 11:8
    UINT32 VC1Wr_LoPri_Park_Size                   :  4;  // Bits 15:12
    UINT32 Reserved                                :  16;  // Bits 31:16
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} CMI_0_REQ_L3_ARB_CONFIG_STRUCT;
typedef union {
  struct {
    UINT32 ADDR_PARITY_CHK_MASK                    :  8;  // Bits 7:0
    UINT32 ADDR_PARITY_GEN_MASK                    :  8;  // Bits 15:8
    UINT32 DIS_PARITY_PCH_EVENT                    :  1;  // Bits 16:16
    UINT32 DIS_PARITY_LOG                          :  1;  // Bits 17:17
    UINT32 DIS_MULTIPLE_PCH_MSG                    :  1;  // Bits 18:18
    UINT32 RESERVED                                :  12;  // Bits 30:19
    UINT32 PARITY_EN                               :  1;  // Bits 31:31
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} CMI_0_PARITY_CONTROL_STRUCT;
typedef union {
  struct {
    UINT32 Reserved_0                              :  6;  // Bits 5:0
    UINT32 ERR_ADDRESSLow                          :  26;  // Bits 31:6
    UINT32 ERR_ADDRESSHigh                         :  14;  // Bits 45:32
    UINT32 Reserved_1                              :  2;  // Bits 47:46
    UINT32 ERR_SRC                                 :  3;  // Bits 50:48
    UINT32 Reserved_2                              :  12;  // Bits 62:51
    UINT32 ERR_STS                                 :  1;  // Bits 63:63
  } Bits;
  UINT64 Data;
  UINT32 Data32[2];
  UINT16 Data16[4];
  UINT8  Data8[8];
} CMI_0_PARITY_ERR_LOG_STRUCT;
typedef union {
  struct {
    UINT32 Reserved_0                              :  8;  // Bits 7:0
    UINT32 ADDR_ERR_EN                             :  1;  // Bits 8:8
    UINT32 Reserved_1                              :  1;  // Bits 9:9
    UINT32 Reserved_2                              :  6;  // Bits 15:10
    UINT32 ERR_SRC_MASK                            :  8;  // Bits 23:16
    UINT32 Reserved_3                              :  8;  // Bits 31:24
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} CMI_0_PARITY_ERR_INJ_STRUCT;
typedef union {
  struct {
    UINT32 RESERVED                                :  32;  // Bits 31:0
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} CMI_0_REQUEST_PORT_0_RESERVED_0_STRUCT;

typedef CMI_0_REQUEST_PORT_0_RESERVED_0_STRUCT CMI_0_REQUEST_PORT_0_RESERVED_1_STRUCT;

typedef CMI_0_REQUEST_PORT_0_RESERVED_0_STRUCT CMI_0_REQUEST_PORT_0_RESERVED_2_STRUCT;
typedef union {
  struct {
    UINT32 SIGNATURE_VAL                           :  16;  // Bits 15:0
    UINT32 CLR                                     :  1;  // Bits 16:16
    UINT32 ENABLE                                  :  1;  // Bits 17:17
    UINT32 FUNC_OV                                 :  1;  // Bits 18:18
    UINT32 EVENT_COUNT                             :  12;  // Bits 30:19
    UINT32 SIGNATURE_TYPE                          :  1;  // Bits 31:31
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} CMI_0_REQUEST_PORT_0_REQ_IN_DFX_INSTR_DBG_STRUCT;

typedef CMI_0_REQUEST_PORT_0_REQ_IN_DFX_INSTR_DBG_STRUCT CMI_0_REQUEST_PORT_0_RD_CPL_OUT_DFX_INSTR_DBG_STRUCT;

typedef CMI_0_REQUEST_PORT_0_REQ_IN_DFX_INSTR_DBG_STRUCT CMI_0_REQUEST_PORT_0_RSP_OUT_DFX_INSTR_DBG_STRUCT;

typedef CMI_0_REQUEST_PORT_0_RESERVED_0_STRUCT CMI_0_REQUEST_PORT_1_RESERVED_0_STRUCT;

typedef CMI_0_REQUEST_PORT_0_RESERVED_0_STRUCT CMI_0_REQUEST_PORT_1_RESERVED_1_STRUCT;

typedef CMI_0_REQUEST_PORT_0_RESERVED_0_STRUCT CMI_0_REQUEST_PORT_1_RESERVED_2_STRUCT;

typedef CMI_0_REQUEST_PORT_0_REQ_IN_DFX_INSTR_DBG_STRUCT CMI_0_REQUEST_PORT_1_REQ_IN_DFX_INSTR_DBG_STRUCT;

typedef CMI_0_REQUEST_PORT_0_REQ_IN_DFX_INSTR_DBG_STRUCT CMI_0_REQUEST_PORT_1_RD_CPL_OUT_DFX_INSTR_DBG_STRUCT;

typedef CMI_0_REQUEST_PORT_0_REQ_IN_DFX_INSTR_DBG_STRUCT CMI_0_REQUEST_PORT_1_RSP_OUT_DFX_INSTR_DBG_STRUCT;

typedef CMI_0_REQUEST_PORT_0_RESERVED_0_STRUCT CMI_0_REQUEST_PORT_2_RESERVED_0_STRUCT;

typedef CMI_0_REQUEST_PORT_0_RESERVED_0_STRUCT CMI_0_REQUEST_PORT_2_RESERVED_1_STRUCT;

typedef CMI_0_REQUEST_PORT_0_RESERVED_0_STRUCT CMI_0_REQUEST_PORT_2_RESERVED_2_STRUCT;

typedef CMI_0_REQUEST_PORT_0_REQ_IN_DFX_INSTR_DBG_STRUCT CMI_0_REQUEST_PORT_2_REQ_IN_DFX_INSTR_DBG_STRUCT;

typedef CMI_0_REQUEST_PORT_0_REQ_IN_DFX_INSTR_DBG_STRUCT CMI_0_REQUEST_PORT_2_RD_CPL_OUT_DFX_INSTR_DBG_STRUCT;

typedef CMI_0_REQUEST_PORT_0_REQ_IN_DFX_INSTR_DBG_STRUCT CMI_0_REQUEST_PORT_2_RSP_OUT_DFX_INSTR_DBG_STRUCT;

typedef CMI_0_REQUEST_PORT_0_RESERVED_0_STRUCT CMI_0_RESPOND_PORT_0_RESERVED_0_STRUCT;

typedef CMI_0_REQUEST_PORT_0_RESERVED_0_STRUCT CMI_0_RESPOND_PORT_0_RESERVED_1_STRUCT;

typedef CMI_0_REQUEST_PORT_0_RESERVED_0_STRUCT CMI_0_RESPOND_PORT_0_RESERVED_2_STRUCT;

typedef CMI_0_REQUEST_PORT_0_REQ_IN_DFX_INSTR_DBG_STRUCT CMI_0_RESPOND_PORT_0_REQ_OUT_DFX_INSTR_DBG_STRUCT;

typedef CMI_0_REQUEST_PORT_0_REQ_IN_DFX_INSTR_DBG_STRUCT CMI_0_RESPOND_PORT_0_RD_CPL_IN_DFX_INSTR_DBG_STRUCT;

typedef CMI_0_REQUEST_PORT_0_REQ_IN_DFX_INSTR_DBG_STRUCT CMI_0_RESPOND_PORT_0_RSP_IN_DFX_INSTR_DBG_STRUCT;

typedef CMI_0_REQUEST_PORT_0_RESERVED_0_STRUCT CMI_0_RESPOND_PORT_1_RESERVED_0_STRUCT;

typedef CMI_0_REQUEST_PORT_0_RESERVED_0_STRUCT CMI_0_RESPOND_PORT_1_RESERVED_1_STRUCT;

typedef CMI_0_REQUEST_PORT_0_RESERVED_0_STRUCT CMI_0_RESPOND_PORT_1_RESERVED_2_STRUCT;

typedef CMI_0_REQUEST_PORT_0_REQ_IN_DFX_INSTR_DBG_STRUCT CMI_0_RESPOND_PORT_1_REQ_OUT_DFX_INSTR_DBG_STRUCT;

typedef CMI_0_REQUEST_PORT_0_REQ_IN_DFX_INSTR_DBG_STRUCT CMI_0_RESPOND_PORT_1_RD_CPL_IN_DFX_INSTR_DBG_STRUCT;

typedef CMI_0_REQUEST_PORT_0_REQ_IN_DFX_INSTR_DBG_STRUCT CMI_0_RESPOND_PORT_1_RSP_IN_DFX_INSTR_DBG_STRUCT;

typedef CMI_0_REQUEST_PORT_0_REQ_IN_DFX_INSTR_DBG_STRUCT CMI_0_REQUEST_PORT_0_REQ_DATA_IN_DFX_INSTR_DBG_STRUCT;

typedef CMI_0_REQUEST_PORT_0_REQ_IN_DFX_INSTR_DBG_STRUCT CMI_0_REQUEST_PORT_0_RD_CPL_DATA_OUT_DFX_INSTR_DBG_STRUCT;

typedef CMI_0_REQUEST_PORT_0_REQ_IN_DFX_INSTR_DBG_STRUCT CMI_0_REQUEST_PORT_1_REQ_DATA_IN_DFX_INSTR_DBG_STRUCT;

typedef CMI_0_REQUEST_PORT_0_REQ_IN_DFX_INSTR_DBG_STRUCT CMI_0_REQUEST_PORT_1_RD_CPL_DATA_OUT_DFX_INSTR_DBG_STRUCT;

typedef CMI_0_REQUEST_PORT_0_REQ_IN_DFX_INSTR_DBG_STRUCT CMI_0_REQUEST_PORT_2_REQ_DATA_IN_DFX_INSTR_DBG_STRUCT;

typedef CMI_0_REQUEST_PORT_0_REQ_IN_DFX_INSTR_DBG_STRUCT CMI_0_REQUEST_PORT_2_RD_CPL_DATA_OUT_DFX_INSTR_DBG_STRUCT;

typedef CMI_0_REQUEST_PORT_0_REQ_IN_DFX_INSTR_DBG_STRUCT CMI_0_RESPOND_PORT_0_REQ_DATA_OUT_DFX_INSTR_DBG_STRUCT;

typedef CMI_0_REQUEST_PORT_0_REQ_IN_DFX_INSTR_DBG_STRUCT CMI_0_RESPOND_PORT_0_RD_CPL_DATA_IN_DFX_INSTR_DBG_STRUCT;

typedef CMI_0_REQUEST_PORT_0_REQ_IN_DFX_INSTR_DBG_STRUCT CMI_0_RESPOND_PORT_1_REQ_DATA_OUT_DFX_INSTR_DBG_STRUCT;

typedef CMI_0_REQUEST_PORT_0_REQ_IN_DFX_INSTR_DBG_STRUCT CMI_0_RESPOND_PORT_1_RD_CPL_DATA_IN_DFX_INSTR_DBG_STRUCT;
typedef union {
  struct {
    UINT32 INSTR_DBG_MODE                          :  1;  // Bits 0:0
    UINT32 DBG_VALID_COUNT                         :  18;  // Bits 18:1
    UINT32 DBG_CYCLE_COUNT                         :  13;  // Bits 31:19
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} CMI_0_DFX_OBSERVE_STRUCT;
typedef union {
  struct {
    UINT32 MUX_DEMUX_STS                           :  1;  // Bits 0:0
    UINT32 RESERVED                                :  31;  // Bits 31:1
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} CMF_0_GLOBAL_STATUS_STRUCT;
typedef union {
  struct {
    UINT32 CREDITS_CONFIG_DONE                     :  1;  // Bits 0:0
    UINT32 RESERVED_0                              :  31;  // Bits 31:1
  } Bits;
  UINT32 Data;
  UINT16 Data16[2];
  UINT8  Data8[4];
} CMF_0_GLOBAL_CFG_STRUCT;

typedef CMF_0_LT_SA_W_PG_ACCESS_POLICY_CP_STRUCT CMF_1_LT_SA_W_PG_ACCESS_POLICY_CP_STRUCT;

typedef CMF_0_LT_SA_W_PG_ACCESS_POLICY_WAC_STRUCT CMF_1_LT_SA_W_PG_ACCESS_POLICY_WAC_STRUCT;

typedef CMF_0_LT_SA_W_PG_ACCESS_POLICY_RAC_STRUCT CMF_1_LT_SA_W_PG_ACCESS_POLICY_RAC_STRUCT;

typedef CMF_0_LT_SA_W_PG_ACCESS_POLICY_CP_STRUCT CMF_1_P_U_CODE_PG_ACCESS_POLICY_CP_STRUCT;

typedef CMF_0_P_U_CODE_PG_ACCESS_POLICY_WAC_STRUCT CMF_1_P_U_CODE_PG_ACCESS_POLICY_WAC_STRUCT;

typedef CMF_0_P_U_CODE_PG_ACCESS_POLICY_WAC_STRUCT CMF_1_P_U_CODE_PG_ACCESS_POLICY_RAC_STRUCT;

typedef CMF_0_LT_SA_W_PG_ACCESS_POLICY_CP_STRUCT CMF_1_OS_PG_ACCESS_POLICY_CP_STRUCT;

typedef CMF_0_OS_PG_ACCESS_POLICY_WAC_STRUCT CMF_1_OS_PG_ACCESS_POLICY_WAC_STRUCT;

typedef CMF_0_LT_SA_W_PG_ACCESS_POLICY_RAC_STRUCT CMF_1_OS_PG_ACCESS_POLICY_RAC_STRUCT;

typedef CMI_0_PRIO_THRESHOLD_STRUCT CMI_1_PRIO_THRESHOLD_STRUCT;

typedef CMI_0_PRIO_LIM_STRUCT CMI_1_PRIO_LIM_STRUCT;

typedef CMF_0_TOPOLOGY_GLOBAL_CFG_0_STRUCT CMF_1_TOPOLOGY_GLOBAL_CFG_0_STRUCT;

typedef CMF_0_TOPOLOGY_GLOBAL_CFG_1_STRUCT CMF_1_TOPOLOGY_GLOBAL_CFG_1_STRUCT;

typedef CMF_0_TOPOLOGY_GLOBAL_CFG_2_STRUCT CMF_1_TOPOLOGY_GLOBAL_CFG_2_STRUCT;

typedef CMF_0_TOPOLOGY_GLOBAL_CFG_3_STRUCT CMF_1_TOPOLOGY_GLOBAL_CFG_3_STRUCT;

typedef CMF_0_TOPOLOGY_GLOBAL_CFG_4_STRUCT CMF_1_TOPOLOGY_GLOBAL_CFG_4_STRUCT;

typedef CMF_0_GLOBAL_CFG_1_STRUCT CMF_1_GLOBAL_CFG_1_STRUCT;

typedef CMI_0_IOSF_SB_EP_CTRL_STRUCT CMI_1_IOSF_SB_EP_CTRL_STRUCT;

typedef CMI_0_CLK_GATE_EN_0_STRUCT CMI_1_CLK_GATE_EN_0_STRUCT;

typedef CMI_0_CLK_GATE_EN_0_STRUCT CMI_1_CLK_GATE_EN_1_STRUCT;

typedef CMF_0_RSVD0_STRUCT CMF_1_RSVD0_STRUCT;

typedef CMF_0_RSVD1_STRUCT CMF_1_RSVD1_STRUCT;

typedef CMI_0_REQUEST_PORT_0_REQ_CONFIG_0_CHANNEL_0_STRUCT CMI_1_REQUEST_PORT_0_REQ_CONFIG_0_CHANNEL_0_STRUCT;

typedef CMI_0_REQUEST_PORT_0_REQ_CONFIG_0_CHANNEL_0_STRUCT CMI_1_REQUEST_PORT_0_REQ_CONFIG_0_CHANNEL_1_STRUCT;

typedef CMI_0_REQUEST_PORT_0_REQ_CONFIG_0_CHANNEL_2_STRUCT CMI_1_REQUEST_PORT_0_REQ_CONFIG_0_CHANNEL_2_STRUCT;

typedef CMI_0_REQUEST_PORT_0_REQ_CONFIG_0_CHANNEL_2_STRUCT CMI_1_REQUEST_PORT_0_REQ_CONFIG_0_CHANNEL_3_STRUCT;

typedef CMI_0_REQUEST_PORT_0_REQ_CONFIG_1_STRUCT CMI_1_REQUEST_PORT_0_REQ_CONFIG_1_STRUCT;

typedef CMI_0_REQUEST_PORT_0_STALL_STRUCT CMI_1_REQUEST_PORT_0_STALL_STRUCT;

typedef CMI_0_REQUEST_PORT_0_REQ_CONFIG_0_CHANNEL_0_STRUCT CMI_1_REQUEST_PORT_1_REQ_CONFIG_0_CHANNEL_0_STRUCT;

typedef CMI_0_REQUEST_PORT_0_REQ_CONFIG_0_CHANNEL_0_STRUCT CMI_1_REQUEST_PORT_1_REQ_CONFIG_0_CHANNEL_1_STRUCT;

typedef CMI_0_REQUEST_PORT_0_REQ_CONFIG_0_CHANNEL_2_STRUCT CMI_1_REQUEST_PORT_1_REQ_CONFIG_0_CHANNEL_2_STRUCT;

typedef CMI_0_REQUEST_PORT_0_REQ_CONFIG_0_CHANNEL_2_STRUCT CMI_1_REQUEST_PORT_1_REQ_CONFIG_0_CHANNEL_3_STRUCT;

typedef CMI_0_REQUEST_PORT_0_REQ_CONFIG_1_STRUCT CMI_1_REQUEST_PORT_1_REQ_CONFIG_1_STRUCT;

typedef CMI_0_REQUEST_PORT_0_STALL_STRUCT CMI_1_REQUEST_PORT_1_STALL_STRUCT;

typedef CMI_0_REQUEST_PORT_0_REQ_CONFIG_0_CHANNEL_0_STRUCT CMI_1_REQUEST_PORT_2_REQ_CONFIG_0_CHANNEL_0_STRUCT;

typedef CMI_0_REQUEST_PORT_0_REQ_CONFIG_0_CHANNEL_0_STRUCT CMI_1_REQUEST_PORT_2_REQ_CONFIG_0_CHANNEL_1_STRUCT;

typedef CMI_0_REQUEST_PORT_0_REQ_CONFIG_0_CHANNEL_2_STRUCT CMI_1_REQUEST_PORT_2_REQ_CONFIG_0_CHANNEL_2_STRUCT;

typedef CMI_0_REQUEST_PORT_0_REQ_CONFIG_0_CHANNEL_2_STRUCT CMI_1_REQUEST_PORT_2_REQ_CONFIG_0_CHANNEL_3_STRUCT;

typedef CMI_0_REQUEST_PORT_0_REQ_CONFIG_1_STRUCT CMI_1_REQUEST_PORT_2_REQ_CONFIG_1_STRUCT;

typedef CMI_0_REQUEST_PORT_0_STALL_STRUCT CMI_1_REQUEST_PORT_2_STALL_STRUCT;

typedef CMI_0_RESPOND_PORT_0_CPL_CONFIG_0_CHANNEL_0_STRUCT CMI_1_RESPOND_PORT_0_CPL_CONFIG_0_CHANNEL_0_STRUCT;

typedef CMI_0_RESPOND_PORT_0_CPL_CONFIG_0_CHANNEL_0_STRUCT CMI_1_RESPOND_PORT_0_CPL_CONFIG_0_CHANNEL_1_STRUCT;

typedef CMI_0_RESPOND_PORT_0_CPL_CONFIG_0_CHANNEL_0_STRUCT CMI_1_RESPOND_PORT_0_CPL_CONFIG_0_CHANNEL_2_STRUCT;

typedef CMI_0_RESPOND_PORT_0_CPL_CONFIG_0_CHANNEL_0_STRUCT CMI_1_RESPOND_PORT_0_CPL_CONFIG_0_CHANNEL_3_STRUCT;

typedef CMI_0_RESPOND_PORT_0_CPL_CONFIG_1_STRUCT CMI_1_RESPOND_PORT_0_CPL_CONFIG_1_STRUCT;

typedef CMI_0_RESPOND_PORT_0_STALL_STRUCT CMI_1_RESPOND_PORT_0_STALL_STRUCT;

typedef CMI_0_RESPOND_PORT_0_CPL_CONFIG_0_CHANNEL_0_STRUCT CMI_1_RESPOND_PORT_1_CPL_CONFIG_0_CHANNEL_0_STRUCT;

typedef CMI_0_RESPOND_PORT_0_CPL_CONFIG_0_CHANNEL_0_STRUCT CMI_1_RESPOND_PORT_1_CPL_CONFIG_0_CHANNEL_1_STRUCT;

typedef CMI_0_RESPOND_PORT_0_CPL_CONFIG_0_CHANNEL_0_STRUCT CMI_1_RESPOND_PORT_1_CPL_CONFIG_0_CHANNEL_2_STRUCT;

typedef CMI_0_RESPOND_PORT_0_CPL_CONFIG_0_CHANNEL_0_STRUCT CMI_1_RESPOND_PORT_1_CPL_CONFIG_0_CHANNEL_3_STRUCT;

typedef CMI_0_RESPOND_PORT_0_CPL_CONFIG_1_STRUCT CMI_1_RESPOND_PORT_1_CPL_CONFIG_1_STRUCT;

typedef CMI_0_RESPOND_PORT_0_STALL_STRUCT CMI_1_RESPOND_PORT_1_STALL_STRUCT;

typedef CMI_0_RESPOND_PORT_0_RD_COUNTER_CHANNEL_0_STRUCT CMI_1_RESPOND_PORT_0_RD_COUNTER_CHANNEL_0_STRUCT;

typedef CMI_0_RESPOND_PORT_0_RD_COUNTER_CHANNEL_0_STRUCT CMI_1_RESPOND_PORT_0_RD_COUNTER_CHANNEL_1_STRUCT;

typedef CMI_0_RESPOND_PORT_0_RD_COUNTER_CHANNEL_0_STRUCT CMI_1_RESPOND_PORT_0_RD_COUNTER_CHANNEL_2_STRUCT;

typedef CMI_0_RESPOND_PORT_0_RD_COUNTER_CHANNEL_0_STRUCT CMI_1_RESPOND_PORT_0_RD_COUNTER_CHANNEL_3_STRUCT;

typedef CMI_0_RESPOND_PORT_0_WR_COUNTER_CHANNEL_0_STRUCT CMI_1_RESPOND_PORT_0_WR_COUNTER_CHANNEL_0_STRUCT;

typedef CMI_0_RESPOND_PORT_0_WR_COUNTER_CHANNEL_0_STRUCT CMI_1_RESPOND_PORT_0_WR_COUNTER_CHANNEL_1_STRUCT;

typedef CMI_0_RESPOND_PORT_0_WR_COUNTER_CHANNEL_0_STRUCT CMI_1_RESPOND_PORT_0_WR_COUNTER_CHANNEL_2_STRUCT;

typedef CMI_0_RESPOND_PORT_0_WR_COUNTER_CHANNEL_0_STRUCT CMI_1_RESPOND_PORT_0_WR_COUNTER_CHANNEL_3_STRUCT;

typedef CMI_0_RESPOND_PORT_0_RD_COUNTER_CHANNEL_0_STRUCT CMI_1_RESPOND_PORT_1_RD_COUNTER_CHANNEL_0_STRUCT;

typedef CMI_0_RESPOND_PORT_0_RD_COUNTER_CHANNEL_0_STRUCT CMI_1_RESPOND_PORT_1_RD_COUNTER_CHANNEL_1_STRUCT;

typedef CMI_0_RESPOND_PORT_0_RD_COUNTER_CHANNEL_0_STRUCT CMI_1_RESPOND_PORT_1_RD_COUNTER_CHANNEL_2_STRUCT;

typedef CMI_0_RESPOND_PORT_0_RD_COUNTER_CHANNEL_0_STRUCT CMI_1_RESPOND_PORT_1_RD_COUNTER_CHANNEL_3_STRUCT;

typedef CMI_0_RESPOND_PORT_0_WR_COUNTER_CHANNEL_0_STRUCT CMI_1_RESPOND_PORT_1_WR_COUNTER_CHANNEL_0_STRUCT;

typedef CMI_0_RESPOND_PORT_0_WR_COUNTER_CHANNEL_0_STRUCT CMI_1_RESPOND_PORT_1_WR_COUNTER_CHANNEL_1_STRUCT;

typedef CMI_0_RESPOND_PORT_0_WR_COUNTER_CHANNEL_0_STRUCT CMI_1_RESPOND_PORT_1_WR_COUNTER_CHANNEL_2_STRUCT;

typedef CMI_0_RESPOND_PORT_0_WR_COUNTER_CHANNEL_0_STRUCT CMI_1_RESPOND_PORT_1_WR_COUNTER_CHANNEL_3_STRUCT;

typedef CMI_0_REQ_L3_ARB_CONFIG_STRUCT CMI_1_REQ_L3_ARB_CONFIG_STRUCT;

typedef CMI_0_PARITY_CONTROL_STRUCT CMI_1_PARITY_CONTROL_STRUCT;

typedef CMI_0_PARITY_ERR_LOG_STRUCT CMI_1_PARITY_ERR_LOG_STRUCT;

typedef CMI_0_PARITY_ERR_INJ_STRUCT CMI_1_PARITY_ERR_INJ_STRUCT;

typedef CMI_0_REQUEST_PORT_0_RESERVED_0_STRUCT CMI_1_REQUEST_PORT_0_RESERVED_0_STRUCT;

typedef CMI_0_REQUEST_PORT_0_RESERVED_0_STRUCT CMI_1_REQUEST_PORT_0_RESERVED_1_STRUCT;

typedef CMI_0_REQUEST_PORT_0_RESERVED_0_STRUCT CMI_1_REQUEST_PORT_0_RESERVED_2_STRUCT;

typedef CMI_0_REQUEST_PORT_0_REQ_IN_DFX_INSTR_DBG_STRUCT CMI_1_REQUEST_PORT_0_REQ_IN_DFX_INSTR_DBG_STRUCT;

typedef CMI_0_REQUEST_PORT_0_REQ_IN_DFX_INSTR_DBG_STRUCT CMI_1_REQUEST_PORT_0_RD_CPL_OUT_DFX_INSTR_DBG_STRUCT;

typedef CMI_0_REQUEST_PORT_0_REQ_IN_DFX_INSTR_DBG_STRUCT CMI_1_REQUEST_PORT_0_RSP_OUT_DFX_INSTR_DBG_STRUCT;

typedef CMI_0_REQUEST_PORT_0_RESERVED_0_STRUCT CMI_1_REQUEST_PORT_1_RESERVED_0_STRUCT;

typedef CMI_0_REQUEST_PORT_0_RESERVED_0_STRUCT CMI_1_REQUEST_PORT_1_RESERVED_1_STRUCT;

typedef CMI_0_REQUEST_PORT_0_RESERVED_0_STRUCT CMI_1_REQUEST_PORT_1_RESERVED_2_STRUCT;

typedef CMI_0_REQUEST_PORT_0_REQ_IN_DFX_INSTR_DBG_STRUCT CMI_1_REQUEST_PORT_1_REQ_IN_DFX_INSTR_DBG_STRUCT;

typedef CMI_0_REQUEST_PORT_0_REQ_IN_DFX_INSTR_DBG_STRUCT CMI_1_REQUEST_PORT_1_RD_CPL_OUT_DFX_INSTR_DBG_STRUCT;

typedef CMI_0_REQUEST_PORT_0_REQ_IN_DFX_INSTR_DBG_STRUCT CMI_1_REQUEST_PORT_1_RSP_OUT_DFX_INSTR_DBG_STRUCT;

typedef CMI_0_REQUEST_PORT_0_RESERVED_0_STRUCT CMI_1_REQUEST_PORT_2_RESERVED_0_STRUCT;

typedef CMI_0_REQUEST_PORT_0_RESERVED_0_STRUCT CMI_1_REQUEST_PORT_2_RESERVED_1_STRUCT;

typedef CMI_0_REQUEST_PORT_0_RESERVED_0_STRUCT CMI_1_REQUEST_PORT_2_RESERVED_2_STRUCT;

typedef CMI_0_REQUEST_PORT_0_REQ_IN_DFX_INSTR_DBG_STRUCT CMI_1_REQUEST_PORT_2_REQ_IN_DFX_INSTR_DBG_STRUCT;

typedef CMI_0_REQUEST_PORT_0_REQ_IN_DFX_INSTR_DBG_STRUCT CMI_1_REQUEST_PORT_2_RD_CPL_OUT_DFX_INSTR_DBG_STRUCT;

typedef CMI_0_REQUEST_PORT_0_REQ_IN_DFX_INSTR_DBG_STRUCT CMI_1_REQUEST_PORT_2_RSP_OUT_DFX_INSTR_DBG_STRUCT;

typedef CMI_0_REQUEST_PORT_0_RESERVED_0_STRUCT CMI_1_RESPOND_PORT_0_RESERVED_0_STRUCT;

typedef CMI_0_REQUEST_PORT_0_RESERVED_0_STRUCT CMI_1_RESPOND_PORT_0_RESERVED_1_STRUCT;

typedef CMI_0_REQUEST_PORT_0_RESERVED_0_STRUCT CMI_1_RESPOND_PORT_0_RESERVED_2_STRUCT;

typedef CMI_0_REQUEST_PORT_0_REQ_IN_DFX_INSTR_DBG_STRUCT CMI_1_RESPOND_PORT_0_REQ_OUT_DFX_INSTR_DBG_STRUCT;

typedef CMI_0_REQUEST_PORT_0_REQ_IN_DFX_INSTR_DBG_STRUCT CMI_1_RESPOND_PORT_0_RD_CPL_IN_DFX_INSTR_DBG_STRUCT;

typedef CMI_0_REQUEST_PORT_0_REQ_IN_DFX_INSTR_DBG_STRUCT CMI_1_RESPOND_PORT_0_RSP_IN_DFX_INSTR_DBG_STRUCT;

typedef CMI_0_REQUEST_PORT_0_RESERVED_0_STRUCT CMI_1_RESPOND_PORT_1_RESERVED_0_STRUCT;

typedef CMI_0_REQUEST_PORT_0_RESERVED_0_STRUCT CMI_1_RESPOND_PORT_1_RESERVED_1_STRUCT;

typedef CMI_0_REQUEST_PORT_0_RESERVED_0_STRUCT CMI_1_RESPOND_PORT_1_RESERVED_2_STRUCT;

typedef CMI_0_REQUEST_PORT_0_REQ_IN_DFX_INSTR_DBG_STRUCT CMI_1_RESPOND_PORT_1_REQ_OUT_DFX_INSTR_DBG_STRUCT;

typedef CMI_0_REQUEST_PORT_0_REQ_IN_DFX_INSTR_DBG_STRUCT CMI_1_RESPOND_PORT_1_RD_CPL_IN_DFX_INSTR_DBG_STRUCT;

typedef CMI_0_REQUEST_PORT_0_REQ_IN_DFX_INSTR_DBG_STRUCT CMI_1_RESPOND_PORT_1_RSP_IN_DFX_INSTR_DBG_STRUCT;

typedef CMI_0_REQUEST_PORT_0_REQ_IN_DFX_INSTR_DBG_STRUCT CMI_1_REQUEST_PORT_0_REQ_DATA_IN_DFX_INSTR_DBG_STRUCT;

typedef CMI_0_REQUEST_PORT_0_REQ_IN_DFX_INSTR_DBG_STRUCT CMI_1_REQUEST_PORT_0_RD_CPL_DATA_OUT_DFX_INSTR_DBG_STRUCT;

typedef CMI_0_REQUEST_PORT_0_REQ_IN_DFX_INSTR_DBG_STRUCT CMI_1_REQUEST_PORT_1_REQ_DATA_IN_DFX_INSTR_DBG_STRUCT;

typedef CMI_0_REQUEST_PORT_0_REQ_IN_DFX_INSTR_DBG_STRUCT CMI_1_REQUEST_PORT_1_RD_CPL_DATA_OUT_DFX_INSTR_DBG_STRUCT;

typedef CMI_0_REQUEST_PORT_0_REQ_IN_DFX_INSTR_DBG_STRUCT CMI_1_REQUEST_PORT_2_REQ_DATA_IN_DFX_INSTR_DBG_STRUCT;

typedef CMI_0_REQUEST_PORT_0_REQ_IN_DFX_INSTR_DBG_STRUCT CMI_1_REQUEST_PORT_2_RD_CPL_DATA_OUT_DFX_INSTR_DBG_STRUCT;

typedef CMI_0_REQUEST_PORT_0_REQ_IN_DFX_INSTR_DBG_STRUCT CMI_1_RESPOND_PORT_0_REQ_DATA_OUT_DFX_INSTR_DBG_STRUCT;

typedef CMI_0_REQUEST_PORT_0_REQ_IN_DFX_INSTR_DBG_STRUCT CMI_1_RESPOND_PORT_0_RD_CPL_DATA_IN_DFX_INSTR_DBG_STRUCT;

typedef CMI_0_REQUEST_PORT_0_REQ_IN_DFX_INSTR_DBG_STRUCT CMI_1_RESPOND_PORT_1_REQ_DATA_OUT_DFX_INSTR_DBG_STRUCT;

typedef CMI_0_REQUEST_PORT_0_REQ_IN_DFX_INSTR_DBG_STRUCT CMI_1_RESPOND_PORT_1_RD_CPL_DATA_IN_DFX_INSTR_DBG_STRUCT;

typedef CMI_0_DFX_OBSERVE_STRUCT CMI_1_DFX_OBSERVE_STRUCT;

typedef CMF_0_GLOBAL_STATUS_STRUCT CMF_1_GLOBAL_STATUS_STRUCT;

typedef CMF_0_GLOBAL_CFG_STRUCT CMF_1_GLOBAL_CFG_STRUCT;

#pragma pack(pop)
#endif
