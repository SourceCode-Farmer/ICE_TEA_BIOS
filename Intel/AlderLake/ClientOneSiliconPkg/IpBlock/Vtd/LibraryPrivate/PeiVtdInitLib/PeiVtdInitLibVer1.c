/** @file
  This code provides a initialization of intel VT-d (Virtualization Technology for Directed I/O).

@copyright
  INTEL CONFIDENTIAL
  Copyright 1999 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains an 'Intel Peripheral Driver' and is uniquely identified as
  "Intel Reference Module" and is licensed for Intel CPUs and chipsets under
  the terms of your license agreement with Intel or your vendor. This file may
  be modified by the user, subject to additional terms of the license agreement.

@par Specification Reference:
**/
#include <PiPei.h>
#include <Uefi/UefiBaseType.h>
#include <Library/DebugLib.h>
#include <Library/BaseLib.h>
#include <Library/BaseMemoryLib.h>
#include <Library/PostCodeLib.h>
#include <Library/HobLib.h>
#include <Library/IoLib.h>
#include <Library/PcdLib.h>
#include <Library/PciSegmentLib.h>
#include <Library/PeiServicesLib.h>
#include <Library/PeiVtdInitLib.h>
#include <Library/CpuPlatformLib.h>
#include <Library/PsfSocLib.h>
#include <Library/BootGuardLibVer1.h>
#include <Library/SaPlatformLib.h>
#include <Library/GraphicsInfoFruLib.h>
#include <Library/VtdInfoLib.h>
#if FixedPcdGetBool(PcdIpuEnable) == 1
#include <Register/IpuRegs.h>
#endif
#include <Register/IgdRegs.h>
#include <Register/VtdRegs.h>
#include <CpuRegs.h>
#include <VtdDataHob.h>
#include <Library/PeiVtdInitFruLib.h>
#include <Guid/VtdPmrInfoHob.h>
#include <Library/MemoryAllocationLib.h>
#include <Library/PeiCpuPsfInitFruLib.h>

typedef union {
  struct {
    UINT32  Low;
    UINT32  High;
  } Data32;
  UINT64 Data;
} UINT64_STRUCT;

extern EFI_GUID gVtdDataHobGuid;

EFI_STATUS
VtdOnMemoryDiscovered (
  IN EFI_PEI_SERVICES           **PeiServices,
  IN EFI_PEI_NOTIFY_DESCRIPTOR  *NotifyDescriptor,
  IN VOID                       *Ppi
  );

EFI_PEI_NOTIFY_DESCRIPTOR mVtdMemoryDiscoveredNotifyDesc = {
  (EFI_PEI_PPI_DESCRIPTOR_NOTIFY_CALLBACK | EFI_PEI_PPI_DESCRIPTOR_TERMINATE_LIST),
  &gEfiPeiMemoryDiscoveredPpiGuid,
  VtdOnMemoryDiscovered
};

#if FixedPcdGetBool(PcdIpuEnable) == 1
/**
  Clear IPU VTD BAR.

  @param[in]   MchBar        MchBar Value
**/
VOID
ClearIpuVtdBar (
  IN  UINTN          MchBar
  )
{
  if (PciSegmentRead16(PCI_SEGMENT_LIB_ADDRESS (SA_SEG_NUM, IPU_BUS_NUM, IPU_DEV_NUM, IPU_FUN_NUM, PCI_VENDOR_ID_OFFSET)) != V_SA_DEVICE_ID_INVALID) {
    PciSegmentWrite32 (PCI_SEGMENT_LIB_ADDRESS (SA_SEG_NUM, IPU_BUS_NUM, IPU_DEV_NUM, IPU_FUN_NUM, R_VTD_IPU_UBAR), 0);
    PciSegmentWrite32 (PCI_SEGMENT_LIB_ADDRESS (SA_SEG_NUM, IPU_BUS_NUM, IPU_DEV_NUM, IPU_FUN_NUM, R_VTD_IPU_LBAR), 0);
    MmioWrite32 (MchBar + R_MCHBAR_VTD2_HIGH_OFFSET, 0);
    MmioWrite32 (MchBar + R_MCHBAR_VTD2_LOW_OFFSET, 0);
  }
}

/**
  Configure IPU VTD BAR.

  @param[in] Vtd            VT-d config block from SA Policy PPI
  @param[in] MchBar         MchBar Value

  @retval    EFI_SUCCESS    Function executed successfully
  @retval    EFI_NOT_FOUND  The required PPI could not be located.
**/
EFI_STATUS
ConfigureIpuVtdBar (
  IN  VTD_CONFIG           *Vtd,
  IN  UINTN                MchBar
  )
{
  EFI_STATUS              Status;
  SI_PREMEM_POLICY_PPI    *SiPreMemPolicyPpi;
  UINT32                  Data32Or;
  IPU_PREMEM_CONFIG       *IpuPreMemPolicy;

  ///
  /// Get policy settings through the SaInitConfigBlock PPI
  ///
  Status = PeiServicesLocatePpi (&gSiPreMemPolicyPpiGuid, 0, NULL, (VOID **) &SiPreMemPolicyPpi);
  ASSERT_EFI_ERROR (Status);
  if ((Status != EFI_SUCCESS) || (SiPreMemPolicyPpi == NULL)) {
    return EFI_NOT_FOUND;
  }

  Status = GetConfigBlock((VOID *) SiPreMemPolicyPpi, &gIpuPreMemConfigGuid, (VOID *) &IpuPreMemPolicy);
  ASSERT_EFI_ERROR (Status);
  ///
  /// Configure VTD2 BAR
  /// Skip IPUVTBAR if IPU is disabled
  ///
  if ((PciSegmentRead16(PCI_SEGMENT_LIB_ADDRESS (SA_SEG_NUM, IPU_BUS_NUM, IPU_DEV_NUM, IPU_FUN_NUM, PCI_VENDOR_ID_OFFSET)) != V_SA_DEVICE_ID_INVALID) && (IpuPreMemPolicy->IpuEnable == 1) && (Vtd->VtdIpuEnable == 1)) {
    Data32Or = Vtd->BaseAddress[1];
    Data32Or |= 0x1;
    MmioWrite32 (MchBar + R_MCHBAR_VTD2_HIGH_OFFSET, 0);
    MmioWrite32 (MchBar + R_MCHBAR_VTD2_LOW_OFFSET, Data32Or);
    if ((MmioRead32 (MchBar + R_MCHBAR_VTD2_LOW_OFFSET) & BIT0) == 0) {
      MmioWrite32 (MchBar + R_MCHBAR_VTD2_LOW_OFFSET, 0);
    } else {
      PciSegmentWrite32 (PCI_SEGMENT_LIB_ADDRESS (SA_SEG_NUM, IPU_BUS_NUM, IPU_DEV_NUM, IPU_FUN_NUM, R_VTD_IPU_UBAR), 0);
      PciSegmentWrite32 (PCI_SEGMENT_LIB_ADDRESS (SA_SEG_NUM, IPU_BUS_NUM, IPU_DEV_NUM, IPU_FUN_NUM, R_VTD_IPU_LBAR), Data32Or);

      Data32Or = PciSegmentRead32 (PCI_SEGMENT_LIB_ADDRESS (SA_SEG_NUM, IPU_BUS_NUM, IPU_DEV_NUM, IPU_FUN_NUM, R_VTD_IPU_LBAR));

      //
      // If bit0 of IPU VT-d BAR is 0, then it means that IPU IP is fused off
      // In that case BIOS must clean VT-d BAR value
      //
      if ((Data32Or & BIT0) == 0) {
        MmioWrite32 (MchBar + R_MCHBAR_VTD2_LOW_OFFSET, 0);
        PciSegmentWrite32 (PCI_SEGMENT_LIB_ADDRESS (SA_SEG_NUM, IPU_BUS_NUM, IPU_DEV_NUM, IPU_FUN_NUM, R_VTD_IPU_LBAR), 0);
      }
    }
  }
  return EFI_SUCCESS;
}
#endif

/**
  Configure IGD VTD BAR.

  @param[in] Vtd          VT-d config block from SA Policy PPI
  @param[in] MchBar       MchBar Value

  @retval    EFI_SUCCESS  Function executed successfully
**/
EFI_STATUS
ConfigureIgdVtdBar (
  IN  VTD_CONFIG  *Vtd,
  IN  UINTN       MchBar
  )
{
  UINT32         Data32Or;

  ///
  /// Configure VTD1 BAR
  /// Skip GFXVTBAR if IGD is disabled
  ///
  if ((PciSegmentRead16 (PCI_SEGMENT_LIB_ADDRESS (SA_SEG_NUM, IGD_BUS_NUM, IGD_DEV_NUM, IGD_FUN_NUM, PCI_VENDOR_ID_OFFSET)) != 0xFFFF) && (!IsDisplayOnlySku())  && (Vtd->VtdIgdEnable == 1)) {
    Data32Or = Vtd->BaseAddress[0];
    Data32Or |= 0x1;
    MmioWrite32 (MchBar + R_MCHBAR_VTD1_OFFSET, Data32Or);
    if ((MmioRead32 (MchBar + R_MCHBAR_VTD1_OFFSET) & BIT0) == 0) {
      MmioWrite32 (MchBar + R_MCHBAR_VTD1_OFFSET, 0);
    } else {
      PciSegmentWrite32 (PCI_SEGMENT_LIB_ADDRESS (SA_SEG_NUM, IGD_BUS_NUM, IGD_DEV_NUM, IGD_FUN_NUM, R_SA_VTD_IGD_UBAR), 0);
      PciSegmentWrite32 (PCI_SEGMENT_LIB_ADDRESS (SA_SEG_NUM, IGD_BUS_NUM, IGD_DEV_NUM, IGD_FUN_NUM, R_SA_VTD_IGD_LBAR), Data32Or);

      Data32Or = PciSegmentRead32 (PCI_SEGMENT_LIB_ADDRESS (SA_SEG_NUM, IGD_BUS_NUM, IGD_DEV_NUM, IGD_FUN_NUM, R_SA_VTD_IGD_LBAR));

      //
      // If bit0 of IGD VT-d BAR is 0, then it means that IGD IP is fused off
      // In that case BIOS must clean VT-d BAR value
      //
      if ((Data32Or & BIT0) == 0) {
        MmioWrite32 (MchBar + R_MCHBAR_VTD1_OFFSET, 0);
        PciSegmentWrite32 (PCI_SEGMENT_LIB_ADDRESS (SA_SEG_NUM, IGD_BUS_NUM, IGD_DEV_NUM, IGD_FUN_NUM, R_SA_VTD_IGD_LBAR), 0);
      } else {
        IgdIommuOverride (Vtd);
      }

    }
  }

  return EFI_SUCCESS;
}

/**
  Configure IOP VTD BAR.

  @param[in] Vtd          VT-d config block from SA Policy PPI
  @param[in] MchBar       MchBar Value

  @retval    EFI_SUCCESS  Function executed successfully
**/
EFI_STATUS
ConfigureIopVtdBar (
  IN  VTD_CONFIG  *Vtd,
  IN  UINTN       MchBar
  )
{
  UINT32         Data32Or;

  ///
  /// Configure VTD3 BAR
  ///
  if (Vtd->VtdIopEnable == 1) {
    if ((MmioRead32 (MchBar + R_MCHBAR_VTD3_OFFSET) & BIT0) == 0) {
      Data32Or = Vtd->BaseAddress[2];
      Data32Or |= 0x1;
      MmioWrite32 (MchBar + R_MCHBAR_VTD3_OFFSET, Data32Or);
    }
  }
  return EFI_SUCCESS;
}

/**
  Clear DMA Protection set by Boot Guard.

  @param[in]   Vtd           VTD config block from SA Policy PPI
**/
VOID
ClearDmaProtectionSetByBootGuard (
  IN  VTD_CONFIG     *Vtd
  )
{
  UINT32         MsrValue;
  UINT32         VtdBaseAddress3;
  UINT32         AcmPolicySts;
  UINT64         McD0BaseAddress;
  UINTN          MchBar;
  UINT32         Data32;

  McD0BaseAddress = PCI_SEGMENT_LIB_ADDRESS (SA_SEG_NUM, SA_MC_BUS, SA_MC_DEV, SA_MC_FUN, 0);
  MchBar          = PciSegmentRead32 (McD0BaseAddress + R_SA_MCHBAR) &~BIT0;

  MsrValue = (UINT32) AsmReadMsr64 (MSR_BOOT_GUARD_SACM_INFO);
  if ((MsrValue & B_BOOT_GUARD_SACM_INFO_NEM_ENABLED) != 0) {
    AcmPolicySts = MmioRead32 (MMIO_ACM_POLICY_STATUS );
    if ((AcmPolicySts & (BIT29)) && (Vtd->PreBootDmaMask == 0)) {
      // DMA protection is enabled
      DEBUG ((DEBUG_INFO, "[Boot Guard] Clear DMA protection set by BTG ACM\n"));
      VtdBaseAddress3 = GetVtdBaseAddress ((UINT8) IOP_VTD);

      Data32 = MmioRead32 (MchBar + R_MCHBAR_VTD3_OFFSET);
      DEBUG ((DEBUG_INFO, " IOP VT-d BAR: 0x%x\n", Data32));

      if (Data32 != (VtdBaseAddress3 | BIT0)) {
        // if IOP VT-d engine is disabled, then BIOS must enable this engine in purpose to disable PMRs
        MmioWrite32 (MchBar + R_MCHBAR_VTD3_OFFSET, VtdBaseAddress3 | BIT0);
        // Read-back IOP VT-d BAR
        Data32 = MmioRead32 (MchBar + R_MCHBAR_VTD3_OFFSET);
        DEBUG ((DEBUG_INFO, " Reprogrammed IOP VT-d BAR: 0x%x\n", Data32));
      }

      if (Data32 & BIT0) {
        Data32 = MmioRead32 (VtdBaseAddress3 + R_VTD_PMEN_OFFSET);
        DEBUG ((DEBUG_INFO, " PMEN before clearing: 0x%x\n", Data32));
        MmioWrite32 ((VtdBaseAddress3 + R_VTD_PMEN_OFFSET), 0);
        // Read-back
        Data32 = MmioRead32 (VtdBaseAddress3 + R_VTD_PMEN_OFFSET);
        DEBUG ((DEBUG_INFO, " PMEN after clearing: 0x%x\n", Data32));
        MmioWrite32 ((VtdBaseAddress3 + R_VTD_PLMBASE_OFFSET), 0);
        MmioWrite32 ((VtdBaseAddress3 + R_VTD_PLMLIMIT_OFFSET), 0);
        MmioWrite32 ((VtdBaseAddress3 + R_VTD_PHMBASE_OFFSET), 0);
        MmioWrite32 ((VtdBaseAddress3 + R_VTD_PHMLIMIT_OFFSET), 0);
      }

      // if VT-d is disabled or IOP VT-d engine is disabled in policy, then BIOS must disable IOP VT-d engine
      if ((Vtd->VtdDisable == 1) || (Vtd->VtdIopEnable == 0)) {
        MmioWrite32 (MchBar + R_MCHBAR_VTD3_OFFSET, 0);
      }

    } // AcmPolicySts & (BIT29)
  }  //NEM enabled by SACM
}

/**
  Installs VTD Data Hob

  @retval EFI_SUCCESS           The HOB was successfully created.
  @retval EFI_OUT_OF_RESOURCES  There is no additional space for HOB creation.
**/
EFI_STATUS
InstallVtdDataHob (
  VOID
  )
{
  EFI_STATUS                  Status;
  VTD_DATA_HOB                *VtdDataHob;

  ///
  /// Create HOB for VTD Data
  ///
  Status = PeiServicesCreateHob (
             EFI_HOB_TYPE_GUID_EXTENSION,
             sizeof (VTD_DATA_HOB),
             (VOID **) &VtdDataHob
             );
  ASSERT_EFI_ERROR (Status);
  ///
  /// Initialize default HOB data
  ///
  VtdDataHob->EfiHobGuidType.Name = gVtdDataHobGuid;
  DEBUG ((DEBUG_INFO, "VtdDataHob->EfiHobGuidType.Name: %g\n", &VtdDataHob->EfiHobGuidType.Name));
  ZeroMem (&(VtdDataHob->VtdDisable), sizeof (VTD_DATA_HOB) - sizeof (EFI_HOB_GUID_TYPE));

  DEBUG ((DEBUG_INFO, "VtdDataHob @ %X\n", VtdDataHob));
  DEBUG ((DEBUG_INFO, "VtdDataHobSize - HobHeader: %X\n", sizeof (VTD_DATA_HOB) - sizeof (EFI_HOB_GUID_TYPE)));
  DEBUG ((DEBUG_INFO, "VtdDataHobSize: %X\n", sizeof (VTD_DATA_HOB)));

  return Status;
}

/**
  Enable DMA buffer in VT-d.

  @param[in] VtdEngineIndex         VT-d engine index.

  @retval EFI_SUCCESS               Enable successfully.
  @retval EFI_INVALID_PARAMETER     Input parameters are invalid.
  @retval EFI_UNSUPPORTED           VT-d isn't supported.
  @retval EFI_OUT_OF_RESOURCES      There is no additional space in the PPI database.
  @retval EFI_NOT_FOUND             Pointer to EnableDmaBuffer function is NULL.
**/
EFI_STATUS
VtdEnableDmaBuffer (
  IN VTD_ENGINE_INDEX VtdEngineIndex
  )
{
  EFI_STATUS            Status;
  SI_PREMEM_POLICY_PPI  *SiPreMemPolicy;
  VTD_CONFIG            *Vtd;
  VTD_ENABLE_DMA_BUFFER EnableDmaBuffer;

  DEBUG ((DEBUG_INFO, "%a(%d) - start\n", __FUNCTION__, VtdEngineIndex));

  Status = PeiServicesLocatePpi (
             &gSiPreMemPolicyPpiGuid,
             0,
             NULL,
             (VOID **)&SiPreMemPolicy
             );
  if (EFI_ERROR (Status)) {
    ASSERT_EFI_ERROR (Status);
    return Status;
  }

  Status = GetConfigBlock ((VOID *) SiPreMemPolicy, &gVtdConfigGuid, (VOID *) &Vtd);
  if (EFI_ERROR (Status)) {
    ASSERT_EFI_ERROR (Status);
    return Status;
  }

  if (Vtd->VtdDisable) {
    DEBUG ((DEBUG_WARN, "VT-d disabled\n"));
    return EFI_UNSUPPORTED;
  }

  if (ReadVtdBaseAddress (VtdEngineIndex) == 0) {
    DEBUG ((DEBUG_WARN, "VT-d engine %d disabled\n", VtdEngineIndex));
    return EFI_UNSUPPORTED;
  }

  if (Vtd->EnableDmaBuffer != (EFI_PHYSICAL_ADDRESS) (UINTN) NULL) {
    EnableDmaBuffer = (VTD_ENABLE_DMA_BUFFER)(UINTN) Vtd->EnableDmaBuffer;
    Status = EnableDmaBuffer (VtdEngineIndex);
  } else {
    Status = EFI_NOT_FOUND;
  }

  if (!EFI_ERROR (Status)) {
    DEBUG ((DEBUG_INFO, "DMA buffer for VT-d engine %d enabled\n", VtdEngineIndex));
  } else {
    DEBUG ((DEBUG_ERROR, "DMA buffer for VT-d engine %d not enabled, status: %r\n", VtdEngineIndex, Status));
  }

  return Status;
}

/**
  A notify callback function to Enable DMA buffer for IOP VT-d engine, installed on memory discovered signal.

  @param[in] PeiServices          General purpose services available to every PEIM.
  @param[in] NotifyDescriptor     The notification structure this PEIM registered on install.
  @param[in] Ppi                  The notify callback PPI.  Not used.

  @retval    EFI_SUCCESS          Always return EFI_SUCCESS.
  */
EFI_STATUS
VtdOnMemoryDiscovered (
  IN EFI_PEI_SERVICES           **PeiServices,
  IN EFI_PEI_NOTIFY_DESCRIPTOR  *NotifyDescriptor,
  IN VOID                       *Ppi
  )
{
  EFI_STATUS           Status;
  SI_PREMEM_POLICY_PPI *SiPreMemPolicyPpi;
  VTD_CONFIG           *Vtd;

  Status = PeiServicesLocatePpi (
             &gSiPreMemPolicyPpiGuid,
             0,
             NULL,
             (VOID **)&SiPreMemPolicyPpi
             );
  ASSERT_EFI_ERROR (Status);

  Status = GetConfigBlock ((VOID *) SiPreMemPolicyPpi, &gVtdConfigGuid, (VOID *) &Vtd);
  ASSERT_EFI_ERROR (Status);

  //
  // Enable DMA buffer for IOP VT-d
  //
  VtdEnableDmaBuffer (IOP_VTD);

  //
  // Disable PMR when only ACM is enabled
  //
  if (IsBootGuardSupported ()) {
    ClearDmaProtectionSetByBootGuard (Vtd);
  }

  return EFI_SUCCESS;
}

/**
  Update Vtd Hob in PostMem

  @param[in]  Vtd    Instance of VTD_CONFIG

  @retval EFI_SUCCESS
**/
EFI_STATUS
UpdateVtdHobPostMem (
  IN       VTD_CONFIG                  *Vtd
  )
{
  VTD_DATA_HOB   *VtdDataHob;
  UINT8          Index;
  UINT32         VtdBase;
  BOOLEAN        VtdIntRemapSupport;

  ///
  /// Locate and update VTD Hob Data
  ///
  VtdDataHob = (VTD_DATA_HOB *) GetFirstGuidHob (&gVtdDataHobGuid);
  if (VtdDataHob != NULL) {
    VtdDataHob->VtdDisable = (BOOLEAN)Vtd->VtdDisable;
    for (Index = 0; Index < VTD_ENGINE_NUMBER; Index++) {
      VtdDataHob->BaseAddress[Index] = Vtd->BaseAddress[Index];
    }
    VtdDataHob->X2ApicOptOut = (BOOLEAN)Vtd->X2ApicOptOut;
    VtdDataHob->DmaControlGuarantee = (BOOLEAN)Vtd->DmaControlGuarantee;

    VtdIntRemapSupport = TRUE;
    for (Index = 0; Index < VTD_ENGINE_NUMBER; Index++) {
      //
      // Set InterruptRemappingSupport default to FALSE
      //
      VtdBase = ReadVtdBaseAddress (Index);

      ///
      /// skip if the VT-d bar is 0
      ///
      if (VtdBase == 0) {
        continue;
      }
      ///
      /// Check IR status and update the InterruptRemappingSupport
      ///
      if ((MmioRead32 (VtdBase + R_VTD_ECAP_OFFSET) & B_VTD_ECAP_REG_IR) == 0) {
        VtdIntRemapSupport = FALSE;
        break;
      }
    }
    VtdDataHob->InterruptRemappingSupport = VtdIntRemapSupport;
  }
  return EFI_SUCCESS;
}

/**
  Check if VT-d is supported or not by silicon.

  @retval TRUE   VT-d is supported by silicon.
  @retval FALSE  VT-d isn't supported by silicon.
**/
BOOLEAN
IsVtdSupported (
  VOID
  )
{
  UINT32  Data32;

  Data32 = PciSegmentRead32 (PCI_SEGMENT_LIB_ADDRESS (SA_SEG_NUM, SA_MC_BUS, SA_MC_DEV, SA_MC_FUN, R_SA_MC_CAPID0_A_OFFSET));

  ///
  /// Check if Silicon is VT-d capable
  ///
  if (Data32 & BIT23) {
    return FALSE;
  }

  return TRUE;
}

/**
  Configure VT-d Base and capabilities.

  @param[in]   VTD_CONFIG       VT-d config block from SA Policy PPI

  @retval      EFI_SUCCESS      VT-d initialization completed
  @exception   EFI_UNSUPPORTED  VT-d is not supported by silicon or not enabled by policy
**/
EFI_STATUS
VtdInit (
  IN VTD_CONFIG  *Vtd
  )
{
  UINT64         McD0BaseAddress;
  UINTN          MchBar;
  EFI_STATUS     Status;

  McD0BaseAddress = PCI_SEGMENT_LIB_ADDRESS (SA_SEG_NUM, SA_MC_BUS, SA_MC_DEV, SA_MC_FUN, 0);
  MchBar          = PciSegmentRead32 (McD0BaseAddress + R_SA_MCHBAR) &~BIT0;

#if FixedPcdGetBool(PcdIpuEnable) == 1
  //
  // BIOS need to clear IPU VTDBAR first
  //
  ClearIpuVtdBar (MchBar);
#endif

  //
  // Install VTD Data Hob
  //
  DEBUG ((DEBUG_INFO, "Install VTD DATA HOB\n"));
  InstallVtdDataHob ();

  //
  // Install callback on Memory Discovered event
  //
  Status = PeiServicesNotifyPpi (&mVtdMemoryDiscoveredNotifyDesc);
  if (EFI_ERROR (Status)) {
    DEBUG ((DEBUG_ERROR, "Cannot register VtdMemoryDiscovered event, status = %r\n", Status));
  }

  ///
  /// Check SA supports VTD and VTD is enabled in setup menu
  ///
  if ((Vtd->VtdDisable) || !IsVtdSupported ()) {
    DEBUG ((DEBUG_WARN, "VTd disabled or no capability!\n"));
    return EFI_UNSUPPORTED;
  }

  DEBUG ((DEBUG_INFO, "VTd enabled\n"));

  ///
  /// Enable VTd in CPU PSFs
  ///
  CpuPsfEnableVtd ();

  ///
  /// Enable PCH PSF for VT-d
  ///
  PsfEnableVtd (PsfGetSegmentTable ());

  ///
  /// Program Remap Engine Base Address
  ///
  ConfigureIgdVtdBar (Vtd, MchBar);
#if FixedPcdGetBool(PcdIpuEnable) == 1
  ConfigureIpuVtdBar (Vtd, MchBar);
#endif
  ConfigureIopVtdBar (Vtd, MchBar);

  return EFI_SUCCESS;
}