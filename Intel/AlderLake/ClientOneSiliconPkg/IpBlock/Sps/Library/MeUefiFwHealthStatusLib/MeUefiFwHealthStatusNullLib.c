/** @file
  Implements ME UEFI FW Health Status Library.

  @copyright
  INTEL CONFIDENTIAL
  Copyright 2020 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary    and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.
**/
#include <Base.h>
#include <PiPei.h>
#include <Uefi.h>
#include <Library/MeUefiFwHealthStatusLib.h>


/**
  Function sets value of determined field in MeUefiFwHealthStatus HOB.

  @param[in] FieldId           MeUefiFwHealthStatus field id
  @param[in] HealthStatus      required status for determined field

  @retval EFI_SUCCESS           The function completed successfully.
  @retval EFI_OUT_OF_RESOURCES  HOB creation failed
  @retval EFI_COMPROMISED_DATA  HOB data is not valid
  @return EFI_UNSUPPORTED       HOB can't be created at DXE phase
**/
EFI_STATUS
SetMeUefiFwHealthStatus (
  ME_UEFI_FW_HEALTH_STATUS_FIELD_ID    FieldId,
  ME_UEFI_FW_HEALTH_STATUS             HealthStatus
  )
{
  return EFI_SUCCESS;
} // SetMeUefiFwHealthStatus

/**
  Function gets value of determined field from MeUefiFwHealthStatus HOB.

  @param[in] FieldId            MeUefiFwHealthStatus field id
  @param[out] HealthStatus      status of determined field

  @retval EFI_SUCCESS           The function completed successfully
  @retval EFI_INVALID_PARAMETER Pointer to HealthStatus is NULL
  @retval EFI_OUT_OF_RESOURCES  MeUefiFwHealthStatus HOB doesn't exist
  @retval EFI_COMPROMISED_DATA  HOB data is not valid
  @retval EFI_UNSUPPORTED       MeUefiFwHealthStatus is not supported
**/
EFI_STATUS
GetMeUefiFwHealthStatus (
  ME_UEFI_FW_HEALTH_STATUS_FIELD_ID    FieldId,
  ME_UEFI_FW_HEALTH_STATUS             *HealthStatus
  )
{
  return EFI_UNSUPPORTED;
} // GetMeUefiFwHealthStatus

/**
  Function sets value (only StatusOk or StatusError) of the determined field in MeUefiFwHealthStatus HOB,
  depending on the received value of Efi Status.

  @param[in] FieldId           MeUefiFwHealthStatus field id
  @param[in] Status            EFI status for determined field

  @retval EFI_SUCCESS           The function completed successfully.
  @retval EFI_OUT_OF_RESOURCES  HOB creation failed
**/
EFI_STATUS
SetMeUefiFwHealthEfiStatus (
  ME_UEFI_FW_HEALTH_STATUS_FIELD_ID    FieldId,
  EFI_STATUS                           EfiStatus
  )
{
  return EFI_SUCCESS;
} // SetMeUefiFwHealthEfiStatus
