/**@file
  This is the code that initializes eSPI.

@copyright
  INTEL CONFIDENTIAL
  Copyright 2015 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains an 'Intel Peripheral Driver' and is uniquely identified as
  "Intel Reference Module" and is licensed for Intel CPUs and chipsets under
  the terms of your license agreement with Intel or your vendor. This file may
  be modified by the user, subject to additional terms of the license agreement.

@par Specification Reference:
**/

#include <ConfigBlock.h>
#include <EspiConfig.h>
#include <EspiHandle.h>
#include <Library/DebugLib.h>
#include <Library/PciSegmentLib.h>
#include <Library/PchPcrLib.h>
#include <Library/EspiLib.h>
#include <IndustryStandard/Pci30.h>
#include <Register/PchRegs.h>
#include <Register/PchPcrRegs.h>
#include <Register/SpiRegs.h>
#include <Register/PchRegsLpc.h>
#include <Library/PeiEspiInitLib.h>

/**
  Configure SPI/eSPI power management

  @param[in] EspiConfig - pointer to eSPI config
**/
STATIC
VOID
EspiConfigurePm (
  IN  ESPI_HANDLE       *EspiHandle
  )
{
  UINT32 AndData;
  UINT32 OrData;
  PCH_ESPI_CONFIG   *EspiConfig;

  EspiConfig = EspiHandle->EspiConfig;

  //
  // Configuring SPI and eSPI dynamic clock gating
  EspiHandle->PciPcrAccess->AndThenOr32 (EspiHandle->PciPcrAccess, R_SPI_PCR_CLK_CTL, ~0u, 0x2000F73F);

  if (EspiConfig->EspiPmHAE) {
    //
    // Clear SPI PCR 0xC008 bit 2,1,0
    // Set SPI PCR 0xC008 bit 5
    //
    AndData = (UINT32)~(BIT2 | BIT1 | BIT0);
    OrData  = BIT5;
  } else {
    //
    // Clear SPI PCR 0xC008 bit 5,2,1,0
    //
    AndData = (UINT32)~(BIT5 | BIT2 | BIT1 | BIT0);
    OrData  = 0;
  }
  EspiHandle->PciPcrAccess->AndThenOr32 (EspiHandle->PciPcrAccess, R_SPI_PCR_PWR_CTL, AndData, OrData);
}

/**
  Enable Parity error handling for eSPI.
**/
VOID
EspiEnableErrorReporting(
  IN  ESPI_HANDLE       *EspiHandle
  )
{
  //
  //  Set PCERR_SLV0 register for enable error reporting
  //
  EspiHandle->PciPcrAccess->AndThenOr32 (
    EspiHandle->PciPcrAccess,
    R_ESPI_PCR_PCERR_SLV0,
    (UINT32) ~(B_ESPI_PCR_PCERR_SLV0_PMMA | B_ESPI_PCR_PCERR_PCNFES),
    ((V_ESPI_PCR_PCERR_PCFEE_SERR << N_ESPI_PCR_PCERR_PCFEE) |
    (V_ESPI_PCR_PCERR_PCNFEE_SERR << N_ESPI_PCR_PCERR_PCNFEE) |
    B_ESPI_PCR_PCERR_PCURRE |
    (V_ESPI_PCR_PCERR_PCRMTARE_SERR << N_ESPI_PCR_PCERR_PCRMTARE))
  );

  //
  //  Set PCERR_SLV1 register for enable error reporting
  //
  EspiHandle->PciPcrAccess->AndThenOr32 (
    EspiHandle->PciPcrAccess,
    R_ESPI_PCR_PCERR_SLV1,
    (UINT32) ~0,
    ((V_ESPI_PCR_PCERR_PCFEE_SERR << N_ESPI_PCR_PCERR_PCFEE) |
    (V_ESPI_PCR_PCERR_PCNFEE_SERR << N_ESPI_PCR_PCERR_PCNFEE) |
    B_ESPI_PCR_PCERR_PCURRE |
    (V_ESPI_PCR_PCERR_PCRMTARE_SERR << N_ESPI_PCR_PCERR_PCRMTARE))
  );

  //
  //  Set FCERR_SLV0 register for enable error reporting
  //
  EspiHandle->PciPcrAccess->AndThenOr32 (
    EspiHandle->PciPcrAccess,
    R_ESPI_PCR_FCERR_SLV0,
    (UINT32) ~0,
    ((V_ESPI_PCR_FCERR_SLV0_FCFEE_SERR << N_ESPI_PCR_FCERR_SLV0_FCFEE) |
    (V_ESPI_PCR_FCERR_SLV0_FCNFEE_SERR << N_ESPI_PCR_FCERR_SLV0_FCNFEE))
  );

  //
  //  Set VWERR_SLV0 register for enable error reporting
  //
  EspiHandle->PciPcrAccess->AndThenOr32 (
    EspiHandle->PciPcrAccess,
    R_ESPI_PCR_VWERR_SLV0,
    (UINT32) ~0,
    ((V_ESPI_PCR_VWERR_VWFEE_SERR << N_ESPI_PCR_VWERR_VWFEE) |
    (V_ESPI_PCR_VWERR_VWNFEE_SERR << N_ESPI_PCR_VWERR_VWNFEE))
  );

  //
  //  Set VWERR_SLV1 register for enable error reporting
  //
  EspiHandle->PciPcrAccess->AndThenOr32 (
    EspiHandle->PciPcrAccess,
    R_ESPI_PCR_VWERR_SLV1,
    (UINT32) ~0,
    ((V_ESPI_PCR_VWERR_VWFEE_SERR << N_ESPI_PCR_VWERR_VWFEE) |
    (V_ESPI_PCR_VWERR_VWNFEE_SERR << N_ESPI_PCR_VWERR_VWNFEE))
  );

  //
  //  Set LNKERR_SLV0 register for enable error reporting
  //
  EspiHandle->PciPcrAccess->AndThenOr32 (
    EspiHandle->PciPcrAccess,
    R_ESPI_PCR_LNKERR_SLV0,
    (UINT32) ~(V_ESPI_PCR_LNKERR_SLV0_LFET1E_SMI << N_ESPI_PCR_LNKERR_SLV0_LFET1E),
    (V_ESPI_PCR_LNKERR_SLV0_LFER1E_SERR << N_ESPI_PCR_LNKERR_SLV0_LFET1E)
  );

  //
  //  Set LNKERR_SLV1 register for enable error reporting
  //
  EspiHandle->PciPcrAccess->AndThenOr32 (
    EspiHandle->PciPcrAccess,
    R_ESPI_PCR_LNKERR_SLV1,
    (UINT32) ~(V_ESPI_PCR_LNKERR_SLV1_LFET1E_SMI << N_ESPI_PCR_LNKERR_SLV1_LFET1E),
    (V_ESPI_PCR_LNKERR_SLV1_LFER1E_SERR << N_ESPI_PCR_LNKERR_SLV1_LFET1E)
  );
}

/**
  Configures PCH eSPI

  @param[in] EspiHandle      Pointer to initialized ESPI handle
**/
VOID
EspiInit (
  IN  ESPI_HANDLE       *EspiHandle
  )
{
  UINT32            Data32;
  PCH_ESPI_CONFIG   *EspiConfig;

  DEBUG ((DEBUG_INFO, "[ConfigureEspi] Entry."));

  EspiConfig        = EspiHandle->EspiConfig;

  if ((PciSegmentRead32 (EspiHandle->PciCfgBase + R_ESPI_CFG_PCBC) & B_ESPI_CFG_PCBC_ESPI_EN) == 0) {
    DEBUG ((DEBUG_INFO, "[ConfigureEspi] eSPI is disabled through strap"));
    return;
  }

  if (EspiConfig->HostC10ReportEnable) {
    //
    // If desired enable HOST_C10 reporting in Virtual Wire Channel Error for device 0 (VWERR_SLV0)
    // No need to enable in both device 0 and device 1 because device 1 uses bits from device 0 register
    //
    EspiHandle->PciPcrAccess->AndThenOr32 (
      EspiHandle->PciPcrAccess,
      R_ESPI_PCR_VWERR_SLV0,
      (UINT32) ~(B_ESPI_PCR_XERR_XNFES | B_ESPI_PCR_XERR_XFES),
      B_ESPI_PCR_VWERR_SLV0_VWHC10OE
      );
  }

  if (EspiConfig->BmeMasterSlaveEnabled) {
    //
    // Set the BME bit on eSPI-MC
    //
    PciSegmentOr16 (EspiHandle->PciCfgBase + PCI_COMMAND_OFFSET, (UINT32) EFI_PCI_COMMAND_BUS_MASTER);

    //
    // Set the BME bit on eSPI device 0 using the Tunneled Access
    //
    PchEspiSlaveGetConfig (0, R_ESPI_SLAVE_CHA_0_CAP_AND_CONF, &Data32);
    Data32 |= B_ESPI_SLAVE_BME;
    PchEspiSlaveSetConfig (0, R_ESPI_SLAVE_CHA_0_CAP_AND_CONF, Data32);

    Data32 = EspiHandle->PciPcrAccess->Read32 (EspiHandle->PciPcrAccess, R_SPI_PCR_ESPI_SOFTSTRAPS);
    if (Data32 & B_SPI_PCR_ESPI_SLAVE) {
      //
      // Set the BME bit on eSPI device 1 using the Tunneled Access
      //
      PchEspiSlaveGetConfig (1, R_ESPI_SLAVE_CHA_0_CAP_AND_CONF, &Data32);
      Data32 |= B_ESPI_SLAVE_BME;
      PchEspiSlaveSetConfig (1, R_ESPI_SLAVE_CHA_0_CAP_AND_CONF, Data32);
    }
  }

  if (EspiHandle->EspiPrivateConfig.PmSupport) {
    EspiConfigurePm (EspiHandle);
  }

  if(EspiHandle->EspiPrivateConfig.EspiErrorReporting) {
    EspiEnableErrorReporting (EspiHandle);
  }

  //
  // Set SBLCL bit after the initial eSPI link configuration to lock
  // access to Configuration Registers from offsets 0x0 - 0x7FF
  //
  if (EspiConfig->LockLinkConfiguration) {
    EspiHandle->PciPcrAccess->AndThenOr32 (
      EspiHandle->PciPcrAccess,
      (UINT16) R_ESPI_PCR_SLV_CFG_REG_CTL,
      ~0u,
      B_ESPI_PCR_SLV_CFG_REG_CTL_SBLCL
      );
  }

  DEBUG ((DEBUG_INFO, "[ConfigureEspi] Exit."));
}
