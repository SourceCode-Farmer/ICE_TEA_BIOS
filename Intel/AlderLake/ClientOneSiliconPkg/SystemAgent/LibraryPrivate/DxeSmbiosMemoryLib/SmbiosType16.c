/** @file
  This library will determine memory configuration information from the chipset
  and memory and create SMBIOS memory structures appropriately.

@copyright
  INTEL CONFIDENTIAL
  Copyright 2013 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains an 'Intel Peripheral Driver' and is uniquely identified as
  "Intel Reference Module" and is licensed for Intel CPUs and chipsets under
  the terms of your license agreement with Intel or your vendor. This file may
  be modified by the user, subject to additional terms of the license agreement.

@par Specification Reference:
**/
#include "SmbiosMemory.h"
#include "MemoryConfig.h"
//[-start-211129-OWENWU0027-modify]//
#ifdef S570_SUPPORT
#include <Library/GpioLib.h>
#endif
//[-end-211129-OWENWU0027-modify]//
//[-start-211201-QINGLIN0121-add]//
#if defined(S370_SUPPORT)
#include <Library/GpioLib.h>

#define GPIO_VER2_LP_GPP_F14             0x090C000E  //Board ID11
#define GPIO_VER2_LP_GPP_F13             0x090C000D  //Board ID10
#define GPIO_VER2_LP_GPP_F12             0x090C000C  //Board ID9
#define GPIO_VER2_LP_GPP_F11             0x090C000B  //Board ID8
//[-start-220118-QINGLIN0149-add]//
#define GPIO_VER2_LP_GPP_A11             0x0902000B  //Board ID7
//[-end-220118-QINGLIN0149-add]//

typedef enum {
  Samsung4G  = 0x00,  //Sam K4A8G165WC-BCWE
  Samsung8G  = 0x01,  //Sam K4AAG165WB-BCWE
  Micron4G   = 0x04,  //Mir MT40A512M16TB-062E:R
  Micron8G   = 0x05,  //Mir MT40A1G16RC-062E:B
  Hynix4G    = 0x08,  //Hy  H5AN8G6NDJR-XNC
  Hynix8G    = 0x09,  //Hy  H5ANAG6NCJR-XNC
//[-start-220118-QINGLIN0149-add]//
  Smart4G    = 0x0C,  //Smart K4A8G165WC-BCWE
  Adata4G    = 0x10,  //Adata ADS4021HY
  Micron8G2nd= 0x15,  //Mir MT40A1G16TB-062E:F
//[-end-220118-QINGLIN0149-add]//
} MEMORY_BRAND_TYPE;
#endif
//[-end-211201-QINGLIN0121-add]//

///
/// Physical Memory Array (Type 16) data
///
GLOBAL_REMOVE_IF_UNREFERENCED SMBIOS_TABLE_TYPE16 SmbiosTableType16Data = {
  { EFI_SMBIOS_TYPE_PHYSICAL_MEMORY_ARRAY, sizeof (SMBIOS_TABLE_TYPE16), 0 },
  MemoryArrayLocationSystemBoard, ///< Location
  MemoryArrayUseSystemMemory,     ///< Use
  TO_BE_FILLED,                   ///< MemoryErrorCorrection
  TO_BE_FILLED,                   ///< MaximumCapacity
  0xFFFE,                         ///< MemoryErrorInformationHandle
  TO_BE_FILLED,                   ///< NumberOfMemoryDevices
  0,                              ///< ExtendedMaximumCapacity
};

//[-start-211201-QINGLIN0121-add]//
//[-start-220118-QINGLIN0149-modify]//
#if defined(S370_SUPPORT)
EFI_STATUS
LcfcGpioGetMemoryConfig(
  OUT UINT8      *MemoryConfig
)
{
  UINT32  BoardID11, BoardID10, BoardID9, BoardID8, BoardID7;
  UINT8   Data8 = 0;
  GpioGetInputValue(GPIO_VER2_LP_GPP_F14,&BoardID11);
  GpioGetInputValue(GPIO_VER2_LP_GPP_F13,&BoardID10);
  GpioGetInputValue(GPIO_VER2_LP_GPP_F12,&BoardID9);
  GpioGetInputValue(GPIO_VER2_LP_GPP_F11,&BoardID8);
  GpioGetInputValue(GPIO_VER2_LP_GPP_A11,&BoardID7);
  
  Data8 |= (UINT8)(BoardID11 << 0);
  Data8 |= (UINT8)(BoardID10 << 1);
  Data8 |= (UINT8)(BoardID9  << 2);
  Data8 |= (UINT8)(BoardID8  << 3);
  Data8 |= (UINT8)(BoardID7  << 4);

//  _outp(0x72,0xDF);
//  _outp(0x73,Data8);
  *MemoryConfig = Data8;
  return EFI_SUCCESS;
};
#endif
//[-end-220118-QINGLIN0149-modify]//
//[-end-211201-QINGLIN0121-add]//

/**
  This function installs SMBIOS Table Type 16 (Physical Memory Array).

  @param[in] SmbiosProtocol    - Instance of Smbios Protocol

  @retval EFI_SUCCESS          - if the data is successfully reported.
  @retval EFI_OUT_OF_RESOURCES - if not able to get resources.
**/
EFI_STATUS
InstallSmbiosType16 (
  IN  EFI_SMBIOS_PROTOCOL *SmbiosProtocol
  )
{
  EFI_STATUS                      Status;
  UINT32                          MaximumCapacity;
  UINT32                          MaxRankCapacity;
  UINT16                          MaxSockets;
  UINT8                           ModuleType;
  UINT8                           DeviceType;
  UINT8                           ChannelIndex;
  UINT8                           DimmIndex;
  UINT8                           Dimm;
  DIMM_INFO                       *DimmInfo;
  UINT8                           SkipSolderedDimmCapacityCount;
  UINT8                           ControllerIndex;
//[-start-211201-QINGLIN0121-add]//
#if defined(S370_SUPPORT)
  UINT8                           MemoryConfig;
#endif
//[-end-211201-QINGLIN0121-add]//
  ///
  /// Configure the data for TYPE 16 SMBIOS Structure
  ///
  ///
  /// Create physical array and associated data for all mainboard memory
  ///

  ///
  /// Update ECC Support
  ///
  if (mMemInfo->EccSupport) {
    SmbiosTableType16Data.MemoryErrorCorrection = MemoryErrorCorrectionSingleBitEcc;
  } else {
    SmbiosTableType16Data.MemoryErrorCorrection = MemoryErrorCorrectionNone;
  }
  ///
  /// Get the Memory DIMM info from policy protocols
  ///
  SkipSolderedDimmCapacityCount = 0;
  MaxSockets = 0;
  MaximumCapacity = 0;
  //@todo - Fix for 2xMC
  for (ControllerIndex = 0; ControllerIndex < MEM_CFG_MAX_CONTROLLERS; ControllerIndex++) {
    for (Dimm = 0; Dimm < (MEM_CFG_MAX_SOCKETS/MEM_CFG_MAX_CONTROLLERS); Dimm++) {
      ChannelIndex = Dimm >> 1;
      DimmIndex = Dimm & 0x1;
      DimmInfo = &mMemInfo->Controller[ControllerIndex].ChannelInfo[ChannelIndex].DimmInfo[DimmIndex];
      if ((DimmInfo->Status == DIMM_PRESENT) && (DimmInfo->DimmCapacity > 0)) {
        ModuleType = DimmInfo->SpdModuleType;
        DeviceType = DimmInfo->SpdDramDeviceType;
        // If Memory Down is detected, report System Memory Size instead of Maximum Capacity
        if ((DeviceType == DDR_DTYPE_LPDDR3) || ((ModuleType & DDR_MTYPE_SPD_MASK) == DDR_MTYPE_MEM_DOWN)) {
          MaximumCapacity += (DimmInfo->DimmCapacity * 1024); // Convert from MB to KB
          SkipSolderedDimmCapacityCount++;
        }
      }
      if ((BIT0 << DimmIndex) & mMemoryDxeConfig->SlotMap[ControllerIndex][ChannelIndex]) {
        MaxSockets++;
      }
    }
  }

  switch (mMemInfo->MemoryType){
    case MemoryTypeDdr4:
      MaxRankCapacity = MAX_RANK_CAPACITY_DDR4;
      break;

    case MemoryTypeDdr5:
      MaxRankCapacity = MAX_RANK_CAPACITY_DDR5;
      break;

    default:
      MaxRankCapacity = MAX_RANK_CAPACITY;
      break;
  }

  MaximumCapacity += (MaxRankCapacity * MEM_CFG_MAX_RANKS_PER_DIMM * (MaxSockets - SkipSolderedDimmCapacityCount));

  if (MaximumCapacity < SMBIOS_TYPE16_USE_EXTENDED_MAX_CAPACITY) {
    SmbiosTableType16Data.MaximumCapacity = MaximumCapacity;
  } else {
    SmbiosTableType16Data.MaximumCapacity = SMBIOS_TYPE16_USE_EXTENDED_MAX_CAPACITY;
    SmbiosTableType16Data.ExtendedMaximumCapacity = ((UINT64) MaximumCapacity) * 1024; // Convert from KB to Byte
  }
//[-start-211009-Ching000009-modify]//
#if defined(S77014_SUPPORT) || defined(S77013_SUPPORT) || defined(S77014IAH_SUPPORT)
  SmbiosTableType16Data.MaximumCapacity = 0x01000000;  //16G
#endif
//[-end-211009-Ching000009-modify]//
//[-start-220118-OWENWU0035-modify]//
//[-start-211129-OWENWU0027-modify]//
#if defined(S570_SUPPORT)
  #define GPIO_VER2_LP_GPP_F14             0x090C000E  //Board ID11
  #define GPIO_VER2_LP_GPP_F13             0x090C000D  //Board ID10
  #define GPIO_VER2_LP_GPP_F12             0x090C000C  //Board ID9
  #define GPIO_VER2_LP_GPP_F11             0x090C000B  //Board ID8
  #define GPIO_VER2_LP_GPP_A11             0x0902000B  //Board ID7  
  
  UINT8   Data8 = 0;
  UINT32  BoardID11, BoardID10, BoardID9, BoardID8, BoardID7;
  static GPIO_INIT_CONFIG  GPIO_Table_early_init[] = {
   //            Pmode,       GPI_IS,      GpioDir,         GPIOTxState,  RxEvCfg,    GPIRoutConfig,    PadRstCfg,      Term,        LockConfig,      Othersetting,      Rsvdbits   
   {GPIO_VER2_LP_GPP_F14,{GpioPadModeGpio, GpioHostOwnGpio, GpioDirIn, GpioOutDefault, GpioIntDis, GpioHostDeepReset,  GpioTermNone, GpioLockDefault}},//BOARD_ID11  
   {GPIO_VER2_LP_GPP_F13,{GpioPadModeGpio, GpioHostOwnGpio, GpioDirIn, GpioOutDefault, GpioIntDis, GpioHostDeepReset,  GpioTermNone, GpioLockDefault}},//BOARD_ID10 
   {GPIO_VER2_LP_GPP_F12,{GpioPadModeGpio, GpioHostOwnGpio, GpioDirIn, GpioOutDefault, GpioIntDis, GpioHostDeepReset,  GpioTermNone, GpioLockDefault}},//BOARD_ID9 
   {GPIO_VER2_LP_GPP_F11,{GpioPadModeGpio, GpioHostOwnGpio, GpioDirIn, GpioOutDefault, GpioIntDis, GpioHostDeepReset,  GpioTermNone, GpioLockDefault}},//BOARD_ID8 
   {GPIO_VER2_LP_GPP_A11,{GpioPadModeGpio, GpioHostOwnGpio, GpioDirIn, GpioOutDefault, GpioIntDis, GpioHostDeepReset,  GpioTermNone, GpioLockDefault}},//BoardID 7
  };

  GpioGetInputValue(GPIO_VER2_LP_GPP_F14,&BoardID11);
  GpioGetInputValue(GPIO_VER2_LP_GPP_F13,&BoardID10);
  GpioGetInputValue(GPIO_VER2_LP_GPP_F12,&BoardID9);
  GpioGetInputValue(GPIO_VER2_LP_GPP_F11,&BoardID8);
  GpioGetInputValue(GPIO_VER2_LP_GPP_A11,&BoardID7);
  
  Data8 |= (UINT8)(BoardID11 << 0);
  Data8 |= (UINT8)(BoardID10 << 1);
  Data8 |= (UINT8)(BoardID9  << 2);
  Data8 |= (UINT8)(BoardID8  << 3);
  Data8 |= (UINT8)(BoardID7  << 4);
  
  switch(Data8){
    case 0x00:    //Sam single 8G channelB  K4AAG165WB-BCWE
    case 0x01:    //Sam Dual channel 4G+4G  K4A8G165WC-BCWE
    case 0x02:    //Mir single 8G channelB  MT40A1G16RC-062E:B
    case 0x04:    //Mir single 8G channelB  MT40A1G16TB-062E:F
    case 0x05:    //Mir Dual channel 4G+4G  MT40A512M16TB-062E:R
    case 0x08:    //Hy  single 8G channelB  H5ANAG6NCJR-XNC
    case 0x09:    //Hy  Dual channel 4G+4G  H5AN8G6NDJR-XNC
    SmbiosTableType16Data.MaximumCapacity  = 0x00800000;  //8G
    break;
    
    case 0x03:    //Sam Dual channel 8G+8G  K4AAG165WB-BCWE
    case 0x06:    //Mir Dual channel 8G+8G  MT40A1G16RC-062E:B
    case 0x07:    //Mir Dual channel 8G+8G  MT40A1G16TB-062E:F
    case 0x0B:    //Hy Dual channel 8G+8G   
    SmbiosTableType16Data.MaximumCapacity = 0x01000000;  //16G
    break;
	
	case 0x0C:    //Sam Dual channel 8G+4G  K4AAG165WB-BCWE  K4A8G165WC-BCWE
    case 0x0D:    //Mir Dual channel 8G+4G  MT40A1G16RC-062E MT40A1G16TB-062E
    case 0x0E:    //Hy Dual channel 8G+8G   H5ANAG6NCJR-XNC  H5AN8G6NDJR-XNC 
    SmbiosTableType16Data.MaximumCapacity = 0x00C00000;  //12G
    break;

    default:
    break;
  }
//[-end-220118-OWENWU0035-modify]//  
//[-start-211201-QINGLIN0121-add]//
#elif defined(S370_SUPPORT)
  Status = LcfcGpioGetMemoryConfig (&MemoryConfig);
  switch (MemoryConfig) {
    case Samsung4G:
    case Micron4G:
    case Hynix4G:
//[-start-220118-QINGLIN0149-add]//
    case Smart4G:
    case Adata4G:
//[-end-220118-QINGLIN0149-add]//
      SmbiosTableType16Data.MaximumCapacity = 0x00C00000; //12G
      break;
    case Micron8G:
    case Samsung8G:
    case Hynix8G:
//[-start-220118-QINGLIN0149-add]//
    case Micron8G2nd:
//[-end-220118-QINGLIN0149-add]//
      SmbiosTableType16Data.MaximumCapacity = 0x01000000; //16G
      break;
    default:
      break;
  }
//[-end-211201-QINGLIN0121-add]//
#endif
//[-end-211129-OWENWU0027-modify]//

  SmbiosTableType16Data.NumberOfMemoryDevices = MaxSockets;

  ///
  /// Install SMBIOS Table Type 16
  ///
  Status = AddSmbiosEntry ((EFI_SMBIOS_TABLE_HEADER *) &SmbiosTableType16Data, NULL, 0, SmbiosProtocol, &mSmbiosType16Handle);
  return Status;
}
