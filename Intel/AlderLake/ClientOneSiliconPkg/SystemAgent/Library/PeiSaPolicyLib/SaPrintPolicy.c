/** @file
  This file provides service for PEI phase policy printing

@copyright
  INTEL CONFIDENTIAL
  Copyright 2014 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains an 'Intel Peripheral Driver' and is uniquely identified as
  "Intel Reference Module" and is licensed for Intel CPUs and chipsets under
  the terms of your license agreement with Intel or your vendor. This file may
  be modified by the user, subject to additional terms of the license agreement.

@par Specification Reference:
**/
#include <Library/DebugLib.h>
#include <Library/GpioNativeLib.h>
#include <Ppi/SiPolicy.h>

/**
  Print a visible header for a policy dump

  @param[in] Policy - Policy block name
**/
VOID
EFIAPI
SaPrintPolicyHeader (
  IN CHAR8 *Policy
  )
{
  DEBUG ((DEBUG_INFO, "%a%a%a\n", " ------------------------ ", Policy, " ------------------------ "));
}

/**
  This function prints the PEI phase PreMem policy.

  @param[in] SiPolicyPreMemPpi - Instance of SI_PREMEM_POLICY_PPI
**/
VOID
EFIAPI
SaPrintPolicyPpiPreMem (
  IN  SI_PREMEM_POLICY_PPI *SiPolicyPreMemPpi
  )
{
  DEBUG_CODE_BEGIN ();
  INTN                                  Index;
  EFI_STATUS                            Status;
  SA_MISC_PEI_PREMEM_CONFIG             *MiscPeiPreMemConfig;

  //
  // Get requisite IP Config Blocks which needs to be used here
  //
  Status = GetConfigBlock ((VOID *)SiPolicyPreMemPpi, &gSaMiscPeiPreMemConfigGuid, (VOID *) &MiscPeiPreMemConfig);
  ASSERT_EFI_ERROR (Status);


  SaPrintPolicyHeader ("SA Policy (PEI PreMem) Print BEGIN");
  DEBUG ((DEBUG_INFO, "Revision : 0x%x\n", SiPolicyPreMemPpi->TableHeader.Header.Revision));

  SaPrintPolicyHeader ("SA_MISC_PEI_PREMEM_CONFIG");
  DEBUG ((DEBUG_INFO, " Revision : %d\n", MiscPeiPreMemConfig->Header.Revision));
  DEBUG ((DEBUG_INFO, " SpdAddressTable[%d] :", MEM_CFG_MAX_SOCKETS));
  for (Index = 0; Index < MEM_CFG_MAX_SOCKETS; Index++) {
    DEBUG ((DEBUG_INFO, " 0x%x", MiscPeiPreMemConfig->SpdAddressTable[Index]));
  }
  DEBUG ((DEBUG_INFO, "\n SmbusBar: 0x%x\n TsegSize: 0x%x\n", MiscPeiPreMemConfig->SmbusBar, MiscPeiPreMemConfig->TsegSize));
  DEBUG ((DEBUG_INFO, " UserBd: 0x%x\n", MiscPeiPreMemConfig->UserBd));
  DEBUG ((DEBUG_INFO, " SkipExtGfxScan: %d\n S3DataPtr: 0x%x\n", MiscPeiPreMemConfig->SkipExtGfxScan, MiscPeiPreMemConfig->S3DataPtr));

  DEBUG ((DEBUG_INFO, " WrcFeatureEnable : 0x%x\n", MiscPeiPreMemConfig->WrcFeatureEnable));

  DEBUG ((DEBUG_INFO, " TxtImplemented: %d\n BdatEnable: %d\n", MiscPeiPreMemConfig->TxtImplemented, MiscPeiPreMemConfig->BdatEnable));
  DEBUG ((DEBUG_INFO, " BdatTestType: %d\n ScanExtGfxForLegacyOpRom: %d\n", MiscPeiPreMemConfig->BdatTestType, MiscPeiPreMemConfig->ScanExtGfxForLegacyOpRom));
  DEBUG ((DEBUG_INFO, " CridEnable: %d\n", MiscPeiPreMemConfig->CridEnable));
  DEBUG ((DEBUG_INFO, " AcpiReservedMemoryBase: 0x%x\n", MiscPeiPreMemConfig->AcpiReservedMemoryBase));
  DEBUG ((DEBUG_INFO, " AcpiReservedMemorySize: 0x%x\n", MiscPeiPreMemConfig->AcpiReservedMemorySize));
  DEBUG ((DEBUG_INFO, " SystemMemoryLength: 0x%x\n", MiscPeiPreMemConfig->SystemMemoryLength));
  DEBUG ((DEBUG_INFO, " OpRomScanTempMmioBar: 0x%x\n", MiscPeiPreMemConfig->OpRomScanTempMmioBar));
  DEBUG ((DEBUG_INFO, " OpRomScanTempMmioLimit: 0x%x\n", MiscPeiPreMemConfig->OpRomScanTempMmioLimit));
  DEBUG ((DEBUG_INFO, " FirstDimmBitMask: 0x%x\n", MiscPeiPreMemConfig->FirstDimmBitMask));
  DEBUG ((DEBUG_INFO, " FirstDimmBitMaskEcc: 0x%x\n", MiscPeiPreMemConfig->FirstDimmBitMaskEcc));


  DEBUG_CODE_END ();
  return;
}

/**
  This function prints the PEI phase policy.

  @param[in] SiPolicyPpi - Instance of SI_POLICY_PPI
**/
VOID
EFIAPI
SaPrintPolicyPpi (
  IN  SI_POLICY_PPI     *SiPolicyPpi
  )
{
  DEBUG_CODE_BEGIN ();
  EFI_STATUS                            Status;
  SA_MISC_PEI_CONFIG                    *MiscPeiConfig;
  //
  // Get requisite IP Config Blocks which needs to be used here
  //

  Status = GetConfigBlock ((VOID *) SiPolicyPpi, &gSaMiscPeiConfigGuid, (VOID *) &MiscPeiConfig);
  ASSERT_EFI_ERROR (Status);

  SaPrintPolicyHeader ("SA Policy (PEI) Print BEGIN");
  DEBUG ((DEBUG_INFO, "Revision : 0x%x\n", SiPolicyPpi->TableHeader.Header.Revision));

  SaPrintPolicyHeader ("SA_MISC_PEI_CONFIG");
  DEBUG ((DEBUG_INFO, " Revision : %d\n", MiscPeiConfig->Header.Revision));
  DEBUG_CODE_END ();
  return;
}
