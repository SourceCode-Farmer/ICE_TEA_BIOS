## @file
#  Component description file for the AlderLake PCH PEI FRU libraries.
#
# @copyright
#  INTEL CONFIDENTIAL
#  Copyright 2018 - 2021 Intel Corporation.
#
#  The source code contained or described herein and all documents related to the
#  source code ("Material") are owned by Intel Corporation or its suppliers or
#  licensors. Title to the Material remains with Intel Corporation or its suppliers
#  and licensors. The Material may contain trade secrets and proprietary and
#  confidential information of Intel Corporation and its suppliers and licensors,
#  and is protected by worldwide copyright and trade secret laws and treaty
#  provisions. No part of the Material may be used, copied, reproduced, modified,
#  published, uploaded, posted, transmitted, distributed, or disclosed in any way
#  without Intel's prior express written permission.
#
#  No license under any patent, copyright, trade secret or other intellectual
#  property right is granted to or conferred upon you by disclosure or delivery
#  of the Materials, either expressly, by implication, inducement, estoppel or
#  otherwise. Any license under such intellectual property rights must be
#  express and approved by Intel in writing.
#
#  Unless otherwise agreed by Intel in writing, you may not remove or alter
#  this notice or any other notice embedded in Materials by Intel or
#  Intel's suppliers or licensors in any way.
#
#  This file contains an 'Intel Peripheral Driver' and is uniquely identified as
#  "Intel Reference Module" and is licensed for Intel CPUs and chipsets under
#  the terms of your license agreement with Intel or your vendor. This file may
#  be modified by the user, subject to additional terms of the license agreement.
#
#@par Specification Reference:
#
##

  PeiThermalLib|$(PLATFORM_SI_PACKAGE)/IpBlock/Thermal/LibraryPrivate/PeiThermalLib/PeiThermalLib.inf
  PeiThermalPolicyLib|$(PLATFORM_SI_PACKAGE)/IpBlock/Thermal/LibraryPrivate/PeiThermalPolicyLib/PeiThermalPolicyLib.inf
  PeiP2sbPolicyLib|$(PLATFORM_SI_PACKAGE)/IpBlock/P2sb/LibraryPrivate/PeiP2sbPolicyLib/PeiP2sbPolicyLib.inf
  PeiP2sbPrivateLib|$(PLATFORM_SI_PACKAGE)/IpBlock/P2sb/LibraryPrivate/PeiP2sbPrivateLib/PeiP2sbPrivateLib.inf
  PeiIshLib|$(PLATFORM_SI_PACKAGE)/IpBlock//Ish/LibraryPrivate/PeiIshLib/PeiIshLibVer2.inf
  PeiEspiInitLib|$(PLATFORM_SI_PACKAGE)/IpBlock/Espi/LibraryPrivate/PeiEspiInitLib/PeiEspiInitLib.inf
  PeiEspiPolicyLib|$(PLATFORM_SI_PACKAGE)/IpBlock/Espi/LibraryPrivate/PeiEspiPolicyLib/PeiEspiPolicyLib.inf
  PeiIshPolicyLib|$(PLATFORM_SI_PACKAGE)/IpBlock/Ish/LibraryPrivate/PeiIshPolicyLib/PeiIshPolicyLib.inf
  PeiRtcLib|$(PLATFORM_SI_PACKAGE)/IpBlock/Rtc/LibraryPrivate/PeiRtcLib/PeiRtcLib.inf
  PeiRtcPolicyLib|$(PLATFORM_SI_PACKAGE)/IpBlock/Rtc/LibraryPrivate/PeiRtcPolicyLib/PeiRtcPolicyLib.inf
  PeiPmcPolicyCommonLib|$(PLATFORM_SI_PACKAGE)/IpBlock/Pmc/LibraryPrivate/PeiPmcPolicyLib/PeiPmcPolicyCommonLib.inf
  PeiPmcPolicyLib|$(PLATFORM_SI_PACKAGE)/Fru/AdlPch/LibraryPrivate/PeiPmcPolicyLib/PeiPmcPolicyLibAdl.inf
  PeiSmbusLib|$(PLATFORM_SI_PACKAGE)/IpBlock/Smbus/LibraryPrivate/PeiSmbusLib/PeiSmbusLib.inf
  PeiSmbusPolicyLib|$(PLATFORM_SI_PACKAGE)/IpBlock/Smbus/LibraryPrivate/PeiSmbusPolicyLib/PeiSmbusPolicyLib.inf
  SpiLib|$(PLATFORM_SI_PACKAGE)/IpBlock/Spi/Library/PeiSpiLib/PeiSpiLib.inf
  PeiScsLib|$(PLATFORM_SI_PACKAGE)/IpBlock/Scs/LibraryPrivate/PeiScsLib/PeiScsLib.inf
  PeiScsPolicyLib|$(PLATFORM_SI_PACKAGE)/IpBlock/Scs/LibraryPrivate/PeiScsPolicyLib/PeiScsPolicyLib.inf
  PeiHdaLib|$(PLATFORM_SI_PACKAGE)/IpBlock/Hda/LibraryPrivate/PeiHdaLib/PeiHdaLib.inf
  PeiHdaPolicyLib|$(PLATFORM_SI_PACKAGE)/IpBlock/Hda/LibraryPrivate/PeiHdaPolicyLib/PeiHdaPolicyLib.inf
  PeiPchDmiLib|$(PLATFORM_SI_PACKAGE)/IpBlock/PchDmi/LibraryPrivate/PeiDxeSmmPchDmiLib/PeiPchDmiLib.inf
  PeiPchDmiPolicyLib|$(PLATFORM_SI_PACKAGE)/IpBlock/PchDmi/LibraryPrivate/PeiPchDmiPolicyLib/PeiPchDmiPolicyLib.inf
  PeiSataLib|$(PLATFORM_SI_PACKAGE)/IpBlock/Sata/LibraryPrivate/PeiSataLib/PeiSataLib.inf
  PeiSataPolicyLib|$(PLATFORM_SI_PACKAGE)/IpBlock/Sata/LibraryPrivate/PeiSataPolicyLib/PeiSataPolicyLib.inf
  PeiIehInitLib|$(PLATFORM_SI_PACKAGE)/IpBlock/Ieh/LibraryPrivate/PeiIehInitLib/PeiIehInitLib.inf
  PeiIehPolicyLib|$(PLATFORM_SI_PACKAGE)/IpBlock/Ieh/LibraryPrivate/PeiIehPolicyLib/PeiIehPolicyLib.inf
  PeiPsfLib|$(PLATFORM_SI_PACKAGE)/IpBlock/Psf/LibraryPrivate/PeiPsfLib/PeiPsfLib.inf
  PeiPsfPolicyLib|$(PLATFORM_SI_PACKAGE)/IpBlock/Psf/LibraryPrivate/PeiPsfPolicyLib/PeiPsfPolicyLib.inf

  MeUmaLib|$(PLATFORM_SI_PACKAGE)/IpBlock/Me/LibraryPrivate/PeiMeUmaLib/PeiMeUmaLib.inf
  MeUefiFwHealthStatusLib|$(PLATFORM_SI_PACKAGE)/IpBlock/Sps/Library/MeUefiFwHealthStatusLib/MeUefiFwHealthStatusNullLib.inf
  MeInitLib|$(PLATFORM_SI_PACKAGE)/IpBlock/Me/LibraryPrivate/PeiMeInitLib/PeiMeInitLib.inf
  PeiMeEnhancedDebugLib|$(PLATFORM_SI_PACKAGE)/IpBlock//Me/LibraryPrivate/PeiMeEnhancedDebugLib/PeiMeEnhancedDebugLib.inf
  PeiMeLib|$(PLATFORM_SI_PACKAGE)/IpBlock/Me/Library/PeiMeLib/PeiMeLib.inf
  PeiMePolicyLib|$(PLATFORM_SI_PACKAGE)/IpBlock/Me/Library/PeiMePolicyLib/PeiMePolicyLib.inf
  GpioHelpersLib|$(PLATFORM_SI_PACKAGE)/IpBlock/Gpio/LibraryPrivate/PeiGpioHelpersLib/PeiGpioHelpersLib.inf
  GpioNameBufferLib|$(PLATFORM_SI_PACKAGE)/IpBlock/Gpio/LibraryPrivate/PeiGpioNameBufferLib/PeiGpioNameBufferLib.inf
  PeiDciSocLib|$(PLATFORM_SI_PACKAGE)/Fru/AdlPch/LibraryPrivate/PeiDciSocLib/PeiDciSocLib.inf
  PeiDciInitLib|$(PLATFORM_SI_PACKAGE)/IpBlock/Dci/LibraryPrivate/PeiDciInitLib/PeiDciInitLib.inf
  PeiDciPolicyLib|$(PLATFORM_SI_PACKAGE)/IpBlock/Dci/LibraryPrivate/PeiDciPolicyLib/PeiDciPolicyLib.inf
  PeiPchTraceHubLib|$(PLATFORM_SI_PACKAGE)/IpBlock/TraceHub/LibraryPrivate/PeiPchTraceHubLib/PeiPchTraceHubLib.inf
  UsbHostControllerInitLib|$(PLATFORM_SI_PACKAGE)/IpBlock/Usb/LibraryPrivate/PeiUsbHostControllerInitLib/PeiUsbHostControllerInitLib.inf
  UsbDeviceControllerInitLib|$(PLATFORM_SI_PACKAGE)/IpBlock/Usb/LibraryPrivate/PeiUsbDeviceControllerInitLib/PeiUsbDeviceControllerInitLib.inf
  PeiCnviPreMemLib|$(PLATFORM_SI_PACKAGE)/IpBlock/Cnvi/LibraryPrivate/PeiCnviLib/PeiCnviPreMemLib.inf
  PeiCnviLib|$(PLATFORM_SI_PACKAGE)/IpBlock/Cnvi/LibraryPrivate/PeiCnviLib/PeiCnviLib.inf
  PeiCnviPolicyLib|$(PLATFORM_SI_PACKAGE)/IpBlock/Cnvi/LibraryPrivate/PeiCnviPolicyLib/PeiCnviPolicyLib.inf
  PeiGbeInitLib|$(PLATFORM_SI_PACKAGE)/IpBlock/Gbe/LibraryPrivate/PeiGbeInitLib/PeiGbeInitLib.inf
  PeiGbePolicyLib|$(PLATFORM_SI_PACKAGE)/IpBlock/Gbe/LibraryPrivate/PeiGbePolicyLib/PeiGbePolicyLib.inf
  PeiPchTraceHubPolicyLib|$(PLATFORM_SI_PACKAGE)/IpBlock/TraceHub/LibraryPrivate/PeiPchTraceHubPolicyLib/PeiPchTraceHubPolicyLib.inf
  PeiTraceHubLib|$(PLATFORM_SI_PACKAGE)/IpBlock/TraceHub/LibraryPrivate/PeiTraceHubLib/PeiTraceHubLib.inf
!if gSiPkgTokenSpaceGuid.PcdEmbeddedEnable == TRUE
  PeiTsnInitLib|$(PLATFORM_SI_PACKAGE)/IpBlock/Tsn/LibraryPrivate/PeiTsnInitLib/PeiTsnInitLib.inf
  PeiTsnPolicyLib|$(PLATFORM_SI_PACKAGE)/IpBlock/Tsn/LibraryPrivate/PeiTsnPolicyLib/PeiTsnPolicyLib.inf
!else
  PeiTsnInitLib|$(PLATFORM_SI_PACKAGE)/IpBlock/Tsn/LibraryPrivate/PeiTsnInitLibNull/PeiTsnInitLibNull.inf
  PeiTsnPolicyLib|$(PLATFORM_SI_PACKAGE)/IpBlock/Tsn/LibraryPrivate/PeiTsnPolicyLibNull/PeiTsnPolicyLibNull.inf
!endif
!if gSiPkgTokenSpaceGuid.PcdAmtEnable == TRUE
  PeiAmtPolicyLib|$(PLATFORM_SI_PACKAGE)/IpBlock/Amt/Library/PeiAmtPolicyLib/PeiAmtPolicyLib.inf
!else
  PeiAmtPolicyLib|$(PLATFORM_SI_PACKAGE)/IpBlock/Amt/Library/PeiAmtPolicyLibNull/PeiAmtPolicyLibNull.inf
!endif
  ActiveManagementLib|$(PLATFORM_SI_PACKAGE)/IpBlock/Amt/LibraryPrivate/PeiActiveManagementLibNull/PeiActiveManagementLibNull.inf
  PeiRstPolicyLib|$(PLATFORM_SI_PACKAGE)/IpBlock/Rst/LibraryPrivate/PeiRstPolicyLib/PeiRstPolicyLibNull.inf
  PeiUsbPolicyLib|$(PLATFORM_SI_PACKAGE)/IpBlock/Usb/LibraryPrivate/PeiUsbPolicyLib/PeiUsbPolicyLib.inf
  PeiWdtPolicyLib|$(PLATFORM_SI_PACKAGE)/IpBlock/Wdt/LibraryPrivate/PeiWdtPolicyLib/PeiWdtPolicyLib.inf
  PeiThcInitLib|$(PLATFORM_SI_PACKAGE)/IpBlock/Thc/LibraryPrivate/PeiThcInitLib/PeiThcInitLib.inf
  PeiThcPolicyLib|$(PLATFORM_SI_PACKAGE)/IpBlock/Thc/LibraryPrivate/PeiThcPolicyLib/PeiThcPolicyLib.inf
  PeiPciePreMemRpInitLib|$(PLATFORM_SI_PACKAGE)/IpBlock/PcieRp/LibraryPrivate/PeiPcieRpInitLib/PeiPciePreMemRpInitLib.inf
  PciSegmentRegAccessLib|$(PLATFORM_SI_PACKAGE)/IpBlock/PcieRp/LibraryPrivate/PeiPciSegmentRegAccessLib/PeiPciSegmentRegAccessLib.inf
  PeiPchPcieClocksLib|$(PLATFORM_SI_PACKAGE)/IpBlock/PcieRp/LibraryPrivate/PeiPchPcieClocksLib/PeiPchPcieClocksLibVer2.inf
  PeiFusaLib|$(PLATFORM_SI_PACKAGE)/IpBlock/Fusa/LibraryPrivate/PeiFusaLib/PeiFusaLib.inf
  PeiFusaPolicyLib|$(PLATFORM_SI_PACKAGE)/IpBlock/Fusa/LibraryPrivate/PeiFusaPolicyLib/PeiFusaPolicyLib.inf
  PeiSerialIoInitLib|$(PLATFORM_SI_PACKAGE)/IpBlock/SerialIo/LibraryPrivate/PeiSerialIoInitLib/PeiSerialIoInitLib.inf
  PeiSerialIoPolicyLib|$(PLATFORM_SI_PACKAGE)/IpBlock/SerialIo/LibraryPrivate/PeiSerialIoPolicyLib/PeiSerialIoPolicyLib.inf
  Usb2PhyLib|$(PLATFORM_SI_PACKAGE)/IpBlock/Usb/LibraryPrivate/PeiUsb2PhyLib/PeiUsb2PhyLib.inf
  PeiPcieSipInitLib|$(PLATFORM_SI_PACKAGE)/IpBlock/PcieRp/LibraryPrivate/PeiPcieSipInitLib/PeiPcieSipInitLib.inf
  PchAdrLib|$(PLATFORM_SI_PACKAGE)/IpBlock/Pmc/LibraryPrivate/PeiPchAdrLib/PeiPchAdrLib.inf
  PeiLpcLib|$(PLATFORM_SI_PACKAGE)/IpBlock/Espi/LibraryPrivate/PeiLpcLib/PeiLpcLib.inf
!if gSiPkgTokenSpaceGuid.PcdAdlLpSupport == TRUE
  PeiCavsInitLib|$(PLATFORM_SI_PACKAGE)/IpBlock/Hda/LibraryPrivate/PeiCavsInitLib/PeiCavsInitLibVer2.inf
!else
  PeiCavsInitLib|$(PLATFORM_SI_PACKAGE)/IpBlock/Hda/LibraryPrivate/PeiCavsInitLib/PeiCavsInitLib.inf
!endif
  HdaSocLib|$(PLATFORM_SI_PACKAGE)/Fru/AdlPch/LibraryPrivate/HdaSocLib/HdaSocLib.inf

!if gSiPkgTokenSpaceGuid.PcdAdlLpSupport == TRUE
  PeiPmcPrivateLib|$(PLATFORM_SI_PACKAGE)/IpBlock/Pmc/LibraryPrivate/PeiPmcPrivateLib/PeiPmcPrivateLibVer2.inf
  PeiItssLib|$(PLATFORM_SI_PACKAGE)/IpBlock/Itss/LibraryPrivate/PeiItssLib/PeiItssLibVer2.inf
  PeiItssPolicyLib|$(PLATFORM_SI_PACKAGE)/IpBlock/Itss/LibraryPrivate/PeiItssPolicyLib/PeiItssPolicyLibVer2.inf
  PeiPcieRpInitLib|$(PLATFORM_SI_PACKAGE)/IpBlock/PcieRp/LibraryPrivate/PeiPcieRpInitLib/PeiPcieRpInitLibVer2.inf
  PeiIshInitLib|$(PLATFORM_SI_PACKAGE)/IpBlock/Ish/LibraryPrivate/PeiIshInitLib/PeiIshInitLibVer2.inf
  PchInitPreMemFruLib|$(PLATFORM_SI_PACKAGE)/Fru/AdlPch/LibraryPrivate/PeiPchInitPreMemFruLib/PeiPchInitPreMemFruLibAdlP.inf
  PeiHybridStoragePolicyLib|$(PLATFORM_SI_PACKAGE)/IpBlock/Rst/LibraryPrivate/PeiHybridStoragePolicyLib/PeiHybridStoragePolicyLib.inf
  FiaSocLib|$(PLATFORM_SI_PACKAGE)/Fru/AdlPch/LibraryPrivate/FiaSocLib/FiaSocLibAdlP.inf

  #
  # Fru Libraries
  #
!else
  PeiPmcPrivateLib|$(PLATFORM_SI_PACKAGE)/IpBlock/Pmc/LibraryPrivate/PeiPmcPrivateLib/PeiPmcPrivateLibVer4.inf
  PeiItssLib|$(PLATFORM_SI_PACKAGE)/IpBlock/Itss/LibraryPrivate/PeiItssLib/PeiItssLibVer4.inf
  PeiItssPolicyLib|$(PLATFORM_SI_PACKAGE)/IpBlock/Itss/LibraryPrivate/PeiItssPolicyLib/PeiItssPolicyLibVer4.inf
  PeiPcieRpInitLib|$(PLATFORM_SI_PACKAGE)/IpBlock/PcieRp/LibraryPrivate/PeiPcieRpInitLib/PeiPcieRpInitLibVer4.inf
  PeiIshInitLib|$(PLATFORM_SI_PACKAGE)/IpBlock/Ish/LibraryPrivate/PeiIshInitLib/PeiIshInitLibVer4.inf
  PchInitPreMemFruLib|$(PLATFORM_SI_PACKAGE)/Fru/AdlPch/LibraryPrivate/PeiPchInitPreMemFruLib/PeiPchInitPreMemFruLibAdlS.inf
  Usb2PhyLib|$(PLATFORM_SI_PACKAGE)/IpBlock/Usb/LibraryPrivate/PeiExternalUsb2PhyLib/PeiExternalUsb2PhyLib.inf
  FiaSocLib|$(PLATFORM_SI_PACKAGE)/Fru/AdlPch/LibraryPrivate/FiaSocLib/FiaSocLibAdlS.inf

  #
  # Fru Libraries
  #
!endif

  #
  # Common FRU Libraries
  #
  GbeSocLib|$(PLATFORM_SI_PACKAGE)/Fru/AdlPch/LibraryPrivate/GbeSocLib/GbeSocLib.inf
  PeiUsbFruLib|$(PLATFORM_SI_PACKAGE)/Fru/AdlPch/LibraryPrivate/PeiUsbFruLib/PeiUsbFruLib.inf


