/** @file
  HDA SoC library. Library allow to configure HDA controller for working on Adl S platform.
  All function in this library is available for PEI.

@copyright
  INTEL CONFIDENTIAL
  Copyright 2020 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains an 'Intel Peripheral Driver' and is uniquely identified as
  "Intel Reference Module" and is licensed for Intel CPUs and chipsets under
  the terms of your license agreement with Intel or your vendor. This file may
  be modified by the user, subject to additional terms of the license agreement.

@par Specification Reference:
**/

#include <CpuRegs.h>
#include <IndustryStandard/Pci30.h>
#include <Library/IoLib.h>
#include <Library/DebugLib.h>
#include <Library/TimerLib.h>
#include <Library/PeiServicesLib.h>
#include <Library/PchCycleDecodingLib.h>
#include <Library/PchPcrLib.h>
#include <Library/PchInfoLib.h>
#include <Library/PciSegmentLib.h>
#include <Library/PostCodeLib.h>
#include <Library/PcdLib.h>
#include <Library/PeiDisplayInitLib.h>
#include <Library/PsfLib.h>
#include <Library/PsfSocLib.h>
#include <Library/PmcSocLib.h>
#include <Library/GpioPrivateLib.h>
#include <Library/GpioNativePads.h>
#include <Library/PeiHdaLib.h>
#include <Library/PeiItssLib.h>
#include <Library/PchPciBdfLib.h>
#include <Library/GraphicsInfoFruLib.h>
#include <Library/PeiCavsInitLib.h>
#include <Register/IgdRegs.h>
#include <Register/PchRegs.h>
#include <Register/HdaRegs.h>
#include <Ppi/SiPolicy.h>
#include <PchConfigHob.h>
#include <Library/HobLib.h>
#include <PchInfoHob.h>

/*
  Configure HDA Audio Link GPIOs

  @param[in] HdaPreMemConfig      Hda PreMem Configuration.
*/
VOID
HdaConfigureHdaLink (
  IN HDAUDIO_PREMEM_CONFIG   *HdaPreMemConfig
  )
{
  GpioSetNativePadByFunction (GPIO_FUNCTION_HDA_BCLK, 0);
  GpioSetNativePadByFunction (GPIO_FUNCTION_HDA_RSTB, 0);
  GpioSetNativePadByFunction (GPIO_FUNCTION_HDA_SYNC, 0);
  GpioSetNativePadByFunction (GPIO_FUNCTION_HDA_SDO, 0);
  if (HdaPreMemConfig->AudioLinkHda.SdiEnable[0]) {
    GpioSetNativePadByFunction (GPIO_FUNCTION_HDA_SDI_0, 0);
  }
  if (HdaPreMemConfig->AudioLinkHda.SdiEnable[1]) {
    GpioSetNativePadByFunction (GPIO_FUNCTION_HDA_SDI_1, 0);
  }

  DEBUG ((DEBUG_INFO, "HDA: HD Audio Link pins configuration\n"));
}

/*
  Configure DMIC Audio Link GPIOs

  @param[in] HdaPreMemConfig      Hda PreMem Configuration.
  @param[in] DmicIndex            Dmic Link Index
*/
VOID
HdaConfigureDmicLinks (
  IN HDAUDIO_PREMEM_CONFIG   *HdaPreMemConfig,
  IN UINT32                  DmicIndex
  )
{
  switch (HdaPreMemConfig->AudioLinkDmic[DmicIndex].DmicClockSelect) {
    case HdaDmicClockSelectClkA:
      GpioSetNativePadByFunction (GPIO_FUNCTION_DMIC_CLKA (DmicIndex), HdaPreMemConfig->AudioLinkDmic[DmicIndex].PinMux.ClkA);
      DEBUG ((DEBUG_INFO, "HDA: DMIC#%d pins configuration ClkA only.\n", DmicIndex));
      break;
    case HdaDmicClockSelectClkB:
      GpioSetNativePadByFunction (GPIO_FUNCTION_DMIC_CLKB (DmicIndex), HdaPreMemConfig->AudioLinkDmic[DmicIndex].PinMux.ClkB);
      DEBUG ((DEBUG_INFO, "HDA: DMIC#%d pins configuration ClkB only.\n", DmicIndex));
      break;
    case HdaDmicClockSelectBoth:
    default:
      GpioSetNativePadByFunction (GPIO_FUNCTION_DMIC_CLKA (DmicIndex), HdaPreMemConfig->AudioLinkDmic[DmicIndex].PinMux.ClkA);
      GpioSetNativePadByFunction (GPIO_FUNCTION_DMIC_CLKB (DmicIndex), HdaPreMemConfig->AudioLinkDmic[DmicIndex].PinMux.ClkB);
      DEBUG ((DEBUG_INFO, "HDA: DMIC#%d pins configuration ClkA and ClkB\n", DmicIndex));
      break;
  }
  GpioSetNativePadByFunction (GPIO_FUNCTION_DMIC_DATA (DmicIndex), HdaPreMemConfig->AudioLinkDmic[DmicIndex].PinMux.Data);
  DEBUG ((DEBUG_INFO, "HDA: DMIC#%d pins configuration\n", DmicIndex));
}

/*
  Configure SSP Audio Link GPIOs

  @param[in] SspIndex       SSP Link Index
*/
VOID
HdaConfigureSspLinks (
  IN UINT32                  SspIndex
  )
{
  EFI_STATUS Status;

  Status = GpioEnableHdaSsp (SspIndex);
  DEBUG ((DEBUG_INFO, "HDA: SSP#%d interface pins configuration, Status = %r\n", SspIndex, Status));
}

/*
  Configure Master Clock GPIOs for SSP Link

  @param[in] MasterClockIndex  SSP Master Clock Index
*/
VOID
HdaConfigureSspMasterClock (
  IN UINT32                  MasterClockIndex
  )
{
  EFI_STATUS Status;

  Status = GpioEnableHdaSspMasterClock (MasterClockIndex);
  DEBUG ((DEBUG_INFO, "HDA: SSP Master Clock pin configuration, Status = %r\n", Status));
}

/*
  Configure Sndw Audio Link GPIOs

  @param[in] SndwIndex       Sndw Link Index
*/
VOID
HdaConfigureSndwLinks (
  IN UINT32                  SndwIndex
  )
{
  EFI_STATUS Status;

  Status = GpioEnableHdaSndw (SndwIndex);
  DEBUG ((DEBUG_INFO, "HDA: SoundWire#%d interface pins configuration, Status = %r\n", SndwIndex, Status));
}

/*
  Function disables HDA Device on PMC level.
*/
VOID
HdaPmcDisableDevice (
  VOID
  )
{
  PmcDisableHda ();
}

/*
  Function disables HDA Device on PSF level.
*/
VOID
HdaPsfDisableDevice (
  VOID
  )
{
  PsfDisableHdaDevice ();
}

/*
  Function disables DSP Bar on PSF level.
*/
VOID
HdaPsfDisableDspBar (
  VOID
  )
{
  PsfDisableDspBar ();
}

/*
  Functions initialize IDisp audio codec in SA.
*/
VOID
HdaInitializeDisplayAudioCodec (
  VOID
  )
{
  InitializeDisplayAudio ();
}

/*
  Functions initialize IDisp audio codec frequency in SA.

  @param[in] RequestedBclkFrequency     IDisplay Link clock frequency to be set
  @param[in] RequestedTmode             IDisplay Link T-Mode to be set
*/
EFI_STATUS
HdaConfigureIDispAudioCodecFrequency (
  IN  HDAUDIO_LINK_FREQUENCY      RequestedBclkFrequency,
  IN  HDAUDIO_IDISP_TMODE         RequestedTmode
  )
{
  return ConfigureIDispAudioFrequency (RequestedBclkFrequency, RequestedTmode);
}

/*
  Function allow to access to SB register.

  @param[in] Offset            Offset of SB register.
  @param[in] AndData           Mask to AND with the SB register.
  @param[in] OrData            Mask to OR with the SB register.
*/
VOID
HdaPcrPidDspAndThenOr32 (
  IN  UINT32                            Offset,
  IN  UINT32                            AndData,
  IN  UINT32                            OrData
  )
{
  PchPcrAndThenOr32 (PID_DSP, Offset, AndData, OrData);
}

/*
  Function allow to access to SB register.

  @param[in] Offset            Offset of SB register.

  @retval Value of SB register
*/
UINT32
HdaPcrPidDspRead32 (
  IN  UINT32                            Offset
  )
{
  return PchPcrRead32 (PID_DSP, Offset);
}

/*
  Function base on type of link and number of link return if link is supported.

  @param[in] AudioLinkType   Link type support to be checked
  @param[in] AudioLinkIndex  Link number

  @retval    TRUE           Link supported
  @retval    FALSE          Link not supported
*/
BOOLEAN
HdaIsAudioInterfaceSupported (
  IN HDAUDIO_LINK_TYPE     AudioLinkType,
  IN UINT32                AudioLinkIndex
  )
{
  return IsAudioInterfaceSupported (AudioLinkType, AudioLinkIndex);
}

/*
  Function check if Tmode is supported for IDisp codec.

  @retval    TRUE           Tmode supported
  @retval    FALSE          Tmode not supported
*/
BOOLEAN
HdaIsAudioIDispTmodeSupported (
  IN HDAUDIO_IDISP_TMODE Tmode
  )
{
  switch (Tmode) {
    case HdaIDispMode1T:
      return (IsPchS ());
    case HdaIDispMode2T:
    case HdaIDispMode4T:
    case HdaIDispMode8T:
      return TRUE;
    case HdaIDispMode16T:
      return (IsPchS ());
    default:
      return FALSE;
  }
}

/*
  Create HDA controller instance.

  @param[in] HdaHandle            Pointer to Hda controller configuration structure
  @param[in] HdaController        Pointer to Hda device structure
  @param[in] HdaCallbacks         Pointer to Hda callbacks structure
  @param[in] SiPolicy             Pointer to SiPolicy
  @param[in] SiPreMemPolicy       Pointer to SiPreMemPolicy
*/
EFI_STATUS
CreateHdaHandle (
  OUT  HDA_HANDLE                 *HdaHandle,
  IN   HDA_PRIVATE_CONFIG         *HdaPrivateConfig, OPTIONAL
  IN   HDA_CONTROLLER             *HdaController,    OPTIONAL
  IN   HDA_CALLBACK               *HdaCallback,      OPTIONAL
  IN   SI_POLICY_PPI              *SiPolicy,         OPTIONAL
  IN   SI_PREMEM_POLICY_PPI       *SiPreMemPolicy    OPTIONAL
  )
{
  EFI_STATUS                    Status;
  HDAUDIO_CONFIG                *HdaConfig;
  HDAUDIO_PREMEM_CONFIG         *HdaPreMemConfig;

  if (SiPolicy != NULL) {
    Status = GetConfigBlock ((VOID *) SiPolicy, &gHdAudioConfigGuid, (VOID *) &HdaConfig);
    ASSERT_EFI_ERROR (Status);
    HdaHandle->HdaConfig = HdaConfig;
  } else {
    HdaHandle->HdaConfig = NULL;
  }

  if (SiPreMemPolicy != NULL) {
    Status = GetConfigBlock ((VOID *) SiPreMemPolicy, &gHdAudioPreMemConfigGuid, (VOID *) &HdaPreMemConfig);
    ASSERT_EFI_ERROR (Status);
    HdaHandle->HdaPreMemConfig = HdaPreMemConfig;
  } else {
    HdaHandle->HdaPreMemConfig = NULL;
  }

  if (HdaController != NULL) {
    HdaController->Segment        = DEFAULT_PCI_SEGMENT_NUMBER_PCH;
    HdaController->Bus            = DEFAULT_PCI_BUS_NUMBER_PCH;
    HdaController->Device         = HdaDevNumber ();
    HdaController->Function       = HdaFuncNumber ();
    HdaController->PciCfgBaseAddr = HdaPciCfgBase ();
    HdaController->HdaMmioAddress = PcdGet32 (PcdSiliconInitTempMemBaseAddr);
    HdaController->DspMmioAddress = PcdGet32 (PcdSiliconInitTempMemBaseAddr) + (UINT32) V_HDA_CFG_HDABAR_SIZE;
  }

  if (HdaCallback != NULL) {
    HdaCallback->HdaConfigureHdaLink                  = &HdaConfigureHdaLink;
    HdaCallback->HdaConfigureSndwLinks                = &HdaConfigureSndwLinks;
    HdaCallback->HdaConfigureDmicLinks                = &HdaConfigureDmicLinks;
    HdaCallback->HdaConfigureSspLinks                 = &HdaConfigureSspLinks;
    HdaCallback->HdaConfigureSspMasterClock           = &HdaConfigureSspMasterClock;
    HdaCallback->HdaPmcDisableDevice                  = &HdaPmcDisableDevice;
    HdaCallback->HdaPsfDisableDevice                  = &HdaPsfDisableDevice;
    HdaCallback->HdaPsfDisableDspBar                  = &HdaPsfDisableDspBar;
    HdaCallback->HdaInitializeDisplayAudioCodec       = &HdaInitializeDisplayAudioCodec;
    HdaCallback->HdaConfigureIDispAudioCodecFrequency = &HdaConfigureIDispAudioCodecFrequency;
    HdaCallback->HdaPcrPidDspAndThenOr32              = &HdaPcrPidDspAndThenOr32;
    HdaCallback->HdaPcrPidDspRead32                   = &HdaPcrPidDspRead32;
    HdaCallback->HdaIsAudioInterfaceSupported         = &HdaIsAudioInterfaceSupported;
    HdaCallback->HdaIsAudioIDispTmodeSupported        = &HdaIsAudioIDispTmodeSupported;
  }

  if (HdaPrivateConfig != NULL) {
    HdaPrivateConfig->HdaMaxSndwLinkNum  = GetPchHdaMaxSndwLinkNum ();
    HdaPrivateConfig->HdaMaxDmicLinkNum  = GetPchHdaMaxDmicLinkNum ();
    HdaPrivateConfig->HdaMaxSspLinkNum   = GetPchHdaMaxSspLinkNum ();
    HdaPrivateConfig->NodeId             = 0x2;

    if (PciSegmentRead16 (PCI_SEGMENT_LIB_ADDRESS (SA_SEG_NUM, IGD_BUS_NUM, IGD_DEV_NUM, IGD_FUN_NUM, PCI_VENDOR_ID_OFFSET)) == 0xFFFF) {
      HdaPrivateConfig->IGfxEnabled = FALSE;
    } else {
      HdaPrivateConfig->IGfxEnabled = TRUE;
    }
  }

  HdaHandle->HdaController     = HdaController;
  HdaHandle->HdaCallback       = HdaCallback;
  HdaHandle->HdaPrivateConfig  = HdaPrivateConfig;

  return EFI_SUCCESS;
}

/**
  Initialize the Intel High Definition Audio Codecs. IDisp audio codec should be initialize after CdClock Initialization.

  @param[in] PeiServices    Pointer to PEI Services Table.
  @param[in] NotifyDesc     Pointer to the descriptor for the Notification event that
                            caused this function to execute.
  @param[in] Ppi            Pointer to the PPI data associated with this function.

  @retval EFI_STATUS        Always return EFI_SUCCESS
**/
EFI_STATUS
EFIAPI
HdAudioInitNotifyOnIDispCodecReady (
  IN CONST EFI_PEI_SERVICES     **PeiServices,
  IN EFI_PEI_NOTIFY_DESCRIPTOR  *NotifyDescriptor,
  IN VOID                       *Ppi
  );

static EFI_PEI_NOTIFY_DESCRIPTOR mHdaIDispCodecReadyNotifyList = {
  (EFI_PEI_PPI_DESCRIPTOR_NOTIFY_CALLBACK | EFI_PEI_PPI_DESCRIPTOR_TERMINATE_LIST),
  &gIDispCodecReadyPpiGuid,
  (EFI_PEIM_NOTIFY_ENTRY_POINT) HdAudioInitNotifyOnIDispCodecReady
};

/**
  Initialize the Intel High Definition Audio Codecs. IDisp audio codec should be initialize after CdClock Initialization.

  @param[in] PeiServices    Pointer to PEI Services Table.
  @param[in] NotifyDesc     Pointer to the descriptor for the Notification event that
                            caused this function to execute.
  @param[in] Ppi            Pointer to the PPI data associated with this function.

  @retval EFI_STATUS        Always return EFI_SUCCESS
**/
EFI_STATUS
EFIAPI
HdAudioInitNotifyOnIDispCodecReady (
  IN CONST EFI_PEI_SERVICES     **PeiServices,
  IN EFI_PEI_NOTIFY_DESCRIPTOR  *NotifyDescriptor,
  IN VOID                       *Ppi
  )
{
  HDA_HANDLE                    HdaHandle;
  HDA_CONTROLLER                HdaController;
  HDA_CALLBACK                  HdaCallback;
  HDA_PRIVATE_CONFIG            HdaPrivateConfig;
  SI_POLICY_PPI                 *SiPolicyPpi;
  SI_PREMEM_POLICY_PPI          *SiPreMemPolicyPpi;
  EFI_STATUS                    Status;
  UINT8                         InterruptPin;

  DEBUG ((DEBUG_INFO, "%a () - Start.\n", __FUNCTION__));

  Status = PeiServicesLocatePpi (
             &gSiPreMemPolicyPpiGuid,
             0,
             NULL,
             (VOID **)&SiPreMemPolicyPpi
             );
  ASSERT_EFI_ERROR (Status);

  Status = PeiServicesLocatePpi (
             &gSiPolicyPpiGuid,
             0,
             NULL,
             (VOID **)&SiPolicyPpi
             );
  ASSERT_EFI_ERROR (Status);

  CreateHdaHandle (&HdaHandle, &HdaPrivateConfig, &HdaController, &HdaCallback, SiPolicyPpi, SiPreMemPolicyPpi);
  PrintHdaHandle (&HdaHandle);

  InterruptPin = ItssGetDevIntPin (SiPolicyPpi, HdaController.Device, HdaController.Function);
  cAvsConfigureHdAudioCodec (&HdaHandle, InterruptPin);

  DEBUG ((DEBUG_INFO, "%a () - End.\n", __FUNCTION__));

  return EFI_SUCCESS;
}

/**
  Initialize the Intel High Definition Audio Controller on Pre-Mem

  @param[in] SiPreMemPolicyPpi       Pre-mem Policy

**/
VOID
HdaPreMemInit (
  IN SI_PREMEM_POLICY_PPI      *SiPreMemPolicyPpi
  )
{
  HDA_HANDLE                    HdaHandle;
  HDA_CONTROLLER                HdaController;
  HDA_CALLBACK                  HdaCallback;
  HDA_PRIVATE_CONFIG            HdaPrivateConfig;

  DEBUG ((DEBUG_INFO, "%a () - Start.\n", __FUNCTION__));

  CreateHdaHandle (&HdaHandle, &HdaPrivateConfig, &HdaController, &HdaCallback, NULL, SiPreMemPolicyPpi);
  PrintHdaHandle (&HdaHandle);
  cAvsConfigureHdAudioOnPreMem (&HdaHandle);

  DEBUG ((DEBUG_INFO, "%a () - End.\n", __FUNCTION__));
}

/**
  Initialize the Intel High Definition Audio Controller. Program of FNCFG register before P2SB Lock

  @param[in] SiPreMemPolicyPpi       Pre-mem Policy
  @param[in] SiPolicyPpi             Si Policy

**/
VOID
HdaPostMemInit (
  IN SI_PREMEM_POLICY_PPI      *SiPreMemPolicyPpi,
  IN SI_POLICY_PPI             *SiPolicyPpi
  )
{
  EFI_STATUS                    Status;
  HDA_HANDLE                    HdaHandle;
  HDA_CONTROLLER                HdaController;
  HDA_CALLBACK                  HdaCallback;
  HDA_PRIVATE_CONFIG            HdaPrivateConfig;
  PCH_CONFIG_HOB                *PchConfigHob;
  EFI_PEI_HOB_POINTERS          HobList;
  UINT32                        XtalValue;
  VOID                          *HobPtr;
  PCH_INFO_HOB                  *PchInfoHob;
  UINT8                         InterruptPin;

  PchInfoHob    = NULL;
  PchConfigHob  = NULL;

  DEBUG ((DEBUG_INFO, "%a () - Start.\n", __FUNCTION__));

  CreateHdaHandle (&HdaHandle, &HdaPrivateConfig, &HdaController, &HdaCallback, SiPolicyPpi, SiPreMemPolicyPpi);
  PrintHdaHandle (&HdaHandle);

  ///
  /// Configure HD Audio interrupt
  ///
  InterruptPin = ItssGetDevIntPin (SiPolicyPpi, HdaController.Device, HdaController.Function);
  PciSegmentWrite8 (HdaController.PciCfgBaseAddr + PCI_INT_PIN_OFFSET, InterruptPin);

  cAvsConfigureHdAudioOnPostMem (&HdaHandle, &XtalValue);

  //
  // Get PCH Config HOB.
  //
  HobList.Guid = GetFirstGuidHob (&gPchConfigHobGuid);
  if (HobList.Guid != NULL) {
    PchConfigHob = (PCH_CONFIG_HOB *) GET_GUID_HOB_DATA (HobList.Guid);
    if (PchConfigHob != NULL) {
      PchConfigHob->HdAudio.AudioXtal = XtalValue;
    }
  }

  HobPtr = GetFirstGuidHob (&gPchInfoHobGuid);
  if (HobPtr != NULL) {
    PchInfoHob = (PCH_INFO_HOB*) GET_GUID_HOB_DATA (HobPtr);
    if (PchInfoHob != NULL) {
      PchInfoHob->AudioDspFusedOut = CavsIsDspFusedOut (&HdaCallback);
    }
  }

  Status = PeiServicesNotifyPpi (&mHdaIDispCodecReadyNotifyList);
  ASSERT_EFI_ERROR (Status);

  DEBUG ((DEBUG_INFO, "%a () - End.\n", __FUNCTION__));
}
