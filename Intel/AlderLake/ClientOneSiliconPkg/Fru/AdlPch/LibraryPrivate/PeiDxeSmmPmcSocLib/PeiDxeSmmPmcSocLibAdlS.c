/** @file
  ADL PCH PMC SoC library.

@copyright
  INTEL CONFIDENTIAL
  Copyright 2020 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains an 'Intel Peripheral Driver' and is uniquely identified as
  "Intel Reference Module" and is licensed for Intel CPUs and chipsets under
  the terms of your license agreement with Intel or your vendor. This file may
  be modified by the user, subject to additional terms of the license agreement.

@par Specification Reference:
**/

#include <Base.h>
#include <Uefi/UefiBaseType.h>
#include <Library/IoLib.h>
#include <Library/DebugLib.h>
#include <Library/BaseLib.h>
#include <Library/BaseMemoryLib.h>
#include <Library/PmcLib.h>
#include <Library/PchPcrLib.h>
#include <Library/PchInfoLib.h>
#include <Library/PmcPrivateLib.h>
#include <Library/SataSocLib.h>
#include <Register/PmcRegs.h>
#include <Register/PmcRegsAdpS.h>
#include <PmcSocConfig.h>

#include <Library/PmcSocLib.h>

/**
  This function enables all SerailIo devices.
  Static power gating disabling takes place after Global Reset, G3 or DeepSx transition.

  @param[in] PmcMmio        PMC MMIO
**/
VOID
PmcEnableSerialIo (
  IN UINT32  PmcMmio
  )
{
  MmioAnd32 (PmcMmio + R_PMC_PWRM_ST_PG_FDIS_PMC_0_V2, (UINT32)~B_PCH_S_PMC_PWRM_ST_PG_FDIS_PMC_0_LPSS);
}

/**
  This function disables (static power gating) all SerailIo devices.
  For SerialIo controllers they can be power gated only if all of them are to be disabled.
  They cannot be statically power gated separately.
  For static power gating to take place Global Reset, G3 or DeepSx transition must happen.

  @param[in] PmcMmio        PMC MMIO
**/
VOID
PmcStaticDisableSerialIo (
  IN UINT32  PmcMmio
  )
{
  MmioOr32 (PmcMmio + R_PMC_PWRM_ST_PG_FDIS_PMC_0_V2, B_PCH_S_PMC_PWRM_ST_PG_FDIS_PMC_0_LPSS);
}

/**
  This function checks if all SerialIo devices are statically disabled (static power gating)

  @param[in] PmcMmio        PMC MMIO

  @retval SerialIo disable state
**/
BOOLEAN
PmcIsSerialIoStaticallyDisabled (
  IN UINT32  PmcMmio
  )
{
  return ((MmioRead32 (PmcMmio + R_PMC_PWRM_ST_PG_FDIS_PMC_0_V2) & B_PCH_S_PMC_PWRM_ST_PG_FDIS_PMC_0_LPSS) == B_PCH_S_PMC_PWRM_ST_PG_FDIS_PMC_0_LPSS);
}

/**
  This function checks if SerialIo device is supported (not disabled by fuse)

  @param[in] PmcMmio        PMC MMIO

  @retval SerialIo support state
**/
BOOLEAN
PmcIsSerialIoSupported (
  IN UINT32  PmcMmio
  )
{
  return ((MmioRead32 (PmcMmio + R_PCH_S_PMC_PWRM_STPG_FUSE_SS_DIS_RD0) & B_PCH_S_PMC_PWRM_STPG_FUSE_SS_DIS_RD0_LPSS) == 0);
}

/**
  This function disables (non-static power gating) SATA and enables ModPHY SPD gating (PCH-LP only).

  @param[in]  SataCtrlIndex     SATA controller index
**/
VOID
PmcDisableSata (
  IN UINT32     SataCtrlIndex
  )
{
}

/**
  This function checks if SATA device is supported (not disabled by fuse)

  @param[in] SataCtrlIndex SATA controller index

  @retval SATA support state
**/
BOOLEAN
PmcIsSataSupported (
  UINT32  SataCtrlIndex
  )
{
  ASSERT (SataCtrlIndex < MaxSataControllerNum ());

  return ((MmioRead32 (PmcGetPwrmBase () + R_PCH_S_PMC_PWRM_STPG_FUSE_SS_DIS_RD0) & B_PCH_S_PMC_PWRM_STPG_FUSE_SS_DIS_RD0_SATA) == 0);
}

/**
  This function checks if ISH is function disabled
  by static power gating

  @retval ISH device state
**/
BOOLEAN
PmcIsIshFunctionDisabled (
  VOID
  )
{
  return ((MmioRead32 ((UINTN) (PmcGetPwrmBase () + R_PMC_PWRM_ST_PG_FDIS_PMC_0_V2)) & B_PCH_S_PMC_PWRM_ST_PG_FDIS_PMC_0_ISH) != 0);
}

/**
  This function checks if ISH device is supported (not disabled by fuse)

  @retval ISH support state
**/
BOOLEAN
PmcIsIshSupported (
  VOID
  )
{
  return ((MmioRead32 ((UINTN) (PmcGetPwrmBase () + R_PCH_S_PMC_PWRM_STPG_FUSE_SS_DIS_RD0)) & B_PCH_S_PMC_PWRM_STPG_FUSE_SS_DIS_RD0_ISH) == 0);
}

/**
  This function will lock the function disable (static and non-static power gating)
  configuration

  @param[in] RegOffset       Offset of the register
  @param[in] DataAnd32       Mask Data for Lock Function disable
  @param[in] DataOr32        Bit to enable the Lock function disable.

**/
VOID
PmcGetFunctionDisableLockConfig (
  IN UINTN* RegOffset,
  IN UINT32* DataAnd32,
  IN UINT32* DataOr32
  )
{
  *RegOffset = R_PMC_PWRM_GEN_PMCON_B;
  *DataOr32 = (UINT32)B_PCH_S_PMC_PMC_PWRM_GEN_PMCON_B_ST_FDIS_LK;
  *DataAnd32 = ~(UINT32)B_PCH_S_PMC_PMC_PWRM_GEN_PMCON_B_ST_FDIS_LK;
  return;
}

/**
  This function checks if function disable (static and non-static power gating)
  configuration is locked

  @param[in] PmcMmio       PMC MMIO

  @retval lock state
**/
BOOLEAN
PmcIsFunctionDisableConfigLocked(
  IN UINT32  PmcMmio
)
{
  return ((MmioRead32 (PmcMmio + R_PMC_PWRM_GEN_PMCON_B) & B_PCH_S_PMC_PMC_PWRM_GEN_PMCON_B_ST_FDIS_LK) != 0);
}

/**
  This function disables ISH device by static power gating.
  For static power gating to take place Global Reset, G3 or DeepSx transition must happen.

  @retval TRUE   Disabled ISH in PMC
  @retval FALSE  Failed to disable ISH in PMC
**/
BOOLEAN
PmcStaticDisableIsh (
  VOID
  )
{
  if (PmcIsFunctionDisableConfigLocked (PmcGetPwrmBase ())) {
    return FALSE;
  }

  MmioOr32 (PmcGetPwrmBase () + R_PMC_PWRM_ST_PG_FDIS_PMC_0_V2, B_PCH_S_PMC_PWRM_ST_PG_FDIS_PMC_0_ISH);
  return TRUE;
}

/**
  This function enables ISH device by disabling static power gating.
  Static power gating disabling takes place after Global Reset, G3 or DeepSx transition.
**/
VOID
PmcEnableIsh (
  VOID
  )
{
  MmioAnd32 (PmcGetPwrmBase () + R_PMC_PWRM_ST_PG_FDIS_PMC_0_V2, (UINT32) (~B_PCH_S_PMC_PWRM_ST_PG_FDIS_PMC_0_ISH));
}

/**
  This function checks if GbE is function disabled
  by static power gating

  @retval GbE device state
**/
BOOLEAN
PmcIsGbeFunctionDisabled (
  VOID
  )
{
  return ((MmioRead32 ((UINTN) (PmcGetPwrmBase () + R_PMC_PWRM_ST_PG_FDIS_PMC_1_V2)) & B_PCH_S_PMC_PWRM_ST_PG_FDIS_PMC_1_GBE) != 0);
}

/**
  This function enables GBE ModPHY SPD gating.
**/
VOID
PmcGbeModPhyPowerGating (
  VOID
  )
{
}

/**
  This function disables GbE device by static power gating and enables ModPHY SPD gating (PCH-LP only).
  For static power gating to take place Global Reset, G3 or DeepSx transition must happen.
**/
VOID
PmcStaticDisableGbe (
  VOID
  )
{
  UINT32 PchPwrmBase;
  PchPwrmBase = PmcGetPwrmBase ();

  ASSERT (!PmcIsFunctionDisableConfigLocked (PmcGetPwrmBase ()));

  MmioOr32 (PchPwrmBase + R_PMC_PWRM_ST_PG_FDIS_PMC_1_V2, B_PCH_S_PMC_PWRM_ST_PG_FDIS_PMC_1_GBE);
}

/**
  This function enables GbE device by disabling static power gating.
  Static power gating disabling takes place after Global Reset, G3 or DeepSx transition.
**/
VOID
PmcEnableGbe (
  VOID
  )
{
  //
  // Before modifying LAN Disable bit, make sure it's not locked.
  //
  ASSERT (!PmcIsFunctionDisableConfigLocked (PmcGetPwrmBase ()));

  MmioAnd32 (PmcGetPwrmBase () + R_PMC_PWRM_ST_PG_FDIS_PMC_1_V2, (UINT32) ~B_PCH_S_PMC_PWRM_ST_PG_FDIS_PMC_1_GBE);
}

/**
  This function checks if GbE device is supported (not disabled by fuse)

  @param[in] PmcMmio       PMC MMIO

  @retval GbE support state
**/
BOOLEAN
PmcIsGbeSupported (
  IN UINT32  PmcMmio
  )
{
  return ((MmioRead32 (PmcMmio + R_PCH_S_PMC_PWRM_STPG_FUSE_SS_DIS_RD1) & B_PCH_S_PMC_PWRM_STPG_FUSE_SS_DIS_RD1_GBE) == 0);
}

/**
  This function disables (non-static power gating) PCIe Root Port and enables ModPHY SPD gating (PCH-LP only).

  @param[in] RpIndex        PCIe Root Port Index (0 based)
**/
VOID
PmcDisablePcieRootPort (
  IN UINT32  RpIndex
  )
{
}

/**
  This function disables (non-static power gating) xHCI and enables ModPHY SPD gating (PCH-LP only).
**/
VOID
PmcDisableXhci (
  VOID
  )
{
}

/**
  This function disables (non-static power gating) XDCI and enables ModPHY SPD gating (PCH-LP only).
**/
VOID
PmcDisableXdci (
  VOID
  )
{
}

/**
  This function checks if XDCI device is supported (not disabled by fuse)

  @retval XDCI support state
**/
BOOLEAN
PmcIsXdciSupported (
  VOID
  )
{
  return ((MmioRead32 (PmcGetPwrmBase () + R_PCH_S_PMC_PWRM_STPG_FUSE_SS_DIS_RD0) & B_PCH_S_PMC_PWRM_STPG_FUSE_SS_DIS_RD0_XDCI) == 0);
}

/**
  This function disables (non-static power gating) HDA device
**/
VOID
PmcDisableHda (
  VOID
  )
{
}

/**
  This function checks if Cnvi device is supported (not disabled by fuse)

  @retval Cnvi support state
**/
BOOLEAN
PmcIsCnviSupported (
  VOID
  )
{
  return ((MmioRead32 (PmcGetPwrmBase () + R_PCH_S_PMC_PWRM_STPG_FUSE_SS_DIS_RD1) & B_PCH_S_PMC_PWRM_STPG_FUSE_SS_DIS_RD1_CNVI) == 0);
}

/**
  This function checks if CNVi is function disabled
  by static power gating

  @retval GbE device state
**/
BOOLEAN
PmcIsCnviFunctionDisabled (
  VOID
  )
{
  return ((MmioRead32 (PmcGetPwrmBase () + R_PMC_PWRM_ST_PG_FDIS_PMC_1_V2) & B_PCH_S_PMC_PWRM_ST_PG_FDIS_PMC_1_CNVI) != 0);
}

/**
  This function enables CNVi device by disabling static power gating.
  Static power gating disabling takes place after Global Reset, G3 or DeepSx transition.
**/
VOID
PmcEnableCnvi (
  VOID
  )
{
  MmioAnd32 (PmcGetPwrmBase () + R_PMC_PWRM_ST_PG_FDIS_PMC_1_V2, (UINT32) ~B_PCH_S_PMC_PWRM_ST_PG_FDIS_PMC_1_CNVI);
}

/**
  This function disables CNVi device by static power gating.
  For static power gating to take place Global Reset, G3 or DeepSx transition must happen.
**/
VOID
PmcStaticDisableCnvi (
  VOID
  )
{
  MmioOr32 (PmcGetPwrmBase () + R_PMC_PWRM_ST_PG_FDIS_PMC_1_V2, B_PCH_S_PMC_PWRM_ST_PG_FDIS_PMC_1_CNVI);
}

/**
  Check if MODPHY SUS PG is supported

  @retval  Status of MODPHY SUS PG support
**/
BOOLEAN
PmcIsModPhySusPgSupported (
  VOID
  )
{
  return FALSE;
}

/**
  Get the PMC SOC configuration.

  @param[out] PmcSocConfig  Configuration of the PMC
**/
VOID
PmcGetSocConfig (
  OUT PMC_SOC_CONFIG  *PmcSocConfig
  )
{
  if (PmcSocConfig == NULL) {
    return;
  }

  ZeroMem (PmcSocConfig, sizeof (PMC_SOC_CONFIG));

  PmcSocConfig->CppmCgInterfaceVersion = 2;
  PmcSocConfig->LpmSupported = TRUE;
  PmcSocConfig->LpmInterfaceVersion = 2;
}

/**
  Check if PMC Extended straps are supported

  @retval  Status of Extended Straps support
**/
BOOLEAN
PmcIsExtendedStrapsSupported (
  VOID
  )
{
  return TRUE;
}

/**
  Check if FIVR is supported.

  @return BOOLEAN specifing whether FIVR is supported in PMC.
**/
BOOLEAN
PmcIsFivrSupported (
  VOID
  )
{
  return FALSE;
}

/**
  Check if PMC IPC1 command for CPU straps is supported.

  @retval  Status of CPU Straps support
**/
BOOLEAN
PmcIsCpuStrapsIpcCommandSupported (
  VOID
  )
{
  return TRUE;
}
