/** @file
  TGL PCH PMC SoC library.

@copyright
  INTEL CONFIDENTIAL
  Copyright 2020 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains an 'Intel Peripheral Driver' and is uniquely identified as
  "Intel Reference Module" and is licensed for Intel CPUs and chipsets under
  the terms of your license agreement with Intel or your vendor. This file may
  be modified by the user, subject to additional terms of the license agreement.

@par Specification Reference:
**/

#include <Base.h>
#include <PiPei.h>
#include <Uefi/UefiBaseType.h>
#include <Library/IoLib.h>
#include <Library/DebugLib.h>
#include <Library/BaseLib.h>
#include <Library/BaseMemoryLib.h>
#include <Library/PeiServicesLib.h>
#include <Library/PchInfoLib.h>
#include <Library/PmcPrivateLib.h>
#include <Register/FlashRegs.h>
#include <Ppi/Spi.h>

#include <Library/PmcSocLib.h>

/**
  Check whether GBETSN_DIS strap is enabled. Note that even
  though this function doesn't have anything to do with PMC it is
  historically a PMC lib responsibility to check whether IPs are fuse/strap
  disabled.

  @retval TRUE                    GBETSN_DIS strap is enabled
  @retval FALSE                   GBETSN_DIS strap is disabled
**/
BOOLEAN
PmcIsGbeTsnStrapDis (
  VOID
  )
{
  EFI_STATUS                Status;
  PCH_SPI_PPI               *SpiPpi;
  UINT8                     GbeTsnStrapValue;

  Status = PeiServicesLocatePpi (
             &gPchSpiPpiGuid,
             0,
             NULL,
             (VOID **)&SpiPpi
             );
  if (EFI_ERROR (Status)) {
    DEBUG ((DEBUG_ERROR, "SpiPpi is not available\n"));
    return FALSE;
  }

  Status = SpiPpi->FlashRead (SpiPpi, FlashRegionAll, R_PCH_SPI_STRP_GBETSN_DIS, sizeof (GbeTsnStrapValue), &GbeTsnStrapValue);
  if (EFI_ERROR (Status)) {
    DEBUG ((DEBUG_ERROR, "FlashRead failed\n"));
    return FALSE;
  }

  if (GbeTsnStrapValue & B_PCH_SPI_STRP_GBETSN_DIS) {
    return TRUE;
  }
  return FALSE;
}

/**
  Checks if the Intel Touch Host Controller is Enabled. Note that
  even though this function doesn't have anything to do with PMC it is
  historically a PMC lib responsibility to check whether IPs are fuse/strap
  disabled.

  @param[in] ThcNumber            THC0/THC1 0 or 1

  @retval TRUE                    THC_DIS strap is 0
  @retval FALSE                   THC_DIS strap is 1
**/
BOOLEAN
PmcIsThcEnabled (
  IN UINT8  ThcNumber
  )
{
  EFI_STATUS     Status;
  PCH_SPI_PPI    *SpiPpi;
  UINT32         ThcStrapValue;
  STATIC UINT32  ThcStrap [] = { B_PCH_SPI_STRP_THC0_DIS, B_PCH_SPI_STRP_THC1_DIS };

  Status = PeiServicesLocatePpi (
             &gPchSpiPpiGuid,
             0,
             NULL,
             (VOID **)&SpiPpi
             );
  if (EFI_ERROR (Status)) {
    DEBUG ((DEBUG_ERROR, "SpiPpi is not available\n"));
    return FALSE;
  }

  Status = SpiPpi->FlashRead (SpiPpi, FlashRegionAll, R_PCH_SPI_STRP_THC_DIS, sizeof (ThcStrapValue), (UINT8 *) &ThcStrapValue);
  ASSERT_EFI_ERROR (Status);

  if (ThcStrapValue & ThcStrap[ThcNumber]) {
    DEBUG ((DEBUG_INFO, "THC%d: Strap disabled \n", ThcNumber));
    return FALSE;
  }

  return TRUE;
}

