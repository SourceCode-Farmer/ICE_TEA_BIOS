/** @file
  ADL PCH PMC SoC library.

@copyright
  INTEL CONFIDENTIAL
  Copyright 2020 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains an 'Intel Peripheral Driver' and is uniquely identified as
  "Intel Reference Module" and is licensed for Intel CPUs and chipsets under
  the terms of your license agreement with Intel or your vendor. This file may
  be modified by the user, subject to additional terms of the license agreement.

@par Specification Reference:
**/

#include <Base.h>
#include <PiPei.h>
#include <Uefi/UefiBaseType.h>
#include <Library/IoLib.h>
#include <Library/DebugLib.h>
#include <Library/BaseLib.h>
#include <Library/BaseMemoryLib.h>
#include <Library/PeiServicesLib.h>
#include <Library/PchInfoLib.h>
#include <Library/PmcPrivateLib.h>
#include <Register/PmcRegsAdpS.h>

#include <Library/PmcSocLib.h>

/**
  Check whether GBETSN_DIS strap is enabled. Note that even
  though this function doesn't have anything to do with PMC it is
  historically a PMC lib responsibility to check whether IPs are fuse/strap
  disabled.

  @retval TRUE                    GBETSN_DIS strap is enabled
  @retval FALSE                   GBETSN_DIS strap is disabled
**/
BOOLEAN
PmcIsGbeTsnStrapDis (
  VOID
  )
{
  return ((MmioRead32 (PmcGetPwrmBase () + R_PCH_S_PMC_PWRM_STPG_FUSE_SS_DIS_RD3) & B_PCH_S_PMC_PWRM_STPG_FUSE_SS_DIS_RD3_GBETSN) != 0);
}

/**
  Checks if the Intel Touch Host Controller is enabled.
  For S SKU we check the register in PMC. For P and M we
  have to check the flash descriptor as PMC doesn't forward this
  information.

  @param[in] ThcNumber            THC0/THC1 0 or 1

  @retval TRUE                    THC_DIS strap is 0
  @retval FALSE                   THC_DIS strap is 1
**/
BOOLEAN
PmcIsThcEnabled (
  IN UINT8  ThcNumber
  )
{
  UINT32  FuseReg;

  FuseReg = MmioRead32 (PmcGetPwrmBase () + R_PCH_S_PMC_PWRM_STPG_FUSE_SS_DIS_RD3);
  if (ThcNumber == 0) {
    return ((FuseReg & B_PCH_S_PMC_PWRM_STPG_FUSE_SS_DIS_RD3_THC0) == 0);
  } else if (ThcNumber == 1) {
    return ((FuseReg & B_PCH_S_PMC_PWRM_STPG_FUSE_SS_DIS_RD3_THC1) == 0);
  } else {
    DEBUG ((DEBUG_ERROR, "Incorrect THC number\n"));
    return FALSE;
  }
}

