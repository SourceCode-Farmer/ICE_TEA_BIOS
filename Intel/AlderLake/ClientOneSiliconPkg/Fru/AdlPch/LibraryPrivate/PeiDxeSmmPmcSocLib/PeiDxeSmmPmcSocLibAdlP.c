/** @file
  TGL PCH PMC SoC library.

@copyright
  INTEL CONFIDENTIAL
  Copyright 2020 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains an 'Intel Peripheral Driver' and is uniquely identified as
  "Intel Reference Module" and is licensed for Intel CPUs and chipsets under
  the terms of your license agreement with Intel or your vendor. This file may
  be modified by the user, subject to additional terms of the license agreement.

@par Specification Reference:
**/

#include <Base.h>
#include <Uefi/UefiBaseType.h>
#include <Library/IoLib.h>
#include <Library/DebugLib.h>
#include <Library/BaseLib.h>
#include <Library/BaseMemoryLib.h>
#include <Library/PmcLib.h>
#include <Library/PchPcrLib.h>
#include <Library/PchInfoLib.h>
#include <Library/PmcPrivateLib.h>
#include <Library/SataSocLib.h>
#include <Register/PmcRegs.h>
#include <Register/PmcRegsAdpP.h>

#include <Library/PmcSocLib.h>

/**
  This function checks if function disable (static and non-static power gating)
  configuration is locked

  @param[in] PmcMmio       PMC MMIO

  @retval lock state
**/
BOOLEAN
PmcIsFunctionDisableConfigLocked (
  IN UINT32  PmcMmio
  )
{
  return ((MmioRead32 (PmcMmio + R_PMC_PWRM_ST_PG_FDIS_PMC_1) & B_PMC_PWRM_ST_PG_FDIS_PMC_1_ST_FDIS_LK) != 0);
}

/**
  This function will return the lock function disable (static and non-static power gating)
  configuration

  @param[in] RegOffset       Offset of the register
  @param[in] DataAnd32       Mask Data for Lock Function disable
  @param[in] DataOr32        Bit to enable the Lock function disable.param[in] PmcMmio       PMC MMIO

**/
VOID
PmcGetFunctionDisableLockConfig (
  IN UINTN  *RegOffset,
  IN UINT32 *DataAnd32,
  IN UINT32 *DataOr32
)
{
  *RegOffset = (UINTN)R_PMC_PWRM_ST_PG_FDIS_PMC_1;
  *DataOr32 = (UINT32)B_PMC_PWRM_ST_PG_FDIS_PMC_1_ST_FDIS_LK;
  *DataAnd32 = ~(UINT32)B_PMC_PWRM_ST_PG_FDIS_PMC_1_ST_FDIS_LK;
  return;
}

/**
  This function disables (non-static power gating) SCS UFS controller and enables ModPHY SPD gating (PCH-LP only).
**/
VOID
PmcDisableScsUfs (
  VOID
  )
{
  UINT32        PchPwrmBase;

  PchPwrmBase = PmcGetPwrmBase ();
  //
  // Set PWRMBASE + NST_PG_FDIS_1 to disable SCS Controller in PMC
  //
  MmioOr32 (PchPwrmBase + R_PMC_PWRM_NST_PG_FDIS_1, B_PCH_LP_PMC_PWRM_NST_PG_FDIS_1_UFS_FDIS_PMC);

  if (PmcIsModPhySusPgSupported ()) {
    //
    // Set MSPDRTREQ:
    // PchPwrmBase + R_PWRM_MODPHY_PM_CFG5[16] = 1 to enable ASL code trigger request for ModPHY SPD gating.
    //
    MmioOr32 (PchPwrmBase + R_PMC_PWRM_MODPHY_PM_CFG5, B_PMC_PWRM_MODPHY_PM_CFG5_MSPDRTREQ_UFS2);
  }
}

/**
  This function checks if SCS UFS device is supported (not disabled by fuse)

  @retval SCS device support state
**/
BOOLEAN
PmcIsScsUfsSupported (
  VOID
  )
{
  return ((MmioRead32 (PmcGetPwrmBase () + R_PMC_PWRM_FUSE_DIS_RD_2) & B_PMC_PWRM_FUSE_DIS_RD_2_UFSX2_FUSE_SS_DIS) == 0);
}

/**
  This function disables (non-static power gating) SCS eMMC controller.
**/
VOID
PmcDisableScsEmmc (
  VOID
  )
{
  //
  // Set PWRMBASE + NST_PG_FDIS_1 to disable SCS Controller in PMC
  //
  MmioOr32 (PmcGetPwrmBase () + R_PMC_PWRM_NST_PG_FDIS_1, B_PMC_PWRM_NST_PG_FDIS_1_EMMC_FDIS_PMC);
}

/**
  This function checks if SCS eMMC device is supported (not disabled by fuse)
  @retval SCS device support state
**/
BOOLEAN
PmcIsScsEmmcSupported (
  VOID
  )
{
  //
  // Get fuse info from PWRMBASE + FUSE_SS_DIS_RD_2
  //
  return ((MmioRead32 (PmcGetPwrmBase () + R_PMC_PWRM_FUSE_DIS_RD_2) & B_PMC_PWRM_FUSE_DIS_RD_2_EMMC_FUSE_SS_DIS) == 0);
}

/**
  This function enables all SerailIo devices.
  Static power gating disabling takes place after Global Reset, G3 or DeepSx transition.

  @param[in] PmcMmio        PMC MMIO
**/
VOID
PmcEnableSerialIo (
  IN UINT32  PmcMmio
  )
{
  //
  // Set PWRMBASE + ST_PG_FDIS_PMC_2
  //
  MmioAnd32 (PmcMmio + R_PMC_PWRM_ST_PG_FDIS_PMC_2, (UINT32)~B_PMC_PWRM_ST_PG_FDIS_PMC_2_SERIALIO);
}

/**
  This function disables (static power gating) all SerailIo devices.
  For SerialIo controllers they can be power gated only if all of them are to be disabled.
  They cannot be statically power gated separately.
  For static power gating to take place Global Reset, G3 or DeepSx transition must happen.

  @param[in] PmcMmio        PMC MMIO
**/
VOID
PmcStaticDisableSerialIo (
  IN UINT32  PmcMmio
  )
{
  //
  // Set PWRMBASE + ST_PG_FDIS_PMC_2
  //
  MmioOr32 (PmcMmio + R_PMC_PWRM_ST_PG_FDIS_PMC_2, B_PMC_PWRM_ST_PG_FDIS_PMC_2_SERIALIO);
}

/**
  This function checks if all SerialIo devices are statically disabled (static power gating)

  @retval SerialIo disable state

  @param[in] PmcMmio        PMC MMIO
**/
BOOLEAN
PmcIsSerialIoStaticallyDisabled (
  IN UINT32  PmcMmio
  )
{
  //
  // Check if all SerialIo controllers are statically disabled in PMC
  //
  return ((MmioRead32 (PmcMmio + R_PMC_PWRM_ST_PG_FDIS_PMC_2) & B_PMC_PWRM_ST_PG_FDIS_PMC_2_SERIALIO) == B_PMC_PWRM_ST_PG_FDIS_PMC_2_SERIALIO);
}

/**
  This function checks if SerialIo device is supported (not disabled by fuse)

  @retval SerialIo support state

  @param[in] PmcMmio        PMC MMIO
**/
BOOLEAN
PmcIsSerialIoSupported (
  IN UINT32  PmcMmio
  )
{
  //
  // Get fuse info from PWRMBASE + FUSE_SS_DIS_RD_2
  //
  return ((MmioRead32 (PmcMmio + R_PMC_PWRM_FUSE_DIS_RD_2) & B_PMC_PWRM_FUSE_DIS_RD_2_SERIALIO_FUSE_SS_DIS) == 0);
}

/**
  This function disables (non-static power gating) SATA and enables ModPHY SPD gating (PCH-LP only).

  @param[in]  SataCtrlIndex     SATA controller index
**/
VOID
PmcDisableSata (
  IN UINT32     SataCtrlIndex
  )
{
  UINT32 PchPwrmBase;
  PchPwrmBase = PmcGetPwrmBase ();

  ASSERT (SataCtrlIndex < MaxSataControllerNum ());

  //
  // Set PWRMBASE + NST_PG_FDIS_1 [22] = 1b to disable SATA Controller in PMC
  //
  MmioOr32 (PchPwrmBase + R_PMC_PWRM_NST_PG_FDIS_1, B_PMC_PWRM_NST_PG_FDIS_1_SATA_FDIS_PMC);

  if (PmcIsModPhySusPgSupported ()) {
    //
    // MPHY SUS PG supported on PCH-LP only:
    //
    // Set MSPDRTREQ:
    // PWRMBASE + R_PWRM_MODPHY_PM_CFG5[12] = 1 to enable ASL code trigger request for ModPHY SPD gating.
    //
    MmioOr32 (PchPwrmBase + R_PMC_PWRM_MODPHY_PM_CFG5, B_PMC_PWRM_MODPHY_PM_CFG5_MSPDRTREQ_SATA);
  }
}

/**
  This function checks if SATA device is supported (not disabled by fuse)

  @param[in] SataCtrlIndex SATA controller index

  @retval SATA support state
**/
BOOLEAN
PmcIsSataSupported (
  UINT32  SataCtrlIndex
  )
{
  ASSERT (SataCtrlIndex < MaxSataControllerNum ());

  //
  // Get fuse info from PWRMBASE + FUSE_SS_DIS_RD_2
  //
  return ((MmioRead32 (PmcGetPwrmBase () + R_PMC_PWRM_FUSE_DIS_RD_2) & B_PMC_PWRM_FUSE_DIS_RD_2_SATA_FUSE_SS_DIS) == 0);
}

/**
  This function checks if ISH is function disabled
  by static power gating

  @retval ISH device state
**/
BOOLEAN
PmcIsIshFunctionDisabled (
  VOID
  )
{
  //
  // Get PG info from PWRMBASE + ST_PG_FDIS_PMC_1
  //
  return ((MmioRead32 ((UINTN) (PmcGetPwrmBase () + R_PMC_PWRM_ST_PG_FDIS_PMC_1)) & B_PMC_PWRM_ST_PG_FDIS_PMC_1_ISH_FDIS_PMC) != 0);
}

/**
  This function checks if ISH device is supported (not disabled by fuse)

  @retval ISH support state
**/
BOOLEAN
PmcIsIshSupported (
  VOID
  )
{
  //
  // Get fuse info from PWRMBASE + FUSE_DIS_RD_2
  //
  return ((MmioRead32 ((UINTN) (PmcGetPwrmBase () + R_PMC_PWRM_FUSE_DIS_RD_2)) & B_PMC_PWRM_FUSE_DIS_RD_2_ISH_FUSE_SS_DIS) == 0);
}

/**
  This function disables ISH device by static power gating.
  For static power gating to take place Global Reset, G3 or DeepSx transition must happen.

  @retval TRUE   Disabled ISH in PMC
  @retval FALSE  Failed to disable ISH in PMC
**/
BOOLEAN
PmcStaticDisableIsh (
  VOID
  )
{
  if (PmcIsFunctionDisableConfigLocked (PmcGetPwrmBase ())) {
    return FALSE;
  }
  //
  // Set PWRMBASE + ST_PG_FDIS_PMC_1[5] = 1b to statically disable ISH Controller
  //
  MmioOr32 (PmcGetPwrmBase () + R_PMC_PWRM_ST_PG_FDIS_PMC_1, B_PMC_PWRM_ST_PG_FDIS_PMC_1_ISH_FDIS_PMC);
  return TRUE;
}

/**
  This function enables ISH device by disabling static power gating.
  Static power gating disabling takes place after Global Reset, G3 or DeepSx transition.
**/
VOID
PmcEnableIsh (
  VOID
  )
{
  //
  // Set PWRMBASE + ST_PG_FDIS_PMC_1[5] = 0b to enable ISH controller
  //
  MmioAnd32 (PmcGetPwrmBase () + R_PMC_PWRM_ST_PG_FDIS_PMC_1, (UINT32) (~B_PMC_PWRM_ST_PG_FDIS_PMC_1_ISH_FDIS_PMC));
}

/**
  This function checks if GbE is function disabled
  by static power gating

  @retval GbE device state
**/
BOOLEAN
PmcIsGbeFunctionDisabled (
  VOID
  )
{
  //
  // Get PG info from PWRMBASE + ST_PG_FDIS_PMC_1
  //
  return ((MmioRead32 ((UINTN) (PmcGetPwrmBase () + R_PMC_PWRM_ST_PG_FDIS_PMC_1)) & B_PMC_PWRM_ST_PG_FDIS_PMC_1_GBE_FDIS_PMC) != 0);
}

/**
  This function enables GBE ModPHY SPD gating.
**/
VOID
PmcGbeModPhyPowerGating (
  VOID
  )
{
  if (PmcIsModPhySusPgSupported ()) {
    //
    // Set B_PCH_PWRM_MODPHY_PM_CFG5_MSPDRTREQ_GBE ModPHY SPD RT Request
    //
    MmioOr32 (PmcGetPwrmBase () + R_PMC_PWRM_MODPHY_PM_CFG5, B_PMC_PWRM_MODPHY_PM_CFG5_MSPDRTREQ_GBE);
  }
}

/**
  This function disables GbE device by static power gating and enables ModPHY SPD gating (PCH-LP only).
  For static power gating to take place Global Reset, G3 or DeepSx transition must happen.
**/
VOID
PmcStaticDisableGbe (
  VOID
  )
{
  UINT32 PchPwrmBase;
  PchPwrmBase = PmcGetPwrmBase ();

  ASSERT (!PmcIsFunctionDisableConfigLocked (PmcGetPwrmBase ()));

  //
  // Set PWRMBASE + ST_PG_FDIS_PMC_1[0] = 1b to statically disable GbE Controller
  //
  MmioOr32 (PchPwrmBase + R_PMC_PWRM_ST_PG_FDIS_PMC_1, B_PMC_PWRM_ST_PG_FDIS_PMC_1_GBE_FDIS_PMC);

  PmcGbeModPhyPowerGating ();
}

/**
  This function enables GbE device by disabling static power gating.
  Static power gating disabling takes place after Global Reset, G3 or DeepSx transition.
**/
VOID
PmcEnableGbe (
  VOID
  )
{
  //
  // Before modifying LAN Disable bit, make sure it's not locked.
  //
  ASSERT (!PmcIsFunctionDisableConfigLocked (PmcGetPwrmBase ()));

  //
  // Set PWRMBASE + ST_PG_FDIS_PMC_1[0] = 0b to enable GbE controller
  //
  MmioAnd32 (PmcGetPwrmBase () + R_PMC_PWRM_ST_PG_FDIS_PMC_1, (UINT32) ~B_PMC_PWRM_ST_PG_FDIS_PMC_1_GBE_FDIS_PMC);
}

/**
  This function checks if GbE device is supported (not disabled by fuse)

  @param[in] PmcMmio       PMC MMIO

  @retval GbE support state
**/
BOOLEAN
PmcIsGbeSupported (
  IN UINT32  PmcMmio
  )
{
  //
  // Get fuse info from PWRMBASE + FUSE_SS_DIS_RD_2
  //
  return ((MmioRead32 (PmcMmio + R_PMC_PWRM_FUSE_DIS_RD_2) & B_PMC_PWRM_FUSE_DIS_RD_2_GBE_FUSE_SS_DIS) == 0);
}

/**
  This function disables (non-static power gating) PCIe Root Port and enables ModPHY SPD gating (PCH-LP only).

  @param[in] RpIndex        PCIe Root Port Index (0 based)
**/
VOID
PmcDisablePcieRootPort (
  IN UINT32  RpIndex
  )
{
  UINT32   NstPgFdis1Mask;
  UINT32   PchPwrmBase;

  PchPwrmBase = PmcGetPwrmBase ();
  //
  // Set PchPwrmBase + NST_PG_FDIS_1 to function disable PCIE port in PMC
  //
  if (IsPchH () && RpIndex >= 20) {
    NstPgFdis1Mask = B_PCH_H_PMC_PWRM_NST_PG_FDIS_1_PCIE_F0_FDIS_PMC << (RpIndex % 20);
  } else {
    NstPgFdis1Mask = B_PMC_PWRM_NST_PG_FDIS_1_PCIE_A0_FDIS_PMC << RpIndex;
  }
  MmioOr32 (PchPwrmBase + R_PMC_PWRM_NST_PG_FDIS_1, NstPgFdis1Mask);

  if (PmcIsModPhySusPgSupported ()) {
    //
    // Set MSPDRTREQ:
    // PWRMBASE + R_PWRM_MODPHY_PM_CFG5[26:0] = 1 to enable ASL code trigger request for ModPHY SPD gating.
    //
    if (RpIndex <= 11) {
      MmioOr32 (PchPwrmBase + R_PMC_PWRM_MODPHY_PM_CFG5, B_PMC_PWRM_MODPHY_PM_CFG5_MSPDRTREQ_A0 << RpIndex);
    } else {
      MmioOr32 (PchPwrmBase + R_PMC_PWRM_MODPHY_PM_CFG5, B_PMC_PWRM_MODPHY_PM_CFG5_MSPDRTREQ_D0 << (RpIndex - 12));
    }
  }
}

/**
  This function disables (non-static power gating) xHCI and enables ModPHY SPD gating (PCH-LP only).
**/
VOID
PmcDisableXhci (
  VOID
  )
{
  UINT32   PchPwrmBase;
  PchPwrmBase = PmcGetPwrmBase ();

  //
  // Set PWRMBASE + NST_PG_FDIS_1 [0] = 1b
  //
  MmioOr32 (PchPwrmBase + R_PMC_PWRM_NST_PG_FDIS_1, B_PMC_PWRM_NST_PG_FDIS_1_XHCI_FDIS_PMC);

  if (PmcIsModPhySusPgSupported ()) {
    //
    // Set MSPDRTREQ:
    // PchPwrmBase + R_PWRM_MODPHY_PM_CFG5[14] = 1 to enable ASL code trigger request for ModPHY SPD gating.
    //
    MmioOr32 (PchPwrmBase + R_PMC_PWRM_MODPHY_PM_CFG5, B_PMC_PWRM_MODPHY_PM_CFG5_MSPDRTREQ_XHCI);
  }
}

/**
  This function disables (non-static power gating) XDCI and enables ModPHY SPD gating (PCH-LP only).
**/
VOID
PmcDisableXdci (
  VOID
  )
{
  UINT32 PchPwrmBase;
  PchPwrmBase = PmcGetPwrmBase ();

  //
  // Set PWRMBASE + NST_PG_FDIS_1 [26] = 1b to disable XDCI Controller in PMC
  //
  MmioOr32 (PchPwrmBase + R_PMC_PWRM_NST_PG_FDIS_1, B_PMC_PWRM_NST_PG_FDIS_1_XDCI_FDIS_PMC);

  if (PmcIsModPhySusPgSupported ()) {
    //
    // Set MSPDRTREQ:
    // PWRMBASE + R_PWRM_MODPHY_PM_CFG5[15] = 1 to enable ASL code trigger request for ModPHY SPD gating.
    //
    MmioOr32 (PchPwrmBase + R_PMC_PWRM_MODPHY_PM_CFG5, B_PMC_PWRM_MODPHY_PM_CFG5_MSPDRTREQ_XDCI);
  }
}

/**
  This function checks if XDCI device is supported (not disabled by fuse)

  @retval XDCI support state
**/
BOOLEAN
PmcIsXdciSupported (
  VOID
  )
{
  //
  // Get fuse info from PWRMBASE + FUSE_SS_DIS_RD_2
  //
  return ((MmioRead32 (PmcGetPwrmBase () + R_PMC_PWRM_FUSE_DIS_RD_2) & B_PMC_PWRM_FUSE_DIS_RD_2_OTG_FUSE_SS_DIS) == 0);
}

/**
  This function disables (non-static power gating) HDA device
**/
VOID
PmcDisableHda (
  VOID
  )
{
  MmioOr32 (PmcGetPwrmBase () + R_PMC_PWRM_NST_PG_FDIS_1, B_PMC_PWRM_NST_PG_FDIS_1_ADSP_FDIS_PMC);
}

/**
  This function checks if Cnvi device is supported (not disabled by fuse)

  @retval Cnvi support state
**/
BOOLEAN
PmcIsCnviSupported (
  VOID
  )
{
  //
  // Get fuse info from PWRMBASE + FUSE_SS_DIS_RD_2
  //
  return ((MmioRead32 (PmcGetPwrmBase () + R_PMC_PWRM_FUSE_DIS_RD_2) & B_PMC_PWRM_FUSE_DIS_RD_2_CNVI_FUSE_SS_DIS) == 0);
}

/**
  This function checks if CNVi is function disabled
  by static power gating

  @retval GbE device state
**/
BOOLEAN
PmcIsCnviFunctionDisabled (
  VOID
  )
{
  //
  // Get PG info from PWRMBASE + ST_PG_FDIS_PMC_1
  //
  return ((MmioRead32 (PmcGetPwrmBase () + R_PMC_PWRM_ST_PG_FDIS_PMC_1) & B_PMC_PWRM_ST_PG_FDIS_PMC_1_CNVI_FDIS_PMC) != 0);
}

/**
  This function enables CNVi device by disabling static power gating.
  Static power gating disabling takes place after Global Reset, G3 or DeepSx transition.
**/
VOID
PmcEnableCnvi (
  VOID
  )
{
  //
  // Set PWRMBASE + ST_PG_FDIS_PMC_1 to enable CNVi controller
  //
  MmioAnd32 (PmcGetPwrmBase () + R_PMC_PWRM_ST_PG_FDIS_PMC_1, (UINT32) ~B_PMC_PWRM_ST_PG_FDIS_PMC_1_CNVI_FDIS_PMC);
}

/**
  This function disables CNVi device by static power gating.
  For static power gating to take place Global Reset, G3 or DeepSx transition must happen.
**/
VOID
PmcStaticDisableCnvi (
  VOID
  )
{
  MmioOr32 (PmcGetPwrmBase () + R_PMC_PWRM_ST_PG_FDIS_PMC_1, B_PMC_PWRM_ST_PG_FDIS_PMC_1_CNVI_FDIS_PMC);
}

/**
  Check if MODPHY SUS PG is supported

  @retval  Status of MODPHY SUS PG support
**/
BOOLEAN
PmcIsModPhySusPgSupported (
  VOID
  )
{
  if (IsPchLp ()) {
    //
    // MPHY SUS PG supported on PCH-LP only:
    //
    return TRUE;
  }
  return FALSE;
}

/**
  Get the PMC SOC configuration.

  @param[out] PmcSocConfig  Configuration of the PMC
**/
VOID
PmcGetSocConfig (
  OUT PMC_SOC_CONFIG  *PmcSocConfig
  )
{
  if (PmcSocConfig == NULL) {
    return;
  }

  ZeroMem (PmcSocConfig, sizeof (PMC_SOC_CONFIG));

  PmcSocConfig->CppmCgInterfaceVersion = 2;
  PmcSocConfig->LpmSupported = TRUE;
  PmcSocConfig->LpmInterfaceVersion = 1;
}

/**
  Check if PMC Extended straps are supported

  @retval  Status of Extended Straps support
**/
BOOLEAN
PmcIsExtendedStrapsSupported (
  VOID
  )
{
  return TRUE;
}

/**
  Check if FIVR is supported.

  @return BOOLEAN specifing whether FIVR is supported in PMC.
**/
BOOLEAN
PmcIsFivrSupported (
  VOID
  )
{
  return TRUE;
}

/**
  Check if PMC IPC1 command for CPU straps is supported.

  @retval  Status of CPU Straps support
**/
BOOLEAN
PmcIsCpuStrapsIpcCommandSupported (
  VOID
  )
{
  return TRUE;
}
