/** @file
  This file contains internal PSF routines for PCH PSF VER2 lib usage

@copyright
  INTEL CONFIDENTIAL
  Copyright 2017 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains an 'Intel Peripheral Driver' and is uniquely identified as
  "Intel Reference Module" and is licensed for Intel CPUs and chipsets under
  the terms of your license agreement with Intel or your vendor. This file may
  be modified by the user, subject to additional terms of the license agreement.

@par Specification Reference:
**/

#include <Base.h>
#include <Library/BaseLib.h>
#include <Uefi/UefiBaseType.h>
#include <Library/IoLib.h>
#include <Library/DebugLib.h>
#include <Library/PchPcrLib.h>
#include <Library/PchInfoLib.h>
#include <Library/SataSocLib.h>
#include <Library/CpuPcieInfoFruLib.h>
#include <Library/BaseMemoryLib.h>
#include <Register/PchRegsPsf.h>
#include <Register/PsfRegsAdpP.h>
#include <Register/PchPcrRegs.h>
#include <Library/PsfLib.h>
#include <Library/PsfSocLib.h>
#include <Library/PciSegmentLib.h>
#include <PchPcieRpInfo.h>
#include <Library/P2SbSidebandAccessLib.h>
#include <PchReservedResources.h>
#include <PchBdfAssignment.h>
#include <Register/PchRegs.h>

GLOBAL_REMOVE_IF_UNREFERENCED P2SB_CONTROLLER P2SbDev = {
  PCI_SEGMENT_LIB_ADDRESS (
           DEFAULT_PCI_SEGMENT_NUMBER_PCH,
           DEFAULT_PCI_BUS_NUMBER_PCH,
           PCI_DEVICE_NUMBER_PCH_P2SB,
           PCI_FUNCTION_NUMBER_PCH_P2SB,
           0
           ),
  PCH_PCR_BASE_ADDRESS
  };

GLOBAL_REMOVE_IF_UNREFERENCED P2SB_SIDEBAND_REGISTER_ACCESS Psf1Access = {
  P2SB_REGISTER_ACCESS_INIT,
  P2SbMmioAccess,
  PID_PSF1,
  0,
  P2SbPrivateConfig,
  FALSE,
  &P2SbDev
  };

GLOBAL_REMOVE_IF_UNREFERENCED P2SB_SIDEBAND_REGISTER_ACCESS Psf2Access = {
  P2SB_REGISTER_ACCESS_INIT,
  P2SbMmioAccess,
  PID_PSF2,
  0,
  P2SbPrivateConfig,
  FALSE,
  &P2SbDev
  };

GLOBAL_REMOVE_IF_UNREFERENCED P2SB_SIDEBAND_REGISTER_ACCESS Psf3Access = {
  P2SB_REGISTER_ACCESS_INIT,
  P2SbMmioAccess,
  PID_PSF3,
  0,
  P2SbPrivateConfig,
  FALSE,
  &P2SbDev
  };

GLOBAL_REMOVE_IF_UNREFERENCED P2SB_SIDEBAND_REGISTER_ACCESS Psf4Access = {
  P2SB_REGISTER_ACCESS_INIT,
  P2SbMmioAccess,
  PID_PSF4,
  0,
  P2SbPrivateConfig,
  FALSE,
  &P2SbDev
  };

GLOBAL_REMOVE_IF_UNREFERENCED P2SB_SIDEBAND_REGISTER_ACCESS Psf5Access = {
  P2SB_REGISTER_ACCESS_INIT,
  P2SbMmioAccess,
  PID_CSME_PSF,
  0,
  P2SbPrivateConfig,
  FALSE,
  &P2SbDev
  };

GLOBAL_REMOVE_IF_UNREFERENCED P2SB_SIDEBAND_REGISTER_ACCESS Psf6Access = {
  P2SB_REGISTER_ACCESS_INIT,
  P2SbMmioAccess,
  PID_PSF6,
  0,
  P2SbPrivateConfig,
  FALSE,
  &P2SbDev
  };

GLOBAL_REMOVE_IF_UNREFERENCED P2SB_SIDEBAND_REGISTER_ACCESS Psf7Access = {
  P2SB_REGISTER_ACCESS_INIT,
  P2SbMmioAccess,
  PID_PSF7,
  0,
  P2SbPrivateConfig,
  FALSE,
  &P2SbDev
  };

GLOBAL_REMOVE_IF_UNREFERENCED P2SB_SIDEBAND_REGISTER_ACCESS Psf8Access = {
  P2SB_REGISTER_ACCESS_INIT,
  P2SbMmioAccess,
  PID_PSF8,
  0,
  P2SbPrivateConfig,
  FALSE,
  &P2SbDev
  };

GLOBAL_REMOVE_IF_UNREFERENCED P2SB_SIDEBAND_REGISTER_ACCESS Psf9Access = {
  P2SB_REGISTER_ACCESS_INIT,
  P2SbMmioAccess,
  PID_PSF9,
  0,
  P2SbPrivateConfig,
  FALSE,
  &P2SbDev
  };

GLOBAL_REMOVE_IF_UNREFERENCED PSF_DEV  Psf1Dev = {
  &Psf1Access.Access,
  FALSE
};

GLOBAL_REMOVE_IF_UNREFERENCED PSF_DEV  Psf2Dev = {
  &Psf2Access.Access,
  FALSE
};

GLOBAL_REMOVE_IF_UNREFERENCED PSF_DEV  Psf3Dev = {
  &Psf3Access.Access,
  FALSE
};

GLOBAL_REMOVE_IF_UNREFERENCED PSF_DEV  Psf4Dev = {
  &Psf4Access.Access,
  FALSE
};

GLOBAL_REMOVE_IF_UNREFERENCED PSF_DEV  Psf5Dev = {
  &Psf5Access.Access,
  TRUE
};

GLOBAL_REMOVE_IF_UNREFERENCED PSF_DEV  Psf6Dev = {
  &Psf6Access.Access,
  FALSE
};

GLOBAL_REMOVE_IF_UNREFERENCED PSF_DEV  Psf7Dev = {
  &Psf7Access.Access,
  FALSE
};

GLOBAL_REMOVE_IF_UNREFERENCED PSF_DEV  Psf8Dev = {
  &Psf8Access.Access,
  FALSE
};

GLOBAL_REMOVE_IF_UNREFERENCED PSF_DEV  Psf9Dev = {
  &Psf8Access.Access,
  FALSE
};

GLOBAL_REMOVE_IF_UNREFERENCED PSF_PORT mPchNSerialIoI2cPsfPort[] =
{
  {&Psf3Dev, R_VER2_PCH_N_PSF3_PCR_T0_SHDW_I2C0_REG_BASE},
  {&Psf3Dev, R_VER2_PCH_N_PSF3_PCR_T0_SHDW_I2C1_REG_BASE},
  {&Psf3Dev, R_VER2_PCH_N_PSF3_PCR_T0_SHDW_I2C2_REG_BASE},
  {&Psf3Dev, R_VER2_PCH_N_PSF3_PCR_T0_SHDW_I2C3_REG_BASE},
  {&Psf3Dev, R_VER2_PCH_N_PSF3_PCR_T0_SHDW_I2C4_REG_BASE},
  {&Psf3Dev, R_VER2_PCH_N_PSF3_PCR_T0_SHDW_I2C5_REG_BASE},
  {&Psf3Dev, R_VER2_PCH_N_PSF3_PCR_T0_SHDW_I2C6_REG_BASE},
  {&Psf3Dev, R_VER2_PCH_N_PSF3_PCR_T0_SHDW_I2C7_REG_BASE}
};

GLOBAL_REMOVE_IF_UNREFERENCED PSF_PORT mPchLpSerialIoI2cPsfPort[] =
{
  {&Psf3Dev, R_VER2_PCH_LP_PSF3_PCR_T0_SHDW_I2C0_REG_BASE},
  {&Psf3Dev, R_VER2_PCH_LP_PSF3_PCR_T0_SHDW_I2C1_REG_BASE},
  {&Psf3Dev, R_VER2_PCH_LP_PSF3_PCR_T0_SHDW_I2C2_REG_BASE},
  {&Psf3Dev, R_VER2_PCH_LP_PSF3_PCR_T0_SHDW_I2C3_REG_BASE},
  {&Psf3Dev, R_VER2_PCH_LP_PSF3_PCR_T0_SHDW_I2C4_REG_BASE},
  {&Psf3Dev, R_VER2_PCH_LP_PSF3_PCR_T0_SHDW_I2C5_REG_BASE},
  {&Psf3Dev, R_VER2_PCH_LP_PSF3_PCR_T0_SHDW_I2C6_REG_BASE},
  {&Psf3Dev, R_VER2_PCH_LP_PSF3_PCR_T0_SHDW_I2C7_REG_BASE}
};

GLOBAL_REMOVE_IF_UNREFERENCED PSF_PORT mPchHSerialIoI2cPsfPort[] =
{
  {&Psf3Dev, R_VER2_PCH_H_PSF3_PCR_T0_SHDW_I2C0_REG_BASE},
  {&Psf3Dev, R_VER2_PCH_H_PSF3_PCR_T0_SHDW_I2C1_REG_BASE},
  {&Psf3Dev, R_VER2_PCH_H_PSF3_PCR_T0_SHDW_I2C2_REG_BASE},
  {&Psf3Dev, R_VER2_PCH_H_PSF3_PCR_T0_SHDW_I2C3_REG_BASE},
  {&Psf3Dev, R_VER2_PCH_H_PSF3_PCR_T0_SHDW_I2C4_REG_BASE},
  {&Psf3Dev, R_VER2_PCH_H_PSF3_PCR_T0_SHDW_I2C5_REG_BASE},
  {&Psf3Dev, R_VER2_PCH_H_PSF3_PCR_T0_SHDW_I2C6_REG_BASE},
  {&Psf3Dev, R_VER2_PCH_H_PSF3_PCR_T0_SHDW_I2C7_REG_BASE}
};

/**
  Return PSF_PORT for SerialIO I2C device

  @param[in] I2cNum  Serial IO I2C device (I2C0, I2C1, ....)

  @retval  PsfPort   PSF PORT structure for SerialIO I2C device
**/
PSF_PORT
PsfSerialIoI2cPort (
  IN UINT32  I2cNum
  )
{
  if (IsPchN ()) {
    if (I2cNum < ARRAY_SIZE (mPchNSerialIoI2cPsfPort)) {
      return mPchNSerialIoI2cPsfPort[I2cNum];
    }
  } else if (IsPchLp ()) {
    if (I2cNum < ARRAY_SIZE (mPchLpSerialIoI2cPsfPort)) {
      return mPchLpSerialIoI2cPsfPort[I2cNum];
    }
  } else {
    if (I2cNum < ARRAY_SIZE (mPchHSerialIoI2cPsfPort)) {
      return mPchHSerialIoI2cPsfPort[I2cNum];
    }
  }

  ASSERT(FALSE);
  return PSF_PORT_NULL;
}

GLOBAL_REMOVE_IF_UNREFERENCED PSF_PORT mPchNSerialIoSpiPsfPort[] =
{
  {&Psf3Dev, R_VER2_PCH_N_PSF3_PCR_T0_SHDW_SPI0_REG_BASE},
  {&Psf3Dev, R_VER2_PCH_N_PSF3_PCR_T0_SHDW_SPI1_REG_BASE},
  {&Psf3Dev, R_VER2_PCH_N_PSF3_PCR_T0_SHDW_SPI2_REG_BASE},
  {&Psf3Dev, R_VER2_PCH_N_PSF3_PCR_T0_SHDW_SPI3_REG_BASE},
  {&Psf3Dev, R_VER2_PCH_N_PSF3_PCR_T0_SHDW_SPI4_REG_BASE},
  {&Psf3Dev, R_VER2_PCH_N_PSF3_PCR_T0_SHDW_SPI5_REG_BASE},
  {&Psf3Dev, R_VER2_PCH_N_PSF3_PCR_T0_SHDW_SPI6_REG_BASE}
};

GLOBAL_REMOVE_IF_UNREFERENCED PSF_PORT mPchLpSerialIoSpiPsfPort[] =
{
  {&Psf3Dev, R_VER2_PCH_LP_PSF3_PCR_T0_SHDW_SPI0_REG_BASE},
  {&Psf3Dev, R_VER2_PCH_LP_PSF3_PCR_T0_SHDW_SPI1_REG_BASE},
  {&Psf3Dev, R_VER2_PCH_LP_PSF3_PCR_T0_SHDW_SPI2_REG_BASE},
  {&Psf3Dev, R_VER2_PCH_LP_PSF3_PCR_T0_SHDW_SPI3_REG_BASE},
  {&Psf3Dev, R_VER2_PCH_LP_PSF3_PCR_T0_SHDW_SPI4_REG_BASE},
  {&Psf3Dev, R_VER2_PCH_LP_PSF3_PCR_T0_SHDW_SPI5_REG_BASE},
  {&Psf3Dev, R_VER2_PCH_LP_PSF3_PCR_T0_SHDW_SPI6_REG_BASE}
};

GLOBAL_REMOVE_IF_UNREFERENCED PSF_PORT mPchHSerialIoSpiPsfPort[] =
{
  {&Psf3Dev, R_VER2_PCH_H_PSF3_PCR_T0_SHDW_SPI0_REG_BASE},
  {&Psf3Dev, R_VER2_PCH_H_PSF3_PCR_T0_SHDW_SPI1_REG_BASE},
  {&Psf3Dev, R_VER2_PCH_H_PSF3_PCR_T0_SHDW_SPI2_REG_BASE},
  {&Psf3Dev, R_VER2_PCH_H_PSF3_PCR_T0_SHDW_SPI3_REG_BASE},
  {&Psf3Dev, R_VER2_PCH_H_PSF3_PCR_T0_SHDW_SPI4_REG_BASE},
  {&Psf3Dev, R_VER2_PCH_H_PSF3_PCR_T0_SHDW_SPI5_REG_BASE},
  {&Psf3Dev, R_VER2_PCH_H_PSF3_PCR_T0_SHDW_SPI6_REG_BASE}
};

/**
  Return PSF_PORT for SerialIO SPI device

  @param[in] I2cNum  Serial IO SPI device (SPI0, SPI1, ....)

  @retval  PsfPort   PSF PORT structure for SerialIO SPI device
**/
PSF_PORT
PsfSerialIoSpiPort (
  IN UINT32  SpiNum
  )
{
  if (IsPchN ()) {
    if (SpiNum < ARRAY_SIZE (mPchNSerialIoSpiPsfPort)) {
      return mPchNSerialIoSpiPsfPort[SpiNum];
    }
  } else if (IsPchLp ()) {
    if (SpiNum < ARRAY_SIZE (mPchLpSerialIoSpiPsfPort)) {
      return mPchLpSerialIoSpiPsfPort[SpiNum];
    }
  } else {
    if (SpiNum < ARRAY_SIZE (mPchHSerialIoSpiPsfPort)) {
      return mPchHSerialIoSpiPsfPort[SpiNum];
    }
  }

  ASSERT(FALSE);
  return PSF_PORT_NULL;
}

GLOBAL_REMOVE_IF_UNREFERENCED PSF_PORT mPchNSerialIoUartPsfPort[] =
{
  {&Psf3Dev, R_VER2_PCH_N_PSF3_PCR_T0_SHDW_UART0_REG_BASE},
  {&Psf3Dev, R_VER2_PCH_N_PSF3_PCR_T0_SHDW_UART1_REG_BASE},
  {&Psf3Dev, R_VER2_PCH_N_PSF3_PCR_T0_SHDW_UART2_REG_BASE},
  {&Psf3Dev, R_VER2_PCH_N_PSF3_PCR_T0_SHDW_UART3_REG_BASE},
  {&Psf3Dev, R_VER2_PCH_N_PSF3_PCR_T0_SHDW_UART4_REG_BASE},
  {&Psf3Dev, R_VER2_PCH_N_PSF3_PCR_T0_SHDW_UART5_REG_BASE},
  {&Psf3Dev, R_VER2_PCH_N_PSF3_PCR_T0_SHDW_UART6_REG_BASE}
};

GLOBAL_REMOVE_IF_UNREFERENCED PSF_PORT mPchLpSerialIoUartPsfPort[] =
{
  {&Psf3Dev, R_VER2_PCH_LP_PSF3_PCR_T0_SHDW_UART0_REG_BASE},
  {&Psf3Dev, R_VER2_PCH_LP_PSF3_PCR_T0_SHDW_UART1_REG_BASE},
  {&Psf3Dev, R_VER2_PCH_LP_PSF3_PCR_T0_SHDW_UART2_REG_BASE},
  {&Psf3Dev, R_VER2_PCH_LP_PSF3_PCR_T0_SHDW_UART3_REG_BASE},
  {&Psf3Dev, R_VER2_PCH_LP_PSF3_PCR_T0_SHDW_UART4_REG_BASE},
  {&Psf3Dev, R_VER2_PCH_LP_PSF3_PCR_T0_SHDW_UART5_REG_BASE},
  {&Psf3Dev, R_VER2_PCH_LP_PSF3_PCR_T0_SHDW_UART6_REG_BASE}
};

GLOBAL_REMOVE_IF_UNREFERENCED PSF_PORT mPchHSerialIoUartPsfPort[] =
{
  {&Psf3Dev, R_VER2_PCH_H_PSF3_PCR_T0_SHDW_UART0_REG_BASE},
  {&Psf3Dev, R_VER2_PCH_H_PSF3_PCR_T0_SHDW_UART1_REG_BASE},
  {&Psf3Dev, R_VER2_PCH_H_PSF3_PCR_T0_SHDW_UART2_REG_BASE},
  {&Psf3Dev, R_VER2_PCH_H_PSF3_PCR_T0_SHDW_UART3_REG_BASE},
  {&Psf3Dev, R_VER2_PCH_H_PSF3_PCR_T0_SHDW_UART4_REG_BASE},
  {&Psf3Dev, R_VER2_PCH_H_PSF3_PCR_T0_SHDW_UART5_REG_BASE},
  {&Psf3Dev, R_VER2_PCH_H_PSF3_PCR_T0_SHDW_UART6_REG_BASE}
};

/**
  Return PSF_PORT for SerialIO UART device

  @param[in] UartNum  Serial IO UART device (UART0, UART1, ....)

  @retval  PsfPort   PSF PORT structure for SerialIO UART device
**/
PSF_PORT
PsfSerialIoUartPort (
  IN UINT32  UartNum
  )
{
  if (IsPchN ()) {
    if (UartNum < ARRAY_SIZE (mPchNSerialIoUartPsfPort)) {
      return mPchNSerialIoUartPsfPort[UartNum];
    }
  } else if (IsPchLp ()) {
    if (UartNum < ARRAY_SIZE (mPchLpSerialIoUartPsfPort)) {
      return mPchLpSerialIoUartPsfPort[UartNum];
    }
  } else {
    if (UartNum < ARRAY_SIZE (mPchHSerialIoUartPsfPort)) {
      return mPchHSerialIoUartPsfPort[UartNum];
    }
  }

  ASSERT(FALSE);
  return PSF_PORT_NULL;
}

/**
  Return PSF_PORT for TraceHub device

  @retval    PsfPort         PSF PORT structure for TraceHub device
**/
PSF_PORT
PsfTraceHubPort (
  VOID
  )
{
  PSF_PORT PsfPort;

  PsfPort.PsfDev = &Psf1Dev;
  if (IsPchLp ()) {
    PsfPort.RegBase = R_VER2_PCH_LP_PSF1_PCR_T0_SHDW_TRACEHUB_REG_BASE;
  } else {
    PsfPort.RegBase = R_VER2_PCH_H_PSF1_PCR_T0_SHDW_TRACEHUB_REG_BASE;
  }
  return PsfPort;
}

/**
  Return PSF_PORT for TraceHub ACPI device

  @retval    PsfPort         PSF PORT structure for TraceHub ACPI device
**/
PSF_PORT
PsfTraceHubAcpiDevPort (
  VOID
  )
{
  PSF_PORT PsfPort;

  PsfPort.PsfDev = &Psf1Dev;
  if (IsPchLp ()) {
    PsfPort.RegBase = R_VER2_PCH_LP_PSF1_PCR_T0_SHDW_NPK_PHANTOM_REG_BASE;
  } else {
    PsfPort.RegBase = R_VER2_PCH_H_PSF1_PCR_T0_SHDW_NPK_PHANTOM_REG_BASE;
  }
  return PsfPort;
}

GLOBAL_REMOVE_IF_UNREFERENCED UINT16 mPchLpHeciPsfRegBase[] =
{
  R_VER2_PCH_LP_PSF1_PCR_T0_SHDW_HECI1_REG_BASE,
  R_VER2_PCH_LP_PSF1_PCR_T0_SHDW_HECI2_REG_BASE,
  R_VER2_PCH_LP_PSF1_PCR_T0_SHDW_HECI3_REG_BASE,
  R_VER2_PCH_LP_PSF1_PCR_T0_SHDW_HECI4_REG_BASE
};

GLOBAL_REMOVE_IF_UNREFERENCED UINT16 mPchHHeciPsfRegBase[] =
{
  R_VER2_PCH_H_PSF1_PCR_T0_SHDW_HECI1_REG_BASE,
  R_VER2_PCH_H_PSF1_PCR_T0_SHDW_HECI2_REG_BASE,
  R_VER2_PCH_H_PSF1_PCR_T0_SHDW_HECI3_REG_BASE,
  R_VER2_PCH_H_PSF1_PCR_T0_SHDW_HECI4_REG_BASE
};

/**
  Return PSF_PORT for HECI device

  @param[in] HeciDevice      HECIx Device (HECI1-4)

  @retval    PsfPort         PSF PORT structure for HECI device
**/
PSF_PORT
PsfHeciPort (
  IN UINT8      HeciDevice
  )
{
  PSF_PORT PsfPort;

  if ((HeciDevice < 1) || (HeciDevice > 4)) {
    ASSERT (FALSE);
    return PSF_PORT_NULL;
  }

  PsfPort.PsfDev = &Psf1Dev;

  if (IsPchLp ()) {
    PsfPort.RegBase = mPchLpHeciPsfRegBase[HeciDevice - 1];
  } else {
    PsfPort.RegBase = mPchHHeciPsfRegBase[HeciDevice - 1];
  }
  return PsfPort;
}

/**
  Return PSF_PORT for SOL device

  @retval    PsfPort         PSF PORT structure for SOL device
**/
PSF_PORT
PsfSolPort (
  VOID
  )
{
  PSF_PORT PsfPort;

  PsfPort.PsfDev = &Psf1Dev;
  if (IsPchLp ()) {
    PsfPort.RegBase = R_VER2_PCH_LP_PSF1_PCR_T0_SHDW_KT_REG_BASE;
  } else {
    PsfPort.RegBase = R_VER2_PCH_H_PSF1_PCR_T0_SHDW_KT_REG_BASE;
  }
  return PsfPort;
}

/**
  Disable IDER device at PSF level
**/
VOID
PsfDisableIderDevice (
  VOID
  )
{
  PSF_PORT PsfPort;

  PsfPort.PsfDev = &Psf1Dev;
  if (IsPchLp ()) {
    PsfPort.RegBase = R_VER2_PCH_LP_PSF1_PCR_T0_SHDW_IDER_REG_BASE;
  } else {
    PsfPort.RegBase = R_VER2_PCH_H_PSF1_PCR_T0_SHDW_IDER_REG_BASE;
  }
  PsfDisableDevice (PsfPort);
}

/**
  Disable HDAudio device at PSF level
**/
VOID
PsfDisableHdaDevice (
  VOID
  )
{
  PSF_PORT PsfPort;

  if (IsPchN ()) {
    PsfPort.PsfDev = &Psf3Dev;
    PsfPort.RegBase = R_VER2_PCH_N_PSF3_PCR_T0_SHDW_AUD_REG_BASE;
  } else if (IsPchLp ()) {
    PsfPort.PsfDev = &Psf3Dev;
    PsfPort.RegBase = R_VER2_PCH_LP_PSF3_PCR_T0_SHDW_AUD_REG_BASE;
  } else {
    PsfPort.PsfDev = &Psf2Dev;
    PsfPort.RegBase = R_VER2_PCH_H_PSF2_PCR_T0_SHDW_AUD_REG_BASE;
  }
  PsfDisableDevice (PsfPort);
}

/**
  Disable DSP Bar (HdAudio BAR4 and BAR5) at PSF level.
**/
VOID
PsfDisableDspBar (
  VOID
  )
{
  PSF_PORT PsfPort;

  if (IsPchN ()) {
    PsfPort.PsfDev = &Psf3Dev;
    PsfPort.RegBase = R_VER2_PCH_N_PSF3_PCR_T0_SHDW_AUD_REG_BASE;
  } else if (IsPchLp ()) {
    PsfPort.PsfDev = &Psf3Dev;
    PsfPort.RegBase = R_VER2_PCH_LP_PSF3_PCR_T0_SHDW_AUD_REG_BASE;
  } else {
    PsfPort.PsfDev = &Psf2Dev;
    PsfPort.RegBase = R_VER2_PCH_H_PSF2_PCR_T0_SHDW_AUD_REG_BASE;
  }

  PsfDisableDeviceBar (PsfPort, (R_PCH_PSFX_PCR_T0_SHDW_BAR5 | R_PCH_PSFX_PCR_T0_SHDW_BAR4));
}

GLOBAL_REMOVE_IF_UNREFERENCED UINT16 mPchLpThcPsfRegBase[] =
{
  R_VER2_PCH_LP_PSF2_PCR_T0_SHDW_THC0_REG_BASE,
  R_VER2_PCH_LP_PSF2_PCR_T0_SHDW_THC1_REG_BASE
};

GLOBAL_REMOVE_IF_UNREFERENCED UINT16 mPchHThcPsfRegBase[] =
{
  R_VER2_PCH_H_PSF3_PCR_T0_SHDW_THC0_REG_BASE,
  R_VER2_PCH_H_PSF3_PCR_T0_SHDW_THC1_REG_BASE
};

/**
  Disable THC device at PSF level

  @param[in]  ThcNumber                Touch Host Controller Number THC0 or THC1
**/
VOID
PsfDisableThcDevice (
  IN  UINT32        ThcNumber
  )
{
  PSF_PORT PsfPort;

  if ((IsPchLp () && ThcNumber >= ARRAY_SIZE (mPchLpThcPsfRegBase)) ||
    (IsPchH () && ThcNumber >= ARRAY_SIZE (mPchHThcPsfRegBase))) {
    DEBUG ((DEBUG_ERROR, "Wrong THC number!\n"));
    ASSERT (FALSE);
    return;
  }
  if (IsPchLp ()) {
    PsfPort.PsfDev  = &Psf2Dev;
    PsfPort.RegBase = mPchLpThcPsfRegBase[ThcNumber];
  } else {
    PsfPort.PsfDev = &Psf3Dev;
    PsfPort.RegBase = mPchHThcPsfRegBase[ThcNumber];
  }

  PsfDisableDevice (PsfPort);
}

/**
  Disable xDCI device at PSF level
**/
VOID
PsfDisableXdciDevice (
  VOID
  )
{
  PSF_PORT PsfPort;

  if (IsPchLp ()) {
    PsfPort.PsfDev = &Psf2Dev;
    PsfPort.RegBase = R_VER2_PCH_LP_PSF2_PCR_T0_SHDW_XDCI_REG_BASE;
  } else {
    PsfPort.PsfDev = &Psf2Dev;
    PsfPort.RegBase = R_VER2_PCH_H_PSF2_PCR_T0_SHDW_XDCI_REG_BASE;
  }
  PsfDisableDevice (PsfPort);
}

/**
  Disable xHCI device at PSF level
**/
VOID
PsfDisableXhciDevice (
  VOID
  )
{
  PSF_PORT PsfPort;

  PsfPort.PsfDev = &Psf1Dev;
  if (IsPchLp ()) {
    PsfPort.RegBase = R_VER2_PCH_LP_PSF1_PCR_T0_SHDW_XHCI_REG_BASE;
  } else {
    PsfPort.RegBase = R_VER2_PCH_H_PSF1_PCR_T0_SHDW_XHCI_REG_BASE;
  }
  PsfDisableDevice (PsfPort);
}

/**
  Disable xHCI VTIO Phantom device at PSF level
**/
VOID
PsfDisableXhciVtioDevice (
  VOID
  )
{
  // Not applicable for ADL platform
}

/**
  Disable SATA device at PSF level

  @param[in]  SataCtrlIndex     SATA controller index
**/
VOID
PsfDisableSataDevice (
  IN UINT32     SataCtrlIndex
  )
{
  PSF_PORT PsfPort;

  ASSERT (SataCtrlIndex < 1);

  PsfPort.PsfDev = &Psf1Dev;
  if (IsPchLp ()) {
    PsfPort.RegBase = R_VER2_PCH_LP_PSF1_PCR_T0_SHDW_SATA_REG_BASE;
  } else {
    PsfPort.RegBase = R_VER2_PCH_H_PSF1_PCR_T0_SHDW_SATA_REG_BASE;
  }
  PsfDisableDevice (PsfPort);
}

/**
  Return PSF_PORT for ISH device

  @retval    PsfPort         PSF PORT structure for ISH device
**/
PSF_PORT
PsfIshPort (
  VOID
  )
{
  PSF_PORT PsfPort;

  PsfPort.PsfDev = &Psf3Dev;
  if (IsPchN()) {
    PsfPort.RegBase = R_VER2_PCH_N_PSF3_PCR_T0_SHDW_ISH_REG_BASE;
  } else if (IsPchLp ()) {
    PsfPort.RegBase = R_VER2_PCH_LP_PSF3_PCR_T0_SHDW_ISH_REG_BASE;
  } else {
    PsfPort.RegBase = R_VER2_PCH_H_PSF3_PCR_T0_SHDW_ISH_REG_BASE;
  }
  return PsfPort;
}

/**
  Disable FPAK device at PSF level
**/
VOID
PsfDisableFpakDevice (
  VOID
  )
{
  if (!IsPchH ()) {
    PsfDisableDevice (PsfFpakPort ());
  }
}

/**
  Return PSF_PORT for FPAK device

  @retval    PsfPort         PSF PORT structure for FPAK device
**/
PSF_PORT
PsfFpakPort (
  VOID
  )
{
  PSF_PORT PsfPort;

  PsfPort.PsfDev = &Psf1Dev;
  PsfPort.RegBase = 0;
  if (IsPchLp ()) {
    PsfPort.RegBase = R_VER2_PCH_LP_PSF1_PCR_T0_SHDW_FPAK_REG_BASE;
  }
  return PsfPort;
}

/**
  Disable GbE device at PSF level
**/
VOID
PsfDisableGbeDevice (
  VOID
  )
{
  PSF_PORT PsfPort;

  if (IsPchLp ()) {
    PsfPort.PsfDev = &Psf1Dev;
    PsfPort.RegBase = R_VER2_PCH_LP_PSF1_PCR_T0_SHDW_GBE_REG_BASE;
  } else {
    PsfPort.PsfDev = &Psf3Dev;
    PsfPort.RegBase = R_VER2_PCH_H_PSF3_PCR_T0_SHDW_GBE_REG_BASE;
  }

  PsfDisableDevice (PsfPort);
}

/**
  Disable SMBUS device at PSF level
**/
VOID
PsfDisableSmbusDevice (
  VOID
  )
{
  PSF_PORT PsfPort;

  PsfPort.PsfDev = &Psf3Dev;
  if (IsPchN()) {
    PsfPort.RegBase = R_VER2_PCH_N_PSF3_PCR_T0_SHDW_SMBUS_REG_BASE;
  } else if (IsPchLp ()) {
    PsfPort.RegBase = R_VER2_PCH_LP_PSF3_PCR_T0_SHDW_SMBUS_REG_BASE;
  } else {
    PsfPort.RegBase = R_VER2_PCH_H_PSF3_PCR_T0_SHDW_SMBUS_REG_BASE;
  }

  PsfDisableDevice (PsfPort);
}

GLOBAL_REMOVE_IF_UNREFERENCED PSF_PORT mPchLpScsUfsPsfPort[] =
{
  {&Psf2Dev, R_VER2_PCH_LP_PSF2_PCR_T0_SHDW_UFS1_REG_BASE},
  {&Psf2Dev, R_VER2_PCH_LP_PSF2_PCR_T0_SHDW_UFS2_REG_BASE}
};

GLOBAL_REMOVE_IF_UNREFERENCED PSF_PORT mPchHScsUfsPsfPort[] =
{
  {&Psf2Dev, R_VER2_PCH_H_PSF2_PCR_T0_SHDW_UFS1_REG_BASE},
  {&Psf2Dev, R_VER2_PCH_H_PSF2_PCR_T0_SHDW_UFS2_REG_BASE}
};

/**
  Return PSF_PORT for SCS UFS device

  @param[in] UfsNum       UFS Device

  @retval    PsfPort      PSF PORT structure for SCS UFS device
**/
PSF_PORT
PsfScsUfsPort (
  IN UINT32  UfsNum
  )
{
  if (IsPchLp ()) {
    if (UfsNum < ARRAY_SIZE (mPchLpScsUfsPsfPort)) {
      return mPchLpScsUfsPsfPort[UfsNum];
    }
  } else {
    if (UfsNum < ARRAY_SIZE (mPchHScsUfsPsfPort)) {
      return mPchHScsUfsPsfPort[UfsNum];
    }
  }

  ASSERT(FALSE);
  return PSF_PORT_NULL;
}

/**
  Return PSF_PORT for SCS EMMC device
  @retval    PsfPort      PSF PORT structure for SCS EMMC device
**/
PSF_PORT
PsfScsEmmcPort (
  VOID
  )
{
  PSF_PORT PsfPort;
  if (IsPchN ()) {
    PsfPort.PsfDev = &Psf3Dev;
    PsfPort.RegBase = R_VER2_PCH_N_PSF3_PCR_T0_SHDW_EMMC_REG_BASE;
    return PsfPort;
  }
  ASSERT(FALSE);
  return PSF_PORT_NULL;
}

//
// RS0 PSF1_T1_SHDW_PCIExx_REG_BASE registers for PCH-LP
//
GLOBAL_REMOVE_IF_UNREFERENCED PSF_REG_BASE mPchPsfPcieRegBase[] =
{
  { 1, R_VER2_PCH_LP_PSF1_PCR_T1_SHDW_PCIE01_RS0_REG_BASE },
  { 1, R_VER2_PCH_LP_PSF1_PCR_T1_SHDW_PCIE02_RS0_REG_BASE },
  { 1, R_VER2_PCH_LP_PSF1_PCR_T1_SHDW_PCIE03_RS0_REG_BASE },
  { 1, R_VER2_PCH_LP_PSF1_PCR_T1_SHDW_PCIE04_RS0_REG_BASE },
  { 1, R_VER2_PCH_LP_PSF1_PCR_T1_SHDW_PCIE05_RS0_REG_BASE },
  { 1, R_VER2_PCH_LP_PSF1_PCR_T1_SHDW_PCIE06_RS0_REG_BASE },
  { 1, R_VER2_PCH_LP_PSF1_PCR_T1_SHDW_PCIE07_RS0_REG_BASE },
  { 1, R_VER2_PCH_LP_PSF1_PCR_T1_SHDW_PCIE08_RS0_REG_BASE },
  { 1, R_VER2_PCH_LP_PSF1_PCR_T1_SHDW_PCIE09_RS0_REG_BASE },
  { 1, R_VER2_PCH_LP_PSF1_PCR_T1_SHDW_PCIE10_RS0_REG_BASE },
  { 1, R_VER2_PCH_LP_PSF1_PCR_T1_SHDW_PCIE11_RS0_REG_BASE },
  { 1, R_VER2_PCH_LP_PSF1_PCR_T1_SHDW_PCIE12_RS0_REG_BASE }
};

/**
  Return PSF_REG_BASE table at root PSF level to which PCIe Root Port device is connected

  @retval    PsfRegBase        PSF REG BASE structure table for PCIe
**/
PSF_REG_BASE*
PsfGetRootPciePortTable (
  VOID
  )
{
  return mPchPsfPcieRegBase;
}

PSF_PCIE_PORT_DATA_TABLE mPsfPciePortData = PSF_PCIE_PORT_DATA_TABLE_INIT (
  { 1, 0, R_VER2_PCH_LP_PSF1_PCR_T1_SHDW_PCIE01_RS0_REG_BASE, R_VER2_PCH_LP_PSF1_PCR_T0_SHDW_PCIE01_RS3_REG_BASE, 0, R_VER2_PCH_LP_PSF1_PCR_T1_AGENT_FUNCTION_CONFIG_SPA_RS0_D28_F0, R_VER2_PCH_LP_PSF1_PCR_T0_AGENT_FUNCTION_CONFIG_SPA_RS3_D28_F0 },
  { 1, 0, R_VER2_PCH_LP_PSF1_PCR_T1_SHDW_PCIE02_RS0_REG_BASE, R_VER2_PCH_LP_PSF1_PCR_T0_SHDW_PCIE02_RS3_REG_BASE, 0, R_VER2_PCH_LP_PSF1_PCR_T1_AGENT_FUNCTION_CONFIG_SPA_RS0_D28_F1, R_VER2_PCH_LP_PSF1_PCR_T0_AGENT_FUNCTION_CONFIG_SPA_RS3_D28_F1 },
  { 1, 0, R_VER2_PCH_LP_PSF1_PCR_T1_SHDW_PCIE03_RS0_REG_BASE, R_VER2_PCH_LP_PSF1_PCR_T0_SHDW_PCIE03_RS3_REG_BASE, 0, R_VER2_PCH_LP_PSF1_PCR_T1_AGENT_FUNCTION_CONFIG_SPA_RS0_D28_F2, R_VER2_PCH_LP_PSF1_PCR_T0_AGENT_FUNCTION_CONFIG_SPA_RS3_D28_F2 },
  { 1, 0, R_VER2_PCH_LP_PSF1_PCR_T1_SHDW_PCIE04_RS0_REG_BASE, R_VER2_PCH_LP_PSF1_PCR_T0_SHDW_PCIE04_RS3_REG_BASE, 0, R_VER2_PCH_LP_PSF1_PCR_T1_AGENT_FUNCTION_CONFIG_SPA_RS0_D28_F3, R_VER2_PCH_LP_PSF1_PCR_T0_AGENT_FUNCTION_CONFIG_SPA_RS3_D28_F3 },
  { 1, 0, R_VER2_PCH_LP_PSF1_PCR_T1_SHDW_PCIE05_RS0_REG_BASE, R_VER2_PCH_LP_PSF1_PCR_T0_SHDW_PCIE05_RS3_REG_BASE, 0, R_VER2_PCH_LP_PSF1_PCR_T1_AGENT_FUNCTION_CONFIG_SPB_RS0_D28_F4, R_VER2_PCH_LP_PSF1_PCR_T0_AGENT_FUNCTION_CONFIG_SPB_RS3_D28_F4 },
  { 1, 0, R_VER2_PCH_LP_PSF1_PCR_T1_SHDW_PCIE06_RS0_REG_BASE, R_VER2_PCH_LP_PSF1_PCR_T0_SHDW_PCIE06_RS3_REG_BASE, 0, R_VER2_PCH_LP_PSF1_PCR_T1_AGENT_FUNCTION_CONFIG_SPB_RS0_D28_F5, R_VER2_PCH_LP_PSF1_PCR_T0_AGENT_FUNCTION_CONFIG_SPB_RS3_D28_F5 },
  { 1, 0, R_VER2_PCH_LP_PSF1_PCR_T1_SHDW_PCIE07_RS0_REG_BASE, R_VER2_PCH_LP_PSF1_PCR_T0_SHDW_PCIE07_RS3_REG_BASE, 0, R_VER2_PCH_LP_PSF1_PCR_T1_AGENT_FUNCTION_CONFIG_SPB_RS0_D28_F6, R_VER2_PCH_LP_PSF1_PCR_T0_AGENT_FUNCTION_CONFIG_SPB_RS3_D28_F6 },
  { 1, 0, R_VER2_PCH_LP_PSF1_PCR_T1_SHDW_PCIE08_RS0_REG_BASE, R_VER2_PCH_LP_PSF1_PCR_T0_SHDW_PCIE08_RS3_REG_BASE, 0, R_VER2_PCH_LP_PSF1_PCR_T1_AGENT_FUNCTION_CONFIG_SPB_RS0_D28_F7, R_VER2_PCH_LP_PSF1_PCR_T0_AGENT_FUNCTION_CONFIG_SPB_RS3_D28_F7 },
  { 1, 0, R_VER2_PCH_LP_PSF1_PCR_T1_SHDW_PCIE09_RS0_REG_BASE, R_VER2_PCH_LP_PSF1_PCR_T0_SHDW_PCIE09_RS3_REG_BASE, 0, R_VER2_PCH_LP_PSF1_PCR_T1_AGENT_FUNCTION_CONFIG_SPC_RS0_D29_F0, R_VER2_PCH_LP_PSF1_PCR_T0_AGENT_FUNCTION_CONFIG_SPC_RS3_D29_F0 },
  { 1, 0, R_VER2_PCH_LP_PSF1_PCR_T1_SHDW_PCIE10_RS0_REG_BASE, R_VER2_PCH_LP_PSF1_PCR_T0_SHDW_PCIE10_RS3_REG_BASE, 0, R_VER2_PCH_LP_PSF1_PCR_T1_AGENT_FUNCTION_CONFIG_SPC_RS0_D29_F1, R_VER2_PCH_LP_PSF1_PCR_T0_AGENT_FUNCTION_CONFIG_SPC_RS3_D29_F1 },
  { 1, 0, R_VER2_PCH_LP_PSF1_PCR_T1_SHDW_PCIE11_RS0_REG_BASE, R_VER2_PCH_LP_PSF1_PCR_T0_SHDW_PCIE11_RS3_REG_BASE, 0, R_VER2_PCH_LP_PSF1_PCR_T1_AGENT_FUNCTION_CONFIG_SPC_RS0_D29_F2, R_VER2_PCH_LP_PSF1_PCR_T0_AGENT_FUNCTION_CONFIG_SPC_RS3_D29_F2 },
  { 1, 0, R_VER2_PCH_LP_PSF1_PCR_T1_SHDW_PCIE12_RS0_REG_BASE, R_VER2_PCH_LP_PSF1_PCR_T0_SHDW_PCIE12_RS3_REG_BASE, 0, R_VER2_PCH_LP_PSF1_PCR_T1_AGENT_FUNCTION_CONFIG_SPC_RS0_D29_F3, R_VER2_PCH_LP_PSF1_PCR_T0_AGENT_FUNCTION_CONFIG_SPC_RS3_D29_F3 }
);

/**
  Get PSF PCIe Root Port Data

  @param[in]  RpIndex         PCIe Root Port Index (0 based)
  @retval     PciePortData    PCIE PORT Data
**/
PSF_PCIE_PORT_DATA*
PsfGetPciePortData (
  IN UINT32  RpIndex
  )
{
  if (RpIndex < mPsfPciePortData.Size) {
    return &mPsfPciePortData.Data[RpIndex];
  }
  return NULL;
}

/**
  Get PSF PCIe Root Port Data Table
**/
PSF_PCIE_PORT_DATA_TABLE*
PsfGetPciePortDataTable (
  VOID
  )
{
  return &mPsfPciePortData;
}

PSF_GRANT_COUNT_NUMBER mGrantCountNumSpa = PSF_GRANT_COUNT_NUMBER_INIT ({ 12,  0 }, { 13,  1 }, { 14,  2 }, { 15,  3 });
PSF_GRANT_COUNT_NUMBER mGrantCountNumSpb = PSF_GRANT_COUNT_NUMBER_INIT ({ 16,  5 }, { 17,  6 }, { 18,  7 }, { 19,  8 });
PSF_GRANT_COUNT_NUMBER mGrantCountNumSpc = PSF_GRANT_COUNT_NUMBER_INIT ({ 20, 10 }, { 21, 11 }, { 22, 12 }, { 23, 13 });

GLOBAL_REMOVE_IF_UNREFERENCED PSF_GRANT_COUNT_REG_DATA mPchGntCntRegs[] = {
  // { DevGntCnt0Base, TargetGntCntPg1Tgt0Base, GrantCountNum[PCH_PCIE_CONTROLLER_PORTS] }
  { R_VER2_PCH_LP_PSF1_PCR_DEV_GNTCNT_RELOAD_DGCR0, R_VER2_PCH_LP_PSF1_PCR_TARGET_GNTCNT_RELOAD_PG1_TGT0, &mGrantCountNumSpa },
  { R_VER2_PCH_LP_PSF1_PCR_DEV_GNTCNT_RELOAD_DGCR0, R_VER2_PCH_LP_PSF1_PCR_TARGET_GNTCNT_RELOAD_PG1_TGT0, &mGrantCountNumSpb },
  { R_VER2_PCH_LP_PSF1_PCR_DEV_GNTCNT_RELOAD_DGCR0, R_VER2_PCH_LP_PSF1_PCR_TARGET_GNTCNT_RELOAD_PG1_TGT0, &mGrantCountNumSpc }
};

//
// PCH-LP PSF topology for PCIe
//
GLOBAL_REMOVE_IF_UNREFERENCED CONST PSF_TOPOLOGY mPchPsf1PcieTreeTopo[] = {
  //{PsfId, Port}, PortType, Child, GrantCountData, PortData
  {{1, 0}, PsfPcieCtrlPort, NULL, &mPchGntCntRegs[0], {0}}, // SPA
  {{1, 1}, PsfPcieCtrlPort, NULL, &mPchGntCntRegs[1], {1}}, // SPB
  {{1, 2}, PsfPcieCtrlPort, NULL, &mPchGntCntRegs[2], {2}}, // SPC
  PSF_TOPOLOGY_END
};

GLOBAL_REMOVE_IF_UNREFERENCED CONST PSF_TOPOLOGY mPchPsfPcieTreeTopo[] = {
  {{0, 0}, PsfToPsfPort, mPchPsf1PcieTreeTopo}, // Abstract root of PSF topology
  PSF_TOPOLOGY_END
};

/**
  Get PSF Pcie Tree topology

  @param[in] PsfTopology          PSF Port from PSF PCIe tree topology

  @retval PsfTopology             PSF PCIe tree topology
**/
CONST PSF_TOPOLOGY*
PsfGetRootPciePsfTopology (
  VOID
  )
{
  return mPchPsfPcieTreeTopo;
}

PSF_REG_DATA_TABLE mPchEoiRegDataTable = PSF_REG_DATA_TABLE_INIT (
  { 1, R_VER2_PCH_LP_PSF1_PCR_MC_AGENT_MCAST0_TGT0_EOI, R_VER2_PCH_LP_PSF1_PCR_MC_CONTROL_MCAST0_EOI, 13 },
  { 3, R_VER2_PCH_LP_PSF3_PCR_MC_AGENT_MCAST0_TGT0_EOI, R_VER2_PCH_LP_PSF3_PCR_MC_CONTROL_MCAST0_EOI, 1 }
);

/**
  Get EOI register data table for all PSFs

  @return PsfEoiRegDataTable   Pointer to PSF_EOI_REG_DATA_TABLE
**/
PSF_REG_DATA_TABLE*
PsfGetEoiRegDataTable (
  VOID
  )
{
  return &mPchEoiRegDataTable;
}

PSF_REG_DATA_TABLE mPchMctpRegDataTable = PSF_REG_DATA_TABLE_INIT (
  { 1, R_VER2_PCH_LP_PSF1_PCR_MC_AGENT_MCAST1_RS0_TGT0_MCTP1, R_VER2_PCH_LP_PSF1_PCR_MC_CONTROL_MCAST1_RS0_MCTP1, 13, 0 }
);

/**
  Get MCTP register data table for all PSFs

  @return PsfEoiRegDataTable   Pointer to PSF_EOI_REG_DATA_TABLE
**/
PSF_REG_DATA_TABLE*
PsfGetMctpRegDataTable (
  VOID
  )
{
  return &mPchMctpRegDataTable;
}

/**
  Fill MCTP Targets Table

  @param[out] TargetIdTable    MCTP Targets table
  @param[in]  MaxTableSize     TargetIdTable real size
  @param[in]  PsfTable         PSF Segment Table
  @param[in]  PcieRegBaseTable Pcie Reg Base Table

  @retval Number of targets, resulting size of the table
**/
UINT32
MctpTargetsTable (
  OUT PSF_PORT_DEST_ID  *TargetIdTable,
  IN  UINT32            MaxTableSize,
  IN  PSF_SEGMENT_TABLE *PsfTable,
  IN  PSF_REG_BASE      *PcieRegBaseTable
  )
{
  UINT32       TargetNum;
  UINT32       RpIndex;
  PSF_PORT     PciePsfPort;

  if (PsfTable == NULL || PcieRegBaseTable == NULL) {
    return 0;
  }

  TargetNum = 0;
  ZeroMem (TargetIdTable, sizeof(TargetIdTable));

  if ((UINT32)(GetPchMaxPciePortNum () + 1) > MaxTableSize) {
    DEBUG ((DEBUG_ERROR, "Cannot create MctpTargetsTable - table size is too small!\n"));
    return 0;
  }

  for (RpIndex = 0; RpIndex < GetPchMaxPciePortNum (); RpIndex++) {
    PciePsfPort.RegBase = PcieRegBaseTable[RpIndex].RegBase;
    PciePsfPort.PsfDev = PsfGetDev (PsfTable, PcieRegBaseTable[RpIndex].PsfNumber);
    if (PsfIsBridgeEnabled (PciePsfPort)) {
      TargetIdTable[TargetNum] = PsfPcieDestinationId (RpIndex);
      TargetNum++;
    }
  }
  if (IsPegPresent ()) {
    TargetIdTable[TargetNum] = PsfDmiDestinationId ();
    TargetNum++;
  }
  return TargetNum;
}

/**
  P2SB PSF port Destination ID (psf_id:port_group_id:port_id:channel_id)

  @retval P2SB Destination ID
**/
PSF_PORT_DEST_ID
PsfP2sbDestinationId (
  VOID
  )
{
  PSF_PORT_DEST_ID P2sbTarget;

  if (IsPchLp ()) {
    P2sbTarget.RegVal = V_VER2_PCH_LP_PSFX_PCR_PSF_MC_AGENT_MCAST_TGT_P2SB;
  } else {
    P2sbTarget.RegVal = V_VER2_PCH_H_PSFX_PCR_PSF_MC_AGENT_MCAST_TGT_P2SB;
  }
  return P2sbTarget;
}

/**
  DMI PSF port Destination ID (psf_id:port_group_id:port_id:channel_id)

  @retval DMI Destination ID
**/
PSF_PORT_DEST_ID
PsfDmiDestinationId (
  VOID
  )
{
  PSF_PORT_DEST_ID DmiTarget;

  DmiTarget.RegVal = V_VER2_PCH_PSFX_PSF_PCR_MC_AGENT_MCAST_TGT_DMI;

  return DmiTarget;
}

GLOBAL_REMOVE_IF_UNREFERENCED PSF_PORT_DEST_ID PchLpRpDestId[] =
{
  {0x18000}, {0x18001}, {0x18002}, {0x18003}, // SPA: PSF1, PortID = 0
  {0x18100}, {0x18101}, {0x18102}, {0x18103}, // SPB: PSF1, PortID = 1
  {0x18200}, {0x18201}, {0x18202}, {0x18203}, // SPC: PSF1, PortID = 2
};

GLOBAL_REMOVE_IF_UNREFERENCED PSF_PORT_DEST_ID PchHRpDestId[] =
{
  {0x78000}, {0x78001}, {0x78002}, {0x78003}, // SPA: PSF7, PortID = 0
  {0x98000}, {0x98001}, {0x98002}, {0x98003}, // SPB: PSF9, PortID = 0
  {0x78100}, {0x78101}, {0x78102}, {0x78103}, // SPC: PSF7, PortID = 1
  {0x88000}, {0x88001}, {0x88002}, {0x88003}, // SPD: PSF8, PortID = 0
  {0x88100}, {0x88101}, {0x88102}, {0x88103}, // SPE: PSF8, PortID = 1
  {0x98100}, {0x98101}, {0x98102}, {0x98103}  // SPF: PSF9, PortID = 1
};

/**
  PCIe PSF port destination ID (psf_id:port_group_id:port_id:channel_id)

  @param[in] RpIndex        PCIe Root Port Index (0 based)

  @retval Destination ID
**/
PSF_PORT_DEST_ID
PsfPcieDestinationId (
  IN UINT32  RpIndex
  )
{
  if (IsPchLp ()) {
    if (RpIndex < ARRAY_SIZE (PchLpRpDestId)) {
      return PchLpRpDestId[RpIndex];
    }
  } else {
    if (RpIndex < ARRAY_SIZE (PchHRpDestId)) {
      return PchHRpDestId[RpIndex];
    }
  }
  ASSERT (FALSE);
  return (PSF_PORT_DEST_ID){0};
}

PSF_PORT_RELAXED_ORDERING_CONFIG_REG  mPsfPortRelaxedOrderingConfigRegs[] =
{
  { &Psf1Dev, R_VER2_PCH_PSF_PCR_PORT_CONFIG_PG1_PORT3, B_PCH_PSF_PCR_PORT_CONFIG_EGRESS_FRO },
  { &Psf1Dev, R_VER2_PCH_PSF_PCR_PORT_CONFIG_PG1_PORT4, B_PCH_PSF_PCR_PORT_CONFIG_EGRESS_FRO },
  { &Psf1Dev, R_VER2_PCH_PSF_PCR_PORT_CONFIG_PG1_PORT5, B_PCH_PSF_PCR_PORT_CONFIG_EGRESS_FRO },
  { &Psf2Dev, R_VER2_PCH_PSF_PCR_PORT_CONFIG_PG1_PORT0, B_PCH_PSF_PCR_PORT_CONFIG_EGRESS_FRO },
  { &Psf2Dev, R_VER2_PCH_PSF_PCR_PORT_CONFIG_PG1_PORT1, B_PCH_PSF_PCR_PORT_CONFIG_EGRESS_FRO },
  { &Psf2Dev, R_VER2_PCH_PSF_PCR_PORT_CONFIG_PG1_PORT2, B_PCH_PSF_PCR_PORT_CONFIG_EGRESS_FRO },
  { &Psf2Dev, R_VER2_PCH_PSF_PCR_PORT_CONFIG_PG1_PORT3, B_PCH_PSF_PCR_PORT_CONFIG_EGRESS_FRO },
  { &Psf2Dev, R_VER2_PCH_PSF_PCR_PORT_CONFIG_PG1_PORT4, B_PCH_PSF_PCR_PORT_CONFIG_EGRESS_FRO },
  { &Psf2Dev, R_VER2_PCH_PSF_PCR_PORT_CONFIG_PG1_PORT5, B_PCH_PSF_PCR_PORT_CONFIG_EGRESS_FRO },
  { &Psf1Dev, R_VER2_PCH_PSF_PCR_PORT_CONFIG_PG0_PORT0, B_PCH_PSF_PCR_PORT_CONFIG_INGRESS_FRO },
  { &Psf2Dev, R_VER2_PCH_PSF_PCR_PORT_CONFIG_PG0_PORT0, B_PCH_PSF_PCR_PORT_CONFIG_INGRESS_FRO },
  { &Psf3Dev, R_VER2_PCH_PSF_PCR_PORT_CONFIG_PG0_PORT0, B_PCH_PSF_PCR_PORT_CONFIG_INGRESS_FRO },
  { &Psf4Dev, R_VER2_PCH_PSF_PCR_PORT_CONFIG_PG0_PORT0, B_PCH_PSF_PCR_PORT_CONFIG_INGRESS_FRO },
  { &Psf6Dev, R_VER2_PCH_PSF_PCR_PORT_CONFIG_PG0_PORT0, B_PCH_PSF_PCR_PORT_CONFIG_INGRESS_FRO }
};

PSF_PORT_RELAXED_ORDERING_CONFIG_REG  mPsfPortRelaxedOrderingConfigRegsPchN[] =
{
  { &Psf6Dev, R_VER2_PCH_PSF_PCR_PORT_CONFIG_PG1_PORT0, B_PCH_PSF_PCR_PORT_CONFIG_EGRESS_FRO },
  { &Psf1Dev, R_VER2_PCH_PSF_PCR_PORT_CONFIG_PG1_PORT0, B_PCH_PSF_PCR_PORT_CONFIG_EGRESS_FRO },
  { &Psf4Dev, R_VER2_PCH_PSF_PCR_PORT_CONFIG_PG1_PORT0, B_PCH_PSF_PCR_PORT_CONFIG_EGRESS_FRO },
  { &Psf3Dev, R_VER2_PCH_PSF_PCR_PORT_CONFIG_PG1_PORT0, B_PCH_PSF_PCR_PORT_CONFIG_EGRESS_FRO },
  { &Psf5Dev, R_VER2_PCH_N_PSF5_PCR_PORT_CONFIG_PG1_PORT0, B_PCH_PSF_PCR_PORT_CONFIG_EGRESS_FRO },
  { &Psf6Dev, R_VER2_PCH_PSF_PCR_PORT_CONFIG_PG1_PORT1, B_PCH_PSF_PCR_PORT_CONFIG_EGRESS_FRO },
  { &Psf1Dev, R_VER2_PCH_PSF_PCR_PORT_CONFIG_PG1_PORT1, B_PCH_PSF_PCR_PORT_CONFIG_EGRESS_FRO },
  { &Psf4Dev, R_VER2_PCH_PSF_PCR_PORT_CONFIG_PG1_PORT1, B_PCH_PSF_PCR_PORT_CONFIG_EGRESS_FRO },
  { &Psf3Dev, R_VER2_PCH_PSF_PCR_PORT_CONFIG_PG1_PORT1, B_PCH_PSF_PCR_PORT_CONFIG_EGRESS_FRO },
  { &Psf6Dev, R_VER2_PCH_PSF_PCR_PORT_CONFIG_PG1_PORT2, B_PCH_PSF_PCR_PORT_CONFIG_EGRESS_FRO },
  { &Psf1Dev, R_VER2_PCH_PSF_PCR_PORT_CONFIG_PG1_PORT2, B_PCH_PSF_PCR_PORT_CONFIG_EGRESS_FRO },
  { &Psf4Dev, R_VER2_PCH_PSF_PCR_PORT_CONFIG_PG1_PORT2, B_PCH_PSF_PCR_PORT_CONFIG_EGRESS_FRO },
  { &Psf3Dev, R_VER2_PCH_PSF_PCR_PORT_CONFIG_PG1_PORT2, B_PCH_PSF_PCR_PORT_CONFIG_EGRESS_FRO },
  { &Psf4Dev, R_VER2_PCH_PSF_PCR_PORT_CONFIG_PG1_PORT3, B_PCH_PSF_PCR_PORT_CONFIG_EGRESS_FRO },
  { &Psf3Dev, R_VER2_PCH_PSF_PCR_PORT_CONFIG_PG1_PORT3, B_PCH_PSF_PCR_PORT_CONFIG_EGRESS_FRO },
  { &Psf3Dev, R_VER2_PCH_PSF_PCR_PORT_CONFIG_PG1_PORT4, B_PCH_PSF_PCR_PORT_CONFIG_EGRESS_FRO },
  { &Psf3Dev, R_VER2_PCH_PSF_PCR_PORT_CONFIG_PG1_PORT5, B_PCH_PSF_PCR_PORT_CONFIG_EGRESS_FRO },
  { &Psf1Dev, R_VER2_PCH_PSF_PCR_PORT_CONFIG_PG1_PORT6, B_PCH_PSF_PCR_PORT_CONFIG_EGRESS_FRO },
  { &Psf3Dev, R_VER2_PCH_PSF_PCR_PORT_CONFIG_PG1_PORT6, B_PCH_PSF_PCR_PORT_CONFIG_EGRESS_FRO },
  { &Psf3Dev, R_VER2_PCH_PSF_PCR_PORT_CONFIG_PG1_PORT7, B_PCH_PSF_PCR_PORT_CONFIG_EGRESS_FRO },
  { &Psf3Dev, R_VER2_PCH_PSF_PCR_PORT_CONFIG_PG1_PORT8, B_PCH_PSF_PCR_PORT_CONFIG_EGRESS_FRO },
  { &Psf3Dev, R_VER2_PCH_PSF_PCR_PORT_CONFIG_PG1_PORT9, B_PCH_PSF_PCR_PORT_CONFIG_EGRESS_FRO },
  { &Psf3Dev, R_VER2_PCH_PSF_PCR_PORT_CONFIG_PG1_PORT10, B_PCH_PSF_PCR_PORT_CONFIG_EGRESS_FRO },
  { &Psf3Dev, R_VER2_PCH_N_PSF_PCR_PORT_CONFIG_PG1_PORT11, B_PCH_PSF_PCR_PORT_CONFIG_EGRESS_FRO }
};

PSF_PORT_RELAXED_ORDERING_CONFIG_REG  mPsfPortRelaxedOrderingConfigRegsPchLp[] =
{
  { &Psf3Dev, R_VER2_PCH_PSF_PCR_PORT_CONFIG_PG1_PORT4, B_PCH_PSF_PCR_PORT_CONFIG_EGRESS_FRO },
  { &Psf4Dev, R_VER2_PCH_PSF_PCR_PORT_CONFIG_PG1_PORT3, B_PCH_PSF_PCR_PORT_CONFIG_EGRESS_FRO }
};

PSF_PORT_RELAXED_ORDERING_CONFIG_REG  mPsfPortRelaxedOrderingConfigRegsPchH[] =
{
  { &Psf1Dev, R_VER2_PCH_PSF_PCR_PORT_CONFIG_PG1_PORT0, B_PCH_PSF_PCR_PORT_CONFIG_EGRESS_FRO },
  { &Psf1Dev, R_VER2_PCH_PSF_PCR_PORT_CONFIG_PG1_PORT1, B_PCH_PSF_PCR_PORT_CONFIG_EGRESS_FRO },
  { &Psf1Dev, R_VER2_PCH_PSF_PCR_PORT_CONFIG_PG1_PORT2, B_PCH_PSF_PCR_PORT_CONFIG_EGRESS_FRO },
  { &Psf7Dev, R_VER2_PCH_PSF_PCR_PORT_CONFIG_PG0_PORT0, B_PCH_PSF_PCR_PORT_CONFIG_INGRESS_FRO },
  { &Psf8Dev, R_VER2_PCH_PSF_PCR_PORT_CONFIG_PG0_PORT0, B_PCH_PSF_PCR_PORT_CONFIG_INGRESS_FRO },
  { &Psf9Dev, R_VER2_PCH_PSF_PCR_PORT_CONFIG_PG0_PORT0, B_PCH_PSF_PCR_PORT_CONFIG_INGRESS_FRO }
};

PSF_RELAXED_ORDER_REGS mPsfPortRelaxedOrderingConfigRegsStruct;

/**
  This function returns Psf Port Relaxed Ordering Configs

  @retval struct containing tables of registers for programming of Relaxed Ordering
**/
PSF_RELAXED_ORDER_REGS*
GetPsfPortRelaxedOrderingTables (
  VOID
  )
{
  mPsfPortRelaxedOrderingConfigRegsStruct.RegsTable = mPsfPortRelaxedOrderingConfigRegs;
  mPsfPortRelaxedOrderingConfigRegsStruct.RegsTableSize = ARRAY_SIZE (mPsfPortRelaxedOrderingConfigRegs);
  if (IsPchN()) {
    mPsfPortRelaxedOrderingConfigRegsStruct.RegsPchTypeSpecific = mPsfPortRelaxedOrderingConfigRegsPchN;
    mPsfPortRelaxedOrderingConfigRegsStruct.RegsPchTypeSpecificTableSize = ARRAY_SIZE (mPsfPortRelaxedOrderingConfigRegsPchN);
  } else if (IsPchLp ()) {
    mPsfPortRelaxedOrderingConfigRegsStruct.RegsPchTypeSpecific = mPsfPortRelaxedOrderingConfigRegsPchLp;
    mPsfPortRelaxedOrderingConfigRegsStruct.RegsPchTypeSpecificTableSize = ARRAY_SIZE (mPsfPortRelaxedOrderingConfigRegsPchLp);
  } else {
    mPsfPortRelaxedOrderingConfigRegsStruct.RegsPchTypeSpecific = mPsfPortRelaxedOrderingConfigRegsPchH;
    mPsfPortRelaxedOrderingConfigRegsStruct.RegsPchTypeSpecificTableSize = ARRAY_SIZE (mPsfPortRelaxedOrderingConfigRegsPchH);
  }
  return &mPsfPortRelaxedOrderingConfigRegsStruct;
}

/**
  Return PSF_PORT for CNVi device

  @retval    PsfPort         PSF PORT structure for CNVi device
**/
PSF_PORT
PsfCnviPort (
  VOID
  )
{
  PSF_PORT PsfPort;

  PsfPort.PsfDev = &Psf3Dev;
  if (IsPchN()) {
    PsfPort.RegBase = R_VER2_PCH_N_PSF3_PCR_T0_SHDW_CNVI_REG_BASE;
  } else if (IsPchLp ()) {
    PsfPort.RegBase = R_VER2_PCH_LP_PSF3_PCR_T0_SHDW_CNVI_REG_BASE;
  } else {
    PsfPort.RegBase = R_VER2_PCH_H_PSF3_PCR_T0_SHDW_CNVI_REG_BASE;
  }
    return PsfPort;
}

/**
  Return PSF_PORT for PMC device

  @retval    PsfPort         PSF PORT structure for PMC device
**/
PSF_PORT
PsfPmcPort (
  VOID
  )
{
  PSF_PORT PsfPort;

  PsfPort.PsfDev = &Psf3Dev;
  if (IsPchN()) {
    PsfPort.RegBase = R_VER2_PCH_N_PSF3_PCR_T0_SHDW_PMC_REG_BASE;
  } else if (IsPchLp ()) {
    PsfPort.RegBase = R_VER2_PCH_LP_PSF3_PCR_T0_SHDW_PMC_REG_BASE;
  } else {
    PsfPort.RegBase = R_VER2_PCH_H_PSF3_PCR_T0_SHDW_PMC_REG_BASE;
  }
  return PsfPort;
}

PSF_SEGMENT_TABLE mPchSegmentTable = PSF_SEGMENT_TABLE_INIT (
  { 1, &Psf1Dev },
  { 2, &Psf2Dev },
  { 3, &Psf3Dev },
  { 4, &Psf4Dev },
  { 5, &Psf5Dev },
  { 6, &Psf6Dev }
);

/**
  Get table of supported PSF segments

  @return  PsfSegmentTable   Table of supported PSF segments
**/
PSF_SEGMENT_TABLE*
PsfGetSegmentTable (
  VOID
  )
{
  return &mPchSegmentTable;
}

PSF_EARLY_INIT_DATA mPsfEarlyInitData = {
  &mPsfPciePortData,
  &mPchEoiRegDataTable,
  &mPchMctpRegDataTable,

  //
  // Is MCTP supported
  //
  TRUE
};

/**
  Return PSF_EARLY_INIT_DATA structure to Early Init Psf

  @retval Psf Early Init Data
**/
PSF_EARLY_INIT_DATA*
PsfGetEarlyInitData (
  VOID
  )
{
  return &mPsfEarlyInitData;
}

/**
  Return RC_OWNER value to program

  @retval RC_OWNER
**/
UINT32
PsfGetRcOwner (
  )
{
  return V_VER2_PCH_PSFX_PCR_RC_OWNER_PMT;
}

/**
  Configures rootspace 3 bus number for PCIe IMR use

  @param[in] Rs3Bus        bus number
**/
VOID
PsfSetRs3Bus (
  UINT8 Rs3Bus
  )
{
  PchPcrWrite32 (PID_PSF1, R_PCH_PSF_PCR_BUS_SHADOW_RS3, Rs3Bus);
}
