/** @file
  This file contains internal PSF routines for PCH PSF SoC lib usage

@copyright
  INTEL CONFIDENTIAL
  Copyright 2019 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains an 'Intel Peripheral Driver' and is uniquely identified as
  "Intel Reference Module" and is licensed for Intel CPUs and chipsets under
  the terms of your license agreement with Intel or your vendor. This file may
  be modified by the user, subject to additional terms of the license agreement.

@par Specification Reference:
**/

#include <Base.h>
#include <Library/BaseLib.h>
#include <Uefi/UefiBaseType.h>
#include <Library/IoLib.h>
#include <Library/DebugLib.h>
#include <Library/PchPcrLib.h>
#include <Library/PchInfoLib.h>
#include <Library/SataSocLib.h>
#include <Library/BaseMemoryLib.h>
#include <Library/CpuPcieInfoFruLib.h>
#include <Register/PchRegsPsf.h>
#include <Register/PsfRegsAdpS.h>
#include <Register/PchPcrRegs.h>
#include <Library/PsfLib.h>
#include <Library/PsfSocLib.h>
#include <Library/PciSegmentLib.h>
#include <PchPcieRpInfo.h>
#include <Library/P2SbSidebandAccessLib.h>
#include <PchReservedResources.h>
#include <PchBdfAssignment.h>
#include <Register/PchRegs.h>

GLOBAL_REMOVE_IF_UNREFERENCED P2SB_CONTROLLER P2SbDev = {
  PCI_SEGMENT_LIB_ADDRESS (
           DEFAULT_PCI_SEGMENT_NUMBER_PCH,
           DEFAULT_PCI_BUS_NUMBER_PCH,
           PCI_DEVICE_NUMBER_PCH_P2SB,
           PCI_FUNCTION_NUMBER_PCH_P2SB,
           0
           ),
  PCH_PCR_BASE_ADDRESS
  };

GLOBAL_REMOVE_IF_UNREFERENCED P2SB_SIDEBAND_REGISTER_ACCESS Psf1Access = {
  P2SB_REGISTER_ACCESS_INIT,
  P2SbMmioAccess,
  PID_PSF1,
  0,
  P2SbPrivateConfig,
  FALSE,
  &P2SbDev
  };

GLOBAL_REMOVE_IF_UNREFERENCED P2SB_SIDEBAND_REGISTER_ACCESS Psf2Access = {
  P2SB_REGISTER_ACCESS_INIT,
  P2SbMmioAccess,
  PID_PSF2,
  0,
  P2SbPrivateConfig,
  FALSE,
  &P2SbDev
  };

GLOBAL_REMOVE_IF_UNREFERENCED P2SB_SIDEBAND_REGISTER_ACCESS Psf3Access = {
  P2SB_REGISTER_ACCESS_INIT,
  P2SbMmioAccess,
  PID_PSF3,
  0,
  P2SbPrivateConfig,
  FALSE,
  &P2SbDev
  };

GLOBAL_REMOVE_IF_UNREFERENCED P2SB_SIDEBAND_REGISTER_ACCESS Psf4Access = {
  P2SB_REGISTER_ACCESS_INIT,
  P2SbMmioAccess,
  PID_PSF4,
  0,
  P2SbPrivateConfig,
  FALSE,
  &P2SbDev
  };

GLOBAL_REMOVE_IF_UNREFERENCED P2SB_SIDEBAND_REGISTER_ACCESS Psf5Access = {
  P2SB_REGISTER_ACCESS_INIT,
  P2SbMmioAccess,
  PID_CSME_PSF,
  0,
  P2SbPrivateConfig,
  FALSE,
  &P2SbDev
  };

GLOBAL_REMOVE_IF_UNREFERENCED P2SB_SIDEBAND_REGISTER_ACCESS Psf6Access = {
  P2SB_REGISTER_ACCESS_INIT,
  P2SbMmioAccess,
  PID_PSF6,
  0,
  P2SbPrivateConfig,
  FALSE,
  &P2SbDev
  };

GLOBAL_REMOVE_IF_UNREFERENCED P2SB_SIDEBAND_REGISTER_ACCESS Psf7Access = {
  P2SB_REGISTER_ACCESS_INIT,
  P2SbMmioAccess,
  PID_PSF7,
  0,
  P2SbPrivateConfig,
  FALSE,
  &P2SbDev
  };

GLOBAL_REMOVE_IF_UNREFERENCED PSF_DEV  Psf1Dev = {
  &Psf1Access.Access,
  FALSE
};

GLOBAL_REMOVE_IF_UNREFERENCED PSF_DEV  Psf2Dev = {
  &Psf2Access.Access,
  FALSE
};

GLOBAL_REMOVE_IF_UNREFERENCED PSF_DEV  Psf3Dev = {
  &Psf3Access.Access,
  FALSE
};

GLOBAL_REMOVE_IF_UNREFERENCED PSF_DEV  Psf4Dev = {
  &Psf4Access.Access,
  FALSE
};

GLOBAL_REMOVE_IF_UNREFERENCED PSF_DEV  Psf5Dev = {
  &Psf5Access.Access,
  TRUE
};

GLOBAL_REMOVE_IF_UNREFERENCED PSF_DEV  Psf6Dev = {
  &Psf6Access.Access,
  FALSE
};

GLOBAL_REMOVE_IF_UNREFERENCED PSF_DEV  Psf7Dev = {
  &Psf7Access.Access,
  FALSE
};

GLOBAL_REMOVE_IF_UNREFERENCED PSF_PORT mPchSSerialIoI2cPsfPort[] =
{
  {&Psf3Dev, R_VER4_PCH_S_PSF3_PCR_T0_SHDW_I2C0_REG_BASE},
  {&Psf3Dev, R_VER4_PCH_S_PSF3_PCR_T0_SHDW_I2C1_REG_BASE},
  {&Psf3Dev, R_VER4_PCH_S_PSF3_PCR_T0_SHDW_I2C2_REG_BASE},
  {&Psf3Dev, R_VER4_PCH_S_PSF3_PCR_T0_SHDW_I2C3_REG_BASE},
  {&Psf3Dev, R_VER4_PCH_S_PSF3_PCR_T0_SHDW_I2C4_REG_BASE},
  {&Psf3Dev, R_VER4_PCH_S_PSF3_PCR_T0_SHDW_I2C5_REG_BASE},
  {&Psf3Dev, R_VER4_PCH_S_PSF3_PCR_T0_SHDW_I2C6_REG_BASE},
  {&Psf3Dev, R_VER4_PCH_S_PSF3_PCR_T0_SHDW_I2C7_REG_BASE}
};

/**
  Return PSF_PORT for SerialIO I2C device

  @param[in] I2cNum  Serial IO I2C device (I2C0, I2C1, ....)

  @retval  PsfPort   PSF PORT structure for SerialIO I2C device
**/
PSF_PORT
PsfSerialIoI2cPort (
  IN UINT32  I2cNum
  )
{
  if (I2cNum < ARRAY_SIZE (mPchSSerialIoI2cPsfPort)) {
    return mPchSSerialIoI2cPsfPort[I2cNum];
  }

  ASSERT(FALSE);
  return PSF_PORT_NULL;
}

GLOBAL_REMOVE_IF_UNREFERENCED PSF_PORT mPchSSerialIoSpiPsfPort[] =
{
  {&Psf3Dev, R_VER4_PCH_S_PSF3_PCR_T0_SHDW_SPI0_REG_BASE},
  {&Psf3Dev, R_VER4_PCH_S_PSF3_PCR_T0_SHDW_SPI1_REG_BASE},
  {&Psf3Dev, R_VER4_PCH_S_PSF3_PCR_T0_SHDW_SPI2_REG_BASE},
  {&Psf3Dev, R_VER4_PCH_S_PSF3_PCR_T0_SHDW_SPI3_REG_BASE},
  {&Psf3Dev, R_VER4_PCH_S_PSF3_PCR_T0_SHDW_SPI4_REG_BASE},
  {&Psf3Dev, R_VER4_PCH_S_PSF3_PCR_T0_SHDW_SPI5_REG_BASE},
  {&Psf3Dev, R_VER4_PCH_S_PSF3_PCR_T0_SHDW_SPI6_REG_BASE}
};

/**
  Return PSF_PORT for SerialIO SPI device

  @param[in] I2cNum  Serial IO SPI device (SPI0, SPI1, ....)

  @retval  PsfPort   PSF PORT structure for SerialIO SPI device
**/
PSF_PORT
PsfSerialIoSpiPort (
  IN UINT32  SpiNum
  )
{
  if (SpiNum < ARRAY_SIZE (mPchSSerialIoSpiPsfPort)) {
    return mPchSSerialIoSpiPsfPort[SpiNum];
  }

  ASSERT(FALSE);
  return PSF_PORT_NULL;
}

GLOBAL_REMOVE_IF_UNREFERENCED PSF_PORT mPchSSerialIoUartPsfPort[] =
{
  {&Psf3Dev, R_VER4_PCH_S_PSF3_PCR_T0_SHDW_UART0_REG_BASE},
  {&Psf3Dev, R_VER4_PCH_S_PSF3_PCR_T0_SHDW_UART1_REG_BASE},
  {&Psf3Dev, R_VER4_PCH_S_PSF3_PCR_T0_SHDW_UART2_REG_BASE},
  {&Psf3Dev, R_VER4_PCH_S_PSF3_PCR_T0_SHDW_UART3_REG_BASE},
  {&Psf3Dev, R_VER4_PCH_S_PSF3_PCR_T0_SHDW_UART4_REG_BASE},
  {&Psf3Dev, R_VER4_PCH_S_PSF3_PCR_T0_SHDW_UART5_REG_BASE},
  {&Psf3Dev, R_VER4_PCH_S_PSF3_PCR_T0_SHDW_UART6_REG_BASE}
};

/**
  Return PSF_PORT for SerialIO UART device

  @param[in] UartNum  Serial IO UART device (UART0, UART1, ....)

  @retval  PsfPort   PSF PORT structure for SerialIO UART device
**/
PSF_PORT
PsfSerialIoUartPort (
  IN UINT32  UartNum
  )
{
  if (UartNum < ARRAY_SIZE (mPchSSerialIoUartPsfPort)) {
    return mPchSSerialIoUartPsfPort[UartNum];
  }

  ASSERT(FALSE);
  return PSF_PORT_NULL;
}

/**
  Return PSF_PORT for TraceHub device

  @retval    PsfPort         PSF PORT structure for TraceHub device
**/
PSF_PORT
PsfTraceHubPort (
  VOID
  )
{
  PSF_PORT PsfPort;

  PsfPort.PsfDev = &Psf1Dev;
  PsfPort.RegBase = R_VER4_PCH_S_PSF1_PCR_T0_SHDW_TRACEHUB_REG_BASE;

  return PsfPort;
}

/**
  Return PSF_PORT for TraceHub ACPI device

  @retval    PsfPort         PSF PORT structure for TraceHub ACPI device
**/
PSF_PORT
PsfTraceHubAcpiDevPort (
  VOID
  )
{
  PSF_PORT PsfPort;

  PsfPort.PsfDev = &Psf1Dev;
  PsfPort.RegBase = R_VER4_PCH_S_PSF1_PCR_T0_SHDW_NPK_PHANTOM_REG_BASE;

  return PsfPort;
}

GLOBAL_REMOVE_IF_UNREFERENCED UINT16 mPchSHeciPsfRegBase[] =
{
  R_VER4_PCH_S_PSF1_PCR_T0_SHDW_HECI1_REG_BASE,
  R_VER4_PCH_S_PSF1_PCR_T0_SHDW_HECI2_REG_BASE,
  R_VER4_PCH_S_PSF1_PCR_T0_SHDW_HECI3_REG_BASE,
  R_VER4_PCH_S_PSF1_PCR_T0_SHDW_HECI4_REG_BASE
};

/**
  Return PSF_PORT for HECI device

  @param[in] HeciDevice      HECIx Device (HECI1-4)

  @retval    PsfPort         PSF PORT structure for HECI device
**/
PSF_PORT
PsfHeciPort (
  IN UINT8      HeciDevice
  )
{
  PSF_PORT PsfPort;

  if ((HeciDevice >= 1) && (HeciDevice <= 4)) {
    PsfPort.PsfDev = &Psf1Dev;
    PsfPort.RegBase = mPchSHeciPsfRegBase[HeciDevice - 1];
    return PsfPort;
  }

  ASSERT (FALSE);
  return PSF_PORT_NULL;
}

/**
  Return PSF_PORT for SOL device

  @retval    PsfPort         PSF PORT structure for SOL device
**/
PSF_PORT
PsfSolPort (
  VOID
  )
{
  PSF_PORT PsfPort;

  PsfPort.PsfDev = &Psf1Dev;
  PsfPort.RegBase = R_VER4_PCH_S_PSF1_PCR_T0_SHDW_KT_REG_BASE;

  return PsfPort;
}

/**
  Disable IDER device at PSF level
**/
VOID
PsfDisableIderDevice (
  VOID
  )
{
  PSF_PORT PsfPort;

  PsfPort.PsfDev = &Psf1Dev;
  PsfPort.RegBase = R_VER4_PCH_S_PSF1_PCR_T0_SHDW_IDER_REG_BASE;
  PsfDisableDevice (PsfPort);
}

/**
  Disable HDAudio device at PSF level
**/
VOID
PsfDisableHdaDevice (
  VOID
  )
{
  PSF_PORT PsfPort;

  PsfPort.PsfDev = &Psf3Dev;
  PsfPort.RegBase = R_VER4_PCH_S_PSF3_PCR_T0_SHDW_AUD_REG_BASE;
  PsfDisableDevice (PsfPort);
}

/**
  Disable DSP Bar (HdAudio BAR4 and BAR5) at PSF level.
**/
VOID
PsfDisableDspBar (
  VOID
  )
{
  PSF_PORT PsfPort;

  PsfPort.PsfDev = &Psf3Dev;
  PsfPort.RegBase = R_VER4_PCH_S_PSF3_PCR_T0_SHDW_AUD_REG_BASE;

  PsfDisableDeviceBar (PsfPort, (R_PCH_PSFX_PCR_T0_SHDW_BAR5 | R_PCH_PSFX_PCR_T0_SHDW_BAR4));
}

GLOBAL_REMOVE_IF_UNREFERENCED UINT16 mPchSThcPsfRegBase[] =
{
  R_VER4_PCH_S_PSF3_PCR_T0_SHDW_THC0_REG_BASE,
  R_VER4_PCH_S_PSF3_PCR_T0_SHDW_THC1_REG_BASE
};

/**
  Disable THC device at PSF level

  @param[in]  ThcNumber                Touch Host Controller Number THC0 or THC1
**/
VOID
PsfDisableThcDevice (
  IN  UINT32        ThcNumber
  )
{
  PSF_PORT PsfPort;

  if ((IsPchS () && ThcNumber >= ARRAY_SIZE (mPchSThcPsfRegBase))) {
    DEBUG ((DEBUG_ERROR, "Wrong THC number!\n"));
    ASSERT (FALSE);
    return;
  }

  PsfPort.PsfDev = &Psf3Dev;
  PsfPort.RegBase = mPchSThcPsfRegBase[ThcNumber];
  PsfDisableDevice (PsfPort);
}

/**
  Disable xDCI device at PSF level
**/
VOID
PsfDisableXdciDevice (
  VOID
  )
{
  PSF_PORT PsfPort;

  PsfPort.PsfDev = &Psf2Dev;
  PsfPort.RegBase = R_VER4_PCH_S_PSF2_PCR_T0_SHDW_XDCI_REG_BASE;
  PsfDisableDevice (PsfPort);
}

/**
  Disable xHCI device at PSF level
**/
VOID
PsfDisableXhciDevice (
  VOID
  )
{
  PSF_PORT PsfPort;

  PsfPort.PsfDev = &Psf1Dev;
  PsfPort.RegBase = R_VER4_PCH_S_PSF1_PCR_T0_SHDW_XHCI_REG_BASE;
  PsfDisableDevice (PsfPort);
}

/**
  Disable xHCI VTIO Phantom device at PSF level
**/
VOID
PsfDisableXhciVtioDevice (
  VOID
  )
{
  // Not applicable for ADL platform
}

/**
  Disable SATA device at PSF level

  @param[in]  SataCtrlIndex     SATA controller index
**/
VOID
PsfDisableSataDevice (
  IN UINT32     SataCtrlIndex
  )
{
  PSF_PORT PsfPort;

  ASSERT (SataCtrlIndex < 1);

  PsfPort.PsfDev = &Psf1Dev;
  PsfPort.RegBase = R_VER4_PCH_S_PSF1_PCR_T0_SHDW_SATA_REG_BASE;
  PsfDisableDevice (PsfPort);
}

/**
  Return PSF_PORT for ISH device

  @retval    PsfPort         PSF PORT structure for ISH device
**/
PSF_PORT
PsfIshPort (
  VOID
  )
{
  PSF_PORT PsfPort;

  PsfPort.PsfDev = &Psf3Dev;
  PsfPort.RegBase = R_VER4_PCH_S_PSF3_PCR_T0_SHDW_ISH_REG_BASE;

  return PsfPort;
}

/**
  Disable FPAK device at PSF level
**/
VOID
PsfDisableFpakDevice (
  VOID
  )
{
  if (!IsPchS ()) {
    PsfDisableDevice (PsfFpakPort ());
  }
}

/**
  Return PSF_PORT for FPAK device

  @retval    PsfPort         PSF PORT structure for FPAK device
**/
PSF_PORT
PsfFpakPort (
  VOID
  )
{
  // Not applicable for ADL platform
  ASSERT (FALSE);
  return PSF_PORT_NULL;
}

/**
  Disable GbE device at PSF level
**/
VOID
PsfDisableGbeDevice (
  VOID
  )
{
  PSF_PORT PsfPort;

  PsfPort.PsfDev = &Psf3Dev;
  PsfPort.RegBase = R_VER4_PCH_S_PSF3_PCR_T0_SHDW_GBE_REG_BASE;
  PsfDisableDevice (PsfPort);
}

/**
  Disable SMBUS device at PSF level
**/
VOID
PsfDisableSmbusDevice (
  VOID
  )
{
  PSF_PORT PsfPort;

  PsfPort.PsfDev = &Psf3Dev;
  PsfPort.RegBase = R_VER4_PCH_S_PSF3_PCR_T0_SHDW_SMBUS_REG_BASE;
  PsfDisableDevice (PsfPort);
}

//
// RS0 PSF1_T1_SHDW_PCIExx_REG_BASE registers for PCH-S
//
GLOBAL_REMOVE_IF_UNREFERENCED PSF_REG_BASE mPchPsfPcieRegBase[] =
{
  { 1, R_VER4_PCH_S_PSF1_PCR_T1_SHDW_PCIE01_RS0_REG_BASE },
  { 1, R_VER4_PCH_S_PSF1_PCR_T1_SHDW_PCIE02_RS0_REG_BASE },
  { 1, R_VER4_PCH_S_PSF1_PCR_T1_SHDW_PCIE03_RS0_REG_BASE },
  { 1, R_VER4_PCH_S_PSF1_PCR_T1_SHDW_PCIE04_RS0_REG_BASE },
  { 1, R_VER4_PCH_S_PSF1_PCR_T1_SHDW_PCIE05_RS0_REG_BASE },
  { 1, R_VER4_PCH_S_PSF1_PCR_T1_SHDW_PCIE06_RS0_REG_BASE },
  { 1, R_VER4_PCH_S_PSF1_PCR_T1_SHDW_PCIE07_RS0_REG_BASE },
  { 1, R_VER4_PCH_S_PSF1_PCR_T1_SHDW_PCIE08_RS0_REG_BASE },
  { 1, R_VER4_PCH_S_PSF1_PCR_T1_SHDW_PCIE09_RS0_REG_BASE },
  { 1, R_VER4_PCH_S_PSF1_PCR_T1_SHDW_PCIE10_RS0_REG_BASE },
  { 1, R_VER4_PCH_S_PSF1_PCR_T1_SHDW_PCIE11_RS0_REG_BASE },
  { 1, R_VER4_PCH_S_PSF1_PCR_T1_SHDW_PCIE12_RS0_REG_BASE },
  { 1, R_VER4_PCH_S_PSF1_PCR_T1_SHDW_PCIE13_RS0_REG_BASE },
  { 1, R_VER4_PCH_S_PSF1_PCR_T1_SHDW_PCIE14_RS0_REG_BASE },
  { 1, R_VER4_PCH_S_PSF1_PCR_T1_SHDW_PCIE15_RS0_REG_BASE },
  { 1, R_VER4_PCH_S_PSF1_PCR_T1_SHDW_PCIE16_RS0_REG_BASE },
  { 1, R_VER4_PCH_S_PSF1_PCR_T1_SHDW_PCIE17_RS0_REG_BASE },
  { 1, R_VER4_PCH_S_PSF1_PCR_T1_SHDW_PCIE18_RS0_REG_BASE },
  { 1, R_VER4_PCH_S_PSF1_PCR_T1_SHDW_PCIE19_RS0_REG_BASE },
  { 1, R_VER4_PCH_S_PSF1_PCR_T1_SHDW_PCIE20_RS0_REG_BASE },
  { 1, R_VER4_PCH_S_PSF1_PCR_T1_SHDW_PCIE21_RS0_REG_BASE },
  { 1, R_VER4_PCH_S_PSF1_PCR_T1_SHDW_PCIE22_RS0_REG_BASE },
  { 1, R_VER4_PCH_S_PSF1_PCR_T1_SHDW_PCIE23_RS0_REG_BASE },
  { 1, R_VER4_PCH_S_PSF1_PCR_T1_SHDW_PCIE24_RS0_REG_BASE },
  { 1, R_VER4_PCH_S_PSF1_PCR_T1_SHDW_PCIE25_RS0_REG_BASE },
  { 1, R_VER4_PCH_S_PSF1_PCR_T1_SHDW_PCIE26_RS0_REG_BASE },
  { 1, R_VER4_PCH_S_PSF1_PCR_T1_SHDW_PCIE27_RS0_REG_BASE },
  { 1, R_VER4_PCH_S_PSF1_PCR_T1_SHDW_PCIE28_RS0_REG_BASE }
};

/**
  Return PSF_REG_BASE table at root PSF level to which PCIe Root Port device is connected

  @retval    PsfRegBase        PSF REG BASE structure table for PCIe
**/
PSF_REG_BASE*
PsfGetRootPciePortTable (
  VOID
  )
{
  return mPchPsfPcieRegBase;
}

PSF_PCIE_PORT_DATA_TABLE mPsfPciePortData = PSF_PCIE_PORT_DATA_TABLE_INIT (
  { 1, 7, R_VER4_PCH_S_PSF1_PCR_T1_SHDW_PCIE01_RS0_REG_BASE, 0, R_VER4_PCH_S_PSF7_PCR_T1_SHDW_PCIE01_REG_BASE, R_VER4_PCH_S_PSF1_PCR_T1_AGENT_FUNCTION_CONFIG_SPA_RS0_D28_F0, 0 },
  { 1, 7, R_VER4_PCH_S_PSF1_PCR_T1_SHDW_PCIE02_RS0_REG_BASE, 0, R_VER4_PCH_S_PSF7_PCR_T1_SHDW_PCIE02_REG_BASE, R_VER4_PCH_S_PSF1_PCR_T1_AGENT_FUNCTION_CONFIG_SPA_RS0_D28_F1, 0 },
  { 1, 7, R_VER4_PCH_S_PSF1_PCR_T1_SHDW_PCIE03_RS0_REG_BASE, 0, R_VER4_PCH_S_PSF7_PCR_T1_SHDW_PCIE03_REG_BASE, R_VER4_PCH_S_PSF1_PCR_T1_AGENT_FUNCTION_CONFIG_SPA_RS0_D28_F2, 0 },
  { 1, 7, R_VER4_PCH_S_PSF1_PCR_T1_SHDW_PCIE04_RS0_REG_BASE, 0, R_VER4_PCH_S_PSF7_PCR_T1_SHDW_PCIE04_REG_BASE, R_VER4_PCH_S_PSF1_PCR_T1_AGENT_FUNCTION_CONFIG_SPA_RS0_D28_F3, 0 },
  { 1, 7, R_VER4_PCH_S_PSF1_PCR_T1_SHDW_PCIE05_RS0_REG_BASE, 0, R_VER4_PCH_S_PSF7_PCR_T1_SHDW_PCIE05_REG_BASE, R_VER4_PCH_S_PSF1_PCR_T1_AGENT_FUNCTION_CONFIG_SPB_RS0_D28_F4, 0 },
  { 1, 7, R_VER4_PCH_S_PSF1_PCR_T1_SHDW_PCIE06_RS0_REG_BASE, 0, R_VER4_PCH_S_PSF7_PCR_T1_SHDW_PCIE06_REG_BASE, R_VER4_PCH_S_PSF1_PCR_T1_AGENT_FUNCTION_CONFIG_SPB_RS0_D28_F5, 0 },
  { 1, 7, R_VER4_PCH_S_PSF1_PCR_T1_SHDW_PCIE07_RS0_REG_BASE, 0, R_VER4_PCH_S_PSF7_PCR_T1_SHDW_PCIE07_REG_BASE, R_VER4_PCH_S_PSF1_PCR_T1_AGENT_FUNCTION_CONFIG_SPB_RS0_D28_F6, 0 },
  { 1, 7, R_VER4_PCH_S_PSF1_PCR_T1_SHDW_PCIE08_RS0_REG_BASE, 0, R_VER4_PCH_S_PSF7_PCR_T1_SHDW_PCIE08_REG_BASE, R_VER4_PCH_S_PSF1_PCR_T1_AGENT_FUNCTION_CONFIG_SPB_RS0_D28_F7, 0 },
  { 1, 7, R_VER4_PCH_S_PSF1_PCR_T1_SHDW_PCIE09_RS0_REG_BASE, 0, R_VER4_PCH_S_PSF7_PCR_T1_SHDW_PCIE09_REG_BASE, R_VER4_PCH_S_PSF1_PCR_T1_AGENT_FUNCTION_CONFIG_SPC_RS0_D29_F0, 0 },
  { 1, 7, R_VER4_PCH_S_PSF1_PCR_T1_SHDW_PCIE10_RS0_REG_BASE, 0, R_VER4_PCH_S_PSF7_PCR_T1_SHDW_PCIE10_REG_BASE, R_VER4_PCH_S_PSF1_PCR_T1_AGENT_FUNCTION_CONFIG_SPC_RS0_D29_F1, 0 },
  { 1, 7, R_VER4_PCH_S_PSF1_PCR_T1_SHDW_PCIE11_RS0_REG_BASE, 0, R_VER4_PCH_S_PSF7_PCR_T1_SHDW_PCIE11_REG_BASE, R_VER4_PCH_S_PSF1_PCR_T1_AGENT_FUNCTION_CONFIG_SPC_RS0_D29_F2, 0 },
  { 1, 7, R_VER4_PCH_S_PSF1_PCR_T1_SHDW_PCIE12_RS0_REG_BASE, 0, R_VER4_PCH_S_PSF7_PCR_T1_SHDW_PCIE12_REG_BASE, R_VER4_PCH_S_PSF1_PCR_T1_AGENT_FUNCTION_CONFIG_SPC_RS0_D29_F3, 0 },
  { 1, 7, R_VER4_PCH_S_PSF1_PCR_T1_SHDW_PCIE13_RS0_REG_BASE, 0, R_VER4_PCH_S_PSF7_PCR_T1_SHDW_PCIE13_REG_BASE, R_VER4_PCH_S_PSF1_PCR_T1_AGENT_FUNCTION_CONFIG_SPD_RS0_D29_F4, 0 },
  { 1, 7, R_VER4_PCH_S_PSF1_PCR_T1_SHDW_PCIE14_RS0_REG_BASE, 0, R_VER4_PCH_S_PSF7_PCR_T1_SHDW_PCIE14_REG_BASE, R_VER4_PCH_S_PSF1_PCR_T1_AGENT_FUNCTION_CONFIG_SPD_RS0_D29_F5, 0 },
  { 1, 7, R_VER4_PCH_S_PSF1_PCR_T1_SHDW_PCIE15_RS0_REG_BASE, 0, R_VER4_PCH_S_PSF7_PCR_T1_SHDW_PCIE15_REG_BASE, R_VER4_PCH_S_PSF1_PCR_T1_AGENT_FUNCTION_CONFIG_SPD_RS0_D29_F6, 0 },
  { 1, 7, R_VER4_PCH_S_PSF1_PCR_T1_SHDW_PCIE16_RS0_REG_BASE, 0, R_VER4_PCH_S_PSF7_PCR_T1_SHDW_PCIE16_REG_BASE, R_VER4_PCH_S_PSF1_PCR_T1_AGENT_FUNCTION_CONFIG_SPD_RS0_D29_F7, 0 },
  { 1, 7, R_VER4_PCH_S_PSF1_PCR_T1_SHDW_PCIE17_RS0_REG_BASE, 0, R_VER4_PCH_S_PSF7_PCR_T1_SHDW_PCIE17_REG_BASE, R_VER4_PCH_S_PSF1_PCR_T1_AGENT_FUNCTION_CONFIG_SPE_RS0_D27_F0, 0 },
  { 1, 7, R_VER4_PCH_S_PSF1_PCR_T1_SHDW_PCIE18_RS0_REG_BASE, 0, R_VER4_PCH_S_PSF7_PCR_T1_SHDW_PCIE18_REG_BASE, R_VER4_PCH_S_PSF1_PCR_T1_AGENT_FUNCTION_CONFIG_SPE_RS0_D27_F1, 0 },
  { 1, 7, R_VER4_PCH_S_PSF1_PCR_T1_SHDW_PCIE19_RS0_REG_BASE, 0, R_VER4_PCH_S_PSF7_PCR_T1_SHDW_PCIE19_REG_BASE, R_VER4_PCH_S_PSF1_PCR_T1_AGENT_FUNCTION_CONFIG_SPE_RS0_D27_F2, 0 },
  { 1, 7, R_VER4_PCH_S_PSF1_PCR_T1_SHDW_PCIE20_RS0_REG_BASE, 0, R_VER4_PCH_S_PSF7_PCR_T1_SHDW_PCIE20_REG_BASE, R_VER4_PCH_S_PSF1_PCR_T1_AGENT_FUNCTION_CONFIG_SPE_RS0_D27_F3, 0 },
  { 1, 7, R_VER4_PCH_S_PSF1_PCR_T1_SHDW_PCIE21_RS0_REG_BASE, 0, R_VER4_PCH_S_PSF7_PCR_T1_SHDW_PCIE21_REG_BASE, R_VER4_PCH_S_PSF1_PCR_T1_AGENT_FUNCTION_CONFIG_SPF_RS0_D27_F4, 0 },
  { 1, 7, R_VER4_PCH_S_PSF1_PCR_T1_SHDW_PCIE22_RS0_REG_BASE, 0, R_VER4_PCH_S_PSF7_PCR_T1_SHDW_PCIE22_REG_BASE, R_VER4_PCH_S_PSF1_PCR_T1_AGENT_FUNCTION_CONFIG_SPF_RS0_D27_F5, 0 },
  { 1, 7, R_VER4_PCH_S_PSF1_PCR_T1_SHDW_PCIE23_RS0_REG_BASE, 0, R_VER4_PCH_S_PSF7_PCR_T1_SHDW_PCIE23_REG_BASE, R_VER4_PCH_S_PSF1_PCR_T1_AGENT_FUNCTION_CONFIG_SPF_RS0_D27_F6, 0 },
  { 1, 7, R_VER4_PCH_S_PSF1_PCR_T1_SHDW_PCIE24_RS0_REG_BASE, 0, R_VER4_PCH_S_PSF7_PCR_T1_SHDW_PCIE24_REG_BASE, R_VER4_PCH_S_PSF1_PCR_T1_AGENT_FUNCTION_CONFIG_SPF_RS0_D27_F7, 0 },
  { 1, 7, R_VER4_PCH_S_PSF1_PCR_T1_SHDW_PCIE25_RS0_REG_BASE, 0, R_VER4_PCH_S_PSF7_PCR_T1_SHDW_PCIE25_REG_BASE, R_VER4_PCH_S_PSF1_PCR_T1_AGENT_FUNCTION_CONFIG_SPG_RS0_D26_F0, 0 },
  { 1, 7, R_VER4_PCH_S_PSF1_PCR_T1_SHDW_PCIE26_RS0_REG_BASE, 0, R_VER4_PCH_S_PSF7_PCR_T1_SHDW_PCIE26_REG_BASE, R_VER4_PCH_S_PSF1_PCR_T1_AGENT_FUNCTION_CONFIG_SPG_RS0_D26_F1, 0 },
  { 1, 7, R_VER4_PCH_S_PSF1_PCR_T1_SHDW_PCIE27_RS0_REG_BASE, 0, R_VER4_PCH_S_PSF7_PCR_T1_SHDW_PCIE27_REG_BASE, R_VER4_PCH_S_PSF1_PCR_T1_AGENT_FUNCTION_CONFIG_SPG_RS0_D26_F2, 0 },
  { 1, 7, R_VER4_PCH_S_PSF1_PCR_T1_SHDW_PCIE28_RS0_REG_BASE, 0, R_VER4_PCH_S_PSF7_PCR_T1_SHDW_PCIE28_REG_BASE, R_VER4_PCH_S_PSF1_PCR_T1_AGENT_FUNCTION_CONFIG_SPG_RS0_D26_F3, 0 }
);

/**
  Get PSF PCIe Root Port Data

  @param[in]  RpIndex         PCIe Root Port Index (0 based)
  @retval     PciePortData    PCIE PORT Data
**/
PSF_PCIE_PORT_DATA*
PsfGetPciePortData (
  IN UINT32  RpIndex
  )
{
  if (RpIndex < mPsfPciePortData.Size) {
    return &mPsfPciePortData.Data[RpIndex];
  }
  return NULL;
}

/**
  Get PSF PCIe Root Port Data Table
**/
PSF_PCIE_PORT_DATA_TABLE*
PsfGetPciePortDataTable (
  VOID
  )
{
  return &mPsfPciePortData;
}
PSF_GRANT_COUNT_NUMBER mGrantCountNumSpa = PSF_GRANT_COUNT_NUMBER_INIT ({ 28,  0 }, { 27,  1 }, { 26,  2 }, { 25,  3 });
PSF_GRANT_COUNT_NUMBER mGrantCountNumSpb = PSF_GRANT_COUNT_NUMBER_INIT ({ 24,  4 }, { 23,  5 }, { 22,  6 }, { 21,  7 });
PSF_GRANT_COUNT_NUMBER mGrantCountNumSpc = PSF_GRANT_COUNT_NUMBER_INIT ({ 20,  8 }, { 19,  9 }, { 18, 10 }, { 17, 11 });
PSF_GRANT_COUNT_NUMBER mGrantCountNumSpd = PSF_GRANT_COUNT_NUMBER_INIT ({ 16, 12 }, { 15, 13 }, { 14, 14 }, { 13, 15 });
PSF_GRANT_COUNT_NUMBER mGrantCountNumSpe = PSF_GRANT_COUNT_NUMBER_INIT ({ 12, 16 }, { 11, 17 }, { 10, 18 }, {  9, 19 });
PSF_GRANT_COUNT_NUMBER mGrantCountNumSpf = PSF_GRANT_COUNT_NUMBER_INIT ({  8, 20 }, {  7, 21 }, {  6, 22 }, {  5, 23 });
PSF_GRANT_COUNT_NUMBER mGrantCountNumSpg = PSF_GRANT_COUNT_NUMBER_INIT ({  4, 24 }, {  3, 25 }, {  2, 26 }, {  1, 27 });

GLOBAL_REMOVE_IF_UNREFERENCED PSF_GRANT_COUNT_REG_DATA mPchGntCntRegs[] = {
  // { DevGntCnt0Base, TargetGntCntPg1Tgt0Base, GrantCountNum[PCH_PCIE_CONTROLLER_PORTS] }
  { R_VER4_PCH_S_PSF7_PCR_DEV_GNTCNT_RELOAD_DGCR0, R_VER4_PCH_S_PSF7_PCR_TARGET_GNTCNT_RELOAD_PG1_TGT0, &mGrantCountNumSpa },
  { R_VER4_PCH_S_PSF7_PCR_DEV_GNTCNT_RELOAD_DGCR0, R_VER4_PCH_S_PSF7_PCR_TARGET_GNTCNT_RELOAD_PG1_TGT0, &mGrantCountNumSpb },
  { R_VER4_PCH_S_PSF7_PCR_DEV_GNTCNT_RELOAD_DGCR0, R_VER4_PCH_S_PSF7_PCR_TARGET_GNTCNT_RELOAD_PG1_TGT0, &mGrantCountNumSpc },
  { R_VER4_PCH_S_PSF7_PCR_DEV_GNTCNT_RELOAD_DGCR0, R_VER4_PCH_S_PSF7_PCR_TARGET_GNTCNT_RELOAD_PG1_TGT0, &mGrantCountNumSpd },
  { R_VER4_PCH_S_PSF7_PCR_DEV_GNTCNT_RELOAD_DGCR0, R_VER4_PCH_S_PSF7_PCR_TARGET_GNTCNT_RELOAD_PG1_TGT0, &mGrantCountNumSpe },
  { R_VER4_PCH_S_PSF7_PCR_DEV_GNTCNT_RELOAD_DGCR0, R_VER4_PCH_S_PSF7_PCR_TARGET_GNTCNT_RELOAD_PG1_TGT0, &mGrantCountNumSpf },
  { R_VER4_PCH_S_PSF7_PCR_DEV_GNTCNT_RELOAD_DGCR0, R_VER4_PCH_S_PSF7_PCR_TARGET_GNTCNT_RELOAD_PG1_TGT0, &mGrantCountNumSpg }
};

PSF_GRANT_COUNT_NUMBER mPsfToPsfGrantCountNum = PSF_GRANT_COUNT_NUMBER_INIT ({ 9, 0 });
GLOBAL_REMOVE_IF_UNREFERENCED PSF_GRANT_COUNT_REG_DATA mPchGntCntPsfToPsf = {
  R_VER4_PCH_S_PSF1_PCR_DEV_GNTCNT_RELOAD_DGCR0, R_VER4_PCH_S_PSF1_PCR_TARGET_GNTCNT_RELOAD_PG1_TGT0, &mPsfToPsfGrantCountNum
};

//
// PCH-S PSF topology for PCIe
//
GLOBAL_REMOVE_IF_UNREFERENCED CONST PSF_TOPOLOGY mPchSPsf7PcieTreeTopo[] = {
  //{PsfId, Port}, PortType, Child, GrantCountData, PortData
  {{7, 0}, PsfPcieCtrlPort, NULL, &mPchGntCntRegs[0], {0}}, // SPA
  {{7, 1}, PsfPcieCtrlPort, NULL, &mPchGntCntRegs[1], {1}}, // SPB
  {{7, 2}, PsfPcieCtrlPort, NULL, &mPchGntCntRegs[2], {2}}, // SPC
  {{7, 3}, PsfPcieCtrlPort, NULL, &mPchGntCntRegs[3], {3}}, // SPD
  {{7, 4}, PsfPcieCtrlPort, NULL, &mPchGntCntRegs[4], {4}}, // SPE
  {{7, 5}, PsfPcieCtrlPort, NULL, &mPchGntCntRegs[5], {5}}, // SPF
  {{7, 6}, PsfPcieCtrlPort, NULL, &mPchGntCntRegs[6], {6}}, // SPG
  PSF_TOPOLOGY_END
};

GLOBAL_REMOVE_IF_UNREFERENCED CONST PSF_TOPOLOGY mPchSPsf1PcieTreeTopo[] = {
  //{PsfId, Port}, PortType, Child, GrantCountData
  {{1, 0}, PsfToPsfPort, mPchSPsf7PcieTreeTopo, &mPchGntCntPsfToPsf},
  PSF_TOPOLOGY_END
};

GLOBAL_REMOVE_IF_UNREFERENCED CONST PSF_TOPOLOGY mPchSPsfPcieTreeTopo[] = {
  //{PsfId, Port}, PortType, Child, GrantCountData
  {{0, 0}, PsfToPsfPort, mPchSPsf1PcieTreeTopo, NULL}, // Abstract root of PSF topology
  PSF_TOPOLOGY_END
};

/**
  Get PSF Pcie Tree topology

  @param[in] PsfTopology          PSF Port from PSF PCIe tree topology

  @retval PsfTopology             PSF PCIe tree topology
**/
CONST PSF_TOPOLOGY*
PsfGetRootPciePsfTopology (
  VOID
  )
{
  if (IsPchS ()) {
    return mPchSPsfPcieTreeTopo;
  }
  //@todo add PCH-LP support
  ASSERT (FALSE);
  return mPchSPsfPcieTreeTopo;
}

PSF_REG_DATA_TABLE mPchEoiRegDataTable = PSF_REG_DATA_TABLE_INIT (
  { 1, R_VER4_PCH_S_PSF1_PCR_MC_AGENT_MCAST0_TGT0_EOI, R_VER4_PCH_S_PSF1_PCR_MC_CONTROL_MCAST0_EOI, 3 },
  { 3, R_VER4_PCH_S_PSF3_PCR_MC_AGENT_MCAST0_TGT0_EOI, R_VER4_PCH_S_PSF3_PCR_MC_CONTROL_MCAST0_EOI, 1 },
  { 7, R_VER4_PCH_S_PSF7_PCR_MC_AGENT_MCAST0_TGT0_EOI, R_VER4_PCH_S_PSF7_PCR_MC_CONTROL_MCAST0_EOI, 28 }
);

/**
  Get EOI register data table for all PSFs

  @return PsfEoiRegDataTable   Pointer to PSF_EOI_REG_DATA_TABLE
**/
PSF_REG_DATA_TABLE*
PsfGetEoiRegDataTable (
  VOID
  )
{
  return &mPchEoiRegDataTable;
}

PSF_REG_DATA_TABLE mPchMctpRegDataTable = PSF_REG_DATA_TABLE_INIT (
  { 1, R_VER4_PCH_S_PSF1_PCR_MC_AGENT_MCAST1_RS0_TGT0_MCTP1, R_VER4_PCH_S_PSF1_PCR_MC_CONTROL_MCAST1_RS0_MCTP1,  3, 0 },
  { 7, R_VER4_PCH_S_PSF7_PCR_MC_AGENT_MCAST1_RS0_TGT0_MCTP1, R_VER4_PCH_S_PSF7_PCR_MC_CONTROL_MCAST1_RS0_MCTP1, 28, 0 }
);

/**
  Get MCTP register data table for all PSFs

  @return PsfEoiRegDataTable   Pointer to PSF_EOI_REG_DATA_TABLE
**/
PSF_REG_DATA_TABLE*
PsfGetMctpRegDataTable (
  VOID
  )
{
  return &mPchMctpRegDataTable;
}

/**
  Fill MCTP Targets Table

  @param[out] TargetIdTable    MCTP Targets table
  @param[in]  MaxTableSize     TargetIdTable real size
  @param[in]  PsfTable         PSF Segment Table
  @param[in]  PcieRegBaseTable Pcie Reg Base Table

  @retval Number of targets, resulting size of the table
**/
UINT32
MctpTargetsTable (
  OUT PSF_PORT_DEST_ID  *TargetIdTable,
  IN  UINT32            MaxTableSize,
  IN  PSF_SEGMENT_TABLE *PsfTable,
  IN  PSF_REG_BASE      *PcieRegBaseTable
  )
{
  UINT32       TargetNum;
  UINT32       RpIndex;
  PSF_PORT     PciePsfPort;

  if (PsfTable == NULL || PcieRegBaseTable == NULL) {
    return 0;
  }

  TargetNum = 0;
  ZeroMem (TargetIdTable, sizeof(TargetIdTable));

  if ((UINT32)(GetPchMaxPciePortNum () + 1) > MaxTableSize) {
    DEBUG ((DEBUG_ERROR, "Cannot create MctpTargetsTable - table size is too small!\n"));
    return 0;
  }

  for (RpIndex = 0; RpIndex < GetPchMaxPciePortNum (); RpIndex++) {
    PciePsfPort.RegBase = PcieRegBaseTable[RpIndex].RegBase;
    PciePsfPort.PsfDev = PsfGetDev (PsfTable, PcieRegBaseTable[RpIndex].PsfNumber);
    if (PsfIsBridgeEnabled (PciePsfPort)) {
      TargetIdTable[TargetNum] = PsfPcieDestinationId (RpIndex);
      TargetNum++;
    }
  }
  if (IsPegPresent ()) {
    TargetIdTable[TargetNum] = PsfDmiDestinationId ();
    TargetNum++;
  }
  return TargetNum;
}

/**
  P2SB PSF port Destination ID (psf_id:port_group_id:port_id:channel_id)

  @retval P2SB Destination ID
**/
PSF_PORT_DEST_ID
PsfP2sbDestinationId (
  VOID
  )
{
  PSF_PORT_DEST_ID P2sbTarget;

  P2sbTarget.RegVal = V_VER4_PCH_S_PSFX_PCR_PSF_MC_AGENT_MCAST_TGT_P2SB;

  return P2sbTarget;
}

/**
  DMI PSF port Destination ID (psf_id:port_group_id:port_id:channel_id)

  @retval DMI Destination ID
**/
PSF_PORT_DEST_ID
PsfDmiDestinationId (
  VOID
  )
{
  PSF_PORT_DEST_ID DmiTarget;

  DmiTarget.RegVal = V_VER4_PCH_PSFX_PSF_PCR_MC_AGENT_MCAST_TGT_DMI;

  return DmiTarget;
}

GLOBAL_REMOVE_IF_UNREFERENCED PSF_PORT_DEST_ID PchSRpDestId[] =
{
  {0x78000}, {0x78001}, {0x78002}, {0x78003}, // SPA: PSF7, PortID = 0
  {0x78100}, {0x78101}, {0x78102}, {0x78103}, // SPB: PSF7, PortID = 1
  {0x78200}, {0x78201}, {0x78202}, {0x78203}, // SPC: PSF7, PortID = 2
  {0x78300}, {0x78301}, {0x78302}, {0x78303}, // SPD: PSF7, PortID = 3
  {0x78400}, {0x78401}, {0x78402}, {0x78403}, // SPE: PSF7, PortID = 4
  {0x78500}, {0x78501}, {0x78502}, {0x78503}, // SPF: PSF7, PortID = 5
  {0x78600}, {0x78601}, {0x78602}, {0x78603}  // SPG: PSF7, PortID = 6
};

/**
  PCIe PSF port destination ID (psf_id:port_group_id:port_id:channel_id)

  @param[in] RpIndex        PCIe Root Port Index (0 based)

  @retval Destination ID
**/
PSF_PORT_DEST_ID
PsfPcieDestinationId (
  IN UINT32  RpIndex
  )
{
  if (RpIndex < ARRAY_SIZE (PchSRpDestId)) {
    return PchSRpDestId[RpIndex];
  }

  ASSERT (FALSE);
  return (PSF_PORT_DEST_ID){0};
}

/**
  Return PSF_PORT for CNVi device

  @retval    PsfPort         PSF PORT structure for CNVi device
**/
PSF_PORT
PsfCnviPort (
  VOID
  )
{
  PSF_PORT PsfPort;

  PsfPort.PsfDev = &Psf3Dev;
  PsfPort.RegBase = R_VER4_PCH_S_PSF3_PCR_T0_SHDW_CNVI_REG_BASE;

  return PsfPort;
}

/**
  Return PSF_PORT for PMC device

  @retval    PsfPort         PSF PORT structure for PMC device
**/
PSF_PORT
PsfPmcPort (
  VOID
  )
{
  PSF_PORT PsfPort;

  PsfPort.PsfDev = &Psf3Dev;
  PsfPort.RegBase = R_VER4_PCH_S_PSF3_PCR_T0_SHDW_PMC_REG_BASE;

  return PsfPort;
}

PSF_SEGMENT_TABLE mPchSegmentTable = PSF_SEGMENT_TABLE_INIT (
  { 1, &Psf1Dev },
  { 2, &Psf2Dev },
  { 3, &Psf3Dev },
  { 4, &Psf4Dev },
  { 5, &Psf5Dev },
  { 6, &Psf6Dev },
  { 7, &Psf7Dev }
);

/**
  Get table of supported PSF segments

  @return  PsfSegmentTable   Table of supported PSF segments
**/
PSF_SEGMENT_TABLE*
PsfGetSegmentTable (
  VOID
  )
{
  return &mPchSegmentTable;
}

PSF_EARLY_INIT_DATA mPsfEarlyInitData = {
  &mPsfPciePortData,
  &mPchEoiRegDataTable,
  &mPchMctpRegDataTable,

  //
  // Is MCTP supported
  //
  TRUE
};

/**
  Return PSF_EARLY_INIT_DATA structure to Early Init Psf

  @retval Psf Early Init Data
**/
PSF_EARLY_INIT_DATA*
PsfGetEarlyInitData (
  VOID
  )
{
  return &mPsfEarlyInitData;
}

PSF_PORT_RELAXED_ORDERING_CONFIG_REG  mPsfPortRelaxedOrderingConfigRegs[] =
{
  { &Psf1Dev, R_VER4_PCH_PSF_PCR_PORT_CONFIG_PG1_PORT1, B_PCH_PSF_PCR_PORT_CONFIG_EGRESS_FRO },
  { &Psf1Dev, R_VER4_PCH_PSF_PCR_PORT_CONFIG_PG1_PORT2, B_PCH_PSF_PCR_PORT_CONFIG_EGRESS_FRO },
  { &Psf1Dev, R_VER4_PCH_PSF_PCR_PORT_CONFIG_PG1_PORT3, B_PCH_PSF_PCR_PORT_CONFIG_EGRESS_FRO },
  { &Psf2Dev, R_VER4_PCH_PSF_PCR_PORT_CONFIG_PG1_PORT0, B_PCH_PSF_PCR_PORT_CONFIG_EGRESS_FRO },
  { &Psf2Dev, R_VER4_PCH_PSF_PCR_PORT_CONFIG_PG1_PORT1, B_PCH_PSF_PCR_PORT_CONFIG_EGRESS_FRO },
  { &Psf2Dev, R_VER4_PCH_PSF_PCR_PORT_CONFIG_PG1_PORT2, B_PCH_PSF_PCR_PORT_CONFIG_EGRESS_FRO },
  { &Psf2Dev, R_VER4_PCH_PSF_PCR_PORT_CONFIG_PG1_PORT3, B_PCH_PSF_PCR_PORT_CONFIG_EGRESS_FRO },
  { &Psf3Dev, R_VER4_PCH_PSF_PCR_PORT_CONFIG_PG1_PORT3, B_PCH_PSF_PCR_PORT_CONFIG_EGRESS_FRO },
  { &Psf1Dev, R_VER4_PCH_PSF_PCR_PORT_CONFIG_PG0_PORT0, B_PCH_PSF_PCR_PORT_CONFIG_INGRESS_FRO },
  { &Psf2Dev, R_VER4_PCH_PSF_PCR_PORT_CONFIG_PG0_PORT0, B_PCH_PSF_PCR_PORT_CONFIG_INGRESS_FRO },
  { &Psf3Dev, R_VER4_PCH_PSF_PCR_PORT_CONFIG_PG0_PORT0, B_PCH_PSF_PCR_PORT_CONFIG_INGRESS_FRO },
  { &Psf4Dev, R_VER4_PCH_PSF_PCR_PORT_CONFIG_PG0_PORT0, B_PCH_PSF_PCR_PORT_CONFIG_INGRESS_FRO },
  { &Psf6Dev, R_VER4_PCH_PSF_PCR_PORT_CONFIG_PG0_PORT0, B_PCH_PSF_PCR_PORT_CONFIG_INGRESS_FRO },
  { &Psf7Dev, R_VER4_PCH_PSF_PCR_PORT_CONFIG_PG0_PORT0, B_PCH_PSF_PCR_PORT_CONFIG_INGRESS_FRO }
};

PSF_RELAXED_ORDER_REGS mPsfPortRelaxedOrderingConfigRegsStruct;

/**
  This function returns Psf Port Relaxed Ordering Configs

  @retval struct containing tables of registers for programming of Relaxed Ordering
**/
PSF_RELAXED_ORDER_REGS*
GetPsfPortRelaxedOrderingTables (
  VOID
  )
{
  mPsfPortRelaxedOrderingConfigRegsStruct.RegsTable = mPsfPortRelaxedOrderingConfigRegs;
  mPsfPortRelaxedOrderingConfigRegsStruct.RegsTableSize = ARRAY_SIZE (mPsfPortRelaxedOrderingConfigRegs);
  mPsfPortRelaxedOrderingConfigRegsStruct.RegsPchTypeSpecific = NULL;
  mPsfPortRelaxedOrderingConfigRegsStruct.RegsPchTypeSpecificTableSize = 0;

  return &mPsfPortRelaxedOrderingConfigRegsStruct;
}

/**
  Return RC_OWNER value to program

  @retval RC_OWNER
**/
UINT32
PsfGetRcOwner (
  )
{
  return V_VER4_PCH_PSFX_PCR_RC_OWNER_PMT;
}

/**
  Configures rootspace 3 bus number for PCIe IMR use

  @param[in] Rs3Bus        bus number
**/
VOID
PsfSetRs3Bus (
  UINT8 Rs3Bus
  )
{
  PchPcrWrite32 (PID_PSF1, R_PCH_PSF_PCR_BUS_SHADOW_RS3, Rs3Bus);
}

/**
  Enable VTd support in PSF.

**/
VOID
PchPsfEnableVtd (
  VOID
  )
{
  PsfEnableVtd (PsfGetSegmentTable ());
}
