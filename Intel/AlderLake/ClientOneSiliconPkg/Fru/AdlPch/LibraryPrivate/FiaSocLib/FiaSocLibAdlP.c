/** @file
  This file contains ADL-P specific FIA routines

@copyright
  INTEL CONFIDENTIAL
  Copyright 2020 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains an 'Intel Peripheral Driver' and is uniquely identified as
  "Intel Reference Module" and is licensed for Intel CPUs and chipsets under
  the terms of your license agreement with Intel or your vendor. This file may
  be modified by the user, subject to additional terms of the license agreement.

@par Specification Reference:
**/

#include <Uefi/UefiBaseType.h>
#include <Library/DebugLib.h>
#include <Register/PchPcrRegs.h>
#include <Register/PchFiaRegs.h>
#include <Register/PchPcrRegsAdpS.h>
#include <Library/FiaSocLib.h>
#include <Library/PchFia15.h>
#include <Library/PchFiaLib.h>
#include <Library/PchInfoLib.h>
#include <Library/P2SbSocLib.h>
#include <Library/P2SbSidebandAccessLib.h>
#include <Library/PeiPcieRpInitLib.h>

/**
  Returns the FIA Instance

  @param[out]  SbAccess            P2SB Register Access pointer
  @param[out]  P2SbController      P2SB controller pointer

  @return FIA Instance
**/
FIA_INSTANCE
FiaGetInstance (
  OUT P2SB_SIDEBAND_REGISTER_ACCESS  *SbAccess,
  OUT P2SB_CONTROLLER                *P2SbController
  )
{
  FIA_INSTANCE FiaInst;
  EFI_STATUS   Status;

  Status = GetP2SbController (P2SbController);
  ASSERT_EFI_ERROR (Status);
  if (EFI_ERROR (Status)) {
    FiaInst.RegisterAccess = NULL;
    return FiaInst;
  }

  BuildP2SbSidebandAccess (
      P2SbController,
      PID_FIA,
      0,
      P2SbMemory,
      P2SbMmioAccess,
      FALSE,
      SbAccess
      );
  FiaInst.RegisterAccess = (REGISTER_ACCESS *) SbAccess;

  return FiaInst;
}

/**
  Returns a FIA lane number for a given SATA port.

  @param[in]  SataCtrlIndex  SATA controller index
  @param[in]  SataPortIndex  SATA port index
  @param[out] LaneNum        Pointer to the variable that stores lane number.
                             The output value is only valid if this function returns TRUE.

  @return TRUE if given SATA port owns FIA lane, FALSE otherwise
**/
BOOLEAN
PchFiaGetSataLaneNum (
  IN  UINT32  SataCtrlIndex,
  IN  UINT32  SataPortIndex,
  OUT UINT8   *LaneNum
  )
{
  FIA_INSTANCE                   FiaInst;
  P2SB_SIDEBAND_REGISTER_ACCESS  FiaSbAccess;
  P2SB_CONTROLLER                P2SbController;

  FiaInst = FiaGetInstance (&FiaSbAccess, &P2SbController);
  if (IsPchLp ()) {
    switch (SataPortIndex) {
      case 0:
        *LaneNum = 10;
        break;
      case 1:
        *LaneNum = 11;
        break;
      default:
        ASSERT (FALSE);
        return FALSE;
    }
  } else {
    ASSERT (IsPchH ());
    switch (SataPortIndex) {
      case 0:
        if (PchFiaGetLaneOwner (FiaInst, 24) == PchFiaOwnerSata) {
          *LaneNum = 24;
          return TRUE;
        } else if (PchFiaGetLaneOwner (FiaInst, 26) == PchFiaOwnerSata) {
          *LaneNum = 26;
          return TRUE;
        }
        return FALSE;
      case 1:
        if (PchFiaGetLaneOwner (FiaInst, 25) == PchFiaOwnerSata) {
          *LaneNum = 25;
          return TRUE;
        } else if (PchFiaGetLaneOwner (FiaInst, 27) == PchFiaOwnerSata) {
          *LaneNum = 27;
          return TRUE;
        }
        return FALSE;
      case 2:
        *LaneNum = 28;
        break;
      case 3:
        *LaneNum = 29;
        break;
      case 4:
        *LaneNum = 30;
        break;
      case 5:
        *LaneNum = 31;
        break;
      case 6:
        *LaneNum = 32;
        break;
      case 7:
        *LaneNum = 33;
        break;
      default:
        ASSERT (FALSE);
        return FALSE;
    }
  }

  return (PchFiaGetLaneOwner (FiaInst, *LaneNum) == PchFiaOwnerSata);
}

/**
  Returns a FIA lane number for a GbE controller

  @param[out] LaneNum  FIA lane number that is owned by GbE

  @retval TRUE   Found FIA lane assigned to GbE
  @retval FALSE  No lanes assigned to GbE
**/
BOOLEAN
PchFiaGetGbeLaneNum (
  OUT UINT8  *LaneNum
  )
{
  FIA_INSTANCE                   FiaInst;
  P2SB_SIDEBAND_REGISTER_ACCESS  FiaSbAccess;
  P2SB_CONTROLLER                P2SbController;
  UINT8                          GbeLanesLp[] = {6, 7, 8};
  UINT8                          GbeLanesH[] = {10, 22, 25, 26};
  UINT8                          *GbeLanes;
  UINT8                          GbeLanesSize;
  UINT8                          LaneIndex;

  FiaInst = FiaGetInstance (&FiaSbAccess, &P2SbController);
  if (IsPchLp ()) {
    GbeLanes = GbeLanesLp;
    GbeLanesSize = ARRAY_SIZE (GbeLanesLp);
  } else {
    ASSERT (IsPchH ());
    GbeLanes = GbeLanesH;
    GbeLanesSize = ARRAY_SIZE (GbeLanesH);
  }

  //
  // Since there is only one instance of GbE controller on the system
  // we return first lane that is owned by GbE.
  //
  for (LaneIndex = 0; LaneIndex < GbeLanesSize; LaneIndex++) {
    if (PchFiaGetLaneOwner (FiaInst, GbeLanes[LaneIndex]) == PchFiaOwnerGbe) {
      *LaneNum = GbeLanes[LaneIndex];
      return TRUE;
    }
  }

  DEBUG ((DEBUG_INFO, "No FIA lanes assigned as GbE\n"));
  return FALSE;
}

/**
  Checks if given lane is DMI

  @param[in]  FiaLaneNum  Fia lane num

  @return TRUE if given lane is DMI, FALSE otherwise
**/
BOOLEAN
PchFiaIsLaneDmi (
  IN UINT8  FiaLaneNum
  )
{
  if (IsPchH () && (FiaLaneNum >= 14) && (FiaLaneNum <= 21)) {
    return TRUE;
  }

  return FALSE;
}

/**
  Returns a FIA lane number for a given PCIe lane.

  @param[in]  PciePhysicalLane  Index of the PCIe lane
  @param[out] LaneNum           Pointer to the variable that stores lane number.
                                The output value is only valid if this function returns TRUE.

  @return TRUE if given PciePhysicalLane owns FIA lane, FALSE otherwise
**/
BOOLEAN
PchFiaGetPcieLaneNum (
  IN  UINT32  PciePhysicalLane,
  OUT UINT8   *LaneNum
  )
{
  FIA_INSTANCE                   FiaInst;
  P2SB_SIDEBAND_REGISTER_ACCESS  FiaSbAccess;
  P2SB_CONTROLLER                P2SbController;
  static UINT8                   FirstLaneH[]  = { 6, 10, 22, 26, 30, 34 };
  UINT32                         ControllerIndex;
  UINT32                         ControllerPhyLane;

  FiaInst = FiaGetInstance (&FiaSbAccess, &P2SbController);

  if (IsPchLp ()) {
    *LaneNum = (UINT8) PciePhysicalLane;
  } else {
    ASSERT (IsPchH ());
    ControllerIndex = PciePhysicalLane / 4;
    ControllerPhyLane = PciePhysicalLane % 4;
    *LaneNum = FirstLaneH[ControllerIndex] + (UINT8) ControllerPhyLane;
  }

  return (PchFiaGetLaneOwner (FiaInst, *LaneNum) == PchFiaOwnerPcie);
}

/**
  Returns a FIA lane number for a given PCIe root port
  This function handles PCIe lane reversal internaly.

  @param[in]  RpIndex       PCIe root port index
  @param[in]  RpLaneIndex   Root port lane index within given root port
  @param[out] LaneNum       Pointer to the variable that stores lane number.
                            The output value is only valid if this function returns TRUE.

  @return TRUE if given RpLane owns FIA lane, FALSE otherwise
**/
BOOLEAN
PchFiaGetPcieRootPortLaneNum (
  IN  UINT32  RpIndex,
  IN  UINT32  RpLaneIndex,
  OUT UINT8  *LaneNum
  )
{
  return PchFiaGetPcieLaneNum (PchPciePhysicalLane (RpIndex, RpLaneIndex), LaneNum);
}

/**
  Returns a FIA lane number for a given USB3 port.
  This function assumes that for ICP-Lp and ICP-H USB3 ports to FIA lane mapping is 1:1

  @param[in]  Usb3PortIndex  USB3 port index
  @param[out] LaneNum        Pointer to the variable that stores lane number.
                             The output value is only valid if this function returns TRUE.

  @return TRUE if given USB3 port owns FIA lane, FALSE otherwise
**/
BOOLEAN
PchFiaGetUsb3LaneNum (
  IN  UINT32  Usb3PortIndex,
  OUT UINT8   *LaneNum
  )
{
  FIA_INSTANCE                   FiaInst;
  P2SB_SIDEBAND_REGISTER_ACCESS  FiaSbAccess;
  P2SB_CONTROLLER                P2SbController;

  FiaInst = FiaGetInstance (&FiaSbAccess, &P2SbController);
  ASSERT (Usb3PortIndex < GetPchXhciMaxUsb3PortNum ());
  *LaneNum = (UINT8) Usb3PortIndex;
  return (PchFiaGetLaneOwner (FiaInst, *LaneNum) == PchFiaOwnerUsb3);
}

/**
  Returns a FIA lane number for a given UFS lane.

  @param[in]  UfsControllerIndex  Index of the UFS controller
  @param[in]  UfsLaneIndex        Index of the UFS lane on given controller
  @param[out] LaneNum             Optional. Pointer to the variable that stores lane number.
                                  The output value is only valid if this function returns TRUE.

 @return TRUE if given UFS lane owns FIA lane, FALSE otherwise
**/
BOOLEAN
PchFiaGetUfsLaneNum (
  IN  UINT32  UfsControllerIndex,
  IN  UINT32  UfsLaneIndex,
  OUT UINT8   *LaneNum
  )
{
  FIA_INSTANCE                   FiaInst;
  P2SB_SIDEBAND_REGISTER_ACCESS  FiaSbAccess;
  P2SB_CONTROLLER                P2SbController;
  UINT8                          LaneNumLocal;

  if (IsPchLp ()) {
    switch (UfsControllerIndex) {
      case 0:
        switch (UfsLaneIndex) {
          case 0:
            LaneNumLocal = 6;
            break;
          case 1:
            LaneNumLocal = 7;
            break;
          default:
            ASSERT (FALSE);
            return FALSE;
        };
        break;
      case 1:
        switch (UfsLaneIndex) {
          case 0:
            LaneNumLocal = 8;
            break;
          case 1:
            LaneNumLocal = 9;
            break;
          default:
            ASSERT (FALSE);
            return FALSE;
        };
        break;
      default:
        ASSERT (FALSE);
        return FALSE;
    }
  } else if (IsPchH ()) {
    switch (UfsControllerIndex) {
      case 0:
        switch (UfsLaneIndex) {
          case 0:
            LaneNumLocal = 30;
            break;
          case 1:
            LaneNumLocal = 31;
            break;
          default:
            ASSERT (FALSE);
            return FALSE;
        };
        break;
      case 1:
        switch (UfsLaneIndex) {
          case 0:
            LaneNumLocal = 32;
            break;
          case 1:
            LaneNumLocal = 33;
            break;
          default:
            ASSERT (FALSE);
            return FALSE;
        };
        break;
      default:
        ASSERT (FALSE);
        return FALSE;
    }
  } else {
    ASSERT (FALSE);
    return FALSE;
  }

  if (LaneNum != NULL) {
    *LaneNum = LaneNumLocal;
  }
  FiaInst = FiaGetInstance (&FiaSbAccess, &P2SbController);
  return (PchFiaGetLaneOwner (FiaInst, LaneNumLocal) == PchFiaOwnerUfs);
}

/**
  Returns a FIA lane number for a given TSN port.

  @param[in]  TsnPortIndex  TSN port index
  @param[out] LaneNum       Pointer to the variable that stores lane number.
                            The output value is only valid if this function returns TRUE.

  @return TRUE if given TSN port owns FIA lane, FALSE otherwise
**/
BOOLEAN
PchFiaGetTsnLaneNum (
  IN  UINT32  TsnPortIndex,
  OUT UINT8   *LaneNum
  )
{
  FIA_INSTANCE                   FiaInst;
  P2SB_SIDEBAND_REGISTER_ACCESS  FiaSbAccess;
  P2SB_CONTROLLER                P2SbController;

  FiaInst = FiaGetInstance (&FiaSbAccess, &P2SbController);

  DEBUG ((DEBUG_INFO, "PchFiaGetTsnLaneNum() - Entry\n"));

  if (IsPchLp ()) {
    if ( TsnPortIndex == 0) {
      if (PchFiaGetLaneOwner (FiaInst, 6) == PchFiaOwnerTsn) {
        *LaneNum = 6;
        return TRUE;
      } else if (PchFiaGetLaneOwner (FiaInst, 7) == PchFiaOwnerTsn) {
        *LaneNum = 7;
        return TRUE;
      }
    }
    DEBUG ((DEBUG_INFO, "No FIA lanes assigned as TSN\n"));
  } else if (IsPchH ()) {
    switch (TsnPortIndex) {
      case 0: //SoC Gbe TSN0
        if (PchFiaGetLaneOwner (FiaInst, 22) == PchFiaOwnerTsn) {
          *LaneNum = 22;
          return TRUE;
        }
        break;
      case 1: //SoC Gbe TSN1
        if (PchFiaGetLaneOwner (FiaInst, 23) == PchFiaOwnerTsn) {
          *LaneNum = 23;
          return TRUE;
        }
        break;
      default:
        DEBUG ((DEBUG_INFO, "No FIA lanes assigned as TSN\n"));
    }
    DEBUG ((DEBUG_INFO, "No FIA lanes assigned as TSN\n"));
  } else {
    DEBUG ((DEBUG_INFO, "No FIA lanes assigned as TSN\n"));
  }
  return FALSE;
}

/**
  Returns number of FIA lanes

  @return Number of FIA lanes
**/
UINT8
PchFiaGetMaxLaneNum (
  VOID
  )
{
  return 12;
}


/**
  Returns number of FIA lanes

  @param[in] FiaInst  FIA Instance

  @return Number of FIA lanes
**/
UINT8
PchFiaGetMaxLaneNumEx (
  IN  FIA_INSTANCE FiaInst OPTIONAL
  )
{
  P2SB_SIDEBAND_REGISTER_ACCESS  *FiaSbAccess;

  if (IsPchLp ()) {
    return 12;
  } else {
    ASSERT (IsPchS ());
    FiaSbAccess = (P2SB_SIDEBAND_REGISTER_ACCESS *) FiaInst.RegisterAccess;
    switch (FiaSbAccess->P2SbPid) {
      case PID_FIAPGS:
        return 20;
      case PID_FIAPD:
        return 16;
      case PID_FIAU:
        return 10;
      default:
        ASSERT (FALSE);
        DEBUG ((DEBUG_INFO, "Invalid FIA Instance \n"));
        return 0;
    }
  }
}

/**
  Return FIA lane owner.

  @param[in] FiaInst  FIA Instance
  @param[in] LaneNum  FIA lane number

  @return  Code of the FIA lane owner, PchFiaOwnerInvalid if lane number wasn't valid
**/
PCH_FIA_LANE_OWNER
PchFiaGetLaneOwner (
  IN  FIA_INSTANCE FiaInst,
  IN  UINT8        LaneNum
  )
{
  if (LaneNum >= PchFiaGetMaxLaneNum ()) {
    ASSERT (FALSE);
    return PchFiaOwnerInvalid;
  }
  return PchFia15GetLaneOwner (FiaInst, LaneNum);
}

/**
  Print FIA LOS registers.
**/
VOID
PchFiaPrintLosRegisters (
  VOID
  )
{
  FIA_INSTANCE                   FiaInst;
  P2SB_SIDEBAND_REGISTER_ACCESS  FiaSbAccess;
  P2SB_CONTROLLER                P2SbController;

  FiaInst = FiaGetInstance (&FiaSbAccess, &P2SbController);
  PchFia15PrintLosRegisters (FiaInst);
}

/**
  Assigns CLKREQ# to PCH PCIe ports

  @param[in] ClkReqMap      Mapping between PCH PCIe ports and CLKREQ#
  @param[in] ClkReqMapSize  Size of the map
**/
VOID
PchFiaAssignPchPciePortsClkReq (
  IN UINT8  *ClkReqMap,
  IN UINT8  ClkReqMapSize
  )
{
  FIA_INSTANCE                   FiaInst;
  P2SB_SIDEBAND_REGISTER_ACCESS  FiaSbAccess;
  P2SB_CONTROLLER                P2SbController;

  FiaInst = FiaGetInstance (&FiaSbAccess, &P2SbController);
  PchFia15AssignPchPciePortsClkReq (FiaInst, ClkReqMap, ClkReqMapSize);
}

/**
  Disables ClkReq mapping for PCIe root port

  @param[in] FiaInst  FIA Instance
  @param[in] RpIndex  Root port index
**/
VOID
PchFiaDisablePchPciePortClkReq (
  IN UINT32  RpIndex
  )
{
  FIA_INSTANCE                   FiaInst;
  P2SB_SIDEBAND_REGISTER_ACCESS  FiaSbAccess;
  P2SB_CONTROLLER                P2SbController;

  FiaInst = FiaGetInstance (&FiaSbAccess, &P2SbController);
  PchFia15DisablePchPciePortClkReq (FiaInst, RpIndex);
}

/**
  Assigns CLKREQ# to CPU PCIe ports

  @param[in] RpIndex        CPU PCIe root port index
  @param[in] ClkReqNum      Number of the CLKREQ
**/
VOID
PchFiaAssignCpuPciePortClkReq (
  IN UINT32  RpIndex,
  IN UINT8   ClkReqNum
  )
{
  FIA_INSTANCE                   FiaInst;
  P2SB_SIDEBAND_REGISTER_ACCESS  FiaSbAccess;
  P2SB_CONTROLLER                P2SbController;

  FiaInst = FiaGetInstance (&FiaSbAccess, &P2SbController);
  PchFia15AssignCpuPciePortClkReq (FiaInst, RpIndex, ClkReqNum);
}

/**
Enable CLKREQ# to CPU PCIe ports

@param[in] RpIndex        CPU PCIe root port index
@param[in] ClkReqNum      Number of the CLKREQ
**/
VOID
PchFiaEnableCpuPciePortClkReq (
  IN UINT32  RpIndex,
  IN UINT8   ClkReqNum
)
{
  FIA_INSTANCE                   FiaInst;
  P2SB_SIDEBAND_REGISTER_ACCESS  FiaSbAccess;
  P2SB_CONTROLLER                P2SbController;

  DEBUG ((DEBUG_INFO, "PchFiaEnableCpuPciePortClkReq Entry\n"));
  FiaInst = FiaGetInstance (&FiaSbAccess, &P2SbController);
  PchFia15EnableCpuPciePortClkReq (FiaInst, RpIndex, ClkReqNum);
  DEBUG ((DEBUG_INFO, "PchFiaEnableCpuPciePortClkReq Exit\n"));
}

/**
  Return the status of the CLKREQ state received with VW msg.

  @param[in] RpIndex  CPU PCIe index.

  @return Status of the CLKREQ.
**/
BOOLEAN
PchFiaGetCpuPcieClkReqStatus (
  IN UINT32  RpIndex
  )
{
  FIA_INSTANCE                   FiaInst;
  P2SB_SIDEBAND_REGISTER_ACCESS  FiaSbAccess;
  P2SB_CONTROLLER                P2SbController;

  FiaInst = FiaGetInstance (&FiaSbAccess, &P2SbController);
  return PchFia15GetCpuPcieClkReqStatus (FiaInst, RpIndex);
}

/**
  Assigns CLKREQ# to GbE

  @param[in]  ClkReqNum  CLKREQ# number
**/
VOID
PchFiaAssignGbeClkReq (
  IN UINT8  ClkReqNum
  )
{
  FIA_INSTANCE                   FiaInst;
  P2SB_SIDEBAND_REGISTER_ACCESS  FiaSbAccess;
  P2SB_CONTROLLER                P2SbController;

  FiaInst = FiaGetInstance (&FiaSbAccess, &P2SbController);
  PchFia15AssignGbeClkReq (FiaInst, ClkReqNum);
}

/**
  Configures lower bound of delay between ClkReq assertion and driving RefClk.
**/
VOID
PchFiaSetClockOutputDelay (
  VOID
  )
{
  FIA_INSTANCE                   FiaInst;
  P2SB_SIDEBAND_REGISTER_ACCESS  FiaSbAccess;
  P2SB_CONTROLLER                P2SbController;

  FiaInst = FiaGetInstance (&FiaSbAccess, &P2SbController);
  PchFia15SetClockOutputDelay (FiaInst);
}

/**
  Performs FIA programming required at the end of configuration and locks lockable FIA registers
**/
VOID
PchFiaFinalizeConfigurationAndLock (
  VOID
  )
{
  FIA_INSTANCE                   FiaInst;
  P2SB_SIDEBAND_REGISTER_ACCESS  FiaSbAccess;
  P2SB_CONTROLLER                P2SbController;
  UINT32                         Data32Or;

  FiaInst = FiaGetInstance (&FiaSbAccess, &P2SbController);

  //
  // Program PCR[FIA] + 20h bit [13:11] to 001, bit [30:28] to [000]
  //
  FiaInst.RegisterAccess->AndThenOr32 (
    FiaInst.RegisterAccess,
    R_PCH_FIA_PCR_PLLCTL,
    (UINT32) ~(B_PCH_FIA_PCR_PLLCTL_CL0PLLWAIT | B_PCH_FIA_PCR_PLLCTL_PLLCLKVADT),
    (UINT32) (1 << N_PCH_FIA_PCR_PLLCTL_CL0PLLWAIT)
    );

  //
  // Program PCR[FIA] + 18h bit [17:16, 1:0] to [00, 00]
  //
  FiaInst.RegisterAccess->AndThenOr32 (
    FiaInst.RegisterAccess,
    R_PCH_FIA_PCR_CLSDM,
    (UINT32) ~(B_PCH_FIA_PCR_CLSDM_DMIIPSLSD | B_PCH_FIA_PCR_CLSDM_PCIEGBEIPSLSD),
    0
    );

  //
  // Set PCR[FIA] + 0h bit [17, 16, 15] to [1, 1, 1]
  //
  FiaInst.RegisterAccess->AndThenOr32 (
    FiaInst.RegisterAccess,
    R_PCH_FIA_PCR_CC,
    ~0u,
    B_PCH_FIA_PCR_CC_PTOCGE | B_PCH_FIA_PCR_CC_OSCDCGE | B_PCH_FIA_PCR_CC_SCPTCGE
    );

  //
  // Set PCR[FIA] + 40h bit [3] to [1]
  //
  Data32Or = B_PCH_FIA_PCR_PMC_PRDPGE;
  FiaInst.RegisterAccess->AndThenOr32 (
    FiaInst.RegisterAccess,
    R_PCH_FIA_PCR_PMC,
    ~0u,
    Data32Or
    );

  //
  // Set PCR[FIA] + 48h bit [0] to [0]
  //
  FiaInst.RegisterAccess->AndThenOr32 (
    FiaInst.RegisterAccess,
    R_PCH_FIA_PCR_PGCUC,
    (UINT32) ~(B_PCH_FIA_PCR_PGCUC_ACC_CLKGATE_DISABLED),
    0
    );

  //
  // Set PCR[FIA] + 50h bit [0] to [0]
  //
  FiaInst.RegisterAccess->AndThenOr32 (
    FiaInst.RegisterAccess,
    R_PCH_FIA_PCR_PGCUCSOC,
    (UINT32) ~(B_PCH_FIA_PCR_PGCUCSOC_ACC_CLKGATE_DISABLED),
    0
    );
}

/**
  Returns number of FIA LOS registers used

  @param[in] FiaIndex        FIA index to specify FIA instance

  @return Number of FIA LOS registers used
**/
UINT32
PchFiaGetMaxLosRegister (
  FIA_INDEX FiaIndex
  )
{
  ASSERT (FiaIndex == FiaIndexFia);
  return (((PchFiaGetMaxLaneNum () - 1) / 8) + 1);
}

/**
  Get FIA LOS register value

  @param[in] FiaIndex        FIA index to specify FIA instance
  @param[in] LosIndex        LOS FIA register index

  @return  LOS FIA register value
**/
UINT32
PchFiaGetLos (
  FIA_INDEX FiaIndex,
  UINT32    LosIndex
  )
{
  UINT32                         MaxLosRegister;
  FIA_INSTANCE                   FiaInst;
  P2SB_SIDEBAND_REGISTER_ACCESS  FiaSbAccess;
  P2SB_CONTROLLER                P2SbController;

  ASSERT (FiaIndex == FiaIndexFia);
  FiaInst = FiaGetInstance (&FiaSbAccess, &P2SbController);

  MaxLosRegister = ((PchFiaGetMaxLaneNum () - 1) / 8) + 1;
  if (LosIndex > MaxLosRegister) {
    ASSERT (FALSE);
    LosIndex = MaxLosRegister;
  }

  return FiaInst.RegisterAccess->Read32 (FiaInst.RegisterAccess, R_PCH_FIA_15_PCR_LOS1_REG_BASE + (4 * LosIndex));
}

/**
  Returns the unassigned lanes

  @param[in]  LaneNum

  @return Unassigned lane in bitwise representation.
**/
UINT32
PchFiaGetUnassignLane (
  )
{
  FIA_INSTANCE                   FiaInst;
  P2SB_SIDEBAND_REGISTER_ACCESS  FiaSbAccess;
  P2SB_CONTROLLER                P2SbController;
  UINT32                         LaneUnassigned;
  UINT8                          MaxLanes;
  UINT8                          Lane;

  FiaInst = FiaGetInstance (&FiaSbAccess, &P2SbController);

  //
  // Initialize the unassign lane to 0.
  //
  LaneUnassigned = 0;

  if (IsPchLp () || IsPchP () || IsPchN ()) {
    MaxLanes = PchFiaGetMaxLaneNum ();
    for (Lane = 0; Lane < MaxLanes; ++Lane) {
      if (PchFiaGetLaneOwner (FiaInst, Lane) == PchFiaOwnerInvalid) {
        LaneUnassigned |= (1 << Lane);
      }
    }
  }
  return LaneUnassigned;
}

/**
  Checks if a given PCIe lane is assigned any FIA lane

  @param[in]  RpIndex      PCIe root port index
  @param[in]  RpLaneIndex  Root port lane index within given root port

  @return  TRUE if given PCIe lane is assigned a FIA lane, FALSE otherwise
**/
BOOLEAN
PchFiaIsPcieRootPortLaneConnected (
  IN UINT32  RpIndex,
  IN UINT32  RpLaneIndex
  )
{
  UINT8  LaneNum;
  return PchFiaGetPcieRootPortLaneNum (RpIndex, RpLaneIndex, &LaneNum);
}

/**
  Checks if a given SATA port is assigned any FIA lane

  @param[in] SataCtrlIndex  SATA controller index
  @param[in] SataPortIndex  SATA port index

  @return TRUE if given SATA port is assigned a FIA lane, FALSE otherwise
**/
BOOLEAN
PchFiaIsSataPortConnected (
  IN UINT32  SataCtrlIndex,
  IN UINT32  SataPortIndex
  )
{
  UINT8  LaneNum;
  return PchFiaGetSataLaneNum (SataCtrlIndex, SataPortIndex, &LaneNum);
}

/**
  Checks if a given USB3 port is assigned any FIA lane

  @param[in]  Usb3PortIndex  USB3 port index

  @return TRUE if given USB3 port is assigned a FIA lane, FALSE otherwise
**/
BOOLEAN
PchFiaIsUsb3PortConnected (
  IN UINT32  Usb3PortIndex
  )
{
  UINT8 LaneNum;
  return PchFiaGetUsb3LaneNum (Usb3PortIndex, &LaneNum);
}

/**
  Check if UFS lane is assigned a FIA lane

  @param[in] UfsControllerIndex  UFS controller index
  @param[in] UfsLaneIndex        Lane index local to given UFS controller

  @retval TRUE  UFS lane is assigned a FIA lane
  @retval FALSE  UFS lane is not assigned any FIA lane
**/
BOOLEAN
PchFiaIsUfsLaneConnected (
  IN UINT32  UfsControllerIndex,
  IN UINT32  UfsLaneIndex
  )
{
  return PchFiaGetUfsLaneNum (UfsControllerIndex, UfsLaneIndex, NULL);
}
