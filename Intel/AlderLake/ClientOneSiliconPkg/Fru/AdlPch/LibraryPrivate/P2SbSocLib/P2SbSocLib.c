/** @file
  P2SB SoC library.
  All function in this library is available for PEI, DXE, and SMM,
  But do not support UEFI RUNTIME environment call.

@copyright
  INTEL CONFIDENTIAL
  Copyright 2020 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains an 'Intel Peripheral Driver' and is uniquely identified as
  "Intel Reference Module" and is licensed for Intel CPUs and chipsets under
  the terms of your license agreement with Intel or your vendor. This file may
  be modified by the user, subject to additional terms of the license agreement.

@par Specification Reference:
**/

#include <Base.h>
#include <Uefi/UefiBaseType.h>
#include <Library/BaseMemoryLib.h>
#include <Library/BaseLib.h>
#include <Library/PchPciBdfLib.h>
#include <Library/P2SbSocLib.h>
#include <Library/PcdLib.h>
#include <Register/PchRegs.h>
#include <PchReservedResources.h>

/**
  Get P2SB instance

  @param[in, out] P2SbController      P2SB controller pointer

  @retval     EFI_SUCCESS           - Completed successfully
              EFI_INVALID_PARAMETER - P2SbController is NULL
**/
EFI_STATUS
GetP2SbController (
  IN OUT P2SB_CONTROLLER      *P2SbController
  )
{
  if (P2SbController == NULL) {
    return EFI_INVALID_PARAMETER;
  }

  ZeroMem (P2SbController, sizeof (P2SB_CONTROLLER));
  P2SbController->PciCfgBaseAddr           = P2sbPciCfgBase ();
  P2SbController->Mmio                     = PCH_PCR_BASE_ADDRESS;
  P2SbController->HpetMmio                 = PcdGet32 (PcdSiHpetBaseAddress);

  return EFI_SUCCESS;
}

