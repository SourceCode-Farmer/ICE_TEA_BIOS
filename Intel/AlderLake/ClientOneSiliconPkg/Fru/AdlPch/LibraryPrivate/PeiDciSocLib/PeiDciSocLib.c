/** @file
  DCI SoC library.
  All function in this library is available for PEI

@copyright
  INTEL CONFIDENTIAL
  Copyright 2020 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains an 'Intel Peripheral Driver' and is uniquely identified as
  "Intel Reference Module" and is licensed for Intel CPUs and chipsets under
  the terms of your license agreement with Intel or your vendor. This file may
  be modified by the user, subject to additional terms of the license agreement.

@par Specification Reference:
**/

#include <Base.h>
#include <Library/PchInfoLib.h>
#include <Library/PeiDciSocLib.h>
#include <Library/DciPrivateLib.h>
#include <ConfigBlock/SiPreMemConfig.h>

/**
  DCI Soc specific configuration

  @param[in]  *DciPreMemConfig      Instance of PCH_DCI_PREMEM_CONFIG
  @param[in]  PlatformDebugConsent  Platform Debug Consent type
**/
VOID
DciSocConfigure (
  IN PCH_DCI_PREMEM_CONFIG          *DciPreMemConfig,
  IN UINT8                          PlatformDebugConsent
  )
{
  if (!IsPchS ()) {
    if (IsDciDebugEnabled ()) {
      DciDisClkReq ();
      //
      // When 4-wire DCI OOB is connected or debug consent is high-power, enable modphy power-gate override which will not power-gate DCI modphy to allow 4-wire DCI OOB connection
      // When 4-wire DCI OOB is not connected but debug is enabled, disable modphy power-gate override to ensure platform is able to enter s0ix.
      //
      if (Is4wireDciOobConnected () || (PlatformDebugConsent == ProbeTypeDciOob)) {
        DciEnModphyPgOverride ();
      } else {
        DciDisModphyPgOverride ();
      }
    } else {
      //
      // debug is not enabled, restore to silicon HW default
      //
      DciEnClkReq ();
      DciEnModphyPgOverride ();
    }
  }
}

/**
  Return if probe connected override is applied

  @retval TRUE    probe connected override is applied
  @retval FALSE   probe connected override is not applied
**/
BOOLEAN
IsProbeConnectedOverride (
  VOID
  )
{
  return FALSE;
}

/**
  Return if DCI Keep Early Trace is supported

  @retval TRUE    DCI Keep Early Trace is supported
  @retval FALSE   DCI Keep Early Trace is not supported
**/
BOOLEAN
IsDciKeepEarlyTraceSupported (
  VOID
  )
{
  return FALSE;
}