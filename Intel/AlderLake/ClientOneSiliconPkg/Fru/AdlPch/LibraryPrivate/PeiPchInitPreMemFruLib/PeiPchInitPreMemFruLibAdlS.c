/** @file
  PEIM to disable the PLL

@copyright
  INTEL CONFIDENTIAL
  Copyright 2019 - 2020 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains an 'Intel Peripheral Driver' and is uniquely identified as
  "Intel Reference Module" and is licensed for Intel CPUs and chipsets under
  the terms of your license agreement with Intel or your vendor. This file may
  be modified by the user, subject to additional terms of the license agreement.

@par Specification Reference:
**/
#include <Ppi/SiPolicy.h>
#include <Library/DebugLib.h>
#include <Library/IoLib.h>
#include <Library/PeiPchInitPreMemFruLib.h>
#include <Library/PciSegmentLib.h>
#include <IndustryStandard/Pci30.h>
#include <Library/PmcPrivateLib.h>
#include <Library/HobLib.h>
#include <Library/PchInfoLib.h>
#include <PchReservedResources.h>

/**
  This function will disable IOTG PLL by disabling the CPU side SSC through PMC command.

  @param[in] PCH_GENERAL_PREMEM_CONFIG *PchGeneralPreMemConfig  Instance of PCH_GENERAL_PREMEM_CONFIG
**/
VOID
DisableIotgPll (
  IN PCH_GENERAL_PREMEM_CONFIG   *PchGeneralPreMemConfig
  )
{
  if (PchGeneralPreMemConfig->IotgPllSscEn == 0) {
    //
    // Disable CPU side SSC
    //
    DEBUG ((DEBUG_INFO, "PmcDisableSaSsc()\n"));
    PmcDisableSaSsc();
  }
  return;
}

/**
  Build Memory Mapped IO Resource which is used to build E820 Table in LegacyBios,
  the resource range should be preserved in ACPI as well.
**/
VOID
PchPreservedMmioResource (
  VOID
  )
{
  UINT32          PcrMmioSize;

  PcrMmioSize = 0;
  //
  // This function builds a HOB that describes a chunk of system memory.
  //
  BuildResourceDescriptorHob (
    EFI_RESOURCE_MEMORY_MAPPED_IO,
    (EFI_RESOURCE_ATTRIBUTE_PRESENT | EFI_RESOURCE_ATTRIBUTE_INITIALIZED | EFI_RESOURCE_ATTRIBUTE_UNCACHEABLE),
    PCH_PRESERVED_BASE_ADDRESS,
    PCH_PRESERVED_MMIO_SIZE
    );
  //
  // This function builds a HOB for the memory allocation.
  //
  BuildMemoryAllocationHob (
    PCH_PRESERVED_BASE_ADDRESS,
    PCH_PRESERVED_MMIO_SIZE,
    EfiMemoryMappedIO
    );
  //
  // Reserve additional P2SB MMIO
  //
  if (IsP2sb20bPcrSupported ()) {
    PcrMmioSize = PCH_PCR_MMIO_SIZE_EX;
  } else {
    PcrMmioSize = PCH_PCR_MMIO_SIZE;
  }
  BuildResourceDescriptorHob (
    EFI_RESOURCE_MEMORY_MAPPED_IO,
    (EFI_RESOURCE_ATTRIBUTE_PRESENT | EFI_RESOURCE_ATTRIBUTE_INITIALIZED | EFI_RESOURCE_ATTRIBUTE_UNCACHEABLE),
    PCH_PCR_BASE_ADDRESS,
    PcrMmioSize
    );
  BuildMemoryAllocationHob (
    PCH_PCR_BASE_ADDRESS,
    PcrMmioSize,
    EfiMemoryMappedIO
    );
}
