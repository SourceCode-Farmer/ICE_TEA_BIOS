/** @file
  HSIO SOC layer Library for ADL-PCH

@copyright
  INTEL CONFIDENTIAL
  Copyright 2020 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains an 'Intel Peripheral Driver' and is uniquely identified as
  "Intel Reference Module" and is licensed for Intel CPUs and chipsets under
  the terms of your license agreement with Intel or your vendor. This file may
  be modified by the user, subject to additional terms of the license agreement.

@par Specification Reference:
**/

#include <Uefi/UefiBaseType.h>
#include <Library/DebugLib.h>
#include <Library/BaseMemoryLib.h>
#include <Register/PchPcrRegs.h>
#include <Register/PchRegsHsio.h>
#include <Library/PchPcrLib.h>
#include <Library/PeiHsioLib.h>
#include <Library/PchInfoLib.h>
#include <Library/MeInitLib.h>
#include <P2SbController.h>
#include <Library/P2SbSocLib.h>
#include <Library/P2SbSidebandAccessLib.h>

/**
  Get HSIO lane representation needed to perform any operation on the lane.

  @param[in]      LaneIndex      HSIO lane number to be examined
  @param[in out]  HsioLane       HSIO lane representation
  @param[in out]  P2SbController Pointer to P2SB controller identification structure
  @param[in    ]  SbAccess       Pointer to Sideband access interface structure

  @retval     EFI_SUCCESS           - Completed successfully
              EFI_INVALID_PARAMETER - HsioLane NULL or I/O Expander id is out of range
**/
VOID
PchHsioGetLane (
  IN      UINT8                          LaneIndex,
  IN OUT  HSIO_LANE                      *HsioLane,
  IN OUT  P2SB_CONTROLLER                *P2SbController,
  IN      P2SB_SIDEBAND_REGISTER_ACCESS  *SbAccess
  )
{
  // PCH-LP
  // iolane  0 -  3 : 0xAB - 000, 400, 800, c00
  // iolane  4 -  7 : 0xAA - 000, 400, 800, c00
  // iolane  8 - 11 : 0xA9 - 000, 400, 800, c00
  // PCH-H
  // iolane  0 -  9 : 0xAB - 000, 400, 800, c00, 1000, 1400, 1800, 1c00, 2000, 2400
  // iolane 10 - 21 : 0xAA - 000, 400, 800, c00, 1000, 1400, 1800, 1c00, 2000, 2400, 2800, 2c00
  // iolane 22 - 37 : 0xA8 - 000, 400, 800, c00, 1000, 1400, 1800, 1c00, 2000, 2400, 2800, 2c00, 3000, 3400, 3800, 3c00

  static UINT8 IoLanesLp[] = { 0, 4, 8, 12 };
  static UINT8 PidsLp[] = { PID_MODPHY0, PID_MODPHY1, PID_MODPHY2 };

  static UINT8 IoLanesH[] = { 0, 10, 22, 38 };
  static UINT8 PidsH[] = { PID_MODPHY0, PID_MODPHY1, PID_MODPHY3 };

  UINT8               *IoLanes;
  UINT8               *Pids;
  UINT8               PidMax;
  UINT32              Index;
  EFI_STATUS          Status;

  if (HsioLane == NULL) {
    return;
  }

  if (IsPchLp ()) {
    IoLanes = IoLanesLp;
    Pids    = PidsLp;
    PidMax  = (sizeof (IoLanesLp) / sizeof (UINT8)) - 1;
  } else {
    IoLanes = IoLanesH;
    Pids    = PidsH;
    PidMax  = (sizeof (IoLanesH) / sizeof (UINT8)) - 1;
  }
  ASSERT (LaneIndex < IoLanes[PidMax]);

  Status = GetP2SbController (P2SbController);
  ASSERT_EFI_ERROR (Status);

  for (Index = 0; Index < PidMax; ++Index) {
    if (LaneIndex < IoLanes[Index + 1]) {
      BuildP2SbSidebandAccess (
          P2SbController,
          Pids[Index],
          0,
          P2SbMemory,
          P2SbMmioAccess,
          FALSE,
          SbAccess
          );
      HsioLane->P2SbAccess = (REGISTER_ACCESS *)SbAccess;
      HsioLane->Index = LaneIndex;
      HsioLane->Pid = SbAccess->P2SbPid;
      HsioLane->Base  = (LaneIndex - IoLanes[Index]) * 0x400;
      return;
    }
  }
  ASSERT (FALSE);
}

/**
  Enable the IOMT tool using the EOM bit that is reflected on CSE FW Status register
**/
VOID
HsioBeforeSaiPostBoot (
  VOID
  )
{
#ifndef PCH_ADPP
  UINT8 PidIndex;
  UINT8 SaiIndex;
  PCH_SBI_PID PidsMpPhy[] = { PID_MPFPW1, PID_MPFPW2, PID_MPFPW3, PID_MPFPW4,
                              PID_MPFPW5, PID_MPFPW6, PID_MPFPW7 };
  PCH_SBI_PID PidsUsb3Phy[] =  { PID_U3FPW1, PID_U3FPW2, PID_U3FPW3 };
  UINT32 SaiPolicyReg[] = { R_HSIO_BTRS_SAI_WRITE_POLICY_LO, R_HSIO_REGION0_SAI_WRITE_POLICY_LO,
                           R_HSIO_REGION1_SAI_WRITE_POLICY_LO, R_HSIO_REGION2_SAI_WRITE_POLICY_LO };

  if(!IsEom () && IsPchS ()) {
    for (PidIndex = 0; PidIndex < (sizeof (PidsMpPhy) / sizeof (PCH_SBI_PID)); PidIndex ++) {
      for (SaiIndex = 0; SaiIndex < (sizeof (SaiPolicyReg) / sizeof (UINT32)); SaiIndex ++) {
        PchPcrAndThenOr32WithReadback (PidsMpPhy[PidIndex], SaiPolicyReg[SaiIndex], (UINT32) ~0, (UINT32) BIT0);
      }
    }

    for (PidIndex = 0; PidIndex < (sizeof (PidsUsb3Phy) / sizeof (PCH_SBI_PID)); PidIndex ++) {
      for (SaiIndex = 0; SaiIndex < (sizeof (SaiPolicyReg) / sizeof (UINT32)); SaiIndex ++) {
        PchPcrAndThenOr32WithReadback (PidsUsb3Phy[PidIndex], SaiPolicyReg[SaiIndex], (UINT32) ~0, (UINT32) BIT0);
      }
    }
  }
#endif
}

/*
 * Get R_HSIO_PCR_PLL_SSC_DWORD2 register offset
 */
UINT32
HsioGetPllSscDword2Offset (
  VOID
  )
{
  return R_HSIO_PCR_PLL_SSC_DWORD2;
}

/*
 * Get R_HSIO_PCR_PLL_SSC_DWORD3 register offset
 */
UINT32
HsioGetPllSscDword3Offset (
  VOID
  )
{
  return R_HSIO_PCR_PLL_SSC_DWORD3;
}
