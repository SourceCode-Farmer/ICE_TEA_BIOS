/** @file
  The P2SB controller SoC specific implementation.

@copyright
  INTEL CONFIDENTIAL
  Copyright 2020 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains an 'Intel Peripheral Driver' and is uniquely identified as
  "Intel Reference Module" and is licensed for Intel CPUs and chipsets under
  the terms of your license agreement with Intel or your vendor. This file may
  be modified by the user, subject to additional terms of the license agreement.

@par Specification Reference:
**/

#include <Ppi/SiPolicy.h>

#include <Library/DebugLib.h>
#include <Library/PcdLib.h>
#include <Library/BaseMemoryLib.h>
#include <Library/PchInfoLib.h>
#include <Library/PchPciBdfLib.h>
#include <Library/PchPolicyLib.h>
#include <Library/P2SbSocLib.h>
#include <Library/PreSiliconEnvDetectLib.h>
#include <Register/PchRegs.h>
#include <Register/PchPcrRegs.h>
#include <P2SbHandle.h>
#include "PchInitPei.h"

GLOBAL_REMOVE_IF_UNREFERENCED UINT16 mPchEndpointMaskPort[] =
{
  PID_PSF1,
  PID_PSF2,
  PID_PSF3,
  PID_PSF4
};

/**
  Get list of P2sb Endpoint Masked PIDs

  @param[out] EndpointMaskTable        Endpoint Masked PIDs
  @param[out] EndpointMaskTableLength  Length of EndpointMaskTable
**/
STATIC
VOID
P2sbGetEndpointMaskedPids (
  OUT UINT16   **EndpointMaskTable,
  OUT UINT32   *EndpointMaskTableLength
  )
{
  *EndpointMaskTable = mPchEndpointMaskPort;
  *EndpointMaskTableLength = ARRAY_SIZE (mPchEndpointMaskPort);
}

/**
  Load default values to P2SB structures
  - Initialize data structures to zero
  - Initialize function pointers to NULL
  - Initialize pointes for P2SB_HANDLE substructures

  @param[in out]  P2SbHandle      Pointer to P2SB handle structure
  @param[in out]  P2SbPrivate     Pointer to P2SB private configuration structure
  @param[in out]  P2SbController  Pointer to P2SB controller identification structure
  @param[in out]  P2SbCallback    Pointer to P2SB callback structure
**/
STATIC
VOID
P2SbHandleLoadDefaults (
  IN OUT  P2SB_HANDLE          *P2SbHandle,
  IN OUT  P2SB_PRIVATE_CONFIG  *P2SbPrivate,
  IN OUT  P2SB_CONTROLLER      *P2SbController,
  IN OUT  P2SB_CALLBACK        *P2SbCallback
  )
{
  ZeroMem (P2SbPrivate, sizeof (P2SB_PRIVATE_CONFIG));
  P2SbHandle->PrivateConfig = P2SbPrivate;

  ZeroMem (P2SbCallback, sizeof (P2SB_CALLBACK));
  P2SbHandle->Callback = P2SbCallback;

  ZeroMem (P2SbController, sizeof (P2SB_CONTROLLER));
  P2SbHandle->Controller = P2SbController;

  P2SbHandle->P2SbConfig = NULL;
}

/**
  Initialize P2SB structures - PCH Legacy
  This should be done according P2SB IP integration in SoC

  @param[in]      SiPolicy        Pointer to Silicon Policy
  @param[in out]  P2SbHandle      Pointer to P2SB handle structure
  @param[in out]  P2SbPrivate     Pointer to P2SB private configuration structure
  @param[in out]  P2SbController  Pointer to P2SB controller identification structure
  @param[in out]  P2SbCallback    Pointer to P2SB callback structure
**/
VOID
PchP2SbHandleInit (
  IN      SI_POLICY_PPI        *SiPolicy,
  IN OUT  P2SB_HANDLE          *P2SbHandle,
  IN OUT  P2SB_PRIVATE_CONFIG  *P2SbPrivate,
  IN OUT  P2SB_CONTROLLER      *P2SbController,
  IN OUT  P2SB_CALLBACK        *P2SbCallback
  )
{
  EFI_STATUS            Status;
  PCH_GENERAL_CONFIG    *PchGeneralConfig;

  //
  // Initialize: data with defaults, function pointers to NULL, substructures pointers
  //
  P2SbHandleLoadDefaults (P2SbHandle, P2SbPrivate, P2SbController, P2SbCallback);

  //
  // Initialize P2SB Private Configuration
  //
  if (SiPolicy) {
    Status = GetConfigBlock ((VOID *) SiPolicy, &gPchGeneralConfigGuid, (VOID *) &PchGeneralConfig);
    ASSERT_EFI_ERROR (Status);
    P2SbHandle->PrivateConfig->LegacyIoLowLatency = PchGeneralConfig->LegacyIoLowLatency != 0;
  }
  P2SbHandle->PrivateConfig->HaPowerGatingSupported = TRUE;
  P2SbHandle->PrivateConfig->IecSupportDisable = TRUE;

  if (IsHSLEEnvironment ()) {
    P2SbHandle->PrivateConfig->HsleWorkaround = TRUE;
  } else {
    P2SbHandle->PrivateConfig->HsleWorkaround = FALSE;
  }

  P2SbHandle->PrivateConfig->DisableHpetAndApicBdfProgramming = FALSE;

  P2SbHandle->PrivateConfig->EndpointMask7 = 0xFFFF0000; // Broadcast and all Multicast group

  //
  // Initialize P2SB callback pointers
  //
  P2SbCallback->GetEndpointMaskedPids = P2sbGetEndpointMaskedPids;

  //
  // Set pointer to P2SB Config Block
  //
  if (SiPolicy) {
    Status = GetConfigBlock ((VOID *) SiPolicy, &gP2sbConfigGuid, (VOID *) &P2SbHandle->P2SbConfig);
    ASSERT (P2SbHandle->P2SbConfig != NULL);
  }

  //
  // Initialize P2SB device data
  //
  Status = GetP2SbController (P2SbController);
  if (EFI_ERROR (Status)) {
    DEBUG ((DEBUG_ERROR, "%a: ERROR: Cannot get P2SB controller!\n", __FUNCTION__));
    ASSERT (FALSE);
  }
}

/**
  The function performs P2SB lock programming.

  @param[in] SiPolicy         The SI Policy PPI instance
**/
VOID
PchP2sbLock (
  IN  SI_POLICY_PPI           *SiPolicy
  )
{
  P2SB_HANDLE           P2SbHandle;
  P2SB_PRIVATE_CONFIG   P2SbPrivate;
  P2SB_CONTROLLER       P2SbController;
  P2SB_CALLBACK         P2SbCallback;

  PchP2SbHandleInit (SiPolicy, &P2SbHandle, &P2SbPrivate, &P2SbController, &P2SbCallback);
  P2sbLock (&P2SbHandle);
}

/**
  The function performs P2SB initialization.

  @param[in]      SiPolicy       Pointer to Silicon Policy
**/
VOID
PchP2SbInit (
  IN  SI_POLICY_PPI  *SiPolicy
  )
{
  P2SB_HANDLE           P2SbHandle;
  P2SB_PRIVATE_CONFIG   P2SbPrivate;
  P2SB_CONTROLLER       P2SbController;
  P2SB_CALLBACK         P2SbCallback;

  PchP2SbHandleInit (SiPolicy, &P2SbHandle, &P2SbPrivate, &P2SbController, &P2SbCallback);
  P2sbConfigure (&P2SbHandle);
}
