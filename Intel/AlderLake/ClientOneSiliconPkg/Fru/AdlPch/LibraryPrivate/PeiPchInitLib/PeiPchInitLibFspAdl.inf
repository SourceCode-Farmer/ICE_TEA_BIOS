## @file
# Component description file for PCH Init Lib Pei Phase for AlderLake PCH (FSP variant)
#
# @copyright
#  INTEL CONFIDENTIAL
#  Copyright 2020 - 2021 Intel Corporation.
#
#  The source code contained or described herein and all documents related to the
#  source code ("Material") are owned by Intel Corporation or its suppliers or
#  licensors. Title to the Material remains with Intel Corporation or its suppliers
#  and licensors. The Material may contain trade secrets and proprietary and
#  confidential information of Intel Corporation and its suppliers and licensors,
#  and is protected by worldwide copyright and trade secret laws and treaty
#  provisions. No part of the Material may be used, copied, reproduced, modified,
#  published, uploaded, posted, transmitted, distributed, or disclosed in any way
#  without Intel's prior express written permission.
#
#  No license under any patent, copyright, trade secret or other intellectual
#  property right is granted to or conferred upon you by disclosure or delivery
#  of the Materials, either expressly, by implication, inducement, estoppel or
#  otherwise. Any license under such intellectual property rights must be
#  express and approved by Intel in writing.
#
#  Unless otherwise agreed by Intel in writing, you may not remove or alter
#  this notice or any other notice embedded in Materials by Intel or
#  Intel's suppliers or licensors in any way.
#
#  This file contains an 'Intel Peripheral Driver' and is uniquely identified as
#  "Intel Reference Module" and is licensed for Intel CPUs and chipsets under
#  the terms of your license agreement with Intel or your vendor. This file may
#  be modified by the user, subject to additional terms of the license agreement.
#
# @par Specification Reference:
#
##


[Defines]
INF_VERSION = 0x00010017
BASE_NAME = PeiPchInitLibFspAdl
FILE_GUID = 668D1BFD-5493-4C20-9B8D-FB5E80D18491
VERSION_STRING = 1.0
MODULE_TYPE = PEIM
LIBRARY_CLASS = PchInitLib


[LibraryClasses]
BaseLib
IoLib
HobLib
DebugLib
TimerLib
PciSegmentLib
BaseMemoryLib
MemoryAllocationLib
PeiServicesLib
PeiServicesTablePointerLib
GpioLib
GpioPrivateLib
GbeLib
PchPcrLib
PchPcieRpLib
PchPolicyLib
SerialIoAccessLib
PchSbiAccessLib
SmbusCommonLib
PchCycleDecodingLib
PciExpressHelpersLib
CpuCommonLib
CpuPlatformLib
OcWdtLib
PeiWdtLib
ConfigBlockLib
PeiMeLib
SerialIoI2cMasterLib
PeiPchTraceHubLib
S3BootScriptLib
PeiSataLib
CnviLib
PsfLib
PeiPsfLib
PmcLib
PmcPrivateLib
PeiPmcPrivateLib
SpiLib
MtrrLib
PchDmiLib
DciPrivateLib
PchFiaLib
PeiGbeInitLib
PeiTsnInitLib
PeiHsioLib
ChipsetInitLib
PeiIshLib
PeiThermalLib
PeiPchPcieClocksLib
UsbLib
PeiP2sbPrivateLib
UsbHostControllerInitLib
UsbDeviceControllerInitLib
Usb2PhyLib
PeiCnviLib
PeiRtcLib
CpuPlatformLib
PeiPchDmiLib
PeiItssLib
SiScheduleResetLib
PeiSmbusLib
PeiEspiInitLib
PeiLpcLib
PeiDciInitLib
PeiIshInitLib
PeiSerialIoInitLib
PeiIehInitLib
PeiPciePreMemRpInitLib
PeiPcieRpInitLib
PeiPcieSipInitLib
EspiAccessPrivateLib
SpiAccessPrivateLib
PeiThcInitLib
PeiPmcPrivateLib
PeiFusaLib
PreSiliconEnvDetectLib
PchInitPreMemFruLib
PeiHybridStorageLib
PchPciBdfLib
PchInfoLib
BaseTraceHubInfoFruLib
PeiRstPrivateLib
PeiMeEnhancedDebugLib
PeiSpiExtendedDecodeLib
PmcSocLib
PchAdrLib
SataLib
SataSocLib
PeiRstPolicyLib
HdaSocLib
GbeSocLib
P2SbSocLib
HsioSocLib
FiaSocLib
PeiCnviPreMemLib
WdtCommonLib

[Packages]
MdePkg/MdePkg.dec
ClientOneSiliconPkg/SiPkg.dec
UefiCpuPkg/UefiCpuPkg.dec

[Sources]
PchInitPreMem.c
PchInit.c
PchUsb.c
PchPsth.c
PchEndOfPei.c
PchHobs.c
PchSataInit.c
PchGbe.c
PchP2Sb.c
PchSmbus.c
PchEspi.c
PchHsio.c
PchRpInit.c
PchIeh.c
PchExtendedBiosDecodeRange.c

[Ppis]
gEfiPeiMemoryDiscoveredPpiGuid ## CONSUMES
gEfiPeiSmbus2PpiGuid ## PRODUCES
gEfiEndOfPeiSignalPpiGuid ## CONSUMES
gHeciPpiGuid ## CONSUMES
gMeBeforeDidSentPpiGuid ## CONSUMES
gSiPreMemPolicyPpiGuid
gExtendedBiosDecodeReadyPpiGuid

[Guids]
gPchDeviceTableHobGuid
gChipsetInitHobGuid
gPchConfigHobGuid
gPchGlobalResetGuid
gPchInfoHobGuid
gSaMiscPeiPreMemConfigGuid
gUsb2PhyConfigGuid          ## CONSUMES
gUsb3HsioConfigGuid         ## CONSUMES
gUsbConfigGuid              ## CONSUMES
gSpiConfigGuid              ## CONSUMES
gGbeConfigGuid              ## CONSUMES
gP2sbConfigGuid             ## CONSUMES

[FixedPcd]
[Pcd]
gSiPkgTokenSpaceGuid.PcdAcpiBaseAddress            ## CONSUMES
gSiPkgTokenSpaceGuid.PcdSiliconInitTempPciBusMin   ## CONSUMES
gSiPkgTokenSpaceGuid.PcdSiliconInitTempPciBusMax   ## CONSUMES
gSiPkgTokenSpaceGuid.PcdSiliconInitTempMemBaseAddr ## CONSUMES
gSiPkgTokenSpaceGuid.PcdFspBinaryEnable            ## CONSUMES
gSiPkgTokenSpaceGuid.PcdHybridStorageSupport       ## CONSUMES
gSiPkgTokenSpaceGuid.PcdEmbeddedEnable             ## CONSUMES
