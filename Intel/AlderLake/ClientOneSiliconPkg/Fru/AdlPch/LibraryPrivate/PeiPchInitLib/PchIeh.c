/** @file
  The IEH initialization After Memory.

@copyright
  INTEL CONFIDENTIAL
  Copyright 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains an 'Intel Peripheral Driver' and is uniquely identified as
  "Intel Reference Module" and is licensed for Intel CPUs and chipsets under
  the terms of your license agreement with Intel or your vendor. This file may
  be modified by the user, subject to additional terms of the license agreement.

@par Specification Reference:
**/

#include "PchInitPei.h"

#include <IehConfig.h>
#include <IehHandle.h>
#include <Library/PchPciBdfLib.h>
#include <Library/PchPcrLib.h>
#include <Library/PchInfoLib.h>
#include <Register/ItssRegs.h>
#include <Register/IehRegs.h>
#include <Register/PchRegs.h>
#include <Register/PchPcrRegs.h>


/**
  Programs severity register bits based on ITSS UEV register

  @param[in] IehHandle    IEH Handle instance
**/
VOID
IehProgramSeverity (
  IN  IEH_HANDLE      *IehHandle
  )
{
  UINT64         Data64;
  UINT32         DataMask;
  UINT32         ItssUev;

  //
  // Programming serverity values (LERRUNCSEV) based on ITSS UEV register
  //
  ItssUev = PchPcrRead32 (PID_ITSS, R_ITSS_PCR_UEV);
  Data64  = BIT5 | BIT4 | BIT3 | BIT2 | BIT1 | BIT0;
  if (ItssUev & BIT20) {
    Data64 |= BIT40 | BIT36 | BIT32 | BIT28 | BIT24 | BIT20 | BIT16 | BIT12 | BIT8;
  }
  //
  // UC severity bits
  //
  if (ItssUev & BIT16) {
    Data64 |= BIT39 | BIT35 | BIT31 | BIT27 | BIT23 | BIT19 | BIT15 | BIT11 | BIT7;
  }
  //
  // Cmd and Data parity bits
  //
  if (ItssUev & BIT22) {
    Data64 |= BIT38 | BIT34 | BIT30 | BIT29 | BIT26 | BIT25 | BIT22 | BIT21 | BIT18 | BIT17 | BIT14 | BIT13 | BIT10 | BIT9 | BIT6;
  }

  if (IsPchH ()) {
    PciSegmentWrite32 (IehHandle->PciCfgBase + R_IEH_CFG_LERRUNCSEV, (UINT32) Data64);
    PciSegmentWrite32 (IehHandle->PciCfgBase + R_IEH_CFG_LERRUNCSEV + 4, (UINT32) RShiftU64 (Data64, 32));
  } else {
    //
    // For LP only register is valid only for 29 bits
    //
    DataMask = (1 << 29) - 1;
    PciSegmentWrite32 (IehHandle->PciCfgBase + R_IEH_CFG_LERRUNCSEV, (UINT32) (Data64 & DataMask));
  }
}

/**
  Programs IEH to Enable Mode

  @param[in] IehHandle    IEH Handle instance
**/
VOID
IehEnableMode (
  IN  IEH_HANDLE      *IehHandle
  )
{
  PciSegmentWrite32 (IehHandle->PciCfgBase + R_IEH_CFG_GCOERRMSK, 0xFFE00000);
  PciSegmentWrite32 (IehHandle->PciCfgBase + R_IEH_CFG_GNFERRMSK, 0xFFE00000);
  PciSegmentWrite32 (IehHandle->PciCfgBase + R_IEH_CFG_GFAERRMSK, 0xFFE00000);
  PciSegmentWrite32 (IehHandle->PciCfgBase + R_IEH_CFG_GSYSEVTMSK, 0x0);
  PciSegmentAndThenOr32 (IehHandle->PciCfgBase + R_IEH_CFG_GSYSEVTMAP, 0x7FFFFFC0, 0x3F);
  PciSegmentAnd32 (IehHandle->PciCfgBase + R_IEH_CFG_IEHTYPEVER, (UINT32)~(B_IEH_CFG_IEHTYPEVER_IEH_BUSNUM));
  PciSegmentAndThenOr32 (IehHandle->PciCfgBase + R_IEH_CFG_ERRPINCTRL, 0xFFFFFFC0, 0x2A);
  PciSegmentAnd32 (IehHandle->PciCfgBase + R_IEH_CFG_ERRPINDATA, 0xFFFFFFF8);
  PciSegmentAnd32 (IehHandle->PciCfgBase + R_IEH_CFG_BITMAP, 0xFFFFFFE0);
  if (IsPchH ()) {
    PciSegmentWrite32 (IehHandle->PciCfgBase + R_IEH_CFG_LERRUNCMSK, 0x0);
    PciSegmentWrite32 (IehHandle->PciCfgBase + R_IEH_CFG_LERRUNCMSK + 4, 0xFFFFFC00);
    PciSegmentWrite32 (IehHandle->PciCfgBase + R_IEH_CFG_LERRCORMSK, 0x0);
    PciSegmentWrite32 (IehHandle->PciCfgBase + R_IEH_CFG_LERRCORMSK + 4, 0xFFFFFC00);
  } else {
    PciSegmentWrite32 (IehHandle->PciCfgBase + R_IEH_CFG_LERRUNCMSK, 0xC0000000);
    PciSegmentWrite32 (IehHandle->PciCfgBase + R_IEH_CFG_LERRUNCMSK + 4, 0xFFFFFFFF);
    PciSegmentWrite32 (IehHandle->PciCfgBase + R_IEH_CFG_LERRCORMSK, 0xC0000000);
    PciSegmentWrite32 (IehHandle->PciCfgBase + R_IEH_CFG_LERRCORMSK + 4, 0xFFFFFFFF);
  }
  PciSegmentWrite32 (IehHandle->PciCfgBase + R_IEH_CFG_LCOERRCNTSEL, 0x0);
}

/**
  Build an IEH handle for PCH

  @param[in]  SiPolicy            SiPolicyPpi instance
  @param[in, out] IehHandle           Initialized IEH handle
  @param[in, out] IehPrivateConfig  Initialized IEH Private Config structure
**/
VOID
PchIehGetHandle (
  IN  SI_POLICY_PPI           *SiPolicy,
  IN OUT  IEH_HANDLE          *IehHandle,
  IN OUT  IEH_PRIVATE_CONFIG  *IehPrivateConfig
  )
{
  EFI_STATUS        Status;
  IEH_CONFIG        *IehConfig;

  //
  // PCIe Config Space Base Address
  //
  IehHandle->PciCfgBase = IehPciCfgBase ();

  //
  // IEH Private Config
  //
  IehPrivateConfig->BusNum = DEFAULT_PCI_BUS_NUMBER_PCH;
  IehHandle->PrivateConfig = IehPrivateConfig;

  //
  // IEH public configuration
  //
  Status = GetConfigBlock ((VOID *) SiPolicy, &gIehConfigGuid, (VOID *) &IehConfig);
  ASSERT_EFI_ERROR (Status);
  IehHandle->Config = IehConfig;
}

/**
  Initialize IEH controller.

  @param[in] SiPolicy  The Silicon Policy PPI instance
**/
VOID
PchIehInit (
  IN SI_POLICY_PPI  *SiPolicy
  )
{
  IEH_HANDLE            IehHandle;
  IEH_PRIVATE_CONFIG    IehPrivateConfig;

  ZeroMem (&IehHandle, sizeof (IEH_HANDLE));
  ZeroMem (&IehPrivateConfig, sizeof (IEH_PRIVATE_CONFIG));

  PchIehGetHandle (SiPolicy, &IehHandle, &IehPrivateConfig);

  IehProgramSeverity (&IehHandle);
  IehInit (&IehHandle);

  if (IehHandle.Config->Mode == IEH_MODE_ENABLE) {
    IehEnableMode (&IehHandle);
  }
}

