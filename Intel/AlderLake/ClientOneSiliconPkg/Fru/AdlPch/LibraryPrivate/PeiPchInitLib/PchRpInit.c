/** @file
  The PCH Root Port controller SoC specific implementation.

@copyright
  INTEL CONFIDENTIAL
  Copyright 2020 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains an 'Intel Peripheral Driver' and is uniquely identified as
  "Intel Reference Module" and is licensed for Intel CPUs and chipsets under
  the terms of your license agreement with Intel or your vendor. This file may
  be modified by the user, subject to additional terms of the license agreement.

@par Specification Reference:
**/

#include <Library/DebugLib.h>
#include <Library/BaseMemoryLib.h>
#include <Library/PchPcieRpLib.h>
#include <Library/PcieRpLib.h>
#include <Library/PchPciBdfLib.h>
#include <Library/P2SbSocLib.h>
#include <Library/P2SbSidebandAccessLib.h>
#include <Library/FiaSocLib.h>
#include <P2SbController.h>
#include <Register/PcieSipRegs.h>
#include "PchInitPei.h"

/**
  The function programs HSIO Rx\Tx Eq policy registers for PCIe lanes.

  @param[in]  SiPreMemPolicyPpi  The SI PreMem Policy PPI instance
**/
VOID
PchHsioRxTxEqPolicyPcieProg (
  IN SI_PREMEM_POLICY_PPI  *SiPreMemPolicyPpi
  )
{
  HSIO_HANDLE                        HsioHandle;
  HSIO_PRIVATE_CONFIG                HsioPrivate;
  HSIO_LANE                          HsioLane;
  P2SB_CONTROLLER                    P2SbController;
  P2SB_SIDEBAND_REGISTER_ACCESS      SbAccess;
  UINT8                              Index;
  UINT8                              MaxPciePorts;
  UINT8                              LaneNum;
  UINT64                             RpBase;
  UINT32                             RpToRetrain;
  UINT32                             TimeSpent;

  MaxPciePorts = GetPchMaxPciePortNum ();
  RpToRetrain = 0;
  for (Index = 0; Index < MaxPciePorts; Index++) {
    RpBase = PchPcieRpPciCfgBase (Index);
    if (PchFiaGetPcieLaneNum (Index, &LaneNum)) {
      PchHsioHandleInit (NULL, SiPreMemPolicyPpi, LaneNum, (UINT32)-1, &HsioHandle, &HsioPrivate, &HsioLane, &P2SbController, &SbAccess);
      HsioRxTxEqPolicyPcieProg (&HsioHandle, Index, RpBase, &RpToRetrain);
    }
  }

  //
  // According to PCIE spec, following Link Disable PCIE device may take up to 100ms to become operational.
  // Wait until all ports are operational again or until 100ms passed
  //
  TimeSpent = 0;
  while ((TimeSpent < 100) && (RpToRetrain != 0)) {
    for (Index = 0; Index < MaxPciePorts; Index++) {
      if ((RpToRetrain & (BIT0 << Index)) == 0) {
        continue;
      }
      RpBase = PchPcieRpPciCfgBase (Index);
      if (PciSegmentRead16 (RpBase + R_PCIE_CFG_LSTS) & B_PCIE_LSTS_LA) {
        RpToRetrain &= ~(BIT0 << Index);
      }
    }
    MicroSecondDelay (1000);
    TimeSpent++;
  }
}

/**
  The function program HSIO registers.

  @param[in] SiPolicyPpi               The SI Policy PPI instance

**/
VOID
PchPcieHsioConfigure (
  IN  SI_POLICY_PPI    *SiPolicyPpi
  )
{
  EFI_STATUS                         Status;
  PCH_PCIE_CONFIG                    *PchPcieConfig;
  HSIO_HANDLE                        HsioHandle;
  HSIO_PRIVATE_CONFIG                HsioPrivate;
  HSIO_LANE                          HsioLane;
  P2SB_CONTROLLER                    P2SbController;
  P2SB_SIDEBAND_REGISTER_ACCESS      SbAccess;
  UINT8                              HsioLaneNo;
  UINT32                             PcieIndex;

  DEBUG ((DEBUG_INFO, "PchPcieHsioConfigure() Start\n"));

  Status = GetConfigBlock ((VOID *) SiPolicyPpi, &gPchPcieConfigGuid, (VOID *) &PchPcieConfig);
  ASSERT_EFI_ERROR (Status);

  //
  // Configure PCIe tuning.
  //
  if (PchPcieConfig->PcieCommonConfig.ComplianceTestMode) {
    for (PcieIndex = 0; PcieIndex < GetPchMaxPciePortNum (); ++PcieIndex) {
      if (PchFiaGetPcieLaneNum (PcieIndex, &HsioLaneNo)) {
        DEBUG ((DEBUG_INFO, "PCIe compliance mode for PCIe%d, HSIO%d\n", PcieIndex + 1, HsioLaneNo));
        PchHsioHandleInit (SiPolicyPpi, NULL, HsioLaneNo, (UINT32)-1, &HsioHandle, &HsioPrivate, &HsioLane, &P2SbController, &SbAccess);
        PchHsioEnablePcieComplianceMode (&HsioHandle);
      }
    }
  }

  // Re-use HSIO Register Access but for PID_MODPHY0
  // This is needed to keep same API accessing to ChipsetInit code
  PchFiaGetPcieLaneNum (0, &HsioLaneNo);
  PchHsioHandleInit (SiPolicyPpi, NULL, HsioLaneNo, (UINT32)-1, &HsioHandle, &HsioPrivate, &HsioLane, &P2SbController, &SbAccess);
  SbAccess.P2SbPid = HsioLane.Pid = PID_MODPHY0;
  PchPciePllSscProg (&HsioHandle);

  DEBUG ((DEBUG_INFO, "PchPcieHsioConfigure() End\n"));
}


