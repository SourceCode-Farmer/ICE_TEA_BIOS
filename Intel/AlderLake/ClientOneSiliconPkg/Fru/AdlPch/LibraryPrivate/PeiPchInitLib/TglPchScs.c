/** @file
  TGL PCH SCS init.

@copyright
  INTEL CONFIDENTIAL
  Copyright 2020 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains an 'Intel Peripheral Driver' and is uniquely identified as
  "Intel Reference Module" and is licensed for Intel CPUs and chipsets under
  the terms of your license agreement with Intel or your vendor. This file may
  be modified by the user, subject to additional terms of the license agreement.

@par Specification Reference:
**/

#include <Register/PchPcrRegs.h>
#include <Library/PchPciBdfLib.h>
#include <Library/PmcSocLib.h>
#include <Library/FiaSocLib.h>
#include <Library/GpioNativePads.h>
#include "PchInitPei.h"
#include <Register/ScsRegs.h>

IOSF2OCP_PORT  mUfs0Port = {
  PID_UFSX2,
  0
};

IOSF2OCP_PORT  mUfs1Port = {
  PID_UFSX2,
  1
};

IOSF2OCP_PORT  mEmmcPort = {
  PID_EMMC,
  0
};

SCS_SD_DLL  mEmmcHcDll = {
  0x505,
  0x0B0B,
  0x1C292828,
  0x1C0B5F32,
  0x21808,
  0x1818
};

SCS_SD_CAPS mEmmcCaps = {
  0x3050EB1E,
  0x40040C8
};

/**
  Get the number of lanes that a given UFS controller may use.

  @param[in] UfsIndex  Index of the UFS

  @return A number of available lanes.
**/
STATIC
UINT8
TglPchScsUfsGetNumOfAvailableLanes (
  IN UINT32  UfsIndex
  )
{
  UINT8  LaneIndex;
  UINT8  AvailableLanes;

  //
  // According to UFS 2.0 specification UFS can support up to 2 lanes.
  //
  AvailableLanes = 0;
  for (LaneIndex = 0; LaneIndex < 2; LaneIndex++) {
    if (PchFiaIsUfsLaneConnected (UfsIndex, LaneIndex)) {
      AvailableLanes++;
    } else {
      break;
    }
  }

  return AvailableLanes;
}

/**
  Called as a last step in UFS controller disable flow.

  @param[in] ScsUfsHandle  Handle
**/
VOID
TglPchScsUfsDisable (
  IN SCS_UFS_HANDLE *ScsUfsHandle
  )
{
  //
  // NOTE: IOSF2OCP controller index just happens to match the PSF controller index. In general they do not
  // have to be the same.
  //
  PsfDisableDevice (PsfScsUfsPort (ScsUfsHandle->Controller.Iosf2OcpPort->ControllerIndex));
  //
  // NOTE: This will power gate the entire UFSx2 subsystem and hence should not be called during the controller 0 disable flow
  //
  if (ScsUfsHandle->Controller.Iosf2OcpPort->ControllerIndex == 1) {
    PmcDisableScsUfs ();
  }
}

/**
  Called after disabling BAR1 in IOSF2OCP.

  @param[in] ScsUfsHandle  Handle
**/
VOID
TglPchScsUfsBar1Disable (
  IN SCS_UFS_HANDLE  *ScsUfsHandle
  )
{
  //
  // NOTE: IOSF2OCP controller index just happens to match the PSF controller index. In general they do not
  // have to be the same.
  //
  PsfDisableDeviceBar (PsfScsUfsPort (ScsUfsHandle->Controller.Iosf2OcpPort->ControllerIndex), (BIT3 | BIT2));
}

/**
  Initializes UFS handle.

  @param[in]  UfsIndex             Index of the UFS
  @param[in]  SiPolicyPpi          SiPolicy
  @param[in]  ScsInfoHob           SCS info hob
  @param[in]  UfsControllerConfig  Pointer to UFS controller platform config
  @param[out] ScsUfsHandle         Initialized handle on output

  @retval EFI_SUCESS  Handle initialized
  @retval Others      Failed to initialize
**/
EFI_STATUS
TglPchScsInitUfsHandle (
  IN  UINT8                      UfsIndex,
  IN  SI_POLICY_PPI              *SiPolicyPpi,
  IN  SCS_INFO_HOB               *ScsInfoHob,
  IN  SCS_UFS_CONTROLLER_CONFIG  *UfsControllerConfig,
  OUT SCS_UFS_HANDLE             *ScsUfsHandle
  )
{
  UINT8            InterruptPin = 0;
  UINT8            Irq = 0;

  if (UfsIndex == 0) {
    ScsUfsHandle->Controller.Iosf2OcpPort = &mUfs0Port;
  } else if (UfsIndex == 1) {
    ScsUfsHandle->Controller.Iosf2OcpPort = &mUfs1Port;
  } else {
    return EFI_INVALID_PARAMETER;
  }
  ScsUfsHandle->Controller.Mmp = NULL; //Required MMP programming is taken care in fuses
  ScsUfsHandle->Controller.PciCfgBase = ScsUfsPciCfgBase (UfsIndex);
  ScsUfsHandle->Controller.MmioBase = PcdGet32 (PcdSiliconInitTempMemBaseAddr);

  ScsUfsHandle->SocConfig.IsBootMedium = FALSE;
  ScsUfsHandle->SocConfig.NumOfLanes = TglPchScsUfsGetNumOfAvailableLanes (UfsIndex);
  if (UfsIndex != 0) {
    ItssGetDevIntConfig (
      SiPolicyPpi,
      ScsUfsDevNumber (UfsIndex),
      ScsUfsFuncNumber (UfsIndex),
      &InterruptPin,
      &Irq
    );
  }
  ScsUfsHandle->SocConfig.IntPin = InterruptPin;
  ScsUfsHandle->SocConfig.Irq = Irq;

  ScsUfsHandle->Config = UfsControllerConfig;

  ScsUfsHandle->Callbacks.Disable = TglPchScsUfsDisable;
  ScsUfsHandle->Callbacks.Bar1Disable = TglPchScsUfsBar1Disable;

  ScsUfsHandle->UfsInfo = &ScsInfoHob->UfsInfo[UfsIndex];
  return EFI_SUCCESS;
}

/**
  Called as a last step in Emmc controller disable flow.
  @param[in] ScsEmmcHandle  Handle
**/
VOID
TglPchScsEmmcDisable (
  IN SCS_EMMC_HANDLE *ScsEmmcHandle
  )
{
  PsfDisableDevice (PsfScsEmmcPort ());
  PmcDisableScsEmmc ();
}

/**
  Called after disabling BAR1 in IOSF2OCP.
  @param[in] ScsEmmcHandle  Handle
**/
VOID
TglPchScsEmmcBar1Disable (
  IN SCS_EMMC_HANDLE  *ScsEmmcHandle
  )
{
  PsfDisableDeviceBar (PsfScsEmmcPort (), (BIT3 | BIT2));
}

/**
  Enable Emmc Gpios
  @param[in] ScsEmmcHandle  Handle
**/
STATIC
VOID
TglPchScsEmmcEnableGpios (
  IN SCS_EMMC_HANDLE    *ScsEmmcHandle
  )
{
  UINT8              Index;
  SCS_EMMC_CONFIG    *EmmcConfig;
  EmmcConfig = ScsEmmcHandle->Config;
  GpioSetNativePadByFunction (GPIO_FUNCTION_EMMC_CMD, EmmcConfig->GpioConfig.Cmd.PinMux);
  GpioSetElectricalConfigByFunction (GPIO_FUNCTION_EMMC_CMD, EmmcConfig->GpioConfig.Cmd.PinMux, EmmcConfig->GpioConfig.Cmd.PadTermination);
  for (Index = 0; Index < SCS_EMMC_MAX_DATA_GPIOS; Index++) {
    GpioSetNativePadByFunction (GPIO_FUNCTION_EMMC_DATA (Index), EmmcConfig->GpioConfig.Data[Index].PinMux);
    GpioSetElectricalConfigByFunction (GPIO_FUNCTION_EMMC_DATA (Index), EmmcConfig->GpioConfig.Data[Index].PinMux, EmmcConfig->GpioConfig.Data[Index].PadTermination);
  }
  GpioSetNativePadByFunction (GPIO_FUNCTION_EMMC_RCLK, EmmcConfig->GpioConfig.Rclk.PinMux);
  GpioSetElectricalConfigByFunction (GPIO_FUNCTION_EMMC_RCLK, EmmcConfig->GpioConfig.Rclk.PinMux, EmmcConfig->GpioConfig.Rclk.PadTermination);
  GpioSetNativePadByFunction (GPIO_FUNCTION_EMMC_CLK, EmmcConfig->GpioConfig.Clk.PinMux);
  GpioSetElectricalConfigByFunction (GPIO_FUNCTION_EMMC_CLK, EmmcConfig->GpioConfig.Clk.PinMux, EmmcConfig->GpioConfig.Clk.PadTermination);
  GpioSetNativePadByFunction (GPIO_FUNCTION_EMMC_RESETB, EmmcConfig->GpioConfig.Resetb.PinMux);
  GpioSetElectricalConfigByFunction (GPIO_FUNCTION_EMMC_RESETB, EmmcConfig->GpioConfig.Resetb.PinMux, EmmcConfig->GpioConfig.Resetb.PadTermination);
}

/**
  Initializes EMMC handle.
  @param[in]  SiPolicyPpi          SiPolicy
  @param[in]  ScsInfoHob           SCS info hob
  @param[in]  EmmcConfig           Pointer to EMMC controller platform config
  @param[out] ScsEmmcHandle        Initialized handle on output
  @retval EFI_SUCESS  Handle initialized
  @retval Others      Failed to initialize
**/
EFI_STATUS
TglPchScsInitEmmcHandle (
  IN  SI_POLICY_PPI              *SiPolicyPpi,
  IN  SCS_INFO_HOB               *ScsInfoHob,
  IN  SCS_EMMC_CONFIG            *EmmcConfig,
  OUT SCS_EMMC_HANDLE            *ScsEmmcHandle
  )
{
  UINT8            InterruptPin = 0;
  UINT8            Irq = 0;
  ScsEmmcHandle->Controller.Iosf2OcpPort = &mEmmcPort;
  ScsEmmcHandle->Controller.PciCfgBase = ScsEmmcPciCfgBase ();
  ScsEmmcHandle->Controller.MmioBase = PcdGet32 (PcdSiliconInitTempMemBaseAddr);
  ScsEmmcHandle->SocConfig.DefaultDll = mEmmcHcDll;
  ScsEmmcHandle->SocConfig.Caps = mEmmcCaps;
  ItssGetDevIntConfig (
    SiPolicyPpi,
    ScsEmmcDevNumber (),
    ScsEmmcFuncNumber (),
    &InterruptPin,
    &Irq
  );
  ScsEmmcHandle->SocConfig.IntPin = InterruptPin;
  ScsEmmcHandle->SocConfig.Irq = Irq;
  ScsEmmcHandle->Config = EmmcConfig;
  ScsEmmcHandle->Callbacks.Disable = TglPchScsEmmcDisable;
  ScsEmmcHandle->Callbacks.Bar1Disable = TglPchScsEmmcBar1Disable;
  ScsEmmcHandle->Callbacks.PhyEnable = TglPchScsEmmcEnableGpios;
  ScsEmmcHandle->EmmcInfo = &ScsInfoHob->EmmcInfo;
  return EFI_SUCCESS;
}

/**
  Initializes TGL PCH SCS UFS subsytem.

  @param[in] SiPolicy                 The SI Policy PPI instance
**/
VOID
TglPchScsUfsInit (
  IN SI_POLICY_PPI  *SiPolicy,
  IN SCS_INFO_HOB   *ScsInfoHob
  )
{
  EFI_STATUS                 Status;
  SCS_UFS_CONFIG             *UfsConfig;
  UINT32                     MaxUfsNum;
  UINT8                      UfsIndex;
  SCS_UFS_HANDLE             ScsUfsHandle;

  //
  // Initialize IOSF2OCP bridge power gating. On TGL there is only
  // one IOSF2OCP bridge.
  //
  Iosf2OcpConfigurePowerAndClockGating (PID_UFSX2, 0x4FF80F);
  Iosf2OcpEnableSnoopedTransactions (PID_UFSX2);

  Status = GetConfigBlock ((VOID*) SiPolicy, &gUfsConfigGuid, (VOID*) &UfsConfig);
  if (EFI_ERROR (Status)) {
    DEBUG ((DEBUG_ERROR, "Failed to locate UFS config block\n"));
    return;
  }

  MaxUfsNum = PchGetMaxUfsNum ();
  for (UfsIndex = 0; UfsIndex < MaxUfsNum; UfsIndex++) {
    //
    // If UFS is not supported by PMC just continue.
    // PSF access to the device has already been disabled in pre-mem.
    //
    if (!PmcIsScsUfsSupported ()) {
      continue;
    }
    Status = TglPchScsInitUfsHandle (UfsIndex, SiPolicy, ScsInfoHob, &UfsConfig->UfsControllerConfig[UfsIndex], &ScsUfsHandle);
    if (!EFI_ERROR (Status)) {
      PciSegmentAnd16 (
        ScsUfsHandle.Controller.PciCfgBase + R_SCS_CFG_PG_CONFIG,
        (UINT16)~(B_SCS_CFG_PG_CONFIG_HAE)
      );
      ScsUfsInit (&ScsUfsHandle);
      if (ScsUfsHandle.Config->Enable && (ScsUfsHandle.SocConfig.NumOfLanes != 0)) {
        PciSegmentOr16 (
          ScsUfsHandle.Controller.PciCfgBase + R_SCS_CFG_PG_CONFIG,
          (B_SCS_CFG_PG_CONFIG_SE | B_SCS_CFG_PG_CONFIG_PGE | B_SCS_CFG_PG_CONFIG_I3E)
        );
      }
    }
  }
}

/**
  Initializes TGL PCH SCS Emmc subsytem.

  @param[in] SiPolicy                 The SI Policy PPI instance
**/
VOID
TglPchScsEmmcInit (
  IN SI_POLICY_PPI  *SiPolicy,
  IN SCS_INFO_HOB   *ScsInfoHob
  )
{
  EFI_STATUS                 Status;
  SCS_EMMC_CONFIG            *EmmcConfig;
  SCS_EMMC_HANDLE            ScsEmmcHandle;

  Status = GetConfigBlock ((VOID*) SiPolicy, &gEmmcConfigGuid, (VOID*) &EmmcConfig);
  if (EFI_ERROR (Status)) {
    DEBUG ((DEBUG_ERROR, "Failed to locate eMMC config block\n"));
    return;
  }
  if (IsPchEmmcSupported ()) {
    Iosf2OcpConfigurePowerAndClockGating (PID_EMMC, 0x1D847);
    Iosf2OcpEnableSnoopedTransactions (PID_EMMC);
    Status = TglPchScsInitEmmcHandle (SiPolicy, ScsInfoHob, EmmcConfig, &ScsEmmcHandle);
    if (!EFI_ERROR (Status)) {
      ScsEmmcInit (&ScsEmmcHandle);
      if (ScsEmmcHandle.Config->Enable) {
        PciSegmentOr16 (
          ScsEmmcHandle.Controller.PciCfgBase + R_SCS_CFG_PG_CONFIG,
          (B_SCS_CFG_PG_CONFIG_SE | B_SCS_CFG_PG_CONFIG_PGE | B_SCS_CFG_PG_CONFIG_I3E)
        );
      }
    }
  }
}

/**
  Initializes TGL PCH SCS subsytem.

  @param[in] SiPolicy                 The SI Policy PPI instance
**/
VOID
ScsInit (
  IN SI_POLICY_PPI  *SiPolicy
  )
{
  VOID                       *HobPointer;
  SCS_INFO_HOB               ScsInfoHob;

  DEBUG ((DEBUG_INFO, "ScsInit(): Start\n"));

  ZeroMem (&ScsInfoHob, sizeof (SCS_INFO_HOB));

  TglPchScsUfsInit (SiPolicy, &ScsInfoHob);
  TglPchScsEmmcInit (SiPolicy, &ScsInfoHob);

  HobPointer = BuildGuidDataHob (
                 &gScsInfoHobGuid,
                 &ScsInfoHob,
                 sizeof (SCS_INFO_HOB)
                 );
  ASSERT (HobPointer != NULL);

  DEBUG ((DEBUG_INFO, "ScsInit(): Finished\n"));
}

/**
  Disable SCS devices before anyone has chance to do invalid access to them.
**/
VOID
ScsEarlyDisable (
  VOID
  )
{

  if (!PmcIsScsUfsSupported ()) {
    DEBUG ((DEBUG_INFO, "ScsEarlyDisable(): Disabling both the controllers\n"));
    //
    // Disable UFS0 and UFS1 if it has been disabled in PMC strap.
    //
    PsfDisableDevice (PsfScsUfsPort (0));
    PsfDisableDevice (PsfScsUfsPort (1));
  }
  if ((IsPchEmmcSupported ()) && (!PmcIsScsEmmcSupported ())) {
    DEBUG ((DEBUG_INFO, "ScsEarlyDisable(): Disabling Emmc controller in Psf\n"));
    PsfDisableDevice (PsfScsEmmcPort ());
  }
}
