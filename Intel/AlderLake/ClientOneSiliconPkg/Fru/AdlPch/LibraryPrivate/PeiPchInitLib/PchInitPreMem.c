/** @file
  The PCH Init PEIM implements the PCH PEI Init PPI.

@copyright
  INTEL CONFIDENTIAL
  Copyright 2004 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains an 'Intel Peripheral Driver' and is uniquely identified as
  "Intel Reference Module" and is licensed for Intel CPUs and chipsets under
  the terms of your license agreement with Intel or your vendor. This file may
  be modified by the user, subject to additional terms of the license agreement.

@par Specification Reference:
**/
#include "PchInitPei.h"
#include "PchUsb.h"
#include <Library/ChipsetInitLib.h>
#include <Library/PostCodeLib.h>
#include <Library/OcWdtLib.h>
#include <Library/SpiLib.h>
#include <Library/PchDmiLib.h>
#include <Library/PeiP2sbPrivateLib.h>
#include <Library/PeiRtcLib.h>
#include <Library/PeiWdtLib.h>
#include <Library/PeiSmbusLib.h>
#include <Library/PeiDciInitLib.h>
#include <Library/PeiItssLib.h>
#include <PchPcieRpInfo.h>
#include <Register/PchPcieRpRegs.h>
#include <Register/MeRegs.h>
#include <Library/PeiPcieRpInitLib.h>
#include <Library/SpiAccessPrivateLib.h>
#include <Library/EspiAccessPrivateLib.h>
#include <Library/PreSiliconEnvDetectLib.h>
#include <Library/PeiPchInitPreMemFruLib.h>
#include <Library/PeiSerialIoInitLib.h>
#include <Library/PchPciBdfLib.h>
#include <Library/PchInfoLib.h>
#include <Library/BaseTraceHubInfoFruLib.h>
#include <Library/PmcSocLib.h>
#include <TcssPeiPreMemConfig.h>
#include <Library/HdaSocLib.h>
#include <Library/P2SbSocLib.h>
#include <Library/HsioSocLib.h>
#include <Library/FiaSocLib.h>
#include <Library/PeiCnviLib.h>
#include <Library/WdtCommonLib.h>
#include "PchInitPei.h"

EFI_STATUS
EFIAPI
PchTraceHubPowerGateNotifyCallback (
  IN EFI_PEI_SERVICES           **PeiServices,
  IN EFI_PEI_NOTIFY_DESCRIPTOR  *NotifyDescriptor,
  IN VOID                       *Ppi
  );

static EFI_PEI_NOTIFY_DESCRIPTOR  mPeiPchTraceHubNotifyList = {
  (EFI_PEI_PPI_DESCRIPTOR_NOTIFY_CALLBACK | EFI_PEI_PPI_DESCRIPTOR_TERMINATE_LIST),
  &gMeBeforeDidSentPpiGuid,
  (EFI_PEIM_NOTIFY_ENTRY_POINT) PchTraceHubPowerGateNotifyCallback
};

/**
  Notify function to disable and power gate Pch trace hub

  @param[in]  PeiServices       General purpose services available to every PEIM.
  @param[in]  NotifyDescriptor  Notify that this module published.
  @param[in]  Ppi               PPI that was installed.

  @retval     EFI_SUCCESS       The function completed successfully.
  @retval     EFI_UNSUPPORTED   The device is not supported
**/
EFI_STATUS
EFIAPI
PchTraceHubPowerGateNotifyCallback (
  IN EFI_PEI_SERVICES           **PeiServices,
  IN EFI_PEI_NOTIFY_DESCRIPTOR  *NotifyDescriptor,
  IN VOID                       *Ppi
  )
{
  return PchTraceHubDisable ();
}

/**
  Check some PCH policies are valid for debugging unexpected problem if these
  values are not initialized or assigned incorrect resource.

  @param[in] SiPreMemPolicy  The SI PreMem Policy PPI instance

**/
VOID
PchValidatePolicy (
  IN  SI_PREMEM_POLICY_PPI   *SiPreMemPolicy
  )
{
  EFI_STATUS                  Status;
  PCH_GENERAL_PREMEM_CONFIG   *PchGeneralPreMemConfig;

  Status = GetConfigBlock ((VOID *) SiPreMemPolicy, &gPchGeneralPreMemConfigGuid, (VOID *) &PchGeneralPreMemConfig);
  ASSERT_EFI_ERROR (Status);
  ASSERT (SiPreMemPolicy->TableHeader.Header.Revision == SI_PREMEM_POLICY_REVISION);
}

/**
  This function configures PCH SBU pins for PCH Type-C port.

  @param[in]  UsbTcPortEn        Bitmap for USB Type C enabled ports
**/
VOID
EFIAPI
PchSbuConfigure (
  IN UINT32      UsbTcPortEn
)
{
  EFI_STATUS                      Status;
  EFI_BOOT_MODE                   BootMode;
  UINT8                           DciPortId;

  DciPortId = 0;

  PeiServicesGetBootMode (&BootMode);
  ///
  /// Per USB-C spec, the SBU pins are required to be in high-Z/Open circuit in unused state
  /// (e.g.USB mode or SAFE mode) if debug is disabled for the corresponding Type-C port. As the
  /// BIOS has the ultimate information of the USB-C ports that are enabled on the platform, BIOS
  /// is required to initialize the PCH USB-C pins to high-Z/Open circuite prior to IOM CRRd to PMC
  /// only if
  /// 1.ExI.ECTRL.DEBUG.ENABLED = 0
  /// 2.After Platform Reset de-assertion
  ///

  if ((BootMode != BOOT_ON_S3_RESUME) && (BootMode != BOOT_ON_S4_RESUME) && (!IsDciDebugEnabled ())) {
    //
    // When debug is disabled
    //
    DEBUG ((DEBUG_INFO, "Configure PCH SBU pins\n"));
    Status = GpioDisableTypeCSbuDebug (UsbTcPortEn);
    if (EFI_ERROR (Status)) {
      DEBUG ((DEBUG_ERROR, "Fail to configure PCH SBU pins, Status = %r\n", Status));
    }
  }
  else if (Is2wireDciOobConnected (&DciPortId)) {
    Gpio2WireDciOobSetting (DciPortId);
  }
}

/**
  Initialize non-common clock port.
  Ports with NCC configuration need to have their mPHY lanes reconfigured by BIOS before
  endpoint detection can start. Reconfiguration is instant, but detection may take up to
  100ms. In order to save as much time as possible, this reconfiguration should be executed
  in PEI pre-mem, so that detection happens in parallel with memory init
  @param[in] RpIndex    Root Port index
**/
VOID
PchPcieInitNccPort (
  IN UINT32   RpIndex
  )
{
  UINT64                         RpBase;
  UINT32                         RpLaneIndex;
  UINT32                         MaxLinkWidth;
  HSIO_LANE                      HsioLane;
  UINT8                          FiaLane;
  P2SB_CONTROLLER                P2SbController;
  P2SB_SIDEBAND_REGISTER_ACCESS  SbAccess;

  DEBUG ((DEBUG_INFO, "PchPcieInitNccPort(%d)\n", RpIndex));

  RpBase = PchPcieRpPciCfgBase (RpIndex);
  MaxLinkWidth = GetMaxLinkWidth (RpBase);
  for (RpLaneIndex = 0; RpLaneIndex < MaxLinkWidth; ++RpLaneIndex) {
    if (PchFiaGetPcieRootPortLaneNum (RpIndex, RpLaneIndex, &FiaLane)) {
      ZeroMem (&HsioLane, sizeof (HSIO_LANE));
      PchHsioGetLane (FiaLane, &HsioLane, &P2SbController, &SbAccess);
      HsioPcieNccLaneInit (&HsioLane);
    }
  }
  PcieRpEnableBlockTransitionState (RpBase);
}

/**
  Initializes ports with NonCommonClock configuration. Such ports can't detect endpoints
  before NCC init ends. To prevent boot delays, NCC handling should happen in pre-mem
  rather than just before endpoint detection in post-mem
**/
VOID
PcieRpEarlyNccHandling (
  VOID
  )
{
  UINT32 RpIndex;

  for (RpIndex = 0; RpIndex < GetPchMaxPciePortNum (); RpIndex++) {
    if (IsPcieNcc (PchPcieRpPciCfgBase (RpIndex))) {
      PchPcieInitNccPort (RpIndex);
    }
  }
}

/**
  This function performs basic initialization for PCH in PEI phase after Policy produced.
  If any of the base address arguments is zero, this function will disable the corresponding
  decoding, otherwise this function will enable the decoding.
  This function locks down the AcpiBase.

  @param[in] SiPreMemPolicyPpi         The Silicon PreMem Policy PPI instance
**/
VOID
EFIAPI
PchOnPolicyInstalled (
  IN  SI_PREMEM_POLICY_PPI    *SiPreMemPolicyPpi
  )
{
  EFI_STATUS                  Status;
  PCH_GENERAL_PREMEM_CONFIG   *PchGeneralPreMemConfig;
  PCH_TRACE_HUB_PREMEM_CONFIG *PchTraceHubPreMemConfig;
  PCH_PCIE_RP_PREMEM_CONFIG   *PcieRpPreMemConfig;
  TCSS_PEI_PREMEM_CONFIG      *TcssPeiPreMemConfig;
  BOOLEAN                     DisableTraceHub;
  UINT8                       Index;
  UINT32                      UsbTcPortEn;
  P2SB_HANDLE                 P2SbHandle;
  P2SB_PRIVATE_CONFIG         P2SbPrivate;
  P2SB_CONTROLLER             P2SbController;
  P2SB_CALLBACK               P2SbCallback;

  PostCode (0xB48);
  DEBUG ((DEBUG_INFO, "PchOnPolicyInstalled() - Start\n"));

  UsbTcPortEn = 0;
  Status = GetConfigBlock ((VOID *) SiPreMemPolicyPpi, &gPchGeneralPreMemConfigGuid, (VOID *) &PchGeneralPreMemConfig);
  ASSERT_EFI_ERROR (Status);
  Status = GetConfigBlock ((VOID *) SiPreMemPolicyPpi, &gPchTraceHubPreMemConfigGuid, (VOID *) &PchTraceHubPreMemConfig);
  ASSERT_EFI_ERROR (Status);
  Status = GetConfigBlock ((VOID *) SiPreMemPolicyPpi, &gPcieRpPreMemConfigGuid, (VOID *) &PcieRpPreMemConfig);
  ASSERT_EFI_ERROR (Status);


  ///
  /// Set PWRM Base in PMC device
  ///
  Status = PmcSetPwrmBase (PmcPciCfgBase (), PCH_PWRM_BASE_ADDRESS);
  ASSERT_EFI_ERROR (Status);

  ///
  /// Set ACPI Base in PMC device
  ///
  PsfSetPmcAbase (PsfPmcPort (), PcdGet16 (PcdAcpiBaseAddress));

  //
  // HdAudio Init
  //
  HdaPreMemInit (SiPreMemPolicyPpi);

  //
  // Check to disable and lock WDT according to Policy.
  //
  WdtDisableAndLock (SiPreMemPolicyPpi);

  //
  // Configure BIOS HSIO Pre-Mem Settings
  //
  if (IsPchChipsetInitSyncSupported ()) {
    PostCode (0xB49);
    // HSIO SATA programming
    //
    PchHsioRxTxEqPolicySataProg (SiPreMemPolicyPpi);

    //
    // HSIO PCIe programming
    //
    PchHsioRxTxEqPolicyPcieProg (SiPreMemPolicyPpi);
  }

  PcieRpEarlyDisabling (PcieRpPreMemConfig);
  PcieRpEarlyNccHandling ();
  if (IsAdlPch ()
     ) {
    PcieRpNccWithSscHandling ();
  }

  PostCode (0xB4A);
  DciConfiguration (SiPreMemPolicyPpi);

  PostCode (0xB4D);
  DisableTraceHub = TraceHubInitialize ((UINT8) PchTraceHubPreMemConfig->TraceHub.EnableMode);
  if (DisableTraceHub) {
    //
    // Register notify to power gate pch trace hub
    // It needs to be power gated between ME GetImrSize and SendDID
    //
    if (EFI_ERROR (PeiServicesNotifyPpi (&mPeiPchTraceHubNotifyList))) {
      ASSERT (FALSE);
    }
  }


  PostCode (0xB15);
  Status = SmbusInit (SiPreMemPolicyPpi);

  if (PchGeneralPreMemConfig->Port80Route == PchReservedPageToLpc) {
    PchDmiSetReservedPageRegToLpc ();
  } else {
    PchDmiSetReservedPageRegToPcieRootPort ();
  }

  PostCode (0xB52);
  PchP2SbHandleInit (NULL, &P2SbHandle, &P2SbPrivate, &P2SbController, &P2SbCallback);
  P2sbHpetInit (&P2SbHandle);

  LpcOnPolicyConfigure (SiPreMemPolicyPpi);

  //
  // Configure PCH SBU pins for PCH Type-C ports
  //
  PostCode(0xB53);

  // Default set all ports are Type C enabled
  for (Index = 0; Index < GetPchMaxTypeCPortNum (); Index++) {
    UsbTcPortEn |= BIT0 << Index;
  }

  //
  // Get PCH Type-C info from TCSS config block if supported
  //
  Status = GetConfigBlock ((VOID *) SiPreMemPolicyPpi, &gTcssPeiPreMemConfigGuid, (VOID *) &TcssPeiPreMemConfig);
  if (!EFI_ERROR(Status)) {
    UsbTcPortEn = (UINT8) TcssPeiPreMemConfig->UsbTcConfig.UsbTcPortEn;
  }
  PchSbuConfigure (UsbTcPortEn);

  CnviInitPreMem (SiPreMemPolicyPpi);

  PostCode (0xB55);
  DEBUG ((DEBUG_INFO, "PchOnPolicyInstalled() - End\n"));
}

/**
  This code is intended to handle all cases where IP could be fuse disabled, static power gated
  or disabled due to soft strap lane assignment

  @retval EFI_SUCCESS
**/
VOID
PchEarlyDisabledDeviceHandling (
  VOID
  )
{
  UINT32                  SataCtrlIndex;
  UINT8                   Index;

  DEBUG ((DEBUG_INFO, "PchEarlyDisabledDeviceHandling() - Start\n"));

  ///
  /// BWG 5.3.1 Fuse Disabled, Static Disabled, non-Static function disable Device Handling
  ///

  //
  // Get PCIE Port configuration and disable value for each RP
  // and perform PSF function disable
  //
  GetConfigAndDisablePcieRootPort ();

  //
  // OTG fused
  //
  if (!PmcIsXdciSupported ()) {
    DEBUG ((DEBUG_INFO, "xDCI (OTG) Fuse disabled - performing PSF disable\n"));
    PsfDisableXdciDevice ();
  }
  //
  // SATA fused
  //
  for (SataCtrlIndex = 0; SataCtrlIndex < MaxSataControllerNum (); SataCtrlIndex++) {
    if (!PmcIsSataSupported (SataCtrlIndex)) {
      DEBUG ((DEBUG_INFO, "SATA %d Fuse disabled - performing PSF disable\n", SataCtrlIndex + 1));
      PsfDisableSataDevice (SataCtrlIndex);
    }
  }

  //
  // Serial Io fused
  //
  if (!PmcIsSerialIoSupported (PmcGetPwrmBase ())) {
    DEBUG ((DEBUG_INFO, "Serial Io Fuse disabled - performing PSF disable\n"));
    for (Index = 0; Index < GetPchMaxSerialIoI2cControllersNum (); Index++) {
      PsfDisableDevice (PsfSerialIoI2cPort (Index));
    }
    for (Index = 0; Index < GetPchMaxSerialIoSpiControllersNum (); Index++) {
      PsfDisableDevice (PsfSerialIoSpiPort (Index));
    }
    for (Index = 0; Index < GetPchMaxSerialIoUartControllersNum (); Index++) {
      PsfDisableDevice (PsfSerialIoUartPort (Index));
    }
  }

  //
  // Not all PCH support all of the I2C
  //
  SerialIoI2cEarlyDisable ();

#if defined(PCH_ADPP)
  ScsEarlyDisable ();
#endif
  //
  // ISH fused
  //
  if (PchIsIshSupported () && !PmcIsIshSupported ()) {
    DEBUG ((DEBUG_INFO, "ISH Supported and Fuse disabled - performing PSF disable\n"));
    PsfDisableDevice (PsfIshPort ());
  }

  //
  // Disable FPAK device at PSF level
  //
  PsfDisableFpakDevice ();

  //
  // Gbe fused
  //
  if (PchIsGbeSupported () && !PmcIsGbeSupported (PmcGetPwrmBase ())) {
    DEBUG ((DEBUG_INFO, "GBE Supported and Fuse disabled - performing PSF disable\n"));
    PsfDisableGbeDevice ();
  }

  if (!PmcIsCnviSupported ()) {
    DEBUG ((DEBUG_INFO, "CNVi WiFi function Fuse disabled - performing PSF disable\n"));
    PsfDisableDevice (PsfCnviPort ());
  }

  DEBUG ((DEBUG_INFO, "PchEarlyDisabledDeviceHandling() - End\n"));
}

/**
  Show PCH related information
**/
VOID
PchShowInformation (
  VOID
  )
{
  DEBUG_CODE_BEGIN ();
  CHAR8           Buffer[PCH_STEPPING_STR_LENGTH_MAX];

  DEBUG ((DEBUG_INFO, "PCH Series   : %a\n", PchGetSeriesStr ()));
  PchGetSteppingStr (Buffer, sizeof (Buffer));
  DEBUG ((DEBUG_INFO, "PCH Stepping : %a\n", Buffer));
  DEBUG ((DEBUG_INFO, "PCH SKU      : %a\n", PchGetSkuStr ()));
  PmcPrintResetReason ();
  DEBUG_CODE_END ();
}

/**
  PCH init pre-memory entry point
**/
VOID
EFIAPI
PchInitPrePolicy (
  VOID
  )
{
  EFI_STATUS            Status;
  P2SB_CONTROLLER       P2SbController;
  UINT8                 PcieRpFuncNumTable[PCH_MAX_PCIE_ROOT_PORTS];
  UINT32                PcieRpFuncNumTableSize;
  UINT32                Index;

  //
  // Dump PCH information
  //
  DEBUG_CODE_BEGIN ();
  PchShowInformation ();
  PchFiaPrintLosRegisters ();
  DEBUG_CODE_END ();

  PostCode (0xB00);
  DEBUG ((DEBUG_INFO, "PchInitPrePolicy() - Start\n"));

  //
  // Check if Pch is supported
  //
  if (!IsPchSupported ()) {
    DEBUG ((DEBUG_ERROR, "PCH SKU is not supported due to no proper PCH LPC found!\n"));
    ASSERT (FALSE);
  }

  //
  // Check if SBREG has been set.
  //
  Status = GetP2SbController (&P2SbController);
  if (EFI_ERROR (Status)) {
    ASSERT (FALSE);
  }
  if (!P2sbIsSbregReady (P2SbController.PciCfgBaseAddr)) {
    DEBUG ((DEBUG_INFO, "SBREG should be programmed before here\n"));
    ASSERT (FALSE);
  }

  //
  // Check if PCH PWRM Base has been set
  //
  DEBUG ((DEBUG_INFO, "PCH PWRM Base needs to be programmed before here\n"));
  ASSERT (PsfGetPmcPwrmBase (PsfPmcPort ()) != 0);

  //
  // Build Memory Mapped IO Resource which is used to build E820 Table in LegacyBios.
  // the resource range should be preserved in ACPI as well.
  //
  PchPreservedMmioResource ();

  //
  // Clear WdtAllowKnownReset that may persist after previous boot.
  //
  WdtClearAllowKnownReset ();

  ///
  /// If there was unexpected reset but no WDT expiration and no resume from S3/S4,
  /// clear unexpected reset status and enforce expiration. This is to inform Firmware
  /// which has no access to unexpected reset status bit, that something went wrong.
  ///
  OcWdtResetCheck ();

  //
  // Initialize WDT and install WDT PPI
  //
  WdtInstallResetCallback ();
  OcWdtInit ();

  //
  // Installs PCH SPI PPI
  //
  SpiServiceInit ();

  PostCode (0xB02);

  //
  // Perform PSF early initialization.
  //
  PcieRpFuncNumTableSize = GetPchMaxPciePortNum ();
  for (Index = 0; Index < PcieRpFuncNumTableSize; Index++) {
    PcieRpFuncNumTable[Index] = PchPcieRpFuncNumber (Index);
  }
  PsfEarlyInit (PsfGetEarlyInitData (), PsfGetSegmentTable (), PcieRpFuncNumTable, PcieRpFuncNumTableSize);

  //
  // Before any interrupt is enabled, set master message enable.
  //
  ItssSetMasterMessageEnable ();
  //
  // The 8259 PIC is still functional and not masked by default,
  // even if APIC is enabled.
  // So need to set all 8259 interrupts to disabled.
  //
  ItssMask8259Interrupts ();

  //
  // PCH-H DMI early init (before any upstream bus master transactions are enabled)
  //
  if (IsPchH ()) {
    PchDmi15EarlyInit ();
  }

  //
  // According to bios spec,
  // Setup "uCode Patch Region Enable" in DMI
  //
  PchDmiEnableUCodePatchRegion ();

  //
  // Do P2SB early configuration.
  //
  P2sbEarlyConfig (&P2SbController, TRUE);

  //
  // Configura RTC when power failure case.
  //
  if (!PmcIsRtcBatteryGood ()) {
    RtcPowerFailureConfiguration ();
  }

  //
  // Clear CF9GR if it's set in previous boot.
  //
  PmcDisableCf9GlobalReset (PmcGetPwrmBase ());

  //
  // Enhance port 8xh LPC decoding
  //
  LpcOnEarlyPeiConfigure ();

  //
  // Handle all static disabled controllers.
  //
  PchEarlyDisabledDeviceHandling ();

  //
  // Perform some early configuration for pcie root ports
  //
  EarlyPcieRootPortConfig ();

  //
  // Disable EISS for variable service
  //
  SpiDisableEiss ();
  EspiDisableEiss ();

  PostCode (0xB7F);
  DEBUG ((DEBUG_INFO, "PchInitPrePolicy() - End\n"));
}

