/** @file
  The PCH Initialization Dispatcher After Memory.

@copyright
  INTEL CONFIDENTIAL
  Copyright 2013 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains an 'Intel Peripheral Driver' and is uniquely identified as
  "Intel Reference Module" and is licensed for Intel CPUs and chipsets under
  the terms of your license agreement with Intel or your vendor. This file may
  be modified by the user, subject to additional terms of the license agreement.

@par Specification Reference:
**/

#include "PchInitPei.h"
#include <Library/PostCodeLib.h>
#include <Library/PchDmiLib.h>
#include <Library/PeiGbeInitLib.h>
#include <Library/PeiP2sbPrivateLib.h>
#include <Library/PeiRtcLib.h>
#include <Library/PeiRstPrivateLib.h>
#include <Library/PchInfoLib.h>
#include <Library/PeiSataLib.h>
#include <Library/PeiIshInitLib.h>
#include <Library/PeiTsnInitLib.h>
#include <Library/SerialIoI2cMasterLib.h>
#include <Library/PeiSerialIoInitLib.h>
#include <Library/PeiSmbusLib.h>
#include <Library/PeiIehInitLib.h>
#include <Library/PeiPcieRpInitLib.h>
#include <Library/PsfSocLib.h>
#include <Library/PeiPsfLib.h>
#include <Library/PsfSocLib.h>
#include <Library/PeiFusaLib.h>
#include <Library/PchInitLib.h>
#include <Ppi/SiPolicy.h>
#include <Library/PeiHybridStorageLib.h>
#include <Library/PchAdrLib.h>
#include <Library/ChipsetInitLib.h>
#if FixedPcdGetBool(PcdFspBinaryEnable) == 0
#include <Library/PchSmmControlLib.h>
#endif
#include <Library/PmcSocLib.h>

/**
  The function issues reset based on SI_SCHEDULE_RESET_HOB
**/
STATIC
VOID
PchPeiReset (
  VOID
  )
{
  SA_MISC_PEI_PREMEM_CONFIG *MiscPeiPreMemConfig;
  EFI_STATUS                Status;
  SI_PREMEM_POLICY_PPI      *SiPreMemPolicyPpi;
  BOOLEAN                   ResetStatus;

  if (!SiScheduleResetIsRequired ()) {
    return;
  }

  Status = PeiServicesLocatePpi (
             &gSiPreMemPolicyPpiGuid,
             0,
             NULL,
             (VOID **) &SiPreMemPolicyPpi
             );

  MiscPeiPreMemConfig = NULL;
  Status = GetConfigBlock ((VOID *) SiPreMemPolicyPpi, &gSaMiscPeiPreMemConfigGuid, (VOID *) &MiscPeiPreMemConfig);
  if (Status != EFI_NOT_FOUND) {
    if (MiscPeiPreMemConfig == NULL) {
      return;
    }

    if (MiscPeiPreMemConfig->S3DataPtr == NULL) {
      DEBUG((DEBUG_INFO, "MRC Data not valid. Postpone reset to DXE\n"));
      return;
    }
  }
  ResetStatus = SiScheduleResetPerformReset ();
  ASSERT (!ResetStatus);
}

/**
  Pch init after memory PEI module

  @param[in] SiPolicy                    The Silicon Policy PPI instance
  @param[in] PchUpstreamComponentConfig  PCH upstream die config

  @retval None
**/
VOID
PchInit (
  IN  SI_POLICY_PPI                  *SiPolicy,
  IN  PCH_UPSTREAM_COMPONENT_CONFIG  *PchUpstreamComponentConfig
  )
{
  PMC_SOC_CONFIG      PmcSocConfig;
  PSF_CONFIG          *PsfConfig;
  EFI_STATUS          Status;

  PostCode (0xB00); // PCH API Entry
  DEBUG ((DEBUG_INFO, "PchInit - Start\n"));

#if FixedPcdGetBool(PcdFspBinaryEnable) == 0
  //
  // Install PEI SMM Control PPI
  //
  PchSmmControlInit ();
#endif

  //
  // Build and Update PCH configuration HOB. This should be done before device init starts.
  //
  BuildPchConfigHob (SiPolicy);

  //
  // Configure PCH HSIO (ModPhy)
  //
  if (IsPchChipsetInitSyncSupported ()) {
    PchPcieHsioConfigure (SiPolicy);
    PchHsioConfigure (SiPolicy);
  }

  //
  // SATA Controller initialization
  //
  PchSataInit (SiPolicy, PchUpstreamComponentConfig);

#if FixedPcdGetBool (PcdHybridStorageSupport) == 1
    HybridStorageCheckAndConfigureClkreq ();
    PchConfigureGbeClkReqMapping ();
#else
    PchConfigureClkreqMapping ();
#endif
  //
  // Configure GbE LAN
  //
  if (PchIsGbeSupported ()) {
    PchGbeInit (SiPolicy);
  }

  //
  // Configure TSN
  //
  if (PchIsTsnSupported ()) {
    TsnInit (SiPolicy);
  }

  PchExtendedBiosDecodeRangeInit (SiPolicy);

  //
  // PchInfo HOB must be created before PCIe root port initialization, because
  // afterwards it cannot be determined which ports were fused off
  //
  BuildPchInfoHob ();

  //
  // Configure PCH PCIe Root Ports
  //
  PcieRpInit (SiPolicy);

  DisableUnusedPcieClocks ();

  //
  // Handle PCH PSF Disable
  //
  PchPsfDisableP2pDecoding (PsfGetSegmentTable ());

  //
  // Configure PCH USB controllers
  //
  PchUsbConfigure (SiPolicy);

  //
  // Configure DMI
  //
  PostCode (0xB0A);
  if (IsPchH () || IsPchS ()) {
    PchDmi15Init (SiPolicy);
  } else {
    PchDmi14Init (SiPolicy);
  }

  //
  // Configure P2SB
  //
  PostCode (0xB0B);
  PchP2SbInit (SiPolicy);

  //
  // Configure PSTH
  //
  PchPsthConfigure (SiPolicy);

  //
  // Set PSF Multicast EOI forwarding for ITSS (with IOAPIC controller)
  // P2SB needs to be set as EOI Target because interrupt controller (ITSS)
  // is a sideband device located behind P2SB
  //
  PsfEnableEoiTarget (PsfP2sbDestinationId (), PsfGetSegmentTable(), PsfGetEoiRegDataTable ());
  //
  // Configure IOAPIC
  //
  PostCode (0xB0C);
  ItssInitIoApic (SiPolicy);

  //
  // Configure interrupts.
  //
  PostCode (0xB0D);
  ItssConfigureInterrupts (SiPolicy);

  //
  // Configure CNVi devices
  //
  CnviInit (SiPolicy);

  //
  // Touch Host Controller
  //
  ThcInit (SiPolicy);

#if defined(PCH_ADPP)
  PostCode (0xB13);
  ScsInit (SiPolicy);
#endif

  //
  // Configure Integrated Sensor Hub (ISH)
  //
  if (PchIsIshSupported ()) {
    PostCode (0xB14);
    IshInit (SiPolicy);
  }

  //
  // Configure GPIO PM and interrupt settings
  //
  GpioConfigurePm ();
  GpioSetIrq (ItssGetGpioDevIntConfig (SiPolicy));

  //
  // Perform initial IO Standby State related configurations
  //
  GpioConfigureIoStandbyState ();

  //
  // Configure PSF PM settings
  //
  Status = GetConfigBlock ((VOID *) SiPolicy, &gPsfConfigGuid, (VOID *) &PsfConfig);
  ASSERT_EFI_ERROR (Status);
  PsfConfigurePowerManagement (PsfConfig, PsfGetSegmentTable ());

  //
  // Configure RTC
  //
  PchRtcConfigure (SiPolicy);

  //
  // Configure SMBUS
  //
  PostCode (0xB15);
  SmbusConfigure (SiPolicy);

  PmcPchFivrInit (SiPolicy);

  //
  // Configure LPC PM
  //
  LpcConfigurePm ();

  //
  // Hide PMC PciCfgSpace
  //
  PsfHideDevice (PsfPmcPort ());

  //
  // Configure Serial IO
  //
  SerialIoInit (SiPolicy);

  //
  // Install I2C protocol for PEI use.
  //
  InstallI2cMasterPpi (0);

  //
  // Configure eSPI after memory
  //
  PchEspiInit (SiPolicy);

  //
  // Configure IEH
  //
  PchIehInit (SiPolicy);


  //
  // SATA controller after RST configuration
  //
  PchSataAfterRstInit (SiPolicy, PchUpstreamComponentConfig);

  //
  // Configure PSF PCIe Grant Counts after PCIe Root Ports are initialized
  // and unused ports are disabled
  //
  PcieRpConfigureGrantCounts ();

  //
  // Configure Serial IO Function 0
  // This has to happen late here after all other PCH devices (non serial) are configured because non serial devices
  // may share device number with func0 serial Io
  //
  SerialIoFunction0Disable (SiPolicy);

  //
  // Has to be done before PMC Init to ensure the USB2PHY and ModPHY Power Gating
  // settings are turned on after CSME has the correct SUS tables.
  //
  ChipsetInitSync ();

  //
  // Configure PMC settings
  //
  PmcGetSocConfig (&PmcSocConfig);
  CopyMem (&PmcSocConfig.AdrSocConfig, &PchUpstreamComponentConfig->AdrConfig, sizeof (PMC_ADR_SOC_CONFIG));
  PmcSocConfig.AdrSocConfig.AdrGpio = AdrGpioB;
  if (IsPchS ()) {
    PmcSocConfig.AdrSocConfig.AdrGenCfgMoved = TRUE;
  } else {
    PmcSocConfig.AdrSocConfig.AdrGenCfgMoved = FALSE;
  }
  PmcInit (SiPolicy, &PmcSocConfig);

  //
  // Configure CNVi BT
  //
  CnviConfigureBt (SiPolicy);

  if (PmcSocConfig.AdrSocConfig.Supported) {
    PchAdrConfigure (&PmcSocConfig.AdrSocConfig);
  }

  //
  // Lock FIVR Configuration
  //
  PmcLockFivrConfig ();

  //
  // Issue Reset based on SiScheduleResetHob
  //
  PchPeiReset ();

  PostCode (0xB7F);  // PCH API Exit
  DEBUG ((DEBUG_INFO, "PchInit - End\n"));
}
