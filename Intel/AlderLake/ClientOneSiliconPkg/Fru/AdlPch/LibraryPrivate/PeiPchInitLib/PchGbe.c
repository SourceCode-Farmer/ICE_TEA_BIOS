/** @file
  The GBE controller SoC specific implementation.

@copyright
  INTEL CONFIDENTIAL
  Copyright 2020 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains an 'Intel Peripheral Driver' and is uniquely identified as
  "Intel Reference Module" and is licensed for Intel CPUs and chipsets under
  the terms of your license agreement with Intel or your vendor. This file may
  be modified by the user, subject to additional terms of the license agreement.

@par Specification Reference:
**/

#include <Ppi/SiPolicy.h>

#include <Library/DebugLib.h>
#include <Library/PcdLib.h>
#include <Library/BaseMemoryLib.h>
#include <Library/PchInfoLib.h>
#include <Library/ConfigBlockLib.h>
#include <Library/PeiGbeInitLib.h>
#include <Library/PmcSocLib.h>
#include <Library/PmcLib.h>
#include <Library/PchFiaLib.h>
#include <Library/PeiItssLib.h>
#include <Library/PeiPchPcieClocksLib.h>
#include <Library/PchPciBdfLib.h>
#include <Library/PchPolicyLib.h>
#include <Register/PchRegs.h>
#include <PmConfig.h>
#include <GbeConfig.h>
#include <GbeHandle.h>
#include <PchPcieRpConfig.h>
#include <Library/GbeSocLib.h>
#include <Library/FiaSocLib.h>

/**
  This function checks if GbE controller is supported (not disabled by fuse)

  @param[in]  GbeController  Pointer to GBE controller structure

  @retval GbE support state
**/
STATIC
BOOLEAN
GbeIsSupported (
  IN  GBE_CONTROLLER          *GbeController
  )
{
  return PmcIsGbeSupported (PmcGetPwrmBase ());
}

/**
  This function checks if GbE is function disabled
  by static power gating

  @param[in]  GbeController  Pointer to GBE controller structure

  @retval GbE device state
**/
STATIC
BOOLEAN
GbeIsFunctionDisabled (
  IN  GBE_CONTROLLER          *GbeController
  )
{
  return PmcIsGbeFunctionDisabled ();
}

/**
  This function enables GbE controller by disabling static power gating.
  Static power gating disabling takes place after Global Reset, G3 or DeepSx transition.

  @param[in]  GbeController  Pointer to GBE controller structure
**/
STATIC
VOID
GbeEnable (
  IN  GBE_CONTROLLER          *GbeController
  )
{
  PmcEnableGbe ();
}

/**
  This function disables GbE device by static power gating and enables ModPHY SPD gating (PCH-LP only).
  For static power gating to take place Global Reset, G3 or DeepSx transition must happen.

  @param[in]  GbeController  Pointer to GBE controller structure
**/
STATIC
VOID
GbeDisable (
  IN  GBE_CONTROLLER          *GbeController
  )
{
  PmcStaticDisableGbe ();
}

/**
  Disables one PCIe clock.

  @param[in]  GbeController   Pointer to GBE controller structure
**/
STATIC
VOID
GbeDisableClock (
  IN  GBE_CONTROLLER       *GbeController
  )
{
  DisableClock (PchClockUsageLan);
}

/**
  This function enables GBE ModPHY SPD gating.

  @param[in]  GbeController   Pointer to GBE controller structure
**/
STATIC
VOID
GbeModPhyPowerGating (
  IN  GBE_CONTROLLER           *GbeController
  )
{
  PmcGbeModPhyPowerGating ();
}

/**
  Load default values to GBE structures
  - Initialize data structures to zero
  - Initialize function pointers to NULL
  - Initialize pointes for GBE_HANDLE substructures

  @param[in out]  GbeHandle      Pointer to GBE handle structure
  @param[in out]  GbePrivate     Pointer to GBE private configuration structure
  @param[in out]  GbeController  Pointer to GBE controller identification structure
  @param[in out]  GbeCallback    Pointer to GBE callback structure
**/
STATIC
VOID
GbeHandleLoadDefaults (
  IN OUT  GBE_HANDLE          *GbeHandle,
  IN OUT  GBE_PRIVATE_CONFIG  *GbePrivate,
  IN OUT  GBE_CONTROLLER      *GbeController,
  IN OUT  GBE_CALLBACK        *GbeCallback
  )
{
  ZeroMem (GbePrivate, sizeof (GBE_PRIVATE_CONFIG));
  GbeHandle->PrivateConfig = GbePrivate;

  ZeroMem (GbeCallback, sizeof (GBE_CALLBACK));
  GbeHandle->Callback = GbeCallback;

  ZeroMem (GbeController, sizeof (GBE_CONTROLLER));
  GbeHandle->Controller = GbeController;

  GbeHandle->GbeConfig = NULL;
}

/**
  Initialize GBE structures
  This should be done according GBE IP integration in SoC

  @param[in]      SiPolicy       Pointer to Silicon Policy
  @param[in out]  GbeHandle      Pointer to GBE handle structure
  @param[in out]  GbePrivate     Pointer to GBE private configuration structure
  @param[in out]  GbeController  Pointer to GBE controller identification structure
  @param[in out]  GbeCallback    Pointer to GBE callback structure
**/
STATIC
VOID
GbeHandleInit (
  IN      SI_POLICY_PPI       *SiPolicy,
  IN OUT  GBE_HANDLE          *GbeHandle,
  IN OUT  GBE_PRIVATE_CONFIG  *GbePrivate,
  IN OUT  GBE_CONTROLLER      *GbeController,
  IN OUT  GBE_CALLBACK        *GbeCallback
  )
{
  EFI_STATUS            Status;
  PCH_PM_CONFIG         *PmConfig;

  if (SiPolicy == NULL) {
    ASSERT (FALSE);
  }
  //
  // Initialize: data with defaults, function pointers to NULL, substructures pointers
  //
  GbeHandleLoadDefaults (GbeHandle, GbePrivate, GbeController, GbeCallback);

  //
  // Initialize GBE Private Configuration
  //
  if (SiPolicy) {
    Status = GetConfigBlock ((VOID *) SiPolicy, &gPmConfigGuid, (VOID *) &PmConfig);
    if (!EFI_ERROR (Status)) {
      GbePrivate->WolEnableOverride = PmConfig->WakeConfig.WolEnableOverride != 0;
    }
  GbePrivate->InterruptPin = ItssGetDevIntPin (SiPolicy, GbeDevNumber (), GbeFuncNumber ());
  }

  if (!PchFiaGetGbeLaneNum (&GbePrivate->LaneNum)) {
    GbePrivate->LaneNum = 0xFF; // no lane assigned
  }

  if (IsClkReqAssigned (PchClockUsageLan)) {
    GbePrivate->HasClkReq = TRUE;
  }


  //
  // Initialize GBE callback pointers
  //
  GbeCallback->IsSupported = GbeIsSupported;
  GbeCallback->IsFunctionDisabled = GbeIsFunctionDisabled;
  GbeCallback->Enable = GbeEnable;
  GbeCallback->Disable = GbeDisable;
  GbeCallback->ModPhyPowerGating = GbeModPhyPowerGating;
  GbeCallback->DisableClk = GbeDisableClock;

  //
  // Set pointer to GBE Config Block
  //
  if (SiPolicy) {
    Status = GetConfigBlock ((VOID *) SiPolicy, &gGbeConfigGuid, (VOID *) &GbeHandle->GbeConfig);
    ASSERT (GbeHandle->GbeConfig != NULL);
  }

  //
  // Initialize GBE device data
  //
  Status = GbeGetController (GbeController);
  if (EFI_ERROR (Status)) {
    DEBUG ((DEBUG_ERROR, "%a: ERROR: Cannot get GBE controller!\n", __FUNCTION__));
    ASSERT (FALSE);
  }

  GbeHandle->Mmio           = PcdGet32 (PcdSiliconInitTempMemBaseAddr);
}

/**
  Initialize LAN device

  @param[in]      SiPolicy       Pointer to Silicon Policy
**/
VOID
PchGbeInit (
  IN  SI_POLICY_PPI  *SiPolicy
  )
{
  GBE_HANDLE           GbeHandle;
  GBE_PRIVATE_CONFIG   GbePrivate;
  GBE_CONTROLLER       GbeController;
  GBE_CALLBACK         GbeCallback;

  GbeHandleInit (SiPolicy, &GbeHandle, &GbePrivate, &GbeController, &GbeCallback);

  //
  // Enable CLKREQ# if supported by board regardless of GbE being enabled
  // to allow clock shut-down.
  //
  if (GbePrivate.HasClkReq) {
    EnableClkReq (PchClockUsageLan);
  }

  GbeInit (&GbeHandle);
}
