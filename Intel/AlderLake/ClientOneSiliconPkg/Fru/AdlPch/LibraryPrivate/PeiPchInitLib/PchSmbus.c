/** @file
  PCH SMBUS init.

@copyright
  INTEL CONFIDENTIAL
  Copyright 2020 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains an 'Intel Peripheral Driver' and is uniquely identified as
  "Intel Reference Module" and is licensed for Intel CPUs and chipsets under
  the terms of your license agreement with Intel or your vendor. This file may
  be modified by the user, subject to additional terms of the license agreement.

@par Specification Reference:
**/
#include "PchInitPei.h"
#include <Library/PeiItssLib.h>
#include <Library/PchPciBdfLib.h>
#include <Library/PeiSmbusLib.h>
#include <Library/P2SbSocLib.h>
#include <Register/PchRegs.h>

// Number of SMBUS controllers
#define MAX_SMBUS_CONTROLLER       1

// Number of ports per SATA controller
#define MAX_SMBUS_PORT             1

// Smbus Controller Index
#define SMBUS_1_CONTROLLER_INDEX   0

/**
Get Maximum Smbus Controller Number

@retval Maximum Smbus Controller Number
**/
UINT8
MaxSmbusPortNum (
  VOID
)
{
  //
  // Defined in SoC integration header
  //
  return MAX_SMBUS_PORT;
}

/**
Get Maximum Smbus Controller Number

@retval Maximum Smbus Controller Number
**/
UINT8
MaxSmbusControllerNum (
  VOID
)
{
  //
  // Defined in SoC integration header
  //
  return MAX_SMBUS_CONTROLLER;
}

/**
This Function initializes SMBUS Controller

@param[in]  SmbusCtrlIndex        SMBUS controller index
@param[out] SmbusController       Pointer to SmbusController structure

@retval     EFI_SUCCESS           - Completted successfully
            EFI_INVALID_PARAMETER - Smbusontroller NULL or SmbusCtrlIndex out of the range
**/
EFI_STATUS
SmbusGetController (
  IN     UINT32             SmbusCtrlIndex,
  IN OUT SMBUS_CONTROLLER   *SmbusController
  )
{
  if (SmbusController == NULL) {
    DEBUG ((DEBUG_ERROR, "%a: Null pointer at SmbusHandle \n", __FUNCTION__));
    return EFI_INVALID_PARAMETER;
  }
  if (SmbusCtrlIndex >= MaxSmbusControllerNum ()) {
    DEBUG ((DEBUG_ERROR, "%a: Controller number out of the range!\n", __FUNCTION__));
    return EFI_INVALID_PARAMETER;
  }

  SmbusController->SmbusCtrlIndex   = SmbusCtrlIndex;
  SmbusController->Segment          = DEFAULT_PCI_SEGMENT_NUMBER_PCH;
  SmbusController->Bus              = DEFAULT_PCI_BUS_NUMBER_PCH;
  SmbusController->Device           = SmbusDevNumber ();
  SmbusController->Function         = SmbusFuncNumber ();
  SmbusController->PciCfgBase       = SmbusPciCfgBase ();
  SmbusController->TotalCtrlPortNum = MaxSmbusPortNum ();
  SmbusController->AcpiBase         = PmcGetAcpiBase ();

  return EFI_SUCCESS;
}

/**
  Configures GPIO pins for SMBUS Interface

  @param[in]  SMBUS_HANDLE     Pointer to SMBUS Handle structure

  @retval EFI_STATUS      Status returned by worker function
**/
EFI_STATUS
EnableSmbusGpio (
  IN  SMBUS_HANDLE       *SmbusHandle
  )
{
  EFI_STATUS Status;

  if (SmbusHandle == NULL) {
    DEBUG ((DEBUG_ERROR, "%a: NULL pointer detected!\n", __FUNCTION__));
    return EFI_INVALID_PARAMETER;
  }

  Status = GpioEnableSmbus ();

  DEBUG ((DEBUG_INFO, "%a: Status: %r\n", __FUNCTION__, Status));

  return Status;
}

/**
  Configures GPIO pins for SMBUS Alert

  @param[in]  SMBUS_HANDLE     Pointer to SMBUS Handle structure

  @retval EFI_STATUS      Status returned by worker function
**/
EFI_STATUS
EnableSmbusAlert (
  IN  SMBUS_HANDLE       *SmbusHandle
  )
{
  EFI_STATUS Status;

  if (SmbusHandle == NULL) {
    DEBUG ((DEBUG_ERROR, "%a: NULL pointer detected!\n", __FUNCTION__));
    return EFI_INVALID_PARAMETER;
  }

  Status = GpioEnableSmbusAlert ();

  DEBUG ((DEBUG_INFO, "%a: Status: %r\n", __FUNCTION__, Status));

  return Status;
}

/**
  Disables SMBUS controller in power controller and fabric

  @param[in]  SMBUS_HANDLE     Pointer to SMBUS Handle structure

**/
VOID
SmbusCtrlDisable (
  IN  SMBUS_HANDLE       *SmbusHandle
  )
{

  if (SmbusHandle == NULL) {
    DEBUG ((DEBUG_ERROR, "%a: NULL pointer detected!\n", __FUNCTION__));
    return;
  }

  PsfDisableSmbusDevice();
}

/**
  Load default values to Smbus structures

  @param[in out]  SmbusHandle            Pointer to Smbus handle structure
  @param[in out]  SmbusController        Pointer to Smbus Controller structure
  @param[in out]  SmbusSocConfig         Pointer to Smbus Soc structure
  @param[in out]  SmbusCallbacks         Pointer to Smbus callback structure
**/
VOID
SmbusHandleLoadDefaults (
  IN OUT  SMBUS_HANDLE                *SmbusHandle,
  IN OUT  SMBUS_CONTROLLER            *SmbusController,
  IN OUT  SMBUS_SOC_CONFIG            *SmbusSocConfig,
  IN OUT  SMBUS_CALLBACKS             *SmbusCallbacks
)
{

  ZeroMem(SmbusController, sizeof(SMBUS_CONTROLLER));
  SmbusHandle->Controller = SmbusController;

  ZeroMem(SmbusSocConfig, sizeof(SMBUS_SOC_CONFIG));
  SmbusHandle->SocConfig = SmbusSocConfig;

  ZeroMem(SmbusCallbacks, sizeof(SMBUS_CALLBACKS));
  SmbusHandle->Callbacks = SmbusCallbacks;

  SmbusHandle->Config = NULL;
}
/**
  Initializes SMBUS handle.

  @param[in]  SiPolicyPpi         Pointer to SiPolicy
  @param[in]  SiPreMemPolicyPpi   Pointer to SiPreMemPolicyPpi
  @param[in]  SmbusHandle         P2SB Register Access to Smbus
  @param[in]  ControllerIndex     Controller Index of Smbus Controller

  @param[out] SmbusHandle         Initialized handle on output
  @param[out] Controller          Initialized Controller on output
  @param[out] SocConfig           Initialized SocConfig on output
  @param[out] Callbacks           Initialized Callbacks on output

  @retval     VOID
**/
VOID
InitSmbusHandle (
  IN  SI_POLICY_PPI                  *SiPolicyPpi,
  IN  SI_PREMEM_POLICY_PPI           *SiPreMemPolicyPpi,
  IN  UINT32                          ControllerIndex,
  IN  P2SB_SIDEBAND_REGISTER_ACCESS  *SmbusSbAccess,
  OUT P2SB_CONTROLLER                *P2SbController,
  OUT SMBUS_HANDLE                   *SmbusHandle,
  OUT SMBUS_CONTROLLER               *Controller,
  OUT SMBUS_SOC_CONFIG               *SocConfig,
  OUT SMBUS_CALLBACKS                *Callbacks
  )
{
  EFI_STATUS                   Status;
  UINT8                        InterruptPin;
  UINT8                        Irq;
  PCH_SMBUS_PREMEM_CONFIG     *SmbusPreMemConfig;

  //
  // Initialize structures to default
  //
  SmbusHandleLoadDefaults (SmbusHandle, Controller, SocConfig, Callbacks);

  //
  // Get Smbus Controller
  //
  SmbusGetController (ControllerIndex, Controller);

  Status = PeiServicesLocatePpi (
             &gSiPreMemPolicyPpiGuid,
             0,
             NULL,
             (VOID **)&SiPreMemPolicyPpi
             );
  ASSERT_EFI_ERROR (Status);
  Status = GetConfigBlock ((VOID *) SiPreMemPolicyPpi, &gSmbusPreMemConfigGuid, (VOID *) &SmbusPreMemConfig);
  ASSERT_EFI_ERROR (Status);

  SmbusHandle->Config = SmbusPreMemConfig;

  //
  //  Initialize Smbus Callback
  //
  Callbacks->SmbusDisable     = SmbusCtrlDisable;
  Callbacks->SmbusAlertEnable = EnableSmbusAlert;
  Callbacks->SmbusGpioEnabled = EnableSmbusGpio;

  // Adding this check to make sure this SiPolicyPpi is not queried in PreMem phase
  if ( SiPolicyPpi != NULL) {
    ItssGetDevIntConfig (
        SiPolicyPpi,
        SmbusDevNumber (),
        SmbusFuncNumber (),
        &InterruptPin,
        &Irq
        );

    SocConfig->IntPin = InterruptPin;
    SocConfig->Irq = Irq;
    SocConfig->TcoIrq = ItssGetTcoDevIntConfig (SiPolicyPpi);
  }
  SocConfig->PowerManagementSupport = TRUE;
  SocConfig->TcoSmiTimeoutSupport = TRUE;
  SocConfig->HostNotifyWakeSupport = TRUE;

  Status = GetP2SbController (P2SbController);
  ASSERT_EFI_ERROR (Status);
  if (EFI_ERROR (Status)) {
    return;
  }
  //
  // Intialize Smbus P2SB Register Access
  //
  BuildP2SbSidebandAccess (
      P2SbController,
      PID_SMB,
      0,
      P2SbMemory,
      P2SbMmioAccess,
      FALSE,
      SmbusSbAccess
      );
  SmbusHandle->SmbusSbAccessMmio = (REGISTER_ACCESS *)SmbusSbAccess;

  return;
}

/**
  Initialize the Smbus PPI and program the Smbus BAR

  @param[in]  SiPreMemPolicyPpi   The SI PreMem Policy PPI instance

  @retval EFI_SUCCESS             The function completes successfully
  @retval others                  Failed to Initialize
**/
EFI_STATUS
SmbusInit (
  IN  SI_PREMEM_POLICY_PPI   *SiPreMemPolicyPpi
  )
{
  EFI_STATUS                      Status;
  SMBUS_HANDLE                    SmbusHandle;
  SMBUS_CONTROLLER                Controller;
  SMBUS_SOC_CONFIG                SocConfig;
  SMBUS_CALLBACKS                 Callbacks;
  P2SB_SIDEBAND_REGISTER_ACCESS   SmbusSbAccess;
  P2SB_CONTROLLER                 P2SbController;

  DEBUG ((DEBUG_INFO, "InitializePchSmbus() Start\n"));
  //
  // Initialize SMBUS handle.
  //
  InitSmbusHandle (NULL, SiPreMemPolicyPpi, SMBUS_1_CONTROLLER_INDEX, &SmbusSbAccess, &P2SbController, &SmbusHandle, &Controller, &SocConfig, &Callbacks);

  //
  // Initialize SMBUS Ppi
  //
  Status = SmbusPreMemInit (&SmbusHandle);
  if (Status != EFI_SUCCESS) {
    return Status;
  }
  DEBUG ((DEBUG_INFO, "InitializePchSmbus() End\n"));
  return EFI_SUCCESS;
}

/**
  The function performs SMBUS specific programming.

  @param[in] SiPolicyPpi       The SI Policy PPI instance

**/
VOID
SmbusConfigure (
  IN  SI_POLICY_PPI           *SiPolicyPpi
  )
{
  SI_PREMEM_POLICY_PPI           *SiPreMemPolicyPpi = NULL;
  SMBUS_HANDLE                    SmbusHandle;
  SMBUS_CONTROLLER                Controller;
  SMBUS_SOC_CONFIG                SocConfig;
  SMBUS_CALLBACKS                 Callbacks;
  P2SB_SIDEBAND_REGISTER_ACCESS   SmbusSbAccess;
  P2SB_CONTROLLER                 P2SbController;
  //
  // Initialize SMBUS handle.
  //
  InitSmbusHandle (SiPolicyPpi, SiPreMemPolicyPpi, SMBUS_1_CONTROLLER_INDEX, &SmbusSbAccess, &P2SbController, &SmbusHandle, &Controller, &SocConfig, &Callbacks);
  //
  // Initialize SMBUS Controller
  //
  SmbusPostMemConfigure (&SmbusHandle);
}
