/** @file
  Header file for the PCH Init PEIM

@copyright
  INTEL CONFIDENTIAL
  Copyright 2013 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains an 'Intel Peripheral Driver' and is uniquely identified as
  "Intel Reference Module" and is licensed for Intel CPUs and chipsets under
  the terms of your license agreement with Intel or your vendor. This file may
  be modified by the user, subject to additional terms of the license agreement.

@par Specification Reference:
**/
#ifndef _PCH_INIT_PEI_H_
#define _PCH_INIT_PEI_H_

#include <Library/DebugLib.h>
#include <Library/IoLib.h>
#include <Library/MemoryAllocationLib.h>
#include <Library/BaseMemoryLib.h>
#include <Library/HobLib.h>
#include <Library/PeiServicesLib.h>
#include <Library/PeiServicesTablePointerLib.h>
#include <Library/TimerLib.h>
#include <Library/ConfigBlockLib.h>
#include <Library/PchCycleDecodingLib.h>
#include <Library/EspiLib.h>
#include <Library/PchPcieRpLib.h>
#include <Library/PchPcrLib.h>
#include <Library/PchSbiAccessLib.h>
#include <Library/PchInfoLib.h>
#include <Library/SataSocLib.h>
#include <Library/PeiPchTraceHubLib.h>
#include <Library/PciSegmentLib.h>
#include <Library/PchPolicyLib.h>
#include <Library/PchFiaLib.h>
#include <Library/PciExpressHelpersLib.h>
#include <Ppi/SiPolicy.h>
#include <Library/GpioLib.h>
#include <Library/GpioNativeLib.h>
#include <Library/PmcLib.h>
#include <Library/GpioPrivateLib.h>
#include <IndustryStandard/Pci30.h>
#include <Register/MeRegs.h>
#include "PchUsb.h"
#include <Library/DciPrivateLib.h>
#include <Library/PsfLib.h>
#include <Library/PsfSocLib.h>
#include <PchResetPlatformSpecific.h>
#include <PchInfoHob.h>
#include <Library/PmcPrivateLib.h>
#include <Library/PeiPmcPrivateLib.h>
#include <Library/PeiPmcPrivateLib.h>
#include <PchConfigHob.h>
#include <PchLimits.h>
#include <Library/PeiThermalLib.h>
#include <Library/PeiPchPcieClocksLib.h>
#include <Library/PeiCnviLib.h>
#include <Library/PeiP2sbPrivateLib.h>
#include <Library/PeiPchDmiLib.h>
#include <Library/PeiItssLib.h>
#include <Library/SiScheduleResetLib.h>
#include <Library/PeiScsLib.h>
#include <Library/PeiLpcLib.h>
#include <Library/PeiIehInitLib.h>
#include <Library/PeiHsioLib.h>
#include <Library/PchInitLib.h>
#include <P2SbHandle.h>

extern EFI_GUID gPchDeviceTableHobGuid;

/**
  The function program HSIO registers.

  @param[in] SiPolicyPpi               The SI Policy PPI instance

**/
VOID
PchHsioConfigure (
  IN  SI_POLICY_PPI    *SiPolicyPpi
  );

/**
  The function program HSIO registers.

  @param[in] SiPolicyPpi               The SI Policy PPI instance

**/
VOID
PchPcieHsioConfigure (
  IN  SI_POLICY_PPI    *SiPolicyPpi
  );

/**
  Initialize HSIO structures
  This should be done according HSIO IP integration in SoC

  @param[in]      SiPolicy       Pointer to Silicon Policy
  @param[in]      SiPreMemPolicy Pointer to Silicon PreMem Policy
  @param[in]      LaneIndex      HSIO lane number
  @param[in]      SataCtrlIndex  SATA Controller index
  @param[in out]  HsioHandle     Pointer to HSIO handle structure
  @param[in out]  HsioPrivate    Pointer to HSIO private configuration structure
  @param[in out]  HsioLane       Pointer to HSIO lane identification structure
  @param[in out]  P2SbController Pointer to P2SB controller identification structure
  @param[in out]  SbAccess       Pointer to Sideband access interface structure
**/
VOID
PchHsioHandleInit (
  IN      SI_POLICY_PPI                  *SiPolicy,
  IN      SI_PREMEM_POLICY_PPI           *SiPreMemPolicy,
  IN      UINT8                          LaneIndex,
  IN      UINT32                         SataCtrlIndex,
  IN OUT  HSIO_HANDLE                    *HsioHandle,
  IN OUT  HSIO_PRIVATE_CONFIG            *HsioPrivate,
  IN OUT  HSIO_LANE                      *HsioLane,
  IN OUT  P2SB_CONTROLLER                *P2SbController,
  IN      P2SB_SIDEBAND_REGISTER_ACCESS  *SbAccess
  );

/**
  The function programs HSIO Rx\Tx Eq policy registers for SATA lanes.

  @param[in]  SiPreMemPolicyPpi  The SI PreMem Policy PPI instance
**/
VOID
PchHsioRxTxEqPolicySataProg (
  IN SI_PREMEM_POLICY_PPI  *SiPreMemPolicyPpi
  );

/**
  The function programs HSIO Rx\Tx Eq policy registers for PCIe lanes.

  @param[in]  SiPreMemPolicyPpi  The SI PreMem Policy PPI instance
**/
VOID
PchHsioRxTxEqPolicyPcieProg (
  IN SI_PREMEM_POLICY_PPI  *SiPreMemPolicyPpi
  );

/**
  Initialize P2SB structures - PCH Legacy
  This should be done according P2SB IP integration in SoC

  @param[in]      SiPolicy        Pointer to Silicon Policy
  @param[in out]  P2SbHandle      Pointer to P2SB handle structure
  @param[in out]  P2SbPrivate     Pointer to P2SB private configuration structure
  @param[in out]  P2SbController  Pointer to P2SB controller identification structure
  @param[in out]  P2SbCallback    Pointer to P2SB callback structure
**/
VOID
PchP2SbHandleInit (
  IN      SI_POLICY_PPI        *SiPolicy,
  IN OUT  P2SB_HANDLE          *P2SbHandle,
  IN OUT  P2SB_PRIVATE_CONFIG  *P2SbPrivate,
  IN OUT  P2SB_CONTROLLER      *P2SbController,
  IN OUT  P2SB_CALLBACK        *P2SbCallback
  );

/**
  The function performs P2SB initialization.

  @param[in] SiPolicy         The SI Policy PPI instance
**/
VOID
PchP2SbInit (
  IN  SI_POLICY_PPI           *SiPolicy
  );

/**
  The function performs P2SB lock programming.

  @param[in] SiPolicy         The SI Policy PPI instance
**/
VOID
PchP2sbLock (
  IN  SI_POLICY_PPI           *SiPolicy
  );

/**
  Initialize LAN device on PCH

  @param[in]      SiPolicy       Pointer to Silicon Policy
**/
VOID
PchGbeInit (
  IN  SI_POLICY_PPI  *SiPolicy
  );

/**
  Initializes SATA Controller

  @param[in] SiPolicy   The SI Policy PPI instance
  @param[in] PchConfig  PCH upstream die config
**/
VOID
PchSataInit (
  IN  SI_POLICY_PPI                  *SiPolicy,
  IN  PCH_UPSTREAM_COMPONENT_CONFIG  *PchConfig
  );

/**
  Initializes SATA Controller after RST

  @param[in] SiPolicy   The SI Policy PPI instance
  @param[in] PchConfig  PCH upstream die config
**/
VOID
PchSataAfterRstInit (
  IN  SI_POLICY_PPI  *SiPolicy,
  IN  PCH_UPSTREAM_COMPONENT_CONFIG  *PchConfig
  );

/**
  Initializes JSL PCH SCS subsytem.

  @param[in] SiPolicyPpi  SI policy PPI.
**/
VOID
ScsInit (
  IN SI_POLICY_PPI  *SiPolicy
  );

/**
  Disable SCS devices before anyone has chance to do invalid access to them.
**/
VOID
ScsEarlyDisable (
  VOID
  );

//TODO move to seprate lib
/**
  Initialize the Intel Touch Host Controller

  @param[in] SiPolicy             Policy

**/
VOID
ThcInit (
  IN SI_POLICY_PPI           *SiPolicy
  );

/**
  Configure PCIe Grant Counts
**/
VOID
PchConfigurePcieGrantCounts (
  VOID
  );

/**
  Configure BiosLock according to policy setting.

  @param[in] LockDownConfig       LockDownConfig policy
**/
VOID
ConfigureBiosLock (
  IN  SI_POLICY_PPI  *SiPolicy
  );

/**
  The function performs PSTH specific programming.

  @param[in] SiPolicy          The SI Policy instance
**/
VOID
PchPsthConfigure (
  IN  SI_POLICY_PPI           *SiPolicy
  );

/**
  The function update pch config hob in the start of PchInit.

  @param[in]      SiPolicy               The SI Policy PPI instance
**/
VOID
BuildPchConfigHob (
  IN     SI_POLICY_PPI          *SiPolicy
  );


/**
  The function update pch info hob in the end of PchInit.
**/
VOID
BuildPchInfoHob (
  VOID
  );

/**
  Initialize the Smbus PPI and program the Smbus BAR

  @param[in]  SiPreMemPolicyPpi   The SI PreMem Policy PPI instance

  @retval EFI_SUCCESS             The function completes successfully
  @retval EFI_OUT_OF_RESOURCES    Insufficient resources to create database
**/
EFI_STATUS
SmbusInit (
  IN  SI_PREMEM_POLICY_PPI       *SiPreMemPolicyPpi
  );

/**
  The function performs SMBUS specific programming.

  @param[in] SiPolicyPpi       The SI Policy PPI instance

**/
VOID
SmbusConfigure (
  IN  SI_POLICY_PPI           *SiPolicyPpi
  );

/**
  Initialize ESPI controller.

  @param[in] SiPolicy  The Silicon Policy PPI instance
**/
VOID
PchEspiInit (
  IN SI_POLICY_PPI  *SiPolicy
  );

/**
  Initialize IEH controller.

  @param[in] SiPolicy  The Silicon Policy PPI instance
**/
VOID
PchIehInit (
  IN SI_POLICY_PPI  *SiPolicy
  );

/**
  Performs Extended BIOS decode range configuration

  @param[in] SiPolicy     The Silicon Policy PPI instance
**/
VOID
PchExtendedBiosDecodeRangeInit (
  IN  SI_POLICY_PPI               *SiPolicy
  );

#endif
