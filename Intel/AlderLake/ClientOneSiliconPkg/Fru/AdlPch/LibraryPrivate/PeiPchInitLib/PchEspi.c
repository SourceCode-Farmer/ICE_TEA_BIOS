/** @file
  The ESPI initialization After Memory.

@copyright
  INTEL CONFIDENTIAL
  Copyright 2020 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains an 'Intel Peripheral Driver' and is uniquely identified as
  "Intel Reference Module" and is licensed for Intel CPUs and chipsets under
  the terms of your license agreement with Intel or your vendor. This file may
  be modified by the user, subject to additional terms of the license agreement.

@par Specification Reference:
**/

#include "PchInitPei.h"

#include <ConfigBlock.h>
#include <EspiConfig.h>
#include <EspiHandle.h>
#include <Library/PeiEspiInitLib.h>
#include <Library/PchPciBdfLib.h>
#include <Library/P2SbSidebandAccessLib.h>

/**
  Build a ESPI handle for PCH

  @param[in]  SiPolicy            SiPolicyPpi instance
  @param[in]  P2SbAccess          P2SB Register Access structure
  @param[in]  P2SbCtrl            P2SB Controller structure
  @param[out] EspiHandle          On output pointer to initialized ESPI handle
**/
VOID
PchEspiGetHandle (
  IN  SI_POLICY_PPI                 *SiPolicy,
  IN  P2SB_SIDEBAND_REGISTER_ACCESS *P2SbAccess,
  IN  P2SB_CONTROLLER               *P2SbCtrl,
  OUT ESPI_HANDLE                   *EspiHandle
  )
{
  EFI_STATUS        Status;
  PCH_ESPI_CONFIG   *EspiConfig;

  //
  // PCIe Config Space Base Address
  //
  EspiHandle->PciCfgBase = EspiPciCfgBase();

  //
  // eSPI public configuration
  //
  Status = GetConfigBlock ((VOID *) SiPolicy, &gEspiConfigGuid, (VOID *) &EspiConfig);
  ASSERT_EFI_ERROR (Status);
  EspiHandle->EspiConfig = EspiConfig;

  //
  // eSPI private configuration
  //
  ZeroMem (&EspiHandle->EspiPrivateConfig, sizeof (ESPI_PRIVATE_CONFIG));
  EspiHandle->EspiPrivateConfig.PmSupport          = TRUE;
  EspiHandle->EspiPrivateConfig.EspiErrorReporting = FALSE;

  //
  // Register Access - PCR
  //
  P2SbCtrl->PciCfgBaseAddr = P2sbPciCfgBase ();
  P2SbCtrl->Mmio           = PCH_PCR_BASE_ADDRESS;
  P2SbCtrl->HpetMmio       = 0;
  BuildP2SbSidebandAccess (
    P2SbCtrl,
    PID_ESPISPI,
    0,
    P2SbPrivateConfig,
    P2SbMmioAccess,
    FALSE,
    P2SbAccess
    );
  EspiHandle->PciPcrAccess = &P2SbAccess->Access;
}

/**
  Initialize ESPI controller.

  @param[in] SiPolicy  The Silicon Policy PPI instance
**/
VOID
PchEspiInit (
  IN SI_POLICY_PPI  *SiPolicy
  )
{
  ESPI_HANDLE                     EspiHandle;
  P2SB_SIDEBAND_REGISTER_ACCESS   P2SbAccess;
  P2SB_CONTROLLER                 P2SbCtrl;

  PchEspiGetHandle (SiPolicy, &P2SbAccess, &P2SbCtrl, &EspiHandle);

  if (EspiHandle.EspiConfig->LgmrEnable) {
    PchLpcMemRangeSet (PCH_ESPI_LGMR_BASE_ADDRESS);
  }

  EspiInit (&EspiHandle);
}

