/** @file
  The HSIO SoC specific implementation.

@copyright
  INTEL CONFIDENTIAL
  Copyright 2020 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains an 'Intel Peripheral Driver' and is uniquely identified as
  "Intel Reference Module" and is licensed for Intel CPUs and chipsets under
  the terms of your license agreement with Intel or your vendor. This file may
  be modified by the user, subject to additional terms of the license agreement.

@par Specification Reference:
**/

#include <Ppi/SiPolicy.h>

#include <Library/DebugLib.h>
#include <Library/BaseMemoryLib.h>
#include <Library/TimerLib.h>
#include <Library/PchInfoLib.h>
#include <Library/ConfigBlockLib.h>
#include <Library/PchPciBdfLib.h>
#include <Library/PchPolicyLib.h>
#include <Library/PciSegmentLib.h>
#include <Library/PeiHsioLib.h>
#include <Library/PchFiaLib.h>
#include <Library/HsioSocLib.h>
#include <Library/SataSocLib.h>
#include <Library/FiaSocLib.h>
#include <Library/P2SbSocLib.h>
#include <Library/P2SbSidebandAccessLib.h>
#include <Register/PcieSipRegs.h>
#include <Register/PchRegsHsio.h>
#include <HsioHandle.h>
#include "PchInitPei.h"

/**
  Load default values to HSIO structures
  - Initialize data structures to zero
  - Initialize function pointers to NULL
  - Initialize pointers for HSIO_HANDLE substructures

  @param[in out]  HsioHandle      Pointer to HSIO handle structure
  @param[in out]  HsioPrivate     Pointer to HSIO private configuration structure
  @param[in out]  HsioLane        Pointer to HSIO lane identification structure
**/
STATIC
VOID
HsioHandleLoadDefaults (
  IN OUT  HSIO_HANDLE         *HsioHandle,
  IN OUT  HSIO_PRIVATE_CONFIG *HsioPrivate,
  IN OUT  HSIO_LANE           *HsioLane
  )
{
  ZeroMem (HsioPrivate, sizeof (HSIO_PRIVATE_CONFIG));
  HsioHandle->PrivateConfig = HsioPrivate;

  ZeroMem (HsioLane, sizeof (HSIO_LANE));
  HsioHandle->Lane = HsioLane;

  HsioHandle->Usb3HsioConfig = NULL;
  HsioHandle->SataLanePreMemConfig = NULL;
  HsioHandle->PcieLanePreMemConfig = NULL;
}

/**
  Initialize HSIO structures
  This should be done according HSIO IP integration in SoC

  @param[in]      SiPolicy       Pointer to Silicon Policy
  @param[in]      SiPreMemPolicy Pointer to Silicon PreMem Policy
  @param[in]      LaneIndex      HSIO lane number
  @param[in]      SataCtrlIndex  SATA Controller index
  @param[in out]  HsioHandle     Pointer to HSIO handle structure
  @param[in out]  HsioPrivate    Pointer to HSIO private configuration structure
  @param[in out]  HsioLane       Pointer to HSIO lane identification structure
  @param[in out]  P2SbController Pointer to P2SB controller identification structure
  @param[in out]  SbAccess       Pointer to Sideband access interface structure
**/
VOID
PchHsioHandleInit (
  IN      SI_POLICY_PPI                  *SiPolicy,
  IN      SI_PREMEM_POLICY_PPI           *SiPreMemPolicy,
  IN      UINT8                          LaneIndex,
  IN      UINT32                         SataCtrlIndex,
  IN OUT  HSIO_HANDLE                    *HsioHandle,
  IN OUT  HSIO_PRIVATE_CONFIG            *HsioPrivate,
  IN OUT  HSIO_LANE                      *HsioLane,
  IN OUT  P2SB_CONTROLLER                *P2SbController,
  IN      P2SB_SIDEBAND_REGISTER_ACCESS  *SbAccess
  )
{
  EFI_STATUS            Status;
  PCH_PM_CONFIG         *PmConfig;

  //
  // Initialize: data with defaults, function pointers to NULL, substructures pointers
  //
  HsioHandleLoadDefaults (HsioHandle, HsioPrivate, HsioLane);

  HsioPrivate->PciePllSsc = PCIE_PLL_SSC_AUTO;

  if (SiPolicy) {
    //
    // Set pointer to USB3 HSIO Config Block
    //
    Status = GetConfigBlock ((VOID *) SiPolicy, &gUsb3HsioConfigGuid, (VOID *) &HsioHandle->Usb3HsioConfig);
    ASSERT_EFI_ERROR (Status);

    Status = GetConfigBlock ((VOID *) SiPolicy, &gPmConfigGuid, (VOID *) &PmConfig);
    ASSERT_EFI_ERROR (Status);

    HsioPrivate->PciePllSsc = PmConfig->PciePllSsc;
  }

  if (SiPreMemPolicy) {
    //
    // Set pointer to PCIe HSIO PreMem Config Blocks
    //
    Status = GetConfigBlock ((VOID *) SiPreMemPolicy, &gHsioPciePreMemConfigGuid, (VOID *) &HsioHandle->PcieLanePreMemConfig);
    ASSERT_EFI_ERROR (Status);

    //
    // Set pointer to SATA and PCIe HSIO PreMem Config Blocks
    //
    if (SataCtrlIndex != (UINT32)(-1)) {
      HsioHandle->SataLanePreMemConfig = GetPchHsioSataPreMemConfig (SiPreMemPolicy, SataCtrlIndex);
    }
  }

  //
  // Initialize HSIO lane data
  //
  PchHsioGetLane (LaneIndex, HsioLane, P2SbController, SbAccess);
}

/**
  The function program HSIO registers.

  @param[in] SiPolicyPpi               The SI Policy PPI instance

**/
VOID
PchHsioConfigure (
  IN  SI_POLICY_PPI    *SiPolicyPpi
  )
{
  EFI_STATUS                         Status;
  PCH_PCIE_CONFIG                    *PchPcieConfig;
  HSIO_HANDLE                        HsioHandle;
  HSIO_PRIVATE_CONFIG                HsioPrivate;
  HSIO_LANE                          HsioLane;
  P2SB_CONTROLLER                    P2SbController;
  P2SB_SIDEBAND_REGISTER_ACCESS      SbAccess;
  UINT8                              HsioLaneNo;
  UINT32                             UnassignedLanes;

  DEBUG ((DEBUG_INFO, "PchHsioConfigure() Start\n"));

  Status = GetConfigBlock ((VOID *) SiPolicyPpi, &gPchPcieConfigGuid, (VOID *) &PchPcieConfig);
  ASSERT_EFI_ERROR (Status);

  // Re-use HSIO Register Access but for other PIDs: PID_MODPHY0, PID_MODPHY1
  // This is needed to keep same API accessing to ChipsetInit code
  PchFiaGetPcieLaneNum (0, &HsioLaneNo);
  PchHsioHandleInit (SiPolicyPpi, NULL, HsioLaneNo, (UINT32)-1, &HsioHandle, &HsioPrivate, &HsioLane, &P2SbController, &SbAccess);
  SbAccess.P2SbPid = HsioLane.Pid = PID_MODPHY1;
  PchTsnHsioPllPwrGatingProg (&HsioHandle);

  //
  // Enable the Power gating in modphy if lanes are unassigned
  //
  UnassignedLanes = PchFiaGetUnassignLane ();
  for (HsioLaneNo = 0; ((UnassignedLanes != 0) && (HsioLaneNo < 32)); HsioLaneNo++) {
    if ((UnassignedLanes & BIT0) != 0) {
      PchHsioHandleInit (SiPolicyPpi, NULL, HsioLaneNo, (UINT32)-1, &HsioHandle, &HsioPrivate, &HsioLane, &P2SbController, &SbAccess);
      PchPcieHsioPllPwrGatingProg (&HsioHandle);
    }
    UnassignedLanes = UnassignedLanes >> 1;
  }

  DEBUG ((DEBUG_INFO, "PchHsioConfigure() End\n"));
}
