/** @file
  USB Host Controller operations FRU lib

@copyright
  INTEL CONFIDENTIAL
  Copyright 2020 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains an 'Intel Peripheral Driver' and is uniquely identified as
  "Intel Reference Module" and is licensed for Intel CPUs and chipsets under
  the terms of your license agreement with Intel or your vendor. This file may
  be modified by the user, subject to additional terms of the license agreement.

@par Specification Reference:
**/
#include <Library/DebugLib.h>
#include <Library/PchPciBdfLib.h>
#include <Library/IoLib.h>
#include <Library/PciSegmentLib.h>
#include <Library/BaseMemoryLib.h>
#include <IndustryStandard/Pci30.h>
#include <Library/PcdLib.h>
#include <Register/UsbRegs.h>

/**
  Clear u2ub_survivability_reg1[0]
**/
VOID
XhciClearSurvReg1 (
  VOID
  )
{
  UINT32                XhciMmioBase;
  UINT64                XhciPciBase;

  XhciMmioBase = PcdGet32 (PcdSiliconInitTempMemBaseAddr);
  XhciPciBase = PchXhciPciCfgBase ();

  //
  // Assign memory resources
  //
  PciSegmentWrite32 (XhciPciBase + R_XHCI_CFG_BAR0, XhciMmioBase);
  PciSegmentWrite32 (XhciPciBase + R_XHCI_CFG_BAR0 + 4, 0);

  PciSegmentOr16 (
    XhciPciBase + PCI_COMMAND_OFFSET,
    (UINT16) (EFI_PCI_COMMAND_MEMORY_SPACE)
    );

  //
  // Clear u2ub_survivability_reg1[0]
  //
  MmioAnd32 (
    (XhciMmioBase + R_XHCI_MEM_SURVIVABILITY_REG1),
    (UINT32)~BIT0
    );

  //
  // Clear memory resources
  //
  PciSegmentAnd16 (
    XhciPciBase + PCI_COMMAND_OFFSET,
    (UINT16) ~(EFI_PCI_COMMAND_MEMORY_SPACE)
    );

  PciSegmentWrite32 ((XhciPciBase + R_XHCI_CFG_BAR0), 0);
  PciSegmentWrite32 ((XhciPciBase + R_XHCI_CFG_BAR0 + 4), 0);
}
