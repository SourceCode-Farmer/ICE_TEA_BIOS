/** @file
  This file provides services for Pmc policy function

@copyright
  INTEL CONFIDENTIAL
  Copyright 2020 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains an 'Intel Peripheral Driver' and is uniquely identified as
  "Intel Reference Module" and is licensed for Intel CPUs and chipsets under
  the terms of your license agreement with Intel or your vendor. This file may
  be modified by the user, subject to additional terms of the license agreement.

@par Specification Reference:
**/

#include <Library/DebugLib.h>
#include <Library/PeiPmcPolicyCommonLib.h>
#include <Library/PeiPmcPolicyLib.h>
#include <Library/SiConfigBlockLib.h>
#include <Library/ConfigBlockLib.h>
#include <Library/PchInfoLib.h>

/**
  Load Config block default

  @param[in] ConfigBlockPointer         Pointer to config block
**/
VOID
PmcLoadConfigDefaultAdl (
  IN VOID          *ConfigBlockPointer
  )
{
  PCH_PM_CONFIG  *PmConfig;
  PmConfig = ConfigBlockPointer;

  DEBUG ((DEBUG_INFO, "PmConfig->Header.GuidHob.Name = %g\n", &PmConfig->Header.GuidHob.Name));
  DEBUG ((DEBUG_INFO, "PmConfig->Header.GuidHob.Header.HobLength = 0x%x\n", PmConfig->Header.GuidHob.Header.HobLength));

  PmConfig->MeWakeSts                            = TRUE;
  PmConfig->WolOvrWkSts                          = TRUE;

  PmConfig->WakeConfig.WolEnableOverride         = TRUE;
  PmConfig->WakeConfig.LanWakeFromDeepSx         = TRUE;

  PmConfig->PchSlpS3MinAssert                    = PchSlpS350ms;
  PmConfig->PchSlpS4MinAssert                    = PchSlpS41s;
  PmConfig->PchSlpSusMinAssert                   = PchSlpSus4s;
//[-start-211008-BAIN000048-modify]//
#ifdef LCFC_SUPPORT
  PmConfig->PchSlpAMinAssert                     = PchSlpA98ms;
#else
  PmConfig->PchSlpAMinAssert                     = PchSlpA2s;
#endif
//[-end-211008-BAIN000048-modify]//
  PmConfig->SlpLanLowDc                          = TRUE;
  PmConfig->PciePllSsc                           = 0xFF;
  PmConfig->CpuC10GatePinEnable                  = TRUE;
  PmConfig->OsIdleEnable                         = TRUE;

  PmConfig->S0ixAutoDemotion                     = TRUE;
  PmConfig->Usb2PhySusPgEnable                   = TRUE;

  PmConfig->PsOnEnable                           = TRUE;

  if (IsPchLp ()) {
    PmConfig->ModPhySusPgEnable                  = TRUE;
  }

  if (IsPchS ()) {
    PmConfig->LpmS0ixSubStateEnable.Val          = 0x3;
    PmConfig->Usb2PhySusPgEnable                 = FALSE;
    PmConfig->PsOnEnable                         = TRUE;
  } else {
    PmConfig->LpmS0ixSubStateEnable.Val          = 0x9;
    PmConfig->PsOnEnable                         = FALSE;
  }
}


STATIC COMPONENT_BLOCK_ENTRY mPmcBlocks[] = {
  {
    &gPmConfigGuid,
    sizeof (PCH_PM_CONFIG),
    PCH_PM_CONFIG_REVISION,
    PmcLoadConfigDefaultAdl
  }
};

/**
  Get Pmc config block table size.

  @retval      Size of config block
**/
UINT16
PmcGetConfigBlockTotalSize (
  VOID
  )
{
  return mPmcBlocks[0].Size;
}

/**
  Add Pmc ConfigBlock.

  @param[in] ConfigBlockTableAddress    The pointer to config block table

  @retval EFI_SUCCESS                   The policy default is initialized.
  @retval EFI_OUT_OF_RESOURCES          Insufficient resources to create buffer
**/
EFI_STATUS
PmcAddConfigBlock (
  IN VOID           *ConfigBlockTableAddress
  )
{
  return AddComponentConfigBlocks (ConfigBlockTableAddress, &mPmcBlocks[0], ARRAY_SIZE(mPmcBlocks));
}