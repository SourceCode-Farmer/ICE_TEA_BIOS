/** @file
  Register names for PCH private chipset register for ADL S

Conventions:

  - Register definition format:
    Prefix_[GenerationName]_[ComponentName]_SubsystemName_RegisterSpace_RegisterName
  - Prefix:
    Definitions beginning with "R_" are registers
    Definitions beginning with "B_" are bits within registers
    Definitions beginning with "V_" are meaningful values within the bits
    Definitions beginning with "S_" are register size
    Definitions beginning with "N_" are the bit position
  - [GenerationName]:
    Three letter acronym of the generation is used (e.g. SKL,KBL,CNL etc.).
    Register name without GenerationName applies to all generations.
  - [ComponentName]:
    This field indicates the component name that the register belongs to (e.g. PCH, SA etc.)
    Register name without ComponentName applies to all components.
    Register that is specific to -H denoted by "_PCH_H_" in component name.
    Register that is specific to -LP denoted by "_PCH_LP_" in component name.
  - SubsystemName:
    This field indicates the subsystem name of the component that the register belongs to
    (e.g. PCIE, USB, SATA, GPIO, PMC etc.).
  - RegisterSpace:
    MEM - MMIO space register of subsystem.
    IO  - IO space register of subsystem.
    PCR - Private configuration register of subsystem.
    CFG - PCI configuration space register of subsystem.
  - RegisterName:
    Full register name.

@copyright
  INTEL CONFIDENTIAL
  Copyright 2020 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains an 'Intel Peripheral Driver' and is uniquely identified as
  "Intel Reference Module" and is licensed for Intel CPUs and chipsets under
  the terms of your license agreement with Intel or your vendor. This file may
  be modified by the user, subject to additional terms of the license agreement.

@par Specification Reference:
**/

#ifndef _PCH_REGS_PCR_ADPS_H_
#define _PCH_REGS_PCR_ADPS_H_


/**
  Definition for SBI PID
  The PCH_SBI_PID defines the PID for PCR MMIO programming and PCH SBI programming as well.
**/
#define PID_WIFI       0x74
#define PID_BTCORE     0x75

#define PID_EXT_USB2   0x4F
#define PID_MPFPW1     0x30
#define PID_MPFPW2     0x31
#define PID_MPFPW3     0x32
#define PID_MPFPW4     0x33
#define PID_MPFPW5     0x34
#define PID_MPFPW6     0x35
#define PID_MPFPW7     0x36
#define PID_MPFPW1P0   0x20
#define PID_MPFPW2P0   0x21
#define PID_MPFPW3P0   0x22
#define PID_MPFPW4P0   0x23
#define PID_MPFPW4P1   0x24
#define PID_MPFPW4P01  0x25
#define PID_MPFPW5P0   0x26
#define PID_MPFPW6P0   0x27
#define PID_MPFPW7P0   0x28
#define PID_MPFPW7P1   0x29
#define PID_MPFPW7P01  0x2A
#define PID_U3FPW1     0x38
#define PID_U3FPW2     0x39
#define PID_U3FPW3     0x3A
#define PID_U3FPW1P0   0x3C
#define PID_U3FPW2P0   0x3D
#define PID_U3FPW3P0   0x3E
#define PID_USB2FPHYS  0x4F

#define PID_ACE        0xBF
#define PID_FIAU       0xCF
#define PID_FIAPGS     0xCE
#define PID_FIAPD      0xC8
#define PID_SPG        0x86

#define PID_DBC        0x76
#define PID_SPR        0x77
#define PID_ICC_B      0xD8
#define PID_SRM0       0x40
#define PID_SRM1       0x41
#define PID_SRM2       0x42
#define PID_SRM3       0x43
#define PID_PSF5       0x8F
#define PID_CSME17     0x67
#define PID_TSN1       0xDD
#define PID_TBTLSX     0xC0
#define PID_MCSMB      0xC1

#endif
