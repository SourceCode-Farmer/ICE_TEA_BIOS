/** @file
  Header file for private PMC SOC Lib.

@copyright
  INTEL CONFIDENTIAL
  Copyright 2020 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains an 'Intel Peripheral Driver' and is uniquely identified as
  "Intel Reference Module" and is licensed for Intel CPUs and chipsets under
  the terms of your license agreement with Intel or your vendor. This file may
  be modified by the user, subject to additional terms of the license agreement.

@par Specification Reference:
**/

#ifndef _PEI_PMC_SOC_LIB_H_
#define _PEI_PMC_SOC_LIB_H_

#include <PmcSocConfig.h>

/**
  Checks if the Intel Touch Host Controller is Enabled

  @param[in] ThcNumber            THC0/THC1 0 or 1

  @retval TRUE                    THC_DIS strap is 0
  @retval FALSE                   THC_DIS strap is 1
**/
BOOLEAN
PmcIsThcEnabled (
  IN UINT8  ThcNumber
  );

/**
  Check whether GBETSN_DIS strap is enabled. Note that even
  though this function doesn't have anything to do with PMC it is
  historically a PMC lib responsibility to check whether IPs are fuse/strap
  disabled.

  @retval TRUE                    GBETSN_DIS strap is enabled
  @retval FALSE                   GBETSN_DIS strap is disabled
**/
BOOLEAN
PmcIsGbeTsnStrapDis (
  VOID
  );

/**
  This function checks if ISH is function disabled
  by static power gating

  @retval ISH device state
**/
BOOLEAN
PmcIsIshFunctionDisabled (
  VOID
  );

/**
  This function checks if ISH device is supported (not disabled by fuse)

  @retval ISH support state
**/
BOOLEAN
PmcIsIshSupported (
  VOID
  );

/**
  This function disables ISH device by static power gating.
  For static power gating to take place Global Reset, G3 or DeepSx transition must happen.

  @retval TRUE   Disabled ISH in PMC
  @retval FALSE  Failed to disable ISH in PMC
**/
BOOLEAN
PmcStaticDisableIsh (
  VOID
  );

/**
  This function enables ISH device by disabling static power gating
**/
VOID
PmcEnableIsh (
  VOID
  );

/**
  This function enables GBE ModPHY SPD gating.
**/
VOID
PmcGbeModPhyPowerGating (
  VOID
  );

/**
  This function checks if GbE is function disabled
  by static power gating

  @retval GbE device state
**/
BOOLEAN
PmcIsGbeFunctionDisabled (
  VOID
  );

/**
  This function disables GbE device by static power gating
  For static power gating to take place Global Reset, G3 or DeepSx transition must happen.
**/
VOID
PmcStaticDisableGbe (
  VOID
  );

/**
  This function enables GbE device by disabling static power gating
  Static power gating disabling takes place after Global Reset, G3 or DeepSx transition.
**/
VOID
PmcEnableGbe (
  VOID
  );

/**
  This function checks if GbE device is supported (not disabled by fuse)

  @param[in] PmcMmio       PMC MMIO

  @retval GbE support state
**/
BOOLEAN
PmcIsGbeSupported (
  IN UINT32  PmcMmio
  );

/**
  This function enables all SerailIo devices
  Static power gating disabling takes place after Global Reset, G3 or DeepSx transition.

  @param[in] PmcMmio        PMC MMIO
**/
VOID
PmcEnableSerialIo (
  IN UINT32  PmcMmio
  );

/**
  This function disables (static power gating) all SerailIo devices.
  For SerialIo controllers they can be power gated only if all of them are to be disabled.
  They cannot be statically power gated separately.
  For static power gating to take place Global Reset, G3 or DeepSx transition must happen.

  @param[in] PmcMmio        PMC MMIO
**/
VOID
PmcStaticDisableSerialIo (
  IN UINT32  PmcMmio
  );

/**
  This function checks if all SerialIo devices are statically disabled (static power gating)

  @param[in] PmcMmio        PMC MMIO

  @retval SerialIo disable state
**/
BOOLEAN
PmcIsSerialIoStaticallyDisabled (
  IN UINT32  PmcMmio
  );

/**
  This function checks if SerialIo device is supported (not disabled by fuse)

  @param[in] PmcMmio        PMC MMIO

  @retval SerialIo support state
**/
BOOLEAN
PmcIsSerialIoSupported (
  IN UINT32  PmcMmio
  );

/**
  This function disables (non-static power gating) HDA device
**/
VOID
PmcDisableHda (
  VOID
  );

/**
  This function checks if Cnvi device is supported (not disabled by fuse)

  @retval Cnvi support state
**/
BOOLEAN
PmcIsCnviSupported (
  VOID
  );

/**
  This function checks if CNVi is function disabled
  by static power gating

  @retval GbE device state
**/
BOOLEAN
PmcIsCnviFunctionDisabled (
  VOID
  );

/**
  This function enables CNVi device by disabling static power gating.
  Static power gating disabling takes place after Global Reset, G3 or DeepSx transition.
**/
VOID
PmcEnableCnvi (
  VOID
  );

/**
  This function disables CNVi device by static power gating
  For static power gating to take place Global Reset, G3 or DeepSx transition must happen.
**/
VOID
PmcStaticDisableCnvi (
  VOID
  );

/**
  This function disables (non-static power gating) PCIe Root Port

  @param[in] RpIndex        PCIe Root Port Index (0 based)
**/
VOID
PmcDisablePcieRootPort (
  IN UINT32  RpIndex
  );

/**
  This function disables (non-static power gating) SATA

  @param[in]  SataCtrlIndex     SATA controller index
**/
VOID
PmcDisableSata (
  IN UINT32     SataCtrlIndex
  );

/**
  This function checks if SATA device is supported (not disabled by fuse)

  @param[in] SataCtrlIndex SATA controller index

  @retval SATA support state
**/
BOOLEAN
PmcIsSataSupported (
  IN UINT32  SataCtrlIndex
  );

/**
  This function disables (non-static power gating) xHCI
**/
VOID
PmcDisableXhci (
  VOID
  );

/**
  This function disables (non-static power gating) XDCI
**/
VOID
PmcDisableXdci (
  VOID
  );

/**
  This function checks if XDCI device is supported (not disabled by fuse)

  @retval XDCI support state
**/
BOOLEAN
PmcIsXdciSupported (
  VOID
  );

/**
  This function disables (non-static power gating) SCS UFS controller and enables ModPHY SPD gating (PCH-LP only).
**/
VOID
PmcDisableScsUfs (
  VOID
  );

/**
  This function checks if SCS UFS device is supported (not disabled by fuse)

  @retval SCS device support state
**/
BOOLEAN
PmcIsScsUfsSupported (
  VOID
  );

/**
  This function disables (non-static power gating) SCS eMMC controller.
**/
VOID
PmcDisableScsEmmc (
  VOID
  );

/**
  This function checks if SCS eMMC device is supported (not disabled by fuse)
  @retval SCS device support state
**/
BOOLEAN
PmcIsScsEmmcSupported (
  VOID
  );

/**
  Check if MODPHY SUS PG is supported

  @retval  Status of MODPHY SUS PG support
**/
BOOLEAN
PmcIsModPhySusPgSupported (
  VOID
  );

/**
  Get the PMC SOC configuration.

  @param[out] PmcSocConfig  Configuration of the PMC
**/
VOID
PmcGetSocConfig (
  OUT PMC_SOC_CONFIG  *PmcSocConfig
  );
/**
  Check if PMC Extended straps are supported

  @retval  Status of Extended Straps support
**/
BOOLEAN
PmcIsExtendedStrapsSupported (
  VOID
  );

/**
  Check if FIVR is supported.

  @return BOOLEAN specifing whether FIVR is supported in PMC.
**/
BOOLEAN
PmcIsFivrSupported (
  VOID
  );

/**
  Check if PMC IPC1 command for CPU straps is supported.

  @retval  Status of CPU Straps support
**/
BOOLEAN
PmcIsCpuStrapsIpcCommandSupported (
  VOID
  );

/**
  This function will return the lock function disable (static and non-static power gating)
  configuration

  @param[in] RegOffset       Offset of the register
  @param[in] DataAnd32       Mask Data for Lock Function disable
  @param[in] DataOr32        Bit to enable the Lock function disable.

**/
VOID
PmcGetFunctionDisableLockConfig(
  IN UINTN*  RegOffset,
  IN UINT32* DataAnd32,
  IN UINT32* DataOr32
  );
#endif
