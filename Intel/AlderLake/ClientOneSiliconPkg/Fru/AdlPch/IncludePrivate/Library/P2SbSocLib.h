/** @file
  Header file for P2SB SoC library.

@copyright
 INTEL CONFIDENTIAL
 Copyright (c) 2020 Intel Corporation. All rights reserved
 This software and associated documentation (if any) is furnished
 under a license and may only be used or copied in accordance
 with the terms of the license. Except as permitted by the
 license, no part of this software or documentation may be
 reproduced, stored in a retrieval system, or transmitted in any
 form or by any means without the express written consent of
 Intel Corporation.
 This file contains an 'Intel Peripheral Driver' and is uniquely
 identified as "Intel Reference Module" and is licensed for Intel
 CPUs and chipsets under the terms of your license agreement with
 Intel or your vendor. This file may be modified by the user, subject
 to additional terms of the license agreement.

@par Specification Reference:
**/

#ifndef _P2SB_SOC_LIB_H_
#define _P2SB_SOC_LIB_H_

#include <P2SbController.h>

/**
  Get P2SB instance

  @param[in, out] P2SbController      P2SB controller pointer

  @retval     EFI_SUCCESS           - Completed successfully
              EFI_INVALID_PARAMETER - P2SbController NULL
**/
EFI_STATUS
GetP2SbController (
  IN OUT P2SB_CONTROLLER      *P2SbController
  );

#endif // _P2SB_SOC_LIB_H_
