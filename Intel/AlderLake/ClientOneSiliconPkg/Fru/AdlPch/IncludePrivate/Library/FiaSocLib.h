/** @file
  Header file for FiaSocLib.

@copyright
  INTEL CONFIDENTIAL
  Copyright 2020 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains an 'Intel Peripheral Driver' and is uniquely identified as
  "Intel Reference Module" and is licensed for Intel CPUs and chipsets under
  the terms of your license agreement with Intel or your vendor. This file may
  be modified by the user, subject to additional terms of the license agreement.

@par Specification Reference:
**/

#ifndef _FIA_SOC_LIB_H_
#define _FIA_SOC_LIB_H_

#include <Library/PchFiaLib.h>
#include <Library/FiaAccessLib.h>


/**
  Returns a FIA lane number for a given SATA port.

  @param[in]  SataCtrlIndex  SATA controller index
  @param[in]  SataPortIndex  SATA port index
  @param[out] LaneNum        Pointer to the variable that stores lane number.
                             The output value is only valid if this function returns TRUE.

  @return TRUE if given SATA port owns FIA lane, FALSE otherwise
**/
BOOLEAN
PchFiaGetSataLaneNum (
  IN  UINT32  SataCtrlIndex,
  IN  UINT32  SataPortIndex,
  OUT UINT8   *LaneNum
  );

/**
  Returns a FIA lane number for a GbE controller

  @param[out] LaneNum  FIA lane number that is owned by GbE

  @retval TRUE   Found FIA lane assigned to GbE
  @retval FALSE  No lanes assigned to GbE
**/
BOOLEAN
PchFiaGetGbeLaneNum (
  OUT UINT8  *LaneNum
  );

/**
  Returns a FIA lane number for a given PCIe lane.

  @param[in]  PciePhysicalLane  Index of the PCIe lane
  @param[out] LaneNum           Pointer to the variable that stores lane number.
                                The output value is only valid if this function returns TRUE.

  @return TRUE if given PciePhysicalLane owns FIA lane, FALSE otherwise
**/
BOOLEAN
PchFiaGetPcieLaneNum (
  IN  UINT32  PciePhysicalLane,
  OUT UINT8   *LaneNum
  );

/**
  Returns a FIA lane number for a given PCIe root port
  This function handles PCIe lane reversal internaly.

  @param[in]  RpIndex       PCIe root port index
  @param[in]  RpLaneIndex   Root port lane index within given root port
  @param[out] LaneNum       Pointer to the variable that stores lane number.
                            The output value is only valid if this function returns TRUE.

  @return TRUE if given RpLane owns FIA lane, FALSE otherwise
**/
BOOLEAN
PchFiaGetPcieRootPortLaneNum (
  IN  UINT32  RpIndex,
  IN  UINT32  RpLaneIndex,
  OUT UINT8   *LaneNum
  );

/**
  Returns a FIA lane number for a given USB3 port.

  @param[in]  Usb3PortIndex  USB3 port index
  @param[out] LaneNum        Pointer to the variable that stores lane number.
                             The output value is only valid if this function returns TRUE.

  @return TRUE if given USB3 port owns FIA lane, FALSE otherwise
**/
BOOLEAN
PchFiaGetUsb3LaneNum (
  IN  UINT32  Usb3PortIndex,
  OUT UINT8   *LaneNum
  );

/**
  Returns a FIA lane number for a given TSN port.

  @param[in]  TsnPortIndex  TSN port index
  @param[out] LaneNum       Pointer to the variable that stores lane number.
                            The output value is only valid if this function returns TRUE.

  @return TRUE if given TSN port owns FIA lane, FALSE otherwise
**/
BOOLEAN
PchFiaGetTsnLaneNum (
  IN UINT32  TsnPortIndex,
  IN UINT8   *LaneNum
  );

/**
  Print FIA LOS registers.
**/
VOID
PchFiaPrintLosRegisters (
  VOID
  );

/**
  Assigns CLKREQ# to PCH PCIe ports

  @param[in] ClkReqMap      Mapping between PCH PCIe ports and CLKREQ#
  @param[in] ClkReqMapSize  Size of the map
**/
VOID
PchFiaAssignPchPciePortsClkReq (
  IN UINT8  *ClkReqMap,
  IN UINT8  ClkReqMapSize
  );

/**
  Assigns CLKREQ# to CPU PCIe ports

  @param[in] RpIndex        Rootport index
  @param[in] ClkReqNum      Number of the clock
**/
VOID
PchFiaAssignCpuPciePortClkReq (
  IN UINT32       RpIndex,
  IN UINT8        ClkReqNum
  );

/**
  Disables ClkReq mapping for PCIe root port

  @param[in] FiaInst  FIA Instance
  @param[in] RpIndex  Root port index
**/
VOID
PchFiaDisablePchPciePortClkReq (
  IN UINT32  RpIndex
  );

/**
  Enable CLKREQ# to CPU PCIe ports

  @param[in] RpIndex        Rootport index
  @param[in] ClkReqNum      Number of the clock
**/
VOID
PchFiaEnableCpuPciePortClkReq (
  IN UINT32       RpIndex,
  IN UINT8        ClkReqNum
  );

/**
  Return the status of the CLKREQ state received with VW msg.

  @param[in] RpIndex  CPU PCIe index.

  @return Status of the CLKREQ.
**/
BOOLEAN
PchFiaGetCpuPcieClkReqStatus (
  IN UINT32  RpIndex
  );

/**
  Assigns CLKREQ# to GbE

  @param[in]  ClkReqNum  CLKREQ# number
**/
VOID
PchFiaAssignGbeClkReq (
  IN UINT8  ClkReqNum
  );

/**
  Configures lower bound of delay between ClkReq assertion and driving RefClk.
**/
VOID
PchFiaSetClockOutputDelay (
  VOID
  );

/**
  Performs FIA programming required at the end of configuration and locks lockable FIA registers
**/
VOID
PchFiaFinalizeConfigurationAndLock (
  VOID
  );

/**
  Returns number of FIA LOS registers used

  @param[in] FiaIndex        FIA index to specify FIA instance

  @return Number of FIA LOS registers used
**/
UINT32
PchFiaGetMaxLosRegister (
  FIA_INDEX FiaIndex
  );

/**
  Get FIA LOS register value

  @param[in] FiaIndex        FIA index to specify FIA instance
  @param[in] LosIndex        LOS FIA register index

  @return  LOS FIA register value
**/
UINT32
PchFiaGetLos (
  FIA_INDEX FiaIndex,
  UINT32    LosIndex
  );

/**
  Returns number of FIA lanes

  @return Number of FIA lanes
**/
UINT8
PchFiaGetMaxLaneNum (
  VOID
  );

/**
  Returns the unassigned lanes

  @param[in]  LaneNum

  @return Unassigned lane in bitwise representation.
**/
UINT32
PchFiaGetUnassignLane (
  );


/**
  Return FIA lane owner.

  @param[in] FiaInst  FIA Instance
  @param[in] LaneNum  FIA lane number

  @return  Code of the FIA lane owner, PchFiaOwnerInvalid if lane number wasn't valid
**/
PCH_FIA_LANE_OWNER
PchFiaGetLaneOwner (
  IN  FIA_INSTANCE FiaInst,
  IN  UINT8        LaneNum
  );

/**
  Returns number of FIA lanes

  @param[in] FiaInst  FIA Instance

  @return Number of FIA lanes
**/
UINT8
PchFiaGetMaxLaneNumEx (
  IN  FIA_INSTANCE FiaInst
  );

/**
  Checks if given lane is DMI

  @param[in]  FiaLaneNum  Fia lane num

  @return TRUE if given lane is DMI, FALSE otherwise
**/
BOOLEAN
PchFiaIsLaneDmi (
  IN UINT8  FiaLaneNum
  );

/**
  Returns a FIA lane number for a given UFS lane.

  @param[in]  UfsControllerIndex  Index of the UFS controller
  @param[in]  UfsLaneIndex        Index of the UFS lane on given controller
  @param[out] LaneNum             Optional. Pointer to the variable that stores lane number.
                                  The output value is only valid if this function returns TRUE.

  @return TRUE if given UFS lane owns FIA lane, FALSE otherwise
**/
BOOLEAN
PchFiaGetUfsLaneNum (
  IN  UINT32  UfsControllerIndex,
  IN  UINT32  UfsLaneIndex,
  OUT UINT8   *LaneNum
  );

/**
  Checks if a given PCIe lane is assigned any FIA lane

  @param[in]  RpIndex      PCIe root port index
  @param[in]  RpLaneIndex  Root port lane index within given root port

  @return  TRUE if given PCIe lane is assigned a FIA lane, FALSE otherwise
**/
BOOLEAN
PchFiaIsPcieRootPortLaneConnected (
  IN UINT32  RpIndex,
  IN UINT32  RpLaneIndex
  );

/**
  Checks if a given SATA port is assigned any FIA lane

  @param[in] SataCtrlIndex  SATA controller index
  @param[in] SataPortIndex  SATA port index

  @return TRUE if given SATA port is assigned a FIA lane, FALSE otherwise
**/
BOOLEAN
PchFiaIsSataPortConnected (
  IN UINT32  SataCtrlIndex,
  IN UINT32  SataPortIndex
  );

/**
  Checks if a given USB3 port is assigned any FIA lane

  @param[in]  Usb3PortIndex  USB3 port index

  @return TRUE if given USB3 port is assigned a FIA lane, FALSE otherwise
**/
BOOLEAN
PchFiaIsUsb3PortConnected (
  IN UINT32  Usb3PortIndex
  );

/**
  Check if UFS lane is assigned a FIA lane

  @param[in] UfsControllerIndex  UFS controller index
  @param[in] UfsLaneIndex        Lane index local to given UFS controller

  @retval TRUE   UFS lane is assigned a FIA lane
  @retval FALSE  UFS lane is not assigned any FIA lane
**/
BOOLEAN
PchFiaIsUfsLaneConnected (
  IN UINT32  UfsControllerIndex,
  IN UINT32  UfsLaneIndex
  );

#endif
