/** @file
  This file contains internal header for PSF lib usage

@copyright
  INTEL CONFIDENTIAL
  Copyright 2020 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains an 'Intel Peripheral Driver' and is uniquely identified as
  "Intel Reference Module" and is licensed for Intel CPUs and chipsets under
  the terms of your license agreement with Intel or your vendor. This file may
  be modified by the user, subject to additional terms of the license agreement.

@par Specification Reference:
**/

#ifndef _PSF_SOC_LIB_H_
#define _PSF_SOC_LIB_H_

#include <Library/PsfLib.h>
#include <Register/PchPcrRegs.h>

/**
  Get table of supported PSF segments

  @return  PsfSegmentTable   Table of supported PSF segments
**/
PSF_SEGMENT_TABLE*
PsfGetSegmentTable (
  VOID
  );

/**
  Return PSF_PORT for SerialIO I2C device

  @param[in] I2cNum  Serial IO I2C device (I2C0, I2C1, ....)

  @retval  PsfPort   PSF PORT structure for SerialIO I2C device
**/
PSF_PORT
PsfSerialIoI2cPort (
  IN UINT32  I2cNum
  );

/**
  Return PSF_PORT for SerialIO SPI device

  @param[in] SpiNum  Serial IO SPI device (SPI0, SPI1, ....)

  @retval  PsfPort   PSF PORT structure for SerialIO SPI device
**/
PSF_PORT
PsfSerialIoSpiPort (
  IN UINT32  SpiNum
  );

/**
  Return PSF_PORT for SerialIO UART device

  @param[in] UartNum  Serial IO UART device (UART0, UART1, ....)

  @retval  PsfPort    PSF PORT structure for SerialIO UART device
**/
PSF_PORT
PsfSerialIoUartPort (
  IN UINT32  UartNum
  );

/**
  Return PSF_PORT for TraceHub device

  @retval    PsfPort         PSF PORT structure for TraceHub device
**/
PSF_PORT
PsfTraceHubPort (
  VOID
  );

/**
  This procedure will return PSF_PORT for TraceHub ACPI device

  @retval    PsfPort         PSF PORT structure for TraceHub ACPI device
**/
PSF_PORT
PsfTraceHubAcpiDevPort (
  VOID
  );

/**
  Return PSF_PORT for HECI device

  @param[in] HeciDevice      HECIx Device (HECI1-4)

  @retval    PsfPort         PSF PORT structure for HECI device
**/
PSF_PORT
PsfHeciPort (
  IN UINT8      HeciDevice
  );

/**
  This procedure will return PSF_PORT for SOL device

  @retval    PsfPort         PSF PORT structure for SOL device
**/
PSF_PORT
PsfSolPort (
  VOID
  );

/**
  Disable IDER device at PSF level
**/
VOID
PsfDisableIderDevice (
  VOID
  );

/**
  Disable SATA device at PSF level

  @param[in]  SataCtrlIndex     SATA controller index
**/
VOID
PsfDisableSataDevice (
  IN UINT32     SataCtrlIndex
  );

/**
  Return PSF_PORT for ISH device

  @retval    PsfPort         PSF PORT structure for ISH device
**/
PSF_PORT
PsfIshPort (
  VOID
  );

/**
  Return PSF_PORT for FPAK device

  @retval    PsfPort         PSF PORT structure for FPAK device
**/
PSF_PORT
PsfFpakPort (
  VOID
  );

/**
  Return PSF_PORT for CNVi device

  @retval    PsfPort         PSF PORT structure for CNVi device
**/
PSF_PORT
PsfCnviPort (
  VOID
  );

/**
  Disable THC device at PSF level

  @param[in]  ThcNumber                Touch Host Controller Number THC0 or THC1
**/
VOID
PsfDisableThcDevice (
  IN  UINT32        ThcNumber
  );

/**
  Disable HDAudio device at PSF level
**/
VOID
PsfDisableHdaDevice (
  VOID
  );

/**
  Disable Dsp bar at PSF level
**/
VOID
PsfDisableDspBar (
  VOID
  );

/**
  Disable xDCI device at PSF level
**/
VOID
PsfDisableXdciDevice (
  VOID
  );

/**
  Disable xHCI device at PSF level
**/
VOID
PsfDisableXhciDevice (
  VOID
  );

/**
  Disable xHCI VTIO Phantom device at PSF level
**/
VOID
PsfDisableXhciVtioDevice (
  VOID
  );

/**
  Return PSF_PORT for PMC device

  @retval    PsfPort         PSF PORT structure for PMC device
**/
PSF_PORT
PsfPmcPort (
  VOID
  );

/**
  Return PSF_PORT for SCS UFS device

  @param[in] UfsNum       UFS Device

  @retval    PsfPort      PSF PORT structure for SCS UFS device
**/
PSF_PORT
PsfScsUfsPort (
  IN UINT32  UfsNum
  );

/**
  Return PSF_PORT for SCS EMMC device
  @retval    PsfPort      PSF PORT structure for SCS EMMC device
**/
PSF_PORT
PsfScsEmmcPort (
  VOID
  );

/**
  Disable FPAK device at PSF level
**/
VOID
PsfDisableFpakDevice (
  VOID
  );

/**
  Disable GbE device at PSF level
**/
VOID
PsfDisableGbeDevice (
  VOID
  );

/**
  Disable SMBUS device at PSF level
**/
VOID
PsfDisableSmbusDevice (
  VOID
  );

/**
  Return PSF_REG_BASE table at root PSF level to which PCIe Root Port device is connected

  @retval    PsfRegBase        PSF REG BASE structure table for PCIe
**/
PSF_REG_BASE*
PsfGetRootPciePortTable (
  VOID
  );

/**
  Get PSF PCIe Root Port Data

  @param[in]  RpIndex         PCIe Root Port Index (0 based)
  @retval     PciePortData    PCIE PORT Data
**/
PSF_PCIE_PORT_DATA*
PsfGetPciePortData (
  IN UINT32  RpIndex
  );

/**
  Get PSF PCIe Root Port Data Table
**/
PSF_PCIE_PORT_DATA_TABLE*
PsfGetPciePortDataTable (
  VOID
  );

//
// Tag for identifying last element of PSF_TOPOLOGY type array
//
#define PSF_TOPOLOGY_END   {{0, 0}, PsfNullPort, NULL}

/**
  Get PSF Pcie Tree topology

  @param[in] PsfTopology          PSF Port from PSF PCIe tree topology

  @retval PsfTopology             PSF PCIe tree topology
**/
CONST PSF_TOPOLOGY*
PsfGetRootPciePsfTopology (
  VOID
  );

/**
  Get EOI register data table for all PSFs

  @return PsfEoiRegDataTable   Pointer to PSF_EOI_REG_DATA_TABLE
**/
PSF_REG_DATA_TABLE*
PsfGetEoiRegDataTable (
  VOID
  );

/**
  Get MCTP register data table for all PSFs

  @return PsfEoiRegDataTable   Pointer to PSF_EOI_REG_DATA_TABLE
**/
PSF_REG_DATA_TABLE*
PsfGetMctpRegDataTable (
  VOID
  );

/**
  Fill MCTP Targets Table

  @param[out] TargetIdTable    MCTP Targets table
  @param[in]  MaxTableSize     TargetIdTable real size
  @param[in]  PsfTable         PSF Segment Table
  @param[in]  PcieRegBaseTable Pcie Reg Base Table

  @retval Number of targets, resulting size of the table
**/
UINT32
MctpTargetsTable (
  OUT PSF_PORT_DEST_ID  *TargetIdTable,
  IN  UINT32            MaxTableSize,
  IN  PSF_SEGMENT_TABLE *PsfTable,
  IN  PSF_REG_BASE      *PcieRegBaseTable
  );

/**
  P2SB PSF port Destination ID (psf_id:port_group_id:port_id:channel_id)

  @retval P2SB Destination ID
**/
PSF_PORT_DEST_ID
PsfP2sbDestinationId (
  VOID
  );

/**
  DMI PSF port Destination ID (psf_id:port_group_id:port_id:channel_id)

  @retval DMI Destination ID
**/
PSF_PORT_DEST_ID
PsfDmiDestinationId (
  VOID
  );

/**
  PCIe PSF port destination ID (psf_id:port_group_id:port_id:channel_id)

  @param[in] RpIndex        PCIe Root Port Index (0 based)

  @retval Destination ID
**/
PSF_PORT_DEST_ID
PsfPcieDestinationId (
  IN UINT32 RpIndex
  );

/**
  Return PSF_EARLY_INIT_DATA structure to Early Init Psf

  @retval Psf Early Init Data
**/
PSF_EARLY_INIT_DATA*
PsfGetEarlyInitData (
  VOID
  );

/**
  Return the PSF (Root level) RS3 Function Config PSF_PORT for PCIe Root Port

  @param[in] RpIndex        PCIe Root Port Index (0 based)

  @retval    PsfPort        PSF PORT structure for PCIe Function Config
**/
PSF_PORT
PsfRootRs3PcieFunctionConfigPort (
  IN UINT32  RpIndex
  );

/**
  This function returns Psf Port Relaxed Ordering Configs

  @retval struct containing tables of registers for programming of Relaxed Ordering
**/
PSF_RELAXED_ORDER_REGS*
GetPsfPortRelaxedOrderingTables (
  VOID
  );

/**
  Configures rootspace 3 bus number for PCIe IMR use

  @param[in] Rs3Bus        bus number
**/
VOID
PsfSetRs3Bus (
  UINT8 Rs3Bus
  );

/**
  Return RC_OWNER value to program

  @retval RC_OWNER
**/
UINT32
PsfGetRcOwner (
  );

/**
  Enable VTd support in PSF.

**/
VOID
PchPsfEnableVtd (
  VOID
  );

#endif
