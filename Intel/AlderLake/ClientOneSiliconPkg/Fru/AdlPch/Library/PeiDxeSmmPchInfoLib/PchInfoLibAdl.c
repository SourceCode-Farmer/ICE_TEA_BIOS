/** @file
  Pch information library for ADL.

  All function in this library is available for PEI, DXE, and SMM,
  But do not support UEFI RUNTIME environment call.

@copyright
  INTEL CONFIDENTIAL
  Copyright 2019 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains an 'Intel Peripheral Driver' and is uniquely identified as
  "Intel Reference Module" and is licensed for Intel CPUs and chipsets under
  the terms of your license agreement with Intel or your vendor. This file may
  be modified by the user, subject to additional terms of the license agreement.

@par Specification Reference:
**/
#include <Base.h>
#include <PchPcieRpInfo.h>
#include <Uefi/UefiBaseType.h>
#include <Library/PchInfoLib.h>
#include <Library/PmcPrivateLib.h>
#include <Library/PrintLib.h>
#include <Library/IoLib.h>
#include <Library/DebugLib.h>
#include <Library/BaseLib.h>
#include <Library/PciSegmentLib.h>
#include <Library/PcdLib.h>
#include <Library/PchPciBdfLib.h>
#include <Register/PchRegsLpcAdl.h>
#include <Register/PchRegs.h>
#include <Register/PchRegsLpc.h>
#include <IndustryStandard/Pci30.h>
#include "PchInfoLibPrivate.h"


/**
  Return LPC Device Id

  @retval PCH_LPC_DEVICE_ID         PCH Lpc Device ID
**/
UINT16
PchGetLpcDid (
  VOID
  )
{
  UINT64  LpcBaseAddress;

  LpcBaseAddress = LpcPciCfgBase ();

  return PciSegmentRead16 (LpcBaseAddress + PCI_DEVICE_ID_OFFSET);
}

/**
  Return Pch Series

  @retval PCH_SERIES            Pch Series
**/
PCH_SERIES
PchSeries (
  VOID
  )
{
  PCH_SERIES        PchSer;
  static PCH_SERIES PchSeries = PCH_UNKNOWN_SERIES;

  if (PchSeries != PCH_UNKNOWN_SERIES) {
    return PchSeries;
  }

  PchSer = PchSeriesFromLpcDid (PchGetLpcDid ());

  PchSeries = PchSer;

  return PchSer;
}

/**
  Return Pch stepping type

  @retval PCH_STEPPING            Pch stepping type
**/
PCH_STEPPING
PchStepping (
  VOID
  )
{
  UINT8                RevId;
  UINT64               LpcBaseAddress;
  static PCH_STEPPING  PchStepping = PCH_STEPPING_MAX;

  if (PchStepping != PCH_STEPPING_MAX) {
    return PchStepping;
  }

  LpcBaseAddress = LpcPciCfgBase ();
  RevId = PciSegmentRead8 (LpcBaseAddress + PCI_REVISION_ID_OFFSET);

  RevId = PchSteppingFromRevId (RevId);

  PchStepping = RevId;

  return RevId;
}

/**
  Determine if PCH is supported

  @retval TRUE                    PCH is supported
  @retval FALSE                   PCH is not supported
**/
BOOLEAN
IsPchSupported (
  VOID
  )
{
  UINT16         LpcVendorId;
  UINT64         LpcBaseAddress;

  LpcBaseAddress = LpcPciCfgBase ();

  LpcVendorId = PciSegmentRead16 (LpcBaseAddress + PCI_VENDOR_ID_OFFSET);

  ///
  /// Verify that this is a supported chipset
  ///
  if ((LpcVendorId == V_PCH_INTEL_VENDOR_ID) && (PchSeries () != PCH_UNKNOWN_SERIES)) {
    return TRUE;
  }
  DEBUG ((DEBUG_ERROR, "PCH code doesn't support the LpcDeviceId: 0x%04x!\n", PciSegmentRead16 (LpcBaseAddress + PCI_DEVICE_ID_OFFSET)));
  return FALSE;
}

/**
  Check if this is PCH M Sku

  @retval TRUE                It's M Sku
  @retval FALSE               It's not M Sku
**/
BOOLEAN
IsPchMSku (
  VOID
)
{
  UINT16    LpcDid;

  LpcDid = PchGetLpcDid ();

  if ((LpcDid == V_ADL_PCH_P_LPC_CFG_DEVICE_ID_MB_6) || (LpcDid == V_ADL_PCH_P_LPC_CFG_DEVICE_ID_MB_7)) {
    return TRUE;
  }
  return FALSE;
}

/**
  Check if this is PCH P series

  @retval TRUE                It's PCH P series
  @retval FALSE               It's not PCH P series
**/
BOOLEAN
IsPchP (
  VOID
  )
{
  return (PchSeries () == PCH_P);
}

/**
  Check if this is PCH M series

  @retval TRUE                It's PCH M series
  @retval FALSE               It's not PCH M series
**/
BOOLEAN
IsPchM (
  VOID
  )
{
  return IsPchMSku ();
}

/**
  Check if this is PCH S series

  @retval TRUE                It's PCH S series
  @retval FALSE               It's not PCH S series
**/
BOOLEAN
IsPchS (
  VOID
  )
{
  return (PchSeries () == PCH_S);
}


/**
  Check if this is EBG PCH generation

  @retval TRUE                It's EBG PCH
  @retval FALSE               It's not EBG PCH
**/
BOOLEAN
IsEbgPch (
  VOID
  )
{
  return (PchGeneration () == EBG_PCH);
}

/**
  Check if this is ADL PCH generation

  @retval TRUE                It's ADL PCH
  @retval FALSE               It's not ADL PCH
**/
BOOLEAN
IsAdlPch (
  VOID
  )
{
  return (
          (PchGeneration () == ADL_PCH)
         );
}



/**
  Check if this is S3m2.0 IBL generation

  @retval TRUE                It's S3m 2.0 IBL
  @retval FALSE               It's not S3m 2.0 IBL
**/
BOOLEAN
IsS3m2Ibl (
  VOID
  )
{
  return (PchGeneration () == S3M2_IBL);
}

/**
  Get PCH stepping ASCII string.
  Function determines major and minor stepping versions and writes them into a buffer.
  The return string is zero terminated

  @param [out]     Buffer               Output buffer of string
  @param [in]      BufferSize           Buffer size.
                                        Must not be less then PCH_STEPPING_STR_LENGTH_MAX

  @retval EFI_SUCCESS                   String copied successfully
  @retval EFI_INVALID_PARAMETER         The stepping is not supported, or parameters are NULL
  @retval EFI_BUFFER_TOO_SMALL          Input buffer size is too small
**/
EFI_STATUS
PchGetSteppingStr (
  OUT    CHAR8                          *Buffer,
  IN     UINT32                         BufferSize
  )
{
  PCH_STEPPING PchStep;

  PchStep = PchStepping ();

  if ((Buffer == NULL) || (BufferSize == 0)) {
    return EFI_INVALID_PARAMETER;
  }
  if (BufferSize < PCH_STEPPING_STR_LENGTH_MAX) {
    return EFI_BUFFER_TOO_SMALL;
  }

  PchPrintSteppingStr (Buffer, BufferSize, PchStep);

  return EFI_SUCCESS;
}

/**
  Get PCH Sku ASCII string
  The return string is zero terminated.

  @retval Static ASCII string of PCH Sku
**/
CHAR8*
PchGetSkuStr (
  VOID
  )
{
  UINTN          Index;
  UINT16         LpcDid;

  LpcDid = PchGetLpcDid ();

  for (Index = 0; mSkuStrs[Index].Id != 0xFFFF; Index++) {
    if (LpcDid == mSkuStrs[Index].Id) {
      return mSkuStrs[Index].String;
    }
  }

  return "Undefined SKU";
}

/**
  Get Pch Maximum Pcie Controller Number

  @retval Pch Maximum Pcie Root Port Number
**/
UINT8
GetPchMaxPcieControllerNum (
  VOID
  )
{
  return GetPchMaxPciePortNum () / PCH_PCIE_CONTROLLER_PORTS;
}

/**
  return support status for P2SB PCR 20-bit addressing

  @retval    TRUE
  @retval    FALSE
**/
BOOLEAN
IsP2sb20bPcrSupported (
  VOID
  )
{
  return (IsPchS ());
}

/**
  Determine Pch Series based on Device Id

  @param[in] LpcDeviceId      Lpc Device Id

  @retval PCH_SERIES          Pch Series
**/
PCH_SERIES
PchSeriesFromLpcDid (
  IN UINT16 LpcDeviceId
  )
{

  switch (LpcDeviceId & B_LPC_CFG_DID) {

    case V_LPC_CFG_DID_ADL_S:
      return PCH_S;

    case V_LPC_CFG_DID_ADL_P:
      return PCH_P;


    default:
      return PCH_UNKNOWN_SERIES;
  }
}

/**
  Determine Pch Stepping based on Revision ID

  @param[in] RevId            Pch Revision Id

  @retval PCH_STEPPING        Pch Stepping
**/
PCH_STEPPING
PchSteppingFromRevId (
  IN UINT8 RevId
  )
{
  return RevId;
}

/**
  Print Pch Stepping String

  @param[out] Buffer         Output buffer of string
  @param[in]  BufferSize     Buffer Size
  @param[in]  PchStep        Pch Stepping Type

  @retval VOID
**/
VOID
PchPrintSteppingStr (
  OUT CHAR8        *Buffer,
  IN  UINT32       BufferSize,
  IN  PCH_STEPPING PchStep
  )
{
  AsciiSPrint (Buffer, BufferSize, "%c%c", 'A' + (PchStep >> 4), '0' + (PchStep & 0xF));
}

/**
  Return Pch Generation

  @retval PCH_GENERATION      Pch Generation
**/
PCH_GENERATION
PchGeneration (
  VOID
  )
{
  return ADL_PCH;
}

/**
  Check if this is Server PCH

  @retval TRUE                It's a Server PCH
  @retval FALSE               It's not a Server PCH
**/
BOOLEAN
IsPchServer (
  VOID
  )
{
  return FALSE;
}

/**
  Check if this is PCH H series

  @retval TRUE                It's PCH H series
  @retval FALSE               It's not PCH H series
**/
BOOLEAN
IsPchH (
  VOID
  )
{
  return (PchSeries () == PCH_H || PchSeries () == PCH_S);
}

/**
  Check if this is PCH N series

  @retval TRUE                It's PCH N series
  @retval FALSE               It's not PCH N series
**/
BOOLEAN
IsPchN (
  VOID
  )
{
  return (PchSeries () == PCH_N);
}

/**
  Check if this is PCH LP series

  @retval TRUE     It's PCH LP series
  @retval FALSE    It's not PCH LP series
**/
BOOLEAN
IsPchLp (
  VOID
  )
{
  return (PchSeries () == PCH_LP || PchSeries () == PCH_P || PchSeries () == PCH_M || PchSeries () == PCH_N );
}

/**
  Get RST mode supported by the silicon

  @retval RST_MODE    RST mode supported by silicon
**/
RST_MODE
PchGetSupportedRstMode (
  VOID
  )
{
  UINT16 LpcDeviceId;
  LpcDeviceId = PchGetLpcDid();

  if (LpcDeviceId == V_ADL_PCH_S_LPC_CFG_DEVICE_ID_MB_11 || LpcDeviceId == V_ADL_PCH_S_LPC_CFG_DEVICE_ID_MB_10) {
    return RstPremium;
  } else {
    return RstUnsupported;
  }
}

/**
  Get PCH series ASCII string.

  @retval PCH Series string
**/
CHAR8*
PchGetSeriesStr (
  VOID
  )
{
  switch (PchSeries ()) {

    case PCH_P:
      return "PCH-P";

    case PCH_M:
      return "PCH-M";

    case PCH_S:
      return "PCH-S";

    case PCH_N:
      return "PCH-N";

    default:
      return NULL;
  }
}


GLOBAL_REMOVE_IF_UNREFERENCED
struct PCH_SKU_STRING mSkuStrs[] = {
  //
  // ADL PCH P M N and S Mobile LPC Device IDs
  //
  {V_ADL_PCH_P_LPC_CFG_DEVICE_ID_MB_0,   "ADL SKU 0"},
  {V_ADL_PCH_P_LPC_CFG_DEVICE_ID_MB_1,   "P Super SKU (SSKU)"},
  {V_ADL_PCH_P_LPC_CFG_DEVICE_ID_MB_2,   "P Premium"},
  {V_ADL_PCH_P_LPC_CFG_DEVICE_ID_MB_3,   "ADL No UFS"},
  {V_ADL_PCH_P_LPC_CFG_DEVICE_ID_MB_4,   "ADL SKU 4"},
  {V_ADL_PCH_P_LPC_CFG_DEVICE_ID_MB_5,   "ADL SKU 5"},
  {V_ADL_PCH_P_LPC_CFG_DEVICE_ID_MB_6,   "M Super SKU (SSKU)"},
  {V_ADL_PCH_P_LPC_CFG_DEVICE_ID_MB_7,   "M Premium"},
  {V_ADL_PCH_N_LPC_CFG_DEVICE_ID_MB_0,   "ADL SKU 0"},
  {V_ADL_PCH_N_LPC_CFG_DEVICE_ID_MB_1,   "TBD"},
  {V_ADL_PCH_N_LPC_CFG_DEVICE_ID_MB_2,   "TBD"},
  {V_ADL_PCH_N_LPC_CFG_DEVICE_ID_MB_3,   "TBD"},
  {V_ADL_PCH_N_LPC_CFG_DEVICE_ID_MB_4,   "TBD"},
  {V_ADL_PCH_N_LPC_CFG_DEVICE_ID_MB_5,   "TBD"},
  {V_ADL_PCH_N_LPC_CFG_DEVICE_ID_MB_6,   "TBD"},
  {V_ADL_PCH_N_LPC_CFG_DEVICE_ID_MB_7,   "TBD"},
  {V_ADL_PCH_N_LPC_CFG_DEVICE_ID_MB_8,   "TBD"},
  {V_ADL_PCH_S_LPC_CFG_DEVICE_ID_MB_0,   "ADL SKU 0"},
  {V_ADL_PCH_S_LPC_CFG_DEVICE_ID_MB_1,   "Client SuperSKU (SSKU)"},
  {V_ADL_PCH_S_LPC_CFG_DEVICE_ID_MB_2,   "Server SuperSKU (SSKU)"},
  {V_ADL_PCH_S_LPC_CFG_DEVICE_ID_MB_3,   "Q670"},
  {V_ADL_PCH_S_LPC_CFG_DEVICE_ID_MB_4,   "Z690"},
  {V_ADL_PCH_S_LPC_CFG_DEVICE_ID_MB_5,   "H670"},
  {V_ADL_PCH_S_LPC_CFG_DEVICE_ID_MB_6,   "B660"},
  {V_ADL_PCH_S_LPC_CFG_DEVICE_ID_MB_7,   "H610"},
  {V_ADL_PCH_S_LPC_CFG_DEVICE_ID_MB_8,   "W680"},
  {V_ADL_PCH_S_LPC_CFG_DEVICE_ID_MB_9,   "X699"},
  {V_ADL_PCH_S_LPC_CFG_DEVICE_ID_MB_10,  "Workstation SuperSKU (SSKU)"},
  {V_ADL_PCH_S_LPC_CFG_DEVICE_ID_MB_11,  "Workstation SuperSKU (SSKU) Locked"},


  {0xFFFF, NULL}
};

/**
  Get Pch Maximum Pcie Clock Number

  @retval Pch Maximum Pcie Clock Number
**/
UINT8
GetPchMaxPcieClockNum (
  VOID
  )
{
  if (IsPchS ()) {
    return 18;
  } else {
    //
    // ADL P has 7 Reference clock and clock reqs +(ADP-P) 3 Output Enables + 4 additional Clock reqs.
    return 10;
  }
}

/**
  Get Pch Maximum Pcie ClockReq Number

  @retval Pch Maximum Pcie ClockReq Number
**/
UINT8
GetPchMaxPcieClockReqNum (
  VOID
  )
{
  return GetPchMaxPcieClockNum ();
}

/**
  Get Pch Maximum Type C Port Number

  @retval Pch Maximum Type C Port Number
**/
UINT8
GetPchMaxTypeCPortNum (
  VOID
  )
{
  switch (PchSeries ()) {
    case PCH_P:
    case PCH_N:
      return 4;
    default:
      return 0;
  }
}

/**
  Check whether integrated LAN controller is supported by PCH Series.

  @retval TRUE                    GbE is supported in current PCH
  @retval FALSE                   GbE is not supported on current PCH
**/
BOOLEAN
PchIsGbeSupported (
  VOID
  )
{
  if (IsPchMSku ()) {
    return FALSE;
  }

  return TRUE;
}

/**
  Check whether integrated TSN is supported by PCH Series.

  @retval TRUE                    TSN is supported in current PCH
  @retval FALSE                   TSN is not supported on current PCH
**/
BOOLEAN
PchIsTsnSupported (
  VOID
  )
{
#if FixedPcdGet8(PcdEmbeddedEnable) == 0x1
  if (IsPchMSku ()) {
    return FALSE;
  }
  return TRUE;
#else
  return FALSE;
#endif
}

/**
  Check whether ISH is supported by PCH Series.

  @retval TRUE                    ISH is supported in current PCH
  @retval FALSE                   ISH is not supported on current PCH
**/
BOOLEAN
PchIsIshSupported (
  VOID
  )
{
  return TRUE;
}

/**
  Get Pch Maximum Pcie Root Port Number

  @retval Pch Maximum Pcie Root Port Number
**/
UINT8
GetPchMaxPciePortNum (
  VOID
  )
{
  switch (PchSeries ()) {
    case PCH_P:
    case PCH_N:
      return 12;
    case PCH_S:
      return 28;
    default:
      return 0;
  }
}

/**
  Get Pch Maximum Hda Dmic Link

  @retval Pch Maximum Hda Dmic Link
**/
UINT8
GetPchHdaMaxDmicLinkNum (
  VOID
  )
{
  return 2;
}

/**
  Get Pch Maximum Hda Sndw Link

  @retval Pch Maximum Hda Sndw Link
**/
UINT8
GetPchHdaMaxSndwLinkNum (
  VOID
  )
{
  return 4;
}

/**
  Get Pch Maximum Hda Ssp Link

  @retval Pch Maximum Hda Ssp Link
**/
UINT8
GetPchHdaMaxSspLinkNum (
  VOID
  )
{
  if (IsPchLp ()) {
    return 6;
  } else {
    return 3;
  }
}

/**
  Check if given Audio Interface is supported

  @param[in] AudioLinkType   Link type support to be checked
  @param[in] AudioLinkIndex  Link number

  @retval    TRUE           Link supported
  @retval    FALSE          Link not supported
**/
BOOLEAN
IsAudioInterfaceSupported (
  IN HDAUDIO_LINK_TYPE     AudioLinkType,
  IN UINT32                AudioLinkIndex
  )
{
  //
  // Interfaces supported:
  // 1. HDA Link (SDI0/SDI1)
  // 2. Display Audio Link (SDI2)
  // 3. SSP[0-5]
  // 4. SNDW[1-4]
  //
  switch (AudioLinkType) {
    case HdaLink:
    case HdaIDispLink:
      return TRUE;
    case HdaDmic:
      if (AudioLinkIndex < 2) {
        return TRUE;
      } else {
        return FALSE;
      }
    case HdaSsp:
      if (AudioLinkIndex < 3) {
        return TRUE;
      } else if (AudioLinkIndex < 6) {
        return IsPchLp ();
      } else {
        return FALSE;
      }
    case HdaSndw:
      if (AudioLinkIndex < 4) {
        return TRUE;
      } else {
        return FALSE;
      }
    default:
      return FALSE;
  }
}

/**
  Get Pch Usb2 Maximum Physical Port Number

  @retval Pch Usb2 Maximum Physical Port Number
**/
UINT8
GetPchUsb2MaxPhysicalPortNum (
  VOID
  )
{
  switch (PchSeries ()) {
    case PCH_P:
    case PCH_N:
      return 10;
    case PCH_S:
      return 14;
    default:
      return 0;
  }
}

/**
  Get Pch Maximum Usb2 Port Number of XHCI Controller

  @retval Pch Maximum Usb2 Port Number of XHCI Controller
**/
UINT8
GetPchXhciMaxUsb2PortNum (
  VOID
  )
{
  switch (PchSeries ()) {
    case PCH_P:
    case PCH_N:
      return 12;
    case PCH_S:
      return 16;
    default:
      return 0;
  }
}

/**
  Get Pch Maximum Usb3 Port Number of XHCI Controller

  @retval Pch Maximum Usb3 Port Number of XHCI Controller
**/
UINT8
GetPchXhciMaxUsb3PortNum (
  VOID
  )
{
  switch (PchSeries ()) {
    case PCH_P:
    case PCH_N:
      return 4;
    case PCH_S:
      return 10;
    default:
      return 0;
  }
}

/**
  Get Pch USB-R Port Number for KVM session

  @retval Pch USB-R Port Number for KVM session
**/
UINT8
GetPchUsbrKvmPortNum (
  VOID
  )
{
  return GetPchUsb2MaxPhysicalPortNum ();
}

/**
  Get Pch USB-R Port Number for Storage session

  @retval Pch USB-R Port Number for Storage session
**/
UINT8
GetPchUsbrStoragePortNum (
  VOID
  )
{
  return GetPchUsb2MaxPhysicalPortNum () + 1;
}

/**
  Gets the maximum number of UFS controller supported by this chipset.

  @return Number of supported UFS controllers
**/
UINT8
PchGetMaxUfsNum (
  VOID
  )
{
  if (IsPchS () || (PchGetLpcDid () == V_ADL_PCH_P_LPC_CFG_DEVICE_ID_MB_3)) {
    return 0;
  } else {
    return 2;
  }
}

/**
  Check if this chipset supports eMMC controller

  @retval BOOLEAN  TRUE if supported, FALSE otherwise
**/
BOOLEAN
IsPchEmmcSupported (
  VOID
  )
{
  switch (PchSeries ()) {
    case PCH_N:
      return TRUE;
    default:
      return FALSE;
  }
}

/**
  Check if this chipset supports SD controller

  @retval BOOLEAN  TRUE if supported, FALSE otherwise
**/
BOOLEAN
IsPchSdCardSupported (
  VOID
  )
{
  return FALSE;
}

/**
  Check if this chipset supports THC controller

  @retval BOOLEAN  TRUE if supported, FALSE otherwise
**/
BOOLEAN
IsPchThcSupported (
  VOID
  )
{
  return TRUE;
}

/**
  Check if this chipset supports HSIO BIOS Sync

  @retval BOOLEAN  TRUE if supported, FALSE otherwise
**/
BOOLEAN
IsPchChipsetInitSyncSupported (
  VOID
  )
{
  if (IsPchS ()) {
    return FALSE;
  } else {
    return TRUE;
  }
}

/**
  Check if link between PCH and CPU is an P-DMI

  @retval    TRUE           P-DMI link
  @retval    FALSE          Not an P-DMI link
**/
BOOLEAN
IsPchWithPdmi (
  VOID
  )
{
  return IsPchS ();
}

/**
  Check whether ATX Shutdown (PS_ON) is supported.

  @retval    TRUE           ATX Shutdown (PS_ON) is supported in PCH
  @retval    FALSE          ATX Shutdown (PS_ON) is not supported by PCH
**/
BOOLEAN
IsPchPSOnSupported (
  VOID
  )
{
  return IsPchH ();
}

/**
  Check if link between PCH and CPU is an OP-DMI

  @retval    TRUE           OP-DMI link
  @retval    FALSE          Not an OP-DMI link
**/
BOOLEAN
IsPchWithOpdmi (
  VOID
  )
{
  return !IsPchS ();
}

/**
  Check if link between PCH and CPU is an F-DMI

  @retval    TRUE           F-DMI link
  @retval    FALSE          Not an F-DMI link
**/
BOOLEAN
IsPchWithFdmi (
  VOID
  )
{
  return FALSE;
}
/**
  Get Pch Maximum ISH UART Controller number

  @retval Pch Maximum ISH UART controllers number
**/
UINT8
GetPchMaxIshUartControllersNum (
  VOID
  )
{
  return 2;
}

/**
  Get Pch Maximum ISH I2C Controller number

  @retval Pch Maximum ISH I2C controllers number
**/
UINT8
GetPchMaxIshI2cControllersNum (
  VOID
  )
{
  return 3;
}

/**
  Get Pch Maximum ISH SPI Controller number

  @retval Pch Maximum ISH SPI controllers number
**/
UINT8
GetPchMaxIshSpiControllersNum (
  VOID
  )
{
  return 1;
}

/**
  Get Pch Maximum ISH SPI Controller Cs pins number

  @retval Pch Maximum ISH SPI controller Cs pins number
**/
UINT8
GetPchMaxIshSpiControllerCsPinsNum (
  VOID
  )
{
  return 1;
}

/**
  Get Pch Maximum ISH GP number

  @retval Pch Maximum ISH GP number
**/
UINT8
GetPchMaxIshGpNum (
  VOID
  )
{
  return 8;
}

/**
  Get Pch Maximum Serial IO I2C controllers number

  @retval Pch Maximum Serial IO I2C controllers number
**/
UINT8
GetPchMaxSerialIoI2cControllersNum (
  VOID
  )
{
  return 8;
}

/**
  Get Pch Maximum Serial IO I3C controllers number

  @retval Pch Maximum Serial IO I3C controllers number
**/
UINT8
GetPchMaxSerialIoI3cControllersNum (
  VOID
  )
{
  return 0;
}

/**
  Get Pch Maximum Serial IO SPI controllers number

  @retval Pch Maximum Serial IO SPI controllers number
**/
UINT8
GetPchMaxSerialIoSpiControllersNum (
  VOID
  )
{
  return 7;
}

/**
  Get Pch Maximum Serial IO UART controllers number

  @retval Pch Maximum Serial IO UART controllers number
**/
UINT8
GetPchMaxSerialIoUartControllersNum (
  VOID
  )
{
  return 7;
}

/**
  Get Pch Maximum Serial IO SPI Chip Selects count

  @retval Pch Maximum Serial IO SPI Chip Selects number
**/
UINT8
GetPchMaxSerialIoSpiChipSelectsNum (
  VOID
  )
{
  return 2;
}

/**
  Get Pch Maximum ME Applet count

  @retval Pch Maximum ME Applet number
**/
UINT8
GetPchMaxMeAppletCount (
  VOID
  )
{
  return 31;
}

/**
  Get Pch Maximum ME Session count

  @retval Pch Maximum ME Sesion number
**/
UINT8
GetPchMaxMeSessionCount (
  VOID
  )
{
  return 16;
}

/**
  Get Pch Maximum THC count

  @retval Pch Maximum THC count number
**/
UINT8
GetPchMaxThcCount (
  VOID
  )
{
  return 2;
}

/**
  Check if this generation supports THC controller in HID over SPI mode

  @retval THC HID Mode support
**/
BOOLEAN
IsPchThcHidModeSupported (
  VOID
  )
{
  return TRUE;
}

/**
  Check if SPI in a given PCH generation supports an Extended BIOS Range Decode

  @retval TRUE or FALSE if PCH supports Extended BIOS Range Decode
**/
BOOLEAN
IsExtendedBiosRangeDecodeSupported (
  VOID
  )
{
  return TRUE;
}

#define SPI_PCH_LP_DMI_TARGET   0x23A8
#define SPI_PCH_S_DMI_TARGET    0x23B8

/**
  Returns DMI target for current PCH SPI

  @retval PCH SPI DMI target value
**/
UINT16
GetPchSpiDmiTarget (
  VOID
  )
{
  if (IsPchLp()) {
    return SPI_PCH_LP_DMI_TARGET;
  } else {
    return SPI_PCH_S_DMI_TARGET;
  }
}
