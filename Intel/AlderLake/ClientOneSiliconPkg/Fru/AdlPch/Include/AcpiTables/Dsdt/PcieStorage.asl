/**@file

  ACPI D3Cold/D3hot support for Storage device connected to PCIE Root Port

 @copyright
  INTEL CONFIDENTIAL
  Copyright 2020 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains an 'Intel Peripheral Driver' and is uniquely identified as
  "Intel Reference Module" and is licensed for Intel CPUs and chipsets under
  the terms of your license agreement with Intel or your vendor. This file may
  be modified by the user, subject to additional terms of the license agreement.

@par Specification Reference:
**/

      External(PCIE_ROOT_PORT.PXSX.PNVM, MethodObj, IntObj)
      External(PCIE_ROOT_PORT.PXSX.PAHC, MethodObj, IntObj)
      External(PCIE_ROOT_PORT.VDID, FieldUnitObj)

      //
      // 1. Check if Root Port exists.
      // 2. Check if Root Port is mapped under VMD.
      // 2. Check for a storage device.
      //
      If (CondRefOf(PCIE_ROOT_PORT)) {
        If(LNotEqual(PCIE_ROOT_PORT.VDID,0xFFFFFFFF)) {
          If (CondRefOf(\STD3)) {
            If (CondRefOf(PCIE_ROOT_PORT.PRMV)) {
              If (LEqual (PCIE_ROOT_PORT.PRMV, 1)) {
                Store(\STD3, PCIE_ROOT_PORT.RD3C)
              }
            }

            If(LOr(PCIE_ROOT_PORT.PXSX.PNVM(),PCIE_ROOT_PORT.PXSX.PAHC())) {
              Store(\STD3, PCIE_ROOT_PORT.RD3C)
            }
          }
        }
      }
