/**@file

  Serial IO SPI ACPI definitions

 @copyright
  INTEL CONFIDENTIAL
  Copyright 2020 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains an 'Intel Peripheral Driver' and is uniquely identified as
  "Intel Reference Module" and is licensed for Intel CPUs and chipsets under
  the terms of your license agreement with Intel or your vendor. This file may
  be modified by the user, subject to additional terms of the license agreement.

@par Specification Reference:
**/

#define SPI0_MODE         SM00
#define SPI1_MODE         SM01
#define SPI2_MODE         SM02
#define SPI3_MODE         SM03
#define SPI4_MODE         SM04
#define SPI5_MODE         SM05
#define SPI6_MODE         SM06

#define SPI0_PCIE_BASE    SC00
#define SPI1_PCIE_BASE    SC01
#define SPI2_PCIE_BASE    SC02
#define SPI3_PCIE_BASE    SC03
#define SPI4_PCIE_BASE    SC04
#define SPI5_PCIE_BASE    SC05
#define SPI6_PCIE_BASE    SC06


#define SPI_DEVICE_ADR       SERIAL_IO_SPI0_ADR
#define SPI_DEVICE_MODE      SPI0_MODE
#define SPI_DEVICE_PCI_BASE  SPI0_PCIE_BASE
Device (SPI0) {
Include ("SerialIoSpiController.asl")
}

#undef SPI_DEVICE_ADR
#undef SPI_DEVICE_MODE
#undef SPI_DEVICE_PCI_BASE
#define SPI_DEVICE_ADR       SERIAL_IO_SPI1_ADR
#define SPI_DEVICE_MODE      SPI1_MODE
#define SPI_DEVICE_PCI_BASE  SPI1_PCIE_BASE
Device (SPI1) {
Include ("SerialIoSpiController.asl")
}

#undef SPI_DEVICE_ADR
#undef SPI_DEVICE_MODE
#undef SPI_DEVICE_PCI_BASE
#define SPI_DEVICE_ADR       SERIAL_IO_SPI2_ADR
#define SPI_DEVICE_MODE      SPI2_MODE
#define SPI_DEVICE_PCI_BASE  SPI2_PCIE_BASE
Device (SPI2) {
Include ("SerialIoSpiController.asl")
}

#undef SPI_DEVICE_ADR
#undef SPI_DEVICE_MODE
#undef SPI_DEVICE_PCI_BASE
#define SPI_DEVICE_ADR       SERIAL_IO_SPI3_ADR
#define SPI_DEVICE_MODE      SPI3_MODE
#define SPI_DEVICE_PCI_BASE  SPI3_PCIE_BASE
Device (SPI3) {
Include ("SerialIoSpiController.asl")
}

#undef SPI_DEVICE_ADR
#undef SPI_DEVICE_MODE
#undef SPI_DEVICE_PCI_BASE
#define SPI_DEVICE_ADR       SERIAL_IO_SPI4_ADR
#define SPI_DEVICE_MODE      SPI4_MODE
#define SPI_DEVICE_PCI_BASE  SPI4_PCIE_BASE
Device (SPI4) {
Include ("SerialIoSpiController.asl")
}

#undef SPI_DEVICE_ADR
#undef SPI_DEVICE_MODE
#undef SPI_DEVICE_PCI_BASE
#define SPI_DEVICE_ADR       SERIAL_IO_SPI5_ADR
#define SPI_DEVICE_MODE      SPI5_MODE
#define SPI_DEVICE_PCI_BASE  SPI5_PCIE_BASE
Device (SPI5) {
Include ("SerialIoSpiController.asl")
}

#undef SPI_DEVICE_ADR
#undef SPI_DEVICE_MODE
#undef SPI_DEVICE_PCI_BASE
#define SPI_DEVICE_ADR       SERIAL_IO_SPI6_ADR
#define SPI_DEVICE_MODE      SPI6_MODE
#define SPI_DEVICE_PCI_BASE  SPI6_PCIE_BASE
Device (SPI6) {
Include ("SerialIoSpiController.asl")
}

