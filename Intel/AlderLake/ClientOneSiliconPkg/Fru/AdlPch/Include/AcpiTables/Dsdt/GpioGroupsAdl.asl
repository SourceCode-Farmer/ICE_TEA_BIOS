/**@file
 VER4 GPIO data for use with GPIO ASL lib

 @copyright
  INTEL CONFIDENTIAL
  Copyright 2019 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains an 'Intel Peripheral Driver' and is uniquely identified as
  "Intel Reference Module" and is licensed for Intel CPUs and chipsets under
  the terms of your license agreement with Intel or your vendor. This file may
  be modified by the user, subject to additional terms of the license agreement.

@par Specification Reference:
**/
Include ("GpioAcpiDefines.h")
Include ("GpioAcpiDefinesAdl.h")

#include "Register/GpioRegs.h"
#include "Register/GpioRegsVer4.h"

//
// If in GPIO_GROUP_INFO structure certain register doesn't exist
// it will have value equal to NO_REGISTER_FOR_PROPERTY
//
#define NO_REGISTER_FOR_PROPERTY 0xFFFF

//
// If in GPIO_GROUP_INFO structure certain group should not be used
// by GPIO OS driver then "Gpio base number" field should be set to below value
//
#define GPIO_OS_DRV_NOT_SUPPORTED 0xFFFF

//
// GPIO Library objects
//
Scope(\_SB)
{
  //
  // GPIO information data structure
  //

  //
  // GPIO information data structure for PCH-S
  //
  Name(GPCS, Package(){
    Package(){ // GPP_I
      PCH_GPIO_COM0,
      GPIO_VER4_PCH_S_GPIO_GPP_I_PAD_MAX,
      R_GPIO_VER4_PCH_S_GPIO_PCR_GPP_I_PADCFG_OFFSET,
      R_GPIO_VER4_PCH_S_GPIO_PCR_GPP_I_HOSTSW_OWN,
      R_GPIO_VER4_PCH_S_GPIO_PCR_GPP_I_PAD_OWN,
      R_GPIO_VER4_PCH_S_GPIO_PCR_GPP_I_GPI_GPE_STS,
      R_GPIO_VER4_PCH_S_GPIO_PCR_GPP_I_PADCFGLOCK,
      R_GPIO_VER4_PCH_S_GPIO_PCR_GPP_I_PADCFGLOCKTX,
      GPIO_VER4_S_DRIVER_GPP_I0
    },
    Package(){ // GPP_R
      PCH_GPIO_COM0,
      GPIO_VER4_PCH_S_GPIO_GPP_R_PAD_MAX,
      R_GPIO_VER4_PCH_S_GPIO_PCR_GPP_R_PADCFG_OFFSET,
      R_GPIO_VER4_PCH_S_GPIO_PCR_GPP_R_HOSTSW_OWN,
      R_GPIO_VER4_PCH_S_GPIO_PCR_GPP_R_PAD_OWN,
      R_GPIO_VER4_PCH_S_GPIO_PCR_GPP_R_GPI_GPE_STS,
      R_GPIO_VER4_PCH_S_GPIO_PCR_GPP_R_PADCFGLOCK,
      R_GPIO_VER4_PCH_S_GPIO_PCR_GPP_R_PADCFGLOCKTX,
      GPIO_VER4_S_DRIVER_GPP_R0
    },
    Package(){ // GPP_J
      PCH_GPIO_COM0,
      GPIO_VER4_PCH_S_GPIO_GPP_J_PAD_MAX,
      R_GPIO_VER4_PCH_S_GPIO_PCR_GPP_J_PADCFG_OFFSET,
      R_GPIO_VER4_PCH_S_GPIO_PCR_GPP_J_HOSTSW_OWN,
      R_GPIO_VER4_PCH_S_GPIO_PCR_GPP_J_PAD_OWN,
      R_GPIO_VER4_PCH_S_GPIO_PCR_GPP_J_GPI_GPE_STS,
      R_GPIO_VER4_PCH_S_GPIO_PCR_GPP_J_PADCFGLOCK,
      R_GPIO_VER4_PCH_S_GPIO_PCR_GPP_J_PADCFGLOCKTX,
      GPIO_VER4_S_DRIVER_GPP_B0
    },
    Package(){ // VGPIO
      PCH_GPIO_COM0,
      GPIO_VER4_PCH_S_GPIO_VGPIO_PAD_MAX,
      R_GPIO_VER4_PCH_S_GPIO_PCR_VGPIO_PADCFG_OFFSET,
      R_GPIO_VER4_PCH_S_GPIO_PCR_VGPIO_HOSTSW_OWN,
      R_GPIO_VER4_PCH_S_GPIO_PCR_VGPIO_PAD_OWN,
      R_GPIO_VER4_PCH_S_GPIO_PCR_VGPIO_GPI_GPE_STS,
      R_GPIO_VER4_PCH_S_GPIO_PCR_VGPIO_PADCFGLOCK,
      R_GPIO_VER4_PCH_S_GPIO_PCR_VGPIO_PADCFGLOCKTX,
      GPIO_VER4_S_DRIVER_VGPIO0
    },
    Package(){ // VGPIO_0
      PCH_GPIO_COM0,
      GPIO_VER4_PCH_S_GPIO_VGPIO_0_PAD_MAX,
      R_GPIO_VER4_PCH_S_GPIO_PCR_VGPIO_0_PADCFG_OFFSET,
      R_GPIO_VER4_PCH_S_GPIO_PCR_VGPIO_0_HOSTSW_OWN,
      R_GPIO_VER4_PCH_S_GPIO_PCR_VGPIO_0_PAD_OWN,
      R_GPIO_VER4_PCH_S_GPIO_PCR_VGPIO_0_GPI_GPE_STS,
      R_GPIO_VER4_PCH_S_GPIO_PCR_VGPIO_0_PADCFGLOCK,
      R_GPIO_VER4_PCH_S_GPIO_PCR_VGPIO_0_PADCFGLOCKTX,
      GPIO_VER4_S_DRIVER_VGPIO_0_0
    },
    Package(){ // GPP_B
      PCH_GPIO_COM1,
      GPIO_VER4_PCH_S_GPIO_GPP_B_PAD_MAX,
      R_GPIO_VER4_PCH_S_GPIO_PCR_GPP_B_PADCFG_OFFSET,
      R_GPIO_VER4_PCH_S_GPIO_PCR_GPP_B_HOSTSW_OWN,
      R_GPIO_VER4_PCH_S_GPIO_PCR_GPP_B_PAD_OWN,
      R_GPIO_VER4_PCH_S_GPIO_PCR_GPP_B_GPI_GPE_STS,
      R_GPIO_VER4_PCH_S_GPIO_PCR_GPP_B_PADCFGLOCK,
      R_GPIO_VER4_PCH_S_GPIO_PCR_GPP_B_PADCFGLOCKTX,
      GPIO_VER4_S_DRIVER_GPP_B0
    },
    Package(){ // GPP_G
      PCH_GPIO_COM1,
      GPIO_VER4_PCH_S_GPIO_GPP_G_PAD_MAX,
      R_GPIO_VER4_PCH_S_GPIO_PCR_GPP_G_PADCFG_OFFSET,
      R_GPIO_VER4_PCH_S_GPIO_PCR_GPP_G_HOSTSW_OWN,
      R_GPIO_VER4_PCH_S_GPIO_PCR_GPP_G_PAD_OWN,
      R_GPIO_VER4_PCH_S_GPIO_PCR_GPP_G_GPI_GPE_STS,
      R_GPIO_VER4_PCH_S_GPIO_PCR_GPP_G_PADCFGLOCK,
      R_GPIO_VER4_PCH_S_GPIO_PCR_GPP_G_PADCFGLOCKTX,
      GPIO_VER4_S_DRIVER_GPP_G0
    },
    Package(){ // GPP_H
      PCH_GPIO_COM1,
      GPIO_VER4_PCH_S_GPIO_GPP_H_PAD_MAX,
      R_GPIO_VER4_PCH_S_GPIO_PCR_GPP_H_PADCFG_OFFSET,
      R_GPIO_VER4_PCH_S_GPIO_PCR_GPP_H_HOSTSW_OWN,
      R_GPIO_VER4_PCH_S_GPIO_PCR_GPP_H_PAD_OWN,
      R_GPIO_VER4_PCH_S_GPIO_PCR_GPP_H_GPI_GPE_STS,
      R_GPIO_VER4_PCH_S_GPIO_PCR_GPP_H_PADCFGLOCK,
      R_GPIO_VER4_PCH_S_GPIO_PCR_GPP_H_PADCFGLOCKTX,
      GPIO_VER4_S_DRIVER_GPP_H0
    },
    Package(){ // GPD
      PCH_GPIO_COM2,
      GPIO_VER4_PCH_S_GPIO_GPD_PAD_MAX,
      R_GPIO_VER4_PCH_S_GPIO_PCR_GPD_PADCFG_OFFSET,
      R_GPIO_VER4_PCH_S_GPIO_PCR_GPD_HOSTSW_OWN,
      R_GPIO_VER4_PCH_S_GPIO_PCR_GPD_PAD_OWN,
      R_GPIO_VER4_PCH_S_GPIO_PCR_GPD_GPI_GPE_STS,
      R_GPIO_VER4_PCH_S_GPIO_PCR_GPD_PADCFGLOCK,
      R_GPIO_VER4_PCH_S_GPIO_PCR_GPD_PADCFGLOCKTX,
      0 // TBD
    },
    Package(){ // SPI
      PCH_GPIO_COM3,
      GPIO_VER4_PCH_S_GPIO_SPI_PAD_MAX,
      R_GPIO_VER4_PCH_S_GPIO_PCR_SPI_PADCFG_OFFSET,
      R_GPIO_VER4_PCH_S_GPIO_PCR_SPI_HOSTSW_OWN,
      R_GPIO_VER4_PCH_S_GPIO_PCR_SPI_PAD_OWN,
      R_GPIO_VER4_PCH_S_GPIO_PCR_SPI_GPI_GPE_STS,
      R_GPIO_VER4_PCH_S_GPIO_PCR_SPI_PADCFGLOCK,
      R_GPIO_VER4_PCH_S_GPIO_PCR_SPI_PADCFGLOCKTX,
      0 // TBD
    },
    Package(){ // GPP_A
      PCH_GPIO_COM3,
      GPIO_VER4_PCH_S_GPIO_GPP_A_PAD_MAX,
      R_GPIO_VER4_PCH_S_GPIO_PCR_GPP_A_PADCFG_OFFSET,
      R_GPIO_VER4_PCH_S_GPIO_PCR_GPP_A_HOSTSW_OWN,
      R_GPIO_VER4_PCH_S_GPIO_PCR_GPP_A_PAD_OWN,
      R_GPIO_VER4_PCH_S_GPIO_PCR_GPP_A_GPI_GPE_STS,
      R_GPIO_VER4_PCH_S_GPIO_PCR_GPP_A_PADCFGLOCK,
      R_GPIO_VER4_PCH_S_GPIO_PCR_GPP_A_PADCFGLOCKTX,
      GPIO_VER4_S_DRIVER_GPP_A0
    },
    Package(){ // GPP_C
      PCH_GPIO_COM3,
      GPIO_VER4_PCH_S_GPIO_GPP_C_PAD_MAX,
      R_GPIO_VER4_PCH_S_GPIO_PCR_GPP_C_PADCFG_OFFSET,
      R_GPIO_VER4_PCH_S_GPIO_PCR_GPP_C_HOSTSW_OWN,
      R_GPIO_VER4_PCH_S_GPIO_PCR_GPP_C_PAD_OWN,
      R_GPIO_VER4_PCH_S_GPIO_PCR_GPP_C_GPI_GPE_STS,
      R_GPIO_VER4_PCH_S_GPIO_PCR_GPP_C_PADCFGLOCK,
      R_GPIO_VER4_PCH_S_GPIO_PCR_GPP_C_PADCFGLOCKTX,
      GPIO_VER4_S_DRIVER_GPP_C0
    },
    Package(){ // VGPIO_3
      PCH_GPIO_COM3,
      GPIO_VER4_PCH_S_GPIO_VGPIO_3_PAD_MAX,
      R_GPIO_VER4_PCH_S_GPIO_PCR_VGPIO_3_PADCFG_OFFSET,
      R_GPIO_VER4_PCH_S_GPIO_PCR_VGPIO_3_HOSTSW_OWN,
      R_GPIO_VER4_PCH_S_GPIO_PCR_VGPIO_3_PAD_OWN,
      R_GPIO_VER4_PCH_S_GPIO_PCR_VGPIO_3_GPI_GPE_STS,
      R_GPIO_VER4_PCH_S_GPIO_PCR_VGPIO_3_PADCFGLOCK,
      R_GPIO_VER4_PCH_S_GPIO_PCR_VGPIO_3_PADCFGLOCKTX,
      GPIO_VER4_S_DRIVER_VGPIO30
    },
    Package(){ // GPP_S
      PCH_GPIO_COM4,
      GPIO_VER4_PCH_S_GPIO_GPP_S_PAD_MAX,
      R_GPIO_VER4_PCH_S_GPIO_PCR_GPP_S_PADCFG_OFFSET,
      R_GPIO_VER4_PCH_S_GPIO_PCR_GPP_S_HOSTSW_OWN,
      R_GPIO_VER4_PCH_S_GPIO_PCR_GPP_S_PAD_OWN,
      R_GPIO_VER4_PCH_S_GPIO_PCR_GPP_S_GPI_GPE_STS,
      R_GPIO_VER4_PCH_S_GPIO_PCR_GPP_S_PADCFGLOCK,
      R_GPIO_VER4_PCH_S_GPIO_PCR_GPP_S_PADCFGLOCKTX,
      GPIO_VER4_S_DRIVER_GPP_S0
    },
    Package(){ // GPP_E
      PCH_GPIO_COM4,
      GPIO_VER4_PCH_S_GPIO_GPP_E_PAD_MAX,
      R_GPIO_VER4_PCH_S_GPIO_PCR_GPP_E_PADCFG_OFFSET,
      R_GPIO_VER4_PCH_S_GPIO_PCR_GPP_E_HOSTSW_OWN,
      R_GPIO_VER4_PCH_S_GPIO_PCR_GPP_E_PAD_OWN,
      R_GPIO_VER4_PCH_S_GPIO_PCR_GPP_E_GPI_GPE_STS,
      R_GPIO_VER4_PCH_S_GPIO_PCR_GPP_E_PADCFGLOCK,
      R_GPIO_VER4_PCH_S_GPIO_PCR_GPP_E_PADCFGLOCKTX,
      GPIO_VER4_S_DRIVER_GPP_E0
    },
    Package(){ // GPP_K
      PCH_GPIO_COM4,
      GPIO_VER4_PCH_S_GPIO_GPP_K_PAD_MAX,
      R_GPIO_VER4_PCH_S_GPIO_PCR_GPP_K_PADCFG_OFFSET,
      R_GPIO_VER4_PCH_S_GPIO_PCR_GPP_K_HOSTSW_OWN,
      R_GPIO_VER4_PCH_S_GPIO_PCR_GPP_K_PAD_OWN,
      R_GPIO_VER4_PCH_S_GPIO_PCR_GPP_K_GPI_GPE_STS,
      R_GPIO_VER4_PCH_S_GPIO_PCR_GPP_K_PADCFGLOCK,
      R_GPIO_VER4_PCH_S_GPIO_PCR_GPP_K_PADCFGLOCKTX,
      GPIO_VER4_S_DRIVER_GPP_K0
    },
    Package(){ // GPP_F
      PCH_GPIO_COM4,
      GPIO_VER4_PCH_S_GPIO_GPP_F_PAD_MAX,
      R_GPIO_VER4_PCH_S_GPIO_PCR_GPP_F_PADCFG_OFFSET,
      R_GPIO_VER4_PCH_S_GPIO_PCR_GPP_F_HOSTSW_OWN,
      R_GPIO_VER4_PCH_S_GPIO_PCR_GPP_F_PAD_OWN,
      R_GPIO_VER4_PCH_S_GPIO_PCR_GPP_F_GPI_GPE_STS,
      R_GPIO_VER4_PCH_S_GPIO_PCR_GPP_F_PADCFGLOCK,
      R_GPIO_VER4_PCH_S_GPIO_PCR_GPP_F_PADCFGLOCKTX,
      GPIO_VER4_S_DRIVER_GPP_F0
    },
    Package(){ // GPP_D
      PCH_GPIO_COM5,
      GPIO_VER4_PCH_S_GPIO_GPP_D_PAD_MAX,
      R_GPIO_VER4_PCH_S_GPIO_PCR_GPP_D_PADCFG_OFFSET,
      R_GPIO_VER4_PCH_S_GPIO_PCR_GPP_D_HOSTSW_OWN,
      R_GPIO_VER4_PCH_S_GPIO_PCR_GPP_D_PAD_OWN,
      R_GPIO_VER4_PCH_S_GPIO_PCR_GPP_D_GPI_GPE_STS,
      R_GPIO_VER4_PCH_S_GPIO_PCR_GPP_D_PADCFGLOCK,
      R_GPIO_VER4_PCH_S_GPIO_PCR_GPP_D_PADCFGLOCKTX,
      GPIO_VER4_S_DRIVER_GPP_D0
    }
  })

  //
  // Object for storing RX Level/Edge Configuration for all pads.
  // One fields contains data for one pad.
  //   00b = Level
  //   01b = Edge (RxInv=0 for rising edge; 1 for falling edge)
  //   10b = Disabled
  //   11b = Either rising edge or falling edge
  //
  // Each row has data for one group. Buffer size needs
  // to equal to number of pads
  Name(RXEV, Package(){
    Buffer(GPIO_VER4_PCH_S_GPIO_GPP_I_PAD_MAX){},   // PCH-S: GPP_I
    Buffer(GPIO_VER4_PCH_S_GPIO_GPP_R_PAD_MAX){},   // PCH-S: GPP_R
    Buffer(GPIO_VER4_PCH_S_GPIO_GPP_J_PAD_MAX){},   // PCH-S: GPP_J
    Buffer(GPIO_VER4_PCH_S_GPIO_VGPIO_PAD_MAX){},   // PCH-S: VGPIO
    Buffer(GPIO_VER4_PCH_S_GPIO_VGPIO_0_PAD_MAX){}, // PCH-S: VGPIO0
    Buffer(GPIO_VER4_PCH_S_GPIO_GPP_B_PAD_MAX){},   // PCH-S: GPP_B
    Buffer(GPIO_VER4_PCH_S_GPIO_GPP_G_PAD_MAX){},   // PCH-S: GPP_G
    Buffer(GPIO_VER4_PCH_S_GPIO_GPP_H_PAD_MAX){},   // PCH-S: GPP_H
    Buffer(GPIO_VER4_PCH_S_GPIO_GPD_PAD_MAX){},     // PCH-S: GPD
    Buffer(GPIO_VER4_PCH_S_GPIO_SPI_PAD_MAX){},     // PCH-S: SPI
    Buffer(GPIO_VER4_PCH_S_GPIO_GPP_A_PAD_MAX){},   // PCH-S: GPP_A
    Buffer(GPIO_VER4_PCH_S_GPIO_GPP_C_PAD_MAX){},   // PCH-S: GPP_C
    Buffer(GPIO_VER4_PCH_S_GPIO_VGPIO_3_PAD_MAX){}, // PCH-S: VGPIO3
    Buffer(GPIO_VER4_PCH_S_GPIO_GPP_S_PAD_MAX){},   // PCH-S: GPP_S
    Buffer(GPIO_VER4_PCH_S_GPIO_GPP_E_PAD_MAX){},   // PCH-S: GPP_E
    Buffer(GPIO_VER4_PCH_S_GPIO_GPP_K_PAD_MAX){},   // PCH-S: GPP_K
    Buffer(GPIO_VER4_PCH_S_GPIO_GPP_F_PAD_MAX){},   // PCH-S: GPP_F
    Buffer(GPIO_VER4_PCH_S_GPIO_GPP_D_PAD_MAX){}    // PCH-S: GPP_D
  })
}