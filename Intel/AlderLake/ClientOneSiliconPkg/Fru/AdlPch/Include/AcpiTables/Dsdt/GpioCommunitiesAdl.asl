/**@file
 ACPI definition for GPIO controller for ADL

 @copyright
  INTEL CONFIDENTIAL
  Copyright 2019 - 2020 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains an 'Intel Peripheral Driver' and is uniquely identified as
  "Intel Reference Module" and is licensed for Intel CPUs and chipsets under
  the terms of your license agreement with Intel or your vendor. This file may
  be modified by the user, subject to additional terms of the license agreement.

@par Specification Reference:
**/
Include ("GpioAcpiDefinesAdl.h")

Scope(\_SB) {
  //----------------------------
  //  GPIO Controller
  //----------------------------
  Device (GPI0)
  {
    Method (_HID) {
      // Return motherboard reserved resources HID when GPIO is hidden
      If (LEqual (GPHD, 1)) {
        Return ("PNP0C02")
      }
        Return (GPIO_VER4_S_ACPI_HID)
    }

    Name (LINK,"\\_SB.GPI0")

    Method (_CRS, 0x0, NotSerialized) {
      Name(RBFS,ResourceTemplate(){
        Interrupt (ResourceConsumer, Level, ActiveLow, Shared, , , IRQH) { 14 } //Interrupt IRQ_EN
        Memory32Fixed (ReadWrite, 0x00000000, 0x00010000, RBS0)
        Memory32Fixed (ReadWrite, 0x00000000, 0x00010000, RBS1)
        Memory32Fixed (ReadWrite, 0x00000000, 0x00010000, RBS3)
        Memory32Fixed (ReadWrite, 0x00000000, 0x00010000, RBS4)
        Memory32Fixed (ReadWrite, 0x00000000, 0x00010000, RBS5)
      })
      CreateDWordField(RBFS,IRQH._INT,INTH)
      Store(SGIR,INTH)
      CreateDWordField(RBFS,RBS0._BAS,CMH0)
      Store( Add(SBRG,PCH_GPIO_COM0), CMH0)
      CreateDWordField(RBFS,RBS1._BAS,CMH1)
      Store( Add(SBRG,PCH_GPIO_COM1), CMH1)
      CreateDWordField(RBFS,RBS3._BAS,CMH3)
      Store( Add(SBRG,PCH_GPIO_COM3), CMH3)
      CreateDWordField(RBFS,RBS4._BAS,CMH4)
      Store( Add(SBRG,PCH_GPIO_COM4), CMH4)
      CreateDWordField(RBFS,RBS5._BAS,CMH5)
      Store( Add(SBRG,PCH_GPIO_COM5), CMH5)
      Return (RBFS)
    }

    Method (_STA, 0x0, NotSerialized) {
      If(LEqual(GPHD, 1)) { // Hide GPIO ACPI device
        Return(0x8)
      }
      Return(0xF)
    }
  } // END Device(GPIO)
} // END Scope(\_SB.PC00)
