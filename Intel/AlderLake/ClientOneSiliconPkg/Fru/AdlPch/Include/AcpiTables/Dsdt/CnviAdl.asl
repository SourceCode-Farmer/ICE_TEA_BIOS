/**@file
  CNVi ACPI definitions

 @copyright
  INTEL CONFIDENTIAL
  Copyright 2020 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains an 'Intel Peripheral Driver' and is uniquely identified as
  "Intel Reference Module" and is licensed for Intel CPUs and chipsets under
  the terms of your license agreement with Intel or your vendor. This file may
  be modified by the user, subject to additional terms of the license agreement.

@par Specification Reference:
**/

#include <Pins/GpioPinsVer2Lp.h>
#include <Pins/GpioPinsVer4S.h>
#include <Register/CnviRegs.h>

External(\_SB.BTRK, MethodObj)
External(\_SB.GBTR, MethodObj)
External(\CNMT)

Scope (\_SB) {
  Method (GBTP, 0, Serialized) {
    //
    // Get BT enable (CNV_BTEN) pin
    //
    If (LOr (LEqual (PCHS, PCH_P), LEqual (PCHS, PCH_M))) {
      Return (GPIO_VER2_LP_VGPIO0)
    } ElseIf (LEqual (PCHS, PCH_S)) {
      Return (GPIO_VER4_S_VGPIO0)
    } Else {
      Return (0)
    }
  }

}

Scope (\_SB.PC00.CNVW) {

  Name (PLRB, 0)

  // Define Platform-level device reset power resource
  PowerResource (WRST, 5, 0) {
    // Define the PowerResource for CNVi WiFi
    // PowerResource expects to have _STA, _ON and _OFF Method per ACPI Spec. Not having one of them will cause BSOD

    // Method: Dummy _STA() to comply with ACPI Spec
    Method (_STA) {
      Return (0x01)
    }

    // Method: Dummy _ON() to comply with ACPI Spec
    Method (_ON, 0) {
    }

    // Method: Dummy _OFF() to comply with ACPI Spec
    Method (_OFF, 0) {
    }

    Method (_RST, 0, NotSerialized) {
      Store (Acquire (\CNMT, 1000), Local0) // save Acquire result so we can check for Mutex acquired
      If (LEqual (Local0, Zero)) // check for Mutex acquired
      {
        CFLR () // WiFi FLR
        Store (1, \_SB.PC00.CNVW.PRRS) // Set Core PLDR Completed successfully

        // Perform WiFi PLDR if RSTT = 1
        If (LAnd (CondRefOf (\_SB.PC00.CNVW.RSTT), LEqual (\_SB.PC00.CNVW.RSTT, 1))) {

          If (LEqual (PCHS, PCH_S)) {
            Store (R_CNVI_VER4_PCR_CNVI_PLDR_ABORT, PLRB)
          } Else {
            Store (R_CNVI_VER2_PCR_CNVI_PLDR_ABORT, PLRB)
          }

          // Query previous PLDR completed by checking PLDR_ABORT_REQUEST = 0
          If (LEqual (And (PCRR (\PCNV, PLRB), B_CNVI_PCR_CNVI_PLDR_ABORT_REQUEST), Zero)) {

            // If BT RF-Kill deasserted, assert BT RF-Kill and wait for 105 ms
            If (LEqual (\_SB.GBTR (), 1)) {
              \_SB.BTRK (0x00) // assert W_DISABLE2#
              Sleep (105)
              Store (1, Local2)
            }

            // Enable CNVi PLDR request and wait for 10 ms
            PCRO (\PCNV, PLRB, Or (B_CNVI_PCR_CNVI_PLDR_ABORT_ENABLE, B_CNVI_PCR_CNVI_PLDR_ABORT_REQUEST))
            Sleep (10)
            Store (PCRR (\PCNV, PLRB), Local1)

            // Check PLDR request is successful if PLDR_ABORT_REQUEST = 0 & CNVB_CNVI_READY = 1
            If (LAnd (LEqual (And (Local1, B_CNVI_PCR_CNVI_PLDR_ABORT_REQUEST), 0), And (Local1, B_CNVI_PCR_SCU_CNVB_CNVI_READY))) {
              Store (2, \_SB.PC00.CNVW.PRRS) // Set Product PLDR Completed successfully
              // Deassert BT RF-Kill and wait for 105 ms
              If (LEqual (Local2, 1)) {
                \_SB.BTRK (0x01) // deassert W_DISABLE2#
                Sleep (105)
              }
            } Else {
              // PLDR timeout
              Store (4, \_SB.PC00.CNVW.PRRS) // Set Product PLDR timeout
            }
          } Else {
            // Previous PLDR not completed
            Store (3, \_SB.PC00.CNVW.PRRS) // Set Previous PLDR Not Completed
          }
        }

        Release (\CNMT)
      }
    }
  } // End WRST

  Method (CFLR) {
    If (LEqual (WFLR, 1)) {
      Store (1, WIFR)
    }
  }
}
