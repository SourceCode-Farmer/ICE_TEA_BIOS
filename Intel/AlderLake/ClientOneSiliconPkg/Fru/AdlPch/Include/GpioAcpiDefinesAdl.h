/**@file
 VER4 GPIO ASL header

@copyright
  INTEL CONFIDENTIAL
  Copyright 2019 - 2020 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains an 'Intel Peripheral Driver' and is uniquely identified as
  "Intel Reference Module" and is licensed for Intel CPUs and chipsets under
  the terms of your license agreement with Intel or your vendor. This file may
  be modified by the user, subject to additional terms of the license agreement.

@par Specification Reference:
**/

//
// Definition for GPIO groups and pads
//
#ifndef _GPIO_ACPI_DEFINES_ADL_
#define _GPIO_ACPI_DEFINES_ADL_

#include "Pins/GpioPinsVer4S.h"

#define GPIO_VER4_S_ACPI_HID   "INTC1056" //ADL-S GPIO Controller

//
// ADL-PCH-S
//
#define GPIO_VER4_S_DRIVER_GPP_I0                 0
#define GPIO_VER4_S_DRIVER_GPP_I1                 1
#define GPIO_VER4_S_DRIVER_GPP_I2                 2
#define GPIO_VER4_S_DRIVER_GPP_I3                 3
#define GPIO_VER4_S_DRIVER_GPP_I4                 4
#define GPIO_VER4_S_DRIVER_GPP_I5                 5
#define GPIO_VER4_S_DRIVER_GPP_I6                 6
#define GPIO_VER4_S_DRIVER_GPP_I7                 7
#define GPIO_VER4_S_DRIVER_GPP_I8                 8
#define GPIO_VER4_S_DRIVER_GPP_I9                 9
#define GPIO_VER4_S_DRIVER_GPP_I10                10
#define GPIO_VER4_S_DRIVER_GPP_I11                11
#define GPIO_VER4_S_DRIVER_GPP_I12                12
#define GPIO_VER4_S_DRIVER_GPP_I13                13
#define GPIO_VER4_S_DRIVER_GPP_I14                14
#define GPIO_VER4_S_DRIVER_GPP_I15                15
#define GPIO_VER4_S_DRIVER_GPP_I16                16
#define GPIO_VER4_S_DRIVER_GPP_I17                17
#define GPIO_VER4_S_DRIVER_GPP_I18                18
#define GPIO_VER4_S_DRIVER_GPP_I19                19
#define GPIO_VER4_S_DRIVER_GPP_I20                20
#define GPIO_VER4_S_DRIVER_GPP_I21                21
#define GPIO_VER4_S_DRIVER_GPP_I22                22

#define GPIO_VER4_S_DRIVER_GPP_R0                 32
#define GPIO_VER4_S_DRIVER_GPP_R1                 33
#define GPIO_VER4_S_DRIVER_GPP_R2                 34
#define GPIO_VER4_S_DRIVER_GPP_R3                 35
#define GPIO_VER4_S_DRIVER_GPP_R4                 36
#define GPIO_VER4_S_DRIVER_GPP_R5                 37
#define GPIO_VER4_S_DRIVER_GPP_R6                 38
#define GPIO_VER4_S_DRIVER_GPP_R7                 39
#define GPIO_VER4_S_DRIVER_GPP_R8                 40
#define GPIO_VER4_S_DRIVER_GPP_R9                 41
#define GPIO_VER4_S_DRIVER_GPP_R10                42
#define GPIO_VER4_S_DRIVER_GPP_R11                43
#define GPIO_VER4_S_DRIVER_GPP_R12                44
#define GPIO_VER4_S_DRIVER_GPP_R13                45
#define GPIO_VER4_S_DRIVER_GPP_R14                46
#define GPIO_VER4_S_DRIVER_GPP_R15                47
#define GPIO_VER4_S_DRIVER_GPP_R16                48
#define GPIO_VER4_S_DRIVER_GPP_R17                49
#define GPIO_VER4_S_DRIVER_GPP_R18                50
#define GPIO_VER4_S_DRIVER_GPP_R19                51
#define GPIO_VER4_S_DRIVER_GPP_R20                52
#define GPIO_VER4_S_DRIVER_GPP_R21                53
#define GPIO_VER4_S_DRIVER_GSPI2_CLK_LOOPBK       54

#define GPIO_VER4_S_DRIVER_GPP_J0                 64
#define GPIO_VER4_S_DRIVER_GPP_J1                 65
#define GPIO_VER4_S_DRIVER_GPP_J2                 66
#define GPIO_VER4_S_DRIVER_GPP_J3                 67
#define GPIO_VER4_S_DRIVER_GPP_J4                 68
#define GPIO_VER4_S_DRIVER_GPP_J5                 69
#define GPIO_VER4_S_DRIVER_GPP_J6                 70
#define GPIO_VER4_S_DRIVER_GPP_J7                 71
#define GPIO_VER4_S_DRIVER_GPP_J8                 72
#define GPIO_VER4_S_DRIVER_GPP_J9                 73
#define GPIO_VER4_S_DRIVER_GPP_J10                74
#define GPIO_VER4_S_DRIVER_GPP_J11                75

#define GPIO_VER4_S_DRIVER_VGPIO0                 96
#define GPIO_VER4_S_DRIVER_VGPIO4                 97
#define GPIO_VER4_S_DRIVER_VGPIO5                 98
#define GPIO_VER4_S_DRIVER_VGPIO6                 99
#define GPIO_VER4_S_DRIVER_VGPIO7                 100
#define GPIO_VER4_S_DRIVER_VGPIO8                 101
#define GPIO_VER4_S_DRIVER_VGPIO9                 102
#define GPIO_VER4_S_DRIVER_VGPIO10                103
#define GPIO_VER4_S_DRIVER_VGPIO11                104
#define GPIO_VER4_S_DRIVER_VGPIO12                105
#define GPIO_VER4_S_DRIVER_VGPIO13                106
#define GPIO_VER4_S_DRIVER_VGPIO18                107
#define GPIO_VER4_S_DRIVER_VGPIO19                108
#define GPIO_VER4_S_DRIVER_VGPIO20                109
#define GPIO_VER4_S_DRIVER_VGPIO21                110
#define GPIO_VER4_S_DRIVER_VGPIO22                111
#define GPIO_VER4_S_DRIVER_VGPIO23                112
#define GPIO_VER4_S_DRIVER_VGPIO24                113
#define GPIO_VER4_S_DRIVER_VGPIO25                114
#define GPIO_VER4_S_DRIVER_VGPIO30                115
#define GPIO_VER4_S_DRIVER_VGPIO31                116
#define GPIO_VER4_S_DRIVER_VGPIO32                117
#define GPIO_VER4_S_DRIVER_VGPIO33                118
#define GPIO_VER4_S_DRIVER_VGPIO34                119
#define GPIO_VER4_S_DRIVER_VGPIO35                120
#define GPIO_VER4_S_DRIVER_VGPIO36                121
#define GPIO_VER4_S_DRIVER_VGPIO37                122

#define GPIO_VER4_S_DRIVER_VGPIO_0_0              128
#define GPIO_VER4_S_DRIVER_VGPIO_0_1              129
#define GPIO_VER4_S_DRIVER_VGPIO_0_2              130
#define GPIO_VER4_S_DRIVER_VGPIO_0_3              131
#define GPIO_VER4_S_DRIVER_VGPIO_0_4              132
#define GPIO_VER4_S_DRIVER_VGPIO_0_5              132
#define GPIO_VER4_S_DRIVER_VGPIO_0_6              133
#define GPIO_VER4_S_DRIVER_VGPIO_0_7              133

#define GPIO_VER4_S_DRIVER_GPP_B0                 160
#define GPIO_VER4_S_DRIVER_GPP_B1                 161
#define GPIO_VER4_S_DRIVER_GPP_B2                 162
#define GPIO_VER4_S_DRIVER_GPP_B3                 163
#define GPIO_VER4_S_DRIVER_GPP_B4                 164
#define GPIO_VER4_S_DRIVER_GPP_B5                 165
#define GPIO_VER4_S_DRIVER_GPP_B6                 166
#define GPIO_VER4_S_DRIVER_GPP_B7                 167
#define GPIO_VER4_S_DRIVER_GPP_B8                 168
#define GPIO_VER4_S_DRIVER_GPP_B9                 169
#define GPIO_VER4_S_DRIVER_GPP_B10                170
#define GPIO_VER4_S_DRIVER_GPP_B11                171
#define GPIO_VER4_S_DRIVER_GPP_B12                172
#define GPIO_VER4_S_DRIVER_GPP_B13                173
#define GPIO_VER4_S_DRIVER_GPP_B14                174
#define GPIO_VER4_S_DRIVER_GPP_B15                175
#define GPIO_VER4_S_DRIVER_GPP_B16                176
#define GPIO_VER4_S_DRIVER_GPP_B17                177
#define GPIO_VER4_S_DRIVER_GPP_B18                178
#define GPIO_VER4_S_DRIVER_GPP_B19                179
#define GPIO_VER4_S_DRIVER_GPP_B20                180
#define GPIO_VER4_S_DRIVER_GPP_B21                181
#define GPIO_VER4_S_DRIVER_GPP_B22                182
#define GPIO_VER4_S_DRIVER_GPP_B23                183

#define GPIO_VER4_S_DRIVER_GPP_G0                 192
#define GPIO_VER4_S_DRIVER_GPP_G1                 193
#define GPIO_VER4_S_DRIVER_GPP_G2                 194
#define GPIO_VER4_S_DRIVER_GPP_G3                 195
#define GPIO_VER4_S_DRIVER_GPP_G4                 196
#define GPIO_VER4_S_DRIVER_GPP_G5                 197
#define GPIO_VER4_S_DRIVER_GPP_G6                 198
#define GPIO_VER4_S_DRIVER_GPP_G7                 199

#define GPIO_VER4_S_DRIVER_GPP_H0                 224
#define GPIO_VER4_S_DRIVER_GPP_H1                 225
#define GPIO_VER4_S_DRIVER_GPP_H2                 226
#define GPIO_VER4_S_DRIVER_GPP_H3                 227
#define GPIO_VER4_S_DRIVER_GPP_H4                 228
#define GPIO_VER4_S_DRIVER_GPP_H5                 229
#define GPIO_VER4_S_DRIVER_GPP_H6                 230
#define GPIO_VER4_S_DRIVER_GPP_H7                 231
#define GPIO_VER4_S_DRIVER_GPP_H8                 232
#define GPIO_VER4_S_DRIVER_GPP_H9                 233
#define GPIO_VER4_S_DRIVER_GPP_H10                234
#define GPIO_VER4_S_DRIVER_GPP_H11                235
#define GPIO_VER4_S_DRIVER_GPP_H12                236
#define GPIO_VER4_S_DRIVER_GPP_H13                237
#define GPIO_VER4_S_DRIVER_GPP_H14                238
#define GPIO_VER4_S_DRIVER_GPP_H15                239
#define GPIO_VER4_S_DRIVER_GPP_H16                240
#define GPIO_VER4_S_DRIVER_GPP_H17                241
#define GPIO_VER4_S_DRIVER_GPP_H18                242
#define GPIO_VER4_S_DRIVER_GPP_H19                243
#define GPIO_VER4_S_DRIVER_GPP_H20                244
#define GPIO_VER4_S_DRIVER_GPP_H21                245
#define GPIO_VER4_S_DRIVER_GPP_H22                246
#define GPIO_VER4_S_DRIVER_GPP_H23                247

#define GPIO_VER4_S_DRIVER_GPP_A0                 256
#define GPIO_VER4_S_DRIVER_GPP_A1                 257
#define GPIO_VER4_S_DRIVER_GPP_A2                 258
#define GPIO_VER4_S_DRIVER_GPP_A3                 259
#define GPIO_VER4_S_DRIVER_GPP_A4                 260
#define GPIO_VER4_S_DRIVER_GPP_A5                 261
#define GPIO_VER4_S_DRIVER_GPP_A6                 262
#define GPIO_VER4_S_DRIVER_GPP_A7                 263
#define GPIO_VER4_S_DRIVER_GPP_A8                 264
#define GPIO_VER4_S_DRIVER_GPP_A9                 265
#define GPIO_VER4_S_DRIVER_GPP_A10                266
#define GPIO_VER4_S_DRIVER_GPP_A11                267
#define GPIO_VER4_S_DRIVER_GPP_A12                268
#define GPIO_VER4_S_DRIVER_GPP_A13                269
#define GPIO_VER4_S_DRIVER_GPP_A14                270
#define GPIO_VER4_S_DRIVER_ESPI_CLK_LOOPBK        271

#define GPIO_VER4_S_DRIVER_GPP_C0                 288
#define GPIO_VER4_S_DRIVER_GPP_C1                 289
#define GPIO_VER4_S_DRIVER_GPP_C2                 290
#define GPIO_VER4_S_DRIVER_GPP_C3                 291
#define GPIO_VER4_S_DRIVER_GPP_C4                 292
#define GPIO_VER4_S_DRIVER_GPP_C5                 293
#define GPIO_VER4_S_DRIVER_GPP_C6                 294
#define GPIO_VER4_S_DRIVER_GPP_C7                 295
#define GPIO_VER4_S_DRIVER_GPP_C8                 296
#define GPIO_VER4_S_DRIVER_GPP_C9                 297
#define GPIO_VER4_S_DRIVER_GPP_C10                298
#define GPIO_VER4_S_DRIVER_GPP_C11                299
#define GPIO_VER4_S_DRIVER_GPP_C12                300
#define GPIO_VER4_S_DRIVER_GPP_C13                301
#define GPIO_VER4_S_DRIVER_GPP_C14                302
#define GPIO_VER4_S_DRIVER_GPP_C15                303
#define GPIO_VER4_S_DRIVER_GPP_C16                304
#define GPIO_VER4_S_DRIVER_GPP_C17                305
#define GPIO_VER4_S_DRIVER_GPP_C18                306
#define GPIO_VER4_S_DRIVER_GPP_C19                307
#define GPIO_VER4_S_DRIVER_GPP_C20                308
#define GPIO_VER4_S_DRIVER_GPP_C21                309
#define GPIO_VER4_S_DRIVER_GPP_C22                310
#define GPIO_VER4_S_DRIVER_GPP_C23                311

#define GPIO_VER4_S_DRIVER_GPP_S0                 320
#define GPIO_VER4_S_DRIVER_GPP_S1                 321
#define GPIO_VER4_S_DRIVER_GPP_S2                 322
#define GPIO_VER4_S_DRIVER_GPP_S3                 323
#define GPIO_VER4_S_DRIVER_GPP_S4                 324
#define GPIO_VER4_S_DRIVER_GPP_S5                 325
#define GPIO_VER4_S_DRIVER_GPP_S6                 326
#define GPIO_VER4_S_DRIVER_GPP_S7                 327

#define GPIO_VER4_S_DRIVER_GPP_E0                 352
#define GPIO_VER4_S_DRIVER_GPP_E1                 353
#define GPIO_VER4_S_DRIVER_GPP_E2                 354
#define GPIO_VER4_S_DRIVER_GPP_E3                 355
#define GPIO_VER4_S_DRIVER_GPP_E4                 356
#define GPIO_VER4_S_DRIVER_GPP_E5                 357
#define GPIO_VER4_S_DRIVER_GPP_E6                 358
#define GPIO_VER4_S_DRIVER_GPP_E7                 359
#define GPIO_VER4_S_DRIVER_GPP_E8                 360
#define GPIO_VER4_S_DRIVER_GPP_E9                 361
#define GPIO_VER4_S_DRIVER_GPP_E10                362
#define GPIO_VER4_S_DRIVER_GPP_E11                363
#define GPIO_VER4_S_DRIVER_GPP_E12                364
#define GPIO_VER4_S_DRIVER_GPP_E13                365
#define GPIO_VER4_S_DRIVER_GPP_E14                366
#define GPIO_VER4_S_DRIVER_GPP_E15                367
#define GPIO_VER4_S_DRIVER_GPP_E16                368
#define GPIO_VER4_S_DRIVER_GPP_E17                369
#define GPIO_VER4_S_DRIVER_GPP_E18                370
#define GPIO_VER4_S_DRIVER_GPP_E19                371
#define GPIO_VER4_S_DRIVER_GPP_E20                372
#define GPIO_VER4_S_DRIVER_GPP_E21                373
#define GPIO_VER4_S_DRIVER_SPI1_THC0_CLK_LOOPBK   374

#define GPIO_VER4_S_DRIVER_GPP_K0                 384
#define GPIO_VER4_S_DRIVER_GPP_K1                 385
#define GPIO_VER4_S_DRIVER_GPP_K2                 386
#define GPIO_VER4_S_DRIVER_GPP_K3                 387
#define GPIO_VER4_S_DRIVER_GPP_K4                 388
#define GPIO_VER4_S_DRIVER_GPP_K5                 389
#define GPIO_VER4_S_DRIVER_GPP_K6                 390
#define GPIO_VER4_S_DRIVER_GPP_K7                 391
#define GPIO_VER4_S_DRIVER_GPP_K8                 392
#define GPIO_VER4_S_DRIVER_GPP_K9                 393
#define GPIO_VER4_S_DRIVER_GPP_K10                394
#define GPIO_VER4_S_DRIVER_GPP_K11                395

#define GPIO_VER4_S_DRIVER_GPP_F0                 416
#define GPIO_VER4_S_DRIVER_GPP_F1                 417
#define GPIO_VER4_S_DRIVER_GPP_F2                 418
#define GPIO_VER4_S_DRIVER_GPP_F3                 419
#define GPIO_VER4_S_DRIVER_GPP_F4                 420
#define GPIO_VER4_S_DRIVER_GPP_F5                 421
#define GPIO_VER4_S_DRIVER_GPP_F6                 422
#define GPIO_VER4_S_DRIVER_GPP_F7                 423
#define GPIO_VER4_S_DRIVER_GPP_F8                 424
#define GPIO_VER4_S_DRIVER_GPP_F9                 425
#define GPIO_VER4_S_DRIVER_GPP_F10                426
#define GPIO_VER4_S_DRIVER_GPP_F11                427
#define GPIO_VER4_S_DRIVER_GPP_F12                428
#define GPIO_VER4_S_DRIVER_GPP_F13                429
#define GPIO_VER4_S_DRIVER_GPP_F14                430
#define GPIO_VER4_S_DRIVER_GPP_F15                431
#define GPIO_VER4_S_DRIVER_GPP_F16                432
#define GPIO_VER4_S_DRIVER_GPP_F17                433
#define GPIO_VER4_S_DRIVER_GPP_F18                434
#define GPIO_VER4_S_DRIVER_GPP_F19                435
#define GPIO_VER4_S_DRIVER_GPP_F20                436
#define GPIO_VER4_S_DRIVER_GPP_F21                437
#define GPIO_VER4_S_DRIVER_GPP_F22                438
#define GPIO_VER4_S_DRIVER_GPP_F23                439

#define GPIO_VER4_S_DRIVER_GPP_D0                 448
#define GPIO_VER4_S_DRIVER_GPP_D1                 449
#define GPIO_VER4_S_DRIVER_GPP_D2                 450
#define GPIO_VER4_S_DRIVER_GPP_D3                 451
#define GPIO_VER4_S_DRIVER_GPP_D4                 452
#define GPIO_VER4_S_DRIVER_GPP_D5                 453
#define GPIO_VER4_S_DRIVER_GPP_D6                 454
#define GPIO_VER4_S_DRIVER_GPP_D7                 455
#define GPIO_VER4_S_DRIVER_GPP_D8                 456
#define GPIO_VER4_S_DRIVER_GPP_D9                 457
#define GPIO_VER4_S_DRIVER_GPP_D10                458
#define GPIO_VER4_S_DRIVER_GPP_D11                459
#define GPIO_VER4_S_DRIVER_GPP_D12                460
#define GPIO_VER4_S_DRIVER_GPP_D13                461
#define GPIO_VER4_S_DRIVER_GPP_D14                462
#define GPIO_VER4_S_DRIVER_GPP_D15                463
#define GPIO_VER4_S_DRIVER_GPP_D16                464
#define GPIO_VER4_S_DRIVER_GPP_D17                465
#define GPIO_VER4_S_DRIVER_GPP_D18                466
#define GPIO_VER4_S_DRIVER_GPP_D19                467
#define GPIO_VER4_S_DRIVER_GPP_D20                468
#define GPIO_VER4_S_DRIVER_GPP_D21                469
#define GPIO_VER4_S_DRIVER_GPP_D22                470
#define GPIO_VER4_S_DRIVER_GPP_D23                471
#define GPIO_VER4_S_DRIVER_GSPI3_THC1_CLK_LOOPBK  472

#endif // _GPIO_ACPI_DEFINES_ADL_
