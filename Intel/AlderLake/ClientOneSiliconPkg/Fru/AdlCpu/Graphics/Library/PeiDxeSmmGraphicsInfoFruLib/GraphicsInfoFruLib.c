/** @file
  Graphics Info FRu Lib implementation.

@copyright
  INTEL CONFIDENTIAL
  Copyright 2019 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains an 'Intel Peripheral Driver' and is uniquely identified as
  "Intel Reference Module" and is licensed for Intel CPUs and chipsets under
  the terms of your license agreement with Intel or your vendor. This file may
  be modified by the user, subject to additional terms of the license agreement.

@par Specification Reference:
**/
#include <Library/GraphicsInfoFruLib.h>
#include <Register/IgdRegs.h>
#include <Register/CommonMsr.h>
#include <Library/GraphicsInfoLib.h>

/**
  Checks if SKU is DisplayOnly

  @retval FALSE  SKU is not DisplayOnly
  @retval TRUE   SKU is DisplayOnly
**/
BOOLEAN
EFIAPI
IsDisplayOnlySku (
  VOID
  )
{
  UINT16    GtDid;

  GtDid = PciSegmentRead16 (PCI_SEGMENT_LIB_ADDRESS (SA_SEG_NUM, IGD_BUS_NUM, IGD_DEV_NUM, IGD_FUN_NUM, PCI_DEVICE_ID_OFFSET));
  if ((GtDid == V_SA_PCI_DEV_2_GT0_ADL_S_ID) || (GtDid == V_SA_PCI_DEV_2_GT0_ADL_P_ID)
     ) {
    return TRUE;
  }
  return FALSE;
}

/**
  Enables Power Well 2 for platform

  @param[in] GttMmAdr            - Base Address of IGFX MMIO BAR

  @retval EFI_SUCCESS            - Power well 2 Enabled
  @retval EFI_UNSUPPORTED        - Power well 2 programming Failed
  @retval EFI_TIMEOUT            - Timed out
**/
EFI_STATUS
EnablePowerWell2 (
  IN  UINT32     GttMmAdr
  )
{
  EFI_STATUS  Status;
  //
  // Poll Fuse PG1 distribution status
  //
  Status = PollGtReady (GttMmAdr, R_SA_GTTMMADR_FUSE_STATUS_OFFSET, B_SA_GTTMMADR_FUSE_STATUS_PG1_DIST_STATUS, B_SA_GTTMMADR_FUSE_STATUS_PG1_DIST_STATUS);
  if (Status != EFI_SUCCESS) {
    return EFI_UNSUPPORTED;
  }
  //
  // Enable PG2 and Poll for PG2 state
  //
  MmioOr32 (GttMmAdr + R_SA_GTTMMADR_PWR_WELL_CTL_OFFSET, B_SA_GTTMMADR_PWR_WELL_CTL_PG_2_ENABLE);
  Status = PollGtReady (GttMmAdr, R_SA_GTTMMADR_PWR_WELL_CTL_OFFSET, B_SA_GTTMMADR_PWR_WELL_CTL_PG_2_STATE, B_SA_GTTMMADR_PWR_WELL_CTL_PG_2_STATE);

  return Status;
}

/**
  Enables Power Well 3 for platform

  @param[in] GttMmAdr            - Base Address of IGFX MMIO BAR

  @retval EFI_SUCCESS            - Power well 3 Enabled
  @retval EFI_UNSUPPORTED        - Power well 3 programming Failed
  @retval EFI_TIMEOUT            - Timed out
**/
EFI_STATUS
EnablePowerWell3 (
  IN  UINT32     GttMmAdr
  )
{
  EFI_STATUS  Status;
  //
  // Poll Fuse PG2 distribution status
  //
  Status = PollGtReady (GttMmAdr, R_SA_GTTMMADR_FUSE_STATUS_OFFSET, B_SA_GTTMMADR_FUSE_STATUS_PG2_DIST_STATUS, B_SA_GTTMMADR_FUSE_STATUS_PG2_DIST_STATUS);
  if (Status != EFI_SUCCESS) {
    return EFI_UNSUPPORTED;
  }
  //
  // Enable PG3
  //
  MmioOr32 (GttMmAdr + R_SA_GTTMMADR_PWR_WELL_CTL_OFFSET, B_SA_GTTMMADR_PWR_WELL_CTL_PG_3_ENABLE);
  //
  // Poll for PG3 state
  //
  Status = PollGtReady (GttMmAdr, R_SA_GTTMMADR_PWR_WELL_CTL_OFFSET, B_SA_GTTMMADR_PWR_WELL_CTL_PG_3_STATE, B_SA_GTTMMADR_PWR_WELL_CTL_PG_3_STATE);

  return Status;
}

/**
  Force keep GT into RC6 state

  @retval EFI_SUCCESS    - GT RC6 MSR programmed successfully
**/
EFI_STATUS
EFIAPI
ForceGtRc6 (
  VOID
  )
{
  MSR_PFAT_GT_SLEEP_CMDSTS_REGISTER  MsrGtSleepCmdSts;

  ///
  /// Set bit 0 in MSR_PFAT_GT_SLEEP_CMDSTS (118h) to force keep GT RC6.
  ///
  MsrGtSleepCmdSts.Uint64 = AsmReadMsr64 (MSR_PFAT_GT_SLEEP_CMDSTS);
  MsrGtSleepCmdSts.Bits.MailboxInternalPcodeKeepGtRc6 = 1;
  AsmWriteMsr64 (MSR_PFAT_GT_SLEEP_CMDSTS, MsrGtSleepCmdSts.Uint64);

  return EFI_SUCCESS;
}