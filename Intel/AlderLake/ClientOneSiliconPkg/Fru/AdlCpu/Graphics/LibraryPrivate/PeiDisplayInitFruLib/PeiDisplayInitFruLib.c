/** @file
  PEIM to initialize FRU Early Display.

@copyright
  INTEL CONFIDENTIAL
  Copyright 2020 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains an 'Intel Peripheral Driver' and is uniquely identified as
  "Intel Reference Module" and is licensed for Intel CPUs and chipsets under
  the terms of your license agreement with Intel or your vendor. This file may
  be modified by the user, subject to additional terms of the license agreement.

@par Specification Reference:
**/
#include <Library/PeiDisplayInitFruLib.h>
#include <Library/PchInfoLib.h>
#include <Library/GraphicsInfoLib.h>
#include <Library/CpuPlatformLib.h>

/**
  ProgramDisplayWorkaround: Programs Display specific Workarounds

  @param[in]  GtPreMemConfig  - GRAPHICS_PEI_PREMEM_PREMEM_CONFIG to access the GtPreMemConfig related information

  @retval     EFI_SUCCESS     - Display workarounds done
**/
EFI_STATUS
ProgramDisplayWorkaround (
  IN   GRAPHICS_PEI_PREMEM_CONFIG      *GtPreMemConfig
  )
{
  UINT32          GttMmAdr;
  UINT64          McD2BaseAddress;
  UINT32          Data32Or;
  UINT32          Data32And;

  Data32Or = 0;
  Data32And = 0;

  DEBUG ((DEBUG_INFO, "ProgramDisplayWorkaround: Begin \n"));

  McD2BaseAddress    = PCI_SEGMENT_LIB_ADDRESS (SA_SEG_NUM, IGD_BUS_NUM, IGD_DEV_NUM, IGD_FUN_NUM, 0);

  if (PciSegmentRead16 (McD2BaseAddress + PCI_VENDOR_ID_OFFSET) == 0xFFFF) {
    return EFI_SUCCESS;
  }

  GttMmAdr = (PciSegmentRead32 (McD2BaseAddress + R_SA_IGD_GTTMMADR)) & 0xFFFFFFF0;
  if (GttMmAdr == 0) {
    GttMmAdr = GtPreMemConfig->GttMmAdr;
    PciSegmentWrite32 (McD2BaseAddress + R_SA_IGD_GTTMMADR, (UINT32) (GttMmAdr & 0xFFFFFFFF));
    PciSegmentWrite32 (McD2BaseAddress + R_SA_IGD_GTTMMADR + 4, 0);
  }

  if (!IgfxCmdRegEnabled()) {
    ///
    /// Enable Bus Initiator and Memory access on 0:2:0
    ///
    PciSegmentOr16 (McD2BaseAddress + PCI_COMMAND_OFFSET, (BIT2 | BIT1));
  }

  if (IsPchLp ()) {
    //
    // Set Fclk hysteresis idle timer to 15 clocks
    //
    MmioOr32 (GttMmAdr + R_SA_GTTMMADR_OFFSET_101014, 0xF);

    //
    // Set hysteresis counter to at least four clocks
    //
    Data32Or |= 0x4 << N_SA_GTTMMADR_OFFSET_HYST_CNTR_OFFSET;
    Data32And &= (UINT32)~(B_SA_GTTMMADR_OFFSET_HYST_CNTR_MASK);
    MmioAndThenOr32 (GttMmAdr + R_SA_GTTMMADR_OFFSET_101038, Data32And, Data32Or);

    //
    // Defeature Igd IOMMU - Posted Interrupt
    //
    MmioOr32 (GttMmAdr + R_SA_GTTMMADR_IOMMU_DEFEATURE_OFFSET, B_SA_GTTMMADR_IOMMU_DEFEATURE_POSTED_INT);
  }

  ///
  /// Program 0x10105C[22:21] = 2'b01
  ///         0x10105C[20:18] = (MKTME_fused_off) ? 3'b001 : 3'b000
  ///
  Data32And = ~(UINT32) (B_SA_GTTMMADR_OFFSET_10105C_GAW_HAW_CTL_MASK);
  Data32Or = (UINT32) ((V_SA_GTTMMADR_GAW_CTL << N_SA_GTTMMADR_GAW_CTL_OFFSET));
  if (FALSE == IsTmeSupported ()) {
    Data32Or |= (V_SA_GTTMMADR_HAW_CTL << N_SA_GTTMMADR_HAW_CTL_OFFSET);
  }
  MmioAndThenOr32 (GttMmAdr + R_SA_GTTMMADR_OFFSET_10105C, Data32And, Data32Or);

  return EFI_SUCCESS;
}

/**
  Get GTI-Uncore clock gating Info

  @param[in] GttMmAdr            - Base Address of IGFX MMIO BAR

  @retval Return Enabled/Disable value for all GTI-Uncore clock gating
**/
UINT32
GetClockGating (
  VOID
  )
{
  return BIT3;
}

/**
  Get graphics aperture Base address

  @param[in] GtPreMemConfig        - GRAPHICS_PEI_PREMEM_CONFIG to access the GtConfig related information

  @retval Return Graphics aperture BAR
**/
UINT64
GetIgfxApertureBar (
  IN   GRAPHICS_PEI_PREMEM_CONFIG      *GtPreMemConfig
  )
{
  return GtPreMemConfig->GmAdr64;
}