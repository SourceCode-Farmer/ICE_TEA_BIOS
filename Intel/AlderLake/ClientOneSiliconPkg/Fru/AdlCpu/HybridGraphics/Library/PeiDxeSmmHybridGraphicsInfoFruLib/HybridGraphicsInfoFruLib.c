/** @file
  Hybrid Graphics information library.

  All function in this library is available for PEI, DXE, and SMM,
  But do not support UEFI RUNTIME environment call.

@copyright
  INTEL CONFIDENTIAL
  Copyright 2020 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains an 'Intel Peripheral Driver' and is uniquely identified as
  "Intel Reference Module" and is licensed for Intel CPUs and chipsets under
  the terms of your license agreement with Intel or your vendor. This file may
  be modified by the user, subject to additional terms of the license agreement.

@par Specification Reference:
**/
#include <Uefi/UefiBaseType.h>
#include <Library/DebugLib.h>
#include <Library/BaseLib.h>
#include <CpuPcieInfo.h>
#include <Library/HybridGraphicsInfoFruLib.h>

/**
  Get Hybrid Graphics Root Port Device and Function Number by Root Port physical Number

  @param[in]  HgSupport             Take policy value to support HG on PEG port.
  @param[out] RpDev                 Return corresponding root port device number.
  @param[out] RpFun                 Return corresponding root port function number.

  @retval     EFI_SUCCESS           Root port device and function is retrieved
**/
EFI_STATUS
EFIAPI
GetHybridGraphicsRpDevFun (
  IN  UINT8        HgSupport,
  OUT UINT8        *RootPortDev,
  OUT UINT8        *RootPortFun
  )
{
  if (HgSupport == 0) {
    *RootPortDev = SA_PEG0_DEV_NUM;
    *RootPortFun = SA_PEG0_FUN_NUM;
  } else if (HgSupport == 1) {
    *RootPortDev = SA_PEG3_DEV_NUM;
    *RootPortFun = SA_PEG3_FUN_NUM;
  } else if (HgSupport == 2) {
    *RootPortDev = SA_PEG3_DEV_NUM;
    *RootPortFun = SA_PEG2_FUN_NUM;
  }

  return EFI_SUCCESS;
}