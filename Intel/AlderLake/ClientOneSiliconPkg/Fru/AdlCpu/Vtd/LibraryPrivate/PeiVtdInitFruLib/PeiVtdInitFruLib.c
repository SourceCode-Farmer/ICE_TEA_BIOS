/** @file
  PEIM to initialize FRU Vtd.

@copyright
  INTEL CONFIDENTIAL
  Copyright 2020 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains an 'Intel Peripheral Driver' and is uniquely identified as
  "Intel Reference Module" and is licensed for Intel CPUs and chipsets under
  the terms of your license agreement with Intel or your vendor. This file may
  be modified by the user, subject to additional terms of the license agreement.

@par Specification Reference:
**/
#include <Uefi.h>
#include <Ppi/SiPolicy.h>
#include <Library/BaseLib.h>
#include <Library/DebugLib.h>
#include <Library/IoLib.h>
#include <Library/BaseMemoryLib.h>
#include <Library/PciSegmentLib.h>
#include <Register/IgdRegs.h>
#include <Register/VtdRegs.h>

/**
  Override Igd IOMMU defeature register

  @param[in]   VTD_CONFIG       VTD config block from SA Policy PPI

**/
VOID
IgdIommuOverride (
  IN  VTD_CONFIG  *Vtd
  )
{
  UINT32          GttMmAdr;
  UINT64          EcapReg;
  UINT64          McD2BaseAddress;

  DEBUG ((DEBUG_INFO, "IgdIommuOverride start\n"));

  McD2BaseAddress    = PCI_SEGMENT_LIB_ADDRESS (SA_SEG_NUM, IGD_BUS_NUM, IGD_DEV_NUM, IGD_FUN_NUM, 0);
  GttMmAdr = (PciSegmentRead32 (McD2BaseAddress + R_SA_IGD_GTTMMADR)) & 0xFFFFFFF0;

  EcapReg = MmioRead64 (Vtd->BaseAddress[0] + R_VTD_ECAP_OFFSET);
  DEBUG ((DEBUG_INFO, "Before Override: EcapReg=%lx\n", EcapReg));

  //
  //  Defeature Igd IOMMU - ECS and DIS
  //
  MmioOr32 (GttMmAdr + R_SA_GTTMMADR_IOMMU_DEFEATURE_OFFSET, B_SA_GTTMMADR_IOMMU_DEFEATURE_ECS | B_SA_GTTMMADR_IOMMU_DEFEATURE_DIS);

  //
  // While SMTS not support, defeature MTS, NEST, PRS, SRS, EAFS, PASID
  //
  if (!(EcapReg & B_VTD_ECAP_REG_SMTS)) {
    MmioOr32 (GttMmAdr + R_SA_GTTMMADR_IOMMU_DEFEATURE_OFFSET, ( B_SA_GTTMMADR_IOMMU_DEFEATURE_PASID \
                                                               | B_SA_GTTMMADR_IOMMU_DEFEATURE_MTS \
                                                               | B_SA_GTTMMADR_IOMMU_DEFEATURE_NEST \
                                                               | B_SA_GTTMMADR_IOMMU_DEFEATURE_PRS \
                                                               | B_SA_GTTMMADR_IOMMU_DEFEATURE_SRS \
                                                               | B_SA_GTTMMADR_IOMMU_DEFEATURE_EAFS));
  }

  //
  // While PASID not support, defeature ERS
  //
  if (!(EcapReg & B_VTD_ECAP_REG_PASID)) {
    MmioOr32 (GttMmAdr + R_SA_GTTMMADR_IOMMU_DEFEATURE_OFFSET, B_SA_GTTMMADR_IOMMU_DEFEATURE_ERS);
  }

  EcapReg = MmioRead64 (Vtd->BaseAddress[0] + R_VTD_ECAP_OFFSET);
  DEBUG ((DEBUG_INFO, "After Override: EcapReg=%lx\n", EcapReg));
}
