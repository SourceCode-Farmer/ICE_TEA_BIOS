/** @file
  Header file for CpuPcieInfoFruLib.

@copyright
  INTEL CONFIDENTIAL
  Copyright 2018 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains an 'Intel Peripheral Driver' and is uniquely identified as
  "Intel Reference Module" and is licensed for Intel CPUs and chipsets under
  the terms of your license agreement with Intel or your vendor. This file may
  be modified by the user, subject to additional terms of the license agreement.

@par Specification Reference:
**/
#ifndef _CPU_PCIE_INFO_FRU_LIB_H_
#define _CPU_PCIE_INFO_FRU_LIB_H_

#include <CpuPcieInfo.h>

#define CPU_PCIE_MAX_ROOT_PORTS       4
#define CPU_PCIE_MAX_CONTROLLERS      3

#define CPU_PCIE_CLKREQMSGTXVLMDESTID  0xBB
#define CPU_PCIE_CLKREQMSGRXVLMI       0x2B
#define CPU_PCIE_CLKREQMSGTXVLMI1      0x27
#define CPU_PCIE_CLKREQMSGTXVLMI0      0x26
#define CPU_PCIE_GDR_LDVWMIDX          0x40

#define CPU_PCIE_ULT_MAX_ROOT_PORT         3
#define CPU_PCIE_ULX_MAX_ROOT_PORT         1
#define CPU_PCIE_DT_HALO_MAX_ROOT_PORT     3
#define CPU_PCIE_ULT_MAX_CONTROLLER        3
#define CPU_PCIE_ULX_MAX_CONTROLLER        1
#define CPU_PCIE_DT_HALO_MAX_CONTROLLER    2

#define CPU_PCIE_CONTROLLER0               0
#define CPU_PCIE_CONTROLLER1               1
#define CPU_PCIE_CONTROLLER2               2

#define CPU_PCIE_x4_MAX_LANES              4
#define CPU_PCIE_x16_MAX_LANES             16

#define LANE_MULTIPLIER                    0x1000

#include <Library/CpuRegbarAccessLib.h>


typedef struct {
    UINT8  TxVlmDestId;
    UINT8  RxVlmI;
    UINT8  TxVlmI1;
    UINT8  TxVlmI0;
} VWM_MAPPING;


/**
  Get VWM Mapping for the Platform

  @retval Pointer to VWM mapping structure
**/
VWM_MAPPING
*GetVwmMapping (
    IN     UINT32        RpIndex
);

/**
  Get CPU Maximum Pcie Root Port Number

  @retval PcieMaxRootPort         Pch Maximum Pcie Root Port Number
**/
UINT8
GetMaxCpuPciePortNum (
  VOID
  );

/**
  Get CPU Maximum Pcie Controller Number

  @retval Pch Maximum Pcie Controller Number
**/
UINT8
GetMaxCpuPcieControllerNum (
  VOID
  );

/**
  Get CPU Maximum Pcie Clock Number

  @retval Pch Maximum Pcie Clock Number
**/
UINT8
GetMaxCpuPcieClockNum (
  VOID
  );

/**
  Get CPU Pcie Controller Number from Root Port Number

  @retval Sa Pcie Controller Number
**/
UINT8
GetCpuPcieControllerNum (
  IN     UINT32        RpIndex
  );

/**
  Returns the PCIe controller configuration (1x16, 1x8-2x4, 2x8, 1x4)

  @retval PCIe controller configuration
**/
UINT8
GetCpuPcieControllerConfig (
  VOID
  );

/**
  Get max link width.

  @param[in] RpBase    Root Port base address
  @retval Max link width
**/
UINT8
CpuPcieGetMaxLinkWidth (
  UINT64  RpBase
  );

/**
  Read PCD register and return true based on port configuration and disable bits

  @param[in] RpIndex (0-based)

  @return  TRUE if root port is enabled
**/
BOOLEAN
CpuPcieReadPcdReg (
  IN UINT32  RpIndex
  );

/**
  Check if a given Root Port is Multi VC

  @param[in] RpIndex (0-based)

  @return  TRUE if it is Multi VC
**/
BOOLEAN
IsRpMultiVc (
  IN  UINT32                   RpIndex
  );

/**
  Checks if PEG port is present

  @retval TRUE     PEG is presented
  @retval FALSE    PEG is not presented
**/
BOOLEAN
IsPegPresent (
  VOID
  );

/**
  Get CPU Pcie Root Port Device and Function Number by Root Port physical Number

  @param[in]  RpNumber              Root port physical number. (0-based)
  @param[out] RpDev                 Return corresponding root port device number.
  @param[out] RpFun                 Return corresponding root port function number.

  @retval     EFI_SUCCESS           Root port device and function is retrieved
  @retval     EFI_INVALID_PARAMETER RpNumber is invalid
**/
EFI_STATUS
EFIAPI
GetCpuPcieRpDevFun (
  IN  UINTN   RpNumber,
  OUT UINTN   *RpDev,
  OUT UINTN   *RpFun
  );

/**
  Get Root Port Index by CPU Pcie Root Port Device and Function Number

  @param[in]  RpDev                Root Port pci Device number.
  @param[in]  RpFun                Root Port pci Function number.
  @return     Corresponding physical Root Port index (0-based)
**/
UINT32
EFIAPI
GetCpuPcieRpNumber (
  IN  UINTN   RpDev,
  IN  UINTN   RpFun
  );

/**
  Gets pci segment base address of PCIe root port.

  @param  RpIndex    Root Port Index (0 based)
  @return PCIe port  base address.
**/
UINT64
CpuPcieBase (
  IN  UINT32   RpIndex
  );

/**
  Determines whether CPU is supported Pciex16.

  @return TRUE if CPU is supported, FALSE otherwise
**/
BOOLEAN
IsCpuPciex16Supported (
  VOID
  );

/**
  Get CPU Pcie SIP version

  @param  RpIndex    Root Port Index (0 based)

  @retval CPU SIP version
**/
UINT8
GetCpuPcieSipInfo (
  IN     UINT32        RpIndex
  );

/**
  This function returns PID according to PCIe controller index

  @param[in] ControllerIndex     PCIe controller index

  @retval CPU_SB_DEVICE_PID    Returns PID for SBI Access
**/
CPU_SB_DEVICE_PID
GetCpuPcieControllerSbiPid (
  IN  UINT32  ControllerIndex
  );

/**
  This function returns PID according to Root Port Number

  @param[in] RpIndex     Root Port Index (0-based)

  @retval CPU_SB_DEVICE_PID    Returns PID for SBI Access
**/
CPU_SB_DEVICE_PID
GetCpuPcieRpSbiPid (
  IN  UINTN  RpIndex
  );

/**
  Get max CPU PCIE lanes.

  @retval Max lanes
**/
UINT8
CpuPcieGetMaxLanes (
  );

/**
  This function returns FIA PID according to Root Port Number

  @param[in] RpIndex     Root Port Index (0-based)

  @retval CPU_SB_DEVICE_PID    Returns PID for SBI Access
**/
CPU_SB_DEVICE_PID
GetCpuPcieFiaSbiPid (
  IN  UINTN  RpIndex
  );

/**
  This function sets Soc specific powerGating
**/
VOID
SocSpecificPowerGating (
  );

/**
  This function checks whether Skip Link Training WA Required or not.

  @param[in] RpIndex     Root Port Index (0-based)

  @return TRUE if Skip Link Training WA Required, FALSE otherwise
**/
BOOLEAN
IsSkipLinkTrainingWaRequired (
  IN  UINT32  RpIndex
  );

/**
  This function will configure PTM initilization according to SKU.

  @return TRUE if Mobile Sku
**/
BOOLEAN
CpuPciePtmConfiguration (
  VOID
  );

/**
  This function keeps PEG10 enable when PEG11/PEG12 has endpoint connected
  @param[in] RpDisableMask  mask of rootprots that don't need to be considered

  @retval mask of rootprots that don't need to be considered
**/
UINT32
CpuPcieFunction0RemapEnable (
  IN UINT32     RpDisableMask
  );

/**
  Reads an 8-bit PCI configuration register.

  @param  RpIndex     Root Port Index (0-based)
  @param  Address   Address that encodes the PCI Segment, Bus, Device, Function, and Register.

  @return The 8-bit PCI configuration register specified by Address.

**/
UINT8
EFIAPI
PegPciSegmentRead8 (
  IN UINT32                    RpIndex,
  IN UINT64                    Address
  );

/**
  Writes an 8-bit PCI configuration register.

  @param  RpIndex     Root Port Index (0-based)
  @param  Address     Address that encodes the PCI Segment, Bus, Device, Function, and Register.
  @param  Value       The value to write.

  @return The value written to the PCI configuration register.

**/
UINT8
EFIAPI
PegPciSegmentWrite8 (
  IN UINT32                    RpIndex,
  IN UINT64                    Address,
  IN UINT8                     Value
  );

/**
  Reads a 16-bit PCI configuration register.

  @param  RpIndex   Root Port Index (0-based)
  @param  Address   Address that encodes the PCI Segment, Bus, Device, Function, and Register.

  @return The 16-bit PCI configuration register specified by Address.

**/
UINT16
EFIAPI
PegPciSegmentRead16 (
  IN UINT32                    RpIndex,
  IN UINT64                    Address
  );

/**
  Writes a 16-bit PCI configuration register.

  @param  RpIndex     Root Port Index (0-based)
  @param  Address     Address that encodes the PCI Segment, Bus, Device, Function, and Register.
  @param  Value       The value to write.

  @return The parameter of Value.

**/
UINT16
EFIAPI
PegPciSegmentWrite16  (
  IN UINT32                    RpIndex,
  IN UINT64                    Address,
  IN UINT16                    Value
  );

/**
  Reads a 32-bit PCI configuration register.

  @param  RpIndex   Root Port Index (0-based)
  @param  Address   Address that encodes the PCI Segment, Bus, Device, Function, and Register.

  @return The 32-bit PCI configuration register specified by Address.

**/
UINT32
EFIAPI
PegPciSegmentRead32 (
  IN UINT32                    RpIndex,
  IN UINT64                    Address
  );

/**
  Writes a 32-bit PCI configuration register.

  @param  RpIndex     Root Port Index (0-based)
  @param  Address     Address that encodes the PCI Segment, Bus, Device, Function, and Register.
  @param  Value       The value to write.

  @return The parameter of Value.

**/
UINT32
EFIAPI
PegPciSegmentWrite32 (
  IN UINT32                    RpIndex,
  IN UINT64                    Address,
  IN UINT32                    Value
  );

/**
  Performs a bitwise OR of a 16-bit PCI configuration register with a 16-bit value.

  @param  RpIndex   Root Port Index (0-based)
  @param  Address   Address that encodes the PCI Segment, Bus, Device, Function, and Register.
  @param  OrData    The value to OR with the PCI configuration register.

  @return The value written to the PCI configuration register.
**/
UINT16
EFIAPI
PegPciSegmentOr16 (
  IN UINT32                    RpIndex,
  IN UINT64                    Address,
  IN UINT16                    OrData
  );

/**
  Performs a bitwise AND of a 16-bit PCI configuration register with a 16-bit value.

  @param  RpIndex   Root Port Index (0-based)
  @param  Address   Address that encodes the PCI Segment, Bus, Device, Function, and Register.
  @param  AndData   The value to AND with the PCI configuration register.

  @return The value written to the PCI configuration register.

**/
UINT16
EFIAPI
PegPciSegmentAnd16 (
  IN UINT32                    RpIndex,
  IN UINT64                    Address,
  IN UINT16                    AndData
  );

/**
  Performs a bitwise AND of a 8-bit PCI configuration register with a 8-bit value.

  @param  RpIndex   Root Port Index (0-based)
  @param  Address   Address that encodes the PCI Segment, Bus, Device, Function, and Register.
  @param  AndData   The value to AND with the PCI configuration register.

  @return The value written to the PCI configuration register.

**/
UINT8
EFIAPI
PegPciSegmentAnd8 (
  IN UINT32                    RpIndex,
  IN UINT64                    Address,
  IN UINT8                     AndData
  );

/**
  Performs a bitwise OR of a 32-bit PCI configuration register with a 32-bit value.

  @param  RpIndex   Root Port Index (0-based)
  @param  Address   Address that encodes the PCI Segment, Bus, Device, Function, and Register.
  @param  OrData    The value to OR with the PCI configuration register.

  @return The value written to the PCI configuration register.

**/
UINT32
EFIAPI
PegPciSegmentOr32 (
  IN UINT32                    RpIndex,
  IN UINT64                    Address,
  IN UINT32                    OrData
  );

/**
  Performs a bitwise OR of a 8-bit PCI configuration register with a 8-bit value.

  @param  RpIndex   Root Port Index (0-based)
  @param  Address   Address that encodes the PCI Segment, Bus, Device, Function, and Register.
  @param  OrData    The value to OR with the PCI configuration register.

  @return The value written to the PCI configuration register.

**/
UINT8
EFIAPI
PegPciSegmentOr8 (
  IN UINT32                    RpIndex,
  IN UINT64                    Address,
  IN UINT8                     OrData
  );

/**
  Performs a bitwise AND of a 32-bit PCI configuration register with a 32-bit value.

  @param  RpIndex   Root Port Index (0-based)
  @param  Address   Address that encodes the PCI Segment, Bus, Device, Function, and Register.
  @param  AndData   The value to AND with the PCI configuration register.

  @return The value written to the PCI configuration register.

**/
UINT32
EFIAPI
PegPciSegmentAnd32 (
  IN UINT32                    RpIndex,
  IN UINT64                    Address,
  IN UINT32                    AndData
  );

/**
  Performs a bitwise AND of a 32-bit PCI configuration register with a 32-bit value,
  followed a  bitwise OR with another 32-bit value.

  @param  RpIndex   Root Port Index (0-based)
  @param  Address   Address that encodes the PCI Segment, Bus, Device, Function, and Register.
  @param  AndData   The value to AND with the PCI configuration register.
  @param  OrData    The value to OR with the PCI configuration register.

  @return The value written to the PCI configuration register.

**/
UINT32
EFIAPI
PegPciSegmentAndThenOr32 (
  IN UINT32                    RpIndex,
  IN UINT64                    Address,
  IN UINT32                    AndData,
  IN UINT32                    OrData
  );

/**
  Performs a bitwise AND of a 16-bit PCI configuration register with a 16-bit value,
  followed a  bitwise OR with another 16-bit value.

  @param  RpIndex   Root Port Index (0-based)
  @param  Address   Address that encodes the PCI Segment, Bus, Device, Function, and Register.
  @param  AndData   The value to AND with the PCI configuration register.
  @param  OrData    The value to OR with the PCI configuration register.

  @return The value written to the PCI configuration register.

**/
UINT16
EFIAPI
PegPciSegmentAndThenOr16 (
  IN UINT32                    RpIndex,
  IN UINT64                    Address,
  IN UINT16                    AndData,
  IN UINT16                    OrData
  );

/**
  Performs a bitwise AND of a 8-bit PCI configuration register with a 8-bit value,
  followed a  bitwise OR with another 8-bit value.

  @param  RpIndex   Root Port Index (0-based)
  @param  Address   Address that encodes the PCI Segment, Bus, Device, Function, and Register.
  @param  AndData   The value to AND with the PCI configuration register.
  @param  OrData    The value to OR with the PCI configuration register.

  @return The value written to the PCI configuration register.

**/
UINT8
EFIAPI
PegPciSegmentAndThenOr8 (
  IN UINT32                    RpIndex,
  IN UINT64                    Address,
  IN UINT8                     AndData,
  IN UINT8                     OrData
  );

/**
  This function checks whether HotPlug is supported or not.

  @param[in] RpIndex     Root Port Index (0-based)

  @return TRUE for Mobile Sku
**/
BOOLEAN
IsHotPlugSupported (
  IN  UINT32  RpIndex
  );

/**
  Get the virtual wire CLKREQ message index for CPU PCIe root port.
  This step is neccessary since there is no explicit hardware indexing
  for PEG root ports. For instance PEG60 which is treated by BIOS as
  CPU root port 1 is advertising itself as CPU root port 3 on virtual wire
  message.

  @note: This function can be in PEG library to indicate the mapping.

  @param[in] RpIndex  CPU PCIe root port index

  @retval Root port index given root port uses in virtual wire CLKREQ message
**/
UINT32
GetCpuRpVirtualWireClkReqIndex (
  IN UINT32  RpIndex
  );

/**
  @param[in] RpIndex  CPU PCIe root port index

  @return  TRUE for Gen5 Capable Controller
**/
BOOLEAN
IsIrrcProgrammingRequired (
  IN UINT32  RpIndex
  );

/**
  Get CPU Pcie Root Port Disable Bit Position

  @param[in]  RpIndex              Root Port Index
  @param[out] Data                 Return corresponding root port disable bit.
  @param[out] PortId               Return corresponding root port PortId.

  @retval     EFI_SUCCESS           Root port device and function is retrieved
**/

EFI_STATUS
EFIAPI
GetCpuPcieRpPcdPortDisable (
  IN  UINT32        RpIndex,
  OUT UINT32        *Data,
  OUT UINT8         *PortId
  );

/**
  This function checks if HsPHY 1 configuration required or not

  @return  TRUE if WA required
**/
BOOLEAN
CheckHsphyWaRequired (
  );

/**
  This function returns register capability version according to SKU.

  @return    UINT32    Capability version.
**/
UINT32
GetCapabilityVersion (
  );

/**
  This function helps set mask for Func0 PCIe Port to disable

  @param[in] RpDisableMask  mask of rootprots that don't need to be considered

  @retval mask of rootprots that don't need to be considered
**/
UINT32
CpuPcieFunc0LinkDisable (
  IN UINT32     RpDisableMask
  );

/**
This function checks whether Mild DCG is supported or not.

@return TRUE for Mobile Sku
**/
BOOLEAN
IsMildDcgSupported(
  VOID
  );
#endif // _CPU_PCIE_INFO_FRU_LIB_H_
