/** @file
  Tcss Initialization Fru Library for TcssInitLib.

@copyright
  INTEL CONFIDENTIAL
  Copyright 2019 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains an 'Intel Peripheral Driver' and is uniquely identified as
  "Intel Reference Module" and is licensed for Intel CPUs and chipsets under
  the terms of your license agreement with Intel or your vendor. This file may
  be modified by the user, subject to additional terms of the license agreement.

@par Specification
**/

#include <Library/DebugLib.h>
#include <Library/BaseLib.h>
#include <Library/CpuPlatformLib.h>
#include <TcssDataHob.h>
#include <CpuSbInfo.h>
#include <Library/CpuSbiAccessLib.h>

STATIC UINT8 mPciePortIdMap[4] = { CPU_SB_PID_PCIE0, CPU_SB_PID_PCIE1, CPU_SB_PID_PCIE2, CPU_SB_PID_PCIE3 };

/**
  This function execute override IomTypeC Configure 1 register setting.

  @param[in]       IomTypecConfigValue    IOM_TYPEC_SW_CONFIGURATION_1 register setting

  @retval          UIBT32                 IOM_TYPEC_SW_CONFIGURATION_1 override setting
**/
UINT32
TcssIomConfig1RegOverride (
  IN  UINT32                    IomTypecConfigValue
  )
{
  return IomTypecConfigValue;
}

/**
  This function execute override PMC replay setting.

  @param[in]       PmcReplay    PmcReplay setting

  @retval          UINT8        PmcReplay override setting
**/
UINT8
TcssPmcReplayOverride (
  IN  UINT8                    PmcReplay
  )
{
  return PmcReplay;
}

/**
  Check if this chipset supports TCSS

  @retval BOOLEAN  TRUE if supported, FALSE otherwise
**/
BOOLEAN
EFIAPI
IsTcssSupported (
  VOID
  )
{
  CPU_SKU CpuSku = GetCpuSku ();

  return (CpuSku != EnumCpuTrad);
}

/**
  Read Sideband register in Iom

  @param[in] register address

  @retval register value
**/
UINT32
TcssSbIomRead (
  IN UINT32 RegAddress
  )
{
  return CpuRegbarRead32 (CPU_SB_PID_IOM, RegAddress);
}

/**
  Write Sideband register in Iom

  @param[in] register address
  @param[in] register value

  @retval register value
**/
UINT32
TcssSbIomWrite (
  IN UINT32 RegAddress,
  IN UINT32 Value
  )
{
  return CpuRegbarWrite32 (CPU_SB_PID_IOM, RegAddress, Value);
}

/**
  Write bitwise OR value into register in Iom

  @param[in] register address
  @param[in] register value

  @retval register value
**/
UINT32
TcssSbIomOr (
  IN UINT32 RegAddress,
  IN UINT32 Value
  )
{
  return CpuRegbarOr32 (CPU_SB_PID_IOM, RegAddress, Value);
}

/**
  Performs a bitwise Read of a 8-bits data.
  Read Sideband register in PCIe

  @param[in]  PciePortIndex   Port number
  @param[in]  RegAddr         Address

  @return The 8-bit register value specified by Offset
**/
UINT8
TcssSbPcieRead8 (
  IN UINT8  PciePortIndex,
  IN UINT32 RegAddr
  )
{
  return CpuRegbarRead8 ((CPU_SB_DEVICE_PID) mPciePortIdMap[PciePortIndex], (UINT16) RegAddr);
}

/**
  Performs a bitwise OR of a 8-bits data.
  Write Sideband register in PCIe

  @param[in]  PciePortIndex    Port number
  @param[in]  RegAddr          Address
  @param[in]  OrData           OR Data. Must be the same size as Size parameter.

  @return The 8-bit register value written to register
**/
UINT8
TcssSbPcieOr8 (
  IN UINT8  PciePortIndex,
  IN UINT32 RegAddr,
  IN UINT8  OrData
  )
{
  return CpuRegbarOr8 ((CPU_SB_DEVICE_PID) mPciePortIdMap[PciePortIndex], (UINT16) RegAddr, OrData);
}

/**
  Performs a bitwise AND then OR of a 8-bits data.
  Write Sideband register in PCIe

  @param[in]  PciePortIndex    Port number
  @param[in]  RegAddr          Address
  @param[in]  AndData          AND Data. Must be the same size as Size parameter.
  @param[in]  OrData           OR Data. Must be the same size as Size parameter.

  @return The 8-bit register value written to register
**/
UINT8
TcssSbPcieAndThenOr8 (
  IN UINT8  PciePortIndex,
  IN UINT32 RegAddr,
  IN UINT8  AndData,
  IN UINT8  OrData
  )
{
  return CpuRegbarAndThenOr8 ((CPU_SB_DEVICE_PID) mPciePortIdMap[PciePortIndex], (UINT16) RegAddr, AndData, OrData);
}

/**
  Performs a bitwise Read of a 32-bits data.
  Read Sideband register in PCIe

  @param[in]  PciePortIndex    Port number
  @param[in]  RegAddr          Address

  @return The 32-bit register value specified by Offset
**/
UINT32
TcssSbPcieRead32 (
  IN UINT8  PciePortIndex,
  IN UINT32 RegAddr
  )
{
  return CpuRegbarRead32 ((CPU_SB_DEVICE_PID) mPciePortIdMap[PciePortIndex], RegAddr);
}

/**
  Performs a bitwise Write of a 32-bits data.
  Write Sideband register in PCIe

  @param[in]  PciePortIndex    Port number
  @param[in]  RegAddr          Address
  @param[in]  Value            Value

  @return The 32-bits register value written to register
**/
UINT32
TcssSbPcieWrite32 (
  IN UINT8  PciePortIndex,
  IN UINT32 RegAddr,
  IN UINT32 Value
  )
{
  return CpuRegbarWrite32 ((CPU_SB_DEVICE_PID) mPciePortIdMap[PciePortIndex], RegAddr, Value);
}

/**
  Performs a bitwise OR of a 32-bits data.
  Write Sideband register in PCIe

  @param[in]  PciePortIndex    Port number
  @param[in]  RegAddr          Address
  @param[in]  OrData           OR Data. Must be the same size as Size parameter.

  @return The 32-bit register value written to register
**/
UINT32
TcssSbPcieOr32 (
  IN UINT8  PciePortIndex,
  IN UINT32 RegAddr,
  IN UINT32 OrData
  )
{
  return CpuRegbarOr32 ((CPU_SB_DEVICE_PID) mPciePortIdMap[PciePortIndex], RegAddr, OrData);
}

/**
  Performs a bitwise AND then OR of a 32-bits data.
  Write Sideband register in PCIe

  @param[in]  PciePortIndex    Port number
  @param[in]  RegAddr          Address
  @param[in]  AndData          AND Data. Must be the same size as Size parameter.
  @param[in]  OrData           OR Data. Must be the same size as Size parameter.

  @return The 32-bit register value written to register
**/
UINT32
TcssSbPcieAndThenOr32 (
  IN UINT8  PciePortIndex,
  IN UINT32 RegAddr,
  IN UINT32 AndData,
  IN UINT32 OrData
  )
{
  return CpuRegbarAndThenOr32 ((CPU_SB_DEVICE_PID) mPciePortIdMap[PciePortIndex], RegAddr, AndData, OrData);
}
