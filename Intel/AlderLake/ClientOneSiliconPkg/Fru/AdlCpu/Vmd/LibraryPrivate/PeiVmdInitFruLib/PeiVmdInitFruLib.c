/** @file
  Fru file for VMD feature initialization.

@copyright
  INTEL CONFIDENTIAL
  Copyright 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains an 'Intel Peripheral Driver' and is uniquely identified as
  "Intel Reference Module" and is licensed for Intel CPUs and chipsets under
  the terms of your license agreement with Intel or your vendor. This file may
  be modified by the user, subject to additional terms of the license agreement.

@par Specification Reference:
**/

#include <Library/PeiVmdInitFruLib.h>
#include <PiPei.h>
#include <Library/PeiServicesLib.h>
#include <Library/PeiServicesTablePointerLib.h>
#include <Library/BaseMemoryLib.h>
#include <Library/DebugLib.h>
#include <Library/HobLib.h>
#include <Library/PciSegmentLib.h>
#include <Uefi/UefiBaseType.h>
#include <PcieRegs.h>
#include <Library/PchPcieRpLib.h>
#include <Library/ConfigBlockLib.h>
#include <Library/PostCodeLib.h>
#include <Library/PeiVmdInitLib.h>
#include <Register/SaRegsHostBridge.h>
#include <Register/CpuPcieRegs.h>
#include <Library/CpuPcieInfoFruLib.h>
#include <Library/PchInfoLib.h>
#include <Register/PchRegs.h>
#include <VmdInfoHob.h>
#include <Register/SataRegs.h>
#include <Register/VmdRegs.h>
#include <Library/PchPciBdfLib.h>
#include <Library/SataSocLib.h>
#include <CpuPcieInfo.h>
#include <CpuSbInfo.h>
#include <Library/VmdInfoLib.h>
#include <Library/CpuPlatformLib.h>

/**
  This function enumerate all downstream bridge.

  @param[in] BusNum  - Primary bus number of current bridge.

  @retval BusNum: return current bus number if current bus is an endpoint device.
  @retval SubBus: return subordinate bus number if current bus is a bridge.
**/
UINT8
VmdEnumerateDownstream (
  IN UINT8  BusNum
  )
{
  UINT64  DeviceBaseAddress;
  UINT8   DevNum;
  UINT16  Buffer16;
  UINT8   SubBus;
  UINT8   SecBus;

  SubBus  = 0;

  SecBus  = BusNum;

  for (DevNum = 0; DevNum < 32; DevNum++) {
    ///
    /// Read Vendor ID to check if device exists
    /// if no device exists, then check next device
    ///
    DeviceBaseAddress = PCI_SEGMENT_LIB_ADDRESS (SA_SEG_NUM, BusNum, DevNum, 0, 0);
    if (PciSegmentRead16 (DeviceBaseAddress + PCI_VENDOR_ID_OFFSET) == 0xFFFF) {
      continue;
    }

    Buffer16 = PciSegmentRead16 (DeviceBaseAddress + R_PCI_SCC_OFFSET);
    ///
    /// Check for PCI/PCI Bridge Device Base Class 6 with subclass 4
    ///
    if (Buffer16 == 0x0604) {
      SecBus++;
      PciSegmentWrite8 (DeviceBaseAddress + PCI_BRIDGE_PRIMARY_BUS_REGISTER_OFFSET, BusNum);
      PciSegmentWrite8 (DeviceBaseAddress + PCI_BRIDGE_SECONDARY_BUS_REGISTER_OFFSET, SecBus);
      ///
      /// Assign temporary subordinate bus number so that device behind this bridge can be seen
      ///
      PciSegmentWrite8 (DeviceBaseAddress + PCI_BRIDGE_SUBORDINATE_BUS_REGISTER_OFFSET, 0xFF);

      ///
      /// A config write is required in order for the device to re-capture the Bus number,
      /// according to PCI Express Base Specification, 2.2.6.2
      /// Write to a read-only register VendorID to not cause any side effects.
      ///
      PciSegmentWrite16 (PCI_SEGMENT_LIB_ADDRESS (SA_SEG_NUM, SecBus, 0, 0, PCI_VENDOR_ID_OFFSET), 0);

      ///
      /// Enumerate bus behind this bridge by calling this function recursively
      ///
      SubBus = VmdEnumerateDownstream (SecBus);
      ///
      /// Update the correct subordinate bus number
      ///
      PciSegmentWrite8 (DeviceBaseAddress + PCI_BRIDGE_SUBORDINATE_BUS_REGISTER_OFFSET, SubBus);
      SecBus = SubBus;
    }
  }

  if (SubBus == 0) {
    return BusNum;
  } else {
    return SubBus;
  }
}


/**
This function detects the mass storage devices attached to PEG Slots and PCH PCIe Root ports and the Sata controller.

@param[in]      *VmdInfoHob     pointer to VMD info Hob to store the information of detected devices.

@retval EFI_SUCCESS            Successfully initialized Pre-Mem configurations.
**/

EFI_STATUS
VmdDetectPcieStorageDevices (
    VMD_INFO_HOB               *VmdInfoHob
)
{
  UINTN           RpBus;
  UINTN           RpDev;
  UINTN           RpFunc;
  UINTN           RpIndex;
  UINT64          RpDeviceBaseAddress;
  UINT64          EpDeviceBaseAddress;
  UINT32          PcieBusNum;
  UINT8           Bus;
  UINT8           Dev;
  UINT8           Func;
  UINT8           MaxFunction;
  UINT8           NumOfDevices;
  UINT8           HeaderType;
  UINT32          DevId;
  UINT8           SubBusNum;
  UINT16          Buffer16;
  UINT8           MaxCpuPciePortNum;
  UINT8           PegDevTableCount;
  UINT32          PegDevenReg;
  UINTN           PegPortIndex;
  UINT32          PegEnable;
  UINT64          PegBaseAddress;
  PEG_DEV         PegDevTable[3];
  CPU_GENERATION  CpuGeneration;
  CPU_SKU         CpuSku;
  UINTN           NonStorageRpBus;
  UINTN           NonStorageRpDev;
  UINTN           NonStorageRpFunc;


  NumOfDevices = 0;
  PegDevenReg  = 0;
  RpBus        = 0;
  NonStorageRpBus  = 0;
  NonStorageRpDev  = 0;
  NonStorageRpFunc = 0;

  DEBUG ((DEBUG_INFO, "VMD: VmdDetectPcieStorageDevices Start\n"));

  DEBUG ((DEBUG_INFO, "VMD: Checking device connection on PEG ports \n"));

  PegDevTableCount = (sizeof(PegDevTable)) / (sizeof(PEG_DEV));
  MaxCpuPciePortNum  = GetMaxCpuPciePortNum();

  if (MaxCpuPciePortNum < PegDevTableCount) {
    PegDevTableCount = MaxCpuPciePortNum;
  }

  ///
  ///  Bus, Device, Function, DevenMask
  ///
  PegDevTable[0].Bus       = SA_PEG_BUS_NUM;
  PegDevTable[0].Device    = SA_PEG3_DEV_NUM;
  PegDevTable[0].Function  = SA_PEG3_FUN_NUM;
  PegDevTable[0].DevenMask = B_SA_DEVEN_D6EN_MASK;

  PegDevTable[1].Bus       = SA_PEG_BUS_NUM;
  PegDevTable[1].Device    = SA_PEG0_DEV_NUM;
  PegDevTable[1].Function  = SA_PEG0_FUN_NUM;
  PegDevTable[1].DevenMask = B_SA_DEVEN_D1F0EN_MASK;

  CpuGeneration = GetCpuGeneration ();
  CpuSku        = GetCpuSku ();
  if (CpuGeneration == EnumAdlCpu
      ) {
    if (CpuSku == EnumCpuTrad) {
      PegDevTable[2].Bus       = SA_PEG_BUS_NUM;
      PegDevTable[2].Device    = SA_PEG1_DEV_NUM;
      PegDevTable[2].Function  = SA_PEG1_FUN_NUM;
      PegDevTable[2].DevenMask = B_SA_DEVEN_D1F1EN_MASK;
    } else if ((CpuSku == EnumCpuUlt) || (CpuSku == EnumCpuUlx)) {
      PegDevTable[2].Bus       = SA_PEG_BUS_NUM;
      PegDevTable[2].Device    = SA_PEG3_DEV_NUM;
      PegDevTable[2].Function  = SA_PEG2_FUN_NUM;
      PegDevTable[2].DevenMask = B_SA_DEVEN_D6F2EN_MASK;
    }
  }

  ///
  /// Read the DEVEN register for PEG 0/1/2 controllers configuration
  ///
  DEBUG((DEBUG_INFO, "Read DEVEN register\n"));
  if (CpuGeneration == EnumAdlCpu
      ) {
    if (CpuSku == EnumCpuTrad) {
      PegDevenReg = PciSegmentRead32 (PCI_SEGMENT_LIB_ADDRESS (SA_SEG_NUM, SA_MC_BUS, SA_MC_DEV, SA_MC_FUN, R_SA_DEVEN)) & (B_SA_DEVEN_D6EN_MASK | B_SA_DEVEN_D1F0EN_MASK | B_SA_DEVEN_D1F1EN_MASK);
    } else if ((CpuSku == EnumCpuUlt) || (CpuSku == EnumCpuUlx)) {
      PegDevenReg = PciSegmentRead32 (PCI_SEGMENT_LIB_ADDRESS (SA_SEG_NUM, SA_MC_BUS, SA_MC_DEV, SA_MC_FUN, R_SA_DEVEN)) & (B_SA_DEVEN_D6EN_MASK | B_SA_DEVEN_D1F0EN_MASK | B_SA_DEVEN_D6F2EN_MASK);
    }
  } else {
    PegDevenReg = PciSegmentRead32 (PCI_SEGMENT_LIB_ADDRESS (SA_SEG_NUM, SA_MC_BUS, SA_MC_DEV, SA_MC_FUN, R_SA_DEVEN)) & (B_SA_DEVEN_D6EN_MASK | B_SA_DEVEN_D1F0EN_MASK | B_SA_DEVEN_D1F1EN_MASK | B_SA_DEVEN_D1F2EN_MASK);
  }
  DEBUG ((DEBUG_INFO, "PegDevenReg = %x\n", PegDevenReg));

  ///
  /// Scan PEG device vs DEVEN register for PEG controllers configuration
  ///
  for (PegPortIndex = 0; PegPortIndex < PegDevTableCount; PegPortIndex++) {
    ///
    /// Initialize Secondary and Subordinate bus number for first Pcie root port
    ///
    PcieBusNum  = 0x00010100;

    RpBus     = PegDevTable[PegPortIndex].Bus;
    RpDev     = PegDevTable[PegPortIndex].Device;
    RpFunc    = PegDevTable[PegPortIndex].Function;
    PegEnable = PegDevTable[PegPortIndex].DevenMask;

    PegBaseAddress = PCI_SEGMENT_LIB_ADDRESS (SA_SEG_NUM, RpBus, RpDev, RpFunc, 0);

    if ((PegDevenReg & PegEnable) == 0) {
      DEBUG ((DEBUG_INFO, "VMD: PEG Port %d/%d/%d is not enabled\n", RpBus, RpDev, RpFunc));
      continue;
    }
    ///
    /// Check for a endpoint presence in the PEG slot.
    ///
    if (PciSegmentRead16 (PegBaseAddress + PCI_VENDOR_ID_OFFSET) == 0xFFFF) {
      DEBUG ((DEBUG_INFO, "VMD: PEG Port %d/%d/%d is not enabled\n", RpBus, RpDev, RpFunc));
      continue;
    }

    PciSegmentWrite32 (PegBaseAddress + PCI_BRIDGE_PRIMARY_BUS_REGISTER_OFFSET, PcieBusNum);
    Bus = PciSegmentRead8 (PegBaseAddress + PCI_BRIDGE_SECONDARY_BUS_REGISTER_OFFSET);

    ///
    /// Assign temporary subordinate bus number so that device this bridge can be seen
    ///
    PciSegmentWrite8 (PegBaseAddress + PCI_BRIDGE_SUBORDINATE_BUS_REGISTER_OFFSET, 0xFF);

    SubBusNum                       = VmdEnumerateDownstream (Bus);
    ///
    /// Update the actual subordinate bus number
    ///
    PciSegmentWrite8 (PegBaseAddress + PCI_BRIDGE_SUBORDINATE_BUS_REGISTER_OFFSET, SubBusNum);

    for (Bus = 1; Bus <= SubBusNum; Bus++) {
      for (Dev = 0; Dev < 32; Dev++) {
        ///
        /// Read Vendor ID to check if device exists
        /// if no device exists, then check next device
        ///
        EpDeviceBaseAddress = PCI_SEGMENT_LIB_ADDRESS (SA_SEG_NUM, Bus, Dev, 0, 0);
        if (PciSegmentRead16 (EpDeviceBaseAddress + PCI_VENDOR_ID_OFFSET) == 0xFFFF) {
          continue;
        }
        ///
        /// Check for a multifunction device
        ///
        HeaderType = PciSegmentRead8 (EpDeviceBaseAddress + PCI_HEADER_TYPE_OFFSET);
        if ((HeaderType & HEADER_TYPE_MULTI_FUNCTION) != 0) {
          MaxFunction = 7;
        } else {
          MaxFunction = 0;
        }

        for (Func = 0; Func <= MaxFunction; Func++) {
          EpDeviceBaseAddress = PCI_SEGMENT_LIB_ADDRESS (SA_SEG_NUM, Bus, Dev, Func, 0);
          if (PciSegmentRead16 (EpDeviceBaseAddress + PCI_VENDOR_ID_OFFSET) == 0xFFFF) {
            continue;
          }
          // Check if the device is mass storage device
          if (PciSegmentRead16 (EpDeviceBaseAddress + R_PCI_SCC_OFFSET) == 0x0108) {
            DevId = (PciSegmentRead16 (EpDeviceBaseAddress + PCI_DEVICE_ID_OFFSET));
            if (NumOfDevices && (VmdInfoHob->VmdPortInfo.PortInfo[NumOfDevices-1].RpDev == RpDev) &&
                (VmdInfoHob->VmdPortInfo.PortInfo[NumOfDevices-1].RpFunc == RpFunc)) {
              DEBUG ((DEBUG_INFO, "VMD: Another storage device found under root port B/D/F  %d/%d/%d \n", RpBus, RpDev, RpFunc));
              DEBUG ((DEBUG_INFO, "EpDevId 0x%x \n", DevId));
              continue;
            }
            if ((NonStorageRpBus == RpBus) && (NonStorageRpDev == RpDev) && (NonStorageRpFunc == RpFunc)) {
              DEBUG ((DEBUG_INFO, "VMD: Another Non storage device was found under the same root port B/D/F  %d/%d/%d \n", RpBus, RpDev, RpFunc));
              DEBUG ((DEBUG_INFO, "Hence skipping this RP entry from possible VMD mapped root ports\n"));
              continue;
            }

            VmdInfoHob->VmdPortInfo.PortInfo[NumOfDevices].DeviceDetected = 1;
            VmdInfoHob->VmdPortInfo.PortInfo[NumOfDevices].DevId = DevId;
            VmdInfoHob->VmdPortInfo.PortInfo[NumOfDevices].RpDev  = (UINT8) RpDev;
            VmdInfoHob->VmdPortInfo.PortInfo[NumOfDevices].RpFunc = (UINT8) RpFunc;
            VmdInfoHob->VmdPortInfo.PortInfo[NumOfDevices].RpIndex = (UINT8)((PciSegmentRead32 (PegBaseAddress + R_PCIE_LCAP) & 0xFF000000) >> 24);
            ++NumOfDevices;

            DEBUG ((DEBUG_INFO, "VMD: Storage device found \t" ));
            DEBUG ((DEBUG_INFO, "on B/D/F  %d/%d/%d \t", Bus, Dev, Func));
            DEBUG ((DEBUG_INFO, "under RP B/D/F  %d/%d/%d \t", RpBus, RpDev, RpFunc));
            DEBUG ((DEBUG_INFO, "EpDevId 0x%x \t Rp Index %d\n", DevId, VmdInfoHob->VmdPortInfo.PortInfo[NumOfDevices-1].RpIndex));
          } else {
            DEBUG ((DEBUG_INFO, "VMD: Non Storage device found \t" ));
            DEBUG ((DEBUG_INFO, "on B/D/F  %d/%d/%d \t", Bus, Dev, Func));
            DEBUG ((DEBUG_INFO, "under RP B/D/F  %d/%d/%d \t", RpBus, RpDev, RpFunc));
            DEBUG ((DEBUG_INFO, "EpDevId 0x%x\n", DevId));
            NonStorageRpBus  = RpBus;
            NonStorageRpDev  = RpDev;
            NonStorageRpFunc = RpFunc;
            if (NumOfDevices && (VmdInfoHob->VmdPortInfo.PortInfo[NumOfDevices-1].RpBus == RpBus) &&
                (VmdInfoHob->VmdPortInfo.PortInfo[NumOfDevices-1].RpDev == RpDev) &&
                (VmdInfoHob->VmdPortInfo.PortInfo[NumOfDevices-1].RpFunc == RpFunc)) {
              DEBUG ((DEBUG_INFO, "VMD: Another Non storage device found under the same root port B/D/F  %d/%d/%d \n", RpBus, RpDev, RpFunc));
              DEBUG ((DEBUG_INFO, "Hence clearing/skipping this RP entry from possible VMD mapped root ports\n"));
              --NumOfDevices;
              VmdInfoHob->VmdPortInfo.PortInfo[NumOfDevices].DeviceDetected = 0;
              VmdInfoHob->VmdPortInfo.PortInfo[NumOfDevices].DevId = 0;
              VmdInfoHob->VmdPortInfo.PortInfo[NumOfDevices].RpDev  = 0;
              VmdInfoHob->VmdPortInfo.PortInfo[NumOfDevices].RpFunc = 0;
              VmdInfoHob->VmdPortInfo.PortInfo[NumOfDevices].RpIndex = 0;
              DEBUG ((DEBUG_INFO, "Cleared last entry, Updated NumOfDevices is %d \n", NumOfDevices));
            }
          }
        }
      }
    }
    ///
    /// Clear bus number on all the bridges that we have opened so far.
    /// We have to do it in the reverse Bus number order.
    ///
    for (Bus = SubBusNum; Bus >= 1; Bus--) {
      for (Dev = 0; Dev < 32; Dev++) {
        ///
        /// Read Vendor ID to check if device exists
        /// if no device exists, then check next device
        ///
        EpDeviceBaseAddress = PCI_SEGMENT_LIB_ADDRESS (SA_SEG_NUM, Bus, Dev, 0, 0);
        if (PciSegmentRead16 (EpDeviceBaseAddress + PCI_VENDOR_ID_OFFSET) == 0xFFFF) {
          continue;
        }

        Buffer16 = PciSegmentRead16 (EpDeviceBaseAddress + R_PCI_SCC_OFFSET);
        ///
        /// Clear Bus Number for PCI/PCI Bridge Device
        ///
        if (Buffer16 == 0x0604) {
          PciSegmentWrite32 (EpDeviceBaseAddress + PCI_BRIDGE_PRIMARY_BUS_REGISTER_OFFSET, 0);
        }
      }
    }
      ///
      /// Clear bus numbers so that PCIe slots are hidden
      ///
      PciSegmentWrite32 (PegBaseAddress + PCI_BRIDGE_PRIMARY_BUS_REGISTER_OFFSET, 0);
      //reset the nus numbers
  } // End of PEG

  DEBUG ((DEBUG_INFO, "VMD: Checking device on PCH PCIe ports \n" ));
  for (RpIndex = 0; RpIndex < GetPchMaxPciePortNum (); RpIndex++) {
    PcieBusNum  = 0x00010100;
    ///
    /// Check if root port exists
    ///
    RpDev = PchPcieRpDevNumber (RpIndex);
    RpFunc = PchPcieRpFuncNumber (RpIndex);
    RpDeviceBaseAddress = PchPcieRpPciCfgBase (RpIndex);
    if (PciSegmentRead16 (RpDeviceBaseAddress + PCI_VENDOR_ID_OFFSET) == 0xFFFF) {
      continue;
    }

    DEBUG ((DEBUG_INFO, "VMD: Checking RP B/D/F 0/%d/%d with DID 0x%x\n", RpDev, RpFunc, PciSegmentRead16 (RpDeviceBaseAddress + PCI_DEVICE_ID_OFFSET)));

    PciSegmentWrite32 (RpDeviceBaseAddress + PCI_BRIDGE_PRIMARY_BUS_REGISTER_OFFSET, PcieBusNum);
    Bus = PciSegmentRead8 (RpDeviceBaseAddress + PCI_BRIDGE_SECONDARY_BUS_REGISTER_OFFSET);

    ///
    /// Assign temporary subordinate bus number so that device this bridge can be seen
    ///
    PciSegmentWrite8 (RpDeviceBaseAddress + PCI_BRIDGE_SUBORDINATE_BUS_REGISTER_OFFSET, 0xFF);

    SubBusNum                       = VmdEnumerateDownstream (Bus);
    ///
    /// Update the actual subordinate bus number
    ///
    PciSegmentWrite8 (RpDeviceBaseAddress + PCI_BRIDGE_SUBORDINATE_BUS_REGISTER_OFFSET, SubBusNum);
    //PcieBusNum = (SubBusNum + 1) << 8;

    for (Bus = 1; Bus <= SubBusNum; Bus++) {
      for (Dev = 0; Dev < 32; Dev++) {
        ///
        /// Read Vendor ID to check if device exists
        /// if no device exists, then check next device
        ///
        EpDeviceBaseAddress = PCI_SEGMENT_LIB_ADDRESS (DEFAULT_PCI_SEGMENT_NUMBER_PCH, Bus, Dev, 0, 0);
        if (PciSegmentRead16 (EpDeviceBaseAddress + PCI_VENDOR_ID_OFFSET) == 0xFFFF) {
          continue;
        }
        ///
        /// Check for a multifunction device
        ///
        HeaderType = PciSegmentRead8 (EpDeviceBaseAddress + PCI_HEADER_TYPE_OFFSET);
        if ((HeaderType & HEADER_TYPE_MULTI_FUNCTION) != 0) {
          MaxFunction = 7;
        } else {
          MaxFunction = 0;
        }

        for (Func = 0; Func <= MaxFunction; Func++) {
          EpDeviceBaseAddress = PCI_SEGMENT_LIB_ADDRESS (DEFAULT_PCI_SEGMENT_NUMBER_PCH, Bus, Dev, Func, 0);
          if (PciSegmentRead16 (EpDeviceBaseAddress + PCI_VENDOR_ID_OFFSET) == 0xFFFF) {
            continue;
          }
          // Check if the device is mass storage device
          if (PciSegmentRead16 (EpDeviceBaseAddress + R_PCI_SCC_OFFSET) == 0x0108) {
            DevId = (PciSegmentRead16 (EpDeviceBaseAddress + PCI_DEVICE_ID_OFFSET));
            if (NumOfDevices && (VmdInfoHob->VmdPortInfo.PortInfo[NumOfDevices-1].RpDev == RpDev) &&
                (VmdInfoHob->VmdPortInfo.PortInfo[NumOfDevices-1].RpFunc == RpFunc)) {
              DEBUG ((DEBUG_INFO, "VMD: Another storage device found under root port B/D/F  %d/%d/%d \n", RpBus, RpDev, RpFunc));
              DEBUG ((DEBUG_INFO, "EpDevId 0x%x \n", DevId));
              continue;
            }
            if ((NonStorageRpBus == RpBus) && (NonStorageRpDev == RpDev) && (NonStorageRpFunc == RpFunc)) {
              DEBUG ((DEBUG_INFO, "VMD: Another Non storage device was found under the same root port B/D/F  %d/%d/%d \n", RpBus, RpDev, RpFunc));
              DEBUG ((DEBUG_INFO, "Hence skipping this RP entry from possible VMD mapped root ports\n"));
              continue;
            }

            VmdInfoHob->VmdPortInfo.PortInfo[NumOfDevices].DeviceDetected = 1;
            VmdInfoHob->VmdPortInfo.PortInfo[NumOfDevices].DevId = DevId;
            VmdInfoHob->VmdPortInfo.PortInfo[NumOfDevices].RpDev  = (UINT8) RpDev;
            VmdInfoHob->VmdPortInfo.PortInfo[NumOfDevices].RpFunc = (UINT8) RpFunc;
            VmdInfoHob->VmdPortInfo.PortInfo[NumOfDevices].RpIndex = (UINT8)((PciSegmentRead32 (RpDeviceBaseAddress + R_PCIE_LCAP) & 0xFF000000) >> 24);
            ++NumOfDevices;

            DEBUG ((DEBUG_INFO, "VMD: Storage device found \t" ));
            DEBUG ((DEBUG_INFO, "on B/D/F  %d/%d/%d \t", Bus, Dev, Func));
            DEBUG ((DEBUG_INFO, "under RP B/D/F  %d/%d/%d \t", RpBus, RpDev, RpFunc));
            DEBUG ((DEBUG_INFO, "EpDevId 0x%x \t Rp Index %d\n", DevId, VmdInfoHob->VmdPortInfo.PortInfo[NumOfDevices-1].RpIndex));
          } else {
            DEBUG ((DEBUG_INFO, "VMD: Non Storage device found \t" ));
            DEBUG ((DEBUG_INFO, "on B/D/F  %d/%d/%d \t", Bus, Dev, Func));
            DEBUG ((DEBUG_INFO, "under RP B/D/F  %d/%d/%d \t", RpBus, RpDev, RpFunc));
            DEBUG ((DEBUG_INFO, "EpDevId 0x%x \n", DevId));
            NonStorageRpBus  = RpBus;
            NonStorageRpDev  = RpDev;
            NonStorageRpFunc = RpFunc;
            if (NumOfDevices && (VmdInfoHob->VmdPortInfo.PortInfo[NumOfDevices-1].RpBus == RpBus) &&
                (VmdInfoHob->VmdPortInfo.PortInfo[NumOfDevices-1].RpDev == RpDev) &&
                (VmdInfoHob->VmdPortInfo.PortInfo[NumOfDevices-1].RpFunc == RpFunc)) {
              DEBUG ((DEBUG_INFO, "VMD: Another Non storage device found under the same root port B/D/F  %d/%d/%d \n", RpBus, RpDev, RpFunc));
              DEBUG ((DEBUG_INFO, "Hence skipping this RP entry from possible VMD mapped root ports\n"));
              --NumOfDevices;
              VmdInfoHob->VmdPortInfo.PortInfo[NumOfDevices].DeviceDetected = 0;
              VmdInfoHob->VmdPortInfo.PortInfo[NumOfDevices].DevId = 0;
              VmdInfoHob->VmdPortInfo.PortInfo[NumOfDevices].RpDev  = 0;
              VmdInfoHob->VmdPortInfo.PortInfo[NumOfDevices].RpFunc = 0;
              VmdInfoHob->VmdPortInfo.PortInfo[NumOfDevices].RpIndex = 0;
              DEBUG ((DEBUG_INFO, "Cleared last entry, Updated NumOfDevices is %d \n", NumOfDevices));
            }
          }
        }
      }
    }
    ///
    /// Clear bus number on all the bridges that we have opened so far.
    /// We have to do it in the reverse Bus number order.
    ///
    for (Bus = SubBusNum; Bus >= 1; Bus--) {
      for (Dev = 0; Dev < 32; Dev++) {
        ///
        /// Read Vendor ID to check if device exists
        /// if no device exists, then check next device
        ///
        EpDeviceBaseAddress = PCI_SEGMENT_LIB_ADDRESS (DEFAULT_PCI_SEGMENT_NUMBER_PCH, Bus, Dev, 0, 0);
        if (PciSegmentRead16 (EpDeviceBaseAddress + PCI_VENDOR_ID_OFFSET) == 0xFFFF) {
          continue;
        }

        Buffer16 = PciSegmentRead16 (EpDeviceBaseAddress + R_PCI_SCC_OFFSET);
        ///
        /// Clear Bus Number for PCI/PCI Bridge Device
        ///
        if (Buffer16 == 0x0604) {
          PciSegmentWrite32 (EpDeviceBaseAddress + PCI_BRIDGE_PRIMARY_BUS_REGISTER_OFFSET, 0);
        }
      }
    }
      ///
      /// Clear bus numbers so that PCIe slots are hidden
      ///
      PciSegmentWrite32 (RpDeviceBaseAddress + PCI_BRIDGE_PRIMARY_BUS_REGISTER_OFFSET, 0);
      //reset the nus numbers
  } // End of PCH PCIe Rp

  DEBUG ((DEBUG_INFO, "VMD: Checking the availability of SATA controller(0/23/0) for sata devices \n" ));
  RpDeviceBaseAddress = SataPciCfgBase (SATA_1_CONTROLLER_INDEX);
  if (PciSegmentRead16 (RpDeviceBaseAddress + PCI_VENDOR_ID_OFFSET) != 0xFFFF) {
    DevId = (PciSegmentRead16 (RpDeviceBaseAddress + PCI_DEVICE_ID_OFFSET));
    VmdInfoHob->VmdPortInfo.PortInfo[NumOfDevices].DeviceDetected = 1;
    VmdInfoHob->VmdPortInfo.PortInfo[NumOfDevices].DevId = DevId;
    VmdInfoHob->VmdPortInfo.PortInfo[NumOfDevices].RpDev  = SataDevNumber (SATA_1_CONTROLLER_INDEX);
    VmdInfoHob->VmdPortInfo.PortInfo[NumOfDevices].RpFunc = SataFuncNumber (SATA_1_CONTROLLER_INDEX);
    ++NumOfDevices;
    DEBUG ((DEBUG_INFO, "VMD: Sata device found \t" ));
    DEBUG ((DEBUG_INFO, "DevId 0x%x\n", DevId));
  }
  return EFI_SUCCESS;
}
