/** @file
  CPU PCIe information library.

  All function in this library is available for PEI, DXE, and SMM,
  But do not support UEFI RUNTIME environment call.

@copyright
  INTEL CONFIDENTIAL
  Copyright 2018 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains an 'Intel Peripheral Driver' and is uniquely identified as
  "Intel Reference Module" and is licensed for Intel CPUs and chipsets under
  the terms of your license agreement with Intel or your vendor. This file may
  be modified by the user, subject to additional terms of the license agreement.

@par Specification Reference:
**/
#include <Uefi/UefiBaseType.h>
#include <Library/CpuPlatformLib.h>
#include <Library/DebugLib.h>
#include <Library/BaseLib.h>
#include <Library/PciSegmentLib.h>
#include <Register/CpuPcieRegs.h>
#include <Library/CpuPcieInfoFruLib.h>
#include <Library/CpuPcieInitCommon.h>
#include <CpuPcieInfo.h>
#include <CpuSbInfo.h>
#include <Library/CpuPcieExpressHelpersLib.h>
#include <PcieRegs.h>
#include <Library/PreSiliconEnvDetectLib.h>

/**
  PCIe controller configuration strings.
**/
GLOBAL_REMOVE_IF_UNREFERENCED CONST CHAR8* mCpuPcieControllerConfigName[] = {
  "1x16",
  "2x8",
  "1x4"
};

/**
  PCIe controller VWM Mapping.
**/
GLOBAL_REMOVE_IF_UNREFERENCED VWM_MAPPING AdlSVwmMapping[] = {
    {0xBB, 0x2B, 0x27, 0x26}, // PEG060
    {0xBB, 0x28, 0x21, 0x20}, // PEG010 (Gen5 Controller)
    {0xBB, 0x29, 0x23, 0x22}  // PEG011 (Gen5 Controller)
};

GLOBAL_REMOVE_IF_UNREFERENCED VWM_MAPPING AdlPVwmMapping[] = {
    {0xBB, 0x2B, 0x27, 0x26}, // PEG060
    {0xBB, 0x28, 0x21, 0x20}, // PEG010 (Gen5 Controller)
    {0xBB, 0x2A, 0x25, 0x24}  // PEG062
};

/**
  Get VWM Mapping for the Platform

  @retval Pointer to VWM mapping structure
**/
VWM_MAPPING
*GetVwmMapping (
    IN     UINT32        RpIndex
  )
{
  switch (GetCpuSku ()) {
    case EnumCpuTrad:
      return &AdlSVwmMapping[RpIndex];
    default:
      return &AdlPVwmMapping[RpIndex];
  }
}

/**
  Returns the PCIe controller configuration (1x16, 1x8-2x4, 2x8, 1x4)

  @retval PCIe controller configuration
**/
UINT8
GetCpuPcieControllerConfig (
  VOID
  )
{
  UINT32                     Data32;
  UINT8                      Config;
  CONST CHAR8*               ControllerName;

  if (PciSegmentRead16 (PCI_SEGMENT_LIB_ADDRESS(SA_SEG_NUM, SA_PEG_BUS_NUM, SA_PEG_DEV_NUM, SA_PEG1_FUN_NUM, PCI_VENDOR_ID_OFFSET)) != 0xFFFF) {
    Data32 = PciSegmentRead32 (PCI_SEGMENT_LIB_ADDRESS(SA_SEG_NUM, SA_PEG_BUS_NUM, SA_PEG_DEV_NUM, SA_PEG1_FUN_NUM, R_PCIE_STRPFUSECFG));
  } else {
    Data32 = PciSegmentRead32 (PCI_SEGMENT_LIB_ADDRESS(SA_SEG_NUM, SA_PEG_BUS_NUM, SA_PEG_DEV_NUM, SA_PEG0_FUN_NUM, R_PCIE_STRPFUSECFG));
  }
  Config = (UINT8)((Data32 & B_PCIE_STRPFUSECFG_RPC_MASK) >> B_PCIE_STRPFUSECFG_RPC_OFFSET);
  DEBUG((DEBUG_INFO, "Controller Config Data32 = %x\n", Data32));
  DEBUG((DEBUG_INFO, "Controller Config = %x\n", Config));

  switch (Config) {
    case CPU_PCIE_1x16 :
      ControllerName = mCpuPcieControllerConfigName[0];
      break;
    case CPU_PCIE_2x8 :
     ControllerName = mCpuPcieControllerConfigName[1];
     break;
    case CPU_PCIE_1x4 :
      ControllerName = mCpuPcieControllerConfigName[2];
     break;
    default:
      ControllerName = mCpuPcieControllerConfigName[2];
      break;
  }
  DEBUG((DEBUG_INFO, "CPU PCIe Furcation is %a\n", ControllerName));
  return Config;
}

/**
  Get Maximum CPU Pcie Root Port Number

  @retval Maximum CPU Pcie Root Port Number
**/
UINT8
GetMaxCpuPciePortNum (
  VOID
  )
{
  switch (GetCpuSku ()) {
    case EnumCpuTrad:
      return CPU_PCIE_DT_HALO_MAX_ROOT_PORT;
    case EnumCpuUlt:
      return CPU_PCIE_ULT_MAX_ROOT_PORT;
    case EnumCpuUlx:
      return CPU_PCIE_ULX_MAX_ROOT_PORT;
    default:
      return CPU_PCIE_ULT_MAX_ROOT_PORT;
  }
}

/**
  Get max link width.

  @param[in] RpBase    Root Port base address
  @retval Max link width
**/
UINT8
CpuPcieGetMaxLinkWidth(
  UINT64  RpBase
  )
{
  UINT8   LinkWidth;

  DEBUG ((DEBUG_INFO, "CpuPcieGetMaxLinkWidth Start!\n"));

  if (PciSegmentRead16 (RpBase + PCI_VENDOR_ID_OFFSET) == 0xFFFF) {
    LinkWidth = 0;
    DEBUG((DEBUG_INFO, "VendorId = %x so assign LinkWidth = %d\n", PciSegmentRead16 (RpBase + PCI_VENDOR_ID_OFFSET), LinkWidth));
    return LinkWidth;
  }
  LinkWidth = (UINT8) ((PciSegmentRead32 (RpBase + R_PCIE_LCAP) & B_PCIE_LCAP_MLW) >> B_PCIE_LCAP_MLW_OFFSET);
  if (GetCpuSku () != EnumCpuTrad) { // Only Desktop SKUs support x16 slot, so limit the Linkwidth to 8 for other ADL HU
    ASSERT (LinkWidth <= 8);
    if (LinkWidth > 8) {
      LinkWidth = 8;
    }
  } else {
    ASSERT(LinkWidth <= 16);
    if (LinkWidth > 16) {
      LinkWidth = 16;
    }
  }
  DEBUG ((DEBUG_INFO, "LinkWidth = %x\n", LinkWidth));
  DEBUG ((DEBUG_INFO, "CpuPcieGetMaxLinkWidth End!\n"));

  return LinkWidth;
}

/**
  Get Maximum CPU Pcie Controller Number

  @retval Maximum CPU Pcie Controller Number
**/
UINT8
GetMaxCpuPcieControllerNum (
  VOID
  )
{
  switch (GetCpuSku ()) {
    case EnumCpuTrad:
      return CPU_PCIE_DT_HALO_MAX_CONTROLLER;
    case EnumCpuUlt:
      return CPU_PCIE_ULT_MAX_CONTROLLER;
    case EnumCpuUlx:
      return CPU_PCIE_ULX_MAX_CONTROLLER;
    default:
      return CPU_PCIE_ULT_MAX_CONTROLLER;
  }
}

/**
  Get CPU Pcie Controller Number from Root Port Number

  @retval CPU Pcie Controller Number
**/
UINT8
GetCpuPcieControllerNum (
  IN     UINT32        RpIndex
  )
{
  switch (RpIndex) {
    case 0:
      return CPU_PCIE_CONTROLLER0;
    case 1:
      return CPU_PCIE_CONTROLLER1;
    case 2:
      if (GetCpuSku () == EnumCpuTrad) {
        return CPU_PCIE_CONTROLLER1;
      } else {
        return CPU_PCIE_CONTROLLER2;
      }
    default:
      return CPU_PCIE_CONTROLLER0;
  }
}

/**
  Check if a given Root Port is Multi VC

  @param[in] RpIndex (0-based)

  @return  TRUE if it is Multi VC
**/
BOOLEAN
IsRpMultiVc (
  IN  UINT32                   RpIndex
  )
{
  //
  // Note : PEG060 will be treated as controller 0 for all SKUs to maintain uniform coding and avoid CpuFamily check in all functions
  //
  if (RpIndex == 0) {
    return TRUE;
  } else if ((RpIndex == 2) && (GetCpuFamily() == CPUID_FULL_FAMILY_MODEL_ALDERLAKE_MOBILE)) { // ADL-P PEG62 supports MultiVc
    return TRUE;
  }
  return FALSE;
}

/**
Read PCD register and return true based on port configuration and disable bits

@param[in] RpIndex (0-based)

@return  TRUE if root port is enabled
**/
BOOLEAN
CpuPcieReadPcdReg (
  IN UINT32  RpIndex
)
{
  UINT32       SpxPcd;
  UINT32       MaxPciePortNum;
  MaxPciePortNum = GetMaxCpuPciePortNum ();
  for (RpIndex = 0; RpIndex < MaxPciePortNum; RpIndex++) {
    CpuPcieRpSbiRead32 (RpIndex, R_SA_SPX_PCR_PCD, &SpxPcd);
    if ((SpxPcd & (B_SA_SPX_PCR_PCD_P0D << RpIndex)) != 0) {
      return TRUE;
    }
  }
  return FALSE;
}

/**
  Checks if PEG port is present

  @retval TRUE     PEG is presented
  @retval FALSE    PEG is not presented
**/
BOOLEAN
IsPegPresent (
  VOID
  )
{
  UINT64  PegBaseAddress;

  PegBaseAddress  = PCI_SEGMENT_LIB_ADDRESS (SA_SEG_NUM, SA_PEG_BUS_NUM, SA_PEG_DEV_NUM, 0, 0);
  if (PciSegmentRead16 (PegBaseAddress) != 0xFFFF) {
    return TRUE;
  }
  return FALSE;
}

/**
  Get CPU Pcie Root Port Device and Function Number by Root Port physical Number

  @param[in]  RpNumber              Root port physical number. (0-based)
  @param[out] RpDev                 Return corresponding root port device number.
  @param[out] RpFun                 Return corresponding root port function number.

  @retval     EFI_SUCCESS           Root port device and function is retrieved
  @retval     EFI_INVALID_PARAMETER RpNumber is invalid
**/
EFI_STATUS
EFIAPI
GetCpuPcieRpDevFun (
  IN  UINTN   RpNumber,
  OUT UINTN   *RpDev,
  OUT UINTN   *RpFun
  )
{
  if (RpNumber > GetMaxCpuPciePortNum ()) {
    DEBUG ((DEBUG_ERROR, "GetCpuPcieRpDevFun invalid RpNumber %x", RpNumber));
    ASSERT (FALSE);
    return EFI_INVALID_PARAMETER;
  }

  switch (RpNumber) {
    case 0:
      *RpDev = 6;
      *RpFun = 0;
      break;
    case 1:
      *RpDev = 1;
      *RpFun = 0;
      break;
    case 2:
      if (GetCpuSku () == EnumCpuTrad) {
        *RpDev = 1;
        *RpFun = 1;
      } else {
        *RpDev = 6;
        *RpFun = 2;
      }
      break;
    default:
      *RpDev = 6;
      *RpFun = 0;
      break;
  }
  return EFI_SUCCESS;
}

/**
  Get Root Port Index by CPU Pcie Root Port Device and Function Number

  @param[in]  RpDev                Root Port pci Device number.
  @param[in]  RpFun                Root Port pci Function number.

  @return     Corresponding physical Root Port index (0-based)
**/
UINT32
EFIAPI
GetCpuPcieRpNumber (
  IN  UINTN   RpDev,
  IN  UINTN   RpFun
  )
{
  UINT32 RpIndex;
  RpIndex = 0;
  switch (RpDev) {
    case 1: // Device 1
      if (RpFun == 0) { // Device 1 Function 0
        RpIndex = 1;
      } else if (RpFun == 1) { // Device 1 Function 1 for ADL-S
        RpIndex = 2;
      }
      break;
    case 6: // Device 6
      if (RpFun == 0) { // Device 6 Function 0
        RpIndex = 0;
      } else { // Device 6 Function 2 for ADL-P
        RpIndex = 2;
      }
      break;
    default:
      RpIndex = 0;
  }
  return RpIndex;
}

/**
  Gets pci segment base address of PCIe root port.

  @param RpIndex    Root Port Index (0 based)

  @return PCIe port base address.
**/
UINT64
CpuPcieBase (
  IN  UINT32   RpIndex
  )
{
  UINTN   RpDevice;
  UINTN   RpFunction;
  GetCpuPcieRpDevFun (RpIndex, &RpDevice, &RpFunction);
  return PCI_SEGMENT_LIB_ADDRESS (SA_SEG_NUM, SA_MC_BUS, (UINT32) RpDevice, (UINT32) RpFunction, 0);
}

/**
  Determines whether CPU is supported Pciex16.

  @return TRUE if CPU is supported, FALSE otherwise
**/
BOOLEAN
IsCpuPciex16Supported (
  VOID
  )
{
  if (GetCpuSku () == EnumCpuTrad) {
    return TRUE;
  } else {
    return FALSE;
  }
}

/**
  Get CPU Pcie SIP version

  @param  RpIndex    Root Port Index (0 based)

  @retval CPU SIP version
**/
UINT8
GetCpuPcieSipInfo (
  IN     UINT32        RpIndex
  )
{
  switch (RpIndex) {
    case 0:
      return PCIE_SIP16;
    case 1:
      return PCIE_SIP17;
    case 2:
      if (GetCpuSku () == EnumCpuTrad) {
        return PCIE_SIP17;
      } else {
        return PCIE_SIP16;
      }
    default:
      return PCIE_SIP16;
  }
}

/**
  This function returns PID according to PCIe controller index

  @param[in] ControllerIndex     PCIe controller index

  @retval CPU_SB_DEVICE_PID    Returns PID for SBI Access
**/
CPU_SB_DEVICE_PID
GetCpuPcieControllerSbiPid (
  IN  UINT32  ControllerIndex
  )
{
//
// Note : PEG060 will be treated as controller 0 for all SKUs to maintain uniform coding and avoid CpuFamily check in all functions
//
  switch(ControllerIndex){
    case 0:
      return CPU_SB_PID_PEG60;
    case 1:
      return CPU_SB_PID_PEG10_11;
    case 2:
      return CPU_SB_PID_PEG62;
    default:
      ASSERT(FALSE);
      return CPU_SB_PID_PEG60;
  }
}

/**
  This function returns PID according to Root Port Number

  @param[in] RpIndex     Root Port Index (0-based)

  @retval CPU_SB_DEVICE_PID    Returns PID for SBI Access
**/
CPU_SB_DEVICE_PID
GetCpuPcieRpSbiPid (
  IN  UINTN  RpIndex
  )
{
//
// Note : PEG060 will be treated as controller 0 for all SKUs to maintain uniform coding and avoid CpuFamily check in all functions
//
  switch(RpIndex){
    case 0:
      return CPU_SB_PID_PEG60;
    case 1:
      return CPU_SB_PID_PEG10_11;
    case 2:
      if (GetCpuFamily() == CPUID_FULL_FAMILY_MODEL_ALDERLAKE_MOBILE) {
        return CPU_SB_PID_PEG62;
      } else {
        return CPU_SB_PID_SRFSM_SA15; // * PMIP will redirect P2SB access with PortID=0x4A to 0x4B with FID=1.
      }
    default:
      ASSERT(FALSE);
      return CPU_SB_PID_PEG60;
  }
}

/**
  Get max CPU PCIE lanes.

  @retval Max lanes
**/
UINT8
CpuPcieGetMaxLanes (
  )
{
  if (GetCpuSku () == EnumCpuTrad || GetCpuSku () == EnumCpuHalo) {
    return  MAX_CPU_PCIE_LANES_DT_HALO;
  } else {
    return  MAX_CPU_PCIE_LANES_ULT_ULX;
  }
}

/**
  This function returns FIA PID according to Root Port Number

  @param[in] RpIndex     Root Port Index (0-based)

  @retval CPU_SB_DEVICE_PID    Returns PID for SBI Access
**/
CPU_SB_DEVICE_PID
GetCpuPcieFiaSbiPid (
  IN  UINTN  RpIndex
  )
{
  switch(RpIndex){
    case 0:
      return CPU_SB_PID_PCIE_FIAx4_0;
    case 1:
      return CPU_SB_PID_PCIE_FIAx16_FIAx8;
    case 2:
      if (GetCpuFamily () == CPUID_FULL_FAMILY_MODEL_ALDERLAKE_MOBILE) {
        return CPU_SB_PID_PCIE_FIAx4_2;
      } else {
        return 0xFF;
      }
    default:
      ASSERT (FALSE);
      return CPU_SB_PID_PCIE_FIAx4_0;
  }
}
/**
  This function sets Soc Specific PowerGating
**/
VOID
SocSpecificPowerGating (
  )
{
  return;
}
/**
  This function checks whether Skip Link Training WA Required or not.

  @param[in] RpIndex     Root Port Index (0-based)

  @return TRUE if Skip Link Training WA Required, FALSE otherwise
**/
BOOLEAN
IsSkipLinkTrainingWaRequired (
  IN  UINT32  RpIndex
  )
{
  return FALSE;
}

/**
  This function will configure PTM initilization according to SKU.

  @return TRUE if Mobile Sku
**/
BOOLEAN
CpuPciePtmConfiguration (
  VOID
  )
{
  if(GetCpuFamily () == CPUID_FULL_FAMILY_MODEL_ALDERLAKE_MOBILE) {
    return TRUE;
  }
  return FALSE;
}


/**
  Reads an 8-bit PCI configuration register.

  @param  RpIndex   Root Port Index (0-based)
  @param  Address   Address that encodes the PCI Segment, Bus, Device, Function, and Register.

  @return The 8-bit PCI configuration register specified by Address.
**/
UINT8
EFIAPI
PegPciSegmentRead8 (
  IN UINT32                    RpIndex,
  IN UINT64                    Address
  )
{
  if (!(IsSimicsEnvironment())) {
    return CpuRegbarRead8 (GetCpuPcieRpSbiPid (RpIndex), (Address & 0xFFF));
  }

  return PciSegmentRead8 (Address);
}

/**
  Writes an 8-bit PCI configuration register.

  @param  RpIndex     Root Port Index (0-based)
  @param  Address     Address that encodes the PCI Segment, Bus, Device, Function, and Register.
  @param  Value       The value to write.

  @return The value written to the PCI configuration register.
**/
UINT8
EFIAPI
PegPciSegmentWrite8 (
  IN UINT32                    RpIndex,
  IN UINT64                    Address,
  IN UINT8                     Value
  )
{
  if (!(IsSimicsEnvironment())) {
    return CpuRegbarWrite8 (GetCpuPcieRpSbiPid (RpIndex), (Address & 0xFFF), Value);
  }

  return PciSegmentWrite8 (Address, Value);
}

/**
  Reads a 16-bit PCI configuration register.

  @param  RpIndex   Root Port Index (0-based)
  @param  Address   Address that encodes the PCI Segment, Bus, Device, Function, and Register.

  @return The 16-bit PCI configuration register specified by Address.
**/
UINT16
EFIAPI
PegPciSegmentRead16 (
  IN UINT32                    RpIndex,
  IN UINT64                    Address
  )
{
  if (!(IsSimicsEnvironment())) {
    return CpuRegbarRead16 (GetCpuPcieRpSbiPid (RpIndex), (Address & 0xFFF));
  }

  return PciSegmentRead16 (Address);
}

/**
  Writes a 16-bit PCI configuration register.

  @param  RpIndex     Root Port Index (0-based)
  @param  Address     Address that encodes the PCI Segment, Bus, Device, Function, and Register.
  @param  Value       The value to write.

  @return The parameter of Value.
**/
UINT16
EFIAPI
PegPciSegmentWrite16 (
  IN UINT32                    RpIndex,
  IN UINT64                    Address,
  IN UINT16                     Value
  )
{
  if (!(IsSimicsEnvironment())) {
    return CpuRegbarWrite16 (GetCpuPcieRpSbiPid (RpIndex), (Address & 0xFFF), Value);
  }

  return PciSegmentWrite16 (Address, Value);
}

/**
  Reads a 32-bit PCI configuration register.

  @param  RpIndex   Root Port Index (0-based)
  @param  Address   Address that encodes the PCI Segment, Bus, Device, Function, and Register.

  @return The 32-bit PCI configuration register specified by Address.
**/
UINT32
EFIAPI
PegPciSegmentRead32 (
  IN UINT32                    RpIndex,
  IN UINT64                    Address
  )
{
  if (!(IsSimicsEnvironment())) {
    return CpuRegbarRead32 (GetCpuPcieRpSbiPid (RpIndex), (Address & 0xFFF));
  }

  return PciSegmentRead32 (Address);
}

/**
  Writes a 32-bit PCI configuration register.

  @param  RpIndex     Root Port Index (0-based)
  @param  Address     Address that encodes the PCI Segment, Bus, Device, Function, and Register.
  @param  Value       The value to write.

  @return The parameter of Value.
**/
UINT32
EFIAPI
PegPciSegmentWrite32 (
  IN UINT32                    RpIndex,
  IN UINT64                    Address,
  IN UINT32                     Value
  )
{
  if (!(IsSimicsEnvironment())) {
    return CpuRegbarWrite32 (GetCpuPcieRpSbiPid (RpIndex), (Address & 0xFFF), Value);
  }

  return PciSegmentWrite32 (Address, Value);
}

/**
  Performs a bitwise OR of a 16-bit PCI configuration register with a 16-bit value.

  @param  RpIndex   Root Port Index (0-based)
  @param  Address   Address that encodes the PCI Segment, Bus, Device, Function, and Register.
  @param  OrData    The value to OR with the PCI configuration register.

  @return The value written to the PCI configuration register.
**/
UINT16
EFIAPI
PegPciSegmentOr16 (
  IN UINT32                    RpIndex,
  IN UINT64                    Address,
  IN UINT16                    OrData
  )
{
  if (!(IsSimicsEnvironment())) {
    return CpuRegbarOr16 (GetCpuPcieRpSbiPid (RpIndex), (Address & 0xFFF), OrData);
  }

  return PciSegmentOr16 (Address, OrData);
}

/**
  Performs a bitwise AND of a 16-bit PCI configuration register with a 16-bit value.

  @param  RpIndex   Root Port Index (0-based)
  @param  Address   Address that encodes the PCI Segment, Bus, Device, Function, and Register.
  @param  AndData   The value to AND with the PCI configuration register.

  @return The value written to the PCI configuration register.

**/
UINT16
EFIAPI
PegPciSegmentAnd16 (
  IN UINT32                    RpIndex,
  IN UINT64                    Address,
  IN UINT16                    AndData
  )
{
  if (!(IsSimicsEnvironment())) {
    return CpuRegbarAnd16 (GetCpuPcieRpSbiPid (RpIndex), (Address & 0xFFF), AndData);
  }

  return PciSegmentAnd16 (Address, AndData);
}

/**
  Performs a bitwise AND of a 8-bit PCI configuration register with a 8-bit value.

  @param  RpIndex   Root Port Index (0-based)
  @param  Address   Address that encodes the PCI Segment, Bus, Device, Function, and Register.
  @param  AndData   The value to AND with the PCI configuration register.

  @return The value written to the PCI configuration register.

**/
UINT8
EFIAPI
PegPciSegmentAnd8 (
  IN UINT32                    RpIndex,
  IN UINT64                    Address,
  IN UINT8                     AndData
  )
{
  if (!(IsSimicsEnvironment())) {
    return CpuRegbarAnd8 (GetCpuPcieRpSbiPid (RpIndex), (Address & 0xFFF), AndData);
  }

  return PciSegmentAnd8 (Address, AndData);
}

/**
  Performs a bitwise OR of a 32-bit PCI configuration register with a 32-bit value.

  @param  RpIndex   Root Port Index (0-based)
  @param  Address   Address that encodes the PCI Segment, Bus, Device, Function, and Register.
  @param  OrData    The value to OR with the PCI configuration register.

  @return The value written to the PCI configuration register.
**/
UINT32
EFIAPI
PegPciSegmentOr32 (
  IN UINT32                    RpIndex,
  IN UINT64                    Address,
  IN UINT32                    OrData
  )
{
  if (!(IsSimicsEnvironment())) {
    return CpuRegbarOr32 (GetCpuPcieRpSbiPid (RpIndex), (Address & 0xFFF), OrData);
  }

  return PciSegmentOr32 (Address, OrData);
}

/**
  Performs a bitwise OR of a 8-bit PCI configuration register with a 8-bit value.

  @param  RpIndex   Root Port Index (0-based)
  @param  Address   Address that encodes the PCI Segment, Bus, Device, Function, and Register.
  @param  OrData    The value to OR with the PCI configuration register.

  @return The value written to the PCI configuration register.
**/
UINT8
EFIAPI
PegPciSegmentOr8 (
  IN UINT32                    RpIndex,
  IN UINT64                    Address,
  IN UINT8                     OrData
  )
{
  if (!(IsSimicsEnvironment())) {
    return CpuRegbarOr8 (GetCpuPcieRpSbiPid (RpIndex), (Address & 0xFFF), OrData);
  }

  return PciSegmentOr8 (Address, OrData);
}

/**
  Performs a bitwise AND of a 32-bit PCI configuration register with a 32-bit value.

  @param  RpIndex   Root Port Index (0-based)
  @param  Address   Address that encodes the PCI Segment, Bus, Device, Function, and Register.
  @param  AndData   The value to AND with the PCI configuration register.

  @return The value written to the PCI configuration register.

**/
UINT32
EFIAPI
PegPciSegmentAnd32 (
  IN UINT32                    RpIndex,
  IN UINT64                    Address,
  IN UINT32                    AndData
  )
{
  if (!(IsSimicsEnvironment())) {
    return CpuRegbarAnd32 (GetCpuPcieRpSbiPid (RpIndex), (Address & 0xFFF), AndData);
  }

  return PciSegmentAnd32 (Address, AndData);
}

/**
  Performs a bitwise AND of a 32-bit PCI configuration register with a 32-bit value,
  followed a  bitwise OR with another 32-bit value.

  @param  RpIndex   Root Port Index (0-based)
  @param  Address   Address that encodes the PCI Segment, Bus, Device, Function, and Register.
  @param  AndData   The value to AND with the PCI configuration register.
  @param  OrData    The value to OR with the PCI configuration register.

  @return The value written to the PCI configuration register.
**/
UINT32
EFIAPI
PegPciSegmentAndThenOr32 (
  IN UINT32                    RpIndex,
  IN UINT64                    Address,
  IN UINT32                    AndData,
  IN UINT32                    OrData
  )
{
  if (!(IsSimicsEnvironment())) {
    return CpuRegbarAndThenOr32 (GetCpuPcieRpSbiPid (RpIndex), (Address & 0xFFF), AndData, OrData);
  }

  return PciSegmentAndThenOr32 (Address, AndData, OrData);
}


/**
  Performs a bitwise AND of a 16-bit PCI configuration register with a 16-bit value,
  followed a  bitwise OR with another 16-bit value.

  @param  RpIndex   Root Port Index (0-based)
  @param  Address   Address that encodes the PCI Segment, Bus, Device, Function, and Register.
  @param  AndData   The value to AND with the PCI configuration register.
  @param  OrData    The value to OR with the PCI configuration register.

  @return The value written to the PCI configuration register.
**/
UINT16
EFIAPI
PegPciSegmentAndThenOr16 (
  IN UINT32                    RpIndex,
  IN UINT64                    Address,
  IN UINT16                    AndData,
  IN UINT16                    OrData
  )
{
  if (!(IsSimicsEnvironment())) {
    return CpuRegbarAndThenOr16 (GetCpuPcieRpSbiPid(RpIndex), (Address & 0xFFF), AndData, OrData);
  }

  return PciSegmentAndThenOr16 (Address, AndData, OrData);
}


/**
  Performs a bitwise AND of a 8-bit PCI configuration register with a 8-bit value,
  followed a  bitwise OR with another 8-bit value.

  @param  RpIndex   Root Port Index (0-based)
  @param  Address   Address that encodes the PCI Segment, Bus, Device, Function, and Register.
  @param  AndData   The value to AND with the PCI configuration register.
  @param  OrData    The value to OR with the PCI configuration register.

  @return The value written to the PCI configuration register.
**/
UINT8
EFIAPI
PegPciSegmentAndThenOr8 (
  IN UINT32                    RpIndex,
  IN UINT64                    Address,
  IN UINT8                     AndData,
  IN UINT8                     OrData
  )
{
  if (!(IsSimicsEnvironment())) {
    return CpuRegbarAndThenOr8 (GetCpuPcieRpSbiPid(RpIndex), (Address & 0xFFF), AndData, OrData);
  }

  return PciSegmentAndThenOr8 (Address, AndData, OrData);
}

/**
  This function checks whether HotPlug is supported or not.

  @param[in] RpIndex     Root Port Index (0-based)

  @return TRUE for Mobile Sku
**/
BOOLEAN
IsHotPlugSupported (
  IN  UINT32  RpIndex
  )
{
  if ((RpIndex != 0x1) && (GetCpuFamily () == CPUID_FULL_FAMILY_MODEL_ALDERLAKE_MOBILE)) {
    return TRUE;
  }
  return FALSE;
}

/**
  This function loads cpu pcie config block default values

  @param[in] CpuPcieRpConfig         Pointer to CpuPcieRpConfig block
**/
VOID
UpdateCpuPcieRpConfigDefault(
  IN CPU_PCIE_CONFIG        *CpuPcieRpConfig
  )
{
  UINT32                     Index;

  ///
  /// These config values are from the patch table entries and needs update once patch table is updated.
  ///
  for (Index = 0; Index < GetMaxCpuPciePortNum (); Index++) {
    if (GetCpuPcieSipInfo(Index) == PCIE_SIP17) {
      CpuPcieRpConfig->RootPort[Index].Gen3Uptp                           = 7;
      CpuPcieRpConfig->RootPort[Index].Gen4Uptp                           = 5;
      CpuPcieRpConfig->RootPort[Index].Gen4Dptp                           = 5;
      CpuPcieRpConfig->RootPort[Index].MultiVcEnabled                     = 0;
    }
    else {
      CpuPcieRpConfig->RootPort[Index].Gen3Uptp                           = 5;
      CpuPcieRpConfig->RootPort[Index].Gen4Uptp                           = 8;
      CpuPcieRpConfig->RootPort[Index].Gen4Dptp                           = 9;
      CpuPcieRpConfig->RootPort[Index].MultiVcEnabled                     = 1;
    }
  }
}

/**
  This function keeps PEG10 enable when PEG11 has endpoint connected
  @param[in] RpDisableMask  mask of rootprots that don't need to be considered

  @retval mask of rootprots that don't need to be considered
**/
UINT32
CpuPcieFunction0RemapEnable (
  IN UINT32     RpDisableMask
  )
{
  if (RpDisableMask & (BIT0 << 2)) {
    return RpDisableMask;
  } else {
    if((GetCpuFamily () == CPUID_FULL_FAMILY_MODEL_ALDERLAKE_MOBILE)) {
      RpDisableMask &= 0xFFFFFFFE;
      return RpDisableMask;
    } else {
      RpDisableMask &= 0xFFFFFFFD;
      return RpDisableMask;
    }
  }
}

/**
  Get the virtual wire CLKREQ message index for CPU PCIe root port.
  This step is neccessary since there is no explicit hardware indexing
  for PEG root ports. For instance PEG60 which is treated by BIOS as
  CPU root port 1 is advertising itself as CPU root port 3 on virtual wire
  message.

  @note: This function can be in PEG library to indicate the mapping.

  @param[in] RpIndex  CPU PCIe root port index

  @retval Root port index given root port uses in virtual wire CLKREQ message
**/
UINT32
GetCpuRpVirtualWireClkReqIndex (
  IN UINT32  RpIndex
  )
{
  switch (RpIndex) {
    case 0:
      return 3; // PEG60
    case 1:
      return 0; // PEG10
    case 2:
      if((GetCpuFamily () == CPUID_FULL_FAMILY_MODEL_ALDERLAKE_MOBILE)) {
        return 2; // PEG62
      } else {
        return 1; //PEG11
      }
    case 3:
      return 0; // PEG12
    default:
      ASSERT  (FALSE);
      return 0;
  }
}

/**
  @param[in] RpIndex  CPU PCIe root port index

  @return  TRUE for Gen5 Capable Controller
**/
BOOLEAN
IsIrrcProgrammingRequired (
  IN UINT32  RpIndex
  )
{
  if (RpIndex == 0x1) {
    return TRUE;
  }
  return FALSE;
}

/**
  Get CPU Pcie Root Port Disable Bit Position

  @param[in]  RpIndex              Root Port Index
  @param[out] Data                 Return corresponding root port disable bit.
  @param[out] PortId               Return corresponding root port PortId.

  @retval     EFI_SUCCESS           Root port device and function is retrieved
**/

EFI_STATUS
EFIAPI
GetCpuPcieRpPcdPortDisable (
  IN  UINT32        RpIndex,
  OUT UINT32        *Data,
  OUT UINT8         *PortId
  )
{
  //
  // There is only one PCD register for PEG1011
  // So decrement the RpIndex to get the correct portid for programming the PCD register in the case of PEG11
  //
  switch (RpIndex) {
    case 0:
    case 1:
      *Data = (UINT32)B_SA_SPX_PCR_PCD_P0D;
      *PortId = (UINT8) GetCpuPcieRpSbiPid (RpIndex);
      break;
    case 2:
      if (GetCpuFamily() == CPUID_FULL_FAMILY_MODEL_ALDERLAKE_MOBILE) {
        *Data = (UINT32)B_SA_SPX_PCR_PCD_P0D;
        *PortId = (UINT8) GetCpuPcieRpSbiPid (RpIndex);
      } else {
        *Data = (UINT32)B_SA_SPX_PCR_PCD_P1D;
        *PortId = (UINT8) GetCpuPcieRpSbiPid (RpIndex-1);
      }
      break;
    default:
      ASSERT (FALSE);
  }
  return EFI_SUCCESS;
}

/**
  This function checks if HsPHY 1 configuration required or not

  @return  TRUE if WA required
**/
BOOLEAN
CheckHsphyWaRequired (
  )
{
  UINT64     McD0BaseAddress;
  UINT64     RpBase;
  UINT32     DevEn;
  BOOLEAN    D0F0StrpFuseLr;
  BOOLEAN    D0F0SlotStsPds;
  BOOLEAN    D0F1SlotStsPds;

  DEBUG ((DEBUG_INFO, "CheckHsphyWaRequired\n"));

  McD0BaseAddress = PCI_SEGMENT_LIB_ADDRESS (SA_SEG_NUM, SA_MC_BUS, 0, 0, 0);
  DevEn           = PciSegmentRead32 (McD0BaseAddress + R_SA_DEVEN);

  if ((DevEn & B_SA_DEVEN_D1F0EN_MASK) || (DevEn & B_SA_DEVEN_D1F1EN_MASK)) {
    RpBase         = CpuPcieBase (1);
    D0F0StrpFuseLr = !!(PciSegmentRead16 (RpBase + R_PCIE_STRPFUSECFG) & B_PCIE_STRPFUSECFG_LR);
    D0F0SlotStsPds = !!(PciSegmentRead16 (RpBase + R_PCIE_SLSTS) & B_PCIE_SLSTS_PDS);

    RpBase         = CpuPcieBase (2);
    D0F1SlotStsPds = !!(PciSegmentRead16 (RpBase + R_PCIE_SLSTS) & B_PCIE_SLSTS_PDS);

    //
    // Return TRUE for the following conditions
    // Peg10.StrpFuseCfg.Lr = 0 && Peg10.SlotSts.Pds = 0 && Peg11.SlotSts.Pds = 1
    // Peg10.StrpFuseCfg.Lr = 1 && Peg10.SlotSts.Pds = 1 && Peg11.SlotSts.Pds = 0
    //
    if (((D0F0StrpFuseLr == FALSE) && (D0F0SlotStsPds == FALSE) && D0F1SlotStsPds) || (D0F0StrpFuseLr && D0F0SlotStsPds  && (D0F1SlotStsPds == FALSE))) {
      return TRUE;
    }
  }
  return FALSE;
}

/**
  This function returns register capability version according to SKU.

  @return    UINT32    Capability version.
**/
UINT32
GetCapabilityVersion (
  VOID
  )
{
  return 1;
}

/**
  This function helps set mask for Func0 PCIe Port to disable

  @param[in] RpDisableMask  mask of rootprots that don't need to be considered

  @retval mask of rootprots that don't need to be considered
**/
UINT32
CpuPcieFunc0LinkDisable (
  IN UINT32     RpDisableMask
  )
{
  if (GetCpuSku () == EnumCpuTrad) {
    RpDisableMask &= ~BIT1;
    return RpDisableMask;
  } else {
    RpDisableMask &= ~BIT0;
    return RpDisableMask;
  }
}

/**
This function checks whether Mild DCG is supported or not.

@return TRUE for Mobile Sku
**/
BOOLEAN
IsMildDcgSupported(
  VOID
  )
{
  if (GetCpuFamily() == CPUID_FULL_FAMILY_MODEL_ALDERLAKE_MOBILE) {
    return TRUE;
  } else {
    return FALSE;
  }
}
