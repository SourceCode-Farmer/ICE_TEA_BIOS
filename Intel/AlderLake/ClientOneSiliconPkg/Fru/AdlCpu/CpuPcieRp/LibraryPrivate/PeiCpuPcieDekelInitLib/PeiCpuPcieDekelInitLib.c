/** @file
  This file contains functions that initializes Dekel

@copyright
  INTEL CONFIDENTIAL
  Copyright 2018 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains an 'Intel Peripheral Driver' and is uniquely identified as
  "Intel Reference Module" and is licensed for Intel CPUs and chipsets under
  the terms of your license agreement with Intel or your vendor. This file may
  be modified by the user, subject to additional terms of the license agreement.

@par Specification Reference:
**/

#include <Library/PeiCpuPcieDekelInitLib.h>
#include <Library/CpuPcieInfoFruLib.h>
#include <CpuSbInfo.h>
#include <Library/DebugLib.h>
#include <Library/PreSiliconEnvDetectLib.h>
#include <Library/CpuPlatformLib.h>
#include <Library/ConfigBlockLib.h>
#include <Library/PeiServicesLib.h>
#include <Library/PciSegmentLib.h>
#include <Register/SaRegsHostBridge.h>
#include <Library/PeiCpuPciePolicyLib.h>
#include <Library/SiConfigBlockLib.h>
#include <Ppi/SiPolicy.h>
#include <Register/CpuPcieDekelRecipe.h>
#include <CpuPcieConfig.h>

/**
  This function returns Dekel PID according to PCIe RootPort controller index

  @param[in]  RpIndex          The root port to be initialized (zero based).

  @retval CPU_SB_DEVICE_PID    Returns PID for SBI Access
**/
CPU_SB_DEVICE_PID
CpuPcieGetDekelSbPid (
  IN  UINT32   RpIndex
  )
{
  CPU_SB_DEVICE_PID  CpuSbDevicePid;

  switch(RpIndex){
    case 0:
      CpuSbDevicePid = (CPU_SB_DEVICE_PID) CPU_SB_PID_PCIE_PHYx4_0;
      break;
    case 2:
      CpuSbDevicePid = (CPU_SB_DEVICE_PID) CPU_SB_PID_PCIE_PHYx4_2;
      break;
    default:
      ASSERT(FALSE);
      CpuSbDevicePid = 0;
      break;
  }
  return CpuSbDevicePid;
}

/**
  Read DEKEL FW download status

  @param[in]  CpuSbDevicePid   CPU SB Device Port ID

  @retval     BOOLEAN          Fw Download Completed TRUE or FALSE
**/

BOOLEAN
EFIAPI
DekelFwStatus (
  IN  UINT32   RpIndex
)
{
  CPU_SB_DEVICE_PID  CpuSbDevicePid;

  CpuSbDevicePid = CpuPcieGetDekelSbPid (RpIndex);

  if ((CpuRegbarRead32(CpuSbDevicePid, R_DEKEL_FW_STATUS) & (UINT32)BIT15) != BIT15) {
    switch (CpuSbDevicePid) {
      case CPU_SB_PID_PCIE_PHYx4_0:
        DEBUG ((DEBUG_ERROR, "ERROR: DEKEL FW download failed for x4 controller (PEG 060) PHY !!!\n"));
        ASSERT(FALSE);
        break;
      case CPU_SB_PID_PCIE_PHYx4_2:
        DEBUG ((DEBUG_ERROR, "ERROR: DEKEL FW download failed for x4 controller (PEG 062) PHY !!!\n"));
        ASSERT(FALSE);
        break;
      default:
        DEBUG ((DEBUG_ERROR, "ERROR: DEKEL FW download failed!!!\n"));
        break;
    }
    return FALSE;
  }
  return TRUE;
}

/**
  Initialize the Dekel in PEI

  @param[in]  RpIndex                The root port to be initialized (zero based).
  @param[in]  CpuPcieRpPreMemConfig  to access the PCIe Config related information
**/

VOID
EFIAPI
DekelInit (
  IN  UINT32                         RpIndex,
  IN  CPU_PCIE_RP_PREMEM_CONFIG     *CpuPcieRpPreMemConfig
  )
{
  UINT8              Index;
  CPU_SB_DEVICE_PID  CpuSbDevicePid;

  DEBUG ((DEBUG_INFO, "CpuPcieDekelInit Start\n"));

  CpuSbDevicePid = CpuPcieGetDekelSbPid (RpIndex);

  DEBUG ((DEBUG_INFO, "CpuSbDevicePid %x \n",CpuSbDevicePid));

  DEBUG ((DEBUG_INFO, "Dekel Programming: \n"));
  DEBUG ((DEBUG_VERBOSE, "********************************************************************\n"));

  if(GetCpuFamily() == CPUID_FULL_FAMILY_MODEL_ALDERLAKE_DT_HALO) {
    DEBUG ((DEBUG_INFO, "Dekel Programming for ADL S: \n"));
    for (Index = 0; Index < sizeof (DekelB0AdlsRegs) / (sizeof (DEKEL_REGS)); Index++) {
      CpuRegbarWrite32 (CpuSbDevicePid, DekelB0AdlsRegs [Index].RegOffset, DekelB0AdlsRegs [Index].Data32);
      DEBUG ((DEBUG_INFO, " RpIndex=%x Index=%x Address=0x%x Value=0x%x\n", RpIndex, Index, DekelB0AdlsRegs [Index].RegOffset, DekelB0AdlsRegs [Index].Data32));
    }
  } else {
    for (Index = 0; Index < sizeof (DekelB0Regs) / (sizeof (DEKEL_REGS)); Index++) {
      CpuRegbarWrite32 (CpuSbDevicePid, DekelB0Regs [Index].RegOffset, DekelB0Regs [Index].Data32);
      DEBUG ((DEBUG_INFO, "RpIndex=%x Index=%x Address=0x%x Value=0x%x\n", RpIndex, Index, DekelB0Regs [Index].RegOffset, DekelB0Regs [Index].Data32));
    }
  }
  //
  // If CpuPcieRpPreMemConfig->NewFom is set, Enable New FOM scheme 0x1FB0[2]
  //
  if (CpuPcieRpPreMemConfig->NewFom[RpIndex]) {
    CpuRegbarOr32 (CpuSbDevicePid, R_DEKEL_CMN_DIG_DWORD44, (UINT32)(B_DEKEL_CMN_DIG_DWORD44));
  }
  //
  // Program CDR relock for PEG60
  //
  if (CpuPcieRpPreMemConfig->CdrRelock[0]) {
    DEBUG ((DEBUG_INFO, "CdrRelock is Enabled for PEG60\n"));
    CpuRegbarAndThenOr32 ((CPU_SB_DEVICE_PID)CPU_SB_PID_PCIE_PHYx4_0, R_DEKEL_CMN_LANE_OFFSET, (UINT32)~(B_DEKEL_CMN_LANE_CDR_RELOCK), (V_DEKEL_CMN_LANE_CDR_RELOCK << B_DEKEL_CMN_LANE_CDR_RELOCK_OFFSET));
    CpuRegbarAndThenOr32 ((CPU_SB_DEVICE_PID)CPU_SB_PID_PCIE_PHYx4_2, R_DEKEL_CMN_LANE_OFFSET, (UINT32)~(B_DEKEL_CMN_LANE_CDR_RELOCK), (V_DEKEL_CMN_LANE_CDR_RELOCK << B_DEKEL_CMN_LANE_CDR_RELOCK_OFFSET));
  } else {
    DEBUG ((DEBUG_INFO, "CdrRelock is Disabled for PEG60\n"));
    CpuRegbarAnd32 ((CPU_SB_DEVICE_PID)CPU_SB_PID_PCIE_PHYx4_0, R_DEKEL_CMN_LANE_OFFSET, (UINT32)~(B_DEKEL_CMN_LANE_CDR_RELOCK));
    CpuRegbarAnd32 ((CPU_SB_DEVICE_PID)CPU_SB_PID_PCIE_PHYx4_2, R_DEKEL_CMN_LANE_OFFSET, (UINT32)~(B_DEKEL_CMN_LANE_CDR_RELOCK));
  }
  DEBUG ((DEBUG_VERBOSE, "********************************************************************\n"));

  DEBUG ((DEBUG_INFO, "CpuPcieDekelInit End\n"));
}

/**
  Read DEKEL Firmware Version

  @param[in]  CpuSbDevicePid   CPU SB Device Port ID

  @retval     UINT32         Dekel Firmware Version
**/

UINT32
EFIAPI
GetDekelFwVersion (
  IN  UINT32   RpIndex
)
{
  if (!IsSimicsEnvironment()) {
    UINT32             DekelFwVersion;
    CPU_SB_DEVICE_PID  CpuSbDevicePid;

    CpuSbDevicePid = CpuPcieGetDekelSbPid(RpIndex);
    DekelFwVersion = CpuRegbarRead32(CpuSbDevicePid, R_DEKEL_FW_VERSION_OFFSET);
    DEBUG((DEBUG_INFO, "DekelFwVersion %x\n", DekelFwVersion));
    return DekelFwVersion;
  }
  return 0;
}

/**
  Create Dekel Phy Port Map based on the Supported SKU

  @retval     UINT32         Dekel Port Map
**/

UINT32
EFIAPI
CpuPcieGetDekelPortMap (
  VOID
)
{
  UINT8                 DekelMap;

  DekelMap = 0;
  if (GetCpuSku () == EnumCpuTrad) {
    DekelMap |= BIT0; // PEG60
  } else if (GetCpuFamily() == CPUID_FULL_FAMILY_MODEL_ALDERLAKE_MOBILE) {
    DekelMap |= (BIT0 | BIT2); // PEG60/62
  }
  return DekelMap;
}

/**
  Check Dekel Phy Controller is enabled

  @param[in]             None

  @retval   TRUE         If PCIe Gen 4 Disable fuse is zero and Pcie set up option is enabled.
  @retval   FALSE        If PCIe Gen 4 Disable fuse is non-zero or Pcie setup option is disabled.
**/

BOOLEAN
EFIAPI
IsDekelPhyCtrlEnabled (
  VOID
)
{
  UINT32                             CapIdValue;

  CapIdValue = 0;

  if (GetCpuSku () == EnumCpuTrad) {
    return TRUE;
  } else {
    //
    // Check CapID
    //
    CapIdValue = PciSegmentRead32 (PCI_SEGMENT_LIB_ADDRESS(SA_SEG_NUM, SA_MC_BUS, SA_MC_DEV, SA_MC_FUN, R_SA_MC_CAPID0_C));
    DEBUG ((DEBUG_INFO, "CAP ID for Pcie Controller with Dekel phy %x\n", !(CapIdValue & B_SA_MC_CAPID0_C_PEGG4_DIS)));
    if (!(CapIdValue & B_SA_MC_CAPID0_C_PEGG4_DIS)) {
      return TRUE;
    }
  }
  return FALSE;
}
