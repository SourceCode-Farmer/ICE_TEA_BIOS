/** @file
  This file contains functions for HsPhy

@copyright
  INTEL CONFIDENTIAL
  Copyright 2020 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains an 'Intel Peripheral Driver' and is uniquely identified as
  "Intel Reference Module" and is licensed for Intel CPUs and chipsets under
  the terms of your license agreement with Intel or your vendor. This file may
  be modified by the user, subject to additional terms of the license agreement.

@par Specification Reference:
**/

#include <Library/PeiCpuPcieHsPhyInfoLib.h>
#include <Library/CpuPcieInfoFruLib.h>
#include <CpuSbInfo.h>
#include <Library/DebugLib.h>
#include <Library/PreSiliconEnvDetectLib.h>
#include <Library/CpuPlatformLib.h>
#include <Library/ConfigBlockLib.h>
#include <Library/CpuRegbarAccessLib.h>
#include <Library/PeiServicesLib.h>
#include <Library/PciSegmentLib.h>
#include <Register/SaRegsHostBridge.h>
#include <Library/PeiCpuPciePolicyLib.h>
#include <CpuPcieHob.h>

/**
  This function returns HS-Phy PID according to PCIe RootPort controller Index

  @param[in]  RpIndex          The root port to be initialized (zero based).

  @retval CPU_SB_DEVICE_PID    Returns PID for SBI Access
**/
CPU_SB_DEVICE_PID
CpuPcieGetHsPhySbPid (
  IN  UINT32   RpIndex
  )
{
  CPU_SB_DEVICE_PID  CpuSbDevicePid;

    switch (RpIndex) {
    case 1:
      CpuSbDevicePid = CPU_SB_PID_PCIE_PHYx16_0;
      break;
    case 2:
      CpuSbDevicePid = CPU_SB_PID_PCIE_PHYx16_1;
      break;
    default:
      ASSERT (FALSE);
      CpuSbDevicePid = 0;
      break;
  }
  return CpuSbDevicePid;
}

/**
  Read HsPhy FW download status

  @param[in]  RpIndex          RootPort Index

  @retval     BOOLEAN          Fw Download Completed TRUE or FALSE
**/

BOOLEAN
EFIAPI
HsPhyFwStatus (
  IN  UINT32   RpIndex
)
{
  if (!IsSimicsEnvironment()) {
    CPU_SB_DEVICE_PID  CpuSbDevicePid;

    CpuSbDevicePid = CpuPcieGetHsPhySbPid(RpIndex);
    if ((CpuRegbarRead32 (CpuSbDevicePid, R_HSPHY_RECIPE_VERSION)) == 0) {
      return FALSE;
    }
  }
  return TRUE;
}

/**
  Create HsPhy Port Map based on the Supported SKU

  @retval     UINT32         HsPhy Port Map
**/

UINT32
EFIAPI
CpuPcieGetHsPhyPortMap (
  VOID
)
{
  UINT8                 HsPhyMap;

  HsPhyMap = 0;
  if (GetCpuSku () == EnumCpuTrad) {
    HsPhyMap |= (BIT2 | BIT1);
  } else if ((GetCpuSku () == EnumCpuUlt) || (GetCpuSku () == EnumCpuUlx)) {
    HsPhyMap |= (BIT1); // PEG10
  }
  return HsPhyMap;
}

/**
  Check Hsphy Controller is enabled

  @param[in]             None

  @retval   TRUE         If PCIe Gen 5 Disable fuse is zero and Pcie set up options are enabled.
  @retval   FALSE        If PCIe Gen 5 Disable fuse is non zero or Pcie setup options are disabled.
**/

BOOLEAN
EFIAPI
IsHsPhyCtrlEnabled (
  VOID
)
{
  UINT8                             RpIndex;
  UINTN                             MaxCpuPciePortNum;
  CPU_PCIE_HOB                      *CpuPcieHob;

  if (GetCpuFamily() == CPUID_FULL_FAMILY_MODEL_ALDERLAKE_ATOM) {
    return FALSE;
  }

  CpuPcieHob = NULL;
  CpuPcieHob = (CPU_PCIE_HOB *)GetFirstGuidHob(&gCpuPcieHobGuid);
  MaxCpuPciePortNum = GetMaxCpuPciePortNum();
  DEBUG ((DEBUG_ERROR, "CpuPcieHob->RpEnabledMask %x\n", CpuPcieHob->RpEnabledMask));
  if (CpuPcieHob != NULL) {
    for (RpIndex = 0; RpIndex < MaxCpuPciePortNum; RpIndex++) {
      if (GetCpuPcieSipInfo (RpIndex) == PCIE_SIP17) {
        if ((CpuPcieHob->RpEnabledMask & (BIT0 << RpIndex))) {
          return TRUE;
        }
      }
    }
  }

  return FALSE;
}

/**
  Read HS-PHY Recipe Version

  @param[in]  RpIndex   RootPort Index

  @retval     UINT32    HS-Phy Recipe Version
**/

UINT32
GetHsPhyRecipeVersion (
  IN  UINT32   RpIndex
)
{
  if (!IsSimicsEnvironment()) {
    UINT32             HsPhyRecipeVersion;
    CPU_SB_DEVICE_PID  CpuSbDevicePid;

    CpuSbDevicePid = CpuPcieGetHsPhySbPid(RpIndex);
    HsPhyRecipeVersion = CpuRegbarRead32(CpuSbDevicePid, R_HSPHY_RECIPE_VERSION);
    DEBUG ((DEBUG_INFO, "HsPhyRecipeVersion %x\n", HsPhyRecipeVersion));
    return HsPhyRecipeVersion;
  }
  return 0;
}

/**
  Read HS-Phy Firmware Version

  @param[in]  RpIndex   RootPort Index

  @retval     UINT32    HS-Phy Firmware Version
**/

UINT32
EFIAPI
GetHsPhyFwVersion (
  IN  UINT32   RpIndex
)
{
  if (!IsSimicsEnvironment()) {
    UINT32             HsPhyFwVersion;
    CPU_SB_DEVICE_PID  CpuSbDevicePid;

    CpuSbDevicePid = CpuPcieGetHsPhySbPid(RpIndex);
    HsPhyFwVersion = CpuRegbarRead32(CpuSbDevicePid, R_HSPHY_FW_VERSION);
    DEBUG ((DEBUG_INFO, "HsPhyFwVersion %x\n", HsPhyFwVersion));
    return HsPhyFwVersion;
  }
  return 0;
}