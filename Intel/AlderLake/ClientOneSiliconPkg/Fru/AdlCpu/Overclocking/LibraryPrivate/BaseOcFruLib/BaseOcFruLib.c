/** @file
  This file contains Overclocking tolopolgy for specific generation.

@copyright
  INTEL CONFIDENTIAL
  Copyright 2020 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains an 'Intel Peripheral Driver' and is uniquely identified as
  "Intel Reference Module" and is licensed for Intel CPUs and chipsets under
  the terms of your license agreement with Intel or your vendor. This file may
  be modified by the user, subject to additional terms of the license agreement.

@par Specification
**/

#include <Library/DebugLib.h>
#include <Library/CpuPlatformLib.h>
#include <Library/CpuMailboxLib.h>
#include <OcMailbox.h>

/**
  This function return whether AVX2 and AVX512 voltage Guard Band supported.

  @retval TRUE  - AVX voltage Guard Band supported.
  @retval FALSE - AVX voltage Guard Band unsupported.
**/
BOOLEAN
IsAvxVoltageGuardBandSupport (
  VOID
  )
{
  return TRUE;
}

/**
  This function return whether Per Core HyperThread Disable supported.

  @retval TRUE  - Per Core HyperThread Disable supported.
  @retval FALSE - Per Core HyperThread Disable unsupported.
**/
BOOLEAN
IsPerCoreHtDisableSupport (
  VOID
  )
{
  return TRUE;
}

/**
  This function return whether L2Atom VF overrides supported.

  @retval TRUE  - L2Atom VF overrides supported.
  @retval FALSE - L2Atom VF overrides unsupported.
**/
BOOLEAN
IsAtomL2OcSupport (
  VOID
  )
{
  return TRUE;
}

/**
  This function return PerCore HT Disable Command.

  @param[out]  ReadCommand   PerCore HT Disable Read Command
  @param[out]  WriteCommand  PerCore HT Disable Write Command
**/
VOID
OcGetPerCoreHtDisableCmd (
  OUT PCODE_MAILBOX_INTERFACE *ReadCommand,
  OUT PCODE_MAILBOX_INTERFACE *WriteCommand
  )
{

  ///
  /// ADL PCODE mailbox 0x37
  ///
  ReadCommand->InterfaceData = 0;
  ReadCommand->Fields.Command = MAILBOX_OC_CMD_OC_INTERFACE;
  ReadCommand->Fields.Param1 = 0x0C;

  WriteCommand->InterfaceData = 0;
  WriteCommand->Fields.Command = MAILBOX_OC_CMD_OC_INTERFACE;
  WriteCommand->Fields.Param1 = 0x0D;
}

/**
  This function return TjMax Offset Command.

  @param[out]  ReadCommand   TjMax Offset Read Command
  @param[out]  WriteCommand  TjMax Offset Write Command
**/
VOID
OcGetTjMaxOffsetCmd (
  OUT PCODE_MAILBOX_INTERFACE *ReadCommand,
  OUT PCODE_MAILBOX_INTERFACE *WriteCommand
  )
{
  ///
  /// ADL PCODE mailbox SubCommand 0x4/0x5
  ///
  ReadCommand->InterfaceData = 0;
  ReadCommand->Fields.Command = MAILBOX_OC_CMD_OC_INTERFACE;
  ReadCommand->Fields.Param1 = MAILBOX_OC_SUBCMD_READ_TJ_MAX_OFFSET;

  WriteCommand->InterfaceData = 0;
  WriteCommand->Fields.Command = MAILBOX_OC_CMD_OC_INTERFACE;
  WriteCommand->Fields.Param1 = MAILBOX_OC_SUBCMD_WRITE_TJ_MAX_OFFSET;
}

/**
  This function return Realtime Memory Timing Enable/Disable Command.

  @param[out]  ReadCommand   Realtime Memory Timing Read Command
  @param[out]  WriteCommand  Realtime Memory Timing Write Command
**/
VOID
OcGetRealtimeMemoryTimingCmd (
  OUT PCODE_MAILBOX_INTERFACE *ReadCommand,
  OUT PCODE_MAILBOX_INTERFACE *WriteCommand
  )
{
  ///
  /// ADL PCODE mailbox SubCommand 0x0/0x1
  ///
  ReadCommand->InterfaceData = 0;
  ReadCommand->Fields.Command = MAILBOX_OC_CMD_OC_INTERFACE;
  ReadCommand->Fields.Param1 = MAILBOX_OC_SUBCMD_READ_OC_MISC_CONFIG;

  WriteCommand->InterfaceData = 0;
  WriteCommand->Fields.Command = MAILBOX_OC_CMD_OC_INTERFACE;
  WriteCommand->Fields.Param1 = MAILBOX_OC_SUBCMD_WRITE_OC_MISC_CONFIG;
}

/**
  This function return whether CPU BCLK OC Freq. supported.

  @retval TRUE  - CPU BCLK OC Freq. supported.
  @retval FALSE - CPU BCLK OC Freq. unsupported.
**/
BOOLEAN
IsCpuBclkOcFreqSupport (
  VOID
  )
{
  return TRUE;
}

/**
  This function return whether SOC BCLK OC Freq. supported.

  @retval TRUE  - SOC BCLK OC Freq. supported
  @retval FALSE - SOC BCLK OC Freq. unsupported
**/
BOOLEAN
IsSocBclkOcFreqSupport (
  VOID
  )
{
  return FALSE;
}

/**
  This function return CPU BCLK Freq. mailbox command.

  @param[out]  ReadCommand   CPU BCLK Freq. Read Command
  @param[out]  WriteCommand  CPU BCLK Freq. Write Command
**/
VOID
OcGetCpuBclkFreqCmd (
  OUT OC_MAILBOX_INTERFACE *ReadCommand,
  OUT OC_MAILBOX_INTERFACE *WriteCommand
  )
{
  ///
  /// ADL OC mailbox 0x22/0x23
  ///
  ReadCommand->InterfaceData = 0;
  ReadCommand->Fields.CommandCompletion = 0x22;

  WriteCommand->InterfaceData = 0;
  WriteCommand->Fields.CommandCompletion = 0x23;
}

/**
  This function return whether Core Ratio Extension mode is supported.

  @retval TRUE  - Core Ratio Extension mode supported.
  @retval FALSE - Core Ratio Extension mode unsupported.
**/
BOOLEAN
IsCoreRatioExtModeSupported (
  VOID
  )
{
  return TRUE;
}

/**
  This function return whether PVD Ratio Threshold Override is supported.

  @retval TRUE  - PVD Ratio Threshold Override supported.
  @retval FALSE - PVD Ratio Threshold Override unsupported.
**/
BOOLEAN
IsPvdRatioThresholdSupported (
  VOID
  )
{
  return TRUE;
}

/**
  This function return whether PVD Mode selection is supported.

  @retval TRUE  - PVD Ratio Mode supported.
  @retval FALSE - PVD Ratio Mode unsupported.
**/
BOOLEAN
IsPvdModeSupported (
  VOID
  )
{
  return FALSE;
}

/**
  This function return whether FLL Overclock mode is supported.

  @retval TRUE  - FLL Overclock supported.
  @retval FALSE - FLL Overclock unsupported.
**/
BOOLEAN
IsFllOverclockSupported (
  VOID
  )
{
  return TRUE;
}

/**
  This function return whether CEP (Current Excursion Protection) configuration supported or not.
  Note: ADL only support IA CAP and GT CEP.
        IA CEP  -- DATA[2:2] IA_CEP_DISABLE When set - Disables IA CEP
        GT CEP  -- DATA[3:3] GT_CEP_DISABLE When set - Disables GT CEP

  @retval TRUE  - CEP (Current Excursion Protection) configuration supported.
  @retval FALSE - CEP (Current Excursion Protection) configuration unsupported.
**/
BOOLEAN
IsOcCepConfigSupport (
  VOID
  )
{
  return TRUE;
}

/**
  This function return CEP (Current Excursion Protection) configuration data
  to control it's enable or disable.
  Note: ADL only support IA CAP and GT CEP.
        IA CEP  -- DATA[2:2] IA_CEP_DISABLE When set - Disables IA CEP
        GT CEP  -- DATA[3:3] GT_CEP_DISABLE When set - Disables GT CEP

  @param[in]      IaCepDisable       Disable IA CEP.
  @param[in]      GtCepDisable       Disable GT CEP.
  @param[in]      SaCepDisable       Disable SA CEP.
  @param[out]     ConfigData         CEP Config data.
**/
VOID
OcGetCepConfigData (
  IN  BOOLEAN IaCepDisable,
  IN  BOOLEAN GtCepDisable,
  IN  BOOLEAN SaCepDisable,
  OUT UINT32  *ConfigData
  )
{
  *ConfigData = (UINT32) (((IaCepDisable << 2) |
                          (GtCepDisable << 3)) & 0x0C);
}

/**
  This function return whether SIRP (SoC Iccmax Reactive Protector) configuration supported or not.

  @retval TRUE  - SIRP (SoC Iccmax Reactive Protector) configuration supported.
  @retval FALSE - SIRP (SoC Iccmax Reactive Protector) configuration unsupported.
**/
BOOLEAN
IsOcSirpConfigSupport (
  VOID
  )
{
  return FALSE;
}

/**
  This function return SIRP (SoC Iccmax Reactive Protector) configuration data
  to control it's enable or disable.

  @param[in]      IaSirpDisable      Disable IA SIRP.
  @param[out]     ConfigData         SIRP Config data.
**/
VOID
OcGetSirpConfigData (
  IN  BOOLEAN IaSirpDisable,
  OUT UINT32  *ConfigData
  )
{
  *ConfigData = 0;

  return;
}

/**
  This function return whether ITD (Inverse Temperature Dependency) Throttle configuration supported or not.

  @retval TRUE  - ITD (Inverse Temperature Dependency) Throttle configuration supported.
  @retval FALSE - ITD (Inverse Temperature Dependency) Throttle configuration unsupported.
**/
BOOLEAN
IsOcItdThrottleConfigSupport (
  VOID
  )
{
  return FALSE;
}

/**
  This function return ITD (Inverse Temperature Dependency) Throttle configuration data
  to control it's enable or disable.

  @param[in]      ItdThrottleDisable Disable ITD Throttle.
  @param[out]     ConfigData         ITD Throttle Config data.
**/
VOID
OcGetItdThrottleConfigData (
  IN  BOOLEAN ItdThrottleDisable,
  OUT UINT32  *ConfigData
  )
{
  *ConfigData = 0;

  return;
}

/**
  This function return whether Sa Pll Frequency Override is supported.

  @retval TRUE  - Sa Pll Frequency Override supported.
  @retval FALSE - Sa Pll Frequency Override unsupported.
**/
BOOLEAN
IsSaPllFreqOverrideSupported (
  VOID
  )
{
  return TRUE;
}

/**
  This function return whether TSC HW Fixup disable is supported.

  @retval TRUE  - TSC HW Fixup disable supported.
  @retval FALSE - TSC HW Fixup disable unsupported.
**/
BOOLEAN
IsCoreHwFixupDisableSupported (
  VOID
  )
{
  return TRUE;
}

/**
  This function return whether ICCMAX override supported.

  @retval TRUE  - ICCMAX override supported.
  @retval FALSE - ICCMAX override unsupported.
**/
BOOLEAN
IsIccMaxOcOverrideSupported (
  VOID
  )
{
  return TRUE;
}

/**
  This function return whether OCMB TVB Config Command for Temperature Thresholds and Down bins is supported.

  @retval TRUE  - TVB Config Temperature and Down bins is supported.
  @retval FALSE - TVB Config Temperature and Down bins is unsupported.
**/
BOOLEAN
IsTvbDownBinsTempThresholdSupported (
  VOID
  )
{
  return TRUE;
}
