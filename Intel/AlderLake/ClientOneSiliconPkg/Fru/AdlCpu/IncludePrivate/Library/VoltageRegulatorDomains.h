/** @file
  Header file which contains Voltage regulator topology data and overrides for domains like SA, IA, Ring, GT, Fivr.

@copyright
  INTEL CONFIDENTIAL
  Copyright 2019 - 2020 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains an 'Intel Peripheral Driver' and is uniquely identified as
  "Intel Reference Module" and is licensed for Intel CPUs and chipsets under
  the terms of your license agreement with Intel or your vendor. This file may
  be modified by the user, subject to additional terms of the license agreement.

@par Specification Reference:
**/
#ifndef _VOLTAGE_REGULATOR_DOMAINS_H_
#define _VOLTAGE_REGULATOR_DOMAINS_H_

#include <CpuGenInfoFruLib.h>  //CPU_IDENTIFIER structure comes from this header. so we need this include

///
///  VR Domain Definitions
///
#define CPU_VR_DOMAIN_IA           0x0
#define CPU_VR_DOMAIN_GT           0x1
#define CPU_VR_DOMAIN_SA           0x2

///
/// VR Topology Data structure containing the address and en/dis information
/// for all SVID VR domains
///
typedef union VR_TOPOLOGY_DATA {
  UINT32 VrTopology;              ///< All bit fields as a 32-bit value.
  ///
  /// Individual bit fields.
  ///
  struct {
    UINT32 Reserved               : 1;  ///< Reserved
    UINT32 Vcc1p05Cpu             : 1;  ///< VCC1p05_CPU_SOURCE_IS_PLATFORM, 0 - 1p05CPU is sourced from PCH, 1 - 1p05CPU is sourced from a platform rail
    UINT32 Reserved1              : 1;  ///< Reserved
    UINT32 VccStPgExist           : 1;  ///< Vcc ST PG Exist. 0 - No VccST PG exist, 1 - VccST PG exist
    UINT32 Reserved2              : 1;  ///< Reserved
    UINT32 VccInAuxLpLevel        : 1;  ///< VccIn Aux Lp Level. 0 - VCCIN_AUX voltage is 1.8V, 1 - VCCIN_AUX voltage is 1.65V
    UINT32 Reserved3              : 1;  ///< Reserved
    UINT32 VccInAuxImonDisable    : 1;  ///< VccIn AUX Imon. 0 - Enable, 1 - Disable
    UINT32 VrIaAddress            : 4;  ///< IA VR address
    UINT32 VrIaSvidType           : 1;  ///< IA VR Type. 1 - No SVID VR, 0 - SVID VR
    UINT32 VrGtAddress            : 4;  ///< GT VR address
    UINT32 VrGtSvidType           : 1;  ///< GT VR Type. 1 - No SVID VR, 0 - SVID VR
    UINT32 SetIaVrVid             : 1;  ///< Set IA VR OFFSET register to allow higher than 1.52V
    UINT32 PlatformType           : 1;  ///< Platform type info
    UINT32 VccInAux_Imon_Add_Sel  : 1;  ///< SVID rail address would be used to read the VccInAux rail current
    UINT32 VrSaAddress            : 4;  ///< SA VR address. Only in ADL M
    UINT32 VrSaSvidType           : 1;  ///< SA VR Type. 1 - No SVID VR, 0 - SVID VR
    UINT32 VrVccAnaAddress        : 4;  ///< VCC ANA VR address. Only in ADL M
    UINT32 VrVccAnaSvidType       : 1;  ///< VCC ANA VR Type. 1 - No SVID VR, 0 - SVID VR
    UINT32 PsysDisable            : 1;  ///< Imon Enable. 1 - Disable, 0 - Enable
  } Fields;
} VR_TOPOLOGY_DATA;

///
/// VR Override table structure
///
typedef struct {
  CPU_IDENTIFIER  CpuIdentifier;
  UINT16          IaIccMax;
  UINT16          GtIccMax;
  UINT16          SaIccMax;
  UINT16          IaTdclimit;
  UINT16          IaTdclimitWithIrms;
  UINT32          IaTdcTimelimitIrms;
  UINT16          GtTdclimit;
  UINT16          SaTdclimit;
  UINT16          IaAcLoadLine;
  UINT16          IaDcLoadLine;
  UINT16          GtAcLoadLine;
  UINT16          GtDcLoadLine;
  UINT16          SaAcLoadLine;
  UINT16          SaDcLoadLine;
} CPU_VR_OVERRIDE_TABLE;

#endif // _VOLTAGE_REGULATOR_DOMAINS_H_
