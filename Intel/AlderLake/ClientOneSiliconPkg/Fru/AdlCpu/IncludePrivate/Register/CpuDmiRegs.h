/** @file
  Register names for DMI and OP-DMI

Conventions:

  - Register definition format:
    Prefix_[GenerationName]_[ComponentName]_SubsystemName_RegisterSpace_RegisterName
  - Prefix:
    Definitions beginning with "R_" are registers
    Definitions beginning with "B_" are bits within registers
    Definitions beginning with "V_" are meaningful values within the bits
    Definitions beginning with "S_" are register size
    Definitions beginning with "N_" are the bit position
  - [GenerationName]:
    Three letter acronym of the generation is used (e.g. SKL,KBL,CNL etc.).
    Register name without GenerationName applies to all generations.
  - [ComponentName]:
    This field indicates the component name that the register belongs to (e.g. PCH, SA etc.)
  - SubsystemName:
    This field indicates the subsystem name of the component that the register belongs to
    (e.g. PCIE, USB, SATA, GPIO, PMC etc.).
  - RegisterSpace:
    MEM - MMIO space register of subsystem.
    IO  - IO space register of subsystem.
    PCR - Private configuration register of subsystem.
    CFG - PCI configuration space register of subsystem.
  - RegisterName:
    Full register name.

@copyright
  INTEL CONFIDENTIAL
  Copyright 2019 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains an 'Intel Peripheral Driver' and is uniquely identified as
  "Intel Reference Module" and is licensed for Intel CPUs and chipsets under
  the terms of your license agreement with Intel or your vendor. This file may
  be modified by the user, subject to additional terms of the license agreement.

@par Specification Reference:
**/
#ifndef _CPU_DMI_REGS_H_
#define _CPU_DMI_REGS_H_

//
// DMI Chipset Configuration Registers (PID:DMI)
//

//
// VC Configuration (Common)
//
#define B_CPU_DMI_V0CTL_EN               BIT31
#define B_CPU_DMI_V0CTL_ID               (7 << 24)                   ///< Bit[26:24]
#define N_CPU_DMI_V0CTL_ID               24
#define V_CPU_DMI_V0CTL_ETVM_MASK        0xFC00
#define V_CPU_DMI_V0CTL_TVM_MASK         0x7E
#define B_CPU_DMI_V0STS_NP               BIT1
#define B_CPU_DMI_V1CTL_EN               BIT31
#define B_CPU_DMI_V1CTL_ID               (0x0F << 24)                ///< Bit[27:24]
#define N_CPU_DMI_V1CTL_ID               24
#define V_CPU_DMI_V1CTL_ETVM_MASK        0xFC00
#define V_CPU_DMI_V1CTL_TVM_MASK         0xFE
#define B_CPU_DMI_V1STS_NP               BIT1

//
// Internal Link Configuration (DMI Only)
//
#define R_CPU_DMI_LCAP                   0x004C                      ///< Link Capabilities
#define B_CPU_DMI_LCAP_EL1               (BIT17 | BIT16 | BIT15)     ///< L1 Exit Latency
#define N_CPU_DMI_LCAP_EL1               15                          ///< L1 Exit Latency
#define V_CPU_DMI_LCAP_EL1_8US_16US      0x4                         ///< L1 Exit Latency = 8us to less than 16us
#define B_CPU_DMI_LCAP_EL0               (BIT14 | BIT13 | BIT12)     ///< L0 Exit Latency
#define N_CPU_DMI_LCAP_EL0               12                          ///< L0 Exit Latency
#define V_CPU_DMI_LCAP_EL0_256NS_512NS   0x3                         ///< L0 Exit Latency = 256ns to less than 512ns
#define V_CPU_DMI_LCAP_EL0_512NS_1US     0x4                         ///< L0 Exit Latency = 512ns to less than 1us
#define B_CPU_DMI_LCAP_APMS              (BIT11 | BIT10)             ///< L0 is supported on DMI
#define B_CPU_DMI_LCAP_MLW               0x000003F0
#define B_CPU_DMI_LCAP_MLS               0x0000000F


//
// DMI Source Decode PCRs (Common)
//
#define R_CPU_DMI_PCIEPAR1E         0x0700                ///< PCIE Port IOxAPIC Range 1 Enable
#define R_CPU_DMI_PCIEPAR2E         0x0704                ///< PCIE Port IOxAPIC Range 2 Enable
#define R_CPU_DMI_PCIEPAR3E         0x0708                ///< PCIE Port IOxAPIC Range 3 Enable
#define R_CPU_DMI_PCIEPAR4E         0x070C                ///< PCIE Port IOxAPIC Range 4 Enable
#define R_CPU_DMI_PCIEPAR1DID       0x0710                ///< PCIE Port IOxAPIC Range 1 Destination ID
#define R_CPU_DMI_PCIEPAR2DID       0x0714                ///< PCIE Port IOxAPIC Range 2 Destination ID
#define R_CPU_DMI_PCIEPAR3DID       0x0718                ///< PCIE Port IOxAPIC Range 3 Destination ID
#define R_CPU_DMI_PCIEPAR4DID       0x071C                ///< PCIE Port IOxAPIC Range 4 Destination ID
#define R_CPU_DMI_P2SBIOR           0x0720                ///< P2SB IO Range
#define R_CPU_DMI_TTTBARB           0x0724                ///< Thermal Throttling BIOS Assigned Thermal Base Address
#define R_CPU_DMI_TTTBARBH          0x0728                ///< Thermal Throttling BIOS Assigned Thermal Base High Address
#define R_CPU_DMI_LPCLGIR1          0x0730                ///< LPC Generic I/O Range 1
#define R_CPU_DMI_LPCLGIR2          0x0734                ///< LPC Generic I/O Range 2
#define R_CPU_DMI_LPCLGIR3          0x0738                ///< LPC Generic I/O Range 3
#define R_CPU_DMI_LPCLGIR4          0x073C                ///< LPC Generic I/O Range 4
#define R_CPU_DMI_LPCGMR            0x0740                ///< LPC Generic Memory Range
#define R_CPU_DMI_SEGIR             0x07BC                ///< Second ESPI Generic I/O Range
#define R_CPU_DMI_SEGMR             0x07C0                ///< Second ESPI Generic Memory Range
#define R_CPU_DMI_LPCBDE            0x0744                ///< LPC BIOS Decode Enable
#define R_CPU_DMI_UCPR              0x0748                ///< uCode Patch Region
#define B_CPU_DMI_UCPR_UPRE         BIT0                  ///< uCode Patch Region Enable
#define R_CPU_DMI_GCS               0x074C                ///< Generic Control and Status
#define B_CPU_DMI_RPRDID            0xFFFF0000            ///< RPR Destination ID
#define B_CPU_DMI_BBS               BIT10                 ///< Boot BIOS Strap
#define B_CPU_DMI_RPR               BIT11                 ///< Reserved Page Route
#define B_CPU_DMI_BILD              BIT0                  ///< BIOS Interface Lock-Down
#define R_CPU_DMI_IOT1              0x0750                ///< I/O Trap Register 1
#define R_CPU_DMI_IOT2              0x0758                ///< I/O Trap Register 2
#define R_CPU_DMI_IOT3              0x0760                ///< I/O Trap Register 3
#define R_CPU_DMI_IOT4              0x0768                ///< I/O Trap Register 4
#define R_CPU_DMI_LPCIOD            0x0770                ///< LPC I/O Decode Ranges
#define R_CPU_DMI_LPCIOE            0x0774                ///< LPC I/O Enables
#define R_CPU_DMI_TCOBASE           0x0778                ///< TCO Base Address
#define B_CPU_DMI_TCOBASE_TCOBA     0xFFE0                ///< TCO Base Address Mask
#define R_CPU_DMI_GPMR1             0x077C                ///< General Purpose Memory Range 1
#define R_CPU_DMI_GPMR1DID          0x0780                ///< General Purpose Memory Range 1 Destination ID
#define R_CPU_DMI_GPMR2             0x0784                ///< General Purpose Memory Range 2
#define R_CPU_DMI_GPMR2DID          0x0788                ///< General Purpose Memory Range 2 Destination ID
#define R_CPU_DMI_GPMR3             0x078C                ///< General Purpose Memory Range 3
#define R_CPU_DMI_GPMR3DID          0x0790                ///< General Purpose Memory Range 3 Destination ID
#define R_CPU_DMI_GPIOR1            0x0794                ///< General Purpose I/O Range 1
#define R_CPU_DMI_GPIOR1DID         0x0798                ///< General Purpose I/O Range 1 Destination ID
#define R_CPU_DMI_GPIOR2            0x079C                ///< General Purpose I/O Range 2
#define R_CPU_DMI_GPIOR2DID         0x07A0                ///< General Purpose I/O Range 2 Destination ID
#define R_CPU_DMI_GPIOR3            0x07A4                ///< General Purpose I/O Range 3
#define R_CPU_DMI_GPIOR3DID         0x07A8                ///< General Purpose I/O Range 3 Destination ID

#endif
