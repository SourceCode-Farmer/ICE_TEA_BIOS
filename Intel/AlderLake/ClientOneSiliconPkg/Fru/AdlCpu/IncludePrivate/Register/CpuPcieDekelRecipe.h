/** @file
  This file contains register offsets and values for Dekel PCIe Configuration

@copyright
  INTEL CONFIDENTIAL
  Copyright 2020 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains an 'Intel Peripheral Driver' and is uniquely identified as
  "Intel Reference Module" and is licensed for Intel CPUs and chipsets under
  the terms of your license agreement with Intel or your vendor. This file may
  be modified by the user, subject to additional terms of the license agreement.

@par Specification Reference:
**/

#ifndef _CPU_PCIE_DEKEL_REGS_H_
#define _CPU_PCIE_DEKEL_REGS_H_

#define R_DEKEL_CL_ANA_CMN_ANA_DWORD0                                           0x1F00
#define V_DEKEL_CL_ANA_CMN_ANA_DWORD0                                           0xFC90FC5

#define R_DEKEL_CL_ANA_CMN_ANA_DWORD1                                           0x1F04
#define V_DEKEL_CL_ANA_CMN_ANA_DWORD1                                           0xFFFFFFFE

#define R_DEKEL_CL_ANA_CMN_ANA_DWORD2                                           0x1F08
#define V_DEKEL_CL_ANA_CMN_ANA_DWORD2                                           0x0

#define R_DEKEL_CL_ANA_CMN_ANA_DWORD28                                          0x1F70
#define V_DEKEL_CL_ANA_CMN_ANA_DWORD28                                          0x80018532

#define R_DEKEL_CL_ANA_CMN_ANA_DWORD3                                           0x1F0C
#define V_DEKEL_CL_ANA_CMN_ANA_DWORD3                                           0x74B0743

#define R_DEKEL_CL_ANA_CMN_ANA_DWORD4                                           0x1F10
#define V_DEKEL_CL_ANA_CMN_ANA_DWORD4                                           0x5007F

#define R_DEKEL_CL_ANA_CMN_ANA_DWORD5                                           0x1F14
#define V_DEKEL_CL_ANA_CMN_ANA_DWORD5                                           0x1FBF

#define R_DEKEL_CL_ANA_CMN_ANA_DWORD6                                           0x1F18
#define V_DEKEL_CL_ANA_CMN_ANA_DWORD6                                           0x74F0747

#define R_DEKEL_CL_ANA_CMN_ANA_DWORD7                                           0x1F1C
#define V_DEKEL_CL_ANA_CMN_ANA_DWORD7                                           0x1FBF

#define R_DEKEL_CL_ANA_CMN_ANA_DWORD8                                           0x1F20
#define V_DEKEL_CL_ANA_CMN_ANA_DWORD8                                           0xC3C3

#define R_DEKEL_CL_ANA_CMN_ANA_DWORD9                                           0x1F24
#define V_DEKEL_CL_ANA_CMN_ANA_DWORD9                                           0x92B0753

#define R_DEKEL_CL_DIG_CMN_DIG_DWORD26                                          0xF68
#define V_DEKEL_CL_DIG_CMN_DIG_DWORD26                                          0xC3C3

#define R_DEKEL_CL_DIG_CMN_DIG_DWORD38                                          0xF98
#define V_DEKEL_CL_DIG_CMN_DIG_DWORD38                                          0xD6C0

#define R_DEKEL_CL_DIG_CMN_DIG_DWORD43                                          0xFAC
#define V_DEKEL_CL_DIG_CMN_DIG_DWORD43                                          0x3208C

#define R_DEKEL_CL_DIG_CMN_DIG_DWORD44                                          0xFB0
#define V_DEKEL_CL_DIG_CMN_DIG_DWORD44                                          0x937
#define V_DEKEL_CL_DIG_CMN_DIG_DWORD44_B0                                       0x1F710937

#define R_DEKEL_CL_DIG_CMN_DIG_DWORD51                                          0xFCC
#define V_DEKEL_CL_DIG_CMN_DIG_DWORD51                                          0xF21C0F2

#define R_DEKEL_CL_DIG_CMN_DIG_DWORD52                                          0xFD0
#define V_DEKEL_CL_DIG_CMN_DIG_DWORD52                                          0x0
#define V_DEKEL_CL_DIG_CMN_DIG_DWORD52_B0                                       0x80018532

#define R_DEKEL_CL_DIG_CMN_DIG_DWORD6                                           0xF18
#define V_DEKEL_CL_DIG_CMN_DIG_DWORD6                                           0x1

#define R_DEKEL_CL_DIG_CMN_DIG_DWORD53                                          0xFD4
#define V_DEKEL_CL_DIG_CMN_DIG_DWORD53                                          0xD0F

#define R_DEKEL_CL_DIG_CMN_DIG_DWORD54                                          0xFD8
#define V_DEKEL_CL_DIG_CMN_DIG_DWORD54                                          0x3A202FA0

#define R_DEKEL_CL_UC_CMN_UC_DWORD0                                             0x3F00
#define V_DEKEL_CL_UC_CMN_UC_DWORD0                                             0x200B400

#define R_DEKEL_CL_UC2_CMN_UC_DWORD0                                            0x3F80
#define V_DEKEL_CL_UC2_CMN_UC_DWORD0                                            0x200B400

#define R_DEKEL_PCS_LANE0_PCS_DWORD3                                            0xC
#define V_DEKEL_PCS_LANE0_PCS_DWORD3                                            0x3A00407

#define R_DEKEL_PCS_LANE1_PCS_DWORD3                                            0x100C
#define V_DEKEL_PCS_LANE1_PCS_DWORD3                                            0x3A00407

#define R_DEKEL_PCS_LANE2_PCS_DWORD3                                            0x200C
#define V_DEKEL_PCS_LANE2_PCS_DWORD3                                            0x3A00407

#define R_DEKEL_PCS_LANE3_PCS_DWORD3                                            0x300C
#define V_DEKEL_PCS_LANE3_PCS_DWORD3                                            0x3A00407

#define R_DEKEL_PMD_LANE_CDR_LANE0_CDR_L0S_CONFIG                               0xC84
#define V_DEKEL_PMD_LANE_CDR_LANE0_CDR_L0S_CONFIG                               0xA73

#define R_DEKEL_PMD_LANE_CDR_LANE0_CDR_SAVE_RESTORE0                            0xC70
#define V_DEKEL_PMD_LANE_CDR_LANE0_CDR_SAVE_RESTORE0                            0x276CE3FB

#define R_DEKEL_PMD_LANE_CDR_LANE0_CFG13                                        0xC48
#define V_DEKEL_PMD_LANE_CDR_LANE0_CFG13                                        0x2E00017C

#define R_DEKEL_PMD_LANE_CDR_LANE1_CDR_L0S_CONFIG                               0x1C84
#define V_DEKEL_PMD_LANE_CDR_LANE1_CDR_L0S_CONFIG                               0xA73

#define R_DEKEL_PMD_LANE_CDR_LANE1_CDR_SAVE_RESTORE0                            0x1C70
#define V_DEKEL_PMD_LANE_CDR_LANE1_CDR_SAVE_RESTORE0                            0x276CE3FB

#define R_DEKEL_PMD_LANE_CDR_LANE1_CFG13                                        0x1C48
#define V_DEKEL_PMD_LANE_CDR_LANE1_CFG13                                        0x2E00017C

#define R_DEKEL_PMD_LANE_CDR_LANE2_CDR_L0S_CONFIG                               0x2C84
#define V_DEKEL_PMD_LANE_CDR_LANE2_CDR_L0S_CONFIG                               0xA73

#define R_DEKEL_PMD_LANE_CDR_LANE2_CDR_SAVE_RESTORE0                            0x2C70
#define V_DEKEL_PMD_LANE_CDR_LANE2_CDR_SAVE_RESTORE0                            0x276CE3FB

#define R_DEKEL_PMD_LANE_CDR_LANE2_CFG13                                        0x2C48
#define V_DEKEL_PMD_LANE_CDR_LANE2_CFG13                                        0x2E00017C

#define R_DEKEL_PMD_LANE_CDR_LANE3_CDR_L0S_CONFIG                               0x3C84
#define V_DEKEL_PMD_LANE_CDR_LANE3_CDR_L0S_CONFIG                               0xA73

#define R_DEKEL_PMD_LANE_CDR_LANE3_CDR_SAVE_RESTORE0                            0x3C70
#define V_DEKEL_PMD_LANE_CDR_LANE3_CDR_SAVE_RESTORE0                            0x276CE3FB

#define R_DEKEL_PMD_LANE_CDR_LANE3_CFG13                                        0x3C48
#define V_DEKEL_PMD_LANE_CDR_LANE3_CFG13                                        0x2E00017C

#define R_DEKEL_PCS_PMD_LANE0_RX_TMRS                                           0x150
#define V_DEKEL_PCS_PMD_LANE0_RX_TMRS                                           0x7F9FF625

#define R_DEKEL_PCS_PMD_LANE1_RX_TMRS                                           0x1150
#define V_DEKEL_PCS_PMD_LANE1_RX_TMRS                                           0x7F9FF625

#define R_DEKEL_PCS_PMD_LANE2_RX_TMRS                                           0x2150
#define V_DEKEL_PCS_PMD_LANE2_RX_TMRS                                           0x7F9FF625

#define R_DEKEL_PCS_PMD_LANE3_RX_TMRS                                           0x3150
#define V_DEKEL_PCS_PMD_LANE3_RX_TMRS                                           0x7F9FF625

#define R_DEKEL_PMD_LANE1_CDR_FAST_FLT_CFG                                      0x0C10
#define V_DEKEL_PMD_LANE1_CDR_FAST_FLT_CFG                                      0xE36FEFE

#define R_DEKEL_PMD_LANE2_CDR_FAST_FLT_CFG                                      0x1C10
#define V_DEKEL_PMD_LANE2_CDR_FAST_FLT_CFG                                      0xE36FEFE

#define R_DEKEL_PMD_LANE3_CDR_FAST_FLT_CFG                                      0x2C10
#define V_DEKEL_PMD_LANE3_CDR_FAST_FLT_CFG                                      0xE36FEFE

#define R_DEKEL_PMD_LANE4_CDR_FAST_FLT_CFG                                      0x3C10
#define V_DEKEL_PMD_LANE4_CDR_FAST_FLT_CFG                                      0xE36FEFE

typedef struct {
   UINT32     RegOffset;
   UINT32     Data32;
} DEKEL_REGS;

GLOBAL_REMOVE_IF_UNREFERENCED static DEKEL_REGS DekelB0Regs[] = {
  {R_DEKEL_CL_ANA_CMN_ANA_DWORD0, V_DEKEL_CL_ANA_CMN_ANA_DWORD0},
  {R_DEKEL_CL_ANA_CMN_ANA_DWORD1, V_DEKEL_CL_ANA_CMN_ANA_DWORD1},
  {R_DEKEL_CL_ANA_CMN_ANA_DWORD2, V_DEKEL_CL_ANA_CMN_ANA_DWORD2},
  {R_DEKEL_CL_ANA_CMN_ANA_DWORD28, V_DEKEL_CL_ANA_CMN_ANA_DWORD28},
  {R_DEKEL_CL_ANA_CMN_ANA_DWORD3, V_DEKEL_CL_ANA_CMN_ANA_DWORD3},
  {R_DEKEL_CL_ANA_CMN_ANA_DWORD4, V_DEKEL_CL_ANA_CMN_ANA_DWORD4},
  {R_DEKEL_CL_ANA_CMN_ANA_DWORD5, V_DEKEL_CL_ANA_CMN_ANA_DWORD5},
  {R_DEKEL_CL_ANA_CMN_ANA_DWORD6, V_DEKEL_CL_ANA_CMN_ANA_DWORD6},
  {R_DEKEL_CL_ANA_CMN_ANA_DWORD7, V_DEKEL_CL_ANA_CMN_ANA_DWORD7},
  {R_DEKEL_CL_ANA_CMN_ANA_DWORD8, V_DEKEL_CL_ANA_CMN_ANA_DWORD8},
  {R_DEKEL_CL_ANA_CMN_ANA_DWORD9, V_DEKEL_CL_ANA_CMN_ANA_DWORD9},
  {R_DEKEL_CL_DIG_CMN_DIG_DWORD26, V_DEKEL_CL_DIG_CMN_DIG_DWORD26},
  {R_DEKEL_CL_DIG_CMN_DIG_DWORD38, V_DEKEL_CL_DIG_CMN_DIG_DWORD38},
  {R_DEKEL_CL_DIG_CMN_DIG_DWORD43, V_DEKEL_CL_DIG_CMN_DIG_DWORD43},
  {R_DEKEL_CL_DIG_CMN_DIG_DWORD44, V_DEKEL_CL_DIG_CMN_DIG_DWORD44_B0},
  {R_DEKEL_CL_DIG_CMN_DIG_DWORD51, V_DEKEL_CL_DIG_CMN_DIG_DWORD51},
  {R_DEKEL_CL_DIG_CMN_DIG_DWORD52, V_DEKEL_CL_DIG_CMN_DIG_DWORD52_B0},
  {R_DEKEL_CL_DIG_CMN_DIG_DWORD53, V_DEKEL_CL_DIG_CMN_DIG_DWORD53},
  {R_DEKEL_CL_DIG_CMN_DIG_DWORD54, V_DEKEL_CL_DIG_CMN_DIG_DWORD54},
  {R_DEKEL_CL_UC_CMN_UC_DWORD0, V_DEKEL_CL_UC_CMN_UC_DWORD0},
  {R_DEKEL_CL_UC2_CMN_UC_DWORD0, V_DEKEL_CL_UC2_CMN_UC_DWORD0},
  {R_DEKEL_PCS_LANE0_PCS_DWORD3, V_DEKEL_PCS_LANE0_PCS_DWORD3},
  {R_DEKEL_PCS_LANE1_PCS_DWORD3, V_DEKEL_PCS_LANE1_PCS_DWORD3},
  {R_DEKEL_PCS_LANE2_PCS_DWORD3, V_DEKEL_PCS_LANE2_PCS_DWORD3},
  {R_DEKEL_PCS_LANE3_PCS_DWORD3, V_DEKEL_PCS_LANE3_PCS_DWORD3},
  {R_DEKEL_PCS_PMD_LANE0_RX_TMRS, V_DEKEL_PCS_PMD_LANE0_RX_TMRS},
  {R_DEKEL_PCS_PMD_LANE1_RX_TMRS, V_DEKEL_PCS_PMD_LANE1_RX_TMRS},
  {R_DEKEL_PCS_PMD_LANE2_RX_TMRS, V_DEKEL_PCS_PMD_LANE2_RX_TMRS},
  {R_DEKEL_PCS_PMD_LANE3_RX_TMRS, V_DEKEL_PCS_PMD_LANE3_RX_TMRS},
  {R_DEKEL_PMD_LANE1_CDR_FAST_FLT_CFG, V_DEKEL_PMD_LANE1_CDR_FAST_FLT_CFG},
  {R_DEKEL_PMD_LANE2_CDR_FAST_FLT_CFG, V_DEKEL_PMD_LANE2_CDR_FAST_FLT_CFG},
  {R_DEKEL_PMD_LANE3_CDR_FAST_FLT_CFG, V_DEKEL_PMD_LANE3_CDR_FAST_FLT_CFG},
  {R_DEKEL_PMD_LANE4_CDR_FAST_FLT_CFG, V_DEKEL_PMD_LANE4_CDR_FAST_FLT_CFG}
};

GLOBAL_REMOVE_IF_UNREFERENCED static DEKEL_REGS DekelB0AdlsRegs[] = {
  {R_DEKEL_CL_ANA_CMN_ANA_DWORD0, V_DEKEL_CL_ANA_CMN_ANA_DWORD0},
  {R_DEKEL_CL_ANA_CMN_ANA_DWORD1, V_DEKEL_CL_ANA_CMN_ANA_DWORD1},
  {R_DEKEL_CL_ANA_CMN_ANA_DWORD2, V_DEKEL_CL_ANA_CMN_ANA_DWORD2},
  {R_DEKEL_CL_ANA_CMN_ANA_DWORD28, V_DEKEL_CL_ANA_CMN_ANA_DWORD28},
  {R_DEKEL_CL_ANA_CMN_ANA_DWORD3, V_DEKEL_CL_ANA_CMN_ANA_DWORD3},
  {R_DEKEL_CL_ANA_CMN_ANA_DWORD4, V_DEKEL_CL_ANA_CMN_ANA_DWORD4},
  {R_DEKEL_CL_ANA_CMN_ANA_DWORD5, V_DEKEL_CL_ANA_CMN_ANA_DWORD5},
  {R_DEKEL_CL_ANA_CMN_ANA_DWORD6, V_DEKEL_CL_ANA_CMN_ANA_DWORD6},
  {R_DEKEL_CL_ANA_CMN_ANA_DWORD7, V_DEKEL_CL_ANA_CMN_ANA_DWORD7},
  {R_DEKEL_CL_ANA_CMN_ANA_DWORD8, V_DEKEL_CL_ANA_CMN_ANA_DWORD8},
  {R_DEKEL_CL_ANA_CMN_ANA_DWORD9, V_DEKEL_CL_ANA_CMN_ANA_DWORD9},
  {R_DEKEL_CL_DIG_CMN_DIG_DWORD26, V_DEKEL_CL_DIG_CMN_DIG_DWORD26},
  {R_DEKEL_CL_DIG_CMN_DIG_DWORD38, V_DEKEL_CL_DIG_CMN_DIG_DWORD38},
  {R_DEKEL_CL_DIG_CMN_DIG_DWORD43, V_DEKEL_CL_DIG_CMN_DIG_DWORD43},
  {R_DEKEL_CL_DIG_CMN_DIG_DWORD44, V_DEKEL_CL_DIG_CMN_DIG_DWORD44_B0},
  {R_DEKEL_CL_DIG_CMN_DIG_DWORD51, V_DEKEL_CL_DIG_CMN_DIG_DWORD51},
  {R_DEKEL_CL_DIG_CMN_DIG_DWORD52, V_DEKEL_CL_DIG_CMN_DIG_DWORD52_B0},
  {R_DEKEL_CL_DIG_CMN_DIG_DWORD53, V_DEKEL_CL_DIG_CMN_DIG_DWORD53},
  {R_DEKEL_CL_DIG_CMN_DIG_DWORD54, V_DEKEL_CL_DIG_CMN_DIG_DWORD54},
  {R_DEKEL_CL_UC_CMN_UC_DWORD0, V_DEKEL_CL_UC_CMN_UC_DWORD0},
  {R_DEKEL_CL_UC2_CMN_UC_DWORD0, V_DEKEL_CL_UC2_CMN_UC_DWORD0},
  {R_DEKEL_PCS_LANE0_PCS_DWORD3, V_DEKEL_PCS_LANE0_PCS_DWORD3},
  {R_DEKEL_PCS_LANE1_PCS_DWORD3, V_DEKEL_PCS_LANE1_PCS_DWORD3},
  {R_DEKEL_PCS_LANE2_PCS_DWORD3, V_DEKEL_PCS_LANE2_PCS_DWORD3},
  {R_DEKEL_PCS_LANE3_PCS_DWORD3, V_DEKEL_PCS_LANE3_PCS_DWORD3},
  {R_DEKEL_PCS_PMD_LANE0_RX_TMRS, V_DEKEL_PCS_PMD_LANE0_RX_TMRS},
  {R_DEKEL_PCS_PMD_LANE1_RX_TMRS, V_DEKEL_PCS_PMD_LANE1_RX_TMRS},
  {R_DEKEL_PCS_PMD_LANE2_RX_TMRS, V_DEKEL_PCS_PMD_LANE2_RX_TMRS},
  {R_DEKEL_PCS_PMD_LANE3_RX_TMRS, V_DEKEL_PCS_PMD_LANE3_RX_TMRS}
};

#endif
