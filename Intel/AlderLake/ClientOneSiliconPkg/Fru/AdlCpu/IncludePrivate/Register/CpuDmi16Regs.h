/** @file
  Register names for DMI and OP-DMI

Conventions:

  - Register definition format:
    Prefix_[GenerationName]_[ComponentName]_SubsystemName_RegisterSpace_RegisterName
  - Prefix:
    Definitions beginning with "R_" are registers
    Definitions beginning with "B_" are bits within registers
    Definitions beginning with "V_" are meaningful values within the bits
    Definitions beginning with "S_" are register size
    Definitions beginning with "N_" are the bit position
  - [GenerationName]:
    Three letter acronym of the generation is used (e.g. SKL,KBL,CNL etc.).
    Register name without GenerationName applies to all generations.
  - [ComponentName]:
    This field indicates the component name that the register belongs to (e.g. PCH, SA etc.)
  - SubsystemName:
    This field indicates the subsystem name of the component that the register belongs to
    (e.g. PCIE, USB, SATA, GPIO, PMC etc.).
  - RegisterSpace:
    MEM - MMIO space register of subsystem.
    IO  - IO space register of subsystem.
    PCR - Private configuration register of subsystem.
    CFG - PCI configuration space register of subsystem.
  - RegisterName:
    Full register name.

@copyright
  INTEL CONFIDENTIAL
  Copyright 2019 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains an 'Intel Peripheral Driver' and is uniquely identified as
  "Intel Reference Module" and is licensed for Intel CPUs and chipsets under
  the terms of your license agreement with Intel or your vendor. This file may
  be modified by the user, subject to additional terms of the license agreement.

@par Specification Reference:
**/
#ifndef _CPU_DMI16_REGS_H_
#define _CPU_DMI16_REGS_H_

#define R_CPU_DMI16_CAPP                   0x34                        ///< Capabilities Pointer
 #define  B_CPU_DMI16_CAPP_PTR_MASK        0xFF
 #define  V_CPU_DMI16_CAPP_PTR             0
//
// DMI Chipset Configuration Registers (PID:DMI)
//
#define R_CPU_DMI16_DCAP                   0x0044                      ///< Device Capabilities
#define B_CPU_DMI16_DCAP_MPS               (BIT2 | BIT1 | BIT0)        ///< Max Payload Size
#define B_CPU_DMI16_DCAP_MPS_MASK          0xFFFFFFF8

#define R_CPU_DMI16_DCTL                   0x0048                      ///< Device Control
#define B_CPU_DMI16_DCTL_MPS               (BIT7 | BIT6 | BIT5)        ///< Max Payload Size
#define B_CPU_DMI16_DCTL_MPS_MASK          0xFFFFFF1F
#define B_CPU_DMI16_DCTL_URE               BIT3                        ///< Unsupported Request Reporting Enable
#define B_CPU_DMI16_DCTL_FEE               BIT2                        ///< Fatal Error Reporting Enable
#define B_CPU_DMI16_DCTL_NFE               BIT1                        ///< Non-Fatal Error Reporting Enable
#define B_CPU_DMI16_DCTL_CEE               BIT0                        ///< Correctable Error Reporting Enable

#define R_CPU_DMI16_LCAP                   0x004C                      ///< Link Capabilities
#define B_CPU_DMI16_LCAP_ASPMOC            BIT22                       ///< ASPM Optionality Compliance
#define B_CPU_DMI16_LCAP_EL1               (BIT17 | BIT16 | BIT15)     ///< L1 Exit Latency
#define N_CPU_DMI16_LCAP_EL1               15                          ///< L1 Exit Latency position
#define V_CPU_DMI16_LCAP_EL1_8US_16US      0x4                         ///< L1 Exit Latency = 4 : 8us to less than 16us
#define B_CPU_DMI16_LCAP_EL0               (BIT14 | BIT13 | BIT12)     ///< L0s Exit Latency
#define N_CPU_DMI16_LCAP_EL0               12                          ///< L0s Exit Latency position
#define V_CPU_DMI16_LCAP_EL0_512NS_1US     0x4                         ///< L0s Exit Latency = 4 : 512ns to less than 1us
#define B_CPU_DMI16_LCAP_APMS              (BIT11 | BIT10)             ///< Active State Link PM Support
#define N_CPU_DMI16_LCAP_APMS              10                          ///< Active State Link PM Support position
#define V_CPU_DMI16_LCAP_APMS_DIS          0x0                         ///< ASPM Disabled
#define V_CPU_DMI16_LCAP_APMS_L0S          0x1                         ///< L0s Entry Enabled
#define V_CPU_DMI16_LCAP_APMS_L1           0x2                         ///< L1 Entry Enabled
#define V_CPU_DMI16_LCAP_APMS_L0S_L1       0x3                         ///< Active State Link PM Support = L0S and L1
#define B_CPU_DMI16_LCAP_MLS               (BIT3 | BIT2 | BIT1 | BIT0) ///< Max Link Speed
#define V_CPU_DMI16_LCAP_MLS_GEN1          1
#define V_CPU_DMI16_LCAP_MLS_GEN2          2
#define V_CPU_DMI16_LCAP_MLS_GEN3          3
#define V_CPU_DMI16_LCAP_MLS_GEN4          4

#define R_CPU_DMI16_LCTL                   0x0050                      ///< Link Control
#define B_CPU_DMI16_LCTL_ES                BIT7                        ///< Extended Synch
#define B_CPU_DMI16_LCTL_RL                BIT5                        ///< link Retrain
#define B_CPU_DMI16_LCTL_ASPM              (BIT1 | BIT0)               ///< Active State Link PM Control
#define V_CPU_DMI16_LCTL_ASPM_DIS          0x0                         ///< ASPM Disabled
#define V_CPU_DMI16_LCTL_ASPM_L0S          0x1                         ///< L0s Entry Enabled
#define V_CPU_DMI16_LCTL_ASPM_L1           0x2                         ///< L1 Entry Enabled
#define V_CPU_DMI16_LCTL_ASPM_L0SL1        0x3                         ///< L0s and L1 Entry Enabled

#define R_CPU_DMI16_LSTS                   0x52                      ///< Link Status
 #define B_CPU_DMI16_LSTS_CLS_SIP          BIT0
 #define B_CPU_DMI16_LSTS_CLS_OFFSET       0
 #define B_CPU_DMI16_LSTS_CLS_MASK         0xF
 #define B_CPU_DMI16_LCTL_RL               BIT5
 #define B_CPU_DMI16_LCTL_RL_OFFSET        5
 #define B_CPU_DMI16_LSTS_LT_OFFSET        BIT11

#define R_CPU_DMI16_LCTL2                  0x0070                      ///< Link Control 2
#define B_CPU_DMI16_LCTL2_TLS              0xF                         ///< Target Link Speed

#define  R_CPU_DMI16_LCTL3                 0xA34
 #define  B_CPU_DMI16_LCTL3_PE             BIT0
 #define  B_CPU_DMI16_LCTL3_PE_OFFSET      0

#define  R_CPU_DMI16_LPCR                  0xC8C
 #define  B_CPU_DMI16_LPCR_DIDOVR_LOCK     BIT24
 #define  B_CPU_DMI16_LPCR_LTRCFGLOCK      BIT16
 #define  B_CPU_DMI16_LPCR_SERL            BIT8
 #define  B_CPU_DMI16_LPCR_SRL             BIT0

#define R_SA_SPX_PCR_SRL                   0x3E24                      ///< Secured Register Lock
 #define  B_SA_SPX_PCR_PCD_SRL             BIT0                        ///< Secured Register Lock bit

#define R_CPU_DMI16_PMCS                   0x00A4                      ///< PCI Power Management Control
#define B_CPU_DMI16_PMCS_NSR               BIT3                        ///< No Soft Reset

#define R_CPU_DMI16_CCFG                   0x00D0                      ///< Channel Configuration
#define B_CPU_DMI16_CCFG_DCGEISMA          BIT15                       ///< Dynamic Clock Gating Enable on ISM Active
#define B_CPU_DMI16_CCFG_UMRPD             BIT26                       ///< Upstream Memory Read Peer Disable

#define R_CPU_DMI16_MPC2                   0x00D4                      ///< Miscellaneous Port Configuration 2
#define B_CPU_DMI16_MPC2_PLLWAIT           (BIT26 | BIT25 | BIT24)     ///< PLL Wait
#define N_CPU_DMI16_MPC2_PLLWAIT           24                          ///< PLL Wait
#define V_CPU_DMI16_MPC2_PLLWAIT_5US       0x1                         ///< PLL Wait = 5us
#define B_CPU_DMI16_MPC2_RUD               BIT23                       ///< Recovery Upconfiguration Disable
#define B_CPU_DMI16_MPC2_GEN2PLLC          BIT21                       ///< GEN2 PLL Coupling
#define B_CPU_DMI16_MPC2_GEN3PLLC          BIT20                       ///< GEN3 PLL Coupling
#define B_CPU_DMI16_MPC2_ORCE              (BIT15 | BIT14)             ///< Offset Re-Calibration Enable
#define B_CPU_DMI16_MPC2_ORCE_MASK         0xC000                      ///< Offset Re-Calibration Enable
#define B_CPU_DMI16_MPC2_ORCE_OFFSET       14                          ///< Offset Re-Calibration Enable
#define V_CPU_DMI16_MPC2_ORCE              0x1                         ///< Offset Re-Calibration Enable
#define V_CPU_DMI16_MPC2_ORCE_EN_GEN2_GEN3 1                           ///< Enable offset re-calibration for Gen 2 and Gen 3 data rate only.
#define B_CPU_DMI16_MPC2_PTNFAE            BIT12                       ///< Poisoned TLP Non-Fatal Advisory Error Enable
#define B_CPU_DMI16_MPC2_IPF               BIT11                       ///< IOSF Packet Fast Transmit Mode
#define B_CPU_DMI16_MPC2_TLPF              BIT9                        ///< Transaction Layer Packet Fast Transmit Mode
#define B_CPU_DMI16_MPC2_CAM               BIT8                        ///< DMI Credit Allocated Update Mode
#define B_CPU_DMI16_MPC2_LSTP              BIT6                        ///< Link Speed Training Policy
#define B_CPU_DMI16_MPC2_EOIFD             BIT1                        ///< EOI Forwarding Disable

#define R_CPU_DMI16_MPC                    0x00D8                      ///< Miscellaneous Port Configuration
#define B_CPU_DMI16_MPC_MMBNCE             BIT27                       ///< MCTP Message Bus Number Check Enable
#define B_CPU_DMI16_MPC_IRRCE              BIT25                       ///< Invalid Receive Range Check Enable
#define B_CPU_DMI16_MPC_SRL                BIT23                       ///< Secured register lock
#define B_CPU_DMI16_MPC_FCDL1E             BIT21                       ///< Flow Control During L1 entry
#define B_CPU_DMI16_MPC_FORCEDET           BIT22                       ///< Force Detect Override
#define B_CPU_DMI16_MPC_CCEL               (BIT17 | BIT16 | BIT15)     ///< Common Clock Exit Latency
#define N_CPU_DMI16_MPC_CCEL               15                          ///< Common Clock Exit Latency
#define V_CPU_DMI16_MPC_CCEL_512NS_1US     0x4                         ///< Common Clock Exit Latency = 512ns to less than 1us
#define B_CPU_DMI16_MPC_FCP                BIT4                        ///< Flow Control Update policy
#define B_CPU_DMI16_MPC_FCP_MASK           0x70
#define B_CPU_DMI16_MPC_FCP_OFFSET         4
#define V_CPU_DMI16_MPC_FCP                0X4
#define B_CPU_DMI16_MPC_MCTPSE             BIT3                        ///< MCTP Support Enable
#define B_CPU_DMI_MPC_PCIESD               BIT13
#define B_CPU_DMI_MPC_PCIESD_OFFSET        13
#define B_CPU_DMI_MPC_PCIESD_MASK          0x6000
#define V_CPU_DMI_MPC_PCIESD_GEN1          1
#define V_CPU_DMI_MPC_PCIESD_GEN2          2
#define V_CPU_DMI_MPC_PCIESD_GEN3          3
#define V_CPU_DMI_MPC_PCIESD_GEN4          4
#define B_CPU_DMI_MPC_BT                   BIT2                        ///< Bridge Type

#define R_CPU_DMI16_RPDCGEN                0x00E1                      ///< Root Port Dynamic Clock Gate Enable
#define B_CPU_DMI16_PTOCGE                 BIT6                        ///< Partition/Trunk Oscillator Clock Gate Enable
#define B_CPU_DMI16_LCLKREQEN              BIT5                        ///< Link CLKREQ Enable
#define B_CPU_DMI16_BBCLKREQEN             BIT4                        ///< Backbone CLKREQ Enable
#define B_CPU_DMI16_SRDBCGEN               BIT2                        ///< Shared Resource Dynamic Backbone Clock Gate Enable
#define B_CPU_DMI16_RPDLCGEN               BIT1                        ///< Root Port Dynamic Link Clock Gate Enable
#define B_CPU_DMI16_RPDBCGEN               BIT0                        ///< Root Port Dynamic Backbone Clock Gate Enable

#define R_CPU_DMI16_RPPGEN                 0x00E2                      ///< Root Port Power Gating Enable
#define B_CPU_DMI16_RPPGEN_SEOSCGE         BIT4                        ///< Sideband Endpoint Oscillator/Side Clock Gating Enable

#define R_CPU_DMI16_PWRCTL                 0x00E8                      ///< Power Control
#define B_CPU_DMI16_PWRCTL_RSP             BIT25
#define B_CPU_DMI16_PWRCTL_LIFECF          BIT23                       ///< Logical Idle Framing Error Check Filter
#define B_CPU_DMI16_PWRCTL_WPDMPGEP        BIT17                       ///< Wake PLL On Detect mod-PHY Power Gating Exit Policy
#define B_CPU_DMI16_PWRCTL_DLP             BIT16
#define B_CPU_DMI16_PWRCTL_DLP_OFFSET      16
#define B_CPU_DMI16_PWRCTL_DBUPI           BIT15                       ///< De-skew Buffer Unload Pointer Increment
#define B_CPU_DMI16_PWRCTL_RPSEWL          (BIT3 | BIT2)               ///< Root Port Squelch Exit Wait Latency
#define N_CPU_DMI16_PWRCTL_RPSEWL          2                           ///< Root Port Squelch Exit Wait Latency
#define V_CPU_DMI16_PWRCTL_RPSEWL_120NS    0x1                         ///< Root Port Squelch Exit Wait Latency = 120 ns
#define B_CPU_DMI16_PWRCTL_RPL1SQPOL       BIT1                        ///< Root Port L1 Squelch Polling
#define B_CPU_DMI16_PWRCTL_RPDTSQPOL       BIT0                        ///< Root Port Detect Squelch Polling

#define R_CPU_DMI16_PHYCTL2                0x00F5                      ///< Physical Layer And AFE Control 2
#define B_CPU_DMI16_PHYCTL2_TDFT           (BIT7 | BIT6)               ///< Transmit Datapath Flush Timer
#define B_CPU_DMI16_PHYCTL2_TXCFGCHGWAIT   (BIT5 | BIT4)               ///< Transmit Configuration Change Wait Time
#define B_CPU_DMI16_PHYCTL2_PXPG3PLLOFFEN  BIT1                        ///< PCI Express GEN3 PLL Off Enable
#define B_CPU_DMI16_PHYCTL2_PXPG2PLLOFFEN  BIT0                        ///< PCI Express GEN2 PLL Off Enable

#define R_CPU_DMI16_PHYCTL3                0x00F6                      ///< Physical Layer And AFE Control 3
#define B_CPU_DMI16_PHYCTL3_SQDIROVREN     BIT2                        ///< Squelch Direction Override Enable
#define B_CPU_DMI16_PHYCTL3_SQDIRCTRL      BIT1                        ///< Squelch Direction

#define R_CPU_DMI16_IOSFSBCS               0x00F7                      ///< IOSF Sideband Control And Status
#define B_CPU_DMI16_IOSFSBCS_SCPTCGE       BIT6                        ///< Side Clock Partition/Trunk Clock Gating Enable
#define B_CPU_DMI16_IOSFSBCS_SIID          (BIT3 | BIT2)               ///< IOSF Sideband Interface Idle Counter

#define R_CPU_DMI16_STRPFUSECFG            0X00FC
#define B_CPU_DMI16_STRPFUSECFG_NAL        BIT10                       ///< Number of ACtive Lanes
#define B_CPU_DMI16_STRPFUSECFG_NAL_MASK   0XC00
#define B_CPU_DMI16_STRPFUSECFG_NALPG      BIT12                       ///< Number of ACtive Lanes with Power Gating Enable

#define R_CPU_DMI16_UEM                  0x0108                      ///< Uncorrectable Error Mask
#define B_CPU_DMI16_UEM_URE              BIT20                       ///< Unsupported Request Error Mask
#define B_CPU_DMI16_UEM_CM               BIT15                       ///< Completer Abort Mask
#define B_CPU_DMI16_UEM_PT               BIT12                       ///< Poisoned TLP Mask

#define R_CPU_DMI16_V0CTL                0x0284                      ///< Virtual channel 0 resource control
#define R_CPU_DMI16_V0STS                0x028A                      ///< Virtual channel 0 status

#define R_CPU_DMI16_V1CTL                0x0290                      ///< Virtual channel 1 resource control
#define R_CPU_DMI16_V1STS                0x0296                      ///< Virtual channel 1 status

#define R_CPU_DMI16_VMCTL                0x02B0                      ///< ME Virtual Channel (VCm) resource control
 #define  B_CPU_DMI16_VMCTL_TVM_MASK                   0x80
 #define  B_CPU_DMI16_VMCTL_TVM                        BIT7
 #define  B_CPU_DMI16_VMCTL_ID_MASK                    0xF000000
 #define  B_CPU_DMI16_VMCTL_ID_OFFSET                  24
 #define  V_CPU_DMI16_VMCTL_ID_TWO                     7
 #define  B_CPU_DMI16_VMCTL_EN_MASK                    0x80000000
 #define  B_CPU_DMI16_VMCTL_EN                         BIT31
#define R_CPU_DMI16_VMSTS                0x02B6                      ///< ME Virtual Channel Resource Status

#define R_CPU_DMI16_G4L0SCTL                      0x0310             ///< GEN4 L0s Control
#define B_CPU_DMI16_G4L0SCTL_G4ASL0SPL            BIT24              ///< Gen4 Active State L0s Preparation Latency
#define B_CPU_DMI16_G4L0SCTL_G4ASL0SPL_MASK       0xFF000000         ///< Gen4 Active State L0s Preparation Latency
#define B_CPU_DMI16_G4L0SCTL_G4ASL0SPL_OFFSET     24                 ///< Gen4 Active State L0s Preparation Latency
#define V_CPU_DMI16_G4L0SCTL_G4ASL0SPL            0x28               ///< Gen4 Active State L0s Preparation Latency
#define B_CPU_DMI16_G4L0SCTL_G4L0SIC              BIT22              ///< GEN4 L0s Entry Idle Control
#define B_CPU_DMI16_G4L0SCTL_G4L0SICL_OFFSET      22                 ///< GEN4 L0s Entry Idle Control
#define B_CPU_DMI16_G4L0SCTL_G4L0SICL_MASK        0x00C00000         ///< GEN4 L0s Entry Idle Control
#define V_CPU_DMI16_G4L0SCTL_G4L0SIC              0x3                ///< GEN4 L0s Entry Idle Control
#define B_CPU_DMI16_G4L0SCTL_G4UCNFTS             BIT8               ///< Gen4 Unique Clock N_FTS
#define B_CPU_DMI16_G4L0SCTL_G4UCNFTS_MASK        0x0000FF00         ///< Gen4 Unique Clock N_FTS
#define B_CPU_DMI16_G4L0SCTL_G4UCNFTS_OFFSET      8                  ///< Gen4 Unique Clock N_FTS
#define V_CPU_DMI16_G4L0SCTL_G4UCNFTS             0X80               ///< Gen4 Unique Clock N_FTS
#define B_CPU_DMI16_G4L0SCTL_G4CCNFTS             BIT0               ///< Gen4 Common Clock N_FTS
#define B_CPU_DMI16_G4L0SCTL_G4CCNFTS_MASK        0xFF               ///< Gen4 Common Clock N_FTS
#define B_CPU_DMI16_G4L0SCTL_G4CCNFTS_OFFSET      0                  ///< Gen4 Common Clock N_FTS
#define V_CPU_DMI16_G4L0SCTL_G4CCNFTS             0X5B               ///< Gen4 Common Clock N_FTS

#define R_CPU_DMI16_PCIENFTS             0x0314                      ///< PCI Express NFTS
#define B_CPU_DMI16_PCIENFTS_G2UCNFTS    0xFF000000                  ///< Gen2 Unique Clock N_FTS
#define N_CPU_DMI16_PCIENFTS_G2UCNFTS    24                          ///< Gen2 Unique Clock N_FTS
#define B_CPU_DMI16_PCIENFTS_G2CCNFTS    0xFF0000                    ///< Gen2 Common Clock N_FTS
#define N_CPU_DMI16_PCIENFTS_G2CCNFTS    16                          ///< Gen2 Common Clock N_FTS
#define B_CPU_DMI16_PCIENFTS_G1UCNFTS    0x0000FF00                  ///< Gen1 Unique Clock N_FTS
#define N_CPU_DMI16_PCIENFTS_G1UCNFTS    8                           ///< Gen1 Unique Clock N_FTS
#define B_CPU_DMI16_PCIENFTS_G1CCNFTS    0xFF                        ///< Gen1 Common Clock N_FTS
#define N_CPU_DMI16_PCIENFTS_G1CCNFTS    0                           ///< Gen1 Common Clock N_FTS

#define R_CPU_DMI16_PCIEL0SC             0x0318                      ///< PCI Express L0s Control
#define B_CPU_DMI16_PCIEL0SC_G2ASL0SPL   0xFF000000                  ///< Gen2 Active State L0s Preparation Latency
#define N_CPU_DMI16_PCIEL0SC_G2ASL0SPL   24                          ///< Gen2 Active State L0s Preparation Latency
#define B_CPU_DMI16_PCIEL0SC_G1ASL0SPL   0x00FF0000                  ///< Gen1 Active State L0s Preparation Latency
#define N_CPU_DMI16_PCIEL0SC_G1ASL0SPL   16                          ///< Gen1 Active State L0s Preparation Latency

#define R_CPU_DMI16_PCIECFG2             0x0320                      ///< PCI Express Configuration 2
#define B_CPU_DMI16_PCIECFG2_LBWSSTE     BIT30                       ///< Low Bandwidth Squelch Settling Timer Enable
#define B_CPU_DMI16_PCIECFG2_CROAOV      BIT24                       ///< Completion Relaxed Ordering Attribute Override Value
#define B_CPU_DMI16_PCIECFG2_CROAOE      BIT23                       ///< Completion Relaxed Ordering Attribute Override Enable
#define B_CPU_DMI16_PCIECFG2_PMET        (BIT21 | BIT20)             ///< PME Timeout
#define N_CPU_DMI16_PCIECFG2_PMET        20                          ///< PME Timeout
#define V_CPU_DMI16_PCIECFG2_PMET_10MS   1                           ///< PME Timeout = 1ms
#define V_CPU_DMI16_PCIECFG2_PMET_DISABLE   3                        ///< PME Timeout = Disable

#define R_CPU_DMI16_PCIEDBG              0x0324                      ///< PCI Express Debug And Configuration
#define B_CPU_DMI16_PCIEDBG_USSP         (BIT27 | BIT26)             ///< Un-Squelch Sampling Period
#define N_CPU_DMI16_PCIEDBG_USSP         26                          ///< Un-Squelch Sampling Period
#define V_CPU_DMI16_PCIEDBG_USSP_32ns    2                           ///< Un-Squelch Sampling Period = 32ns
#define B_CPU_DMI16_PCIEDBG_LGCLKSQEXITDBTIMERS         (BIT25 | BIT24)            ///< Link Clock Domain Squelch Exit Debounce Timers
#define N_CPU_DMI16_PCIEDBG_LGCLKSQEXITDBTIMERS         24                         ///< Link Clock Domain Squelch Exit Debounce Timers
#define V_CPU_DMI16_PCIEDBG_LGCLKSQEXITDBTIMERS_0ns    0                           ///< Link Clock Domain Squelch Exit Debounce Timers = 0ns
#define B_CPU_DMI16_PCIEDBG_CTONFAE      BIT14                       ///< Completion Time-Out Non-Fatal Advisory Error Enable
#define B_CPU_DMI16_PCIEDBG_SQOL0        BIT7                        ///< Squelch Off in L0
#define B_CPU_DMI16_PCIEDBG_SPCE         BIT5                        ///< Squelch Propagation Control Enable

#define R_CPU_DMI16_PHYCTL4              0x0408                      ///< Physical Layer And AFE Control 4
#define B_CPU_DMI16_PHYCTL4_SQDIS        BIT27                       ///< Squelch Disable

#define R_CPU_DMI16_STRPFUSECF                        0X414
#define B_CPU_DMI16_STRPFUSECF_FORCEL                 BIT7          ///< Force Limit
#define B_CPU_DMI16_STRPFUSECF_FORCEL_MASK            0x380
#define B_CPU_DMI16_STRPFUSECF_FORCEL_OFFSET          7
#define V_CPU_DMI16_STRPFUSECF_FORCEL                 0X4

#define R_CPU_DMI16_PCIEPMECTL                        0x0420         ///< PCIe PM Extension Control
#define B_CPU_DMI16_PCIEPMECTL_FDPPGE                 BIT31          ///< Function Disable PHY Power Gating Enable
#define B_CPU_DMI16_PCIEPMECTL_DLSULPPGE              BIT30          ///< Disabled, Detect and L23_Rdy State PHY Lane Power Gating Enable
#define B_CPU_DMI16_PCIEPMECTL_DLSULDLSD              BIT29          ///< Disabled, Detect, L23_Rdy State,Un-Configured Lane and Down-Configured Lane Squelch Disable
#define B_CPU_DMI16_PCIEPMECTL_IPIEP                  BIT21          ///< IP-Inaccessible Entry Policy
#define B_CPU_DMI16_PCIEPMECTL_IPACPE                 BIT20          ///< IP-Accessible Context Propagation Enable
#define B_CPU_DMI16_PCIEPMECTL_L1FSOE                 BIT0           ///< L1 Full Squelch OFF Enable
#define B_CPU_DMI16_PCIEPMECTL_L1LTRTLV               BIT4
#define B_CPU_DMI16_PCIEPMECTL_L1LTRTLV_OFFSET        4
#define B_CPU_DMI16_PCIEPMECTL_L1LTRTLV_MASK          0x3FF0
#define V_CPU_DMI16_PCIEPMECTL_L1LTRTLV               0x32

#define R_CPU_DMI16_PCIEPMECTL2                0x0424                   ///< PCIe PM Extension Control 2
#define B_CPU_DMI16_PCIEPMECTL2_CPMCSRE        BIT27                    ///< Chassis PMC Save and Restore Enable
#define B_CPU_DMI16_PCIEPMECTL2_CPGENH         BIT12                    ///< Controller Power Gating Entry Hysteresis
#define B_CPU_DMI16_PCIEPMECTL2_CPGENH_OFFSET  12                       ///< Controller Power Gating Entry Hysteresis
#define B_CPU_DMI16_PCIEPMECTL2_CPGENH_MASK    0x3000                   ///< Controller Power Gating Entry Hysteresis
#define V_CPU_DMI16_PCIEPMECTL2_CPGENH         0x1                      ///< Controller Power Gating Entry Hysteresis
#define B_CPU_DMI16_PCIEPMECTL2_CPGEXH         BIT14                    ///< Controller Power Gating Exit Hysteresis
#define B_CPU_DMI16_PCIEPMECTL2_CPGEXH_OFFSET  14                       ///< Controller Power Gating Exit Hysteresis
#define B_CPU_DMI16_PCIEPMECTL2_CPGEXH_MASK    0xC000                   ///< Controller Power Gating Exit Hysteresis
#define V_CPU_DMI16_PCIEPMECTL2_CPGEXH         0x1                      ///< Controller Power Gating Exit Hysteresis
#define B_CPU_DMI16_PCIEPMECTL2_PHYCLPGE       BIT11                    ///< PHY Common Lane Power Gating Enable
#define B_CPU_DMI16_PCIEPMECTL2_L1SPHYDLPGE    BIT9                     ///< L1 State PHY Data Lane Power Gating Enable
#define B_CPU_DMI16_PCIEPMECTL2_FDCPGE         BIT8                     ///< Function Disable Controller Power Gating Enable
#define B_CPU_DMI16_PCIEPMECTL2_DISSCPGE       BIT5                     ///< Disabled State Controller Power Gating Enable
#define B_CPU_DMI16_PCIEPMECTL2_L1SCPGE        BIT4                     ///< L1 State Controller Power Gating Enable

#define R_CPU_DMI16_PCE                  0x0428                      ///< Power Control Enable
#define B_CPU_DMI16_PCE_HAE              BIT5                        ///< Hardware Autonomous Enable
#define B_CPU_DMI16_PCE_SE               BIT3                        ///< Sleep Enable
#define B_CPU_DMI16_PCE_PMCRE            BIT0                        ///< PMC Request Enable

#define R_CPU_DMI16_PME_CTL3                   0x0434                      ///< PCIe PM Extension Control 3
#define B_CPU_DMI16_PME_CTL3_L1PGAUTOPGEN      BIT4                        ///< L1.PG Auto Power Gate Enable
#define B_CPU_DMI16_PME_CTL3_OSCCGH            (BIT3 | BIT2)               ///< OSC Clock Gate Hysteresis
#define N_CPU_DMI16_PME_CTL3_OSCCGH            2                           ///< OSC Clock Gate Hysteresis
#define B_CPU_DMI16_PME_CTL3_OSCCGH_OFFSET     2                           ///< OSC Clock Gate Hysteresis
#define V_CPU_DMI16_PME_CTL3_OSCCGH_1US        0x1                         ///< OSC Clock Gate Hysteresis = 1us
#define B_CPU_DMI16_PME_CTL3_PMREQCPGEXH       (BIT1 | BIT0)               ///< PM Request Controller Power Gating Exit Hysteresis
#define N_CPU_DMI16_PME_CTL3_PMREQCPGEXH       0                           ///< PM Request Controller Power Gating Exit Hysteresis
#define V_CPU_DMI16_PME_CTL3_PMREQCPGEXH       0x0                         ///< PM Request Controller Power Gating Exit Hysteresis
#define V_CPU_DMI16_PME_CTL3_PMREQCPGEXH_5US   0x1                         ///< PM Request Controller Power Gating Exit Hysteresis = 5us

#define  R_CPU_DMI16_HAEQ                              0x468
 #define  B_CPU_DMI16_HAEQ_HAPCCPI                     (BIT31 | BIT30 | BIT29 | BIT28)
 #define  B_CPU_DMI16_HAEQ_HAPCCPI_OFFSET              28
 #define  B_CPU_DMI16_HAEQ_FOMEM                       0x0FF00000
 #define  B_CPU_DMI16_HAEQ_FOMEM_OFFSET                20
 #define  B_CPU_DMI16_HAEQ_MACFOMC                     BIT19
 #define  B_CPU_DMI16_HAEQ_SL                          (BIT18 | BIT17 | BIT16)
 #define  B_CPU_DMI16_HAEQ_SL_OFFSET                   16
 #define  B_CPU_DMI16_HAEQ_DL                          0x0000FF00
 #define  B_CPU_DMI16_HAEQ_DL_OFFSET                   8
 #define  B_CPU_DMI16_HAEQ_SFOMFM                      0x000000FF
 #define  B_CPU_DMI16_HAEQ_SFOMFM_OFFSET               0

#define R_CPU_DMI16_EQCFG1               0x0450                      ///< Equalization Configuration 1
#define B_CPU_DMI16_EQCFG1_RTLEPCEB      BIT16                       ///< Remote Transmit Link Equalization Preset/Coefficient Evaluation Bypass
#define  S_CPU_DMI16_EQCFG1                           4
#define  B_CPU_DMI16_EQCFG1_REC                       0xFF000000
#define  B_CPU_DMI16_EQCFG1_REC_OFFSET                24
#define  B_CPU_DMI16_EQCFG1_REIFECE                   BIT23
#define  B_CPU_DMI16_EQCFG1_LERSMIE                   BIT21
#define  B_CPU_DMI16_EQCFG1_LEB                       BIT19
#define  B_CPU_DMI16_EQCFG1_LEP23B                    BIT18
#define  B_CPU_DMI16_EQCFG1_LEP3B                     BIT17
#define  B_CPU_DMI16_EQCFG1_RTPCOE                    BIT15
#define  B_CPU_DMI16_EQCFG1_LERSCIE                   BIT14
#define  B_CPU_DMI16_EQCFG1_HPCMQE                    BIT13
#define  B_CPU_DMI16_EQCFG1_HAED                      BIT12
#define  B_CPU_DMI16_EQCFG1_RWTNEVE                   BIT8
#define  B_CPU_DMI16_EQCFG1_RWTNEVE_OFFSET            8
#define  B_CPU_DMI16_EQCFG1_RWTNEVE_MASK              0x00000F00
#define  V_CPU_DMI16_EQCFG1_RWTNEVE_1US               1
#define  B_CPU_DMI16_EQCFG1_EQTS2IRRC                 BIT7
#define  B_CPU_DMI16_EQCFG1_HAPCCPIE                  BIT5
#define  B_CPU_DMI16_EQCFG1_MEQSMMFLNTL               BIT4
#define  B_CPU_DMI16_EQCFG1_MFLNTL                    BIT2
#define  B_CPU_DMI16_EQCFG1_TUPP                      BIT1
#define  B_CPU_DMI16_EQCFG1_RUPP                      BIT0

#define  R_CPU_DMI16_EQCFG4                            0x48C
 #define  B_CPU_DMI16_EQCFG4_PX16GTSWLPCE              BIT27
 #define  B_CPU_DMI16_EQCFG4_PX16GTSWLPCE_OFFSET       27
 #define  B_CPU_DMI16_EQCFG4_PX16GTSWLPCE_MASK         (BIT29 | BIT28 | BIT27)
 #define  V_CPU_DMI16_EQCFG4_PX16GTSWLPCE_5US          4
 #define  B_CPU_DMI16_EQCFG4_PX8GTSWLPCE               BIT24
 #define  B_CPU_DMI16_EQCFG4_PX8GTSWLPCE_OFFSET        24
 #define  B_CPU_DMI16_EQCFG4_PX8GTSWLPCE_MASK          (BIT26 | BIT25 | BIT24)
 #define  V_CPU_DMI16_EQCFG4_PX8GTSWLPCE_4US           3
 #define  B_CPU_DMI16_EQCFG4_FOMSCP_OFFSET             21
 #define  B_CPU_DMI16_EQCFG4_FOMSCP_MASK               (BIT23 | BIT22 | BIT21)
 #define  B_CPU_DMI16_EQCFG4_PX16GREIC                 BIT20
 #define  B_CPU_DMI16_EQCFG4_PX16GLEP23B               BIT18
 #define  B_CPU_DMI16_EQCFG4_PX16GLEP3B                BIT17
 #define  B_CPU_DMI16_EQCFG4_PX16GLEPCEB               BIT16
 #define  B_CPU_DMI16_EQCFG4_PX16GRTPCOE               BIT15
 #define  B_CPU_DMI16_EQCFG4_PX16GRWTNEVE              BIT8
 #define  B_CPU_DMI16_EQCFG4_PX16GRWTNEVE_OFFSET       8
 #define  B_CPU_DMI16_EQCFG4_PX16GRWTNEVE_MASK         0x00000F00
 #define  V_CPU_DMI16_EQCFG4_PX16GRWTNEVE_1US          1
 #define  V_CPU_DMI16_EQCFG4_PX16GRWTNEVE_3US          3
 #define  B_CPU_DMI16_EQCFG4_PX16GHAED                 BIT12
 #define  B_CPU_DMI16_EQCFG4_PX16GEQTS2IRRC            BIT7
 #define  B_CPU_DMI16_EQCFG4_PX16GHAPCCPI              BIT3
 #define  B_CPU_DMI16_EQCFG4_PX16GHAPCCPI_OFFSET       3
 #define  B_CPU_DMI16_EQCFG4_PX16GHAPCCPI_MASK         (BIT6 | BIT5 | BIT4 | BIT3)
 #define  B_CPU_DMI16_EQCFG4_PX16GHAPCCPIE             BIT2
 #define  B_CPU_DMI16_EQCFG4_PX16GMEQSMMFLNTL          BIT1
 #define  B_CPU_DMI16_EQCFG4_PX16GMFLNTL               BIT0

#define  R_CPU_DMI_RTPCL1                            0x454
 #define  B_CPU_DMI_RTPCL1_PCM                       BIT31
 #define  B_CPU_DMI_RTPCL1_PCM_OFFSET                31

#define  R_CPU_DMI16_PX16GRTPCL1                       0x4DC
 #define  B_CPU_DMI16_PX16GRTPCL1_PCM                  BIT31
 #define  B_CPU_DMI16_PX16GRTPCL1_PCM_OFFSET           31

#define R_CPU_DMI16_EQCFG2                             0x47C
 #define  B_CPU_DMI16_EQCFG2_NTIC                      0xFF000000
 #define  B_CPU_DMI16_EQCFG2_EMD                       BIT23
 #define  B_CPU_DMI16_EQCFG2_NTSS                      (BIT22 | BIT21 | BIT20)
 #define  B_CPU_DMI16_EQCFG2_PCET                      (BIT19 | BIT18 | BIT17 | BIT16)
 #define  B_CPU_DMI16_EQCFG2_PCET_OFFSET               16
 #define  B_CPU_DMI16_EQCFG2_HAPCSB                    (BIT15 | BIT14 | BIT13 | BIT12)
 #define  B_CPU_DMI16_EQCFG2_HAPCSB_OFFSET             12
 #define  B_CPU_DMI16_EQCFG2_NTEME                     BIT11
 #define  B_CPU_DMI16_EQCFG2_MPEME                     BIT10
 #define  B_CPU_DMI16_EQCFG2_REWMETM                   (BIT9 | BIT8)
 #define  B_CPU_DMI16_EQCFG2_REWMETM_OFFSET            8
 #define  B_CPU_DMI16_EQCFG2_REWMET                    0xFF

#define R_CPU_DMI16_LTCO1                0x0470                      ///< Local Transmitter Coefficient Override 1
#define R_CPU_DMI16_LTCO2                0x0474                      ///< Local Transmitter Coefficient Override 2
#define B_CPU_DMI16_L1357TCOE            BIT25                       ///< Lane 1/3 Transmitter Coefficient Override Enable
#define B_CPU_DMI16_L0246TCOE            BIT24                       ///< Lane 0/2 Transmitter Coefficient Override Enable
#define B_CPU_DMI16_L1357TPOSTCO         0x00FC0000                  ///< Lane 1/3 Transmitter Post-Cursor Coefficient Override mask
#define N_CPU_DMI16_L1357TPOSTCO         18                          ///< Lane 1/3 Transmitter Post-Cursor Coefficient Override value offset
#define B_CPU_DMI16_L1357TPRECO          0x0003F000                  ///< Lane 1/3 Transmitter Pre-Cursor Coefficient Override mask
#define N_CPU_DMI16_L1357TPRECO          12                          ///< Lane 1/3 Transmitter Pre-Cursor Coefficient Override value offset
#define B_CPU_DMI16_L0246TPOSTCO         0x00000FC0                  ///< Lane 0/2 Transmitter Post-Cursor Coefficient Override mask
#define N_CPU_DMI16_L0246TPOSTCO         6                           ///< Lane 0/2 Transmitter Post-Cursor Coefficient Override value offset
#define B_CPU_DMI16_L0246TPRECO          0x0000003F                  ///< Lane 0/2 Transmitter Pre-Cursor Coefficient Override mask
#define N_CPU_DMI16_L0246TPRECO          0                           ///< Lane 0/2 Transmitter Pre-Cursor Coefficient Override value offset

#define R_CPU_DMI16_G3L0SCTL             0x0478                      ///< GEN3 L0s Control
#define B_CPU_DMI16_G3L0SCTL_G3ASL0SPL   0xFF000000                  ///< Gen3 Active State L0s Preparation Latency
#define N_CPU_DMI16_G3L0SCTL_G3ASL0SPL   24                          ///< Gen3 Active State L0s Preparation Latency
#define B_CPU_DMI16_G3L0SCTL_G3UCNFTS    0x0000FF00                  ///< Gen3 Unique Clock N_FTS
#define N_CPU_DMI16_G3L0SCTL_G3UCNFTS    8                           ///< Gen3 Unique Clock N_FTS
#define B_CPU_DMI16_G3L0SCTL_G3CCNFTS    0xFF                        ///< Gen3 Common Clock N_FTS
#define N_CPU_DMI16_G3L0SCTL_G3CCNFTS    0                           ///< Gen3 Common Clock N_FTS

#define R_CPU_DMI16_UPHWAWC             0x049C                       ///< Upstream Port HW Autonomous Width Control
#define B_CPU_DMI16_UPHWAWC_TS3TW       (BIT15 | BIT14 | BIT13)      ///< Thermal Sensor 3 Target Width
#define N_CPU_DMI16_UPHWAWC_TS3TW       13                           ///< Thermal Sensor 3 Target Width
#define B_CPU_DMI16_UPHWAWC_TS2TW       (BIT12 | BIT11 | BIT10)      ///< Thermal Sensor 2 Target Width
#define N_CPU_DMI16_UPHWAWC_TS2TW       10                           ///< Thermal Sensor 2 Target Width
#define B_CPU_DMI16_UPHWAWC_TS1TW       (BIT9 | BIT8 | BIT7)         ///< Thermal Sensor 1 Target Width
#define N_CPU_DMI16_UPHWAWC_TS1TW       7                            ///< Thermal Sensor 1 Target Width
#define B_CPU_DMI16_UPHWAWC_TS0TW       (BIT6 | BIT5 | BIT4)         ///< Thermal Sensor 0 Target Width
#define N_CPU_DMI16_UPHWAWC_TS0TW       4                            ///< Thermal Sensor 0 Target Width
#define B_CPU_DMI16_UPHWAWC_TSAWEN      BIT0                         ///< Thermal Sensor Autonomous Width Enable

#define R_CPU_DMI16_CTRL1                0x04A0                      ///< Control 1
#define B_CPU_DMI16_CTRL1_MRSCDIS        BIT21                       ///< MEUMA Root Space Check Disable
#define B_CPU_DMI16_CTRL1_L0SPFCUF       (BIT16 | BIT15)             ///< L0s Periodic Flow Control Update Frequency
#define B_CPU_DMI16_CTRL1_L1PL           (BIT11 | BIT10 | BIT9)      ///< DMI L1 Preparation Latency
#define N_CPU_DMI16_CTRL1_L1PL           9                           ///< DMI L1 Preparation Latency
#define V_CPU_DMI16_CTRL1_L1PL_4US       0x3                         ///< DMI L1 Preparation Latency = 1us
#define B_CPU_DMI16_CTRL1_TCUAPF         BIT6                        ///< Transaction Credit Update Arbitration Performance Fix
#define B_CPU_DMI16_CTRL1_PSS            BIT3                        ///< IOSF Primary SAI Select
#define B_CPU_DMI16_CTRL1_UTPB           BIT0                        ///< Unsupported Transaction Policy Bit

#define R_CPU_DMI16_CTRL2                0x04A4                      ///< Control 2
#define B_CPU_DMI16_CTRL2_DLDRSP         BIT13                       ///< DMI Link Data Rate Sustain Policy
#define B_CPU_DMI16_CTRL2_CPGEXLCWDIS    BIT5                        ///< CPG Exit Link Clock Wake Disable
#define B_CPU_DMI16_CTRL2_PMETOFD        BIT6                        ///< PMETO Timeout Fix Disable

#define  R_CPU_DMI16_EQCFG5                           0x4F8
 #define  B_CPU_DMI16_EQCFG5_NTIC                     BIT24
 #define  B_CPU_DMI16_EQCFG5_NTIC_MASK                0xFF000000
 #define  B_CPU_DMI16_EQCFG5_EMD                      BIT23
 #define  B_CPU_DMI16_EQCFG5_PCET                     BIT16
 #define  B_CPU_DMI16_EQCFG5_PCET_MASK                (BIT19 | BIT18 | BIT17 | BIT16)
 #define  B_CPU_DMI16_EQCFG5_PCET_OFFSET              16
 #define  B_CPU_DMI16_EQCFG5_HAPCSB                   BIT12
 #define  B_CPU_DMI16_EQCFG5_HAPCSB_OFFSET            12
 #define  B_CPU_DMI16_EQCFG5_HAPCSB_MASK              (BIT15 | BIT14 | BIT13 | BIT12)
 #define  B_CPU_DMI16_EQCFG5_NTEME                    BIT11
 #define  B_CPU_DMI16_EQCFG5_MPEME                    BIT10

#define R_CPU_DMI16_L0P0P1PCM            0x0500                      ///< Lane 0 P0 And P1 Preset-Coefficient Mapping
#define R_CPU_DMI16_L0P1P2P3PCM          0x0504                      ///< Lane 0 P1, P2 And P3 Preset-Coefficient Mapping
#define R_CPU_DMI16_L0P3P4PCM            0x0508                      ///< Lane 0 P3 And P4 Preset-Coefficient Mapping
#define R_CPU_DMI16_L0P5P6PCM            0x050C                      ///< Lane 0 P5 And P6 Preset-Coefficient Mapping
#define R_CPU_DMI16_L0P6P7P8PCM          0x0510                      ///< Lane 0 P6, P7 And P8 Preset-Coefficient Mapping
#define R_CPU_DMI16_L0P8P9PCM            0x0514                      ///< Lane 0 P8 And P9 Preset-Coefficient Mapping
#define R_CPU_DMI16_L0P10PCM             0x0518                      ///< Lane 0 P10 Preset-Coefficient Mapping ()
#define R_CPU_DMI16_L0LFFS               0x051C                      ///< Lane 0 LF And FS

#define R_CPU_DMI16_PX16GP0P1PCM         0x0520                      ///< 16.0 GT/s P0 And P1 Preset-Coefficient Mapping
#define R_CPU_DMI16_PX16GP1P2P3PCM       0x0524                      ///< 16.0 GT/s P1, P2 And P3 Preset-Coefficient Mapping
#define R_CPU_DMI16_PX16GP3P4PCM         0x0528                      ///< 16.0 GT/s P3 And P4 Preset-Coefficient Mapping
#define R_CPU_DMI16_PX16GP5P6PCM         0x052C                      ///< 16.0 GT/s P5 And P6 Preset-Coefficient Mapping
#define R_CPU_DMI16_PX16GP6P7P8PCM       0x0530                      ///< 16.0 GT/s P6, P7 And P8 Preset-Coefficient Mapping
#define R_CPU_DMI16_PX16GP8P9PCM         0x0534                      ///< 16.0 GT/s P8 And P9 Preset-Coefficient Mapping
#define R_CPU_DMI16_PX16GP10PCM          0x0538                      ///< 16.0 GT/s P10 Preset-Coefficient Mapping
#define R_CPU_DMI16_PX16GLFFS            0x053C                      ///< 16.0 GT/s LF And FS

#define R_CPU_DMI16_LTCO3                0x0598                      ///< Local Transmitter Coefficient Override 3
#define R_CPU_DMI16_LTCO4                0x059C                      ///< Local Transmitter Coefficient Override 4

#define R_CPU_DMI16_HWSNR                0x05F0                      ///< Hardware Save and Restore
#define B_CPU_DMI16_HWSNR_BEPW           0x0000000F                  ///< Bank Enable Pulse Width
#define N_CPU_DMI16_HWSNR_BEPW           0                           ///< Bank Enable Pulse Width
#define V_CPU_DMI16_HWSNR_BEPW_8CLKS     0x5                         ///< Bank Enable Pulse Width = 8 clocks
#define B_CPU_DMI16_HWSNR_REPW           0x000000F0                  ///< Restore Enable Pulse Width
#define N_CPU_DMI16_HWSNR_REPW           4                           ///< Restore Enable Pulse Width
#define V_CPU_DMI16_HWSNR_REPW_2CLKS     0x1                         ///< Restore Enable Pulse Width = 2 clocks
#define B_CPU_DMI16_HWSNR_EEH            (BIT9 | BIT8)               ///< Entry and Exit Hysteresis
#define N_CPU_DMI16_HWSNR_EEH            8                           ///< Entry and Exit Hysteresis
#define V_CPU_DMI16_HWSNR_EEH_16CLKS     0x2                         ///< Entry and Exit Hysteresis = 16 clocks

#define R_CPU_DMI16_PGCTRL                        0x05F4                      ///< Power Gating Control
#define B_CPU_DMI16_PGCTRL_PMREQBLKRSPT           0x00000007                  ///< PM_REQ Block Response Time
#define N_CPU_DMI16_PGCTRL_PMREQBLKRSPT           0                           ///< PM_REQ Block Response Time
#define V_CPU_DMI16_PGCTRL_PMREQBLKRSPT_5US       0x1                         ///< PM_REQ Block Response Time = 5us
#define V_CPU_DMI16_PGCTRL_PMREQBLKRSPT_10US      0x2                         ///< PM_REQ Block Response Time = 10us

#define R_CPU_DMI16_ADVMCTRL                      0x05BC                      ///< Hardware Save and Restore 367
#define B_CPU_DMI16_ADVMCTRL_PMREQBLKPGRSPT       (BIT7 | BIT6 | BIT5)        ///< PM_REQ Block and Power Gate Response Time
#define N_CPU_DMI16_ADVMCTRL_PMREQBLKPGRSPT       5                           ///< PM_REQ Block and Power Gate Response Time
#define V_CPU_DMI16_ADVMCTRL_PMREQBLKPGRSPT_5US   0x1                         ///< PM_REQ Block and Power Gate Response Time = 5us
#define V_CPU_DMI16_ADVMCTRL_PMREQBLKPGRSPT_10US  0x2                         ///< PM_REQ Block and Power Gate Response Time = 10us
#define V_CPU_DMI16_ADVMCTRL_PMREQBLKPGRSPT_35US  0x7                         ///< PM_REQ Block and Power Gate Response Time = 35us
#define B_CPU_DMI16_ADVMCTRL_PMREQCWC             (BIT18 | BIT17 | BIT16)     ///< PM_REQ_Clock Wake Control
#define N_CPU_DMI16_ADVMCTRL_PMREQCWC             16                          ///< PM_REQ_Clock Wake Control
#define V_CPU_DMI16_ADVMCTRL_PMREQCWC             6                           ///< PM_REQ_Clock Wake Control
#define B_CPU_DMI16_ADVMCTRL_F10BTSE              BIT24

#define  R_CPU_DMI16_DLFCAP                       0xA94
 #define  B_CPU_DMI16_DLFCAP_LSFCS                BIT0

#define R_CPU_DMI16_HWSNR368                      0x05C0                      ///< Hardware Save and Restore 368
 #define B_CPU_DMI16_HWSNR368_L1PGLTREN            BIT0                        ///< L1 Power Gating LTR Enable

#define  R_CPU_DMI16_PX16GLTCO1                   0x600
 #define  B_CPU_DMI16_PX16GLTCO1_L1357TCOE        BIT25
 #define  B_CPU_DMI16_PX16GLTCO1_L0246TCOE        BIT24

#define  R_CPU_DMI16_PCIEATL                            0x31C              ///< Ack Latency
 #define  B_CPU_DMI16_PCIEATL_G1X1                      BIT6
 #define  B_CPU_DMI16_PCIEATL_G1X1_OFFSET               6
 #define  B_CPU_DMI16_PCIEATL_G1X1_MASK                 0x1C0
 #define  V_CPU_DMI16_PCIEATL_G1X1                      0x7

#define  R_CPU_DMI16_PCIEATL1                           0x6A8              ///< Ack Latency 1
 #define  B_CPU_DMI16_PCIEATL1_G4X8                     BIT27
 #define  B_CPU_DMI16_PCIEATL1_G4X8_OFFSET              27
 #define  B_CPU_DMI16_PCIEATL1_G4X8_MASK                0x38000000
 #define  V_CPU_DMI16_PCIEATL1_G4X8                     0x2
 #define  B_CPU_DMI16_PCIEATL1_G4X4                     BIT24
 #define  B_CPU_DMI16_PCIEATL1_G4X4_OFFSET              24
 #define  B_CPU_DMI16_PCIEATL1_G4X4_MASK                0x7000000
 #define  V_CPU_DMI16_PCIEATL1_G4X4                     0x3
 #define  B_CPU_DMI16_PCIEATL1_G4X2                     BIT21
 #define  B_CPU_DMI16_PCIEATL1_G4X2_OFFSET              21
 #define  B_CPU_DMI16_PCIEATL1_G4X2_MASK                0xE00000
 #define  V_CPU_DMI16_PCIEATL1_G4X2                     0x4
 #define  B_CPU_DMI16_PCIEATL1_G4X1                     BIT18
 #define  B_CPU_DMI16_PCIEATL1_G4X1_OFFSET              18
 #define  B_CPU_DMI16_PCIEATL1_G4X1_MASK                0x1C0000
 #define  V_CPU_DMI16_PCIEATL1_G4X1                     0x4
 #define  B_CPU_DMI16_PCIEATL1_G3X8                     BIT15
 #define  B_CPU_DMI16_PCIEATL1_G3X8_OFFSET              15
 #define  B_CPU_DMI16_PCIEATL1_G3X8_MASK                0x38000
 #define  V_CPU_DMI16_PCIEATL1_G3X8                     0x2
 #define  B_CPU_DMI16_PCIEATL1_G3X16                    BIT12
 #define  B_CPU_DMI16_PCIEATL1_G3X16_OFFSET             12
 #define  B_CPU_DMI16_PCIEATL1_G3X16_MASK               0x7000
 #define  V_CPU_DMI16_PCIEATL1_G3X16                    0x1
 #define  B_CPU_DMI16_PCIEATL1_G2X8                     BIT9
 #define  B_CPU_DMI16_PCIEATL1_G2X8_OFFSET              9
 #define  B_CPU_DMI16_PCIEATL1_G2X8_MASK                0xE00
 #define  V_CPU_DMI16_PCIEATL1_G2X8                     0x1
 #define  B_CPU_DMI16_PCIEATL1_G2X16                    BIT6
 #define  B_CPU_DMI16_PCIEATL1_G2X16_OFFSET             6
 #define  B_CPU_DMI16_PCIEATL1_G2X16_MASK               0x1C0
 #define  V_CPU_DMI16_PCIEATL1_G2X16                    0x1
 #define  B_CPU_DMI16_PCIEATL1_G1X8                     BIT3
 #define  B_CPU_DMI16_PCIEATL1_G1X8_OFFSET              3
 #define  B_CPU_DMI16_PCIEATL1_G1X8_MASK                0x38
 #define  V_CPU_DMI16_PCIEATL1_G1X8                     0x1

#define R_CPU_DMI16_ACRG3                         0x06CC                      ///< Advance Control Register Group 3
#define B_CPU_DMI16_ACRG3_CPGWAKECTRL             (BIT23 | BIT22)             ///< CPG Wake Control
#define N_CPU_DMI16_ACRG3_CPGWAKECTRL             22                          ///< CPG Wake Control
#define V_CPU_DMI16_ACRG3_CPGWAKECTRL             0x0                         ///< CPG Wake Control = Disabled
#define V_CPU_DMI16_ACRG3_CPGWAKECTRL_8US         0x2                         ///< CPG Wake Control = 8 us


#define  R_CPU_DMI_AECR1G3                        0xC80
 #define  B_CPU_DMI_AECR1G3_TPSE                  BIT10
 #define  B_CPU_DMI_AECR1G3_DTCGCM                BIT14

#define R_CPU_DMI16_CWBCTL                        0x0820                      ///< Central Write Buffer Control
#define B_CPU_DMI16_CWBCTL_CWBE                   BIT0                        ///< Central Write Buffer Enable

#define B_CPU_DMI16_UPL1357TP            0x0F000000                  ///< Upstream Port Lane 1/3/5/7 Transmitter Preset Hint mask
#define N_CPU_DMI16_UPL1357TP            24                          ///< Upstream Port Lane 1/3/5/7 Transmitter Preset Hint value offset
#define B_CPU_DMI16_UPL0246TP            0x000000F0                  ///< Upstream Port Lane 0/2 Transmitter Preset Hint mask
#define N_CPU_DMI16_UPL0246TP            8                           ///< Upstream Port Lane 0/2 Transmitter Preset Hint value offset
#define V_CPU_DMI16_UPL0TP               7                           ///< Upstream Port Lane 0 Transmitter Preset Hint value
#define V_CPU_DMI16_UPL1TP               7                           ///< Upstream Port Lane 1 Transmitter Preset Hint value
#define V_CPU_DMI16_UPL2TP               7                           ///< Upstream Port Lane 2 Transmitter Preset Hint value
#define V_CPU_DMI16_UPL3TP               7                           ///< Upstream Port Lane 3 Transmitter Preset Hint value
#define V_CPU_DMI16_UPL4TP               7                           ///< Upstream Port Lane 4 Transmitter Preset Hint value
#define V_CPU_DMI16_UPL5TP               7                           ///< Upstream Port Lane 5 Transmitter Preset Hint value
#define V_CPU_DMI16_UPL6TP               7                           ///< Upstream Port Lane 6 Transmitter Preset Hint value
#define V_CPU_DMI16_UPL7TP               7                           ///< Upstream Port Lane 7 Transmitter Preset Hint value

#define R_CPU_DMI_ACGR3S2                0xC50
 #define  B_CPU_DMI_ACGR3S2_G4EBM        BIT9
 #define  B_CPU_DMI_ACGR3S2_G3EBM        BIT8
 #define  B_CPU_DMI_ACGR3S2_G2EBM        BIT7
 #define  B_CPU_DMI_ACGR3S2_G1EBM        BIT6
 #define  B_CPU_DMI_ACGR3S2_SRT          BIT5
 #define  B_CPU_DMI_ACGR3S2_LSTPTLS      BIT0
 #define  V_CPU_DMI_ACGR3S2_LSTPTLS      0x1
 #define  B_CPU_DMI_ACGR3S2_LSTPTLS_OFFSET   0

#define  R_CPU_DMI16_DCAP2               0x64
 #define  B_CPU_DMI16_DCAP2_PX10BTRS     BIT17
 #define  B_CPU_DMI16_DCAP2_PX10BTCS     BIT16

#define  R_CPU_DMI16_DCTL2               0x68
 #define  B_CPU_DMI16_DCTL2_PX10BTRE     BIT11
//
// Internal Link Configuration
//
#define R_CPU_DMI_PCR_LCAP                       0x204C                       ///< Link Capabilities
#define B_CPU_DMI_PCR_LCAP_EL1                   (BIT17 | BIT16 | BIT15)      ///< L1 Exit Latency
#define N_CPU_DMI_PCR_LCAP_EL1                   15                           ///< L1 Exit Latency
#define V_CPU_DMI_PCR_LCAP_EL1_8US_16US          0x4                          ///< L1 Exit Latency = 8us to less than 16us
#define B_CPU_DMI_PCR_LCAP_EL0                   (BIT14 | BIT13 | BIT12)      ///< L0 Exit Latency
#define N_CPU_DMI_PCR_LCAP_EL0                   12                           ///< L0 Exit Latency
#define V_CPU_DMI_PCR_LCAP_EL0_256NS_512NS       0x3                          ///< L0 Exit Latency = 256ns to less than 512ns
#define B_CPU_DMI_PCR_LCAP_APMS                  (BIT11 | BIT10)              ///< L0 is supported on DMI
#define B_CPU_DMI_PCR_LCAP_MLW                   0x000003F0
#define B_CPU_DMI_PCR_LCAP_MLS                   0x0000000F

#define  R_CPU_DMI_STRPFUSECFG                      0xFC
#define  B_CPU_DMI_STRPFUSECFG_PXIP_MASK            0xF000000

#define R_CPU_DMI16_L01EC                0x0A3C                      ///< Lane 0 and Lane 1 Equalization Control
#define R_CPU_DMI16_L23EC                0x0A40                      ///< Lane 2 and Lane 3 Equalization Control
#define R_CPU_DMI16_L45EC                0x0A44                      ///< Lane 4 and Lane 5 Equalization Control
#define R_CPU_DMI16_L67EC                0x0A48                      ///< Lane 6 and Lane 7 Equalization Control
 #define  B_CPU_DMI16_L01EC_DPL0246TP                     BIT0
 #define  B_CPU_DMI16_L01EC_DPL0246TP_OFFSET              0
 #define  B_CPU_DMI16_L01EC_DPL0246TP_MASK                0xF
 #define  B_CPU_DMI16_L01EC_DPL0246RPH                    BIT4
 #define  B_CPU_DMI16_L01EC_DPL0246RPH_OFFSET             4
 #define  B_CPU_DMI16_L01EC_DPL0246RPH_MASK               0x70
 #define  B_CPU_DMI16_L01EC_DPL1357TP                     BIT16
 #define  B_CPU_DMI16_L01EC_DPL1357TP_OFFSET              16
 #define  B_CPU_DMI16_L01EC_DPL1357TP_MASK                0xF0000
 #define  B_CPU_DMI16_L01EC_DPL1357RPH                    BIT20
 #define  B_CPU_DMI16_L01EC_DPL1357RPH_OFFSET             20
 #define  B_CPU_DMI16_L01EC_DPL1357RPH_MASK               0x700000

 #define  B_CPU_DMI16_L01EC_UPL0246TP                     BIT8
 #define  B_CPU_DMI16_L01EC_UPL0246TP_OFFSET              8
 #define  B_CPU_DMI16_L01EC_UPL0246TP_MASK                0xF00
 #define  B_CPU_DMI16_L01EC_UPL0246RPH                    BIT12
 #define  B_CPU_DMI16_L01EC_UPL0246RPH_OFFSET             12
 #define  B_CPU_DMI16_L01EC_UPL0246RPH_MASK               0x7000
 #define  B_CPU_DMI16_L01EC_UPL1357TP                     BIT24
 #define  B_CPU_DMI16_L01EC_UPL1357TP_OFFSET              24
 #define  B_CPU_DMI16_L01EC_UPL1357TP_MASK                0xF000000
 #define  B_CPU_DMI16_L01EC_UPL1357RPH                    BIT28
 #define  B_CPU_DMI16_L01EC_UPL1357RPH_OFFSET             28
 #define  B_CPU_DMI16_L01EC_UPL1357RPH_MASK               0x70000000

#define  R_CPU_DMI16_PL16L01EC                     0xABC                      ///< Physical Layer 16.0 GT/s Lane 01 Equalization Control Register
#define  R_CPU_DMI16_PL16L23EC                     0xABE                      ///< Physical Layer 16.0 GT/s Lane 23 Equalization Control Register
#define  R_CPU_DMI16_PL16L45EC                     0xAC0                      ///< Physical Layer 16.0 GT/s Lane 45 Equalization Control Register
#define  R_CPU_DMI16_PL16L67EC                     0xAC2                      ///< Physical Layer 16.0 GT/s Lane 67 Equalization Control Register
 #define  B_CPU_DMI16_PL16L01EC_UP16L1357TP_MASK         0x0000F000
 #define  B_CPU_DMI16_PL16L01EC_UP16L1357TP              BIT12
 #define  B_CPU_DMI16_PL16L01EC_UP16L1357TP_OFFSET       12
 #define  B_CPU_DMI16_PL16L01EC_DP16L1357TP_MASK         0x00000F00
 #define  B_CPU_DMI16_PL16L01EC_DP16L1357TP              BIT8
 #define  B_CPU_DMI16_PL16L01EC_DP16L1357TP_OFFSET       8
 #define  B_CPU_DMI16_PL16L01EC_UP16L0246TP_MASK         0xF0
 #define  B_CPU_DMI16_PL16L01EC_UP16L0246TP              BIT4
 #define  B_CPU_DMI16_PL16L01EC_UP16L0246TP_OFFSET       4
 #define  B_CPU_DMI16_PL16L01EC_DP16L0246TP              BIT0
 #define  B_CPU_DMI16_PL16L01EC_DP16L0246TP_OFFSET       0
 #define  B_CPU_DMI16_PL16L01EC_DP16L0246TP_MASK         0xF
//
// Advanced Error Reporting Capability (CAPID:0001h)
//
#define R_CPU_DMI_AECH                                    0x100
#define V_CPU_DMI_EX_AEC_CID                              0x0001 ///< Capability ID
#define N_CPU_DMI_EXCAP_CV                                16
#define V_CPU_DMI_CV                                      0x1
//
// Secondary PCI Express Extended Capability Header (CAPID:0019h)
//
#define R_CPU_DMI_SPEECH                                  0xA30
#define V_CPU_DMI_EX_SPE_CID                              0x0019 ///< Capability ID
//
// Data Link Feature Extended Capability Header (CAPID:0025h)
//
#define R_CPU_DMI_DLFECH                                  0xA90
#define V_CPU_DMI_EX_DLFECH_CID                           0x0025 ///< Capability ID
#define  R_CPU_DMI16_RXMC1                                   0xC90
 #define  B_CPU_DMI16_RXMC1_MSRVS                            BIT26
 #define  B_CPU_DMI16_RXMC1_MSRVS_OFFSET                     26
 #define  B_CPU_DMI16_RXMC1_MSRVS_MASK                       0xFC000000
 #define  V_CPU_DMI16_RXMC1_MSRVS                            0x1F
 #define  B_CPU_DMI16_RXMC1_MSRTS                            BIT20
 #define  B_CPU_DMI16_RXMC1_MSRTS_OFFSET                     20
 #define  B_CPU_DMI16_RXMC1_MSRTS_MASK                       0x3F00000
 #define  V_CPU_DMI16_RXMC1_MSRTS                            0x3F
 #define  B_CPU_DMI16_RXMC1_TMSLOP                           BIT13
 #define  B_CPU_DMI16_RXMC1_TMSLOP_OFFSET                    13
 #define  B_CPU_DMI16_RXMC1_TMSLOP_MASK                      0x6000
 #define  V_CPU_DMI16_RXMC1_TMSLOP                           0x1
 #define  B_CPU_DMI16_RXMC1_VMSLOP                           BIT11
 #define  B_CPU_DMI16_RXMC1_VMSLOP_OFFSET                    11
 #define  B_CPU_DMI16_RXMC1_VMSLOP_MASK                      0x1800
 #define  V_CPU_DMI16_RXMC1_VMSLOP                           0x1
 #define  B_CPU_DMI16_RXMC1_MSRM                             BIT9
 #define  B_CPU_DMI16_RXMC1_MMNOLS                           BIT4
 #define  B_CPU_DMI16_RXMC1_MMNOLS_OFFSET                    4
 #define  B_CPU_DMI16_RXMC1_MMNOLS_MASK                      0x1F0
 #define  V_CPU_DMI16_RXMC1_MMNOLS                           0x1
 #define  B_CPU_DMI16_RXMC1_MVS                              BIT3
 #define  B_CPU_DMI16_RXMC1_MILRTS                           BIT2
 #define  B_CPU_DMI16_RXMC1_MIUDVMS                          BIT1
 #define  B_CPU_DMI16_RXMC1_MIESS                            BIT0

#define  R_CPU_DMI16_RXMC2                                   0xC94
 #define  B_CPU_DMI16_RXMC2_MNOTSS                           BIT19
 #define  B_CPU_DMI16_RXMC2_MNOTSS_OFFSET                    19
 #define  B_CPU_DMI16_RXMC2_MNOTSS_MASK                      0x1F80000
 #define  V_CPU_DMI16_RXMC2_MNOTSS                           0x20
 #define  B_CPU_DMI16_RXMC2_MMTOS                            BIT13
 #define  B_CPU_DMI16_RXMC2_MMTOS_OFFSET                     13
 #define  B_CPU_DMI16_RXMC2_MMTOS_MASK                       0x7E000
 #define  V_CPU_DMI16_RXMC2_MMTOS                            0x28
 #define  B_CPU_DMI16_RXMC2_MNOVSS                           BIT6
 #define  B_CPU_DMI16_RXMC2_MNOVSS_OFFSET                    6
 #define  B_CPU_DMI16_RXMC2_MNOVSS_MASK                      0x1FC0
 #define  V_CPU_DMI16_RXMC2_MNOVSS                           0x28
 #define  B_CPU_DMI16_RXMC2_MMVOS                            BIT0
 #define  B_CPU_DMI16_RXMC2_MMVOS_OFFSET                     0
 #define  B_CPU_DMI16_RXMC2_MMVOS_MASK                       0x3F
 #define  V_CPU_DMI16_RXMC2_MMVOS                            0x0

#define  R_CPU_DMI16_COCTL                                   0x594
 #define  B_CPU_DMI16_COCTL_NPCLM                            BIT15
 #define  V_CPU_DMI16_COCTL_NPCLM                            0x2
 #define  B_CPU_DMI16_COCTL_NPCLM_OFFSET                     15
 #define  B_CPU_DMI16_COCTL_NPCLM_MASK                       0x18000
 #define  B_CPU_DMI16_COCTL_PCLM                             BIT13
 #define  B_CPU_DMI16_COCTL_PCLM_OFFSET                      13
 #define  V_CPU_DMI16_COCTL_PCLM                             0x2
 #define  B_CPU_DMI16_COCTL_PCLM_MASK                        0x6000
 #define  B_CPU_DMI16_COCTL_MAGPARCD                         BIT12
 #define  B_CPU_DMI16_COCTL_MAGPARCD_OFFSET                  12
 #define  B_CPU_DMI16_COCTL_ROAOP                            BIT11
 #define  B_CPU_DMI16_COCTL_ROAOP_OFFSET                     11
 #define  B_CPU_DMI16_COCTL_CTE                              BIT10
 #define  B_CPU_DMI16_COCTL_CTE_OFFSET                       10
 #define  B_CPU_DMI16_COCTL_CT                               BIT2
 #define  B_CPU_DMI16_COCTL_CT_OFFSET                        2
 #define  B_CPU_DMI16_COCTL_CT_MASK                          0x3FC
 #define  V_CPU_DMI16_COCTL_CT                               0x23
 #define  B_CPU_DMI16_COCTL_DDCE                             BIT1
 #define  B_CPU_DMI16_COCTL_DDCE_OFFSET                      1
 #define  B_CPU_DMI16_COCTL_PWCE                             BIT0
 #define  B_CPU_DMI16_COCTL_PWCE_OFFSET                      0

#define  R_CPU_DMI16_FOMS                              0x464
 #define  B_CPU_DMI16_FOMS_I                           (BIT31 | BIT30 | BIT29)
 #define  B_CPU_DMI16_FOMS_I_OFFSET                    29
 #define  B_CPU_DMI16_FOMS_LN                          0x1F000000
 #define  B_CPU_DMI16_FOMS_LN_OFFSET                   24
 #define  B_CPU_DMI16_FOMS_FOMSV                       0x00FFFFFF
 #define  B_CPU_DMI16_FOMS_FOMSV0                      0x000000FF
 #define  B_CPU_DMI16_FOMS_FOMSV0_OFFSET               0
 #define  B_CPU_DMI16_FOMS_FOMSV1                      0x0000FF00
 #define  B_CPU_DMI16_FOMS_FOMSV1_OFFSET               8
 #define  B_CPU_DMI16_FOMS_FOMSV2                      0x00FF0000
 #define  B_CPU_DMI16_FOMS_FOMSV2_OFFSET               16

#define  R_CPU_DMI16_MM                                0x480
 #define  B_CPU_DMI16_MM_MSST                          BIT8
 #define  B_CPU_DMI16_MM_MSST_OFFSET                   8
 #define  B_CPU_DMI16_MM_MSST_MASK                     0xFFFFFF00
 #define  B_CPU_DMI16_MM_MSS                           BIT0
 #define  B_CPU_DMI16_MM_MSS_OFFSET                    0
 #define  B_CPU_DMI16_MM_MSS_MASK                      0xFF

#define R_CPU_DMI16_CDM                                0x484
 #define B_CPU_DMI16_CDM_MSS                           BIT4
 #define B_CPU_DMI16_CDM_MSS_OFFSET                    4
 #define B_CPU_DMI16_CDM_MSS_MASK                      0x0F000000

#define  R_CPU_DMI16_CAPP                              0x34

#define  R_CPU_DMI16_MID                               0x80
 #define  B_CPU_DMI16_MID_NEXT                         BIT8
 #define  N_CPU_DMI16_MID_NEXT                         8
 #define  B_CPU_DMI16_MID_NEXT_MASK                    0xFF00
 #define  B_CPU_DMI16_MID_CID                          BIT0
 #define  B_CPU_DMI16_MID_CID_OFFSET                   0
 #define  B_CPU_DMI16_MID_CID_MASK                     0xFF
 #define  V_CPU_DMI16_MID_NEXT                         0xA0
 #define  V_CPU_DMI16_MID_CID                          0x5

#define  R_CPU_DMI16_SVCAP                             0x90
 #define  B_CPU_DMI16_SVCAP_NEXT                       BIT8
 #define  N_CPU_DMI16_SVCAP_NEXT                       8
 #define  B_CPU_DMI16_SVCAP_NEXT_MASK                  0xFF00
 #define  B_CPU_DMI16_SVCAP_CID                        BIT0
 #define  B_CPU_DMI16_SVCAP_CID_OFFSET                 0
 #define  B_CPU_DMI16_SVCAP_CID_MASK                   0xFF
 #define  V_CPU_DMI16_SVCAP_NEXT                       0xA0
 #define  V_CPU_DMI16_SVCAP_CID                        0x0D

#define  R_CPU_DMI16_SVID                              0x94
 #define  B_CPU_DMI16_SVID_SID                         BIT16
 #define  B_CPU_DMI16_SVID_SID_OFFSET                  16
 #define  B_CPU_DMI16_SVID_SID_MASK                    0xFFFF0000
 #define  B_CPU_DMI16_SVID_SVID                        BIT0
 #define  B_CPU_DMI16_SVID_SVID_OFFSET                 0
 #define  B_CPU_DMI16_SVID_SVID_MASK                   0xFFFF

#define  R_CPU_DMI16_PMCAP                             0xA0
 #define  B_CPU_DMI16_PMCAP_NEXT                       BIT8
 #define  N_CPU_DMI16_PMCAP_NEXT                       8
 #define  B_CPU_DMI16_PMCAP_NEXT_MASK                  0xFF00
 #define  B_CPU_DMI16_PMCAP_CID                        BIT0
 #define  B_CPU_DMI16_PMCAP_CID_OFFSET                 0
 #define  B_CPU_DMI16_PMCAP_CID_MASK                   0xFF
 #define  V_CPU_DMI16_PMCAP_NEXT                       0x00
 #define  V_CPU_DMI16_PMCAP_CID                        0x1

#define  R_CPU_DMI16_AECH                              0x100
 #define  B_CPU_DMI16_AECH_NCO                         BIT20
 #define  B_CPU_DMI16_AECH_NCO_OFFSET                  20
 #define  B_CPU_DMI16_AECH_NCO_MASK                    0xFFF00000
 #define  V_CPU_DMI16_AECH_NCO                         0x280
 #define  B_CPU_DMI16_AECH_CV                          BIT16
 #define  B_CPU_DMI16_AECH_CV_OFFSET                   16
 #define  B_CPU_DMI16_AECH_CV_MASK                     0xF0000
 #define  V_CPU_DMI16_AECH_CV                          0x1
 #define  V_CPU_DMI16_AECH_CID                         0x1
 #define  B_CPU_DMI16_AECH_CID_OFFSET                  0
 #define  B_CPU_DMI16_AECH_CID_MASK                    0xFFFF

#define  R_CPU_DMI16_VCCH                              0x280
 #define  B_CPU_DMI16_VCCH_NCO                         BIT20
 #define  B_CPU_DMI16_VCCH_NCO_OFFSET                  20
 #define  B_CPU_DMI16_VCCH_NCO_MASK                    0xFFF00000
 #define  V_CPU_DMI16_VCCH_NCO                         0xA30
 #define  B_CPU_DMI16_VCCH_CV                          BIT16
 #define  B_CPU_DMI16_VCCH_CV_OFFSET                   16
 #define  B_CPU_DMI16_VCCH_CV_MASK                     0xF0000
 #define  V_CPU_DMI16_VCCH_CV                          0x1
 #define  V_CPU_DMI16_VCCH_CID                         0x2
 #define  B_CPU_DMI16_VCCH_CID_OFFSET                  0
 #define  B_CPU_DMI16_VCCH_CID_MASK                    0xFFFF

#define  R_CPU_DMI16_SPEECH                            0xA30
 #define  B_CPU_DMI16_SPEECH_NCO                       BIT20
 #define  B_CPU_DMI16_SPEECH_NCO_OFFSET                20
 #define  B_CPU_DMI16_SPEECH_NCO_MASK                  0xFFF00000
 #define  V_CPU_DMI16_SPEECH_NCO                       0xA90
 #define  B_CPU_DMI16_SPEECH_CV                        BIT16
 #define  B_CPU_DMI16_SPEECH_CV_OFFSET                 16
 #define  B_CPU_DMI16_SPEECH_CV_MASK                   0xF0000
 #define  V_CPU_DMI16_SPEECH_CV                        0x1
 #define  V_CPU_DMI16_SPEECH_CID                       0x19
 #define  B_CPU_DMI16_SPEECH_CID_OFFSET                0
 #define  B_CPU_DMI16_SPEECH_CID_MASK                  0xFFFF

#define  R_CPU_DMI16_DLFECH                            0xA90
 #define  B_CPU_DMI16_DLFECH_NCO                       BIT20
 #define  B_CPU_DMI16_DLFECH_NCO_OFFSET                20
 #define  B_CPU_DMI16_DLFECH_NCO_MASK                  0xFFF00000
 #define  V_CPU_DMI16_DLFECH_NCO                       0xA9C
 #define  B_CPU_DMI16_DLFECH_CV                        BIT16
 #define  B_CPU_DMI16_DLFECH_CV_OFFSET                 16
 #define  B_CPU_DMI16_DLFECH_CV_MASK                   0xF0000
 #define  V_CPU_DMI16_DLFECH_CV                        0x1
 #define  V_CPU_DMI16_DLFECH_CID                       0x25
 #define  B_CPU_DMI16_DLFECH_CID_OFFSET                0
 #define  B_CPU_DMI16_DLFECH_CID_MASK                  0xFFFF

#define  R_CPU_DMI16_PL16GECH                          0xA9C
 #define  B_CPU_DMI16_PL16GECH_NCO                     BIT20
 #define  B_CPU_DMI16_PL16GECH_NCO_OFFSET              20
 #define  B_CPU_DMI16_PL16GECH_NCO_MASK                0xFFF00000
 #define  V_CPU_DMI16_PL16GECH_NCO                     0xEDC
 #define  B_CPU_DMI16_PL16GECH_CV                      BIT16
 #define  B_CPU_DMI16_PL16GECH_CV_OFFSET               16
 #define  B_CPU_DMI16_PL16GECH_CV_MASK                 0xF0000
 #define  V_CPU_DMI16_PL16GECH_CV                      0x1
 #define  V_CPU_DMI16_PL16GECH_CID                     0x26
 #define  B_CPU_DMI16_PL16GECH_CID_OFFSET              0
 #define  B_CPU_DMI16_PL16GECH_CID_MASK                0xFFFF

#define  R_CPU_DMI16_PL16MECH                          0xEDC
 #define  B_CPU_DMI16_PL16MECH_NCO                     BIT20
 #define  B_CPU_DMI16_PL16MECH_NCO_OFFSET              20
 #define  B_CPU_DMI16_PL16MECH_NCO_MASK                0xFFF00000
 #define  V_CPU_DMI16_PL16MECH_NCO                     0x0
 #define  B_CPU_DMI16_PL16MECH_CV                      BIT16
 #define  B_CPU_DMI16_PL16MECH_CV_OFFSET               16
 #define  B_CPU_DMI16_PL16MECH_CV_MASK                 0xF0000
 #define  V_CPU_DMI16_PL16MECH_CV                      0x1
 #define  V_CPU_DMI16_PL16MECH_CID                     0x27
 #define  B_CPU_DMI16_PL16MECH_CID_OFFSET              0
 #define  B_CPU_DMI16_PL16MECH_CID_MASK                0xFFFF

#define  R_CPU_DMI16_RXQC                              0xC7C
 #define  B_CPU_DMI16_RXQC_VMRQNPCOC_MASK              (BIT15 | BIT14)
 #define  B_CPU_DMI16_RXQC_VMRQNPCOC_OFFSET            14
 #define  V_CPU_DMI16_RXQC_VMRQNPCOC                   0x1
 #define  B_CPU_DMI16_RXQC_VMRQPCOC_MASK               (BIT13 | BIT12)
 #define  B_CPU_DMI16_RXQC_VMRQPCOC_OFFSET             12
 #define  V_CPU_DMI16_RXQC_VMRQPCOC                    0x1
 #define  B_CPU_DMI16_RXQC_V1RQNPCOC_MASK              (BIT9 | BIT8)
 #define  B_CPU_DMI16_RXQC_V1RQNPCOC_OFFSET            8
 #define  V_CPU_DMI16_RXQC_V1RQNPCOC                   0x1
 #define  B_CPU_DMI16_RXQC_V1RQPCOC_MASK               (BIT7 | BIT6)
 #define  B_CPU_DMI16_RXQC_V1RQPCOC_OFFSET             6
 #define  V_CPU_DMI16_RXQC_V1RQPCOC                    0x1
 #define  B_CPU_DMI16_RXQC_V0RQNPCOC_MASK              (BIT3 | BIT2)
 #define  B_CPU_DMI16_RXQC_V0RQNPCOC_OFFSET            2
 #define  V_CPU_DMI16_RXQC_V0RQNPCOC                   0x1
 #define  B_CPU_DMI16_RXQC_V0RQPCOC_MASK               (BIT1 | BIT0)
 #define  B_CPU_DMI16_RXQC_V0RQPCOC_OFFSET             0
 #define  V_CPU_DMI16_RXQC_V0RQPCOC                    0x1

#define  R_CPU_DMI16_BCTRL                             0x3E
 #define  B_CPU_DMI16_BCTRL_SE                         BIT1

#define  B_CPU_DMI16_CMD_SEE                           BIT8

#endif
