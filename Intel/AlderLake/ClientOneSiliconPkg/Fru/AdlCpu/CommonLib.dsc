## @file
#  Component description file for the AlderLake CPU Common FRU libraries.
#
# @copyright
#  INTEL CONFIDENTIAL
#  Copyright 2018 - 2020 Intel Corporation.
#
#  The source code contained or described herein and all documents related to the
#  source code ("Material") are owned by Intel Corporation or its suppliers or
#  licensors. Title to the Material remains with Intel Corporation or its suppliers
#  and licensors. The Material may contain trade secrets and proprietary and
#  confidential information of Intel Corporation and its suppliers and licensors,
#  and is protected by worldwide copyright and trade secret laws and treaty
#  provisions. No part of the Material may be used, copied, reproduced, modified,
#  published, uploaded, posted, transmitted, distributed, or disclosed in any way
#  without Intel's prior express written permission.
#
#  No license under any patent, copyright, trade secret or other intellectual
#  property right is granted to or conferred upon you by disclosure or delivery
#  of the Materials, either expressly, by implication, inducement, estoppel or
#  otherwise. Any license under such intellectual property rights must be
#  express and approved by Intel in writing.
#
#  Unless otherwise agreed by Intel in writing, you may not remove or alter
#  this notice or any other notice embedded in Materials by Intel or
#  Intel's suppliers or licensors in any way.
#
#  This file contains an 'Intel Peripheral Driver' and is uniquely identified as
#  "Intel Reference Module" and is licensed for Intel CPUs and chipsets under
#  the terms of your license agreement with Intel or your vendor. This file may
#  be modified by the user, subject to additional terms of the license agreement.
#
#@par Specification Reference:
#
##

IpuLib|$(PLATFORM_SI_PACKAGE)/IpBlock/Ipu/Library/PeiDxeSmmIpuLib/IpuLib.inf
VtdInfoLib|$(PLATFORM_SI_PACKAGE)/IpBlock/Vtd/Library/PeiDxeSmmVtdInfoLib/PeiDxeSmmVtdInfoLibVer1.inf
CpuPcieRpLib|$(PLATFORM_SI_PACKAGE)/IpBlock/CpuPcieRp/Library/PeiDxeSmmCpuPcieRpLib/PeiDxeSmmCpuPcieRpLib.inf
GnaInfoLib|$(PLATFORM_SI_PACKAGE)/IpBlock/Gna/Library/PeiDxeSmmGnaInfoLib/PeiDxeSmmGnaInfoLib.inf
TelemetryLib|$(PLATFORM_SI_PACKAGE)/IpBlock/Telemetry/Library/PeiDxeTelemetryLib/PeiDxeTelemetryLib.inf
TelemetryPrivateLib|$(PLATFORM_SI_PACKAGE)/IpBlock/Telemetry/LibraryPrivate/PeiDxeTelemetryPrivateLib/PeiDxeTelemetryPrivateLib.inf
VmdInfoLib|$(PLATFORM_SI_PACKAGE)/IpBlock/Vmd/Library/VmdInfoLib/VmdInfoLib.inf
CpuPcieInfoFruLib|$(PLATFORM_SI_PACKAGE)/Fru/AdlCpu/CpuPcieRp/Library/PeiDxeSmmCpuPcieInfoFruLib/PeiDxeSmmCpuPcieInfoFruLib.inf
CpuDmiInfoFruLib|$(PLATFORM_SI_PACKAGE)/Fru/AdlCpu/CpuDmi/Library/PeiDxeSmmCpuDmiInfoFruLib/PeiDxeSmmCpuDmiInfoFruLib.inf
HybridGraphicsInfoFruLib|$(PLATFORM_SI_PACKAGE)/Fru/AdlCpu/HybridGraphics/Library/PeiDxeSmmHybridGraphicsInfoFruLib/PeiDxeSmmHybridGraphicsInfoFruLib.inf
MsrFruLib|$(PLATFORM_SI_PACKAGE)/Fru/AdlCpu/LibraryPrivate/BaseMsrFruLib/BaseMsrFruLib.inf
PeiDxeTelemetryFruLib|$(PLATFORM_SI_PACKAGE)/Fru/AdlCpu/Telemetry/LibraryPrivate/PeiDxeTelemetryFruLib/PeiDxeTelemetryFruLib.inf
CpuInfoFruLib|$(PLATFORM_SI_PACKAGE)/Fru/AdlCpu/LibraryPrivate/BaseCpuInfoFruLib/BaseCpuInfoFruLib.inf
TccLib|$(PLATFORM_SI_PACKAGE)/IpBlock/Tcc/Library/PeiDxeSmmTccLib/PeiDxeSmmTccLib.inf
BaseTraceHubInfoFruLib|$(PLATFORM_SI_PACKAGE)/Fru/AdlCpu/TraceHub/LibraryPrivate/BaseTraceHubInfoFruLib/BaseTraceHubInfoFruLib.inf
BaseIpuFruLib|$(PLATFORM_SI_PACKAGE)/Fru/AdlCpu/Ipu/LibraryPrivate/BaseIpuFruLib/BaseIpuFruLib.inf
TmeInfoLib|$(PLATFORM_SI_PACKAGE)/IpBlock/Tme/Library/TmeInfoLib.inf
!if gSiPkgTokenSpaceGuid.PcdOverclockEnable == TRUE
OcFruLib|$(PLATFORM_SI_PACKAGE)/Fru/AdlCpu/Overclocking/LibraryPrivate/BaseOcFruLib/BaseOcFruLib.inf
!endif
