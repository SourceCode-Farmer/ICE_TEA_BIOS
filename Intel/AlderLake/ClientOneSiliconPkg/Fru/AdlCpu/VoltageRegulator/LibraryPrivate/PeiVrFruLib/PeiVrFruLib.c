/** @file
  This file updates voltage regulator overrides and  programs Fivr Rfi settings.

@copyright
  INTEL CONFIDENTIAL
  Copyright 2019 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains an 'Intel Peripheral Driver' and is uniquely identified as
  "Intel Reference Module" and is licensed for Intel CPUs and chipsets under
  the terms of your license agreement with Intel or your vendor. This file may
  be modified by the user, subject to additional terms of the license agreement.

@par Specification
**/

#include <Library/DebugLib.h>
#include <Library/PeiServicesLib.h>
#include <Library/CpuMailboxLib.h>
#include <Library/ConfigBlockLib.h>
#include <Library/CpuPlatformLib.h>
#include <CpuPowerMgmt.h>
#include <Library/IoLib.h>
#include <Library/MemoryAllocationLib.h>
#include <Ppi/SiPolicy.h>
#include <Register/Cpuid.h>
#include <Library/PeiPmcPrivateLib.h>
#include <Library/PciSegmentLib.h>
#include <Library/VoltageRegulatorDomains.h>
#include <VoltageRegulatorCommands.h>
#include <Library/PeiVrLib.h>
#include <Register/SaRegsHostBridge.h>
#include <Library/CpuInfoFruLib.h>
#include <Library/PeiVrDomainLib.h>

#define TDC_LIMIT_255A      1020
#define VOLTAGE_LIMIT_0_7V  700
#define IA_IMON_SLOPE_110A  110
#define MCHBAR_MMIO_SCRATCHPAD0          0x5428

#define PSI1_THRESHOLD  0x3FF
#define PSI2_THRESHOLD  0
#define PSI3_THRESHOLD  0
#define PSI3_DISABLE    1
#define PSI4_DISABLE    0

#define SLOW_SLEW_RATE_FAST_2    0

GLOBAL_REMOVE_IF_UNREFERENCED CPU_VR_OVERRIDE_TABLE mCpuVrOverrideTable[] = {
///
/// Cpu Identifier                   IaIccMax    GtIccMax    SaIccMax    IaTdc     IaTdcIrms     IaTdcTime     GtTdc       SaTdc       IaAcLL       IaDcLL        GtAcLL        GtDcLL        SaAcLL      SaDcLL
///                                   1/4A        1/4A       1/4A         1/8A        1/8A        Sec          1/8A        1/8A      1/100mOhm    1/100mOhm     1/100mOhm    1/100mOhm     1/100mOhm    1/100mOhm
  { EnumAdlS35Watt881fCpuId,          154*4,      30*4,         0,        101*8,      56*8,        28,         20*8,         0,        170,        170,          400,           400,            0,           0},
  { EnumAdlS65Watt881fCpuId,          240*4,      30*4,         0,        160*8,      89*8,        28,         22*8,         0,        110,        110,          400,           400,            0,           0},
  { EnumAdlS125Watt881fCpuId,         280*4,      30*4,         0,        184*8,     132*8,        56,         22*8,         0,        110,        110,          400,           400,            0,           0},
  { EnumAdlS150Watt881fCpuId,         280*4,      30*4,         0,        208*8,     132*8,        56,         22*8,         0,        110,        110,          400,           400,            0,           0},
  { EnumAdlS35Watt841fCpuId,          145*4,      30*4,         0,         85*8,      49*8,        28,         20*8,         0,        170,        170,          400,           400,            0,           0},
  { EnumAdlS65Watt841fCpuId,          220*4,      30*4,         0,        130*8,      77*8,        28,         22*8,         0,        110,        110,          400,           400,            0,           0},
  { EnumAdlS125Watt841fCpuId,         240*4,      30*4,         0,        135*8,     109*8,        56,         22*8,         0,        110,        110,          400,           400,            0,           0},
  { EnumAdlS35Watt641fCpuId,          154*4,      30*4,         0,        101*8,      56*8,        28,         20*8,         0,        170,        170,          400,           400,            0,           0},
  { EnumAdlS65Watt641fCpuId,          240*4,      30*4,         0,        160*8,      89*8,        28,         22*8,         0,        110,        110,          400,           400,            0,           0},
  { EnumAdlS125Watt641fCpuId,         175*4,      30*4,         0,        105*8,      96*8,        28,         22*8,         0,        110,        110,          400,           400,            0,           0},
  { EnumAdlS35Watt681fCpuId,          154*4,      30*4,         0,        101*8,      56*8,        28,         20*8,         0,        170,        170,          400,           400,            0,           0},
  { EnumAdlS35Watt801fCpuId,          154*4,      30*4,         0,        101*8,      56*8,        28,         20*8,         0,        170,        170,          400,           400,            0,           0},
  { EnumAdlS65Watt801fCpuId,          240*4,      30*4,         0,        160*8,      89*8,        28,         22*8,         0,        110,        110,          400,           400,            0,           0},
  { EnumAdlS35Watt601fCpuId,          100*4,      30*4,         0,         65*8,      44*8,        28,         20*8,         0,        170,        170,          400,           400,            0,           0},
  { EnumAdlS65Watt601fCpuId,          151*4,      30*4,         0,         90*8,      66*8,        28,         22*8,         0,        170,        170,          400,           400,            0,           0},
  { EnumAdlS35Watt401fCpuId,           90*4,      30*4,         0,         57*8,      40*8,        28,         20*8,         0,        170,        170,          400,           400,            0,           0},
  { EnumAdlS65Watt401fCpuId,          110*4,      30*4,         0,         74*8,      60*8,        28,         22*8,         0,        170,        170,          400,           400,            0,           0},
  { EnumAdlS35Watt201fCpuId,           37*4,      30*4,         0,         30*8,      30*8,        28,         20*8,         0,        170,        170,          400,           400,            0,           0},
  { EnumAdlS46Watt201fCpuId,           49*4,      30*4,         0,         39*8,      39*8,        28,         20*8,         0,        170,        170,          400,           400,            0,           0},
  { EnumAdlS65Watt201fCpuId,           37*4,      30*4,         0,         30*8,      30*8,        28,         20*8,         0,        170,        170,          400,           400,            0,           0},
  { EnumAdlS58Watt401fCpuId,          110*4,      30*4,         0,         74*8,      60*8,        28,         22*8,         0,        170,        170,          400,           400,            0,           0},
  { EnumAdlS60Watt401fCpuId,          110*4,      30*4,         0,         74*8,      60*8,        28,         22*8,         0,        170,        170,          400,           400,            0,           0},
  { EnumAdlS63Watt401fCpuId,          110*4,      30*4,         0,         74*8,      60*8,        28,         22*8,         0,        170,        170,          400,           400,            0,           0},
  { EnumAdlS65WattBga881fCpuId,       240*4,      30*4,         0,        160*8,      89*8,        28,         21*8,         0,        110,        110,          400,           400,            0,           0},
  { EnumAdlS65WattBga841fCpuId,       240*4,      30*4,         0,        160*8,      89*8,        28,         21*8,         0,        110,        110,          400,           400,            0,           0},
  { EnumAdlS65WattBga441fCpuId,       200*4,      30*4,         0,        133*8,      77*8,        28,         21*8,         0,        110,        110,          400,           400,            0,           0},
  { EnumAdlS65WattBga601fCpuId,       200*4,      30*4,         0,        133*8,      85*8,        28,         21*8,         0,        110,        110,          400,           400,            0,           0},
  { EnumAdlS65WattBga681fCpuId,       200*4,      30*4,         0,        133*8,      85*8,        28,         21*8,         0,        110,        110,          400,           400,            0,           0},
  { EnumAdlS65WattBga401fCpuId,       200*4,      30*4,         0,        133*8,      77*8,        28,         21*8,         0,        110,        110,          400,           400,            0,           0},
  { EnumAdlS55WattHBga881fCpuId,      200*4,      30*4,         0,        133*8,      77*8,        56,         20*8,         0,        170,        170,          400,           400,            0,           0},
  { EnumAdlS55WattHBga681fCpuId,      200*4,      30*4,         0,        133*8,      77*8,        56,         20*8,         0,        170,        170,          400,           400,            0,           0},
  { EnumAdlS55WattHBga481fCpuId,      160*4,      30*4,         0,        106*8,      72*8,        56,         20*8,         0,        170,        170,          400,           400,            0,           0},
  { EnumAdlS55WattHBga441fCpuId,      120*4,      30*4,         0,         76*8,      58*8,        56,         20*8,         0,        170,        170,          400,           400,            0,           0},
//[start-220113-STORM1125-modify]
#if defined(C770_SUPPORT)
  { EnumAdlP15Watt282fCpuId,           80*4,      40*4,         0,         43*8,      43*8,         1,         23*8,         0,        280,        280,          320,           320,            0,           0},
#else
  { EnumAdlP15Watt282fCpuId,           80*4,      40*4,         0,         43*8,      19*8,        42,         23*8,         0,        280,        280,          320,           320,            0,           0},
#endif
//[end-220113-STORM1125-modify]
  { EnumAdlP15Watt142fCpuId,           80*4,      40*4,         0,         43*8,      19*8,        42,         23*8,         0,        280,        280,          320,           320,            0,           0},
  { EnumAdlP15Watt242fCpuId,           80*4,      40*4,         0,         43*8,      19*8,        42,         23*8,         0,        280,        280,          320,           320,            0,           0},
  { EnumAdlP28Watt282fCpuId,           85*4,      55*4,         0,         63*8,      63*8,         1,         30*8,         0,        230,        230,          320,           320,            0,           0},
  { EnumAdlP28Watt142fCpuId,           85*4,      55*4,         0,         63*8,      39*8,        28,         30*8,         0,        230,        230,          320,           320,            0,           0},
  { EnumAdlP28Watt242fCpuId,           85*4,      55*4,         0,         63*8,      39*8,        28,         30*8,         0,        230,        230,          320,           320,            0,           0},
  { EnumAdlP28Watt482fCpuId,           85*4,      55*4,         0,         63*8,      39*8,        28,         30*8,         0,        230,        230,          320,           320,            0,           0},
  { EnumAdlP28Watt682fCpuId,          109*4,      55*4,         0,         63*8,      39*8,        28,         30*8,         0,        230,        230,          320,           320,            0,           0},
  { EnumAdlP28Watt662fCpuId,          109*4,      55*4,         0,         63*8,      39*8,        28,         30*8,         0,        230,        230,          320,           320,            0,           0},
  { EnumAdlP28Watt642fCpuId,          109*4,      55*4,         0,         63*8,      39*8,        28,         30*8,         0,        230,        230,          320,           320,            0,           0},
  { EnumAdlP28Watt182fCpuId,           85*4,      55*4,         0,         63*8,      39*8,        28,         30*8,         0,        230,        230,          320,           320,            0,           0},
  { EnumAdlP28Watt442fCpuId,           85*4,      55*4,         0,         63*8,      39*8,        28,         30*8,         0,        230,        230,          320,           320,            0,           0},
  { EnumAdlP45Watt242fCpuId,          160*4,      55*4,         0,         70*8,      50*8,        56,         30*8,         0,        230,        230,          320,           320,            0,           0},
  { EnumAdlP45Watt682fCpuId,          160*4,      55*4,         0,         93*8,      56*8,        56,         30*8,         0,        230,        230,          320,           320,            0,           0},
  { EnumAdlP45Watt482fCpuId,          160*4,      55*4,         0,         70*8,      50*8,        56,         30*8,         0,        230,        230,          320,           320,            0,           0},
  { EnumAdlP45Watt442fCpuId,          160*4,      55*4,         0,         70*8,      50*8,        56,         30*8,         0,        230,        230,          320,           320,            0,           0},
  { EnumAdlP45Watt642fCpuId,          160*4,      55*4,         0,         70*8,      56*8,        56,         30*8,         0,        230,        230,          320,           320,            0,           0},
  { EnumAdlM7Watt182fPmicCpuId,        40*4,      21*4,      12*4,         22*8,         0,         1,         13*8,      12*8,        500,        500,          550,           550,         1050,        1050},
  { EnumAdlM5Watt182fPmicCpuId,        40*4,      21*4,      12*4,         22*8,         0,         1,         13*8,      12*8,        500,        500,          550,           550,         1050,        1050},
  { EnumAdlM5Watt182fPmicCpuId,        40*4,      21*4,      12*4,         22*8,         0,         1,         13*8,      12*8,        500,        500,          550,           550,         1050,        1050},
  { EnumAdlM5Watt142fPmicCpuId,        40*4,      21*4,      12*4,         22*8,         0,         1,         13*8,      12*8,        500,        500,          550,           550,         1050,        1050},
  { EnumAdlM7Watt142fPmicCpuId,        40*4,      21*4,      12*4,         22*8,         0,         1,         13*8,      12*8,        500,        500,          550,           550,         1050,        1050},
  { EnumAdlM7Watt242fHybDiscreteCpuId, 50*4,      25*4,      12*4,         29*8,      15*8,        28,         15*8,       8*8,        450,        450,          550,           550,          800,         800},
  { EnumAdlM7Watt282fHybDiscreteCpuId, 50*4,      25*4,      12*4,         29*8,      15*8,        28,         15*8,       8*8,        450,        450,          550,           550,          800,         800},
  { EnumAdlM7Watt182fHybDiscreteCpuId, 50*4,      25*4,      12*4,         29*8,      15*8,        28,         15*8,       8*8,        450,        450,          550,           550,          800,         800},
  { EnumAdlM7Watt242fHybDiscreteCpuId, 50*4,      25*4,      12*4,         29*8,      15*8,        28,         15*8,       8*8,        450,        450,          550,           550,          800,         800},
  { EnumAdlM9Watt142fHybDiscreteCpuId, 50*4,      25*4,      12*4,         29*8,      15*8,        28,         15*8,       8*8,        450,        450,          550,           550,          800,         800},
  { EnumAdlM9Watt242fHybDiscreteCpuId, 50*4,      25*4,      12*4,         29*8,      15*8,        28,         15*8,       8*8,        450,        450,          550,           550,          800,         800},
  { EnumAdlM9Watt282fHybDiscreteCpuId, 50*4,      25*4,      12*4,         29*8,      15*8,        28,         15*8,       8*8,        450,        450,          550,           550,          800,         800},
  { EnumAdlM12Watt242fHybDiscreteCpuId,50*4,      25*4,      12*4,         29*8,      15*8,        28,         15*8,       8*8,        450,        450,          550,           550,          800,         800},
  { EnumAdlM12Watt142fHybDiscreteCpuId,50*4,      25*4,      12*4,         29*8,      15*8,        28,         15*8,       8*8,        450,        450,          550,           550,          800,         800},
  { EnumAdlM12Watt182fHybDiscreteCpuId,50*4,      25*4,      12*4,         29*8,      15*8,        28,         15*8,       8*8,        450,        450,          550,           550,          800,         800},
  { EnumAdlM12Watt282fHybDiscreteCpuId,50*4,      25*4,      12*4,         29*8,      15*8,        28,         15*8,       8*8,        450,        450,          550,           550,          800,         800},

};

///
///

/**
  Check Vr Vendor Id and Prod Id for ADL-M

  @retval TRUE     Vendor Id and Prod Id is for ADL-M
  @retval FALSE    Vendor Id and Prod Id is not for ADL-M
**/
BOOLEAN
IsAdlMVendorIdProdId (
  VOID
  )
{
  UINT32           VendorId;
  UINT32           ProdId;

  GetVrVendorIdProdId (&VendorId, &ProdId);
  //
  // Check ADL-M Vendor Id and Prod Id
  //
  if ((VendorId == 0x14) && (ProdId == 0xF1)) {
    return TRUE;
  } else {
    return FALSE;
  }
}

/**
  Updates the Vr Config block with Intel default override values if needed

  @param[in]  CpuPowerMgmtVrConfig       CPU Power Management VR Config Block
  @param[in]  SelectedCtdpLevel          Ctdp Level Selected in BIOS
  @param[out] OverrideIccMax             Override IccMax setting
  @param[out] OverrideTdcCurrentLimit    Override TdcCurrentLimit setting
  @param[out] OverrideTimeWindow         Override TimeWindow setting
  @param[out] OverrideAcLoadline         Override AcLoadline setting
  @param[out] OverrideDcLoadline         Override DcLoadline setting
  @param[out] VccInAuxImonIccImax        Override VccInAuxImonIccImax setting
  @param[out] VccInAuxImonSlope          Override VccInAuxImonSlope setting
  @param[out] OverrideImonSlope          Override ImonSlope setting
**/
VOID
UpdateVrOverrides (
  IN  CPU_POWER_MGMT_VR_CONFIG *CpuPowerMgmtVrConfig,
  IN  UINT16                   SelectedCtdpLevel,
  OUT UINT16                   *OverrideIccMax,
  OUT UINT16                   *OverrideTdcCurrentLimit,
  OUT UINT32                   *OverrideTdcTimeWindow,
  OUT UINT16                   *OverrideAcLoadline,
  OUT UINT16                   *OverrideDcLoadline,
  OUT UINT16                   *VccInAuxImonIccImax,
  OUT UINT32                   *VccInAuxImonSlope,
  OUT UINT16                   *OverrideImonSlope
  )
{
  UINTN                       TableIndex;
  UINTN                       NoOfOverrides;
  CPU_VR_OVERRIDE_TABLE       *VrOverrideTable;
  CPU_IDENTIFIER              CpuIdentifier;

  if ((CpuPowerMgmtVrConfig == NULL) || (OverrideIccMax == NULL) || (OverrideTdcCurrentLimit == NULL) ||
      (OverrideTdcTimeWindow == NULL) || (OverrideAcLoadline == NULL) || (OverrideDcLoadline == NULL)|| (OverrideImonSlope == NULL))
  {
    ASSERT (FALSE);
    return;
  }

  VrOverrideTable = mCpuVrOverrideTable;
  NoOfOverrides = 0;

  ///
  /// Get CpuIdentifier to identify which set of VR values we need to override
  ///
  DEBUG ((DEBUG_INFO, "VR: SelectedCtdpLevel = %X\n", SelectedCtdpLevel));
  CpuIdentifier = GetCpuIdentifierWithCtdp (SelectedCtdpLevel);
  DEBUG ((DEBUG_INFO, "VR: Cpu Identifier = %X\n", CpuIdentifier));
  if (CpuIdentifier == EnumUnknownCpuId) {
    DEBUG ((DEBUG_ERROR, "VR: Unknown Cpu Identifier, bypassing VR overrides.\n"));
    return;
  }

  //
  // Update VccIn Aux Icc Max
  //
  if (CpuPowerMgmtVrConfig->VccInAuxImonIccImax == 0) {
    switch (GetCpuFamily ()) {
      case CPUID_FULL_FAMILY_MODEL_ALDERLAKE_MOBILE:
        if (IsSaVrSupportFru ()) {
          *VccInAuxImonIccImax = 48;     // 12*4 For ADL-M
        } else {
          *VccInAuxImonIccImax = 128;    // 32*4 For ADL-P
        }
        break;

      case CPUID_FULL_FAMILY_MODEL_ALDERLAKE_DT_HALO:
        *VccInAuxImonIccImax = 132;      // 33*4 For ADL-S
        break;

      default:
        *VccInAuxImonIccImax = 160;
        break;
    }
    DEBUG ((DEBUG_INFO, "VR: Override VccInAuxImonIccImax = %d \n", *VccInAuxImonIccImax));
  }

  if (IsAdlMVendorIdProdId ()) {
    if (CpuPowerMgmtVrConfig->VccInAuxImonSlope != 0) {
      *VccInAuxImonSlope = 105;
      DEBUG ((DEBUG_INFO, "VR: Override VccInAuxImonSlope = %d \n", *VccInAuxImonSlope));
    }
  }

  ///
  /// VrPowerDelivery Design
  ///
  if (CpuPowerMgmtVrConfig->VrPowerDeliveryDesign != 0) {
    ///
    /// This field is required to be non-zero.
    /// Used to communicate the power delivery design of the board.
    /// The VR override code traditionally looked at the CPU sku to determine the default VR
    /// override values. However, the platforms require different VR values based on the
    /// board power delivery design. This is due to CPU socket compatibility across different board
    /// designs. Without this field populated, the system may experience VR OCP shutdowns, hangs,
    /// thermal and performance loss.
    ///
    CpuIdentifier = CpuPowerMgmtVrConfig->VrPowerDeliveryDesign;
    DEBUG ((DEBUG_INFO, "CPU Identifier from VR Power Design - %X \n", CpuIdentifier));
  } else {
    DEBUG ((DEBUG_ERROR, "VR ERROR: VrPowerDeliveryDesign is missing! VR override values may cause system instability!\n"));
  }

  ///
  /// Update the VR config block with the VR override table data.
  ///
  NoOfOverrides = (sizeof (mCpuVrOverrideTable)) / (sizeof (CPU_VR_OVERRIDE_TABLE));

  for (TableIndex = 0; TableIndex < NoOfOverrides; TableIndex++, VrOverrideTable++) {
    ///
    /// If Cpu Identifier matches, then update with overrides values
    ///
    if (VrOverrideTable->CpuIdentifier == CpuIdentifier) {
      ///
      /// ICC Max Overrides
      ///
      if ((CpuPowerMgmtVrConfig->IccMax[CPU_VR_DOMAIN_IA] == 0) && (VrOverrideTable->IaIccMax != 0)) {
        OverrideIccMax[CPU_VR_DOMAIN_IA] = VrOverrideTable->IaIccMax;
      }
      if ((CpuPowerMgmtVrConfig->IccMax[CPU_VR_DOMAIN_GT] == 0) && (VrOverrideTable->GtIccMax != 0)) {
        OverrideIccMax[CPU_VR_DOMAIN_GT] = VrOverrideTable->GtIccMax;
      }
      if (IsSaVrSupportFru ()) {
        if ((CpuPowerMgmtVrConfig->IccMax[CPU_VR_DOMAIN_SA] == 0) && (VrOverrideTable->SaIccMax != 0)) {
          OverrideIccMax[CPU_VR_DOMAIN_SA] = VrOverrideTable->SaIccMax;
        }
      }
      ///
      /// VR TDC Overrides
      ///
      if ((CpuPowerMgmtVrConfig->TdcCurrentLimit[CPU_VR_DOMAIN_IA] == 0) && (VrOverrideTable->IaTdclimit != 0)) {
        if (CpuPowerMgmtVrConfig->Irms[CPU_VR_DOMAIN_IA] == 1) {
          OverrideTdcCurrentLimit[CPU_VR_DOMAIN_IA] = VrOverrideTable->IaTdclimitWithIrms;
          OverrideTdcTimeWindow[CPU_VR_DOMAIN_IA] = VrOverrideTable->IaTdcTimelimitIrms * 1000;
        } else {
          OverrideTdcCurrentLimit[CPU_VR_DOMAIN_IA] = VrOverrideTable->IaTdclimit;
        }
      }
      if ((CpuPowerMgmtVrConfig->TdcCurrentLimit[CPU_VR_DOMAIN_GT] == 0) && (VrOverrideTable->GtTdclimit != 0)) {
        OverrideTdcCurrentLimit[CPU_VR_DOMAIN_GT] = VrOverrideTable->GtTdclimit;
      }
      if (IsSaVrSupportFru ()) {
        if ((CpuPowerMgmtVrConfig->TdcCurrentLimit[CPU_VR_DOMAIN_SA] == 0) && (VrOverrideTable->SaTdclimit != 0)) {
          OverrideTdcCurrentLimit[CPU_VR_DOMAIN_SA] = VrOverrideTable->SaTdclimit;
        }
      }
      ///
      /// AC/DC Loadlines
      ///
      if ((CpuPowerMgmtVrConfig->AcLoadline[CPU_VR_DOMAIN_IA] == 0) && (VrOverrideTable->IaAcLoadLine != 0)) {
        OverrideAcLoadline[CPU_VR_DOMAIN_IA] = VrOverrideTable->IaAcLoadLine;
      }
      if ((CpuPowerMgmtVrConfig->DcLoadline[CPU_VR_DOMAIN_IA] == 0) && (VrOverrideTable->IaDcLoadLine != 0)) {
        OverrideDcLoadline[CPU_VR_DOMAIN_IA] = VrOverrideTable->IaDcLoadLine;
      }
      if ((CpuPowerMgmtVrConfig->AcLoadline[CPU_VR_DOMAIN_GT] == 0)  && (VrOverrideTable->GtAcLoadLine != 0)) {
        OverrideAcLoadline[CPU_VR_DOMAIN_GT] = VrOverrideTable->GtAcLoadLine;
      }
      if ((CpuPowerMgmtVrConfig->DcLoadline[CPU_VR_DOMAIN_GT] == 0) && (VrOverrideTable->GtDcLoadLine != 0)) {
        OverrideDcLoadline[CPU_VR_DOMAIN_GT] = VrOverrideTable->GtDcLoadLine;
      }
      if (IsSaVrSupportFru ()) {
        if ((CpuPowerMgmtVrConfig->AcLoadline[CPU_VR_DOMAIN_SA] == 0)  && (VrOverrideTable->GtAcLoadLine != 0)) {
          OverrideAcLoadline[CPU_VR_DOMAIN_SA] = VrOverrideTable->SaAcLoadLine;
        }
        if ((CpuPowerMgmtVrConfig->DcLoadline[CPU_VR_DOMAIN_SA] == 0) && (VrOverrideTable->SaDcLoadLine != 0)) {
          OverrideDcLoadline[CPU_VR_DOMAIN_SA] = VrOverrideTable->SaDcLoadLine;
        }
      }

      if (IsAdlMVendorIdProdId ()) {
        if (CpuPowerMgmtVrConfig->ImonSlope[CPU_VR_DOMAIN_GT] != 0) {
          OverrideImonSlope[CPU_VR_DOMAIN_GT] = 167;
        }
        if (CpuPowerMgmtVrConfig->ImonSlope[CPU_VR_DOMAIN_SA] != 0) {
          OverrideImonSlope[CPU_VR_DOMAIN_SA] = 93;
        }
      }
      ///
      /// Exit loop when the correct CPU overrides have been applied.
      ///
      return;
    }
  }
  ///
  /// No matching CpuIdentifier found in the table
  ///
  DEBUG ((DEBUG_INFO, "No matching CpuIdentifier found in the VrOverride Table.\n"));
}

/**
  Load CPU power management Vr Config block default for specific generation.

  @param[in, out] CpuPowerMgmtVrConfig   Pointer to CPU_POWER_MGMT_VR_CONFIG instance
**/
VOID
EFIAPI
PeiCpuLoadPowerMgmtVrConfigDefaultFru (
  IN OUT CPU_POWER_MGMT_VR_CONFIG    *CpuPowerMgmtVrConfig
  )
{
  CpuPowerMgmtVrConfig->FivrSpectrumEnable  = 1;  // Enable Spread Spectrum
  CpuPowerMgmtVrConfig->FivrSpreadSpectrum  = 8;  // 0.5 Spread
  CpuPowerMgmtVrConfig->VccInAuxImonSlope   = 1;
  CpuPowerMgmtVrConfig->VsysFullScale              = 0x18;
  CpuPowerMgmtVrConfig->VsysCriticalThreshold      = 0x6;
  CpuPowerMgmtVrConfig->VsysAssertionDeglitchMantissa     = 0x1;
  CpuPowerMgmtVrConfig->VsysDeassertionDeglitchMantissa   = 0xD;
  CpuPowerMgmtVrConfig->VsysDeassertionDeglitchExponent   = 0x2;
  CpuPowerMgmtVrConfig->TdcTimeWindow1[CPU_VR_DOMAIN_IA] = 1000; // 1 Sec
  CpuPowerMgmtVrConfig->TdcTimeWindow1[CPU_VR_DOMAIN_GT] = 1; // 1 mSec
  CpuPowerMgmtVrConfig->TdcTimeWindow1[CPU_VR_DOMAIN_SA] = 1; // 1 mSec
}

/**
  Update the VR override workarounds
  @param[in] ImonSlope  Slope Value from Policy
**/
VOID
VrOverrideWorkaroundFru (
  IN UINT16   ImonSlope
  )
{
  EFI_STATUS                  Status;
  UINT32                      MailboxData;
  UINT32                      MailboxStatus;
  PCODE_MAILBOX_INTERFACE     MailboxCommand;

  ///
  /// Allows PS4 to be disabled in the PSYS domain. This subcommand cannot be performed after BIOS completion is asserted.
  /// In case a PS4 command is issued, it will be performed on PSYS VR directly.
  /// This allows putting it in PS4 so it will not consume power in the case we decide to disable it.
  ///
  MailboxCommand.InterfaceData = 0;
  MailboxCommand.Fields.Command = MAILBOX_VR_CMD_SVID_VR_HANDLER;
  MailboxCommand.Fields.Param1 = MAILBOX_VR_SUBCMD_SVID_SET_PSYS_PS4_DISABLE;
  MailboxData = BIT0; // Disable PSYS PS4
  DEBUG ((DEBUG_INFO, "(MAILBOX) Mailbox Write Command = SET_PSYS_PS4_DISABLE\n"));
  DEBUG ((DEBUG_INFO, "(MAILBOX) SET_PSYS_PS4_DISABLE = %x\n", MailboxData));
  Status = MailboxWrite (MAILBOX_TYPE_VR_MSR, MailboxCommand.InterfaceData, MailboxData, &MailboxStatus);
  if (MailboxStatus != PCODE_MAILBOX_CC_SUCCESS) {
    DEBUG ((DEBUG_ERROR, "VR: Error SET_PSYS_PS4_DISABLE EFI_STATUS = %r, Mailbox Status = %X\n", Status, MailboxStatus));
  }
}

/**
  Override the VR Voltage Limits (VMAX)

  @param[in] VrVoltageLimit  Vmax Value to be updated
**/
VOID
OverrideVrVoltageLimitWa (
  IN UINT16   VoltageLimit
  )
{
  CPUID_VERSION_INFO_EAX      Eax;
  EFI_STATUS                  Status;
  UINT32                      MailboxData;
  UINT32                      MailboxStatus;
  UINT8                       GfxRevisionId;
  UINTN                       MchBar;
  PCODE_MAILBOX_INTERFACE     MailboxCommand;
  UINT8                       Index;
  UINT16                      CpuDid;
  ///
  /// Read the CPUID information
  ///
  AsmCpuid (CPUID_VERSION_INFO, &Eax.Uint32, NULL, NULL, NULL);
  ///
  /// Get the Graphics Revision Id
  ///
  MchBar = PciSegmentRead32 (PCI_SEGMENT_LIB_ADDRESS (SA_SEG_NUM, SA_MC_BUS, SA_MC_DEV, SA_MC_FUN, R_SA_MCHBAR)) & ~BIT0;
  GfxRevisionId = (UINT8) MmioRead32 (MchBar + MCHBAR_MMIO_SCRATCHPAD0);
  DEBUG ((DEBUG_INFO, "GfxRevisionId - %x\n", GfxRevisionId));
  CpuDid = PciSegmentRead16 (PCI_SEGMENT_LIB_ADDRESS (SA_SEG_NUM, SA_MC_BUS, SA_MC_DEV, SA_MC_FUN, R_SA_MC_DEVICE_ID));
  if (((Eax.Uint32 & CPUID_FULL_FAMILY_MODEL) == CPUID_FULL_FAMILY_MODEL_ALDERLAKE_MOBILE) && (GetCpuStepping () == EnumAdlQ0) && ((CpuDid == V_SA_DEVICE_ID_MB_ULX_1) || (CpuDid == V_SA_DEVICE_ID_MB_ULX_2) || (CpuDid == V_SA_DEVICE_ID_MB_ULX_3) || (CpuDid == V_SA_DEVICE_ID_MB_ULX_4))) {
    for (Index = CPU_VR_DOMAIN_IA; Index <= CPU_VR_DOMAIN_SA; Index ++) {
      if (IsAdlMVendorIdProdId ()) {
        ///
        /// PS Cutoff Current
        ///
        MailboxData =  (UINT32)(PSI1_THRESHOLD & PSI_THRESHOLD_MASK) |
                       ((PSI2_THRESHOLD & PSI_THRESHOLD_MASK) << PSI2_THRESHOLD_OFFSET) |
                       ((PSI3_THRESHOLD & PSI_THRESHOLD_MASK) << PSI3_THRESHOLD_OFFSET) |
                       ((~PSI3_DISABLE & BIT0) << PSI3_ENABLE_OFFSET) |
                       ((~PSI4_DISABLE & BIT0) << PSI4_ENABLE_OFFSET);
        MailboxCommand.InterfaceData = 0;
        MailboxCommand.Fields.Command = MAILBOX_VR_CMD_SVID_VR_HANDLER;
        MailboxCommand.Fields.Param1 = MAILBOX_VR_SUBCMD_SVID_SET_PS_CUTOFF;
        MailboxCommand.Fields.Param2 = (Index & VR_ADDRESS_MASK);
        DEBUG ((DEBUG_INFO, "(MAILBOX) Mailbox Write Command = WRITE_PSI_CUTOFF_CMD\n"));
        DEBUG ((DEBUG_INFO, "(MAILBOX) Psi1Threshold         = %d (1/4 Amp)\n", PSI1_THRESHOLD));
        DEBUG ((DEBUG_INFO, "(MAILBOX) Psi2Threshold         = %d (1/4 Amp)\n", PSI2_THRESHOLD));
        DEBUG ((DEBUG_INFO, "(MAILBOX) Psi3Threshold         = %d (1/4 Amp)\n", PSI3_THRESHOLD));
        DEBUG ((DEBUG_INFO, "(MAILBOX) Psi3Enable            = %d, Psi4Enable            = %d\n", PSI3_DISABLE, PSI4_DISABLE));
        Status = MailboxWrite (MAILBOX_TYPE_VR_MSR, MailboxCommand.InterfaceData, MailboxData, &MailboxStatus);
        if (MailboxStatus != PCODE_MAILBOX_CC_SUCCESS) {
          DEBUG ((DEBUG_ERROR, "VR: Error Writing PS Cutoff Current. EFI_STATUS = %r, Mailbox Status = %X\n", Status, MailboxStatus));
        }
     }
   }
  }
}

/**
  Programs the FIVR RFI settings based on XTAL clock frequency.

  @param[IN] CpuPowerMgmtVrConfig  - CPU Power Management VR Config Block
**/
VOID
UpdateFivrRfiSettings (
  IN CPU_POWER_MGMT_VR_CONFIG *CpuPowerMgmtVrConfig
  )
{
  UINTN                       MchBar;
  UINT32                      RfiData;
  UINT32                      RegEcx;
  UINT16                      RfiValue;
  UINT16                      DoubledValue;
  UINT8                       RfiLowBits;
  UINT8                       RfiHighByte;
  UINT8                       LowOffset;
  UINT8                       HighOffset;
  UINT32                      RfiMask;
  UINT32                      SpreadSpectrumData;
  CPUID_VERSION_INFO_EAX      Eax;

  ///
  /// Read the CPUID information
  ///
  AsmCpuid (CPUID_VERSION_INFO, &Eax.Uint32, NULL, NULL, NULL);

  MchBar = PciSegmentRead32 (PCI_SEGMENT_LIB_ADDRESS (SA_SEG_NUM, SA_MC_BUS, SA_MC_DEV, SA_MC_FUN, R_SA_MCHBAR)) & ~BIT0;
  SpreadSpectrumData = 0;
  if (CpuPowerMgmtVrConfig->FivrSpectrumEnable != 0) {
    DEBUG ((DEBUG_INFO, "EncodedSpectrum - %x\n", CpuPowerMgmtVrConfig->FivrSpreadSpectrum));
    //
    // Enable and write the Spread Spectrum value.
    //
    SpreadSpectrumData =  MmioRead32 (MchBar + R_EMI_CONTROL_0_0_0_MCHBAR_PCU);
    SpreadSpectrumData &= ~B_SPREAD_SPECTRUM_MASK;
    SpreadSpectrumData |= (CpuPowerMgmtVrConfig->FivrSpreadSpectrum | B_SPREAD_SPECTRUM_ENABLE);
  }

  MmioWrite32 ((MchBar + R_EMI_CONTROL_0_0_0_MCHBAR_PCU), SpreadSpectrumData);
  ///
  /// PCH FIVR is supported in Mobile only
  ///
  if (((Eax.Uint32 & CPUID_FULL_FAMILY_MODEL) != CPUID_FULL_FAMILY_MODEL_ALDERLAKE_DT_HALO)
    ) {
    //
    // Update PCH FIVR RFI setting based on RFIControl0/EMIControl0
    // settings on CPU side
    //
    PmcConfigureFivrEmiControl0 (SpreadSpectrumData);
  }

  if (CpuPowerMgmtVrConfig->FivrRfiFrequency != 0) {
    ///
    /// Determine XTAL clock frequency
    ///
    AsmCpuid (CPUID_TIME_STAMP_COUNTER, NULL, NULL, &RegEcx, NULL);

    if (RegEcx == CLOCK_FREQUENCY_38MHz) {
      RfiData = MmioRead32 (MchBar + R_RFI_CONTROL2_0_0_0_MCHBAR_PCU);
      DEBUG ((DEBUG_INFO, "Reading RFI Frequency, R_RFI_CONTROL2_0_0_0_MCHBAR_PCU = %X\n", RfiData));
      DEBUG ((DEBUG_INFO, "CpuPowerMgmtVrConfig->FivrRfiFrequency = %X\n", CpuPowerMgmtVrConfig->FivrRfiFrequency));
      //
      // RfiValue = Config->RfiFreq * 12800 / 38400.
      // RfiValue = Config->RfiFreq * 1 / 3.
      // Round to nearest by doubling before dividing by 3, then add 1 to the result and divide by 2.
      //
      DoubledValue = (CpuPowerMgmtVrConfig->FivrRfiFrequency * 2) / 3;
      RfiValue = (DoubledValue + 1) / 2;
      //
      // Set offsets and mask based on clock.
      //
      LowOffset = N_RFI_FREQ_LO_PF3_OFFSET;
      HighOffset = N_RFI_FREQ_HI_PF3_OFFSET;
      RfiMask = B_RFI_FREQ_PF3_MASK;
    } else {
      RfiData = MmioRead32 (MchBar + R_RFI_CONTROL_0_0_0_MCHBAR_PCU);
      ///
      /// RfiValue = Policy->RfiFreq * 100KHz * (128/ClockInKhz).
      ///
      if (RegEcx == CLOCK_FREQUENCY_19MHz) {
        //
        // RfiValue = Config->RfiFreq * 12800 / 19200.
        // RfiValue = Config->RfiFreq * 2 / 3.
        // Round to nearest by doubling before dividing by 3, then add 1 to the result and divide by 2.
        //
        DoubledValue = (CpuPowerMgmtVrConfig->FivrRfiFrequency * 4) / 3;
        RfiValue = (DoubledValue + 1) / 2;
        //
        // Set offsets and mask based on clock.
        //
        LowOffset = N_RFI_FREQ_LO_PF1_OFFSET;
        HighOffset = N_RFI_FREQ_HI_PF1_OFFSET;
        RfiMask = B_RFI_FREQ_PF1_MASK;
      } else if (RegEcx == CLOCK_FREQUENCY_24MHz) {
        //
        // RfiValue = Config->RfiFreq * 12800 / 24000.
        // RfiValue = Config->RfiFreq * 8 / 15.
        // Round to nearest by doubling before dividing by 15, then add 1 to the result and divide by 2.
        //
        DoubledValue = (CpuPowerMgmtVrConfig->FivrRfiFrequency * 16) / 15;
        RfiValue = (DoubledValue + 1) / 2;
        //
        // Set offsets and mask based on clock.
        //
        LowOffset = N_RFI_FREQ_LO_PF2_OFFSET;
        HighOffset = N_RFI_FREQ_HI_PF2_OFFSET;
        RfiMask = B_RFI_FREQ_PF2_MASK;
      } else {
        DEBUG ((DEBUG_ERROR, "Unexpected Clock Frequency! RegEcx: %d\n", RegEcx));
        return;
      }
    }
    if (RfiValue > V_MAX_RFI_VALUE) {
      RfiValue = V_MAX_RFI_VALUE;
    }

    if (RegEcx == CLOCK_FREQUENCY_38MHz) {
      RfiData = (RfiData & ~RfiMask);
      RfiLowBits = (RfiValue & (BIT2 | BIT1 | BIT0));
      RfiHighByte = (UINT8) (RfiValue >> 3);
      RfiData = RfiData | (UINT32) (RfiLowBits << LowOffset);
      RfiData = RfiData | (UINT32) (RfiHighByte << HighOffset);
      DEBUG ((DEBUG_INFO, "Writing RFI Frequency, R_RFI_CONTROL2_0_0_0_MCHBAR_PCU = %X\n", RfiData));
      MmioWrite32 ((MchBar + R_RFI_CONTROL2_0_0_0_MCHBAR_PCU), RfiData);
    } else {
      RfiData = (RfiData & ~RfiMask);
      RfiLowBits = (RfiValue & (BIT1 | BIT0));
      RfiHighByte = (UINT8) (RfiValue >> 2);
      RfiData = RfiData | (UINT32) (RfiLowBits << LowOffset);
      RfiData = RfiData | (UINT32) (RfiHighByte << HighOffset);
      DEBUG ((DEBUG_INFO, "Writing RFI Frequency, R_RFI_CONTROL_0_0_0_MCHBAR_PCU = %X\n", RfiData));
      MmioWrite32 ((MchBar + R_RFI_CONTROL_0_0_0_MCHBAR_PCU), RfiData);
    }
    if (((Eax.Uint32 & CPUID_FULL_FAMILY_MODEL) != CPUID_FULL_FAMILY_MODEL_ALDERLAKE_DT_HALO)
      ) {
      //
      // Update PCH FIVR RFI setting based on RFIControl0/EMIControl0
      //
      PmcConfigureFivrRfiControl4 (RfiData);
    }
  }
}


/**
  Get Vr topologies which contain generation specific Vr address and bit which represents Svid Enabled/Disabled. Transferred as parameter from Fru to IP block.

  @param[IN,OUT] - VrDomainTopology - It takes VR_DOMAIN_TOPOLOGY as parameter and returns same which contains VR address and Svid Enable/Disabled.
**/
VOID
GetCpuVrTopology (
  IN OUT VR_DOMAIN_TOPOLOGY *VrDomainTopology
  )
{
  EFI_STATUS                  Status;
  UINT32                      MailboxStatus;
  UINT32                      MailboxType;
  VR_TOPOLOGY_DATA            VrTopology;
  UINTN                       VrIndex;
  PCODE_MAILBOX_INTERFACE     MailboxCommand;

  ///
  /// CPU VR MSR mailbox
  ///
  MailboxType = MAILBOX_TYPE_VR_MSR;
  VrTopology.VrTopology = 0;

  ///
  /// Get CPU VR topology
  ///
  DEBUG ((DEBUG_INFO, "(MAILBOX) Mailbox Read Command = READ_VR_STRAP_CONFIG_CMD\n"));
  MailboxCommand.InterfaceData = 0;
  MailboxCommand.Fields.Command = MAILBOX_VR_CMD_SVID_VR_HANDLER;
  MailboxCommand.Fields.Param1 = MAILBOX_VR_SUBCMD_SVID_GET_STRAP_CONFIGURATION;
  Status = MailboxRead (MailboxType, MailboxCommand.InterfaceData, (UINT32*)&VrTopology.VrTopology, &MailboxStatus);
  if (MailboxStatus != PCODE_MAILBOX_CC_SUCCESS) {
    DEBUG ((DEBUG_ERROR, "VR: Error Reading VR topology. EFI_STATUS = %r, Mailbox Status = %X\n", Status, MailboxStatus));
  }
  VrDomainTopology->MinVrIndex = CPU_VR_DOMAIN_IA;
  VrDomainTopology->MaxVrIndex = CPU_VR_DOMAIN_GT + 1;
  ///
  /// Print VR Topology data
  ///
  DEBUG ((DEBUG_INFO, "VR: VrTopology data = 0x%x\n", VrTopology.VrTopology));
  DEBUG ((DEBUG_INFO, "    VR Type 0 = SVID, VR Type 1 = non-SVID\n"));
  DEBUG ((DEBUG_INFO, "    IA VR Address    = 0x%x\n", VrTopology.Fields.VrIaAddress));
  DEBUG ((DEBUG_INFO, "    IA VR Type       = 0x%x\n", VrTopology.Fields.VrIaSvidType));
  DEBUG ((DEBUG_INFO, "    GT VR Address    = 0x%x\n", VrTopology.Fields.VrGtAddress));
  DEBUG ((DEBUG_INFO, "    GT VR Type       = 0x%x\n", VrTopology.Fields.VrGtSvidType));
  if (IsSaVrSupportFru ()) {
    DEBUG ((DEBUG_INFO, "    SA VR Address    = 0x%x\n", VrTopology.Fields.VrSaAddress));
    DEBUG ((DEBUG_INFO, "    SA VR Type       = 0x%x\n", VrTopology.Fields.VrSaSvidType));
    VrDomainTopology->MaxVrIndex = CPU_VR_DOMAIN_SA + 1;
  }
  for (VrIndex = CPU_VR_DOMAIN_IA; VrIndex < VrDomainTopology->MaxVrIndex; VrIndex++) {
    switch (VrIndex) {
      case CPU_VR_DOMAIN_IA:
        VrDomainTopology->VrAddress[VrIndex] = (UINT8) VrTopology.Fields.VrIaAddress;
        VrDomainTopology->SvidEnabled[VrIndex] = (UINT8) (((~VrTopology.Fields.VrIaSvidType) & BIT0));
        break;

      case CPU_VR_DOMAIN_GT:
        VrDomainTopology->VrAddress[VrIndex] = (UINT8) VrTopology.Fields.VrGtAddress;
        VrDomainTopology->SvidEnabled[VrIndex] = (UINT8) (((~VrTopology.Fields.VrGtSvidType) & BIT0));
        break;

      case CPU_VR_DOMAIN_SA:
        VrDomainTopology->VrAddress[VrIndex] = (UINT8)VrTopology.Fields.VrSaAddress;
        VrDomainTopology->SvidEnabled[VrIndex] = (UINT8)(((~VrTopology.Fields.VrSaSvidType) & BIT0));
        break;

      default:
        VrDomainTopology->VrAddress[VrIndex] = 0;
        VrDomainTopology->SvidEnabled[VrIndex] = 0;
        break;
    }
  }
}

/**
  Returns the maximum number of voltage regulator domains.

  @retval Maximum Number of VR Domains
**/
UINT16
GetMaxNumVrsFru (
  VOID
  )
{
  if (IsSaVrSupportFru ()) {
    return 3;
  } else {
    return 2;
  }
}

/**
  This function uses mailbox communication to control settled time of Richtek VR which is enabled for
  VccSA to meet IMVP8 VR settled time.

  @param[IN] - VrIndex - It represent VR domains like SA,IA,RING,GT.
**/
VOID
VrSettledTimerRt (
  IN UINTN VrIndex
  )
{
  // This is needed only for Richtek VCCSA.
  return;
}

/**
  Return TRUE when the VCC In Demotion is supported in the specific VR.

  @param[in]  VrIndex   Index of VR.

  @retval TRUE     VCC IN Demotion is supported.
  @retval FALSE    VCC IN Demotion is not supported.
 */
BOOLEAN
IsVccInDemotionSupported (
  IN  UINTN  VrIndex
  )
{
  return FALSE;
}

/**
  Return TRUE when the Fast Vmode IccLimit is supported in the specific VR.

  @retval TRUE     IccLimit is supported.
  @retval FALSE    IccLimit is not supported.
 */
BOOLEAN
IsFastVmodeIccLimitSupport (
  VOID
  )
{
  EFI_STATUS                  Status;
  UINT32                      MailboxStatus;
  UINT32                      MailboxType;
  VR_TOPOLOGY_DATA            VrTopology;
  PCODE_MAILBOX_INTERFACE     MailboxCommand;

  ///
  /// CPU VR MSR mailbox
  ///
  MailboxType = MAILBOX_TYPE_VR_MSR;
  VrTopology.VrTopology = 0;

  MailboxCommand.InterfaceData  = 0;
  MailboxCommand.Fields.Command = MAILBOX_VR_CMD_SVID_VR_HANDLER;
  MailboxCommand.Fields.Param1  = MAILBOX_VR_SUBCMD_SVID_GET_STRAP_CONFIGURATION;

  ///
  /// Get CPU VR topology
  /// BIOS is expected to enable this feature only for a RPL unit on a RPL board.
  /// BIOS can discover the platform support via bit 19 in DW3 (platform_is_rpl) which
  /// is exposed via MAILBOX_VR_SUBCMD_SVID_GET_STRAP_CONFIGURATION .
  /// BIOS must not set any ILIMIT value >0 if this bit is 0.
  ///
  DEBUG ((EFI_D_INFO, "(MAILBOX) Mailbox Read Command = 0x%08x\n", MailboxCommand.InterfaceData));
  Status = MailboxRead (MailboxType, MailboxCommand.InterfaceData, (UINT32*)&VrTopology.VrTopology, &MailboxStatus);
  if (MailboxStatus != PCODE_MAILBOX_CC_SUCCESS) {
    DEBUG ((DEBUG_ERROR, "VR: Error Reading VR topology. EFI_STATUS = %r, Mailbox Status = %X\n", Status, MailboxStatus));
    return FALSE;
  }

  if (VrTopology.Fields.PlatformType == 1) {
    //
    // Platform is RPL.
    //
    return TRUE;
  }

  return FALSE;
}

/**
  Get VR VendorId and ProdId

  @param[in,out] VendorId  Get VendorId from Pcode MailBox
  @param[in,out] ProdId    Get ProdId from Pcode MailBox
**/
VOID
GetVrVendorIdProdId (
  IN OUT UINT32 *VendorId,
  IN OUT UINT32 *ProdId
  )
{
  EFI_STATUS                  Status;
  PCODE_MAILBOX_INTERFACE     MailboxCommand;
  MAILBOX_VR_INTERFACE_DATA   VrData;
  UINT32                      MailboxData;
  UINT32                      MailboxStatus;
  //
  // Get the VendorID from VR Interface
  //
  VrData.VrInterfaceData = 0;
  VrData.Fields.VrId = 0;
  VrData.Fields.VrCommand = MAILBOX_VR_CMD_SVID_COMMAND_GET_REG; // Get Reg
  VrData.Fields.VrRegAddr = 0x0; // Get VendorID
  MailboxData = VrData.VrInterfaceData;
  MailboxCommand.InterfaceData = 0;
  MailboxCommand.Fields.Command = MAILBOX_VR_CMD_VR_INTERFACE;
  MailboxCommand.Fields.Param1 = 0;
  MailboxCommand.Fields.Param2 = 0;
  Status = MailboxRead (MAILBOX_TYPE_VR_MSR, MailboxCommand.InterfaceData, &MailboxData, &MailboxStatus);
  if (MailboxStatus != PCODE_MAILBOX_CC_SUCCESS) {
    DEBUG ((DEBUG_ERROR, "VR: Error READ_VR_INTERFACE_VENDOR_ID_CMD EFI_STATUS = %r, Mailbox Status = %X\n", Status, MailboxStatus));
  }
  *VendorId = MailboxData;
  DEBUG ((DEBUG_INFO, "VR: Vendor ID = %x\n", *VendorId));
  //
  // Get the ProdId from VR Interface
  //
  VrData.VrInterfaceData = 0;
  VrData.Fields.VrId = 0;
  VrData.Fields.VrCommand = MAILBOX_VR_CMD_SVID_COMMAND_GET_REG; // Get Reg
  VrData.Fields.VrRegAddr = 0x1; // Get ProdID
  MailboxData = VrData.VrInterfaceData;
  MailboxCommand.InterfaceData = 0;
  MailboxCommand.Fields.Command = MAILBOX_VR_CMD_VR_INTERFACE;
  MailboxCommand.Fields.Param1 = 0;
  MailboxCommand.Fields.Param2 = 0;
  Status = MailboxRead (MAILBOX_TYPE_VR_MSR, MailboxCommand.InterfaceData, &MailboxData, &MailboxStatus);
  if (MailboxStatus != PCODE_MAILBOX_CC_SUCCESS) {
    DEBUG ((DEBUG_ERROR, "VR: Error READ_VR_INTERFACE_PROD_ID_CMD EFI_STATUS = %r, Mailbox Status = %X\n", Status, MailboxStatus));
  }
  *ProdId = MailboxData;
  DEBUG ((DEBUG_INFO, "VR: Production ID = %x\n", *ProdId));
}

/**
  Set the Slow Slew Rate for IA Domain to Fast/2
**/
VOID
SetIaSlowSlewRate (
  VOID
  )
{
  EFI_STATUS                  Status;
  UINT32                      MailboxStatus;
  UINT32                      MailboxData;
  PCODE_MAILBOX_INTERFACE     MailboxCommand;

  MailboxCommand.InterfaceData = 0;
  MailboxCommand.Fields.Command = MAILBOX_VR_CMD_SVID_VR_HANDLER;
  MailboxCommand.Fields.Param1 = MAILBOX_VR_SUBCMD_SVID_SET_VR_SLEW_RATE;
  MailboxCommand.Fields.Param2 = 0;
  MailboxData = SLOW_SLEW_RATE_FAST_2;
  DEBUG ((DEBUG_INFO, "(MAILBOX) Mailbox Write Command = WRITE_SVID_SET_VR_SLEW_RATE_CMD\n"));
  DEBUG ((DEBUG_INFO, "(MAILBOX) Setting IA SlowSlewRate to Fast/2\n"));
  Status = MailboxWrite (MAILBOX_TYPE_VR_MSR, MailboxCommand.InterfaceData, MailboxData, &MailboxStatus);
  if (MailboxStatus != PCODE_MAILBOX_CC_SUCCESS) {
    DEBUG ((DEBUG_ERROR, "VR: Error Writing Slow Slew Rate for IA Vr. EFI_STATUS = %r, Mailbox Status = %X\n", Status, MailboxStatus));
  }
}

