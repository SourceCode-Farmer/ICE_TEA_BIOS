/** @file
  This file contains MSR access for specific generation.

@copyright
  INTEL CONFIDENTIAL
  Copyright 2019 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains an 'Intel Peripheral Driver' and is uniquely identified as
  "Intel Reference Module" and is licensed for Intel CPUs and chipsets under
  the terms of your license agreement with Intel or your vendor. This file may
  be modified by the user, subject to additional terms of the license agreement.

@par Specification
**/

#include <Library/DebugLib.h>
#include <Library/BaseLib.h>
#include <Library/MsrFruLib.h>
#include <Library/CpuPlatformLib.h>
#include <Register/CommonMsr.h>
#include <Register/AdlMsr.h>
#include <CpuRegs.h>
#include <Register/ArchMsr.h>

/**
  This function get MSR_FIT_BIOS_ERROR.

  @param[out]  FitEntryType
  @param[out]  FitErrorCode
**/
VOID
MsrGetFitBiosError (
  OUT UINT8 *FitEntryType,
  OUT UINT8 *FitErrorCode
  )
{
  ADL_MSR_FIT_BIOS_ERROR_REGISTER FitData;
  FitData.Uint64 = AsmReadMsr64 (ADL_MSR_FIT_BIOS_ERROR);
  *FitEntryType = (UINT8) FitData.Bits.EntryType;
  *FitErrorCode = (UINT8) FitData.Bits.ErrorCode;
}

/**
  This function returns if programable TJ offset enable

  @retval TRUE if programable TJ offset is supported
          FALSE if programable TJ offset is not supported
**/
BOOLEAN
MsrIsPrgTjOffsetEn (
  VOID
  )
{
  ADL_MSR_PLATFORM_INFO_REGISTER PlatformInfoMsr;
  PlatformInfoMsr.Uint64 = AsmReadMsr64 (ADL_MSR_PLATFORM_INFO);
  return PlatformInfoMsr.Bits.PrgTjOffsetEn != 0;
}

/**
  This function return max and min bus ratio.

  @param[out]  MaxBusRatio  Maximum Non-Turbo Ratio
  @param[out]  MinBusRatio  Max efficiency ratio
**/
VOID
MsrGetBusRatio (
  OUT UINT8 *MaxBusRatio, OPTIONAL
  OUT UINT8 *MinBusRatio  OPTIONAL
  )
{
  ADL_MSR_PLATFORM_INFO_REGISTER PlatformInfoMsr;
  if (MaxBusRatio != NULL || MinBusRatio != NULL) {
    PlatformInfoMsr.Uint64 = AsmReadMsr64 (ADL_MSR_PLATFORM_INFO);
    if (MaxBusRatio != NULL) {
      *MaxBusRatio = (UINT8) PlatformInfoMsr.Bits.MaxNonTurboLimRatio;
    }
    if (MinBusRatio != NULL) {
      *MinBusRatio = (UINT8) PlatformInfoMsr.Bits.MaxEfficiencyRatio;
    }
  }
}

/**
  This function return max Efficiency Ratio.

  @retval Max efficiency ratio
**/
UINT8
MsrGetMaxEfficiencyRatio (
  VOID
  )
{
  ADL_MSR_PLATFORM_INFO_REGISTER PlatformInfoMsr;
  PlatformInfoMsr.Uint64 = AsmReadMsr64 (ADL_MSR_PLATFORM_INFO);
  return (UINT8) PlatformInfoMsr.Bits.MaxEfficiencyRatio;
}

/**
  This function return max Non-Turbo Ratio.

  @retval Max Non-Turbo Ratio
**/
UINT8
MsrGetMaxNonTurboRatio (
  VOID
  )
{
  ADL_MSR_PLATFORM_INFO_REGISTER PlatformInfoMsr;
  PlatformInfoMsr.Uint64 = AsmReadMsr64 (ADL_MSR_PLATFORM_INFO);
  return (UINT8) PlatformInfoMsr.Bits.MaxNonTurboLimRatio;
}

/**
  This function return intended frequency

  @param[in]  ConfigTdpLevel  Config Tdp Level

  @retval intended frequency
**/
UINT8
MsrGetSelectCtdpRatio (
  IN UINT8 ConfigTdpLevel
  )
{
  MSR_CONFIG_TDP_NOMINAL_REGISTER       CtdpNominal;
  MSR_CONFIG_TDP_LEVEL1_REGISTER        CtdpDownMsr;
  MSR_CONFIG_TDP_LEVEL2_REGISTER        CtdpUpMsr;

  if (ConfigTdpLevel == 0) {
    CtdpNominal.Uint64 = AsmReadMsr64 (MSR_CONFIG_TDP_NOMINAL);
    return (UINT8) CtdpNominal.Bits.TdpRatio;
  }

  if (ConfigTdpLevel == 1) {
    CtdpDownMsr.Uint64 = AsmReadMsr64 (MSR_CONFIG_TDP_LEVEL1);
    return (UINT8) CtdpDownMsr.Bits.TdpRatio;
  }
  if (ConfigTdpLevel == 2) {
    CtdpUpMsr.Uint64 = AsmReadMsr64 (MSR_CONFIG_TDP_LEVEL2);
    return (UINT8) CtdpUpMsr.Bits.TdpRatio;
  }
  return 0;
}

/**
  This function return the supported Config TDP Levels.

  @retval number of config TDP levels
**/
UINT8
MsrGetConfigTdpLevels (
  VOID
  )
{
  ADL_MSR_PLATFORM_INFO_REGISTER PlatformInfoMsr;
  PlatformInfoMsr.Uint64 = AsmReadMsr64 (ADL_MSR_PLATFORM_INFO);
  return (UINT8) PlatformInfoMsr.Bits.ConfigTdpLevels;
}

/**
  This function returns if PPIN feature present

  @retval TRUE if PPIN feature present
          FALSE if PPIN feature not present
**/
BOOLEAN
MsrIsPpinCap (
  VOID
  )
{
  ADL_MSR_PLATFORM_INFO_REGISTER PlatformInfoMsr;
  PlatformInfoMsr.Uint64 = AsmReadMsr64 (ADL_MSR_PLATFORM_INFO);
  return PlatformInfoMsr.Bits.PpinCap != 0;
}

/**
  This function returns if MCU 2nd patch load success

  @retval TRUE if MCU 2nd patch success
          FALSE if MCU 2nd patch not success
**/
BOOLEAN
MsrIsMcu2ndPatchSuccess (
  VOID
  )
{
  ADL_MSR_BIOS_DEBUG_REGISTER BiosDebugMsr;
  BiosDebugMsr.Uint64 = AsmReadMsr64 (ADL_MSR_BIOS_DEBUG);
  return BiosDebugMsr.Bits.PatchRevIdStatusPppeLoadFail == 0;
}

/**
  This function returns if Turbo RatioLimit is Programmable or not

  @retval TRUE if Turbo RatioLimit is Programmable
          FALSE if Turbo RatioLimit is Not Programmable
**/
BOOLEAN
MsrIsTurboRatioLimitProgrammable (
  VOID
  )
{
  ADL_MSR_PLATFORM_INFO_REGISTER PlatformInfoMsr;
  PlatformInfoMsr.Uint64 = AsmReadMsr64 (ADL_MSR_PLATFORM_INFO);
  return PlatformInfoMsr.Bits.PrgTurboRatioEn != 0;
}

/**
  This function returns if TDP Limit is Programmable or not

  @retval TRUE if TDP Limit is Programmable
          FALSE if TDP Limit is Not Programmable
**/
BOOLEAN
MsrIsTdpLimitProgrammable (
  VOID
  )
{
  ADL_MSR_PLATFORM_INFO_REGISTER PlatformInfoMsr;
  PlatformInfoMsr.Uint64 = AsmReadMsr64 (ADL_MSR_PLATFORM_INFO);
  return PlatformInfoMsr.Bits.PrgTdpLimEn != 0;
}
/**
  This function returns if Timed Mwait is Supported or Not

  @retval TRUE if Timed Mwait is Supported
          FALSE if Timed Mwait is Not Supported
**/
BOOLEAN
MsrIsTimedMwaitSupported (
  VOID
  )
{
  ADL_MSR_PLATFORM_INFO_REGISTER PlatformInfoMsr;
  PlatformInfoMsr.Uint64 = AsmReadMsr64 (ADL_MSR_PLATFORM_INFO);
  return PlatformInfoMsr.Bits.TimedMwaitEnable != 0;
}

/**
  Return if CPU supports PFAT

  @retval TRUE             If CPU Supports
  @retval FALSE            If CPU doesn't Supports
**/
BOOLEAN
MsrIsPfatEnabled (
  VOID
  )
{
  ADL_MSR_PLATFORM_INFO_REGISTER  PlatformInfoMsr;
  PlatformInfoMsr.Uint64 = AsmReadMsr64 (ADL_MSR_PLATFORM_INFO);
  return (BOOLEAN) PlatformInfoMsr.Bits.PfatEnable;
}

/**
  Return if CPU Supports LPM

  @retval TRUE             If Supports
  @retval FALSE            If not supports
**/
BOOLEAN
MsrIsCpuSupportsLpm (
  VOID
  )
{
  ADL_MSR_PLATFORM_INFO_REGISTER  PlatformInfoMsr;
  PlatformInfoMsr.Uint64 = AsmReadMsr64 (ADL_MSR_PLATFORM_INFO);
  return (BOOLEAN) PlatformInfoMsr.Bits.LpmSupport;
}

/**
  This function is to get platform PowerLimit.

  @param[out]  PowerLimit1
  @param[out]  PowerLimit1En
  @param[out]  PowerLimit1Time
  @param[out]  PowerLimit2
  @param[out]  PowerLimit2En
  @param[out]  Lock
**/
VOID
MsrGetPlatformPowerLimit (
  OUT UINT32   *PowerLimit1,
  OUT BOOLEAN  *PowerLimit1En,
  OUT UINT32   *PowerLimit1Time,
  OUT UINT32   *PowerLimit2,
  OUT BOOLEAN  *PowerLimit2En,
  OUT BOOLEAN  *Lock
  )
{
  MSR_PLATFORM_POWER_LIMIT_REGISTER PlatformPowerLimitMsr;

  PlatformPowerLimitMsr.Uint64 = AsmReadMsr64 (MSR_PLATFORM_POWER_LIMIT);

  if (PowerLimit1 != NULL) {
    *PowerLimit1 = PlatformPowerLimitMsr.Bits.PowerLimit1;
  }

  if (PowerLimit1En != NULL) {
    *PowerLimit1En = (BOOLEAN) PlatformPowerLimitMsr.Bits.PowerLimit1En;
  }

  if (PowerLimit1Time != NULL) {
    *PowerLimit1Time = PlatformPowerLimitMsr.Bits.PowerLimit1Time;
  }

  if (PowerLimit2 != NULL) {
    *PowerLimit2 = PlatformPowerLimitMsr.Bits.PowerLimit2;
  }

  if (PowerLimit2En != NULL) {
    *PowerLimit2En = (BOOLEAN) PlatformPowerLimitMsr.Bits.PowerLimit2En;
  }

  if (Lock != NULL) {
    *Lock = (BOOLEAN) PlatformPowerLimitMsr.Bits.Lock;
  }
}

/**
  This function is to set platform PowerLimit. All input parameters
  must be provided to override the existing platform PowerLimit value.

  @param[in]  PowerLimit1
  @param[in]  PowerLimit1En
  @param[in]  PowerLimit1Time
  @param[in]  PowerLimit2
  @param[in]  PowerLimit2En
  @param[in]  Lock
**/
VOID
MsrSetPlatformPowerLimit (
  IN UINT32   PowerLimit1,
  IN BOOLEAN  PowerLimit1En,
  IN UINT32   PowerLimit1Time,
  IN UINT32   PowerLimit2,
  IN BOOLEAN  PowerLimit2En,
  IN BOOLEAN  Lock
  )
{
  MSR_PLATFORM_POWER_LIMIT_REGISTER PlatformPowerLimitMsr;

  PlatformPowerLimitMsr.Uint64 = AsmReadMsr64 (MSR_PLATFORM_POWER_LIMIT);

  PlatformPowerLimitMsr.Bits.PowerLimit1 = PowerLimit1;
  PlatformPowerLimitMsr.Bits.PowerLimit1En = (UINT32) PowerLimit1En;
  //
  // Always set to allows processor to go below the OS requested P States.
  //
  PlatformPowerLimitMsr.Bits.CriticalPowerClamp1 = 1;
  PlatformPowerLimitMsr.Bits.PowerLimit1Time = PowerLimit1Time;
  PlatformPowerLimitMsr.Bits.PowerLimit2 = PowerLimit2;
  PlatformPowerLimitMsr.Bits.PowerLimit2En = (UINT32) PowerLimit2En;
  PlatformPowerLimitMsr.Bits.Lock = (UINT32) Lock;

  AsmWriteMsr64 (MSR_PLATFORM_POWER_LIMIT, PlatformPowerLimitMsr.Uint64);
}

/**
  Return if CPU Supports SMM_SUPOVR_STATE_LOCK

  @retval TRUE             If CPU supports
  @retval FALSE            If CPU doesn't support
**/
BOOLEAN
MsrIsSmmSupovrStateLockSupported (
  VOID
  )
{
  ADL_MSR_PLATFORM_INFO_REGISTER  PlatformInfoMsr;
  PlatformInfoMsr.Uint64 = AsmReadMsr64 (ADL_MSR_PLATFORM_INFO);
  return (BOOLEAN) PlatformInfoMsr.Bits.SmmSupovrStateLockEnable;
}

/**

  This function returns bit per logical processor indicating whether
  the logical processor is in the middle of long flow and hence will
  delay servicing of SMI.

  @retval Bit per logical processor

**/
UINT64
MsrGetSmmDelayed (
  VOID
  )
{
  ADL_MSR_SMM_DELAYED_REGISTER  SmmDelayed;
  SmmDelayed.Uint64 = AsmReadMsr64 (ADL_MSR_SMM_DELAYED);
  return SmmDelayed.Bits.LogProc;
}

/**
  This function returns bit per logical processor indicating whether
  the logical processor is in state where SMIs are blocked.

  @retval Bit per logical processor

**/
UINT64
MsrGetSmmBlocked (
  VOID
  )
{
  ADL_MSR_SMM_BLOCKED_CNT_REGISTER  SmmBlocked;
  SmmBlocked.Uint64 = AsmReadMsr64 (ADL_MSR_SMM_BLOCKED_CNT);
  return SmmBlocked.Bits.BlockedCount;
}

/**
  Return if Edram Enable

  @retval TRUE             If Edram Enable
  @retval FALSE            If Edram Disable
**/
BOOLEAN
MsrIsEdramEnable(
  VOID
  )
{
  ADL_MSR_PLATFORM_INFO_REGISTER  PlatformInfoMsr;
  PlatformInfoMsr.Uint64 = AsmReadMsr64 (ADL_MSR_PLATFORM_INFO);
  return PlatformInfoMsr.Bits.EdramEnable != 0;
}

/**
  Sets WRC COS of specific policy

  @param[in] UINT8   Wrc COS Policy to set ways
  @param[in] UINT32  COS Ways Mask

  @retval EFI_SUCCESS           set WRC COS successful
  @retval EFI_INVALID_PARAMETER invalid COS policy
**/
EFI_STATUS
MsrSetWrcCos (
  IN UINT8     CosPolicy,
  IN UINT32    CosWaysMask
  )
{
  return EFI_SUCCESS;
}

/**
  This function return if AesXts256 is supported

  @retval TRUE             If AesXts256 is supported
  @retval FALSE            If AesXts256 is not supported
**/
BOOLEAN
MsrIsAesXts256Supported (
  VOID
  )
{
  ADL_MSR_TME_CAPABILITY_REGISTER    TmeCapability;

  ///
  /// MK-TME MSRs can only be accessed when hardware
  /// supports it.  Otherwise, system will throw exception.
  ///
  if (IsTmeSupported () == TRUE) {
    TmeCapability.Uint64 = 0;
    TmeCapability.Uint64 = AsmReadMsr64 (ADL_MSR_TME_CAPABILITY);
    return TmeCapability.Bits.AesXts256 != 0;
  }

  return FALSE;
}

/**
  This function return MkTmeMaxKeyidBits value

  @retval MkTmeMaxKeyidBits value
**/
UINT32
MsrGetMkTmeMaxKeyidBits (
  VOID
  )
{
  ADL_MSR_TME_CAPABILITY_REGISTER    TmeCapability;

  ///
  /// MK-TME MSRs can only be accessed when hardware
  /// supports it.  Otherwise, system will throw exception.
  ///
  if (IsTmeSupported () == TRUE) {
    TmeCapability.Uint64 = 0;
    TmeCapability.Uint64 = AsmReadMsr64 (ADL_MSR_TME_CAPABILITY);
    return (UINT32) TmeCapability.Bits.MkTmeMaxKeyidBits;
  }

  return 0;
}

/**
  This function return MkTmeMaxKey value

  @retval MkTmeMaxKey value
**/
UINT32
MsrGetMkTmeMaxKey (
  VOID
  )
{
  ADL_MSR_TME_CAPABILITY_REGISTER    TmeCapability;

  ///
  /// MK-TME MSRs can only be accessed when hardware
  /// supports it.  Otherwise, system will throw exception.
  ///
  if (IsTmeSupported () == TRUE) {
    TmeCapability.Uint64 = 0;
    TmeCapability.Uint64 = AsmReadMsr64 (ADL_MSR_TME_CAPABILITY);
    return (UINT32) TmeCapability.Bits.MkTmeMaxKeys;
  }

  return 0;
}

/**
  This function is to configure TME Activate MSR.

  @param[in]  TmeEnable
  @param[in]  KeySelect
  @param[in]  SaveKeyForStandby
  @param[in]  TmePolicy
  @param[in]  MkTmeKeyIdBits
  @param[in]  MkTmeCryptoAlgs
**/
VOID
MsrConfigureTmeActivate (
  IN UINT8   TmeEnable,
  IN UINT8   KeySelect,
  IN UINT8   SaveKeyForStandby,
  IN UINT8   TmePolicy,
  IN UINT8   MkTmeKeyIdBits,
  IN UINT16  MkTmeCryptoAlgs
  )
{
  ADL_MSR_TME_ACTIVATE_REGISTER      TmeActivate;

  ///
  /// MK-TME MSRs can only be accessed when hardware
  /// supports it.  Otherwise, system will throw exception.
  ///
  if (IsTmeSupported () == TRUE) {
    TmeActivate.Uint64                 = 0;
    TmeActivate.Bits.TmeEnable         = TmeEnable;
    TmeActivate.Bits.KeySelect         = KeySelect;
    TmeActivate.Bits.SaveKeyForStandby = SaveKeyForStandby;
    TmeActivate.Bits.TmePolicy         = TmePolicy;
    TmeActivate.Bits.MkTmeKeyidBits    = MkTmeKeyIdBits;
    TmeActivate.Bits.MkTmeCryptoAlgs   = MkTmeCryptoAlgs;

    AsmWriteMsr64 (ADL_MSR_TME_ACTIVATE, TmeActivate.Uint64);
  }
}

/**
  This function write-lock the TME Activate MSR.

  @param[in]  TmeEnable
**/
VOID
MsrLockTmeActivate (
  IN UINT32  TmeEnable
  )
{
  ADL_MSR_TME_ACTIVATE_REGISTER      TmeActivate;

  ///
  /// MK-TME MSRs can only be accessed when hardware
  /// supports it.  Otherwise, system will throw exception.
  ///
  if (IsTmeSupported () == TRUE) {
    TmeActivate.Uint64                 = 0;
    TmeActivate.Bits.TmeEnable         = TmeEnable;
    AsmWriteMsr64 (ADL_MSR_TME_ACTIVATE, TmeActivate.Uint64);
  }
}

/**
  Return if Lock bit in TME Activate MSR is set

  @retval TRUE             If lock bit is set
  @retval FALSE            If lock bit is not set
**/
BOOLEAN
MsrIsTmeActivateLockSet (
  VOID
  )
{
  ADL_MSR_TME_ACTIVATE_REGISTER      TmeActivate;

  ///
  /// MK-TME MSRs can only be accessed when hardware
  /// supports it.  Otherwise, system will throw exception.
  ///
  if (IsTmeSupported () == TRUE) {
    TmeActivate.Uint64 = 0;
    TmeActivate.Uint64 = AsmReadMsr64 (ADL_MSR_TME_ACTIVATE);
    return TmeActivate.Bits.Lock != 0;
  }

  return 0;
}

/**
  Return if TME is enabled

  @retval TRUE             If TME enable & lock bit is set
  @retval FALSE            If TME enable & lock bit is not set
**/
BOOLEAN
MsrIsTmeEnabled (
  VOID
  )
{
  ADL_MSR_TME_ACTIVATE_REGISTER      TmeActivate;

  ///
  /// MK-TME MSRs can only be accessed when hardware
  /// supports it.  Otherwise, system will throw exception.
  ///
  if (IsTmeSupported () == TRUE) {
    TmeActivate.Uint64 = 0;
    TmeActivate.Uint64 = AsmReadMsr64 (ADL_MSR_TME_ACTIVATE);

    if ((TmeActivate.Bits.Lock == 1) &&
        (TmeActivate.Bits.TmeEnable == 1)) {
      return TRUE;
    }
  }

  return FALSE;
}

/**
  Return if the processor is a preproduction sample

  @retval TRUE          If the processor is a preproduction sample
  @retval FALSE         If the part is intended for production
**/
BOOLEAN
MsrIsSamplePartFru (
  VOID
  )
{
  ADL_MSR_PLATFORM_INFO_REGISTER    PlatformInfoMsr;
  PlatformInfoMsr.Uint64 = AsmReadMsr64 (ADL_MSR_PLATFORM_INFO);
  return PlatformInfoMsr.Bits.SamplePart != 0;
}

/**
  Return if FME_ACTIVATE MSR can be setup and
  the FZM functionality can be enabled

  @retval TRUE          If FME_ACTIVATE MSR can be setup and
                        the FZM functionality can be enabled
  @retval FALSE         If FME_ACTIVATE MSR can not be setup and
                        the FZM functionality can not be enabled
**/
BOOLEAN
MsrIsSxp2lmEnabled (
  VOID
  )
{
  ADL_MSR_PLATFORM_INFO_REGISTER    PlatformInfoMsr;
  PlatformInfoMsr.Uint64 = AsmReadMsr64 (ADL_MSR_PLATFORM_INFO);
  return PlatformInfoMsr.Bits.Sxp2lmEnable != 0;
}

/**
  This function is to Enable RAR Timer
**/
VOID
MsrEnableRarTimer (
  VOID
  )
{
  ADL_MSR_RAR_TIMER_CONFIG_REGISTER   RarTimerMsr;
  MSR_CORE_CAPABILITIES_REGISTER      CoreCapabilitiesMsr;

  CoreCapabilitiesMsr.Uint64 = AsmReadMsr64 (MSR_CORE_CAPABILITIES);
  if (CoreCapabilitiesMsr.Bits.RarSupported != 0) {
    RarTimerMsr.Uint64 = AsmReadMsr64 (ADL_MSR_RAR_TIMER_CONFIG);
    //
    // The reset value of this MSR is "Timer Disabled".
    // Set this MSR to a default of 0, meaning RAR TIMER disabled.
    //
    RarTimerMsr.Bits.Threshold = 0;
    AsmWriteMsr64 (ADL_MSR_RAR_TIMER_CONFIG, RarTimerMsr.Uint64);
  }
  return;
}

/**
  This function returns Max Turbo Ratio.

  @return Max Turbo Ratio
**/
UINT8
MsrGetMaxTurboRatio (
  VOID
  )
{
  ADL_MSR_BIGCORE_TURBO_RATIO_LIMIT_REGISTER MsrTurboRatioLimitRatio;

  //
  // Read BIGCORE turbo ratio limit ratio
  //
  MsrTurboRatioLimitRatio.Uint64 = AsmReadMsr64 (ADL_MSR_BIGCORE_TURBO_RATIO_LIMIT);
  return (UINT8) MsrTurboRatioLimitRatio.Bits.MaxTurboGroup0;
}

/**
  Disable or enable CPU Crashlog dump feature by MSR.

  @param[in] BOOLEAN      Policy for CrashLog
**/
VOID
MsrEnableCpuCrashLog (
  IN  BOOLEAN                 CrashLogEnable
  )
{
  MSR_CRASHLOG_CONTROL_REGISTER    CurrentValue;
  MSR_CRASHLOG_CONTROL_REGISTER    ExpectedValue;

  CurrentValue.Uint64 = AsmReadMsr64 (MSR_CRASHLOG_CONTROL);

  ExpectedValue.Uint64 = CurrentValue.Uint64;

  if (CrashLogEnable) {
    ExpectedValue.Bits.Cddis = 0;
  } else {
    ExpectedValue.Bits.Cddis = 1;;
  }

  if (CurrentValue.Uint64 != ExpectedValue.Uint64) {
    //
    // MSR value does not match policy.
    // Write MSR with the expected value.
    //
    AsmWriteMsr64 (MSR_CRASHLOG_CONTROL, ExpectedValue.Uint64);
  }
}

/**
  This function set GrashLog GPRs (General Purpose Registers) Mask
  Which allows GPRs to not be included in the Crash Log CPU register data.

  @param[in] BOOLEAN     Policy for CrashLog Enable
  @param[in] UINT8       Policy for CrashLog GPRs dump
**/
VOID
MsrSetCrashLogGprsMask (
  IN  BOOLEAN                 CrashLogEnable,
  IN  UINT8                   CrashLogGprsEnable
  )
{
  ADL_MSR_CRASHLOG_CONTROL_REGISTER    CurrentValue;
  ADL_MSR_CRASHLOG_CONTROL_REGISTER    ExpectedValue;

  CurrentValue.Uint64 = AsmReadMsr64 (MSR_CRASHLOG_CONTROL);
  ExpectedValue.Uint64 = CurrentValue.Uint64;

  //
  // Disable Gprs dump when CrashLog is disabled
  //
  if (CrashLogEnable) {
    switch (CrashLogGprsEnable) {
      //
      // 0: GPRs dump masked
      // 1: GPRs dump enabled
      // 2: GPRs dump enabled but SMM GPRs masked
      //
      case 0:
        ExpectedValue.Bits.EnGprs = 0;
        ExpectedValue.Bits.EnGprsInSmm = 0;
        break;
      case 1:
        ExpectedValue.Bits.EnGprs = 1;
        ExpectedValue.Bits.EnGprsInSmm = 1;
        break;
      case 2:
        ExpectedValue.Bits.EnGprs = 1;
        ExpectedValue.Bits.EnGprsInSmm = 0;
        break;
      default:
        break;
    }
  } else {
    ExpectedValue.Bits.EnGprs = 0;
    ExpectedValue.Bits.EnGprsInSmm = 0;
  }
  if (CurrentValue.Uint64 != ExpectedValue.Uint64) {
    AsmWriteMsr64 (MSR_CRASHLOG_CONTROL, ExpectedValue.Uint64);
  }
}
