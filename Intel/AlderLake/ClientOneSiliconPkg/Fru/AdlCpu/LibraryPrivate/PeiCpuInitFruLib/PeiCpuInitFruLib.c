/** @file
  This file is the implementation for PeiCpuInitFru library.

@copyright
  INTEL CONFIDENTIAL
  Copyright 2020 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains an 'Intel Peripheral Driver' and is uniquely identified as
  "Intel Reference Module" and is licensed for Intel CPUs and chipsets under
  the terms of your license agreement with Intel or your vendor. This file may
  be modified by the user, subject to additional terms of the license agreement.

@par Specification
**/

#include <Register/CommonMsr.h>
#include <Register/AdlMsr.h>
#include <Library/PeiCpuInitFruLib.h>
#include <Library/BaseMemoryLib.h>
#include <Library/PcdLib.h>
#include <Library/CpuPlatformLib.h>
#include <Register/CommonMsr.h>
#include <Register/Cpuid.h>
#include <Library/MsrFruLib.h>
#include <Library/PreSiliconEnvDetectLib.h>
#include <Library/MemoryAllocationLib.h>
#include <Library/CpuMailboxLib.h>
#include <Library/CpuInfoFruLib.h>
#include <Library/PeiOcLib.h>

//
// MCHECK BIOS Parameter Structure per MCHECK 2.5 CORE SAS
//
#define MCHECK_BIOS_PARAM_INFO_UUID    {0x13, 0x43, 0xF1, 0x5C, 0xAA, 0x00, 0x42, 0x07, 0xA2, 0x14, 0xA8, 0x4F, 0x35, 0x58, 0x22, 0xA2}
#define MCHECK_STRUCTURE_VERSION       3
#define C6DRAM_ENABLE                  BIT2
#define KEYLOCKER_ENABLE               BIT3
#define MAX_BUFFER_SIZE 16

typedef union
{
  UINT8               Uuid[16];
  UINT64              Uuid64[2];
} UUID;

typedef struct {
  UUID                Uuid;
  UINT16              Size;
  UINT16              Version;
  UINT8               Reserved[12];
} BIOS_PARAM_HEADER;

typedef struct {
  BIOS_PARAM_HEADER   Header;
  UINT64              Features;
  UINT8               SgxInfoBuffer[MAX_BUFFER_SIZE];
  UINT8               SeamInfoBuffer[MAX_BUFFER_SIZE];
  UINT8               TopologyInfoBuffer[MAX_BUFFER_SIZE];
} MCHECK_BIOS_PARAM_INFO;

///
/// Structure for CPU Strap settings
///
typedef struct {
  UINT32 HtDisabled              :  1; ///< Intel HT Technology Disable.
  UINT32 NumberOfActiveBigCores  :  4; ///< Number of Active Big Cores.
  UINT32 Bist                    :  1; ///< Built In Self Test (BIST) initiation.
  UINT32 FlexRatio               :  6; ///< Flex Ratio.
  UINT32 BootRatio               :  1; ///< Processor boot ratio; When set allows the processor to power up in maximum non-turbo ratio from the following boot.
  UINT32 JtagC10PowerGateDisable :  1; ///< JTAG Power gate disable.
  UINT32 CoreHighVoltageMode     :  1; ///< Enable all IA FIVR domains voltage higher than 1.85v.
  UINT32 NonCoreHighVoltageMode  :  1; ///< Enable all GT and CLR FIVR domains voltage higher than 1.85v.
  UINT32 EnDebugDisable          :  1; ///< Encrypted debug disable.
  UINT32 NumberOfActiveAtomCores :  6; ///< Number of enabled Atom cores.
  UINT32 DlvrForceBypass         :  1; ///< Enable DLVR into bypass mode.
  UINT32 Reserved                :  6; ///< RESERVED.
  UINT32 KvmrCaptureDis          :  1; ///< Disable CSME VDM messages by OEM.
  UINT32 KvmrSpriteDis           :  1; ///< Disable CSME VDM messages by OEM.
} CPU_STRAP_SET;

/**
  CPU SSC configuration for specific generation.

  @param[in] ComputeDieSscEnable    - Enable/Disable Compute Die SSC.
  @param[in] SocDieSscEnable        - Enable/Disable Soc Die SSC.
  @param[in] ComputeDieSscValue     - Compute Die SSC value to config.
  @param[in] SocDieSscValue         - Soc Die SSC value to config.

**/
EFI_STATUS
EFIAPI
PeiCpuSscConfigPreMem (
  IN BOOLEAN    ComputeDieSscEnable,
  IN BOOLEAN    SocDieSscEnable,
  IN UINT8      ComputeDieSscValue,
  IN UINT8      SocDieSscValue
  )
{
  return EFI_SUCCESS;
}

/**
  Load CPU power management basic Config block default for specific generation.

  @param[in, out] CpuPowerMgmtBasicConfig   Pointer to CPU_POWER_MGMT_BASIC_CONFIG instance

**/
VOID
EFIAPI
PeiCpuLoadPowerMgmtBasicConfigDefault (
  IN OUT CPU_POWER_MGMT_BASIC_CONFIG    *CpuPowerMgmtBasicConfig
  )
{
  ADL_MSR_BIGCORE_TURBO_RATIO_LIMIT_REGISTER        MsrTurboRatioLimitRatio;
  ADL_MSR_BIGCORE_TURBO_RATIO_LIMIT_CORES_REGISTER  MsrTurboRatioLimitCores;
  ADL_MSR_ATOM_TURBO_RATIO_LIMIT_REGISTER           AtomMsrTurboRatioLimitRatio;
  MSR_TURBO_RATIO_LIMIT_CORES_REGISTER              AtomMsrTurboRatioLimitCores;
  UINT8                                             AllSmallCoreCount;

  AllSmallCoreCount = 0;
  MsrTurboRatioLimitRatio.Uint64 = AsmReadMsr64 (ADL_MSR_BIGCORE_TURBO_RATIO_LIMIT);
  CpuPowerMgmtBasicConfig->TurboRatioLimitRatio[0] = (UINT8) MsrTurboRatioLimitRatio.Bits.MaxTurboGroup7;
  CpuPowerMgmtBasicConfig->TurboRatioLimitRatio[1] = (UINT8) MsrTurboRatioLimitRatio.Bits.MaxTurboGroup7;
  CpuPowerMgmtBasicConfig->TurboRatioLimitRatio[2] = (UINT8) MsrTurboRatioLimitRatio.Bits.MaxTurboGroup7;
  CpuPowerMgmtBasicConfig->TurboRatioLimitRatio[3] = (UINT8) MsrTurboRatioLimitRatio.Bits.MaxTurboGroup7;
  CpuPowerMgmtBasicConfig->TurboRatioLimitRatio[4] = (UINT8) MsrTurboRatioLimitRatio.Bits.MaxTurboGroup7;
  CpuPowerMgmtBasicConfig->TurboRatioLimitRatio[5] = (UINT8) MsrTurboRatioLimitRatio.Bits.MaxTurboGroup7;
  CpuPowerMgmtBasicConfig->TurboRatioLimitRatio[6] = (UINT8) MsrTurboRatioLimitRatio.Bits.MaxTurboGroup7;
  CpuPowerMgmtBasicConfig->TurboRatioLimitRatio[7] = (UINT8) MsrTurboRatioLimitRatio.Bits.MaxTurboGroup7;

  MsrTurboRatioLimitCores.Uint64 = AsmReadMsr64 (ADL_MSR_BIGCORE_TURBO_RATIO_LIMIT_CORES);
  CpuPowerMgmtBasicConfig->TurboRatioLimitNumCore[0] = (UINT8) MsrTurboRatioLimitCores.Bits.Numcore0;
  CpuPowerMgmtBasicConfig->TurboRatioLimitNumCore[1] = (UINT8) MsrTurboRatioLimitCores.Bits.Numcore1;
  CpuPowerMgmtBasicConfig->TurboRatioLimitNumCore[2] = (UINT8) MsrTurboRatioLimitCores.Bits.Numcore2;
  CpuPowerMgmtBasicConfig->TurboRatioLimitNumCore[3] = (UINT8) MsrTurboRatioLimitCores.Bits.Numcore3;
  CpuPowerMgmtBasicConfig->TurboRatioLimitNumCore[4] = (UINT8) MsrTurboRatioLimitCores.Bits.Numcore4;
  CpuPowerMgmtBasicConfig->TurboRatioLimitNumCore[5] = (UINT8) MsrTurboRatioLimitCores.Bits.Numcore5;
  CpuPowerMgmtBasicConfig->TurboRatioLimitNumCore[6] = (UINT8) MsrTurboRatioLimitCores.Bits.Numcore6;
  CpuPowerMgmtBasicConfig->TurboRatioLimitNumCore[7] = (UINT8) MsrTurboRatioLimitCores.Bits.Numcore7;

  GetCpuSupportedCoresAndAtomCores (NULL, &AllSmallCoreCount);

  if ((AllSmallCoreCount != 0) && !IsSimicsEnvironment ()  && !IsHSLEEnvironment ()) {
    AtomMsrTurboRatioLimitRatio.Uint64 = AsmReadMsr64 (ADL_MSR_ATOM_TURBO_RATIO_LIMIT);
    CpuPowerMgmtBasicConfig->AtomTurboRatioLimitRatio[0] = (UINT8) AtomMsrTurboRatioLimitRatio.Bits.MaxTurboGroup0;
    CpuPowerMgmtBasicConfig->AtomTurboRatioLimitRatio[1] = (UINT8) AtomMsrTurboRatioLimitRatio.Bits.MaxTurboGroup1;
    CpuPowerMgmtBasicConfig->AtomTurboRatioLimitRatio[2] = (UINT8) AtomMsrTurboRatioLimitRatio.Bits.MaxTurboGroup2;
    CpuPowerMgmtBasicConfig->AtomTurboRatioLimitRatio[3] = (UINT8) AtomMsrTurboRatioLimitRatio.Bits.MaxTurboGroup3;
    CpuPowerMgmtBasicConfig->AtomTurboRatioLimitRatio[4] = (UINT8) AtomMsrTurboRatioLimitRatio.Bits.MaxTurboGroup4;
    CpuPowerMgmtBasicConfig->AtomTurboRatioLimitRatio[5] = (UINT8) AtomMsrTurboRatioLimitRatio.Bits.MaxTurboGroup5;
    CpuPowerMgmtBasicConfig->AtomTurboRatioLimitRatio[6] = (UINT8) AtomMsrTurboRatioLimitRatio.Bits.MaxTurboGroup6;
    CpuPowerMgmtBasicConfig->AtomTurboRatioLimitRatio[7] = (UINT8) AtomMsrTurboRatioLimitRatio.Bits.MaxTurboGroup7;

    AtomMsrTurboRatioLimitCores.Uint64 = AsmReadMsr64 (ADL_MSR_ATOM_TURBO_RATIO_LIMIT_CORES);
    CpuPowerMgmtBasicConfig->AtomTurboRatioLimitNumCore[0] = (UINT8) AtomMsrTurboRatioLimitCores.Bits.Numcore0;
    CpuPowerMgmtBasicConfig->AtomTurboRatioLimitNumCore[1] = (UINT8) AtomMsrTurboRatioLimitCores.Bits.Numcore1;
    CpuPowerMgmtBasicConfig->AtomTurboRatioLimitNumCore[2] = (UINT8) AtomMsrTurboRatioLimitCores.Bits.Numcore2;
    CpuPowerMgmtBasicConfig->AtomTurboRatioLimitNumCore[3] = (UINT8) AtomMsrTurboRatioLimitCores.Bits.Numcore3;
    CpuPowerMgmtBasicConfig->AtomTurboRatioLimitNumCore[4] = (UINT8) AtomMsrTurboRatioLimitCores.Bits.Numcore4;
    CpuPowerMgmtBasicConfig->AtomTurboRatioLimitNumCore[5] = (UINT8) AtomMsrTurboRatioLimitCores.Bits.Numcore5;
    CpuPowerMgmtBasicConfig->AtomTurboRatioLimitNumCore[6] = (UINT8) AtomMsrTurboRatioLimitCores.Bits.Numcore6;
    CpuPowerMgmtBasicConfig->AtomTurboRatioLimitNumCore[7] = (UINT8) AtomMsrTurboRatioLimitCores.Bits.Numcore7;
  }
}

/**
  Set Turbo Ratio Limit for specific generation.

  @param[in] CpuPowerMgmtBasicConfig    Pointer to CPU_POWER_MGMT_BASIC_CONFIG instance

**/
VOID
EFIAPI
PeiCpuSetTurboRatioLimit (
  IN CPU_POWER_MGMT_BASIC_CONFIG    *CpuPowerMgmtBasicConfig
  )
{
  ADL_MSR_BIGCORE_TURBO_RATIO_LIMIT_REGISTER        MsrTurboRatioLimitRatio;
  ADL_MSR_BIGCORE_TURBO_RATIO_LIMIT_CORES_REGISTER  MsrTurboRatioLimitCores;
  ADL_MSR_ATOM_TURBO_RATIO_LIMIT_REGISTER           AtomMsrTurboRatioLimitRatio;
  ADL_MSR_ATOM_TURBO_RATIO_LIMIT_CORES_REGISTER     AtomMsrTurboRatioLimitCores;
  UINT8                                             Index;
  UINT8                                             MaxBusRatio;
  BOOLEAN                                           Valid;
  UINT8                                             AllSmallCoreCount;

  MaxBusRatio = MsrGetMaxNonTurboRatio ();
  Valid = TRUE;
  AllSmallCoreCount = 0;
  //
  // RATIO_[i] must be not less than Max Non-Turbo Ratio and
  // RATIO_[i+1] must be less than or equal to RATIO_[i]
  //
  for (Index = 0; Index < TURBO_RATIO_LIMIT_ARRAY_SIZE - 1; Index++) {
    if (Index == 0) {
      if (CpuPowerMgmtBasicConfig->TurboRatioLimitRatio[0] != 0 &&
          CpuPowerMgmtBasicConfig->TurboRatioLimitRatio[0] < MaxBusRatio) {
        Valid = FALSE;
        break;
      }
    }

    if (CpuPowerMgmtBasicConfig->TurboRatioLimitRatio[Index+1] != 0 &&
        (CpuPowerMgmtBasicConfig->TurboRatioLimitRatio[Index+1] < MaxBusRatio ||
         CpuPowerMgmtBasicConfig->TurboRatioLimitRatio[Index+1] > CpuPowerMgmtBasicConfig->TurboRatioLimitRatio[Index])) {
      Valid = FALSE;
      break;
    }
  }

  if (Valid) {
    MsrTurboRatioLimitRatio.Bits.MaxTurboGroup0 = CpuPowerMgmtBasicConfig->TurboRatioLimitRatio[0];
    MsrTurboRatioLimitRatio.Bits.MaxTurboGroup1 = CpuPowerMgmtBasicConfig->TurboRatioLimitRatio[1];
    MsrTurboRatioLimitRatio.Bits.MaxTurboGroup2 = CpuPowerMgmtBasicConfig->TurboRatioLimitRatio[2];
    MsrTurboRatioLimitRatio.Bits.MaxTurboGroup3 = CpuPowerMgmtBasicConfig->TurboRatioLimitRatio[3];
    MsrTurboRatioLimitRatio.Bits.MaxTurboGroup4 = CpuPowerMgmtBasicConfig->TurboRatioLimitRatio[4];
    MsrTurboRatioLimitRatio.Bits.MaxTurboGroup5 = CpuPowerMgmtBasicConfig->TurboRatioLimitRatio[5];
    MsrTurboRatioLimitRatio.Bits.MaxTurboGroup6 = CpuPowerMgmtBasicConfig->TurboRatioLimitRatio[6];
    MsrTurboRatioLimitRatio.Bits.MaxTurboGroup7 = CpuPowerMgmtBasicConfig->TurboRatioLimitRatio[7];
    AsmWriteMsr64 (ADL_MSR_BIGCORE_TURBO_RATIO_LIMIT, MsrTurboRatioLimitRatio.Uint64);
  }

  Valid = TRUE;

  //
  // NUMCORE_[i+1] must be greater than NUMCORE_[i]
  //
  for (Index = 0; Index < TURBO_RATIO_LIMIT_ARRAY_SIZE - 1; Index++) {
    if (CpuPowerMgmtBasicConfig->TurboRatioLimitNumCore[Index+1] != 0 &&
        CpuPowerMgmtBasicConfig->TurboRatioLimitNumCore[Index+1] <= CpuPowerMgmtBasicConfig->TurboRatioLimitNumCore[Index]) {
      Valid = FALSE;
      break;
    }
  }

  if (Valid) {
    MsrTurboRatioLimitCores.Bits.Numcore0 = CpuPowerMgmtBasicConfig->TurboRatioLimitNumCore[0];
    MsrTurboRatioLimitCores.Bits.Numcore1 = CpuPowerMgmtBasicConfig->TurboRatioLimitNumCore[1];
    MsrTurboRatioLimitCores.Bits.Numcore2 = CpuPowerMgmtBasicConfig->TurboRatioLimitNumCore[2];
    MsrTurboRatioLimitCores.Bits.Numcore3 = CpuPowerMgmtBasicConfig->TurboRatioLimitNumCore[3];
    MsrTurboRatioLimitCores.Bits.Numcore4 = CpuPowerMgmtBasicConfig->TurboRatioLimitNumCore[4];
    MsrTurboRatioLimitCores.Bits.Numcore5 = CpuPowerMgmtBasicConfig->TurboRatioLimitNumCore[5];
    MsrTurboRatioLimitCores.Bits.Numcore6 = CpuPowerMgmtBasicConfig->TurboRatioLimitNumCore[6];
    MsrTurboRatioLimitCores.Bits.Numcore7 = CpuPowerMgmtBasicConfig->TurboRatioLimitNumCore[7];
    AsmWriteMsr64 (ADL_MSR_BIGCORE_TURBO_RATIO_LIMIT_CORES, MsrTurboRatioLimitCores.Uint64);
  }

  GetCpuSupportedCoresAndAtomCores (NULL, &AllSmallCoreCount);

  if ((AllSmallCoreCount != 0) && !IsSimicsEnvironment () && !IsHSLEEnvironment ()) {
    Valid = TRUE;

    //
    // RATIO_[i+1] must be less than or equal to RATIO_[i]
    //
    for (Index = 0; Index < TURBO_RATIO_LIMIT_ARRAY_SIZE - 1; Index++) {
      if (CpuPowerMgmtBasicConfig->AtomTurboRatioLimitRatio[Index+1] != 0 &&
          CpuPowerMgmtBasicConfig->AtomTurboRatioLimitRatio[Index+1] > CpuPowerMgmtBasicConfig->AtomTurboRatioLimitRatio[Index]) {
        Valid = FALSE;
        break;
      }
    }

    if (Valid) {
      AtomMsrTurboRatioLimitRatio.Bits.MaxTurboGroup0 = CpuPowerMgmtBasicConfig->AtomTurboRatioLimitRatio[0];
      AtomMsrTurboRatioLimitRatio.Bits.MaxTurboGroup1 = CpuPowerMgmtBasicConfig->AtomTurboRatioLimitRatio[1];
      AtomMsrTurboRatioLimitRatio.Bits.MaxTurboGroup2 = CpuPowerMgmtBasicConfig->AtomTurboRatioLimitRatio[2];
      AtomMsrTurboRatioLimitRatio.Bits.MaxTurboGroup3 = CpuPowerMgmtBasicConfig->AtomTurboRatioLimitRatio[3];
      AtomMsrTurboRatioLimitRatio.Bits.MaxTurboGroup4 = CpuPowerMgmtBasicConfig->AtomTurboRatioLimitRatio[4];
      AtomMsrTurboRatioLimitRatio.Bits.MaxTurboGroup5 = CpuPowerMgmtBasicConfig->AtomTurboRatioLimitRatio[5];
      AtomMsrTurboRatioLimitRatio.Bits.MaxTurboGroup6 = CpuPowerMgmtBasicConfig->AtomTurboRatioLimitRatio[6];
      AtomMsrTurboRatioLimitRatio.Bits.MaxTurboGroup7 = CpuPowerMgmtBasicConfig->AtomTurboRatioLimitRatio[7];
      AsmWriteMsr64 (ADL_MSR_ATOM_TURBO_RATIO_LIMIT, AtomMsrTurboRatioLimitRatio.Uint64);
    }

    Valid = TRUE;

    //
    // NUMCORE_[i+1] must be greater than NUMCORE_[i]
    //
    for (Index = 0; Index < TURBO_RATIO_LIMIT_ARRAY_SIZE - 1; Index++) {
      if (CpuPowerMgmtBasicConfig->AtomTurboRatioLimitNumCore[Index+1] != 0 &&
          CpuPowerMgmtBasicConfig->AtomTurboRatioLimitNumCore[Index+1] <= CpuPowerMgmtBasicConfig->AtomTurboRatioLimitNumCore[Index]) {
        Valid = FALSE;
        break;
      }
    }

    if (Valid) {
      AtomMsrTurboRatioLimitCores.Bits.Numcore0 = CpuPowerMgmtBasicConfig->AtomTurboRatioLimitNumCore[0];
      AtomMsrTurboRatioLimitCores.Bits.Numcore1 = CpuPowerMgmtBasicConfig->AtomTurboRatioLimitNumCore[1];
      AtomMsrTurboRatioLimitCores.Bits.Numcore2 = CpuPowerMgmtBasicConfig->AtomTurboRatioLimitNumCore[2];
      AtomMsrTurboRatioLimitCores.Bits.Numcore3 = CpuPowerMgmtBasicConfig->AtomTurboRatioLimitNumCore[3];
      AtomMsrTurboRatioLimitCores.Bits.Numcore4 = CpuPowerMgmtBasicConfig->AtomTurboRatioLimitNumCore[4];
      AtomMsrTurboRatioLimitCores.Bits.Numcore5 = CpuPowerMgmtBasicConfig->AtomTurboRatioLimitNumCore[5];
      AtomMsrTurboRatioLimitCores.Bits.Numcore6 = CpuPowerMgmtBasicConfig->AtomTurboRatioLimitNumCore[6];
      AtomMsrTurboRatioLimitCores.Bits.Numcore7 = CpuPowerMgmtBasicConfig->AtomTurboRatioLimitNumCore[7];
      AsmWriteMsr64 (ADL_MSR_ATOM_TURBO_RATIO_LIMIT_CORES, AtomMsrTurboRatioLimitCores.Uint64);
    }
  }
}

/**
  Compare the hyper threading setup option against the CPU strap setting
  and in case of mismatch request a reset.

  @param[in] CpuStrapSetData           - The current strap setting data.
  @param[in] HyperThreading            - hyper threading setting from BIOS Setup option.

  @retval NO_RESET          - No reset is needed.
  @retval WARM_RESET        - Update requires a warm reset.
**/
CPU_RESET_TYPE
PeiCpuStrapHtEnableDisable (
  IN VOID              *CpuStrapSetData,
  IN UINT8             HyperThreading
  )
{
  CPU_STRAP_SET      *CpuStrapSet;
  CPU_RESET_TYPE     ResetType;
  UINT16             TotalThreadsPerCore;
  CPUID_EXTENDED_TOPOLOGY_EBX  CpuidExtendedTopologyEbx;

  ResetType   = NO_RESET;
  CpuStrapSet = (CPU_STRAP_SET *) CpuStrapSetData;

  ///
  /// Read CPUID(0xB) with ECX=0 for number of logical processors per Core.
  /// This value does NOT change based on Intel HT Technology disable and core disables.
  ///
  AsmCpuidEx (CPUID_EXTENDED_TOPOLOGY, 0, NULL, &CpuidExtendedTopologyEbx.Uint32, NULL, NULL);
  TotalThreadsPerCore = (UINT16) CpuidExtendedTopologyEbx.Bits.LogicalProcessors;

  if (TotalThreadsPerCore > 1) {
    ///
    /// HT is supported
    ///
    DEBUG ((DEBUG_INFO, "HT is supported\n"));
    ///
    /// Check if the configuration of HT matches the BIOS Setup option.
    ///
    if (CpuStrapSet->HtDisabled == HyperThreading) {
      ///
      /// HT configuration doesn't match BIOS Setup option; update the Strap Set Data and Issue a Warm reset
      ///
      DEBUG ((DEBUG_INFO, "HT configuration doesn't match the setup value\n"));
      CpuStrapSet->HtDisabled = !(HyperThreading);
      ResetType |= WARM_RESET;
    }
  } else {
    ///
    /// HT is not supported by default fusing.
    ///
    DEBUG ((DEBUG_WARN, "HT is NOT supported\n"));
    CpuStrapSet->HtDisabled = 1;
  }

  return ResetType;
}

/**
  Compare the number of active cores setup option against the CPU strap setting
  and in case of mismatch request a reset.

  @param[in] CpuStrapSetData             - The current strap setting.
  @param[in] ActiveCoreCount             - Active big core count.
  @param[in] ActiveSmallCoreCount        - Active small core count.
  @param[in] ActiveSocNorthAtomCoreCount - Active Soc North atom core count.

  @retval NO_RESET          - No reset is needed.
  @retval WARM_RESET        - Update requires a warm reset.
  @retval COLD_RESET        - Update requires a cold reset.
**/
CPU_RESET_TYPE
PeiCpuStrapSetActiveCores (
  IN VOID              *CpuStrapSetData,
  IN UINT8             ActiveCoreCount,
  IN UINT8             ActiveSmallCoreCount,
  IN UINT8             ActiveSocNorthAtomCoreCount
  )
{
  CPU_STRAP_SET                   *CpuStrapSet;
  CPU_RESET_TYPE                  ResetType;
  MSR_CORE_THREAD_COUNT_REGISTER  MsrCoreThreadCount;
  UINT32                          NumberOfActiveCores;
  UINT32                          NumberOfActiveThreads;
  UINT8                           AllCoreCount;
  UINT8                           AllSmallCoreCount;

  ResetType         = NO_RESET;
  AllCoreCount      = 0;
  AllSmallCoreCount = 0;
  CpuStrapSet       = (CPU_STRAP_SET *) CpuStrapSetData;

  /* CORE_THREAD_COUNT - msr 0x35
     Symbol      Name        MSB:LSB  Description
     CoreCount   CoreCount   31:16    The Core Count reflects the enabled cores based
                                      on the above thread count and the value of threads_
                                      available (to determine if HT is on) at reset time.

     ThreadCount ThreadCount 15:0     The Thread Count reflects the enabled threads based
                                      on the factory-configured thread count and the value
                                      of the PCH Soft Reset Data register for Client processors
                                      at reset time.

     Read MSR for Active Core and Thread Count.
  */
  MsrCoreThreadCount.Uint64 = AsmReadMsr64 (MSR_CORE_THREAD_COUNT);
  NumberOfActiveCores       = MsrCoreThreadCount.Bits.Corecount;
  NumberOfActiveThreads     = MsrCoreThreadCount.Bits.Threadcount;

  DEBUG ((DEBUG_INFO, "Number of Active Cores / Threads = 0x%x / 0x%x\n", NumberOfActiveCores, NumberOfActiveThreads));

  //
  // Mapping {ALL, ALL} into {0,0} before CPU strap setting since the pCode default value is {0,0}.
  //
  GetCpuSupportedCoresAndAtomCores (&AllCoreCount, &AllSmallCoreCount);

  if (ActiveCoreCount == 0xFF) {
    ActiveCoreCount = AllCoreCount;
  }

  if (ActiveSmallCoreCount == 0xFF) {
    ActiveSmallCoreCount = AllSmallCoreCount;
  }

  if (ActiveCoreCount == AllCoreCount && ActiveSmallCoreCount == AllSmallCoreCount) {
    ActiveCoreCount = 0;
    ActiveSmallCoreCount = 0;
  }

  ///
  /// Check if the configuration of "Active Big Cores" matches the BIOS Setup option.
  ///
  if (CpuStrapSet->NumberOfActiveBigCores != ActiveCoreCount) {
    DEBUG ((
      DEBUG_INFO,
      "Number Of active big cores doesn't match the value from Setup = %x / %x\n",
      CpuStrapSet->NumberOfActiveBigCores,
      ActiveCoreCount
      ));
    CpuStrapSet->NumberOfActiveBigCores = ActiveCoreCount;
    ResetType |= COLD_RESET;
  }

  ///
  /// Check if the configuration of "Active Small Cores" matches the BIOS Setup option.
  ///
  if (CpuStrapSet->NumberOfActiveAtomCores != ActiveSmallCoreCount) {
    DEBUG ((
      DEBUG_INFO,
      "Number Of active atom cores doesn't match the value from Setup = %x / %x\n",
      CpuStrapSet->NumberOfActiveAtomCores,
      ActiveSmallCoreCount
      ));
    CpuStrapSet->NumberOfActiveAtomCores = ActiveSmallCoreCount;
    ResetType |= COLD_RESET;
  }

  return ResetType;
}

/**
  Compare the BIST setup option against the CPU strap setting
  and in case of mismatch request a reset.

  @param[in] CpuStrapSetData    - The current strap setting.
  @param[in] BistOnReset        - BistOnReset CPU Test Config Policy.

  @retval NO_RESET          - No reset is needed.
  @retval WARM_RESET        - Update requires a warm reset.
**/
CPU_RESET_TYPE
PeiCpuStrapBistEnableDisable (
  IN VOID              *CpuStrapSetData,
  IN UINT8             BistOnReset
  )
{
  CPU_STRAP_SET     *CpuStrapSet;

  CpuStrapSet = (CPU_STRAP_SET *) CpuStrapSetData;

  if (CpuStrapSet->Bist == BistOnReset) {
    return NO_RESET;
  } else {
    CpuStrapSet->Bist = BistOnReset;
    DEBUG ((DEBUG_INFO, "BIST configuration doesn't match the setup value\n"));
    return WARM_RESET;
  }
}

/**
  Compare the flex multiplier setup options against the CPU strap settings
  and in case of mismatch request a reset.

  @param[in] CpuStrapSetData    - The current strap setting.
  @param[in] CpuRatio           - CpuRatio CPU policy.

  @retval NO_RESET          - No reset is needed.
  @retval WARM_RESET        - Update requires a warm reset.
**/
CPU_RESET_TYPE
PeiCpuStrapProgramFlexMultiplier (
  IN VOID                        *CpuStrapSetData,
  IN UINT8                       CpuRatio
  )
{
  CPU_STRAP_SET             *CpuStrapSet;
  MSR_FLEX_RATIO_REGISTER   MsrFlexRatio;
  CPU_RESET_TYPE            ResetType;

  ResetType     = NO_RESET;
  CpuStrapSet   = (CPU_STRAP_SET *) CpuStrapSetData;

  DEBUG ((DEBUG_INFO, "Ratio from Policy is 0x%X\n", CpuRatio));

  ///
  /// Read and save current Flex Ratio data; disregard enable bit (MSR 194h [15:8])
  ///
  MsrFlexRatio.Uint64 = AsmReadMsr64 (MSR_FLEX_RATIO);
  DEBUG ((DEBUG_INFO, "Current Flex Ratio from MSR(FLEX_RATIO) is 0x%X\n", MsrFlexRatio.Bits.FlexRatio));
  DEBUG ((DEBUG_INFO, "Current Flex Ratio from CPU Strap: 0x%X\n", CpuStrapSet->FlexRatio));
  ///
  /// Check and set Flex Ratio to requested ratio if possible
  ///
  if (CpuRatio == CpuStrapSet->FlexRatio) {
    ///
    /// Do nothing, ratio is already set to requested value and enabled
    ///
    DEBUG ((DEBUG_INFO, "No need to set Flex Ratio.\n"));
  } else {
    ///
    /// Set Flex Ratio to user selected value
    ///
    MsrFlexRatio.Bits.FlexRatio = CpuRatio;
    MsrFlexRatio.Bits.Enable = 1;
    AsmWriteMsr64 (MSR_FLEX_RATIO, MsrFlexRatio.Uint64);

    ///
    /// Set Soft Reset Data for Flex Ratio
    ///
    CpuStrapSet->FlexRatio = CpuRatio;

    ///
    /// Set RESET flag
    ///
    DEBUG ((DEBUG_INFO, "Setting Flex Ratio to 0x%X\n", CpuStrapSet->FlexRatio));
    ResetType = WARM_RESET;
  }

  return ResetType;
}

/**
  Compare the boot frequency setup option against the boot ratio strap setting
  and in case of mismatch request a reset.

  @param[in] CpuStrapSetData    - The current strap setting.
  @param[in] BootFrequency      - BootFrequency CPU policy.

  @retval NO_RESET          - No reset is needed.
  @retval WARM_RESET        - Update requires a warm reset.
**/
CPU_RESET_TYPE
PeiCpuStrapBootRatioEnableDisable (
  IN VOID                *CpuStrapSetData,
  IN UINT8               BootFrequency
  )
{
  CPU_STRAP_SET       *CpuStrapSet;
  CPU_RESET_TYPE      ResetType;

  ResetType   = NO_RESET;
  CpuStrapSet = (CPU_STRAP_SET *) CpuStrapSetData;

  ///
  /// Check if the configuration of BootRatio from Bit12 of strap setting is not aligned with the BootFrequency setup option.
  ///
  if (((CpuStrapSet->BootRatio == 1) && (BootFrequency == 0)) ||
      ((CpuStrapSet->BootRatio == 0) && (BootFrequency > 0))) {
    DEBUG ((
      DEBUG_INFO,
      "Boot Ratio strap setting of %x does not match the BootFrequency policy %x\n",
      CpuStrapSet->BootRatio,
      BootFrequency
      ));
    if (BootFrequency > 0) {
      CpuStrapSet->BootRatio = 1;
    } else {
      CpuStrapSet->BootRatio = 0;
    }
    ResetType |= WARM_RESET;
  }

  return ResetType;
}

/**
  Compare the JTAG power gate setup option against the CPU strap setting
  and in case of mismatch request a reset.

  @param[in] CpuStrapSet    - The current strap setting.
  @param[in] JtagC10PowerGateDisable - JtagC10PowerGateDisable CPU policy.

  @retval NO_RESET          - No reset is needed.
  @retval WARM_RESET        - Update requires a warm reset.
**/
CPU_RESET_TYPE
PeiCpuStrapJtagPowerGateEnableDisable (
  IN VOID                    *CpuStrapSetData,
  IN UINT8                   JtagC10PowerGateDisable
  )
{
  CPU_STRAP_SET       *CpuStrapSet;
  CPU_RESET_TYPE      ResetType;

  ResetType   = NO_RESET;
  CpuStrapSet = (CPU_STRAP_SET *) CpuStrapSetData;

  ///
  /// Check if the configuration of "JtagC10PowerGateDisable" from Bit13 of strap setting matches the BIOS Setup option.
  ///
  if (CpuStrapSet->JtagC10PowerGateDisable != JtagC10PowerGateDisable) {
    DEBUG ((
      DEBUG_INFO,
      "JtagC10PowerGateDisable strap setting doesn't match the value from Setup = %x / %x\n",
      CpuStrapSet->JtagC10PowerGateDisable,
      JtagC10PowerGateDisable
      ));
    CpuStrapSet->JtagC10PowerGateDisable = JtagC10PowerGateDisable;
    ResetType |= WARM_RESET;
  }

  return ResetType;
}


/**
  Compare the Dlvr Bypass Mode setup option against the CPU strap setting
  and in case of mismatch request a reset.

  @param[in] CpuStrapSetData    - The current strap setting.
  @param[in] DlvrBypassMode     - DlvrBypassMode Config Policy.

  @retval NO_RESET          - No reset is needed.
  @retval WARM_RESET        - Update requires a warm reset.
**/
CPU_RESET_TYPE
PeiCpuStrapDlvrSetting (
  IN VOID              *CpuStrapSetData,
  IN UINT8             DlvrBypassModeEnable
  )
{
  CPU_STRAP_SET     *CpuStrapSet;

  CpuStrapSet = (CPU_STRAP_SET *) CpuStrapSetData;

  if (CpuStrapSet->DlvrForceBypass == DlvrBypassModeEnable) {
    return NO_RESET;
  } else {
    CpuStrapSet->DlvrForceBypass = DlvrBypassModeEnable;
    DEBUG ((DEBUG_INFO, "DlvrBypassMode configuration doesn't match the setup value\n"));
    return COLD_RESET;
  }
}


/**
  Return Total Memory Encryption (TME) default policy setting

  @retval Total Memory Encryption (TME) default policy setting
**/
UINT32
PeiCpuTmeDefaultSetting (
  VOID
  )
{
  return 0;
}

/**
  Disable or enable CPU Crashlog dump feature by Mailbox.

  @param[in] BOOLEAN      Policy for CrashLog
**/
VOID
PeiCpuCrashLogMailboxEnable (
  IN  BOOLEAN  CrashLogEnable
  )
{

}

/**
  Program the BIOS Parameters required for MCHECK module.
**/
VOID
ProgramBiosParamsForMcheck (
  VOID
  )
{
  MCHECK_BIOS_PARAM_INFO    *BiosParamInfo = NULL;
  UINT8                     BiosParamInfoUuid[] = MCHECK_BIOS_PARAM_INFO_UUID;
  CPUID_STRUCTURED_EXTENDED_FEATURE_FLAGS_ECX  CpuidStructuredExtendedFeatureEcx;
  //
  // BIOS_INFO Structure should be 4K Aligned
  //
  BiosParamInfo = (MCHECK_BIOS_PARAM_INFO *) AllocateZeroPool (SIZE_4KB * 2);
  BiosParamInfo = (MCHECK_BIOS_PARAM_INFO *) (((UINTN) BiosParamInfo + SIZE_4KB - 1) & ~(SIZE_4KB - 1));
  if (BiosParamInfo == NULL) {
    DEBUG ((DEBUG_INFO, "Error: Cannot allocate memory for BiosParamInfo\n"));
    ASSERT (FALSE);
    return;
  }

  CopyMem (BiosParamInfo->Header.Uuid.Uuid, BiosParamInfoUuid, sizeof (UUID));
  BiosParamInfo->Header.Size = sizeof (MCHECK_BIOS_PARAM_INFO) - sizeof (BIOS_PARAM_HEADER);
  BiosParamInfo->Header.Version = MCHECK_STRUCTURE_VERSION;

  //
  // Bit Map of features
  //
  // SEAM        BIT1
  // C6DRAM      BIT2
  // Key Locker  BIT3
  // FuSa SAF    BIT4
  //
  BiosParamInfo->Features = C6DRAM_ENABLE;
  //
  // Check if Keylocker is enabled in the FUSES
  //
  AsmCpuidEx (CPUID_STRUCTURED_EXTENDED_FEATURE_FLAGS,
              CPUID_STRUCTURED_EXTENDED_FEATURE_FLAGS_SUB_LEAF_INFO,
              NULL,
              NULL,
              &CpuidStructuredExtendedFeatureEcx.Uint32,
              NULL
              );
  //
  // Update the KL feature flag for Hybrid or Small Core only Config
  //
  if ((CpuidStructuredExtendedFeatureEcx.Uint32 & BIT23) && (IsHeteroCoreSupported () || (GetCpuFamily () == CPUID_FULL_FAMILY_MODEL_ALDERLAKE_ATOM))) {
    BiosParamInfo->Features |= KEYLOCKER_ENABLE;
  }
  AsmWriteMsr64 (MSR_MCHECK_BOOT_SCENARIO, (UINTN) BiosParamInfo);
  return;
}

/**
  Return the boot frequency default policy setting

  @retval Boot Frequency default policy setting
**/
UINT32
PeiCpuBootFreqDefaultSetting (
  VOID
  )
{

  return 2;
}

/**
  Set the PRMRR region.

  @param PrmrrBase PRMRR base address
  @param PrmrrSize PRMRR size

  @retval RETURN_SUCCESS       PRMRR region is set successfully.
  @retval RETURN_UNSUPPORTED   PRMRR is not supported.
  @retval RETURN_ACCESS_DENIED PRMRR region is already locked and cannot be changed.
*/
RETURN_STATUS
PeiCpuSetPrmrrRegion (
  UINT64 PrmrrBase,
  UINT32 PrmrrSize
  )
{
  return SetPrmrrMsr (PrmrrBase, PrmrrSize);
}

/**
  Update CpuStrap base on platform type

  @param[in] CpuStrapSetData    - The current strap setting.
  @param[in] VendorId           - VR VendorId.
  @param[in] ProdId             - VR ProdId.

  @retval NO_RESET          - No reset is needed.
  @retval WARM_RESET        - Update requires a warm reset.
**/
CPU_RESET_TYPE
PeiCpuStrapPlatformType (
  IN UINT32 *CpuStrapSetData,
  IN UINT32 VendorId,
  IN UINT32 ProdId
  )
{
  //
  // Ignore all ADL segemnts
  //
  if (GetCpuGeneration () == EnumAdlCpu) {
    return NO_RESET;
  }

  if (((*CpuStrapSetData & BIT19) != 0) && (VendorId == 0x1E && (ProdId == 0x0 || ProdId == 0x08))) {
    //
    // Clear BIT19 to indicate platform is not RPL
    //
    *CpuStrapSetData &= ~(BIT19);
    return WARM_RESET;
  } else if ((*CpuStrapSetData & BIT19) == 0) {
    //
    // Override BIT19 to indicate platform is RPL
    //
    *CpuStrapSetData |= BIT19;
    return WARM_RESET;
  }
  return NO_RESET;
}

