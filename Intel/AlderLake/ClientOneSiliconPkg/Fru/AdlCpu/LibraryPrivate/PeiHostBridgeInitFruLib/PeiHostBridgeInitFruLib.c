/** @file
  PEIM Private Library to initialize PeiHostBridgeInitFruLib

@copyright
  INTEL CONFIDENTIAL
  Copyright 2019 - 2020 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains an 'Intel Peripheral Driver' and is uniquely identified as
  "Intel Reference Module" and is licensed for Intel CPUs and chipsets under
  the terms of your license agreement with Intel or your vendor. This file may
  be modified by the user, subject to additional terms of the license agreement.

@par Specification Reference:
**/

#include <Ppi/SiPolicy.h>
#include <Library/PeiServicesLib.h>
#include <Library/DebugLib.h>
#include <Library/IoLib.h>
#include <Library/BaseMemoryLib.h>
#include <Library/PciSegmentLib.h>
#include <Register/SaRegsHostBridge.h>
#include <Library/HobLib.h>
#include <Library/CpuPlatformLib.h>
#include <Library/MsrFruLib.h>

typedef union {
  struct {
    UINT64 Lock                                    :  1;   // Bits 0:0
    UINT64 Rsvd1                                   :  9;   // Bits 9:1
    UINT64 ImrIaExcBaseLow                         :  22;  // Bits 31:10
    UINT64 ImrIaExcBaseHigh                        :  7;   // Bits 38:32
    UINT64 Rsvd2                                   :  25;  // Bits 63:39
  } Bits;
  UINT64 Data;
} IMRIAEXCBASE_MCHBAR_CBO_INGRESS_STRUCT;

typedef union {
  struct {
    UINT64 Lock                                    :  1;   // Bits 0:0
    UINT64 Rsvd1                                   :  9;   // Bits 9:1
    UINT64 ImrIaExcLimitLow                        :  22;  // Bits 31:10
    UINT64 ImrIaExcLimitHigh                       :  7;   // Bits 38:32
    UINT64 Rsvd2                                   :  25;  // Bits 63:39
  } Bits;
  UINT64 Data;
} IMRIAEXCLIMIT_MCHBAR_CBO_INGRESS_STRUCT;

typedef union {
  struct {
    UINT32  Low;
    UINT32  High;
  } Data32;
  UINT64 Data;
} UINT64_STRUCT;

/**
  Programs HostBridge Bars

  @param[in] MiscPeiPreMemConfig - Instance of SA_MISC_PEI_PREMEM_CONFIG
**/
VOID
ProgramHostBridgeBars (
  VOID
  )
{
  UINT64                        McD0BaseAddress;
  HOST_BRIDGE_PREMEM_CONFIG     *HostBridgePreMemConfig;
  SI_PREMEM_POLICY_PPI          *SiPreMemPolicyPpi;
  EFI_STATUS                    Status;

  ///
  /// Get policy settings through the SiPreMemPolicyPpi PPI
  ///
  Status = PeiServicesLocatePpi (
             &gSiPreMemPolicyPpiGuid,
             0,
             NULL,
             (VOID **) &SiPreMemPolicyPpi
             );
  ASSERT_EFI_ERROR (Status);
  if ((Status != EFI_SUCCESS) || (SiPreMemPolicyPpi == NULL)) {
    DEBUG ((DEBUG_INFO, "Fail to locate SiPreMemPolicyPpi\n"));
    return;
  }

  HostBridgePreMemConfig = NULL;
  Status = GetConfigBlock ((VOID *) SiPreMemPolicyPpi, &gHostBridgePeiPreMemConfigGuid, (VOID *) &HostBridgePreMemConfig);
  ASSERT_EFI_ERROR (Status);

  ///
  /// Program MchBar, DmiBar and EpBar
  ///
  McD0BaseAddress = PCI_SEGMENT_LIB_ADDRESS (SA_SEG_NUM, SA_MC_BUS, SA_MC_DEV, SA_MC_FUN, 0);

  PciSegmentWrite32 (McD0BaseAddress + R_SA_MCHBAR + 4, (UINT32)RShiftU64 (HostBridgePreMemConfig->MchBar, 32));
  PciSegmentWrite32 (McD0BaseAddress + R_SA_MCHBAR, HostBridgePreMemConfig->MchBar | BIT0);

  PciSegmentWrite32 (McD0BaseAddress + R_SA_DMIBAR + 4, (UINT32)RShiftU64 (HostBridgePreMemConfig->DmiBar, 32));
  PciSegmentWrite32 (McD0BaseAddress + R_SA_DMIBAR, HostBridgePreMemConfig->DmiBar | BIT0);

  PciSegmentWrite32 (McD0BaseAddress + R_SA_PXPEPBAR + 4, (UINT32)RShiftU64 (HostBridgePreMemConfig->EpBar, 32));
  PciSegmentWrite32 (McD0BaseAddress + R_SA_PXPEPBAR, HostBridgePreMemConfig->EpBar | BIT0);

  ///
  /// Program RegBar
  ///
  MmioWrite32 ((UINTN)(HostBridgePreMemConfig->MchBar) + R_SA_MCHBAR_REGBAR_OFFSET + 4, (UINT32)RShiftU64 (HostBridgePreMemConfig->RegBar, 32));
  MmioWrite32 ((UINTN)(HostBridgePreMemConfig->MchBar) + R_SA_MCHBAR_REGBAR_OFFSET, (UINT32)(HostBridgePreMemConfig->RegBar | BIT0));

  //
  // This function builds a HOB that describes a chunk of system memory.
  //
  BuildResourceDescriptorHob (
    EFI_RESOURCE_MEMORY_MAPPED_IO,
    (EFI_RESOURCE_ATTRIBUTE_PRESENT | EFI_RESOURCE_ATTRIBUTE_INITIALIZED | EFI_RESOURCE_ATTRIBUTE_UNCACHEABLE),
    HostBridgePreMemConfig->RegBar,
    V_SA_MCHBAR_REGBAR_SIZE
    );
  //
  // This function builds a HOB for the memory allocation.
  //
  BuildMemoryAllocationHob (
    HostBridgePreMemConfig->RegBar,
    V_SA_MCHBAR_REGBAR_SIZE,
    EfiMemoryMappedIO
    );

  ///
  /// Detect EDRAM
  ///
  if (MsrIsEdramEnable ()) {
    ///
    /// Program EdramBar
    ///
    MmioWrite32 ((UINTN) (HostBridgePreMemConfig->MchBar) + R_SA_MCHBAR_EDRAMBAR_OFFSET + 4, (UINT32)RShiftU64 (HostBridgePreMemConfig->EdramBar, 32));
    MmioWrite32 ((UINTN) (HostBridgePreMemConfig->MchBar) + R_SA_MCHBAR_EDRAMBAR_OFFSET, (UINT32)(HostBridgePreMemConfig->EdramBar | BIT0));
  }
}


/**
  This function Program IA/GT IMR Exclusion

  @param[in] IMR Base
  @param[in] IMR Limit
**/
VOID
SetIaGtImrExclusion (
  UINT32  ImrBase,
  UINT32  ImrLimit
  )
{
  UINT32                                   MchBar;
  IMRIAEXCBASE_MCHBAR_CBO_INGRESS_STRUCT   ImrExcBase;
  IMRIAEXCLIMIT_MCHBAR_CBO_INGRESS_STRUCT  ImrExcLimit;

  DEBUG ((DEBUG_INFO, "SetIaGtImrExclusion Enter\n"));

  MchBar = PciSegmentRead32 (PCI_SEGMENT_LIB_ADDRESS (SA_SEG_NUM, SA_MC_BUS, SA_MC_DEV, SA_MC_FUN, R_SA_MCHBAR)) & ~BIT0;
  // Program IMR IA/GT Exclusion Base and Limit
  //    IMRIAEXCLBASE (MCHBAR + 0x6A40h[38:10]) and IMRGTEXCLBASE (MCHBAR + 0x6A48h[38:10]) should be programmed to the lowest address (base) of the IMR region.
  //    IMRIAEXCLIMIT (MCHBAR + 0x6A50h[38:10]) and IMRGTEXCLIMIT (MCHBAR + 0x6A58h[38:10]) should be programmed to the highest address (base + IMR Size) of the IMR region.
  ImrExcBase.Data                     = 0;
  ImrExcLimit.Data                    = 0;
  ImrExcBase.Bits.ImrIaExcBaseLow     = ImrBase << 10;
  ImrExcBase.Bits.ImrIaExcBaseHigh    = ImrBase >> 12;
  ImrExcLimit.Bits.ImrIaExcLimitLow   = ImrLimit << 10;
  ImrExcLimit.Bits.ImrIaExcLimitHigh  = ImrLimit >> 12;

  MmioWrite32 (MchBar + R_SA_MCHBAR_IMRIAEXCBASE_CBO_INGRESS_REG + 4, (UINT32) RShiftU64 (ImrExcBase.Data, 32));
  MmioWrite32 (MchBar + R_SA_MCHBAR_IMRIAEXCBASE_CBO_INGRESS_REG, (UINT32) ImrExcBase.Data);

  MmioWrite32 (MchBar + R_SA_MCHBAR_IMRIAEXCLIMIT_CBO_INGRESS_REG + 4, (UINT32) RShiftU64 (ImrExcLimit.Data, 32));
  MmioWrite32 (MchBar + R_SA_MCHBAR_IMRIAEXCLIMIT_CBO_INGRESS_REG, (UINT32) ImrExcLimit.Data);

  MmioWrite32 (MchBar + R_SA_MCHBAR_IMRGTEXCBASE_CBO_INGRESS_REG + 4, (UINT32) RShiftU64 (ImrExcBase.Data, 32));
  MmioWrite32 (MchBar + R_SA_MCHBAR_IMRGTEXCBASE_CBO_INGRESS_REG, (UINT32) ImrExcBase.Data);

  MmioWrite32 (MchBar + R_SA_MCHBAR_IMRGTEXCLIMIT_CBO_INGRESS_REG + 4, (UINT32) RShiftU64 (ImrExcLimit.Data, 32));
  MmioWrite32 (MchBar + R_SA_MCHBAR_IMRGTEXCLIMIT_CBO_INGRESS_REG, (UINT32) ImrExcLimit.Data);

  DEBUG ((DEBUG_INFO, "SetIaGtImrExclusion End\n"));
}

/**
  This function Lock IA/GT IMR Exclusion
**/
VOID
SetIaGtImrExclusionLock (
  VOID
  )
{
  UINT32                                   MchBar;

  DEBUG ((DEBUG_INFO, "SetIaGtImrExclusionLock Enter\n"));
  MchBar = PciSegmentRead32 (PCI_SEGMENT_LIB_ADDRESS (SA_SEG_NUM, SA_MC_BUS, SA_MC_DEV, SA_MC_FUN, R_SA_MCHBAR)) & ~BIT0;

  // Lock IMR IA/GT Exclusion Base and Limit
  MmioOr64 (MchBar + R_SA_MCHBAR_IMRIAEXCBASE_CBO_INGRESS_REG, BIT0);
  MmioOr64 (MchBar + R_SA_MCHBAR_IMRIAEXCLIMIT_CBO_INGRESS_REG, BIT0);
  MmioOr64 (MchBar + R_SA_MCHBAR_IMRGTEXCBASE_CBO_INGRESS_REG, BIT0);
  MmioOr64 (MchBar + R_SA_MCHBAR_IMRGTEXCLIMIT_CBO_INGRESS_REG, BIT0);

  DEBUG ((DEBUG_INFO, "SetIaGtImrExclusionLock End\n"));
}


/**
  ProgramEdramMode - Disable EDRAM by default and enable it through HW Test Mode policy if needed

  @param[in] HostBridgePeiConfig - Instance of HOST_BRIDGE_PEI_CONFIG
**/
VOID
ProgramEdramMode (
  IN    HOST_BRIDGE_PEI_CONFIG      *HostBridgePeiConfig
  )
{
  UINT32                    Data32;
  UINT64_STRUCT             MchBar;

  MchBar.Data32.High = PciSegmentRead32 (PCI_SEGMENT_LIB_ADDRESS (SA_SEG_NUM, SA_MC_BUS, SA_MC_DEV, SA_MC_FUN, R_SA_MCHBAR + 4));
  MchBar.Data32.Low  = PciSegmentRead32 (PCI_SEGMENT_LIB_ADDRESS (SA_SEG_NUM, SA_MC_BUS, SA_MC_DEV, SA_MC_FUN, R_SA_MCHBAR));
  MchBar.Data       &= (UINT64) ~BIT0;

  Data32 = 0x00000000;  // Edram HW mode by default
  if (HostBridgePeiConfig->EdramTestMode == 0) {
    Data32 = 0x80000000; //EDRAM SW Disable
    DEBUG ((DEBUG_INFO, "Edram SW Disabled %x\n", Data32));
  } else if (HostBridgePeiConfig->EdramTestMode == 1) {
    Data32 = 0x80000001; //EDRAM SW Enable
    DEBUG ((DEBUG_INFO, "Edram SW Enabled %x\n", Data32));
  }
  DEBUG ((DEBUG_INFO, " Writing MchBar + 0x5878 :  %X\n", Data32));
  MmioWrite32 ((UINTN) MchBar.Data + 0x5878, Data32);

}
