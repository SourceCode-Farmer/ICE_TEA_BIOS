/** @file
  This file contains the Cpu Information for specific generation.

@copyright
  INTEL CONFIDENTIAL
  Copyright 2019 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains an 'Intel Peripheral Driver' and is uniquely identified as
  "Intel Reference Module" and is licensed for Intel CPUs and chipsets under
  the terms of your license agreement with Intel or your vendor. This file may
  be modified by the user, subject to additional terms of the license agreement.

@par Specification
**/
#include <CpuGenInfoFruLib.h>
#include <Library/BaseLib.h>
#include <Library/DebugLib.h>
#include <Register/Cpuid.h>
#include <Library/CpuPlatformLib.h>
#include <Library/CpuInfoFruLib.h>
#include <Library/CpuCommonLib.h>
#include <Register/ArchitecturalMsr.h>
#include <Register/ArchMsr.h>
#include <Register/AdlMsr.h>
#include <Library/MemoryAllocationLib.h>
#include <Register/CommonMsr.h>
#include <IndustryStandard/SmBios.h>
#include <Library/PcdLib.h>
#include <Library/MsrFruLib.h>
#include <Library/PchInfoLib.h>
#include <IndustryStandard/Pci22.h>
#include <Register/IgdRegs.h>

#define CTDP_NOMINAL_LEVEL 0x00

STATIC CONST CHAR8 mAdlCpuFamilyString[] = "AlderLake";
typedef struct {
  UINT32  CPUID;
  UINT8   CpuSku;
  CHAR8   *String;
} CPU_REV;

typedef struct {
  CPU_IDENTIFIER                 CpuIdentifier;
  UINT8                          SupportedCores;
  UINT8                          SupportedAtomCores;
} CPU_CORE_COUNT;

GLOBAL_REMOVE_IF_UNREFERENCED CONST CPU_REV  mProcessorRevisionTable[] = {
  {CPUID_FULL_FAMILY_MODEL_ALDERLAKE_DT_HALO     + EnumAdlA0, EnumCpuTrad,    "A0"},
  {CPUID_FULL_FAMILY_MODEL_ALDERLAKE_DT_HALO     + EnumAdlB0, EnumCpuTrad,    "B0"},
  {CPUID_FULL_FAMILY_MODEL_ALDERLAKE_DT_HALO     + EnumAdlC0, EnumCpuTrad,    "C0"},
  {CPUID_FULL_FAMILY_MODEL_ALDERLAKE_DT_HALO     + EnumAdlD0, EnumCpuTrad,    "D0"},
  {CPUID_FULL_FAMILY_MODEL_ALDERLAKE_DT_HALO     + EnumAdlG0, EnumCpuTrad,    "G0"},
  {CPUID_FULL_FAMILY_MODEL_ALDERLAKE_DT_HALO     + EnumAdlH0, EnumCpuTrad,    "H0"},
  {CPUID_FULL_FAMILY_MODEL_ALDERLAKE_MOBILE      + EnumAdlJ0, EnumCpuUlt,     "J0"},
  {CPUID_FULL_FAMILY_MODEL_ALDERLAKE_MOBILE      + EnumAdlK0, EnumCpuUlt,     "K0"},
  {CPUID_FULL_FAMILY_MODEL_ALDERLAKE_MOBILE      + EnumAdlL0, EnumCpuUlt,     "L0"},
  {CPUID_FULL_FAMILY_MODEL_ALDERLAKE_MOBILE      + EnumAdlQ0, EnumCpuUlt,     "Q0"},
  {CPUID_FULL_FAMILY_MODEL_ALDERLAKE_MOBILE      + EnumAdlR0, EnumCpuUlt,     "R0"},
  {CPUID_FULL_FAMILY_MODEL_ALDERLAKE_MOBILE      + EnumAdlQ0, EnumCpuUlx,     "Q0"},
  {CPUID_FULL_FAMILY_MODEL_ALDERLAKE_MOBILE      + EnumAdlR0, EnumCpuUlx,     "R0"},
  {CPUID_FULL_FAMILY_MODEL_ALDERLAKE_MOBILE      + EnumAdlS0, EnumCpuUlx,     "S0"},
};

///
/// PowerLimits Override table for all SKUs. Non-cTDP parts would have '0' data for TDP level information.
///
GLOBAL_REMOVE_IF_UNREFERENCED PPM_OVERRIDE_TABLE mPowerLimitsOverrideTable[] = {
///
/// CpuIdentifier                      TDP      MSR PL1   MSR PL2    TdpUp    TdpUp    TdpNominal   TdpNominal   TdpDown    TdpDown      MSR     MSR Disablefvm
///                                              PL1       PL2        PL1      PL2       PL1          PL2          PL1        PL2        PL4         PL4
  {EnumAdlS35Watt881fCpuId,           3500,      3500,    10600,       0,     10600,      0,         10600,         0,       10600,     17700,       0}, ///  35W  881 ADL-S
  {EnumAdlS65Watt881fCpuId,           6500,      6500,    20200,       0,     20200,      0,         20200,         0,       20200,     31100,       0}, ///  65W  881 ADL-S
  {EnumAdlS125Watt881fCpuId,         12500,     24100,    24100,       0,     24100,      0,         24100,         0,       24100,     35900,       0}, ///  125W 881 ADL-S
  {EnumAdlS150Watt881fCpuId,         15000,     24100,    24100,       0,     24100,      0,         24100,         0,       24100,     35900,       0}, ///  150W 881 ADL-S
  {EnumAdlS35Watt841fCpuId,           3500,      3500,     9900,       0,      9900,      0,          9900,         0,        9900,     14000,       0}, ///  35W  841 ADL-S
  {EnumAdlS65Watt841fCpuId,           6500,      6500,    18000,       0,     18000,      0,         18000,         0,       18000,     26000,       0}, ///  65W  841 ADL-S
  {EnumAdlS125Watt841fCpuId,         12500,     19000,    19000,       0,     19000,      0,         19000,         0,       19000,     28000,       0}, ///  125W 841 ADL-S
  {EnumAdlS35Watt641fCpuId,           3500,      3500,    10600,       0,     10600,      0,         10600,         0,       10600,     17700,       0}, ///  35W  641 ADL-S
  {EnumAdlS65Watt641fCpuId,           6500,      6500,    16900,       0,     16900,      0,         16900,         0,       16900,     24900,       0}, ///  65W  641 ADL-S
  {EnumAdlS125Watt641fCpuId,         12500,     15000,    15000,       0,     15000,      0,         15000,         0,       15000,     23000,       0}, ///  125W 641 ADL-S
  {EnumAdlS35Watt681fCpuId,           3500,      3500,    10600,       0,     10600,      0,         10600,         0,       10600,     17700,       0}, ///  35W  681 ADL-S
  {EnumAdlS35Watt801fCpuId,           3500,      3500,    10600,       0,     10600,      0,         10600,         0,       10600,     17900,       0}, ///  35W  801 ADL-S
  {EnumAdlS65Watt801fCpuId,           6500,      6500,    20200,       0,     20200,      0,         20200,         0,       20200,     31100,       0}, ///  65W  801 ADL-S
  {EnumAdlS35Watt601fCpuId,           3500,      3500,     7400,       0,      7400,      0,          7400,         0,        7400,     11200,       0}, ///  35W  601 ADL-S
  {EnumAdlS65Watt601fCpuId,           6500,      6500,    11700,       0,     11700,      0,         11700,         0,       11700,     17600,       0}, ///  65W  601 ADL-S
  {EnumAdlS35Watt401fCpuId,           3500,      3500,     6900,       0,      6900,      0,          6900,         0,        6900,      9800,       0}, ///  35W  401 ADL-S
  {EnumAdlS65Watt401fCpuId,           6500,      6500,     8900,       0,      8900,      0,          8900,         0,        8900,     12500,       0}, ///  65W  401 ADL-S
  {EnumAdlS35Watt201fCpuId,           3500,      3500,     3500,       0,      3500,      0,          3500,         0,        3500,      4400,       0}, ///  35W  201 ADL-S
  {EnumAdlS46Watt201fCpuId,           4600,      4600,     4600,       0,      4600,      0,          4600,         0,        4600,      5700,       0}, ///  46W  201 ADL-S
  {EnumAdlS65Watt201fCpuId,           6500,      6500,     3500,       0,      3500,      0,          3500,         0,        3500,      4400,       0}, ///  65W  201 ADL-S
  {EnumAdlS58Watt401fCpuId,           5800,      5800,     8900,       0,      8900,      0,          8900,         0,        8900,     12500,       0}, ///  58W  401 ADL-S
  {EnumAdlS60Watt401fCpuId,           6000,      6000,     8900,       0,      8900,      0,          8900,         0,        8900,     12500,       0}, ///  60W  401 ADL-S
  {EnumAdlS63Watt401fCpuId,           6300,      6300,     8900,       0,      8900,      0,          8900,         0,        8900,     12500,       0}, ///  63W  401 ADL-S
  {EnumAdlS65WattBga881fCpuId,        6500,      6500,    20200,       0,     20200,      0,         20200,         0,       20200,     31100,       0}, ///  65W  BGA 881 ADL-S
  {EnumAdlS65WattBga841fCpuId,        6500,      6500,    20200,       0,     20200,      0,         20200,         0,       20200,     31100,       0}, ///  65W  BGA 841 ADL-S
  {EnumAdlS65WattBga441fCpuId,        6500,      6500,    16000,       0,     16000,      0,         16000,         0,       16000,     14000,       0}, ///  65W  BGA 441 ADL-S
  {EnumAdlS65WattBga601fCpuId,        6500,      6500,    18000,       0,     18000,      0,         18000,         0,       18000,     28000,       0}, ///  65W  BGA 601 ADL-S
  {EnumAdlS65WattBga681fCpuId,        6500,      6500,    18000,       0,     18000,      0,         18000,         0,       18000,     28000,       0}, ///  65W  BGA 681 ADL-S
  {EnumAdlS65WattBga401fCpuId,        6500,      6500,    16000,       0,     16000,      0,         16000,         0,       16000,     14000,       0}, ///  65W  BGA 401 ADL-S
  {EnumAdlS55WattHBga881fCpuId,       5500,      5500,    15700,       0,     15700,      0,         15700,         0,       15700,     24200,       0}, ///  55W  BGA H55 881 ADL-S
  {EnumAdlS55WattHBga681fCpuId,       5500,      5500,    15700,       0,     15700,      0,         15700,         0,       15700,     24200,       0}, ///  55W  BGA H55 681 ADL-S
  {EnumAdlS55WattHBga481fCpuId,       5500,      5500,    11400,       0,     11400,      0,         11400,         0,       11400,     18200,       0}, ///  55W  BGA H55 481 ADL-S
  {EnumAdlS55WattHBga441fCpuId,       5500,      5500,     9300,       0,      9300,      0,          9300,         0,        9300,     12500,       0}, ///  55W  BGA H55 441 ADL-S
  {EnumAdlP15Watt282fCpuId,           1500,      1500,     5500,       0,      5500,      0,          5500,         0,        5500,     12300,       0}, ///  15W  282 ADL-P
  {EnumAdlP15Watt142fCpuId,           1500,      1500,     5500,       0,      5500,      0,          5500,         0,        5500,     12300,       0}, ///  15W  142 ADL-P
  {EnumAdlP15Watt242fCpuId,           1500,      1500,     5500,       0,      5500,      0,          5500,         0,        5500,     12300,       0}, ///  15W  242 ADL-P
  {EnumAdlP28Watt282fCpuId,           2800,      2800,     6400,       0,      6400,      0,          6400,         0,        6400,      9000,       0}, ///  28W  282 ADL-P
  {EnumAdlP28Watt482fCpuId,           2800,      2800,     6400,       0,      6400,      0,          6400,         0,        6400,      9000,       0}, ///  28W  482 ADL-P
  {EnumAdlP28Watt682fCpuId,           2800,      2800,     6400,       0,      6400,      0,          6400,         0,        6400,     14000,       0}, ///  26W  682 ADL-P
  {EnumAdlP28Watt142fCpuId,           2800,      2800,     6400,       0,      6400,      0,          6400,         0,        6400,      9000,       0}, ///  28W  142 ADL-P
  {EnumAdlP28Watt242fCpuId,           2800,      2800,     6400,       0,      6400,      0,          6400,         0,        6400,      9000,       0}, ///  28W  242 ADL-P
  {EnumAdlP28Watt442fCpuId,           2800,      2800,     6400,       0,      6400,      0,          6400,         0,        6400,      9000,       0}, ///  28W  442 ADL-P
  {EnumAdlP28Watt182fCpuId,           2800,      2800,     6400,       0,      6400,      0,          6400,         0,        6400,      9000,       0}, ///  28W  182 ADL-P
  {EnumAdlP28Watt642fCpuId,           2800,      2800,     6400,       0,      6400,      0,          6400,         0,        6400,     14000,       0}, ///  26W  682 ADL-P
  {EnumAdlP28Watt662fCpuId,           2800,      2800,     6400,       0,      6400,      0,          6400,         0,        6400,     14000,       0}, ///  26W  682 ADL-P
  {EnumAdlP45Watt682fCpuId,           4500,      4500,    11500,       0,     11500,      0,         11500,         0,       11500,     21500,       0}, ///  45W  682 ADL-P
  {EnumAdlP45Watt242fCpuId,           4500,      4500,     9500,       0,      9500,      0,          9500,         0,        9500,     12500,       0}, ///  45W  242 ADL-P
  {EnumAdlP45Watt482fCpuId,           4500,      4500,     9500,       0,      9500,      0,          9500,         0,        9500,     12500,       0}, ///  45W  482 ADL-P
  {EnumAdlP45Watt442fCpuId,           4500,      4500,     9500,       0,      9500,      0,          9500,         0,        9500,     12500,       0}, ///  45W  442 ADL-P
  {EnumAdlP45Watt642fCpuId,           4500,      4500,    11500,       0,     11500,      0,         11500,         0,       11500,     21500,       0}, ///  45W  642 ADL-P
  {EnumAdlM7Watt182fPmicCpuId,         700,       700,     2400,       0,      2400,      0,          2400,         0,        2400,      5800,       0}, ///  7W   182 ADL-M PMIC
  {EnumAdlM7Watt142fPmicCpuId,         700,       700,     2400,       0,      2400,      0,          2400,         0,        2400,      5800,       0}, ///  7W   142 ADL-M PMIC
  {EnumAdlM5Watt142fPmicCpuId,         500,       500,     2400,       0,      2400,      0,          2400,         0,        2400,      5800,       0}, ///  5W   142 ADL-M PMIC
  {EnumAdlM5Watt182fPmicCpuId,         500,       500,     2400,       0,      2400,      0,          2400,         0,        2400,      5800,       0}, ///  5W   182 ADL-M PMIC
  {EnumAdlM7Watt242fHybDiscreteCpuId,  700,       700,     2900,       0,      2900,      0,          2900,         0,        2900,      6800,       0}, ///  7W   242 ADL-M Hybrid/Discrete
  {EnumAdlM7Watt282fHybDiscreteCpuId,  700,       700,     2900,       0,      2900,      0,          2900,         0,        2900,      6800,       0}, ///  7W   282 ADL-M Hybrid/Discrete
  {EnumAdlM7Watt182fHybDiscreteCpuId,  700,       700,     2900,       0,      2900,      0,          2900,         0,        2900,      6800,       0}, ///  7W   182 ADL-M Hybrid/Discrete
  {EnumAdlM9Watt282fHybDiscreteCpuId,  900,       900,     2900,       0,      2900,      0,          2900,         0,        2900,      6800,       0}, ///  9W   282 ADL-M Hybrid/Discrete
  {EnumAdlM9Watt242fHybDiscreteCpuId,  900,       900,     2900,       0,      2900,      0,          2900,         0,        2900,      6800,       0}, ///  9W   242 ADL-M Hybrid/Discrete
  {EnumAdlM9Watt142fHybDiscreteCpuId,  900,       900,     2900,       0,      2900,      0,          2900,         0,        2900,      6800,       0}, ///  9W   142 ADL-M Hybrid/Discrete
  {EnumAdlM12Watt142fHybDiscreteCpuId,1200,      1200,     2900,       0,      2900,      0,          2900,         0,        2900,      6800,       0}, /// 12W   142 ADL-M Hybrid/Discrete
  {EnumAdlM12Watt242fHybDiscreteCpuId,1200,      1200,     2900,       0,      2900,      0,          2900,         0,        2900,      6800,       0}, /// 12W   242 ADL-M Hybrid/Discrete
  {EnumAdlM12Watt182fHybDiscreteCpuId,1200,      1200,     2900,       0,      2900,      0,          2900,         0,        2900,      6800,       0}, /// 12W   182 ADL-M Hybrid/Discrete
  {EnumAdlM12Watt282fHybDiscreteCpuId,1200,      1200,     2900,       0,      2900,      0,          2900,         0,        2900,      6800,       0}, /// 12W   282 ADL-M Hybrid/Discrete
};

GLOBAL_REMOVE_IF_UNREFERENCED CONST  CPU_CORE_COUNT  mCpuCoreCountMappingTable[] = {
  { EnumAdlS35Watt881fCpuId,            8,  8},
  { EnumAdlS65Watt881fCpuId,            8,  8},
  { EnumAdlS125Watt881fCpuId,           8,  8},
  { EnumAdlS150Watt881fCpuId,           8,  8},
  { EnumAdlS35Watt841fCpuId,            8,  4},
  { EnumAdlS65Watt841fCpuId,            8,  4},
  { EnumAdlS125Watt841fCpuId,           8,  4},
  { EnumAdlS35Watt641fCpuId,            6,  4},
  { EnumAdlS65Watt641fCpuId,            6,  4},
  { EnumAdlS125Watt641fCpuId,           6,  4},
  { EnumAdlS35Watt681fCpuId,            6,  8},
  { EnumAdlS35Watt801fCpuId,            8,  0},
  { EnumAdlS65Watt801fCpuId,            8,  0},
  { EnumAdlS35Watt601fCpuId,            6,  0},
  { EnumAdlS65Watt601fCpuId,            6,  0},
  { EnumAdlS35Watt201fCpuId,            2,  0},
  { EnumAdlS46Watt201fCpuId,            2,  0},
  { EnumAdlS65Watt201fCpuId,            2,  0},
  { EnumAdlS60Watt401fCpuId,            4,  0},
  { EnumAdlS63Watt401fCpuId,            4,  0},
  { EnumAdlS35Watt401fCpuId,            4,  0},
  { EnumAdlS65Watt401fCpuId,            4,  0},
  { EnumAdlS58Watt401fCpuId,            4,  0},
  { EnumAdlS65WattBga881fCpuId,         8,  8},
  { EnumAdlS65WattBga841fCpuId,         8,  4},
  { EnumAdlS65WattBga441fCpuId,         4,  4},
  { EnumAdlS65WattBga601fCpuId,         6,  0},
  { EnumAdlS65WattBga681fCpuId,         6,  8},
  { EnumAdlS65WattBga401fCpuId,         4,  0},
  { EnumAdlS55WattHBga881fCpuId,        8,  8},
  { EnumAdlS55WattHBga681fCpuId,        6,  8},
  { EnumAdlS55WattHBga481fCpuId,        4,  8},
  { EnumAdlS55WattHBga441fCpuId,        4,  4},
  { EnumAdlP15Watt282fCpuId,            2,  8},
  { EnumAdlP28Watt282fCpuId,            2,  8},
  { EnumAdlP28Watt482fCpuId,            4,  8},
  { EnumAdlP28Watt682fCpuId,            6,  8},
  { EnumAdlP45Watt682fCpuId,            6,  8},
  { EnumAdlP45Watt482fCpuId,            4,  8},
  { EnumAdlP45Watt442fCpuId,            4,  4},
  { EnumAdlP28Watt442fCpuId,            4,  4},
  { EnumAdlP15Watt142fCpuId,            1,  4},
  { EnumAdlP28Watt142fCpuId,            1,  4},
  { EnumAdlP15Watt242fCpuId,            2,  4},
  { EnumAdlP28Watt242fCpuId,            2,  4},
  { EnumAdlP45Watt242fCpuId,            2,  4},
  { EnumAdlP45Watt642fCpuId,            6,  4},
  { EnumAdlP28Watt182fCpuId,            1,  8},
  { EnumAdlP28Watt642fCpuId,            6,  4},
  { EnumAdlP28Watt662fCpuId,            6,  6},
  { EnumAdlM7Watt182fPmicCpuId,         1,  8},
  { EnumAdlM5Watt182fPmicCpuId,         1,  8},
  { EnumAdlM7Watt142fPmicCpuId,         1,  4},
  { EnumAdlM5Watt142fPmicCpuId,         1,  4},
  { EnumAdlM7Watt242fHybDiscreteCpuId,  2,  4},
  { EnumAdlM7Watt282fHybDiscreteCpuId,  2,  8},
  { EnumAdlM7Watt182fHybDiscreteCpuId,  1,  8},
  { EnumAdlM9Watt282fHybDiscreteCpuId,  2,  8},
  { EnumAdlM9Watt242fHybDiscreteCpuId,  2,  4},
  { EnumAdlM9Watt142fHybDiscreteCpuId,  1,  4},
  { EnumAdlM12Watt142fHybDiscreteCpuId, 1,  4},
  { EnumAdlM12Watt242fHybDiscreteCpuId, 2,  4},
  { EnumAdlM12Watt182fHybDiscreteCpuId, 1,  8},
  { EnumAdlM12Watt282fHybDiscreteCpuId, 2,  8},

};

///
/// CPUID bitfield not in SDM
///
/**
  CPUID Structured Extended Feature Flags Enumeration in EDX for CPUID leaf
  #CPUID_STRUCTURED_EXTENDED_FEATURE_FLAGS sub leaf
  #CPUID_STRUCTURED_EXTENDED_FEATURE_FLAGS_SUB_LEAF_INFO.
**/
typedef union {
  ///
  /// Individual bit fields
  ///
  struct {
    ///
    /// [Bit 1:0] Reserved.
    ///
    UINT32  Reserved1:2;
    ///
    /// [Bit 2] AVX512_4VNNIW. (Intel Xeon Phi only.)
    ///
    UINT32  AVX512_4VNNIW:1;
    ///
    /// [Bit 3] AVX512_4FMAPS. (Intel Xeon Phi only.)
    ///
    UINT32  AVX512_4FMAPS:1;
    ///
    /// [Bit 14:4] Reserved.
    ///
    UINT32  Reserved2:11;
    ///
    /// [Bit 15] Supports Hetero if 1.
    ///
    UINT32  Hetero:1;
    ///
    /// [Bit 25:16] Reserved.
    ///
    UINT32  Reserved3:10;
    ///
    /// [Bit 26] Enumerates support for indirect branch restricted speculation
    /// (IBRS) and the indirect branch pre-dictor barrier (IBPB). Processors
    /// that set this bit support the IA32_SPEC_CTRL MSR and the IA32_PRED_CMD
    /// MSR. They allow software to set IA32_SPEC_CTRL[0] (IBRS) and
    /// IA32_PRED_CMD[0] (IBPB).
    ///
    UINT32  EnumeratesSupportForIBRSAndIBPB:1;
    ///
    /// [Bit 27] Enumerates support for single thread indirect branch
    /// predictors (STIBP). Processors that set this bit support the
    /// IA32_SPEC_CTRL MSR. They allow software to set IA32_SPEC_CTRL[1]
    /// (STIBP).
    ///
    UINT32  EnumeratesSupportForSTIBP:1;
    ///
    /// [Bit 28] Enumerates support for L1D_FLUSH. Processors that set this bit
    /// support the IA32_FLUSH_CMD MSR. They allow software to set
    /// IA32_FLUSH_CMD[0] (L1D_FLUSH).
    ///
    UINT32  EnumeratesSupportForL1D_FLUSH:1;
    ///
    /// [Bit 29] Enumerates support for the IA32_ARCH_CAPABILITIES MSR.
    ///
    UINT32  EnumeratesSupportForCapability:1;
    ///
    /// [Bit 30] Reserved.
    ///
    UINT32  Reserved4:1;
    ///
    /// [Bit 31] Enumerates support for Speculative Store Bypass Disable (SSBD).
    /// Processors that set this bit sup-port the IA32_SPEC_CTRL MSR. They allow
    /// software to set IA32_SPEC_CTRL[2] (SSBD).
    ///
    UINT32  EnumeratesSupportForSSBD:1;
  } Bits;
  ///
  /// All bit fields as a 32-bit value
  ///
  UINT32  Uint32;
} CPUID_EXTENDED_FEATURE_FLAGS_EDX_HETERO;

/**
  Return CPU Sku

  @param[in]  UINT32             CpuFamilyModel
  @param[in]  UINT16             CpuDid

  @retval     UINT8              CPU Sku
**/
UINT8
GetCpuSkuInfo (
  IN UINT32 CpuFamilyModel,
  IN UINT16 CpuDid
  )
{
  UINT8              CpuType;
  BOOLEAN            SkuFound;

  SkuFound  = TRUE;
  CpuType   = EnumCpuUnknown;

  switch (CpuFamilyModel) {
    case CPUID_FULL_FAMILY_MODEL_ALDERLAKE_DT_HALO:
      switch (CpuDid) {
        case V_SA_DEVICE_ID_DT_1:        // AlderLake Desktop (8+8+GT) SA DID
        case V_SA_DEVICE_ID_DT_2:        // AlderLake Desktop (8+6(f)+GT) SA DID
        case V_SA_DEVICE_ID_DT_3:        // AlderLake Desktop (8+4(f)+GT) SA DID
        case V_SA_DEVICE_ID_DT_4:        // AlderLake Desktop (8+2(f)+GT) SA DID
        case V_SA_DEVICE_ID_DT_5:        // AlderLake Desktop (8+0(f)+GT) SA DID
        case V_SA_DEVICE_ID_DT_6:        // AlderLake Desktop (6(f)+8+GT) SA DID
        case V_SA_DEVICE_ID_DT_7:        // AlderLake Desktop (6(f)+6(f)+GT) SA DID
        case V_SA_DEVICE_ID_DT_8:        // AlderLake Desktop (6(f)+4(f)+GT) SA DID
        case V_SA_DEVICE_ID_DT_9:        // AlderLake Desktop (6(f)+2(f)+GT) SA DID
        case V_SA_DEVICE_ID_DT_10:       // AlderLake Desktop (6+0+GT) SA DID
        case V_SA_DEVICE_ID_DT_11:       // AlderLake Desktop (4(f)+0+GT) SA DID
        case V_SA_DEVICE_ID_DT_12:       // AlderLake Desktop (2(f)+0+GT) SA DID
        case V_SA_DEVICE_ID_DT_13:       // AlderLake Desktop (8+6(f)+GT) SA DID
        case V_SA_DEVICE_ID_DT_15:       // AlderLake Desktop BGA (8+8(f)+GT) SA DID
        case V_SA_DEVICE_ID_DT_16:       // AlderLake Desktop BGA (8+4(f)+GT) SA DID
        case V_SA_DEVICE_ID_DT_17:       // AlderLake Desktop BGA (6+0(f)+GT) SA DID
        case V_SA_DEVICE_ID_DT_18:       // AlderLake Desktop BGA (4+0(f)+GT) SA DID
        case V_SA_DEVICE_ID_DT_19:       // AlderLake Mobile S BGA (8+8(f)+GT) SA DID
        case V_SA_DEVICE_ID_DT_20:       // AlderLake Mobile S BGA (6+8(f)+GT) SA DID
        case V_SA_DEVICE_ID_DT_21:       // AlderLake Mobile S BGA (4+8(f)+GT) SA DID
        case V_SA_DEVICE_ID_DT_22:       // AlderLake Mobile S BGA (4+4(f)+GT) SA DID
          CpuType = EnumCpuTrad;
          break;
        default:
          SkuFound = FALSE;
          break;
      }
    break;
    case CPUID_FULL_FAMILY_MODEL_ALDERLAKE_MOBILE:
      switch (CpuDid) {
        case V_SA_DEVICE_ID_MB_ULT_1:    // AlderLake P (6+8+GT)
        case V_SA_DEVICE_ID_MB_ULT_2:    // AlderLake P (6+4(f)+GT)
        case V_SA_DEVICE_ID_MB_ULT_3:    // AlderLake P (4(f)+8+GT)
        case V_SA_DEVICE_ID_MB_ULT_4:    // AlderLake P (2(f)+4(f)+GT)
        case V_SA_DEVICE_ID_MB_ULT_5:    // AlderLake P (2+8+GT)
        case V_SA_DEVICE_ID_MB_ULT_6:    // AlderLake P (2+4(f)+GT)
        case V_SA_DEVICE_ID_MB_ULT_7:    // AlderLake P (4+4(f)+GT)
        case V_SA_DEVICE_ID_MB_ULT_8:    // AlderLake P (1+4+GT) SA DID
        case V_SA_DEVICE_ID_MB_ULT_9:    // AlderLake P (1+8+GT) SA DID
        case V_SA_DEVICE_ID_MB_ULT_10:   // AlderLake P (6+6+GT) SA DID
          CpuType = EnumCpuUlt;
          break;
        case V_SA_DEVICE_ID_MB_ULX_1:    // AlderLake M (2+8+GT) SA DID
        case V_SA_DEVICE_ID_MB_ULX_2:    // AlderLake M (2+4(f)+GT) SA DID
        case V_SA_DEVICE_ID_MB_ULX_3:    // AlderLake M (1+4(f)+GT) SA DID
        case V_SA_DEVICE_ID_MB_ULX_4:    // AlderLake M (1+8(f)+GT) SA DID
        case V_SA_DEVICE_ID_MB_ULX_5:    // AlderLake N (0+8+1) SA DID
        case V_SA_DEVICE_ID_MB_ULX_6:    // AlderLake N Celeron (0+2+0) SA DID
        case V_SA_DEVICE_ID_MB_ULX_7:    // AlderLake N (0+4+0)(f) SA DID
        case V_SA_DEVICE_ID_MB_ULX_8:    // AlderLake N Pentium (0+4+0) SA DID
        case V_SA_DEVICE_ID_MB_ULX_9:    // AlderLake N Celeron (0+4+0) SA DID
          CpuType = EnumCpuUlx;
          break;
        default:
          SkuFound = FALSE;
          break;
      }
    break;

    case CPUID_FULL_FAMILY_MODEL_ALDERLAKE_ATOM:
      switch (CpuDid) {
        case V_SA_DEVICE_ID_MB_ULX_5:   // AlderLake N (0+8+1)
        case V_SA_DEVICE_ID_MB_ULX_6:   // AlderLake N Celeron (0+2+0) SA DID
        case V_SA_DEVICE_ID_MB_ULX_7:   // AlderLake N (0+4+0)(f) SA DID
        case V_SA_DEVICE_ID_MB_ULX_8:   // AlderLake N Pentium (0+4+0) SA DID
        case V_SA_DEVICE_ID_MB_ULX_9:   // AlderLake N Celeron (0+4+0) SA DID
          CpuType = EnumCpuUlx;
          break;
        default:
          SkuFound = FALSE;
          break;
      }
    break;

  }

  if (!SkuFound) {
    DEBUG ((DEBUG_ERROR, "Unsupported CPU SKU, Device ID: 0x%02X, CPUID: 0x%08X!\n", CpuDid, CpuFamilyModel));
    ASSERT (FALSE);
  }

  return CpuType;
}

/**
  Get processor generation

  @param[in]  CPU_FAMILY         CpuFamilyModel

  @retval     CPU_GENERATION     Returns the executing thread's processor generation.
**/
CPU_GENERATION
GetCpuSkuGeneration (
  CPU_FAMILY         CpuFamilyModel
  )
{
  CPU_GENERATION     CpuGeneration;

  switch (CpuFamilyModel) {
    case CPUID_FULL_FAMILY_MODEL_ALDERLAKE_DT_HALO:
    case CPUID_FULL_FAMILY_MODEL_ALDERLAKE_MOBILE:
    case CPUID_FULL_FAMILY_MODEL_ALDERLAKE_ATOM:
      CpuGeneration = EnumAdlCpu;
      break;

    default:
      CpuGeneration = EnumCpuUnknownGeneration;
      ASSERT (FALSE);
      break;
  }

  return CpuGeneration;
}

/**
  This function returns the supported cores count info.

  @param[out] *NumberOfSupportedCores              - variable that will store Maximum supported cores.
  @param[out] *NumberOfSupportedAtomCores          - variable that will store Maximum supported Atom cores.

  @retval     EFI_SUCCESS     Get the supported core count info successfully.
  @retval     EFI_UNSUPPORTED Doesn't support to get the supported core count info.
**/
EFI_STATUS
GetCpuSupportedCoreCountInfo (
  OUT UINT8                     *NumberOfSupportedCores,            OPTIONAL
  OUT UINT8                     *NumberOfSupportedAtomCores         OPTIONAL
  )
{
  UINTN                 Index;
  UINTN                 Count;
  UINT32                CpuIdentifier;

  CpuIdentifier = GetCpuIdentifier ();

  Count = ARRAY_SIZE (mCpuCoreCountMappingTable);

  for (Index = 0; Index < Count; Index++) {
    if (CpuIdentifier == (UINT32) mCpuCoreCountMappingTable[Index].CpuIdentifier) {
      if (NumberOfSupportedCores != NULL) {
        *NumberOfSupportedCores = mCpuCoreCountMappingTable[Index].SupportedCores;
      }

      if (NumberOfSupportedAtomCores != NULL) {
        *NumberOfSupportedAtomCores = mCpuCoreCountMappingTable[Index].SupportedAtomCores;
      }

      return EFI_SUCCESS;
    }
  }

  DEBUG ((DEBUG_ERROR, "GetCpuSupportedCoreCountInfo: Unsupported CPU Identifier(%X) in Core Count Mapping Table!\n", CpuIdentifier));

  if (IsSimicsEnvironment ()) {
    DEBUG ((DEBUG_WARN, "GetCpuSupportedCoreCountInfo: Simics environment is detected, mapping {Core, Atom} to {1, 1}!\n"));

    *NumberOfSupportedCores     = 1;
    *NumberOfSupportedAtomCores = 1;

    return EFI_SUCCESS;
  }
  ASSERT (FALSE);
  return EFI_UNSUPPORTED;
}

/**
  This function returns the Soc-North supported cores count info.

  @param[out] *NumberOfSupportedSocNorthAtomCores              - variable that will store Maximum supported cores.

  @retval     EFI_SUCCESS     Get the supported core count info successfully.
  @retval     EFI_UNSUPPORTED Doesn't support to get the supported core count info.
**/
EFI_STATUS
GetSocNorthSupportedAtomCoresFru (
  OUT UINT8                     *NumberOfSupportedSocNorthAtomCores
  )
{
  *NumberOfSupportedSocNorthAtomCores = 0;
  return EFI_UNSUPPORTED;
}

/**
  This function returns the Soc-North active cores count info.

  @retval     get the active core count info.
**/
UINT8
GetSocNorthActiveAtomCoresFru (
  VOID
  )
{
  return 0;
}

/**
  Returns Generation string of the respective CPU

  @param[in]   CpuFamilyId

  @retval      Character pointer of Generation string
**/
CONST CHAR8*
GetFruGenerationString (
  IN   UINT32   CpuFamilyId
  )
{
  switch (CpuFamilyId) {
    case CPUID_FULL_FAMILY_MODEL_ALDERLAKE_DT_HALO:
    case CPUID_FULL_FAMILY_MODEL_ALDERLAKE_MOBILE:
    case CPUID_FULL_FAMILY_MODEL_ALDERLAKE_ATOM:
      return mAdlCpuFamilyString;
    default:
      return NULL;
  }
}

/**
  Returns Revision Table string

  @param[in]   CpuId

  @retval      Character pointer of Revision Table string
**/
CONST CHAR8*
GetRevisionTableString (
  UINT32                CpuId
  )
{
  UINTN                 Index = 0;
  UINTN                 Count;
  CPU_SKU               CpuSku;

  CpuSku = GetCpuSku ();
  Count = ARRAY_SIZE (mProcessorRevisionTable);

  for (Index = 0; Index < Count; Index++) {
    if ((CpuId == mProcessorRevisionTable[Index].CPUID) && CpuSku == (mProcessorRevisionTable[Index].CpuSku)) {
      return mProcessorRevisionTable[Index].String;
    }
  }
  return NULL;
}

/**
  This function returns if CPU support SyncFeatures

  @retval TRUE             SyncFeature is supported
  @retval FALSE            SyncFeature is not Supported
**/
BOOLEAN
IsSmmSyncFeatureSupported (
  VOID
  )
{
  return FALSE;
}

/**
  This function returns Number of CBO0 Bank Index.

  @retval Number of CBO0 Bank Index.
**/
UINT8
GetCbo0BankIndex (
  VOID
  )
{
  return 8;
}

/**
  Detect if Hetero Core is supported.

  @retval TRUE - Processor support HeteroCore
  @retval FALSE - Processor doesnt support HeteroCore
**/
BOOLEAN
IsHeteroCoreSupported (
  VOID
  )
{
  CPUID_EXTENDED_FEATURE_FLAGS_EDX_HETERO Edx;

  ///
  /// Check Hetero feature is supported
  /// with CPUID.(EAX=7,ECX=0):EDX[15]=1
  ///
  AsmCpuidEx (CPUID_STRUCTURED_EXTENDED_FEATURE_FLAGS, 0, NULL, NULL, NULL, &Edx.Uint32);
  if (Edx.Bits.Hetero == 1) {
    return TRUE;
  }
  return FALSE;
}

/**
  Detect the type of core, whether it is Big/Small Core.

  @param[out]    CoreType      Output pointer that get CPUID_NATIVE_MODEL_ID_INFO data
                 10h - Quark
                 20h - Atom
                 30H - Knights
                 40H - Core
**/
VOID
EFIAPI
DetectCoreType (
  OUT  UINT8   *CoreType
  )
{
  CPUID_NATIVE_MODEL_ID_AND_CORE_TYPE_EAX     Eax;

  if (IsHeteroCoreSupported ()) {
    //
    // Check which is the running core by reading CPUID.(EAX=1AH, ECX=00H):EAX
    //
    AsmCpuid (CPUID_HYBRID_INFORMATION, &Eax.Uint32, NULL, NULL, NULL);
    *CoreType = (UINT8) Eax.Bits.CoreType;
  } else if (GetCpuFamily () == CPUID_FULL_FAMILY_MODEL_ALDERLAKE_ATOM) {
    *CoreType = CPUID_CORE_TYPE_INTEL_ATOM;
  } else {
    *CoreType = CPUID_CORE_TYPE_INTEL_CORE;
  }
}

/**
  Returns power limits Table

  @param[in]      NoOfOverrides

  @retval         override table pointer of power limits Table
**/
PPM_OVERRIDE_TABLE*
GetFruPowerLimits (
  IN UINTN                   *NoOfOverrides
  )
{
  *NoOfOverrides = ARRAY_SIZE (mPowerLimitsOverrideTable);
  return mPowerLimitsOverrideTable;
}

/**
  Return CPU Identifier used to identify various CPU types

  @param[in]  CPU_SKU                       CpuSku
  @param[in]  CPU_STEPPING                  CpuStepping
  @param[in]  SelectedCtdpLevel             Ctdp Level Selected in BIOS

  @retval CPU_IDENTIFIER           CPU Identifier
**/
CPU_IDENTIFIER
EFIAPI
GetCpuSkuIdentifier (
  IN  CPU_SKU         CpuSku,
  IN  CPU_STEPPING    CpuStepping,
  IN  UINT16          SelectedCtdpLevel
  )
{
  CPU_IDENTIFIER                        CpuIdentifier;
  UINT16                                PackageTdp;
  UINT16                                PackageTdpWatt;
  UINT16                                TempPackageTdp;
  UINT8                                 ProcessorPowerUnit;
  UINT16                                CtdpTempRatio1;
  UINT16                                CtdpTempRatio2;
  UINT16                                ConfigTdpWatt1;
  UINT16                                ConfigTdpWatt2;
  MSR_PACKAGE_POWER_SKU_REGISTER        PackagePowerSkuMsr;
  MSR_PACKAGE_POWER_SKU_UNIT_REGISTER   PackagePowerSkuUnitMsr;
  MSR_CONFIG_TDP_LEVEL1_REGISTER        CtdpMsr1;
  MSR_CONFIG_TDP_LEVEL2_REGISTER        CtdpMsr2;
  CPU_FAMILY                            CpuFamily;
  UINT16                                CpuDid;
  UINT16                                GtDid;

  CpuIdentifier = EnumUnknownCpuId;
  CpuDid = PciSegmentRead16 (PCI_SEGMENT_LIB_ADDRESS (SA_SEG_NUM, SA_MC_BUS, SA_MC_DEV, SA_MC_FUN, R_SA_MC_DEVICE_ID));
  GtDid = PciSegmentRead16 (PCI_SEGMENT_LIB_ADDRESS (SA_SEG_NUM, IGD_BUS_NUM, IGD_DEV_NUM, IGD_FUN_NUM, PCI_DEVICE_ID_OFFSET));
  CpuFamily = GetCpuFamily ();

  ///
  /// Find Package TDP value in 1/100 Watt units
  ///
  PackagePowerSkuMsr.Uint64     = AsmReadMsr64 (MSR_PACKAGE_POWER_SKU);
  PackagePowerSkuUnitMsr.Uint64 = AsmReadMsr64 (MSR_PACKAGE_POWER_SKU_UNIT);
  CtdpMsr1.Uint64               = AsmReadMsr64 (MSR_CONFIG_TDP_LEVEL1);
  CtdpMsr2.Uint64               = AsmReadMsr64 (MSR_CONFIG_TDP_LEVEL2);

  ProcessorPowerUnit           = (UINT8) (PackagePowerSkuUnitMsr.Bits.PwrUnit);
  if (ProcessorPowerUnit == 0) {
    ProcessorPowerUnit = 1;
  } else {
    ProcessorPowerUnit = (UINT8) LShiftU64 (2, (ProcessorPowerUnit - 1));
    if (IsSimicsEnvironment () && ProcessorPowerUnit == 0) {
      ProcessorPowerUnit = 1;
    }
  }
  TempPackageTdp = (UINT16) PackagePowerSkuMsr.Bits.PkgTdp;
  PackageTdpWatt = (UINT16) DivU64x32 (TempPackageTdp, ProcessorPowerUnit);

  PackageTdp = (PackageTdpWatt * 100);
  if ((TempPackageTdp % ProcessorPowerUnit) !=0) {
    PackageTdp += ((TempPackageTdp % ProcessorPowerUnit) * 100) / ProcessorPowerUnit;
  }

  DEBUG ((DEBUG_INFO, "GetCpuIdentifier () - CpuSku      = 0x%X\n", CpuSku));
  DEBUG ((DEBUG_INFO, "                    - PackageTdp  = %d\n", PackageTdp));
  DEBUG ((DEBUG_INFO, "                    - CpuStepping = %d\n", CpuStepping));
  DEBUG ((DEBUG_INFO, "                    - CpuDID      = 0x%X\n", CpuDid));
  DEBUG ((DEBUG_INFO, "                    - GtDid      = 0x%X\n", GtDid));

  if (SelectedCtdpLevel != CTDP_NOMINAL_LEVEL) {
    CtdpMsr1.Uint64               = AsmReadMsr64 (MSR_CONFIG_TDP_LEVEL1);
    CtdpMsr2.Uint64               = AsmReadMsr64 (MSR_CONFIG_TDP_LEVEL2);
    CtdpTempRatio1 = (UINT16) (CtdpMsr1.Bits.PkgTdp);
    ConfigTdpWatt1 = (UINT16) DivU64x32 (CtdpTempRatio1, ProcessorPowerUnit);
    ConfigTdpWatt1 = (ConfigTdpWatt1 * 100);
    CtdpTempRatio2 = (UINT16) (CtdpMsr2.Bits.PkgTdp);
    ConfigTdpWatt2 = (UINT16) DivU64x32 (CtdpTempRatio2, ProcessorPowerUnit);
    ConfigTdpWatt2 = (ConfigTdpWatt2 * 100);
    DEBUG ((DEBUG_INFO, "                 - ConfigTdpWatt_level1 = %d\n", ConfigTdpWatt1));
    DEBUG ((DEBUG_INFO, "                 - ConfigTdpWatt_level2 = %d\n", ConfigTdpWatt2));
  }

  ///
  /// Logic to determine the CPU Identifier
  ///
  switch(CpuSku) {
    case EnumCpuUlx:
      if (CpuFamily == CPUID_FULL_FAMILY_MODEL_ALDERLAKE_MOBILE) {
        if (CpuDid == V_SA_DEVICE_ID_MB_ULX_3) {
          if (PackageTdp == CPU_TDP_5_WATTS) {
            ///
            ///  ADL-M 1+4+2 5W
            ///
            DEBUG ((DEBUG_INFO, "CPU Identifier = ADL-M 1+4+2 5W\n"));
            CpuIdentifier = EnumAdlM5Watt142fPmicCpuId;
          } else if (PackageTdp == CPU_TDP_7_WATTS) {
            ///
            ///  ADL-M 1+4+2 7W
            ///
            DEBUG ((DEBUG_INFO, "CPU Identifier = ADL-M 1+4+2 7W\n"));
            CpuIdentifier = EnumAdlM7Watt142fPmicCpuId;
          } else if (PackageTdp == CPU_TDP_9_WATTS) {
            ///
            ///  ADL-M 1+4+2 9W
            ///
            DEBUG ((DEBUG_INFO, "CPU Identifier = ADL-M 1+4+2 9W\n"));
            CpuIdentifier = EnumAdlM9Watt142fHybDiscreteCpuId;
          }
          else if (PackageTdp == CPU_TDP_12_WATTS) {
            ///
            ///  ADL-M 1+4+2 12W
            ///
            DEBUG ((DEBUG_INFO, "CPU Identifier = ADL-M 1+4+2 12W\n"));
            CpuIdentifier = EnumAdlM12Watt142fHybDiscreteCpuId;
          }
        } else if (CpuDid == V_SA_DEVICE_ID_MB_ULX_1) {
          if (PackageTdp == CPU_TDP_9_WATTS) {
            ///
            ///  ADL-M 2+8+2 9W
            ///
            DEBUG ((DEBUG_INFO, "CPU Identifier = ADL-M 2+8+2 9W\n"));
            CpuIdentifier = EnumAdlM9Watt282fHybDiscreteCpuId;
          }
          else if (PackageTdp == CPU_TDP_12_WATTS) {
            ///
            ///  ADL-M 2+8+2 12W
            ///
            DEBUG ((DEBUG_INFO, "CPU Identifier = ADL-M 2+8+2 12W\n"));
            CpuIdentifier = EnumAdlM12Watt282fHybDiscreteCpuId;
          }
        } else if (CpuDid == V_SA_DEVICE_ID_MB_ULX_4) {
          if (PackageTdp == CPU_TDP_5_WATTS) {
            ///
            ///  ADL-M 1+8+2 5W
            ///
            DEBUG ((DEBUG_INFO, "CPU Identifier = ADL-M 1+8+2 5W\n"));
            CpuIdentifier = EnumAdlM5Watt182fPmicCpuId;
          } else if (PackageTdp == CPU_TDP_7_WATTS) {
            ///
            ///  ADL-M 1+8+2 7W
            ///
            DEBUG ((DEBUG_INFO, "CPU Identifier = ADL-M 1+8+2 7W\n"));
            CpuIdentifier = EnumAdlM7Watt182fPmicCpuId;
          }
          else if (PackageTdp == CPU_TDP_12_WATTS) {
            ///
            ///  ADL-M 1+8+2 12W
            ///
            DEBUG ((DEBUG_INFO, "CPU Identifier = ADL-M 1+8+2 12W\n"));
            CpuIdentifier = EnumAdlM12Watt182fHybDiscreteCpuId;
          }
        } else if (CpuDid == V_SA_DEVICE_ID_MB_ULX_2) {
          if (PackageTdp == CPU_TDP_7_WATTS) {
            ///
            ///  ADL-M 2+4+2 7W
            ///
            DEBUG ((DEBUG_INFO, "CPU Identifier = ADL-M 2+4+2 7W\n"));
            CpuIdentifier = EnumAdlM7Watt242fHybDiscreteCpuId;
          } else if (PackageTdp == CPU_TDP_9_WATTS) {
            ///
            ///  ADL-M 2+4+2 9W
            ///
            DEBUG ((DEBUG_INFO, "CPU Identifier = ADL-M 2+4+2 9W\n"));
            CpuIdentifier = EnumAdlM9Watt242fHybDiscreteCpuId;
          }
          else if (PackageTdp == CPU_TDP_12_WATTS) {
            ///
            ///  ADL-M 2+4+2 12W
            ///
            DEBUG ((DEBUG_INFO, "CPU Identifier = ADL-M 2+4+2 12W\n"));
            CpuIdentifier = EnumAdlM12Watt242fHybDiscreteCpuId;
          }
        }
      }
      break;

    case EnumCpuUlt:
      if (CpuFamily == CPUID_FULL_FAMILY_MODEL_ALDERLAKE_MOBILE) {
        if (CpuDid == V_SA_DEVICE_ID_MB_ULT_1) {
          if (PackageTdp == CPU_TDP_45_WATTS) {
            ///
            ///  ADL-P 6+8+2 45W
            ///
            DEBUG ((DEBUG_INFO, "CPU Identifier = ADL-P 6+8+2 45W\n"));
            CpuIdentifier = EnumAdlP45Watt682fCpuId;
          } else if (PackageTdp == CPU_TDP_28_WATTS) {
            ///
            ///  ADL-P 6+8+2 28W
            ///
            DEBUG ((DEBUG_INFO, "CPU Identifier = ADL-P 6+8+2 28W\n"));
            CpuIdentifier = EnumAdlP28Watt682fCpuId;
          }
        } else if (CpuDid == V_SA_DEVICE_ID_MB_ULT_5) {
          if (PackageTdp == CPU_TDP_15_WATTS) {
            ///
            ///  ADL-P 2+8+2 15W
            ///
            DEBUG ((DEBUG_INFO, "CPU Identifier = ADL-P 2+8+2 15W\n"));
            CpuIdentifier = EnumAdlP15Watt282fCpuId;
          } else if (PackageTdp == CPU_TDP_28_WATTS) {
            ///
            ///  ADL-P 2+8+2 28W
            ///
            DEBUG ((DEBUG_INFO, "CPU Identifier = ADL-P 2+8+2 28W\n"));
            CpuIdentifier = EnumAdlP28Watt282fCpuId;
          }
        } else if (CpuDid == V_SA_DEVICE_ID_MB_ULT_3) {
          if (PackageTdp == CPU_TDP_28_WATTS) {
            ///
            ///  ADL-P 4+8+2 28W
            ///
            DEBUG ((DEBUG_INFO, "CPU Identifier = ADL-P 4+8+2 28W\n"));
            CpuIdentifier = EnumAdlP28Watt482fCpuId;
          } else if (PackageTdp == CPU_TDP_45_WATTS) {
            ///
            ///  ADL-P 4+8+2 45W
            ///
            DEBUG ((DEBUG_INFO, "CPU Identifier = ADL-P 4+8+2 45W\n"));
            CpuIdentifier = EnumAdlP45Watt482fCpuId;
          }
        } else if (CpuDid == V_SA_DEVICE_ID_MB_ULT_7) {
          if (PackageTdp == CPU_TDP_45_WATTS) {
            ///
            ///  ADL-P 4+4+2 45W
            ///
            DEBUG ((DEBUG_INFO, "CPU Identifier = ADL-P 4+4+2 45W\n"));
            CpuIdentifier = EnumAdlP45Watt442fCpuId;
          } else if (PackageTdp == CPU_TDP_28_WATTS) {
            ///
            ///  ADL-P 4+4+2 28W
            ///
            DEBUG ((DEBUG_INFO, "CPU Identifier = ADL-P 4+4+2 28W\n"));
            CpuIdentifier = EnumAdlP28Watt442fCpuId;
          }
        } else if (CpuDid == V_SA_DEVICE_ID_MB_ULT_8) {
          if (PackageTdp == CPU_TDP_15_WATTS) {
            ///
            ///  ADL-P 1+4+2 15W
            ///
            DEBUG ((DEBUG_INFO, "CPU Identifier = ADL-P 1+4+2 15W\n"));
            CpuIdentifier = EnumAdlP15Watt142fCpuId;
          } else if (PackageTdp == CPU_TDP_28_WATTS) {
            ///
            ///  ADL-P 1+4+2 28W
            ///
            DEBUG ((DEBUG_INFO, "CPU Identifier = ADL-P 1+4+2 28W\n"));
            CpuIdentifier = EnumAdlP28Watt142fCpuId;
          }
        } else if (CpuDid == V_SA_DEVICE_ID_MB_ULT_4) {
          if (PackageTdp == CPU_TDP_15_WATTS) {
            ///
            ///  ADL-P 2+4+2 15W
            ///
            DEBUG ((DEBUG_INFO, "CPU Identifier = ADL-P 2+4+2 15W\n"));
            CpuIdentifier = EnumAdlP15Watt242fCpuId;
          } else if (PackageTdp == CPU_TDP_28_WATTS) {
            ///
            ///  ADL-P 2+4+2 28W
            ///
            DEBUG ((DEBUG_INFO, "CPU Identifier = ADL-P 2+4+2 28W\n"));
            CpuIdentifier = EnumAdlP28Watt242fCpuId;
          } else if (PackageTdp == CPU_TDP_45_WATTS) {
            ///
            ///  ADL-P 2+4+2 45W
            ///
            DEBUG ((DEBUG_INFO, "CPU Identifier = ADL-P 2+4+2 45W\n"));
            CpuIdentifier = EnumAdlP45Watt242fCpuId;
          }
        } else if (CpuDid == V_SA_DEVICE_ID_MB_ULT_9) {
          if (PackageTdp == CPU_TDP_28_WATTS) {
            ///
            ///  ADL-P 1+8+2 28W
            ///
            DEBUG ((DEBUG_INFO, "CPU Identifier = ADL-P 1+8+2 28W\n"));
            CpuIdentifier = EnumAdlP28Watt182fCpuId;
          }
        } else if (CpuDid == V_SA_DEVICE_ID_MB_ULT_10) {
          if (PackageTdp == CPU_TDP_28_WATTS) {
            ///
            ///  ADL-P 6+6+2 28W
            ///
            DEBUG ((DEBUG_INFO, "CPU Identifier = ADL-P 6+6+2 28W\n"));
            CpuIdentifier = EnumAdlP28Watt662fCpuId;
          }
        } else if (CpuDid == V_SA_DEVICE_ID_MB_ULT_2) {
          if (PackageTdp == CPU_TDP_28_WATTS) {
            ///
            ///  ADL-P 6+4+2 28W
            ///
            DEBUG ((DEBUG_INFO, "CPU Identifier = ADL-P 6+4+2 28W\n"));
            CpuIdentifier = EnumAdlP28Watt642fCpuId;
          } else if (PackageTdp == CPU_TDP_45_WATTS) {
            ///
            ///  ADL-P 6+4+2 45W
            ///
            DEBUG ((DEBUG_INFO, "CPU Identifier = ADL-P 6+4+2 45W\n"));
            CpuIdentifier = EnumAdlP45Watt642fCpuId;
          }
        }
      }
      break;

    case EnumCpuHalo:
      break;

    case EnumCpuTrad:
      if (CpuFamily == CPUID_FULL_FAMILY_MODEL_ALDERLAKE_DT_HALO) {
        if (PackageTdp == CPU_TDP_35_WATTS) {
          if (CpuDid == V_SA_DEVICE_ID_DT_1) {
            ///
            ///  ADL-S 8+8+1 35W
            ///
            DEBUG ((DEBUG_INFO, "CPU Identifier = ADL-S 8+8+1 35W\n"));
            CpuIdentifier = EnumAdlS35Watt881fCpuId;
          } else if (CpuDid == V_SA_DEVICE_ID_DT_3) {
            ///
            ///  ADL-S 8+4+1 35W
            ///
            DEBUG ((DEBUG_INFO, "CPU Identifier = ADL-S 8+4+1 35W\n"));
            CpuIdentifier = EnumAdlS35Watt841fCpuId;
          } else if (CpuDid == V_SA_DEVICE_ID_DT_6) {
            ///
            ///  ADL-S 6+8+1 35W
            ///
            DEBUG ((DEBUG_INFO, "CPU Identifier = ADL-S 6+8+1 35W\n"));
            CpuIdentifier = EnumAdlS35Watt681fCpuId;
          } else if (CpuDid == V_SA_DEVICE_ID_DT_8) {
            ///
            ///  ADL-S 6+4+1 35W
            ///
            DEBUG ((DEBUG_INFO, "CPU Identifier = ADL-S 6+4+1 35W\n"));
            CpuIdentifier = EnumAdlS35Watt641fCpuId;
          } else if (CpuDid == V_SA_DEVICE_ID_DT_10) {
            ///
            ///  ADL-S 6+0+1 35W
            ///
            DEBUG ((DEBUG_INFO, "CPU Identifier = ADL-S 6+0+1 35W\n"));
            CpuIdentifier = EnumAdlS35Watt601fCpuId;
          } else if (CpuDid == V_SA_DEVICE_ID_DT_11) {
            ///
            ///  ADL-S 4+0+1 35W
            ///
            DEBUG ((DEBUG_INFO, "CPU Identifier = ADL-S 4+0+1 35W\n"));
            CpuIdentifier = EnumAdlS35Watt401fCpuId;
          } else if (CpuDid == V_SA_DEVICE_ID_DT_12) {
            ///
            ///  ADL-S 2+0+1 35W
            ///
            DEBUG ((DEBUG_INFO, "CPU Identifier = ADL-S 2+0+1 35W\n"));
            CpuIdentifier = EnumAdlS35Watt201fCpuId;
          } else if (CpuDid == V_SA_DEVICE_ID_DT_5) {
            ///
            ///  ADL-S 8+0+1 35W
            ///
            DEBUG ((DEBUG_INFO, "CPU Identifier = ADL-S 8+0+1 35W\n"));
            CpuIdentifier = EnumAdlS35Watt801fCpuId;
          }
        }
        else if (PackageTdp == CPU_TDP_46_WATTS) {
          if (CpuDid == V_SA_DEVICE_ID_DT_12) {
            ///
            ///  ADL-S 2+0+1 46W
            ///
            DEBUG ((DEBUG_INFO, "CPU Identifier = ADL-S 2+0+1 46W\n"));
            CpuIdentifier = EnumAdlS46Watt201fCpuId;
          }
        }
          else if (PackageTdp == CPU_TDP_55_WATTS) {
          if (CpuDid == V_SA_DEVICE_ID_DT_19) {
            ///
            ///  ADL-S H55 BGA 8+8+1 55W
            ///
            DEBUG ((DEBUG_INFO, "CPU Identifier = ADL-S Mobile BGA 8+8+1 55W\n"));
            CpuIdentifier = EnumAdlS55WattHBga881fCpuId;
          } else if (CpuDid == V_SA_DEVICE_ID_DT_20) {
            ///
            ///  ADL-S H55 BGA 6+8+1 55W
            ///
            DEBUG ((DEBUG_INFO, "CPU Identifier = ADL-S Mobile BGA 6+8+1 55W\n"));
            CpuIdentifier = EnumAdlS55WattHBga681fCpuId;
          } else if (CpuDid == V_SA_DEVICE_ID_DT_21) {
            ///
            ///  ADL-S H55 BGA 4+8+1 55W
            ///
            DEBUG ((DEBUG_INFO, "CPU Identifier = ADL-S Mobile BGA 4+8+1 55W\n"));
            CpuIdentifier = EnumAdlS55WattHBga481fCpuId;
          } else if (CpuDid == V_SA_DEVICE_ID_DT_22) {
            ///
            ///  ADL-S H55 BGA 4+4+1 55W
            ///
            DEBUG ((DEBUG_INFO, "CPU Identifier = ADL-S Mobile BGA 4+4+1 55W\n"));
            CpuIdentifier = EnumAdlS55WattHBga441fCpuId;
          }
        } else if (PackageTdp == CPU_TDP_58_WATTS) {
          if (CpuDid == V_SA_DEVICE_ID_DT_11) {
            ///
            ///  ADL-S 4+0+1 58W
            ///
            DEBUG ((DEBUG_INFO, "CPU Identifier = ADL-S 4+0+1 58W\n"));
            CpuIdentifier = EnumAdlS58Watt401fCpuId;
          }
        } else if (PackageTdp == CPU_TDP_60_WATTS) {
          if (CpuDid == V_SA_DEVICE_ID_DT_11) {
            ///
            ///  ADL-S 4+0+1 60W
            ///
            DEBUG ((DEBUG_INFO, "CPU Identifier = ADL-S 4+0+1 60W\n"));
            CpuIdentifier = EnumAdlS60Watt401fCpuId;
          }
        } else if (PackageTdp == CPU_TDP_63_WATTS) {
          if (CpuDid == V_SA_DEVICE_ID_DT_11) {
            ///
            ///  ADL-S 4+0+1 63W
            ///
            DEBUG ((DEBUG_INFO, "CPU Identifier = ADL-S 4+0+1 63W\n"));
            CpuIdentifier = EnumAdlS63Watt401fCpuId;
          }
        } else if (PackageTdp == CPU_TDP_65_WATTS) {
          if (CpuDid == V_SA_DEVICE_ID_DT_1) {
            ///
            ///  ADL-S 8+8+1 65W
            ///
            DEBUG ((DEBUG_INFO, "CPU Identifier = ADL-S 8+8+1 65W\n"));
            CpuIdentifier = EnumAdlS65Watt881fCpuId;
          } else if (CpuDid == V_SA_DEVICE_ID_DT_3) {
            ///
            ///  ADL-S 8+4+1 65W
            ///
            DEBUG ((DEBUG_INFO, "CPU Identifier = ADL-S 8+4+1 65W\n"));
            CpuIdentifier = EnumAdlS65Watt841fCpuId;
          } else if (CpuDid == V_SA_DEVICE_ID_DT_8) {
            ///
            ///  ADL-S 6+4+1 65W
            ///
            DEBUG ((DEBUG_INFO, "CPU Identifier = ADL-S 6+4+1 65W\n"));
            CpuIdentifier = EnumAdlS65Watt641fCpuId;
          }  else if (CpuDid == V_SA_DEVICE_ID_DT_12) {
            ///
            ///  ADL-S 2+0+1 65W
            ///
            DEBUG ((DEBUG_INFO, "CPU Identifier = ADL-S 2+0+1 65W\n"));
            CpuIdentifier = EnumAdlS65Watt201fCpuId;
          } else if (CpuDid == V_SA_DEVICE_ID_DT_11) {
            ///
            ///  ADL-S 4+0+1 65W
            ///
            DEBUG ((DEBUG_INFO, "CPU Identifier = ADL-S 4+0+1 65W\n"));
            CpuIdentifier = EnumAdlS65Watt401fCpuId;
          } else if (CpuDid == V_SA_DEVICE_ID_DT_10) {
            ///
            ///  ADL-S 6+0+1 65W
            ///
            DEBUG ((DEBUG_INFO, "CPU Identifier = ADL-S 6+0+1 65W\n"));
            CpuIdentifier = EnumAdlS65Watt601fCpuId;
          } else if (CpuDid == V_SA_DEVICE_ID_DT_5) {
            ///
            ///  ADL-S 8+0+1 65W
            ///
            DEBUG ((DEBUG_INFO, "CPU Identifier = ADL-S 8+0+1 65W\n"));
            CpuIdentifier = EnumAdlS65Watt801fCpuId;
          } else if (CpuDid == V_SA_DEVICE_ID_DT_15) {
            ///
            ///  ADL-S BGA 8+8+1 65W
            ///
            DEBUG ((DEBUG_INFO, "CPU Identifier = ADL-S BGA 8+8+1 65W\n"));
            CpuIdentifier = EnumAdlS65WattBga881fCpuId;
          } else if (CpuDid == V_SA_DEVICE_ID_DT_16) {
            ///
            ///  ADL-S BGA 8+4+1 65W
            ///
            DEBUG ((DEBUG_INFO, "CPU Identifier = ADL-S BGA 8+4+1 65W\n"));
            CpuIdentifier = EnumAdlS65WattBga841fCpuId;
          } else if (CpuDid == V_SA_DEVICE_ID_DT_17) {
            ///
            ///  ADL-S BGA 6+0+1 65W
            ///
            DEBUG ((DEBUG_INFO, "CPU Identifier = ADL-S BGA 6+0+1 65W\n"));
            CpuIdentifier = EnumAdlS65WattBga601fCpuId;
          } else if (CpuDid == V_SA_DEVICE_ID_DT_18) {
            ///
            ///  ADL-S BGA 4+0+1 65W
            ///
            DEBUG ((DEBUG_INFO, "CPU Identifier = ADL-S BGA 4+0+1 65W\n"));
            CpuIdentifier = EnumAdlS65WattBga401fCpuId;
          } else if (CpuDid == V_SA_DEVICE_ID_DT_22) {
            ///
            ///  ADL-S BGA 4+4+1 65W
            ///
            DEBUG ((DEBUG_INFO, "CPU Identifier = ADL-S BGA 4+4+1 65W\n"));
            CpuIdentifier = EnumAdlS65WattBga441fCpuId;
          } else if (CpuDid == V_SA_DEVICE_ID_DT_20) {
            ///
            ///  ADL-S BGA 6+8+1 65W
            ///
            DEBUG ((DEBUG_INFO, "CPU Identifier = ADL-S Mobile BGA 6+8+1 65W\n"));
            CpuIdentifier = EnumAdlS65WattBga681fCpuId;
          }
        } else if (PackageTdp == CPU_TDP_125_WATTS) {
          if (CpuDid == V_SA_DEVICE_ID_DT_1) {
            ///
            ///  ADL-S 8+8+1 125W
            ///
            DEBUG ((DEBUG_INFO, "CPU Identifier = ADL-S 8+8+1 125W\n"));
            CpuIdentifier = EnumAdlS125Watt881fCpuId;
          } else if (CpuDid == V_SA_DEVICE_ID_DT_3) {
            ///
            ///  ADL-S 8+4+1 125W
            ///
            DEBUG ((DEBUG_INFO, "CPU Identifier = ADL-S 8+4+1 125W\n"));
            CpuIdentifier = EnumAdlS125Watt841fCpuId;
          } else if (CpuDid == V_SA_DEVICE_ID_DT_8) {
            ///
            ///  ADL-S 6+4+1 125W
            ///
            DEBUG ((DEBUG_INFO, "CPU Identifier = ADL-S 6+4+1 125W\n"));
            CpuIdentifier = EnumAdlS125Watt641fCpuId;
          }
        }
        else if (PackageTdp == CPU_TDP_150_WATTS) {
          if (CpuDid == V_SA_DEVICE_ID_DT_1) {
            ///
            ///  ADL-S 8+8+1 150W
            ///
            DEBUG ((DEBUG_INFO, "CPU Identifier = ADL-S 8+8+1 150W\n"));
            CpuIdentifier = EnumAdlS150Watt881fCpuId;
          }
        }
      }

      break;

    default:
      CpuIdentifier = EnumUnknownCpuId;
      break;
    }

  return CpuIdentifier;
}

/**
  Programs Processor Upgrade for type 4 SMBIOS Processor Info HOB.

  @retval Returns Processor Upgrade value for type 4 SMBIOS Processor Info HOB.
**/
UINT8
SmbiosProcessorInfoHobType4 (
  VOID
  )
{
  return ProcessorUpgradeOther; ///< @todo: this value will be updated once DMTF assigns an official value
}

/**
  Determine if CPU supports Intel Turbo Boost Max Technology 3.0 (ITBM).

  @retval Bit is set if ITBM is supported
**/
BOOLEAN
GetItbmSupportedStatus (
  VOID
  )
{
  CPUID_THERMAL_POWER_MANAGEMENT_EAX PowerEax;

  AsmCpuidEx (
    CPUID_THERMAL_POWER_MANAGEMENT,
    0,
    &PowerEax.Uint32,
    NULL,
    NULL,
    NULL
    );
  return (BOOLEAN) (PowerEax.Bits.TurboBoostMaxTechnology30 != 0);
}

/**
  This function is used to Patch SmmSupovrStateLock.

  @retval This corresponds to bit 2 of MSR_SMM_SUPOVR_STATE_LOCK_REGISTER. When set, prevents WRMSR to IA32_SMM_MONITOR_CTL (aka MSEG) MSR.

**/
BOOLEAN
SmmSupovrStateLockPatch (
  VOID
  )
{
  return 1;
}

/**
  This function returns the supported Physical Address Size

  @retval returns the supported Physical Address Size.
**/
UINT8
GetMaxPhysicalAddressSizeFru (
  VOID
  )
{
  //
  // Even though CPUID Leaf CPUID_VIR_PHY_ADDRESS_SIZE (0x80000008) MAX_PA will report 46.
  // For ADL BIOS will return Memory expansion 39 bit (0 - 38) + MKTME (Bits 39-41 must be zero - 3 bit hole in the middle) 42-45 bit is MKTME Keys.
  //
  return 39;
}

/**
  This function is used to return 32 bit PRMRR physical base MSR

  @retval 32 bit PRMRR physical base MSR value
**/
UINT32
GetPrmrrBaseMsrAddress (
  VOID
  )
{
  return MSR_PRMRR_BASE_0;
}

/**
  This function check if CPU support VCCIO VR

  @retval TRUE    CPU support VCCIO VR
  @retval FALSE   CPU does not support VCCIO VR
**/
BOOLEAN
IsCpuVccIoVrSupported (
  VOID
  )
{
  if (TRUE == IsPchN ()) {
    return FALSE;
  }
  return TRUE;
}

/**
  This function is to check whether Avx enable/disable is supported or not.

  @retval TRUE    Avx enable/disable feature is supported.
  @retval FALSE   Avx enable/disable feature is unsupported.
**/
BOOLEAN
IsAvxSetSupported (
  VOID
  )
{
  return TRUE;
}

/**
  This function is to check whether BCLK Source Change is supported.

  @retval TRUE    BCLK Source Change is supported.
  @retval FALSE   BCLK Source Change is unsupported.
**/
BOOLEAN
IsBclkSourceChangeSupportedFru (
  VOID
  )
{
  if (EnumCpuHalo == GetCpuSku () || EnumCpuTrad == GetCpuSku ()) {
    return TRUE;
  } else {
    return FALSE;
  }
}

/**
  Detect if KeyLocker is supported.

  @retval TRUE -  Processor support KeyLocker
  @retval FALSE - Processor doesnt support KeyLocker
**/
BOOLEAN
IsKeyLockerSupported (
  VOID
  )
{
  CPUID_STRUCTURED_EXTENDED_FEATURE_FLAGS_ECX  CpuidStructuredExtendedFeatureEcx;
  //
  // Read the Extended Feature Flag for Keylocker support
  //
  AsmCpuidEx (CPUID_STRUCTURED_EXTENDED_FEATURE_FLAGS, 0, NULL, NULL, &CpuidStructuredExtendedFeatureEcx.Uint32, NULL);
  if (CpuidStructuredExtendedFeatureEcx.Uint32 & BIT23) {
    return TRUE;
  } else {
    return FALSE;
  }
}

/**
  This function is used to detect if SA VR Support

  @retval TRUE     SA VR Support
  @retval FALSE    Not Support
**/
BOOLEAN
IsSaVrSupportFru (
  VOID
  )
{
  UINT16                  CpuDid;
  ///
  /// Read the CPUID & DID information
  ///
  CpuDid = PciSegmentRead16 (PCI_SEGMENT_LIB_ADDRESS (SA_SEG_NUM, SA_MC_BUS, SA_MC_DEV, SA_MC_FUN, R_SA_MC_DEVICE_ID));
  if ((GetCpuFamily () == CPUID_FULL_FAMILY_MODEL_ALDERLAKE_MOBILE) && ((CpuDid == V_SA_DEVICE_ID_MB_ULX_1) || (CpuDid == V_SA_DEVICE_ID_MB_ULX_2) || (CpuDid == V_SA_DEVICE_ID_MB_ULX_3) || (CpuDid == V_SA_DEVICE_ID_MB_ULX_4))) {
   return TRUE;
  } else {
   return FALSE;
  }
}

/**
  This function return scaling factor of small and big cores.

  @param[out]  ScalingFactorSmallCore  Small Core Scaling factor
  @param[out]  ScalingFactorBigCore    Big Core Scaling factor
**/
VOID
GetScalingFactor (
  OUT UINT16 *ScalingFactorSmallCore,
  OUT UINT16 *ScalingFactorBigCore
  )
{
  *ScalingFactorBigCore   = 127;
  *ScalingFactorSmallCore = 100;
}

/**
  Return TRUE when the C6 DRAM is supported.

  @retval TRUE  C6 DRAM is supported.
  @retval FALSE C6 DRAM is not supported.
**/
BOOLEAN
IsC6DramSupported (
  VOID
  )
{
  return (BOOLEAN) (GetCpuFamily () == CPUID_FULL_FAMILY_MODEL_ALDERLAKE_MOBILE || GetCpuFamily () == CPUID_FULL_FAMILY_MODEL_ALDERLAKE_ATOM);
}

/**
  Return TRUE when the Nominal Frequency is supported.

  @retval TRUE  Nominal Frequency is supported.
  @retval FALSE Nominal Frequency is not supported.
**/
BOOLEAN
IsNominalFreqSupported (
  VOID
  )
{
  return TRUE;
}

/**
  This function is used to detect if the SKU is SBGA

  @retval TRUE     SBGA is Supported
  @retval FALSE    Not Supported
**/
BOOLEAN
IsSbgaSkuSupported (
  VOID
  )
{
  UINT16                  CpuDid;
  ///
  /// Read the CPUID & DID information
  ///
  CpuDid = PciSegmentRead16 (PCI_SEGMENT_LIB_ADDRESS (SA_SEG_NUM, SA_MC_BUS, SA_MC_DEV, SA_MC_FUN, R_SA_MC_DEVICE_ID));
  if ((GetCpuFamily () == CPUID_FULL_FAMILY_MODEL_ALDERLAKE_DT_HALO) & 
     ((CpuDid == V_SA_DEVICE_ID_DT_15) || (CpuDid == V_SA_DEVICE_ID_DT_16) || (CpuDid == V_SA_DEVICE_ID_DT_17) || (CpuDid == V_SA_DEVICE_ID_DT_18) || (CpuDid == V_SA_DEVICE_ID_DT_19) || (CpuDid == V_SA_DEVICE_ID_DT_20)
      || (CpuDid == V_SA_DEVICE_ID_DT_21) || (CpuDid == V_SA_DEVICE_ID_DT_22))) {
   return TRUE;
  } else {
   return FALSE;
  }
}