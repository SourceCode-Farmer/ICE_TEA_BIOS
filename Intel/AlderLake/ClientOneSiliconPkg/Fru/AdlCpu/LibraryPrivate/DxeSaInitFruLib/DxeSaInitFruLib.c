/** @file
  Source file for DXE SA Init Fru Lib

@copyright
  INTEL CONFIDENTIAL
  Copyright 2010 - 2021   Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains an 'Intel Peripheral Driver' and is uniquely identified as
  "Intel Reference Module" and is licensed for Intel CPUs and chipsets under
  the terms of your license agreement with Intel or your vendor. This file may
  be modified by the user, subject to additional terms of the license agreement.

@par Specification Reference:
**/
#include <Library/DxeSaInitFruLib.h>

/**
  This function check if Above 4GB MMIO needs to be enabled or not.

  @retval TRUE  - Above 4GB MMIO is enabled.
  @retval FALSE - Above 4GB MMIO is not enabled.
**/
BOOLEAN
CheckAbove4GbMmio (
  VOID
)
{
  HOST_BRIDGE_DATA_HOB      *HostBridgeDataHob;
  SA_CONFIG_HOB             *SaConfigHob;

  //
  // Get HostBridgeData HOB and see if above 4GB MMIO BIOS assignment enabled
  //
  HostBridgeDataHob = (HOST_BRIDGE_DATA_HOB *) GetFirstGuidHob (&gHostBridgeDataHobGuid);
  if ((HostBridgeDataHob != NULL) && (HostBridgeDataHob->EnableAbove4GBMmio == 1)) {
    return TRUE;
  }

  //
  // Enable Above 4GB MMIO when Aperture Size is 2GB or higher
  //
  SaConfigHob = (SA_CONFIG_HOB *) GetFirstGuidHob(&gSaConfigHobGuid);
  if ((SaConfigHob != NULL) && (SaConfigHob->ApertureSize >= 15)) {
    return TRUE;
  }

  return FALSE;

}

