/** @file
  This file is the implementation for PeiSaInitFru library.

@copyright
  INTEL CONFIDENTIAL
  Copyright 2020 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains an 'Intel Peripheral Driver' and is uniquely identified as
  "Intel Reference Module" and is licensed for Intel CPUs and chipsets under
  the terms of your license agreement with Intel or your vendor. This file may
  be modified by the user, subject to additional terms of the license agreement.

@par Specification Reference:
**/
#include <Library/PeiSaInitFruLib.h>
#include <Library/PcdLib.h>
#include <Register/SaRegsHostBridge.h>
#include <Library/MsrFruLib.h>
#include <CpuRegs.h>
#include <Register/IgdRegs.h>
#include <IndustryStandard/Pci22.h>

#define MCHBAR_MMIO_SCRATCHPAD0          0x5428

/**
  Programs WRC (Write Cache) Feature for IOP.

  @param[in] MiscPeiPreMemConfig  - Instance of SA_MISC_PEI_PREMEM_CONFIG
**/
VOID
ProgramWrcFeatureForIop (
  IN SA_MISC_PEI_PREMEM_CONFIG      *MiscPeiPreMemConfig
)
{
  UINT32                        WrcFeatureSetting;
  UINT64_STRUCT                 MchBar;
  UINT64                        McD0BaseAddress;

  McD0BaseAddress = PCI_SEGMENT_LIB_ADDRESS(SA_SEG_NUM, SA_MC_BUS, SA_MC_DEV, SA_MC_FUN, 0);
  MchBar.Data32.High = PciSegmentRead32(McD0BaseAddress + R_SA_MCHBAR + 4);
  MchBar.Data32.Low = PciSegmentRead32(McD0BaseAddress + R_SA_MCHBAR);
  MchBar.Data &= (UINT64)~BIT0;

  ///
  /// WRC (Write Cache) feature of IOP. Program MCHBAR + R_SA_MCHBAR_HCTL0_IMPH [20] to 1 to disable it.
  ///
  WrcFeatureSetting = BIT20;

  ///
  /// WRC_DIS is program according to setup option, with the default as feature enabled.
  ///
  WrcFeatureSetting = (UINT32)((~MiscPeiPreMemConfig->WrcFeatureEnable & BIT0) << N_SA_MCHBAR_HCTL0_IMPH_WRC_OFFSET);

  MmioAndThenOr32((UINTN)MchBar.Data + R_SA_MCHBAR_HCTL0_IMPH, (UINT32)~B_SA_MCHBAR_HCTL0_IMPH_WRC_MASK, WrcFeatureSetting);

#if FixedPcdGet8(PcdEmbeddedEnable) == 0x1
  if (MiscPeiPreMemConfig->WrcFeatureEnable == 1) {

    // WRC Enable VCrt
    WrcFeatureSetting = (UINT32)((V_SA_MCHBAR_WRC_VC_ENABLE_VCRT) << N_SA_MCHBAR_WRC_VC_ENABLE_OFFSET);

    MmioAndThenOr32((UINTN)MchBar.Data + R_SA_MCHBAR_WRC_VC_ENABLE, (UINT32)~B_SA_MCHBAR_WRC_VC_ENABLE_MASK, WrcFeatureSetting);

    // WRC Set VCrt Allocate
    WrcFeatureSetting = (UINT32)((V_SA_MCHBAR_WRC_VC_ALLOCATE_VCRT) << N_SA_MCHBAR_WRC_VC_ALLOCATE_OFFSET);

    MmioAndThenOr64((UINTN)MchBar.Data + R_SA_MCHBAR_WRC_VC_ALLOCATE, (UINT32)~B_SA_MCHBAR_WRC_VC_ALLOCATE_MASK, WrcFeatureSetting);

    // WRC Set VCrt Clos
    WrcFeatureSetting = (UINT32)((V_SA_MCHBAR_WRC_VC_CLOS_VCRT) << N_SA_MCHBAR_WRC_VC_CLOS_OFFSET);

    MmioAndThenOr64((UINTN)MchBar.Data + R_SA_MCHBAR_WRC_VC_CLOS, (UINT32)~B_SA_MCHBAR_WRC_VC_CLOS_MASK, WrcFeatureSetting);

    MsrSetWrcCos(V_SA_MCHBAR_WRC_VC_CLOS_VCRT, BIT11);
  }
#endif
}

/**
  This function performs SA internal devices enabling/disabling

  @param[in] HostBridgePeiConfig - Instance of HOST_BRIDGE_PEI_CONFIG
  @param[in] GnaConfig - Instance of GNA_CONFIG

**/
VOID
DeviceConfigure (
  IN    HOST_BRIDGE_PEI_CONFIG  *HostBridgePeiConfig,
  IN    GNA_CONFIG              *GnaConfig
  )
{
  UINT64         McD0BaseAddress;
  UINT32         DevEn;

  McD0BaseAddress = PCI_SEGMENT_LIB_ADDRESS(SA_SEG_NUM, SA_MC_BUS, 0, 0, 0);
  DevEn = PciSegmentRead32(McD0BaseAddress + R_SA_DEVEN);
  ///
  /// Enable/Disable Thermal device (B0,D4,F0).
  ///
  if (HostBridgePeiConfig->Device4Enable) {
    DevEn |= B_SA_DEVEN_D4EN_MASK;
  }
  else {
    DevEn &= ~B_SA_DEVEN_D4EN_MASK;
  }
  ///
  /// Enable/Disable GNA device (B0,D8,F0).
  ///
  if (GnaConfig->GnaEnable) {
    DevEn |= B_SA_DEVEN_D8EN_MASK;
  }
  else {
    DevEn &= ~B_SA_DEVEN_D8EN_MASK;
  }

  PciSegmentWrite32(McD0BaseAddress + R_SA_DEVEN, DevEn);
  return;
}

/**
  This function does SA security lock
**/
VOID
SaSecurityLock (
  VOID
  )
{
  EFI_STATUS                 Status;
  SI_POLICY_PPI              *SiPolicyPpi;
  UINT64_STRUCT              DmiBar;

  DEBUG((DEBUG_INFO, "SaSecurityLock Start\n"));
  PostCode(0xA50);

  DmiBar.Data32.High = PciSegmentRead32(PCI_SEGMENT_LIB_ADDRESS(SA_SEG_NUM, SA_MC_BUS, 0, 0, R_SA_DMIBAR + 4));
  DmiBar.Data32.Low = PciSegmentRead32(PCI_SEGMENT_LIB_ADDRESS(SA_SEG_NUM, SA_MC_BUS, 0, 0, R_SA_DMIBAR));
  DmiBar.Data &= (UINT64)~BIT0;

  ///
  /// System Agent Security Lock configuration
  ///

  Status = PeiServicesLocatePpi(
    &gSiPolicyPpiGuid,
    0,
    NULL,
    (VOID **)&SiPolicyPpi
    );
  if ((Status == EFI_SUCCESS) && (SiPolicyPpi != NULL)) {
    if (Status == EFI_SUCCESS) {
    }
  }

    if (!IsPchLinkDmi()) {
      ///
      /// Enable pOPIO Clockgating during POSTBOOT_SAI Transition before setting pOPIO Security Lock Bit
      ///
      MmioOr32((UINTN)(DmiBar.Data + R_SA_DMIBAR_OPIO_PHY_CONTROL), N_SA_DMIBAR_OPIO_CLOCK_GATE);
      if (MmioRead32(TXT_PUBLIC_BASE + 0x200) & BIT31) {
        ///
        /// For ULT/ULX set DMIBAR offset 0xB34 [26] to lockdown OPI debug on production systems
        ///
        MmioOr32((UINTN)(DmiBar.Data + R_SA_DMIBAR_OPIO_PHY_CONTROL), N_SA_DMIBAR_OPIO_SECURITY_LOCK_BIT);
      }
    }

  DEBUG((DEBUG_INFO, "SaSecurityLock End\n"));
  PostCode(0xA5F);
}

/**
  Program Host Bridge work arounds
**/
VOID
HostBridgeWorkAround (
  VOID
  )
{
  UINT64          McD0BaseAddress;
  UINT64_STRUCT   MchBar;

  McD0BaseAddress = PCI_SEGMENT_LIB_ADDRESS(SA_SEG_NUM, SA_MC_BUS, SA_MC_DEV, SA_MC_FUN, 0);
  MchBar.Data32.High = PciSegmentRead32(McD0BaseAddress + R_SA_MCHBAR + 4);
  MchBar.Data32.Low = PciSegmentRead32(McD0BaseAddress + R_SA_MCHBAR);
  MchBar.Data &= (UINT64)~BIT0;
  ///
  /// Configure MchBar+ R_SA_MCHBAR_HDAUDRID_IMPH [7:3] = 0x1F and set Bit31.
  ///
  MmioAndThenOr32((UINTN)MchBar.Data + R_SA_MCHBAR_HDAUDRID_IMPH_VER2, (UINT32)~B_SA_MCHBAR_HDAUDRID_IMPH_DEVNUM_MASK, (UINT32)(0x1F << N_SA_MCHBAR_HDAUDRID_IMPH_DEVNUM_OFFSET) | BIT31);
}

/**
  Update SA Hob in PostMem

  @param[in]  GtConfig                 - Instance of GRAPHICS_PEI_PREMEM_CONFIG

  @retval EFI_SUCCESS
**/
EFI_STATUS
UpdateSaHobPostMem (
  IN       GRAPHICS_PEI_PREMEM_CONFIG  *GtPreMemConfig
)
{
  SA_CONFIG_HOB               *SaConfigHob;

  ///
  /// Locate HOB for SA Config Data
  ///
  SaConfigHob = (SA_CONFIG_HOB *) GetFirstGuidHob (&gSaConfigHobGuid);
  if (SaConfigHob != NULL) {
    SaConfigHob->ApertureSize = (UINT8)(GtPreMemConfig->ApertureSize);
  }

  return EFI_SUCCESS;
}

/**
  Get the GFX revision and update the same in ScratchPad register
**/
VOID
UpdateGfxRevisionToScratchpad (
  VOID
  )
{
  UINT8                       GfxRevisionId;
  UINTN                       McD2BaseAddress;
  UINTN                       MchBar;
  MchBar = PciSegmentRead32 (PCI_SEGMENT_LIB_ADDRESS (SA_SEG_NUM, SA_MC_BUS, SA_MC_DEV, SA_MC_FUN, R_SA_MCHBAR)) & ~BIT0;
  ///
  /// Get the Graphics Revision Id
  ///
  McD2BaseAddress = PCI_SEGMENT_LIB_ADDRESS (SA_SEG_NUM, IGD_BUS_NUM, IGD_DEV_NUM, IGD_FUN_NUM, 0);
  GfxRevisionId = PciSegmentRead8 (McD2BaseAddress + PCI_REVISION_ID_OFFSET);
  if (GfxRevisionId) {
    MmioWrite32 ((MchBar + MCHBAR_MMIO_SCRATCHPAD0), GfxRevisionId);
  }
}

/**
  Disable Gna Device
**/
VOID
DisableGnaDevice (
  VOID
  )
{
  UINT64    McD0BaseAddress;

  McD0BaseAddress = PCI_SEGMENT_LIB_ADDRESS (SA_SEG_NUM, SA_MC_BUS, SA_MC_DEV, SA_MC_FUN, 0);
  PciSegmentAnd32 (McD0BaseAddress + R_SA_DEVEN, (UINT32) ~B_SA_DEVEN_D8EN_MASK);
}

/**
  This function loads MemConfigSaGv default values

  @param[in] MemConfig         Pointer to Memory Config block
**/
VOID
UpdateMemConfigSaGvDefault(
  IN  MEMORY_CONFIGURATION  *MemConfig
  )
{
#if FixedPcdGetBool (PcdAdlLpSupport) == 0
  MemConfig->SaGv               = 0; // Disabled
#else
  MemConfig->SaGv               = 5; // Enabled
#endif
}

