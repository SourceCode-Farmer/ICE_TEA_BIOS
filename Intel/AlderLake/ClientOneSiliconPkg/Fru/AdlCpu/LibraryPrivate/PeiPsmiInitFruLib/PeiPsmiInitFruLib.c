/** @file
  This code provides an instance of Pei PSMI Init Fru Lib.

@copyright
  INTEL CONFIDENTIAL
  Copyright 2020 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains an 'Intel Peripheral Driver' and is uniquely identified as
  "Intel Reference Module" and is licensed for Intel CPUs and chipsets under
  the terms of your license agreement with Intel or your vendor. This file may
  be modified by the user, subject to additional terms of the license agreement.

@par Specification Reference:
**/
#include <Library/DebugLib.h>
#include <Library/PcdLib.h>
#include <Library/PciSegmentLib.h>
#include <IndustryStandard/Pci.h>
#include <Register/TraceHubRegs.h>
#include <Library/PeiPsmiInitFruLib.h>
#include <Library/PeiTraceHubLib.h>

/**
  Override policies relevant to CPU Trace Hub to enable PSMI debug in PreMem.

  @param[in] SiPreMemPolicyPpi         The Silicon PreMem Policy PPI instance
**/
VOID
PsmiTraceHubPolicyOverride (
  IN  SI_PREMEM_POLICY_PPI             *SiPreMemPolicyPpi
  )
{
  EFI_STATUS                           Status;
  CPU_TRACE_HUB_PREMEM_CONFIG          *CpuTraceHubPreMemConfig;
  PSMI_DATA_HOB_VER2                   *PsmiDataHob;

  CpuTraceHubPreMemConfig            = NULL;
  PsmiDataHob                        = NULL;

  Status = GetConfigBlock ((VOID *) SiPreMemPolicyPpi, &gCpuTraceHubPreMemConfigGuid, (VOID *) &CpuTraceHubPreMemConfig);
  ASSERT_EFI_ERROR (Status);

  PsmiDataHob = (PSMI_DATA_HOB_VER2 *)GetFirstGuidHob (&gPsmiDataHobGuid);
  if (PsmiDataHob != NULL) {
  //
  // CPU Trace Hub settings for PSMI (Force HostDebugger Mode)
  //
    if (PsmiDataHob->PsmiConfig.HandlerInfo.HandlerSize != 0) {

      DEBUG ((DEBUG_INFO, "PSMI Force CPU Trace Hub in HostDebugger mode \n"));
      DEBUG ((DEBUG_INFO, "Override CPU Trace Hub MemSize 0/1 to 0 \n"));

      CpuTraceHubPreMemConfig->TraceHub.MemReg0Size = TraceBufferNone;
      CpuTraceHubPreMemConfig->TraceHub.MemReg1Size = TraceBufferNone;
      CpuTraceHubPreMemConfig->TraceHub.EnableMode  = TraceHubModeHostDebugger;

      DEBUG ((DEBUG_INFO, "CpuTraceHubPreMemConfig->TraceHub.EnableMode = %x\n", CpuTraceHubPreMemConfig->TraceHub.EnableMode));
      DEBUG ((DEBUG_INFO, "CpuTraceHubPreMemConfig->TraceHub.MemReg0Size = %x\n", CpuTraceHubPreMemConfig->TraceHub.MemReg0Size));
      DEBUG ((DEBUG_INFO, "CpuTraceHubPreMemConfig->TraceHub.MemReg1Size = %x\n", CpuTraceHubPreMemConfig->TraceHub.MemReg1Size));
    }
  }
}

/**
  Returns the Base address of CPU Trace Hub

  @retval CPU Trace Hub Base Address
**/
UINT32
GetTraceHubBaseAddress (
  VOID
  )
{
  return PCI_SEGMENT_LIB_ADDRESS (SA_SEG_NUM, PCI_BUS_NUMBER_CPU_TRACE_HUB, PCI_DEVICE_NUMBER_CPU_TRACE_HUB, PCI_FUNCTION_NUMBER_CPU_TRACE_HUB, 0);
}

/**
  Returns the Base address of Trace Hub MTB Bar

  @retval Tracehub MTB Bar base address
**/
UINT32
GetTraceHubMtbBarBaseAddress (
  VOID
  )
{
  return PcdGet32 (PcdCpuTraceHubMtbBarBase);
}
