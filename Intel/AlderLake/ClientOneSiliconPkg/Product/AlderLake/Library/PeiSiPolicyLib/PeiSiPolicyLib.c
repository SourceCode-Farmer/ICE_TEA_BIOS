/** @file
  This file is PeiSiPolicyLib library creates default settings of RC
  Policy and installs RC Policy PPI.

@copyright
  INTEL CONFIDENTIAL
  Copyright 2014 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains an 'Intel Peripheral Driver' and is uniquely identified as
  "Intel Reference Module" and is licensed for Intel CPUs and chipsets under
  the terms of your license agreement with Intel or your vendor. This file may
  be modified by the user, subject to additional terms of the license agreement.

@par Specification Reference:
**/
#include "PeiSiPolicyLibrary.h"
#include <Library/PcdLib.h>
#include <Library/PeiRtcPolicyLib.h>
#include <Library/PeiThermalPolicyLib.h>
#include <Library/PchInfoLib.h>
#if FixedPcdGetBool (PcdITbtEnable) == 1
#include <Library/PeiTcssInitFruLib.h>
#endif
/**
  Get Si config block table total size.

  @retval                               Size of PCH config block table
**/
UINT16
EFIAPI
SiGetConfigBlockTotalSize (
  VOID
  )
{
  return (UINT16) sizeof (SI_CONFIG);
}

EFI_STATUS
EFIAPI
LoadSiConfigBlockDefault (
  IN VOID *ConfigBlockPointer
  )
{
  SI_CONFIG                         *SiConfig;

  SiConfig = ConfigBlockPointer;

  DEBUG ((DEBUG_INFO, "SiConfig->Header.GuidHob.Name = %g\n", &SiConfig->Header.GuidHob.Name));
  DEBUG ((DEBUG_INFO, "SiConfig->Header.GuidHob.Header.HobLength = 0x%x\n", SiConfig->Header.GuidHob.Header.HobLength));

  SiConfig->Header.Revision = SI_CONFIG_REVISION;

  return EFI_SUCCESS;
}

EFI_STATUS
EFIAPI
SiAddConfigBlocks (
  IN     VOID      *ConfigBlockTableAddress
  )
{
  VOID                 *ConfigBlockPointer;
  EFI_STATUS           Status;
  CONFIG_BLOCK_HEADER  SiBlock;

  //
  // Initalize SiBlock
  //
  CopyMem (&(SiBlock.GuidHob.Name), &gSiConfigGuid, sizeof (EFI_GUID));
  SiBlock.GuidHob.Header.HobLength = sizeof (SI_CONFIG);
  SiBlock.Revision                 = SI_CONFIG_REVISION;
  //
  // Initialize ConfigBlockPointer
  //
  ConfigBlockPointer = (VOID *)&SiBlock;
  //
  // Add config block fro SiBlock
  //
  DEBUG ((DEBUG_INFO, "gSiConfigGuid = %g\n", &gSiConfigGuid));
  DEBUG ((DEBUG_INFO, "SiConfig->Header.GuidHob.Name = %g\n", &(SiBlock.GuidHob.Name)));
  Status = AddConfigBlock (ConfigBlockTableAddress, (VOID *) &ConfigBlockPointer);
  ASSERT_EFI_ERROR (Status);

  LoadSiConfigBlockDefault ((VOID *) ConfigBlockPointer);

  return Status;
}

/**
  SiCreateConfigBlocks creates the config blocksg of Silicon Policy.
  It allocates and zero out buffer, and fills in the Intel default settings.

  @param[out] SiPolicyPpi         The pointer to get Silicon Policy PPI instance

  @retval EFI_SUCCESS             The policy default is initialized.
  @retval EFI_OUT_OF_RESOURCES    Insufficient resources to create buffer
**/
EFI_STATUS
EFIAPI
SiCreateConfigBlocks (
  OUT  SI_POLICY_PPI **SiPolicyPpi
  )
{
  UINT16        TotalBlockSize;
  EFI_STATUS    Status;
  SI_POLICY_PPI *SiPolicy;
  UINT16        RequiredSize;

  SiPolicy = NULL;
  //
  // TotalBlockSize = Si, Pch, AMT, ME, SA and CPU config block size.
  //
  TotalBlockSize = SiGetConfigBlockTotalSize () +
                   PchGetConfigBlockTotalSize () +
                   AmtGetConfigBlockTotalSize () +
                   MeGetConfigBlockTotalSize () +
                   SaGetConfigBlockTotalSize () +
                   CpuGetConfigBlockTotalSize ();

#if FixedPcdGetBool (PcdITbtEnable) == 1
  if (IsTcssSupported ()) {
    TotalBlockSize += ITbtGetConfigBlockTotalSize ();
  }
#endif
  //
  // Add size of IpBlock config blocks
  //
  TotalBlockSize += RtcGetConfigBlockTotalSize ();
  TotalBlockSize += ThermalGetConfigBlockTotalSize ();
  TotalBlockSize += CnviGetConfigBlockTotalSize ();
  TotalBlockSize += EspiGetConfigBlockTotalSize ();
  TotalBlockSize += GbeGetConfigBlockTotalSize ();
  TotalBlockSize += TsnGetConfigBlockTotalSize ();
  TotalBlockSize += HdaGetConfigBlockTotalSize ();
  TotalBlockSize += IehGetConfigBlockTotalSize ();
  if (PchIsIshSupported ()) {
    TotalBlockSize += IshGetConfigBlockTotalSize ();
  }
  TotalBlockSize += ItssGetConfigBlockTotalSize ();
  TotalBlockSize += P2sbGetConfigBlockTotalSize ();
  TotalBlockSize += PchDmiGetConfigBlockTotalSize ();
  TotalBlockSize += PmcGetConfigBlockTotalSize ();
  TotalBlockSize += SataGetConfigBlockTotalSize ();
#if defined(PCH_ADPP)
  TotalBlockSize += ScsGetConfigBlockTotalSize ();
#endif
  TotalBlockSize += UsbGetConfigBlockTotalSize ();
  TotalBlockSize += ThcGetConfigBlockTotalSize ();
  TotalBlockSize += PsfGetConfigBlockTotalSize ();
  TotalBlockSize += FusaGetConfigBlockTotalSize ();
  TotalBlockSize += GraphicsGetConfigBlockTotalSize ();
  TotalBlockSize += TcssGetConfigBlockTotalSize ();
  TotalBlockSize += GnaGetConfigBlockTotalSize ();
  TotalBlockSize += VmdGetPeiConfigBlockTotalSize ();
  TotalBlockSize += SaPcieGetConfigBlockTotalSize ();
  TotalBlockSize += CpuPcieGetConfigBlockTotalSize ();
  TotalBlockSize += TelemetryGetPeiConfigBlockTotalSize ();
  TotalBlockSize += SerialIoGetConfigBlockTotalSize ();
  TotalBlockSize += GetCpuPowerMgmtVrConfigBlockTotalSize ();
  TotalBlockSize += HybridStorageGetConfigBlockTotalSize ();
  TotalBlockSize += HostBridgeGetPeiConfigBlockTotalSize ();
  TotalBlockSize += SpiGetConfigBlockTotalSize ();

  DEBUG ((DEBUG_INFO, "TotalBlockSize = 0x%x\n", TotalBlockSize));

  RequiredSize = sizeof (CONFIG_BLOCK_TABLE_HEADER) + TotalBlockSize;

  Status = CreateConfigBlockTable (RequiredSize, (VOID *) &SiPolicy);
  ASSERT_EFI_ERROR (Status);

  //
  // General initialization
  //
  SiPolicy->TableHeader.Header.Revision = SI_POLICY_REVISION;
  //
  // Add config blocks.
  //
  Status = SiAddConfigBlocks ((VOID *) SiPolicy);
  ASSERT_EFI_ERROR (Status);
  Status = PchAddConfigBlocks ((VOID *) SiPolicy);
  ASSERT_EFI_ERROR (Status);
  Status = AmtAddConfigBlocks ((VOID *) SiPolicy);
  ASSERT_EFI_ERROR (Status);
  Status = MeAddConfigBlocks ((VOID *) SiPolicy);
  ASSERT_EFI_ERROR (Status);
  Status = SaAddConfigBlocks ((VOID *) SiPolicy);
  ASSERT_EFI_ERROR (Status);
  Status = CpuAddConfigBlocks ((VOID *) SiPolicy);
  ASSERT_EFI_ERROR (Status);

#if FixedPcdGetBool (PcdITbtEnable) == 1
  if (IsTcssSupported ()) {
    Status = ITbtAddConfigBlocks ((VOID *) SiPolicy);
    ASSERT_EFI_ERROR (Status);
  }
#endif

  //
  // IpBlock config blocks
  //
  Status = RtcAddConfigBlock (SiPolicy);
  ASSERT_EFI_ERROR (Status);
  Status = ThermalAddConfigBlock (SiPolicy);
  ASSERT_EFI_ERROR (Status);
  Status = CnviAddConfigBlock (SiPolicy);
  ASSERT_EFI_ERROR (Status);
  Status = EspiAddConfigBlock (SiPolicy);
  ASSERT_EFI_ERROR (Status);
  Status = GbeAddConfigBlock (SiPolicy);
  ASSERT_EFI_ERROR (Status);
  Status = TsnAddConfigBlock (SiPolicy);
  ASSERT_EFI_ERROR (Status);
  Status = HdaAddConfigBlock (SiPolicy);
  ASSERT_EFI_ERROR (Status);
  Status = IehAddConfigBlock (SiPolicy);
  ASSERT_EFI_ERROR (Status);
  if (PchIsIshSupported ()) {
    Status = IshAddConfigBlock (SiPolicy);
    ASSERT_EFI_ERROR (Status);
  }
  Status = ItssAddConfigBlock (SiPolicy);
  ASSERT_EFI_ERROR (Status);
  Status = P2sbAddConfigBlock (SiPolicy);
  ASSERT_EFI_ERROR (Status);
  Status = PchDmiAddConfigBlock (SiPolicy);
  ASSERT_EFI_ERROR (Status);
  Status = PmcAddConfigBlock (SiPolicy);
  ASSERT_EFI_ERROR (Status);
  Status = SataAddConfigBlock (SiPolicy);
  ASSERT_EFI_ERROR (Status);
#if defined(PCH_ADPP)
  Status = ScsAddConfigBlock (SiPolicy);
  ASSERT_EFI_ERROR (Status);
#endif
  Status = UsbAddConfigBlock (SiPolicy);
  ASSERT_EFI_ERROR (Status);
  Status = ThcAddConfigBlock (SiPolicy);
  ASSERT_EFI_ERROR (Status);
  Status = PsfAddConfigBlock (SiPolicy);
  ASSERT_EFI_ERROR (Status);
  Status = FusaAddConfigBlock (SiPolicy);
  ASSERT_EFI_ERROR (Status);
  Status = GraphicsAddConfigBlocks ((VOID *) SiPolicy);
  ASSERT_EFI_ERROR (Status);
  Status = TcssAddConfigBlock ((VOID *) SiPolicy);
  ASSERT_EFI_ERROR (Status);
  Status = GnaAddConfigBlocks ((VOID *) SiPolicy);
  ASSERT_EFI_ERROR (Status);
  Status = VmdAddPeiConfigBlock ((VOID *) SiPolicy);
  ASSERT_EFI_ERROR (Status);
  Status = CpuPcieAddConfigBlocks ((VOID *)SiPolicy);
  ASSERT_EFI_ERROR (Status);
  Status = TelemetryAddPeiConfigBlock ((VOID *) SiPolicy);
  ASSERT_EFI_ERROR (Status);
  Status = SerialIoAddConfigBlock ((VOID *) SiPolicy);
  ASSERT_EFI_ERROR (Status);
  Status = AddCpuPowerMgmtVrConfigBlock ((VOID *) SiPolicy);
  ASSERT_EFI_ERROR (Status);
  Status = HybridStorageAddConfigBlocks ((VOID *) SiPolicy);
  ASSERT_EFI_ERROR (Status);
  Status = HostBridgeAddPeiConfigBlock ((VOID *) SiPolicy);
  ASSERT_EFI_ERROR (Status);
  Status = SpiAddConfigBlock (SiPolicy);
  ASSERT_EFI_ERROR (Status);

  //
  // Assignment for returning SaInitPolicy config block base address
  //
  *SiPolicyPpi = SiPolicy;
  return Status;
}

/**
  SiInstallPolicyPpi installs SiPolicyPpi.
  While installed, RC assumes the Policy is ready and finalized. So please update and override
  any setting before calling this function.

  @param[in] SiPolicyPpi         The pointer to Silicon Policy PPI instance

  @retval EFI_SUCCESS            The policy is installed.
  @retval EFI_OUT_OF_RESOURCES   Insufficient resources to create buffer
**/
EFI_STATUS
EFIAPI
SiInstallPolicyPpi (
  IN  SI_POLICY_PPI *SiPolicyPpi
  )
{
  EFI_STATUS             Status;
  EFI_PEI_PPI_DESCRIPTOR *SiPolicyPpiDesc;
  SI_CONFIG              *SiConfig;

  SiPolicyPpiDesc = (EFI_PEI_PPI_DESCRIPTOR *) AllocateZeroPool (sizeof (EFI_PEI_PPI_DESCRIPTOR));
  if (SiPolicyPpiDesc == NULL) {
    ASSERT (FALSE);
    return EFI_OUT_OF_RESOURCES;
  }

  SiPolicyPpiDesc->Flags = EFI_PEI_PPI_DESCRIPTOR_PPI | EFI_PEI_PPI_DESCRIPTOR_TERMINATE_LIST;
  SiPolicyPpiDesc->Guid  = &gSiPolicyPpiGuid;
  SiPolicyPpiDesc->Ppi   = SiPolicyPpi;

  Status = GetConfigBlock ((VOID *) SiPolicyPpi, &gSiConfigGuid, (VOID *) &SiConfig);
  ASSERT_EFI_ERROR (Status);

  //
  // Install Silicon Policy PPI
  //
  Status = PeiServicesInstallPpi (SiPolicyPpiDesc);
  ASSERT_EFI_ERROR (Status);
  return Status;
}

/**
  SiInstallPolicyReadyPpi installs SiPolicyReadyPpi.
  While installed, RC assumes the Policy is ready and finalized. So please update and override
  any setting before calling this function.

  @retval EFI_SUCCESS            The policy is installed.
  @retval EFI_OUT_OF_RESOURCES   Insufficient resources to create buffer
**/
EFI_STATUS
EFIAPI
SiInstallPolicyReadyPpi (
  VOID
  )
{
  EFI_STATUS             Status;
  EFI_PEI_PPI_DESCRIPTOR *SiPolicyReadyPpiDesc;

  SiPolicyReadyPpiDesc = (EFI_PEI_PPI_DESCRIPTOR *) AllocateZeroPool (sizeof (EFI_PEI_PPI_DESCRIPTOR));
  if (SiPolicyReadyPpiDesc == NULL) {
    ASSERT (FALSE);
    return EFI_OUT_OF_RESOURCES;
  }

  SiPolicyReadyPpiDesc->Flags = EFI_PEI_PPI_DESCRIPTOR_PPI | EFI_PEI_PPI_DESCRIPTOR_TERMINATE_LIST;
  SiPolicyReadyPpiDesc->Guid  = &gSiPolicyReadyPpiGuid;
  SiPolicyReadyPpiDesc->Ppi   = NULL;

  //
  // Install Silicon Policy Ready PPI
  //
  Status = PeiServicesInstallPpi (SiPolicyReadyPpiDesc);
  ASSERT_EFI_ERROR (Status);
  return Status;
}

