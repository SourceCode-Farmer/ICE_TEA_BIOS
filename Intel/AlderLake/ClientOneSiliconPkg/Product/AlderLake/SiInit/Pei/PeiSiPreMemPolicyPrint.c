/** @file
  Source code file for PEI Silicon Pre Memory Policy Print function.

  @copyright
  INTEL CONFIDENTIAL
  Copyright 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains an 'Intel Peripheral Driver' and is uniquely identified as
  "Intel Reference Module" and is licensed for Intel CPUs and chipsets under
  the terms of your license agreement with Intel or your vendor. This file may
  be modified by the user, subject to additional terms of the license agreement.
**/

#include <Library/PeiServicesLib.h>
#include <Library/SiPolicyLib.h>
#include <Library/PeiPcieRpPolicyLib.h>
#include <Library/PchPolicyLib.h>
#include <Library/PeiMePolicyLib.h>
#include <Library/PeiMeServerPreMemPolicyLib.h>
#include <Library/PeiSaPolicyLib.h>
#include <Library/PeiMemPolicyLib.h>
#include <Library/PeiIpuPolicyPrivateLib.h>
#include <Library/PeiCpuDmiPolicyLib.h>
#include <Library/PeiGraphicsPolicyLib.h>
#include <Library/PeiTcssPolicyLib.h>
#include <Library/PeiVtdPolicyLib.h>
#include <Library/PeiTelemetryPolicyLib.h>
#include <Library/PeiHostBridgePolicyLib.h>
#include <Library/CpuPolicyLib.h>
#include <Library/PeiDciPolicyLib.h>
#include <Library/PeiHdaPolicyLib.h>
#include <Library/PeiFiaPolicyLib.h>
#include <Library/PchInfoLib.h>
#include <Library/PeiIshPolicyLib.h>
#include <Library/PeiCpuTraceHubPolicyLib.h>
#include <Library/PeiPchTraceHubPolicyLib.h>
#include <Library/PeiPchDmiPolicyLib.h>
#include <Library/PeiSmbusPolicyLib.h>
#include <Library/PeiWdtPolicyLib.h>
#include <Library/PeiOcPolicyLib.h>
#include <Library/PeiCpuPciePolicyLib.h>
#include <Library/PeiHybridGraphicsPolicyLib.h>
#include <Library/PeiDpInPolicyLib.h>
#include <Library/PeiCnviPolicyLib.h>

/**
  PPI function to print Silicon PreMem Policy settings.

  @retval EFI_STATUS       - Locate SiPreMemPolicy PPI successfully.
  @retval others           - Locate SiPreMemPolicy PPI fail.
**/
EFI_STATUS
EFIAPI
PeiPreMemSiPolicyPrint (
  VOID
  )
{
  EFI_STATUS            Status;
  SI_PREMEM_POLICY_PPI  *SiPreMemPolicyPpi;

  Status = PeiServicesLocatePpi (
             &gSiPreMemPolicyPpiGuid,
             0,
             NULL,
             (VOID **)&SiPreMemPolicyPpi
             );
  if (Status == EFI_SUCCESS) {
    //
    // Print SI PreMem config block
    //
    SiPreMemPrintPolicyPpi (SiPreMemPolicyPpi);
    //
    // Print PCIe PreMem config block
    //
    PciePreMemPrintPolicyPpi (SiPreMemPolicyPpi);
    //
    // Print PCH PreMem config blocks
    //
    PchPreMemPrintPolicyPpi (SiPreMemPolicyPpi);
    //
    // Print ME PreMem config blocks
    //
    MePrintPolicyPpiPreMem (SiPreMemPolicyPpi);
    //
    // Print MeServer PreMem config blocks
    //
    MeServerPreMemPrintPolicyPpi (SiPreMemPolicyPpi);
    //
    // Print SA PreMem config blocks
    //
    SaPrintPolicyPpiPreMem (SiPreMemPolicyPpi);
    //
    // Print Mrc PreMem config blocks
    //
    MrcPrintPolicyPpiPreMem (SiPreMemPolicyPpi);
    //
    //
    // Print IPU PreMem config blocks
    //
    IpuPrintPolicyPpiPreMem (SiPreMemPolicyPpi);
    //
    // Print CPU DMIs PreMem config blocks
    //
    CpuDmiPreMemPrintConfig (SiPreMemPolicyPpi);
    GraphicsPrintPolicyPpiPreMem (SiPreMemPolicyPpi);
    TcssPrintConfigPreMem (SiPreMemPolicyPpi);
    VtdPrintPolicyPpi (SiPreMemPolicyPpi);
    TelemetryPrintPeiPolicyPpiPreMem (SiPreMemPolicyPpi);
    HostBridgePreMemPrintConfig (SiPreMemPolicyPpi);
    //
    // Print CPU PreMem config blocks
    //
    CpuPreMemPrintPolicy (SiPreMemPolicyPpi);
    IpuPrintPolicyPpiPreMem (SiPreMemPolicyPpi);
    DciPreMemPrintConfig (SiPreMemPolicyPpi);
    HdaPreMemPrintConfig (SiPreMemPolicyPpi);
    FiaMuxPreMemPrintConfig (SiPreMemPolicyPpi);
    if (PchIsIshSupported ()) {
      IshPreMemPrintConfig (SiPreMemPolicyPpi);
    }
    CpuTraceHubPreMemPrintConfig (SiPreMemPolicyPpi);
    PchTraceHubPreMemPrintConfig (SiPreMemPolicyPpi);
    SmbusPreMemPrintConfig (SiPreMemPolicyPpi);
    WdtPreMemPrintConfig (SiPreMemPolicyPpi);
    OcPreMemPrintConfig (SiPreMemPolicyPpi);
    CpuPciePrintPolicyPpiPreMem (SiPreMemPolicyPpi);
    CpuPciePeiPrintPolicyPpiPreMem (SiPreMemPolicyPpi);
    HybridGraphicsPrintPolicyPpi (SiPreMemPolicyPpi);
    DpInPrintConfigPreMem (SiPreMemPolicyPpi);
    CnviPreMemPrintConfig (SiPreMemPolicyPpi);
  }

  return Status;
}