/** @file
  Source code file for PEI Silicon Policy Print function.

  @copyright
  INTEL CONFIDENTIAL
  Copyright 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains an 'Intel Peripheral Driver' and is uniquely identified as
  "Intel Reference Module" and is licensed for Intel CPUs and chipsets under
  the terms of your license agreement with Intel or your vendor. This file may
  be modified by the user, subject to additional terms of the license agreement.
**/

#include <Library/PeiServicesLib.h>
#include <Library/SiPolicyLib.h>
#include <Library/PcdLib.h>
#include <Library/PchPolicyLib.h>
#include <Library/PeiRtcPolicyLib.h>
#include <Library/PeiThermalPolicyLib.h>
#include <Library/PeiCnviPolicyLib.h>
#include <Library/PeiEspiPolicyLib.h>
#include <Library/PeiGbePolicyLib.h>
#include <Library/PeiTsnPolicyLib.h>
#include <Library/PeiHdaPolicyLib.h>
#include <Library/PeiIehPolicyLib.h>
#include <Library/PchInfoLib.h>
#include <Library/PeiIshPolicyLib.h>
#include <Library/PeiItssPolicyLib.h>
#include <Library/PeiP2sbPolicyLib.h>
#include <Library/PeiPchDmiPolicyLib.h>
#include <Library/PeiPmcPolicyLib.h>
#include <Library/PeiSataPolicyLib.h>
#include <Library/PeiScsPolicyLib.h>
#include <Library/PeiUsbPolicyLib.h>
#include <Library/PeiThcPolicyLib.h>
#include <Library/PeiPsfPolicyLib.h>
#include <Library/PeiFusaPolicyLib.h>
#include <Library/PeiHybridStoragePolicyLib.h>
#include <Library/PeiSpiPolicyLib.h>
#include <Library/PeiAmtPolicyLib.h>
#include <Library/PeiMePolicyLib.h>
#include <Library/PeiSaPolicyLib.h>
#include <Library/PeiGraphicsPolicyLib.h>
#include <Library/PeiGnaPolicyLib.h>
#include <Library/PeiVmdPolicyLib.h>
#include <Library/PeiTelemetryPolicyLib.h>
#include <Library/PeiHostBridgePolicyLib.h>
#include <Library/PeiSerialIoPolicyLib.h>
#include <Library/CpuPolicyLib.h>
#include <Library/PeiTcssPolicyLib.h>
#include <Library/PeiITbtPolicyLib.h>
#include <Library/PeiCpuPciePolicyLib.h>
#include <Library/PeiVrPolicyLib.h>

/**
  PPI function to print Silicon Policy config block settings

  @retval EFI_STATUS       - Locate SiPolicy PPI successfully.
  @retval others           - Locate SiPolicy PPI fail.
 **/
EFI_STATUS
EFIAPI
PeiSiPolicyPrint (
  VOID
  )
{
  EFI_STATUS     Status;
  SI_POLICY_PPI  *SiPolicyPpi;

  Status = PeiServicesLocatePpi (
             &gSiPolicyPpiGuid,
             0,
             NULL,
             (VOID **)&SiPolicyPpi
             );
  if (Status == EFI_SUCCESS) {
    //
    // Print SI config blocks and serial out.
    //
    SiPrintPolicyPpi (SiPolicyPpi);

    //
    // Print PCH config blocks and serial out.
    //
    PchPrintPolicyPpi (SiPolicyPpi);
    //
    // Print IpBlocks config.
    //
    RtcPrintConfig (SiPolicyPpi);
    ThermalPrintConfig (SiPolicyPpi);
    CnviPrintConfig (SiPolicyPpi);
    EspiPrintConfig (SiPolicyPpi);
    GbePrintConfig (SiPolicyPpi);
    TsnPrintConfig (SiPolicyPpi);
    HdaPrintConfig (SiPolicyPpi);
    IehPrintConfig (SiPolicyPpi);
    if (PchIsIshSupported ()) {
      IshPrintConfig (SiPolicyPpi);
    }
    ItssPrintConfig (SiPolicyPpi);
    P2sbPrintConfig (SiPolicyPpi);
    PchDmiPrintConfig (SiPolicyPpi);
    PmcPrintConfig (SiPolicyPpi);
    SataPrintConfig (SiPolicyPpi);
#if defined(PCH_ADPP)
    ScsPrintConfig (SiPolicyPpi);
#endif
    UsbPrintConfig (SiPolicyPpi);
    ThcPrintConfig (SiPolicyPpi);
    PsfPrintConfig (SiPolicyPpi);
    FusaPrintConfig (SiPolicyPpi);
    HybridStoragePrintConfig (SiPolicyPpi);
    SpiPrintConfig (SiPolicyPpi);
    //
    // Print AMT config blocks and serial out.
    //
    AmtPrintPolicyPpi (SiPolicyPpi);
    //
    // Print ME config blocks and serial out.
    //
    MePrintPolicyPpi (SiPolicyPpi);
    //
    // Print SA config blocks and serial out.
    //
    SaPrintPolicyPpi (SiPolicyPpi);
    //
    // Print SA IpBlocks config.
    //
    GraphicsPrintPolicyPpi (SiPolicyPpi);
    GnaPrintPolicyPpi (SiPolicyPpi);
    VmdPrintPeiPolicyPpi (SiPolicyPpi);
    TelemetryPrintPeiPolicyPpi (SiPolicyPpi);
    HostBridgePeiPrintConfig (SiPolicyPpi);
    SerialIoPrintConfig (SiPolicyPpi);

    //
    // Print CPU config block and serial out.
    //
    CpuPrintPolicy (SiPolicyPpi);
    TcssPrintConfig (SiPolicyPpi);
#if FixedPcdGetBool (PcdITbtEnable) == 1
    ITbtPrintPolicy (SiPolicyPpi);
#endif
    //
    // Print Cpu Pcie config block and serial out.
    //
    CpuPciePrintPolicyPpi (SiPolicyPpi);

    //
    // Print VR config block.
    //
    CpuPowerMgmtVrConfigPrint (SiPolicyPpi);
  }

  return Status;
}