/** @file
    Source code file for TCSS Init Post Memory module.

@copyright
  INTEL CONFIDENTIAL
  Copyright 2013 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains an 'Intel Peripheral Driver' and is uniquely identified as
  "Intel Reference Module" and is licensed for Intel CPUs and chipsets under
  the terms of your license agreement with Intel or your vendor. This file may
  be modified by the user, subject to additional terms of the license agreement.

@par Specification
**/

#include "SiInitTcss.h"

#include <Register/SaRegsHostBridge.h>
#include <Register/CpuUsbRegs.h>

#include <Library/BaseMemoryLib.h>
#include <Library/TcssInitLib.h>
#include <Library/PciSegmentLib.h>
#include <Library/BaseMemoryLib.h>
#include <Library/ConfigBlockLib.h>
#include <Library/DebugLib.h>
#include <Library/DciPrivateLib.h>
#include <Library/PreSiliconEnvDetectLib.h>
#include <Library/MeFwStsLib.h>
#include <Library/CpuPlatformLib.h>
#include <Library/PcdLib.h>
#include <CpuGenInfo.h>

#include <TcssInfo.h>
#include <TcssPeiConfig.h>
#include <TcssPeiPreMemConfig.h>

/**
  Creates USB Host controller handle to pass to xHCI library with all necessary
  data for proper controller initialization

  @param[out]   UsbController       Instance of UsbController structure that
                                    will describe PCH USB Host Controller
**/
STATIC
VOID
CreateUsbHostControllerHandle (
  OUT   USB_CONTROLLER    *UsbController
  )
{
  ZeroMem (UsbController, sizeof (USB_CONTROLLER));

  UsbController->Segment        = SA_SEG_NUM;
  UsbController->Bus            = XHCI_NORTH_BUS_NUM;
  UsbController->Device         = XHCI_NORTH_DEV_NUM;
  UsbController->Function       = XHCI_NORTH_FUNC_NUM;
  UsbController->PciCfgBaseAddr = PCI_SEGMENT_LIB_ADDRESS (
                                    UsbController->Segment,
                                    UsbController->Bus,
                                    UsbController->Device,
                                    UsbController->Function,
                                    0);
  UsbController->Usb3LanesCount = MAX_TCSS_USB3_PORTS;
}

/**
  Creates USB Device controller handle to pass to xDCI library with all necessary
  data for proper controller initialization

  @param[out]   UsbController       Instance of UsbController structure that
                                    will describe PCH USB Device Controller
**/
STATIC
VOID
CreateUsbDeviceControllerHandle (
  OUT   USB_CONTROLLER    *UsbController
  )
{
  ZeroMem (UsbController, sizeof (USB_CONTROLLER));

  UsbController->Segment        = SA_SEG_NUM;
  UsbController->Bus            = XDCI_NORTH_BUS_NUM;
  UsbController->Device         = XDCI_NORTH_DEV_NUM;
  UsbController->Function       = XDCI_NORTH_FUNC_NUM;
  UsbController->PciCfgBaseAddr = PCI_SEGMENT_LIB_ADDRESS (
                                    UsbController->Segment,
                                    UsbController->Bus,
                                    UsbController->Device,
                                    UsbController->Function,
                                    0);
}

/**
  Initializes USB controllers handle for both Host and Device controllers
  along with required callbacks for cross IP dependencies

  @param[in]  SiPolicy            The Silicon Policy PPI instance
  @param[in]  TcssPeiConfig       TCSS_PEI_CONFIG instance
  @param[out] UsbHandle           USB_HANDLE structure
  @param[out] UsbPrivateConfig    USB_PRIVATE_CONFIG structure
  @param[out] UsbHostController   USB_CONTROLLER structure for Host Controller
  @param[out] UsbDeviceController USB_CONTROLLER structure for Device Controller
  @param[out] UsbCallback         USB_CALLBACK structure
**/
STATIC
VOID
CreateUsbControllerHandle (
  IN      SI_POLICY_PPI       *SiPolicy,
  IN      TCSS_PEI_CONFIG     *TcssPeiConfig,
  IN OUT  USB_HANDLE          *UsbHandle,
  IN OUT  USB_PRIVATE_CONFIG  *UsbPrivateConfig,
  IN OUT  USB_CONTROLLER      *UsbHostController,
  IN OUT  USB_CONTROLLER      *UsbDeviceController,
  IN OUT  USB_CALLBACK        *UsbCallback
  )
{
  //
  // Create private config values
  //
  UsbPrivateConfig->IsDebugEnabled = (IsHdciDebugEnabled () || !IsCpuDebugDisabled ());
  UsbPrivateConfig->IsSimulationEnvironment = (IsSimicsEnvironment () || IsHfpgaEnvironment ());
  UsbPrivateConfig->IpVersion = V19_0;
  UsbPrivateConfig->Location = Tcss;
  UsbPrivateConfig->EnableHcResetIsolationFlow = TRUE;

  UsbPrivateConfig->LtrHigh = 0x00050002;
  UsbPrivateConfig->LtrMid = 0x00050002;
  UsbPrivateConfig->LtrLow = 0x00050002;

  if (GetCpuFamily () == CPUID_FULL_FAMILY_MODEL_ALDERLAKE_MOBILE) {
    UsbPrivateConfig->CpxProgramming = TRUE;

    UsbPrivateConfig->Cp13Deemph.Field.Czero = 0x39;
    UsbPrivateConfig->Cp13Deemph.Field.CminusOnePrecursor = 0x6;

    UsbPrivateConfig->Cp14Deemph.Field.CplusOnePrecursor = 0x8;
    UsbPrivateConfig->Cp14Deemph.Field.Czero = 0x37;

    UsbPrivateConfig->Cp15Deemph.Field.CplusOnePrecursor = 0x8;
    UsbPrivateConfig->Cp15Deemph.Field.Czero = 0x31;
    UsbPrivateConfig->Cp15Deemph.Field.CminusOnePrecursor = 0x6;

    UsbPrivateConfig->Cp16Deemph.Field.Czero = 0x3F;
  }

  UsbHandle->PrivateConfig = UsbPrivateConfig;

  //
  // Populate callbacks pointers
  //
  UsbHandle->Callback = UsbCallback;

  //
  // Populate both host and device controllers data
  //
  CreateUsbHostControllerHandle (UsbHostController);
  CreateUsbDeviceControllerHandle (UsbDeviceController);
  UsbHandle->HostController = UsbHostController;
  UsbHandle->DeviceController = UsbDeviceController;

  UsbHandle->Mmio = PcdGet32 (PcdSiliconInitTempMemBaseAddr);

  //
  // Set config pointer
  //
  UsbHandle->UsbConfig = &(TcssPeiConfig->UsbConfig);
}

/**
  This function shall call USB IP initialization code after it gets all necessary data
  regarding the USB controllers configuration

  @param[in]  SiPolicy            The Silicon Policy PPI instance
  @param[in]  SiPreMemPolicy      The Silicon Pre Mem Policy PPI instance
**/
VOID
SiInitTcss (
  IN    SI_POLICY_PPI               *SiPolicy,
  IN    SI_PREMEM_POLICY_PPI        *SiPreMemPolicy
  )
{
  TCSS_PEI_PREMEM_CONFIG      *TcssPeiPreMemConfig;
  TCSS_PEI_CONFIG             *TcssPeiConfig;
  EFI_STATUS                  Status;
  USB_CONTROLLER              HostController;
  USB_CONTROLLER              DeviceController;
  USB_PRIVATE_CONFIG          PrivateConfig;
  USB_CALLBACK                UsbCallback;
  USB_HANDLE                  UsbHandle;

  //
  // Clear structures data
  //
  ZeroMem (&HostController, sizeof(USB_CONTROLLER));
  ZeroMem (&DeviceController, sizeof(USB_CONTROLLER));
  ZeroMem (&PrivateConfig, sizeof(USB_PRIVATE_CONFIG));
  ZeroMem (&UsbCallback, sizeof(USB_CALLBACK));
  ZeroMem (&UsbHandle, sizeof(USB_HANDLE));

  Status = GetConfigBlock (
             (VOID *) SiPreMemPolicy,
             &gTcssPeiPreMemConfigGuid,
             (VOID *) &TcssPeiPreMemConfig
             );
  ASSERT_EFI_ERROR (Status);
  if (EFI_ERROR (Status)) {
    DEBUG ((DEBUG_ERROR, "%a - error while getting TCSS Pre Mem config block. Aborting TCSS init.\n", __FUNCTION__));
    return;
  }

  Status = GetConfigBlock (
             (VOID *) SiPolicy,
             &gTcssPeiConfigGuid,
             (VOID *) &TcssPeiConfig
             );
  ASSERT_EFI_ERROR(Status);
  if (EFI_ERROR (Status)) {
    DEBUG ((DEBUG_ERROR, "%a - error while getting TCSS config block. Aborting TCSS init.\n", __FUNCTION__));
    return;
  }

  //
  // Initialize PCH USB Handle
  //
  CreateUsbControllerHandle (
    SiPolicy, TcssPeiConfig, &UsbHandle, &PrivateConfig, &HostController, &DeviceController, &UsbCallback
    );

  TcssInit (SiPolicy, TcssPeiPreMemConfig, TcssPeiConfig, &UsbHandle);
}
