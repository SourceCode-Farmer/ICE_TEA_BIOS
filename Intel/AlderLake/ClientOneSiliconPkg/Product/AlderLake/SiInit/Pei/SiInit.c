/** @file
  Source code file for Silicon Init Post Memory module.

@copyright
  INTEL CONFIDENTIAL
  Copyright 2013 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains an 'Intel Peripheral Driver' and is uniquely identified as
  "Intel Reference Module" and is licensed for Intel CPUs and chipsets under
  the terms of your license agreement with Intel or your vendor. This file may
  be modified by the user, subject to additional terms of the license agreement.

@par Specification
**/

#include "SiInit.h"
#include "SiInitTcss.h"
#include <Library/PerformanceLib.h>
#include <Library/PeiP2sbPrivateLib.h>
#include <Library/PeiSiPolicyOverrideLib.h>
#include <Library/PostCodeLib.h>
#include <Library/IpuInitLib.h>
#include <Library/PeiSiSsidLib.h>
#include <Library/VoltageRegulatorDomains.h>
#include <VoltageRegulatorCommands.h>
#include <Library/PeiVrPolicyLib.h>
#include <Library/PeiVrLib.h>

#include <Register/IgdRegs.h>

#ifdef FSP_FLAG
#include <Library/FspCommonLib.h>
#endif
#include <Library/SiPolicyLib.h>
#include <Library/MemoryAllocationLib.h>
#include <Ppi/PeiSiDefaultPolicy.h>
#include <Ppi/MpServices2.h>
#include <Library/PciSegmentLib.h>
#include <Register/GnaRegs.h>
#include <PcieRegs.h>
#include <SaConfigHob.h>
#include <VtdDataHob.h>
#include <Ppi/SiPolicy.h>
#include <Library/SaInitLib.h>
#include <Library/GnaInitLib.h>
#include <Library/PeiGraphicsInitLib.h>
#include <Library/PeiVtdInitLib.h>
#include <Library/PeiCpuPcieVgaInitLib.h>
#include <Library/PeiCpuPcieRpInitLib.h>
#include <Library/PeiCpuTraceHubLib.h>
#include <Library/PeiServicesTablePointerLib.h>
#include <Library/ConfigBlockLib.h>
#include <Library/PeiDisplayInitLib.h>
#include <Library/GpioPrivateLib.h>
#include <Library/GpioNativePads.h>
#include <Library/CpuDmiInfoLib.h>
#include <Library/PeiVmdInitLib.h>
#include <Library/TelemetryPrivateLib.h>
#include <TelemetryPeiConfig.h>
#include <Library/CpuPlatformLib.h>
#include <Library/PeiHostBridgeInitLib.h>
#include <Library/MsrFruLib.h>
#include <Library/PeiTcssInitFruLib.h>
#include <Library/PeiCpuDmiInitLib.h>
#include <Library/PeiCpuInitFruLib.h>
#include <Library/CpuCommonLib.h>
#include <Library/PeiHsioLib.h>
#include <Library/HsioSocLib.h>
#include <Library/PeiSaInitFruLib.h>
#include <Library/PeiFusaLib.h>
#include <Library/PeiOcLib.h>
#include <Library/PmcPrivateLib.h>
#include <Library/PchInfoLib.h>
#include <Ppi/PeiSiPolicyPrint.h>
#include <Library/PeiHybridStorageLib.h>
#include <Library/PeiDisplayInitLib.h>
//[-start-220118-QINGLIN0147-add]//
#if defined(S370_SUPPORT)
#include <Library/IoLib.h>
#endif
//[-end-220118-QINGLIN0147-add]//

EFI_PEI_PPI_DESCRIPTOR mEndOfSiInit = {
  (EFI_PEI_PPI_DESCRIPTOR_PPI | EFI_PEI_PPI_DESCRIPTOR_TERMINATE_LIST),
  &gEndOfSiInitPpiGuid,
  NULL
};

EFI_PEI_PPI_DESCRIPTOR  mEnablePeiGraphicsPpi = {
  (EFI_PEI_PPI_DESCRIPTOR_PPI | EFI_PEI_PPI_DESCRIPTOR_TERMINATE_LIST),
  &gEnablePeiGraphicsPpiGuid,
  NULL
};

#if FixedPcdGetBool(PcdFspBinaryEnable) == 0
GLOBAL_REMOVE_IF_UNREFERENCED EFI_PEI_NOTIFY_DESCRIPTOR  mSiInitNotifyList[] = {
  {
    EFI_PEI_PPI_DESCRIPTOR_NOTIFY_CALLBACK | EFI_PEI_PPI_DESCRIPTOR_TERMINATE_LIST,
    &gEndOfSiInitPpiGuid,
    SiInitOnEndOfPei
  }
};
#else
GLOBAL_REMOVE_IF_UNREFERENCED EFI_PEI_NOTIFY_DESCRIPTOR  mSiInitNotifyList[] = {
  {
    EFI_PEI_PPI_DESCRIPTOR_NOTIFY_CALLBACK | EFI_PEI_PPI_DESCRIPTOR_TERMINATE_LIST,
    &gEfiEndOfPeiSignalPpiGuid,
    SiInitOnEndOfPei
  }
};
#endif

static EFI_PEI_NOTIFY_DESCRIPTOR  mSiInitPostMemNotifyList[] = {
  {
    EFI_PEI_PPI_DESCRIPTOR_NOTIFY_DISPATCH | EFI_PEI_PPI_DESCRIPTOR_TERMINATE_LIST,
    &gSiPolicyReadyPpiGuid,
    SiInitPostMemOnPolicy
  }
};

STATIC PEI_SI_POLICY_PRINT_PPI mPeiSiPolicyPrintPpi = {
  PeiSiPolicyPrint
};

STATIC EFI_PEI_PPI_DESCRIPTOR  mPeiSiPolicyPrintPpiList = {
  EFI_PEI_PPI_DESCRIPTOR_PPI | EFI_PEI_PPI_DESCRIPTOR_TERMINATE_LIST,
  &gSiPolicyPrintPpiGuid,
  &mPeiSiPolicyPrintPpi
};

/**
  PPI function to install default ConfigBlock Policy PPI.

  @retval EFI_STATUS       - Status from each sub function.
**/
EFI_STATUS
EFIAPI
PeiSiDefaultPolicyInit (
  VOID
  )
{
  EFI_STATUS                            Status;
  EFI_PEI_PPI_DESCRIPTOR                *SiPolicyPpiDesc;
  SI_POLICY_PPI                         *SiPolicyPpi;

  SiPolicyPpi = NULL;
  Status = SiCreateConfigBlocks (&SiPolicyPpi);
  ASSERT_EFI_ERROR (Status);
  if (SiPolicyPpi != NULL) {
    SiPolicyPpiDesc = (EFI_PEI_PPI_DESCRIPTOR *) AllocateZeroPool (sizeof (EFI_PEI_PPI_DESCRIPTOR));
    if (SiPolicyPpiDesc == NULL) {
      ASSERT (FALSE);
      return EFI_OUT_OF_RESOURCES;
    }

    SiPolicyPpiDesc->Flags = EFI_PEI_PPI_DESCRIPTOR_PPI | EFI_PEI_PPI_DESCRIPTOR_TERMINATE_LIST;
    SiPolicyPpiDesc->Guid  = &gSiPolicyPpiGuid;
    SiPolicyPpiDesc->Ppi   = SiPolicyPpi;
    //
    // Install Silicon Policy PPI
    //
    Status = PeiServicesInstallPpi (SiPolicyPpiDesc);
    ASSERT_EFI_ERROR (Status);
  }
  return Status;
}

PEI_SI_DEFAULT_POLICY_INIT_PPI mPeiSiDefaultPolicyInitPpi = {
  PeiSiDefaultPolicyInit
};

static EFI_PEI_PPI_DESCRIPTOR  mPeiSiDefaultPolicyInitPpiList[] = {
  {
    EFI_PEI_PPI_DESCRIPTOR_PPI | EFI_PEI_PPI_DESCRIPTOR_TERMINATE_LIST,
    &gSiDefaultPolicyInitPpiGuid,
    &mPeiSiDefaultPolicyInitPpi
  }
};

EFI_PEI_PPI_DESCRIPTOR      mIDispCodecReadyPpi = {
  (EFI_PEI_PPI_DESCRIPTOR_PPI | EFI_PEI_PPI_DESCRIPTOR_TERMINATE_LIST),
  &gIDispCodecReadyPpiGuid,
  NULL
};

/**
  Silicon Init End of PEI callback function. This is the last change before entering DXE and OS when S3 resume.

  @param[in] PeiServices   - Pointer to PEI Services Table.
  @param[in] NotifyDesc    - Pointer to the descriptor for the Notification event that
                             caused this function to execute.
  @param[in] Ppi           - Pointer to the PPI data associated with this function.

  @retval EFI_STATUS       - Always return EFI_SUCCESS
**/
EFI_STATUS
SiInitOnEndOfPei (
  IN EFI_PEI_SERVICES                   **PeiServices,
  IN EFI_PEI_NOTIFY_DESCRIPTOR          *NotifyDesc,
  IN VOID                               *Ppi
  )
{
  EFI_STATUS                     Status;
  SI_POLICY_PPI                  *SiPolicy;
  SI_CONFIG                      *SiConfig;
  CPU_CONFIG                     *CpuConfig;
  EFI_BOOT_MODE                  BootMode;
  SI_PREMEM_POLICY_PPI           *SiPreMemPolicy;
  HOST_BRIDGE_PEI_CONFIG         *HostBridgePeiConfig;
  GRAPHICS_PEI_PREMEM_CONFIG     *GtPreMemConfig;
  GRAPHICS_PEI_CONFIG            *GtConfig;
  CPU_POWER_MGMT_VR_CONFIG       *CpuPowerMgmtVrConfig;
  CPU_CONFIG_LIB_PREMEM_CONFIG   *CpuConfigLibPreMemConfig;

  //
  // Get Policy settings through the SiPolicy PPI
  //
  Status = PeiServicesLocatePpi (
             &gSiPolicyPpiGuid,
             0,
             NULL,
             (VOID **) &SiPolicy
             );
  if ((Status != EFI_SUCCESS) || (SiPolicy == NULL)) {
    ASSERT (FALSE);
    return EFI_SUCCESS;
  }

  //
  // Get Si PreMem Policy settings through the SiPreMemPolicy PPI
  //
  Status = PeiServicesLocatePpi (
             &gSiPreMemPolicyPpiGuid,
             0,
             NULL,
             (VOID **) &SiPreMemPolicy
             );
  if (Status != EFI_SUCCESS) {
    ASSERT (FALSE);
    return EFI_SUCCESS;
  }

  Status = GetConfigBlock ((VOID *) SiPolicy, &gSiConfigGuid, (VOID *) &SiConfig);
  ASSERT_EFI_ERROR (Status);
  Status = GetConfigBlock ((VOID *) SiPolicy, &gCpuConfigGuid, (VOID *)&CpuConfig);
  ASSERT_EFI_ERROR(Status);

  Status = GetConfigBlock ((VOID *) SiPolicy, &gHostBridgePeiConfigGuid, (VOID *) &HostBridgePeiConfig);
  ASSERT_EFI_ERROR (Status);

  Status = GetConfigBlock ((VOID *) SiPreMemPolicy, &gGraphicsPeiPreMemConfigGuid, (VOID *) &GtPreMemConfig);
  ASSERT_EFI_ERROR (Status);

  Status = GetConfigBlock ((VOID *) SiPolicy, &gGraphicsPeiConfigGuid, (VOID *) &GtConfig);
  ASSERT_EFI_ERROR (Status);

  Status = GetConfigBlock ((VOID *) SiPolicy, &gCpuPowerMgmtVrConfigGuid, (VOID *) &CpuPowerMgmtVrConfig);
  ASSERT_EFI_ERROR(Status);

  Status = GetConfigBlock ((VOID *) SiPreMemPolicy, &gCpuConfigLibPreMemConfigGuid, (VOID *) &CpuConfigLibPreMemConfig);
  ASSERT_EFI_ERROR (Status);

  DEBUG ((DEBUG_INFO, "SiInitOnEndOfPei - Start\n"));
  if (IsFusaSupported()) {
    DEBUG ((DEBUG_INFO, "FUSA override configuration\n"));
    ///
    /// Fusa Configurations
    ///
    IopFusaOverrideProgramming(SiPolicy);
  } else {
    DEBUG ((DEBUG_INFO, "FUSA is not supported\n"));
  }

  #if FixedPcdGet8(PcdEmbeddedEnable) == 0x1
   if (IsFusaSupported() )
   {
     DEBUG ((DEBUG_INFO, "FusaInit Start\n"));
     PostCode (0xA80);
     FusaInit (SiPolicy);
   } else {
     DEBUG ((DEBUG_INFO, "FUSA is not supported\n"));
   }
  #endif

  //
  // Inform Gfx MemConfig
  //
  InformGfxMemConfig ();
  //
  // Execute before P2SB lock to ensure hiding trace hub thru PSF is valid.
  //
  TraceHubConfigEndOfPei ();
  //
  // Initializes PCH after End of Pei
  // Locks PSF and P2SB
  //
  PchOnEndOfPei ();

  DEBUG ((DEBUG_INFO, "SubsystemID programming on Pch Pcie rootports\n"));
  SiPcieProgramSsid (SiPolicy);
  //
  // Enable IOMT tool in manufacture mode.
  // This must be done before SetBiosDone.
  //
  HsioBeforeSaiPostBoot ();

  Status = PeiServicesGetBootMode (&BootMode);
  ASSERT_EFI_ERROR (Status);

#if FixedPcdGet8(PcdEmbeddedEnable) == 0x1
  if ((BootMode == BOOT_ON_FLASH_UPDATE) && (SiConfig->SkipBiosDoneWhenFwUpdate)) {
    DEBUG ((DEBUG_INFO, "Skip BIOS_DONE when updating flash.\n"));
  } else {
#endif
    //
    // Do necessary PCH configuration for Unsupported Transaction Policy
    //
    PchDmiConfigUnsupportedTransactionPolicy ();
    if (CpuConfig->SkipMpInit == 0) {
      //
      // Set BIOS DONE MSR on all Cores
      //
        SetBiosDone ();
    }
#if FixedPcdGet8(PcdEmbeddedEnable) == 0x1
  }
#endif

  //
  // Program the BIOS Parms needed for MCHECK
  //
  ProgramBiosParamsForMcheck ();

  CpuInitAtEndOfPei ();

  //
  // Vmax Voltage Workaround for IA domain
  // This should be executed before signalling RESET_CPL
  //
  if (CpuConfigLibPreMemConfig->ActiveSmallCoreCount != 0) {
    OverrideVrVoltageLimitWa (CpuPowerMgmtVrConfig->VrVoltageLimit[0]);
  }

  //
  // Workarounds for the VR related ones
  //
  VrOverrideWorkaroundFru (CpuPowerMgmtVrConfig->ImonSlope[0]);

  //
  // Update the default Slow Slew Rate to Fast/2 for Desktop and Mobile segments even when Acoustic Noise is disabled
  //
  if (((GetCpuSku () == EnumCpuTrad) || (GetCpuSku () ==  EnumCpuUlt)) && (CpuPowerMgmtVrConfig->AcousticNoiseMitigation == 0)) {
    SetIaSlowSlewRate ();
  }

  ///
  /// Set SAPMCTL Register.
  ///
  SetSaPmCtlReg ();
  //
  // Set BIOS_RESET_CPL to indicate BIOS initialization completed
  //
  PERF_START_EX (&gPerfSaResetPostMemGuid, NULL, NULL, AsmReadTsc (), 0x4090);

  ///
  /// Set BIOS_RESET_CPL
  ///
  SetBiosResetCpl ();


  ///
  /// Graphics PM initialization after BIOS_RESET_CPL
  ///
  DEBUG ((DEBUG_INFO, "GraphicsPmInit Start\n"));
  PostCode (0xA63);
  GraphicsPmInit (GtPreMemConfig, GtConfig);

  ///
  /// Enable DMA buffer for IGD VT-d
  ///
  DEBUG ((DEBUG_INFO, "Enabling DMA buffer for IGD VT-d\n"));
  VtdEnableDmaBuffer (IGD_VTD);

  ///
  /// Initialize PEI Display
  ///
  DEBUG ((DEBUG_INFO, "Initializing Pei Display\n"));
  PostCode (0xA03);
  PeiDisplayInit (GtPreMemConfig, GtConfig);

  ///
  /// Initialize full CD Clock
  ///
  DEBUG ((DEBUG_INFO, "Initializing CD Clock\n"));
  PostCode (0xA65);
  CdClkInit (GtConfig, GtPreMemConfig);
  PeiServicesInstallPpi (&mIDispCodecReadyPpi);

  ///
  /// Print SA PCI space in Debug log.
  ///
  DEBUG ((DEBUG_INFO, "SaPciPrint Start\n"));
  PostCode (0xA64);
  SaPciPrint ();

  PERF_END_EX (&gPerfSaResetPostMemGuid, NULL, NULL, AsmReadTsc (), 0x4091);

  //
  // Initialize power management after RESET_CPL at post-memory phase.
  //
  if (CpuConfig->SkipMpInit == 0) {
    PERF_START_EX (&gPerfCpuPowerMgmtGuid, NULL, NULL, AsmReadTsc (), 0x40A0);
    CpuPowerMgmtInit ();
    PERF_END_EX (&gPerfCpuPowerMgmtGuid, NULL, NULL, AsmReadTsc (), 0x40A1);
  }

  SaOnEndOfPei ();

  //
  // Build FVI Info HOB in normal boot
  //
  if (BootMode != BOOT_ON_S3_RESUME) {
    BuildFviInfoHob ();
  }

  InitializeSmbiosCpuHobs ();

  if (IsPchP () || IsPchN ()) {
    PmcSendBiosResetCompletionMemCalDone ();
  }

  DEBUG ((DEBUG_INFO, "SiInitOnEndOfPei - End\n"));
  return EFI_SUCCESS;
}

/**
  Get HybridGraphics subsystem ID.

  @retval  HybridGraphics subsystem ID
**/
STATIC
UINT16
GetHgSsid (
  VOID
  )
{
  EFI_STATUS                    Status;
  SI_PREMEM_POLICY_PPI          *SiPreMemPolicyPpi;
  HYBRID_GRAPHICS_CONFIG        *HgGpioData;
  UINT16                        HgSubSystemId;

  SiPreMemPolicyPpi = NULL;
  HgGpioData = NULL;
  HgSubSystemId = 0;

  Status = PeiServicesLocatePpi (
             &gSiPreMemPolicyPpiGuid,
             0,
             NULL,
             (VOID **) &SiPreMemPolicyPpi
             );

  if (!EFI_ERROR (Status) && (SiPreMemPolicyPpi != NULL)) {
    Status = GetConfigBlock ((VOID *) SiPreMemPolicyPpi, &gHybridGraphicsConfigGuid, (VOID *) &HgGpioData);
    if (HgGpioData != NULL) {
      HgSubSystemId = HgGpioData->HgSubSystemId;
    }
  }
  return HgSubSystemId;
}

/**
  Override HG SVID and SSID

  @param[in]      PciDevNum       Pci device number
  @param[in]      PciFuncNum      Pci function number
  @param[in,out]  Svid            Svid value
  @param[in,out]  Ssid            Ssid value

  @retval         TRUE            Silicon overrides the SSID
  @retval         FALSE           Silicon doesn't override the SSID
**/
STATIC
BOOLEAN
HgSsidOverride (
  UINT32                 PciDevNum,
  UINT32                 PciFuncNum,
  UINT16                 *Svid,
  UINT16                 *Ssid
  )
{
  UINT16    HgSsid;

  if ((PciDevNum == IGD_DEV_NUM) && (PciFuncNum == IGD_FUN_NUM)) {
    HgSsid = GetHgSsid ();
    if (HgSsid != 0) {
      *Ssid = HgSsid;
      return TRUE;
    }
  }
  return FALSE;
}

/**
  Initializes PCH_UPSTREAM_COMPONENT_CONFIG based on the CPU connected to the system.

  @param[in, out] PchUpstreamComponentConfig  Pch upstream component config
**/
VOID
SiInitPchUpstreamComponentConfig (
  IN OUT PCH_UPSTREAM_COMPONENT_CONFIG  *PchUpstreamComponentConfig
  )
{
  ZeroMem (PchUpstreamComponentConfig, sizeof (PCH_UPSTREAM_COMPONENT_CONFIG));

  //
  // Snoop LTR requierment is set to 75us per requierments from BWG.
  //
  PchUpstreamComponentConfig->SnoopLtrReqUs = 0x4A;
}

/**
  Slicon Initializes after PostMem phase Policy PPI produced,
  All required polices must be installed before the callback

  @param[in] PeiServices          General purpose services available to every PEIM.
  @param[in] NotifyDescriptor     The notification structure this PEIM registered on install.
  @param[in] Ppi                  SiPolicy PPI.

  @retval EFI_SUCCESS             Succeeds.
  @retval EFI_UNSUPPORTED         The function failed to locate SiPolicy
**/
EFI_STATUS
EFIAPI
SiInitPostMemOnPolicy (
  IN  EFI_PEI_SERVICES             **PeiServices,
  IN  EFI_PEI_NOTIFY_DESCRIPTOR    *NotifyDescriptor,
  IN  VOID                         *Ppi
  )
{
  SI_POLICY_PPI                 *SiPolicy;
  SI_PREMEM_POLICY_PPI          *SiPreMemPolicyPpi;
  VOID                          *HobPtr;
  EFI_STATUS                    Status;
  CPU_CONFIG                    *CpuConfig;
  SI_CONFIG                     *SiConfig;
  SA_MISC_PEI_CONFIG            *MiscPeiConfig;
  HOST_BRIDGE_PEI_CONFIG        *HostBridgePeiConfig;
#if FixedPcdGetBool(PcdCpuPcieEnable) == 1
  PCIE_PEI_PREMEM_CONFIG        *PciePeiPreMemConfig;
  CPU_PCIE_CONFIG               *CpuPcieRpConfig;
#endif
  GRAPHICS_PEI_PREMEM_CONFIG     *GtPreMemConfig;
  GRAPHICS_PEI_CONFIG            *GtConfig;
  CPU_TRACE_HUB_PREMEM_CONFIG    *CpuTraceHubPreMemConfig;
  VTD_CONFIG                     *Vtd;
  GNA_CONFIG                     *GnaConfig;
  MEMORY_CONFIGURATION           *MemConfig;
  VMD_PEI_CONFIG                 *VmdPeiConfig;
  TELEMETRY_PEI_CONFIG           *TelemetryPeiConfig;
  CPU_CONFIG_LIB_PREMEM_CONFIG   *CpuConfigLibPreMemConfig;
  BOOLEAN                        ScanForLegacyOpRom;
  SA_MISC_PEI_PREMEM_CONFIG      *MiscPeiPreMemConfig;
  DISPLAY_DEVICE                 PrimaryDisplay;
  UINT32                         PegMmioLength;
  BOOLEAN                        TempFoundLegacyOpRom;
  SA_DATA_HOB                    *SaDataHob;
  OVERCLOCKING_PREMEM_CONFIG     *OverClockingConfig;
  EDKII_PEI_MP_SERVICES2_PPI     *MpServices2Ppi;
  PCH_UPSTREAM_COMPONENT_CONFIG  PchUpstreamComponentConfig;

  DEBUG ((DEBUG_INFO, "SiInit () - Start\n"));

  SiPreMemPolicyPpi   = NULL;
  SiPolicy            = NULL;
  HostBridgePeiConfig = NULL;
  MiscPeiPreMemConfig = NULL;
  OverClockingConfig  = NULL;
  MpServices2Ppi      = NULL;

  Status = PeiServicesLocatePpi (
             &gSiPreMemPolicyPpiGuid,
             0,
             NULL,
             (VOID **)&SiPreMemPolicyPpi
             );
  ASSERT_EFI_ERROR (Status);

  Status = PeiServicesLocatePpi (
             &gSiPolicyPpiGuid,
             0,
             NULL,
             (VOID **)&SiPolicy
             );
  ASSERT_EFI_ERROR (Status);
  if ((Status != EFI_SUCCESS) || (SiPolicy == NULL)) {
    return EFI_UNSUPPORTED;
  }

  Status = PeiSiPolicyPrint ();
  ASSERT_EFI_ERROR (Status);

  Status = GetConfigBlock ((VOID *) SiPolicy, &gCpuConfigGuid, (VOID *) &CpuConfig);
  ASSERT_EFI_ERROR(Status);

  if (CpuConfig->SkipMpInit == 0) {
    ///
    /// Locate CpuMpCpu MpService Ppi
    ///
    Status = PeiServicesLocatePpi (
               &gEdkiiPeiMpServices2PpiGuid,
               0,
               NULL,
               (VOID **) &MpServices2Ppi
               );
    ASSERT_EFI_ERROR (Status);
  }

  Status = GetConfigBlock ((VOID *) SiPolicy, &gSiConfigGuid, (VOID *) &SiConfig);
  ASSERT_EFI_ERROR(Status);

  Status = GetConfigBlock ((VOID *) SiPreMemPolicyPpi, &gSaMiscPeiPreMemConfigGuid, (VOID *) &MiscPeiPreMemConfig);
  ASSERT_EFI_ERROR(Status);

  SaDataHob = (SA_DATA_HOB *)GetFirstGuidHob (&gSaDataHobGuid);
  PrimaryDisplay = SaDataHob->PrimaryDisplay;

  Status = GetConfigBlock ((VOID *) SiPolicy, &gSaMiscPeiConfigGuid, (VOID *) &MiscPeiConfig);
  ASSERT_EFI_ERROR (Status);

  Status = GetConfigBlock ((VOID *) SiPolicy, &gHostBridgePeiConfigGuid, (VOID *) &HostBridgePeiConfig);
  ASSERT_EFI_ERROR (Status);

#if FixedPcdGetBool(PcdCpuPcieEnable) == 1
  Status = GetConfigBlock ((VOID *) SiPreMemPolicyPpi, &gCpuPciePeiPreMemConfigGuid, (VOID *) &PciePeiPreMemConfig);
  ASSERT_EFI_ERROR (Status);
#endif

  Status = GetConfigBlock ((VOID *) SiPreMemPolicyPpi, &gVtdConfigGuid, (VOID *) &Vtd);
  ASSERT_EFI_ERROR (Status);

  Status = GetConfigBlock ((VOID *) SiPreMemPolicyPpi, &gGraphicsPeiPreMemConfigGuid, (VOID *) &GtPreMemConfig);
  ASSERT_EFI_ERROR (Status);

  Status = GetConfigBlock ((VOID *) SiPolicy, &gGraphicsPeiConfigGuid, (VOID *) &GtConfig);
  ASSERT_EFI_ERROR (Status);

  Status = GetConfigBlock ((VOID *) SiPolicy, &gGnaConfigGuid, (VOID *) &GnaConfig);
  ASSERT_EFI_ERROR (Status);

  Status = GetConfigBlock ((VOID *) SiPreMemPolicyPpi, &gMemoryConfigGuid, (VOID *) &MemConfig);
  ASSERT_EFI_ERROR (Status);

  Status = GetConfigBlock ((VOID *) SiPreMemPolicyPpi, &gCpuTraceHubPreMemConfigGuid, (VOID *) &CpuTraceHubPreMemConfig);
  ASSERT_EFI_ERROR (Status);

  VmdPeiConfig = NULL;
#if FixedPcdGetBool(PcdVmdEnable) == 1
  Status = GetConfigBlock ((VOID *) SiPolicy, &gVmdPeiConfigGuid, (VOID *) &VmdPeiConfig);
  ASSERT_EFI_ERROR (Status);
#endif

  Status = GetConfigBlock ((VOID *) SiPolicy, &gTelemetryPeiConfigGuid, (VOID *) &TelemetryPeiConfig);
  ASSERT_EFI_ERROR (Status);


#if FixedPcdGetBool(PcdCpuPcieEnable) == 1
  Status = GetConfigBlock ((VOID *) SiPolicy, &gCpuPcieRpConfigGuid, (VOID *)&CpuPcieRpConfig);
  ASSERT_EFI_ERROR(Status);
#endif

  Status = GetConfigBlock ((VOID *) SiPreMemPolicyPpi, &gCpuConfigLibPreMemConfigGuid, (VOID *) &CpuConfigLibPreMemConfig);
  ASSERT_EFI_ERROR (Status);

  if (CpuConfig->SkipMpInit == 0) {
    Status = GetConfigBlock ((VOID *) SiPreMemPolicyPpi, &gOverclockingPreMemConfigGuid, (VOID *) &OverClockingConfig);
    ASSERT_EFI_ERROR (Status);
  }

  //
  // PSMI Configuration
  //
  ConfigurePsmi ();

  //
  // Cross IP Policy override for specific feature enabling
  //
  PeiSiPolicyOverride (SiPreMemPolicyPpi, SiPolicy);

  HobPtr = BuildGuidDataHob (&gSiConfigHobGuid, SiConfig, sizeof (SI_CONFIG));
  ASSERT (HobPtr != 0);

  ///
  /// SubsystemID programming on Internal Devices.
  ///
  DEBUG((DEBUG_INFO, "SubsystemID programming on Internal Devices\n"));
  SiProgramSsid (SiPolicy, HgSsidOverride);

  //
  // Perform ME post mem init
  // Call before PchInit to have MbpHob data ready.
  //
  PERF_START_EX (&gPerfMePostMemGuid, NULL, NULL, AsmReadTsc (), 0x40B0);
  MePostMemInit (SiPolicy);
  PERF_END_EX (&gPerfMePostMemGuid, NULL, NULL, AsmReadTsc (), 0x40B1);

  //
  // Initializes PCH after memory services initialized
  //
  PERF_START_EX (&gPerfPchPostMemGuid, NULL, NULL, AsmReadTsc (), 0x4020);
  SiInitPchUpstreamComponentConfig (&PchUpstreamComponentConfig);
  PchInit (SiPolicy, &PchUpstreamComponentConfig);
  PERF_END_EX (&gPerfPchPostMemGuid, NULL, NULL, AsmReadTsc (), 0x4021);

  //
  // SA Post Mem initialization
  //
  PERF_START_EX (&gPerfSaPostMemGuid, NULL, NULL, AsmReadTsc (), 0x4030);

  ///
  /// SA device configuration
  ///
  DEBUG ((DEBUG_INFO, "DeviceConfigure Start\n"));
  PostCode (0xA01);
  DeviceConfigure (HostBridgePeiConfig, GnaConfig);

  ///
  /// SA Register Mirror
  ///
  DEBUG ((DEBUG_INFO, "Sa Register Mirror Start\n"));
  SaRegisterMirror (GtPreMemConfig);

  ///
  /// Install PPI to enable Gfx PEIM
  ///
  if (GtConfig->SkipFspGop == 0) {
    if ((GtConfig->PeiGraphicsPeimInit == 1) && (GtConfig->GraphicsConfigPtr != NULL)) {
      Status = PeiServicesInstallPpi (&mEnablePeiGraphicsPpi);
      ASSERT_EFI_ERROR (Status);
    }
  }

  ///
  /// Initialize SA GNA Device
  ///
  DEBUG ((DEBUG_INFO, "Initializing SA GNA device\n"));
  PostCode (0xA16);
  GnaInit (GnaConfig);

  if (IsPchLinkDmi ()){
    DEBUG ((DEBUG_INFO, "CpuDmi16SpeedChange Start\n"));
    Status = CpuDmi16SpeedChange();
  }
  ///
  /// PciExpress PostMem Initialization
  ///
  DEBUG((DEBUG_INFO, "PciExpress PostMem Initialization\n"));
  PostCode(0xA20);
//[-start-220118-QINGLIN0147-add]//
#if defined(S370_SUPPORT)
{ //LfcEcLibStopWDT
  UINT8 Data8 = 0;
  Data8 = MmioRead8(0xFE0B0422);
  Data8 |= BIT4;//Set Bit4 = 1
  MmioWrite8(0xFE0B0422, Data8);
}
#endif
//[-end-220118-QINGLIN0147-add]//
  CpuPcieRpInit (SiPolicy);
//[-start-220118-QINGLIN0147-add]//
#if defined(S370_SUPPORT)
{ //LfcEcLibStartWDT()
  UINT8 Data8 = 0;
  Data8 = MmioRead8(0xFE0B0422);
  Data8 &= ~BIT4;//Clear Bit4 = 0
  MmioWrite8(0xFE0B0422, Data8);
}
#endif
//[-end-220118-QINGLIN0147-add]//

  if (MiscPeiPreMemConfig != NULL) {
    if (MiscPeiPreMemConfig->SkipExtGfxScan == DISABLED) {
      if (MiscPeiPreMemConfig->ScanExtGfxForLegacyOpRom == 0) {
        ScanForLegacyOpRom = FALSE;
      } else {
        ScanForLegacyOpRom = TRUE;
      }

      PegMmioLength         = 0;
      TempFoundLegacyOpRom  = FALSE;
      DEBUG ((DEBUG_INFO, "PrimaryDisplay %x\n", PrimaryDisplay));
      PostCode (0xA44);
      CheckAndInitializePegVga (
        &PrimaryDisplay,
        GtPreMemConfig,
        &PegMmioLength,
        MiscPeiPreMemConfig->OpRomScanTempMmioBar,
        MiscPeiPreMemConfig->OpRomScanTempMmioLimit,
        ScanForLegacyOpRom,
        &TempFoundLegacyOpRom
        );
    }
  }

  ///
  /// Program and lock graphic control register
  ///
  UpdateAndLockGgcReg (&PrimaryDisplay, GtPreMemConfig);

  ///
  /// Program DPR lock and EPM bit
  ///
  UpdateDpr ();

  //
  // Configure Hybrid Storage devices if present on the platform
  //
#if FixedPcdGetBool (PcdHybridStorageSupport) == 1
  HybridStorageDynamicDetectionAndConfig();
#endif

  ///
  /// CPU Trace Hub Initialization
  ///
  DEBUG ((DEBUG_INFO, "Initializing ConfigureCpuTraceHub\n"));
  PostCode (0xA22);
  ConfigureCpuTraceHub (CpuTraceHubPreMemConfig->TraceHub.EnableMode);

  ///
  /// Initializing TCSS Devices
  ///
  if (IsTcssSupported ()) {
    DEBUG ((DEBUG_INFO, "Initializing TCSS\n"));
    PostCode (0xA31);
    SiInitTcss (SiPolicy, SiPreMemPolicyPpi);
  }

  ///
  /// Update HostBridge Hob in PostMem
  ///
  UpdateHostBridgeHobPostMem (HostBridgePeiConfig);

  ///
  /// Update CpuPcie Hob in PostMem
  ///
#if FixedPcdGetBool(PcdCpuPcieEnable) == 1
  UpdateCpuPcieHobPostMem (PciePeiPreMemConfig, CpuPcieRpConfig);
#endif

  ///
  /// Update SA HOBs in PostMem
  ///
  DEBUG ((DEBUG_INFO, "UpdateSaHobPostMem Start\n"));
  PostCode (0xA02);
  UpdateSaHobPostMem (GtPreMemConfig
  );
  DEBUG ((DEBUG_INFO, "PostMem SA Data HOB updated\n"));

  ///
  /// VMD Initializations if the VMD IP is Supported
  ///
  DEBUG ((DEBUG_INFO, "Initializing VMD\n"));
  PostCode (0xA33);
  VmdInit(VmdPeiConfig);

  ///
  /// PAVP Initialization
  ///
  DEBUG ((DEBUG_INFO, "Initializing Pavp\n"));
  PostCode (0xA32);
  PavpInit (GtConfig, MiscPeiConfig, GtPreMemConfig);

  ///
  /// Program PSMI Registers
  ///
  ProgramPsmiRegs (GtConfig, GtPreMemConfig);

  ///
  /// Configure CPU CrashLog
  ///
  if ((!TelemetryPeiConfig->CpuCrashLogEnable)) {
    DEBUG ((DEBUG_INFO, "Disable CpuCrashLog\n"));
    PostCode (0xA35);
    CpuCrashLogDisable ();
  }
  ///
  /// Program Edram Mode
  ///
  if (MsrIsEdramEnable ()) {
    DEBUG ((DEBUG_INFO, "ProgramEdramMode Start\n"));
    PostCode (0xA36);
    ProgramEdramMode (HostBridgePeiConfig);
  }

  PERF_END_EX (&gPerfSaPostMemGuid, NULL, NULL, AsmReadTsc (), 0x4031);

  //
  //  IPU Initilization
  //
  DEBUG ((DEBUG_INFO, "Initializing IPU device\n"));
  PostCode (0xA14);
  IpuInit();

  if (CpuConfig->SkipMpInit == 0) {
    //
    // Overclocking Post memory Initialize.
    //
    PERF_START_EX (&gPerfCpuPostMemGuid, NULL, NULL, AsmReadTsc (), 0x407E);
    Status = CpuOcInitPostMem (OverClockingConfig, MpServices2Ppi);
    ASSERT_EFI_ERROR (Status);
    PERF_END_EX (&gPerfCpuPostMemGuid, NULL, NULL, AsmReadTsc (), 0x407F);
  }

  //
  // Initialize processor features, performance and power management features,
  // BIOS GUARD, and overclocking etc features before RESET_CPL at post-memory phase.
  //
  PERF_START_EX (&gPerfCpuPostMemGuid, NULL, NULL, AsmReadTsc (), 0x4080);
  CpuInit (SiPolicy);
  PERF_END_EX (&gPerfCpuPostMemGuid, NULL, NULL, AsmReadTsc (), 0x4081);

  DEBUG ((DEBUG_INFO, "ConfigureSvidVrs Start \n"));
  PostCode (0xC23);

  ///
  /// Program platform power and SVID VR's
  ///
  ConfigureSvidVrs (SiPolicy, (UINT16) CpuConfigLibPreMemConfig->ConfigTdpLevel);

  //
  // Perform AMT post mem init
  //
  PERF_START_EX (&gPerfAmtPostMemGuid, NULL, NULL, AsmReadTsc (), 0x40C0);
  AmtPostMemInit ();
  PERF_END_EX (&gPerfAmtPostMemGuid, NULL, NULL, AsmReadTsc (), 0x40C1);

  ///
  /// Update Vtd Hob in PostMem
  ///
  UpdateVtdHobPostMem (Vtd);

  //
  // SA Security Lock down after all initialization done
  //
  PERF_START_EX (&gPerfSaSecLockPostMemGuid, NULL, NULL, AsmReadTsc (), 0x4050);
  SaSecurityLock ();
  PERF_END_EX (&gPerfSaSecLockPostMemGuid, NULL, NULL, AsmReadTsc (), 0x4051);

  //
  // Install EndOfPei callback function.
  //
#ifdef FSP_FLAG
  if (GetFspGlobalDataPointer()->FspMode == FSP_IN_DISPATCH_MODE) {
    CopyGuid (mSiInitNotifyList->Guid, &gEfiEndOfPeiSignal2PpiGuid);
  }
#endif

  Status = PeiServicesNotifyPpi (mSiInitNotifyList);
  ASSERT_EFI_ERROR (Status);

  //
  // End of SiInit notification event
  //
#ifndef FSP_FLAG
  Status = PeiServicesInstallPpi (&mEndOfSiInit);
  ASSERT_EFI_ERROR (Status);
#else
  if (GetFspGlobalDataPointer()->FspMode == FSP_IN_DISPATCH_MODE) {
    Status = PeiServicesInstallPpi (&mEndOfSiInit);
    ASSERT_EFI_ERROR (Status);
  }
#endif

  DEBUG ((DEBUG_INFO, "SiInit () - End\n"));

  return EFI_SUCCESS;
}

/**
  Silicon Initializes after memory services initialized

  @param[in] FileHandle           The file handle of the file, Not used.
  @param[in] PeiServices          General purpose services available to every PEIM.

  @retval EFI_SUCCESS             The function completes successfully
**/
EFI_STATUS
EFIAPI
SiInit (
  IN  EFI_PEI_FILE_HANDLE               FileHandle,
  IN CONST EFI_PEI_SERVICES             **PeiServices
  )
{
  EFI_STATUS                Status;

  //
  // Install PostMem phase OnPolicyInstalled callback function.
  //
  Status = PeiServicesNotifyPpi (mSiInitPostMemNotifyList);
  ASSERT_EFI_ERROR (Status);

  //
  // Install a Default Policy initialization PPI
  //
  Status = PeiServicesInstallPpi (mPeiSiDefaultPolicyInitPpiList);
  ASSERT_EFI_ERROR (Status);

  //
  // Install the Pei Silicon Policy print PPI
  //
  Status = PeiServicesInstallPpi (&mPeiSiPolicyPrintPpiList);
  ASSERT_EFI_ERROR (Status);

  return Status;
}
