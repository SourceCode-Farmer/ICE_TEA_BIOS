/** @file
  Source code file for Silicon Init Pre Memory module.

@copyright
  INTEL CONFIDENTIAL
  Copyright 2013 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains an 'Intel Peripheral Driver' and is uniquely identified as
  "Intel Reference Module" and is licensed for Intel CPUs and chipsets under
  the terms of your license agreement with Intel or your vendor. This file may
  be modified by the user, subject to additional terms of the license agreement.

@par Specification
**/
#include "SiInitPreMem.h"
#include <Library/PerformanceLib.h>
#include <Library/PeiSiPolicyOverrideLib.h>
#include <Library/SiPolicyLib.h>
#include <Library/MemoryAllocationLib.h>
#include <Ppi/PeiPreMemSiDefaultPolicy.h>
#include <Library/PostCodeLib.h>
#include <Library/PeiHostBridgeInitLib.h>
#include <SaDataHob.h>
#include <SaConfigHob.h>
#include <Library/TcssInitLib.h>
#include <Library/CpuDmiInfoLib.h>
#include <Library/CpuPlatformLib.h>
#include <Library/PeiCpuPsfInitFruLib.h>
#include <Library/PeiCpuPcieVgaInitLib.h>
#include <Library/PeiCpuDmiInitLib.h>
#include <Library/PeiHybridGraphicsInitLib.h>
#include <Library/PeiCpuPciePreMemRpInitLib.h>
#include <Library/PeiGraphicsInitLib.h>
#include <Library/PeiDisplayInitLib.h>
#include <Library/PeiVtdInitLib.h>
#include <Library/TelemetryPrivateLib.h>
#include <Library/PeiOcLib.h>
#include <Library/PreSiliconEnvDetectLib.h>
#include <Library/GraphicsInfoLib.h>
#include <Library/HostBridgeInfoLib.h>
#include <Library/PeiMeLib.h>
#include <Library/PeiDisplayInitFruLib.h>
#include <Library/PeiTcssInitFruLib.h>
#include <Library/PeiSaInitFruLib.h>
#include <Ppi/PeiPreMemSiPolicyPrint.h>
static EFI_PEI_NOTIFY_DESCRIPTOR  mSiInitNotifyList[] = {
  {
    EFI_PEI_PPI_DESCRIPTOR_NOTIFY_DISPATCH | EFI_PEI_PPI_DESCRIPTOR_TERMINATE_LIST,
    &gSiPreMemPolicyReadyPpiGuid,
    SiInitPreMemOnPolicy
  }
};

FSP_TEMP_RAM_EXIT_PPI mTempRamExitPpi = {
  TempRamExit
};

static EFI_PEI_PPI_DESCRIPTOR  mTempRamExitPpiList[] = {
  {
    EFI_PEI_PPI_DESCRIPTOR_PPI | EFI_PEI_PPI_DESCRIPTOR_TERMINATE_LIST,
    &gFspTempRamExitPpiGuid,
    &mTempRamExitPpi
  }
};

static EFI_PEI_PPI_DESCRIPTOR mPpiPeiBeforeGraphicsDetection = {
  (EFI_PEI_PPI_DESCRIPTOR_PPI | EFI_PEI_PPI_DESCRIPTOR_TERMINATE_LIST),
  &gPeiBeforeGraphicsDetectionPpiGuid,
  NULL
};

STATIC PEI_PREMEM_SI_POLICY_PRINT_PPI mPeiPreMemSiPolicyPrintPpi = {
  PeiPreMemSiPolicyPrint
};

STATIC EFI_PEI_PPI_DESCRIPTOR mPeiPreMemSiPolicyPrintPpiList = {
  EFI_PEI_PPI_DESCRIPTOR_PPI | EFI_PEI_PPI_DESCRIPTOR_TERMINATE_LIST,
  &gSiPreMemPolicyPrintPpiGuid,
  &mPeiPreMemSiPolicyPrintPpi
};

/**
  Slicon Initializes after Policy PPI produced, All required polices must be installed before the callback

  @param[in] PeiServices          General purpose services available to every PEIM.
  @param[in] NotifyDescriptor     The notification structure this PEIM registered on install.
  @param[in] Ppi                  The memory discovered PPI.  Not used.

  @retval EFI_SUCCESS             Succeeds.
**/
EFI_STATUS
EFIAPI
SiInitPreMemOnPolicy (
  IN  EFI_PEI_SERVICES             **PeiServices,
  IN  EFI_PEI_NOTIFY_DESCRIPTOR    *NotifyDescriptor,
  IN  VOID                         *Ppi
  )
{
  EFI_STATUS                    Status;
  SI_POLICY_PPI                 *SiPolicyPpi;
  SI_PREMEM_POLICY_PPI          *SiPreMemPolicyPpi;
  HOST_BRIDGE_PREMEM_CONFIG     *HostBridgePreMemConfig;
  HOST_BRIDGE_DATA_HOB          *HostBridgeDataHob;
  SA_MISC_PEI_PREMEM_CONFIG     *MiscPeiPreMemConfig;
  SA_DATA_HOB                   *SaDataHob;
  SA_CONFIG_HOB                 *SaConfigHob;
  TCSS_PEI_PREMEM_CONFIG        *TcssPeiPreMemConfig;
#if FixedPcdGetBool(PcdCpuPcieEnable) == 1
  PCIE_PEI_PREMEM_CONFIG        *PciePeiPreMemConfig;
#endif
#if FixedPcdGetBool(PcdHgEnable) == 1
  HYBRID_GRAPHICS_CONFIG        *HgGpioData;
#endif
#if FixedPcdGetBool(PcdCpuPcieEnable) == 1
  CPU_PCIE_RP_PREMEM_CONFIG     *CpuPcieRpPreMemConfig;
#endif
  GRAPHICS_PEI_PREMEM_CONFIG    *GtPreMemConfig;
  DISPLAY_DEVICE                PrimaryDisplay;
  UINT32                        IGfxMmioLength;
  VTD_CONFIG                    *VtdConfig;
  OVERCLOCKING_PREMEM_CONFIG    *OverclockingPreMemConfig;
  TELEMETRY_PEI_PREMEM_CONFIG   *TelemetryPreMemConfig;
  UINT32                        PchPcieMmioLength;
  UINT32                        PegMmioLength;
  BOOLEAN                       ScanForLegacyOpRom;
  BOOLEAN                       TempFoundLegacyOpRom;
  CPU_CONFIG_LIB_PREMEM_CONFIG  *CpuConfigLibPreMemConfig;

  DEBUG ((DEBUG_INFO, "SiInitPreMemOnPolicy() Start\n"));

  SiPolicyPpi = NULL;
  Status = PeiServicesLocatePpi (
             &gSiPolicyPpiGuid,
             0,
             NULL,
             (VOID **)&SiPolicyPpi
             );
  //
  // Prevent from potential execution in PostMem phase
  //
  if (Status == EFI_SUCCESS) {
    return EFI_SUCCESS;
  }

  Status = PeiServicesLocatePpi (
             &gSiPreMemPolicyPpiGuid,
             0,
             NULL,
             (VOID **)&SiPreMemPolicyPpi
             );
  ASSERT_EFI_ERROR (Status);

  Status = PeiPreMemSiPolicyPrint ();
  ASSERT_EFI_ERROR (Status);

  HostBridgePreMemConfig = NULL;
  Status = GetConfigBlock ((VOID *) SiPreMemPolicyPpi, &gHostBridgePeiPreMemConfigGuid, (VOID *) &HostBridgePreMemConfig);
  ASSERT_EFI_ERROR (Status);

  MiscPeiPreMemConfig = NULL;
  Status = GetConfigBlock ((VOID *) SiPreMemPolicyPpi, &gSaMiscPeiPreMemConfigGuid, (VOID *) &MiscPeiPreMemConfig);
  ASSERT_EFI_ERROR (Status);

  TcssPeiPreMemConfig = NULL;
  if (IsTcssSupported ()) {
    Status = GetConfigBlock ((VOID *) SiPreMemPolicyPpi, &gTcssPeiPreMemConfigGuid, (VOID *) &TcssPeiPreMemConfig);
    ASSERT_EFI_ERROR (Status);
  }

#if FixedPcdGetBool(PcdCpuPcieEnable) == 1
  PciePeiPreMemConfig = NULL;
  Status = GetConfigBlock ((VOID *) SiPreMemPolicyPpi, &gCpuPciePeiPreMemConfigGuid, (VOID *) &PciePeiPreMemConfig);
  ASSERT_EFI_ERROR (Status);
#endif

#if FixedPcdGetBool(PcdHgEnable) == 1
  HgGpioData = NULL;
  Status = GetConfigBlock ((VOID *) SiPreMemPolicyPpi, &gHybridGraphicsConfigGuid, (VOID *) &HgGpioData);
  ASSERT_EFI_ERROR (Status);
#endif

#if FixedPcdGetBool(PcdCpuPcieEnable) == 1
  CpuPcieRpPreMemConfig = NULL;
  Status = GetConfigBlock ((VOID *) SiPreMemPolicyPpi, &gCpuPcieRpPrememConfigGuid, (VOID *) &CpuPcieRpPreMemConfig);
  ASSERT_EFI_ERROR(Status);
#endif

  GtPreMemConfig = NULL;
  Status = GetConfigBlock ((VOID *) SiPreMemPolicyPpi, &gGraphicsPeiPreMemConfigGuid, (VOID *) &GtPreMemConfig);
  ASSERT_EFI_ERROR (Status);

  VtdConfig = NULL;
  Status = GetConfigBlock ((VOID *) SiPreMemPolicyPpi, &gVtdConfigGuid, (VOID *) &VtdConfig);
  ASSERT_EFI_ERROR (Status);

  OverclockingPreMemConfig = NULL;
  Status = GetConfigBlock ((VOID *) SiPreMemPolicyPpi, &gOverclockingPreMemConfigGuid, (VOID *) &OverclockingPreMemConfig);
  ASSERT_EFI_ERROR (Status);

  TelemetryPreMemConfig = NULL;
  Status = GetConfigBlock ((VOID *) SiPreMemPolicyPpi, &gTelemetryPeiPreMemConfigGuid, (VOID *) &TelemetryPreMemConfig);
  ASSERT_EFI_ERROR (Status);

  MiscPeiPreMemConfig = NULL;
  Status = GetConfigBlock ((VOID *) SiPreMemPolicyPpi, &gSaMiscPeiPreMemConfigGuid, (VOID *) &MiscPeiPreMemConfig);
  ASSERT_EFI_ERROR (Status);

  CpuConfigLibPreMemConfig = NULL;
  Status = GetConfigBlock ((VOID *) SiPreMemPolicyPpi, &gCpuConfigLibPreMemConfigGuid, (VOID *) &CpuConfigLibPreMemConfig);
  ASSERT_EFI_ERROR (Status);

  PrimaryDisplay = IGD;
  IGfxMmioLength = 0;
  PchPcieMmioLength = 0;
  PegMmioLength = 0;
  TempFoundLegacyOpRom = FALSE;

  //
  // Initialize PSMI handler/trace sizes before MRC
  //
  PsmiInit (SiPreMemPolicyPpi);
  //
  // Initialize ME after Policy PPI produced
  //
  PERF_START_EX (&gPerfHeciPreMemGuid, NULL, NULL, AsmReadTsc (), 0x5060);
  //
  // Install HECI PPI
  // PCH needs to get CRC from HECI message. So it must be ready
  // before Policy callback function of PCH.
  //
  Status = InstallHeciPpi ();
  ASSERT (!EFI_ERROR (Status));
  PeiHeciDevicesInit ();
  PERF_END_EX (&gPerfHeciPreMemGuid, NULL, NULL, AsmReadTsc (), 0x5061);
  //
  // Cross IP Policy override for specific feature enabling
  //
  PeiSiPolicyOverridePreMem (SiPreMemPolicyPpi);

#ifndef MDEPKG_NDEBUG
  //
  // Validate PCH policies
  //
  PERF_START_EX (&gPerfPchValidateGuid, NULL, NULL, AsmReadTsc (), 0x5010);
  PchValidatePolicy (SiPreMemPolicyPpi);
  PERF_END_EX (&gPerfPchValidateGuid, NULL, NULL, AsmReadTsc (), 0x5011);

  //
  // Validate CPU policies
  //
  PERF_START_EX (&gPerfCpuValidateGuid, NULL, NULL, AsmReadTsc(), 0x5030);
  CpuValidatePolicy (SiPreMemPolicyPpi);
  PERF_END_EX (&gPerfCpuValidateGuid, NULL, NULL, AsmReadTsc(), 0x5031);

  //
  // Validate ME policies
  //
  PERF_START_EX (&gPerfMeValidateGuid, NULL, NULL, AsmReadTsc (), 0x5040);
  MeValidatePolicy (SiPreMemPolicyPpi);
  PERF_END_EX (&gPerfMeValidateGuid, NULL, NULL, AsmReadTsc (), 0x5041);

  //
  // Validate SA policies
  //
  PERF_START_EX (&gPerfSaValidateGuid, NULL, NULL, AsmReadTsc (), 0x5050);
  SaValidatePolicy (SiPreMemPolicyPpi);
  PERF_END_EX (&gPerfSaValidateGuid, NULL, NULL, AsmReadTsc (), 0x5051);

#endif // MDEPKG_NDEBUG
  //
  // Initialize PCH after Policy PPI produced
  //
  PERF_START_EX (&gPerfPchPreMemGuid, NULL, NULL, AsmReadTsc (), 0x5070);
  PchOnPolicyInstalled (SiPreMemPolicyPpi);
  PERF_END_EX (&gPerfPchPreMemGuid, NULL, NULL, AsmReadTsc (), 0x5071);

  //
  // Initialize CPU after Policy PPI produced
  //
  PERF_START_EX (&gPerfCpuPreMemGuid, NULL, NULL, AsmReadTsc(), 0x5080);
  CpuOnPolicyInstalled (SiPreMemPolicyPpi);
  PERF_END_EX (&gPerfCpuPreMemGuid, NULL, NULL, AsmReadTsc(), 0x5081);

  ///
  ///
  ///
  /// Init Overclocking
  ///
  PERF_START_EX (&gPerfCpuPreMemGuid, NULL, NULL, AsmReadTsc(), 0x5082);
  CpuOcInitPreMem (SiPreMemPolicyPpi);
  PERF_END_EX (&gPerfCpuPreMemGuid, NULL, NULL, AsmReadTsc(), 0x5083);

  //
  // Initialize ME after Policy PPI produced
  //
  PERF_START_EX (&gPerfMePreMemGuid, NULL, NULL, AsmReadTsc (), 0x5090);
  MeOnPolicyInstalled ();
  PERF_END_EX (&gPerfMePreMemGuid, NULL, NULL, AsmReadTsc (), 0x5091);

  //
  // Initialize SA after Policy PPI produced
  //
  PERF_START_EX (&gPerfSaPreMemGuid, NULL, NULL, AsmReadTsc (), 0x50B0);
  ///
  /// Program Host Bridge Bar Registers
  ///
  DEBUG ((DEBUG_INFO, "Programming Host Bridge Bars\n"));
  PostCode (0xA06);
  ProgramHostBridgeBars ();
  ///
  /// Install Host Bridge Data HOB
  ///
  InstallHostBridgeDataHob (HostBridgePreMemConfig, &HostBridgeDataHob);
  ///
  /// Enable VLW's
  ///
  VlwEnable (HostBridgePreMemConfig);
  ///
  /// Install SA HOBs
  ///
  DEBUG ((DEBUG_INFO, "Install SA HOBs\n"));
  PostCode (0xA08);
  InstallSaHob (MiscPeiPreMemConfig, &SaDataHob, &SaConfigHob);
  ///
  /// Initialize TCSS PreMem
  ///
  PostCode (0xA09);
  TcssPreMemInit (TcssPeiPreMemConfig, MiscPeiPreMemConfig);
  ///
  /// Report SA PCIe code version
  ///
  /// ToDo: need clarify the reason to report silicon verison to scratchpad register.
//  DEBUG ((DEBUG_INFO, "Reporting CPU PCIe code version\n"));
//  PostCode (0xA0A);
//  ReportPcieVersion (HostBridgePreMemConfig);
  if (IsPchLinkDmi ()){
    DEBUG ((DEBUG_INFO, "Initializing DMI\n"));
    PostCode (0xA10);
    CpuDmiInit();
  }
  ///
  /// Initialize DMI/OPI Max PayLoad Size
  ///
  if (IsCpuDmiEnabled()) {
    DEBUG ((DEBUG_INFO, "Initializing DMI/OPI Max PayLoad Size\n"));
    PostCode (0xA1F);
    MaxPayloadSizeInit ();
  }
  ///
  /// Initialize Cpu Psf
  ///
  CpuPsfInit ();
  ///
  /// Install mPpiPeiBeforeGraphicsDetection PPI
  ///
  Status = PeiServicesInstallPpi (&mPpiPeiBeforeGraphicsDetection);
  ASSERT_EFI_ERROR (Status);
  ///
  /// Disable IGD VGA Decode Bits
  ///
  DisableIgdVgaDecodeBits ();
  ///
  /// Initialize HybridGraphics
  ///
  DEBUG ((DEBUG_INFO, "Initializing HybridGraphics\n"));
  PostCode (0xA20);
#if FixedPcdGetBool(PcdHgEnable) == 1
  PeiHybridGraphicsInit (HgGpioData);
#endif
  ///
  /// Initialize CPU PCIe
  ///
  DEBUG ((DEBUG_INFO, "Initializing CPU PCIe\n"));
  PostCode (0xA30);
#if FixedPcdGetBool(PcdCpuPcieEnable) == 1
  PciExpressInit (CpuPcieRpPreMemConfig);
#endif
  ///
  /// Initialize DMI Tc/Vc mapping setting
  ///
  if (IsCpuDmiEnabled()) {
    DEBUG ((DEBUG_INFO, "Initializing DMI Tc/Vc mapping\n"));
    PostCode (0xA40);
    Status = CpuDmiTcVcInit ();
  }
  ///
  /// Graphics Detection (IGD/PEG/PCH)
  ///
  DEBUG ((DEBUG_INFO, "Create Graphics Data Hob\n"));
  CreateGfxDataHob (MiscPeiPreMemConfig);
  DEBUG ((DEBUG_INFO, "Graphics detection\n"));
  if (MiscPeiPreMemConfig->SkipExtGfxScan == DISABLED) {
    if (MiscPeiPreMemConfig->ScanExtGfxForLegacyOpRom == 0) {
      ScanForLegacyOpRom = FALSE;
    } else {
      ScanForLegacyOpRom = TRUE;
    }
    DEBUG ((DEBUG_INFO, "CheckOffboardPcieVga\n"));
    PostCode (0xA42);
    CheckOffboardPcieVga (
      &PchPcieMmioLength,
      &PrimaryDisplay,
      MiscPeiPreMemConfig->OpRomScanTempMmioBar,
      MiscPeiPreMemConfig->OpRomScanTempMmioLimit,
      ScanForLegacyOpRom,
      &TempFoundLegacyOpRom,
      MiscPeiPreMemConfig->ResizableBarSupport
      );
  }

  ///
  /// Update the GFX revision to Scratchpad register
  ///
  UpdateGfxRevisionToScratchpad ();
  ///
  /// Initialize Graphics (IGD)
  ///
  DEBUG ((DEBUG_INFO, "Initializing Graphics\n"));
  PostCode (0xA50);
  GraphicsInit (GtPreMemConfig, &PrimaryDisplay, &IGfxMmioLength);

  ///
  /// Check and Enable Panel Power (Vdd Bit)
  ///
  CheckAndForceVddOn (GtPreMemConfig);

  DEBUG ((DEBUG_INFO, "Initializing Pre-Memory Display \n"));
  PostCode (0xA0B);
  DisplayInitPreMem (GtPreMemConfig);
  ///
  /// Initialize VT-d
  ///
  DEBUG ((DEBUG_INFO, "Initializing Vtd\n"));
  PostCode (0xA51);
  VtdInit (VtdConfig);
  ///
  /// Initialize System Agent Overclocking
  ///
  DEBUG ((DEBUG_INFO, "Initializing System Agent Overclocking\n"));
  PostCode (0xA52);
  SaOcInit (OverclockingPreMemConfig, CpuConfigLibPreMemConfig);
  ///
  /// Enable/Disable CPU CrashLog Device in DEVEN
  ///
  ConfigureCrashLogDevEn ((BOOLEAN) TelemetryPreMemConfig->CpuCrashLogDevice);
  ///
  /// Calculate required MMIO size
  ///
  MmioSizeCalculation (PchPcieMmioLength, PegMmioLength, IGfxMmioLength);
  ///
  /// Program Host Bridge work arounds
  ///
  HostBridgeWorkAround ();
  ///
  /// Program WRC feature
  ///
  ProgramWrcFeatureForIop (MiscPeiPreMemConfig);

  PERF_END_EX (&gPerfSaPreMemGuid, NULL, NULL, AsmReadTsc (), 0x50B1);

  //
  // Register MRC initialization callback when gEfiPeiMasterBootModePpiGuid is installed.
  //
  PERF_START_EX (&gPerfMemGuid, NULL, NULL, AsmReadTsc (), 0x50D0);
  InstallMrcCallback ();
  PERF_END_EX (&gPerfMemGuid, NULL, NULL, AsmReadTsc (), 0x50D1);

  DEBUG ((DEBUG_INFO, "SiInitPreMemOnPolicy() - End\n"));
  return EFI_SUCCESS;
}

/**
  PPI function to install default ConfigBlock Policy PPI.

  @retval EFI_STATUS       - Status from each sub function.
**/
EFI_STATUS
EFIAPI
PeiPreMemSiDefaultPolicyInit (
  VOID
  )
{
  EFI_STATUS                            Status;
  EFI_PEI_PPI_DESCRIPTOR                *SiPolicyPreMemPpiDesc;
  SI_PREMEM_POLICY_PPI                  *SiPreMemPolicyPpi;

  SiPreMemPolicyPpi = NULL;
  Status = SiCreatePreMemConfigBlocks (&SiPreMemPolicyPpi);
  ASSERT_EFI_ERROR (Status);
  if (SiPreMemPolicyPpi != NULL) {

    SiPolicyPreMemPpiDesc = (EFI_PEI_PPI_DESCRIPTOR *) AllocateZeroPool (sizeof (EFI_PEI_PPI_DESCRIPTOR));
    if (SiPolicyPreMemPpiDesc == NULL) {
      ASSERT (FALSE);
      return EFI_OUT_OF_RESOURCES;
    }

    SiPolicyPreMemPpiDesc->Flags = EFI_PEI_PPI_DESCRIPTOR_PPI | EFI_PEI_PPI_DESCRIPTOR_TERMINATE_LIST;
    SiPolicyPreMemPpiDesc->Guid  = &gSiPreMemPolicyPpiGuid;
    SiPolicyPreMemPpiDesc->Ppi   = SiPreMemPolicyPpi;
    //
    // Install Silicon Policy PPI
    //
    Status = PeiServicesInstallPpi (SiPolicyPreMemPpiDesc);
    ASSERT_EFI_ERROR (Status);
  }
  return Status;
}

PEI_PREMEM_SI_DEFAULT_POLICY_INIT_PPI mPeiPreMemSiDefaultPolicyInitPpi = {
  PeiPreMemSiDefaultPolicyInit
};

static EFI_PEI_PPI_DESCRIPTOR  mPeiPreMemSiDefaultPolicyInitPpiList[] = {
  {
    EFI_PEI_PPI_DESCRIPTOR_PPI | EFI_PEI_PPI_DESCRIPTOR_TERMINATE_LIST,
    &gSiPreMemDefaultPolicyInitPpiGuid,
    &mPeiPreMemSiDefaultPolicyInitPpi
  }
};

/**
  Silicon Initializes before Policy PPI produced

  @param[in] FileHandle           The file handle of the file, Not used.
  @param[in] PeiServices          General purpose services available to every PEIM.

  @retval EFI_SUCCESS             The function completes successfully
**/
EFI_STATUS
EFIAPI
SiInitPrePolicy (
  IN  EFI_PEI_FILE_HANDLE      FileHandle,
  IN CONST EFI_PEI_SERVICES  **PeiServices
  )
{
  EFI_STATUS                            Status;

  DEBUG ((DEBUG_INFO, "SiInitPrePolicy() Start\n"));

  //
  // Initializes PCH before Policy initialized
  //
  PERF_START_EX (&gPerfPchPrePolicyGuid, NULL, NULL, AsmReadTsc (), 0x50F0);
  PchInitPrePolicy ();
  PERF_END_EX (&gPerfPchPrePolicyGuid, NULL, NULL, AsmReadTsc (), 0x50F1);

  //
  // Register Silicon init call back after PlatformPolicy PPI produced
  //
  Status = PeiServicesNotifyPpi (mSiInitNotifyList);
  ASSERT_EFI_ERROR (Status);

  //
  // Install Silicon ProgramMtrr PPI
  //
  Status = PeiServicesInstallPpi (mTempRamExitPpiList);
  ASSERT_EFI_ERROR (Status);

  //
  // Install a Default Policy initialization PPI
  //
  Status = PeiServicesInstallPpi (mPeiPreMemSiDefaultPolicyInitPpiList);
  ASSERT_EFI_ERROR (Status);

  //
  // Install the Si PreMem Policy print PPI
  //
  Status = PeiServicesInstallPpi (&mPeiPreMemSiPolicyPrintPpiList);
  ASSERT_EFI_ERROR (Status);

  DEBUG ((DEBUG_INFO, "SiInitPrePolicy() - End\n"));
  return EFI_SUCCESS;
}
