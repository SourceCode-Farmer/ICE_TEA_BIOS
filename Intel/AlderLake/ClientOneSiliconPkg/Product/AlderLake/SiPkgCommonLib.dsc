## @file
#  Component description file for the AlderLake SiPkg both Pei and Dxe libraries DSC file.
#
# @copyright
#  INTEL CONFIDENTIAL
#  Copyright 2016 - 2021 Intel Corporation.
#
#  The source code contained or described herein and all documents related to the
#  source code ("Material") are owned by Intel Corporation or its suppliers or
#  licensors. Title to the Material remains with Intel Corporation or its suppliers
#  and licensors. The Material may contain trade secrets and proprietary and
#  confidential information of Intel Corporation and its suppliers and licensors,
#  and is protected by worldwide copyright and trade secret laws and treaty
#  provisions. No part of the Material may be used, copied, reproduced, modified,
#  published, uploaded, posted, transmitted, distributed, or disclosed in any way
#  without Intel's prior express written permission.
#
#  No license under any patent, copyright, trade secret or other intellectual
#  property right is granted to or conferred upon you by disclosure or delivery
#  of the Materials, either expressly, by implication, inducement, estoppel or
#  otherwise. Any license under such intellectual property rights must be
#  express and approved by Intel in writing.
#
#  Unless otherwise agreed by Intel in writing, you may not remove or alter
#  this notice or any other notice embedded in Materials by Intel or
#  Intel's suppliers or licensors in any way.
#
#  This file contains an 'Intel Peripheral Driver' and is uniquely identified as
#  "Intel Reference Module" and is licensed for Intel CPUs and chipsets under
#  the terms of your license agreement with Intel or your vendor. This file may
#  be modified by the user, subject to additional terms of the license agreement.
#
#@par Specification Reference:
#
##
#
# Silicon Init Common Library
#

#
# Set PCH generation according PCD.
# The DEFINE will be used to select PCH library INF file corresponding to PCH generation
#

!include $(PLATFORM_SI_PACKAGE)/UniversalCommonLib.dsc
#
# FRUs
#
!include $(PLATFORM_SI_PACKAGE)/Fru/AdlCpu/CommonLib.dsc
!include $(PLATFORM_SI_PACKAGE)/Fru/AdlPch/CommonLib.dsc
#
# Common
#
 PreSiliconEnvDetectLib|$(PLATFORM_SI_PACKAGE)/Library/BasePreSiliconEnvDetectLib/BasePreSiliconEnvDetectLib.inf
 MmPciLib|$(PLATFORM_SI_PACKAGE)/Library/PeiDxeSmmMmPciLib/PeiDxeSmmMmPciLib.inf
 PciExpressLib|$(PLATFORM_SI_PACKAGE)/Library/BasePciExpressMultiSegLib/BasePciExpressMultiSegLib.inf
 PciSegmentLib|$(PLATFORM_SI_PACKAGE)/Library/BasePciSegmentMultiSegLibPci/BasePciSegmentMultiSegLibPci.inf
 SystemTimeLib|$(PLATFORM_SI_PACKAGE)/Library/BaseSystemTimeLib/BaseSystemTimeLib.inf
 SiMtrrLib|$(PLATFORM_SI_PACKAGE)/Library/SiMtrrLib/SiMtrrLib.inf
 PeiDxeSmmReserveMmio64SizeLib|$(PLATFORM_SI_PACKAGE)/Library/PeiDxeSmmReserveMmio64SizeLib/PeiDxeSmmReserveMmio64SizeLib.inf

#
# Cpu
#
 SecCpuLib|$(PLATFORM_SI_PACKAGE)/Cpu/Library/SecCpuLib/SecCpuLib.inf
 TxtLib|$(PLATFORM_SI_PACKAGE)/Cpu/Library/PeiDxeTxtLib/PeiDxeTxtLib.inf
 CpuPlatformLib|$(PLATFORM_SI_PACKAGE)/Cpu/Library/PeiDxeSmmCpuPlatformLib/PeiDxeSmmCpuPlatformLib.inf
 PeiVrLib|$(PLATFORM_SI_PACKAGE)/IpBlock/VoltageRegulator/LibraryPrivate/PeiVrLib/PeiVrLib.inf

!if gSiPkgTokenSpaceGuid.PcdBootGuardEnable == TRUE
!if gSiPkgTokenSpaceGuid.PcdBtgTxtLegacyPkgEnable == TRUE
 BootGuardLib|$(PLATFORM_SI_PACKAGE)/Cpu/Library/PeiDxeSmmBootGuardLib/PeiDxeSmmBootGuardLib.inf
!else
BootGuardLib|$(PLATFORM_SI_PACKAGE)/IpBlock/BootGuard/LibraryPrivate/PeiDxeSmmBootGuardLib/PeiDxeSmmBootGuardLib.inf
!endif
!else # gSiPkgTokenSpaceGuid.PcdBootGuardEnable == FALSE
!if gSiPkgTokenSpaceGuid.PcdBtgTxtLegacyPkgEnable == TRUE
 BootGuardLib|$(PLATFORM_SI_PACKAGE)/Cpu/Library/PeiDxeSmmBootGuardLibNull/PeiDxeSmmBootGuardLibNull.inf
 !else
 BootGuardLib|$(PLATFORM_SI_PACKAGE)/IpBlock/BootGuard/LibraryPrivate/PeiDxeSmmBootGuardLibNull/PeiDxeSmmBootGuardLibNull.inf
!endif
!endif

 CpuMailboxLib|$(PLATFORM_SI_PACKAGE)/Library/PeiDxeSmmCpuMailboxLib/PeiDxeSmmCpuMailboxLib.inf

#
# Pch
#
 SerialIoUartDebugPropertyPcdLib|$(PLATFORM_SI_PACKAGE)/IpBlock/SerialIo/Uart/Library/SerialIoUartDebugPropertyPcdLib/SerialIoUartDebugPropertyPcdLib.inf
 SecPchLib|$(PLATFORM_SI_PACKAGE)/Pch/Library/SecPchLib/SecPchLib.inf
 PchCycleDecodingLib|$(PLATFORM_SI_PACKAGE)/Pch/Library/PeiDxeSmmPchCycleDecodingLib/PeiDxeSmmPchCycleDecodingLib.inf
 CpuPcieInitCommonLib|$(PLATFORM_SI_PACKAGE)/IpBlock/CpuPcieRp/Library/PeiDxeSmmCpuPcieInitCommonLib/PeiDxeSmmCpuPcieInitCommonLib.inf
 CpuPcieRpLib|$(PLATFORM_SI_PACKAGE)/IpBlock/CpuPcieRp/Library/PeiDxeSmmCpuPcieRpLib/PeiDxeSmmCpuPcieRpLib.inf
CpuSbiAccessLib|$(PLATFORM_SI_PACKAGE)/IpBlock/P2sb/Library/PeiDxeSmmCpuSbiAccessLib/PeiDxeSmmCpuSbiAccessLib.inf
!if gSiPkgTokenSpaceGuid.PcdSerialIoUartEnable == TRUE
 SerialIoUartLib|$(PLATFORM_SI_PACKAGE)/IpBlock/SerialIo/Uart/Library/PeiDxeSmmSerialIoUartLib/PeiDxeSmmSerialIoUartLib.inf
!else
 SerialIoUartLib|$(PLATFORM_SI_PACKAGE)/IpBlock/SerialIo/Uart/Library/BaseSerialIoUartLibNull/BaseSerialIoUartLibNull.inf
!endif

 SerialIoAccessLib|$(PLATFORM_SI_PACKAGE)/IpBlock/SerialIo/Library/PeiDxeSmmSerialIoAccessLib/PeiDxeSmmSerialIoAccessLib.inf
 !if gSiPkgTokenSpaceGuid.PcdAdlLpSupport == TRUE
   SerialIoPrivateLib|$(PLATFORM_SI_PACKAGE)/IpBlock/SerialIo/LibraryPrivate/PeiDxeSmmSerialIoPrivateLib/PeiDxeSmmSerialIoPrivateLibVer2.inf
 !else
   SerialIoPrivateLib|$(PLATFORM_SI_PACKAGE)/IpBlock/SerialIo/LibraryPrivate/PeiDxeSmmSerialIoPrivateLib/PeiDxeSmmSerialIoPrivateLibVer4.inf
 !endif
 SerialIoI2cLib|$(PLATFORM_SI_PACKAGE)/IpBlock/SerialIo/I2c/Library/BaseSerialIoI2cLib/BaseSerialIoI2cLib.inf
 SerialIoSpiLib|$(PLATFORM_SI_PACKAGE)/IpBlock/SerialIo/Spi/Library/BaseSerialIoSpiLib/BaseSerialIoSpiLib.inf
 ResetSystemLib|$(PLATFORM_SI_PACKAGE)/Pch/Library/BaseResetSystemLib/BaseResetSystemLib.inf
#private
 SerialIoI2cPrivateLib|$(PLATFORM_SI_PACKAGE)/IpBlock/SerialIo/I2c/LibraryPrivate/SerialIoI2cPrivateLib/SerialIoI2cPrivateLib.inf
 !if gSiPkgTokenSpaceGuid.PcdAdlLpSupport == TRUE
   GpioPrivateLib|$(PLATFORM_SI_PACKAGE)/IpBlock/Gpio/LibraryPrivate/PeiDxeSmmGpioPrivateLib/PeiDxeSmmGpioPrivateLibVer2.inf
 !else
   GpioPrivateLib|$(PLATFORM_SI_PACKAGE)/IpBlock/Gpio/LibraryPrivate/PeiDxeSmmGpioPrivateLib/PeiDxeSmmGpioPrivateLibVer4.inf
 !endif
 SerialIoI2cMasterCommonLib|$(PLATFORM_SI_PACKAGE)/IpBlock/SerialIo/I2c/LibraryPrivate/PeiDxeSerialIoI2cMasterCommonLib/PeiDxeSerialIoI2cMasterCommonLib.inf

 PeiHsioLib|$(PLATFORM_SI_PACKAGE)/IpBlock/Hsio/LibraryPrivate/PeiHsioLib/PeiHsioLib.inf
 HsioSocLib|$(PLATFORM_SI_PACKAGE)/Fru/AdlPch/LibraryPrivate/HsioSocLib/HsioSocLibAdl.inf
 ChipsetInitLib|$(PLATFORM_SI_PACKAGE)/IpBlock/Hsio/LibraryPrivate/ChipsetInitLib/ChipsetInitLib.inf

 SiScheduleResetLib|$(PLATFORM_SI_PACKAGE)/Pch/LibraryPrivate/BaseSiScheduleResetLib/BaseSiScheduleResetLib.inf
 PchPciBdfLib|$(PLATFORM_SI_PACKAGE)/Pch/Library/BasePchPciBdfLib/BasePchPciBdfLib.inf

#
# SA
#
 SaPlatformLib|$(PLATFORM_SI_PACKAGE)/SystemAgent/Library/PeiDxeSmmSaPlatformLib/PeiDxeSmmSaPlatformLib.inf
 ItbtPcieRpLib|$(PLATFORM_SI_PACKAGE)/IpBlock/CpuPcieRp/Library/PeiDxeSmmItbtPcieRpLib/PeiDxeSmmItbtPcieRpLib.inf
 CpuRegbarAccessLib|$(PLATFORM_SI_PACKAGE)/IpBlock/P2sb/Library/PeiDxeSmmCpuRegbarAccessLib/PeiDxeSmmCpuRegbarAccessLib.inf
 GraphicsInfoLib|$(PLATFORM_SI_PACKAGE)/IpBlock/Graphics/Library/PeiDxeSmmGraphicsInfoLib/GraphicsInfoLibVer1.inf
 GraphicsInfoFruLib|$(PLATFORM_SI_PACKAGE)/Fru/AdlCpu/Graphics/Library/PeiDxeSmmGraphicsInfoFruLib/GraphicsInfoFruLib.inf
!if gSiPkgTokenSpaceGuid.PcdITbtEnable == TRUE
 TcssInfoLib|$(PLATFORM_SI_PACKAGE)/IpBlock/Tcss/Library/PeiDxeSmmTcssInfoLib/TcssInfoLib.inf
!else
 TcssInfoLib|$(PLATFORM_SI_PACKAGE)/IpBlock/Tcss/Library/PeiDxeSmmTcssInfoLibNull/TcssInfoLibNull.inf
!endif
 CpuDmiInfoLib|$(PLATFORM_SI_PACKAGE)/IpBlock/CpuDmi/Library/CpuDmiInfoLib/CpuDmiInfoLib.inf

 SecHostBridgeLib|$(PLATFORM_SI_PACKAGE)/IpBlock/HostBridge/Library/SecHostBridgeLib/SecHostBridgeLib.inf
 HostBridgeDataLib|$(PLATFORM_SI_PACKAGE)/IpBlock/HostBridge/Library/HostBridgeDataLib/HostBridgeDataLib.inf
 HostBridgeInfoFruLib|$(PLATFORM_SI_PACKAGE)/Fru/AdlCpu/HostBridge/Library/PeiDxeSmmHostBridgeInfoFruLib/HostBridgeInfoFruLib.inf
#
# TBT
#

  TbtCommonLib|$(PLATFORM_SI_PACKAGE)/IpBlock/Tbt/Library/PeiDxeSmmTbtCommonLib/TbtCommonLib.inf
  DxeTbtDisBmeLib|$(PLATFORM_SI_PACKAGE)/IpBlock/Tbt/Library/DxeTbtDisBmeLib/DxeTbtDisBmeLib.inf

#
# Overclocking
#
!if gSiPkgTokenSpaceGuid.PcdOverclockEnable == TRUE
  OcCommonLib|$(PLATFORM_SI_PACKAGE)/IpBlock/Overclocking/LibraryPrivate/PeiDxeSmmOcCommonLib/PeiDxeSmmOcCommonLib.inf
!else
  OcCommonLib|$(PLATFORM_SI_PACKAGE)/IpBlock/Overclocking/LibraryPrivate/PeiDxeSmmOcCommonLibNull/PeiDxeSmmOcCommonLibNull.inf
!endif

#
# Memory
#
MemoryAddressEncodeLib|$(PLATFORM_SI_PACKAGE)/IpBlock/MemoryInit/Adl/Library/PeiDxeSmmMemAddrEncodeLib/PeiDxeSmmMemAddrEncodeLib.inf

