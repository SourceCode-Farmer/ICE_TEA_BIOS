/** @file
  PEI SA Init FRU Lib header file

@copyright
  INTEL CONFIDENTIAL
  Copyright 2020 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains an 'Intel Peripheral Driver' and is uniquely identified as
  "Intel Reference Module" and is licensed for Intel CPUs and chipsets under
  the terms of your license agreement with Intel or your vendor. This file may
  be modified by the user, subject to additional terms of the license agreement.

@par Specification Reference:
**/
#ifndef _PEI_SA_INIT_FRU_LIB_H_
#define _PEI_SA_INIT_FRU_LIB_H_

#include <Ppi/SiPolicy.h>
#include <SaConfigHob.h>
#include <Library/BaseLib.h>
#include <Library/PeiServicesLib.h>
#include <Library/BaseMemoryLib.h>
#include <CpuPcieHobGen3.h>
#include <Library/IoLib.h>
#include <Base.h>
#include <Library/PeiHostBridgeInitLib.h>
#include <Library/PciSegmentLib.h>
#include <Library/ConfigBlockLib.h>
#include <Library/CpuDmiInfoLib.h>
#include <Register/GnaRegs.h>
#include <Library/GnaInitLib.h>
#include <Library/PostCodeLib.h>
#include <Library/DebugLib.h>

//
// Functions
//
/**
  Programs WRC (Write Cache) Feature for IOP.

  @param[in] MiscPeiPreMemConfig  - Instance of SA_MISC_PEI_PREMEM_CONFIG
**/
VOID
ProgramWrcFeatureForIop (
  IN SA_MISC_PEI_PREMEM_CONFIG      *MiscPeiPreMemConfig
  );

/**
  Init and Install CPU Pcie Gen3 Hob

  @param[out] SaPegHob            - SA_PEG_HOB instance installed by this function
  @retval EFI_SUCCESS
**/
EFI_STATUS
InstallCpuPcieGen3Hob (
  OUT SA_PEG_HOB      **SaPegHobOut
  );

/**
  This function performs SA internal devices enabling/disabling

  @param[in] HostBridgePeiConfig - Instance of HOST_BRIDGE_PEI_CONFIG
  @param[in] GnaConfig - Instance of GNA_CONFIG

**/
VOID
DeviceConfigure(
  IN    HOST_BRIDGE_PEI_CONFIG  *HostBridgePeiConfig,
  IN    GNA_CONFIG              *GnaConfig
  );

/**
  This function does SA security lock
**/
VOID
SaSecurityLock(
  VOID
  );

/**
  Program Host Bridge work arounds
**/
VOID
HostBridgeWorkAround(
  VOID
  );

/**
  Update SA Hob in PostMem

  @param[in]  GtConfig                 - Instance of GRAPHICS_PEI_PREMEM_CONFIG

  @retval EFI_SUCCESS
**/
EFI_STATUS
UpdateSaHobPostMem (
  IN       GRAPHICS_PEI_PREMEM_CONFIG  *GtPreMemConfig
  );


/**
  Get the GFX revision and update the same in ScratchPad register
**/
VOID
UpdateGfxRevisionToScratchpad (
  VOID
);

/**
  Disable Gna Device
**/
VOID
DisableGnaDevice (
  VOID
  );

/**
  Enable Gna Device
**/
VOID
EnableGnaDevice (
  VOID
  );

/**
  This function loads MemConfigSaGv default values

  @param[in] MemConfig         Pointer to Memory Config block
**/
VOID
UpdateMemConfigSaGvDefault(
  IN  MEMORY_CONFIGURATION  *MemConfig
  );

#endif
