/** @file
  SEC PCH library in C.

@copyright
  INTEL CONFIDENTIAL
  Copyright 2015 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains an 'Intel Peripheral Driver' and is uniquely identified as
  "Intel Reference Module" and is licensed for Intel CPUs and chipsets under
  the terms of your license agreement with Intel or your vendor. This file may
  be modified by the user, subject to additional terms of the license agreement.

@par Specification Reference:
**/
#include <Base.h>
#include <Uefi/UefiBaseType.h>
#include <Library/IoLib.h>
#include <Library/DebugLib.h>
#include <Library/BaseLib.h>
#include <Library/PciSegmentLib.h>
#include <Library/PchCycleDecodingLib.h>
#include <Library/PchPciBdfLib.h>
#include <Library/PchPcrLib.h>
#include <Library/SerialIoUartLib.h>
#include <Library/PmcLib.h>
#include <Library/PrintLib.h>
#include <Library/PsfSocLib.h>
#include <Library/PmcPrivateLib.h>
#include <Library/PeiSerialIoInitLib.h>
#include <Library/SerialIoAccessLib.h>
#include <PchReservedResources.h>
#include <Register/PchRegs.h>
#include <Register/PchPcrRegs.h>
#include <Register/P2sbRegs.h>
#include <Register/SmbusRegs.h>
#include <Register/RtcRegs.h>
#include <IndustryStandard/Pci30.h>
#include <SerialIoDevices.h>

#if FixedPcdGetBool(PcdFspBinaryEnable) == 1
/**
  Serial Io Uart Debug Configuration Wrapper

  @param[out] UartDeviceConfig           A pointer to the SERIAL_IO_UART_CONFIG.
  @param[out] SerialIoUartDebugEnable    Serial Io Uart Debug Enable/Disable.
  @param[out] SerialIoUartNumber         The Number of Serial Io Uart.
  @param[out] SerialIoUartPciMmioBase    MMIO Base Address by default in PCI Mode.
**/
VOID
SerialIoUartDebugConfigurationWrapper (
  OUT SERIAL_IO_UART_CONFIG  *UartDeviceConfig,
  OUT UINT8                  *SerialIoUartDebugEnable,
  OUT UINT8                  *SerialIoUartNumber,
  OUT UINT32                 *SerialIoUartPciMmioBase
  );

/**
  Serial Io Additional Uart Configuration Wrapper
  Allows to configure another UART in FSPT

  @param[out] UartEnable                 Serial Io Additional Uart Enable/Disable.
  @param[out] UartDeviceConfig           A pointer to the SERIAL_IO_UART_CONFIG.
  @param[out] SerialIoUartNumber         The Number of Serial Io Uart.
  @param[out] SerialIoUartPciMmioBase    MMIO Base Address by default in PCI Mode
**/
VOID
SerialIo2ndUartConfigurationWrapper (
  OUT UINT8                  *UartEnable,
  OUT SERIAL_IO_UART_CONFIG  *UartDeviceConfig,
  OUT UINT8                  *SerialIoUartNumber,
  OUT UINT32                 *SerialIoUartPciMmioBase
  );

/**
  Serial Io Spi Configuration Wrapper

  @param[out] SpiDeviceConfig           A pointer to the SERIAL_IO_SPI_CONFIG.
  @param[out] SerialIoSpiNumber         The Number of Serial Io Spi.
  @param[out] SerialIoSpiMmioBase       MMIO Base Address by default in PCI Mode
**/
VOID
SerialIoSpiConfigurationWrapper (
  OUT SERIAL_IO_SPI_CONFIG  *SpiDeviceConfig,
  OUT UINT8                 *SerialIoSpiNumber,
  OUT UINT32                *SerialIoSpiMmioBase
  );
#else
#include <Library/SerialIoUartDebugPropertyPcdLib.h>
#endif
/**
  This is helper function to initialize SerialIoSpi in early init.
**/
VOID
EFIAPI
SerialIoSpiEarlyinitalization (
  VOID
  )
{
#if FixedPcdGetBool(PcdFspBinaryEnable) == 1
  SERIAL_IO_SPI_CONFIG  SpiDeviceConfig;
  UINT8                 SerialIoSpiNumber;
  UINT32                SerialIoSpiMmioBase;
  UINT64                SerialIoSpiPciCfgBase;

  SerialIoSpiConfigurationWrapper (&SpiDeviceConfig, &SerialIoSpiNumber, &SerialIoSpiMmioBase);

  if (SpiDeviceConfig.Mode == 0) {
    return;
  }
  SerialIoSpiPciCfgBase  = GetSerialIoSpiPciCfg (SerialIoSpiNumber);

  PciSegmentWrite32 (SerialIoSpiPciCfgBase + PCI_BASE_ADDRESSREG_OFFSET, SerialIoSpiMmioBase);
  PciSegmentWrite32 (SerialIoSpiPciCfgBase + PCI_BASE_ADDRESSREG_OFFSET + 4, 0x0);

  SerialIoSpiConfiguration (SerialIoSpiNumber, &SpiDeviceConfig, TRUE);
#endif
}

/**
  This is helper function to initialize SerialIoUart in early init.
**/
VOID
EFIAPI
SerialIoUartDebugConfiguration (
  VOID
  )
{
  SERIAL_IO_UART_CONFIG  UartDeviceConfig;
  UINT8                  SerialIoUartDebugEnable;
  UINT8                  SerialIoUartNumber;
  UINT32                 SerialIoUartPciMmioBase;
  CHAR8                  CarInitBuffer[32];

#if FixedPcdGetBool(PcdFspBinaryEnable) == 1
  UINT8                  AdditionalUartEnabled;

  AsciiSPrint (CarInitBuffer, sizeof (CarInitBuffer), "FSP-T: CAR Init\n");
  SerialIoUartDebugConfigurationWrapper (&UartDeviceConfig, &SerialIoUartDebugEnable, &SerialIoUartNumber, &SerialIoUartPciMmioBase);
#else
  AsciiSPrint (CarInitBuffer, sizeof (CarInitBuffer), "CAR Init\n");
  SerialIoUartDebugEnable = SerialIoUartDebugPcdGetDebugEnable ();
  SerialIoUartPciMmioBase = SerialIoUartDebugPcdGetPciDefaultMmioBase ();
  SerialIoUartDebugPcdGetDeviceConfig (&UartDeviceConfig, &SerialIoUartNumber);
#endif

  UartDeviceConfig.DBG2      = FALSE;
  UartDeviceConfig.DmaEnable = FALSE;

  //
  // Initialize SerialIo UART for debug message
  //
  if (SerialIoUartDebugEnable == 1) {
    if (UartDeviceConfig.Mode == SerialIoUartPci) {
      SerialIoUartSetMmioInPciMode (SerialIoUartNumber, (UINT64)SerialIoUartPciMmioBase);
    }
    SerialIoUartConfiguration (SerialIoUartNumber, &UartDeviceConfig, TRUE);
    SerialIoUartWrite (SerialIoUartNumber, (UINT8 *)CarInitBuffer, AsciiStrLen (CarInitBuffer));
  }

#if FixedPcdGetBool(PcdFspBinaryEnable) == 1
  SerialIo2ndUartConfigurationWrapper (&AdditionalUartEnabled, &UartDeviceConfig, &SerialIoUartNumber, &SerialIoUartPciMmioBase);
  if (AdditionalUartEnabled == 1) {
    if (UartDeviceConfig.Mode == SerialIoUartPci) {
      SerialIoUartSetMmioInPciMode (SerialIoUartNumber, (UINT64)SerialIoUartPciMmioBase);
    }
    SerialIoUartConfiguration (SerialIoUartNumber, &UartDeviceConfig, TRUE);
  }
#endif
}

/**
  This function do the PCH cycle decoding initialization.
**/
VOID
EFIAPI
EarlyCycleDecoding (
  VOID
  )
{
  UINT64   P2sbBase;
  UINT64   SmbusBase;

  //
  // Enable PCR base address in PCH
  //
  P2sbBase = P2sbPciCfgBase ();
  PciSegmentWrite32 (P2sbBase + R_P2SB_CFG_SBREG_BAR, PCH_PCR_BASE_ADDRESS);
  //
  // Enable P2SB MSE
  //
  PciSegmentOr8 (P2sbBase + PCI_COMMAND_OFFSET, EFI_PCI_COMMAND_MEMORY_SPACE);

  //
  // Program PWRM BASE
  //
  PmcSetPwrmBase (PmcPciCfgBase (), PCH_PWRM_BASE_ADDRESS);

  //
  // Program ACPI Base.
  //
  PsfSetPmcAbase (PsfPmcPort (), PcdGet16 (PcdAcpiBaseAddress));

  //
  // Program and Enable TCO Base
  //
  PchTcoBaseSet (PcdGet16 (PcdTcoBaseAddress));

  SmbusBase = SmbusPciCfgBase ();
  //
  // Set SMBus PCI 0x64 = 0x0A0A0000
  //
  PciSegmentWrite32 (SmbusBase + R_SMBUS_CFG_64, 0x0A0A0000);
  //
  // Initialize SMBUS IO BAR
  //
  PciSegmentWrite16 (SmbusBase + R_SMBUS_CFG_BASE, PcdGet16 (PcdSmbusBaseAddress));
  //
  // Enable the Smbus I/O Enable
  //
  PciSegmentOr8 (SmbusBase + PCI_COMMAND_OFFSET, EFI_PCI_COMMAND_IO_SPACE);
  //
  // Enable the SMBUS Controller
  //
  PciSegmentOr8 (SmbusBase + R_SMBUS_CFG_HOSTC, B_SMBUS_CFG_HOSTC_HST_EN);

  //
  // Enable the upper 128-byte bank of RTC RAM.
  // PCR [RTC] + 0x3400 [2] = 1
  //
  PchPcrAndThenOr32 (PID_RTC_HOST, R_RTC_PCR_CONF, (UINT32) ~0, B_RTC_PCR_CONF_UCMOS_EN);
}
