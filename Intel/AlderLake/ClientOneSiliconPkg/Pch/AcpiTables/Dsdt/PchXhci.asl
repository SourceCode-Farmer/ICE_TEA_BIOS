/**@file

 @copyright
  INTEL CONFIDENTIAL
  Copyright 2010 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains an 'Intel Peripheral Driver' and is uniquely identified as
  "Intel Reference Module" and is licensed for Intel CPUs and chipsets under
  the terms of your license agreement with Intel or your vendor. This file may
  be modified by the user, subject to additional terms of the license agreement.

@par Specification Reference:
**/

#include <Register/UsbRegs.h>

External(\_SB.PC00.XHCI.PS0X, MethodObj)
External(\_SB.PC00.XHCI.PS3X, MethodObj)
External(\_SB.PC00.XHCI.RHUB.PS0X, MethodObj)
External(\_SB.PC00.XHCI.RHUB.PS2X, MethodObj)
External(\_SB.PC00.XHCI.RHUB.PS3X, MethodObj)
External(\_SB.PC00.XHCI.RHUB.INIR, MethodObj)
External(DUWS)

External(PU2C) // PCH USB2 Port count from NVS
External(PU3C) // PCH USB3 Port count from NVS
//[-start-210830-TAMT000004-add]//
#if defined(C770_SUPPORT)
External(\MCSZ, IntObj) // Machine size, 12 = 12'. 13 = 13'
#endif
//[-end-210830-TAMT000004-add]//
Scope(\_SB_.PC00) {
  Device(XHCI) {

    Name(_ADR, 0x00140000)

    OperationRegion(XPRT,PCI_Config,0x00,0x100)
    Field(XPRT,AnyAcc,NoLock,Preserve)
    {
      DVID, 16,
      Offset(0x10),
      XADL,  32,
      XADH,  32,
      Offset(R_XHCI_CFG_PWR_CNTL_STS), //0x74 PCI Power management control/status, PM_CS
      ,8,
      PMEE,1,
      ,6,
      PMES,1,
      Offset(R_XHCI_CFG_PCE), // 0xA2
          ,  2,
      D3HE,  1,  // D3_hot_en
    }

    //
    // XHCI controller _DSM method
    //
    Method(_DSM,4,serialized){
      ADBG("XHCI _DSM")

      If(PCIC(Arg0)) { return(PCID(Arg0,Arg1,Arg2,Arg3)) }

      Return(Buffer() {0})
    }

    Method(_S3D, 0, NotSerialized)
    {
      Return(3)
    }
    Method(_S4D, 0, NotSerialized)
    {
      Return(3)
    }
    Method(_S3W, 0, NotSerialized)
    {
      Return(3)
    }
    Method(_S4W, 0, NotSerialized)
    {
      Return(3)
    }

    Method(_S0W, 0x0, NotSerialized)
    {
      Return(0x3)
    }

    Method(_PRW, 0) {
      If(CondRefOf(DUWS)) {
        Return(GPRW(0x6D, DUWS)) // can wakeup from deepest USB Sleep Wake State
      } Else {
//[-start-210617-STATHAM0002-modify]//
#ifdef LCFC_SUPPORT
        Return(GPRW(0x6D, 3)) // can wakeup from S3 state
#else
        Return(GPRW(0x6D, 4)) // can wakeup from S4 state
#endif
//[-end-210617-STATHAM0002-modify]//
      }
    }

    Method(GPEH) {
      If(LEqual(^DVID,0xFFFF))
      {
        Return
      }
      If(LEqual(PMES,1)) {
        Notify(\_SB.PC00.XHCI, 0x02)
      }
    }

    Method(_DSW, 3) {}

    Method(_INI) {
      If(CondRefOf(\_SB.PC00.XHCI.RHUB.INIR)) {      // _INI for RTD3
        \_SB.PC00.XHCI.RHUB.INIR()
      }
    }

    //
    // USRA (USbR port Address), method for calculating address of first USBR port in XHCI controller
    //  Arg0: Number of USB2 Port
    //
    Method(USRA,1,Serialized) {
      Return (Add (PU2C, Arg0))
    }

    //
    // SSPA (SuperSpeed Port Address), method for calculating address of first SS port in XHCI controller
    //  Arg0: Number of Super Speed Port
    //
    Method(SSPA,1,Serialized) {
      Return (Add (PU2C, Add (2, Arg0)))
    }

    Method(_PS0,0,Serialized)
    {
      ADBG("XHCI D0")

      If(LEqual(^DVID,0xFFFF))
      {
        Return()
      }

      //
      // Call platform XHCI PS0 method if present
      //
      If(CondRefOf(\_SB.PC00.XHCI.PS0X))
      {
        \_SB.PC00.XHCI.PS0X()
      }
    }

    Method(_PS3,0,Serialized)
    {
      ADBG("XHCI D3")

      If(LEqual(^DVID,0xFFFF))
      {
        Return()
      }

      //
      // Call platform XHCI PS3 method if present
      //
      If(CondRefOf(\_SB.PC00.XHCI.PS3X))
      {
        \_SB.PC00.XHCI.PS3X()
      }
    }

    //
    //
    // Check for XHCI switch UUID
    //
    // Arguments:
    //  Arg0 (Buffer) : UUID
    //
    // Returns:
    //  1: It's valid UUID
    //  0: Invalid UUID
    //
    Method(CUID,1,Serialized) {
      If(LEqual(Arg0,ToUUID("7c9512a9-1705-4cb4-af7d-506a2423ab71"))) {
        Return(1)
      }
      Return(0)
    }

    Device(RHUB)
    {
      Name(_ADR, Zero)

      // PS0 Method for xHCI Root Hub
      Method(_PS0,0,Serialized)
      {
        ADBG("XHCI RHUB PS0")
        If(LEqual(\_SB.PC00.XHCI.DVID,0xFFFF))
        {
          Return()
        }
        //
        // Call platform XHCI.RHUB PS0 method if present.
        //
        If(CondRefOf(\_SB.PC00.XHCI.RHUB.PS0X))
        {
          ADBG("XHCI CSD0")
          \_SB.CSD0(MODPHY_SPD_GATING_XHCI)
          \_SB.PC00.XHCI.RHUB.PS0X()
        }
      }

      // PS2 Method for xHCI Root Hub
      Method(_PS2,0,Serialized)
      {
        ADBG("XHCI RHUB PS2")
        If(LEqual(\_SB.PC00.XHCI.DVID,0xFFFF))
        {
          Return()
        }
        //
        // Call platform XHCI.RHUB PS2 method if present.
        //
        If(CondRefOf(\_SB.PC00.XHCI.RHUB.PS2X))
        {
          \_SB.PC00.XHCI.RHUB.PS2X()
          ADBG("XHCI CSD3")
          \_SB.CSD3(MODPHY_SPD_GATING_XHCI)
        }
      }

      // PS3 Method for xHCI Root Hub
      Method(_PS3,0,Serialized)
      {
        ADBG("XHCI RHUB PS3")
        If(LEqual(\_SB.PC00.XHCI.DVID,0xFFFF))
        {
          Return()
        }
        //
        // Call platform XHCI.RHUB PS3 method if present.
        //
        If(CondRefOf(\_SB.PC00.XHCI.RHUB.PS3X))
        {
          \_SB.PC00.XHCI.RHUB.PS3X()
          ADBG("XHCI CSD3")
          \_SB.CSD3(MODPHY_SPD_GATING_XHCI)
        }
      }
    } // device rhub
  } // device xhc
} //scope

If(CondRefOf(PU2C)) {
  Scope(\_SB.PC00.XHCI.RHUB) {
//[-start-210818-TAMT000001-add]//
//[-start-210830-TAMT000004-modify]//
#if defined(C970_SUPPORT)
//[-end-210830-TAMT000004-modify]//
    //
    // High Speed Ports (without USBR)
    //   
    If (LLessEqual (1, PU2C)) {
      Device (HS01) {
        Name (_ADR, 1)
        Name (_UPC, Package() { 0x00, 0x00, 0x00, 0x00 }) //Not connectable
      }
    }

    If (LLessEqual (2, PU2C)) {
      Device (HS02) {
        Name (_ADR, 2)
        Name (_UPC, Package() { 0xFF, 0x0A, 0x00, 0x00 }) //USB2.0 type C
                                                     //connector
        Name(_PLD, Package() { Buffer(0x10) {
        0x82, 0x00, 0x00, 0x00, //bit6:0 Rev.2, bit7 Color ignore
        0x00, 0x00, 0x00, 0x00, 
        0x31, 0x1C, 0x80, 0x01, //bit64 Visible, bit67:69 Unknown 
        0x00, 0x00, 0x00, 0x00} //Panel,bit77:74 Unknown Shape,
        })                      //bit79:94 Group Token 0,  
                              //Group Position 2, must be the same
                              //number with super speed device 
                              //SSP1, recommend do not 
                              //use the same Group Token and
                              //Position number for other ports.
      }
    }

    If (LLessEqual (3, PU2C)) {
      Device (HS03) {
        Name (_ADR, 3)
        Name (_UPC, Package() { 0xFF, 0x0A, 0x00, 0x00 }) //USB2.0 type C
                                                     //connector
        Name (_PLD, Package() { Buffer(0x10) {
        0x82, 0x00, 0x00, 0x00, //bit6:0 Rev.2, bit7 Color ignore
        0x00, 0x00, 0x00, 0x00, 
        0x31, 0x1C, 0x80, 0x02, //bit64 Visible, bit67:69 Unknown 
        0x00, 0x00, 0x00, 0x00} //Panel,bit77:74 Unknown Shape,
        })                      //bit79:94 Group Token 0,  
                              //Group Position 2, must be the same
                              //number with super speed device 
                              //SSP1, recommend do not 
                              //use the same Group Token and
                              //Position number for other ports.
      }
    }

    If (LLessEqual (4, PU2C)) {
      Device (HS04) {
        Name (_ADR, 4)
        Name (_UPC, Package() { 0xFF, 0x0A, 0x00, 0x00 }) //USB2.0 type C
                                                     //connector
        Name (_PLD, Package() { Buffer(0x10) {
        0x82, 0x00, 0x00, 0x00, //bit6:0 Rev.2, bit7 Color ignore
        0x00, 0x00, 0x00, 0x00, 
        0x31, 0x1C, 0x80, 0x03, //bit64 Visible, bit67:69 Unknown 
        0x00, 0x00, 0x00, 0x00} //Panel,bit77:74 Unknown Shape,
        })                      //bit79:94 Group Token 0,  
                              //Group Position 2, must be the same
                              //number with super speed device 
                              //SSP1, recommend do not 
                              //use the same Group Token and
                              //Position number for other ports.
      }
    }

    If (LLessEqual (5, PU2C)) {
      Device (HS05) {
        Name (_ADR, 5)
        Name (_UPC, Package() { 0x00, 0x00, 0x00, 0x00 }) //Not connectable
      }
    }

    If (LLessEqual (6, PU2C)) {
      Device (HS06) {  //Finger Print
        Name (_ADR, 6)
        Name (_UPC, Package() { 0xFF, 0xFF, 0x00, 0x00 }) //Proprietary 
                                                       //connector
        Name (_PLD, Package() { Buffer(0x10) {
        0x82, 0x00, 0x00, 0x00, //bit6:0 Rev.2, bit7 Color ignore 
        0x00, 0x00, 0x00, 0x00, 
        0x30, 0x0C, 0x00, 0x02, //bit64 Invisible, bit69:67 Unknown Panel,
        0x00, 0x00, 0x00, 0x00} //bit77:74 Unknown Shape, bit79:94 Group 
                                //Token 0,Group Position 4.
        })
      }
    }

    If (LLessEqual (7, PU2C)) {
      Device (HS07) {
        Name (_ADR, 7)
        Name (_UPC, Package() { 0x00, 0x00, 0x00, 0x00 }) //Not connectable
      }
    }

    If (LLessEqual (8, PU2C)) {
      Device (HS08) {  //cammera
        Name (_ADR, 8)
        Name (_UPC, Package() { 0xFF, 0xFF, 0x00, 0x00 }) //Proprietary 
                                                         //connector
        Name (_PLD, Package() { Buffer(0x14) {
        0x82, 0x00, 0x00, 0x00, //bit6:0 rev.2, bit7 color ignore
        0x00, 0x00, 0x00, 0x00,   
        0x24, 0x01, 0x00, 0x04, //bit64 Invisible, bit66 Lid, bit 69:67 
        0x00, 0x00, 0x00, 0x00, //Panel Front ,bit71:70 Vertical Posi-
        0xC3, 0x00, 0xC3, 0x00} //tion Upper, bit 73:72 Horizontal 
        })                      //Position Center, bit77:74 Shape Round, 
                              //bit79:94 Group Token 0,Group Position 8.
                              //bit 143:128 Vertical Offset, bit 159:144 
                              //Horizontal Offset, you can modify it
                              //by real measuring results if can't pass 
                              //WHQL test.

        Device(CAMA)
        {
          Name (_ADR, 0x08)
          Name (_UPC, Package() { 0xFF, 0xFF, 0x00, 0x00 })   //Proprietary 
                                                           //connector
          Name(_PLD, Package() {Buffer (0x14) {
          0x82, 0x00, 0x00, 0x00, //bit6:0 rev.2, bit7 color ignore
          0x00, 0x00, 0x00, 0x00,   
          0x24, 0x01, 0x00, 0x04, //bit64 Invisible, bit66 Lid, bit 69:67 
          0x00, 0x00, 0x00, 0x00, //Panel Front , bit71:70 Vertical Posi-
          0xC3, 0x00, 0xC3, 0x00} //tion Upper, bit 73:72 Horizontal 
          })                      //Position Center, bit77:74 Shape Round, 
                                  //bit79:94 Group Token 0,Group Position 8. 
                                  //bit 143:128 Vertical Offset, bit 159:144 
                                  //Horizontal Offset, you can modify it
                                  //by real measuring results if can't pass 
                                  //WHQL test.
        }

        //Add IR camera PLD in usb port8 for [WHLK] Camera Driver Registration, Metadata, and Dependency System Test fail, Root Cause is Camera and IR camera shared usb port 5, but usb port 5 not have IR camera PLD, so WHLK test fail.
        Device(CAMB) //IR camera
        {
          Name(_ADR, 0x0A)                                   //This _ADR can see in Camera Device Manger "Address"
          Name(_UPC, Package() { 0xFF, 0xFF, 0x00, 0x00 })   //Proprietary 
                                                           //connector
          Name(_PLD, Package() {Buffer (0x14) {
          0x82, 0x00, 0x00, 0x00, //bit6:0 rev.2, bit7 color ignore
          0x00, 0x00, 0x00, 0x00,   
          0x24, 0x01, 0x00, 0x04, //bit64 Invisible, bit66 Lid, bit 69:67 
          0x00, 0x00, 0x00, 0x00, //Panel Front , bit71:70 Vertical Posi-
          0xC3, 0x00, 0xC3, 0x00} //tion Upper, bit 73:72 Horizontal 
          })                      //Position Center, bit77:74 Shape Round, 
                                //bit79:94 Group Token 0,Group Position 8. 
                                //bit 143:128 Vertical Offset, bit 159:144 
                                //Horizontal Offset, you can modify it
                                //by real measuring results if can't pass 
                                //WHQL test.        
        }

      }
    }

    If (LLessEqual (9, PU2C)) {
      Device (HS09) {
        Name (_ADR, 9)
        Name (_UPC, Package() { 0xFF, 0x00, 0x00, 0x00 }) //USB2.0 type A
                                                         //connector
        Name (_PLD, Package() { Buffer(0x10) {
        0x82, 0x00, 0x00, 0x00, //bit6:0 Rev.2, bit7 Color ignore
        0x00, 0x00, 0x00, 0x00, 
        0x31, 0x1C, 0x80, 0x00, //bit64 Visible, bit67:69 Unknown 
        0x00, 0x00, 0x00, 0x00} //Panel,bit77:74 Unknown Shape,
        })                      //bit79:94 Group Token 0,  
                              //Group Position 1, must be the same
                              //number with super speed device 
                              //SSP1, recommend do not 
                              //use the same Group Token and
                              //Position number for other ports.
      }
    }

    If (LLessEqual (10, PU2C)) {
      Device (HS10) {  //Bluetooth
        Name (_ADR, 10)
        Name (_UPC, Package() { 0xFF, 0xFF, 0x00, 0x00 }) //Proprietary
                                                       //connector
        Name (_PLD, Package() { Buffer(0x10) {
        0x82, 0x00, 0x00, 0x00, //bit6:0 rev.2, bit7 color ignore
        0x00, 0x00, 0x00, 0x00,  
        0x58, 0x1E, 0x80, 0x03, //bit64 Invisible, bit 69:67 Panel
        0x00, 0x00, 0x00, 0x00} //Right, advice not to fill in 
        })                      //Unknown, cannot let Bluetooth show in 
                                  //airplane mode otherwise, bit77:74 Shape 
                                  //Unknown, bit79:94 Group Token 0,  
                                  //Group Position 6.

        Method (_S3D, 0, NotSerialized) // _S3D: S3 Device State 
        { 
          Return (0x03) // If BT device is powered off, make device enter D3 power state .
                          //Otherwise maybe cause that lost Bluetooth function after resume from S3. 
        } 
        
        Method (_S4D, 0, NotSerialized) // _S4D: S4 Device State 
        { 
          Return (0x03) // If BT device is powered off, make device enter D3 power state.
        }         
      }
    }

    If (LLessEqual (11, PU2C)) {
      Device (HS11) {
        Name (_ADR, 11)
        Name (_UPC, Package() { 0x00, 0x00, 0x00, 0x00 }) //Not connectable
        
      }
    }

    If (LLessEqual (12, PU2C)) {
      Device (HS12) {
        Name (_ADR, 12)
        Name (_UPC, Package() { 0x00, 0x00, 0x00, 0x00 }) //Not connectable
      }
    }

    If (LLessEqual (13, PU2C)) {
      Device (HS13) {
        Name (_ADR, 13)
        Name ( _UPC, Package() { 0x00, 0x00, 0x00, 0x00 }) //Not connectable
      }
    }

    If (LLessEqual (14, PU2C)) {
      Device (HS14) {
        Name (_ADR, 14)
        Name (_UPC, Package() { 0x00, 0x00, 0x00, 0x00 }) //Not connectable
      }
    }
//[-start-210830-TAMT000004-add]//
#elif defined(C770_SUPPORT)
    //
    // High Speed Ports (without USBR)
    //   
    If (LLessEqual (1, PU2C)) {
      Device (HS01) {
        Name (_ADR, 1)
        Name (_UPC, Package() { 0x00, 0x00, 0x00, 0x00 }) //Not connectable
      }
    }

    If (LLessEqual (2, PU2C)) {
      Device (HS02) {
        Name (_ADR, 2)
        Name (_UPC, Package() { 0xFF, 0x0A, 0x00, 0x00 }) //USB2.0 type C
                                                     //connector
        Name(_PLD, Package() { Buffer(0x10) {
        0x82, 0x00, 0x00, 0x00, //bit6:0 Rev.2, bit7 Color ignore
        0x00, 0x00, 0x00, 0x00, 
        0x31, 0x1C, 0x80, 0x01, //bit64 Visible, bit67:69 Unknown 
        0x00, 0x00, 0x00, 0x00} //Panel,bit77:74 Unknown Shape,
        })                      //bit79:94 Group Token 0,  
                              //Group Position 2, must be the same
                              //number with super speed device 
                              //SSP1, recommend do not 
                              //use the same Group Token and
                              //Position number for other ports.
      }
    }

    If (LLessEqual (3, PU2C)) {
      Device (HS03) {
        Name (_ADR, 3)
        Name (_UPC, Package() { 0xFF, 0x0A, 0x00, 0x00 }) //USB2.0 type C
                                                     //connector
        Name (_PLD, Package() { Buffer(0x10) {
        0x82, 0x00, 0x00, 0x00, //bit6:0 Rev.2, bit7 Color ignore
        0x00, 0x00, 0x00, 0x00, 
        0x31, 0x1C, 0x80, 0x02, //bit64 Visible, bit67:69 Unknown 
        0x00, 0x00, 0x00, 0x00} //Panel,bit77:74 Unknown Shape,
        })                      //bit79:94 Group Token 0,  
                              //Group Position 2, must be the same
                              //number with super speed device 
                              //SSP1, recommend do not 
                              //use the same Group Token and
                              //Position number for other ports.
      }
    }

    If (LLessEqual (4, PU2C)) {
      Device (HS04) {
        Name (_ADR, 4)
        Name (_UPC, Package() { 0x00, 0x00, 0x00, 0x00 }) //Not connectable
      }
    }  

    If (LLessEqual (5, PU2C)) {
      Device (HS05) {
        Name (_ADR, 5)
        Name(UPC, Package() { 0xFF, 0xFF, 0x00, 0x00 }) //Proprietary 
        Name(UPCN, Package() { 0x00, 0x00, 0x00, 0x00 }) //Not connectable
        Method(_UPC){
          If (LEqual(MCSZ, 0x16)) 
          {
            Return (UPC) //touch screen 
          } 
          Else 
          {
            Return (UPCN)
          }
        }  
        
        Name(_PLD, Package() { Buffer(0x10) {
        0x82, 0x00, 0x00, 0x00, //bit6:0 Rev.2, bit7 Color ignore 
        0x00, 0x00, 0x00, 0x00, 
        0x30, 0x0C, 0x00, 0x03, //bit64 Invisible, bit69:67 Unknown Panel,
        0x00, 0x00, 0x00, 0x00}  //bit77:74 Unknown Shape, bit79:94 Group 
//[-start-210830-TAMT000004-A-add]//       
        })                       //Token 0,Group Position 6.   
//[-end-210830-TAMT000004-A-add]//  
      }
    }

    If (LLessEqual (6, PU2C)) {
      Device (HS06) {  //Finger Print
        Name (_ADR, 6)
        Name (_UPC, Package() { 0xFF, 0xFF, 0x00, 0x00 }) //Proprietary 
                                                       //connector
        Name (_PLD, Package() { Buffer(0x10) {
        0x82, 0x00, 0x00, 0x00, //bit6:0 Rev.2, bit7 Color ignore 
        0x00, 0x00, 0x00, 0x00, 
        0x30, 0x0C, 0x00, 0x02, //bit64 Invisible, bit69:67 Unknown Panel,
        0x00, 0x00, 0x00, 0x00} //bit77:74 Unknown Shape, bit79:94 Group 
                                //Token 0,Group Position 4.
        })
      }
    }

    If (LLessEqual (7, PU2C)) {
      Device (HS07) {
        Name (_ADR, 7)
        Name(UPC, Package() { 0xFF, 0x03, 0x00, 0x00 }) //Proprietary 
        Name(UPCN, Package() { 0x00, 0x00, 0x00, 0x00 }) //Not connectable
        Method(_UPC){
          If (LEqual(MCSZ, 0x16)) 
          {
            Return (UPC)
          } 
          Else 
          {
            Return (UPCN)
          }
        }  
        
        Name(_PLD, Package() { Buffer(0x10) {
        0x82, 0x00, 0x00, 0x00, //bit6:0 Rev.2, bit7 Color ignore
        0x00, 0x00, 0x00, 0x00, 
        0x31, 0x1C, 0x80, 0x03, //bit64 Visible, bit67:69 Unknown 
        0x00, 0x00, 0x00, 0x00} //Panel,bit77:74 Unknown Shape,
        })                      //bit79:94 Group Token 0,  
                              //Group Position 1, must be the same
                              //number with super speed device 
                              //SSP1, recommend do not 
                              //use the same Group Token and
                              //Position number for other ports.
      }
    }

    If (LLessEqual (8, PU2C)) {
      Device (HS08) {  //cammera
        Name (_ADR, 8)
        Name (_UPC, Package() { 0xFF, 0xFF, 0x00, 0x00 }) //Proprietary 
                                                         //connector
        Name (_PLD, Package() { Buffer(0x14) {
        0x82, 0x00, 0x00, 0x00, //bit6:0 rev.2, bit7 color ignore
        0x00, 0x00, 0x00, 0x00,   
        0x24, 0x01, 0x00, 0x04, //bit64 Invisible, bit66 Lid, bit 69:67 
        0x00, 0x00, 0x00, 0x00, //Panel Front ,bit71:70 Vertical Posi-
        0xC3, 0x00, 0xC3, 0x00} //tion Upper, bit 73:72 Horizontal 
        })                      //Position Center, bit77:74 Shape Round, 
                              //bit79:94 Group Token 0,Group Position 8.
                              //bit 143:128 Vertical Offset, bit 159:144 
                              //Horizontal Offset, you can modify it
                              //by real measuring results if can't pass 
                              //WHQL test.

        Device(CAMA)
        {
          Name (_ADR, 0x08)
          Name (_UPC, Package() { 0xFF, 0xFF, 0x00, 0x00 })   //Proprietary 
                                                           //connector
          Name(_PLD, Package() {Buffer (0x14) {
          0x82, 0x00, 0x00, 0x00, //bit6:0 rev.2, bit7 color ignore
          0x00, 0x00, 0x00, 0x00,   
          0x24, 0x01, 0x00, 0x04, //bit64 Invisible, bit66 Lid, bit 69:67 
          0x00, 0x00, 0x00, 0x00, //Panel Front , bit71:70 Vertical Posi-
          0xC3, 0x00, 0xC3, 0x00} //tion Upper, bit 73:72 Horizontal 
          })                      //Position Center, bit77:74 Shape Round, 
                                  //bit79:94 Group Token 0,Group Position 8. 
                                  //bit 143:128 Vertical Offset, bit 159:144 
                                  //Horizontal Offset, you can modify it
                                  //by real measuring results if can't pass 
                                  //WHQL test.
        }

        //Add IR camera PLD in usb port8 for [WHLK] Camera Driver Registration, Metadata, and Dependency System Test fail, Root Cause is Camera and IR camera shared usb port 5, but usb port 5 not have IR camera PLD, so WHLK test fail.
        Device(CAMB) //IR camera
        {
          Name(_ADR, 0x0A)                                   //This _ADR can see in Camera Device Manger "Address"
          Name(_UPC, Package() { 0xFF, 0xFF, 0x00, 0x00 })   //Proprietary 
                                                           //connector
          Name(_PLD, Package() {Buffer (0x14) {
          0x82, 0x00, 0x00, 0x00, //bit6:0 rev.2, bit7 color ignore
          0x00, 0x00, 0x00, 0x00,   
          0x24, 0x01, 0x00, 0x04, //bit64 Invisible, bit66 Lid, bit 69:67 
          0x00, 0x00, 0x00, 0x00, //Panel Front , bit71:70 Vertical Posi-
          0xC3, 0x00, 0xC3, 0x00} //tion Upper, bit 73:72 Horizontal 
          })                      //Position Center, bit77:74 Shape Round, 
                                //bit79:94 Group Token 0,Group Position 8. 
                                //bit 143:128 Vertical Offset, bit 159:144 
                                //Horizontal Offset, you can modify it
                                //by real measuring results if can't pass 
                                //WHQL test.        
        }

      }
    }

    If (LLessEqual (9, PU2C)) {
      Device (HS09) {
        Name (_ADR, 9)
        Name (_UPC, Package() { 0xFF, 0x00, 0x00, 0x00 }) //USB2.0 type A
                                                         //connector
        Name (_PLD, Package() { Buffer(0x10) {
        0x82, 0x00, 0x00, 0x00, //bit6:0 Rev.2, bit7 Color ignore
        0x00, 0x00, 0x00, 0x00, 
        0x31, 0x1C, 0x80, 0x00, //bit64 Visible, bit67:69 Unknown 
        0x00, 0x00, 0x00, 0x00} //Panel,bit77:74 Unknown Shape,
        })                      //bit79:94 Group Token 0,  
                              //Group Position 1, must be the same
                              //number with super speed device 
                              //SSP1, recommend do not 
                              //use the same Group Token and
                              //Position number for other ports.
      }
    }

    If (LLessEqual (10, PU2C)) {
      Device (HS10) {  //Bluetooth
        Name (_ADR, 10)
        Name (_UPC, Package() { 0xFF, 0xFF, 0x00, 0x00 }) //Proprietary
                                                       //connector
        Name (_PLD, Package() { Buffer(0x10) {
        0x82, 0x00, 0x00, 0x00, //bit6:0 rev.2, bit7 color ignore
        0x00, 0x00, 0x00, 0x00,  
        0x58, 0x1E, 0x80, 0x03, //bit64 Invisible, bit 69:67 Panel
        0x00, 0x00, 0x00, 0x00} //Right, advice not to fill in 
        })                      //Unknown, cannot let Bluetooth show in 
                                  //airplane mode otherwise, bit77:74 Shape 
                                  //Unknown, bit79:94 Group Token 0,  
                                  //Group Position 6.

        Method (_S3D, 0, NotSerialized) // _S3D: S3 Device State 
        { 
          Return (0x03) // If BT device is powered off, make device enter D3 power state .
                          //Otherwise maybe cause that lost Bluetooth function after resume from S3. 
        } 
        
        Method (_S4D, 0, NotSerialized) // _S4D: S4 Device State 
        { 
          Return (0x03) // If BT device is powered off, make device enter D3 power state.
        }         
      }
    }

    If (LLessEqual (11, PU2C)) {
      Device (HS11) {
        Name (_ADR, 11)
        Name (_UPC, Package() { 0x00, 0x00, 0x00, 0x00 }) //Not connectable
        
      }
    }

    If (LLessEqual (12, PU2C)) {
      Device (HS12) {
        Name (_ADR, 12)
        Name (_UPC, Package() { 0x00, 0x00, 0x00, 0x00 }) //Not connectable
      }
    }

    If (LLessEqual (13, PU2C)) {
      Device (HS13) {
        Name (_ADR, 13)
        Name ( _UPC, Package() { 0x00, 0x00, 0x00, 0x00 }) //Not connectable
      }
    }

    If (LLessEqual (14, PU2C)) {
      Device (HS14) {
        Name (_ADR, 14)
        Name (_UPC, Package() { 0x00, 0x00, 0x00, 0x00 }) //Not connectable
      }
    }
//[-end-210830-TAMT000004-add]//

//[-start-210906-Xinwei001-add]//
#elif defined(S370_SUPPORT)
    //
    // High Speed Ports (without USBR)
    //   
    If (LLessEqual (1, PU2C)) {    //usb2.0 port2
      Device (HS01) {
        Name (_ADR, 1)
        Name (_UPC, Package() { 0xFF, 0x00, 0x00, 0x00 }) //USB2.0 
                                                         //connector
        Name (_PLD, Package() { Buffer(0x10) {
        0x82, 0x00, 0x00, 0x00, //bit6:0 Rev.2, bit7 Color ignore
        0x00, 0x00, 0x00, 0x00, 
//[-start-210911-QINGLIN0064-modify]//
        0x31, 0x1C, 0x80, 0x00, //bit64 Visible, bit67:69 Unknown 
//[-end-210911-QINGLIN0064-modify]//
        0x00, 0x00, 0x00, 0x00} //Panel,bit77:74 Unknown Shape,
        })                      //bit79:94 Group Token 0,  
                              //Group Position 1, must be the same
                              //number with super speed device 
                              //SSP1, recommend do not 
                              //use the same Group Token and
                              //Position number for other ports.
      }
    }

    If (LLessEqual (2, PU2C)) {
      Device (HS02) {
        Name (_ADR, 2)
        Name (_UPC, Package() { 0xFF, 0x0A, 0x00, 0x00 }) //USB2.0 type C
                                                     //connector
        Name(_PLD, Package() { Buffer(0x10) {
        0x82, 0x00, 0x00, 0x00, //bit6:0 Rev.2, bit7 Color ignore
        0x00, 0x00, 0x00, 0x00, 
//[-start-210911-QINGLIN0064-modify]//
        0x31, 0x1C, 0x00, 0x01, //bit64 Visible, bit67:69 Unknown 
//[-end-210911-QINGLIN0064-modify]//
        0x00, 0x00, 0x00, 0x00} //Panel,bit77:74 Unknown Shape,
        })                      //bit79:94 Group Token 0,  
                              //Group Position 2, must be the same
                              //number with super speed device 
                              //SSP1, recommend do not 
                              //use the same Group Token and
                              //Position number for other ports.
      }
    }

    If (LLessEqual (3, PU2C)) {
      Device (HS03) {
        Name (_ADR, 3)
          Name (_UPC, Package() { 0x00, 0x00, 0x00, 0x00 }) //Not connectable
          }
        }


    If (LLessEqual (4, PU2C)) {
      Device (HS04) {
        Name (_ADR, 4)
          Name (_UPC, Package() { 0x00, 0x00, 0x00, 0x00 }) //Not connectable
          }
        }


    If (LLessEqual (5, PU2C)) {
      Device (HS05) {
        Name (_ADR, 5)
        Name (_UPC, Package() { 0x00, 0x00, 0x00, 0x00 }) //Not connectable
      }
    }

    If (LLessEqual (6, PU2C)) {
      Device (HS06) {  //Finger Print
        Name (_ADR, 6)
        Name (_UPC, Package() { 0xFF, 0xFF, 0x00, 0x00 }) //Proprietary 
                                                       //connector
        Name (_PLD, Package() { Buffer(0x10) {
        0x82, 0x00, 0x00, 0x00, //bit6:0 Rev.2, bit7 Color ignore 
        0x00, 0x00, 0x00, 0x00, 
//[-start-210911-QINGLIN0064-modify]//
        0x30, 0x0C, 0x80, 0x01, //bit64 Invisible, bit69:67 Unknown Panel,
//[-end-210911-QINGLIN0064-modify]//
        0x00, 0x00, 0x00, 0x00} //bit77:74 Unknown Shape, bit79:94 Group 
                                //Token 0,Group Position 3.
        })
      }
    }

    If (LLessEqual (7, PU2C)) {
      Device (HS07) {
        Name (_ADR, 7)
        Name (_UPC, Package() { 0x00, 0x00, 0x00, 0x00 }) //Not connectable
      }
    }

    If (LLessEqual (8, PU2C)) {
      Device (HS08) {  //cammera
        Name (_ADR, 8)
        Name (_UPC, Package() { 0xFF, 0xFF, 0x00, 0x00 }) //Proprietary 
                                                         //connector
        Name (_PLD, Package() { Buffer(0x14) {
        0x82, 0x00, 0x00, 0x00, //bit6:0 rev.2, bit7 color ignore
        0x00, 0x00, 0x00, 0x00,   
//[-start-210911-QINGLIN0064-modify]//
        0x24, 0x01, 0x00, 0x02, //bit64 Invisible, bit66 Lid, bit 69:67 
//[-end-210911-QINGLIN0064-modify]//
        0x00, 0x00, 0x00, 0x00, //Panel Front ,bit71:70 Vertical Posi-
        0xC3, 0x00, 0xC3, 0x00} //tion Upper, bit 73:72 Horizontal 
        })                      //Position Center, bit77:74 Shape Round, 
                              //bit79:94 Group Token 0,Group Position 4.
                              //bit 143:128 Vertical Offset, bit 159:144 
                              //Horizontal Offset, you can modify it
                              //by real measuring results if can't pass 
                              //WHQL test.

        Device(CAMA)
        {
          Name (_ADR, 0x08)
          Name (_UPC, Package() { 0xFF, 0xFF, 0x00, 0x00 })   //Proprietary 
                                                           //connector
          Name(_PLD, Package() {Buffer (0x14) {
          0x82, 0x00, 0x00, 0x00, //bit6:0 rev.2, bit7 color ignore
          0x00, 0x00, 0x00, 0x00,   
//[-start-210911-QINGLIN0064-modify]//
          0x24, 0x01, 0x00, 0x02, //bit64 Invisible, bit66 Lid, bit 69:67 
//[-end-210911-QINGLIN0064-modify]//
          0x00, 0x00, 0x00, 0x00, //Panel Front , bit71:70 Vertical Posi-
          0xC3, 0x00, 0xC3, 0x00} //tion Upper, bit 73:72 Horizontal 
          })                      //Position Center, bit77:74 Shape Round, 
                                  //bit79:94 Group Token 0,Group Position 4. 
                                  //bit 143:128 Vertical Offset, bit 159:144 
                                  //Horizontal Offset, you can modify it
                                  //by real measuring results if can't pass 
                                  //WHQL test.
        }

      }
    }

    If (LLessEqual (9, PU2C)) {
      Device (HS09) {
        Name (_ADR, 9)
        Name (_UPC, Package() { 0xFF, 0x03, 0x00, 0x00 }) //USB2.0 type A
                                                         //connector
        Name (_PLD, Package() { Buffer(0x10) {
        0x82, 0x00, 0x00, 0x00, //bit6:0 Rev.2, bit7 Color ignore
        0x00, 0x00, 0x00, 0x00, 
//[-start-210911-QINGLIN0064-modify]//
        0x31, 0x1C, 0x80, 0x02, //bit64 Visible, bit67:69 Unknown 
//[-end-210911-QINGLIN0064-modify]//
        0x00, 0x00, 0x00, 0x00} //Panel,bit77:74 Unknown Shape,
        })                      //bit79:94 Group Token 0,  
                              //Group Position 5, must be the same
                              //number with super speed device 
                              //SSP1, recommend do not 
                              //use the same Group Token and
                              //Position number for other ports.
      }
    }

    If (LLessEqual (10, PU2C)) {
      Device (HS10) {  //Bluetooth
        Name (_ADR, 10)
        Name (_UPC, Package() { 0xFF, 0xFF, 0x00, 0x00 }) //Proprietary
                                                       //connector
        Name (_PLD, Package() { Buffer(0x10) {
        0x82, 0x00, 0x00, 0x00, //bit6:0 rev.2, bit7 color ignore
        0x00, 0x00, 0x00, 0x00,  
//[-start-210911-QINGLIN0064-modify]//
        0x58, 0x1E, 0x00, 0x03, //bit64 Invisible, bit 69:67 Panel
//[-end-210911-QINGLIN0064-modify]//
        0x00, 0x00, 0x00, 0x00} //Right, advice not to fill in 
        })                      //Unknown, cannot let Bluetooth show in 
                                //airplane mode otherwise, bit77:74 Shape 
                                //Unknown, bit79:94 Group Token 0,  
                                //Group Position 6.

        Method (_S3D, 0, NotSerialized) // _S3D: S3 Device State 
        { 
          Return (0x03) // If BT device is powered off, make device enter D3 power state .
                          //Otherwise maybe cause that lost Bluetooth function after resume from S3. 
        } 
        
        Method (_S4D, 0, NotSerialized) // _S4D: S4 Device State 
        { 
          Return (0x03) // If BT device is powered off, make device enter D3 power state.
        }         
      }
    }

    If (LLessEqual (11, PU2C)) {
      Device (HS11) {
        Name (_ADR, 11)
        Name (_UPC, Package() { 0x00, 0x00, 0x00, 0x00 }) //Not connectable
        
      }
    }

    If (LLessEqual (12, PU2C)) {
      Device (HS12) {
        Name (_ADR, 12)
        Name (_UPC, Package() { 0x00, 0x00, 0x00, 0x00 }) //Not connectable
      }
    }

    If (LLessEqual (13, PU2C)) {
      Device (HS13) {
        Name (_ADR, 13)
        Name ( _UPC, Package() { 0x00, 0x00, 0x00, 0x00 }) //Not connectable
      }
    }

    If (LLessEqual (14, PU2C)) {
      Device (HS14) {
        Name (_ADR, 14)
        Name (_UPC, Package() { 0x00, 0x00, 0x00, 0x00 }) //Not connectable
      }
    }
//[-end-210906-Xinwei001-add]//
//[-start-210919-QINGLIN0073-add]//
#elif defined(S570_SUPPORT)
    //
    // High Speed Ports (without USBR)
    //   
    If (LLessEqual (1, PU2C)) {    //CARDREADER
      Device (HS01) {
        Name (_ADR, 1)
        Name (_UPC, Package() { 0xFF, 0xFF, 0x00, 0x00 }) // Proprietary 
                                                          //connector
        Name (_PLD, Package() { Buffer(0x10) {
        0x82, 0x00, 0x00, 0x00, //bit6:0 Rev.2, bit7 Color ignore
        0x00, 0x00, 0x00, 0x00, 
        0x30, 0x1C, 0x80, 0x00, //bit64 Invisible, bit67:69 Unknown 
        0x00, 0x00, 0x00, 0x00} //Panel,bit77:74 Unknown Shape,
        })                      //bit79:94 Group Token 0,  
                                //Group Position 1.
      }
    }

    If (LLessEqual (2, PU2C)) {
      Device (HS02) {
        Name (_ADR, 2)
        Name (_UPC, Package() { 0xFF, 0x0A, 0x00, 0x00 }) //USB2.0 type C port1
                                                          //connector
        Name(_PLD, Package() { Buffer(0x10) {
        0x82, 0x00, 0x00, 0x00, //bit6:0 Rev.2, bit7 Color ignore
        0x00, 0x00, 0x00, 0x00, 
        0x31, 0x1C, 0x00, 0x01, //bit64 Visible, bit67:69 Unknown 
        0x00, 0x00, 0x00, 0x00} //Panel,bit77:74 Unknown Shape,
        })                      //bit79:94 Group Token 0,  
                                //Group Position 2, must be the same
                                //number with super speed device 
                                //SSP1, recommend do not 
                                //use the same Group Token and
                                //Position number for other ports.
      }
    }

    If (LLessEqual (3, PU2C)) {
      Device (HS03) {
        Name (_ADR, 3)
        Name (_UPC, Package() { 0xFF, 0x0A, 0x00, 0x00 }) //USB2.0 type C port2
                                                          //connector
        Name(_PLD, Package() { Buffer(0x10) {
        0x82, 0x00, 0x00, 0x00, //bit6:0 Rev.2, bit7 Color ignore
        0x00, 0x00, 0x00, 0x00, 
        0x31, 0x1C, 0x80, 0x01, //bit64 Visible, bit67:69 Unknown 
        0x00, 0x00, 0x00, 0x00} //Panel,bit77:74 Unknown Shape,
        })                      //bit79:94 Group Token 0,  
                                //Group Position 3, must be the same
                                //number with super speed device 
                                //SSP1, recommend do not 
                                //use the same Group Token and
                                //Position number for other ports.
      }
    }

    If (LLessEqual (4, PU2C)) {
      Device (HS04) {
        Name (_ADR, 4)
          Name (_UPC, Package() { 0x00, 0x00, 0x00, 0x00 }) //Not connectable
          }
    }


    If (LLessEqual (5, PU2C)) {
      Device (HS05) {
        Name (_ADR, 5)
        Name (_UPC, Package() { 0x00, 0x00, 0x00, 0x00 }) //Not connectable
      }
    }

    If (LLessEqual (6, PU2C)) {
      Device (HS06) {  //Finger Print
        Name (_ADR, 6)
        Name (_UPC, Package() { 0xFF, 0xFF, 0x00, 0x00 }) //Proprietary 
                                                          //connector
        Name (_PLD, Package() { Buffer(0x10) {
        0x82, 0x00, 0x00, 0x00, //bit6:0 Rev.2, bit7 Color ignore 
        0x00, 0x00, 0x00, 0x00, 
        0x30, 0x0C, 0x00, 0x02, //bit64 Invisible, bit69:67 Unknown Panel,
        0x00, 0x00, 0x00, 0x00} //bit77:74 Unknown Shape, bit79:94 Group 
                                //Token 0,Group Position 4.
        })
      }
    }

    If (LLessEqual (7, PU2C)) {
      Device (HS07) { //USB3.0 PORT2 AOU
        Name (_ADR, 7)
        Name (_UPC, Package() { 0xFF, 0x03, 0x00, 0x00 }) //USB3.0 type A connector
                                                          //connector
        Name(_PLD, Package() { Buffer(0x10) {
        0x82, 0x00, 0x00, 0x00, //bit6:0 Rev.2, bit7 Color ignore
        0x00, 0x00, 0x00, 0x00, 
        0x31, 0x1C, 0x80, 0x02, //bit64 Visible, bit67:69 Unknown 
        0x00, 0x00, 0x00, 0x00} //Panel,bit77:74 Unknown Shape,
        })                      //bit79:94 Group Token 0,  
                                //Group Position 5, must be the same
                                //number with super speed device 
                                //SSP1, recommend do not 
                                //use the same Group Token and
                                //Position number for other ports.
      }
    }

    If (LLessEqual (8, PU2C)) {
      Device (HS08) {  //cammera
        Name (_ADR, 8)
        Name (_UPC, Package() { 0xFF, 0xFF, 0x00, 0x00 }) //Proprietary 
                                                         //connector
        Name(_PLD, Package() {Buffer (0x14) {
        0x82, 0x00, 0x00, 0x00, //bit6:0 rev.2, bit7 color ignore
        0x00, 0x00, 0x00, 0x00,   
        0x24, 0x01, 0x00, 0x03, //bit64 Invisible, bit66 Lid, bit 69:67 
        0x00, 0x00, 0x00, 0x00, //Panel Front , bit71:70 Vertical Posi-
        0xC3, 0x00, 0xC3, 0x00} //tion Upper, bit 73:72 Horizontal 
        })                      //Position Center, bit77:74 Shape Round, 
                                //bit79:94 Group Token 0,Group Position 6. 
                                //bit 143:128 Vertical Offset, bit 159:144 
                                //Horizontal Offset, you can modify it
                                //by real measuring results if can't pass 
                                //WHQL test.


        Device(CAMA)
        {
          Name (_ADR, 0x08)
          Name (_UPC, Package() { 0xFF, 0xFF, 0x00, 0x00 })   //Proprietary 
                                                           //connector
          Name(_PLD, Package() {Buffer (0x14) {
          0x82, 0x00, 0x00, 0x00, //bit6:0 rev.2, bit7 color ignore
          0x00, 0x00, 0x00, 0x00,   
          0x24, 0x01, 0x00, 0x03, //bit64 Invisible, bit66 Lid, bit 69:67 
          0x00, 0x00, 0x00, 0x00, //Panel Front , bit71:70 Vertical Posi-
          0xC3, 0x00, 0xC3, 0x00} //tion Upper, bit 73:72 Horizontal 
          })                      //Position Center, bit77:74 Shape Round, 
                                  //bit79:94 Group Token 0,Group Position 6. 
                                  //bit 143:128 Vertical Offset, bit 159:144 
                                  //Horizontal Offset, you can modify it
                                  //by real measuring results if can't pass 
                                  //WHQL test.
        }
      }
    }

    If (LLessEqual (9, PU2C)) {
      Device (HS09) { //USB3.0 PORT1
        Name (_ADR, 9)
        Name (_UPC, Package() { 0xFF, 0x03, 0x00, 0x00 }) //USB3.0 type A connector
                                                          //connector
        Name (_PLD, Package() { Buffer(0x10) {
        0x82, 0x00, 0x00, 0x00, //bit6:0 Rev.2, bit7 Color ignore
        0x00, 0x00, 0x00, 0x00, 
        0x31, 0x1C, 0x80, 0x03, //bit64 Visible, bit67:69 Unknown 
        0x00, 0x00, 0x00, 0x00} //Panel,bit77:74 Unknown Shape,
        })                      //bit79:94 Group Token 0,  
                                //Group Position 7, must be the same
                                //number with super speed device 
                                //SSP1, recommend do not 
                                //use the same Group Token and
                                //Position number for other ports.
      }
    }

    If (LLessEqual (10, PU2C)) {
      Device (HS10) {  //Bluetooth
        Name (_ADR, 10)
        Name (_UPC, Package() { 0xFF, 0xFF, 0x00, 0x00 }) //Proprietary
                                                          //connector
        Name (_PLD, Package() { Buffer(0x10) {
        0x82, 0x00, 0x00, 0x00, //bit6:0 rev.2, bit7 color ignore
        0x00, 0x00, 0x00, 0x00,  
        0x58, 0x1E, 0x00, 0x04, //bit64 Invisible, bit 69:67 Panel
        0x00, 0x00, 0x00, 0x00} //Right, advice not to fill in 
        })                      //Unknown, cannot let Bluetooth show in 
                                //airplane mode otherwise, bit77:74 Shape 
                                //Unknown, bit79:94 Group Token 0,  
                                //Group Position 8.

        Method (_S3D, 0, NotSerialized) // _S3D: S3 Device State 
        { 
          Return (0x03) // If BT device is powered off, make device enter D3 power state .
                          //Otherwise maybe cause that lost Bluetooth function after resume from S3. 
        } 
        
        Method (_S4D, 0, NotSerialized) // _S4D: S4 Device State 
        { 
          Return (0x03) // If BT device is powered off, make device enter D3 power state.
        }         
      }
    }

    If (LLessEqual (11, PU2C)) {
      Device (HS11) {
        Name (_ADR, 11)
        Name (_UPC, Package() { 0x00, 0x00, 0x00, 0x00 }) //Not connectable
        
      }
    }

    If (LLessEqual (12, PU2C)) {
      Device (HS12) {
        Name (_ADR, 12)
        Name (_UPC, Package() { 0x00, 0x00, 0x00, 0x00 }) //Not connectable
      }
    }

    If (LLessEqual (13, PU2C)) {
      Device (HS13) {
        Name (_ADR, 13)
        Name ( _UPC, Package() { 0x00, 0x00, 0x00, 0x00 }) //Not connectable
      }
    }

    If (LLessEqual (14, PU2C)) {
      Device (HS14) {
        Name (_ADR, 14)
        Name (_UPC, Package() { 0x00, 0x00, 0x00, 0x00 }) //Not connectable
      }
    }
//[-end-210919-QINGLIN0073-add]//
//[-start-210919-TAMT000009-add]//
#elif defined(S77014_SUPPORT) || defined(S77014IAH_SUPPORT)
    //
    // High Speed Ports (without USBR)
    //   
    If (LLessEqual (1, PU2C)) {
      Device (HS01) {
        Name (_ADR, 1)
        Name (_UPC, Package() { 0x00, 0x00, 0x00, 0x00 }) //Not connectable
      }
    }

    If (LLessEqual (2, PU2C)) {
      Device (HS02) {
        Name (_ADR, 2)
        Name (_UPC, Package() { 0xFF, 0x0A, 0x00, 0x00 }) //USB2.0 type C
                                                     //connector
        Name(_PLD, Package() { Buffer(0x10) {
        0x82, 0x00, 0x00, 0x00, //bit6:0 Rev.2, bit7 Color ignore
        0x00, 0x00, 0x00, 0x00, 
        0x31, 0x1C, 0x80, 0x01, //bit64 Visible, bit67:69 Unknown 
        0x00, 0x00, 0x00, 0x00} //Panel,bit77:74 Unknown Shape,
        })                      //bit79:94 Group Token 0,  
                              //Group Position 2, must be the same
                              //number with super speed device 
                              //SSP1, recommend do not 
                              //use the same Group Token and
                              //Position number for other ports.
      }
    }

    If (LLessEqual (3, PU2C)) {
      Device (HS03) {
        Name (_ADR, 3)
        Name (_UPC, Package() { 0xFF, 0x0A, 0x00, 0x00 }) //USB2.0 type C
                                                     //connector
        Name (_PLD, Package() { Buffer(0x10) {
        0x82, 0x00, 0x00, 0x00, //bit6:0 Rev.2, bit7 Color ignore
        0x00, 0x00, 0x00, 0x00, 
        0x31, 0x1C, 0x80, 0x02, //bit64 Visible, bit67:69 Unknown 
        0x00, 0x00, 0x00, 0x00} //Panel,bit77:74 Unknown Shape,
        })                      //bit79:94 Group Token 0,  
                              //Group Position 2, must be the same
                              //number with super speed device 
                              //SSP1, recommend do not 
                              //use the same Group Token and
                              //Position number for other ports.
      }
    }

    If (LLessEqual (4, PU2C)) {
      Device (HS04) {
        Name (_ADR, 4)
        Name (_UPC, Package() { 0x00, 0x00, 0x00, 0x00 }) //Not connectable
      }
    }

    If (LLessEqual (5, PU2C)) {
      Device (HS05) {
        Name (_ADR, 5)
        Name (_UPC, Package() { 0x00, 0x00, 0x00, 0x00 }) //Not connectable
      }
    }

    If (LLessEqual (6, PU2C)) {
      Device (HS06) {  //Finger Print
        Name (_ADR, 6)
        Name (_UPC, Package() { 0x00, 0x00, 0x00, 0x00 }) //Not connectable
      }
    }

    If (LLessEqual (7, PU2C)) {
      Device (HS07) {
        Name (_ADR, 7)
        Name (_UPC, Package() { 0x00, 0x00, 0x00, 0x00 }) //Not connectable
      }
    }

    If (LLessEqual (8, PU2C)) {
      Device (HS08) {  //cammera
        Name (_ADR, 8)
        Name (_UPC, Package() { 0xFF, 0xFF, 0x00, 0x00 }) //Proprietary 
                                                         //connector
        Name (_PLD, Package() { Buffer(0x14) {
        0x82, 0x00, 0x00, 0x00, //bit6:0 rev.2, bit7 color ignore
        0x00, 0x00, 0x00, 0x00,   
        0x24, 0x01, 0x00, 0x04, //bit64 Invisible, bit66 Lid, bit 69:67 
        0x00, 0x00, 0x00, 0x00, //Panel Front ,bit71:70 Vertical Posi-
        0xC3, 0x00, 0xC3, 0x00} //tion Upper, bit 73:72 Horizontal 
        })                      //Position Center, bit77:74 Shape Round, 
                              //bit79:94 Group Token 0,Group Position 8.
                              //bit 143:128 Vertical Offset, bit 159:144 
                              //Horizontal Offset, you can modify it
                              //by real measuring results if can't pass 
                              //WHQL test.

        Device(CAMA)
        {
          Name (_ADR, 0x08)
          Name (_UPC, Package() { 0xFF, 0xFF, 0x00, 0x00 })   //Proprietary 
                                                           //connector
          Name(_PLD, Package() {Buffer (0x14) {
          0x82, 0x00, 0x00, 0x00, //bit6:0 rev.2, bit7 color ignore
          0x00, 0x00, 0x00, 0x00,   
          0x24, 0x01, 0x00, 0x04, //bit64 Invisible, bit66 Lid, bit 69:67 
          0x00, 0x00, 0x00, 0x00, //Panel Front , bit71:70 Vertical Posi-
          0xC3, 0x00, 0xC3, 0x00} //tion Upper, bit 73:72 Horizontal 
          })                      //Position Center, bit77:74 Shape Round, 
                                  //bit79:94 Group Token 0,Group Position 8. 
                                  //bit 143:128 Vertical Offset, bit 159:144 
                                  //Horizontal Offset, you can modify it
                                  //by real measuring results if can't pass 
                                  //WHQL test.
        }

        //Add IR camera PLD in usb port8 for [WHLK] Camera Driver Registration, Metadata, and Dependency System Test fail, Root Cause is Camera and IR camera shared usb port 5, but usb port 5 not have IR camera PLD, so WHLK test fail.
        Device(CAMB) //IR camera
        {
          Name(_ADR, 0x0A)                                   //This _ADR can see in Camera Device Manger "Address"
          Name(_UPC, Package() { 0xFF, 0xFF, 0x00, 0x00 })   //Proprietary 
                                                           //connector
          Name(_PLD, Package() {Buffer (0x14) {
          0x82, 0x00, 0x00, 0x00, //bit6:0 rev.2, bit7 color ignore
          0x00, 0x00, 0x00, 0x00,   
          0x24, 0x01, 0x00, 0x04, //bit64 Invisible, bit66 Lid, bit 69:67 
          0x00, 0x00, 0x00, 0x00, //Panel Front , bit71:70 Vertical Posi-
          0xC3, 0x00, 0xC3, 0x00} //tion Upper, bit 73:72 Horizontal 
          })                      //Position Center, bit77:74 Shape Round, 
                                //bit79:94 Group Token 0,Group Position 8. 
                                //bit 143:128 Vertical Offset, bit 159:144 
                                //Horizontal Offset, you can modify it
                                //by real measuring results if can't pass 
                                //WHQL test.        
        }

      }
    }

    If (LLessEqual (9, PU2C)) {
      Device (HS09) {
        Name (_ADR, 9)
        Name (_UPC, Package() { 0xFF, 0x00, 0x00, 0x00 }) //USB2.0 type A
                                                         //connector
        Name (_PLD, Package() { Buffer(0x10) {
        0x82, 0x00, 0x00, 0x00, //bit6:0 Rev.2, bit7 Color ignore
        0x00, 0x00, 0x00, 0x00, 
        0x31, 0x1C, 0x80, 0x00, //bit64 Visible, bit67:69 Unknown 
        0x00, 0x00, 0x00, 0x00} //Panel,bit77:74 Unknown Shape,
        })                      //bit79:94 Group Token 0,  
                              //Group Position 1, must be the same
                              //number with super speed device 
                              //SSP1, recommend do not 
                              //use the same Group Token and
                              //Position number for other ports.
      }
    }

    If (LLessEqual (10, PU2C)) {
      Device (HS10) {  //Bluetooth
        Name (_ADR, 10)
        Name (_UPC, Package() { 0xFF, 0xFF, 0x00, 0x00 }) //Proprietary
                                                       //connector
        Name (_PLD, Package() { Buffer(0x10) {
        0x82, 0x00, 0x00, 0x00, //bit6:0 rev.2, bit7 color ignore
        0x00, 0x00, 0x00, 0x00,  
        0x58, 0x1E, 0x80, 0x03, //bit64 Invisible, bit 69:67 Panel
        0x00, 0x00, 0x00, 0x00} //Right, advice not to fill in 
        })                      //Unknown, cannot let Bluetooth show in 
                                  //airplane mode otherwise, bit77:74 Shape 
                                  //Unknown, bit79:94 Group Token 0,  
                                  //Group Position 6.

        Method (_S3D, 0, NotSerialized) // _S3D: S3 Device State 
        { 
          Return (0x03) // If BT device is powered off, make device enter D3 power state .
                          //Otherwise maybe cause that lost Bluetooth function after resume from S3. 
        } 
        
        Method (_S4D, 0, NotSerialized) // _S4D: S4 Device State 
        { 
          Return (0x03) // If BT device is powered off, make device enter D3 power state.
        }         
      }
    }

    If (LLessEqual (11, PU2C)) {
      Device (HS11) {
        Name (_ADR, 11)
        Name (_UPC, Package() { 0x00, 0x00, 0x00, 0x00 }) //Not connectable
        
      }
    }

    If (LLessEqual (12, PU2C)) {
      Device (HS12) {
        Name (_ADR, 12)
        Name (_UPC, Package() { 0x00, 0x00, 0x00, 0x00 }) //Not connectable
      }
    }

    If (LLessEqual (13, PU2C)) {
      Device (HS13) {
        Name (_ADR, 13)
        Name ( _UPC, Package() { 0x00, 0x00, 0x00, 0x00 }) //Not connectable
      }
    }

    If (LLessEqual (14, PU2C)) {
      Device (HS14) {
        Name (_ADR, 14)
        Name (_UPC, Package() { 0x00, 0x00, 0x00, 0x00 }) //Not connectable
      }
    } 
//[-end-210919-TAMT000009-add]//
//[-start-211013-TAMT000028-add]//
#elif defined(S77013_SUPPORT)
    //
    // High Speed Ports (without USBR)
    //   
    If (LLessEqual (1, PU2C)) {
      Device (HS01) {
        Name (_ADR, 1)
        Name (_UPC, Package() { 0x00, 0x00, 0x00, 0x00 }) //Not connectable
      }
    }

    If (LLessEqual (2, PU2C)) {
      Device (HS02) {
        Name (_ADR, 2)
        Name (_UPC, Package() { 0xFF, 0x0A, 0x00, 0x00 }) //USB2.0 type C
                                                     //connector
        Name(_PLD, Package() { Buffer(0x10) {
        0x82, 0x00, 0x00, 0x00, //bit6:0 Rev.2, bit7 Color ignore
        0x00, 0x00, 0x00, 0x00, 
        0x31, 0x1C, 0x80, 0x01, //bit64 Visible, bit67:69 Unknown 
        0x00, 0x00, 0x00, 0x00} //Panel,bit77:74 Unknown Shape,
        })                      //bit79:94 Group Token 0,  
                              //Group Position 2, must be the same
                              //number with super speed device 
                              //SSP1, recommend do not 
                              //use the same Group Token and
                              //Position number for other ports.
      }
    }

    If (LLessEqual (3, PU2C)) {
      Device (HS03) {
        Name (_ADR, 3)
        Name (_UPC, Package() { 0x00, 0x00, 0x00, 0x00 }) //Not connectable        
      }
    }

    If (LLessEqual (4, PU2C)) {
      Device (HS04) {
        Name (_ADR, 4)
//[-start-220224-TAMT000045-add]//
        Name (_UPC, Package() { 0xFF, 0x03, 0x00, 0x00 }) //USB2.0 type C
                                                     //connector
//[-end-220224-TAMT000045-add]//
        Name (_PLD, Package() { Buffer(0x10) {
        0x82, 0x00, 0x00, 0x00, //bit6:0 Rev.2, bit7 Color ignore
        0x00, 0x00, 0x00, 0x00, 
        0x31, 0x1C, 0x80, 0x02, //bit64 Visible, bit67:69 Unknown 
        0x00, 0x00, 0x00, 0x00} //Panel,bit77:74 Unknown Shape,
        })                      //bit79:94 Group Token 0,  
                              //Group Position 2, must be the same
                              //number with super speed device 
                              //SSP1, recommend do not 
                              //use the same Group Token and
                              //Position number for other ports.
      }
    }

    If (LLessEqual (5, PU2C)) {
      Device (HS05) {
        Name (_ADR, 5)
        Name (_UPC, Package() { 0x00, 0x00, 0x00, 0x00 }) //Not connectable
      }
    }

    If (LLessEqual (6, PU2C)) {
      Device (HS06) {  //Finger Print
        Name (_ADR, 6)
        Name (_UPC, Package() { 0x00, 0x00, 0x00, 0x00 }) //Not connectable
      }
    }

    If (LLessEqual (7, PU2C)) {
      Device (HS07) {
        Name (_ADR, 7)
        Name (_UPC, Package() { 0x00, 0x00, 0x00, 0x00 }) //Not connectable
      }
    }

    If (LLessEqual (8, PU2C)) {
      Device (HS08) {  //cammera
        Name (_ADR, 8)
        Name (_UPC, Package() { 0xFF, 0xFF, 0x00, 0x00 }) //Proprietary 
                                                         //connector
        Name (_PLD, Package() { Buffer(0x14) {
        0x82, 0x00, 0x00, 0x00, //bit6:0 rev.2, bit7 color ignore
        0x00, 0x00, 0x00, 0x00,   
        0x24, 0x01, 0x00, 0x04, //bit64 Invisible, bit66 Lid, bit 69:67 
        0x00, 0x00, 0x00, 0x00, //Panel Front ,bit71:70 Vertical Posi-
        0xC3, 0x00, 0xC3, 0x00} //tion Upper, bit 73:72 Horizontal 
        })                      //Position Center, bit77:74 Shape Round, 
                              //bit79:94 Group Token 0,Group Position 8.
                              //bit 143:128 Vertical Offset, bit 159:144 
                              //Horizontal Offset, you can modify it
                              //by real measuring results if can't pass 
                              //WHQL test.

        Device(CAMA)
        {
          Name (_ADR, 0x08)
          Name (_UPC, Package() { 0xFF, 0xFF, 0x00, 0x00 })   //Proprietary 
                                                           //connector
          Name(_PLD, Package() {Buffer (0x14) {
          0x82, 0x00, 0x00, 0x00, //bit6:0 rev.2, bit7 color ignore
          0x00, 0x00, 0x00, 0x00,   
          0x24, 0x01, 0x00, 0x04, //bit64 Invisible, bit66 Lid, bit 69:67 
          0x00, 0x00, 0x00, 0x00, //Panel Front , bit71:70 Vertical Posi-
          0xC3, 0x00, 0xC3, 0x00} //tion Upper, bit 73:72 Horizontal 
          })                      //Position Center, bit77:74 Shape Round, 
                                  //bit79:94 Group Token 0,Group Position 8. 
                                  //bit 143:128 Vertical Offset, bit 159:144 
                                  //Horizontal Offset, you can modify it
                                  //by real measuring results if can't pass 
                                  //WHQL test.
        }

        //Add IR camera PLD in usb port8 for [WHLK] Camera Driver Registration, Metadata, and Dependency System Test fail, Root Cause is Camera and IR camera shared usb port 5, but usb port 5 not have IR camera PLD, so WHLK test fail.
        Device(CAMB) //IR camera
        {
          Name(_ADR, 0x0A)                                   //This _ADR can see in Camera Device Manger "Address"
          Name(_UPC, Package() { 0xFF, 0xFF, 0x00, 0x00 })   //Proprietary 
                                                           //connector
          Name(_PLD, Package() {Buffer (0x14) {
          0x82, 0x00, 0x00, 0x00, //bit6:0 rev.2, bit7 color ignore
          0x00, 0x00, 0x00, 0x00,   
          0x24, 0x01, 0x00, 0x04, //bit64 Invisible, bit66 Lid, bit 69:67 
          0x00, 0x00, 0x00, 0x00, //Panel Front , bit71:70 Vertical Posi-
          0xC3, 0x00, 0xC3, 0x00} //tion Upper, bit 73:72 Horizontal 
          })                      //Position Center, bit77:74 Shape Round, 
                                //bit79:94 Group Token 0,Group Position 8. 
                                //bit 143:128 Vertical Offset, bit 159:144 
                                //Horizontal Offset, you can modify it
                                //by real measuring results if can't pass 
                                //WHQL test.        
        }

      }
    }

    If (LLessEqual (9, PU2C)) {
      Device (HS09) {
        Name (_ADR, 9)
        Name (_UPC, Package() { 0x00, 0x00, 0x00, 0x00 }) //Not connectable
      }
    }

    If (LLessEqual (10, PU2C)) {
      Device (HS10) {  //Bluetooth
        Name (_ADR, 10)
        Name (_UPC, Package() { 0xFF, 0xFF, 0x00, 0x00 }) //Proprietary
                                                       //connector
        Name (_PLD, Package() { Buffer(0x10) {
        0x82, 0x00, 0x00, 0x00, //bit6:0 rev.2, bit7 color ignore
        0x00, 0x00, 0x00, 0x00,  
        0x58, 0x1E, 0x80, 0x03, //bit64 Invisible, bit 69:67 Panel
        0x00, 0x00, 0x00, 0x00} //Right, advice not to fill in 
        })                      //Unknown, cannot let Bluetooth show in 
                                  //airplane mode otherwise, bit77:74 Shape 
                                  //Unknown, bit79:94 Group Token 0,  
                                  //Group Position 6.

        Method (_S3D, 0, NotSerialized) // _S3D: S3 Device State 
        { 
          Return (0x03) // If BT device is powered off, make device enter D3 power state .
                          //Otherwise maybe cause that lost Bluetooth function after resume from S3. 
        } 
        
        Method (_S4D, 0, NotSerialized) // _S4D: S4 Device State 
        { 
          Return (0x03) // If BT device is powered off, make device enter D3 power state.
        }         
      }
    }

    If (LLessEqual (11, PU2C)) {
      Device (HS11) {
        Name (_ADR, 11)
        Name (_UPC, Package() { 0x00, 0x00, 0x00, 0x00 }) //Not connectable
        
      }
    }

    If (LLessEqual (12, PU2C)) {
      Device (HS12) {
        Name (_ADR, 12)
        Name (_UPC, Package() { 0x00, 0x00, 0x00, 0x00 }) //Not connectable
      }
    }

    If (LLessEqual (13, PU2C)) {
      Device (HS13) {
        Name (_ADR, 13)
        Name ( _UPC, Package() { 0x00, 0x00, 0x00, 0x00 }) //Not connectable
      }
    }

    If (LLessEqual (14, PU2C)) {
      Device (HS14) {
        Name (_ADR, 14)
        Name (_UPC, Package() { 0x00, 0x00, 0x00, 0x00 }) //Not connectable
      }
    } 
//[-end-211013-TAMT000028-add]//
#else
//[-end-210818-TAMT000001-add]//
    //
    // High Speed Ports (without USBR)
    //
    If (LLessEqual (1, PU2C)) {
      Device (HS01) {
        Name (_ADR, 1)
      }
    }

    If (LLessEqual (2, PU2C)) {
      Device (HS02) {
        Name (_ADR, 2)
      }
    }

    If (LLessEqual (3, PU2C)) {
      Device (HS03) {
        Name (_ADR, 3)
      }
    }

    If (LLessEqual (4, PU2C)) {
      Device (HS04) {
        Name (_ADR, 4)
      }
    }

    If (LLessEqual (5, PU2C)) {
      Device (HS05) {
        Name (_ADR, 5)
      }
    }

    If (LLessEqual (6, PU2C)) {
      Device (HS06) {
        Name (_ADR, 6)
      }
    }

    If (LLessEqual (7, PU2C)) {
      Device (HS07) {
        Name (_ADR, 7)
      }
    }

    If (LLessEqual (8, PU2C)) {
      Device (HS08) {
        Name (_ADR, 8)
      }
    }

    If (LLessEqual (9, PU2C)) {
      Device (HS09) {
        Name (_ADR, 9)
      }
    }

    If (LLessEqual (10, PU2C)) {
      Device (HS10) {
        Name (_ADR, 10)
      }
    }

    If (LLessEqual (11, PU2C)) {
      Device (HS11) {
        Name (_ADR, 11)
      }
    }

    If (LLessEqual (12, PU2C)) {
      Device (HS12) {
        Name (_ADR, 12)
      }
    }

    If (LLessEqual (13, PU2C)) {
      Device (HS13) {
        Name (_ADR, 13)
      }
    }

    If (LLessEqual (14, PU2C)) {
      Device (HS14) {
        Name (_ADR, 14)
      }
    }

    //
    // USBR ports definitions for supported PCH
    //
    Device(USR1) {
      Method(_ADR) { Return (USRA (1))}
    }

    Device(USR2) {
      Method(_ADR) { Return (USRA (2))}
    }
//[-start-210818-TAMT000001-add]//
#endif
//[-end-210818-TAMT000001-add]//
  }
}

If(CondRefOf(PU3C)) {
  Scope(\_SB.PC00.XHCI.RHUB) {
//[-start-210818-TAMT000001-add]//
//[-start-210830-TAMT000004-modify]//
#if defined(C970_SUPPORT)
//[-end-210830-TAMT000004-modify]//
    //
    // Super Speed Ports
    //
    If (LLessEqual (1, PU3C)) {
      Device (SS01) {
        Method (_ADR) { Return (SSPA (1)) }
        Name (_UPC, Package() { 0xFF, 0x03, 0x00, 0x00 }) //USB3.0 type A
                                                         //connector
        Name(_PLD, Package() { Buffer(0x10) {
        0x82, 0x00, 0x00, 0x00, //bit6:0 Rev.2, bit7 Color ignore
        0x00, 0x00, 0x00, 0x00, 
        0x31, 0x1C, 0x80, 0x00, //bit64 Visible, bit67:69 Unknown 
        0x00, 0x00, 0x00, 0x00} //Panel,bit77:74 Unknown Shape,
        })                      //bit79:94 Group Token 0,  
                              //Group Position 1, must be the same
                              //number with super speed device 
                              //SSP1, recommend do not 
                              //use the same Group Token and
                              //Position number for other ports.
      }
    }

    If (LLessEqual (2, PU3C)) {
      Device (SS02) {
        Method (_ADR) { Return (SSPA (2)) }
        Name (_UPC, Package() { 0x00, 0x00, 0x00, 0x00 }) //Not connectable
      }
    }

    If (LLessEqual (3, PU3C)) {
      Device (SS03) {
        Method (_ADR) { Return (SSPA (3)) }
        Name (_UPC, Package() { 0x00, 0x00, 0x00, 0x00 }) //Not connectable
      }
    }

    If (LLessEqual (4, PU3C)) {
      Device (SS04) {
        Method (_ADR) { Return (SSPA (4)) }
        Name (_UPC, Package() { 0x00, 0x00, 0x00, 0x00 }) //Not connectable
      }
    }

    If (LLessEqual (5, PU3C)) {
      Device (SS05) {
        Method (_ADR) { Return (SSPA (5)) }
        Name (_UPC, Package() { 0x00, 0x00, 0x00, 0x00 }) //Not connectable
      }
    }

    If (LLessEqual (6, PU3C)) {
      Device (SS06) {
        Method (_ADR) { Return (SSPA (6)) }
        Name (_UPC, Package() { 0x00, 0x00, 0x00, 0x00 }) //Not connectable
      }
    }

    If (LLessEqual (7, PU3C)) {
      Device (SS07) {
        Method (_ADR) { Return (SSPA (7)) }
        Name (_UPC, Package() { 0x00, 0x00, 0x00, 0x00 }) //Not connectable
      }
    }

    If (LLessEqual (8, PU3C)) {
      Device (SS08) {
        Method (_ADR) { Return (SSPA (8)) }
        Name (_UPC, Package() { 0x00, 0x00, 0x00, 0x00 }) //Not connectable
      }
    }

    If (LLessEqual (9, PU3C)) {
      Device (SS09) {
        Method (_ADR) { Return (SSPA (9)) }
        Name (_UPC, Package() { 0x00, 0x00, 0x00, 0x00 }) //Not connectable
      }
    }

    If (LLessEqual (10, PU3C)) {
      Device (SS10) {
        Method (_ADR) { Return (SSPA (10)) }
        Name(_UPC, Package() { 0x00, 0x00, 0x00, 0x00 }) //Not connectable
      }
    }
//[-start-210830-TAMT000004-add]//
#elif defined(C770_SUPPORT)
    //
    // Super Speed Ports
    //
    If (LLessEqual (1, PU3C)) {
      Device (SS01) {
        Method (_ADR) { Return (SSPA (1)) }
        Name (_UPC, Package() { 0xFF, 0x03, 0x00, 0x00 }) //USB3.0 type A
                                                         //connector
        Name(_PLD, Package() { Buffer(0x10) {
        0x82, 0x00, 0x00, 0x00, //bit6:0 Rev.2, bit7 Color ignore
        0x00, 0x00, 0x00, 0x00, 
        0x31, 0x1C, 0x80, 0x00, //bit64 Visible, bit67:69 Unknown 
        0x00, 0x00, 0x00, 0x00} //Panel,bit77:74 Unknown Shape,
        })                      //bit79:94 Group Token 0,  
                              //Group Position 1, must be the same
                              //number with super speed device 
                              //SSP1, recommend do not 
                              //use the same Group Token and
                              //Position number for other ports.
      }
    }

    If (LLessEqual (2, PU3C)) {
      Device (SS02) {
        Method (_ADR) { Return (SSPA (2)) }
        Name(UPC, Package() { 0xFF, 0x03, 0x00, 0x00 }) //Proprietary 
        Name(UPCN, Package() { 0x00, 0x00, 0x00, 0x00 }) //Not connectable
        Method(_UPC){
          If (LEqual(MCSZ, 0x16)) 
          {
            Return (UPC)
          } 
          Else 
          {
            Return (UPCN)
          }
        }  
        
        Name(_PLD, Package() { Buffer(0x10) {
        0x82, 0x00, 0x00, 0x00, //bit6:0 Rev.2, bit7 Color ignore
        0x00, 0x00, 0x00, 0x00, 
        0x31, 0x1C, 0x80, 0x03, //bit64 Visible, bit67:69 Unknown 
        0x00, 0x00, 0x00, 0x00} //Panel,bit77:74 Unknown Shape,
        })                      //bit79:94 Group Token 0,  
                              //Group Position 1, must be the same
                              //number with super speed device 
                              //SSP1, recommend do not 
                              //use the same Group Token and
                              //Position number for other ports.
      }
     }

    If (LLessEqual (3, PU3C)) {
      Device (SS03) {
        Method (_ADR) { Return (SSPA (3)) }
        Name (_UPC, Package() { 0x00, 0x00, 0x00, 0x00 }) //Not connectable
      }
    }

    If (LLessEqual (4, PU3C)) {
      Device (SS04) {
        Method (_ADR) { Return (SSPA (4)) }
        Name (_UPC, Package() { 0x00, 0x00, 0x00, 0x00 }) //Not connectable
      }
    }

    If (LLessEqual (5, PU3C)) {
      Device (SS05) {
        Method (_ADR) { Return (SSPA (5)) }
        Name (_UPC, Package() { 0x00, 0x00, 0x00, 0x00 }) //Not connectable
      }
    }

    If (LLessEqual (6, PU3C)) {
      Device (SS06) {
        Method (_ADR) { Return (SSPA (6)) }
        Name (_UPC, Package() { 0x00, 0x00, 0x00, 0x00 }) //Not connectable
      }
    }

    If (LLessEqual (7, PU3C)) {
      Device (SS07) {
        Method (_ADR) { Return (SSPA (7)) }
        Name (_UPC, Package() { 0x00, 0x00, 0x00, 0x00 }) //Not connectable
      }
    }

    If (LLessEqual (8, PU3C)) {
      Device (SS08) {
        Method (_ADR) { Return (SSPA (8)) }
        Name (_UPC, Package() { 0x00, 0x00, 0x00, 0x00 }) //Not connectable
      }
    }

    If (LLessEqual (9, PU3C)) {
      Device (SS09) {
        Method (_ADR) { Return (SSPA (9)) }
        Name (_UPC, Package() { 0x00, 0x00, 0x00, 0x00 }) //Not connectable
      }
    }

    If (LLessEqual (10, PU3C)) {
      Device (SS10) {
        Method (_ADR) { Return (SSPA (10)) }
        Name(_UPC, Package() { 0x00, 0x00, 0x00, 0x00 }) //Not connectable
      }
    }
//[-end-210830-TAMT000004-add]//
//[-end-210830-TAMT000004-modify]//
//[-start-210927-TAMT000009-A-add]//
#elif defined(S77014_SUPPORT) || defined(S77014IAH_SUPPORT)
    //
    // Super Speed Ports
    //
    If (LLessEqual (1, PU3C)) {
      Device (SS01) {
        Method (_ADR) { Return (SSPA (1)) }
        Name (_UPC, Package() { 0xFF, 0x03, 0x00, 0x00 }) //USB2.0 type A
                                                         //connector
        Name (_PLD, Package() { Buffer(0x10) {
        0x82, 0x00, 0x00, 0x00, //bit6:0 Rev.2, bit7 Color ignore
        0x00, 0x00, 0x00, 0x00, 
        0x31, 0x1C, 0x80, 0x00, //bit64 Visible, bit67:69 Unknown 
        0x00, 0x00, 0x00, 0x00} //Panel,bit77:74 Unknown Shape,
        })                      //bit79:94 Group Token 0,  
                              //Group Position 1, must be the same
                              //number with super speed device 
                              //SSP1, recommend do not 
                              //use the same Group Token and
                              //Position number for other ports.
      }
    }

    If (LLessEqual (2, PU3C)) {
      Device (SS02) {
        Method (_ADR) { Return (SSPA (2)) }
        Name(_UPC, Package() { 0x00, 0x00, 0x00, 0x00 }) //Not connectable
      }
    }

    If (LLessEqual (3, PU3C)) {
      Device (SS03) {
        Method (_ADR) { Return (SSPA (3)) }
        Name(_UPC, Package() { 0x00, 0x00, 0x00, 0x00 }) //Not connectable
      }
    }

    If (LLessEqual (4, PU3C)) {
      Device (SS04) {
        Method (_ADR) { Return (SSPA (4)) }
        Name(_UPC, Package() { 0x00, 0x00, 0x00, 0x00 }) //Not connectable
      }
    }

    If (LLessEqual (5, PU3C)) {
      Device (SS05) {
        Method (_ADR) { Return (SSPA (5)) }
        Name(_UPC, Package() { 0x00, 0x00, 0x00, 0x00 }) //Not connectable
      }
    }

    If (LLessEqual (6, PU3C)) {
      Device (SS06) {
        Method (_ADR) { Return (SSPA (6)) }
        Name(_UPC, Package() { 0x00, 0x00, 0x00, 0x00 }) //Not connectable
      }
    }

    If (LLessEqual (7, PU3C)) {
      Device (SS07) {
        Method (_ADR) { Return (SSPA (7)) }
        Name(_UPC, Package() { 0x00, 0x00, 0x00, 0x00 }) //Not connectable
      }
    }

    If (LLessEqual (8, PU3C)) {
      Device (SS08) {
        Method (_ADR) { Return (SSPA (8)) }
        Name(_UPC, Package() { 0x00, 0x00, 0x00, 0x00 }) //Not connectable
      }
    }

    If (LLessEqual (9, PU3C)) {
      Device (SS09) {
        Method (_ADR) { Return (SSPA (9)) }
        Name(_UPC, Package() { 0x00, 0x00, 0x00, 0x00 }) //Not connectable
      }
    }

    If (LLessEqual (10, PU3C)) {
      Device (SS10) {
        Method (_ADR) { Return (SSPA (10)) }
        Name(_UPC, Package() { 0x00, 0x00, 0x00, 0x00 }) //Not connectable
      }
    }
//[-end-210927-TAMT000009-A-add]//
//[-start-210906-Xinwei001-add]//
#elif defined(S370_SUPPORT)
    //
    // Super Speed Ports
    //
    If (LLessEqual (1, PU3C)) {
      Device (SS01) {
        Method (_ADR) { Return (SSPA (1)) }
        Name (_UPC, Package() { 0xFF, 0x03, 0x00, 0x00 }) //USB3.0 type A
                                                         //connector
        Name(_PLD, Package() { Buffer(0x10) {
        0x82, 0x00, 0x00, 0x00, //bit6:0 Rev.2, bit7 Color ignore
        0x00, 0x00, 0x00, 0x00, 
//[-start-210911-QINGLIN0064-modify]//
        0x31, 0x1C, 0x80, 0x02, //bit64 Visible, bit67:69 Unknown 
//[-end-210911-QINGLIN0064-modify]//
        0x00, 0x00, 0x00, 0x00} //Panel,bit77:74 Unknown Shape,
        })                      //bit79:94 Group Token 0,  
                              //Group Position 5, must be the same
                              //number with super speed device 
                              //SSP1, recommend do not 
                              //use the same Group Token and
                              //Position number for other ports.
      }
    }

    If (LLessEqual (2, PU3C)) {
      Device (SS02) {
        Method (_ADR) { Return (SSPA (2)) }
        Name (_UPC, Package() { 0x00, 0x00, 0x00, 0x00 }) //Not connectable
      }
    }

    If (LLessEqual (3, PU3C)) {
      Device (SS03) {
        Method (_ADR) { Return (SSPA (3)) }
        Name (_UPC, Package() { 0x00, 0x00, 0x00, 0x00 }) //Not connectable
      }
    }

    If (LLessEqual (4, PU3C)) {
      Device (SS04) {
        Method (_ADR) { Return (SSPA (4)) }
        Name (_UPC, Package() { 0x00, 0x00, 0x00, 0x00 }) //Not connectable
      }
    }

    If (LLessEqual (5, PU3C)) {
      Device (SS05) {
        Method (_ADR) { Return (SSPA (5)) }
        Name (_UPC, Package() { 0x00, 0x00, 0x00, 0x00 }) //Not connectable
      }
    }

    If (LLessEqual (6, PU3C)) {
      Device (SS06) {
        Method (_ADR) { Return (SSPA (6)) }
        Name (_UPC, Package() { 0x00, 0x00, 0x00, 0x00 }) //Not connectable
      }
    }

    If (LLessEqual (7, PU3C)) {
      Device (SS07) {
        Method (_ADR) { Return (SSPA (7)) }
        Name (_UPC, Package() { 0x00, 0x00, 0x00, 0x00 }) //Not connectable
      }
    }

    If (LLessEqual (8, PU3C)) {
      Device (SS08) {
        Method (_ADR) { Return (SSPA (8)) }
        Name (_UPC, Package() { 0x00, 0x00, 0x00, 0x00 }) //Not connectable
      }
    }

    If (LLessEqual (9, PU3C)) {
      Device (SS09) {
        Method (_ADR) { Return (SSPA (9)) }
        Name (_UPC, Package() { 0x00, 0x00, 0x00, 0x00 }) //Not connectable
      }
    }

    If (LLessEqual (10, PU3C)) {
      Device (SS10) {
        Method (_ADR) { Return (SSPA (10)) }
        Name(_UPC, Package() { 0x00, 0x00, 0x00, 0x00 }) //Not connectable
      }
    }
//[-end-210906-Xinwei001-add]//
//[-start-210919-QINGLIN0073-add]//
#elif defined(S570_SUPPORT)
    //
    // Super Speed Ports
    //
    If (LLessEqual (1, PU3C)) {
      Device (SS01) { //USB3.0 PORT1
        Method (_ADR) { Return (SSPA (1)) }
        Name (_UPC, Package() { 0xFF, 0x03, 0x00, 0x00 }) //USB3.0 type A connector
                                                          //connector
        Name (_PLD, Package() { Buffer(0x10) {
        0x82, 0x00, 0x00, 0x00, //bit6:0 Rev.2, bit7 Color ignore
        0x00, 0x00, 0x00, 0x00, 
        0x31, 0x1C, 0x80, 0x03, //bit64 Visible, bit67:69 Unknown 
        0x00, 0x00, 0x00, 0x00} //Panel,bit77:74 Unknown Shape,
        })                      //bit79:94 Group Token 0,  
                                //Group Position 7, must be the same
                                //number with super speed device 
                                //SSP1, recommend do not 
                                //use the same Group Token and
                                //Position number for other ports.
      }
    }

    If (LLessEqual (2, PU3C)) {
      Device (SS02) { 
        Method (_ADR) { Return (SSPA (2)) }
        Name (_UPC, Package() { 0x00, 0x00, 0x00, 0x00 }) //Not connectable
      }
    }

    If (LLessEqual (3, PU3C)) {
      Device (SS03) {//USB3.0 PORT2 AOU
        Method (_ADR) { Return (SSPA (3)) }
        Name (_UPC, Package() { 0xFF, 0x03, 0x00, 0x00 }) //USB3.0 type A connector
                                                          //connector
        Name(_PLD, Package() { Buffer(0x10) {
        0x82, 0x00, 0x00, 0x00, //bit6:0 Rev.2, bit7 Color ignore
        0x00, 0x00, 0x00, 0x00, 
        0x31, 0x1C, 0x80, 0x02, //bit64 Visible, bit67:69 Unknown 
        0x00, 0x00, 0x00, 0x00} //Panel,bit77:74 Unknown Shape,
        })                      //bit79:94 Group Token 0,  
                                //Group Position 5, must be the same
                                //number with super speed device 
                                //SSP1, recommend do not 
                                //use the same Group Token and
                                //Position number for other ports.
      }
    }

    If (LLessEqual (4, PU3C)) {
      Device (SS04) {
        Method (_ADR) { Return (SSPA (4)) }
        Name (_UPC, Package() { 0x00, 0x00, 0x00, 0x00 }) //Not connectable
      }
    }

    If (LLessEqual (5, PU3C)) {
      Device (SS05) {
        Method (_ADR) { Return (SSPA (5)) }
        Name (_UPC, Package() { 0x00, 0x00, 0x00, 0x00 }) //Not connectable
      }
    }

    If (LLessEqual (6, PU3C)) {
      Device (SS06) {
        Method (_ADR) { Return (SSPA (6)) }
        Name (_UPC, Package() { 0x00, 0x00, 0x00, 0x00 }) //Not connectable
      }
    }

    If (LLessEqual (7, PU3C)) {
      Device (SS07) {
        Method (_ADR) { Return (SSPA (7)) }
        Name (_UPC, Package() { 0x00, 0x00, 0x00, 0x00 }) //Not connectable
      }
    }

    If (LLessEqual (8, PU3C)) {
      Device (SS08) {
        Method (_ADR) { Return (SSPA (8)) }
        Name (_UPC, Package() { 0x00, 0x00, 0x00, 0x00 }) //Not connectable
      }
    }

    If (LLessEqual (9, PU3C)) {
      Device (SS09) {
        Method (_ADR) { Return (SSPA (9)) }
        Name (_UPC, Package() { 0x00, 0x00, 0x00, 0x00 }) //Not connectable
      }
    }

    If (LLessEqual (10, PU3C)) {
      Device (SS10) {
        Method (_ADR) { Return (SSPA (10)) }
        Name(_UPC, Package() { 0x00, 0x00, 0x00, 0x00 }) //Not connectable
      }
    }
//[-end-210919-QINGLIN0073-add]//
//[-start-211013-TAMT000028-add]//
#elif defined(S77013_SUPPORT)
    //
    // Super Speed Ports
    //
    If (LLessEqual (1, PU3C)) {
      Device (SS01) {
        Method (_ADR) { Return (SSPA (1)) }
        Name(_UPC, Package() { 0x00, 0x00, 0x00, 0x00 }) //Not connectable
      }
    }

    If (LLessEqual (2, PU3C)) {
      Device (SS02) {
        Method (_ADR) { Return (SSPA (2)) }
        Name(_UPC, Package() { 0x00, 0x00, 0x00, 0x00 }) //Not connectable
      }
    }

    If (LLessEqual (3, PU3C)) {
      Device (SS03) {
        Method (_ADR) { Return (SSPA (3)) }
        Name(_UPC, Package() { 0x00, 0x00, 0x00, 0x00 }) //Not connectable
      }
    }

    If (LLessEqual (4, PU3C)) {
      Device (SS04) {
        Method (_ADR) { Return (SSPA (4)) }
        Name(_UPC, Package() { 0x00, 0x00, 0x00, 0x00 }) //Not connectable
      }
    }

    If (LLessEqual (5, PU3C)) {
      Device (SS05) {
        Method (_ADR) { Return (SSPA (5)) }
        Name(_UPC, Package() { 0x00, 0x00, 0x00, 0x00 }) //Not connectable
      }
    }

    If (LLessEqual (6, PU3C)) {
      Device (SS06) {
        Method (_ADR) { Return (SSPA (6)) }
        Name(_UPC, Package() { 0x00, 0x00, 0x00, 0x00 }) //Not connectable
      }
    }

    If (LLessEqual (7, PU3C)) {
      Device (SS07) {
        Method (_ADR) { Return (SSPA (7)) }
        Name(_UPC, Package() { 0x00, 0x00, 0x00, 0x00 }) //Not connectable
      }
    }

    If (LLessEqual (8, PU3C)) {
      Device (SS08) {
        Method (_ADR) { Return (SSPA (8)) }
        Name(_UPC, Package() { 0x00, 0x00, 0x00, 0x00 }) //Not connectable
      }
    }

    If (LLessEqual (9, PU3C)) {
      Device (SS09) {
        Method (_ADR) { Return (SSPA (9)) }
        Name(_UPC, Package() { 0x00, 0x00, 0x00, 0x00 }) //Not connectable
      }
    }

    If (LLessEqual (10, PU3C)) {
      Device (SS10) {
        Method (_ADR) { Return (SSPA (10)) }
        Name(_UPC, Package() { 0x00, 0x00, 0x00, 0x00 }) //Not connectable
      }
    }
//[-end-211013-TAMT000028-add]//
#else
//[-end-210818-TAMT000001-add]//
    //
    // Super Speed Ports
    //
    If (LLessEqual (1, PU3C)) {
      Device (SS01) {
        Method (_ADR) { Return (SSPA (1)) }
      }
    }

    If (LLessEqual (2, PU3C)) {
      Device (SS02) {
        Method (_ADR) { Return (SSPA (2)) }
      }
    }

    If (LLessEqual (3, PU3C)) {
      Device (SS03) {
        Method (_ADR) { Return (SSPA (3)) }
      }
    }

    If (LLessEqual (4, PU3C)) {
      Device (SS04) {
        Method (_ADR) { Return (SSPA (4)) }
      }
    }

    If (LLessEqual (5, PU3C)) {
      Device (SS05) {
        Method (_ADR) { Return (SSPA (5)) }
      }
    }

    If (LLessEqual (6, PU3C)) {
      Device (SS06) {
        Method (_ADR) { Return (SSPA (6)) }
      }
    }

    If (LLessEqual (7, PU3C)) {
      Device (SS07) {
        Method (_ADR) { Return (SSPA (7)) }
      }
    }

    If (LLessEqual (8, PU3C)) {
      Device (SS08) {
        Method (_ADR) { Return (SSPA (8)) }
      }
    }

    If (LLessEqual (9, PU3C)) {
      Device (SS09) {
        Method (_ADR) { Return (SSPA (9)) }
      }
    }

    If (LLessEqual (10, PU3C)) {
      Device (SS10) {
        Method (_ADR) { Return (SSPA (10)) }
      }
    }
//[-start-210818-TAMT000001-add]//
#endif
//[-end-210818-TAMT000001-add]//
  } // end RHUB scope
} // end if
