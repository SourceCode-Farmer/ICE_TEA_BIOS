/** @file
  Header file for PchInfoDefs.

@copyright
  INTEL CONFIDENTIAL
  Copyright 2020 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains an 'Intel Peripheral Driver' and is uniquely identified as
  "Intel Reference Module" and is licensed for Intel CPUs and chipsets under
  the terms of your license agreement with Intel or your vendor. This file may
  be modified by the user, subject to additional terms of the license agreement.

@par Specification Reference:
**/
#ifndef _PCH_INFO_DEFS_H_
#define _PCH_INFO_DEFS_H_


#define PCH_A0                0x00
#define PCH_A1                0x01
#define PCH_B0                0x10
#define PCH_B1                0x11
#define PCH_C0                0x20
#define PCH_C1                0x21
#define PCH_D0                0x30
#define PCH_D1                0x31
#define PCH_Z0                0xF0
#define PCH_Z1                0xF1
#define PCH_STEPPING_MAX      0xFF

#define PCH_H                   1
#define PCH_LP                  2
#define PCH_N                   3
#define PCH_S                   4
#define PCH_P                   5
#define PCH_M                   6
#define PCH_SERVER              0x80
#define PCH_UNKNOWN_SERIES      0xFF

#define CNL_PCH                 3
#define ICL_PCH                 4
#define TGL_PCH                 5
#define JSL_PCH                 6
#define ADL_PCH                 7
#define CDF_PCH                 0x80
#define EBG_PCH                 0x81
#define S3M2_IBL                0x90
#define PCH_UNKNOWN_GENERATION  0xFF

#endif
