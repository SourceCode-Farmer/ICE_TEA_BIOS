/** @file
  PCH DMI/PSTH SMI handler implementation.

@copyright
  INTEL CONFIDENTIAL
  Copyright 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains an 'Intel Peripheral Driver' and is uniquely identified as
  "Intel Reference Module" and is licensed for Intel CPUs and chipsets under
  the terms of your license agreement with Intel or your vendor. This file may
  be modified by the user, subject to additional terms of the license agreement.

@par Specification
**/
#include "PchInitSmm.h"
#include <Register/PchDmiRegs.h>
#include <Register/PchRegsPsth.h>
#include <Register/PchPcrRegs.h>

/**
  A SW SMI callback to copy the data in the PSTH TRPREG0-3 registers to the DMI IOT-4 registers

  @param[in]      DispatchHandle  - The handle of this callback, obtained when registering
  @param[in]      CallbackContext - Pointer to the EFI_SMM_IO_TRAP_DISPATCH_CALLBACK_CONTEXT
  @param[in/out]  ComBuffer - Unused
  @param[in/out]  ComBuferSize - Unused
**/
VOID
EFIAPI
PchSmiDmiPsthCallBack (
  IN  EFI_HANDLE                            DispatchHandle,
  IN  EFI_SMM_IO_TRAP_CONTEXT               *CallbackContext,
  IN OUT VOID                               *CommBuffer,
  IN OUT UINTN                              *CommBufferSize
  )
{
  UINT8 TrapHandlerNumber;

  //
  // Mirror the values of the PSTH TREG[0-3] high and low registers into the cooresponding DMI IOT[1-4] registers
  //
  for (TrapHandlerNumber = 0; TrapHandlerNumber < 4; TrapHandlerNumber++) {
    PchPcrWrite32 (PID_DMI, R_PCH_DMI_PCR_IOT1 + TrapHandlerNumber * 8,     PchPcrRead32 (PID_PSTH, R_PSTH_PCR_TRPREG0 + TrapHandlerNumber * 8));
    PchPcrWrite32 (PID_DMI, R_PCH_DMI_PCR_IOT1 + TrapHandlerNumber * 8 + 4, PchPcrRead32 (PID_PSTH, R_PSTH_PCR_TRPREG0 + TrapHandlerNumber * 8 + 4));
  }
}

/**
  This registers a SW SMI that copies the IoTrap registers to their cooresponding DMI mirror registers

  @param[in] ImageHandle       Used to initialize protocol

  @retval    EFI_SUCCESS       SMI DMI PSTH Handler is registered successfully.
  @retval    EFI_UNSUPPORTED   Unable to register SMI DMI PSTH Handler successfully.
**/
EFI_STATUS
EFIAPI
PchSmiDmiPsthRegister (
  IN  EFI_HANDLE                              ImageHandle
  )
{
  EFI_STATUS                     Status;
  EFI_HANDLE                     SwHandle;
  EFI_SMM_SW_REGISTER_CONTEXT    SwContext;
  EFI_SMM_SW_DISPATCH2_PROTOCOL  *SwDispatch;
  PCH_SMI_DMI_PSTH_PROTOCOL      *PchSmiDmiPsthProtocol;

  //
  //  Locate the SMM SW dispatch protocol
  //
  if (gSmst != NULL) {
    Status = gSmst->SmmLocateProtocol (&gEfiSmmSwDispatch2ProtocolGuid, NULL, (VOID**)&SwDispatch);
    ASSERT_EFI_ERROR (Status);
  } else {
    return EFI_UNSUPPORTED;
  }

  SwContext.SwSmiInputValue = (UINTN) -1;

  //
  // Set up the SW SMI callback
  //
  Status = SwDispatch->Register (SwDispatch,
                                 (EFI_SMM_HANDLER_ENTRY_POINT2) PchSmiDmiPsthCallBack,
                                 &SwContext,
                                 &SwHandle);
  ASSERT_EFI_ERROR (Status);

  //
  // Set up PchSmiDmiPsthProtocol so the S3 Bootscript has access to the SwSwiInputValue
  //
  (gBS->AllocatePool) (EfiBootServicesData, sizeof (PCH_SMI_DMI_PSTH_PROTOCOL), (VOID **)&PchSmiDmiPsthProtocol);

  if (PchSmiDmiPsthProtocol == NULL) {
    return EFI_UNSUPPORTED;
  }

  PchSmiDmiPsthProtocol->PchSmiDmiPsthAddress = (UINT16) SwContext.SwSmiInputValue;

  return gBS->InstallMultipleProtocolInterfaces (
                  &ImageHandle,
                  &gPchSmiDmiPsthProtocolGuid,
                  PchSmiDmiPsthProtocol,
                  NULL
                  );
}
