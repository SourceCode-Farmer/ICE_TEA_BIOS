#******************************************************************************
#* Copyright (c) 2012 - 2019, Insyde Software Corp. All Rights Reserved.
#*
#* You may not reproduce, distribute, publish, display, perform, modify, adapt,
#* transmit, broadcast, present, recite, release, license or otherwise exploit
#* any part of this publication in any form, by any means, without the prior
#* written permission of Insyde Software Corporation.
#*
#******************************************************************************
#
## @file
#  Component description file for the AldrLake SiPkg DSC file.
#
# @copyright
#  INTEL CONFIDENTIAL
#  Copyright 2019 - 2020 Intel Corporation.
#
#  The source code contained or described herein and all documents related to the
#  source code ("Material") are owned by Intel Corporation or its suppliers or
#  licensors. Title to the Material remains with Intel Corporation or its suppliers
#  and licensors. The Material may contain trade secrets and proprietary and
#  confidential information of Intel Corporation and its suppliers and licensors,
#  and is protected by worldwide copyright and trade secret laws and treaty
#  provisions. No part of the Material may be used, copied, reproduced, modified,
#  published, uploaded, posted, transmitted, distributed, or disclosed in any way
#  without Intel's prior express written permission.
#
#  No license under any patent, copyright, trade secret or other intellectual
#  property right is granted to or conferred upon you by disclosure or delivery
#  of the Materials, either expressly, by implication, inducement, estoppel or
#  otherwise. Any license under such intellectual property rights must be
#  express and approved by Intel in writing.
#
#  Unless otherwise agreed by Intel in writing, you may not remove or alter
#  this notice or any other notice embedded in Materials by Intel or
#  Intel's suppliers or licensors in any way.
#
#  This file contains an 'Intel Peripheral Driver' and is uniquely identified as
#  "Intel Reference Module" and is licensed for Intel CPUs and chipsets under
#  the terms of your license agreement with Intel or your vendor. This file may
#  be modified by the user, subject to additional terms of the license agreement.
#
#@par Specification Reference:
#
##

[PcdsFixedAtBuild]
gSiPkgTokenSpaceGuid.PcdSmmVariableEnable            |TRUE
gSiPkgTokenSpaceGuid.PcdAtaEnable                    |FALSE
#[-start-200420-IB17800056-modify]#
gSiPkgTokenSpaceGuid.PcdHgEnable                     |TRUE
#gSiPkgTokenSpaceGuid.PcdHgEnable                     |FALSE
#[-end-200420-IB17800056-modify]#
gSiPkgTokenSpaceGuid.PcdAcpiEnable                   |TRUE
gSiPkgTokenSpaceGuid.PcdSourceDebugEnable            |FALSE
gSiPkgTokenSpaceGuid.PcdPpmEnable                    |TRUE
gSiPkgTokenSpaceGuid.PcdTxtEnable                    |FALSE
#[-start-200103-IB16740000-modify]#
gSiPkgTokenSpaceGuid.PcdAmtEnable                    |$(AMT_ENABLE)
#[-end-200103-IB16740000-modify]#
gSiPkgTokenSpaceGuid.PcdPttEnable                    |FALSE
gSiPkgTokenSpaceGuid.PcdJhiEnable                    |FALSE
gSiPkgTokenSpaceGuid.PcdSmbiosEnable                 |TRUE
gSiPkgTokenSpaceGuid.PcdS3Enable                     |TRUE
gSiPkgTokenSpaceGuid.PcdOverclockEnable              |FALSE
gSiPkgTokenSpaceGuid.PcdCpuPowerOnConfigEnable       |FALSE
gSiPkgTokenSpaceGuid.PcdBdatEnable                   |TRUE
gSiPkgTokenSpaceGuid.PcdIgdEnable                    |TRUE
gSiPkgTokenSpaceGuid.PcdSaDmiEnable                  |TRUE
gSiPkgTokenSpaceGuid.PcdIpuEnable                    |TRUE
gSiPkgTokenSpaceGuid.PcdGnaEnable                    |TRUE
gSiPkgTokenSpaceGuid.PcdVtdEnable                    |TRUE
#[-start-200103-IB16740000-modify]#
gSiPkgTokenSpaceGuid.PcdBiosGuardEnable              |$(BIOS_GUARD_SUPPORT)  #BiosGuardModule.bin
#[-end-200103-IB16740000-modify]#
gSiPkgTokenSpaceGuid.PcdOptimizeCompilerEnable       |TRUE
gSiPkgTokenSpaceGuid.PcdPeiDisplayEnable             |TRUE
gSiPkgTokenSpaceGuid.PcdSiCatalogDebugEnable         |FALSE
gSiPkgTokenSpaceGuid.PcdOcWdtEnable                  |TRUE
gSiPkgTokenSpaceGuid.PcdBootGuardEnable              |TRUE
gSiPkgTokenSpaceGuid.PcdSerialIoUartEnable           |TRUE

gSiPkgTokenSpaceGuid.PcdITbtEnable                   |TRUE
gSiPkgTokenSpaceGuid.PcdThcEnable                    |TRUE
gSiPkgTokenSpaceGuid.PcdPpamEnable                   |FALSE
gSiPkgTokenSpaceGuid.PcdVmdEnable                    |TRUE
gSiPkgTokenSpaceGuid.PcdPsmiEnable                   |TRUE
gSiPkgTokenSpaceGuid.PcdEmbeddedEnable               |0x0
gSiPkgTokenSpaceGuid.PcdCpuPcieEnable                |TRUE
gSiPkgTokenSpaceGuid.PcdTmeLibSupported              |TRUE
gSiPkgTokenSpaceGuid.PcdAdlLpSupport                 |FALSE
!if gSiPkgTokenSpaceGuid.PcdPpamEnable == TRUE
#
# PCD for State Save Support on DGR
# TRUE - SMM State Save region access is protected
# FALSE - SMM can have Read/Write access to SMM State Save region
#
gSiPkgTokenSpaceGuid.PcdSpsStateSaveEnable           |FALSE
#
# PCD to enable SPA Support on DGR
# Note: This PCD is mainly used for Debugging purpose. Not recommended to set for End Product.
#
gSiPkgTokenSpaceGuid.PcdSpaEnable                    |FALSE
!endif

[PcdsFixedAtBuild.common]
gEfiMdePkgTokenSpaceGuid.PcdPciExpressBaseAddress       |0xC0000000
gSiPkgTokenSpaceGuid.PcdSiPciExpressBaseAddress         |gEfiMdePkgTokenSpaceGuid.PcdPciExpressBaseAddress
gSiPkgTokenSpaceGuid.PcdTemporaryPciExpressRegionLength |0x10000000

[PcdsDynamicDefault.common]
gSiPkgTokenSpaceGuid.PcdPciExpressRegionLength          |0x10000000
## Specifies the AP wait loop state during POST phase.
#  The value is defined as below.
#  1: Place AP in the Hlt-Loop state.
#  2: Place AP in the Mwait-Loop state.
#  3: Place AP in the Run-Loop state.
# @Prompt The AP wait loop state.
gUefiCpuPkgTokenSpaceGuid.PcdCpuApLoopMode|2
## Specifies the AP target C-state for Mwait during POST phase.
#  The default value 0 means C1 state.
#  The value is defined as below.<BR><BR>
# @Prompt The specified AP target C-state for Mwait.
gUefiCpuPkgTokenSpaceGuid.PcdCpuApTargetCstate|0

[Defines]
  PLATFORM_NAME = ClientOneSiliconPkg
  PLATFORM_GUID = CCD38CA7-61D3-4185-9CDA-A9FDF209CB31
  PLATFORM_VERSION = 0.4
  DSC_SPECIFICATION = 0x00010005
  OUTPUT_DIRECTORY = Build/ClientOneSiliconPkg
  SUPPORTED_ARCHITECTURES = IA32|X64
  BUILD_TARGETS = DEBUG|RELEASE
  SKUID_IDENTIFIER = DEFAULT

  DEFINE   PLATFORM_SI_PACKAGE        = ClientOneSiliconPkg
  DEFINE   C1S_PRODUCT_PATH           = ClientOneSiliconPkg/Product/AlderLake
  #
  # Definition for Build Flag
  #
  !include ClientOneSiliconPkg/Package.env

#[-start-210421-IB09480139-add]#
#
#  Notice!! This section is for build tool behavior. Avoid build tool first process the Defines section.
#
[PcdsFixedAtBuild]
#[-end-210421-IB09480139-add]#
  #
  # Silicon On/Off feature are defined here
  #
  !include $(C1S_PRODUCT_PATH)/SiPkgPcdInit.dsc

[LibraryClasses.common]
  #
  # Entry point
  #
  PeiCoreEntryPoint|MdePkg/Library/PeiCoreEntryPoint/PeiCoreEntryPoint.inf
  PeimEntryPoint|MdePkg/Library/PeimEntryPoint/PeimEntryPoint.inf
  DxeCoreEntryPoint|MdePkg/Library/DxeCoreEntryPoint/DxeCoreEntryPoint.inf
  UefiDriverEntryPoint|MdePkg/Library/UefiDriverEntryPoint/UefiDriverEntryPoint.inf
  UefiApplicationEntryPoint|MdePkg/Library/UefiApplicationEntryPoint/UefiApplicationEntryPoint.inf
  PeCoffExtraActionLib|MdePkg/Library/BasePeCoffExtraActionLibNull/BasePeCoffExtraActionLibNull.inf

  #
  # Basic
  #
  BaseLib|MdePkg/Library/BaseLib/BaseLib.inf
  BaseMemoryLib|MdePkg/Library/BaseMemoryLibRepStr/BaseMemoryLibRepStr.inf
  PrintLib|MdePkg/Library/BasePrintLib/BasePrintLib.inf
  CpuLib|MdePkg/Library/BaseCpuLib/BaseCpuLib.inf
  IoLib|MdePkg/Library/BaseIoLibIntrinsic/BaseIoLibIntrinsic.inf
  PciSegmentLib|$(PLATFORM_SI_PACKAGE)/Library/BasePciSegmentMultiSegLibPci/BasePciSegmentMultiSegLibPci.inf
  PciLib|MdePkg/Library/BasePciLibPciExpress/BasePciLibPciExpress.inf
  PciCf8Lib|MdePkg/Library/BasePciCf8Lib/BasePciCf8Lib.inf
  PciExpressLib|$(PLATFORM_SI_PACKAGE)/Library/BasePciExpressMultiSegLib/BasePciExpressMultiSegLib.inf
  CacheMaintenanceLib|MdePkg/Library/BaseCacheMaintenanceLib/BaseCacheMaintenanceLib.inf
  PeCoffLib|MdePkg/Library/BasePeCoffLib/BasePeCoffLib.inf
  PeCoffGetEntryPointLib|MdePkg/Library/BasePeCoffGetEntryPointLib/BasePeCoffGetEntryPointLib.inf
  PeiVrFruLib|$(PLATFORM_SI_PACKAGE)/Fru/AdlCpu/VoltageRegulator/LibraryPrivate/PeiVrFruLib/PeiVrFruLib.inf

  #
  # UEFI & PI
  #
  UefiBootServicesTableLib|MdePkg/Library/UefiBootServicesTableLib/UefiBootServicesTableLib.inf
  UefiRuntimeServicesTableLib|MdePkg/Library/UefiRuntimeServicesTableLib/UefiRuntimeServicesTableLib.inf
  UefiRuntimeLib|MdePkg/Library/UefiRuntimeLib/UefiRuntimeLib.inf
  UefiLib|MdePkg/Library/UefiLib/UefiLib.inf
  DevicePathLib|MdePkg/Library/UefiDevicePathLib/UefiDevicePathLib.inf
  PeiServicesTablePointerLib|MdePkg/Library/PeiServicesTablePointerLibIdt/PeiServicesTablePointerLibIdt.inf
  PeiServicesLib|MdePkg/Library/PeiServicesLib/PeiServicesLib.inf
  DxeServicesLib|MdePkg/Library/DxeServicesLib/DxeServicesLib.inf
  DxeServicesTableLib|MdePkg/Library/DxeServicesTableLib/DxeServicesTableLib.inf

#[-start-190816-IB17700068-remove]#
#   S3BootScriptLib|MdePkg/Library/BaseS3BootScriptLibNull/BaseS3BootScriptLibNull.inf
#[-end-190816-IB17700068-remove]#
  S3IoLib|MdePkg/Library/BaseS3IoLib/BaseS3IoLib.inf
  S3PciLib|MdePkg/Library/BaseS3PciLib/BaseS3PciLib.inf
  S3PciSegmentLib|MdePkg/Library/BaseS3PciSegmentLib/BaseS3PciSegmentLib.inf

  UefiUsbLib|MdePkg/Library/UefiUsbLib/UefiUsbLib.inf
  UefiScsiLib|MdePkg/Library/UefiScsiLib/UefiScsiLib.inf
  SynchronizationLib|MdePkg/Library/BaseSynchronizationLib/BaseSynchronizationLib.inf
#[-start-190806-IB16740000-remove]
#  DebugPrintErrorLevelLib|MdePkg/Library/BaseDebugPrintErrorLevelLib/BaseDebugPrintErrorLevelLib.inf
#[-end-190806-IB16740000-remove]
  #
  # Misc
  #
#[-start-190806-IB16740000-remove]
#  DebugLib|MdePkg/Library/BaseDebugLibNull/BaseDebugLibNull.inf
#  PerformanceLib|MdePkg/Library/BasePerformanceLibNull/BasePerformanceLibNull.inf
#  PcdLib|MdePkg/Library/BasePcdLibNull/BasePcdLibNull.inf
#  TimerLib|MdePkg/Library/BaseTimerLibNullTemplate/BaseTimerLibNullTemplate.inf
#  PostCodeLib|MdePkg/Library/BasePostCodeLibDebug/BasePostCodeLibDebug.inf
#  ReportStatusCodeLib|MdePkg/Library/BaseReportStatusCodeLibNull/BaseReportStatusCodeLibNull.inf
#  MtrrLib|UefiCpuPkg/Library/MtrrLib/MtrrLib.inf
#[-end-190806-IB16740000-remove]
  RngLib|MdePkg/Library/BaseRngLib/BaseRngLib.inf

#####################################################################################################

#
# Silicon Init Common Library
#
!include $(C1S_PRODUCT_PATH)/SiPkgCommonLib.dsc
ConfigBlockLib|IntelSiliconPkg/Library/BaseConfigBlockLib/BaseConfigBlockLib.inf

[LibraryClasses.IA32.SEC]
  SecGetFsptApiParameterLib|$(PLATFORM_SI_PACKAGE)/Library/SecGetFsptApiParameterLib/SecGetFsptApiParameterLib.inf
  GpioHelpersLib|$(PLATFORM_SI_PACKAGE)/IpBlock/Gpio/LibraryPrivate/BaseGpioHelpersLibNull/BaseGpioHelpersLibNull.inf

[LibraryClasses.$(PEI_ARCH)]
#
# PEI phase common
#
#[-start-190806-IB16740000-remove]
#  PcdLib|MdePkg/Library/PeiPcdLib/PeiPcdLib.inf
#  HobLib|MdePkg/Library/PeiHobLib/PeiHobLib.inf
#  MemoryAllocationLib|MdePkg/Library/PeiMemoryAllocationLib/PeiMemoryAllocationLib.inf
#[-end-190806-IB16740000-remove]
  ExtractGuidedSectionLib|MdePkg/Library/PeiExtractGuidedSectionLib/PeiExtractGuidedSectionLib.inf

#####################################################################################################################################

#
# Silicon Init Pei Library
#
!include $(C1S_PRODUCT_PATH)/SiPkgPeiLib.dsc

[LibraryClasses.IA32.SEC]
  ReportStatusCodeLib|MdePkg/Library/BaseReportStatusCodeLibNull/BaseReportStatusCodeLibNull.inf

[LibraryClasses.X64]
 #
 # DXE phase common
 #
#[-start-190806-IB16740000-remove]
#  HobLib|MdePkg/Library/DxeHobLib/DxeHobLib.inf
#  PcdLib|MdePkg/Library/DxePcdLib/DxePcdLib.inf
#  MemoryAllocationLib|MdePkg/Library/UefiMemoryAllocationLib/UefiMemoryAllocationLib.inf
#[-end-190806-IB16740000-remove]
  ExtractGuidedSectionLib|MdePkg/Library/DxeExtractGuidedSectionLib/DxeExtractGuidedSectionLib.inf

#
# Hsti
#
  HstiLib|MdePkg/Library/DxeHstiLib/DxeHstiLib.inf

###################################################################################################
#
# Silicon Init Dxe Library
#
!include $(C1S_PRODUCT_PATH)/SiPkgDxeLib.dsc
  PostCodeLib|$(PLATFORM_SI_PACKAGE)/Library/BasePostCodeLibPort80TraceHub/BasePostCodeLibPort80TraceHub.inf
  PostCodeToScratchPadLib|$(PLATFORM_SI_PACKAGE)/Library/BasePostCodeToScratchPadLibNull/BasePostCodeToScratchPadLibNull.inf


[LibraryClasses.X64.PEIM]

[LibraryClasses.X64.DXE_CORE]
#[-start-190806-IB16740000-remove]
#  HobLib|MdePkg/Library/DxeCoreHobLib/DxeCoreHobLib.inf
#  PcdLib|MdePkg/Library/BasePcdLibNull/BasePcdLibNull.inf
#[-end-190806-IB16740000-remove]
[LibraryClasses.X64.DXE_SMM_DRIVER]
  SmmServicesTableLib|MdePkg/Library/SmmServicesTableLib/SmmServicesTableLib.inf
  MemoryAllocationLib|MdePkg/Library/SmmMemoryAllocationLib/SmmMemoryAllocationLib.inf
  SmmIoLib|MdePkg/Library/SmmIoLib/SmmIoLib.inf
#[-start-180731-IB15410171-modify]#
  SmmMemLib|MdePkg/Library/SmmMemLib/SmmMemLib.inf {
    <SOURCE_OVERRIDE_PATH>
      MdePkg/Override/Library/SmmMemLib
  }
#[-end-180731-IB15410171-modify]#

[LibraryClasses.X64.SMM_CORE]

[LibraryClasses.X64.UEFI_DRIVER]
#[-start-190806-IB16740000-remove]
#  PcdLib|MdePkg/Library/BasePcdLibNull/BasePcdLibNull.inf
#[-end-190806-IB16740000-remove]

[LibraryClasses.X64.UEFI_APPLICATION]
#[-start-190806-IB16740000-remove]
#  PcdLib|MdePkg/Library/DxePcdLib/DxePcdLib.inf
#[-end-190806-IB16740000-remove]

[Components.IA32]
#
# CpuMp PEIM for MpService PPI
#
!if gIntelFsp2WrapperTokenSpaceGuid.PcdFspModeSelection == 1
  UefiCpuPkg/CpuMpPei/CpuMpPei.inf {
    <LibraryClasses>
      NULL|$(PLATFORM_SI_PACKAGE)/Library/PeiReadyToInstallMpLib/PeiReadyToInstallMpLib.inf
  }
!endif #PcdFspModeSelection

#[-start-200220-IB10189038-modify]#
!if gSiPkgTokenSpaceGuid.PcdFspWrapperEnable == FALSE
!include $(C1S_PRODUCT_PATH)/SiPkgPei.dsc
!endif
#[-end-200220-IB10189038-modify]#

[Components.X64]
!include $(C1S_PRODUCT_PATH)/SiPkgDxe.dsc
#
# AcpiTables
#
$(PLATFORM_SI_PACKAGE)/Pch/AcpiTables/Dsdt/PchAcpiTablesSelfTest.inf
