/** @file
  Header file of CPU feature control module

@copyright
  INTEL CONFIDENTIAL
  Copyright 1999 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains an 'Intel Peripheral Driver' and is uniquely identified as
  "Intel Reference Module" and is licensed for Intel CPUs and chipsets under
  the terms of your license agreement with Intel or your vendor. This file may
  be modified by the user, subject to additional terms of the license agreement.

@par Specification
**/
#ifndef _FEATURES_H_
#define _FEATURES_H_

#include <Library/DebugLib.h>
#include <Library/BaseMemoryLib.h>
#include <Library/SynchronizationLib.h>
#include <Library/PeiServicesLib.h>
#include <Library/PeiServicesTablePointerLib.h>
#include <Library/HobLib.h>
#include <Library/MemoryAllocationLib.h>
#include <Library/PciSegmentLib.h>
#include <Library/CpuInitLib.h>
#include <CpuInitDataHob.h>
#include <Ppi/MpServices2.h>
#include <Ppi/SiPolicy.h>
#include <Register/Cpuid.h>
#include <Register/Msr.h>
#include <CpuDataStruct.h>
#include <PowerMgmtNvsStruct.h>
#include <Register/HeciRegs.h>
#include <Register/MeRegs.h>
#include <Register/ArchMsr.h>
#include <Register/CommonMsr.h>
#include <Library/MsrFruLib.h>
#include <Register/Intel/Microcode.h>

extern EDKII_PEI_MP_SERVICES2_PPI  *mMpServices2Ppi;

/**
  Program all processor features basing on desired settings

  @param[in] Buffer - .A pointer to a buffer used to pass Cpu Policy PPI
**/
VOID
EFIAPI
ProgramProcessorFeature (
  IN VOID *Buffer
  );

/**
  Get Trace Hub Acpi Base address for BSP
**/
VOID
EFIAPI
GetTraceHubAcpiBaseAddressForBsp (
  VOID
  );

/**
  Initialize performance and power management features before RESET_CPL at Post-memory phase.

  @param[in] SiPolicyPpi     The SI Policy PPI instance.
**/
VOID
CpuInitPreResetCpl (
  IN SI_POLICY_PPI            *SiPolicyPpi
  );


/**
  This will check if the microcode address is valid for this processor, and if so, it will
  load it to the processor.

  @param[in]  MicrocodeAddress      - The address of the microcode update binary (in memory).
  @param[in]  MicrocodeRegionSize   - Size of microcode region (in memory).

  @retval EFI_SUCCESS             - A new microcode update is loaded.
  @retval Other                   - Due to some reason, no new microcode update is loaded.
**/
EFI_STATUS
InitializeMicrocode (
  IN  CPU_MICROCODE_HEADER *MicrocodeAddress,
  IN  UINT32               MicrocodeRegionSize
  );

#endif
