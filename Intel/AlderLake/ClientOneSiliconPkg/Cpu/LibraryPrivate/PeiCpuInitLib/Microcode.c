/** @file
  CPU microcode update library.

@copyright
  INTEL CONFIDENTIAL
  Copyright 2015 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains an 'Intel Peripheral Driver' and is uniquely identified as
  "Intel Reference Module" and is licensed for Intel CPUs and chipsets under
  the terms of your license agreement with Intel or your vendor. This file may
  be modified by the user, subject to additional terms of the license agreement.

@par Specification
**/

#include <Library/MicrocodeLib.h>
#include <Library/DebugLib.h>
#include <Library/SynchronizationLib.h>
#include <Library/CpuPlatformLib.h>
#include <Library/CpuCommonLib.h>
#include "Features.h"

/**
  This will check if the microcode address is valid for this processor, and if so, it will
  load it to the processor.

  @param[in]  Microcode      - The address of the microcode update binary (in memory).
  @param[in]  MicrocodeSize  - Size of microcode region (in memory).

  @retval EFI_SUCCESS             - A new microcode update is loaded.
  @retval Other                   - Due to some reason, no new microcode update is loaded.
**/
EFI_STATUS
InitializeMicrocode (
  IN  CPU_MICROCODE_HEADER *Microcode,
  IN  UINT32               MicrocodeSize
  )
{
  UINT32                        LatestRevision;
  UINT32                        CurrentRevision;
  UINTN                         MicrocodeEnd;
  CPU_MICROCODE_HEADER          *LatestMicrocode;
  EDKII_PEI_MICROCODE_CPU_ID    MicrocodeCpuId;
  MSR_IA32_PLATFORM_ID_REGISTER Msr;

  Msr.Uint64 = AsmReadMsr64 (MSR_IA32_PLATFORM_ID);
  MicrocodeCpuId.PlatformId = (UINT8) Msr.Bits.PlatformId;
  AsmCpuid (CPUID_VERSION_INFO, &MicrocodeCpuId.ProcessorSignature, NULL, NULL, NULL);

  if (IsBsp ()) {
    DEBUG ((DEBUG_INFO, "[InitializeMicrocode]: Microcode Region Address = %x, Size = %d\n", Microcode, MicrocodeSize));
  }

  CurrentRevision = GetProcessorMicrocodeSignature ();

  LatestRevision  = 0;
  LatestMicrocode = NULL;
  MicrocodeEnd = (UINTN) Microcode + MicrocodeSize;

  do {
    if (!IsValidMicrocode (Microcode, MicrocodeEnd - (UINTN) Microcode, LatestRevision, &MicrocodeCpuId, 1, TRUE)) {
      //
      // Microcode is aligned in 1KB. Check next 1KB when invalid header is met.
      //
      Microcode = (CPU_MICROCODE_HEADER *) ((UINTN) Microcode + SIZE_1KB);
      continue;
    }
    LatestMicrocode = Microcode;
    LatestRevision  = LatestMicrocode->UpdateRevision;
    if (IsBsp ()) {
      DEBUG ((DEBUG_INFO, "[InitializeMicrocode]: Find a potential patch [revision = 0x%x]!\n", LatestRevision));
    }
    Microcode = (CPU_MICROCODE_HEADER *) (((UINTN) Microcode) + GetMicrocodeLength (Microcode));
  } while ((UINTN) Microcode < MicrocodeEnd);

  if (LatestRevision >= CurrentRevision) {
    if (IsBsp ()) {
      DEBUG ((DEBUG_INFO, "LoadMicrocode: Before load patch [revision = 0x%x], revision = 0x%x\n", LatestRevision, CurrentRevision));
    }
    LoadMicrocode (LatestMicrocode);
    if (IsBsp ()) {
      DEBUG ((DEBUG_INFO, "LoadMicrocode: After load [revision = 0x%x], revision = 0x%x\n", LatestRevision, GetProcessorMicrocodeSignature ()));
    }
    return EFI_SUCCESS;
  }
  return EFI_NOT_FOUND;
}
