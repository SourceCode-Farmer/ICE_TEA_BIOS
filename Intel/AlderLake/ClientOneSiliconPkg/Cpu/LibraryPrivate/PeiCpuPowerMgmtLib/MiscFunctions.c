/** @file
  This file contains Processor Power Management ACPI related functions for
  processors.

  <b>Acronyms:</b>
     - PPM: Processor Power Management
     - TM:  Thermal Monitor
     - IST: Intel(R) Speedstep technology
     - HT:  Hyper-Threading Technology

@copyright
  INTEL CONFIDENTIAL
  Copyright 2012 - 2020 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains an 'Intel Peripheral Driver' and is uniquely identified as
  "Intel Reference Module" and is licensed for Intel CPUs and chipsets under
  the terms of your license agreement with Intel or your vendor. This file may
  be modified by the user, subject to additional terms of the license agreement.

@par Specification
**/

#include "PowerMgmtCommon.h"

/**
  This will perform Miscellaneous Power Management related programming.

  @param[in] CtdpSupport  Status of InitializeConfigurableTdp funtion
**/
VOID
InitMiscFeatures (
  EFI_STATUS CtdpSupport
  )
{
  if (mCpuConfig->SkipMpInit == 0) {

    ///
    /// Configure Package Turbo Power Limits
    ///
    if ((CtdpSupport == EFI_SUCCESS) && (gCpuPowerMgmtBasicConfig->ApplyConfigTdp == 1)) {
      ConfigureCtdp ();
    } else {
      ConfigurePowerLimitsNonConfigTdpSkus ();
    }


    ///
    /// Configure PL3 limits
    ///
    ConfigurePl3PowerLimits ();

    ///
    /// Configure PL4 limits
    ///
    ConfigurePl4PowerLimits ();

    ///
    /// Configure DDR RAPL PowerLimits
    ///
    ConfigureDdrPowerLimits ();
  }
  ///
  /// Configure Platform Power Limits
  ///
  ConfigurePlatformPowerLimits ();
}

/**
  Locks down all settings.
**/
VOID
PpmLockDown (
  VOID
  )
{
  MSR_PACKAGE_RAPL_LIMIT_REGISTER   PkgRaplLimitMsr;
  MSR_POWER_CTL_REGISTER            PowerCtl;
  MSR_DDR_RAPL_LIMIT_REGISTER       DdrRaplLimitMsr;
  MSR_MISC_PWR_MGMT_REGISTER        MiscPwrMgmtMsr;
  ///
  /// Program PMG_CST_CONFIG MSR [15] (CFG lock bit)
  ///
  ApSafeLockDown (gCpuPowerMgmtTestConfig);
  mMpServices2Ppi->StartupAllAPs (
                    mMpServices2Ppi,
                    (EFI_AP_PROCEDURE) ApSafeLockDown,
                    FALSE,
                    0,
                    (VOID *) gCpuPowerMgmtTestConfig
                    );
  ///
  /// Lock Package power limit MSR
  ///
  if (gCpuPowerMgmtBasicConfig->TurboPowerLimitLock) {
    PkgRaplLimitMsr.Uint64 = AsmReadMsr64 (MSR_PACKAGE_RAPL_LIMIT);
    PkgRaplLimitMsr.Bits.PkgPwrLimLock = 1;
    AsmWriteMsr64 (MSR_PACKAGE_RAPL_LIMIT, PkgRaplLimitMsr.Uint64);
  }

  ///
  /// Program the PROCHOT_Lock
  ///
  if (gCpuPowerMgmtTestConfig->ProcHotLock) {
    PowerCtl.Uint64 = AsmReadMsr64 (MSR_POWER_CTL);
    PowerCtl.Bits.ProchotLock = 1;
    AsmWriteMsr64 (MSR_POWER_CTL, PowerCtl.Uint64);
  }

  ///
  /// Program Ddr RAPL LIMIT Lock
  ///
  if (gCpuPowerMgmtBasicConfig->TurboPowerLimitLock) {
    DdrRaplLimitMsr.Uint64 = AsmReadMsr64 (MSR_DDR_RAPL_LIMIT);
    DdrRaplLimitMsr.Bits.Locked = 1;
    AsmWriteMsr64 (MSR_DDR_RAPL_LIMIT, DdrRaplLimitMsr.Uint64);
  }

  ///
  /// Program the HWP Lock BIT in MISC PWR MGMT MSR
  ///
  if (gCpuPowerMgmtBasicConfig->HwpLock) {
    MiscPwrMgmtMsr.Uint64 = AsmReadMsr64 (MSR_MISC_PWR_MGMT);
    MiscPwrMgmtMsr.Bits.Lock = 1;
    AsmWriteMsr64 (MSR_MISC_PWR_MGMT, MiscPwrMgmtMsr.Uint64);
  }
  return;
}

/**
  Lock MSR_CST_CONFIG_CONTROL.
  This function must be MP safe.

  @param[in] Buffer    Pointer to the function parameters passed in.
**/
VOID
EFIAPI
ApSafeLockDown (
  IN OUT VOID *Buffer
  )
{
  MSR_CLOCK_CST_CONFIG_CONTROL_REGISTER     PmCfgCtrlMsr;
  CPU_POWER_MGMT_TEST_CONFIG                *CpuPowerMgmtTestConfig;
  UINT8                                     CfgLock;

  CfgLock = TRUE;

  CpuPowerMgmtTestConfig = (CPU_POWER_MGMT_TEST_CONFIG *) Buffer;
  if (CpuPowerMgmtTestConfig != NULL) {
    CfgLock = (UINT8) CpuPowerMgmtTestConfig->PmgCstCfgCtrlLock;
  }
  PmCfgCtrlMsr.Uint64 = AsmReadMsr64 (MSR_CLOCK_CST_CONFIG_CONTROL);
  PmCfgCtrlMsr.Bits.Lock = CfgLock;
  AsmWriteMsr64 (MSR_CLOCK_CST_CONFIG_CONTROL, PmCfgCtrlMsr.Uint64);

  return;
}

/**
  Dump FVID Tables.

  @param[in out] FvidPointer         Pointer to a table to be updated
  @param[in]     LpssNumberOfStates  Number of entries in the table pointed to by FvidPointer for LPSS
  @param[in]     LpssNumberOfStates  Number of entries in the table pointed to by FvidPointer for TPSS
**/
VOID
DumpFvidTable (
  IN OUT FVID_TABLE *FvidPointer,
  IN UINT16         LpssNumberOfStates,
  IN UINT16         TpssNumberOfStates
  )
{
  UINTN             Index;

  //
  // Print LPSS and TPSS FVID Tables
  //
  DEBUG ((DEBUG_INFO, "LPSS FVID Table (%d)\n", LpssNumberOfStates));
  DEBUG ((DEBUG_INFO, "Index\tState\tRatio\tPower\n"));
  for (Index = 1; Index <= LpssNumberOfStates ; Index++) {
    DEBUG ((DEBUG_INFO, "%d\t%d\t%d\t%d\n",Index, FvidPointer[Index].FvidState.Limit16State, FvidPointer[Index].FvidState.Limit16BusRatio, FvidPointer[Index].FvidState.Limit16Power));
  }

  DEBUG ((DEBUG_INFO, "TPSS FVID Table (%d)\n", TpssNumberOfStates));
  DEBUG ((DEBUG_INFO, "Index\tState\tRatio\tPower\n"));
  for (Index = 1; Index <= TpssNumberOfStates ; Index++) {
    DEBUG ((DEBUG_INFO, "%d\t%d\t%d\t%d\n",Index, FvidPointer[Index].FvidState.State, FvidPointer[Index].FvidState.BusRatio, FvidPointer[Index].FvidState.Power));
  }
}

