 /** @file
  This file contains generation specific interfaces for PEI CPU initialization.

@copyright
  INTEL CONFIDENTIAL
  Copyright 2020 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains an 'Intel Peripheral Driver' and is uniquely identified as
  "Intel Reference Module" and is licensed for Intel CPUs and chipsets under
  the terms of your license agreement with Intel or your vendor. This file may
  be modified by the user, subject to additional terms of the license agreement.

@par Specification Reference:
**/
#ifndef _PEI_CPU_INIT_FRU_LIB_H_
#define _PEI_CPU_INIT_FRU_LIB_H_

#include <ConfigBlock.h>
#include <ConfigBlock/CpuPowerMgmtBasicConfig.h>
#include <CpuPowerMgmtVrConfig.h>
#include <Library/DebugLib.h>
#include <Library/BaseLib.h>
#include <Library/CpuCommonLib.h>

/**
  CPU SSC configuration for specific generation.

  @param[in] ComputeDieSscEnable    - Enable/Disable Compute Die SSC.
  @param[in] SocDieSscEnable        - Enable/Disable Soc Die SSC.
  @param[in] ComputeDieSscValue     - Compute Die SSC value to config.
  @param[in] SocDieSscValue         - Soc Die SSC value to config.

**/
EFI_STATUS
EFIAPI
PeiCpuSscConfigPreMem (
  IN BOOLEAN    ComputeDieSscEnable,
  IN BOOLEAN    SocDieSscEnable,
  IN UINT8      ComputeDieSscValue,
  IN UINT8      SocDieSscValue
  );

/**
  Load CPU power management basic Config block default for specific generation.

  @param[in, out] CpuPowerMgmtBasicConfig   Pointer to CPU_POWER_MGMT_BASIC_CONFIG instance

**/
VOID
EFIAPI
PeiCpuLoadPowerMgmtBasicConfigDefault (
  IN OUT CPU_POWER_MGMT_BASIC_CONFIG    *CpuPowerMgmtBasicConfig
  );

/**
  Set Turbo Ratio Limit for specific generation.

  @param[in] CpuPowerMgmtBasicConfig    Pointer to CPU_POWER_MGMT_BASIC_CONFIG instance

**/
VOID
EFIAPI
PeiCpuSetTurboRatioLimit (
  IN CPU_POWER_MGMT_BASIC_CONFIG    *CpuPowerMgmtBasicConfig
  );

/**
  Compare the hyper threading setup option against the CPU strap setting
  and in case of mismatch request a reset.

  @param[in] CpuStrapSetData           - The current strap setting data.
  @param[in] HyperThreading            - hyper threading setting from BIOS Setup option.

  @retval NO_RESET          - No reset is needed.
  @retval WARM_RESET        - Update requires a warm reset.
**/
CPU_RESET_TYPE
PeiCpuStrapHtEnableDisable (
  IN VOID              *CpuStrapSetData,
  IN UINT8             HyperThreading
  );

/**
  Compare the number of active cores setup option against the CPU strap setting
  and in case of mismatch request a reset.

  @param[in] CpuStrapSetData             - The current strap setting.
  @param[in] ActiveCoreCount             - Active big core count.
  @param[in] ActiveSmallCoreCount        - Active small core count.
  @param[in] ActiveSocNorthAtomCoreCount - Active Soc North atom core count.

  @retval NO_RESET          - No reset is needed.
  @retval WARM_RESET        - Update requires a warm reset.
  @retval COLD_RESET        - Update requires a cold reset.
**/
CPU_RESET_TYPE
PeiCpuStrapSetActiveCores (
  IN VOID              *CpuStrapSetData,
  IN UINT8             ActiveCoreCount,
  IN UINT8             ActiveSmallCoreCount,
  IN UINT8             ActiveSocNorthAtomCoreCount
  );

/**
  Compare the BIST setup option against the CPU strap setting
  and in case of mismatch request a reset.

  @param[in] CpuStrapSetData    - The current strap setting.
  @param[in] BistOnReset        - BistOnReset CPU Test Config Policy.

  @retval NO_RESET          - No reset is needed.
  @retval WARM_RESET        - Update requires a warm reset.
**/
CPU_RESET_TYPE
PeiCpuStrapBistEnableDisable (
  IN VOID              *CpuStrapSetData,
  IN UINT8             BistOnReset
  );

/**
  Compare the flex multiplier setup options against the CPU strap settings
  and in case of mismatch request a reset.

  @param[in] CpuStrapSetData    - The current strap setting.
  @param[in] CpuRatio           - CpuRatio CPU policy.

  @retval NO_RESET          - No reset is needed.
  @retval WARM_RESET        - Update requires a warm reset.
**/
CPU_RESET_TYPE
PeiCpuStrapProgramFlexMultiplier (
  IN VOID                        *CpuStrapSetData,
  IN UINT8                       CpuRatio
  );

/**
  Compare the boot frequency setup option against the boot ratio strap setting
  and in case of mismatch request a reset.

  @param[in] CpuStrapSetData    - The current strap setting.
  @param[in] BootFrequency      - BootFrequency CPU policy.

  @retval NO_RESET          - No reset is needed.
  @retval WARM_RESET        - Update requires a warm reset.
**/
CPU_RESET_TYPE
PeiCpuStrapBootRatioEnableDisable (
  IN VOID                *CpuStrapSetData,
  IN UINT8               BootFrequency
  );

/**
  Compare the JTAG power gate setup option against the CPU strap setting
  and in case of mismatch request a reset.

  @param[in] CpuStrapSet    - The current strap setting.
  @param[in] JtagC10PowerGateDisable - JtagC10PowerGateDisable CPU policy.

  @retval NO_RESET          - No reset is needed.
  @retval WARM_RESET        - Update requires a warm reset.
**/
CPU_RESET_TYPE
PeiCpuStrapJtagPowerGateEnableDisable (
  IN VOID                    *CpuStrapSetData,
  IN UINT8                   JtagC10PowerGateDisable
  );

/**
  Compare the Dlvr Bypass Mode setup option against the CPU strap setting
  and in case of mismatch request a reset.

  @param[in] CpuStrapSetData    - The current strap setting.
  @param[in] DlvrBypassMode     - DlvrBypassMode Config Policy.

  @retval NO_RESET          - No reset is needed.
  @retval WARM_RESET        - Update requires a warm reset.
**/
CPU_RESET_TYPE
PeiCpuStrapDlvrSetting (
  IN VOID              *CpuStrapSetData,
  IN UINT8             DlvrBypassModeEnable
  );

/**
  Return Total Memory Encryption (TME) default policy setting

  @retval Total Memory Encryption (TME) default policy setting
**/
UINT32
PeiCpuTmeDefaultSetting (
  VOID
  );

/**
  Disable or enable CPU Crashlog dump feature by Mailbox.

  @param[in] BOOLEAN      Policy for CrashLog
**/
VOID
PeiCpuCrashLogMailboxEnable (
  IN  BOOLEAN  CrashLogEnable
  );

/**
  Program the BIOS Parameters required for MCHECK module.
**/
VOID
ProgramBiosParamsForMcheck (
  VOID
  );

/**
  Return the boot frequency default policy setting

  @retval Boot Frequency default policy setting
**/
UINT32
PeiCpuBootFreqDefaultSetting (
  VOID
  );

/**
  Set the PRMRR region.

  @param PrmrrBase PRMRR base address
  @param PrmrrSize PRMRR size

  @retval RETURN_SUCCESS       PRMRR region is set successfully.
  @retval RETURN_UNSUPPORTED   PRMRR is not supported.
  @retval RETURN_ACCESS_DENIED PRMRR region is already locked and cannot be changed.
**/
RETURN_STATUS
PeiCpuSetPrmrrRegion (
  UINT64 PrmrrBase,
  UINT32 PrmrrSize
  );

/**
  Configure INF_DFX power.

  @param[in] InfDfxPwrEnable. TRUE, keep INF_DFX power up; FALSE, shutdown INF_DFX power

  @retval EFI_UNSUPPORTED           Command unsupported
  @retval EFI_SUCCESS               Command succeeded
**/
EFI_STATUS
PeiCpuInfDfxPwrEnable (
  IN BOOLEAN               InfDfxPwrEnable
  );

/**
  Update CpuStrap base on platform type

  @param[in] CpuStrapSetData    - The current strap setting.
  @param[in] VendorId           - VR VendorId.
  @param[in] ProdId             - VR ProdId.

  @retval NO_RESET          - No reset is needed.
  @retval WARM_RESET        - Update requires a warm reset.
**/
CPU_RESET_TYPE
PeiCpuStrapPlatformType (
  IN UINT32 *CpuStrapSetData,
  IN UINT32 VendorId,
  IN UINT32 ProdId
  );
#endif // _PEI_CPU_INIT_FRU_LIB_H_
