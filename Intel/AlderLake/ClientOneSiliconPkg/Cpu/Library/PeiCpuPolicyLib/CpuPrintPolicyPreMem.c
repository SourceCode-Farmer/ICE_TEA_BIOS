/** @file
  This file is PeiCpuPolicy library.

@copyright
  INTEL CONFIDENTIAL
  Copyright 2014 - 2022 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains an 'Intel Peripheral Driver' and is uniquely identified as
  "Intel Reference Module" and is licensed for Intel CPUs and chipsets under
  the terms of your license agreement with Intel or your vendor. This file may
  be modified by the user, subject to additional terms of the license agreement.

@par Specification
**/
#include "PeiCpuPolicyLibrary.h"
#include <Library/ConfigBlockLib.h>

/**
  Print CPU_SECURITY_PREMEM_CONFIG and serial out.

  @param[in] CpuSecurityPreMemConfig     Pointer to a CPU_SECURITY_PREMEM_CONFIG
**/
VOID
CpuSecurityPreMemConfigPrint (
  IN CONST CPU_SECURITY_PREMEM_CONFIG        *CpuSecurityPreMemConfig
  )
{
  DEBUG ((DEBUG_INFO, "------------------ CPU_SECURITY_PREMEM_CONFIG Begin ------------------\n"));
  DEBUG ((DEBUG_INFO, " PrmrrSize = 0x%x\n", CpuSecurityPreMemConfig->PrmrrSize));
  DEBUG ((DEBUG_INFO, " BiosSize = 0x%x\n", CpuSecurityPreMemConfig->BiosSize));
  DEBUG ((DEBUG_INFO, " BiosGuard = 0x%x\n", CpuSecurityPreMemConfig->BiosGuard));
  DEBUG ((DEBUG_INFO, " BiosGuardToolsInterface = 0x%x\n", CpuSecurityPreMemConfig->BiosGuardToolsInterface));
  DEBUG ((DEBUG_INFO, " Txt = 0x%x\n", CpuSecurityPreMemConfig->Txt));
  DEBUG ((DEBUG_INFO, " SkipStopPbet = 0x%x\n", CpuSecurityPreMemConfig->SkipStopPbet));
  DEBUG ((DEBUG_INFO, " EnableC6Dram = 0x%x\n", CpuSecurityPreMemConfig->EnableC6Dram));
  DEBUG ((DEBUG_INFO, " ResetAux = 0x%x\n", CpuSecurityPreMemConfig->ResetAux));
  DEBUG ((DEBUG_INFO, " TxtAcheckRequest = 0x%x\n", CpuSecurityPreMemConfig->TxtAcheckRequest));
  DEBUG ((DEBUG_INFO, "------------------ CPU_SECURITY_PREMEM_CONFIG End ------------------\n"));

}

/**
  Print CPU_TXT_PREMEM_CONFIG and serial out.

  @param[in] CpuTxtPreMemConfig     Pointer to a CPU_TXT_PREMEM_CONFIG
**/
VOID
CpuTxtPreMemConfigPrint (
  IN CONST CPU_TXT_PREMEM_CONFIG        *CpuTxtPreMemConfig
  )
{
  DEBUG ((DEBUG_INFO, "------------------ CPU_TXT_PREMEM_CONFIG Begin ------------------\n"));
  DEBUG ((DEBUG_INFO, " SinitMemorySize = 0x%x\n", CpuTxtPreMemConfig->SinitMemorySize));
  DEBUG ((DEBUG_INFO, " TxtHeapMemorySize = 0x%x\n", CpuTxtPreMemConfig->TxtHeapMemorySize));
  DEBUG ((DEBUG_INFO, " TxtDprMemorySize = 0x%x\n", CpuTxtPreMemConfig->TxtDprMemorySize));
  DEBUG ((DEBUG_INFO, " TxtDprMemoryBase = 0x%x\n", CpuTxtPreMemConfig->TxtDprMemoryBase));
  DEBUG ((DEBUG_INFO, " TxtLcpPdSize = 0x%x\n", CpuTxtPreMemConfig->TxtLcpPdSize));
  DEBUG ((DEBUG_INFO, " TxtLcpPdBase = 0x%x\n", CpuTxtPreMemConfig->TxtLcpPdBase));
  DEBUG ((DEBUG_INFO, " ApStartupBase = 0x%x\n", CpuTxtPreMemConfig->ApStartupBase));
  DEBUG ((DEBUG_INFO, " BiosAcmBase = 0x%x\n", CpuTxtPreMemConfig->BiosAcmBase));
  DEBUG ((DEBUG_INFO, " BiosAcmSize = 0x%x\n", CpuTxtPreMemConfig->BiosAcmSize));
  DEBUG ((DEBUG_INFO, " TgaSize = 0x%x\n", CpuTxtPreMemConfig->TgaSize));
  DEBUG ((DEBUG_INFO, " IsTPMPresence = 0x%x\n", CpuTxtPreMemConfig->IsTPMPresence));
  DEBUG ((DEBUG_INFO, "------------------ CPU_TXT_PREMEM_CONFIG End ------------------\n"));
}

/**
  Print CPU_CONFIG_LIB_PREMEM_CONFIG and serial out.

  @param[in] CpuConfigLibPreMemConfig     Pointer to a CPU_CONFIG_LIB_PREMEM_CONFIG

**/
VOID
CpuConfigLibPreMemConfigPrint (
  IN CONST CPU_CONFIG_LIB_PREMEM_CONFIG        *CpuConfigLibPreMemConfig
  )
{
  DEBUG ((DEBUG_INFO, "------------------ CPU_CONFIG_LIB_PREMEM_CONFIG Begin ------------------\n"));
  DEBUG ((DEBUG_INFO, " HyperThreading = 0x%x\n", CpuConfigLibPreMemConfig->HyperThreading));
  DEBUG ((DEBUG_INFO, " BootFrequency = 0x%x\n", CpuConfigLibPreMemConfig->BootFrequency));
  DEBUG ((DEBUG_INFO, " ActiveCoreCount1 = 0x%x\n", CpuConfigLibPreMemConfig->ActiveCoreCount1));
  DEBUG ((DEBUG_INFO, " JtagC10PowerGateDisable = 0x%x\n", CpuConfigLibPreMemConfig->JtagC10PowerGateDisable));
  DEBUG ((DEBUG_INFO, " BistOnReset = 0x%x\n", CpuConfigLibPreMemConfig->BistOnReset));
  DEBUG ((DEBUG_INFO, " VmxEnable = 0x%x\n", CpuConfigLibPreMemConfig->VmxEnable));
  DEBUG ((DEBUG_INFO, " CrashLogEnable = 0x%x\n", CpuConfigLibPreMemConfig->CrashLogEnable));
  DEBUG ((DEBUG_INFO, " TmeEnable = 0x%x\n", CpuConfigLibPreMemConfig->TmeEnable));
  DEBUG ((DEBUG_INFO, " DebugInterfaceEnable = 0x%X\n", CpuConfigLibPreMemConfig->DebugInterfaceEnable));
  DEBUG ((DEBUG_INFO, " DebugInterfaceLockEnable = 0x%X\n", CpuConfigLibPreMemConfig->DebugInterfaceLockEnable));
  DEBUG ((DEBUG_INFO, " ActiveCoreCount1 = 0x%x\n", CpuConfigLibPreMemConfig->ActiveCoreCount1));
  DEBUG ((DEBUG_INFO, " PeciSxReset = 0x%x\n", CpuConfigLibPreMemConfig->PeciSxReset));
  DEBUG ((DEBUG_INFO, " PeciC10Reset = 0x%x\n", CpuConfigLibPreMemConfig->PeciC10Reset));
  DEBUG ((DEBUG_INFO, " ActiveSmallCoreCount = 0x%x\n", CpuConfigLibPreMemConfig->ActiveSmallCoreCount));
  DEBUG ((DEBUG_INFO, " CrashLogGprs = 0x%x\n", CpuConfigLibPreMemConfig->CrashLogGprs));
  DEBUG ((DEBUG_INFO, " ComputeDieSscEnable = 0x%x\n", CpuConfigLibPreMemConfig->ComputeDieSscEnable));
  DEBUG ((DEBUG_INFO, " SocDieSscEnable = 0x%x\n", CpuConfigLibPreMemConfig->SocDieSscEnable));
  DEBUG ((DEBUG_INFO, " CpuRatio = 0x%x\n", CpuConfigLibPreMemConfig->CpuRatio));
  DEBUG ((DEBUG_INFO, " ActiveSocNorthAtomCoreCount = 0x%x\n", CpuConfigLibPreMemConfig->ActiveSocNorthAtomCoreCount));
  DEBUG ((DEBUG_INFO, " ComputeDieSscValue = 0x%x\n", CpuConfigLibPreMemConfig->ComputeDieSscValue));
  DEBUG ((DEBUG_INFO, " SocDieSscValue = 0x%x\n", CpuConfigLibPreMemConfig->SocDieSscValue));
  DEBUG ((DEBUG_INFO, " BclkSource = 0x%x\n", CpuConfigLibPreMemConfig->BclkSource));
  DEBUG ((DEBUG_INFO, " InfDfxPwrEnable = 0x%x\n", CpuConfigLibPreMemConfig->InfDfxPwrEnable));
  DEBUG ((DEBUG_INFO, " ConfigTdpLevel = 0x%x\n", CpuConfigLibPreMemConfig->ConfigTdpLevel));
  DEBUG ((DEBUG_INFO, " CustomPowerLimit1 = 0x%x\n", CpuConfigLibPreMemConfig->CustomPowerLimit1));
  DEBUG ((DEBUG_INFO, "------------------ CPU_CONFIG_LIB_PREMEM_CONFIG End ------------------\n"));
}



/**
  Print whole CPU Config blocks of SI_PREMEM_POLICY_PPI and serial out in PreMem.

  @param[in] SiPreMemPolicyPpi The SI Pre-Mem Policy PPI instance
**/
VOID
CpuPreMemPrintPolicy (
  IN  SI_PREMEM_POLICY_PPI       *SiPreMemPolicyPpi
  )
{
DEBUG_CODE_BEGIN();
  EFI_STATUS                    Status;
  CPU_SECURITY_PREMEM_CONFIG    *CpuSecurityPreMemConfig;
  CPU_TXT_PREMEM_CONFIG         *CpuTxtPreMemConfig;
  CPU_CONFIG_LIB_PREMEM_CONFIG  *CpuConfigLibPreMemConfig;
  OVERCLOCKING_PREMEM_CONFIG    *CpuOverClockingPreMemConfig;

  DEBUG ((DEBUG_INFO, "\n------------------------ CPU - SiPreMemPolicyPpi Print Begin in PreMem -----------------\n"));

  DEBUG ((DEBUG_INFO, " Revision= %x\n", SiPreMemPolicyPpi->TableHeader.Header.Revision));

  Status = GetConfigBlock ((VOID *) SiPreMemPolicyPpi, &gCpuSecurityPreMemConfigGuid, (VOID *) &CpuSecurityPreMemConfig);
  ASSERT_EFI_ERROR (Status);
  Status = GetConfigBlock ((VOID *) SiPreMemPolicyPpi, &gCpuTxtPreMemConfigGuid, (VOID *) &CpuTxtPreMemConfig);
  ASSERT_EFI_ERROR (Status);
  Status = GetConfigBlock ((VOID *) SiPreMemPolicyPpi, &gCpuConfigLibPreMemConfigGuid, (VOID *) &CpuConfigLibPreMemConfig);
  ASSERT_EFI_ERROR (Status);
  Status = GetConfigBlock ((VOID *) SiPreMemPolicyPpi, &gOverclockingPreMemConfigGuid, (VOID *) &CpuOverClockingPreMemConfig);
  ASSERT_EFI_ERROR (Status);
  CpuSecurityPreMemConfigPrint(CpuSecurityPreMemConfig);
  CpuTxtPreMemConfigPrint(CpuTxtPreMemConfig);
  CpuConfigLibPreMemConfigPrint(CpuConfigLibPreMemConfig);
  DEBUG ((DEBUG_INFO, "\n------------------------ CPU - SiPreMemPolicyPpi Print End -----------------\n\n"));
DEBUG_CODE_END();
}
