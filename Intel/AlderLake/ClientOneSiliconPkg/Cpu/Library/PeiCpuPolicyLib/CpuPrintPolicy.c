/** @file
  This file is PeiCpuPolicy library.

@copyright
  INTEL CONFIDENTIAL
  Copyright 2014 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains an 'Intel Peripheral Driver' and is uniquely identified as
  "Intel Reference Module" and is licensed for Intel CPUs and chipsets under
  the terms of your license agreement with Intel or your vendor. This file may
  be modified by the user, subject to additional terms of the license agreement.

@par Specification
**/
#include "PeiCpuPolicyLibrary.h"
#include <Library/ConfigBlockLib.h>
#include <Library/PcdLib.h>

/**
  Print CPU_CONFIG and serial out.

  @param[in] CpuConfig   Pointer to a CPU_CONFIG
**/
VOID
CpuConfigPrint (
  IN CONST CPU_CONFIG   *CpuConfig
  )
{
  DEBUG ((DEBUG_INFO, "------------------ CPU_CONFIG Begin ------------------\n"));
  DEBUG ((DEBUG_INFO, " MicrocodePatchRegionSize : 0x%x\n", CpuConfig->MicrocodePatchRegionSize));
  DEBUG ((DEBUG_INFO, " MicrocodePatchAddress : 0x%x\n", CpuConfig->MicrocodePatchAddress));
  DEBUG ((DEBUG_INFO, " AesEnable : 0x%x\n", CpuConfig->AesEnable));
  DEBUG ((DEBUG_INFO, " TxtEnable : 0x%X\n", CpuConfig->TxtEnable));
  DEBUG ((DEBUG_INFO, " SkipMpInit : 0x%X\n", CpuConfig->SkipMpInit));
  DEBUG ((DEBUG_INFO, " PpinSupport : 0x%x\n", CpuConfig->PpinSupport));

#if FixedPcdGet8(PcdEmbeddedEnable) == 0x1
  DEBUG ((DEBUG_INFO, " AcSplitLock : 0x%x\n", CpuConfig->AcSplitLock));
#endif
  DEBUG ((DEBUG_INFO, " AvxDisable : 0x%x\n", CpuConfig->AvxDisable));
  DEBUG ((DEBUG_INFO, " X2ApicSupport : 0x%x\n", CpuConfig->X2ApicSupport));

  DEBUG ((DEBUG_INFO, " SmbiosType4MaxSpeedOverride : %d\n", CpuConfig->SmbiosType4MaxSpeedOverride));
  DEBUG ((DEBUG_INFO, "------------------ CPU_CONFIG End ------------------\n"));
}

/**
  Print BIOS_GUARD_CONFIG and serial out.

  @param[in] BiosGuardConfig   Pointer to a BIOS_GUARD_CONFIG2
**/
VOID
BiosGuardConfigPrint (
  IN CONST BIOS_GUARD_CONFIG   *BiosGuardConfig
  )
{
  DEBUG ((DEBUG_INFO, "------------------ BIOS_GUARD_CONFIG Begin ------------------\n"));
  DEBUG ((DEBUG_INFO, " \n"));
  DEBUG ((DEBUG_INFO, " Platform attributes : 0x%X\n", BiosGuardConfig->BiosGuardAttr));
  DEBUG ((DEBUG_INFO, " BiosGuardModulePtr : 0x%016llX\n", BiosGuardConfig->BiosGuardModulePtr));
  DEBUG ((DEBUG_INFO, " SendEcCmd : 0x%X\n", BiosGuardConfig->SendEcCmd));
  DEBUG ((DEBUG_INFO, " EcCmdProvisionEav : 0x%X\n", BiosGuardConfig->EcCmdProvisionEav));
  DEBUG ((DEBUG_INFO, " EcCmdLock : 0x%X\n", BiosGuardConfig->EcCmdLock));
  DEBUG ((DEBUG_INFO, "------------------ BIOS_GUARD_CONFIG End ------------------\n"));
}

/**
  Print CPU_POWER_MGMT_BASIC_CONFIG and serial out.

  @param[in] CpuPowerMgmtBasicConfig   Pointer to a CPU_POWER_MGMT_BASIC_CONFIG
**/
VOID
CpuPowerMgmtBasicConfigPrint (
  IN CONST CPU_POWER_MGMT_BASIC_CONFIG   *CpuPowerMgmtBasicConfig
  )
{
  UINT8 Index;

  DEBUG ((DEBUG_INFO, "------------------ CPU_POWER_MGMT_BASIC_CONFIG Begin ------------------\n"));
  DEBUG ((DEBUG_INFO, " BootFrequency : 0x%x\n", CpuPowerMgmtBasicConfig->BootFrequency));
  DEBUG ((DEBUG_INFO, " SkipSetBootPState : 0x%x\n", CpuPowerMgmtBasicConfig->SkipSetBootPState));
  DEBUG ((DEBUG_INFO, " Hwp : 0x%x\n", CpuPowerMgmtBasicConfig->Hwp));
  DEBUG ((DEBUG_INFO, " HdcControl : 0x%X\n", CpuPowerMgmtBasicConfig->HdcControl));
  DEBUG ((DEBUG_INFO, " PowerLimit2 : 0x%x\n", CpuPowerMgmtBasicConfig->PowerLimit2));
  DEBUG ((DEBUG_INFO, " TurboPowerLimitLock : 0x%x\n", CpuPowerMgmtBasicConfig->TurboPowerLimitLock));
  DEBUG ((DEBUG_INFO, " PowerLimit3DutyCycle : 0x%x\n", CpuPowerMgmtBasicConfig->PowerLimit3DutyCycle));
  DEBUG ((DEBUG_INFO, " PowerLimit3Lock : 0x%x\n", CpuPowerMgmtBasicConfig->PowerLimit3Lock));
  DEBUG ((DEBUG_INFO, " PowerLimit4Lock : 0x%x\n", CpuPowerMgmtBasicConfig->PowerLimit4Lock));
  DEBUG ((DEBUG_INFO, " TccOffsetClamp : 0x%X\n", CpuPowerMgmtBasicConfig->TccOffsetClamp));
  DEBUG ((DEBUG_INFO, " TccOffsetLock : 0x%X\n", CpuPowerMgmtBasicConfig->TccOffsetLock));
  DEBUG ((DEBUG_INFO, " TurboMode : 0x%x\n", CpuPowerMgmtBasicConfig->TurboMode));
  DEBUG ((DEBUG_INFO, " HwpInterruptControl : 0x%x\n", CpuPowerMgmtBasicConfig->HwpInterruptControl));
  DEBUG ((DEBUG_INFO, " ApplyConfigTdp : 0x%x\n", CpuPowerMgmtBasicConfig->ApplyConfigTdp));
  DEBUG ((DEBUG_INFO, " HwpLock : 0x%x\n", CpuPowerMgmtBasicConfig->HwpLock));
  DEBUG ((DEBUG_INFO, " DualTauBoost : 0x%x\n", CpuPowerMgmtBasicConfig->DualTauBoost));
  DEBUG ((DEBUG_INFO, " OneCoreRatioLimit : 0x%X\n", CpuPowerMgmtBasicConfig->OneCoreRatioLimit));
  DEBUG ((DEBUG_INFO, " TwoCoreRatioLimit : 0x%X\n", CpuPowerMgmtBasicConfig->TwoCoreRatioLimit));
  DEBUG ((DEBUG_INFO, " ThreeCoreRatioLimit : 0x%X\n", CpuPowerMgmtBasicConfig->ThreeCoreRatioLimit));
  DEBUG ((DEBUG_INFO, " FourCoreRatioLimit : 0x%X\n", CpuPowerMgmtBasicConfig->FourCoreRatioLimit));
  DEBUG ((DEBUG_INFO, " FiveCoreRatioLimit : 0x%X\n", CpuPowerMgmtBasicConfig->FiveCoreRatioLimit));
  DEBUG ((DEBUG_INFO, " SixCoreRatioLimit : 0x%X\n", CpuPowerMgmtBasicConfig->SixCoreRatioLimit));
  DEBUG ((DEBUG_INFO, " SevenCoreRatioLimit : 0x%X\n", CpuPowerMgmtBasicConfig->SevenCoreRatioLimit));
  DEBUG ((DEBUG_INFO, " EightCoreRatioLimit : 0x%X\n", CpuPowerMgmtBasicConfig->EightCoreRatioLimit));
  DEBUG ((DEBUG_INFO, " TccActivationOffset : 0x%X\n", CpuPowerMgmtBasicConfig->TccActivationOffset));
  DEBUG ((DEBUG_INFO, " (Intel Turbo Boost Max Technology 3.0)EnableItbm : 0x%X\n", CpuPowerMgmtBasicConfig->EnableItbm));
  DEBUG ((DEBUG_INFO, " EnableItbmDriver : 0x%X\n", CpuPowerMgmtBasicConfig->EnableItbmDriver));
  DEBUG ((DEBUG_INFO, " EnablePerCorePState : 0x%x\n", CpuPowerMgmtBasicConfig->EnablePerCorePState));
  DEBUG ((DEBUG_INFO, " EnableHwpAutoPerCorePstate : 0x%x\n", CpuPowerMgmtBasicConfig->EnableHwpAutoPerCorePstate));
  DEBUG ((DEBUG_INFO, " EnableHwpAutoEppGrouping : 0x%x\n", CpuPowerMgmtBasicConfig->EnableHwpAutoEppGrouping));
  DEBUG ((DEBUG_INFO, " EnableEpbPeciOverride : 0x%x\n", CpuPowerMgmtBasicConfig->EnableEpbPeciOverride));
  DEBUG ((DEBUG_INFO, " EnableFastMsrHwpReq : 0x%x\n", CpuPowerMgmtBasicConfig->EnableFastMsrHwpReq));
  DEBUG ((DEBUG_INFO, " MinRingRatioLimit : 0x%x\n", CpuPowerMgmtBasicConfig->MinRingRatioLimit));
  DEBUG ((DEBUG_INFO, " MaxRingRatioLimit : 0x%x\n", CpuPowerMgmtBasicConfig->MaxRingRatioLimit));
  DEBUG ((DEBUG_INFO, " PowerLimit1 : 0x%x\n", CpuPowerMgmtBasicConfig->PowerLimit1));
  DEBUG ((DEBUG_INFO, " PowerLimit2Power : 0x%x\n", CpuPowerMgmtBasicConfig->PowerLimit2Power));
  DEBUG ((DEBUG_INFO, " PowerLimit3 : 0x%x\n", CpuPowerMgmtBasicConfig->PowerLimit3));
  DEBUG ((DEBUG_INFO, " PowerLimit4 : 0x%x\n", CpuPowerMgmtBasicConfig->PowerLimit4));
  DEBUG ((DEBUG_INFO, " PowerLimit1Time : 0x%x\n", CpuPowerMgmtBasicConfig->PowerLimit1Time));
  DEBUG ((DEBUG_INFO, " PowerLimit3Time : 0x%x\n", CpuPowerMgmtBasicConfig->PowerLimit3Time));
  DEBUG ((DEBUG_INFO, " TccOffsetTimeWindowForRatl : 0x%X\n", CpuPowerMgmtBasicConfig->TccOffsetTimeWindowForRatl));
  for (Index = 0; Index < TURBO_RATIO_LIMIT_ARRAY_SIZE; Index++) {
    DEBUG ((DEBUG_INFO, " TurboRatioLimitRatio[0x%x] : 0x%X\n", Index, CpuPowerMgmtBasicConfig->TurboRatioLimitRatio[Index]));
    DEBUG ((DEBUG_INFO, " TurboRatioLimitNumCore[0x%x] : 0x%X\n", Index, CpuPowerMgmtBasicConfig->TurboRatioLimitNumCore[Index]));
    DEBUG ((DEBUG_INFO, " AtomTurboRatioLimitRatio[0x%x] : 0x%X\n", Index, CpuPowerMgmtBasicConfig->AtomTurboRatioLimitRatio[Index]));
    DEBUG ((DEBUG_INFO, " AtomTurboRatioLimitNumCore[0x%x] : 0x%X\n", Index, CpuPowerMgmtBasicConfig->AtomTurboRatioLimitNumCore[Index]));
  }
  DEBUG ((DEBUG_INFO, "------------------ CPU_POWER_MGMT_BASIC_CONFIG End ------------------\n"));
}

/**
  Print CPU_POWER_MGMT_CUSTOM_CONFIG and serial out.

  @param[in] CpuPowerMgmtCustomConfig   Pointer to a CPU_POWER_MGMT_CUSTOM_CONFIG
**/
VOID
CpuPowerMgmtCustomConfigPrint (
  IN CONST CPU_POWER_MGMT_CUSTOM_CONFIG   *CpuPowerMgmtCustomConfig
  )
{
  UINT32 Index = 0;
  DEBUG ((DEBUG_INFO, "------------------ CPU_POWER_MGMT_CUSTOM_CONFIG Begin ------------------\n"));
  DEBUG ((DEBUG_INFO, "\n CustomRatioTable... \n"));
  DEBUG ((DEBUG_INFO, " VidMaxRatio : 0x%x\n", CpuPowerMgmtCustomConfig->CustomRatioTable.MaxRatio));
  DEBUG ((DEBUG_INFO, " VidNumber : 0x%x\n", CpuPowerMgmtCustomConfig->CustomRatioTable.NumberOfEntries));
  DEBUG ((DEBUG_INFO, " VidCpuid : 0x%x\n", CpuPowerMgmtCustomConfig->CustomRatioTable.Cpuid));
  for (Index = 0; Index < MAX_CUSTOM_RATIO_TABLE_ENTRIES; Index++) {
    DEBUG ((DEBUG_INFO, " StateRatio[%d] : 0x%x\n", Index, CpuPowerMgmtCustomConfig->CustomRatioTable.StateRatio[Index]));
  }
  for (Index = 0; Index < MAX_16_CUSTOM_RATIO_TABLE_ENTRIES; Index++) {
    DEBUG ((DEBUG_INFO, " StateRatioMax16[%d] : 0x%x\n", Index, CpuPowerMgmtCustomConfig->CustomRatioTable.StateRatioMax16[Index]));
  }
  for (Index = 0; Index < MAX_CUSTOM_CTDP_ENTRIES; Index++) {
    DEBUG (
            (DEBUG_INFO,
             " CustomConfigTdpTable[%d] CustomPowerLimit1Time : 0x%x\n",
             Index,CpuPowerMgmtCustomConfig->CustomConfigTdpTable[Index].CustomPowerLimit1Time)
            );
    DEBUG (
            (DEBUG_INFO,
             " CustomConfigTdpTable[%d] CustomTurboActivationRatio : 0x%x\n",
             Index,CpuPowerMgmtCustomConfig->CustomConfigTdpTable[Index].CustomTurboActivationRatio)
            );
    DEBUG (
            (DEBUG_INFO,
             " CustomConfigTdpTable[%d] CustomPowerLimit1 : 0x%x\n",
             Index,CpuPowerMgmtCustomConfig->CustomConfigTdpTable[Index].CustomPowerLimit1)
            );
    DEBUG (
            (DEBUG_INFO,
             " CustomConfigTdpTable[%d] CustomPowerLimit2 : 0x%x\n",
             Index,CpuPowerMgmtCustomConfig->CustomConfigTdpTable[Index].CustomPowerLimit2)
            );
  }
  DEBUG ((DEBUG_INFO, " ConfigTdpLock : 0x%x\n", CpuPowerMgmtCustomConfig->ConfigTdpLock));
  DEBUG ((DEBUG_INFO, " ConfigTdpBios : 0x%x\n", CpuPowerMgmtCustomConfig->ConfigTdpBios));
  DEBUG ((DEBUG_INFO, "------------------ CPU_POWER_MGMT_CUSTOM_CONFIG End ------------------\n"));
}

/**
  Print CPU_POWER_MGMT_PSYS_CONFIG and serial out.

  @param[in] CpuPowerMgmtPsysConfig   Pointer to a CPU_POWER_MGMT_PSYS_CONFIG
**/
VOID
CpuPowerMgmtPsysConfigPrint (
  IN CONST CPU_POWER_MGMT_PSYS_CONFIG   *CpuPowerMgmtPsysConfig
  )
{
  DEBUG ((DEBUG_INFO, "------------------ CPU_POWER_MGMT_PSYS_CONFIG Begin ------------------\n"));
  DEBUG ((DEBUG_INFO, " PsysPowerLimit1 : 0x%x\n", CpuPowerMgmtPsysConfig->PsysPowerLimit1));
  DEBUG ((DEBUG_INFO, " PsysPowerLimit1Time : 0x%x\n", CpuPowerMgmtPsysConfig->PsysPowerLimit1Time));
  DEBUG ((DEBUG_INFO, " PsysPowerLimit2 : 0x%x\n", CpuPowerMgmtPsysConfig->PsysPowerLimit2));
  DEBUG ((DEBUG_INFO, " PsysPowerLimit1Power = 0x%x\n", CpuPowerMgmtPsysConfig->PsysPowerLimit1Power));
  DEBUG ((DEBUG_INFO, " PsysPowerLimit2Power = 0x%x\n", CpuPowerMgmtPsysConfig->PsysPowerLimit2Power));
  DEBUG ((DEBUG_INFO, " PsysPmax : 0x%X\n", CpuPowerMgmtPsysConfig->PsysPmax));
  DEBUG ((DEBUG_INFO, "------------------ CPU_POWER_MGMT_PSYS_CONFIG End ------------------\n"));
}

/**
  Print CPU_TEST_CONFIG and serial out.

  @param[in] CpuTestConfig   Pointer to a CPU_TEST_CONFIG
**/
VOID
CpuTestConfigPrint (
  IN CONST CPU_TEST_CONFIG   *CpuTestConfig
  )
{
  UINT8 PcdCpuApLoopMode;

  PcdCpuApLoopMode = PcdGet8 (PcdCpuApLoopMode);

  DEBUG ((DEBUG_INFO, "------------------ CPU_TEST_CONFIG Begin ------------------\n"));
  DEBUG ((DEBUG_INFO, " MlcStreamerPrefetcher : 0x%X\n", CpuTestConfig->MlcStreamerPrefetcher));
  DEBUG ((DEBUG_INFO, " MlcSpatialPrefetcher : 0x%X\n", CpuTestConfig->MlcSpatialPrefetcher));
  DEBUG ((DEBUG_INFO, " MonitorMwaitEnable : 0x%X\n", CpuTestConfig->MonitorMwaitEnable));
  DEBUG ((DEBUG_INFO, " MachineCheckEnable : 0x%X\n", CpuTestConfig->MachineCheckEnable));
  DEBUG ((DEBUG_INFO, " ProcessorTraceOutputScheme : 0x%X\n", CpuTestConfig->ProcessorTraceOutputScheme));
  DEBUG ((DEBUG_INFO, " ProcessorTraceEnable : 0x%X\n", CpuTestConfig->ProcessorTraceEnable));
  DEBUG ((DEBUG_INFO, " ThreeStrikeCounterDisable : 0x%X\n", CpuTestConfig->ThreeStrikeCounterDisable));
  DEBUG ((DEBUG_INFO, " ProcessorTraceMemSize : 0x%X\n", CpuTestConfig->ProcessorTraceMemSize));
  DEBUG ((DEBUG_INFO, " PcdCpuApLoopMode: 0x%X\n", PcdCpuApLoopMode));
  DEBUG ((DEBUG_INFO, "------------------ CPU_TEST_CONFIG End ------------------\n"));
}

/**
  Print CPU_POWER_MGMT_TEST_CONFIG and serial out.

  @param[in] CpuPowerMgmtTestConfig   Pointer to a CPU_POWER_MGMT_TEST_CONFIG
**/
VOID
CpuPowerMgmtTestConfigPrint (
  IN CONST CPU_POWER_MGMT_TEST_CONFIG   *CpuPowerMgmtTestConfig
  )
{
  DEBUG ((DEBUG_INFO, "------------------ CPU Power Mgmt Test Config ------------------\n"));
  DEBUG ((DEBUG_INFO, " Eist : 0x%x\n", CpuPowerMgmtTestConfig->Eist));
  DEBUG ((DEBUG_INFO, " EnergyEfficientPState : 0x%x\n", CpuPowerMgmtTestConfig->EnergyEfficientPState));
  DEBUG ((DEBUG_INFO, " EnergyEfficientTurbo : 0x%x\n", CpuPowerMgmtTestConfig->EnergyEfficientTurbo));
  DEBUG ((DEBUG_INFO, " TStates : 0x%x\n", CpuPowerMgmtTestConfig->TStates));
  DEBUG ((DEBUG_INFO, " BiProcHot : 0x%x\n", CpuPowerMgmtTestConfig->BiProcHot));
  DEBUG ((DEBUG_INFO, " DisableProcHotOut : 0x%x\n", CpuPowerMgmtTestConfig->DisableProcHotOut));
  DEBUG ((DEBUG_INFO, " ProcHotResponse : 0x%x\n", CpuPowerMgmtTestConfig->ProcHotResponse));
  DEBUG ((DEBUG_INFO, " DisableVrThermalAlert : 0x%x\n", CpuPowerMgmtTestConfig->DisableVrThermalAlert));
  DEBUG ((DEBUG_INFO, " EnableAllThermalFunctions : 0x%x\n", CpuPowerMgmtTestConfig->EnableAllThermalFunctions));
  DEBUG ((DEBUG_INFO, " ThermalMonitor : 0x%x\n", CpuPowerMgmtTestConfig->ThermalMonitor));
  DEBUG ((DEBUG_INFO, " Cx : 0x%x\n", CpuPowerMgmtTestConfig->Cx));
  DEBUG ((DEBUG_INFO, " PmgCstCfgCtrlLock : 0x%x\n", CpuPowerMgmtTestConfig->PmgCstCfgCtrlLock));
  DEBUG ((DEBUG_INFO, " C1e : 0x%x\n", CpuPowerMgmtTestConfig->C1e));
  DEBUG ((DEBUG_INFO, " C1Autodemotion : 0x%x\n", CpuPowerMgmtTestConfig->C1AutoDemotion));
  DEBUG ((DEBUG_INFO, " C1Undemotion : 0x%x\n", CpuPowerMgmtTestConfig->C1UnDemotion));
  DEBUG ((DEBUG_INFO, " PkgCState Demotion : 0x%x\n", CpuPowerMgmtTestConfig->PkgCStateDemotion));
  DEBUG ((DEBUG_INFO, " PkgCstateUndemotion : 0x%x\n", CpuPowerMgmtTestConfig->PkgCStateUnDemotion));
  DEBUG ((DEBUG_INFO, " CStatePreWake : 0x%x\n", CpuPowerMgmtTestConfig->CStatePreWake));
  DEBUG ((DEBUG_INFO, " TimedMwait : 0x%x\n", CpuPowerMgmtTestConfig->TimedMwait));
  DEBUG ((DEBUG_INFO, " CstCfgCtrIoMwaitRedirection : 0x%x\n", CpuPowerMgmtTestConfig->CstCfgCtrIoMwaitRedirection));
  DEBUG ((DEBUG_INFO, " ProcHotLock : 0x%x\n", CpuPowerMgmtTestConfig->ProcHotLock));
  DEBUG ((DEBUG_INFO, " RaceToHalt  : 0x%x\n",  CpuPowerMgmtTestConfig->RaceToHalt));
  DEBUG ((DEBUG_INFO, " CstateLatencyControl0Irtl : 0x%x\n", CpuPowerMgmtTestConfig->CstateLatencyControl0Irtl));
  DEBUG ((DEBUG_INFO, " CstateLatencyControl1Irtl : 0x%x\n", CpuPowerMgmtTestConfig->CstateLatencyControl1Irtl));
  DEBUG ((DEBUG_INFO, " CstateLatencyControl2Irtl : 0x%x\n", CpuPowerMgmtTestConfig->CstateLatencyControl2Irtl));
  DEBUG ((DEBUG_INFO, " CstateLatencyControl3Irtl : 0x%x\n", CpuPowerMgmtTestConfig->CstateLatencyControl3Irtl));
  DEBUG ((DEBUG_INFO, " CstateLatencyControl4Irtl : 0x%x\n", CpuPowerMgmtTestConfig->CstateLatencyControl4Irtl));
  DEBUG ((DEBUG_INFO, " CstateLatencyControl5Irtl : 0x%x\n", CpuPowerMgmtTestConfig->CstateLatencyControl5Irtl));
  DEBUG ((DEBUG_INFO, " PkgCStateLimit : 0x%x\n", CpuPowerMgmtTestConfig->PkgCStateLimit));
  DEBUG ((DEBUG_INFO, " CstateLatencyControl0TimeUnit : 0x%x\n", CpuPowerMgmtTestConfig->CstateLatencyControl0TimeUnit));
  DEBUG ((DEBUG_INFO, " CstateLatencyControl1TimeUnit : 0x%x\n", CpuPowerMgmtTestConfig->CstateLatencyControl1TimeUnit));
  DEBUG ((DEBUG_INFO, " CstateLatencyControl2TimeUnit : 0x%x\n", CpuPowerMgmtTestConfig->CstateLatencyControl2TimeUnit));
  DEBUG ((DEBUG_INFO, " CstateLatencyControl3TimeUnit : 0x%x\n", CpuPowerMgmtTestConfig->CstateLatencyControl3TimeUnit));
  DEBUG ((DEBUG_INFO, " CstateLatencyControl4TimeUnit : 0x%x\n", CpuPowerMgmtTestConfig->CstateLatencyControl4TimeUnit));
  DEBUG ((DEBUG_INFO, " CstateLatencyControl5TimeUnit : 0x%x\n", CpuPowerMgmtTestConfig->CstateLatencyControl5TimeUnit));
  DEBUG ((DEBUG_INFO, " CustomPowerUnit : 0x%x\n", CpuPowerMgmtTestConfig->CustomPowerUnit));
  DEBUG ((DEBUG_INFO, " PpmIrmSetting : 0x%x\n", CpuPowerMgmtTestConfig->PpmIrmSetting));
 DEBUG ((DEBUG_INFO, "------------------ CPU_POWER_MGMT_TEST_CONFIG End ------------------\n"));
}
/**
  Print whole CPU config blocks of SI_POLICY_PPI and serial out in PostMem.

  @param[in] SiPolicyPpi The SI Policy PPI instance
**/
VOID
CpuPrintPolicy (
  IN  SI_POLICY_PPI       *SiPolicyPpi
  )
{
DEBUG_CODE_BEGIN();
  EFI_STATUS                       Status;
  CPU_CONFIG                       *CpuConfig;
  BIOS_GUARD_CONFIG                *BiosGuardConfig;

  CPU_POWER_MGMT_BASIC_CONFIG      *CpuPowerMgmtBasicConfig;
  CPU_POWER_MGMT_CUSTOM_CONFIG     *CpuPowerMgmtCustomConfig;
  CPU_POWER_MGMT_PSYS_CONFIG       *CpuPowerMgmtPsysConfig;
  CPU_TEST_CONFIG                  *CpuTestConfig;
  CPU_POWER_MGMT_TEST_CONFIG       *CpuPowerMgmtTestConfig;

  Status = GetConfigBlock ((VOID *) SiPolicyPpi, &gCpuConfigGuid, (VOID *) &CpuConfig);
  ASSERT_EFI_ERROR (Status);

  Status = GetConfigBlock ((VOID *) SiPolicyPpi, &gBiosGuardConfigGuid, (VOID *) &BiosGuardConfig);
  ASSERT_EFI_ERROR (Status);

  Status = GetConfigBlock ((VOID *) SiPolicyPpi, &gCpuPowerMgmtBasicConfigGuid, (VOID *) &CpuPowerMgmtBasicConfig);
  ASSERT_EFI_ERROR (Status);

  Status = GetConfigBlock ((VOID *) SiPolicyPpi, &gCpuPowerMgmtCustomConfigGuid, (VOID *) &CpuPowerMgmtCustomConfig);
  ASSERT_EFI_ERROR (Status);

  Status = GetConfigBlock ((VOID *) SiPolicyPpi, &gCpuPowerMgmtPsysConfigGuid, (VOID *) &CpuPowerMgmtPsysConfig);
  ASSERT_EFI_ERROR (Status);

  Status = GetConfigBlock ((VOID *) SiPolicyPpi, &gCpuTestConfigGuid, (VOID *) &CpuTestConfig);
  ASSERT_EFI_ERROR (Status);

  Status = GetConfigBlock ((VOID *) SiPolicyPpi, &gCpuPowerMgmtTestConfigGuid, (VOID *) &CpuPowerMgmtTestConfig);
  ASSERT_EFI_ERROR (Status);
  DEBUG ((DEBUG_INFO, "\n ------------------------ SiCpuPolicy Print Begin in PostMem----------------- \n"));
  DEBUG ((DEBUG_INFO, " Revision= %x\n", SiPolicyPpi->TableHeader.Header.Revision));

  CpuConfigPrint(CpuConfig);
  BiosGuardConfigPrint(BiosGuardConfig);
  CpuPowerMgmtBasicConfigPrint(CpuPowerMgmtBasicConfig);
  CpuPowerMgmtCustomConfigPrint(CpuPowerMgmtCustomConfig);
  CpuPowerMgmtPsysConfigPrint(CpuPowerMgmtPsysConfig);
  CpuTestConfigPrint(CpuTestConfig);
  CpuPowerMgmtTestConfigPrint(CpuPowerMgmtTestConfig);
  DEBUG ((DEBUG_INFO, "\n ------------------------ SiCpuPolicy Print End in PostMem ----------------- \n\n"));
DEBUG_CODE_END();
}

