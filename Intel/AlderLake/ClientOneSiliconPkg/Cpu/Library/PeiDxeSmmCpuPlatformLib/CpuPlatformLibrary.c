/** @file
  CPU Platform Lib implementation.

@copyright
  INTEL CONFIDENTIAL
  Copyright 2012 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains an 'Intel Peripheral Driver' and is uniquely identified as
  "Intel Reference Module" and is licensed for Intel CPUs and chipsets under
  the terms of your license agreement with Intel or your vendor. This file may
  be modified by the user, subject to additional terms of the license agreement.

@par Specification Reference:
**/
#include "CpuPlatformLibrary.h"
#include <Library/PciSegmentLib.h>
#include <Register/SaRegsHostBridge.h>
#include <Register/IgdRegs.h>
#include <Register/HeciRegs.h>
#include <Register/MeRegs.h>
#include <Library/CpuMailboxLib.h>
#include <Library/CpuInfoFruLib.h>
#include <Register/CommonMsr.h>
#include <Register/ArchMsr.h>
#include <Library/MsrFruLib.h>
#include <CpuGenInfoFruLib.h>
#include <Pi/PiStatusCode.h>
#include <Library/ReportStatusCodeLib.h>

#define SKIP_MICROCODE_CHECKSUM_CHECK 1

/**
  Return CPU Family ID

  @retval CPU_FAMILY              CPU Family ID
**/
CPU_FAMILY
EFIAPI
GetCpuFamily (
  VOID
  )
{
  CPUID_VERSION_INFO_EAX  Eax;
  ///
  /// Read the CPUID information
  ///
  AsmCpuid (CPUID_VERSION_INFO, &Eax.Uint32, NULL, NULL, NULL);
  return (Eax.Uint32 & CPUID_FULL_FAMILY_MODEL);
}

/**
  Return Cpu stepping type

  @retval UINT8                   Cpu stepping type
**/
CPU_STEPPING
EFIAPI
GetCpuStepping (
  VOID
  )
{
  CPUID_VERSION_INFO_EAX  Eax;
  ///
  /// Read the CPUID information
  ///
  AsmCpuid (CPUID_VERSION_INFO, &Eax.Uint32, NULL, NULL, NULL);
  return ((CPU_STEPPING) (Eax.Uint32 & CPUID_FULL_STEPPING));
}

/**
  Return CPU Sku

  @retval UINT8              CPU Sku
**/
UINT8
EFIAPI
GetCpuSku (
  VOID
  )
{
  UINT16                  CpuDid;
  UINT32                  CpuFamilyModel;
  CPUID_VERSION_INFO_EAX  Eax;

  ///
  /// Read the CPUID & DID information
  ///
  AsmCpuid (CPUID_VERSION_INFO, &Eax.Uint32, NULL, NULL, NULL);
  CpuFamilyModel = Eax.Uint32 & CPUID_FULL_FAMILY_MODEL;
  CpuDid = PciSegmentRead16 (PCI_SEGMENT_LIB_ADDRESS (SA_SEG_NUM, SA_MC_BUS, SA_MC_DEV, SA_MC_FUN, R_SA_MC_DEVICE_ID));

  return GetCpuSkuInfo (CpuFamilyModel, CpuDid);

}

/**
  Check on the processor if SMX (Safer Mode Extensions) is supported.

  @retval  TRUE        CPU supports SMX.
  @retval  FALSE       CPU do not support SMX.
**/
BOOLEAN
IsSmxSupported (
  VOID
  )
{
  CPUID_VERSION_INFO_ECX   Ecx;

  ///
  /// Read CPUID Version Information returned in ECX for CPUID leaf.
  ///
  AsmCpuid (CPUID_VERSION_INFO, NULL, NULL, &Ecx.Uint32, NULL);
  if (Ecx.Bits.SMX == 1) {
    return TRUE;
  }

  return FALSE;
}

/**
  Return CPU Identifier used to identify various CPU types

  @retval CPU_IDENTIFIER           CPU Identifier
**/
UINT32
GetCpuIdentifier (
  )
{
  CPU_SKU                               CpuSku;
  CPU_STEPPING                          CpuStepping;

  ///
  /// Initialize local variables
  ///
  CpuSku    = GetCpuSku ();
  CpuStepping = GetCpuStepping ();
  return (UINT32) GetCpuSkuIdentifier (CpuSku, CpuStepping, 0);
}

/**
  Return CPU Identifier used to identify various CPU types
  @param[in] SelectedCtdpLevel    - Ctdp Level Selected in BIOS

  @retval CPU_IDENTIFIER           CPU Identifier
**/
UINT32
GetCpuIdentifierWithCtdp (
  IN UINT16               SelectedCtdpLevel
  )
{
  CPU_SKU                               CpuSku;
  CPU_STEPPING                          CpuStepping;
  ///
  /// Initialize local variables
  ///
  CpuSku    = GetCpuSku ();
  CpuStepping = GetCpuStepping ();
  return (UINT32) GetCpuSkuIdentifier (CpuSku, CpuStepping, SelectedCtdpLevel);
}

/**
  Returns the processor microcode revision of the processor installed in the system.

  @retval Processor Microcode Revision
**/
UINT32
GetCpuUcodeRevision (
  VOID
  )
{
  MSR_IA32_BIOS_SIGN_ID_REGISTER  BiosSignMsr;

  AsmWriteMsr64 (MSR_IA32_BIOS_SIGN_ID, 0);
  AsmCpuid (CPUID_VERSION_INFO, NULL, NULL, NULL, NULL);
  BiosSignMsr.Uint64 = AsmReadMsr64 (MSR_IA32_BIOS_SIGN_ID);

  return BiosSignMsr.Bits.MicrocodeUpdateSignature;
}

/**
  This function is to program Trace Hub ACPI base address to processor's MSR TRACE_HUB_STH_ACPIBAR_BASE.

  @param[in]  TraceHubAcpiBase - Base address of Trace Hub ACPI Base address
**/
VOID
EFIAPI
CpuWriteTraceHubAcpiBase (
  IN UINT64  TraceHubAcpiBase
  )
{
  MSR_NPK_STH_ACPIBAR_BASE_REGISTER BaseAddressMask;
  MSR_NPK_STH_ACPIBAR_BASE_REGISTER MsrTraceHubAcpiBase;

  //
  // Make up Trace Hub ACPI Base address mask.
  //
  BaseAddressMask.Uint64 = MAX_UINT64;
  BaseAddressMask.Bits.Lock = 0;
  BaseAddressMask.Bits.Rsvd1 = 0;

  //
  // Check the pass in Trace Hub ACPI base if 256KB alignment.
  //
  if ((TraceHubAcpiBase & ~BaseAddressMask.Uint64) != 0) {
    ASSERT ((TraceHubAcpiBase & ~BaseAddressMask.Uint64) == 0);
    return;
  }

  ///
  /// Set MSR TRACE_HUB_STH_ACPIBAR_BASE[0] LOCK bit for the AET packets to be directed to NPK MMIO.
  ///
  MsrTraceHubAcpiBase.Uint64 = TraceHubAcpiBase;
  MsrTraceHubAcpiBase.Bits.Lock = 1;
  AsmWriteMsr64 (MSR_NPK_STH_ACPIBAR_BASE, MsrTraceHubAcpiBase.Uint64);

  return;
}

/**
  Detect if C6DRAM supported or not by reading it from PCODE mailbox.

  @retval TRUE - Supported
  @retval FALSE - Not Supported
**/
BOOLEAN
IsC6dramSupported (
  VOID
  )
{
  UINT32       MailboxStatus;
  UINT32       MailboxData;
  EFI_STATUS   Status;
  PCODE_MAILBOX_INTERFACE MailboxCommand;

  ///
  /// For C6DRAM, PCODE mailbox returns fuse_c6dram_en in bit 1.
  ///
  MailboxCommand.InterfaceData = 0;
  MailboxCommand.Fields.Command = MAILBOX_PCODE_CMD_READ_C6DRAM_CONFIG;
  Status = MailboxRead (MAILBOX_TYPE_PCODE, MailboxCommand.InterfaceData, &MailboxData , &MailboxStatus);
  if (Status != EFI_SUCCESS || MailboxStatus != EFI_SUCCESS) {
    DEBUG ((DEBUG_ERROR, "Mailbox read command failed unexpectedly, C6DRAM is not supported. MailboxStatus = %x , Mailbox command return status %r\n", MailboxStatus, Status));
    return FALSE;
  }

  return (MailboxData & B_MAILBOX_C6DRAM_SUPPORTED) == B_MAILBOX_C6DRAM_SUPPORTED;
}

/**
  Get processor generation

  @retval CPU_GENERATION  Returns the executing thread's processor generation.
**/
CPU_GENERATION
GetCpuGeneration (
  VOID
  )
{
  CPU_FAMILY                 CpuFamilyModel;
  CPUID_VERSION_INFO_EAX     Eax;

  ///
  /// Read the CPUID information
  ///
  AsmCpuid (CPUID_VERSION_INFO, &Eax.Uint32, NULL, NULL, NULL);
  CpuFamilyModel = (CPU_FAMILY) (Eax.Uint32 & CPUID_FULL_FAMILY_MODEL);


  return GetCpuSkuGeneration (CpuFamilyModel);
}

/**
  Check if this is non-core processor - HT AP thread

  @retval TRUE if this is HT AP thread
  @retval FALSE if this is core thread
**/
BOOLEAN
IsSecondaryThread (
  VOID
  )
{
  CPUID_VERSION_INFO_EDX      CpuidVersionInfoEdx;
  CPUID_EXTENDED_TOPOLOGY_EAX CpuIdExtendedTopologyEax;
  UINT32                      ApicId;
  UINT32                      MaskShift;
  UINT32                      Mask;

  AsmCpuid (CPUID_VERSION_INFO, NULL, NULL, NULL, &CpuidVersionInfoEdx.Uint32);
  if (CpuidVersionInfoEdx.Bits.HTT == 0) {
    return FALSE;
  }

  AsmCpuidEx (
    CPUID_EXTENDED_TOPOLOGY,
    0,  // Sub-leaf 0
    &CpuIdExtendedTopologyEax.Uint32,
    NULL,
    NULL,
    &ApicId
    );

  MaskShift = CpuIdExtendedTopologyEax.Bits.ApicIdShift;
  Mask = ~(0xffffffff << MaskShift);

  return (ApicId & Mask) > 0;
}

/**
  This function returns Number of enabled cores in the package.

  @retval Number of enabled cores in the package.
**/

UINT16
GetEnabledCoreCount (
  VOID
  )
{
  MSR_CORE_THREAD_COUNT_REGISTER   MsrValue;

  MsrValue.Uint64 = AsmReadMsr64 (MSR_CORE_THREAD_COUNT);

  return (UINT16) MsrValue.Bits.Corecount;
}

/**
  This function returns Number of enabled Threads in the package.

  @retval Number of enabled threads in the package.
**/
UINT16
GetEnabledThreadCount (
  VOID
  )
{
  MSR_CORE_THREAD_COUNT_REGISTER   MsrValue;

  MsrValue.Uint64 = AsmReadMsr64 (MSR_CORE_THREAD_COUNT);

  return (UINT16) MsrValue.Bits.Threadcount;
}

/**
  This function checks whether uCode loaded from FIT.

  @retval TRUE  - uCode loaded from FIT successful
  @retval FALSE - Failed on FIT to load uCode
**/
BOOLEAN
IsValiduCodeEntry (
  VOID
  )
{
  UINT8                       FitEntryType;
  UINT8                       FitErrorCode;
  MSR_BIOS_SIGN_ID_REGISTER   MsrSignId;
  UINT32                      MsrValue;

  MsrSignId.Uint64 = AsmReadMsr64 (MSR_BIOS_SIGN_ID);
  if (MsrSignId.Bits.PatchId != 0) {
    DEBUG ((DEBUG_INFO, "Valid FIT uCode Found: 0x%08X\n", MsrSignId.Bits.PatchId));
    return TRUE;  // FIT_SUCCESSFUL
  } else {
    ///
    /// Read the FIT BIOS Error
    ///
    MsrGetFitBiosError (&FitEntryType, &FitErrorCode);
    MsrValue = (UINT32) AsmReadMsr64 (MSR_BOOT_GUARD_SACM_INFO);
    if (FitErrorCode != FIT_SUCCESSFUL) {
      switch (FitEntryType) {
        case FIT_HEADER_ENTRY:
          DEBUG ((DEBUG_INFO, "Fit Entry Type: Fit Header Entry\n"));
          switch (FitErrorCode) {
            case FIT_SIZE_CHECK:
              DEBUG ((DEBUG_INFO, "Fit Error Code: Size check failed\n"));
              break;
            case FIT_RESERVED_FIELD_CHECK:
              DEBUG ((DEBUG_INFO, "Fit Error Code: Reserved field check failed\n"));
              break;
            case FIT_VERSION_AND_TYPE_CHECK:
              DEBUG ((DEBUG_INFO, "Fit Error Code: Version and Type Check failed\n"));
              break;
            default:
              DEBUG ((DEBUG_INFO, "Fit Error Code: Unknown\n"));
              break;
          }
          break;
        case FIT_MICROCODE_UPDATE_ENTRY:
          DEBUG ((DEBUG_INFO, "Fit Entry Type: Microcode Update Entry\n"));
          switch (FitErrorCode) {
            case FIT_NO_MICROCODE_UPDATE:
              DEBUG ((DEBUG_INFO, "Fit Error Code: No microcode update found\n"));
              REPORT_STATUS_CODE_EX (
                (EFI_ERROR_CODE | EFI_ERROR_UNRECOVERED),
                (EFI_COMPUTING_UNIT_HOST_PROCESSOR|EFI_CU_HP_EC_NO_MICROCODE_UPDATE),
                0,
                &gCpuFspErrorTypeCallerId,
                NULL,
                NULL,
                0
                );
              break;
            case FIT_MICROCODE_UPDATE_FAIL:
              DEBUG ((DEBUG_INFO, "Fit Error Code: Microcode update failure\n"));
              REPORT_STATUS_CODE_EX (
                (EFI_ERROR_CODE | EFI_ERROR_UNRECOVERED),
                (EFI_COMPUTING_UNIT_HOST_PROCESSOR|EFI_CU_HP_EC_MICROCODE_UPDATE),
                0,
                &gCpuFspErrorTypeCallerId,
                NULL,
                NULL,
                0
                );
              break;
            default:
              DEBUG ((DEBUG_INFO, "Fit Error Code: Unknown\n"));
              break;
          }
          break;
        case FIT_STARTUP_ACM_ENTRY:
          if (((MsrValue & B_BOOT_GUARD_SACM_INFO_NEM_ENABLED) != 0)) {
            DEBUG ((DEBUG_INFO, "Fit Entry Type: Startup ACM Entry\n"));
            switch (FitErrorCode) {
              case FIT_STARTUP_ACM_NOT_SUPPORTED:
                DEBUG ((DEBUG_INFO, "Fit Error Code: Startup ACM not supported by CPU\n"));
                break;
              case FIT_FATAL_ERROR_DURING_ACM:
                DEBUG ((DEBUG_INFO, "Fit Error Code: Fatal error during ACM in previous boot\n"));
                break;
              case FIT_CPU_DOES_NOT_SUPPORT_LT:
                DEBUG((DEBUG_INFO, "Fit Error Code: CPU doesn't support LT\n"));
                break;
             case FIT_BIST_ERRORS:
                DEBUG ((DEBUG_INFO, "Fit Error Code: BIST errors\n"));
                break;
             case FIT_BEYOND_END_OF_FIT:
                DEBUG ((DEBUG_INFO, "Fit Error Code: Beyond end of FIT\n"));
                break;
             case FIT_NO_FIT_ACM_TYPE_MISMATCH:
                DEBUG ((DEBUG_INFO, "Fit Error Code: No FIT ACM type mismatch\n"));
                break;
             case FIT_ACM_BASE_SIZE_AND_CHECKS:
                DEBUG ((DEBUG_INFO, "Fit Error Code: ACM base size and checks\n"));
                break;
             default:
                DEBUG ((DEBUG_INFO, "Fit Error Code: Unknown\n"));
                break;
            }
          }
          break;
        case FIT_GENERAL_CHECKS:
          DEBUG ((DEBUG_INFO, "Fit Entry Type: General Checks\n"));
          switch (FitErrorCode) {
            case FIT_DISABLED_BY_CPU:
              DEBUG ((DEBUG_INFO, "Fit Error Code: FIT disabled by CPU\n"));
              break;
            case FIT_POINTER_ERROR:
              DEBUG ((DEBUG_INFO, "Fit Error Code: FIT pointer error (> FFFFFFB0; < FF000000)\n"));
              break;
            case FIT_FIRST_FIT_ENTRY_MISMATCH:
              DEBUG ((DEBUG_INFO, "Fit Error Code: First FIT entry doesn't match \"_FIT_\"\n"));
              break;
            default:
              DEBUG ((DEBUG_INFO, "Fit Error Code: Unknown\n"));
              break;
          }
          break;
        default:
          DEBUG ((DEBUG_INFO, "Fit Entry Type: Unknown\n"));
          DEBUG ((DEBUG_INFO, "Fit Error Code: Unknown\n"));
          break;
      }
    }
  }
  return FALSE;
}

/**
  Return if CPU supports PFAT

  @retval TRUE             If CPU Supports
  @retval FALSE            If CPU doesn't Supports
**/
BOOLEAN
IsPfatEnabled (
  VOID
  )
{
  return MsrIsPfatEnabled ();
}

/**
  Is BIOS GUARD enabled.

  @retval TRUE   BIOS GUARD is supported and enabled.
  @retval FALSE  BIOS GUARD is disabled.
**/
BOOLEAN
IsBiosGuardEnabled (
  VOID
  )
{
#if FixedPcdGetBool(PcdBiosGuardEnable) == 1
  MSR_PLAT_FRMW_PROT_CTRL_REGISTER  ProtCtrlMsr;
  if (MsrIsPfatEnabled ()) {
    ProtCtrlMsr.Uint64 = AsmReadMsr64 (MSR_PLAT_FRMW_PROT_CTRL);
    if(ProtCtrlMsr.Bits.PfatEnable) {
      return TRUE;
    }
  }
#endif
  return FALSE;
}

/**
  Determine if CPU supports Intel Turbo Boost Max Technology 3.0 (ITBM).

  @retval TRUE   ITBM is supported and enabled.
  @retval FALSE  ITBM is disabled.
**/
BOOLEAN
IsItbmSupported (
  VOID
  )
{
  return (GetItbmSupportedStatus ());
}

/**
  Determine if CPU supports Overclocking by reading the number of bins in MSR FLEX_RATIO (194h)

  @retval TRUE   OC is supported and enabled.
  @retval FALSE  OC is disabled.
**/
BOOLEAN
IsOcSupported (
  VOID
  )
{
  MSR_FLEX_RATIO_REGISTER   FlexRatioMsr;
  FlexRatioMsr.Uint64 = AsmReadMsr64 (MSR_FLEX_RATIO);
  return (FlexRatioMsr.Bits.OcBins != 0);
}

/**
  Determine if CPU supports Programmable Core Ratio Limit for the Turbo mode.

  @retval TRUE   Core Ratio Limit for the Turbo mode is supported and enabled.
  @retval FALSE  Core Ratio Limit for the Turbo mode is disabled.
**/
BOOLEAN
IsCoreRatioLimitSupported (
  VOID
  )
{
  return MsrIsTurboRatioLimitProgrammable ();
}

/**
  Determine if CPU supports Programmable TDC/TDP Limit for the Turbo mode.

  @retval TRUE   TDC/TDP Limit is supported and enabled.
  @retval FALSE  TDC/TDP Limit is disabled.
**/
BOOLEAN
IsXETdcTdpLimitSupported (
  VOID
  )
{
  return MsrIsTdpLimitProgrammable ();
}

/**
  Determine if CPU supports Turbo mode.

  @retval TRUE   Turbo mode is supported and enabled.
  @retval FALSE  Turbo mode is disabled.
**/
BOOLEAN
IsTurboModeSupported (
  VOID
  )
{
  CPUID_THERMAL_POWER_MANAGEMENT_EAX   PowerEax;
  MSR_MISC_ENABLES_REGISTER            MiscMsr;

  AsmCpuid (
    CPUID_THERMAL_POWER_MANAGEMENT,
    &PowerEax.Uint32,
    NULL,
    NULL,
    NULL
    );

  MiscMsr.Uint64 = AsmReadMsr64 (MSR_IA32_MISC_ENABLE);
  return ((BOOLEAN) (PowerEax.Bits.TurboBoostTechnology) || (MiscMsr.Bits.TurboModeDisable != 0));
}

/**
  Determine if CPU supports PPIN (Protected Processor Inventory Number)

  @retval TRUE   PPIN feature is available.
  @retval FALSE  PPIN feature is not available.
**/
BOOLEAN
IsPpinFeatureAvailable (
  VOID
  )
{
  return MsrIsPpinCap ();
}

/**
  Determine if CPU supports Hardware P-States.

  @retval TRUE   Hardware P-States is supported and enabled.
  @retval FALSE  Hardware P-States is disabled.
**/
BOOLEAN
IsHwpSupported (
  VOID
  )
{
  MSR_MISC_PWR_MGMT_REGISTER  PwrMgmtMsr;
  PwrMgmtMsr.Uint64 = AsmReadMsr64 (MSR_MISC_PWR_MGMT);
  return (BOOLEAN)PwrMgmtMsr.Bits.EnableHwp;
}

/**
  Determine if CPU supports Mwait.

  @retval TRUE   Mwait is supported and enabled.
  @retval FALSE  Mwait is disabled.
**/
BOOLEAN
IsTimedMwaitSupported (
  VOID
  )
{
  return  MsrIsTimedMwaitSupported ();
}

/**
  Determine if CPU supports LPM.

  @retval TRUE   LPM is supported and enabled.
  @retval FALSE  LPM is disabled.
**/
BOOLEAN
IsLpmSupported (
  VOID
  )
{
  return MsrIsCpuSupportsLpm ();
}

/**
  Determine if CPU supports ConfigTdp.

  @retval TRUE   ConfigTdp is supported and enabled.
  @retval FALSE  ConfigTdp is disabled.
**/
BOOLEAN
IsConfigTdpSupported (
  VOID
  )
{
  UINT8 CtdpLevels;

  CtdpLevels = MsrGetConfigTdpLevels ();

  if ((CtdpLevels == 1) || (CtdpLevels == 2)) {
    return TRUE;
  }
  return FALSE;
}

/**
  Determine if CPU supports Turbo mode.

  @retval TRUE   Efficiency Turbo mode is supported and enabled.
  @retval FALSE  Efficiency Turbo mode is disabled.
**/
BOOLEAN
IsEnergyEfficientTurboSupported (
  VOID
  )
{
  CPUID_THERMAL_POWER_MANAGEMENT_ECX PowerEcx;
  AsmCpuid (
    CPUID_THERMAL_POWER_MANAGEMENT,
    NULL,
    NULL,
    &PowerEcx.Uint32,
    NULL
    );

  return (BOOLEAN) (PowerEcx.Bits.PerformanceEnergyBias);
}

/**
  Determine if CPU supports Hyper-Threading

  @retval TRUE   Hyper-Threading is supported and enabled.
  @retval FALSE  Hyper-Threading is disabled.
**/
BOOLEAN
IsHyperThreadingSupported (
  VOID
  )
{
  CPUID_EXTENDED_TOPOLOGY_EBX  Ebx;

  AsmCpuidEx (
    CPUID_EXTENDED_TOPOLOGY,
    0,
    NULL,
    &Ebx.Uint32,
    NULL,
    NULL
    );

  return ((Ebx.Bits.LogicalProcessors) > 1);
}

/**
  Determines what Ctdp levels are supported on the silicon.

  @param[in] CtdpDownSupported   - Pointer to CtdpDownSupported
  @param[in] CtdpUpSupported     - Pointer to CtdpUpSupported

  @retval VOID
**/
VOID GetConfigTdpLevelsSupported (
  IN UINT8     *CtdpDownSupported,
  IN UINT8     *CtdpUpSupported
  )
{
  UINT8                              CtdpLevels;
  UINT8                              CtdpLevel0Ratio;
  UINT8                              CtdpTempRatio;
  MSR_CONFIG_TDP_NOMINAL_REGISTER    TdpNominal;
  MSR_CONFIG_TDP_LEVEL2_REGISTER     TdpLevel2;
  MSR_CONFIG_TDP_LEVEL1_REGISTER     TdpLevel1;

  *CtdpDownSupported = FALSE;
  *CtdpUpSupported = FALSE;

  CtdpLevels = MsrGetConfigTdpLevels ();

  //
  // Check the known cases of 0 and 2 additional cTDP levels supported first
  //
  if (CtdpLevels == 0) {
    //
    // No additional cTdp Levels are supported
    //
    *CtdpDownSupported = FALSE;
    *CtdpUpSupported = FALSE;
    return;
  } else if (CtdpLevels == 2) {
    //
    // All additional cTDP levels are supported
    //
    *CtdpDownSupported = TRUE;
    *CtdpUpSupported = TRUE;
    return;
  }

  //
  // Now we read the cTDP ratios from each level
  //
  TdpNominal.Uint64 = AsmReadMsr64 (MSR_CONFIG_TDP_NOMINAL);
  CtdpLevel0Ratio = (UINT8) (TdpNominal.Bits.TdpRatio);

  TdpLevel1.Uint64 = AsmReadMsr64 (MSR_CONFIG_TDP_LEVEL1);
  CtdpTempRatio = (UINT8) TdpLevel1.Bits.TdpRatio;

  //
  // If there is no ratio value in LVL1 MSR, check LVL2 MSR
  //
  if (CtdpTempRatio == 0) {
    TdpLevel2.Uint64 = AsmReadMsr64 (MSR_CONFIG_TDP_LEVEL2);
    CtdpTempRatio = (UINT8) TdpLevel2.Bits.TdpRatio;

  }

  //
  //  Check if additional cTDP level is Up or Down
  //
  if (CtdpTempRatio > CtdpLevel0Ratio) {
    *CtdpUpSupported = TRUE;
  } else if (CtdpTempRatio <= CtdpLevel0Ratio) {
    *CtdpDownSupported = TRUE;
  }

  DEBUG ((DEBUG_INFO, "CTDP: CtdpDownSupported = %d, CtdpUpSupported = %d\n", *CtdpDownSupported, *CtdpUpSupported));

  return;
}

/**
  Determines what Ctdp levels are supported on the silicon.

  @param[in] MwaitEcx     - Pointer to MwaitEcx (CPUID_MONITOR_MWAIT_ECX)
  @param[in] MwaitEdx     - Pointer to MwaitEdx (CPUID_MONITOR_MWAIT_EDX)

  @retval VOID
**/
VOID GetSubCStateSupported (
  IN UINT32     *MwaitEcx,
  IN UINT32     *MwaitEdx
  )
{
  AsmCpuid (
    CPUID_MONITOR_MWAIT,
    NULL,
    NULL,
    MwaitEcx,
    MwaitEdx
    );
  return;
}

/**
  Determine Number of system supported cores.
  Note: It's not suitable for the hybrid CPU platform.

  @retval Number of system supported cores.
**/
UINT8
GetCpuNumberofCores (
  VOID
  )
{
  UINT32 Threads;
  CPUID_EXTENDED_TOPOLOGY_EBX  Ebx;

  AsmCpuidEx (
    CPUID_EXTENDED_TOPOLOGY,
    0,
    NULL,
    &Ebx.Uint32,
    NULL,
    NULL
    );

  Threads = Ebx.Bits.LogicalProcessors;

  AsmCpuidEx (
    CPUID_EXTENDED_TOPOLOGY,
    1,
    NULL,
    &Ebx.Uint32,
    NULL,
    NULL
    );

  return (UINT8)(Ebx.Uint32/Threads);
}

/**
  Determine Number of system supported cores.
  Note: It's suitable for both hybrid CPU and Non-hybrid platform.

  @param[out] *NumberOfSupportedCores              - variable that will store Maximum supported cores.
  @param[out] *NumberOfSupportedAtomCores          - variable that will store Maximum supported Atom cores.

  @retval     EFI_SUCCESS     Get the supported core & atom cores.
  @retval     EFI_UNSUPPORTED Doesn't support to get the supported core & atom cores.
**/
EFI_STATUS
GetCpuSupportedCoresAndAtomCores (
  OUT UINT8                     *NumberOfSupportedCores,            OPTIONAL
  OUT UINT8                     *NumberOfSupportedAtomCores         OPTIONAL
  )
{
  return GetCpuSupportedCoreCountInfo (NumberOfSupportedCores, NumberOfSupportedAtomCores);
}

/**
  Determine Number of system supported cores for Soc-North.

  @param[out] *NumberOfSocNorthSupportedAtomCores              - variable that will store Maximum supported Soc-North Atom cores.

  @retval     EFI_SUCCESS     Get the supported core & atom cores.
  @retval     EFI_UNSUPPORTED Doesn't support to get the supported core & atom cores.
**/
EFI_STATUS
GetSupportedSocNorthAtomCores (
  OUT UINT8                     *NumberOfSocNorthSupportedAtomCores
  )
{
  ASSERT (NumberOfSocNorthSupportedAtomCores != NULL);

  if (NumberOfSocNorthSupportedAtomCores != NULL) {
    return GetSocNorthSupportedAtomCoresFru (NumberOfSocNorthSupportedAtomCores);
  } else {
    return EFI_INVALID_PARAMETER;
  }

}

/**
  This function gets number of Soc North Atom cores enabled in the platform.

  @retval      Number of active soc atom core.
**/
UINT8
GetActiveSocNorthAtomCoreCount (
  VOID
  )
{
  return GetSocNorthActiveAtomCoresFru ();
}

/**
  Detect Supported CPU Features

  @param[InOut] CpuEcx   Pointer to CpuEcx (CPUID_VERSION_INFO_ECX).
**/
VOID
GetSupportedCpuFeatures (
  IN UINT32     *RegEcx
  )
{
  AsmCpuid (
    CPUID_VERSION_INFO,
    NULL,
    NULL,
    RegEcx,
    NULL
    );
  return;
}

/**
  Detect if Processor Trace supported or not

  @retval TRUE   IntelProcessorTrace is supported and enabled.
  @retval FALSE  IntelProcessorTrace is disabled.
**/
BOOLEAN
IsIntelProcessorTraceSupported (
  VOID
  )
{
  CPUID_STRUCTURED_EXTENDED_FEATURE_FLAGS_EBX Ebx;

  AsmCpuidEx (
    CPUID_STRUCTURED_EXTENDED_FEATURE_FLAGS,
    0,
    NULL,
    &Ebx.Uint32,
    NULL,
    NULL
    );

  return (BOOLEAN) (Ebx.Bits.IntelProcessorTrace);
}

/**
  Returns Generation string of the respective CPU

  @retval      Character pointer of Generation string
**/
CONST CHAR8*
GetGenerationString (
  VOID
  )
{
  UINT32               CpuFamilyId;
  CpuFamilyId = GetCpuFamily ();

  return GetFruGenerationString (CpuFamilyId);
}

/**
  Returns Revision Table

  @retval      Character pointer of Revision Table String
**/
CONST CHAR8*
GetRevisionTable (
  VOID
  )
{
  CPUID_VERSION_INFO_EAX  Eax;

  AsmCpuid (CPUID_VERSION_INFO, &Eax.Uint32, NULL, NULL, NULL);

  return GetRevisionTableString (Eax.Uint32 & CPUID_FULL_FAMILY_MODEL_STEPPING);
}

/**
  Returns Sku String

  @retval      Character pointer of Sku String
**/
CONST CHAR8*
GetSkuString (
  VOID
  )
{
  CPU_SKU                            CpuSku;
  CONST CHAR8                        *SkuString = NULL;

  CpuSku = GetCpuSku ();
  switch (CpuSku) {
    case EnumCpuUlt:
      SkuString = "ULT";
      break;

    case EnumCpuUlx:
      SkuString = "ULX";
      break;

    case EnumCpuTrad:
      SkuString = "DT";
      break;

    case EnumCpuHalo:
      SkuString = "Halo";
      break;

    default:
      SkuString = NULL;
      break;
  }
  return SkuString;
}

/**
  Returns PPAM version details to support DGR with Nifty Rock feature based on SKU.

  @retval 0    DGR with Nifty Rock feature is not supported, No need to load PPAM.
  @retval 11   DGR with Nifty Rock feature is supported with PPAM Version 1.1.
**/
UINT8
NiftyRockSupportLevel (
  VOID
  )
{
  HECI_FW_STS3_REGISTER    Hfsts3;
  //
  // DGR with NiftyRock supports only if below conditions are met.
  // 1. CSME FW image shall be of Corp Image. Heci FW STS3 register bits(6:4) = 3(HECI1_CSE_GS2_FW_SKU_CORPORATE).
  // 2. Platform Info MSR bit59 (SMM_SUPOVR_STATE_LOCK_ENABLE) shall be Set.
  // 3. SMX shall be supported.
  //

  Hfsts3.ul = 0;
  Hfsts3.ul = PciSegmentRead32 (PCI_SEGMENT_LIB_ADDRESS (ME_SEGMENT, ME_BUS, ME_DEVICE_NUMBER, HECI_FUNCTION_NUMBER, R_ME_HFS_3));
  // In case if ME Device is disabled, Status may get 111 for FwSku, but still it returns as Not Supported.
  if (Hfsts3.r.FwSku != ME_FWSTS3_FW_SKU_CORPORATE) {
    DEBUG ((DEBUG_INFO, "NiftyRock not supported. Reason-CSME FW is not Corporate FW.\n"));
    return NO_NR_PPAM_SUPPORT;
  }

  if (! MsrIsSmmSupovrStateLockSupported ()) {
    DEBUG ((DEBUG_INFO, "NiftyRock not supported. Reason-SMM_SUPOVR_STATE_LOCK MSR is not supported.\n"));
    return NO_NR_PPAM_SUPPORT;
  }

  if (! IsSmxSupported ()) {
    DEBUG ((DEBUG_INFO, "NiftyRock not supported. Reason-SMX is not supported.\n"));
    return NO_NR_PPAM_SUPPORT;
  }

  return NR_PPAM_11_SUPPORT;
}

/**
  This function return max and min bus ratio.

  @param[out]  MaxBusRatio
  @param[out]  MinBusRatio
**/
VOID
GetBusRatio (
  OUT UINT8 *MaxBusRatio, OPTIONAL
  OUT UINT8 *MinBusRatio OPTIONAL
  )
{
  MsrGetBusRatio (MaxBusRatio, MinBusRatio);
  return;
}

/**
  This function return max Efficiency Ratio.

  @retval Max efficiency ratio
**/
UINT8
GetMaxEfficiencyRatio (
  VOID
  )
{
  return MsrGetMaxEfficiencyRatio ();
}

/**
  This function return max Non-Turbo Ratio.

  @retval Max Non-Turbo Ratio
**/
UINT8
GetMaxNonTurboRatio (
  VOID
  )
{
  return MsrGetMaxNonTurboRatio ();
}

/**
  This function return the supported Config TDP Levels.

  @retval number of config TDP levels
**/
UINT8
GetConfigTdpLevels (
  VOID
  )
{
  return MsrGetConfigTdpLevels ();
}

/**
  This function return the supported Prmmr Size

  @retval  Supported Prmrr Size
**/
UINT32
GetMaxSupportedPrmrrSize (
  VOID
  )
{
  MSR_PRMRR_VALID_CONFIG_REGISTER  PrmrrValidConfigMsr;
  PrmrrValidConfigMsr.Uint64 = AsmReadMsr64 (MSR_PRMRR_VALID_CONFIG);
  return (UINT32) PrmrrValidConfigMsr.Uint64;
}

/**
  This function returns the supported Physical Address Size

  @retval supported Physical Address Size.
**/
UINT8
GetMaxPhysicalAddressSize (
  VOID
  )
{
  return GetMaxPhysicalAddressSizeFru ();
}

/**
  Return if Edram Enable

  @retval TRUE             If Edram Enable
  @retval FALSE            If Edram Disable
**/
BOOLEAN
IsEdramEnable (
  VOID
  )
{
  return MsrIsEdramEnable ();
}

/**
  This function is used to detect if SA VR Support

  @retval TRUE     SA VR Support
  @retval FALSE    Not Support
**/
BOOLEAN
IsSaVrSupport (
  VOID
  )
{
  return IsSaVrSupportFru ();
}

/**
  This function return if TME is supported

  @retval TRUE             If TME is supported
  @retval FALSE            If TME is not supported
**/
BOOLEAN
IsTmeSupported (
  VOID
  )
{
  CPUID_STRUCTURED_EXTENDED_FEATURE_FLAGS_ECX  CpuidStructuredExtendedFeatureEcx;

  ///
  /// Verify TME supported through CPUID.7.0.ECX.13
  ///
  AsmCpuidEx (CPUID_STRUCTURED_EXTENDED_FEATURE_FLAGS,
              CPUID_STRUCTURED_EXTENDED_FEATURE_FLAGS_SUB_LEAF_INFO,
              NULL,
              NULL,
              &CpuidStructuredExtendedFeatureEcx.Uint32,
              NULL
              );

  if ((CpuidStructuredExtendedFeatureEcx.Uint32 & BIT13) != BIT13) {
    return FALSE;
  }

  return TRUE;
}

/**
  Return if the processor is a preproduction sample

  @retval TRUE          If the processor is a preproduction sample
  @retval FALSE         If the part is intended for production
**/
BOOLEAN
IsSamplePart (
  VOID
  )
{
  return MsrIsSamplePartFru ();
}

/**
  This function return Package TDP

  @retval Package TDP
**/
UINT16
GetPackageTdp (
  VOID
  )
{
  UINT16                                PackageTdp;
  UINT16                                PackageTdpWatt;
  UINT16                                TempPackageTdp;
  UINT8                                 ProcessorPowerUnit;
  MSR_PACKAGE_POWER_SKU_REGISTER        PackagePowerSkuMsr;
  MSR_PACKAGE_POWER_SKU_UNIT_REGISTER   PackagePowerSkuUnitMsr;
  ///
  /// Find Package TDP value in 1/100 Watt units
  ///
  PackagePowerSkuMsr.Uint64     = AsmReadMsr64 (MSR_PACKAGE_POWER_SKU);
  PackagePowerSkuUnitMsr.Uint64 = AsmReadMsr64 (MSR_PACKAGE_POWER_SKU_UNIT);

  ProcessorPowerUnit           = (UINT8) (PackagePowerSkuUnitMsr.Bits.PwrUnit);
  if (ProcessorPowerUnit == 0) {
    ProcessorPowerUnit = 1;
  } else {
    ProcessorPowerUnit = (UINT8) LShiftU64 (2, (ProcessorPowerUnit - 1));
    if (IsSimicsEnvironment () && ProcessorPowerUnit == 0) {
      ProcessorPowerUnit = 1;
    }
  }
  TempPackageTdp = (UINT16) PackagePowerSkuMsr.Bits.PkgTdp;
  PackageTdpWatt = (UINT16) DivU64x32 (TempPackageTdp, ProcessorPowerUnit);

  PackageTdp = (PackageTdpWatt * 100);
  if ((TempPackageTdp % ProcessorPowerUnit) !=0) {
    PackageTdp += ((TempPackageTdp % ProcessorPowerUnit) * 100) / ProcessorPowerUnit;
  }

  return PackageTdp;
}