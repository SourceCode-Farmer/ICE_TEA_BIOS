/** @file
  Supporting functions for profile sync driver ASF communication.

@copyright
  INTEL CONFIDENTIAL
  Copyright 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains a 'Sample Driver' and is licensed as such under the terms
  of your license agreement with Intel or your vendor. This file may be modified
  by the user, subject to the additional terms of the license agreement.

@par Specification Reference:
**/
#ifndef _WIFI_PROFILE_SYNC_ASF_MSGS_H_
#define _WIFI_PROFILE_SYNC_ASF_MSGS_H_

#include "AsfMsgs.h"

// Root CA index status if no root CA avaliable
#define ROOT_CA_NOT_AVAILABLE     0xFFFFFFFF

//
// AMT WiFi profile data defines
//
#define MAC_ADDR_SIZE                 6
#define PROFILE_NAME_SIZE             33
#define SSID_SIZE                     32
#define PSK_KEY_SIZE                  64
#define USER_CRED_IDENTITY_SIZE       81
#define USER_CRED_DOMAIN_SIZE         129
#define USER_CRED_USERNAME_SIZE       129
#define USER_CRED_PASSWORD_SIZE       33
#define SERVER_CERT_NAME_SIZE         81
#define MAX_CLIENT_KEY_SIZE           6500
#define MAX_CLIENT_CERTIFICATE_SIZE   4100

#pragma pack(push, 1)
///
/// ASF Message header with profile name char array as data
///
typedef struct {
  ASF_MESSAGE_HEADER              AsfHeader;                      ///< ASF Message Header
  CHAR8                           Data[PROFILE_NAME_SIZE];        ///< ASF Data Request
} ASF_PROFILE_MESSAGE_HEADER;

///
/// Get Wifi profile name response from ASF
///
typedef struct {
  ASF_MESSAGE_HEADER              AsfHeader;                      ///< ASF Message Header
  UINT32                          Status;                         ///< ASF Response Message Status
  CHAR8                           ProfileName[PROFILE_NAME_SIZE]; ///< ASF Return WiFi Profile Name.
} GET_WIFI_PROFILE_NAME_RESPONSE;

///
/// ASF buffer for Wifi profile name
///
typedef union {
  ASF_MESSAGE_HEADER              Request;                        ///< ASF Request
  GET_WIFI_PROFILE_NAME_RESPONSE  Response;                       ///< ASF Response
} GET_WIFI_PROFILE_NAME_BUFFER;

///
/// Get Wifi profile data response from ASF
///
typedef struct {
  UINT32  Status;
  UINT8   MACAddress[MAC_ADDR_SIZE];
  CHAR8   ProfileName[PROFILE_NAME_SIZE];
  UINT8   SSIdLength;
  CHAR8   SSId[SSID_SIZE];
  UINT32  EncryptionMethod;
  UINT32  AuthenticationMethod;
  UINT8   PskKeyType;
  UINT8   PskKeyLen;
  UINT8   PskKey[PSK_KEY_SIZE];
//
//  The following fields are related to 802.1x Authentication:
//  (see EDKII_8021XProfile)
//
  UINT32  AuthenticationProtocol_eapMethod;
  UINT32  AuthenticationProtocolInnerMethod;
  CHAR8   UserCredentialsRoamingIdentity[USER_CRED_IDENTITY_SIZE];
  CHAR8   UserCredentialsDomain[USER_CRED_DOMAIN_SIZE];
  CHAR8   UserCredentialsUsername[USER_CRED_USERNAME_SIZE];
  CHAR8   UserCredentialsPassword[USER_CRED_PASSWORD_SIZE];
  UINT8   UserCredentialsClientCertificateAvailable;
  UINT32  RootCACertificateIndex;
  CHAR8   ServerCertificateName[SERVER_CERT_NAME_SIZE];
  UINT16  ServerCertificateNameComparison;
} WIFI_PROFILE_DATA;

typedef struct {
  ASF_MESSAGE_HEADER              AsfHeader;                      ///< ASF Message Header
  WIFI_PROFILE_DATA               Profile;                        ///< ASF Return WiFi Profile Data.
} GET_WIFI_PROFILE_DATA_RESPONSE;

///
/// ASF buffer for Wifi profile data
///
typedef union {
  ASF_PROFILE_MESSAGE_HEADER      Request;                        ///< ASF Request
  GET_WIFI_PROFILE_DATA_RESPONSE  Response;                       ///< ASF Response
} GET_WIFI_PROFILE_DATA_BUFFER;

///
/// Get Wifi profile specific 8021X client certificate from ASF
///
typedef struct {
  UINT32  Status;
  UINT32  CertSize;
  UINT8   Certificate[MAX_CLIENT_CERTIFICATE_SIZE];
} WIFI_8021X_CLIENT_CERT;

typedef struct {
  ASF_MESSAGE_HEADER              AsfHeader;                      ///< ASF Message Header
  WIFI_8021X_CLIENT_CERT          Cert;                           ///< ASF Return WiFi Client Certificate.
} GET_WIFI_CLIENT_CERT_RESPONSE;

///
/// ASF buffer for Wifi profile client certificate
///
typedef union {
  ASF_PROFILE_MESSAGE_HEADER      Request;                        ///< ASF Request
  GET_WIFI_CLIENT_CERT_RESPONSE   Response;                       ///< ASF Resposne
} GET_WIFI_CLIENT_CERT_BUFFER;

///
/// Get Wifi profile specific 8021X client key from ASF
///
typedef struct {
  UINT32  Status;
  UINT32  KeySize;
  UINT8   Key[MAX_CLIENT_KEY_SIZE];
} WIFI_8021X_CLIENT_KEY;

typedef struct {
  ASF_MESSAGE_HEADER              AsfHeader;                      ///< ASF Message Header
  WIFI_8021X_CLIENT_KEY           Key;                            ///< ASF Return WiFi Client Key.
} GET_WIFI_CLIENT_KEY_RESPONSE;

///
/// ASF buffer for Wifi profile client key
///
typedef union {
  ASF_PROFILE_MESSAGE_HEADER      Request;                        ///< ASF Request
  GET_WIFI_CLIENT_KEY_RESPONSE    Response;                       ///< ASF Response
} GET_WIFI_CLIENT_KEY_BUFFER;

//
//  WiFi profile data structure recieved from ASF
//
typedef struct {
  WIFI_PROFILE_DATA       *ProfileData;
  WIFI_8021X_CLIENT_CERT  *Cert;
  WIFI_8021X_CLIENT_KEY   *Key;
  ROOT_CA_CERTIFICATE     *CaCert;
} WIFI_PROFILE;

#pragma pack(pop)

#endif
