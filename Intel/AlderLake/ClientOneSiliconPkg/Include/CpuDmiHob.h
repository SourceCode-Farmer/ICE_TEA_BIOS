/** @file
  The GUID definition for CpuDmiHob

@copyright
  INTEL CONFIDENTIAL
  Copyright 2020 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains an 'Intel Peripheral Driver' and is uniquely identified as
  "Intel Reference Module" and is licensed for Intel CPUs and chipsets under
  the terms of your license agreement with Intel or your vendor. This file may
  be modified by the user, subject to additional terms of the license agreement.

@par Specification Reference:
**/
#ifndef _CPU_DMI_HOB_H_
#define _CPU_DMI_HOB_H_

#include <Base.h>

#define CPU_DMI_HWEQ_COEFFS_MAX    8

extern EFI_GUID gCpuDmiHobGuid;
#pragma pack (push,1)

/**
  The CPU_DMI_HOB block describes the expected configuration of Cpu Dmi
**/
typedef struct {
  EFI_HOB_GUID_TYPE           EfiHobGuidType;                           ///< Offset 0 - 23: GUID Hob type structure for gCpuDmieHobGuid

  ///
  /// These members describe the configuration of each CPU Dmi root port.
  ///
  UINT8   CmGen3[CPU_DMI_HWEQ_COEFFS_MAX];                 ///< Coefficient C-1 for Gen3
  UINT8   CpGen3[CPU_DMI_HWEQ_COEFFS_MAX];                 ///< Coefficient C+1 for Gen3
  UINT8   CmGen4[CPU_DMI_HWEQ_COEFFS_MAX];                 ///< Coefficient C-1 for Gen4
  UINT8   CpGen4[CPU_DMI_HWEQ_COEFFS_MAX];                 ///< Coefficient C+1 for Gen4
} CPU_DMI_HOB;
#pragma pack (pop)
#endif
