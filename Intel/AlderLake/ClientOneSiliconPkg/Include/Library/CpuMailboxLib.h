/** @file
  Header file for Cpu Mailbox Lib.

@copyright
  INTEL CONFIDENTIAL
  Copyright 2014 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains an 'Intel Peripheral Driver' and is uniquely identified as
  "Intel Reference Module" and is licensed for Intel CPUs and chipsets under
  the terms of your license agreement with Intel or your vendor. This file may
  be modified by the user, subject to additional terms of the license agreement.

@par Specification Reference:
**/

#ifndef _CPU_MAILBOX_LIB_H_
#define _CPU_MAILBOX_LIB_H_

//
//  Mailbox Related Definitions
//
#define MAILBOX_TYPE_PCODE                    0x00000001
#define MAILBOX_TYPE_OC                       0x00000002
#define MAILBOX_TYPE_VR_MSR                   0x00000003
#define PCODE_MAILBOX_INTERFACE_OFFSET        0x5DA4
#define PCODE_MAILBOX_DATA_OFFSET             0x5DA0
#define PCU_CR_BCLK_FREQ_MCHBAR               0x00005F60
#define OC_MAILBOX_MSR                        0x00000150
#define VR_MAILBOX_MSR_INTERFACE              0x00000607
#define VR_MAILBOX_MSR_DATA                   0x00000608

#define MAILBOX_PCODE_CMD_READ_PEG_CRIO_CR                   0x00000029
#define MAILBOX_PCODE_CMD_WRITE_PEG_CRIO_CR                  0x0000002A
#define N_MAILBOX_TARGET_IO_BLOCK_OFFSET                     0x18

#define MAILBOX_PCODE_CMD_READ_C6DRAM_CONFIG                 0x0000001C
#define B_MAILBOX_BIOS_ALLOW_C6DRAM                          BIT0
#define B_MAILBOX_C6DRAM_SUPPORTED                           BIT1
#define MAILBOX_PCODE_CMD_WRITE_C6DRAM_CONFIG                0x0000001D
#define MAILBOX_OC_CMD_BCLK_FREQUENCY_CMD                    0x00000005

#define MAILBOX_PCODE_CMD_BCLK_CONFIG                        0x0000003F
#define MAILBOX_PCODE_BCLK_CONFIG_SET_SSC_CONTROL_SUBCOMMAND 0x00000004
#define MAILBOX_PCODE_BCLK_CONFIG_GET_SSC_CONTROL_SUBCOMMAND 0x00000003

//
// Inform Graphics Memory Config
//
#define BIOS_MAILBOX_INFORM_GRAPHICS_MEMORY_CONFIG           0x55
#define SELECT_ALTERNATE_DID2                                0x0

//
// HwP Misc Functions
//
#define MAILBOX_PCODE_CMD_READ_AUTONOMOUS_PARAMS             0x00000010
#define MAILBOX_PCODE_CMD_WRITE_AUTONOMOUS_PARAMS            0x00000011
#define MAILBOX_PCODE_CMD_READ_PCU_MISC_CONFIG               0x00000005
#define MAILBOX_PCODE_CMD_WRITE_PCU_MISC_CONFIG              0x00000006
#define MAILBOX_OC_CMD_READ_PER_CORE_RATIO_LIMITS_CMD        0x0000001C
#define MAILBOX_OC_CMD_WRITE_PER_CORE_RATIO_LIMITS_CMD       0x0000001D

//
// MAILBOX_BIOS_CMD_MISC_ALG_CONFIG_INTERFACE command set
//
#define MAILBOX_PCODE_CMD_MISC_ALG_CONFIG_INTERFACE                        0x2B
#define MAILBOX_PCODE_SUBCMD_READ_HDC_DIS_WHEN_OSREQ_ABOVE_PE_SUBCOMMAND   0x00
#define MAILBOX_PCODE_SUBCMD_WRITE_HDC_DIS_WHEN_OSREQ_ABOVE_PE_SUBCOMMAND  0x01
#define MAILBOX_PCODE_SUBCMD_READ_EPB_PECI_CONTROL_SUBCOMMAND              0x02
#define MAILBOX_PCODE_SUBCMD_WRITE_EPB_PECI_CONTROL_SUBCOMMAND             0x03
#define MAILBOX_PCODE_SUBCMD_READ_RING_DISTRESS_DISABLE_SUBCOMMAND         0x04
#define MAILBOX_PCODE_SUBCMD_WRITE_RING_DISTRESS_DISABLE_SUBCOMMAND        0x05
#define MAILBOX_PCODE_SUBCMD_READ_RING_AUTOGV_DISABLE_SUBCOMMAND           0x08
#define MAILBOX_PCODE_SUBCMD_WRITE_RING_AUTOGV_DISABLE_SUBCOMMAND          0x09

#define MAILBOX_BIOS_CMD_WRITE_HSPHY_CONFIG                  0x0000002D
#define B_MAILBOX_BIOS_ALLOW_HSPHY_1                         BIT0

//
// MAILBOX_BIOS_POWER_RAIL_SHUTDOWN_POLICY
//
#define MAILBOX_PCODE_CMD_BIOS_WRITE_POWER_RAIL_SHUTDOWN_POLICY            0x42
#define B_MAILBOX_INF_DFX_PWROFF_REQ                                       BIT0
#define V_MAILBOX_INF_DFX_PWROFF_REQ_DIS                                      0  ///< INF_DFX will keep up
#define V_MAILBOX_INF_DFX_PWROFF_REQ_EN                                       1  ///< INF_DFX will shutdown
#define MAILBOX_PCODE_CMD_BIOS_READ_POWER_RAIL_SHUTDOWN_POLICY             0x43

#ifndef MAILBOX_WAIT_TIMEOUT
#define MAILBOX_WAIT_TIMEOUT  1000  ///< 1 millisecond
#endif
#ifndef MAILBOX_WAIT_STALL
#define MAILBOX_WAIT_STALL    1     ///< 1 microsecond
#endif
#ifndef MAILBOX_READ_TIMEOUT
#define MAILBOX_READ_TIMEOUT  10    ///< 10 microseconds
#endif



//
//  Pcode Mailbox completion codes
//
#define PCODE_MAILBOX_CC_SUCCESS                      0x0
#define PCODE_MAILBOX_CC_ILLEGAL_CMD                  0x1
#define PCODE_MAILBOX_CC_TIMEOUT                      0x2
#define PCODE_MAILBOX_CC_ILLEGAL_DATA                 0x3
#define PCODE_MAILBOX_CC_EDRAM_NOT_FUNCTIONAL         0x4
#define PCODE_MAILBOX_CC_ILLEGAL_VR_ID                0x5
#define PCODE_MAILBOX_CC_LOCKED                       0x6
#define PCODE_MAILBOX_CC_VR_ERROR                     0x7
#define PCODE_MAILBOX_CC_ILLEGAL_SUBCOMMAND           0x8
#define PCODE_MAILBOX_CC_EDRAM_CURRENTLY_UNAVAILABLE  0xA

#define MAILBOX_PARAM_1_OFFSET                        8
#define MAILBOX_PARAM_2_OFFSET                        16

///
/// Expanded Overclocking Mailbox interface defintion, contains command id/completion code,
/// input parameters and the run/busy bit
///
typedef union _OC_MAILBOX_INTERFACE {
  UINT32 InterfaceData;          ///< All bit fields as a 32-bit value.
  ///
  /// Individual bit fields.
  ///
  struct {
    UINT8 CommandCompletion : 8; ///< Command ID and completion code
    UINT8 Param1            : 8; ///< Parameter 1, generally used to specify the CPU Domain ID
    UINT8 Param2            : 8; ///< Parameter 2, only current usage is as a core index for ratio limits message
    UINT8 Reserved          : 7; ///< Reserved for future use
    UINT8 RunBusy           : 1; ///< Run/Busy bit. This bit is set by BIOS to indicate the mailbox buffer is ready. pcode will clear this bit after the message is consumed.
  } Fields;
} OC_MAILBOX_INTERFACE;

///
///  Consolidated OC mailbox command structure containing both data and interface information
///
typedef struct _OC_MAILBOX_FULL {
  UINT32               Data;      ///< OC Mailbox read/write data
  OC_MAILBOX_INTERFACE Interface; ///< OC mailbox interface
} OC_MAILBOX_FULL;

///
///  Expanded Pcode Mailbox interface defintion, contains command id, param1, param2,
///  and the run/busy bit
///
typedef union _PCODE_MAILBOX_INTERFACE {
  UINT32 InterfaceData;   ///< All bit fields as a 32-bit value.
  ///
  /// Individual bit fields.
  ///
  struct {
    UINT32 Command  : 8;  ///< Pcode mailbox command
    UINT32 Param1   : 8;  ///< This field contains parameter 1, used by sub-commands.
    UINT32 Param2   : 13; ///< This field contains parameter 2.
    UINT32 Reserved : 2;  ///< Reserved for future use
    UINT32 RunBusy  : 1;  ///< Run/Busy bit. This bit is set by BIOS to indicate the mailbox buffer is ready. pcode will clear this bit after the message is consumed.
  } Fields;
} PCODE_MAILBOX_INTERFACE;

///
///  Consolidated Pcode mailbox command structure containing both data and interface information
///
typedef struct _PCODE_MAILBOX_FULL {
  PCODE_MAILBOX_INTERFACE Interface; ///< Pcode mailbox interface
  UINT32                  Data;      ///< Pcode mailbox read/write data
} PCODE_MAILBOX_FULL;

/**
  Generic Mailbox function for mailbox write commands. This function will
  poll the mailbox interface for control, issue the write request, poll
  for completion, and verify the write was succussful.

  @param[in]  MailboxType     The type of mailbox interface to read. The Overclocking mailbox is defined as MAILBOX_TYPE_OC = 2.
  @param[in]  MailboxCommand  Overclocking mailbox command data
  @param[in]  MailboxData     Overclocking mailbox interface data
  @param[out] *MailboxStatus  Pointer to the mailbox status returned from pcode. Possible mailbox status values are:
                              - SUCCESS (0)               Command succeeded.
                              - OC_LOCKED (1)             Overclocking is locked. Service is read-only.
                              - INVALID_DOMAIN (2)        Invalid Domain ID provided in command data.
                              - MAX_RATIO_EXCEEDED (3)    Ratio exceeds maximum overclocking limits.
                              - MAX_VOLTAGE_EXCEEDED (4)  Voltage exceeds input VR's max voltage.
                              - OC_NOT_SUPPORTED (5)      Domain does not support overclocking.

  @retval EFI_STATUS
          - EFI_SUCCESS           Command succeeded.
          - EFI_INVALID_PARAMETER Invalid read data detected from pcode.
          - EFI_UNSUPPORTED       Unsupported MailboxType parameter.
**/
EFI_STATUS
EFIAPI
MailboxWrite (
  IN UINT32  MailboxType,
  IN UINT32  MailboxCommand,
  IN UINT32  MailboxData,
  OUT UINT32 *MailboxStatus
  );

/**
  Generic Mailbox function for mailbox read commands. This function will write
  the read request from MailboxType, and populate the read results in the MailboxDataPtr.

  @param[in]  MailboxType     The type of mailbox interface to read. The Overclocking mailbox is defined as MAILBOX_TYPE_OC = 2.
  @param[in]  MailboxCommand  Overclocking mailbox command data
  @param[out] *MailboxDataPtr Pointer to the overclocking mailbox interface data
  @param[out] *MailboxStatus  Pointer to the mailbox status returned from pcode. Possible mailbox status are
                              - SUCCESS (0)               Command succeeded.
                              - OC_LOCKED (1)             Overclocking is locked. Service is read-only.
                              - INVALID_DOMAIN (2)        Invalid Domain ID provided in command data.
                              - MAX_RATIO_EXCEEDED (3)    Ratio exceeds maximum overclocking limits.
                              - MAX_VOLTAGE_EXCEEDED (4)  Voltage exceeds input VR's max voltage.
                              - OC_NOT_SUPPORTED (5)      Domain does not support overclocking.

  @retval EFI_STATUS
          - EFI_SUCCESS           Command succeeded.
          - EFI_INVALID_PARAMETER Invalid read data detected from pcode.
          - EFI_UNSUPPORTED       Unsupported MailboxType parameter.

**/
EFI_STATUS
EFIAPI
MailboxRead (
  IN UINT32  MailboxType,
  IN UINT32  MailboxCommand,
  OUT UINT32 *MailboxDataPtr,
  OUT UINT32 *MailboxStatus
  );

/**
  Poll the run/busy bit of the mailbox until available or timeout expires.

  @param[in]  MailboxType

  @retval EFI_STATUS
          - EFI_SUCCESS           Command succeeded.
          - EFI_TIMEOUT           Command timeout.
**/
EFI_STATUS
EFIAPI
PollMailboxReady (
  IN UINT32 MailboxType
  );
#endif
