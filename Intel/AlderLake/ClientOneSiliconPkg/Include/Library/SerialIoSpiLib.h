/** @file
  Basic SPI Library that allows SPI communication through Read/Write/WriteRead operations.

@copyright
  INTEL CONFIDENTIAL
  Copyright 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains a 'Sample Driver' and is licensed as such under the terms
  of your license agreement with Intel or your vendor. This file may be modified
  by the user, subject to the additional terms of the license agreement.

@par Specification Reference:
**/

#ifndef _SERIAL_IO_SPI_LIB_H_
#define _SERIAL_IO_SPI_LIB_H_

/**
  Configure Initial Chip Select settings

  @param[in]  SpiBaseAddress - Base Address of SPI Controller
  @param[in]  SpiDeviceConfig  SerialIo SPI Config
  @param[in]  CsOutput       - Default CS used by the SPI HC. 0 = CS0, 1 = CS1, CS2, CS3.
  @param[in]  CsMode         - Sets Chip Select Control mode Hardware or Software. 0 = HW Control, 1 = SW Control
  @param[in]  CsState        - 0 = CS is set to low; 1 = CS is set to high
  @param[in]  CsEnableBits         Bit based Cs Enable setting (BIT0 -> Enable CS0, BIT1 -> Enable CS1 etc.)
  @param[in]  CsIdlePolarityBits   Bit based Cs Polarity - Sets Inactive/Idle polarity of each CS
**/
VOID
SerialIoSpiChipSelectConfig (
  IN UINT64 SpiBaseAddress,
  IN UINT8  CsOutput,
  IN UINT8  CsMode,
  IN UINT8  CsState,
  IN UINT8  CsEnableBits,
  IN UINT8  CsIdlePolarityBits
  );

/**
  Execute an SPI write
  - Read data is ingnored

  @param[in]   PciCfgBase        Pci Config Base Address of specified SPI controller
                                 @Note: If SPI Number is provided this parameter is ignored and can be left as 0
  @param[in]   SpiNumber         Optional: SPI controller applicable on bus 0 only
  @param[in]   WriteLength       Write length of the current transaction
  @param[in]   WriteBuffer       Pointer to data in write buffer
  @param[in]   TimeBudget        Time allotted to verify if SPI is enabled
  @param[in]   Frequency         Bus clock frequency (in Hz)
  @param[in]   ClockIdlePolarity Clock Idle Polarity 0 = Low, 1 = High
  @param[in]   ClockPhase        0 - Clock is inactive one cycle at the start of a frame and one cycle at the end of a frame
                                 1 - Clock is inactive for one halfcycle at the start of a frame and one cycle at the end of a frame
  @param[in]   CsOutputSelect    Select which SPI CS Signal is to be driven by the SPI Frame (0 = CS0; 1 = CS1 etc.)
  @param[in]   CsActivePolarity  Selects SPI ChipSelect signal polarity, 0 = Low, 1 = High
  @param[in]   AssertCs          FALSE - CS will remain in De-Asserted case. Used for scenario when external CS is used (ex GPIO)
                                 TRUE - CS either CS0/CS1 will be asserted once SPI transaction starts

  @retval   EFI_INVALID_PARAMETER   Invalid length parameters were passed into this function
  @retval   EFI_TIMEOUT             Timeout occurred in SPI disable/enable or transaction timeout
  @retval   EFI_DEVICE_ERROR        The verification failed
  @retval   EFI_SUCCESS             Write was successful
  @retval   EFI_BAD_BUFFER_SIZE     Write buffer is NULL
**/
EFI_STATUS
SerialIoSpiWrite (
  IN UINTN        PciCfgBase,
  IN UINT8        *SpiNumber  OPTIONAL,
  IN UINT32       WriteLength,
  IN UINT8        *WriteBuffer,
  IN UINT64       TimeBudget,
  IN UINTN        Frequency,
  IN UINT8        ClockIdlePolarity,
  IN UINT8        ClockPhase,
  IN UINT8        CsOutputSelect,
  IN UINT8        CsActivePolarity,
  IN BOOLEAN      AssertCs
  );

/**
  Execute an SPI read command
  - Write data is always 0x0

  @param[in]   PciCfgBase        Pci Config Base Address of specified SPI controller
                                 @Note: If SPI Number is provided this parameter is ignored and can be left as 0
  @param[in]   SpiNumber         Optional: SPI controller applicable on bus 0 only
  @param[in]   ReadLength        Read length of the current transaction
  @param[in]   ReadBuffer        Pointer to data in read buffer
  @param[in]   TimeBudget        Time allotted to verify if SPI is enabled
  @param[in]   Frequency         Bus clock frequency (in Hz)
  @param[in]   ClockIdlePolarity Clock Idle Polarity 0 = Low, 1 = High
  @param[in]   ClockPhase        0 - Clock is inactive one cycle at the start of a frame and one cycle at the end of a frame
                                 1 - Clock is inactive for one halfcycle at the start of a frame and one cycle at the end of a frame
  @param[in]   CsOutputSelect    Select which SPI CS Signal is to be driven by the SPI Frame (0 = CS0; 1 = CS1 etc.)
  @param[in]   CsActivePolarity  Selects SPI ChipSelect signal polarity, 0 = Low, 1 = High
  @param[in]   AssertCs          FALSE - CS will remain in De-Asserted case. Used for scenario when external CS is used (ex GPIO)
                                 TRUE - CS either CS0/CS1 will be asserted once SPI transaction starts

  @retval   EFI_INVALID_PARAMETER   Invalid length parameters were passed into this function
  @retval   EFI_TIMEOUT             Timeout occurred in SPI disable/enable or transaction timeout
  @retval   EFI_DEVICE_ERROR        The verification failed
  @retval   EFI_SUCCESS             Read was successful
  @retval   EFI_BAD_BUFFER_SIZE     Read buffer is NULL
**/
EFI_STATUS
SerialIoSpiRead (
  IN UINTN        PciCfgBase,
  IN UINT8        *SpiNumber  OPTIONAL,
  IN UINT32       ReadLength,
  IN UINT8        *ReadBuffer,
  IN UINT64       TimeBudget,
  IN UINTN        Frequency,
  IN UINT8        ClockIdlePolarity,
  IN UINT8        ClockPhase,
  IN UINT8        CsOutputSelect,
  IN UINT8        CsActivePolarity,
  IN BOOLEAN      AssertCs
  );

/**
  Execute an SPI write-read operation

  @param[in]   PciCfgBase        Pci Config Base Address of specified SPI controller
                                 @Note: If SPI Number is provided this parameter is ignored and can be left as 0
  @param[in]   SpiNumber         Optional: SPI controller applicable on bus 0 only
  @param[in]   TransactionLength Read length of the current transaction
  @param[in]   WriteBuffer       Pointer to data in write buffer
  @param[in]   ReadBuffer        Pointer to data in read buffer
  @param[in]   TimeBudget        Time allotted to verify if SPI is enabled
  @param[in]   Frequency         Bus clock frequency (in Hz)
  @param[in]   ClockIdlePolarity Clock Idle Polarity 0 = Low, 1 = High
  @param[in]   ClockPhase        0 - Clock is inactive one cycle at the start of a frame and one cycle at the end of a frame
                                 1 - Clock is inactive for one halfcycle at the start of a frame and one cycle at the end of a frame
  @param[in]   CsOutputSelect    Select which SPI CS Signal is to be driven by the SPI Frame (0 = CS0; 1 = CS1 etc.)
  @param[in]   CsActivePolarity  Selects SPI ChipSelect signal polarity, 0 = Low, 1 = High
  @param[in]   AssertCs          FALSE - CS will remain in De-Asserted case. Used for scenario when external CS is used (ex GPIO)
                                 TRUE - CS either CS0/CS1 will be asserted once SPI transaction starts

  @retval   EFI_INVALID_PARAMETER   Invalid length parameters were passed into this function
  @retval   EFI_TIMEOUT             Timeout occurred in SPI disable/enable or transaction timeout
  @retval   EFI_DEVICE_ERROR        The verification failed
  @retval   EFI_SUCCESS             WriteRead was successful
  @retval   EFI_BAD_BUFFER_SIZE     One of the buffers is NULL
**/
EFI_STATUS
SerialIoSpiWriteRead (
  IN UINTN        PciCfgBase,
  IN UINT8        *SpiNumber  OPTIONAL,
  IN UINT32       TransactionLength,
  IN UINT8        *WriteBuffer,
  IN UINT8        *ReadBuffer,
  IN UINT64       TimeBudget,
  IN UINTN        Frequency,
  IN UINT8        ClockIdlePolarity,
  IN UINT8        ClockPhase,
  IN UINT8        CsOutputSelect,
  IN UINT8        CsActivePolarity,
  IN BOOLEAN      AssertCs
  );

#endif //_SERIAL_IO_SPI_LIB_H_
