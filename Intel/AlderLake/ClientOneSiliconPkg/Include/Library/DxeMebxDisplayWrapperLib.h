/** @file
  Wrapper functions for MEBx display protocol functions.

@copyright
  INTEL CONFIDENTIAL
  Copyright 2020 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains an 'Intel Peripheral Driver' and is uniquely identified as
  "Intel Reference Module" and is licensed for Intel CPUs and chipsets under
  the terms of your license agreement with Intel or your vendor. This file may
  be modified by the user, subject to additional terms of the license agreement.

@par Specification Reference:
**/

#ifndef _DISPLAY_WRAPPER_H_
#define _DISPLAY_WRAPPER_H_

#include <Protocol/MebxDisplayProtocol.h>

/**
  This function configures screen.

  @param[in]  ScreenType   Screen type, the value can indicate the BIOS screen or MEBx screen.

  @retval EFI_SUCCESS      Screen was initialized successfully
  @retval Others           Error has occured during screen initialization
**/
EFI_STATUS
MebxConfigureScreen (
  IN BOOLEAN        ScreenType
  );

/**
  This function prints MEBx information string to screen.

  @param[in] MsgId         Message id

  @retval EFI_SUCCESS      Screen was initialized successfully
  @retval Others           Error has occured during screen initialization
**/
EFI_STATUS
MebxDisplayText (
  IN MEBX_MSG_ID Token
  );

/**
  This function prints MEBx error string to screen and to console.

  @param[in] MsgId         Message Id which will be displayed

  @retval EFI_SUCCESS      Text was displayed successfully
  @retval EFI_NOT_FOUND    The string specified by Token isn't present
**/
EFI_STATUS
MebxDisplayError (
  IN MEBX_MSG_ID  MsgId
  );

/**
  This function prints MEBx error string with information about corrupted data entry.

  @param[in] ModuleId       Module id
  @param[in] VariableId     Variable id

  @retval EFI_SUCCESS       Text was displayed successfully
  @retval Others            Error has occurred during displaying text
**/
EFI_STATUS
MebxDisplayErrorId (
  IN UINT16 ModuleId,
  IN UINT16 VariableId
  );

/**
  This function gets user input.

  @param[out]  UserInput        User input

  @retval EFI_SUCCESS           Successfully read user input
  @retval EFI_NOT_READY         There was no input from an user.
  @retval EFI_INVALID_PARAMETER Invalid parameter
**/
EFI_STATUS
MebxReadUserInput (
  OUT MEBX_USER_INPUT   *UserInput
  );

/**
  This function clears a screen's content.

  @retval EFI_SUCCESS      The function succeeded
  @retval Others           Error has occurred
**/
EFI_STATUS
MebxClearScreen (
  VOID
  );

#endif // _DISPLAY_WRAPPER_H_
