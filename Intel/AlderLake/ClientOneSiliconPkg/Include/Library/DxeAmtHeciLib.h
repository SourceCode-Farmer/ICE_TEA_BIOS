/** @file
  Header file for AMT functionality

@copyright
  INTEL CONFIDENTIAL
  Copyright 2006 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains an 'Intel Peripheral Driver' and is uniquely identified as
  "Intel Reference Module" and is licensed for Intel CPUs and chipsets under
  the terms of your license agreement with Intel or your vendor. This file may
  be modified by the user, subject to additional terms of the license agreement.

@par Specification Reference:
**/
#ifndef _DXE_AMT_HECI_LIB_H_
#define _DXE_AMT_HECI_LIB_H_

#include <AsfMsgs.h>
#include <WifiProfileSyncAsfMsgs.h>

/**
  Stop ASF Watch Dog Timer HECI message.

**/
VOID
AsfStopWatchDog (
  VOID
  );

/**
  Start ASF Watch Dog Timer

  @param[in] WatchDogType         Which kind of WatchDog, ASF OS WatchDog Timer setting or ASF BIOS WatchDog Timer setting

**/
VOID
AsfStartWatchDog (
  IN  UINT8                       WatchDogType
  );

/**
  This message is sent to switch active LAN interface.

  @param[in] ActiveInterface      Active Lan Interface
                                    0: As defined in FIT
                                    1: Integrated LAN
                                    2: Discrete LAN

  @retval EFI_SUCCESS             Command succeeded
  @retval EFI_UNSUPPORTED         Current ME mode doesn't support this function
  @retval EFI_DEVICE_ERROR        HECI Device error, command aborts abnormally
  @retval EFI_TIMEOUT             HECI does not return the buffer before timeout
**/
EFI_STATUS
AmtSetActiveLanInterface (
  IN UINT8                        ActiveInterface
  );

/**
  This message is sent to get active LAN interface.

  @param[out] ActiveInterface     Active Lan Interface
                                    0: As defined in FIT
                                    1: Integrated LAN
                                    2: Discrete LAN

  @retval EFI_SUCCESS             Command succeeded
  @retval EFI_UNSUPPORTED         Current ME mode doesn't support this function
  @retval EFI_DEVICE_ERROR        HECI Device error, command aborts abnormally
  @retval EFI_TIMEOUT             HECI does not return the buffer before timeout
**/
EFI_STATUS
AmtGetActiveLanInterface (
  OUT UINT8                          *ActiveInterface
  );

/**
  This is used to send KVM request message to Intel ME. When
  Bootoptions indicate that a KVM session is requested then BIOS
  will send this message before any graphical display output to
  ensure that FW is ready for KVM session.

  @param[in] QueryType            0 - Query Request
                                  1 - Cancel Request
  @param[out] ResponseCode        1h - Continue, KVM session established.
                                  2h - Continue, KVM session cancelled.

  @retval EFI_UNSUPPORTED         Current ME mode doesn't support this function
  @retval EFI_SUCCESS             Command succeeded
  @retval EFI_DEVICE_ERROR        HECI Device error, command aborts abnormally
  @retval EFI_TIMEOUT             HECI does not return the buffer before timeout
  @retval EFI_BUFFER_TOO_SMALL    Message Buffer is too small for the Acknowledge
**/
EFI_STATUS
AmtQueryKvm (
  IN  UINT32                      QueryType,
  OUT UINT32                      *ResponseCode
  );

/**
  This message is sent by the BIOS in order to pass MAC Address
  which should be used for LAN in a dock.

  @param[in] Enabled              Determines if MAC Passthrough should be used
  @param[in] Mac Address          MAC Address to be used

  @retval EFI_UNSUPPORTED         Current ME mode doesn't support this function
  @retval EFI_SUCCESS             Command succeeded
  @retval EFI_DEVICE_ERROR        HECI Device error, command aborts abnormally
  @retval EFI_TIMEOUT             HECI does not return the buffer before timeout
  @retval EFI_BUFFER_TOO_SMALL    Message Buffer is too small for the Acknowledge
**/
EFI_STATUS
AmtSetMacPassthrough (
  IN UINT8 Enabled,
  IN UINT8 *MacAddress
  );

/**
  Send secure erase operation status using PET

  @param[in]    OperationResult   Status of secure erase operation

  @retval EFI_UNSUPPORTED         Current ME mode doesn't support this function
  @retval EFI_SUCCESS             Command succeeded
  @retval EFI_DEVICE_ERROR        HECI Device error, command aborts abnormally
  @retval EFI_TIMEOUT             HECI does not return the buffer before timeout
  @retval EFI_BUFFER_TOO_SMALL    Message Buffer is too small for the Acknowledge
**/
EFI_STATUS
SendRsePetAlert (
  IN EFI_STATUS                        OperationResult
  );

/**
  Send Remote Platform Erase operation status using PET

  @param[in]    RpeOperationCode    PET Alert Code for the RPE Operation.
  @param[in]    RpeDevice           Device for which the RPE OperationCode is reported.
  @param[in]    RpeOperationStatus  Reports if RPE Operation is in progress or failure.

  @retval EFI_UNSUPPORTED           Current ME mode doesn't support this function
  @retval EFI_SUCCESS               Command succeeded
  @retval EFI_DEVICE_ERROR          HECI Device error, command aborts abnormally
  @retval EFI_TIMEOUT               HECI does not return the buffer before timeout
  @retval EFI_BUFFER_TOO_SMALL      Message Buffer is too small for the Acknowledge
**/
EFI_STATUS
SendRpePetAlert (
  IN UINT8                        RpeOperationCode,
  IN UINT8                        RpeDevice,
  IN UINT8                        RpeOperationStatus
  );

/**
  This function is deprecated.
  Use ASF_GetRsePassword to get disk password from the FW

  @param[in,out]   Password            Preallocated buffer to save c string
                                       password to. It has to be at least 32
                                       characters wide.

  @retval EFI_SUCCESS                  Buffer Password contains returned password
  @retval EFI_NOT_FOUND                Either there is no password in AMT memory
                                       or Heci communication failed
  @retval EFI_DEVICE_ERROR             Failed to initialize HECI
  @retval EFI_TIMEOUT                  HECI is not ready for communication
  @retval EFI_UNSUPPORTED              Current ME mode doesn't support send this function
**/
EFI_STATUS
GetRsePassword (
  IN OUT CHAR16                       *Password
  );

/**
  Use GetRsePassword to get disk password from the FW

  @param[in,out]   Password            Preallocated buffer to save string password to.
                                       It has to be at least 32 characters wide.

  @retval EFI_SUCCESS                  Buffer Password contains returned password
  @retval EFI_NOT_FOUND                Either there is no password in AMT memory or Heci communication failed
  @retval EFI_DEVICE_ERROR             Failed to initialize HECI
  @retval EFI_TIMEOUT                  HECI is not ready for communication
  @retval EFI_UNSUPPORTED              Current ME mode doesn't support send this function
**/
EFI_STATUS
GetRsePasswordV2 (
  IN OUT CHAR8                         *Password
  );

/**
  Set UEFI Boot Options States that are available for AMT to use by sending ASF
  command through HECI

  @param[in] AmtBootControl      AMT boot control

  @retval EFI_UNSUPPORTED        Current ME mode doesn't support this function
  @retval EFI_SUCCESS            Command succeeded
  @retval EFI_INVALID_PARAMETER  Invalid Boot Options State
  @retval EFI_NOT_FOUND          HECI Communication failed
  @retval EFI_DEVICE_ERROR       ASF Response is invalid
**/
EFI_STATUS
AsfSetUefiBootOptionsState (
  IN AMT_BOOT_CONTROL      AmtBootControl
  );

/**
  Get UEFI Boot Options States that are available for AMT use by sending ASF
  proper HECI Command.

  @param[out] AmtBootControl     AMT boot control

  @retval EFI_UNSUPPORTED        Current ME mode doesn't support this function
  @retval EFI_SUCCESS            Command succeeded
  @retval EFI_INVALID_PARAMETER  Invalid Boot Options State
  @retval EFI_NOT_FOUND          HECI Communication failed
  @retval EFI_DEVICE_ERROR        ASF Response is invalid
**/
EFI_STATUS
AsfGetUefiBootOptionsState (
  OUT AMT_BOOT_CONTROL      *AmtBootControl
  );

/**
  Update UEFI Boot Options available to ASF by sending proper HECI Command

  @param[in] UefiBootOptions      UEFI Boot Options
  @param[in] NumOfUefiBootOptions Number of UEFI Boot Options

  @retval EFI_UNSUPPORTED         Current ME mode doesn't support this function
  @retval EFI_SUCCESS             Command succeeded
  @retval EFI_INVALID_PARAMETER   Invalid Boot Options State
  @retval EFI_NOT_FOUND           HECI Communication failed
  @retval EFI_OUT_OF_RESOURCES    Ran out of resources
  @retval EFI_DEVICE_ERROR        ASF Response is invalid
**/
EFI_STATUS
AsfUpdateUefiBootOptions (
  IN UEFI_BOOT_OPTION      *UefiBootOptions,
  IN UINT16                NumOfUefiBootOptions
  );

/**
  Get Boot Option with Parameters that being requested from ASF by sending proper
  HECI Command

  @param[out] UefiBootOptionWithParm    UEFI Boot Option with parameters

  @retval EFI_UNSUPPORTED               Current ME mode doesn't support this function
  @retval EFI_SUCCESS                   Command succeeded
  @retval EFI_INVALID_PARAMETER         Invalid Boot Options State
  @retval EFI_NOT_FOUND                 HECI Communication failed
  @retval EFI_DEVICE_ERROR              ASF Response is invalid
**/
EFI_STATUS
AsfGetUefiBootParameters (
  OUT UEFI_BOOT_OPTION_PARAMETER  *UefiBootOptionWithParm
  );

/**
  Get Root Certificate Authority(CA) Certificate from ASF at specified Index by sending
  proper HECI Command

  @param[in]  Index              Index
  @param[out] Cert               Root CA Certificate

  @retval EFI_UNSUPPORTED        Current ME mode doesn't support this function
  @retval EFI_SUCCESS            Command succeeded
  @retval EFI_INVALID_PARAMETER  Invalid Boot Options State
  @retval EFI_NOT_FOUND          HECI Communication failed
  @retval EFI_DEVICE_ERROR       ASF Response is invalid
**/
EFI_STATUS
AsfGetRootCaCertificate (
  IN UINT32                   Index,
  OUT ROOT_CA_CERTIFICATE     *Cert
  );

/**
  This function is deprecated.
  Send One Click Recovery status using PET

  @param[in] EventType            OCR Event Type
  @param[in] LoadOptionType       OCR Load Option Type

  @retval EFI_UNSUPPORTED         Current ME mode doesn't support this function
  @retval EFI_SUCCESS             Command succeeded
**/
EFI_STATUS
SendOcrPetAlert (
  IN UINT8              EventType,
  IN UINT8              LoadOptionType
  );

/**
  Send One Click Recovery progress or error event using PET

  @param[in] EventOffset          ASF Event Data Codes for OCR Fw Progress/Error Events
  @param[in] EventData3           OCR Additional Event Data 3
  @param[in] EventData4           OCR Additional Event Data 4

  @retval EFI_UNSUPPORTED         Current ME mode doesn't support this function
  @retval EFI_SUCCESS             Command succeeded
**/
EFI_STATUS
SendOcrPetEvent (
  IN UINT8              EventOffset,
  IN UINT8              EventData3,
  IN UINT8              EventData4
  );

/**
  Get the wireless profile name

  @param[in, out] Profile         Profile where name will be held and used to aquire profile data, cert and key

  @retval EFI_UNSUPPORTED         Current ME mode doesn't support this function
  @retval EFI_SUCCESS             Command succeeded
  @return Others                  Error happened
**/
EFI_STATUS
AsfWifiGetProfileName (
  IN OUT WIFI_PROFILE_DATA  *Profile
  );

/**
  Get the wireless profile data

  @param[in, out] Profile         Profile data defined in wifi profile protocol

  @retval EFI_UNSUPPORTED         Current ME mode doesn't support this function
  @retval EFI_SUCCESS             Command succeeded
  @return Others                  Error happened
**/
EFI_STATUS
AsfWifiGetProfileData (
  IN OUT WIFI_PROFILE_DATA  *Profile
  );

/**
  Get the wireless profile specific 8021X client certificate

  @param[in, out] Profile         Profile data defined in wifi profile protocol

  @retval EFI_UNSUPPORTED         Current ME mode doesn't support this function
  @retval EFI_SUCCESS             Command succeeded
  @return Others                  Error happened
**/
EFI_STATUS
AsfWifiGetClientCertificate (
  IN OUT WIFI_PROFILE     *Profile
  );

/**
  Get the wireless profile specific 8021X client key

  @param[in, out] Profile         Profile data defined in wifi profile protocol

  @retval EFI_UNSUPPORTED         Current ME mode doesn't support this function
  @retval EFI_SUCCESS             Command succeeded
  @return Others                  Error happened
**/
EFI_STATUS
AsfWifiGetClientKey (
  IN OUT WIFI_PROFILE     *Profile
  );

#endif
