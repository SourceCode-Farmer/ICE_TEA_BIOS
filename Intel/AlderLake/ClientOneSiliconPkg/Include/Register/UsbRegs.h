/** @file
  Register names for USB Host and device controller

  Conventions:

  - Register definition format:
    Prefix_[GenerationName]_[ComponentName]_SubsystemName_RegisterSpace_RegisterName
  - Prefix:
    Definitions beginning with "R_" are registers
    Definitions beginning with "B_" are bits within registers
    Definitions beginning with "V_" are meaningful values within the bits
    Definitions beginning with "S_" are register size
    Definitions beginning with "N_" are the bit position
  - [GenerationName]:
    Three letter acronym of the generation is used (e.g. SKL,KBL,CNL etc.).
    Register name without GenerationName applies to all generations.
  - [ComponentName]:
    This field indicates the component name that the register belongs to (e.g. PCH, SA etc.)
    Register name without ComponentName applies to all components.
    Register that is specific to -H denoted by "_PCH_H_" in component name.
    Register that is specific to -LP denoted by "_PCH_LP_" in component name.
  - SubsystemName:
    This field indicates the subsystem name of the component that the register belongs to
    (e.g. PCIE, USB, SATA, GPIO, PMC etc.).
  - RegisterSpace:
    MEM - MMIO space register of subsystem.
    IO  - IO space register of subsystem.
    PCR - Private configuration register of subsystem.
    CFG - PCI configuration space register of subsystem.
  - RegisterName:
    Full register name.

@copyright
  INTEL CONFIDENTIAL
  Copyright 1999 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains an 'Intel Peripheral Driver' and is uniquely identified as
  "Intel Reference Module" and is licensed for Intel CPUs and chipsets under
  the terms of your license agreement with Intel or your vendor. This file may
  be modified by the user, subject to additional terms of the license agreement.

@par Specification Reference:
**/
#ifndef _USB_REGS_H_
#define _USB_REGS_H_

//
// XHCI PCI Config Space registers
//
#define R_XHCI_CFG_BAR0                     PCI_BASE_ADDRESSREG_OFFSET
#define B_XHCI_CFG_ALIGN_MASK               0xFFFF

#define R_XHCI_CFG_XHCC1                    0x40
#define B_XHCI_CFG_XHCC1_ACCTRL             BIT31
#define B_XHCI_CFG_XHCC1_URD                BIT23
#define B_XHCI_CFG_XHCC1_URRE               BIT22
#define R_XHCI_CFG_XHCC2                    0x44
#define B_XHCI_CFG_XHCC2_WRREQSZCTRL        (BIT24 | BIT23 | BIT22)  ///< IDMA Write Request Size Control mask
#define N_XHCI_CFG_XHCC2_WRREQSZCTRL        22                       ///< IDMA Write Request Size Control bit position
#define B_XHCI_CFG_XHCC2_RDREQSZCTRL        (BIT2 | BIT1 | BIT0)     ///< IDMA Read Request Size Control mask
#define B_XHCI_CFG_XHCC2_OCCFDONE           BIT31
#define R_XHCI_CFG_XHCLKGTEN                0x50
#define B_XHCI_CFG_XHCLKGTEN_SSCTGE         (BIT19 | BIT18 | BIT17 | BIT16)
#define N_XHCI_CFG_XHCLKGTEN_SSCTGE         16
#define R_XHCI_CFG_PWR_CNTL_STS             0x74
#define B_XHCI_CFG_PWR_CNTL_STS_PWR_STS     (BIT1 | BIT0)
#define V_XHCI_CFG_PWR_CNTL_STS_PWR_STS_D3  (BIT1 | BIT0)
#define R_XHCI_CFG_PCE                      0xA2
#define B_XHCI_CFG_PCE_D3HE                 BIT2
#define R_XHCI_CFG_HSCFG2                   0xA4
#define R_XHCI_CFG_SSCFG1                   0xA8
#define R_XHCI_CFG_HSCFG1                   0xAC
#define R_XHCI_CFG_XHCC3                    0xFC                     ///< XHCI System Bus Configuration 3

//
// xHCI MMIO registers
//

//
// 0x00 - 0x1F - Capability Registers
//
#define R_XHCI_MEM_CAPLENGTH                0x00
#define B_XHCI_MEM_HCIVERSION               0xFFFF
#define V_XHCI_MEM_HCIVERSION_0120          0x0120
#define N_XHCI_MEM_HCIVERSION               16
#define R_XHCI_MEM_HCSPARAMS3               0x0C
#define B_XHCI_MEM_HCSPARAMS3               (BIT15 | BIT14 | BIT13 | BIT12 | BIT11 | BIT10 | BIT9 | BIT8)
#define N_XHCI_MEM_HCSPARAMS3_U2DEL         16
#define R_XHCI_MEM_HCCPARAMS2               0x1C

//
// 0x80 - 0xBF - Operational Registers
//
#define R_XHCI_MEM_PORTSC_START_OFFSET      0x480  ///< Port Status and Control Registers base offset
#define S_XHCI_MEM_PORTSC_PORT_SPACING      0x10   ///< Size of space between PortSC register for each port

#define B_XHCI_MEM_PORTSCXUSB2_PED              BIT1   ///< Port Enable/Disabled
#define B_XHCI_MEM_PORTSCXUSB2_CCS              BIT0   ///< Current Connect Status
//
// 0x2000 - 0x21FF - Runtime Registers
// 0x3000 - 0x307F - Doorbell Registers
//
#define R_XHCI_MEM_XECP_SUPP_USB2_2                     0x8008
#define R_XHCI_MEM_XECP_SUPP_USB3_2                     0x8028
#define B_XHCI_MEM_XECP_SUPP_USBX_2_CPC                 (BIT15 | BIT14 | BIT13 | BIT12 | BIT11 | BIT10 | BIT9 | BIT8)  ///< Mask for Compatible Port Count in Capability
#define N_XHCI_MEM_XECP_SUPP_USBX_2_CPC                 8       ///< Shift for Compatible Port Count
#define R_XHCI_MEM_HOST_CTRL_SCH_REG                    0x8094
#define R_XHCI_MEM_HOST_CTRL_ODMA_REG                   0x8098  ///< Host Control ODMA Register
#define R_XHCI_MEM_PMCTRL                               0x80A4
#define R_XHCI_MEM_HOST_CTRL_MISC_REG                   0x80B0  ///< Host Controller Misc Reg
#define R_XHCI_MEM_HOST_CTRL_MISC_REG_2                 0x80B4  ///< Host Controller Misc Reg 2
#define R_XHCI_MEM_SSPE                                 0x80B8  ///< Super Speed Port Enables
#define R_XHCI_MEM_AUX_CTRL_REG                         0x80C0  ///< AUX_CTRL_REG - AUX Reset Control
#define R_XHCI_MEM_DUAL_ROLE_CFG0                       0x80D8
#define R_XHCI_MEM_DUAL_ROLE_CFG1                       0x80DC
#define R_XHCI_MEM_HOST_CTRL_PORT_LINK_REG              0x80EC  ///< SuperSpeed Port Link Control
#define R_XHCI_MEM_HOST_IF_CTRL_REG                     0x8108  ///< HOST_IF_CTRL_REG - Host Controller Interface Control Register
#define R_XHCI_MEM_USB2_PROTOCOL_GAP_TIMER_HIGH_REG     0x8138  ///< USB2_PROTOCOL_GAP_TIMER_HIGH_REG - USB2 Protocol Gap Timer HIGH
#define B_XHCI_MEM_USB2_PROTOCOL_GAP_TIMER_HIGH_REG_GAP_TIME_AFTER_LS_TX  (BIT23 | BIT22 | BIT21 | BIT20 | BIT19 | BIT18 | BIT17 | BIT16)
#define N_XHCI_MEM_USB2_PROTOCOL_GAP_TIMER_HIGH_REG_GAP_TIME_AFTER_LS_TX  16
#define R_XHCI_MEM_HOST_IF_PWR_CTRL_REG0                0x8140  ///< HOST_IF_PWR_CTRL_REG0 - Power Scheduler Control 0
#define R_XHCI_MEM_HOST_IF_PWR_CTRL_REG1                0x8144  ///< HOST_IF_PWR_CTRL_REG1 - Power Scheduler Control 1
#define R_XHCI_MEM_AUX_CTRL_REG2                        0x8154  ///< AUX_CTRL_REG2 - Aux PM Control Register 2
#define R_XHCI_MEM_USBLPM                               0x8170  ///< USB LPM Parameters
#define R_XHCI_MEM_XLTP_LTV1                            0x8174  ///< xHC Latency Tolerance Parameters - LTV Control
#define B_XHCI_MEM_XLTP_LTV1_USB2_PL0_LTV               0xFFF   ///< USB2 Port L0 LTV
#define R_XHCI_MEM_XHCLTVCTL2                           0x8178  ///< xHC Latency Tolerance Control 2 - LTV Control 2
#define B_XHCI_MEM_XHCLTVCTL2_LTV_LIMIT                 0x1FFF  ///< LTV Limit field bits
#define R_XHCI_MEM_LTVHIT                               0x817C  ///< xHC Latency Tolerance Parameters - High Idle Time Control
#define R_XHCI_MEM_LTVMIT                               0x8180  ///< xHC Latency Tolerance Parameters - Medium Idle Time Control
#define R_XHCI_MEM_LTVLIT                               0x8184  ///< xHC Latency Tolerance Parameters - Low Idle Time Control
#define R_XHCI_MEM_CFG_USB2_LTV_U2_NOREQ_REG            0x8188  ///< ADO USB2 LTR Register
#define R_XHCI_MEM_D0I2CTRL                             0x81BC  ///< D0I2 Control Register
#define N_XHCI_MEM_D0I2CTRL_MSID0I2PWT                  16      ///< Bitshift for MSI D0i2 Pre Wake Time
#define R_XHCI_MEM_D0I2SCH_ALARM_CTRL                   0x81C0  ///< D0i2 Scheduler Alarm Control Register
#define B_XHCI_MEM_D0I2SCH_ALARM_CTRL                   0x1FFF1FFF  ///< Bitmask for D0i2 Scheduler Alarm Control Register
#define N_XHCI_MEM_D0I2SCH_ALARM_CTRL_D0I2IT            16      ///< Bitshift for D0i2 Idle Time
#define R_XHCI_MEM_USB2PMCTRL                           0x81C4  ///< USB2 Power Management Control
#define R_XHCI_MEM_AUX_CTRL_REG3                        0x81C8  ///< Aux PM Control 3 Register
#define B_XHCI_MEM_AUX_CTRL_REG3_HYSTERESIS_TIMEOUT     (BIT9 | BIT8 | BIT7)  ///< Inaccessible Hysteresis Timer
#define B_XHCI_MEM_AUX_CTRL_REG3_HYST_TIMEOUT_150US     BIT8    ///< Force PG timeout 150us

#define R_XHCI_MEM_DBGDEV_CTRL_REG1                     0x8754    ///< Debug Device Control Register 1

#define R_XHCI_MEM_PMCTRL2                                    0x8468    ///< PMCTRL2 - Power Management Control 2
#define R_XHCI_MEM_HOST_CTRL_SUS_LINK_PORT_REG                0x81F8
#define R_XHCI_MEM_HOST_CTRL_EARLY_DBG_REG                    0x81FC
#define R_XHCI_MEM_MULT_IN_SCH_POLICY                         0x82A0    ///< Multiple_in Scheduler Policy Register
#define R_XHCI_MEM_XHCI_ECN_REG                               0x82FC
#define R_XHCI_MEM_GRP0_SAI_WAC_POLICY_LO                     0x8308    ///< Group0 SAI Write Access Control Policy Register Low
#define R_XHCI_MEM_GRP0_SAI_RAC_POLICY_LO                     0x8310    ///< Group0 SAI Read Access Control Policy Low
#define R_XHCI_MEM_PMREQ_CTRL_REG                             0x83D0    ///< PMREQ Control Register
#define R_XHCI_MEM_SURVIVABILITY_REG1                         0x83E8    ///< U2U Bridge Survivability Register 1
#define R_XHCI_MEM_SURVIVABILITY_REG2                         0x83EC    ///< U2U Bridge Survivability Register 2
#define B_XHCI_MEM_SURVIVABILITY_REG2_DRIVE_DEV_BYP_CLK_READY BIT14
#define R_XHCI_MEM_HOST_CTRL_SSP_LINK_REG2                    0x8E68
#define R_XHCI_MEM_HOST_CTRL_SSP_LFPS_REG4                    0x8E7C
#define B_XHCI_MEM_HOST_CTRL_SSP_LFPS_REG4_SCD_LFPS_TIMEOUT   (BIT23 | BIT22 | BIT21 | BIT20 | BIT19 | BIT18 | BIT17)
#define N_XHCI_MEM_HOST_CTRL_SSP_LFPS_REG4_SCD_LFPS_TIMEOUT   17
#define B_XHCI_MEM_HOST_CTRL_SSP_LFPS_REG4_SCD_TX_COUNT       (BIT16 | BIT15 | BIT14)
#define N_XHCI_MEM_HOST_CTRL_SSP_LFPS_REG4_SCD_TX_COUNT       14
#define R_XHCI_MEM_HOST_CTRL_SSP_CONFIG_REG2                  0x8E9C
#define R_XHCI_MEM_HOST_CTRL_USB3_RECAL                       0x8E84
#define R_XHCI_MEM_HOST_CTRL_SSP_TUNNELING_REG                0x8EA0    ///< HOST_CTRL_SSP_TUNNELING_REG

#define R_XHCI_MEM_HOST_CTRL_USB3_CP13_DEEMPH                 0x8E8C
#define R_XHCI_MEM_HOST_CTRL_USB3_CP14_DEEMPH                 0x8E90
#define R_XHCI_MEM_HOST_CTRL_USB3_CP15_DEEMPH                 0x8E94
#define R_XHCI_MEM_HOST_CTRL_USB3_CP16_DEEMPH                 0x8E98

//
// TCSS CPU XHCI DAP ESS PORT Registers in MMIO Space
//
#define R_CPU_XHCI_MEM_DAP_ESS_PORT_CONTROL1                  0x8AC8 ///< DAP eSS Port <N> Control1 Register for TCSS CPU XHCI PORT.
#define S_CPU_XHCI_MEM_ESS_PORT_CONTROL1_SPACING              0x10   ///< Size of space between DAP eSS Port <N> Control1 Register for each port. Max 4 CPU XHCI Port
#define B_CPU_XHCI_MEM_DAP_ESS_PORT_CONTROL1_RTCO             BIT3   ///< DAP eSS Port <N> Control1 Register for TCSS CPU XHCI PORT.

//
// Extended Capability Registers
//
#define R_PCH_XHCI_MEM_USB2PDO                  0x84F8    ///< USB2 Port Disable Override register
#define R_PCH_XHCI_MEM_USB3PDO                  0x84FC    ///< USB3 Port Disable Override register

//
// USB Audio Offload registers
//
#define R_XHCI_MEM_AUDIO_OFFLOAD_CTR            0x91F4    ///< Audio Offload Control

//
// USB2 Port Reset Messaging Enable
//
#define R_XHCI_MEM_U2PRM_U2PRDE                 0x92F4    ///< Control bits to enable USB2 Port Reset Messaging

//
// Debug Capability Descriptor Parameters
//
#define R_XHCI_MEM_DBC_DBCCTL                   0x8760    ///< DBCCTL - DbC Control
#define N_XHCI_MEM_DBC_DBCCTL_DISC_RXD_CNT      2         ///< Soft Disconnect RX Detect Count bitshift

#define R_XHCI_MEM_SOCHWSTSAVE1                 0x8504    ///< SOC HW State Save 1 Register
#define B_XHCI_MEM_SOCHWSTSAVE1_CMD_SSV         BIT31     ///< cmd_ssv flag which indicates that context is saved or not

//
// Over Current Mapping registers
//
#define R_XHCI_MEM_U2OCM                        0x90A4    ///< XHCI USB2 Overcurrent Pin N Mapping
#define R_XHCI_MEM_U3OCM                        0x9124    ///< XHCI USB3 Overcurrent Pin N Mapping

#define R_XHCI_MEM_HOST_CTRL_USB3_PORT_CONFIG2                  0x9470
#define B_XHCI_MEM_HOST_CTRL_USB3_PORT_CONFIG2_USB3_PROG_DONE   BIT0

//
// xHCI Private registers
//
#define R_XHCI_PCR_DAP_COMMON_CONTROL_REG         0x440   ///< DAP Common Control register
#define R_XHCI_PCR_DAP_USB2_PORT_CONTROL_0_REG_0  0x500   ///< DAP USB2 Port0 Control 0 Register
#define B_XHCI_PCR_DAP_USB2_PORT_CONTROL_0_REG_0_CONNECTOR_EVENT             (BIT8 | BIT7 | BIT6 | BIT5)   ///< Connector Event (CE) in DAP USB2 Port<N> Control 0 Register
#define V_XHCI_PCR_DAP_USB2_PORT_CONTROL_0_REG_0_CONNECTOR_EVENT_HOST        0        ///< Host Operation State
#define V_XHCI_PCR_DAP_USB2_PORT_CONTROL_0_REG_0_CONNECTOR_EVENT_DISCONNECT  0x20     ///< Un-subscription Operation State
#define V_XHCI_PCR_DAP_USB2_PORT_CONTROL_0_REG_0_CONNECTOR_EVENT_DBC         0x180    ///< DBC Operation State
#define R_XHCI_PCR_DAP_USB2PORT_STATUS_0          0x508   ///< DAP USB2 Port0 Status 0 Register
#define B_XHCI_PCR_DAP_USB2PORT_STATUS_0_OS       (BIT7 | BIT6 | BIT5 | BIT4 | BIT3 | BIT2 | BIT1 | BIT0)    ///< Operation State (OS) in DAP USB2 Port<N> Status 0 Register
#define V_XHCI_PCR_DAP_USB2PORT_STATUS_0_OS_DBC   0x40    ///< DBC Operation State
#define R_XHCI_PCR_DAP_ESS_PORT_CONTROL1          0x604   ///< DAP eSS Port <N> Control 1 Register
#define S_XHCI_PCR_DAP_ESS_PORT_CONTROL1_SPACING  0x10    ///< Size of space between DAP eSS Port <N> Control 1 Register for each port

//
// xDCI (OTG) PCI Config Space Registers
//
#define R_XDCI_CFG_PMCSR                      0x84      ///< Power Management Control and Status Register
#define R_XDCI_CFG_CPGE                       0xA2      ///< Chassis Power Gate Enable

//
// xDCI (OTG) MMIO registers
//
#define R_XDCI_MEM_GCTL                         0xC110  ///< Xdci Global Ctrl
#define B_XDCI_MEM_GCTL_GHIBEREN                BIT1    ///< Hibernation enable
#define R_XDCI_MEM_GUSB2PHYCFG                  0xC200  ///< Global USB2 PHY Configuration Register
#define B_XDCI_MEM_GUSB2PHYCFG_SUSPHY           BIT6    ///< Suspend USB2.0 HS/FS/LS PHY
#define R_XDCI_MEM_GUSB3PIPECTL0                0xC2C0  ///< Global USB3 PIPE Control Register 0
#define B_XDCI_MEM_GUSB3PIPECTL0_SUSPEN_EN      BIT17   ///< Suspend USB3.0 SS PHY (Suspend_en)
#define R_XDCI_MEM_APBFC_U3PMU_CFG2             0x10F810
#define R_XDCI_MEM_APBFC_U3PMU_CFG4             0x10F818
#define R_XDCI_MEM_APBFC_U3PMU_CFG5             0x10F81C
#define R_XDCI_MEM_APBFC_U3PMU_CFG6             0x10F820
#define B_XDCI_MEM_APBFC_U3PMU_CFG6_RXSBCTRLEN  BIT13
#define B_XDCI_MEM_APBFC_U3PMU_CFG6_ENLRIP2     BIT4

//
// xDCI (OTG) Private Configuration Registers
// (PID:OTG)
// @todo: Verify PCR vaLidity for CPU
//
#define R_OTG_PCR_IOSF_PMCTL              0x1D0
#define R_OTG_PCR_PCICFGCTRL1             0x200
#define B_OTG_PCR_PCICFGCTRL_PCI_IRQ      (BIT27 | BIT26 | BIT25 | BIT24 | BIT23 | BIT22 | BIT21 | BIT20)
#define N_OTG_PCR_PCICFGCTRL_PCI_IRQ      20
#define B_OTG_PCR_PCICFGCTRL_ACPI_IRQ     (BIT19 | BIT18 | BIT17 | BIT16 | BIT15 | BIT14 | BIT13 | BIT12)
#define N_OTG_PCR_PCICFGCTRL_ACPI_IRQ     12
#define B_OTG_PCR_PCICFGCTRL_INT_PIN      (BIT11 | BIT10 | BIT9 | BIT8)
#define N_OTG_PCR_PCICFGCTRL_INT_PIN      8
#define B_OTG_PCR_PCICFGCTRL_ACPI_INT_EN  BIT1

//
// USB2 Private Configuration Registers
// USB2 HIP design featured
// (PID:USB2)
// Doesn't apply to CPU (only USB3 functionality present)
//
#define R_USB2_PCR_GLOBAL_PORT                0x4001                    ///< USB2 GLOBAL PORT
#define R_USB2_PCR_PP_LANE_BASE_ADDR          0x4000                    ///< PP LANE base address
#define R_USB2_PCR_PER_PORT                   0x00                      ///< USB2 PER PORT          Addr[7:2] = 0x00
#define B_USB2_PCR_PER_PORT_PERPORTRXISET     (BIT19 | BIT18 | BIT17)
#define N_USB2_PCR_PER_PORT_PERPORTRXISET     17
#define B_USB2_PCR_PER_PORT_PERPORTTXPEHALF   BIT14
#define N_USB2_PCR_PER_PORT_PERPORTTXPEHALF   14
#define B_USB2_PCR_PER_PORT_PERPORTPETXISET   (BIT13 | BIT12 | BIT11)
#define N_USB2_PCR_PER_PORT_PERPORTPETXISET   11
#define B_USB2_PCR_PER_PORT_PERPORTTXISET     (BIT10 | BIT9 | BIT8)
#define N_USB2_PCR_PER_PORT_PERPORTTXISET     8
#define R_USB2_PCR_UTMI_MISC_PER_PORT         0x08                      ///< UTMI MISC REG PER PORT Addr[7:2] = 0x08
#define R_USB2_PCR_PER_PORT_2                 0x26                      ///< USB2 PER PORT 2        Addr[7:2] = 0x26
#define B_USB2_PCR_PER_PORT_2_TXEMPHASISEN    (BIT24 | BIT23)           ///< HSNPREDRVSEL bits value USB2 PER PORT2 register
#define N_USB2_PCR_PER_PORT_2_TXEMPHASISEN    23
#define R_USB2_PCR_GLB_ADP_VBUS_REG           0x402B                    ///< GLB ADP VBUS REG
#define R_USB2_PCR_GLOBAL_PORT_2              0x402C                    ///< USB2 GLOBAL PORT 2
#define R_USB2_PCR_PLLDIVRATIOS_0             0x7000                    ///< PLLDIVRATIOS_0
#define B_USB2_PCR_PLLDIVRATIOS_0_FBDIVRATIO  (BIT31 | BIT30 | BIT29 | BIT28 | BIT27 | BIT26 | BIT25 | BIT24)
#define N_USB2_PCR_PLLDIVRATIOS_0_FBDIVRATIO  24
#define R_USB2_PCR_CONFIG_0                   0x7008                    ///< CONFIG_0
#define B_USB2_PCR_CONFIG_0_INT_COEFF         (BIT17 | BIT16 | BIT15 | BIT14 | BIT13)
#define N_USB2_PCR_CONFIG_0_INT_COEFF         13
#define B_USB2_PCR_CONFIG_0_PROP_COEFF        (BIT12 | BIT11 | BIT10 | BIT9)
#define N_USB2_PCR_CONFIG_0_PROP_COEFF        9
#define R_USB2_PCR_CONFIG_3                   0x7014                    ///< CONFIG_3
#define B_USB2_PCR_CONFIG_3_SFRCALIB_FMINCNT  (BIT31 | BIT30 | BIT29 | BIT28 | BIT27 | BIT26 | BIT25 | BIT24)
#define N_USB2_PCR_CONFIG_3_SFRCALIB_FMINCNT  24
#define B_USB2_PCR_CONFIG_3_SFRCALIB_FMAXCNT  (BIT23 | BIT22 | BIT21 | BIT20 | BIT19 | BIT18 | BIT17 | BIT16)
#define N_USB2_PCR_CONFIG_3_SFRCALIB_FMAXCNT  16
#define B_USB2_PCR_CONFIG_3_TDCCALIB_OSCCNT   (BIT15 | BIT14 | BIT13 | BIT12 | BIT11 | BIT10 | BIT9 | BIT8)
#define N_USB2_PCR_CONFIG_3_TDCCALIB_OSCCNT   8
#define B_USB2_PCR_CONFIG_3_LOCKTIMERCNT_TH   (BIT7 | BIT6 | BIT5 | BIT4 | BIT3 | BIT2 | BIT1)
#define N_USB2_PCR_CONFIG_3_LOCKTIMERCNT_TH   1
#define R_USB2_PCR_DFT_1                      0x7024                    ///< DFT_1
#define B_USB2_PCR_DFT_1_KPSCALE              (BIT16 | BIT15)
#define N_USB2_PCR_DFT_1_KPSCALE              15
#define B_USB2_PCR_DFT_1_KISCALE              (BIT14 | BIT13)
#define N_USB2_PCR_DFT_1_KISCALE              13
#define R_USB2_PCR_SFRCONFIG_0                0x702C                    ///< SFRCONFIG_0
#define B_USB2_PCR_SFRCONFIG_0_SFRTIMER_COEF  (BIT7 | BIT6 | BIT5 | BIT4)
#define N_USB2_PCR_SFRCONFIG_0_SFRTIMER_COEF  4
#define R_USB2_PCR_PLL1                       0x7F02                    ///< USB2 PLL1
#define R_USB2_PCR_CFG_COMPBG                 0x7F04                    ///< USB2 COMPBG

//
// External USB2 PHY private registers
//
#define R_USB2_PCR_SNPSPHYSIPPOL                0x00        ///< SNPS PHY SIP POLICIES
#define R_USB2_PCR_SNPSPHYSIPPOL_ENCMNONNOVRL1  BIT12       ///< Enable COMMONONN Override In L1
#define R_USB2_PCR_SNPSPHYSIPPOL_CLKCORETCGL1   BIT2        ///< CLKCORE (refclk) Trunk Gating Enable on SLEEP/L1

//
// xHCI SSIC registers
//
#define R_XHCI_MEM_SSIC_CONF_REG2_PORT_1          0x880C    ///< SSIC Configuration Register 2 Port 1
#define R_XHCI_MEM_SSIC_CONF_REG2_PORT_2          0x883C    ///< SSIC Configuration Register 2 Port 2
#define B_XHCI_MEM_SSIC_CONF_REG2_PORT_UNUSED     BIT31
#define B_XHCI_MEM_SSIC_CONF_REG2_PROG_DONE       BIT30

//
// xHCI DCI MMIO Registers
//
#define R_XHCI_PCR_DCI_DBC_TRACE_IN_PAYLOAD_BP_LOW         0x50
#define R_XHCI_PCR_DCI_DBC_TRACE_IN_PAYLOAD_BP_HIGH        0x54
#define R_XHCI_PCR_DCI_DBC_TRACE_IN_PAYLOAD_QUALIFIERS     0x58
#define R_XHCI_PCR_DCI_DBC_TRACE_IN_STATUS_BP_LOW          0x60
#define R_XHCI_PCR_DCI_DBC_TRACE_IN_STATUS_BP_HIGH         0x64
#define R_XHCI_PCR_DCI_DBC_TRACE_IN_STATUS_QUALIFIERS      0x68
#define V_XHCI_PCR_DCI_DBC_TRACE_QUALIFIERS                0x22B800

#endif // _USB_REGS_H_
