/** @file
  Functional Safety policy.

@copyright
  INTEL CONFIDENTIAL
  Copyright 2019-2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains an 'Intel Peripheral Driver' and is uniquely identified as
  "Intel Reference Module" and is licensed for Intel CPUs and chipsets under
  the terms of your license agreement with Intel or your vendor. This file may
  be modified by the user, subject to additional terms of the license agreement.

@par Specification Reference:
**/

#ifndef _FUSA_CONFIG_H_
#define _FUSA_CONFIG_H_

/**
 Making any setup structure change after code frozen
 will need to maintain backward compatibility, bump up
 structure revision and update below history table\n
  <b>Revision 1</b>:  - Initial version.
  <b>Revision 3</b>:  - Add FusaConfigEnable switch, BIST scan and locksteps for module.
**/

#define FUSA_CONFIG_REVISION 3

extern EFI_GUID gFusaConfigGuid;

#pragma pack (push,1)

/**
  The FUSA_CONFIG block describes the expected configuration of Functional Safety
**/
typedef struct {
  CONFIG_BLOCK_HEADER  Header;           ///< Config Block Header
  /**
    Enable/Disable Fusa feature on Display
    Fusa configuration on Display <b>0: Disable</b>; 1: Enable.
    Enable/Disable Fusa feature on IOP MCA Check
    Fusa configuration on IOP MCA Check <b>0: Disable</b>; 1: Enable.
  **/
  UINT32    DisplayFusaConfigEnable                :  1;    ///< Enable/Disable FuSa feature on DE: 0=Disable, 1=Enable
  UINT32    GraphicFusaConfigEnable                :  1;    ///< Enable/Disable FuSa feature on GT: 0=Disable, 1=Enable
  UINT32    OpioFusaConfigEnable                   :  1;    ///< Enable/Disable FuSa feature on OPIO: 0=Disable, 1=Enable
  UINT32    PsfFusaConfigEnable                    :  1;    ///< Enable/Disable FuSa feature on PSF: 0=Disable, 1=Enable
  UINT32    IopFusaConfigEnable                    :  1;    ///< Enable/Disable FuSa feature on IOP: 0=Disable, 1=Enable
  UINT32    IopFusaMcaCheckEnable                  :  1;    ///< Enable/Disable FuSa feature on IOP MCA Check Enable: 0=Disable, 1=Enable
  UINT32    FusaConfigEnable                       :  1;    ///< Enable/Disable FuSa feature: 0=Disable, 1=Enable
  UINT32    FusaRunStartupArrayBist                :  1;    ///< Enabling this will execute startup array test during boot
  UINT32    FusaRunStartupScanBist                 :  1;    ///< Enabling this will execute startup scan test during boot
  UINT32    FusaRunPeriodicScanBist                :  1;    ///< Enabling this will execute periodic scan test during boot
  UINT32    Module0Lockstep                        :  2;    ///< Enable/Disable Lockstep for Atom module 1, which has 4 cores;
                                                            ///< 0: Disable lockstep; 1: Enable lockstep for Core 0 with Core 1, Core 2 with Core 3;
                                                            ///< 2: Enable lockstep for Core 0 with Core 1; 3: Enable lockstep for Core 2 with Core 3
  UINT32    Module1Lockstep                        :  2;    ///< Enable/Disable Lockstep for Atom module 1, which has 4 cores;
                                                            ///< 0: Disable lockstep; 1: Enable lockstep for Core 0 with Core 1, Core 2 with Core 3;
                                                            ///< 2: Enable lockstep for Core 0 with Core 1; 3: Enable lockstep for Core 2 with Core 3
  UINT32    RsvdBits0                              :  18;   ///< Reserved bits
} FUSA_CONFIG;

#pragma pack (pop)

/**

**/
#define MSR_FUSA_CONFIG                     0x000002D1
#define MSR_TRIGGER_STARTUP_MEM_BIST        0x000002D4
#define MSR_STARTUP_MEM_BIST_STATUS         0x000002D5
#define MSR_FUSA_CAPABILITIES_A             0x000002D9

#define B_MSR_FUSA_CAP_STARTUP_SCAN_DIAGNOGSIS_MASK     BIT0
#define B_MSR_FUSA_CAP_STARTUP_ARRAY_DIAGNOGSIS_MASK    BIT1
#define B_MSR_FUSA_CAP_PERIODIC_ARRAY_DIAGNOGSIS_MASK   BIT2
#define B_MSR_FUSA_CAP_LOCKSTEP_MODE_MASK               BIT3
#define B_MSR_FUSA_CAP_PERIODIC_SCAN_DIAGNOGSIS_MASK    BIT4

#define MAX_AP_COUNT   24

typedef union {
  BOOLEAN   GlobalApEnable;
  UINT8     CurrProcessor;
  BOOLEAN   LockStepEn[MAX_AP_COUNT];
} LOCKSTEP_CONFIG;

/**
  LOCKSTEP ENABLE
**/
#define MSR_ENABLE_LSM          0x000002D8

typedef enum {
  LockstepDisabled    = 0x0,
  LockstepC0C1_C2C3   = 0x1,
  LockstepC0C1        = 0x2,
  LockstepC2C3        = 0x3
} LOCKSTEP_MODE;

#define GRT_MOD8_LPID0_APIC_ID      0x40
#define GRT_MOD8_LPID1_APIC_ID      0x42
#define GRT_MOD8_LPID2_APIC_ID      0x44
#define GRT_MOD8_LPID3_APIC_ID      0x46
#define GRT_MOD9_LPID0_APIC_ID      0x48
#define GRT_MOD9_LPID1_APIC_ID      0x4A
#define GRT_MOD9_LPID2_APIC_ID      0x4C
#define GRT_MOD9_LPID3_APIC_ID      0x4E

#endif // _FUSA_CONFIG_H_
