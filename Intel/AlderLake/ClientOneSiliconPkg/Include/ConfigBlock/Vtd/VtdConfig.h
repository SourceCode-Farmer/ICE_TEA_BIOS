/** @file
  VT-d policy definitions.

@copyright
  INTEL CONFIDENTIAL
  Copyright 2014 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains an 'Intel Peripheral Driver' and is uniquely identified as
  "Intel Reference Module" and is licensed for Intel CPUs and chipsets under
  the terms of your license agreement with Intel or your vendor. This file may
  be modified by the user, subject to additional terms of the license agreement.

@par Specification Reference:
**/
#ifndef _VTD_CONFIG_H_
#define _VTD_CONFIG_H_

#include <Library/VtdInfoLib.h>
#pragma pack(push, 1)

#define VTD_CONFIG_REVISION 2
#define VTD_DXE_CONFIG_REVISION 3

//
// Forward declaration for the VTd Enable DMA Buffer PPI.
//

/**
  Enable DMA buffer in VTd

  @param[in] VtdEngineIndex         VTd engine index.

  @retval EFI_SUCCESS               Enable successfully.
  @retval EFI_INVALID_PARAMETER     Input parameters are invalid.
  @retval EFI_UNSUPPORTED           VTd base is zero.
  @retval EFI_OUT_OF_RESOURCES      There is no additional space in the PPI database.
**/
typedef
EFI_STATUS
(EFIAPI *VTD_ENABLE_DMA_BUFFER) (
  IN VTD_ENGINE_INDEX VtdEngineIndex
  );

/**
  The data elements should be initialized by a Platform Module.
  The data structure is for VT-d driver initialization\n
  <b>Revision 1</b>:
  - Initial version.
  <b>Revision 2</b>:
  - Added field EnableDmaBuffer.
**/
typedef struct {
  CONFIG_BLOCK_HEADER   Header;                      ///< Offset 0-27 Config Block Header
  /**
    Offset 28:
    VT-D Support can be verified by reading CAP ID register as expalined in BIOS Spec.
    This policy is for debug purpose only.
    If VT-D is not supported, all other policies in this config block will be ignored.
    <b>0 = To use Vt-d</b>;
    1 = Avoids programming Vtd bars, Vtd overrides and DMAR table.
  **/
  UINT8                 VtdDisable;
  UINT8                 X2ApicOptOut;        ///< Offset 29: This field is used to enable the X2APIC_OPT_OUT bit in the DMAR table. 1=Enable/Set and <b>0=Disable/Clear</b>
  UINT8                 DmaControlGuarantee; ///< Offset 30: This field is used to enable the DMA_CONTROL_GUARANTEE bit in the DMAR table. 1=Enable/Set and <b>0=Disable/Clear</b>
  UINT8                 VtdIgdEnable;        ///< Offset 31: This field is used to enable the VtdIgdEnable Policy. 1=Enable/Set and <b>0=Disable/Clear</b>
  UINT8                 VtdIpuEnable;        ///< Offset 32: This field is used to enable the VtdIpuEnable Policy. 1=Enable/Set and <b>0=Disable/Clear</b>
  UINT8                 VtdIopEnable;        ///< Offset 33: This field is used to enable the VtdIopEnable Policy. 1=Enable/Set and <b>0=Disable/Clear</b>
  UINT8                 VtdItbtEnable;       /// Deprecate
  UINT8                 PreBootDmaMask;      ///< Offset 35: Convey PcdVTdPolicyPropertyMask value from EDK2 IntelSiliconPkg
  UINT32                BaseAddress[VTD_ENGINE_NUMBER];  ///< Offset 36: This field is used to describe the base addresses for VT-d function
  UINT32                DmaBufferSize;       ///< Offset 48: Protect Memory Region (PMR) DMA buffer size
  UINT32                Reserved;            ///< Offset 52: Reserved
  EFI_PHYSICAL_ADDRESS  EnableDmaBuffer;     ///< Offset 56: Pointer to EnableDmaBuffer function. Platform code provides an interface to enable DMA buffer.
                                             ///<            VTD_ENABLE_DMA_BUFFER is a prototype for that interface.
} VTD_CONFIG;

/**
  The data structure is for VT-d driver initialization in DXE\n
  <b>Revision 1</b>:
  - Initial version.
  <b>Revision 2</b>:
  - Added TE Disable for Igd.
  <b>Revision 3</b>:
  - Removed TE Disable for Igd.
**/
typedef struct {
  CONFIG_BLOCK_HEADER   Header;                    ///< Offset 0-27 Config Block Header
  UINT8                 Rsvd[4];
} VTD_DXE_CONFIG;
#pragma pack(pop)

#endif   //  _VTD_CONFIG_H_
