/** @file
  CPU Power Management VR Config Block.

@copyright
  INTEL CONFIDENTIAL
  Copyright 2015 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains an 'Intel Peripheral Driver' and is uniquely identified as
  "Intel Reference Module" and is licensed for Intel CPUs and chipsets under
  the terms of your license agreement with Intel or your vendor. This file may
  be modified by the user, subject to additional terms of the license agreement.

@par Specification Reference:
**/
#ifndef _CPU_POWER_MGMT_VR_CONFIG_H_
#define _CPU_POWER_MGMT_VR_CONFIG_H_

#define CPU_POWER_MGMT_VR_CONFIG_REVISION 16

extern EFI_GUID gCpuPowerMgmtVrConfigGuid;

#pragma pack (push,1)

///
/// Defines the maximum number of VR domains supported.
/// @warning: Changing this define would cause DWORD alignment issues in policy structures.
///
#define MAX_NUM_VRS         5

/**
  CPU Power Management VR Configuration Structure.

  <b>Revision 1</b>:
  - Initial version.
  <b>Revision 2</b>:
  - Updated Acoustic Noise Mitigation.
  <b>Revision 3</b>:
  - Deprecate PsysOffset and added PsysOffset1 for Psys Offset Correction
  <b>Revision 4</b>:
  - Deprecate TdcTimeWindow and added TdcTimeWindow1 for TDC Time
    Added Irms support.
  <b>Revision 5</b>:
  - Add RfiMitigation.
  <b>Revision 6</b>:
  - Add Dynamic Periodicity Alteration (DPA) tuning feature
  <b>Revision 7</b>:
  - Add VccIn Aux Imon feature
  <b>Revision 8</b>:
  - Deprecate SendVrMbxCmd
  <b>Revision 9</b>:
  - Added an option to Enable/Disable FIVR Spread Spectrum
  <b>Revision 10</b>:
  - Add DlvrRfiEnable, DlvrSpreadSpectrumPercentage and DlvrRfiFrequency for RFI control.
  <b>Revision 11</b>:
  - Added an option to Enable/Disable PS1 to PS0/PS2 to PS1 dynamic cutoff
  <b>Revision 12</b>:
  - Added an option for Vr Power Delivery design
  <b>Revision 13</b>:
  - Add VccInDemotionEnable, VccInDemotionQuiescentPowerInMw and VccInDemotionCapacitanceInUf
  <b>Revision 14</b>:
  - Add EnableVsysCritical, VsysFullScale,VsysCriticalThreshold, VsysAssertionDeglitchMantissa
  - VsysAssertionDeglitchExponent, VsysDeassertionDeglitchMantissa, VsysDeassertionDeglitchExponent
  <b>Revision 15</b>:
  - Add Voltage Regulator Fast Vmode ICC Limit
  <b>Revision 16</b>:
  - Modified IccMax range up to 512A
**/
typedef struct {
  CONFIG_BLOCK_HEADER   Header;                   ///< Config Block Header
  UINT32 AcousticNoiseMitigation        : 1;      ///< Enable or Disable Acoustic Noise Mitigation feature. <b>0: Disabled</b>; 1: Enabled

  UINT32 SendVrMbxCmd                   : 2;      ///< Deprecated
  UINT32 EnableMinVoltageOverride       : 1;      ///< Enable or disable Minimum Voltage override for minimum voltage runtime and minimum voltage C8. <b>0: Disabled</b> 1: Enabled.
  UINT32 RfiMitigation                  : 1;      ///< Enable or Disable RFI Mitigation. <b>0: Disable - DCM is the IO_N default</b>; 1: Enable - Enable IO_N DCM/CCM switching as RFI mitigation.
  UINT32 RsvdBits                       : 27;     ///< Reserved for future use.
  UINT8  PsysSlope;                               ///< PCODE MMIO Mailbox: Platform Psys slope correction. <b>0: Auto</b> Specified in 1/100 increment values. Range is 0-200. 125 = 1.25.
  UINT8  PsysOffset;                              ///< PCODE MMIO Mailbox: Platform Psys offset correction. <b>0: Auto</b> Units 1/4, Range 0-255. Value of 100 = 100/4 = 25 offset. Deprecated
  /**
   PCODE MMIO Mailbox: FIVR RFI Spread Spectrum Range.
   <b>1.5%</b>
   Range: 0.5%, 1%, 1.5%, 2%, 3%, 4%, 5%, 6%.
   Each Range is translated to an encoded value for FIVR register.
   0.5% = 0, 1% = 3, 1.5% = 8, 2% = 18, 3% = 28, 4% = 34, 5% = 39, 6% = 44.
  **/
  UINT8  FivrSpreadSpectrum;
  UINT8  RsvdBytes0;
  /**
   PCODE MMIO Mailbox: Set the desired RFI frequency, in increments of 100KHz.
   <b>0: Auto</b>
   Range varies based on XTAL clock:
    - 0-1918 (Up to 191.8HMz) for 24MHz clock.
    - 0-1535 (Up to 153.5MHz) for 19MHz clock.
  **/
  UINT16 FivrRfiFrequency;
  UINT8  RsvdBytes1[2];
  /** @name VR Settings
  The VR related settings are sorted in an array where each index maps to the VR domain as defined below:
   - 0 = System Agent VR
   - 1 = IA Core VR
   - 2 = Ring Vr
   - 3 = GT VR
   - 4 = FIVR VR

  The VR settings for a given domain must be populated in the appropriate index.
  **/
  ///@{
  UINT16 TdcCurrentLimit[MAX_NUM_VRS];            ///< PCODE MMIO Mailbox: Thermal Design Current current limit. Specified in 1/8A units. Range is 0-4095. 1000 = 125A. <b>0: 0 Amps</b>
  UINT16 AcLoadline[MAX_NUM_VRS];                 ///< PCODE MMIO Mailbox: AcLoadline in 1/100 mOhms (ie. 1250 = 12.50 mOhm); Range is 0-6249. <b>Intel Recommended Defaults vary by domain and SKU.</b>
  UINT16 DcLoadline[MAX_NUM_VRS];                 ///< PCODE MMIO Mailbox: DcLoadline in 1/100 mOhms (ie. 1250 = 12.50 mOhm); Range is 0-6249.<b>Intel Recommended Defaults vary by domain and SKU.</b>
  UINT16 Psi1Threshold[MAX_NUM_VRS];              ///< PCODE MMIO Mailbox: Power State 1 current cuttof in 1/4 Amp increments. Range is 0-128A.
  UINT16 Psi2Threshold[MAX_NUM_VRS];              ///< PCODE MMIO Mailbox: Power State 2 current cuttof in 1/4 Amp increments. Range is 0-128A.
  UINT16 Psi3Threshold[MAX_NUM_VRS];              ///< PCODE MMIO Mailbox: Power State 3 current cuttof in 1/4 Amp increments. Range is 0-128A.
  INT16  ImonOffset[MAX_NUM_VRS];                 ///< PCODE MMIO Mailbox: Imon offset correction. Value is a 2's complement signed integer. Units 1/1000, Range 0-63999. For an offset = 12.580, use 12580. <b>0: Auto</b>
  UINT16 IccMax[MAX_NUM_VRS];                     ///< PCODE MMIO Mailbox: VR Icc Max limit. 0-512A in 1/4 A units. 400 = 100A. <b>Default: 0 - Auto, no override</b>
  UINT16 VrVoltageLimit[MAX_NUM_VRS];             ///< PCODE MMIO Mailbox: VR Voltage Limit. Range is 0-7999mV.
  UINT16 ImonSlope[MAX_NUM_VRS];                  ///< PCODE MMIO Mailbox: Imon slope correction. Specified in 1/100 increment values. Range is 0-200. 125 = 1.25. <b>0: Auto</b>
  UINT8  Psi3Enable[MAX_NUM_VRS];                 ///< PCODE MMIO Mailbox: Power State 3 enable/disable; 0: Disable; <b>1: Enable</b>.
  UINT8  Psi4Enable[MAX_NUM_VRS];                 ///< PCODE MMIO Mailbox: Power State 4 enable/disable; 0: Disable; <b>1: Enable</b>.
  UINT8  VrConfigEnable[MAX_NUM_VRS];             ///< Enable/Disable BIOS configuration of VR; 0: Disable; <b>1: Enable.</b>
  UINT8  TdcEnable[MAX_NUM_VRS];                  ///< PCODE MMIO Mailbox: Thermal Design Current enable/disable; <b>0: Disable; </b>1: Enable
  UINT8  TdcTimeWindow[MAX_NUM_VRS];              ///< @deprecated. PCODE MMIO Mailbox: Thermal Design Current time window. Defined in milli seconds. <b>1ms default</b>
  UINT8  TdcLock[MAX_NUM_VRS];                    ///< PCODE MMIO Mailbox: Thermal Design Current Lock; <b>0: Disable</b>; 1: Enable.
  UINT8  FastPkgCRampDisable[MAX_NUM_VRS];        ///< Disable Fast Slew Rate for Deep Package C States for VR IA,GT,SA,VLCC,FIVR domain based on Acoustic Noise Mitigation feature enabled. <b>0: False</b>; 1: True
  UINT8  SlowSlewRate[MAX_NUM_VRS];               ///< Slew Rate configuration for Deep Package C States for VR VR IA,GT,SA,VLCC,FIVR domain based on Acoustic Noise Mitigation feature enabled. <b>0: Fast/2</b>; 1: Fast/4; 2: Fast/8; 3: Fast/16
  ///@}
  UINT16 MinVoltageRuntime;                       ///< PCODE MMIO Mailbox: Minimum voltage for runtime. Valid if EnableMinVoltageOverride = 1 .Range 0 to 1999mV. <b> 0: 0mV </b>
  UINT16 MinVoltageC8;                            ///< PCODE MMIO Mailbox: Minimum voltage for C8. Valid if EnableMinVoltageOverride = 1. Range 0 to 1999mV. <b> 0: 0mV </b>
  UINT16 PsysOffset1;                             ///< PCODE MMIO Mailbox: Platform Psys offset correction. <b>0: Auto</b> Units 1/1000, Range 0-63999. For an offset of 25.348, enter 25348.
  UINT8  RsvdBytes2[2];
  UINT32 TdcTimeWindow1[MAX_NUM_VRS];             ///< PCODE MMIO Mailbox: Thermal Design Current time window. Defined in milli seconds. <b>1ms default</b>
  UINT8  Irms[MAX_NUM_VRS];                       ///< PCODE MMIO Mailbox: Current root mean square. <b>0: Disable</b>; 1: Enable.
  UINT8  FivrSpectrumEnable;                      ///< Enable or Disable FIVR Spread Spectrum 0: Disable; <b> 1: Enable.</b>
  UINT8  DlvrSpreadSpectrumPercentage;            ///< PCODE MMIO Mailbox: Encoded DLVR SSC value. 0x0 = Disabled. u3.2 value from 0% - 7.75%.
  UINT8  DlvrRfiEnable;                           ///< PCODE MMIO Mailbox: Enable/Disable RFI frequency hopping. <b>0: Disable.</b> 1:Disable
  UINT8  PreWake;                                 ///< PCODE MMIO Mailbox: Acoustic Noise Mitigation Range. This can be programmed only if AcousticNoiseMitigation is enabled.<b>Default Value = 0 micro ticks</b> Defines the max pre-wake randomization time in micro ticks. Range is 0-255.
  UINT8  RampUp;                                  ///< PCODE MMIO Mailbox: Acoustic Noise Mitigation Range. This can be programmed only if AcousticNoiseMitigation is enabled.<b>Default Value = 0 micro ticks</b> Defines the max ramp up randomization time in micro ticks. Range is 0-255.
  UINT8  RampDown;                                ///< PCODE MMIO Mailbox: Acoustic Noise Mitigation Range. This can be programmed only if AcousticNoiseMitigation is enabled.<b>Default Value = 0 micro ticks</b> Defines the max ramp down randomization time in micro ticks. Range is 0-255.
  UINT8  Rsvd2[1];
  UINT16 VccInAuxImonIccImax;                     ///< PCODE MMIO Mailbox: VccIn Aux Imon IccMax. <b>0 - Auto</b> Values are in 1/4 Amp increments. Range is 0-128A8.
  UINT16 VccInAuxImonSlope;                       ///< PCODE MMIO Mailbox: VccInAuxImon slope correction. Specified in 1/100 increment values. Range is 0-200. 125 = 1.25. <b>0: Auto</b>
  INT16  VccInAuxImonOffset;                      ///< PCODE MMIO Mailbox: VccinAuxImon offset correction. Value is a 2's complement signed integer. Units 1/1000, Range 0-63999. For an offset = 12.580, use 12580. <b>0: Auto</b>
  UINT16 DlvrRfiFrequency;                        ///< PCODE MMIO Mailbox: DLVR clock frequency in MHz. Default is 0x55A MHz.
  UINT8  PS1toPS0DynamicCutoffEnable[MAX_NUM_VRS];///< Enable/Disable PS1 to PS0 Dynamic Cutoff; <b>0: Disable</b>; 1: Enable.
  UINT8  PS2toPS1DynamicCutoffEnable[MAX_NUM_VRS];///< Enable/Disable:PS2 to PS1 Dynamic Cutoff; <b>0: Disable</b>; 1: Enable.
  UINT16 PS1toPS0MCoef[MAX_NUM_VRS];              ///< PCODE MMIO Mailbox: PS1 to PS0 M Coef. This number is in M*100 units. <b>Default: 100</b> Range: 0-4096.
  INT16  PS1toPS0CCoef[MAX_NUM_VRS];              ///< PCODE MMIO Mailbox: PS1 to PS0 C Coef. This number is in C*100 units. <b>Default: 2000</b> Range: 0-4096.
  UINT16 PS2toPS1MCoef[MAX_NUM_VRS];              ///< PCODE MMIO Mailbox: PS2 to PS1 M Coef. This number is in M*100 units. <b>Default: 100</b> Range: 0-4096.
  INT16  PS2toPS1CCoef[MAX_NUM_VRS];              ///< PCODE MMIO Mailbox: PS2 to PS1 C Coef. This number is in C*100 units. <b>Default: 500</b> Range: 0-4096.
  /**
    This field is required to be non-zero on Desktop platforms.

    Used to communicate the power delivery design capability of the board. This value is an
    enum of the available power delivery segments that are defined in the Platform Design Guide.
    This is required due to CPU socket compatibility across different board designs. Without
    this field populated, the system may experience VR OCP/OVP shutdowns, hangs, thermal and
    performance loss.
    <b> 0 - Disable </b>
  **/
  UINT8  VrPowerDeliveryDesign;
  UINT8  Rsvd4[1];
  /**
    Enable VCC Demotion.
      0: Disable;
      <b>1: Enable with default threshold</b>;
      2: Enable with user configured threshold.
  **/
  UINT8  VccInDemotionEnable[MAX_NUM_VRS];
  /**
    User configured platform quiescent threshold in milli-watt
    when VCC Demotion is enabled with user configured threshold.
    Range: 0-255.
  **/
  UINT8  VccInDemotionQuiescentPowerInMw[MAX_NUM_VRS];
  UINT8  Rsvd5[2];                                ///< Make next field 4-byte aligned.
  /**
    User configured VR's quiescent power threshold in micro-farad
    when VCC Demotion is enabled with user configured threshold.
    Range: 0-2000.
  **/
  UINT32 VccInDemotionCapacitanceInUf[MAX_NUM_VRS];

  UINT8  EnableVsysCritical;                     ///< PCODE MMIO Mailbox: Enable/Disable Vsys Critical; <b>0: Disable</b>; 1: Enable.
  /**
    Set VSYS Critical [0x4A]
    Write to 0x4A to program Vsys Critical Threshold which is a linear fractional value of the Vsys input full scale voltage
    where minimun is 0 and maximum is 255. Default 0
    For example: Vsys_Crit (0x4A) = (Critical Threshold/Full Scale) * 0xFF
  **/
  UINT8  VsysFullScale;
  UINT8  VsysCriticalThreshold;
  /**
    Set Assertion Deglitch [0x4F] and De-Assertion Deglitch [0x49]
    Provide 2 fields in BIOS Menu for Assertion/De-Assertion Deglitch: Mantissa and Exponent.
    The format for Assertion/De-Assertion Deglitch: 0.002ms * Mantissa * 2^(Exponent).
  **/
  UINT8  VsysAssertionDeglitchMantissa;
  UINT8  VsysAssertionDeglitchExponent;
  UINT8  VsysDeassertionDeglitchMantissa;
  UINT8  VsysDeassertionDeglitchExponent;
  UINT8  Rsvd6[1];
  /**
    PCODE VR Mailbox: Voltage Regulator Fast Vmode ICC Limit.
    This value represents the current threshold where the VR would initiate reactive protection if Fast Vmode is enabled.
    A value of 0 corresponds to feature disabled (no reactive protection).
    <b>Default: 0 - Disable, no override</b>
  **/
  UINT16 IccLimit[MAX_NUM_VRS];
  UINT8  RsvdBytes3[2];
} CPU_POWER_MGMT_VR_CONFIG;

#pragma pack (pop)

#endif // _CPU_POWER_MGMT_VR_CONFIG_H_
