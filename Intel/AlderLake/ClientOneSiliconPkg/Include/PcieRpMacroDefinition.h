/** @file
  This file contains definitions of PCIE RP MACRO.

@copyright
  INTEL CONFIDENTIAL
  Copyright 2020 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains an 'Intel Peripheral Driver' and is uniquely identified as
  "Intel Reference Module" and is licensed for Intel CPUs and chipsets under
  the terms of your license agreement with Intel or your vendor. This file may
  be modified by the user, subject to additional terms of the license agreement.

@par Specification Reference:
**/
#ifndef _PCIE_RP_MACRO_DEFINITION_H
#define _PCIE_RP_MACRO_DEFINITION_H

//
// PCIE RP Type MACRO Definitions
//


// Defining MACRO Value for PCH PCIE RP Type.
#ifndef PCIE_RP_TYPE_PCH
#define PCIE_RP_TYPE_PCH                  0x02
#endif

// Defining MACRO Value for CPU PCIE RP Type.
#ifndef PCIE_RP_TYPE_CPU
#define PCIE_RP_TYPE_CPU                  0x04
#endif

// Defining MACRO Value for ITBT PCIE RP Type.
#ifndef PCIE_RP_TYPE_ITBT
#define PCIE_RP_TYPE_ITBT                 0x08
#endif


//
// PCIE EP Type MACRO Definitions
//

// Note:- Defining only those EndPoint Device Type Macro, those doesn't fully
// compliance with PCIE Standard Power Sequence. For these device we might need
// additional flow in Power Sequence.


// Defining MACRO Value for Generic PCIE EP Type.
#ifndef PCIE_EP_TYPE_GENERIC_PCIE
#define PCIE_EP_TYPE_GENERIC_PCIE         0x02
#endif

// Defining MACRO Value for Discrete Thunderbolt EP Type.
#ifndef PCIE_EP_TYPE_DTBT
#define PCIE_EP_TYPE_DTBT                 0x04
#endif

// Defining MACRO Value for Invalid EP Type.
#ifndef PCIE_EP_TYPE_INVALID
#define PCIE_EP_TYPE_INVALID              0xFF
#endif

#endif // _PCIE_RP_MACRO_DEFINITION_H
