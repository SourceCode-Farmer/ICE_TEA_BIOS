/** @file
  Header file containing Voltage Regulator initialization common definitions.

@copyright
  INTEL CONFIDENTIAL
  Copyright 2019 -2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains an 'Intel Peripheral Driver' and is uniquely identified as
  "Intel Reference Module" and is licensed for Intel CPUs and chipsets under
  the terms of your license agreement with Intel or your vendor. This file may
  be modified by the user, subject to additional terms of the license agreement.

@par Specification Reference:
**/
#ifndef _PEI_VR_COMMON_H_
#define _PEI_VR_COMMON_H_

#define MIN_VR_INDEX  0x0
#define MAX_VR_INDEX  0x5

//
// VR mailbox commands
//
#define MAILBOX_VR_CMD_VR_INTERFACE                           0x04
#define MAILBOX_VR_CMD_SVID_COMMAND_GET_REG                   0x07

#define MAILBOX_VR_CMD_READ_ACOUSTIC_MITIGATION_RANGE         0x07
#define MAILBOX_VR_CMD_WRITE_ACOUSTIC_MITIGATION_RANGE        0x08
#define MAILBOX_VR_CMD_READ_VR_TDC_CONFIG                     0x19
#define MAILBOX_VR_CMD_WRITE_VR_TDC_CONFIG                    0x1A

//
// MAILBOX_VR_CMD_SVID_VR_HANDLER command set.
//
#define MAILBOX_VR_CMD_SVID_VR_HANDLER                        0x18
#define MAILBOX_VR_SUBCMD_SVID_GET_STRAP_CONFIGURATION        0x00
#define MAILBOX_VR_SUBCMD_SVID_GET_ACDC_LOADLINE              0x01
#define MAILBOX_VR_SUBCMD_SVID_SET_ACDC_LOADLINE              0x02
#define MAILBOX_VR_SUBCMD_SVID_SET_PS_CUTOFF                  0x03
#define MAILBOX_VR_SUBCMD_SVID_SET_IMON_CONFIG                0x04
#define MAILBOX_VR_SUBCMD_SVID_GET_MAX_ICC                    0x05
#define MAILBOX_VR_SUBCMD_SVID_SET_MAX_ICC                    0x06
#define MAILBOX_VR_SUBCMD_SVID_GET_VOLTAGE_LIMIT              0x07
#define MAILBOX_VR_SUBCMD_SVID_SET_VOLTAGE_LIMIT              0x08
#define MAILBOX_VR_SUBCMD_SVID_SET_PMON_CONFIG                0x09
#define MAILBOX_VR_SUBCMD_SVID_GET_PMON_PMAX                  0x0A
#define MAILBOX_VR_SUBCMD_SVID_SET_PMON_PMAX                  0x0B
#define MAILBOX_VR_SUBCMD_SVID_SET_VR_SLEW_RATE               0x0C
#define MAILBOX_VR_SUBCMD_SVID_SET_DISABLE_FAST_PKGC_RAMP     0x0D
#define MAILBOX_VR_SUBCMD_SVID_SET_PSYS_PS4_DISABLE           0x0E
#define MAILBOX_VR_SUBCMD_SVID_GET_PSYS_PS4_DISABLE           0x0F
#define MAILBOX_VR_SUBCMD_SVID_GET_PSYS_REGISTER              0x10
#define MAILBOX_VR_SUBCMD_SVID_SET_PSYS_REGISTER              0x11
#define MAILBOX_VR_SUBCMD_SVID_EXCLUSIVE_MODE                 0x12
#define MAILBOX_VR_SUBCMD_SVID_GET_PS_CUTOFF                  0x13
#define MAILBOX_VR_SUBCMD_SVID_GET_IMON_CONFIG                0x14
#define MAILBOX_VR_SUBCMD_SVID_READ_REG_WHITELIST             0x15
#define MAILBOX_VR_SUBCMD_SVID_WRITE_REG_WHITELIST            0x16
#define MAILBOX_VR_SUBCMD_SVID_SET_VCCINAUX_IMON_IMAX         0x17
#define MAILBOX_VR_SUBCMD_SVID_GET_VCCINAUX_IMON_IMAX         0x18
#define MAILBOX_VR_SUBCMD_SVID_GET_PMON_CONFIG                0x19
#define MAILBOX_VR_SUBCMD_SVID_SET_VCCINAUX_IMON_CONFIG       0x1A
#define MAILBOX_VR_SUBCMD_SVID_GET_VCCINAUX_IMON_CONFIG       0x1B
#define MAILBOX_VR_SUBCMD_SVID_GET_VR_SLEW_RATE               0x1C

#define MAILBOX_VR_SUBCMD_SVID_SET_VR                         0x11
#define MAILBOX_VR_SUBCMD_SVID_SET_PSYS_VR                    0xD
#define MAILBOX_VR_SUBCMD_SVID_SET_VR_VSYS_MODE               0x34
#define MAILBOX_VR_SUBCMD_SVID_SET_VR_CRIT_THRESHOLD          0x4A
#define MAILBOX_VR_SUBCMD_SVID_SET_VR_CONFIG2                 0x4F
#define MAILBOX_VR_SUBCMD_SVID_SET_VR_CONFIG1                 0x49

#define MAILBOX_VR_SUBCMD_SVID_SET_PS1_PS0_DYNAMIC_CUTOFF     0x20
#define MAILBOX_VR_SUBCMD_SVID_SET_PS2_PS1_DYNAMIC_CUTOFF     0x21
#define MAILBOX_VR_SUBCMD_SVID_GET_PS1_PS0_DYNAMIC_CUTOFF     0x22
#define MAILBOX_VR_SUBCMD_SVID_GET_PS2_PS1_DYNAMIC_CUTOFF     0x23
#define MAILBOX_VR_SUBCMD_SVID_SET_QUIESCENT_POWER_AND_PLATFORM_CAP 0x1E
#define MAILBOX_VR_SUBCMD_SVID_GET_QUIESCENT_POWER_AND_PLATFORM_CAP 0x1F
#define MAILBOX_VR_SUBCMD_SVID_GET_FAST_VMODE_ICC_LIMIT       0x24
#define MAILBOX_VR_SUBCMD_SVID_SET_FAST_VMODE_ICC_LIMIT       0x25

#define MAILBOX_PCODE_CMD_READ_SOFT_STRAPS 0x1F
#define CPU_SOFTSTRAP_SET1_HIGH            1
#define CPU_SOFTSTRAP_SET2_LOW             2
#define CPU_SOFTSTRAP_SET2_HIGH            3
#define STRAP_RAW_VALUE                    0
#define STRAP_RESOLVED_VALUE               1

//
//  VR mailbox commands for Acoustic Noise Mitigation
//
#define NOISE_MIGITATION_RANGE_MASK               0xFF
#define RAMP_UP_OFFSET                            8
#define RAMP_DOWN_OFFSET                          16

#define VR_ADDRESS_MASK                   0xF
#define AC_LOADLINE_MASK                  0xFFFF
#define DC_LOADLINE_MASK                  0xFFFF0000
#define VR_TDC_ADDRESS_OFFSET             8
#define DC_LOADLINE_OFFSET                16
#define AC_DC_LOADLINE_MAX                6249 ///< 62.49 mOhm max
#define PSI_THRESHOLD_MASK                0x3FF
#define PSI2_THRESHOLD_OFFSET_MASK        0xFFC00
#define PSI3_THRESHOLD_OFFSET_MASK        0x3FF00000
#define PSI2_THRESHOLD_OFFSET             10
#define PSI3_THRESHOLD_OFFSET             20
#define PSI3_ENABLE_OFFSET                30
#define PSI4_ENABLE_OFFSET                31
#define VR_IMON_SLOPE_OFFSET              16
#define VR_IMON_SLOPE_MASK                0xFFFF0000
#define VR_IMON_OFFSET_MASK               0xFFFF
#define VR_VOLTAGE_LIMIT_MASK             0xFFFF
#define VR_PSYS_SLOPE_OFFSET              16
#define VR_PSYS_OFFSET_MASK               0xFFFF
#define VR_TDC_CURRENT_LIMIT_MASK         0x7FFF
#define VR_TDC_TIME_WINDOW_MASK           0x7F
#define VR_TDC_TIME_WINDOW_LOCATION_MASK  0xFE0000
#define VR_TDC_ENABLE_OFFSET              15
#define VR_TDC_TIME_WINDOW_OFFSET         17
#define VR_TDC_IRMS_OFFSET                30
#define VR_TDC_LOCK_OFFSET                31
#define VR_TDC_TIME_WINDOW_MAX            10

///
///  RFI Definitions
///
#define CLOCK_FREQUENCY_19MHz                    19200000
#define CLOCK_FREQUENCY_24MHz                    24000000
#define CLOCK_FREQUENCY_38MHz                    38400000

#define R_EMI_CONTROL_0_0_0_MCHBAR_PCU           0x5A08
#define B_SPREAD_SPECTRUM_ENABLE                 BIT8
#define B_SPREAD_SPECTRUM_MASK                   0xFF
#define V_SPREAD_SPECTRUM_POINT_ONE_ENCODING_MAX 0x1C
#define V_FIVR_SPREAD_SPECTRUM_MAX               0x3F

//24MHz XTAL Clock Frequency
#define R_RFI_CONTROL_0_0_0_MCHBAR_PCU           0x5A0C
#define N_RFI_FREQ_LO_PF2_OFFSET                 2
#define N_RFI_FREQ_HI_PF2_OFFSET                 16
#define B_RFI_FREQ_PF2_MASK                      (BIT23 | BIT22 | BIT21 | BIT20 | BIT19 | BIT18 | BIT17 | BIT16 | BIT3 | BIT2)

//38.4MHx XTAL Clock Frequency
#define R_RFI_CONTROL2_0_0_0_MCHBAR_PCU          0x5A18
#define N_RFI_FREQ_LO_PF3_OFFSET                 11
#define N_RFI_FREQ_HI_PF3_OFFSET                 16
#define B_RFI_FREQ_PF3_MASK                      (BIT23 | BIT22 | BIT21 | BIT20 | BIT19 | BIT18 | BIT17 | BIT16 | BIT13 | BIT12 | BIT11)
#define N_RFI_FREQ_LO_PF3_OFFSET2                10
#define B_RFI_FREQ_PF3_MASK2                     (BIT23 | BIT22 | BIT21 | BIT20 | BIT19 | BIT18 | BIT17 | BIT16 | BIT11 | BIT10)

#define B_RFI_FREQ_ADJ_MASK                      0xFF
#define V_RFI_FREQ_ADJ_MAX                       0x7F
#define B_RFI_FREQ_SPREAD_MASK                   0xFF00
#define B_RFI_FREQ_UPDATE_MASK                   0xFFFF
#define N_RFI_FREQ_LO_PF1_OFFSET                 0
#define N_RFI_FREQ_HI_PF1_OFFSET                 8
#define B_RFI_FREQ_PF1_MASK                      (BIT15 | BIT14 | BIT13 | BIT12 | BIT11 | BIT10 | BIT9 | BIT8 | BIT1 | BIT0)
#define R_RFI_STATUS_0_0_0_MCHBAR_PCU            0x5A10
#define B_RFI_CURRENT_FREQ_MASK                  0xFFF
#define V_MAX_RFI_VALUE                          0x3FF


typedef union {
  UINT32 VrInterfaceData;   ///< All bit fields as a 32-bit value.
  ///
  /// Individual bit fields.
  ///
  struct {
    UINT32 VrId        : 4;  ///< VR ID of the domain
    UINT32 VrCommand   : 5;  ///< This field contains VR command
    UINT32 Rsvd        : 7;  ///< Reserved for future use
    UINT32 VrRegAddr   : 8;  ///< SVID Register Address
    UINT32 Rsvd2       : 7;  ///< Reserved for future use
    UINT32 Lock        : 1;  ///< Lock bit
  } Fields;
} MAILBOX_VR_INTERFACE_DATA;

#endif // _PEI_VR_COMMON_H_
