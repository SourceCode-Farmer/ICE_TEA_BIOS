/** @file
  APIs for Intel One Click Recovery Main Protocol.

@copyright
  INTEL CONFIDENTIAL
  Copyright 2020 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains a 'Sample Driver' and is licensed as such under the terms
  of your license agreement with Intel or your vendor. This file may be modified
  by the user, subject to the additional terms of the license agreement.

@par Specification Reference:
**/
#ifndef _ONE_CLICK_RECOVERY_PROTOCOL_H_
#define _ONE_CLICK_RECOVERY_PROTOCOL_H_

#include <AsfMsgs.h>

//
// Intel One Click Recovery Capabilites
//
typedef union {
  struct{
    UINT8 OcrBootHttps        : 1;    ///< [0]    BIOS supports Intel One Click Recovery Https  with AMT specified device path
    UINT8 OcrBootPba          : 1;    ///< [1]    BIOS supports Intel One Click Recovery PBA Boot with AMT specified device path
    UINT8 OcrBootWinRe        : 1;    ///< [2]    BIOS supports Intel One Click Recovery WinRe Boot
    UINT8 OcrAmtDisSecBoot    : 1;    ///< [3]    BIOS suporrts Intel One Click Recovery disable Secure Boot for AMT provided UEFI Load option
    UINT8 OcrWifiProfile      : 1;    ///< [4]    BIOS suporrts Intel One Click Recovery WiFi Profile sync
    UINT8 Reserved2           : 3;    ///< [7:5]  Reserved
  } Bits;
  UINT8   Data;
} ONE_CLICK_RECOVERY_CAP;

//
// Extern the GUID for protocol users.
//
extern EFI_GUID                         gOneClickRecoveryProtocolGuid;

//
// Forward reference for ANSI C compatibility
//
typedef struct _ONE_CLICK_RECOVERY_PROTOCOL                 ONE_CLICK_RECOVERY_PROTOCOL;

/**
  The Intel One Click Recovery Setup main function.

  The function does the necessary work to Setup the One Click Recovery feature.

  @retval     EFI_SUCCESS       One Click Recovery tasks have been ran
  @retval     EFI_UNSUPPORTED   One Click Recovery is not supported
**/
typedef
EFI_STATUS
(EFIAPI *ONE_CLICK_RECOVERY_ENTRY) (
  VOID
  );

/**
  Gives Intel One click Recovery BIOS Capabilities supported

  @retval ONE_CLICK_RECOVERY_CAPS       One Click Recovery BIOS Capabilites
**/
typedef
ONE_CLICK_RECOVERY_CAP
(EFIAPI *ONE_CLICK_RECOVERY_CAPABILITIES) (
  VOID
  );

/**
  Saves OCR Uefi Boot Option from AMT.

  Needs to Be called before Clear Boot Options.

  @retval     EFI_SUCCESS       Saved Boot Option
**/
typedef
EFI_STATUS
(EFIAPI *ONE_CLICK_RECOVERY_SAVE_UEFI_BOOT_OPTION) (
  IN ASF_BOOT_OPTIONS           AsfBootOptions
  );

struct _ONE_CLICK_RECOVERY_PROTOCOL {
  /**
    This member specifies the revision of this structure. This field is used to
    indicate backwards compatible changes to the protocol.
  **/
  UINT32                                        Revision;
  ONE_CLICK_RECOVERY_ENTRY                      OcrEntry;
  ONE_CLICK_RECOVERY_CAPABILITIES               OcrCap;
  ONE_CLICK_RECOVERY_SAVE_UEFI_BOOT_OPTION      OcrSaveUefiBootOption;
};

/**
  Intel One Click Recovery Setup Protocol revision number

  Revision 1:   Initial version
**/
#define  ONE_CLICK_RECOVERY_PROTOCOL_REVISION                 1

#endif
