/** @file
  MEBx Display Protocol definitions

@copyright
  INTEL CONFIDENTIAL
  Copyright 2020 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains an 'Intel Peripheral Driver' and is uniquely identified as
  "Intel Reference Module" and is licensed for Intel CPUs and chipsets under
  the terms of your license agreement with Intel or your vendor. This file may
  be modified by the user, subject to additional terms of the license agreement.

@par Specification Reference:
**/
#ifndef _MEBX_DISPLAY_PROTOCOL_H_
#define _MEBX_DISPLAY_PROTOCOL_H_


///
/// MEBx Display Protocol
///

typedef struct _MEBX_DISPLAY_PROTOCOL MEBX_DISPLAY_PROTOCOL;

typedef enum {
  ScreenModeMebx,
  ScreenModeBios
} SCREEN_MODE;

typedef enum {
  MebxUserInputNoInput,
  MebxUserInputEsc,
  MebxUserInputEnter,
  MebxUserInputOtherKey
} MEBX_USER_INPUT;

///
/// MEBx Message ID
///
typedef enum {
  RecordInvalidError,
  LoginFailedError,
  ProvDataMissingError,
  FailedEnabledFeaturesError,
  FailedUpdateManageabilityModeError,
  AppFailedLoadError,
  FwUpdateError,
  FwUpdateDeprecatedError,
  FailedGetProvStatusError,
  PkiDnsSuffixInvalidError,
  PkiDnsSuffixError,
  ConfigServerFqdnInvalidError,
  RemoteConfigEnDisInvalidError,
  RemoteConfigEnDisError,
  DefaultHashEnInvalidError,
  DefaultHashEnError,
  DefaultHashDisError,
  CustomHashConfigInvalidError,
  CustomHashConfigError,
  DeleteHashError,
  CustomHashStateError,
  InvalidCustomHashError,
  SolStorageRedirConfigInvalidError,
  SolStorageRedirDataError,
  SolStorageRedirAuthProtError,
  HostNameLargeError,
  DomainNameLargeError,
  DhcpInvalidError,
  IdleTimeoutInvalidError,
  IdleTimeoutError,
  ProvServerInvalidError,
  ProvServerPortInvalidError,
  Ipv4ParamsInvalidError,
  PwdPolicyInvalidError,
  Ipv6DataInvalidError,
  Ipv6SettingError,
  SharedFqdnInvalidError,
  DdnsInvalidError,
  KvmStateInvalidError,
  KvmStateError,
  OptInDataInvalidError,
  MeProvHaltDataInvalidError,
  MeProvHaltError,
  ManualSetupConfDataInvalidError,
  ProvServAddressError,
  HashHandlesError,
  HashEntriesError,
  AddCustomHashError,
  MeProvActivateError,
  Ipv4ParametersError,
  FqdnSettingError,
  OptinDataError,
  CompleteConfigFailedError,
  NewPasswordError,
  PowerPackagesError,
  ConinError,
  AmthiGetAmthiInterfaceVersionApiError,
  AmthiGetKvmStateApiError,
  AmthiGetPolicyAmtRedirectionStateApiError,
  AmthiGetOptinStateApiError,
  AmthiGetConfigSvrDataApiError,
  AmthiGetZtcConfigApiError,
  AmthiGetPkiFqdnSuffixApiError,
  AmthiGetIpv4ParamsApiError,
  AmthiGetFqdnApiError,
  AmthiGetIdleTimeoutApiError,
  AmthiGetProvisionStateApiError,
  AmthiGetAuditRecordApiError,
  AmthiGetHashDataApiError,
  AmthiGetConnectionStatusApiError,
  AmthiGetIpv6LanIntfSettingsApiError,
  AmthiGetPrivacyLevelApiError,
  AmthiGetPowerPolicyApiError,
  AmthiSetMebxEnterStateApiError,
  AmthiSetMebxExitStateApiError,
  KvmActiveSessionMsg,
  AmthiSetIpv4ParamsApiError,
  AmthiSetPkiFqdnSuffixApiError,
  AmthiSetSolStorageRedirectionStateApiError,
  AmthiSetKvmStateApiError,
  AmthiSetPwdPolicyApiError,
  AmthiSetFqdnApiError,
  AmthiSetOptinStateApiError,
  AmthiSetIdleTimeoutApiError,
  AmthiSetConfigServerApiError,
  AmthiPowerPolicyApiError,
  AmthiSetHashStateApiError,
  AmthiSetZtcApiError,
  AmthiStopConfigUnprovisionApiError,
  MeGetUserCapsApiError,
  MeGetFwCapsApiError,
  MeGetFwEnabledFeatureApiError,
  MeMeWaitFwFeatureAvailableApiError,
  MeGetMoffOvrdStateApiError,
  MeGetSouthClinkStateApiError,
  MeSetAmtStateApiError,
  CoreUnconfigWoPassGetUnconfigStatusError,
  CoreUsbProvError,
  MeSetMoffOvrdStateApiError,
  MeSetSouthClinkStateApiError,
  AmthiCloseUserInitiatedConnApiError,
  PressAnyKeyMsg,
  FoundUsbKeyMsg,
  ContinueAutoProvMsg,
  LoadingAmtMsg,
  DoneMsg,
  AmtManageabilityUsbDataMissingMsg,
  AmtManageabilityAlreadyProvMsg,
  StringNotAppliedTooManyHashesMsg,
  ConfigAppliedMsg,
  ContinueBootMsg,
  CoreCautionMsg,
  CoreCpuReplacementMsg,
  CoreFeaturesDisabledMsg,
  CoreFeaturesEnabledMsg,
  CoreConfirm1Msg,
  CoreConfirm2Msg,
  AmtManageabilityCilaMsg,
  CoreUnconfigWoPassMsg,
  AmtManualConfigUnsupportedMsg,

  // Values from 0x80000 to 0x8FFFF are reserved for USB Provisioning record display
  CorruptedDataEntryStart = 0x80000,
  CorruptedDataEntryEnd   = 0x8FFFF
} MEBX_MSG_ID;

/**
  This function prints MEBx information string to screen.

  @param[in,out] This      Pointer to MEBx Display Protocol
  @param[in]     MsgId     Message id

  @retval EFI_SUCCESS      Text was displayed successfully
  @retval Others           Error has occurred during displaying text
**/
typedef
EFI_STATUS
(EFIAPI *MEBX_DISPLAY_TEXT) (
  IN OUT MEBX_DISPLAY_PROTOCOL *This,
  IN     MEBX_MSG_ID           MsgId
  );

/**
  This function prints MEBx error string to screen and to console.

  @param[in,out] This      Pointer to MEBx Display Protocol.
  @param[in]     MsgId     The identifier of message.
  @param[in]     Delay     The amount of time for which the text will be displayed in microseconds.
                           If value is 0, the text is displayed in a fixed microseconds.

  @retval EFI_SUCCESS      Text was displayed successfully.
  @retval EFI_NOT_FOUND    The string specified by MsgId wasn't found.
**/
typedef
EFI_STATUS
(EFIAPI *MEBX_DISPLAY_ERROR) (
  IN OUT MEBX_DISPLAY_PROTOCOL *This,
  IN     MEBX_MSG_ID           MsgId,
  IN     UINT32                Delay
  );

/**
  This function draws image on the graphics screen.

  @param[in,out] This           Pointer to MEBx Display Protocol
  @param[in]     Bitmap         The data to transfer to the graphics screen
  @param[in]     ImageWidth     The width of image
  @param[in]     ImageHeight    The height of image

  @retval EFI_SUCCESS           Image was displayed successfully
  @retval EFI_INVALID_PARAMETER Invalid parameter
**/
typedef
EFI_STATUS
(EFIAPI *MEBX_DISPLAY_IMAGE) (
  IN OUT MEBX_DISPLAY_PROTOCOL *This,
  IN     UINT8                 *Bitmap,
  IN     UINT32                ImageWidth,
  IN     UINT32                ImageHeight
  );

/**
  This function sets specified graphics mode.

  @param[in,out] This      Pointer to MEBx Display Protocol
  @param[in]     Mode      Graphics mode to set

  @retval EFI_SUCCESS      New graphics mode set successfully
  @retval Others           Error has occurred
**/
typedef
EFI_STATUS
(EFIAPI *MEBX_CONFIGURE_SCREEN) (
  IN OUT MEBX_DISPLAY_PROTOCOL *This,
  IN     SCREEN_MODE           Mode
  );

/**
  This function reads user input.

  @param[in,out] This      Pointer to MEBx Display Protocol
  @param[out]    UserInput User input

  @retval EFI_SUCCESS      Successfully read user input
  @retval EFI_NOT_READY    There was no input from an user.
  @retval Others           Error has occurred
**/
typedef
EFI_STATUS
(EFIAPI *MEBX_GET_USER_INPUT) (
  IN OUT MEBX_DISPLAY_PROTOCOL *This,
     OUT MEBX_USER_INPUT       *UserInput
  );

/**
  This function gets information about current screen resolution.

  @param[in,out] This      Pointer to MEBx Display Protocol
  @param[out]    Width     Current screen width
  @param[out]    Height    Current screen height

  @retval EFI_SUCCESS      The function succeeded
  @retval Others           Error has occurred
**/
typedef
EFI_STATUS
(EFIAPI *MEBX_GET_SCREEN_RESOLUTION) (
  IN OUT MEBX_DISPLAY_PROTOCOL *This,
     OUT UINT16                *Width,
     OUT UINT16                *Height
  );

/**
  This function clears a screen's content.

  @param[in,out] This      Pointer to MEBx Display Protocol

  @retval EFI_SUCCESS      The function succeeded
  @retval Others           Error has occurred
**/
typedef
EFI_STATUS
(EFIAPI *MEBX_CLEAR_SCREEN) (
  IN OUT MEBX_DISPLAY_PROTOCOL *This
  );

/**
  MEBx Display Protocol
  The interface functions are used for I/O operations related to screen.
  It is platform vendor's responsibility to implement the function instance.
**/
typedef struct _MEBX_DISPLAY_PROTOCOL {
  MEBX_CONFIGURE_SCREEN      ConfigureScreen;
  MEBX_DISPLAY_TEXT          DisplayText;
  MEBX_DISPLAY_ERROR         DisplayError;
  MEBX_DISPLAY_IMAGE         DisplayImage;
  MEBX_GET_USER_INPUT        GetUserInput;
  MEBX_GET_SCREEN_RESOLUTION GetScreenResolution;
  MEBX_CLEAR_SCREEN          ClearScreen;
} MEBX_DISPLAY_PROTOCOL;

extern EFI_GUID gMebxDisplayProtocolGuid;

#endif // _MEBX_DISPLAY_PROTOCOL_H_
