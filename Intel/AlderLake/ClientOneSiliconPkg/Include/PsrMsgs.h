/** @file
  Definitions for querying PSR message

@copyright
  INTEL CONFIDENTIAL
  Copyright 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains an 'Intel Peripheral Driver' and is uniquely identified as
  "Intel Reference Module" and is licensed for Intel CPUs and chipsets under
  the terms of your license agreement with Intel or your vendor. This file may
  be modified by the user, subject to additional terms of the license agreement.

@par Specification Reference:
**/

#ifndef _PSR_MSG_H_
#define _PSR_MSG_H_

#define PSR_GENESIS_SIZE                               1288
#define PSR_LEDGER_SIZE                                16
#define PSR_EVENT_SIZE                                 12
#define PSR_MAX_EVENT_COUNT                            100
#define PSR_MAX_CERT_CHAIN_SIZE                        3000
#define PSR_ODCA_CHAIN_LEN                             4
#define PSR_MAX_SIGNATURE_LENGTH                       512
#define NONCE_MAXSIZE                                  20
#define PSR_SHA512_LEN_BYTES                           512
#define PSR_PLATFORM_ID_LENGTH                         64

//PSR Commands
#define GET_PSR_REQUEST_CMD                            0x0A
#define PSR_INFORM_CHASSIS_INTRUSION_CMD               0x0B

#pragma pack(1)

typedef struct {
  UINT16           Major;
  UINT16           Minor;
} PSR_VERSION;

typedef struct {
  UINT16            Major;
  UINT16            Minor;
  UINT16            Hotfix;
  UINT16            Build;
} CSME_FW_VERSION;

#ifndef VFRCOMPILE

typedef enum {
  Ecdsa384WithSha384 = 0,
} LOG_SIGNING_MECHANISM;

typedef enum {
  PsrStatusSuccess,
  PsrStatusFeatureNotSupported,
  PsrStatusUpidDisabled,
  PsrStatusActionNotAllowed,
  PsrStatusInvalidInputParameter,
  PsrStatusInternalError,
  PsrStatusNotAllowedAfterEop,
} PSR_STATUS;

typedef enum {
  PsrLogStateNotStarted = 0, /* Default value */
  PsrLogStateStarted,
  PsrLogStateStopped,
} PSR_LOGGING_STATUS;

typedef struct {
  UINT8             Command;
  UINT8             Reserved;
  UINT16            ByteCount;
} PSR_HECI_HEADER;

typedef struct {
  PSR_HECI_HEADER    Header;
  UINT8              UserNonce[NONCE_MAXSIZE];
} PSR_REQUEST;

typedef struct {
  UINT8              Psrid[16];
  UINT8              Upid[PSR_PLATFORM_ID_LENGTH];
  UINT8              GenesisBuffer[PSR_GENESIS_SIZE];
  UINT32             Ledger[PSR_LEDGER_SIZE];
  UINT32             TotalEventCount;
  UINT8              EventsBuffer[(PSR_EVENT_SIZE * PSR_MAX_EVENT_COUNT)];
} PLATFORM_SERVICE_RECORD;

typedef struct {
  PSR_VERSION                      PsrVersion;
  PLATFORM_SERVICE_RECORD          Psr;
  UINT8                            PsrHash[PSR_SHA512_LEN_BYTES];
  UINT8                            UserNonce[NONCE_MAXSIZE];
  UINT8                            CsmeNonce[NONCE_MAXSIZE];
  CSME_FW_VERSION                  FwVersion;
  LOG_SIGNING_MECHANISM            SignatureMechanism;
  UINT8                            Signature[PSR_MAX_SIGNATURE_LENGTH];
  UINT16                           LengthsOfCertificate[PSR_ODCA_CHAIN_LEN];
  UINT8                            Certificates[PSR_MAX_CERT_CHAIN_SIZE];
} PSR_DATA;

typedef struct {
  PSR_HECI_HEADER                  Header;
  PSR_STATUS                       Status;
  PSR_LOGGING_STATUS               PsrLoggingStatus;
  PSR_DATA                         PsrData;
} PSR_RESPONSE;

typedef union {
  PSR_REQUEST  Request;
  PSR_RESPONSE Response;
} GET_PSR_BUFFER;

typedef struct {
  PSR_HECI_HEADER         Header;
} PSR_CHASSIS_INTRUSION_DET_EVENT_REQUEST;

#endif //VFRCOMPILE

#pragma pack()

#endif // _PSR_MSG_H_

