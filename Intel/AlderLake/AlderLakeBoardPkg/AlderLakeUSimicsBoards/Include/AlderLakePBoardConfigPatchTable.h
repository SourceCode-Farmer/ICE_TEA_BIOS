/** @file

 @copyright
  INTEL CONFIDENTIAL
  Copyright 2016 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains a 'Sample Driver' and is licensed as such under the terms
  of your license agreement with Intel or your vendor. This file may be modified
  by the user, subject to the additional terms of the license agreement.

@par Specification Reference:
**/

#ifndef _ALDERLAKE_CONFIG_PATCH_TABLE_H_
#define _ALDERLAKE_CONFIG_PATCH_TABLE_H_

#include <Library/SetupInitLib.h>
#include <SetupVariable.h>
#include <Protocol/RetimerCapsuleUpdate.h>

CONFIG_PATCH_TABLE  mAlderLakePDdr4RvpSaSetupConfigPatchTable[] = {
//{-Type------------------Size-------------------------------------------Offset------------------------------------Data-------------},
  { SIZE_OF_FIELD(SA_SETUP, EnableVtd),                            OFFSET_OF(SA_SETUP, EnableVtd),                           1 },
  { SIZE_OF_FIELD(SA_SETUP, VmdEnable),                            OFFSET_OF(SA_SETUP, VmdEnable),                           0 },
  { SIZE_OF_FIELD(SA_SETUP, VmdGlobalMapping),                     OFFSET_OF(SA_SETUP, VmdGlobalMapping),                    0 }
};

///
///

CONFIG_PATCH_STRUCTURE mAlderLakePDdr4RvpConfigPatchStruct[] = {
  { L"Setup",    &gSetupVariableGuid,    sizeof(SETUP_DATA), NULL, 0 },
  { L"SaSetup",  &gSaSetupVariableGuid,  sizeof(SA_SETUP),  mAlderLakePDdr4RvpSaSetupConfigPatchTable,   SIZE_OF_TABLE(mAlderLakePDdr4RvpSaSetupConfigPatchTable,  CONFIG_PATCH_TABLE) },
  { L"PchSetup", &gPchSetupVariableGuid, sizeof(PCH_SETUP),  NULL, 0 },
  { L"MeSetup",  &gMeSetupVariableGuid,  sizeof(ME_SETUP),   NULL, 0 }
};

#endif // _ALDERLAKE_CONFIG_PATCH_TABLE_H_
