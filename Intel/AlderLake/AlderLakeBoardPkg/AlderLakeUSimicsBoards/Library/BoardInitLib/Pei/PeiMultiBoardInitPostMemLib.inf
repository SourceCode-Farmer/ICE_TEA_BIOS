## @file
#
#******************************************************************************
#* Copyright (c) 2019, Insyde Software Corporation. All Rights Reserved.
#*
#* You may not reproduce, distribute, publish, display, perform, modify, adapt,
#* transmit, broadcast, present, recite, release, license or otherwise exploit
#* any part of this publication in any form, by any means, without the prior
#* written permission of Insyde Software Corporation.
#*
#******************************************************************************
## @file
# Component information file for AlderLake Multi-Board Initialization in PEI post memory phase.
#
#@copyright
#  INTEL CONFIDENTIAL
#  Copyright 2017 - 2021 Intel Corporation.
#
#  The source code contained or described herein and all documents related to the
#  source code ("Material") are owned by Intel Corporation or its suppliers or
#  licensors. Title to the Material remains with Intel Corporation or its suppliers
#  and licensors. The Material may contain trade secrets and proprietary and
#  confidential information of Intel Corporation and its suppliers and licensors,
#  and is protected by worldwide copyright and trade secret laws and treaty
#  provisions. No part of the Material may be used, copied, reproduced, modified,
#  published, uploaded, posted, transmitted, distributed, or disclosed in any way
#  without Intel's prior express written permission.
#
#  No license under any patent, copyright, trade secret or other intellectual
#  property right is granted to or conferred upon you by disclosure or delivery
#  of the Materials, either expressly, by implication, inducement, estoppel or
#  otherwise. Any license under such intellectual property rights must be
#  express and approved by Intel in writing.
#
#  Unless otherwise agreed by Intel in writing, you may not remove or alter
#  this notice or any other notice embedded in Materials by Intel or
#  Intel's suppliers or licensors in any way.
#
#  This file contains a 'Sample Driver' and is licensed as such under the terms
#  of your license agreement with Intel or your vendor. This file may be modified
#  by the user, subject to the additional terms of the license agreement.
#
# @par Specification Reference:
#
# @par Glossary:
###

[Defines]
  INF_VERSION                    = 0x00010005
  BASE_NAME                      = PeiAdlPSimicsMultiBoardInitLib
  FILE_GUID                      = C7D39F17-E5BA-41D9-8DFE-FF9017499280
  MODULE_TYPE                    = BASE
  VERSION_STRING                 = 1.0
  LIBRARY_CLASS                  = NULL
  CONSTRUCTOR                    = PeiAdlPSimicsMultiBoardInitLibConstructor

[LibraryClasses]
  BaseLib
  DebugLib
  BaseMemoryLib
  MemoryAllocationLib
  PcdLib
  SiliconInitLib
  MultiBoardInitSupportLib
  PciSegmentLib
  PreSiliconEnvDetectLib
  GpioLib
  HobLib
  BoardConfigLib
#[-start-191001-16990100-add]#
  PeiOemSvcChipsetLibDefault
#[-end-191001-16990100-add]#

[Packages]
  MinPlatformPkg/MinPlatformPkg.dec
  AlderLakeBoardPkg/BoardPkg.dec
  MdePkg/MdePkg.dec
  MdeModulePkg/MdeModulePkg.dec
  ClientOneSiliconPkg/SiPkg.dec
  AlderLakePlatSamplePkg/PlatformPkg.dec
#[-start-191001-16990100-add]#
  SioDummyPkg/SioDummyPkg.dec
  InsydeModulePkg/InsydeModulePkg.dec
#[-end-191001-16990100-add]#
#[-start-191218-IB16740000-add]#
  $(CHIPSET_PKG)/$(CHIPSET_PKG).dec
#[-end-191218-IB16740000-add]#

[Sources]
  PeiInitPostMemLib.c
  PeiMultiBoardInitPostMemLib.c

[FixedPcd]

[Pcd]
  # Board GPIO Table
  gBoardModuleTokenSpaceGuid.PcdBoardGpioTable
  gBoardModuleTokenSpaceGuid.PcdSataPortsEnable0
  gBoardModuleTokenSpaceGuid.PcdPreferredPmProfile
  gBoardModuleTokenSpaceGuid.PcdPlatformFlavor            ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdMipiCamGpioEnable
  gBoardModuleTokenSpaceGuid.PcdBoardPmcPdEnable

  # Usb Port Mapping PCDs
  gBoardModuleTokenSpaceGuid.PcdUsbTypeCSupport
  gBoardModuleTokenSpaceGuid.PcdTypeCPortsSupported
  gBoardModuleTokenSpaceGuid.PcdUsbTypeCPort1
  gBoardModuleTokenSpaceGuid.PcdUsbTypeCPort1Pch
  gBoardModuleTokenSpaceGuid.PcdUsbCPort1Properties
  gBoardModuleTokenSpaceGuid.PcdUsbTypeCPort2
  gBoardModuleTokenSpaceGuid.PcdUsbTypeCPort2Pch
  gBoardModuleTokenSpaceGuid.PcdUsbCPort2Properties
  gBoardModuleTokenSpaceGuid.PcdUsbTypeCPort3
  gBoardModuleTokenSpaceGuid.PcdUsbTypeCPort3Pch
  gBoardModuleTokenSpaceGuid.PcdUsbCPort3Properties
  gBoardModuleTokenSpaceGuid.PcdUsbTypeCPort4
  gBoardModuleTokenSpaceGuid.PcdUsbTypeCPort4Pch
  gBoardModuleTokenSpaceGuid.PcdUsbCPort4Properties

  # CPU USB 3.0 Port Over Current Pin
  gBoardModuleTokenSpaceGuid.PcdCpuUsb30OverCurrentPinTable
  gBoardModuleTokenSpaceGuid.VpdPcdCpuUsb3OcMap
  #Tbt config
  gBoardModuleTokenSpaceGuid.PcdTcssPdType                     ## PRODUCE

  #Misc Config
  gBoardModuleTokenSpaceGuid.PcdSataLedEnable
  gBoardModuleTokenSpaceGuid.PcdVrAlertEnable
  gBoardModuleTokenSpaceGuid.PcdPchThermalHotEnable
  gBoardModuleTokenSpaceGuid.PcdMemoryThermalSensorGpioCPmsyncEnable
  gBoardModuleTokenSpaceGuid.PcdMemoryThermalSensorGpioDPmsyncEnable
  gBoardModuleTokenSpaceGuid.PcdBoardBomId

  #HDA config
  gBoardModuleTokenSpaceGuid.PcdHdaVerbTableDatabase

  #TouchPanel Config
  gBoardModuleTokenSpaceGuid.PcdBoardGpioTableTouchPanel1
  gBoardModuleTokenSpaceGuid.PcdBoardGpioTableTouchPanel1Size
  gBoardModuleTokenSpaceGuid.PcdBoardGpioTableTouchPanel2
  gBoardModuleTokenSpaceGuid.PcdBoardGpioTableTouchPanel2Size

  #CVF Config
  gBoardModuleTokenSpaceGuid.PcdBoardGpioTableCvf
  gBoardModuleTokenSpaceGuid.PcdBoardGpioTableCvfSize

  # TSN Config
  gBoardModuleTokenSpaceGuid.PcdBoardGpioTableTsnDevice
  gBoardModuleTokenSpaceGuid.PcdBoardGpioTableTsnDeviceSize

  # Pcd Hook Function
  gBoardModuleTokenSpaceGuid.PcdFuncPeiBoardSpecificInitPostMem

  # PCIE SLOT 1 - X4 CONNECTOR
  gBoardModuleTokenSpaceGuid.PcdPcieSlot1WakeGpioPin
  gBoardModuleTokenSpaceGuid.PcdPcieSlot1RootPort
  gBoardModuleTokenSpaceGuid.PcdDg1VramSRGpio

  # PCIE SLOT 2 - X1 CONNECTOR
  gBoardModuleTokenSpaceGuid.PcdPchPCIeSlot2PwrEnableGpioNo
  gBoardModuleTokenSpaceGuid.PcdPchPCIeSlot2PwrEnableGpioPolarity
  gBoardModuleTokenSpaceGuid.PcdPchPCIeSlot2RstGpioNo
  gBoardModuleTokenSpaceGuid.PcdPchPCIeSlot2RstGpioPolarity
  gBoardModuleTokenSpaceGuid.PcdPcieSlot2WakeGpioPin
  gBoardModuleTokenSpaceGuid.PcdPcieSlot2RootPort

  # PCH M.2 SSD RTD3
  gBoardModuleTokenSpaceGuid.PcdPchSsd1PwrEnableGpioNo
  gBoardModuleTokenSpaceGuid.PcdPchSsd1PwrEnableGpioPolarity
  gBoardModuleTokenSpaceGuid.PcdPchSsd1RstGpioNo
  gBoardModuleTokenSpaceGuid.PcdPchSsd1RstGpioPolarity

  # PCH SATA port RTD3
  gBoardModuleTokenSpaceGuid.PcdPchSataPortPwrEnableGpioNo
  gBoardModuleTokenSpaceGuid.PcdPchSataPortPwrEnableGpioPolarity

  # PCIe x4 M.2 SSD RTD3
  gBoardModuleTokenSpaceGuid.PcdPcieSsd2PwrEnableGpioNo
  gBoardModuleTokenSpaceGuid.PcdPcieSsd2RstGpioNo
  gBoardModuleTokenSpaceGuid.PcdPcieSsd2PwrEnableGpioPolarity
  gBoardModuleTokenSpaceGuid.PcdPcieSsd2RstGpioPolarity

  #Pch Pcie x4 SLOT
  gBoardModuleTokenSpaceGuid.PcdPcieSlot1GpioSupport
  gBoardModuleTokenSpaceGuid.PcdPcieSlot1HoldRstExpanderNo
  gBoardModuleTokenSpaceGuid.PcdPcieSlot1HoldRstGpioNo
  gBoardModuleTokenSpaceGuid.PcdPcieSlot1HoldRstGpioPolarity
  gBoardModuleTokenSpaceGuid.PcdPcieSlot1PwrEnableExpanderNo
  gBoardModuleTokenSpaceGuid.PcdPcieSlot1PwrEnableGpioNo
  gBoardModuleTokenSpaceGuid.PcdPcieSlot1PwrEnableGpioPolarity
  gBoardModuleTokenSpaceGuid.PcdPcieSsd2RstGpioPolarity

  # WLAN
  gBoardModuleTokenSpaceGuid.PcdWlanWakeGpio
  gBoardModuleTokenSpaceGuid.PcdWwanSourceClock

  # I2C Touch Panel 0 & 1 RTD3
  gBoardModuleTokenSpaceGuid.PcdTouchpanelPwrEnableGpio
  gBoardModuleTokenSpaceGuid.PcdTouchpanelRstGpio
  gBoardModuleTokenSpaceGuid.PcdTouchpanelPwrEnableGpioPolarity
  gBoardModuleTokenSpaceGuid.PcdTouchpanelRstGpioPolarity
  gBoardModuleTokenSpaceGuid.PcdTouchpadIrqGpioPolarity
  gBoardModuleTokenSpaceGuid.PcdTouchpanelIrqGpioPolarity
  gBoardModuleTokenSpaceGuid.PcdTouchpadIrqGpio
  gBoardModuleTokenSpaceGuid.PcdTouchpanelIrqGpio
  gBoardModuleTokenSpaceGuid.PcdTouchpanel1PwrEnableGpio
  gBoardModuleTokenSpaceGuid.PcdTouchpanel1RstGpio
  gBoardModuleTokenSpaceGuid.PcdTouchpanel1IrqGpio
  gBoardModuleTokenSpaceGuid.PcdTouchpanel1PwrEnableGpioPolarity
  gBoardModuleTokenSpaceGuid.PcdTouchpanel1RstGpioPolarity
  gBoardModuleTokenSpaceGuid.PcdTouchpanel1IrqGpioPolarity
  gBoardModuleTokenSpaceGuid.PcdEcSmiGpio
  gBoardModuleTokenSpaceGuid.PcdEcLowPowerExitGpio

  gBoardModuleTokenSpaceGuid.PcdBoardId
  gBoardModuleTokenSpaceGuid.PcdSkuType
  gBoardModuleTokenSpaceGuid.VpdPcdBoardGpioTable
