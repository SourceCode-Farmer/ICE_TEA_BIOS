/** @file

;******************************************************************************
;* Copyright (c) 2020, Insyde Software Corporation. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/
/** @file

@copyright
  INTEL CONFIDENTIAL
  Copyright 2017 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains a 'Sample Driver' and is licensed as such under the terms
  of your license agreement with Intel or your vendor. This file may be modified
  by the user, subject to the additional terms of the license agreement.

@par Specification Reference:
**/

#include <PiPei.h>
#include <Library/BaseMemoryLib.h>
#include <Library/BiosIdLib.h>
#include <Library/PcdLib.h>
#include <Library/DebugLib.h>
#include <Library/PciLib.h>
#include <Library/MemoryAllocationLib.h>
#include <Library/PeiLib.h>
#include <Guid/MemoryOverwriteControl.h>
#include <Library/MmioInitLib.h>
#include <PlatformBoardConfig.h>
#include <Library/SiliconInitLib.h>
#include <Library/PchCycleDecodingLib.h>
#include <Register/PmcRegs.h>
#include <Library/PmcLib.h>
#include <Library/PeiBootModeLib.h>
#include <Ppi/ReadOnlyVariable2.h>
#include <Library/PeiServicesLib.h>
#include <Library/WakeupEventLib.h>
#include <Library/GpioLib.h>
#include <Library/BoardConfigLib.h>
#include <Library/TimerLib.h>
#include <AlderLakePBoardConfigPatchTable.h>
#include <PlatformBoardId.h>
#include <SioRegs.h>
#include <Library/IoLib.h>
#include <Pins/GpioPinsVer2Lp.h>
//[-start-191001-16990100-add]//
#include <Library/PeiOemSvcChipsetLib.h>
//[-end-191001-16990100-add]//

#define SIO_RUNTIME_REG_BASE_ADDRESS      0x0680

static EFI_PEI_PPI_DESCRIPTOR mSetupVariablesReadyPpi = {
  (EFI_PEI_PPI_DESCRIPTOR_PPI | EFI_PEI_PPI_DESCRIPTOR_TERMINATE_LIST),
  &gSetupVariablesReadyPpiGuid,
  NULL
};

/**
  Alderlake P boards configuration init function for PEI pre-memory phase.

  @retval EFI_SUCCESS             The function completed successfully.
**/
EFI_STATUS
EFIAPI
AdlPSimicsInitPreMem (
  VOID
  )
{
  EFI_STATUS                        Status;
  UINTN                             VariableSize;
  VOID                              *MemorySavedData;
  UINT8                             MorControl;
  VOID                              *MorControlPtr;
  EFI_PEI_READ_ONLY_VARIABLE2_PPI   *VariableServices;

  Status = PeiServicesLocatePpi (
             &gEfiPeiReadOnlyVariable2PpiGuid,
             0,
             NULL,
             (VOID **) &VariableServices
             );
  ASSERT_EFI_ERROR (Status);
  //
  // Initialize S3 Data variable (S3DataPtr). It may be used for warm and fast boot paths.
  //
  VariableSize = 0;
  MemorySavedData = NULL;
  Status = VariableServices->GetVariable (
                               VariableServices,
                               L"MemoryConfig",
                               &gFspNonVolatileStorageHobGuid,
                               NULL,
                               &VariableSize,
                               MemorySavedData
                               );
  if (Status == EFI_BUFFER_TOO_SMALL) {
    //
    // Set the DISB bit
    // after memory Data is saved to NVRAM.
    //
    PmcSetDramInitScratchpad ();
  }

  //
  // MOR
  //
  MorControl = 0;
  MorControlPtr = &MorControl;
  VariableSize = sizeof (MorControl);
  Status = PeiGetVariable (
             MEMORY_OVERWRITE_REQUEST_VARIABLE_NAME,
             &gEfiMemoryOverwriteControlDataGuid,
             &MorControlPtr,
             &VariableSize
             );
  DEBUG ((DEBUG_INFO, "MorControl - 0x%x (%r)\n", MorControl, Status));
  if (MOR_CLEAR_MEMORY_VALUE (MorControl)) {
    PcdSet8S (PcdCleanMemory, MorControl & MOR_CLEAR_MEMORY_BIT_MASK);
  }

  PcdSet32S (PcdStackBase, PcdGet32 (PcdTemporaryRamBase) + PcdGet32 (PcdTemporaryRamSize) - (PcdGet32 (PcdFspTemporaryRamSize) + PcdGet32 (PcdFspReservedBufferSize)));
  PcdSet32S (PcdStackSize, PcdGet32 (PcdFspTemporaryRamSize));

  PcdSet8S (PcdCpuRatio, 0x0);
  PcdSet8S (PcdBiosGuard, 0x0);

  return EFI_SUCCESS;
}

/**
  Updates the wakeupType.
**/
VOID
AdlPSimicsWakeUpTypeUpdate (
  VOID
  )
{
  UINT8   WakeupType;
  //
  // Updates the wakeupType which will be used to update the same in Smbios table 01
  //
  GetWakeupEvent (&WakeupType);
  PcdSet8S (PcdWakeupType, WakeupType);
}

VOID
AdlPSimicsMrcConfigInit (
  VOID
  );

VOID
AdlPSimicsSaMiscConfigInit (
  VOID
  );

VOID
AdlPSimicsSaGpioConfigInit (
  VOID
  );

VOID
AdlPSimicsSaDisplayConfigInit (
  VOID
  );

VOID
AdlPSimicsSaUsbConfigInit (
  VOID
  );

EFI_STATUS
AdlPSimicsRootPortClkInfoInit (
  VOID
  );

EFI_STATUS
AdlPSimicsUsbConfigInit (
  VOID
  );

VOID
AdlPSimicsGpioTablePreMemInit(
  VOID
  );

VOID
AdlPSimicsGpioGroupTierInit (
  VOID
  );

/**
  HSIO init function for PEI pre-memory phase.
**/
VOID
AdlPSimicsHsioInit (
  VOID
  )
{
}

/**
  Configure Super IO
**/
STATIC
VOID
AdlPSimicsSioInit (
  VOID
  )
{
  //
  // Program and Enable Default Super IO Configuration Port Addresses and range
  //
  PchLpcGenIoRangeSet (PcdGet16 (PcdLpcSioConfigDefaultPort) & (~0xF), 0x10);

  //
  // Enable LPC decode for KCS and mailbox SIO for iBMC communication
  //
  if (PcdGet8 (PcdPlatformFlavor) == FlavorUpServer) {
    PchLpcGenIoRangeSet (BMC_KCS_BASE_ADDRESS, 0x10);
    PchLpcGenIoRangeSet (PILOTIII_MAILBOX_BASE_ADDRESS, 0x10);
  } else {
  //
  // 128 Byte Boundary and SIO Runtime Register Range is 0x0 to 0xF;
  //
    PchLpcGenIoRangeSet (SIO_RUNTIME_REG_BASE_ADDRESS  & (~0x7F), 0x10);
  }

  //
  // We should not depend on SerialPortLib to initialize KBC for legacy USB
  // So initialize KBC for legacy USB driver explicitly here.
  // After we find how to enable mobile KBC, we will add enabling code for mobile then.
  //
  if ((PcdGet8 (PcdPlatformFlavor) == FlavorDesktop) ||
      (PcdGet8 (PcdPlatformFlavor) == FlavorWorkstation)) {
    //
    // Enable KBC for National PC8374 SIO
    //
    if (PcdGetBool (PcdPc8374SioKbcPresent)) {
      IoWrite8 (PcdGet16 (PcdDesktopLpcSioIndexDefaultPort), 0x07);
      IoWrite8 (PcdGet16 (PcdDesktopLpcSioDataDefaultPort), 0x06);
      IoWrite8 (PcdGet16 (PcdDesktopLpcSioIndexDefaultPort), 0x30);
      IoWrite8 (PcdGet16 (PcdDesktopLpcSioDataDefaultPort), 0x01);
    }
  }

  return;
}

/**
  Notifies the gPatchConfigurationDataPreMemPpiGuid has been Installed

  @param[in] PeiServices          General purpose services available to every PEIM.
  @param[in] NotifyDescriptor     The notification structure this PEIM registered on install.
  @param[in] Ppi                  The memory discovered PPI.  Not used.

  @retval EFI_SUCCESS             The function completed successfully.
**/
EFI_STATUS
EFIAPI
AdlPSimicsBoardPatchConfigurationDataPreMemCallback (
  IN EFI_PEI_SERVICES           **PeiServices,
  IN EFI_PEI_NOTIFY_DESCRIPTOR  *NotifyDescriptor,
  IN VOID                       *Ppi
  )
{
//[-start-200225-IB14630334-remove]//
//  UINTN   ConfigPatchStructSize;
  UINT16    BoardId;
  BoardId = PcdGet16(PcdBoardId);

  switch (BoardId) {
/**  //
  // **/
    case BoardIdAdlPSimics:
//
//  ConfigPatchStructSize = SIZE_OF_TABLE (mAlderLakeSDdr4RvpConfigPatchStruct, CONFIG_PATCH_STRUCTURE);
//  PatchSetupConfigurationDataPreMem (mAlderLakeSDdr4RvpConfigPatchStruct, ConfigPatchStructSize);
//[-end-200225-IB14630334-remove]//
      break;
    default:
      break;
  }

  PeiServicesInstallPpi (&mSetupVariablesReadyPpi);

  return RETURN_SUCCESS;
}

/**
  Board Misc init function for PEI pre-memory phase.
**/
VOID
AdlPSimicsBoardMiscInitPreMem (
  VOID
  )
{
  PCD64_BLOB PcdData;
  UINT16    BoardId;
  BoardId = PcdGet16(PcdBoardId);

  //
  // RecoveryMode GPIO
  //
  PcdData.Blob = 0;
  PcdData.BoardGpioConfig.Type = BoardGpioTypeNotSupported;
  PcdSet64S (PcdRecoveryModeGpio, PcdData.Blob);

  //
  // OddPower Init
  //
  PcdSetBoolS (PcdOddPowerInitEnable, FALSE);

  //
  // Pc8374SioKbc Present
  //
  PcdSetBoolS (PcdPc8374SioKbcPresent, FALSE);

  //
  // Smbus Alert function Init.
  //
  PcdSetBoolS (PcdSmbusAlertEnable, FALSE);


  //
  // Configure WWAN Full Card Power Off and reset pins
  //
  switch (BoardId) {
    case BoardIdAdlPSimics:
      PcdSet32S (PcdWwanFullCardPowerOffGpio, GPIO_VER2_LP_GPP_C11);
      PcdSet32S (PcdWwanBbrstGpio, GPIO_VER2_LP_GPP_C10);
      PcdSet32S (PcdWwanPerstGpio, GPIO_VER2_LP_GPP_B17);
      PcdSet32S (PcdWwanWakeGpio, GPIO_VER2_LP_GPP_C9);
      PcdSetBoolS (PcdWwanPerstGpioPolarity, 0);
      PcdSetBoolS (PcdWwanFullCardPowerOffGpioPolarity, PIN_GPIO_ACTIVE_HIGH);
      PcdSetBoolS (PcdWwanBbrstGpioPolarity, PIN_GPIO_ACTIVE_LOW);
      PcdSet8S (PcdWwanSourceClock, 2);
      break;
  }

  //EC FailSafe Cpu Temp and Fan Speed Setting
  PcdSet8S (PcdEcFailSafeActionCpuTemp, 85);
  PcdSet8S (PcdEcFailSafeActionFanPwm, 65);
}

/**
  A hook for board-specific initialization prior to memory initialization.

  @retval EFI_SUCCESS   The board initialization was successful.
**/
EFI_STATUS
EFIAPI
AdlPSimicsBoardInitBeforeMemoryInit (
  VOID
  )
{
  EFI_STATUS        Status;
//[-start-200225-IB14630334-add]//
  EFI_STATUS	      OemSvcStatus;
//[-end-200225-IB14630334-add]//
//[-start-191001-16990100-add]//
  UINT16            GpioTableSizePreMem;
  GPIO_INIT_CONFIG  *GpioTablePreMem;
//[-end-191001-16990100-add]//

  DEBUG ((DEBUG_INFO, "AdlPBoardInitBeforeMemoryInit\n"));

  //[-start-191111-IB10189001-remove]//
  // GetBiosId (NULL);
  //[-end-191111-IB10189001-remove]//

  AdlPSimicsInitPreMem ();

  AdlPSimicsWakeUpTypeUpdate ();
  AdlPSimicsBoardMiscInitPreMem ();
  AdlPSimicsSioInit ();

  ///
  /// Do basic PCH init
  ///
  SiliconInit ();

  AdlPSimicsGpioGroupTierInit ();
  AdlPSimicsGpioTablePreMemInit ();

  AdlPSimicsHsioInit ();
  AdlPSimicsMrcConfigInit ();
  AdlPSimicsSaGpioConfigInit ();
  AdlPSimicsSaMiscConfigInit ();
  Status = AdlPSimicsRootPortClkInfoInit ();
  Status = AdlPSimicsUsbConfigInit ();
  AdlPSimicsSaDisplayConfigInit ();
  AdlPSimicsSaUsbConfigInit ();

  // Configure GPIO Before Memory is not ready
  // WA #26 --
  //GpioInit (PcdGetPtr (PcdBoardGpioTablePreMem));
//[-start-200225-IB14630334-modify]//
//[-start-191001-16990100-add]//
  GpioTablePreMem = PcdGetPtr (PcdBoardGpioTablePreMem);
  GetGpioTableSize (GpioTablePreMem, &GpioTableSizePreMem);
  DEBUG_OEM_SVC ((DEBUG_INFO, "Pei OemChipsetServices Call: OemSvcModifyGpioSettingTablePreMem \n"));
  OemSvcStatus = OemSvcModifyGpioSettingTablePreMem (&GpioTablePreMem, &GpioTableSizePreMem);
  DEBUG_OEM_SVC ((DEBUG_INFO, "Pei OemChipsetServices OemSvcModifyGpioSettingTablePreMem Status: %r\n", OemSvcStatus));
  // Configure GPIO Before Memory is not ready
  if (GpioTableSizePreMem != 0 && OemSvcStatus != EFI_SUCCESS) {
    GpioConfigurePads ((UINT32) GpioTableSizePreMem, GpioTablePreMem);
  }
//[-end-191001-16990100-add]//
//[-end-200225-IB14630334-modify]//

  // Configure GPIO group GPE tier
  GpioGroupTierInit ();

  ASSERT(Status == EFI_SUCCESS);
  return EFI_SUCCESS;
}

/**
  A hook for board-specific initialization after memory initialization.

  @retval EFI_SUCCESS   The board initialization was successful.
**/
EFI_STATUS
EFIAPI
AdlPSimicsBoardInitAfterMemoryInit(
  VOID
  )
{
  DEBUG ((DEBUG_INFO, "AdlPSimicsBoardInitAfterMemoryInit\n"));

  return MmioInit ();
}

/**
  This board service initializes board-specific debug devices.

  @retval EFI_SUCCESS   Board-specific debug initialization was successful.
**/
EFI_STATUS
EFIAPI
AdlPSimicsBoardDebugInit (
  VOID
  )
{
  DEBUG ((DEBUG_INFO, "AdlPBoardDebugInit\n"));

  return EFI_SUCCESS;
}

/**
  This board service detects the boot mode.

  @retval EFI_BOOT_MODE The boot mode.
**/
EFI_BOOT_MODE
EFIAPI
AdlPSimicsBoardBootModeDetect (
  VOID
  )
{
  EFI_BOOT_MODE                             BootMode;

  DEBUG ((DEBUG_INFO, "AdlPSimicsBoardBootModeDetect\n"));
  BootMode = DetectBootMode ();
  DEBUG ((DEBUG_INFO, "BootMode: 0x%02X\n", BootMode));

  return BootMode;
}
