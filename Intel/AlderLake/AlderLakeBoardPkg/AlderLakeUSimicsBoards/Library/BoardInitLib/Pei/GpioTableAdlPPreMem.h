/** @file
  Alderlake P RVP GPIO definition table for Pre-Memory Initialization

@copyright
  Copyright (c) 2018 - 2020 Intel Corporation. All rights reserved
  This software and associated documentation (if any) is furnished
  under a license and may only be used or copied in accordance
  with the terms of the license. Except as permitted by such
  license, no part of this software or documentation may be
  reproduced, stored in a retrieval system, or transmitted in any
  form or by any means without the express written consent of
  Intel Corporation.
  This file contains an 'Intel Peripheral Driver' and is
  licensed for Intel CPUs and chipsets under the terms of your
  license agreement with Intel or your vendor. This file may
  be modified by the user, subject to additional terms of the
  license agreement.
**/
#ifndef _ALDERLAKE_P_PREMEM_GPIO_TABLE_H_
#define _ALDERLAKE_P_PREMEM_GPIO_TABLE_H_

#include <Pins/GpioPinsVer2Lp.h>
#include <Library/GpioLib.h>
#include <Library/GpioConfig.h>

///
/// !!! GPIOs designated to Native Functions shall not be configured by Platform Code.
/// Native Pins shall be configured by Silicon Code (based on BIOS policies setting) or soft straps(set by CSME in FITc).
/// Configuring pins to native function in GPIO table would override the pin settings resulting in unexpected behavior.
///

GLOBAL_REMOVE_IF_UNREFERENCED GPIO_INIT_CONFIG mGpioTablePreMemAdlPDdr4[] =
{
  { GPIO_VER2_LP_GPP_A14, { GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut, GpioOutLow, GpioIntDis, GpioPlatformReset, GpioTermNone } },  //ONBOARD_X4_PCIE_SLOT1_PWREN_N
  { GPIO_VER2_LP_GPP_C13, { GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut, GpioOutHigh, GpioIntDis, GpioPlatformReset, GpioTermNone } },  //ONBOARD_X4_PCIE_SLOT1_RESET_N
  // CPU M.2 SSD
  { GPIO_VER2_LP_GPP_D16, { GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut, GpioOutHigh, GpioIntDis, GpioPlatformReset, GpioTermNone } },  //CPU SSD PWREN
  { GPIO_VER2_LP_GPP_A11, { GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut, GpioOutHigh, GpioIntDis, GpioPlatformReset, GpioTermNone } },  //CPU SSD RESET
  {GPIO_VER2_LP_GPP_A23,  { GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut, GpioOutLow,  GpioIntDis,GpioPlatformReset,  GpioTermNone}},  //TC_RETIMER_FORCE_PWR
  {0x0}
};

GLOBAL_REMOVE_IF_UNREFERENCED GPIO_INIT_CONFIG mGpioTableAdlPDdr4WwanOnEarlyPreMem[] =
{
  {GPIO_VER2_LP_GPP_H23,  { GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,    GpioOutHigh,     GpioIntDis,               GpioResumeReset,    GpioTermNone,  GpioOutputStateUnlock}},  //WWAN_PWREN
  {GPIO_VER2_LP_GPP_C11,  { GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,    GpioOutHigh,     GpioIntDis,               GpioResumeReset,    GpioTermNone,  GpioOutputStateUnlock}},  //WWAN_FCP_OFF_N
  {GPIO_VER2_LP_GPP_C10,  { GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,    GpioOutHigh,     GpioIntDis,               GpioResumeReset,    GpioTermNone,  GpioOutputStateUnlock}},  //WWAN_RST_N
  {GPIO_VER2_LP_GPP_B17,  { GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,    GpioOutHigh,     GpioIntDis,               GpioPlatformReset,  GpioTermNone,  GpioOutputStateUnlock}},  //WWAN_PERST_N
  {GPIO_VER2_LP_GPP_C9,   { GpioPadModeGpio, GpioHostOwnGpio, GpioDirInInv,  GpioOutDefault,  GpioIntLevel|GpioIntSci,  GpioHostDeepReset,  GpioTermNone,  GpioPadConfigUnlock}},    //WWAN_WAKE_N
  {GPIO_VER2_LP_GPP_D15,  { GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,    GpioOutHigh,     GpioIntDis,               GpioResumeReset,    GpioTermNone,  GpioOutputStateUnlock}},  //WWAN_DISABLE_N
};

GLOBAL_REMOVE_IF_UNREFERENCED GPIO_INIT_CONFIG mGpioTableAdlPDdr4WwanOffEarlyPreMem[] =
{
  {GPIO_VER2_LP_GPP_C10,  { GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,    GpioOutLow,      GpioIntDis,               GpioResumeReset,    GpioTermNone,  GpioOutputStateUnlock}},  //WWAN_RST_N
  {GPIO_VER2_LP_GPP_B17,  { GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,    GpioOutLow,      GpioIntDis,               GpioResumeReset,    GpioTermNone,  GpioOutputStateUnlock}},  //WWAN_PERST_N
  {GPIO_VER2_LP_GPP_C11,  { GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,    GpioOutLow,      GpioIntDis,               GpioResumeReset,    GpioTermNone,  GpioOutputStateUnlock}},  //WWAN_FCP_OFF_N
};

#endif // _ALDERLAKE_P_PREMEM_GPIO_TABLE_H_
