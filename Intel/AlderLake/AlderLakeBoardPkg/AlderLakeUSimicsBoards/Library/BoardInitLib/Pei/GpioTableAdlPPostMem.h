/** @file
  GPIO definition table for AlderLake P

@copyright
  Copyright (c) 2018 - 2021 Intel Corporation. All rights reserved
  This software and associated documentation (if any) is furnished
  under a license and may only be used or copied in accordance
  with the terms of the license. Except as permitted by such
  license, no part of this software or documentation may be
  reproduced, stored in a retrieval system, or transmitted in any
  form or by any means without the express written consent of
  Intel Corporation.
  This file contains an 'Intel Peripheral Driver' and is
  licensed for Intel CPUs and chipsets under the terms of your
  license agreement with Intel or your vendor. This file may
  be modified by the user, subject to additional terms of the
  license agreement.
**/
#ifndef _ALDERLAKE_P_GPIO_TABLE_H_
#define _ALDERLAKE_P_GPIO_TABLE_H_

#include <Pins/GpioPinsVer2Lp.h>
#include <Library/GpioLib.h>
#include <Library/GpioConfig.h>

///
/// !!! GPIOs designated to Native Functions shall not be configured by Platform Code.
/// Native Pins shall be configured by Silicon Code (based on BIOS policies setting) or soft straps(set by CSME in FITc).
/// Configuring pins to native function in GPIO table would override the pin settings resulting in unexpected behavior.
///

STATIC GPIO_INIT_CONFIG mAdlPTouchPanel1GpioTable[] =
{
  // Touch Panel 1, Same pins shared between THC and I2C based Panel,
  {GPIO_VER2_LP_GPP_H14, {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutHigh,   GpioIntDis,GpioPlatformReset,  GpioTermNone,  GpioPadConfigUnlock}},  //TCH_PNL_PWR_EN
  {GPIO_VER2_LP_GPP_E6,  {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutHigh,   GpioIntDis,GpioPlatformReset,  GpioTermNone,  GpioPadConfigUnlock}},  //THC0_RST
  {GPIO_VER2_LP_GPP_E17, {GpioPadModeGpio, GpioHostOwnGpio, GpioDirInInv,  GpioOutDefault,GpioIntEdge|GpioIntApic,GpioPlatformReset,  GpioTermNone,  GpioPadConfigUnlock  }},  //THC0_INT
};

STATIC GPIO_INIT_CONFIG mAdlPCvfGpioTable[] =
{
  {GPIO_VER2_LP_GPP_B23, {GpioPadModeGpio, GpioHostOwnGpio, GpioDirIn,   GpioOutHigh,  GpioIntEdge, GpioPlatformReset,  GpioTermNone, GpioPadConfigUnlock}},  //CVF_HOST_IRQ
  {GPIO_VER2_LP_GPP_R5, {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,  GpioOutLow,   GpioIntDis,  GpioPlatformReset,  GpioTermNone, GpioPadConfigUnlock}},  //CVF_RST_N
  {GPIO_VER2_LP_GPP_B14,  {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,  GpioOutLow,  GpioIntDis,  GpioPlatformReset,  GpioTermNone, GpioPadConfigUnlock}},  //CVF_HOST_WAKE
};

STATIC GPIO_INIT_CONFIG mAdlPTouchPanel2GpioTable[] =
{
  // Touch Panel 2, Not used by default in RVP; Applicable for Converge Mobility RVP SKU only;
  {GPIO_VER2_LP_GPP_F7,  {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutHigh,   GpioIntDis,GpioPlatformReset,  GpioTermNone,  GpioPadConfigUnlock}},  //TCH_PNL2_PWR_EN
  {GPIO_VER2_LP_GPP_F17, {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutHigh,   GpioIntDis,GpioPlatformReset,  GpioTermNone,  GpioPadConfigUnlock}},  //THC1_SPI2_RSTB
  {GPIO_VER2_LP_GPP_F18, {GpioPadModeGpio, GpioHostOwnGpio, GpioDirInInv,  GpioOutDefault,GpioIntEdge|GpioIntApic,GpioPlatformReset,  GpioTermNone,  GpioPadConfigUnlock  }},  //THC1_SPI2_INTB
};

/*static GPIO_INIT_CONFIG mTglTouchPanelGpioTable[] =
{
  // Touch panel specific handling if needed.
  {GPIO_VER2_LP_GPP_H14, {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutHigh,  GpioIntDis, GpioPlatformReset, GpioTermNone,  GpioPadConfigUnlock}},  //TCH_PNL_PWR_EN

};*/
#endif // _ALDERLAKE_P_GPIO_TABLE_H_
