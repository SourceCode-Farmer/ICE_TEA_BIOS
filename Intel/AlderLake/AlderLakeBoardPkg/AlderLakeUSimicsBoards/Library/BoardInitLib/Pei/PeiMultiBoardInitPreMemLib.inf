## @file
#
#******************************************************************************
#* Copyright (c) 2020, Insyde Software Corporation. All Rights Reserved.
#*
#* You may not reproduce, distribute, publish, display, perform, modify, adapt,
#* transmit, broadcast, present, recite, release, license or otherwise exploit
#* any part of this publication in any form, by any means, without the prior
#* written permission of Insyde Software Corporation.
#*
#******************************************************************************
## @file
# Component information file for PEI Alderlake U Board Init Pre-Mem Library
#
#@copyright
#  INTEL CONFIDENTIAL
#  Copyright 2017 - 2021 Intel Corporation.
#
#  The source code contained or described herein and all documents related to the
#  source code ("Material") are owned by Intel Corporation or its suppliers or
#  licensors. Title to the Material remains with Intel Corporation or its suppliers
#  and licensors. The Material may contain trade secrets and proprietary and
#  confidential information of Intel Corporation and its suppliers and licensors,
#  and is protected by worldwide copyright and trade secret laws and treaty
#  provisions. No part of the Material may be used, copied, reproduced, modified,
#  published, uploaded, posted, transmitted, distributed, or disclosed in any way
#  without Intel's prior express written permission.
#
#  No license under any patent, copyright, trade secret or other intellectual
#  property right is granted to or conferred upon you by disclosure or delivery
#  of the Materials, either expressly, by implication, inducement, estoppel or
#  otherwise. Any license under such intellectual property rights must be
#  express and approved by Intel in writing.
#
#  Unless otherwise agreed by Intel in writing, you may not remove or alter
#  this notice or any other notice embedded in Materials by Intel or
#  Intel's suppliers or licensors in any way.
#
#  This file contains a 'Sample Driver' and is licensed as such under the terms
#  of your license agreement with Intel or your vendor. This file may be modified
#  by the user, subject to the additional terms of the license agreement.
#
# @par Specification Reference:
#
# @par Glossary:
###

[Defines]
  INF_VERSION                    = 0x00010005
  BASE_NAME                      = PeiAdlPSimicsMultiBoardInitPreMemLib
  FILE_GUID                      = EA05BD43-136F-45EE-BBBA-27D75817574F
  MODULE_TYPE                    = BASE
  VERSION_STRING                 = 1.0
  LIBRARY_CLASS                  = NULL
  CONSTRUCTOR                    = PeiAdlPSimicsMultiBoardInitPreMemLibConstructor

[LibraryClasses]
  BaseLib
  #[-start-191111-IB10189001-remove]#
  # BiosIdLib
  #[-end-191111-IB10189001-remove]#
  DebugLib
  BaseMemoryLib
  MemoryAllocationLib
  PcdLib
  SiliconInitLib
  MultiBoardInitSupportLib
  PeiLib
  EcMiscLib
  MmioInitLib
  BoardConfigLib
  BoardIdsLib
  PeiBootModeLib
  WakeupEventLib
  PreSiliconEnvDetectLib
  SetupInitLib
  SiliconInitLib
#[-start-191001-16990100-add]#
  PeiOemSvcChipsetLibDefault
#[-end-191001-16990100-add]#
#[-start-200220-IB14630326-add]#
  MultiBoardSupportLib
#[-end-200220-IB14630326-add]#

[Packages]
  MinPlatformPkg/MinPlatformPkg.dec
  AlderLakeBoardPkg/BoardPkg.dec
  MdePkg/MdePkg.dec
  MdeModulePkg/MdeModulePkg.dec
  IntelFsp2Pkg/IntelFsp2Pkg.dec
  ClientOneSiliconPkg/SiPkg.dec
  BoardModulePkg/BoardModulePkg.dec
  AlderLakePlatSamplePkg/PlatformPkg.dec
#[-start-191001-16990100-add]#
  SioDummyPkg/SioDummyPkg.dec
#[-end-191001-16990100-add]#
#[-start-191218-IB16740000-add]#
  $(CHIPSET_PKG)/$(CHIPSET_PKG).dec
#[-end-191218-IB16740000-add]#

[Sources]
  PeiInitPreMemLib.c
  PeiMultiBoardInitPreMemLib.c
  PeiDetect.c
  BoardSaInitPreMemLib.c
  BoardPchInitPreMemLib.c

[Pcd]
  gBoardModuleTokenSpaceGuid.PcdLpcSioConfigDefaultPort

  # SA Misc Config
  gBoardModuleTokenSpaceGuid.PcdMrcSpdData
  gBoardModuleTokenSpaceGuid.PcdMrcSpdData00
  gBoardModuleTokenSpaceGuid.PcdMrcSpdData01
  gBoardModuleTokenSpaceGuid.PcdMrcSpdData10
  gBoardModuleTokenSpaceGuid.PcdMrcSpdData11
  gBoardModuleTokenSpaceGuid.PcdMrcSpdDataSize

  # SPD Address Table
  gBoardModuleTokenSpaceGuid.PcdMrcSpdAddressTable0
  gBoardModuleTokenSpaceGuid.PcdMrcSpdAddressTable1
  gBoardModuleTokenSpaceGuid.PcdMrcSpdAddressTable2
  gBoardModuleTokenSpaceGuid.PcdMrcSpdAddressTable3
  gBoardModuleTokenSpaceGuid.PcdMrcSpdAddressTable4
  gBoardModuleTokenSpaceGuid.PcdMrcSpdAddressTable5
  gBoardModuleTokenSpaceGuid.PcdMrcSpdAddressTable6
  gBoardModuleTokenSpaceGuid.PcdMrcSpdAddressTable7
  gBoardModuleTokenSpaceGuid.PcdMrcSpdAddressTable8
  gBoardModuleTokenSpaceGuid.PcdMrcSpdAddressTable9
  gBoardModuleTokenSpaceGuid.PcdMrcSpdAddressTable10
  gBoardModuleTokenSpaceGuid.PcdMrcSpdAddressTable11
  gBoardModuleTokenSpaceGuid.PcdMrcSpdAddressTable12
  gBoardModuleTokenSpaceGuid.PcdMrcSpdAddressTable13
  gBoardModuleTokenSpaceGuid.PcdMrcSpdAddressTable14
  gBoardModuleTokenSpaceGuid.PcdMrcSpdAddressTable15

  #MRC Config
  gBoardModuleTokenSpaceGuid.PcdMrcDqByteMap
  gBoardModuleTokenSpaceGuid.PcdMrcDqByteMapSize
  gBoardModuleTokenSpaceGuid.PcdMrcRcompResistor
  gBoardModuleTokenSpaceGuid.PcdMrcRcompTarget
  gBoardModuleTokenSpaceGuid.PcdMrcDqPinsInterleavedControl
  gBoardModuleTokenSpaceGuid.PcdMrcDqPinsInterleaved
  gBoardModuleTokenSpaceGuid.PcdMrcDqsMapCpu2DramSize
  gBoardModuleTokenSpaceGuid.PcdMrcDqMapCpu2DramSize
  gBoardModuleTokenSpaceGuid.PcdMrcDqsMapCpu2Dram
  gBoardModuleTokenSpaceGuid.PcdMrcDqMapCpu2Dram
  gBoardModuleTokenSpaceGuid.PcdMrcCaVrefConfig
  gBoardModuleTokenSpaceGuid.PcdSaMiscUserBd
  gBoardModuleTokenSpaceGuid.PcdSaDdrFreqLimit

  #MISC
  gBoardModuleTokenSpaceGuid.PcdRecoveryModeGpio
  gBoardModuleTokenSpaceGuid.PcdOddPowerInitEnable
  gBoardModuleTokenSpaceGuid.PcdPc8374SioKbcPresent
  gBoardModuleTokenSpaceGuid.PcdSmbusAlertEnable
  gBoardModuleTokenSpaceGuid.PcdWakeupType
  gBoardModuleTokenSpaceGuid.PcdSetupEnable

  # USB 2.0 Port Over Current Pin
  gBoardModuleTokenSpaceGuid.PcdUsb2OverCurrentPinTable

  # USB 3.0 Port Over Current Pin
  gBoardModuleTokenSpaceGuid.PcdUsb3OverCurrentPinTable

  #Board Information
  gBoardModuleTokenSpaceGuid.PcdPlatformGeneration
  gBoardModuleTokenSpaceGuid.PcdSpdPresent
  gBoardModuleTokenSpaceGuid.PcdDockAttached
  gBoardModuleTokenSpaceGuid.PcdPlatformType
  gBoardModuleTokenSpaceGuid.PcdPlatformFlavor
  gBoardModuleTokenSpaceGuid.PcdBoardRev
  gBoardModuleTokenSpaceGuid.PcdBoardId
  gBoardModuleTokenSpaceGuid.PcdSkuType
  gBoardModuleTokenSpaceGuid.PcdBoardBomId
  gBoardModuleTokenSpaceGuid.PcdBoardType
  gBoardModuleTokenSpaceGuid.PcdBoardName
  gBoardModuleTokenSpaceGuid.PcdEcPresent
  gEfiMdeModulePkgTokenSpaceGuid.PcdAcpiDefaultOemTableId
  gBoardModuleTokenSpaceGuid.PcdBoardRetimerForcePwrGpio
  gBoardModuleTokenSpaceGuid.VpdPcdBoardGpioTablePreMem
  gBoardModuleTokenSpaceGuid.VpdPcdPcieClkUsageMap
  gBoardModuleTokenSpaceGuid.VpdPcdUSB2OCMap
  gBoardModuleTokenSpaceGuid.VpdPcdUSB3OCMap

  gBoardModuleTokenSpaceGuid.PcdCpuRatio
  gBoardModuleTokenSpaceGuid.PcdBiosGuard

  gIntelFsp2PkgTokenSpaceGuid.PcdTemporaryRamBase              ## CONSUMES
  gIntelFsp2PkgTokenSpaceGuid.PcdTemporaryRamSize              ## CONSUMES
  gIntelFsp2PkgTokenSpaceGuid.PcdFspTemporaryRamSize           ## CONSUMES
  gIntelFsp2PkgTokenSpaceGuid.PcdFspReservedBufferSize         ## CONSUMES

  gBoardModuleTokenSpaceGuid.PcdStackBase
  gBoardModuleTokenSpaceGuid.PcdStackSize
  gBoardModuleTokenSpaceGuid.PcdNvsBufferPtr
  gBoardModuleTokenSpaceGuid.PcdCleanMemory

  #SA GPIO Config
  gBoardModuleTokenSpaceGuid.PcdRootPortIndex

  # PCIE Slot1 (x4 Connector) GPIO PCDs
  gBoardModuleTokenSpaceGuid.PcdPcieSlot1GpioSupport
  gBoardModuleTokenSpaceGuid.PcdPcieSlot1HoldRstExpanderNo
  gBoardModuleTokenSpaceGuid.PcdPcieSlot1HoldRstGpioNo
  gBoardModuleTokenSpaceGuid.PcdPcieSlot1HoldRstGpioPolarity
  gBoardModuleTokenSpaceGuid.PcdPcieSlot1PwrEnableExpanderNo
  gBoardModuleTokenSpaceGuid.PcdPcieSlot1PwrEnableGpioNo
  gBoardModuleTokenSpaceGuid.PcdPcieSlot1PwrEnableGpioPolarity
  gBoardModuleTokenSpaceGuid.PcdPcieSsd2RstGpioPolarity

  # RTD3 PCIE
  gBoardModuleTokenSpaceGuid.PcdPcie0GpioSupport                   ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdPcie0HoldRstExpanderNo             ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdPcie0HoldRstGpioNo                 ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdPcie0HoldRstActive                 ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdPcie0PwrEnableExpanderNo           ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdPcie0PwrEnableGpioNo               ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdPcie0PwrEnableActive               ## CONSUMES

  gBoardModuleTokenSpaceGuid.PcdPcie1GpioSupport                   ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdPcie1HoldRstExpanderNo             ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdPcie1HoldRstGpioNo                 ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdPcie1HoldRstActive                 ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdPcie1PwrEnableExpanderNo           ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdPcie1PwrEnableGpioNo               ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdPcie1PwrEnableActive               ## CONSUMES

  gBoardModuleTokenSpaceGuid.PcdPcie2GpioSupport                   ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdPcie2HoldRstExpanderNo             ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdPcie2HoldRstGpioNo                 ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdPcie2HoldRstActive                 ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdPcie2PwrEnableExpanderNo           ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdPcie2PwrEnableGpioNo               ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdPcie2PwrEnableActive               ## CONSUMES

  gBoardModuleTokenSpaceGuid.PcdPcie3GpioSupport                   ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdPcie3HoldRstExpanderNo             ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdPcie3HoldRstGpioNo                 ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdPcie3HoldRstActive                 ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdPcie3PwrEnableExpanderNo           ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdPcie3PwrEnableGpioNo               ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdPcie3PwrEnableActive               ## CONSUMES

  # PCIe x4 M.2 SSD RTD3
  gBoardModuleTokenSpaceGuid.PcdPcieSsd2PwrEnableGpioNo
  gBoardModuleTokenSpaceGuid.PcdPcieSsd2RstGpioNo
  gBoardModuleTokenSpaceGuid.PcdPcieSsd2PwrEnableGpioPolarity

  #SA USB Config
  gBoardModuleTokenSpaceGuid.PcdCpuXhciPortSupportMap

  #Tbt config
  gBoardModuleTokenSpaceGuid.PcdITbtRootPortNumber

  # PCIe Clock Info
  gBoardModuleTokenSpaceGuid.PcdPcieClock0
  gBoardModuleTokenSpaceGuid.PcdPcieClock1
  gBoardModuleTokenSpaceGuid.PcdPcieClock2
  gBoardModuleTokenSpaceGuid.PcdPcieClock3
  gBoardModuleTokenSpaceGuid.PcdPcieClock4
  gBoardModuleTokenSpaceGuid.PcdPcieClock5
  gBoardModuleTokenSpaceGuid.PcdPcieClock6
  gBoardModuleTokenSpaceGuid.PcdPcieClock7
  gBoardModuleTokenSpaceGuid.PcdPcieClock8
  gBoardModuleTokenSpaceGuid.PcdPcieClock9
  gBoardModuleTokenSpaceGuid.PcdPcieClock10
  gBoardModuleTokenSpaceGuid.PcdPcieClock11
  gBoardModuleTokenSpaceGuid.PcdPcieClock12
  gBoardModuleTokenSpaceGuid.PcdPcieClock13
  gBoardModuleTokenSpaceGuid.PcdPcieClock14
  gBoardModuleTokenSpaceGuid.PcdPcieClock15

  # USB 2.0 PHY Port parameters
  gBoardModuleTokenSpaceGuid.PcdUsb2PhyTuningTable

  # GPIO Group Tier
  gBoardModuleTokenSpaceGuid.PcdGpioGroupToGpeDw0
  gBoardModuleTokenSpaceGuid.PcdGpioGroupToGpeDw1
  gBoardModuleTokenSpaceGuid.PcdGpioGroupToGpeDw2

  # HSIO

  # WWAN Full Card Power Off and reset pins
  gBoardModuleTokenSpaceGuid.PcdWwanFullCardPowerOffGpio
  gBoardModuleTokenSpaceGuid.PcdWwanFullCardPowerOffGpioPolarity
  gBoardModuleTokenSpaceGuid.PcdWwanBbrstGpio
  gBoardModuleTokenSpaceGuid.PcdWwanBbrstGpioPolarity
  gBoardModuleTokenSpaceGuid.PcdWwanPerstGpio
  gBoardModuleTokenSpaceGuid.PcdWwanPerstGpioPolarity
  gBoardModuleTokenSpaceGuid.PcdWwanWakeGpio
  gBoardModuleTokenSpaceGuid.PcdWwanSourceClock

  gBoardModuleTokenSpaceGuid.PcdDisableVpdGpioTable
  gBoardModuleTokenSpaceGuid.PcdBoardGpioTablePreMem
  gBoardModuleTokenSpaceGuid.PcdBoardGpioTableWwanOnEarlyPreMem
  gBoardModuleTokenSpaceGuid.PcdBoardGpioTableWwanOnEarlyPreMemSize
  gBoardModuleTokenSpaceGuid.PcdBoardGpioTableWwanOffEarlyPreMem
  gBoardModuleTokenSpaceGuid.PcdBoardGpioTableWwanOffEarlyPreMemSize

  # Display DDI
  gBoardModuleTokenSpaceGuid.PcdSaDisplayConfigTable           ## PRODUCES
  gBoardModuleTokenSpaceGuid.PcdSaDisplayConfigTableSize       ## PRODUCES

  gPlatformModuleTokenSpaceGuid.PcdDesktopLpcSioDataDefaultPort   ## CONSUMES
  gPlatformModuleTokenSpaceGuid.PcdDesktopLpcSioIndexDefaultPort  ## CONSUMES

  # EC FailSafe Cpu Temp and Fan Speed Setting
  gBoardModuleTokenSpaceGuid.PcdEcFailSafeActionCpuTemp
  gBoardModuleTokenSpaceGuid.PcdEcFailSafeActionFanPwm
##[-start-190906-IB04530031-add]##
  gChipsetPkgTokenSpaceGuid.PcdCrbSkuId
##[-end-190906-IB04530031-add]##

[Guids]
  gFspNonVolatileStorageHobGuid
  gEfiMemoryOverwriteControlDataGuid
  gSetupVariableGuid
  gSaSetupVariableGuid
  gCpuSetupVariableGuid
  gPchSetupVariableGuid
  gMeSetupVariableGuid
  gSiSetupVariableGuid

[Ppis]
  gPatchConfigurationDataPreMemPpiGuid    ## NOTIFY
  gEfiPeiReadOnlyVariable2PpiGuid         ## CONSUMES
  gSetupVariablesReadyPpiGuid             ## PRODUCES
