/** @file
 
;******************************************************************************
;* Copyright 2021 Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corp.
;*
;******************************************************************************
*/
/** @file

@copyright
  INTEL CONFIDENTIAL
  Copyright 2017 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains a 'Sample Driver' and is licensed as such under the terms
  of your license agreement with Intel or your vendor. This file may be modified
  by the user, subject to the additional terms of the license agreement.

@par Specification Reference:
**/

#include <PiPei.h>
#include <Library/DebugLib.h>
#include <Library/PcdLib.h>

#include <Library/SiliconInitLib.h>
#include <Library/PreSiliconEnvDetectLib.h>
#include <Library/BoardConfigLib.h>
#include "GpioTableAdlPPostMem.h"
#include <Library/PeiServicesLib.h>
#include <Library/GpioLib.h>
#include <PlatformBoardId.h>
#include <PlatformBoardConfig.h>
#include <UsbTypeC.h>
#include <Library/PeiHdaVerbTables.h>
//[-start-191001-16990100-add]//
#include <Library/PeiOemSvcChipsetLib.h>
//[-end-191001-16990100-add]//

/**
  Alderlake P boards configuration init function for PEI post memory phase.

  @retval EFI_SUCCESS             The function completed successfully.
**/
EFI_STATUS
EFIAPI
AdlPSimicsInit (
  VOID
  )
{
  UINT16            GpioCount;
  UINTN             Size;
  EFI_STATUS        Status;
  GPIO_INIT_CONFIG  *GpioTable;
  //
  // GPIO Table Init
  //
  Status = EFI_SUCCESS;
  GpioCount = 0;
  Size = 0;
  GpioTable = NULL;
  //
  // GPIO Table Init
  //
  GpioTable = (GPIO_INIT_CONFIG *)PcdGetPtr(VpdPcdBoardGpioTable);
  DEBUG ((DEBUG_INFO, "Simics Environment : GpioTableAdlPDdr4Simics Update\n"));
  GetGpioTableSize (GpioTable, &GpioCount);
  //
  // Increase GpioCount for the zero terminator.
  //
  GpioCount ++;
  Size = (UINTN) (GpioCount * sizeof (GPIO_INIT_CONFIG));
  Status = PcdSetPtrS (PcdBoardGpioTable, &Size, GpioTable);
  ASSERT_EFI_ERROR (Status);

  PcdSet8S (PcdSataPortsEnable0, 0x1);

  return Status;
}

/**
  Board I2C pads termination configuration init function for PEI pre-memory phase.
**/
VOID
AdlPSimicsSerialIoI2cPadsTerminationInit (
  VOID
  )
{
}

/**
  Touch panel GPIO init function for PEI post memory phase.
**/
VOID
AdlPSimicsTouchPanelGpioInit (
  VOID
  )
{
  PcdSet32S (PcdBoardGpioTableTouchPanel1,  (UINTN) mAdlPTouchPanel1GpioTable);
  PcdSet16S (PcdBoardGpioTableTouchPanel1Size,  sizeof(mAdlPTouchPanel1GpioTable)/ sizeof(GPIO_INIT_CONFIG));
  PcdSet32S (PcdBoardGpioTableTouchPanel2,  (UINTN) mAdlPTouchPanel2GpioTable);
  PcdSet16S (PcdBoardGpioTableTouchPanel2Size,  sizeof(mAdlPTouchPanel2GpioTable)/ sizeof(GPIO_INIT_CONFIG));
}

/**
  CVF GPIO init function for PEI post memory phase.
**/
VOID
AdlPSimicsCvfGpioInit (
  VOID
  )
{
  PcdSet32S (PcdBoardGpioTableCvf,  (UINTN) mAdlPCvfGpioTable);
  PcdSet16S (PcdBoardGpioTableCvfSize,  sizeof(mAdlPCvfGpioTable)/ sizeof(GPIO_INIT_CONFIG));
}

/**
  Tsn device GPIO init function for PEI post memory phase.
**/
VOID
AdlPSimicsTsnDeviceGpioInit (
  VOID
  )
{
/*
#if FixedPcdGet8(PcdEmbeddedEnable) == 0x1
  PcdSet32S (PcdBoardGpioTableTsnDevice,  (UINTN) mTglUTsnDeviceGpioTable);
  PcdSet16S (PcdBoardGpioTableTsnDeviceSize,  sizeof(mTglUTsnDeviceGpioTable)/ sizeof(GPIO_INIT_CONFIG));
#endif
*/
}

/**
  HDA VerbTable init function for PEI post memory phase.
**/
VOID
AdlPSimicsHdaVerbTableInit (
  VOID
  )
{
  PcdSet32S (PcdHdaVerbTableDatabase, (UINT32) &HdaVerbTableDbAdlNoDmic);
}

/**
  Misc. init function for PEI post memory phase.
**/
VOID
AdlPSimicsBoardMiscInit (
  VOID
  )
{
  UINT16    BoardId;
  BoardId = PcdGet16(PcdBoardId);
  PcdSetBoolS (PcdSataLedEnable, FALSE);
  PcdSetBoolS (PcdVrAlertEnable, FALSE);
  PcdSetBoolS (PcdPchThermalHotEnable, FALSE);
  PcdSetBoolS (PcdMemoryThermalSensorGpioCPmsyncEnable, FALSE);
  PcdSetBoolS (PcdMemoryThermalSensorGpioDPmsyncEnable, FALSE);
  PcdSet32S (PcdEcSmiGpio, GPIO_VER2_LP_GPP_E7);
  PcdSet32S (PcdEcLowPowerExitGpio,GPIO_VER2_LP_GPP_E8);

  //
  // For PCIe Slot1 - x4 Connector
  //
  PcdSet32S (PcdPcieSlot1WakeGpioPin, GPIO_VER2_LP_GPP_F5);
  PcdSet8S (PcdPcieSlot1RootPort, 5);
  PcdSet32S (PcdDg1VramSRGpio, GPIO_VER2_LP_GPP_F20);

  //
  // For PCIe Slot2 - x1 Connector
  //
  PcdSet32S (PcdPcieSlot2WakeGpioPin, GPIO_VER2_LP_GPP_F4);
  PcdSet32S (PcdPchPCIeSlot2PwrEnableGpioNo, GPIO_VER2_LP_GPP_F9);
  PcdSet32S (PcdPchPCIeSlot2RstGpioNo, GPIO_VER2_LP_GPP_F10);
  PcdSetBoolS (PcdPchPCIeSlot2PwrEnableGpioPolarity, PIN_GPIO_ACTIVE_HIGH);
  PcdSetBoolS (PcdPchPCIeSlot2RstGpioPolarity, PIN_GPIO_ACTIVE_HIGH);
  PcdSet8S (PcdPcieSlot2RootPort, 8);

  switch (BoardId) {
    case BoardIdAdlPSimics:
      //
      // PCH M.2 SSD and Sata port
      //
      PcdSet32S (PcdPchSsd1PwrEnableGpioNo, GPIO_VER2_LP_GPP_B16);  // PCH M.2 SSD power enable gpio pin
      PcdSet32S (PcdPchSsd1RstGpioNo, GPIO_VER2_LP_GPP_H0);  // PCH M.2 SSD reset gpio pin
      PcdSetBoolS (PcdPchSsd1PwrEnableGpioPolarity, PIN_GPIO_ACTIVE_HIGH);  // PCH M.2 SSD power enable gpio pin polarity
      PcdSetBoolS (PcdPchSsd1RstGpioPolarity, PIN_GPIO_ACTIVE_HIGH);  // PCH M.2 SSD reset gpio pin polarity
      PcdSet32S (PcdPchSataPortPwrEnableGpioNo, GPIO_VER2_LP_GPP_B16);  // Sata port power pin
      PcdSetBoolS (PcdPchSataPortPwrEnableGpioPolarity, PIN_GPIO_ACTIVE_HIGH);  // Sata port power pin polarity
      //
      // Touch Pad and Panels 0 & 1
      //
      PcdSet32S (PcdTouchpadIrqGpio, GPIO_VER2_LP_GPP_C8);  // Touch Pad Interrupt Pin
      PcdSetBoolS (PcdTouchpadIrqGpioPolarity, PIN_GPIO_ACTIVE_LOW);  // Touch Pad Interrupt pin polarity
      PcdSet32S (PcdTouchpanelIrqGpio, GPIO_VER2_LP_GPP_E17);  // Touch 0 Interrupt Pin
      PcdSet32S (PcdTouchpanelPwrEnableGpio, GPIO_VER2_LP_GPP_H14);  // Touch 0 power enable pin
      PcdSet32S (PcdTouchpanelRstGpio, GPIO_VER2_LP_GPP_E6);  // Touch 0 reset pin
      PcdSetBoolS (PcdTouchpanelIrqGpioPolarity, PIN_GPIO_ACTIVE_HIGH);  // Touch 0 Interrupt Pin polarity
      PcdSetBoolS (PcdTouchpanelPwrEnableGpioPolarity, PIN_GPIO_ACTIVE_HIGH);  // Touch 0 power enable pin polarity
      PcdSetBoolS (PcdTouchpanelRstGpioPolarity, PIN_GPIO_ACTIVE_HIGH);  // Touch 0 reset pin polarity
      PcdSet32S (PcdTouchpanel1IrqGpio, GPIO_VER2_LP_GPP_F18);  // Touch 1 Interrupt Pin
      PcdSet32S (PcdTouchpanel1PwrEnableGpio, GPIO_VER2_LP_GPP_F7);  // Touch 1 power enable pin
      PcdSet32S (PcdTouchpanel1RstGpio, GPIO_VER2_LP_GPP_F17);  // Touch 1 reset pin
      PcdSetBoolS (PcdTouchpanel1IrqGpioPolarity, PIN_GPIO_ACTIVE_HIGH);  // Touch 1 Interrupt Pin polarity
      PcdSetBoolS (PcdTouchpanel1PwrEnableGpioPolarity, PIN_GPIO_ACTIVE_HIGH);  // Touch 1 power enable pin polarity
      PcdSetBoolS (PcdTouchpanel1RstGpioPolarity, PIN_GPIO_ACTIVE_HIGH);  // Touch 1 reset pin polarity
      //
      // WLAN
      //
      PcdSet32S(PcdWlanWakeGpio, GPIO_VER2_LP_GPP_C23);  // Sata port power pin
      break;
  }
  return;
}

/**
  Tbt init function for PEI post memory phase.
**/
VOID
AdlPSimicsBoardTbtInit (
  VOID
  )
{

  PcdSetBoolS(PcdMipiCamGpioEnable, TRUE);

  //
  // Set ALL port setting by default from VPD.
  //
  PcdSet32S (PcdCpuUsb30OverCurrentPinTable, (UINT32) (PcdGetPtr (VpdPcdCpuUsb3OcMap)));

  PcdSet8S (PcdTcssPdType, TcssPdTypeTi);
}

/**
  Board Specific Init for PEI post memory phase.
**/
VOID
PeiAdlPSimicsBoardSpecificInitPostMemNull (
  VOID
  )
{
}

/**
  Board's PCD function hook init function for PEI post memory phase.
**/
VOID
AdlPSimicsBoardFunctionInit (
  VOID
)
{
  PcdSet32S (PcdFuncPeiBoardSpecificInitPostMem, (UINTN) PeiAdlPSimicsBoardSpecificInitPostMemNull);
}

/**
  PMC-PD solution enable init lib
**/
VOID
AdlPSimicsBoardPmcPdInit (
  VOID
  )
{
  PcdSetBoolS (PcdBoardPmcPdEnable, 1);
}

/**
  USB TypeC init function for before silicon initialisation.
**/
VOID
AdlPSimicsBoardTypeCPortMapping (
  VOID
  )
{
  UINT16                          BoardId;

  BoardId = PcdGet16 (PcdBoardId);

  switch (BoardId) {
    //
    //   Board             Type C port     CPU Port    PCH Port    Split support
    //   ADL P DDR4             1            0            1            Yes
    //   ERB                    2            1            5            Yes
    //                          3            2            6            Yes
    //                          4            3            7            Yes
    case BoardIdAdlPSimics:
      PcdSetBoolS (PcdUsbTypeCSupport, TRUE);
      // Number of ports supported
      PcdSet8S(PcdTypeCPortsSupported, 4);
      // Type C port 1 mapping
      PcdSet8S(PcdUsbTypeCPort1, 0);
      PcdSet8S(PcdUsbTypeCPort1Pch, 1);
      PcdSet8S(PcdUsbCPort1Properties, (0 << TCSS_TYPEC_ROOT_PORT_INDEX_BIT_OFFSET | USB_TYPEC_CPU << 1 | SPLIT_SUPPORTED));
      // Type C port 2 mapping
      PcdSet8S(PcdUsbTypeCPort2, 1);
      PcdSet8S(PcdUsbTypeCPort2Pch, 5);
      PcdSet8S(PcdUsbCPort2Properties, (1 << TCSS_TYPEC_ROOT_PORT_INDEX_BIT_OFFSET | USB_TYPEC_CPU << 1 | SPLIT_SUPPORTED));
      // Type C port 3 mapping
      PcdSet8S(PcdUsbTypeCPort3, 2);
      PcdSet8S(PcdUsbTypeCPort3Pch, 6);
      PcdSet8S(PcdUsbCPort3Properties, (2 << TCSS_TYPEC_ROOT_PORT_INDEX_BIT_OFFSET | USB_TYPEC_CPU << 1 | SPLIT_SUPPORTED));
      // Type C port 4 mapping
      PcdSet8S(PcdUsbTypeCPort4, 3);
      PcdSet8S(PcdUsbTypeCPort4Pch, 7);
      PcdSet8S(PcdUsbCPort4Properties, (3 << TCSS_TYPEC_ROOT_PORT_INDEX_BIT_OFFSET | USB_TYPEC_CPU << 1 | SPLIT_SUPPORTED));
      break;
    default:
      PcdSetBoolS (PcdUsbTypeCSupport, FALSE);
      break;
  }

}


/**
  Configure GPIO, TouchPanel, HDA, PMC, TBT etc.

  @retval  EFI_SUCCESS   Operation success.
**/
EFI_STATUS
EFIAPI
AdlPSimicsBoardInitBeforeSiliconInit (
  VOID
  )
{
//[-start-191001-16990100-add]//
  EFI_STATUS       Status;
  UINT16           GpioTableCount;
  GPIO_INIT_CONFIG *GpioTable;
//[-end-191001-16990100-add]//
  AdlPSimicsInit ();

  AdlPSimicsSerialIoI2cPadsTerminationInit ();
  AdlPSimicsTouchPanelGpioInit ();
  AdlPSimicsCvfGpioInit();
  AdlPSimicsHdaVerbTableInit ();
  AdlPSimicsBoardMiscInit ();
  AdlPSimicsBoardTbtInit ();
  AdlPSimicsBoardFunctionInit();
  AdlPSimicsBoardPmcPdInit ();
  AdlPSimicsBoardTypeCPortMapping ();

  // WA #14
  //GpioInit (PcdGetPtr (PcdBoardGpioTable));
//[-start-191001-16990100-add]//
  GpioTable = PcdGetPtr (PcdBoardGpioTable);
  GetGpioTableSize (GpioTable, &GpioTableCount);
//[-start-191204-IB17700101-modify]//
  DEBUG_OEM_SVC ((DEBUG_INFO, "Pei OemChipsetServices Call: OemSvcModifyGpioSettingTable \n"));
  Status = OemSvcModifyGpioSettingTable (&GpioTable, &GpioTableCount);
  DEBUG_OEM_SVC ((DEBUG_INFO, "Pei OemChipsetServices OemSvcModifyGpioSettingTable Status: %r\n", Status));
//[-end-191204-IB17700101-modify]//
  if (GpioTableCount != 0 && Status != EFI_SUCCESS) {
    GpioConfigurePads ((UINT32) GpioTableCount, GpioTable);
  }
//[-end-191001-16990100-add]//
  ///
  /// Do Late PCH init
  ///
  LateSiliconInit ();

  return EFI_SUCCESS;
}
VOID
AdlPSimicsBoardPmProfileSettings (
  VOID
  )
{
  //
  //Modify Preferred_PM_Profile field based on Board SKU's. Default is set to Mobile
  //
    PcdSet8S (PcdPreferredPmProfile, EFI_ACPI_2_0_PM_PROFILE_MOBILE);
}
/**
  Board init for PEI after Silicon initialized

  @retval  EFI_SUCCESS   Operation success.
**/
EFI_STATUS
EFIAPI
AdlPSimicsBoardInitAfterSiliconInit (
  VOID
  )
{
  AdlPSimicsBoardPmProfileSettings ();
  return EFI_SUCCESS;
}
