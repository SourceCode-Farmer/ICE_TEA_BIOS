/** @file
 Source code for the board SA configuration Pcd init functions in Pre-Memory init phase.

@copyright
  INTEL CONFIDENTIAL
  Copyright 2015 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains a 'Sample Driver' and is licensed as such under the terms
  of your license agreement with Intel or your vendor. This file may be modified
  by the user, subject to the additional terms of the license agreement.

@par Specification Reference:
**/

#include "BoardSaConfigPreMem.h"
#include <Library/CpuPlatformLib.h>
#include <Pins/GpioPinsVer2Lp.h>
#include <PlatformBoardId.h>
#include <Library/PreSiliconEnvDetectLib.h>
#include <PlatformBoardConfig.h>
#include <Library/PcdLib.h>

/**
  MRC configuration init function for PEI pre-memory phase.

  @param[in]  VOID

  @retval VOID
**/
VOID
AdlPSimicsSaMiscConfigInit (
  VOID
  )
{
  PcdSet8S (PcdSaMiscUserBd, 6);        // btUlxUltType4
  PcdSet16S (PcdSaDdrFreqLimit, 0);

  return;
}

/**
  Board Memory Init related configuration init function for PEI pre-memory phase.

  @param[in]  VOID

  @retval VOID
**/
VOID
AdlPSimicsMrcConfigInit (
  VOID
  )
{
  UINT16    BoardId;
  BOOLEAN   ExternalSpdPresent;

  BoardId = PcdGet16(PcdBoardId);

  if( IsSimicsEnvironment()){
    ExternalSpdPresent = TRUE;
  } else {
    ExternalSpdPresent = PcdGetBool (PcdSpdPresent);
  }

  // SPD is the same size for all boards
  PcdSet16S (PcdMrcSpdDataSize, sizeof (mLpddr4Ddp8Gb200bSpd));

  // Check for external SPD presence on LPDDR4 boards
  if (ExternalSpdPresent){
    switch (BoardId) {
      case BoardIdAdlPSimics:
        PcdSet8S (PcdMrcSpdAddressTable0, 0xA0);
        PcdSet8S (PcdMrcSpdAddressTable1, 0xA2);
        PcdSet8S (PcdMrcSpdAddressTable2, 0x0);
        PcdSet8S (PcdMrcSpdAddressTable3, 0x0);
        PcdSet8S (PcdMrcSpdAddressTable4, 0x0);
        PcdSet8S (PcdMrcSpdAddressTable5, 0x0);
        PcdSet8S (PcdMrcSpdAddressTable6, 0x0);
        PcdSet8S (PcdMrcSpdAddressTable7, 0x0);
        PcdSet8S (PcdMrcSpdAddressTable8, 0xA4);
        PcdSet8S (PcdMrcSpdAddressTable9, 0xA6);
        PcdSet8S (PcdMrcSpdAddressTable10, 0x0);
        PcdSet8S (PcdMrcSpdAddressTable11, 0x0);
        PcdSet8S (PcdMrcSpdAddressTable12, 0x0);
        PcdSet8S (PcdMrcSpdAddressTable13, 0x0);
        PcdSet8S (PcdMrcSpdAddressTable14, 0x0);
        PcdSet8S (PcdMrcSpdAddressTable15, 0x0);
        break;
        }
  } else {
    // Use internal SPD table if external is not present
    PcdSet32S (PcdMrcSpdData, (UINTN) mLpddr4Ddp8Gb200bSpd);
    PcdSet8S (PcdMrcSpdAddressTable0, 0x00);
    PcdSet8S (PcdMrcSpdAddressTable1, 0x00);
    PcdSet8S (PcdMrcSpdAddressTable2, 0x00);
    PcdSet8S (PcdMrcSpdAddressTable3, 0x00);
    PcdSet8S (PcdMrcSpdAddressTable4, 0x00);
    PcdSet8S (PcdMrcSpdAddressTable5, 0x00);
    PcdSet8S (PcdMrcSpdAddressTable6, 0x00);
    PcdSet8S (PcdMrcSpdAddressTable7, 0x00);
    PcdSet8S (PcdMrcSpdAddressTable8, 0x00);
    PcdSet8S (PcdMrcSpdAddressTable9, 0x00);
    PcdSet8S (PcdMrcSpdAddressTable10, 0x00);
    PcdSet8S (PcdMrcSpdAddressTable11, 0x00);
    PcdSet8S (PcdMrcSpdAddressTable12, 0x00);
    PcdSet8S (PcdMrcSpdAddressTable13, 0x00);
    PcdSet8S (PcdMrcSpdAddressTable14, 0x00);
    PcdSet8S (PcdMrcSpdAddressTable15, 0x00);
  }

  // Setting the default DQ Byte Map. It may be overriden to board specific settings below.
  PcdSet32S (PcdMrcDqByteMap, (UINTN) DqByteMapAdlP);
  PcdSet16S (PcdMrcDqByteMapSize, sizeof (DqByteMapAdlP));

  // ICL uses the same RCOMP resistors for all DDR types
  PcdSet32S (PcdMrcRcompResistor, (UINTN) AdlPRcompResistorZero);

  // Use default RCOMP target values for all boards
  PcdSet32S (PcdMrcRcompTarget, (UINTN) RcompTargetAdlP);

  // Default is NIL
  PcdSetBoolS (PcdMrcDqPinsInterleavedControl, TRUE);
  PcdSetBoolS (PcdMrcDqPinsInterleaved, FALSE);

  // DqsMapCpu2Dram is the same size for all boards
  PcdSet16S (PcdMrcDqsMapCpu2DramSize, sizeof (DqsMapCpu2DramAdlPLp4Rvp));
  // DqMapCpu2Dram is the same size for all boards
  PcdSet16S (PcdMrcDqMapCpu2DramSize, sizeof (DqMapCpu2DramAdlPLp4Rvp));
  //
  // CA Vref routing: board-dependent
  // 0 - VREF_CA goes to both CH_A and CH_B (LPDDR3/DDR3L)
  // 1 - VREF_CA to CH_A, VREF_DQ_A to CH_B (should not be used)
  // 2 - VREF_CA to CH_A, VREF_DQ_B to CH_B (DDR4)
  //
  // Set it to 2 for all our DDR4 boards; it is ignored for LPDDR4
  //
  PcdSet8S (PcdMrcCaVrefConfig, 2);

  return;
}

/**
  Board SA related GPIO configuration init function for PEI pre-memory phase.

  @param[in]  VOID

  @retval VOID
**/
VOID
AdlPSimicsSaGpioConfigInit (
  VOID
  )
{
  UINT16    BoardId;
  BoardId = PcdGet16(PcdBoardId);
  //
  // Assigning default values to PCIE RTD3 GPIOs
  //
  switch (BoardId) {
    case BoardIdAdlPSimics:
      PcdSetBoolS(PcdPcieSlot1HoldRstGpioPolarity, PIN_GPIO_ACTIVE_LOW);
      PcdSetBoolS(PcdPcieSsd2RstGpioPolarity, PIN_GPIO_ACTIVE_LOW);
      break;
    default:
      break;
  }

  switch (BoardId) {
    case BoardIdAdlPSimics:
      PcdSet8S(PcdRootPortIndex, 4);
      PcdSet8S(PcdPcieSlot1GpioSupport, PchGpio);
      PcdSet8S(PcdPcieSlot1HoldRstExpanderNo, 0);
      PcdSet32S(PcdPcieSlot1HoldRstGpioNo, GPIO_VER2_LP_GPP_C13);
      PcdSet8S(PcdPcieSlot1PwrEnableExpanderNo, 0);
      PcdSet32S(PcdPcieSlot1PwrEnableGpioNo, GPIO_VER2_LP_GPP_A14);
      PcdSetBoolS(PcdPcieSlot1PwrEnableGpioPolarity, PIN_GPIO_ACTIVE_LOW);
      //
      // Configure CPU M.2 SSD GPIO PCDs
      //
      PcdSet32S(PcdPcieSsd2PwrEnableGpioNo, GPIO_VER2_LP_GPP_D16);
      PcdSet32S(PcdPcieSsd2RstGpioNo, GPIO_VER2_LP_GPP_A11);
      PcdSetBoolS(PcdPcieSsd2PwrEnableGpioPolarity, PIN_GPIO_ACTIVE_HIGH);
      break;
  }

  //
  // PEG PCIE RTD3 GPIO
  //
 PcdSet8S(PcdPcie0GpioSupport, PchGpio);
  PcdSet8S(PcdPcie0HoldRstExpanderNo, 0);
  PcdSet32S(PcdPcie0HoldRstGpioNo, GPIO_VER2_LP_GPP_C13);
  PcdSetBoolS(PcdPcie0HoldRstActive, FALSE);
  PcdSet8S(PcdPcie0PwrEnableExpanderNo, 0);
  PcdSet32S(PcdPcie0PwrEnableGpioNo, GPIO_VER2_LP_GPP_A14);
  PcdSetBoolS(PcdPcie0PwrEnableActive, FALSE);

  PcdSet8S(PcdPcie1GpioSupport, NotSupported);
  PcdSet8S(PcdPcie1HoldRstExpanderNo, 0);
  PcdSet32S(PcdPcie1HoldRstGpioNo, 0);
  PcdSetBoolS(PcdPcie1HoldRstActive, FALSE);
  PcdSet8S(PcdPcie1PwrEnableExpanderNo, 0);
  PcdSet32S(PcdPcie1PwrEnableGpioNo, 0);
  PcdSetBoolS(PcdPcie1PwrEnableActive, FALSE);

  PcdSet8S(PcdPcie2GpioSupport, NotSupported);
  PcdSet8S(PcdPcie2HoldRstExpanderNo, 0);
  PcdSet32S(PcdPcie2HoldRstGpioNo, 0);
  PcdSetBoolS(PcdPcie2HoldRstActive, FALSE);
  PcdSet8S(PcdPcie2PwrEnableExpanderNo, 0);
  PcdSet32S(PcdPcie2PwrEnableGpioNo, 0);
  PcdSetBoolS(PcdPcie2PwrEnableActive, FALSE);

  PcdSet8S(PcdPcie3GpioSupport, NotSupported);
  PcdSet8S(PcdPcie3HoldRstExpanderNo, 0);
  PcdSet32S(PcdPcie3HoldRstGpioNo, 0);
  PcdSetBoolS(PcdPcie3HoldRstActive, FALSE);
  PcdSet8S(PcdPcie3PwrEnableExpanderNo, 0);
  PcdSet32S(PcdPcie3PwrEnableGpioNo, 0);
  PcdSetBoolS(PcdPcie3PwrEnableActive, FALSE);
  return;
}

/**
  SA Display DDI configuration init function for PEI pre-memory phase.

  @param[in]  VOID

  @retval     VOID
**/
VOID
AdlPSimicsSaDisplayConfigInit (
  VOID
  )
{
  UINT16    BoardId;

  BoardId = PcdGet16(PcdBoardId);

  switch (BoardId) {
    case BoardIdAdlPSimics:
        PcdSet32S (PcdSaDisplayConfigTable, (UINTN) mAdlPDdr4RowDisplayDdiConfig);
        PcdSet16S (PcdSaDisplayConfigTableSize, sizeof (mAdlPDdr4RowDisplayDdiConfig));
      break;
  }

  return;
}

/**
  Board USB related configuration init function for PEI pre-memory phase.

  @param[in]  VOID

  @retval VOID
**/
VOID
AdlPSimicsSaUsbConfigInit (
  VOID
  )
{
  UINT16    BoardId;
  BoardId = PcdGet16(PcdBoardId);

  switch (BoardId) {
    //
    // Override ALL port setting if required.
    //
    case BoardIdAdlPSimics:
      PcdSet8S (PcdCpuXhciPortSupportMap, 0x0F);
      PcdSet8S (PcdITbtRootPortNumber, 4);
      break;
  }
  //
  // Update Cpu Xhci Port Enable Map PCD based on SaSetup Data and PcdCpuXhciPortSupportMap.
  //
  TcssUpdateCpuXhciPortEnableMapPcd ();

  return;
}
