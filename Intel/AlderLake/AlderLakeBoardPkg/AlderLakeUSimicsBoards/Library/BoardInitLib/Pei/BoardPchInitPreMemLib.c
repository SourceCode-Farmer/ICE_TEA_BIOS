/** @file
 Source code for the board PCH configuration Pcd init functions for Pre-Mmeory Init phase.

@copyright
  INTEL CONFIDENTIAL
  Copyright 2015 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains a 'Sample Driver' and is licensed as such under the terms
  of your license agreement with Intel or your vendor. This file may be modified
  by the user, subject to the additional terms of the license agreement.

@par Specification Reference:
**/

#include <Uefi.h>
#include <Library/BaseLib.h>
#include <Library/PcdLib.h>
#include <Library/MemoryAllocationLib.h>
#include <Library/DebugLib.h>
#include <Library/BoardConfigLib.h>

#include <Library/PreSiliconEnvDetectLib.h>
#include <Include/PlatformBoardId.h>
#include <PlatformBoardConfig.h>
#include <GpioTableAdlPPreMem.h>
#include <Library/PcdLib.h>
#include <PlatformBoardId.h>

/**
  Board Root Port Clock Info configuration init function for PEI pre-memory phase.

  @retval EFI_SUCCESS   The function completed successfully.
**/
EFI_STATUS
AdlPSimicsRootPortClkInfoInit (
  VOID
  )
{
  PCD64_BLOB Clock[PCH_MAX_PCIE_CLOCKS];
  UINT32 Index;
  PCIE_CLOCKS_USAGE *PcieClocks;

  PcieClocks = NULL;

  //
  // The default clock assignment will be FREE_RUNNING, which corresponds to PchClockUsageUnspecified
  // This is safe but power-consuming setting. If Platform code doesn't contain port-clock map for a given board,
  // the clocks will keep on running anyway, allowing PCIe devices to operate. Downside is that clocks will
  // continue to draw power. To prevent this, remember to provide port-clock map for every board.
  //
  for (Index = 0; Index < PCH_MAX_PCIE_CLOCKS; Index++) {
    Clock[Index].PcieClock.ClkReqSupported = TRUE;
    Clock[Index].PcieClock.ClockUsage = FREE_RUNNING;
  }

  ///
  /// Assign ClkReq signal to root port. (Base 0)
  /// For LP, Set 0 - 5
  /// For H,  Set 0 - 15
  /// Note that if GbE is enabled, ClkReq assigned to GbE will not be available for Root Port.
  ///

  PcieClocks = PcdGetPtr(VpdPcdPcieClkUsageMap);

  Clock[0].PcieClock.ClockUsage  = PcieClocks->ClockUsage[0];
  Clock[1].PcieClock.ClockUsage  = PcieClocks->ClockUsage[1];
  Clock[2].PcieClock.ClockUsage  = PcieClocks->ClockUsage[2];
  Clock[3].PcieClock.ClockUsage  = PcieClocks->ClockUsage[3];
  Clock[4].PcieClock.ClockUsage  = PcieClocks->ClockUsage[4];
  Clock[5].PcieClock.ClockUsage  = PcieClocks->ClockUsage[5];
  Clock[6].PcieClock.ClockUsage  = PcieClocks->ClockUsage[6];

  PcdSet64S (PcdPcieClock0,  Clock[ 0].Blob); // @todo Those individual PCDs should probably be replaced with something like a VOID* that can be iterated over
  PcdSet64S (PcdPcieClock1,  Clock[ 1].Blob); // @todo Those individual PCDs should probably be replaced with something like a VOID* that can be iterated over
  PcdSet64S (PcdPcieClock2,  Clock[ 2].Blob); // @todo Those individual PCDs should probably be replaced with something like a VOID* that can be iterated over
  PcdSet64S (PcdPcieClock3,  Clock[ 3].Blob); // @todo Those individual PCDs should probably be replaced with something like a VOID* that can be iterated over
  PcdSet64S (PcdPcieClock4,  Clock[ 4].Blob); // @todo Those individual PCDs should probably be replaced with something like a VOID* that can be iterated over
  PcdSet64S (PcdPcieClock5,  Clock[ 5].Blob); // @todo Those individual PCDs should probably be replaced with something like a VOID* that can be iterated over
  PcdSet64S (PcdPcieClock6,  Clock[ 6].Blob); // @todo Those individual PCDs should probably be replaced with something like a VOID* that can be iterated over
  PcdSet64S (PcdPcieClock7,  Clock[ 7].Blob); // @todo Those individual PCDs should probably be replaced with something like a VOID* that can be iterated over
  PcdSet64S (PcdPcieClock8,  Clock[ 8].Blob); // @todo Those individual PCDs should probably be replaced with something like a VOID* that can be iterated over
  PcdSet64S (PcdPcieClock9,  Clock[ 9].Blob); // @todo Those individual PCDs should probably be replaced with something like a VOID* that can be iterated over
  PcdSet64S (PcdPcieClock10, Clock[10].Blob); // @todo Those individual PCDs should probably be replaced with something like a VOID* that can be iterated over
  PcdSet64S (PcdPcieClock11, Clock[11].Blob); // @todo Those individual PCDs should probably be replaced with something like a VOID* that can be iterated over
  PcdSet64S (PcdPcieClock12, Clock[12].Blob); // @todo Those individual PCDs should probably be replaced with something like a VOID* that can be iterated over
  PcdSet64S (PcdPcieClock13, Clock[13].Blob); // @todo Those individual PCDs should probably be replaced with something like a VOID* that can be iterated over
  PcdSet64S (PcdPcieClock14, Clock[14].Blob); // @todo Those individual PCDs should probably be replaced with something like a VOID* that can be iterated over
  PcdSet64S (PcdPcieClock15, Clock[15].Blob); // @todo Those individual PCDs should probably be replaced with something like a VOID* that can be iterated over
//  PcdSet64S (PcdPcieClock16, Clock[16].Blob); // @todo Those individual PCDs should probably be replaced with something like a VOID* that can be iterated over
//  PcdSet64S (PcdPcieClock17, Clock[17].Blob); // @todo Those individual PCDs should probably be replaced with something like a VOID* that can be iterated over
  return EFI_SUCCESS;
}

/**
  Board USB related configuration init function for PEI pre-memory phase.

  @param[in]  VOID

  @retval EFI_SUCCESS   The function completed successfully.
**/
EFI_STATUS
AdlPSimicsUsbConfigInit (
  VOID
  )
{
  USB_OC_MAP_TABLE   *Usb2OcMappingTable;
  USB_OC_MAP_TABLE   *Usb3OcMappingTable;

  //
  // USB2 PHY settings.
  //
  PcdSet32S (PcdUsb2PhyTuningTable, (UINT32) &mUsb2PhyTuningTable);

  //
  // USB Port Over Current Pin
  //
  Usb2OcMappingTable = PcdGetPtr(VpdPcdUSB2OCMap);
  Usb3OcMappingTable = PcdGetPtr(VpdPcdUSB3OCMap);

  PcdSet32S (PcdUsb2OverCurrentPinTable, (UINT32) Usb2OcMappingTable);
  PcdSet32S (PcdUsb3OverCurrentPinTable, (UINT32) Usb3OcMappingTable);

  return EFI_SUCCESS;
}

/**
  Board GPIO Group Tier configuration init function for PEI pre-memory phase.
**/
VOID
AdlPSimicsGpioGroupTierInit (
  VOID
  )
{
  //
  // GPIO Group Tier
  //
  PcdSet32S (PcdGpioGroupToGpeDw0, 0);
  PcdSet32S (PcdGpioGroupToGpeDw1, 0);
  PcdSet32S (PcdGpioGroupToGpeDw2, 0);

  return;
}

/**
  GPIO init function for PEI pre-memory phase.

  @param[in]  BoardId   An unsigned integrer represent the board id.

  @retval EFI_SUCCESS   The function completed successfully.
**/
EFI_STATUS
AdlPSimicsGpioTablePreMemInit (
  IN UINT16 BoardId
  )
{
  GPIO_INIT_CONFIG  *GpioTable;
  GpioTable = NULL;

  //
  // GPIO Table Init, Update PreMem GPIO table to PcdBoardGpioTablePreMem
  //
  PcdSet32S (PcdBoardGpioTableWwanOnEarlyPreMem, (UINTN) mGpioTableAdlPDdr4WwanOnEarlyPreMem);
  PcdSet16S (PcdBoardGpioTableWwanOnEarlyPreMemSize, sizeof (mGpioTableAdlPDdr4WwanOnEarlyPreMem) / sizeof (GPIO_INIT_CONFIG));
  PcdSet32S (PcdBoardGpioTableWwanOffEarlyPreMem, (UINTN) mGpioTableAdlPDdr4WwanOffEarlyPreMem);
  PcdSet16S (PcdBoardGpioTableWwanOffEarlyPreMemSize, sizeof (mGpioTableAdlPDdr4WwanOffEarlyPreMem) / sizeof (GPIO_INIT_CONFIG));
  GpioTable = (GPIO_INIT_CONFIG *)PcdGetPtr(VpdPcdBoardGpioTablePreMem);
  ConfigureGpioTabletoPCD (GpioTable, PRE_MEM);

  return EFI_SUCCESS;
}
