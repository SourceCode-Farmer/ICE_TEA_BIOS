/** @file

;******************************************************************************
;* Copyright (c) 2021, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
**/
/** @file
  This file is SampleCode for Intel PEI Platform Policy initialization in pre-memory.
  For FSP API mode

@copyright
  INTEL CONFIDENTIAL
  Copyright 2017 - 2020 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains a 'Sample Driver' and is licensed as such under the terms
  of your license agreement with Intel or your vendor. This file may be modified
  by the user, subject to the additional terms of the license agreement.

@par Specification Reference:
**/

#include "PeiPolicy.h"
#include <Library/PeiSiPolicyUpdateLib.h>

#include <SetupVariable.h>
#include <Ppi/ReadOnlyVariable2.h>

#include <Library/PeiServicesLib.h>
#include <Library/PeiServicesTablePointerLib.h>

#include <FspEas.h>
#include <Library/FspCommonLib.h>
#include <Library/FspWrapperApiLib.h>

VOID
InternalPrintVariableData (
  IN UINT8   *Data8,
  IN UINTN   DataSize
  )
{
  UINTN      Index;

  for (Index = 0; Index < DataSize; Index++) {
    if (Index % 0x10 == 0) {
      DEBUG ((DEBUG_INFO, "\n%08X:", Index));
    }
    DEBUG ((DEBUG_INFO, " %02X", *Data8++));
  }
  DEBUG ((DEBUG_INFO, "\n"));
}


VOID *
EFIAPI
SiliconPolicyUpdatePreMemLoadDefaultUpd (
  VOID
  )
{
  FSP_INFO_HEADER              *FspmHeaderPtr;
  FSPM_UPD_COMMON              *FspmUpdDataPtr;
  UINTN                        *SourceData;

  DEBUG ((DEBUG_INFO, "Silicon PEI Policy Initialization Start in Pre-Memory...\n"));

  //
  // Copy default FSP-M UPD data from Flash
  //
  FspmHeaderPtr = (FSP_INFO_HEADER *) FspFindFspHeader (PcdGet32 (PcdFspmBaseAddress));
  if (FspmHeaderPtr == NULL) {
    DEBUG ((DEBUG_ERROR, "Fail to get FspmHeaderPtr!\n"));
    ASSERT (FspmHeaderPtr != NULL);
    return NULL;
  }
  DEBUG ((DEBUG_INFO, "FspmHeaderPtr - 0x%x\n", FspmHeaderPtr));

  FspmUpdDataPtr = (FSPM_UPD_COMMON *) AllocateZeroPool ((UINTN) FspmHeaderPtr->CfgRegionSize);
  if (FspmUpdDataPtr == NULL) {
    DEBUG ((DEBUG_ERROR, "Fail to allocate memory for FspmUpdData!\n"));
    ASSERT (FspmUpdDataPtr != NULL);
    return NULL;
  }
  SourceData = (UINTN *) ((UINTN) FspmHeaderPtr->ImageBase + (UINTN) FspmHeaderPtr->CfgRegionOffset);

  CopyMem (FspmUpdDataPtr, SourceData, (UINTN) FspmHeaderPtr->CfgRegionSize);

  PcdSet32S (PcdFspmUpdDataAddress, (UINT32) FspmUpdDataPtr);
  DEBUG ((DEBUG_INFO, "FspmUpdDataPtr - 0x%x\n", (UINT32) FspmUpdDataPtr));

  return FspmUpdDataPtr;
}


VOID
EFIAPI
SiliconPolicyUpdatePreMemFirmwareConfig (
  VOID
  )
{
  EFI_STATUS                        Status;
  EFI_PEI_READ_ONLY_VARIABLE2_PPI   *VariableServices;
  SETUP_DATA                        SystemConfiguration;
  UINTN                             Size;
  UINT8                             FirmwareConfiguration;

  Status = PeiServicesLocatePpi (
             &gEfiPeiReadOnlyVariable2PpiGuid,
             0,
             NULL,
             (VOID **) &VariableServices
             );
  if (EFI_ERROR(Status)) {
    DEBUG((DEBUG_ERROR, "PeiServicesLocatePpi failed\n"));
    return;
  }
  FirmwareConfiguration = 0;
  Size = sizeof (SETUP_DATA);
  Status = VariableServices->GetVariable (
                               VariableServices,
                               L"Setup",
                               &gSetupVariableGuid,
                               NULL,
                               &Size,
                               &SystemConfiguration
                               );
  if (!EFI_ERROR (Status)) {
    FirmwareConfiguration = SystemConfiguration.FirmwareConfiguration;
  }

//[-start-201119-IB06462250-modify]//
//
//  Remove FirmwareConfiguration condition because H2O doesn't use it.
//
  // if (FirmwareConfiguration != FwConfigDefault) {
    //
    // Update and override all platform related and customized settings below.
    //
    UpdatePeiSiPolicyPreMem ();
    UpdatePeiPchPolicyPreMem ();
    UpdatePeiMePolicyPreMem ();
    UpdatePeiSaPolicyPreMem ();
    UpdatePeiCpuPolicyPreMem ();

    // if ((FirmwareConfiguration >= FwConfigTest) && (FirmwareConfiguration < FwConfigMax)) {
      // Update Debug Policies
      UpdatePeiPchPolicyDebugPreMem ();
      UpdatePeiSaPolicyDebugPreMem ();
      UpdatePeiCpuPolicyDebugPreMem ();
    // }


  // }
//[-end-201119-IB06462250-modify]//

//[-start-210421-IB14630476-remove]//
//   //
//   // Work around to reset a PCIe card which requires more delay than the standard delay as per PCIe spec.
//   //
// #if FixedPcdGetBool(PcdHgEnable) == 1
//   PcieCardResetWorkAround ();
// #endif
//[-end-210421-IB14630476-remove]//
}


VOID *
EFIAPI
SiliconPolicyUpdatePreMem (
  IN OUT VOID *Policy
  )
{
  Policy = NULL;

  SiliconPolicyUpdatePreMemLoadDefaultUpd ();

  //
  // Update policy by board configuration
  //
  UpdatePeiPchPolicyBoardConfigPreMem ();
  UpdatePeiMePolicyBoardConfigPreMem ();
  UpdatePeiSaPolicyBoardConfigPreMem ();
  UpdatePeiCpuPolicyBoardConfigPreMem ();

  // @todo separate out the advanced policy update process from this module.
  SiliconPolicyUpdatePreMemFirmwareConfig ();

  return Policy;
}
