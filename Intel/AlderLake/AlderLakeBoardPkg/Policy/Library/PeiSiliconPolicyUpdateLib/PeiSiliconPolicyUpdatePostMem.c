/** @file
 
;******************************************************************************
;* Copyright 2021 Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corp.
;*
;******************************************************************************
*/
/** @file
  This file is SampleCode for Intel PEI Platform Policy initialization in post-memory.

@copyright
  INTEL CONFIDENTIAL
  Copyright 2017 - 2020 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains a 'Sample Driver' and is licensed as such under the terms
  of your license agreement with Intel or your vendor. This file may be modified
  by the user, subject to the additional terms of the license agreement.

@par Specification Reference:
**/

#include "PeiPolicy.h"
#include <Library/PeiSiPolicyUpdateLib.h>

#include <SetupVariable.h>
#include <Ppi/ReadOnlyVariable2.h>

#include <Library/PeiServicesLib.h>
#include <Library/PeiServicesTablePointerLib.h>

VOID
InternalPrintVariableData (
  IN UINT8   *Data8,
  IN UINTN   DataSize
  );

VOID
EFIAPI
SiliconPolicyUpdatePostMemFirmwareConfig (
  VOID
  )
{
  EFI_STATUS                        Status;
  EFI_PEI_READ_ONLY_VARIABLE2_PPI   *VariableServices;
  SETUP_DATA                        SystemConfiguration;
  UINTN                             Size;
  UINT8                             FirmwareConfiguration;

  Status = PeiServicesLocatePpi (
             &gEfiPeiReadOnlyVariable2PpiGuid,
             0,
             NULL,
             (VOID **) &VariableServices
             );
  if (EFI_ERROR(Status)) {
    DEBUG((DEBUG_ERROR, "PeiServicesLocatePpi failed\n"));
    return;
  }
  FirmwareConfiguration = 0;
  Size = sizeof (SETUP_DATA);
  Status = VariableServices->GetVariable (
                               VariableServices,
                               L"Setup",
                               &gSetupVariableGuid,
                               NULL,
                               &Size,
                               &SystemConfiguration
                               );
  if (!EFI_ERROR (Status)) {
    FirmwareConfiguration = SystemConfiguration.FirmwareConfiguration;
  }

//[-start-201119-IB06462250-modify]//
//
//  Remove FirmwareConfiguration condition because H2O doesn't use it.
//
  // if (FirmwareConfiguration != FwConfigDefault) {
  //   //
  //   // Update and override all platform related and customized settings below.
  //   //
    UpdatePeiSiPolicy ();
    UpdatePeiTbtPolicy ();
    UpdatePeiPchPolicy ();
    UpdatePeiSaPolicy ();
    UpdatePeiCpuPolicy ();
#if FixedPcdGetBool(PcdAmtEnable) == 1
    if (MeTypeIsClient ()) {
      UpdatePeiAmtPolicy ();
    }
#endif
    UpdatePeiMePolicy ();

    // if ((FirmwareConfiguration >= FwConfigTest) && (FirmwareConfiguration < FwConfigMax)) {
      // Update Debug Policies
      UpdatePeiPchPolicyDebug ();
      UpdatePeiCpuPolicyDebug ();
      UpdatePeiSiPolicyDebug ();
      UpdatePeiSaPolicyDebug ();
    // }


  // }
//[-end-201119-IB06462250-modify]//
}

VOID *
EFIAPI
SiliconPolicyUpdatePostMem (
  IN OUT VOID *Policy
  )
{
  Policy = NULL;
  //
  // Update policy by board configuration
  //
  UpdatePeiSiPolicyBoardConfig ();
  UpdatePeiPchPolicyBoardConfig ();
  UpdatePeiSaPolicyBoardConfig ();
  UpdatePeiCpuPolicyBoardConfig ();
#if FixedPcdGetBool(PcdAmtEnable) == 1
  UpdatePeiAmtPolicyBoardConfig ();
#endif
  UpdatePeiMePolicyBoardConfig ();
  UpdatePeiTbtPolicyBoardConfig ();

  // @todo separate out the advanced policy update process from this module.
  SiliconPolicyUpdatePostMemFirmwareConfig ();

  return Policy;
}
