### @file
#
#******************************************************************************
#* Copyright (c) 2014 - 2020, Insyde Software Corporation. All Rights Reserved.
#*
#* You may not reproduce, distribute, publish, display, perform, modify, adapt,
#* transmit, broadcast, present, recite, release, license or otherwise exploit
#* any part of this publication in any form, by any means, without the prior
#* written permission of Insyde Software Corporation.
#*
#******************************************************************************
#
### @file
# Module Information description file for BIOS Info PEIM.
# The module provides BiosInfo structure listing up all firmware volume's base addresses,
# sizes, attributes, those information associated to each firmware volume.
#
#@copyright
#  INTEL CONFIDENTIAL
#  Copyright 2011 - 2021 Intel Corporation.
#
#  The source code contained or described herein and all documents related to the
#  source code ("Material") are owned by Intel Corporation or its suppliers or
#  licensors. Title to the Material remains with Intel Corporation or its suppliers
#  and licensors. The Material may contain trade secrets and proprietary and
#  confidential information of Intel Corporation and its suppliers and licensors,
#  and is protected by worldwide copyright and trade secret laws and treaty
#  provisions. No part of the Material may be used, copied, reproduced, modified,
#  published, uploaded, posted, transmitted, distributed, or disclosed in any way
#  without Intel's prior express written permission.
#
#  No license under any patent, copyright, trade secret or other intellectual
#  property right is granted to or conferred upon you by disclosure or delivery
#  of the Materials, either expressly, by implication, inducement, estoppel or
#  otherwise. Any license under such intellectual property rights must be
#  express and approved by Intel in writing.
#
#  Unless otherwise agreed by Intel in writing, you may not remove or alter
#  this notice or any other notice embedded in Materials by Intel or
#  Intel's suppliers or licensors in any way.
#
#  This file contains a 'Sample Driver' and is licensed as such under the terms
#  of your license agreement with Intel or your vendor. This file may be modified
#  by the user, subject to the additional terms of the license agreement.
#
# @par Specification Reference:
#
# @par Glossary:
###

[Defines]
  INF_VERSION                    = 0x00010017
  BASE_NAME                      = BiosInfo
  FILE_GUID                      = 4A4CA1C6-871C-45BB-8801-6910A7AA5807
  VERSION_STRING                 = 1.0
  MODULE_TYPE                    = PEIM
  ENTRY_POINT                    = BiosInfoEntryPoint
#
# The following information is for reference only and not required by the build tools.
#
# VALID_ARCHITECTURES IA32 X64
#

[LibraryClasses]
  PeimEntryPoint
  PeiServicesLib
  PeiServicesTablePointerLib
  HobLib
  BaseMemoryLib
  MemoryAllocationLib
  DebugLib
  SpiAccessLib

[Packages]
  MdePkg/MdePkg.dec
  MdeModulePkg/MdeModulePkg.dec
  AlderLakePlatSamplePkg/PlatformPkg.dec
  ClientOneSiliconPkg/SiPkg.dec
  IntelSiliconPkg/IntelSiliconPkg.dec
  MinPlatformPkg/MinPlatformPkg.dec
  AlderLakeBoardPkg/BoardPkg.dec
  SecurityPkg/SecurityPkg.dec
  BoardModulePkg/BoardModulePkg.dec
  #  for BootGuardTableDefinition >>
  $(CHIPSET_PKG)/$(CHIPSET_PKG).dec
  $(PROJECT_PKG)/Project.dec
  InsydeModulePkg/InsydeModulePkg.dec
  #  for BootGuardTableDefinition <<

[Pcd]
  gSiPkgTokenSpaceGuid.PcdBiosAreaBaseAddress                     ## CONSUMES
  #   insyde no use it. >>
  # gSiPkgTokenSpaceGuid.PcdBiosSize                                ## CONSUMES
  #   insyde no use it. <<
  gEfiMdeModulePkgTokenSpaceGuid.PcdFlashNvStorageVariableBase    ## CONSUMES
  gEfiMdeModulePkgTokenSpaceGuid.PcdFlashNvStorageVariableSize    ## CONSUMES
  gEfiMdeModulePkgTokenSpaceGuid.PcdFlashNvStorageFtwWorkingBase  ## CONSUMES
  gEfiMdeModulePkgTokenSpaceGuid.PcdFlashNvStorageFtwWorkingSize  ## CONSUMES
  gEfiMdeModulePkgTokenSpaceGuid.PcdFlashNvStorageFtwSpareBase    ## CONSUMES
  gEfiMdeModulePkgTokenSpaceGuid.PcdFlashNvStorageFtwSpareSize    ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdFlashFvFirmwareBinariesBase       ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdFlashFvFirmwareBinariesSize       ## CONSUMES
  gMinPlatformPkgTokenSpaceGuid.PcdFlashFvAdvancedBase            ## CONSUMES
  gMinPlatformPkgTokenSpaceGuid.PcdFlashFvAdvancedSize            ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdFlashFvOptionalBase               ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdFlashFvOptionalSize               ## CONSUMES
  gMinPlatformPkgTokenSpaceGuid.PcdFlashFvSecurityBase            ## CONSUMES
  gMinPlatformPkgTokenSpaceGuid.PcdFlashFvSecuritySize            ## CONSUMES
  gMinPlatformPkgTokenSpaceGuid.PcdFlashFvOsBootBase              ## CONSUMES
  gMinPlatformPkgTokenSpaceGuid.PcdFlashFvOsBootSize              ## CONSUMES
  gMinPlatformPkgTokenSpaceGuid.PcdFlashFvUefiBootBase            ## CONSUMES
  gMinPlatformPkgTokenSpaceGuid.PcdFlashFvUefiBootSize            ## CONSUMES
  gMinPlatformPkgTokenSpaceGuid.PcdFlashFvPostMemoryBase          ## CONSUMES
  gMinPlatformPkgTokenSpaceGuid.PcdFlashFvPostMemorySize          ## CONSUMES
  gMinPlatformPkgTokenSpaceGuid.PcdFlashFvMicrocodeBase           ## CONSUMES
  gMinPlatformPkgTokenSpaceGuid.PcdFlashFvMicrocodeSize           ## CONSUMES
  gMinPlatformPkgTokenSpaceGuid.PcdFlashFvFspSBase                ## CONSUMES
  gMinPlatformPkgTokenSpaceGuid.PcdFlashFvFspSSize                ## CONSUMES
  gMinPlatformPkgTokenSpaceGuid.PcdFlashFvFspMBase                ## CONSUMES
  gMinPlatformPkgTokenSpaceGuid.PcdFlashFvFspMSize                ## CONSUMES
  gMinPlatformPkgTokenSpaceGuid.PcdFlashFvFspTBase                ## CONSUMES
  gMinPlatformPkgTokenSpaceGuid.PcdFlashFvFspTSize                ## CONSUMES
  gMinPlatformPkgTokenSpaceGuid.PcdFlashFvPreMemoryBase           ## CONSUMES
  gMinPlatformPkgTokenSpaceGuid.PcdFlashFvPreMemorySize           ## CONSUMES
  gPlatformModuleTokenSpaceGuid.PcdCapsuleEnable                  ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdFlashFvTsnMacAddressBase          ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdFlashFvTsnMacAddressSize          ## CONSUMES
  gPlatformModuleTokenSpaceGuid.PcdResiliencyEnable               ## CONSUMES
  gSiPkgTokenSpaceGuid.PcdEmbeddedEnable                          ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdFlashFvExtendedPostMemorySize     ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdFlashFvExtendedPostMemoryBase     ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdFlashFvExtendedAdvancedSize       ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdFlashFvExtendedAdvancedBase       ## CONSUMES
  gPlatformModuleTokenSpaceGuid.PcdExtendedBiosRegionSupport      ## CONSUMES
  #   for BootGuardTableDefinition >>
  gChipsetPkgTokenSpaceGuid.PcdFlashFvRecovery0Base
  gChipsetPkgTokenSpaceGuid.PcdFlashFvRecovery0Size
  gChipsetPkgTokenSpaceGuid.PcdFlashFvRecovery2Base
  gChipsetPkgTokenSpaceGuid.PcdFlashFvRecovery2Size
  gChipsetPkgTokenSpaceGuid.PcdH2OFdmOffset
  gChipsetPkgTokenSpaceGuid.PcdH2OFdmSize

  gChipsetPkgTokenSpaceGuid.PcdFlashFirmwareBinariesFvBase
  gChipsetPkgTokenSpaceGuid.PcdFlashFirmwareBinariesFvSize
  gInsydeTokenSpaceGuid.PcdFlashFvMainBase
  gInsydeTokenSpaceGuid.PcdFlashFvMainSize
  gInsydeTokenSpaceGuid.PcdFlashFvRecoveryBase
  gInsydeTokenSpaceGuid.PcdFlashFvRecoverySize
  #   for BootGuardTableDefinition <<


[Sources]
  BiosInfo.c

[Ppis]
  gEfiPeiFirmwareVolumeInfoMeasurementExcludedPpiGuid    ## PRODUCES
  gPeiFvMeasurementExcludedPlatformPpiGuid               ## NOTIFY

[Guids]
  gBiosInfoGuid                                 ## PRODUCES
  gBiosInfoRecoveryGuid                         ## SOMETIMES_PRODUCES
  gSysFwUpdateProgressGuid                      ## COMSUMES

[Depex]
  gEfiPeiMasterBootModePpiGuid
