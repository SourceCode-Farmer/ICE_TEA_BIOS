/** @file
 Intel PEI TBT Policy update by board configuration

@copyright
  INTEL CONFIDENTIAL
  Copyright 2017 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains a 'Sample Driver' and is licensed as such under the terms
  of your license agreement with Intel or your vendor. This file may be modified
  by the user, subject to the additional terms of the license agreement.

@par Specification Reference:
**/
#include "PeiPolicyBoardConfig.h"
#include <Ppi/PeiDTbtPolicy.h>
#include <PlatformBoardId.h>
#include <Library/PmcLib.h>
#include <Pins/GpioPinsVer2Lp.h>
#include <Pins/GpioPinsVer2H.h>
#include <Pins/GpioPinsVer4S.h>
#include <Library/GpioLib.h>
#include <Ppi/ReadOnlyVariable2.h>
#include <Library/PcdLib.h>
#include <Library/CpuPlatformLib.h>
#include <TcssDataHob.h>

/**
  Configure dTBT Force Power pin for debug mode.
**/
VOID
DTbtForcePwrGpioPreMem (
  VOID
  )
{
  GPIO_CONFIG                     GpioConfig;
  GPIO_PAD                        GpioPad[2] = {0};
  EFI_STATUS                      Status;
  SETUP_DATA                      SetupData;
  UINTN                           VarSize;
  EFI_PEI_READ_ONLY_VARIABLE2_PPI *VariableServices;
  UINT8                           Index;
  DEBUG ((DEBUG_INFO, "DTbtForcePwrGpioPreMem Start\n"));

  //
  // Locate Setup variables
  //
  Status = PeiServicesLocatePpi (
             &gEfiPeiReadOnlyVariable2PpiGuid,
             0,
             NULL,
             (VOID **) &VariableServices
             );
  if (EFI_ERROR (Status)) {
    DEBUG ((DEBUG_ERROR, "PeiServicesLocatePpi failed\n"));
    return;
  }

  VarSize = sizeof (SETUP_DATA);
  Status = VariableServices->GetVariable (
                               VariableServices,
                               L"Setup",
                               &gSetupVariableGuid,
                               NULL,
                               &VarSize,
                               &SetupData
                               );
  if (EFI_ERROR (Status)) {
    DEBUG ((DEBUG_ERROR, "GetVariable (SetupData) failed\n"));
    return;
  }

  for (Index = 0; Index < MAX_DTBT_CONTROLLER_NUMBER; Index++) {

    if (Index == 0) {
      if (PcdGet32 (PcdBoarddTBTForcePwrGpio0)) {
        GpioPad[Index] = PcdGet32 (PcdBoarddTBTForcePwrGpio0);
      }
    } else {
      if (PcdGet32 (PcdBoarddTBTForcePwrGpio1)) {
        GpioPad[Index] = PcdGet32 (PcdBoarddTBTForcePwrGpio1);
      }
    }

    if (GpioPad[Index] != 0) {
      GpioGetPadConfig (GpioPad[Index], &GpioConfig);

      if ((SetupData.Gpio3ForcePwr == 1) && (SetupData.DTbtController[Index]==1)) {
        //
        // value 1: Set the GPIO pin to high active (debug mode)
        // value 0: default
        //
        GpioConfig.OutputState = GpioOutHigh;

        DEBUG ((DEBUG_INFO, "Set FP Pin\n"));
      } else {
        GpioConfig.OutputState = GpioOutLow;
        DEBUG ((DEBUG_INFO, "Clear FP Pin\n"));
      }
      GpioSetPadConfig (GpioPad[Index], &GpioConfig);
    DEBUG ((DEBUG_INFO, "dTBT Force Power Gpio for controller %d is : 0x%08x\n",Index, GpioPad[Index]));
    }
  }
  DEBUG ((DEBUG_INFO, "DTbtForcePwrGpioPreMem End\n"));
}

/**
  Configure dTBT RTD Power pin for debug mode.
**/
VOID
DTbtRTD3PwrGpioPreMem (
  VOID
  )
{
  GPIO_CONFIG                     GpioConfig;
  GPIO_PAD                        GpioPad[MAX_DTBT_CONTROLLER_NUMBER];
  EFI_STATUS                      Status;
  SETUP_DATA                      SetupData;
  UINTN                           VarSize;
  EFI_PEI_READ_ONLY_VARIABLE2_PPI *VariableServices;
  UINT8                           Index;
  DEBUG ((DEBUG_INFO, "DTbtRTD3PwrGpioPreMem Start\n"));

  ZeroMem(GpioPad, MAX_DTBT_CONTROLLER_NUMBER);
  //
  // Locate Setup variables
  //
  Status = PeiServicesLocatePpi (
             &gEfiPeiReadOnlyVariable2PpiGuid,
             0,
             NULL,
             (VOID **) &VariableServices
             );
  if (EFI_ERROR (Status)) {
    DEBUG ((DEBUG_ERROR, "PeiServicesLocatePpi failed\n"));
    return;
  }

  VarSize = sizeof (SETUP_DATA);
  Status = VariableServices->GetVariable (
                               VariableServices,
                               L"Setup",
                               &gSetupVariableGuid,
                               NULL,
                               &VarSize,
                               &SetupData
                               );
  if (EFI_ERROR (Status)) {
    DEBUG ((DEBUG_ERROR, "GetVariable (SetupData) failed\n"));
    return;
  }

  //
  // To get the DTBT GPIO setting of DTBT Controll 0 / 1
  // Set the OutputState to GpioOutLow prevents the device show in device manager when the DTBT disable or
  // disable of DTBT Controll 0 / 1
  //
  for (Index = 0; Index < MAX_DTBT_CONTROLLER_NUMBER; Index++) {
    if ((SetupData.DiscreteTbtSupport == 0) || (SetupData.DTbtController[Index] == 0)) {
      switch (Index) {
        case 0:
          GpioPad[Index] = PcdGet32 (PcdBoarddTBTRTD3PwrGpio0);
          break;
        case 1:
          GpioPad[Index] = PcdGet32 (PcdBoarddTBTRTD3PwrGpio1);
          break;
        default:
          GpioPad[Index] = 0;
          break;
      }

      if (GpioPad[Index] != 0) {
        GpioGetPadConfig (GpioPad[Index], &GpioConfig);
        GpioConfig.OutputState = GpioOutLow;
        GpioSetPadConfig (GpioPad[Index], &GpioConfig);
        DEBUG ((DEBUG_INFO, "DTBT RTD3 Gpio for controller %d is : 0x%08x\n",Index, GpioPad[Index]));
      }
    }
  }
  DEBUG ((DEBUG_INFO, "DTbtRTD3PwrGpioPreMem End\n"));
}


/**
  Configure Discrete DpMux Init PreMeme.
**/
VOID
DiscreteDpMuxInitPreMem (
  VOID
  )
{
  GPIO_CONFIG                     GpioConfig;
  GPIO_PAD                        GpioPad;
  EFI_STATUS                      Status;
  SETUP_DATA                      SetupData;
  UINTN                           VarSize;
  EFI_PEI_READ_ONLY_VARIABLE2_PPI *VariableServices;
  DEBUG ((DEBUG_INFO, "DiscreteDpMuxInitPreMem Start\n"));

  GpioPad = 0; // Assign GPIO PAD Value to 0.
  VarSize = 0;

  //
  // Locate Setup variables
  //
  Status = PeiServicesLocatePpi (
             &gEfiPeiReadOnlyVariable2PpiGuid,
             0,
             NULL,
             (VOID **) &VariableServices
             );
  if (EFI_ERROR (Status)) {
    DEBUG ((DEBUG_ERROR, "PeiServicesLocatePpi failed\n"));
    return;
  }

  VarSize = sizeof (SETUP_DATA);
  Status = VariableServices->GetVariable (
                               VariableServices,
                               L"Setup",
                               &gSetupVariableGuid,
                               NULL,
                               &VarSize,
                               &SetupData
                               );
  if (EFI_ERROR (Status)) {
    DEBUG ((DEBUG_ERROR, "GetVariable (SetupData) failed\n"));
    return;
  }

  if (PcdGet32 (PcdDpMuxGpio)) {
    GpioPad = PcdGet32 (PcdDpMuxGpio);
    GpioGetPadConfig (GpioPad, &GpioConfig);
    if (SetupData.DpInExternalEn == 1) {
      //
      // value 1: Set the GPIO pin to high active
      // value 0: default
      //
      GpioConfig.OutputState = GpioOutHigh;
      DEBUG ((DEBUG_INFO, "Enable DP Mux switch\n"));
    } else {
      GpioConfig.OutputState = GpioOutLow;
      DEBUG ((DEBUG_INFO, "Disable DP Mux switch\n"));
    }
    GpioSetPadConfig (GpioPad, &GpioConfig);
    DEBUG ((DEBUG_INFO, "Discrete DP Mux is : 0x%08x\n", GpioPad));
  } else {
    DEBUG ((DEBUG_ERROR, "PcdDpMuxGpio is NULL : %d\n", PcdGet32 (PcdDpMuxGpio)));
  }

  DEBUG ((DEBUG_INFO, "DiscreteDpMuxInitPreMem End\n"));
}
/**
  This function performs PEI TBT Policy update by board configuration.

  @retval EFI_SUCCESS             The SI Policy is successfully updated.
  @retval Others                  The SI Policy is not successfully updated.
**/
EFI_STATUS
EFIAPI
UpdatePeiTbtPolicyBoardConfig (
  VOID
  )
{
#if FixedPcdGetBool (PcdDTbtEnable) == 1
  EFI_STATUS                         Status;
  PEI_DTBT_POLICY                    *PeiDTbtConfig;
  UINT16                             BoardId;
  DEBUG ((DEBUG_INFO, "Updating TBT Policy by board config in Post-Mem\n"));

  Status = EFI_SUCCESS;
  PeiDTbtConfig = NULL;
  PcdSet32S (PcdBoarddTBTForcePwrGpio0, 0); //Initization
  PcdSet32S (PcdBoarddTBTForcePwrGpio1, 0); //Initization
  PcdSet32S (PcdBoarddTBTRTD3PwrGpio0, 0); //Initization
  PcdSet32S (PcdBoarddTBTRTD3PwrGpio1, 0); //Initization

  //
  // Ascertain BoardId
  //
  BoardId = PcdGet16 (PcdBoardId);
  DEBUG ((DEBUG_INFO, "UpdatePeiDTbtConfigBoardConfig - BoardId is 0x%04X\n", BoardId));
  Status = PeiServicesLocatePpi (
             &gPeiDTbtPolicyPpiGuid,
             0,
             NULL,
             (VOID **) &PeiDTbtConfig
             );

  if (PeiDTbtConfig != NULL) {
    //
    // Update TBT Config Block based on BoardId value.
    //
    switch (BoardId) {
      case BoardIdAdlSAdpSDdr4UDimm2DCrbEv:
      case BoardIdAdlSAdpSDdr4UDimm2DCrbCpv:
      case BoardIdAdlSAdpSDdr4UDimm2DCrb:
      case BoardIdAdlSAdpSDdr5UDimm1DCrb:
      case BoardIdAdlSAdpSDdr5UDimm2DCrb:
      case BoardIdAdlSAdpSDdr5UDimm1DCrbPpv:
      case BoardIdAdlSAdpSDdr5UDimm1DSr:
        DEBUG ((DEBUG_INFO, "Update DTBT Platform Specific Details...\n"));
        PcdSet32S (PcdBoarddTBTForcePwrGpio0,  GPIO_VER4_S_GPP_G0); //1: Set the GPIO pin to high active (debug mode); 0: default
        PeiDTbtConfig->DTbtControllerConfig[0].Type = 0x01;// TYPE_PCIE;//DTBT_TYPE_PCH
        PeiDTbtConfig->DTbtControllerConfig[0].PcieRpNumber = 9; //Connected to RP 09
        PeiDTbtConfig->DTbtControllerConfig[0].CioPlugEventGpio.GpioPad = GPIO_VER4_S_GPP_G7;
        PeiDTbtConfig->DTbtControllerConfig[0].PcieRstGpio.GpioPad = 0;
        PeiDTbtConfig->DTbtControllerConfig[0].PcieRstGpio.GpioLevel = 0;
        PeiDTbtConfig->DTbtGenericConfig.DTbtSharedGpioConfiguration = TRUE;
        PeiDTbtConfig->DTbtGenericConfig.PcieRstSupport = FALSE;
        if (GpioCheckFor2Tier (GPIO_VER4_S_GPP_G7)) {
          PeiDTbtConfig->DTbtControllerConfig[0].CioPlugEventGpio.AcpiGpeSignature = SIGNATURE_32 ('X', 'T', 'B', 'T');
          PeiDTbtConfig->DTbtControllerConfig[0].CioPlugEventGpio.AcpiGpeSignaturePorting = 0;
        }
        break;

      case BoardIdAdlSAdpSDdr5SODimmCrb:
      case BoardIdAdlSAdpSDdr4SODimmCrb:
        DEBUG((DEBUG_INFO, "Update DTBT Platform Specific Details...\n"));
        PcdSet32S (PcdBoarddTBTForcePwrGpio0,  GPIO_VER4_S_GPP_G0); //1: Set the GPIO pin to high active (debug mode); 0: default
        PeiDTbtConfig->DTbtControllerConfig[0].Type = 0x01;// TYPE_PCIE;//DTBT_TYPE_PCH
        PeiDTbtConfig->DTbtControllerConfig[0].PcieRpNumber = 25; //Connected to RP 25
        PeiDTbtConfig->DTbtControllerConfig[0].CioPlugEventGpio.GpioPad = GPIO_VER4_S_GPP_G7;
        PeiDTbtConfig->DTbtControllerConfig[0].PcieRstGpio.GpioPad = 0;
        PeiDTbtConfig->DTbtControllerConfig[0].PcieRstGpio.GpioLevel = 0;
        PeiDTbtConfig->DTbtGenericConfig.DTbtSharedGpioConfiguration = TRUE;
        PeiDTbtConfig->DTbtGenericConfig.PcieRstSupport = FALSE;
        if (GpioCheckFor2Tier (GPIO_VER4_S_GPP_G7)) {
          PeiDTbtConfig->DTbtControllerConfig[0].CioPlugEventGpio.AcpiGpeSignature = SIGNATURE_32 ('X', 'T', 'B', 'T');
          PeiDTbtConfig->DTbtControllerConfig[0].CioPlugEventGpio.AcpiGpeSignaturePorting = 0;
        }
        break;

      case BoardIdAdlSAdpSDdr5UDimm1DAep:
        DEBUG ((DEBUG_INFO, "Update DTBT Platform Specific Details for AEP...\n"));
        PcdSet32S (PcdBoarddTBTForcePwrGpio0, (UINTN) GPIO_VER4_S_GPP_G0); //1: Set the GPIO pin to high active (debug mode); 0: default
        PeiDTbtConfig->DTbtControllerConfig[0].Type = 0x01;// TYPE_PCIE;//DTBT_TYPE_PCH
        PeiDTbtConfig->DTbtControllerConfig[0].PcieRpNumber = 21; //Connected to RP 21
        PeiDTbtConfig->DTbtControllerConfig[0].CioPlugEventGpio.GpioPad = GPIO_VER4_S_GPP_G7;
        PeiDTbtConfig->DTbtControllerConfig[0].PcieRstGpio.GpioPad = 0;
        PeiDTbtConfig->DTbtControllerConfig[0].PcieRstGpio.GpioLevel = 0;
        PeiDTbtConfig->DTbtGenericConfig.DTbtSharedGpioConfiguration = FALSE;
        PeiDTbtConfig->DTbtGenericConfig.PcieRstSupport = FALSE;
        if (GpioCheckFor2Tier(GPIO_VER4_S_GPP_G7)) {
          PeiDTbtConfig->DTbtControllerConfig[0].CioPlugEventGpio.AcpiGpeSignature = SIGNATURE_32('X', 'T', 'B', 'T');
          PeiDTbtConfig->DTbtControllerConfig[0].CioPlugEventGpio.AcpiGpeSignaturePorting = 0;
        }
        PcdSet32S (PcdBoarddTBTForcePwrGpio1, (UINTN) GPIO_VER4_S_GPP_D13); //1: Set the GPIO pin to high active (debug mode); 0: default
        PeiDTbtConfig->DTbtControllerConfig[1].Type = 0x01;// TYPE_PCIE;//DTBT_TYPE_PCH
        PeiDTbtConfig->DTbtControllerConfig[1].PcieRpNumber = 25; //Connected to RP 25
        PeiDTbtConfig->DTbtControllerConfig[1].CioPlugEventGpio.GpioPad = GPIO_VER4_S_GPP_D14;
        PeiDTbtConfig->DTbtControllerConfig[1].PcieRstGpio.GpioPad = 0;
        PeiDTbtConfig->DTbtControllerConfig[1].PcieRstGpio.GpioLevel = 0;
        if (GpioCheckFor2Tier(GPIO_VER4_S_GPP_D14)) {
          PeiDTbtConfig->DTbtControllerConfig[1].CioPlugEventGpio.AcpiGpeSignature = SIGNATURE_32('X', 'T', 'B', 'T');
          PeiDTbtConfig->DTbtControllerConfig[1].CioPlugEventGpio.AcpiGpeSignaturePorting = 0;
        }
        break;

      case BoardIdAdlSAdpSSbgaDdr5SODimmErb:
      case BoardIdAdlSAdpSSbgaDdr5SODimmCrb:
        DEBUG ((DEBUG_INFO, "Update DTBT Platform Specific Details for SBGA DDR5...\n"));
        PcdSet32S (PcdBoarddTBTForcePwrGpio0, (UINTN) GPIO_VER4_S_GPP_G0); //1: Set the GPIO pin to high active (debug mode); 0: default
        PeiDTbtConfig->DTbtControllerConfig[0].Type = 0x01;// TYPE_PCIE;//DTBT_TYPE_PCH
        PeiDTbtConfig->DTbtControllerConfig[0].PcieRpNumber = 25; //Connected to RP 25
        PeiDTbtConfig->DTbtControllerConfig[0].CioPlugEventGpio.GpioPad = GPIO_VER4_S_GPP_G7;
        PeiDTbtConfig->DTbtControllerConfig[0].PcieRstGpio.GpioPad = GPIO_VER4_S_GPP_F16;
        PeiDTbtConfig->DTbtControllerConfig[0].PcieRstGpio.GpioLevel = 0;
        PeiDTbtConfig->DTbtGenericConfig.DTbtSharedGpioConfiguration = FALSE;
        PeiDTbtConfig->DTbtGenericConfig.PcieRstSupport = TRUE;
        if (GpioCheckFor2Tier(GPIO_VER4_S_GPP_G7)) {
          PeiDTbtConfig->DTbtControllerConfig[0].CioPlugEventGpio.AcpiGpeSignature = SIGNATURE_32('X', 'T', 'B', 'T');
          PeiDTbtConfig->DTbtControllerConfig[0].CioPlugEventGpio.AcpiGpeSignaturePorting = 0;
        }
        PcdSet32S (PcdBoarddTBTForcePwrGpio1, (UINTN) GPIO_VER4_S_GPP_G0); //1: Set the GPIO pin to high active (debug mode); 0: default
        PeiDTbtConfig->DTbtControllerConfig[1].Type = 0x01;// TYPE_PCIE;//DTBT_TYPE_PCH
        PeiDTbtConfig->DTbtControllerConfig[1].PcieRpNumber = 9; //Connected to RP 9
        PeiDTbtConfig->DTbtControllerConfig[1].CioPlugEventGpio.GpioPad = GPIO_VER4_S_GPP_B22;
        PeiDTbtConfig->DTbtControllerConfig[1].PcieRstGpio.GpioPad = GPIO_VER4_S_GPP_F2;
        PeiDTbtConfig->DTbtControllerConfig[1].PcieRstGpio.GpioLevel = 0;
        if (GpioCheckFor2Tier(GPIO_VER4_S_GPP_B22)) {
          PeiDTbtConfig->DTbtControllerConfig[1].CioPlugEventGpio.AcpiGpeSignature = SIGNATURE_32('X', 'T', 'B', 'T');
          PeiDTbtConfig->DTbtControllerConfig[1].CioPlugEventGpio.AcpiGpeSignaturePorting = 0;
        }
        break;
      case BoardIdAdlSAdpSSbgaDdr5SODimmAep:
        DEBUG ((DEBUG_INFO, "Update DTBT Platform Specific Details for SBGA DDR5 AEP...\n"));
        PcdSet32S (PcdBoarddTBTForcePwrGpio0, (UINTN) GPIO_VER4_S_GPP_G0); //1: Set the GPIO pin to high active (debug mode); 0: default
        PeiDTbtConfig->DTbtControllerConfig[0].Type = 0x01;// TYPE_PCIE;//DTBT_TYPE_PCH
        PeiDTbtConfig->DTbtControllerConfig[0].PcieRpNumber = 9; //Connected to RP 9
        PeiDTbtConfig->DTbtControllerConfig[0].CioPlugEventGpio.GpioPad = GPIO_VER4_S_GPP_G7;
        PeiDTbtConfig->DTbtControllerConfig[0].PcieRstGpio.GpioPad = GPIO_VER4_S_GPP_F16;
        PeiDTbtConfig->DTbtControllerConfig[0].PcieRstGpio.GpioLevel = 0;
        PeiDTbtConfig->DTbtGenericConfig.DTbtSharedGpioConfiguration = 0;
        PeiDTbtConfig->DTbtGenericConfig.PcieRstSupport = 0;
        if (GpioCheckFor2Tier(GPIO_VER4_S_GPP_G7)) {
          PeiDTbtConfig->DTbtControllerConfig[0].CioPlugEventGpio.AcpiGpeSignature = SIGNATURE_32('X', 'T', 'B', 'T');
          PeiDTbtConfig->DTbtControllerConfig[0].CioPlugEventGpio.AcpiGpeSignaturePorting = 0;
        }
        break;
      case BoardIdAdlSAdpSSbgaDdr4SODimmCrb:
        DEBUG ((DEBUG_INFO, "Update DTBT Platform Specific Details for SBGA DDR4...\n"));
        PcdSet32S (PcdBoarddTBTForcePwrGpio0, (UINTN) GPIO_VER4_S_GPP_G0); //1: Set the GPIO pin to high active (debug mode); 0: default
        PcdSet32S (PcdBoarddTBTRTD3PwrGpio0, (UINTN) GPIO_VER4_S_GPP_F5);
        PeiDTbtConfig->DTbtControllerConfig[0].Type = 0x01;// TYPE_PCIE;//DTBT_TYPE_PCH
        PeiDTbtConfig->DTbtControllerConfig[0].PcieRpNumber = 25; //Connected to RP 25
        PeiDTbtConfig->DTbtControllerConfig[0].CioPlugEventGpio.GpioPad = GPIO_VER4_S_GPP_G7;
        PeiDTbtConfig->DTbtControllerConfig[0].PcieRstGpio.GpioPad = GPIO_VER4_S_GPP_F16;
        PeiDTbtConfig->DTbtControllerConfig[0].PcieRstGpio.GpioLevel = 0;
        PeiDTbtConfig->DTbtGenericConfig.DTbtSharedGpioConfiguration = FALSE;
        PeiDTbtConfig->DTbtGenericConfig.PcieRstSupport = TRUE;
        if (GpioCheckFor2Tier(GPIO_VER4_S_GPP_G7)) {
          PeiDTbtConfig->DTbtControllerConfig[0].CioPlugEventGpio.AcpiGpeSignature = SIGNATURE_32('X', 'T', 'B', 'T');
          PeiDTbtConfig->DTbtControllerConfig[0].CioPlugEventGpio.AcpiGpeSignaturePorting = 0;
        }
        DTbtRTD3PwrGpioPreMem ();
        break;
      case BoardIdAdlPDdr5MRRvp:
        DEBUG((DEBUG_INFO, "Update DTBT Platform Specific Details for ADL-P DDR5 2 DTBT MR Controller Configuration...\n"));
        PcdSet32S (PcdBoarddTBTForcePwrGpio0, (UINTN) GPIO_VER2_LP_GPP_D12); //1: Set the GPIO pin to high active (debug mode); 0: default
        PeiDTbtConfig->DTbtControllerConfig[0].Type = 0x01;// TYPE_PCIE;//DTBT_TYPE_PCH
        PeiDTbtConfig->DTbtControllerConfig[0].PcieRpNumber = 5; //Connected to RP 5-8

        PcdSet32S (PcdBoarddTBTForcePwrGpio1, (UINTN) GPIO_VER2_LP_GPP_D12); //1: Set the GPIO pin to high active (debug mode); 0: default
        PeiDTbtConfig->DTbtControllerConfig[1].Type = 0x01;// TYPE_PCIE;//DTBT_TYPE_PCH
        PeiDTbtConfig->DTbtControllerConfig[1].PcieRpNumber = 9; //Connected to RP 9-12
        break;
      default:
        DEBUG ((DEBUG_INFO, "UpdatePeiDTbtConfigBoardConfig - Unhandled BoardId 0x%04X\n", BoardId));
        break;
    }

    //
    // USB4 SW CM support setting
    //
    PeiDTbtConfig->DTbtGenericConfig.Usb4CmMode = USB4_CM_MODE_SWITCH_UNSUPPORTED;
    DEBUG ((DEBUG_INFO, "DTBT USB4 CM mode = 0x%0x in board config init\n", PeiDTbtConfig->DTbtGenericConfig.Usb4CmMode));
  } // if (PeiDTbtConfig != NULL)

  //
  // Discrete DP Mux Switch GPIO Setting
  // The setup option will be hidden if PcdDpMuxGpio is NULL
  //
  switch (BoardId) {
    case BoardIdAdlSAdpSSbgaDdr5SODimmErb:
    case BoardIdAdlSAdpSSbgaDdr5SODimmCrb:
      PcdSet32S (PcdDpMuxGpio, GPIO_VER4_S_GPP_F0);
      break;
    case BoardIdAdlPDdr5MRRvp:
      PcdSet32S (PcdDpMuxGpio, GPIO_VER2_LP_GPP_A7);
      break;
    default:
      DEBUG ((DEBUG_INFO, "PcdDpMuxGpio is unsupported\n"));
      break;
  }
  DiscreteDpMuxInitPreMem ();
  DTbtForcePwrGpioPreMem ();

  return Status;
#else
  return EFI_SUCCESS;
#endif
}
