/** @file
 Intel PEI SA Policy update by board configuration

@copyright
  INTEL CONFIDENTIAL
  Copyright 2017 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains a 'Sample Driver' and is licensed as such under the terms
  of your license agreement with Intel or your vendor. This file may be modified
  by the user, subject to the additional terms of the license agreement.

@par Specification Reference:
**/
#include "PeiPolicyBoardConfig.h"
#if FixedPcdGetBool(PcdITbtEnable) == 1
#include <TcssPeiPreMemConfig.h>
#include <TcssPeiConfig.h>
#include <PlatformBoardConfig.h>
#endif
#include <Pins/GpioPinsVer2Lp.h>
#include <PlatformBoardId.h>
#if FixedPcdGetBool(PcdITbtEnable) == 1
#include <TcssInfo.h>
#endif
#include <PolicyUpdateMacro.h>

#if FixedPcdGetBool(PcdITbtEnable) == 1
//[-start-210804-QINGLIN0008-add]//
//[-start-210802-SHAONN0003-add]//
#if defined(S370_SUPPORT)
  IOM_AUX_ORI_PAD_CONFIG mIomAuxAdlPTable[MAX_IOM_AUX_BIAS_COUNT] = {
    // Pull UP GPIO Pin, Pull Down GPIO pin
    {GPIO_VER2_LP_GPP_E22, GPIO_VER2_LP_GPP_E23}, // Port 0
    {0,                  0}, // Port 1
    {0,                  0}, // Port 2
    {0,                  0}, // Port 3
  };
#else
IOM_AUX_ORI_PAD_CONFIG mIomAuxAdlPTable[MAX_IOM_AUX_BIAS_COUNT] = {
  // Pull UP GPIO Pin, Pull Down GPIO pin
  {0,                  0}, // Port 0
  {0,                  0}, // Port 1
  {GPIO_VER2_LP_GPP_E23, GPIO_VER2_LP_GPP_E22},  // Port 2
  {0,                  0}, // Port 3
};
#endif
//[-end-210802-SHAONN0003-add]//
//[-end-210804-QINGLIN0008-add]//
IOM_AUX_ORI_PAD_CONFIG mIomAuxAdlMLp4xTable[MAX_IOM_AUX_BIAS_COUNT] = {
  // Pull UP GPIO Pin, Pull Down GPIO pin
  {0,                  0}, // Port 0
  {GPIO_VER2_LP_GPP_E23, GPIO_VER2_LP_GPP_E22},  // Port 1
  {0,                  0}, // Port 2
  {0,                  0}, // Port 3
};

IOM_AUX_ORI_PAD_CONFIG mIomAuxAdlMLp5Table[MAX_IOM_AUX_BIAS_COUNT] = {
  // Pull UP GPIO Pin, Pull Down GPIO pin
  {GPIO_VER2_LP_GPP_E23, GPIO_VER2_LP_GPP_E22},  // Port 0
  {0,                  0}, // Port 1
  {0,                  0}, // Port 2
  {0,                  0}, // Port 3
};
IOM_AUX_ORI_PAD_CONFIG mIomAuxNullTable[MAX_IOM_AUX_BIAS_COUNT] = {
  // Pull UP GPIO Pin, Pull Down GPIO pin
  {0,                  0}, // Port 0
  {0,                  0}, // Port 1
  {0,                  0}, // Port 2
  {0,                  0}, // Port 3
};

IOM_AUX_ORI_PAD_CONFIG mIomAuxAdlPLp5T3Table[MAX_IOM_AUX_BIAS_COUNT] = {
  // Pull UP GPIO Pin, Pull Down GPIO pin
  {0,                  0}, // Port 0
  {GPIO_VER2_LP_GPP_E20, GPIO_VER2_LP_GPP_E21}, // Port 1
  {GPIO_VER2_LP_GPP_E22, GPIO_VER2_LP_GPP_E23}, // Port 2
  {GPIO_VER2_LP_GPP_D11, GPIO_VER2_LP_GPP_D12}, // Port 3
};
#endif // PcdITbtEnable

/**
  This function performs PEI SA Policy update by board configuration.

  @retval EFI_SUCCESS             The SI Policy is successfully updated.
  @retval Others                  The SI Policy is not successfully updated.
**/
EFI_STATUS
EFIAPI
UpdatePeiSaPolicyBoardConfig (
  VOID
  )
{
  EFI_STATUS                         Status;
#if FixedPcdGet8(PcdFspModeSelection) == 1
  VOID                               *FspsUpd;
#else
#if FixedPcdGetBool(PcdITbtEnable) == 1
  TCSS_PEI_CONFIG                    *TcssConfig;
  SI_POLICY_PPI                      *SiPolicyPpi;
#endif
#endif
#if FixedPcdGetBool(PcdITbtEnable) == 1
  UINT16                             BoardId;
  UINT16                             TcssIomAuxOriEnaBitMap;
  USB_OC_MAP_TABLE                   *UsbOcMappingTable;
  UINT8                              PortCount;
  UINT32                             PortIndex;
  IOM_AUX_ORI_PAD_CONFIG             *IomAuxOriOverrideTablePtr;
#endif
  EFI_PEI_READ_ONLY_VARIABLE2_PPI    *VariableServices;
  UINTN                              VariableSize;
  SETUP_DATA                         SetupData;

  DEBUG ((DEBUG_INFO, "Updating SA Policy by board config in Post-Mem\n"));

  Status = EFI_SUCCESS;
#if FixedPcdGetBool(PcdITbtEnable) == 1
  UsbOcMappingTable  = NULL;
  PortIndex = 0;
  PortCount = 0;
#endif
#if FixedPcdGet8(PcdFspModeSelection) == 1
  FspsUpd = NULL;
#else
#if FixedPcdGetBool(PcdITbtEnable) == 1
  SiPolicyPpi = NULL;
  TcssConfig  = NULL;
#endif
#endif

#if FixedPcdGet8(PcdFspModeSelection) == 1
  FspsUpd = (FSPS_UPD *) PcdGet32 (PcdFspsUpdDataAddress);
  ASSERT (FspsUpd != NULL);
  if (FspsUpd == NULL){
    return EFI_OUT_OF_RESOURCES;
  }
#else
#if FixedPcdGetBool(PcdITbtEnable) == 1
  Status = PeiServicesLocatePpi (&gSiPolicyPpiGuid, 0, NULL, (VOID **) &SiPolicyPpi);
  ASSERT_EFI_ERROR(Status);

  Status = GetConfigBlock ((VOID *) SiPolicyPpi, &gTcssPeiConfigGuid, (VOID *) &TcssConfig);
  ASSERT_EFI_ERROR (Status);
#endif  // PcdITbtEnable
#endif

  Status = PeiServicesLocatePpi (
             &gEfiPeiReadOnlyVariable2PpiGuid,
             0,
             NULL,
             (VOID **) &VariableServices
             );
  ASSERT_EFI_ERROR (Status);

  VariableSize = sizeof (SETUP_DATA);
  Status = VariableServices->GetVariable (
                  VariableServices,
                  L"Setup",
                  &gSetupVariableGuid,
                  NULL,
                  &VariableSize,
                  &SetupData
                  );
  ASSERT_EFI_ERROR (Status);

#if FixedPcdGetBool(PcdITbtEnable) == 1
#if FixedPcdGet8(PcdFspModeSelection) == 1
  if (FspsUpd != NULL) {
#else
  if (TcssConfig != NULL) {
#endif

    //
    // Ascertain BoardId
    //
    BoardId = PcdGet16(PcdBoardId);
    DEBUG((DEBUG_INFO, "UpdatePeiSaPolicyBoardConfig - BoardId is 0x%04X\n", BoardId));

    //
    // Initialize TCSS IOM_AUX_ORI_ENA BIT MAP to 0
    //
    TcssIomAuxOriEnaBitMap = 0;

    //
    // Initialize Aux and HSL orientation Table to mIomAuxNullTable.
    //
    IomAuxOriOverrideTablePtr = mIomAuxNullTable;

    //
    // Set Aux and HSL orientation Table based on BoardId value if AuxOriOverride is enable.
    //
    // TODO: As platforms are defined and added, new cases and values will need to be
    //       added to the below switch to set Aux and HSL orientations as appropriate.
    //
    if (SetupData.AuxOriOverride == 1) {
      switch (BoardId) {
        case BoardIdAdlPSimics:
        case BoardIdAdlPLp5Rvp:
        case BoardIdAdlPDdr4Rvp:
          IomAuxOriOverrideTablePtr = mIomAuxAdlPTable;
          break;
        case BoardIdAdlPT3Lp5Rvp:
          IomAuxOriOverrideTablePtr = mIomAuxAdlPLp5T3Table;
          break;
        case BoardIdAdlMLp4Rvp:
          IomAuxOriOverrideTablePtr = mIomAuxAdlMLp4xTable;
          break;
        case BoardIdAdlMLp5Rvp:
        case BoardIdAdlMLp5Rvp2a:
          IomAuxOriOverrideTablePtr = mIomAuxAdlMLp5Table;
          break;
        default:
          IomAuxOriOverrideTablePtr = mIomAuxNullTable;
          DEBUG ((DEBUG_INFO, "UpdatePeiSaPolicyBoardConfig - Unhandled BoardId 0x%04X\n", BoardId));
          break;
      }
    }
    for (PortIndex = 0; PortIndex < MAX_IOM_AUX_BIAS_COUNT; PortIndex++) {
      //
      // The TypeC Port GPIO config setting reference on board design
      //
      UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.IomTypeCPortPadCfg[(PortIndex * 2)],     TcssConfig->IomConfig.IomAuxPortPad[PortIndex].GpioPullN, IomAuxOriOverrideTablePtr[PortIndex].GpioPullN);
      UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.IomTypeCPortPadCfg[(PortIndex * 2) + 1], TcssConfig->IomConfig.IomAuxPortPad[PortIndex].GpioPullP, IomAuxOriOverrideTablePtr[PortIndex].GpioPullP);
      if ((IomAuxOriOverrideTablePtr[PortIndex].GpioPullN != 0) && (IomAuxOriOverrideTablePtr[PortIndex].GpioPullP != 0)) {
        TcssIomAuxOriEnaBitMap |= (BIT0 << (PortIndex * 2));
      }
    }
    UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.TcssAuxOri, TcssConfig->IomConfig.IomOverrides.AuxOri, (UINT16) TcssIomAuxOriEnaBitMap);
    UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.TcssHslOri, TcssConfig->IomConfig.IomOverrides.HslOri, 0);

#if FixedPcdGetBool(PcdFspModeSelection) == 1
    DEBUG ((DEBUG_INFO, "UpdatePeiSaPolicyBoardConfig - AuxOri is 0x%04X\n", ((FSPS_UPD *) FspsUpd)->FspsConfig.TcssAuxOri));
#else
    DEBUG ((DEBUG_INFO, "UpdatePeiSaPolicyBoardConfig - AuxOri is 0x%04X\n", TcssConfig->IomConfig.IomOverrides.AuxOri));
#endif

    //
    // Updating CPU USB3 OC pin mapping
    //
    UsbOcMappingTable = (USB_OC_MAP_TABLE *) PcdGet32 (PcdCpuUsb30OverCurrentPinTable);
    if (UsbOcMappingTable == NULL) {
      DEBUG ((DEBUG_INFO, "UpdatePeiSaPolicyBoardConfig: No data for CPU USB3 OC pin mapping provided.\n"));
      return EFI_INVALID_PARAMETER;
    }

  //
  // Warning for size mismatch between data and port count
  //
  if (UsbOcMappingTable->Size > MAX_TCSS_USB3_PORTS) {
    DEBUG ((
      DEBUG_WARN,
      "UpdatePeiSaPolicyBoardConfig: Mismatch between CPU USB3 OC mapping table size and port count. Got: %d\n",
      UsbOcMappingTable->Size
      ));
  }

  //
  // To avoid overflow get minimum from either table size or max USB2 port count
  //
  PortCount = MIN (MAX_TCSS_USB3_PORTS, UsbOcMappingTable->Size);
  DEBUG ((DEBUG_INFO, "UpdatePeiSaPolicyBoardConfig: Updating CPU USB OC mapping for %d USB3 ports.\n", PortCount));

  for (PortIndex = 0; PortIndex < PortCount; PortIndex++) {
    if ((UsbOcMappingTable->Data[PortIndex] < USB_OC_MAX_PINS) || (UsbOcMappingTable->Data[PortIndex] == USB_OC_SKIP)) {
      UPDATE_POLICY (
        ((FSPS_UPD *)FspsUpd)->FspsConfig.CpuUsb3OverCurrentPin[PortIndex],
        TcssConfig->UsbConfig.PortUsb30[PortIndex].OverCurrentPin,
        UsbOcMappingTable->Data[PortIndex]
        );
    } else {
      DEBUG ((DEBUG_ERROR, "UpdatePeiSaPolicyBoardConfig: Invalid OverCurrent pin specified CPU USB3 port %d\n", PortIndex));
    }
  }

#if FixedPcdGet8(PcdFspModeSelection) == 1
    ((FSPS_UPD *) FspsUpd)->FspsConfig.UsbTcPortEn = PcdGet8 (PcdCpuUsb30PortEnable);
#else
    for (PortIndex = 0; PortIndex < MAX_TCSS_USB3_PORTS; PortIndex++) {
      TcssConfig->UsbConfig.PortUsb30[PortIndex].Enable             = (PcdGet8 (PcdCpuUsb30PortEnable) >> PortIndex) & 0x01;
    }
#endif
  }
#endif  // PcdITbtEnable
  return Status;
}
