/** @file
  Source code file for Silicon Init Pre-Memory Library

@copyright
  INTEL CONFIDENTIAL
  Copyright 2019 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains a 'Sample Driver' and is licensed as such under the terms
  of your license agreement with Intel or your vendor. This file may be modified
  by the user, subject to the additional terms of the license agreement.

@par Specification Reference:
**/

#include <PiPei.h>

#include <Library/BaseLib.h>
#include <Library/PcdLib.h>
#include <Library/DebugLib.h>
#include <Library/BaseMemoryLib.h>
#include <Library/PostCodeLib.h>
#include <Library/PchCycleDecodingLib.h>
#include <Library/PciSegmentLib.h>
#include <Library/PeiServicesLib.h>
#include <Register/PchRegsLpc.h>
#include <Register/PchRegs.h>
#include <Library/PchPciBdfLib.h>
#include <Pins/GpioPinsVer4S.h>
#include <Library/GpioLib.h>
#include <Library/GpioConfig.h>
#include <Include/PlatformBoardId.h>

typedef struct {
  GPIO_PAD                          GpioPad;
  GPIO_ELECTRICAL_CONFIG            Value;
}PadWaTable;

PadWaTable SiliconPadWaTblS03Fab1[] = {
  {GPIO_VER4_S_C10_WAKE, GpioTermWpd20K}  //C10_Wake with 20k pull-down
};

PadWaTable SiliconPadWaTblAll[] = {
  {GPIO_VER4_S_GPP_B8,   GpioTermWpu20K}, //Ish_GP_0 with 20k pull-up
  {GPIO_VER4_S_GPP_B9,   GpioTermWpu20K}, //Ish_GP_1 with 20k pull-up
  {GPIO_VER4_S_GPP_B10,  GpioTermWpu20K}, //Ish_GP_2 with 20k pull-up
  {GPIO_VER4_S_GPP_B15,  GpioTermWpu20K}, //Ish_GP_3 with 20k pull-up
  {GPIO_VER4_S_GPP_B16,  GpioTermWpu20K}, //Ish_GP_4 with 20k pull-up
  {GPIO_VER4_S_GPP_B17,  GpioTermWpu20K}, //Ish_GP_5 with 20k pull-up
  {GPIO_VER4_S_GPP_B5,   GpioTermWpu20K}, //Ish_GP_6 with 20k pull-up
  {GPIO_VER4_S_GPP_B7,   GpioTermWpu20K}  //Ish_GP_7 with 20k pull-up
};

/**
  SiliconPadWa

  @param[in]   None

  @retval[out] None
**/
VOID SiliconPadWa(
  IN UINT16 BoardId,
  IN UINT16 FabId
  )
{
  EFI_STATUS                        Status;
  UINT8                             Index;

  switch (BoardId) {
    case BoardIdAdlSAdpSDdr4UDimm2DErb1:
    case BoardIdAdlSAdpSDdr4UDimm2DCrbEv:
    case BoardIdAdlSAdpSDdr4UDimm2DCrb:
      if (FabId < FabIdAdlSAdpSDdr4UDimm2DCrbRev1) {
        // For S03 Fab1
        for (Index = 0; Index < sizeof (SiliconPadWaTblS03Fab1) / sizeof (PadWaTable); Index++) {
          // set termimation of Pad, the owner of pad must be host.
          Status = GpioSetPadElectricalConfig (SiliconPadWaTblS03Fab1[Index].GpioPad, SiliconPadWaTblS03Fab1[Index].Value);
          ASSERT_EFI_ERROR(Status);
        }
      }
      break;
    default:
      break;
  }

  // For all sku
  for (Index = 0; Index < sizeof (SiliconPadWaTblAll) / sizeof (PadWaTable); Index++) {
    //set termimation of Pad, the owner of pad must be host.
    Status = GpioSetPadElectricalConfig (SiliconPadWaTblAll[Index].GpioPad, SiliconPadWaTblAll[Index].Value);
    ASSERT_EFI_ERROR(Status);
  }
}

/**
  Early Platform PCH initialization
**/
VOID
EarlyLpcIoDecode (
  VOID
  )
{
  UINT16                            LpcIoe;
  UINT16                            LpcIoeOrg;
  UINT64                            LpcBaseAddress;

  ///
  /// LPC I/O Configuration
  ///
  PchLpcIoDecodeRangesSet (
    (V_LPC_CFG_IOD_LPT_378  << N_LPC_CFG_IOD_LPT)  |
    (V_LPC_CFG_IOD_COMB_3E8 << N_LPC_CFG_IOD_COMB) |
    (V_LPC_CFG_IOD_COMA_3F8 << N_LPC_CFG_IOD_COMA)
    );

  PchLpcIoEnableDecodingSet (
    B_LPC_CFG_IOE_ME2  |
    B_LPC_CFG_IOE_SE   |
    B_LPC_CFG_IOE_ME1  |
    B_LPC_CFG_IOE_KE   |
    B_LPC_CFG_IOE_HGE  |
    B_LPC_CFG_IOE_LGE  |
    B_LPC_CFG_IOE_FDE  |
    B_LPC_CFG_IOE_PPE  |
    B_LPC_CFG_IOE_CBE  |
    B_LPC_CFG_IOE_CAE
    );

  ///
  /// Enable LPC IO decode for EC access
  ///
  LpcBaseAddress = LpcPciCfgBase ();
  LpcIoeOrg = PciSegmentRead16 (LpcBaseAddress + R_LPC_CFG_IOE);
  LpcIoe = LpcIoeOrg | B_LPC_CFG_IOE_ME1;
#if FixedPcdGet8(PcdEmbeddedEnable) == 0x1
  // Enable LPC IO decode for SIO (0x2e) access.
  LpcIoe = LpcIoeOrg | B_LPC_CFG_IOE_SE;
  LpcIoeOrg |= B_LPC_CFG_IOE_SE;
#endif

  PchLpcIoEnableDecodingSet (LpcIoe);

  if (PcdGetBool (PcdEcPresent) == FALSE) {
    ///
    /// Restore LPC IO decode setting
    ///
    PchLpcIoEnableDecodingSet (LpcIoeOrg);
  }

}

// @todo: It should be moved Policy Init.
/**
  Initialize the GPIO IO selection, GPIO USE selection, and GPIO signal inversion registers

**/
VOID
SiliconInit (
  VOID
  )
{
}

