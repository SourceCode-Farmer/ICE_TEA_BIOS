/** @file

    Source code file for the Report Firmware Volume (FV) library

@copyright
  INTEL CONFIDENTIAL
  Copyright 2018 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains a 'Sample Driver' and is licensed as such under the terms
  of your license agreement with Intel or your vendor. This file may be modified
  by the user, subject to the additional terms of the license agreement.

@par Specification Reference:
**/

#include <Base.h>
#include <PiPei.h>
#include <Library/BaseMemoryLib.h>
#include <Library/DebugLib.h>
#include <Library/HobLib.h>
#include <Library/PcdLib.h>
#include <Library/PeiServicesLib.h>
#include <Library/ReportFvLib.h>
#include <Guid/FirmwareFileSystem2.h>
#include <Ppi/FirmwareVolumeInfo.h>
#include <Ppi/ReadOnlyVariable2.h>
#include <BootStateLib.h>
#include <SetupVariable.h>
#include <Guid/BootStateCapsule.h>
#include <Library/PcdLib.h>
#include <Pi/PiStatusCode.h>
#include <Library/MtrrLib.h>
#include <Library/ReportStatusCodeLib.h>

EFI_STATUS
EFIAPI
InstallFvExtendedPostMemoryCallback (
  IN EFI_PEI_SERVICES           **PeiServices,
  IN EFI_PEI_NOTIFY_DESCRIPTOR  *NotifyDescriptor,
  IN VOID                       *Ppi
  );

EFI_STATUS
EFIAPI
InstallFvExtendedAdvancedCallback (
  IN EFI_PEI_SERVICES           **PeiServices,
  IN EFI_PEI_NOTIFY_DESCRIPTOR  *NotifyDescriptor,
  IN VOID                       *Ppi
  );

/**
  Notify list for FV installation to the memory for Extended BIOS Region.
  Each entry of the notify list may need to be registered based on a boot path.
  Make sure what FV is installed by each callback and notify them per needs.
**/
static EFI_PEI_NOTIFY_DESCRIPTOR mExtendedBiosDecodeReadyNotifyList [] = {
  {
    (EFI_PEI_PPI_DESCRIPTOR_NOTIFY_CALLBACK | EFI_PEI_PPI_DESCRIPTOR_TERMINATE_LIST),
    &gExtendedBiosDecodeReadyPpiGuid,
    InstallFvExtendedPostMemoryCallback
  },
  {
    (EFI_PEI_PPI_DESCRIPTOR_NOTIFY_CALLBACK | EFI_PEI_PPI_DESCRIPTOR_TERMINATE_LIST),
    &gExtendedBiosDecodeReadyPpiGuid,
    InstallFvExtendedAdvancedCallback
  },
};

VOID
PrintFvHeaderInfo (
  EFI_FIRMWARE_VOLUME_HEADER  *FvHeader
  );

VOID
ReportPreMemFv (
  VOID
  )
{
  /*
    Note : FSP FVs except FSP-T FV are installed in IntelFsp2Wrapper Pkg or FspPkg in Dispatch mode.
  */
  if (FixedPcdGetBool (PcdFspWrapperBootMode)) {
    DEBUG ((DEBUG_INFO, "Install FlashFvFspT - 0x%x, 0x%x\n", PcdGet32 (PcdFlashFvFspTBase), PcdGet32 (PcdFlashFvFspTSize)));
    PeiServicesInstallFvInfo2Ppi (
      &(((EFI_FIRMWARE_VOLUME_HEADER *) (UINTN) PcdGet32 (PcdFlashFvFspTBase))->FileSystemGuid),
      (VOID *) (UINTN) PcdGet32 (PcdFlashFvFspTBase),
      PcdGet32 (PcdFlashFvFspTSize),
      NULL,
      NULL,
      0
      );
  }


  DEBUG ((DEBUG_INFO, "Install FlashFvFirmwareBinaries - 0x%x, 0x%x\n", PcdGet32 (PcdFlashFvFirmwareBinariesBase), PcdGet32 (PcdFlashFvFirmwareBinariesSize)));
  PeiServicesInstallFvInfo2Ppi (
    &(((EFI_FIRMWARE_VOLUME_HEADER *) (UINTN) PcdGet32 (PcdFlashFvFirmwareBinariesBase))->FileSystemGuid),
    (VOID *) (UINTN) PcdGet32 (PcdFlashFvFirmwareBinariesBase),
    PcdGet32 (PcdFlashFvFirmwareBinariesSize),
    NULL,
    NULL,
    0
    );
}

/**
  FvCnvUncompact section dependency PPI
**/
static EFI_PEI_PPI_DESCRIPTOR  mFvCnvDispatchFlagPpi = {
  (EFI_PEI_PPI_DESCRIPTOR_PPI | EFI_PEI_PPI_DESCRIPTOR_TERMINATE_LIST),
  &gPeiFvCnvDispatchFlagPpiGuid,
  NULL
};

/**
  This function call checks if this is after capsule update by BootStateAfterCapsule variable.
  The variable is set to TRUE at the end of capsule update process.
  The boot paths always require all FVs to be installed
  so UEFI global boot option variables are initialized.

  @retval  TRUE   This is the first boot after capsule update.
  @retval  FALSE  This is NOT the first boot after capsule update.
**/
BOOLEAN
IsAfterCapsule (
  VOID
  )
{
  EFI_STATUS                        Status;
  EFI_PEI_READ_ONLY_VARIABLE2_PPI   *VariablePpi;
  UINTN                             VariableSize;
  BOOLEAN                           BootStateAfterCapsule;
  BOOLEAN                           IsAfterCapsule;
  BOOLEAN                           CapsuleUpdateEnable;
  BootStateAfterCapsule = FALSE;
  IsAfterCapsule = FALSE;

  Status = PeiServicesLocatePpi (
              &gEfiPeiReadOnlyVariable2PpiGuid,
              0,
              NULL,
              (VOID **)&VariablePpi
              );
  if (EFI_ERROR (Status)) {
    DEBUG ((DEBUG_ERROR, "IsAfterCapsule : Read Only Variable PPI is not found.\n"));
    ASSERT_EFI_ERROR (Status);
    return FALSE;
  }

  CapsuleUpdateEnable = PcdGetBool (PcdCapsuleEnable);
  if (CapsuleUpdateEnable) {
    VariableSize = sizeof (BOOLEAN);
    Status = VariablePpi->GetVariable (
                            VariablePpi,
                            BOOT_STATE_AFTER_CAPSULE_VARIABLE_NAME,
                            &gBootStateAfterCapsuleGuid,
                            NULL,
                            &VariableSize,
                            &BootStateAfterCapsule
                            );
    if ((Status != EFI_NOT_FOUND) && EFI_ERROR (Status)) {
      DEBUG ((DEBUG_ERROR, "IsAfterCapsule : Get variable failure.\n"));
      ASSERT_EFI_ERROR (Status);
      IsAfterCapsule = FALSE;
    }
    if (BootStateAfterCapsule) {
      DEBUG ((DEBUG_INFO, "IsAfterCapsule : This is the first boot path after Capsule update.\n"));
      IsAfterCapsule = TRUE;
    } else {
      DEBUG ((DEBUG_INFO, "IsAfterCapsule : This is NOT the first boot path after capsule update.\n"));
      IsAfterCapsule = FALSE;
    }
  }

  return IsAfterCapsule;
}

/**
  This function call checks if Network is enabled by Setup variables.

  @retval  TRUE     Network is enabled.
  @retval  FALSE    Network is not enabled.
**/
BOOLEAN
IsNetworkEnabled (
  VOID
  )
{
  EFI_STATUS                        Status;
  EFI_PEI_READ_ONLY_VARIABLE2_PPI   *VariablePpi;
  UINTN                             VariableSize;
  SETUP_DATA                        SystemConfiguration;

  Status = PeiServicesLocatePpi (
              &gEfiPeiReadOnlyVariable2PpiGuid,
              0,
              NULL,
              (VOID **)&VariablePpi
              );
  if (EFI_ERROR (Status)) {
    DEBUG ((DEBUG_ERROR, "IsNetworkEnabled : Read Only Variable PPI is not found.\n"));
    ASSERT_EFI_ERROR (Status);
    return FALSE;
  }

  VariableSize = sizeof (SETUP_DATA);
  Status = VariablePpi->GetVariable (
                          VariablePpi,
                          L"Setup",
                          &gSetupVariableGuid,
                          NULL,
                          &VariableSize,
                          &SystemConfiguration
                          );
  ASSERT_EFI_ERROR (Status);
  if (!EFI_ERROR (Status)) {
    if ((SystemConfiguration.EfiNetworkSupport != 0) || (SystemConfiguration.PrebootBleEnable == 1)) {
      DEBUG ((DEBUG_INFO, "IsNetworkEnabled : Network wants to be initialized. \n"));
      return TRUE;
    }
  }

  return FALSE;
}


/**
  This function call installs section dependency PPIs for the child FVs in Optional FV.

  @param[out] *Installed    Return TRUE if section dependencies in Optional FV are installed.
                            Return FALSE when none of section dependencies in Optional FV are installed.

  @retval  EFI_SUCCESS      Section dependency installation completes successfully.
  @retval  Others           Section dependency installation fails to complete.
**/
EFI_STATUS
InstallOptionalFvDependency (
  OUT BOOLEAN  *Installed
  )
{
  EFI_STATUS   Status;

  Status     = EFI_SUCCESS;
  *Installed = FALSE;

#if FixedPcdGet8(PcdFspModeSelection) == 0 // #if Dispatch Mode
  if (!IsBootStatePresent () || IsAfterCapsule () || IsNetworkEnabled ()) {
#endif
    DEBUG ((DEBUG_INFO, "InstallOptionalFvDependency : Installing FvCnvUncompact dependency.\n"));
    Status = PeiServicesInstallPpi (&mFvCnvDispatchFlagPpi);
    *Installed = TRUE;
    ASSERT_EFI_ERROR (Status);
#if FixedPcdGet8(PcdFspModeSelection) == 0 // #if Dispatch Mode
  }
#endif
  return Status;
}
VOID
ReportPostMemFv (
  VOID
  )
{
  EFI_STATUS                             Status;
  EFI_BOOT_MODE                          BootMode;
  EFI_HOB_GUID_TYPE                      *GuidHob;
  BOOLEAN                                IsOptFvDependencyInstalled;
  IsOptFvDependencyInstalled = FALSE;

  Status = PeiServicesGetBootMode (&BootMode);
  ASSERT_EFI_ERROR (Status);

  /*
    Note : FSP FVs except FSP-T FV are installed in IntelFsp2WrapperPkg or FspPkg.
  */

  ///
  /// Build HOB for DXE
  ///
  if (BootMode == BOOT_IN_RECOVERY_MODE) {
    ///
    /// Prepare the recovery service
    ///
  } else {
    GuidHob = GetFirstGuidHob (&gBiosInfoRecoveryGuid);
    if (GuidHob == NULL) {
      DEBUG ((DEBUG_INFO, "Install FlashFvSecurity - 0x%x, 0x%x\n", PcdGet32 (PcdFlashFvSecurityBase), PcdGet32 (PcdFlashFvSecuritySize)));
      PeiServicesInstallFvInfo2Ppi (
        &(((EFI_FIRMWARE_VOLUME_HEADER *) (UINTN) PcdGet32 (PcdFlashFvSecurityBase))->FileSystemGuid),
        (VOID *) (UINTN) PcdGet32 (PcdFlashFvSecurityBase),
        PcdGet32 (PcdFlashFvSecuritySize),
        NULL,
        NULL,
        0
        );
      if (BootMode != BOOT_ON_S3_RESUME) {
        DEBUG ((DEBUG_INFO, "Install FlashFvAdvanced - 0x%x, 0x%x\n", PcdGet32 (PcdFlashFvAdvancedBase), PcdGet32 (PcdFlashFvAdvancedSize)));
        PeiServicesInstallFvInfo2Ppi (
          &(((EFI_FIRMWARE_VOLUME_HEADER *) (UINTN) PcdGet32 (PcdFlashFvAdvancedBase))->FileSystemGuid),
          (VOID *) (UINTN) PcdGet32 (PcdFlashFvAdvancedBase),
          PcdGet32 (PcdFlashFvAdvancedSize),
          NULL,
          NULL,
          0
          );
        DEBUG ((DEBUG_INFO, "Install FlashFvUefiBoot - 0x%x, 0x%x\n", PcdGet32 (PcdFlashFvUefiBootBase), PcdGet32 (PcdFlashFvUefiBootSize)));
        PeiServicesInstallFvInfo2Ppi (
          &(((EFI_FIRMWARE_VOLUME_HEADER *) (UINTN) PcdGet32 (PcdFlashFvUefiBootBase))->FileSystemGuid),
          (VOID *) (UINTN) PcdGet32 (PcdFlashFvUefiBootBase),
          PcdGet32 (PcdFlashFvUefiBootSize),
          NULL,
          NULL,
          0
          );
        DEBUG ((DEBUG_INFO, "Install FlashFvOsBoot - 0x%x, 0x%x\n", PcdGet32 (PcdFlashFvOsBootBase), PcdGet32 (PcdFlashFvOsBootSize)));
        PeiServicesInstallFvInfo2Ppi (
          &(((EFI_FIRMWARE_VOLUME_HEADER *) (UINTN) PcdGet32 (PcdFlashFvOsBootBase))->FileSystemGuid),
          (VOID *) (UINTN) PcdGet32 (PcdFlashFvOsBootBase),
          PcdGet32 (PcdFlashFvOsBootSize),
          NULL,
          NULL,
          0
          );
        Status = InstallOptionalFvDependency (&IsOptFvDependencyInstalled);
        if (!EFI_ERROR (Status) && IsOptFvDependencyInstalled) {
          DEBUG ((DEBUG_INFO, "Install FlashFvOptional - 0x%x, 0x%x\n", PcdGet32 (PcdFlashFvOptionalBase), PcdGet32 (PcdFlashFvOptionalSize)));
          PeiServicesInstallFvInfo2Ppi (
          &(((EFI_FIRMWARE_VOLUME_HEADER *) (UINTN) PcdGet32 (PcdFlashFvOptionalBase))->FileSystemGuid),
          (VOID *) (UINTN) PcdGet32 (PcdFlashFvOptionalBase),
          PcdGet32 (PcdFlashFvOptionalSize),
          NULL,
          NULL,
          0
          );
        }
      }
      Status = PeiServicesNotifyPpi (&mExtendedBiosDecodeReadyNotifyList [1]);
      ASSERT_EFI_ERROR (Status);
    }
    DEBUG ((DEBUG_INFO, "Install FlashFvPostMemory - 0x%x, 0x%x\n", PcdGet32 (PcdFlashFvPostMemoryBase), PcdGet32 (PcdFlashFvPostMemorySize)));
    PeiServicesInstallFvInfo2Ppi (
      &(((EFI_FIRMWARE_VOLUME_HEADER *) (UINTN) PcdGet32 (PcdFlashFvPostMemoryBase))->FileSystemGuid),
      (VOID *) (UINTN) PcdGet32 (PcdFlashFvPostMemoryBase),
      PcdGet32 (PcdFlashFvPostMemorySize),
      NULL,
      NULL,
      0
      );

    Status = PeiServicesNotifyPpi (&mExtendedBiosDecodeReadyNotifyList [0]);
    ASSERT_EFI_ERROR (Status);

    if (BootMode != BOOT_ON_S3_RESUME) {
      BuildFvHob (
        (UINTN) FixedPcdGet32 (PcdFlashFvMicrocodeBase),
        (UINTN) FixedPcdGet32 (PcdFlashFvMicrocodeSize)
        );
      DEBUG ((DEBUG_INFO, "Build FlashFvMicrocode Hob - 0x%x, 0x%x\n", PcdGet32 (PcdFlashFvMicrocodeBase), PcdGet32 (PcdFlashFvMicrocodeSize)));

#if FixedPcdGetBool(PcdResiliencyEnable) == 1
        BuildFvHob (
          (UINTN) FixedPcdGet32 (PcdFlashFvPreMemoryBase),
          (UINTN) FixedPcdGet32 (PcdFlashFvPreMemorySize)
          );
        DEBUG ((DEBUG_INFO, "PcdResiliencyEnable = True , Build PcdFlashFvPreMemory Hob - 0x%x, 0x%x\n", PcdGet32 (PcdFlashFvPreMemoryBase), PcdGet32 (PcdFlashFvPreMemorySize)));
#endif
    }
  }

  //
  // Report resource HOB for flash FV
  //
  BuildResourceDescriptorHob (
    EFI_RESOURCE_MEMORY_MAPPED_IO,
    (EFI_RESOURCE_ATTRIBUTE_PRESENT    |
    EFI_RESOURCE_ATTRIBUTE_INITIALIZED |
    EFI_RESOURCE_ATTRIBUTE_UNCACHEABLE),
    (UINTN) PcdGet32 (PcdFlashAreaBaseAddress),
    (UINTN) PcdGet32 (PcdFlashAreaSize)
    );
  BuildMemoryAllocationHob (
    (UINTN) PcdGet32 (PcdFlashAreaBaseAddress),
    (UINTN) PcdGet32 (PcdFlashAreaSize),
    EfiMemoryMappedIO
    );
}


/**
  Callback on Extended BIOS Decode Ready Ppi so Extended PostMemory FV is installed to the memory
  for Extended BIOS Region. The callback is called regardless of Extended BIOS Region support
  in platform code.

  @param[in] PeiServices          General purpose services available to every PEIM.
  @param[in] NotifyDescriptor     The notification structure this PEIM registered on install.
  @param[in] Ppi                  The gExtendedBiosDecodeReady PPI.  Not used.

  @retval EFI_SUCCESS             Always returns EFI_SUCCESS
**/
EFI_STATUS
EFIAPI
InstallFvExtendedPostMemoryCallback (
  IN EFI_PEI_SERVICES           **PeiServices,
  IN EFI_PEI_NOTIFY_DESCRIPTOR  *NotifyDescriptor,
  IN VOID                       *Ppi
  )
{
#if FixedPcdGetBool(PcdExtendedBiosRegionSupport) == 1
  DEBUG ((DEBUG_INFO, "[BIOS] InstallFvExtendedPostMemoryCallback\n"));
  EFI_FIRMWARE_VOLUME_HEADER  *FvHeader;
  UINT32                      ExtendedBiosRegionBaseAddress;
  UINT32                      ExtendedBiosRegionSize;
  MTRR_SETTINGS               ExtendedBiosRegionMtrrSetting;
  EFI_STATUS                  Status;

  ExtendedBiosRegionBaseAddress = 0xF8000000;
  ExtendedBiosRegionSize        = SIZE_32MB;

  ///
  /// Reset all MTRR setting
  ///
  ZeroMem (&ExtendedBiosRegionMtrrSetting, sizeof (MTRR_SETTINGS));

  //
  // Get the Current MTRR Settings
  //
  MtrrGetAllMtrrs (&ExtendedBiosRegionMtrrSetting);

  Status = MtrrSetMemoryAttributeInMtrrSettings (
              &ExtendedBiosRegionMtrrSetting,
              ExtendedBiosRegionBaseAddress,
              ExtendedBiosRegionSize,
              CacheWriteProtected
              );

  if (EFI_ERROR (Status)) {
    REPORT_STATUS_CODE_EX (
      (EFI_ERROR_CODE | EFI_ERROR_UNRECOVERED),
      (EFI_COMPUTING_UNIT_HOST_PROCESSOR|EFI_CU_HP_EC_CACHE),
      0,
      &gCpuFspErrorTypeCallerId,
      NULL,
      NULL,
      0
      );
  }

  ///
  /// Update MTRR setting from MTRR buffer
  ///
  MtrrSetAllMtrrs (&ExtendedBiosRegionMtrrSetting);
  MtrrDebugPrintAllMtrrs ();

  DEBUG ((DEBUG_INFO, "Install FlashFvExtendedPostMemory - 0x%x, 0x%x\n", PcdGet32 (PcdFlashFvExtendedPostMemoryBase), PcdGet32 (PcdFlashFvExtendedPostMemorySize)));
  FvHeader = (EFI_FIRMWARE_VOLUME_HEADER *) (UINTN) (UINT32) PcdGet32 (PcdFlashFvExtendedPostMemoryBase);
  PeiServicesInstallFvInfo2Ppi (
    &(FvHeader->FileSystemGuid),
    (VOID *) (UINTN) PcdGet32 (PcdFlashFvExtendedPostMemoryBase),
    PcdGet32 (PcdFlashFvExtendedPostMemorySize),
    NULL,
    NULL,
    0
    );
  PrintFvHeaderInfo (FvHeader);

#else
  DEBUG ((DEBUG_INFO, "Extended BIOS Region is not supported by the image. No FV installed here\n"));
#endif
  return EFI_SUCCESS;
}


/**
  Callback on Extended BIOS Decode Ready Ppi so Extended Advanced FV is installed to the memory
  for Extended BIOS Region. The callback is called regardless of Extended BIOS Region support in
  platform code. Extended Advanced FV contains DXE phase drivers only and may want to be skipped
  on S3 path for responsiveness.

  @param[in] PeiServices          General purpose services available to every PEIM.
  @param[in] NotifyDescriptor     The notification structure this PEIM registered on install.
  @param[in] Ppi                  The gExtendedBiosDecodeReady PPI.  Not used.

  @retval EFI_SUCCESS             Always returns EFI_SUCCESS
**/
EFI_STATUS
EFIAPI
InstallFvExtendedAdvancedCallback (
  IN EFI_PEI_SERVICES           **PeiServices,
  IN EFI_PEI_NOTIFY_DESCRIPTOR  *NotifyDescriptor,
  IN VOID                       *Ppi
  )
{
#if FixedPcdGetBool(PcdExtendedBiosRegionSupport) == 1
  EFI_FIRMWARE_VOLUME_HEADER  *FvHeader;

  DEBUG ((DEBUG_INFO, "Install FlashFvExtendedAdvanced - 0x%x, 0x%x\n", PcdGet32 (PcdFlashFvExtendedAdvancedBase), PcdGet32 (PcdFlashFvExtendedAdvancedSize)));
  FvHeader = (EFI_FIRMWARE_VOLUME_HEADER *) (UINTN) (UINT32) PcdGet32 (PcdFlashFvExtendedAdvancedBase);
  PeiServicesInstallFvInfo2Ppi (
    &(FvHeader->FileSystemGuid),
    (VOID *) (UINTN) PcdGet32 (PcdFlashFvExtendedAdvancedBase),
    PcdGet32 (PcdFlashFvExtendedAdvancedSize),
    NULL,
    NULL,
    0
    );
  PrintFvHeaderInfo (FvHeader);

#else
  DEBUG ((DEBUG_INFO, "Extended BIOS Region is not supported by the image. No FV installed here\n"));
#endif
  return EFI_SUCCESS;
}


/**
  Debug support function to output detailed information on a firmware volume being installed.
  Setting DEBUG_PROPERTY_DEBUG_CODE_ENABLED bit of PcdDebugProperyMask lets the function to be
  included in a module. Refer to DEBUG_CODE macro.

  @param[in] FvHeader   Pointer to firmware volume header
**/
VOID
PrintFvHeaderInfo (
  EFI_FIRMWARE_VOLUME_HEADER  *FvHeader
  )
{
  DEBUG_CODE (
    EFI_FIRMWARE_VOLUME_EXT_HEADER  *FvExtHeader;
    EFI_FFS_FILE_HEADER             *FfsHeader;

      DEBUG ((DEBUG_INFO, "[ FV @ 0x%x ] \n", FvHeader));
      DEBUG ((DEBUG_INFO, " FV File System       :  %g   \n",   &FvHeader->FileSystemGuid));
    if (FvHeader->ExtHeaderOffset != 0) {
      FvExtHeader = (EFI_FIRMWARE_VOLUME_EXT_HEADER *) ((UINT8 *) FvHeader + FvHeader->ExtHeaderOffset);
      FfsHeader   = (EFI_FFS_FILE_HEADER *) ((UINT8 *) FvExtHeader + FvExtHeader->ExtHeaderSize);
      FfsHeader = (EFI_FFS_FILE_HEADER *) ALIGN_POINTER (FfsHeader, 8);
      DEBUG ((DEBUG_INFO, " FV GUID              :  %g   \n",   &FvExtHeader->FvName));
      DEBUG ((DEBUG_INFO, " File GUID            :  %g   \n",   &FfsHeader->Name));
    }
  );
}
