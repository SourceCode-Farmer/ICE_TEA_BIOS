/** @file
  HDAudio verb tables for ADL boards.

@copyright
  INTEL CONFIDENTIAL
  Copyright 2015 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains a 'Sample Driver' and is licensed as such under the terms
  of your license agreement with Intel or your vendor. This file may be modified
  by the user, subject to the additional terms of the license agreement.

@par Specification Reference:
**/

#include <Ppi/SiPolicy.h>
#include <HdAudioConfig.h>
#include <PlatformBoardConfig.h>
#include <Library/PeiHdaVerbTables.h>

#if FixedPcdGetBool(PcdAdlLpSupport) == 1
HDAUDIO_VERB_TABLE HdaVerbTableDisplayAudio = HDAUDIO_VERB_TABLE_INIT (
  //
  //  VerbTable: ADL-P Display Audio Codec
  //  Revision ID = 0xFF
  //  Codec Vendor: 0x8086281C for ADL-P
  //
  0x8086, 0x281C,
  0xFF, 0xFF,
  //
  // Display Audio Verb Table
  //
  // Enable the third converter and 4 pin widgets (for GEN10+, the Vendor Node ID is 02h)
  0x00278111,
  // Pin Widget 5 - PORT B - Configuration Default: 0x18560010
  0x00571C10,
  0x00571D00,
  0x00571E56,
  0x00571F18,
  // Pin Widget 6 - PORT C - Configuration Default: 0x18560020
  0x00671C20,
  0x00671D00,
  0x00671E56,
  0x00671F18,
  // Pin Widget 7 - PORT D - Configuration Default: 0x18560030
  0x00771C30,
  0x00771D00,
  0x00771E56,
  0x00771F18,
  // Pin Widget 8 - PORT E - Configuration Default: 0x18560040
  // For GEN10+ forth port was added and Node ID 8 has this Pin Widget for the forth port
  0x00871C40,
  0x00871D00,
  0x00871E56,
  0x00871F18,
  // Disable the third converter and 4 Pins (NID 02h)
  0x00278100
);
#else
HDAUDIO_VERB_TABLE HdaVerbTableDisplayAudio = HDAUDIO_VERB_TABLE_INIT (
  //
  //  VerbTable: ADL-S Display Audio Codec
  //  Revision ID = 0xFF
  //  Codec Vendor: 0x80862815
  //
  0x8086, 0x2815,
  0xFF, 0xFF,
  //
  // Display Audio Verb Table
  //
  // Enable the third converter and 4 pin widgets (for GEN10+, the Vendor Node ID is 02h)
  0x00278111,
  // Pin Widget 5 - PORT B - Configuration Default: 0x18560010
  0x00571C10,
  0x00571D00,
  0x00571E56,
  0x00571F18,
  // Pin Widget 6 - PORT C - Configuration Default: 0x18560020
  0x00671C20,
  0x00671D00,
  0x00671E56,
  0x00671F18,
  // Pin Widget 7 - PORT D - Configuration Default: 0x18560030
  0x00771C30,
  0x00771D00,
  0x00771E56,
  0x00771F18,
  // Pin Widget 8 - PORT E - Configuration Default: 0x18560040
  // For GEN10+ forth port was added and Node ID 8 has this Pin Widget for the forth port
  0x00871C40,
  0x00871D00,
  0x00871E56,
  0x00871F18,
  // Disable the third converter and 4 Pins (NID 02h)
  0x00278100
);
#endif

HDAUDIO_VERB_TABLE HdaVerbTableAlc286S = HDAUDIO_VERB_TABLE_INIT (
  //
  //  VerbTable: (Realtek ALC286S)
  //  Revision ID = 0xff
  //  Codec Verb Table for SKL PCH boards
  //  Codec Address: CAd value (0/1/2)
  //  Codec Vendor: 0x10EC0286
  //
  0x10EC, 0x0286,
  0xFF, 0xFF,
  //===================================================================================================
  //
  //                               Realtek Semiconductor Corp.
  //
  //===================================================================================================

  //Realtek High Definition Audio Configuration - Version : 5.0.2.1
  //Realtek HD Audio Codec : ALC286
  //PCI PnP ID : PCI\VEN_0000&DEV_0000&SUBSYS_00000000
  //HDA Codec PnP ID : HDAUDIO\FUNC_01&VEN_10EC&DEV_0286&SUBSYS_00000000
  //The number of verb command block : 15

  //    NID 0x12 : 0x90A60130
  //    NID 0x13 : 0x40000000
  //    NID 0x14 : 0x90170110
  //    NID 0x17 : 0x411111F0
  //    NID 0x18 : 0x03A19020
  //    NID 0x19 : 0x411111F0
  //    NID 0x1A : 0x0381302F
  //    NID 0x1D : 0x4066832D
  //    NID 0x1E : 0x411111F0
  //    NID 0x21 : 0x0321101F

  //===== HDA Codec Subsystem ID Verb-table =====
  //HDA Codec Subsystem ID  : 0x00000000
  0x00172000,
  0x00172100,
  0x00172200,
  0x00172300,

  //===== Pin Widget Verb-table =====
  //Widget node 0x01 :
  0x0017FF00,
  0x0017FF00,
  0x0017FF00,
  0x0017FF00,
  //Pin widget 0x12 - DMIC
  0x01271C40,
  0x01271D01,
  0x01271EA6,
  0x01271F90,
  //Pin widget 0x13 - DMIC
  0x01371C00,
  0x01371D00,
  0x01371E00,
  0x01371F40,
  //Pin widget 0x14 - SPEAKER-OUT (Port-D)
  0x01471C10,
  0x01471D01,
  0x01471E17,
  0x01471F90,
  //Pin widget 0x17 - I2S-OUT
  0x01771CF0,
  0x01771D11,
  0x01771E11,
  0x01771F41,
  //Pin widget 0x18 - MIC1 (Port-B)
  0x01871C30,
  0x01871D90,
  0x01871EA1,
  0x01871F03,
  //Pin widget 0x19 - I2S-IN
  0x01971CF0,
  0x01971D11,
  0x01971E11,
  0x01971F41,
  //Pin widget 0x1A - LINE1 (Port-C)
  0x01A71C3F,
  0x01A71D30,
  0x01A71E81,
  0x01A71F03,
  //Pin widget 0x1D - PC-BEEP
  0x01D71C2D,
  0x01D71D23,
  0x01D71E66,
  0x01D71F40,
  //Pin widget 0x1E - S/PDIF-OUT
  0x01E71CF0,
  0x01E71D11,
  0x01E71E11,
  0x01E71F41,
  //Pin widget 0x21 - HP-OUT (Port-A)
  0x02171C20,
  0x02171D10,
  0x02171E21,
  0x02171F03,
  //Widget node 0x20 :
  0x02050071,
  0x02040014,
  0x02050010,
  0x02040C22,
  //Widget node 0x20 - 1 :
  0x0205004F,
  0x02045029,
  0x02050051,
  0x02045428,
  //Widget node 0x20 - 2 :
  0x0205002B,
  0x02040C50,
  0x0205002D,
  0x02041020,
  // New verbs from Realtek
  0x02050063,
  0x02042906
);

HDAUDIO_VERB_TABLE HdaVerbTableAlc298 = HDAUDIO_VERB_TABLE_INIT (
  //
  //  VerbTable: (Realtek ALC298)
  //  Revision ID = 0xff
  //  Codec Verb Table for SKL PCH boards
  //  Codec Address: CAd value (0/1/2)
  //  Codec Vendor: 0x10EC0286
  //
  0x10EC, 0x0298,
  0xFF, 0xFF,
  //===================================================================================================
  //
  //                               Realtek Semiconductor Corp.
  //
  //===================================================================================================

  //Realtek High Definition Audio Configuration - Version : 5.0.2.1
  //Realtek HD Audio Codec : ALC298
  //PCI PnP ID : PCI\VEN_0000&DEV_0000&SUBSYS_00000000
  //HDA Codec PnP ID : HDAUDIO\FUNC_01&VEN_10EC&DEV_0298&SUBSYS_00000000
  //The number of verb command block : 15

  //    NID 0x12 : 0x90A60130
  //    NID 0x13 : 0x411111F0
  //    NID 0x14 : 0x90170110
  //    NID 0x17 : 0x40000000
  //    NID 0x18 : 0x03A11020
  //    NID 0x19 : 0x411111F0
  //    NID 0x1A : 0x01813140
  //    NID 0x1D : 0x40E6852D
  //    NID 0x1E : 0x411111F0
  //    NID 0x21 : 0x0321101F


  //===== HDA Codec Subsystem ID Verb-table =====
  //HDA Codec Subsystem ID  : 0x00000000
  0x00172000,
  0x00172100,
  0x00172200,
  0x00172300,


  //===== Pin Widget Verb-table =====
  //Widget node 0x01 :
  0x0017FF00,
  0x0017FF00,
  0x0017FF00,
  0x0017FF00,
  //Pin widget 0x12 - DMIC
  0x01271C30,
  0x01271D01,
  0x01271EA6,
  0x01271F90,
  //Pin widget 0x13 - DMIC
  0x01371CF0,
  0x01371D11,
  0x01371E11,
  0x01371F41,
  //Pin widget 0x14 - SPEAKER-OUT (Port-D)
  0x01471C10,
  0x01471D01,
  0x01471E17,
  0x01471F90,
  //Pin widget 0x17 - I2S-OUT
  0x01771C00,
  0x01771D00,
  0x01771E00,
  0x01771F40,
  //Pin widget 0x18 - MIC1 (Port-B)
  0x01871C20,
  0x01871D10,
  0x01871EA1,
  0x01871F03,
  //Pin widget 0x19 - I2S-IN
  0x01971CF0,
  0x01971D11,
  0x01971E11,
  0x01971F41,
  //Pin widget 0x1A - LINE1 (Port-C)
  0x01A71C40,
  0x01A71D31,
  0x01A71E81,
  0x01A71F01,
  //Pin widget 0x1D - PC-BEEP
  0x01D71C2D,
  0x01D71D85,
  0x01D71EE6,
  0x01D71F40,
  //Pin widget 0x1E - S/PDIF-OUT
  0x01E71CF0,
  0x01E71D11,
  0x01E71E11,
  0x01E71F41,

  //Pin widget 0x1F - S/PDIF-IN
  0x01F71CF0,
  0x01F71D11,
  0x01F71E11,
  0x01F71F41,

  //Pin widget 0x21 - HP-OUT (Port-A)
  0x02171C1F,
  0x02171D10,
  0x02171E21,
  0x02171F03,
  //Widget node 0x20 :
  0x02050019,
  0x02040217,
  0x02050001,
  0x0204ADAA,
  //Widget node 0x20 - 1 :
  0x02050002,
  0x02048EB5,
  0x02050034,
  0x02045610,
  //Widget node 0x20 - 2 :
  0x02050035,
  0x02041AA4,
  0x0205008F,
  0x02041000,
  //Widget node 0x20 - 3 :
  0x0205004F,
  0x02045009,
  0x0205004F,
  0x02045009
);

//
// CNL codecs verb tables
//
HDAUDIO_VERB_TABLE HdaVerbTableAlc700 = HDAUDIO_VERB_TABLE_INIT (
  //
  //  VerbTable: (Realtek ALC700) CNL RVP
  //  Revision ID = 0xff
  //  Codec Verb Table for CNL PCH boards
  //  Codec Address: CAd value (0/1/2)
  //  Codec Vendor: 0x10EC0700
  //
  0x10EC, 0x0700,
  0xFF, 0xFF,
  //===================================================================================================
  //
  //                               Realtek Semiconductor Corp.
  //
  //===================================================================================================

  //Realtek High Definition Audio Configuration - Version : 5.0.3.0
  //Realtek HD Audio Codec : ALC700
  //PCI PnP ID : PCI\VEN_8086&DEV_2668&SUBSYS_72708086
  //HDA Codec PnP ID : HDAUDIO\FUNC_01&VEN_10EC&DEV_0700&SUBSYS_10EC10F2
  //The number of verb command block : 17

  //    NID 0x12 : 0x411111F0
  //    NID 0x13 : 0x40000000
  //    NID 0x14 : 0x411111F0
  //    NID 0x15 : 0x411111F0
  //    NID 0x16 : 0x411111F0
  //    NID 0x17 : 0x90170110
  //    NID 0x18 : 0x411111F0
  //    NID 0x19 : 0x04A11030
  //    NID 0x1A : 0x411111F0
  //    NID 0x1B : 0x411111F0
  //    NID 0x1D : 0x40622005
  //    NID 0x1E : 0x411111F0
  //    NID 0x1F : 0x411111F0
  //    NID 0x21 : 0x04211020
  //    NID 0x29 : 0x411111F0

  //===== HDA Codec Subsystem ID Verb-table =====
  //HDA Codec Subsystem ID  : 0x10EC10F2
  0x001720F2,
  0x00172110,
  0x001722EC,
  0x00172310,

  //===== Pin Widget Verb-table =====
  //Widget node 0x01 :
  0x0017FF00,
  0x0017FF00,
  0x0017FF00,
  0x0017FF00,
  //Pin widget 0x12 - DMIC
  0x01271C00,
  0x01271D00,
  0x01271E00,
  0x01271F40,
  //Pin widget 0x13 - DMIC
  0x01371C00,
  0x01371D00,
  0x01371E00,
  0x01371F40,
  //Pin widget 0x14 - FRONT (Port-D)
  0x01471CF0,
  0x01471D11,
  0x01471E11,
  0x01471F41,
  //Pin widget 0x15 - I2S-OUT
  0x01571CF0,
  0x01571D11,
  0x01571E11,
  0x01571F41,
  //Pin widget 0x16 - LINE3 (Port-B)
  0x01671CF0,
  0x01671D11,
  0x01671E11,
  0x01671F41,
  //Pin widget 0x17 - I2S-OUT
  0x01771C10,
  0x01771D01,
  0x01771E17,
  0x01771F90,
  //Pin widget 0x18 - I2S-IN
  0x01871CF0,
  0x01871D11,
  0x01871E11,
  0x01871F41,
  //Pin widget 0x19 - MIC2 (Port-F)
  0x01971C30,
  0x01971D10,
  0x01971EA1,
  0x01971F04,
  //Pin widget 0x1A - LINE1 (Port-C)
  0x01A71CF0,
  0x01A71D11,
  0x01A71E11,
  0x01A71F41,
  //Pin widget 0x1B - LINE2 (Port-E)
  0x01B71CF0,
  0x01B71D11,
  0x01B71E11,
  0x01B71F41,
  //Pin widget 0x1D - PC-BEEP
  0x01D71C05,
  0x01D71D20,
  0x01D71E62,
  0x01D71F40,
  //Pin widget 0x1E - S/PDIF-OUT
  0x01E71CF0,
  0x01E71D11,
  0x01E71E11,
  0x01E71F41,
  //Pin widget 0x1F - S/PDIF-IN
  0x01F71CF0,
  0x01F71D11,
  0x01F71E11,
  0x01F71F41,
  //Pin widget 0x21 - HP-OUT (Port-I)
  0x02171C20,
  0x02171D10,
  0x02171E21,
  0x02171F04,
  //Pin widget 0x29 - I2S-IN
  0x02971CF0,
  0x02971D11,
  0x02971E11,
  0x02971F41,
  //Widget node 0x20 :
  0x02050045,
  0x02045289,
  0x0205004A,
  0x0204201B,
  //Widget node 0x20 - 1 :
  0x05850000,
  0x05843888,
  0x0205006F,
  0x02042C0B,


  //Widget node 0X20 for ALC1305   20160603 update
  0x02050024,
  0x02040010,
  0x02050026,
  0x02040000,
  0x02050028,
  0x02040000,
  0x02050029,
  0x0204B024,

  0x02050024,
  0x02040010,
  0x02050026,
  0x02040004,
  0x02050028,
  0x02040600,
  0x02050029,
  0x0204B024,

  0x02050024,
  0x02040010,
  0x02050026,
  0x0204003C,
  0x02050028,
  0x0204FFD0,
  0x02050029,
  0x0204B024,

  0x02050024,
  0x02040010,
  0x02050026,
  0x02040080,
  0x02050028,
  0x02040080,
  0x02050029,
  0x0204B024,

  0x02050024,
  0x02040010,
  0x02050026,
  0x02040080,
  0x02050028,
  0x02040880,
  0x02050029,
  0x0204B024,

  0x02050024,
  0x02040010,
  0x02050026,
  0x0204003A,
  0x02050028,
  0x02040DFE,
  0x02050029,
  0x0204B024,

  0x02050024,
  0x02040010,
  0x02050026,
  0x0204006A,
  0x02050028,
  0x0204005D,
  0x02050029,
  0x0204B024,

  0x02050024,
  0x02040010,
  0x02050026,
  0x0204006C,
  0x02050028,
  0x02040442,
  0x02050029,
  0x0204B024,

  0x02050024,
  0x02040010,
  0x02050026,
  0x02040005,
  0x02050028,
  0x02040880,
  0x02050029,
  0x0204B024,

  0x02050024,
  0x02040010,
  0x02050026,
  0x02040006,
  0x02050028,
  0x02040000,
  0x02050029,
  0x0204B024,

  0x02050024,
  0x02040010,
  0x02050026,
  0x02040008,
  0x02050028,
  0x0204B000,
  0x02050029,
  0x0204B024,

  0x02050024,
  0x02040010,
  0x02050026,
  0x0204002E,
  0x02050028,
  0x02040800,
  0x02050029,
  0x0204B024,

  0x02050024,
  0x02040010,
  0x02050026,
  0x0204006A,
  0x02050028,
  0x020400C3,
  0x02050029,
  0x0204B024,

  0x02050024,
  0x02040010,
  0x02050026,
  0x0204006C,
  0x02050028,
  0x0204D4A0,
  0x02050029,
  0x0204B024,

  0x02050024,
  0x02040010,
  0x02050026,
  0x0204006A,
  0x02050028,
  0x020400CC,
  0x02050029,
  0x0204B024,

  0x02050024,
  0x02040010,
  0x02050026,
  0x0204006C,
  0x02050028,
  0x0204400A,
  0x02050029,
  0x0204B024,

  0x02050024,
  0x02040010,
  0x02050026,
  0x0204006A,
  0x02050028,
  0x020400C1,
  0x02050029,
  0x0204B024,

  0x02050024,
  0x02040010,
  0x02050026,
  0x0204006C,
  0x02050028,
  0x02040320,
  0x02050029,
  0x0204B024,

  0x02050024,
  0x02040010,
  0x02050026,
  0x02040039,
  0x02050028,
  0x02040000,
  0x02050029,
  0x0204B024,

  0x02050024,
  0x02040010,
  0x02050026,
  0x0204003B,
  0x02050028,
  0x0204FFFF,
  0x02050029,
  0x0204B024,

  0x02050024,
  0x02040010,
  0x02050026,
  0x0204003C,
  0x02050028,
  0x0204FC20,
  0x02050029,
  0x0204B024,

  0x02050024,
  0x02040010,
  0x02050026,
  0x0204003A,
  0x02050028,
  0x02041DFE,
  0x02050029,
  0x0204B024,
  //
  0x02050024,
  0x02040010,
  0x02050026,
  0x020400C0,
  0x02050028,
  0x020401FA,
  0x02050029,
  0x0204B024,

  0x02050024,
  0x02040010,
  0x02050026,
  0x020400C1,
  0x02050028,
  0x0204DE23,
  0x02050029,
  0x0204B024,

  0x02050024,
  0x02040010,
  0x02050026,
  0x020400C2,
  0x02050028,
  0x02041C00,
  0x02050029,
  0x0204B024,

  0x02050024,
  0x02040010,
  0x02050026,
  0x020400C3,
  0x02050028,
  0x02040000,
  0x02050029,
  0x0204B024,

  0x02050024,
  0x02040010,
  0x02050026,
  0x020400C4,
  0x02050028,
  0x02040200,
  0x02050029,
  0x0204B024,

  0x02050024,
  0x02040010,
  0x02050026,
  0x020400C5,
  0x02050028,
  0x02040000,
  0x02050029,
  0x0204B024,

  0x02050024,
  0x02040010,
  0x02050026,
  0x020400C6,
  0x02050028,
  0x020403F5,
  0x02050029,
  0x0204B024,

  0x02050024,
  0x02040010,
  0x02050026,
  0x020400C7,
  0x02050028,
  0x0204AF1B,
  0x02050029,
  0x0204B024,

  0x02050024,
  0x02040010,
  0x02050026,
  0x020400C8,
  0x02050028,
  0x02041E0A,
  0x02050029,
  0x0204B024,

  0x02050024,
  0x02040010,
  0x02050026,
  0x020400C9,
  0x02050028,
  0x0204368E,
  0x02050029,
  0x0204B024,

  0x02050024,
  0x02040010,
  0x02050026,
  0x020400CA,
  0x02050028,
  0x020401FA,
  0x02050029,
  0x0204B024,

  0x02050024,
  0x02040010,
  0x02050026,
  0x020400CB,
  0x02050028,
  0x0204DE23,
  0x02050029,
  0x0204B024,

  0x02050024,
  0x02040010,
  0x02050026,
  0x020400CC,
  0x02050028,
  0x02041C00,
  0x02050029,
  0x0204B024,

  0x02050024,
  0x02040010,
  0x02050026,
  0x020400CD,
  0x02050028,
  0x02040000,
  0x02050029,
  0x0204B024,

  0x02050024,
  0x02040010,
  0x02050026,
  0x020400CE,
  0x02050028,
  0x02040200,
  0x02050029,
  0x0204B024,

  0x02050024,
  0x02040010,
  0x02050026,
  0x020400CF,
  0x02050028,
  0x02040000,
  0x02050029,
  0x0204B024,

  0x02050024,
  0x02040010,
  0x02050026,
  0x020400D0,
  0x02050028,
  0x020403F5,
  0x02050029,
  0x0204B024,

  0x02050024,
  0x02040010,
  0x02050026,
  0x020400D1,
  0x02050028,
  0x0204AF1B,
  0x02050029,
  0x0204B024,

  0x02050024,
  0x02040010,
  0x02050026,
  0x020400D2,
  0x02050028,
  0x02041E0A,
  0x02050029,
  0x0204B024,

  0x02050024,
  0x02040010,
  0x02050026,
  0x020400D3,
  0x02050028,
  0x0204368E,
  0x02050029,
  0x0204B024,

  0x02050024,
  0x02040010,
  0x02050026,
  0x02040040,
  0x02050028,
  0x0204800F,
  0x02050029,
  0x0204B024,

  0x02050024,
  0x02040010,
  0x02050026,
  0x02040062,
  0x02050028,
  0x02048000,
  0x02050029,
  0x0204B024,

  0x02050024,
  0x02040010,
  0x02050026,
  0x02040063,
  0x02050028,
  0x02044848,
  0x02050029,
  0x0204B024,

  0x02050024,
  0x02040010,
  0x02050026,
  0x02040064,
  0x02050028,
  0x02040800,
  0x02050029,
  0x0204B024,

  0x02050024,
  0x02040010,
  0x02050026,
  0x02040065,
  0x02050028,
  0x02040000,
  0x02050029,
  0x0204B024,

  0x02050024,
  0x02040010,
  0x02050026,
  0x02040066,
  0x02050028,
  0x02044004,
  0x02050029,
  0x0204B024,

  0x02050024,
  0x02040010,
  0x02050026,
  0x02040067,
  0x02050028,
  0x02040802,
  0x02050029,
  0x0204B024,

  0x02050024,
  0x02040010,
  0x02050026,
  0x02040068,
  0x02050028,
  0x0204890F,
  0x02050029,
  0x0204B024,

  0x02050024,
  0x02040010,
  0x02050026,
  0x02040069,
  0x02050028,
  0x0204E021,
  0x02050029,
  0x0204B024,

  0x02050024,
  0x02040010,
  0x02050026,
  0x02040070,
  0x02050028,
  0x02040000,
  0x02050029,
  0x0204B024,

  0x02050024,
  0x02040010,
  0x02050026,
  0x02040071,
  0x02050000,
  0x02043330,
  0x02050029,
  0x0204B024,

  0x02050024,
  0x02040010,
  0x02050026,
  0x02040072,
  0x02050000,
  0x02043333,
  0x02050029,
  0x0204B024,

  0x02050024,
  0x02040010,
  0x02050026,
  0x02040073,
  0x02050028,
  0x02040000,
  0x02050029,
  0x0204B024,

  0x02050024,
  0x02040010,
  0x02050026,
  0x02040074,
  0x02050028,
  0x02040000,
  0x02050029,
  0x0204B024,

  0x02050024,
  0x02040010,
  0x02050026,
  0x02040075,
  0x02050028,
  0x02040000,
  0x02050029,
  0x0204B024,

  0x02050024,
  0x02040010,
  0x02050026,
  0x02040076,
  0x02050028,
  0x02040000,
  0x02050029,
  0x0204B024,

  0x02050024,
  0x02040010,
  0x02050026,
  0x02040050,
  0x02050028,
  0x020402EC,
  0x02050029,
  0x0204B024,

  0x02050024,
  0x02040010,
  0x02050026,
  0x02040051,
  0x02050028,
  0x02044909,
  0x02050029,
  0x0204B024,

  0x02050024,
  0x02040010,
  0x02050026,
  0x02040052,
  0x02050028,
  0x020440B0,
  0x02050029,
  0x0204B024,

  0x02050024,
  0x02040010,
  0x02050026,
  0x02040046,
  0x02050028,
  0x0204C22E,
  0x02050029,
  0x0204B024,

  0x02050024,
  0x02040010,
  0x02050026,
  0x02040047,
  0x02050028,
  0x02040C00,
  0x02050029,
  0x0204B024,

  0x02050024,
  0x02040010,
  0x02050026,
  0x02040048,
  0x02050028,
  0x02040000,
  0x02050029,
  0x0204B024,

  0x02050024,
  0x02040010,
  0x02050026,
  0x02040049,
  0x02050028,
  0x02040000,
  0x02050029,
  0x0204B024,

  0x02050024,
  0x02040010,
  0x02050026,
  0x0204004A,
  0x02050028,
  0x02040000,
  0x02050029,
  0x0204B024,

  0x02050024,
  0x02040010,
  0x02050026,
  0x0204004B,
  0x02050028,
  0x02041C00,
  0x02050029,
  0x0204B024,

  0x02050024,
  0x02040010,
  0x02050026,
  0x0204006A,
  0x02050028,
  0x02040090,
  0x02050029,
  0x0204B024,

  0x02050024,
  0x02040010,
  0x02050026,
  0x0204006C,
  0x02050028,
  0x0204721F,
  0x02050029,
  0x0204B024,

  0x02050024,
  0x02040010,
  0x02050026,
  0x0204009E,
  0x02050028,
  0x02040001,
  0x02050029,
  0x0204B024,

  0x02050024,
  0x02040010,
  0x02050026,
  0x02040004,
  0x02050028,
  0x02040500,
  0x02050029,
  0x0204B024
); // HdaVerbTableAlc700

HDAUDIO_VERB_TABLE HdaVerbTableAlc701 = HDAUDIO_VERB_TABLE_INIT (
  //
  //  VerbTable: (Realtek ALC701) CNL external kit
  //  Revision ID = 0xff
  //  Codec Verb Table for CNL PCH boards
  //  Codec Address: CAd value (0/1/2)
  //  Codec Vendor: 0x10EC0701
  //
  0x10EC, 0x0701,
  0xFF, 0xFF,
  //===================================================================================================
  //
  //                               Realtek Semiconductor Corp.
  //
  //===================================================================================================

  //Realtek High Definition Audio Configuration - Version : 5.0.3.2
  //Realtek HD Audio Codec : ALC701
  //PCI PnP ID : PCI\VEN_8086&DEV_2668&SUBSYS_72708086
  //HDA Codec PnP ID : HDAUDIO\FUNC_01&VEN_10EC&DEV_0701&SUBSYS_10EC10F4
  //The number of verb command block : 17

  //    NID 0x12 : 0x90A60130
  //    NID 0x13 : 0x411111F0
  //    NID 0x14 : 0x411111F0
  //    NID 0x15 : 0x411111F0
  //    NID 0x16 : 0x411111F0
  //    NID 0x17 : 0x90170110
  //    NID 0x18 : 0x411111F0
  //    NID 0x19 : 0x04A11040
  //    NID 0x1A : 0x411111F0
  //    NID 0x1B : 0x411111F0
  //    NID 0x1D : 0x40600001
  //    NID 0x1E : 0x411111F0
  //    NID 0x1F : 0x411111F0
  //    NID 0x21 : 0x04211020
  //    NID 0x29 : 0x411111F0


  //===== HDA Codec Subsystem ID Verb-table =====
  //HDA Codec Subsystem ID  : 0x10EC10F4
  0x001720F4,
  0x00172110,
  0x001722EC,
  0x00172310,

  //===== Pin Widget Verb-table =====
  //Widget node 0x01 :
  0x0017FF00,
  0x0017FF00,
  0x0017FF00,
  0x0017FF00,
  //Pin widget 0x13 - DMIC
  0x01371CF0,
  0x01371D11,
  0x01371E11,
  0x01371F41,
  //Pin widget 0x14 - FRONT (Port-D)
  0x01471CF0,
  0x01471D11,
  0x01471E11,
  0x01471F41,
  //Pin widget 0x15 - I2S-OUT
  0x01571CF0,
  0x01571D11,
  0x01571E11,
  0x01571F41,
  //Pin widget 0x16 - LINE3 (Port-B)
  0x01671CF0,
  0x01671D11,
  0x01671E11,
  0x01671F41,
  //Pin widget 0x17 - I2S-OUT
  0x01771C10,
  0x01771D01,
  0x01771E17,
  0x01771F90,
  //Pin widget 0x18 - I2S-IN
  0x01871CF0,
  0x01871D11,
  0x01871E11,
  0x01871F41,
  //Pin widget 0x19 - MIC2 (Port-F)
  0x01971C40,
  0x01971D10,
  0x01971EA1,
  0x01971F04,
  //Pin widget 0x1A - LINE1 (Port-C)
  0x01A71CF0,
  0x01A71D11,
  0x01A71E11,
  0x01A71F41,
  //Pin widget 0x1B - LINE2 (Port-E)
  0x01B71CF0,
  0x01B71D11,
  0x01B71E11,
  0x01B71F41,
  //Pin widget 0x1D - PC-BEEP
  0x01D71C01,
  0x01D71D00,
  0x01D71E60,
  0x01D71F40,
  //Pin widget 0x1E - S/PDIF-OUT
  0x01E71CF0,
  0x01E71D11,
  0x01E71E11,
  0x01E71F41,
  //Pin widget 0x1F - S/PDIF-IN
  0x01F71CF0,
  0x01F71D11,
  0x01F71E11,
  0x01F71F41,
  //Pin widget 0x21 - HP-OUT (Port-I)
  0x02171C20,
  0x02171D10,
  0x02171E21,
  0x02171F04,
  //Pin widget 0x29 - I2S-IN
  0x02971CF0,
  0x02971D11,
  0x02971E11,
  0x02971F41,
  //Widget node 0x20 :
  0x02050045,
  0x02045289,
  0x0205004A,
  0x0204201B,

  //Widget node 0x20 - 1 :
  0x05B50010,
  0x05B45C1D,
  0x0205006F,
  0x02040F8B,

  0X205003C,
  0X204F254,
  0X205003C,
  0X204F214,

  //Class D silent detection Enable -84dB t,res,old
  0X2050030,
  0X2049000,
  0X2050037,
  0X204FE15,

//Widget node 0X20 for ALC1306   20180730 update   2W/4o,m
  0X2050024,
  0X2040010,
  0X2050026,
  0X2040000,
  0X2050028,
  0X2040000,
  0X2050029,
  0X204B024,

  0X2050024,
  0X2040010,
  0X2050026,
  0X204006A,
  0X2050028,
  0X20400CF,
  0X2050029,
  0X204B024,

  0X2050024,
  0X2040010,
  0X2050026,
  0X204006C,
  0X2050028,
  0X2045548,
  0X2050029,
  0X204B024,

  0X2050024,
  0X2040010,
  0X2050026,
  0X204003F,
  0X2050028,
  0X2041000,
  0X2050029,
  0X204B024,

  0X2050024,
  0X2040010,
  0X2050026,
  0X2040004,
  0X2050028,
  0X2040600,
  0X2050029,
  0X204B024,

  0X2050024,
  0X2040010,
  0X2050026,
  0X204003C,
  0X2050028,
  0X204FFD0,
  0X2050029,
  0X204B024,

  0X2050024,
  0X2040010,
  0X2050026,
  0X2040080,
  0X2050028,
  0X2040080,
  0X2050029,
  0X204B024,

  0X2050024,
  0X2040010,
  0X2050026,
  0X2040080,
  0X2050028,
  0X2040880,
  0X2050029,
  0X204B024,

  0X2050024,
  0X2040010,
  0X2050026,
  0X204003A,
  0X2050028,
  0X2040DFE,
  0X2050029,
  0X204B024,

  0X2050024,
  0X2040010,
  0X2050026,
  0X204006A,
  0X2050028,
  0X204005D,
  0X2050029,
  0X204B024,

  0X2050024,
  0X2040010,
  0X2050026,
  0X204006C,
  0X2050028,
  0X2040442,
  0X2050029,
  0X204B024,

  0X2050024,
  0X2040010,
  0X2050026,
  0X2040005,
  0X2050028,
  0X2040880,
  0X2050029,
  0X204B024,

  0X2050024,
  0X2040010,
  0X2050026,
  0X2040006,
  0X2050028,
  0X2040000,
  0X2050029,
  0X204B024,

  0X2050024,
  0X2040010,
  0X2050026,
  0X2040008,
  0X2050028,
  0X204B000,
  0X2050029,
  0X204B024,

  0X2050024,
  0X2040010,
  0X2050026,
  0X204002E,
  0X2050028,
  0X2040800,
  0X2050029,
  0X204B024,

  0X2050024,
  0X2040010,
  0X2050026,
  0X204006A,
  0X2050028,
  0X20400C3,
  0X2050029,
  0X204B024,

  0X2050024,
  0X2040010,
  0X2050026,
  0X204006C,
  0X2050028,
  0X204D4A0,
  0X2050029,
  0X204B024,

  0X2050024,
  0X2040010,
  0X2050026,
  0X204006A,
  0X2050028,
  0X20400CC,
  0X2050029,
  0X204B024,

  0X2050024,
  0X2040010,
  0X2050026,
  0X204006C,
  0X2050028,
  0X204400A,
  0X2050029,
  0X204B024,

  0X2050024,
  0X2040010,
  0X2050026,
  0X204006A,
  0X2050028,
  0X20400C1,
  0X2050029,
  0X204B024,

  0X2050024,
  0X2040010,
  0X2050026,
  0X204006C,
  0X2050028,
  0X2040320,
  0X2050029,
  0X204B024,

  0X2050024,
  0X2040010,
  0X2050026,
  0X2040039,
  0X2050028,
  0X2040000,
  0X2050029,
  0X204B024,

  0X2050024,
  0X2040010,
  0X2050026,
  0X204003B,
  0X2050028,
  0X204FFFF,
  0X2050029,
  0X204B024,

  0X2050024,
  0X2040010,
  0X2050026,
  0X204003C,
  0X2050028,
  0X204FC20,
  0X2050029,
  0X204B024,

  0X2050024,
  0X2040010,
  0X2050026,
  0X204006A,
  0X2050028,
  0X2040006,
  0X2050029,
  0X204B024,

  0X2050024,
  0X2040010,
  0X2050026,
  0X204006C,
  0X2050028,
  0X20400C0,
  0X2050029,
  0X204B024,

  0X2050024,
  0X2040010,
  0X2050026,
  0X204003C,
  0X2050028,
  0X204FCA0,
  0X2050029,
  0X204B024,

  0X2050024,
  0X2040010,
  0X2050026,
  0X204003C,
  0X2050028,
  0X204FCE0,
  0X2050029,
  0X204B024,

  0X2050024,
  0X2040010,
  0X2050026,
  0X204003C,
  0X2050028,
  0X204FCF0,
  0X2050029,
  0X204B024,

  0X2050024,
  0X2040010,
  0X2050026,
  0X2040080,
  0X2050028,
  0X2040080,
  0X2050029,
  0X204B024,

  0X2050024,
  0X2040010,
  0X2050026,
  0X2040080,
  0X2050028,
  0X2040880,
  0X2050029,
  0X204B024,

  0X2050024,
  0X2040010,
  0X2050026,
  0X2040080,
  0X2050028,
  0X2040880,
  0X2050029,
  0X204B024,

  0X2050024,
  0X2040010,
  0X2050026,
  0X204003C,
  0X2050028,
  0X204FCE0,
  0X2050029,
  0X204B024,

  0X2050024,
  0X2040010,
  0X2050026,
  0X204003C,
  0X2050028,
  0X204FCA0,
  0X2050029,
  0X204B024,

  0X2050024,
  0X2040010,
  0X2050026,
  0X204003C,
  0X2050028,
  0X204FC20,
  0X2050029,
  0X204B024,

  0X2050024,
  0X2040010,
  0X2050026,
  0X204006A,
  0X2050028,
  0X2040006,
  0X2050029,
  0X204B024,

  0X2050024,
  0X2040010,
  0X2050026,
  0X204006C,
  0X2050028,
  0X2040000,
  0X2050029,
  0X204B024,

  0X2050024,
  0X2040010,
  0X2050026,
  0X2040080,
  0X2050028,
  0X2040000,
  0X2050029,
  0X204B024,

  0X2050024,
  0X2040010,
  0X2050026,
  0X20400C0,
  0X2050028,
  0X20401F0,
  0X2050029,
  0X204B024,

  0X2050024,
  0X2040010,
  0X2050026,
  0X20400C1,
  0X2050028,
  0X204C1C7,
  0X2050029,
  0X204B024,

  0X2050024,
  0X2040010,
  0X2050026,
  0X20400C2,
  0X2050028,
  0X2041C00,
  0X2050029,
  0X204B024,

  0X2050024,
  0X2040010,
  0X2050026,
  0X20400C3,
  0X2050028,
  0X2040000,
  0X2050029,
  0X204B024,

  0X2050024,
  0X2040010,
  0X2050026,
  0X20400C4,
  0X2050028,
  0X2040200,
  0X2050029,
  0X204B024,

  0X2050024,
  0X2040010,
  0X2050026,
  0X20400C5,
  0X2050028,
  0X2040000,
  0X2050029,
  0X204B024,

  0X2050024,
  0X2040010,
  0X2050026,
  0X20400C6,
  0X2050028,
  0X20403E1,
  0X2050029,
  0X204B024,

  0X2050024,
  0X2040010,
  0X2050026,
  0X20400C7,
  0X2050028,
  0X2040F5A,
  0X2050029,
  0X204B024,

  0X2050024,
  0X2040010,
  0X2050026,
  0X20400C8,
  0X2050028,
  0X2041E1E,
  0X2050029,
  0X204B024,

  0X2050024,
  0X2040010,
  0X2050026,
  0X20400C9,
  0X2050028,
  0X204083F,
  0X2050029,
  0X204B024,

  0X2050024,
  0X2040010,
  0X2050026,
  0X20400CA,
  0X2050028,
  0X20401F0,
  0X2050029,
  0X204B024,

  0X2050024,
  0X2040010,
  0X2050026,
  0X20400CB,
  0X2050028,
  0X204C1C7,
  0X2050029,
  0X204B024,

  0X2050024,
  0X2040010,
  0X2050026,
  0X20400CC,
  0X2050028,
  0X2041C00,
  0X2050029,
  0X204B024,

  0X2050024,
  0X2040010,
  0X2050026,
  0X20400CD,
  0X2050028,
  0X2040000,
  0X2050029,
  0X204B024,

  0X2050024,
  0X2040010,
  0X2050026,
  0X20400CE,
  0X2050028,
  0X2040200,
  0X2050029,
  0X204B024,

  0X2050024,
  0X2040010,
  0X2050026,
  0X20400CF,
  0X2050028,
  0X2040000,
  0X2050029,
  0X204B024,

  0X2050024,
  0X2040010,
  0X2050026,
  0X20400D0,
  0X2050028,
  0X20403E1,
  0X2050029,
  0X204B024,

  0X2050024,
  0X2040010,
  0X2050026,
  0X20400D1,
  0X2050028,
  0X2040F5A,
  0X2050029,
  0X204B024,

  0X2050024,
  0X2040010,
  0X2050026,
  0X20400D2,
  0X2050028,
  0X2041E1E,
  0X2050029,
  0X204B024,

  0X2050024,
  0X2040010,
  0X2050026,
  0X20400D3,
  0X2050028,
  0X204083F,
  0X2050029,
  0X204B024,

  0X2050024,
  0X2040010,
  0X2050026,
  0X2040062,
  0X2050028,
  0X2048000,
  0X2050029,
  0X204B024,

  0X2050024,
  0X2040010,
  0X2050026,
  0X2040063,
  0X2050028,
  0X2045F5F,
  0X2050029,
  0X204B024,

  0X2050024,
  0X2040010,
  0X2050026,
  0X2040064,
  0X2050028,
  0X2042000,
  0X2050029,
  0X204B024,

  0X2050024,
  0X2040010,
  0X2050026,
  0X2040065,
  0X2050028,
  0X2040000,
  0X2050029,
  0X204B024,

  0X2050024,
  0X2040010,
  0X2050026,
  0X2040066,
  0X2050028,
  0X2044004,
  0X2050029,
  0X204B024,

  0X2050024,
  0X2040010,
  0X2050026,
  0X2040067,
  0X2050028,
  0X2040802,
  0X2050029,
  0X204B024,

  0X2050024,
  0X2040010,
  0X2050026,
  0X2040068,
  0X2050028,
  0X204890F,
  0X2050029,
  0X204B024,

  0X2050024,
  0X2040010,
  0X2050026,
  0X2040069,
  0X2050028,
  0X204E021,
  0X2050029,
  0X204B024,

  0x2050024,
  0x2040010,
  0x2050026,
  0x2040070,
  0x2050028,
  0x2048012,
  0x2050029,
  0x204B024,

  0x2050024,
  0x2040010,
  0x2050026,
  0x2040071,
  0x2050028,
  0x2043450,
  0x2050029,
  0x204B024,

  0x2050024,
  0x2040010,
  0x2050026,
  0x2040072,
  0x2050028,
  0x2040123,
  0x2050029,
  0x204B024,

  0x2050024,
  0x2040010,
  0x2050026,
  0x2040073,
  0x2050028,
  0x2044543,
  0x2050029,
  0x204B024,

  0x2050024,
  0x2040010,
  0x2050026,
  0x2040074,
  0x2050028,
  0x2042100,
  0x2050029,
  0x204B024,

  0x2050024,
  0x2040010,
  0x2050026,
  0x2040075,
  0x2050028,
  0x2044321,
  0x2050029,
  0x204B024,

  0x2050024,
  0x2040010,
  0x2050026,
  0x2040076,
  0x2050028,
  0x2040000,
  0x2050029,
  0x204B024,

  0x2050024,
  0x2040010,
  0x2050026,
  0x2040050,
  0x2050028,
  0x2048200,
  0x2050029,
  0x204B024,

  0x2050024,
  0x2040010,
  0x2050026,
  0x2040051,
  0x2050028,
  0x2040707,
  0x2050029,
  0x204B024,

  0x2050024,
  0x2040010,
  0x2050026,
  0x2040052,
  0x2050028,
  0x2044090,
  0x2050029,
  0x204B024,

  0x2050024,
  0x2040010,
  0x2050026,
  0x204006A,
  0x2050028,
  0x2040090,
  0x2050029,
  0x204B024,

  0x2050024,
  0x2040010,
  0x2050026,
  0x204006C,
  0x2050028,
  0x204721F,
  0x2050029,
  0x204B024,

  0x2050024,
  0x2040010,
  0x2050026,
  0x2040012,
  0x2050028,
  0x204DFDF,
  0x2050029,
  0x204B024,

  0x2050024,
  0x2040010,
  0x2050026,
  0x204009E,
  0x2050028,
  0x2040000,
  0x2050029,
  0x204B024,

  0x2050024,
  0x2040010,
  0x2050026,
  0x2040004,
  0x2050028,
  0x2040500,
  0x2050029,
  0x204B024,

  0x2050024,
  0x2040010,
  0x2050026,
  0x2040060,
  0x2050028,
  0x2042213,
  0x2050029,
  0x204B024,

  0x2050024,
  0x2040010,
  0x2050026,
  0x204003A,
  0x2050028,
  0x2041DFE,
  0x2050029,
  0x204B024,

  0x2050024,
  0x2040010,
  0x2050026,
  0x204003F,
  0x2050028,
  0x2043000,
  0x2050029,
  0x204B024,

  0x2050024,
  0x2040010,
  0x2050026,
  0x2040040,
  0x2050028,
  0x204800C,
  0x2050029,
  0x204B024,

  0x2050024,
  0x2040010,
  0x2050026,
  0x2040046,
  0x2050028,
  0x204C22E,
  0x2050029,
  0x204B024,

  0x2050024,
  0x2040010,
  0x2050026,
  0x204004B,
  0x2050028,
  0x2040000,
  0x2050029,
  0x204B024
); // HdaVerbTableAlc701

HDAUDIO_VERB_TABLE HdaVerbTableAlc274 = HDAUDIO_VERB_TABLE_INIT (
  //
  //  VerbTable: (Realtek ALC274) CNL external kit
  //  Revision ID = 0xff
  //  Codec Verb Table for CNL PCH boards
  //  Codec Address: CAd value (0/1/2)
  //  Codec Vendor: 0x10EC0274
  //
  0x10EC, 0x0274,
  0xFF, 0xFF,
  //===================================================================================================
  //
  //                               Realtek Semiconductor Corp.
  //
  //===================================================================================================

  //Realtek High Definition Audio Configuration - Version : 5.0.3.0
  //Realtek HD Audio Codec : ALC274
  //PCI PnP ID : PCI\VEN_8086&DEV_2668&SUBSYS_72708086
  //HDA Codec PnP ID : HDAUDIO\FUNC_01&VEN_10EC&DEV_0274&SUBSYS_10EC10F6
  //The number of verb command block : 16

  //    NID 0x12 : 0x40000000
  //    NID 0x13 : 0x411111F0
  //    NID 0x14 : 0x411111F0
  //    NID 0x15 : 0x411111F0
  //    NID 0x16 : 0x411111F0
  //    NID 0x17 : 0x411111F0
  //    NID 0x18 : 0x411111F0
  //    NID 0x19 : 0x04A11020
  //    NID 0x1A : 0x411111F0
  //    NID 0x1B : 0x411111F0
  //    NID 0x1D : 0x40451B05
  //    NID 0x1E : 0x411111F0
  //    NID 0x1F : 0x411111F0
  //    NID 0x21 : 0x04211010


  //===== HDA Codec Subsystem ID Verb-table =====
  //,DA Codec Subsystem ID  : 0x10EC10F6
  0x001720F6,
  0x00172110,
  0x001722EC,
  0x00172310,

  //===== Pin Widget Verb-table =====
  //Widget node 0x01 :
  0x0017FF00,
  0x0017FF00,
  0x0017FF00,
  0x0017FF00,
  //Pin widget 0x12 - DMIC
  0x01271C00,
  0x01271D00,
  0x01271E00,
  0x01271F40,
  //Pin widget 0x13 - DMIC
  0x01371CF0,
  0x01371D11,
  0x01371E11,
  0x01371F41,
  //Pin widget 0x14 - NPC
  0x01471CF0,
  0x01471D11,
  0x01471E11,
  0x01471F41,
  //Pin widget 0x15 - I2S_OUT2
  0x01571CF0,
  0x01571D11,
  0x01571E11,
  0x01571F41,
  //Pin widget 0x16 - LINE3 (Port-B)
  0x01671CF0,
  0x01671D11,
  0x01671E11,
  0x01671F41,
  //Pin widget 0x17 - I2S_OUT1
  0x01771CF0,
  0x01771D11,
  0x01771E11,
  0x01771F41,
  //Pin widget 0x18 - I2S_IN
  0x01871CF0,
  0x01871D11,
  0x01871E11,
  0x01871F41,
  //Pin widget 0x19 - MIC2 (Port-F)
  0x01971C20,
  0x01971D10,
  0x01971EA1,
  0x01971F04,
  //Pin widget 0x1A - LINE1 (Port-C)
  0x01A71CF0,
  0x01A71D11,
  0x01A71E11,
  0x01A71F41,
  //Pin widget 0x1B - LINE2 (Port-E)
  0x01B71CF0,
  0x01B71D11,
  0x01B71E11,
  0x01B71F41,
  //Pin widget 0x1D - PC-BEEP
  0x01D71C05,
  0x01D71D1B,
  0x01D71E45,
  0x01D71F40,
  //Pin widget 0x1E - S/PDIF-OUT
  0x01E71CF0,
  0x01E71D11,
  0x01E71E11,
  0x01E71F41,
  //Pin widget 0x1F - S/PDIF-IN
  0x01F71CF0,
  0x01F71D11,
  0x01F71E11,
  0x01F71F41,
  //Pin widget 0x21 - HP-OUT (Port-I)
  0x02171C10,
  0x02171D10,
  0x02171E21,
  0x02171F04,
  //Widget node 0x20 :
  0x02050045,
  0x02045289,
  0x0205006F,
  0x02042C0B,
  //Widget node 0x20 - 1 :
  0x02050035,
  0x02048968,
  0x05B50001,
  0x05B48540,
  //Widget node 0x20 - 2 :
  0x05850000,
  0x05843888,
  0x05850000,
  0x05843888,
  //Widget node 0x20 - 3 :
  0x0205004A,
  0x0204201B,
  0x0205004A,
  0x0204201B
); //HdaVerbTableAlc274

#if FixedPcdGetBool(PcdAdlLpSupport) == 1
HDAUDIO_VERB_TABLE HdaVerbTableAlc711 = HDAUDIO_VERB_TABLE_INIT (
  //
  //  VerbTable: (Realtek ALC711)
  //  Revision ID = 0xff
  //  Codec Verb Table
  //  Codec Address: CAd value (0/1/2)
  //  Codec Vendor: 0x10EC0700
  //
  0x10EC, 0x0711,
  0xFF, 0xFF,
  //===================================================================================================
  //
  //                               Realtek Semiconductor Corp.
  //
  //===================================================================================================

  //Realtek High Definition Audio Configuration - Version : 5.0.3.2
  //Realtek HD Audio Codec : ALC700
  //PCI PnP ID : PCI\VEN_8086&DEV_2668&SUBSYS_72708086
  //HDA Codec PnP ID : INTELAUDIO\FUNC_01&VEN_10EC&DEV_0711&SUBSYS_10EC127E
  //The number of verb command block : 17

  //    NID 0x12 : 0x411111F0
  //    NID 0x13 : 0x411111F0
  //    NID 0x14 : 0x411111F0
  //    NID 0x15 : 0x411111F0
  //    NID 0x16 : 0x411111F0
  //    NID 0x17 : 0x411111F0
  //    NID 0x18 : 0x411111F0
  //    NID 0x19 : 0x04A11030
  //    NID 0x1A : 0x411111F0
  //    NID 0x1B : 0x411111F0
  //    NID 0x1D : 0x40600001
  //    NID 0x1E : 0x411111F0
  //    NID 0x1F : 0x411111F0
  //    NID 0x21 : 0x04211010
  //    NID 0x29 : 0x411111F0

  //===== HDA Codec Subsystem ID Verb-table =====
  //HDA Codec Subsystem ID  : 0x10EC127E
  0x0017207E,
  0x00172112,
  0x001722EC,
  0x00172310,

  //===== Pin Widget Verb-table =====
  //Widget node 0x01 :
  0x0017FF00,
  0x0017FF00,
  0x0017FF00,
  0x0017FF00,
  //Pin widget 0x12 - DMIC
  0x01271CF0,
  0x01271D11,
  0x01271E11,
  0x01271F41,
  //Pin widget 0x13 - DMIC
  0x01371CF0,
  0x01371D11,
  0x01371E11,
  0x01371F41,
  //Pin widget 0x14 - FRONT (Port-D)
  0x01471CF0,
  0x01471D11,
  0x01471E11,
  0x01471F41,
  //Pin widget 0x15 - I2S-OUT
  0x01571CF0,
  0x01571D11,
  0x01571E11,
  0x01571F41,
  //Pin widget 0x16 - LINE3 (Port-B)
  0x01671CF0,
  0x01671D11,
  0x01671E11,
  0x01671F41,
  //Pin widget 0x17 - I2S-OUT
  0x01771CF0,
  0x01771D11,
  0x01771E11,
  0x01771F41,
  //Pin widget 0x18 - I2S-IN
  0x01871CF0,
  0x01871D11,
  0x01871E11,
  0x01871F41,
  //Pin widget 0x19 - MIC2 (Port-F)
  0x01971C30,
  0x01971D10,
  0x01971EA1,
  0x01971F04,
  //Pin widget 0x1A - LINE1 (Port-C)
  0x01A71CF0,
  0x01A71D11,
  0x01A71E11,
  0x01A71F41,
  //Pin widget 0x1B - LINE2 (Port-E)
  0x01B71CF0,
  0x01B71D11,
  0x01B71E11,
  0x01B71F41,
  //Pin widget 0x1D - PC-BEEP
  0x01D71C01,
  0x01D71D00,
  0x01D71E60,
  0x01D71F40,
  //Pin widget 0x1E - S/PDIF-OUT
  0x01E71CF0,
  0x01E71D11,
  0x01E71E11,
  0x01E71F41,
  //Pin widget 0x1F - S/PDIF-IN
  0x01F71CF0,
  0x01F71D11,
  0x01F71E11,
  0x01F71F41,
  //Pin widget 0x21 - HP-OUT (Port-I)
  0x02171C10,
  0x02171D10,
  0x02171E21,
  0x02171F04,
  //Pin widget 0x29 - I2S-IN
  0x02971CF0,
  0x02971D11,
  0x02971E11,
  0x02971F41,

  //Widget node 0x20 - 1 : //remove NID 58 realted setting for ALC711  bypass DAC02 DRE(NID5B bit14)
  0x05B50010,
  0x05B4581D,
  0x0205006F,
  0x0204058B,
  //Widget node 0x20 - 2 : //2 MIC Vrefo-L/R for headset
  0x02050045,
  0x02045289,
  0x0205004A,
  0x0204201B,
  //Widget node 0x20 - 3 : /To set 1 pin detect 2 port JD for headset(100K pull low) &  JD2 headset behavior.
  0x02050008,
  0x0204A807,
  0x02050009,
  0x0204042A,
  //Widget node 0x20 - 4 : /To set 1 pin detect JD voltage to 1.8V AHP-JD final result control for JD-1/2 result
  0x0205000B,
  0x02047770,
  0x02050011,
  0x0204047A
); // HdaVerbTableAlc711
#else
HDAUDIO_VERB_TABLE HdaVerbTableAlc711 = HDAUDIO_VERB_TABLE_INIT (
  //
  //  VerbTable: (Realtek ALC711)
  //  Revision ID = 0xff
  //  Codec Verb Table
  //  Codec Address: CAd value (0/1/2)
  //  Codec Vendor: 0x10EC0700
  //
  0x10EC, 0x0711,
  0xFF, 0xFF,
  //===================================================================================================
  //
  //                               Realtek Semiconductor Corp.
  //
  //===================================================================================================

  // Realtek High Definition Audio Configuration - Version : 5.0.3.2
  // Realtek HD Audio Codec : ALC711
  // PCI PnP ID : PCI\VEN_8086&DEV_2668&SUBSYS_72708086
  // HDA Codec PnP ID : INTELAUDIO\FUNC_01&VEN_10EC&DEV_0711&SUBSYS_10EC1250
  // The number of verb command block : 17

  //    NID 0x12 : 0x90A60120
  //    NID 0x13 : 0x90A60140
  //    NID 0x14 : 0x411111F0
  //    NID 0x15 : 0x411111F0
  //    NID 0x16 : 0x411111F0
  //    NID 0x17 : 0x411111F0
  //    NID 0x18 : 0x411111F0
  //    NID 0x19 : 0x04A11030
  //    NID 0x1A : 0x411111F0
  //    NID 0x1B : 0x411111F0
  //    NID 0x1D : 0x40600001
  //    NID 0x1E : 0x411111F0
  //    NID 0x1F : 0x411111F0
  //    NID 0x21 : 0x04211010
  //    NID 0x29 : 0x411111F0

  //===== HDA Codec Subsystem ID Verb-table =====
  //HDA Codec Subsystem ID  : 0x10EC11C2
  0x00172050,
  0x00172112,
  0x001722EC,
  0x00172310,

  //===== Pin Widget Verb-table =====
  //Widget node 0x01 :
  0x0017FF00,
  0x0017FF00,
  0x0017FF00,
  0x0017FF00,
  //Pin widget 0x12 - DMIC
  0x01271C20,
  0x01271D01,
  0x01271EA6,
  0x01271F90,
  //Pin widget 0x13 - DMIC
  0x01371C40,
  0x01371D01,
  0x01371EA6,
  0x01371F90,
  //Pin widget 0x14 - FRONT (Port-D)
  0x01471CF0,
  0x01471D11,
  0x01471E11,
  0x01471F41,
  //Pin widget 0x15 - I2S-OUT
  0x01571CF0,
  0x01571D11,
  0x01571E11,
  0x01571F41,
  //Pin widget 0x16 - LINE3 (Port-B)
  0x01671CF0,
  0x01671D11,
  0x01671E11,
  0x01671F41,
  //Pin widget 0x17 - I2S-OUT
  0x01771CF0,
  0x01771D11,
  0x01771E11,
  0x01771F41,
  //Pin widget 0x18 - I2S-IN
  0x01871CF0,
  0x01871D11,
  0x01871E11,
  0x01871F41,
  //Pin widget 0x19 - MIC2 (Port-F)
  0x01971C30,
  0x01971D10,
  0x01971EA1,
  0x01971F04,
  //Pin widget 0x1A - LINE1 (Port-C)
  0x01A71CF0,
  0x01A71D11,
  0x01A71E11,
  0x01A71F41,
  //Pin widget 0x1B - LINE2 (Port-E)
  0x01B71CF0,
  0x01B71D11,
  0x01B71E11,
  0x01B71F41,
  //Pin widget 0x1D - PC-BEEP
  0x01D71C01,
  0x01D71D00,
  0x01D71E60,
  0x01D71F40,
  //Pin widget 0x1E - S/PDIF-OUT
  0x01E71CF0,
  0x01E71D11,
  0x01E71E11,
  0x01E71F41,
  //Pin widget 0x1F - S/PDIF-IN
  0x01F71CF0,
  0x01F71D11,
  0x01F71E11,
  0x01F71F41,
  //Pin widget 0x21 - HP-OUT (Port-I)
  0x02171C10,
  0x02171D10,
  0x02171E21,
  0x02171F04,
  //Pin widget 0x29 - I2S-IN
  0x02971CF0,
  0x02971D11,
  0x02971E11,
  0x02971F41,
  //Widget node 0x20 - 1 : //remove NID 58 realted setting for ALC711  bypass DAC02 DRE(NID5B bit14)
  0x05B50010,
  0x05B4581D,
  0x0205006F,
  0x0204058B,
  //Widget node 0x20 - 2 : //2 MIC Vrefo-L/R for headset
  0x02050045,
  0x02045289,
  0x0205004A,
  0x0204201B,
  //Widget node 0x20 - 3 : /To set 1 pin detect JD for headset &  JD2 headset behavior.
  0x02050008,
  0x0204A805,
  0x02050009,
  0x0204142B,
  //Widget node 0x20 - 4 : /To set 1 pin detect JD voltage to 1.8V AHP-JD final result control for JD-1/2 result
  0x0205000B,
  0x0204777A,
  0x02050011,
  0x0204047A
); // HdaVerbTableAlc711
#endif

HDAUDIO_VERB_TABLE HdaVerbTableCS4207 = HDAUDIO_VERB_TABLE_INIT (
  //Codec Verb Table For HD Audio

  //Codec Address (CAd): 00h
  //Codec Vendor:

  //VendorID/DeviceID
  0x1013, 0x4207,
  0xFF, 0xFF,
  //----------------------------

  //Verb count  28h

  //pin widget (Node Id 09h)
  0x00971cf0,
  0x00971d10,
  0x00971e21,
  0x00971f02,

  //pin widget (Node Id 0ah)
  0x00a71cf0,
  0x00a71d10,
  0x00a71e01,
  0x00a71f01,

  //pin widget (Node Id 0bh)
  0x00b71cf0,
  0x00b71d00,
  0x00b71e17,
  0x00b71f50,

  //pin widget (Node Id 0ch)
  0x00c71cf0,
  0x00c71d10,
  0x00c71e81,
  0x00c71f01,

  //pin widget (Node Id 0dh)
  0x00d71cf0,
  0x00d71d10,
  0x00d71ea1,
  0x00d71f02,

  //pin widget (Node Id 0eh)
  0x00e71c3e,
  0x00e71d00,
  0x00e71ed6,
  0x00e71f77,

  //pin widget (Node Id 0fh)
  0x00f71cf0,
  0x00f71de0,
  0x00f71ec4,
  0x00f71f42,

  //pin widget (Node Id 10h)
  0x01071cf0,
  0x01071d60,
  0x01071e44,
  0x01071f41,

  //pin widget (Node Id 12h)
  0x01271c5e,
  0x01271d00,
  0x01271ed6,
  0x01271f77,

  //pin widget (Node Id 15h)
  0x01571cf0,
  0x01571d10,
  0x01571e45,
  0x01571f41
); // HdaVerbTableCS4207

HDAUDIO_VERB_TABLE HdaVerbTableAlc245NoDmic = HDAUDIO_VERB_TABLE_INIT (
  //
  //  VerbTable: (Realtek ALC245)
  //  Revision ID = 0xff
  //  Codec Verb Table
  //  Codec Address: CAd value (0/1/2)
  //  Codec Vendor: 0x10EC0245
  //
  0x10EC, 0x0245,
  0xFF, 0xFF,
  //===================================================================================================
  //
  //                               Realtek Semiconductor Corp.
  //
  //===================================================================================================

  //Realtek High Definition Audio Configuration - Version : 5.0.3.0
  //Realtek HD Audio Codec : ALC295
  //PCI PnP ID : PCI\VEN_8086&DEV_2668&SUBSYS_124C10EC
  //HDA Codec PnP ID : INTELAUDIO\FUNC_01&VEN_10EC&DEV_0245&SUBSYS_10EC124C

  //    NID 0x12 : 0x411111F0
  //    NID 0x13 : 0x40000000
  //    NID 0x14 : 0x90170110
  //    NID 0x16 : 0x411111F0
  //    NID 0x17 : 0x411111F0
  //    NID 0x18 : 0x411111F0
  //    NID 0x19 : 0x04A11040
  //    NID 0x1A : 0x411111F0
  //    NID 0x1B : 0x411111F0
  //    NID 0x1D : 0x40600001
  //    NID 0x1E : 0x411111F0
  //    NID 0x21 : 0x04211020

  //===== HDA Codec Subsystem ID Verb-table =====
  //HDA Codec Subsystem ID  : 0x10EC124C
  0x0017204C,
  0x00172112,
  0x001722EC,
  0x00172310,

  //===== Pin Widget Verb-table =====
  //Widget node 0x01 :
  0x0017FF00,
  0x0017FF00,
  0x0017FF00,
  0x0017FF00,
  //===== 1bit reset =====
  0x0205001A,
  0x0204C003,
  0x0205001A,
  0x02048003,
  //Pin widget 0x12 - DMIC
  0x01271CF0,
  0x01271D11,
  0x01271E11,
  0x01271F41,
  //Pin widget 0x13 - DMIC
  0x01371C00,
  0x01371D00,
  0x01371E00,
  0x01371F40,
  //Pin widget 0x14 - Front (Port-D)
  0x01471C10,
  0x01471D01,
  0x01471E17,
  0x01471F90,
  //Pin widget 0x16 - HP2-OUT (Port-B)
  0x01671CF0,
  0x01671D11,
  0x01671E11,
  0x01671F41,
  //Pin widget 0x17 - I2S OUT
  0x01771CF0,
  0x01771D11,
  0x01771E11,
  0x01771F41,
  //Pin widget 0x18 - I2S IN
  0x01871CF0,
  0x01871D11,
  0x01871E11,
  0x01871F41,
  //Pin widget 0x19 - MIC2 (Port-F)
  0x01971C40,
  0x01971D10,
  0x01971EA1,
  0x01971F04,
  //Pin widget 0x1A - LINE1 (Port-C)
  0x01A71CF0,
  0x01A71D11,
  0x01A71E11,
  0x01A71F41,
  //Pin widget 0x1B - LINE2 (Port-E)
  0x01B71CF0,
  0x01B71D11,
  0x01B71E11,
  0x01B71F41,
  //Pin widget 0x1D - BEEP-IN
  0x01D71C01,
  0x01D71D00,
  0x01D71E60,
  0x01D71F40,
  //Pin widget 0x1E - S/PDIF-OUT
  0x01E71CF0,
  0x01E71D11,
  0x01E71E11,
  0x01E71F41,
  //Pin widget 0x21 - HP1-OUT (Port-I)
  0x02171C20,
  0x02171D10,
  0x02171E21,
  0x02171F04,
  //Class-D power on reset
  0x0205003C,
  0x0204F175,
  0x0205003C,
  0x0204F135,
  //NID 0x20 JD1 - 1 pin detect 2 port
  0x02050009,
  0x0204E003,
  0x0205000A,
  0x02047770,
  //NID 0x20 HP-JD enable and EAPD as default
  0x0205004A,
  0x02042010,
  0x02050010,
  0x02040020,
  //Set Class-d to 2W /4ohm and Smart amp Sine tone monitor setting
  0x02050038,
  0x02047909,
  0x05C50000,
  0x05C43982,
  //Class D silent detection Enable(-84dB threshold)
  0x02050037,
  0x0204FE15,
  0x02050030,
  0x02049000,
  // I2S Pad Floating Control  and DMIC Floating
  0x05A50001,
  0x05A4001F,
  0x02050035,
  0x02048D82,
  //Class-d DC detect
  0x0205006B,
  0x0204A390,
  0x0205006B,
  0x0204A390,
  //Class-d DC detect
  0x0205006C,
  0x02040C9E,
  0x0205006D,
  0x02040C00,
  //restore default for ultra-low power mode
  0x02050008,
  0x02046A8C,
  0x02050076,
  0x0204F000,

  0x0205000E,
  0x020465C0,
  0x02050033,
  0x02048580,

  0x02050069,
  0x0204FDA8,
  0x02050068,
  0x02040000,

  0x02050003,
  0x02040002,
  0x02050069,
  0x02040000,

  0x02050068,
  0x02040001,
  0x0205002E,
  0x0204290E
);

HDAUDIO_VERB_TABLE HdaVerbTableAlc897 = HDAUDIO_VERB_TABLE_INIT (
  //
  //  VerbTable: (Realtek ALC245)
  //  Revision ID = 0xff
  //  Codec Verb Table
  //  Codec Address: CAd value (0/1/2)
  //  Codec Vendor: 0x10EC0245
  //
  0x10EC, 0x0897,
  0xFF, 0xFF,
  //===================================================================================================
  //
  //                               Realtek Semiconductor Corp.
  //
  //===================================================================================================

  //Realtek High Definition Audio Configuration - Version : 5.0.3.2
  //Realtek HD Audio Codec : ALC897
  //PCI PnP ID : PCI\VEN_8086&DEV_2668&SUBSYS_72708086
  //HDA Codec PnP ID : INTELAUDIO\FUNC_01&VEN_10EC&DEV_0897&SUBSYS_10EC1268
  //The number of verb command block : 16

  //    NID 0x11 : 0x40130000
  //    NID 0x12 : 0x90A60150
  //    NID 0x14 : 0x01014010
  //    NID 0x15 : 0x01011012
  //    NID 0x16 : 0x01016011
  //    NID 0x17 : 0x01012014
  //    NID 0x18 : 0x01A19030
  //    NID 0x19 : 0x02A1903F
  //    NID 0x1A : 0x01813040
  //    NID 0x1B : 0x02214020
  //    NID 0x1C : 0x411111F0
  //    NID 0x1D : 0x4025D601
  //    NID 0x1E : 0x411111F0
  //    NID 0x1F : 0x411111F0

  //===== HDA Codec Subsystem ID Verb-table =====
  //HDA Codec Subsystem ID  : 0x10EC1268
  0x00172068,
  0x00172112,
  0x001722EC,
  0x00172310,

  //===== Pin Widget Verb-table =====
  //Widget node 0x01 :
  0x0017FF00,
  0x0017FF00,
  0x0017FF00,
  0x0017FF00,
  //Pin widget 0x11 - S/PDIF-OUT2
  0x01171C00,
  0x01171D00,
  0x01171E13,
  0x01171F40,
  //Pin widget 0x12 - DMIC
  0x01271C50,
  0x01271D01,
  0x01271EA6,
  0x01271F90,
  //Pin widget 0x14 - FRONT (Port-D)
  0x01471C10,
  0x01471D40,
  0x01471E01,
  0x01471F01,
  //Pin widget 0x15 - SURR (Port-A)
  0x01571C12,
  0x01571D10,
  0x01571E01,
  0x01571F01,
  //Pin widget 0x16 - CEN/LFE (Port-G)
  0x01671C11,
  0x01671D60,
  0x01671E01,
  0x01671F01,
  //Pin widget 0x17 - SIDESURR (Port-H)
  0x01771C14,
  0x01771D20,
  0x01771E01,
  0x01771F01,
  //Pin widget 0x18 - MIC1 (Port-B)
  0x01871C30,
  0x01871D90,
  0x01871EA1,
  0x01871F01,
  //Pin widget 0x19 - MIC2 (Port-F)
  0x019713F,
  0x01971D90,
  0x01971EA1,
  0x01971F02,
  //Pin widget 0x1A - LINE1 (Port-C)
  0x01A71C40,
  0x01A71D30,
  0x01A71E81,
  0x01A71F01,
  //Pin widget 0x1B - LINE2 (Port-E)
  0x01B71C20,
  0x01B71D40,
  0x01B71E21,
  0x01B71F02,
  //Pin widget 0x1C - CD-IN
  0x01C71CF0,
  0x01C71D11,
  0x01C71E11,
  0x01C71F41,
  //Pin widget 0x1D - BEEP-IN
  0x01D71C01,
  0x01D71DD6,
  0x01D71E25,
  0x01D71F40,
  //Pin widget 0x1E - S/PDIF-OUT1
  0x01E71CF0,
  0x01E71D11,
  0x01E71E11,
  0x01E71F41,
  //Pin widget 0x1F - S/PDIF-IN
  0x01F71CF0,
  0x01F71D11,
  0x01F71E11,
  0x01F71F41,

  //Pin widget 0x20 -1
  0x0205003B,
  0x02040080,
  0x02050039,
  0x02044031,
  //Pin widget 0x20 -2
  0x02050013,
  0x02040053,
  0x02050030,
  0x020492D1,
  //Pin widget 0x20 -3
  0x02050021,
  0x02040000,
  0x02050023,
  0x02040000,
  //Pin widget 0x20 -4
  0x02050025,
  0x02040000,
  0x02050027,
  0x02040000,
  //Pin widget 0x20 -5
  0x02050029,
  0x02040000,
  0x0205002A,
  0x02041640,
  //Pin widget 0x20 -6
  0x0205002B,
  0x02041640,
  0x02050021,
  0x02040C00,
  //Pin widget 0x20 -7
  0x02050023,
  0x02040C00,
  0x02050025,
  0x02040C00,
  //Pin widget 0x20 -8
  0x02050027,
  0x02040C00,
  0x02050029,
  0x02040C00,
  //Pin widget 0x20 -9
  0x0205002A,
  0x02041641,
  0x0205002B,
  0x02041641,
  //Pin widget 0x20 -10
  0x02050030,
  0x02049251,
  0x0205002A,
  0x02041649,
  //Pin widget 0x20 -11
  0x0205002B,
  0x02041649,
  0x0205000C,
  0x02043F06
);

HDAUDIO_VERB_TABLE HdaVerbTableSenaryTech  = HDAUDIO_VERB_TABLE_INIT (
  // Device ID(CX11970, DID:20D0, VID:14F1)
  0x14F1, 0x20D0,
  0xFF, 0xFF,

  // Node 16(Port A): 03211040(HP out)
  0x01671C40,
  0x01671D10,
  0x01671E21,
  0x01671F03,
  // Node 17(port G): 91170110(SPK/Class_D)
  0x1771C10,
  0x1771D01,
  0x1771E17,
  0x1771F91,
  // Node 18(Port B): 40F001F0(Mic/Line In)-Disable
  0x01871CF0,
  0x01871D01,
  0x01871EF0,
  0x01871F40,
  // Node 19(Port D): 03A11030(Headset Mic)
  0x01971C30,
  0x01971D10,
  0x01971EA1,
  0x01971F03,
  // Node 1A(port C): 40F001F0(1st Dig-Mic I/F)-Disable
  0x1A71CF0,
  0x1A71D01,
  0x1A71EF0,
  0x1A71F40,
  // Node 1D(port E):40F001F0(I2S Line-Out)-Disable
  0x01D71CF0,
  0x01D71D01,
  0x01D71EF0,
  0x01D71F40,
  // Node 1E(port F): 40F001F0(I2S Line-In)-Disable
  0x01E71CF0,
  0x01E71D01,
  0x01E71EF0,
  0x01E71F40,
  // Node 1F(port H): 40F001F0(2nd D-Mic I/F)-Disable
  0x01F71CF0,
  0x01F71D01,
  0x01F71EF0,
  0x01F71F40,
  // Node 21(port I): 40F001F0(SPDI/F)-Disable
  0x02171CF0,
  0x02171D01,
  0x02171EF0,
  0x02171F40,
  // Node 26(port J): 40F001F0(I2S Line-Out)-Disable
  0x2671CF0,
  0x2671D01,
  0x2671EF0,
  0x2671F40,
  // Node 26(port K): 40F001F0(I2S Line-In)-Disable
  0x2771CF0,
  0x2771D01,
  0x2771EF0,
  0x2771F40
);

HDAUDIO_VERB_TABLE HdaVerbTableAlc245DmicX4 = HDAUDIO_VERB_TABLE_INIT (
  //
  //  VerbTable: (Realtek ALC245)
  //  Revision ID = 0xff
  //  Codec Verb Table
  //  Codec Address: CAd value (0/1/2)
  //  Codec Vendor: 0x10EC0245
  //
  0x10EC, 0x0245,
  0xFF, 0xFF,
//===================================================================================================
//
//                               Realtek Semiconductor Corp.
//
//===================================================================================================

//Realtek High Definition Audio Configuration - Version : 5.0.3.0
//Realtek HD Audio Codec : ALC295
//PCI PnP ID : PCI\VEN_8086&DEV_2668&SUBSYS_72708086
//HDA Codec PnP ID : INTELAUDIO\FUNC_01&VEN_10EC&DEV_0245&SUBSYS_10EC124C

//    NID 0x12 : 0x90A60130
//    NID 0x13 : 0x90A60140
//    NID 0x14 : 0x90170110
//    NID 0x16 : 0x411111F0
//    NID 0x17 : 0x411111F0
//    NID 0x18 : 0x411111F0
//    NID 0x19 : 0x04A11040
//    NID 0x1A : 0x411111F0
//    NID 0x1B : 0x411111F0
//    NID 0x1D : 0x40E00001
//    NID 0x1E : 0x411111F0
//    NID 0x21 : 0x04211020


//===== HDA Codec Subsystem ID Verb-table =====
//HDA Codec Subsystem ID  : 0x10EC124C
0x0017204C,
0x00172112,
0x001722EC,
0x00172310,


//===== Pin Widget Verb-table =====
//Widget node 0x01 :
0x0017FF00,
0x0017FF00,
0x0017FF00,
0x0017FF00,
//===== 1bit reset =====
0x0205001A,
0x0204C003,
0x0205001A,
0x02048003,
//Pin widget 0x12 - DMIC
0x01271C30,
0x01271D01,
0x01271EA6,
0x01271F90,
//Pin widget 0x13 - DMIC
0x01371C40,
0x01371D01,
0x01371EA6,
0x01371F90,
//Pin widget 0x14 - Front (Port-D)
0x01471C10,
0x01471D01,
0x01471E17,
0x01471F90,
//Pin widget 0x16 - HP2-OUT (Port-B)
0x01671CF0,
0x01671D11,
0x01671E11,
0x01671F41,
//Pin widget 0x17 - I2S OUT
0x01771CF0,
0x01771D11,
0x01771E11,
0x01771F41,
//Pin widget 0x18 - I2S IN
0x01871CF0,
0x01871D11,
0x01871E11,
0x01871F41,
//Pin widget 0x19 - MIC2 (Port-F)
0x01971C40,
0x01971D10,
0x01971EA1,
0x01971F04,
//Pin widget 0x1A - LINE1 (Port-C)
0x01A71CF0,
0x01A71D11,
0x01A71E11,
0x01A71F41,
//Pin widget 0x1B - LINE2 (Port-E)
0x01B71CF0,
0x01B71D11,
0x01B71E11,
0x01B71F41,
//Pin widget 0x1D - BEEP-IN
0x01D71C01,
0x01D71D00,
0x01D71EE0,
0x01D71F40,
//Pin widget 0x1E - S/PDIF-OUT
0x01E71CF0,
0x01E71D11,
0x01E71E11,
0x01E71F41,
//Pin widget 0x21 - HP1-OUT (Port-I)
0x02171C20,
0x02171D10,
0x02171E21,
0x02171F04,
//Class-D power on reset
0x0205003C,
0x0204F175,
0x0205003C,
0x0204F135,
//NID 0x20 JD1 - 1 pin detect 2 port
0x02050009,
0x0204E003,
0x0205000A,
0x02047770,
//NID 0x20 HP-JD enable and EAPD as default
0x0205004A,
0x02042010,
0x02050010,
0x02040020,
//Set Class-d to 2W /4ohm and Smart amp Sine tone monitor setting
0x02050038,
0x02047909,
0x05C50000,
0x05C43982,
//Class D silent detection Enable(-84dB threshold)
0x02050037,
0x0204FE15,
0x02050030,
0x02049000,
// I2S Pad Floating Control  and DMIC Floating
0x05A50001,
0x05A4001F,
0x02050035,
0x02048D82,
//Class-d DC detect
0x0205006B,
0x0204A390,
0x0205006B,
0x0204A390,
//Class-d DC detect
0x0205006C,
0x02040C9E,
0x0205006D,
0x02040C00,
//restore default for ultra-low power mode
0x02050008,
0x02046A8C,
0x02050076,
0x0204F000,

0x0205000E,
0x020465C0,
0x02050033,
0x02048580,

0x02050069,
0x0204FDA8,
0x02050068,
0x02040000,

0x02050003,
0x02040002,
0x02050069,
0x02040000,

0x02050068,
0x02040001,
0x0205002E,
0x0204290E
);

HDAUDIO_VERB_TABLE HdaVerbTableAlc892 = HDAUDIO_VERB_TABLE_INIT (
  //
  //  VerbTable: (Realtek Audio ALC892)
  //  Revision ID = 0xff
  //  Codec Verb Table
  //  Codec Address: CAd value (0/1/2)
  //  Codec Vendor: 0x10EC0892
  //
  0x10EC, 0x0892,
  0xFF, 0xFF,
  //===================================================================================================
  //
  //                               Realtek Semiconductor Corp.
  //
  //===================================================================================================

  // Realtek High Definition Audio Configuration - Version : 5.0.3.2
  // Realtek HD Audio Codec : ALC892
  // PCI PnP ID : PCI\VEN_8086&DEV_2668&SUBSYS_72708086
  // HDA Codec PnP ID : INTELAUDIO\FUNC_01&VEN_10EC&DEV_0892&SUBSYS_10EC1266
  // The number of verb command block : 17

  //    NID 0x11 : 0x40000000
  //    NID 0x12 : 0x411111F0
  //    NID 0x14 : 0x01014010
  //    NID 0x15 : 0x01011012
  //    NID 0x16 : 0x01016011
  //    NID 0x17 : 0x01012014
  //    NID 0x18 : 0x01A19020
  //    NID 0x19 : 0x411111F0
  //    NID 0x1A : 0x0181302F
  //    NID 0x1B : 0x411111F0
  //    NID 0x1C : 0x411111F0
  //    NID 0x1D : 0x40232601
  //    NID 0x1E : 0x411111F0
  //    NID 0x1F : 0x411111F0

  //===== HDA Codec Subsystem ID Verb-table =====
  //HDA Codec Subsystem ID  : 0x10EC1266
  0x00172066,
  0x00172112,
  0x001722EC,
  0x00172310,
  //===== Pin Widget Verb-table =====
  //Widget node 0x01 :
  0x0017FF00,
  0x0017FF00,
  0x0017FF00,
  0x0017FF00,
  //Pin widget 0x11 - S/PDIF-OUT2
  0x01171C00,
  0x01171D00,
  0x01171E00,
  0x01171F40,
  //Pin widget 0x12 - DMIC
  0x01271CF0,
  0x01271D11,
  0x01271E11,
  0x01271F41,
  //Pin widget 0x14 - FRONT (Port-D)
  0x01471C10,
  0x01471D40,
  0x01471E01,
  0x01471F01,
  //Pin widget 0x15 - SURR (Port-A)
  0x01571C12,
  0x01571D10,
  0x01571E01,
  0x01571F01,
  //Pin widget 0x16 - CEN/LFE (Port-G)
  0x01671C11,
  0x01671D60,
  0x01671E01,
  0x01671F01,
  //Pin widget 0x17 - SIDESURR (Port-H)
  0x01771C14,
  0x01771D20,
  0x01771E01,
  0x01771F01,
  //Pin widget 0x18 - MIC1 (Port-B)
  0x01871C20,
  0x01871D90,
  0x01871EA1,
  0x01871F01,
  //Pin widget 0x19 - MIC2 (Port-F)
  0x01971CF0,
  0x01971D11,
  0x01971E11,
  0x01971F41,
  //Pin widget 0x1A - LINE1 (Port-C)
  0x01A71C2F,
  0x01A71D30,
  0x01A71E81,
  0x01A71F01,
  //Pin widget 0x1B - LINE2 (Port-E)
  0x01B71CF0,
  0x01B71D11,
  0x01B71E11,
  0x01B71F41,
  //Pin widget 0x1C - CD-IN
  0x01C71CF0,
  0x01C71D11,
  0x01C71E11,
  0x01C71F41,
  //Pin widget 0x1D - BEEP-IN
  0x01D71C01,
  0x01D71D26,
  0x01D71E23,
  0x01D71F40,
  //Pin widget 0x1E - S/PDIF-OUT
  0x01E71CF0,
  0x01E71D11,
  0x01E71E11,
  0x01E71F41,
  //Pin widget 0x1F - S/PDIF-IN
  0x01F71CF0,
  0x01F71D11,
  0x01F71E11,
  0x01F71F41,
  //Widget node 0x20 :
  0x02050007,
  0x020409C8,
  0x02050007,
  0x020409C8
);

HDA_VERB_TABLE_DATABASE HdaVerbTableDbAdlNoDmic = HDAUDIO_VERB_TABLE_DATABASE_INIT (
  &HdaVerbTableDisplayAudio,
  &HdaVerbTableAlc711,
  &HdaVerbTableAlc701,
  &HdaVerbTableAlc274,
  &HdaVerbTableAlc245NoDmic,
  &HdaVerbTableAlc897,
  &HdaVerbTableSenaryTech,
  &HdaVerbTableCS4207,
  &HdaVerbTableAlc892
);

HDA_VERB_TABLE_DATABASE HdaVerbTableDbAdlDmic = HDAUDIO_VERB_TABLE_DATABASE_INIT (
  &HdaVerbTableDisplayAudio,
  &HdaVerbTableAlc711,
  &HdaVerbTableAlc701,
  &HdaVerbTableAlc274,
  &HdaVerbTableAlc245DmicX4,
  &HdaVerbTableAlc897,
  &HdaVerbTableSenaryTech,
  &HdaVerbTableAlc892
);