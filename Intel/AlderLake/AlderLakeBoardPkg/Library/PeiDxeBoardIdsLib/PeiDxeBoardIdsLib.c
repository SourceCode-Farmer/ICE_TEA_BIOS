/** @file

@copyright
  INTEL CONFIDENTIAL
  Copyright 2015 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains a 'Sample Driver' and is licensed as such under the terms
  of your license agreement with Intel or your vendor. This file may be modified
  by the user, subject to the additional terms of the license agreement.

@par Specification Reference:
**/

#include "Uefi.h"

CHAR16 *mBoardIdIndex[] = {
//L"0123456789ABCDEF0123456789ABCDEF", // !! Do not exceed 32 characters !!
  L"TBD",                              // 0x00
  L"AlderLake-M LP4x RVP",             // 0x01
  L"AlderLake-M LP5 RVP",              // 0x02
  L"AlderLake-M LP5 PMIC RVP",         // 0x03
  L"TBD",                              // 0x04
  L"TBD",                              // 0x05
  L"TBD",                              // 0x06
  L"TBD",                              // 0x07
  L"TBD",                              // 0x08
  L"TBD",                              // 0x09
  L"TBD",                              // 0x0A
  L"TBD",                              // 0x0B
  L"TBD",                              // 0x0C
  L"TBD",                              // 0x0D
  L"TBD",                              // 0x0E
  L"AlderLake-M LP5x AEP",             // 0x0F
  L"AlderLake-P LP4x RVP",             // 0x10
  L"TBD",                              // 0x11
  L"AlderLake-P DDR5 RVP",             // 0x12
  L"AlderLake-P LP5 RVP",              // 0x13
  L"AlderLake-P DDR4 RVP",             // 0x14
  L"TBD",                              // 0x15
  L"TBD",                              // 0x16
  L"TBD",                              // 0x17
  L"TBD",                              // 0x18
  L"TBD",                              // 0x19
  L"TBD",                              // 0x1A
  L"TBD",                              // 0x1B
  L"TBD",                              // 0x1C
  L"TBD",                              // 0x1D
  L"TBD",                              // 0x1E
  L"TBD",                              // 0x1F
  L"AlderLake-S TGP-H DDR4 ERB2",      // 0x20
  L"AlderLake-S TGP-H DDR4 CRB",       // 0x21
  L"AlderLake-S TGP-H DDR4 CRB PPV",   // 0x22
  L"TBD",                              // 0x23
  L"AlderLake-S TGP-H DDR5 CRB",       // 0x24
  L"TBD",                              // 0x25
  L"ADL-S ADP-S DDR4 UDIMM ERB1",      // 0x26
  L"AlderLake-S ADP-S DDR4 UDIMM CRB", // 0x27
  L"ADL-S ADP-S DDR4 UDIMM CRB EV",    // 0x28
  L"ADL-S ADP-S DDR4 UDIMM CRB CPV",   // 0x29
  L"TBD",                              // 0x2A
  L"AlderLake-S ADP-S DDR5 UDIMM CRB", // 0x2B
  L"ADL-S ADP-S DDR5 UDIMM CRB PPV",   // 0x2C
  L"TBD",                              // 0x2D
  L"ADL-S ADP-S DDR5 UDIMM 2DPC CRB",  // 0x2E
  L"TBD",                              // 0x2F
  L"ADL-S ADP-S DDR5 SODIMM CRB",      // 0x30
  L"TBD",                              // 0x31
  L"ADL-S HSIO UDIMM DDR4 2DPC RVP",   // 0x32
  L"AlderLake S ADP-S DDR5 AEP",       // 0x33
  L"ADL-S ADP-S DDR5 Single Rail CRB", // 0x34
  L"ADL-S ADP-S DDR4 SODIMM CRB",      // 0x35
  L"ADL-S ADP-S DDR5 UDIMM OC CRB",    // 0x36
  L"TBD",                              // 0x37
  L"TBD",                              // 0x38
  L"TBD",                              // 0x39
  L"AlderLake-S SBGA DDR5 SODIMM ERB", // 0x3A
  L"AlderLake-S SBGA DDR5 SODIMM CRB", // 0x3B
  L"AlderLake-S SBGA DDR5 SODIMM PPV", // 0x3C
  L"TBD",                              // 0x3D
  L"AlderLake-S SBGA DDR4 SODIMM CRB", // 0x3E
  L"AlderLake-P Simics Board"          // 0x3F
};

UINT8 mSizeOfmBoardIndex = sizeof(mBoardIdIndex)/sizeof(CHAR16*);

