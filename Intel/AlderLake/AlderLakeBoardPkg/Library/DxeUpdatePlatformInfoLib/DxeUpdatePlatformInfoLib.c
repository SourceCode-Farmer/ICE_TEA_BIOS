/** @file

;******************************************************************************
;* Copyright (c) 2019, Insyde Software Corporation. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/
/** @file
   Updates Platform Information to Setup Data

@copyright
  INTEL CONFIDENTIAL
  Copyright 2018 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains a 'Sample Driver' and is licensed as such under the terms
  of your license agreement with Intel or your vendor. This file may be modified
  by the user, subject to the additional terms of the license agreement.

@par Specification Reference:
**/

#include <Library/DxeUpdatePlatformInfoLib.h>

/**

  This function update the UEFI variable "SetupVolatileData" to reflect current platform values,
  Return out if the variable is NOT found.

**/
VOID
UpdatePlatformInfo (
  VOID
  )
{

#if FixedPcdGetBool (PcdSetupEnable) == 1
  EFI_STATUS              Status;
  BOARD_INFO_SETUP        BoardInfoSetup;
  UINT32                  BoardInfoAttributes;
  SETUP_VOLATILE_DATA     SetupVolatileData;
  UINT32                  SetupVolAttributes;
  UINTN                   VariableSize;
  UINTN                   VolVariableSize;
  UINT64                  HbPciD0F0RegBase;
  SETUP_DATA              SetupData;
  UINT32                  VarAttributes;
  UINTN                   VarSize;

  DEBUG ((DEBUG_INFO, "UpdatePlatformInfo Start.\n"));

  SetMem (&SetupVolatileData, sizeof (SETUP_VOLATILE_DATA),0);

  VolVariableSize = sizeof (SETUP_VOLATILE_DATA);
  Status = gRT->GetVariable (
                  L"SetupVolatileData",
                  &gSetupVariableGuid,
                  &SetupVolAttributes,
                  &VolVariableSize,
                  &SetupVolatileData
                  );
  if (EFI_ERROR (Status)) {
    DEBUG ((DEBUG_ERROR, "UpdatePlatformInfo Get SetupVolatileData Variable Error.\n"));
    return;
  }

  SetMem (&BoardInfoSetup, sizeof (BOARD_INFO_SETUP), 0);

  VariableSize = sizeof (BOARD_INFO_SETUP);
  Status = gRT->GetVariable (
                  L"BoardInfoSetup",
                  &gBoardInfoVariableGuid,
                  &BoardInfoAttributes,
                  &VariableSize,
                  &BoardInfoSetup
                  );
  if (EFI_ERROR (Status)) {
    DEBUG ((DEBUG_ERROR, "UpdatePlatformInfo Get BoardInfoSetup Variable Error.\n"));
    return;
  }

  //
  // Get VTd status
  //
  HbPciD0F0RegBase = PCI_SEGMENT_LIB_ADDRESS (SA_SEG_NUM, SA_MC_BUS, SA_MC_DEV, SA_MC_FUN, 0);
  SetupVolatileData.VTdAvailable = (PciSegmentRead32 (HbPciD0F0RegBase + 0xe4) & BIT23) ? 0 : 1;

  // @todo separate board information varibales from Setup variables.
  BoardInfoSetup.EcPresent               = PcdGetBool (PcdEcPresent);
  BoardInfoSetup.EcEspiFlashSharingMode  = PcdGet8 (PcdEcEspiFlashSharingMode);
  BoardInfoSetup.EcPeciMode              = PcdGet8 (PcdEcPeciMode);
  BoardInfoSetup.EcMajorRevision         = PcdGet8 (PcdEcMajorRevision);
  BoardInfoSetup.EcMinorRevision         = PcdGet8 (PcdEcMinorRevision);
  BoardInfoSetup.BoardId                 = PcdGet16 (PcdBoardId);
  BoardInfoSetup.SpdPresent              = PcdGetBool (PcdSpdPresent);
  BoardInfoSetup.PlatformGeneration      = PcdGet8 (PcdPlatformGeneration);
  SetupVolatileData.PlatformFlavor = PcdGet8 (PcdPlatformFlavor);
  BoardInfoSetup.FabId          = PcdGet16 (PcdBoardRev);
  BoardInfoSetup.BoardBomId     = PcdGet16 (PcdBoardBomId);
  SetupVolatileData.PlatformType   = PcdGet8 (PcdPlatformType);
  SetupVolatileData.BoardType      = PcdGet8 (PcdBoardType);
//[-start-190906-IB04530031-modify]//
  SetupVolatileData.PlatId         = PcdGet16 (PcdCrbSkuId);
//[-end-190906-IB04530031-modify]//
  SetupVolatileData.PchGeneration  = PchGeneration ();
  SetupVolatileData.DockAttached   = PcdGetBool (PcdDockAttached);
  BoardInfoSetup.DisplayId         = PcdGet16 (PcdDisplayId);
  SetupVolatileData.TcssPdType     = PcdGet8 (PcdTcssPdType);
  // Update No of Type-C Port Supported
  if (PcdGetBool (PcdUsbTypeCSupport)) {
    SetupVolatileData.TotalNumberOfTypeCPortsSupported = PcdGet8 (PcdTypeCPortsSupported);
  }
  SetupVolatileData.UsbcEcPdNegotiation  = PcdGetBool (PcdUsbcEcPdNegotiation) ? 1 : 0;

  if (PcdGetBool (PcdEcPresent)) {
    VarSize = sizeof (SETUP_DATA);
    Status = gRT->GetVariable (
                    L"Setup",
                    &gSetupVariableGuid,
                    &VarAttributes,
                    &VarSize,
                    &SetupData
                    );
    if (!EFI_ERROR (Status)) {
      SetupData.EcPeciMode = PcdGet8 (PcdEcPeciMode);
      DEBUG ((DEBUG_INFO, "SetupData.EcPeciMode = %d\n", SetupData.EcPeciMode));
      Status = gRT->SetVariable (
                      L"Setup",
                      &gSetupVariableGuid,
                      VarAttributes,
                      VarSize,
                      &SetupData
                      );
      ASSERT_EFI_ERROR (Status);
    }
  }

  DEBUG ((DEBUG_INFO, "SetupVolatileData.PchGeneration = %d\n", SetupVolatileData.PchGeneration));
  SetupVolAttributes = EFI_VARIABLE_BOOTSERVICE_ACCESS;
  Status = gRT->SetVariable (
                  L"SetupVolatileData",
                  &gSetupVariableGuid,
                  SetupVolAttributes,
                  sizeof (SETUP_VOLATILE_DATA),
                  &SetupVolatileData
                  );
  if (EFI_ERROR (Status)) {
    DEBUG ((DEBUG_ERROR, "UpdatePlatformInfo Set SetupVolatileData Variable Error.\n"));
    ASSERT_EFI_ERROR (Status);
    return;
  }

  Status = gRT->SetVariable (
                  L"BoardInfoSetup",
                  &gBoardInfoVariableGuid,
                  BoardInfoAttributes,
                  sizeof (BOARD_INFO_SETUP),
                  &BoardInfoSetup
                  );
  if (EFI_ERROR (Status)) {
    DEBUG ((DEBUG_ERROR, "UpdatePlatformInfo Set BoardInfoSetup Variable Error.\n"));
    ASSERT_EFI_ERROR (Status);
    return;
  }

  DEBUG ((DEBUG_INFO, "UpdatePlatformInfo End.\n"));

#else
  //
  // No Setup Data to update
  //
  DEBUG ((DEBUG_INFO, "UpdatePlatformInfo Null Function.\n"));

#endif
  return;
}
