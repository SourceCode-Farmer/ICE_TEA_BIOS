#! python3
## @file
# StripFlashmap.py
#
# This script removes unneeded lines in fdf according to Build Target.
# This is for the subsequent FSP rebase script which does not support "!if $(TARGET) == DEBUG/!else/!endif" condictions.
# Only one pair of "!if $(TARGET) == DEBUG/!else/!endif" is supported.
#
# Copyright (c) 2020, Intel Corporation. All rights reserved.
# This software and associated documentation (if any) is furnished
# under a license and may only be used or copied in accordance
# with the terms of the license. Except as permitted by such
# license, no part of this software or documentation may be
# reproduced, stored in a retrieval system, or transmitted in any
# form or by any means without the express written consent of
# Intel Corporation.
##

import os
import sys
import re
import subprocess

if len(sys.argv) != 4:
  print("StripFlashmap.py - Error in number of arguments received")
  print("Usage - StripFlashmap.py <Build Target - DEBUG/RELEASE> <FlashMap file path> <Outputted FlashMap file path>")
  exit(1)

buildTarget         = sys.argv[1].upper()
flashMapName        = sys.argv[2]
outputflashMapName  = sys.argv[3]

#
# Make sure argument passed or valid
#
if not (buildTarget in "DEBUG" or buildTarget in "RELEASE") :
  print("WARNING!  " + str(buildTarget) + " is not a valid build target")
  exit(1)
if not os.path.exists(flashMapName):
  print("WARNING!  " + str(flashMapName) + " is not found.")
  exit(1)

#
# Get Flash Map
#
file = open (flashMapName, "r")
data = file.read ()
file.close

# Get the common data before !if $(TARGET) == DEBUG
flashmapcommonhead = data.rsplit("!if $(TARGET) == DEBUG", 1)[0]

# Based on Build Target, select the section in the FlashMap file
if buildTarget in "DEBUG":
  flashmap = data.rsplit("!if $(TARGET) == DEBUG", 1)[1].split("!else")[0]
else:
  flashmap = data.rsplit("!else", 1)[1].split("!endif")[0]

# Get the tail common data after !endif
flashmapcommontail = data.rsplit("!endif", 1)[1]

strippedFlashMap = flashmapcommonhead + flashmap + flashmapcommontail


outputflashMap = open(outputflashMapName, "w")
outputflashMap.write(strippedFlashMap)
outputflashMap.close()

exit(0)
