@REM @file
@REM
@REM @copyright
@REM  INTEL CONFIDENTIAL
@REM  Copyright 2017 - 2021 Intel Corporation.
@REM
@REM  The source code contained or described herein and all documents related to the
@REM  source code ("Material") are owned by Intel Corporation or its suppliers or
@REM  licensors. Title to the Material remains with Intel Corporation or its suppliers
@REM  and licensors. The Material may contain trade secrets and proprietary and
@REM  confidential information of Intel Corporation and its suppliers and licensors,
@REM  and is protected by worldwide copyright and trade secret laws and treaty
@REM  provisions. No part of the Material may be used, copied, reproduced, modified,
@REM  published, uploaded, posted, transmitted, distributed, or disclosed in any way
@REM  without Intel's prior express written permission.
@REM
@REM  No license under any patent, copyright, trade secret or other intellectual
@REM  property right is granted to or conferred upon you by disclosure or delivery
@REM  of the Materials, either expressly, by implication, inducement, estoppel or
@REM  otherwise. Any license under such intellectual property rights must be
@REM  express and approved by Intel in writing.
@REM
@REM  Unless otherwise agreed by Intel in writing, you may not remove or alter
@REM  this notice or any other notice embedded in Materials by Intel or
@REM  Intel's suppliers or licensors in any way.
@REM
@REM  This file contains a 'Sample Driver' and is licensed as such under the terms
@REM  of your license agreement with Intel or your vendor. This file may be modified
@REM  by the user, subject to the additional terms of the license agreement.
@REM
@REM @par Specification Reference:
@REM

@REM #
@REM #  Module Name:
@REM #
@REM #    postbuild.bat
@REM #
@REM #  Abstract:
@REM #
@REM #    Post build script.
@REM #
@REM #--*/

echo on

@set SCRIPT_ERROR=0

setlocal ENABLEDELAYEDEXPANSION
@echo Flash Map File %FLASHMAP_FDF%
@set PREMEM_FV_ADDRESS=

@if not defined PYTHON_COMMAND (
  set PYTHON_COMMAND=py -3
)



@if not defined WORKSPACE (
    echo.
    echo !!! ERROR !!! WORKSPACE must be defined before executing postbuild.bat !!!
    echo.
    set SCRIPT_ERROR=1
    goto :EndPostBuild
)

@if not defined WORKSPACE_PLATFORM (
    echo.
    echo !!! ERROR !!! WORKSPACE_PLATFORM must be defined before executing postbuild.bat !!!
    echo.
    set SCRIPT_ERROR=1
    goto :EndPostBuild
)

pushd %WORKSPACE%

@if not %CD% == %WORKSPACE% (
    echo.
    echo !!! ERROR !!! postbuild.bat is not executed under %WORKSPACE%
    echo.
    set SCRIPT_ERROR=1
    goto :EndPostBuild
)

@set PATH=%PATH%;%BUILD_DIR_PATH%\FV
copy /y %BUILD_DIR%\FV\GuidedSectionTools.txt %BUILD_DIR%\FV\GuidToolDefinitionConf.ini
copy /y %BUILD_DIR%\FV\GuidedSectionTools.txt %BUILD_DIR%\FV\FmmtConf.ini

@rem
@rem Get the prefixes for ROM Images
@rem

@if "%TARGET%" == "DEBUG" (
  set BUILD_TYPE=D
) else (
  set BUILD_TYPE=R
)

for /f "usebackq tokens=3" %%i in (`FINDSTR /b /c:"VERSION_MAJOR" %WORKSPACE%\%BIOS_ID_FILE%`) do (
  for /f "usebackq tokens=3" %%j in (`FINDSTR /b /c:"VERSION_MINOR" %WORKSPACE%\%BIOS_ID_FILE%`) do (
      set BIOS_MAJOR_VERSION=%%i
      set BIOS_MIN_VERSION=%%j
  )
)

set BIOS_PREFIX=ADL_S
set IFWI_PREFIX=ADLS
set IFWI_DIR=%WORKSPACE_PLATFORM%\%PLATFORM_FULL_PACKAGE%\Tools\RomImage\PreSiIfwi
@if /I "%EMBEDDED_BUILD%" EQU "TRUE" (
  set BIOS_PREFIX=ADL_SI
)
@if /I "%ADLP_BUILD%" EQU "TRUE" (
  set BIOS_PREFIX=ADL_P
  set IFWI_PREFIX=ADLP
  set IFWI_VERSION=_WW40.4
)

@REM
@REM XmlCli: Post Build Process Begin
@REM
set WORKSPACE_BUILD_DIR=%WORKSPACE%\%BUILD_DIR%
set XML_CLI_COMMON_BUILD_PATH=%WORKSPACE_BUILD_DIR%\X64\XmlCliFeaturePkg\XmlCliCommon
set XML_CLI_COMMON_DIR=%WORKSPACE_COMMON%\Features\XmlCliFeaturePkg\XmlCliCommon
@if exist %XML_CLI_COMMON_BUILD_PATH% (
  @echo Create and insert Bios Knobs Data Bin file for XmlCli
  @call %PYTHON_COMMAND% %XML_CLI_COMMON_DIR%\Tool\ParseSetup.py post_build -b %WORKSPACE_BUILD_DIR% -xb %XML_CLI_COMMON_BUILD_PATH% -iv %WORKSPACE_BUILD_DIR%\X64\%PLATFORM_FULL_PACKAGE%\Setup\Setup\OUTPUT %WORKSPACE_BUILD_DIR%\X64\%PLATFORM_FULL_PACKAGE%\Features\UiApp\D89A7D8B-D016-4D26-93E3-EAB6B4D3B0A2\OUTPUT %WORKSPACE_BUILD_DIR%\X64\%PLATFORM_RESTRICTED_PACKAGE%\TestMenuSetup\Dxe\TestMenuSetup\OUTPUT %WORKSPACE_BUILD_DIR%\X64\SecurityPkg\Tcg\Tcg2Config\Tcg2ConfigDxe\OUTPUT %XML_CLI_COMMON_BUILD_PATH%\Dxe\OUTPUT -edk %EDK_TOOLS_BIN%
  @echo ==== XmlCliPostBuild: BiosKnobsData.bin File Created =====

  @if not exist %BUILD_DIR%\FV\ClientBios_XmlCli.fd (
    @echo === XmlCliPostBuild: Trying to generate fd file once again... ===
    %EDK_TOOLS_BIN%\FMMT.exe -a %WORKSPACE%\%BUILD_DIR%\FV\ClientBios.fd A881D567-6CB0-4EEE-8435-2E72D33E45B5 %XML_CLI_COMMON_BUILD_PATH%\BiosKnobsData.ffs %WORKSPACE%\%BUILD_DIR%\FV\ClientBios_XmlCli.fd
    @if not exist %BUILD_DIR%\FV\ClientBios_XmlCli.fd (
      @echo !!! XmlCliPostBuild-ERROR:Build Fail !!!
      @echo *** XmlCliPostBuild: %WORKSPACE%\%BUILD_DIR%\FV\ClientBios_XmlCli.fd does not exist. ***
      @echo -- XmlCliPostBuild: FMMT insertion process may have failed, please check logs above --
      set SCRIPT_ERROR=1
    )
  )
  @REM backing up ClientBios.fd before overwriting
  @if not exist %BUILD_DIR%\FV\ClientBios.fd.bak (
    copy %BUILD_DIR%\FV\ClientBios.fd %BUILD_DIR%\FV\ClientBios.fd.bak
  )
  copy /y /b %BUILD_DIR%\FV\ClientBios_XmlCli.fd %BUILD_DIR%\FV\ClientBios.fd
  @REM del %BUILD_DIR%\FV\ClientBios_XmlCli.fd
  del %BUILD_DIR%\FV\ClientBios.fd.bak
  @echo ==== XmlCliPostBuild: BiosKnobsData.bin File Inserted =====
)
@REM
@REM XmlCli: Post Build Process End
@REM

@REM
REM Post-build FitGen
@REM
@set BIOS_INFO_GUID=4A4CA1C6-871C-45bb-8801-6910A7AA5807
@set STARTUP_AC_MODULE_GUID=26fdaa3d-b7ed-4714-8509-eecf1593800d
@set MICROCODE_ARRAY_FFS_GUID=197DB236-F856-4924-90F8-CDF12FB875F3

findstr %STARTUP_AC_MODULE_GUID% %BUILD_DIR%\FV\FvFwBinaries.inf > NUL
@if %errorlevel% NEQ 0 goto NoStartupAcm
set STARTUP_AC_PARA=-S %STARTUP_AC_MODULE_GUID%

:NoStartupAcm

@REM
REM Run FCE tool to write default setup variable to FD image
@REM
@if %SETUP_ENABLE% EQU FALSE (
  goto NoSetup
)
copy /y %BUILD_DIR%\FV\ClientBios.fd %BUILD_DIR%\FV\ClientBios_FcePre.fd
REM Prepend FvSetupData to enable processing
copy /y /b %BUILD_DIR%\FV\FvSetupData.fv + %BUILD_DIR%\FV\ClientBios.fd %BUILD_DIR%\FV\ClientSetup_Pre.fd
@REM UQI 0006 005C 0078 0030 0034 0039 0046 is for PlatId question prompt (STR_PLATID_PROMPT) UQI string value.
@REM First 0006 is the length of string; Next six byte values are mapped to STR_PLATID_PROMPT string value (\x049F) defined in %WORKSPACE_PLATFORM%\%PLATFORM_PACKAGE%\Setup\UqiList.uni.
%EDK_TOOLS_BIN%\FCE.exe ^
read ^
-i %BUILD_DIR%\FV\ClientSetup_Pre.fd ^
0006 005C 0078 0030 0034 0039 0046 > %BUILD_DIR%\FV\ClientBios.fd.config
@if %errorlevel% NEQ 0 (
  set SCRIPT_ERROR=1
  goto EndPostBuild
)

REM Place FFS file in FvPreMemory
%EDK_TOOLS_BIN%\FCE.exe ^
update ^
-i %BUILD_DIR%\FV\ClientSetup_Pre.fd ^
-s %BUILD_DIR%\FV\ClientBios.fd.config ^
-o %BUILD_DIR%\FV\ClientSetup.fd ^
-g FC8FE6B5-CD9B-411E-BD8F-31824D0CDE3D ^
-a
@if %errorlevel% NEQ 0 (
  set SCRIPT_ERROR=1
  goto EndPostBuild
)

REM Strip FvSetupData back out of the image
@if not exist %BUILD_DIR%\FV\ClientSetup.fd (
  @echo Error: %BUILD_DIR%\FV\ClientSetup.fd not found
  set SCRIPT_ERROR=1
  goto EndPostBuild
)

call split -f %BUILD_DIR%\FV\ClientSetup.fd -s 0x120000
@if %errorlevel% NEQ 0 (
  set SCRIPT_ERROR=1
  goto EndPostBuild
)
del %BUILD_DIR%\FV\ClientSetup.fd1
move /y %BUILD_DIR%\FV\ClientSetup.fd2 %BUILD_DIR%\FV\ClientBios_FcePost.fd

copy /y %BUILD_DIR%\FV\ClientBios_FcePost.fd %BUILD_DIR%\FV\ClientBios.fd

:NoSetup


@REM
@REM Cutting off unused part of Extended BIOS Region out of the FD image
@REM
@if %EXTENDEDREGION_BUILD% EQU FALSE goto SkipExtendedRegionPostBuildProcess
@if not defined FLASHMAP_FDF goto SkipExtendedRegionPostBuildProcess
  @set EXTENDED_REGION_SIZE=
  @set EXTENDED_REGION_IN_USE=
  @for /f "tokens=4" %%i in ('@findstr /c:"DEFINE EXTENDED_REGION_SIZE " %FLASHMAP_FDF%') do @set EXTENDED_REGION_SIZE=%%i
  @for /f "tokens=4" %%j in ('@findstr /c:"DEFINE EXTENDED_REGION_IN_USE" %FLASHMAP_FDF%') do @set EXTENDED_REGION_IN_USE=%%j
  @echo Extended BIOS Region size        : %EXTENDED_REGION_SIZE%
  @echo Extended BIOS Region size in use : %EXTENDED_REGION_IN_USE%
  @set /a CUTOFF_SIZE="%EXTENDED_REGION_SIZE%-%EXTENDED_REGION_IN_USE%"
  @echo Cutting off unused %CUTOFF_SIZE% in size
  @copy /y /b %BUILD_DIR%\FV\ClientBios.fd %BUILD_DIR%\FV\ClientBios_ExtSplitPre.fd
  call split -f %BUILD_DIR%\FV\ClientBios.fd -s %CUTOFF_SIZE% -t %BUILD_DIR%\FV\ClientBios_ExtSplitPost.fd -o %BUILD_DIR%\FV\Garbage.bin
  @copy /y /b %BUILD_DIR%\FV\ClientBios_ExtSplitPost.fd %BUILD_DIR%\FV\ClientBios.fd
  @del %BUILD_DIR%\FV\Garbage.bin
:SkipExtendedRegionPostBuildProcess


@rem
@REM Remove MinPlatformPkg module instances that are overridden in AlderLakeBoardPkg
@REM
copy %BUILD_DIR%\FV\ClientBios.fd %BUILD_DIR%\FV\ClientBios_FmmtPre.fd /Y
@set MODULE_REMOVER_CMD=py -3 %WORKSPACE_PLATFORM%\%PLATFORM_BOARD_PACKAGE%\module_remover.py
@set FMMT_PATH=%EDK_TOOLS_BIN%\FMMT.exe
@set MODULE_LIST=UiApp
%MODULE_REMOVER_CMD% %FMMT_PATH% %BUILD_DIR%\FV\ClientBios.fd %MODULE_LIST%
@if %errorlevel% NEQ 0 (
  @echo Failure in running FMMT by module remover.
  set SCRIPT_ERROR=1
  goto EndPostBuild
)
@rem
@rem Capsule update fault tolerance support begin
@rem
@rem Below scripts calculate the digest of OBB region to replace the dummy one in FvPreMemory.
@rem The purpose of OBB digest is to examine the integrity when loading OBB from external storage for recovery.
@rem So this section must be executed when:
@rem   1. After all operations on OBB FVs are done.
@rem   2. Before BootGuard BPM/KM is created.
@rem
@rem gObbSha256HashFileGuid
@set OBB_HASH_FILE_GUID=F57757FC-2603-404F-AAE2-34C6232388E8
@set OBB_HASH_TMP_FOLDER=%BUILD_DIR%\FV\ObbDigestTmp
@call %EDK_TOOLS_BIN%\FMMT.exe -v %BUILD_DIR%\FV\FVPREMEMORY.FV | findstr /c:"ObbDigest"
@if !errorlevel! NEQ 0 (
  echo No ObbDigest found in FvPreMemory. Skip fault tolerance support process in postbuild
  goto :SkipFaultToleranceSupport
)
@if not exist %OBB_HASH_TMP_FOLDER% mkdir %OBB_HASH_TMP_FOLDER%

@rem
@rem Generate Obb.bin. Calculate Obb size and offset for splitting BIOS ROM
@rem
@for /f "tokens=4" %%i in ('@findstr /c:"gBoardModuleTokenSpaceGuid\.PcdFlashObbOffset" %FLASHMAP_FDF%') do @set FLASH_OBB_OFFSET=%%i
@for /f "tokens=4" %%i in ('@findstr /c:"gBoardModuleTokenSpaceGuid\.PcdFlashObbSize" %FLASHMAP_FDF%') do @set FLASH_OBB_SIZE=%%i

@set FLASH_NVS_OBB_SIZE=
@set /a FLASH_NVS_OBB_SIZE="!FLASH_OBB_OFFSET!+!FLASH_OBB_SIZE!"
@REM
@REM A tricky way to covert FW Version from DEC to HEX
@rem
call cmd /c exit /b !FLASH_NVS_OBB_SIZE!
set FLASH_NVS_OBB_SIZE=0x!=exitcode!

@rem
@rem Split ClientBios.fd to get Obb.bin
@rem
call split -f %BUILD_DIR%\FV\ClientBios.fd -s !FLASH_NVS_OBB_SIZE! -o %OBB_HASH_TMP_FOLDER%\NvsObb.bin -t %OBB_HASH_TMP_FOLDER%\IBBx.bin
call split -f %OBB_HASH_TMP_FOLDER%\NvsObb.bin -s !FLASH_OBB_OFFSET! -o %OBB_HASH_TMP_FOLDER%\Nvs.bin -t %OBB_HASH_TMP_FOLDER%\Obb.bin

@rem
@rem Calculate Obb SHA256 digest and replace DummySha256Digest.bin with it.
@rem
call %OPENSSL_PATH%\openssl.exe dgst -binary -sha256 %OBB_HASH_TMP_FOLDER%\Obb.bin > %OBB_HASH_TMP_FOLDER%\ObbDigest.bin
@rem
@rem Prepare ObbDigest.ffs
@rem
call %EDK_TOOLS_BIN%\GenSec.exe -s EFI_SECTION_USER_INTERFACE -n "ObbDigest" -o %OBB_HASH_TMP_FOLDER%\ObbDigest.ui
call %EDK_TOOLS_BIN%\GenSec.exe -s EFI_SECTION_RAW %OBB_HASH_TMP_FOLDER%\ObbDigest.bin -o %OBB_HASH_TMP_FOLDER%\ObbDigest.raw
call %EDK_TOOLS_BIN%\GenFfs.exe -t EFI_FV_FILETYPE_FREEFORM -g %OBB_HASH_FILE_GUID% -o %OBB_HASH_TMP_FOLDER%\ObbDigest.ffs -i %OBB_HASH_TMP_FOLDER%\ObbDigest.raw -i %OBB_HASH_TMP_FOLDER%\ObbDigest.ui
@rem
@rem Replace ObbDigest.ffs in ClientBios.fd (FvPreMemory) with the actual one
@rem
call %EDK_TOOLS_BIN%\FMMT.exe -r %BUILD_DIR%\FV\ClientBios.fd FC8FE6B5-CD9B-411E-BD8F-31824D0CDE3D ObbDigest %OBB_HASH_TMP_FOLDER%\ObbDigest.ffs %OBB_HASH_TMP_FOLDER%\ClientBios.fd
copy /y /b %OBB_HASH_TMP_FOLDER%\ClientBios.fd %BUILD_DIR%\FV\ClientBios.fd
@rem Keep Obb.bin for Resiliency Obb BGUP build below
copy /y /b %OBB_HASH_TMP_FOLDER%\Obb.bin %BUILD_DIR%\FV\Obb.bin
if exist %OBB_HASH_TMP_FOLDER% rmdir /q /s %OBB_HASH_TMP_FOLDER%
@rem
@rem Capsule update fault tolerance support end
@rem
:SkipFaultToleranceSupport

@rem
@rem ChasmFalls Resiliency support begin
@rem
@rem Below scripts build two BGUPs and replace the dummy ones in FvPreMemory for resiliency support.
@rem The purpose is to support IBB/IBBR sync up and OBB restore when BiosGuard is enabled.
@rem So this section must be executed when:
@rem   1. After all operations on OBB FVs are done.
@rem   2. Before BootGuard BPM/KM is created.
@rem
@rem gIbbBgslFileGuid
@set IBB_BGSL_FILE_GUID=f53fc14b-025c-4477-9b48-7a1b19f80f30
@rem gObbRBgslFileGuid
@set OBB_BGSL_FILE_GUID=318d30b7-f669-4af2-ade1-e3f84d097bb3
@set BGSL_TMP_FOLDER=%BUILD_DIR%\FV\BgslTmp
@set BIOS_GUARD_UPDATE_PACKAGE_PATH=%WORKSPACE_PLATFORM%\%PLATFORM_FULL_PACKAGE%\InternalOnly\ToolScripts\BiosGuard\UpdatePackage\
@rem Get Bios version for Bios Gaurd SVN
@set BIOS_GUARD_SVN=
@for /f "usebackq tokens=3" %%i in (`FINDSTR /b /c:"VERSION_MAJOR" %WORKSPACE_ROOT%\%BIOS_ID_FILE%`) do (set BIOS_GUARD_SVN=%%i)
@if %RESILIENCY_BUILD% EQU TRUE (
  @if not exist %BGSL_TMP_FOLDER% mkdir %BGSL_TMP_FOLDER%
  @rem
  @rem Generate Ibb <-> IbbR sync up BGUP.
  @rem
  call %BIOS_GUARD_UPDATE_PACKAGE_PATH%\BuildBGUP_SPI.bat -p script_BuildBGUP_template_Pri_Sec_Sync.bgsl -v !BIOS_GUARD_SVN! -use_ftu false
  call %PYTHON_COMMAND% %BIOS_GUARD_UPDATE_PACKAGE_PATH%\BiosGuardCapsule.py -i update_package.BIOS_Guard -o capsule_update_package.BIOS_Guard
  copy /b capsule_update_package.BIOS_Guard + update_package_bgupc.biosguard %BGSL_TMP_FOLDER%\IbbBgsl.bin
  @REM Delete BGUP related build files
  @del script.bin 1>NUL 2>&1
  @del *update_package* 1>NUL 2>&1
  @rem
  @rem Generate OBB_to_SPI BGUP.
  @rem
  call %BIOS_GUARD_UPDATE_PACKAGE_PATH%\BuildBGUP_SPI.bat -d %BUILD_DIR%\FV\Obb.bin -p script_BuildBGUP_OBBR_template.bgsl -v !BIOS_GUARD_SVN! -use_ftu false
  call %PYTHON_COMMAND% %BIOS_GUARD_UPDATE_PACKAGE_PATH%\BiosGuardCapsule.py -i update_package.BIOS_Guard -o capsule_update_package.BIOS_Guard
  copy /b capsule_update_package.BIOS_Guard + update_package_bgupc.biosguard %BGSL_TMP_FOLDER%\ObbRBgsl.bin
  @REM Delete BGUP related build files
  @del script.bin 1>NUL 2>&1
  @del *update_package* 1>NUL 2>&1

  @rem
  @rem Prepare IbbBgsl.ffs
  @rem
  call %EDK_TOOLS_BIN%\GenSec.exe -s EFI_SECTION_USER_INTERFACE -n "IbbBgsl" -o %BGSL_TMP_FOLDER%\IbbBgsl.ui
  call %EDK_TOOLS_BIN%\GenSec.exe -s EFI_SECTION_RAW %BGSL_TMP_FOLDER%\IbbBgsl.bin -o %BGSL_TMP_FOLDER%\IbbBgsl.raw
  call %EDK_TOOLS_BIN%\GenFfs.exe -t EFI_FV_FILETYPE_FREEFORM -g %IBB_BGSL_FILE_GUID% -o %BGSL_TMP_FOLDER%\IbbBgsl.ffs -i %BGSL_TMP_FOLDER%\IbbBgsl.raw -i %BGSL_TMP_FOLDER%\IbbBgsl.ui

  @rem
  @rem Prepare ObbRBgsl.ffs
  @rem
  call %EDK_TOOLS_BIN%\GenSec.exe -s EFI_SECTION_USER_INTERFACE -n "ObbRBgsl" -o %BGSL_TMP_FOLDER%\ObbRBgsl.ui
  call %EDK_TOOLS_BIN%\GenSec.exe -s EFI_SECTION_RAW %BGSL_TMP_FOLDER%\ObbRBgsl.bin -o %BGSL_TMP_FOLDER%\ObbRBgsl.raw
  call %EDK_TOOLS_BIN%\GenFfs.exe -t EFI_FV_FILETYPE_FREEFORM -g %OBB_BGSL_FILE_GUID% -o %BGSL_TMP_FOLDER%\ObbRBgsl.ffs -i %BGSL_TMP_FOLDER%\ObbRBgsl.raw -i %BGSL_TMP_FOLDER%\ObbRBgsl.ui

  @rem
  @rem Replace IbbBgsl.ffs and ObbRBgsl.ffs in ClientBios.fd (FvPreMemory) with the actual one
  @rem
  call %EDK_TOOLS_BIN%\FMMT.exe -r %BUILD_DIR%\FV\ClientBios.fd FC8FE6B5-CD9B-411E-BD8F-31824D0CDE3D IbbBgsl %BGSL_TMP_FOLDER%\IbbBgsl.ffs %BGSL_TMP_FOLDER%\ClientBios_tmp.fd
  call %EDK_TOOLS_BIN%\FMMT.exe -r %BGSL_TMP_FOLDER%\ClientBios_tmp.fd FC8FE6B5-CD9B-411E-BD8F-31824D0CDE3D ObbRBgsl %BGSL_TMP_FOLDER%\ObbRBgsl.ffs %BGSL_TMP_FOLDER%\ClientBios.fd
  copy /y /b %BGSL_TMP_FOLDER%\ClientBios.fd %BUILD_DIR%\FV\ClientBios.fd
  if exist %BGSL_TMP_FOLDER% rmdir /q /s %BGSL_TMP_FOLDER%
)
@rem
@rem ChasmFalls Rresiliency support end
@rem

@REM
REM Run FitGen tool to generate FIT
@REM
copy %BUILD_DIR%\FV\ClientBios.fd %BUILD_DIR%\FV\ClientBios_FitPre.fd /Y

@REM
@REM FitGen parameter usage reference:
@REM   -D It is FD file instead of FV file.
@REM   -F <FitTablePointerOffset>
@REM   -L <MicrocodeSlotSize> <MicrocodeFfsGuid>
@REM   -I <BiosInfoGuid>
@REM   -NA No 0x800 aligned Microcode requirement. No -NA means Microcode is 0x800 aligned.
@REM   -O RecordType <RecordDataAddress RecordDataSize>|<RESERVE RecordDataSize>
@REM   -P RecordType <IndexPort DataPort Width Bit Index>
@REM
@REM For slot mode, the SLOT_SIZE envir variable already defined in prep.bat script
@REM

%EDK_TOOLS_BIN%\FitGen.exe ^
-D ^
%BUILD_DIR%\FV\ClientBios.fd ^
%BUILD_DIR%\FV\ClientBios.fd ^
-F 0x40 ^
-NA ^
-L %SLOT_SIZE% %MICROCODE_ARRAY_FFS_GUID% ^
-I %BIOS_INFO_GUID% ^
%STARTUP_AC_PARA% ^
-O 0x0C RESERVE 0x600 ^
-O 0x0B RESERVE 0x400 ^
-P 0xA 0x70 0x71 0x1 0x4 0x2a

@if %errorlevel% NEQ 0 (
 echo Error: FitGen failure
 set SCRIPT_ERROR=1
 goto EndPostBuild
)

copy /b %BUILD_DIR%\FV\ClientBios.fd %BUILD_DIR%\FV\ClientBios_Post_Fit.fd /Y

:SkipGenFitTable
@if %STARTUP_ACM_ENABLE% EQU FALSE (
  goto :NoAcm
)

@REM
@echo Create BTG keymanifest and Boot Policy Manifest
@REM

@rem
if exist %WORKSPACE_PLATFORM%\%PLATFORM_FULL_PACKAGE%\Tools\ToolScripts\BpmGen\postbuildBpmGen.bat (
  call %WORKSPACE_PLATFORM%\%PLATFORM_FULL_PACKAGE%\Tools\ToolScripts\BpmGen\postbuildBpmGen.bat ClientBios  ClientBios
  if %SCRIPT_ERROR% NEQ 0 goto :NoAcm
)

@rem NOTE: postbuildBpmGen.bat will copy the updated .fd with KEYM/BPM as ClientBios.fd (passed as second parameter)
@rem
@rem The invocation to the BpmGen tool should be the last step of the build process, with no further modifications to the BIOS
@rem binary; any change will invalidate the reference hashes computed by BpmGen and required for verified boot flows.
@rem The only exception to this case being: replacing the pre-production ACM binaries with production signed ACM when creating
@rem the Prod_*.rom BIOS binaries.

:NoAcm

if not exist %WORKSPACE_ROM% mkdir %WORKSPACE_ROM%

@echo ---Create ROM and Simics images---
call %WORKSPACE_PLATFORM%\%PLATFORM_FULL_PACKAGE%\Tools\RomImage\SetupRomDirs.bat
@if %errorlevel% NEQ 0 (
  @echo !!! ERROR !!! Create ROM and Simics images !!!
  set SCRIPT_ERROR=1
  goto EndPostBuild
)


:EndPostBuild
@set STARTUP_AC_MODULE_GUID=
@set BIOS_INFO_GUID=
@set STARTUP_AC_PARA=
@popd
endlocal && set SCRIPT_ERROR=%SCRIPT_ERROR%
