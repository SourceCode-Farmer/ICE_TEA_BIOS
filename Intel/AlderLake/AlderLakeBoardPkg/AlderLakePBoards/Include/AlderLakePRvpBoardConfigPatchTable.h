/** @file

 @copyright
  INTEL CONFIDENTIAL
  Copyright 2020 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains a 'Sample Driver' and is licensed as such under the terms
  of your license agreement with Intel or your vendor. This file may be modified
  by the user, subject to the additional terms of the license agreement.

@par Specification Reference:
**/

#ifndef _ALDERLAKE_CONFIG_PATCH_TABLE_H_
#define _ALDERLAKE_CONFIG_PATCH_TABLE_H_

#include <Library/SetupInitLib.h>
#include <SetupVariable.h>
#include <Protocol/RetimerCapsuleUpdate.h>

CONFIG_PATCH_TABLE  mAlderLakePRvpSetupConfigPatchTable[] = {
//{-Type------------------Size-------------------------------------------Offset------------------------------------Data-------------},
  { SIZE_OF_FIELD (SETUP_DATA, PchI2cSensorDevicePort[SERIAL_IO_I2C0]), OFFSET_OF (SETUP_DATA, PchI2cSensorDevicePort[SERIAL_IO_I2C0]),  (SERIAL_IO_I2C_TOUCHPAD|SERIAL_IO_I2C_TOUCHPANEL) },
  { SIZE_OF_FIELD (SETUP_DATA, AuxOriOverrideSupport),                  OFFSET_OF (SETUP_DATA, AuxOriOverrideSupport),                   0 },
  { SIZE_OF_FIELD (SETUP_DATA, DiscreteTbtPlatformConfigurationSupport),OFFSET_OF (SETUP_DATA, DiscreteTbtPlatformConfigurationSupport), 0 },
  { SIZE_OF_FIELD (SETUP_DATA, HebcValueSupport),                       OFFSET_OF (SETUP_DATA, HebcValueSupport),                        1 },
  { SIZE_OF_FIELD (SETUP_DATA, SensorHubType),                          OFFSET_OF (SETUP_DATA, SensorHubType),                           1 },
  { SIZE_OF_FIELD (SETUP_DATA, Rp08D3ColdSupport),                    OFFSET_OF (SETUP_DATA, Rp08D3ColdSupport),                     1 }
};


CONFIG_PATCH_TABLE  mDiscreteThunderboltSetupEnableConfigPatchTable[] = {
//{-Type------------------Size-------------------------------------------Offset------------------------------------Data-------------},
  { SIZE_OF_FIELD (SETUP_DATA, DiscreteTbtPlatformConfigurationSupport), OFFSET_OF (SETUP_DATA, DiscreteTbtPlatformConfigurationSupport), 1 }
};

CONFIG_PATCH_TABLE  mRp08SetupDisableConfigPatchTable[] = {
//{-Type------------------Size-------------------------------------------Offset------------------------------------Data-------------},
  { SIZE_OF_FIELD (SETUP_DATA, Rp08D3ColdSupport),                       OFFSET_OF (SETUP_DATA, Rp08D3ColdSupport),               0 }
};

CONFIG_PATCH_TABLE  mIntegratedThunderboltSetupDisableConfigPatchTable[] = {
//{-Type------------------Size-------------------------------------------Offset------------------------------------Data-------------},
  { SIZE_OF_FIELD (SETUP_DATA, ITbtPcieRootPortSupported[0]),           OFFSET_OF (SETUP_DATA, ITbtPcieRootPortSupported[0]),                0 },
  { SIZE_OF_FIELD (SETUP_DATA, ITbtPcieRootPortSupported[1]),           OFFSET_OF (SETUP_DATA, ITbtPcieRootPortSupported[1]),                0 },
  { SIZE_OF_FIELD (SETUP_DATA, ITbtPcieRootPortSupported[2]),           OFFSET_OF (SETUP_DATA, ITbtPcieRootPortSupported[2]),                0 },
  { SIZE_OF_FIELD (SETUP_DATA, ITbtPcieRootPortSupported[3]),           OFFSET_OF (SETUP_DATA, ITbtPcieRootPortSupported[3]),                0 },
  { SIZE_OF_FIELD (SETUP_DATA, ITbtRootPort[0]),           OFFSET_OF (SETUP_DATA, ITbtRootPort[0]),                0 },
  { SIZE_OF_FIELD (SETUP_DATA, ITbtRootPort[1]),           OFFSET_OF (SETUP_DATA, ITbtRootPort[1]),                0 },
  { SIZE_OF_FIELD (SETUP_DATA, ITbtRootPort[2]),           OFFSET_OF (SETUP_DATA, ITbtRootPort[2]),                0 },
  { SIZE_OF_FIELD (SETUP_DATA, ITbtRootPort[3]),           OFFSET_OF (SETUP_DATA, ITbtRootPort[3]),                0 },
  { SIZE_OF_FIELD (SETUP_DATA, IntegratedTbtSupport),                   OFFSET_OF (SETUP_DATA, IntegratedTbtSupport),                        0 }
};

CONFIG_PATCH_TABLE  mIntegratedThunderboltSaSetupDisableConfigPatchTable[] = {
//{-Type------------------Size-------------------------------------------Offset------------------------------------Data-------------},
  { SIZE_OF_FIELD(SA_SETUP, TcssItbtPcie0En),                      OFFSET_OF(SA_SETUP, TcssItbtPcie0En),                     0 },
  { SIZE_OF_FIELD(SA_SETUP, TcssItbtPcie1En),                      OFFSET_OF(SA_SETUP, TcssItbtPcie1En),                     0 },
  { SIZE_OF_FIELD(SA_SETUP, TcssItbtPcie2En),                      OFFSET_OF(SA_SETUP, TcssItbtPcie2En),                     0 },
  { SIZE_OF_FIELD(SA_SETUP, TcssItbtPcie3En),                      OFFSET_OF(SA_SETUP, TcssItbtPcie3En),                     0 },
  { SIZE_OF_FIELD(SA_SETUP, TcssDma0En),                           OFFSET_OF(SA_SETUP, TcssDma0En),                          0 },
  { SIZE_OF_FIELD(SA_SETUP, TcssDma1En),                           OFFSET_OF(SA_SETUP, TcssDma1En),                          0 }
};

// Enable only "AuxOriOverrideSupport" for board which can support Aux Ori Override after some board rework, not by default configuration.
CONFIG_PATCH_TABLE  mAlderLakePAuxOverrideSupportSetupConfigPatchTable[] = {
//{-Type------------------Size-------------------------------------------Offset------------------------------------Data-------------},
  { SIZE_OF_FIELD (SETUP_DATA, AuxOriOverrideSupport),                  OFFSET_OF (SETUP_DATA, AuxOriOverrideSupport),       1 }
};

// Enable both "AuxOriOverrideSupport" and "AuxOriOverride" for boards which have atleast one Type-C Port as Retimerless port.
// Or having Aux Ori Override support by default.
CONFIG_PATCH_TABLE  mAlderLakePAuxOverrideEnableSetupConfigPatchTable[] = {
//{-Type------------------Size-------------------------------------------Offset------------------------------------Data-------------},
  { SIZE_OF_FIELD (SETUP_DATA, AuxOriOverrideSupport),                  OFFSET_OF (SETUP_DATA, AuxOriOverrideSupport),       1 },
  { SIZE_OF_FIELD (SETUP_DATA, AuxOriOverride),                         OFFSET_OF (SETUP_DATA, AuxOriOverride),              1 }
};

//
// Disable iTBT PCIE RP and corresponding DMA controller if Type-C PORT is not having Retimer. So No PCIE Support.
//

// LP5-T4 board, TCP2 (count starts from 0), this port is USB/DP only port.
CONFIG_PATCH_TABLE  mAlderLakePLp5T4iTBTPcieRpDmaSaSetupDisableConfigPatchTable[] = {
//{-Type------------------Size-------------------------------------------Offset------------------------------------Data-------------},
  { SIZE_OF_FIELD(SA_SETUP, TcssItbtPcie2En),                      OFFSET_OF(SA_SETUP, TcssItbtPcie2En),                     0 }
};

// LP5-T3 board, TCP1,TCP2 and TCP3 are USB/DP only ports. So here also for all of these 3 ports PCIe RP should be disabled.
// DMA1 also should be disabled for T3 board as both ports on controller1 are Non-TBT.
CONFIG_PATCH_TABLE  mAlderLakePLp5T3iTBTPcieRpDmaSaSetupDisableConfigPatchTable[] = {
//{-Type------------------Size-------------------------------------------Offset------------------------------------Data-------------},
  { SIZE_OF_FIELD(SA_SETUP, TcssItbtPcie1En),                      OFFSET_OF(SA_SETUP, TcssItbtPcie1En),                     0 },
  { SIZE_OF_FIELD(SA_SETUP, TcssItbtPcie2En),                      OFFSET_OF(SA_SETUP, TcssItbtPcie2En),                     0 },
  { SIZE_OF_FIELD(SA_SETUP, TcssItbtPcie3En),                      OFFSET_OF(SA_SETUP, TcssItbtPcie3En),                     0 },
  { SIZE_OF_FIELD(SA_SETUP, TcssDma1En),                           OFFSET_OF(SA_SETUP, TcssDma1En),                          0 }
};

// DDR4, TCP3 is Fixed DP only config which doesn't require PCIe RP.
CONFIG_PATCH_TABLE  mAlderLakePDdr4iTBTPcieRpDmaSaSetupDisableConfigPatchTable[] = {
//{-Type------------------Size-------------------------------------------Offset------------------------------------Data-------------},
  { SIZE_OF_FIELD(SA_SETUP, TcssItbtPcie3En),                      OFFSET_OF(SA_SETUP, TcssItbtPcie3En),                     0x0 },
};

CONFIG_PATCH_TABLE  mAlderLakePTcssUcmDisableSetupConfigPatchTable[] = {
//{-Type------------------Size-------------------------------------------Offset------------------------------------Data-------------},
  { SIZE_OF_FIELD (SETUP_DATA, TcssUcmDevice),                     OFFSET_OF (SETUP_DATA, TcssUcmDevice),                    0 }
};

CONFIG_PATCH_TABLE  mAlderLakePDdr4Ddr5SetupConfigPatchTable[] = {
//{-Type------------------Size-------------------------------------------Offset------------------------------------Data-------------},
  { SIZE_OF_FIELD (SETUP_DATA, CvfSupport),                             OFFSET_OF (SETUP_DATA, CvfSupport),                  0 }
};

CONFIG_PATCH_TABLE  mAlderLakePRvpPchSetupConfigPatchTable[] = {
//{-Type------------------Size-------------------------------------------Offset------------------------------------Data-------------},
  { SIZE_OF_FIELD(PCH_SETUP, PmcLpmS0i2p1En),                     OFFSET_OF(PCH_SETUP,      PmcLpmS0i2p1En),                0 },
  { SIZE_OF_FIELD(PCH_SETUP, PchSerialIoSpi[SERIAL_IO_SPI1]),     OFFSET_OF(PCH_SETUP, PchSerialIoSpi[SERIAL_IO_SPI1]),     1 },
  { SIZE_OF_FIELD(PCH_SETUP, PchUartHwFlowCtrl[SERIAL_IO_UART0]), OFFSET_OF(PCH_SETUP, PchUartHwFlowCtrl[SERIAL_IO_UART0]), 0 },
  { SIZE_OF_FIELD(PCH_SETUP, PchSerialIoI2c[SERIAL_IO_I2C0]),     OFFSET_OF(PCH_SETUP, PchSerialIoI2c[SERIAL_IO_I2C0]),     1 },
  { SIZE_OF_FIELD(PCH_SETUP, PchSerialIoI2c[SERIAL_IO_I2C1]),     OFFSET_OF(PCH_SETUP, PchSerialIoI2c[SERIAL_IO_I2C1]),     1 },
  { SIZE_OF_FIELD(PCH_SETUP, PchSerialIoI2c[SERIAL_IO_I2C2]),     OFFSET_OF(PCH_SETUP, PchSerialIoI2c[SERIAL_IO_I2C2]),     0 },
  { SIZE_OF_FIELD(PCH_SETUP, PchSerialIoI2c[SERIAL_IO_I2C3]),     OFFSET_OF(PCH_SETUP, PchSerialIoI2c[SERIAL_IO_I2C3]),     0 },
  { SIZE_OF_FIELD(PCH_SETUP, PchSerialIoI2c[SERIAL_IO_I2C4]),     OFFSET_OF(PCH_SETUP, PchSerialIoI2c[SERIAL_IO_I2C4]),     0 },
  { SIZE_OF_FIELD(PCH_SETUP, PchSerialIoI2c[SERIAL_IO_I2C5]),     OFFSET_OF(PCH_SETUP, PchSerialIoI2c[SERIAL_IO_I2C5]),     1 },
// PchIshGpEnable[GP_INDEX 7] Needs to be enabled to put GPPC_B_15_TIME_SYNC_0_ISH_GP_7 PadMode to Function 5.
  { SIZE_OF_FIELD(PCH_SETUP, PchIshGpEnable[7]),                  OFFSET_OF(PCH_SETUP, PchIshGpEnable[7]),                  1 },
  { SIZE_OF_FIELD(PCH_SETUP, CnviBtAudioOffload),                 OFFSET_OF(PCH_SETUP, CnviBtAudioOffload),                 1 }
};

CONFIG_PATCH_TABLE  mAlderLakePLp5GcsPchSetupConfigPatchTable[] = {
//{-Type------------------Size-------------------------------------------Offset------------------------------------Data-------------},
  { SIZE_OF_FIELD(PCH_SETUP, PchLan),                             OFFSET_OF(PCH_SETUP, PchLan),                             0 },
  { SIZE_OF_FIELD(PCH_SETUP, ThcPort0Assignment),                 OFFSET_OF(PCH_SETUP, ThcPort0Assignment),                 1 },
  { SIZE_OF_FIELD(PCH_SETUP, PchHdAudioHdaLinkEnable),            OFFSET_OF(PCH_SETUP, PchHdAudioHdaLinkEnable),            0 },
  { SIZE_OF_FIELD(PCH_SETUP, PchHdAudioSndwLinkEnable[0]),        OFFSET_OF(PCH_SETUP, PchHdAudioSndwLinkEnable[0]),        1 },
  { SIZE_OF_FIELD(PCH_SETUP, PchHdAudioSndwLinkEnable[1]),        OFFSET_OF(PCH_SETUP, PchHdAudioSndwLinkEnable[1]),        1 },
  { SIZE_OF_FIELD(PCH_SETUP, PchHdAudioSndwLinkEnable[2]),        OFFSET_OF(PCH_SETUP, PchHdAudioSndwLinkEnable[2]),        1 }
};

CONFIG_PATCH_TABLE  mAlderLakePLp5GcsSetupConfigPatchTable[] = {
//{-Type------------------Size-------------------------------------------Offset------------------------------------Data-------------},
  { SIZE_OF_FIELD (SETUP_DATA, Rtd3Support),                              OFFSET_OF (SETUP_DATA, Rtd3Support),                              0 },
  { SIZE_OF_FIELD (SETUP_DATA, PowermeterDeviceEnable),                   OFFSET_OF (SETUP_DATA, PowermeterDeviceEnable),                   1 },
  { SIZE_OF_FIELD (SETUP_DATA, CvfSupport),                               OFFSET_OF (SETUP_DATA, CvfSupport),                               0 },
  { SIZE_OF_FIELD (SETUP_DATA, PchI2cTouchPadType),                       OFFSET_OF (SETUP_DATA, PchI2cTouchPadType),                       7 },
  { SIZE_OF_FIELD (SETUP_DATA, MipiCam_ControlLogic0),                    OFFSET_OF (SETUP_DATA, MipiCam_ControlLogic0),                    1 },
  { SIZE_OF_FIELD (SETUP_DATA, MipiCam_ControlLogic1),                    OFFSET_OF (SETUP_DATA, MipiCam_ControlLogic1),                    0 },
  { SIZE_OF_FIELD (SETUP_DATA, MipiCam_ControlLogic2),                    OFFSET_OF (SETUP_DATA, MipiCam_ControlLogic2),                    0 },
  { SIZE_OF_FIELD (SETUP_DATA, MipiCam_ControlLogic3),                    OFFSET_OF (SETUP_DATA, MipiCam_ControlLogic3),                    0 },
  { SIZE_OF_FIELD (SETUP_DATA, MipiCam_Link0),                            OFFSET_OF (SETUP_DATA, MipiCam_Link0),                            1 },
  { SIZE_OF_FIELD (SETUP_DATA, MipiCam_Link1),                            OFFSET_OF (SETUP_DATA, MipiCam_Link1),                            0 },
  { SIZE_OF_FIELD (SETUP_DATA, MipiCam_Link1_FlashDriverSelection),       OFFSET_OF (SETUP_DATA, MipiCam_Link1_FlashDriverSelection),       0 },
  { SIZE_OF_FIELD (SETUP_DATA, MipiCam_Link2),                            OFFSET_OF (SETUP_DATA, MipiCam_Link2),                            0 },
  { SIZE_OF_FIELD (SETUP_DATA, MipiCam_Link3),                            OFFSET_OF (SETUP_DATA, MipiCam_Link3),                            0 },
  { SIZE_OF_FIELD (SETUP_DATA, MipiCam_ControlLogic0_Type),               OFFSET_OF (SETUP_DATA, MipiCam_ControlLogic0_Type),               2 },
  { SIZE_OF_FIELD (SETUP_DATA, MipiCam_ControlLogic0_CrdVersion),         OFFSET_OF (SETUP_DATA, MipiCam_ControlLogic0_CrdVersion),      0x50 },
  { SIZE_OF_FIELD (SETUP_DATA, MipiCam_ControlLogic0_InputClock),         OFFSET_OF (SETUP_DATA, MipiCam_ControlLogic0_InputClock),      0x10 },
  { SIZE_OF_FIELD (SETUP_DATA, MipiCam_ControlLogic0_PchClockSource),     OFFSET_OF (SETUP_DATA, MipiCam_ControlLogic0_PchClockSource),     0 },
  { SIZE_OF_FIELD (SETUP_DATA, MipiCam_ControlLogic0_Pld),                OFFSET_OF (SETUP_DATA, MipiCam_ControlLogic0_Pld),             0x29 },
  { SIZE_OF_FIELD (SETUP_DATA, MipiCam_ControlLogic0_I2cChannel),         OFFSET_OF (SETUP_DATA, MipiCam_ControlLogic0_I2cChannel),         1 },
  { SIZE_OF_FIELD (SETUP_DATA, MipiCam_Link0_SensorModel),                OFFSET_OF (SETUP_DATA, MipiCam_Link0_SensorModel),               16 },
  { SIZE_OF_FIELD (SETUP_DATA, MipiCam_Link0_LanesClkDiv),                OFFSET_OF (SETUP_DATA, MipiCam_Link0_LanesClkDiv),             0x00 },
  { SIZE_OF_FIELD (SETUP_DATA, MipiCam_Link0_DriverData_CrdVersion),      OFFSET_OF (SETUP_DATA, MipiCam_Link0_DriverData_CrdVersion),   0x50 },
  { SIZE_OF_FIELD (SETUP_DATA, MipiCam_Link0_DriverData_ControlLogic),    OFFSET_OF (SETUP_DATA, MipiCam_Link0_DriverData_ControlLogic),    0 },
  { SIZE_OF_FIELD (SETUP_DATA, MipiCam_Link0_CameraPhysicalLocation),     OFFSET_OF (SETUP_DATA, MipiCam_Link0_CameraPhysicalLocation),  0x61 },
  { SIZE_OF_FIELD (SETUP_DATA, MipiCam_Link0_DriverData_FlashSupport),    OFFSET_OF (SETUP_DATA, MipiCam_Link0_DriverData_FlashSupport),    3 },
  { SIZE_OF_FIELD (SETUP_DATA, MipiCam_Link0_DriverData_PmicPosition),    OFFSET_OF (SETUP_DATA, MipiCam_Link0_DriverData_PmicPosition),    1 },
  { SIZE_OF_FIELD(SETUP_DATA, MipiCam_Link0_ModuleName[0]),               OFFSET_OF(SETUP_DATA, MipiCam_Link0_ModuleName[0]),            0x35 }, // Set default to 56B6 for ADL-P GCS
  { SIZE_OF_FIELD(SETUP_DATA, MipiCam_Link0_ModuleName[1]),               OFFSET_OF(SETUP_DATA, MipiCam_Link0_ModuleName[1]),            0x36 },
  { SIZE_OF_FIELD(SETUP_DATA, MipiCam_Link0_ModuleName[2]),               OFFSET_OF(SETUP_DATA, MipiCam_Link0_ModuleName[2]),            0x42 },
  { SIZE_OF_FIELD(SETUP_DATA, MipiCam_Link0_ModuleName[3]),               OFFSET_OF(SETUP_DATA, MipiCam_Link0_ModuleName[3]),            0x36 },
  { SIZE_OF_FIELD(SETUP_DATA, MipiCam_Link0_DriverData_LinkUsed),         OFFSET_OF(SETUP_DATA, MipiCam_Link0_DriverData_LinkUsed),         1 },
  { SIZE_OF_FIELD(SETUP_DATA, MipiCam_Link0_DriverData_LaneUsed),         OFFSET_OF(SETUP_DATA, MipiCam_Link0_DriverData_LaneUsed),         1 },
  { SIZE_OF_FIELD(SETUP_DATA, MipiCam_Link0_DriverData_EepromType),       OFFSET_OF(SETUP_DATA, MipiCam_Link0_DriverData_EepromType),    0x00 },
  { SIZE_OF_FIELD(SETUP_DATA, MipiCam_Link0_DriverData_VcmType),          OFFSET_OF(SETUP_DATA, MipiCam_Link0_DriverData_VcmType),       0x00 },
  { SIZE_OF_FIELD(SETUP_DATA, MipiCam_Link0_I2cDevicesEnabled),           OFFSET_OF(SETUP_DATA, MipiCam_Link0_I2cDevicesEnabled),           1 },
  { SIZE_OF_FIELD(SETUP_DATA, MipiCam_Link0_I2cChannel),                  OFFSET_OF(SETUP_DATA, MipiCam_Link0_I2cChannel),                  1 },
  { SIZE_OF_FIELD(SETUP_DATA, MipiCam_Link0_I2cAddress[0]),               OFFSET_OF(SETUP_DATA, MipiCam_Link0_I2cAddress[0]),            0x37 }
};

CONFIG_PATCH_TABLE  mAlderLakePRvpSaSetupConfigPatchTable[] = {
//{-Type------------------Size-------------------------------------------Offset------------------------------------Data-------------},
  { 0, 0, 0 }  // End of Table
};

CONFIG_PATCH_TABLE  mAlderLakePRvpCpuSetupConfigPatchTable[] = {
//{-Type------------------Size-------------------------------------------Offset------------------------------------Data-------------},
  { SIZE_OF_FIELD(CPU_SETUP, TmeEnable),                           OFFSET_OF(CPU_SETUP, TmeEnable),                          0 },
  { SIZE_OF_FIELD(CPU_SETUP, EnergyEfficientTurbo),                OFFSET_OF(CPU_SETUP, EnergyEfficientTurbo),               1 },
  { SIZE_OF_FIELD(CPU_SETUP, Irms),                                OFFSET_OF(CPU_SETUP, Irms),                               1 }
};

CONFIG_PATCH_TABLE  mAlderLakePPchSteppingPatchTable[] = {
  //{-Type------------------Size-------------------------------------------Offset------------------------------------Data-------------},
  { SIZE_OF_FIELD(PCH_SETUP, PchRetToLowCurModeVolTranTime),     OFFSET_OF(PCH_SETUP, PchRetToLowCurModeVolTranTime),  0x2B },
  { SIZE_OF_FIELD(PCH_SETUP, PchRetToHighCurModeVolTranTime),    OFFSET_OF(PCH_SETUP, PchRetToHighCurModeVolTranTime), 0x36 },
  { 0, 0, 0 }  // End of Table
};

CONFIG_PATCH_STRUCTURE mAlderLakePPchSteppingConfigPatchStruct[] = {
  { L"PchSetup", &gPchSetupVariableGuid, sizeof(PCH_SETUP),  mAlderLakePPchSteppingPatchTable,  SIZE_OF_TABLE(mAlderLakePPchSteppingPatchTable,  CONFIG_PATCH_TABLE) },
};
CONFIG_PATCH_STRUCTURE mAlderLakePRvpConfigPatchStruct[] = {
  { L"Setup",    &gSetupVariableGuid,    sizeof(SETUP_DATA), mAlderLakePRvpSetupConfigPatchTable,     SIZE_OF_TABLE(mAlderLakePRvpSetupConfigPatchTable,     CONFIG_PATCH_TABLE) },
  { L"SaSetup",  &gSaSetupVariableGuid,  sizeof(SA_SETUP),   mAlderLakePRvpSaSetupConfigPatchTable,   SIZE_OF_TABLE(mAlderLakePRvpSaSetupConfigPatchTable,   CONFIG_PATCH_TABLE) },
  { L"CpuSetup", &gCpuSetupVariableGuid, sizeof(CPU_SETUP),  mAlderLakePRvpCpuSetupConfigPatchTable,  SIZE_OF_TABLE(mAlderLakePRvpCpuSetupConfigPatchTable,  CONFIG_PATCH_TABLE) },
  { L"PchSetup", &gPchSetupVariableGuid, sizeof(PCH_SETUP),  mAlderLakePRvpPchSetupConfigPatchTable,  SIZE_OF_TABLE(mAlderLakePRvpPchSetupConfigPatchTable,  CONFIG_PATCH_TABLE) },
  { L"MeSetup",  &gMeSetupVariableGuid,  sizeof(ME_SETUP),   NULL, 0 }
};

CONFIG_PATCH_STRUCTURE mAlderLakePLp4HsioAuxOverrideEnableConfigPatchStruct[] = {
  { L"Setup",    &gSetupVariableGuid,    sizeof(SETUP_DATA),  mAlderLakePAuxOverrideSupportSetupConfigPatchTable,  SIZE_OF_TABLE(mAlderLakePAuxOverrideSupportSetupConfigPatchTable,  CONFIG_PATCH_TABLE) },
};

CONFIG_PATCH_STRUCTURE mAlderLakePLp5T4AuxOverrideEnableConfigPatchStruct[] = {
  { L"Setup",    &gSetupVariableGuid,    sizeof(SETUP_DATA),  mAlderLakePAuxOverrideEnableSetupConfigPatchTable,  SIZE_OF_TABLE(mAlderLakePAuxOverrideEnableSetupConfigPatchTable,  CONFIG_PATCH_TABLE) },
  { L"SaSetup",  &gSaSetupVariableGuid,  sizeof(SA_SETUP),    mAlderLakePLp5T4iTBTPcieRpDmaSaSetupDisableConfigPatchTable, SIZE_OF_TABLE(mAlderLakePLp5T4iTBTPcieRpDmaSaSetupDisableConfigPatchTable, CONFIG_PATCH_TABLE) },
};

CONFIG_PATCH_STRUCTURE mAlderLakePLp5T3AuxOverrideEnableConfigPatchStruct[] = {
  { L"Setup",    &gSetupVariableGuid,    sizeof(SETUP_DATA),  mAlderLakePAuxOverrideEnableSetupConfigPatchTable,  SIZE_OF_TABLE(mAlderLakePAuxOverrideEnableSetupConfigPatchTable,  CONFIG_PATCH_TABLE) },
  { L"SaSetup",  &gSaSetupVariableGuid,  sizeof(SA_SETUP),    mAlderLakePLp5T3iTBTPcieRpDmaSaSetupDisableConfigPatchTable, SIZE_OF_TABLE(mAlderLakePLp5T3iTBTPcieRpDmaSaSetupDisableConfigPatchTable, CONFIG_PATCH_TABLE) },
};

CONFIG_PATCH_STRUCTURE mAlderLakePDdr4AuxOverrideEnableConfigPatchStruct[] = {
  { L"Setup",    &gSetupVariableGuid,    sizeof(SETUP_DATA),  mAlderLakePAuxOverrideSupportSetupConfigPatchTable,  SIZE_OF_TABLE(mAlderLakePAuxOverrideSupportSetupConfigPatchTable,  CONFIG_PATCH_TABLE) },
  { L"SaSetup",  &gSaSetupVariableGuid,  sizeof(SA_SETUP),    mAlderLakePDdr4iTBTPcieRpDmaSaSetupDisableConfigPatchTable, SIZE_OF_TABLE(mAlderLakePDdr4iTBTPcieRpDmaSaSetupDisableConfigPatchTable, CONFIG_PATCH_TABLE) },
};

CONFIG_PATCH_STRUCTURE mAlderLakePTcssUcmDisableConfigPatchStruct[] = {
  { L"Setup",    &gSetupVariableGuid,    sizeof(SETUP_DATA),  mAlderLakePTcssUcmDisableSetupConfigPatchTable,  SIZE_OF_TABLE(mAlderLakePTcssUcmDisableSetupConfigPatchTable,  CONFIG_PATCH_TABLE) },
};

CONFIG_PATCH_STRUCTURE mAlderLakePDdr4Ddr5ConfigPatchStruct[] = {
  { L"Setup",    &gSetupVariableGuid,    sizeof(SETUP_DATA),  mAlderLakePDdr4Ddr5SetupConfigPatchTable,   SIZE_OF_TABLE(mAlderLakePDdr4Ddr5SetupConfigPatchTable,   CONFIG_PATCH_TABLE) }
};

CONFIG_PATCH_STRUCTURE mDiscreteThunderboltSetupEnableConfigPatchStruct[] = {
  { L"Setup",    &gSetupVariableGuid,    sizeof(SETUP_DATA),  mDiscreteThunderboltSetupEnableConfigPatchTable,   SIZE_OF_TABLE(mDiscreteThunderboltSetupEnableConfigPatchTable,   CONFIG_PATCH_TABLE) }
};

CONFIG_PATCH_STRUCTURE mRp08SetupDisableConfigPatchStruct[] = {
  { L"Setup",    &gSetupVariableGuid,    sizeof(SETUP_DATA),  mRp08SetupDisableConfigPatchTable,                  SIZE_OF_TABLE(mRp08SetupDisableConfigPatchTable,   CONFIG_PATCH_TABLE) }
};

CONFIG_PATCH_STRUCTURE mIntegratedThunderboltSetupDisableConfigPatchStruct[] = {
  { L"Setup",    &gSetupVariableGuid,    sizeof(SETUP_DATA),  mIntegratedThunderboltSetupDisableConfigPatchTable,   SIZE_OF_TABLE(mIntegratedThunderboltSetupDisableConfigPatchTable,   CONFIG_PATCH_TABLE) },
  { L"SaSetup",  &gSaSetupVariableGuid,  sizeof(SA_SETUP),    mIntegratedThunderboltSaSetupDisableConfigPatchTable, SIZE_OF_TABLE(mIntegratedThunderboltSaSetupDisableConfigPatchTable, CONFIG_PATCH_TABLE) },
};

CONFIG_PATCH_STRUCTURE mAlderLakePLp5GcsConfigPatchStruct[] = {
  { L"PchSetup", &gPchSetupVariableGuid, sizeof(PCH_SETUP),   mAlderLakePLp5GcsPchSetupConfigPatchTable,   SIZE_OF_TABLE(mAlderLakePLp5GcsPchSetupConfigPatchTable,  CONFIG_PATCH_TABLE) },
  { L"Setup",    &gSetupVariableGuid,    sizeof(SETUP_DATA),  mAlderLakePLp5GcsSetupConfigPatchTable,      SIZE_OF_TABLE(mAlderLakePLp5GcsSetupConfigPatchTable,     CONFIG_PATCH_TABLE) },
};

CONFIG_PATCH_TABLE  mAlderlakePSimicsSaSetupConfigPatchTable[] = {
//{---------------Type------Size-----------------------------------Offset----------------------------------------------------Data},
  { SIZE_OF_FIELD(SA_SETUP, VmdEnable),                            OFFSET_OF(SA_SETUP, VmdEnable),                           0x0 },
  { SIZE_OF_FIELD(SA_SETUP, VmdGlobalMapping),                     OFFSET_OF(SA_SETUP, VmdGlobalMapping),                    0x0 }
};

CONFIG_PATCH_STRUCTURE mAlderlakePSimicsConfigPatchStruct[] = {
  { L"SaSetup",  &gSaSetupVariableGuid,  sizeof(SA_SETUP),   mAlderlakePSimicsSaSetupConfigPatchTable,  SIZE_OF_TABLE(mAlderlakePSimicsSaSetupConfigPatchTable,  CONFIG_PATCH_TABLE) }
};


CONFIG_PATCH_TABLE  mAlderLakePAepPchSetupConfigPatchTable[] = {
  { SIZE_OF_FIELD(PCH_SETUP, PchHdAudioHdaLinkEnable),            OFFSET_OF(PCH_SETUP, PchHdAudioHdaLinkEnable),            0 },
  { SIZE_OF_FIELD(PCH_SETUP, PchHdAudioSndwLinkEnable[0]),        OFFSET_OF(PCH_SETUP, PchHdAudioSndwLinkEnable[0]),        1 },
  { SIZE_OF_FIELD(PCH_SETUP, PchHdAudioSndwLinkEnable[1]),        OFFSET_OF(PCH_SETUP, PchHdAudioSndwLinkEnable[1]),        1 },
  { SIZE_OF_FIELD(PCH_SETUP, PchHdAudioSndwLinkEnable[2]),        OFFSET_OF(PCH_SETUP, PchHdAudioSndwLinkEnable[2]),        1 }
};

CONFIG_PATCH_STRUCTURE mAlderLakePAepConfigPatchStruct[] = {
  { L"PchSetup", &gPchSetupVariableGuid, sizeof(PCH_SETUP),  mAlderLakePAepPchSetupConfigPatchTable,  SIZE_OF_TABLE(mAlderLakePAepPchSetupConfigPatchTable,  CONFIG_PATCH_TABLE) }
};

CONFIG_PATCH_TABLE  mAlderLakePDgAepSaSetupConfigPatchTable[] = {
//{-Type------------------Size-------------------------------------------Offset------------------------------------Data-------------},
  { SIZE_OF_FIELD(SA_SETUP, PrimaryDisplay),                       OFFSET_OF(SA_SETUP, PrimaryDisplay),                     4 }
};

CONFIG_PATCH_STRUCTURE mAlderLakePDgAepConfigPatchStruct[] = {
  { L"SaSetup",  &gSaSetupVariableGuid,  sizeof(SA_SETUP),   mAlderLakePDgAepSaSetupConfigPatchTable,   SIZE_OF_TABLE(mAlderLakePDgAepSaSetupConfigPatchTable,   CONFIG_PATCH_TABLE) },
};

#endif // _ALDERLAKE_CONFIG_PATCH_TABLE_H_
