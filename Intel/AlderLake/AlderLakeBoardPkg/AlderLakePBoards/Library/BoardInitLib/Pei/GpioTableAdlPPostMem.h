/** @file
  GPIO definition table for AlderLake P

@copyright
INTEL CONFIDENTIAL
  Copyright (c) 2020 - 2021 Intel Corporation. All rights reserved
  This software and associated documentation (if any) is furnished
  under a license and may only be used or copied in accordance
  with the terms of the license. Except as permitted by such
  license, no part of this software or documentation may be
  reproduced, stored in a retrieval system, or transmitted in any
  form or by any means without the express written consent of
  Intel Corporation.
  This file contains an 'Intel Peripheral Driver' and is
  licensed for Intel CPUs and chipsets under the terms of your
  license agreement with Intel or your vendor. This file may
  be modified by the user, subject to additional terms of the
  license agreement.
**/
#ifndef _ALDERLAKE_P_GPIO_TABLE_H_
#define _ALDERLAKE_P_GPIO_TABLE_H_

///

/// !!! For those GPIO pins are designed as native function, BIOS CAN NOT configure the pins to Native function in GPIO init table!!!

/// BIOS has to leave the native pins programming to Silicon Code(based on BIOS policies setting) or soft strap(set by CSME in FITc).

/// Configuring pins to native function in GPIO table would cause BIOS perform multiple programming the pins and then related function might be abnormal.

///

#include <Pins/GpioPinsVer2Lp.h>
#include <Library/GpioLib.h>
#include <Library/GpioConfig.h>

GLOBAL_REMOVE_IF_UNREFERENCED GPIO_INIT_CONFIG mAdlPTouchPanel1GpioTable[] =
{
  // Touch Panel 1, Same pins shared between THC and I2C based Panel,
  {GPIO_VER2_LP_GPP_F7,  {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutHigh,   GpioIntDis,GpioPlatformReset,  GpioTermNone,  GpioOutputStateUnlock}},  //TCH_PNL_PWR_EN
  {GPIO_VER2_LP_GPP_F17,  {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutHigh,   GpioIntDis,GpioPlatformReset,  GpioOutputStateUnlock}},  //TCH_PNL_RST
  {GPIO_VER2_LP_GPP_F18,  {GpioPadModeGpio, GpioHostOwnGpio, GpioDirInInv,  GpioOutDefault,GpioIntEdge|GpioIntApic,GpioPlatformReset,  GpioTermNone,  GpioPadConfigUnlock  }},  //TCH_PNL_INT_N
};


GLOBAL_REMOVE_IF_UNREFERENCED GPIO_INIT_CONFIG mAdlPCvfGpioTable[] =
{
  // Repurposed GPIO for CVF usage
  {GPIO_VER2_LP_GPP_B23, {GpioPadModeGpio, GpioHostOwnGpio, GpioDirIn,   GpioOutHigh,  GpioIntEdge, GpioPlatformReset,  GpioTermNone, GpioPadConfigUnlock}},  //CVF_HOST_IRQ
  {GPIO_VER2_LP_GPP_R5, {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,  GpioOutLow,   GpioIntDis,  GpioPlatformReset,  GpioTermNone, GpioPadConfigUnlock}},  //CVF_RST_N
  {GPIO_VER2_LP_GPP_B14,  {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,  GpioOutLow,  GpioIntDis,  GpioPlatformReset,  GpioTermNone, GpioPadConfigUnlock}},  //CVF_HOST_WAKE
};

GLOBAL_REMOVE_IF_UNREFERENCED GPIO_INIT_CONFIG mAdlPDg128AepCvfGpioTable[] =
{
  // Repurposed GPIO for CVF usage
  {GPIO_VER2_LP_GPP_A7, {GpioPadModeGpio, GpioHostOwnGpio, GpioDirIn,   GpioOutHigh,  GpioIntEdge, GpioPlatformReset,  GpioTermNone, GpioPadConfigUnlock}},  //CVF_HOST_IRQ
  {GPIO_VER2_LP_GPP_R5, {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,  GpioOutLow,   GpioIntDis,  GpioPlatformReset,  GpioTermNone, GpioPadConfigUnlock}},  //CVF_RST_N
  {GPIO_VER2_LP_GPP_B14,  {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,  GpioOutLow,  GpioIntDis,  GpioPlatformReset,  GpioTermNone, GpioPadConfigUnlock}},  //CVF_HOST_WAKE
};

//
// GPIO for AlderLake P LP4x BEP+ TouchPanel
//
GLOBAL_REMOVE_IF_UNREFERENCED GPIO_INIT_CONFIG mAdlPBepTouchPanelGpioTable[] =
{
  {GPIO_VER2_LP_GPP_E6,  { GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut, GpioOutHigh,    GpioIntDis, GpioPlatformReset, GpioTermNone } },  //THC0_SPI1_RST_N
  {GPIO_VER2_LP_GPP_E17, { GpioPadModeGpio, GpioHostOwnGpio, GpioDirIn,  GpioOutDefault, GpioIntDis, GpioPlatformReset, GpioTermNone } },  //THC0_SPI1_INTB

  { 0x0 }
};

GLOBAL_REMOVE_IF_UNREFERENCED GPIO_INIT_CONFIG mAdlPDg02TouchPanel1GpioTable[] =
{
  // Touch Panel 1
  {GPIO_VER2_LP_GPP_E6,  {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,   GpioOutHigh,    GpioIntDis,              GpioPlatformReset,  GpioTermNone,  GpioPadConfigUnlock}},  //THC0_RST
  {GPIO_VER2_LP_GPP_E17, {GpioPadModeGpio, GpioHostOwnGpio, GpioDirInInv, GpioOutDefault, GpioIntEdge|GpioIntApic, GpioPlatformReset,  GpioTermNone,  GpioPadConfigUnlock}},  //THC0_INT
};

GLOBAL_REMOVE_IF_UNREFERENCED GPIO_INIT_CONFIG mAdlPTypeCAuxOverrideEnableGpioTable[] =
{
  // TypeC BIAS : Not used by default in RVP Platfrom Code. Used for Type-C Aux Override enable case.
  {GPIO_VER2_LP_GPP_E22, {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutLow,    GpioIntDis, GpioPlatformReset,  GpioTermNone}},  //DISP_AUX_P_BIAS_GPIO
  {GPIO_VER2_LP_GPP_E23, {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutHigh,   GpioIntDis, GpioPlatformReset,  GpioTermNone}},  //DISP_AUX_N_BIAS_GPIO
};

GLOBAL_REMOVE_IF_UNREFERENCED GPIO_INIT_CONFIG mAdlPDnxEnableGpioTable[] =
{
  // DNX
  {GPIO_VER2_LP_GPP_E23, {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutHigh,   GpioIntDis,GpioPlatformReset,  GpioTermNone}},  //DISP_AUX_N_BIAS_GPIO
};

GLOBAL_REMOVE_IF_UNREFERENCED GPIO_INIT_CONFIG mAdlPLidSwitchTable[] =
{
 //LID SWITCH WAKE

 {GPIO_VER2_LP_GPD2,    {GpioPadModeGpio,  GpioHostOwnAcpi,  GpioDirInInv,  GpioOutDefault,  GpioIntLevel|GpioIntSci,   GpioHostDeepReset,  GpioTermNone,  GpioPadConfigUnlock  }}, //SMC_WAKE_SCI_N
};
#endif // _ALDERLAKE_P_GPIO_TABLE_H_
