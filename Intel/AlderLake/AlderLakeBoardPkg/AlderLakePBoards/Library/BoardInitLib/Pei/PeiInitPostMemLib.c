/** @file

;******************************************************************************
;* Copyright (c) 2020 - 2021, Insyde Software Corporation. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/
/** @file

@copyright
  INTEL CONFIDENTIAL
  Copyright 2020 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains a 'Sample Driver' and is licensed as such under the terms
  of your license agreement with Intel or your vendor. This file may be modified
  by the user, subject to the additional terms of the license agreement.

@par Specification Reference:
**/

#include <PiPei.h>
#include <Library/DebugLib.h>
#include <Library/PcdLib.h>

#include <Register/PttPtpRegs.h>
#include <Library/SiliconInitLib.h>
#include <Library/PreSiliconEnvDetectLib.h>
#include <Library/BoardConfigLib.h>
#include "GpioTableAdlPPostMem.h"
#include <Library/PeiServicesLib.h>
#include <Library/GpioLib.h>
#include <Library/IoLib.h>
#include <Library/PeiHdaVerbTables.h>
#include <PlatformBoardId.h>
#include <PlatformBoardConfig.h>
#include <UsbTypeC.h>
//[-start-201104-IB17510119-add]//
#include <Library/GpioCfgLib.h>
#include <Library/GpioNativeLib.h>
#include <Library/MemoryAllocationLib.h>
#include <Library/BaseMemoryLib.h>
#include <Library/PeiOemSvcKernelLib.h>
//[-end-201104-IB17510119-add]//
//[-start-191001-16990100-add]//
#include <Library/PeiOemSvcChipsetLib.h>
//[-end-191001-16990100-add]//
#include <Ppi/ReadOnlyVariable2.h>
#include <SetupVariable.h>
#include <Library/PchInfoLib.h>
#include <Library/GbeLib.h>
#include <Library/EcMiscLib.h>
extern EFI_GUID gSetupVariableGuid;
extern EFI_GUID gPchSetupVariableGuid;
//[-start-220107-TAMT000041-add]//
#if defined(S77013_SUPPORT)
#include <Library/LfcEcLib.h>
#include <Library/PcdLib.h>


static GLOBAL_REMOVE_IF_UNREFERENCED GPIO_INIT_CONFIG S77013_LED_early_init[] =
{
  {GPIO_VER2_LP_GPP_H20, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirOut,     GpioOutLow,    GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//CPU_FN_LED_N
  {GPIO_VER2_LP_GPP_H22, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirOut,     GpioOutLow,    GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//CPU_CAPS_LED_N
};
#endif
//[-end-220107-TAMT000041-add]//
/**
  Alderlake P boards configuration init function for PEI post memory phase.

  @retval EFI_SUCCESS             The function completed successfully.
**/
EFI_STATUS
EFIAPI
AdlPInit (
  VOID
  )
{
  UINT16            GpioCount;
  UINTN             Size;
  EFI_STATUS        Status;
  GPIO_INIT_CONFIG  *GpioTable;
  //
  // GPIO Table Init
  //
  Status = EFI_SUCCESS;
  GpioCount = 0;
  Size = 0;
  GpioTable = NULL;
  //
  // GPIO Table Init
  //
  //
  // GPIO Table Init, Update PostMem GPIO table to PcdBoardGpioTable
  //
  GpioTable = (GPIO_INIT_CONFIG *)PcdGetPtr(VpdPcdBoardGpioTable);

  GetGpioTableSize (GpioTable, &GpioCount);
  //
  // Increase GpioCount for the zero terminator.
  //
  GpioCount ++;
  Size = (UINTN) (GpioCount * sizeof (GPIO_INIT_CONFIG));
  Status = PcdSetPtrS (PcdBoardGpioTable, &Size, GpioTable);
  ASSERT_EFI_ERROR (Status);

  PcdSet8S (PcdSataPortsEnable0, 0x1);

  return Status;
}

/**
  Board I2C pads termination configuration init function for PEI pre-memory phase.
**/
VOID
AdlPSerialIoI2cPadsTerminationInit (
  VOID
  )
{
}

/**
  Touch panel GPIO init function for PEI post memory phase.
**/
VOID
AdlPTouchPanelGpioInit (
  VOID
  )
{
  UINT16  BoardId;
  BoardId = PcdGet16 (PcdBoardId);

  switch (BoardId) {
    case BoardIdAdlPLp4Bep:
    case BoardIdAdlPLp5Aep:
    case BoardIdAdlPDdr5Dg384Aep:
    case BoardIdAdlPLp5Dg128Aep:
      PcdSet32S (PcdBoardGpioTableTouchPanel1, (UINTN)mAdlPDg02TouchPanel1GpioTable);
      PcdSet16S (PcdBoardGpioTableTouchPanel1Size, sizeof (mAdlPDg02TouchPanel1GpioTable) / sizeof (GPIO_INIT_CONFIG));
      break;
    case BoardIdAdlPLp4Rvp:
    case BoardIdAdlPLp5Rvp:
    case BoardIdAdlPT3Lp5Rvp:
    case BoardIdAdlPDdr5Rvp:
    case BoardIdAdlPDdr4Rvp:
    case BoardIdAdlPDdr5MRRvp:
      PcdSet32S (PcdBoardGpioTableTouchPanel1, (UINTN) mAdlPTouchPanel1GpioTable);
      PcdSet16S (PcdBoardGpioTableTouchPanel1Size, sizeof (mAdlPTouchPanel1GpioTable) / sizeof (GPIO_INIT_CONFIG));
      break;
    case BoardIdAdlPLp5Gcs:
      PcdSet32S (PcdBoardGpioTableTouchPanel1, (UINTN) mAdlPTouchPanel1GpioTable);
      PcdSet16S (PcdBoardGpioTableTouchPanel1Size, sizeof (mAdlPTouchPanel1GpioTable) / sizeof (GPIO_INIT_CONFIG));
      break;
  }
}


/**
  CVF GPIO init function for PEI post memory phase.
**/
VOID
AdlPCvfGpioInit (
  VOID
  )
{
  UINT16  BoardId;
  BoardId = PcdGet16 (PcdBoardId);

  switch (BoardId) {
    case BoardIdAdlPLp4Rvp:
    case BoardIdAdlPLp5Rvp:
    case BoardIdAdlPT3Lp5Rvp:
    case BoardIdAdlPDdr5Rvp:
    case BoardIdAdlPDdr4Rvp:
    case BoardIdAdlPDdr5MRRvp:
    case BoardIdAdlPDdr5Dg384Aep:
    case BoardIdAdlPLp5Aep:
    case BoardIdAdlPLp5Gcs:
    case BoardIdAdlPLp5MbAep:
      PcdSet32S (PcdBoardGpioTableCvf,  (UINTN) mAdlPCvfGpioTable);
      PcdSet16S (PcdBoardGpioTableCvfSize,  sizeof(mAdlPCvfGpioTable)/ sizeof(GPIO_INIT_CONFIG));
      break;
    case BoardIdAdlPLp5Dg128Aep:
      PcdSet32S (PcdBoardGpioTableCvf,  (UINTN)mAdlPDg128AepCvfGpioTable);
      PcdSet16S (PcdBoardGpioTableCvfSize,  sizeof(mAdlPDg128AepCvfGpioTable)/ sizeof(GPIO_INIT_CONFIG));
      break;
    default:
      //
      // Do nothing.
      //
      break;
  }
}

/**
  Tsn device GPIO init function for PEI post memory phase.
**/
VOID
AdlPTsnDeviceGpioInit (
  VOID
  )
{
}


/**
  HDA VerbTable init function for PEI post memory phase.
**/
VOID
AdlPHdaVerbTableInit (
  VOID
  )
{
  PCH_SETUP                         PchSetup;
  EFI_STATUS                        Status;
  EFI_PEI_READ_ONLY_VARIABLE2_PPI   *VariableServices;
  UINTN                             VariableSize;

  VariableServices = NULL;
  Status = PeiServicesLocatePpi (
              &gEfiPeiReadOnlyVariable2PpiGuid,
              0,
              NULL,
              (VOID **) &VariableServices
              );
  if (Status == EFI_SUCCESS) {
    VariableSize = sizeof (PCH_SETUP);
    Status = VariableServices->GetVariable (VariableServices, L"PchSetup", &gPchSetupVariableGuid, NULL, &VariableSize, &PchSetup);

    if (Status == EFI_SUCCESS) {
      if (PchSetup.PchHdaAlc245DmicConfiguration == 1) {
        PcdSet32S (PcdHdaVerbTableDatabase, (UINT32) &HdaVerbTableDbAdlDmic);
      } else {
        PcdSet32S (PcdHdaVerbTableDatabase, (UINT32) &HdaVerbTableDbAdlNoDmic);
      }
    } else {
      ASSERT_EFI_ERROR (Status);
    }
  }
}

/**
  Return Type-C Aux Override Value of Setup variable
**/
UINT8
AdlPAuxOverrideSetup (
  VOID
  )
{
#if FixedPcdGetBool (PcdSetupEnable) == 1
  EFI_PEI_READ_ONLY_VARIABLE2_PPI   *VariableServices;
  EFI_STATUS                        Status;
  SETUP_DATA                        Setup;
  UINTN                             DataSize;
  Status = PeiServicesLocatePpi (
             &gEfiPeiReadOnlyVariable2PpiGuid,
             0,
             NULL,
             (VOID **) &VariableServices
             );
  if (Status == EFI_SUCCESS) {
    DataSize = sizeof (SETUP_DATA);
    Status = VariableServices->GetVariable (
                                 VariableServices,
                                 L"Setup",
                                 &gSetupVariableGuid,
                                 NULL,
                                 &DataSize,
                                 &Setup
                                 );
    if (Status == EFI_SUCCESS) {
      DEBUG ((DEBUG_INFO, "AuxOriOverride= %d\n", Setup.AuxOriOverride));
      return Setup.AuxOriOverride;
    }
  }
#endif
  return 0;
}


/**
  Configures GPIO

  @param[in]  GpioTable       Point to Platform Gpio table
  @param[in]  GpioTableCount  Number of Gpio table entries

**/
VOID
ConfigureGpio (
  IN GPIO_INIT_CONFIG                 *GpioDefinition,
  IN UINT16                           GpioTableCount
  )
{
  DEBUG ((DEBUG_INFO, "ConfigureGpio() Start\n"));

  GpioConfigurePads (GpioTableCount, GpioDefinition);

  DEBUG ((DEBUG_INFO, "ConfigureGpio() End\n"));
}

/**
  Misc. init function for PEI post memory phase.
**/
VOID
AdlPBoardMiscInit (
  VOID
  )
{
  UINT16    BoardId;
  BoardId = PcdGet16 (PcdBoardId);
  EFI_PEI_READ_ONLY_VARIABLE2_PPI   *VariableServices;
  EFI_STATUS                        Status;
  SETUP_DATA                        Setup;
  UINTN                             DataSize;
  PCH_SETUP                         PchSetup;
  GPIO_CONFIG                       GpioConfig;
  GPIO_PAD                          GpioPad;

  Status = PeiServicesLocatePpi (
             &gEfiPeiReadOnlyVariable2PpiGuid,
             0,
             NULL,
             (VOID **) &VariableServices
             );
  if (Status == EFI_SUCCESS) {
    DataSize = sizeof (PCH_SETUP);
    Status = VariableServices->GetVariable (
                                 VariableServices,
                                 L"PchSetup",
                                 &gPchSetupVariableGuid,
                                 NULL,
                                 &DataSize,
                                 &PchSetup
                                 );
    if (EFI_ERROR(Status)) {
      DEBUG ((DEBUG_ERROR, "AdlMboardMiscInit: pch setup failed\n"));
      ASSERT_EFI_ERROR (Status);
    }
    DataSize = sizeof (SETUP_DATA);
    Status = VariableServices->GetVariable (
                                 VariableServices,
                                 L"Setup",
                                 &gSetupVariableGuid,
                                 NULL,
                                 &DataSize,
                                 &Setup
                                 );
    if (Status == EFI_SUCCESS) {
      ASSERT_EFI_ERROR (Status);
    }
  }

  PcdSetBoolS (PcdSataLedEnable, FALSE);
  PcdSetBoolS (PcdVrAlertEnable, FALSE);
  PcdSetBoolS (PcdPchThermalHotEnable, FALSE);
  PcdSetBoolS (PcdMemoryThermalSensorGpioCPmsyncEnable, FALSE);
  PcdSetBoolS (PcdMemoryThermalSensorGpioDPmsyncEnable, FALSE);
  PcdSetBoolS (PcdDashGEnable, TRUE);

  switch (BoardId) {
    case BoardIdAdlPLp4Bep:
    case BoardIdAdlPLp5Dg128Aep:
    case BoardIdAdlPLp5Aep:
    case BoardIdAdlPMMAep:
    case BoardIdAdlPLp5MbAep:
      break;
    case BoardIdAdlPLp5Gcs:
      PcdSetBoolS (PcdVrAlertEnable, TRUE);
      break;
    case BoardIdAdlPLp4Rvp:
    case BoardIdAdlPLp5Rvp:
    case BoardIdAdlPT3Lp5Rvp:
//[-start-210609-IB05660171-modify]//
      PcdSet8S (PcdPcieSlot1RootPort, 7);
//[-end-210609-IB05660171-modify]//
      PcdSet32S (PcdPcieSlot1PwrEnableGpioNo, GPIO_VER2_LP_GPP_H17);
      PcdSet32S (PcdPcieSlot1HoldRstGpioNo, GPIO_VER2_LP_GPP_F10);
      PcdSetBoolS (PcdPcieSlot1PwrEnableGpioPolarity, PIN_GPIO_ACTIVE_LOW);
      PcdSetBoolS (PcdPcieSlot1HoldRstGpioPolarity, PIN_GPIO_ACTIVE_LOW);
      break;
    case BoardIdAdlPDdr5Dg384Aep:
      //
      // For PCIe Slot1 - SD card controller x1 Connector
      //
      PcdSet8S (PcdPcieSlot1RootPort, 7);
      PcdSet32S (PcdPcieSlot1WakeGpioPin, GPIO_VER2_LP_GPP_A8);
      PcdSet32S (PcdPcieSlot1HoldRstGpioNo, GPIO_VER2_LP_GPP_F10);
      PcdSetBoolS (PcdPcieSlot1HoldRstGpioPolarity, PIN_GPIO_ACTIVE_LOW);
      break;
    case BoardIdAdlPDdr5Rvp:
    case BoardIdAdlPDdr4Rvp:
//[-start-210428-IB05660158-modify]//
      PcdSet8S (PcdPcieSlot1RootPort, 7);
//[-end-210428-IB05660158-modify]//
      PcdSet32S (PcdPcieSlot1PwrEnableGpioNo, GPIO_VER2_LP_GPP_A22);
      PcdSet32S (PcdPcieSlot1HoldRstGpioNo, GPIO_VER2_LP_GPP_F10);
      PcdSetBoolS (PcdPcieSlot1PwrEnableGpioPolarity, PIN_GPIO_ACTIVE_LOW);
      PcdSetBoolS (PcdPcieSlot1HoldRstGpioPolarity, PIN_GPIO_ACTIVE_LOW);
      break;
    case BoardIdAdlPDdr5MRRvp:
      //
      // For PCIe Slot1 - x1 Connector
      //
      PcdSet8S (PcdPcieSlot1RootPort, 3);
      PcdSet32S (PcdPcieSlot1PwrEnableGpioNo, GPIO_VER2_LP_GPP_A22);
      PcdSet32S (PcdPcieSlot1HoldRstGpioNo, GPIO_VER2_LP_GPP_F10);
      PcdSetBoolS (PcdPcieSlot1PwrEnableGpioPolarity, PIN_GPIO_ACTIVE_LOW);
      PcdSetBoolS (PcdPcieSlot1HoldRstGpioPolarity, PIN_GPIO_ACTIVE_LOW);

      // Maple Ridge 1 (Back Panel)
      PcdSet32S (PcdPchPCIeSlot2PwrEnableGpioNo, GPIO_VER2_LP_GPP_A12);           // Maple Ridge BP RTD3 CIO Power Enable Gpio pin
      PcdSetBoolS (PcdPchPCIeSlot2PwrEnableGpioPolarity, PIN_GPIO_ACTIVE_HIGH);   // Maple Ridge BP RTD3 CIO Power Enable Gpio pin polarity
      PcdSet32S (PcdPchPCIeSlot2RstGpioNo, GPIO_VER2_LP_GPP_D9);                  // Maple Ridge BP Reset Gpio pin
      PcdSetBoolS (PcdPchPCIeSlot2RstGpioPolarity, PIN_GPIO_ACTIVE_LOW);          // Maple Ridge BP Reset Gpio pin polarity
      PcdSet32S (PcdPcieSlot2WakeGpioPin, GPIO_VER2_LP_GPP_H15);                  // Maple Ridge BP Wake Gpio Pin
      PcdSet8S (PcdPcieSlot2RootPort, 5);                                         // Maple Ridge BP Root Port number

      // Maple Ridge 2 (Front Panel)
      PcdSet32S (PcdPchPCIeSlot3PwrEnableGpioNo, GPIO_VER2_LP_GPP_E4);            // Maple Ridge FP RTD3 CIO Power Enable Gpio pin
      PcdSetBoolS (PcdPchPCIeSlot3PwrEnableGpioPolarity, PIN_GPIO_ACTIVE_HIGH);   // Maple Ridge FP RTD3 CIO Power Enable Gpio pin polarity
//[-start-210831-YUNLEI0128-modify]//
#if defined(C970_SUPPORT) || defined(C770_SUPPORT)

#else
      PcdSet32S (PcdPchPCIeSlot3RstGpioNo, GPIO_VER2_LP_GPP_E18);                 // Maple Ridge FP Reset Gpio pin
#endif
//[-end-210831-YUNLEI0128-modify]//
      PcdSetBoolS (PcdPchPCIeSlot3RstGpioPolarity, PIN_GPIO_ACTIVE_LOW);          // Maple Ridge FP Reset Gpio pin polarity
      PcdSet32S (PcdPcieSlot3WakeGpioPin, GPIO_VER2_LP_GPP_H17);                  // Maple Ridge FP Wake Gpio Pin
      PcdSet8S (PcdPcieSlot3RootPort, 9);                                         // Maple Ridge FP Root Port number

      // eDP Display Mux GPIO
      PcdSet32S (PcdDisplayMuxGpioNo, GPIO_VER2_LP_GPP_A7);
      break;
  }
  //
  // RootPort08 Rework Wake Capability on Pch Pcie slot
  //
  switch (BoardId) {
    case BoardIdAdlPLp4Rvp:
    case BoardIdAdlPLp5Rvp:
    case BoardIdAdlPT3Lp5Rvp:
    case BoardIdAdlPDdr5Rvp:
    case BoardIdAdlPDdr4Rvp:
      if (Setup.Rp08WakeReworkDone) {
          PcdSet32S (PcdPcieSlot1WakeGpioPin, GPIO_VER2_LP_GPP_A20);
          GpioPad = PcdGet32 (PcdPcieSlot1WakeGpioPin);
          DEBUG((DEBUG_INFO, "Pcie x4 pcie slot wake  : 0x%08x\n", GpioPad));
          GpioGetPadConfig (GpioPad, &GpioConfig);
          GpioConfig.InterruptConfig = GpioIntLevel | GpioIntSci;
        }
    break;
  }
  //
  // MIPI CAM
  //
  PcdSetBoolS (PcdMipiCamGpioEnable, TRUE);

  switch (BoardId) {
    case BoardIdAdlPLp4Rvp:
    case BoardIdAdlPLp5Rvp:
    case BoardIdAdlPT3Lp5Rvp:
    case BoardIdAdlPDdr5Rvp:
    case BoardIdAdlPDdr4Rvp:
      //
      // PCH M.2 SSD and Sata port
      //
      PcdSet32S (PcdPchSsd1PwrEnableGpioNo, GPIO_VER2_LP_GPP_D16);              // PCH M.2 SSD power enable gpio pin
      PcdSet32S (PcdPchSsd1RstGpioNo, GPIO_VER2_LP_GPP_H0);                     // PCH M.2 SSD reset gpio pin
      PcdSetBoolS (PcdPchSsd1PwrEnableGpioPolarity, PIN_GPIO_ACTIVE_HIGH);      // PCH M.2 SSD power enable gpio pin polarity
      PcdSetBoolS (PcdPchSsd1RstGpioPolarity, PIN_GPIO_ACTIVE_LOW);             // PCH M.2 SSD reset gpio pin polarity
      //
      // Touch Pad
      //
      PcdSet32S (PcdTouchpadIrqGpio, GPIO_VER2_LP_GPP_A15);                     // Touch Pad Interrupt Pin
      PcdSetBoolS (PcdTouchpadIrqGpioPolarity, PIN_GPIO_ACTIVE_LOW);            // Touch Pad Interrupt pin polarity
      //
      //Touch panel
      //
      if ((PchSetup.ThcPort0Assignment == ThcAssignmentNone) && (Setup.PchI2cTouchPanelType != 0)) {
        PcdSet32S (PcdTouchpanelIrqGpio, GPIO_VER2_LP_GPP_F18);                  // Touch Panel Interrupt Pin
        PcdSet32S (PcdTouchpanelPwrEnableGpio, GPIO_VER2_LP_GPP_F7);             // Touch Panel power enable pin
        PcdSet32S (PcdTouchpanelRstGpio, GPIO_VER2_LP_GPP_F17);                  // Touch Panel reset pin
        PcdSetBoolS (PcdTouchpanelIrqGpioPolarity, PIN_GPIO_ACTIVE_HIGH);         // Touch Panel Interrupt Pin polarity
        PcdSetBoolS (PcdTouchpanelPwrEnableGpioPolarity, PIN_GPIO_ACTIVE_HIGH);  // Touch Panel power enable pin polarity
        PcdSetBoolS (PcdTouchpanelRstGpioPolarity, PIN_GPIO_ACTIVE_HIGH);         // Touch Panel reset pin polarity
      }
      //
      // LID WAKE
      //
      PcdSet32S(PcdLidSwitchWakeGpio, GPIO_VER2_LP_GPD2);
      if((!PchSetup.PchLan) || (!IsGbePresent()) || (!PchIsGbeSupported()) || (!PchSetup.PchWakeOnLan)) {
        ConfigureGpio ((VOID *) (UINTN) mAdlPLidSwitchTable, (UINTN) (sizeof (mAdlPLidSwitchTable) / sizeof (GPIO_INIT_CONFIG)));
      }
      //
      // WLAN
      //
      PcdSet32S(PcdWlanWakeGpio, GPIO_VER2_LP_GPP_D13);  // Wlan wake gpio.
      PcdSet8S (PcdWlanRootPortNumber, 5);
      break;
      case BoardIdAdlPLp4Bep:
      //
      // Touch Pad and Touch Panel
      //
      PcdSet32S (PcdTouchpadIrqGpio, GPIO_VER2_LP_GPP_A20);                     // Touch Pad Interrupt Pin
      PcdSetBoolS (PcdTouchpadIrqGpioPolarity, PIN_GPIO_ACTIVE_LOW);            // Touch Pad Interrupt pin polarity
      PcdSet32S (PcdTouchpanelIrqGpio, GPIO_VER2_LP_GPP_E17);                   // Touch Panel Interrupt Pin
      PcdSet32S (PcdTouchpanelRstGpio, GPIO_VER2_LP_GPP_E6);                    // Touch Panel reset pin
      PcdSetBoolS (PcdTouchpanelIrqGpioPolarity, PIN_GPIO_ACTIVE_HIGH);         // Touch Panel Interrupt Pin polarity
      PcdSetBoolS (PcdTouchpanelRstGpioPolarity, PIN_GPIO_ACTIVE_HIGH);         // Touch Panel reset pin polarity
      break;
    case BoardIdAdlPDdr5Dg384Aep:
      //
      // PCH M.2 SSD and Sata port
      //
      PcdSet32S (PcdPchSsd1PwrEnableGpioNo, GPIO_VER2_LP_GPP_D16);              // PCH M.2 SSD power enable gpio pin
      PcdSet32S (PcdPchSsd1RstGpioNo, GPIO_VER2_LP_GPP_H0);                     // PCH M.2 SSD reset gpio pin
      PcdSetBoolS (PcdPchSsd1PwrEnableGpioPolarity, PIN_GPIO_ACTIVE_HIGH);      // PCH M.2 SSD power enable gpio pin polarity
      PcdSetBoolS (PcdPchSsd1RstGpioPolarity, PIN_GPIO_ACTIVE_LOW);             // PCH M.2 SSD reset gpio pin polarity
      PcdSet32S (PcdPchSataPortPwrEnableGpioNo, GPIO_VER2_LP_GPP_B4);           // Sata port power pin
      PcdSetBoolS (PcdPchSataPortPwrEnableGpioPolarity, PIN_GPIO_ACTIVE_HIGH);  // Sata port power pin polarity
      //
      // Touch Panel
      //
      PcdSet32S (PcdTouchpadIrqGpio, GPIO_VER2_LP_GPP_E12);                     // Touch Pad Interrupt Pin
      PcdSetBoolS (PcdTouchpadIrqGpioPolarity, PIN_GPIO_ACTIVE_LOW);            // Touch Pad Interrupt pin polarity
      PcdSet32S (PcdTouchpanelIrqGpio, GPIO_VER2_LP_GPP_E17);                   // Touch 0 Interrupt Pin
      PcdSet32S (PcdTouchpanelRstGpio, GPIO_VER2_LP_GPP_E6);                    // Touch 0 reset pin
      PcdSetBoolS (PcdTouchpanelIrqGpioPolarity, PIN_GPIO_ACTIVE_HIGH);         // Touch 0 Interrupt Pin polarity
      PcdSetBoolS (PcdTouchpanelRstGpioPolarity, PIN_GPIO_ACTIVE_HIGH);         // Touch 0 reset pin polarity
      //
      // WLAN
      //
      PcdSet32S(PcdWlanWakeGpio, GPIO_VER2_LP_GPP_D13);  // Wifi Wake
      break;
    case BoardIdAdlPLp5Aep:
      //
      // Touch Pad and Panels 0 & 1
      //
      PcdSet32S (PcdTouchpadIrqGpio, GPIO_VER2_LP_GPP_A15);                     // Touch Pad Interrupt Pin
      PcdSetBoolS (PcdTouchpadIrqGpioPolarity, PIN_GPIO_ACTIVE_LOW);            // Touch Pad Interrupt pin polarity
      PcdSet32S (PcdTouchpanelIrqGpio, GPIO_VER2_LP_GPP_E17);                   // Touch 0 Interrupt Pin
      PcdSetBoolS (PcdTouchpanelIrqGpioPolarity, PIN_GPIO_ACTIVE_HIGH);         // Touch 0 Interrupt Pin polarity
      PcdSet32S (PcdTouchpanelRstGpio, GPIO_VER2_LP_GPP_E6);                    // Touch 0 reset pin
      PcdSetBoolS (PcdTouchpanelRstGpioPolarity, PIN_GPIO_ACTIVE_HIGH);         // Touch 0 reset pin polarity
      break;
    case BoardIdAdlPLp5Dg128Aep:
      //
      // Touch Pad and Panels 0 & 1
      //
      PcdSet32S (PcdTouchpadIrqGpio, GPIO_VER2_LP_GPP_E12);                     // Touch Pad Interrupt Pin
      PcdSetBoolS (PcdTouchpadIrqGpioPolarity, PIN_GPIO_ACTIVE_LOW);            // Touch Pad Interrupt pin polarity
      PcdSet32S (PcdTouchpanelIrqGpio, GPIO_VER2_LP_GPP_E17);                   // Touch 0 Interrupt Pin
      PcdSetBoolS (PcdTouchpanelIrqGpioPolarity, PIN_GPIO_ACTIVE_HIGH);         // Touch 0 Interrupt Pin polarity
      PcdSet32S (PcdTouchpanelRstGpio, GPIO_VER2_LP_GPP_E6);                    // Touch 0 reset pin
      PcdSetBoolS (PcdTouchpanelRstGpioPolarity, PIN_GPIO_ACTIVE_HIGH);         // Touch 0 reset pin polarity
      break;
    case BoardIdAdlPLp5Gcs:
      //
      // LID WAKE
      //
      PcdSet32S(PcdLidSwitchWakeGpio, GPIO_VER2_LP_GPD2);
      if((!PchSetup.PchLan) || (!IsGbePresent()) || (!PchIsGbeSupported()) || (!PchSetup.PchWakeOnLan)) {
        ConfigureGpio ((VOID *) (UINTN) mAdlPLidSwitchTable, (UINTN) (sizeof (mAdlPLidSwitchTable) / sizeof (GPIO_INIT_CONFIG)));
      }
      //
      // Touch Pad and Panels 0 & 1
      //
      PcdSet32S (PcdTouchpadIrqGpio, GPIO_VER2_LP_GPP_A15);                     // Touch Pad Interrupt Pin
      PcdSetBoolS (PcdTouchpadIrqGpioPolarity, PIN_GPIO_ACTIVE_LOW);            // Touch Pad Interrupt pin polarity
      PcdSet32S (PcdTouchpanelIrqGpio, GPIO_VER2_LP_GPP_E17);                   // Touch 0 Interrupt Pin
      PcdSet32S (PcdTouchpanelPwrEnableGpio, GPIO_VER2_LP_GPP_F7);              // Touch 0 power enable pin
      PcdSet32S (PcdTouchpanelRstGpio, GPIO_VER2_LP_GPP_E6);                    // Touch 0 reset pin
      PcdSetBoolS (PcdTouchpanelIrqGpioPolarity, PIN_GPIO_ACTIVE_LOW);          // Touch 0 Interrupt Pin polarity
      PcdSetBoolS (PcdTouchpanelPwrEnableGpioPolarity, PIN_GPIO_ACTIVE_HIGH);   // Touch 0 power enable pin polarity
      PcdSetBoolS (PcdTouchpanelRstGpioPolarity, PIN_GPIO_ACTIVE_LOW);          // Touch 0 reset pin polarity
      break;
    case BoardIdAdlPMMAep:
      //
      // PCH M.2 SSD and Sata port
      //
      PcdSet32S (PcdPchSsd1PwrEnableGpioNo, GPIO_VER2_LP_GPP_D16);              // PCH M.2 SSD power enable gpio pin
      PcdSet32S (PcdPchSsd1RstGpioNo, GPIO_VER2_LP_GPP_H0);                     // PCH M.2 SSD reset gpio pin
      PcdSetBoolS (PcdPchSsd1PwrEnableGpioPolarity, PIN_GPIO_ACTIVE_HIGH);      // PCH M.2 SSD power enable gpio pin polarity
      PcdSetBoolS (PcdPchSsd1RstGpioPolarity, PIN_GPIO_ACTIVE_LOW);             // PCH M.2 SSD reset gpio pin polarity
      //
      // Touch Pad
      //
      PcdSet32S (PcdTouchpadIrqGpio, GPIO_VER2_LP_GPP_F18);                     // Touch Pad Interrupt Pin
      PcdSetBoolS (PcdTouchpadIrqGpioPolarity, PIN_GPIO_ACTIVE_LOW);
      break;
    case BoardIdAdlPLp5MbAep:
      PcdSet32S (PcdTouchpadIrqGpio, GPIO_VER2_LP_GPP_A15);                     // Touch Pad Interrupt Pin
      PcdSetBoolS (PcdTouchpadIrqGpioPolarity, PIN_GPIO_ACTIVE_LOW);            // Touch Pad Interrupt pin polarity
      break;
  }

  //
  // Type-C Aux Override GPIO Configuration or DNX GPIO Configuration.
  //
  switch (BoardId) {
    case BoardIdAdlPDdr4Rvp:
      if (AdlPAuxOverrideSetup () != 0) {
        DEBUG ((DEBUG_INFO, "Type-C Aux Override is Enable for Adl-P Board\n"));
        ConfigureGpio ((VOID *) (UINTN) mAdlPTypeCAuxOverrideEnableGpioTable, (UINTN) (sizeof ( mAdlPTypeCAuxOverrideEnableGpioTable) / sizeof (GPIO_INIT_CONFIG)));
      }
      break;

    case BoardIdAdlPLp5Rvp:
//    case BoardIdAdlPDdr5MRRvp: // This RVP Doesn't support Aux Override and Dnx
      if (AdlPAuxOverrideSetup () != 0) {
        DEBUG ((DEBUG_INFO, "Type-C Aux Override is Enable for Adl-P Board\n"));
        ConfigureGpio ((VOID *) (UINTN) mAdlPTypeCAuxOverrideEnableGpioTable, (UINTN) (sizeof ( mAdlPTypeCAuxOverrideEnableGpioTable) / sizeof (GPIO_INIT_CONFIG)));
      } else {
        DEBUG ((DEBUG_INFO, "DNX is Enable for Adl-P Board\n"));
        ConfigureGpio ((VOID *) (UINTN) mAdlPDnxEnableGpioTable, (UINTN) (sizeof ( mAdlPDnxEnableGpioTable) / sizeof (GPIO_INIT_CONFIG)));
      }
      break;

    case BoardIdAdlPLp4Rvp:
    case BoardIdAdlPDdr5Rvp:
      DEBUG ((DEBUG_INFO, "DNX is Enable for Adl-P Board\n"));
      ConfigureGpio ((VOID *) (UINTN) mAdlPDnxEnableGpioTable, (UINTN) (sizeof ( mAdlPDnxEnableGpioTable) / sizeof (GPIO_INIT_CONFIG)));
      break;

  }
  return;
}

/**
  Tbt init function for PEI post memory phase.
**/
VOID
AdlPBoardTbtInit (
  VOID
  )
{
  UINT16    BoardId;
  BoardId = PcdGet16(PcdBoardId);

#if FixedPcdGetBool (PcdDTbtEnable) == 1
  switch (BoardId) {
    case BoardIdAdlPDdr5MRRvp:
      PcdSet8S (PcdDTbtControllerNumber, 2);
      break;

    default:
      PcdSet8S (PcdDTbtControllerNumber, 0);
      break;
  }
#endif

  //
  // Set ALL port setting by default from VPD.
  //
  PcdSet32S (PcdCpuUsb30OverCurrentPinTable, (UINT32) (PcdGetPtr (VpdPcdCpuUsb3OcMap)));

  switch (BoardId) {
    case BoardIdAdlPDdr5Dg384Aep:
    case BoardIdAdlPDdr5Rvp:
    case BoardIdAdlPDdr5MRRvp:
    case BoardIdAdlPDdr4Rvp:
    case BoardIdAdlPSimics:
    case BoardIdAdlPMMAep:
      PcdSet8S (PcdTcssPdType, TcssPdTypeCypress);
      PcdSet8S (PcdTcssPdNumber, 2);
      break;
    case BoardIdAdlPLp4Bep:
    case BoardIdAdlPLp4Rvp:
    case BoardIdAdlPLp5Rvp:
    case BoardIdAdlPT3Lp5Rvp:
    case BoardIdAdlPLp5Aep:
    case BoardIdAdlPLp5Dg128Aep:
    case BoardIdAdlPLp5MbAep:
      PcdSet8S (PcdTcssPdType, TcssPdTypeTi);
      PcdSet8S (PcdTcssPdNumber, 2);
      break;
    case BoardIdAdlPLp5Gcs:
      PcdSet8S (PcdTcssPdType, TcssPdTypeTi);
      PcdSet8S (PcdTcssPdNumber, 1);
      break;
  }
}

/**
   Security GPIO init function for PEI post memory phase.
**/
VOID
AdlPBoardSecurityInit (
  VOID
  )
{
  GPIO_PAD  GpioPad;
  UINT32    GpioIrqNumber;
  UINT32    VerFtif;
  UINT16    BoardId;
  BoardId = PcdGet16 (PcdBoardId);

  if (FeaturePcdGet (PcdTpm2Enable) == TRUE) {
    //
    // If dTPM is connected to the SPI I/F, configure interrupt
    //
    VerFtif = MmioRead32 (R_PTT_TXT_STS_FTIF);

    if ((VerFtif & FTIF_FT_LOC_MASK) == V_FTIF_SPI_DTPM_PRESENT) {
      switch (BoardId) {
      case BoardIdAdlPLp5Gcs:
        GpioPad = (GPIO_PAD) GPIO_VER2_LP_GPP_F14;
        break;
      case BoardIdAdlPLp4Bep:
      case BoardIdAdlPLp4Rvp:
      case BoardIdAdlPLp5Rvp:
      case BoardIdAdlPT3Lp5Rvp:
      case BoardIdAdlPDdr5MRRvp:
      case BoardIdAdlPDdr5Rvp:
      case BoardIdAdlPDdr4Rvp:
      case BoardIdAdlPDdr5Dg384Aep:
      case BoardIdAdlPLp5Aep:
      case BoardIdAdlPLp5Dg128Aep:
      case BoardIdAdlPLp5MbAep:
      default:
        GpioPad = (GPIO_PAD) GPIO_VER2_LP_GPP_E3;
        break;
      }

      GpioGetPadIoApicIrqNumber (GpioPad, &GpioIrqNumber);

      DEBUG ((DEBUG_INFO, "TPM Interrupt Number: %d\n", GpioIrqNumber));
      PcdSet32S (PcdTpm2CurrentIrqNum, GpioIrqNumber);
    } else {
      PcdSet32S (PcdTpm2CurrentIrqNum, 0);
    }
  }
}

/**
  Board Specific Init for PEI post memory phase.
**/
VOID
PeiAdlPBoardSpecificInitPostMemNull (
  VOID
  )
{
}

/**
  Board's PCD function hook init function for PEI post memory phase.
**/
VOID
AdlPBoardFunctionInit (
  VOID
)
{
  PcdSet32S (PcdFuncPeiBoardSpecificInitPostMem, (UINTN) PeiAdlPBoardSpecificInitPostMemNull);
}

/**
  PMC-PD solution enable init lib
**/
VOID
AdlPBoardPmcPdInit (
  VOID
  )
{
  PcdSetBoolS (PcdBoardPmcPdEnable, 1);
}

/**
  USB TypeC init function for before silicon initialisation.
**/
VOID
AdlPBoardTypeCPortMapping (
  VOID
  )
{
  UINT16                          BoardId;

  BoardId = PcdGet16 (PcdBoardId);

  switch (BoardId) {
    case BoardIdAdlPLp4Bep:
      PcdSetBoolS (PcdUsbTypeCSupport, TRUE);
      // Number of ports supported
      PcdSet8S (PcdTypeCPortsSupported, 2);
      // Type C port 1 mapping
      PcdSet8S (PcdUsbTypeCPort1, 0);
      PcdSet8S (PcdUsbTypeCPort1Pch, 1);
      PcdSet8S (PcdUsbCPort1Properties, (0 << TCSS_TYPEC_ROOT_PORT_INDEX_BIT_OFFSET | USB_TYPEC_CPU << 1 | SPLIT_SUPPORTED));
      // Type C port 2 mapping
      PcdSet8S (PcdUsbTypeCPort2, 1);
      PcdSet8S (PcdUsbTypeCPort2Pch, 2);
      PcdSet8S (PcdUsbCPort2Properties, (1 << TCSS_TYPEC_ROOT_PORT_INDEX_BIT_OFFSET | USB_TYPEC_CPU << 1 | SPLIT_SUPPORTED));
      break;
    //
    //   Board             Type C port     CPU Port    PCH Port    Split support
    //   ADL P LP4,LP5          1            0            1            Yes
    //   ERB                    2            1            2            Yes
    //                          3            2            3            Yes
    //                          4            3            5            Yes
    case BoardIdAdlPLp4Rvp:
    case BoardIdAdlPLp5Rvp:
    case BoardIdAdlPT3Lp5Rvp:
    case BoardIdAdlPDdr5Rvp:
    case BoardIdAdlPDdr4Rvp:
    case BoardIdAdlPLp5Aep:
//[-start-211223-JAYAN00016-modify]//
#ifdef LCFC_SUPPORT
#ifdef C970_SUPPORT
      PcdSetBoolS (PcdUsbTypeCSupport, TRUE);
      // Number of ports supported
      PcdSet8S (PcdTypeCPortsSupported, 3);
      // Type C port 1 mapping
      PcdSet8S (PcdUsbTypeCPort1, 0);
      PcdSet8S (PcdUsbTypeCPort1Pch, 2);
      PcdSet8S (PcdUsbCPort1Properties, (0 << TCSS_TYPEC_ROOT_PORT_INDEX_BIT_OFFSET | USB_TYPEC_TBT << 1 | SPLIT_SUPPORTED));
      // Type C port 2 mapping
      PcdSet8S (PcdUsbTypeCPort2, 1);
      PcdSet8S (PcdUsbTypeCPort2Pch, 3);
      PcdSet8S (PcdUsbCPort2Properties, (1 << TCSS_TYPEC_ROOT_PORT_INDEX_BIT_OFFSET | USB_TYPEC_TBT << 1 | SPLIT_SUPPORTED));
      // Type C port 3 mapping
      PcdSet8S (PcdUsbTypeCPort3, 2);
      PcdSet8S (PcdUsbTypeCPort3Pch, 4);
      PcdSet8S (PcdUsbCPort3Properties, (2 << TCSS_TYPEC_ROOT_PORT_INDEX_BIT_OFFSET | USB_TYPEC_CPU << 1 | SPLIT_SUPPORTED));
      // Type C port 4 mapping
//      PcdSet8S (PcdUsbTypeCPort4, 3);
//      PcdSet8S (PcdUsbTypeCPort4Pch, 5);
//      PcdSet8S (PcdUsbCPort4Properties, (3 << TCSS_TYPEC_ROOT_PORT_INDEX_BIT_OFFSET | USB_TYPEC_CPU << 1 | SPLIT_SUPPORTED));
#endif
//[-start-211227-JAYAN00017-modify]//
#ifdef C770_SUPPORT
      PcdSetBoolS (PcdUsbTypeCSupport, TRUE);
      // Number of ports supported
      PcdSet8S (PcdTypeCPortsSupported, 2);
      // Type C port 1 mapping
      PcdSet8S (PcdUsbTypeCPort1, 0);
      PcdSet8S (PcdUsbTypeCPort1Pch, 2);
      PcdSet8S (PcdUsbCPort1Properties, (0 << TCSS_TYPEC_ROOT_PORT_INDEX_BIT_OFFSET | USB_TYPEC_TBT << 1 | SPLIT_SUPPORTED));
      // Type C port 2 mapping
//      PcdSet8S (PcdUsbTypeCPort2, 1);
//      PcdSet8S (PcdUsbTypeCPort2Pch, 2);
//      PcdSet8S (PcdUsbCPort2Properties, (1 << TCSS_TYPEC_ROOT_PORT_INDEX_BIT_OFFSET | USB_TYPEC_CPU << 1 | SPLIT_SUPPORTED));
      // Type C port 3 mapping
      PcdSet8S (PcdUsbTypeCPort3, 2);
      PcdSet8S (PcdUsbTypeCPort3Pch, 3);
      PcdSet8S (PcdUsbCPort3Properties, (2 << TCSS_TYPEC_ROOT_PORT_INDEX_BIT_OFFSET | USB_TYPEC_TBT << 1 | SPLIT_SUPPORTED));
      // Type C port 4 mapping
//      PcdSet8S (PcdUsbTypeCPort4, 3);
//      PcdSet8S (PcdUsbTypeCPort4Pch, 5);
//      PcdSet8S (PcdUsbCPort4Properties, (3 << TCSS_TYPEC_ROOT_PORT_INDEX_BIT_OFFSET | USB_TYPEC_CPU << 1 | SPLIT_SUPPORTED));
#endif
//[-end-211227-JAYAN00017-modify]//
#if defined(S77014_SUPPORT) || defined(S77014IAH_SUPPORT)
      PcdSetBoolS (PcdUsbTypeCSupport, TRUE);
      // Number of ports supported
      PcdSet8S (PcdTypeCPortsSupported, 2);
      // Type C port 1 mapping
      PcdSet8S (PcdUsbTypeCPort1, 0);
      PcdSet8S (PcdUsbTypeCPort1Pch, 2);
      PcdSet8S (PcdUsbCPort1Properties, (0 << TCSS_TYPEC_ROOT_PORT_INDEX_BIT_OFFSET | USB_TYPEC_TBT << 1 | SPLIT_SUPPORTED));
      // Type C port 2 mapping
//      PcdSet8S (PcdUsbTypeCPort2, 1);
//      PcdSet8S (PcdUsbTypeCPort2Pch, 2);
//      PcdSet8S (PcdUsbCPort2Properties, (1 << TCSS_TYPEC_ROOT_PORT_INDEX_BIT_OFFSET | USB_TYPEC_CPU << 1 | SPLIT_SUPPORTED));
      // Type C port 3 mapping
      PcdSet8S (PcdUsbTypeCPort3, 2);
      PcdSet8S (PcdUsbTypeCPort3Pch, 3);
      PcdSet8S (PcdUsbCPort3Properties, (2 << TCSS_TYPEC_ROOT_PORT_INDEX_BIT_OFFSET | USB_TYPEC_TBT << 1 | SPLIT_SUPPORTED));
      // Type C port 4 mapping
//      PcdSet8S (PcdUsbTypeCPort4, 3);
//      PcdSet8S (PcdUsbTypeCPort4Pch, 5);
//      PcdSet8S (PcdUsbCPort4Properties, (3 << TCSS_TYPEC_ROOT_PORT_INDEX_BIT_OFFSET | USB_TYPEC_CPU << 1 | SPLIT_SUPPORTED));
#endif
#ifdef S77013_SUPPORT
      PcdSetBoolS (PcdUsbTypeCSupport, TRUE);
      // Number of ports supported
      PcdSet8S (PcdTypeCPortsSupported, 2);
      // Type C port 1 mapping
      PcdSet8S (PcdUsbTypeCPort1, 0);
      PcdSet8S (PcdUsbTypeCPort1Pch, 2);
      PcdSet8S (PcdUsbCPort1Properties, (0 << TCSS_TYPEC_ROOT_PORT_INDEX_BIT_OFFSET | USB_TYPEC_TBT << 1 | SPLIT_SUPPORTED));
      // Type C port 2 mapping
//      PcdSet8S (PcdUsbTypeCPort2, 1);
//      PcdSet8S (PcdUsbTypeCPort2Pch, 2);
//      PcdSet8S (PcdUsbCPort2Properties, (1 << TCSS_TYPEC_ROOT_PORT_INDEX_BIT_OFFSET | USB_TYPEC_CPU << 1 | SPLIT_SUPPORTED));
      // Type C port 3 mapping
      PcdSet8S (PcdUsbTypeCPort3, 2);
      PcdSet8S (PcdUsbTypeCPort3Pch, 4);
      PcdSet8S (PcdUsbCPort3Properties, (2 << TCSS_TYPEC_ROOT_PORT_INDEX_BIT_OFFSET | USB_TYPEC_CPU << 1 | SPLIT_SUPPORTED));
      // Type C port 4 mapping
//      PcdSet8S (PcdUsbTypeCPort4, 3);
//      PcdSet8S (PcdUsbTypeCPort4Pch, 5);
//      PcdSet8S (PcdUsbCPort4Properties, (3 << TCSS_TYPEC_ROOT_PORT_INDEX_BIT_OFFSET | USB_TYPEC_CPU << 1 | SPLIT_SUPPORTED));
#endif
//[-start-211230-OWENWU0033-modify]//
#ifdef S570_SUPPORT
      PcdSetBoolS (PcdUsbTypeCSupport, TRUE);
      // Number of ports supported
      PcdSet8S (PcdTypeCPortsSupported, 2);
      // Type C port 1 mapping
      PcdSet8S (PcdUsbTypeCPort1, 0);
      PcdSet8S (PcdUsbTypeCPort1Pch, 2);
      PcdSet8S (PcdUsbCPort1Properties, (0 << TCSS_TYPEC_ROOT_PORT_INDEX_BIT_OFFSET | USB_TYPEC_CPU << 1 | SPLIT_SUPPORTED));
      //Type C port 2 mapping
      PcdSet8S (PcdUsbTypeCPort2, 1);
      PcdSet8S (PcdUsbTypeCPort2Pch, 2);
      PcdSet8S (PcdUsbCPort2Properties, (1 << TCSS_TYPEC_ROOT_PORT_INDEX_BIT_OFFSET | USB_TYPEC_CPU << 1 | SPLIT_SUPPORTED));
//       Type C port 3 mapping
//      PcdSet8S (PcdUsbTypeCPort3, 2);
//      PcdSet8S (PcdUsbTypeCPort3Pch, 3);
//      PcdSet8S (PcdUsbCPort3Properties, (2 << TCSS_TYPEC_ROOT_PORT_INDEX_BIT_OFFSET | USB_TYPEC_CPU << 1 | SPLIT_SUPPORTED));
      // Type C port 4 mapping
//      PcdSet8S (PcdUsbTypeCPort4, 3);
//      PcdSet8S (PcdUsbTypeCPort4Pch, 5);
//      PcdSet8S (PcdUsbCPort4Properties, (3 << TCSS_TYPEC_ROOT_PORT_INDEX_BIT_OFFSET | USB_TYPEC_CPU << 1 | SPLIT_SUPPORTED));
#endif
//[-end-211230-OWENWU0033-modify]//
#ifdef S370_SUPPORT
//[-start-211223-QINGLIN0133-add]//
      PcdSetBoolS (PcdUsbTypeCSupport, TRUE);
      // Number of ports supported
      PcdSet8S (PcdTypeCPortsSupported, 1);
      // Type C port 1 mapping
      PcdSet8S (PcdUsbTypeCPort1, 0);
      PcdSet8S (PcdUsbTypeCPort1Pch, 2);
      PcdSet8S (PcdUsbCPort1Properties, (0 << TCSS_TYPEC_ROOT_PORT_INDEX_BIT_OFFSET | USB_TYPEC_CPU << 1 | SPLIT_SUPPORTED));
      // Type C port 2 mapping
//      PcdSet8S (PcdUsbTypeCPort2, 1);
//      PcdSet8S (PcdUsbTypeCPort2Pch, 2);
//      PcdSet8S (PcdUsbCPort2Properties, (1 << TCSS_TYPEC_ROOT_PORT_INDEX_BIT_OFFSET | USB_TYPEC_CPU << 1 | SPLIT_SUPPORTED));
      // Type C port 3 mapping
//      PcdSet8S (PcdUsbTypeCPort3, 2);
//      PcdSet8S (PcdUsbTypeCPort3Pch, 3);
//      PcdSet8S (PcdUsbCPort3Properties, (2 << TCSS_TYPEC_ROOT_PORT_INDEX_BIT_OFFSET | USB_TYPEC_CPU << 1 | SPLIT_SUPPORTED));
      // Type C port 4 mapping
//      PcdSet8S (PcdUsbTypeCPort4, 3);
//      PcdSet8S (PcdUsbTypeCPort4Pch, 5);
//      PcdSet8S (PcdUsbCPort4Properties, (3 << TCSS_TYPEC_ROOT_PORT_INDEX_BIT_OFFSET | USB_TYPEC_CPU << 1 | SPLIT_SUPPORTED));
#endif
//[-end-211223-QINGLIN0133-add]//
#else
      PcdSetBoolS (PcdUsbTypeCSupport, TRUE);
      // Number of ports supported
      PcdSet8S (PcdTypeCPortsSupported, 4);
      // Type C port 1 mapping
      PcdSet8S (PcdUsbTypeCPort1, 0);
      PcdSet8S (PcdUsbTypeCPort1Pch, 1);
      PcdSet8S (PcdUsbCPort1Properties, (0 << TCSS_TYPEC_ROOT_PORT_INDEX_BIT_OFFSET | USB_TYPEC_CPU << 1 | SPLIT_SUPPORTED));
      // Type C port 2 mapping
      PcdSet8S (PcdUsbTypeCPort2, 1);
      PcdSet8S (PcdUsbTypeCPort2Pch, 2);
      PcdSet8S (PcdUsbCPort2Properties, (1 << TCSS_TYPEC_ROOT_PORT_INDEX_BIT_OFFSET | USB_TYPEC_CPU << 1 | SPLIT_SUPPORTED));
      // Type C port 3 mapping
      PcdSet8S (PcdUsbTypeCPort3, 2);
      PcdSet8S (PcdUsbTypeCPort3Pch, 3);
      PcdSet8S (PcdUsbCPort3Properties, (2 << TCSS_TYPEC_ROOT_PORT_INDEX_BIT_OFFSET | USB_TYPEC_CPU << 1 | SPLIT_SUPPORTED));
      // Type C port 4 mapping
      PcdSet8S (PcdUsbTypeCPort4, 3);
      PcdSet8S (PcdUsbTypeCPort4Pch, 5);
      PcdSet8S (PcdUsbCPort4Properties, (3 << TCSS_TYPEC_ROOT_PORT_INDEX_BIT_OFFSET | USB_TYPEC_CPU << 1 | SPLIT_SUPPORTED));
#endif
//[-end-211223-JAYAN00016-modify]//
      break;
    case BoardIdAdlPDdr5Dg384Aep:
      PcdSetBoolS (PcdUsbTypeCSupport, TRUE);
      // Number of ports supported
      PcdSet8S(PcdTypeCPortsSupported, 2);
      // Type C port 1 mapping
      PcdSet8S (PcdUsbTypeCPort1, 2);
      PcdSet8S (PcdUsbTypeCPort1Pch, 3);
      PcdSet8S (PcdUsbCPort1Properties, (0 << TCSS_TYPEC_ROOT_PORT_INDEX_BIT_OFFSET | USB_TYPEC_CPU << 1 | SPLIT_SUPPORTED));
      // Type C port 2 mapping
      PcdSet8S (PcdUsbTypeCPort2, 3);
      PcdSet8S (PcdUsbTypeCPort2Pch, 5);
      PcdSet8S (PcdUsbCPort2Properties, (1 << TCSS_TYPEC_ROOT_PORT_INDEX_BIT_OFFSET | USB_TYPEC_CPU << 1 | SPLIT_SUPPORTED));
      break;
    case BoardIdAdlPLp5Dg128Aep:
    case BoardIdAdlPLp5MbAep:
      PcdSetBoolS (PcdUsbTypeCSupport, TRUE);
      // Number of ports supported
      PcdSet8S(PcdTypeCPortsSupported, 3);
      // Type C port 1 mapping
      PcdSet8S (PcdUsbTypeCPort1, 1);
      PcdSet8S (PcdUsbTypeCPort1Pch, 2);
      PcdSet8S (PcdUsbCPort1Properties, (0 << TCSS_TYPEC_ROOT_PORT_INDEX_BIT_OFFSET | USB_TYPEC_CPU << 1 | SPLIT_SUPPORTED));
      // Type C port 2 mapping
      PcdSet8S (PcdUsbTypeCPort2, 2);
      PcdSet8S (PcdUsbTypeCPort2Pch, 3);
      PcdSet8S (PcdUsbCPort2Properties, (1 << TCSS_TYPEC_ROOT_PORT_INDEX_BIT_OFFSET | USB_TYPEC_CPU << 1 | SPLIT_SUPPORTED));
      // Type C port 3 mapping
      PcdSet8S (PcdUsbTypeCPort3, 3);
      PcdSet8S (PcdUsbTypeCPort3Pch, 5);
      PcdSet8S (PcdUsbCPort3Properties, (2 << TCSS_TYPEC_ROOT_PORT_INDEX_BIT_OFFSET | USB_TYPEC_CPU << 1 | SPLIT_SUPPORTED));
      break;
    case BoardIdAdlPLp5Gcs:
    case BoardIdAdlPMMAep:
      PcdSetBoolS (PcdUsbTypeCSupport, TRUE);
      // Number of ports supported
      PcdSet8S(PcdTypeCPortsSupported, 2);
      // Type C port 1 mapping
      PcdSet8S (PcdUsbTypeCPort1, 2);
      PcdSet8S (PcdUsbTypeCPort1Pch, 3);
      PcdSet8S (PcdUsbCPort1Properties, (0 << TCSS_TYPEC_ROOT_PORT_INDEX_BIT_OFFSET | USB_TYPEC_CPU << 1 | SPLIT_SUPPORTED));
      // Type C port 2 mapping
      PcdSet8S (PcdUsbTypeCPort2, 3);
      PcdSet8S (PcdUsbTypeCPort2Pch, 5);
      PcdSet8S (PcdUsbCPort2Properties, (1 << TCSS_TYPEC_ROOT_PORT_INDEX_BIT_OFFSET | USB_TYPEC_CPU << 1 | SPLIT_SUPPORTED));
      break;

    //
    //   Board             Type C port   Port Origin    PCH Port    Split support
    //   ADL P 2 MR             5          CPU 0          8            Yes
    //                          1          MR1 0          1            Yes
    //                          2          MR1 1          2            Yes
    //                          3          MR2 0          3            Yes
    //                          4          MR2 1          5            Yes
    case BoardIdAdlPDdr5MRRvp: // ADL-P 2 DTBT MR Controller RVP
      PcdSetBoolS (PcdUsbTypeCSupport, TRUE);
      // Number of ports supported
      PcdSet8S (PcdTypeCPortsSupported, 5);
      // Type C port 5 mapping: CPU XHCI Port 1 Type-A: PCH USB2 Port 8
      PcdSet8S (PcdUsbTypeCPort5, 0);
      PcdSet8S (PcdUsbTypeCPort5Pch, 8);
      PcdSet8S (PcdUsbCPort5Properties, (0 << TCSS_TYPEC_ROOT_PORT_INDEX_BIT_OFFSET | USB_TYPEA_CPU << 1 | SPLIT_SUPPORTED));
      // Type C port 1 mapping: MR 1 Port 0 Type-C: PCH USB2 Port 1
      PcdSet8S (PcdUsbTypeCPort1, 1);
      PcdSet8S (PcdUsbTypeCPort1Pch, 1);
      PcdSet8S (PcdUsbCPort1Properties, (5 << TCSS_TYPEC_ROOT_PORT_INDEX_BIT_OFFSET | USB_TYPEC_TBT << 1 | SPLIT_SUPPORTED));
      // Type C port 2 mapping: MR 1 Port 1 Type-C: PCH USB2 Port 2
      PcdSet8S (PcdUsbTypeCPort2, 2);
      PcdSet8S (PcdUsbTypeCPort2Pch, 2);
      PcdSet8S (PcdUsbCPort2Properties, (5 << TCSS_TYPEC_ROOT_PORT_INDEX_BIT_OFFSET | USB_TYPEC_TBT << 1 | SPLIT_SUPPORTED));
      // Type C port 3 mapping: MR 2 Port 0 Type-C: PCH USB2 Port 3
      PcdSet8S (PcdUsbTypeCPort3, 1);
      PcdSet8S (PcdUsbTypeCPort3Pch, 3);
      PcdSet8S (PcdUsbCPort3Properties, (9 << TCSS_TYPEC_ROOT_PORT_INDEX_BIT_OFFSET | USB_TYPEC_TBT << 1 | SPLIT_SUPPORTED));
      // Type C port 4 mapping: MR 2 Port 1 Type-C: PCH USB2 Port 5
      PcdSet8S (PcdUsbTypeCPort4, 2);
      PcdSet8S (PcdUsbTypeCPort4Pch, 5);
      PcdSet8S (PcdUsbCPort4Properties, (9 << TCSS_TYPEC_ROOT_PORT_INDEX_BIT_OFFSET | USB_TYPEC_TBT << 1 | SPLIT_SUPPORTED));
      break;
    default:
      PcdSetBoolS (PcdUsbTypeCSupport, FALSE);
      break;
  }
}

/**
  Notify Ec To Pd firmware for enabling Vpro.
**/
VOID
AdlPNotifyEcToPdForVpro (
  VOID
  )
{
  EFI_STATUS                        Status;
  EFI_BOOT_MODE                     BootMode;
  EFI_PEI_READ_ONLY_VARIABLE2_PPI   *VariableServices;
  ME_SETUP                          MeSetup;
  UINTN                             VariableSize;

  PeiServicesGetBootMode (&BootMode);

  //
  // PD will keep original contents during Sx state.
  // It doesn't need to do anything during these states.
  //
  if ((BootMode == BOOT_ON_S3_RESUME) ||
      (BootMode == BOOT_ON_S4_RESUME) ||
      (BootMode == BOOT_ON_S5_RESUME)) {
    return;
  }

  Status = PeiServicesLocatePpi (
             &gEfiPeiReadOnlyVariable2PpiGuid,
             0,
             NULL,
             (VOID **) &VariableServices
             );
  ASSERT_EFI_ERROR (Status);

  VariableSize = sizeof (ME_SETUP);
  Status = VariableServices->GetVariable (
                               VariableServices,
                               L"MeSetup",
                               &gMeSetupVariableGuid,
                               NULL,
                               &VariableSize,
                               &MeSetup
                               );
  ASSERT_EFI_ERROR (Status);

  if (MeSetup.vProTbtDock == 1) {
    NotifyEcToPdForVpro ();
  }

  return;
}

//[-start-201104-IB17510119-add]//
STATIC
EFI_STATUS
GpioCfgPei (
//[-start-210120-IB16560239-modify]//
  IN H2O_BOARD_ID               Board,
//[-end-210120-IB16560239-modify]//
  IN BOOLEAN                    PreMemFlag
  )
{
  EFI_STATUS                    Status;
  H2O_GPIO_INIT_CONFIG          *GpioInitStruct;
  UINT32                        GpioInitStructCount;
  UINT32                        GpioTableCount;
  GPIO_INIT_CONFIG              *GpioDefinitionTable;
  H2O_GPIO_INIT_CONFIG          SingleGpioInitStruct = {0};
  UINT32                        Index;
  UINT32                        PadMaxSize;
  GPIO_GROUP                    Group;

  PadMaxSize = 0;
  for (Group = GpioGetLowestGroup(); Group <= GpioGetHighestGroup(); Group ++) {
    PadMaxSize += GpioGetPadPerGroup (Group);
  }

  GpioDefinitionTable = NULL;
  GpioDefinitionTable = (GPIO_INIT_CONFIG *)AllocateZeroPool (PadMaxSize * sizeof (GPIO_INIT_CONFIG));
  if (GpioDefinitionTable == NULL) {
    return EFI_OUT_OF_RESOURCES;
  }
  //
  // Use GetGpioCfg in GpioCfgLib to get build-time GPIO configuration
  // Input Board value corresponds to Skuids in Project.dsc
  //
  Status = GetGpioCfg (Board, &GpioInitStruct, &GpioInitStructCount);
  ASSERT_EFI_ERROR (Status);
  if (EFI_ERROR (Status)) {
    return EFI_NOT_FOUND;
  }

  GpioTableCount = 0;
  Status = EFI_UNSUPPORTED;
  for (Index = 0; Index < GpioInitStructCount; Index++) {
    if (GpioInitStruct[Index].Flag != PreMemFlag) {
      continue;
    }
    CopyMem (&SingleGpioInitStruct, &GpioInitStruct[Index], sizeof(H2O_GPIO_INIT_CONFIG));
    //
    // OemServices
    // This function works when PCT being FALSE or PCT's setting being null.
    // Please refer to Power On Guide 0.4.
    //
    DEBUG ((DEBUG_INFO, "OemKernelServices Call: OemSvcUpdateGpioCfg \n"));
    Status = OemSvcUpdateGpioCfg (&SingleGpioInitStruct);
    DEBUG ((DEBUG_INFO, "OemKernelServices OemSvcUpdateGpioCfg Status: %r\n", Status));
    if (Status != EFI_SUCCESS) {
     CopyMem (&GpioDefinitionTable[GpioTableCount], &SingleGpioInitStruct, sizeof(GPIO_INIT_CONFIG));
     GpioTableCount++;
    }
  }
  if ((Status == EFI_SUCCESS) && (GpioTableCount == 0x00)) {
    return EFI_SUCCESS;
  }

  //
  // Batch process GPIO configuraions.
  //
  DEBUG ((DEBUG_INFO, "ConfigureGpio() Start\n"));
  Status = GpioConfigurePads (GpioTableCount, GpioDefinitionTable);
  DEBUG ((DEBUG_INFO, "ConfigureGpio() End\n"));

  return EFI_SUCCESS;
}
//[-end-201104-IB17510119-add]//

/**
  Configure GPIO, TouchPanel, HDA, PMC, TBT etc.

  @retval  EFI_SUCCESS   Operation success.
**/
EFI_STATUS
EFIAPI
AdlPBoardInitBeforeSiliconInit (
  VOID
  )
{
//[-start-191001-16990100-add]//
  EFI_STATUS       Status;
  UINT16           GpioTableCount;
  GPIO_INIT_CONFIG *GpioTable;
//[-end-191001-16990100-add]//
//[-start-201104-IB17510119-add]//
  UINT16            BoardId;
  
  BoardId = PcdGet16(PcdBoardId);
//[-end-201104-IB17510119-add]//
  AdlPInit ();

  AdlPSerialIoI2cPadsTerminationInit ();
  AdlPTouchPanelGpioInit ();
  AdlPCvfGpioInit();
  AdlPHdaVerbTableInit ();
  AdlPBoardMiscInit ();
  AdlPBoardTbtInit ();
  AdlPBoardFunctionInit();
  AdlPBoardSecurityInit();
  AdlPBoardPmcPdInit ();
  AdlPBoardTypeCPortMapping ();
//[-start-210120-IB16560239-remove]//
//  GpioInit (PcdGetPtr (PcdBoardGpioTable));
//[-end-210120-IB16560239-remove]//
//[-start-220107-TAMT000041-add]//
#if defined(S77013_SUPPORT)
  UINT8      BootupStatus;
  UINT8      Bootup;

  #define    EC_BOOTUP_STATE_CMD              0x49
  Status = EcCommand (
              EC_CMD_STATE,
              EC_DATA,
              1,
              &BootupStatus,
              EC_BOOTUP_STATE_CMD,
              0);
    if (EFI_ERROR (Status)) {
      return Status;
    }
    PcdSet8S (NotifyECBootUp, BootupStatus);
    Bootup = PcdGet8(NotifyECBootUp);
    if((BootupStatus == 0)) {//BootupStatus =0 is Normal_Bootup_State
      GpioConfigurePads (2, S77013_LED_early_init); //LED On
    } 
#endif
//[-end-220107-TAMT000041-add]//
//[-start-201104-IB17510119-modify]//
  if (PcdGetBool (PcdH2OGpioCfgSupported) && !PcdGetBool(PcdDisableVpdGpioTable)) {
    //
    // Program GPIOs, Input Board value corresponds to Skuids in Project.dsc
    //
    DEBUG ((DEBUG_INFO, "GpioCfgPei() Start\n"));
//[-start-210120-IB16560239-modify]//
    GpioCfgPei ((H2O_BOARD_ID)BoardId, FALSE);
//[-end-210120-IB16560239-modify]//
    DEBUG ((DEBUG_INFO, "GpioCfgPei() END\n"));
  } else {
    GpioTable = PcdGetPtr (PcdBoardGpioTable);
    GetGpioTableSize (GpioTable, &GpioTableCount);
    DEBUG_OEM_SVC ((DEBUG_INFO, "Pei OemChipsetServices Call: OemSvcModifyGpioSettingTable \n"));
    Status = OemSvcModifyGpioSettingTable (&GpioTable, &GpioTableCount);
    DEBUG_OEM_SVC ((DEBUG_INFO, "Pei OemChipsetServices OemSvcModifyGpioSettingTable Status: %r\n", Status));
    if (GpioTableCount != 0 && Status != EFI_SUCCESS) {
      GpioConfigurePads ((UINT32) GpioTableCount, GpioTable);
    }
  }
//[-end-201104-IB17510119-modify]//
  ///
  /// Do Late PCH init
  ///
  LateSiliconInit ();

  //
  // Notify Ec To Pd firmware for enabling Vpro
  //
  AdlPNotifyEcToPdForVpro ();

  return EFI_SUCCESS;
}
VOID
AdlPBoardSpecificGpioInitPostMem (
  VOID
  )
{
  UINT16    BoardId;
  BoardId = PcdGet16 (PcdBoardId);
  //
  // Assign FingerPrint, Gnss, Bluetooth & TouchPanel relative GPIO.
  //
  switch (BoardId) {
    case BoardIdAdlPLp4Bep:
      PcdSet32S (PcdWwanModemBaseBandResetGpio, GPIO_VER2_LP_GPP_F18); // WWAN/Modem Base Band Reset pin
      PcdSet32S (PcdBtRfKillGpio, GPIO_VER2_LP_GPP_A13);               // Bluetooth RF-KILL# pin
      //
      //Update PcdBoardRtd3TableSignature per Setup
      //
      PcdSet64S (PcdBoardRtd3TableSignature, SIGNATURE_64 ('A', 'd', 'l', 'P', '_', 'R', 'v', 'p'));
      //
      // Update OEM table ID
      //
      PcdSet64S (PcdXhciAcpiTableSignature, SIGNATURE_64 ('x', 'h', '_', 'a', 'd', 'l', 'L', 'P'));
      break;
    case BoardIdAdlPLp5Rvp:
    case BoardIdAdlPT3Lp5Rvp:
    case BoardIdAdlPLp4Rvp:
    case BoardIdAdlPDdr5Rvp:
    case BoardIdAdlPDdr4Rvp:
      PcdSet32S (PcdHdaI2sCodecIrqGpio,         GPIO_VER2_LP_GPP_H3); // Audio I2S Codec IRQ Pin
      PcdSet8S  (PcdHdaI2sCodecI2cBusNumber,    0);                    // Audio I2S Codec conntected to I2C0
      PcdSet32S (PcdWwanModemBaseBandResetGpio, GPIO_VER2_LP_GPP_F14); // WWAN/Modem Base Band Reset pin
      //
      // Configure GPIOs for BT modules - UART or USB
      //
      PcdSet32S (PcdBtRfKillGpio, GPIO_VER2_LP_GPP_A13); // Bluetooth RF-KILL# pin
      if (PcdGet8 (PcdDiscreteBtModule) == 2) {          // Only for BT Over UART Selection
        PcdSet32S (PcdBtIrqGpio, GPIO_VER2_LP_GPP_E0);  // Bluetooth IRQ Pin
      }
      //
      //Update PcdBoardRtd3TableSignature per Setup
      //
      PcdSet64S (PcdBoardRtd3TableSignature, SIGNATURE_64 ('A', 'd', 'l', 'P', '_', 'R', 'v', 'p'));
      //
      // Update OEM table ID
      //
      PcdSet64S (PcdXhciAcpiTableSignature, SIGNATURE_64 ('x', 'h', '_', 'a', 'd', 'l', 'L', 'P'));
      break;
    case BoardIdAdlPLp5Aep:
      PcdSet32S (PcdWwanModemBaseBandResetGpio, GPIO_VER2_LP_GPP_F18); // WWAN/Modem Base Band Reset pin
      //
      // Configure GPIOs for BT modules - UART or USB
      //
      PcdSet32S (PcdBtRfKillGpio, GPIO_VER2_LP_GPP_A13); // Bluetooth RF-KILL# pin
      //
      //Update PcdBoardRtd3TableSignature per Setup
      //
      PcdSet64S (PcdBoardRtd3TableSignature, SIGNATURE_64 ('A', 'd', 'l', 'P', '_', 'R', 'v', 'p'));
      //
      // Update OEM table ID
      //
      PcdSet64S (PcdXhciAcpiTableSignature, SIGNATURE_64 ('x', 'h', '_', 'a', 'd', 'l', 'L', 'P'));
      break;
    case BoardIdAdlPMMAep:
      PcdSet32S (PcdWwanModemBaseBandResetGpio, GPIO_VER2_LP_GPP_F14); // WWAN/Modem Base Band Reset pin
      //
      // Configure GPIOs for BT modules - UART or USB
      //
      PcdSet32S (PcdBtRfKillGpio, GPIO_VER2_LP_GPP_A13); // Bluetooth RF-KILL# pin
      //
      //Update PcdBoardRtd3TableSignature per Setup
      //
      PcdSet64S (PcdBoardRtd3TableSignature, SIGNATURE_64 ('A', 'd', 'l', 'P', '_', 'R', 'v', 'p'));
      //
      // Update OEM table ID
      //
      PcdSet64S (PcdXhciAcpiTableSignature, SIGNATURE_64 ('x', 'h', '_', 'a', 'd', 'l', 'L', 'P'));
      break;
    case BoardIdAdlPDdr5Dg384Aep:
      //
      // Configure GPIOs for BT modules - UART or USB
      //
      PcdSet32S (PcdBtRfKillGpio, GPIO_VER2_LP_GPP_A13); // Bluetooth RF-KILL# pin
      //
      //Update PcdBoardRtd3TableSignature per Setup
      //
      PcdSet64S (PcdBoardRtd3TableSignature, SIGNATURE_64 ('A', 'd', 'l', 'P', '_', '3', '8', '4'));
      //
      // Update OEM table ID
      //
      PcdSet64S (PcdXhciAcpiTableSignature, SIGNATURE_64 ('x', 'h', '_', 'a', 'd', 'l', 'L', 'P'));
      break;
    case BoardIdAdlPLp5Dg128Aep:
    case BoardIdAdlPLp5MbAep:
      PcdSet32S (PcdWwanModemBaseBandResetGpio, GPIO_VER2_LP_GPP_F18); // WWAN/Modem Base Band Reset pin
      //
      // Configure GPIOs for BT modules - UART or USB
      //
      PcdSet32S (PcdBtRfKillGpio, GPIO_VER2_LP_GPP_A13); // Bluetooth RF-KILL# pin
      //
      //Update PcdBoardRtd3TableSignature per Setup
      //
      PcdSet64S (PcdBoardRtd3TableSignature, SIGNATURE_64 ('A', 'd', 'l', 'P', '_', '1', '2', '8'));
      //
      // Update OEM table ID
      //
      PcdSet64S (PcdXhciAcpiTableSignature, SIGNATURE_64 ('x', 'h', '_', 'a', 'd', 'l', 'L', 'P'));
      break;
    case BoardIdAdlPLp5Gcs:
      //
      // Configure GPIOs for BT modules - UART or USB
      //
      PcdSet32S (PcdBtRfKillGpio, GPIO_VER2_LP_GPP_A13); // Bluetooth RF-KILL# pin
      //
      //Update PcdBoardRtd3TableSignature per Setup
      //
      PcdSet64S (PcdBoardRtd3TableSignature, SIGNATURE_64 ('A', 'd', 'l', 'P', '_', 'R', 'v', 'p'));
      //
      // Update OEM table ID
      //
      PcdSet64S (PcdXhciAcpiTableSignature, SIGNATURE_64 ('x', 'h', '_', 'a', 'd', 'l', 'L', 'P'));
      break;
    case BoardIdAdlPDdr5MRRvp:
      PcdSet32S (PcdHdaI2sCodecIrqGpio,         GPIO_VER2_LP_GPP_H3); // Audio I2S Codec IRQ Pin
      PcdSet8S  (PcdHdaI2sCodecI2cBusNumber,    0);                    // Audio I2S Codec conntected to I2C0
      PcdSet32S (PcdWwanModemBaseBandResetGpio, GPIO_VER2_LP_GPP_F14); // WWAN/Modem Base Band Reset pin
      //
      // Configure GPIOs for BT modules - UART or USB
      //
      PcdSet32S (PcdBtRfKillGpio, GPIO_VER2_LP_GPP_A13); // Bluetooth RF-KILL# pin
      if (PcdGet8 (PcdDiscreteBtModule) == 2) {          // Only for BT Over UART Selection
        PcdSet32S (PcdBtIrqGpio, GPIO_VER2_LP_GPP_E0);  // Bluetooth IRQ Pin
      }
      //
      //Update PcdBoardRtd3TableSignature per Setup
      //
      PcdSet64S (PcdBoardRtd3TableSignature, SIGNATURE_64 ('A', 'd', 'l', 'P', '_', '2', 'M', 'R'));
      //
      // Update OEM table ID
      //
      PcdSet64S (PcdXhciAcpiTableSignature, SIGNATURE_64 ('x', 'h', '_', 'a', 'd', 'l', 'M', 'R'));
      break;

    default:
      //
      //Update PcdBoardRtd3TableSignature per Setup
      //
      PcdSet64S (PcdBoardRtd3TableSignature, SIGNATURE_64 ('A', 'd', 'l', 'P', '_', 'R', 'v', 'p'));
      //
      // Update OEM table ID
      //
      PcdSet64S (PcdXhciAcpiTableSignature, SIGNATURE_64 ('x', 'h', '_', 'a', 'd', 'l', 'L', 'P'));
      break;
  }
  //Modify Preferred_PM_Profile field based on Board SKU's. Default is set to Mobile
  //
  PcdSet8S (PcdPreferredPmProfile, EFI_ACPI_2_0_PM_PROFILE_MOBILE);
  if (PcdGet8 (PcdPlatformFlavor) == FlavorUpServer) {
    PcdSet8S (PcdPreferredPmProfile, EFI_ACPI_2_0_PM_PROFILE_ENTERPRISE_SERVER);
  }
}

VOID
AdlPInitCommonPlatformPcdPostMem (
  VOID
  )
{
  UINT16                          BoardId;
  BoardId = PcdGet16(PcdBoardId);

  PcdSet32S (PcdEcSmiGpio, GPIO_VER2_LP_GPP_E7);
  PcdSet32S (PcdEcLowPowerExitGpio,GPIO_VER2_LP_GPP_F9);
  PcdSetBoolS (PcdPssReadSN, TRUE);
  PcdSet8S (PcdPssI2cSlaveAddress, 0x6E);
  PcdSet8S (PcdPssI2cBusNumber, 0x05);
  PcdSetBoolS (PcdSpdAddressOverride, FALSE);

  //
  // Battery Present
  // Real & Virtual battery is need to supported in all except Desktop
  //
  switch (BoardId) {
    case BoardIdAdlPLp5Gcs:
      PcdSet8S (PcdBatteryPresent, BOARD_REAL_BATTERY_SUPPORTED);
      break;
    default:
      PcdSet8S (PcdBatteryPresent, BOARD_REAL_BATTERY_SUPPORTED | BOARD_VIRTUAL_BATTERY_SUPPORTED);
      break;
  }
  //
  // Real Battery 1 Control & Real Battery 2 Control
  //
  PcdSet8S (PcdRealBattery1Control, 1);
  PcdSet8S (PcdRealBattery2Control, 2);
   //
  // Sky Camera Sensor
  //
  PcdSetBoolS (PcdMipiCamSensor, FALSE);

  //
  // H8S2113 SIO, UART
  //
  PcdSetBoolS (PcdH8S2113SIO, FALSE);
    PcdSetBoolS (PcdH8S2113UAR, FALSE);

  //
  // NCT6776F COM, SIO & HWMON
  //
  PcdSetBoolS (PcdNCT6776FCOM, FALSE);
  PcdSetBoolS (PcdNCT6776FSIO, FALSE);
  PcdSetBoolS (PcdNCT6776FHWMON, FALSE);
  //
  // ZPODD
  //
  PcdSet8S (PcdZPoddConfig, 0);
  //
  // SMC Runtime Sci Pin
  // EC will use eSpi interface to generate SCI
  //
  PcdSet32S (PcdSmcRuntimeSciPin, 0x00);
  //
  // Convertable Dock Support
  // Not supported only for S & H SKU's
  PcdSetBoolS (PcdConvertableDockSupport, TRUE);
  //
  // Ec Hotkey F3, F4, F5, F6, F7 and F8 Support
  //
  PcdSet8S (PcdEcHotKeyF3Support, TRUE);
  PcdSet8S (PcdEcHotKeyF4Support, TRUE);
  PcdSet8S (PcdEcHotKeyF5Support, TRUE);
  PcdSet8S (PcdEcHotKeyF6Support, TRUE);
  PcdSet8S (PcdEcHotKeyF7Support, TRUE);
  PcdSet8S (PcdEcHotKeyF8Support, TRUE);

  //
  // Virtual Button Volume Up & Done Support
  // Virtual Button Home Button Support
  // Virtual Button Rotation Lock Support
  //
  PcdSetBoolS (PcdVirtualButtonVolumeUpSupport, TRUE);
  PcdSetBoolS (PcdVirtualButtonVolumeDownSupport, TRUE);
  PcdSetBoolS (PcdVirtualButtonHomeButtonSupport, TRUE);
  PcdSetBoolS (PcdVirtualButtonRotationLockSupport, TRUE);
  //
  // Slate Mode Switch Support
  //
  PcdSetBoolS (PcdSlateModeSwitchSupport, TRUE);
  //
  // Virtual Gpio Button Support
  //
  PcdSetBoolS (PcdVirtualGpioButtonSupport, TRUE);
  //
  // Ac Dc Auto Switch Support
  //
  PcdSetBoolS (PcdAcDcAutoSwitchSupport, TRUE);

  //
  // Pm Power Button Gpio Pin
  //
  PcdSet32S (PcdPmPowerButtonGpioPin, GPIO_VER2_LP_GPD3);
  //
  // Acpi Enable All Button Support
  //
  PcdSetBoolS (PcdAcpiEnableAllButtonSupport, TRUE);
  //
  // Acpi Hid Driver Button Support
  //
  PcdSetBoolS (PcdAcpiHidDriverButtonSupport, TRUE);

  //
  // ADL-P supports EC-PD design, for communication between EC and PD.
  //
  PcdSetBoolS (PcdUsbcEcPdNegotiation, TRUE);
}

/**
  Board init for PEI after Silicon initialized

  @retval  EFI_SUCCESS   Operation success.
**/
EFI_STATUS
EFIAPI
AdlPBoardInitAfterSiliconInit (
  VOID
  )
{
  DEBUG ((DEBUG_INFO, "ADLPBoardInitAfterSiliconInit \n"));
  AdlPBoardSpecificGpioInitPostMem ();
  AdlPInitCommonPlatformPcdPostMem ();

  return EFI_SUCCESS;
}
