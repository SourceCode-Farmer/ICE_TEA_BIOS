/** @file

;******************************************************************************
;* Copyright (c) 2020, Insyde Software Corporation. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/
/** @file

@copyright
  INTEL CONFIDENTIAL
  Copyright 2020 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains a 'Sample Driver' and is licensed as such under the terms
  of your license agreement with Intel or your vendor. This file may be modified
  by the user, subject to the additional terms of the license agreement.

@par Specification Reference:
**/

#include <PiPei.h>
#include <Library/BaseMemoryLib.h>
#include <Library/BiosIdLib.h>
#include <Library/PcdLib.h>
#include <Library/DebugLib.h>
#include <Library/PciLib.h>
#include <Library/MemoryAllocationLib.h>
#include <Library/PeiLib.h>
#include <Guid/MemoryOverwriteControl.h>
#include <Library/MmioInitLib.h>
#include <PlatformBoardConfig.h>
#include <Library/SiliconInitLib.h>
#include <Library/PchCycleDecodingLib.h>
#include <Register/PmcRegs.h>
#include <Library/PmcLib.h>
#include <Library/PeiBootModeLib.h>
#include <Ppi/ReadOnlyVariable2.h>
#include <Library/PeiServicesLib.h>
#include <Library/WakeupEventLib.h>
#include <Library/GpioLib.h>
#include <Library/BoardConfigLib.h>
#include <Library/TimerLib.h>
#include <AlderLakePRvpBoardConfigPatchTable.h>
#include <PlatformBoardId.h>
#include <SioRegs.h>
#include <Library/IoLib.h>
#include <Pins/GpioPinsVer2Lp.h>
#include <Library/PreSiliconEnvDetectLib.h>
#include <Library/PchInfoLib.h>
#include <Library/EcMiscLib.h>
//[-start-201104-IB17510119-add]//
#include <Library/GpioCfgLib.h>
#include <Library/GpioNativeLib.h>
#include <Library/PeiOemSvcKernelLib.h>
//[-end-201104-IB17510119-add]//
//[-start-191001-16990100-add]//
#include <Library/PeiOemSvcChipsetLib.h>
//[-end-191001-16990100-add]//
//[-start-210908-YUNLEI0131-modify]//
#ifdef C770_SUPPORT
#include <Library/Gpiolib.h>
#include <Library/OemSvcLfcPeiGetBoardID.h>
#endif
//[-end-210908-YUNLEI0131-modify]//
//[-start-210914-SHAONN0007-modify]//
#ifdef S370_SUPPORT
#include <Library/Gpiolib.h>
#include <Library/OemSvcLfcPeiGetBoardID.h>
//[-start-210914-QINGLIN0066-add]//
#include <ChipsetSetupConfig.h>
//[-end-210914-QINGLIN0066-add]//
#endif
//[-end-210914-SHAONN0007-modify]//
//[-start-210928-BAOBO0003-add]//
#include <Library/LfcEcLib.h>
//[-end-210928-BAOBO0003-add]//

#define SIO_RUNTIME_REG_BASE_ADDRESS      0x0680

static EFI_PEI_PPI_DESCRIPTOR mSetupVariablesReadyPpi = {
  (EFI_PEI_PPI_DESCRIPTOR_PPI | EFI_PEI_PPI_DESCRIPTOR_TERMINATE_LIST),
  &gSetupVariablesReadyPpiGuid,
  NULL
};
//[-start-210928-BAOBO0003-add]//
#ifdef C970_SUPPORT
static GLOBAL_REMOVE_IF_UNREFERENCED GPIO_INIT_CONFIG YogaC970_LED_early_init[] =
{
  {GPIO_VER2_LP_GPP_H20, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirOut,     GpioOutLow,    GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//CPU_FN_LED_N
  {GPIO_VER2_LP_GPP_H22, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirOut,     GpioOutLow,    GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//CPU_CAPS_LED_N
};
#endif
//[-end-210928-BAOBO0003-add]//
//[-start-210929-QINGLIN0087-add]//
#ifdef S570_SUPPORT
static GLOBAL_REMOVE_IF_UNREFERENCED GPIO_INIT_CONFIG S570_LED_early_init[] =
{
  {GPIO_VER2_LP_GPP_H20, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirOut,     GpioOutLow,     GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioOutputStateUnlock}},//CPU_FN_LED_N
  {GPIO_VER2_LP_GPP_H21, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirOut,     GpioOutLow,     GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioOutputStateUnlock}},//CPU_NUM_LED_N
  {GPIO_VER2_LP_GPP_H22, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirOut,     GpioOutLow,     GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioOutputStateUnlock}},//CPU_CAPS_LED_N
};
#endif
#ifdef S370_SUPPORT
static GLOBAL_REMOVE_IF_UNREFERENCED GPIO_INIT_CONFIG S370_LED_early_init[] =
{
  {GPIO_VER2_LP_GPP_H20, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirOut,     GpioOutLow,     GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioOutputStateUnlock}},//CPU_FN_LED_N
  {GPIO_VER2_LP_GPP_H22, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirOut,     GpioOutLow,     GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioOutputStateUnlock}},//CPU_CAPS_LED_N
  {GPIO_VER2_LP_GPP_D4,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirOut,     GpioOutLow,     GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioOutputStateUnlock}},//CPU_NUM_LED_N
};
#endif

//[-end-210929-QINGLIN0087-add]//
//[-start-211009-YUNLEI0140-add]//
#ifdef C770_SUPPORT
static GLOBAL_REMOVE_IF_UNREFERENCED GPIO_INIT_CONFIG C770_14_LED_early_init[] =
{
  {GPIO_VER2_LP_GPP_H20, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirOut,     GpioOutLow,     GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//CPU_FN_LED_N
  {GPIO_VER2_LP_GPP_H22, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirOut,     GpioOutLow,     GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//CPU_CAPS_LED_N
};

static GLOBAL_REMOVE_IF_UNREFERENCED GPIO_INIT_CONFIG C770_16_LED_early_init[] =
{
  {GPIO_VER2_LP_GPP_H20, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirOut,     GpioOutLow,     GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//CPU_FN_LED_N
  {GPIO_VER2_LP_GPP_H22, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirOut,     GpioOutLow,     GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//CPU_CAPS_LED_N
  {GPIO_VER2_LP_GPP_D4,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirOut,     GpioOutLow,     GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//CPU_NUM_LED_N
};

#endif
//[-end-211009-YUNLEI0140-add]//
//[-start-211011-TAMT000025-add]//
#if defined(S77014_SUPPORT) || defined(S77014IAH_SUPPORT)
static GLOBAL_REMOVE_IF_UNREFERENCED GPIO_INIT_CONFIG S77014_LED_early_init[] =
{
  {GPIO_VER2_LP_GPP_H20, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirOut,     GpioOutHigh,    GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//CPU_FN_LED_N
  {GPIO_VER2_LP_GPP_H22, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirOut,     GpioOutHigh,    GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//CPU_CAPS_LED_N
};
#endif
//[-start-220107-TAMT000041-remove]//
//#ifdef S77013_SUPPORT
//static GLOBAL_REMOVE_IF_UNREFERENCED GPIO_INIT_CONFIG S77013_LED_early_init[] =
//{
//  {GPIO_VER2_LP_GPP_H20, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirOut,     GpioOutLow,    GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//CPU_FN_LED_N
//  {GPIO_VER2_LP_GPP_H22, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirOut,     GpioOutLow,    GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//CPU_CAPS_LED_N
//};
//#endif
//[-end-220107-TAMT000041-remove]//
//[-end-211011-TAMT000025-add]//

/**
  Alderlake P boards configuration init function for PEI pre-memory phase.

  @retval EFI_SUCCESS             The function completed successfully.
**/
EFI_STATUS
EFIAPI
AdlPInitPreMem (
  VOID
  )
{
  EFI_STATUS                        Status;
  UINTN                             VariableSize;
  VOID                              *MemorySavedData;
  UINT8                             MorControl;
  VOID                              *MorControlPtr;
  EFI_PEI_READ_ONLY_VARIABLE2_PPI   *VariableServices;

  Status = PeiServicesLocatePpi (
             &gEfiPeiReadOnlyVariable2PpiGuid,
             0,
             NULL,
             (VOID **) &VariableServices
             );
  ASSERT_EFI_ERROR (Status);
  //
  // Initialize S3 Data variable (S3DataPtr). It may be used for warm and fast boot paths.
  //
  VariableSize = 0;
  MemorySavedData = NULL;
  Status = VariableServices->GetVariable (
                               VariableServices,
                               L"MemoryConfig",
                               &gFspNonVolatileStorageHobGuid,
                               NULL,
                               &VariableSize,
                               MemorySavedData
                               );
  if (Status == EFI_BUFFER_TOO_SMALL) {
    //
    // Set the DISB bit
    // after memory Data is saved to NVRAM.
    //
    PmcSetDramInitScratchpad ();
  }

  //
  // MOR
  //
  MorControl = 0;
  MorControlPtr = &MorControl;
  VariableSize = sizeof (MorControl);
  Status = PeiGetVariable (
             MEMORY_OVERWRITE_REQUEST_VARIABLE_NAME,
             &gEfiMemoryOverwriteControlDataGuid,
             &MorControlPtr,
             &VariableSize
             );
  DEBUG ((DEBUG_INFO, "MorControl - 0x%x (%r)\n", MorControl, Status));
  if (MOR_CLEAR_MEMORY_VALUE (MorControl)) {
    PcdSet8S (PcdCleanMemory, MorControl & MOR_CLEAR_MEMORY_BIT_MASK);
  }

  PcdSet32S (PcdStackBase, PcdGet32 (PcdTemporaryRamBase) + PcdGet32 (PcdTemporaryRamSize) - (PcdGet32 (PcdFspTemporaryRamSize) + PcdGet32 (PcdFspReservedBufferSize)));
  PcdSet32S (PcdStackSize, PcdGet32 (PcdFspTemporaryRamSize));

  PcdSet8S (PcdCpuRatio, 0x0);
  PcdSet8S (PcdBiosGuard, 0x0);

  return EFI_SUCCESS;
}

/**
  Updates the wakeupType.
**/
VOID
AdlPWakeUpTypeUpdate (
  VOID
  )
{
  UINT8   WakeupType;
  //
  // Updates the wakeupType which will be used to update the same in Smbios table 01
  //
  GetWakeupEvent (&WakeupType);
  PcdSet8S (PcdWakeupType, WakeupType);
}

VOID
AdlPMrcConfigInit (
  VOID
  );

VOID
AdlPSaMiscConfigInit (
  VOID
  );

VOID
AdlPSaGpioConfigInit (
  VOID
  );

VOID
AdlPSaDisplayConfigInit (
  VOID
  );

VOID
AdlPSaUsbConfigInit (
  VOID
  );

EFI_STATUS
AdlPRootPortClkInfoInit (
  VOID
  );

EFI_STATUS
AdlPUsbConfigInit (
  VOID
  );

VOID
AdlPGpioTablePreMemInit(
  VOID
  );

VOID
AdlPGpioGroupTierInit (
  VOID
  );

/**
  HSIO init function for PEI pre-memory phase.
**/
VOID
AdlPHsioInit (
  VOID
  )
{
}

/**
  Configure Super IO
**/
STATIC
VOID
AdlPSioInit (
  VOID
  )
{
  //
  // Program and Enable Default Super IO Configuration Port Addresses and range
  //
  PchLpcGenIoRangeSet (PcdGet16 (PcdLpcSioConfigDefaultPort) & (~0xF), 0x10);

  //
  // Enable LPC decode for KCS and mailbox SIO for iBMC communication
  //
  if (PcdGet8 (PcdPlatformFlavor) == FlavorUpServer) {
    PchLpcGenIoRangeSet (BMC_KCS_BASE_ADDRESS, 0x10);
    PchLpcGenIoRangeSet (PILOTIII_MAILBOX_BASE_ADDRESS, 0x10);
  } else {
  //
  // 128 Byte Boundary and SIO Runtime Register Range is 0x0 to 0xF;
  //
    PchLpcGenIoRangeSet (SIO_RUNTIME_REG_BASE_ADDRESS  & (~0x7F), 0x10);
  }

  //
  // We should not depend on SerialPortLib to initialize KBC for legacy USB
  // So initialize KBC for legacy USB driver explicitly here.
  // After we find how to enable mobile KBC, we will add enabling code for mobile then.
  //
  if ((PcdGet8 (PcdPlatformFlavor) == FlavorDesktop) ||
      (PcdGet8 (PcdPlatformFlavor) == FlavorWorkstation)) {
    //
    // Enable KBC for National PC8374 SIO
    //
    if (PcdGetBool (PcdPc8374SioKbcPresent)) {
      IoWrite8 (PcdGet16 (PcdDesktopLpcSioIndexDefaultPort), 0x07);
      IoWrite8 (PcdGet16 (PcdDesktopLpcSioDataDefaultPort), 0x06);
      IoWrite8 (PcdGet16 (PcdDesktopLpcSioIndexDefaultPort), 0x30);
      IoWrite8 (PcdGet16 (PcdDesktopLpcSioDataDefaultPort), 0x01);
    }
  }

  return;
}

/**
  Notifies the gPatchConfigurationDataPreMemPpiGuid has been Installed

  @param[in] PeiServices          General purpose services available to every PEIM.
  @param[in] NotifyDescriptor     The notification structure this PEIM registered on install.
  @param[in] Ppi                  The memory discovered PPI.  Not used.

  @retval EFI_SUCCESS             The function completed successfully.
**/
EFI_STATUS
EFIAPI
AdlPBoardPatchConfigurationDataPreMemCallback (
  IN EFI_PEI_SERVICES           **PeiServices,
  IN EFI_PEI_NOTIFY_DESCRIPTOR  *NotifyDescriptor,
  IN VOID                       *Ppi
  )
{
//[-start-200225-IB14630334-remove]//
//  UINTN   ConfigPatchStructSize;
//  UINT16    BoardId;
//  BoardId = PcdGet16(PcdBoardId);
//
//  switch (BoardId) {
//    case BoardIdAdlPLp4Rvp:
//      ConfigPatchStructSize = SIZE_OF_TABLE (mAlderLakePRvpConfigPatchStruct, CONFIG_PATCH_STRUCTURE);
//      PatchSetupConfigurationDataPreMem (mAlderLakePRvpConfigPatchStruct, ConfigPatchStructSize);
//      break;

//    case BoardIdAdlPDdr5Dg384Aep:
//    case BoardIdAdlPLp5Dg128Aep:
//    case BoardIdAdlPMMAep:
//      ConfigPatchStructSize = SIZE_OF_TABLE(mAlderLakePDgAepConfigPatchStruct, CONFIG_PATCH_STRUCTURE);
//      PatchSetupConfigurationDataPreMem(mAlderLakePDgAepConfigPatchStruct, ConfigPatchStructSize);
//    case BoardIdAdlPLp5Aep:
//    case BoardIdAdlPLp4Bep:
//    case BoardIdAdlPLp5MbAep:
//      ConfigPatchStructSize = SIZE_OF_TABLE (mAlderLakePRvpConfigPatchStruct, CONFIG_PATCH_STRUCTURE);
//      PatchSetupConfigurationDataPreMem (mAlderLakePRvpConfigPatchStruct, ConfigPatchStructSize);
//      ConfigPatchStructSize = SIZE_OF_TABLE (mAlderLakePAepConfigPatchStruct, CONFIG_PATCH_STRUCTURE);
//      PatchSetupConfigurationDataPreMem (mAlderLakePAepConfigPatchStruct, ConfigPatchStructSize);
//      break;
//
//    case BoardIdAdlPDdr5MRRvp:
//      ConfigPatchStructSize = SIZE_OF_TABLE (mAlderLakePRvpConfigPatchStruct, CONFIG_PATCH_STRUCTURE);
//      PatchSetupConfigurationDataPreMem (mAlderLakePRvpConfigPatchStruct, ConfigPatchStructSize);
//      ConfigPatchStructSize = SIZE_OF_TABLE (mDiscreteThunderboltSetupEnableConfigPatchStruct, CONFIG_PATCH_STRUCTURE);
//      PatchSetupConfigurationDataPreMem (mDiscreteThunderboltSetupEnableConfigPatchStruct, ConfigPatchStructSize);
//      ConfigPatchStructSize = SIZE_OF_TABLE (mIntegratedThunderboltSetupDisableConfigPatchStruct, CONFIG_PATCH_STRUCTURE);
//      PatchSetupConfigurationDataPreMem (mIntegratedThunderboltSetupDisableConfigPatchStruct, ConfigPatchStructSize);
//      ConfigPatchStructSize = SIZE_OF_TABLE (mAlderLakePTcssUcmDisableConfigPatchStruct, CONFIG_PATCH_STRUCTURE);
//      PatchSetupConfigurationDataPreMem (mAlderLakePTcssUcmDisableConfigPatchStruct, ConfigPatchStructSize);
//      ConfigPatchStructSize = SIZE_OF_TABLE (mRp08SetupDisableConfigPatchStruct, CONFIG_PATCH_STRUCTURE);
//      PatchSetupConfigurationDataPreMem (mRp08SetupDisableConfigPatchStruct, ConfigPatchStructSize);
//      break;
//    case BoardIdAdlPLp5Rvp:
//      ConfigPatchStructSize = SIZE_OF_TABLE (mAlderLakePRvpConfigPatchStruct, CONFIG_PATCH_STRUCTURE);
//      PatchSetupConfigurationDataPreMem (mAlderLakePRvpConfigPatchStruct, ConfigPatchStructSize);
//      ConfigPatchStructSize = SIZE_OF_TABLE (mAlderLakePLp5T4AuxOverrideEnableConfigPatchStruct, CONFIG_PATCH_STRUCTURE);
//      PatchSetupConfigurationDataPreMem (mAlderLakePLp5T4AuxOverrideEnableConfigPatchStruct, ConfigPatchStructSize);
//      break;
//    case BoardIdAdlPT3Lp5Rvp:
//      ConfigPatchStructSize = SIZE_OF_TABLE (mAlderLakePRvpConfigPatchStruct, CONFIG_PATCH_STRUCTURE);
//      PatchSetupConfigurationDataPreMem (mAlderLakePRvpConfigPatchStruct, ConfigPatchStructSize);
//      ConfigPatchStructSize = SIZE_OF_TABLE (mAlderLakePLp5T3AuxOverrideEnableConfigPatchStruct, CONFIG_PATCH_STRUCTURE);
//      PatchSetupConfigurationDataPreMem (mAlderLakePLp5T3AuxOverrideEnableConfigPatchStruct, ConfigPatchStructSize);
//      break;
//    case BoardIdAdlPDdr5Rvp:
//      ConfigPatchStructSize = SIZE_OF_TABLE (mAlderLakePRvpConfigPatchStruct, CONFIG_PATCH_STRUCTURE);
//      PatchSetupConfigurationDataPreMem (mAlderLakePRvpConfigPatchStruct, ConfigPatchStructSize);
//      ConfigPatchStructSize = SIZE_OF_TABLE (mAlderLakePDdr4Ddr5ConfigPatchStruct, CONFIG_PATCH_STRUCTURE);
//      PatchSetupConfigurationDataPreMem (mAlderLakePDdr4Ddr5ConfigPatchStruct, ConfigPatchStructSize);
//      ConfigPatchStructSize = SIZE_OF_TABLE (mAlderLakePTcssUcmDisableConfigPatchStruct, CONFIG_PATCH_STRUCTURE);
//      PatchSetupConfigurationDataPreMem (mAlderLakePTcssUcmDisableConfigPatchStruct, ConfigPatchStructSize);
//      break;

//    case BoardIdAdlPDdr4Rvp:
//      ConfigPatchStructSize = SIZE_OF_TABLE (mAlderLakePRvpConfigPatchStruct, CONFIG_PATCH_STRUCTURE);
//      PatchSetupConfigurationDataPreMem (mAlderLakePRvpConfigPatchStruct, ConfigPatchStructSize);
//      ConfigPatchStructSize = SIZE_OF_TABLE (mAlderLakePDdr4Ddr5ConfigPatchStruct, CONFIG_PATCH_STRUCTURE);
//      PatchSetupConfigurationDataPreMem (mAlderLakePDdr4Ddr5ConfigPatchStruct, ConfigPatchStructSize);
//      ConfigPatchStructSize = SIZE_OF_TABLE (mAlderLakePDdr4AuxOverrideEnableConfigPatchStruct, CONFIG_PATCH_STRUCTURE);
//      PatchSetupConfigurationDataPreMem (mAlderLakePDdr4AuxOverrideEnableConfigPatchStruct, ConfigPatchStructSize);
//      ConfigPatchStructSize = SIZE_OF_TABLE (mAlderLakePTcssUcmDisableConfigPatchStruct, CONFIG_PATCH_STRUCTURE);
//      PatchSetupConfigurationDataPreMem (mAlderLakePTcssUcmDisableConfigPatchStruct, ConfigPatchStructSize);
//      break;

//  case BoardIdAdlPLp5Gcs:
//    ConfigPatchStructSize = SIZE_OF_TABLE (mAlderLakePRvpConfigPatchStruct, CONFIG_PATCH_STRUCTURE);
//    PatchSetupConfigurationDataPreMem (mAlderLakePRvpConfigPatchStruct, ConfigPatchStructSize);
//    ConfigPatchStructSize = SIZE_OF_TABLE (mAlderLakePLp5GcsConfigPatchStruct, CONFIG_PATCH_STRUCTURE);
//    PatchSetupConfigurationDataPreMem (mAlderLakePLp5GcsConfigPatchStruct, ConfigPatchStructSize);
//    break;

//  default:
//    break;
//  }
//
//  //
//  //
//  if (IsSimicsEnvironment ()) {
//    ConfigPatchStructSize = SIZE_OF_TABLE (mAlderlakePSimicsConfigPatchStruct, CONFIG_PATCH_STRUCTURE);
//    PatchSetupConfigurationDataPreMem (mAlderlakePSimicsConfigPatchStruct, ConfigPatchStructSize);
//  }
//  if (PchStepping () >= PCH_A1) {
//    ConfigPatchStructSize = SIZE_OF_TABLE (mAlderLakePPchSteppingConfigPatchStruct, CONFIG_PATCH_STRUCTURE);
//    PatchSetupConfigurationDataPreMem (mAlderLakePPchSteppingConfigPatchStruct, ConfigPatchStructSize);
//  }
//[-end-200225-IB14630334-remove]//
  PeiServicesInstallPpi (&mSetupVariablesReadyPpi);

  return RETURN_SUCCESS;
}

/**
  Board Misc init function for PEI pre-memory phase.
**/
VOID
AdlPBoardMiscInitPreMem (
  VOID
  )
{
  PCD64_BLOB PcdData;
  UINT16    BoardId;
  BoardId = PcdGet16(PcdBoardId);

  //
  // RecoveryMode GPIO
  //
  PcdData.Blob = 0;
  PcdData.BoardGpioConfig.Type = BoardGpioTypeNotSupported;
  PcdSet64S (PcdRecoveryModeGpio, PcdData.Blob);

  //
  // OddPower Init
  //
  PcdSetBoolS (PcdOddPowerInitEnable, FALSE);

  //
  // Pc8374SioKbc Present
  //
  PcdSetBoolS (PcdPc8374SioKbcPresent, FALSE);

  //
  // Smbus Alert function Init.
  //
  PcdSetBoolS (PcdSmbusAlertEnable, FALSE);


  //
  // Configure WWAN Full Card Power Off and reset pins
  //
  switch (BoardId) {
    case BoardIdAdlPLp4Bep:
    case BoardIdAdlPLp5Aep:
    case BoardIdAdlPLp5Dg128Aep:
    case BoardIdAdlPLp5MbAep:
      PcdSet32S (PcdWwanFullCardPowerOffGpio, GPIO_VER2_LP_GPP_F17);
      PcdSet32S (PcdWwanBbrstGpio, GPIO_VER2_LP_GPP_F18);
      PcdSet32S (PcdWwanPerstGpio, GPIO_VER2_LP_GPP_C5);
      PcdSet32S (PcdWwanWakeGpio, GPIO_VER2_LP_GPP_D18);
      PcdSetBoolS (PcdWwanPerstGpioPolarity, PIN_GPIO_ACTIVE_LOW);
      PcdSetBoolS (PcdWwanFullCardPowerOffGpioPolarity, PIN_GPIO_ACTIVE_HIGH);
      PcdSetBoolS (PcdWwanBbrstGpioPolarity, PIN_GPIO_ACTIVE_LOW);
      PcdSet8S (PcdWwanSourceClock, 5);
      PcdSet8S (PcdWwanRootPortNumber, 0x06);
      break;
    case BoardIdAdlPLp5Rvp:
    case BoardIdAdlPT3Lp5Rvp:
    case BoardIdAdlPLp4Rvp:
    case BoardIdAdlPDdr5Rvp:
    case BoardIdAdlPDdr4Rvp:
      PcdSet32S (PcdWwanFullCardPowerOffGpio, GPIO_VER2_LP_GPP_F15);
      PcdSet32S (PcdWwanBbrstGpio, GPIO_VER2_LP_GPP_F14);
      PcdSet32S (PcdWwanPerstGpio, GPIO_VER2_LP_GPP_C5);
      PcdSet32S (PcdWwanWakeGpio, GPIO_VER2_LP_GPP_D18);
      PcdSetBoolS (PcdWwanPerstGpioPolarity, PIN_GPIO_ACTIVE_LOW);
      PcdSetBoolS (PcdWwanFullCardPowerOffGpioPolarity, PIN_GPIO_ACTIVE_HIGH);
      PcdSetBoolS (PcdWwanBbrstGpioPolarity, PIN_GPIO_ACTIVE_LOW);
      PcdSet8S (PcdWwanSourceClock, 5);
      PcdSet8S (PcdWwanRootPortNumber, 0x06);
      break;
    case BoardIdAdlPMMAep:
      PcdSet32S (PcdWwanFullCardPowerOffGpio, GPIO_VER2_LP_GPP_F17);
      PcdSet32S (PcdWwanBbrstGpio, GPIO_VER2_LP_GPP_F14);
      PcdSet32S (PcdWwanPerstGpio, GPIO_VER2_LP_GPP_C5);
      PcdSet32S (PcdWwanWakeGpio, GPIO_VER2_LP_GPP_A17);
      PcdSetBoolS (PcdWwanPerstGpioPolarity, PIN_GPIO_ACTIVE_LOW);
      PcdSetBoolS (PcdWwanFullCardPowerOffGpioPolarity, PIN_GPIO_ACTIVE_HIGH);
      PcdSetBoolS (PcdWwanBbrstGpioPolarity, PIN_GPIO_ACTIVE_LOW);
      PcdSet8S (PcdWwanSourceClock, 6);
      PcdSet8S (PcdWwanRootPortNumber, 0x06);
      break;
    case BoardIdAdlPDdr5Dg384Aep:
    case BoardIdAdlPLp5Gcs:
      break;
    case BoardIdAdlPDdr5MRRvp:
      PcdSet32S (PcdWwanFullCardPowerOffGpio, GPIO_VER2_LP_GPP_F15);
      PcdSet32S (PcdWwanBbrstGpio, GPIO_VER2_LP_GPP_F14);
      PcdSet32S (PcdWwanPerstGpio, GPIO_VER2_LP_GPP_C5);
      PcdSet32S (PcdWwanWakeGpio, GPIO_VER2_LP_GPP_D18);
      PcdSetBoolS (PcdWwanPerstGpioPolarity, PIN_GPIO_ACTIVE_LOW);
      PcdSetBoolS (PcdWwanFullCardPowerOffGpioPolarity, PIN_GPIO_ACTIVE_HIGH);
      PcdSetBoolS (PcdWwanBbrstGpioPolarity, PIN_GPIO_ACTIVE_LOW);
      PcdSet8S (PcdWwanSourceClock, 5);
      PcdSet8S (PcdWwanRootPortNumber, 0x03);
      break;
  }

  //EC FailSafe Cpu Temp and Fan Speed Setting
  PcdSet8S (PcdEcFailSafeActionCpuTemp, 85);
  PcdSet8S (PcdEcFailSafeActionFanPwm, 65);
}

/**
  Notify Ec To Pd firmware for enabling or disabling Pcie.
**/
VOID
AdlPNotifyEcToPdForPcieTunnel (
  VOID
  )
{
  EFI_STATUS                        Status;
  EFI_BOOT_MODE                     BootMode;
  EFI_PEI_READ_ONLY_VARIABLE2_PPI   *VariableServices;
  SETUP_DATA                        SetupData;
  UINTN                             VariableSize;


  PeiServicesGetBootMode (&BootMode);

  //
  // PD will keep original contents during Sx state.
  // It doesn't need to do anything during these states.
  //
  if ((BootMode == BOOT_ON_S3_RESUME) ||
      (BootMode == BOOT_ON_S4_RESUME) ||
      (BootMode == BOOT_ON_S5_RESUME)) {
    return;
  }

  Status = PeiServicesLocatePpi (
             &gEfiPeiReadOnlyVariable2PpiGuid,
             0,
             NULL,
             (VOID **) &VariableServices
             );
  ASSERT_EFI_ERROR (Status);

  VariableSize = sizeof (SETUP_DATA);
  Status = VariableServices->GetVariable (
                               VariableServices,
                               L"Setup",
                               &gSetupVariableGuid,
                               NULL,
                               &VariableSize,
                               (VOID *) &SetupData
                               );

  ASSERT_EFI_ERROR (Status);

  if (EFI_ERROR (Status)) {
    DEBUG ((DEBUG_ERROR, "Get Setup variable fail Status = %r\n", Status));
    return;
  }

  //
  // PCIE Tunnel En/Dis function. PD is set enable PCIE Tunnel by default.
  // Ideally BIOS send command when option is disable.
  //
  NotifyEcToPdForPcieTunnel (SetupData.EnablePcieTunnelingOverUsb4);

  return;
}

//[-start-201104-IB17510119-add]//
STATIC
EFI_STATUS
GpioCfgPei (
//[-start-210120-IB16560239-modify]//
  IN H2O_BOARD_ID               Board,
//[-end-210120-IB16560239-modify]//
  IN BOOLEAN                    PreMemFlag
  )
{
  EFI_STATUS                    Status;
  H2O_GPIO_INIT_CONFIG          *GpioInitStruct;
  UINT32                        GpioInitStructCount;
  UINT32                        GpioTableCount;
  GPIO_INIT_CONFIG              *GpioDefinitionTable;
  H2O_GPIO_INIT_CONFIG          SingleGpioInitStruct = {0};
  UINT32                        Index;
  UINT32                        PadMaxSize;
  GPIO_GROUP                    Group;

  PadMaxSize = 0;
  for (Group = GpioGetLowestGroup(); Group <= GpioGetHighestGroup(); Group ++) {
    PadMaxSize += GpioGetPadPerGroup (Group);
  }

  GpioDefinitionTable = NULL;
  GpioDefinitionTable = (GPIO_INIT_CONFIG *)AllocateZeroPool (PadMaxSize * sizeof (GPIO_INIT_CONFIG));
  if (GpioDefinitionTable == NULL) {
    return EFI_OUT_OF_RESOURCES;
  }
  //
  // Use GetGpioCfg in GpioCfgLib to get build-time GPIO configuration
  // Input Board value corresponds to Skuids in Project.dsc
  //
  Status = GetGpioCfg (Board, &GpioInitStruct, &GpioInitStructCount);
  ASSERT_EFI_ERROR (Status);
  if (EFI_ERROR (Status)) {
    return EFI_NOT_FOUND;
  }

  GpioTableCount = 0;
  Status = EFI_UNSUPPORTED;
  for (Index = 0; Index < GpioInitStructCount; Index++) {
    if (GpioInitStruct[Index].Flag != PreMemFlag) {
      continue;
    }
    CopyMem (&SingleGpioInitStruct, &GpioInitStruct[Index], sizeof(H2O_GPIO_INIT_CONFIG));
    //
    // OemServices
    // This function works when PCT being FALSE or PCT's setting being null.
    // Please refer to Power On Guide 0.4.
    //
    DEBUG ((DEBUG_INFO, "OemKernelServices Call: OemSvcUpdateGpioCfg \n"));
    Status = OemSvcUpdateGpioCfg (&SingleGpioInitStruct);
    DEBUG ((DEBUG_INFO, "OemKernelServices OemSvcUpdateGpioCfg Status: %r\n", Status));
    if (Status != EFI_SUCCESS) {
     CopyMem (&GpioDefinitionTable[GpioTableCount], &SingleGpioInitStruct, sizeof(GPIO_INIT_CONFIG));
     GpioTableCount++;
    }
  }
  if ((Status == EFI_SUCCESS) && (GpioTableCount == 0x00)) {
    return EFI_SUCCESS;
  }

  //
  // Batch process GPIO configuraions.
  //
  DEBUG ((DEBUG_INFO, "ConfigureGpio() Start\n"));
  Status = GpioConfigurePads (GpioTableCount, GpioDefinitionTable);
  DEBUG ((DEBUG_INFO, "ConfigureGpio() End\n"));

  return EFI_SUCCESS;
}
//[-end-201104-IB17510119-add]//

//[-start-210914-QINGLIN0066-add]//
//[-start-211021-JAYAN00001-modify]//
#if defined(C770_SUPPORT) || defined(S370_SUPPORT)
//[-end-211021-JAYAN00001-modify]//
BOOLEAN
IsSetupSwitchableGraphicConfig (
  VOID
  )
{
  EFI_PEI_READ_ONLY_VARIABLE2_PPI             *VariableServices;
  EFI_STATUS                                  Status;
  UINTN                                       VariableSize;
  SA_SETUP                                    SaSetup;

  //
  // Locate PEI Read Only Variable PPI
  //
  Status = PeiServicesLocatePpi (
             &gEfiPeiReadOnlyVariable2PpiGuid,
             0,
             NULL,
             (VOID **)&VariableServices
             );
  if (!EFI_ERROR (Status)) {
    //
    // Get SA Setup Variable
    //
    VariableSize = sizeof (SA_SETUP);
    Status = VariableServices->GetVariable (
                                 VariableServices,
                                 SA_SETUP_VARIABLE_NAME,
                                 &gSaSetupVariableGuid,
                                 NULL,
                                 &VariableSize,
                                 &SaSetup
                                 );
    if (!EFI_ERROR (Status) && (SaSetup.PrimaryDisplay == 4)) {
      return TRUE;
    }
  }

  return FALSE;
}
#endif
//[-end-210914-QINGLIN0066-add]//

/**
  A hook for board-specific initialization prior to memory initialization.

  @retval EFI_SUCCESS   The board initialization was successful.
**/
EFI_STATUS
EFIAPI
AdlPBoardInitBeforeMemoryInit (
  VOID
  )
{
  EFI_STATUS        Status;
//[-start-200225-IB14630334-add]//
  EFI_STATUS	      OemSvcStatus;
//[-end-200225-IB14630334-add]//
//[-start-191001-16990100-add]//
  UINT16            GpioTableSizePreMem;
  GPIO_INIT_CONFIG  *GpioTablePreMem;
//[-end-191001-16990100-add]//
//[-start-201104-IB17510119-add]//
  UINT16            BoardId;
//[-start-210908-YUNLEI0131-modify]//
#ifdef C770_SUPPORT
  UINT8             UmaAndDisType;
#endif
//[-end-210908-YUNLEI0131-modify]//
  //[-start-210914-SHAONN0007-modify]//
#ifdef S370_SUPPORT
  UINT8             UmaAndDisType;
#endif
  //[-end-210914-SHAONN0007-modify]//

  BoardId = PcdGet16(PcdBoardId);
//[-end-201104-IB17510119-add]//

  DEBUG ((DEBUG_INFO, "AdlPBoardInitBeforeMemoryInit\n"));

  //[-start-191111-IB10189001-remove]//
  // GetBiosId (NULL);
  //[-end-191111-IB10189001-remove]//

  AdlPInitPreMem ();

  AdlPWakeUpTypeUpdate ();
  AdlPBoardMiscInitPreMem ();
  AdlPSioInit ();

  ///
  /// Do basic PCH init
  ///
  SiliconInit ();

  AdlPGpioGroupTierInit ();
  AdlPGpioTablePreMemInit ();

  AdlPHsioInit ();
  AdlPMrcConfigInit ();
  AdlPSaGpioConfigInit ();
  AdlPSaMiscConfigInit ();
  Status = AdlPRootPortClkInfoInit ();
  Status = AdlPUsbConfigInit ();
  AdlPSaDisplayConfigInit ();
  AdlPSaUsbConfigInit ();
  if (PcdGetPtr (PcdBoardGpioTableEarlyPreMem) != 0) {
    GpioInit (PcdGetPtr (PcdBoardGpioTableEarlyPreMem));
    //
    // We are enabling dTBT CIO Power GPIO in EarlyPreMem GpioTable Init and dTBT SLOT Reset GPIO in PreMem GpioTable
    // init. So we have to add minimum of 10ms delay in between them as per dTBT BWG.
    //
    MicroSecondDelay (15 * 1000); // 15 ms Delay
  }
  // Configure GPIO Before Memory
//[-start-210120-IB16560239-remove]//
//  GpioInit (PcdGetPtr (PcdBoardGpioTablePreMem));
//[-end-210120-IB16560239-remove]//
//[-start-201104-IB17510119-modify]//
  if (PcdGetBool (PcdH2OGpioCfgSupported) && !PcdGetBool(PcdDisableVpdGpioTable)) {
    //
    // Program GPIOs, Input Board value corresponds to Skuids in Project.dsc
    //
    DEBUG ((DEBUG_INFO, "GpioCfgPei() Start\n"));
//[-start-210120-IB16560239-modify]//
    GpioCfgPei ((H2O_BOARD_ID)BoardId, TRUE);
//[-end-210120-IB16560239-modify]//
    DEBUG ((DEBUG_INFO, "GpioCfgPei() END\n"));
  } else {
    GpioTablePreMem = PcdGetPtr (PcdBoardGpioTablePreMem);
    GetGpioTableSize (GpioTablePreMem, &GpioTableSizePreMem);
    DEBUG_OEM_SVC ((DEBUG_INFO, "Pei OemChipsetServices Call: OemSvcModifyGpioSettingTablePreMem \n"));
    OemSvcStatus = OemSvcModifyGpioSettingTablePreMem (&GpioTablePreMem, &GpioTableSizePreMem);
    DEBUG_OEM_SVC ((DEBUG_INFO, "Pei OemChipsetServices OemSvcModifyGpioSettingTablePreMem Status: %r\n", OemSvcStatus));
    if (GpioTableSizePreMem != 0 && OemSvcStatus != EFI_SUCCESS) {
      GpioConfigurePads ((UINT32) GpioTableSizePreMem, GpioTablePreMem);
    }
  }
//[-end-201104-IB17510119-modify]//
  // Configure GPIO group GPE tier
  GpioGroupTierInit ();
//[-start-211021-JAYAN00001-modify]//
//[-start-210908-YUNLEI0131-modify]//
#ifdef C770_SUPPORT
  OemSvcLfcGetBoardID (GPU_ID, &UmaAndDisType);
  if((UmaAndDisType == GPU_ID_DIS_INTEL) && (IsSetupSwitchableGraphicConfig())) {
    MicroSecondDelay(100000);
    Status = GpioSetOutputValue(GPIO_VER2_LP_GPP_A15,0x1);
  }else {
    //GpioSetOutputValue (GPIO_VER2_LP_GPP_A15, 0x00);  // Set dGPU PST pin to low
    MicroSecondDelay(100000);
    Status = GpioSetOutputValue (GPIO_VER2_LP_GPP_E10, 0x00);  // Set dGPU PWREN pin to low
  }
#endif
//[-end-210908-YUNLEI0131-modify]//
//[-end-211021-JAYAN00001-modify]//

  // Notify Ec To Pd firmware for enabling or disabling Pcie.
  AdlPNotifyEcToPdForPcieTunnel ();

//[-start-210914-SHAONN0007-modify]//
//[-start-210914-QINGLIN0066-modify]//
#ifdef S370_SUPPORT
  OemSvcLfcGetBoardID (GPU_ID, &UmaAndDisType);
  if ((UmaAndDisType == GPU_ID_DIS_INTEL) && (IsSetupSwitchableGraphicConfig())) {
    MicroSecondDelay(100000);
    Status = GpioSetOutputValue(GPIO_VER2_LP_GPP_A15,0x1);  // Set dGPU PWREN pin to high
  } else {
    //GpioSetOutputValue (GPIO_VER2_LP_GPP_A15, 0x00);  // Set dGPU PST pin to low
    MicroSecondDelay(100000);
    Status = GpioSetOutputValue (GPIO_VER2_LP_GPP_E10, 0x00);  // Set dGPU PWREN pin to low
  }
#endif
//[-end-210914-QINGLIN0066-modify]//
//[-end-210914-SHAONN0007-modify]//
//[-start-210928-BAOBO0003-add]//
//[-start-210929-QINGLIN0087-modify]//
//#ifdef C970_SUPPORT
#if defined(C970_SUPPORT) || defined(S570_SUPPORT) || defined(S370_SUPPORT)
  UINT8    BootupStatus;
  #define  EC_BOOTUP_STATE_CMD               0x49
  Status = EcCommand (
             EC_CMD_STATE,
             EC_DATA,
             1,
             &BootupStatus,
             EC_BOOTUP_STATE_CMD,
             0);
  if (EFI_ERROR (Status)) {
    return Status;
  }
  PcdSet8S (NotifyECBootUp, BootupStatus);
  if((BootupStatus == 0)) { //BootupStatus =0 is Normal_Bootup_State
#ifdef C970_SUPPORT
    GpioConfigurePads (2, YogaC970_LED_early_init); //LED On
#endif
#ifdef S570_SUPPORT
    GpioConfigurePads (3, S570_LED_early_init); //LED On
#endif
#ifdef S370_SUPPORT
    GpioConfigurePads (3, S370_LED_early_init); //LED On
#endif
  }
#endif  
//[-end-210929-QINGLIN0087-modify]//
//[-end-210928-BAOBO0003-add]//
//[-start-211009-YUNLEI0140-add]//
#if defined(C770_SUPPORT)
  UINT8    Panel_Size	 = 0;
  UINT8    BootupStatus;
  #define  EC_BOOTUP_STATE_CMD               0x49
  Status = EcCommand (
             EC_CMD_STATE,
             EC_DATA,
             1,
             &BootupStatus,
             EC_BOOTUP_STATE_CMD,
             0);
  if (EFI_ERROR (Status)) {
    return Status;
  }
  PcdSet8S (NotifyECBootUp, BootupStatus);
  if((BootupStatus == 0)) { //BootupStatus =0 is Normal_Bootup_State
    OemSvcLfcGetBoardID(PANEL_SIZE, &Panel_Size);
    if(PANEL_SIZE_16 == Panel_Size){ 
    GpioConfigurePads (3, C770_16_LED_early_init); //LED On
    }
    else{
      GpioConfigurePads (2, C770_14_LED_early_init); //LED On
    }
  }
#endif
//[-end-211009-YUNLEI0140-add]//
//[-start-211011-TAMT000025-add]//
//[-start-211222-TAMT000025-A-midify]//
#if defined(S77014_SUPPORT) || defined(S77014IAH_SUPPORT)
  UINT8    BootupStatus;
  #define  EC_BOOTUP_STATE_CMD               0x49
  Status = EcCommand (
             EC_CMD_STATE,
             EC_DATA,
             1,
             &BootupStatus,
             EC_BOOTUP_STATE_CMD,
             0);
  if (EFI_ERROR (Status)) {
    return Status;
  }
  PcdSet8S (NotifyECBootUp, BootupStatus);
  if((BootupStatus == 0)) {//BootupStatus =0 is Normal_Bootup_State
    GpioConfigurePads (2, S77014_LED_early_init); //LED On
  }
#endif
//[-start-220107-TAMT000041-remove]//
//#if defined(S77013_SUPPORT)	
//  UINT8      BootupStatus;
//  #define    EC_BOOTUP_STATE_CMD              0x49
//  Status = EcCommand (
//              EC_CMD_STATE,
//              EC_DATA,
//              1,
//              &BootupStatus,
//              EC_BOOTUP_STATE_CMD,
//              0);
//    if (EFI_ERROR (Status)) {
//      return Status;
//    }
//    PcdSet8S (NotifyECBootUp, BootupStatus);
//    if((BootupStatus == 0)) {//BootupStatus =0 is Normal_Bootup_State
//      GpioConfigurePads (2, S77013_LED_early_init); //LED On
//    }
//#endif
//[-end-220107-TAMT000041-remove]//
//[-end-211222-TAMT000025-A-midify]//
//[-end-211011-TAMT000025-add]//
  ASSERT(Status == EFI_SUCCESS);
  return EFI_SUCCESS;
}

/**
  A hook for board-specific initialization after memory initialization.

  @retval EFI_SUCCESS   The board initialization was successful.
**/
EFI_STATUS
EFIAPI
AdlPBoardInitAfterMemoryInit(
  VOID
  )
{
  DEBUG ((DEBUG_INFO, "AdlPBoardInitAfterMemoryInit\n"));

  return MmioInit ();
}

/**
  This board service initializes board-specific debug devices.

  @retval EFI_SUCCESS   Board-specific debug initialization was successful.
**/
EFI_STATUS
EFIAPI
AdlPBoardDebugInit (
  VOID
  )
{
  DEBUG ((DEBUG_INFO, "AdlPBoardDebugInit\n"));

  return EFI_SUCCESS;
}

/**
  This board service detects the boot mode.

  @retval EFI_BOOT_MODE The boot mode.
**/
EFI_BOOT_MODE
EFIAPI
AdlPBoardBootModeDetect (
  VOID
  )
{
  EFI_BOOT_MODE                             BootMode;

  DEBUG ((DEBUG_INFO, "AdlPBoardBootModeDetect\n"));
  BootMode = DetectBootMode ();
  DEBUG ((DEBUG_INFO, "BootMode: 0x%02X\n", BootMode));

  return BootMode;
}
