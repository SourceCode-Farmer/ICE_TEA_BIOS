/** @file
 
;******************************************************************************
;* Copyright 2021 Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corp.
;*
;******************************************************************************
*/
/** @file

@copyright
  INTEL CONFIDENTIAL
  Copyright 2020-2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains a 'Sample Driver' and is licensed as such under the terms
  of your license agreement with Intel or your vendor. This file may be modified
  by the user, subject to the additional terms of the license agreement.

@par Specification Reference:
**/

#include <PiPei.h>
#include <Library/PcdLib.h>
#include <Library/DebugLib.h>
#include <Library/EcMiscLib.h>
#include <PlatformBoardType.h>
#include <Library/PeiDxeBoardIdsLib.h>
#include <PlatformBoardId.h>
#include <PlatformBoardConfig.h>
#include <Pins/GpioPinsVer2Lp.h>
#include <Library/BoardConfigLib.h>

BOOLEAN
IsAdlP (
  VOID
  )
{
  UINT16          BoardId;
   BoardId = PcdGet16 (PcdBoardId);
  if (BoardId == 0) {
    DEBUG ((DEBUG_INFO, "Let's get Board information first ...\n"));
    GetBoardConfig ();
    BoardId = PcdGet16 (PcdBoardId);
  }
  switch (BoardId) {
    case BoardIdAdlPLp5Rvp:
    case BoardIdAdlPT3Lp5Rvp:
    case BoardIdAdlPLp4Rvp:
    case BoardIdAdlPDdr5Rvp:
    case BoardIdAdlPDdr4Rvp:
    case BoardIdAdlPLp4Bep:
    case BoardIdAdlPDdr5Dg384Aep:
    case BoardIdAdlPLp5Aep:
    case BoardIdAdlPLp5Dg128Aep:
    case BoardIdAdlPLp5Gcs:
    case BoardIdAdlPMMAep:
    case BoardIdAdlPLp5MbAep:
      DEBUG ((DEBUG_INFO, "AlderLake P Board detected\n"));
      PcdSet32S (PcdBoardRetimerForcePwrGpio, (UINTN) GPIO_VER2_LP_GPP_E4);
      // set sku type to ADL P
      PcdSet8S (PcdSkuType, AdlPSkuType);
      return TRUE;
      break;
    case BoardIdAdlPDdr5MRRvp:
      DEBUG ((DEBUG_INFO, "AlderLake P 2MR Board detected\n"));
      // set sku type to ADL P
      PcdSet8S (PcdSkuType, AdlPSkuType);
      return TRUE;
      break;
    default:
      return FALSE;
  }
}

EFI_STATUS
EFIAPI
AdlPBoardDetect (
  VOID
  )
{
  UINTN          SkuId;
  SkuId      = 0;

//[-start-201027-IB02950666-modify]//
  if (PcdGet16 (PcdCrbSkuId) != 0) {
    //
    // Customer assiged reference CRB SkuId, Here will sync it with RC Skutype PCD. 
    //
    SkuId = (UINTN) PcdGet16 (PcdCrbSkuId);
    PcdSet8S (PcdSkuType, (UINT8) SkuId);  
    return EFI_SUCCESS;
  }
//[-end-201027-IB02950666-modify]//

  DEBUG ((DEBUG_INFO, "AlderLakeP Board Detection Callback\n"));

  if (IsAdlP ()) {
//[-start-201027-IB02950666-add]//
    SkuId = (UINTN) PcdGet8(PcdSkuType);
    PcdSet16S (PcdCrbSkuId, (UINT16) SkuId);
//[-start-201102-IB02950667-modify]//
    DEBUG ((DEBUG_INFO, "SKU_ID: 0x%x\n", PcdGet16 (PcdCrbSkuId)));
//[-end-201102-IB02950667-modify]//
//[-end-201027-IB02950666-add]//
    SkuId = (UINTN) (PcdGet16 (PcdBoardBomId) << 16) | (PcdGet16 (PcdBoardRev) << 8) | (PcdGet16 (PcdBoardId));
    LibPcdSetSku (SkuId);
    DEBUG ((DEBUG_INFO, "SKU_ID: 0x%x\n", LibPcdGetSku()));
  }
  return EFI_SUCCESS;
}
