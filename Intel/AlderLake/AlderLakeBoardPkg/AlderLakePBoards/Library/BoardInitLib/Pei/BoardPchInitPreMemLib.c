/** @file
 Source code for the board PCH configuration Pcd init functions for Pre-Memory Init phase.

@copyright
  INTEL CONFIDENTIAL
  Copyright 2020 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains a 'Sample Driver' and is licensed as such under the terms
  of your license agreement with Intel or your vendor. This file may be modified
  by the user, subject to the additional terms of the license agreement.

@par Specification Reference:
**/

#include <Uefi.h>
#include <Library/BaseLib.h>
#include <Library/PcdLib.h>
#include <Library/MemoryAllocationLib.h>
#include <Library/DebugLib.h>
#include <Library/BoardConfigLib.h>

#include <Library/PreSiliconEnvDetectLib.h>
#include <Include/PlatformBoardId.h>
#include <PlatformBoardConfig.h>
#include <GpioTableAdlPPreMem.h>
#include <Library/PcdLib.h>
#include <PlatformBoardId.h>
#include <Library/PchInfoLib.h>

//[start-210726-STORM1100-modify]//
#ifdef C770_SUPPORT

#define GPU_ID_DIS_INTEL                     0x4
#define GPU_ID_UMA_ONLY                      0x0

//[-start-210728-YUNLEI0116-modify]//
static GLOBAL_REMOVE_IF_UNREFERENCED GPIO_INIT_CONFIG YogaC770_GPIO_Table_early_init[] =
{
  {GPIO_VER2_LP_GPP_F15,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirIn,      GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//Board ID12
  {GPIO_VER2_LP_GPP_F16,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirIn,      GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//Board ID13
  {GPIO_VER2_LP_GPP_R6,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirIn,      GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//Board ID5
  {GPIO_VER2_LP_GPP_R7,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirIn,      GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//Board ID6
};
//[-end-210728-YUNLEI0116-modify]//

/**
Use to get GPUID to load different PcieClk clkreq
**/
EFI_STATUS
LcfcGpioGetGPUID(
    OUT UINT8      *GPUID
){
  UINT32  BoardID12, BoardID13;

//[-start-210728-YUNLEI0116-modify]//
  GpioConfigurePads (4, YogaC770_GPIO_Table_early_init);
//[-end-210728-YUNLEI0116-modify]//


// baord ID, 12:13
// 00: UMA
// 01: DIS SUMSANG
// 10: DIS HYNIX
  GpioGetInputValue (GPIO_VER2_LP_GPP_F15, &BoardID12);
  GpioGetInputValue (GPIO_VER2_LP_GPP_F16, &BoardID13);

  if(0x01 == (BoardID12 ^ BoardID13 )){
    *GPUID = GPU_ID_DIS_INTEL;
  } else {
    *GPUID = GPU_ID_UMA_ONLY;
  }

  DEBUG ((DEBUG_INFO, "KEBIN dbg boardid 12 13 value = 0x%x, 0x%x\n",BoardID12, BoardID13));
  DEBUG ((DEBUG_INFO, "KEBIN dbg2 GPUID value = 0x%x\n",*GPUID));

  return EFI_SUCCESS;
};

//[-start-210728-YUNLEI0116-modify]//
/**
Use to get size to load different USB configuration
**/
//kebin poweron
EFI_STATUS
LcfcGpioGetMachineSizepreme(
    OUT UINT8      *MachineSize
){
  UINT32  BoardID5, BoardID6;
  UINT8   Data8 = 0;
  GpioGetInputValue(GPIO_VER2_LP_GPP_R6,&BoardID5);
  GpioGetInputValue(GPIO_VER2_LP_GPP_R7,&BoardID6);

  Data8 |= (UINT8)(BoardID6 << 0);
  Data8 |= (UINT8)(BoardID5 << 1);
  DEBUG ((DEBUG_INFO, "load different USB configuration Machine Size = 0x%x\n",Data8));
  *MachineSize = Data8;
   DEBUG ((DEBUG_INFO, "load different USB configuration MachineSize = 0x%x\n",*MachineSize));
  return EFI_SUCCESS;
};
//[-end-210728-YUNLEI0116-modify]//
#endif
//[-start-210927-TAMT000015-add]//
#if defined(S77014_SUPPORT) || defined(S77014IAH_SUPPORT)
#define GPU_ID_UMA_ONLY 				 0x0
#define GPU_ID_DIS_NVIDIA				 0x1

static GLOBAL_REMOVE_IF_UNREFERENCED GPIO_INIT_CONFIG YogaS77014_GPIO_Table_early_init[] =
{
  {GPIO_VER2_LP_GPP_F15, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirIn,      GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//Board ID12 
  {GPIO_VER2_LP_GPP_F16, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirIn,      GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//Board ID13
};

/**
Use to get GPUID to load different PcieClk clkreq
**/
EFI_STATUS
LcfcGpioGetGPUID(
  OUT UINT8      *GPUID
  )
{
  UINT32  BoardID12, BoardID13;

  GpioConfigurePads (2, YogaS77014_GPIO_Table_early_init);
#if defined(S77014_SUPPORT)
// baord ID, 12:13
// 00: UMA
// 01: DIS GN18-S5
// 10: DIS GN20-S5
// 11: DIS GN20-S7
  GpioGetInputValue (GPIO_VER2_LP_GPP_F15, &BoardID12);
  GpioGetInputValue (GPIO_VER2_LP_GPP_F16, &BoardID13);

  if((BoardID12 == 0) && (BoardID13 == 0)){
    *GPUID = GPU_ID_UMA_ONLY;
  } else {
    *GPUID = GPU_ID_DIS_NVIDIA;
  }
#endif

#if defined(S77014IAH_SUPPORT)
// baord ID, 12:13
// 00: H45 PCR i7 UMA
// 01: H45IAH UMA
// 10: H45IAH N18
// 11: H45IAH N20
  GpioGetInputValue (GPIO_VER2_LP_GPP_F15, &BoardID12);
  GpioGetInputValue (GPIO_VER2_LP_GPP_F16, &BoardID13);

  if(((BoardID12 == 0) && (BoardID13 == 0)) || ((BoardID12 == 0) && (BoardID13 == 1))){
    *GPUID = GPU_ID_UMA_ONLY;
  } else {
    *GPUID = GPU_ID_DIS_NVIDIA;
  }
#endif

  DEBUG ((DEBUG_INFO, "KEBIN dbg boardid 12 13 value = 0x%x, 0x%x\n",BoardID12, BoardID13));
  DEBUG ((DEBUG_INFO, "KEBIN dbg2 GPUID value = 0x%x\n",*GPUID));

  return EFI_SUCCESS;
};
#endif
//[-end-210927-TAMT000015-modify]//
//[end-210726-STORM1100-modify]//

/**
  Board Root Port Clock Info configuration init function for PEI pre-memory phase.

  @retval EFI_SUCCESS   The function completed successfully.
**/
EFI_STATUS
AdlPRootPortClkInfoInit (
  VOID
  )
{
  PCD64_BLOB Clock[PCH_MAX_PCIE_CLOCKS];
  UINT32 Index;
  PCIE_CLOCKS_USAGE *PcieClocks;
//[start-210726-STORM1100-modify]//
#ifdef C770_SUPPORT
  UINT8     GPUID = 0;
#endif
//[end-210726-STORM1100-modify]//

  PcieClocks = NULL;

  //
  //The default clock assignment will be NOT_USED, which corresponds to PchClockUsageNotUsed. This will prevent clocks drawing Power by default. 
  //If Platform code doesn't contain port-clock map for a given board, the clocks will be NOT_USED, preventing PCIe devices not to operate. 
  //To prevent this, remember to provide port-clock map for every board.
  //
  for (Index = 0; Index < PCH_MAX_PCIE_CLOCKS; Index++) {
    Clock[Index].PcieClock.ClkReqSupported = TRUE;
    Clock[Index].PcieClock.ClockUsage = NOT_USED;
  }

  ///
  /// Assign ClkReq signal to root port. (Base 0)
  /// For LP, Set 0 - 5
  /// For H,  Set 0 - 15
  /// Note that if GbE is enabled, ClkReq assigned to GbE will not be available for Root Port.
  ///

  PcieClocks = PcdGetPtr(VpdPcdPcieClkUsageMap);

//[start-210726-STORM1100-modify]//

#ifdef C770_SUPPORT

  LcfcGpioGetGPUID(&GPUID);
  if(GPU_ID_DIS_INTEL == GPUID) {
    PcieClocks = PcdGetPtr(VpdC770DISPcdPcieClkUsageMap);
  } else {
    PcieClocks = PcdGetPtr(VpdC770UMAPcdPcieClkUsageMap);
  }

#endif
//[end-210726-STORM1100-modify]//
//[-start-210927-TAMT000015-add]//
#if defined(S77014_SUPPORT) || defined(S77014IAH_SUPPORT)
{
    UINT8     GPUID = 0;
    LcfcGpioGetGPUID(&GPUID);
    if(GPU_ID_DIS_NVIDIA == GPUID) {
     _outp(0x72,0x70);
     _outp(0x73,0x70);
     PcieClocks = PcdGetPtr(VpdS77014DISPcdPcieClkUsageMap);
    } else {
      _outp(0x72,0x71);
      _outp(0x73,0x71);
      PcieClocks = PcdGetPtr(VpdS77014UMAPcdPcieClkUsageMap);
    }
}  
#endif
//[-end-210927-TAMT000015-add]//

  Clock[0].PcieClock.ClockUsage  = PcieClocks->ClockUsage[0];
  Clock[1].PcieClock.ClockUsage  = PcieClocks->ClockUsage[1];
  Clock[2].PcieClock.ClockUsage  = PcieClocks->ClockUsage[2];
  Clock[3].PcieClock.ClockUsage  = PcieClocks->ClockUsage[3];
  Clock[4].PcieClock.ClockUsage  = PcieClocks->ClockUsage[4];
  Clock[5].PcieClock.ClockUsage  = PcieClocks->ClockUsage[5];
  Clock[6].PcieClock.ClockUsage  = PcieClocks->ClockUsage[6];
  Clock[7].PcieClock.ClockUsage  = PcieClocks->ClockUsage[7];
  Clock[8].PcieClock.ClockUsage  = PcieClocks->ClockUsage[8];
  Clock[9].PcieClock.ClockUsage  = PcieClocks->ClockUsage[9];

  PcdSet64S (PcdPcieClock0,  Clock[ 0].Blob); // @todo Those individual PCDs should probably be replaced with something like a VOID* that can be iterated over
  PcdSet64S (PcdPcieClock1,  Clock[ 1].Blob); // @todo Those individual PCDs should probably be replaced with something like a VOID* that can be iterated over
  PcdSet64S (PcdPcieClock2,  Clock[ 2].Blob); // @todo Those individual PCDs should probably be replaced with something like a VOID* that can be iterated over
  PcdSet64S (PcdPcieClock3,  Clock[ 3].Blob); // @todo Those individual PCDs should probably be replaced with something like a VOID* that can be iterated over
  PcdSet64S (PcdPcieClock4,  Clock[ 4].Blob); // @todo Those individual PCDs should probably be replaced with something like a VOID* that can be iterated over
  PcdSet64S (PcdPcieClock5,  Clock[ 5].Blob); // @todo Those individual PCDs should probably be replaced with something like a VOID* that can be iterated over
  PcdSet64S (PcdPcieClock6,  Clock[ 6].Blob); // @todo Those individual PCDs should probably be replaced with something like a VOID* that can be iterated over
  PcdSet64S (PcdPcieClock7,  Clock[ 7].Blob); // @todo Those individual PCDs should probably be replaced with something like a VOID* that can be iterated over
  PcdSet64S (PcdPcieClock8,  Clock[ 8].Blob); // @todo Those individual PCDs should probably be replaced with something like a VOID* that can be iterated over
  PcdSet64S (PcdPcieClock9,  Clock[ 9].Blob); // @todo Those individual PCDs should probably be replaced with something like a VOID* that can be iterated over
  PcdSet64S (PcdPcieClock10, Clock[10].Blob); // @todo Those individual PCDs should probably be replaced with something like a VOID* that can be iterated over
  PcdSet64S (PcdPcieClock11, Clock[11].Blob); // @todo Those individual PCDs should probably be replaced with something like a VOID* that can be iterated over
  PcdSet64S (PcdPcieClock12, Clock[12].Blob); // @todo Those individual PCDs should probably be replaced with something like a VOID* that can be iterated over
  PcdSet64S (PcdPcieClock13, Clock[13].Blob); // @todo Those individual PCDs should probably be replaced with something like a VOID* that can be iterated over
  PcdSet64S (PcdPcieClock14, Clock[14].Blob); // @todo Those individual PCDs should probably be replaced with something like a VOID* that can be iterated over
  PcdSet64S (PcdPcieClock15, Clock[15].Blob); // @todo Those individual PCDs should probably be replaced with something like a VOID* that can be iterated over
//  PcdSet64S (PcdPcieClock16, Clock[16].Blob); // @todo Those individual PCDs should probably be replaced with something like a VOID* that can be iterated over
//  PcdSet64S (PcdPcieClock17, Clock[17].Blob); // @todo Those individual PCDs should probably be replaced with something like a VOID* that can be iterated over
  return EFI_SUCCESS;
}

/**
  Board USB related configuration init function for PEI pre-memory phase.

  @param[in]  VOID

  @retval EFI_SUCCESS   The function completed successfully.
**/
EFI_STATUS
AdlPUsbConfigInit (
  VOID
  )
{
  USB_OC_MAP_TABLE   *Usb2OcMappingTable;
  USB_OC_MAP_TABLE   *Usb3OcMappingTable;

  //
  // USB2 PHY settings.
  //
  PcdSet32S (PcdUsb2PhyTuningTable, (UINT32) &mUsb2PhyTuningTable);

  //
  // USB Port Over Current Pin
  //
//[-start-210728-YUNLEI0116-modify]//
#ifdef C770_SUPPORT
  UINT8                              MachineSize;
  LcfcGpioGetMachineSizepreme(&MachineSize);
  if(MachineSize==2)
  {
    Usb2OcMappingTable = PcdGetPtr(VpdPcd16USB2OCMap);
    Usb3OcMappingTable = PcdGetPtr(VpdPcd16USB3OCMap);
  }else{
    Usb2OcMappingTable = PcdGetPtr(VpdPcdUSB2OCMap);
    Usb3OcMappingTable = PcdGetPtr(VpdPcdUSB3OCMap);
  }
#else
  Usb2OcMappingTable = PcdGetPtr(VpdPcdUSB2OCMap);
  Usb3OcMappingTable = PcdGetPtr(VpdPcdUSB3OCMap);
#endif
//[-end-210728-YUNLEI0116-modify]//

  PcdSet32S (PcdUsb2OverCurrentPinTable, (UINT32) Usb2OcMappingTable);
  PcdSet32S (PcdUsb3OverCurrentPinTable, (UINT32) Usb3OcMappingTable);

  return EFI_SUCCESS;
}

/**
  Board GPIO Group Tier configuration init function for PEI pre-memory phase.
**/
VOID
AdlPGpioGroupTierInit (
  VOID
  )
{
  //
  // GPIO Group Tier
  //
  PcdSet32S (PcdGpioGroupToGpeDw0, 0);
  PcdSet32S (PcdGpioGroupToGpeDw1, 0);
  PcdSet32S (PcdGpioGroupToGpeDw2, 0);

  return;
}

/**
  GPIO init function for PEI pre-memory phase.

  @param[in]  BoardId   An unsigned integrer represent the board id.

  @retval EFI_SUCCESS   The function completed successfully.
**/
EFI_STATUS
AdlPGpioTablePreMemInit (
  IN UINT16 BoardId
  )
{
  GPIO_INIT_CONFIG  *GpioTable;
  GpioTable = NULL;

  BoardId = PcdGet16 (PcdBoardId);
  //if (PchStepping () < PCH_A1) {
    switch (BoardId) {
      case  SkuIdAdlPLp4Rvp:
      case  SkuIdAdlPLp5Rvp:
      case  SkuIdAdlPDdr5Rvp:
      case  SkuIdAdlPDdr4Rvp:
      case  SkuIdAdlPT3Lp5Rvp:
        GpioConfigurePads ((UINT32)(sizeof (mGpioTableAdlPOebClkEarlyPreMem) / sizeof(GPIO_INIT_CONFIG)), &(mGpioTableAdlPOebClkEarlyPreMem[0]));
        break;
      default:
        break;
    }
  //}

  //
  // GPIO Table Init, Update Early-PreMem GPIO table to PcdBoardGpioTableEarlyPreMem
  //
  switch (BoardId) {
    case  BoardIdAdlPDdr5MRRvp:
      DEBUG ((DEBUG_INFO, " BoardIdAdlPDdr5MRRvp Early Pre-mem GPIO Table Assignment\n"));
      ConfigureGpioTabletoPCD (mGpioTableAdlPDdr5MREarlyPreMem, EARLY_PRE_MEM);
      break;
    default:
      DEBUG ((DEBUG_INFO, " Early Pre-mem GPIO Table Configuration Not Supported\n"));
      break;
    }

  //
  // GPIO Table Init, Update PreMem GPIO table to PcdBoardGpioTablePreMem
  //


  switch (BoardId) {
    case BoardIdAdlPLp5Aep:
    case BoardIdAdlPLp5Dg128Aep:
    case BoardIdAdlPLp5MbAep:
      PcdSet32S (PcdBoardGpioTableWwanOnEarlyPreMem, (UINTN) mGpioTableAdlPLp5AepWwanOnEarlyPreMem);
      PcdSet16S (PcdBoardGpioTableWwanOnEarlyPreMemSize, sizeof (mGpioTableAdlPLp5AepWwanOnEarlyPreMem) / sizeof (GPIO_INIT_CONFIG));
      PcdSet32S (PcdBoardGpioTableWwanOffEarlyPreMem, (UINTN) mGpioTableAdlPLp4BepWwanOffEarlyPreMem);
      PcdSet16S (PcdBoardGpioTableWwanOffEarlyPreMemSize, sizeof (mGpioTableAdlPLp4BepWwanOffEarlyPreMem) / sizeof (GPIO_INIT_CONFIG));
      break;
    case BoardIdAdlPMMAep:
      PcdSet32S (PcdBoardGpioTableWwanOnEarlyPreMem, (UINTN) mGpioTableAdlPMMAepWwanOnEarlyPreMem);
      PcdSet16S (PcdBoardGpioTableWwanOnEarlyPreMemSize, sizeof (mGpioTableAdlPMMAepWwanOnEarlyPreMem) / sizeof (GPIO_INIT_CONFIG));
      PcdSet32S (PcdBoardGpioTableWwanOffEarlyPreMem, (UINTN) mGpioTableAdlPMMAepWwanOffEarlyPreMem);
      PcdSet16S (PcdBoardGpioTableWwanOffEarlyPreMemSize, sizeof (mGpioTableAdlPMMAepWwanOffEarlyPreMem) / sizeof (GPIO_INIT_CONFIG));
      break;
    case BoardIdAdlPLp4Bep:
      PcdSet32S (PcdBoardGpioTableWwanOnEarlyPreMem, (UINTN) mGpioTableAdlPLp4BepWwanOnEarlyPreMem);
      PcdSet16S (PcdBoardGpioTableWwanOnEarlyPreMemSize, sizeof (mGpioTableAdlPLp4BepWwanOnEarlyPreMem) / sizeof (GPIO_INIT_CONFIG));
      PcdSet32S (PcdBoardGpioTableWwanOffEarlyPreMem, (UINTN) mGpioTableAdlPLp4BepWwanOffEarlyPreMem);
      PcdSet16S (PcdBoardGpioTableWwanOffEarlyPreMemSize, sizeof (mGpioTableAdlPLp4BepWwanOffEarlyPreMem) / sizeof (GPIO_INIT_CONFIG));
      break;
    case BoardIdAdlPDdr5Dg384Aep:
    case BoardIdAdlPLp5Gcs:
      break;
    default:
      DEBUG ((DEBUG_INFO, " GpioTablePreMemAdlPLp4 Update\n"));
      PcdSet32S (PcdBoardGpioTableWwanOnEarlyPreMem, (UINTN) mGpioTableAdlPLp4RvpWwanOnEarlyPreMem);
      PcdSet16S (PcdBoardGpioTableWwanOnEarlyPreMemSize, sizeof (mGpioTableAdlPLp4RvpWwanOnEarlyPreMem) / sizeof (GPIO_INIT_CONFIG));
      PcdSet32S (PcdBoardGpioTableWwanOffEarlyPreMem, (UINTN) mGpioTableAdlPLp4RvpWwanOffEarlyPreMem);
      PcdSet16S (PcdBoardGpioTableWwanOffEarlyPreMemSize, sizeof (mGpioTableAdlPLp4RvpWwanOffEarlyPreMem) / sizeof (GPIO_INIT_CONFIG));
      PcdSet32S (PcdBoardGpioTableM80WwanOnEarlyPreMem, (UINTN) mGpioTableAdlPM80WwanEarlyPreMem);
      PcdSet16S (PcdBoardGpioTableM80WwanOnEarlyPreMemSize, sizeof (mGpioTableAdlPM80WwanEarlyPreMem) / sizeof (GPIO_INIT_CONFIG));
      break;
  }
  GpioTable = (GPIO_INIT_CONFIG *)PcdGetPtr(VpdPcdBoardGpioTablePreMem);
  ConfigureGpioTabletoPCD (GpioTable , PRE_MEM);

  return EFI_SUCCESS;
}
