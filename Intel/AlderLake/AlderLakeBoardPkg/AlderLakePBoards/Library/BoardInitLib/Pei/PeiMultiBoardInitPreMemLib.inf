## @file
#
#******************************************************************************
#* Copyright (c) 2020, Insyde Software Corporation. All Rights Reserved.
#*
#* You may not reproduce, distribute, publish, display, perform, modify, adapt,
#* transmit, broadcast, present, recite, release, license or otherwise exploit
#* any part of this publication in any form, by any means, without the prior
#* written permission of Insyde Software Corporation.
#*
#******************************************************************************
## @file
# Component information file for PEI Alderlake P Board Init Pre-Mem Library
#
#@copyright
#  INTEL CONFIDENTIAL
#  Copyright 2020 - 2021 Intel Corporation.
#
#  The source code contained or described herein and all documents related to the
#  source code ("Material") are owned by Intel Corporation or its suppliers or
#  licensors. Title to the Material remains with Intel Corporation or its suppliers
#  and licensors. The Material may contain trade secrets and proprietary and
#  confidential information of Intel Corporation and its suppliers and licensors,
#  and is protected by worldwide copyright and trade secret laws and treaty
#  provisions. No part of the Material may be used, copied, reproduced, modified,
#  published, uploaded, posted, transmitted, distributed, or disclosed in any way
#  without Intel's prior express written permission.
#
#  No license under any patent, copyright, trade secret or other intellectual
#  property right is granted to or conferred upon you by disclosure or delivery
#  of the Materials, either expressly, by implication, inducement, estoppel or
#  otherwise. Any license under such intellectual property rights must be
#  express and approved by Intel in writing.
#
#  Unless otherwise agreed by Intel in writing, you may not remove or alter
#  this notice or any other notice embedded in Materials by Intel or
#  Intel's suppliers or licensors in any way.
#
#  This file contains a 'Sample Driver' and is licensed as such under the terms
#  of your license agreement with Intel or your vendor. This file may be modified
#  by the user, subject to the additional terms of the license agreement.
#
# @par Specification Reference:
#
# @par Glossary:
###

[Defines]
  INF_VERSION                    = 0x00010005
  BASE_NAME                      = PeiAdlPMultiBoardInitPreMemLib
  FILE_GUID                      = EA05BD43-136F-45EE-BBBA-27D75817574F
  MODULE_TYPE                    = BASE
  VERSION_STRING                 = 1.0
  LIBRARY_CLASS                  = NULL
  CONSTRUCTOR                    = PeiAdlPMultiBoardInitPreMemLibConstructor

[LibraryClasses]
  BaseLib
  #[-start-191111-IB10189001-remove]#
  # BiosIdLib
  #[-end-191111-IB10189001-remove]#
  DebugLib
  BaseMemoryLib
  MemoryAllocationLib
  PcdLib
  SiliconInitLib
  MultiBoardInitSupportLib
  PeiLib
  EcMiscLib
  MmioInitLib
  BoardConfigLib
  BoardIdsLib
  PeiBootModeLib
  WakeupEventLib
  PreSiliconEnvDetectLib
  SetupInitLib
  SiliconInitLib
  PchInfoLib
#[-start-191001-16990100-add]#
  PeiOemSvcChipsetLibDefault
#[-end-191001-16990100-add]#
#[-start-200220-IB14630326-add]#
  MultiBoardSupportLib
#[-end-200220-IB14630326-add]#
#[-start-201104-IB17510119-add]#
  GpioLib
  GpioCfgLib
  PeiOemSvcKernelLibDefault
#[-end-201104-IB17510119-add]#
#[-start-210908-YUNLEI0131-modify]
#[-start-210914-SHAONN0007-modify]//
!if ($(C770_SUPPORT_ENABLE) == YES) OR ($(S370_SUPPORT_ENABLE) == YES)
  LfcProjectPeiLib
!endif
#[-end-210914-SHAONN0007-modify]//
#[-end-210908-YUNLEI0131-modify]
#[-start-210928-BAOBO0003-add]#  
  LfcEcLib
#[-end-210928-BAOBO0003-add]#    
[Packages]
  MinPlatformPkg/MinPlatformPkg.dec
  AlderLakeBoardPkg/BoardPkg.dec
  MdePkg/MdePkg.dec
  MdeModulePkg/MdeModulePkg.dec
  IntelFsp2Pkg/IntelFsp2Pkg.dec
  ClientOneSiliconPkg/SiPkg.dec
  BoardModulePkg/BoardModulePkg.dec
  AlderLakePlatSamplePkg/PlatformPkg.dec
#[-start-191001-16990100-add]#
  SioDummyPkg/SioDummyPkg.dec
#[-end-191001-16990100-add]#
#[-start-191218-IB16740000-add]#
  $(CHIPSET_PKG)/$(CHIPSET_PKG).dec
#[-end-191218-IB16740000-add]#
#[-start-201104-IB17510119-add]#
  InsydeModulePkg/InsydeModulePkg.dec
  InsydeOemServicesPkg/InsydeOemServicesPkg.dec
#[-end-201104-IB17510119-add]#
#[-start-210908-YUNLEI0131-modify]
#[-start-210914-SHAONN0007-modify]//
#[-start-210928-BAOBO0003-A-add]#
#[-start-210929-Dennis0005-modify]#
!if ($(C770_SUPPORT_ENABLE) == YES) OR ($(S370_SUPPORT_ENABLE)== YES) OR ($(C970_SUPPORT_ENABLE) == YES) OR ($(S77014_SUPPORT_ENABLE) == YES) OR ($(S77013_SUPPORT_ENABLE) == YES) OR ($(S570_SUPPORT_ENABLE)== YES) OR ($(S77014IAH_SUPPORT_ENABLE) == YES)
  LfcPkg/LfcPkg.dec
!endif
#[-end-210929-Dennis0005-modify]#
#[-end-210928-BAOBO0003-A-add]#
#[-end-210914-SHAONN0007-modify]//
#[-end-210908-YUNLEI0131-modify]
[Sources]
  PeiInitPreMemLib.c
  PeiMultiBoardInitPreMemLib.c
  PeiDetect.c
  BoardSaInitPreMemLib.c
  BoardPchInitPreMemLib.c

[Pcd]
  gBoardModuleTokenSpaceGuid.PcdLpcSioConfigDefaultPort
#[-start-210928-BAOBO0003-add]#    
  gBoardModuleTokenSpaceGuid.NotifyECBootUp
#[-end-210928-BAOBO0003-add]#    
  # SA Misc Config
  gBoardModuleTokenSpaceGuid.PcdMrcSpdData
  gBoardModuleTokenSpaceGuid.PcdMrcSpdData00
  gBoardModuleTokenSpaceGuid.PcdMrcSpdData01
  gBoardModuleTokenSpaceGuid.PcdMrcSpdData10
  gBoardModuleTokenSpaceGuid.PcdMrcSpdData11
  gBoardModuleTokenSpaceGuid.PcdMrcSpdDataSize

  # SPD Address Table
  gBoardModuleTokenSpaceGuid.PcdMrcSpdAddressTable0
  gBoardModuleTokenSpaceGuid.PcdMrcSpdAddressTable1
  gBoardModuleTokenSpaceGuid.PcdMrcSpdAddressTable2
  gBoardModuleTokenSpaceGuid.PcdMrcSpdAddressTable3
  gBoardModuleTokenSpaceGuid.PcdMrcSpdAddressTable4
  gBoardModuleTokenSpaceGuid.PcdMrcSpdAddressTable5
  gBoardModuleTokenSpaceGuid.PcdMrcSpdAddressTable6
  gBoardModuleTokenSpaceGuid.PcdMrcSpdAddressTable7
  gBoardModuleTokenSpaceGuid.PcdMrcSpdAddressTable8
  gBoardModuleTokenSpaceGuid.PcdMrcSpdAddressTable9
  gBoardModuleTokenSpaceGuid.PcdMrcSpdAddressTable10
  gBoardModuleTokenSpaceGuid.PcdMrcSpdAddressTable11
  gBoardModuleTokenSpaceGuid.PcdMrcSpdAddressTable12
  gBoardModuleTokenSpaceGuid.PcdMrcSpdAddressTable13
  gBoardModuleTokenSpaceGuid.PcdMrcSpdAddressTable14
  gBoardModuleTokenSpaceGuid.PcdMrcSpdAddressTable15

  #MRC Config
  gBoardModuleTokenSpaceGuid.PcdMrcDqByteMap
  gBoardModuleTokenSpaceGuid.PcdMrcDqByteMapSize
  gBoardModuleTokenSpaceGuid.PcdMrcRcompResistor
  gBoardModuleTokenSpaceGuid.PcdMrcRcompTarget
  gBoardModuleTokenSpaceGuid.PcdMrcDqPinsInterleavedControl
  gBoardModuleTokenSpaceGuid.PcdMrcDqPinsInterleaved
  gBoardModuleTokenSpaceGuid.PcdMrcDqsMapCpu2DramSize
  gBoardModuleTokenSpaceGuid.PcdMrcDqMapCpu2DramSize
  gBoardModuleTokenSpaceGuid.PcdMrcDqsMapCpu2Dram
  gBoardModuleTokenSpaceGuid.PcdMrcDqMapCpu2Dram
  gBoardModuleTokenSpaceGuid.PcdMrcCaVrefConfig
  gBoardModuleTokenSpaceGuid.PcdMrcLp5CccConfig
  gBoardModuleTokenSpaceGuid.PcdSaMiscUserBd
  gBoardModuleTokenSpaceGuid.PcdSaDdrFreqLimit

  #MISC
  gBoardModuleTokenSpaceGuid.PcdRecoveryModeGpio
  gBoardModuleTokenSpaceGuid.PcdOddPowerInitEnable
  gBoardModuleTokenSpaceGuid.PcdPc8374SioKbcPresent
  gBoardModuleTokenSpaceGuid.PcdSmbusAlertEnable
  gBoardModuleTokenSpaceGuid.PcdWakeupType
  gBoardModuleTokenSpaceGuid.PcdSetupEnable

  # USB 2.0 Port Over Current Pin
  gBoardModuleTokenSpaceGuid.PcdUsb2OverCurrentPinTable

  # USB 3.0 Port Over Current Pin
  gBoardModuleTokenSpaceGuid.PcdUsb3OverCurrentPinTable

  #Board Information
  gBoardModuleTokenSpaceGuid.PcdPlatformGeneration
  gBoardModuleTokenSpaceGuid.PcdSpdPresent
  gBoardModuleTokenSpaceGuid.PcdDockAttached
  gBoardModuleTokenSpaceGuid.PcdPlatformType
  gBoardModuleTokenSpaceGuid.PcdPlatformFlavor
  gBoardModuleTokenSpaceGuid.PcdBoardRev
  gBoardModuleTokenSpaceGuid.PcdBoardId
  gBoardModuleTokenSpaceGuid.PcdSkuType
  gBoardModuleTokenSpaceGuid.PcdBoardBomId
  gBoardModuleTokenSpaceGuid.PcdBoardType
  gBoardModuleTokenSpaceGuid.PcdBoardName
  gBoardModuleTokenSpaceGuid.PcdCvfUsbPort
  gBoardModuleTokenSpaceGuid.PcdEcPresent
  gEfiMdeModulePkgTokenSpaceGuid.PcdAcpiDefaultOemTableId
  gBoardModuleTokenSpaceGuid.PcdBoardRetimerForcePwrGpio
  gBoardModuleTokenSpaceGuid.VpdPcdBoardGpioTablePreMem
  gBoardModuleTokenSpaceGuid.VpdPcdPcieClkUsageMap
  gBoardModuleTokenSpaceGuid.VpdPcdUSB2OCMap
  gBoardModuleTokenSpaceGuid.VpdPcdUSB3OCMap
#[-start-210728-YUNLEI0116-modify]
!if $(C770_SUPPORT_ENABLE) == YES
  gBoardModuleTokenSpaceGuid.VpdPcd16USB2OCMap
  gBoardModuleTokenSpaceGuid.VpdPcd16USB3OCMap
!endif
#[-end-210728-YUNLEI0116-modify]
  gBoardModuleTokenSpaceGuid.VpdPcdMrcSpdData
  gBoardModuleTokenSpaceGuid.VpdPcdMrcDqsMapCpu2Dram
  gBoardModuleTokenSpaceGuid.VpdPcdMrcDqMapCpu2Dram
  gBoardModuleTokenSpaceGuid.PcdDisplayId                      ## CONSUMES
#[-start-210702-YUNLEI0109-modify]
#[-start-210519-KEBIN00001-modify]#
!if $(C970_SUPPORT_ENABLE) == YES
  gBoardModuleTokenSpaceGuid.Yogac970MO1Q1R16GSpdData
  gBoardModuleTokenSpaceGuid.Yogac970MO1Q2R16GSpdData
  gBoardModuleTokenSpaceGuid.Yogac970MO1D1R8GSpdData
  gBoardModuleTokenSpaceGuid.Yogac970MO2O2R32GSpdData
  gBoardModuleTokenSpaceGuid.YogaC9VpdPcdMrcDqsMapCpu2Dram
  gBoardModuleTokenSpaceGuid.YogaC9VpdPcdMrcDqMapCpu2Dram
  gBoardModuleTokenSpaceGuid.Yogac970Samsung16GSpdData
  gBoardModuleTokenSpaceGuid.Yogac970hynix16GSpdData
  gBoardModuleTokenSpaceGuid.Yogac970hynix8GSpdData
 !endif    
#end C970_SUPPORT_ENABLE

#[start-210720-STORM1100-modify]#
!if $(C770_SUPPORT_ENABLE) == YES
  gBoardModuleTokenSpaceGuid.VpdC770DISPcdPcieClkUsageMap
  gBoardModuleTokenSpaceGuid.VpdC770UMAPcdPcieClkUsageMap
  gBoardModuleTokenSpaceGuid.YogaC716VpdPcdMrcDqsMapCpu2Dram
  gBoardModuleTokenSpaceGuid.YogaC714VpdPcdMrcDqsMapCpu2Dram
  gBoardModuleTokenSpaceGuid.YogaC716VpdPcdMrcDqMapCpu2Dram
  gBoardModuleTokenSpaceGuid.YogaC714VpdPcdMrcDqMapCpu2Dram
#[-start-210726-YUNLEI0113-modify]
  gBoardModuleTokenSpaceGuid.Yogac970MO1Q2R16GSpdData
  gBoardModuleTokenSpaceGuid.Yogac970MO1D1R8GSpdData
  gBoardModuleTokenSpaceGuid.Yogac970MO2O2R32GSpdData
  gBoardModuleTokenSpaceGuid.Yogac970Samsung16GSpdData
  gBoardModuleTokenSpaceGuid.Yogac970hynix16GSpdData
  gBoardModuleTokenSpaceGuid.Yogac970hynix8GSpdData
  gBoardModuleTokenSpaceGuid.Yogac770Samsung32GSpdData
  gBoardModuleTokenSpaceGuid.Yogac770hynix32GSpdData
#[-end-210726-YUNLEI0113-modify]
!endif
#end C770_SUPPORT_ENABLE
#[end-210720-STORM1100-modify]#

#[-end-210519-KEBIN00001-modify]#
#[-end-210702-YUNLEI0109-modify]
#[-start-210817-DABING0002-modify]#
!if ($(S77014_SUPPORT_ENABLE) == YES) OR ($(S77014IAH_SUPPORT_ENABLE) == YES)
  gBoardModuleTokenSpaceGuid.S77014VpdPcdMrcDqMapCpu2Dram
  gBoardModuleTokenSpaceGuid.S77014VpdPcdMrcDqsMapCpu2Dram
  gBoardModuleTokenSpaceGuid.Yogac970Samsung16GSpdData 
#[-start-210825-TAMT000002-add]#	 
  gBoardModuleTokenSpaceGuid.Yogac970MO1D1R8GSpdData 
  gBoardModuleTokenSpaceGuid.Yogac970MO1Q2R16GSpdData 
  gBoardModuleTokenSpaceGuid.Yogac970hynix8GSpdData
  gBoardModuleTokenSpaceGuid.Yogac970hynix16GSpdData
#[-end-210825-TAMT000002-add]#
#[-start-210927-TAMT000015-add]#
  gBoardModuleTokenSpaceGuid.VpdS77014DISPcdPcieClkUsageMap
  gBoardModuleTokenSpaceGuid.VpdS77014UMAPcdPcieClkUsageMap
#[-end-210927-TAMT000015-add]#
!endif
#end S77014_SUPPORT_ENABLE
#[-END-210817-DABING0002-modify]#

#[-start-210914-DABING0006-modify]#
!if $(S77013_SUPPORT_ENABLE) == YES
  gBoardModuleTokenSpaceGuid.S77013VpdPcdMrcDqMapCpu2Dram
  gBoardModuleTokenSpaceGuid.S77013VpdPcdMrcDqsMapCpu2Dram
  gBoardModuleTokenSpaceGuid.Yogac970Samsung16GSpdData 	 
  gBoardModuleTokenSpaceGuid.Yogac970MO1D1R8GSpdData 
  gBoardModuleTokenSpaceGuid.Yogac970MO1Q2R16GSpdData 
  gBoardModuleTokenSpaceGuid.Yogac970hynix8GSpdData
  gBoardModuleTokenSpaceGuid.Yogac970hynix16GSpdData
!endif
#[-end-210914-DABING0006-modify]#
  gBoardModuleTokenSpaceGuid.PcdCpuRatio
  gBoardModuleTokenSpaceGuid.PcdBiosGuard

  gIntelFsp2PkgTokenSpaceGuid.PcdTemporaryRamBase              ## CONSUMES
  gIntelFsp2PkgTokenSpaceGuid.PcdTemporaryRamSize              ## CONSUMES
  gIntelFsp2PkgTokenSpaceGuid.PcdFspTemporaryRamSize           ## CONSUMES
  gIntelFsp2PkgTokenSpaceGuid.PcdFspReservedBufferSize         ## CONSUMES

  gBoardModuleTokenSpaceGuid.PcdStackBase
  gBoardModuleTokenSpaceGuid.PcdStackSize
  gBoardModuleTokenSpaceGuid.PcdNvsBufferPtr
  gBoardModuleTokenSpaceGuid.PcdCleanMemory

  #SA GPIO Config
  gBoardModuleTokenSpaceGuid.PcdRootPortIndex

  # PCIE Slot1 (x4 Connector) GPIO PCDs
  gBoardModuleTokenSpaceGuid.PcdPcieSlot1GpioSupport
  gBoardModuleTokenSpaceGuid.PcdPcieSlot1HoldRstExpanderNo
  gBoardModuleTokenSpaceGuid.PcdPcieSlot1HoldRstGpioNo
  gBoardModuleTokenSpaceGuid.PcdPcieSlot1HoldRstGpioPolarity
  gBoardModuleTokenSpaceGuid.PcdPcieSlot1PwrEnableExpanderNo
  gBoardModuleTokenSpaceGuid.PcdPcieSlot1PwrEnableGpioNo
  gBoardModuleTokenSpaceGuid.PcdPcieSlot1PwrEnableGpioPolarity
  gBoardModuleTokenSpaceGuid.PcdPcieSsd2RstGpioPolarity

  # RTD3 PCIE
  gBoardModuleTokenSpaceGuid.PcdPcie0GpioSupport                   ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdPcie0HoldRstExpanderNo             ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdPcie0HoldRstGpioNo                 ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdPcie0HoldRstActive                 ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdPcie0PwrEnableExpanderNo           ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdPcie0PwrEnableGpioNo               ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdPcie0PwrEnableActive               ## CONSUMES

  gBoardModuleTokenSpaceGuid.PcdPcie1GpioSupport                   ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdPcie1HoldRstExpanderNo             ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdPcie1HoldRstGpioNo                 ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdPcie1HoldRstActive                 ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdPcie1PwrEnableExpanderNo           ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdPcie1PwrEnableGpioNo               ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdPcie1PwrEnableActive               ## CONSUMES

  gBoardModuleTokenSpaceGuid.PcdPcie2GpioSupport                   ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdPcie2HoldRstExpanderNo             ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdPcie2HoldRstGpioNo                 ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdPcie2HoldRstActive                 ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdPcie2PwrEnableExpanderNo           ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdPcie2PwrEnableGpioNo               ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdPcie2PwrEnableActive               ## CONSUMES

  gBoardModuleTokenSpaceGuid.PcdPcie3GpioSupport                   ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdPcie3HoldRstExpanderNo             ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdPcie3HoldRstGpioNo                 ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdPcie3HoldRstActive                 ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdPcie3PwrEnableExpanderNo           ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdPcie3PwrEnableGpioNo               ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdPcie3PwrEnableActive               ## CONSUMES

  # PCIe x4 M.2 SSD1 RTD3
  gBoardModuleTokenSpaceGuid.PcdPcieSsd2PwrEnableGpioNo
  gBoardModuleTokenSpaceGuid.PcdPcieSsd2RstGpioNo
  gBoardModuleTokenSpaceGuid.PcdPcieSsd2PwrEnableGpioPolarity

 # PCIe x4 M.2 SSD2 RTD3
  gBoardModuleTokenSpaceGuid.PcdPcieSsd3PwrEnableGpioNo
  gBoardModuleTokenSpaceGuid.PcdPcieSsd3RstGpioNo
  gBoardModuleTokenSpaceGuid.PcdPcieSsd3PwrEnableGpioPolarity
  gBoardModuleTokenSpaceGuid.PcdPcieSsd3RstGpioPolarity

  # CPU PCIe x8 DG RTD3
  gBoardModuleTokenSpaceGuid.PcdPcieDG2PwrEnableGpioNo
  gBoardModuleTokenSpaceGuid.PcdPcieDG2RstGpioNo
  gBoardModuleTokenSpaceGuid.PcdPcieDG2PwrEnableGpioPolarity
  gBoardModuleTokenSpaceGuid.PcdPcieDG2RstGpioPolarity
  gBoardModuleTokenSpaceGuid.PcdPcieDG2WakeGpioPin

  # CPU PEG Slot1 RTD3
  gBoardModuleTokenSpaceGuid.PcdPegSlot1PwrEnableGpioNo
  gBoardModuleTokenSpaceGuid.PcdPegSlot1PwrEnableGpioPolarity
  gBoardModuleTokenSpaceGuid.PcdPegSlot1RstGpioNo
  gBoardModuleTokenSpaceGuid.PcdPegSlot1RstGpioPolarity
  gBoardModuleTokenSpaceGuid.PcdPegSlot1WakeGpioPin
  gBoardModuleTokenSpaceGuid.PcdPegSlot1RootPort

  # CPU PEG Slot2 RTD3
  gBoardModuleTokenSpaceGuid.PcdPegSlot2PwrEnableGpioNo
  gBoardModuleTokenSpaceGuid.PcdPegSlot2PwrEnableGpioPolarity
  gBoardModuleTokenSpaceGuid.PcdPegSlot2RstGpioNo
  gBoardModuleTokenSpaceGuid.PcdPegSlot2RstGpioPolarity
  gBoardModuleTokenSpaceGuid.PcdPegSlot2WakeGpioPin
  gBoardModuleTokenSpaceGuid.PcdPegSlot2RootPort

  #SA USB Config
  gBoardModuleTokenSpaceGuid.PcdCpuXhciPortSupportMap

  #Tbt config
  gBoardModuleTokenSpaceGuid.PcdITbtRootPortNumber

  # PCIe Clock Info
  gBoardModuleTokenSpaceGuid.PcdPcieClock0
  gBoardModuleTokenSpaceGuid.PcdPcieClock1
  gBoardModuleTokenSpaceGuid.PcdPcieClock2
  gBoardModuleTokenSpaceGuid.PcdPcieClock3
  gBoardModuleTokenSpaceGuid.PcdPcieClock4
  gBoardModuleTokenSpaceGuid.PcdPcieClock5
  gBoardModuleTokenSpaceGuid.PcdPcieClock6
  gBoardModuleTokenSpaceGuid.PcdPcieClock7
  gBoardModuleTokenSpaceGuid.PcdPcieClock8
  gBoardModuleTokenSpaceGuid.PcdPcieClock9
  gBoardModuleTokenSpaceGuid.PcdPcieClock10
  gBoardModuleTokenSpaceGuid.PcdPcieClock11
  gBoardModuleTokenSpaceGuid.PcdPcieClock12
  gBoardModuleTokenSpaceGuid.PcdPcieClock13
  gBoardModuleTokenSpaceGuid.PcdPcieClock14
  gBoardModuleTokenSpaceGuid.PcdPcieClock15

  # USB 2.0 PHY Port parameters
  gBoardModuleTokenSpaceGuid.PcdUsb2PhyTuningTable

  # GPIO Group Tier
  gBoardModuleTokenSpaceGuid.PcdGpioGroupToGpeDw0
  gBoardModuleTokenSpaceGuid.PcdGpioGroupToGpeDw1
  gBoardModuleTokenSpaceGuid.PcdGpioGroupToGpeDw2

  # HSIO

  # WWAN Full Card Power Off and reset pins
  gBoardModuleTokenSpaceGuid.PcdWwanFullCardPowerOffGpio               ## PRODUCES
  gBoardModuleTokenSpaceGuid.PcdWwanFullCardPowerOffGpioPolarity       ## PRODUCES
  gBoardModuleTokenSpaceGuid.PcdWwanBbrstGpio                          ## PRODUCES
  gBoardModuleTokenSpaceGuid.PcdWwanBbrstGpioPolarity                  ## PRODUCES
  gBoardModuleTokenSpaceGuid.PcdWwanPerstGpio                          ## PRODUCES
  gBoardModuleTokenSpaceGuid.PcdWwanPerstGpioPolarity                  ## PRODUCES
  gBoardModuleTokenSpaceGuid.PcdWwanWakeGpio                           ## PRODUCES
  gBoardModuleTokenSpaceGuid.PcdWwanSourceClock                        ## PRODUCES
  gBoardModuleTokenSpaceGuid.PcdWwanRootPortNumber                     ## PRODUCES

  gBoardModuleTokenSpaceGuid.PcdDisableVpdGpioTable
  gBoardModuleTokenSpaceGuid.PcdBoardGpioTablePreMem                   ## PRODUCES
  gBoardModuleTokenSpaceGuid.PcdBoardGpioTableWwanOnEarlyPreMem        ## PRODUCES
  gBoardModuleTokenSpaceGuid.PcdBoardGpioTableWwanOnEarlyPreMemSize    ## PRODUCES
  gBoardModuleTokenSpaceGuid.PcdBoardGpioTableWwanOffEarlyPreMem       ## PRODUCES
  gBoardModuleTokenSpaceGuid.PcdBoardGpioTableWwanOffEarlyPreMemSize   ## PRODUCES
  gBoardModuleTokenSpaceGuid.PcdBoardGpioTableM80WwanOnEarlyPreMem     ## PRODUCES
  gBoardModuleTokenSpaceGuid.PcdBoardGpioTableM80WwanOnEarlyPreMemSize ## PRODUCES
  gBoardModuleTokenSpaceGuid.PcdBoardGpioTableEarlyPreMem              ## PRODUCES


  # Display DDI
  gBoardModuleTokenSpaceGuid.PcdSaDisplayConfigTable           ## PRODUCES
  gBoardModuleTokenSpaceGuid.PcdSaDisplayConfigTableSize       ## PRODUCES

  gPlatformModuleTokenSpaceGuid.PcdDesktopLpcSioDataDefaultPort   ## CONSUMES
  gPlatformModuleTokenSpaceGuid.PcdDesktopLpcSioIndexDefaultPort  ## CONSUMES

  # EC FailSafe Cpu Temp and Fan Speed Setting
  gBoardModuleTokenSpaceGuid.PcdEcFailSafeActionCpuTemp
  gBoardModuleTokenSpaceGuid.PcdEcFailSafeActionFanPwm

##[-start-190906-IB04530031-add]##
  gChipsetPkgTokenSpaceGuid.PcdCrbSkuId
##[-end-190906-IB04530031-add]##
#[-start-201104-IB17510119-add]#
  gChipsetPkgTokenSpaceGuid.PcdH2OGpioCfgSupported
#[-end-201104-IB17510119-add]#
#[-start-210721-QINGLIN0001-add]#
!if $(S570_SUPPORT_ENABLE) == YES
  gChipsetPkgTokenSpaceGuid.PcdH2OMemoryDownSpdDataMc0Ch0Dimm0
  gChipsetPkgTokenSpaceGuid.PcdH2OMemoryDownSpdDataMc1Ch0Dimm0
!endif
#[-end-210721-QINGLIN0001-add]#
#[-start-210803-QINGLIN0008-add]#
#[-start-210802-SHAONN0003-add]#
!if ($(S370_SUPPORT_ENABLE) == YES)
  gChipsetPkgTokenSpaceGuid.PcdH2OMemoryDownSpdDataMc1Ch0Dimm0
!endif
#[-end-210802-SHAONN0003-add]#
#[-end-210803-QINGLIN0008-add]#
#[-start-210910-QINGLIN0060-modify]#
!if ($(S370_SUPPORT_ENABLE) == YES) OR ($(S570_SUPPORT_ENABLE) == YES)
  gBoardModuleTokenSpaceGuid.DDR4Samsung4GSpdData
  gBoardModuleTokenSpaceGuid.DDR4Samsung8GSpdData
  gBoardModuleTokenSpaceGuid.DDR4Hynix4GSpdData
  gBoardModuleTokenSpaceGuid.DDR4Hynix8GSpdData
  gBoardModuleTokenSpaceGuid.DDR4Micron4GSpdData
  gBoardModuleTokenSpaceGuid.DDR4Micron8GSpdData
  gBoardModuleTokenSpaceGuid.DDR4NullSpdData
!endif
#[-start-211230-QINGLIN0140-modify]#
!if ($(S570_SUPPORT_ENABLE) == YES) OR ($(S370_SUPPORT_ENABLE) == YES)
#[-end-211230-QINGLIN0140-modify]#
  gBoardModuleTokenSpaceGuid.DDR4Micron8G2ndSpdData
!endif
#[-end-210910-QINGLIN0060-modify]#
#[-start-211228-QINGLIN0136-add]#
!if ($(S370_SUPPORT_ENABLE) == YES)
  gBoardModuleTokenSpaceGuid.DDR4Smart4GSpdData
  gBoardModuleTokenSpaceGuid.DDR4Adata4GSpdData
  gBoardModuleTokenSpaceGuid.DDR4Micron8G170SpdData
  gBoardModuleTokenSpaceGuid.DDR4Smart8G170SpdData    
!endif
#[-end-211228-QINGLIN0136-add]#

[Guids]
  gFspNonVolatileStorageHobGuid
  gEfiMemoryOverwriteControlDataGuid
  gSetupVariableGuid
  gSaSetupVariableGuid
  gCpuSetupVariableGuid
  gPchSetupVariableGuid
  gMeSetupVariableGuid
  gSiSetupVariableGuid

[Ppis]
  gPatchConfigurationDataPreMemPpiGuid    ## NOTIFY
  gEfiPeiReadOnlyVariable2PpiGuid         ## CONSUMES
  gSetupVariablesReadyPpiGuid             ## PRODUCES
