/** @file
  PEI Boards Configurations for PreMem phase.

@copyright
  INTEL CONFIDENTIAL
  Copyright 2020 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains a 'Sample Driver' and is licensed as such under the terms
  of your license agreement with Intel or your vendor. This file may be modified
  by the user, subject to the additional terms of the license agreement.

@par Specification Reference:
**/

#ifndef _BOARD_SA_CONFIG_PRE_MEM_H_
#define _BOARD_SA_CONFIG_PRE_MEM_H_

#include <Ppi/SiPolicy.h>
#include <Library/BoardConfigLib.h>

//
// LPDDR3 / LPDDR4 RVP boards (memory soldered down)
// Single SPD EEPROM at 0xA2 serves both C0D0 and C1D0 (LPDDR3/4 is 1DPC only)
//
#define DIMM_SMB_SPD_P0C0D0_STP 0xA2
#define DIMM_SMB_SPD_P0C0D1_STP 0xA0
#define DIMM_SMB_SPD_P0C1D0_STP 0xA2
#define DIMM_SMB_SPD_P0C1D1_STP 0xA0
#define SA_MRC_MAX_RCOMP_TARGETS  (5)

GLOBAL_REMOVE_IF_UNREFERENCED const UINT16 AdlPRcompTargetZeroes[SA_MRC_MAX_RCOMP_TARGETS] = { 0, 0, 0, 0, 0 };

//
// Reference RCOMP resistors on motherboard - for ICL - MRC will set automatically
//
GLOBAL_REMOVE_IF_UNREFERENCED const UINT16 AdlPRcompResistorZero = 0;

//
// ICL U SDS ERB board RCOMP target values for RdOdt, WrDS, WrDSCmd, WrDSCtl, WrDSClk - MRC will set automatically
//
GLOBAL_REMOVE_IF_UNREFERENCED const UINT16 RcompTargetAdlP[SA_MRC_MAX_RCOMP_TARGETS] = { 0, 0, 0, 0, 0 };

// ADL-P LPDDR5 DQS byte swizzling between CPU and DRAM
GLOBAL_REMOVE_IF_UNREFERENCED const UINT8 DqsMapCpu2DramAdlPLp5Type4Rvp[8][2] = {
  // Ch 0 1 2 3
  { 1, 0 }, { 0, 1 }, { 0, 1 }, { 0, 1 }, // Controller 0
  { 1, 0 }, { 0, 1 }, { 0, 1 }, { 0, 1 }  // Controller 1
};

// Adl-P LPDDR4 Interposer on DDR4 system DQS byte swizzling between CPU and DRAM
GLOBAL_REMOVE_IF_UNREFERENCED const UINT8 DqsMapCpu2DramAdlPDdr4Interposer[8][2] = {
  // Ch 0     1         2         3
  { 1, 0 }, { 1, 0 }, { 1, 0 }, { 1, 0 },  //controller 0
  { 1, 0 }, { 1, 0 }, { 1, 0 }, { 0, 1 }   //controller 1
};

// Adl-P LPDDR4 GCS DQS byte and bit swizzing between CPU and DRAM
GLOBAL_REMOVE_IF_UNREFERENCED const UINT8 DqsMapCpu2DramBoardIdAdlPLp4Gcs[8][2] = {
  // Ch 0     1         2         3
  { 0, 1 }, { 0, 1 }, { 0, 1 }, { 0, 1 },  //controller 0
  { 0, 1 }, { 0, 1 }, { 0, 1 }, { 0, 1 }   //controller 1
};

GLOBAL_REMOVE_IF_UNREFERENCED const UINT8 DqMapCpu2DramAdlPLp5Type4[8][2][8] = {
  //Controller 0
  {{  0,  1,  2,  3,  6,  4,  7,  5 },   // Byte 0
  {  13, 14, 12, 11,  8, 15, 10,  9 }},  // Byte 1
  {{ 15, 14, 13, 12, 10,  8, 11,  9 },   // Byte 2
  {   6,  0,  1,  5,  2,  7,  3,  4 }},  // Byte 3
  {{  0,  1,  7,  2,  5,  4,  6,  3 },   // Byte 4
  {  12, 15, 14, 13,  8,  9, 10, 11 }},  // Byte 5
  {{  6,  0,  1,  5,  7,  4,  2,  3 },   // Byte 6
  {  14, 13,  8,  9, 10, 15, 11, 12 }},  // Byte 7
  //Controller 1
  {{  3,  0,  1,  2,  6,  5,  4,  7 },   // Byte 0
  {  11, 12, 13, 14,  8, 15,  9, 10 }},  // Byte 1
  {{ 13, 10, 12, 11, 15,  8, 14,  9 },   // Byte 2
  {   3,  2,  7,  4,  1,  0,  5,  6 }},  // Byte 3
  {{  0,  1,  7,  2,  5,  3,  6,  4 },   // Byte 4
  {  15, 12, 13, 14, 10, 11,  9,  8 }},  // Byte 5
  {{  3,  4,  2,  7,  1,  0,  6,  5 },   // Byte 6
  {  14, 13,  9,  8, 12, 10, 11, 15 }}   // Byte 7
};

GLOBAL_REMOVE_IF_UNREFERENCED const UINT8 DqMapCpu2DramBoardIdAdlPLp4Aep[8][2][8] = {
  //Controller 0
  {{ 0,  1,  3,  2,  4,  5,  6,  7 },  // Byte 0
  { 12, 13, 14, 15, 11, 10,  9,  8 }}, // Byte 1
  {{ 7,  2,  6,  3,  5,  1,  4,  0 },  // Byte 2
  { 10,  8,  9, 11, 15, 12, 14, 13 }}, // Byte 3
  {{ 3,  2,  1,  0,  4,  5,  6,  7 },  // Byte 4
  { 12, 13, 14, 15, 11, 10,  9,  8 }}, // Byte 5
  {{ 7,  0,  1,  6,  5,  4,  2,  3 },  // Byte 6
  { 15, 14,  8,  9, 10, 12, 11, 13 }}, // Byte 7
  //Controller 1
  {{ 3,  2,  1,  0,  4,  5,  6,  7 },  // Byte 0 ChB!
  { 12, 13, 14, 15, 11, 10,  9,  8 }}, // Byte 1 ChB!
  {{ 3,  4,  2,  5,  0,  6,  1,  7 },  // Byte 2
  { 13, 12, 11, 10, 14, 15,  9,  8 }}, // Byte 3
  {{ 3,  2,  1,  0,  7,  4,  5,  6 },  // Byte 4
  { 15, 14, 13, 12,  8,  9, 10, 11 }}, // Byte 5
  {{ 3,  4,  2,  5,  1,  0,  7,  6 },  // Byte 6
  { 15, 14,  9,  8, 12, 10, 11, 13 }}  // Byte 7
};

// LPDDR4 Type-4 RVP DQ swizzling between CPU and DRAM
GLOBAL_REMOVE_IF_UNREFERENCED const UINT8 DqMapCpu2DramAdlPDDr4Interposer[8][2][8] = {
  //Controller 0
  {{ 13, 12, 14, 15, 11,  8,  9, 10 },  // Byte 0
  {  2,  3,  1,  0,  7,  4,  5,  6 }},  // Byte 1
  {{ 11, 12, 13, 10, 14,  8, 15,  9 },  // Byte 2
  {  2,  3,  1,  0,  6,  5,  7,  4 }},  // Byte 3
  {{ 15, 14, 12, 13, 11, 10,  8,  9 },  // Byte 4
  {  0,  1,  2,  3,  4,  7,  6,  5 }},  // Byte 5
  {{ 12, 13, 11, 10,  9, 15,  8, 14 },  // Byte 6
  {  2,  3,  0,  1,  5,  4,  7,  6 }},  // Byte 7
    //Controller 1
  {{ 15, 14, 13, 12, 10,  8,  9, 11 },  // Byte 0
  {  0,  1,  2,  3,  7,  4,  5,  6 }},  // Byte 1
  {{ 15, 14, 11, 10, 13, 12,  8,  9 },  // Byte 2
  {  1,  7,  0,  6,  3,  5,  2,  4 }},  // Byte 3
  {{ 15, 14, 13, 12,  9, 10, 11,  8 },  // Byte 4
  {  0,  1,  7,  6,  3,  2,  5,  4 }},  // Byte 5
  {{  4,  3,  5,  2,  6,  7,  0,  1 },  // Byte 6
  { 15, 14, 10, 11, 12,  9,  8, 13 }}   // Byte 7
};

GLOBAL_REMOVE_IF_UNREFERENCED const UINT8 DqMapCpu2DramBoardIdAdlPLp4Gcs[8][2][8] = {
  //Controller 0
  {{  0,  1,  3,  2,  4,  5,  6,  7 },   // Byte 0
   { 12, 13, 14, 15, 11, 10,  9,  8 }},  // Byte 1
  {{  7,  2,  6,  3,  5,  1,  4,  0 },   // Byte 2
   { 10,  8,  9, 11, 15, 12, 14, 13 }},  // Byte 3
  {{  3,  2,  1,  0,  4,  5,  6,  7 },   // Byte 4
   { 12, 13, 14, 15, 11, 10,  9,  8 }},  // Byte 5
  {{  7,  0,  1,  6,  5,  4,  2,  3 },   // Byte 6
   { 15, 14,  8,  9, 10, 12, 11, 13 }},  // Byte 7
  //Controller 1
  {{  3,  2,  1,  0,  4,  5,  6,  7},    // Byte 0
   { 12, 13, 14, 15, 11, 10,  9,  8 }},  // Byte 1
  {{  3,  4,  2,  5,  0,  6,  1,  7 },   // Byte 2
   { 13, 12, 11, 10, 14, 15,  9,  8 }},  // Byte 3
  {{  3,  2,  1,  0,  7,  4,  5,  6 },   // Byte 4
   { 15, 14, 13, 12,  8,  9, 10, 11 }},  // Byte 5
  {{  3,  4,  2,  5,  1,  0,  7,  6 },   // Byte 6
   { 15, 14,  9,  8, 12, 10, 11, 13 }}   // Byte 7
};
//
// DQ byte mapping to CMD/CTL/CLK, from the CPU side
// Used for ICL-U LPDDR4 SDS, ICL-U LPDDR4 Type-4 RVP
//
GLOBAL_REMOVE_IF_UNREFERENCED const UINT8 DqByteMapAdlP[2][6][2] = {
  // Channel 0:
  {
    { 0x0F, 0xF0 }, // CLK0 goes to package 0 - Bytes[3:0], CLK1 goes to package 1 - Bytes[7:4]
    { 0x0F, 0xF0 }, // Cmd CAA goes to Bytes[3:0], Cmd CAB goes to Byte[7:4]
    { 0xFF, 0x00 }, // CTL (CS) goes to all bytes
    { 0x00, 0x00 }, // Unused in ICL MRC
    { 0x00, 0x00 }, // Unused in ICL MRC
    { 0x00, 0x00 }, // Unused in ICL MRC
  },
  // Channel 1:
  {
    { 0x0F, 0xF0 }, // CLK0 goes to package 0 - Bytes[3:0], CLK1 goes to package 1 - Bytes[7:4]
    { 0x0F, 0xF0 }, // Cmd CAA goes to Bytes[3:0], Cmd CAB goes to Byte[7:4]
    { 0xFF, 0x00 }, // CTL (CS) goes to all bytes
    { 0x00, 0x00 }, // Unused in ICL MRC
    { 0x00, 0x00 }, // Unused in ICL MRC
    { 0x00, 0x00 }, // Unused in ICL MRC
  }
};

//
// Display DDI settings for Adl-P Dual Edp
//
GLOBAL_REMOVE_IF_UNREFERENCED const UINT8 mAdlPDualEdpDisplayDdiConfig[16] = {
                                                                               DdiPortEdp,      // DDI Port A Config : DdiPortDisabled = No LFP is Connected, DdiPortEdp = eDP, DdiPortMipiDsi = MIPI DSI
                                                                               DdiPortEdp,      // DDI Port B Config : DdiPortDisabled = No LFP is Connected, DdiPortEdp = eDP, DdiPortMipiDsi = MIPI DSI
                                                                               DdiHpdDisable,   // DDI Port A HPD : DdiHpdDisable = Disable, DdiHpdEnable = Enable HPD
                                                                               DdiHpdDisable,   // DDI Port B HPD : DdiHpdDisable = Disable, DdiHpdEnable = Enable HPD
                                                                               DdiHpdDisable,   // DDI Port C HPD : DdiHpdDisable = Disable, DdiHpdEnable = Enable HPD
                                                                               DdiHpdDisable,   // DDI Port 1 HPD : DdiHpdDisable = Disable, DdiHpdEnable = Enable HPD
                                                                               DdiHpdDisable,   // DDI Port 2 HPD : DdiHpdDisable = Disable, DdiHpdEnable = Enable HPD
                                                                               DdiHpdDisable,   // DDI Port 3 HPD : DdiHpdDisable = Disable, DdiHpdEnable = Enable HPD
                                                                               DdiHpdDisable,   // DDI Port 4 HPD : DdiHpdDisable = Disable, DdiHpdEnable = Enable HPD
                                                                               DdiDisable,      // DDI Port A DDC : DdiDisable = Disable, DdiDdcEnable = Enable DDC
                                                                               DdiDisable,      // DDI Port B DDC : DdiDisable = Disable, DdiDdcEnable = Enable DDC
                                                                               DdiDisable,      // DDI Port C DDC : DdiDisable = Disable, DdiDdcEnable = Enable DDC
                                                                               DdiDisable,      // DDI Port 1 DDC : DdiDisable = Disable, DdiDdcEnable = Enable DDC
                                                                               DdiDisable,      // DDI Port 2 DDC : DdiDisable = Disable, DdiDdcEnable = Enable DDC
                                                                               DdiDisable,      // DDI Port 3 DDC : DdiDisable = Disable, DdiDdcEnable = Enable DDC
                                                                               DdiDisable       // DDI Port 4 DDC : DdiDisable = Disable, DdiDdcEnable = Enable DDC
};

//
// Display DDI settings for Adl-P Dual Mipi
//
GLOBAL_REMOVE_IF_UNREFERENCED const UINT8 mAdlPDualMipiDisplayDdiConfig[16] = {
                                                                               DdiPortMipiDsi,  // DDI Port A Config : DdiPortDisabled = No LFP is Connected, DdiPortEdp = eDP, DdiPortMipiDsi = MIPI DSI
                                                                               DdiPortMipiDsi,  // DDI Port B Config : DdiPortDisabled = No LFP is Connected, DdiPortEdp = eDP, DdiPortMipiDsi = MIPI DSI
                                                                               DdiHpdDisable,   // DDI Port A HPD : DdiHpdDisable = Disable, DdiHpdEnable = Enable HPD
                                                                               DdiHpdDisable,   // DDI Port B HPD : DdiHpdDisable = Disable, DdiHpdEnable = Enable HPD
                                                                               DdiHpdDisable,   // DDI Port C HPD : DdiHpdDisable = Disable, DdiHpdEnable = Enable HPD
                                                                               DdiHpdDisable,   // DDI Port 1 HPD : DdiHpdDisable = Disable, DdiHpdEnable = Enable HPD
                                                                               DdiHpdDisable,   // DDI Port 2 HPD : DdiHpdDisable = Disable, DdiHpdEnable = Enable HPD
                                                                               DdiHpdDisable,   // DDI Port 3 HPD : DdiHpdDisable = Disable, DdiHpdEnable = Enable HPD
                                                                               DdiHpdDisable,   // DDI Port 4 HPD : DdiHpdDisable = Disable, DdiHpdEnable = Enable HPD
                                                                               DdiDisable,      // DDI Port A DDC : DdiDisable = Disable, DdiDdcEnable = Enable DDC
                                                                               DdiDisable,      // DDI Port B DDC : DdiDisable = Disable, DdiDdcEnable = Enable DDC
                                                                               DdiDisable,      // DDI Port C DDC : DdiDisable = Disable, DdiDdcEnable = Enable DDC
                                                                               DdiDisable,      // DDI Port 1 DDC : DdiDisable = Disable, DdiDdcEnable = Enable DDC
                                                                               DdiDisable,      // DDI Port 2 DDC : DdiDisable = Disable, DdiDdcEnable = Enable DDC
                                                                               DdiDisable,      // DDI Port 3 DDC : DdiDisable = Disable, DdiDdcEnable = Enable DDC
                                                                               DdiDisable       // DDI Port 4 DDC : DdiDisable = Disable, DdiDdcEnable = Enable DDC
};

//
// Display DDI settings for Adl-P Edp + Mipi
//
GLOBAL_REMOVE_IF_UNREFERENCED const UINT8 mAdlPEdpMipiDisplayDdiConfig[16] = {
                                                                               DdiPortEdp,      // DDI Port A Config : DdiPortDisabled = No LFP is Connected, DdiPortEdp = eDP, DdiPortMipiDsi = MIPI DSI
                                                                               DdiPortMipiDsi,  // DDI Port B Config : DdiPortDisabled = No LFP is Connected, DdiPortEdp = eDP, DdiPortMipiDsi = MIPI DSI
                                                                               DdiHpdDisable,   // DDI Port A HPD : DdiHpdDisable = Disable, DdiHpdEnable = Enable HPD
                                                                               DdiHpdDisable,   // DDI Port B HPD : DdiHpdDisable = Disable, DdiHpdEnable = Enable HPD
                                                                               DdiHpdDisable,   // DDI Port C HPD : DdiHpdDisable = Disable, DdiHpdEnable = Enable HPD
                                                                               DdiHpdDisable,   // DDI Port 1 HPD : DdiHpdDisable = Disable, DdiHpdEnable = Enable HPD
                                                                               DdiHpdDisable,   // DDI Port 2 HPD : DdiHpdDisable = Disable, DdiHpdEnable = Enable HPD
                                                                               DdiHpdDisable,   // DDI Port 3 HPD : DdiHpdDisable = Disable, DdiHpdEnable = Enable HPD
                                                                               DdiHpdDisable,   // DDI Port 4 HPD : DdiHpdDisable = Disable, DdiHpdEnable = Enable HPD
                                                                               DdiDisable,      // DDI Port A DDC : DdiDisable = Disable, DdiDdcEnable = Enable DDC
                                                                               DdiDisable,      // DDI Port B DDC : DdiDisable = Disable, DdiDdcEnable = Enable DDC
                                                                               DdiDisable,      // DDI Port C DDC : DdiDisable = Disable, DdiDdcEnable = Enable DDC
                                                                               DdiDisable,      // DDI Port 1 DDC : DdiDisable = Disable, DdiDdcEnable = Enable DDC
                                                                               DdiDisable,      // DDI Port 2 DDC : DdiDisable = Disable, DdiDdcEnable = Enable DDC
                                                                               DdiDisable,      // DDI Port 3 DDC : DdiDisable = Disable, DdiDdcEnable = Enable DDC
                                                                               DdiDisable       // DDI Port 4 DDC : DdiDisable = Disable, DdiDdcEnable = Enable DDC
};

//
// Display DDI settings for Adl-P Mipi + Edp
//
GLOBAL_REMOVE_IF_UNREFERENCED const UINT8 mAdlPMipiEdpDisplayDdiConfig[16] = {
                                                                               DdiPortMipiDsi,  // DDI Port A Config : DdiPortDisabled = No LFP is Connected, DdiPortEdp = eDP, DdiPortMipiDsi = MIPI DSI
                                                                               DdiPortEdp,      // DDI Port B Config : DdiPortDisabled = No LFP is Connected, DdiPortEdp = eDP, DdiPortMipiDsi = MIPI DSI
                                                                               DdiHpdDisable,   // DDI Port A HPD : DdiHpdDisable = Disable, DdiHpdEnable = Enable HPD
                                                                               DdiHpdDisable,   // DDI Port B HPD : DdiHpdDisable = Disable, DdiHpdEnable = Enable HPD
                                                                               DdiHpdDisable,   // DDI Port C HPD : DdiHpdDisable = Disable, DdiHpdEnable = Enable HPD
                                                                               DdiHpdDisable,   // DDI Port 1 HPD : DdiHpdDisable = Disable, DdiHpdEnable = Enable HPD
                                                                               DdiHpdDisable,   // DDI Port 2 HPD : DdiHpdDisable = Disable, DdiHpdEnable = Enable HPD
                                                                               DdiHpdDisable,   // DDI Port 3 HPD : DdiHpdDisable = Disable, DdiHpdEnable = Enable HPD
                                                                               DdiHpdDisable,   // DDI Port 4 HPD : DdiHpdDisable = Disable, DdiHpdEnable = Enable HPD
                                                                               DdiDisable,      // DDI Port A DDC : DdiDisable = Disable, DdiDdcEnable = Enable DDC
                                                                               DdiDisable,      // DDI Port B DDC : DdiDisable = Disable, DdiDdcEnable = Enable DDC
                                                                               DdiDisable,      // DDI Port C DDC : DdiDisable = Disable, DdiDdcEnable = Enable DDC
                                                                               DdiDisable,      // DDI Port 1 DDC : DdiDisable = Disable, DdiDdcEnable = Enable DDC
                                                                               DdiDisable,      // DDI Port 2 DDC : DdiDisable = Disable, DdiDdcEnable = Enable DDC
                                                                               DdiDisable,      // DDI Port 3 DDC : DdiDisable = Disable, DdiDdcEnable = Enable DDC
                                                                               DdiDisable       // DDI Port 4 DDC : DdiDisable = Disable, DdiDdcEnable = Enable DDC
};

//
// Display DDI settings for Adl-P LP4/Ddr4 Rvp Edp + HDMI
//
GLOBAL_REMOVE_IF_UNREFERENCED const UINT8 mAdlPEdpHdmiDisplayDdiConfig[16] = {
                                                                               DdiPortEdp,      // DDI Port A Config : DdiPortDisabled = No LFP is Connected, DdiPortEdp = eDP, DdiPortMipiDsi = MIPI DSI
                                                                               DdiPortDisabled, // DDI Port B Config : DdiPortDisabled = No LFP is Connected, DdiPortEdp = eDP, DdiPortMipiDsi = MIPI DSI
                                                                               DdiHpdDisable,   // DDI Port A HPD : DdiHpdDisable = Disable, DdiHpdEnable = Enable HPD
                                                                               DdiHpdEnable,    // DDI Port B HPD : DdiHpdDisable = Disable, DdiHpdEnable = Enable HPD
                                                                               DdiHpdDisable,   // DDI Port C HPD : DdiHpdDisable = Disable, DdiHpdEnable = Enable HPD
                                                                               DdiHpdDisable,   // DDI Port 1 HPD : DdiHpdDisable = Disable, DdiHpdEnable = Enable HPD
                                                                               DdiHpdDisable,   // DDI Port 2 HPD : DdiHpdDisable = Disable, DdiHpdEnable = Enable HPD
                                                                               DdiHpdDisable,   // DDI Port 3 HPD : DdiHpdDisable = Disable, DdiHpdEnable = Enable HPD
                                                                               DdiHpdDisable,   // DDI Port 4 HPD : DdiHpdDisable = Disable, DdiHpdEnable = Enable HPD
                                                                               DdiDisable,      // DDI Port A DDC : DdiDisable = Disable, DdiDdcEnable = Enable DDC
                                                                               DdiDdcEnable,    // DDI Port B DDC : DdiDisable = Disable, DdiDdcEnable = Enable DDC
                                                                               DdiDisable,      // DDI Port C DDC : DdiDisable = Disable, DdiDdcEnable = Enable DDC
                                                                               DdiDisable,      // DDI Port 1 DDC : DdiDisable = Disable, DdiDdcEnable = Enable DDC
                                                                               DdiDisable,      // DDI Port 2 DDC : DdiDisable = Disable, DdiDdcEnable = Enable DDC
                                                                               DdiDisable,      // DDI Port 3 DDC : DdiDisable = Disable, DdiDdcEnable = Enable DDC
                                                                               DdiDisable       // DDI Port 4 DDC : DdiDisable = Disable, DdiDdcEnable = Enable DDC
};

//
// Display DDI settings for Adl-P Ddr5 Rvp Edp + DP
//
GLOBAL_REMOVE_IF_UNREFERENCED const UINT8 mAdlPDdr5RvpDisplayDdiConfig[16] = {
                                                                               DdiPortEdp,      // DDI Port A Config : DdiPortDisabled = No LFP is Connected, DdiPortEdp = eDP, DdiPortMipiDsi = MIPI DSI
                                                                               DdiPortDisabled, // DDI Port B Config : DdiPortDisabled = No LFP is Connected, DdiPortEdp = eDP, DdiPortMipiDsi = MIPI DSI
                                                                               DdiHpdDisable,   // DDI Port A HPD : DdiHpdDisable = Disable, DdiHpdEnable = Enable HPD
                                                                               DdiHpdEnable,    // DDI Port B HPD : DdiHpdDisable = Disable, DdiHpdEnable = Enable HPD
                                                                               DdiHpdDisable,   // DDI Port C HPD : DdiHpdDisable = Disable, DdiHpdEnable = Enable HPD
                                                                               DdiHpdDisable,   // DDI Port 1 HPD : DdiHpdDisable = Disable, DdiHpdEnable = Enable HPD
                                                                               DdiHpdDisable,   // DDI Port 2 HPD : DdiHpdDisable = Disable, DdiHpdEnable = Enable HPD
                                                                               DdiHpdDisable,   // DDI Port 3 HPD : DdiHpdDisable = Disable, DdiHpdEnable = Enable HPD
                                                                               DdiHpdDisable,   // DDI Port 4 HPD : DdiHpdDisable = Disable, DdiHpdEnable = Enable HPD
                                                                               DdiDisable,      // DDI Port A DDC : DdiDisable = Disable, DdiDdcEnable = Enable DDC
                                                                               DdiDisable,      // DDI Port B DDC : DdiDisable = Disable, DdiDdcEnable = Enable DDC
                                                                               DdiDisable,      // DDI Port C DDC : DdiDisable = Disable, DdiDdcEnable = Enable DDC
                                                                               DdiDisable,      // DDI Port 1 DDC : DdiDisable = Disable, DdiDdcEnable = Enable DDC
                                                                               DdiDisable,      // DDI Port 2 DDC : DdiDisable = Disable, DdiDdcEnable = Enable DDC
                                                                               DdiDisable,      // DDI Port 3 DDC : DdiDisable = Disable, DdiDdcEnable = Enable DDC
                                                                               DdiDisable       // DDI Port 4 DDC : DdiDisable = Disable, DdiDdcEnable = Enable DDC
};

//
// Display DDI settings for Adl-P Ddr5 Rvp Edp + DP + TC2(DP) TC3(DP) TC4(DP)
//
GLOBAL_REMOVE_IF_UNREFERENCED const UINT8 mAdlPDdr5MRRvpDisplayDdiConfig[16] = {
                                                                               DdiPortEdp,      // DDI Port A Config : DdiPortDisabled = No LFP is Connected, DdiPortEdp = eDP, DdiPortMipiDsi = MIPI DSI
                                                                               DdiPortDisabled, // DDI Port B Config : DdiPortDisabled = No LFP is Connected, DdiPortEdp = eDP, DdiPortMipiDsi = MIPI DSI
                                                                               DdiHpdDisable,   // DDI Port A HPD : DdiHpdDisable = Disable, DdiHpdEnable = Enable HPD
                                                                               DdiHpdEnable,    // DDI Port B HPD : DdiHpdDisable = Disable, DdiHpdEnable = Enable HPD
                                                                               DdiHpdDisable,   // DDI Port C HPD : DdiHpdDisable = Disable, DdiHpdEnable = Enable HPD
                                                                               DdiHpdDisable,   // DDI Port 1 HPD : DdiHpdDisable = Disable, DdiHpdEnable = Enable HPD
                                                                               DdiHpdEnable,    // DDI Port 2 HPD : DdiHpdDisable = Disable, DdiHpdEnable = Enable HPD
                                                                               DdiHpdEnable,    // DDI Port 3 HPD : DdiHpdDisable = Disable, DdiHpdEnable = Enable HPD
                                                                               DdiHpdEnable,    // DDI Port 4 HPD : DdiHpdDisable = Disable, DdiHpdEnable = Enable HPD
                                                                               DdiDisable,      // DDI Port A DDC : DdiDisable = Disable, DdiDdcEnable = Enable DDC
                                                                               DdiDisable,      // DDI Port B DDC : DdiDisable = Disable, DdiDdcEnable = Enable DDC
                                                                               DdiDisable,      // DDI Port C DDC : DdiDisable = Disable, DdiDdcEnable = Enable DDC
                                                                               DdiDisable,      // DDI Port 1 DDC : DdiDisable = Disable, DdiDdcEnable = Enable DDC
                                                                               DdiDisable,      // DDI Port 2 DDC : DdiDisable = Disable, DdiDdcEnable = Enable DDC
                                                                               DdiDisable,      // DDI Port 3 DDC : DdiDisable = Disable, DdiDdcEnable = Enable DDC
                                                                               DdiDisable       // DDI Port 4 DDC : DdiDisable = Disable, DdiDdcEnable = Enable DDC
};

//
// Display DDI settings for Adl-P Lp4 Bep+
//
GLOBAL_REMOVE_IF_UNREFERENCED const UINT8 mAdlPLp4BepRowDisplayDdiConfig[16] = {
                                                                               DdiPortEdp,      // DDI Port A Config : DdiPortDisabled = No LFP is Connected, DdiPortEdp = eDP, DdiPortMipiDsi = MIPI DSI
                                                                               DdiPortDisabled, // DDI Port B Config : DdiPortDisabled = No LFP is Connected, DdiPortEdp = eDP, DdiPortMipiDsi = MIPI DSI
                                                                               DdiHpdDisable,   // DDI Port A HPD : DdiHpdDisable = Disable, DdiHpdEnable = Enable HPD
                                                                               DdiHpdEnable,    // DDI Port B HPD : DdiHpdDisable = Disable, DdiHpdEnable = Enable HPD
                                                                               DdiHpdDisable,   // DDI Port C HPD : DdiHpdDisable = Disable, DdiHpdEnable = Enable HPD
                                                                               DdiHpdDisable,   // DDI Port 1 HPD : DdiHpdDisable = Disable, DdiHpdEnable = Enable HPD
                                                                               DdiHpdDisable,   // DDI Port 2 HPD : DdiHpdDisable = Disable, DdiHpdEnable = Enable HPD
                                                                               DdiHpdDisable,   // DDI Port 3 HPD : DdiHpdDisable = Disable, DdiHpdEnable = Enable HPD
                                                                               DdiHpdDisable,   // DDI Port 4 HPD : DdiHpdDisable = Disable, DdiHpdEnable = Enable HPD
                                                                               DdiDisable,      // DDI Port A DDC : DdiDisable = Disable, DdiDdcEnable = Enable DDC
                                                                               DdiDisable,      // DDI Port B DDC : DdiDisable = Disable, DdiDdcEnable = Enable DDC
                                                                               DdiDisable,      // DDI Port C DDC : DdiDisable = Disable, DdiDdcEnable = Enable DDC
                                                                               DdiDisable,      // DDI Port 1 DDC : DdiDisable = Disable, DdiDdcEnable = Enable DDC
                                                                               DdiDisable,      // DDI Port 2 DDC : DdiDisable = Disable, DdiDdcEnable = Enable DDC
                                                                               DdiDisable,      // DDI Port 3 DDC : DdiDisable = Disable, DdiDdcEnable = Enable DDC
                                                                               DdiDisable       // DDI Port 4 DDC : DdiDisable = Disable, DdiDdcEnable = Enable DDC
};

//
// Display DDI settings for Adl-P DG02 384 Aep
//
GLOBAL_REMOVE_IF_UNREFERENCED const UINT8 mAdlPDg384AepDisplayDdiConfig[16] = {
                                                                               DdiPortEdp,      // DDI Port A Config : DdiPortDisabled = No LFP is Connected, DdiPortEdp = eDP, DdiPortMipiDsi = MIPI DSI
                                                                               DdiPortDisabled, // DDI Port B Config : DdiPortDisabled = No LFP is Connected, DdiPortEdp = eDP, DdiPortMipiDsi = MIPI DSI
                                                                               DdiHpdDisable,   // DDI Port A HPD : DdiHpdDisable = Disable, DdiHpdEnable = Enable HPD
                                                                               DdiHpdDisable,   // DDI Port B HPD : DdiHpdDisable = Disable, DdiHpdEnable = Enable HPD
                                                                               DdiHpdDisable,   // DDI Port C HPD : DdiHpdDisable = Disable, DdiHpdEnable = Enable HPD
                                                                               DdiHpdDisable,   // DDI Port 1 HPD : DdiHpdDisable = Disable, DdiHpdEnable = Enable HPD
                                                                               DdiHpdDisable,   // DDI Port 2 HPD : DdiHpdDisable = Disable, DdiHpdEnable = Enable HPD
                                                                               DdiHpdDisable,   // DDI Port 3 HPD : DdiHpdDisable = Disable, DdiHpdEnable = Enable HPD
                                                                               DdiHpdDisable,   // DDI Port 4 HPD : DdiHpdDisable = Disable, DdiHpdEnable = Enable HPD
                                                                               DdiDisable,      // DDI Port A DDC : DdiDisable = Disable, DdiDdcEnable = Enable DDC
                                                                               DdiDisable,      // DDI Port B DDC : DdiDisable = Disable, DdiDdcEnable = Enable DDC
                                                                               DdiDisable,      // DDI Port C DDC : DdiDisable = Disable, DdiDdcEnable = Enable DDC
                                                                               DdiDisable,      // DDI Port 1 DDC : DdiDisable = Disable, DdiDdcEnable = Enable DDC
                                                                               DdiDisable,      // DDI Port 2 DDC : DdiDisable = Disable, DdiDdcEnable = Enable DDC
                                                                               DdiDisable,      // DDI Port 3 DDC : DdiDisable = Disable, DdiDdcEnable = Enable DDC
                                                                               DdiDisable       // DDI Port 4 DDC : DdiDisable = Disable, DdiDdcEnable = Enable DDC
};

//
// Display DDI settings for Adl-P DG02 128 Aep
//
GLOBAL_REMOVE_IF_UNREFERENCED const UINT8 mAdlPDg128AepDisplayDdiConfig[16] = {
                                                                               DdiPortEdp,      // DDI Port A Config : DdiPortDisabled = No LFP is Connected, DdiPortEdp = eDP, DdiPortMipiDsi = MIPI DSI
                                                                               DdiPortDisabled, // DDI Port B Config : DdiPortDisabled = No LFP is Connected, DdiPortEdp = eDP, DdiPortMipiDsi = MIPI DSI
                                                                               DdiHpdDisable,   // DDI Port A HPD : DdiHpdDisable = Disable, DdiHpdEnable = Enable HPD
                                                                               DdiHpdDisable,   // DDI Port B HPD : DdiHpdDisable = Disable, DdiHpdEnable = Enable HPD
                                                                               DdiHpdDisable,   // DDI Port C HPD : DdiHpdDisable = Disable, DdiHpdEnable = Enable HPD
                                                                               DdiHpdEnable,    // DDI Port 1 HPD : DdiHpdDisable = Disable, DdiHpdEnable = Enable HPD
                                                                               DdiHpdDisable,   // DDI Port 2 HPD : DdiHpdDisable = Disable, DdiHpdEnable = Enable HPD
                                                                               DdiHpdDisable,   // DDI Port 3 HPD : DdiHpdDisable = Disable, DdiHpdEnable = Enable HPD
                                                                               DdiHpdDisable,   // DDI Port 4 HPD : DdiHpdDisable = Disable, DdiHpdEnable = Enable HPD
                                                                               DdiDisable,      // DDI Port A DDC : DdiDisable = Disable, DdiDdcEnable = Enable DDC
                                                                               DdiDisable,      // DDI Port B DDC : DdiDisable = Disable, DdiDdcEnable = Enable DDC
                                                                               DdiDisable,      // DDI Port C DDC : DdiDisable = Disable, DdiDdcEnable = Enable DDC
                                                                               DdiDdcEnable,    // DDI Port 1 DDC : DdiDisable = Disable, DdiDdcEnable = Enable DDC
                                                                               DdiDisable,      // DDI Port 2 DDC : DdiDisable = Disable, DdiDdcEnable = Enable DDC
                                                                               DdiDisable,      // DDI Port 3 DDC : DdiDisable = Disable, DdiDdcEnable = Enable DDC
                                                                               DdiDisable       // DDI Port 4 DDC : DdiDisable = Disable, DdiDdcEnable = Enable DDC
};

//
// Display DDI settings for Adl-P MB Aep
//
GLOBAL_REMOVE_IF_UNREFERENCED const UINT8 mAdlPMbAepDisplayDdiConfig[16] = {
                                                                               DdiPortEdp,      // DDI Port A Config : DdiPortDisabled = No LFP is Connected, DdiPortEdp = eDP, DdiPortMipiDsi = MIPI DSI
                                                                               DdiPortDisabled, // DDI Port B Config : DdiPortDisabled = No LFP is Connected, DdiPortEdp = eDP, DdiPortMipiDsi = MIPI DSI
                                                                               DdiHpdDisable,   // DDI Port A HPD : DdiHpdDisable = Disable, DdiHpdEnable = Enable HPD
                                                                               DdiHpdDisable,   // DDI Port B HPD : DdiHpdDisable = Disable, DdiHpdEnable = Enable HPD
                                                                               DdiHpdDisable,   // DDI Port C HPD : DdiHpdDisable = Disable, DdiHpdEnable = Enable HPD
                                                                               DdiHpdEnable,    // DDI Port 1 HPD : DdiHpdDisable = Disable, DdiHpdEnable = Enable HPD
                                                                               DdiHpdDisable,   // DDI Port 2 HPD : DdiHpdDisable = Disable, DdiHpdEnable = Enable HPD
                                                                               DdiHpdDisable,   // DDI Port 3 HPD : DdiHpdDisable = Disable, DdiHpdEnable = Enable HPD
                                                                               DdiHpdDisable,   // DDI Port 4 HPD : DdiHpdDisable = Disable, DdiHpdEnable = Enable HPD
                                                                               DdiDisable,      // DDI Port A DDC : DdiDisable = Disable, DdiDdcEnable = Enable DDC
                                                                               DdiDisable,      // DDI Port B DDC : DdiDisable = Disable, DdiDdcEnable = Enable DDC
                                                                               DdiDisable,      // DDI Port C DDC : DdiDisable = Disable, DdiDdcEnable = Enable DDC
                                                                               DdiDisable,      // DDI Port 1 DDC : DdiDisable = Disable, DdiDdcEnable = Enable DDC
                                                                               DdiDisable,      // DDI Port 2 DDC : DdiDisable = Disable, DdiDdcEnable = Enable DDC
                                                                               DdiDisable,      // DDI Port 3 DDC : DdiDisable = Disable, DdiDdcEnable = Enable DDC
                                                                               DdiDisable       // DDI Port 4 DDC : DdiDisable = Disable, DdiDdcEnable = Enable DDC
};

#endif // _BOARD_SA_CONFIG_PRE_MEM_H_
