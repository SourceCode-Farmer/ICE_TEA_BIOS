/** @file
 Source code for the board SA configuration Pcd init functions in Pre-Memory init phase.

@copyright
  INTEL CONFIDENTIAL
  Copyright 2020 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains a 'Sample Driver' and is licensed as such under the terms
  of your license agreement with Intel or your vendor. This file may be modified
  by the user, subject to the additional terms of the license agreement.

@par Specification Reference:
**/

#include "BoardSaConfigPreMem.h"
#include <Library/CpuPlatformLib.h>
#include <Pins/GpioPinsVer2Lp.h>
#include <PlatformBoardId.h>
#include <Library/PreSiliconEnvDetectLib.h>
#include <PlatformBoardConfig.h>
#include <Library/PcdLib.h>
#include <Library/DebugLib.h>
#include <Ppi/ReadOnlyVariable2.h>
#include <SetupVariable.h>
#include <Library/PeiServicesLib.h>
extern EFI_GUID gSetupVariableGuid;
//[-start-210519-KEBIN00001-modify]//
#ifdef C970_SUPPORT
#include <Library/GpioLib.h>
#include <Library/DebugLib.h>
#define GPIO_VER2_LP_GPP_F14             0x090C000E  //Board ID11
#define GPIO_VER2_LP_GPP_F13             0x090C000D  //Board ID10
#define GPIO_VER2_LP_GPP_F12             0x090C000C  //Board ID9
#define GPIO_VER2_LP_GPP_F11             0x090C000B  //Board ID8

static GLOBAL_REMOVE_IF_UNREFERENCED GPIO_INIT_CONFIG YogaC970_GPIO_Table_early_init[] =
{
  {GPIO_VER2_LP_GPP_F14, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirIn,      GpioOutDefault, GpioIntDis,     GpioPlatformReset, GpioTermNone,    GpioLockDefault}},//BOARD_ID11
  {GPIO_VER2_LP_GPP_F11, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirIn,      GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//Board ID8
  {GPIO_VER2_LP_GPP_F12, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirIn,      GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//Board ID9
  {GPIO_VER2_LP_GPP_F13, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirIn,      GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//Board ID10
};


/**
Use to get memory configuraton setting
**/
//kebin poweron
EFI_STATUS
LcfcGpioGetMemoryConfig(
    OUT UINT8      *MemoryConfig
){
  UINT32  BoardID11, BoardID10, BoardID9, BoardID8;
  UINT8   Data8 = 0;
  GpioGetInputValue(GPIO_VER2_LP_GPP_F14,&BoardID11);  
  GpioGetInputValue(GPIO_VER2_LP_GPP_F13,&BoardID10);
  GpioGetInputValue(GPIO_VER2_LP_GPP_F12,&BoardID9);
  GpioGetInputValue(GPIO_VER2_LP_GPP_F11,&BoardID8);

  Data8 |= (UINT8)(BoardID11 << 0);
  Data8 |= (UINT8)(BoardID10 << 1);
  Data8 |= (UINT8)(BoardID9  << 2);
  Data8 |= (UINT8)(BoardID8  << 3);
  _outp(0x72,0xDF);
  _outp(0x73,Data8);
  DEBUG ((DEBUG_INFO, "KEBIN dbg BoardId = 0x%x\n",Data8));
  *MemoryConfig = Data8;
   DEBUG ((DEBUG_INFO, "KEBIN dbg2 BoardId = 0x%x\n",*MemoryConfig));
  return EFI_SUCCESS;
};
#endif
//[-end-210519-KEBIN00001-modify]//
//[start-210720-STORM1100-modify]//
#ifdef C770_SUPPORT
#include <Library/GpioLib.h>
#include <Library/DebugLib.h>
#define GPIO_VER2_LP_GPP_F14             0x090C000E  //Board ID11
#define GPIO_VER2_LP_GPP_F13             0x090C000D  //Board ID10
#define GPIO_VER2_LP_GPP_F12             0x090C000C  //Board ID9
#define GPIO_VER2_LP_GPP_F11             0x090C000B  //Board ID8
#define GPIO_VER2_LP_GPP_R6              0x09030006  //Board ID5
#define GPIO_VER2_LP_GPP_R7              0x09030007  //Board ID6

static GLOBAL_REMOVE_IF_UNREFERENCED GPIO_INIT_CONFIG YogaC770_GPIO_Table_early_init[] =
{
  {GPIO_VER2_LP_GPP_F14, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirIn,      GpioOutDefault, GpioIntDis,     GpioPlatformReset, GpioTermNone,    GpioLockDefault}},//BOARD_ID11
  {GPIO_VER2_LP_GPP_F11, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirIn,      GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//Board ID8
  {GPIO_VER2_LP_GPP_F12, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirIn,      GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//Board ID9
  {GPIO_VER2_LP_GPP_F13, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirIn,      GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//Board ID10
  {GPIO_VER2_LP_GPP_R6,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirIn,      GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//Board ID5
  {GPIO_VER2_LP_GPP_R7,  {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirIn,      GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//Board ID6
};


/**
Use to get memory configuraton setting
**/
//kebin poweron
EFI_STATUS
LcfcGpioGetMemoryConfig(
    OUT UINT8      *MemoryConfig
){
  UINT32  BoardID11, BoardID10, BoardID9, BoardID8;
  UINT8   Data8 = 0;
  GpioGetInputValue(GPIO_VER2_LP_GPP_F14,&BoardID11);  
  GpioGetInputValue(GPIO_VER2_LP_GPP_F13,&BoardID10);
  GpioGetInputValue(GPIO_VER2_LP_GPP_F12,&BoardID9);
  GpioGetInputValue(GPIO_VER2_LP_GPP_F11,&BoardID8);

  Data8 |= (UINT8)(BoardID11 << 0);
  Data8 |= (UINT8)(BoardID10 << 1);
  Data8 |= (UINT8)(BoardID9  << 2);
  Data8 |= (UINT8)(BoardID8  << 3);
  _outp(0x72,0xDF);
  _outp(0x73,Data8);
  DEBUG ((DEBUG_INFO, "KEBIN dbg BoardId = 0x%x\n",Data8));
  *MemoryConfig = Data8;
   DEBUG ((DEBUG_INFO, "KEBIN dbg2 BoardId = 0x%x\n",*MemoryConfig));
  return EFI_SUCCESS;
};


/**
Use to get size to load different DQ & DQS
**/
//kebin poweron
EFI_STATUS
LcfcGpioGetMachineSize(
    OUT UINT8      *MachineSize
){
  UINT32  BoardID5, BoardID6;
  UINT8   Data8 = 0;
  GpioGetInputValue(GPIO_VER2_LP_GPP_R6,&BoardID5);
  GpioGetInputValue(GPIO_VER2_LP_GPP_R7,&BoardID6);

  Data8 |= (UINT8)(BoardID6 << 0);
  Data8 |= (UINT8)(BoardID5 << 1);

  DEBUG ((DEBUG_INFO, "KEBIN dbg Machine Size = 0x%x\n",Data8));
  *MachineSize = Data8;
   DEBUG ((DEBUG_INFO, "KEBIN dbg2 MachineSize = 0x%x\n",*MachineSize));
  return EFI_SUCCESS;
};

#endif

//[end-210720-STORM1100-modify]//

//[-start-210817-DABING0002-modify]//
//[-start-210914-DABING0006-modify]//
#if defined(S77014_SUPPORT) || defined(S77013_SUPPORT) || defined(S77014IAH_SUPPORT)
#include <Library/GpioLib.h>
#include <Library/DebugLib.h>
#define GPIO_VER2_LP_GPP_F14             0x090C000E  //Board ID11
#define GPIO_VER2_LP_GPP_F13             0x090C000D  //Board ID10
#define GPIO_VER2_LP_GPP_F12             0x090C000C  //Board ID9
#define GPIO_VER2_LP_GPP_F11             0x090C000B  //Board ID8

static GLOBAL_REMOVE_IF_UNREFERENCED GPIO_INIT_CONFIG S77014_GPIO_Table_early_init[] =
{
  {GPIO_VER2_LP_GPP_F14, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirIn,      GpioOutDefault, GpioIntDis,     GpioPlatformReset, GpioTermNone,    GpioLockDefault}},//BOARD_ID11
  {GPIO_VER2_LP_GPP_F11, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirIn,      GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//Board ID8
  {GPIO_VER2_LP_GPP_F12, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirIn,      GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//Board ID9
  {GPIO_VER2_LP_GPP_F13, {GpioPadModeGpio,    GpioHostOwnGpio,    GpioDirIn,      GpioOutDefault, GpioIntDis,     GpioHostDeepReset, GpioTermNone,    GpioLockDefault}},//Board ID10
};


/**
Use to get memory configuraton setting
**/
//kebin poweron
EFI_STATUS
LcfcGpioGetMemoryConfig(
    OUT UINT8      *MemoryConfig
){
  UINT32  BoardID11, BoardID10, BoardID9, BoardID8;
  UINT8   Data8 = 0;
  GpioGetInputValue(GPIO_VER2_LP_GPP_F14,&BoardID11);  
  GpioGetInputValue(GPIO_VER2_LP_GPP_F13,&BoardID10);
  GpioGetInputValue(GPIO_VER2_LP_GPP_F12,&BoardID9);
  GpioGetInputValue(GPIO_VER2_LP_GPP_F11,&BoardID8);

  Data8 |= (UINT8)(BoardID11 << 0);
  Data8 |= (UINT8)(BoardID10 << 1);
  Data8 |= (UINT8)(BoardID9  << 2);
  Data8 |= (UINT8)(BoardID8  << 3);
  _outp(0x72,0xDF);
  _outp(0x73,Data8);
  DEBUG ((DEBUG_INFO, "KEBIN dbg BoardId = 0x%x\n",Data8));
  *MemoryConfig = Data8;
   DEBUG ((DEBUG_INFO, "KEBIN dbg2 BoardId = 0x%x\n",*MemoryConfig));
  return EFI_SUCCESS;
};
#endif
//[-end-210914-DABING0006-modify]//
//[-end-210817-DABING0002-modify]//

//[-start-210721-QINGLIN0001-add]//
//[-start-210803-QINGLIN0008-modify]//
//[-start-210802-SHAONN0003-modify]//
//[-start-210910-QINGLIN0060-modify]//
#if defined(S570_SUPPORT) || defined(S370_SUPPORT)
#include <Library/GpioLib.h>
#include <Library/DebugLib.h>
#define GPIO_VER2_LP_GPP_F14             0x090C000E  //Board ID11
#define GPIO_VER2_LP_GPP_F13             0x090C000D  //Board ID10
#define GPIO_VER2_LP_GPP_F12             0x090C000C  //Board ID9
#define GPIO_VER2_LP_GPP_F11             0x090C000B  //Board ID8
//[-start-211229-QINGLIN0136-add]//
#define GPIO_VER2_LP_GPP_A11             0x0902000B  //Board ID7
//[-end-211229-QINGLIN0136-add]//


//[-start-210726-QINGLIN0004-modify]//
static GPIO_INIT_CONFIG  GPIO_Table_early_init[] = {
  //            Pmode,       GPI_IS,      GpioDir,         GPIOTxState,  RxEvCfg,    GPIRoutConfig,    PadRstCfg,      Term,        LockConfig,      Othersetting,      Rsvdbits   
  {GPIO_VER2_LP_GPP_F14,{GpioPadModeGpio, GpioHostOwnGpio, GpioDirIn, GpioOutDefault, GpioIntDis, GpioHostDeepReset,  GpioTermNone, GpioLockDefault}},//BOARD_ID11  
  {GPIO_VER2_LP_GPP_F13,{GpioPadModeGpio, GpioHostOwnGpio, GpioDirIn, GpioOutDefault, GpioIntDis, GpioHostDeepReset,  GpioTermNone, GpioLockDefault}},//BOARD_ID10 
  {GPIO_VER2_LP_GPP_F12,{GpioPadModeGpio, GpioHostOwnGpio, GpioDirIn, GpioOutDefault, GpioIntDis, GpioHostDeepReset,  GpioTermNone, GpioLockDefault}},//BOARD_ID9 
  {GPIO_VER2_LP_GPP_F11,{GpioPadModeGpio, GpioHostOwnGpio, GpioDirIn, GpioOutDefault, GpioIntDis, GpioHostDeepReset,  GpioTermNone, GpioLockDefault}},//BOARD_ID8 
//[-start-211228-QINGLIN0136-add]//
  {GPIO_VER2_LP_GPP_A11,{GpioPadModeGpio, GpioHostOwnGpio, GpioDirIn, GpioOutDefault, GpioIntDis, GpioHostDeepReset,  GpioTermNone, GpioLockDefault}},//BoardID 7
//[-end-211228-QINGLIN0136-add]//
};
//[-end-210726-QINGLIN0004-modify]//

//[-start-210802-Dennis0001-modify]//
//[-start-210802-SHAONN0002-modify]//
#if defined(S570_SUPPORT)
typedef enum {
  Samsung8G_single  = 0x00,   //Sam single 8G channelB  K4AAG165WB-BCWE
  Samsung8G         = 0x01,   //Sam Dual channel 4G+4G  K4A8G165WC-BCWE
  Micron8G_single1  = 0x02,   //Mir single 8G channelB  MT40A1G16RC-062E:B
  Samsung16G        = 0x03,   //Sam Dual channel 8G+8G  K4AAG165WB-BCWE
  Micron8G_single2  = 0x04,   //Mir single 8G channelB  MT40A1G16TB-062E:F
  Micron8G          = 0x05,   //Mir Dual channel 4G+4G  MT40A512M16TB-062E:R
  Micron16G         = 0x06,   //Mir Dual channel 8G+8G  MT40A1G16RC-062E:B
  Micron16G_2nd     = 0x07,   //Mir Dual channel 8G+8G  MT40A1G16TB-062E:F
  Hynix8G_single    = 0x08,   //Hy  single 8G channelB  H5ANAG6NCJR-XNC
  Hynix8G           = 0x09,   //Hy  Dual channel 4G+4G  H5AN8G6NDJR-XNC
  Hynix16G          = 0x0B,   //Hy Dual channel 8G+8G 
//[-start-220101-OWENWU0034-add]//     
  Samsung12G        = 0x0C,   //Sam Dual channel 8G+4G  K4A8G165WB-BCWE  K4A8G165WC-BCWE
  Micron12G         = 0x0D,   //Mir Dual channel 8G+4G  MT40A1G16RC-062E MT40A1G16TB-062E
  Hynix12G          = 0x0E,   //Hy Dual channel 8G+8G   H5ANAG6NCJR-XNC  H5AN8G6NDJR-XNC
//[-end-220101-OWENWU0034-add]//  
} MEMORY_BRAND_TYPE;
#elif defined(S370_SUPPORT)
typedef enum {
  Samsung4G  = 0x00,  //Sam K4A8G165WC-BCWE
  Samsung8G  = 0x01,  //Sam K4AAG165WB-BCWE
  Micron4G   = 0x04,  //Mir MT40A512M16TB-062E:R
  Micron8G   = 0x05,  //Mir MT40A1G16RC-062E:B
  Hynix4G    = 0x08,  //Hy  H5AN8G6NDJR-XNC
  Hynix8G    = 0x09,  //Hy  H5ANAG6NCJR-XNC
//[-start-211228-QINGLIN0136-add]//
  Smart4G    = 0x0C,  //Smart K4A8G165WC-BCWE
  Adata4G    = 0x10,  //Adata ADS4021HY
//[-end-211228-QINGLIN0136-add]//

//[-start-211228-QINGLIN0136-add]//
  Micron8G_170    = 0x0D,  //SM31H94915 Micron HTH5AN8G6NDJR-XND
  Smart8G_170    = 0x0F,  //SM31K40193   Smart 
//[-end-211228-QINGLIN0136-add]//

//[-start-211230-QINGLIN0140-add]//
  Micron8G2nd= 0x15,  //Mir MT40A1G16TB-062E:F
//[-end-211230-QINGLIN0140-add]//
} MEMORY_BRAND_TYPE;
#else
//empty
#endif
//[-end-210802-SHAONN0002-modify]//
//[-end-210802-Dennis0001-modify]//

EFI_STATUS
LcfcGpioGetMemoryConfig(
  OUT UINT8      *MemoryConfig
){
  UINT32  BoardID11, BoardID10, BoardID9, BoardID8;
//[-start-211228-QINGLIN0136-add]//
  UINT32  BoardID7;
//[-end-211228-QINGLIN0136-add]//
  UINT8   Data8 = 0;

  GpioGetInputValue(GPIO_VER2_LP_GPP_F14,&BoardID11);
  GpioGetInputValue(GPIO_VER2_LP_GPP_F13,&BoardID10);
  GpioGetInputValue(GPIO_VER2_LP_GPP_F12,&BoardID9);
  GpioGetInputValue(GPIO_VER2_LP_GPP_F11,&BoardID8);
//[-start-211228-QINGLIN0136-add]//
  GpioGetInputValue(GPIO_VER2_LP_GPP_A11,&BoardID7);
//[-end-211228-QINGLIN0136-add]//

  Data8 |= (UINT8)(BoardID11 << 0);
  Data8 |= (UINT8)(BoardID10 << 1);
  Data8 |= (UINT8)(BoardID9  << 2);
  Data8 |= (UINT8)(BoardID8  << 3);
//[-start-211228-QINGLIN0136-add]//
  Data8 |= (UINT8)(BoardID7  << 4);
//[-end-211228-QINGLIN0136-add]//

  _outp(0x72,0xDF);
  _outp(0x73,Data8);
  *MemoryConfig = Data8;
  return EFI_SUCCESS;
};

#if defined(S570_SUPPORT)
VOID
UpdateMemorySPD_S570(
  VOID 
)
{
  UINT8                            *SpdInChannelA;
  UINT8                            *SpdInChannelB;
  UINTN                            SpdSize1;
  UINTN                            SpdSize2;
  UINT8                            MemoryConfig;
  EFI_STATUS                       Status;
  SPD_DATA                         *SpdDataA;
  SPD_DATA                         *SpdDataB;

//[-start-220101-OWENWU0034-modify]//
  GpioConfigurePads (5, GPIO_Table_early_init);
//[-end-220101-OWENWU0034-modify]//
  
  Status = LcfcGpioGetMemoryConfig (&MemoryConfig);
  
  switch (MemoryConfig) {
    case Samsung8G_single: //0x00
      SpdDataA = PcdGetPtr (DDR4NullSpdData);
      SpdDataB = PcdGetPtr (DDR4Samsung8GSpdData);
      break;
    case Samsung8G: //0x01
      SpdDataA = PcdGetPtr (DDR4Samsung4GSpdData);
      SpdDataB = PcdGetPtr (DDR4Samsung4GSpdData);
      break;
    case Samsung16G: //0x03
      SpdDataA = PcdGetPtr (DDR4Samsung8GSpdData);
      SpdDataB = PcdGetPtr (DDR4Samsung8GSpdData);
      break;
    case Micron8G_single1: //0x02
      SpdDataA = PcdGetPtr (DDR4NullSpdData);
      SpdDataB = PcdGetPtr (DDR4Micron8GSpdData);
      break;
    case Micron8G_single2: //0x04
      SpdDataA = PcdGetPtr (DDR4NullSpdData);
      SpdDataB = PcdGetPtr (DDR4Micron8G2ndSpdData);
      break;
    case Micron8G: //0x05
      SpdDataA = PcdGetPtr (DDR4Micron4GSpdData);
      SpdDataB = PcdGetPtr (DDR4Micron4GSpdData);
      break;
    case Micron16G: //0x06
      SpdDataA = PcdGetPtr (DDR4Micron8GSpdData);
      SpdDataB = PcdGetPtr (DDR4Micron8GSpdData);
      break;
    case Micron16G_2nd: //0x07
      SpdDataA = PcdGetPtr (DDR4Micron8G2ndSpdData);
      SpdDataB = PcdGetPtr (DDR4Micron8G2ndSpdData);
      break;
    case Hynix8G_single: //0x08
      SpdDataA = PcdGetPtr (DDR4NullSpdData);
      SpdDataB = PcdGetPtr (DDR4Hynix8GSpdData);
      break;
    case Hynix8G: //0x09
      SpdDataA = PcdGetPtr (DDR4Hynix4GSpdData);
      SpdDataB = PcdGetPtr (DDR4Hynix4GSpdData);
      break;
    case Hynix16G: //0x0B
      SpdDataA = PcdGetPtr (DDR4Hynix8GSpdData);
      SpdDataB = PcdGetPtr (DDR4Hynix8GSpdData);
      break;
//[-start-220101-OWENWU0034-add]//
//[-start-220118-OWENWU0035-modify]//
    case Samsung12G:
	  SpdDataA = PcdGetPtr (DDR4Samsung4GSpdData);
      SpdDataB = PcdGetPtr (DDR4Samsung8GSpdData);
      break;
    case Micron12G:
	  SpdDataA = PcdGetPtr (DDR4Micron4GSpdData);
      SpdDataB = PcdGetPtr (DDR4Micron8GSpdData);
      break;
	case Hynix12G:
      SpdDataA = PcdGetPtr (DDR4Hynix4GSpdData);
      SpdDataB = PcdGetPtr (DDR4Hynix8GSpdData);
      break;  
//[-end-220118-OWENWU0035-modify]// 	  
//[-end-220101-OWENWU0034-add]//  	  
    default:
      SpdDataA = PcdGetPtr (DDR4NullSpdData);
      SpdDataB = PcdGetPtr (DDR4NullSpdData);
      break;
  }

  SpdSize1 = 512;
  SpdInChannelA = SpdDataA->SpdData;
  SpdSize2 = 512;
  SpdInChannelB = SpdDataB->SpdData;
  
  PcdSetPtrS (PcdH2OMemoryDownSpdDataMc0Ch0Dimm0, &SpdSize1, (VOID *) SpdInChannelA);
  PcdSetPtrS (PcdH2OMemoryDownSpdDataMc1Ch0Dimm0, &SpdSize2, (VOID *) SpdInChannelB);
  DEBUG((DEBUG_ERROR, "[LCFC][S570]-CopyMemoryDownSpd Status = %r\n", Status));
  DEBUG((DEBUG_ERROR, "[LCFC][S570]-BoardID GPIO Value = %x\n", MemoryConfig));
}

VOID
UpdateSPDAddress_S570(
  VOID 
){
  PcdSet8S (PcdMrcSpdAddressTable0, 0x00);
  PcdSet8S (PcdMrcSpdAddressTable1, 0x00);
  PcdSet8S (PcdMrcSpdAddressTable2, 0x00);
  PcdSet8S (PcdMrcSpdAddressTable3, 0x00);
  PcdSet8S (PcdMrcSpdAddressTable4, 0x00);
  PcdSet8S (PcdMrcSpdAddressTable5, 0x00);
  PcdSet8S (PcdMrcSpdAddressTable6, 0x00);
  PcdSet8S (PcdMrcSpdAddressTable7, 0x00);
  PcdSet8S (PcdMrcSpdAddressTable8, 0x00);
  PcdSet8S (PcdMrcSpdAddressTable9, 0x00);
  PcdSet8S (PcdMrcSpdAddressTable10, 0x00);
  PcdSet8S (PcdMrcSpdAddressTable11, 0x00);
  PcdSet8S (PcdMrcSpdAddressTable12, 0x00);
  PcdSet8S (PcdMrcSpdAddressTable13, 0x00);
  PcdSet8S (PcdMrcSpdAddressTable14, 0x00);
  PcdSet8S (PcdMrcSpdAddressTable15, 0x00);
}
#endif

#if defined(S370_SUPPORT)
VOID
UpdateMemorySPD_S370(
  VOID 
)
{
  UINT8                            *SpdInChannelB;
  UINTN                            SpdSize2;
  UINT8                            MemoryConfig;
  EFI_STATUS                       Status;
  SPD_DATA                         *SpdDataB;

//[-start-211229-QINGLIN0136-modify]//
  GpioConfigurePads (5, GPIO_Table_early_init);
//[-end-211229-QINGLIN0136-modify]//
  
  Status = LcfcGpioGetMemoryConfig (&MemoryConfig);

  switch (MemoryConfig) {
    case Samsung4G:
      SpdDataB = PcdGetPtr (DDR4Samsung4GSpdData);
      break;
    case Samsung8G:
      SpdDataB = PcdGetPtr (DDR4Samsung8GSpdData);
      break;
    case Micron4G:
      SpdDataB = PcdGetPtr (DDR4Micron4GSpdData);
      break;
    case Micron8G:
      SpdDataB = PcdGetPtr (DDR4Micron8GSpdData);
      break;
    case Hynix4G:
      SpdDataB = PcdGetPtr (DDR4Hynix4GSpdData);
      break;
    case Hynix8G:
      SpdDataB = PcdGetPtr (DDR4Hynix8GSpdData);
      break;
//[-start-211228-QINGLIN0136-add]//
    case Smart4G:
      SpdDataB = PcdGetPtr (DDR4Smart4GSpdData);
      break;
    case Adata4G:
      SpdDataB = PcdGetPtr (DDR4Adata4GSpdData);
      break;
//[-end-211228-QINGLIN0136-add]//
//[-start-211228-QINGLIN0136-add]//
    case Micron8G_170: //SM31H94915 Micron HTH5AN8G6NDJR-XND
      SpdDataB = PcdGetPtr (DDR4Micron8G170SpdData);
      break;
    case Smart8G_170: //SM31K40193   Smart 
      SpdDataB = PcdGetPtr (DDR4Smart8G170SpdData);
      break;
//[-end-211228-QINGLIN0136-add]//
//[-start-211230-QINGLIN0140-add]//
    case Micron8G2nd:
      SpdDataB = PcdGetPtr (DDR4Micron8G2ndSpdData);
      break;
//[-end-211230-QINGLIN0140-add]//
    default:
      SpdDataB = PcdGetPtr (DDR4NullSpdData);
      break;
  }

  SpdSize2 = 512;
  SpdInChannelB = SpdDataB->SpdData;

  PcdSetPtrS (PcdH2OMemoryDownSpdDataMc1Ch0Dimm0, &SpdSize2, (VOID *) SpdInChannelB);
  DEBUG((DEBUG_ERROR, "[LCFC][S370]-CopyMemoryDownSpd Status = %r\n", Status));
  DEBUG((DEBUG_ERROR, "[LCFC][S370]-BoardID GPIO Value = %x\n", MemoryConfig));
}

VOID
UpdateSPDAddress_S370(
  VOID 
){
  PcdSet8S (PcdMrcSpdAddressTable0, 0xA0);
  PcdSet8S (PcdMrcSpdAddressTable1, 0x00);
  PcdSet8S (PcdMrcSpdAddressTable2, 0x00);
  PcdSet8S (PcdMrcSpdAddressTable3, 0x00);
  PcdSet8S (PcdMrcSpdAddressTable4, 0x00);
  PcdSet8S (PcdMrcSpdAddressTable5, 0x00);
  PcdSet8S (PcdMrcSpdAddressTable6, 0x00);
  PcdSet8S (PcdMrcSpdAddressTable7, 0x00);
  PcdSet8S (PcdMrcSpdAddressTable8, 0x00);
  PcdSet8S (PcdMrcSpdAddressTable9, 0x00);
  PcdSet8S (PcdMrcSpdAddressTable10, 0x00);
  PcdSet8S (PcdMrcSpdAddressTable11, 0x00);
  PcdSet8S (PcdMrcSpdAddressTable12, 0x00);
  PcdSet8S (PcdMrcSpdAddressTable13, 0x00);
  PcdSet8S (PcdMrcSpdAddressTable14, 0x00);
  PcdSet8S (PcdMrcSpdAddressTable15, 0x00);
}
#endif

#endif
//[-end-210910-QINGLIN0060-modify]//
//[-end-210802-SHAONN0003-modify]//
//[-end-210803-QINGLIN0008-modify]//
//[-end-210721-QINGLIN0001-add]//
/**
  MRC configuration init function for PEI pre-memory phase.

  @param[in]  VOID

  @retval VOID
**/
VOID
AdlPSaMiscConfigInit (
  VOID
  )
{

//[-start-210916-Dongxu0018-modify]//
#ifdef LCFC_SUPPORT
 #if defined(C970_SUPPORT)
   PcdSet8S (PcdSaMiscUserBd, 6);
 #else
   PcdSet8S (PcdSaMiscUserBd, 5);
 #endif  
#else
  switch (PcdGet16(PcdBoardId)) {
    case BoardIdAdlPT3Lp5Rvp:
      // Type3 RVP
      PcdSet8S (PcdSaMiscUserBd, 5);        // btUlxUltType3
      break;

    default:
      // Type4 RVP
      PcdSet8S (PcdSaMiscUserBd, 6);        // btUlxUltType4
      break;
  }
#endif
//[-end-210916-Dongxu0018-modify]//
  PcdSet16S (PcdSaDdrFreqLimit, 0);

  return;
}

/**
  Board Memory Init related configuration init function for PEI pre-memory phase.

  @param[in]  VOID

  @retval VOID
**/
VOID
AdlPMrcConfigInit (
  VOID
  )
{
  UINT16    BoardId;
  BOOLEAN   ExternalSpdPresent;
  MRC_DQS   *MrcDqs;
  MRC_DQ    *MrcDq;
  SPD_DATA  *SpdData;
//[-start-210519-KEBIN00001-add]//
#ifdef C970_SUPPORT
  UINT8                              MemoryConfig;
  EFI_STATUS                         Status;
#endif

//[start-210720-STORM1100-modify]//
#ifdef C770_SUPPORT
  UINT8                              MemoryConfig;
  UINT8                              MachineSize;
  EFI_STATUS                         Status;
#endif
//[end-210720-STORM1100-modify]//

//[-end-210519-KEBIN00001-add]//
//[-start-210817-DABING0002-modify]//
//[-start-210914-DABING0006-modify]//
#if defined(S77014_SUPPORT) || defined(S77013_SUPPORT) || defined(S77014IAH_SUPPORT)
  UINT8                              MemoryConfig;
  EFI_STATUS                         Status;
#endif
//[-end-210914-DABING0006-modify]//
//[-end-210817-DABING0002-modify]//

  BoardId = PcdGet16(PcdBoardId);

  // SPD is the same size for all boards
//[-start-210803-QINGLIN0008-modify]//
#if !defined(S570_SUPPORT) && !defined(S370_SUPPORT)
  PcdSet16S (PcdMrcSpdDataSize, 512);
#endif
//[-end-210803-QINGLIN0008-modify]//

  ExternalSpdPresent = PcdGetBool (PcdSpdPresent);

  // Assume internal SPD is used
  PcdSet8S (PcdMrcSpdAddressTable0,  0);
  PcdSet8S (PcdMrcSpdAddressTable1,  0);
  PcdSet8S (PcdMrcSpdAddressTable2,  0);
  PcdSet8S (PcdMrcSpdAddressTable3,  0);
  PcdSet8S (PcdMrcSpdAddressTable4,  0);
  PcdSet8S (PcdMrcSpdAddressTable5,  0);
  PcdSet8S (PcdMrcSpdAddressTable6,  0);
  PcdSet8S (PcdMrcSpdAddressTable7,  0);
  PcdSet8S (PcdMrcSpdAddressTable8,  0);
  PcdSet8S (PcdMrcSpdAddressTable9,  0);
  PcdSet8S (PcdMrcSpdAddressTable10, 0);
  PcdSet8S (PcdMrcSpdAddressTable11, 0);
  PcdSet8S (PcdMrcSpdAddressTable12, 0);
  PcdSet8S (PcdMrcSpdAddressTable13, 0);
  PcdSet8S (PcdMrcSpdAddressTable14, 0);
  PcdSet8S (PcdMrcSpdAddressTable15, 0);

  // Check for external SPD presence
  if (ExternalSpdPresent) {
    switch (BoardId) {
      case BoardIdAdlPDdr5Rvp:
      case BoardIdAdlPDdr4Rvp:
      case BoardIdAdlPDdr5Dg384Aep:
      case BoardIdAdlPDdr5MRRvp:
      case BoardIdAdlPMMAep:
        PcdSet8S (PcdMrcSpdAddressTable0,  0xA0);
        PcdSet8S (PcdMrcSpdAddressTable1,  0xA2);
        PcdSet8S (PcdMrcSpdAddressTable8,  0xA4);
        PcdSet8S (PcdMrcSpdAddressTable9,  0xA6);
        break;
      case BoardIdAdlPLp4Bep:
      case BoardIdAdlPLp4Rvp:
      case BoardIdAdlPLp5Rvp:
      case BoardIdAdlPT3Lp5Rvp:
      case BoardIdAdlPLp5Aep:
      case BoardIdAdlPLp5Gcs:
      case BoardIdAdlPLp5Dg128Aep:
      case BoardIdAdlPLp5MbAep:
        PcdSet8S (PcdMrcSpdAddressTable0,  0xA8);
        PcdSet8S (PcdMrcSpdAddressTable2,  0xA8);
        PcdSet8S (PcdMrcSpdAddressTable4,  0xA8);
        PcdSet8S (PcdMrcSpdAddressTable6,  0xA8);
        PcdSet8S (PcdMrcSpdAddressTable8,  0xA8);
        PcdSet8S (PcdMrcSpdAddressTable10, 0xA8);
        PcdSet8S (PcdMrcSpdAddressTable12, 0xA8);
        PcdSet8S (PcdMrcSpdAddressTable14, 0xA8);
        break;
      default:
        break;
    }
  }
//[-start-210519-KEBIN00001-modify]//
#ifdef C970_SUPPORT
  GpioConfigurePads (4, YogaC970_GPIO_Table_early_init);
  Status = LcfcGpioGetMemoryConfig (&MemoryConfig);
#endif
//[-end-210519-KEBIN00001-modify]//

//[start-210720-STORM1100-modify]//
#ifdef C770_SUPPORT
    GpioConfigurePads (6, YogaC770_GPIO_Table_early_init);
    Status = LcfcGpioGetMemoryConfig (&MemoryConfig);
#endif
//[end-210720-STORM1100-modify]//
//[-start-210817-DABING0002-modify]//
//[-start-210914-DABING0006-modify]//
#if defined(S77014_SUPPORT) || defined(S77013_SUPPORT) || defined(S77014IAH_SUPPORT)
  GpioConfigurePads (4, S77014_GPIO_Table_early_init);
  Status = LcfcGpioGetMemoryConfig (&MemoryConfig);
#endif
//[-end-210914-DABING0006-modify]//
//[-end-210817-DABING0002-modify]//

  // Setting the default DQ Byte Map. It may be overriden to board specific settings below.
  PcdSet32S (PcdMrcDqByteMap, (UINTN) DqByteMapAdlP);
  PcdSet16S (PcdMrcDqByteMapSize, sizeof (DqByteMapAdlP));

  // ADL uses the same RCOMP resistors for all DDR types
  PcdSet32S (PcdMrcRcompResistor, (UINTN) AdlPRcompResistorZero);

  // Use default RCOMP target values for all boards
  PcdSet32S (PcdMrcRcompTarget, (UINTN) RcompTargetAdlP);

  // Default is NIL
  PcdSetBoolS (PcdMrcDqPinsInterleavedControl, TRUE);
  PcdSetBoolS (PcdMrcDqPinsInterleaved, FALSE);

  // DqsMapCpu2Dram is the same size for all boards
  PcdSet16S (PcdMrcDqsMapCpu2DramSize, sizeof (MRC_DQS));
  // DqMapCpu2Dram is the same size for all boards
  PcdSet16S (PcdMrcDqMapCpu2DramSize, sizeof (MRC_DQ));

   switch (BoardId) {
    case BoardIdAdlPLp5Rvp:
    case BoardIdAdlPT3Lp5Rvp:
    case BoardIdAdlPLp5Aep:
    case BoardIdAdlPLp5Gcs:
    case BoardIdAdlPLp5MbAep:
      PcdSet8S (PcdMrcLp5CccConfig, 0xFF);      // ADL-L LP5 RVP uses descending CCC order
      break;
    case BoardIdAdlPLp5Dg128Aep:
      PcdSet8S (PcdMrcLp5CccConfig, 0x99);      // Ascending and Descending config
      break;
    default:
      PcdSet8S (PcdMrcLp5CccConfig, 0x0);
      break;
   }

//[-start-210519-KEBIN00001-modify]//
//[-start-210608-KEBIN00010-modify]//
#ifdef LCFC_SUPPORT
  // CPU-DRAM DQ mapping
 #ifdef C970_SUPPORT
    MrcDq = PcdGetPtr (YogaC9VpdPcdMrcDqMapCpu2Dram);
  if (MrcDq != NULL) {
    PcdSet32S (PcdMrcDqMapCpu2Dram, (UINTN)MrcDq->DqMapCpu2Dram);
  }

  // CPU-DRAM DQS mapping
  MrcDqs = PcdGetPtr (YogaC9VpdPcdMrcDqsMapCpu2Dram);
  if (MrcDqs != NULL) {
    PcdSet32S (PcdMrcDqsMapCpu2Dram, (UINTN)MrcDqs->DqsMapCpu2Dram);
  }
  #endif

//[start-210720-STORM1100-modify]//
#ifdef C770_SUPPORT

  Status = LcfcGpioGetMachineSize (&MachineSize);

  if(2 == MachineSize) {
// CPU-DRAM DQ mapping
    MrcDq = PcdGetPtr (YogaC716VpdPcdMrcDqMapCpu2Dram);
    if (MrcDq != NULL) {
      PcdSet32S (PcdMrcDqMapCpu2Dram, (UINTN)MrcDq->DqMapCpu2Dram);
    }
// CPU-DRAM DQS mapping
    MrcDqs = PcdGetPtr (YogaC716VpdPcdMrcDqsMapCpu2Dram);
    if (MrcDqs != NULL) {
      PcdSet32S (PcdMrcDqsMapCpu2Dram, (UINTN)MrcDqs->DqsMapCpu2Dram);
    }
  } else {
// CPU-DRAM DQ mapping
    MrcDq = PcdGetPtr (YogaC714VpdPcdMrcDqMapCpu2Dram);
    if (MrcDq != NULL) {
      PcdSet32S (PcdMrcDqMapCpu2Dram, (UINTN)MrcDq->DqMapCpu2Dram);
    }
// CPU-DRAM DQS mapping
    MrcDqs = PcdGetPtr (YogaC714VpdPcdMrcDqsMapCpu2Dram);
    if (MrcDqs != NULL) {
      PcdSet32S (PcdMrcDqsMapCpu2Dram, (UINTN)MrcDqs->DqsMapCpu2Dram);
    }
  }
#endif
//[end-210720-STORM1100-modify]//

//[-start-210817-DABING0002-modify]//
 #if defined(S77014_SUPPORT) || defined(S77014IAH_SUPPORT)
    MrcDq = PcdGetPtr (S77014VpdPcdMrcDqMapCpu2Dram);
  if (MrcDq != NULL) {
    PcdSet32S (PcdMrcDqMapCpu2Dram, (UINTN)MrcDq->DqMapCpu2Dram);
  }

  // CPU-DRAM DQS mapping
  MrcDqs = PcdGetPtr (S77014VpdPcdMrcDqsMapCpu2Dram);
  if (MrcDqs != NULL) {
    PcdSet32S (PcdMrcDqsMapCpu2Dram, (UINTN)MrcDqs->DqsMapCpu2Dram);
  }
  #endif
//[-end-210817-DABING0002-modify]//

//[-start-210914-DABING0006-modify]//
 #ifdef S77013_SUPPORT
    MrcDq = PcdGetPtr (S77013VpdPcdMrcDqMapCpu2Dram);
  if (MrcDq != NULL) {
    PcdSet32S (PcdMrcDqMapCpu2Dram, (UINTN)MrcDq->DqMapCpu2Dram);
  }

  // CPU-DRAM DQS mapping
  MrcDqs = PcdGetPtr (S77013VpdPcdMrcDqsMapCpu2Dram);
  if (MrcDqs != NULL) {
    PcdSet32S (PcdMrcDqsMapCpu2Dram, (UINTN)MrcDqs->DqsMapCpu2Dram);
  }
  #endif
//[-end-210914-DABING0006-modify]//

//[-start-210721-QINGLIN0001-add]//
//[-start-210803-QINGLIN0008-modify]//
//[-start-210802-SHAONN0003-modify]//
// #ifdef S570_SUPPORT
 #if defined(S570_SUPPORT) || defined(S370_SUPPORT)
   MrcDq = PcdGetPtr (VpdPcdMrcDqMapCpu2Dram);
   if (MrcDq != NULL) {
     PcdSet32S (PcdMrcDqMapCpu2Dram, (UINTN)MrcDq->DqMapCpu2Dram);
   }
   
   // CPU-DRAM DQS mapping
   MrcDqs = PcdGetPtr (VpdPcdMrcDqsMapCpu2Dram);
   if (MrcDqs != NULL) {
     PcdSet32S (PcdMrcDqsMapCpu2Dram, (UINTN)MrcDqs->DqsMapCpu2Dram);
   }
 #endif
//[-end-210802-SHAONN0003-modify]//
//[-end-210803-QINGLIN0008-modify]//
//[-end-210721-QINGLIN0001-add]//

 #else
  MrcDq = PcdGetPtr (VpdPcdMrcDqMapCpu2Dram);
  if (MrcDq != NULL) {
    PcdSet32S (PcdMrcDqMapCpu2Dram, (UINTN)MrcDq->DqMapCpu2Dram);
  }

  // CPU-DRAM DQS mapping
  MrcDqs = PcdGetPtr (VpdPcdMrcDqsMapCpu2Dram);
  if (MrcDqs != NULL) {
    PcdSet32S (PcdMrcDqsMapCpu2Dram, (UINTN)MrcDqs->DqsMapCpu2Dram);
  }
#endif

  // DRAM SPD Data
#ifdef C970_SUPPORT
DEBUG ((DEBUG_INFO, "kebin dbg MemoryConfig = 0x%x\n",MemoryConfig));  

//[-start-210526-KEBIN00005-modify]//
//[-start-210702-YUNLEI0109-modify]//
switch (MemoryConfig) {
  case 2:
  SpdData = PcdGetPtr (Yogac970Samsung16GSpdData); //Samsung 16G
  break;

  case 0:
  case 4:
//[-start-210709-YUNLEI0111-modify]//
  SpdData = PcdGetPtr (Yogac970MO1D1R8GSpdData); //Micron 8G
  break;
  case 8:
  SpdData = PcdGetPtr (Yogac970hynix8GSpdData); //Hynix 8G
  break;
  
  case 10:
  SpdData = PcdGetPtr (Yogac970hynix16GSpdData);//Hynix 16G
  break;
  case 6:
  SpdData = PcdGetPtr (Yogac970MO1Q2R16GSpdData);//Micron 16G
  break;

  case 7:
  SpdData = PcdGetPtr (Yogac970MO2O2R32GSpdData); //Micron 32G
  break;
 //[-end-210709-YUNLEI0111-modify]//
  default:
  SpdData = PcdGetPtr (Yogac970MO1Q2R16GSpdData);
  break;
   }
//[-end-210702-YUNLEI0109-modify]//
#else 
//[start-210720-STORM1100-modify]//
  #ifdef C770_SUPPORT
    DEBUG ((DEBUG_INFO, "kebin dbg MemoryConfig = 0x%x\n",MemoryConfig));
//[-start-210726-YUNLEI0113-modify]//
    switch (MemoryConfig) {
      case 2:
	  SpdData = PcdGetPtr (Yogac970Samsung16GSpdData); //Samsung 16G  same as C970
	  break;
		 
	  case 3:
      SpdData = PcdGetPtr (Yogac770Samsung32GSpdData); //Samsung 32G 
      break;
		 
      case 4:
	  SpdData = PcdGetPtr (Yogac970MO1D1R8GSpdData); //Micron 8G same as C970
	  break;
		 
	  case 6:
	  SpdData = PcdGetPtr (Yogac970MO1Q2R16GSpdData);//Micron 16G   same as C970
	  break;
	 
	  case 7:
	  SpdData = PcdGetPtr (Yogac970MO2O2R32GSpdData); //Micron 32G  same as C970
	  break;
	  
	  case 8:
	  SpdData = PcdGetPtr (Yogac970hynix8GSpdData); //Hynix 8G  same as C970
	  break;
		 
	  case 10:
	  SpdData = PcdGetPtr (Yogac970hynix16GSpdData);//Hynix 16G  same as C970
	  break;
		 
	  case 11:
	  SpdData = PcdGetPtr (Yogac770hynix32GSpdData); ////Hynix 32G
	  break;

      default:
      SpdData = PcdGetPtr (Yogac970MO1D1R8GSpdData);  //Micron 8G
      break;
     }
//[-end-210726-YUNLEI0113-modify]//
//[-start-210817-DABING0002-modify]//
//[-start-210914-DABING0006-modify]//
  #else
    #if defined(S77014_SUPPORT) || defined(S77013_SUPPORT) || defined(S77014IAH_SUPPORT)
      DEBUG ((DEBUG_INFO, "kebin dbg MemoryConfig = 0x%x\n",MemoryConfig));

       switch (MemoryConfig) {	
//[-start-210825-TAMT000002-add]//	 
         case 2:
         SpdData = PcdGetPtr (Yogac970Samsung16GSpdData); //Samsung 16G same as C970
         break;

         case 4:
         SpdData = PcdGetPtr (Yogac970MO1D1R8GSpdData); //Micron 8G same as C970
         break;

         case 6:
         SpdData = PcdGetPtr (Yogac970MO1Q2R16GSpdData);//Micron 16G  same as C970
         break;

         case 8:
         SpdData = PcdGetPtr (Yogac970hynix8GSpdData); //Hynix 8G  same as C970
         break;

         case 10:
         SpdData = PcdGetPtr (Yogac970hynix16GSpdData);//Hynix 16G  same as C970
         break;

       default:
       SpdData = PcdGetPtr (Yogac970Samsung16GSpdData);  // Yogac970Samsung16GSpdData
       break;
//[-end-210914-DABING0006-modify]//	   
//[-end-210825-TAMT000002-add]//	 
        }
    #else 
       SpdData = PcdGetPtr (VpdPcdMrcSpdData);
    #endif
  #endif
//[-end-210817-DABING0002-modify]//  
//[end-210720-STORM1100-modify]//
#endif
//[-end-210526-KEBIN00005-modify]//
//[-end-210519-KEBIN00001-modify]//
//[-end-210608-KEBIN00010-modify]//
  if (SpdData != NULL) {
    if (SpdData->OverrideSpd == TRUE) {
      PcdSet32S (PcdMrcSpdData, (UINTN)SpdData->SpdData);
    }
  }

  //
  // CA Vref routing: board-dependent
  // 0 - VREF_CA goes to both CH_A and CH_B (LPDDR3/DDR3L)
  // 1 - VREF_CA to CH_A, VREF_DQ_A to CH_B (should not be used)
  // 2 - VREF_CA to CH_A, VREF_DQ_B to CH_B (DDR4)
  //
  // Set it to 2 for all our DDR4 boards; it is ignored for LPDDR4
  //
  PcdSet8S (PcdMrcCaVrefConfig, 2);

//[-start-210721-QINGLIN0001-add]//
#if defined(S570_SUPPORT)
  UpdateSPDAddress_S570();
  UpdateMemorySPD_S570();
#endif
//[-end-210721-QINGLIN0001-add]//
//[-start-210803-QINGLIN0008-modify]//
#if defined(S370_SUPPORT)
  UpdateSPDAddress_S370();
  UpdateMemorySPD_S370();
#endif
//[-end-210803-QINGLIN0008-modify]//

  return;
}

/**
  Board SA related GPIO configuration init function for PEI pre-memory phase.

  @param[in]  VOID

  @retval VOID
**/
VOID
AdlPSaGpioConfigInit (
  VOID
  )
{
  UINT16    BoardId;
  BoardId = PcdGet16(PcdBoardId);
  EFI_PEI_READ_ONLY_VARIABLE2_PPI   *VariableServices;
  EFI_STATUS                        Status;
  SETUP_DATA                        Setup;
  UINTN                             DataSize;

  Status = PeiServicesLocatePpi (
             &gEfiPeiReadOnlyVariable2PpiGuid,
             0,
             NULL,
             (VOID **) &VariableServices
             );

  DataSize = sizeof (SETUP_DATA);
  Status = VariableServices->GetVariable (
                                 VariableServices,
                                 L"Setup",
                                 &gSetupVariableGuid,
                                 NULL,
                                 &DataSize,
                                 &Setup
                                 );
  if (Status == EFI_SUCCESS) {
    ASSERT_EFI_ERROR (Status);
  }
  //
  // Assigning default values to PCIE RTD3 GPIOs
  //
  switch (BoardId) {
    case BoardIdAdlPLp5Rvp:
    case BoardIdAdlPT3Lp5Rvp:
    case BoardIdAdlPLp4Rvp:
    case BoardIdAdlPDdr5Rvp:
    case BoardIdAdlPDdr4Rvp:
    case BoardIdAdlPDdr5MRRvp:
      PcdSetBoolS(PcdPcieSsd2RstGpioPolarity, PIN_GPIO_ACTIVE_LOW);       // M.2 CPU SSD 1
      PcdSetBoolS(PcdPcieSsd3RstGpioPolarity, PIN_GPIO_ACTIVE_LOW);       // M.2 CPU SSD 2
      PcdSetBoolS(PcdPcieDG2RstGpioPolarity, PIN_GPIO_ACTIVE_LOW);        // CPU M.2 DG
      break;
    default:
      break;
  }

  switch (BoardId) {
    case BoardIdAdlPLp5Rvp:
    case BoardIdAdlPT3Lp5Rvp:
    case BoardIdAdlPLp4Rvp:
      PcdSet8S (PcdRootPortIndex, 4);
      PcdSet8S (PcdPcieSlot1GpioSupport, PchGpio);
      PcdSet8S (PcdPcieSlot1HoldRstExpanderNo, 0);
      PcdSet8S (PcdPcieSlot1PwrEnableExpanderNo, 0);
//[-start-211114-JEPLIUT195-Remove]//      
#if defined(S570_SUPPORT)
            
#else
      //
      // Configure CPU M.2 SSD 1 GPIO PCDs
      //
      PcdSet32S (PcdPcieSsd2PwrEnableGpioNo, GPIO_VER2_LP_GPP_D14);
      PcdSet32S (PcdPcieSsd2RstGpioNo, GPIO_VER2_LP_GPP_F20);
      PcdSetBoolS (PcdPcieSsd2PwrEnableGpioPolarity, PIN_GPIO_ACTIVE_HIGH);
      //
      // Configure CPU M.2 SSD 2 GPIO PCDs
      //
      PcdSet32S (PcdPcieSsd3PwrEnableGpioNo, GPIO_VER2_LP_GPP_C2);
      PcdSet32S (PcdPcieSsd3RstGpioNo, GPIO_VER2_LP_GPP_H1);
#endif
//[-end-211114-JEPLIUT195-Remove]//    
      PcdSetBoolS (PcdPcieSsd3PwrEnableGpioPolarity, PIN_GPIO_ACTIVE_HIGH);
      //
      // Configure PEG X8 DG/DG2 GPIO PCDs
      //
//[start-210731-STORM1101-modify]//
#if defined(C770_SUPPORT)
//      PcdSet32S(PcdPcieDG2PwrEnableGpioNo, GPIO_VER2_LP_GPP_A14);
//      PcdSet32S(PcdPcieDG2RstGpioNo, GPIO_VER2_LP_GPP_B2);
//      PcdSetBoolS(PcdPcieDG2PwrEnableGpioPolarity, PIN_GPIO_ACTIVE_LOW);
//      PcdSet32S (PcdPegSlot1WakeGpioPin, GPIO_VER2_LP_GPP_A20);
      PcdSet32S(PcdPcieDG2PwrEnableGpioNo, GPIO_VER2_LP_GPP_E10);
      PcdSet32S(PcdPcieDG2RstGpioNo, GPIO_VER2_LP_GPP_A15);
      PcdSetBoolS(PcdPcieDG2PwrEnableGpioPolarity, PIN_GPIO_ACTIVE_HIGH);
      PcdSet32S (PcdPegSlot1WakeGpioPin, GPIO_VER2_LP_GPP_A20);
//[-start-210915-GEORGE0004-add]//
#elif defined(S77014_SUPPORT) || defined(S77014IAH_SUPPORT)
      PcdSet32S(PcdPcieDG2PwrEnableGpioNo, GPIO_VER2_LP_GPP_E11);
      PcdSet32S(PcdPcieDG2RstGpioNo, GPIO_VER2_LP_GPP_H13);
      PcdSetBoolS(PcdPcieDG2PwrEnableGpioPolarity, PIN_GPIO_ACTIVE_HIGH);
      PcdSet32S (PcdPegSlot1WakeGpioPin, GPIO_VER2_LP_GPP_A20);
//[-end-210915-GEORGE0004-add]//
#else
      PcdSet32S(PcdPcieDG2PwrEnableGpioNo, GPIO_VER2_LP_GPP_A14);
      PcdSet32S(PcdPcieDG2RstGpioNo, GPIO_VER2_LP_GPP_B2);
      PcdSetBoolS(PcdPcieDG2PwrEnableGpioPolarity, PIN_GPIO_ACTIVE_LOW);
      PcdSet32S (PcdPegSlot1WakeGpioPin, GPIO_VER2_LP_GPP_A20);
#endif
//[end-210731-STORM1101-modify]//

      break;
    case BoardIdAdlPDdr5MRRvp:
      PcdSet8S (PcdRootPortIndex, 4);
      PcdSet8S (PcdPcieSlot1GpioSupport, PchGpio);
      PcdSet8S (PcdPcieSlot1HoldRstExpanderNo, 0);
      PcdSet8S (PcdPcieSlot1PwrEnableExpanderNo, 0);
      //
      // Configure CPU M.2 SSD 1 GPIO PCDs
      //
      PcdSet32S (PcdPcieSsd2PwrEnableGpioNo, GPIO_VER2_LP_GPP_D14);
      PcdSet32S (PcdPcieSsd2RstGpioNo, GPIO_VER2_LP_GPP_F20);
      PcdSetBoolS (PcdPcieSsd2PwrEnableGpioPolarity, PIN_GPIO_ACTIVE_HIGH);
      //
      // Configure CPU M.2 SSD 2 GPIO PCDs
      //
      PcdSet32S (PcdPcieSsd3PwrEnableGpioNo, GPIO_VER2_LP_GPP_C2);
      PcdSet32S (PcdPcieSsd3RstGpioNo, GPIO_VER2_LP_GPP_H1);
      PcdSetBoolS (PcdPcieSsd3PwrEnableGpioPolarity, PIN_GPIO_ACTIVE_HIGH);
      //
      // Configure PEG X8 DG/DG2 GPIO PCDs
      //
      PcdSet32S (PcdPcieDG2PwrEnableGpioNo, GPIO_VER2_LP_GPP_A8);
      PcdSet32S (PcdPcieDG2RstGpioNo, GPIO_VER2_LP_GPP_B2);
      PcdSetBoolS (PcdPcieDG2PwrEnableGpioPolarity, PIN_GPIO_ACTIVE_LOW);
      PcdSet32S (PcdPegSlot1WakeGpioPin, GPIO_VER2_LP_GPP_A17);
      break;
    case BoardIdAdlPLp4Bep:
      //
      // Configure CPU M.2 SSD GPIO PCDs
      //
      PcdSet8S (PcdRootPortIndex, 1);
      PcdSet32S (PcdPcieSsd2PwrEnableGpioNo, GPIO_VER2_LP_GPP_D16);
      PcdSet32S (PcdPcieSsd2RstGpioNo, GPIO_VER2_LP_GPP_F20);
      PcdSetBoolS (PcdPcieSsd2PwrEnableGpioPolarity, PIN_GPIO_ACTIVE_HIGH);
      PcdSetBoolS (PcdPcieSsd2RstGpioPolarity, PIN_GPIO_ACTIVE_LOW);
      break;
    case BoardIdAdlPDdr5Rvp:
    case BoardIdAdlPDdr4Rvp:
      PcdSet8S (PcdRootPortIndex, 4);
      PcdSet8S (PcdPcieSlot1GpioSupport, PchGpio);
      PcdSet8S (PcdPcieSlot1HoldRstExpanderNo, 0);
      PcdSet8S (PcdPcieSlot1PwrEnableExpanderNo, 0);
      //
      // Configure CPU M.2 SSD 1 GPIO PCDs
      //
      PcdSet32S (PcdPcieSsd2PwrEnableGpioNo, GPIO_VER2_LP_GPP_D14);
      PcdSet32S (PcdPcieSsd2RstGpioNo, GPIO_VER2_LP_GPP_F20);
      PcdSetBoolS (PcdPcieSsd2PwrEnableGpioPolarity, PIN_GPIO_ACTIVE_HIGH);
      //
      // Configure CPU M.2 SSD 2 GPIO PCDs
      //
      PcdSet32S (PcdPcieSsd3PwrEnableGpioNo, GPIO_VER2_LP_GPP_C2);
      PcdSet32S (PcdPcieSsd3RstGpioNo, GPIO_VER2_LP_GPP_H1);
      PcdSetBoolS (PcdPcieSsd3PwrEnableGpioPolarity, PIN_GPIO_ACTIVE_HIGH);
      //
      // Configure PEG X8 DG/DG2 GPIO PCDs
      //
//[-start-210804-QINGLIN0008-modify]//
#if defined(S370_SUPPORT)
      PcdSet32S(PcdPcieDG2PwrEnableGpioNo, GPIO_VER2_LP_GPP_E10);
      PcdSet32S(PcdPcieDG2RstGpioNo, GPIO_VER2_LP_GPP_A15);
      PcdSetBoolS(PcdPcieDG2PwrEnableGpioPolarity, PIN_GPIO_ACTIVE_HIGH);
      PcdSet32S (PcdPegSlot1WakeGpioPin, GPIO_VER2_LP_GPP_A20);
//[-start-210917-QINGLIN0068-add]//
#elif defined(S570_SUPPORT)
      PcdSet32S(PcdPcieDG2PwrEnableGpioNo, GPIO_VER2_LP_GPP_B7);
      PcdSet32S(PcdPcieDG2RstGpioNo, GPIO_VER2_LP_GPP_D0);
      PcdSetBoolS(PcdPcieDG2PwrEnableGpioPolarity, PIN_GPIO_ACTIVE_HIGH);
//[-end-210917-QINGLIN0068-add]//
#else
      PcdSet32S (PcdPcieDG2PwrEnableGpioNo, GPIO_VER2_LP_GPP_A14);
      PcdSet32S (PcdPcieDG2RstGpioNo, GPIO_VER2_LP_GPP_B2);
      PcdSetBoolS (PcdPcieDG2PwrEnableGpioPolarity, PIN_GPIO_ACTIVE_LOW);
      PcdSet32S (PcdPegSlot1WakeGpioPin, GPIO_VER2_LP_GPP_A20);
#endif
      break;
    case BoardIdAdlPDdr5Dg384Aep:
      //
      // Configure CPU M.2 SSD 2 GPIO PCDs
      //
      PcdSet32S (PcdPcieSsd3PwrEnableGpioNo, GPIO_VER2_LP_GPP_C2);
      PcdSet32S (PcdPcieSsd3RstGpioNo, GPIO_VER2_LP_GPP_F21);
      PcdSetBoolS (PcdPcieSsd3PwrEnableGpioPolarity, PIN_GPIO_ACTIVE_HIGH);
      PcdSetBoolS (PcdPcieSsd3RstGpioPolarity, PIN_GPIO_ACTIVE_LOW);
      //
      // Configure PEG X8 DG/DG2 GPIO PCDs
      //
      PcdSet32S (PcdPcieDG2PwrEnableGpioNo, GPIO_VER2_LP_GPP_A14);
      PcdSet32S (PcdPcieDG2RstGpioNo, GPIO_VER2_LP_GPP_A15);
      PcdSetBoolS (PcdPcieDG2PwrEnableGpioPolarity, PIN_GPIO_ACTIVE_HIGH);
      PcdSetBoolS (PcdPcieDG2RstGpioPolarity, PIN_GPIO_ACTIVE_LOW);
      PcdSet32S (PcdPegSlot1WakeGpioPin, GPIO_VER2_LP_GPP_A20);
      break;
    case BoardIdAdlPLp5Aep:
    case BoardIdAdlPLp5Gcs:
      //
      // Configure CPU M.2 SSD 1 GPIO PCDs
      //
      PcdSet32S(PcdPcieSsd2PwrEnableGpioNo, GPIO_VER2_LP_GPP_D16);
      PcdSet32S(PcdPcieSsd2RstGpioNo, GPIO_VER2_LP_GPP_F20);
      PcdSetBoolS(PcdPcieSsd2PwrEnableGpioPolarity, PIN_GPIO_ACTIVE_HIGH);
      PcdSetBoolS(PcdPcieSsd2RstGpioPolarity, PIN_GPIO_ACTIVE_LOW);
      break;
    case BoardIdAdlPLp5Dg128Aep:
      //
      // Configure CPU M.2 SSD 2 GPIO PCDs
      //
      PcdSet32S (PcdPcieSsd3PwrEnableGpioNo, GPIO_VER2_LP_GPP_D16);
      PcdSet32S (PcdPcieSsd3RstGpioNo, GPIO_VER2_LP_GPP_F21);
      PcdSetBoolS (PcdPcieSsd3PwrEnableGpioPolarity, PIN_GPIO_ACTIVE_HIGH);
      PcdSetBoolS (PcdPcieSsd3RstGpioPolarity, PIN_GPIO_ACTIVE_LOW);
      //
      // Configure PEG X8 DG/DG2 GPIO PCDs
      //
      PcdSet32S (PcdPcieDG2PwrEnableGpioNo, GPIO_VER2_LP_GPP_A14);
      PcdSet32S (PcdPcieDG2RstGpioNo, GPIO_VER2_LP_GPP_A15);
      PcdSetBoolS (PcdPcieDG2PwrEnableGpioPolarity, PIN_GPIO_ACTIVE_HIGH);
      PcdSetBoolS (PcdPcieDG2RstGpioPolarity, PIN_GPIO_ACTIVE_LOW);
      PcdSet32S (PcdPegSlot1WakeGpioPin, GPIO_VER2_LP_GPP_A20);
      break;
    case BoardIdAdlPMMAep:
      //
      // Configure CPU M.2 SSD 2 GPIO PCDs
      //
      PcdSet32S (PcdPcieSsd3PwrEnableGpioNo, GPIO_VER2_LP_GPP_F13);
      PcdSet32S (PcdPcieSsd3RstGpioNo, GPIO_VER2_LP_GPP_F21);
      PcdSetBoolS (PcdPcieSsd3PwrEnableGpioPolarity, PIN_GPIO_ACTIVE_HIGH);
      PcdSetBoolS (PcdPcieSsd3RstGpioPolarity, PIN_GPIO_ACTIVE_LOW);
      //
      // Configure PEG X8 DG/DG2 GPIO PCDs
      //
      PcdSet32S (PcdPcieDG2PwrEnableGpioNo, GPIO_VER2_LP_GPP_A14);
      PcdSet32S (PcdPcieDG2RstGpioNo, GPIO_VER2_LP_GPP_A15);
      PcdSetBoolS (PcdPcieDG2PwrEnableGpioPolarity, PIN_GPIO_ACTIVE_HIGH);
      PcdSetBoolS (PcdPcieDG2RstGpioPolarity, PIN_GPIO_ACTIVE_LOW);
      break;
    case BoardIdAdlPLp5MbAep:
      // Configure CPU M.2 SSD 1 GPIO PCDs
      //
      PcdSet32S(PcdPcieSsd2PwrEnableGpioNo, GPIO_VER2_LP_GPP_D16);
      PcdSet32S(PcdPcieSsd2RstGpioNo, GPIO_VER2_LP_GPP_F20);
      PcdSetBoolS(PcdPcieSsd2PwrEnableGpioPolarity, PIN_GPIO_ACTIVE_HIGH);
      PcdSetBoolS(PcdPcieSsd2RstGpioPolarity, PIN_GPIO_ACTIVE_LOW);       // M.2 CPU SSD 1
      //
      // Configure CPU M.2 SSD 2 GPIO PCDs
      //
      PcdSet32S(PcdPcieSsd3PwrEnableGpioNo, GPIO_VER2_LP_GPP_H13);
      PcdSet32S(PcdPcieSsd3RstGpioNo, GPIO_VER2_LP_GPP_F21);
      PcdSetBoolS(PcdPcieSsd3PwrEnableGpioPolarity, PIN_GPIO_ACTIVE_HIGH);
      PcdSetBoolS(PcdPcieSsd3RstGpioPolarity, PIN_GPIO_ACTIVE_LOW);       // M.2 CPU SSD 2
      break;
  }
  //
  // WA added to route peg PEG X8 DG/DG2 wake pin to pch pcie slot on rework enabled.
  //
  switch (BoardId) {
    case BoardIdAdlPDdr5Rvp:
    case BoardIdAdlPDdr4Rvp:
    case BoardIdAdlPLp5Rvp:
    case BoardIdAdlPT3Lp5Rvp:
    case BoardIdAdlPLp4Rvp:
      if (!Setup.Rp08WakeReworkDone) {
        PcdSet32S (PcdPegSlot1WakeGpioPin, GPIO_VER2_LP_GPP_A20);
      }
    break;
  }
  //
  // PEG PCIE RTD3 GPIO
  //
  switch (BoardId) {
    case BoardIdAdlPLp4Bep:
    case BoardIdAdlPLp5Aep:
    case BoardIdAdlPLp5Gcs:
      PcdSet8S (PcdPcie0GpioSupport, NotSupported);
      PcdSet8S (PcdPcie0HoldRstExpanderNo, 0);
      PcdSet32S (PcdPcie0HoldRstGpioNo, FALSE);
      PcdSetBoolS (PcdPcie0HoldRstActive, FALSE);
      PcdSet8S (PcdPcie0PwrEnableExpanderNo, 0);
      PcdSet32S (PcdPcie0PwrEnableGpioNo, FALSE);
      PcdSetBoolS (PcdPcie0PwrEnableActive, FALSE);

      PcdSet8S (PcdPcie1GpioSupport, NotSupported);
      PcdSet8S (PcdPcie1HoldRstExpanderNo, 0);
      PcdSet32S (PcdPcie1HoldRstGpioNo, 0);
      PcdSetBoolS (PcdPcie1HoldRstActive, FALSE);
      PcdSet8S (PcdPcie1PwrEnableExpanderNo, 0);
      PcdSet32S (PcdPcie1PwrEnableGpioNo, 0);
      PcdSetBoolS (PcdPcie1PwrEnableActive, FALSE);

      PcdSet8S (PcdPcie2GpioSupport, NotSupported);
      PcdSet8S (PcdPcie2HoldRstExpanderNo, 0);
      PcdSet32S (PcdPcie2HoldRstGpioNo, 0);
      PcdSetBoolS (PcdPcie2HoldRstActive, FALSE);
      PcdSet8S (PcdPcie2PwrEnableExpanderNo, 0);
      PcdSet32S (PcdPcie2PwrEnableGpioNo, 0);
      PcdSetBoolS (PcdPcie2PwrEnableActive, FALSE);

      PcdSet8S (PcdPcie3GpioSupport, NotSupported);
      PcdSet8S (PcdPcie3HoldRstExpanderNo, 0);
      PcdSet32S (PcdPcie3HoldRstGpioNo, 0);
      PcdSetBoolS (PcdPcie3HoldRstActive, FALSE);
      PcdSet8S (PcdPcie3PwrEnableExpanderNo, 0);
      PcdSet32S (PcdPcie3PwrEnableGpioNo, 0);
      PcdSetBoolS (PcdPcie3PwrEnableActive, FALSE);
      break;
    case BoardIdAdlPMMAep:
      // CPU SSD 1
      PcdSet8S (PcdPcie0GpioSupport, NotSupported);
      PcdSet8S (PcdPcie0HoldRstExpanderNo, 0);
      PcdSet32S (PcdPcie0HoldRstGpioNo, 0);
      PcdSetBoolS (PcdPcie0HoldRstActive, FALSE);
      PcdSet8S (PcdPcie0PwrEnableExpanderNo, 0);
      PcdSet32S (PcdPcie0PwrEnableGpioNo, 0);
      PcdSetBoolS (PcdPcie0PwrEnableActive, FALSE);

      // x8 DG2
      PcdSet8S (PcdPcie1GpioSupport, PchGpio);
      PcdSet8S (PcdPcie1HoldRstExpanderNo, 0);
      PcdSet32S (PcdPcie1HoldRstGpioNo, GPIO_VER2_LP_GPP_A15);
      PcdSetBoolS (PcdPcie1HoldRstActive, FALSE);
      PcdSet8S (PcdPcie1PwrEnableExpanderNo, 0);
      PcdSet32S (PcdPcie1PwrEnableGpioNo, GPIO_VER2_LP_GPP_A14);
      PcdSetBoolS (PcdPcie1PwrEnableActive, FALSE);

      // CPU SSD 2
      PcdSet8S (PcdPcie2GpioSupport, NotSupported);
      PcdSet8S (PcdPcie2HoldRstExpanderNo, 0);
      PcdSet32S (PcdPcie2HoldRstGpioNo, 0);
      PcdSetBoolS (PcdPcie2HoldRstActive, FALSE);
      PcdSet8S (PcdPcie2PwrEnableExpanderNo, 0);
      PcdSet32S (PcdPcie2PwrEnableGpioNo, 0);
      PcdSetBoolS (PcdPcie2PwrEnableActive, FALSE);
      break;
    case BoardIdAdlPDdr5Dg384Aep:
    case BoardIdAdlPLp5Dg128Aep:
    case BoardIdAdlPLp5MbAep:
      break;
    default:
      PcdSet8S (PcdPcie0GpioSupport, PchGpio);
      PcdSet8S (PcdPcie0HoldRstExpanderNo, 0);
      PcdSet32S (PcdPcie0HoldRstGpioNo, GPIO_VER2_LP_GPP_F10);
      PcdSetBoolS (PcdPcie0HoldRstActive, FALSE);
      PcdSet8S (PcdPcie0PwrEnableExpanderNo, 0);
      PcdSet32S (PcdPcie0PwrEnableGpioNo, GPIO_VER2_LP_GPP_H17);
      PcdSetBoolS (PcdPcie0PwrEnableActive, FALSE);

//[-start-210922-GEORGE0006-add]//
#if defined(S77014_SUPPORT) || defined(S77014IAH_SUPPORT)
      PcdSet32S (PcdPcie0HoldRstGpioNo, GPIO_VER2_LP_GPP_H13);
      PcdSetBoolS (PcdPcie0HoldRstActive, FALSE);
      PcdSet32S (PcdPcie0PwrEnableGpioNo, GPIO_VER2_LP_GPP_E11);
      PcdSetBoolS (PcdPcie0PwrEnableActive, TRUE);
#endif
//[-end-210922-GEORGE0006-add]//

      PcdSet8S (PcdPcie1GpioSupport, NotSupported);
      PcdSet8S (PcdPcie1HoldRstExpanderNo, 0);
      PcdSet32S (PcdPcie1HoldRstGpioNo, 0);
      PcdSetBoolS (PcdPcie1HoldRstActive, FALSE);
      PcdSet8S (PcdPcie1PwrEnableExpanderNo, 0);
      PcdSet32S (PcdPcie1PwrEnableGpioNo, 0);
      PcdSetBoolS (PcdPcie1PwrEnableActive, FALSE);

      PcdSet8S (PcdPcie2GpioSupport, NotSupported);
      PcdSet8S (PcdPcie2HoldRstExpanderNo, 0);
      PcdSet32S (PcdPcie2HoldRstGpioNo, 0);
      PcdSetBoolS (PcdPcie2HoldRstActive, FALSE);
      PcdSet8S (PcdPcie2PwrEnableExpanderNo, 0);
      PcdSet32S (PcdPcie2PwrEnableGpioNo, 0);
      PcdSetBoolS (PcdPcie2PwrEnableActive, FALSE);

      PcdSet8S (PcdPcie3GpioSupport, NotSupported);
      PcdSet8S (PcdPcie3HoldRstExpanderNo, 0);
      PcdSet32S (PcdPcie3HoldRstGpioNo, 0);
      PcdSetBoolS (PcdPcie3HoldRstActive, FALSE);
      PcdSet8S (PcdPcie3PwrEnableExpanderNo, 0);
      PcdSet32S (PcdPcie3PwrEnableGpioNo, 0);
      PcdSetBoolS (PcdPcie3PwrEnableActive, FALSE);
      break;
  }
  return;
}

/**
  SA Display DDI configuration init function for PEI pre-memory phase.

  @param[in]  VOID

  @retval     VOID
**/
VOID
AdlPSaDisplayConfigInit (
  VOID
  )
{
  UINT16    BoardId;
  UINT16    DisplayId;

  BoardId   = PcdGet16 (PcdBoardId);
  DisplayId = PcdGet16 (PcdDisplayId);

  DEBUG ((DEBUG_INFO, "BoardId = 0x%x DisplayId = 0x%x\n",BoardId,DisplayId));

  switch (BoardId) {
    case BoardIdAdlPLp4Rvp:
      if (DisplayId & DisplayIdAdlpmEdpExtDisp) {
        DEBUG ((DEBUG_INFO, "DDI Configuration ADLP LP4 Edp Hdmi\n"));
        PcdSet32S (PcdSaDisplayConfigTable, (UINTN) mAdlPEdpHdmiDisplayDdiConfig);
        PcdSet16S (PcdSaDisplayConfigTableSize, sizeof (mAdlPEdpHdmiDisplayDdiConfig));
      } else {
        DEBUG ((DEBUG_INFO, "DDI Configuration ADLP LP4 Dual Edp\n"));
        PcdSet32S (PcdSaDisplayConfigTable, (UINTN) mAdlPDualEdpDisplayDdiConfig);
        PcdSet16S (PcdSaDisplayConfigTableSize, sizeof (mAdlPDualEdpDisplayDdiConfig));
      }
      break;
    case BoardIdAdlPLp5Rvp:
    case BoardIdAdlPT3Lp5Rvp:
      if (DisplayId == DisplayIdAdlpmMipiEdp) {
        DEBUG ((DEBUG_INFO, "DDI Configuration ADLP LP5 Mipi Edp\n"));
        PcdSet32S (PcdSaDisplayConfigTable, (UINTN) mAdlPMipiEdpDisplayDdiConfig);
        PcdSet16S (PcdSaDisplayConfigTableSize, sizeof (mAdlPMipiEdpDisplayDdiConfig));
      } else if (DisplayId == DisplayIdAdlpmEdpMipi) {
        DEBUG ((DEBUG_INFO, "DDI Configuration ADLP LP5 Edp Mipi\n"));
        PcdSet32S (PcdSaDisplayConfigTable, (UINTN) mAdlPEdpMipiDisplayDdiConfig);
        PcdSet16S (PcdSaDisplayConfigTableSize, sizeof (mAdlPEdpMipiDisplayDdiConfig));
      } else if (DisplayId == DisplayIdAdlpmDualMipi) {
        DEBUG ((DEBUG_INFO, "DDI Configuration ADLP LP5 Dual Mipi\n"));
        PcdSet32S (PcdSaDisplayConfigTable, (UINTN) mAdlPDualMipiDisplayDdiConfig);
        PcdSet16S (PcdSaDisplayConfigTableSize, sizeof (mAdlPDualMipiDisplayDdiConfig));
      } else {
        DEBUG ((DEBUG_INFO, "DDI Configuration ADLP LP5 Dual Edp\n"));
        PcdSet32S (PcdSaDisplayConfigTable, (UINTN) mAdlPDualEdpDisplayDdiConfig);
        PcdSet16S (PcdSaDisplayConfigTableSize, sizeof (mAdlPDualEdpDisplayDdiConfig));
      }
      break;
    case BoardIdAdlPLp4Bep:
      PcdSet32S (PcdSaDisplayConfigTable, (UINTN) mAdlPLp4BepRowDisplayDdiConfig);
      PcdSet16S (PcdSaDisplayConfigTableSize, sizeof (mAdlPLp4BepRowDisplayDdiConfig));
      break;
    case BoardIdAdlPDdr5Dg384Aep:
    case BoardIdAdlPLp5Aep:
    case BoardIdAdlPLp5Gcs:
    case BoardIdAdlPMMAep:
      PcdSet32S (PcdSaDisplayConfigTable, (UINTN)mAdlPDg384AepDisplayDdiConfig);
      PcdSet16S (PcdSaDisplayConfigTableSize, sizeof (mAdlPDg384AepDisplayDdiConfig));
      break;
    case BoardIdAdlPLp5Dg128Aep:
      PcdSet32S (PcdSaDisplayConfigTable, (UINTN) mAdlPDg128AepDisplayDdiConfig);
      PcdSet16S (PcdSaDisplayConfigTableSize, sizeof (mAdlPDg128AepDisplayDdiConfig));
      break;
    case BoardIdAdlPLp5MbAep:
      PcdSet32S (PcdSaDisplayConfigTable, (UINTN) mAdlPMbAepDisplayDdiConfig);
      PcdSet16S (PcdSaDisplayConfigTableSize, sizeof (mAdlPMbAepDisplayDdiConfig));
      break;
    case BoardIdAdlPDdr5Rvp:
        DEBUG ((DEBUG_INFO, "DDI Configuration ADLP Edp DP\n"));
        PcdSet32S (PcdSaDisplayConfigTable, (UINTN) mAdlPDdr5RvpDisplayDdiConfig);
        PcdSet16S (PcdSaDisplayConfigTableSize, sizeof (mAdlPDdr5RvpDisplayDdiConfig));
      break;
    case BoardIdAdlPDdr5MRRvp:
        DEBUG ((DEBUG_INFO, "DDI Configuration ADLP MR Edp DP\n"));
        PcdSet32S (PcdSaDisplayConfigTable, (UINTN) mAdlPDdr5MRRvpDisplayDdiConfig);
        PcdSet16S (PcdSaDisplayConfigTableSize, sizeof (mAdlPDdr5MRRvpDisplayDdiConfig));
      break;
    case BoardIdAdlPDdr4Rvp:
        DEBUG ((DEBUG_INFO, "DDI Configuration ADLP Ddr4 Edp Hdmi\n"));
        PcdSet32S (PcdSaDisplayConfigTable, (UINTN) mAdlPEdpHdmiDisplayDdiConfig);
        PcdSet16S (PcdSaDisplayConfigTableSize, sizeof (mAdlPEdpHdmiDisplayDdiConfig));
      break;
    default:
      break;
  }

  return;
}

/**
  Board USB related configuration init function for PEI pre-memory phase.

  @param[in]  VOID

  @retval VOID
**/
VOID
AdlPSaUsbConfigInit (
  VOID
  )
{
  UINT16    BoardId;
  BoardId = PcdGet16(PcdBoardId);

  switch (BoardId) {
    //
    // Override ALL port setting if required.
    //
    case BoardIdAdlPLp4Bep:
      PcdSet8S (PcdCpuXhciPortSupportMap, 0x03);
      PcdSet8S (PcdITbtRootPortNumber, 4);
      PcdSet8S (PcdCvfUsbPort, 0x07);
      break;
    case BoardIdAdlPDdr5Dg384Aep:
      PcdSet8S (PcdCpuXhciPortSupportMap, 0x0C);
      PcdSet8S (PcdITbtRootPortNumber, 2);
      PcdSet8S (PcdCvfUsbPort, 0x08);
      break;
    case BoardIdAdlPLp5Gcs:
    case BoardIdAdlPMMAep:
      PcdSet8S (PcdCpuXhciPortSupportMap, 0x0C);
      PcdSet8S (PcdITbtRootPortNumber, 2);
      break;
    case BoardIdAdlPDdr5Rvp:
    case BoardIdAdlPLp4Rvp:
    case BoardIdAdlPLp5Rvp:
    case BoardIdAdlPT3Lp5Rvp:
    case BoardIdAdlPDdr4Rvp:
    case BoardIdAdlPLp5Aep:
      PcdSet8S (PcdCpuXhciPortSupportMap, 0x0F);
      PcdSet8S (PcdITbtRootPortNumber, 4);
      PcdSet8S (PcdCvfUsbPort, 0x07);
      break;
    case BoardIdAdlPLp5Dg128Aep:
    case BoardIdAdlPLp5MbAep:
      PcdSet8S (PcdCpuXhciPortSupportMap, 0x0E);
      PcdSet8S (PcdITbtRootPortNumber, 3);
      PcdSet8S (PcdCvfUsbPort, 0x08);
      break;
    case BoardIdAdlPDdr5MRRvp: // ADL-P with two dTBT MR Controller Design. No iTBT PCIE RP and 1 CPU XHCI Port.
      PcdSet8S (PcdCpuXhciPortSupportMap, 0x01);
      PcdSet8S (PcdITbtRootPortNumber, 0);
      PcdSet8S (PcdCvfUsbPort, 0x07);
      break;
  }
  //
  // Update Cpu Xhci Port Enable Map PCD based on SaSetup Data and PcdCpuXhciPortSupportMap.
  //
  TcssUpdateCpuXhciPortEnableMapPcd ();
}
