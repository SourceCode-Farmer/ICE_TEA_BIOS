/** @file
  Alderlake P RVP GPIO definition table for Pre-Memory Initialization

@copyright
  Copyright (c) 2020 - 2021 Intel Corporation. All rights reserved
  This software and associated documentation (if any) is furnished
  under a license and may only be used or copied in accordance
  with the terms of the license. Except as permitted by such
  license, no part of this software or documentation may be
  reproduced, stored in a retrieval system, or transmitted in any
  form or by any means without the express written consent of
  Intel Corporation.
  This file contains an 'Intel Peripheral Driver' and is
  licensed for Intel CPUs and chipsets under the terms of your
  license agreement with Intel or your vendor. This file may
  be modified by the user, subject to additional terms of the
  license agreement.
**/
#ifndef _ALDERLAKE_P_PREMEM_GPIO_TABLE_H_
#define _ALDERLAKE_P_PREMEM_GPIO_TABLE_H_

///

/// !!! For those GPIO pins are designed as native function, BIOS CAN NOT configure the pins to Native function in GPIO init table!!!

/// BIOS has to leave the native pins programming to Silicon Code(based on BIOS policies setting) or soft strap(set by CSME in FITc).

/// Configuring pins to native function in GPIO table would cause BIOS perform multiple programming the pins and then related function might be abnormal.

///

#include <Pins/GpioPinsVer2Lp.h>
#include <Library/GpioLib.h>
#include <Library/GpioConfig.h>

GLOBAL_REMOVE_IF_UNREFERENCED GPIO_INIT_CONFIG mGpioTableAdlPLp4RvpWwanOnEarlyPreMem[] =
{
  // WWAN
  // Please don't change the sequence of Gpio Initialization
  {GPIO_VER2_LP_GPP_F21, {GpioPadModeGpio, GpioHostOwnAcpi,    GpioDirOut,   GpioOutHigh,     GpioIntDis,                GpioResumeReset,   GpioTermNone, GpioOutputStateUnlock }},   // WWAN_PWREN
  {GPIO_VER2_LP_GPP_F15, {GpioPadModeGpio, GpioHostOwnDefault, GpioDirOut,   GpioOutHigh,     GpioIntDis,                GpioResumeReset,   GpioTermNone, GpioOutputStateUnlock }},   // WWAN_FCP_OFF_N
  {GPIO_VER2_LP_GPP_F14, {GpioPadModeGpio, GpioHostOwnAcpi,    GpioDirOut,   GpioOutHigh,     GpioIntDis,                GpioResumeReset,   GpioTermNone, GpioOutputStateUnlock }},   // WWAN_RST_N
  {GPIO_VER2_LP_GPP_C5,  {GpioPadModeGpio, GpioHostOwnAcpi,    GpioDirOut,   GpioOutHigh,     GpioIntDis,                GpioPlatformReset, GpioTermNone, GpioOutputStateUnlock }},   // WWAN_PERST_N
  {GPIO_VER2_LP_GPP_D18, {GpioPadModeGpio, GpioHostOwnAcpi,    GpioDirInInv, GpioOutDefault,  GpioIntLevel|GpioIntSci,   GpioHostDeepReset, GpioTermNone, GpioPadConfigUnlock }},     // WWAN_WAKE_N
  {GPIO_VER2_LP_GPP_D15, {GpioPadModeGpio, GpioHostOwnDefault, GpioDirOut,   GpioOutHigh,     GpioIntDis,                GpioResumeReset,   GpioTermNone, GpioOutputStateUnlock }},   // WWAN_DISABLE_N
  {GPIO_VER2_LP_GPP_A6,  {GpioPadModeGpio, GpioHostOwnAcpi,    GpioDirInInv, GpioOutDefault,  GpioIntLevel|GpioIntSci,   GpioHostDeepReset, GpioTermNone, GpioPadConfigUnlock }},     // SAR_DPR_PCH
};

GLOBAL_REMOVE_IF_UNREFERENCED GPIO_INIT_CONFIG mGpioTableAdlPLp4RvpWwanOffEarlyPreMem[] =
{
  {GPIO_VER2_LP_GPP_F14,  {GpioPadModeGpio, GpioHostOwnAcpi,    GpioDirOut,    GpioOutLow,    GpioIntDis,        GpioResumeReset,    GpioTermNone,  GpioOutputStateUnlock }},        // WWAN_RST_N
  {GPIO_VER2_LP_GPP_C5,   {GpioPadModeGpio, GpioHostOwnAcpi,    GpioDirOut,    GpioOutLow,    GpioIntDis,        GpioPlatformReset,  GpioTermNone,  GpioOutputStateUnlock }},        // WWAN_PERST_N
  {GPIO_VER2_LP_GPP_F15,  {GpioPadModeGpio, GpioHostOwnDefault, GpioDirOut,    GpioOutLow,    GpioIntDis,        GpioResumeReset,    GpioTermNone,  GpioOutputStateUnlock }},        // WWAN_FCP_OFF_N
};

//
// AlderLake P BEP+ Lp4x
//
GLOBAL_REMOVE_IF_UNREFERENCED GPIO_INIT_CONFIG mGpioTableAdlPLp4BepWwanOnEarlyPreMem[] =
{
  // WWAN
  { GPIO_VER2_LP_GPP_F15, { GpioPadModeGpio, GpioHostOwnAcpi,    GpioDirOut,   GpioOutHigh,    GpioIntDis,                GpioResumeReset,   GpioTermNone, GpioOutputStateUnlock } },  // WWAN_PWREN
  { GPIO_VER2_LP_GPP_F17, { GpioPadModeGpio, GpioHostOwnDefault, GpioDirOut,   GpioOutHigh,    GpioIntDis,                GpioResumeReset,   GpioTermNone, GpioOutputStateUnlock } },  // WWAN_FCP_OFF_N
  { GPIO_VER2_LP_GPP_F18, { GpioPadModeGpio, GpioHostOwnAcpi,    GpioDirOut,   GpioOutHigh,    GpioIntDis,                GpioResumeReset,   GpioTermNone, GpioOutputStateUnlock } },  // WWAN_RST_N
  { GPIO_VER2_LP_GPP_C5,  { GpioPadModeGpio, GpioHostOwnAcpi,    GpioDirOut,   GpioOutHigh,    GpioIntDis,                GpioPlatformReset, GpioTermNone, GpioOutputStateUnlock } },  // WWAN_PERST_N
  { GPIO_VER2_LP_GPP_D18, { GpioPadModeGpio, GpioHostOwnAcpi,    GpioDirInInv, GpioOutDefault, GpioIntLevel | GpioIntSci, GpioResumeReset,   GpioTermNone, GpioPadConfigUnlock   } },  // WWAN_WAKE_N
  { GPIO_VER2_LP_GPP_D15, { GpioPadModeGpio, GpioHostOwnDefault, GpioDirOut,   GpioOutHigh,    GpioIntDis,                GpioResumeReset,   GpioTermNone, GpioOutputStateUnlock } },  // WWAN_DISABLE_N
  { GPIO_VER2_LP_GPP_A11, { GpioPadModeGpio, GpioHostOwnAcpi,    GpioDirOut,   GpioOutHigh,    GpioIntDis,                GpioResumeReset,   GpioTermNone, GpioOutputStateUnlock } },  // WWAN_RESET_OUT
  { GPIO_VER2_LP_GPP_A6,  { GpioPadModeGpio, GpioHostOwnAcpi,    GpioDirInInv, GpioOutDefault, GpioIntLevel | GpioIntSci, GpioHostDeepReset, GpioTermNone, GpioPadConfigUnlock   } },  // SAR_DPR_PCH
  { GPIO_VER2_LP_GPP_A21, { GpioPadModeGpio, GpioHostOwnAcpi,    GpioDirOut,   GpioOutHigh,    GpioIntDis,                GpioResumeReset,   GpioTermNone, GpioOutputStateUnlock } },  // GNSS_DISABLE_N
};

GLOBAL_REMOVE_IF_UNREFERENCED GPIO_INIT_CONFIG mGpioTableAdlPLp4BepWwanOffEarlyPreMem[] =
{
  { GPIO_VER2_LP_GPP_F18, { GpioPadModeGpio, GpioHostOwnAcpi,    GpioDirOut, GpioOutLow,  GpioIntDis, GpioResumeReset,   GpioTermNone, GpioOutputStateUnlock } },                      // WWAN_RST_N
  { GPIO_VER2_LP_GPP_C5,  { GpioPadModeGpio, GpioHostOwnAcpi,    GpioDirOut, GpioOutLow,  GpioIntDis, GpioPlatformReset, GpioTermNone, GpioOutputStateUnlock } },                      // WWAN_PERST_N
  { GPIO_VER2_LP_GPP_F17, { GpioPadModeGpio, GpioHostOwnDefault, GpioDirOut, GpioOutLow,  GpioIntDis, GpioResumeReset,   GpioTermNone, GpioOutputStateUnlock } },                      // WWAN_FCP_OFF_N
};

GLOBAL_REMOVE_IF_UNREFERENCED GPIO_INIT_CONFIG mGpioTableAdlPMMAepWwanOnEarlyPreMem[] =
{
  // WWAN
  // Please don't change the sequence of Gpio Initialization
  {GPIO_VER2_LP_GPP_F15, {GpioPadModeGpio, GpioHostOwnAcpi,     GpioDirOut,   GpioOutHigh,     GpioIntDis,                GpioResumeReset,   GpioTermNone, GpioOutputStateUnlock } },  // WWAN_PWREN
  {GPIO_VER2_LP_GPP_F17, {GpioPadModeGpio, GpioHostOwnDefault,  GpioDirOut,   GpioOutHigh,     GpioIntDis,                GpioResumeReset,   GpioTermNone, GpioOutputStateUnlock } },  // WWAN_FCP_OFF_N
  {GPIO_VER2_LP_GPP_F14, {GpioPadModeGpio, GpioHostOwnAcpi,     GpioDirOut,   GpioOutHigh,     GpioIntDis,                GpioResumeReset,   GpioTermNone, GpioOutputStateUnlock } },  // WWAN_RST_N
  {GPIO_VER2_LP_GPP_C5,  {GpioPadModeGpio, GpioHostOwnAcpi,     GpioDirOut,   GpioOutHigh,     GpioIntDis,                GpioPlatformReset, GpioTermNone, GpioOutputStateUnlock } },  // WWAN_PERST_N
  {GPIO_VER2_LP_GPP_D18, {GpioPadModeGpio, GpioHostOwnAcpi,     GpioDirInInv, GpioOutDefault,  GpioIntLevel|GpioIntSci,   GpioResumeReset,   GpioTermNone, GpioPadConfigUnlock   } },  // WWAN_WAKE_N
  {GPIO_VER2_LP_GPP_F16, {GpioPadModeGpio, GpioHostOwnDefault,  GpioDirOut,   GpioOutHigh,     GpioIntDis,                GpioResumeReset,   GpioTermNone, GpioOutputStateUnlock } },  // WWAN_DISABLE_N
  {GPIO_VER2_LP_GPP_A6,  {GpioPadModeGpio, GpioHostOwnAcpi,     GpioDirIn,    GpioOutDefault,  GpioIntLevel|GpioIntSci,   GpioHostDeepReset, GpioTermNone, GpioPadConfigUnlock   } },  // SAR_DPR_PCH
  {GPIO_VER2_LP_GPP_A21, {GpioPadModeGpio, GpioHostOwnAcpi,     GpioDirOut,   GpioOutHigh,     GpioIntDis,                GpioPlatformReset, GpioTermNone, GpioOutputStateUnlock } },  // GNSS_DISABLE_N
};

GLOBAL_REMOVE_IF_UNREFERENCED GPIO_INIT_CONFIG mGpioTableAdlPMMAepWwanOffEarlyPreMem[] =
{
  { GPIO_VER2_LP_GPP_F14, { GpioPadModeGpio, GpioHostOwnAcpi,    GpioDirOut, GpioOutLow,  GpioIntDis, GpioResumeReset,   GpioTermNone, GpioOutputStateUnlock } },                      // WWAN_RST_N
  { GPIO_VER2_LP_GPP_C5,  { GpioPadModeGpio, GpioHostOwnAcpi,    GpioDirOut, GpioOutLow,  GpioIntDis, GpioPlatformReset, GpioTermNone, GpioOutputStateUnlock } },                      // WWAN_PERST_N
  { GPIO_VER2_LP_GPP_F17, { GpioPadModeGpio, GpioHostOwnDefault, GpioDirOut, GpioOutLow,  GpioIntDis, GpioResumeReset,   GpioTermNone, GpioOutputStateUnlock } },                      // WWAN_FCP_OFF_N
};

GLOBAL_REMOVE_IF_UNREFERENCED GPIO_INIT_CONFIG mGpioTableAdlPLp5AepWwanOnEarlyPreMem[] =
{
  // WWAN
  // Please don't change the sequence of Gpio Initialization
  {GPIO_VER2_LP_GPP_F15, {GpioPadModeGpio, GpioHostOwnAcpi,     GpioDirOut,   GpioOutHigh,     GpioIntDis,                GpioResumeReset,   GpioTermNone, GpioOutputStateUnlock } },  // WWAN_PWREN
  {GPIO_VER2_LP_GPP_F17, {GpioPadModeGpio, GpioHostOwnDefault,  GpioDirOut,   GpioOutHigh,     GpioIntDis,                GpioResumeReset,   GpioTermNone, GpioOutputStateUnlock } },  // WWAN_FCP_OFF_N
  {GPIO_VER2_LP_GPP_F18, {GpioPadModeGpio, GpioHostOwnAcpi,     GpioDirOut,   GpioOutHigh,     GpioIntDis,                GpioResumeReset,   GpioTermNone, GpioOutputStateUnlock } },  // WWAN_RST_N
  {GPIO_VER2_LP_GPP_C5,  {GpioPadModeGpio, GpioHostOwnAcpi,     GpioDirOut,   GpioOutHigh,     GpioIntDis,                GpioPlatformReset, GpioTermNone, GpioOutputStateUnlock } },  // WWAN_PERST_N
  {GPIO_VER2_LP_GPP_D18, {GpioPadModeGpio, GpioHostOwnAcpi,     GpioDirInInv, GpioOutDefault,  GpioIntLevel|GpioIntSci,   GpioResumeReset,   GpioTermNone, GpioPadConfigUnlock   } },  // WWAN_WAKE_N
  {GPIO_VER2_LP_GPP_D15, {GpioPadModeGpio, GpioHostOwnDefault,  GpioDirOut,   GpioOutHigh,     GpioIntDis,                GpioResumeReset,   GpioTermNone, GpioOutputStateUnlock } },  // WWAN_DISABLE_N
  {GPIO_VER2_LP_GPP_A6,  {GpioPadModeGpio, GpioHostOwnAcpi,     GpioDirIn,    GpioOutDefault,  GpioIntLevel|GpioIntSci,   GpioHostDeepReset, GpioTermNone, GpioPadConfigUnlock   } },  // SAR_DPR_PCH
  {GPIO_VER2_LP_GPP_A21, {GpioPadModeGpio, GpioHostOwnAcpi,     GpioDirOut,   GpioOutHigh,     GpioIntDis,                GpioPlatformReset, GpioTermNone, GpioOutputStateUnlock } },  // GNSS_DISABLE_N
};

GLOBAL_REMOVE_IF_UNREFERENCED GPIO_INIT_CONFIG mGpioTableAdlPM80WwanEarlyPreMem[] =
{
  {GPIO_VER2_LP_GPP_F21,  { GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,    GpioOutHigh,     GpioIntDis,               GpioResumeReset,    GpioTermNone,  GpioOutputStateUnlock}},    // WWAN_PWREN
  {GPIO_VER2_LP_GPP_F15,  { GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,    GpioOutHigh,     GpioIntDis,               GpioResumeReset,    GpioTermNone,  GpioOutputStateUnlock}},    // WWAN_FCP_OFF_N
  {GPIO_VER2_LP_GPP_F14,  { GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,    GpioOutLow,      GpioIntDis,               GpioResumeReset,    GpioTermNone,  GpioOutputStateUnlock}},    // WWAN_RST_N
  {GPIO_VER2_LP_GPP_C5,   { GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,    GpioOutLow,      GpioIntDis,               GpioResumeReset,    GpioTermNone,  GpioOutputStateUnlock}},    // WWAN_PERST_N
  {GPIO_VER2_LP_GPP_D18,  { GpioPadModeGpio, GpioHostOwnGpio, GpioDirInInv,  GpioOutDefault,  GpioIntLevel|GpioIntSci,  GpioHostDeepReset,  GpioTermNone,  GpioPadConfigUnlock}},      // WWAN_WAKE_N
  {GPIO_VER2_LP_GPP_D15,  { GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,    GpioOutHigh,     GpioIntDis,               GpioResumeReset,    GpioTermNone,  GpioOutputStateUnlock}},    // WWAN_DISABLE_N
  {GPIO_VER2_LP_GPP_A6,   { GpioPadModeGpio, GpioHostOwnAcpi, GpioDirIn,     GpioOutDefault,  GpioIntLevel|GpioIntSci,  GpioHostDeepReset,  GpioTermNone,  GpioPadConfigUnlock }},     // SAR_DPR_PCH
};

GLOBAL_REMOVE_IF_UNREFERENCED GPIO_INIT_CONFIG mGpioTableAdlPOebClkEarlyPreMem[] =
{
  //
  //
  { GPIO_VER2_LP_GPP_E5,{ GpioPadModeGpio,  GpioHostOwnGpio,  GpioDirOut,  GpioOutLow,  GpioIntDefault,  GpioPlatformReset,  GpioTermNone } }, // SRCCLK_OEB6
  { GPIO_VER2_LP_GPP_A7,{ GpioPadModeGpio,  GpioHostOwnGpio,  GpioDirOut,  GpioOutLow,  GpioIntDefault,  GpioPlatformReset,  GpioTermNone } }, // SRCCLK_OEB7
};

GLOBAL_REMOVE_IF_UNREFERENCED GPIO_INIT_CONFIG mGpioTableAdlPDdr5MREarlyPreMem[] =
{
  //
  // dTBT MR Controller, Once Slot Power enable then 1st Clock Must be enable, 2nd CIO RTD3 Power Enable (EarlyPreMem
  // GPIO Configuration) and then 3rd SLOT_RESET GPIO must be enable (Pre-Mem GPIO Configuration). There must be
  // minimum of 10ms delay between CIO RTD3 Power Enable and Slot Reset Enable.
  //
  {GPIO_VER2_LP_GPP_A12,    {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutHigh,     GpioIntDefault,          GpioHostDeepReset,     GpioTermNone}},//MR1 RTD3 enable
  {GPIO_VER2_LP_GPP_E4,     {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutHigh,     GpioIntDefault,          GpioHostDeepReset,     GpioTermNone}},//MR2 RTD3 enable
  {0x0}  // terminator
};

#endif // _ALDERLAKE_P_PREMEM_GPIO_TABLE_H_
