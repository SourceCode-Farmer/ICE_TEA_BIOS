## @file
#  FDF file of Platform.
#
#******************************************************************************
#* Copyright (c) 2020 - 2021, Insyde Software Corp. All Rights Reserved.
#*
#* You may not reproduce, distribute, publish, display, perform, modify, adapt,
#* transmit, broadcast, present, recite, release, license or otherwise exploit
#* any part of this publication in any form, by any means, without the prior
#* written permission of Insyde Software Corporation.
#*
#******************************************************************************
# @copyright
#  INTEL CONFIDENTIAL
#  Copyright 2018 - 2019 Intel Corporation.
#
#  The source code contained or described herein and all documents related to the
#  source code ("Material") are owned by Intel Corporation or its suppliers or
#  licensors. Title to the Material remains with Intel Corporation or its suppliers
#  and licensors. The Material may contain trade secrets and proprietary and
#  confidential information of Intel Corporation and its suppliers and licensors,
#  and is protected by worldwide copyright and trade secret laws and treaty
#  provisions. No part of the Material may be used, copied, reproduced, modified,
#  published, uploaded, posted, transmitted, distributed, or disclosed in any way
#  without Intel's prior express written permission.
#
#  No license under any patent, copyright, trade secret or other intellectual
#  property right is granted to or conferred upon you by disclosure or delivery
#  of the Materials, either expressly, by implication, inducement, estoppel or
#  otherwise. Any license under such intellectual property rights must be
#  express and approved by Intel in writing.
#
#  Unless otherwise agreed by Intel in writing, you may not remove or alter
#  this notice or any other notice embedded in Materials by Intel or
#  Intel's suppliers or licensors in any way.
#
#  This file contains a 'Sample Driver' and is licensed as such under the terms
#  of your license agreement with Intel or your vendor. This file may be modified
#  by the user, subject to the additional terms of the license agreement.
#
# @par Specification
##

[FV.DXEFV]

!if gBoardModuleTokenSpaceGuid.PcdNvmeEnable == TRUE
INF  $(PLATFORM_FEATURES_PATH)/Nvme/PowerLossNotifyDxe/PowerLossNotifyDxe.inf
!endif

# XmlCli: Include Feature based on Pcd Value
#[-start-211108-IB19370023-modify]#
!if gBoardModuleTokenSpaceGuid.PcdIntelXmlCliFeatureEnable == TRUE
#[-end-211108-IB19370023-modify]#
  !include XmlCliFeaturePkg/Include/PostMemory.fdf
!endif

#[-start-201223-IB19010009-add]#
#
# MEBx
#
!if gSiPkgTokenSpaceGuid.PcdAmtEnable == TRUE

!if gMebxFeaturePkgTokenSpaceGuid.PcdMebxFeatureEnable == TRUE
  !include MebxFeaturePkg/Include/MebxFeaturePostMem.fdf
!endif #PcdMebxFeatureEnable

INF $(PLATFORM_FULL_PACKAGE)/Features/Amt/AmtSaveMebxConfigDxe/AmtSaveMebxConfigDxe.inf
INF $(PLATFORM_FULL_PACKAGE)/Features/Amt/AmtPetAlertDxe/AmtPetAlertDxe.inf
INF $(PLATFORM_FULL_PACKAGE)/Features/Amt/AsfTable/AsfTable.inf
INF $(PLATFORM_FEATURES_PATH)/Amt/AmtMacPassThrough/AmtMacPassThrough.inf
INF $(PLATFORM_FULL_PACKAGE)/Features/Amt/AmtWrapperDxe/AmtWrapperDxe.inf

!if gPlatformModuleTokenSpaceGuid.PcdEnableSecureErase == TRUE
INF  $(PLATFORM_FULL_PACKAGE)/Features/Amt/SecureEraseDxe/SecureEraseDxe.inf
!endif
INF $(PLATFORM_SI_PACKAGE)/IpBlock/Amt/AmtInit/Dxe/AmtInitDxe.inf
INF $(PLATFORM_SI_PACKAGE)/IpBlock/Amt/Mebx/Dxe/Mebx.inf
INF $(PLATFORM_SI_PACKAGE)/IpBlock/Amt/Sol/Dxe/SerialOverLan.inf
#[-start-211111-IB09480171-modify]#
!if gPlatformModuleTokenSpaceGuid.PcdOneClickRecoveryEnable == TRUE
INF APRIORI=0xA000 $(PLATFORM_FULL_PACKAGE)/Features/Amt/OneClickRecovery/OneClickRecovery.inf
!endif
#[-end-211111-IB09480171-modify]#
!endif
#[-end-201223-IB19010009-add]#
!if gPlatformModuleTokenSpaceGuid.PcdRemotePlatformEraseSupport == TRUE
INF  $(PLATFORM_FULL_PACKAGE)/Features/Rpe/RemotePlatformErase.inf
!endif
!if gBoardModuleTokenSpaceGuid.PcdAdvancedFeatureEnable == TRUE
INF $(PLATFORM_BOARD_PACKAGE)/Acpi/AdvancedAcpiDxe/AdvancedAcpiDxe.inf
INF RuleOverride = ACPITABLE $(PLATFORMSAMPLE_PACKAGE)/Features/Acpi/AcpiTables/AcpiFeatures.inf
INF RuleOverride = ACPITABLE $(PLATFORM_BOARD_PACKAGE)/Acpi/AcpiTables/AcpiTables.inf
INF RuleOverride = DRIVER_ACPITABLE $(PLATFORM_BOARD_PACKAGE)/Acpi/AdvancedAcpiDxe/AdvancedPmaxDxe.inf
!else
INF MinPlatformPkg/Acpi/AcpiTables/AcpiPlatform.inf
INF RuleOverride = ACPITABLE $(PLATFORM_BOARD_PACKAGE)/Acpi/BoardAcpiDxe/BoardAcpiDxe.inf
!endif

[FV.RECOVERYFV0]
INF $(PLATFORM_FULL_PACKAGE)/Features/Pct/PlatformConfigTool.inf
#[-start-200420-IB17800056-modify]#
#INF $(PLATFORM_BOARD_PACKAGE)/BootMediaInfo/BootMediaInfoPei.inf
INF BoardModulePkg/FirmwareBootMediaInfo/FirmwareBootMediaInfoPei.inf
#[-end-200420-IB17800056-modify]#
INF $(PLATFORM_BOARD_PACKAGE)/BiosInfo/BiosInfo.inf

#[-start-201223-IB19010009-add]#
[FV.RECOVERYFV]
!if gSiPkgTokenSpaceGuid.PcdAmtEnable == TRUE
INF  $(PLATFORM_FEATURES_PATH)/Amt/AmtStatusCodePei/AmtStatusCodePei.inf
!endif
#[-end-201223-IB19010009-add]#

#[-start-211108-IB19370023-modify]#
!if gBoardModuleTokenSpaceGuid.PcdIntelXmlCliFeatureEnable == TRUE
#[-end-211108-IB19370023-modify]#
  !include XmlCliFeaturePkg/Include/PreMemory.fdf
!endif
#[-start-211208-TAMT000035-add]#
!if $(S77013_SUPPORT_ENABLE) == YES
INF $(PLATFORM_FULL_PACKAGE)/E3/Pei/E3DonglePei/E3DonglePei.inf
!endif
#[-end-211208-TAMT000035-add]#