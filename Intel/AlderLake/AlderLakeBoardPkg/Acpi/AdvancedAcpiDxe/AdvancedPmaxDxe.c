/** @file
  Advanced PMAX table install and update driver

@copyright
  INTEL CONFIDENTIAL
  Copyright 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains a 'Sample Driver' and is licensed as such under the terms
  of your license agreement with Intel or your vendor. This file may be modified
  by the user, subject to the additional terms of the license agreement.

**/

#include <Library/BaseLib.h>
#include <Library/BaseMemoryLib.h>
#include <Library/DebugLib.h>
#include <Library/MemoryAllocationLib.h>
#include <Library/UefiBootServicesTableLib.h>
#include <Protocol/FirmwareVolume2.h>
#include <IndustryStandard/AcpiAml.h>
#include <Library/AslUpdateLib.h>
#include <Library/PcdLib.h>
#include <PlatformBoardConfig.h>
#include <PmaxDevDef.h>

/**
  Calculate the string length, will include NULL-terminated.

  @param[in] StrPtr     Point to the string.

  @retval String length.
**/
UINTN
GetStrLength (
  IN     CHAR8 *StrPtr
  )
{
  UINTN StrLength;

  StrLength = AsciiStrLen (StrPtr);

  return (StrLength + 1);

}

/**
  Install the PMAX table.

  @param[in] Table         Point to PMAX table..

  @retval EFI_SUCCESS      Install table success.
  @retval Other            The function encountered an error and could not complete process.
**/
EFI_STATUS
InstallTable (
  IN     EFI_ACPI_DESCRIPTION_HEADER *Table
  )
{
  EFI_STATUS              Status;
  EFI_ACPI_TABLE_PROTOCOL *mAcpiTable;
  UINTN                   TableKey;

  mAcpiTable = NULL;
  TableKey   = 0;

  //
  // Locate ACPI Table protocol.
  //
  Status = gBS->LocateProtocol (&gEfiAcpiTableProtocolGuid, NULL, (VOID **) &mAcpiTable);
  //
  //  Install table.
  //
  if (!EFI_ERROR (Status)) {
    Status = mAcpiTable->InstallAcpiTable (mAcpiTable, Table, Table->Length, &TableKey);
  }

  FreePool (Table);

  return Status;
}

/**
  Get PCD and update the values.

  @param[in, out] PmaxTable     Point to PMAX table.

  @retval EFI_SUCCESS           The function completed successfully.
  @retval Other                 The function encountered an error and could not complete process.
**/
EFI_STATUS
UpdatePmaxTable (
  IN OUT EFI_ACPI_DESCRIPTION_HEADER *PmaxTable
  )
{
  VPD_PMAX_DEV *PmaxCfg;
  UINT8        Index;
  UINT8        *CurrPtr;
  UINT8        *TableEndPtr;
  UINT32       *NameStrPtr;
  UINT8        *DataPtr;

  CurrPtr     = NULL;
  TableEndPtr = NULL;
  NameStrPtr  = NULL;
  DataPtr     = NULL;

  //
  // Get PCD and check the values.
  //
  PmaxCfg = PcdGetPtr (VpdPcdPmaxDevices);
  for (Index = 0; Index < 4; Index ++) {
    if ((GetStrLength (PmaxCfg[Index].DevStr) > MAX_PMAX_DEVICE_STRING_LENGTH) ||
        (PmaxCfg[Index].DevD0 > MAX_PEAK_POWER_VALUE) || (PmaxCfg[Index].DevDx > MAX_PEAK_POWER_VALUE)) {
          return EFI_INVALID_PARAMETER;
    }
  }

  CurrPtr = (UINT8 *) PmaxTable;
  TableEndPtr = CurrPtr + PmaxTable->Length;

  //
  // Loop through the aml code looking for values that need to be updated.
  //
  for (; CurrPtr < TableEndPtr; CurrPtr++) {
    //
    // Find name aml op code and determine the values need to be updated.
    //
    if ((*CurrPtr) == AML_NAME_OP) {
      NameStrPtr = (UINT32 *) (CurrPtr + 1);
      DataPtr = CurrPtr + 6;

      switch (*NameStrPtr) {
        case (SIGNATURE_32 ('R','T','K','S')):
          //
          // To keep the PMAX table in fixed size, fill the vacancy of the string buffer with AML NO OP.
          // This will makes PMAX table shows numerous "NoOp" after device name.
          //
          SetMem (DataPtr, MAX_PMAX_DEVICE_STRING_LENGTH, AML_NOOP_OP);
          CopyMem (DataPtr, PmaxCfg[0].DevStr, GetStrLength (PmaxCfg[0].DevStr));
          break;

        case (SIGNATURE_32 ('R','T','K','0')):
          CopyMem (DataPtr, &(PmaxCfg[0].DevD0), sizeof (UINT16));
          break;

        case (SIGNATURE_32 ('R','T','K','X')):
          CopyMem (DataPtr, &(PmaxCfg[0].DevDx), sizeof (UINT16));
          break;

        case (SIGNATURE_32 ('W','F','C','S')):
          //
          // To keep the PMAX table in fixed size, fill the vacancy of the string buffer with AML NO OP.
          // This will makes PMAX table shows numerous "NoOp" after device name.
          //
          SetMem (DataPtr, MAX_PMAX_DEVICE_STRING_LENGTH, AML_NOOP_OP);
          CopyMem (DataPtr, PmaxCfg[1].DevStr, GetStrLength (PmaxCfg[1].DevStr));
          break;

        case (SIGNATURE_32 ('W','F','C','0')):
          CopyMem (DataPtr, &(PmaxCfg[1].DevD0), sizeof (UINT16));
          break;

        case (SIGNATURE_32 ('W','F','C','X')):
          CopyMem (DataPtr, &(PmaxCfg[1].DevDx), sizeof (UINT16));
          break;

        case (SIGNATURE_32 ('U','F','C','S')):
          //
          // To keep the PMAX table in fixed size, fill the vacancy of the string buffer with AML NO OP.
          // This will makes PMAX table shows numerous "NoOp" after device name.
          //
          SetMem (DataPtr, MAX_PMAX_DEVICE_STRING_LENGTH, AML_NOOP_OP);
          CopyMem (DataPtr, PmaxCfg[2].DevStr, GetStrLength (PmaxCfg[1].DevStr));
          break;

        case (SIGNATURE_32 ('U','F','C','0')):
          CopyMem (DataPtr, &(PmaxCfg[2].DevD0), sizeof (UINT16));
          break;

        case (SIGNATURE_32 ('U','F','C','X')):
          CopyMem (DataPtr, &(PmaxCfg[2].DevDx), sizeof (UINT16));
          break;

        case (SIGNATURE_32 ('F','L','D','S')):
          //
          // To keep the PMAX table in fixed size, fill the vacancy of the string buffer with AML NO OP.
          // This will makes PMAX table shows numerous "NoOp" after device name.
          //
          SetMem (DataPtr, MAX_PMAX_DEVICE_STRING_LENGTH, AML_NOOP_OP);
          CopyMem (DataPtr, PmaxCfg[3].DevStr, GetStrLength (PmaxCfg[1].DevStr));
          break;

        case (SIGNATURE_32 ('F','L','D','0')):
          CopyMem (DataPtr, &(PmaxCfg[3].DevD0), sizeof (UINT16));
          break;

        case (SIGNATURE_32 ('F','L','D','X')):
          CopyMem (DataPtr, &(PmaxCfg[3].DevDx), sizeof (UINT16));
          break;

        default:
          break;
      }
    }
  }

  return EFI_SUCCESS;
}


/**
  Find PMAX SSDT Table.

  @param[out] PmaxTable     Point to PMAX table.

  @retval EFI_SUCCESS       Find PMAX table successfully.
  @retval Other             The function encountered an error and could not complete process.
**/
EFI_STATUS
FindPmaxTable (
     OUT EFI_ACPI_DESCRIPTION_HEADER **PmaxTable
  )
{
  EFI_STATUS                    Status;
  EFI_HANDLE                    *HandleBuffer;
  UINTN                         NumberOfHandles;
  UINTN                         Index;
  INTN                          Instance;
  UINTN                         Size;
  UINT32                        FvStatus;
  EFI_FV_FILETYPE               FileType;
  EFI_FV_FILE_ATTRIBUTES        Attributes;
  EFI_FIRMWARE_VOLUME2_PROTOCOL *FwVol;
  EFI_ACPI_COMMON_HEADER        *Table;

  Instance = 0;
  FwVol    = NULL;
  Table    = NULL;

  //
  // Locate FV protocol.
  //
  Status = gBS->LocateHandleBuffer (
                  ByProtocol,
                  &gEfiFirmwareVolume2ProtocolGuid,
                  NULL,
                  &NumberOfHandles,
                  &HandleBuffer
                  );

  //
  // Look for FV with ACPI storage file
  //
  for (Index = 0; Index < NumberOfHandles; Index++) {
    //
    // Get the protocol on this handle
    //
    Status = gBS->HandleProtocol (
                    HandleBuffer[Index],
                    &gEfiFirmwareVolume2ProtocolGuid,
                    (VOID **) &FwVol
                    );
    if (FwVol == NULL) {
      return EFI_NOT_FOUND;
    }

    //
    // See if it has the ACPI storage file
    //
    Size      = 0;
    FvStatus  = 0;
    Status = FwVol->ReadFile (
                      FwVol,
                      &gPmaxAcpiTableStorageGuid,
                      NULL,
                      &Size,
                      &FileType,
                      &Attributes,
                      &FvStatus
                      );
    if (!EFI_ERROR (Status)) {
      break;
    }
  }
  //
  // Free any allocated buffers
  //
  FreePool (HandleBuffer);

  //
  // Check that we found the data file
  //
  if (FwVol == NULL) {
    return EFI_NOT_FOUND;
  }

  //
  // Read tables from the storage file.
  //
  while (Status == EFI_SUCCESS) {
    //
    // Read the ACPI tables
    //
    Status = FwVol->ReadSection (
                      FwVol,
                      &gPmaxAcpiTableStorageGuid,
                      EFI_SECTION_RAW,
                      Instance,
                      (VOID **) &Table,
                      &Size,
                      &FvStatus
                      );
    if (!EFI_ERROR (Status)) {
      *PmaxTable = (EFI_ACPI_DESCRIPTION_HEADER *) Table;

      if ((*PmaxTable)->OemTableId == SIGNATURE_64 ('P', 'm', 'a', 'x', '_', 'D', 'e', 'v')) {
        return Status;
      }

      //
      // Increase the instance
      //
      Instance++;
      Table = NULL;
    }
  }
  return Status;
}

/**
  PMAX table installation driver.

  @param[in] ImageHandle     Handle for this drivers loaded image protocol.
  @param[in] SystemTable     EFI system table.

  @retval EFI_SUCCESS        The driver installed without error.
  @retval Other              The driver encountered an error and could not complete installation of
                             the ACPI tables.

**/
EFI_STATUS
EFIAPI
InstallPmaxTable (
  IN EFI_HANDLE         ImageHandle,
  IN EFI_SYSTEM_TABLE   *SystemTable
  )
{
  EFI_STATUS                  Status;
  EFI_ACPI_DESCRIPTION_HEADER *PmaxTable;

  PmaxTable = NULL;

  //
  // Find default PMAX table.
  //
  Status = FindPmaxTable (&PmaxTable);

  //
  // Update PMAX table.
  //
  if ((!EFI_ERROR (Status)) && (PmaxTable != NULL)) {
    Status = UpdatePmaxTable (PmaxTable);
  }

  //
  // Install PMAX table.
  //
  if ((!EFI_ERROR (Status)) && (PmaxTable != NULL)) {
    Status = InstallTable (PmaxTable);
  }

  return Status;
}


