/** @file

;******************************************************************************
;* Copyright (c) 2021, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/
/** @file
  Advanced ACPI Driver

@copyright
  INTEL CONFIDENTIAL
  Copyright 2011 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains a 'Sample Driver' and is licensed as such under the terms
  of your license agreement with Intel or your vendor. This file may be modified
  by the user, subject to the additional terms of the license agreement.

@par Specification Reference:
**/
#include <Base.h>
#include <Uefi.h>
#include <Library/HobLib.h>
#include <Guid/HobList.h>
#include <Protocol/FirmwareVolume2.h>
#include <PlatformBoardId.h>
#include <IndustryStandard/AcpiAml.h>
#include <IndustryStandard/Pci30.h>
#include "AcpiPlatform.h"
#include <Library/PciSegmentLib.h>
#include <Library/PchInfoLib.h>
#include <Library/PchPcieRpLib.h>
#include <Library/SerialIoAccessLib.h>
#include <Library/GpioLib.h>
#include <Library/EspiLib.h>
#include <Library/GbeLib.h>
#include <Library/PchPciBdfLib.h>
#include <MeBiosPayloadHob.h>
#include <PlatformBoardConfig.h>
#include <Library/CpuPlatformLib.h>
#include <PlatformBoardType.h>
#include <PlatformInfo.h>
#include <Guid/S3MemoryVariable.h>
//[-start-190620-IB11270237-add]//
#include <Library/BaseOemSvcChipsetLib.h>
#include <Library/DxeOemSvcChipsetLib.h>
#include <Library/AcpiPlatformLib.h>
#include <ChipsetSetupConfig.h>
#include <Library/VariableLib.h>
//[-end-190620-IB11270237-add]//
#include <Protocol/VariableLock.h>
#include <Mcfg.h>
#if FixedPcdGetBool(PcdITbtEnable) == 1
#include <TcssDataHob.h>
#endif
#include <Usb4PlatformHob.h>
#include <Library/IpuLib.h>
#include <Library/GnaInfoLib.h>
#include <Library/VmdInfoLib.h>
#include <Library/SataLib.h>
#include <Library/SataSocLib.h>
#include <Library/GraphicsInfoLib.h>
#include "GenSsdtLib.h"
//[-start-210127-IB05660155-add]//
#include <HybridGraphicsDefine.h>
//[-emd-210127-IB05660155-add]//

//
// Global variables
//
GLOBAL_REMOVE_IF_UNREFERENCED VOID                                      *mGfxNvsAddress;
GLOBAL_REMOVE_IF_UNREFERENCED ME_SETUP                                  mMeSetup;
GLOBAL_REMOVE_IF_UNREFERENCED PCH_SETUP                                 mPchSetup;
GLOBAL_REMOVE_IF_UNREFERENCED SETUP_DATA                                mSystemConfiguration;
//[-start-190620-IB11270237-add]//
GLOBAL_REMOVE_IF_UNREFERENCED CHIPSET_CONFIGURATION                     mChipsetSetupData;
//[-end-190620-IB11270237-add]//
GLOBAL_REMOVE_IF_UNREFERENCED PLATFORM_NVS_AREA_PROTOCOL                mPlatformNvsAreaProtocol;

GLOBAL_REMOVE_IF_UNREFERENCED EFI_CPU_IO2_PROTOCOL                      *mCpuIo;
GLOBAL_REMOVE_IF_UNREFERENCED EFI_CPU_ID_ORDER_MAP                      *mCpuApicIdOrderTable;
GLOBAL_REMOVE_IF_UNREFERENCED EFI_CPU_APIC_ID_REORDER_MAP               *mCpuApicIdReorderTable;
GLOBAL_REMOVE_IF_UNREFERENCED UINTN                                     mNumberOfCpus;
GLOBAL_REMOVE_IF_UNREFERENCED UINTN                                     mNumberOfEnabledCpus;
GLOBAL_REMOVE_IF_UNREFERENCED BOOLEAN                                   IsX2ApicMode;

GLOBAL_REMOVE_IF_UNREFERENCED EFI_CPU_PACKAGE_INFO                      mSbspPackageInfo;
#if FixedPcdGetBool(PcdITbtEnable) == 1
GLOBAL_REMOVE_IF_UNREFERENCED TCSS_DATA_HOB                             *mTcssHob;
#endif
//
// Function implementations
//

/**
  Install Soc ACPI Table

  @param[in] *SsdtTableGuid      Pointer to GUID that identifies new SSDT table
  @param[in] Signature           Signature of the table

**/
VOID
STATIC
InstallSocAcpiTable (
  IN EFI_GUID  *SsdtTableGuid,
  IN UINT64    Signature
  )
{
  EFI_STATUS                    Status;
  EFI_HANDLE                    *HandleBuffer;
  UINTN                         NumberOfHandles;
  UINTN                         Index;
  INTN                          Instance;
  UINTN                         Size;
  UINT32                        FvStatus;
  UINTN                         TableHandle;
  EFI_FV_FILETYPE               FileType;
  EFI_FV_FILE_ATTRIBUTES        Attributes;
  EFI_FIRMWARE_VOLUME2_PROTOCOL *FwVol;
  EFI_ACPI_TABLE_PROTOCOL       *AcpiTable;
  EFI_ACPI_DESCRIPTION_HEADER   *TableHeader;
  EFI_ACPI_COMMON_HEADER        *Table;

  FwVol         = NULL;
  Table         = NULL;

  //
  // Locate FV protocol.
  //
  Status = gBS->LocateHandleBuffer (
                  ByProtocol,
                  &gEfiFirmwareVolume2ProtocolGuid,
                  NULL,
                  &NumberOfHandles,
                  &HandleBuffer
                  );
  ASSERT_EFI_ERROR (Status);

  //
  // Look for FV with ACPI storage file
  //
  for (Index = 0; Index < NumberOfHandles; Index++) {
    //
    // Get the protocol on this handle
    // This should not fail because of LocateHandleBuffer
    //
    Status = gBS->HandleProtocol (
                    HandleBuffer[Index],
                    &gEfiFirmwareVolume2ProtocolGuid,
                    (VOID **) &FwVol
                    );
    ASSERT_EFI_ERROR (Status);
    if (FwVol == NULL) {
      return;
    }
    //
    // See if it has the ACPI storage file
    //
    Size      = 0;
    FvStatus  = 0;
    Status = FwVol->ReadFile (
                      FwVol,
                      SsdtTableGuid,
                      NULL,
                      &Size,
                      &FileType,
                      &Attributes,
                      &FvStatus
                      );
    if (!EFI_ERROR (Status)) {
      break;
    }
  }
  //
  // Free any allocated buffers
  //
  FreePool (HandleBuffer);

  //
  // Sanity check that we found our data file
  //
  ASSERT (FwVol);

  //
  // Locate ACPI tables
  //
  Status = gBS->LocateProtocol (&gEfiAcpiTableProtocolGuid, NULL, (VOID **) &AcpiTable);

  //
  // Read tables from the storage file.
  //
  if (FwVol == NULL) {
    ASSERT_EFI_ERROR (EFI_NOT_FOUND);
    return;
  }
  Instance = 0;

  while (Status == EFI_SUCCESS) {
    //
    // Read the ACPI tables
    //
    Status = FwVol->ReadSection (
                      FwVol,
                      SsdtTableGuid,
                      EFI_SECTION_RAW,
                      Instance,
                      (VOID **) &Table,
                      &Size,
                      &FvStatus
                      );
    if (!EFI_ERROR (Status)) {
      TableHeader = (EFI_ACPI_DESCRIPTION_HEADER *) Table;

      if (((EFI_ACPI_DESCRIPTION_HEADER *) TableHeader)->OemTableId == Signature) {
        TableHandle = 0;
        //
        // Add the table
        //
        Status = AcpiTable->InstallAcpiTable (
                              AcpiTable,
                              TableHeader,
                              TableHeader->Length,
                              &TableHandle
                              );
      }

      //
      // Increment the instance
      //
      Instance++;
      Table = NULL;
    }
  }
}

/**
  Install Xhci ACPI Table
**/
VOID
InstallXhciAcpiTable (
  VOID
  )
{
  EFI_STATUS                    Status;
  EFI_FIRMWARE_VOLUME2_PROTOCOL *FwVol;
  EFI_ACPI_COMMON_HEADER        *CurrentTable;
  UINT32                        FvStatus;
  UINTN                         Size;
  UINTN                         TableHandle;
  INTN                          Instance;
  EFI_ACPI_TABLE_PROTOCOL       *AcpiTable;
  EFI_ACPI_DESCRIPTION_HEADER   *TableHeader;
  EFI_GUID                      EfiAcpiMultiTableStorageGuid;

  EfiAcpiMultiTableStorageGuid = gAcpiTableStorageGuid;
  Instance      = 0;
  TableHandle   = 0;
  CurrentTable  = NULL;
  FwVol         = NULL;

  DEBUG ((DEBUG_INFO, "InstallXhciAcpiTable\n"));
  //
  // Update OEM table ID
  //
  if (PcdGet64 (PcdXhciAcpiTableSignature) == 0) {
    DEBUG ((DEBUG_INFO, "InstallXhciAcpiTable: Cannot find PcdXhciAcpiTableSignature\n"));
    return;
  }

  //
  // Find the AcpiSupport protocol
  //
  Status = LocateSupportProtocol (
            &gEfiAcpiTableProtocolGuid,
            EfiAcpiMultiTableStorageGuid,
            (VOID **) &AcpiTable,
            FALSE
            );
  ASSERT_EFI_ERROR (Status);

  //
  // Locate the firmware volume protocol
  //
  Status = LocateSupportProtocol (
            &gEfiFirmwareVolume2ProtocolGuid,
            EfiAcpiMultiTableStorageGuid,
            (VOID **) &FwVol,
            TRUE
            );

  //
  // Read tables from the storage file.
  //
  while (Status == EFI_SUCCESS) {
    Status = FwVol->ReadSection (
                      FwVol,
                      &EfiAcpiMultiTableStorageGuid,
                      EFI_SECTION_RAW,
                      Instance,
                      (VOID **) &CurrentTable,
                      &Size,
                      &FvStatus
                      );

    if (!EFI_ERROR (Status)) {

      TableHeader = (EFI_ACPI_DESCRIPTION_HEADER *) CurrentTable;

      if (TableHeader->OemTableId == PcdGet64 (PcdXhciAcpiTableSignature)) {
        DEBUG ((DEBUG_INFO, "Install xhci table: %x\n", TableHeader->OemTableId));
        //
        // Add the table
        //
        TableHandle = 0;

        Status = AcpiTable->InstallAcpiTable (
                              AcpiTable,
                              CurrentTable,
                              CurrentTable->Length,
                              &TableHandle
                              );

        break;
      }

      //
      // Increment the instance
      //
      Instance++;
      gBS->FreePool (CurrentTable);
      CurrentTable = NULL;
    }
  }

  if (CurrentTable != NULL) {
    gBS->FreePool (CurrentTable);
    CurrentTable = NULL;
  }
}
//[-start-190620-IB11270237-remove]//
//VOID
//PublishOverClockingAcpiTable (
//  VOID
//  )
//{
//  EFI_STATUS                    Status;
//  EFI_HANDLE                    *HandleBuffer;
//  UINTN                         NumberOfHandles;
//  UINTN                         Index;
//  EFI_FIRMWARE_VOLUME2_PROTOCOL *FwVol;
//  EFI_ACPI_TABLE_PROTOCOL       *AcpiTable;
//  EFI_ACPI_COMMON_HEADER        *Table;
//  UINTN                         Size;
//  UINT32                        FvStatus;
//  EFI_ACPI_DESCRIPTION_HEADER   *TableHeader;
//  UINTN                         TableHandle;
//
//  FwVol = NULL;
//  Table = NULL;
//
//  DEBUG ((DEBUG_INFO, "PublishOverClockingAcpiTable\n"));
//
//  //
//  // Locate FV protocol.
//  //
//  Status = gBS->LocateHandleBuffer (
//                  ByProtocol,
//                  &gEfiFirmwareVolume2ProtocolGuid,
//                  NULL,
//                  &NumberOfHandles,
//                  &HandleBuffer
//                  );
//  ASSERT_EFI_ERROR (Status);
//
//  //
//  // Look for FV with ACPI storage file
//  //
//  for (Index = 0; Index < NumberOfHandles; Index++) {
//
//    //
//    // Get the protocol on this handle
//    // This should not fail because of LocateHandleBuffer
//    //
//    Status = gBS->HandleProtocol (
//                  HandleBuffer[Index],
//                  &gEfiFirmwareVolume2ProtocolGuid,
//                  (VOID **) &FwVol
//                  );
//    ASSERT_EFI_ERROR (Status);
//
//    //
//    // See if it has the OverClocking Acpi Table file
//    //
//    Status = FwVol->ReadSection (
//                      FwVol,
//                      &gOcAcpiTableStorageGuid,
//                      EFI_SECTION_RAW,
//                      0,
//                      (VOID **) &Table,
//                      &Size,
//                      &FvStatus
//                      );
//    if (!EFI_ERROR (Status)) {
//      break;
//    }
//  }
//  //
//  // Free any allocated buffers
//  //
//  FreePool (HandleBuffer);
//
//  //
//  // Not find the table, it's fine the over clocking support is not enabled
//  //
//  if (Table == NULL) {
//    return;
//  }
//
//  Status = gBS->LocateProtocol (&gEfiAcpiTableProtocolGuid, NULL, (VOID **) &AcpiTable);
//
//  TableHeader = (EFI_ACPI_DESCRIPTION_HEADER *) Table;
//  if (CompareMem (&TableHeader->OemTableId, "PerfTune", 8) == 0) {
//    TableHandle = 0;
//    Status = AcpiTable->InstallAcpiTable (
//                          AcpiTable,
//                          Table,
//                          Table->Length,
//                          &TableHandle
//                          );
//  }
//
//  if (Table != NULL) {
//    gBS->FreePool (Table);
//    Table = NULL;
//  }
//
//}
//[-start-190620-IB11270237-remove]//
/**
  Locate the first instance of a protocol.  If the protocol requested is an
  FV protocol, then it will return the first FV that contains the ACPI table
  storage file.

  @param[in] Protocol           The protocol to find.
  @param[in] Instance           Return pointer to the first instance of the protocol.
  @param[in] Type               TRUE if the desired protocol is a FV protocol.

  @retval EFI_SUCCESS           The function completed successfully.
  @retval EFI_NOT_FOUND         The protocol could not be located.
  @retval EFI_OUT_OF_RESOURCES  There are not enough resources to find the protocol.
**/
EFI_STATUS
LocateSupportProtocol (
  IN     EFI_GUID                      *Protocol,
  IN     EFI_GUID                      gEfiAcpiMultiTableStorageGuid,
     OUT VOID                          **Instance,
  IN     BOOLEAN                       Type
  )
{
  EFI_STATUS              Status;
  EFI_HANDLE              *HandleBuffer;
  UINTN                   NumberOfHandles;
  EFI_FV_FILETYPE         FileType;
  UINT32                  FvStatus;
  EFI_FV_FILE_ATTRIBUTES  Attributes;
  UINTN                   Size;
  UINTN                   Index;

  //
  // Locate protocol.
  //
  Status = gBS->LocateHandleBuffer (
                  ByProtocol,
                  Protocol,
                  NULL,
                  &NumberOfHandles,
                  &HandleBuffer
                  );
  if (EFI_ERROR (Status)) {
    //
    // Defined errors at this time are not found and out of resources.
    //
    return Status;
  }

  //
  // Looking for FV with ACPI storage file
  //
  for (Index = 0; Index < NumberOfHandles; Index++) {

    //
    // Get the protocol on this handle
    // This should not fail because of LocateHandleBuffer
    //
    Status = gBS->HandleProtocol (
                    HandleBuffer[Index],
                    Protocol,
                    Instance
                    );
    ASSERT_EFI_ERROR (Status);

    if (!Type) {

      //
      // Not looking for the FV protocol, so find the first instance of the
      // protocol.  There should not be any errors because our handle buffer
      // should always contain at least one or LocateHandleBuffer would have
      // returned not found.
      //
      break;
    }

    //
    // See if it has the ACPI storage file
    //
    Size      = 0;
    FvStatus  = 0;
    Status = ((EFI_FIRMWARE_VOLUME2_PROTOCOL *) (*Instance))->ReadFile (
                                                              *Instance,
                                                              &gEfiAcpiMultiTableStorageGuid,
                                                              NULL,
                                                              &Size,
                                                              &FileType,
                                                              &Attributes,
                                                              &FvStatus
                                                              );

    //
    // If we found it, then we are done
    //
    if (Status == EFI_SUCCESS) {
      break;
    }
  }

  //
  // Our exit status is determined by the success of the previous operations
  // If the protocol was found, Instance already points to it.
  //
  //
  // Free any allocated buffers
  //
  FreePool (HandleBuffer);

  return Status;
}

/*
  This function will determine whether Native ASPM is supported on the platform.

  @param[in] SystemConfiguration     System configuration data

  @return TRUE if Native ASPM is supported, FALSE otherwise
*/
BOOLEAN
IsNativeAspmSupported (
//[-start-190620-IB11270237-remove]//
//  CONST SETUP_DATA* SystemConfiguration
//[-end-190620-IB11270237-remove]//
  )
{
//[-start-190620-IB11270237-modify]//
//  if (SystemConfiguration->PciExpNative == 0) {
  if (mSystemConfiguration.PciExpNative == 0) {
    return FALSE;
  }
//  switch (SystemConfiguration->NativeAspmEnable) {
  switch (mSystemConfiguration.NativeAspmEnable) {
//[-end-190620-IB11270237-modify]//
  case 0:
    return FALSE;
  case 1:
    return TRUE;
  case 2: /* AUTO */
    return PchIsPcieNativeAspmSupported ();
  default:
    ASSERT (0);
  }
  return TRUE;
}

/**
  This function will provide the EC GPE number for ECDT update according to platform design

  @retval EcGpeNumber     EC GPE number to be reported in ECDT
**/
UINT32
GetEcGpeNumber (
  VOID
  )
{
  UINT32                                EcGpeNumber;

  //
  // EC GPE number assignment should be same as Method _GPE under ACPI EC Device which is used for OS without ECDT support
  //
  if (IsEspiEnabled ()) {
    EcGpeNumber = 0x6E;
  } else {
    EcGpeNumber = 23;
    if (mPlatformNvsAreaProtocol.Area->PcdSmcRuntimeSciPin != 0) {
      GpioGetGpeNumber ((GPIO_PAD) mPlatformNvsAreaProtocol.Area->PcdSmcRuntimeSciPin, &EcGpeNumber);
    }
  }

  DEBUG((DEBUG_INFO, "ACPI EcGpeNumber = 0x%x\n ", EcGpeNumber));

  return EcGpeNumber;
}

/**
  This function will update any runtime platform specific information.
  This currently includes:
    Setting OEM table values, ID, table ID, creator ID and creator revision.
    Enabling the proper processor entries in the APIC tables
  It also indicates with which ACPI table version the table belongs.

  @param[in] Table        The table to update
  @param[in] Version      Where to install this table

  @retval EFI_SUCCESS     Updated tables commplete.
**/
EFI_STATUS
PlatformUpdateTables (
  IN OUT EFI_ACPI_COMMON_HEADER       *Table,
  IN OUT EFI_ACPI_TABLE_VERSION       *Version
  )
{
  EFI_ACPI_DESCRIPTION_HEADER                 *TableHeader;
  UINT8                                       *CurrPtr;
  UINT8                                       *TmpDsdtPointer;
  UINT8                                       *EndPtr;
  EFI_ACPI_1_0_PROCESSOR_LOCAL_APIC_STRUCTURE *ApicPtr;
  EFI_STATUS                                  Status;
  EFI_MP_SERVICES_PROTOCOL                    *MpService;

  UINT8                                       MaximumDsdtPointLength;

//[-start-190613-IB11270237-remove]//
//  EFI_PCI_ROOT_BRIDGE_IO_PROTOCOL             *PciRootBridgeIo;
//[-end-190613-IB11270237-remove]//
  UINT32                                      HpetBaseAdress;
  UINT32                                      LocalApicCounter;
  //UINT32                                      GcsRegister;
  SA_SETUP                                    SaSetup;
  UINTN                                       VariableSize;


  CurrPtr           = NULL;
  EndPtr            = NULL;
  ApicPtr           = NULL;

  LocalApicCounter  = 0;
  MaximumDsdtPointLength = 20;

  VariableSize = sizeof (SA_SETUP);
  Status = gRT->GetVariable (
                  L"SaSetup",
                  &gSaSetupVariableGuid,
                  NULL,
                  &VariableSize,
                  &SaSetup
                  );
  //
  // This will be accurate except for FACS structure
  //
  TableHeader = (EFI_ACPI_DESCRIPTION_HEADER *) Table;

  //
  // Update the OEM and creator information for every table except FACS.
  //
  if (Table->Signature != EFI_ACPI_1_0_FIRMWARE_ACPI_CONTROL_STRUCTURE_SIGNATURE) {
    if ((Table->Signature == EFI_ACPI_1_0_SECONDARY_SYSTEM_DESCRIPTION_TABLE_SIGNATURE) &&
        (CompareMem (&TableHeader->OemId, "Rtd3", 4) == 0)) {
      // For SsdtRtd3 table, "OemId" update is done later
    } else {
      CopyMem (TableHeader->OemId, PcdGetPtr (PcdAcpiDefaultOemId), sizeof (TableHeader->OemId));
    }

    //
    // Skip OEM table ID and creator information for SSDT tables, since these are
    // created by an ASL compiler and the creator information is useful.
    //
    if (Table->Signature != EFI_ACPI_1_0_SECONDARY_SYSTEM_DESCRIPTION_TABLE_SIGNATURE) {
      //
      // Set through patchable PCD value
      //
      TableHeader->OemTableId = PcdGet64 (PcdAcpiDefaultOemTableId);
      TableHeader->OemRevision = PcdGet32(PcdAcpiDefaultOemRevision);

      //
      // Update creator information
      //
       TableHeader->CreatorId        = PcdGet32 (PcdAcpiDefaultCreatorId);
       TableHeader->CreatorRevision  = PcdGet32 (PcdAcpiDefaultCreatorRevision) ;
    }
  }

//[-start-190613-IB11270237-remove]//
//  //
//  // Locate the PCI IO protocol
//  //
//  Status = gBS->LocateProtocol (
//                  &gEfiPciRootBridgeIoProtocolGuid,
//                  NULL,
//                  (VOID **) &PciRootBridgeIo
//                  );
//  ASSERT_EFI_ERROR (Status);
//[-end-190613-IB11270237-remove]//

  //
  // Locate the MP services protocol
  // Find the MP Protocol. This is an MP platform, so MP protocol must be there.
  //
  Status = gBS->LocateProtocol (
                  &gEfiMpServiceProtocolGuid,
                  NULL,
                  (VOID **) &MpService
                  );
  ASSERT_EFI_ERROR (Status);

  //
  // Get supported CPU Interrupt model (xApic or x2Apic)
  //
  IsX2ApicMode = (GetApicMode() == LOCAL_APIC_MODE_X2APIC) ? TRUE : FALSE;

  //
  // By default, a table belongs in all ACPI table versions published.
  // Some tables will override this because they have different versions of the table.
  //
  *Version = EFI_ACPI_TABLE_VERSION_1_0B | EFI_ACPI_TABLE_VERSION_2_0 | EFI_ACPI_TABLE_VERSION_3_0;

  //
  // Update the various table types with the necessary updates
  //
  switch (Table->Signature) {

  case EFI_ACPI_1_0_APIC_SIGNATURE:
    //
    // if not MP and not APIC then don't publish the APIC tables.
    //
      //@todo mSystemConfiguration.ApicEnabled default value is 1. So, the following logic is not executed.
      //if (mSystemConfiguration.ApicEnabled == 0) {
      //  *Version = EFI_ACPI_TABLE_VERSION_NONE;
      //  break;
      //}
    CurrPtr = (UINT8 *) &((EFI_ACPI_DESCRIPTION_HEADER *) Table)[1];
    CurrPtr = CurrPtr + 8;
    //
    // Size of Local APIC Address & Flag
    //
    EndPtr  = (UINT8 *) Table;
    EndPtr  = EndPtr + Table->Length;

    ApicPtr = (EFI_ACPI_1_0_PROCESSOR_LOCAL_APIC_STRUCTURE *) CurrPtr;
    DEBUG((DEBUG_INFO, "ApicPtr Type = %x \n ", ApicPtr->Type));
    if ((IsX2ApicMode && ApicPtr->Type == EFI_ACPI_1_0_PROCESSOR_LOCAL_APIC) ||(!IsX2ApicMode && ApicPtr->Type == EFI_ACPI_6_3_PROCESSOR_LOCAL_X2APIC)) {
      // Lets skip the MADT table in below cases:
      // 1. MADT table with Local APIC records when X2Apic Mode is enabled.
      // 2. MADT table with x2Apic records when xApic Mode is enabled.

      *Version = EFI_ACPI_TABLE_VERSION_NONE;
      break;
    }

    //
    // Call for Local APIC ID Reorder
    //
    SortCpuLocalApicInTable(MpService);

    while (CurrPtr < EndPtr) {
      ApicPtr = (EFI_ACPI_1_0_PROCESSOR_LOCAL_APIC_STRUCTURE *) CurrPtr;

      //
      // Check table entry type
      //
      if ((ApicPtr->Type == EFI_ACPI_1_0_PROCESSOR_LOCAL_APIC) || (ApicPtr->Type == EFI_ACPI_6_3_PROCESSOR_LOCAL_X2APIC)) {
        AppendCpuMapTableEntry (
          ApicPtr,
          LocalApicCounter
          );
        LocalApicCounter++;
      }

      //
      // Go to the next structure in the APIC table
      //
      CurrPtr = CurrPtr + ApicPtr->Length;
    }
    break;

  case EFI_ACPI_6_3_FIXED_ACPI_DESCRIPTION_TABLE_SIGNATURE:
    //
    // Fix up all FACP Table values if configuration requires it.
    // This code fixes up the following Table values:
    // (1) C2/C3/CST Enable FACP values
    // (2) RTC S4 Flag
    //
    {
      EFI_ACPI_6_3_FIXED_ACPI_DESCRIPTION_TABLE *FadtPointer;

      FadtPointer = (EFI_ACPI_6_3_FIXED_ACPI_DESCRIPTION_TABLE *) Table;

      //
      // Check the version of the table
      //
      *Version = EFI_ACPI_TABLE_VERSION_NONE;
      if (FadtPointer->Header.Revision == EFI_ACPI_1_0_FIXED_ACPI_DESCRIPTION_TABLE_REVISION) {
        *Version = EFI_ACPI_TABLE_VERSION_NONE;
      } else if (FadtPointer->Header.Revision == EFI_ACPI_2_0_FIXED_ACPI_DESCRIPTION_TABLE_REVISION) {
        *Version = EFI_ACPI_TABLE_VERSION_1_0B | EFI_ACPI_TABLE_VERSION_2_0;
      } else if (FadtPointer->Header.Revision == EFI_ACPI_3_0_FIXED_ACPI_DESCRIPTION_TABLE_REVISION) {
        *Version = EFI_ACPI_TABLE_VERSION_1_0B | EFI_ACPI_TABLE_VERSION_3_0;
      } else if (FadtPointer->Header.Revision == EFI_ACPI_5_0_FIXED_ACPI_DESCRIPTION_TABLE_REVISION) {
        *Version = EFI_ACPI_TABLE_VERSION_1_0B | EFI_ACPI_TABLE_VERSION_5_0;
      } else if (FadtPointer->Header.Revision == EFI_ACPI_6_1_FIXED_ACPI_DESCRIPTION_TABLE_REVISION) {
        *Version = EFI_ACPI_TABLE_VERSION_5_0;
      } else if (FadtPointer->Header.Revision == EFI_ACPI_6_3_FIXED_ACPI_DESCRIPTION_TABLE_REVISION) {
        *Version = EFI_ACPI_TABLE_VERSION_5_0;
      }

      //
      // Modify MSI capability based on setup
      //
      if (mSystemConfiguration.EnableMsiInFadt == 0) {
        FadtPointer->IaPcBootArch |= (1 << 3);
      }

      //
      // Modify Preferred_PM_Profile field based on Board SKU's. Default is set to Mobile
      //
      FadtPointer->PreferredPmProfile = PcdGet8 (PcdPreferredPmProfile);

      //
      // if Native ASPM is disabled, set FACP table to skip Native ASPM
      //
//[-start-200224-IB14630330-modify]//
      if (!IsNativeAspmSupported ()) {
//[-end-200224-IB14630330-modify]//
        FadtPointer->IaPcBootArch |= 0x10;
      }

      //
      // PME WAKE supported, set PCI_EXP_WAK, BIT14 of Fixed feature flags.
      //
      FadtPointer->Flags |= (EFI_ACPI_6_0_PCI_EXP_WAK);

      if (mSystemConfiguration.LowPowerS0Idle) {
        //
        // The Flags field within the FADT (offset 112)
        //   1) will have a new Low Power S0 Idle Capable ACPI flag (bit offset 21).
        //
        FadtPointer->Flags = (BIT21 | FadtPointer->Flags);
        //
        //Only passive docking available in Conected Standby mode. Clear Docking capability Bit
        //
        FadtPointer->Flags &= ~BIT9;
        //
        // set indicates the power button is handled as a control method device.
        //
          FadtPointer->Flags |= EFI_ACPI_2_0_PWR_BUTTON;
      } else {
        FadtPointer->Flags &= ~(EFI_ACPI_2_0_PWR_BUTTON); // clear indicates the power button is handled as a fixed feature programming model
      }

      //
      // 1. set header revision.
      //
      FadtPointer->Header.Revision = EFI_ACPI_6_3_FIXED_ACPI_DESCRIPTION_TABLE_REVISION;

      //
      // 2. set all GAR register AccessSize to valid value.
      //
      ((EFI_ACPI_6_3_FIXED_ACPI_DESCRIPTION_TABLE *)FadtPointer)->ResetReg.AccessSize    = EFI_ACPI_6_3_BYTE;
      ((EFI_ACPI_6_3_FIXED_ACPI_DESCRIPTION_TABLE *)FadtPointer)->XPm1aEvtBlk.AccessSize = EFI_ACPI_6_3_WORD;
      ((EFI_ACPI_6_3_FIXED_ACPI_DESCRIPTION_TABLE *)FadtPointer)->XPm1bEvtBlk.AccessSize = EFI_ACPI_6_3_WORD;
      ((EFI_ACPI_6_3_FIXED_ACPI_DESCRIPTION_TABLE *)FadtPointer)->XPm1aCntBlk.AccessSize = EFI_ACPI_6_3_WORD;
      ((EFI_ACPI_6_3_FIXED_ACPI_DESCRIPTION_TABLE *)FadtPointer)->XPm1bCntBlk.AccessSize = EFI_ACPI_6_3_WORD;
      ((EFI_ACPI_6_3_FIXED_ACPI_DESCRIPTION_TABLE *)FadtPointer)->XPm2CntBlk.AccessSize  = EFI_ACPI_6_3_BYTE;
      ((EFI_ACPI_6_3_FIXED_ACPI_DESCRIPTION_TABLE *)FadtPointer)->XPmTmrBlk.AccessSize   = EFI_ACPI_6_3_DWORD;
      ((EFI_ACPI_6_3_FIXED_ACPI_DESCRIPTION_TABLE *)FadtPointer)->XGpe0Blk.AccessSize    = EFI_ACPI_6_3_BYTE;
      ((EFI_ACPI_6_3_FIXED_ACPI_DESCRIPTION_TABLE *)FadtPointer)->XGpe1Blk.AccessSize    = EFI_ACPI_6_3_BYTE;

      //
      // SLEEP_CONTROL_REG and SLEEP_STATUS_REG are optional
      // Only implement in HW-Reduced ACPI system
      // In non-HW-Reduced system will remain all 0 as default
      //
      if ((FadtPointer->Flags & EFI_ACPI_6_3_HW_REDUCED_ACPI) != 0) {
        //
        // If the HW_REDUCED_ACPI flag in the table is set, OSPM will ignore fields related to the ACPI HW
        //
        ((EFI_ACPI_6_3_FIXED_ACPI_DESCRIPTION_TABLE *)FadtPointer)->SleepControlReg.AddressSpaceId    = 0x1;
        ((EFI_ACPI_6_3_FIXED_ACPI_DESCRIPTION_TABLE *)FadtPointer)->SleepControlReg.RegisterBitWidth  = 0x8;
        ((EFI_ACPI_6_3_FIXED_ACPI_DESCRIPTION_TABLE *)FadtPointer)->SleepControlReg.RegisterBitOffset = 0;
        ((EFI_ACPI_6_3_FIXED_ACPI_DESCRIPTION_TABLE *)FadtPointer)->SleepControlReg.AccessSize        = EFI_ACPI_6_3_DWORD;
//[-start-190620-IB11270237-modify]//
        ((EFI_ACPI_5_0_FIXED_ACPI_DESCRIPTION_TABLE *)FadtPointer)->SleepControlReg.Address           = FixedPcdGet16 (PcdPerfPkgAcpiIoPortBaseAddress) + 4;
//[-end-190620-IB11270237-modify]//
        ((EFI_ACPI_6_3_FIXED_ACPI_DESCRIPTION_TABLE *)FadtPointer)->SleepStatusReg.AddressSpaceId     = 0x1;
        ((EFI_ACPI_6_3_FIXED_ACPI_DESCRIPTION_TABLE *)FadtPointer)->SleepStatusReg.RegisterBitWidth   = 0x8;
        ((EFI_ACPI_6_3_FIXED_ACPI_DESCRIPTION_TABLE *)FadtPointer)->SleepStatusReg.RegisterBitOffset  = 0;
        ((EFI_ACPI_6_3_FIXED_ACPI_DESCRIPTION_TABLE *)FadtPointer)->SleepStatusReg.AccessSize         = EFI_ACPI_6_3_DWORD;
//[-start-190620-IB11270237-modify]//
        ((EFI_ACPI_5_0_FIXED_ACPI_DESCRIPTION_TABLE *)FadtPointer)->SleepStatusReg.Address            = FixedPcdGet16 (PcdPerfPkgAcpiIoPortBaseAddress);
//[-end-190620-IB11270237-modify]//
      }

      //
      // Set the S4BiosReq to 0 as we don't support it in the FACS for TGL.
      //
      FadtPointer->S4BiosReq = 0;

        //@todo RtcS4WakeEnable and PmTimerEnable default value is 1. So, the following logic is not executed.
        ////
        //// Check if RTC S4 is enabled
        ////
        //if (!mSystemConfiguration.RtcS4WakeEnable) {

        //  //
        //  // Clear the S4 RTC flag
        //  //
        //  FadtPointer->Flags &= ~EFI_ACPI_2_0_RTC_S4;
        //}

        ////
        //// Check if PM timer is enabled
        ////
        //if (!mSystemConfiguration.PmTimerEnable) {

        //  //
        //  // Clear the PM timer flag
        //  //
        //  FadtPointer->Flags &= ~EFI_ACPI_3_0_USE_PLATFORM_CLOCK;
        //}
    }
    break;

  case EFI_ACPI_1_0_PERSISTENT_SYSTEM_DESCRIPTION_TABLE_SIGNATURE:
    //
    // These items only belong in the 1.0 tables.
    //
    *Version = EFI_ACPI_TABLE_VERSION_1_0B;
    break;

  case EFI_ACPI_2_0_DIFFERENTIATED_SYSTEM_DESCRIPTION_TABLE_SIGNATURE:
    //
    // Fix up the AML code in the DSDT affected by end user options.
    // Fix up the following ASL Code:
    // (1)  ACPI Global NVS Memory Base and Size.
    // (2)  ACPI Graphics NVS Memory Base and Size.
    // (3)  SMBus I/O Base.
    // (4)  Thermal Management Methods.
    //
    {
      UINT8   *DsdtPointer;
      UINT32  *Signature;
      UINT8   *Operation;
//[-start-190620-IB11270237-modify]//
//      UINT32  *Address;
      UINT8   *Value;
//      UINT16  *Size;
//[-end-190620-IB11270237-modify]//
      BOOLEAN EnterDock = FALSE;
      //
      // Loop through the ASL looking for values that we must fix up.
      //
      CurrPtr = (UINT8 *) Table;
      for (DsdtPointer = CurrPtr;
//[-start-190620-IB11270237-modify]//
           DsdtPointer <= (CurrPtr + ((EFI_ACPI_COMMON_HEADER *) CurrPtr)->Length);
//[-end-190620-IB11270237-modify]//
           DsdtPointer++
          ) {
        Signature = (UINT32 *) DsdtPointer;
        switch (*Signature) {
        //
        // GNVS operation region
        //
        case (SIGNATURE_32 ('G', 'N', 'V', 'S')):
          //
          // Conditional match.  For Region Objects, the Operator will always be the
          // byte immediately before the specific name.  Therefore, subtract 1 to check
          // the Operator.
          //
//[-start-190620-IB11270237-modify]//
//          Operation = DsdtPointer - 1;
//          if (*Operation == AML_EXT_REGION_OP) {
//            Address   = (UINT32 *) (DsdtPointer + 6);
//            *Address  = (UINT32) (UINTN) mPlatformNvsAreaProtocol.Area;
//            Size      = (UINT16 *) (DsdtPointer + 11);
//            *Size     = sizeof (PLATFORM_NVS_AREA);
//          }
          if (IsAmlOpRegionObject (DsdtPointer)) {
            SetOpRegion (DsdtPointer, mPlatformNvsAreaProtocol.Area, sizeof (PLATFORM_NVS_AREA));
          }
          break;
        case (SIGNATURE_32 ('O', 'G', 'N', 'S')):
          if (IsAmlOpRegionObject (DsdtPointer)) {
            SetOpRegion (DsdtPointer, mPlatformNvsAreaProtocol.OemArea, sizeof (OEM_PLATFORM_NVS_AREA));
          }
          break;


        case (SIGNATURE_32 ('M', 'D', 'B', 'G')):
          if (*(UINT8*) (DsdtPointer + 5) == 0x0C ){                    // Check if it's OperationRegion(\MDBG,SystemMemory,0x55AA55AA, 0x55AA55AA) in MemDbg.asl
            mSystemConfiguration.AcpiDebugAddress = *(UINT32*) (DsdtPointer + 6); // Get offset value of OperationRegion(\MDBG,SystemMemory,0x55AA55AA, 0x55AA55AA)
            {
              UINTN     VarDataSize;
              VarDataSize = sizeof(SETUP_DATA);
              Status = gRT->SetVariable (
                              L"Setup",
                              &gSetupVariableGuid,
                              EFI_VARIABLE_NON_VOLATILE | EFI_VARIABLE_BOOTSERVICE_ACCESS | EFI_VARIABLE_RUNTIME_ACCESS,
                              VarDataSize,
                              &mSystemConfiguration
                              );
            }
//[-end-190620-IB11270237-modify]//
          }
          break;

        //
        // _AC0 method
        //
        case (SIGNATURE_32 ('_', 'A', 'C', '0')):
          //
          // Conditional match.  _AC0 is >63 and <4095 bytes, so the package length is 2 bytes.
          // Therefore, subtract 3 to check the Operator.
          //
          Operation = DsdtPointer - 3;
          if (*Operation == AML_METHOD_OP) {
            //
            // Check if we want _AC0 enabled
            //
            if (mSystemConfiguration.DisableActiveTripPoints == 0) {
              Signature   = (UINT32 *) DsdtPointer;
              *Signature  = SIGNATURE_32 ('X', 'A', 'C', '0');
            }
          }
          break;

        //
        // _AL0 method
        //
        case (SIGNATURE_32 ('_', 'A', 'L', '0')):
          //
          // Conditional match.  For Name Objects, the Operator will always be the byte
          // immediately before the specific name. Therefore, subtract 1 to check the
          // Operator.
          //
          Operation = DsdtPointer - 1;
          if (*Operation == AML_NAME_OP) {

            //
            // Check if we want _AL0 enabled
            //
            if (mSystemConfiguration.DisableActiveTripPoints == 0) {
              Signature   = (UINT32 *) DsdtPointer;
              *Signature  = SIGNATURE_32 ('X', 'A', 'L', '0');
            }
          }
          break;

        //
        // _AC1 method
        //
        case (SIGNATURE_32 ('_', 'A', 'C', '1')):
          //
          // Conditional match.  _AC1 is < 63 bytes, so the package length is 1 byte.
          // Therefore, subtract 2 to check the Operator.
          //
          Operation = DsdtPointer - 2;
          if (*Operation == AML_METHOD_OP) {

            //
            // Check if we want _AC1 enabled
            //
            if (mSystemConfiguration.DisableActiveTripPoints == 0) {
              Signature   = (UINT32 *) DsdtPointer;
              *Signature  = SIGNATURE_32 ('X', 'A', 'C', '1');
            }
          }
          break;

        //
        // _AL1 method
        //
        case (SIGNATURE_32 ('_', 'A', 'L', '1')):
          //
          // Conditional match.  For Name Objects, the Operator will always be the byte
          // immediately before the specific name. Therefore, subtract 1 to check the
          // Operator.
          //
          Operation = DsdtPointer - 1;
          if (*Operation == AML_NAME_OP) {

            //
            // Check if we want _AL1 enabled
            //
            if (mSystemConfiguration.DisableActiveTripPoints == 0) {
              Signature   = (UINT32 *) DsdtPointer;
              *Signature  = SIGNATURE_32 ('X', 'A', 'L', '1');
            }
          }
          break;

        //
        // _AC2 method
        //
        case (SIGNATURE_32 ('_', 'A', 'C', '2')):
          //
          // Conditional match.  _AC2 is < 63 bytes, so the package length is 1 byte.
          // Therefore, subtract 2 to check the Operator.
          //
          Operation = DsdtPointer - 2;
          if (*Operation == AML_METHOD_OP) {

            //
            // Check if we want _AC2 enabled
            //
            if (mSystemConfiguration.DisableActiveTripPoints == 0) {
              Signature   = (UINT32 *) DsdtPointer;
              *Signature  = SIGNATURE_32 ('X', 'A', 'C', '2');
            }
          }
          break;

        //
        // _AL2 method
        //
        case (SIGNATURE_32 ('_', 'A', 'L', '2')):
          //
          // Conditional match.  For Name Objects, the Operator will always be the byte
          // immediately before the specific name. Therefore, subtract 1 to check the
          // Operator.
          //
          Operation = DsdtPointer - 1;
          if (*Operation == AML_NAME_OP) {

            //
            // Check if we want _AL2 enabled
            //
            if (mSystemConfiguration.DisableActiveTripPoints == 0) {
              Signature   = (UINT32 *) DsdtPointer;
              *Signature  = SIGNATURE_32 ('X', 'A', 'L', '2');
            }
          }
          break;

        //
        // _AC3 method
        //
        case (SIGNATURE_32 ('_', 'A', 'C', '3')):
          //
          // Conditional match.  _AC3 is < 63 bytes, so the package length is 1 byte.
          // Therefore, subtract 2 to check the Operator.
          //
          Operation = DsdtPointer - 2;
          if (*Operation == AML_METHOD_OP) {

            //
            // Check if we want _AC3 enabled
            //
            if (mSystemConfiguration.DisableActiveTripPoints == 0) {
              Signature   = (UINT32 *) DsdtPointer;
              *Signature  = SIGNATURE_32 ('X', 'A', 'C', '3');
            }
          }
          break;

        //
        // _AL3 method
        //
        case (SIGNATURE_32 ('_', 'A', 'L', '3')):
          //
          // Conditional match.  For Name Objects, the Operator will always be the byte
          // immediately before the specific name. Therefore, subtract 1 to check the
          // Operator.
          //
          Operation = DsdtPointer - 1;
          if (*Operation == AML_NAME_OP) {

            //
            // Check if we want _AL3 enabled
            //
            if (mSystemConfiguration.DisableActiveTripPoints == 0) {
              Signature   = (UINT32 *) DsdtPointer;
              *Signature  = SIGNATURE_32 ('X', 'A', 'L', '3');
            }
          }
          break;

        //
        // _AC4 method
        //
        case (SIGNATURE_32 ('_', 'A', 'C', '4')):
          //
          // Conditional match.  _AC4 is < 63 bytes, so the package length is 1 byte.
          // Therefore, subtract 2 to check the Operator.
          //
          Operation = DsdtPointer - 2;
          if (*Operation == AML_METHOD_OP) {

            //
            // Check if we want _AC4 enabled
            //
            if (mSystemConfiguration.DisableActiveTripPoints == 0) {
              Signature   = (UINT32 *) DsdtPointer;
              *Signature  = SIGNATURE_32 ('X', 'A', 'C', '4');
            }
          }
          break;

        //
        // _AL4 method
        //
        case (SIGNATURE_32 ('_', 'A', 'L', '4')):
          //
          // Conditional match.  For Name Objects, the Operator will always be the byte
          // immediately before the specific name. Therefore, subtract 1 to check the
          // Operator.
          //
          Operation = DsdtPointer - 1;
          if (*Operation == AML_NAME_OP) {

            //
            // Check if we want _AL4 enabled
            //
            if (mSystemConfiguration.DisableActiveTripPoints == 0) {
              Signature   = (UINT32 *) DsdtPointer;
              *Signature  = SIGNATURE_32 ('X', 'A', 'L', '4');
            }
          }
          break;

        //
        // _AC5 method
        //
        case (SIGNATURE_32 ('_', 'A', 'C', '5')):
          //
          // Conditional match.  _AC5 is < 63 bytes, so the package length is 1 byte.
          // Therefore, subtract 2 to check the Operator.
          //
          Operation = DsdtPointer - 2;
          if (*Operation == AML_METHOD_OP) {

            //
            // Check if we want _AC5 enabled
            //
            if (mSystemConfiguration.DisableActiveTripPoints == 0) {
              Signature   = (UINT32 *) DsdtPointer;
              *Signature  = SIGNATURE_32 ('X', 'A', 'C', '5');
            }
          }
          break;

        //
        // _AL5 method
        //
        case (SIGNATURE_32 ('_', 'A', 'L', '5')):
          //
          // Conditional match.  For Name Objects, the Operator will always be the byte
          // immediately before the specific name. Therefore, subtract 1 to check the
          // Operator.
          //
          Operation = DsdtPointer - 1;
          if (*Operation == AML_NAME_OP) {

            //
            // Check if we want _AL5 enabled
            //
            if (mSystemConfiguration.DisableActiveTripPoints == 0) {
              Signature   = (UINT32 *) DsdtPointer;
              *Signature  = SIGNATURE_32 ('X', 'A', 'L', '5');
            }
          }
          break;

        //
        // _AC6 method
        //
        case (SIGNATURE_32 ('_', 'A', 'C', '6')):
          //
          // Conditional match.  _AC6 is < 63 bytes, so the package length is 1 byte.
          // Therefore, subtract 2 to check the Operator.
          //
          Operation = DsdtPointer - 2;
          if (*Operation == AML_METHOD_OP) {

            //
            // Check if we want _AC6 enabled
            //
            if (mSystemConfiguration.DisableActiveTripPoints == 0) {
              Signature   = (UINT32 *) DsdtPointer;
              *Signature  = SIGNATURE_32 ('X', 'A', 'C', '6');
            }
          }
          break;

        //
        // _AL6 method
        //
        case (SIGNATURE_32 ('_', 'A', 'L', '6')):
          //
          // Conditional match.  For Name Objects, the Operator will always be the byte
          // immediately before the specific name. Therefore, subtract 1 to check the
          // Operator.
          //
          Operation = DsdtPointer - 1;
          if (*Operation == AML_NAME_OP) {

            //
            // Check if we want _AL6 enabled
            //
            if (mSystemConfiguration.DisableActiveTripPoints == 0) {
              Signature   = (UINT32 *) DsdtPointer;
              *Signature  = SIGNATURE_32 ('X', 'A', 'L', '6');
            }
          }
          break;

        //
        // _AC7 method
        //
        case (SIGNATURE_32 ('_', 'A', 'C', '7')):
          //
          // Conditional match.  _AC7 is < 63 bytes, so the package length is 1 byte.
          // Therefore, subtract 2 to check the Operator.
          //
          Operation = DsdtPointer - 2;
          if (*Operation == AML_METHOD_OP) {

            //
            // Check if we want _AC7 enabled
            //
            if (mSystemConfiguration.DisableActiveTripPoints == 0) {
              Signature   = (UINT32 *) DsdtPointer;
              *Signature  = SIGNATURE_32 ('X', 'A', 'C', '7');
            }
          }
          break;

        //
        // _AL7 method
        //
        case (SIGNATURE_32 ('_', 'A', 'L', '7')):
          //
          // Conditional match.  For Name Objects, the Operator will always be the byte
          // immediately before the specific name. Therefore, subtract 1 to check the
          // Operator.
          //
          Operation = DsdtPointer - 1;
          if (*Operation == AML_NAME_OP) {

            //
            // Check if we want _AL7 enabled
            //
            if (mSystemConfiguration.DisableActiveTripPoints == 0) {
              Signature   = (UINT32 *) DsdtPointer;
              *Signature  = SIGNATURE_32 ('X', 'A', 'L', '7');
            }
          }
          break;

        //
        // _AC8 method
        //
        case (SIGNATURE_32 ('_', 'A', 'C', '8')):
          //
          // Conditional match.  _AC8 is < 63 bytes, so the package length is 1 byte.
          // Therefore, subtract 2 to check the Operator.
          //
          Operation = DsdtPointer - 2;
          if (*Operation == AML_METHOD_OP) {

            //
            // Check if we want _AC8 enabled
            //
            if (mSystemConfiguration.DisableActiveTripPoints == 0) {
              Signature   = (UINT32 *) DsdtPointer;
              *Signature  = SIGNATURE_32 ('X', 'A', 'C', '8');
            }
          }
          break;

        //
        // _AL8 method
        //
        case (SIGNATURE_32 ('_', 'A', 'L', '8')):
          //
          // Conditional match.  For Name Objects, the Operator will always be the byte
          // immediately before the specific name. Therefore, subtract 1 to check the
          // Operator.
          //
          Operation = DsdtPointer - 1;
          if (*Operation == AML_NAME_OP) {

            //
            // Check if we want _AL8 enabled
            //
            if (mSystemConfiguration.DisableActiveTripPoints == 0) {
              Signature   = (UINT32 *) DsdtPointer;
              *Signature  = SIGNATURE_32 ('X', 'A', 'L', '8');
            }
          }
          break;

        //
        // _AC9 method
        //
        case (SIGNATURE_32 ('_', 'A', 'C', '9')):
          //
          // Conditional match.  _AC9 is < 63 bytes, so the package length is 1 byte.
          // Therefore, subtract 2 to check the Operator.
          //
          Operation = DsdtPointer - 2;
          if (*Operation == AML_METHOD_OP) {

            //
            // Check if we want _AC9 enabled
            //
            if (mSystemConfiguration.DisableActiveTripPoints == 0) {
              Signature   = (UINT32 *) DsdtPointer;
              *Signature  = SIGNATURE_32 ('X', 'A', 'C', '9');
            }
          }
          break;

        //
        // _AL9 method
        //
        case (SIGNATURE_32 ('_', 'A', 'L', '9')):
          //
          // Conditional match.  For Name Objects, the Operator will always be the byte
          // immediately before the specific name. Therefore, subtract 1 to check the
          // Operator.
          //
          Operation = DsdtPointer - 1;
          if (*Operation == AML_NAME_OP) {

            //
            // Check if we want _AL9 enabled
            //
            if (mSystemConfiguration.DisableActiveTripPoints == 0) {
              Signature   = (UINT32 *) DsdtPointer;
              *Signature  = SIGNATURE_32 ('X', 'A', 'L', '9');
            }
          }
          break;

        //
        // _PSL method
        //
        case (SIGNATURE_32 ('_', 'P', 'S', 'L')):
          //
          // Conditional match.  _PSL is < 256 bytes, so the package length is 1 byte.
          // Therefore, subtract 2 to check the Operator.
          //
          Operation = DsdtPointer - 3;
          if (*Operation == AML_METHOD_OP) {

            //
            // Check if we want _PSL enabled
            //
            if (mSystemConfiguration.DisablePassiveTripPoints == 0) {
              Signature   = (UINT32 *) DsdtPointer;
              *Signature  = SIGNATURE_32 ('X', 'P', 'S', 'L');
            }
          }
          break;

        //
        // _PSV method
        //
        case (SIGNATURE_32 ('_', 'P', 'S', 'V')):
          //
          // Conditional match.  _PSV is < 256 bytes, so the package length is 1 byte.
          // Therefore, subtract 2 to check the Operator.
          //
          Operation = DsdtPointer - 3;
          if (*Operation == AML_METHOD_OP) {

            //
            // Check if we want _PSV enabled
            //
            if (mSystemConfiguration.DisablePassiveTripPoints == 0) {
              Signature   = (UINT32 *) DsdtPointer;
              *Signature  = SIGNATURE_32 ('X', 'P', 'S', 'V');
            }
          }
          break;

        //
        // _CRT method
        //
        case (SIGNATURE_32 ('_', 'C', 'R', 'T')):
          //
          // Conditional match.  _CRT is < 256 bytes, so the package length is 1 byte.
          // Subtract 3 to check the Operator for CRB, subract 2 for Harris Beach.
          //
          Operation = DsdtPointer - 3;
          if (*Operation == AML_METHOD_OP) {

            //
            // Check if we want _CRT enabled
            //
            if (mSystemConfiguration.DisableCriticalTripPoints == 0) {
              Signature   = (UINT32 *) DsdtPointer;
              *Signature  = SIGNATURE_32 ('X', 'C', 'R', 'T');
            }
          }
          break;

        //
        // _TC1 method
        //
        case (SIGNATURE_32 ('_', 'T', 'C', '1')):
          //
          // Conditional match.  _TC1 is < 256 bytes, so the package length is 1 byte.
          // Therefore, subtract 2 to check the Operator.
          //
          Operation = DsdtPointer - 2;
          if (*Operation == AML_METHOD_OP) {

            //
            // Check if we want _TC1 enabled
            //
            if (mSystemConfiguration.DisablePassiveTripPoints == 0) {
              Signature   = (UINT32 *) DsdtPointer;
              *Signature  = SIGNATURE_32 ('X', 'T', 'C', '1');
            }
          }
          break;

        //
        // _TC2 method
        //
        case (SIGNATURE_32 ('_', 'T', 'C', '2')):
          //
          // Conditional match.  _TC2 is < 256 bytes, so the package length is 1 byte.
          // Therefore, subtract 2 to check the Operator.
          //
          Operation = DsdtPointer - 2;
          if (*Operation == AML_METHOD_OP) {

            //
            // Check if we want _TC2 enabled
            //
            if (mSystemConfiguration.DisablePassiveTripPoints == 0) {
              Signature   = (UINT32 *) DsdtPointer;
              *Signature  = SIGNATURE_32 ('X', 'T', 'C', '2');
            }
          }
          break;

        //
        // _TSP method
        //
        case (SIGNATURE_32 ('_', 'T', 'S', 'P')):
          //
          // Conditional match.  _TSP is < 256 bytes, so the package length is 1 byte.
          // Therefore, subtract 2 to check the Operator.
          //
          Operation = DsdtPointer - 2;
          if (*Operation == AML_METHOD_OP) {

            //
            // Check if we want _TSP enabled
            //
            if (mSystemConfiguration.DisablePassiveTripPoints == 0) {
              Signature   = (UINT32 *) DsdtPointer;
              *Signature  = SIGNATURE_32 ('X', 'T', 'S', 'P');
            }
          }
          break;

        //
        // Update SS3 Name with Setup value
        //
        case (SIGNATURE_32 ('S', 'S', '3', '_')):
          Operation = DsdtPointer - 1;
          if (*Operation == AML_NAME_OP) {
            Value   = (UINT8 *) DsdtPointer + 4;
//[-start-190620-IB11270237-modify]//
//            *Value  = (UINT8)mSystemConfiguration.AcpiSleepState;
            *Value  = (UINT8)mChipsetSetupData.DisableAcpiS3;
//[-end-190620-IB11270237-modify]//
          }
          break;
        //
        // Update SS4 Name with Setup value
        //
        case (SIGNATURE_32 ('S', 'S', '4', '_')):
          Operation = DsdtPointer - 1;
          if (*Operation == AML_NAME_OP) {
            Value   = (UINT8 *) DsdtPointer + 4;
            *Value  = mSystemConfiguration.AcpiHibernate;
          }
          break;
        //
        // _EJ0 method
        //
        case (SIGNATURE_32 ('_', 'E', 'J', '0')):
          if (mSystemConfiguration.LowPowerS0Idle) {
            //
            // Remove _EJ0 for SOC
            //
            if (*(DsdtPointer-3) == AML_METHOD_OP) {
              Signature   = (UINT32 *) DsdtPointer;
              *Signature  = SIGNATURE_32 ('X', 'E', 'J', '0');
              EnterDock = TRUE;
            }
          }
          break;
        //
        // _STA method for Device (\_SB.PC00.DOCK)
        //
        case (SIGNATURE_32 ('_', 'S', 'T', 'A')):
          if (mSystemConfiguration.LowPowerS0Idle) {
            //
            // Remove _STA in (\_SB.PC00.DOCK) for SOC
            //
            if ((*(DsdtPointer-3) == AML_METHOD_OP) && (EnterDock)) {
              Signature   = (UINT32 *) DsdtPointer;
              *Signature  = SIGNATURE_32 ('X', 'S', 'T', 'A');
              EnterDock = FALSE;
            }
          }
          break;
        //
        // _UPC method for Device (\_SB.PC00.XHCI.RHUB)
        //
        case (SIGNATURE_32('H', 'S', '1', '3')):
          for (TmpDsdtPointer = DsdtPointer;
               TmpDsdtPointer <= DsdtPointer + MaximumDsdtPointLength;
               TmpDsdtPointer++){
            Signature = (UINT32 *) TmpDsdtPointer;
            switch (*Signature) {
              case(SIGNATURE_32('U', 'P', 'C', 'P')):
                Value   = (UINT8 *)((UINT32 *)TmpDsdtPointer + 2);
                break;
              default:
                //
                // Do nothing.
                //
                break;
            }
          }
        break;


        //
        // _DCK method
        //
        case (SIGNATURE_32 ('_', 'D', 'C', 'K')):
          if (mSystemConfiguration.LowPowerS0Idle) {
            //
            // Remove _DCK for SOC
            //
            if (*(DsdtPointer-3) == AML_METHOD_OP) {
              Signature   = (UINT32 *) DsdtPointer;
              *Signature  = SIGNATURE_32 ('X', 'D', 'C', 'K');
            }
          }
          break;

        //
        // mask _DEP from CPU's scope if CS disabled.
        //
        case (SIGNATURE_32 ('P', 'R', '0', '0')):
        case (SIGNATURE_32 ('P', 'R', '0', '1')):
        case (SIGNATURE_32 ('P', 'R', '0', '2')):
        case (SIGNATURE_32 ('P', 'R', '0', '3')):
        case (SIGNATURE_32 ('P', 'R', '0', '4')):
        case (SIGNATURE_32 ('P', 'R', '0', '5')):
        case (SIGNATURE_32 ('P', 'R', '0', '6')):
        case (SIGNATURE_32 ('P', 'R', '0', '7')):
        case (SIGNATURE_32 ('P', 'R', '0', '8')):
        case (SIGNATURE_32 ('P', 'R', '0', '9')):
        case (SIGNATURE_32 ('P', 'R', '1', '0')):
        case (SIGNATURE_32 ('P', 'R', '1', '1')):
        case (SIGNATURE_32 ('P', 'R', '1', '2')):
        case (SIGNATURE_32 ('P', 'R', '1', '3')):
        case (SIGNATURE_32 ('P', 'R', '1', '4')):
        case (SIGNATURE_32 ('P', 'R', '1', '5')):
          if (mSystemConfiguration.LowPowerS0Idle == 0) {
            for (TmpDsdtPointer = DsdtPointer; TmpDsdtPointer <= DsdtPointer + MaximumDsdtPointLength; TmpDsdtPointer++){
              Signature = (UINT32 *) TmpDsdtPointer;
              switch (*Signature) {
                case(SIGNATURE_32('_', 'D', 'E', 'P')):
                  *(UINT8 *) TmpDsdtPointer = 'X';
                  break;
                default:
                  //
                  // Do nothing.
                  //
                  break;
              }
            }
          }
          break;

        //
        // _EDL name
        //
        case (SIGNATURE_32 ('_', 'E', 'D', 'L')):
          if (mSystemConfiguration.LowPowerS0Idle) {
            //
            // Remove _EDL for SOC
            //
            if (*(DsdtPointer-1) == AML_NAME_OP) {
              Signature   = (UINT32 *) DsdtPointer;
              *Signature  = SIGNATURE_32 ('X', 'E', 'D', 'L');
            }
          }
          break;

        default:
          //
          // Do nothing.
          //
          break;
        }
//[-start-190620-IB11270237-add]//
        Status = OemSvcUpdateDsdtAcpiTable (Signature);
//[-end-190620-IB11270237-modify]//
      }
//[-start-190620-IB11270237-modify]//
        DEBUG ((DEBUG_INFO, "Dxe OemChipsetServices Call: OemSvcUpdateDsdtAcpiTable \n"));
        DEBUG ((DEBUG_INFO, "Dxe OemChipsetServices OemSvcUpdateDsdtAcpiTable Status: %r\n", Status));
//[-add-190620-IB11270237-modify]//
    }
    break;

  case EFI_ACPI_3_0_HIGH_PRECISION_EVENT_TIMER_TABLE_SIGNATURE:
    //
    // If HPET is disabled in setup, don't publish the table.
    //
//[-start-190620-IB11270237-modify]//
//    if (mPchSetup.Hpet == FALSE) {
//      *Version = EFI_ACPI_TABLE_VERSION_NONE;
//    } else {
      //
      // Get HPET base address
      //
      PchHpetBaseGet (&HpetBaseAdress);
      //
      // Adjust HPET Table to correct the Base Address
      //
      ((EFI_ACPI_HIGH_PRECISION_EVENT_TIMER_TABLE_HEADER*) Table)->BaseAddressLower32Bit.Address = HpetBaseAdress;
//    }
//[-end-190620-IB11270237-modify]//
    break;

  case EFI_ACPI_3_0_PCI_EXPRESS_MEMORY_MAPPED_CONFIGURATION_SPACE_BASE_ADDRESS_DESCRIPTION_TABLE_SIGNATURE:
    //
    // Update MCFG base and end bus number
    //
    ((EFI_ACPI_MEMORY_MAPPED_CONFIGURATION_SPACE_ACCESS_DESCRIPTION_TABLE *) Table)->AllocationStructure[0].BaseAddress
      = PcdGet64 (PcdPciExpressBaseAddress);

    ((EFI_ACPI_MEMORY_MAPPED_CONFIGURATION_SPACE_ACCESS_DESCRIPTION_TABLE *) Table)->AllocationStructure[0].EndBusNumber
      = (UINT8)(PcdGet32 (PcdPciExpressRegionLength) / 0x100000) - 1;

    if ((((EFI_ACPI_MEMORY_MAPPED_CONFIGURATION_SPACE_ACCESS_DESCRIPTION_TABLE *) Table)->AllocationStructure[0].EndBusNumber > MAX_OS_VISIBLE_BUSES_WITH_VMD)
        && (SaSetup.VmdEnable == 1)) {
      ((EFI_ACPI_MEMORY_MAPPED_CONFIGURATION_SPACE_ACCESS_DESCRIPTION_TABLE *) Table)->AllocationStructure[0].EndBusNumber
        = MAX_OS_VISIBLE_BUSES_WITH_VMD;
      DEBUG((DEBUG_INFO,"VMD is enabled so restricting OS visible bus numbers to %d\n", MAX_OS_VISIBLE_BUSES_WITH_VMD));
    }

#if FixedPcdGetBool(PcdITbtEnable) == 1
    if (mTcssHob->TcssData.PcieMultipleSegmentEnabled) {
      ((EFI_ACPI_MEMORY_MAPPED_CONFIGURATION_SPACE_ACCESS_DESCRIPTION_TABLE *) Table)->AllocationStructure[1].BaseAddress
      = PcdGet64 (PcdPciExpressBaseAddress) + SIZE_256MB;
    } else
#endif
    {
      ((EFI_ACPI_MEMORY_MAPPED_CONFIGURATION_SPACE_ACCESS_DESCRIPTION_TABLE *) Table)->Header.Length -= sizeof (EFI_ACPI_MEMORY_MAPPED_ENHANCED_CONFIGURATION_SPACE_BASE_ADDRESS_ALLOCATION_STRUCTURE);
    }
    break;

  case EFI_ACPI_5_0_EMBEDDED_CONTROLLER_BOOT_RESOURCES_TABLE_SIGNATURE:
    //
    // If EC is not present, don't publish the table.
    //
    if (mPlatformNvsAreaProtocol.Area->EcAvailable == PLATFORM_NVS_DEVICE_DISABLE) {
      *Version = EFI_ACPI_TABLE_VERSION_NONE;
    } else {
      //
      // Update GPE Number
      //
      ((EFI_ACPI_5_0_EMBEDDED_CONTROLLER_BOOT_RESOURCES_TABLE *) Table)->GpeBit = (UINT8) GetEcGpeNumber ();
    }
    break;

/* @todo Need to uncomment once MSFT fully implements uPEP
  case SIGNATURE_32 ('L', 'P', 'I', 'T'):
    //
    // If L0s is disabled in setup, don't publish the table.
    //
    if (mSystemConfiguration.LowPowerS0Idle == 0) {
      *Version = EFI_ACPI_TABLE_VERSION_NONE;
    }
    break;
*/
  case EFI_ACPI_1_0_SECONDARY_SYSTEM_DESCRIPTION_TABLE_SIGNATURE:
  {
    UINT8   *TablePointer;
    UINT32  *Signature;
    UINT8   *Operation;

    //
    // Do not load the xHCI table. It is handled by separate function.
    //
    if (CompareMem (&TableHeader->OemTableId, "xh_", 3) == 0) {
      DEBUG((DEBUG_INFO,"TableHeader->OemTableId = %x\n ", TableHeader->OemTableId));
      *Version = EFI_ACPI_TABLE_VERSION_NONE;
    }

    //
    // Load SSDT tables for the platforms based on boardID; default to RVP tables.
    // Load Ther_Rvp for all others.
    //
    if (CompareMem (&TableHeader->OemTableId, "Ther_Rvp", 8) == 0) {
      CurrPtr = (UINT8 *) Table;
      for (TablePointer = CurrPtr;TablePointer < (CurrPtr + ((EFI_ACPI_COMMON_HEADER *) CurrPtr)->Length - sizeof (UINT32));TablePointer++)
      {
        Signature = (UINT32 *) TablePointer;
        switch (*Signature) {
        //
        // _AC0 method
        //
        case (SIGNATURE_32 ('_', 'A', 'C', '0')):
          //
          // Conditional match.  _AC0 is >63 and <4095 bytes, so the package length is 2 bytes.
          // Therefore, subtract 3 to check the Operator.
          //
          Operation = TablePointer - 3;
          if (*Operation == AML_METHOD_OP) {
            //
            // Check if we want _AC0 enabled
            //
            if (mSystemConfiguration.DisableActiveTripPoints == 0) {
              Signature   = (UINT32 *) TablePointer;
              *Signature  = SIGNATURE_32 ('X', 'A', 'C', '0');
            }
          }
          break;
        //
        // _AL0 method
        //
        case (SIGNATURE_32 ('_', 'A', 'L', '0')):
          //
          // Conditional match.  For Name Objects, the Operator will always be the byte
          // immediately before the specific name. Therefore, subtract 1 to check the
          // Operator.
          //
          Operation = TablePointer - 1;
          if (*Operation == AML_NAME_OP) {
            //
            // Check if we want _AL0 enabled
            //
            if (mSystemConfiguration.DisableActiveTripPoints == 0) {
              Signature   = (UINT32 *) TablePointer;
              *Signature  = SIGNATURE_32 ('X', 'A', 'L', '0');
            }
          }
          break;
        //
        // _AC1 method
        //
        case (SIGNATURE_32 ('_', 'A', 'C', '1')):
          //
          // Conditional match.  _AC1 is < 63 bytes, so the package length is 1 byte.
          // Therefore, subtract 2 to check the Operator.
          //
          Operation = TablePointer - 2;
          if (*Operation == AML_METHOD_OP) {
            //
            // Check if we want _AC1 enabled
            //
            if (mSystemConfiguration.DisableActiveTripPoints == 0) {
              Signature   = (UINT32 *) TablePointer;
              *Signature  = SIGNATURE_32 ('X', 'A', 'C', '1');
            }
          }
          break;
        //
        // _AL1 method
        //
        case (SIGNATURE_32 ('_', 'A', 'L', '1')):
          //
          // Conditional match.  For Name Objects, the Operator will always be the byte
          // immediately before the specific name. Therefore, subtract 1 to check the
          // Operator.
          //
          Operation = TablePointer - 1;
          if (*Operation == AML_NAME_OP) {
            //
            // Check if we want _AL1 enabled
            //
            if (mSystemConfiguration.DisableActiveTripPoints == 0) {
              Signature   = (UINT32 *) TablePointer;
              *Signature  = SIGNATURE_32 ('X', 'A', 'L', '1');
            }
          }
          break;
        //
        // _AC2 method
        //
        case (SIGNATURE_32 ('_', 'A', 'C', '2')):
          //
          // Conditional match.  _AC2 is < 63 bytes, so the package length is 1 byte.
          // Therefore, subtract 2 to check the Operator.
          //
          Operation = TablePointer - 2;
          if (*Operation == AML_METHOD_OP) {
            //
            // Check if we want _AC2 enabled
            //
            if (mSystemConfiguration.DisableActiveTripPoints == 0) {
              Signature   = (UINT32 *) TablePointer;
              *Signature  = SIGNATURE_32 ('X', 'A', 'C', '2');
            }
          }
          break;
        //
        // _AL2 method
        //
        case (SIGNATURE_32 ('_', 'A', 'L', '2')):
          //
          // Conditional match.  For Name Objects, the Operator will always be the byte
          // immediately before the specific name. Therefore, subtract 1 to check the
          // Operator.
          //
          Operation = TablePointer - 1;
          if (*Operation == AML_NAME_OP) {
            //
            // Check if we want _AL2 enabled
            //
            if (mSystemConfiguration.DisableActiveTripPoints == 0) {
              Signature   = (UINT32 *) TablePointer;
              *Signature  = SIGNATURE_32 ('X', 'A', 'L', '2');
            }
          }
          break;
        //
        // _AC3 method
        //
        case (SIGNATURE_32 ('_', 'A', 'C', '3')):
          //
          // Conditional match.  _AC3 is < 63 bytes, so the package length is 1 byte.
          // Therefore, subtract 2 to check the Operator.
          //
          Operation = TablePointer - 2;
          if (*Operation == AML_METHOD_OP) {
            //
            // Check if we want _AC3 enabled
            //
            if (mSystemConfiguration.DisableActiveTripPoints == 0) {
              Signature   = (UINT32 *) TablePointer;
              *Signature  = SIGNATURE_32 ('X', 'A', 'C', '3');
            }
          }
          break;
        //
        // _AL3 method
        //
        case (SIGNATURE_32 ('_', 'A', 'L', '3')):
          //
          // Conditional match.  For Name Objects, the Operator will always be the byte
          // immediately before the specific name. Therefore, subtract 1 to check the
          // Operator.
          //
          Operation = TablePointer - 1;
          if (*Operation == AML_NAME_OP) {
            //
            // Check if we want _AL3 enabled
            //
            if (mSystemConfiguration.DisableActiveTripPoints == 0) {
              Signature   = (UINT32 *) TablePointer;
              *Signature  = SIGNATURE_32 ('X', 'A', 'L', '3');
            }
          }
          break;
        //
        // _AC4 method
        //
        case (SIGNATURE_32 ('_', 'A', 'C', '4')):
          //
          // Conditional match.  _AC4 is < 63 bytes, so the package length is 1 byte.
          // Therefore, subtract 2 to check the Operator.
          //
          Operation = TablePointer - 2;
          if (*Operation == AML_METHOD_OP) {
            //
            // Check if we want _AC4 enabled
            //
            if (mSystemConfiguration.DisableActiveTripPoints == 0) {
              Signature   = (UINT32 *) TablePointer;
              *Signature  = SIGNATURE_32 ('X', 'A', 'C', '4');
            }
          }
          break;
        //
        // _AL4 method
        //
        case (SIGNATURE_32 ('_', 'A', 'L', '4')):
          //
          // Conditional match.  For Name Objects, the Operator will always be the byte
          // immediately before the specific name. Therefore, subtract 1 to check the
          // Operator.
          //
          Operation = TablePointer - 1;
          if (*Operation == AML_NAME_OP) {
            //
            // Check if we want _AL4 enabled
            //
            if (mSystemConfiguration.DisableActiveTripPoints == 0) {
              Signature   = (UINT32 *) TablePointer;
              *Signature  = SIGNATURE_32 ('X', 'A', 'L', '4');
            }
          }
          break;
        // _PSL method
        //
        case (SIGNATURE_32 ('_', 'P', 'S', 'L')):
          //
          // Conditional match.  _PSL is < 256 bytes, so the package length is 1 byte.
          // Therefore, subtract 2 to check the Operator.
          //
          Operation = TablePointer - 3;
          if (*Operation == AML_METHOD_OP) {
            //
            // Check if we want _PSL enabled
            //
            if (mSystemConfiguration.DisablePassiveTripPoints == 0) {
              Signature   = (UINT32 *) TablePointer;
              *Signature  = SIGNATURE_32 ('X', 'P', 'S', 'L');
            }
          }
          break;
        //
        // _PSV method
        //
        case (SIGNATURE_32 ('_', 'P', 'S', 'V')):
          //
          // Conditional match.  _PSV is < 256 bytes, so the package length is 1 byte.
          // Therefore, subtract 2 to check the Operator.
          //
          Operation = TablePointer - 3;
          if (*Operation == AML_METHOD_OP) {
            //
            // Check if we want _PSV enabled
            //
            if (mSystemConfiguration.DisablePassiveTripPoints == 0) {
              Signature   = (UINT32 *) TablePointer;
              *Signature  = SIGNATURE_32 ('X', 'P', 'S', 'V');
            }
          }
          break;
        //
        // _CRT method
        //
        case (SIGNATURE_32 ('_', 'C', 'R', 'T')):
          //
          // Conditional match.  _CRT is < 256 bytes, so the package length is 1 byte.
          // Subtract 3 to check the Operator for CRB, subract 2 for Harris Beach.
          //
          Operation = TablePointer - 3;
          if (*Operation == AML_METHOD_OP) {
            //
            // Check if we want _CRT enabled
            //
            if (mSystemConfiguration.DisableCriticalTripPoints == 0) {
              Signature   = (UINT32 *) TablePointer;
              *Signature  = SIGNATURE_32 ('X', 'C', 'R', 'T');
            }
          }
          break;
        //
        // _TC1 method
        //
        case (SIGNATURE_32 ('_', 'T', 'C', '1')):
          //
          // Conditional match.  _TC1 is < 256 bytes, so the package length is 1 byte.
          // Therefore, subtract 2 to check the Operator.
          //
          Operation = TablePointer - 2;
          if (*Operation == AML_METHOD_OP) {
            //
            // Check if we want _TC1 enabled
            //
            if (mSystemConfiguration.DisablePassiveTripPoints == 0) {
              Signature   = (UINT32 *) TablePointer;
              *Signature  = SIGNATURE_32 ('X', 'T', 'C', '1');
            }
          }
          break;
        //
        // _TC2 method
        //
        case (SIGNATURE_32 ('_', 'T', 'C', '2')):
          //
          // Conditional match.  _TC2 is < 256 bytes, so the package length is 1 byte.
          // Therefore, subtract 2 to check the Operator.
          //
          Operation = TablePointer - 2;
          if (*Operation == AML_METHOD_OP) {
            //
            // Check if we want _TC2 enabled
            //
            if (mSystemConfiguration.DisablePassiveTripPoints == 0) {
              Signature   = (UINT32 *) TablePointer;
              *Signature  = SIGNATURE_32 ('X', 'T', 'C', '2');
            }
          }
          break;
        //
        // _TSP method
        //
        case (SIGNATURE_32 ('_', 'T', 'S', 'P')):
          //
          // Conditional match.  _TSP is < 256 bytes, so the package length is 1 byte.
          // Therefore, subtract 2 to check the Operator.
          //
          Operation = TablePointer - 2;
          if (*Operation == AML_METHOD_OP) {
            //
            // Check if we want _TSP enabled
            //
            if (mSystemConfiguration.DisablePassiveTripPoints == 0) {
              Signature   = (UINT32 *) TablePointer;
              *Signature  = SIGNATURE_32 ('X', 'T', 'S', 'P');
            }
          }
          break;
        }
      }
    }

    //
    // Load RTD3 SSDT table for ADL RVP SKUs
    // Note: "OemId" field is used to indentify whether SSDT table is for RTD3 usage
    //
    if ((CompareMem (&TableHeader->OemId,  "Rtd3", 4) == 0)) {
      //
      // OemId should be replaced with PcdAcpiDefaultOemId
      //
      CopyMem (TableHeader->OemId, PcdGetPtr (PcdAcpiDefaultOemId), sizeof (TableHeader->OemId));
      //
      // set default = none
      //
      *Version = EFI_ACPI_TABLE_VERSION_NONE;

      //
      // check RTD3 enabled in setup
      //
      if (mSystemConfiguration.Rtd3Support == 1) {
        //
        // match table ID and BoardId
        //

        if (TableHeader->OemTableId == PcdGet64 (PcdBoardRtd3TableSignature)) {
          DEBUG ((DEBUG_INFO, "Board SsdtRtd3 Table: %x\n", TableHeader->OemTableId));
          *Version = EFI_ACPI_TABLE_VERSION_1_0B | EFI_ACPI_TABLE_VERSION_2_0 | EFI_ACPI_TABLE_VERSION_3_0;
        }
      } // Rtd3Support
    } // Load RTD3 SSDT table
  }
    break;

  default:
    break;
  }
  return EFI_SUCCESS;
}

/**
  This function calculates RCR based on PCI Device ID and Vendor ID from the devices
  available on the platform.
  It also includes other instances of BIOS change to calculate CRC and provides as
  HWSignature filed in FADT table.
**/
VOID
IsHardwareChange (
  VOID
  )
{
  EFI_STATUS                    Status;
  UINTN                         Index;
  UINTN                         HandleCount;
  EFI_HANDLE                    *HandleBuffer;
  EFI_PCI_IO_PROTOCOL           *PciIo;
  UINT32                        CRC;
  UINT32                        *HWChange;
  UINTN                         HWChangeSize;
  UINT32                        PciId;
  UINTN                         Handle;
  EFI_ACPI_2_0_FIRMWARE_ACPI_CONTROL_STRUCTURE *FacsPtr;
  EFI_ACPI_6_3_FIXED_ACPI_DESCRIPTION_TABLE    *pFADT;

  HandleCount  = 0;
  HandleBuffer = NULL;

  Status = gBS->LocateHandleBuffer (
                  ByProtocol,
                  &gEfiPciIoProtocolGuid,
                  NULL,
                  &HandleCount,
                  &HandleBuffer
                  );
  if (EFI_ERROR (Status)) {
    return; // PciIO protocol not installed yet!
  }

  if (HandleCount > 0x1FFF) { // PCI config space max limit
    return;
  }

  //
  // Allocate memory for HWChange and add additional entrie for
  // pFADT->XDsdt
  //
  HWChangeSize = HandleCount + 1;
  HWChange = AllocateZeroPool( sizeof(UINT32) * HWChangeSize );
  ASSERT( HWChange != NULL );

  if (HWChange == NULL) return;

  //
  // add HWChange inputs: PCI devices
  //
  for (Index = 0; HandleCount > 0; HandleCount--) {
    PciId = 0;
    Status = gBS->HandleProtocol (HandleBuffer[Index], &gEfiPciIoProtocolGuid, (VOID **) &PciIo);
    if (!EFI_ERROR (Status)) {
      Status = PciIo->Pci.Read (PciIo, EfiPciIoWidthUint32, 0, 1, &PciId);
      if (EFI_ERROR (Status)) {
        continue;
      }
      HWChange[Index++] = PciId;
    }
  }

  //
  // Locate FACP Table
  //
  Handle = 0;
  Status = LocateAcpiTableBySignature (
              EFI_ACPI_6_3_FIXED_ACPI_DESCRIPTION_TABLE_SIGNATURE,
              (EFI_ACPI_DESCRIPTION_HEADER **) &pFADT,
              &Handle
              );
  if (EFI_ERROR (Status) || (pFADT == NULL)) {
    return;  //Table not found or out of memory resource for pFADT table
  }

  //
  // add HWChange inputs: others
  //
  HWChange[Index++] = (UINT32)pFADT->XDsdt;

  //
  // Calculate CRC value with HWChange data.
  //
  Status = gBS->CalculateCrc32(HWChange, sizeof(UINT32) * HWChangeSize, &CRC);
  DEBUG((DEBUG_INFO,"CRC = %x and Status = %r\n", CRC, Status));

  //
  // Set HardwareSignature value based on CRC value.
  //
  FacsPtr = (EFI_ACPI_2_0_FIRMWARE_ACPI_CONTROL_STRUCTURE *)(UINTN)pFADT->FirmwareCtrl;
  FacsPtr->HardwareSignature = CRC;
  FreePool( HWChange );
}
//[-start-190620-IB11270237-remove]//
//EFI_STATUS
//PublishAcpiTablesFromFv (
//  IN EFI_GUID gEfiAcpiMultiTableStorageGuid
//  )
//{
//  EFI_STATUS                    Status;
//  EFI_FIRMWARE_VOLUME2_PROTOCOL *FwVol;
//  EFI_ACPI_COMMON_HEADER        *CurrentTable;
//  UINT32                        FvStatus;
//  UINTN                         Size;
//  EFI_ACPI_TABLE_VERSION        Version;
//  UINTN                         TableHandle;
//  INTN                          Instance;
//  EFI_ACPI_TABLE_PROTOCOL       *AcpiTable;
//
//  Instance      = 0;
//  TableHandle   = 0;
//  CurrentTable  = NULL;
//  FwVol         = NULL;
//
//  //
//  // Find the AcpiSupport protocol
//  //
//  Status = LocateSupportProtocol (
//            &gEfiAcpiTableProtocolGuid,
//            gEfiAcpiMultiTableStorageGuid,
//            (VOID **) &AcpiTable,
//            FALSE
//            );
//  ASSERT_EFI_ERROR (Status);
//
//  //
//  // Locate the firmware volume protocol
//  //
//  Status = LocateSupportProtocol (
//            &gEfiFirmwareVolume2ProtocolGuid,
//            gEfiAcpiMultiTableStorageGuid,
//            (VOID **) &FwVol,
//            TRUE
//            );
//
//  /* TODO: Hang at native mode. Need to find the cause
//   * when the related drivers are finished for native support. */
//  //ASSERT_EFI_ERROR (Status);
//
//  //
//  // Read tables from the storage file.
//  //
//  while (Status == EFI_SUCCESS) {
//    Status = FwVol->ReadSection (
//                      FwVol,
//                      &gEfiAcpiMultiTableStorageGuid,
//                      EFI_SECTION_RAW,
//                      Instance,
//                      (VOID **) &CurrentTable,
//                      &Size,
//                      &FvStatus
//                      );
//
//    if (!EFI_ERROR (Status)) {
//
//      //
//      // Perform any table specific updates.
//      //
//      PlatformUpdateTables (CurrentTable, &Version);
//
//      //
//      // Add the table
//      //
//      TableHandle = 0;
//
//      if (Version != EFI_ACPI_TABLE_VERSION_NONE) {
//        DEBUG((DEBUG_INFO, "[Advanced ACPI] Installing table: %x : %x\n", CurrentTable->Signature, CurrentTable));
//        Status = AcpiTable->InstallAcpiTable (
//                              AcpiTable,
//                              CurrentTable,
//                              CurrentTable->Length,
//                              &TableHandle
//                              );
//      }
//
//      ASSERT_EFI_ERROR (Status);
//
//      //
//      // Increment the instance
//      //
//      Instance++;
//      gBS->FreePool (CurrentTable);
//      CurrentTable = NULL;
//    }
//
//    if (CurrentTable != NULL) {
//      gBS->FreePool (CurrentTable);
//      CurrentTable = NULL;
//    }
//  }
//
//  //
//  // Finished
//  //
//  return EFI_SUCCESS;
//}
//[-end-190620-IB11270237-remove]//
/**
  Allocate MemoryType below 4G memory address.

  @param[in] Size     - Size of memory to allocate.
  @param[in] Buffer   - Allocated address for output.

  @retval EFI_SUCCESS - Memory successfully allocated.
  @retval Other       - Other errors occur.

**/
EFI_STATUS
AllocateMemoryBelow4G (
  IN   EFI_MEMORY_TYPE MemoryType,
  IN   UINTN           Size,
  OUT  VOID           **Buffer
  )
{
  UINTN                 Pages;
  EFI_PHYSICAL_ADDRESS  Address;
  EFI_STATUS            Status;

  Pages = EFI_SIZE_TO_PAGES (Size);
  Address = 0xffffffff;

  Status  = (gBS->AllocatePages) (
                   AllocateMaxAddress,
                   MemoryType,
                   Pages,
                   &Address
                   );

  *Buffer = (VOID *) (UINTN) Address;

  return Status;
}
//[-start-190620-IB11270237-remove]//
//EFI_STATUS
//ReserveS3Memory (
//  VOID
//  )
//{
//  VOID                                          *AcpiMemoryBase;
//  UINT32                                        AcpiMemorySize;
//  EFI_PEI_HOB_POINTERS                          Hob;
//  UINT64                                        MemoryLength;
//  S3_MEMORY_VARIABLE                            S3MemVariable;
//  VOID                                          *VariableData;
//  UINTN                                         VarSize;
//  EFI_STATUS                                    Status;
//  EDKII_VARIABLE_LOCK_PROTOCOL                  *VariableLock;
//
//  DEBUG ((DEBUG_INFO, "ReserveS3Memory\n"));
//
//  //
//  // Allocate ACPI reserved memory under 4G
//  //
//  Status = AllocateMemoryBelow4G (EfiReservedMemoryType, PcdGet32 (PcdS3AcpiReservedMemorySize), &AcpiMemoryBase);
//  ASSERT (AcpiMemoryBase != 0);
//  AcpiMemorySize = PcdGet32 (PcdS3AcpiReservedMemorySize);
//
//  //
//  // Calculate the system memory length by memory hobs
//  //
//  MemoryLength  = 0x100000;
//  Hob.Raw = GetFirstHob (EFI_HOB_TYPE_RESOURCE_DESCRIPTOR);
//  ASSERT (Hob.Raw != NULL);
//  while ((Hob.Raw != NULL) && (!END_OF_HOB_LIST (Hob))) {
//    if (Hob.ResourceDescriptor->ResourceType == EFI_RESOURCE_SYSTEM_MEMORY) {
//      //
//      // Skip the memory region below 1MB
//      //
//      if (Hob.ResourceDescriptor->PhysicalStart >= 0x100000) {
//        MemoryLength += Hob.ResourceDescriptor->ResourceLength;
//      }
//    }
//    Hob.Raw = GET_NEXT_HOB (Hob);
//    Hob.Raw = GetNextHob (EFI_HOB_TYPE_RESOURCE_DESCRIPTOR, Hob.Raw);
//  }
//
//  S3MemVariable.AcpiReservedMemoryBase = (EFI_PHYSICAL_ADDRESS)(UINTN) AcpiMemoryBase;
//  S3MemVariable.AcpiReservedMemorySize = AcpiMemorySize;
//  S3MemVariable.SystemMemoryLength = MemoryLength;
//
//  DEBUG((DEBUG_INFO, "ReserveS3Memory: AcpiMemoryBase is 0x%8x\n", S3MemVariable.AcpiReservedMemoryBase));
//  DEBUG((DEBUG_INFO, "ReserveS3Memory: AcpiMemorySize is 0x%8x\n", S3MemVariable.AcpiReservedMemorySize));
//  DEBUG((DEBUG_INFO, "ReserveS3Memory: SystemMemoryLength is 0x%8x\n", S3MemVariable.SystemMemoryLength));
//
//  VarSize = sizeof(S3_MEMORY_VARIABLE);
//  Status = gBS->AllocatePool(
//                  EfiBootServicesData,
//                  VarSize,
//                  (VOID **) &VariableData
//                  );
//  ASSERT_EFI_ERROR(Status);
//  if (EFI_ERROR(Status)){
//    return Status;
//  }
//  Status = gRT->GetVariable (
//                    S3_MEMORY_VARIABLE_NAME,
//                    &gS3MemoryVariableGuid,
//                    NULL,
//                    &VarSize,
//                    VariableData
//                    );
//  if (EFI_ERROR (Status) || (0 != CompareMem (&S3MemVariable, VariableData, VarSize))) {
//    DEBUG((DEBUG_INFO, "Update S3MemVariable.\n"));
//    Status = gRT->SetVariable (
//                    S3_MEMORY_VARIABLE_NAME,
//                    &gS3MemoryVariableGuid,
//                    EFI_VARIABLE_BOOTSERVICE_ACCESS | EFI_VARIABLE_NON_VOLATILE,
//                    VarSize,
//                    &S3MemVariable
//                    );
//  }
//  gBS->FreePool(VariableData);
//
//  Status = gBS->LocateProtocol (&gEdkiiVariableLockProtocolGuid, NULL, (VOID **) &VariableLock);
//  if (!EFI_ERROR (Status)) {
//    Status = VariableLock->RequestToLock (VariableLock, S3_MEMORY_VARIABLE_NAME, &gS3MemoryVariableGuid);
//    ASSERT_EFI_ERROR (Status);
//  }
//
//  return EFI_SUCCESS;
//}
//[-start-201222-IB09480120-modify]//
VOID
EFIAPI
AcpiEndOfDxeEvent (
  EFI_EVENT           Event,
  VOID                *ParentImageHandle
  )
{

  if (Event != NULL) {
    gBS->CloseEvent(Event);
  }


  //
  // Install xHCI SSDT ACPI Table
  //
  InstallXhciAcpiTable ();

  //
  // Install Soc Gpe SSDT ACPI Table
  //
  InstallSocAcpiTable (&gSocGpeSsdtAcpiTableStorageGuid, SIGNATURE_64 ('S', 'o', 'c', 'G', 'p', 'e', ' ', 0));

  //
  // Install Soc Common SSDT ACPI Table
  //
  InstallSocAcpiTable (&gSocCmnSsdtAcpiTableStorageGuid, SIGNATURE_64 ('S', 'o', 'c', 'C', 'm', 'n', ' ', 0));

  //
  // Calculate Hardware Signature value based on current platform configurations
  //
  IsHardwareChange();

  //
  // Reserve memory for S3 resume
  //
  //ReserveS3Memory ();
}
//[-end-201222-IB09480120-modify]//
//[-end-190620-IB11270237-remove]//

/**
  Convert string containing GUID in the canonical form:
  "aabbccdd-eeff-gghh-iijj-kkllmmnnoopp"
  where aa - pp are unicode hexadecimal digits
  to the buffer format to be used in ACPI, byte ordering:
  [Byte 0] gg, hh, ee, ff, aa, bb, cc, dd [Byte 7]
  [Byte 8] pp, oo, nn, mm, ll, kk, jj, ii [Byte 16]

  @param[in]  GuidString    - GUID String null terminated (aligned on a 16-bit boundary)
  @param[out] AcpiGuidPart1 - First half of buffer  (bytes 0 - 7)
  @param[out] AcpiGuidPart2 - Second half of buffer (bytes 8 - 16)

  @retval EFI_SUCCESS     - String converted successfully.
  @retval EFI_UNSUPPORTED - Wrong input string format.

**/
EFI_STATUS
GuidStringToAcpiBuffer (
  IN  CHAR16 *GuidString,
  OUT UINT64 *AcpiGuidPart1,
  OUT UINT64 *AcpiGuidPart2
  )
{
  UINT32 GuidTempPart32 = 0;
  UINT16 GuidTempPart16 = 0;
  UINT8  GuidPartIndex;

  DEBUG((DEBUG_INFO,"GuidStringToAcpiBuffer() - GUID = %s\n", GuidString));

  for (GuidPartIndex = 0; GuidPartIndex < 4; GuidPartIndex++) {
    switch (GuidPartIndex){
      case 0:
        GuidTempPart32 = SwapBytes32((UINT32)StrHexToUint64(GuidString));
        *AcpiGuidPart1 = ((UINT64)GuidTempPart32 << 0x20);
        break;
      case 1:
        GuidTempPart16 = SwapBytes16((UINT16)StrHexToUint64(GuidString));
        *AcpiGuidPart1 += ((UINT64)GuidTempPart16 << 0x10);
        break;
      case 2:
        GuidTempPart16 = SwapBytes16((UINT16)StrHexToUint64(GuidString));
        *AcpiGuidPart1 += ((UINT64)GuidTempPart16);
        break;
      case 3:
        GuidTempPart16 = (UINT16)StrHexToUint64(GuidString);
        break;
//[-start-190620-IB11270237-remove]//
//      default:
//        return EFI_UNSUPPORTED;
//[-start-190620-IB11270237-remove]//
    }

    while ((*GuidString != L'-') && (*GuidString != L'\0')) {
      GuidString++;
    }

    if (*GuidString == L'-') {
      GuidString++;
    } else {
      return EFI_UNSUPPORTED;
    }
  }

  *AcpiGuidPart2 = ((UINT64)GuidTempPart16 << 0x30) + StrHexToUint64(GuidString);

  // Switch endianess because it will be swapped again in ACPI Buffer object
  *AcpiGuidPart1 = SwapBytes64(*AcpiGuidPart1);
  *AcpiGuidPart2 = SwapBytes64(*AcpiGuidPart2);

  return EFI_SUCCESS;
}
//[-start-190620-IB11270237-remove]//
///**
//  Save Acpi Setup relatived variables.
//**/
//EFI_STATUS
//SetAcpiSetupVariables (
//  VOID
//  )
//{
//  EFI_STATUS                   Status;
//  UINTN                        Handle;
//  SETUP_DATA                   SetupData;
//  UINTN                        VariableSize;
//  UINT32                       VariableAttributes;
//  EFI_ACPI_DESCRIPTION_HEADER  *Table;
//
//  VariableSize = sizeof (SETUP_DATA);
//  Status = gRT->GetVariable (
//                  L"Setup",
//                  &gSetupVariableGuid,
//                  &VariableAttributes,
//                  &VariableSize,
//                  &SetupData
//                  );
//  if (EFI_ERROR (Status)) {
//    return Status;
//  }
//
//  ///
//  /// Locate table with matching ID
//  ///
//  Handle = 0;
//  Table  = NULL;
//  Status = LocateAcpiTableBySignature (
//             EFI_ACPI_6_3_FIXED_ACPI_DESCRIPTION_TABLE_SIGNATURE,
//             (EFI_ACPI_DESCRIPTION_HEADER **) &Table,
//             &Handle
//             );
//  if (EFI_ERROR (Status) || (Table == NULL)) {
//    return Status;
//  }
//
//  SetupData.AcpiTableRevision = Table->Revision;
//  Status = gRT->SetVariable (
//                  L"Setup",
//                  &gSetupVariableGuid,
//                  VariableAttributes,
//                  VariableSize,
//                  &SetupData
//                  );
//  ASSERT_EFI_ERROR(Status);
//  DEBUG ((DEBUG_INFO, "Acpi table revision: 0x%x, saved successfully.\n", Table->Revision));
//
//  return Status;
//}
//[-end-190620-IB11270237-remove]//
/**
  ACPI Platform driver installation function.

  @param[in] ImageHandle     Handle for this drivers loaded image protocol.
  @param[in] SystemTable     EFI system table.

  @retval EFI_SUCCESS        The driver installed without error.
  @retval EFI_ABORTED        The driver encountered an error and could not complete installation of
                             the ACPI tables.

**/
EFI_STATUS
EFIAPI
InstallAcpiPlatform (
  IN EFI_HANDLE         ImageHandle,
  IN EFI_SYSTEM_TABLE   *SystemTable
  )
{
  EFI_STATUS                    Status;
  UINTN                         VarDataSize;
  EFI_HANDLE                    Handle;
  UINT8                         PortData;
  //UINT8                         BatteryNum;
  CPUID_VERSION_INFO_EAX        CpuidVersionInfoEax;
  EFI_MP_SERVICES_PROTOCOL      *MpService;
  UINTN                         Index;
//[-start-201222-IB09480120-modify]//
//[-start-190620-IB11270237-remove]//
  EFI_EVENT                     EndOfDxeEvent;
//[-end-190620-IB11270237-remove]//
//[-end-201222-IB09480120-modify]//
  SA_SETUP                      SaSetup;
  CPU_SETUP                     CpuSetup;
  UINT64                        RpDeviceBaseAddress;
  UINT8                         PreBootCmMode;
  UINT8                         CmTbtMask;
  UINT8                         Usb4CmSwitchEnable;
  VOID                          *HobPtr;
  USB4_PLATFORM_HOB             *Usb4PlatformHob;
  USB4_PLATFORM_INFO            *Usb4PlatformInfo;

  UINT16                        ModuleName[MIPICAM_MODULE_NAME_LENGTH];
  UINT64                        AcpiGuidPart1;
  UINT64                        AcpiGuidPart2;
  CHAR16                        LocalGuidString[GUID_CHARS_NUMBER];
  UINT8                         SerialIoIndex;
  UINT8                         PepSerialIoI2c[PCH_MAX_SERIALIO_I2C_CONTROLLERS];
  UINT8                         PepSerialIoSpi[PCH_MAX_SERIALIO_SPI_CONTROLLERS];
  UINT8                         PepSerialIoUart[PCH_MAX_SERIALIO_UART_CONTROLLERS];
  UINT8                         PepUfs[PCH_MAX_UFS_DEV_NUM];
  UINT8                         UfsIndex;
//[-start-190620-IB11270237-add]//
  PLATFORM_NVS_AREA_PROTOCOL    *PlatformNvsArea;
  EFI_EVENT                     EventOutPut;
  BOOLEAN                       PowerStateIsAc;
  EFI_STATUS                    CommandStatus;
//[-end-190620-IB11270237-add]//
  Handle                 = NULL;
  mCpuApicIdOrderTable   = NULL;
  mCpuApicIdReorderTable = NULL;

  AsmCpuid (
    CPUID_VERSION_INFO,
    &CpuidVersionInfoEax.Uint32,
    NULL,
    NULL,
    NULL
    );
//[-start-190620-IB11270237-remove]//
//  PublishOverClockingAcpiTable ();
//[-end-190620-IB11270237-remove]//
  //
  // Locate the setup configuration data
  // We assert if it is not found because we have a dependency on PlatformCpu,
  // published by the platform setup driver.
  // This should ensure that we only get called after the setup has updated the
  // variable.
  //
//[-start-190620-IB11270237-add]//
  VarDataSize = sizeof (CHIPSET_CONFIGURATION);
  Status = gRT->GetVariable (
                  L"Setup",
                  &gSystemConfigurationGuid,
                  NULL,
                  &VarDataSize,
                  &mChipsetSetupData
                  );
//[-end-190620-IB11270237-add]//
  VarDataSize = sizeof (SETUP_DATA);
  Status = gRT->GetVariable (
                  L"Setup",
                  &gSetupVariableGuid,
                  NULL,
                  &VarDataSize,
                  &mSystemConfiguration
                  );

  VarDataSize = sizeof (SA_SETUP);
  Status = gRT->GetVariable (
                  L"SaSetup",
                  &gSaSetupVariableGuid,
                  NULL,
                  &VarDataSize,
                  &SaSetup
                  );

  VarDataSize = sizeof (ME_SETUP);
  Status = gRT->GetVariable (
                  L"MeSetup",
                  &gMeSetupVariableGuid,
                  NULL,
                  &VarDataSize,
                  &mMeSetup
                  );

  VarDataSize = sizeof (CPU_SETUP);
  Status = gRT->GetVariable (
                  L"CpuSetup",
                  &gCpuSetupVariableGuid,
                  NULL,
                  &VarDataSize,
                  &CpuSetup
                  );

  VarDataSize = sizeof (PCH_SETUP);
  Status = gRT->GetVariable (
                  L"PchSetup",
                  &gPchSetupVariableGuid,
                  NULL,
                  &VarDataSize,
                  &mPchSetup
                  );
//[-start-190620-IB11270237-add]//
  Status = gBS->LocateProtocol (
                  &gPlatformNvsAreaProtocolGuid,
                  NULL,
                  (VOID **) &PlatformNvsArea
                  );
  if (EFI_ERROR (Status)) {
    return Status;
  }
  mPlatformNvsAreaProtocol = *PlatformNvsArea;
//[-end-190620-IB11270237-add]//
  //
  // Locate the MP services protocol
  // Find the MP Protocol. This is an MP platform, so MP protocol must be there.
  //
  Status = gBS->LocateProtocol (
                  &gEfiMpServiceProtocolGuid,
                  NULL,
                  (VOID **) &MpService
                  );
  if (EFI_ERROR (Status)) {
    return Status;
  }

  //
  // Determine the number of processors
  //
  MpService->GetNumberOfProcessors (
              MpService,
              &mNumberOfCpus,
              &mNumberOfEnabledCpus
              );

  DEBUG ((DEBUG_INFO, "mNumberOfCpus - %x,mNumberOfEnabledCpus - %x\n", mNumberOfCpus, mNumberOfEnabledCpus));
  mCpuApicIdOrderTable   = AllocateZeroPool (mNumberOfCpus * sizeof (EFI_CPU_ID_ORDER_MAP));
  if (mCpuApicIdOrderTable == NULL) {
    return EFI_UNSUPPORTED;
  }
  mCpuApicIdReorderTable = AllocateZeroPool (mNumberOfCpus * sizeof (EFI_CPU_APIC_ID_REORDER_MAP));
  if (mCpuApicIdReorderTable == NULL) {
    return EFI_UNSUPPORTED;
  }

  //
  // Allocate and initialize the NVS area for SMM and ASL communication.
  //
  AcpiGnvsInit((VOID **) &mPlatformNvsAreaProtocol.Area);

  //
  // Update global NVS area for ASL and SMM init code to use
  //
  //mPlatformNvsAreaProtocol.Area->EnableThermalOffset        = mSystemConfiguration.EnableThermalOffset;
  mPlatformNvsAreaProtocol.Area->Ac1TripPoint                 = mSystemConfiguration.Ac1TripPoint;
  mPlatformNvsAreaProtocol.Area->Ac0TripPoint                 = mSystemConfiguration.Ac0TripPoint;
  mPlatformNvsAreaProtocol.Area->Ac1FanSpeed                  = mSystemConfiguration.Ac1FanSpeed;
  mPlatformNvsAreaProtocol.Area->Ac0FanSpeed                  = mSystemConfiguration.Ac0FanSpeed;
  mPlatformNvsAreaProtocol.Area->PassiveThermalTripPoint      = mSystemConfiguration.PassiveThermalTripPoint;
  mPlatformNvsAreaProtocol.Area->PassiveTc1Value              = mSystemConfiguration.PassiveTc1Value;
  mPlatformNvsAreaProtocol.Area->PassiveTc2Value              = mSystemConfiguration.PassiveTc2Value;
  mPlatformNvsAreaProtocol.Area->PassiveTspValue              = mSystemConfiguration.PassiveTspValue;
  mPlatformNvsAreaProtocol.Area->CriticalThermalTripPoint     = mSystemConfiguration.CriticalThermalTripPoint;
  mPlatformNvsAreaProtocol.Area->EmaEnable                    = 0; //mSystemConfiguration.EmaEnable; //todo
  mPlatformNvsAreaProtocol.Area->ThreadCount                  = (UINT8) mNumberOfEnabledCpus;
  mPlatformNvsAreaProtocol.Area->MefEnable                    = 0; //mSystemConfiguration.MefEnable; //todo

  //
  // Intel(R) Dynamic Tuning Technology Devices and trip points
  //
  mPlatformNvsAreaProtocol.Area->EnableDptf                   = mSystemConfiguration.EnableDptf;

  if (mSystemConfiguration.EnableDptf == 1) {

    mPlatformNvsAreaProtocol.Area->EnableDCFG                   = mSystemConfiguration.EnableDCFG;
    mPlatformNvsAreaProtocol.Area->EnableSaDevice               = mSystemConfiguration.EnableSaDevice;
    mPlatformNvsAreaProtocol.Area->PpccStepSize                 = mSystemConfiguration.PpccStepSize;

    //
    // Intel(R) Dynamic Tuning Technology Devices
    //
    mPlatformNvsAreaProtocol.Area->EnableFan1Device             = mSystemConfiguration.EnableFan1Device;
    mPlatformNvsAreaProtocol.Area->EnableFan2Device             = mSystemConfiguration.EnableFan2Device;
    mPlatformNvsAreaProtocol.Area->EnableFan3Device             = mSystemConfiguration.EnableFan3Device;

    //
    // Intel(R) Dynamic Tuning Technology Oem Design Variable
    //

    mPlatformNvsAreaProtocol.Area->OemDesignVariable0           = mSystemConfiguration.OemDesignVariable0;
    mPlatformNvsAreaProtocol.Area->OemDesignVariable1           = mSystemConfiguration.OemDesignVariable1;
    mPlatformNvsAreaProtocol.Area->OemDesignVariable2           = mSystemConfiguration.OemDesignVariable2;
    mPlatformNvsAreaProtocol.Area->OemDesignVariable3           = mSystemConfiguration.OemDesignVariable3;
    mPlatformNvsAreaProtocol.Area->OemDesignVariable4           = mSystemConfiguration.OemDesignVariable4;
    mPlatformNvsAreaProtocol.Area->OemDesignVariable5           = mSystemConfiguration.OemDesignVariable5;
//[-start-210701-KEBIN00030-modify]//
#ifdef LCFC_SUPPORT
    mPlatformNvsAreaProtocol.Area->OemDesignVariable6           = mSystemConfiguration.OemDesignVariable6;
    mPlatformNvsAreaProtocol.Area->OemDesignVariable7           = mSystemConfiguration.OemDesignVariable7;
    mPlatformNvsAreaProtocol.Area->OemDesignVariable8           = mSystemConfiguration.OemDesignVariable8;
    mPlatformNvsAreaProtocol.Area->OemDesignVariable9           = mSystemConfiguration.OemDesignVariable9;
    mPlatformNvsAreaProtocol.Area->OemDesignVariable10          = mSystemConfiguration.OemDesignVariable10;
    mPlatformNvsAreaProtocol.Area->OemDesignVariable11          = mSystemConfiguration.OemDesignVariable11;
    mPlatformNvsAreaProtocol.Area->OemDesignVariable12          = mSystemConfiguration.OemDesignVariable12;
    mPlatformNvsAreaProtocol.Area->OemDesignVariable13          = mSystemConfiguration.OemDesignVariable13;
    mPlatformNvsAreaProtocol.Area->OemDesignVariable14          = mSystemConfiguration.OemDesignVariable14;
    mPlatformNvsAreaProtocol.Area->OemDesignVariable15          = mSystemConfiguration.OemDesignVariable15;
    mPlatformNvsAreaProtocol.Area->OemDesignVariable16          = mSystemConfiguration.OemDesignVariable16;
    mPlatformNvsAreaProtocol.Area->OemDesignVariable17          = mSystemConfiguration.OemDesignVariable17;
    mPlatformNvsAreaProtocol.Area->OemDesignVariable18          = mSystemConfiguration.OemDesignVariable18;
    mPlatformNvsAreaProtocol.Area->OemDesignVariable19          = mSystemConfiguration.OemDesignVariable19;
    mPlatformNvsAreaProtocol.Area->OemDesignVariable20          = mSystemConfiguration.OemDesignVariable20;
//[-start-211202-QINGLIN0124-add]//
#if defined(S570_SUPPORT)
    mPlatformNvsAreaProtocol.Area->OemDesignVariable21          = mSystemConfiguration.OemDesignVariable21;
#endif
//[-end-211202-QINGLIN0124-add]//
#endif
//[-end-210701-KEBIN00030-modify]//
    mPlatformNvsAreaProtocol.Area->EnableChargerParticipant     = mSystemConfiguration.EnableChargerParticipant;
    mPlatformNvsAreaProtocol.Area->EnablePowerParticipant       = mSystemConfiguration.EnablePowerParticipant;
    mPlatformNvsAreaProtocol.Area->PowerParticipantPollingRate  = mSystemConfiguration.PowerParticipantPollingRate;

    mPlatformNvsAreaProtocol.Area->EnableBatteryParticipant      = mSystemConfiguration.EnableBatteryParticipant;
    mPlatformNvsAreaProtocol.Area->EnableInt3400Device           = mSystemConfiguration.EnableInt3400Device;

    mPlatformNvsAreaProtocol.Area->EnablePchFivrParticipant       = mSystemConfiguration.EnablePchFivrParticipant;

    mPlatformNvsAreaProtocol.Area->EnableSen1Participant        = mSystemConfiguration.EnableSen1Participant;
    mPlatformNvsAreaProtocol.Area->EnableSen2Participant        = mSystemConfiguration.EnableSen2Participant;
    mPlatformNvsAreaProtocol.Area->EnableSen3Participant        = mSystemConfiguration.EnableSen3Participant;
    mPlatformNvsAreaProtocol.Area->EnableSen4Participant        = mSystemConfiguration.EnableSen4Participant;
    mPlatformNvsAreaProtocol.Area->EnableSen5Participant        = mSystemConfiguration.EnableSen5Participant;
    mPlatformNvsAreaProtocol.Area->EnableDgpuParticipant        = mSystemConfiguration.EnableDgpuParticipant;

  }

  mPlatformNvsAreaProtocol.Area->HebcValue               = mSystemConfiguration.HebcValue;

  //
  // BIOS only version of CTDP. (CTDP without using Intel(R) Dynamic Tuning Technology)
  //
  if (CpuSetup.ConfigTdpLock == 1 || mSystemConfiguration.EnableDptf == 1) {
    mPlatformNvsAreaProtocol.Area->ConfigTdpBios      = 0;
  } else {
    mPlatformNvsAreaProtocol.Area->ConfigTdpBios      = CpuSetup.ConfigTdpBios;
  }

  //
  // PECI Access Method
  //
  mPlatformNvsAreaProtocol.Area->PeciAccessMethod     = mSystemConfiguration.PeciAccessMethod;

  //
  // Wireless support
  //
  mPlatformNvsAreaProtocol.Area->PowerSharingManagerEnable  = mSystemConfiguration.PowerSharingManagerEnable;
  mPlatformNvsAreaProtocol.Area->PsmSplcDomainType1         = mSystemConfiguration.PsmSplcDomainType1;
  mPlatformNvsAreaProtocol.Area->PsmSplcPowerLimit1         = mSystemConfiguration.PsmSplcPowerLimit1;
  mPlatformNvsAreaProtocol.Area->PsmSplcTimeWindow1         = mSystemConfiguration.PsmSplcTimeWindow1;

  mPlatformNvsAreaProtocol.Area->PsmDplcDomainType1         = mSystemConfiguration.PsmDplcDomainType1;
  mPlatformNvsAreaProtocol.Area->PsmDplcDomainPreference1   = mSystemConfiguration.PsmDplcDomainPreference1;
  mPlatformNvsAreaProtocol.Area->PsmDplcPowerLimitIndex1    = mSystemConfiguration.PsmDplcPowerLimitIndex1;
  mPlatformNvsAreaProtocol.Area->PsmDplcDefaultPowerLimit1  = mSystemConfiguration.PsmDplcDefaultPowerLimit1;
  mPlatformNvsAreaProtocol.Area->PsmDplcDefaultTimeWindow1  = mSystemConfiguration.PsmDplcDefaultTimeWindow1;
  mPlatformNvsAreaProtocol.Area->PsmDplcMinimumPowerLimit1  = mSystemConfiguration.PsmDplcMinimumPowerLimit1;
  mPlatformNvsAreaProtocol.Area->PsmDplcMaximumPowerLimit1  = mSystemConfiguration.PsmDplcMaximumPowerLimit1;
  mPlatformNvsAreaProtocol.Area->PsmDplcMaximumTimeWindow1  = mSystemConfiguration.PsmDplcMaximumTimeWindow1;

  mPlatformNvsAreaProtocol.Area->WifiEnable                = mSystemConfiguration.WifiEnable;
  if (mSystemConfiguration.WifiEnable == 1) {
    mPlatformNvsAreaProtocol.Area->WifiDomainType1         = mSystemConfiguration.WifiDomainType1;
    mPlatformNvsAreaProtocol.Area->WifiPowerLimit1         = mSystemConfiguration.WifiPowerLimit1;
    mPlatformNvsAreaProtocol.Area->WifiTimeWindow1         = mSystemConfiguration.WifiTimeWindow1;
    mPlatformNvsAreaProtocol.Area->TRxDelay0               = mSystemConfiguration.TRxDelay0;
    mPlatformNvsAreaProtocol.Area->TRxCableLength0         = mSystemConfiguration.TRxCableLength0;
    mPlatformNvsAreaProtocol.Area->TRxDelay1               = mSystemConfiguration.TRxDelay1;
    mPlatformNvsAreaProtocol.Area->TRxCableLength1         = mSystemConfiguration.TRxCableLength1;
    mPlatformNvsAreaProtocol.Area->WrddDomainType1         = mSystemConfiguration.WrddDomainType1;
    mPlatformNvsAreaProtocol.Area->WrddCountryIndentifier1 = mSystemConfiguration.WrddCountryIndentifier1;
    mPlatformNvsAreaProtocol.Area->WrdsWiFiSarEnable       = mSystemConfiguration.WrdsWiFiSarEnable;
    mPlatformNvsAreaProtocol.Area->AntennaDiversity        = mSystemConfiguration.AntennaDiversity;

    if (mPlatformNvsAreaProtocol.Area->WrdsWiFiSarEnable == 1) {
      mPlatformNvsAreaProtocol.Area->WrdsWiFiSarTxPowerSet1Limit1  = mSystemConfiguration.WrdsWiFiSarTxPowerSet1Limit1;
      mPlatformNvsAreaProtocol.Area->WrdsWiFiSarTxPowerSet1Limit2  = mSystemConfiguration.WrdsWiFiSarTxPowerSet1Limit2;
      mPlatformNvsAreaProtocol.Area->WrdsWiFiSarTxPowerSet1Limit3  = mSystemConfiguration.WrdsWiFiSarTxPowerSet1Limit3;
      mPlatformNvsAreaProtocol.Area->WrdsWiFiSarTxPowerSet1Limit4  = mSystemConfiguration.WrdsWiFiSarTxPowerSet1Limit4;
      mPlatformNvsAreaProtocol.Area->WrdsWiFiSarTxPowerSet1Limit5  = mSystemConfiguration.WrdsWiFiSarTxPowerSet1Limit5;
      mPlatformNvsAreaProtocol.Area->WrdsWiFiSarTxPowerSet1Limit6  = mSystemConfiguration.WrdsWiFiSarTxPowerSet1Limit6;
      mPlatformNvsAreaProtocol.Area->WrdsWiFiSarTxPowerSet1Limit7  = mSystemConfiguration.WrdsWiFiSarTxPowerSet1Limit7;
      mPlatformNvsAreaProtocol.Area->WrdsWiFiSarTxPowerSet1Limit8  = mSystemConfiguration.WrdsWiFiSarTxPowerSet1Limit8;
      mPlatformNvsAreaProtocol.Area->WrdsWiFiSarTxPowerSet1Limit9  = mSystemConfiguration.WrdsWiFiSarTxPowerSet1Limit9;
      mPlatformNvsAreaProtocol.Area->WrdsWiFiSarTxPowerSet1Limit10 = mSystemConfiguration.WrdsWiFiSarTxPowerSet1Limit10;
      mPlatformNvsAreaProtocol.Area->WrdsWiFiSarTxPowerSet1Limit11 = mSystemConfiguration.WrdsWiFiSarTxPowerSet1Limit11;
      mPlatformNvsAreaProtocol.Area->WrdsWiFiSarTxPowerSet1Limit12 = mSystemConfiguration.WrdsWiFiSarTxPowerSet1Limit12;
      mPlatformNvsAreaProtocol.Area->WrdsWiFiSarTxPowerSet1Limit13 = mSystemConfiguration.WrdsWiFiSarTxPowerSet1Limit13;
      mPlatformNvsAreaProtocol.Area->WrdsWiFiSarTxPowerSet1Limit14 = mSystemConfiguration.WrdsWiFiSarTxPowerSet1Limit14;
      mPlatformNvsAreaProtocol.Area->WrdsWiFiSarTxPowerSet1Limit15 = mSystemConfiguration.WrdsWiFiSarTxPowerSet1Limit15;
      mPlatformNvsAreaProtocol.Area->WrdsWiFiSarTxPowerSet1Limit16 = mSystemConfiguration.WrdsWiFiSarTxPowerSet1Limit16;
      mPlatformNvsAreaProtocol.Area->WrdsWiFiSarTxPowerSet1Limit17 = mSystemConfiguration.WrdsWiFiSarTxPowerSet1Limit17;
      mPlatformNvsAreaProtocol.Area->WrdsWiFiSarTxPowerSet1Limit18 = mSystemConfiguration.WrdsWiFiSarTxPowerSet1Limit18;
      mPlatformNvsAreaProtocol.Area->WrdsWiFiSarTxPowerSet1Limit19 = mSystemConfiguration.WrdsWiFiSarTxPowerSet1Limit19;
      mPlatformNvsAreaProtocol.Area->WrdsWiFiSarTxPowerSet1Limit20 = mSystemConfiguration.WrdsWiFiSarTxPowerSet1Limit20;
      mPlatformNvsAreaProtocol.Area->WrdsWiFiSarTxPowerSet1Limit21 = mSystemConfiguration.WrdsWiFiSarTxPowerSet1Limit21;
      mPlatformNvsAreaProtocol.Area->WrdsWiFiSarTxPowerSet1Limit22 = mSystemConfiguration.WrdsWiFiSarTxPowerSet1Limit22;

      mPlatformNvsAreaProtocol.Area->WrdsCdbWiFiSarTxPowerSet1Limit1  = mSystemConfiguration.WrdsCdbWiFiSarTxPowerSet1Limit1;
      mPlatformNvsAreaProtocol.Area->WrdsCdbWiFiSarTxPowerSet1Limit2  = mSystemConfiguration.WrdsCdbWiFiSarTxPowerSet1Limit2;
      mPlatformNvsAreaProtocol.Area->WrdsCdbWiFiSarTxPowerSet1Limit3  = mSystemConfiguration.WrdsCdbWiFiSarTxPowerSet1Limit3;
      mPlatformNvsAreaProtocol.Area->WrdsCdbWiFiSarTxPowerSet1Limit4  = mSystemConfiguration.WrdsCdbWiFiSarTxPowerSet1Limit4;
      mPlatformNvsAreaProtocol.Area->WrdsCdbWiFiSarTxPowerSet1Limit5  = mSystemConfiguration.WrdsCdbWiFiSarTxPowerSet1Limit5;
      mPlatformNvsAreaProtocol.Area->WrdsCdbWiFiSarTxPowerSet1Limit6  = mSystemConfiguration.WrdsCdbWiFiSarTxPowerSet1Limit6;
      mPlatformNvsAreaProtocol.Area->WrdsCdbWiFiSarTxPowerSet1Limit7  = mSystemConfiguration.WrdsCdbWiFiSarTxPowerSet1Limit7;
      mPlatformNvsAreaProtocol.Area->WrdsCdbWiFiSarTxPowerSet1Limit8  = mSystemConfiguration.WrdsCdbWiFiSarTxPowerSet1Limit8;
      mPlatformNvsAreaProtocol.Area->WrdsCdbWiFiSarTxPowerSet1Limit9  = mSystemConfiguration.WrdsCdbWiFiSarTxPowerSet1Limit9;
      mPlatformNvsAreaProtocol.Area->WrdsCdbWiFiSarTxPowerSet1Limit10 = mSystemConfiguration.WrdsCdbWiFiSarTxPowerSet1Limit10;
      mPlatformNvsAreaProtocol.Area->WrdsCdbWiFiSarTxPowerSet1Limit11 = mSystemConfiguration.WrdsCdbWiFiSarTxPowerSet1Limit11;
      mPlatformNvsAreaProtocol.Area->WrdsCdbWiFiSarTxPowerSet1Limit12 = mSystemConfiguration.WrdsCdbWiFiSarTxPowerSet1Limit12;
      mPlatformNvsAreaProtocol.Area->WrdsCdbWiFiSarTxPowerSet1Limit13 = mSystemConfiguration.WrdsCdbWiFiSarTxPowerSet1Limit13;
      mPlatformNvsAreaProtocol.Area->WrdsCdbWiFiSarTxPowerSet1Limit14 = mSystemConfiguration.WrdsCdbWiFiSarTxPowerSet1Limit14;
      mPlatformNvsAreaProtocol.Area->WrdsCdbWiFiSarTxPowerSet1Limit15 = mSystemConfiguration.WrdsCdbWiFiSarTxPowerSet1Limit15;
      mPlatformNvsAreaProtocol.Area->WrdsCdbWiFiSarTxPowerSet1Limit16 = mSystemConfiguration.WrdsCdbWiFiSarTxPowerSet1Limit16;
      mPlatformNvsAreaProtocol.Area->WrdsCdbWiFiSarTxPowerSet1Limit17 = mSystemConfiguration.WrdsCdbWiFiSarTxPowerSet1Limit17;
      mPlatformNvsAreaProtocol.Area->WrdsCdbWiFiSarTxPowerSet1Limit18 = mSystemConfiguration.WrdsCdbWiFiSarTxPowerSet1Limit18;
      mPlatformNvsAreaProtocol.Area->WrdsCdbWiFiSarTxPowerSet1Limit19 = mSystemConfiguration.WrdsCdbWiFiSarTxPowerSet1Limit19;
      mPlatformNvsAreaProtocol.Area->WrdsCdbWiFiSarTxPowerSet1Limit20 = mSystemConfiguration.WrdsCdbWiFiSarTxPowerSet1Limit20;
      mPlatformNvsAreaProtocol.Area->WrdsCdbWiFiSarTxPowerSet1Limit21 = mSystemConfiguration.WrdsCdbWiFiSarTxPowerSet1Limit21;
      mPlatformNvsAreaProtocol.Area->WrdsCdbWiFiSarTxPowerSet1Limit22 = mSystemConfiguration.WrdsCdbWiFiSarTxPowerSet1Limit22;
    } else {
      mPlatformNvsAreaProtocol.Area->WrdsWiFiSarTxPowerSet1Limit1  = 0x0;
      mPlatformNvsAreaProtocol.Area->WrdsWiFiSarTxPowerSet1Limit2  = 0x0;
      mPlatformNvsAreaProtocol.Area->WrdsWiFiSarTxPowerSet1Limit3  = 0x0;
      mPlatformNvsAreaProtocol.Area->WrdsWiFiSarTxPowerSet1Limit4  = 0x0;
      mPlatformNvsAreaProtocol.Area->WrdsWiFiSarTxPowerSet1Limit5  = 0x0;
      mPlatformNvsAreaProtocol.Area->WrdsWiFiSarTxPowerSet1Limit6  = 0x0;
      mPlatformNvsAreaProtocol.Area->WrdsWiFiSarTxPowerSet1Limit7  = 0x0;
      mPlatformNvsAreaProtocol.Area->WrdsWiFiSarTxPowerSet1Limit8  = 0x0;
      mPlatformNvsAreaProtocol.Area->WrdsWiFiSarTxPowerSet1Limit9  = 0x0;
      mPlatformNvsAreaProtocol.Area->WrdsWiFiSarTxPowerSet1Limit10 = 0x0;
      mPlatformNvsAreaProtocol.Area->WrdsWiFiSarTxPowerSet1Limit11 = 0x0;
      mPlatformNvsAreaProtocol.Area->WrdsWiFiSarTxPowerSet1Limit12 = 0x0;
      mPlatformNvsAreaProtocol.Area->WrdsWiFiSarTxPowerSet1Limit13 = 0x0;
      mPlatformNvsAreaProtocol.Area->WrdsWiFiSarTxPowerSet1Limit14 = 0x0;
      mPlatformNvsAreaProtocol.Area->WrdsWiFiSarTxPowerSet1Limit15 = 0x0;
      mPlatformNvsAreaProtocol.Area->WrdsWiFiSarTxPowerSet1Limit16 = 0x0;
      mPlatformNvsAreaProtocol.Area->WrdsWiFiSarTxPowerSet1Limit17 = 0x0;
      mPlatformNvsAreaProtocol.Area->WrdsWiFiSarTxPowerSet1Limit18 = 0x0;
      mPlatformNvsAreaProtocol.Area->WrdsWiFiSarTxPowerSet1Limit19 = 0x0;
      mPlatformNvsAreaProtocol.Area->WrdsWiFiSarTxPowerSet1Limit20 = 0x0;
      mPlatformNvsAreaProtocol.Area->WrdsWiFiSarTxPowerSet1Limit21 = 0x0;
      mPlatformNvsAreaProtocol.Area->WrdsWiFiSarTxPowerSet1Limit22 = 0x0;

      mPlatformNvsAreaProtocol.Area->WrdsCdbWiFiSarTxPowerSet1Limit1  = 0x00;
      mPlatformNvsAreaProtocol.Area->WrdsCdbWiFiSarTxPowerSet1Limit2  = 0x00;
      mPlatformNvsAreaProtocol.Area->WrdsCdbWiFiSarTxPowerSet1Limit3  = 0x00;
      mPlatformNvsAreaProtocol.Area->WrdsCdbWiFiSarTxPowerSet1Limit4  = 0x00;
      mPlatformNvsAreaProtocol.Area->WrdsCdbWiFiSarTxPowerSet1Limit5  = 0x00;
      mPlatformNvsAreaProtocol.Area->WrdsCdbWiFiSarTxPowerSet1Limit6  = 0x00;
      mPlatformNvsAreaProtocol.Area->WrdsCdbWiFiSarTxPowerSet1Limit7  = 0x00;
      mPlatformNvsAreaProtocol.Area->WrdsCdbWiFiSarTxPowerSet1Limit8  = 0x00;
      mPlatformNvsAreaProtocol.Area->WrdsCdbWiFiSarTxPowerSet1Limit9  = 0x00;
      mPlatformNvsAreaProtocol.Area->WrdsCdbWiFiSarTxPowerSet1Limit10 = 0x00;
      mPlatformNvsAreaProtocol.Area->WrdsCdbWiFiSarTxPowerSet1Limit11 = 0x00;
      mPlatformNvsAreaProtocol.Area->WrdsCdbWiFiSarTxPowerSet1Limit12 = 0x00;
      mPlatformNvsAreaProtocol.Area->WrdsCdbWiFiSarTxPowerSet1Limit13 = 0x00;
      mPlatformNvsAreaProtocol.Area->WrdsCdbWiFiSarTxPowerSet1Limit14 = 0x00;
      mPlatformNvsAreaProtocol.Area->WrdsCdbWiFiSarTxPowerSet1Limit15 = 0x00;
      mPlatformNvsAreaProtocol.Area->WrdsCdbWiFiSarTxPowerSet1Limit16 = 0x00;
      mPlatformNvsAreaProtocol.Area->WrdsCdbWiFiSarTxPowerSet1Limit17 = 0x00;
      mPlatformNvsAreaProtocol.Area->WrdsCdbWiFiSarTxPowerSet1Limit18 = 0x00;
      mPlatformNvsAreaProtocol.Area->WrdsCdbWiFiSarTxPowerSet1Limit19 = 0x00;
      mPlatformNvsAreaProtocol.Area->WrdsCdbWiFiSarTxPowerSet1Limit20 = 0x00;
      mPlatformNvsAreaProtocol.Area->WrdsCdbWiFiSarTxPowerSet1Limit21 = 0x00;
      mPlatformNvsAreaProtocol.Area->WrdsCdbWiFiSarTxPowerSet1Limit22 = 0x00;
    }

    mPlatformNvsAreaProtocol.Area->EwrdWiFiDynamicSarEnable = mSystemConfiguration.EwrdWiFiDynamicSarEnable;
    mPlatformNvsAreaProtocol.Area->EwrdWiFiDynamicSarRangeSets = mSystemConfiguration.EwrdWiFiDynamicSarRangeSets;
    if (mPlatformNvsAreaProtocol.Area->EwrdWiFiDynamicSarEnable == 1) {
        mPlatformNvsAreaProtocol.Area->EwrdWiFiSarTxPowerSet2Limit1  = mSystemConfiguration.EwrdWiFiSarTxPowerSet2Limit1;
        mPlatformNvsAreaProtocol.Area->EwrdWiFiSarTxPowerSet2Limit2  = mSystemConfiguration.EwrdWiFiSarTxPowerSet2Limit2;
        mPlatformNvsAreaProtocol.Area->EwrdWiFiSarTxPowerSet2Limit3  = mSystemConfiguration.EwrdWiFiSarTxPowerSet2Limit3;
        mPlatformNvsAreaProtocol.Area->EwrdWiFiSarTxPowerSet2Limit4  = mSystemConfiguration.EwrdWiFiSarTxPowerSet2Limit4;
        mPlatformNvsAreaProtocol.Area->EwrdWiFiSarTxPowerSet2Limit5  = mSystemConfiguration.EwrdWiFiSarTxPowerSet2Limit5;
        mPlatformNvsAreaProtocol.Area->EwrdWiFiSarTxPowerSet2Limit6  = mSystemConfiguration.EwrdWiFiSarTxPowerSet2Limit6;
        mPlatformNvsAreaProtocol.Area->EwrdWiFiSarTxPowerSet2Limit7  = mSystemConfiguration.EwrdWiFiSarTxPowerSet2Limit7;
        mPlatformNvsAreaProtocol.Area->EwrdWiFiSarTxPowerSet2Limit8  = mSystemConfiguration.EwrdWiFiSarTxPowerSet2Limit8;
        mPlatformNvsAreaProtocol.Area->EwrdWiFiSarTxPowerSet2Limit9  = mSystemConfiguration.EwrdWiFiSarTxPowerSet2Limit9;
        mPlatformNvsAreaProtocol.Area->EwrdWiFiSarTxPowerSet2Limit10 = mSystemConfiguration.EwrdWiFiSarTxPowerSet2Limit10;
        mPlatformNvsAreaProtocol.Area->EwrdWiFiSarTxPowerSet2Limit11 = mSystemConfiguration.EwrdWiFiSarTxPowerSet2Limit11;
        mPlatformNvsAreaProtocol.Area->EwrdWiFiSarTxPowerSet2Limit12 = mSystemConfiguration.EwrdWiFiSarTxPowerSet2Limit12;
        mPlatformNvsAreaProtocol.Area->EwrdWiFiSarTxPowerSet2Limit13 = mSystemConfiguration.EwrdWiFiSarTxPowerSet2Limit13;
        mPlatformNvsAreaProtocol.Area->EwrdWiFiSarTxPowerSet2Limit14 = mSystemConfiguration.EwrdWiFiSarTxPowerSet2Limit14;
        mPlatformNvsAreaProtocol.Area->EwrdWiFiSarTxPowerSet2Limit15 = mSystemConfiguration.EwrdWiFiSarTxPowerSet2Limit15;
        mPlatformNvsAreaProtocol.Area->EwrdWiFiSarTxPowerSet2Limit16 = mSystemConfiguration.EwrdWiFiSarTxPowerSet2Limit16;
        mPlatformNvsAreaProtocol.Area->EwrdWiFiSarTxPowerSet2Limit17 = mSystemConfiguration.EwrdWiFiSarTxPowerSet2Limit17;
        mPlatformNvsAreaProtocol.Area->EwrdWiFiSarTxPowerSet2Limit18 = mSystemConfiguration.EwrdWiFiSarTxPowerSet2Limit18;
        mPlatformNvsAreaProtocol.Area->EwrdWiFiSarTxPowerSet2Limit19 = mSystemConfiguration.EwrdWiFiSarTxPowerSet2Limit19;
        mPlatformNvsAreaProtocol.Area->EwrdWiFiSarTxPowerSet2Limit20 = mSystemConfiguration.EwrdWiFiSarTxPowerSet2Limit20;
        mPlatformNvsAreaProtocol.Area->EwrdWiFiSarTxPowerSet2Limit21 = mSystemConfiguration.EwrdWiFiSarTxPowerSet2Limit21;
        mPlatformNvsAreaProtocol.Area->EwrdWiFiSarTxPowerSet2Limit22 = mSystemConfiguration.EwrdWiFiSarTxPowerSet2Limit22;
        mPlatformNvsAreaProtocol.Area->EwrdCdbWiFiSarTxPowerSet2Limit1  = mSystemConfiguration.EwrdCdbWiFiSarTxPowerSet2Limit1;
        mPlatformNvsAreaProtocol.Area->EwrdCdbWiFiSarTxPowerSet2Limit2  = mSystemConfiguration.EwrdCdbWiFiSarTxPowerSet2Limit2;
        mPlatformNvsAreaProtocol.Area->EwrdCdbWiFiSarTxPowerSet2Limit3  = mSystemConfiguration.EwrdCdbWiFiSarTxPowerSet2Limit3;
        mPlatformNvsAreaProtocol.Area->EwrdCdbWiFiSarTxPowerSet2Limit4  = mSystemConfiguration.EwrdCdbWiFiSarTxPowerSet2Limit4;
        mPlatformNvsAreaProtocol.Area->EwrdCdbWiFiSarTxPowerSet2Limit5  = mSystemConfiguration.EwrdCdbWiFiSarTxPowerSet2Limit5;
        mPlatformNvsAreaProtocol.Area->EwrdCdbWiFiSarTxPowerSet2Limit6  = mSystemConfiguration.EwrdCdbWiFiSarTxPowerSet2Limit6;
        mPlatformNvsAreaProtocol.Area->EwrdCdbWiFiSarTxPowerSet2Limit7  = mSystemConfiguration.EwrdCdbWiFiSarTxPowerSet2Limit7;
        mPlatformNvsAreaProtocol.Area->EwrdCdbWiFiSarTxPowerSet2Limit8  = mSystemConfiguration.EwrdCdbWiFiSarTxPowerSet2Limit8;
        mPlatformNvsAreaProtocol.Area->EwrdCdbWiFiSarTxPowerSet2Limit9  = mSystemConfiguration.EwrdCdbWiFiSarTxPowerSet2Limit9;
        mPlatformNvsAreaProtocol.Area->EwrdCdbWiFiSarTxPowerSet2Limit10 = mSystemConfiguration.EwrdCdbWiFiSarTxPowerSet2Limit10;
        mPlatformNvsAreaProtocol.Area->EwrdCdbWiFiSarTxPowerSet2Limit11 = mSystemConfiguration.EwrdCdbWiFiSarTxPowerSet2Limit11;
        mPlatformNvsAreaProtocol.Area->EwrdCdbWiFiSarTxPowerSet2Limit12 = mSystemConfiguration.EwrdCdbWiFiSarTxPowerSet2Limit12;
        mPlatformNvsAreaProtocol.Area->EwrdCdbWiFiSarTxPowerSet2Limit13 = mSystemConfiguration.EwrdCdbWiFiSarTxPowerSet2Limit13;
        mPlatformNvsAreaProtocol.Area->EwrdCdbWiFiSarTxPowerSet2Limit14 = mSystemConfiguration.EwrdCdbWiFiSarTxPowerSet2Limit14;
        mPlatformNvsAreaProtocol.Area->EwrdCdbWiFiSarTxPowerSet2Limit15 = mSystemConfiguration.EwrdCdbWiFiSarTxPowerSet2Limit15;
        mPlatformNvsAreaProtocol.Area->EwrdCdbWiFiSarTxPowerSet2Limit16 = mSystemConfiguration.EwrdCdbWiFiSarTxPowerSet2Limit16;
        mPlatformNvsAreaProtocol.Area->EwrdCdbWiFiSarTxPowerSet2Limit17 = mSystemConfiguration.EwrdCdbWiFiSarTxPowerSet2Limit17;
        mPlatformNvsAreaProtocol.Area->EwrdCdbWiFiSarTxPowerSet2Limit18 = mSystemConfiguration.EwrdCdbWiFiSarTxPowerSet2Limit18;
        mPlatformNvsAreaProtocol.Area->EwrdCdbWiFiSarTxPowerSet2Limit19 = mSystemConfiguration.EwrdCdbWiFiSarTxPowerSet2Limit19;
        mPlatformNvsAreaProtocol.Area->EwrdCdbWiFiSarTxPowerSet2Limit20 = mSystemConfiguration.EwrdCdbWiFiSarTxPowerSet2Limit20;
        mPlatformNvsAreaProtocol.Area->EwrdCdbWiFiSarTxPowerSet2Limit21 = mSystemConfiguration.EwrdCdbWiFiSarTxPowerSet2Limit21;
        mPlatformNvsAreaProtocol.Area->EwrdCdbWiFiSarTxPowerSet2Limit22 = mSystemConfiguration.EwrdCdbWiFiSarTxPowerSet2Limit22;

        mPlatformNvsAreaProtocol.Area->EwrdWiFiSarTxPowerSet3Limit1  = mSystemConfiguration.EwrdWiFiSarTxPowerSet3Limit1;
        mPlatformNvsAreaProtocol.Area->EwrdWiFiSarTxPowerSet3Limit2  = mSystemConfiguration.EwrdWiFiSarTxPowerSet3Limit2;
        mPlatformNvsAreaProtocol.Area->EwrdWiFiSarTxPowerSet3Limit3  = mSystemConfiguration.EwrdWiFiSarTxPowerSet3Limit3;
        mPlatformNvsAreaProtocol.Area->EwrdWiFiSarTxPowerSet3Limit4  = mSystemConfiguration.EwrdWiFiSarTxPowerSet3Limit4;
        mPlatformNvsAreaProtocol.Area->EwrdWiFiSarTxPowerSet3Limit5  = mSystemConfiguration.EwrdWiFiSarTxPowerSet3Limit5;
        mPlatformNvsAreaProtocol.Area->EwrdWiFiSarTxPowerSet3Limit6  = mSystemConfiguration.EwrdWiFiSarTxPowerSet3Limit6;
        mPlatformNvsAreaProtocol.Area->EwrdWiFiSarTxPowerSet3Limit7  = mSystemConfiguration.EwrdWiFiSarTxPowerSet3Limit7;
        mPlatformNvsAreaProtocol.Area->EwrdWiFiSarTxPowerSet3Limit8  = mSystemConfiguration.EwrdWiFiSarTxPowerSet3Limit8;
        mPlatformNvsAreaProtocol.Area->EwrdWiFiSarTxPowerSet3Limit9  = mSystemConfiguration.EwrdWiFiSarTxPowerSet3Limit9;
        mPlatformNvsAreaProtocol.Area->EwrdWiFiSarTxPowerSet3Limit10 = mSystemConfiguration.EwrdWiFiSarTxPowerSet3Limit10;
        mPlatformNvsAreaProtocol.Area->EwrdWiFiSarTxPowerSet3Limit11 = mSystemConfiguration.EwrdWiFiSarTxPowerSet3Limit11;
        mPlatformNvsAreaProtocol.Area->EwrdWiFiSarTxPowerSet3Limit12 = mSystemConfiguration.EwrdWiFiSarTxPowerSet3Limit12;
        mPlatformNvsAreaProtocol.Area->EwrdWiFiSarTxPowerSet3Limit13 = mSystemConfiguration.EwrdWiFiSarTxPowerSet3Limit13;
        mPlatformNvsAreaProtocol.Area->EwrdWiFiSarTxPowerSet3Limit14 = mSystemConfiguration.EwrdWiFiSarTxPowerSet3Limit14;
        mPlatformNvsAreaProtocol.Area->EwrdWiFiSarTxPowerSet3Limit15 = mSystemConfiguration.EwrdWiFiSarTxPowerSet3Limit15;
        mPlatformNvsAreaProtocol.Area->EwrdWiFiSarTxPowerSet3Limit16 = mSystemConfiguration.EwrdWiFiSarTxPowerSet3Limit16;
        mPlatformNvsAreaProtocol.Area->EwrdWiFiSarTxPowerSet3Limit17 = mSystemConfiguration.EwrdWiFiSarTxPowerSet3Limit17;
        mPlatformNvsAreaProtocol.Area->EwrdWiFiSarTxPowerSet3Limit18 = mSystemConfiguration.EwrdWiFiSarTxPowerSet3Limit18;
        mPlatformNvsAreaProtocol.Area->EwrdWiFiSarTxPowerSet3Limit19 = mSystemConfiguration.EwrdWiFiSarTxPowerSet3Limit19;
        mPlatformNvsAreaProtocol.Area->EwrdWiFiSarTxPowerSet3Limit20 = mSystemConfiguration.EwrdWiFiSarTxPowerSet3Limit20;
        mPlatformNvsAreaProtocol.Area->EwrdWiFiSarTxPowerSet3Limit21 = mSystemConfiguration.EwrdWiFiSarTxPowerSet3Limit21;
        mPlatformNvsAreaProtocol.Area->EwrdWiFiSarTxPowerSet3Limit22 = mSystemConfiguration.EwrdWiFiSarTxPowerSet3Limit22;
        mPlatformNvsAreaProtocol.Area->EwrdCdbWiFiSarTxPowerSet3Limit1  = mSystemConfiguration.EwrdCdbWiFiSarTxPowerSet3Limit1;
        mPlatformNvsAreaProtocol.Area->EwrdCdbWiFiSarTxPowerSet3Limit2  = mSystemConfiguration.EwrdCdbWiFiSarTxPowerSet3Limit2;
        mPlatformNvsAreaProtocol.Area->EwrdCdbWiFiSarTxPowerSet3Limit3  = mSystemConfiguration.EwrdCdbWiFiSarTxPowerSet3Limit3;
        mPlatformNvsAreaProtocol.Area->EwrdCdbWiFiSarTxPowerSet3Limit4  = mSystemConfiguration.EwrdCdbWiFiSarTxPowerSet3Limit4;
        mPlatformNvsAreaProtocol.Area->EwrdCdbWiFiSarTxPowerSet3Limit5  = mSystemConfiguration.EwrdCdbWiFiSarTxPowerSet3Limit5;
        mPlatformNvsAreaProtocol.Area->EwrdCdbWiFiSarTxPowerSet3Limit6  = mSystemConfiguration.EwrdCdbWiFiSarTxPowerSet3Limit6;
        mPlatformNvsAreaProtocol.Area->EwrdCdbWiFiSarTxPowerSet3Limit7  = mSystemConfiguration.EwrdCdbWiFiSarTxPowerSet3Limit7;
        mPlatformNvsAreaProtocol.Area->EwrdCdbWiFiSarTxPowerSet3Limit8  = mSystemConfiguration.EwrdCdbWiFiSarTxPowerSet3Limit8;
        mPlatformNvsAreaProtocol.Area->EwrdCdbWiFiSarTxPowerSet3Limit9  = mSystemConfiguration.EwrdCdbWiFiSarTxPowerSet3Limit9;
        mPlatformNvsAreaProtocol.Area->EwrdCdbWiFiSarTxPowerSet3Limit10 = mSystemConfiguration.EwrdCdbWiFiSarTxPowerSet3Limit10;
        mPlatformNvsAreaProtocol.Area->EwrdCdbWiFiSarTxPowerSet3Limit11 = mSystemConfiguration.EwrdCdbWiFiSarTxPowerSet3Limit11;
        mPlatformNvsAreaProtocol.Area->EwrdCdbWiFiSarTxPowerSet3Limit12 = mSystemConfiguration.EwrdCdbWiFiSarTxPowerSet3Limit12;
        mPlatformNvsAreaProtocol.Area->EwrdCdbWiFiSarTxPowerSet3Limit13 = mSystemConfiguration.EwrdCdbWiFiSarTxPowerSet3Limit13;
        mPlatformNvsAreaProtocol.Area->EwrdCdbWiFiSarTxPowerSet3Limit14 = mSystemConfiguration.EwrdCdbWiFiSarTxPowerSet3Limit14;
        mPlatformNvsAreaProtocol.Area->EwrdCdbWiFiSarTxPowerSet3Limit15 = mSystemConfiguration.EwrdCdbWiFiSarTxPowerSet3Limit15;
        mPlatformNvsAreaProtocol.Area->EwrdCdbWiFiSarTxPowerSet3Limit16 = mSystemConfiguration.EwrdCdbWiFiSarTxPowerSet3Limit16;
        mPlatformNvsAreaProtocol.Area->EwrdCdbWiFiSarTxPowerSet3Limit17 = mSystemConfiguration.EwrdCdbWiFiSarTxPowerSet3Limit17;
        mPlatformNvsAreaProtocol.Area->EwrdCdbWiFiSarTxPowerSet3Limit18 = mSystemConfiguration.EwrdCdbWiFiSarTxPowerSet3Limit18;
        mPlatformNvsAreaProtocol.Area->EwrdCdbWiFiSarTxPowerSet3Limit19 = mSystemConfiguration.EwrdCdbWiFiSarTxPowerSet3Limit19;
        mPlatformNvsAreaProtocol.Area->EwrdCdbWiFiSarTxPowerSet3Limit20 = mSystemConfiguration.EwrdCdbWiFiSarTxPowerSet3Limit20;
        mPlatformNvsAreaProtocol.Area->EwrdCdbWiFiSarTxPowerSet3Limit21 = mSystemConfiguration.EwrdCdbWiFiSarTxPowerSet3Limit21;
        mPlatformNvsAreaProtocol.Area->EwrdCdbWiFiSarTxPowerSet3Limit22 = mSystemConfiguration.EwrdCdbWiFiSarTxPowerSet3Limit22;

        mPlatformNvsAreaProtocol.Area->EwrdWiFiSarTxPowerSet4Limit1  = mSystemConfiguration.EwrdWiFiSarTxPowerSet4Limit1;
        mPlatformNvsAreaProtocol.Area->EwrdWiFiSarTxPowerSet4Limit2  = mSystemConfiguration.EwrdWiFiSarTxPowerSet4Limit2;
        mPlatformNvsAreaProtocol.Area->EwrdWiFiSarTxPowerSet4Limit3  = mSystemConfiguration.EwrdWiFiSarTxPowerSet4Limit3;
        mPlatformNvsAreaProtocol.Area->EwrdWiFiSarTxPowerSet4Limit4  = mSystemConfiguration.EwrdWiFiSarTxPowerSet4Limit4;
        mPlatformNvsAreaProtocol.Area->EwrdWiFiSarTxPowerSet4Limit5  = mSystemConfiguration.EwrdWiFiSarTxPowerSet4Limit5;
        mPlatformNvsAreaProtocol.Area->EwrdWiFiSarTxPowerSet4Limit6  = mSystemConfiguration.EwrdWiFiSarTxPowerSet4Limit6;
        mPlatformNvsAreaProtocol.Area->EwrdWiFiSarTxPowerSet4Limit7  = mSystemConfiguration.EwrdWiFiSarTxPowerSet4Limit7;
        mPlatformNvsAreaProtocol.Area->EwrdWiFiSarTxPowerSet4Limit8  = mSystemConfiguration.EwrdWiFiSarTxPowerSet4Limit8;
        mPlatformNvsAreaProtocol.Area->EwrdWiFiSarTxPowerSet4Limit9  = mSystemConfiguration.EwrdWiFiSarTxPowerSet4Limit9;
        mPlatformNvsAreaProtocol.Area->EwrdWiFiSarTxPowerSet4Limit10 = mSystemConfiguration.EwrdWiFiSarTxPowerSet4Limit10;
        mPlatformNvsAreaProtocol.Area->EwrdWiFiSarTxPowerSet4Limit11 = mSystemConfiguration.EwrdWiFiSarTxPowerSet4Limit11;
        mPlatformNvsAreaProtocol.Area->EwrdWiFiSarTxPowerSet4Limit12 = mSystemConfiguration.EwrdWiFiSarTxPowerSet4Limit12;
        mPlatformNvsAreaProtocol.Area->EwrdWiFiSarTxPowerSet4Limit13 = mSystemConfiguration.EwrdWiFiSarTxPowerSet4Limit13;
        mPlatformNvsAreaProtocol.Area->EwrdWiFiSarTxPowerSet4Limit14 = mSystemConfiguration.EwrdWiFiSarTxPowerSet4Limit14;
        mPlatformNvsAreaProtocol.Area->EwrdWiFiSarTxPowerSet4Limit15 = mSystemConfiguration.EwrdWiFiSarTxPowerSet4Limit15;
        mPlatformNvsAreaProtocol.Area->EwrdWiFiSarTxPowerSet4Limit16 = mSystemConfiguration.EwrdWiFiSarTxPowerSet4Limit16;
        mPlatformNvsAreaProtocol.Area->EwrdWiFiSarTxPowerSet4Limit17 = mSystemConfiguration.EwrdWiFiSarTxPowerSet4Limit17;
        mPlatformNvsAreaProtocol.Area->EwrdWiFiSarTxPowerSet4Limit18 = mSystemConfiguration.EwrdWiFiSarTxPowerSet4Limit18;
        mPlatformNvsAreaProtocol.Area->EwrdWiFiSarTxPowerSet4Limit19 = mSystemConfiguration.EwrdWiFiSarTxPowerSet4Limit19;
        mPlatformNvsAreaProtocol.Area->EwrdWiFiSarTxPowerSet4Limit20 = mSystemConfiguration.EwrdWiFiSarTxPowerSet4Limit20;
        mPlatformNvsAreaProtocol.Area->EwrdWiFiSarTxPowerSet4Limit21 = mSystemConfiguration.EwrdWiFiSarTxPowerSet4Limit21;
        mPlatformNvsAreaProtocol.Area->EwrdWiFiSarTxPowerSet4Limit22 = mSystemConfiguration.EwrdWiFiSarTxPowerSet4Limit22;
        mPlatformNvsAreaProtocol.Area->EwrdCdbWiFiSarTxPowerSet4Limit1  = mSystemConfiguration.EwrdCdbWiFiSarTxPowerSet4Limit1;
        mPlatformNvsAreaProtocol.Area->EwrdCdbWiFiSarTxPowerSet4Limit2  = mSystemConfiguration.EwrdCdbWiFiSarTxPowerSet4Limit2;
        mPlatformNvsAreaProtocol.Area->EwrdCdbWiFiSarTxPowerSet4Limit3  = mSystemConfiguration.EwrdCdbWiFiSarTxPowerSet4Limit3;
        mPlatformNvsAreaProtocol.Area->EwrdCdbWiFiSarTxPowerSet4Limit4  = mSystemConfiguration.EwrdCdbWiFiSarTxPowerSet4Limit4;
        mPlatformNvsAreaProtocol.Area->EwrdCdbWiFiSarTxPowerSet4Limit5  = mSystemConfiguration.EwrdCdbWiFiSarTxPowerSet4Limit5;
        mPlatformNvsAreaProtocol.Area->EwrdCdbWiFiSarTxPowerSet4Limit6  = mSystemConfiguration.EwrdCdbWiFiSarTxPowerSet4Limit6;
        mPlatformNvsAreaProtocol.Area->EwrdCdbWiFiSarTxPowerSet4Limit7  = mSystemConfiguration.EwrdCdbWiFiSarTxPowerSet4Limit7;
        mPlatformNvsAreaProtocol.Area->EwrdCdbWiFiSarTxPowerSet4Limit8  = mSystemConfiguration.EwrdCdbWiFiSarTxPowerSet4Limit8;
        mPlatformNvsAreaProtocol.Area->EwrdCdbWiFiSarTxPowerSet4Limit9  = mSystemConfiguration.EwrdCdbWiFiSarTxPowerSet4Limit9;
        mPlatformNvsAreaProtocol.Area->EwrdCdbWiFiSarTxPowerSet4Limit10 = mSystemConfiguration.EwrdCdbWiFiSarTxPowerSet4Limit10;
        mPlatformNvsAreaProtocol.Area->EwrdCdbWiFiSarTxPowerSet4Limit11 = mSystemConfiguration.EwrdCdbWiFiSarTxPowerSet4Limit11;
        mPlatformNvsAreaProtocol.Area->EwrdCdbWiFiSarTxPowerSet4Limit12 = mSystemConfiguration.EwrdCdbWiFiSarTxPowerSet4Limit12;
        mPlatformNvsAreaProtocol.Area->EwrdCdbWiFiSarTxPowerSet4Limit13 = mSystemConfiguration.EwrdCdbWiFiSarTxPowerSet4Limit13;
        mPlatformNvsAreaProtocol.Area->EwrdCdbWiFiSarTxPowerSet4Limit14 = mSystemConfiguration.EwrdCdbWiFiSarTxPowerSet4Limit14;
        mPlatformNvsAreaProtocol.Area->EwrdCdbWiFiSarTxPowerSet4Limit15 = mSystemConfiguration.EwrdCdbWiFiSarTxPowerSet4Limit15;
        mPlatformNvsAreaProtocol.Area->EwrdCdbWiFiSarTxPowerSet4Limit16 = mSystemConfiguration.EwrdCdbWiFiSarTxPowerSet4Limit16;
        mPlatformNvsAreaProtocol.Area->EwrdCdbWiFiSarTxPowerSet4Limit17 = mSystemConfiguration.EwrdCdbWiFiSarTxPowerSet4Limit17;
        mPlatformNvsAreaProtocol.Area->EwrdCdbWiFiSarTxPowerSet4Limit18 = mSystemConfiguration.EwrdCdbWiFiSarTxPowerSet4Limit18;
        mPlatformNvsAreaProtocol.Area->EwrdCdbWiFiSarTxPowerSet4Limit19 = mSystemConfiguration.EwrdCdbWiFiSarTxPowerSet4Limit19;
        mPlatformNvsAreaProtocol.Area->EwrdCdbWiFiSarTxPowerSet4Limit20 = mSystemConfiguration.EwrdCdbWiFiSarTxPowerSet4Limit20;
        mPlatformNvsAreaProtocol.Area->EwrdCdbWiFiSarTxPowerSet4Limit21 = mSystemConfiguration.EwrdCdbWiFiSarTxPowerSet4Limit21;
        mPlatformNvsAreaProtocol.Area->EwrdCdbWiFiSarTxPowerSet4Limit22 = mSystemConfiguration.EwrdCdbWiFiSarTxPowerSet4Limit22;

    } else {
        mPlatformNvsAreaProtocol.Area->EwrdWiFiSarTxPowerSet2Limit1  = 0x0;
        mPlatformNvsAreaProtocol.Area->EwrdWiFiSarTxPowerSet2Limit2  = 0x0;
        mPlatformNvsAreaProtocol.Area->EwrdWiFiSarTxPowerSet2Limit3  = 0x0;
        mPlatformNvsAreaProtocol.Area->EwrdWiFiSarTxPowerSet2Limit4  = 0x0;
        mPlatformNvsAreaProtocol.Area->EwrdWiFiSarTxPowerSet2Limit5  = 0x0;
        mPlatformNvsAreaProtocol.Area->EwrdWiFiSarTxPowerSet2Limit6  = 0x0;
        mPlatformNvsAreaProtocol.Area->EwrdWiFiSarTxPowerSet2Limit7  = 0x0;
        mPlatformNvsAreaProtocol.Area->EwrdWiFiSarTxPowerSet2Limit8  = 0x0;
        mPlatformNvsAreaProtocol.Area->EwrdWiFiSarTxPowerSet2Limit9  = 0x0;
        mPlatformNvsAreaProtocol.Area->EwrdWiFiSarTxPowerSet2Limit10 = 0x0;
        mPlatformNvsAreaProtocol.Area->EwrdWiFiSarTxPowerSet2Limit11 = 0x0;
        mPlatformNvsAreaProtocol.Area->EwrdWiFiSarTxPowerSet2Limit12 = 0x0;
        mPlatformNvsAreaProtocol.Area->EwrdWiFiSarTxPowerSet2Limit13 = 0x0;
        mPlatformNvsAreaProtocol.Area->EwrdWiFiSarTxPowerSet2Limit14 = 0x0;
        mPlatformNvsAreaProtocol.Area->EwrdWiFiSarTxPowerSet2Limit15 = 0x0;
        mPlatformNvsAreaProtocol.Area->EwrdWiFiSarTxPowerSet2Limit16 = 0x0;
        mPlatformNvsAreaProtocol.Area->EwrdWiFiSarTxPowerSet2Limit17 = 0x0;
        mPlatformNvsAreaProtocol.Area->EwrdWiFiSarTxPowerSet2Limit18 = 0x0;
        mPlatformNvsAreaProtocol.Area->EwrdWiFiSarTxPowerSet2Limit19 = 0x0;
        mPlatformNvsAreaProtocol.Area->EwrdWiFiSarTxPowerSet2Limit20 = 0x0;
        mPlatformNvsAreaProtocol.Area->EwrdWiFiSarTxPowerSet2Limit21 = 0x0;
        mPlatformNvsAreaProtocol.Area->EwrdWiFiSarTxPowerSet2Limit22 = 0x0;
        mPlatformNvsAreaProtocol.Area->EwrdCdbWiFiSarTxPowerSet2Limit1  = 0x00;
        mPlatformNvsAreaProtocol.Area->EwrdCdbWiFiSarTxPowerSet2Limit2  = 0x00;
        mPlatformNvsAreaProtocol.Area->EwrdCdbWiFiSarTxPowerSet2Limit3  = 0x00;
        mPlatformNvsAreaProtocol.Area->EwrdCdbWiFiSarTxPowerSet2Limit4  = 0x00;
        mPlatformNvsAreaProtocol.Area->EwrdCdbWiFiSarTxPowerSet2Limit5  = 0x00;
        mPlatformNvsAreaProtocol.Area->EwrdCdbWiFiSarTxPowerSet2Limit6  = 0x00;
        mPlatformNvsAreaProtocol.Area->EwrdCdbWiFiSarTxPowerSet2Limit7  = 0x00;
        mPlatformNvsAreaProtocol.Area->EwrdCdbWiFiSarTxPowerSet2Limit8  = 0x00;
        mPlatformNvsAreaProtocol.Area->EwrdCdbWiFiSarTxPowerSet2Limit9  = 0x00;
        mPlatformNvsAreaProtocol.Area->EwrdCdbWiFiSarTxPowerSet2Limit10 = 0x00;
        mPlatformNvsAreaProtocol.Area->EwrdCdbWiFiSarTxPowerSet2Limit11 = 0x00;
        mPlatformNvsAreaProtocol.Area->EwrdCdbWiFiSarTxPowerSet2Limit12 = 0x00;
        mPlatformNvsAreaProtocol.Area->EwrdCdbWiFiSarTxPowerSet2Limit13 = 0x00;
        mPlatformNvsAreaProtocol.Area->EwrdCdbWiFiSarTxPowerSet2Limit14 = 0x00;
        mPlatformNvsAreaProtocol.Area->EwrdCdbWiFiSarTxPowerSet2Limit15 = 0x00;
        mPlatformNvsAreaProtocol.Area->EwrdCdbWiFiSarTxPowerSet2Limit16 = 0x00;
        mPlatformNvsAreaProtocol.Area->EwrdCdbWiFiSarTxPowerSet2Limit17 = 0x00;
        mPlatformNvsAreaProtocol.Area->EwrdCdbWiFiSarTxPowerSet2Limit18 = 0x00;
        mPlatformNvsAreaProtocol.Area->EwrdCdbWiFiSarTxPowerSet2Limit19 = 0x00;
        mPlatformNvsAreaProtocol.Area->EwrdCdbWiFiSarTxPowerSet2Limit20 = 0x00;
        mPlatformNvsAreaProtocol.Area->EwrdCdbWiFiSarTxPowerSet2Limit21 = 0x00;
        mPlatformNvsAreaProtocol.Area->EwrdCdbWiFiSarTxPowerSet2Limit22 = 0x00;

        mPlatformNvsAreaProtocol.Area->EwrdWiFiSarTxPowerSet3Limit1  = 0x0;
        mPlatformNvsAreaProtocol.Area->EwrdWiFiSarTxPowerSet3Limit2  = 0x0;
        mPlatformNvsAreaProtocol.Area->EwrdWiFiSarTxPowerSet3Limit3  = 0x0;
        mPlatformNvsAreaProtocol.Area->EwrdWiFiSarTxPowerSet3Limit4  = 0x0;
        mPlatformNvsAreaProtocol.Area->EwrdWiFiSarTxPowerSet3Limit5  = 0x0;
        mPlatformNvsAreaProtocol.Area->EwrdWiFiSarTxPowerSet3Limit6  = 0x0;
        mPlatformNvsAreaProtocol.Area->EwrdWiFiSarTxPowerSet3Limit7  = 0x0;
        mPlatformNvsAreaProtocol.Area->EwrdWiFiSarTxPowerSet3Limit8  = 0x0;
        mPlatformNvsAreaProtocol.Area->EwrdWiFiSarTxPowerSet3Limit9  = 0x0;
        mPlatformNvsAreaProtocol.Area->EwrdWiFiSarTxPowerSet3Limit10 = 0x0;
        mPlatformNvsAreaProtocol.Area->EwrdWiFiSarTxPowerSet3Limit11 = 0x0;
        mPlatformNvsAreaProtocol.Area->EwrdWiFiSarTxPowerSet3Limit12 = 0x0;
        mPlatformNvsAreaProtocol.Area->EwrdWiFiSarTxPowerSet3Limit13 = 0x0;
        mPlatformNvsAreaProtocol.Area->EwrdWiFiSarTxPowerSet3Limit14 = 0x0;
        mPlatformNvsAreaProtocol.Area->EwrdWiFiSarTxPowerSet3Limit15 = 0x0;
        mPlatformNvsAreaProtocol.Area->EwrdWiFiSarTxPowerSet3Limit16 = 0x0;
        mPlatformNvsAreaProtocol.Area->EwrdWiFiSarTxPowerSet3Limit17 = 0x0;
        mPlatformNvsAreaProtocol.Area->EwrdWiFiSarTxPowerSet3Limit18 = 0x0;
        mPlatformNvsAreaProtocol.Area->EwrdWiFiSarTxPowerSet3Limit19 = 0x0;
        mPlatformNvsAreaProtocol.Area->EwrdWiFiSarTxPowerSet3Limit20 = 0x0;
        mPlatformNvsAreaProtocol.Area->EwrdWiFiSarTxPowerSet3Limit21 = 0x0;
        mPlatformNvsAreaProtocol.Area->EwrdWiFiSarTxPowerSet3Limit22 = 0x0;
        mPlatformNvsAreaProtocol.Area->EwrdCdbWiFiSarTxPowerSet3Limit1  = 0x00;
        mPlatformNvsAreaProtocol.Area->EwrdCdbWiFiSarTxPowerSet3Limit2  = 0x00;
        mPlatformNvsAreaProtocol.Area->EwrdCdbWiFiSarTxPowerSet3Limit3  = 0x00;
        mPlatformNvsAreaProtocol.Area->EwrdCdbWiFiSarTxPowerSet3Limit4  = 0x00;
        mPlatformNvsAreaProtocol.Area->EwrdCdbWiFiSarTxPowerSet3Limit5  = 0x00;
        mPlatformNvsAreaProtocol.Area->EwrdCdbWiFiSarTxPowerSet3Limit6  = 0x00;
        mPlatformNvsAreaProtocol.Area->EwrdCdbWiFiSarTxPowerSet3Limit7  = 0x00;
        mPlatformNvsAreaProtocol.Area->EwrdCdbWiFiSarTxPowerSet3Limit8  = 0x00;
        mPlatformNvsAreaProtocol.Area->EwrdCdbWiFiSarTxPowerSet3Limit9  = 0x00;
        mPlatformNvsAreaProtocol.Area->EwrdCdbWiFiSarTxPowerSet3Limit10 = 0x00;
        mPlatformNvsAreaProtocol.Area->EwrdCdbWiFiSarTxPowerSet3Limit11 = 0x00;
        mPlatformNvsAreaProtocol.Area->EwrdCdbWiFiSarTxPowerSet3Limit12 = 0x00;
        mPlatformNvsAreaProtocol.Area->EwrdCdbWiFiSarTxPowerSet3Limit13 = 0x00;
        mPlatformNvsAreaProtocol.Area->EwrdCdbWiFiSarTxPowerSet3Limit14 = 0x00;
        mPlatformNvsAreaProtocol.Area->EwrdCdbWiFiSarTxPowerSet3Limit15 = 0x00;
        mPlatformNvsAreaProtocol.Area->EwrdCdbWiFiSarTxPowerSet3Limit16 = 0x00;
        mPlatformNvsAreaProtocol.Area->EwrdCdbWiFiSarTxPowerSet3Limit17 = 0x00;
        mPlatformNvsAreaProtocol.Area->EwrdCdbWiFiSarTxPowerSet3Limit18 = 0x00;
        mPlatformNvsAreaProtocol.Area->EwrdCdbWiFiSarTxPowerSet3Limit19 = 0x00;
        mPlatformNvsAreaProtocol.Area->EwrdCdbWiFiSarTxPowerSet3Limit20 = 0x00;
        mPlatformNvsAreaProtocol.Area->EwrdCdbWiFiSarTxPowerSet3Limit21 = 0x00;
        mPlatformNvsAreaProtocol.Area->EwrdCdbWiFiSarTxPowerSet3Limit22 = 0x00;

        mPlatformNvsAreaProtocol.Area->EwrdWiFiSarTxPowerSet4Limit1  = 0x0;
        mPlatformNvsAreaProtocol.Area->EwrdWiFiSarTxPowerSet4Limit2  = 0x0;
        mPlatformNvsAreaProtocol.Area->EwrdWiFiSarTxPowerSet4Limit3  = 0x0;
        mPlatformNvsAreaProtocol.Area->EwrdWiFiSarTxPowerSet4Limit4  = 0x0;
        mPlatformNvsAreaProtocol.Area->EwrdWiFiSarTxPowerSet4Limit5  = 0x0;
        mPlatformNvsAreaProtocol.Area->EwrdWiFiSarTxPowerSet4Limit6  = 0x0;
        mPlatformNvsAreaProtocol.Area->EwrdWiFiSarTxPowerSet4Limit7  = 0x0;
        mPlatformNvsAreaProtocol.Area->EwrdWiFiSarTxPowerSet4Limit8  = 0x0;
        mPlatformNvsAreaProtocol.Area->EwrdWiFiSarTxPowerSet4Limit9  = 0x0;
        mPlatformNvsAreaProtocol.Area->EwrdWiFiSarTxPowerSet4Limit10 = 0x0;
        mPlatformNvsAreaProtocol.Area->EwrdWiFiSarTxPowerSet4Limit11 = 0x0;
        mPlatformNvsAreaProtocol.Area->EwrdWiFiSarTxPowerSet4Limit12 = 0x0;
        mPlatformNvsAreaProtocol.Area->EwrdWiFiSarTxPowerSet4Limit13 = 0x0;
        mPlatformNvsAreaProtocol.Area->EwrdWiFiSarTxPowerSet4Limit14 = 0x0;
        mPlatformNvsAreaProtocol.Area->EwrdWiFiSarTxPowerSet4Limit15 = 0x0;
        mPlatformNvsAreaProtocol.Area->EwrdWiFiSarTxPowerSet4Limit16 = 0x0;
        mPlatformNvsAreaProtocol.Area->EwrdWiFiSarTxPowerSet4Limit17 = 0x0;
        mPlatformNvsAreaProtocol.Area->EwrdWiFiSarTxPowerSet4Limit18 = 0x0;
        mPlatformNvsAreaProtocol.Area->EwrdWiFiSarTxPowerSet4Limit19 = 0x0;
        mPlatformNvsAreaProtocol.Area->EwrdWiFiSarTxPowerSet4Limit20 = 0x0;
        mPlatformNvsAreaProtocol.Area->EwrdWiFiSarTxPowerSet4Limit21 = 0x0;
        mPlatformNvsAreaProtocol.Area->EwrdWiFiSarTxPowerSet4Limit22 = 0x0;
        mPlatformNvsAreaProtocol.Area->EwrdCdbWiFiSarTxPowerSet4Limit1  = 0x00;
        mPlatformNvsAreaProtocol.Area->EwrdCdbWiFiSarTxPowerSet4Limit2  = 0x00;
        mPlatformNvsAreaProtocol.Area->EwrdCdbWiFiSarTxPowerSet4Limit3  = 0x00;
        mPlatformNvsAreaProtocol.Area->EwrdCdbWiFiSarTxPowerSet4Limit4  = 0x00;
        mPlatformNvsAreaProtocol.Area->EwrdCdbWiFiSarTxPowerSet4Limit5  = 0x00;
        mPlatformNvsAreaProtocol.Area->EwrdCdbWiFiSarTxPowerSet4Limit6  = 0x00;
        mPlatformNvsAreaProtocol.Area->EwrdCdbWiFiSarTxPowerSet4Limit7  = 0x00;
        mPlatformNvsAreaProtocol.Area->EwrdCdbWiFiSarTxPowerSet4Limit8  = 0x00;
        mPlatformNvsAreaProtocol.Area->EwrdCdbWiFiSarTxPowerSet4Limit9  = 0x00;
        mPlatformNvsAreaProtocol.Area->EwrdCdbWiFiSarTxPowerSet4Limit10 = 0x00;
        mPlatformNvsAreaProtocol.Area->EwrdCdbWiFiSarTxPowerSet4Limit11 = 0x00;
        mPlatformNvsAreaProtocol.Area->EwrdCdbWiFiSarTxPowerSet4Limit12 = 0x00;
        mPlatformNvsAreaProtocol.Area->EwrdCdbWiFiSarTxPowerSet4Limit13 = 0x00;
        mPlatformNvsAreaProtocol.Area->EwrdCdbWiFiSarTxPowerSet4Limit14 = 0x00;
        mPlatformNvsAreaProtocol.Area->EwrdCdbWiFiSarTxPowerSet4Limit15 = 0x00;
        mPlatformNvsAreaProtocol.Area->EwrdCdbWiFiSarTxPowerSet4Limit16 = 0x00;
        mPlatformNvsAreaProtocol.Area->EwrdCdbWiFiSarTxPowerSet4Limit17 = 0x00;
        mPlatformNvsAreaProtocol.Area->EwrdCdbWiFiSarTxPowerSet4Limit18 = 0x00;
        mPlatformNvsAreaProtocol.Area->EwrdCdbWiFiSarTxPowerSet4Limit19 = 0x00;
        mPlatformNvsAreaProtocol.Area->EwrdCdbWiFiSarTxPowerSet4Limit20 = 0x00;
        mPlatformNvsAreaProtocol.Area->EwrdCdbWiFiSarTxPowerSet4Limit21 = 0x00;
        mPlatformNvsAreaProtocol.Area->EwrdCdbWiFiSarTxPowerSet4Limit22 = 0x00;
    }
    mPlatformNvsAreaProtocol.Area->WiFiDynamicSarAntennaACurrentSet   = mSystemConfiguration.WiFiDynamicSarAntennaACurrentSet;
    mPlatformNvsAreaProtocol.Area->WiFiDynamicSarAntennaBCurrentSet   = mSystemConfiguration.WiFiDynamicSarAntennaBCurrentSet;

    mPlatformNvsAreaProtocol.Area->WgdsWiFiSarDeltaGroup1PowerMax1    = mSystemConfiguration.WgdsWiFiSarDeltaGroup1PowerMax1;
    mPlatformNvsAreaProtocol.Area->WgdsWiFiSarDeltaGroup1PowerChainA1 = mSystemConfiguration.WgdsWiFiSarDeltaGroup1PowerChainA1;
    mPlatformNvsAreaProtocol.Area->WgdsWiFiSarDeltaGroup1PowerChainB1 = mSystemConfiguration.WgdsWiFiSarDeltaGroup1PowerChainB1;
    mPlatformNvsAreaProtocol.Area->WgdsWiFiSarDeltaGroup1PowerMax2    = mSystemConfiguration.WgdsWiFiSarDeltaGroup1PowerMax2;
    mPlatformNvsAreaProtocol.Area->WgdsWiFiSarDeltaGroup1PowerChainA2 = mSystemConfiguration.WgdsWiFiSarDeltaGroup1PowerChainA2;
    mPlatformNvsAreaProtocol.Area->WgdsWiFiSarDeltaGroup1PowerChainB2 = mSystemConfiguration.WgdsWiFiSarDeltaGroup1PowerChainB2;
    mPlatformNvsAreaProtocol.Area->WgdsWiFiSarDeltaGroup1PowerMax3    = mSystemConfiguration.WgdsWiFiSarDeltaGroup1PowerMax3;
    mPlatformNvsAreaProtocol.Area->WgdsWiFiSarDeltaGroup1PowerChainA3 = mSystemConfiguration.WgdsWiFiSarDeltaGroup1PowerChainA3;
    mPlatformNvsAreaProtocol.Area->WgdsWiFiSarDeltaGroup1PowerChainB3 = mSystemConfiguration.WgdsWiFiSarDeltaGroup1PowerChainB3;
    mPlatformNvsAreaProtocol.Area->WgdsWiFiSarDeltaGroup2PowerMax1    = mSystemConfiguration.WgdsWiFiSarDeltaGroup2PowerMax1;
    mPlatformNvsAreaProtocol.Area->WgdsWiFiSarDeltaGroup2PowerChainA1 = mSystemConfiguration.WgdsWiFiSarDeltaGroup2PowerChainA1;
    mPlatformNvsAreaProtocol.Area->WgdsWiFiSarDeltaGroup2PowerChainB1 = mSystemConfiguration.WgdsWiFiSarDeltaGroup2PowerChainB1;
    mPlatformNvsAreaProtocol.Area->WgdsWiFiSarDeltaGroup2PowerMax2    = mSystemConfiguration.WgdsWiFiSarDeltaGroup2PowerMax2;
    mPlatformNvsAreaProtocol.Area->WgdsWiFiSarDeltaGroup2PowerChainA2 = mSystemConfiguration.WgdsWiFiSarDeltaGroup2PowerChainA2;
    mPlatformNvsAreaProtocol.Area->WgdsWiFiSarDeltaGroup2PowerChainB2 = mSystemConfiguration.WgdsWiFiSarDeltaGroup2PowerChainB2;
    mPlatformNvsAreaProtocol.Area->WgdsWiFiSarDeltaGroup2PowerMax3    = mSystemConfiguration.WgdsWiFiSarDeltaGroup2PowerMax3;
    mPlatformNvsAreaProtocol.Area->WgdsWiFiSarDeltaGroup2PowerChainA3 = mSystemConfiguration.WgdsWiFiSarDeltaGroup2PowerChainA3;
    mPlatformNvsAreaProtocol.Area->WgdsWiFiSarDeltaGroup2PowerChainB3 = mSystemConfiguration.WgdsWiFiSarDeltaGroup2PowerChainB3;
    mPlatformNvsAreaProtocol.Area->WgdsWiFiSarDeltaGroup3PowerMax1    = mSystemConfiguration.WgdsWiFiSarDeltaGroup3PowerMax1;
    mPlatformNvsAreaProtocol.Area->WgdsWiFiSarDeltaGroup3PowerChainA1 = mSystemConfiguration.WgdsWiFiSarDeltaGroup3PowerChainA1;
    mPlatformNvsAreaProtocol.Area->WgdsWiFiSarDeltaGroup3PowerChainB1 = mSystemConfiguration.WgdsWiFiSarDeltaGroup3PowerChainB1;
    mPlatformNvsAreaProtocol.Area->WgdsWiFiSarDeltaGroup3PowerMax2    = mSystemConfiguration.WgdsWiFiSarDeltaGroup3PowerMax2;
    mPlatformNvsAreaProtocol.Area->WgdsWiFiSarDeltaGroup3PowerChainA2 = mSystemConfiguration.WgdsWiFiSarDeltaGroup3PowerChainA2;
    mPlatformNvsAreaProtocol.Area->WgdsWiFiSarDeltaGroup3PowerChainB2 = mSystemConfiguration.WgdsWiFiSarDeltaGroup3PowerChainB2;
    mPlatformNvsAreaProtocol.Area->WgdsWiFiSarDeltaGroup3PowerMax3    = mSystemConfiguration.WgdsWiFiSarDeltaGroup3PowerMax3;
    mPlatformNvsAreaProtocol.Area->WgdsWiFiSarDeltaGroup3PowerChainA3 = mSystemConfiguration.WgdsWiFiSarDeltaGroup3PowerChainA3;
    mPlatformNvsAreaProtocol.Area->WgdsWiFiSarDeltaGroup3PowerChainB3 = mSystemConfiguration.WgdsWiFiSarDeltaGroup3PowerChainB3;


    mPlatformNvsAreaProtocol.Area->WifiAntGainEnale                   = mSystemConfiguration.WifiAntGainEnale;
    mPlatformNvsAreaProtocol.Area->WifiAntGainChainA1                 = mSystemConfiguration.WifiAntGain2400ChainA;
    mPlatformNvsAreaProtocol.Area->WifiAntGainChainA2                 = mSystemConfiguration.WifiAntGain5150ChainA;
    mPlatformNvsAreaProtocol.Area->WifiAntGainChainA3                 = mSystemConfiguration.WifiAntGain5350ChainA;
    mPlatformNvsAreaProtocol.Area->WifiAntGainChainA4                 = mSystemConfiguration.WifiAntGain5470ChainA;
    mPlatformNvsAreaProtocol.Area->WifiAntGainChainA5                 = mSystemConfiguration.WifiAntGain5725ChainA;
    mPlatformNvsAreaProtocol.Area->WifiAntGainChainA6                 = mSystemConfiguration.WifiAntGain5945ChainA;
    mPlatformNvsAreaProtocol.Area->WifiAntGainChainA7                 = mSystemConfiguration.WifiAntGain6165ChainA;
    mPlatformNvsAreaProtocol.Area->WifiAntGainChainA8                 = mSystemConfiguration.WifiAntGain6405ChainA;
    mPlatformNvsAreaProtocol.Area->WifiAntGainChainA9                 = mSystemConfiguration.WifiAntGain6525ChainA;
    mPlatformNvsAreaProtocol.Area->WifiAntGainChainA10                = mSystemConfiguration.WifiAntGain6705ChainA;
    mPlatformNvsAreaProtocol.Area->WifiAntGainChainA11                = mSystemConfiguration.WifiAntGain6865ChainA;
    mPlatformNvsAreaProtocol.Area->WifiAntGainChainB1                 = mSystemConfiguration.WifiAntGain2400ChainB;
    mPlatformNvsAreaProtocol.Area->WifiAntGainChainB2                 = mSystemConfiguration.WifiAntGain5150ChainB;
    mPlatformNvsAreaProtocol.Area->WifiAntGainChainB3                 = mSystemConfiguration.WifiAntGain5350ChainB;
    mPlatformNvsAreaProtocol.Area->WifiAntGainChainB4                 = mSystemConfiguration.WifiAntGain5470ChainB;
    mPlatformNvsAreaProtocol.Area->WifiAntGainChainB5                 = mSystemConfiguration.WifiAntGain5725ChainB;
    mPlatformNvsAreaProtocol.Area->WifiAntGainChainB6                 = mSystemConfiguration.WifiAntGain5945ChainB;
    mPlatformNvsAreaProtocol.Area->WifiAntGainChainB7                 = mSystemConfiguration.WifiAntGain6165ChainB;
    mPlatformNvsAreaProtocol.Area->WifiAntGainChainB8                 = mSystemConfiguration.WifiAntGain6405ChainB;
    mPlatformNvsAreaProtocol.Area->WifiAntGainChainB9                 = mSystemConfiguration.WifiAntGain6525ChainB;
    mPlatformNvsAreaProtocol.Area->WifiAntGainChainB10                = mSystemConfiguration.WifiAntGain6705ChainB;
    mPlatformNvsAreaProtocol.Area->WifiAntGainChainB11                = mSystemConfiguration.WifiAntGain6865ChainB;
    mPlatformNvsAreaProtocol.Area->AxSettingUkraine                   = mSystemConfiguration.AxSettingUkraine;
    mPlatformNvsAreaProtocol.Area->AxModeUkraine                      = mSystemConfiguration.AxModeUkraine;
    mPlatformNvsAreaProtocol.Area->AxSettingRussia                    = mSystemConfiguration.AxSettingRussia;
    mPlatformNvsAreaProtocol.Area->AxModeRussia                       = mSystemConfiguration.AxModeRussia;

    mPlatformNvsAreaProtocol.Area->WifiActiveChannelSrd               = mSystemConfiguration.WifiActiveChannelSrd;
    mPlatformNvsAreaProtocol.Area->WifiIndonesia5GhzSupport           = mSystemConfiguration.WifiIndonesia5GhzSupport;
    mPlatformNvsAreaProtocol.Area->WifiUltraHighBandSupport           = mSystemConfiguration.WifiUltraHighBandSupport;
    mPlatformNvsAreaProtocol.Area->WifiRegulatoryConfigurations       = mSystemConfiguration.WifiRegulatoryConfigurations;
    mPlatformNvsAreaProtocol.Area->WifiUartConfigurations             = mSystemConfiguration.WifiUartConfigurations;
    mPlatformNvsAreaProtocol.Area->WifiUnii4                          = mSystemConfiguration.WifiUnii4;
    mPlatformNvsAreaProtocol.Area->WifiIndoorControl                  = mSystemConfiguration.WifiIndoorControl;
    mPlatformNvsAreaProtocol.Area->WifiTASSelection                   = mSystemConfiguration.WifiTASSelection;
    mPlatformNvsAreaProtocol.Area->WifiTASListEntries                 = mSystemConfiguration.WifiTASListEntries;
    mPlatformNvsAreaProtocol.Area->WTASBlockedListEntry1              = mSystemConfiguration.WTASBlockedList[0];
    mPlatformNvsAreaProtocol.Area->WTASBlockedListEntry2              = mSystemConfiguration.WTASBlockedList[1];
    mPlatformNvsAreaProtocol.Area->WTASBlockedListEntry3              = mSystemConfiguration.WTASBlockedList[2];
    mPlatformNvsAreaProtocol.Area->WTASBlockedListEntry4              = mSystemConfiguration.WTASBlockedList[3];
    mPlatformNvsAreaProtocol.Area->WTASBlockedListEntry5              = mSystemConfiguration.WTASBlockedList[4];
    mPlatformNvsAreaProtocol.Area->WTASBlockedListEntry6              = mSystemConfiguration.WTASBlockedList[5];
    mPlatformNvsAreaProtocol.Area->WTASBlockedListEntry7              = mSystemConfiguration.WTASBlockedList[6];
    mPlatformNvsAreaProtocol.Area->WTASBlockedListEntry8              = mSystemConfiguration.WTASBlockedList[7];
    mPlatformNvsAreaProtocol.Area->WTASBlockedListEntry9              = mSystemConfiguration.WTASBlockedList[8];
    mPlatformNvsAreaProtocol.Area->WTASBlockedListEntry10             = mSystemConfiguration.WTASBlockedList[9];
    mPlatformNvsAreaProtocol.Area->WTASBlockedListEntry11             = mSystemConfiguration.WTASBlockedList[10];
    mPlatformNvsAreaProtocol.Area->WTASBlockedListEntry12             = mSystemConfiguration.WTASBlockedList[11];
    mPlatformNvsAreaProtocol.Area->WTASBlockedListEntry13             = mSystemConfiguration.WTASBlockedList[12];
    mPlatformNvsAreaProtocol.Area->WTASBlockedListEntry14             = mSystemConfiguration.WTASBlockedList[13];
    mPlatformNvsAreaProtocol.Area->WTASBlockedListEntry15             = mSystemConfiguration.WTASBlockedList[14];
    mPlatformNvsAreaProtocol.Area->WTASBlockedListEntry16             = mSystemConfiguration.WTASBlockedList[15];


    mPlatformNvsAreaProtocol.Area->BluetoothSar = mSystemConfiguration.BluetoothSar;
    if (mPlatformNvsAreaProtocol.Area->BluetoothSar == 1) {
      mPlatformNvsAreaProtocol.Area->BluetoothSarBr     = mSystemConfiguration.BluetoothSarBr;
      mPlatformNvsAreaProtocol.Area->BluetoothSarEdr2   = mSystemConfiguration.BluetoothSarEdr2;
      mPlatformNvsAreaProtocol.Area->BluetoothSarEdr3   = mSystemConfiguration.BluetoothSarEdr3;
      mPlatformNvsAreaProtocol.Area->BluetoothSarLe     = mSystemConfiguration.BluetoothSarLe;
      mPlatformNvsAreaProtocol.Area->BluetoothSarLe2Mhz = mSystemConfiguration.BluetoothSarLe2Mhz;
      mPlatformNvsAreaProtocol.Area->BluetoothSarLeLr   = mSystemConfiguration.BluetoothSarLeLr;
    } else {
      mPlatformNvsAreaProtocol.Area->BluetoothSarBr     = 0x0;
      mPlatformNvsAreaProtocol.Area->BluetoothSarEdr2   = 0x0;
      mPlatformNvsAreaProtocol.Area->BluetoothSarEdr3   = 0x0;
      mPlatformNvsAreaProtocol.Area->BluetoothSarLe     = 0x0;
      mPlatformNvsAreaProtocol.Area->BluetoothSarLe2Mhz = 0x0;
      mPlatformNvsAreaProtocol.Area->BluetoothSarLeLr   = 0x0;
    }
    mPlatformNvsAreaProtocol.Area->CnvExtClock          = mSystemConfiguration.CnvExtClock;
  }

  mPlatformNvsAreaProtocol.Area->BtTileMode              = mSystemConfiguration.BtTileMode;
  mPlatformNvsAreaProtocol.Area->TileS0                  = mSystemConfiguration.TileS0;
  mPlatformNvsAreaProtocol.Area->TileS0ix                = mSystemConfiguration.TileS0ix;
  mPlatformNvsAreaProtocol.Area->TileS4                  = mSystemConfiguration.TileS4;
  mPlatformNvsAreaProtocol.Area->TileS5                  = mSystemConfiguration.TileS5;

#if (FixedPcdGetBool(PcdAdlLpSupport) == 1)
  mPlatformNvsAreaProtocol.Area->BtLedConfig             = mSystemConfiguration.BtLedConfig;
  mPlatformNvsAreaProtocol.Area->BtLedPulseDuration      = mSystemConfiguration.BtLedPulseDuration;
  mPlatformNvsAreaProtocol.Area->BtLedPulseInterval      = mSystemConfiguration.BtLedPulseInterval;
#else
  mPlatformNvsAreaProtocol.Area->SpecialLedConfig        = mSystemConfiguration.SpecialLedConfig;
  mPlatformNvsAreaProtocol.Area->LedDuration             = mSystemConfiguration.LedDuration;
  mPlatformNvsAreaProtocol.Area->AirplaneMode            = mSystemConfiguration.AirplaneMode;
#endif

  mPlatformNvsAreaProtocol.Area->SystemTimeAndAlarmSource  = mSystemConfiguration.SystemTimeAndAlarmSource;
  mPlatformNvsAreaProtocol.Area->CoExistenceManager        = mSystemConfiguration.CoExistenceManager;
  mPlatformNvsAreaProtocol.Area->XdciFnEnable              = mPchSetup.PchXdciSupport;
  //
  // Initilize platform ID in Global NVS Area
  //
  mPlatformNvsAreaProtocol.Area->PlatformId     = (UINT8) PcdGet16 (PcdBoardId);
  mPlatformNvsAreaProtocol.Area->GenerationId   = PcdGet8 (PcdPlatformGeneration);
  mPlatformNvsAreaProtocol.Area->PlatformFlavor = PcdGet8 (PcdPlatformFlavor);
  mPlatformNvsAreaProtocol.Area->BoardRev       = PcdGet16 (PcdBoardRev);
  mPlatformNvsAreaProtocol.Area->BoardType      = PcdGet8 (PcdBoardType);

//[-start-180824-IB0672PO-add]//
  mPlatformNvsAreaProtocol.Area->SerialPortAcpiDebug        = mSystemConfiguration.AcpiDebug;
  if (mSystemConfiguration.SerialPortAcpiDebug == 1) {
    mPlatformNvsAreaProtocol.Area->SerialPortAcpiDebug        |= BIT1;
  }
//[-end-180824-IB0672PO-add]//
//[-start-211109-IB18410129-add]//
  mPlatformNvsAreaProtocol.Area->OsDebugPort                = mPchSetup.OsDebugPort;
//[-end-211109-IB18410129-add]//

  mPlatformNvsAreaProtocol.Area->Rtd3Support                = mSystemConfiguration.Rtd3Support;
  mPlatformNvsAreaProtocol.Area->Rtd3P0dl                   = mSystemConfiguration.Rtd3P0dl;
  mPlatformNvsAreaProtocol.Area->Rtd3AudioDelay             = mSystemConfiguration.Rtd3AudioDelay;
  mPlatformNvsAreaProtocol.Area->Rtd3SensorHub              = mSystemConfiguration.Rtd3SensorHub;
  mPlatformNvsAreaProtocol.Area->Rtd3TouchPanelDelay        = mSystemConfiguration.Rtd3TouchPanelDelay;
  mPlatformNvsAreaProtocol.Area->Rtd3TouchPadDelay          = mSystemConfiguration.Rtd3TouchPadDelay;
  mPlatformNvsAreaProtocol.Area->VRRampUpDelay              = mSystemConfiguration.VRRampUpDelay;
  mPlatformNvsAreaProtocol.Area->PstateCapping              = mSystemConfiguration.PstateCapping;
  mPlatformNvsAreaProtocol.Area->UsbPowerResourceTest       = (mSystemConfiguration.Rtd3UsbPt2 << 4)
                                                              | mSystemConfiguration.Rtd3UsbPt1;
  mPlatformNvsAreaProtocol.Area->SensorStandby              = mSystemConfiguration.SensorStandby;
  mPlatformNvsAreaProtocol.Area->Rtd3Config0                = ((mSystemConfiguration.WwanEnable && (mSystemConfiguration.Rtd3WWAN != 0) ? 1:0) << 7)
                                                              | (mSystemConfiguration.Rtd3I2CTouchPanel << 4); // Applicable for SKL SDS RTD3 SIP only
  mPlatformNvsAreaProtocol.Area->Rtd3Config1                =   (mSystemConfiguration.Rtd3RemapCR3  << 10)
                                                              | (mSystemConfiguration.Rtd3RemapCR2  << 9)
                                                              | (mSystemConfiguration.Rtd3RemapCR1  << 8)
                                                              | (mSystemConfiguration.Rtd3SataPort7 << 7)
                                                              | (mSystemConfiguration.Rtd3SataPort6 << 6)
                                                              | (mSystemConfiguration.Rtd3SataPort5 << 5)
                                                              | (mSystemConfiguration.Rtd3SataPort4 << 4)
                                                              | (mSystemConfiguration.Rtd3SataPort3 << 3)
                                                              | (mSystemConfiguration.Rtd3SataPort2 << 2)
                                                              | (mSystemConfiguration.Rtd3SataPort1 << 1)
                                                              |  mSystemConfiguration.Rtd3SataPort0;
  mPlatformNvsAreaProtocol.Area->StorageRtd3Support           = mSystemConfiguration.StorageRtd3Support;

  //
  // Enable PowerState
  //
  mPlatformNvsAreaProtocol.Area->PowerState = 1; // AC =1; for mobile platform, will update this value in SmmPlatform.c
//[-start-190620-IB11270237-add]//
  DEBUG_OEM_SVC ((DEBUG_INFO, "Base OemChipsetServices Call: OemSvcEcPowerState \n"));
  Status = OemSvcEcPowerState (&PowerStateIsAc);
  DEBUG_OEM_SVC ((DEBUG_INFO, "Base OemChipsetServices OemSvcEcPowerState Status: %r\n", Status));
  if (Status == EFI_MEDIA_CHANGED) {
    if (PowerStateIsAc) {
      mPlatformNvsAreaProtocol.Area->PowerState = 1;
    } else {
      mPlatformNvsAreaProtocol.Area->PowerState = 0;
    }
  }
//[-end-190620-IB11270237-add]//
  //
  // These don't have setup controls yet
  //
  mPlatformNvsAreaProtocol.Area->IdeMode = 0;

  if (PcdGetBool (PcdEcPresent) == TRUE) {
    //
    // Call EC lib to get PCIe dock status
    //
    PortData = 0;
//[-start-190620-IB11270237-modify]//
//    Status = GetPcieDockStatus (&PortData);
    DEBUG_OEM_SVC ((DEBUG_INFO, "Base OemChipsetServices Call: OemSvcEcGetPcieDockStatus \n"));
    Status = OemSvcEcGetPcieDockStatus (&CommandStatus, &PortData);
    DEBUG_OEM_SVC ((DEBUG_INFO, "Base OemChipsetServices OemSvcEcGetPcieDockStatus Status: %r\n", Status));
    if (CommandStatus == EFI_SUCCESS) {
      //
      // the bit0 is PCIe Dock Status, 1 = docked
      //
      mPlatformNvsAreaProtocol.Area->PcieDockStatus = (PortData & 1);
    }
  }
//[-end-190620-IB11270237-modify]//

  mPlatformNvsAreaProtocol.Area->NativePCIESupport        = mSystemConfiguration.PciExpNative;
  mPlatformNvsAreaProtocol.Area->PlatformCpuId            = (CpuidVersionInfoEax.Uint32 & 0x0FFFFF);

//[-start-201222-IB09480120-modify]//
//[-start-190611-IB11270237-remove]//
//  //
//  // Create an End of DXE event.
//  //
   Status = gBS->CreateEventEx (
                   EVT_NOTIFY_SIGNAL,
                   TPL_CALLBACK,
                   AcpiEndOfDxeEvent,
                   NULL,
                   &gEfiEndOfDxeEventGroupGuid,
                   &EndOfDxeEvent
                   );
   ASSERT_EFI_ERROR (Status);
//[-end-190611-IB11270237-remove]//
//[-end-201222-IB09480120-modify]//

  //
  // SIO related option
  //
  Status = gBS->LocateProtocol (&gEfiCpuIo2ProtocolGuid, NULL, (VOID **) &mCpuIo);
  ASSERT_EFI_ERROR (Status);

  mPlatformNvsAreaProtocol.Area->WPCN381U = PLATFORM_NVS_DEVICE_DISABLE;

  mPlatformNvsAreaProtocol.Area->DockedSioPresent = PLATFORM_NVS_DEVICE_DISABLE;
  mPlatformNvsAreaProtocol.Area->DockComA = PLATFORM_NVS_DEVICE_DISABLE;
  mPlatformNvsAreaProtocol.Area->DockComB = PLATFORM_NVS_DEVICE_DISABLE;
  mPlatformNvsAreaProtocol.Area->DockLpt = PLATFORM_NVS_DEVICE_DISABLE;
  mPlatformNvsAreaProtocol.Area->DockFdc = PLATFORM_NVS_DEVICE_DISABLE;

  //
  // Check ID for NS87393 SIO, which is connected to Thimble Peak.
  //
  PortData = 0x20;
  Status = mCpuIo->Io.Write (
                        mCpuIo,
                        EfiCpuIoWidthUint8,
                        NAT_CONFIG_INDEX,
                        1,
                        &PortData
                        );
  ASSERT_EFI_ERROR (Status);

  Status = mCpuIo->Io.Read (
                        mCpuIo,
                        EfiCpuIoWidthUint8,
                        NAT_CONFIG_DATA,
                        1,
                        &PortData
                        );
  ASSERT_EFI_ERROR (Status);

  if (PortData == 0xEA) {
    mPlatformNvsAreaProtocol.Area->DockedSioPresent = PLATFORM_NVS_DEVICE_ENABLE;
    mPlatformNvsAreaProtocol.Area->DockComA         = PLATFORM_NVS_DEVICE_ENABLE;
    mPlatformNvsAreaProtocol.Area->DockComB         = PLATFORM_NVS_DEVICE_ENABLE;
  }

  if (mPlatformNvsAreaProtocol.Area->DockedSioPresent != PLATFORM_NVS_DEVICE_ENABLE) {
    //
    // Check ID for SIO WPCN381U
    //
    Status = mCpuIo->Io.Read (
                          mCpuIo,
                          EfiCpuIoWidthUint8,
                          WPCN381U_CONFIG_INDEX,
                          1,
                          &PortData
                          );
    ASSERT_EFI_ERROR (Status);
    if (PortData != 0xFF) {
      PortData = 0x20;
      Status = mCpuIo->Io.Write (
                            mCpuIo,
                            EfiCpuIoWidthUint8,
                            WPCN381U_CONFIG_INDEX,
                            1,
                            &PortData
                            );
      ASSERT_EFI_ERROR (Status);
      Status = mCpuIo->Io.Read (
                            mCpuIo,
                            EfiCpuIoWidthUint8,
                            WPCN381U_CONFIG_DATA,
                            1,
                            &PortData
                            );
      ASSERT_EFI_ERROR (Status);
      if ((PortData == WPCN381U_CHIP_ID) || (PortData == WDCP376_CHIP_ID)) {
        mPlatformNvsAreaProtocol.Area->WPCN381U = PLATFORM_NVS_DEVICE_ENABLE;
      }
    }
  }

  //
  // Enable APIC
  //
  mPlatformNvsAreaProtocol.Area->ApicEnable = PLATFORM_NVS_DEVICE_ENABLE;

  //
  // EC function
  //
  if (PcdGetBool (PcdEcPresent) == TRUE) {
    mPlatformNvsAreaProtocol.Area->EcAvailable     = TRUE;
    mPlatformNvsAreaProtocol.Area->EcLowPowerMode  = mSystemConfiguration.EcLowPowerMode;
    mPlatformNvsAreaProtocol.Area->EcSmiGpioPin    = PcdGet32(PcdEcSmiGpio);
    mPlatformNvsAreaProtocol.Area->EcLowPowerModeGpioPin  = PcdGet32(PcdEcLowPowerExitGpio);
  } else {
//[-start-210519-KEBIN00001-modify]//
#ifdef LCFC_SUPPORT
    mPlatformNvsAreaProtocol.Area->EcAvailable     = (FeaturePcdGet (PcdEnableEconFlag)) ? TRUE : FALSE;
#else
    mPlatformNvsAreaProtocol.Area->EcAvailable     = FALSE;
#endif
//[-end-210519-KEBIN00001-modify]//
    mPlatformNvsAreaProtocol.Area->EcLowPowerMode  = 0;
    mPlatformNvsAreaProtocol.Area->EcSmiGpioPin    = 0;
    mPlatformNvsAreaProtocol.Area->EcLowPowerModeGpioPin  = 0;
  }
  mPlatformNvsAreaProtocol.Area->EnableVoltageMargining = PcdGetBool (PcdEnableVoltageMargining);

  //
  // Comms Hub Support Enable/Disable
  //
  mPlatformNvsAreaProtocol.Area->CommsHubEnable  = mMeSetup.CommsHubEnable;

  mPlatformNvsAreaProtocol.Area->IuerButtonEnable = SaSetup.IuerButtonEnable;

  //
  // Enable 10sec Power Button Override only if CS is enabled.
  //
  if (mSystemConfiguration.LowPowerS0Idle == 1) {
    mPlatformNvsAreaProtocol.Area->TenSecondPowerButtonEnable |= BIT0; // Enable 10sec Power Button OVR.
    if (mSystemConfiguration.HidEventFilterDriverEnable) {
      mPlatformNvsAreaProtocol.Area->HidEventFilterEnable = 0x01;        // Enable Hid Event Filter Driver
    } else {
      mPlatformNvsAreaProtocol.Area->HidEventFilterEnable = 0x00;        // Disable Hid Event Filter Driver
    }
  } else {
    mPlatformNvsAreaProtocol.Area->TenSecondPowerButtonEnable &= ~(BIT0); // Disable 10sec Power Button OVR.
    //
    // HID Event Filter Enable/Disable
    //
    mPlatformNvsAreaProtocol.Area->HidEventFilterEnable = mSystemConfiguration.HidEventFilterDriverEnable;
  }
  mPlatformNvsAreaProtocol.Area->TenSecondPowerButtonEnable |= (SaSetup.SlateIndicatorRT << 3);
  mPlatformNvsAreaProtocol.Area->TenSecondPowerButtonEnable |= (SaSetup.DockIndicatorRT << 4);
  mPlatformNvsAreaProtocol.Area->IuerConvertibleEnable = SaSetup.IuerConvertibleEnable;
  mPlatformNvsAreaProtocol.Area->IuerDockEnable = SaSetup.IuerDockEnable;

  mPlatformNvsAreaProtocol.Area->VirtualGpioButtonSxBitmask  = 0x00;
  mPlatformNvsAreaProtocol.Area->VirtualGpioButtonSxBitmask |=  SaSetup.SlateIndicatorSx << 6;
  mPlatformNvsAreaProtocol.Area->VirtualGpioButtonSxBitmask |=  SaSetup.DockIndicatorSx << 7;

  //
  // Low Power S0 Idle - Enabled/Disabled
  //
  mPlatformNvsAreaProtocol.Area->LowPowerS0Idle = mSystemConfiguration.LowPowerS0Idle;
  mPlatformNvsAreaProtocol.Area->PseudoG3State = mSystemConfiguration.PseudoG3State;
  mPlatformNvsAreaProtocol.Area->PuisEnable = mSystemConfiguration.PuisEnable;

  //
  // WWAN GPIO pin configuration
  //
  mPlatformNvsAreaProtocol.Area->WwanRtd3Option = mSystemConfiguration.Rtd3WWAN;
  mPlatformNvsAreaProtocol.Area->WwanPerstGpio  = PcdGet32 (PcdWwanPerstGpio);
  mPlatformNvsAreaProtocol.Area->WwanPerstGpioPolarity = PcdGetBool (PcdWwanPerstGpioPolarity);
  mPlatformNvsAreaProtocol.Area->WwanBbrstGpio = PcdGet32 (PcdWwanBbrstGpio);
  mPlatformNvsAreaProtocol.Area->WwanBbrstGpioPolarity = PcdGetBool (PcdWwanBbrstGpioPolarity);
  mPlatformNvsAreaProtocol.Area->WwanWakeGpio = PcdGet32 (PcdWwanWakeGpio);
  mPlatformNvsAreaProtocol.Area->WwanFullCardPowerOffGpio = PcdGet32 (PcdWwanFullCardPowerOffGpio);
  mPlatformNvsAreaProtocol.Area->WwanFullCardPowerOffGpioPolarity = PcdGetBool (PcdWwanFullCardPowerOffGpioPolarity);
  mPlatformNvsAreaProtocol.Area->WwanSourceClock = PcdGet8 (PcdWwanSourceClock);
  mPlatformNvsAreaProtocol.Area->WwanRootPortNumber = PcdGet8 (PcdWwanRootPortNumber);
  mPlatformNvsAreaProtocol.Area->PcieDeviceOnWwanSlot = PcdGetBool (PcdPcieDeviceOnWwanSlot);
  mPlatformNvsAreaProtocol.Area->WwanFwFlashDevice  = mSystemConfiguration.WwanFwFlashDevice;
  mPlatformNvsAreaProtocol.Area->WccdEnable         = mSystemConfiguration.WirelessCnvConfigDevice;
  mPlatformNvsAreaProtocol.Area->WwanEnable = PcdGet8 (PcdPcieWwanEnable);
  mPlatformNvsAreaProtocol.Area->WwanTOn2ResDelayMs = PcdGet16 (PcdBoardWwanTOn2ResDelayMs);
  mPlatformNvsAreaProtocol.Area->WwanTOnRes2PerDelayMs = PcdGet16 (PcdBoardWwanTOnRes2PerDelayMs);
  mPlatformNvsAreaProtocol.Area->WwanTOnPer2PdsDelayMs = PcdGet16 (PcdBoardWwanTOnPer2PdsDelayMs);
  mPlatformNvsAreaProtocol.Area->WwanTRes2OffDelayMs = PcdGet16 (PcdBoardWwanTRes2OffDelayMs);
  mPlatformNvsAreaProtocol.Area->WwanTOffDisDelayMs = PcdGet16 (PcdBoardWwanTOffDisDelayMs);
  mPlatformNvsAreaProtocol.Area->WwanTResTogDelayMs = PcdGet16 (PcdBoardWwanTResTogDelayMs);
  mPlatformNvsAreaProtocol.Area->WwanTRes2PdsDelayMs = PcdGet16 (PcdBoardWwanTRes2PdsDelayMs);
  mPlatformNvsAreaProtocol.Area->WwanTPer2ResDelayMs = PcdGet16 (PcdBoardWwanTPer2ResDelayMs);
  mPlatformNvsAreaProtocol.Area->WwanOemSvid         = mSystemConfiguration.WwanOemSvid;
  mPlatformNvsAreaProtocol.Area->WwanSvidTimeout     = mSystemConfiguration.WwanSvidTimeout;

  // SD card D3 cold support
  mPlatformNvsAreaProtocol.Area->Rp08D3ColdDisable  = mSystemConfiguration.Rp08D3ColdDisable;
  mPlatformNvsAreaProtocol.Area->Rp08D3ColdSupport  = 0;

  //Wake on LAN AIC card support
  mPlatformNvsAreaProtocol.Area->Rp08WakeReworkDone = mSystemConfiguration.Rp08WakeReworkDone;

  //
  // PCH PCIe x4 Slot 1 GPIO pin configuration
  //
  mPlatformNvsAreaProtocol.Area->PcieSlot1WakeGpio = PcdGet32 (PcdPcieSlot1WakeGpioPin);
  mPlatformNvsAreaProtocol.Area->PcieSlot1PowerEnableGpio = PcdGet32(PcdPcieSlot1PwrEnableGpioNo);
  mPlatformNvsAreaProtocol.Area->PcieSlot1PowerEnableGpioPolarity = PcdGetBool(PcdPcieSlot1PwrEnableGpioPolarity);
  mPlatformNvsAreaProtocol.Area->PcieSlot1RstGpio = PcdGet32(PcdPcieSlot1HoldRstGpioNo);
  mPlatformNvsAreaProtocol.Area->PcieSlot1RstGpioPolarity = PcdGetBool(PcdPcieSlot1HoldRstGpioPolarity);
  mPlatformNvsAreaProtocol.Area->PcieSlot1RpNumber = PcdGet8 (PcdPcieSlot1RootPort);
  mPlatformNvsAreaProtocol.Area->Dg1VramSRGpio = PcdGet32 (PcdDg1VramSRGpio);
  mPlatformNvsAreaProtocol.Area->DisplayMuxGpioNo = PcdGet32 (PcdDisplayMuxGpioNo);

//[-start-201124-IB09480117-add]//
//[-start-210113-IB05660151-modify]//
  if ((FeaturePcdGet (PcdHybridGraphicsSupported) == 1) &&
      (FixedPcdGetBool (PcdUseCrbHgDefaultSettings) == 0) &&
//[-start-210127-IB05660155-modify]//
//[-start-210318-IB05660157-modify]//
//[-start-210428-IB05660158-modify]//
      (mChipsetSetupData.HgSlot == PCH)) {
//[-end-210428-IB05660158-modify]//
//[-end-210318-IB05660157-modify]//
//[-start-210127-IB05660155-modify]//
    mPlatformNvsAreaProtocol.Area->PcieSlot1WakeGpio = FixedPcdGet32 (PcdHgDgpuWakeGpioNo);
    mPlatformNvsAreaProtocol.Area->PcieSlot1PowerEnableGpio = FixedPcdGet32 (PcdHgDgpuPwrEnableGpioNo);
    mPlatformNvsAreaProtocol.Area->PcieSlot1PowerEnableGpioPolarity = FixedPcdGetBool (PcdHgDgpuPwrEnableActive);
    mPlatformNvsAreaProtocol.Area->PcieSlot1RstGpio = FixedPcdGet32 (PcdHgDgpuHoldRstGpioNo);
    mPlatformNvsAreaProtocol.Area->PcieSlot1RstGpioPolarity = FixedPcdGetBool (PcdHgDgpuHoldRstActive);
    mPlatformNvsAreaProtocol.Area->PcieSlot1RpNumber = FixedPcdGet8 (PcdHgPcieRootPortIndex);
    mPlatformNvsAreaProtocol.Area->Dg1VramSRGpio = PcdGet32 (PcdIntelDg1VramSrGpioNo);
  }
//[-end-210113-IB05660151-modify]//
//[-end-201124-IB09480117-add]//

  //
  // PCH PCIe x4 Slot 2 GPIO pin configuration
  //
  mPlatformNvsAreaProtocol.Area->PcieSlot2WakeGpio = PcdGet32(PcdPcieSlot2WakeGpioPin);
  mPlatformNvsAreaProtocol.Area->PcieSlot2PowerEnableGpio = PcdGet32(PcdPchPCIeSlot2PwrEnableGpioNo);
  mPlatformNvsAreaProtocol.Area->PcieSlot2PowerEnableGpioPolarity = PcdGetBool(PcdPchPCIeSlot2PwrEnableGpioPolarity);
  mPlatformNvsAreaProtocol.Area->PcieSlot2RstGpio = PcdGet32(PcdPchPCIeSlot2RstGpioNo);
  mPlatformNvsAreaProtocol.Area->PcieSlot2RstGpioPolarity = PcdGetBool(PcdPchPCIeSlot2RstGpioPolarity);
  mPlatformNvsAreaProtocol.Area->PcieSlot2RpNumber = PcdGet8(PcdPcieSlot2RootPort);

  //
  // PCH PCIe x2 Slot 3 GPIO pin configuration
  //
  mPlatformNvsAreaProtocol.Area->PcieSlot3WakeGpio = PcdGet32(PcdPcieSlot3WakeGpioPin);
  mPlatformNvsAreaProtocol.Area->PcieSlot3PowerEnableGpio = PcdGet32(PcdPchPCIeSlot3PwrEnableGpioNo);
  mPlatformNvsAreaProtocol.Area->PcieSlot3PowerEnableGpioPolarity = PcdGetBool(PcdPchPCIeSlot3PwrEnableGpioPolarity);
  mPlatformNvsAreaProtocol.Area->PcieSlot3RstGpio = PcdGet32(PcdPchPCIeSlot3RstGpioNo);
  mPlatformNvsAreaProtocol.Area->PcieSlot3RstGpioPolarity = PcdGetBool(PcdPchPCIeSlot3RstGpioPolarity);
  mPlatformNvsAreaProtocol.Area->PcieSlot3RpNumber = PcdGet8(PcdPcieSlot3RootPort);

  //
  // CPU M.2 SSD GPIO pin configuration
  //
  mPlatformNvsAreaProtocol.Area->M2Ssd2PowerEnableGpio = PcdGet32(PcdPcieSsd2PwrEnableGpioNo);
  mPlatformNvsAreaProtocol.Area->M2Ssd2PowerEnableGpioPolarity = PcdGetBool(PcdPcieSsd2PwrEnableGpioPolarity);
  mPlatformNvsAreaProtocol.Area->M2Ssd2RstGpio = PcdGet32(PcdPcieSsd2RstGpioNo);
  mPlatformNvsAreaProtocol.Area->M2Ssd2RstGpioPolarity = PcdGetBool(PcdPcieSsd2RstGpioPolarity);

  //
  // CPU M.2 SSD2 GPIO pin configuration
  //
  mPlatformNvsAreaProtocol.Area->M2Ssd3PowerEnableGpio = PcdGet32(PcdPcieSsd3PwrEnableGpioNo);
  mPlatformNvsAreaProtocol.Area->M2Ssd3PowerEnableGpioPolarity = PcdGetBool(PcdPcieSsd3PwrEnableGpioPolarity);
  mPlatformNvsAreaProtocol.Area->M2Ssd3RstGpio = PcdGet32(PcdPcieSsd3RstGpioNo);
  mPlatformNvsAreaProtocol.Area->M2Ssd3RstGpioPolarity = PcdGetBool(PcdPcieSsd3RstGpioPolarity);

  //
  // CPU M.2 DG GPIO pin Configuration
  //
  mPlatformNvsAreaProtocol.Area->M2DG2PowerEnableGpio = PcdGet32(PcdPcieDG2PwrEnableGpioNo);
  mPlatformNvsAreaProtocol.Area->M2DG2PowerEnableGpioPolarity = PcdGetBool(PcdPcieDG2PwrEnableGpioPolarity);
  mPlatformNvsAreaProtocol.Area->M2DG2RstGpio= PcdGet32(PcdPcieDG2RstGpioNo);
  mPlatformNvsAreaProtocol.Area->M2DG2RstGpioPolarity = PcdGetBool(PcdPcieDG2RstGpioPolarity);
  mPlatformNvsAreaProtocol.Area->M2DG2WakeGpioPin = PcdGet32(PcdPcieDG2WakeGpioPin);

  //
  // PCH M.2 SSD GPIO pin configuration
  //
  mPlatformNvsAreaProtocol.Area->PchM2SsdPowerEnableGpio = PcdGet32(PcdPchSsd1PwrEnableGpioNo);
  mPlatformNvsAreaProtocol.Area->PchM2SsdPowerEnableGpioPolarity = PcdGetBool(PcdPchSsd1PwrEnableGpioPolarity);
  mPlatformNvsAreaProtocol.Area->PchM2SsdRstGpio = PcdGet32(PcdPchSsd1RstGpioNo);
  mPlatformNvsAreaProtocol.Area->PchM2SsdRstGpioPolarity = PcdGetBool(PcdPchSsd1RstGpioPolarity);

  //
  // PCH M.2 SSD2 GPIO pin configuration
  //
  mPlatformNvsAreaProtocol.Area->PchM2Ssd2PowerEnableGpio         = PcdGet32(PcdPchSsd2PwrEnableGpioNo);
  mPlatformNvsAreaProtocol.Area->PchM2Ssd2PowerEnableGpioPolarity = PcdGetBool(PcdPchSsd2PwrEnableGpioPolarity);
  mPlatformNvsAreaProtocol.Area->PchM2Ssd2RstGpio                 = PcdGet32(PcdPchSsd2RstGpioNo);
  mPlatformNvsAreaProtocol.Area->PchM2Ssd2RstGpioPolarity         = PcdGetBool(PcdPchSsd2RstGpioPolarity);

  //
  // PCH M.2 SSD3 GPIO pin configuration
  //
  mPlatformNvsAreaProtocol.Area->PchM2Ssd3PowerEnableGpio         = PcdGet32(PcdPchSsd3PwrEnableGpioNo);
  mPlatformNvsAreaProtocol.Area->PchM2Ssd3PowerEnableGpioPolarity = PcdGetBool(PcdPchSsd3PwrEnableGpioPolarity);
  mPlatformNvsAreaProtocol.Area->PchM2Ssd3RstGpio                 = PcdGet32(PcdPchSsd3RstGpioNo);
  mPlatformNvsAreaProtocol.Area->PchM2Ssd3RstGpioPolarity         = PcdGetBool(PcdPchSsd3RstGpioPolarity);

  //
  // Onboard MR 1 GPIO pin configuration (for ADL S8 only)
  //
  mPlatformNvsAreaProtocol.Area->OnBoardMr1PowerEnableGpio         = PcdGet32 (PcdOnBoardMr1PowerEnableGpioNo);
  mPlatformNvsAreaProtocol.Area->OnBoardMr1PowerEnableGpioPolarity = PcdGetBool (PcdOnBoardMr1PowerEnableGpioPolarity);
  mPlatformNvsAreaProtocol.Area->OnBoardMr1RstGpio                 = PcdGet32 (PcdOnBoardMr1RstGpioNo);
  mPlatformNvsAreaProtocol.Area->OnBoardMr1RstGpioPolarity         = PcdGetBool (PcdOnBoardMr1RstGpioPolarity);
  mPlatformNvsAreaProtocol.Area->OnBoardMr1WakeGpio                = PcdGet32 (PcdOnBoardMr1WakeGpioPin);
  mPlatformNvsAreaProtocol.Area->OnBoardMr1RpNumber                = PcdGet8 (PcdOnBoardMr1RootPort);

  //
  // Onboard MR 2 GPIO pin configuration
  //
  mPlatformNvsAreaProtocol.Area->OnBoardMr2PowerEnableGpio = PcdGet32(PcdOnBoardMr2PowerEnableGpioNo);
  mPlatformNvsAreaProtocol.Area->OnBoardMr2PowerEnableGpioPolarity = PcdGetBool(PcdOnBoardMr2PowerEnableGpioPolarity);
  mPlatformNvsAreaProtocol.Area->OnBoardMr2RstGpio = PcdGet32(PcdOnBoardMr2RstGpioNo);
  mPlatformNvsAreaProtocol.Area->OnBoardMr2RstGpioPolarity = PcdGetBool(PcdOnBoardMr2RstGpioPolarity);
  mPlatformNvsAreaProtocol.Area->OnBoardMr2WakeGpio = PcdGet32(PcdOnBoardMr2WakeGpioPin);
  mPlatformNvsAreaProtocol.Area->OnBoardMr2RpNumber = PcdGet8(PcdOnBoardMr2RootPort);

  //
  // PCH Sata ports GPIO pin configuration
  //
  mPlatformNvsAreaProtocol.Area->SataPortPowerEnableGpio = PcdGet32(PcdPchSataPortPwrEnableGpioNo);
  mPlatformNvsAreaProtocol.Area->SataPortPowerEnableGpioPolarity = PcdGetBool(PcdPchSataPortPwrEnableGpioPolarity);

  //
  // CPU PEG1 GPIO pin configuration
  //
  mPlatformNvsAreaProtocol.Area->PegSlot1PwrEnableGpioNo       = PcdGet32 (PcdPegSlot1PwrEnableGpioNo);
  mPlatformNvsAreaProtocol.Area->PegSlot1PwrEnableGpioPolarity = PcdGetBool (PcdPegSlot1PwrEnableGpioPolarity);
  mPlatformNvsAreaProtocol.Area->PegSlot1RstGpioNo             = PcdGet32 (PcdPegSlot1RstGpioNo);
  mPlatformNvsAreaProtocol.Area->PegSlot1RstGpioPolarity       = PcdGetBool (PcdPegSlot1RstGpioPolarity);
  mPlatformNvsAreaProtocol.Area->PegSlot1WakeGpioPin           = PcdGet32 (PcdPegSlot1WakeGpioPin);
  mPlatformNvsAreaProtocol.Area->PegSlot1RootPort              = PcdGet8 (PcdPegSlot1RootPort);

//[-start-201124-IB09480117-add]//
//[-start-210113-IB05660151-modify]//
  if ((FeaturePcdGet (PcdHybridGraphicsSupported) == 1) &&
      (FixedPcdGetBool (PcdUseCrbHgDefaultSettings) == 0) &&
//[-start-210127-IB05660155-modify]//
//[-start-210318-IB05660157-modify]//
//[-start-210428-IB05660158-modify]//
//[-start-210913-IB05660178-modify]//
      (mChipsetSetupData.HgSlot == PEG) &&
      ((FeaturePcdGet (PcdNvidiaOptimusSupported)) ||
       (FeaturePcdGet (PcdAmdPowerXpressSupported)))) {
    if (PcdGet16 (PcdCrbSkuId) == 1) { // ADL-S
//[-end-210428-IB05660158-modify]//
//[-end-210318-IB05660157-modify]//
//[-end-210127-IB05660155-modify]//
      mPlatformNvsAreaProtocol.Area->PegSlot1WakeGpioPin = FixedPcdGet32 (PcdHgDgpuWakeGpioNo);
      mPlatformNvsAreaProtocol.Area->PegSlot1PwrEnableGpioNo = FixedPcdGet32 (PcdHgDgpuPwrEnableGpioNo);
      mPlatformNvsAreaProtocol.Area->PegSlot1PwrEnableGpioPolarity = FixedPcdGetBool (PcdHgDgpuPwrEnableActive);
      mPlatformNvsAreaProtocol.Area->PegSlot1RstGpioNo = FixedPcdGet32 (PcdHgDgpuHoldRstGpioNo);
      mPlatformNvsAreaProtocol.Area->PegSlot1RstGpioPolarity = FixedPcdGetBool (PcdHgDgpuHoldRstActive);
      mPlatformNvsAreaProtocol.Area->PegSlot1RootPort = FixedPcdGet8 (PcdHgPcieRootPortIndex);
    } else if (PcdGet16 (PcdCrbSkuId) == 2) { // ADL-P
      if ((PcdGet8 (PcdHgPegBridgeDevice) == 6) &&   //OEM_customize: please follow your HW design to fill PcdHgPegBridgeDevice and modify conditional expression
          (PcdGet8 (PcdHgPegBridgeFunction) == 0)) { //OEM_customize: please follow your HW design to fill PcdHgPegBridgeFunction and modify conditional expression
        mPlatformNvsAreaProtocol.Area->M2Ssd2PowerEnableGpio = FixedPcdGet32 (PcdHgDgpuPwrEnableGpioNo);
        mPlatformNvsAreaProtocol.Area->M2Ssd2PowerEnableGpioPolarity = FixedPcdGetBool (PcdHgDgpuPwrEnableActive);
        mPlatformNvsAreaProtocol.Area->M2Ssd2RstGpio = FixedPcdGet32 (PcdHgDgpuHoldRstGpioNo);
        mPlatformNvsAreaProtocol.Area->M2Ssd2RstGpioPolarity = FixedPcdGetBool (PcdHgDgpuHoldRstActive);
      } else if ((PcdGet8 (PcdHgPegBridgeDevice) == 1) &&   //OEM_customize: please follow your HW design to fill PcdHgPegBridgeDevice and modify conditional expression
                 (PcdGet8 (PcdHgPegBridgeFunction) == 0)) { //OEM_customize: please follow your HW design to fill PcdHgPegBridgeFunction and modify conditional expression
        mPlatformNvsAreaProtocol.Area->M2DG2PowerEnableGpio = FixedPcdGet32 (PcdHgDgpuPwrEnableGpioNo);
        mPlatformNvsAreaProtocol.Area->M2DG2PowerEnableGpioPolarity = FixedPcdGetBool (PcdHgDgpuPwrEnableActive);
        mPlatformNvsAreaProtocol.Area->M2DG2RstGpio = FixedPcdGet32 (PcdHgDgpuHoldRstGpioNo);
        mPlatformNvsAreaProtocol.Area->M2DG2RstGpioPolarity = FixedPcdGetBool (PcdHgDgpuHoldRstActive);
      }
    }
//[-end-210913-IB05660178-modify]//
  }
//[-end-210113-IB05660151-modify]//
//[-end-201124-IB09480117-add]//

  //
  // CPU PEG2 GPIO pin configuration
  //
  mPlatformNvsAreaProtocol.Area->PegSlot2PwrEnableGpioNo       = PcdGet32 (PcdPegSlot2PwrEnableGpioNo);
  mPlatformNvsAreaProtocol.Area->PegSlot2PwrEnableGpioPolarity = PcdGetBool (PcdPegSlot2PwrEnableGpioPolarity);
  mPlatformNvsAreaProtocol.Area->PegSlot2RstGpioNo             = PcdGet32 (PcdPegSlot2RstGpioNo);
  mPlatformNvsAreaProtocol.Area->PegSlot2RstGpioPolarity       = PcdGetBool (PcdPegSlot2RstGpioPolarity);
  mPlatformNvsAreaProtocol.Area->PegSlot2WakeGpioPin           = PcdGet32 (PcdPegSlot2WakeGpioPin);
  mPlatformNvsAreaProtocol.Area->PegSlot2RootPort              = PcdGet8 (PcdPegSlot2RootPort);

  //
  // Foxville I225 Lan GPIO pin configuration
  //
  mPlatformNvsAreaProtocol.Area->FoxLanWakeGpio                = PcdGet32(PcdFoxLanWakeGpio);
  mPlatformNvsAreaProtocol.Area->FoxLanDisableNGpio            = PcdGet32(PcdFoxLanDisableNGpio);
  mPlatformNvsAreaProtocol.Area->FoxLanDisableNGpioPolarity    = PcdGetBool(PcdFoxLanDisableNGpioPolarity);
  mPlatformNvsAreaProtocol.Area->FoxLanRstGpio                 = PcdGet32(PcdFoxLanResetGpio);
  mPlatformNvsAreaProtocol.Area->FoxLanRstGpioPolarity         = PcdGetBool(PcdFoxLanResetGpioPolarity);
  mPlatformNvsAreaProtocol.Area->FoxLanSupport                 = mPchSetup.FoxvilleLanSupport;
  mPlatformNvsAreaProtocol.Area->FoxLanRpNumber                = PcdGet8(PcdFoxlanRootPortNumber);
  //
  // Touch Pad and Panel GPIO pin configuration
  //
  mPlatformNvsAreaProtocol.Area->TouchpadIrqGpio = PcdGet32(PcdTouchpadIrqGpio);
  mPlatformNvsAreaProtocol.Area->TouchpadIrqGpioPolarity = PcdGetBool(PcdTouchpadIrqGpioPolarity);
  mPlatformNvsAreaProtocol.Area->TouchpanelIrqGpio = PcdGet32(PcdTouchpanelIrqGpio);
  mPlatformNvsAreaProtocol.Area->TouchpanelPowerEnableGpio = PcdGet32(PcdTouchpanelPwrEnableGpio);
  mPlatformNvsAreaProtocol.Area->TouchpanelPowerRstGpio = PcdGet32(PcdTouchpanelRstGpio);
  mPlatformNvsAreaProtocol.Area->TouchpanelIrqGpioPolarity = PcdGetBool(PcdTouchpanelIrqGpioPolarity);
  mPlatformNvsAreaProtocol.Area->TouchpanelPowerEnableGpioPolarity = PcdGetBool(PcdTouchpanelPwrEnableGpioPolarity);
  mPlatformNvsAreaProtocol.Area->TouchpanelPowerRstGpioPolarity = PcdGetBool(PcdTouchpanelRstGpioPolarity);
  mPlatformNvsAreaProtocol.Area->Touchpanel1IrqGpio = PcdGet32(PcdTouchpanel1IrqGpio);
  mPlatformNvsAreaProtocol.Area->Touchpanel1PowerEnableGpio = PcdGet32(PcdTouchpanel1PwrEnableGpio);
  mPlatformNvsAreaProtocol.Area->Touchpanel1PowerRstGpio = PcdGet32(PcdTouchpanel1RstGpio);
  mPlatformNvsAreaProtocol.Area->Touchpanel1IrqGpioPolarity = PcdGetBool(PcdTouchpanel1IrqGpioPolarity);
  mPlatformNvsAreaProtocol.Area->Touchpanel1PowerEnableGpioPolarity = PcdGetBool(PcdTouchpanel1PwrEnableGpioPolarity);
  mPlatformNvsAreaProtocol.Area->Touchpanel1PowerRstGpioPolarity = PcdGetBool(PcdTouchpanel1RstGpioPolarity);
  // WLAN GPIO pin configuration
  //
  mPlatformNvsAreaProtocol.Area->WlanWakeGpio = PcdGet32(PcdWlanWakeGpio);
  mPlatformNvsAreaProtocol.Area->WlanRootPortNumber = PcdGet8 (PcdWlanRootPortNumber);

  //
  // Lid Switch Wake Gpio Config
  //
  mPlatformNvsAreaProtocol.Area->LidSwitchWakeGpio = PcdGet32 (PcdLidSwitchWakeGpio);

  //
  // Platform AUX power limit configuration
  //
  mPlatformNvsAreaProtocol.Area->AuxPowerLimit = mSystemConfiguration.AuxRailBudget * 1000;//Maximum power supported by the aux rail.
  DEBUG((DEBUG_INFO,"AuxPowerLimit = %d\n", mPlatformNvsAreaProtocol.Area->AuxPowerLimit));

  //
  //Disable PEP constarints for devices which are not present in CNL-H platform
  //
  if (IsPchH ()) {
  //  Note: I2c4 and I2c5 present on ADL-S platform
  //  mSystemConfiguration.PepI2c4 = 0;
  //  mSystemConfiguration.PepI2c5 = 0;
  }
  //
  // Disable PEP constraint if IPU B0:D5:F0 device is not preset
  //
  if (PciSegmentRead16 (PCI_SEGMENT_LIB_ADDRESS (SA_SEG_NUM, GetIpuBusNumber (), GetIpuDevNumber (), GetIpuFuncNumber (), PCI_VENDOR_ID_OFFSET)) == 0xFFFF) {
    mSystemConfiguration.PepIpu = 0;
  }
  //
  // Disable PEP constraint if GNA B0:D8:F0 device is not preset
  //
  if (PciSegmentRead16 (PCI_SEGMENT_LIB_ADDRESS (SA_SEG_NUM, GetGnaBusNumber (), GetGnaDevNumber (), GetGnaFuncNumber (), PCI_VENDOR_ID_OFFSET)) == 0xFFFF) {
    mSystemConfiguration.PepGna = 0;
  }
  //
  //Disable PEP constraint if VMD B0:D14:F0 device is not present
  //
  if (!IsVmdEnabled ()) {
    mSystemConfiguration.PepVmd = 0;
  }
  //
  // Disable PEP constraint if PEG B0:D1:F0 device is not preset
  //
  if (PciSegmentRead16 (PCI_SEGMENT_LIB_ADDRESS (SA_SEG_NUM, SA_PEG_BUS_NUM, SA_PEG3_DEV_NUM, SA_PEG0_FUN_NUM, PCI_VENDOR_ID_OFFSET)) == 0xFFFF) {
    mSystemConfiguration.PepPeg0 = 0;
  }

//[start-210907-STORM1112-modify]
#if defined(C770_SUPPORT) || defined(S370_SUPPORT) || defined(S570_SUPPORT) || defined(S77014_SUPPORT) || defined(S77014IAH_SUPPORT)
  if (PciSegmentRead16 (PCI_SEGMENT_LIB_ADDRESS (SA_SEG_NUM, SA_PEG_BUS_NUM, SA_PEG3_DEV_NUM, SA_PEG2_FUN_NUM, PCI_VENDOR_ID_OFFSET)) == 0xFFFF) {
#else
  if (PciSegmentRead16 (PCI_SEGMENT_LIB_ADDRESS (SA_SEG_NUM, SA_PEG_BUS_NUM, SA_PEG1_DEV_NUM, SA_PEG0_FUN_NUM, PCI_VENDOR_ID_OFFSET)) == 0xFFFF) {
#endif
//[end-210907-STORM1112-modify]
    mSystemConfiguration.PepPcieDg = 0;
  }

  //
  // Disable PEP constraint if IGD B0:D2:F0 device is not preset
  //
  if (PciSegmentRead16 (PCI_SEGMENT_LIB_ADDRESS (SA_SEG_NUM, GetIgdBusNumber(), GetIgdDevNumber(), GetIgdFuncNumber(), PCI_VENDOR_ID_OFFSET)) == 0xFFFF) {
    mSystemConfiguration.PepGfx = 0;
  }
  //
  //Disable Gbe PEP constarints if it's not presented.
  //
  if (!IsGbePresent()) {
    mSystemConfiguration.PepGbe = 0;
  }
  //
  // Disable PEP constraint if PCH SATA B0:D23:F0 device is not preset
  //
  RpDeviceBaseAddress =  SataPciCfgBase(SATA_1_CONTROLLER_INDEX);
  if ((PciSegmentRead16 (RpDeviceBaseAddress + PCI_VENDOR_ID_OFFSET) == 0xFFFF) || (PciSegmentRead16 (RpDeviceBaseAddress + PCI_DEVICE_ID_OFFSET) == VMD_DUMMY_DEVICE_ID)) {
    mSystemConfiguration.PepSataContraints = 0;
  }

  //
  // Disable PEP Constrains for THC
  //
  if (mPchSetup.ThcPort0Assignment != ThcAssignmentThc0) {
    // Disable PEP for THC0
    mSystemConfiguration.PepThc0 = 0;
  }
  if (mPchSetup.ThcPort1Assignment != ThcAssignmentThc1) {
    // Disable PEP for THC1
    mSystemConfiguration.PepThc1 = 0; //THC1 is not used
  }

  //
  // Disable PEP constraint if PCH HDA B0:D31:F3 device is not preset
  //
  if (PciSegmentRead16 (HdaPciCfgBase () + PCI_VENDOR_ID_OFFSET) == 0xFFFF) {
    mSystemConfiguration.PepAudio = 0;
  }

  //
  // Disable PEP constraint if PCH XHCI B0:D20:F0 device is not preset
  //
  if (PciSegmentRead16 (PchXhciPciCfgBase () + PCI_VENDOR_ID_OFFSET) == 0xFFFF) {
    mSystemConfiguration.PepXhci = 0;
  }

  //
  // Disable PEP constraint if PCH CSME device B0:D22:F0 is not preset
  //
  if (PciSegmentRead16 (PchHeci1PciCfgBase () + PCI_VENDOR_ID_OFFSET) == 0xFFFF) {
    mSystemConfiguration.PepCsme = 0;
  }

  //
  // Disable PEP constraint for EMMC device if not preset
  //
  if (!IsPchEmmcSupported ()) {
    mSystemConfiguration.PepEmmc = 0;
  }
  //
  // Disable PEP constraint if HECI3 B0:D22:F4 device is not preset
  //
  if (PciSegmentRead16 (PchHeci3PciCfgBase () + PCI_VENDOR_ID_OFFSET) == 0xFFFF) {
    DEBUG((DEBUG_INFO,"HECI3 device is not present\n"));
    mSystemConfiguration.PepHeci3 = 0;
  }

  //
  // Enable PEP constraints for Serial Io devices
  // Override Function 0 Enabled devices
  //
  ZeroMem (PepSerialIoSpi, sizeof (PepSerialIoSpi));
  for (SerialIoIndex = 0; SerialIoIndex < GetPchMaxSerialIoSpiControllersNum (); SerialIoIndex++) {
    PepSerialIoSpi[SerialIoIndex] = mPchSetup.PchSerialIoSpi[SerialIoIndex];
    if ((PepSerialIoSpi[SerialIoIndex] == 0) && IsSerialIoSpiFunction0Enabled (SerialIoIndex)) {
      PepSerialIoSpi[SerialIoIndex] = 1;
    }
  }
  ZeroMem (PepSerialIoI2c, sizeof (PepSerialIoI2c));
  for (SerialIoIndex = 0; SerialIoIndex < GetPchMaxSerialIoI2cControllersNum (); SerialIoIndex++) {
    PepSerialIoI2c[SerialIoIndex] = mPchSetup.PchSerialIoI2c[SerialIoIndex];
    if ((PepSerialIoI2c[SerialIoIndex] == 0) && IsSerialIoI2cFunction0Enabled (SerialIoIndex)) {
      PepSerialIoI2c[SerialIoIndex] = 1;
    }
  }
  ZeroMem (PepSerialIoUart, sizeof (PepSerialIoUart));
  for (SerialIoIndex = 0; SerialIoIndex < GetPchMaxSerialIoUartControllersNum (); SerialIoIndex++) {
    PepSerialIoUart[SerialIoIndex] = mPchSetup.PchSerialIoUart[SerialIoIndex];
    if ((PepSerialIoUart[SerialIoIndex] == 0) && IsSerialIoUartFunction0Enabled (SerialIoIndex)) {
      PepSerialIoUart[SerialIoIndex] = 1;
    }
  }
  ZeroMem (PepUfs, sizeof (PepUfs));
  for (UfsIndex = 0; UfsIndex < PchGetMaxUfsNum (); UfsIndex++) {
    PepUfs[UfsIndex] = mPchSetup.PchScsUfsEnable[UfsIndex];
    if(PciSegmentRead16 (ScsUfsPciCfgBase (UfsIndex) + PCI_VENDOR_ID_OFFSET) == 0xFFFF ) {
      PepUfs[UfsIndex] = 0;
    }
  }

  //
  // Micro-PEP constraint list
  //
  mPlatformNvsAreaProtocol.Area->LowPowerS0IdleConstraint = (((mPchSetup.PchSata == 1 ? 0x3:0) & mSystemConfiguration.PepSataContraints)  <<  0) | // Bit[1:0] - Storage (0:None, 1:Adapter D0/F1, 2:Raid, 3:Adapter D3)
                                                            ((PepSerialIoUart[0] && mSystemConfiguration.PepUart)                         <<  2) | // Bit[2]   - En/Dis UART0
                                                            ((PepSerialIoUart[1] && mSystemConfiguration.PepUart)                         <<  3) | // Bit[3]   - En/Dis UART1
                                                            ((PepSerialIoI2c[0] && mSystemConfiguration.PepI2c0)                          <<  4) | // Bit[4]   - En/Dis I2C0
                                                            ((PepSerialIoI2c[1] && mSystemConfiguration.PepI2c1)                          <<  5) | // Bit[5]   - En/Dis I2C1
                                                            ( mSystemConfiguration.PepXhci                                                <<  6) | // Bit[6]   - En/Dis XHCI
                                                            (((mPchSetup.PchHdAudio == 1 ? 0x3:0) & mSystemConfiguration.PepAudio)       <<  7) | // Bit[8:7]   - HD Audio (includes ADSP) (0:No Constraint or 1: D0/F1 or 3:D3)
                                                            ( mSystemConfiguration.PepGfx                                                 <<  9) | // Bit[9]   - En/Dis GFX
                                                            ( mSystemConfiguration.PepCpu                                                 << 10) | // Bit[10]  - En/Dis CPU
                                                            ((mPchSetup.PchScsEmmcEnabled && mSystemConfiguration.PepEmmc)                << 11) | // Bit[11]  - En/Dis EMMC
                                                            (0                                                                            << 12) | // Bit[12]  - En/Dis SDXC
                                                            ((PepSerialIoI2c[2] && mSystemConfiguration.PepI2c2)                          << 13) | // Bit[13]  - En/Dis I2C2
                                                            ((PepSerialIoI2c[3] && mSystemConfiguration.PepI2c3)                          << 14) | // Bit[14]  - En/Dis I2C3
                                                            ((PepSerialIoI2c[4] && mSystemConfiguration.PepI2c4)                          << 15) | // Bit[15]  - En/Dis I2C4
                                                            ((PepSerialIoI2c[5] && mSystemConfiguration.PepI2c5)                          << 16) | // Bit[16]  - En/Dis I2C5
                                                            ((PepSerialIoUart[2] && mSystemConfiguration.PepUart)                         << 17) | // Bit[17]  - En/Dis UART2
                                                            ((PepSerialIoSpi[0] && mSystemConfiguration.PepSpi)                           << 18) | // Bit[18]  - En/Dis SPI0
                                                            ((PepSerialIoSpi[1] && mSystemConfiguration.PepSpi)                           << 19) | // Bit[19]  - En/Dis SPI1
                                                            ((PepSerialIoSpi[2] && mSystemConfiguration.PepSpi)                           << 20) | // Bit[20]   -En/Dis SPI2
                                                            ((SaSetup.SaIpuEnable && mSystemConfiguration.PepIpu)                         << 21) | // Bit[21]  - En/Dis IPU0
                                                            ( mSystemConfiguration.PepCsme                                                << 22) | // Bit[22]  - En/Dis CSME
                                                            ((mPchSetup.PchLan && mSystemConfiguration.PepGbe)                            << 23) | // Bit[23]  - En/Dis LAN(GBE)
                                                            ( 0                                                                           << 24) | // Bit[24]  - En/Dis PEG0
                                                            (mSystemConfiguration.PepThc0                                                 << 25) | // Bit[25]  - En/Dis THC0
                                                            (mSystemConfiguration.PepThc1                                                 << 26) | // Bit[26]  - En/Dis THC1
                                                            ( 0                                                                           << 27) | // Bit[27]  - Reserved
                                                            ((UINT64)(PepSerialIoI2c[6] && mSystemConfiguration.PepI2c6)                  << 28) | // Bit[28]  - En/Dis I2C6
                                                            ( mSystemConfiguration.PepTcss                                                << 29) | // Bit[29]   -En/Dis TCSS
                                                            ((SaSetup.GnaEnable && mSystemConfiguration.PepGna)                           << 30) | // Bit[30]  - En/Dis GNA0
                                                            (UINT32)(mSystemConfiguration.PepVmd                                          << 31) | // Bit[31]  - En/Dis VMD
                                                            (((UINT64)mSystemConfiguration.PepHeci3)                                      << 32) | // Bit[32]  - En/Dis HECI3
                                                            (((UINT64)mSystemConfiguration.PepPcieStorage)                                << 33) | // Bit[34:33] - PCIE Storage RP (0:No Constraint or 1: D0/F1 or 3:D3)
                                                            (((UINT64)mSystemConfiguration.PepPcieLan)                                    << 35) | // Bit[36:35] - Pcie Lan (0:No Constraint or 1:D0/F1 or 3:D3)
                                                            (((UINT64)mSystemConfiguration.PepPcieWlan)                                   << 37) | // Bit[38:37] - Pcie Wlan (0:No Constraint or 1:D0/F1 or 3:D3)
                                                            (((UINT64)mSystemConfiguration.PepPcieGfx)                                    << 39) | // Bit[40:39] - Pcie Gfx (0:No Constraint or 1:D0/F1 or 3:D3)
                                                            (((UINT64)mSystemConfiguration.PepPcieOther)                                  << 41) | // Bit[42:41] - Pcie Other (0:No Constraint or 1:D0/F1 or 3:D3)
                                                            (((UINT64)mSystemConfiguration.PepPcieDg)                                     << 43) | // Bit[43]  - En/Dis DG on x8 PEG port (PEG1)
                                                            ((UINT64)(PepUfs[0] && mSystemConfiguration.PepUfs0)                          << 44) | // Bit[44]  - En/Dis UFS0
                                                            ((UINT64)(PepUfs[1] && mSystemConfiguration.PepUfs1)                          << 45) | // Bit[45]  - En/Dis UFS1
                                                            ((UINT64)(PepSerialIoI2c[7] && mSystemConfiguration.PepI2c7)                  << 46);  // Bit[46]  - En/Dis I2C7
  DEBUG((DEBUG_INFO, "ACPI NVS, LowPowerS0IdleConstraint(Micro-pep constraints) = 0x%lX \n", mPlatformNvsAreaProtocol.Area->LowPowerS0IdleConstraint ));

  //
  // Don't enumerate D3 constraints if SATA is in RAID mode.
  // This is due to the fact that RST driver won't necessarily enumerate SATA ports according to the ACPI specification.
  // This might lead to unsatisfied PEP constraints while in RAID mode.
  //
  if (mPchSetup.PchSata && mSystemConfiguration.PepSataEnumeration && (mPchSetup.SataInterfaceMode != SATA_MODE_RAID)) {
    mPlatformNvsAreaProtocol.Area->EnumerateSataPortConstraints = TRUE;
  } else {
    mPlatformNvsAreaProtocol.Area->EnumerateSataPortConstraints = FALSE;
  }

  //
  // Assign EPI device interrupt pin and Enable/Disable Device by using PCD and pass through NVS
  //

  //
  // Disable / Enable Power meter device based on user settings
  //
  mPlatformNvsAreaProtocol.Area->PowermeterDeviceEnable = mSystemConfiguration.PowermeterDeviceEnable;

  mPlatformNvsAreaProtocol.Area->CSNotifyEC      = mSystemConfiguration.CSNotifyEC;

  mPlatformNvsAreaProtocol.Area->PL1LimitCS      = mSystemConfiguration.PL1LimitCS;
  mPlatformNvsAreaProtocol.Area->PL1LimitCSValue = mSystemConfiguration.PL1LimitCSValue;

  //
  // I2C Touch Pad Data Update to I2cTouchPad.asl
  //
  mPlatformNvsAreaProtocol.Area->TPDT           = mSystemConfiguration.PchI2cTouchPadType;
  mPlatformNvsAreaProtocol.Area->TPDM           = mSystemConfiguration.PchI2cTouchPadIrqMode;
  mPlatformNvsAreaProtocol.Area->TPDB           = mSystemConfiguration.PchI2cTouchPadBusAddress;
  mPlatformNvsAreaProtocol.Area->TPDH           = mSystemConfiguration.PchI2cTouchPadHidAddress;
  mPlatformNvsAreaProtocol.Area->TPDS           = mSystemConfiguration.PchI2cTouchPadSpeed;

  //
  // I2C Touch Panel Data Update to I2cTouchPanel.asl
  //
  mPlatformNvsAreaProtocol.Area->TPLT           = mSystemConfiguration.PchI2cTouchPanelType;
  mPlatformNvsAreaProtocol.Area->TPLM           = mSystemConfiguration.PchI2cTouchPanelIrqMode;
  mPlatformNvsAreaProtocol.Area->TPLB           = mSystemConfiguration.PchI2cTouchPanelBusAddress;
  mPlatformNvsAreaProtocol.Area->TPLH           = mSystemConfiguration.PchI2cTouchPanelHidAddress;
  mPlatformNvsAreaProtocol.Area->TPLS           = mSystemConfiguration.PchI2cTouchPanelSpeed;

  //
  // SPI Finger Print Data Update to SerialIoFingerPrint.asl
  //
  mPlatformNvsAreaProtocol.Area->FPTT           = mSystemConfiguration.PchSpiFingerPrintType;
  mPlatformNvsAreaProtocol.Area->FPTM           = mSystemConfiguration.PchSpiSensorIrqMode;

  mPlatformNvsAreaProtocol.Area->SDS0           = mSystemConfiguration.PchI2cSensorDevicePort[SERIAL_IO_I2C0];
  mPlatformNvsAreaProtocol.Area->SDS1           = mSystemConfiguration.PchI2cSensorDevicePort[SERIAL_IO_I2C1];
  mPlatformNvsAreaProtocol.Area->SDS2           = mSystemConfiguration.PchI2cSensorDevicePort[SERIAL_IO_I2C2];
  mPlatformNvsAreaProtocol.Area->SDS3           = mSystemConfiguration.PchI2cSensorDevicePort[SERIAL_IO_I2C3];
  mPlatformNvsAreaProtocol.Area->SDS4           = mSystemConfiguration.PchI2cSensorDevicePort[SERIAL_IO_I2C4];
  mPlatformNvsAreaProtocol.Area->SDS5           = mSystemConfiguration.PchI2cSensorDevicePort[SERIAL_IO_I2C5];
  //mPlatformNvsAreaProtocol.Area->SDS6           = mSystemConfiguration.PchSpi0SensorDevice;
  //mPlatformNvsAreaProtocol.Area->SDM6           = mSystemConfiguration.PchI2c0SensorIrqMode;
  mPlatformNvsAreaProtocol.Area->SDS7           = mSystemConfiguration.PchSpiSensorDevicePort[SERIAL_IO_SPI1];
  mPlatformNvsAreaProtocol.Area->SDS8           = mSystemConfiguration.PchSpiSensorDevicePort[SERIAL_IO_SPI2];

  mPlatformNvsAreaProtocol.Area->SDS9           = mSystemConfiguration.DiscreteBtModule;
  mPlatformNvsAreaProtocol.Area->SDM9           = mSystemConfiguration.DiscreteBtModuleIrqMode;

  mPlatformNvsAreaProtocol.Area->WTVX           = mSystemConfiguration.PchI2cWittVersion;
  mPlatformNvsAreaProtocol.Area->WITX           = mSystemConfiguration.PchI2cWittDevice;
  mPlatformNvsAreaProtocol.Area->UTKX           = mSystemConfiguration.PchUartUtkDevice;
  mPlatformNvsAreaProtocol.Area->GDBT           = mPchSetup.PchGpioDebounce;
  mPlatformNvsAreaProtocol.Area->GPTD           = mPchSetup.PchGpioTestDevices;
  mPlatformNvsAreaProtocol.Area->SPTD           = mPchSetup.PchAdditionalSerialIoDevices;


#if FixedPcdGet8(PcdEmbeddedEnable) == 0x1
  mPlatformNvsAreaProtocol.Area->TsnPcsEnabled  = mPchSetup.TsnPcsEnabled;
#else
  mPlatformNvsAreaProtocol.Area->TsnPcsEnabled  = 0;
#endif

  //
  // UCMC support
  //
  mPlatformNvsAreaProtocol.Area->UCMS           = mSystemConfiguration.TcssUcmDevice;

  if (PcdGet32 (PcdBoardUcmcGpio1)) {
    mPlatformNvsAreaProtocol.Area->UcmcPort1Gpio = PcdGet32 (PcdBoardUcmcGpio1);
  }
  if (PcdGet32 (PcdBoardUcmcGpio2)) {
    mPlatformNvsAreaProtocol.Area->UcmcPort2Gpio = PcdGet32 (PcdBoardUcmcGpio2);
  }
  if (PcdGet32 (PcdBoardUcmcGpio3)) {
    mPlatformNvsAreaProtocol.Area->UcmcPort3Gpio = PcdGet32 (PcdBoardUcmcGpio3);
  }
  if (PcdGet32 (PcdBoardUcmcGpio4)) {
    mPlatformNvsAreaProtocol.Area->UcmcPort4Gpio = PcdGet32 (PcdBoardUcmcGpio4);
  }

  //
  // Serial IO timing parameters
  //
  mPlatformNvsAreaProtocol.Area->USTP = mSystemConfiguration.PchSerialIoUseTimingParameters;

  mPlatformNvsAreaProtocol.Area->SSH0 = mSystemConfiguration.PchSerialIoTimingSSH0;  // [SSH0] SSCN-HIGH for I2C0
  mPlatformNvsAreaProtocol.Area->SSL0 = mSystemConfiguration.PchSerialIoTimingSSL0;  // [SSL0] SSCN-LOW  for I2C0
  mPlatformNvsAreaProtocol.Area->SSD0 = mSystemConfiguration.PchSerialIoTimingSSD0;  // [SSD0] SSCN-HOLD for I2C0
  mPlatformNvsAreaProtocol.Area->FMH0 = mSystemConfiguration.PchSerialIoTimingFMH0;  // [FMH0] FMCN-HIGH for I2C0
  mPlatformNvsAreaProtocol.Area->FML0 = mSystemConfiguration.PchSerialIoTimingFML0;  // [FML0] FMCN-LOW  for I2C0
  mPlatformNvsAreaProtocol.Area->FMD0 = mSystemConfiguration.PchSerialIoTimingFMD0;  // [FMD0] FMCN-HOLD for I2C0
  mPlatformNvsAreaProtocol.Area->FPH0 = mSystemConfiguration.PchSerialIoTimingFPH0;  // [FPH0] FPCN-HIGH for I2C0
  mPlatformNvsAreaProtocol.Area->FPL0 = mSystemConfiguration.PchSerialIoTimingFPL0;  // [FPL0] FPCN-LOW  for I2C0
  mPlatformNvsAreaProtocol.Area->FPD0 = mSystemConfiguration.PchSerialIoTimingFPD0;  // [FPD0] FPCN-HOLD for I2C0
  mPlatformNvsAreaProtocol.Area->HSH0 = mSystemConfiguration.PchSerialIoTimingHSH0;  // [HSH0] HSCN-HIGH for I2C0
  mPlatformNvsAreaProtocol.Area->HSL0 = mSystemConfiguration.PchSerialIoTimingHSL0;  // [HSL0] HSCN-LOW  for I2C0
  mPlatformNvsAreaProtocol.Area->HSD0 = mSystemConfiguration.PchSerialIoTimingHSD0;  // [HSD0] HSCN-HOLD for I2C0

  mPlatformNvsAreaProtocol.Area->SSH1 = mSystemConfiguration.PchSerialIoTimingSSH1;  // [SSH1] SSCN-HIGH for I2C1
  mPlatformNvsAreaProtocol.Area->SSL1 = mSystemConfiguration.PchSerialIoTimingSSL1;  // [SSL1] SSCN-LOW  for I2C1
  mPlatformNvsAreaProtocol.Area->SSD1 = mSystemConfiguration.PchSerialIoTimingSSD1;  // [SSD1] SSCN-HOLD for I2C1
  mPlatformNvsAreaProtocol.Area->FMH1 = mSystemConfiguration.PchSerialIoTimingFMH1;  // [FMH1] FMCN-HIGH for I2C1
  mPlatformNvsAreaProtocol.Area->FML1 = mSystemConfiguration.PchSerialIoTimingFML1;  // [FML1] FMCN-LOW  for I2C1
  mPlatformNvsAreaProtocol.Area->FMD1 = mSystemConfiguration.PchSerialIoTimingFMD1;  // [FMD1] FMCN-HOLD for I2C1
  mPlatformNvsAreaProtocol.Area->FPH1 = mSystemConfiguration.PchSerialIoTimingFPH1;  // [FPH1] FPCN-HIGH for I2C1
  mPlatformNvsAreaProtocol.Area->FPL1 = mSystemConfiguration.PchSerialIoTimingFPL1;  // [FPL1] FPCN-LOW  for I2C1
  mPlatformNvsAreaProtocol.Area->FPD1 = mSystemConfiguration.PchSerialIoTimingFPD1;  // [FPD1] FPCN-HOLD for I2C1
  mPlatformNvsAreaProtocol.Area->HSH1 = mSystemConfiguration.PchSerialIoTimingHSH1;  // [HSH1] HSCN-HIGH for I2C1
  mPlatformNvsAreaProtocol.Area->HSL1 = mSystemConfiguration.PchSerialIoTimingHSL1;  // [HSL1] HSCN-LOW  for I2C1
  mPlatformNvsAreaProtocol.Area->HSD1 = mSystemConfiguration.PchSerialIoTimingHSD1;  // [HSD1] HSCN-HOLD for I2C1

  mPlatformNvsAreaProtocol.Area->SSH2 = mSystemConfiguration.PchSerialIoTimingSSH2;  // [SSH2] SSCN-HIGH for I2C2
  mPlatformNvsAreaProtocol.Area->SSL2 = mSystemConfiguration.PchSerialIoTimingSSL2;  // [SSL2] SSCN-LOW  for I2C2
  mPlatformNvsAreaProtocol.Area->SSD2 = mSystemConfiguration.PchSerialIoTimingSSD2;  // [SSD2] SSCN-HOLD for I2C2
  mPlatformNvsAreaProtocol.Area->FMH2 = mSystemConfiguration.PchSerialIoTimingFMH2;  // [FMH2] FMCN-HIGH for I2C2
  mPlatformNvsAreaProtocol.Area->FML2 = mSystemConfiguration.PchSerialIoTimingFML2;  // [FML2] FMCN-LOW  for I2C2
  mPlatformNvsAreaProtocol.Area->FMD2 = mSystemConfiguration.PchSerialIoTimingFMD2;  // [FMD2] FMCN-HOLD for I2C2
  mPlatformNvsAreaProtocol.Area->FPH2 = mSystemConfiguration.PchSerialIoTimingFPH2;  // [FPH2] FPCN-HIGH for I2C2
  mPlatformNvsAreaProtocol.Area->FPL2 = mSystemConfiguration.PchSerialIoTimingFPL2;  // [FPL2] FPCN-LOW  for I2C2
  mPlatformNvsAreaProtocol.Area->FPD2 = mSystemConfiguration.PchSerialIoTimingFPD2;  // [FPD2] FPCN-HOLD for I2C2
  mPlatformNvsAreaProtocol.Area->HSH2 = mSystemConfiguration.PchSerialIoTimingHSH2;  // [HSH2] HSCN-HIGH for I2C2
  mPlatformNvsAreaProtocol.Area->HSL2 = mSystemConfiguration.PchSerialIoTimingHSL2;  // [HSL2] HSCN-LOW  for I2C2
  mPlatformNvsAreaProtocol.Area->HSD2 = mSystemConfiguration.PchSerialIoTimingHSD2;  // [HSD2] HSCN-HOLD for I2C2

  mPlatformNvsAreaProtocol.Area->SSH3 = mSystemConfiguration.PchSerialIoTimingSSH3;  // [SSH3] SSCN-HIGH for I2C3
  mPlatformNvsAreaProtocol.Area->SSL3 = mSystemConfiguration.PchSerialIoTimingSSL3;  // [SSL3] SSCN-LOW  for I2C3
  mPlatformNvsAreaProtocol.Area->SSD3 = mSystemConfiguration.PchSerialIoTimingSSD3;  // [SSD3] SSCN-HOLD for I2C3
  mPlatformNvsAreaProtocol.Area->FMH3 = mSystemConfiguration.PchSerialIoTimingFMH3;  // [FMH3] FMCN-HIGH for I2C3
  mPlatformNvsAreaProtocol.Area->FML3 = mSystemConfiguration.PchSerialIoTimingFML3;  // [FML3] FMCN-LOW  for I2C3
  mPlatformNvsAreaProtocol.Area->FMD3 = mSystemConfiguration.PchSerialIoTimingFMD3;  // [FMD3] FMCN-HOLD for I2C3
  mPlatformNvsAreaProtocol.Area->FPH3 = mSystemConfiguration.PchSerialIoTimingFPH3;  // [FPH3] FPCN-HIGH for I2C3
  mPlatformNvsAreaProtocol.Area->FPL3 = mSystemConfiguration.PchSerialIoTimingFPL3;  // [FPL3] FPCN-LOW  for I2C3
  mPlatformNvsAreaProtocol.Area->FPD3 = mSystemConfiguration.PchSerialIoTimingFPD3;  // [FPD3] FPCN-HOLD for I2C3
  mPlatformNvsAreaProtocol.Area->HSH3 = mSystemConfiguration.PchSerialIoTimingHSH3;  // [HSH3] HSCN-HIGH for I2C3
  mPlatformNvsAreaProtocol.Area->HSL3 = mSystemConfiguration.PchSerialIoTimingHSL3;  // [HSL3] HSCN-LOW  for I2C3
  mPlatformNvsAreaProtocol.Area->HSD3 = mSystemConfiguration.PchSerialIoTimingHSD3;  // [HSD3] HSCN-HOLD for I2C3

  mPlatformNvsAreaProtocol.Area->SSH4 = mSystemConfiguration.PchSerialIoTimingSSH4;  // [SSH4] SSCN-HIGH for I2C4
  mPlatformNvsAreaProtocol.Area->SSL4 = mSystemConfiguration.PchSerialIoTimingSSL4;  // [SSL4] SSCN-LOW  for I2C4
  mPlatformNvsAreaProtocol.Area->SSD4 = mSystemConfiguration.PchSerialIoTimingSSD4;  // [SSD4] SSCN-HOLD for I2C4
  mPlatformNvsAreaProtocol.Area->FMH4 = mSystemConfiguration.PchSerialIoTimingFMH4;  // [FMH4] FMCN-HIGH for I2C4
  mPlatformNvsAreaProtocol.Area->FML4 = mSystemConfiguration.PchSerialIoTimingFML4;  // [FML4] FMCN-LOW  for I2C4
  mPlatformNvsAreaProtocol.Area->FMD4 = mSystemConfiguration.PchSerialIoTimingFMD4;  // [FMD4] FMCN-HOLD for I2C4
  mPlatformNvsAreaProtocol.Area->FPH4 = mSystemConfiguration.PchSerialIoTimingFPH4;  // [FPH4] FPCN-HIGH for I2C4
  mPlatformNvsAreaProtocol.Area->FPL4 = mSystemConfiguration.PchSerialIoTimingFPL4;  // [FPL4] FPCN-LOW  for I2C4
  mPlatformNvsAreaProtocol.Area->FPD4 = mSystemConfiguration.PchSerialIoTimingFPD4;  // [FPD4] FPCN-HOLD for I2C4
  mPlatformNvsAreaProtocol.Area->HSH4 = mSystemConfiguration.PchSerialIoTimingHSH4;  // [HSH4] HSCN-HIGH for I2C4
  mPlatformNvsAreaProtocol.Area->HSL4 = mSystemConfiguration.PchSerialIoTimingHSL4;  // [HSL4] HSCN-LOW  for I2C4
  mPlatformNvsAreaProtocol.Area->HSD4 = mSystemConfiguration.PchSerialIoTimingHSD4;  // [HSD4] HSCN-HOLD for I2C4

  mPlatformNvsAreaProtocol.Area->SSH5 = mSystemConfiguration.PchSerialIoTimingSSH5;  // [SSH5] SSCN-HIGH for I2C5
  mPlatformNvsAreaProtocol.Area->SSL5 = mSystemConfiguration.PchSerialIoTimingSSL5;  // [SSL5] SSCN-LOW  for I2C5
  mPlatformNvsAreaProtocol.Area->SSD5 = mSystemConfiguration.PchSerialIoTimingSSD5;  // [SSD5] SSCN-HOLD for I2C5
  mPlatformNvsAreaProtocol.Area->FMH5 = mSystemConfiguration.PchSerialIoTimingFMH5;  // [FMH5] FMCN-HIGH for I2C5
  mPlatformNvsAreaProtocol.Area->FML5 = mSystemConfiguration.PchSerialIoTimingFML5;  // [FML5] FMCN-LOW  for I2C5
  mPlatformNvsAreaProtocol.Area->FMD5 = mSystemConfiguration.PchSerialIoTimingFMD5;  // [FMD5] FMCN-HOLD for I2C5
  mPlatformNvsAreaProtocol.Area->FPH5 = mSystemConfiguration.PchSerialIoTimingFPH5;  // [FPH5] FPCN-HIGH for I2C5
  mPlatformNvsAreaProtocol.Area->FPL5 = mSystemConfiguration.PchSerialIoTimingFPL5;  // [FPL5] FPCN-LOW  for I2C5
  mPlatformNvsAreaProtocol.Area->FPD5 = mSystemConfiguration.PchSerialIoTimingFPD5;  // [FPD5] FPCN-HOLD for I2C5
  mPlatformNvsAreaProtocol.Area->HSH5 = mSystemConfiguration.PchSerialIoTimingHSH5;  // [HSH5] HSCN-HIGH for I2C5
  mPlatformNvsAreaProtocol.Area->HSL5 = mSystemConfiguration.PchSerialIoTimingHSL5;  // [HSL5] HSCN-LOW  for I2C5
  mPlatformNvsAreaProtocol.Area->HSD5 = mSystemConfiguration.PchSerialIoTimingHSD5;  // [HSD5] HSCN-HOLD for I2C5

  mPlatformNvsAreaProtocol.Area->SSH6 = mSystemConfiguration.PchSerialIoTimingSSH6;  // [SSH6] SSCN-HIGH for I2C6
  mPlatformNvsAreaProtocol.Area->SSL6 = mSystemConfiguration.PchSerialIoTimingSSL6;  // [SSL6] SSCN-LOW  for I2C6
  mPlatformNvsAreaProtocol.Area->SSD6 = mSystemConfiguration.PchSerialIoTimingSSD6;  // [SSD6] SSCN-HOLD for I2C6
  mPlatformNvsAreaProtocol.Area->FMH6 = mSystemConfiguration.PchSerialIoTimingFMH6;  // [FMH6] FMCN-HIGH for I2C6
  mPlatformNvsAreaProtocol.Area->FML6 = mSystemConfiguration.PchSerialIoTimingFML6;  // [FML6] FMCN-LOW  for I2C6
  mPlatformNvsAreaProtocol.Area->FMD6 = mSystemConfiguration.PchSerialIoTimingFMD6;  // [FMD6] FMCN-HOLD for I2C6
  mPlatformNvsAreaProtocol.Area->FPH6 = mSystemConfiguration.PchSerialIoTimingFPH6;  // [FPH6] FPCN-HIGH for I2C6
  mPlatformNvsAreaProtocol.Area->FPL6 = mSystemConfiguration.PchSerialIoTimingFPL6;  // [FPL6] FPCN-LOW  for I2C6
  mPlatformNvsAreaProtocol.Area->FPD6 = mSystemConfiguration.PchSerialIoTimingFPD6;  // [FPD6] FPCN-HOLD for I2C6
  mPlatformNvsAreaProtocol.Area->HSH6 = mSystemConfiguration.PchSerialIoTimingHSH6;  // [HSH6] HSCN-HIGH for I2C6
  mPlatformNvsAreaProtocol.Area->HSL6 = mSystemConfiguration.PchSerialIoTimingHSL6;  // [HSL6] HSCN-LOW  for I2C6
  mPlatformNvsAreaProtocol.Area->HSD6 = mSystemConfiguration.PchSerialIoTimingHSD6;  // [HSD6] HSCN-HOLD for I2C6

  mPlatformNvsAreaProtocol.Area->SSH7 = mSystemConfiguration.PchSerialIoTimingSSH7;  // [SSH7] SSCN-HIGH for I2C7
  mPlatformNvsAreaProtocol.Area->SSL7 = mSystemConfiguration.PchSerialIoTimingSSL7;  // [SSL7] SSCN-LOW  for I2C7
  mPlatformNvsAreaProtocol.Area->SSD7 = mSystemConfiguration.PchSerialIoTimingSSD7;  // [SSD7] SSCN-HOLD for I2C7
  mPlatformNvsAreaProtocol.Area->FMH7 = mSystemConfiguration.PchSerialIoTimingFMH7;  // [FMH7] FMCN-HIGH for I2C7
  mPlatformNvsAreaProtocol.Area->FML7 = mSystemConfiguration.PchSerialIoTimingFML7;  // [FML7] FMCN-LOW  for I2C7
  mPlatformNvsAreaProtocol.Area->FMD7 = mSystemConfiguration.PchSerialIoTimingFMD7;  // [FMD7] FMCN-HOLD for I2C7
  mPlatformNvsAreaProtocol.Area->FPH7 = mSystemConfiguration.PchSerialIoTimingFPH7;  // [FPH7] FPCN-HIGH for I2C7
  mPlatformNvsAreaProtocol.Area->FPL7 = mSystemConfiguration.PchSerialIoTimingFPL7;  // [FPL7] FPCN-LOW  for I2C7
  mPlatformNvsAreaProtocol.Area->FPD7 = mSystemConfiguration.PchSerialIoTimingFPD7;  // [FPD7] FPCN-HOLD for I2C7
  mPlatformNvsAreaProtocol.Area->HSH7 = mSystemConfiguration.PchSerialIoTimingHSH7;  // [HSH7] HSCN-HIGH for I2C7
  mPlatformNvsAreaProtocol.Area->HSL7 = mSystemConfiguration.PchSerialIoTimingHSL7;  // [HSL7] HSCN-LOW  for I2C7
  mPlatformNvsAreaProtocol.Area->HSD7 = mSystemConfiguration.PchSerialIoTimingHSD7;  // [HSD7] HSCN-HOLD for I2C7

  mPlatformNvsAreaProtocol.Area->M0C0 = mSystemConfiguration.PchSerialIoTimingM0C0;  // [M0C0] M0D3 for I2C0
  mPlatformNvsAreaProtocol.Area->M1C0 = mSystemConfiguration.PchSerialIoTimingM1C0;  // [M1C0] M1D3 for I2C0
  mPlatformNvsAreaProtocol.Area->M0C1 = mSystemConfiguration.PchSerialIoTimingM0C1;  // [M0C1] M0D3 for I2C1
  mPlatformNvsAreaProtocol.Area->M1C1 = mSystemConfiguration.PchSerialIoTimingM1C1;  // [M1C1] M1D3 for I2C1
  mPlatformNvsAreaProtocol.Area->M0C2 = mSystemConfiguration.PchSerialIoTimingM0C2;  // [M0C2] M0D3 for I2C2
  mPlatformNvsAreaProtocol.Area->M1C2 = mSystemConfiguration.PchSerialIoTimingM1C2;  // [M1C2] M1D3 for I2C2
  mPlatformNvsAreaProtocol.Area->M0C3 = mSystemConfiguration.PchSerialIoTimingM0C3;  // [M0C3] M0D3 for I2C3
  mPlatformNvsAreaProtocol.Area->M1C3 = mSystemConfiguration.PchSerialIoTimingM1C3;  // [M1C3] M1D3 for I2C3
  mPlatformNvsAreaProtocol.Area->M0C4 = mSystemConfiguration.PchSerialIoTimingM0C4;  // [M0C4] M0D3 for I2C4
  mPlatformNvsAreaProtocol.Area->M1C4 = mSystemConfiguration.PchSerialIoTimingM1C4;  // [M1C4] M1D3 for I2C4
  mPlatformNvsAreaProtocol.Area->M0C5 = mSystemConfiguration.PchSerialIoTimingM0C5;  // [M0C5] M0D3 for I2C5
  mPlatformNvsAreaProtocol.Area->M1C5 = mSystemConfiguration.PchSerialIoTimingM1C5;  // [M1C5] M1D3 for I2C5
  mPlatformNvsAreaProtocol.Area->M0CC = mSystemConfiguration.PchSerialIoTimingM0CC;  // [M0CC] M0D3 for I2C6
  mPlatformNvsAreaProtocol.Area->M0CC = mSystemConfiguration.PchSerialIoTimingM0CC;  // [M0CC] M1D3 for I2C6
  mPlatformNvsAreaProtocol.Area->M0CD = mSystemConfiguration.PchSerialIoTimingM0CD;  // [M0CD] M0D3 for I2C7
  mPlatformNvsAreaProtocol.Area->M0CD = mSystemConfiguration.PchSerialIoTimingM0CD;  // [M0CD] M1D3 for I2C7
  mPlatformNvsAreaProtocol.Area->M0C6 = mSystemConfiguration.PchSerialIoTimingM0C6;  // [M0C6] M0D3 for SPI0
  mPlatformNvsAreaProtocol.Area->M1C6 = mSystemConfiguration.PchSerialIoTimingM1C6;  // [M1C6] M1D3 for SPI0
  mPlatformNvsAreaProtocol.Area->M0C7 = mSystemConfiguration.PchSerialIoTimingM0C7;  // [M0C7] M0D3 for SPI1
  mPlatformNvsAreaProtocol.Area->M1C7 = mSystemConfiguration.PchSerialIoTimingM1C7;  // [M1C7] M1D3 for SPI1
  mPlatformNvsAreaProtocol.Area->M0C8 = mSystemConfiguration.PchSerialIoTimingM0C8;  // [M0C8] M0D3 for SPI2
  mPlatformNvsAreaProtocol.Area->M1C8 = mSystemConfiguration.PchSerialIoTimingM1C8;  // [M1C8] M1D3 for SPI2
  mPlatformNvsAreaProtocol.Area->M0C9 = mSystemConfiguration.PchSerialIoTimingM0C9;  // [M0C9] M0D3 for UART0
  mPlatformNvsAreaProtocol.Area->M1C9 = mSystemConfiguration.PchSerialIoTimingM1C9;  // [M1C9] M1D3 for UART0
  mPlatformNvsAreaProtocol.Area->M0CA = mSystemConfiguration.PchSerialIoTimingM0CA;  // [M0CA] M0D3 for UART1
  mPlatformNvsAreaProtocol.Area->M1CA = mSystemConfiguration.PchSerialIoTimingM1CA;  // [M1CA] M1D3 for UART1
  mPlatformNvsAreaProtocol.Area->M0CB = mSystemConfiguration.PchSerialIoTimingM0CB;  // [M0CB] M0D3 for UART2
  mPlatformNvsAreaProtocol.Area->M1CB = mSystemConfiguration.PchSerialIoTimingM1CB;  // [M1CB] M1D3 for UART2

  mPlatformNvsAreaProtocol.Area->FingerPrintSleepGpio        = PcdGet32 (PcdFingerPrintSleepGpio);
  mPlatformNvsAreaProtocol.Area->FingerPrintIrqGpio          = PcdGet32 (PcdFingerPrintIrqGpio);
  mPlatformNvsAreaProtocol.Area->WwanModemBaseBandResetGpio  = PcdGet32(PcdWwanModemBaseBandResetGpio);
  mPlatformNvsAreaProtocol.Area->DiscreteBtModuleRfKillGpio  = PcdGet32 (PcdBtRfKillGpio);
  mPlatformNvsAreaProtocol.Area->DiscreteBtModuleIrqGpio     = PcdGet32 (PcdBtIrqGpio);
  mPlatformNvsAreaProtocol.Area->TouchpadIrqGpio             = PcdGet32 (PcdTouchpadIrqGpio);
  mPlatformNvsAreaProtocol.Area->TouchpanelIrqGpio           = PcdGet32 (PcdTouchpanelIrqGpio);

  mPlatformNvsAreaProtocol.Area->Revision       = PLATFORM_NVS_AREA_REVISION_1; // for TPM related fields
//[-start-190620-IB11270237-modify]//
//  if (mSystemConfiguration.FastBoot == 0) {
//    mPlatformNvsAreaProtocol.Area->Ps2MouseEnable = IsPs2MouseConnected ();
//  } else {
//  #if FixedPcdGetBool (PcdAdlLpSupport) == 1
//    mPlatformNvsAreaProtocol.Area->Ps2MouseEnable = 0;
//  #else
//    mPlatformNvsAreaProtocol.Area->Ps2MouseEnable = 1;
//  #endif
//  }
  mPlatformNvsAreaProtocol.Area->Ps2MouseEnable = 1; //IsPs2MouseConnected ();
//[-end-190620-IB11270237-modify]//
  mPlatformNvsAreaProtocol.Area->Ps2KbMsEnable  = mSystemConfiguration.Ps2KbMsEnable;

#if FixedPcdGetBool(PcdAdlLpSupport) == 1
  mPlatformNvsAreaProtocol.Area->ClosedLidWovLighting = mSystemConfiguration.ClosedLidWovLightingSupport;
  mPlatformNvsAreaProtocol.Area->ClwlI2cController = PcdGet8 (PcdClwlI2cController);
  mPlatformNvsAreaProtocol.Area->ClwlI2cSlaveAddress = PcdGet8 (PcdClwlI2cSlaveAddress);
  DEBUG ((DEBUG_INFO,"ClosedLidWovLighting support = %x\n", mPlatformNvsAreaProtocol.Area->ClosedLidWovLighting));
  DEBUG ((DEBUG_INFO,"ClwlI2cController  = %x\n", mPlatformNvsAreaProtocol.Area->ClwlI2cController));
  DEBUG ((DEBUG_INFO,"ClwlI2cSlaveAddress  = %x\n", mPlatformNvsAreaProtocol.Area->ClwlI2cSlaveAddress));
#endif
//[-start-190620-IB11270237-add]//
  Status = EfiCreateEventReadyToBootEx (
             TPL_CALLBACK,
             UpdatePs2MouseStatus,
             mPlatformNvsAreaProtocol.Area,
             &EventOutPut
             );
  if (EFI_ERROR (Status)) {
    DEBUG ((EFI_D_ERROR | EFI_D_INFO, "Create Check PS2M Event fail: %x", Status));
  }
//[-end-190620-IB11270237-add]//
  //
  // Feature Specific RVP Details
  //
  mPlatformNvsAreaProtocol.Area->UsbTypeCSupport                      = PcdGetBool (PcdUsbTypeCSupport);
  mPlatformNvsAreaProtocol.Area->UsbTypeCEcLess                       = PcdGet8 (PcdUsbTypeCEcLess);
  mPlatformNvsAreaProtocol.Area->PcdTsOnDimmTemperature               = PcdGetBool (PcdTsOnDimmTemperature);
  mPlatformNvsAreaProtocol.Area->PcdRealBattery1Control               = PcdGet8 (PcdRealBattery1Control);
  mPlatformNvsAreaProtocol.Area->PcdRealBattery2Control               = PcdGet8 (PcdRealBattery2Control);
  mPlatformNvsAreaProtocol.Area->PcdMipiCamSensor                     = PcdGetBool (PcdMipiCamSensor);
  mPlatformNvsAreaProtocol.Area->PcdNCT6776FCOM                       = PcdGetBool (PcdNCT6776FCOM);
  mPlatformNvsAreaProtocol.Area->PcdNCT6776FSIO                       = PcdGetBool (PcdNCT6776FSIO);
  mPlatformNvsAreaProtocol.Area->PcdNCT6776FHWMON                     = PcdGetBool (PcdNCT6776FHWMON);
  mPlatformNvsAreaProtocol.Area->PcdH8S2113SIO                        = PcdGetBool (PcdH8S2113SIO);
  mPlatformNvsAreaProtocol.Area->PcdH8S2113UAR                        = PcdGetBool (PcdH8S2113UAR);
  mPlatformNvsAreaProtocol.Area->PcdSmcRuntimeSciPin                  = PcdGet32 (PcdSmcRuntimeSciPin);
  mPlatformNvsAreaProtocol.Area->PcdConvertableDockSupport            = PcdGetBool (PcdConvertableDockSupport);
  mPlatformNvsAreaProtocol.Area->PcdEcHotKeyF3Support                 = PcdGet8 (PcdEcHotKeyF3Support);
  mPlatformNvsAreaProtocol.Area->PcdEcHotKeyF4Support                 = PcdGet8 (PcdEcHotKeyF4Support);
  mPlatformNvsAreaProtocol.Area->PcdEcHotKeyF5Support                 = PcdGet8 (PcdEcHotKeyF5Support);
  mPlatformNvsAreaProtocol.Area->PcdEcHotKeyF6Support                 = PcdGet8 (PcdEcHotKeyF6Support);
  mPlatformNvsAreaProtocol.Area->PcdEcHotKeyF7Support                 = PcdGet8 (PcdEcHotKeyF7Support);
  mPlatformNvsAreaProtocol.Area->PcdEcHotKeyF8Support                 = PcdGet8 (PcdEcHotKeyF8Support);
  mPlatformNvsAreaProtocol.Area->PcdVirtualButtonVolumeUpSupport      = PcdGetBool (PcdVirtualButtonVolumeUpSupport);
  mPlatformNvsAreaProtocol.Area->PcdVirtualButtonVolumeDownSupport    = PcdGetBool (PcdVirtualButtonVolumeDownSupport);
  mPlatformNvsAreaProtocol.Area->PcdVirtualButtonHomeButtonSupport    = PcdGetBool (PcdVirtualButtonHomeButtonSupport);
  mPlatformNvsAreaProtocol.Area->PcdVirtualButtonRotationLockSupport  = PcdGetBool (PcdVirtualButtonRotationLockSupport);
  mPlatformNvsAreaProtocol.Area->PcdSlateModeSwitchSupport            = PcdGetBool (PcdSlateModeSwitchSupport);
  mPlatformNvsAreaProtocol.Area->PcdAcDcAutoSwitchSupport             = PcdGetBool (PcdAcDcAutoSwitchSupport);
  mPlatformNvsAreaProtocol.Area->PcdPmPowerButtonGpioPin              = PcdGet32 (PcdPmPowerButtonGpioPin);
  mPlatformNvsAreaProtocol.Area->PcdAcpiEnableAllButtonSupport        = PcdGetBool (PcdAcpiEnableAllButtonSupport);
  mPlatformNvsAreaProtocol.Area->PcdAcpiHidDriverButtonSupport        = PcdGetBool (PcdAcpiHidDriverButtonSupport);
  mPlatformNvsAreaProtocol.Area->PcdBatteryPresent                    = PcdGet8 (PcdBatteryPresent);

  // PD PS_ON GPIO
  mPlatformNvsAreaProtocol.Area->PPOE              = mSystemConfiguration.TcssPdPsOnEnable;
  mPlatformNvsAreaProtocol.Area->POVP              = PcdGet32 (PcdUsbCPsonOverrideN);
  mPlatformNvsAreaProtocol.Area->PSG1              = PcdGet32 (PcdUsbCPsonS0ixEntryReq);
  mPlatformNvsAreaProtocol.Area->PSG2              = PcdGet32 (PcdUsbCPsonS0ixEntryAck);

  mPlatformNvsAreaProtocol.Area->DPIW              = mSystemConfiguration.DpInExternalEn;
  mPlatformNvsAreaProtocol.Area->DPG1              = PcdGet32 (PcdDpMuxGpio);

  // USBC DR_SWAP
  mPlatformNvsAreaProtocol.Area->UsbcDataRoleSwap = (mSystemConfiguration.UsbcDataRoleSwapPlatformDisable) ? 1 : 0;

  //
  // Always report PS2MouseEnable for non-EC board
  //
  if (mPlatformNvsAreaProtocol.Area->EcAvailable == PLATFORM_NVS_DEVICE_DISABLE) {
    mPlatformNvsAreaProtocol.Area->Ps2MouseEnable = 1;
  }

  mPlatformNvsAreaProtocol.Area->UsbSensorHub    = mSystemConfiguration.UsbSensorHub;

  //
  // CVF configuration
  //

  mPlatformNvsAreaProtocol.Area->CvfSupport = mSystemConfiguration.CvfSupport;
  mPlatformNvsAreaProtocol.Area->CvfUsbPort = PcdGet8 (PcdCvfUsbPort);
  //
  // MipiCam sensor configuration
  //

  mPlatformNvsAreaProtocol.Area->MipiCamControlLogic0 = mSystemConfiguration.MipiCam_ControlLogic0;
  mPlatformNvsAreaProtocol.Area->MipiCamControlLogic1 = mSystemConfiguration.MipiCam_ControlLogic1;
  mPlatformNvsAreaProtocol.Area->MipiCamControlLogic2 = mSystemConfiguration.MipiCam_ControlLogic2;
  mPlatformNvsAreaProtocol.Area->MipiCamControlLogic3 = mSystemConfiguration.MipiCam_ControlLogic3;
  mPlatformNvsAreaProtocol.Area->MipiCamControlLogic4 = mSystemConfiguration.MipiCam_ControlLogic4;
  mPlatformNvsAreaProtocol.Area->MipiCamControlLogic5 = mSystemConfiguration.MipiCam_ControlLogic5;
  mPlatformNvsAreaProtocol.Area->MipiCamLink0Enabled = mSystemConfiguration.MipiCam_Link0;
  mPlatformNvsAreaProtocol.Area->MipiCamLink1Enabled = mSystemConfiguration.MipiCam_Link1;
  mPlatformNvsAreaProtocol.Area->MipiCamLink2Enabled = mSystemConfiguration.MipiCam_Link2;
  mPlatformNvsAreaProtocol.Area->MipiCamLink3Enabled = mSystemConfiguration.MipiCam_Link3;
  mPlatformNvsAreaProtocol.Area->MipiCamLink4Enabled = mSystemConfiguration.MipiCam_Link4;
  mPlatformNvsAreaProtocol.Area->MipiCamLink5Enabled = mSystemConfiguration.MipiCam_Link5;

  // MipiCam Control Logic 0 specific
  mPlatformNvsAreaProtocol.Area->MipiCamCtrlLogic0_Version = 1;
  mPlatformNvsAreaProtocol.Area->MipiCamCtrlLogic0_Type = mSystemConfiguration.MipiCam_ControlLogic0_Type;
  mPlatformNvsAreaProtocol.Area->MipiCamCtrlLogic0_CrdVersion = mSystemConfiguration.MipiCam_ControlLogic0_CrdVersion;
  mPlatformNvsAreaProtocol.Area->MipiCamCtrlLogic0_InputClock = mSystemConfiguration.MipiCam_ControlLogic0_InputClock;
  mPlatformNvsAreaProtocol.Area->MipiCamCtrlLogic0_GpioPinsEnabled = mSystemConfiguration.MipiCam_ControlLogic0_GpioPinsEnabled;
  mPlatformNvsAreaProtocol.Area->MipiCamCtrlLogic0_I2cBus = mSystemConfiguration.MipiCam_ControlLogic0_I2cChannel;
  mPlatformNvsAreaProtocol.Area->MipiCamCtrlLogic0_I2cAddress = mSystemConfiguration.MipiCam_ControlLogic0_I2cAddress;
  mPlatformNvsAreaProtocol.Area->MipiCamCtrlLogic0_Pld = mSystemConfiguration.MipiCam_ControlLogic0_Pld;
  mPlatformNvsAreaProtocol.Area->MipiCamCtrlLogic0_Wled1FlashMaxCurrent = mSystemConfiguration.MipiCam_ControlLogic0_Wled1FlashMaxCurrent;
  mPlatformNvsAreaProtocol.Area->MipiCamCtrlLogic0_Wled1TorchMaxCurrent = mSystemConfiguration.MipiCam_ControlLogic0_Wled1TorchMaxCurrent;
  mPlatformNvsAreaProtocol.Area->MipiCamCtrlLogic0_Wled2FlashMaxCurrent = mSystemConfiguration.MipiCam_ControlLogic0_Wled2FlashMaxCurrent;
  mPlatformNvsAreaProtocol.Area->MipiCamCtrlLogic0_Wled2TorchMaxCurrent = mSystemConfiguration.MipiCam_ControlLogic0_Wled2TorchMaxCurrent;
  mPlatformNvsAreaProtocol.Area->MipiCamCtrlLogic0_SubPlatformId = mSystemConfiguration.MipiCam_ControlLogic0_SubPlatformId;
  mPlatformNvsAreaProtocol.Area->MipiCamCtrlLogic0_Wled1Type = mSystemConfiguration.MipiCam_ControlLogic0_Wled1Type;
  mPlatformNvsAreaProtocol.Area->MipiCamCtrlLogic0_Wled2Type = mSystemConfiguration.MipiCam_ControlLogic0_Wled2Type;
  mPlatformNvsAreaProtocol.Area->MipiCamCtrlLogic0_PchClockSource = mSystemConfiguration.MipiCam_ControlLogic0_PchClockSource;

  for (Index = 0; Index < MIPICAM_GPIO_COUNT; Index++) {
    if (Index < mSystemConfiguration.MipiCam_ControlLogic0_GpioPinsEnabled) {
      mPlatformNvsAreaProtocol.Area->MipiCamCtrlLogic0_GpioGroupPadNumber[Index] = mSystemConfiguration.MipiCam_ControlLogic0_GpioGroupPadNumber[Index];
      mPlatformNvsAreaProtocol.Area->MipiCamCtrlLogic0_GpioGroupNumber[Index] = mSystemConfiguration.MipiCam_ControlLogic0_GpioGroupNumber[Index];
      mPlatformNvsAreaProtocol.Area->MipiCamCtrlLogic0_GpioFunction[Index] = mSystemConfiguration.MipiCam_ControlLogic0_GpioFunction[Index];
      mPlatformNvsAreaProtocol.Area->MipiCamCtrlLogic0_GpioActiveValue[Index] = mSystemConfiguration.MipiCam_ControlLogic0_GpioActiveValue[Index];
      mPlatformNvsAreaProtocol.Area->MipiCamCtrlLogic0_GpioInitialValue[Index] = mSystemConfiguration.MipiCam_ControlLogic0_GpioInitialValue[Index];
    } else {
      mPlatformNvsAreaProtocol.Area->MipiCamCtrlLogic0_GpioGroupPadNumber[Index] = 0;
      mPlatformNvsAreaProtocol.Area->MipiCamCtrlLogic0_GpioGroupNumber[Index] = 0;
      mPlatformNvsAreaProtocol.Area->MipiCamCtrlLogic0_GpioFunction[Index] = 0;
      mPlatformNvsAreaProtocol.Area->MipiCamCtrlLogic0_GpioActiveValue[Index] = 1;
      mPlatformNvsAreaProtocol.Area->MipiCamCtrlLogic0_GpioInitialValue[Index] = 0;
    }
  }

  // MipiCam Control Logic 1 specific
  mPlatformNvsAreaProtocol.Area->MipiCamCtrlLogic1_Version = 1;
  mPlatformNvsAreaProtocol.Area->MipiCamCtrlLogic1_Type = mSystemConfiguration.MipiCam_ControlLogic1_Type;
  mPlatformNvsAreaProtocol.Area->MipiCamCtrlLogic1_CrdVersion = mSystemConfiguration.MipiCam_ControlLogic1_CrdVersion;
  mPlatformNvsAreaProtocol.Area->MipiCamCtrlLogic1_InputClock = mSystemConfiguration.MipiCam_ControlLogic1_InputClock;
  mPlatformNvsAreaProtocol.Area->MipiCamCtrlLogic1_GpioPinsEnabled = mSystemConfiguration.MipiCam_ControlLogic1_GpioPinsEnabled;
  mPlatformNvsAreaProtocol.Area->MipiCamCtrlLogic1_I2cBus = mSystemConfiguration.MipiCam_ControlLogic1_I2cChannel;
  mPlatformNvsAreaProtocol.Area->MipiCamCtrlLogic1_I2cAddress = mSystemConfiguration.MipiCam_ControlLogic1_I2cAddress;
  mPlatformNvsAreaProtocol.Area->MipiCamCtrlLogic1_Pld = mSystemConfiguration.MipiCam_ControlLogic1_Pld;
  mPlatformNvsAreaProtocol.Area->MipiCamCtrlLogic1_Wled1FlashMaxCurrent = mSystemConfiguration.MipiCam_ControlLogic1_Wled1FlashMaxCurrent;
  mPlatformNvsAreaProtocol.Area->MipiCamCtrlLogic1_Wled1TorchMaxCurrent = mSystemConfiguration.MipiCam_ControlLogic1_Wled1TorchMaxCurrent;
  mPlatformNvsAreaProtocol.Area->MipiCamCtrlLogic1_Wled2FlashMaxCurrent = mSystemConfiguration.MipiCam_ControlLogic1_Wled2FlashMaxCurrent;
  mPlatformNvsAreaProtocol.Area->MipiCamCtrlLogic1_Wled2TorchMaxCurrent = mSystemConfiguration.MipiCam_ControlLogic1_Wled2TorchMaxCurrent;
  mPlatformNvsAreaProtocol.Area->MipiCamCtrlLogic1_SubPlatformId = mSystemConfiguration.MipiCam_ControlLogic1_SubPlatformId;
  mPlatformNvsAreaProtocol.Area->MipiCamCtrlLogic1_Wled1Type = mSystemConfiguration.MipiCam_ControlLogic1_Wled1Type;
  mPlatformNvsAreaProtocol.Area->MipiCamCtrlLogic1_Wled2Type = mSystemConfiguration.MipiCam_ControlLogic1_Wled2Type;
  mPlatformNvsAreaProtocol.Area->MipiCamCtrlLogic1_PchClockSource = mSystemConfiguration.MipiCam_ControlLogic1_PchClockSource;

  for (Index = 0; Index < MIPICAM_GPIO_COUNT; Index++) {
    if (Index < mSystemConfiguration.MipiCam_ControlLogic1_GpioPinsEnabled) {
      mPlatformNvsAreaProtocol.Area->MipiCamCtrlLogic1_GpioGroupPadNumber[Index] = mSystemConfiguration.MipiCam_ControlLogic1_GpioGroupPadNumber[Index];
      mPlatformNvsAreaProtocol.Area->MipiCamCtrlLogic1_GpioGroupNumber[Index] = mSystemConfiguration.MipiCam_ControlLogic1_GpioGroupNumber[Index];
      mPlatformNvsAreaProtocol.Area->MipiCamCtrlLogic1_GpioFunction[Index] = mSystemConfiguration.MipiCam_ControlLogic1_GpioFunction[Index];
      mPlatformNvsAreaProtocol.Area->MipiCamCtrlLogic1_GpioActiveValue[Index] = mSystemConfiguration.MipiCam_ControlLogic1_GpioActiveValue[Index];
      mPlatformNvsAreaProtocol.Area->MipiCamCtrlLogic1_GpioInitialValue[Index] = mSystemConfiguration.MipiCam_ControlLogic1_GpioInitialValue[Index];
    } else {
      mPlatformNvsAreaProtocol.Area->MipiCamCtrlLogic1_GpioGroupPadNumber[Index] = 0;
      mPlatformNvsAreaProtocol.Area->MipiCamCtrlLogic1_GpioGroupNumber[Index] = 0;
      mPlatformNvsAreaProtocol.Area->MipiCamCtrlLogic1_GpioFunction[Index] = 0;
      mPlatformNvsAreaProtocol.Area->MipiCamCtrlLogic1_GpioActiveValue[Index] = 1;
      mPlatformNvsAreaProtocol.Area->MipiCamCtrlLogic1_GpioInitialValue[Index] = 0;
    }
  }

  // MipiCam Control Logic 2 specific
  mPlatformNvsAreaProtocol.Area->MipiCamCtrlLogic2_Version = 1;
  mPlatformNvsAreaProtocol.Area->MipiCamCtrlLogic2_Type = mSystemConfiguration.MipiCam_ControlLogic2_Type;
  mPlatformNvsAreaProtocol.Area->MipiCamCtrlLogic2_CrdVersion = mSystemConfiguration.MipiCam_ControlLogic2_CrdVersion;
  mPlatformNvsAreaProtocol.Area->MipiCamCtrlLogic2_InputClock = mSystemConfiguration.MipiCam_ControlLogic2_InputClock;
  mPlatformNvsAreaProtocol.Area->MipiCamCtrlLogic2_GpioPinsEnabled = mSystemConfiguration.MipiCam_ControlLogic2_GpioPinsEnabled;
  mPlatformNvsAreaProtocol.Area->MipiCamCtrlLogic2_I2cBus = mSystemConfiguration.MipiCam_ControlLogic2_I2cChannel;
  mPlatformNvsAreaProtocol.Area->MipiCamCtrlLogic2_I2cAddress = mSystemConfiguration.MipiCam_ControlLogic2_I2cAddress;
  mPlatformNvsAreaProtocol.Area->MipiCamCtrlLogic2_Pld = mSystemConfiguration.MipiCam_ControlLogic2_Pld;
  mPlatformNvsAreaProtocol.Area->MipiCamCtrlLogic2_Wled1FlashMaxCurrent = mSystemConfiguration.MipiCam_ControlLogic2_Wled1FlashMaxCurrent;
  mPlatformNvsAreaProtocol.Area->MipiCamCtrlLogic2_Wled1TorchMaxCurrent = mSystemConfiguration.MipiCam_ControlLogic2_Wled1TorchMaxCurrent;
  mPlatformNvsAreaProtocol.Area->MipiCamCtrlLogic2_Wled2FlashMaxCurrent = mSystemConfiguration.MipiCam_ControlLogic2_Wled2FlashMaxCurrent;
  mPlatformNvsAreaProtocol.Area->MipiCamCtrlLogic2_Wled2TorchMaxCurrent = mSystemConfiguration.MipiCam_ControlLogic2_Wled2TorchMaxCurrent;
  mPlatformNvsAreaProtocol.Area->MipiCamCtrlLogic2_SubPlatformId = mSystemConfiguration.MipiCam_ControlLogic2_SubPlatformId;
  mPlatformNvsAreaProtocol.Area->MipiCamCtrlLogic2_Wled1Type = mSystemConfiguration.MipiCam_ControlLogic2_Wled1Type;
  mPlatformNvsAreaProtocol.Area->MipiCamCtrlLogic2_Wled2Type = mSystemConfiguration.MipiCam_ControlLogic2_Wled2Type;
  mPlatformNvsAreaProtocol.Area->MipiCamCtrlLogic2_PchClockSource = mSystemConfiguration.MipiCam_ControlLogic2_PchClockSource;

  for (Index = 0; Index < MIPICAM_GPIO_COUNT; Index++) {
    if (Index < mSystemConfiguration.MipiCam_ControlLogic2_GpioPinsEnabled) {
      mPlatformNvsAreaProtocol.Area->MipiCamCtrlLogic2_GpioGroupPadNumber[Index] = mSystemConfiguration.MipiCam_ControlLogic2_GpioGroupPadNumber[Index];
      mPlatformNvsAreaProtocol.Area->MipiCamCtrlLogic2_GpioGroupNumber[Index] = mSystemConfiguration.MipiCam_ControlLogic2_GpioGroupNumber[Index];
      mPlatformNvsAreaProtocol.Area->MipiCamCtrlLogic2_GpioFunction[Index] = mSystemConfiguration.MipiCam_ControlLogic2_GpioFunction[Index];
      mPlatformNvsAreaProtocol.Area->MipiCamCtrlLogic2_GpioActiveValue[Index] = mSystemConfiguration.MipiCam_ControlLogic2_GpioActiveValue[Index];
      mPlatformNvsAreaProtocol.Area->MipiCamCtrlLogic2_GpioInitialValue[Index] = mSystemConfiguration.MipiCam_ControlLogic2_GpioInitialValue[Index];
    } else {
      mPlatformNvsAreaProtocol.Area->MipiCamCtrlLogic2_GpioGroupPadNumber[Index] = 0;
      mPlatformNvsAreaProtocol.Area->MipiCamCtrlLogic2_GpioGroupNumber[Index] = 0;
      mPlatformNvsAreaProtocol.Area->MipiCamCtrlLogic2_GpioFunction[Index] = 0;
      mPlatformNvsAreaProtocol.Area->MipiCamCtrlLogic2_GpioActiveValue[Index] = 1;
      mPlatformNvsAreaProtocol.Area->MipiCamCtrlLogic2_GpioInitialValue[Index] = 0;
    }
  }

  // MipiCam Control Logic 3 specific
  mPlatformNvsAreaProtocol.Area->MipiCamCtrlLogic3_Version = 1;
  mPlatformNvsAreaProtocol.Area->MipiCamCtrlLogic3_Type = mSystemConfiguration.MipiCam_ControlLogic3_Type;
  mPlatformNvsAreaProtocol.Area->MipiCamCtrlLogic3_CrdVersion = mSystemConfiguration.MipiCam_ControlLogic3_CrdVersion;
  mPlatformNvsAreaProtocol.Area->MipiCamCtrlLogic3_InputClock = mSystemConfiguration.MipiCam_ControlLogic3_InputClock;
  mPlatformNvsAreaProtocol.Area->MipiCamCtrlLogic3_GpioPinsEnabled = mSystemConfiguration.MipiCam_ControlLogic3_GpioPinsEnabled;
  mPlatformNvsAreaProtocol.Area->MipiCamCtrlLogic3_I2cBus = mSystemConfiguration.MipiCam_ControlLogic3_I2cChannel;
  mPlatformNvsAreaProtocol.Area->MipiCamCtrlLogic3_I2cAddress = mSystemConfiguration.MipiCam_ControlLogic3_I2cAddress;
  mPlatformNvsAreaProtocol.Area->MipiCamCtrlLogic3_Pld = mSystemConfiguration.MipiCam_ControlLogic3_Pld;
  mPlatformNvsAreaProtocol.Area->MipiCamCtrlLogic3_Wled1FlashMaxCurrent = mSystemConfiguration.MipiCam_ControlLogic3_Wled1FlashMaxCurrent;
  mPlatformNvsAreaProtocol.Area->MipiCamCtrlLogic3_Wled1TorchMaxCurrent = mSystemConfiguration.MipiCam_ControlLogic3_Wled1TorchMaxCurrent;
  mPlatformNvsAreaProtocol.Area->MipiCamCtrlLogic3_Wled2FlashMaxCurrent = mSystemConfiguration.MipiCam_ControlLogic3_Wled2FlashMaxCurrent;
  mPlatformNvsAreaProtocol.Area->MipiCamCtrlLogic3_Wled2TorchMaxCurrent = mSystemConfiguration.MipiCam_ControlLogic3_Wled2TorchMaxCurrent;
  mPlatformNvsAreaProtocol.Area->MipiCamCtrlLogic3_SubPlatformId = mSystemConfiguration.MipiCam_ControlLogic3_SubPlatformId;
  mPlatformNvsAreaProtocol.Area->MipiCamCtrlLogic3_Wled1Type = mSystemConfiguration.MipiCam_ControlLogic3_Wled1Type;
  mPlatformNvsAreaProtocol.Area->MipiCamCtrlLogic3_Wled2Type = mSystemConfiguration.MipiCam_ControlLogic3_Wled2Type;
  mPlatformNvsAreaProtocol.Area->MipiCamCtrlLogic3_PchClockSource = mSystemConfiguration.MipiCam_ControlLogic3_PchClockSource;

  for (Index = 0; Index < MIPICAM_GPIO_COUNT; Index++) {
    if (Index < mSystemConfiguration.MipiCam_ControlLogic3_GpioPinsEnabled) {
      mPlatformNvsAreaProtocol.Area->MipiCamCtrlLogic3_GpioGroupPadNumber[Index] = mSystemConfiguration.MipiCam_ControlLogic3_GpioGroupPadNumber[Index];
      mPlatformNvsAreaProtocol.Area->MipiCamCtrlLogic3_GpioGroupNumber[Index] = mSystemConfiguration.MipiCam_ControlLogic3_GpioGroupNumber[Index];
      mPlatformNvsAreaProtocol.Area->MipiCamCtrlLogic3_GpioFunction[Index] = mSystemConfiguration.MipiCam_ControlLogic3_GpioFunction[Index];
      mPlatformNvsAreaProtocol.Area->MipiCamCtrlLogic3_GpioActiveValue[Index] = mSystemConfiguration.MipiCam_ControlLogic3_GpioActiveValue[Index];
      mPlatformNvsAreaProtocol.Area->MipiCamCtrlLogic3_GpioInitialValue[Index] = mSystemConfiguration.MipiCam_ControlLogic3_GpioInitialValue[Index];
    } else {
      mPlatformNvsAreaProtocol.Area->MipiCamCtrlLogic3_GpioGroupPadNumber[Index] = 0;
      mPlatformNvsAreaProtocol.Area->MipiCamCtrlLogic3_GpioGroupNumber[Index] = 0;
      mPlatformNvsAreaProtocol.Area->MipiCamCtrlLogic3_GpioFunction[Index] = 0;
      mPlatformNvsAreaProtocol.Area->MipiCamCtrlLogic3_GpioActiveValue[Index] = 1;
      mPlatformNvsAreaProtocol.Area->MipiCamCtrlLogic3_GpioInitialValue[Index] = 0;
    }
  }

  // MipiCam Control Logic 4 specific
  mPlatformNvsAreaProtocol.Area->MipiCamCtrlLogic4_Version = 1;
  mPlatformNvsAreaProtocol.Area->MipiCamCtrlLogic4_Type = mSystemConfiguration.MipiCam_ControlLogic4_Type;
  mPlatformNvsAreaProtocol.Area->MipiCamCtrlLogic4_CrdVersion = mSystemConfiguration.MipiCam_ControlLogic4_CrdVersion;
  mPlatformNvsAreaProtocol.Area->MipiCamCtrlLogic4_InputClock = mSystemConfiguration.MipiCam_ControlLogic4_InputClock;
  mPlatformNvsAreaProtocol.Area->MipiCamCtrlLogic4_GpioPinsEnabled = mSystemConfiguration.MipiCam_ControlLogic4_GpioPinsEnabled;
  mPlatformNvsAreaProtocol.Area->MipiCamCtrlLogic4_I2cBus = mSystemConfiguration.MipiCam_ControlLogic4_I2cChannel;
  mPlatformNvsAreaProtocol.Area->MipiCamCtrlLogic4_I2cAddress = mSystemConfiguration.MipiCam_ControlLogic4_I2cAddress;
  mPlatformNvsAreaProtocol.Area->MipiCamCtrlLogic4_Pld = mSystemConfiguration.MipiCam_ControlLogic4_Pld;
  mPlatformNvsAreaProtocol.Area->MipiCamCtrlLogic4_Wled1FlashMaxCurrent = mSystemConfiguration.MipiCam_ControlLogic4_Wled1FlashMaxCurrent;
  mPlatformNvsAreaProtocol.Area->MipiCamCtrlLogic4_Wled1TorchMaxCurrent = mSystemConfiguration.MipiCam_ControlLogic4_Wled1TorchMaxCurrent;
  mPlatformNvsAreaProtocol.Area->MipiCamCtrlLogic4_Wled2FlashMaxCurrent = mSystemConfiguration.MipiCam_ControlLogic4_Wled2FlashMaxCurrent;
  mPlatformNvsAreaProtocol.Area->MipiCamCtrlLogic4_Wled2TorchMaxCurrent = mSystemConfiguration.MipiCam_ControlLogic4_Wled2TorchMaxCurrent;
  mPlatformNvsAreaProtocol.Area->MipiCamCtrlLogic4_SubPlatformId = mSystemConfiguration.MipiCam_ControlLogic4_SubPlatformId;
  mPlatformNvsAreaProtocol.Area->MipiCamCtrlLogic4_Wled1Type = mSystemConfiguration.MipiCam_ControlLogic4_Wled1Type;
  mPlatformNvsAreaProtocol.Area->MipiCamCtrlLogic4_Wled2Type = mSystemConfiguration.MipiCam_ControlLogic4_Wled2Type;
  mPlatformNvsAreaProtocol.Area->MipiCamCtrlLogic4_PchClockSource = mSystemConfiguration.MipiCam_ControlLogic4_PchClockSource;

  for (Index = 0; Index < MIPICAM_GPIO_COUNT; Index++) {
    if (Index < mSystemConfiguration.MipiCam_ControlLogic4_GpioPinsEnabled) {
      mPlatformNvsAreaProtocol.Area->MipiCamCtrlLogic4_GpioGroupPadNumber[Index] = mSystemConfiguration.MipiCam_ControlLogic4_GpioGroupPadNumber[Index];
      mPlatformNvsAreaProtocol.Area->MipiCamCtrlLogic4_GpioGroupNumber[Index] = mSystemConfiguration.MipiCam_ControlLogic4_GpioGroupNumber[Index];
      mPlatformNvsAreaProtocol.Area->MipiCamCtrlLogic4_GpioFunction[Index] = mSystemConfiguration.MipiCam_ControlLogic4_GpioFunction[Index];
      mPlatformNvsAreaProtocol.Area->MipiCamCtrlLogic4_GpioActiveValue[Index] = mSystemConfiguration.MipiCam_ControlLogic4_GpioActiveValue[Index];
      mPlatformNvsAreaProtocol.Area->MipiCamCtrlLogic4_GpioInitialValue[Index] = mSystemConfiguration.MipiCam_ControlLogic4_GpioInitialValue[Index];
    } else {
      mPlatformNvsAreaProtocol.Area->MipiCamCtrlLogic4_GpioGroupPadNumber[Index] = 0;
      mPlatformNvsAreaProtocol.Area->MipiCamCtrlLogic4_GpioGroupNumber[Index] = 0;
      mPlatformNvsAreaProtocol.Area->MipiCamCtrlLogic4_GpioFunction[Index] = 0;
      mPlatformNvsAreaProtocol.Area->MipiCamCtrlLogic4_GpioActiveValue[Index] = 1;
      mPlatformNvsAreaProtocol.Area->MipiCamCtrlLogic4_GpioInitialValue[Index] = 0;
    }
  }

  // MipiCam Control Logic 5 specific
  mPlatformNvsAreaProtocol.Area->MipiCamCtrlLogic5_Version = 1;
  mPlatformNvsAreaProtocol.Area->MipiCamCtrlLogic5_Type = mSystemConfiguration.MipiCam_ControlLogic5_Type;
  mPlatformNvsAreaProtocol.Area->MipiCamCtrlLogic5_CrdVersion = mSystemConfiguration.MipiCam_ControlLogic5_CrdVersion;
  mPlatformNvsAreaProtocol.Area->MipiCamCtrlLogic5_InputClock = mSystemConfiguration.MipiCam_ControlLogic5_InputClock;
  mPlatformNvsAreaProtocol.Area->MipiCamCtrlLogic5_GpioPinsEnabled = mSystemConfiguration.MipiCam_ControlLogic5_GpioPinsEnabled;
  mPlatformNvsAreaProtocol.Area->MipiCamCtrlLogic5_I2cBus = mSystemConfiguration.MipiCam_ControlLogic5_I2cChannel;
  mPlatformNvsAreaProtocol.Area->MipiCamCtrlLogic5_I2cAddress = mSystemConfiguration.MipiCam_ControlLogic5_I2cAddress;
  mPlatformNvsAreaProtocol.Area->MipiCamCtrlLogic5_Pld = mSystemConfiguration.MipiCam_ControlLogic5_Pld;
  mPlatformNvsAreaProtocol.Area->MipiCamCtrlLogic5_Wled1FlashMaxCurrent = mSystemConfiguration.MipiCam_ControlLogic5_Wled1FlashMaxCurrent;
  mPlatformNvsAreaProtocol.Area->MipiCamCtrlLogic5_Wled1TorchMaxCurrent = mSystemConfiguration.MipiCam_ControlLogic5_Wled1TorchMaxCurrent;
  mPlatformNvsAreaProtocol.Area->MipiCamCtrlLogic5_Wled2FlashMaxCurrent = mSystemConfiguration.MipiCam_ControlLogic5_Wled2FlashMaxCurrent;
  mPlatformNvsAreaProtocol.Area->MipiCamCtrlLogic5_Wled2TorchMaxCurrent = mSystemConfiguration.MipiCam_ControlLogic5_Wled2TorchMaxCurrent;
  mPlatformNvsAreaProtocol.Area->MipiCamCtrlLogic5_SubPlatformId = mSystemConfiguration.MipiCam_ControlLogic5_SubPlatformId;
  mPlatformNvsAreaProtocol.Area->MipiCamCtrlLogic5_Wled1Type = mSystemConfiguration.MipiCam_ControlLogic5_Wled1Type;
  mPlatformNvsAreaProtocol.Area->MipiCamCtrlLogic5_Wled2Type = mSystemConfiguration.MipiCam_ControlLogic5_Wled2Type;
  mPlatformNvsAreaProtocol.Area->MipiCamCtrlLogic5_PchClockSource = mSystemConfiguration.MipiCam_ControlLogic5_PchClockSource;

  for (Index = 0; Index < MIPICAM_GPIO_COUNT; Index++) {
    if (Index < mSystemConfiguration.MipiCam_ControlLogic5_GpioPinsEnabled) {
      mPlatformNvsAreaProtocol.Area->MipiCamCtrlLogic5_GpioGroupPadNumber[Index] = mSystemConfiguration.MipiCam_ControlLogic5_GpioGroupPadNumber[Index];
      mPlatformNvsAreaProtocol.Area->MipiCamCtrlLogic5_GpioGroupNumber[Index] = mSystemConfiguration.MipiCam_ControlLogic5_GpioGroupNumber[Index];
      mPlatformNvsAreaProtocol.Area->MipiCamCtrlLogic5_GpioFunction[Index] = mSystemConfiguration.MipiCam_ControlLogic5_GpioFunction[Index];
      mPlatformNvsAreaProtocol.Area->MipiCamCtrlLogic5_GpioActiveValue[Index] = mSystemConfiguration.MipiCam_ControlLogic5_GpioActiveValue[Index];
      mPlatformNvsAreaProtocol.Area->MipiCamCtrlLogic5_GpioInitialValue[Index] = mSystemConfiguration.MipiCam_ControlLogic5_GpioInitialValue[Index];
    } else {
      mPlatformNvsAreaProtocol.Area->MipiCamCtrlLogic5_GpioGroupPadNumber[Index] = 0;
      mPlatformNvsAreaProtocol.Area->MipiCamCtrlLogic5_GpioGroupNumber[Index] = 0;
      mPlatformNvsAreaProtocol.Area->MipiCamCtrlLogic5_GpioFunction[Index] = 0;
      mPlatformNvsAreaProtocol.Area->MipiCamCtrlLogic5_GpioActiveValue[Index] = 1;
      mPlatformNvsAreaProtocol.Area->MipiCamCtrlLogic5_GpioInitialValue[Index] = 0;
    }
  }

  // MipiCam Link0 specific
  mPlatformNvsAreaProtocol.Area->MipiCamLink0SensorModel = mSystemConfiguration.MipiCam_Link0_SensorModel;
  mPlatformNvsAreaProtocol.Area->MipiCamLink0Pld = mSystemConfiguration.MipiCam_Link0_CameraPhysicalLocation;
  ZeroMem(ModuleName, sizeof(UINT16) * MIPICAM_MODULE_NAME_LENGTH);
  // Need to copy to temporary alligned buffer due to pack(1) in SetupVariables.h
  CopyMem(ModuleName,mSystemConfiguration.MipiCam_Link0_ModuleName, sizeof(UINT16) * MIPICAM_MODULE_NAME_LENGTH);
  UnicodeStrToAsciiStrS (ModuleName, (CHAR8 *) mPlatformNvsAreaProtocol.Area->MipiCamLink0ModuleName, sizeof(mPlatformNvsAreaProtocol.Area->MipiCamLink0ModuleName));
  ZeroMem(ModuleName, sizeof(UINT16) * MIPICAM_MODULE_NAME_LENGTH);
  CopyMem(ModuleName,mSystemConfiguration.MipiCam_Link0_UserHid, sizeof(UINT16) * MIPICAM_HID_LENGTH);
  UnicodeStrToAsciiStrS (ModuleName, (CHAR8 *) mPlatformNvsAreaProtocol.Area->MipiCamLink0UserHid, sizeof(mPlatformNvsAreaProtocol.Area->MipiCamLink0UserHid));
  mPlatformNvsAreaProtocol.Area->MipiCamLink0I2cDevicesEnabled = mSystemConfiguration.MipiCam_Link0_I2cDevicesEnabled;
  mPlatformNvsAreaProtocol.Area->MipiCamLink0I2cBus = mSystemConfiguration.MipiCam_Link0_I2cChannel;

  // MipiCam Link0 I2C Devices
  for (Index = 0; Index < MIPICAM_I2C_DEVICES_COUNT; Index++) {
    mPlatformNvsAreaProtocol.Area->MipiCamLink0I2cAddrDev[Index] = mSystemConfiguration.MipiCam_Link0_I2cAddress[Index];
    mPlatformNvsAreaProtocol.Area->MipiCamLink0I2cDeviceType[Index] = mSystemConfiguration.MipiCam_Link0_I2cDeviceType[Index];
  }

  mPlatformNvsAreaProtocol.Area->MipiCamLink0DD_Version = 1;
  mPlatformNvsAreaProtocol.Area->MipiCamLink0DD_CrdVersion = mSystemConfiguration.MipiCam_Link0_DriverData_CrdVersion;
  mPlatformNvsAreaProtocol.Area->MipiCamLink0DD_LanesClkDivision = mSystemConfiguration.MipiCam_Link0_LanesClkDiv;
  mPlatformNvsAreaProtocol.Area->MipiCamLink0DD_LinkUsed = mSystemConfiguration.MipiCam_Link0_DriverData_LinkUsed;
  mPlatformNvsAreaProtocol.Area->MipiCamLink0DD_LaneUsed = mSystemConfiguration.MipiCam_Link0_DriverData_LaneUsed;
  mPlatformNvsAreaProtocol.Area->MipiCamLink0DD_EepromType = mSystemConfiguration.MipiCam_Link0_DriverData_EepromType;
  mPlatformNvsAreaProtocol.Area->MipiCamLink0DD_VcmType = mSystemConfiguration.MipiCam_Link0_DriverData_VcmType;
  mPlatformNvsAreaProtocol.Area->MipiCamLink0DD_Mclk = mSystemConfiguration.MipiCam_Link0_DriverData_Mclk;
  mPlatformNvsAreaProtocol.Area->MipiCamLink0DD_ControlLogic = mSystemConfiguration.MipiCam_Link0_DriverData_ControlLogic;
  mPlatformNvsAreaProtocol.Area->MipiCamLink0DD_FlashSupport = mSystemConfiguration.MipiCam_Link0_DriverData_FlashSupport;
  mPlatformNvsAreaProtocol.Area->MipiCamLink0DD_PrivacyLed = mSystemConfiguration.MipiCam_Link0_DriverData_PrivacyLed;
  mPlatformNvsAreaProtocol.Area->MipiCamLink0DD_Degree = mSystemConfiguration.MipiCam_Link0_DriverData_Degree;
  mPlatformNvsAreaProtocol.Area->MipiCamLink0DD_PmicPosition = mSystemConfiguration.MipiCam_Link0_DriverData_PmicPosition;
  mPlatformNvsAreaProtocol.Area->MipiCamLink0DD_VoltageRail = mSystemConfiguration.MipiCam_Link0_DriverData_VoltageRail;
  mPlatformNvsAreaProtocol.Area->MipiCamLink0_FlashDriverSelection = mSystemConfiguration.MipiCam_Link0_FlashDriverSelection;

  // MipiCam Link1 specific
  mPlatformNvsAreaProtocol.Area->MipiCamLink1SensorModel = mSystemConfiguration.MipiCam_Link1_SensorModel;
  mPlatformNvsAreaProtocol.Area->MipiCamLink1Pld = mSystemConfiguration.MipiCam_Link1_CameraPhysicalLocation;
  ZeroMem(ModuleName, sizeof(UINT16) * MIPICAM_MODULE_NAME_LENGTH);
  // Need to copy to temporary alligned buffer due to pack(1) in SetupVariables.h
  CopyMem(ModuleName,mSystemConfiguration.MipiCam_Link1_ModuleName, sizeof(UINT16) * MIPICAM_MODULE_NAME_LENGTH);
  UnicodeStrToAsciiStrS (ModuleName, (CHAR8 *) mPlatformNvsAreaProtocol.Area->MipiCamLink1ModuleName, sizeof(mPlatformNvsAreaProtocol.Area->MipiCamLink1ModuleName));
  ZeroMem(ModuleName, sizeof(UINT16) * MIPICAM_MODULE_NAME_LENGTH);
  CopyMem(ModuleName,mSystemConfiguration.MipiCam_Link1_UserHid, sizeof(UINT16) * MIPICAM_HID_LENGTH);
  UnicodeStrToAsciiStrS (ModuleName, (CHAR8 *) mPlatformNvsAreaProtocol.Area->MipiCamLink1UserHid, sizeof(mPlatformNvsAreaProtocol.Area->MipiCamLink1UserHid));
  mPlatformNvsAreaProtocol.Area->MipiCamLink1I2cDevicesEnabled = mSystemConfiguration.MipiCam_Link1_I2cDevicesEnabled;
  mPlatformNvsAreaProtocol.Area->MipiCamLink1I2cBus = mSystemConfiguration.MipiCam_Link1_I2cChannel;

  // MipiCam Link1 I2C Devices
  for (Index = 0; Index < MIPICAM_I2C_DEVICES_COUNT; Index++) {
    mPlatformNvsAreaProtocol.Area->MipiCamLink1I2cAddrDev[Index] = mSystemConfiguration.MipiCam_Link1_I2cAddress[Index];
    mPlatformNvsAreaProtocol.Area->MipiCamLink1I2cDeviceType[Index] = mSystemConfiguration.MipiCam_Link1_I2cDeviceType[Index];
  }
  mPlatformNvsAreaProtocol.Area->MipiCamLink1DD_Version = 1;
  mPlatformNvsAreaProtocol.Area->MipiCamLink1DD_CrdVersion = mSystemConfiguration.MipiCam_Link1_DriverData_CrdVersion;
  mPlatformNvsAreaProtocol.Area->MipiCamLink1DD_LanesClkDivision = mSystemConfiguration.MipiCam_Link1_LanesClkDiv;
  mPlatformNvsAreaProtocol.Area->MipiCamLink1DD_LinkUsed = mSystemConfiguration.MipiCam_Link1_DriverData_LinkUsed;
  mPlatformNvsAreaProtocol.Area->MipiCamLink1DD_LaneUsed = mSystemConfiguration.MipiCam_Link1_DriverData_LaneUsed;
  mPlatformNvsAreaProtocol.Area->MipiCamLink1DD_EepromType = mSystemConfiguration.MipiCam_Link1_DriverData_EepromType;
  mPlatformNvsAreaProtocol.Area->MipiCamLink1DD_VcmType = mSystemConfiguration.MipiCam_Link1_DriverData_VcmType;
  mPlatformNvsAreaProtocol.Area->MipiCamLink1DD_Mclk = mSystemConfiguration.MipiCam_Link1_DriverData_Mclk;
  mPlatformNvsAreaProtocol.Area->MipiCamLink1DD_ControlLogic = mSystemConfiguration.MipiCam_Link1_DriverData_ControlLogic;
  mPlatformNvsAreaProtocol.Area->MipiCamLink1DD_FlashSupport = mSystemConfiguration.MipiCam_Link1_DriverData_FlashSupport;
  mPlatformNvsAreaProtocol.Area->MipiCamLink1DD_PrivacyLed = mSystemConfiguration.MipiCam_Link1_DriverData_PrivacyLed;
  mPlatformNvsAreaProtocol.Area->MipiCamLink1DD_Degree = mSystemConfiguration.MipiCam_Link1_DriverData_Degree;
  mPlatformNvsAreaProtocol.Area->MipiCamLink1DD_PmicPosition = mSystemConfiguration.MipiCam_Link1_DriverData_PmicPosition;
  mPlatformNvsAreaProtocol.Area->MipiCamLink1DD_VoltageRail = mSystemConfiguration.MipiCam_Link1_DriverData_VoltageRail;
  mPlatformNvsAreaProtocol.Area->MipiCamLink1_FlashDriverSelection = mSystemConfiguration.MipiCam_Link1_FlashDriverSelection;

  // MipiCam Link2 specific
  mPlatformNvsAreaProtocol.Area->MipiCamLink2SensorModel = mSystemConfiguration.MipiCam_Link2_SensorModel;
  mPlatformNvsAreaProtocol.Area->MipiCamLink2Pld = mSystemConfiguration.MipiCam_Link2_CameraPhysicalLocation;
  ZeroMem(ModuleName, sizeof(UINT16) * MIPICAM_MODULE_NAME_LENGTH);
  // Need to copy to temporary alligned buffer due to pack(1) in SetupVariables.h
  CopyMem(ModuleName,mSystemConfiguration.MipiCam_Link2_ModuleName, sizeof(UINT16) * MIPICAM_MODULE_NAME_LENGTH);
  UnicodeStrToAsciiStrS (ModuleName, (CHAR8 *) mPlatformNvsAreaProtocol.Area->MipiCamLink2ModuleName, sizeof(mPlatformNvsAreaProtocol.Area->MipiCamLink2ModuleName));
  ZeroMem(ModuleName, sizeof(UINT16) * MIPICAM_MODULE_NAME_LENGTH);
  CopyMem(ModuleName,mSystemConfiguration.MipiCam_Link2_UserHid, sizeof(UINT16) * MIPICAM_HID_LENGTH);
  UnicodeStrToAsciiStrS (ModuleName, (CHAR8 *) mPlatformNvsAreaProtocol.Area->MipiCamLink2UserHid, sizeof(mPlatformNvsAreaProtocol.Area->MipiCamLink2UserHid));
  mPlatformNvsAreaProtocol.Area->MipiCamLink2I2cDevicesEnabled = mSystemConfiguration.MipiCam_Link2_I2cDevicesEnabled;
  mPlatformNvsAreaProtocol.Area->MipiCamLink2I2cBus = mSystemConfiguration.MipiCam_Link2_I2cChannel;

  // MipiCam Link2 I2C Devices
  for (Index = 0; Index < MIPICAM_I2C_DEVICES_COUNT; Index++) {
    mPlatformNvsAreaProtocol.Area->MipiCamLink2I2cAddrDev[Index] = mSystemConfiguration.MipiCam_Link2_I2cAddress[Index];
    mPlatformNvsAreaProtocol.Area->MipiCamLink2I2cDeviceType[Index] = mSystemConfiguration.MipiCam_Link2_I2cDeviceType[Index];
  }
  mPlatformNvsAreaProtocol.Area->MipiCamLink2DD_Version = 1;
  mPlatformNvsAreaProtocol.Area->MipiCamLink2DD_CrdVersion = mSystemConfiguration.MipiCam_Link2_DriverData_CrdVersion;
  mPlatformNvsAreaProtocol.Area->MipiCamLink2DD_LanesClkDivision = mSystemConfiguration.MipiCam_Link2_LanesClkDiv;
  mPlatformNvsAreaProtocol.Area->MipiCamLink2DD_LinkUsed = mSystemConfiguration.MipiCam_Link2_DriverData_LinkUsed;
  mPlatformNvsAreaProtocol.Area->MipiCamLink2DD_LaneUsed = mSystemConfiguration.MipiCam_Link2_DriverData_LaneUsed;
  mPlatformNvsAreaProtocol.Area->MipiCamLink2DD_EepromType = mSystemConfiguration.MipiCam_Link2_DriverData_EepromType;
  mPlatformNvsAreaProtocol.Area->MipiCamLink2DD_VcmType = mSystemConfiguration.MipiCam_Link2_DriverData_VcmType;
  mPlatformNvsAreaProtocol.Area->MipiCamLink2DD_Mclk = mSystemConfiguration.MipiCam_Link2_DriverData_Mclk;
  mPlatformNvsAreaProtocol.Area->MipiCamLink2DD_ControlLogic = mSystemConfiguration.MipiCam_Link2_DriverData_ControlLogic;
  mPlatformNvsAreaProtocol.Area->MipiCamLink2DD_FlashSupport = mSystemConfiguration.MipiCam_Link2_DriverData_FlashSupport;
  mPlatformNvsAreaProtocol.Area->MipiCamLink2DD_PrivacyLed = mSystemConfiguration.MipiCam_Link2_DriverData_PrivacyLed;
  mPlatformNvsAreaProtocol.Area->MipiCamLink2DD_Degree = mSystemConfiguration.MipiCam_Link2_DriverData_Degree;
  mPlatformNvsAreaProtocol.Area->MipiCamLink2DD_PmicPosition = mSystemConfiguration.MipiCam_Link2_DriverData_PmicPosition;
  mPlatformNvsAreaProtocol.Area->MipiCamLink2DD_VoltageRail = mSystemConfiguration.MipiCam_Link2_DriverData_VoltageRail;
  mPlatformNvsAreaProtocol.Area->MipiCamLink2_FlashDriverSelection = mSystemConfiguration.MipiCam_Link2_FlashDriverSelection;

  // MipiCam Link3 specific
  mPlatformNvsAreaProtocol.Area->MipiCamLink3SensorModel = mSystemConfiguration.MipiCam_Link3_SensorModel;
  mPlatformNvsAreaProtocol.Area->MipiCamLink3Pld = mSystemConfiguration.MipiCam_Link3_CameraPhysicalLocation;
  ZeroMem(ModuleName, sizeof(UINT16) * MIPICAM_MODULE_NAME_LENGTH);
  // Need to copy to temporary alligned buffer due to pack(1) in SetupVariables.h
  CopyMem(ModuleName,mSystemConfiguration.MipiCam_Link3_ModuleName, sizeof(UINT16) * MIPICAM_MODULE_NAME_LENGTH);
  UnicodeStrToAsciiStrS (ModuleName, (CHAR8 *) mPlatformNvsAreaProtocol.Area->MipiCamLink3ModuleName, sizeof(mPlatformNvsAreaProtocol.Area->MipiCamLink3ModuleName));
  ZeroMem(ModuleName, sizeof(UINT16) * MIPICAM_MODULE_NAME_LENGTH);
  CopyMem(ModuleName,mSystemConfiguration.MipiCam_Link3_UserHid, sizeof(UINT16) * MIPICAM_HID_LENGTH);
  UnicodeStrToAsciiStrS (ModuleName, (CHAR8 *) mPlatformNvsAreaProtocol.Area->MipiCamLink3UserHid, sizeof(mPlatformNvsAreaProtocol.Area->MipiCamLink3UserHid));
  mPlatformNvsAreaProtocol.Area->MipiCamLink3I2cDevicesEnabled = mSystemConfiguration.MipiCam_Link3_I2cDevicesEnabled;
  mPlatformNvsAreaProtocol.Area->MipiCamLink3I2cBus = mSystemConfiguration.MipiCam_Link3_I2cChannel;

  // MipiCam Link3 I2C Devices
  for (Index = 0; Index < MIPICAM_I2C_DEVICES_COUNT; Index++) {
    mPlatformNvsAreaProtocol.Area->MipiCamLink3I2cAddrDev[Index] = mSystemConfiguration.MipiCam_Link3_I2cAddress[Index];
    mPlatformNvsAreaProtocol.Area->MipiCamLink3I2cDeviceType[Index] = mSystemConfiguration.MipiCam_Link3_I2cDeviceType[Index];
  }
  mPlatformNvsAreaProtocol.Area->MipiCamLink3DD_Version = 1;
  mPlatformNvsAreaProtocol.Area->MipiCamLink3DD_CrdVersion = mSystemConfiguration.MipiCam_Link3_DriverData_CrdVersion;
  mPlatformNvsAreaProtocol.Area->MipiCamLink3DD_LanesClkDivision = mSystemConfiguration.MipiCam_Link3_LanesClkDiv;
  mPlatformNvsAreaProtocol.Area->MipiCamLink3DD_LinkUsed = mSystemConfiguration.MipiCam_Link3_DriverData_LinkUsed;
  mPlatformNvsAreaProtocol.Area->MipiCamLink3DD_LaneUsed = mSystemConfiguration.MipiCam_Link3_DriverData_LaneUsed;
  mPlatformNvsAreaProtocol.Area->MipiCamLink3DD_EepromType = mSystemConfiguration.MipiCam_Link3_DriverData_EepromType;
  mPlatformNvsAreaProtocol.Area->MipiCamLink3DD_VcmType = mSystemConfiguration.MipiCam_Link3_DriverData_VcmType;
  mPlatformNvsAreaProtocol.Area->MipiCamLink3DD_Mclk = mSystemConfiguration.MipiCam_Link3_DriverData_Mclk;
  mPlatformNvsAreaProtocol.Area->MipiCamLink3DD_ControlLogic = mSystemConfiguration.MipiCam_Link3_DriverData_ControlLogic;
  mPlatformNvsAreaProtocol.Area->MipiCamLink3DD_FlashSupport = mSystemConfiguration.MipiCam_Link3_DriverData_FlashSupport;
  mPlatformNvsAreaProtocol.Area->MipiCamLink3DD_PrivacyLed = mSystemConfiguration.MipiCam_Link3_DriverData_PrivacyLed;
  mPlatformNvsAreaProtocol.Area->MipiCamLink3DD_Degree = mSystemConfiguration.MipiCam_Link3_DriverData_Degree;
  mPlatformNvsAreaProtocol.Area->MipiCamLink3DD_PmicPosition = mSystemConfiguration.MipiCam_Link3_DriverData_PmicPosition;
  mPlatformNvsAreaProtocol.Area->MipiCamLink3DD_VoltageRail = mSystemConfiguration.MipiCam_Link3_DriverData_VoltageRail;
  mPlatformNvsAreaProtocol.Area->MipiCamLink3_FlashDriverSelection = mSystemConfiguration.MipiCam_Link3_FlashDriverSelection;

  // MipiCam Link4 specific
  mPlatformNvsAreaProtocol.Area->MipiCamLink4SensorModel = mSystemConfiguration.MipiCam_Link4_SensorModel;
  mPlatformNvsAreaProtocol.Area->MipiCamLink4Pld = mSystemConfiguration.MipiCam_Link4_CameraPhysicalLocation;
  ZeroMem(ModuleName, sizeof(UINT16) * MIPICAM_MODULE_NAME_LENGTH);
  // Need to copy to temporary alligned buffer due to pack(1) in SetupVariables.h
  CopyMem(ModuleName,mSystemConfiguration.MipiCam_Link4_ModuleName, sizeof(UINT16) * MIPICAM_MODULE_NAME_LENGTH);
  UnicodeStrToAsciiStrS (ModuleName, (CHAR8 *) mPlatformNvsAreaProtocol.Area->MipiCamLink4ModuleName, sizeof(mPlatformNvsAreaProtocol.Area->MipiCamLink4ModuleName));
  ZeroMem(ModuleName, sizeof(UINT16) * MIPICAM_MODULE_NAME_LENGTH);
  CopyMem(ModuleName,mSystemConfiguration.MipiCam_Link4_UserHid, sizeof(UINT16) * MIPICAM_HID_LENGTH);
  UnicodeStrToAsciiStrS (ModuleName, (CHAR8 *) mPlatformNvsAreaProtocol.Area->MipiCamLink4UserHid, sizeof(mPlatformNvsAreaProtocol.Area->MipiCamLink4UserHid));
  mPlatformNvsAreaProtocol.Area->MipiCamLink4I2cDevicesEnabled = mSystemConfiguration.MipiCam_Link4_I2cDevicesEnabled;
  mPlatformNvsAreaProtocol.Area->MipiCamLink4I2cBus = mSystemConfiguration.MipiCam_Link4_I2cChannel;

  // MipiCam Link4 I2C Devices
  for (Index = 0; Index < MIPICAM_I2C_DEVICES_COUNT; Index++) {
    mPlatformNvsAreaProtocol.Area->MipiCamLink4I2cAddrDev[Index] = mSystemConfiguration.MipiCam_Link4_I2cAddress[Index];
    mPlatformNvsAreaProtocol.Area->MipiCamLink4I2cDeviceType[Index] = mSystemConfiguration.MipiCam_Link4_I2cDeviceType[Index];
  }
  mPlatformNvsAreaProtocol.Area->MipiCamLink4DD_Version = 1;
  mPlatformNvsAreaProtocol.Area->MipiCamLink4DD_CrdVersion = mSystemConfiguration.MipiCam_Link4_DriverData_CrdVersion;
  mPlatformNvsAreaProtocol.Area->MipiCamLink4DD_LanesClkDivision = mSystemConfiguration.MipiCam_Link4_LanesClkDiv;
//[-start-190620-IB11270237-modify]//
  mPlatformNvsAreaProtocol.Area->MipiCamLink4DD_LinkUsed = mSystemConfiguration.MipiCam_Link3_DriverData_LinkUsed;
//[-end-190620-IB11270237-modify]//
  mPlatformNvsAreaProtocol.Area->MipiCamLink4DD_LaneUsed = mSystemConfiguration.MipiCam_Link4_DriverData_LaneUsed;
  mPlatformNvsAreaProtocol.Area->MipiCamLink4DD_EepromType = mSystemConfiguration.MipiCam_Link4_DriverData_EepromType;
  mPlatformNvsAreaProtocol.Area->MipiCamLink4DD_VcmType = mSystemConfiguration.MipiCam_Link4_DriverData_VcmType;
  mPlatformNvsAreaProtocol.Area->MipiCamLink4DD_Mclk = mSystemConfiguration.MipiCam_Link4_DriverData_Mclk;
  mPlatformNvsAreaProtocol.Area->MipiCamLink4DD_ControlLogic = mSystemConfiguration.MipiCam_Link4_DriverData_ControlLogic;
  mPlatformNvsAreaProtocol.Area->MipiCamLink4DD_FlashSupport = mSystemConfiguration.MipiCam_Link4_DriverData_FlashSupport;
  mPlatformNvsAreaProtocol.Area->MipiCamLink4DD_PrivacyLed = mSystemConfiguration.MipiCam_Link4_DriverData_PrivacyLed;
  mPlatformNvsAreaProtocol.Area->MipiCamLink4DD_Degree = mSystemConfiguration.MipiCam_Link4_DriverData_Degree;
  mPlatformNvsAreaProtocol.Area->MipiCamLink4DD_PmicPosition = mSystemConfiguration.MipiCam_Link4_DriverData_PmicPosition;
  mPlatformNvsAreaProtocol.Area->MipiCamLink4DD_VoltageRail = mSystemConfiguration.MipiCam_Link4_DriverData_VoltageRail;
  mPlatformNvsAreaProtocol.Area->MipiCamLink4_FlashDriverSelection = mSystemConfiguration.MipiCam_Link4_FlashDriverSelection;

  // MipiCam Link5 specific
  mPlatformNvsAreaProtocol.Area->MipiCamLink5SensorModel = mSystemConfiguration.MipiCam_Link5_SensorModel;
  mPlatformNvsAreaProtocol.Area->MipiCamLink5Pld = mSystemConfiguration.MipiCam_Link5_CameraPhysicalLocation;
  ZeroMem(ModuleName, sizeof(UINT16) * MIPICAM_MODULE_NAME_LENGTH);
  // Need to copy to temporary alligned buffer due to pack(1) in SetupVariables.h
  CopyMem(ModuleName,mSystemConfiguration.MipiCam_Link5_ModuleName, sizeof(UINT16) * MIPICAM_MODULE_NAME_LENGTH);
  UnicodeStrToAsciiStrS (ModuleName, (CHAR8 *) mPlatformNvsAreaProtocol.Area->MipiCamLink5ModuleName, sizeof(mPlatformNvsAreaProtocol.Area->MipiCamLink5ModuleName));
  ZeroMem(ModuleName, sizeof(UINT16) * MIPICAM_MODULE_NAME_LENGTH);
  CopyMem(ModuleName,mSystemConfiguration.MipiCam_Link5_UserHid, sizeof(UINT16) * MIPICAM_HID_LENGTH);
  UnicodeStrToAsciiStrS (ModuleName, (CHAR8 *) mPlatformNvsAreaProtocol.Area->MipiCamLink5UserHid, sizeof(mPlatformNvsAreaProtocol.Area->MipiCamLink5UserHid));
  mPlatformNvsAreaProtocol.Area->MipiCamLink5I2cDevicesEnabled = mSystemConfiguration.MipiCam_Link5_I2cDevicesEnabled;
  mPlatformNvsAreaProtocol.Area->MipiCamLink5I2cBus = mSystemConfiguration.MipiCam_Link5_I2cChannel;

  // MipiCam Link5 I2C Devices
  for (Index = 0; Index < MIPICAM_I2C_DEVICES_COUNT; Index++) {
    mPlatformNvsAreaProtocol.Area->MipiCamLink5I2cAddrDev[Index] = mSystemConfiguration.MipiCam_Link5_I2cAddress[Index];
    mPlatformNvsAreaProtocol.Area->MipiCamLink5I2cDeviceType[Index] = mSystemConfiguration.MipiCam_Link5_I2cDeviceType[Index];
  }
  mPlatformNvsAreaProtocol.Area->MipiCamLink5DD_Version = 1;
  mPlatformNvsAreaProtocol.Area->MipiCamLink5DD_CrdVersion = mSystemConfiguration.MipiCam_Link5_DriverData_CrdVersion;
  mPlatformNvsAreaProtocol.Area->MipiCamLink5DD_LanesClkDivision = mSystemConfiguration.MipiCam_Link5_LanesClkDiv;
//[-start-190620-IB11270237-modify]//
  mPlatformNvsAreaProtocol.Area->MipiCamLink5DD_LinkUsed = mSystemConfiguration.MipiCam_Link3_DriverData_LinkUsed;
//[-end-190620-IB11270237-modify]//
  mPlatformNvsAreaProtocol.Area->MipiCamLink5DD_LaneUsed = mSystemConfiguration.MipiCam_Link5_DriverData_LaneUsed;
  mPlatformNvsAreaProtocol.Area->MipiCamLink5DD_EepromType = mSystemConfiguration.MipiCam_Link5_DriverData_EepromType;
  mPlatformNvsAreaProtocol.Area->MipiCamLink5DD_VcmType = mSystemConfiguration.MipiCam_Link5_DriverData_VcmType;
  mPlatformNvsAreaProtocol.Area->MipiCamLink5DD_Mclk = mSystemConfiguration.MipiCam_Link5_DriverData_Mclk;
  mPlatformNvsAreaProtocol.Area->MipiCamLink5DD_ControlLogic = mSystemConfiguration.MipiCam_Link5_DriverData_ControlLogic;
  mPlatformNvsAreaProtocol.Area->MipiCamLink5DD_FlashSupport = mSystemConfiguration.MipiCam_Link5_DriverData_FlashSupport;
  mPlatformNvsAreaProtocol.Area->MipiCamLink5DD_PrivacyLed = mSystemConfiguration.MipiCam_Link5_DriverData_PrivacyLed;
  mPlatformNvsAreaProtocol.Area->MipiCamLink5DD_Degree = mSystemConfiguration.MipiCam_Link5_DriverData_Degree;
  mPlatformNvsAreaProtocol.Area->MipiCamLink5DD_PmicPosition = mSystemConfiguration.MipiCam_Link5_DriverData_PmicPosition;
  mPlatformNvsAreaProtocol.Area->MipiCamLink5DD_VoltageRail = mSystemConfiguration.MipiCam_Link5_DriverData_VoltageRail;
  mPlatformNvsAreaProtocol.Area->MipiCamLink5_FlashDriverSelection = mSystemConfiguration.MipiCam_Link5_FlashDriverSelection;

  // MipiCam Flash0 specific
  mPlatformNvsAreaProtocol.Area->MipiCamFlash0Model              = mSystemConfiguration.MipiCam_Flash0_Model;
  mPlatformNvsAreaProtocol.Area->MipiCamFlash0Mode               = mSystemConfiguration.MipiCam_Flash0_Mode;
  ZeroMem(ModuleName, sizeof(UINT16)* MIPICAM_MODULE_NAME_LENGTH);
  // Need to copy to temporary aligned buffer due to pack(1) in SetupVariables.h
  CopyMem(ModuleName, mSystemConfiguration.MipiCam_Flash0_ModuleName, sizeof(UINT16)* MIPICAM_MODULE_NAME_LENGTH);
  UnicodeStrToAsciiStrS(ModuleName, (CHAR8 *)mPlatformNvsAreaProtocol.Area->MipiCamFlash0ModuleName, sizeof(mPlatformNvsAreaProtocol.Area->MipiCamFlash0ModuleName));
  mPlatformNvsAreaProtocol.Area->MipiCamFlash0I2cBus             = mSystemConfiguration.MipiCam_Flash0_I2cChannel;
  mPlatformNvsAreaProtocol.Area->MipiCamFlash0I2cAddrDev         = mSystemConfiguration.MipiCam_Flash0_I2cAddress;
  mPlatformNvsAreaProtocol.Area->MipiCamFlash0GpioGroupPadNumber = mSystemConfiguration.MipiCam_Flash0_GpioGroupPadNumber;
  mPlatformNvsAreaProtocol.Area->MipiCamFlash0GpioGroupNumber    = mSystemConfiguration.MipiCam_Flash0_GpioGroupNumber;
  mPlatformNvsAreaProtocol.Area->MipiCamFlash0GpioActiveValue    = mSystemConfiguration.MipiCam_Flash0_GpioActiveValue;
  mPlatformNvsAreaProtocol.Area->MipiCamFlash0GpioInitialValue   = mSystemConfiguration.MipiCam_Flash0_GpioInitialValue;
  mPlatformNvsAreaProtocol.Area->MipiCamFlash0OperatingMode      = mSystemConfiguration.MipiCam_Flash0_OperatingMode;

  // MipiCam Flash1 specific
  mPlatformNvsAreaProtocol.Area->MipiCamFlash1Model              = mSystemConfiguration.MipiCam_Flash1_Model;
  mPlatformNvsAreaProtocol.Area->MipiCamFlash1Mode               = mSystemConfiguration.MipiCam_Flash1_Mode;
  ZeroMem(ModuleName, sizeof(UINT16)* MIPICAM_MODULE_NAME_LENGTH);
  // Need to copy to temporary aligned buffer due to pack(1) in SetupVariables.h
  CopyMem(ModuleName, mSystemConfiguration.MipiCam_Flash1_ModuleName, sizeof(UINT16)* MIPICAM_MODULE_NAME_LENGTH);
  UnicodeStrToAsciiStrS(ModuleName, (CHAR8 *)mPlatformNvsAreaProtocol.Area->MipiCamFlash1ModuleName, sizeof(mPlatformNvsAreaProtocol.Area->MipiCamFlash1ModuleName));
  mPlatformNvsAreaProtocol.Area->MipiCamFlash1I2cBus             = mSystemConfiguration.MipiCam_Flash1_I2cChannel;
  mPlatformNvsAreaProtocol.Area->MipiCamFlash1I2cAddrDev         = mSystemConfiguration.MipiCam_Flash1_I2cAddress;
  mPlatformNvsAreaProtocol.Area->MipiCamFlash1GpioGroupPadNumber = mSystemConfiguration.MipiCam_Flash1_GpioGroupPadNumber;
  mPlatformNvsAreaProtocol.Area->MipiCamFlash1GpioGroupNumber    = mSystemConfiguration.MipiCam_Flash1_GpioGroupNumber;
  mPlatformNvsAreaProtocol.Area->MipiCamFlash1GpioActiveValue    = mSystemConfiguration.MipiCam_Flash1_GpioActiveValue;
  mPlatformNvsAreaProtocol.Area->MipiCamFlash1GpioInitialValue   = mSystemConfiguration.MipiCam_Flash1_GpioInitialValue;
  mPlatformNvsAreaProtocol.Area->MipiCamFlash1OperatingMode      = mSystemConfiguration.MipiCam_Flash1_OperatingMode;

  // MipiCam Flash2 specific
  mPlatformNvsAreaProtocol.Area->MipiCamFlash2Model              = mSystemConfiguration.MipiCam_Flash2_Model;
  mPlatformNvsAreaProtocol.Area->MipiCamFlash2Mode               = mSystemConfiguration.MipiCam_Flash2_Mode;
  ZeroMem(ModuleName, sizeof(UINT16)* MIPICAM_MODULE_NAME_LENGTH);
  // Need to copy to temporary aligned buffer due to pack(1) in SetupVariables.h
  CopyMem(ModuleName, mSystemConfiguration.MipiCam_Flash2_ModuleName, sizeof(UINT16)* MIPICAM_MODULE_NAME_LENGTH);
  UnicodeStrToAsciiStrS(ModuleName, (CHAR8 *)mPlatformNvsAreaProtocol.Area->MipiCamFlash2ModuleName, sizeof(mPlatformNvsAreaProtocol.Area->MipiCamFlash2ModuleName));
  mPlatformNvsAreaProtocol.Area->MipiCamFlash2I2cBus             = mSystemConfiguration.MipiCam_Flash2_I2cChannel;
  mPlatformNvsAreaProtocol.Area->MipiCamFlash2I2cAddrDev         = mSystemConfiguration.MipiCam_Flash2_I2cAddress;
  mPlatformNvsAreaProtocol.Area->MipiCamFlash2GpioGroupPadNumber = mSystemConfiguration.MipiCam_Flash2_GpioGroupPadNumber;
  mPlatformNvsAreaProtocol.Area->MipiCamFlash2GpioGroupNumber    = mSystemConfiguration.MipiCam_Flash2_GpioGroupNumber;
  mPlatformNvsAreaProtocol.Area->MipiCamFlash2GpioActiveValue    = mSystemConfiguration.MipiCam_Flash2_GpioActiveValue;
  mPlatformNvsAreaProtocol.Area->MipiCamFlash2GpioInitialValue   = mSystemConfiguration.MipiCam_Flash2_GpioInitialValue;
  mPlatformNvsAreaProtocol.Area->MipiCamFlash2OperatingMode      = mSystemConfiguration.MipiCam_Flash2_OperatingMode;

  // MipiCam Flash3 specific
  mPlatformNvsAreaProtocol.Area->MipiCamFlash3Model              = mSystemConfiguration.MipiCam_Flash3_Model;
  mPlatformNvsAreaProtocol.Area->MipiCamFlash3Mode               = mSystemConfiguration.MipiCam_Flash3_Mode;
  ZeroMem(ModuleName, sizeof(UINT16)* MIPICAM_MODULE_NAME_LENGTH);
  // Need to copy to temporary aligned buffer due to pack(1) in SetupVariables.h
  CopyMem(ModuleName, mSystemConfiguration.MipiCam_Flash3_ModuleName, sizeof(UINT16)* MIPICAM_MODULE_NAME_LENGTH);
  UnicodeStrToAsciiStrS(ModuleName, (CHAR8 *)mPlatformNvsAreaProtocol.Area->MipiCamFlash3ModuleName, sizeof(mPlatformNvsAreaProtocol.Area->MipiCamFlash3ModuleName));
  mPlatformNvsAreaProtocol.Area->MipiCamFlash3I2cBus             = mSystemConfiguration.MipiCam_Flash3_I2cChannel;
  mPlatformNvsAreaProtocol.Area->MipiCamFlash3I2cAddrDev         = mSystemConfiguration.MipiCam_Flash3_I2cAddress;
  mPlatformNvsAreaProtocol.Area->MipiCamFlash3GpioGroupPadNumber = mSystemConfiguration.MipiCam_Flash3_GpioGroupPadNumber;
  mPlatformNvsAreaProtocol.Area->MipiCamFlash3GpioGroupNumber    = mSystemConfiguration.MipiCam_Flash3_GpioGroupNumber;
  mPlatformNvsAreaProtocol.Area->MipiCamFlash3GpioActiveValue    = mSystemConfiguration.MipiCam_Flash3_GpioActiveValue;
  mPlatformNvsAreaProtocol.Area->MipiCamFlash3GpioInitialValue   = mSystemConfiguration.MipiCam_Flash3_GpioInitialValue;
  mPlatformNvsAreaProtocol.Area->MipiCamFlash3OperatingMode      = mSystemConfiguration.MipiCam_Flash3_OperatingMode;

  // MipiCam Flash4 specific
  mPlatformNvsAreaProtocol.Area->MipiCamFlash4Model              = mSystemConfiguration.MipiCam_Flash4_Model;
  mPlatformNvsAreaProtocol.Area->MipiCamFlash4Mode               = mSystemConfiguration.MipiCam_Flash4_Mode;
  ZeroMem(ModuleName, sizeof(UINT16)* MIPICAM_MODULE_NAME_LENGTH);
  // Need to copy to temporary aligned buffer due to pack(1) in SetupVariables.h
  CopyMem(ModuleName, mSystemConfiguration.MipiCam_Flash4_ModuleName, sizeof(UINT16)* MIPICAM_MODULE_NAME_LENGTH);
  UnicodeStrToAsciiStrS(ModuleName, (CHAR8 *)mPlatformNvsAreaProtocol.Area->MipiCamFlash4ModuleName, sizeof(mPlatformNvsAreaProtocol.Area->MipiCamFlash4ModuleName));
  mPlatformNvsAreaProtocol.Area->MipiCamFlash4I2cBus             = mSystemConfiguration.MipiCam_Flash4_I2cChannel;
  mPlatformNvsAreaProtocol.Area->MipiCamFlash4I2cAddrDev         = mSystemConfiguration.MipiCam_Flash4_I2cAddress;
  mPlatformNvsAreaProtocol.Area->MipiCamFlash4GpioGroupPadNumber = mSystemConfiguration.MipiCam_Flash4_GpioGroupPadNumber;
  mPlatformNvsAreaProtocol.Area->MipiCamFlash4GpioGroupNumber    = mSystemConfiguration.MipiCam_Flash4_GpioGroupNumber;
  mPlatformNvsAreaProtocol.Area->MipiCamFlash4GpioActiveValue    = mSystemConfiguration.MipiCam_Flash4_GpioActiveValue;
  mPlatformNvsAreaProtocol.Area->MipiCamFlash4GpioInitialValue   = mSystemConfiguration.MipiCam_Flash4_GpioInitialValue;
  mPlatformNvsAreaProtocol.Area->MipiCamFlash4OperatingMode      = mSystemConfiguration.MipiCam_Flash4_OperatingMode;

  // MipiCam Flash5 specific
  mPlatformNvsAreaProtocol.Area->MipiCamFlash5Model              = mSystemConfiguration.MipiCam_Flash5_Model;
  mPlatformNvsAreaProtocol.Area->MipiCamFlash5Mode               = mSystemConfiguration.MipiCam_Flash5_Mode;
  ZeroMem(ModuleName, sizeof(UINT16)* MIPICAM_MODULE_NAME_LENGTH);
  // Need to copy to temporary aligned buffer due to pack(1) in SetupVariables.h
  CopyMem(ModuleName, mSystemConfiguration.MipiCam_Flash5_ModuleName, sizeof(UINT16)* MIPICAM_MODULE_NAME_LENGTH);
  UnicodeStrToAsciiStrS(ModuleName, (CHAR8 *)mPlatformNvsAreaProtocol.Area->MipiCamFlash5ModuleName, sizeof(mPlatformNvsAreaProtocol.Area->MipiCamFlash5ModuleName));
  mPlatformNvsAreaProtocol.Area->MipiCamFlash5I2cBus             = mSystemConfiguration.MipiCam_Flash5_I2cChannel;
  mPlatformNvsAreaProtocol.Area->MipiCamFlash5I2cAddrDev         = mSystemConfiguration.MipiCam_Flash5_I2cAddress;
  mPlatformNvsAreaProtocol.Area->MipiCamFlash5GpioGroupPadNumber = mSystemConfiguration.MipiCam_Flash5_GpioGroupPadNumber;
  mPlatformNvsAreaProtocol.Area->MipiCamFlash5GpioGroupNumber    = mSystemConfiguration.MipiCam_Flash5_GpioGroupNumber;
  mPlatformNvsAreaProtocol.Area->MipiCamFlash5GpioActiveValue    = mSystemConfiguration.MipiCam_Flash5_GpioActiveValue;
  mPlatformNvsAreaProtocol.Area->MipiCamFlash5GpioInitialValue   = mSystemConfiguration.MipiCam_Flash5_GpioInitialValue;
  mPlatformNvsAreaProtocol.Area->MipiCamFlash5OperatingMode      = mSystemConfiguration.MipiCam_Flash5_OperatingMode;

  // Flash id Specific
  mPlatformNvsAreaProtocol.Area->MipiCamLink0DD_FlashID     = mSystemConfiguration.MipiCam_Link0_DriverData_FlashID;
  mPlatformNvsAreaProtocol.Area->MipiCamLink1DD_FlashID     = mSystemConfiguration.MipiCam_Link1_DriverData_FlashID;
  mPlatformNvsAreaProtocol.Area->MipiCamLink2DD_FlashID     = mSystemConfiguration.MipiCam_Link2_DriverData_FlashID;
  mPlatformNvsAreaProtocol.Area->MipiCamLink3DD_FlashID     = mSystemConfiguration.MipiCam_Link3_DriverData_FlashID;
  mPlatformNvsAreaProtocol.Area->MipiCamLink4DD_FlashID     = mSystemConfiguration.MipiCam_Link4_DriverData_FlashID;
  mPlatformNvsAreaProtocol.Area->MipiCamLink5DD_FlashID     = mSystemConfiguration.MipiCam_Link5_DriverData_FlashID;
  //
  // HDAudio Configuration
  //
  mPlatformNvsAreaProtocol.Area->I2SC = mPchSetup.PchHdAudioI2sCodecSelect;
  mPlatformNvsAreaProtocol.Area->I2SI = PcdGet32 (PcdHdaI2sCodecIrqGpio);
  mPlatformNvsAreaProtocol.Area->I2SB = mPchSetup.PchHdAudioI2sCodecBusNumber;

  DEBUG((DEBUG_INFO,"HDA: I2S Codec Select [I2SC] = 0x%02x\n", mPlatformNvsAreaProtocol.Area->I2SC));
  DEBUG((DEBUG_INFO,"HDA: I2S Codec JD Int pin [I2SI] = 0x%08x\n", mPlatformNvsAreaProtocol.Area->I2SI));
  DEBUG((DEBUG_INFO,"HDA: I2S Codec I2C Bus Number [I2SB] = 0x%02x\n", mPlatformNvsAreaProtocol.Area->I2SB));

  for (Index = 0; Index < HDAUDIO_PP_MODULES; Index++) {
    mPlatformNvsAreaProtocol.Area->HdaDspPpModuleMask |= (UINT32)(mPchSetup.PchHdAudioPostProcessingMod[Index] ? (1 << Index) : 0);
  }
  DEBUG((DEBUG_INFO,"HDA: HdaDspPpModuleMask [ADPM] = 0x%08x\n", mPlatformNvsAreaProtocol.Area->HdaDspPpModuleMask));

  if (mPchSetup.PchHdAudioPostProcessingMod[29]){
    DEBUG((DEBUG_INFO,"HDA: AudioDSP Pre/Post-Processing custom module 'Alpha' enabled (BIT29)\n"));

    CopyMem(LocalGuidString, mPchSetup.PchHdAudioPostProcessingModCustomGuid1, GUID_CHARS_NUMBER * sizeof(CHAR16));
    GuidStringToAcpiBuffer (LocalGuidString, &AcpiGuidPart1, &AcpiGuidPart2);
    mPlatformNvsAreaProtocol.Area->HdaDspPpModCustomGuid1Low  = AcpiGuidPart1;
    mPlatformNvsAreaProtocol.Area->HdaDspPpModCustomGuid1High = AcpiGuidPart2;
    DEBUG((DEBUG_INFO,"HdaDspPpModCustomGuid1Low  = 0x%016Lx\nHdaDspPpModCustomGuid2High = 0x%016Lx\n",
            mPlatformNvsAreaProtocol.Area->HdaDspPpModCustomGuid1Low, mPlatformNvsAreaProtocol.Area->HdaDspPpModCustomGuid1High));
  }

  if (mPchSetup.PchHdAudioPostProcessingMod[30]){
    DEBUG((DEBUG_INFO,"HDA: AudioDSP Pre/Post-Processing custom module 'Beta' enabled (BIT30)\n"));

    CopyMem(LocalGuidString, mPchSetup.PchHdAudioPostProcessingModCustomGuid2, GUID_CHARS_NUMBER * sizeof(CHAR16));
    GuidStringToAcpiBuffer (LocalGuidString, &AcpiGuidPart1, &AcpiGuidPart2);
    mPlatformNvsAreaProtocol.Area->HdaDspPpModCustomGuid2Low  = AcpiGuidPart1;
    mPlatformNvsAreaProtocol.Area->HdaDspPpModCustomGuid2High = AcpiGuidPart2;
    DEBUG((DEBUG_INFO,"HdaDspPpModCustomGuid2Low  = 0x%016Lx\nHdaDspPpModCustomGuid2High = 0x%016Lx\n",
            mPlatformNvsAreaProtocol.Area->HdaDspPpModCustomGuid2Low, mPlatformNvsAreaProtocol.Area->HdaDspPpModCustomGuid2High));
  }

  if (mPchSetup.PchHdAudioPostProcessingMod[31]){
    DEBUG((DEBUG_INFO,"HDA: AudioDSP Pre/Post-Processing custom module 'Gamma' enabled (BIT31)\n"));

    CopyMem(LocalGuidString, mPchSetup.PchHdAudioPostProcessingModCustomGuid3, GUID_CHARS_NUMBER * sizeof(CHAR16));
    GuidStringToAcpiBuffer (LocalGuidString, &AcpiGuidPart1, &AcpiGuidPart2);
    mPlatformNvsAreaProtocol.Area->HdaDspPpModCustomGuid3Low  = AcpiGuidPart1;
    mPlatformNvsAreaProtocol.Area->HdaDspPpModCustomGuid3High = AcpiGuidPart2;
    DEBUG((DEBUG_INFO,"HdaDspPpModCustomGuid3Low  = 0x%016Lx\nHdaDspPpModCustomGuid3High = 0x%016Lx\n",
            mPlatformNvsAreaProtocol.Area->HdaDspPpModCustomGuid3Low, mPlatformNvsAreaProtocol.Area->HdaDspPpModCustomGuid3High));
  }

  ///
  /// PMAX Configuration
  ///
  mPlatformNvsAreaProtocol.Area->PmaxDevice = mSystemConfiguration.PmaxDevice;
  mPlatformNvsAreaProtocol.Area->PmaxAudioCodec = mSystemConfiguration.PmaxAudioCodec;
  mPlatformNvsAreaProtocol.Area->PmaxWfCamera = mSystemConfiguration.PmaxWfCamera;
  mPlatformNvsAreaProtocol.Area->PmaxUfCamera = mSystemConfiguration.PmaxUfCamera;
  mPlatformNvsAreaProtocol.Area->PmaxFlashDevice = mSystemConfiguration.PmaxFlashDevice;

  ///
  /// Get TcssHob HOB
  ///
#if FixedPcdGetBool(PcdITbtEnable) == 1
  mTcssHob = (TCSS_DATA_HOB *) GetFirstGuidHob (&gTcssHobGuid);
#endif
  mPlatformNvsAreaProtocol.Area->PciDelayOptimizationEcr = mSystemConfiguration.PciDelayOptimizationEcr;

  if ((mSystemConfiguration.SdevXhciEntry == TRUE) && (mSystemConfiguration.SdevXhciNumberOfDevices > 0)) {
    mPlatformNvsAreaProtocol.Area->SdevXhciInterfaceNumber1 = mSystemConfiguration.SdevXhciInterfaceNumber[0];
    mPlatformNvsAreaProtocol.Area->SdevXhciRootPortNumber1  = mSystemConfiguration.SdevXhciRootPortNumber[0];
    if (mSystemConfiguration.SdevXhciNumberOfDevices > 1) {
      mPlatformNvsAreaProtocol.Area->SdevXhciInterfaceNumber2 = mSystemConfiguration.SdevXhciInterfaceNumber[1];
      mPlatformNvsAreaProtocol.Area->SdevXhciRootPortNumber2  = mSystemConfiguration.SdevXhciRootPortNumber[1];
    }
  }
  mPlatformNvsAreaProtocol.Area->SdevXhciEntry = mSystemConfiguration.SdevXhciEntry;

  mPlatformNvsAreaProtocol.Area->DeepestUSBSleepWakeCapability = mSystemConfiguration.DeepestUSBSleepWakeCapability;
  mPlatformNvsAreaProtocol.Area->EnablePcieTunnelingOverUsb4 = mSystemConfiguration.EnablePcieTunnelingOverUsb4;

  PreBootCmMode      = 0;
  CmTbtMask          = 0;
  Usb4CmSwitchEnable = 0;

  //
  // Find USB4 Hob to get platform USB4 host router information
  //
  HobPtr = NULL;
  HobPtr = GetFirstGuidHob (&gUsb4PlatformHobGuid);
  if (HobPtr == NULL) {
    DEBUG ((DEBUG_ERROR, "Unable to find USB4 platform hob!\n"));
  } else {
    Usb4PlatformHob = GET_GUID_HOB_DATA (HobPtr);
    Usb4PlatformInfo = &(Usb4PlatformHob->Usb4PlatformInfo);
    //
    // Bit 7   - Valid bit
    // Bit 6:4 - Setup bit
    // Bit 3   - Reserved
    // Bit 2:0 - Pre-boot CM mode
    //
    PreBootCmMode = (Usb4PlatformInfo->CmModeOption & 0x07) << 4;
    PreBootCmMode |= (Usb4PlatformInfo->CmMode & 0x07);
    PreBootCmMode |= 0x80;

    CmTbtMask          = Usb4PlatformInfo->Usb4HrMask;
    Usb4CmSwitchEnable = Usb4PlatformInfo->CmSwitchSupport;
  }

  mPlatformNvsAreaProtocol.Area->PreBootCmMode      = PreBootCmMode;
  mPlatformNvsAreaProtocol.Area->CmTbtMask          = CmTbtMask;
  mPlatformNvsAreaProtocol.Area->Usb4CmSwitchEnable = Usb4CmSwitchEnable;
  DEBUG ((DEBUG_INFO, "PreBootCmMode = 0x%x, CmTbtMask = 0x%x, Usb4CmSwitchEnable = 0x%x\n",
          mPlatformNvsAreaProtocol.Area->PreBootCmMode,
          mPlatformNvsAreaProtocol.Area->CmTbtMask,
          mPlatformNvsAreaProtocol.Area->Usb4CmSwitchEnable
          ));

//[-start-190613-IB11270237-remove]//
//  Status = gBS->InstallMultipleProtocolInterfaces (
//                  &Handle,
//                  &gPlatformNvsAreaProtocolGuid,
//                  &mPlatformNvsAreaProtocol,
//                  NULL
//                  );
//  ASSERT_EFI_ERROR (Status);
//[-end-190613-IB11270237-remove]//

  //
  // Allocate NVS area for VBIOS. This is not currently used.
  //
  Status = AllocateMemoryBelow4G (
             EfiACPIMemoryNVS,
             0x10000,
             &mGfxNvsAddress
             );
  ASSERT_EFI_ERROR (Status);

  //
  // Generate ACPI tables from configuration data
  //
  Status = GenerateSsdtFromConfigFile();
  ASSERT_EFI_ERROR (Status);

  //
  // Platform ACPI Tables
  //
//[-start-190620-IB11270237-remove]//
//  PublishAcpiTablesFromFv(gAcpiTableStorageGuid);
//[-end-190620-IB11270237-remove]//
  //
  // Reference Code ACPI Tables
  //
//[-start-190620-IB11270237-remove]//
//  PublishAcpiTablesFromFv(gRcAcpiTableStorageGuid);
//[-end-190620-IB11270237-remove]//

//[-start-190620-IB11270237-add]//
  DEBUG_OEM_SVC ((DEBUG_INFO, "Dxe OemChipsetServices Call: OemSvcUpdatePlatformNvs \n"));
  Status = OemSvcUpdatePlatformNvs (mPlatformNvsAreaProtocol.Area, mPlatformNvsAreaProtocol.OemArea);
  DEBUG_OEM_SVC ((DEBUG_INFO, "Dxe OemChipsetServices OemSvcUpdatePlatformNvs Status: %r\n", Status));
//[-end-190620-IB11270237-add]//
//[-start-190620-IB11270237-remove]//
//  //
//  // Save Acpi Setup Variables
//  //
//  Status = SetAcpiSetupVariables ();
//  ASSERT_EFI_ERROR (Status);
//
//[-end-190620-IB11270237-remove]//
  //
  // Finished
  //
  return EFI_SUCCESS;
}

/**
  Sort ordering of CPUs according to the priorities of cores and threads.
  ** ASSUMPTION: 1) single CPU package systsem wide; 2) two threads per core

  @param[in] MpService             The MP servicde protocol (used to retrievel MP info)

  @retval EFI_SUCCESS              The driver installed without error.

**/
EFI_STATUS
SortCpuLocalApicInTable (
  IN  EFI_MP_SERVICES_PROTOCOL          *MpService
  )
{
  EFI_PROCESSOR_INFORMATION                 *MpContext;
  UINT8                                     Index;
  UINTN                                     BspIndex;
  UINT8                                     CurrProcessor;
  EFI_STATUS                                Status;
  UINT8                                     MaxCpuCore;
  UINT8                                     MaxCpuThread;

  MpContext                     = NULL;
  MaxCpuCore                    = 0;
  MaxCpuThread                  = 0;

  Status = MpService->WhoAmI (
                        MpService,
                        &BspIndex
                        );

  //
  // Fill mCpuApicIdOrderTable
  //
  for (CurrProcessor = 0; CurrProcessor < mNumberOfCpus; CurrProcessor++) {

    MpContext = AllocatePool (sizeof(EFI_PROCESSOR_INFORMATION));
    ASSERT (MpContext != NULL);
    Status = MpService->GetProcessorInfo (
                          MpService,
                          CurrProcessor,
                          MpContext
                          );
    ASSERT_EFI_ERROR (Status);

    if (MpContext == NULL) {
      return Status;
    }
    mCpuApicIdOrderTable[CurrProcessor].ApicId  = (UINT32) MpContext->ProcessorId;
    DEBUG((DEBUG_INFO, "The CurrProcessor 0x%x ApicId is 0x%x\n", CurrProcessor, mCpuApicIdOrderTable[CurrProcessor].ApicId));
    mCpuApicIdOrderTable[CurrProcessor].Flags   = (MpContext->StatusFlag & PROCESSOR_ENABLED_BIT)? 1: 0;
    mCpuApicIdOrderTable[CurrProcessor].Package = (UINT8) MpContext->Location.Package;
    mCpuApicIdOrderTable[CurrProcessor].Die     = (UINT8) 0;
    mCpuApicIdOrderTable[CurrProcessor].Core    = (UINT8) MpContext->Location.Core;
    mCpuApicIdOrderTable[CurrProcessor].Thread  = (UINT8) MpContext->Location.Thread;

    if (MaxCpuThread < (UINT8) MpContext->Location.Thread) {
      MaxCpuThread = (UINT8) MpContext->Location.Thread;
    }

    if (MaxCpuCore < (UINT8) MpContext->Location.Core) {
      MaxCpuCore = (UINT8) MpContext->Location.Core;
    }
  }

  //
  // Do some statistics about the SBSP package
  //
  for (CurrProcessor = 0; CurrProcessor < mNumberOfCpus; CurrProcessor++) {
    if (CurrProcessor == BspIndex) {
      mSbspPackageInfo.BspApicId = mCpuApicIdOrderTable[CurrProcessor].ApicId;
      mSbspPackageInfo.PackageNo   = mCpuApicIdOrderTable[CurrProcessor].Package;
      mSbspPackageInfo.TotalThreads = 0;
      mSbspPackageInfo.CoreNo = 0;
      mSbspPackageInfo.LogicalThreadNo = 0;
    }
  }

  for (CurrProcessor = 0; CurrProcessor < mNumberOfCpus; CurrProcessor++) {
    if (mCpuApicIdOrderTable[CurrProcessor].Package == mSbspPackageInfo.PackageNo) {
      mSbspPackageInfo.TotalThreads++;
      if (mCpuApicIdOrderTable[CurrProcessor].Thread == 0) {
        mSbspPackageInfo.CoreNo++;
      } else {
        mSbspPackageInfo.LogicalThreadNo++;
      }
    }
  }

  //
  // Output debug info
  //
  for (Index = 0; Index < mNumberOfCpus; Index++) {
    DEBUG((DEBUG_INFO, "Package = %x,  Die = %x,  Core = %x,  Thread = %x,  ApicId = %x\n", \
      mCpuApicIdOrderTable[Index].Package, \
      mCpuApicIdOrderTable[Index].Die, \
      mCpuApicIdOrderTable[Index].Core, \
      mCpuApicIdOrderTable[Index].Thread, \
      mCpuApicIdOrderTable[Index].ApicId));
  }

  DEBUG((DEBUG_INFO, "MaxCpuCore    = %x\n", MaxCpuCore));
  DEBUG((DEBUG_INFO, "MaxCpuThread    = %x\n\n", MaxCpuThread));
  DEBUG((DEBUG_INFO, "mSbspPackageInfo.BspApicId        = %x\n", mSbspPackageInfo.BspApicId));
  DEBUG((DEBUG_INFO, "mSbspPackageInfo.TotalThreads     = %x\n", mSbspPackageInfo.TotalThreads));
  DEBUG((DEBUG_INFO, "mSbspPackageInfo.PackageNo        = %x\n", mSbspPackageInfo.PackageNo));
  DEBUG((DEBUG_INFO, "mSbspPackageInfo.CoreNo           = %x\n", mSbspPackageInfo.CoreNo));
  DEBUG((DEBUG_INFO, "mSbspPackageInfo.LogicalThreadNo  = %x\n", mSbspPackageInfo.LogicalThreadNo));

  //
  // First entry is always SBSP
  //
  CurrProcessor = 0;
  mCpuApicIdReorderTable[CurrProcessor].ApicId = mSbspPackageInfo.BspApicId;
  mCpuApicIdReorderTable[CurrProcessor].Package = mSbspPackageInfo.PackageNo;
  mCpuApicIdReorderTable[CurrProcessor].Flags = 1;
  CurrProcessor++;

  //
  // Output debug info
  //
  for (Index = 0; Index < mNumberOfCpus; Index++) {
    DEBUG((DEBUG_INFO, "Index = %x,  ApicId = %x,  Package = %x\n", \
      Index, \
      mCpuApicIdOrderTable[Index].ApicId, \
      mCpuApicIdOrderTable[Index].Package));
  }

  FreePool (MpContext);

  return  EFI_SUCCESS;
}


EFI_STATUS
AppendCpuMapTableEntry (
  IN  EFI_ACPI_1_0_PROCESSOR_LOCAL_APIC_STRUCTURE   *AcpiLocalApic,
  IN UINT32                                          LocalApicCounter
  )
{
  EFI_STATUS    Status = EFI_SUCCESS;
  EFI_ACPI_6_3_PROCESSOR_LOCAL_X2APIC_STRUCTURE *LocalX2ApicPtr;

  if (AcpiLocalApic->Type == EFI_ACPI_1_0_PROCESSOR_LOCAL_APIC) {
    if (LocalApicCounter < mNumberOfCpus) {
      AcpiLocalApic->Flags = (UINT8)mCpuApicIdOrderTable[LocalApicCounter].Flags;
      AcpiLocalApic->ApicId = (UINT8)mCpuApicIdOrderTable[LocalApicCounter].ApicId;
      //
      // Make AcpiProcessorIds 0-based
      //
      AcpiLocalApic->AcpiProcessorId = (UINT8)LocalApicCounter;
    } else {
      AcpiLocalApic->Flags = 0;
      AcpiLocalApic->ApicId = 0xFF;
      //
      // Make AcpiProcessorIds 0-based
      //
      AcpiLocalApic->AcpiProcessorId = (UINT8)LocalApicCounter;
      Status = EFI_UNSUPPORTED;
    }

    //
    // Display the results
    //
    DEBUG((DEBUG_INFO, "AcpiLocalApic: AcpiProcessorId=%x, ApicId=%x, Flags=%x\n", \
    AcpiLocalApic->AcpiProcessorId, \
    AcpiLocalApic->ApicId, \
    AcpiLocalApic->Flags));
  } else if (AcpiLocalApic->Type == EFI_ACPI_6_3_PROCESSOR_LOCAL_X2APIC) {
    LocalX2ApicPtr = (EFI_ACPI_6_3_PROCESSOR_LOCAL_X2APIC_STRUCTURE *)AcpiLocalApic;
    if (LocalApicCounter < mNumberOfCpus) {
      LocalX2ApicPtr->Flags = (UINT8)mCpuApicIdOrderTable[LocalApicCounter].Flags;
      LocalX2ApicPtr->X2ApicId = mCpuApicIdOrderTable[LocalApicCounter].ApicId;
      LocalX2ApicPtr->AcpiProcessorUid = LocalApicCounter;
    } else {
      LocalX2ApicPtr->Flags = 0;
      LocalX2ApicPtr->X2ApicId = (UINT32)-1;
      LocalX2ApicPtr->AcpiProcessorUid = LocalApicCounter;
      Status = EFI_UNSUPPORTED;
    }
    //
    // Display the results
    //
    DEBUG((DEBUG_INFO, "AcpiLocalx2Apic: AcpiProcessorId=%x, ApicId=%x, Flags=%x\n", \
    LocalX2ApicPtr->AcpiProcessorUid, \
    LocalX2ApicPtr->X2ApicId, \
    LocalX2ApicPtr->Flags));
  } else {
    Status = EFI_UNSUPPORTED;
  }
  return Status;
}

/**
  I/O work flow to wait input buffer empty in given time.

  @param[in] Timeout       Wating time.

  @retval EFI_TIMEOUT      if input is still not empty in given time.
  @retval EFI_SUCCESS      input is empty.
**/
EFI_STATUS
WaitInputEmpty (
  IN UINTN Timeout
  )
{
  UINTN Delay;
  UINT8 Data;

  Delay = Timeout / 50;

  do {
    Data = IoRead8 (KBC_CMD_STS_PORT);

    //
    // Check keyboard controller status bit 0 and 1 (Output Buffer and Input Buffer status)
    //
    if ((Data & KBC_OUTB) != 0) {
      IoRead8 (KBC_DATA_PORT);
    } else if ((Data & KBC_INPB) == 0) {
      break;
    }

    gBS->Stall (50);
    Delay--;
  } while (Delay != 0);

  if (Delay == 0) {
    return EFI_TIMEOUT;
  }

  return EFI_SUCCESS;
}
//[-start-210511-IB05660163-add]//
/**
  I/O work flow to wait output buffer empty in given time.

  @param Timeout given time

  @retval EFI_TIMEOUT  output is not empty in given time
  @retval EFI_SUCCESS  output is empty in given time.
**/
EFI_STATUS
WaitOutputEmpty (
  IN UINTN                                Timeout
  )
{
  UINTN Delay;
  UINT8 Data;

  Delay = Timeout / 50;

  do {
    Data = IoRead8 (KBC_CMD_STS_PORT);

    //
    // Check keyboard controller status bit 0(output buffer status)
    //
    if ((Data & KBC_OUTB) == 0) {
      break;
    }

    Data = IoRead8 (KBC_DATA_PORT);

    gBS->Stall (50);
    Delay--;
  } while (Delay);

  if (Delay == 0) {
    return EFI_TIMEOUT;
  }

  return EFI_SUCCESS;
}
//[-end-210511-IB05660163--add]//
/**
  I/O work flow to wait output buffer full in given time.

  @param[in] Timeout       given time

  @retval EFI_TIMEOUT      output is not full in given time
  @retval EFI_SUCCESS      output is full in given time.
**/
EFI_STATUS
WaitOutputFull (
  IN UINTN Timeout
  )
{
  UINTN Delay;
  UINT8 Data;

  Delay = Timeout / 50;

  do {
    Data = IoRead8 (KBC_CMD_STS_PORT);

    //
    // Check keyboard controller status bit 0(output buffer status)
    //  & bit5(output buffer for auxiliary device)
    //
    if ((Data & (KBC_OUTB | KBC_AUXB)) == (KBC_OUTB | KBC_AUXB)) {
      break;
    }

    gBS->Stall (50);
    Delay--;
  } while (Delay != 0);

  if (Delay == 0) {
    return EFI_TIMEOUT;
  }

  return EFI_SUCCESS;
}
//[-start-210511-IB05660163-add]//
//
// 8042 I/O function
//
/**
  I/O work flow of outing 8042 command.

  @param Command I/O command.

  @retval EFI_SUCCESS Success to execute I/O work flow
  @retval EFI_TIMEOUT Keyboard controller time out.
**/
EFI_STATUS
Out8042Command (
  IN UINT8                                Command
  )
{
  EFI_STATUS  Status;

  //
  // Wait keyboard controller input buffer empty
  //
  Status = WaitInputEmpty (TIMEOUT);
  if (EFI_ERROR (Status)) {
    return Status;
  }
  //
  // Send command
  //
  IoWrite8 (KBC_CMD_STS_PORT, Command);

  Status = WaitInputEmpty (TIMEOUT);
  if (EFI_ERROR (Status)) {
    return Status;
  }

  return EFI_SUCCESS;
}
//[-end-210511-IB05660163-add]//
/**
  I/O work flow of in 8042 Aux data.

  @param Data    Buffer holding return value.

  @retval EFI_SUCCESS Success to excute I/O work flow
  @retval EFI_TIMEOUT Keyboard controller time out.
**/
EFI_STATUS
In8042AuxData (
  IN OUT UINT8 *Data
  )
{
  EFI_STATUS Status;

  //
  // wait for output data
  //
  Status = WaitOutputFull (BAT_TIMEOUT);
  if (EFI_ERROR (Status)) {
    return Status;
  }

  *Data = IoRead8 (KBC_DATA_PORT);

  return EFI_SUCCESS;
}

/**
  I/O work flow of outing 8042 Aux command.

  @param Command Aux I/O command
//[-start-210511-IB05660163-add]//
  @param Resend  Whether need resend the Aux command.
//[-end-210511-IB05660163-add]//

  @retval EFI_SUCCESS Success to excute I/O work flow
  @retval EFI_TIMEOUT Keyboard controller time out.
**/
EFI_STATUS
Out8042AuxCommand (
  IN UINT8     Command,
//[-start-210511-IB05660163-add]//
  IN BOOLEAN   Resend
//[-end-210511-IB05660163-add]//
  )
{
  EFI_STATUS  Status;
  UINT8       Data;

  //
  // Wait keyboard controller input buffer empty
  //
  Status = WaitInputEmpty (TIMEOUT);
  if (EFI_ERROR (Status)) {
    return Status;
  }
  //
  // Send write to auxiliary device command
  //
  Data = WRITE_AUX_DEV;
  IoWrite8 (KBC_CMD_STS_PORT, Data);

  Status = WaitInputEmpty (TIMEOUT);
  if (EFI_ERROR (Status)) {
    return Status;
  }
  //
  // Send auxiliary device command
  //
  IoWrite8 (KBC_DATA_PORT, Command);

  //
  // Read return code
  //
  Status = In8042AuxData (&Data);
  if (EFI_ERROR (Status)) {
    return Status;
  }

  if (Data == PS2_ACK) {
    //
    // Receive mouse acknowledge, command send success
    //
    return EFI_SUCCESS;

//[-start-210511-IB05660163-add]//
  } else if (Resend) {
    //
    // Resend fail
    //
    return EFI_DEVICE_ERROR;
//[-end-210511-IB05660163-add]//

  } else if (Data == PS2_RESEND) {
    //
    // Resend command
    //

//[-start-210511-IB05660163-modify]//
    Status = Out8042AuxCommand (Command, TRUE);
//[-end-210511-IB05660163-modify]//

    if (EFI_ERROR (Status)) {
      return Status;
    }

  } else {
    //
    // Invalid return code
    //
    return EFI_DEVICE_ERROR;

  }

  return EFI_SUCCESS;
}

/**
  Issue command to enable Ps2 mouse.

  @return Status of command issuing.
**/
EFI_STATUS
PS2MouseEnable (
  VOID
  )
{
  //
  // Send auxiliary command to enable mouse
  //
//[-start-210511-IB05660163-modify]//
//  return Out8042AuxCommand (ENABLE_CMD);

  EFI_STATUS    Status;

  //
  // Disable AUX and wait output buffer empty
  //
  Out8042Command (DISABLE_AUX);
  Status = WaitOutputEmpty (CLEAR_BUFF_TIMEOUT);
  if (EFI_ERROR (Status)) {
    return EFI_UNSUPPORTED;
  }
  //
  // Enable AUX
  //
  Out8042Command (ENABLE_AUX);
  //
  // Send auxiliary command to enable mouse
  //
  Status = Out8042AuxCommand (ENABLE_CMD, FALSE);
  //
  // Check done and disable AUX
  //
  Out8042Command (DISABLE_AUX);
  WaitOutputEmpty (CLEAR_BUFF_TIMEOUT);

  return Status;
//[-end-210511-IB05660163-modify]//
}

/**
  Check whether there is Ps/2 mouse device in system

  @retval TRUE      - Keyboard in System.
  @retval FALSE     - Keyboard not in System.

**/
BOOLEAN
IsPs2MouseConnected (
  VOID
  )
{
  EFI_STATUS Status;

  Status = PS2MouseEnable ();

  if (!EFI_ERROR (Status)) {
    return TRUE;
  }

  return FALSE;
}
//[-start-190620-IB11270237-add]//
VOID
EFIAPI
UpdatePs2MouseStatus (
  IN EFI_EVENT            Event,
  IN VOID                 *Context
  )
{
  PLATFORM_NVS_AREA                   *PlatformNvsAreaProtocol;

  PlatformNvsAreaProtocol = (PLATFORM_NVS_AREA *) Context;
  PlatformNvsAreaProtocol->Ps2MouseEnable       = (UINT8) IsPs2MouseConnected ();
}
//[-end-190620-IB11270237-add]//
