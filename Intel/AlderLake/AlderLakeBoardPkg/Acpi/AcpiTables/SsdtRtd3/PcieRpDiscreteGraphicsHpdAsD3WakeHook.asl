/** @file
  ACPI RTD3 SSDT Library for Generic Pcie Rp with End Point as Discrete Graphics Device to Disable HPD SCI Implementated As D3 Wake Hook .

@copyright
  INTEL CONFIDENTIAL
  Copyright 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains a 'Sample Driver' and is licensed as such under the terms
  of your license agreement with Intel or your vendor. This file may be modified
  by the user, subject to the additional terms of the license agreement.

@par Specification Reference:
**/

/// @details
/// Code in this file uses following variables:
/// WAKG: Discrete Graphics HPD SCI implementated As D3 Wake GPIO - optional
/** @defgroup pcie_scope PCIe Root Port Scope **/

  //
  // GpioLib imports(DSDT)
  //
  External (\_SB.SHPO, MethodObj)

  Name (DGCE, 0) // DG CS Entry Happen or Exit Happen. 0: CS Exit, 1 : CS Entry

  // Disable Discrete Graphics HPD SCI implementated As D3 Wake GPIO
  // Since DG HPD wake is not required when System is in Sx or S0ix,
  // this method will be used to disable GD HPD SCI Event.
  Method (DHDW) {

    // Disable WAKE
    If (LOr (CondRefOf (WAKG), LNotEqual (WAKG, 0))) {
      \_SB.SHPO (WAKG, 1) // set gpio ownership to driver(0=ACPI mode, 1=GPIO mode)
      \_SB.CAGS (WAKG) // Clear GPIO Status if set for 2-tier GPIO
    }
  }

  // Enable Discrete Graphics HPD SCI implementated As D3 Wake GPIO
  // Since DG HPD wake is not required when System is in Sx or S0ix,
  // this method will be used to enable GD HPD SCI Event.
  Method (EHDW) {
    If (LEqual (DGCE, 1)) {
      // Force Disable the Wake Capability
      DHDW ()
      Return (1)// Enable Failed
    }

    // Enable WAKE
    If (LOr (CondRefOf (WAKG), LNotEqual (WAKG, 0))) {
      \_SB.SHPO (WAKG, 0) // set gpio ownership to ACPI(0=ACPI mode, 1=GPIO mode)
      \_SB.CAGS (WAKG) // Clear GPIO Status if set for 2-tier GPIO
    }
    Return (0) // Enable Successfully
  }
