/** @file
 
;******************************************************************************
;* Copyright 2021 Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corp.
;*
;******************************************************************************
*/
/** @file
  ACPI RTD3 SSDT table for ADL S Dddr5 Udimm  AEP (TBT version)

@copyright
  INTEL CONFIDENTIAL
  Copyright 2020 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains a 'Sample Driver' and is licensed as such under the terms
  of your license agreement with Intel or your vendor. This file may be modified
  by the user, subject to the additional terms of the license agreement.

@par Specification Reference:
**/
#include <Include/AcpiDebug.h>

DefinitionBlock (
    "Rtd3.aml",
    "SSDT",
    2,
    "Rtd3",
    "AdlSATbt",
    0x1000
    )
{
External(RTD3, IntObj)
External(S0ID)
External(OSYS)
External(HGMD)
External(\ECON, IntObj)

External(GBED)
External(SDS9)
Include ("Rtd3Common.asl")

External(\_SB.PC00.HDAS, DeviceObj)
External(\_SB.PC00.HDAS.VDID)
External(PEPC)
External(WRTO)
External(PRST)
External(WPRP)
External(WFCP)
External(PFCP)
External(WBRS)
External(PBRS)
External(GPDI)
External(GPLI)
External(GPLP)
External(GPLR)
External(GPI1)
External(TPP1)
External(TPR1)
External(PPDI)
External(PPLI)
External(PPLP)
External(PPLR)
External(PPI1)
External(PPP1)
External(PPR1)
External(PSPE)
External(PPSP)
External(PSPR)
External(PPSR)
External(PSWP)
External(RPNB)
External(PSW2)
External(RPN2)
External(PSP2)
External(PS2P)
External(PSR2)
External(PSP3)
External(PS3P)
External(PSR3)
External(SR3P)
External(PSW3)
External(RPN3)
External(SR2P)
External(WLWK)
External(SATP)
External(STPP)
External(SSDP)
External(SDPP)
External(SSDR)
External(SDRP)
External(SD2P)
External(SDP1)
External(SD2R)
External(SDR1)
External(SD3P)
External(SDP3)
External(SD3R)
External(SDR3)
External(SD4P)
External(SDP4)
External(SD4R)
External(SDR4)
External(TPLS)
External(P1PE)
External(P1PP)
External(P1RE)
External(P1RP)
External(P1WP)
External(PRP1)
External(P2PE)
External(P2PP)
External(P2RE)
External(P2RP)
External(P2WP)
External(PRP2)
External(VMDE)
External(FVWP)
External(FVRE)
External(FVRP)
External(FVPE)
External(FVPP)
External(FVSP)
External(\DTWA, IntObj)
External(\_SB.PEPD, DeviceObj)
External(\_SB.GHPO, MethodObj)
External(\_SB.SHPO, MethodObj)

External(\_SB.PC00.LPCB.H_EC.PVOL)
External(\_SB.PC00.XHCI.RHUB.HS14, DeviceObj)
External(\_SB.PC00.RP08.PXSX.WIST, MethodObj)
External(\_SB.GBTR, MethodObj)
External(\_SB.BTRK, MethodObj)
External(\_SB.PC00.UA00.BTH0, DeviceObj)
External(\_SB.PC00.PEG0.PEGP, DeviceObj)
External(\_SB.PC00.RP13.PXSX, DeviceObj)
External(\_SB.PC00.RP01.PXSX, DeviceObj)
External(\_SB.PC00.RP25.PXSX, DeviceObj)
External(\_SB.PC00.I2C1, DeviceObj)
External(\_SB.PC00.I2C1.TPL1, DeviceObj)
External(\_SB.PC00.I2C1.TPL1._STA, MethodObj)

External(SPPR) // PchNvs SataPortPresence
External(\PSOC, MethodObj) // PS_ON control method
External(\PSOS, MethodObj) // PS_ON status method

//[-start-201124-IB09480117-add]//
#if FeaturePcdGet (PcdHybridGraphicsSupported)
#define PCI_SCOPE             \_SB.PC00
#define DGPU_BRIDGE_SCOPE     PCI_SCOPE.PEG0
#define DGPU_DEVICE           PEGP
#define DGPU_SCOPE            DGPU_BRIDGE_SCOPE.DGPU_DEVICE
#define NVIDIA_VID            0x10DE
#define NV_GEN_17             17

External (DGPU_BRIDGE_SCOPE.CEDR, FieldUnitObj)
External (DGPU_BRIDGE_SCOPE.CMDR, FieldUnitObj)
External (DGPU_BRIDGE_SCOPE.D0ST, FieldUnitObj)
External (DGPU_BRIDGE_SCOPE.LREN, FieldUnitObj)
External (DGPU_SCOPE.HDAE, FieldUnitObj)
External (DGPU_SCOPE.LTRE, IntObj)
External (DGPU_SCOPE.SSSV, FieldUnitObj)
External (PCI_SCOPE.DGCX, IntObj)
External (PCI_SCOPE.DGPV, FieldUnitObj)
External (PCI_SCOPE.DSSV, FieldUnitObj)
External (PCI_SCOPE.NVGE, FieldUnitObj)
External (PCI_SCOPE.GC6I, MethodObj)
External (PCI_SCOPE.GC6O, MethodObj)
External (PCI_SCOPE.NINI, MethodObj)
External (PCI_SCOPE.OPTF, FieldUnitObj)
External (PCI_SCOPE.TDGC, IntObj)
External (PCI_SCOPE.TGPC, BuffObj)
#endif
//[-end-201124-IB09480117-add]//

//
// Platform power distribution description.
// Keep the power resources in the "\" scope
// to allow Windows to disable them if they are not
// referenced by any devices. Windows will not turn off
// unused power resources located in device scope.
//
Scope(\) {
  Include ("PsonVeto.asl")
  //
  // Note: SATA PRT0~3 on ADL-S is conencted to M.2 slot
  // and will be powered up from the auxilary power supply during
  // PS_ON so it does not depend on ATX power resource. Only PRT5 is
  // aplicable for ADL-S AEP
  PowerResource(SPR5, 0, 0) {
    Method(_STA) {Return(\STMS(5))}
    Method(_ON) {\STMC(5, 1)}
    Method(_OFF) {\STMC(5, 0)}
  }
} // End of Scope(\)

// PCIe root ports - START

    ///
    /// 2.5Gbe LAN Foxville I225
    ///

    //FVWP, 32,       // [FoxLanWakeGpio         ] Foxville I225 Wake Gpio pin
    //FVRE, 32,       // [FoxLanRstGpio          ] Foxville I225 Reset Gpio pin
    //FVRP, 8,        // [FoxLanRstGpioPolarity  ] Foxville I225 Reset Gpio pin polarity
    //FVPE, 32,      // [FoxLanDisableNGpio  ] Foxville I225 Disable N Gpio pin
    //FVPP, 8,       // [FoxLanDisableNGpioPolarity] Foxville I225 Disable N Gpio pin polarity
    //FVSP, 8,       // [FoxLanSupport] Foxville I225 support configuration
    If(LEqual(FVSP,1)) {
      Scope(\_SB.PC00.RP07) {
        Name(RSTG, Package() {0, 0})
        Store(FVRE, Index(RSTG, 0))
        Store(FVRP, Index(RSTG, 1))
        Name(PWRG, Package() {0, 0})
        Store(FVPE, Index(PWRG, 0))
        Store(FVPP, Index(PWRG, 1))
        Name(WAKG, 0)
        Store(FVWP, WAKG)
        Name(WAKP, 0)
        Name(SCLK, 5)
//[-start-211125-IB05660187-add]//
        #undef DGPU_BRIDGE_SCOPE
        #undef DGPU_DEVICE
        #define DGPU_BRIDGE_SCOPE    \_SB.PC00.RP07
        #define DGPU_DEVICE          PXSX
//[-end-211125-IB05660187-add]//
        Include("PcieRpGenericPcieDeviceRtd3.asl")
      }
    }

#ifndef PCH_ADPP
    ///
    /// PCIE RTD3 - PCIE SLOT 1 - X4 CONNECTOR (Back Panel Tbt Controller)
    ///
    //PSPE, 32,     // [PcieSlot1PowerEnableGpio           ] Pcie Slot 1 Power Enable Gpio pin
    //PPSP, 8,      // [PcieSlot1PowerEnableGpioPolarity   ] Pcie Slot 1 Power Enable Gpio pin polarity
    //PSPR, 32,     // [PcieSlot1RstGpio                   ] Pcie Slot 1 Rest Gpio pin
    //PPSR, 8,      // [PcieSlot1RstGpioPolarity           ] Pcie Slot 1 Rest Gpio pin polarity
    //PSWP, 32,     // [PcieSlot1WakeGpio       ] Pcie Slot Wake Gpio pin
    Scope(\_SB.PC00.RP21) {
      Name(RSTG, Package() {0, 0})
      Store(PSPR, Index(RSTG, 0))
      Store(PPSR, Index(RSTG, 1))
      Name(PWRG, Package() {0, 0})
      Store(PSPE, Index(PWRG, 0))
      Store(PPSP, Index(PWRG, 1))
      Name(WAKG, 0)
      Store(PSWP, WAKG)
      Name(WAKP, 0)             // must be defined due to compiler bug, will be removed when fixed https://bugs.acpica.org/show_bug.cgi?id=1432
      Name(SCLK, 10)
      Name(TUID, 0) // TBT RP Number 0 for RP21
      Include("Rtd3PcieTbt.asl")

      //
      // The PRx resource list is dependend on how many groups added.
      //
      Method(_PR0) {
        // TODO add switch for exposing PDPG or not
        Return(Package(){\_SB.PC00.RP21.PXP})
      }
      Method(_PR3) {
        // TODO add switch for exposing PDPG or not
        Return(Package(){\_SB.PC00.RP21.PXP})
      }

      Method (TBNF, 0) {
        // TBT attached Root port specific method and needed for TBT RTD3 only.
        // It's not related to GPE event but handles CS wake up(Screen On event).
        Notify(\_SB.PC00.RP21, 0x02)   // device wake, must match with TBT attached PCIe root port.
      }

      Method (SXWK, 0, Serialized) { // Enable DTbt PCIE RP Wake capability from Sx
        // enable WAKE
        If(CondRefOf (WAKG)) {
          If(LAnd (LNotEqual (WAKG, 0), \DTWA)) {
            \_SB.CAGS (WAKG) // Clear Pending Wake GPIO Interrupt.
            \_SB.SHPO (WAKG, 0) // Enable Wake GPIO.
          }
        }
      }
    }

    ///
    /// PCIE RTD3 - PCIE SLOT 2 - X4 CONNECTOR (Front Panel Tbt Controller)
    ///
    //PSP2, 32,     // [PcieSlot2PowerEnableGpio          ] Pcie Slot 2 Power Enable Gpio pin
    //PS2P, 8,      // [PcieSlot2PowerEnableGpioPolarity  ] Pcie Slot 2 Power Enable Gpio pin polarity
    //PSR2, 32,     // [PcieSlot2RstGpio                  ] Pcie Slot 2 Rest Gpio pin
    //SR2P, 8,      // [PcieSlot2RstGpioPolarity          ] Pcie Slot 2 Rest Gpio pin polarity
    //PSW2, 32,     // [PcieSlot2WakeGpio      ] Pcie Slot 2 Wake Gpio pin
    Scope(\_SB.PC00.RP25) {
      Name(RSTG, Package() {0, 0})
      Store(PSR2, Index(RSTG, 0))
      Store(SR2P, Index(RSTG, 1))
      Name(PWRG, Package() {0, 0})
      Store(PSP2, Index(PWRG, 0))
      Store(PS2P, Index(PWRG, 1))
      Name(WAKG, 0)
      Store(PSW2, WAKG)
      Name(WAKP, 0)             // must be defined due to compiler bug, will be removed when fixed https://bugs.acpica.org/show_bug.cgi?id=1432
      Name(SCLK, 13)
      Name(TUID, 1) // TBT RP Number 1 for RP25
      Include("Rtd3PcieTbt.asl")

      //
      // The PRx resource list is dependend on how many groups added.
      //
      Method(_PR0) {
        // TODO add switch for exposing PDPG or not
        Return(Package(){\_SB.PC00.RP25.PXP})
      }
      Method(_PR3) {
        // TODO add switch for exposing PDPG or not
        Return(Package(){\_SB.PC00.RP25.PXP})
      }

      Method (TBNF, 0) {
        // TBT attached Root port specific method and needed for TBT RTD3 only.
        // It's not related to GPE event but handles CS wake up(Screen On event).
        Notify(\_SB.PC00.RP25, 0x02)   // device wake, must match with TBT attached PCIe root port.
      }

      Method (SXWK, 0, Serialized) { // Enable DTbt PCIE RP Wake capability from Sx
        // enable WAKE
        If(CondRefOf (WAKG)) {
          If(LAnd (LNotEqual (WAKG, 0), \DTWA)) {
            \_SB.CAGS (WAKG) // Clear Pending Wake GPIO Interrupt.
            \_SB.SHPO (WAKG, 0) // Enable Wake GPIO.
          }
        }
      }
    }
#endif

    /// No RTD3 Support for EDSFF

    ///
    /// PCH M.2 SSD RTD3
    ///
    //SSDP, 32,     // [PchM2SsdPowerEnableGpio           ] Pch M.2 SSD Power Enable Gpio pin
    //SDPP, 8,      // [PchM2SsdPowerEnableGpioPolarity   ] Pch M.2 SSD Power Enable Gpio pin polarity
    //SSDR, 32,     // [PchM2SsdRstGpio                   ] Pch M.2 SSD Reset Gpio pin
    //SDRP, 8,      // [PchM2SsdRstGpioPolarity           ] Pch M.2 SSD Reset Gpio pin polarity
    Scope(\_SB.PC00.RP13) {
      Name(RSTG, Package() {0, 0})
      Store(SSDR, Index(RSTG, 0))
      Store(SDRP, Index(RSTG, 1))
      Name(PWRG, Package() {0, 0})
      Store(SSDP, Index(PWRG, 0))
      Store(SDPP, Index(PWRG, 1))
      Name(WAKG, 0)              // must be defined due to compiler bug, will be removed when fixed https://bugs.acpica.org/show_bug.cgi?id=1432
      Name(WAKP, 0)              // must be defined due to compiler bug, will be removed when fixed https://bugs.acpica.org/show_bug.cgi?id=1432
      Name(SCLK, 12)
//[-start-211125-IB05660187-add]//
      #undef DGPU_BRIDGE_SCOPE
      #undef DGPU_DEVICE
      #define DGPU_BRIDGE_SCOPE    \_SB.PC00.RP13
      #define DGPU_DEVICE          PXSX
//[-end-211125-IB05660187-add]//
      Include("PcieRpGenericPcieDeviceRtd3.asl")
      Scope(\_SB.PC00.RP13.PXSX) {
        Include("PcieRpSsdStorageRtd3Hook.asl")
      }
    }

    ///
    /// CPU M.2 SSD RTD3
    ///
    //SD2P, 32,     // [M2Ssd2PowerEnableGpio             ] PCIe x4 M.2 SSD Power Enable Gpio pin
    //SDP1, 8,      // [M2Ssd2PowerEnableGpioPolarity     ] PCIe x4 M.2 SSD Power Enable Gpio pin polarity
    //SD2R, 32,     // [M2Ssd2RstGpio                     ] PCIe x4 M.2 SSD Reset Gpio pin
    //SDR1, 8,      // [M2Ssd2RstGpioPolarity             ] PCIe x4 M.2 SSD Reset Gpio pin polarity
    Scope(\_SB.PC00.PEG0) {
      Name(RSTG, Package() {0, 0})
      Store(SD2R, Index(RSTG, 0))
      Store(SDR1, Index(RSTG, 1))
      Name(PWRG, Package() {0, 0})
      Store(SD2P, Index(PWRG, 0))
      Store(SDP1, Index(PWRG, 1))
      Name(WAKG, 0)
      Name(WAKP, 0)             // must be defined due to compiler bug, will be removed when fixed https://bugs.acpica.org/show_bug.cgi?id=1432
      Name(SCLK, 11)
//[-start-211125-IB05660187-add]//
      #undef DGPU_BRIDGE_SCOPE
      #undef DGPU_DEVICE
      #define DGPU_BRIDGE_SCOPE    \_SB.PC00.PEG0
      #define DGPU_DEVICE          PEGP
//[-end-211125-IB05660187-add]//
      Include("PcieRpGenericPcieDeviceRtd3.asl")
      Scope(\_SB.PC00.PEG0.PEGP) {
        Include("PcieRpSsdStorageRtd3Hook.asl")
      }
    }
    ///
    /// CPU PEG1
    ///
    //P1PE, 32,     // [PegSlot1PwrEnableGpioNo           ] PEG slot 1 Power Enable Gpio pin
    //P1PP, 8,      // [PegSlot1PwrEnableGpioPolarity     ] PEG slot 1 Power Enable Gpio pin polarity
    //P1RE, 32,     // [PegSlot1RstGpioNo                 ] PEG slot 1 Reset Gpio pin
    //P1RP, 8,      // [PegSlot1RstGpioPolarity           ] PEG slot 1 Reset Gpio pin polarity
    //P1WP, 32,     // [PegSlot1WakeGpioPin               ] PEG slot 1 Wake Gpio pin
    //PRP1, 8,      // [PegSlot1RootPort                  ] PEG slot 1 Root Port
    Scope(\_SB.PC00.PEG1) {
      Name(RSTG, Package() {0, 0})
      Store(P1RE, Index(RSTG, 0))
      Store(P1RP, Index(RSTG, 1))
      Name(PWRG, Package() {0, 0})
      Store(P1PE, Index(PWRG, 0))
      Store(P1PP, Index(PWRG, 1))
      Name(WAKG, 0)
      Store(P1WP, WAKG)
      Name(WAKP, 0)
      Name(SCLK, 0)
      //
      // CEM slot power limit
      // note: even though PCIe CEM form factor
      // defines maximum auxilary power as 1238mW
      // our PCIe CEM slots can safely handle 3300mW.
      //
      Name(PSPL, 3300)
      //
      // Block core power removal.
      // Arg0 - 0: Allow for core power removal, 1: block core power removal
      //
      Method(BCPR, 1, Serialized) {\PCPA(3, Arg0)}
      Include("PcieAuxPowerBudgeting.asl")
//[-start-211125-IB05660187-add]//
      #undef DGPU_BRIDGE_SCOPE
      #undef DGPU_DEVICE
      #define DGPU_BRIDGE_SCOPE    \_SB.PC00.PEG1
      #define DGPU_DEVICE          PEGP
//[-end-211125-IB05660187-add]//
      Include("PcieRpGenericPcieDeviceRtd3.asl")
    }

// PCIe root ports - END

//
// SATA - START
//
  Scope(\_SB.PC00.SAT0) {
   Scope(PRT5) {
     Name(_S0W, 4)
     Method(SPON) {
       \STMC(5, 1)
     }
     Method(SPOF) {
       \STMC(5, 0)
     }
     External(PRES, MethodObj)
     If (PRES()) {
       Method(_PR0) {Return(Package() {SPR5})}
       Method(_PR3) {Return(Package() {SPR5})}
     }
    }
  }

//
// SATA - END
//

//
// VMD - START
//
#if (FixedPcdGetBool (PcdVmdEnable) == 1)
  Include ("Rtd3Vmd.asl")
#endif
//
// VMD - END
//

//
// USB - START
//

//
// Report BT power resource if CNVd BT is UART.
//
If (LEqual (SDS9, 2)) { // Only required for CNVd BT over UART selection
  Scope (\_SB.PC00.UA00.BTH0) {
    Method(_PS0, 0, Serialized)
    {
      \_SB.BTRK (0x01) // Turn on BT
    }
    Method(_PS3, 0, Serialized)
    {
      \_SB.BTRK (0x00) // Turn off BT
    }
  }
}

External(XDCE)
//
// XDCI - start
//
  If(LEqual(XDCE,1)) {
    Scope(\_SB)
    {
      //
      // Dummy power resource for USB D3 cold support
      //
      PowerResource(USBC, 0, 0)
      {
        Method(_STA) { Return (0xF) }
        Method(_ON) {}
        Method(_OFF) {}
      }
    }

    Scope(\_SB.PC00.XDCI)
    {
      OperationRegion (GENR, SystemMemory, Add(And(XDCB, 0xFFFFFFFFFFFFFF00), 0x10F81C), 0x4)  //AON MMIO - 10F81C: APBFC_U3PMU_CFG5
      Field (GENR, WordAcc, NoLock, Preserve)
      {
            ,   2,
        CPME,   1,    //bit2 core_pme_en
        U3EN,   1,    //bit3 u3_pme_en
        U2EN,   1     //bit4 u2_pme_en
      }

      Method (_PS3, 0, NotSerialized)
      {
        Store (One, CPME)
        Store (One, U2EN)
        Store (One, U3EN)

        \_SB.CSD3(MODPHY_SPD_GATING_XDCI)
      }
      Method (_PS0, 0, NotSerialized)
      {
        Store (Zero, CPME)
        Store (Zero, U2EN)
        Store (Zero, U3EN)

        If(LEqual(DVID,0xFFFF)) {
          Return(Zero)
        }

        \_SB.CSD0(MODPHY_SPD_GATING_XDCI)
        Return(Zero)
      }

      Method (_RMV, 0, NotSerialized)  // _RMV: Removal Status
      {
        Return (Zero)
      }

      Method (_PR3, 0, NotSerialized)  // _PR3: Power Resources for D3hot
      {
        Return (Package (0x01)
        {
          USBC // return dummy package
        })
      }
    } // Scope(\_SB.PC00.XDCI)
  } // XDCE
//
// XDCI - end
//

//
// USB - END
//

If (LNotEqual(GBES,0)) {
  Scope(\_SB.PC00.GLAN)
  {
    Method (_PS3, 0, NotSerialized)
    {
      \_SB.CSD3(MODPHY_SPD_GATING_GBE)
    }
    Method (_PS0, 0, NotSerialized)
    {
      If(LNot(GBED)){  // If GBE_FDIS_PMC == 0
        \_SB.CSD0(MODPHY_SPD_GATING_GBE)
      }
    }
  } // Scope(\_SB.PC00.GLAN)
}

//
// Human Interface Devices Start
//

//
//No Touch panel for AEP, Removed PCH I2C1 TPL1

//
    //Power Resource for Audio Codec
    Scope(\_SB.PC00)
    {
      PowerResource(PAUD, 0, 0) {
        /// Namespace variable used:
        Name(PSTA, One) /// PSTA: Physical Power Status of Codec 0 - OFF; 1-ON
        Name(ONTM, Zero) /// ONTM: 0 - Not in Speculative ON ; Non-Zero - elapsed time in Nanosecs after Physical ON

        Name(_STA, One) /// _STA: PowerResource Logical Status 0 - OFF; 1-ON

        ///@defgroup pr_paud Power Resource for onboard Audio CODEC

        Method(_ON, 0){     /// _ON method \n
          Store(One, _STA)        ///- Set Logocal power state
          PUAM() ///- Call PUAM() to tansition Physical state to match current logical state
                    ///@addtogroup pr_paud
        } // End _ON

        Method(_OFF, 0){    /// _OFF method \n
          Store(Zero, _STA)    ///- Set the current power state
          PUAM() ///- Call PUAM() to tansition Physical state to match current logical state
        ///@addtogroup pr_paud
        } // End _OFF

        ///  PUAM - Power Resource User Absent Mode for onboard Audio CODEC
        ///  Arguments:
        ///
        ///  Uses:
        ///      _STA - Variable updated by Power Resource _ON/_OFF methods \n
        ///      \\UAMS - Variable updated by GUAM method to show User absent present \n
        ///      ONTM - Local variable to store ON time during Speculative ON \n
        /// ______________________________
        // |  Inputs      |   Outputs    |
        // ______________________________
        // | _STA | \UAMS | GPIO | ONTM |
        // ______________________________
        // |   1  |   0   | ON   |   !0 |
        // |   1  |   !0  | ON   |   !0 |
        // |   0  |   0   | ON   |   !0 |
        // |   0  |   !0  | OFF  |   0  |
        // ______________________________
                    /**
                    <table>
                    <tr> <th colspan="2"> Inputs <th colspan="2"> Output
                    <tr> <th>_STA <th> \\UAMS <th> GPIO <th>ONTM
                    <tr> <td>1 <td>0 <td>ON <td>!0
                    <tr> <td>1 <td>!0<td>ON <td>!0
                    <tr> <td>0 <td>0 <td>ON <td>!0
                    <tr> <td>0 <td>!0<td>OFF<td> 0
                    </table>
                    **/
        ///@addtogroup pr_paud_puam
        Method(PUAM, 0, Serialized)
        {
                // power rail = NOT there for ICL U
                // Note:- Audio Power enable need not be implemented by default and need rework if we need power control.
          If (LAnd(LEqual(^_STA, Zero), LNotEqual(\UAMS, Zero))) { ///New state = OFF Check if (_STA ==0 && \UAMS != 0) \n
          } Else { /// New state = ON (_STA=1) or (_STA=0 and \UAMS=0)
            /// Turn power on \n
            If(LNotEqual(^PSTA, One)) { ///- Skip below if Power Resource is already in ON
              Store(One, ^PSTA)  ///- >> Set PSTA to 1
              Store(Timer(), ^ONTM) ///- >> Start the timer for this PR
            }
          }
        ///@defgroup pr_paud_puam Power Resource User Absent Mode for onboard Audio CODEC
        } //PUAM
      } //PAUD
    } //Scope(\_SB.PC00)

//
// Check HDAS (HD-Audio) controller present
//
    If(LNotEqual(\_SB.PC00.HDAS.VDID, 0xFFFFFFFF)) {
      Scope(\_SB.PC00.HDAS) {
        Method(PS0X,0,Serialized)     /// Platform D0 Method for HD-A Controller
        {
          If(LEqual(\_SB.PC00.PAUD.ONTM, Zero)){    ///- Check if ONTM=0
            Return()
          }

          ///
          ///- Make sure "D0 delay" (AUDD) delay is elapsed before returning _PS0
          ///- Local0: Elapse time since the _ON method
          ///- VRRD: VR Rampup Delay
          ///- AUDD: Time required for device to be ready after power on
          ///- Local1 = AUDD + VRRD: Need to incorporate VRRD since the _ON method no longer has VR Rampup Delay
          ///- So only need sleep for (Local1 - Local0), the amount of time remaining since the _ON method
          ///
          Divide(Subtract(Timer(), \_SB.PC00.PAUD.ONTM), 10000, , Local0) ///- Store Elapsed time in ms, ignore remainder
          Add(AUDD, VRRD, Local1) ///- Incorporate VR Rampup Delay
          If(LLess(Local0, Local1)) { ///- Do not sleep if already past the delay requirement audio
            ///- Delay for device init
            Sleep(Subtract(Local1, Local0)) ///- Sleep (AUDD + VRRD - time elapsed)
          }
        }

        ///Associate _PR0 with \ref pr_paud
        Name(_PR0, Package(){\_SB.PC00.PAUD})
      ///@defgroup hdef_scope       Intel High Definition Audio Scope
      }
    }// If(LNotEqual(\_SB.PC00.HDAS.VDID, 0xFFFFFFFF))
//GPE Event handling - Start
  Scope(\_GPE) {
    //
    // Alternate _L6F(), to handle 2-tier RTD3 GPE events here
    //
    Method(AL6F) {
      // Maple Ridge BP (SLOT1) wake event
      If (\_SB.ISME(PSWP))
      {
        \_SB.SHPO(PSWP, 1) // set gpio ownership to driver(0=ACPI mode, 1=GPIO mode)
        Notify(\_SB.PC00.RP21, 0x02)   // device wake
        \_SB.CAGS(PSWP)
      }
      // Maple Ridge FP (SLOT2) wake event
      If (\_SB.ISME(PSW2))
      {
        \_SB.SHPO(PSW2, 1) // set gpio ownership to driver(0=ACPI mode, 1=GPIO mode)
        Notify(\_SB.PC00.RP25, 0x02)     // device wake
        \_SB.CAGS(PSW2)    // Clear GPE event status
      }

      // Foxville Lan wake event
      If (\_SB.ISME(FVWP))
      {
        \_SB.SHPO(FVWP, 1) // set gpio ownership to driver(0=ACPI mode, 1=GPIO mode)
        Notify(\_SB.PC00.RP07, 0x02)     // device wake
        \_SB.CAGS(FVWP)    // Clear GPE event status
      }

      // WLAN wake event
      If (\_SB.ISME(WLWK))
      {
        \_SB.SHPO(WLWK, 1) // set gpio ownership to driver(0=ACPI mode, 1=GPIO mode)
        Notify(\_SB.PC00.RP08, 0x02)      // device wake
        \_SB.CAGS(WLWK)    // WIFI_WAKE_N
      }
    }
  } //Scope(\_GPE)
//GPE Event handling - End
} // End SSDT