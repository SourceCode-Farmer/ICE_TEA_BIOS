/** @file
  ACPI RTD3 support for VMD remapped SATA drives

 @copyright
  INTEL CONFIDENTIAL
  Copyright 2019 - 2020 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains a 'Sample Driver' and is licensed as such under the terms
  of your license agreement with Intel or your vendor. This file may be modified
  by the user, subject to the additional terms of the license agreement.

@par Specification Reference:
**/
// Power resource for NVMx (remapped) device.

//
// DSDT imports
//
External(VSON, MethodObj)
External(VSOF, MethodObj)
External(VS3D, MethodObj)
External(\STD3, FieldUnitObj) // 0: D3 disable. 1: D3Hot support for Storages. 2: D3Cold support for Storages.

//
// The deepest D-state supported by this device in the S0 system sleeping state where the device can wake itself,
// "4" represents D3cold.
//
Method(_S0W, 0x0, NotSerialized)
{
  If(CondRefOf(\STD3)) {
    If (LEqual(\STD3, 0x02)) {
      Return(0x4)
    }
    Return(0x3)
  }
  Return(0x3)
}

If (CondRefOf(\STD3)) {
  If(LEqual(\STD3, 0x02)) { // Is RTD3 support for storage enabled in Bios Setup?
    Method(_PR0) {
      Return(Package(){NVPR})
    }

    Method(_PR3) {
      Return(Package(){NVPR})
    }
  }
}

If (CondRefOf(\STD3)) {
  If(LEqual(\STD3, 0x02)) { // Is RTD3 support for storage enabled in Bios Setup?
    PowerResource(NVPR, 4, 0) { // Power Resource for remapped devices
      Name(_STA, 0x01)

      Method(_ON, 0, Serialized) {
        //
        // If VMD RAID support has been added check if RTD3 isn't blocked - VR3D method
        // If RTD3 is blocked return.
        //
        If (LNot(VS3D())) {
          VSON()
          Store(0x1, _STA);
        }
      }

      Method(_OFF, 0, Serialized) {
        //
        // If VMD RAID support has been added check if RTD3 isn't blocked - VR3D method
        // If RTD3 is blocked return.
        //
        If (LNot(VS3D())) {
          VSOF()
          Store(0x0, _STA);
        }
      }
    }
  }
}