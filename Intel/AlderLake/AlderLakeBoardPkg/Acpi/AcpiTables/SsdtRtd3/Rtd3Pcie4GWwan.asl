/** @file
  ACPI RTD3 SSDT table for PCIe 4G WWAN

@copyright
  INTEL CONFIDENTIAL
  Copyright 2020 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains a 'Sample Driver' and is licensed as such under the terms
  of your license agreement with Intel or your vendor. This file may be modified
  by the user, subject to the additional terms of the license agreement.

@par Specification Reference:
**/

//
// PCIe slot power resource definition
//
PowerResource(PXP, 0, 0) {
  Method(_STA, 0, Serialized) {
    If (LEqual (VDID, 0xFFFFFFFF)) {
      Return(0)
    }
    Return(PSTA())
  }

  Method(_ON, 0, Serialized) {
    If (LEqual (VDID, 0xFFFFFFFF)) {
      Return()
    }

    If (LEqual(ONEN, 0)) {
      Return()
    }

    \_SB.SHPO(WAKG, 1)

    If(CondRefOf(WOFF)) {
      If(LNotEqual(WOFF, Zero)) {
        Divide(Subtract(Timer(), WOFF), 10000, , Local0) // Store Elapsed time in ms, ignore remainder
        If(LLess(Local0, 500)) {                          // If Elapsed time is less than 500ms
          Sleep(Subtract(500, Local0))                    // Sleep for the remaining time
        }
      }
    }

    //
    // Turn on slot power
    //
    PON()

    //
    // Trigger L2/L3 ready exit flow in rootport - transition link to Detect
    //
    L23D()

    // This time includes PCIe Detect Timer and PCIe Link Establishment Timer for modem. Time delay should
    // kick in L3 Path only
    If(CondRefOf(WOFF)) {
      If(LNotEqual(WOFF, Zero)) {
        Store(0, WOFF)
        Sleep(500)
      }
    }

    Store(1, OFEN)
    Store(0, ONEN)
  }

  Method(_OFF, 0, Serialized) {
    If (LEqual (VDID, 0xFFFFFFFF)) {
      Return()
    }

    If (LEqual(OFEN, 0)) {
      Return()
    }

    //
    // Trigger L2/L3 ready entry flow in rootport
    //
    DL23()

    //
    // Turn off slot power
    //
    POFF()

    If(LEqual(WKEN, 0)) {
      // Drive BB RESET Pin low
      \PIN.ON(BRST)
      Sleep(2) // 2ms
      \PIN.OFF(PWRG)          // Set power pin to low
      If(CondRefOf(WOFF)) {
        Store(Timer(), WOFF) // Start OFF timer here
      }
    }

    // Enable WAKE
    If(CondRefOf (WAKG)) {
      If(LAnd(LNotEqual(WAKG, 0), WKEN)) {
        \_SB.SHPO(WAKG, 0)
      } Else {
        \_SB.SHPO(WAKG, 1)
      }
    }

    Store(0, WKEN)
    Store(0, OFEN)
    Store(1, ONEN)
  }
}
