/** @file
  ACPI RTD3 SSDT table for PCIe 5G WWAN

@copyright
  INTEL CONFIDENTIAL
  Copyright 2020 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains a 'Sample Driver' and is licensed as such under the terms
  of your license agreement with Intel or your vendor. This file may be modified
  by the user, subject to the additional terms of the license agreement.

@par Specification Reference:
**/

//
// PCIe slot power resource definition for 5G
//
PowerResource(PXP5, 0, 0) {
  Method(_STA, 0, Serialized) {
    If (LEqual (VDID, 0xFFFFFFFF)) {
      Return(0)
    }
    Return(PSTA())
  }

  Method(_ON, 0, Serialized) {
    //
    // Acquire _ON mutex and save acquire result so we can check for Mutex acquired
    //
    Store (Acquire (\WWMT, 1000), Local0)
    If (LEqual (Local0, Zero)) // check for _ON Mutex acquired
    {
      If (LEqual (VDID, 0xFFFFFFFF)) {
        Release(\WWMT)
        Return()
      }

      If (LEqual(OFEN, 1)) {
        Release(\WWMT)
        Return()
      }

      \_SB.SHPO(WAKG, 1) // Change HostOwnership to GPIO Driver mode for disabling wake

      //
      // Turn on slot power
      //
      PON5()

      //
      // Trigger L2/L3 ready exit flow in rootport - transition link to Detect
      //
      L23D()

      If(CondRefOf(WOFF)) {
        If(LNotEqual(WOFF, Zero)) {
          Store(0, WOFF)

          // This time includes PCIe Detect Timer and PCIe Link Establishment Timer for modem. Time delay should
          // kick in L3 Path only
          Sleep(TR2P)
        }
      }

      Store(1, OFEN)

      //
      // Release WWAN _ON mutex
      //
      Release(\WWMT)
    }
  }

  Method(_OFF, 0, Serialized) {
    //
    // Acquire _OFF mutex and save acquire result so we can check for Mutex acquired
    //
    Store (Acquire (\WWMT, 1000), Local0)
    If (LEqual (Local0, Zero)) // check for _OFF Mutex acquired
    {
      If (LEqual (VDID, 0xFFFFFFFF)) {
        Release(\WWMT)
        Return()
      }

      If (LEqual(OFEN, 0)) {
        Release(\WWMT)
        Return()
      }

      //
      // Trigger L2/L3 ready entry flow in rootport
      //
      DL23()

      //
      // Turn off slot power
      //
      POF5()

      Store(0, WKEN)
      Store(0, OFEN)

      //
      // Release WWAN _OFF mutex
      //
      Release(\WWMT)
    }
  }
}

// Turn on power to PCIe Slot for 5G
// Since this method is also used by the remapped devices to turn on power to the slot
// this method should not make any access to the PCie config space.
Method(PON5, 0, Serialized) {
  // Restore power to the modPHY
  \_SB.PSD0(SLOT)

  // Turn ON Power for PCIe Slot
  If(CondRefOf(WOFF)) {
    If(LNotEqual(WOFF, 0)) {
      // Delay by TFDI ms if required using WOFF
      Divide(Subtract(Timer(), WOFF), 10000, , Local0)  // Store Elapsed time in ms, ignore remainder
      If(LLess(Local0, TFDI)) {                         // If Elapsed time is less than TFDI ms
        Sleep(Subtract(TFDI, Local0))                   // Sleep for the remaining time
      }

      If(CondRefOf(PWRG)) {
        \PIN.ON(PWRG)
      }
      // Delay of TN2B ms between de-assertion of FCPO# and de-assertion of BBRST#
      Sleep(TN2B)

      If(CondRefOf(BRST)) {
        // Drive BB Reset Pin high
        \PIN.OFF(BRST)
      }
      // Delay of TB2R ms between de-assertion of BBRST# and de-assertion of PERST#
      Sleep(TB2R)
    }
  }

  If(CondRefOf(SCLK)) {
    SPCO(SCLK, 1)
  }

  // De-assert Reset Pin
  \PIN.OFF(RSTG)
}

// Turn off power to PCIe Slot 5G
// Since this method is also used by the remapped devices to turn off power to the slot
// this method should not make any access to the PCIe config space.
Method(POF5, 0, Serialized) {

  // Assert Reset Pin
  // Reset pin is mandatory for correct PCIe RTD3 flow
  \PIN.ON(RSTG)

  // Enable modPHY power gating
  // This must be done after the device has been put in reset
  \_SB.PSD3(SLOT)

  //
  // On RTD3 entry, BIOS will instruct the PMC to disable source clocks.
  // This is done through sending a PMC IPC command.
  //
  If(CondRefOf(SCLK)) {
    SPCO(SCLK, 0)
    Sleep(16)
  }

  // Power OFF for Slot
  If(LEqual(WKEN, 0)) {
    // Delay of TR2B ms between assertion of PERST# and assertion of BBRST#
    Sleep(TR2B)
    If(CondRefOf(BRST)) {
      \PIN.ON(BRST) // Drive BB RESET Pin low
    }
    // Delay of TB2F ms between assertion of BBRST# and assertion of FCPO#
    Sleep(TB2F)
    If(CondRefOf(PWRG)) {
      \PIN.OFF(PWRG)
    }
    // Store current timestamp in WOFF
    If(CondRefOf(WOFF)) {
      Store(Timer(), WOFF)
    }
  }

  // Enable WAKE
  If(CondRefOf (WAKG)) {
    If(LAnd(LNotEqual(WAKG, 0), WKEN)) {
      \_SB.SHPO(WAKG, 0)
    } Else {
      \_SB.SHPO(WAKG, 1)
    }
  }
}

Scope(PXSX) {
  //
  // Method _PS0: Provides required delay for WWAN device to transition to D0 from another WWAN device state
  //
  Method(_PS0, 0, Serialized) {
    //Wait for Link Active with retry every 16 ms with a timeout value of 176 ms.
    Store(0, Local0)
    While(LEqual(WWAN_PCIE_ROOT_PORT.LASX, 0)) {
      If(Lgreater(Local0, 20)) // Poll for ~300ms
      {
        Break
      }
      Sleep(16)
      Increment(Local0)
    }
  }

  //
  // Method _PS3: Dummy _PS3() to comply with ACPI Spec
  //
  Method(_PS3, 0, Serialized) {
  }
}
