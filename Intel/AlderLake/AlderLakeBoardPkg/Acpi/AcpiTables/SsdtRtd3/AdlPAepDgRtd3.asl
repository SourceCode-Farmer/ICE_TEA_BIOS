/** @file
  ACPI RTD3 SSDT table for ADL P AEP DG02

@copyright
  INTEL CONFIDENTIAL
  Copyright 2020 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains a 'Sample Driver' and is licensed as such under the terms
  of your license agreement with Intel or your vendor. This file may be modified
  by the user, subject to the additional terms of the license agreement.

@par Specification Reference:
**/
#include <Include/AcpiDebug.h>

DefinitionBlock (
    "Rtd3.aml",
    "SSDT",
    2,
    "Rtd3",
    "AdlP_384",
    0x1000
    )
{
External(S0ID)

//
//Pcie Slot 1 Pins
//
External(PSPR)
External(PPSP)
External(PSPE)
External(PPSR)
External(PSWP)

//
// Pch ssd Pins
//
External(SSDP)
External(SSDR)
External(SDRP)
External(SDPP)

// Touch Pad
External(TPDT)

//
// CPU ssd2 Pins
//
External(SD5R)
External(SDR5)
External(SD5P)
External(SDP5)

//
// DG Pins
//
External(DG2P)
External(DGP2)
External(DGR2)
External(DG2R)
External(P1WP)
External(XDCE)

External(GBED)

//
// Touch Pins
//
External(GPDI)
External(PPDI)

//
//Vmd Pins
//
External(VMDE)

//
//sata
//

External(SATP)
External(STPP)

Include ("Rtd3Common.asl")
#define WWAN_PCIE_ROOT_PORT \_SB.PC00.RP06
External(WWAN_PCIE_ROOT_PORT.PXSX, DeviceObj)
External(WWAN_PCIE_ROOT_PORT.LASX)
External(\_SB.PC00.RP09.PXSX,DeviceObj)
External(\_SB.PC00.PEG1.PEGP,DeviceObj)
External(\_SB.PC00.PEG2.PEGP,DeviceObj)
External(\_SB.PC00.RP05.PXSX.WIST,MethodObj)
External(\_SB.PC00.XHCI.RHUB.HS10, DeviceObj)
External(\_SB.GBTR, MethodObj)
External(\_SB.BTRK, MethodObj)
External(\_SB.PC00.UA02.BTH0, DeviceObj)

External(\_SB.PC00.I2C0, DeviceObj) //I2C0 Controller
External(\_SB.PC00.I2C0.TPD0, DeviceObj) // Touch pad
External(\_SB.PC00.I2C0.TPD0._STA, MethodObj)

External(\_SB.PC00.I2C1, DeviceObj) //I2C1 Controller
External(\_SB.PC00.I2C1.TPL1._STA, MethodObj)

External(\_SB.PC00.HDAS, DeviceObj)
External(\_SB.PC00.HDAS.VDID)

//Board configuration
//PCIe P7 -x1 SD controller -SRC CLK-6
//PCIe P9:P12 -PCIe X4 Nvme / x1 M.2 SATA/UFS/Sata direct -SRC CLK-1
//PCIe5/PEG1 P0:P7- X8 DG/DG2 -SRC CLK-3
//PCIe4/PEG2 P4:P7- X4 SSD2 -SRC CLK-4

// PCIe root ports - START
    ///
    /// PCIE RTD3 - PCIE SLOT - X1 CONNECTOR // - SD card controller
    ///
    //PSWP, 32,     // [PcieSlot1WakeGpio                  ] Pcie Slot Wake Gpio pin
    //PSPR, 32,     // [PcieSlot1RstGpio                   ] Pcie Slot 1 Rest Gpio pin
    //PPSR, 8,      // [PcieSlot1RstGpioPolarity           ] Pcie Slot 1 Rest Gpio pin polarity
    Scope(\_SB.PC00.RP07) {
      Name(RSTG, Package() {0, 0})
      Store(PSPR, Index(RSTG, 0))
      Store(PPSR, Index(RSTG, 1))
      Name(PWRG, Package() {0, 0})
      Name(WAKG, 0)
      Store(PSWP, WAKG)
      Name(SCLK, 6)
      Name(WAKP, 0)             // must be defined due to compiler bug, will be removed when fixed https://bugs.acpica.org/show_bug.cgi?id=1432
      Include("PcieRpGenericPcieDeviceRtd3.asl")
    }

    ///
    /// PCIe RP09 RTD3 - PCH M.2 SSD RTD3
    ///
    //  SSDP, 32,     // [PchM2SsdPowerEnableGpio           ] Pch M.2 SSD Power Enable Gpio pin
    //  SDPP, 8,      // [PchM2SsdPowerEnableGpioPolarity   ] Pch M.2 SSD Power Enable Gpio pin polarity
    //  SSDR, 32,     // [PchM2SsdRstGpio                   ] Pch M.2 SSD Reset Gpio pin
    //  SDRP, 8,      // [PchM2SsdRstGpioPolarity           ] Pch M.2 SSD Reset Gpio pin polarity
    Scope(\_SB.PC00.RP09) {
      Name(RSTG, Package() {0, 0})
      Store(SSDR, Index(RSTG, 0))
      Store(SDRP, Index(RSTG, 1))
      Name(PWRG, Package() {0, 0})
      Store(SSDP, Index(PWRG, 0))
      Store(SDPP, Index(PWRG, 1))
      Name(WAKG, 0)              // must be defined due to compiler bug, will be removed when fixed https://bugs.acpica.org/show_bug.cgi?id=1432
      Name(WAKP, 0)              // must be defined due to compiler bug, will be removed when fixed https://bugs.acpica.org/show_bug.cgi?id=1432
      Name(SCLK, 1)
      Include("PcieRpGenericPcieDeviceRtd3.asl")
      Scope(\_SB.PC00.RP09.PXSX) {
        Include("PcieRpSsdStorageRtd3Hook.asl")
      }
    }



    ///
    /// CPU M.2 SSD RTD3
    ///
    //  SD5P, 32,     // [M2Ssd3PowerEnableGpio             ] PCIe x4 M.2 SSD Power Enable Gpio pin
    //  SDP5, 8,      // [M2Ssd3PowerEnableGpioPolarity     ] PCIe x4 M.2 SSD Power Enable Gpio pin polarity
    //  SD5R, 32,     // [M2Ssd3RstGpio                     ] PCIe x4 M.2 SSD Reset Gpio pin
    //  SDR5, 8,      // [M2Ssd3RstGpioPolarity             ] PCIe x4 M.2 SSD Reset Gpio pin polarity
    Scope(\_SB.PC00.PEG2) {
      Name(RSTG, Package() {0, 0})
      Name(PWRG, Package() {0, 0})
      Store(SD5R, Index(RSTG, 0))
      Store(SDR5, Index(RSTG, 1))
      Store(SD5P, Index(PWRG, 0))
      Store(SDP5, Index(PWRG, 1))
      Name(WAKG, 0)
      Store(0, WAKG)
      Name(WAKP, 0)             // must be defined due to compiler bug, will be removed when fixed https://bugs.acpica.org/show_bug.cgi?id=1432
      Name(SCLK, 4)
      Include("PcieRpGenericPcieDeviceRtd3.asl")
      Scope(\_SB.PC00.PEG2.PEGP) {
        Include("PcieRpSsdStorageRtd3Hook.asl")
      }
    }

    ///
    /// PEG PCIE RTD3 - PEG PCIE SLOT 3 - X8 CONNECTOR
    ///
    //  DG2P, 32,     // [M2DG2PowerEnableGpio             ] PCIe x5 M.2 Discrete Graphics Power Enable Gpio Pin
    //  DGP2, 8,      // [M2DG2PowerEnableGpioPolarity     ] PCIe x5 M.2 Discrete Graphics Power Enable Gpio Pin polarity
    //  DG2R, 32,     // [M2DG2RstGpio                     ] PCIe x5 M.2 Discrete Graphics Reset Gpio Pin
    //  DGR2, 8,      // [M2DG2RstGpioPolarity             ] PCIe x5 M.2 Discrete Graphics Reset Gpio Pin Polarity
    Scope(\_SB.PC00.PEG1) {
      Name(RSTG, Package() {0, 0})
      Name(PWRG, Package() {0, 0})
      Name(WAKP, 0)             // must be defined due to compiler bug, will be removed when fixed https://bugs.acpica.org/show_bug.cgi?id=1432
      Name(SCLK, 3)
      Store(DG2R, Index(RSTG, 0))
      Store(DGR2, Index(RSTG, 1))
      Store(DG2P, Index(PWRG, 0))
      Store(DGP2, Index(PWRG, 1))
      Name(WAKG, 0)
      Store(P1WP, WAKG)
      // Note: IPC1 Command Timeout need to increase for dGPU only.
      Name (TMCS, 3000) // IPC Command Timeout Increase to 3 Secs.
      If(LNotEqual (DGBA, 0)) {
        Include("PcieRpDiscreteGraphicsDeviceRtd3Hook.asl")
      }
      Scope(\_SB.PC00.PEG1.PEGP) {
        Include("PcieRpSsdStorageRtd3Hook.asl")
      }
      Include("PcieRpGenericPcieDeviceRtd3.asl")
    }

// PCIe root ports - END

//
// VMD - START
//
#if (FixedPcdGetBool (PcdVmdEnable) == 1)
  Include ("Rtd3Vmd.asl")
#endif
//
// VMD - END
//

//
// USB - START
//

//
// XDCI - start
//
  If(LEqual(XDCE,1)) {
    Scope(\_SB)
    {
      //
      // Dummy power resource for USB D3 cold support
      //
      PowerResource(USBC, 0, 0)
      {
        Method(_STA) { Return (0xF) }
        Method(_ON) {}
        Method(_OFF) {}
      }
    }

    Scope(\_SB.PC00.XDCI)
    {
      OperationRegion (GENR, SystemMemory, Add(And(XDCB, 0xFFFFFFFFFFFFFF00), 0x10F81C), 0x4)  //AON MMIO - 10F81C: APBFC_U3PMU_CFG5
      Field (GENR, WordAcc, NoLock, Preserve)
      {
            ,   2,
        CPME,   1,    //bit2 core_pme_en
        U3EN,   1,    //bit3 u3_pme_en
        U2EN,   1     //bit4 u2_pme_en
      }

      Method (_PS3, 0, NotSerialized)
      {
        Store (One, CPME)
        Store (One, U2EN)
        Store (One, U3EN)

        \_SB.CSD3(MODPHY_SPD_GATING_XDCI)
      }
      Method (_PS0, 0, NotSerialized)
      {
        Store (Zero, CPME)
        Store (Zero, U2EN)
        Store (Zero, U3EN)

        If(LEqual(DVID,0xFFFF)) {
          Return()
        }

        \_SB.CSD0(MODPHY_SPD_GATING_XDCI)
      }

      Method (_RMV, 0, NotSerialized)  // _RMV: Removal Status
      {
        Return (Zero)
      }

      Method (_PR3, 0, NotSerialized)  // _PR3: Power Resources for D3hot
      {
        Return (Package (0x01)
        {
          USBC // return dummy package
        })
      }
    } // Scope(\_SB.PC00.XDCI)
  } // XDCE
//
// XDCI - end
//

//
// USB - END
//

//
// Human Interface Devices Start
//

//

//
// PCH I2C0 - TouchPad Power control
//
    Scope(\_SB.PC00.I2C0){

      Method(PS0X,0,Serialized)
      {
      }
      /// PS3X Method, called by PS3 method in PchSerialIo.asl
      Method(PS3X,0,Serialized)
      {
      }
      /// \ref i2c0_pr_pxtc
      ///@defgroup i2c0_scope  I2C1 Scope
      If (LNotEqual(TPDT,0)) {
        PowerResource(PXTC, 0, 0){
          Method(_STA){
            Return(PSTA(0))
          }

          Method(_ON){
            PON(0)
          }

          Method(_OFF){
            POFF(0)
          }
        } // End PXTC
      }
      /// Variable:
      Name(ONTM, 0) ///ONTM: 0 - Not in Speculative ON ; Non-Zero - elapsed time in Nanosecs after Physical ON
      Method(PSTA, 1, Serialized){
        If (Arg0 == 0) {
          Return(0x01)
        }
        Return(0x00)
      }
      Method(PON, 1, Serialized) /// _ON Method \n Turn on
      {
        If (Arg0 == 0) {
          // enable int line
          \_SB.SGRA(GPDI, PPDI)
        }
      }

      Method(POFF, 1, Serialized)  /// _OFF method \n Turn off
      {
        If (Arg0 == 0) {
          // disable int line
          Xor(PPDI, 1, Local0)
          \_SB.SGRA(GPDI, Local0)
        }
      }
      If (LNotEqual(TPDT,0)) {
        Scope(TPD0){
          Name (TD_P, Package(){\_SB.PC00.I2C0.PXTC})               // TD_P - Touch Device Power Package

          Alias(IC0D, TD_D)                                         // TD_D - Touch Device power on delay
          Alias(\_SB.PC00.I2C0.ONTM, TD_C)                          // TD_C - Touch Device I2C controller power on timestamp

          Include("Rtd3I2cTouchDev.asl")
          Method(_PS0) { PS0X() }
          Method(_PS3) { PS3X() }
        }// End Of Scope(TPD0)
       }
    }//  Scope(\_SB.PC00.I2C0)

//
    //Power Resource for Audio Codec
    Scope(\_SB.PC00)
    {
      PowerResource(PAUD, 0, 0) {
        /// Namespace variable used:
        Name(PSTA, One) /// PSTA: Physical Power Status of Codec 0 - OFF; 1-ON
        Name(ONTM, Zero) /// ONTM: 0 - Not in Speculative ON ; Non-Zero - elapsed time in Nanosecs after Physical ON

        Name(_STA, One) /// _STA: PowerResource Logical Status 0 - OFF; 1-ON

        ///@defgroup pr_paud Power Resource for onboard Audio CODEC

        Method(_ON, 0){     /// _ON method \n
          Store(One, _STA)        ///- Set Logocal power state
          PUAM() ///- Call PUAM() to tansition Physical state to match current logical state
                    ///@addtogroup pr_paud
        } // End _ON

        Method(_OFF, 0){    /// _OFF method \n
          Store(Zero, _STA)    ///- Set the current power state
          PUAM() ///- Call PUAM() to tansition Physical state to match current logical state
        ///@addtogroup pr_paud
        } // End _OFF

        ///  PUAM - Power Resource User Absent Mode for onboard Audio CODEC
        ///  Arguments:
        ///
        ///  Uses:
        ///      _STA - Variable updated by Power Resource _ON/_OFF methods \n
        ///      \\UAMS - Variable updated by GUAM method to show User absent present \n
        ///      ONTM - Local variable to store ON time during Speculative ON \n
        /// ______________________________
        // |  Inputs      |   Outputs    |
        // ______________________________
        // | _STA | \UAMS | GPIO | ONTM |
        // ______________________________
        // |   1  |   0   | ON   |   !0 |
        // |   1  |   !0  | ON   |   !0 |
        // |   0  |   0   | ON   |   !0 |
        // |   0  |   !0  | OFF  |   0  |
        // ______________________________
                    /**
                    <table>
                    <tr> <th colspan="2"> Inputs <th colspan="2"> Output
                    <tr> <th>_STA <th> \\UAMS <th> GPIO <th>ONTM
                    <tr> <td>1 <td>0 <td>ON <td>!0
                    <tr> <td>1 <td>!0<td>ON <td>!0
                    <tr> <td>0 <td>0 <td>ON <td>!0
                    <tr> <td>0 <td>!0<td>OFF<td> 0
                    </table>
                    **/
        ///@addtogroup pr_paud_puam
        Method(PUAM, 0, Serialized)
        {
                // power rail = NOT there for ICL U
                // Note:- Audio Power enable need not be implemented by default and need rework if we need power control.
          If (LAnd(LEqual(^_STA, Zero), LNotEqual(\UAMS, Zero))) { ///New state = OFF Check if (_STA ==0 && \UAMS != 0) \n
          } Else { /// New state = ON (_STA=1) or (_STA=0 and \UAMS=0)
            /// Turn power on \n
            If(LNotEqual(^PSTA, One)) { ///- Skip below if Power Resource is already in ON
              Store(One, ^PSTA)  ///- >> Set PSTA to 1
              Store(Timer(), ^ONTM) ///- >> Start the timer for this PR
            }
          }
        ///@defgroup pr_paud_puam Power Resource User Absent Mode for onboard Audio CODEC
        } //PUAM
      } //PAUD
    } //Scope(\_SB.PC00)

//
// Check HDAS (HD-Audio) controller present
//
    If(LNotEqual(\_SB.PC00.HDAS.VDID, 0xFFFFFFFF)) {
      Scope(\_SB.PC00.HDAS) {
        Method(PS0X,0,Serialized)     /// Platform D0 Method for HD-A Controller
        {
          If(LEqual(\_SB.PC00.PAUD.ONTM, Zero)){    ///- Check if ONTM=0
            Return()
          }

          ///
          ///- Make sure "D0 delay" (AUDD) delay is elapsed before returning _PS0
          ///- Local0: Elapse time since the _ON method
          ///- VRRD: VR Rampup Delay
          ///- AUDD: Time required for device to be ready after power on
          ///- Local1 = AUDD + VRRD: Need to incorporate VRRD since the _ON method no longer has VR Rampup Delay
          ///- So only need sleep for (Local1 - Local0), the amount of time remaining since the _ON method
          ///
          Divide(Subtract(Timer(), \_SB.PC00.PAUD.ONTM), 10000, , Local0) ///- Store Elapsed time in ms, ignore remainder
          Add(AUDD, VRRD, Local1) ///- Incorporate VR Rampup Delay
          If(LLess(Local0, Local1)) { ///- Do not sleep if already past the delay requirement audio
            ///- Delay for device init
            Sleep(Subtract(Local1, Local0)) ///- Sleep (AUDD + VRRD - time elapsed)
          }
        }

        ///Associate _PR0 with \ref pr_paud
        Name(_PR0, Package(){\_SB.PC00.PAUD})
      ///@defgroup hdef_scope       Intel High Definition Audio Scope
      }
    }// If(LNotEqual(\_SB.PC00.HDAS.VDID, 0xFFFFFFFF))
//GPE Event handling - Start
  Scope(\_GPE) {
      Method(_L14) {
        If (\_SB.ISME(P1WP))
        {
          \_SB.SHPO(P1WP, 1) // set gpio ownership to driver(0=ACPI mode, 1=GPIO mode)
          If (CondRefOf(\_SB.PC00.PEG1)) {
            If(LNotEqual(\_SB.PC00.PEG1.VDID,0xFFFFFFFF)) { // If Device present in X8 slot
              Notify(\_SB.PC00.PEG1, 0x02)   // device wake
            }
          }
          \_SB.CAGS(P1WP)
        }
      }
    //
    // Alternate _L6F(), to handle 2-tier RTD3 GPE events here
    //
    Method(AL6F) {
      //Pcie x4 slot wake event
      If (\_SB.ISME(PSWP))
      {
        \_SB.SHPO(PSWP, 1) // set gpio ownership to driver(0=ACPI mode, 1=GPIO mode)
        Notify(\_SB.PC00.RP07, 0x02)     // device wake
        \_SB.CAGS(PSWP)
      }
    }
  } //Scope(\_GPE)
//GPE Event handling - End
} // End SSDT
