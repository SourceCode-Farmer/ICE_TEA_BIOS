/**@file
  VMD device description

 @copyright
  INTEL CONFIDENTIAL
  Copyright 2019 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains an 'Intel Peripheral Driver' and is uniquely identified as
  "Intel Reference Module" and is licensed for Intel CPUs and chipsets under
  the terms of your license agreement with Intel or your vendor. This file may
  be modified by the user, subject to additional terms of the license agreement.

@par Specification Reference:
**/

External(\_SB.PC00.VMD0, DeviceObj)

If(LAnd(CondRefOf(VMDE),CondRefOf(\_SB.PC00.VMD0))) {
  Scope(\_SB.PC00)
  {
    If(LEqual(VMDE,1)) {
      Scope(VMD0) {
        External(RP01, DeviceObj)
        External(RP02, DeviceObj)
        External(RP03, DeviceObj)
        External(RP04, DeviceObj)
        External(RP05, DeviceObj)
        External(RP06, DeviceObj)
        External(RP07, DeviceObj)
        External(RP08, DeviceObj)
        External(RP09, DeviceObj)
        External(RP10, DeviceObj)
        External(RP11, DeviceObj)
        External(RP12, DeviceObj)
        External(RP13, DeviceObj)
        External(RP14, DeviceObj)
        External(RP15, DeviceObj)
        External(RP16, DeviceObj)
        External(RP17, DeviceObj)
        External(RP18, DeviceObj)
        External(RP19, DeviceObj)
        External(RP20, DeviceObj)
        External(RP21, DeviceObj)
        External(RP22, DeviceObj)
        External(RP23, DeviceObj)
        External(RP24, DeviceObj)
        External(RP25, DeviceObj)
        External(RP26, DeviceObj)
        External(RP27, DeviceObj)
        External(RP28, DeviceObj)

        External(PEG0, DeviceObj)
        External(PEG1, DeviceObj)
        External(PEG2, DeviceObj)
        External(PEG3, DeviceObj)

        External(PRT0, DeviceObj)
        External(PRT1, DeviceObj)
        External(PRT2, DeviceObj)
        External(PRT3, DeviceObj)
        External(PRT4, DeviceObj)
        External(PRT5, DeviceObj)
        External(PRT6, DeviceObj)
        External(PRT7, DeviceObj)

        External(VMR1)
        External(VMR2)
        External(VMR3)
        External(VMR4)
        External(VMCP)
        External(VMS0)

        External(HBSL)

        If(LAnd(CondRefOf(VMR1),CondRefOf(HBSL))) {
          If(LAnd((VMR1 & 0x01),LNot((HBSL & 0x01)))) {
            Scope(RP01) {
              Include("Rtd3VmdPciePort.asl")
            }
          }

          If(LAnd((VMR1 & 0x02),LNot((HBSL & 0x01)))) {
            Scope(RP02) {
              Include("Rtd3VmdPciePort.asl")
            }
          }

          If(LAnd((VMR1 & 0x04),LNot((HBSL & 0x01)))) {
            Scope(RP03) {
              Include("Rtd3VmdPciePort.asl")
            }
          }

          If(LAnd((VMR1 & 0x08),LNot((HBSL & 0x01)))) {
            Scope(RP04) {
              Include("Rtd3VmdPciePort.asl")
            }
          }

          If(LAnd((VMR1 & 0x10),LNot((HBSL & 0x02)))) {
            Scope(RP05) {
              Include("Rtd3VmdPciePort.asl")
            }
          }

          If(LAnd((VMR1 & 0x20),LNot((HBSL & 0x02))))  {
            Scope(RP06) {
              Include("Rtd3VmdPciePort.asl")
            }
          }

          If(LAnd((VMR1 & 0x40),LNot((HBSL & 0x02))))  {
            Scope(RP07) {
              Include("Rtd3VmdPciePort.asl")
            }
          }

          If(LAnd((VMR1 & 0x80),LNot((HBSL & 0x02))))  {
            Scope(RP08) {
              Include("Rtd3VmdPciePort.asl")
            }
          }
        }

        If(LAnd(CondRefOf(VMR2),CondRefOf(HBSL))) {
          If(LAnd((VMR2 & 0x01),LNot((HBSL & 0x04)))) {
            Scope(RP09) {
              Include("Rtd3VmdPciePort.asl")
            }
          }

          If(LAnd((VMR2 & 0x02),LNot((HBSL & 0x04)))) {
            Scope(RP10) {
              Include("Rtd3VmdPciePort.asl")
            }
          }

          If(LAnd((VMR2 & 0x04),LNot((HBSL & 0x04)))) {
            Scope(RP11) {
              Include("Rtd3VmdPciePort.asl")
            }
          }

          If(LAnd((VMR2 & 0x08),LNot((HBSL & 0x04)))) {
            Scope(RP12) {
              Include("Rtd3VmdPciePort.asl")
            }
          }

          If(LAnd((VMR2 & 0x10),LNot((HBSL & 0x08)))) {
            Scope(RP13) {
              Include("Rtd3VmdPciePort.asl")
            }
          }

          If(LAnd((VMR2 & 0x20),LNot((HBSL & 0x08)))) {
            Scope(RP14) {
              Include("Rtd3VmdPciePort.asl")
            }
          }

          If(LAnd((VMR2 & 0x40),LNot((HBSL & 0x08)))) {
            Scope(RP15) {
              Include("Rtd3VmdPciePort.asl")
            }
          }

          If(LAnd((VMR2 & 0x80),LNot((HBSL & 0x08)))) {
            Scope(RP16) {
              Include("Rtd3VmdPciePort.asl")
            }
          }
        }

        If(LAnd(CondRefOf(VMR3),CondRefOf(HBSL))) {
          If(LAnd((VMR3 & 0x01),LNot((HBSL & 0x10)))) {
            Scope(RP17) {
              Include("Rtd3VmdPciePort.asl")
            }
          }

          If(LAnd((VMR3 & 0x02),LNot((HBSL & 0x10)))) {
            Scope(RP18) {
              Include("Rtd3VmdPciePort.asl")
            }
          }

          If(LAnd((VMR3 & 0x04),LNot((HBSL & 0x10)))) {
            Scope(RP19) {
              Include("Rtd3VmdPciePort.asl")
            }
          }

          If(LAnd((VMR3 & 0x08),LNot((HBSL & 0x10)))) {
            Scope(RP20) {
              Include("Rtd3VmdPciePort.asl")
            }
          }

          If(LAnd((VMR3 & 0x10),LNot((HBSL & 0x20)))) {
            Scope(RP21) {
              Include("Rtd3VmdPciePort.asl")
            }
          }

          If(LAnd((VMR3 & 0x20),LNot((HBSL & 0x20)))) {
            Scope(RP22) {
              Include("Rtd3VmdPciePort.asl")
            }
          }

          If(LAnd((VMR3 & 0x40),LNot((HBSL & 0x20)))) {
            Scope(RP23) {
              Include("Rtd3VmdPciePort.asl")
            }
          }

          If(LAnd((VMR3 & 0x80),LNot((HBSL & 0x20)))){
            Scope(RP24) {
              Include("Rtd3VmdPciePort.asl")
            }
          }
        }

        If(LAnd(CondRefOf(VMR4),CondRefOf(HBSL))) {
          If(LAnd((VMR4 & 0x01),LNot((HBSL & 0x40)))) {
            Scope(RP25) {
              Include("Rtd3VmdPciePort.asl")
            }
          }

          If(LAnd((VMR4 & 0x02),LNot((HBSL & 0x40)))) {
            Scope(RP26) {
              Include("Rtd3VmdPciePort.asl")
            }
          }

          If(LAnd((VMR4 & 0x04),LNot((HBSL & 0x40)))) {
            Scope(RP27) {
              Include("Rtd3VmdPciePort.asl")
            }
          }

          If(LAnd((VMR4 & 0x08),LNot((HBSL & 0x40)))) {
            Scope(RP28) {
              Include("Rtd3VmdPciePort.asl")
            }
          }
        }

        If(CondRefOf(VMCP)) {
        // P.E.G. Root Port D1F0
          If(VMCP & 0x1) {
            Scope(PEG1) {
              Include("Rtd3VmdPciePort.asl")
            }
          }
          // P.E.G. Root Port D1F1
          If(LOr((VMCP & 0x2), (VMCP & 0x10))) {
            Scope(PEG2) {
              Include("Rtd3VmdPciePort.asl")
            }
          }
          // P.E.G. Root Port D1F2
          If(VMCP & 0x4) {
            Scope(PEG3) {
              Include("Rtd3VmdPciePort.asl")
            }
          }
          // P.E.G. Root Port D6F0
          If(VMCP & 0x8) {
            Scope(PEG0) {
              Include("Rtd3VmdPciePort.asl")
            }
          }
        }

        If(CondRefOf(VMS0)) {
          // SATA Controller 0 ATA Port 0
          If(VMS0 & 0x01) {
            Scope(PRT0) {
              Include("Rtd3VmdSataPort.asl")
            }
          }
          // SATA Controller 0 ATA Port 1
          If(VMS0 & 0x02) {
            Scope(PRT1) {
              Include("Rtd3VmdSataPort.asl")
            }
          }
          // SATA Controller 0 ATA Port 2
          If(VMS0 & 0x04) {
            Scope(PRT2) {
              Include("Rtd3VmdSataPort.asl")
            }
          }
          // SATA Controller 0 ATA Port 3
          If(VMS0 & 0x08) {
            Scope(PRT3) {
              Include("Rtd3VmdSataPort.asl")
            }
          }
          // SATA Controller 0 ATA Port 4
          If(VMS0 & 0x10) {
            Scope(PRT4) {
              Include("Rtd3VmdSataPort.asl")
            }
          }
          // SATA Controller 0 ATA Port 5
          If(VMS0 & 0x20) {
            Scope(PRT5) {
              Include("Rtd3VmdSataPort.asl")
            }
          }
          // SATA Controller 0 ATA Port 6
          If(VMS0 & 0x40) {
            Scope(PRT6) {
              Include("Rtd3VmdSataPort.asl")
            }
          }
          // SATA Controller 0 ATA Port 7
          If(VMS0 & 0x80) {
            Scope(PRT7) {
              Include("Rtd3VmdSataPort.asl")
            }
          }
        }
      }
    }
  }
}
