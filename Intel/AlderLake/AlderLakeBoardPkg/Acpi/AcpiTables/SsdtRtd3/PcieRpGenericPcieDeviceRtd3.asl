/** @file

;******************************************************************************
;* Copyright (c) 2021, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/
/** @file
  PCIe Root Port Generic PCIE Device Rtd3 file.

@copyright
  INTEL CONFIDENTIAL
  Copyright 2020 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains a 'Sample Driver' and is licensed as such under the terms
  of your license agreement with Intel or your vendor. This file may be modified
  by the user, subject to the additional terms of the license agreement.

@par Specification Reference:
**/

/// @details
/// Code in this file uses following variables:
/// SCLK: ICC Clock number - optional
/// WAKG: WAKE GPIO pad - optional
/// Below objects should be defined according to the format described in PinDriverLib.asl
/// RSTG: reset pin definition - mandatory
/// PWRG: power GPIO pad - optional
/// WAKP: Flag to indicate that power gating must not be performed if WAKE is enabled - optional
/** @defgroup pcie_scope PCIe Root Port Scope **/

  //
  // AcpiPinDriverLib imports(from DSDT in platform)
  //
  External (\PIN.STA, MethodObj)
  External (\PIN.ON, MethodObj)
  External (\PIN.OFF, MethodObj)
  //
  // GpioLib imports(DSDT)
  //
  External (\_SB.SHPO, MethodObj)

  //
  // HSIO lib imports
  //
  External (\_SB.PSD0, MethodObj)
  External (\_SB.PSD3, MethodObj)
  External (DVES, MethodObj)
  External (DHDW, MethodObj)
  External (EHDW, MethodObj)
  External (D3HT, FieldUnitObj) // CPU PCIE RP Power State
  External (PCPB, IntObj) // PCIe core power veto bitmask, default 0 - allow for core power removal
  External (HBSL, FieldUnitObj)
  External (\HBCL, FieldUnitObj)
  External (\HBPL, FieldUnitObj)
  External (TMCS, IntObj)
  External (CEMP, MethodObj)
//[-start-210816-IB05660174-add]//
  External (\DGPV)
#if FeaturePcdGet (PcdHybridGraphicsSupported)
  #define PCI_SCOPE             \_SB.PC00
//[-start-210917-GEORGE0004-modify]//
//[-start-210918-QINGLIN0068-modify]//
//#ifdef S77014_SUPPORT
#if defined(S77014_SUPPORT) || defined(S570_SUPPORT) || defined(S77014IAH_SUPPORT)
//[-end-210918-QINGLIN0068-modify]//
  #define DGPU_BRIDGE_SCOPE     PCI_SCOPE.PEG2
#else
  #define DGPU_BRIDGE_SCOPE     PCI_SCOPE.PEG1
#endif
//[-end-210917-GEORGE0004-modify]//
  #define DGPU_DEVICE           PEGP
  #define DGPU_SCOPE            DGPU_BRIDGE_SCOPE.DGPU_DEVICE
  #define NVIDIA_VID            0x10DE
  #define NV_GEN_17             17
  #define NV_GEN_20             20

  External (DGPU_BRIDGE_SCOPE.CEDR, FieldUnitObj)
  External (DGPU_BRIDGE_SCOPE.CMDR, FieldUnitObj)
  External (DGPU_BRIDGE_SCOPE.D0ST, FieldUnitObj)
  External (DGPU_BRIDGE_SCOPE.GSTA, MethodObj)
  External (DGPU_BRIDGE_SCOPE.LREN, FieldUnitObj)
  External (DGPU_SCOPE.HDAE, FieldUnitObj)
  External (DGPU_SCOPE.LTRE, IntObj)
  External (DGPU_SCOPE.NVID, FieldUnitObj)
  External (DGPU_SCOPE.PWGD, FieldUnitObj)
  External (DGPU_SCOPE.SSSV, FieldUnitObj)
  External (PCI_SCOPE.DGCX, IntObj)
  External (PCI_SCOPE.DSSV, FieldUnitObj)
  External (PCI_SCOPE.NVGE, FieldUnitObj)
  External (PCI_SCOPE.GC6I, MethodObj)
  External (PCI_SCOPE.GC6O, MethodObj)
  External (PCI_SCOPE.NINI, MethodObj)
  External (PCI_SCOPE.OPTF, FieldUnitObj)
  External (PCI_SCOPE.TDGC, IntObj)
  External (PCI_SCOPE.TGPC, BuffObj)
  //External (\DLHR)
  //External (\DLPW)
#endif
//[-end-210816-IB05660174-add]//
//[-start-210915-QINGLIN0068-add]//
#if defined(C770_SUPPORT) || defined(S370_SUPPORT) || defined(S570_SUPPORT) || defined(S77014_SUPPORT) || defined(S77014IAH_SUPPORT)
  External (\_SB.PC00.LPCB.EC0.ECAV)
  External (\_SB.PC00.LPCB.EC0.LfcM)
  External (\_SB.PC00.LPCB.EC0.GATY)
#endif
//[-end-210915-QINGLIN0068-add]//

  // WAKE enable on PCIe device.
  Name (WKEN, 0)

  // Last OFF Timestamp (WOFF): The time stamp of the last power resource _OFF method evaluation
  Name (WOFF, 0)

  Name (LNRD, 0) // Delay before PERST# assertion in us

  //
  // The deepest D-state supported by this device in the S0 system sleeping state where the device can wake itself,
  // "4" represents D3cold.
  //
  Method (_S0W, 0x0, NotSerialized)
  {

    If (CondRefOf (RD3C)) {
      If (LEqual (RD3C, 0x02)) {
        Return (0x4)
      }
    }

//[-start-210816-IB05660174-modify]//
//[-start-211125-IB05660187-modify]//
#if FeaturePcdGet (PcdHybridGraphicsSupported)
    Return (0x4)
#else
    Return (0x0)
#endif
//[-end-211125-IB05660187-modify]//
//[-end-210816-IB05660174-modify]//
  }

  //
  // Device Sleep Wake - sets the sleep and wake transition states for a device.
  // ACPI Specification Defined Method. Called by OSPM.
  //
  Method (_DSW, 3, NotSerialized)
  {
    /// This method is used to enable/disable wake from PCIe (WKEN)

    If (Arg1)
    { /// If entering Sx, enable Sx WAKE
      Store (1, WKEN)
    } Else {  /// If Staying in S0
      If (LAnd(Arg0, Arg2)) ///- Check if Exiting D0 and arming for wake
      { ///- Set PME
        Store (1, WKEN)
      } Else { ///- Disable runtime PME, either because staying in D0 or disabling wake
        Store (0, WKEN)
      }
    }
  /** @defgroup pcie_dsw PCIE _DSW **/
  } // End _DSW

  Method (PPS0, 0, Serialized) { // Platform specific PCIe root port _PS0 Hook Function.
  }

  Method (PPS3, 0, Serialized) { // Platform specific PCIe root port _PS3 Hook Function.
  }

  //
  // PCIe slot power resource definition
  //
  PowerResource (PXP, 0, 0) {

    Method (_STA, 0) {

      //
      // Check if PCIE RP is available or Not by Checking Vendor and Device ID.
      //
      If (LEqual (VDID, 0xFFFFFFFF)) {
        Return (0)
      }

      //
      // Check if PCIE RP Power Resource is supported or Not.
      //
      If (LEqual (GPRS (), 0)) {
        Return (0)
      }

#if FeaturePcdGet (PcdHybridGraphicsSupported)
//[-start-211103-IB05660186-modify]//
      If (LEqual (DGPV, NVIDIA_VID)) {
        If (LEqual (PCI_SCOPE.NVGE, NV_GEN_20)) {
          Return (DGPU_BRIDGE_SCOPE.GSTA ())
        }
      }
//[-end-211103-IB05660186-modify]//
#endif

      //
      // Return PCIE RP Power Resource Status.
      //
      Return (PSTA ())
    }

    Method (_ON) {

      //
      // Check if PCIE RP is available or Not by Checking Vendor and Device ID.
      //
      If (LEqual (VDID, 0xFFFFFFFF)) {
        Return ()
      }

      //
      // Check if PCIE RP Power Resource is supported or Not.
      //
      If (LEqual (GPRS (), 0)) {
        Return ()
      }

//[-start-210816-IB05660174-add]//
#if FeaturePcdGet (PcdHybridGraphicsSupported)
      If (LEqual (DGPV, NVIDIA_VID)) {
        If (LEqual (PCI_SCOPE.NVGE, NV_GEN_20)) {
          If (LNotEqual(DGPU_BRIDGE_SCOPE.GSTA(), Zero)) { //check the dGPU OFF?
            Return ()
          }
        }
        If (LEqual (PCI_SCOPE.TDGC, One)) {
          If (LEqual (PCI_SCOPE.DGCX, 0x03)) {
            PCI_SCOPE.GC6O ()
          } ElseIf (LEqual (PCI_SCOPE.DGCX, 0x04)) {
            PCI_SCOPE.GC6O ()
          }
          // Clear Defer flag
          Store (Zero, PCI_SCOPE.TDGC)
          Store (Zero, PCI_SCOPE.DGCX)
        } Else {
          //
          // Turn on slot power
          //
          PON ()

          //
          // Trigger L2/L3 ready exit flow in rootport - transition link to Detect
          //
          L23D ()

          Store(0x07, DGPU_BRIDGE_SCOPE.CMDR)               // Enable root port cmd reg
          Store(Zero, DGPU_BRIDGE_SCOPE.D0ST)               // Put root port in D0
          While (LNotEqual (DGPU_SCOPE.NVID, NVIDIA_VID)) { // Wait dGPU present on PCI bus
            Sleep (One)
          }
          Store (DGPU_SCOPE.LTRE, DGPU_BRIDGE_SCOPE.LREN)   // Restore LTR enable bit
          Store (One, DGPU_BRIDGE_SCOPE.CEDR)               // Clear Correctable Error bit
          Store (PCI_SCOPE.DSSV, DGPU_SCOPE.SSSV)           // OEM_customize: fill SVID/SSID in DWORD
        }
//[-start-210917-QINGLIN0068-add]//
//[-start-210925-GEORGE0009-modify]//
#if defined(S570_SUPPORT) || defined(S77014_SUPPORT) || defined(S77014IAH_SUPPORT)
//[-end-210925-GEORGE0009-modify]//
        If (\_SB.PC00.LPCB.EC0.ECAV) {
          If(LEqual(Acquire(\_SB.PC00.LPCB.EC0.LfcM, 0xA000),0x0)) { 
            Store(0x02, \_SB.PC00.LPCB.EC0.GATY)          //enable EC polling reading GPU temprature
            Sleep(10)
            Release(\_SB.PC00.LPCB.EC0.LfcM)
          }
        }
#endif
//[-end-210917-QINGLIN0068-add]//
      } Else {
#endif
//[-end-210816-IB05660174-add]//
      //
      // Turn on slot power
      //
      PON ()

      //
      // Trigger L2/L3 ready exit flow in rootport - transition link to Detect
      //
      L23D ()

//[-start-210915-QINGLIN0068-add]//
#if defined(C770_SUPPORT) || defined(S370_SUPPORT)
      If (CondRefOf (DVES)) { //Only DG2 dGPU root port have this method 
        If(LNotEqual (DGBA, 0)) {
          If (\_SB.PC00.LPCB.EC0.ECAV) {
            If(LEqual(Acquire(\_SB.PC00.LPCB.EC0.LfcM, 0xA000),0x0)) { 
//[start-210916-STORM1114-modify]
//[-start-220120-QINGLIN0149-modify]//
//#if defined(C770_SUPPORT)
              Store(0x03, \_SB.PC00.LPCB.EC0.GATY)          //enable EC polling reading GPU temprature
//#else
//              Store(0x02, \_SB.PC00.LPCB.EC0.GATY)          //enable EC polling reading GPU temprature
//#endif
//[-end-220120-QINGLIN0149-modify]//
//[end-210916-STORM1114-modify]
              Sleep(10)
              Release(\_SB.PC00.LPCB.EC0.LfcM)
            }
          }
        }
      }
#endif
//[-end-210915-QINGLIN0068-add]//
//[-start-210816-IB05660174-add]//
#if FeaturePcdGet (PcdHybridGraphicsSupported)
      }
#endif
//[-end-210816-IB05660174-add]//
    }

    Method (_OFF) {

      //
      // Check if PCIE RP is available or Not by Checking Vendor and Device ID.
      //
      If (LEqual (VDID, 0xFFFFFFFF)) {
        Return ()
      }

      //
      // Check if PCIE RP Power Resource is supported or Not.
      //
      If (LEqual (GPRS (), 0)) {
        Return ()
      }

//[-start-210816-IB05660174-add]//
#if FeaturePcdGet (PcdHybridGraphicsSupported)
      If (LEqual (DGPV, NVIDIA_VID)) {
        If (LEqual (PCI_SCOPE.NVGE, NV_GEN_20)) {
          If (LNotEqual(DGPU_BRIDGE_SCOPE.GSTA(), One)) { //check the dGPU ON?
            Return ()
          }
        }
//[-start-210917-QINGLIN0068-add]//
//[-start-210925-GEORGE0009-modify]//
#if defined(S570_SUPPORT) || defined(S77014_SUPPORT) || defined(S77014IAH_SUPPORT)
//[-end-210925-GEORGE0009-modify]//
        If (\_SB.PC00.LPCB.EC0.ECAV) {
          If(LEqual(Acquire(\_SB.PC00.LPCB.EC0.LfcM, 0xA000),0x0)) { 
            Store(0x00, \_SB.PC00.LPCB.EC0.GATY)          //disable EC polling reading GPU temprature
            Sleep(10)
            Release(\_SB.PC00.LPCB.EC0.LfcM)
          }
        }
#endif
//[-end-210917-QINGLIN0068-add]//
        If (LEqual (PCI_SCOPE.TDGC, One)) {
          CreateField (PCI_SCOPE.TGPC, 0, 3, GPPC) // GPU Power Control
          If (LEqual (ToInteger (GPPC), 0x01)) {
            PCI_SCOPE.GC6I ()
          } ElseIf (LEqual (ToInteger (GPPC), 0x02)) {
            PCI_SCOPE.GC6I ()
          }
        } Else {
          Store (DGPU_BRIDGE_SCOPE.LREN, DGPU_SCOPE.LTRE) // Save LTR enable bit

          //
          // Trigger L2/L3 ready entry flow in rootport
          //
          DL23 ()

          //
          // Turn off slot power
          //
          POFF ()
        }
      } Else {
#endif
//[-end-210816-IB05660174-add]//
//[-start-210915-QINGLIN0068-add]//
#if defined(C770_SUPPORT) || defined(S370_SUPPORT)
      If (CondRefOf (DVES)) { //Only DG2 dGPU root port have this method 
        If(LNotEqual (DGBA, 0)) {
          If (\_SB.PC00.LPCB.EC0.ECAV) {
            If(LEqual(Acquire(\_SB.PC00.LPCB.EC0.LfcM, 0xA000),0x0)) { 
              Store(0, \_SB.PC00.LPCB.EC0.GATY)          //enable EC polling reading GPU temprature
              Sleep(10)
              Release(\_SB.PC00.LPCB.EC0.LfcM)
            }
          }
        }
      }
#endif
//[-end-210915-QINGLIN0068-add]//
      //
      // Trigger L2/L3 ready entry flow in rootport
      //
      DL23 ()

      //
      // Turn off slot power
      //
      POFF ()
//[-start-210816-IB05660174-add]//
#if FeaturePcdGet (PcdHybridGraphicsSupported)
      }
#endif
//[-end-210816-IB05660174-add]//
    }
  } // End PowerResource

  //
  // Get Permission for Power Removal.
  // Check whether or not to Disable Power Package GPIO During Device Power OFF(D3 Cold Transition).
  // Input: VOID
  //
  // @return 1 if it is Safe to Remove/Disable Power. 0 Not allow for Power Removal.
  //
  Method (GPPR, 0) {

    // If WAKP has not been defined we can safely disable power.
    // If WAKP is defined this slot does not supply device with auxilary power and we have to keep primary power
    // to allow for WAKE. If WAKP is not equal to 0 and WKEN has been enabled do not disable the power.
    If (CondRefOf (WAKP)) {
      If (LAnd (LNotEqual (WAKP, 0), LEqual (WKEN, 0))) {
        Return (0)
      }
    }

    //
    // If PCPB has not been defined we can safely disable power.
    // If PCPB is defined and non Zero we have to keep primary power.
    //
    If (CondRefOf (PCPB)) {
      If (LNotEqual (PCPB, 0)) {
        Return (0)
      }
    }

    //
    // If DVES Method has not been defined we can safely disable power.
    // If DVES Method is defined and return Zero value we have to keep primary power.
    //
    If (CondRefOf (DVES)) {
      If (LEqual (DVES (), 0)) {
        Return (0)
      }
    }

    //
    // Now Safe To Remove/Disable Power.
    //
    Return (1)
  }

  //
  // Get PCIe RP Power Resource Support.
  // If D3 Cold is supported by PCIE RP or Not.
  // Input: VOID
  //
  // @return 1 PCIE RP Power Resource Supported. 0 PCIE RP Power Resource Not Supported.
  //
  Method (GPRS, 0) {

    If (LAnd (CondRefOf (PRTP), LEqual (PRTP, PCIE_RP_TYPE_CPU))) {

      //
      // Check for Hybrid Connection Status for PCIE RP and for single CPU M.2 with x2 MUX
      //
      If (CondRefOf (\HBCL)) {
        If (LOr(LNotEqual(\HBCL, 0xFF), LLessEqual(\HBCL, 0x2))) {
          If (LEqual (\HBCL, SLOT)) {
            Return (0)
          }
        }
      }
    }

    //
    // Check for Hybrid Connection Status for PCIE RP and for single CPU M.2 with x2 MUX
    //
    If (LAnd (CondRefOf (PRTP), LEqual (PRTP, PCIE_RP_TYPE_PCH))) {
      If (CondRefOf (HBSL)) {
        Divide ( Subtract (SLOT, 1), 4, , Local0) // Get PCIE RP Controller Number ( (SLOT-1)/4)
        // Check if Hybrid Connection Status BIT Corresponding to Controller Number is SET or NOT.
        If ( And (HBSL, ShiftLeft (BIT0, Local0))) {
          Return (0)
        }
      }

      If (LAnd(CondRefOf (\HBCL), CondRefOf (\HBPL))) {
        If (LOr(LNotEqual(\HBCL, 0xFF), LLessEqual(\HBCL, 0x2))) {
          If (LEqual (\HBPL, Subtract(SLOT, 1))) {
            Return (0)
          }
        }
      }

    }

    //
    // Check if D3 Cold is supported for PCIE RP.
    //
//[start-211104-STORM1118-modify]
//[start-211108-SHAONN0014-modify]
#if defined(C770_SUPPORT) || defined(S370_SUPPORT)
    If (CondRefOf (DVES)) { //Only DG2 dGPU root port have this method 
        // DG2 support D3 Cold
    } Else {
#endif
//[end-211104-STORM1118-modify]
    If (CondRefOf (RD3C)) {
      If (LNotEqual (RD3C, 0x02)) {
        Return (0)
      }
    }
//[start-211104-STORM1118-modify]
#if defined(C770_SUPPORT) || defined(S370_SUPPORT)
    }
#endif
//[end-211108-SHAONN0014-modify]
//[end-211104-STORM1118-modify]
    //
    // Check if PCIE RP is Mapped under VMD. D3 Cold flow will be taken care by VMD and it's Child ACPI Devices.
    //
    If (CondRefOf (PRMV)) {
      If (LEqual (PRMV, 1)) {
        Return (0)
      }
    }

    //
    // D3 Cold is supported for PCIE RP
    //
    Return (1)
  }

  //
  // Returns the status of PCIe slot core power.
  //
  Method (PSTA, 0) {

    //
    // RESET# assertion is mandatory for PCIe RTD3
    // So if RESET# is asserted the whole slot is off
    //
    If (\PIN.STA (RSTG)) {
      Return (0)
    } Else {
      Return (1)
    }
  }

  // Turn on power to PCIe Slot
  // Since this method is also used by the remapped devices to turn on power to the slot
  // this method should not make any access to the PCie config space.
  Method (PON) {

    //Check if the RP is a CEM slot
   If (CondRefOf(CEMP)) {
     CEMP(1)  //set the CEM slot to leave D3Cold
   }
    // Disable WAKE
    If (CondRefOf (WAKG)) {
      \_SB.SHPO (WAKG, 1) // set gpio ownership to driver(0=ACPI mode, 1=GPIO mode)
      \_SB.CAGS (WAKG) // Clear GPIO Status if set.
    }
    If (CondRefOf (DHDW)) { // DG support HPD as D3 Wake GPIO and DHDW exists
      DHDW () // Disable Discrete Graphics HPD SCI implementated As D3 Wake GPIO
    }

    // Restore power to the modPHY (Only for PCH PCIE RP)
    If (LAnd (CondRefOf (PRTP), LEqual (PRTP, PCIE_RP_TYPE_PCH))) {
      \_SB.PSD0 (SLOT)
    }

    // Turn ON Power for PCIe Slot
    If (CondRefOf (PWRG)) {
      // Delay by 10 ms if required using WOFF
      If (CondRefOf (WOFF)) {
        If (LNotEqual (WOFF, Zero)) {
          Divide (Subtract (Timer (), WOFF), 10000, , Local0) // Store Elapsed time in ms, ignore remainder
          If (LLess (Local0, 200)) {                           // If Elapsed time is less than 20ms
            Sleep (Subtract (200, Local0))                     // Sleep for the remaining time
          }
          Store (0, WOFF)
        }
      }
      \PIN.ON (PWRG)
//[-start-210816-IB05660174-add]//
      // HG power sequence tuning: delay after power enable for PCIe by DelayAfterPwrEn of SCU
      // Following delay code needs to be opened by project requirement.
      //Sleep (DLPW)
//[-end-210816-IB05660174-add]//
//[-start-210927-QINGLIN0086-modify]//
#if defined(S570_SUPPORT) 
#if defined(NV_DGPU_PEG_PORT) && (NV_DGPU_PEG_PORT)
      If(CondRefOf(PWGG))
      {
        Store(100, Local0)  //max delay 100ms
        Store(DeRefOf(Index(PWGG, 0)), Local1)   //get current GPI status
        Store(DeRefOf(Index(PWGG, 1)), Local2)  //get active value
        While(LAnd(LNotEqual(\_SB.GGIV(Local1), Local2), Local0))  //wait power good status
        {
          Decrement(Local0)
          Sleep(1)
        }
        If (LNotEqual(Local0, 0)) { //power good OK
          Sleep(3) //sleep 3ms after power good signel OK
        }
      }
      Else
      {
        Sleep (PEP0)
      }
#else
      Sleep (PEP0)
#endif
//[-start-220403-TAMT000047-add]//
#elif defined(S77014IAH_SUPPORT) 
#if defined(NV_DGPU_PEG_PORT) && (NV_DGPU_PEG_PORT)
      If(CondRefOf(PWGG))
      {
        Store(100, Local0)  //max delay 100ms
        Store(DeRefOf(Index(PWGG, 0)), Local1)   //get current GPI status
        Store(DeRefOf(Index(PWGG, 1)), Local2)  //get active value
        While(LAnd(LNotEqual(\_SB.GGIV(Local1), Local2), Local0))  //wait power good status
        {
          Decrement(Local0)
          Sleep(1)
        }
        If (LNotEqual(Local0, 0)) { //power good OK
          Sleep(3) //sleep 3ms after power good signel OK
        }
      }
      Else
      {
        Sleep (PEP0)
      }
#else
      Sleep (PEP0)
#endif
//[-end-220403-TAMT000047-add]//
#else
      Sleep (PEP0)
#endif
//[-end-210927-QINGLIN0086-modify]//
    }

    //
    // On RTD3 Exit, BIOS will instruct the PMC to Enable source clocks.
    // This is done through sending a PMC IPC command if IPC Command is supported.
    //
    If (CondRefOf (SCLK)) {
//[-start-210927-QINGLIN0086-modify]//
#if defined(S570_SUPPORT) && defined(NV_DGPU_PEG_PORT) && (NV_DGPU_PEG_PORT)
      // not need, remove it.
//[-start-220403-TAMT000047-add]//
#elif defined(S77014IAH_SUPPORT) && defined(NV_DGPU_PEG_PORT) && (NV_DGPU_PEG_PORT)
      // not need, remove it.
//[-end-220403-TAMT000047-add]//
#else
      If(CondRefOf(TMCS)) {
//[-start-210927-QINGLIN0086-modify]//
        SPCX(SCLK, 1, TMCS)
      } Else {
        SPCO(SCLK, 1)
      }
#endif
//[-end-210927-QINGLIN0086-modify]//
    }

#if FeaturePcdGet (PcdHybridGraphicsSupported)
//[-start-211112-QINGLIN0114-modify]//
#if defined(S570_SUPPORT) && defined(NV_DGPU_PEG_PORT) && (NV_DGPU_PEG_PORT)
    //Already down
//[-start-220403-TAMT000047-add]//
#elif defined(S77014IAH_SUPPORT) && defined(NV_DGPU_PEG_PORT) && (NV_DGPU_PEG_PORT)
    //Already down
//[-end-220403-TAMT000047-add]//
#else
    // IBV_customize ADD below for before De-Assert Reset Pin
//[-start-211103-IB05660186-modify]//
    If (LEqual (DGPV, NVIDIA_VID)) {
      If (LEqual (PCI_SCOPE.NVGE, NV_GEN_20)) {
        Store (Zero, Local0)
        While (LEqual (\_SB.GGIV (DGPU_SCOPE.PWGD), Zero)) { // OEM need add PWR_OK pin GPIO number for PWOK
          If (LGreater (Local0, 0x20)) {
            Store ("PON: Time Out waiting PWOK ON", Debug)
            Break
          }

          Sleep (One)
          Increment (Local0)
        }
      }
    }
//[-end-211103-IB05660186-modify]//
#endif
//[-end-211112-QINGLIN0114-modify]//
#endif

    // De-Assert Reset Pin
    \PIN.OFF (RSTG)
//[-start-210816-IB05660174-add]//
    // HG power sequence tuning: delay after hold reset for PCIe by DelayAfterHoldReset of SCU
    // Following delay code needs to be opened by project requirement.
    //Sleep (DLHR)
//[-end-210816-IB05660174-add]//
  }

  // Turn off power to PCIe Slot
  // Since this method is also used by the remapped devices to turn off power to the slot
  // this method should not make any access to the PCIe config space.
  Method (POFF) {

    // Assert Reset Pin after the delay passed from the bus driver
    Divide (LNRD, 1000, , Local1)
    Sleep (Local1)

    // Reset pin is mandatory for correct PCIe RTD3 flow
    \PIN.ON (RSTG)
//[-start-210816-IB05660174-add]//
    // HG power sequence tuning: delay after hold reset for PCIe by DelayAfterHoldReset of SCU
    // Following delay code needs to be opened by project requirement.
    //Sleep (DLHR)
//[-end-210816-IB05660174-add]//

    If (LAnd (CondRefOf (PRTP), LEqual (PRTP, PCIE_RP_TYPE_PCH))) {
      // Enable modPHY power gating
      // This must be done after the device has been put in reset
      \_SB.PSD3 (SLOT)
    }

    //
    // On RTD3 entry, BIOS will instruct the PMC to disable source clocks.
    // This is done through sending a PMC IPC command if IPC Command is supported.
    //
    If (CondRefOf (SCLK)) {
//[-start-210927-QINGLIN0086-modify]//
#if defined(S570_SUPPORT) && defined(NV_DGPU_PEG_PORT) && (NV_DGPU_PEG_PORT)
      // not need, remove it.
//[-start-220403-TAMT000047-add]//
#elif defined(S77014IAH_SUPPORT) && defined(NV_DGPU_PEG_PORT) && (NV_DGPU_PEG_PORT)
      // not need, remove it.
//[-end-220403-TAMT000047-add]//
#else    
      If(CondRefOf(TMCS)) {
        SPCX(SCLK, 0, TMCS)
      } Else {
        SPCO(SCLK, 0)
      }
#endif
//[-end-210927-QINGLIN0086-modify]//
    }

    // Power OFF for Slot
    If (CondRefOf (PWRG)) {
      If ( LEqual ( GPPR(), 1)) { // we can safely disable power.
        \PIN.OFF (PWRG)
//[-start-210927-QINGLIN0086-add]//
//[-start-220403-TAMT000047-add]//
#if defined(S570_SUPPORT) || defined(S77014IAH_SUPPORT)
//[-end-220403-TAMT000047-add]//
#if defined(NV_DGPU_PEG_PORT) && (NV_DGPU_PEG_PORT)
        Sleep (100) //sleep 100ms
#endif
#endif
//[-end-210927-QINGLIN0086-add]//
//[-start-210816-IB05660174-add]//
        // HG power sequence tuning: delay after power enable for PCIe by DelayAfterPwrEn of SCU
        // Following delay code needs to be opened by project requirement.
        //Sleep (DLPW) 
//[-end-210816-IB05660174-add]//
      }
      // Store current timestamp in WOFF
      If (CondRefOf (WOFF)) {
        Store (Timer (), WOFF)
      }
    }

    // enable WAKE
    If (CondRefOf (WAKG)) {
      If (LAnd (LNotEqual (WAKG, 0), WKEN)) {
        \_SB.SHPO (WAKG, 0)
      }
    }
    If (CondRefOf (EHDW)) { // DG support HPD as D3 Wake GPIO and DHDW exists
      EHDW () // Enable Discrete Graphics HPD SCI implementated As D3 Wake GPIO
    }
    //Check if the RP is a CEM slot
    If (CondRefOf(CEMP)) {
      CEMP(0) //Set the CEM slot to enter D3cold
    }
  }

  Method (_PR0) {
    Return (Package (){PXP})
  }

  Method (_PR3) {
    Return (Package (){PXP})
  }

  //
  // Update PERST# assertion delay.
  // This function will be called from reference code during PCIe _DSM function index 11 evaluation.
  // Arg0 - New delay value in microseconds. Max is 10ms
  //
  // @return Last sucessfully negotiated value in us. 0 if no such value exists.
  //
  Method (UPRD, 1, Serialized) {
    If (LLessEqual (Arg0, 10000)) {
      // If the value does not exceed the limit
      // Update last negotiated value and calculate the value in ms.
      Store (Arg0, LNRD)
    }
    Return (LNRD)
  }

