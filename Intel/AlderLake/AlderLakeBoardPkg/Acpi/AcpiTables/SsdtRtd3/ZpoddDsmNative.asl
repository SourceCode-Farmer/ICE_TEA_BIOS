/** @file
  ACPI RTD3 native ZPODD _DSM support for SATA ports

@copyright
  INTEL CONFIDENTIAL
  Copyright 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains a 'Sample Driver' and is licensed as such under the terms
  of your license agreement with Intel or your vendor. This file may be modified
  by the user, subject to the additional terms of the license agreement.

@par Specification Reference:
**/

    //
    // ZPODD windows native _DSM support start
    // This follows windows requirements
    //
    ///
    /// _DSM : Device Specific Method for the Windows native ZPODD support.
    /// This method enables management of each port of a SATA controller separate from the host controller as a whole.
    ///
    /// Arg0: Integer Revision Level
    /// Arg1: Integer Function Index
    /// Arg2: Package Parameters
    ///
    Method(ZPOV, 3, Serialized, 0, UnknownObj, {IntObj, IntObj, PkgObj}) {

      Switch (ToInteger(Arg1)) {
        Case (0) {
          ///Case 0: Standard query - A bitmask of functions supported
          ///- Supports function 0-3
          Return(Buffer() {0xF}) // Functions supported when ZPODD RTD3 enabled
        }
        Case (1) {
          ///Case 1: Query Device IDs (Addresses) of children where drive power and/or DevSleep are supported.
          ///- SATA HBA provides autonomous link (DevSleep) support, return a package of 0 elements
          Return( Package(){})     ///- SATA HBA provides native DevSleep
        }
        Case (2) {
          /// Case 2: Control power to device
          Store(ToInteger(DerefOf(Index(Arg2, Zero))), Local0)
          Store(ToInteger(DerefOf(Index(Arg2, One))), Local1)

          Switch(ToInteger(Local0)) {
            Case (Package () {0x0007FFFF}){
              If(Local1) { // Applying Power
                \ZPDR._ON()
              }
            }
          }
          Return (0)
        }
        Case (3) {
          /// Case 3: Current status of Device/Link of Port
          Store(ToInteger(DerefOf(Index(Arg2, Zero))), Local0)
          Switch(ToInteger(Local0)) {  /// Check for SATA port
            Case (Package () {0x0007FFFF}) {  ///- SATA Port 7
              ///- Bit0 => Device power state
              Return (\ZPDR._STA())
            }

            Default { ///- Invalid SATA Port - error
              Return (Ones)
            }
          }
        }
        Default {
          Return (0)
        }
      }
    } /// @defgroup sata_dsm SATA _DSM Method