/**@file

 @copyright
  INTEL CONFIDENTIAL
  Copyright 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains an 'Intel Peripheral Driver' and is uniquely identified as
  "Intel Reference Module" and is licensed for Intel CPUs and chipsets under
  the terms of your license agreement with Intel or your vendor. This file may
  be modified by the user, subject to additional terms of the license agreement.

@par Specification Reference:
**/

DefinitionBlock (
    "UsbPortXhciAdlMErb.aml",
    "SSDT",
    0x02, // SHAD revision.
        // A Revision field value greater than or equal to 2 signifies that integers
        // declared within the Definition Block are to be evaluated as 64-bit values
    "INTEL", // OEM ID (6 byte string)
    "xh_adl_M", // OEM table ID  (8 byte string) BoardIdAdlMLp4Rvp
    0x0 // OEM version of DSDT table (4 byte Integer)
    )
{
  External (\_SB.PC00.XHCI.RHUB.HS01, DeviceObj)
  External (\_SB.PC00.XHCI.RHUB.HS02, DeviceObj)
  External (\_SB.PC00.XHCI.RHUB.HS03, DeviceObj)
  External (\_SB.PC00.XHCI.RHUB.HS04, DeviceObj)
  External (\_SB.PC00.XHCI.RHUB.HS05, DeviceObj)
  External (\_SB.PC00.XHCI.RHUB.HS06, DeviceObj)

  External (\_SB.PC00.XHCI.RHUB.SS01, DeviceObj)
  External (\_SB.PC00.XHCI.RHUB.SS02, DeviceObj)
  External (\_SB.PC00.XHCI.RHUB.SS03, DeviceObj)
  External (\_SB.PC00.XHCI.RHUB.SS04, DeviceObj)

  External (\_SB.PC00.TXHC.RHUB.SS01, DeviceObj)
  External (\_SB.PC00.TXHC.RHUB.SS02, DeviceObj)

  External (\_SB.UBTC.RUCC, MethodObj)

  External (\ADBG, MethodObj)
  External (\_SB.BTRK, MethodObj)

  External (PU2C) // PCH USB2 Port count from NVS
  External (PU3C) // PCH USB3 Port count from NVS


  Method (GPLD, 2, Serialized) {
    //
    // Arg0:  Visible
    // Arg1:  Group position
    //
    Name(PCKG, Package() { Buffer(0x10) {} })
    CreateField(DerefOf(Index(PCKG,0)), 0, 7, REV)
    Store(2,REV)
    CreateField(DerefOf(Index(PCKG,0)), 7, 1, RGB)
    Store(1,RGB)
    CreateField(DerefOf(Index(PCKG,0)), 64, 1, VISI) // Arg 0
    Store(Arg0, VISI)
    CreateField(DerefOf(Index(PCKG,0)), 87, 8, GPOS) // Arg 1
    Store(Arg1, GPOS)
    return (PCKG)
  }

  Method (GUPC, 2, Serialized) {
    /*
    Arg0: Connectable
    Non-Zero value: Connectable

    Arg1: Type
    0x00: Type A connector
    0x01: Mini-AB connector
    0x02: ExpressCard
    0x03: USB 3 Standard-A connector
    0x04: USB 3 Standard-B connector
    0x05: USB 3 Micro-B connector
    0x06: USB 3 Micro-AB connector
    0x07: USB 3 Power-B connector
    0x08: Type C connector - USB2-only
    0x09: Type C connector - USB2 and SS with Switch
    0x0A: Type C connector - USB2 and SS without Switch
    0x0B - 0xFE: Reserved
    0xFF: Proprietary connector
    */
    Name(PCKG, Package(4) { 0xFF, 0xFF, 0, 0 } )
    Store(Arg0,Index(PCKG,0))
    Store(Arg1,Index(PCKG,1))
    return (PCKG)
  }

  // Board information

  // PCH USB 2.0 and Corresponding USB 3.1
  // USB 2.0 Port No:Port1
  // Port Details:Type C port -various configurations - TCP0
  //
  // USB 2.0 Port No:Port2
  // Port Details:Type C port -various configurations - TCP1
  //
  // USB 2.0 Port No:Port3
  // USB 3.1 Port No:Port3
  // Port Details:M.2/Header to Type A - WLAN M.2 Module / TTK3 Header/ USB2.0 FP header-1
  //
  // USB 2.0 Port No:Port4
  // USB 3.1 Port No:Port4
  // Port Details:M.2/Header to Type A - WWAN M.2 Module / Flex connector/ USB2.0 header-2
  //
  // USB 2.0 Port No:Port5
  // USB 3.1 Port No:Port1
  // Port Details:Type- A  USB3.2 Gen2x Type-A Port -TAP1/ USB 3.2 Gen2x1 Type-A Port -TAP3
  //
  // USB 2.0 Port No:Port 6
  // USB 3.1 Port No:Port 2
  // Port Details: USB3.2 Gen2x1 Type-A Port -TAP2/Fingerprint Sensor Connector
  //

  //
  // CPU XHCI Ports
  //
  Scope (\_SB.PC00.TXHC.RHUB.SS01) {
    // Type C connector - USB2 and SS with Switch
    Method (_UPC) {
      Return (\_SB.UBTC.RUCC(0x01,1))
    }
    // Maps to Type C connector 1
    Method (_PLD) {
      Return (\_SB.UBTC.RUCC(0x01,2))
    }
  }

  Scope (\_SB.PC00.TXHC.RHUB.SS02) {
    // Type C connector - USB2 and SS with Switch
    Method (_UPC) {
      Return (\_SB.UBTC.RUCC(0x02,1))
    }
    // Maps to Type C connector 2
    Method (_PLD) {
      Return (\_SB.UBTC.RUCC(0x02,2))
    }
  }

  //
  // PCH XHCI Ports
  //
  If (LLessEqual (1, PU2C)) {
    Scope (\_SB.PC00.XHCI.RHUB.HS01) {
      // Maps to Type C connector 0
      Method (_UPC) {
        Return (\_SB.UBTC.RUCC(0x01,1))
      }
      Method (_PLD) {
        Return (\_SB.UBTC.RUCC(0x01,2))
      }
    }
  }

  If (LLessEqual (2, PU2C)) {
    Scope (\_SB.PC00.XHCI.RHUB.HS02) {
      // Maps to Type C connector 1
      Method (_UPC) {
        Return (\_SB.UBTC.RUCC(0x02,1))
      }
      Method (_PLD) {
        Return (\_SB.UBTC.RUCC(0x02,2))
      }
    }
  }

  If (LLessEqual (3, PU2C)) {
    Scope (\_SB.PC00.XHCI.RHUB.HS03) {
      Method (_UPC) {
        Return (GUPC (1,0xFF))
      }
      Method (_PLD) {
        Return (GPLD (0,0x00))
      }
    }
  }

  If (LLessEqual (4, PU2C)) {
    Scope (\_SB.PC00.XHCI.RHUB.HS04) {
      Method (_UPC) {
        Return (GUPC (1,0xFF))
      }
      Method (_PLD) {
        Return (GPLD (0,0x00))
      }
    }
  }

  If (LLessEqual (5, PU2C)) {
    Scope (\_SB.PC00.XHCI.RHUB.HS05) {
      Method (_UPC) {
        Return (GUPC (1,0x00))
      }
      Method (_PLD) {
        Return (GPLD (1,0x05))
      }
    }
  }

  If (LLessEqual (6, PU2C)) {
    Scope (\_SB.PC00.XHCI.RHUB.HS06) {
      Method (_UPC) {
        Return (GUPC (1,0x00))
      }
      Method (_PLD) {
        Return (GPLD (1,0x06))
      }
    }
  }

  If (LLessEqual (1, PU3C)) {
    Scope (\_SB.PC00.XHCI.RHUB.SS01) {
      Method (_UPC) {
        Return (GUPC (1,0x00))
      }
      Method (_PLD) {
        Return (GPLD (1,0x05))
      }
    }
  }

  If (LLessEqual (2, PU3C)) {
    Scope (\_SB.PC00.XHCI.RHUB.SS02) {
      Method (_UPC) {
        Return (GUPC (1,0x00))
     }
      Method (_PLD) {
        Return (GPLD (1,0x06))
      }
    }
  }

  If (LLessEqual (3, PU3C)) {
    Scope (\_SB.PC00.XHCI.RHUB.SS03) {
      Method (_UPC) {
       Return (GUPC (1,0xFF))
      }
      Method (_PLD) {
        Return (GPLD (0,0x00))
      }
    }
  }

  If (LLessEqual (4, PU3C)) {
    Scope (\_SB.PC00.XHCI.RHUB.SS04) {
      Method (_UPC) {
        Return (GUPC (1,0xFF))
      }
      Method (_PLD) {
        Return (GPLD (0,0x00))
      }
    }
  }
}

