/**@file

 @copyright
  INTEL CONFIDENTIAL
  Copyright 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains an 'Intel Peripheral Driver' and is uniquely identified as
  "Intel Reference Module" and is licensed for Intel CPUs and chipsets under
  the terms of your license agreement with Intel or your vendor. This file may
  be modified by the user, subject to additional terms of the license agreement.

@par Specification Reference:
**/
#include <Include/AcpiDebug.h>

DefinitionBlock (
    "UsbPortXhciAdlSSbgaRvp3.aml",
    "SSDT",
    0x02, // SHAD revision.
        // A Revision field value greater than or equal to 2 signifies that integers
        // declared within the Definition Block are to be evaluated as 64-bit values
    "INTEL", // OEM ID (6 byte string)
    "xh_adsb3", // OEM table ID  (8 byte string) BoardIdAdlSAdpSSbgaDdr4SODimmCrb
    0x0 // OEM version of DSDT table (4 byte Integer)
    )
{
  External(\_SB.PC00.XHCI.RHUB.HS01, DeviceObj)
  External(\_SB.PC00.XHCI.RHUB.HS02, DeviceObj)
  External(\_SB.PC00.XHCI.RHUB.HS03, DeviceObj)
  External(\_SB.PC00.XHCI.RHUB.HS04, DeviceObj)
  External(\_SB.PC00.XHCI.RHUB.HS05, DeviceObj)
  External(\_SB.PC00.XHCI.RHUB.HS06, DeviceObj)
  External(\_SB.PC00.XHCI.RHUB.HS07, DeviceObj)
  External(\_SB.PC00.XHCI.RHUB.HS08, DeviceObj)
  External(\_SB.PC00.XHCI.RHUB.HS09, DeviceObj)
  External(\_SB.PC00.XHCI.RHUB.HS10, DeviceObj)
  External(\_SB.PC00.XHCI.RHUB.HS11, DeviceObj)
  External(\_SB.PC00.XHCI.RHUB.HS12, DeviceObj)
  External(\_SB.PC00.XHCI.RHUB.HS13, DeviceObj)
  External(\_SB.PC00.XHCI.RHUB.HS14, DeviceObj)

  External(\_SB.PC00.XHCI.RHUB.SS01, DeviceObj)
  External(\_SB.PC00.XHCI.RHUB.SS02, DeviceObj)
  External(\_SB.PC00.XHCI.RHUB.SS03, DeviceObj)
  External(\_SB.PC00.XHCI.RHUB.SS04, DeviceObj)
  External(\_SB.PC00.XHCI.RHUB.SS05, DeviceObj)
  External(\_SB.PC00.XHCI.RHUB.SS06, DeviceObj)
  External(\_SB.PC00.XHCI.RHUB.SS07, DeviceObj)
  External(\_SB.PC00.XHCI.RHUB.SS08, DeviceObj)
  External(\_SB.PC00.XHCI.RHUB.SS09, DeviceObj)
  External(\_SB.PC00.XHCI.RHUB.SS10, DeviceObj)

  External(\_SB.PC00.RP08.PXSX.WIST, MethodObj)

  External(ATDV)
  External(BTSE)
  External(BTBR)
  External(BED2)
  External(BED3)
  External(BTLE)
  External(BTL2)
  External(BTLL)
  External(CECV)
  External(\CNMT)
  External(\_SB.BTRK, MethodObj)
  External(TILE)
  External(TIS0)
  External(TS0X)
  External(TIS4)
  External(TIS5)
  External(SLEC)
  External(LEDU)
  External(TAPM)

  External(PU2C) // PCH USB2 Port count from NVS
  External(PU3C) // PCH USB3 Port count from NVS
  External(\_SB.PC00.XHCI, DeviceObj)
  External(PPOE, IntObj) // PD PD_ON Enabled

  External(\_SB.UBTC.RUCC, MethodObj)

  If (LNotEqual (PPOE, 0)) { // Only if PD PS_ON is enabled
    Scope (\_SB.PC00.XHCI) {
      Include ("EcLessPd.asl")
      Method (_PR0) {
        Return (Package() {PDPG})
      }

      Method (_PR3) {
        Return (Package() {PDPG})
      }
    } // End Scope (\_SB.PC00.XHCI)
  } // End Only if PD PS_ON is enabled

  Method (GPLD, 2, Serialized) {
    //
    // Arg0:  Visible
    // Arg1:  Group position
    //
    Name(PCKG, Package() { Buffer(0x10) {} })
    CreateField(DerefOf(Index(PCKG,0)), 0, 7, REV)
    Store(2,REV)
    CreateField(DerefOf(Index(PCKG,0)), 7, 1, RGB)
    Store(1,RGB)
    CreateField(DerefOf(Index(PCKG,0)), 64, 1, VISI) // Arg 0
    Store(Arg0, VISI)
    CreateField(DerefOf(Index(PCKG,0)), 87, 8, GPOS) // Arg 1
    Store(Arg1, GPOS)
    return (PCKG)
  }

  Method (GUPC, 2, Serialized) {
    /*
    Arg0: Connectable
    Non-Zero value: Connectable

    Arg1: Type
    0x00: Type A connector
    0x01: Mini-AB connector
    0x02: ExpressCard
    0x03: USB 3 Standard-A connector
    0x04: USB 3 Standard-B connector
    0x05: USB 3 Micro-B connector
    0x06: USB 3 Micro-AB connector
    0x07: USB 3 Power-B connector
    0x08: Type C connector - USB2-only
    0x09: Type C connector - USB2 and SS with Switch
    0x0A: Type C connector - USB2 and SS without Switch
    0x0B - 0xFE: Reserved
    0xFF: Proprietary connector
    */
    Name(PCKG, Package(4) { 0xFF, 0xFF, 0, 0 } )
    Store(Arg0,Index(PCKG,0))
    Store(Arg1,Index(PCKG,1))
    return (PCKG)
  }

  //
  // PCH XHCI Ports
  //
  If (LLessEqual (1, PU2C)) {
    Scope (\_SB.PC00.XHCI.RHUB.HS01) {  // Front panel : USB 3.2 Type-C
      Method (_UPC) {
        Return (\_SB.UBTC.RUCC(3, 1))
      }
      Method (_PLD) {
        Return (\_SB.UBTC.RUCC(3, 2))
      }
    }
  }

  If (LLessEqual (2, PU2C)) {
    Scope (\_SB.PC00.XHCI.RHUB.HS02) {  // FPS header
      Method (_UPC) {
        Return (GUPC (1,0xFF))
      }
      Method (_PLD) {
        Return (GPLD (0,0x02))
      }
    }
  }

  If (LLessEqual (3, PU2C)) {
    Scope (\_SB.PC00.XHCI.RHUB.HS03) {  // Vertical Type A 1
      Method (_UPC) {
        Return (GUPC (1,0x03))
      }
      Method (_PLD) {
        Return (GPLD (1,0x03))
      }
    }
  }

  If (LLessEqual (4, PU2C)) {
    Scope (\_SB.PC00.XHCI.RHUB.HS04) {  // TTK3 HDR
      Method (_UPC) {
        Return (GUPC (1,0xFF))
      }
      Method (_PLD) {
        Return (GPLD (0,0x04))
      }
    }
  }

  If (LLessEqual (5, PU2C)) {
    Scope (\_SB.PC00.XHCI.RHUB.HS05) {  // POR Thunderbolt
      Method (_UPC) {
        Return (GUPC (1,0xFF))
      }
      Method (_PLD) {
        Return (GPLD (0,0x05))
      }
    }
  }

  If (LLessEqual (6, PU2C)) {
    Scope (\_SB.PC00.XHCI.RHUB.HS06) {  // POR Thunderbolt
      Method (_UPC) {
        Return (GUPC (1,0xFF))
      }
      Method (_PLD) {
        Return (GPLD (0,0x06))
      }
    }
  }

  If (LLessEqual (7, PU2C)) {
    Scope (\_SB.PC00.XHCI.RHUB.HS07) {  // Back Panel Type-A 3
      Method (_UPC) {
        Return (GUPC (1,0x03))
      }
      Method (_PLD) {
        Return (GPLD (0,0x07))
      }
    }
  }

  If (LLessEqual (8, PU2C)) {
    Scope (\_SB.PC00.XHCI.RHUB.HS08) {  // Back Panel Type-A 2
      Method (_UPC) {
        Return (GUPC (1,0x03))
      }
      Method (_PLD) {
        Return (GPLD (1,0x08))
      }
    }
  }

  If (LLessEqual (9, PU2C)) {
    Scope (\_SB.PC00.XHCI.RHUB.HS09) {  // not used
      Method (_UPC) {
        Return (GUPC (0,0xFF))
      }
      Method (_PLD) {
        Return (GPLD (0,0x09))
      }
     }
  }

  If (LLessEqual (10, PU2C)) {
    Scope (\_SB.PC00.XHCI.RHUB.HS10) {  // Front Panel USB 2.0 P1
      Method (_UPC) {
        Return (GUPC (1,0x00))
      }
      Method (_PLD) {
        Return (GPLD (1,0x0A))
      }
    }
  }

  If (LLessEqual (11, PU2C)) {
    Scope (\_SB.PC00.XHCI.RHUB.HS11) {  // Front Panel USB 2.0 P2
      Method (_UPC) {
        Return (GUPC (1,0x00))
      }
      Method (_PLD) {
        Return (GPLD (1,0x0B))
      }
    }
  }

  If (LLessEqual (12, PU2C)) {
    Scope (\_SB.PC00.XHCI.RHUB.HS12) {  // Back Panel Type-A 1
      Method (_UPC) {
        Return (GUPC (1,0x03))
      }
      Method (_PLD) {
        Return (GPLD (1,0x0C))
      }
    }
  }

  If (LLessEqual (13, PU2C)) {
    Scope (\_SB.PC00.XHCI.RHUB.HS13) {  // WWAN M.2 Module
      Method (_UPC) {
        Return (GUPC (1,0xFF))
      }
      Method (_PLD) {
        Return (GPLD (0,0x0D))
      }
    }
  }

  If (LLessEqual (14, PU2C)) {
    Scope (\_SB.PC00.XHCI.RHUB.HS14) {  // Maps to M.2 BT
      Method(_UPC) {
        Return (GUPC(1,0xFF))
      }
      Method(_PLD) {
        Return (GPLD(0,0x0E))
      }

      If (\_SB.PC00.RP08.PXSX.WIST()) {
        Include("AntennaDiversity.asl")
        Include("BtFeatureDsm.asl")
        Include("BtReset.asl")
        Include("BtRegulatory.asl")
        Include("GeneralPurposeConfig.asl")
        Include("BtTile.asl")
      }
    }
  }

  If (LLessEqual (1, PU3C)) {
    Scope (\_SB.PC00.XHCI.RHUB.SS01) {  // Back Panel Type-A 1
      Method (_UPC) {
       Return (GUPC (1,0x03))
      }
      Method (_PLD) {
        Return (GPLD (1,0x0C))
      }
    }
  }


  If (LLessEqual (2, PU3C)) {
    Scope (\_SB.PC00.XHCI.RHUB.SS02) {  // Back Panel Type-A 2
      Method (_UPC) {
        Return (GUPC (1,0x03))
      }
      Method (_PLD) {
        Return (GPLD (1,0x08))
      }
    }
  }

  If (LLessEqual (3, PU3C)) {
    Scope (\_SB.PC00.XHCI.RHUB.SS03) {  // Back Panel Type-A 3
      Method (_UPC) {
        Return (GUPC (1,0x03))
      }
      Method (_PLD) {
        Return (GPLD (1,0x07))
      }
    }
  }

  If (LLessEqual (4, PU3C)) {
    Scope (\_SB.PC00.XHCI.RHUB.SS04) {  // Front panel : USB 3.2 Type-C
      Method (_UPC) {
        Return (\_SB.UBTC.RUCC(3, 1))
      }
      Method (_PLD) {
        Return (\_SB.UBTC.RUCC(3, 2))
      }
    }
  }

  If (LLessEqual (5, PU3C)) {
    Scope (\_SB.PC00.XHCI.RHUB.SS05) {  // Front panel : USB 3.2 FP AIC
      Method (_UPC) {
        Return (GUPC (1,0xFF))
      }
      Method (_PLD) {
        Return (GPLD (0,0x02))
      }
    }
  }

  If (LLessEqual (6, PU3C)) {
    Scope (\_SB.PC00.XHCI.RHUB.SS06) {  // Vertical Type A 1
      Method (_UPC) {
        Return (GUPC (1,0x03))
      }
      Method (_PLD) {
        Return (GPLD (1,0x03))
      }
    }
  }


  If (LLessEqual (7, PU3C)) {
    Scope (\_SB.PC00.XHCI.RHUB.SS07) {  // Unused
      Method (_UPC) {
        Return (GUPC (0,0xFF))
      }
      Method (_PLD) {
        Return (GPLD (0,0x12))
      }
    }
  }

  If (LLessEqual (8, PU3C)) {
    Scope (\_SB.PC00.XHCI.RHUB.SS08) {  // Unused
      Method (_UPC) {
        Return (GUPC (0,0xFF))
      }
      Method (_PLD) {
        Return (GPLD (0,0x13))
      }
    }
  }

  If (LLessEqual (9, PU3C)) {
    Scope (\_SB.PC00.XHCI.RHUB.SS09) {  // Unused
      Method (_UPC) {
        Return (GUPC (0,0xFF))
      }
      Method (_PLD) {
        Return (GPLD (0,0x14))
      }
    }
  }

  If (LLessEqual (10, PU3C)) {
    Scope (\_SB.PC00.XHCI.RHUB.SS10) {  // Unused
      Method (_UPC) {
        Return (GUPC (0,0xFF))
      }
      Method (_PLD) {
        Return (GPLD (0,0x15))
      }
    }
  }
}
