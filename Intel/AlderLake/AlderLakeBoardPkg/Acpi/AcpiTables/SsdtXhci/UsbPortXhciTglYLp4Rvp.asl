/**@file

 @copyright
  INTEL CONFIDENTIAL
  Copyright 2018 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains an 'Intel Peripheral Driver' and is uniquely identified as
  "Intel Reference Module" and is licensed for Intel CPUs and chipsets under
  the terms of your license agreement with Intel or your vendor. This file may
  be modified by the user, subject to additional terms of the license agreement.

@par Specification Reference:
**/

DefinitionBlock (
    "UsbPortXhciTglYLp4Rvp.aml",
    "SSDT",
    0x02, // SHAD revision.
        // A Revision field value greater than or equal to 2 signifies that integers
        // declared within the Definition Block are to be evaluated as 64-bit values
    "INTEL", // OEM ID (6 byte string)
    "xh_tydd4", // OEM table ID  (8 byte string) BoardIdTglYLp4
    0x0 // OEM version of DSDT table (4 byte Integer)
    )
{
  External(\_SB.PC00.TXHC.RHUB.SS01, DeviceObj)
  External(\_SB.PC00.TXHC.RHUB.SS02, DeviceObj)
  External(\_SB.PC00.TXHC.RHUB.SS03, DeviceObj)
  External(\_SB.PC00.TXHC.RHUB.SS04, DeviceObj)

  External(\_SB.PC00.XHCI.RHUB.HS01, DeviceObj)
  External(\_SB.PC00.XHCI.RHUB.HS02, DeviceObj)
  External(\_SB.PC00.XHCI.RHUB.HS03, DeviceObj)
  External(\_SB.PC00.XHCI.RHUB.HS04, DeviceObj)
  External(\_SB.PC00.XHCI.RHUB.HS05, DeviceObj)
  External(\_SB.PC00.XHCI.RHUB.HS06, DeviceObj)
  External(\_SB.PC00.XHCI.RHUB.HS07, DeviceObj)
  External(\_SB.PC00.XHCI.RHUB.HS08, DeviceObj)
  External(\_SB.PC00.XHCI.RHUB.HS09, DeviceObj)
  External(\_SB.PC00.XHCI.RHUB.HS10, DeviceObj)
  External(\_SB.PC00.XHCI.RHUB.SS01, DeviceObj)
  External(\_SB.PC00.XHCI.RHUB.SS02, DeviceObj)
  External(\_SB.PC00.XHCI.RHUB.SS03, DeviceObj)
  External(\_SB.PC00.XHCI.RHUB.SS04, DeviceObj)
  External(\_SB.PC00.XHCI.RHUB.SS05, DeviceObj)
  External(\_SB.PC00.XHCI.RHUB.SS06, DeviceObj)

  External(\_SB.PC00.TXHC.RHUB.TPLD, MethodObj)
  External(\_SB.PC00.TXHC.RHUB.TUPC, MethodObj)

  External(\_SB.UBTC.RUCC, MethodObj)
  External(\_SB.PC00.CNIP, MethodObj)
  External(\_SB.PC00.RP03.PXSX.WIST, MethodObj)

  External(ATDV)
  External(BTSE)
  External(BTBR)
  External(BED2)
  External(BED3)
  External(BTLE)
  External(BTL2)
  External(BTLL)
  External(CECV)

  Method(GPLD, 2, Serialized) {
    //
    // Arg0:  Visible
    // Arg1:  Group position
    //
    Name(PCKG, Package() { Buffer(0x10) {} })
    CreateField(DerefOf(Index(PCKG,0)), 0, 7, REV)
    Store(2,REV)
    CreateField(DerefOf(Index(PCKG,0)), 7, 1, RGB)
    Store(1,RGB)
    CreateField(DerefOf(Index(PCKG,0)), 64, 1, VISI) // Arg 0
    Store(Arg0, VISI)
    CreateField(DerefOf(Index(PCKG,0)), 87, 8, GPOS) // Arg 1
    Store(Arg1, GPOS)
    return (PCKG)
  }

  Method(GUPC, 2, Serialized) {
    /*
    Arg0: Connectable
    Non-Zero value: Connectable

    Arg1: Type
    0x00: Type A connector
    0x01: Mini-AB connector
    0x02: ExpressCard
    0x03: USB 3 Standard-A connector
    0x04: USB 3 Standard-B connector
    0x05: USB 3 Micro-B connector
    0x06: USB 3 Micro-AB connector
    0x07: USB 3 Power-B connector
    0x08: Type C connector - USB2-only
    0x09: Type C connector - USB2 and SS with Switch
    0x0A: Type C connector - USB2 and SS without Switch
    0x0B - 0xFE: Reserved
    0xFF: Proprietary connector
    */
    Name(PCKG, Package(4) { 0xFF, 0xFF, 0, 0 } )
    Store(Arg0,Index(PCKG,0))
    Store(Arg1,Index(PCKG,1))
    return (PCKG)
  }
  //
  // CPU XHCI Ports
  //
  Scope (\_SB.PC00.TXHC.RHUB.SS01) {
    // Type C connector - USB2 and SS with Switch
    Method(_UPC) {
      Return (\_SB.UBTC.RUCC(0x01,1))
    }
    // Maps to Type C connector 1
    Method(_PLD) {
      Return (\_SB.UBTC.RUCC(0x01,2))
    }
  }

  Scope (\_SB.PC00.TXHC.RHUB.SS02) {
    // Type C connector - USB2 and SS with Switch
    Method(_UPC) {
      Return (\_SB.UBTC.RUCC(0x02,1))
    }
    // Maps to Type C connector 2
    Method(_PLD) {
      Return (\_SB.UBTC.RUCC(0x02,2))
    }
  }

  Scope (\_SB.PC00.TXHC.RHUB.SS03) {
    // Type C connector - USB2 and SS with Switch
    Method(_UPC) {
      Return (\_SB.UBTC.RUCC(0x03,1))
    }
    // Maps to Type C connector 3
    Method(_PLD) {
      Return (\_SB.UBTC.RUCC(0x03,2))
    }
  }

  Scope (\_SB.PC00.TXHC.RHUB.SS04) {
    // Unused
    Method(_UPC) {
      Return (GUPC(0x00, 0xFF))
    }
  }

  //
  // PCH XHCI Ports
  //
  Scope (\_SB.PC00.XHCI.RHUB.HS01) {
    // Maps to Type C connector 1
    Method(_UPC) {
      Return (\_SB.UBTC.RUCC(0x01,1))
    }
    Method(_PLD) {
      Return (\_SB.UBTC.RUCC(0x01,2))
    }
  }
  Scope (\_SB.PC00.XHCI.RHUB.HS02) {
    // Maps to M2
    Method(_UPC) {
      Return (GUPC(1,0x0))
    }
    Method(_PLD) {
      Return (GPLD(1,0x06))
    }
  }

  Scope (\_SB.PC00.XHCI.RHUB.HS03) {
    Method(_UPC) {
      If (\_SB.PC00.CNIP) {
        // Maps to M2
        Return (GUPC(0,0xFF))
      }
      Else {
        // Maps to Type A connector
        Return (GUPC(1,0x00))
      }
    }
    Method(_PLD) {
      If (\_SB.PC00.CNIP) {
        // Maps to M.2 CNVi
        Return (GPLD(0,3))
      }
      Else {
        // Maps to USB2 Type A connector
        Return (GPLD(1,3))
      }
    }
  }
  If (\_SB.PC00.RP03.PXSX.WIST()) {
    Scope (\_SB.PC00.XHCI.RHUB.HS03) {
      Include("AntennaDiversity.asl")
      Include("BtRegulatory.asl")
      Include("GeneralPurposeConfig.asl")
    }
  }

  Scope (\_SB.PC00.XHCI.RHUB.HS04) {
    // Maps to USB3/2 Type A connector
    Method(_UPC) {
      Return (GUPC(1,0x0))
    }
    Method(_PLD) {
      Return (GPLD(1,0x05))
    }
  }

  Scope (\_SB.PC00.XHCI.RHUB.HS05) {
    // Maps to Type C connector 2
    Method(_UPC) {
      Return (\_SB.UBTC.RUCC(0x02,1))
    }
    Method(_PLD) {
      Return (\_SB.UBTC.RUCC(0x02,2))
    }
  }

  Scope (\_SB.PC00.XHCI.RHUB.HS06) {
    // Maps to Type C connector 3
    Method(_UPC) {
      Return (\_SB.UBTC.RUCC(0x03,1))
    }
    Method(_PLD) {
      Return (\_SB.UBTC.RUCC(0x03,2))
    }
  }

  Scope (\_SB.PC00.XHCI.RHUB.HS07) {
    // Unused
    Method(_UPC) {
      Return (GUPC(0,0xFF))
    }
  }

  Scope (\_SB.PC00.XHCI.RHUB.HS08) {
    // Unused
    Method(_UPC) {
      Return (GUPC(0,0xFF))
    }
  }

  Scope (\_SB.PC00.XHCI.RHUB.HS09) {
    // Unused
    Method(_UPC) {
      Return (GUPC(0,0xFF))
    }
  }

  Scope (\_SB.PC00.XHCI.RHUB.HS10) {
    // Unused
    Method(_UPC) {
      Return (GUPC(0,0xFF))
    }
  }

  Scope (\_SB.PC00.XHCI.RHUB.SS01) {
    // Maps to USB3/2 Type A connector
    Method(_UPC) {
      Return (GUPC(1,0x0))
    }
    Method(_PLD) {
      Return (GPLD(1,0x05))
    }
  }

  Scope (\_SB.PC00.XHCI.RHUB.SS02) {
    // Maps to USB3/2 Type A connector
    Method(_UPC) {
      Return (GUPC(1,0x0))
    }
    Method(_PLD) {
      Return (GPLD(1,0x06))
    }
  }

  Scope (\_SB.PC00.XHCI.RHUB.SS03) {
    // Maps to M2
    Method(_UPC) {
      Return (GUPC(0,0xFF))
    }
  }

  Scope (\_SB.PC00.XHCI.RHUB.SS04) {
    // For PCIE using
    Method(_UPC) {
      Return (GUPC(0,0xFF))
    }
  }

  Scope (\_SB.PC00.XHCI.RHUB.SS05) {
    // Unused
    Method(_UPC) {
      Return (GUPC(0,0xFF))
    }
  }

  Scope (\_SB.PC00.XHCI.RHUB.SS06) {
    // Unused
    Method(_UPC) {
      Return (GUPC(0,0xFF))
    }
  }
}
