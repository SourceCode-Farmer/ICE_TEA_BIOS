/**@file

 @copyright
  INTEL CONFIDENTIAL
  Copyright 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains an 'Intel Peripheral Driver' and is uniquely identified as
  "Intel Reference Module" and is licensed for Intel CPUs and chipsets under
  the terms of your license agreement with Intel or your vendor. This file may
  be modified by the user, subject to additional terms of the license agreement.

@par Specification Reference:
**/
#include <Include/AcpiDebug.h>

DefinitionBlock (
    "UsbPortXhciAdlpDdr5MrRvp.aml",
    "SSDT",
    0x02, // SHAD revision.
        // A Revision field value greater than or equal to 2 signifies that integers
        // declared within the Definition Block are to be evaluated as 64-bit values
    "INTEL", // OEM ID (6 byte string)
    "xh_adlMR", // OEM table ID  (8 byte string) BoardIdAdlPDdr5MRRvp
    0x0 // OEM version of DSDT table (4 byte Integer)
    )
{
  External (\_SB.PC00.TXHC.RHUB.SS01, DeviceObj)

  External (\_SB.PC00.XHCI.RHUB.HS01, DeviceObj)
  External (\_SB.PC00.XHCI.RHUB.HS02, DeviceObj)
  External (\_SB.PC00.XHCI.RHUB.HS03, DeviceObj)
  External (\_SB.PC00.XHCI.RHUB.HS04, DeviceObj)
  External (\_SB.PC00.XHCI.RHUB.HS05, DeviceObj)
  External (\_SB.PC00.XHCI.RHUB.HS06, DeviceObj)
  External (\_SB.PC00.XHCI.RHUB.HS07, DeviceObj)
  External (\_SB.PC00.XHCI.RHUB.HS08, DeviceObj)
  External (\_SB.PC00.XHCI.RHUB.HS09, DeviceObj)
  External (\_SB.PC00.XHCI.RHUB.HS10, DeviceObj)

  External (\_SB.PC00.XHCI.RHUB.SS01, DeviceObj)
  External (\_SB.PC00.XHCI.RHUB.SS02, DeviceObj)
  External (\_SB.PC00.XHCI.RHUB.SS03, DeviceObj)
  External (\_SB.PC00.XHCI.RHUB.SS04, DeviceObj)

  External (\_SB.PC00.RP05.PXSX.WIST, MethodObj)

  External (ATDV)
  External (BTSE)
  External (BTBR)
  External (BED2)
  External (BED3)
  External (BTLE)
  External (BTL2)
  External (BTLL)
  External (CECV)
  External (\CNMT)
  External (\ADBG, MethodObj)
  External (\_SB.BTRK, MethodObj)
  External (TILE)
  External (TIS0)
  External (TS0X)
  External (TIS4)
  External (TIS5)
  External (BLEC)
  External (BLPD)
  External (BLPI)

  External (\_SB.UBTC.RUCC, MethodObj)
  External (PU2C) // PCH USB2 Port count from NVS
  External (PU3C) // PCH USB3 Port count from NVS
  External (\_SB.PC00.XHCI, DeviceObj)

  Method (GPLD, 2, Serialized) {
    //
    // Arg0:  Visible
    // Arg1:  Group position
    //
    Name (PCKG, Package() { Buffer(0x10) {} })
    CreateField (DerefOf(Index(PCKG,0)), 0, 7, REV)
    Store (2,REV)
    CreateField (DerefOf(Index(PCKG,0)), 7, 1, RGB)
    Store (1,RGB)
    CreateField (DerefOf(Index(PCKG,0)), 64, 1, VISI) // Arg 0
    Store (Arg0, VISI)
    CreateField (DerefOf(Index(PCKG,0)), 87, 8, GPOS) // Arg 1
    Store (Arg1, GPOS)
    return (PCKG)
  }

  Method (GUPC, 2, Serialized) {
    /*
    Arg0: Connectable
    Non-Zero value: Connectable

    Arg1: Type
    0x00: Type A connector
    0x01: Mini-AB connector
    0x02: ExpressCard
    0x03: USB 3 Standard-A connector
    0x04: USB 3 Standard-B connector
    0x05: USB 3 Micro-B connector
    0x06: USB 3 Micro-AB connector
    0x07: USB 3 Power-B connector
    0x08: Type C connector - USB2-only
    0x09: Type C connector - USB2 and SS with Switch
    0x0A: Type C connector - USB2 and SS without Switch
    0x0B - 0xFE: Reserved
    0xFF: Proprietary connector
    */
    Name (PCKG, Package(4) { 0xFF, 0xFF, 0, 0 } )
    Store (Arg0,Index(PCKG,0))
    Store (Arg1,Index(PCKG,1))
    return (PCKG)
  }

  //
  //   For RUCC() Method Call
  //   Board             Type C port   Port Origin    PCH Port    Split support
  //   ADL P 2 MR             5          CPU 0          8            Yes
  //                          1          MR1 0          1            Yes
  //                          2          MR1 1          2            Yes
  //                          3          MR2 0          3            Yes
  //                          4          MR2 1          5            Yes

  //
  // Board information
  //
  // PCH USB 2.0 and Corresponding USB 3.1
  // USB 2.0 Port No:Port1
  // Port Details:Type C port -various configurations - TCP0
  //
  // USB 2.0 Port No:Port2
  // Port Details:Type C port -various configurations - TCP1
  //
  // USB 2.0 Port No:Port3
  // Port Details:Type C port -various configurations - TCP2
  //
  // USB 2.0 Port No:Port4
  // USB 3.1 Port No:Port4
  // Port Details:M.2/Type A-WWAN -M.2 Module / USB2.0 Type-A Port-2
  //
  // USB 2.0 Port No:Port5
  // Port Details: Type C port -various configurations-TCP3
  //
  // USB 2.0 Port No:Port 6
  // Port Details: Type A -Fingerprint Sensor Connector / USB2.0 Type-A Port-1
  //
  // USB 2.0 Port No:Port 7
  // USB 3.1 Port No:Port 2
  // Port Details:Type-A  -USB3.2 Gen2x1 Type-A Port -TAP2
  //
  // USB 2.0 Port No:Port 8
  // USB 3.1 Port No:Port 3
  // Port Details:Type-A  -USB3.2 Gen2x1 Type-A Port -TAP3
  //
  // USB 2.0 Port No:Port 9
  // USB 3.1 Port No:Port 1
  // Port Details:Type-A  -USB3.2 Gen2x1 Type-A Port -TAP1
  //
  // USB 2.0 Port No:Port 10
  // Port Details:M.2 WLAN M.2 Module
  //


  //
  // CPU XHCI Ports
  //
  Scope (\_SB.PC00.TXHC.RHUB.SS01) {
    // Type A connector - USB2 and SS with Switch
    Method (_UPC) {
      Return (\_SB.UBTC.RUCC(0x05,1))
    }
    // Maps to Type C connector 1
    Method (_PLD) {
      Return (\_SB.UBTC.RUCC(0x05,2))
    }
  }

  //
  // PCH XHCI Ports
  //
  If (LLessEqual (1, PU2C)) {
    Scope (\_SB.PC00.XHCI.RHUB.HS01) {
      // Maps to Type C connector 1
      Method (_UPC) {
        Return (\_SB.UBTC.RUCC(0x01,1))
      }
      Method (_PLD) {
        Return (\_SB.UBTC.RUCC(0x01,2))
      }
    }
  }

  If (LLessEqual (2, PU2C)) {
    Scope (\_SB.PC00.XHCI.RHUB.HS02) {
      // Maps to Type C connector 2
      Method (_UPC) {
        Return (\_SB.UBTC.RUCC(0x02,1))
      }
      Method (_PLD) {
        Return (\_SB.UBTC.RUCC(0x02,2))
      }
    }
  }

  If (LLessEqual (3, PU2C)) {
    Scope (\_SB.PC00.XHCI.RHUB.HS03) {
      // Maps to Type C connector 4
      Method (_UPC) {
        Return (\_SB.UBTC.RUCC(0x03,1))
      }
      Method (_PLD) {
        Return (\_SB.UBTC.RUCC(0x03,2))
      }
    }
  }


  If (LLessEqual (4, PU2C)) {
    Scope (\_SB.PC00.XHCI.RHUB.HS04) {
      Method (_UPC) {
        Return (GUPC (1,0xFF))
      }
      Method (_PLD) {
        Return (GPLD (0,0x00))
      }
    }
  }

  If (LLessEqual (5, PU2C)) {
    Scope (\_SB.PC00.XHCI.RHUB.HS05) {
      // Maps to Type C connector 5
      Method (_UPC) {
        Return (\_SB.UBTC.RUCC(0x04,1))
      }
      Method (_PLD) {
        Return (\_SB.UBTC.RUCC(0x04,2))
      }
    }
  }

  If (LLessEqual (6, PU2C)) {
    Scope (\_SB.PC00.XHCI.RHUB.HS06) {
      Method (_UPC) {
        Return (GUPC (1,0xFF))
      }
      Method (_PLD) {
        Return (GPLD (0,0x06))
      }
    }
  }

  If (LLessEqual (7, PU2C)) {
    Scope (\_SB.PC00.XHCI.RHUB.HS07) {
      Method (_UPC) {
        Return (GUPC (1,0x00))
      }
      Method (_PLD) {
        Return (GPLD (1,0x07))
      }
    }
  }

  If (LLessEqual (8, PU2C)) {
    Scope (\_SB.PC00.XHCI.RHUB.HS08) {
      // Maps to Type C connector 1
      Method (_UPC) {
        Return (\_SB.UBTC.RUCC(0x05,1))
      }
      Method (_PLD) {
        Return (\_SB.UBTC.RUCC(0x05,2))
      }
    }
  }

  If (LLessEqual (9, PU2C)) {
    Scope (\_SB.PC00.XHCI.RHUB.HS09) {
      Method (_UPC) {
        Return (GUPC (1,0x00))
      }
      Method (_PLD) {
        Return (GPLD (1,0x09))
      }
     }
  }

  If (LLessEqual (10, PU2C)) {
    Scope (\_SB.PC00.XHCI.RHUB.HS10) {
      Method (_UPC) {
        Return (GUPC (1,0xFF))
      }
      Method (_PLD) {
        Return (GPLD (0,0x00))
      }
    }
  }

  If (LLessEqual (1, PU3C)) {
    Scope (\_SB.PC00.XHCI.RHUB.SS01) {  // USB3.2 Gen2x1 Type-A Port - TAP1
      Method (_UPC) {
        Return (GUPC (1,0x00))
      }
      Method (_PLD) {
        Return (GPLD (1,0x09))
      }
    }
  }

  If (LLessEqual (2, PU3C)) {
    Scope (\_SB.PC00.XHCI.RHUB.SS02) {  // USB3.2 Gen2x1 Type-A Port - TAP2
      Method (_UPC) {
        Return (GUPC (1,0x00))
     }
      Method (_PLD) {
        Return (GPLD (1,0x07))
      }
    }
  }

  If (LLessEqual (3, PU3C)) {
    Scope (\_SB.PC00.XHCI.RHUB.SS03) {
      Method (_UPC) {
       Return (GUPC (1,0xFF))
      }
      Method (_PLD) {
        Return (GPLD (0,0x00))
      }
    }
  }

  If (LLessEqual (4, PU3C)) {
    Scope (\_SB.PC00.XHCI.RHUB.SS04) {
      Method (_UPC) {
        Return (GUPC (0,0x00))
      }
      Method (_PLD) {
        Return (GPLD (0,0x00))
      }
    }
  }
}