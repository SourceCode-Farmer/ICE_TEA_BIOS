/** @file
  Intel ACPI Reference Code for connectivity modules

@copyright
  INTEL CONFIDENTIAL
  Copyright 2020 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains a 'Sample Driver' and is licensed as such under the terms
  of your license agreement with Intel or your vendor. This file may be modified
  by the user, subject to the additional terms of the license agreement.

@par Specification Reference:
**/

Name (BTLY, Package()
{
  0,                      // Revision, DWordConst
  Package()               // Configuration for Bluetooth
  {
    0x12,                 // DomainType, 0x12:Bluetooth Core
    0x00,                 // Tile Mode
    0x00,                 // Tile S0
    0x00,                 // Tile S0ix
    0x00,                 // Tile S4
    0x00,                 // Tile S5
    0x00,                 // Special LED Config
    0x0000,               // LED Duration
    0x00,                 // Airplane Mode
    0x00,                 // Reserved
  }
})                        // End of BTLY object

// BTLC (Bluetooth Tile Configuration)
//
// Provide Bluetooth Tile Configuration - Find My PC
//
// Arguments: (0)
//   None
// Return Value:
//
// Name (BTLC, Package()
// {
//   Revision,                        // DWordConst
//   Package()                        // Configuration for Bluetooth
//   {
//     DomainType,                    // DomainType, 0x12:Bluetooth Core
//     BtTileMode,                    // BT Tile Mode
//     TileS0,                        // The activity of Tile in S0
//     TileS0ix,                      // The activity of Tile in S0ix
//     TileS4,                        // The activity of Tile in S4
//     TileS5,                        // The activity of Tile in S5
//     SpecialLedConfig,              // Special LED Config
//     LedDuration,                   // LED Duration in milliseconds
//     AirplaneMode,                  // Airplane Mode
//     Reserved                       // Reserved
//   }
// })                                 // End of BTLC object
//

Method(BTLC,0,Serialized)
{
  Store(\TILE,Index (DeRefOf (Index (BTLY, 1)), 1))  // Tile Mode
  Store(\TIS0,Index (DeRefOf (Index (BTLY, 1)), 2))  // The activity of Tile in S0
  Store(\TS0X,Index (DeRefOf (Index (BTLY, 1)), 3))  // The activity of Tile in S0ix
  Store(\TIS4,Index (DeRefOf (Index (BTLY, 1)), 4))  // The activity of Tile in S4
  Store(\TIS5,Index (DeRefOf (Index (BTLY, 1)), 5))  // The activity of Tile in S5
  Store(\SLEC,Index (DeRefOf (Index (BTLY, 1)), 6))  // Special LED Config
  Store(\LEDU,Index (DeRefOf (Index (BTLY, 1)), 7))  // LED Duration in milliseconds
  Store(\TAPM,Index (DeRefOf (Index (BTLY, 1)), 8))  // Airplane Mode

  Return(BTLY)
}
