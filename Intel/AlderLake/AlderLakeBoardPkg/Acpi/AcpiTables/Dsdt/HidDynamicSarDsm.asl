/** @file
  ACPI DSDT table

@copyright
  INTEL CONFIDENTIAL
  Copyright 2011 - 2020 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains a 'Sample Driver' and is licensed as such under the terms
  of your license agreement with Intel or your vendor. This file may be modified
  by the user, subject to the additional terms of the license agreement.

@par Specification Reference:
**/
  Method (ISAR, 4)
  {
    Name (BUF1, Buffer(2) {0,0})
    //--------------------------------------------
    //  Intel Proprietary Dynamic SAR solution
    //
    //  Method(_DSM, 0x4, Serialized, 0, {IntObj, BuffObj}, {BuffObj, IntObj, IntObj, PkgObj})
    //
    //--------------------------------------------
    If (LEqual(Arg0, ToUUID("1730E71D-E5DD-4A34-BE57-4D76B6A2FE37")))
    {
      // Function 3 : SAR Sensor data upate
      If(LEqual(Arg2, 3))
      {
        If (CondRefOf(\_SB.USSV))
        {
           //BYTE[0] split into two parts:
           //bit[7:4] is used to indicate Protocol ID and
           //bit[3:0] is used to indicate number of SAR channels or Wi-Fi Antennas
           //Store(DeRefOf(Index(Arg3, 0)), Local0)

           //BYTE[1] : Power policy index of Antenna A
           Store(DeRefOf(Index(Arg3, 1)), Local1)
           Add (Local1, 0x1, Local1)

           //BYTE[2] : Power policy index of Antenna B
           Store(DeRefOf(Index(Arg3, 2)), Local2)
           Add (Local2, 0x1, Local2)

           ShiftLeft(Local2, 16, Local2)
           Or(Local2, Local1, Local2)

           \_SB.USSV(Local2)
            Notify(\_SB.WFDE,0xD0)
        }
        Return(0)
      }
      Else
      {
        Return(0) // Function number mismatch but normal return.
      }
    }
    Else
    {
      Return(Buffer(One) { 0x00 }) // Guid mismatch
    }
  } // End Method (ISAR)
