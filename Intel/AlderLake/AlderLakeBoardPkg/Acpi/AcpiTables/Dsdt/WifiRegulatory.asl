/** @file
  Intel ACPI Sample Code for connectivity modules

@copyright
  INTEL CONFIDENTIAL
  Copyright 2017 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains a 'Sample Driver' and is licensed as such under the terms
  of your license agreement with Intel or your vendor. This file may be modified
  by the user, subject to the additional terms of the license agreement.

@par Specification Reference:
**/

Name (WRDX, Package()
{
  0,                      // Revision, [0-255]
  Package()               // Configuration for WiFi
  {
    0x80000000,           // DomainType, 0x7:WiFi Core
    0x8000,               // RegulatoryDomain, Country identifier as defined in ISO/IEC 3166-1 Alpha 2 code
  }
})                        // End of WRDD object

// WRDD (Wireless Regulatory Domain Description)
//
// WiFi has regulatory limitations which prohibit or allow usage of certain bands or channels as well as limiting the Tx power.
// Those settings are different per country (or groups of countries). In order to reduce the number of
// HW SKUs the direction is now to include platform level ACPI setting that determines the country.
// WiFi driver shall read the WRDD object during initialization process and shall configure uCode to apply the right regulatory limits.
//
// Arguments: (0)
//   None
// Return Value:
//
// Name (WRDD, Package()
// {
//   Revision,               // Revision, [0-255]
//   Package()               // Configuration for WiFi
//   {
//     DomainType,           // 0x7:WiFi  Core
//     RegulatoryDomain,     // Country identifier as defined in ISO/IEC 3166-1 Alpha 2 code
//   }
// })                        // End of WRDD object
//
Method(WRDD,0,Serialized)
{
  Store(\WDM1,Index (DeRefOf (Index (WRDX, 1)), 0)) // DomainType
  Store(\CID1,Index (DeRefOf (Index (WRDX, 1)), 1)) // Country identifier

  Return(WRDX)
}

Name (WRDY, Package()
{
  2,                      // Revision, DWordConst, 2 - CDB (Concurrent Dual Band)
  Package()               // Configuration for WiFi
  {
    0x07,                 // DomainType, 0x7:WiFi
    0x80,                 // WiFi SAR Enable/Disable
    0x80,                 // WiFi SAR Tx Power Limit for 2400 MHz Set 1 Chain A
    0x80,                 // WiFi SAR Tx Power Limit for 5150-5350 MHz Set 1 Chain A
    0x80,                 // WiFi SAR Tx Power Limit for 5350-5470 MHz Set 1 Chain A
    0x80,                 // WiFi SAR Tx Power Limit for 5470-5725 MHz Set 1 Chain A
    0x80,                 // WiFi SAR Tx Power Limit for 5725-5925 MHz Set 1 Chain A
    0x80,                 // WiFi SAR Tx Power Limit for 5945-6165 MHz Set 1 Chain A
    0x80,                 // WiFi SAR Tx Power Limit for 6165-6405 MHz Set 1 Chain A
    0x80,                 // WiFi SAR Tx Power Limit for 6405-6525 MHz Set 1 Chain A
    0x80,                 // WiFi SAR Tx Power Limit for 6525-6705 MHz Set 1 Chain A
    0x80,                 // WiFi SAR Tx Power Limit for 6705-6865 MHz Set 1 Chain A
    0x80,                 // WiFi SAR Tx Power Limit for 6865-7105 MHz Set 1 Chain A
    0x80,                 // WiFi SAR Tx Power Limit for 2400 MHz Set 1 Chain B
    0x80,                 // WiFi SAR Tx Power Limit for 5150-5350 MHz Set 1 Chain B
    0x80,                 // WiFi SAR Tx Power Limit for 5350-5470 MHz Set 1 Chain B
    0x80,                 // WiFi SAR Tx Power Limit for 5470-5725 MHz Set 1 Chain B
    0x80,                 // WiFi SAR Tx Power Limit for 5725-5925 MHz Set 1 Chain B
    0x80,                 // WiFi SAR Tx Power Limit for 5945-6165 MHz Set 1 Chain B
    0x80,                 // WiFi SAR Tx Power Limit for 6165-6405 MHz Set 1 Chain B
    0x80,                 // WiFi SAR Tx Power Limit for 6405-6525 MHz Set 1 Chain B
    0x80,                 // WiFi SAR Tx Power Limit for 6525-6705 MHz Set 1 Chain B
    0x80,                 // WiFi SAR Tx Power Limit for 6705-6865 MHz Set 1 Chain B
    0x80,                 // WiFi SAR Tx Power Limit for 6865-7105 MHz Set 1 Chain B
    0x80,                 // WiFi SAR Tx Power Limit for CDB 2400 MHz Set 1 Chain A
    0x80,                 // WiFi SAR Tx Power Limit for CDB 5150-5350 MHz Set 1 Chain A
    0x80,                 // WiFi SAR Tx Power Limit for CDB 5350-5470 MHz Set 1 Chain A
    0x80,                 // WiFi SAR Tx Power Limit for CDB 5470-5725 MHz Set 1 Chain A
    0x80,                 // WiFi SAR Tx Power Limit for CDB 5725-5925 MHz Set 1 Chain A
    0x80,                 // WiFi SAR Tx Power Limit for CDB 5945-6165 MHz Set 1 Chain A
    0x80,                 // WiFi SAR Tx Power Limit for CDB 6165-6405 MHz Set 1 Chain A
    0x80,                 // WiFi SAR Tx Power Limit for CDB 6405-6525 MHz Set 1 Chain A
    0x80,                 // WiFi SAR Tx Power Limit for CDB 6525-6705 MHz Set 1 Chain A
    0x80,                 // WiFi SAR Tx Power Limit for CDB 6705-6865 MHz Set 1 Chain A
    0x80,                 // WiFi SAR Tx Power Limit for CDB 6865-7105 MHz Set 1 Chain A
    0x80,                 // WiFi SAR Tx Power Limit for CDB 2400 MHz Set 1 Chain B
    0x80,                 // WiFi SAR Tx Power Limit for CDB 5150-5350 MHz Set 1 Chain B
    0x80,                 // WiFi SAR Tx Power Limit for CDB 5350-5470 MHz Set 1 Chain B
    0x80,                 // WiFi SAR Tx Power Limit for CDB 5470-5725 MHz Set 1 Chain B
    0x80,                 // WiFi SAR Tx Power Limit for CDB 5725-5925 MHz Set 1 Chain B
    0x80,                 // WiFi SAR Tx Power Limit for CDB 5945-6165 MHz Set 1 Chain B
    0x80,                 // WiFi SAR Tx Power Limit for CDB 6165-6405 MHz Set 1 Chain B
    0x80,                 // WiFi SAR Tx Power Limit for CDB 6405-6525 MHz Set 1 Chain B
    0x80,                 // WiFi SAR Tx Power Limit for CDB 6525-6705 MHz Set 1 Chain B
    0x80,                 // WiFi SAR Tx Power Limit for CDB 6705-6865 MHz Set 1 Chain B
    0x80,                 // WiFi SAR Tx Power Limit for CDB 6865-7105 MHz Set 1 Chain B
  }
})                        // End of WRDD object

// WRDS (Wireless Regulatory Domain SAR)
//
// WiFi has regulatory limitations which prohibit or allow usage of certain bands or channels as well as limiting the Tx power.
// This method is used to modify the WiFi SAR Tx Power Limit to properly control it
//
// Arguments: (0)
//   None
// Return Value:
//
// Name (WRDS, Package()
// {
//   Revision,                // DWordConst
//   Package()                // Configuration for WiFi
//   {
//     DomainType,            // 0x7:WiFi
//     WiFiSarEnable,         // Configures the source location of the WiFi SAR table to be used; 0:Device ignores BIOS configuration; 1:Device uses BIOS configuration
//     WrdsWiFiSarTxPowerLimit1,  // WiFi SAR Tx Power Limit for 2400 MHz Set 1 Chain A
//     WrdsWiFiSarTxPowerLimit2,  // WiFi SAR Tx Power Limit for 5150-5350 MHz Set 1 Chain A
//     WrdsWiFiSarTxPowerLimit3,  // WiFi SAR Tx Power Limit for 5350-5470 MHz Set 1 Chain A
//     WrdsWiFiSarTxPowerLimit4,  // WiFi SAR Tx Power Limit for 5470-5725 MHz Set 1 Chain A
//     WrdsWiFiSarTxPowerLimit5,  // WiFi SAR Tx Power Limit for 5725-5925 MHz Set 1 Chain A
//     WrdsWiFiSarTxPowerLimit6,  // WiFi SAR Tx Power Limit for 5945-6165 MHz Set 1 Chain A
//     WrdsWiFiSarTxPowerLimit7,  // WiFi SAR Tx Power Limit for 6165-6405 MHz Set 1 Chain A
//     WrdsWiFiSarTxPowerLimit8,  // WiFi SAR Tx Power Limit for 6405-6525 MHz Set 1 Chain A
//     WrdsWiFiSarTxPowerLimit9,  // WiFi SAR Tx Power Limit for 6525-6705 MHz Set 1 Chain A
//     WrdsWiFiSarTxPowerLimit10, // WiFi SAR Tx Power Limit for 6705-6865 MHz Set 1 Chain A
//     WrdsWiFiSarTxPowerLimit11, // WiFi SAR Tx Power Limit for 6865-7105 MHz Set 1 Chain A
//     WrdsWiFiSarTxPowerLimit12, // WiFi SAR Tx Power Limit for 2400 MHz Set 1 Chain B
//     WrdsWiFiSarTxPowerLimit13, // WiFi SAR Tx Power Limit for 5150-5350 MHz Set 1 Chain B
//     WrdsWiFiSarTxPowerLimit14, // WiFi SAR Tx Power Limit for 5350-5470 MHz Set 1 Chain B
//     WrdsWiFiSarTxPowerLimit15, // WiFi SAR Tx Power Limit for 5470-5725 MHz Set 1 Chain B
//     WrdsWiFiSarTxPowerLimit16, // WiFi SAR Tx Power Limit for 5725-5925 MHz Set 1 Chain B
//     WrdsWiFiSarTxPowerLimit17, // WiFi SAR Tx Power Limit for 5945-6165 MHz Set 1 Chain B
//     WrdsWiFiSarTxPowerLimit18, // WiFi SAR Tx Power Limit for 6165-6405 MHz Set 1 Chain B
//     WrdsWiFiSarTxPowerLimit19, // WiFi SAR Tx Power Limit for 6405-6525 MHz Set 1 Chain B
//     WrdsWiFiSarTxPowerLimit20, // WiFi SAR Tx Power Limit for 6525-6705 MHz Set 1 Chain B
//     WrdsWiFiSarTxPowerLimit21, // WiFi SAR Tx Power Limit for 6705-6865 MHz Set 1 Chain B
//     WrdsWiFiSarTxPowerLimit22, // WiFi SAR Tx Power Limit for 6865-7105 MHz Set 1 Chain B

//     WrdsCdbWiFiSarTxPowerLimit23, // CDB Mode WiFi SAR Tx Power Limit for 2400 MHz Set 1 Chain A
//     WrdsCdbWiFiSarTxPowerLimit24, // CDB Mode WiFi SAR Tx Power Limit for 5150-5350 MHz Set 1 Chain A
//     WrdsCdbWiFiSarTxPowerLimit25, // CDB Mode WiFi SAR Tx Power Limit for 5350-5470 MHz Set 1 Chain A
//     WrdsCdbWiFiSarTxPowerLimit26, // CDB Mode WiFi SAR Tx Power Limit for 5470-5725 MHz Set 1 Chain A
//     WrdsCdbWiFiSarTxPowerLimit27, // CDB Mode WiFi SAR Tx Power Limit for 5725-5925 MHz Set 1 Chain A
//     WrdsCdbWiFiSarTxPowerLimit28, // CDB Mode WiFi SAR Tx Power Limit for 5945-6165 MHz Set 1 Chain A
//     WrdsCdbWiFiSarTxPowerLimit29, // CDB Mode WiFi SAR Tx Power Limit for 6165-6405 MHz Set 1 Chain A
//     WrdsCdbWiFiSarTxPowerLimit30, // CDB Mode WiFi SAR Tx Power Limit for 6405-6525 MHz Set 1 Chain A
//     WrdsCdbWiFiSarTxPowerLimit31, // CDB Mode WiFi SAR Tx Power Limit for 6525-6705 MHz Set 1 Chain A
//     WrdsCdbWiFiSarTxPowerLimit32, // CDB Mode WiFi SAR Tx Power Limit for 6705-6865 MHz Set 1 Chain A
//     WrdsCdbWiFiSarTxPowerLimit33, // CDB Mode WiFi SAR Tx Power Limit for 6865-7105 MHz Set 1 Chain A
//     WrdsCdbWiFiSarTxPowerLimit34, // CDB Mode WiFi SAR Tx Power Limit for 2400 MHz Set 1 Chain B
//     WrdsCdbWiFiSarTxPowerLimit35, // CDB Mode WiFi SAR Tx Power Limit for 5150-5350 MHz Set 1 Chain B
//     WrdsCdbWiFiSarTxPowerLimit36, // CDB Mode WiFi SAR Tx Power Limit for 5350-5470 MHz Set 1 Chain B
//     WrdsCdbWiFiSarTxPowerLimit37, // CDB Mode WiFi SAR Tx Power Limit for 5470-5725 MHz Set 1 Chain B
//     WrdsCdbWiFiSarTxPowerLimit38, // CDB Mode WiFi SAR Tx Power Limit for 5725-5925 MHz Set 1 Chain B
//     WrdsCdbWiFiSarTxPowerLimit39, // CDB Mode WiFi SAR Tx Power Limit for 5945-6165 MHz Set 1 Chain B
//     WrdsCdbWiFiSarTxPowerLimit40, // CDB Mode WiFi SAR Tx Power Limit for 6165-6405 MHz Set 1 Chain B
//     WrdsCdbWiFiSarTxPowerLimit41, // CDB Mode WiFi SAR Tx Power Limit for 6405-6525 MHz Set 1 Chain B
//     WrdsCdbWiFiSarTxPowerLimit42, // CDB Mode WiFi SAR Tx Power Limit for 6525-6705 MHz Set 1 Chain B
//     WrdsCdbWiFiSarTxPowerLimit43, // CDB Mode WiFi SAR Tx Power Limit for 6705-6865 MHz Set 1 Chain B
//     WrdsCdbWiFiSarTxPowerLimit44, // CDB Mode WiFi SAR Tx Power Limit for 6865-7105 MHz Set 1 Chain B
//   }
// })                         // End of WRDS object
//

Method(WRDS,0,Serialized)
{
  Store(\STXE,Index (DeRefOf (Index (WRDY, 1)), 1))  // WiFi SAR Enable/Disable
  Store(\ST10,Index (DeRefOf (Index (WRDY, 1)), 2))  // WiFi SAR Tx Power Limit for 2400 MHz Set 1 Chain A
  Store(\ST11,Index (DeRefOf (Index (WRDY, 1)), 3))  // WiFi SAR Tx Power Limit for 5150-5350 MHz Set 1 Chain A
  Store(\ST12,Index (DeRefOf (Index (WRDY, 1)), 4))  // WiFi SAR Tx Power Limit for 5350-5470 MHz Set 1 Chain A
  Store(\ST13,Index (DeRefOf (Index (WRDY, 1)), 5))  // WiFi SAR Tx Power Limit for 5470-5725 MHz Set 1 Chain A
  Store(\ST14,Index (DeRefOf (Index (WRDY, 1)), 6))  // WiFi SAR Tx Power Limit for 5725-5945 MHz Set 1 Chain A
  Store(\ST15,Index (DeRefOf (Index (WRDY, 1)), 7))  // WiFi SAR Tx Power Limit for 5945-6165 MHz Set 1 Chain A
  Store(\ST16,Index (DeRefOf (Index (WRDY, 1)), 8))  // WiFi SAR Tx Power Limit for 6165-6405 MHz Set 1 Chain A
  Store(\ST17,Index (DeRefOf (Index (WRDY, 1)), 9))  // WiFi SAR Tx Power Limit for 6405-6525 MHz Set 1 Chain A
  Store(\ST18,Index (DeRefOf (Index (WRDY, 1)), 10)) // WiFi SAR Tx Power Limit for 6525-6705 MHz Set 1 Chain A
  Store(\ST19,Index (DeRefOf (Index (WRDY, 1)), 11)) // WiFi SAR Tx Power Limit for 6705-6865 MHz Set 1 Chain A
  Store(\ST50,Index (DeRefOf (Index (WRDY, 1)), 12)) // WiFi SAR Tx Power Limit for 6865-7105 MHz Set 1 Chain A

  Store(\ST51,Index (DeRefOf (Index (WRDY, 1)), 13)) // WiFi SAR Tx Power Limit for 2400 MHz Set 1 Chain B
  Store(\ST52,Index (DeRefOf (Index (WRDY, 1)), 14)) // WiFi SAR Tx Power Limit for 5150-5350 MHz Set 1 Chain B
  Store(\ST53,Index (DeRefOf (Index (WRDY, 1)), 15)) // WiFi SAR Tx Power Limit for 5350-5470 MHz Set 1 Chain B
  Store(\ST54,Index (DeRefOf (Index (WRDY, 1)), 16)) // WiFi SAR Tx Power Limit for 5470-5725 MHz Set 1 Chain B
  Store(\ST55,Index (DeRefOf (Index (WRDY, 1)), 17)) // WiFi SAR Tx Power Limit for 5725-5925 MHz Set 1 Chain B
  Store(\ST56,Index (DeRefOf (Index (WRDY, 1)), 18)) // WiFi SAR Tx Power Limit for 5945-6165 MHz Set 1 Chain B
  Store(\ST57,Index (DeRefOf (Index (WRDY, 1)), 19)) // WiFi SAR Tx Power Limit for 6165-6405 MHz Set 1 Chain B
  Store(\ST58,Index (DeRefOf (Index (WRDY, 1)), 20)) // WiFi SAR Tx Power Limit for 6405-6525 MHz Set 1 Chain B
  Store(\ST59,Index (DeRefOf (Index (WRDY, 1)), 21)) // WiFi SAR Tx Power Limit for 6525-6705 MHz Set 1 Chain B
  Store(\ST5A,Index (DeRefOf (Index (WRDY, 1)), 22)) // WiFi SAR Tx Power Limit for 6705-6865 MHz Set 1 Chain B
  Store(\ST5B,Index (DeRefOf (Index (WRDY, 1)), 23)) // WiFi SAR Tx Power Limit for 6865-7105 MHz Set 1 Chain B

  Store(\CD10,Index (DeRefOf (Index (WRDY, 1)), 24))  // CDB Mode WiFi SAR Tx Power Limit for 2400 MHz Set 1 Chain A
  Store(\CD11,Index (DeRefOf (Index (WRDY, 1)), 25))  // CDB Mode WiFi SAR Tx Power Limit for 5150-5350 MHz Set 1 Chain A
  Store(\CD12,Index (DeRefOf (Index (WRDY, 1)), 26))  // CDB Mode WiFi SAR Tx Power Limit for 5350-5470 MHz Set 1 Chain A
  Store(\CD13,Index (DeRefOf (Index (WRDY, 1)), 27))  // CDB Mode WiFi SAR Tx Power Limit for 5470-5725 MHz Set 1 Chain A
  Store(\CD14,Index (DeRefOf (Index (WRDY, 1)), 28))  // CDB Mode WiFi SAR Tx Power Limit for 5725-5945 MHz Set 1 Chain A
  Store(\CD15,Index (DeRefOf (Index (WRDY, 1)), 29))  // CDB Mode WiFi SAR Tx Power Limit for 5945-6165 MHz Set 1 Chain A
  Store(\CD16,Index (DeRefOf (Index (WRDY, 1)), 30))  // CDB Mode WiFi SAR Tx Power Limit for 6165-6405 MHz Set 1 Chain A
  Store(\CD17,Index (DeRefOf (Index (WRDY, 1)), 31))  // CDB Mode WiFi SAR Tx Power Limit for 6405-6525 MHz Set 1 Chain A
  Store(\CD18,Index (DeRefOf (Index (WRDY, 1)), 32))  // CDB Mode WiFi SAR Tx Power Limit for 6525-6705 MHz Set 1 Chain A
  Store(\CD19,Index (DeRefOf (Index (WRDY, 1)), 33))  // CDB Mode WiFi SAR Tx Power Limit for 6705-6865 MHz Set 1 Chain A
  Store(\CD1A,Index (DeRefOf (Index (WRDY, 1)), 34))  // CDB Mode WiFi SAR Tx Power Limit for 6865-7105 MHz Set 1 Chain A

  Store(\CD20,Index (DeRefOf (Index (WRDY, 1)), 35))  // CDB Mode WiFi SAR Tx Power Limit for 2400 MHz Set 1 Chain B
  Store(\CD21,Index (DeRefOf (Index (WRDY, 1)), 36))  // CDB Mode WiFi SAR Tx Power Limit for 5150-5350 MHz Set 1 Chain B
  Store(\CD22,Index (DeRefOf (Index (WRDY, 1)), 37))  // CDB Mode WiFi SAR Tx Power Limit for 5350-5470 MHz Set 1 Chain B
  Store(\CD23,Index (DeRefOf (Index (WRDY, 1)), 38))  // CDB Mode WiFi SAR Tx Power Limit for 5470-5725 MHz Set 1 Chain B
  Store(\CD24,Index (DeRefOf (Index (WRDY, 1)), 39))  // CDB Mode WiFi SAR Tx Power Limit for 5725-5925 MHz Set 1 Chain B
  Store(\CD25,Index (DeRefOf (Index (WRDY, 1)), 40))  // CDB Mode WiFi SAR Tx Power Limit for 5945-6165 MHz Set 1 Chain B
  Store(\CD26,Index (DeRefOf (Index (WRDY, 1)), 41))  // CDB Mode WiFi SAR Tx Power Limit for 6165-6405 MHz Set 1 Chain B
  Store(\CD27,Index (DeRefOf (Index (WRDY, 1)), 42))  // CDB Mode WiFi SAR Tx Power Limit for 6405-6525 MHz Set 1 Chain B
  Store(\CD28,Index (DeRefOf (Index (WRDY, 1)), 43))  // CDB Mode WiFi SAR Tx Power Limit for 6525-6705 MHz Set 1 Chain B
  Store(\CD29,Index (DeRefOf (Index (WRDY, 1)), 44))  // CDB Mode WiFi SAR Tx Power Limit for 6705-6865 MHz Set 1 Chain B
  Store(\CD2A,Index (DeRefOf (Index (WRDY, 1)), 45))  // CDb Mode WiFi SAR Tx Power Limit for 6865-7105 MHz Set 1 Chain B

  Return(WRDY)
}

Name (EWRY, Package()
{
  2,                      // Revision, DWordConst, 2 - CDB Support
  Package()               // Configuration for WiFi
  {
    0x07,                 // DomainType, 0x7:WiFi
    0x80,                 // WiFi Dynamic SAR Enable/Disable
    0x80,                 // WiFi SAR Number of Optional added SAR table sets to be used
    0x80,                 // WiFi SAR Tx Power Limit for 2400 MHz Set 2 Chain A
    0x80,                 // WiFi SAR Tx Power Limit for 5150-5350 MHz Set 2 Chain A
    0x80,                 // WiFi SAR Tx Power Limit for 5350-5470 MHz Set 2 Chain A
    0x80,                 // WiFi SAR Tx Power Limit for 5470-5725 MHz Set 2 Chain A
    0x80,                 // WiFi SAR Tx Power Limit for 5725-5925 MHz Set 2 Chain A
    0x80,                 // WiFi SAR Tx Power Limit for 5945-6165 MHz Set 2 Chain A
    0x80,                 // WiFi SAR Tx Power Limit for 6165-6405 MHz Set 2 Chain A
    0x80,                 // WiFi SAR Tx Power Limit for 6405-6525 MHz Set 2 Chain A
    0x80,                 // WiFi SAR Tx Power Limit for 6525-6705 MHz Set 2 Chain A
    0x80,                 // WiFi SAR Tx Power Limit for 6705-6865 MHz Set 2 Chain A
    0x80,                 // WiFi SAR Tx Power Limit for 6865-7105 MHz Set 2 Chain A
    0x80,                 // WiFi SAR Tx Power Limit for 2400 MHz Set 2 Chain B
    0x80,                 // WiFi SAR Tx Power Limit for 5150-5350 MHz Set 2 Chain B
    0x80,                 // WiFi SAR Tx Power Limit for 5350-5470 MHz Set 2 Chain B
    0x80,                 // WiFi SAR Tx Power Limit for 5470-5725 MHz Set 2 Chain B
    0x80,                 // WiFi SAR Tx Power Limit for 5725-5925 MHz Set 2 Chain B
    0x80,                 // WiFi SAR Tx Power Limit for 5945-6165 MHz Set 2 Chain B
    0x80,                 // WiFi SAR Tx Power Limit for 6165-6405 MHz Set 2 Chain B
    0x80,                 // WiFi SAR Tx Power Limit for 6405-6525 MHz Set 2 Chain B
    0x80,                 // WiFi SAR Tx Power Limit for 6525-6705 MHz Set 2 Chain B
    0x80,                 // WiFi SAR Tx Power Limit for 6705-6865 MHz Set 2 Chain B
    0x80,                 // WiFi SAR Tx Power Limit for 6865-7105 MHz Set 2 Chain B
    0x80,                 // WiFi SAR Tx Power Limit for 2400 MHz Set 3 Chain A
    0x80,                 // WiFi SAR Tx Power Limit for 5150-5350 MHz Set 3 Chain A
    0x80,                 // WiFi SAR Tx Power Limit for 5350-5470 MHz Set 3 Chain A
    0x80,                 // WiFi SAR Tx Power Limit for 5470-5725 MHz Set 3 Chain A
    0x80,                 // WiFi SAR Tx Power Limit for 5725-5925 MHz Set 3 Chain A
    0x80,                 // WiFi SAR Tx Power Limit for 5945-6165 MHz Set 3 Chain A
    0x80,                 // WiFi SAR Tx Power Limit for 6165-6405 MHz Set 3 Chain A
    0x80,                 // WiFi SAR Tx Power Limit for 6405-6525 MHz Set 3 Chain A
    0x80,                 // WiFi SAR Tx Power Limit for 6525-6705 MHz Set 3 Chain A
    0x80,                 // WiFi SAR Tx Power Limit for 6705-6865 MHz Set 3 Chain A
    0x80,                 // WiFi SAR Tx Power Limit for 6865-7105 MHz Set 3 Chain A
    0x80,                 // WiFi SAR Tx Power Limit for 2400 MHz Set 3 Chain B
    0x80,                 // WiFi SAR Tx Power Limit for 5150-5350 MHz Set 3 Chain B
    0x80,                 // WiFi SAR Tx Power Limit for 5350-5470 MHz Set 3 Chain B
    0x80,                 // WiFi SAR Tx Power Limit for 5470-5725 MHz Set 3 Chain B
    0x80,                 // WiFi SAR Tx Power Limit for 5725-5925 MHz Set 3 Chain B
    0x80,                 // WiFi SAR Tx Power Limit for 5945-6165 MHz Set 3 Chain B
    0x80,                 // WiFi SAR Tx Power Limit for 6165-6405 MHz Set 3 Chain B
    0x80,                 // WiFi SAR Tx Power Limit for 6405-6525 MHz Set 3 Chain B
    0x80,                 // WiFi SAR Tx Power Limit for 6525-6705 MHz Set 3 Chain B
    0x80,                 // WiFi SAR Tx Power Limit for 6705-6865 MHz Set 3 Chain B
    0x80,                 // WiFi SAR Tx Power Limit for 6865-7105 MHz Set 3 Chain B
    0x80,                 // WiFi SAR Tx Power Limit for 2400 MHz Set 4 Chain A
    0x80,                 // WiFi SAR Tx Power Limit for 5150-5350 MHz Set 4 Chain A
    0x80,                 // WiFi SAR Tx Power Limit for 5350-5470 MHz Set 4 Chain A
    0x80,                 // WiFi SAR Tx Power Limit for 5470-5725 MHz Set 4 Chain A
    0x80,                 // WiFi SAR Tx Power Limit for 5725-5945 MHz Set 4 Chain A
    0x80,                 // WiFi SAR Tx Power Limit for 5945-6165 MHz Set 4 Chain A
    0x80,                 // WiFi SAR Tx Power Limit for 6165-6405 MHz Set 4 Chain A
    0x80,                 // WiFi SAR Tx Power Limit for 6405-6525 MHz Set 4 Chain A
    0x80,                 // WiFi SAR Tx Power Limit for 6525-6705 MHz Set 4 Chain A
    0x80,                 // WiFi SAR Tx Power Limit for 6705-6865 MHz Set 4 Chain A
    0x80,                 // WiFi SAR Tx Power Limit for 6865-7105 MHz Set 4 Chain A
    0x80,                 // WiFi SAR Tx Power Limit for 2400 MHz Set 4 Chain B
    0x80,                 // WiFi SAR Tx Power Limit for 5150-5350 MHz Set 4 Chain B
    0x80,                 // WiFi SAR Tx Power Limit for 5350-5470 MHz Set 4 Chain B
    0x80,                 // WiFi SAR Tx Power Limit for 5470-5725 MHz Set 4 Chain B
    0x80,                 // WiFi SAR Tx Power Limit for 5725-5945 MHz Set 4 Chain B
    0x80,                 // WiFi SAR Tx Power Limit for 5945-6165 MHz Set 4 Chain B
    0x80,                 // WiFi SAR Tx Power Limit for 6165-6405 MHz Set 4 Chain B
    0x80,                 // WiFi SAR Tx Power Limit for 6405-6525 MHz Set 4 Chain B
    0x80,                 // WiFi SAR Tx Power Limit for 6525-6705 MHz Set 4 Chain B
    0x80,                 // WiFi SAR Tx Power Limit for 6705-6865 MHz Set 4 Chain B
    0x80,                 // WiFi SAR Tx Power Limit for 6865-7105 MHz Set 4 Chain B
    0x80,                 // CDB Mode WiFi SAR Tx Power Limit for 2400 MHz Set 2 Chain A
    0x80,                 // CDB Mode WiFi SAR Tx Power Limit for 5150-5350 MHz Set 2 Chain A
    0x80,                 // CDB Mode WiFi SAR Tx Power Limit for 5350-5470 MHz Set 2 Chain A
    0x80,                 // CDB Mode WiFi SAR Tx Power Limit for 5470-5725 MHz Set 2 Chain A
    0x80,                 // CDB Mode WiFi SAR Tx Power Limit for 5725-5925 MHz Set 2 Chain A
    0x80,                 // CDB Mode WiFi SAR Tx Power Limit for 5945-6165 MHz Set 2 Chain A
    0x80,                 // CDB Mode WiFi SAR Tx Power Limit for 6165-6405 MHz Set 2 Chain A
    0x80,                 // CDB Mode WiFi SAR Tx Power Limit for 6405-6525 MHz Set 2 Chain A
    0x80,                 // CDB Mode WiFi SAR Tx Power Limit for 6525-6705 MHz Set 2 Chain A
    0x80,                 // CDB Mode WiFi SAR Tx Power Limit for 6705-6865 MHz Set 2 Chain A
    0x80,                 // CDB Mode WiFi SAR Tx Power Limit for 6865-7105 MHz Set 2 Chain A
    0x80,                 // CDB Mode WiFi SAR Tx Power Limit for 2400 MHz Set 2 Chain B
    0x80,                 // CDB Mode WiFi SAR Tx Power Limit for 5150-5350 MHz Set 2 Chain B
    0x80,                 // CDB Mode WiFi SAR Tx Power Limit for 5350-5470 MHz Set 2 Chain B
    0x80,                 // CDB Mode WiFi SAR Tx Power Limit for 5470-5725 MHz Set 2 Chain B
    0x80,                 // CDB Mode WiFi SAR Tx Power Limit for 5725-5925 MHz Set 2 Chain B
    0x80,                 // CDB Mode WiFi SAR Tx Power Limit for 5945-6165 MHz Set 2 Chain B
    0x80,                 // CDB Mode WiFi SAR Tx Power Limit for 6165-6405 MHz Set 2 Chain B
    0x80,                 // CDB Mode WiFi SAR Tx Power Limit for 6405-6525 MHz Set 2 Chain B
    0x80,                 // CDB Mode WiFi SAR Tx Power Limit for 6525-6705 MHz Set 2 Chain B
    0x80,                 // CDB Mode WiFi SAR Tx Power Limit for 6705-6865 MHz Set 2 Chain B
    0x80,                 // CDB Mode WiFi SAR Tx Power Limit for 6865-7105 MHz Set 2 Chain B
    0x80,                 // CDB Mode WiFi SAR Tx Power Limit for 2400 MHz Set 3 Chain A
    0x80,                 // CDB Mode WiFi SAR Tx Power Limit for 5150-5350 MHz Set 3 Chain A
    0x80,                 // CDB Mode WiFi SAR Tx Power Limit for 5350-5470 MHz Set 3 Chain A
    0x80,                 // CDB Mode WiFi SAR Tx Power Limit for 5470-5725 MHz Set 3 Chain A
    0x80,                 // CDB Mode WiFi SAR Tx Power Limit for 5725-5925 MHz Set 3 Chain A
    0x80,                 // CDB Mode WiFi SAR Tx Power Limit for 5945-6165 MHz Set 3 Chain A
    0x80,                 // CDB Mode WiFi SAR Tx Power Limit for 6165-6405 MHz Set 3 Chain A
    0x80,                 // CDB Mode WiFi SAR Tx Power Limit for 6405-6525 MHz Set 3 Chain A
    0x80,                 // CDB Mode WiFi SAR Tx Power Limit for 6525-6705 MHz Set 3 Chain A
    0x80,                 // CDB Mode WiFi SAR Tx Power Limit for 6705-6865 MHz Set 3 Chain A
    0x80,                 // CDB Mode WiFi SAR Tx Power Limit for 6865-7105 MHz Set 3 Chain A
    0x80,                 // CDB Mode WiFi SAR Tx Power Limit for 2400 MHz Set 3 Chain B
    0x80,                 // CDB Mode WiFi SAR Tx Power Limit for 5150-5350 MHz Set 3 Chain B
    0x80,                 // CDB Mode WiFi SAR Tx Power Limit for 5350-5470 MHz Set 3 Chain B
    0x80,                 // CDB Mode WiFi SAR Tx Power Limit for 5470-5725 MHz Set 3 Chain B
    0x80,                 // CDB Mode WiFi SAR Tx Power Limit for 5725-5925 MHz Set 3 Chain B
    0x80,                 // CDB Mode WiFi SAR Tx Power Limit for 5945-6165 MHz Set 3 Chain B
    0x80,                 // CDB Mode WiFi SAR Tx Power Limit for 6165-6405 MHz Set 3 Chain B
    0x80,                 // CDB Mode WiFi SAR Tx Power Limit for 6405-6525 MHz Set 3 Chain B
    0x80,                 // CDB Mode WiFi SAR Tx Power Limit for 6525-6705 MHz Set 3 Chain B
    0x80,                 // CDB Mode WiFi SAR Tx Power Limit for 6705-6865 MHz Set 3 Chain B
    0x80,                 // CDB Mode WiFi SAR Tx Power Limit for 6865-7105 MHz Set 3 Chain B
    0x80,                 // CDB Mode WiFi SAR Tx Power Limit for 2400 MHz Set 4 Chain A
    0x80,                 // CDB Mode WiFi SAR Tx Power Limit for 5150-5350 MHz Set 4 Chain A
    0x80,                 // CDB Mode WiFi SAR Tx Power Limit for 5350-5470 MHz Set 4 Chain A
    0x80,                 // CDB Mode WiFi SAR Tx Power Limit for 5470-5725 MHz Set 4 Chain A
    0x80,                 // CDB Mode WiFi SAR Tx Power Limit for 5725-5945 MHz Set 4 Chain A
    0x80,                 // CDB Mode WiFi SAR Tx Power Limit for 5945-6165 MHz Set 4 Chain A
    0x80,                 // CDB Mode WiFi SAR Tx Power Limit for 6165-6405 MHz Set 4 Chain A
    0x80,                 // CDB mode WiFi SAR Tx Power Limit for 6405-6525 MHz Set 4 Chain A
    0x80,                 // CDB Mode WiFi SAR Tx Power Limit for 6525-6705 MHz Set 4 Chain A
    0x80,                 // CDB Mode WiFi SAR Tx Power Limit for 6705-6865 MHz Set 4 Chain A
    0x80,                 // CDB Mode WiFi SAR Tx Power Limit for 6865-7105 MHz Set 4 Chain A
    0x80,                 // CDB Mode WiFi SAR Tx Power Limit for 2400 MHz Set 4 Chain B
    0x80,                 // CDB Mode WiFi SAR Tx Power Limit for 5150-5350 MHz Set 4 Chain B
    0x80,                 // CDB Mode WiFi SAR Tx Power Limit for 5350-5470 MHz Set 4 Chain B
    0x80,                 // CDB Mode WiFi SAR Tx Power Limit for 5470-5725 MHz Set 4 Chain B
    0x80,                 // CDB Mode WiFi SAR Tx Power Limit for 5725-5945 MHz Set 4 Chain B
    0x80,                 // CDB Mode WiFi SAR Tx Power Limit for 5945-6165 MHz Set 4 Chain B
    0x80,                 // CDB Mode WiFi SAR Tx Power Limit for 6165-6405 MHz Set 4 Chain B
    0x80,                 // CDB Mode WiFi SAR Tx Power Limit for 6405-6525 MHz Set 4 Chain B
    0x80,                 // CDB Mode WiFi SAR Tx Power Limit for 6525-6705 MHz Set 4 Chain B
    0x80,                 // CDB Mode WiFi SAR Tx Power Limit for 6705-6865 MHz Set 4 Chain B
    0x80,                 // CDB Mode WiFi SAR Tx Power Limit for 6865-7105 MHz Set 4 Chain B
  }
})                        // End of EWRY object

// EWRD (Extended Wireless Regulatory Domain SAR)
//
// WiFi has regulatory limitations which prohibit or allow usage of certain bands or channels as well as limiting the Tx power.
// This method is used to modify the WiFi SAR Tx Power Limit to properly control it
// This adds Three more Sets in addition to the Set defined in WRDS
//
// Arguments: (0)
//   None
// Return Value:
//
//Name (EWRY, Package()
//{
//  Revision,                                       // Revision, DWordConst
//  Package()                                       // Configuration for WiFi
//  {
//    DomainType,                                   // DomainType, 0x7:WiFi
//    EwrdWiFiDynamicSarEnable,                     // WiFi Dynamic SAR Enable/Disable
//    EwrdWiFiDynamicSarRangeSets,                  // WiFi SAR Number of Optional added SAR table sets to be used
//    EwrdWiFiSarTxPowerSet2Limit1,                 // WiFi SAR Tx Power Limit for 2400 MHz Set 2 Chain A
//    EwrdWiFiSarTxPowerSet2Limit2,                 // WiFi SAR Tx Power Limit for 5150-5350 MHz Set 2 Chain A
//    EwrdWiFiSarTxPowerSet2Limit3,                 // WiFi SAR Tx Power Limit for 5350-5470 MHz Set 2 Chain A
//    EwrdWiFiSarTxPowerSet2Limit4,                 // WiFi SAR Tx Power Limit for 5470-5725 MHz Set 2 Chain A
//    EwrdWiFiSarTxPowerSet2Limit5,                 // WiFi SAR Tx Power Limit for 5725-5925 MHz Set 2 Chain A
//    EwrdWiFiSarTxPowerSet2Limit6,                 // WiFi SAR Tx Power Limit for 5945-6165 MHz Set 2 Chain A
//    EwrdWiFiSarTxPowerSet2Limit7,                 // WiFi SAR Tx Power Limit for 6165-6405 MHz Set 2 Chain A
//    EwrdWiFiSarTxPowerSet2Limit8,                 // WiFi SAR Tx Power Limit for 6405-6525 MHz Set 2 Chain A
//    EwrdWiFiSarTxPowerSet2Limit9,                 // WiFi SAR Tx Power Limit for 6525-6705 MHz Set 2 Chain A
//    EwrdWiFiSarTxPowerSet2Limit10,                // WiFi SAR Tx Power Limit for 6705-6865 MHz Set 2 Chain A
//    EwrdWiFiSarTxPowerSet2Limit11,                // WiFi SAR Tx Power Limit for 6865-7105 MHz Set 2 Chain A
//    EwrdWiFiSarTxPowerSet2Limit12,                // WiFi SAR Tx Power Limit for 2400 MHz Set 2 Chain B
//    EwrdWiFiSarTxPowerSet2Limit13,                // WiFi SAR Tx Power Limit for 5150-5350 MHz Set 2 Chain B
//    EwrdWiFiSarTxPowerSet2Limit14,                // WiFi SAR Tx Power Limit for 5350-5470 MHz Set 2 Chain B
//    EwrdWiFiSarTxPowerSet2Limit15,                // WiFi SAR Tx Power Limit for 5470-5725 MHz Set 2 Chain B
//    EwrdWiFiSarTxPowerSet2Limit16,                // WiFi SAR Tx Power Limit for 5725-5925 MHz Set 2 Chain B
//    EwrdWiFiSarTxPowerSet2Limit17,                // WiFi SAR Tx Power Limit for 5945-6165 MHz Set 2 Chain B
//    EwrdWiFiSarTxPowerSet2Limit18,                // WiFi SAR Tx Power Limit for 6165-6405 MHz Set 2 Chain B
//    EwrdWiFiSarTxPowerSet2Limit19,                // WiFi SAR Tx Power Limit for 6405-6525 MHz Set 2 Chain B
//    EwrdWiFiSarTxPowerSet2Limit20,                // WiFi SAR Tx Power Limit for 6525-6705 MHz Set 2 Chain B
//    EwrdWiFiSarTxPowerSet2Limit21,                // WiFi SAR Tx Power Limit for 6705-6865 MHz Set 2 Chain B
//    EwrdWiFiSarTxPowerSet2Limit22,                // WiFi SAR Tx Power Limit for 6865-7105 MHz Set 2 Chain B
//    EwrdWiFiSarTxPowerSet3Limit1,                 // WiFi SAR Tx Power Limit for 2400 MHz Set 3 Chain A
//    EwrdWiFiSarTxPowerSet3Limit2,                 // WiFi SAR Tx Power Limit for 5150-5350 MHz Set 3 Chain A
//    EwrdWiFiSarTxPowerSet3Limit3,                 // WiFi SAR Tx Power Limit for 5350-5470 MHz Set 3 Chain A
//    EwrdWiFiSarTxPowerSet3Limit4,                 // WiFi SAR Tx Power Limit for 5470-5725 MHz Set 3 Chain A
//    EwrdWiFiSarTxPowerSet3Limit5,                 // WiFi SAR Tx Power Limit for 5725-5925 MHz Set 3 Chain A
//    EwrdWiFiSarTxPowerSet3Limit6,                 // WiFi SAR Tx Power Limit for 5945-6165 MHz Set 3 Chain A
//    EwrdWiFiSarTxPowerSet3Limit7,                 // WiFi SAR Tx Power Limit for 6165-6405 MHz Set 3 Chain A
//    EwrdWiFiSarTxPowerSet3Limit8,                 // WiFi SAR Tx Power Limit for 6405-6525 MHz Set 3 Chain A
//    EwrdWiFiSarTxPowerSet3Limit9,                 // WiFi SAR Tx Power Limit for 6525-6705 MHz Set 3 Chain A
//    EwrdWiFiSarTxPowerSet3Limit10,                // WiFi SAR Tx Power Limit for 6705-6865 MHz Set 3 Chain A
//    EwrdWiFiSarTxPowerSet3Limit11,                // WiFi SAR Tx Power Limit for 6865-7105 MHz Set 3 Chain A
//    EwrdWiFiSarTxPowerSet3Limit12,                // WiFi SAR Tx Power Limit for 2400 MHz Set 3 Chain B
//    EwrdWiFiSarTxPowerSet3Limit13,                // WiFi SAR Tx Power Limit for 5150-5350 MHz Set 3 Chain B
//    EwrdWiFiSarTxPowerSet3Limit14,                // WiFi SAR Tx Power Limit for 5350-5470 MHz Set 3 Chain B
//    EwrdWiFiSarTxPowerSet3Limit15,                // WiFi SAR Tx Power Limit for 5470-5725 MHz Set 3 Chain B
//    EwrdWiFiSarTxPowerSet3Limit16,                // WiFi SAR Tx Power Limit for 5725-5925 MHz Set 3 Chain B
//    EwrdWiFiSarTxPowerSet3Limit17,                // WiFi SAR Tx Power Limit for 5945-6165 MHz Set 3 Chain B
//    EwrdWiFiSarTxPowerSet3Limit18,                // WiFi SAR Tx Power Limit for 6165-6405 MHz Set 3 Chain B
//    EwrdWiFiSarTxPowerSet3Limit19,                // WiFi SAR Tx Power Limit for 6405-6525 MHz Set 3 Chain B
//    EwrdWiFiSarTxPowerSet3Limit20,                // WiFi SAR Tx Power Limit for 6525-6705 MHz Set 3 Chain B
//    EwrdWiFiSarTxPowerSet3Limit21,                // WiFi SAR Tx Power Limit for 6705-6865 MHz Set 3 Chain B
//    EwrdWiFiSarTxPowerSet3Limit22,                // WiFi SAR Tx Power Limit for 6865-7105 MHz Set 3 Chain B
//    EwrdWiFiSarTxPowerSet4Limit1,                 // WiFi SAR Tx Power Limit for 2400 MHz Set 4 Chain A
//    EwrdWiFiSarTxPowerSet4Limit2,                 // WiFi SAR Tx Power Limit for 5150-5350 MHz Set 4 Chain A
//    EwrdWiFiSarTxPowerSet4Limit3,                 // WiFi SAR Tx Power Limit for 5350-5470 MHz Set 4 Chain A
//    EwrdWiFiSarTxPowerSet4Limit4,                 // WiFi SAR Tx Power Limit for 5470-5725 MHz Set 4 Chain A
//    EwrdWiFiSarTxPowerSet4Limit5,                 // WiFi SAR Tx Power Limit for 5725-5925 MHz Set 4 Chain A
//    EwrdWiFiSarTxPowerSet4Limit6,                 // WiFi SAR Tx Power Limit for 5945-6165 MHz Set 4 Chain A
//    EwrdWiFiSarTxPowerSet4Limit7,                 // WiFi SAR Tx Power Limit for 6165-6405 MHz Set 4 Chain A
//    EwrdWiFiSarTxPowerSet4Limit8,                 // WiFi SAR Tx Power Limit for 6405-6525 MHz Set 4 Chain A
//    EwrdWiFiSarTxPowerSet4Limit9,                 // WiFi SAR Tx Power Limit for 6525-6705 MHz Set 4 Chain A
//    EwrdWiFiSarTxPowerSet4Limit10,                // WiFi SAR Tx Power Limit for 6705-6865 MHz Set 4 Chain A
//    EwrdWiFiSarTxPowerSet4Limit11,                // WiFi SAR Tx Power Limit for 6865-7105 MHz Set 4 Chain A
//    EwrdWiFiSarTxPowerSet4Limit12,                // WiFi SAR Tx Power Limit for 2400 MHz Set 4 Chain B
//    EwrdWiFiSarTxPowerSet4Limit13,                // WiFi SAR Tx Power Limit for 5150-5350 MHz Set 4 Chain B
//    EwrdWiFiSarTxPowerSet4Limit14,                // WiFi SAR Tx Power Limit for 5350-5470 MHz Set 4 Chain B
//    EwrdWiFiSarTxPowerSet4Limit15,                // WiFi SAR Tx Power Limit for 5470-5725 MHz Set 4 Chain B
//    EwrdWiFiSarTxPowerSet4Limit16,                // WiFi SAR Tx Power Limit for 5725-5925 MHz Set 4 Chain B
//    EwrdWiFiSarTxPowerSet4Limit17,                // WiFi SAR Tx Power Limit for 5945-6165 MHz Set 4 Chain B
//    EwrdWiFiSarTxPowerSet4Limit18,                // WiFi SAR Tx Power Limit for 6165-6405 MHz Set 4 Chain B
//    EwrdWiFiSarTxPowerSet4Limit19,                // WiFi SAR Tx Power Limit for 6405-6525 MHz Set 4 Chain B
//    EwrdWiFiSarTxPowerSet4Limit20,                // WiFi SAR Tx Power Limit for 6525-6705 MHz Set 4 Chain B
//    EwrdWiFiSarTxPowerSet4Limit21,                // WiFi SAR Tx Power Limit for 6705-6865 MHz Set 4 Chain B
//    EwrdWiFiSarTxPowerSet4Limit22,                // WiFi SAR Tx Power Limit for 6865-7105 MHz Set 4 Chain B
//
//CDB
//
//    EwrdCdbWiFiSarTxPowerSet2Limit1,               // CDB Mode WiFi SAR Tx Power Limit for 2400 MHz Set 2 Chain A
//    EwrdCdbWiFiSarTxPowerSet2Limit2,               // WiFi SAR Tx Power Limit for 5150-5350 MHz Set 2 Chain A
//    EwrdCdbWiFiSarTxPowerSet2Limit3,               // WiFi SAR Tx Power Limit for 5350-5470 MHz Set 2 Chain A
//    EwrdCdbWiFiSarTxPowerSet2Limit4,               // WiFi SAR Tx Power Limit for 5470-5725 MHz Set 2 Chain A
//    EwrdCbsWiFiSarTxPowerSet2Limit5,               // WiFi SAR Tx Power Limit for 5725-5925 MHz Set 2 Chain A
//    EwrdCdbWiFiSarTxPowerSet2Limit6,               // WiFi SAR Tx Power Limit for 5945-6165 MHz Set 2 Chain A
//    EwrdCdbWiFiSarTxPowerSet2Limit7,               // WiFi SAR Tx Power Limit for 6165-6405 MHz Set 2 Chain A
//    EwrdCdbWiFiSarTxPowerSet2Limit8,               // WiFi SAR Tx Power Limit for 6405-6525 MHz Set 2 Chain A
//    EwrdCdbWiFiSarTxPowerSet2Limit9,               // WiFi SAR Tx Power Limit for 6525-6705 MHz Set 2 Chain A
//    EwrdCdbWiFiSarTxPowerSet2Limit10,              // WiFi SAR Tx Power Limit for 6705-6865 MHz Set 2 Chain A
//    EwrdCdbWiFiSarTxPowerSet2Limit11,              // WiFi SAR Tx Power Limit for 6865-7105 MHz Set 2 Chain A
//    EwrdCdbWiFiSarTxPowerSet2Limit12,              // WiFi SAR Tx Power Limit for 2400 MHz Set 2 Chain B
//    EwrdCdbWiFiSarTxPowerSet2Limit13,              // WiFi SAR Tx Power Limit for 5150-5350 MHz Set 2 Chain B
//    EwrdCdbWiFiSarTxPowerSet2Limit14,              // WiFi SAR Tx Power Limit for 5350-5470 MHz Set 2 Chain B
//    EwrdCdbWiFiSarTxPowerSet2Limit15,              // WiFi SAR Tx Power Limit for 5470-5725 MHz Set 2 Chain B
//    EwrdCdbWiFiSarTxPowerSet2Limit16,              // WiFi SAR Tx Power Limit for 5725-5925 MHz Set 2 Chain B
//    EwrdCdbWiFiSarTxPowerSet2Limit17,              // WiFi SAR Tx Power Limit for 5945-6165 MHz Set 2 Chain B
//    EwrdCdbWiFiSarTxPowerSet2Limit18,              // WiFi SAR Tx Power Limit for 6165-6405 MHz Set 2 Chain B
//    EwrdCdbWiFiSarTxPowerSet2Limit19,              // WiFi SAR Tx Power Limit for 6405-6525 MHz Set 2 Chain B
//    EwrdCdbWiFiSarTxPowerSet2Limit20,              // WiFi SAR Tx Power Limit for 6525-6705 MHz Set 2 Chain B
//    EwrdCdbWiFiSarTxPowerSet2Limit21,              // WiFi SAR Tx Power Limit for 6705-6865 MHz Set 2 Chain B
//    EwrdCdbWiFiSarTxPowerSet2Limit22,              // WiFi SAR Tx Power Limit for 6865-7105 MHz Set 2 Chain B
//    EwrdCdbWiFiSarTxPowerSet3Limit1,               // WiFi SAR Tx Power Limit for 2400 MHz Set 3 Chain A
//    EwrdCdbWiFiSarTxPowerSet3Limit2,               // WiFi SAR Tx Power Limit for 5150-5350 MHz Set 3 Chain A
//    EwrdCdbWiFiSarTxPowerSet3Limit3,               // WiFi SAR Tx Power Limit for 5350-5470 MHz Set 3 Chain A
//    EwrdCdbWiFiSarTxPowerSet3Limit4,               // WiFi SAR Tx Power Limit for 5470-5725 MHz Set 3 Chain A
//    EwrdCdbWiFiSarTxPowerSet3Limit5,               // WiFi SAR Tx Power Limit for 5725-5925 MHz Set 3 Chain A
//    EwrdCdbWiFiSarTxPowerSet3Limit6,               // WiFi SAR Tx Power Limit for 5945-6165 MHz Set 3 Chain A
//    EwrdCdbWiFiSarTxPowerSet3Limit7,               // WiFi SAR Tx Power Limit for 6165-6405 MHz Set 3 Chain A
//    EwrdCdbWiFiSarTxPowerSet3Limit8,               // WiFi SAR Tx Power Limit for 6405-6525 MHz Set 3 Chain A
//    EwrdCdbWiFiSarTxPowerSet3Limit9,               // WiFi SAR Tx Power Limit for 6525-6705 MHz Set 3 Chain A
//    EwrdCdbWiFiSarTxPowerSet3Limit10,              // WiFi SAR Tx Power Limit for 6705-6865 MHz Set 3 Chain A
//    EwrdCdbWiFiSarTxPowerSet3Limit11,              // WiFi SAR Tx Power Limit for 6865-7105 MHz Set 3 Chain A
//    EwrdCdbWiFiSarTxPowerSet3Limit12,              // WiFi SAR Tx Power Limit for 2400 MHz Set 3 Chain B
//    EwrdCdbWiFiSarTxPowerSet3Limit13,              // WiFi SAR Tx Power Limit for 5150-5350 MHz Set 3 Chain B
//    EwrdCdbWiFiSarTxPowerSet3Limit14,              // WiFi SAR Tx Power Limit for 5350-5470 MHz Set 3 Chain B
//    EwrdCdbWiFiSarTxPowerSet3Limit15,              // WiFi SAR Tx Power Limit for 5470-5725 MHz Set 3 Chain B
//    EwrdCdbWiFiSarTxPowerSet3Limit16,              // WiFi SAR Tx Power Limit for 5725-5925 MHz Set 3 Chain B
//    EwrdCdbWiFiSarTxPowerSet3Limit17,              // WiFi SAR Tx Power Limit for 5945-6165 MHz Set 3 Chain B
//    EwrdCdbWiFiSarTxPowerSet3Limit18,              // WiFi SAR Tx Power Limit for 6165-6405 MHz Set 3 Chain B
//    EwrdCdbWiFiSarTxPowerSet3Limit19,              // WiFi SAR Tx Power Limit for 6405-6525 MHz Set 3 Chain B
//    EwrdCdbWiFiSarTxPowerSet3Limit20,              // WiFi SAR Tx Power Limit for 6525-6705 MHz Set 3 Chain B
//    EwrdCdbWiFiSarTxPowerSet3Limit21,              // WiFi SAR Tx Power Limit for 6705-6865 MHz Set 3 Chain B
//    EwrdCdbWiFiSarTxPowerSet3Limit22,              // WiFi SAR Tx Power Limit for 6865-7105 MHz Set 3 Chain B
//    EwrdCdbWiFiSarTxPowerSet4Limit1,               // WiFi SAR Tx Power Limit for 2400 MHz Set 4 Chain A
//    EwrdCdbWiFiSarTxPowerSet4Limit2,               // WiFi SAR Tx Power Limit for 5150-5350 MHz Set 4 Chain A
//    EwrdCdbWiFiSarTxPowerSet4Limit3,               // WiFi SAR Tx Power Limit for 5350-5470 MHz Set 4 Chain A
//    EwrdCdbWiFiSarTxPowerSet4Limit4,               // WiFi SAR Tx Power Limit for 5470-5725 MHz Set 4 Chain A
//    EwrdCdbWiFiSarTxPowerSet4Limit5,               // WiFi SAR Tx Power Limit for 5725-5925 MHz Set 4 Chain A
//    EwrdCdbWiFiSarTxPowerSet4Limit6,               // WiFi SAR Tx Power Limit for 5945-6165 MHz Set 4 Chain A
//    EwrdCdbWiFiSarTxPowerSet4Limit7,               // WiFi SAR Tx Power Limit for 6165-6405 MHz Set 4 Chain A
//    EwrdCdbWiFiSarTxPowerSet4Limit8,               // WiFi SAR Tx Power Limit for 6405-6525 MHz Set 4 Chain A
//    EwrdCdbWiFiSarTxPowerSet4Limit9,               // WiFi SAR Tx Power Limit for 6525-6705 MHz Set 4 Chain A
//    EwrdCdbWiFiSarTxPowerSet4Limit10,              // WiFi SAR Tx Power Limit for 6705-6865 MHz Set 4 Chain A
//    EwrdCdbWiFiSarTxPowerSet4Limit11,              // WiFi SAR Tx Power Limit for 6865-7105 MHz Set 4 Chain A
//    EwrdCdbWiFiSarTxPowerSet4Limit12,              // WiFi SAR Tx Power Limit for 2400 MHz Set 4 Chain B
//    EwrdCdbWiFiSarTxPowerSet4Limit13,              // WiFi SAR Tx Power Limit for 5150-5350 MHz Set 4 Chain B
//    EwrdCdbWiFiSarTxPowerSet4Limit14,              // WiFi SAR Tx Power Limit for 5350-5470 MHz Set 4 Chain B
//    EwrdCdbWiFiSarTxPowerSet4Limit15,              // WiFi SAR Tx Power Limit for 5470-5725 MHz Set 4 Chain B
//    EwrdCdbWiFiSarTxPowerSet4Limit16,              // WiFi SAR Tx Power Limit for 5725-5925 MHz Set 4 Chain B
//    EwrdCdbWiFiSarTxPowerSet4Limit17,              // WiFi SAR Tx Power Limit for 5945-6165 MHz Set 4 Chain B
//    EwrdCdbWiFiSarTxPowerSet4Limit18,              // WiFi SAR Tx Power Limit for 6165-6405 MHz Set 4 Chain B
//    EwrdCdbWiFiSarTxPowerSet4Limit19,              // WiFi SAR Tx Power Limit for 6405-6525 MHz Set 4 Chain B
//    EwrdCdbWiFiSarTxPowerSet4Limit20,              // WiFi SAR Tx Power Limit for 6525-6705 MHz Set 4 Chain B
//    EwrdCdbWiFiSarTxPowerSet4Limit21,              // WiFi SAR Tx Power Limit for 6705-6865 MHz Set 4 Chain B
//    EwrdCdbWiFiSarTxPowerSet4Limit22,              // WiFi SAR Tx Power Limit for 6865-7105 MHz Set 4 Chain B

//  }
//})                        // End of EWRD object
//
Method(EWRD,0,Serialized)
{
  Store(\STDE,Index (DeRefOf (Index (EWRY, 1)), 1))  // WiFi Dynamic SAR Enable/Disable
  Store(\STRS,Index (DeRefOf (Index (EWRY, 1)), 2))  // WiFi SAR Number of Optional added SAR table sets to be used
  Store(\ST20,Index (DeRefOf (Index (EWRY, 1)), 3))  // WiFi SAR Tx Power Limit for 2400 MHz Set 2 Chain A
  Store(\ST21,Index (DeRefOf (Index (EWRY, 1)), 4))  // WiFi SAR Tx Power Limit for 5150-5350 MHz Set 2 Chain A
  Store(\ST22,Index (DeRefOf (Index (EWRY, 1)), 5))  // WiFi SAR Tx Power Limit for 5350-5470 MHz Set 2 Chain A
  Store(\ST23,Index (DeRefOf (Index (EWRY, 1)), 6))  // WiFi SAR Tx Power Limit for 5470-5725 MHz Set 2 Chain A
  Store(\ST24,Index (DeRefOf (Index (EWRY, 1)), 7))  // WiFi SAR Tx Power Limit for 5725-5925 MHz Set 2 Chain A
  Store(\ST25,Index (DeRefOf (Index (EWRY, 1)), 8))  // WiFi SAR Tx Power Limit for 5945-6165 MHz Set 2 Chain A
  Store(\ST26,Index (DeRefOf (Index (EWRY, 1)), 9))  // WiFi SAR Tx Power Limit for 6165-6405 MHz Set 2 Chain A
  Store(\ST27,Index (DeRefOf (Index (EWRY, 1)), 10)) // WiFi SAR Tx Power Limit for 6405-6525 MHz Set 2 Chain A
  Store(\ST28,Index (DeRefOf (Index (EWRY, 1)), 11)) // WiFi SAR Tx Power Limit for 6525-6705 MHz Set 2 Chain A
  Store(\ST29,Index (DeRefOf (Index (EWRY, 1)), 12)) // WiFi SAR Tx Power Limit for 6705-6865 MHz Set 2 Chain A
  Store(\ST60,Index (DeRefOf (Index (EWRY, 1)), 13)) // WiFi SAR Tx Power Limit for 6865-7105 MHz Set 2 Chain A
  Store(\ST61,Index (DeRefOf (Index (EWRY, 1)), 14)) // WiFi SAR Tx Power Limit for 2400 MHz Set 2 Chain B
  Store(\ST62,Index (DeRefOf (Index (EWRY, 1)), 15)) // WiFi SAR Tx Power Limit for 5150-5350 MHz Set 2 Chain B
  Store(\ST63,Index (DeRefOf (Index (EWRY, 1)), 16)) // WiFi SAR Tx Power Limit for 5350-5470 MHz Set 2 Chain B
  Store(\ST64,Index (DeRefOf (Index (EWRY, 1)), 17)) // WiFi SAR Tx Power Limit for 5470-5725 MHz Set 2 Chain B
  Store(\ST65,Index (DeRefOf (Index (EWRY, 1)), 18)) // WiFi SAR Tx Power Limit for 5725-5925 MHz Set 2 Chain B
  Store(\ST66,Index (DeRefOf (Index (EWRY, 1)), 19)) // WiFi SAR Tx Power Limit for 5945-6165 MHz Set 2 Chain B
  Store(\ST67,Index (DeRefOf (Index (EWRY, 1)), 20)) // WiFi SAR Tx Power Limit for 6165-6405 MHz Set 2 Chain B
  Store(\ST68,Index (DeRefOf (Index (EWRY, 1)), 21)) // WiFi SAR Tx Power Limit for 6405-6525 MHz Set 2 Chain B
  Store(\ST69,Index (DeRefOf (Index (EWRY, 1)), 22)) // WiFi SAR Tx Power Limit for 6525-6705 MHz Set 2 Chain B
  Store(\ST6A,Index (DeRefOf (Index (EWRY, 1)), 23)) // WiFi SAR Tx Power Limit for 6705-6865 MHz Set 2 Chain B
  Store(\ST6B,Index (DeRefOf (Index (EWRY, 1)), 24)) // WiFi SAR Tx Power Limit for 6865-7105 MHz Set 2 Chain B

  Store(\ST30,Index (DeRefOf (Index (EWRY, 1)), 25)) // WiFi SAR Tx Power Limit for 2400 MHz Set 3 Chain A
  Store(\ST31,Index (DeRefOf (Index (EWRY, 1)), 26)) // WiFi SAR Tx Power Limit for 5150-5350 MHz Set 3 Chain A
  Store(\ST32,Index (DeRefOf (Index (EWRY, 1)), 27)) // WiFi SAR Tx Power Limit for 5350-5470 MHz Set 3 Chain A
  Store(\ST33,Index (DeRefOf (Index (EWRY, 1)), 28)) // WiFi SAR Tx Power Limit for 5470-5725 MHz Set 3 Chain A
  Store(\ST34,Index (DeRefOf (Index (EWRY, 1)), 29)) // WiFi SAR Tx Power Limit for 5725-5925 MHz Set 3 Chain A
  Store(\ST35,Index (DeRefOf (Index (EWRY, 1)), 30)) // WiFi SAR Tx Power Limit for 5945-6165 MHz Set 3 Chain A
  Store(\ST36,Index (DeRefOf (Index (EWRY, 1)), 31)) // WiFi SAR Tx Power Limit for 6165-6405 MHz Set 3 Chain A
  Store(\ST37,Index (DeRefOf (Index (EWRY, 1)), 32)) // WiFi SAR Tx Power Limit for 6405-6525 MHz Set 3 Chain A
  Store(\ST38,Index (DeRefOf (Index (EWRY, 1)), 33)) // WiFi SAR Tx Power Limit for 6525-6705 MHz Set 3 Chain A
  Store(\ST39,Index (DeRefOf (Index (EWRY, 1)), 34)) // WiFi SAR Tx Power Limit for 6705-6865 MHz Set 3 Chain A
  Store(\ST70,Index (DeRefOf (Index (EWRY, 1)), 35)) // WiFi SAR Tx Power Limit for 6865-7105 MHz Set 3 Chain A
  Store(\ST71,Index (DeRefOf (Index (EWRY, 1)), 36)) // WiFi SAR Tx Power Limit for 2400 MHz Set 3 Chain B
  Store(\ST72,Index (DeRefOf (Index (EWRY, 1)), 37)) // WiFi SAR Tx Power Limit for 5150-5350 MHz Set 3 Chain B
  Store(\ST73,Index (DeRefOf (Index (EWRY, 1)), 38)) // WiFi SAR Tx Power Limit for 5350-5470 MHz Set 3 Chain B
  Store(\ST74,Index (DeRefOf (Index (EWRY, 1)), 39)) // WiFi SAR Tx Power Limit for 5470-5725 MHz Set 3 Chain B
  Store(\ST75,Index (DeRefOf (Index (EWRY, 1)), 40)) // WiFi SAR Tx Power Limit for 5725-5925 MHz Set 3 Chain B
  Store(\ST76,Index (DeRefOf (Index (EWRY, 1)), 41)) // WiFi SAR Tx Power Limit for 5945-6165 MHz Set 3 Chain B
  Store(\ST77,Index (DeRefOf (Index (EWRY, 1)), 42)) // WiFi SAR Tx Power Limit for 6165-6405 MHz Set 3 Chain B
  Store(\ST78,Index (DeRefOf (Index (EWRY, 1)), 43)) // WiFi SAR Tx Power Limit for 6405-6525 MHz Set 3 Chain B
  Store(\ST79,Index (DeRefOf (Index (EWRY, 1)), 44)) // WiFi SAR Tx Power Limit for 6525-6705 MHz Set 3 Chain B
  Store(\ST7A,Index (DeRefOf (Index (EWRY, 1)), 45)) // WiFi SAR Tx Power Limit for 6705-6865 MHz Set 3 Chain B
  Store(\ST7B,Index (DeRefOf (Index (EWRY, 1)), 46)) // WiFi SAR Tx Power Limit for 6865-7105 MHz Set 3 Chain B

  Store(\ST40,Index (DeRefOf (Index (EWRY, 1)), 47)) // WiFi SAR Tx Power Limit for 2400 MHz Set 4 Chain A
  Store(\ST41,Index (DeRefOf (Index (EWRY, 1)), 48)) // WiFi SAR Tx Power Limit for 5150-5350 MHz Set 4 Chain A
  Store(\ST42,Index (DeRefOf (Index (EWRY, 1)), 49)) // WiFi SAR Tx Power Limit for 5350-5470 MHz Set 4 Chain A
  Store(\ST43,Index (DeRefOf (Index (EWRY, 1)), 50)) // WiFi SAR Tx Power Limit for 5470-5725 MHz Set 4 Chain A
  Store(\ST44,Index (DeRefOf (Index (EWRY, 1)), 51)) // WiFi SAR Tx Power Limit for 5725-5925 MHz Set 4 Chain A
  Store(\ST45,Index (DeRefOf (Index (EWRY, 1)), 52)) // WiFi SAR Tx Power Limit for 5945-6165 MHz Set 4 Chain A
  Store(\ST46,Index (DeRefOf (Index (EWRY, 1)), 53)) // WiFi SAR Tx Power Limit for 6165-6405 MHz Set 4 Chain A
  Store(\ST47,Index (DeRefOf (Index (EWRY, 1)), 54)) // WiFi SAR Tx Power Limit for 6405-6525 MHz Set 4 Chain A
  Store(\ST48,Index (DeRefOf (Index (EWRY, 1)), 55)) // WiFi SAR Tx Power Limit for 6525-6705 MHz Set 4 Chain A
  Store(\ST49,Index (DeRefOf (Index (EWRY, 1)), 56)) // WiFi SAR Tx Power Limit for 6705-6865 MHz Set 4 Chain A
  Store(\ST80,Index (DeRefOf (Index (EWRY, 1)), 57)) // WiFi SAR Tx Power Limit for 6865-7105 MHz Set 4 Chain A
  Store(\ST81,Index (DeRefOf (Index (EWRY, 1)), 58)) // WiFi SAR Tx Power Limit for 2400 MHz Set 4 Chain B
  Store(\ST82,Index (DeRefOf (Index (EWRY, 1)), 59)) // WiFi SAR Tx Power Limit for 5150-5350 MHz Set 4 Chain B
  Store(\ST83,Index (DeRefOf (Index (EWRY, 1)), 60)) // WiFi SAR Tx Power Limit for 5350-5470 MHz Set 4 Chain B
  Store(\ST84,Index (DeRefOf (Index (EWRY, 1)), 61)) // WiFi SAR Tx Power Limit for 5470-5725 MHz Set 4 Chain B
  Store(\ST85,Index (DeRefOf (Index (EWRY, 1)), 62)) // WiFi SAR Tx Power Limit for 5725-5925 MHz Set 4 Chain B
  Store(\ST86,Index (DeRefOf (Index (EWRY, 1)), 63)) // WiFi SAR Tx Power Limit for 5945-6165 MHz Set 4 Chain B
  Store(\ST87,Index (DeRefOf (Index (EWRY, 1)), 64)) // WiFi SAR Tx Power Limit for 6165-6405 MHz Set 4 Chain B
  Store(\ST88,Index (DeRefOf (Index (EWRY, 1)), 65)) // WiFi SAR Tx Power Limit for 6405-6525 MHz Set 4 Chain B
  Store(\ST89,Index (DeRefOf (Index (EWRY, 1)), 66)) // WiFi SAR Tx Power Limit for 6525-6705 MHz Set 4 Chain B
  Store(\ST8A,Index (DeRefOf (Index (EWRY, 1)), 67)) // WiFi SAR Tx Power Limit for 6705-6865 MHz Set 4 Chain B
  Store(\ST8B,Index (DeRefOf (Index (EWRY, 1)), 68)) // WiFi SAR Tx Power Limit for 6865-7105 MHz Set 4 Chain B
  Store(\CD30,Index (DeRefOf (Index (EWRY, 1)), 69))  // CDB Mode WiFi SAR Tx Power Limit for 2400 MHz Set 2 Chain A
  Store(\CD31,Index (DeRefOf (Index (EWRY, 1)), 70))  // CDB Mode WiFi SAR Tx Power Limit for 5150-5350 MHz Set 2 Chain A
  Store(\CD32,Index (DeRefOf (Index (EWRY, 1)), 71))  // CDB Mode WiFi SAR Tx Power Limit for 5350-5470 MHz Set 2 Chain A
  Store(\CD33,Index (DeRefOf (Index (EWRY, 1)), 72))  // CDB Mode WiFi SAR Tx Power Limit for 5470-5725 MHz Set 2 Chain A
  Store(\CD34,Index (DeRefOf (Index (EWRY, 1)), 73))  // CDB Mode WiFi SAR Tx Power Limit for 5725-5925 MHz Set 2 Chain A
  Store(\CD35,Index (DeRefOf (Index (EWRY, 1)), 74))  // CDB Mode WiFi SAR Tx Power Limit for 5945-6165 MHz Set 2 Chain A
  Store(\CD36,Index (DeRefOf (Index (EWRY, 1)), 75))  // CDB Mode WiFi SAR Tx Power Limit for 6165-6405 MHz Set 2 Chain A
  Store(\CD37,Index (DeRefOf (Index (EWRY, 1)), 76))  // CDB Mode WiFi SAR Tx Power Limit for 6405-6525 MHz Set 2 Chain A
  Store(\CD38,Index (DeRefOf (Index (EWRY, 1)), 77))  // CDB Mode WiFi SAR Tx Power Limit for 6525-6705 MHz Set 2 Chain A
  Store(\CD39,Index (DeRefOf (Index (EWRY, 1)), 78))  // CDB Mode WiFi SAR Tx Power Limit for 6705-6865 MHz Set 2 Chain A
  Store(\CD3A,Index (DeRefOf (Index (EWRY, 1)), 79))  // CDB Mode WiFi SAR Tx Power Limit for 6865-7105 MHz Set 2 Chain A
  Store(\CD3B,Index (DeRefOf (Index (EWRY, 1)), 80))  // CDB Mode WiFi SAR Tx Power Limit for 2400 MHz Set 2 Chain B
  Store(\CD3C,Index (DeRefOf (Index (EWRY, 1)), 81))  // CDB Mode WiFi SAR Tx Power Limit for 5150-5350 MHz Set 2 Chain B
  Store(\CD3D,Index (DeRefOf (Index (EWRY, 1)), 82))  // CDB Mode WiFi SAR Tx Power Limit for 5350-5470 MHz Set 2 Chain B
  Store(\CD3E,Index (DeRefOf (Index (EWRY, 1)), 83))  // CDB Mode WiFi SAR Tx Power Limit for 5470-5725 MHz Set 2 Chain B
  Store(\CD3F,Index (DeRefOf (Index (EWRY, 1)), 84))  // CDB Mode WiFi SAR Tx Power Limit for 5725-5925 MHz Set 2 Chain B
  Store(\CD40,Index (DeRefOf (Index (EWRY, 1)), 85))  // CDB Mode WiFi SAR Tx Power Limit for 5945-6165 MHz Set 2 Chain B
  Store(\CD41,Index (DeRefOf (Index (EWRY, 1)), 86))  // CDB Mode WiFi SAR Tx Power Limit for 6165-6405 MHz Set 2 Chain B
  Store(\CD42,Index (DeRefOf (Index (EWRY, 1)), 87))  // CDB Mode WiFi SAR Tx Power Limit for 6405-6525 MHz Set 2 Chain B
  Store(\CD43,Index (DeRefOf (Index (EWRY, 1)), 88))  // CDB Mode WiFi SAR Tx Power Limit for 6525-6705 MHz Set 2 Chain B
  Store(\CD44,Index (DeRefOf (Index (EWRY, 1)), 89))  // CDB Mode WiFi SAR Tx Power Limit for 6705-6865 MHz Set 2 Chain B
  Store(\CD45,Index (DeRefOf (Index (EWRY, 1)), 90))  // CDB Mode WiFi SAR Tx Power Limit for 6865-7105 MHz Set 2 Chain B

  Store(\CD46,Index (DeRefOf (Index (EWRY, 1)), 91))  // CDB Mode WiFi SAR Tx Power Limit for 2400 MHz Set 3 Chain A
  Store(\CD47,Index (DeRefOf (Index (EWRY, 1)), 92))  // CDB Mode WiFi SAR Tx Power Limit for 5150-5350 MHz Set 3 Chain A
  Store(\CD48,Index (DeRefOf (Index (EWRY, 1)), 93))  // CDB Mode WiFi SAR Tx Power Limit for 5350-5470 MHz Set 3 Chain A
  Store(\CD49,Index (DeRefOf (Index (EWRY, 1)), 94))  // CDB Mode WiFi SAR Tx Power Limit for 5470-5725 MHz Set 3 Chain A
  Store(\CD4A,Index (DeRefOf (Index (EWRY, 1)), 95))  // CDB Mode WiFi SAR Tx Power Limit for 5725-5925 MHz Set 3 Chain A
  Store(\CD4B,Index (DeRefOf (Index (EWRY, 1)), 96))  // CDB Mode WiFi SAR Tx Power Limit for 5945-6165 MHz Set 3 Chain A
  Store(\CD4C,Index (DeRefOf (Index (EWRY, 1)), 97))  // CDB Mode WiFi SAR Tx Power Limit for 6165-6405 MHz Set 3 Chain A
  Store(\CD4D,Index (DeRefOf (Index (EWRY, 1)), 98))  // CDB Mode WiFi SAR Tx Power Limit for 6405-6525 MHz Set 3 Chain A
  Store(\CD4E,Index (DeRefOf (Index (EWRY, 1)), 99))  // CDB Mode WiFi SAR Tx Power Limit for 6525-6705 MHz Set 3 Chain A
  Store(\CD4F,Index (DeRefOf (Index (EWRY, 1)), 100)) // CDB Mode WiFi SAR Tx Power Limit for 6705-6865 MHz Set 3 Chain A
  Store(\CD50,Index (DeRefOf (Index (EWRY, 1)), 101)) // CDB Mode WiFi SAR Tx Power Limit for 6865-7105 MHz Set 3 Chain A
  Store(\CD51,Index (DeRefOf (Index (EWRY, 1)), 102)) // CDB Mode WiFi SAR Tx Power Limit for 2400 MHz Set 3 Chain B
  Store(\CD52,Index (DeRefOf (Index (EWRY, 1)), 103)) // CDB Mode WiFi SAR Tx Power Limit for 5150-5350 MHz Set 3 Chain B
  Store(\CD53,Index (DeRefOf (Index (EWRY, 1)), 104)) // CDB Mode WiFi SAR Tx Power Limit for 5350-5470 MHz Set 3 Chain B
  Store(\CD54,Index (DeRefOf (Index (EWRY, 1)), 105)) // CDB Mode WiFi SAR Tx Power Limit for 5470-5725 MHz Set 3 Chain B
  Store(\CD55,Index (DeRefOf (Index (EWRY, 1)), 106)) // CDB Mode WiFi SAR Tx Power Limit for 5725-5925 MHz Set 3 Chain B
  Store(\CD56,Index (DeRefOf (Index (EWRY, 1)), 107)) // CDB Mode WiFi SAR Tx Power Limit for 5945-6165 MHz Set 3 Chain B
  Store(\CD57,Index (DeRefOf (Index (EWRY, 1)), 108)) // CDB Mode WiFi SAR Tx Power Limit for 6165-6405 MHz Set 3 Chain B
  Store(\CD58,Index (DeRefOf (Index (EWRY, 1)), 109)) // CDB Mode WiFi SAR Tx Power Limit for 6405-6525 MHz Set 3 Chain B
  Store(\CD59,Index (DeRefOf (Index (EWRY, 1)), 110)) // CDB Mode WiFi SAR Tx Power Limit for 6525-6705 MHz Set 3 Chain B
  Store(\CD5A,Index (DeRefOf (Index (EWRY, 1)), 111)) // CDB Mode WiFi SAR Tx Power Limit for 6705-6865 MHz Set 3 Chain B
  Store(\CD5B,Index (DeRefOf (Index (EWRY, 1)), 112)) // CDB Mode WiFi SAR Tx Power Limit for 6865-7105 MHz Set 3 Chain B

  Store(\CD5C,Index (DeRefOf (Index (EWRY, 1)), 113)) // CDB Mode WiFi SAR Tx Power Limit for 2400 MHz Set 4 Chain A
  Store(\CD5D,Index (DeRefOf (Index (EWRY, 1)), 114)) // CDB Mode WiFi SAR Tx Power Limit for 5150-5350 MHz Set 4 Chain A
  Store(\CD5E,Index (DeRefOf (Index (EWRY, 1)), 115)) // CDB Mode WiFi SAR Tx Power Limit for 5350-5470 MHz Set 4 Chain A
  Store(\CD5F,Index (DeRefOf (Index (EWRY, 1)), 116)) // CDB Mode WiFi SAR Tx Power Limit for 5470-5725 MHz Set 4 Chain A
  Store(\CD60,Index (DeRefOf (Index (EWRY, 1)), 117)) // CDB Mode WiFi SAR Tx Power Limit for 5725-5925 MHz Set 4 Chain A
  Store(\CD61,Index (DeRefOf (Index (EWRY, 1)), 118)) // CDB Mode WiFi SAR Tx Power Limit for 5945-6165 MHz Set 4 Chain A
  Store(\CD62,Index (DeRefOf (Index (EWRY, 1)), 119)) // CDB Mode WiFi SAR Tx Power Limit for 6165-6405 MHz Set 4 Chain A
  Store(\CD63,Index (DeRefOf (Index (EWRY, 1)), 120)) // CDB Mode WiFi SAR Tx Power Limit for 6405-6525 MHz Set 4 Chain A
  Store(\CD64,Index (DeRefOf (Index (EWRY, 1)), 121)) // CDB Mode WiFi SAR Tx Power Limit for 6525-6705 MHz Set 4 Chain A
  Store(\CD65,Index (DeRefOf (Index (EWRY, 1)), 122)) // CDB Mode WiFi SAR Tx Power Limit for 6705-6865 MHz Set 4 Chain A
  Store(\CD66,Index (DeRefOf (Index (EWRY, 1)), 123)) // CDB Mode WiFi SAR Tx Power Limit for 6865-7105 MHz Set 4 Chain A
  Store(\CD67,Index (DeRefOf (Index (EWRY, 1)), 124)) // CDB Mode WiFi SAR Tx Power Limit for 2400 MHz Set 4 Chain B
  Store(\CD68,Index (DeRefOf (Index (EWRY, 1)), 125)) // CDB Mode WiFi SAR Tx Power Limit for 5150-5350 MHz Set 4 Chain B
  Store(\CD69,Index (DeRefOf (Index (EWRY, 1)), 126)) // CDB Mode WiFi SAR Tx Power Limit for 5350-5470 MHz Set 4 Chain B
  Store(\CD6A,Index (DeRefOf (Index (EWRY, 1)), 127)) // CDB Mode WiFi SAR Tx Power Limit for 5470-5725 MHz Set 4 Chain B
  Store(\CD6B,Index (DeRefOf (Index (EWRY, 1)), 128)) // CDB Mode WiFi SAR Tx Power Limit for 5725-5925 MHz Set 4 Chain B
  Store(\CD6C,Index (DeRefOf (Index (EWRY, 1)), 129)) // CDB Mode WiFi SAR Tx Power Limit for 5945-6165 MHz Set 4 Chain B
  Store(\CD6D,Index (DeRefOf (Index (EWRY, 1)), 130)) // CDB Mode WiFi SAR Tx Power Limit for 6165-6405 MHz Set 4 Chain B
  Store(\CD6E,Index (DeRefOf (Index (EWRY, 1)), 131)) // CDB Mode WiFi SAR Tx Power Limit for 6405-6525 MHz Set 4 Chain B
  Store(\CD6F,Index (DeRefOf (Index (EWRY, 1)), 132)) // CDB Mode WiFi SAR Tx Power Limit for 6525-6705 MHz Set 4 Chain B
  Store(\CD70,Index (DeRefOf (Index (EWRY, 1)), 133)) // CDB Mode WiFi SAR Tx Power Limit for 6705-6865 MHz Set 4 Chain B
  Store(\CD71,Index (DeRefOf (Index (EWRY, 1)), 134)) // CDB Mode WiFi SAR Tx Power Limit for 6865-7105 MHz Set 4 Chain B

  Return(EWRY)
}

Name (WGDY, Package()
{
  2,                      // Revision, DWordConst
  Package()               // Configuration for WiFi
  {
    0x07,                 // DomainType, 0x7:WiFi
    0x80,                 // WiFi output power delta for Group 1 FCC 2400 MHZ Max Allowed
    0x80,                 // WiFi output power delta for Group 1 FCC 2400 Chain A Offset
    0x80,                 // WiFi output power delta for Group 1 FCC 2400 Chain B Offset
    0x80,                 // WiFi output power delta for Group 1 FCC 5200 MHZ Max Allowed
    0x80,                 // WiFi output power delta for Group 1 FCC 5200 Chain A Offset
    0x80,                 // WiFi output power delta for Group 1 FCC 5200 Chain B Offset
    0x80,                 // WiFi output power delta for Group 1 FCC 6000-7000 MHZ Max Allowed
    0x80,                 // WiFi output power delta for Group 1 FCC 6000-7000 Chain A Offset
    0x80,                 // WiFi output power delta for Group 1 FCC 6000-7000 Chain B Offset
    0x80,                 // WiFi output power delta for Group 2 EC Japan 2400 MHZ Max Allowed
    0x80,                 // WiFi output power delta for Group 2 EC Japan 2400 Chain A Offset
    0x80,                 // WiFi output power delta for Group 2 EC Japan 2400 Chain B Offset
    0x80,                 // WiFi output power delta for Group 2 EC Japan 5200 MHZ Max Allowed
    0x80,                 // WiFi output power delta for Group 2 EC Japan 5200 Chain A Offset
    0x80,                 // WiFi output power delta for Group 2 EC Japan 5200 Chain B Offset
    0x80,                 // WiFi output power delta for Group 2 EC Japan 6000-7000 MHZ Max Allowed
    0x80,                 // WiFi output power delta for Group 2 EC Japan 6000-7000 Chain A Offset
    0x80,                 // WiFi output power delta for Group 2 EC Japan 6000-7000 Chain B Offset
    0x80,                 // WiFi output power delta for Group 3 ROW 2400 MHZ Max Allowed
    0x80,                 // WiFi output power delta for Group 3 ROW 2400 Chain A Offset
    0x80,                 // WiFi output power delta for Group 3 ROW 2400 Chain B Offset
    0x80,                 // WiFi output power delta for Group 3 ROW 5200 MHZ Max Allowed
    0x80,                 // WiFi output power delta for Group 3 ROW 5200 Chain A Offset
    0x80,                 // WiFi output power delta for Group 3 ROW 5200 Chain B Offset
    0x80,                 // WiFi output power delta for Group 3 ROW 6000-7000 MHZ Max Allowed
    0x80,                 // WiFi output power delta for Group 3 ROW 6000-7000 Chain A Offset
    0x80,                 // WiFi output power delta for Group 3 ROW 6000-7000 Chain B Offset
  }
})                        // End of WGDY object

// WGDS (Wireless Geo Delta Settings)-Revision 2
//
// Defines the output power delta of the current SAR set, this value will allow an increased Tx power
// compared to the default (Canada) BIOS SAR settings in case DRS mechanism
// has detected country that is compatible with Europe regulations
//
//
// Arguments: (0)
//   None
// Return Value:
//
// Name (WGDY, Package()
// {
//  Revision,                                      // Revision, DWordConst
//  Package()                                      // Configuration for WiFi
//  {
//    DomainType,                                  // DomainType, 0x7:WiFi
//    WgdsWiFiSarDeltaGroup1PowerMax1,             // WiFi output power delta for Group 1 FCC 2400 MHZ Max Allowed
//    WgdsWiFiSarDeltaGroup1PowerChainA1,          // WiFi output power delta for Group 1 FCC 2400 Chain A Offset
//    WgdsWiFiSarDeltaGroup1PowerChainB1,          // WiFi output power delta for Group 1 FCC 2400 Chain B Offset
//    WgdsWiFiSarDeltaGroup1PowerMax2,             // WiFi output power delta for Group 1 FCC 5200 MHZ Max Allowed
//    WgdsWiFiSarDeltaGroup1PowerChainA2,          // WiFi output power delta for Group 1 FCC 5200 Chain A Offset
//    WgdsWiFiSarDeltaGroup1PowerChainB2,          // WiFi output power delta for Group 1 FCC 5200 Chain B Offset
//    WgdsWiFiSarDeltaGroup1PowerMax3,             // WiFi output power delta for Group 1 FCC 6000-7000 MHZ Max Allowed
//    WgdsWiFiSarDeltaGroup1PowerChainA3,          // WiFi output power delta for Group 1 FCC 6000-7000 Chain A Offset
//    WgdsWiFiSarDeltaGroup1PowerChainB3,          // WiFi output power delta for Group 1 FCC 6000-7000 Chain B Offset
//    WgdsWiFiSarDeltaGroup2PowerMax1,             // WiFi output power delta for Group 2 EC Japan 2400 MHZ Max Allowed
//    WgdsWiFiSarDeltaGroup2PowerChainA1,          // WiFi output power delta for Group 2 EC Japan 2400 Chain A Offset
//    WgdsWiFiSarDeltaGroup2PowerChainB1,          // WiFi output power delta for Group 2 EC Japan 2400 Chain B Offset
//    WgdsWiFiSarDeltaGroup2PowerMax2,             // WiFi output power delta for Group 2 EC Japan 5200 MHZ Max Allowed
//    WgdsWiFiSarDeltaGroup2PowerChainA2,          // WiFi output power delta for Group 2 EC Japan 5200 Chain A Offset
//    WgdsWiFiSarDeltaGroup2PowerChainB2,          // WiFi output power delta for Group 2 EC Japan 5200 Chain B Offset
//    WgdsWiFiSarDeltaGroup2PowerMax3,             // WiFi output power delta for Group 2 EC Japan 6000-7000 MHZ Max Allowed
//    WgdsWiFiSarDeltaGroup2PowerChainA3,          // WiFi output power delta for Group 2 EC Japan 6000-7000 Chain A Offset
//    WgdsWiFiSarDeltaGroup2PowerChainB3,          // WiFi output power delta for Group 2 EC Japan 6000-7000 Chain B Offset
//    WgdsWiFiSarDeltaGroup3PowerMax1,             // WiFi output power delta for Group 3 ROW 2400 MHZ Max Allowed
//    WgdsWiFiSarDeltaGroup3PowerChainA1,          // WiFi output power delta for Group 3 ROW 2400 Chain A Offset
//    WgdsWiFiSarDeltaGroup3PowerChainB1,          // WiFi output power delta for Group 3 ROW 2400 Chain B Offset
//    WgdsWiFiSarDeltaGroup3PowerMax2,             // WiFi output power delta for Group 3 ROW 5200 MHZ Max Allowed
//    WgdsWiFiSarDeltaGroup3PowerChainA2,          // WiFi output power delta for Group 3 ROW 5200 Chain A Offset
//    WgdsWiFiSarDeltaGroup3PowerChainB2,          // WiFi output power delta for Group 3 ROW 5200 Chain B Offset
//    WgdsWiFiSarDeltaGroup3PowerMax3,             // WiFi output power delta for Group 3 ROW 6000-7000 MHZ Max Allowed
//    WgdsWiFiSarDeltaGroup3PowerChainA3,          // WiFi output power delta for Group 3 ROW Japan 6000-7000 Chain A Offset
//    WgdsWiFiSarDeltaGroup3PowerChainB3,          // WiFi output power delta for Group 3 ROW Japan 6000-7000 Chain B Offset
//  }
//})                        // End of WGDS object
//

Method(WGDS,0,Serialized)
{
  Store(\SD11,Index (DeRefOf (Index (WGDY, 1)), 1)) // WiFi output power delta for Group 1 FCC 2400 MHZ Max Allowed
  Store(\SD12,Index (DeRefOf (Index (WGDY, 1)), 2)) // WiFi output power delta for Group 1 FCC 2400 Chain A Offset
  Store(\SD13,Index (DeRefOf (Index (WGDY, 1)), 3)) // WiFi output power delta for Group 1 FCC 2400 Chain B Offset
  Store(\SD14,Index (DeRefOf (Index (WGDY, 1)), 4)) // WiFi output power delta for Group 1 FCC 5200 MHZ Max Allowed
  Store(\SD15,Index (DeRefOf (Index (WGDY, 1)), 5)) // WiFi output power delta for Group 1 FCC 5200 Chain A Offset
  Store(\SD16,Index (DeRefOf (Index (WGDY, 1)), 6)) // WiFi output power delta for Group 1 FCC 5200 Chain B Offset
  Store(\SD17,Index (DeRefOf (Index (WGDY, 1)), 7)) // WiFi output power delta for Group 1 FCC 6000-7000 MHz Max Allowed
  Store(\SD18,Index (DeRefOf (Index (WGDY, 1)), 8)) // WiFi output power delta for Group 1 FCC 6000-7000 Chain A Offset
  Store(\SD19,Index (DeRefOf (Index (WGDY, 1)), 9)) // WiFi output power delta for Group 1 FCC 6000-7000 Chain B Offset

  Store(\SD21,Index (DeRefOf (Index (WGDY, 1)), 10)) // WiFi output power delta for Group 2 EC Japan 2400 MHZ Max Allowed
  Store(\SD22,Index (DeRefOf (Index (WGDY, 1)), 11)) // WiFi output power delta for Group 2 EC Japan 2400 Chain A Offset
  Store(\SD23,Index (DeRefOf (Index (WGDY, 1)), 12)) // WiFi output power delta for Group 2 EC Japan 2400 Chain B Offset
  Store(\SD24,Index (DeRefOf (Index (WGDY, 1)), 13)) // WiFi output power delta for Group 2 EC Japan 5200 MHZ Max Allowed
  Store(\SD25,Index (DeRefOf (Index (WGDY, 1)), 14)) // WiFi output power delta for Group 2 EC Japan 5200 Chain A Offset
  Store(\SD26,Index (DeRefOf (Index (WGDY, 1)), 15)) // WiFi output power delta for Group 2 EC Japan 5200 Chain B Offset
  Store(\SD27,Index (DeRefOf (Index (WGDY, 1)), 16)) // WiFi output power delta for Group 2 EC Japan 6000-7000 MHz Max Allowed
  Store(\SD28,Index (DeRefOf (Index (WGDY, 1)), 17)) // WiFi output power delta for Group 2 EC Japan 6000-7000 Chain A Offset
  Store(\SD29,Index (DeRefOf (Index (WGDY, 1)), 18)) // WiFi output power delta for Group 2 EC Japan 6000-7000 Chain B Offset

  Store(\SD31,Index (DeRefOf (Index (WGDY, 1)), 19)) // WiFi output power delta for Group 3 ROW 2400 MHZ Max Allowed
  Store(\SD32,Index (DeRefOf (Index (WGDY, 1)), 20)) // WiFi output power delta for Group 3 ROW 2400 Chain A Offset
  Store(\SD33,Index (DeRefOf (Index (WGDY, 1)), 21)) // WiFi output power delta for Group 3 ROW 2400 Chain B Offset
  Store(\SD34,Index (DeRefOf (Index (WGDY, 1)), 22)) // WiFi output power delta for Group 3 ROW 5200 MHZ Max Allowed
  Store(\SD35,Index (DeRefOf (Index (WGDY, 1)), 23)) // WiFi output power delta for Group 3 ROW 5200 Chain A Offset
  Store(\SD36,Index (DeRefOf (Index (WGDY, 1)), 24)) // WiFi output power delta for Group 3 ROW 5200 Chain B Offset
  Store(\SD37,Index (DeRefOf (Index (WGDY, 1)), 25)) // WiFi output power delta for Group 3 ROW 6000-7000 MHz Max Allowed
  Store(\SD38,Index (DeRefOf (Index (WGDY, 1)), 26)) // WiFi output power delta for Group 3 ROW Japan 6000-7000 Chain A Offset
  Store(\SD39,Index (DeRefOf (Index (WGDY, 1)), 27)) // WiFi output power delta for Group 3 ROW Japan 6000-7000 Chain B Offset

  Return(WGDY)
}

Name (ECKY, Package()
{
  0,                      // Revision, DWordConst
  Package()               // Configuration for Wifi
  {
    0x07,                 // DomainType, 0x7  Wi-Fi Core
    0x00,                 // External 32kHz clock Valid/Not Valid
  }
})                        // End of ECKY object

// ECKV (External Clock Valid)
//
// CNV has 32kHz clock which is generated internally and supplied from a crystal Osc, mounted on the M.2 CARD.
// Has high power penalty when Using a 32k sourced from Crystal Osc.
// There is an option to get an external 32k clock from the platform which will save 0.5 mW in sleep state whereas overall current consumption is 1.8mW.
// This method is used to specify that platform does have valid external clock to use it.
//
// Arguments: (0)
//   None
// Return Value:
//
// Name (ECKV, Package()
// {
//   Revision,                        // DWordConst
//   Package()                        // Configuration for both WiFi and Bluetooth
//   {
//     DomainType,                    // DomainType, 0x7:WiFi Core
//     CnvExternal32kHzClock,         // External 32kHz clock Valid/Not Valid
//   }
// })                                 // End of ECKV object
//

Method(ECKV,0,Serialized)
{
  Store(\CECV,Index (DeRefOf (Index (ECKY, 1)), 1))

  Return(ECKY)
}

Name (PPAY, Package()
{
  1,                          // Revision 0 - Legacy , 1 - UHB
  Package()
  {
    0x07,                     // Domain Type
    0x00,                     // Wifi ANT gain BIOS
    0x18,                     // Wifi ANT gain 2400 Chain A
    0x28,                     // Wifi ANT gain 5150-5350 Chain A
    0x28,                     // Wifi ANT gain 5350-5470 Chain A
    0x28,                     // Wifi ANT gain 5470-5725 Chain A
    0x28,                     // Wifi ANT gain 5725-5945 Chain A
    0x28,                     // WiFi ANT gain 5945-6165 Chain A
    0x28,                     // WiFi ANT gain 6165-6405 Chain A
    0x28,                     // WiFi ANT gain 6405-6525 Chain A
    0x28,                     // WiFi ANT gain 6525-6705 Chain A
    0x28,                     // WiFi ANT gain 6705-6865 Chain A
    0x28,                     // WiFi ANT gain 6865-7105 Chain A
    0x18,                     // Wifi ANT gain 2400 Chain B
    0x28,                     // Wifi ANT gain 5150-5350 Chain B
    0x28,                     // Wifi ANT gain 5350-5470 Chain B
    0x28,                     // Wifi ANT gain 5470-5725 Chain B
    0x28,                     // Wifi ANT gain 5725-5945 Chain B
    0x28,                     // WiFi ANT gain 5945-6165 Chain B
    0x28,                     // WiFi ANT gain 6165-6405 Chain B
    0x28,                     // WiFi ANT gain 6405-6525 Chain B
    0x28,                     // WiFi ANT gain 6525-6705 Chain B
    0x28,                     // WiFi ANT gain 6705-6865 Chain B
    0x28,                     // WiFi ANT gain 6865-7105 Chain B
  }
})

// PPAG (Per Platform Antenna Gain)
//
// WiFi has regulatory limits values are set assuming worst case antenna gain (3dBi for LB and 5dBi for HB)
// This method is used to modify the WiFi PPAG typical antenna gain to properly control it
//
// Arguments: (0)
//   None
// Return Value:
//
// Name (PPAG, Package()
// {
//   Revision,                // DWordConst
//   Package()                // Configuration for WiFi
//   {
//     DomainType,            // 0x7:WiFi
//     WifiAntGainEnable,     // Configures the source location of the WiFi PPAG table to be used; 0:Device ignores BIOS configuration; 1:Device uses BIOS configuration
//     WifiAntGain1,          // WiFi Antenna Gain for 2400 MHz Chain A
//     WifiAntGain2,          // WiFi Antenna Gain for 5150-5350 MHz Chain A
//     WifiAntGain3,          // WiFi Antenna Gain for 5350-5470 MHz Chain A
//     WifiAntGain4,          // WiFi Antenna Gain for 5470-5725 MHz Chain A
//     WifiAntGain5,          // WiFi Antenna Gain for 5725-5945 MHz Chain A
//     WifiAntGain6,          // WiFi Antenna Gain for 5945-6165 MHz Chain A
//     WifiAntGain7,          // WiFi Antenna Gain for 6165-6405 MHz Chain A
//     WifiAntGain8,          // WiFi Antenna Gain for 6405-6525 MHz Chain A
//     WifiAntGain9,          // WiFi Antenna Gain for 6525-6705 MHz Chain A
//     WifiAntGain10,         // WiFi Antenna Gain for 6705-6865 MHz Chain A
//     WifiAntGain11,         // WiFi Antenna Gain for 6865-7105 MHz Chain A
//     WifiAntGain12,         // WiFi Antenna Gain for 2400 MHz Chain B
//     WifiAntGain13,         // WiFi Antenna Gain for 5150-5350 MHz Chain B
//     WifiAntGain14,         // WiFi Antenna Gain for 5350-5470 MHz Chain B
//     WifiAntGain15,         // WiFi Antenna Gain for 5470-5725 MHz Chain B
//     WifiAntGain16,         // WiFi Antenna Gain for 5725-5945 MHz Chain B
//     WifiAntGain17,         // WiFi Antenna Gain for 5745-6165 MHz Chain B
//     WifiAntGain18,         // WiFi Antenna Gain for 6165-6405 MHz Chain B
//     WifiAntGain19,         // WiFi Antenna Gain for 6405-6525 MHz Chain B
//     WifiAntGain20,         // WiFi Antenna Gain for 6525-6705 MHz Chain B
//     WifiAntGain21,         // WiFi Antenna Gain for 6705-6865 MHz Chain B
//     WifiAntGain22,         // WiFi Antenna Gain for 6865-7105 MHz Chain B
//   }
// })                         // End of PPAG object
//
Method(PPAG,0,Serialized)
{
  Store(\WAGE,Index (DeRefOf (Index (PPAY, 1)), 1))
  Store(\AGA1,Index (DeRefOf (Index (PPAY, 1)), 2))
  Store(\AGA2,Index (DeRefOf (Index (PPAY, 1)), 3))
  Store(\AGA3,Index (DeRefOf (Index (PPAY, 1)), 4))
  Store(\AGA4,Index (DeRefOf (Index (PPAY, 1)), 5))
  Store(\AGA5,Index (DeRefOf (Index (PPAY, 1)), 6))
  Store(\AGA6,Index (DeRefOf (Index (PPAY, 1)), 7))
  Store(\AGA7,Index (DeRefOf (Index (PPAY, 1)), 8))
  Store(\AGA8,Index (DeRefOf (Index (PPAY, 1)), 9))
  Store(\AGA9,Index (DeRefOf (Index (PPAY, 1)), 10))
  Store(\AGAA,Index (DeRefOf (Index (PPAY, 1)), 11))
  Store(\AGAB,Index (DeRefOf (Index (PPAY, 1)), 12))
  Store(\AGB1,Index (DeRefOf (Index (PPAY, 1)), 13))
  Store(\AGB2,Index (DeRefOf (Index (PPAY, 1)), 14))
  Store(\AGB3,Index (DeRefOf (Index (PPAY, 1)), 15))
  Store(\AGB4,Index (DeRefOf (Index (PPAY, 1)), 16))
  Store(\AGB5,Index (DeRefOf (Index (PPAY, 1)), 17))
  Store(\AGB6,Index (DeRefOf (Index (PPAY, 1)), 18))
  Store(\AGB7,Index (DeRefOf (Index (PPAY, 1)), 19))
  Store(\AGB8,Index (DeRefOf (Index (PPAY, 1)), 20))
  Store(\AGB9,Index (DeRefOf (Index (PPAY, 1)), 21))
  Store(\AGBA,Index (DeRefOf (Index (PPAY, 1)), 22))
  Store(\AGBB,Index (DeRefOf (Index (PPAY, 1)), 23))

  Return(PPAY)
}


Name (WTSY, Package()
{
  0,
  Package()
  {
    0x07,                     // Domain Type
    0x00,                     // TAS Selection: Enable/Disable
    0x00,                     // List entries
    0x00,                     // Blocked Country Code [CC1 -  CC16]
    0x00,
    0x00,
    0x00,
    0x00,
    0x00,
    0x00,
    0x00,
    0x00,
    0x00,
    0x00,
    0x00,
    0x00,
    0x00,
    0x00,
    0x00,
  }
})

// WTAS (WiFi Time Average SAR)
//
// The Time-Average-SAR by computing Tx-Power Integral over time as a sliding window, and forcing the system not to cross over the Budget. Where the budget is the Fixed SAR Limit
// as known today multiplied by the average time span as defined by the standard.
// By default, the feature is disabled
//
// Arguments: (0)
//   None
// Return Value:
//
// Name (WTSY, Package()
// {
//   Revision,                // DWordConst
//   Package()                // Configuration for WiFi
//   {
//     DomainType,            // 0x7:WiFi
//     WifiTASSelection,      // Enable/Disable the TAS feature
//     WifiTASListEntries,    // No. of blocked countries not approved by OEM to support this feature
//     BlockedListEntry1,     // ISO CC to block
//     BlockedListEntry2,
//     BlockedListEntry3,
//     BlockedListEntry4,
//     BlockedListEntry5,
//     BlockedListEntry6,
//     BlockedListEntry7,
//     BlockedListEntry8,
//     BlockedListEntry9,
//     BlockedListEntry10,
//     BlockedListEntry11,
//     BlockedListEntry12,
//     BlockedListEntry13,
//     BlockedListEntry14,
//     BlockedListEntry15,
//     BlockedListEntry16,
//   }
// })                         // End of WTSY object
//
Method(WTAS,0,Serialized)
{
  Store(\WTSE, Index (DeRefOf (Index (WTSY, 1)), 1))
  Store(\WTLE, Index (DeRefOf (Index (WTSY, 1)), 2))
  Store(\BL01, Index (DeRefOf (Index (WTSY, 1)), 3))
  Store(\BL02, Index (DeRefOf (Index (WTSY, 1)), 4))
  Store(\BL03, Index (DeRefOf (Index (WTSY, 1)), 5))
  Store(\BL04, Index (DeRefOf (Index (WTSY, 1)), 6))
  Store(\BL05, Index (DeRefOf (Index (WTSY, 1)), 7))
  Store(\BL06, Index (DeRefOf (Index (WTSY, 1)), 8))
  Store(\BL07, Index (DeRefOf (Index (WTSY, 1)), 9))
  Store(\BL08, Index (DeRefOf (Index (WTSY, 1)), 10))
  Store(\BL09, Index (DeRefOf (Index (WTSY, 1)), 11))
  Store(\BL10, Index (DeRefOf (Index (WTSY, 1)), 12))
  Store(\BL11, Index (DeRefOf (Index (WTSY, 1)), 13))
  Store(\BL12, Index (DeRefOf (Index (WTSY, 1)), 14))
  Store(\BL13, Index (DeRefOf (Index (WTSY, 1)), 15))
  Store(\BL14, Index (DeRefOf (Index (WTSY, 1)), 16))
  Store(\BL15, Index (DeRefOf (Index (WTSY, 1)), 17))
  Store(\BL16, Index (DeRefOf (Index (WTSY, 1)), 18))

  Return(WTSY)
}
