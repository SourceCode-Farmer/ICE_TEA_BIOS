/** @file
  ACPI DSDT table - Discrete DP Mux Switch

@copyright
  INTEL CONFIDENTIAL
  Copyright 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains a 'Sample Driver' and is licensed as such under the terms
  of your license agreement with Intel or your vendor. This file may be modified
  by the user, subject to the additional terms of the license agreement.

@par Specification Reference:
**/

External (\_SB.PC00.GFX0.IPCO, MethodObj)
External (\_SB.SGOV, MethodObj)
External (\_SB.GGOV, MethodObj)
External (\DPIW)                // Discrete DpIn Connection Policy Setup option
External (\DPG1)                // PcdDpMuxGpio
#define DP_ACTIVATE 1           // DP_ACTIVATE = 1 - External
                                // DP_ACTIVATE = 0 - Internal
#define DYNAMIC_SWITCH_ENABLE 2 // Runtime Switch

//
// Discrete Display Mux Switch (while Booting to OS, once per boot only)
// It is a feature allows an internal or external Gfx to tunnel its display
// via discrete TBT/USB4 port(TypeC connector) based on platform preferences
//
// Switching mux to dGfx source by _OSC which indicates booting to OS successfully
// _OSC UUID - 0811B06E-4A27-44F9-8D60-3CBBC22E7B48
//
Method (DDMS)
{
  //
  // Discrete DP Mux Switch - Runtime
  //
  //
  // Report Discrete Runtime Switch Supportivity
  //
  If (LAnd (CondRefOf (\DPIW), LAnd (\DPIW, DYNAMIC_SWITCH_ENABLE))) {
    If (\_SB.PC00.GFX0.IPCO ()) {
      If (CondRefOf (\DPG1)) {
        \_SB.SGOV (\DPG1, DP_ACTIVATE)
      } Else {
      }
    } Else {
    }
  } Else {
  }
}
