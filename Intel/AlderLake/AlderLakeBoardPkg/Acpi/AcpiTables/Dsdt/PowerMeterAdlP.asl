/** @file
  Power Meter Device Configuration

 @copyright
  INTEL CONFIDENTIAL
  Copyright 2020 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains a 'Sample Driver' and is licensed as such under the terms
  of your license agreement with Intel or your vendor. This file may be modified
  by the user, subject to the additional terms of the license agreement.

@par Specification Reference:
**/

Scope(\_SB.PC00.I2C5)
{
  //
  // PA01 Device: Defines how the PAC193x is connected to the I2C Controller.
  //
  Device(PA01)
  {
    Name(_HID, "MCHP1930")
    Name(_UID, 1)

    // Lowest power D-State supported by the device is D3
    Name (_S0W, 3)

    // Device Status: present, enabled, and functioning properly
    Method(_STA, 0x0, NotSerialized)
    {
      If(POME)
      {
        Switch (ToInteger(PLID)) {
          Case (Package () {BoardIdAdlPLp4Rvp, BoardIdAdlPDdr5Rvp, BoardIdAdlPLp5Rvp, BoardIdAdlPDdr4Rvp, BoardIdAdlMLp4Rvp, BoardIdAdlMLp5Rvp, BoardIdAdlPLp4Bep, BoardIdAdlPDdr5Dg384Aep, BoardIdAdlPLp5Aep, BoardIdAdlPLp5Dg128Aep, BoardIdAdlPLp5MbAep, BoardIdAdlPMMAep, BoardIdAdlPLp5Gcs}) {
            Return(0x0F)
          }
        }
      }
      Return (0x00)
    }

    //
    // Current Resources Settings
    //
    Method(_CRS, 0x0, NotSerialized)
    {
      Name(RBUF, ResourceTemplate()
      {
        I2CSerialBus(0x0,                   // 7-bit Peripheral Address
                    ControllerInitiated,    // Peripheral or Controller?
                    400000,                 // Connection Speed in hz
                    AddressingMode7Bit,     // 7-bit or 10-bit addressing?
                    "\\_SB.PC00.I2C5",      // I2C Controller to which PAC is connected
                    0,                      // Resource Index
                    ResourceConsumer,       // Consumer or Producer?
                    I2CB)                    // Descriptor name
      })
      CreateWordField(RBUF,I2CB._ADR,BADR)
      Switch (ToInteger(PLID)) {
        Case (Package () {BoardIdAdlPLp4Rvp, BoardIdAdlPDdr5Rvp, BoardIdAdlPLp5Rvp, BoardIdAdlPDdr4Rvp, BoardIdAdlMLp4Rvp, BoardIdAdlMLp5Rvp, BoardIdAdlPLp4Bep, BoardIdAdlPDdr5Dg384Aep, BoardIdAdlPLp5Aep, BoardIdAdlPLp5Dg128Aep, BoardIdAdlPMMAep}) {
          Store (0x18, BADR)
        }
        Case(Package () {BoardIdAdlPLp5MbAep}) {
          Store (0x14, BADR)
        }
        Case(Package () {BoardIdAdlPLp5Gcs}) {
          Store (0x10, BADR)
        }
        Default {
          Store (0x0, BADR)
        }
      }
      Return(RBUF)
    }

    //
    // _DSM - Device Specific Method
    //
    // This method returns configuration information that tells the driver
    // which devices each line in the PAC193x is wired to monitor.
    // Names of the monitored devices must come from the Microsoft-defined
    // power rail taxonomy.
    //
    // The UUID for the Microchip PAC193x's DSM is {033771E0-1705-47B4-9535-D1BBE14D9A09}.
    // This is unique to the device, and must match what the Windows PAC193x driver expects.
    //
    // Returns:
    //         Either: A Buffer (for Functions 0 and 2, or an error)
    //                 A Package containing EMI resources (rail/resistor pairs)
    //
    // Input Arguments (per _DSM standard):
    //
    //     Arg0: UUID - Function Identifier
    //     Arg1: Integer - Revision
    //     Arg2: Integer - Function Index
    //     Arg3: Package - Parameters (not used in our implementation)
    //
    Function(_DSM, {BuffObj, PkgObj}, {BuffObj, IntObj, IntObj, PkgObj})
    {
      //
      //  Is our UUID being invoked?
      //
      if(LNotEqual(Arg0, ToUUID("033771E0-1705-47B4-9535-D1BBE14D9A09")))
      {
        return(Buffer() {0x0})  // incorrect UUID, return NULL for error
      }

      //
      // Switch based on the function number requested...
      //
      switch(ToInteger(Arg2))
      {
        //
        // Function zero returns a bit-mask of supported functions
        //
        case(0)
        {
          switch(ToInteger(Arg1)) // revision check
          {
            case(0)
            {
              return (Buffer() {0x3}) // Revision 0: function 1 is supported
            }
            case(1)
            {
              return (Buffer() {0x7f}) // Revision 1: functions 1->6 are supported
            }
          }
          break;
        }

        // Function 1 returns the channel "Rail Name" and the Resistor Value expressed in milli-Ohms.
        case(1)
        {
          // All Revisions supported (0 and 1 presently defined)
          Name(PKG1, Package()
          {
            Package()
            {
              // Rail Name, Resistor Value
              "",      0, // Channel 1
              "",      0, // Channel 2
              "",      0, // Channel 3
              "",      0  // Channel 4
            },
            Package()  // ADL P
            {
              // Rail Name, Resistor Value
              "VBATA_VCCCORE_IN",   2,   // Channel 1
              "VBATA_VCCGT_IN",     2,   // Channel 2
              "VCC1P8_CPU",         10,  // Channel 3
              "VCC1P05_CPU",        2    // Channel 4
            },
            Package() // ADL M
            {
              // Rail Name, Resistor Value
              "VBATA_VCCCORE_IN",   2,   // Channel 1
              "VBATA_VCCGT_IN",     2,   // Channel 2
              "VBATA_VCCSA_IN",     2,   // Channel 3
              "VBATA_VCCANA_IN",    5    // Channel 4
            },
            Package()  // ADL P MB AEP
            {
              // Rail Name, Resistor Value
              "",            0,   // Channel 1
              "VCCIN_AUX",   1,   // Channel 2
              "",            0,   // Channel 3
              "",            0    // Channel 4
            },
            Package()  // ADL P GCS
            {
              // Rail Name, Resistor Value
              "SNS_VCCCORE_12", 1,   // Channel 1
              "SNS_VCCCORE_34", 1,   // Channel 2
              "SNS_VDD2_MEM"  , 2,   // Channel 3
              "SNS_VPP"       , 5    // Channel 4
            }
          })
          Switch (ToInteger(PLID)) {
            Case (Package () {BoardIdAdlPLp4Rvp, BoardIdAdlPDdr5Rvp, BoardIdAdlPLp5Rvp, BoardIdAdlPDdr4Rvp, BoardIdAdlPLp4Bep, BoardIdAdlPDdr5Dg384Aep, BoardIdAdlPLp5Aep, BoardIdAdlPLp5Dg128Aep, BoardIdAdlPMMAep}) {
              return(DerefOf(Index(PKG1, 1)))
            }
            Case (Package () {BoardIdAdlMLp4Rvp, BoardIdAdlMLp5Rvp}) {
              return(DerefOf(Index(PKG1, 2)))
            }
            Case (Package () {BoardIdAdlPLp5MbAep}) {
              return(DerefOf(Index(PKG1, 3)))
            }
            Case (Package () {BoardIdAdlPLp5Gcs}) {
              return(DerefOf(Index(PKG1, 4)))
            }
            Default {
              return(DerefOf(Index(PKG1, 0)))
            }
          }
        }

        // Function 2 returns the Resistor values expressed in micro-Ohms.
        case(2)
        {
          If(LLess(Arg1,1)){ break; } // Revision 0 not supported.
          // Return enhanced precision resistor values.
          Name(PKG2, Package()
          {
            Package()
            {
              // Value
              0, // Channel 1
              0, // Channel 2
              0, // Channel 3
              0  // Channel 4
            },
            Package() // ADL P
            {
              // Value
              2000,   // Channel 1
              2000,   // Channel 2
              10000,  // Channel 3
              2000    // Channel 4
            },
            Package() // ADL M
            {
              // Value
              2000,   // Channel 1
              2000,   // Channel 2
              2000,   // Channel 3
              5000    // Channel 4
            },
            Package() // ADL P MB AEP
            {
              // Value
              0,     // Channel 1
              1000,  // Channel 2
              0,     // Channel 3
              0      // Channel 4
            },
            Package() // ADL P GCS
            {
              // Value
              500,   // Channel 1
              500,   // Channel 2
              2000,  // Channel 3
              5000   // Channel 4
            }
          })
          Switch (ToInteger(PLID)) {
            Case (Package () {BoardIdAdlPLp4Rvp, BoardIdAdlPDdr5Rvp, BoardIdAdlPLp5Rvp, BoardIdAdlPDdr4Rvp, BoardIdAdlPLp4Bep, BoardIdAdlPDdr5Dg384Aep, BoardIdAdlPLp5Aep, BoardIdAdlPLp5Dg128Aep, BoardIdAdlPMMAep}) {
              return(DerefOf(Index(PKG2, 1)))
            }
            Case (Package () {BoardIdAdlMLp4Rvp, BoardIdAdlMLp5Rvp}) {
              return(DerefOf(Index(PKG2, 2)))
            }
            Case (Package () {BoardIdAdlPLp5MbAep}) {
              return(DerefOf(Index(PKG2, 3)))
            }
            Case (Package () {BoardIdAdlPLp5Gcs}) {
              return(DerefOf(Index(PKG2, 4)))
            }
            Default {
              return(DerefOf(Index(PKG2, 0)))
            }
          }
        }

        // Function 3 returns the EMI enabled/disabled flags
        case(3)
        {
          If(LLess(Arg1,1)){ break; } // Revision 0 not supported.
          // Return ON/OFF flags.
          Name(BUF3, Package()
          {
            0xF // driver EMI ENABLE mask, CH1:CH2:CH3:CH4 - 1b=ON, 0b=OFF
            // EMI mask bit set but rail name is NULL -> EMI is not created (private channel)
            // channel name valid but mask bit not set -> EMI is not created (private channel)
          })
          return(BUF3)
        }

        // Function 4 returns the channel bipolar settings
        case(4)
        {
          If(LLess(Arg1,1)){ break; } // Revision 0 not supported.
          // Return the target NEG_PWR flags.
          Name(BUF4, Package()
          {
            0x00 // CH1:CH2:CH3:CH4-BIDI : CH1:CH2:CH3:CH4-BIDV
          })
          return(BUF4)
        }

        // Function 5 returns the SPS for ACTIVE and IDLE
        case(5)
        {
        If(LLess(Arg1,1)){ break; } // Revision 0 not supported.
          Name(BUF5, Package()
          {
            1024, // ACTIVE sps - accepted values = {1024, 256, 64, 8}
            8 // IDLE sps - accepted values = {1024, 256, 64, 8}
          })
          return(BUF5)
        }

        // Function 6 returns the watchdog interval
        case(6)
        {
          If(LLess(Arg1,1)){ break; } // Revision 0 not supported.
          Name(BUF6, Package()
          {
            900 // seconds (min=60, max=900)
          })
          return(BUF6)
        }

      } // switch(Arg2)
      //
      // Return an error (a buffer with a value of zero)
      // if we didn't return anything else above
      //
      return(Buffer() {0x0})
    } // _DSM
  } // PowerMeter Device PA01 Scope End

  //
  // PA02 Device: Defines how the PAC193x is connected to the I2C Controller.
  //
  Device(PA02)
  {
    Name(_HID, "MCHP1930")
    Name(_UID, 2)

    // Lowest power D-State supported by the device is D3
    Name (_S0W, 3)

    // Device Status: present, enabled, and functioning properly
    Method(_STA, 0x0, NotSerialized)
    {
      If(POME)
      {
        Switch (ToInteger(PLID)) {
          Case (Package () {BoardIdAdlPLp4Rvp, BoardIdAdlPDdr5Rvp, BoardIdAdlPLp5Rvp, BoardIdAdlPDdr4Rvp, BoardIdAdlMLp4Rvp, BoardIdAdlMLp5Rvp, BoardIdAdlPLp4Bep, BoardIdAdlPDdr5Dg384Aep, BoardIdAdlPLp5Aep, BoardIdAdlPLp5Dg128Aep, BoardIdAdlPLp5MbAep, BoardIdAdlPMMAep, BoardIdAdlPLp5Gcs}) {
            Return(0x0F)
          }
        }
      }
      Return (0x00)
    }

    //
    // Current Resources Settings
    //
    Method(_CRS, 0x0, NotSerialized)
    {
      Name(RBUF, ResourceTemplate()
      {
        I2CSerialBus(0x0,                   // 7-bit Peripheral Address
                    ControllerInitiated,    // Peripheral or Controller?
                    400000,                 // Connection Speed in hz
                    AddressingMode7Bit,     // 7-bit or 10-bit addressing?
                    "\\_SB.PC00.I2C5",      // I2C Controller to which PAC is connected
                    0,                      // Resource Index
                    ResourceConsumer,       // Consumer or Producer?
                    I2CB)                   // Descriptor name
      })
      CreateWordField(RBUF,I2CB._ADR,BADR)
      Switch (ToInteger(PLID)) {
        Case (Package () {BoardIdAdlPLp4Rvp, BoardIdAdlPDdr5Rvp, BoardIdAdlPLp5Rvp, BoardIdAdlPDdr4Rvp, BoardIdAdlMLp4Rvp, BoardIdAdlMLp5Rvp, BoardIdAdlPLp4Bep, BoardIdAdlPDdr5Dg384Aep, BoardIdAdlPLp5Aep, BoardIdAdlPLp5Dg128Aep, BoardIdAdlPMMAep}) {
          Store (0x1E, BADR)
        }
        Case(Package () {BoardIdAdlPLp5MbAep}) {
          Store (0x15, BADR)
        }
        Case(Package () {BoardIdAdlPLp5Gcs}) {
          Store (0x12, BADR)
        }
        Default {
          Store (0x0, BADR)
        }
      }
      Return(RBUF)
    }

    //
    // _DSM - Device Specific Method
    //
    // This method returns configuration information that tells the driver
    // which devices each line in the PAC193x is wired to monitor.
    // Names of the monitored devices must come from the Microsoft-defined
    // power rail taxonomy.
    //
    // The UUID for the Microchip PAC193x's DSM is {033771E0-1705-47B4-9535-D1BBE14D9A09}.
    // This is unique to the device, and must match what the Windows PAC193x driver expects.
    //
    // Returns:
    //         Either: A Buffer (for Functions 0 and 2, or an error)
    //                 A Package containing EMI resources (rail/resistor pairs)
    //
    // Input Arguments (per _DSM standard):
    //
    //     Arg0: UUID - Function Identifier
    //     Arg1: Integer - Revision
    //     Arg2: Integer - Function Index
    //     Arg3: Package - Parameters (not used in our implementation)
    //
    Function(_DSM, {BuffObj, PkgObj}, {BuffObj, IntObj, IntObj, PkgObj})
    {
      //
      //  Is our UUID being invoked?
      //
      if(LNotEqual(Arg0, ToUUID("033771E0-1705-47B4-9535-D1BBE14D9A09")))
      {
        return(Buffer() {0x0})  // incorrect UUID, return NULL for error
      }

      //
      // Switch based on the function number requested...
      //
      switch(ToInteger(Arg2))
      {
        //
        // Function zero returns a bit-mask of supported functions
        //
        case(0)
        {
          switch(ToInteger(Arg1)) // revision check
          {
            case(0)
            {
              return (Buffer() {0x3}) // Revision 0: function 1 is supported
            }
            case(1)
            {
              return (Buffer() {0x7f}) // Revision 1: functions 1->6 are supported
            }
          }
          break;
        }

        // Function 1 returns the channel "Rail Name" and the Resistor Value expressed in milli-Ohms.
        case(1)
        {
          // All Revisions supported (0 and 1 presently defined)
          Name(PKG1, Package()
          {
            Package()
            {
              // Rail Name, Resistor Value
              "", 0, // Channel 1
              "", 0, // Channel 2
              "", 0, // Channel 3
              "", 0  // Channel 4
            },
            Package()  // ADL P
            {
              // Rail Name, Resistor Value
              "VBATA_VCCIN_AUX_IN",        2,  // Channel 1
              "VDD2_CPU",                  2,  // Channel 2
              "VDD2_MEM",                  2,  // Channel 3
              "V1P8U_MEM",                 5   // Channel 4
            },
            Package()  // ADL M
            {
              // Rail Name, Resistor Value
              "VBATA_VCCIN_AUX_IN",        2,  // Channel 1
              "VDD2_CPU",                  2,  // Channel 2
              "VCCPRIM_3P3",               22, // Channel 3
              "VCC1P24_MIPI",              10  // Channel 4
            },
            Package()  // ADL MB AEP
            {
              // Rail Name, Resistor Value
              "VDD1_CPU_MEM",        5,  // Channel 1
              "VDD2_CPU_MEM",        2,  // Channel 2
              "VDDQTX_CPU_MEM",      5,  // Channel 3
              "",                    0   // Channel 4
            },
            Package()  // ADL P GCS
            {
              // Rail Name, Resistor Value
              "SNS_V1P8A",          10,  // Channel 1
              "SNS_VBATA",           2,  // Channel 2
              "SNS_V3P3A_DSW",       2,  // Channel 3
              "SNS_V3P3A_DSW_WLAN",  5   // Channel 4
            },
            Package()  // ADL P DDR5
            {
              // Rail Name, Resistor Value
              "VBATA_VCCIN_AUX_IN",        2,  // Channel 1
              "VDD2_CPU",                  2,  // Channel 2
              "VIN_BULK_DRAM",             5,  // Channel 3
              "",                          0   // Channel 4
            }
          })
          Switch (ToInteger(PLID)) {
            Case (Package () {BoardIdAdlPLp4Rvp, BoardIdAdlPLp5Rvp, BoardIdAdlPDdr4Rvp, BoardIdAdlPLp4Bep, BoardIdAdlPDdr5Dg384Aep, BoardIdAdlPLp5Aep, BoardIdAdlPLp5Dg128Aep, BoardIdAdlPMMAep}) {
              return(DerefOf(Index(PKG1, 1)))
            }
            Case (Package () {BoardIdAdlMLp4Rvp, BoardIdAdlMLp5Rvp}) {
              return(DerefOf(Index(PKG1, 2)))
            }
            Case (Package () {BoardIdAdlPLp5MbAep}) {
              return(DerefOf(Index(PKG1, 3)))
            }
            Case (Package () {BoardIdAdlPLp5Gcs}) {
              return(DerefOf(Index(PKG1, 4)))
            }
            Case (Package () {BoardIdAdlPDdr5Rvp}) {
              return(DerefOf(Index(PKG1, 5)))
            }
            Default {
              return(DerefOf(Index(PKG1, 0)))
            }
          }
        }

        // Function 2 returns the Resistor values expressed in micro-Ohms.
        case(2)
        {
          If(LLess(Arg1,1)){ break; } // Revision 0 not supported.
          // Return enhanced precision resistor values.
          Name(PKG2, Package()
          {
            Package()
            {
              // Value
              0,       // Channel 1
              0,       // Channel 2
              0,       // Channel 3
              0        // Channel 4
            },
            Package()  // ADL P
            {
              // Value
              2000,     // Channel 1
              2000,     // Channel 2
              2000,     // Channel 3
              5000      // Channel 4
            },
            Package()   // ADL M
            {
              // Value
              2000,     // Channel 1
              2000,     // Channel 2
              22000,    // Channel 3
              10000     // Channel 4
            },
            Package()   // ADL P MB AEP
            {
              // Value
              5000,     // Channel 1
              2000,     // Channel 2
              5000,     // Channel 3
              0         // Channel 4
            },
            Package()   // ADL P GCS
            {
              // Value
              10000,    // Channel 1
              2000,     // Channel 2
              2000,     // Channel 3
              5000      // Channel 4
            },
             Package()   // ADL P DDR5
            {
              // Value
              2000,     // Channel 1
              2000,     // Channel 2
              5000,     // Channel 3
              0         // Channel 4
            }
          })
          Switch (ToInteger(PLID)) {
            Case (Package () {BoardIdAdlPLp4Rvp, BoardIdAdlPLp5Rvp, BoardIdAdlPDdr4Rvp, BoardIdAdlPLp4Bep, BoardIdAdlPDdr5Dg384Aep, BoardIdAdlPLp5Aep, BoardIdAdlPLp5Dg128Aep, BoardIdAdlPMMAep}) {
              return(DerefOf(Index(PKG2, 1)))
            }
            Case (Package () {BoardIdAdlMLp4Rvp, BoardIdAdlMLp5Rvp}) {
              return(DerefOf(Index(PKG2, 2)))
            }
            Case (Package () {BoardIdAdlPLp5MbAep}) {
              return(DerefOf(Index(PKG2, 3)))
            }
            Case (Package () {BoardIdAdlPLp5Gcs}) {
              return(DerefOf(Index(PKG2, 4)))
            }
            Case (Package () {BoardIdAdlPDdr5Rvp}) {
              return(DerefOf(Index(PKG2, 5)))
            }
            Default {
              return(DerefOf(Index(PKG2, 0)))
            }
          }
        }

        // Function 3 returns the EMI enabled/disabled flags
        case(3)
        {
          If(LLess(Arg1,1)){ break; } // Revision 0 not supported.
          // Return ON/OFF flags.
          Name(BUF3, Package()
          {
            0xF // driver EMI ENABLE mask, CH1:CH2:CH3:CH4 - 1b=ON, 0b=OFF
            // EMI mask bit set but rail name is NULL -> EMI is not created (private channel)
            // channel name valid but mask bit not set -> EMI is not created (private channel)
          })
          return(BUF3)
        }

        // Function 4 returns the channel bipolar settings
        case(4)
        {
          If(LLess(Arg1,1)){ break; } // Revision 0 not supported.
          // Return the target NEG_PWR flags.
          Name(BUF4, Package()
          {
            0x00 // CH1:CH2:CH3:CH4-BIDI : CH1:CH2:CH3:CH4-BIDV
          })
          return(BUF4)
        }

        // Function 5 returns the SPS for ACTIVE and IDLE
        case(5)
        {
        If(LLess(Arg1,1)){ break; } // Revision 0 not supported.
          Name(BUF5, Package()
          {
            1024, // ACTIVE sps - accepted values = {1024, 256, 64, 8}
            8 // IDLE sps - accepted values = {1024, 256, 64, 8}
          })
          return(BUF5)
        }

        // Function 6 returns the watchdog interval
        case(6)
        {
          If(LLess(Arg1,1)){ break; } // Revision 0 not supported.
          Name(BUF6, Package()
          {
            900 // seconds (min=60, max=900)
          })
          return(BUF6)
        }
      } // switch(Arg2)
      //
      // Return an error (a buffer with a value of zero)
      // if we didn't return anything else above
      //
      return(Buffer() {0x0})
    } // _DSM
  } // PowerMeter Device PA02 Scope End

  //
  // PA03 Device: Defines how the PAC193x is connected to the I2C Controller.
  //
  Device(PA03)
  {
    Name(_HID, "MCHP1930")
    Name(_UID, 3)

    // Lowest power D-State supported by the device is D3
    Name (_S0W, 3)

    // Device Status: present, enabled, and functioning properly
    Method(_STA, 0x0, NotSerialized)
    {
      If(POME)
      {
        Switch (ToInteger(PLID)) {
          Case (Package () {BoardIdAdlPLp4Rvp, BoardIdAdlPDdr5Rvp, BoardIdAdlPLp5Rvp, BoardIdAdlPDdr4Rvp, BoardIdAdlMLp4Rvp, BoardIdAdlMLp5Rvp, BoardIdAdlPLp4Bep, BoardIdAdlPDdr5Dg384Aep, BoardIdAdlPLp5Aep, BoardIdAdlPLp5Dg128Aep, BoardIdAdlPLp5MbAep, BoardIdAdlPMMAep, BoardIdAdlPLp5Gcs}) {
            Return(0x0F)
          }
        }
      }
      Return (0x00)
    }

    //
    // Current Resources Settings
    //
    Method(_CRS, 0x0, NotSerialized)
    {
      Name(RBUF, ResourceTemplate()
      {
        I2CSerialBus(0x0,                   // 7-bit Peripheral Address
                    ControllerInitiated,    // Peripheral or Controller?
                    400000,                 // Connection Speed in hz
                    AddressingMode7Bit,     // 7-bit or 10-bit addressing?
                    "\\_SB.PC00.I2C5",      // I2C Controller to which PAC is connected
                    0,                      // Resource Index
                    ResourceConsumer,       // Consumer or Producer?
                    I2CB)                   // Descriptor name
      })
      CreateWordField(RBUF,I2CB._ADR,BADR)
      Switch (ToInteger(PLID)) {
        Case (Package () {BoardIdAdlPLp4Rvp, BoardIdAdlPDdr5Rvp, BoardIdAdlPLp5Rvp, BoardIdAdlPDdr4Rvp, BoardIdAdlMLp4Rvp, BoardIdAdlMLp5Rvp, BoardIdAdlPLp4Bep, BoardIdAdlPDdr5Dg384Aep, BoardIdAdlPLp5Aep, BoardIdAdlPLp5Dg128Aep, BoardIdAdlPMMAep}) {
          Store (0x11, BADR)
        }
        Case(Package () {BoardIdAdlPLp5MbAep}){
          Store (0x12, BADR)
        }
        Case(Package () {BoardIdAdlPLp5Gcs}){
          Store (0x11, BADR)
        }
        Default {
          Store (0x0, BADR)
        }
      }
      Return(RBUF)
    }

    //
    // _DSM - Device Specific Method
    //
    // This method returns configuration information that tells the driver
    // which devices each line in the PAC193x is wired to monitor.
    // Names of the monitored devices must come from the Microsoft-defined
    // power rail taxonomy.
    //
    // The UUID for the Microchip PAC193x's DSM is {033771E0-1705-47B4-9535-D1BBE14D9A09}.
    // This is unique to the device, and must match what the Windows PAC193x driver expects.
    //
    // Returns:
    //         Either: A Buffer (for Functions 0 and 2, or an error)
    //                 A Package containing EMI resources (rail/resistor pairs)
    //
    // Input Arguments (per _DSM standard):
    //
    //     Arg0: UUID - Function Identifier
    //     Arg1: Integer - Revision
    //     Arg2: Integer - Function Index
    //     Arg3: Package - Parameters (not used in our implementation)
    //
    Function(_DSM, {BuffObj, PkgObj}, {BuffObj, IntObj, IntObj, PkgObj})
    {
      //
      //  Is our UUID being invoked?
      //
      if(LNotEqual(Arg0, ToUUID("033771E0-1705-47B4-9535-D1BBE14D9A09")))
      {
        return(Buffer() {0x0})  // incorrect UUID, return NULL for error
      }

      //
      // Switch based on the function number requested...
      //
      switch(ToInteger(Arg2))
      {
        //
        // Function zero returns a bit-mask of supported functions
        //
        case(0)
        {
          switch(ToInteger(Arg1)) // revision check
          {
            case(0)
            {
              return (Buffer() {0x3}) // Revision 0: function 1 is supported
            }
            case(1)
            {
              return (Buffer() {0x7f}) // Revision 1: functions 1->6 are supported
            }
          }
          break;
        }

        // Function 1 returns the channel "Rail Name" and the Resistor Value expressed in milli-Ohms.
        case(1)
        {
          // All Revisions supported (0 and 1 presently defined)
          Name(PKG1, Package()
          {
            Package()
            {
              // Rail Name, Resistor Value
              "", 0, // Channel 1
              "", 0, // Channel 2
              "", 0, // Channel 3
              "", 0  // Channel 4
            },
            Package()  // ADL P
            {
              // Rail Name, Resistor Value
              "V1P8A_MCP",            5,  // Channel 1
              "V1P8S_MCSI_CAM1",      10, // Channel 2
              "V1P8S_MCSI_CAM2",      10, // Channel 3
              "V3P3A_WWAN",           5   // Channel 4
            },
            Package()  // ADL M
            {
              // Rail Name, Resistor Value
              "V1P8A_MCP",            5,  // Channel 1
              "V1P8S_MCSI_CAM1",      10, // Channel 2
              "V3P3S_MCSI_CAM1",      10, // Channel 3
              "V3P3A_WLAN",           5   // Channel 4
            },
            Package()  // ADL P MB AEP
            {
              // Rail Name, Resistor Value
              "VCCIN",                 1, // Channel 1
              "V3P3X_SSD2",            5, // Channel 2
              "VCCGT",                 1, // Channel 3
              "",                      0  // Channel 4
            },
            Package()  // ADL P GCS
            {
              // Rail Name, Resistor Value
              "SNS_VCCGT",             1, // Channel 1
              "SNS_VBATA_VCCCORE",     0, // Channel 2
              "SNS_VCCIN_AUX",         1, // Channel 3
              "V3P3A_KBC_EC",         50  // Channel 4
            }
          })
          Switch (ToInteger(PLID)) {
            Case (Package () {BoardIdAdlPLp4Rvp, BoardIdAdlPDdr5Rvp, BoardIdAdlPLp5Rvp, BoardIdAdlPDdr4Rvp, BoardIdAdlPLp4Bep, BoardIdAdlPDdr5Dg384Aep, BoardIdAdlPLp5Aep, BoardIdAdlPLp5Dg128Aep, BoardIdAdlPMMAep}) {
              return(DerefOf(Index(PKG1, 1)))
            }
            Case (Package () {BoardIdAdlMLp4Rvp, BoardIdAdlMLp5Rvp}) {
              return(DerefOf(Index(PKG1, 2)))
            }
            Case (Package () {BoardIdAdlPLp5MbAep}) {
              return(DerefOf(Index(PKG1, 3)))
            }
            Case (Package () {BoardIdAdlPLp5Gcs}) {
              return(DerefOf(Index(PKG1, 4)))
            }
            Default {
              return(DerefOf(Index(PKG1, 0)))
            }
          }
        }

        // Function 2 returns the Resistor values expressed in micro-Ohms.
        case(2)
        {
          If(LLess(Arg1,1)){ break; } // Revision 0 not supported.
          // Return enhanced precision resistor values.
          Name(PKG2, Package()
          {
            Package()
            {
              // Value
              0, // Channel 1
              0, // Channel 2
              0, // Channel 3
              0  // Channel 4
            },
            Package()
            {
              // Value
              5000,  // Channel 1
              10000, // Channel 2
              10000, // Channel 3
              5000   // Channel 4
            },
            Package()
            {
              // Value
              1000,  // Channel 1
              5000,  // Channel 2
              1000,  // Channel 3
              0      // Channel 4
            },
            Package()
            {
              // Value
              500,   // Channel 1
              0,     // Channel 2
              500,   // Channel 3
              50000  // Channel 4
            }
          })
          Switch (ToInteger(PLID)) {
            Case (Package () {BoardIdAdlPLp4Rvp, BoardIdAdlPDdr5Rvp, BoardIdAdlPLp5Rvp, BoardIdAdlPDdr4Rvp, BoardIdAdlMLp4Rvp, BoardIdAdlMLp5Rvp, BoardIdAdlPLp4Bep,BoardIdAdlPDdr5Dg384Aep, BoardIdAdlPLp5Aep, BoardIdAdlPLp5Dg128Aep, BoardIdAdlPMMAep}) {
              return(DerefOf(Index(PKG2, 1)))
            }
            Case (Package () {BoardIdAdlPLp5MbAep}) {
              return(DerefOf(Index(PKG2, 2)))
            }
            Case (Package () {BoardIdAdlPLp5Gcs}) {
              return(DerefOf(Index(PKG2, 3)))
            }
            Default {
              return(DerefOf(Index(PKG2, 0)))
            }
          }
        }

        // Function 3 returns the EMI enabled/disabled flags
        case(3)
        {
          If(LLess(Arg1,1)){ break; } // Revision 0 not supported.
          // Return ON/OFF flags.
          Name(BUF3, Package()
          {
            0xF // driver EMI ENABLE mask, CH1:CH2:CH3:CH4 - 1b=ON, 0b=OFF
            // EMI mask bit set but rail name is NULL -> EMI is not created (private channel)
            // channel name valid but mask bit not set -> EMI is not created (private channel)
          })
          return(BUF3)
        }

        // Function 4 returns the channel bipolar settings
        case(4)
        {
          If(LLess(Arg1,1)){ break; } // Revision 0 not supported.
          // Return the target NEG_PWR flags.
          Name(BUF4, Package()
          {
            0x00 // CH1:CH2:CH3:CH4-BIDI : CH1:CH2:CH3:CH4-BIDV
          })
          return(BUF4)
        }

        // Function 5 returns the SPS for ACTIVE and IDLE
        case(5)
        {
        If(LLess(Arg1,1)){ break; } // Revision 0 not supported.
          Name(BUF5, Package()
          {
            1024, // ACTIVE sps - accepted values = {1024, 256, 64, 8}
               8  // IDLE sps - accepted values = {1024, 256, 64, 8}
          })
          return(BUF5)
        }

        // Function 6 returns the watchdog interval
        case(6)
        {
          If(LLess(Arg1,1)){ break; } // Revision 0 not supported.
          Name(BUF6, Package()
          {
            900 // seconds (min=60, max=900)
          })
          return(BUF6)
        }

      } // switch(Arg2)
      //
      // Return an error (a buffer with a value of zero)
      // if we didn't return anything else above
      //
      return(Buffer() {0x0})
    } // _DSM
  } // PowerMeter Device PA03 Scope End

  //
  // PA04 Device: Defines how the PAC193x is connected to the I2C Controller.
  //
  Device(PA04)
  {
    Name(_HID, "MCHP1930")
    Name(_UID, 4)

    // Lowest power D-State supported by the device is D3
    Name (_S0W, 3)

    // Device Status: present, enabled, and functioning properly
    Method(_STA, 0x0, NotSerialized)
    {
      If(POME)
      {
        Switch (ToInteger(PLID)) {
          Case (Package () {BoardIdAdlPLp4Rvp, BoardIdAdlPDdr5Rvp, BoardIdAdlPLp5Rvp, BoardIdAdlPDdr4Rvp, BoardIdAdlMLp4Rvp, BoardIdAdlMLp5Rvp, BoardIdAdlPLp4Bep, BoardIdAdlPDdr5Dg384Aep, BoardIdAdlPLp5Aep, BoardIdAdlPLp5Dg128Aep, BoardIdAdlPLp5MbAep, BoardIdAdlPMMAep, BoardIdAdlPLp5Gcs}) {
            Return(0x0F)
          }
        }
      }
      Return (0x00)
    }

    //
    // Current Resources Settings
    //
    Method(_CRS, 0x0, NotSerialized)
    {
      Name(RBUF, ResourceTemplate()
      {
        I2CSerialBus(0x0,                   // 7-bit Peripheral Address
                    ControllerInitiated,    // Peripheral or Controller?
                    400000,                 // Connection Speed in hz
                    AddressingMode7Bit,     // 7-bit or 10-bit addressing?
                    "\\_SB.PC00.I2C5",      // I2C Controller to which PAC is connected
                    0,                      // Resource Index
                    ResourceConsumer,       // Consumer or Producer?
                    I2CB)                   // Descriptor name
      })
      CreateWordField(RBUF,I2CB._ADR,BADR)
      Switch (ToInteger(PLID)) {
        Case (Package () {BoardIdAdlPLp4Rvp, BoardIdAdlPDdr5Rvp, BoardIdAdlPLp5Rvp, BoardIdAdlPDdr4Rvp, BoardIdAdlMLp4Rvp, BoardIdAdlMLp5Rvp, BoardIdAdlPLp4Bep, BoardIdAdlPDdr5Dg384Aep, BoardIdAdlPLp5Aep, BoardIdAdlPLp5Dg128Aep, BoardIdAdlPMMAep}) {
          Store (0x15, BADR)
        }
        Case(Package () {BoardIdAdlPLp5MbAep}) {
          Store (0x11, BADR)
        }
        Case(Package () {BoardIdAdlPLp5Gcs}) {
          Store (0x13, BADR)
        }
        Default {
          Store (0x0, BADR)
        }
      }
      Return(RBUF)
    }

    //
    // _DSM - Device Specific Method
    //
    // This method returns configuration information that tells the driver
    // which devices each line in the PAC193x is wired to monitor.
    // Names of the monitored devices must come from the Microsoft-defined
    // power rail taxonomy.
    //
    // The UUID for the Microchip PAC193x's DSM is {033771E0-1705-47B4-9535-D1BBE14D9A09}.
    // This is unique to the device, and must match what the Windows PAC193x driver expects.
    //
    // Returns:
    //         Either: A Buffer (for Functions 0 and 2, or an error)
    //                 A Package containing EMI resources (rail/resistor pairs)
    //
    // Input Arguments (per _DSM standard):
    //
    //     Arg0: UUID - Function Identifier
    //     Arg1: Integer - Revision
    //     Arg2: Integer - Function Index
    //     Arg3: Package - Parameters (not used in our implementation)
    //
    Function(_DSM, {BuffObj, PkgObj}, {BuffObj, IntObj, IntObj, PkgObj})
    {
      //
      //  Is our UUID being invoked?
      //
      if(LNotEqual(Arg0, ToUUID("033771E0-1705-47B4-9535-D1BBE14D9A09")))
      {
        return(Buffer() {0x0})  // incorrect UUID, return NULL for error
      }

      //
      // Switch based on the function number requested...
      //
      switch(ToInteger(Arg2))
      {
        //
        // Function zero returns a bit-mask of supported functions
        //
        case(0)
        {
          switch(ToInteger(Arg1)) // revision check
          {
            case(0)
            {
              return (Buffer() {0x3}) // Revision 0: function 1 is supported
            }
            case(1)
            {
              return (Buffer() {0x7f}) // Revision 1: functions 1->6 are supported
            }
          }
          break;
        }

        // Function 1 returns the channel "Rail Name" and the Resistor Value expressed in milli-Ohms.
        case(1)
        {
          // All Revisions supported (0 and 1 presently defined)
          Name(PKG1, Package()
          {
            // Rail Name, Resistor Value
            Package () {
              "",     0, // Channel 1
              "",     0, // Channel 2
              "",     0, // Channel 3
              "",     0  // Channel 4
            },
            Package () {  // ADL P
              "VBATA_CORE",           2, // Channel 1
              "V3P3DX_PCH_SSD",       5, // Channel 2
              "VBATA",                2, // Channel 3
              "V3P3DX_CPU_SSD1",      5  // Channel 4
            },
            Package () {  // ADL M
              "V3P3A_SNSR_HDR",       10, // Channel 1
              "3V3_CONNX44",          5,  // Channel 2
              "VBATA",                2,  // Channel 3
              "3V3_CONNX45",          5   // Channel 4
            },
            Package () {  // ADL P MB AEP
              "",                     0,  // Channel 1
              "V3P3X_SSD",            5,  // Channel 2
              "V3P3X_EDP",           10,  // Channel 3
              "VCC_EDP_BKLT",        10,  // Channel 4
            },
            Package () {  // ADL P GCS
              "SNS_V3P3A_WLAN",        5,  // Channel 1
              "SNS_V3P3DX_EDP",       10,  // Channel 2
              "SNS_VCC_EDP_BKLT",     50,  // Channel 3
              "SNS_V5P0A_2",           2   // Channel 4
            }
          })
          Switch (ToInteger(PLID)) {
            Case (Package () {BoardIdAdlPLp4Rvp, BoardIdAdlPDdr5Rvp, BoardIdAdlPLp5Rvp, BoardIdAdlPDdr4Rvp, BoardIdAdlPLp4Bep, BoardIdAdlPDdr5Dg384Aep, BoardIdAdlPLp5Aep, BoardIdAdlPLp5Dg128Aep, BoardIdAdlPMMAep}) {
              return(DerefOf(Index(PKG1, 1)))
            }
            Case (Package () {BoardIdAdlMLp4Rvp, BoardIdAdlMLp5Rvp}) {
              return(DerefOf(Index(PKG1, 2)))
            }
            Case (Package () {BoardIdAdlPLp5MbAep}) {
              return(DerefOf(Index(PKG1, 3)))
            }
            Case (Package () {BoardIdAdlPLp5Gcs}) {
              return(DerefOf(Index(PKG1, 4)))
            }
            Default {
              return(DerefOf(Index(PKG1, 0)))
            }
          }
        }

        // Function 2 returns the Resistor values expressed in micro-Ohms.
        case(2)
        {
          If(LLess(Arg1,1)){ break; } // Revision 0 not supported.
          // Return enhanced precision resistor values.
          Name(PKG2, Package()
          {
            // Value
            Package () {
              0, // Channel 1
              0, // Channel 2
              0, // Channel 3
              0  // Channel 4
            },
            Package () {  // ADL P
               2000, // Channel 1
               5000, // Channel 2
               2000, // Channel 3
               5000  // Channel 4
            },
            Package () {  // ADL M
               10000, // Channel 1
               5000,  // Channel 2
               2000,  // Channel 3
               5000   // Channel 4
            },
            Package () {  // ADL P MB AEP
               0,      // Channel 1
               5000,   // Channel 2
               10000,  // Channel 3
               10000,  // Channel 4
            },
            Package () {  // ADL P GCS
              5000,    // Channel 1
              10000,   // Channel 2
              50000,   // Channel 3
              2000     // Channel 4
            }
          })
          Switch (ToInteger(PLID)) {
            Case (Package () {BoardIdAdlPLp4Rvp, BoardIdAdlPDdr5Rvp, BoardIdAdlPLp5Rvp, BoardIdAdlPDdr4Rvp, BoardIdAdlPLp4Bep, BoardIdAdlPDdr5Dg384Aep, BoardIdAdlPLp5Aep, BoardIdAdlPLp5Dg128Aep, BoardIdAdlPMMAep}) {
              return(DerefOf(Index(PKG2, 1)))
            }
            Case (Package () {BoardIdAdlMLp4Rvp, BoardIdAdlMLp5Rvp}) {
              return(DerefOf(Index(PKG2, 2)))
            }
            Case (Package () {BoardIdAdlPLp5MbAep}) {
              return(DerefOf(Index(PKG2, 3)))
            }
            Case (Package () {BoardIdAdlPLp5Gcs}) {
              return(DerefOf(Index(PKG2, 4)))
            }
            Default {
              return(DerefOf(Index(PKG2, 0)))
            }
          }
        }

        // Function 3 returns the EMI enabled/disabled flags
        case(3)
        {
          If(LLess(Arg1,1)){ break; } // Revision 0 not supported.
          // Return ON/OFF flags.
          Name(BUF3, Package()
          {
            0xF // driver EMI ENABLE mask, CH1:CH2:CH3:CH4 - 1b=ON, 0b=OFF
            // EMI mask bit set but rail name is NULL -> EMI is not created (private channel)
            // channel name valid but mask bit not set -> EMI is not created (private channel)
          })
          return(BUF3)
        }

        // Function 4 returns the channel bipolar settings
        case(4)
        {
          If(LLess(Arg1,1)){ break; } // Revision 0 not supported.
          // Return the target NEG_PWR flags.
          Name(BUF4, Package()
          {
            0x00 // CH1:CH2:CH3:CH4-BIDI : CH1:CH2:CH3:CH4-BIDV
          })
          return(BUF4)
        }

        // Function 5 returns the SPS for ACTIVE and IDLE
        case(5)
        {
        If(LLess(Arg1,1)){ break; } // Revision 0 not supported.
          Name(BUF5, Package()
          {
            1024, // ACTIVE sps - accepted values = {1024, 256, 64, 8}
               8  // IDLE sps - accepted values = {1024, 256, 64, 8}
          })
          return(BUF5)
        }

        // Function 6 returns the watchdog interval
        case(6)
        {
          If(LLess(Arg1,1)){ break; } // Revision 0 not supported.
          Name(BUF6, Package()
          {
            900 // seconds (min=60, max=900)
          })
          return(BUF6)
        }

      } // switch(Arg2)
      //
      // Return an error (a buffer with a value of zero)
      // if we didn't return anything else above
      //
      return(Buffer() {0x0})
    } // _DSM
  } // PowerMeter Device PA04 Scope End
  //
  // PA05 Device: Defines how the PAC193x is connected to the I2C Controller.
  //
  Device(PA05)
  {
    Name(_HID, "MCHP1930")
    Name(_UID, 5)

    // Lowest power D-State supported by the device is D3
    Name (_S0W, 3)

    // Device Status: present, enabled, and functioning properly
    Method(_STA, 0x0, NotSerialized)
    {
      If(POME)
      {
        Switch (ToInteger(PLID)) {
          Case (Package () {BoardIdAdlPLp5MbAep, BoardIdAdlPLp5Gcs}) {
            Return(0x0F)
          }
        }
      }
      Return (0x00)
    }

    //
    // Current Resources Settings
    //
    Method(_CRS, 0x0, NotSerialized)
    {
      Name(RBUF, ResourceTemplate()
      {
        I2CSerialBus(0x0,                   // 7-bit Peripheral Address
                    ControllerInitiated,    // Peripheral or Controller?
                    400000,                 // Connection Speed in hz
                    AddressingMode7Bit,     // 7-bit or 10-bit addressing?
                    "\\_SB.PC00.I2C5",      // I2C Controller to which PAC is connected
                    0,                      // Resource Index
                    ResourceConsumer,       // Consumer or Producer?
                    I2CB)                   // Descriptor name
      })
      CreateWordField(RBUF,I2CB._ADR,BADR)
      Switch (ToInteger(PLID)) {
        Case(Package () {BoardIdAdlPLp5MbAep}){
          Store (0x13, BADR)
        }
        Case(Package () {BoardIdAdlPLp5Gcs}) {
          Store (0x17, BADR)
        }
        Default {
          Store (0x0, BADR)
        }
      }
      Return(RBUF)
    }

    //
    // _DSM - Device Specific Method
    //
    // This method returns configuration information that tells the driver
    // which devices each line in the PAC193x is wired to monitor.
    // Names of the monitored devices must come from the Microsoft-defined
    // power rail taxonomy.
    //
    // The UUID for the Microchip PAC193x's DSM is {033771E0-1705-47B4-9535-D1BBE14D9A09}.
    // This is unique to the device, and must match what the Windows PAC193x driver expects.
    //
    // Returns:
    //         Either: A Buffer (for Functions 0 and 2, or an error)
    //                 A Package containing EMI resources (rail/resistor pairs)
    //
    // Input Arguments (per _DSM standard):
    //
    //     Arg0: UUID - Function Identifier
    //     Arg1: Integer - Revision
    //     Arg2: Integer - Function Index
    //     Arg3: Package - Parameters (not used in our implementation)
    //
    Function(_DSM, {BuffObj, PkgObj}, {BuffObj, IntObj, IntObj, PkgObj})
    {
      //
      //  Is our UUID being invoked?
      //
      if(LNotEqual(Arg0, ToUUID("033771E0-1705-47B4-9535-D1BBE14D9A09")))
      {
        return(Buffer() {0x0})  // incorrect UUID, return NULL for error
      }

      //
      // Switch based on the function number requested...
      //
      switch(ToInteger(Arg2))
      {
        //
        // Function zero returns a bit-mask of supported functions
        //
        case(0)
        {
          switch(ToInteger(Arg1)) // revision check
          {
            case(0)
            {
              return (Buffer() {0x3}) // Revision 0: function 1 is supported
            }
            case(1)
            {
              return (Buffer() {0x7f}) // Revision 1: functions 1->6 are supported
            }
          }
          break;
        }

        // Function 1 returns the channel "Rail Name" and the Resistor Value expressed in milli-Ohms.
        case(1)
        {
          // All Revisions supported (0 and 1 presently defined)
          Name(PKG1, Package()
          {
            // Rail Name, Resistor Value
            Package () {
              "",     0, // Channel 1
              "",     0, // Channel 2
              "",     0, // Channel 3
              "",     0  // Channel 4
            },
            Package () {  // ADL P MB AEP
              "VBATTERY",            5,  // Channel 1
              "V3P3A_WWAN",          5,  // Channel 2
              "V3P3A_WLAN",          5,  // Channel 3
              "VBATA",               2   // Channel 4
            },
            Package () {  // ADL P GCS
              "SNS_V3P3A_CAM_BRD",  10,  // Channel 1
              "",                    0,  // Channel 2
              "SNS_V5P0A",           2,  // Channel 3
              "SNS_V3P3X_SSD",       5   // Channel 4
            }
          })
          Switch (ToInteger(PLID)) {
            Case (Package () {BoardIdAdlPLp5MbAep}) {
              return(DerefOf(Index(PKG1, 1)))
            }
            Case (Package () {BoardIdAdlPLp5Gcs}) {
              return(DerefOf(Index(PKG1, 2)))
            }
            Default {
              return(DerefOf(Index(PKG1, 0)))
            }
          }
        }

        // Function 2 returns the Resistor values expressed in micro-Ohms.
        case(2)
        {
          If(LLess(Arg1,1)){ break; } // Revision 0 not supported.
          // Return enhanced precision resistor values.
          Name(PKG2, Package()
          {
            // Value
            Package () {
              0, // Channel 1
              0, // Channel 2
              0, // Channel 3
              0  // Channel 4
            },
            Package () {  // ADL P MB AEP
               5000,  // Channel 1
               5000,  // Channel 2
               5000,  // Channel 3
               2000   // Channel 4
            },
            Package () {  // ADL P GCS
              10000,  // Channel 1
              0,      // Channel 2
              2000,   // Channel 3
              5000    // Channel 4
            }
          })
          Switch (ToInteger(PLID)) {
            Case (Package () {BoardIdAdlPLp5MbAep}) {
              return(DerefOf(Index(PKG2, 1)))
            }
            Case (Package () {BoardIdAdlPLp5Gcs}) {
              return(DerefOf(Index(PKG2, 2)))
            }
            Default {
              return(DerefOf(Index(PKG2, 0)))
            }
          }
        }

        // Function 3 returns the EMI enabled/disabled flags
        case(3)
        {
          If(LLess(Arg1,1)){ break; } // Revision 0 not supported.
          // Return ON/OFF flags.
          Name(BUF3, Package()
          {
            0xF // driver EMI ENABLE mask, CH1:CH2:CH3:CH4 - 1b=ON, 0b=OFF
            // EMI mask bit set but rail name is NULL -> EMI is not created (private channel)
            // channel name valid but mask bit not set -> EMI is not created (private channel)
          })
          return(BUF3)
        }

        // Function 4 returns the channel bipolar settings
        case(4)
        {
          If(LLess(Arg1,1)){ break; } // Revision 0 not supported.
          // Return the target NEG_PWR flags.
          Name(BUF4, Package()
          {
            0x00 // CH1:CH2:CH3:CH4-BIDI : CH1:CH2:CH3:CH4-BIDV
          })
          return(BUF4)
        }

        // Function 5 returns the SPS for ACTIVE and IDLE
        case(5)
        {
        If(LLess(Arg1,1)){ break; } // Revision 0 not supported.
          Name(BUF5, Package()
          {
            1024, // ACTIVE sps - accepted values = {1024, 256, 64, 8}
               8  // IDLE sps - accepted values = {1024, 256, 64, 8}
          })
          return(BUF5)
        }

        // Function 6 returns the watchdog interval
        case(6)
        {
          If(LLess(Arg1,1)){ break; } // Revision 0 not supported.
          Name(BUF6, Package()
          {
            900 // seconds (min=60, max=900)
          })
          return(BUF6)
        }

      } // switch(Arg2)
      //
      // Return an error (a buffer with a value of zero)
      // if we didn't return anything else above
      //
      return(Buffer() {0x0})
    } // _DSM
  } // PowerMeter Device PA05 Scope End

} // Scope I2C5
