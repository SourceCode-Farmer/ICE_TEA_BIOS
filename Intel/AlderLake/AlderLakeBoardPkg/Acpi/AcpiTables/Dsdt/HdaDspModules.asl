/** @file

;******************************************************************************
;* Copyright (c) 2019, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/
/** @file
  ACPI DSDT table - 3rd party IP modules supporrt for iSST

@copyright
 Copyright (c) 2016-2021 Intel Corporation. All rights reserved
 This software and associated documentation (if any) is furnished
 under a license and may only be used or copied in accordance
 with the terms of the license. Except as permitted by the
 license, no part of this software or documentation may be
 reproduced, stored in a retrieval system, or transmitted in any
 form or by any means without the express written consent of
 Intel Corporation.
 This file contains a 'Sample Driver' and is licensed as such
 under the terms of your license agreement with Intel or your
 vendor. This file may be modified by the user, subject to
 the additional terms of the license agreement.

@par Specification Reference:
**/

#ifndef  BIT0
#define  BIT0     0x00000001
#endif
#ifndef  BIT1
#define  BIT1     0x00000002
#endif
#ifndef  BIT2
#define  BIT2     0x00000004
#endif
#ifndef  BIT3
#define  BIT3     0x00000008
#endif
#ifndef  BIT4
#define  BIT4     0x00000010
#endif
#ifndef  BIT5
#define  BIT5     0x00000020
#endif
#ifndef  BIT6
#define  BIT6     0x00000040
#endif
#ifndef  BIT7
#define  BIT7     0x00000080
#endif
#ifndef  BIT8
#define  BIT8     0x00000100
#endif
#ifndef  BIT9
#define  BIT9     0x00000200
#endif
#ifndef  BIT10
#define  BIT10    0x00000400
#endif
#ifndef  BIT11
#define  BIT11    0x00000800
#endif
#ifndef  BIT12
#define  BIT12    0x00001000
#endif
#ifndef  BIT13
#define  BIT13    0x00002000
#endif
#ifndef  BIT14
#define  BIT14    0x00004000
#endif
#ifndef  BIT15
#define  BIT15    0x00008000
#endif
#ifndef  BIT16
#define  BIT16    0x00010000
#endif
#ifndef  BIT17
#define  BIT17    0x00020000
#endif
#ifndef  BIT18
#define  BIT18    0x00040000
#endif
#ifndef  BIT19
#define  BIT19    0x00080000
#endif
#ifndef  BIT20
#define  BIT20    0x00100000
#endif
#ifndef  BIT21
#define  BIT21    0x00200000
#endif
#ifndef  BIT22
#define  BIT22    0x00400000
#endif
#ifndef  BIT23
#define  BIT23    0x00800000
#endif
#ifndef  BIT24
#define  BIT24    0x01000000
#endif
#ifndef  BIT25
#define  BIT25    0x02000000
#endif
#ifndef  BIT26
#define  BIT26    0x04000000
#endif
#ifndef  BIT27
#define  BIT27    0x08000000
#endif
#ifndef  BIT28
#define  BIT28    0x10000000
#endif
#ifndef  BIT29
#define  BIT29    0x20000000
#endif
#ifndef  BIT30
#define  BIT30    0x40000000
#endif
#ifndef  BIT31
#define  BIT31    0x80000000
#endif

Scope(\_SB.PC00.HDAS)
{
  //
  // Pre/Post processing modules (3rd Party IP) support for Intel Smart Sound Technology
  //
  Method(PPMS, 1, Serialized)
  {
    // Helper platform method for HDAS._DSM Function 3 - Query Pre/Post Processing Module Support
    // Arg0 (equals Arg3 passed to _DSM) - UUID: Specifies the UUID of the 3rd Party IP module to check (Buffer)
    // Return - TRUE if PP Module supported, else FALSE.

    // ADPM - Platform NVS AudioDSP Post-Processing Module Bit Mask:
    //  [BIT0] - Waves; [BIT1] - DTS; [BIT2] - Intel SST Speech; [BIT3] - Dolby
    //  [BIT4] - Icepower; [BIT5] - Audyssey; [BIT6] - [Unused]
    //  [BIT7] - ForteMedia SAMSoft; [BIT8] - Intel WoV; [BIT9] - Sound Research IP
    //  [BIT10] - Conexant Pre-Process; [BIT11] - Conexant Smart Amp
    //  [BIT12] - Realtek Post-Process; [BIT13] - Realtek Smart Amp
    //  [BIT14] - PP Module Icepower IP MFX sub module; [BIT15] - PP Module Icepower IP EFX sub module
    //  [BIT16] - PP Module Voice Preprocessing; [BIT17] - PP Module Voice Preprocessing
    //  [BIT31:29] - Custom 3rd party IP modules (GUIDs provided from BIOS Setup)

    //
    // Example (to be updated with real GUIDs of supported 3rd party IP):
    //
    // 3rd Party DSP Processing Module placeholder
    // Check support for 3rd Party IP module with GUID AABBCCDD-EEFF-1122-3344-556677889900
    // If (LEqual(Arg0, ToUUID ("AABBCCDD-EEFF-1122-3344-556677889900"))){
    //   Return (1)) // GUID passed to _DSM Function 3 as Arg3 matches - module supported, return true.
    // }
    //
    // Implement for all supported PP modules
    //

    // PP Module Intel SST Speech
    If (LEqual(Arg0, ToUUID ("7C708106-3AFF-40FE-88BE-8C999B3F7445"))){
      Return(And(ADPM, BIT2)) // DSDT NVS ADPM[BIT2] set - supported (return true)
    }

    // PP Module Intel WoV
    If (LEqual(Arg0, ToUUID ("EC774FA9-28D3-424A-90E4-69F984F1EEB7"))){
      Return(And(ADPM, BIT8)) // ADPM[BIT8] set - supported (return true)
    }

    // PP Module 'Alpha' Custom GUID
    // Check PP module with GUID - entered in Setup menu
    If (LEqual(Arg0, ACCG(AG1L, AG1H))){
      Return(And(ADPM, BIT29)) // ADPM[BIT29] set - supported (return true)
    }

    // PP Module 'Beta' Custom GUID
    // Check PP module with GUID - entered in Setup menu
    If (LEqual(Arg0, ACCG(AG2L, AG2H))){
      Return(And(ADPM, BIT30)) // ADPM[BIT30] set - supported (return true)
    }

    // PP Module 'Gamma' Custom GUID
    // Check PP module with GUID - entered in Setup menu
    If (LEqual(Arg0, ACCG(AG3L, AG3H))){
      Return(And(ADPM, BIT31)) // ADPM[BIT31] set - supported (return true)
    }
//[-start-180820-IB15590124-add]//
    // PP Module Waves Post-process
    If (LEqual(Arg0, ToUUID ("B489C2DE-0F96-42E1-8A2D-C25B5091EE49"))){
      Return(And(ADPM, BIT0)) // ADPM[BIT0] set - supported (return true)
    }

    // PP Module DTS
    If (LEqual(Arg0, ToUUID ("E1284052-8664-4FE4-A353-3878F72704C3"))){
      Return(And(ADPM, BIT1)) // ADPM[BIT1] set - supported (return true)
    }

    // PP Module Dolby
//[-start-210615-KEBIN00012-modify]//
#ifdef LCFC_SUPPORT
    If (LEqual(Arg0, ToUUID ("56A9EA76-3745-4CC0-815C-A621FAA8C9CB"))){
#else
    If (LEqual(Arg0, ToUUID ("849F0D73-1678-4D57-8C78-61C548253993"))){
#endif
//[-end-210615-KEBIN00015-modify]//	
      Return(And(ADPM, BIT3)) // ADPM[BIT3] set - supported (return true)
    }

    // PP Module Waves Pre-process
    If (LEqual(Arg0, ToUUID ("347297C3-A6D5-40DB-8120-ACE66BABF491"))){
      Return(And(ADPM, BIT4)) // ADPM[BIT4] set - supported (return true)
    }

    // PP Module Audyssey
    If (LEqual(Arg0, ToUUID ("BD70CE66-7CEE-4277-A91A-D6368FEAF83D"))){
      Return(And(ADPM, BIT5)) // ADPM[BIT5] set - supported (return true)
    }

    // PP Module Maxim Smart AMP
//[-start-210615-KEBIN00012-modify]//
//[-start-210721-QINGLIN0001-modify]//
//#ifdef LCFC_SUPPORT
#if defined(C970_SUPPORT) || defined(C770_SUPPORT)
    If (LEqual(Arg0, ToUUID ("9A44A769-1A51-464A-A54B-1A4084BF370B"))){
#else
    If (LEqual(Arg0, ToUUID ("C75061F3-F2B2-4DCC-8F9F-82ABB4131E66"))){
#endif
//[-end-210721-QINGLIN0001-modify]//
//[-end-210615-KEBIN00015-modify]//
      Return(And(ADPM, BIT6)) // ADPM[BIT6] set - supported (return true)
    }

    // PP Module ForteMedia SAMSoft
    If (LEqual(Arg0, ToUUID ("B3573EFF-6441-4A75-91F7-4281EEC4597D"))){
      Return(And(ADPM, BIT7)) // ADPM[BIT7] set - supported (return true)
    }

    // PP Module Sound Research IP
    If (LEqual(Arg0, ToUUID ("D46F9D72-81A4-47FD-B301-8E39D17C0981"))){
      Return(And(ADPM, BIT9)) // ADPM[BIT9] set - supported (return true)
    }

    // PP Module Conexant Pre-Process
    If (LEqual(Arg0, ToUUID ("F3578986-4400-4ADF-AE7E-CD433CD3F26E"))){
      Return(And(ADPM, BIT10)) // ADPM[BIT10] set - supported (return true)
    }

    // PP Module Conexant Smart Amp
    If (LEqual(Arg0, ToUUID ("2C093145-5895-4699-9DDB-6FEFDC77E85D"))){
      Return(And(ADPM, BIT11)) // ADPM[BIT11] set - supported (return true)
    }

    // PP Module Realtek Post-Process
    If (LEqual(Arg0, ToUUID ("B55EBA24-8349-4DAD-AFC7-8489D23C93EF"))){
      Return(And(ADPM, BIT12)) // ADPM[BIT12] set - supported (return true)
    }

    // PP Module Realtek Smart Amp
    If (LEqual(Arg0, ToUUID ("7111001F-D35F-44D9-81D2-7AC685BED3D7"))){
      Return(And(ADPM, BIT13)) // ADPM[BIT13] set - supported (return true)
    }

    // PP Module Icepower IP MFX sub module
    If (LEqual(Arg0, ToUUID ("88373A01-16A5-469D-A39A-BDEB594178B8"))){
      Return(And(ADPM, BIT14)) // ADPM[BIT14] set - supported (return true)
    }

    // PP Module Icepower IP EFX sub module
    If (LEqual(Arg0, ToUUID ("54CAB206-82CB-4B67-A1EE-FCD45C126DC8"))){
      Return(And(ADPM, BIT15)) // ADPM[BIT15] set - supported (return true)
    }

    // PP Module Icepower IP SFX sub module
    If (LEqual(Arg0, ToUUID ("70002307-DA3E-4E0B-BA03-4C72314B2AAA"))){
      Return(And(ADPM, BIT16)) // ADPM[BIT16] set - supported (return true)
    }

    // PP Module Voice Preprocessing
    If (LEqual(Arg0, ToUUID ("7AEA5E24-E5E9-4D6D-B88F-B505C1ACAB11"))){
      Return(And(ADPM, BIT17)) // ADPM[BIT17] set - supported (return true)
    }
//[-end-180820-IB15590124-add]//  
    Return (0) // GUID not found - module not supported
  }

  Method (ACCG, 2, NotSerialized)
  {
    // ACCG - AudioDSP custom Pre/Post-Processing module GUID create
    // Auxiliary method to be used inside _DSM Function 3 / PPMS() (PP Module support)
    // Argument values passed from BIOS Setup as two 64bit variables
    // Arg0 - First half of GUID  - bits 0-63
    // Arg1 - Second half of GUID - bits 64-127
    // Return - 16 byte buffer containing GUID with the same byte order as ToUUID function
    Name(GBUF, Buffer(16) {})
    Concatenate(Arg0, Arg1, GBUF)
    Return (GBUF)
  }
}
