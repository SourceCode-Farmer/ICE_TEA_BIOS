/** @file
  Intel ACPI Reference Code for External _DSM WiFi Feature Enabling Implementation

@copyright
  INTEL CONFIDENTIAL
  Copyright 2020 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains a 'Sample Driver' and is licensed as such under the terms
  of your license agreement with Intel or your vendor. This file may be modified
  by the user, subject to the additional terms of the license agreement.

@par Specification Reference:
  **/

//--------------------------------------------------
//  Intel Proprietary WiFi Feature Enabling Solution
//--------------------------------------------------
//
// _DSM (Device Specific Method) for external configuration
//
// This optional object is a control method that enables devices to provide device specific control
// functions that are consumed by the device driver.
//
// Arguments: (4)
//   Arg0 - A Buffer containing a UUID
//   Arg1 - An Integer containing the Revision ID
//   Arg2 - An Integer containing the Function Index
//   Arg3 - A Package that contains function-specific arguments
// Return Value:
//   If Function Index = 0, a Buffer containing a function index bit-field which has set of supported function indexes.
//   Otherwise, the return feature value based on set of supported function type which depends on the UUID and revision ID.
//
Method (EFUN, 4, Serialized) {
  //
  // Function 0 : Query Function/Get supported functions (based on revision)
  //              For Revision 0,
  //              -- Return value would contain one bit for each function index. Starting with zero.
  //              -- Bit 0 indicates whether there is support for any functions other than function 0 for the specified UUID and Revision ID.
  //              -- If set to zero, no functions are supported.
  //
  If (LEqual (Arg2, Zero)) {
    // Revision 0
    If (LEqual (Arg1, Zero)) { // The current revision is 0
      //
      // Below are functions defined.
      // Bit0    - Indicates whether its support for any other functions
      // Bit1    - Disable SRD (Short Range Device) Active Channels
      // Bit2    - Supported Indonesia 5.15-5.35 GHz Band
      // Bit3    - Ultra High Band Support
      // Bit4    - DRS (Dynamic Regulatory Sensing) configuration
      // Bit5    - Reserved UART Configurations
      // Bit6    - 11Ax Control
      // Bit7    - Control Enablement UNII-4 on certificated modules
      // Bit8    - Device for Indoor Use Only
      // Bit31:9 - Reserved for future options
      //
      Return (Buffer() {0xFF, 0x01})
    } Else {
      Return (Buffer() {0}) // No supported functions.
    }
  }

  //
  // Function 1 : Disable SRD (Short Range Device) Active Channels
  //
  // ETSI - European Telecommunications Standards Institute & SRD - Short Range Device
  //   00 - ETSI 5.8GHz SRD Active Scan
  //   01 - ETSI 5.8GHz SRD Passive Scan
  //   02 - ETSI 5.8GHz SRD Disabled
  //
  If (LEqual (Arg2, One)) {
    Store (\ACSD, Local0) // Switched Antenna Diversity Selection
    Return (Local0)
  }

  //
  // Function 2 : Supported Indonesia 5.15-5.35 GHz Band
  //
  // 00 - Set 5.15-5.35GHz to Disable in Indonesia
  // 01 - Set 5.15-5.35GHz to Enable (Passive) in Indonesia
  // 02 - Reserved
  //
  If (LEqual (Arg2, 2)) {
    Store (\I5BS, Local0) // Supported Indonesia 5.15-5.35 GHz Band
    Return (Local0)       // Return Set 5.15-5.35GHz to Disable in Indonesia
  }

  //
  // Function 3 : 6GHz/UHB Support
  //
  // Bit0
  //      '0' [Default] , no override - use device settings
  //      '1',            Force disable all countries which are not defined in the following bits
  //
  // Bit1    - USA
  //      '0' [Default] , USA 6GHz disable
  //      '1',            6GHz Allowed in USA (enabled only if Device certified for USA)
  //
  // Bit2    - Rest Of the World, certificate countries that not represent in the Bits.
  //      '0' [Default] , rest of the world 6GHz disable
  //      '1',            6GHz Allowed in  rest of the world (enabled only if Device certified for rest of the world)
  //
  // Bit3    - EU countries
  // Austria, Belgium, Bosnia-Herzegovina, Bulgaria, Croatia, Czech Rep, Cyprus, Denmark, Estonia, Finland, France, Germany,
  // Greece, Hungary, Iceland, Ireland, Italy, Latvia, Liechtenstein, Lithuania, Luxemburg, Macedonia, Malta, Montenegro,
  // Netherlands, Norway, Poland, Portugal, Romania, Slovakia, Slovenia, Spain, Sweden, Switzerland, United Kingdom
  //      '0' [Default] ,  EU countries 6GHz disable
  //      '1',             6GHz Allowed in EU countries (enabled only if Device certified for EU countries)
  //
  // Bit4    - South Korea
  //      '0' [Default] , South Korea 6GHz disable
  //      '1',            6GHz Allowed in South Korea (enabled only if Device certified for South Korea)
  //
  // Bit5    - Brazil
  //      '0' [Default] , Brazil 6GHz disable
  //      '1',            6GHz Allowed in Brazil (enabled only if Device certified for Brazil)
  //
  // Bit6    - Chile
  //      '0' [Default] , Chile 6GHz disable
  //      '1',            6GHz Allowed in Chile (enabled only if Device certified for Chile)
  //
  // Bit7    - Japan
  //      '0' [Default] , Japan 6GHz disable
  //      '1',            6GHz Allowed in Japan (enabled only if Device certified for Japan)
  //
  // Bit31:8 - Reserved, Must to set with Zeros
  //
  If (LEqual (Arg2, 3)) {
    Store (\UHBS, Local0) // UHB support
    Return (Local0)
  }

  //
  // Function 6 : 11Ax Control
  //
  // Bit 0 - Apply changes to country Ukraine. 11Ax Setting within module certification
  // 0 - None. Work with Wi-Fi FW/OTP definitions [Default]
  // 1 - Apply changes.
  //
  // Bit 1 - 11Ax Mode. Effective only if Bit 0 set to 1
  // 0 - Disable 11Ax on country Ukraine [Default]
  // 1 - Enable 11Ax on country Ukraine
  //
  // Bit 2 - Apply changes to country Russia. 11Ax Setting within module certification
  // 0 - None. Work with Wi-Fi FW/OTP definitions [Default]
  // 1 - Apply changes.
  //
  // Bit 3 - 11Ax Mode. Effective only if Bit 2 set to 1
  // 0 - Disable 11Ax on country Russia [Default]
  // 1 - Enable 11Ax on country Russia
  //
  // Bit 31:04 - Reserved
  //
  If (LEqual (Arg2, 6)) {
    Store (\AXMU, Local0)         // 11Ax Mode for Ukraine // MSB
    ShiftLeft (Local0, 1, Local0) // Left shifting the LSB (11Ax Setting for Ukraine) by 1 bit
    Or (Local0, \AXSU, Local0)    // Or the left shifted LSB (11Ax Setting for Ukraine) with MSB (11Ax Mode for Ukraine)

    Store (\AXMR, Local1)         // 11Ax Mode for Russia // MSB
    ShiftLeft (Local1, 1, Local1) // Left shifting the LSB (11Ax Setting for Russia) by 1 bit
    Or (Local1, \AXSR, Local1)    // Or the left shifted LSB (11Ax Setting for Russia) with MSB (11Ax Mode for Russia)
    ShiftLeft (Local1, 2, Local1) // Left Shifting the final value by 2 bits

    Or (local0, local1, local0)   // Or the final results of Ukraine and Russia
    Return (Local0)               // Apply changes
  }

  //
  // Function 4 : Regulatory Configurations
  // 00      - DRS by Driver/FW configuration
  //
  // Bit0    - Enabling DRS for China Location, *Will work only in case NON CHINA SKU. In case that OEM defined also CHINA_SKU in BIOS WRDD table, in this case the Bit will ignored by Driver*
  // Bit31:1 - Reserved
  //
  If (LEqual (Arg2, 4)) {
    Return (\WFRC)  // DRS by Driver/FW configuration
  }

  //
  // Function 5 : UART Configurations - TBD
  // 00      - Default as defined in Intel SW Driver/FW
  //
  If (LEqual (Arg2, 5)) {
    Return (\WFUC)  // Defualt Behaivour as defined in Intel SW Driver/FW
  }

  //
  // Function 7 : Control Enablement UNII-4 over certificate modules
  // 00      - Default as defined in Intel Wi-Fi certificated modules
  //
  // Profiles described by two bits
  //
  // BIT A   - Apply changes over profile XXX ,UNII-4 setting within module certification
  //    '0'  - None. Work with Wi-Fi FW/OTP definitions.
  //    '1'  - Apply changes
  // Note: supported only on Intel UNII-4 certificated NIC.
  //       In case that Module not certificated, the Wi-Fi ignore and disabled UNII-4 on country XXX.
  //
  // BIT B   - OEM UNII-4 Mode. Note: Effective only if BIT A set to '1'
  //    '0'  - Disable UNII-4 over profile XXX
  //    '1'  - Enable UNII-4 over profile XXX
  //
  // FCC
  // Bit 0   - Apply changes over FCC, UNII-4 setting within module certification (Default Intel module definitions)
  // Bit 1   - UNII-4 mode on FCC.
  //
  // ETSI
  // bit 2   - Apply changes over ETSI, UNII-4 setting within module certification (Default Intel module definitions)
  // Bit 3   - UNII-4 mode on ETSI. )
  // Bit31:4 - Reserved shall set to zeros
  //
  If (LEqual (Arg2, 7)) {
    Return (\UNI4)  // Default Behavior as defined in Intel SW Driver/FW
  }

  //
  // Function 8 : Device for Indoor Use Only (From Solar Family TyP/GfP2/GfP4)
  // 00      - Default as defined in Intel Wi-Fi certificated modules 'Not Activated'
  //
  // Enabling the 'Device for Indoor Use Only' is per the definitions Bits:
  // Bit 0   - EU
  // Bit 1   - Japan
  // Bit 2   - China, applied only in case of China BIOS or DRS in China Enabled
  // Bit 3   - USA
  // Bit31:4 - Reserved shall set to zeros
  //
  If (LEqual (Arg2, 8)) {
    Return (\WFIC)  // Default Behavior as defined in Intel SW Driver/FW
  }
  // No other supported functions, but normal return.
  Return (0)
}

//
// Checks if External WiFi specific _DSM is called
//
// @retval    1 if true, 0 otherwise
//
Method (EDSM, 1, Serialized) {
  If (LEqual (Arg0, ToUUID ("F21202BF-8F78-4DC6-A5B3-1F738E285ADE"))) {
    return (1)
  }
  return (0)
}
