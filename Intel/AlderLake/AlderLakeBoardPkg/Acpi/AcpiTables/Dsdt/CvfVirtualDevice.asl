/** @file
  CloverFalls (CVF) Virtual Device
  This ASL file will support the CVF companion die which is interfaced
  via Native IOs or USB IOs Expansion.

@copyright
  INTEL CONFIDENTIAL
  Copyright 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains a 'Sample Driver' and is licensed as such under the terms
  of your license agreement with Intel or your vendor. This file may be modified
  by the user, subject to the additional terms of the license agreement.

@par Specification Reference:
**/

//
// If USB based CVF is selected, Expose VI2C and VGPIO devices to access the IOs(GPIO,I2c) in the USB chip.
//
If (LEqual (CVFS,CVF_INTERFACE_USB)) {
  //
  // Virtual GPIO Device to access the GPIO pins from ucontroller interfaced with CVF.
  //
  Device (VGPO) {
    Name (_UID, "VGPO")

    Method (_HID)
    {
        Return ("INTC1096")
    }

    Name (_DDN, "Intel UsbGpio Device")

  }// End of VGPO

  //
  // Virutal I2C Device to access the I2C controller from ucontroller interfaced with CVF.
  //
  Device (VIC0) {

    Name (_UID, "VIC0")

    Method (_HID)
    {
        Return ("INTC1097")
    }

    Name (_DDN, "Intel UsbI2C Device")

  }// End of VIC0

  Device (VIC1) {

    Name (_UID, "VIC1")

    Method (_HID)
    {
        Return ("INTC1097")
    }

    Name (_DDN, "Intel UsbI2C Device")

  }// End of VIC1

  //
  // Virutal SPI Device to access SPI from ucontroller interfaced with CVF.
  //
  Device (VSPI) {
    Name (_UID, "VSPI")

    Method (_HID)
    {
        Return ("INTC1098")
    }

    Name (_DDN, "Intel UsbSPI Device")

  }// End of VSPI
}