//;******************************************************************************
//;* Copyright (c) 2017 - 2021, Insyde Software Corporation. All Rights Reserved.
//;*
//;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
//;* transmit, broadcast, present, recite, release, license or otherwise exploit
//;* any part of this publication in any form, by any means, without the prior
//;* written permission of Insyde Software Corporation.
//;*
//;******************************************************************************

Name(BufN, 0x0)                 // Buffer Number Offset
Name(MBuf,Buffer(FixedPcdGet32 (PcdDebugSize)){})        // Memory Buffer

OperationRegion(\MDBG,SystemMemory,0x55AA55AA, 0x55AA55AA)  
Field(\MDBG,AnyAcc,Lock,Preserve)
{
      Offset(0),
      MDG0, 32768         // Set total memory for debug code = 4096 * 8
}

//
// ======================   Command  ============================
//
//
// **********************   Show Number  ************************ 
//
Method(DB2H, 1, Serialized)             // Show Byte Number in Memory
{
    if (SRLD) {
      SHOW(arg0)
      MDGC(0x20)
      Store(MBuf, MDG0)
    } else { 
      Return
    }
}

Method(DW2H, 1, Serialized)             // Show Word Number in Memory
{
    Store(arg0, local0)
    ShiftRight(arg0, 8, local1)
    And(local0, 0xff, local0)
    And(local1, 0xff, local1)
    DB2H(local1)
    Decrement(BufN)
    DB2H(local0)
}

Method(DD2H, 1, Serialized)             // Show DWord Number in Memory
{
    Store(arg0, local0)
    ShiftRight(arg0, 16, local1)
    And(local0, 0xffff, local0)
    And(local1, 0xffff, local1)
    DW2H(local1)
    Decrement(BufN)
    DW2H(local0)
}

//
// **********************   Show String    ********************** 
//
Method(MBGS, 1, Serialized)             // Show String in Memory
{
    if (SRLD) {
      Store(Sizeof(arg0), local0)         // Get String Length
      Name(BUFS, Buffer(local0){})
      Store(ARG0, BUFS)
      MDGC(0x20)
//    LINE()
      While(local0) {
          MDGC(Derefof(Index(BUFS, Subtract(Sizeof(arg0), local0))))
          Decrement(local0)    
      }
      Store(MBuf, MDG0)
    } else {
      Return
    }
//    LINE()
}

//
// Writes debug output to serial port
//
Method (SDBG, 1, Serialized)
{
  Switch (OSDP) {
    Case (0) {
      OperationRegion (U3F8, SystemIO, 0x3F8, 0x10)
      Field (U3F8, ByteAcc, Lock, Preserve) {
        TXBF, 8,    // TX Buffer; DLL
        DLM, 8,     // Also IER
        FCR, 8,
        LCR, 8,
      }

      // Init UART
      Store (0x83, LCR)    // Enable DLL, DLM
      Store (0x01, TXBF)   // Set divisor=1, which means baud rate 115200
      Store (0x00, DLM)
      Store (0xE1, FCR)    // Enable FIFO, 64 bytes
      Store (0x03, LCR)    // 8 bits, 1 stop bit, no parity
      Store (0x00, DLM)    // Disable Interrupts

      // Local3 = buffer, Local4 = size, Local5 = iterator
      ToHexString (Arg0, Local3)    // convert argument to Hexadecimal String in case it isn't a string already. If it is, nothing happens.
      Store (Sizeof (Local3), Local4)

      Store (0, Local5)
      While (LLess (Local5, Local4)) {
        Mid (Local3, Local5, 1, TXBF)    // Store() doesn't work. Mid() does. Not sure what's the difference
        Stall (100)
        Increment (Local5)
      }
      Stall (100)
      Store (0xD, TXBF)    // Carriage return
      Stall (100)
      Store (0xA, TXBF)    // New line
    }

    Case (3) {
      OperationRegion (U2CF, SystemMemory, UC02, 0x20)
      Field (U2CF, AnyAcc, NoLock, Preserve) {
        Offset(0x10),
        BAR0, 64
      }
      OperationRegion (U2B0, SystemMemory, And (BAR0, 0xFFFFFFFFFFFFF000), 0x10)
      Field (U2B0, ByteAcc, Lock, Preserve) {
        U2TX, 8,    // RBR, THR, DLL
        U2DH, 8,    // IER, DLH
        U2FC, 8,    // IIR, FCR
        U2LC, 8     // LCR
      }

      // Init UART
      Store (0x83, U2LC)    // Enable DLL, DLM
      Store (0x01, U2TX)    // Set divisor=1, which means baud rate 115200
      Store (0x00, U2DH)
      Store (0xE1, U2FC)    // Enable FIFO, 64 bytes
      Store (0x03, U2LC)    // 8 bits, 1 stop bit, no parity
      Store (0x00, U2DH)    // Disable Interrupts

      // Local3 = buffer, Local4 = size, Local5 = iterator
      ToHexString (Arg0, Local3)    // convert argument to Hexadecimal String in case it isn't a string already. If it is, nothing happens.
      Store (Sizeof (Local3), Local4)

      Store (0, Local5)
      While (LLess (Local5, Local4)) {
        Mid (Local3, Local5, 1, U2TX)    // Store() doesn't work. Mid() does. Not sure what's the difference
        Stall (100)
        Increment (Local5)
      }
      Stall (100)
      Store (0xD, U2TX)    // Carriage return
      Stall (100)
      Store (0xA, U2TX)    // New line
    }
  }
}

Method(ADBG, 1, Serialized)             // Show String in Memory
{
  if (And(SRLD, 0x01)) {
    if (And(SRLD, 0x02)) {
      SDBG(Arg0)
    }
    ToHexString (Arg0, Local1)
    Store(Sizeof(Local1), local0)         // Get String Length
    Name(BUFS, Buffer(local0){})
    Store(Local1, BUFS)
    MDGC(0x20)
    While(local0) {
        MDGC(Derefof(Index(BUFS, Subtract(Sizeof(Local1), local0))))
        Decrement(local0)    
    }
    Store(MBuf, MDG0)
  }
}

//
// **********************  Command function  *********************
//
Method(SHOW, 1, Serialized)             // Show Number
{
    MDGC(NtoC(ShiftRight(arg0, 4)))
    MDGC(NtoC(arg0))
}

Method(LINE, 0, Serialized)             // Change line
{
    Store(BufN, local0)
    And(local0, 0x0f, local0)
    while(local0){
        MDGC(0x0)
        Increment(local0)
        And(local0, 0x0f, local0)
    }    
}

//
// **********************  Show Character  **********************
//
Method(MDGC, 1, Serialized)
{
        Store(arg0, Index(MBuf, BufN))      // Save character to buffer
        Add(BufN, 1, BufN)
        If(LGreater(BufN, FixedPcdGet32 (PcdDebugSizeMask))) {           
            And(BufN, FixedPcdGet32 (PcdDebugSizeMask), BufN)
            UP_L(0xF)                         // Move up 15 line, to avoid frequently moving up it 
        }
}

Method(UP_L, 1, Serialized)             // Move memory up
{
    Store(arg0, local2)                 // get line number
    ShiftLeft(local2, 4, local2)
    MOVE(local2)
    Subtract(FixedPcdGet32 (PcdDebugSize), local2, local3)
    while(local2){
        Store(0x0, Index(MBuf, local3))
        Increment(local3)
        Decrement(local2)
    }    
}

Method(MOVE, 1, Serialized)
{
    Store(arg0, local4)
    Store(0x0, BufN)

    Subtract(FixedPcdGet32 (PcdDebugSize), local4, local5)
    while(local5){    
        Decrement(local5)
        Store(DeRefOf(Index(MBuf,local4)), Index(MBuf, BufN))
        Increment(BufN)
        Increment(local4)
    }
    
}
//
// ****************  Convert INT to ASCII character ***************
//
Method(NtoC, 1, Serialized)                 // Change Number to character
{
    And(arg0, 0x0f, local0)
    If(LLess(local0, 10)) {                 // 0 ~ 9 ASCII code
        Add(local0, 0x30, local0)
    }
    Else {                                  // A ~ F ASCII code
        Add(local0, 0x37, local0)
    }
    Return(local0)
}

