/** @file
  Intel ACPI Reference Code for Intel Internal _DSM WiFi Feature Enabling Implementation

@copyright
  INTEL CONFIDENTIAL
  Copyright 2020 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains a 'Sample Driver' and is licensed as such under the terms
  of your license agreement with Intel or your vendor. This file may be modified
  by the user, subject to the additional terms of the license agreement.

@par Specification Reference:
  **/

External (\_SB.PC00.CNVW.RSTT)
External (\_SB.PC00.CNVW.PRRS)
External (CRFI)

//--------------------------------------------------
//  Intel Proprietary WiFi Feature Enabling Solution
//--------------------------------------------------
//
// _DSM (Device Specific Method) for Intel internal configuration
//
// This optional object is a control method that enables devices to provide device specific control
// functions that are consumed by the device driver.
//
// Arguments: (4)
//   Arg0 - A Buffer containing a UUID
//   Arg1 - An Integer containing the Revision ID
//   Arg2 - An Integer containing the Function Index
//   Arg3 - A Package that contains function-specific arguments
// Return Value:
//   If Function Index = 0, a Buffer containing a function index bit-field which has set of supported function indexes.
//   Otherwise, the return feature value based on set of supported function type which depends on the UUID and revision ID.
//
Method (IFUN, 4, Serialized) {
  //
  // Function 0 : Query Function/Get supported functions (based on revision)
  //              For Revision 0,
  //              -- Return value would contain one bit for each function index. Starting with zero.
  //              -- Bit 0 indicates whether there is support for any functions other than function 0 for the specified UUID and Revision ID.
  //              -- If set to zero, no functions are supported.
  //
  If (LEqual (Arg2, Zero)) {
    // Revision 0
    If(LEqual (Arg1, Zero)) { // The current revision is 0
      //
      // Below are functions defined.
      // Bit0    - Indicates whether its support for any other functions
      // Bit1    - vPro information
      // Bit2    - Dynamic PLDR
      // Bit3    - RFI enablement
      // Bit31:4 - Reserved for future options
      //
      Return (Buffer() {0x0F})
    } Else {
      Return (Buffer() {0}) // No supported functions.
    }
  }

  //
  // Function 1: Platform Information
  // Type: return type DWORD
  // Bit 0 - Is vPRO platform
  //
  If (LEqual (Arg2, 1)) {
    ShiftLeft (CVPR, 0, Local0)
    Return (Local0)
  }

  //
  // Function 2: Dynamic PLDR
  // Arg 3: DWORD
  // Struct PLDR_MODE {
  //   UINT16 CmdType;
  //   UINT16 CmdPayload
  // };
  //
  If (LEqual (Arg2, 2)) {
    CreateWordField (Arg3, 0, CMDT)
    CreateWordField (Arg3, 2, CMDP)

    //
    // Get _PRR Mode
    //
    if (LEqual (CMDT, 1)) {
      If (CondRefOf (\_SB.PC00.CNVW.RSTT)) {
        return (\_SB.PC00.CNVW.RSTT)
      } Else {
        return (0)
      }
    }

    //
    // Set _PRR Mode
    //
    if (LEqual (CMDT, 2)) {
      If (CondRefOf (\_SB.PC00.CNVW.RSTT)) {
        Store (CMDP, \_SB.PC00.CNVW.RSTT)
      }
      return (0)
    }

    //
    // Get Last_PRR status
    //
    if (LEqual (CMDT, 3)) {
      If (CondRefOf (\_SB.PC00.CNVW.PRRS)) {
        return (\_SB.PC00.CNVW.PRRS)
      } Else {
        return (0)
      }
    }

    return (0)
  }

  //
  // Function 3: RFI enablement
  // 0 Feature Enable
  // 1 Feature Disable
  //
  If (LEqual (Arg2, 3)) {
    if (LEqual (CRFI, 1)) {
      Return (0)
    } Else {
      Return (1)
    }
  }

  // No other supported functions, but normal return.
  Return (0)
}

//
// Checks if Internal WiFi specific _DSM is called
//
// @retval    1 if true, 0 otherwise
//
Method (IDSM, 1, Serialized) {
  If (LEqual (Arg0, ToUUID ("7266172C-220B-4B29-814F-75E4DD26B5FD"))) {
    return (1)
  }
  return (0)
}

