/**@file
 UCMC Device of ACPI

@copyright
  INTEL CONFIDENTIAL
  Copyright 2020 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains a 'Sample Driver' and is licensed as such under the terms
  of your license agreement with Intel or your vendor. This file may be modified
  by the user, subject to the additional terms of the license agreement.

@par Specification Reference:
**/
External (\_SB.PC00.I2CS, MethodObj)
External (\_SB.SHPO, MethodObj)
External (UTCE, IntObj)
External (UCMS, IntObj)
External (UCG1, IntObj)
External (UCG2, IntObj)
External (UCG3, IntObj)
External (UCG4, IntObj)

//------------------------
// Expose UCMC devices under I2C based on setup option setting.
//------------------------

Device (PD01) {
  Name (_HID, "INT3515")
  Method (_UID, 0) {
    Return (I2CI)
  }

  Method (_CRS, 0x0, Serialized) {
    Name (SBFI, ResourceTemplate () {
      Interrupt (ResourceConsumer, Level, ActiveLow, Exclusive, , , UCMI) {0}
    })

    // Update APIC interrupt descriptor
    CreateDWordField (SBFI, UCMI._INT, INT1)
    Store (INUM (UCG1), INT1)

    Return (ConcatenateResTemplate (UCMM (I2CX), SBFI))
  }

  Method (_STA, 0x0, Serialized) {
    If (LAnd (UCMS, LAnd (LEqual (UTCE, 1), I2CS (I2CN)))) {
      ADBG ("return 0x0F")
      Return (0x0F)
    } Else {
      ADBG ("return 0x00")
      Return (0x00)
    }
  } // Method (STA)
} //Device (PD01)
