/** @file

;******************************************************************************
;* Copyright (c) 2019, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/
/** @file
  ACPI DSDT table

@copyright
  INTEL CONFIDENTIAL
  Copyright 2011 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains a 'Sample Driver' and is licensed as such under the terms
  of your license agreement with Intel or your vendor. This file may be modified
  by the user, subject to the additional terms of the license agreement.

@par Specification Reference:
**/

#include "PlatformBoardId.h"
#include <Include/AcpiDebug.h>
#include <Register/PmcRegs.h>

// Definitions for INTB (Interrupt descriptor buffer method)
#define INT_LEVEL_TRIG  0
#define INT_EDGE_TRIG   1
#define INT_ACTIVE_LOW  0
#define INT_ACTIVE_HIGH 1

DefinitionBlock (
  "DSDT.aml",
  "DSDT",
  0x02, // DSDT revision.
        // A Revision field value greater than or equal to 2 signifies that integers
        // declared within the Definition Block are to be evaluated as 64-bit values
  "INTEL",   // OEM ID (6 byte string)
  "SKL     ",// OEM table ID  (8 byte string)
  0x0 // OEM version of DSDT table (4 byte Integer)
)

// BEGIN OF ASL SCOPE
{
  External(\_SB.PC00.GFX0.ALSI)
  External(\_SB.PC00.GFX0.CLID)
  External(\_SB.PC00.GFX0.CDCK)
  External(\_SB.PC00.GFX0.CBLV)
  External(\_SB.PC00.GFX0.GSSE)
  External(\_SB.PC00.GFX0.DD1F, DeviceObj)
  External(\_SB.PC00.GFX0.DD2F, DeviceObj)
  External(\_SB.PC00.SAT0.PRT0, DeviceObj)
  External(\_SB.PC00.SAT0.PRT1, DeviceObj)
  External(\_SB.PC00.SAT0.PRT2, DeviceObj)
  External(\_SB.PC00.SAT0.PRT3, DeviceObj)
  External(\_SB.PC00.SAT0.PRT4, DeviceObj)
  External(\_SB.PC00.SAT0.PRT5, DeviceObj)
  External(\_SB.PC00.SAT0.PRT6, DeviceObj)
  External(\_SB.PC00.SAT0.PRT7, DeviceObj)
  External(\_SB.PC00.GFX0.GDCK, MethodObj)
  External(\_SB.PC00.GFX0.GHDS, MethodObj)
  External(\_SB.PC00.GFX0.AINT, MethodObj)
  External(\_SB.PC00.GFX0.GLID, MethodObj)
  External(\_SB.PC00.GFX0.GSCI, MethodObj)
  External(\_SB.PC00.RP01, DeviceObj)
  External(\_SB.PC00.RP02, DeviceObj)
  External(\_SB.PC00.RP03, DeviceObj)
  External(\_SB.PC00.RP04, DeviceObj)
  External(\_SB.PC00.RP05, DeviceObj)
  External(\_SB.PC00.RP06, DeviceObj)
  External(\_SB.PC00.RP07, DeviceObj)
  External(\_SB.PC00.RP08, DeviceObj)
  External(\_SB.PC00.RP09, DeviceObj)
  External(\_SB.PC00.RP10, DeviceObj)
  External(\_SB.PC00.RP11, DeviceObj)
  External(\_SB.PC00.RP12, DeviceObj)
  External(\_SB.PC00.RP13, DeviceObj)
  External(\_SB.PC00.RP14, DeviceObj)
  External(\_SB.PC00.RP15, DeviceObj)
  External(\_SB.PC00.RP16, DeviceObj)
  External(\_SB.PC00.RP17, DeviceObj)
  External(\_SB.PC00.RP18, DeviceObj)
  External(\_SB.PC00.RP19, DeviceObj)
  External(\_SB.PC00.RP20, DeviceObj)
  External(\_SB.PC00.RP21, DeviceObj)
  External(\_SB.PC00.RP22, DeviceObj)
  External(\_SB.PC00.RP23, DeviceObj)
  External(\_SB.PC00.RP24, DeviceObj)
  External(\_SB.PC00.RP25, DeviceObj)
  External(\_SB.PC00.RP26, DeviceObj)
  External(\_SB.PC00.RP27, DeviceObj)
  External(\_SB.PC00.RP28, DeviceObj)
  External(\_SB.PR00._PSS, MethodObj)
  External(\_SB.PR00.LPSS, PkgObj)
  External(\_SB.PR00.TPSS, PkgObj)
  External(\_SB.PR00._PPC, MethodObj)
  External(\_SB.CPPC, IntObj)
  External(\_TZ.TZ00, DeviceObj)
  External(\_TZ.ETMD, IntObj)
  External(\_TZ.FN00._OFF, MethodObj)
  External(\_SB.PC00.TXHC, DeviceObj) // CPU XHCI object of TCSS, it is a must existed object for CPU TCSS
  External (\STD3, FieldUnitObj) // 0: D3 disable. 1: D3Hot support for Storages. 2: D3Cold support for Storages.
  External(DGBA)
  External(\_SB.PC00.PEG1.CLID)
  External(\_SB.PC00.PEG1.CBLV)
  External(\_SB.PC00.PEG1.DLID, MethodObj)

  // Miscellaneous services enabled in Project

  Name (SANB, 0xFFFF0000)  // SA NVS Base address
  Name (SANL, 0xAA55)      // SA NVS Length

  Include ("PcieRpObjCommonExternalInclude.asl")

  Include ("SaNvs.asl")
  Include ("AmlUpd.asl")
  Include ("PlatformNvs.asl")
  Include ("PciTree.asl")
  Include ("TimeAndAlarmDevice.asl")
  Include ("AcpiAssert.asl")
//[-start-190717-IB15410291-add]//
  Include ("OemPlatformNvs.asl")
//[-start-200831-IB17800087-modify]//
#ifdef ACPI_DEBUG_FLAG
  Include ("Memdbg.asl")
#endif
//[-end-200831-IB17800087-modify]//
//[-end-190717-IB15410291-add]//

  if(LEqual(ECR1,1)){
    Scope(\_SB.PC00) {
      //
      // PCI-specific method's GUID
      //
      Name(PCIG, ToUUID("E5C937D0-3553-4d7a-9117-EA4D19C3434D"))
      //
      // PCI's _DSM - an attempt at modular _DSM implementation
      // When writing your own _DSM function that needs to include PCI-specific methods, do this:
      //
      // Method(_YOUR_DSM,4){
      //   if(Lequal(Arg0,PCIG)) { return(PCID(Arg0,Arg1,Arg2,Arg3)) }
      //   ...continue your _DSM by checking different GUIDs...
      //   else { return(0) }
      // }
      //
      Method(PCID, 4, Serialized) {
        If(LEqual(Arg0, PCIG)) {         // PCIE capabilities UUID
          If(LGreaterEqual(Arg1,3)) {                                              // revision at least 3
            If(LEqual(Arg2,0)) { Return (Buffer(2){0x01,0x03}) }                   // function 0: list of supported functions
            If(LEqual(Arg2,8)) { Return (1) }                                      // function 8: Avoiding Power-On Reset Delay Duplication on Sx Resume
            If(LEqual(Arg2,9)) { Return (Package(5){50000,Ones,Ones,50000,Ones}) } // function 9: Specifying Device Readiness Durations
          }
        }
        return (Buffer(1){0})
      }
    }//scope
  }//if

  Scope(\_SB.PC00) {
    //PciCheck, Arg0=UUID, returns true if support for 'PCI delays optimization ECR' is enabled and the UUID is correct
    Method(PCIC,1,Serialized) {
      If(LEqual(ECR1,1)) {
        If(LEqual(Arg0, PCIG)) {
          return (1)
        }
      }
      return (0)
    }

    //
    // Create I2C Bus Resource descriptor for _CRS usage
    // Arg0 - I2C bus address of the connection (Peripheral Address)
    // Arg1 - I2C controller number (Resource Source)
    // Returns buffer with 'I2cSerialBus' resource descriptor
    //
    Method (IICB, 2, Serialized) {
      Switch (ToInteger(Arg1)) {
        Case(0) { Name (IIC0, ResourceTemplate () { I2cSerialBus (0, ControllerInitiated, 400000, AddressingMode7Bit,
          "\\_SB.PC00.I2C0", 0x00, ResourceConsumer, DEV0,) })
          CreateWordField (IIC0, DEV0._ADR, DAD0)
          Store (Arg0, DAD0)
          Return (IIC0) }
        Case(1) { Name (IIC1 , ResourceTemplate () { I2cSerialBus (0, ControllerInitiated, 400000, AddressingMode7Bit,
          "\\_SB.PC00.I2C1", 0x00, ResourceConsumer, DEV1,) })
          CreateWordField (IIC1, DEV1._ADR, DAD1)
          Store (Arg0, DAD1)
          Return (IIC1) }
        Case(2) { Name (IIC2 , ResourceTemplate () { I2cSerialBus (0, ControllerInitiated, 400000, AddressingMode7Bit,
          "\\_SB.PC00.I2C2", 0x00, ResourceConsumer, DEV2,) })
          CreateWordField (IIC2, DEV2._ADR, DAD2)
          Store (Arg0, DAD2)
          Return (IIC2) }
        Case(3) { Name (IIC3 , ResourceTemplate () { I2cSerialBus (0, ControllerInitiated, 400000, AddressingMode7Bit,
          "\\_SB.PC00.I2C3", 0x00, ResourceConsumer, DEV3,) })
          CreateWordField (IIC3, DEV3._ADR, DAD3)
          Store (Arg0, DAD3)
          Return (IIC3) }
        Case(4) { Name (IIC4 , ResourceTemplate () { I2cSerialBus (0, ControllerInitiated, 400000, AddressingMode7Bit,
          "\\_SB.PC00.I2C4", 0x00, ResourceConsumer, DEV4,) })
          CreateWordField (IIC4, DEV4._ADR, DAD4)
          Store (Arg0, DAD4)
          Return (IIC4) }
        Case(5) { Name (IIC5 , ResourceTemplate () { I2cSerialBus (0, ControllerInitiated, 400000, AddressingMode7Bit,
          "\\_SB.PC00.I2C5", 0x00, ResourceConsumer, DEV5,) })
          CreateWordField (IIC5, DEV5._ADR, DAD5)
          Store (Arg0, DAD5)
          Return (IIC5) }
        Case(6) { Name (IIC6 , ResourceTemplate () { I2cSerialBus (0, ControllerInitiated, 400000, AddressingMode7Bit,
          "\\_SB.PC00.I2C6", 0x00, ResourceConsumer, DEV6,) })
          CreateWordField (IIC6, DEV6._ADR, DAD6)
          Store (Arg0, DAD6)
          Return (IIC6) }
        Case(7) { Name (IIC7 , ResourceTemplate () { I2cSerialBus (0, ControllerInitiated, 400000, AddressingMode7Bit,
          "\\_SB.PC00.I2C7", 0x00, ResourceConsumer, DEV7,) })
          CreateWordField (IIC7, DEV7._ADR, DAD7)
          Store (Arg0, DAD7)
          Return (IIC7) }
        Default {Return (0)}
      }
    }

    //
    // Create I2C Bus Resource descriptor for _CRS usage
    // Arg0 - I2C bus address of the connection (Peripheral Address)
    // Arg1 - I2C Device number
    // Returns buffer with 'I2cSerialBus' resource descriptor
    //
    Method (VIIC, 2, Serialized) {
      Switch (ToInteger (Arg1))
      {
        Case (Zero) // When virtual I2C device VIC0 is selected
        {
          Name (VI00, ResourceTemplate () {
              I2cSerialBus (0, ControllerInitiated, 400000,
                  AddressingMode7Bit, "\\_SB.PC00.XHCI.RHUB.HS07.VIC0",
                  0x00, ResourceConsumer, VI0P,)
          })
          Name (VI01, ResourceTemplate () {
              I2cSerialBus (0, ControllerInitiated, 400000,
                  AddressingMode7Bit, "\\_SB.PC00.XHCI.RHUB.HS06.VIC0",
                  0x00, ResourceConsumer, VI0M,)
          })
          Name (VI02, ResourceTemplate () {
              I2cSerialBus (0, ControllerInitiated, 400000,
                  AddressingMode7Bit, "\\_SB.PC00.XHCI.RHUB.HS08.VIC0",
                  0x00, ResourceConsumer, VI0N,)
          })
          Name (VI03, ResourceTemplate () {
              I2cSerialBus (0, ControllerInitiated, 400000,
                  AddressingMode7Bit, "\\_SB.PC00.XHCI.RHUB.HS03.VIC0",
                  0x00, ResourceConsumer, VI0O,)
          })
          // USB Port Mapping:
          // 3 : ADL M AEP
          // 6 : ADL M
          // 7 : ADL P
          // 8 : ADL P AEP
          //
          If(LEqual (CUPN,0x06)) {
            CreateWordField (VI01, VI0M._ADR, DAQ0)
            Store (Arg0, DAQ0)
            Return (VI01) /* \_SB_.PC00.VIIC.VI01 */
          } ElseIf(LEqual (CUPN,0x07)) {
            CreateWordField (VI00, VI0P._ADR, DAR0)
            Store (Arg0, DAR0)
            Return (VI00) /* \_SB_.PC00.VIIC.VI00 */
          } ElseIf(LEqual (CUPN,0x08)) {
            CreateWordField (VI02, VI0N._ADR, DAS0)
            Store (Arg0, DAS0)
            Return (VI02) /* \_SB_.PC00.VIIC.VI02 */
          } ElseIf(LEqual (CUPN,0x03)) {
            CreateWordField (VI03, VI0O._ADR, DAT0)
            Store (Arg0, DAT0)
            Return (VI03) /* \_SB_.PC00.VIIC.VI03 */
          }
          Return (ResourceTemplate () {I2cSerialBus (0, ControllerInitiated, 400000,AddressingMode7Bit, "\\_SB.PC00.XHCI.RHUB.HSXX.VIC0",0x00, ResourceConsumer, ,)}) // Dummy
        }
        Case (One) // When virtual I2C device VIC1 is selected
        {
          Name (VI10, ResourceTemplate () {
              I2cSerialBus (0, ControllerInitiated, 400000,
                  AddressingMode7Bit, "\\_SB.PC00.XHCI.RHUB.HS07.VIC1",
                  0x00, ResourceConsumer, VI1P,)
          })
          Name (VI11, ResourceTemplate () {
              I2cSerialBus (0, ControllerInitiated, 400000,
                  AddressingMode7Bit, "\\_SB.PC00.XHCI.RHUB.HS06.VIC1",
                  0x00, ResourceConsumer, VI1M,)
          })
          Name (VI12, ResourceTemplate () {
              I2cSerialBus (0, ControllerInitiated, 400000,
                  AddressingMode7Bit, "\\_SB.PC00.XHCI.RHUB.HS08.VIC1",
                  0x00, ResourceConsumer, VI1N,)
          })
          Name (VI13, ResourceTemplate () {
              I2cSerialBus (0, ControllerInitiated, 400000,
                  AddressingMode7Bit, "\\_SB.PC00.XHCI.RHUB.HS03.VIC1",
                  0x00, ResourceConsumer, VI1O,)
          })
          // USB Port Mapping:
          // 3 : ADL M AEP
          // 6 : ADL M
          // 7 : ADL P
          // 8 : ADL P AEP
          //
          If(LEqual (CUPN,0x06)) {
            CreateWordField (VI11, VI1M._ADR, DAQ1)
            Store (Arg0, DAQ1)
            Return (VI11) /* \_SB_.PC00.VIIC.VI11 */
          } ElseIf(LEqual (CUPN,0x07)) {
            CreateWordField (VI10, VI1P._ADR, DAR1)
            Store (Arg0, DAR1)
            Return (VI10) /* \_SB_.PC00.VIIC.VI10 */
          } ElseIf(LEqual (CUPN,0x08)) {
            CreateWordField (VI12, VI1N._ADR, DAS1)
            Store (Arg0, DAS1)
            Return (VI12) /* \_SB_.PC00.VIIC.VI12 */
          } ElseIf(LEqual (CUPN,0x03)) {
            CreateWordField (VI13, VI1O._ADR, DAT1)
            Store (Arg0, DAT1)
            Return (VI13) /* \_SB_.PC00.VIIC.VI13 */
          }
          Return (ResourceTemplate () {I2cSerialBus (0, ControllerInitiated, 400000,AddressingMode7Bit, "\\_SB.PC00.XHCI.RHUB.HSXX.VIC1",0x00, ResourceConsumer, ,)}) // Dummy
        }
        Default
        {
          Return (ResourceTemplate () {I2cSerialBus (0, ControllerInitiated, 400000,AddressingMode7Bit, "\\_SB.PC00.XHCI.RHUB.HSXX.VICX",0x00, ResourceConsumer, ,)}) // Dummy
        }
      }
    }

    //
    // Create Interrupt Resource descriptor for _CRS usage
    // Arg0 - GPIO Pad used as Interrupt source
    // Arg1 - Edge (1) or Level (0) interrupt triggering
    // Arg2 - Active Level: High (1) or Low (0)
    // Returns buffer with 'Interrupt' resource descriptor
    //
    Method (INTB, 3, Serialized) {
      Name (INTR, ResourceTemplate () { Interrupt (ResourceConsumer, Level, ActiveLow, ExclusiveAndWake,,,INTI) {0} })
      // Update APIC Interrupt descriptor
      CreateDWordField (INTR, INTI._INT, NUMI) // Interrupt Number
      Store (INUM(Arg0), NUMI)
      CreateBitField (INTR, INTI._HE, LEVI) // Level or Edge
      Store (Arg1, LEVI)
      CreateBitField (INTR, INTI._LL, ACTI) // Active High or Low
      Store (Arg2, ACTI)

      Return (INTR)
    }

  } // END Scope(\_SB.PC00)

  Include ("Pch.asl")  // Not in this package. Refer to the PCH Reference Code accordingly
  Include ("LpcB.asl")
  Include ("Platform.asl")
  Include ("Cpu.asl")
  Include ("PciDrc.asl")
  Include ("Video.asl")
  Include ("PlatformGnvsTestMenu.asl")
  Include ("Gpe.asl")
  Include ("PcieDock.asl")
  Include ("PchRpPxsxWrapper.asl")
  Include ("WifiDynamicSar.asl")
  Include ("Pep.asl")
  Include ("Psm.asl")
  Include ("Connectivity.asl")
  Include ("MipiCamSensors.asl")
  Include ("SerialIoDevices.asl")
  Include ("HdaSoundWireDev.asl")
  Include ("HdaSlaveDevices.asl")
  Include ("HdaDspModules.asl")
  Include ("HidPlatformEventDev.asl")
  Include ("HidDynamicSarDsm.asl")
  Include ("DiscreteDPMuxSwitch.asl")
#if FixedPcdGetBool(PcdEcEnable) == 1
  Include ("HIDWakeDSM.asl")
#endif
  Include ("PinDriverLib.asl")
  Include ("Pld.asl")
  Include ("WirelessConfigDevice.asl")
  If(LAnd(LNotEqual(CVFS,0),LLessEqual(CVFS,2))) {
    Include("Cvf.asl")
  }

#if FixedPcdGetBool(PcdAdlLpSupport) == 1
  If(LEqual(LP5E,1)) {
    Include("ClosedLidWoVLed.asl")
  }
#endif

  If(LEqual(VMDE,1)) {
    Name(VDSD, 1) // VMD _DSD Method - 1: Expose 0: Hide

    If (LEqual (\STD3, 0)) {
      Store (0, VDSD)
    }
    Include("Vmd.asl")
  }

  Scope(\_SB.PC00.RP01) {
    Method(_PRW, 0) {
      If (LAnd(LNotEqual(PSW3, 0), LEqual(RPN3, 0x01))) {
        Return (GPRW (GGPE (PSW3), 4))
      } ElseIf (LAnd(LNotEqual(PSW2, 0), LEqual(RPN2, 0x01))) {
        Return (GPRW (GGPE (PSW2), 4))
      } Else {
        Return (GPRW (GPE_PCI_EXP, 4))
      }
    }
  }
  Scope(\_SB.PC00.RP02) {
    Method(_PRW, 0) {
      If (LAnd(LNotEqual(WWKP, 0), LEqual(WWRP, 0x02))) {
        Return (GPRW (GGPE (WWKP), 4))
      } Else {
        Return (GPRW (GPE_PCI_EXP, 4))
      }
    }
  }
  Scope(\_SB.PC00.RP03) {
    Method(_PRW, 0) {
      If (LAnd(LNotEqual(WWKP, 0), LEqual(WWRP, 0x03))) {
        Return (GPRW (GGPE (WWKP), 4))
      } Else {
        Return (GPRW (GPE_PCI_EXP, 4))
      }
    }
  }
  Scope(\_SB.PC00.RP04) {
    Method(_PRW, 0) {
      If (LAnd(LNotEqual(WLWK, 0), LEqual(WLRP, 0x04))) {
        Return (GPRW (GGPE (WLWK), 4))
      } ElseIf (LAnd(LNotEqual(WWKP, 0), LEqual(WWRP, 0x04))) {
        Return (GPRW (GGPE (WWKP), 4))
      } Else {
        Return (GPRW (GPE_PCI_EXP, 4))
      }
    }
  }
  Scope(\_SB.PC00.RP05) {
    Method(_PRW, 0) {
      If (LOr((LAnd(LNotEqual(PSW3, 0), LEqual(RPN3, 0x05))) ,(LAnd(LNotEqual(WLWK, 0), LEqual(WLRP, 0x05))))) {
        If (LAnd(LNotEqual(PSW3, 0), LEqual(RPN3, 0x05))) {
          Return (GPRW (GGPE (PSW3), 4))
        } ElseIf (LAnd(LNotEqual(WLWK, 0), LEqual(WLRP, 0x05))) {
          Return (GPRW (GGPE (WLWK), 4))
        } Else {
          Return (GPRW (GPE_PCI_EXP, 4))
        }
      } ElseIf (LAnd(LNotEqual(PSW2, 0), LEqual(RPN2, 0x05))) {
          Return (GPRW (GGPE (PSW2), 4))
      } Else {
        Return (GPRW (GPE_PCI_EXP, 4))
      }
    }
  }
  Scope(\_SB.PC00.RP06) {
    Method(_PRW, 0) {
      If (LAnd(LNotEqual(WWKP, 0), LEqual(WWRP, 0x06))) {
        Return (GPRW (GGPE (WWKP), 4))
      } Else {
        Return (GPRW (GPE_PCI_EXP, 4))
      }
    }
  }
  Scope(\_SB.PC00.RP07) {
    Method(_PRW, 0) {
      If (LAnd(LNotEqual(PSWP, 0), LEqual(RPNB, 0x07))) {
        Return (GPRW (GGPE (PSWP), 4))
      } ElseIf (LAND(LEqual(FVSP,1), LAnd(LNotEqual(FVWP, 0), LEqual(RPFV, 0x07)))) {
        Return (GPRW (GGPE (FVWP), 4))
      } Else {
        Return (GPRW (GPE_PCI_EXP, 4))
      }
    }
  }
  Scope(\_SB.PC00.RP08) {
    Method(_PRW, 0) {
      If (LAnd(LNotEqual(WLWK, 0), LEqual(WLRP, 0x08))) {
        Return (GPRW (GGPE (WLWK), 4))
      } ElseIf (LAnd(LNotEqual(PSWP, 0), LEqual(RPNB, 0x08))) {
        Return (GPRW (GGPE (PSWP), 4))
      } Else {
        Return (GPRW (GPE_PCI_EXP, 4))
      }
    }
  }
  Scope(\_SB.PC00.RP09) {
    Method(_PRW, 0) {
      If (LAnd(LNotEqual(PSW2, 0), LEqual(RPN2, 0x09))) {
        Return (GPRW (GGPE (PSW2), 4))
      } ElseIf (LAnd(LNotEqual(PSW3, 0), LEqual(RPN3, 0x09))) {
        Return (GPRW (GGPE (PSW3), 4))
      } ElseIf (LAnd(LNotEqual(PSWP, 0), LEqual(RPNB, 0x09))) {
        Return (GPRW (GGPE (PSWP), 4))
      } ElseIf (LAnd(LNotEqual(OMW1, 0), LEqual(RPO1, 0x09))) {
        Return (GPRW (GGPE (OMW1), 4))
      } Else {
        Return (GPRW (GPE_PCI_EXP, 4))
      }
    }
  }
  Scope(\_SB.PC00.RP10) {
    Method(_PRW, 0) {
      If (LAnd(LNotEqual(WLWK, 0), LEqual(WLRP, 0x0A))) {
        Return (GPRW (GGPE (WLWK), 4))
      } Else {
        Return (GPRW (GPE_PCI_EXP, 4))
      }
    }
  }
  Scope(\_SB.PC00.RP11) {
    Method(_PRW, 0) {
      Return (GPRW (GPE_PCI_EXP, 4))
    }
  }
  Scope(\_SB.PC00.RP12) {
    Method(_PRW, 0) {
      Return (GPRW (GPE_PCI_EXP, 4))
    }
  }
  Scope(\_SB.PC00.RP13) {
    Method(_PRW, 0) {
      If (LAnd(LNotEqual(PSWP, 0), LEqual(RPNB, 0x0D))) {
        Return (GPRW (GGPE (PSWP), 4))
      } Else {
        Return (GPRW (GPE_PCI_EXP, 4))
      }
    }
  }
  Scope(\_SB.PC00.RP14) {
    Method(_PRW, 0) {
      Return (GPRW (GPE_PCI_EXP, 4))
    }
  }
  Scope(\_SB.PC00.RP15) {
    Method(_PRW, 0) {
      Return (GPRW (GPE_PCI_EXP, 4))
    }
  }
  Scope(\_SB.PC00.RP16) {
    Method(_PRW, 0) {
      Return (GPRW (GPE_PCI_EXP, 4))
    }
  }
  Scope(\_SB.PC00.RP17) {
    Method(_PRW, 0) {
      Return (GPRW (GPE_PCI_EXP, 4))
    }
  }
  Scope(\_SB.PC00.RP18) {
    Method(_PRW, 0) {
      Return (GPRW (GPE_PCI_EXP, 4))
    }
  }
  Scope(\_SB.PC00.RP19) {
    Method(_PRW, 0) {
      Return (GPRW (GPE_PCI_EXP, 4))
    }
  }
  Scope(\_SB.PC00.RP20) {
    Method(_PRW, 0) {
      Return (GPRW (GPE_PCI_EXP, 4))
    }
  }
  Scope(\_SB.PC00.RP21) {
    Method(_PRW, 0) {
      If (LAnd(LNotEqual(PSWP, 0), LEqual(RPNB, 0x15))) {
        Return (GPRW (GGPE (PSWP), 4))
      } Else {
        Return (GPRW (GPE_PCI_EXP, 4))
      }
    }
  }
  Scope(\_SB.PC00.RP22) {
    Method(_PRW, 0) {
      Return (GPRW (GPE_PCI_EXP, 4))
    }
  }
  Scope(\_SB.PC00.RP23) {
    Method(_PRW, 0) {
      Return (GPRW (GPE_PCI_EXP, 4))
    }
  }
  Scope(\_SB.PC00.RP24) {
    Method(_PRW, 0) {
      Return (GPRW (GPE_PCI_EXP, 4))
    }
  }
  Scope(\_SB.PC00.RP25) {
    Method(_PRW, 0) {
      If (LAnd(LNotEqual(PSW2, 0), LEqual(RPN2, 0x19))) {
        Return (GPRW (GGPE (PSW2), 4))
      } ElseIf (LAnd(LNotEqual(OMW1, 0), LEqual(RPO1, 0x19))) {
        Return (GPRW (GGPE (OMW1), 4))
      } Else {
        Return (GPRW (GPE_PCI_EXP, 4))
      }
    }
  }
  Scope(\_SB.PC00.RP26) {
    Method(_PRW, 0) {
      Return (GPRW (GPE_PCI_EXP, 4))
    }
  }
  Scope(\_SB.PC00.RP27) {
    Method(_PRW, 0) {
      Return (GPRW (GPE_PCI_EXP, 4))
    }
  }
  Scope(\_SB.PC00.RP28) {
    Method(_PRW, 0) {
      Return (GPRW (GPE_PCI_EXP, 4))
    }
  }

#if FixedPcdGetBool(PcdEcEnable) == 1
  //
  // Enable SCI wake for LID SWITCH
  //
  If(LNotEqual(LSWP, 0)) {
//[-start-210219-IB14630474-add]//
#if FeaturePcdGet (PcdUseCrbEcFlag)   
//[-end-210219-IB14630474-add]// 
    Scope(\_SB.PC00.LPCB.H_EC.LID0) {
      Method (_PRW, 0) {
        Return (GPRW (GGPE (LSWP), 3))
      }
    }
//[-start-210219-IB14630474-add]//
#endif      
//[-end-210219-IB14630474-add]//    
  }
#endif

  //
  // Add WWAN Firmware Flash Device Support
  //
  If (LEqual(WFFD, 1)) {
    Include("WwanFlash.asl")
  }

  Name(\_S0, Package(4){0x0,0x0,0,0}) // mandatory System state
  if(SS1) { Name(\_S1, Package(4){0x1,0x0,0,0})}
//[-start-190722-IB17700055-modify]//
#if FixedPcdGetBool (PcdAcpiS3Enable)
  if(SS3) { Name(\_S3, Package(4){0x5,0x0,0,0})}
#endif  
//[-end-190722-IB17700055-modify]//  
  if(SS4) { Name(\_S4, Package(4){0x6,0x0,0,0})}
  Name(\_S5, Package(4){0x7,0x0,0,0}) // mandatory System state

  Method(PTS, 1) {        // METHOD CALLED FROM _PTS PRIOR TO ENTER ANY SLEEP STATE
    If(Arg0)              // entering any sleep state
      {
      }
    }
    Method(WAK, 1) {      // METHOD CALLED FROM _WAK RIGHT AFTER WAKE UP
  }

  Scope(\_SB.PC00.SAT0) {
    //
    // Check for platform defined _DSM method support
    //
    // Arg0 - _DSM GUID
    //
    Method(SATC, 1, Serialized) {
      If(LEqual(Arg0, ToUUID("E4DB149B-FCFE-425b-A6D8-92357D78FC7F"))) {
        Return (1)
      } Else {
        Return (0)
      }
    }

    //
    // Execute platform defined _DSM method
    //
    // Arg0 - _DSM GUID
    // Arg1 - _DSM revision
    // Arg2 - Function index
    // Arg3 - Package with parameters
    //
    Method(SATD, 4, Serialized) {
      Switch (ToInteger(Arg2)) {
        Case (0) {
          ///Case 0: Standard query - A bitmask of functions supported
          Return(Buffer() {0x3})
        }
        Default {
          Return (0)
        }
      }
      Return(Buffer() {0x0})
    }

    //
    // ACPI Method to support PUIS capability for each Sata port
    //
    If (LAnd(LEqual(\S0ID,1), LEqual(\PUIS,1))) {
      Scope(PRT0) {
        Include ("SataPuis.asl")
      }

      Scope(PRT1) {
        Include ("SataPuis.asl")
      }

      Scope(PRT2) {
        Include ("SataPuis.asl")
      }

      Scope(PRT3) {
        Include ("SataPuis.asl")
      }

      Scope(PRT4) {
        Include ("SataPuis.asl")
      }

      Scope(PRT5) {
        Include ("SataPuis.asl")
      }

      If(CondRefOf(PRT6)) {
        Scope(PRT6) {
          Include ("SataPuis.asl")
        }
      }

      If(CondRefOf(PRT7)) {
        Scope(PRT7) {
          Include ("SataPuis.asl")
        }
      }
    }
  }

  //
  // PCH xDCI scope for platform specific functions
  //
  Scope (\_SB.PC00.XDCI) {
    Method(XDAT, 0, Serialized) {
      If (CondRefOf (\_SB.PC00.LPCB.H_EC.XDAT)) {
//[-start-220112-IB19270008-modify]//
#if FeaturePcdGet (PcdUseCrbEcFlag)
        Return (\_SB.PC00.LPCB.H_EC.XDAT ())
#endif
//[-end-220112-IB19270008-modify]//
      }
      Return (0)
    }
  }
}// End of ASL File
