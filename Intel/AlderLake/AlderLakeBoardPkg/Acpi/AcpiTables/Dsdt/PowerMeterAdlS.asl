/** @file
  Power Meter Device Configuration for Alderlake-S

 @copyright
  INTEL CONFIDENTIAL
  Copyright 2020 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains a 'Sample Driver' and is licensed as such under the terms
  of your license agreement with Intel or your vendor. This file may be modified
  by the user, subject to the additional terms of the license agreement.

@par Specification Reference:
**/

Scope(\_SB.PC00.I2C0)
{
  //
  // PA01 Device: Defines how the PAC193x is connected to the I2C Controller.
  //
  Device(PA01)
  {
    Name(_HID, "MCHP1930")
    Name(_UID, 1)

    // Lowest power D-State supported by the device is D3
    Name (_S0W, 3)

    // Device Status: present, enabled, and functioning properly
    Method(_STA, 0x0, NotSerialized)
    {
      If(POME)
      {
        Switch (ToInteger(PLID)) {
          Case (Package () {BoardIdAdlSAdpSDdr4UDimm2DCrb, BoardIdAdlSAdpSDdr4UDimm2DCrbEv, BoardIdAdlSAdpSDdr4UDimm2DCrbCpv,
                            BoardIdAdlSAdpSDdr5UDimm1DCrb, BoardIdAdlSAdpSDdr5UDimm1DCrbPpv, BoardIdAdlSAdpSDdr5SODimmCrb,
                            BoardIdAdlSAdpSDdr4SODimmCrb}) {
            Return(0x0F)
          }
        }
      }
      Return (0x00)
    }

    //
    // Current Resources Settings
    //
    Method(_CRS, 0x0, NotSerialized)
    {
      Name(RBUF, ResourceTemplate()
      {
        I2CSerialBus(0x0,                   // 7-bit Peripheral Address
                    ControllerInitiated,    // Peripheral or Controller?
                    400000,                 // Connection Speed in hz
                    AddressingMode7Bit,     // 7-bit or 10-bit addressing?
                    "\\_SB.PC00.I2C0",      // I2C Controller to which PAC is connected
                    0,                      // Resource Index
                    ResourceConsumer,       // Consumer or Producer?
                    I2CB)                   // Descriptor name
      })
      CreateWordField(RBUF,I2CB._ADR,BADR)
      Switch (ToInteger(PLID)) {
        Case (Package () {BoardIdAdlSAdpSDdr4UDimm2DCrb, BoardIdAdlSAdpSDdr4UDimm2DCrbEv, BoardIdAdlSAdpSDdr4UDimm2DCrbCpv,
                          BoardIdAdlSAdpSDdr5UDimm1DCrb, BoardIdAdlSAdpSDdr5UDimm1DCrbPpv, BoardIdAdlSAdpSDdr5SODimmCrb,
                          BoardIdAdlSAdpSDdr4SODimmCrb}) {
          Store (0x10, BADR)
        }
        Default {
          Store (0x0, BADR)
        }
      }
      Return(RBUF)
    }

    //
    // _DSM - Device Specific Method
    //
    // This method returns configuration information that tells the driver
    // which devices each line in the PAC193x is wired to monitor.
    // Names of the monitored devices must come from the Microsoft-defined
    // power rail taxonomy.
    //
    // The UUID for the Microchip PAC193x's DSM is {033771E0-1705-47B4-9535-D1BBE14D9A09}.
    // This is unique to the device, and must match what the Windows PAC193x driver expects.
    //
    // Returns:
    //         Either: A Buffer (for Functions 0 and 2, or an error)
    //                 A Package containing EMI resources (rail/resistor pairs)
    //
    // Input Arguments (per _DSM standard):
    //
    //     Arg0: UUID - Function Identifier
    //     Arg1: Integer - Revision
    //     Arg2: Integer - Function Index
    //     Arg3: Package - Parameters (not used in our implementation)
    //
    Function(_DSM, {BuffObj, PkgObj}, {BuffObj, IntObj, IntObj, PkgObj})
    {
      //
      //  Is our UUID being invoked?
      //
      if(LNotEqual(Arg0, ToUUID("033771E0-1705-47B4-9535-D1BBE14D9A09")))
      {
        return(Buffer() {0x0})  // incorrect UUID, return NULL for error
      }

      //
      // Switch based on the function number requested...
      //
      switch(ToInteger(Arg2))
      {
        //
        // Function zero returns a bit-mask of supported functions
        //
        case(0)
        {
          switch(ToInteger(Arg1)) // revision check
          {
            case(0)
            {
              return (Buffer() {0x3}) // Revision 0: function 1 is supported
            }
            case(1)
            {
              return (Buffer() {0x7f}) // Revision 1: functions 1->6 are supported
            }
          }
          break;
        }

        // Function 1 returns the channel "Rail Name" and the Resistor Value expressed in milli-Ohms.
        case(1)
        {
          // All Revisions supported (0 and 1 presently defined)
          Name(PKG1, Package()
          {
            Package()
            {
              // Rail Name, Resistor Value
              "",      0, // Channel 1
              "",      0, // Channel 2
              "",      0, // Channel 3
              "",      0  // Channel 4
            },
            Package()
            {
              // Rail Name, Resistor Value
              "VCCIN_AUX_PH1",          2,   // Channel 1
              "VCCIN_AUX_PH2",          2,   // Channel 2
              "V12S_CPU_VCCIN_AUX_VIN", 5,  // Channel 3
              "V3P3DX_PCH_SSD1",        10   // Channel 4
            }
          })
          Switch (ToInteger(PLID)) {
            Case (Package () {BoardIdAdlSAdpSDdr4UDimm2DCrb, BoardIdAdlSAdpSDdr4UDimm2DCrbEv, BoardIdAdlSAdpSDdr4UDimm2DCrbCpv,
                              BoardIdAdlSAdpSDdr5UDimm1DCrb, BoardIdAdlSAdpSDdr5UDimm1DCrbPpv, BoardIdAdlSAdpSDdr5SODimmCrb,
                              BoardIdAdlSAdpSDdr4SODimmCrb}) {
              return(DerefOf(Index(PKG1, 1)))
            }
            Default {
              return(DerefOf(Index(PKG1, 0)))
            }
          }
        }

        // Function 2 returns the Resistor values expressed in micro-Ohms.
        case(2)
        {
          If(LLess(Arg1,1)){ break; } // Revision 0 not supported.
          // Return enhanced precision resistor values.
          Name(PKG2, Package()
          {
            Package()
            {
              // Value
              0, // Channel 1
              0, // Channel 2
              0, // Channel 3
              0  // Channel 4
            },
            Package()
            {
              // Value
              2000,   // Channel 1
              2000,   // Channel 2
              5000,   // Channel 3
              10000   // Channel 4
            }
          })
          Switch (ToInteger(PLID)) {
            Case (Package () {BoardIdAdlSAdpSDdr4UDimm2DCrb, BoardIdAdlSAdpSDdr4UDimm2DCrbEv, BoardIdAdlSAdpSDdr4UDimm2DCrbCpv,
                              BoardIdAdlSAdpSDdr5UDimm1DCrb, BoardIdAdlSAdpSDdr5UDimm1DCrbPpv, BoardIdAdlSAdpSDdr5SODimmCrb,
                              BoardIdAdlSAdpSDdr4SODimmCrb}) {
              return(DerefOf(Index(PKG2, 1)))
            }
            Default {
              return(DerefOf(Index(PKG2, 0)))
            }
          }
        }

        // Function 3 returns the EMI enabled/disabled flags
        case(3)
        {
          If(LLess(Arg1,1)){ break; } // Revision 0 not supported.
          // Return ON/OFF flags.
          Name(BUF3, Package()
          {
            0xF // driver EMI ENABLE mask, CH1:CH2:CH3:CH4 - 1b=ON, 0b=OFF
            // EMI mask bit set but rail name is NULL -> EMI is not created (private channel)
            // channel name valid but mask bit not set -> EMI is not created (private channel)
          })
          return(BUF3)
        }

        // Function 4 returns the channel bipolar settings
        case(4)
        {
          If(LLess(Arg1,1)){ break; } // Revision 0 not supported.
          // Return the target NEG_PWR flags.
          Name(BUF4, Package()
          {
            0x00 // CH1:CH2:CH3:CH4-BIDI : CH1:CH2:CH3:CH4-BIDV
          })
          return(BUF4)
        }

        // Function 5 returns the SPS for ACTIVE and IDLE
        case(5)
        {
        If(LLess(Arg1,1)){ break; } // Revision 0 not supported.
          Name(BUF5, Package()
          {
            1024, // ACTIVE sps - accepted values = {1024, 256, 64, 8}
            8 // IDLE sps - accepted values = {1024, 256, 64, 8}
          })
          return(BUF5)
        }

        // Function 6 returns the watchdog interval
        case(6)
        {
          If(LLess(Arg1,1)){ break; } // Revision 0 not supported.
          Name(BUF6, Package()
          {
            900 // seconds (min=60, max=900)
          })
          return(BUF6)
        }

      } // switch(Arg2)
      //
      // Return an error (a buffer with a value of zero)
      // if we didn't return anything else above
      //
      return(Buffer() {0x0})
    } // _DSM
  } // PowerMeter Device PA01 Scope End

  //
  // PA02 Device: Defines how the PAC193x is connected to the I2C Controller.
  //
  Device(PA02)
  {
    Name(_HID, "MCHP1930")
    Name(_UID, 2)

    // Lowest power D-State supported by the device is D3
    Name (_S0W, 3)

    // Device Status: present, enabled, and functioning properly
    Method(_STA, 0x0, NotSerialized)
    {
      If(POME)
      {
        Switch (ToInteger(PLID)) {
          Case (Package () {BoardIdAdlSAdpSDdr4UDimm2DCrb, BoardIdAdlSAdpSDdr4UDimm2DCrbEv, BoardIdAdlSAdpSDdr4UDimm2DCrbCpv,
                            BoardIdAdlSAdpSDdr5UDimm1DCrb, BoardIdAdlSAdpSDdr5UDimm1DCrbPpv, BoardIdAdlSAdpSDdr5SODimmCrb,
                            BoardIdAdlSAdpSDdr4SODimmCrb}) {
            Return(0x0F)
          }
        }
      }
      Return (0x00)
    }

    //
    // Current Resources Settings
    //
    Method(_CRS, 0x0, NotSerialized)
    {
      Name(RBUF, ResourceTemplate()
      {
        I2CSerialBus(0x0,                   // 7-bit Peripheral Address
                    ControllerInitiated,    // Peripheral or Controller?
                    400000,                 // Connection Speed in hz
                    AddressingMode7Bit,     // 7-bit or 10-bit addressing?
                    "\\_SB.PC00.I2C0",      // I2C Controller to which PAC is connected
                    0,                      // Resource Index
                    ResourceConsumer,       // Consumer or Producer?
                    I2CB)                   // Descriptor name
      })
      CreateWordField(RBUF,I2CB._ADR,BADR)
      Switch (ToInteger(PLID)) {
        Case (Package () {BoardIdAdlSAdpSDdr4UDimm2DCrb, BoardIdAdlSAdpSDdr4UDimm2DCrbEv, BoardIdAdlSAdpSDdr4UDimm2DCrbCpv,
                          BoardIdAdlSAdpSDdr5UDimm1DCrb, BoardIdAdlSAdpSDdr5UDimm1DCrbPpv, BoardIdAdlSAdpSDdr5SODimmCrb,
                          BoardIdAdlSAdpSDdr4SODimmCrb}) {
          Store (0x12, BADR)
        }
        Default {
          Store (0x0, BADR)
        }
      }
      Return(RBUF)
    }

    //
    // _DSM - Device Specific Method
    //
    // This method returns configuration information that tells the driver
    // which devices each line in the PAC193x is wired to monitor.
    // Names of the monitored devices must come from the Microsoft-defined
    // power rail taxonomy.
    //
    // The UUID for the Microchip PAC193x's DSM is {033771E0-1705-47B4-9535-D1BBE14D9A09}.
    // This is unique to the device, and must match what the Windows PAC193x driver expects.
    //
    // Returns:
    //         Either: A Buffer (for Functions 0 and 2, or an error)
    //                 A Package containing EMI resources (rail/resistor pairs)
    //
    // Input Arguments (per _DSM standard):
    //
    //     Arg0: UUID - Function Identifier
    //     Arg1: Integer - Revision
    //     Arg2: Integer - Function Index
    //     Arg3: Package - Parameters (not used in our implementation)
    //
    Function(_DSM, {BuffObj, PkgObj}, {BuffObj, IntObj, IntObj, PkgObj})
    {
      //
      //  Is our UUID being invoked?
      //
      if(LNotEqual(Arg0, ToUUID("033771E0-1705-47B4-9535-D1BBE14D9A09")))
      {
        return(Buffer() {0x0})  // incorrect UUID, return NULL for error
      }

      //
      // Switch based on the function number requested...
      //
      switch(ToInteger(Arg2))
      {
        //
        // Function zero returns a bit-mask of supported functions
        //
        case(0)
        {
          switch(ToInteger(Arg1)) // revision check
          {
            case(0)
            {
              return (Buffer() {0x3}) // Revision 0: function 1 is supported
            }
            case(1)
            {
              return (Buffer() {0x7f}) // Revision 1: functions 1->6 are supported
            }
          }
          break;
        }

        // Function 1 returns the channel "Rail Name" and the Resistor Value expressed in milli-Ohms.
        case(1)
        {
          // All Revisions supported (0 and 1 presently defined)
          Name(PKG1, Package()
          {
            Package()
            {
              // Rail Name, Resistor Value
              "", 0, // Channel 1
              "", 0, // Channel 2
              "", 0, // Channel 3
              "", 0  // Channel 4
            },
            Package()
            {
              // Rail Name, Resistor Value
              "VCCCORE_VIN",        2,  // Channel 1
              "VCCGT_VIN",          2,  // Channel 2
              "VCCCORE_PH1",        1,  // Channel 3
              "VCCCORE_PH2",        1   // Channel 4
            }
          })
          Switch (ToInteger(PLID)) {
            Case (Package () {BoardIdAdlSAdpSDdr4UDimm2DCrb, BoardIdAdlSAdpSDdr4UDimm2DCrbEv, BoardIdAdlSAdpSDdr4UDimm2DCrbCpv,
                              BoardIdAdlSAdpSDdr5UDimm1DCrb, BoardIdAdlSAdpSDdr5UDimm1DCrbPpv, BoardIdAdlSAdpSDdr5SODimmCrb,
                              BoardIdAdlSAdpSDdr4SODimmCrb}) {
              return(DerefOf(Index(PKG1, 1)))
            }
            Default {
              return(DerefOf(Index(PKG1, 0)))
            }
          }
        }

        // Function 2 returns the Resistor values expressed in micro-Ohms.
        case(2)
        {
          If(LLess(Arg1,1)){ break; } // Revision 0 not supported.
          // Return enhanced precision resistor values.
          Name(PKG2, Package()
          {
            Package()
            {
              // Value
              0,       // Channel 1
              0,       // Channel 2
              0,       // Channel 3
              0        // Channel 4
            },
            Package()
            {
              // Value
              2000,     // Channel 1
              2000,     // Channel 2
              1000,     // Channel 3
              1000      // Channel 4
            }
          })
          Switch (ToInteger(PLID)) {
            Case (Package () {BoardIdAdlSAdpSDdr4UDimm2DCrb, BoardIdAdlSAdpSDdr4UDimm2DCrbEv, BoardIdAdlSAdpSDdr4UDimm2DCrbCpv,
                              BoardIdAdlSAdpSDdr5UDimm1DCrb, BoardIdAdlSAdpSDdr5UDimm1DCrbPpv, BoardIdAdlSAdpSDdr5SODimmCrb,
                              BoardIdAdlSAdpSDdr4SODimmCrb}) {
              return(DerefOf(Index(PKG2, 1)))
            }
            Default {
              return(DerefOf(Index(PKG2, 0)))
            }
          }
        }

        // Function 3 returns the EMI enabled/disabled flags
        case(3)
        {
          If(LLess(Arg1,1)){ break; } // Revision 0 not supported.
          // Return ON/OFF flags.
          Name(BUF3, Package()
          {
            0xF // driver EMI ENABLE mask, CH1:CH2:CH3:CH4 - 1b=ON, 0b=OFF
            // EMI mask bit set but rail name is NULL -> EMI is not created (private channel)
            // channel name valid but mask bit not set -> EMI is not created (private channel)
          })
          return(BUF3)
        }

        // Function 4 returns the channel bipolar settings
        case(4)
        {
          If(LLess(Arg1,1)){ break; } // Revision 0 not supported.
          // Return the target NEG_PWR flags.
          Name(BUF4, Package()
          {
            0x00 // CH1:CH2:CH3:CH4-BIDI : CH1:CH2:CH3:CH4-BIDV
          })
          return(BUF4)
        }

        // Function 5 returns the SPS for ACTIVE and IDLE
        case(5)
        {
        If(LLess(Arg1,1)){ break; } // Revision 0 not supported.
          Name(BUF5, Package()
          {
            1024, // ACTIVE sps - accepted values = {1024, 256, 64, 8}
            8 // IDLE sps - accepted values = {1024, 256, 64, 8}
          })
          return(BUF5)
        }

        // Function 6 returns the watchdog interval
        case(6)
        {
          If(LLess(Arg1,1)){ break; } // Revision 0 not supported.
          Name(BUF6, Package()
          {
            900 // seconds (min=60, max=900)
          })
          return(BUF6)
        }
      } // switch(Arg2)
      //
      // Return an error (a buffer with a value of zero)
      // if we didn't return anything else above
      //
      return(Buffer() {0x0})
    } // _DSM
  } // PowerMeter Device PA02 Scope End

  //
  // PA03 Device: Defines how the PAC193x is connected to the I2C Controller.
  //
  Device(PA03)
  {
    Name(_HID, "MCHP1930")
    Name(_UID, 3)

    // Lowest power D-State supported by the device is D3
    Name (_S0W, 3)

    // Device Status: present, enabled, and functioning properly
    Method(_STA, 0x0, NotSerialized)
    {
      If(POME)
      {
        Switch (ToInteger(PLID)) {
          Case (Package () {BoardIdAdlSAdpSDdr4UDimm2DCrb, BoardIdAdlSAdpSDdr4UDimm2DCrbEv, BoardIdAdlSAdpSDdr4UDimm2DCrbCpv,
                            BoardIdAdlSAdpSDdr5UDimm1DCrb, BoardIdAdlSAdpSDdr5UDimm1DCrbPpv, BoardIdAdlSAdpSDdr5SODimmCrb,
                            BoardIdAdlSAdpSDdr4SODimmCrb}) {
            Return(0x0F)
          }
        }
      }
      Return (0x00)
    }

    //
    // Current Resources Settings
    //
    Method(_CRS, 0x0, NotSerialized)
    {
      Name(RBUF, ResourceTemplate()
      {
        I2CSerialBus(0x0,                   // 7-bit Peripheral Address
                    ControllerInitiated,    // Peripheral or Controller?
                    400000,                 // Connection Speed in hz
                    AddressingMode7Bit,     // 7-bit or 10-bit addressing?
                    "\\_SB.PC00.I2C0",      // I2C Controller to which PAC is connected
                    0,                      // Resource Index
                    ResourceConsumer,       // Consumer or Producer?
                    I2CB)                   // Descriptor name
      })
      CreateWordField(RBUF,I2CB._ADR,BADR)
      Switch (ToInteger(PLID)) {
        Case (Package () {BoardIdAdlSAdpSDdr4UDimm2DCrb, BoardIdAdlSAdpSDdr4UDimm2DCrbEv, BoardIdAdlSAdpSDdr4UDimm2DCrbCpv,
                          BoardIdAdlSAdpSDdr5UDimm1DCrb, BoardIdAdlSAdpSDdr5UDimm1DCrbPpv, BoardIdAdlSAdpSDdr5SODimmCrb,
                          BoardIdAdlSAdpSDdr4SODimmCrb}) {
          Store (0x14, BADR)
        }
        Default {
          Store (0x0, BADR)
        }
      }
      Return(RBUF)
    }

    //
    // _DSM - Device Specific Method
    //
    // This method returns configuration information that tells the driver
    // which devices each line in the PAC193x is wired to monitor.
    // Names of the monitored devices must come from the Microsoft-defined
    // power rail taxonomy.
    //
    // The UUID for the Microchip PAC193x's DSM is {033771E0-1705-47B4-9535-D1BBE14D9A09}.
    // This is unique to the device, and must match what the Windows PAC193x driver expects.
    //
    // Returns:
    //         Either: A Buffer (for Functions 0 and 2, or an error)
    //                 A Package containing EMI resources (rail/resistor pairs)
    //
    // Input Arguments (per _DSM standard):
    //
    //     Arg0: UUID - Function Identifier
    //     Arg1: Integer - Revision
    //     Arg2: Integer - Function Index
    //     Arg3: Package - Parameters (not used in our implementation)
    //
    Function(_DSM, {BuffObj, PkgObj}, {BuffObj, IntObj, IntObj, PkgObj})
    {
      //
      //  Is our UUID being invoked?
      //
      if(LNotEqual(Arg0, ToUUID("033771E0-1705-47B4-9535-D1BBE14D9A09")))
      {
        return(Buffer() {0x0})  // incorrect UUID, return NULL for error
      }

      //
      // Switch based on the function number requested...
      //
      switch(ToInteger(Arg2))
      {
        //
        // Function zero returns a bit-mask of supported functions
        //
        case(0)
        {
          switch(ToInteger(Arg1)) // revision check
          {
            case(0)
            {
              return (Buffer() {0x3}) // Revision 0: function 1 is supported
            }
            case(1)
            {
              return (Buffer() {0x7f}) // Revision 1: functions 1->6 are supported
            }
          }
          break;
        }

        // Function 1 returns the channel "Rail Name" and the Resistor Value expressed in milli-Ohms.
        case(1)
        {
          // All Revisions supported (0 and 1 presently defined)
          Name(PKG1, Package()
          {
            Package()
            {
              // Rail Name, Resistor Value
              "", 0, // Channel 1
              "", 0, // Channel 2
              "", 0, // Channel 3
              "", 0  // Channel 4
            },
            Package()
            {
              // Rail Name, Resistor Value
              "VCCCORE_PH3",      1,  // Channel 1
              "VCCCORE_PH4",      1,  // Channel 2
              "VCCCORE_PH5",      1,  // Channel 3
              "VCCCORE_PH6",      1   // Channel 4
            }
          })
          Switch (ToInteger(PLID)) {
            Case (Package () {BoardIdAdlSAdpSDdr4UDimm2DCrb, BoardIdAdlSAdpSDdr4UDimm2DCrbEv, BoardIdAdlSAdpSDdr4UDimm2DCrbCpv,
                              BoardIdAdlSAdpSDdr5UDimm1DCrb, BoardIdAdlSAdpSDdr5UDimm1DCrbPpv, BoardIdAdlSAdpSDdr5SODimmCrb,
                              BoardIdAdlSAdpSDdr4SODimmCrb}) {
              return(DerefOf(Index(PKG1, 1)))
            }
            Default {
              return(DerefOf(Index(PKG1, 0)))
            }
          }
        }

        // Function 2 returns the Resistor values expressed in micro-Ohms.
        case(2)
        {
          If(LLess(Arg1,1)){ break; } // Revision 0 not supported.
          // Return enhanced precision resistor values.
          Name(PKG2, Package()
          {
            Package()
            {
              // Value
              0, // Channel 1
              0, // Channel 2
              0, // Channel 3
              0  // Channel 4
            },
            Package()
            {
              // Value
              1000,  // Channel 1
              1000,  // Channel 2
              1000,  // Channel 3
              1000   // Channel 4
            }
          })
          Switch (ToInteger(PLID)) {
            Case (Package () {BoardIdAdlSAdpSDdr4UDimm2DCrb, BoardIdAdlSAdpSDdr4UDimm2DCrbEv, BoardIdAdlSAdpSDdr4UDimm2DCrbCpv,
                              BoardIdAdlSAdpSDdr5UDimm1DCrb, BoardIdAdlSAdpSDdr5UDimm1DCrbPpv, BoardIdAdlSAdpSDdr5SODimmCrb,
                              BoardIdAdlSAdpSDdr4SODimmCrb}) {
              return(DerefOf(Index(PKG2, 1)))
            }
            Default {
              return(DerefOf(Index(PKG2, 0)))
            }
          }
        }

        // Function 3 returns the EMI enabled/disabled flags
        case(3)
        {
          If(LLess(Arg1,1)){ break; } // Revision 0 not supported.
          // Return ON/OFF flags.
          Name(BUF3, Package()
          {
            0xF // driver EMI ENABLE mask, CH1:CH2:CH3:CH4 - 1b=ON, 0b=OFF
            // EMI mask bit set but rail name is NULL -> EMI is not created (private channel)
            // channel name valid but mask bit not set -> EMI is not created (private channel)
          })
          return(BUF3)
        }

        // Function 4 returns the channel bipolar settings
        case(4)
        {
          If(LLess(Arg1,1)){ break; } // Revision 0 not supported.
          // Return the target NEG_PWR flags.
          Name(BUF4, Package()
          {
            0x00 // CH1:CH2:CH3:CH4-BIDI : CH1:CH2:CH3:CH4-BIDV
          })
          return(BUF4)
        }

        // Function 5 returns the SPS for ACTIVE and IDLE
        case(5)
        {
        If(LLess(Arg1,1)){ break; } // Revision 0 not supported.
          Name(BUF5, Package()
          {
            1024, // ACTIVE sps - accepted values = {1024, 256, 64, 8}
               8  // IDLE sps - accepted values = {1024, 256, 64, 8}
          })
          return(BUF5)
        }

        // Function 6 returns the watchdog interval
        case(6)
        {
          If(LLess(Arg1,1)){ break; } // Revision 0 not supported.
          Name(BUF6, Package()
          {
            900 // seconds (min=60, max=900)
          })
          return(BUF6)
        }

      } // switch(Arg2)
      //
      // Return an error (a buffer with a value of zero)
      // if we didn't return anything else above
      //
      return(Buffer() {0x0})
    } // _DSM
  } // PowerMeter Device PA03 Scope End

  //
  // PA04 Device: Defines how the PAC193x is connected to the I2C Controller.
  //
  Device(PA04)
  {
    Name(_HID, "MCHP1930")
    Name(_UID, 4)

    // Lowest power D-State supported by the device is D3
    Name (_S0W, 3)

    // Device Status: present, enabled, and functioning properly
    Method(_STA, 0x0, NotSerialized)
    {
      If(POME)
      {
        Switch (ToInteger(PLID)) {
          Case (Package () {BoardIdAdlSAdpSDdr4UDimm2DCrb, BoardIdAdlSAdpSDdr4UDimm2DCrbEv, BoardIdAdlSAdpSDdr4UDimm2DCrbCpv,
                            BoardIdAdlSAdpSDdr5UDimm1DCrb, BoardIdAdlSAdpSDdr5UDimm1DCrbPpv, BoardIdAdlSAdpSDdr5SODimmCrb,
                            BoardIdAdlSAdpSDdr4SODimmCrb}) {
            Return(0x0F)
          }
        }
      }
      Return (0x00)
    }

    //
    // Current Resources Settings
    //
    Method(_CRS, 0x0, NotSerialized)
    {
      Name(RBUF, ResourceTemplate()
      {
        I2CSerialBus(0x0,                   // 7-bit Peripheral Address
                    ControllerInitiated,    // Peripheral or Controller?
                    400000,                 // Connection Speed in hz
                    AddressingMode7Bit,     // 7-bit or 10-bit addressing?
                    "\\_SB.PC00.I2C0",      // I2C Controller to which PAC is connected
                    0,                      // Resource Index
                    ResourceConsumer,       // Consumer or Producer?
                    I2CB)                   // Descriptor name
      })
      CreateWordField(RBUF,I2CB._ADR,BADR)
      Switch (ToInteger(PLID)) {
        Case (Package () {BoardIdAdlSAdpSDdr4UDimm2DCrb, BoardIdAdlSAdpSDdr4UDimm2DCrbEv, BoardIdAdlSAdpSDdr4UDimm2DCrbCpv,
                          BoardIdAdlSAdpSDdr5UDimm1DCrb, BoardIdAdlSAdpSDdr5UDimm1DCrbPpv, BoardIdAdlSAdpSDdr5SODimmCrb,
                          BoardIdAdlSAdpSDdr4SODimmCrb}) {
          Store (0x16, BADR)
        }
        Default {
          Store (0x0, BADR)
        }
      }
      Return(RBUF)
    }

    //
    // _DSM - Device Specific Method
    //
    // This method returns configuration information that tells the driver
    // which devices each line in the PAC193x is wired to monitor.
    // Names of the monitored devices must come from the Microsoft-defined
    // power rail taxonomy.
    //
    // The UUID for the Microchip PAC193x's DSM is {033771E0-1705-47B4-9535-D1BBE14D9A09}.
    // This is unique to the device, and must match what the Windows PAC193x driver expects.
    //
    // Returns:
    //         Either: A Buffer (for Functions 0 and 2, or an error)
    //                 A Package containing EMI resources (rail/resistor pairs)
    //
    // Input Arguments (per _DSM standard):
    //
    //     Arg0: UUID - Function Identifier
    //     Arg1: Integer - Revision
    //     Arg2: Integer - Function Index
    //     Arg3: Package - Parameters (not used in our implementation)
    //
    Function(_DSM, {BuffObj, PkgObj}, {BuffObj, IntObj, IntObj, PkgObj})
    {
      //
      //  Is our UUID being invoked?
      //
      if(LNotEqual(Arg0, ToUUID("033771E0-1705-47B4-9535-D1BBE14D9A09")))
      {
        return(Buffer() {0x0})  // incorrect UUID, return NULL for error
      }

      //
      // Switch based on the function number requested...
      //
      switch(ToInteger(Arg2))
      {
        //
        // Function zero returns a bit-mask of supported functions
        //
        case(0)
        {
          switch(ToInteger(Arg1)) // revision check
          {
            case(0)
            {
              return (Buffer() {0x3}) // Revision 0: function 1 is supported
            }
            case(1)
            {
              return (Buffer() {0x7f}) // Revision 1: functions 1->6 are supported
            }
          }
          break;
        }

        // Function 1 returns the channel "Rail Name" and the Resistor Value expressed in milli-Ohms.
        case(1)
        {
          // All Revisions supported (0 and 1 presently defined)
          Name(PKG1, Package()
          {
            // Rail Name, Resistor Value
            Package () {
              "",     0, // Channel 1
              "",     0, // Channel 2
              "",     0, // Channel 3
              "",     0  // Channel 4
            },
            Package () {
              "VCCCORE_PH7",       1, // Channel 1
              "VCCCORE_PH8",       1, // Channel 2
              "VCCGT",             1, // Channel 3
              "V0P82A_PCH",        2  // Channel 4
            }
          })
          Switch (ToInteger(PLID)) {
            Case (Package () {BoardIdAdlSAdpSDdr4UDimm2DCrb, BoardIdAdlSAdpSDdr4UDimm2DCrbEv, BoardIdAdlSAdpSDdr4UDimm2DCrbCpv,
                              BoardIdAdlSAdpSDdr5UDimm1DCrb, BoardIdAdlSAdpSDdr5UDimm1DCrbPpv, BoardIdAdlSAdpSDdr5SODimmCrb,
                              BoardIdAdlSAdpSDdr4SODimmCrb}) {
              return(DerefOf(Index(PKG1, 1)))
            }
            Default {
              return(DerefOf(Index(PKG1, 0)))
            }
          }
        }

        // Function 2 returns the Resistor values expressed in micro-Ohms.
        case(2)
        {
          If(LLess(Arg1,1)){ break; } // Revision 0 not supported.
          // Return enhanced precision resistor values.
          Name(PKG2, Package()
          {
            // Value
            Package () {
              0, // Channel 1
              0, // Channel 2
              0, // Channel 3
              0  // Channel 4
            },
            Package () {
              1000, // Channel 1
              1000, // Channel 2
              1000, // Channel 3
              2000  // Channel 4
            }
          })
          Switch (ToInteger(PLID)) {
            Case (Package () {BoardIdAdlSAdpSDdr4UDimm2DCrb, BoardIdAdlSAdpSDdr4UDimm2DCrbEv, BoardIdAdlSAdpSDdr4UDimm2DCrbCpv,
                              BoardIdAdlSAdpSDdr5UDimm1DCrb, BoardIdAdlSAdpSDdr5UDimm1DCrbPpv, BoardIdAdlSAdpSDdr5SODimmCrb,
                              BoardIdAdlSAdpSDdr4SODimmCrb}) {
              return(DerefOf(Index(PKG2, 1)))
            }
            Default {
              return(DerefOf(Index(PKG2, 0)))
            }
          }
        }

        // Function 3 returns the EMI enabled/disabled flags
        case(3)
        {
          If(LLess(Arg1,1)){ break; } // Revision 0 not supported.
          // Return ON/OFF flags.
          Name(BUF3, Package()
          {
            0xF // driver EMI ENABLE mask, CH1:CH2:CH3:CH4 - 1b=ON, 0b=OFF
            // EMI mask bit set but rail name is NULL -> EMI is not created (private channel)
            // channel name valid but mask bit not set -> EMI is not created (private channel)
          })
          return(BUF3)
        }

        // Function 4 returns the channel bipolar settings
        case(4)
        {
          If(LLess(Arg1,1)){ break; } // Revision 0 not supported.
          // Return the target NEG_PWR flags.
          Name(BUF4, Package()
          {
            0x00 // CH1:CH2:CH3:CH4-BIDI : CH1:CH2:CH3:CH4-BIDV
          })
          return(BUF4)
        }

        // Function 5 returns the SPS for ACTIVE and IDLE
        case(5)
        {
        If(LLess(Arg1,1)){ break; } // Revision 0 not supported.
          Name(BUF5, Package()
          {
            1024, // ACTIVE sps - accepted values = {1024, 256, 64, 8}
               8  // IDLE sps - accepted values = {1024, 256, 64, 8}
          })
          return(BUF5)
        }

        // Function 6 returns the watchdog interval
        case(6)
        {
          If(LLess(Arg1,1)){ break; } // Revision 0 not supported.
          Name(BUF6, Package()
          {
            900 // seconds (min=60, max=900)
          })
          return(BUF6)
        }

      } // switch(Arg2)
      //
      // Return an error (a buffer with a value of zero)
      // if we didn't return anything else above
      //
      return(Buffer() {0x0})
    } // _DSM
  } // PowerMeter Device PA04 Scope End

  //
  // PA05 Device: Defines how the PAC193x is connected to the I2C Controller.
  //
  Device(PA05)
  {
    Name(_HID, "MCHP1930")
    Name(_UID, 5)

    // Lowest power D-State supported by the device is D3
    Name (_S0W, 3)

    // Device Status: present, enabled, and functioning properly
    Method(_STA, 0x0, NotSerialized)
    {
      If(POME)
      {
        Switch (ToInteger(PLID)) {
          Case (Package () {BoardIdAdlSAdpSDdr4UDimm2DCrb, BoardIdAdlSAdpSDdr4UDimm2DCrbEv, BoardIdAdlSAdpSDdr4UDimm2DCrbCpv,
                            BoardIdAdlSAdpSDdr5UDimm1DCrb, BoardIdAdlSAdpSDdr5UDimm1DCrbPpv, BoardIdAdlSAdpSDdr5SODimmCrb,
                            BoardIdAdlSAdpSDdr4SODimmCrb}) {
            Return(0x0F)
          }
        }
      }
      Return (0x00)
    }

    //
    // Current Resources Settings
    //
    Method(_CRS, 0x0, NotSerialized)
    {
      Name(RBUF, ResourceTemplate()
      {
        I2CSerialBus(0x0,                   // 7-bit Peripheral Address
                    ControllerInitiated,    // Peripheral or Controller?
                    400000,                 // Connection Speed in hz
                    AddressingMode7Bit,     // 7-bit or 10-bit addressing?
                    "\\_SB.PC00.I2C0",      // I2C Controller to which PAC is connected
                    0,                      // Resource Index
                    ResourceConsumer,       // Consumer or Producer?
                    I2CB)                   // Descriptor name
      })
      CreateWordField(RBUF,I2CB._ADR,BADR)
      Switch (ToInteger(PLID)) {
        Case (Package () {BoardIdAdlSAdpSDdr4UDimm2DCrb, BoardIdAdlSAdpSDdr4UDimm2DCrbEv, BoardIdAdlSAdpSDdr4UDimm2DCrbCpv,
                          BoardIdAdlSAdpSDdr5UDimm1DCrb, BoardIdAdlSAdpSDdr5UDimm1DCrbPpv, BoardIdAdlSAdpSDdr5SODimmCrb,
                          BoardIdAdlSAdpSDdr4SODimmCrb}) {
          Store (0x18, BADR)
        }
        Default {
          Store (0x0, BADR)
        }
      }
      Return(RBUF)
    }

    //
    // _DSM - Device Specific Method
    //
    // This method returns configuration information that tells the driver
    // which devices each line in the PAC193x is wired to monitor.
    // Names of the monitored devices must come from the Microsoft-defined
    // power rail taxonomy.
    //
    // The UUID for the Microchip PAC193x's DSM is {033771E0-1705-47B4-9535-D1BBE14D9A09}.
    // This is unique to the device, and must match what the Windows PAC193x driver expects.
    //
    // Returns:
    //         Either: A Buffer (for Functions 0 and 2, or an error)
    //                 A Package containing EMI resources (rail/resistor pairs)
    //
    // Input Arguments (per _DSM standard):
    //
    //     Arg0: UUID - Function Identifier
    //     Arg1: Integer - Revision
    //     Arg2: Integer - Function Index
    //     Arg3: Package - Parameters (not used in our implementation)
    //
    Function(_DSM, {BuffObj, PkgObj}, {BuffObj, IntObj, IntObj, PkgObj})
    {
      //
      //  Is our UUID being invoked?
      //
      if(LNotEqual(Arg0, ToUUID("033771E0-1705-47B4-9535-D1BBE14D9A09")))
      {
        return(Buffer() {0x0})  // incorrect UUID, return NULL for error
      }

      //
      // Switch based on the function number requested...
      //
      switch(ToInteger(Arg2))
      {
        //
        // Function zero returns a bit-mask of supported functions
        //
        case(0)
        {
          switch(ToInteger(Arg1)) // revision check
          {
            case(0)
            {
              return (Buffer() {0x3}) // Revision 0: function 1 is supported
            }
            case(1)
            {
              return (Buffer() {0x7f}) // Revision 1: functions 1->6 are supported
            }
          }
          break;
        }

        // Function 1 returns the channel "Rail Name" and the Resistor Value expressed in milli-Ohms.
        case(1)
        {
          // All Revisions supported (0 and 1 presently defined)
          Name(PKG1, Package()
          {
            // Rail Name, Resistor Value
            Package () {
              "",     0, // Channel 1
              "",     0, // Channel 2
              "",     0, // Channel 3
              "",     0  // Channel 4
            },
            Package () {
              "V5P0DX_SSD",       5,  // Channel 1
              "V1P05A_CPU",       5,  // Channel 2
              "VCCPRIM_1P8",      22, // Channel 3
              "VCCPDSW_3P3",      22  // Channel 4
            }
          })
          Switch (ToInteger(PLID)) {
            Case (Package () {BoardIdAdlSAdpSDdr4UDimm2DCrb, BoardIdAdlSAdpSDdr4UDimm2DCrbEv, BoardIdAdlSAdpSDdr4UDimm2DCrbCpv,
                              BoardIdAdlSAdpSDdr5UDimm1DCrb, BoardIdAdlSAdpSDdr5UDimm1DCrbPpv, BoardIdAdlSAdpSDdr5SODimmCrb,
                              BoardIdAdlSAdpSDdr4SODimmCrb}) {
              return(DerefOf(Index(PKG1, 1)))
            }
            Default {
              return(DerefOf(Index(PKG1, 0)))
            }
          }
        }

        // Function 2 returns the Resistor values expressed in micro-Ohms.
        case(2)
        {
          If(LLess(Arg1,1)){ break; } // Revision 0 not supported.
          // Return enhanced precision resistor values.
          Name(PKG2, Package()
          {
            // Value
            Package () {
              0, // Channel 1
              0, // Channel 2
              0, // Channel 3
              0  // Channel 4
            },
            Package () {
              5000,  // Channel 1
              5000,  // Channel 2
              22000, // Channel 3
              22000  // Channel 4
            }
          })
          Switch (ToInteger(PLID)) {
            Case (Package () {BoardIdAdlSAdpSDdr4UDimm2DCrb, BoardIdAdlSAdpSDdr4UDimm2DCrbEv, BoardIdAdlSAdpSDdr4UDimm2DCrbCpv,
                              BoardIdAdlSAdpSDdr5UDimm1DCrb, BoardIdAdlSAdpSDdr5UDimm1DCrbPpv, BoardIdAdlSAdpSDdr5SODimmCrb,
                              BoardIdAdlSAdpSDdr4SODimmCrb}) {
              return(DerefOf(Index(PKG2, 1)))
            }
            Default {
              return(DerefOf(Index(PKG2, 0)))
            }
          }
        }

        // Function 3 returns the EMI enabled/disabled flags
        case(3)
        {
          If(LLess(Arg1,1)){ break; } // Revision 0 not supported.
          // Return ON/OFF flags.
          Name(BUF3, Package()
          {
            0xF // driver EMI ENABLE mask, CH1:CH2:CH3:CH4 - 1b=ON, 0b=OFF
            // EMI mask bit set but rail name is NULL -> EMI is not created (private channel)
            // channel name valid but mask bit not set -> EMI is not created (private channel)
          })
          return(BUF3)
        }

        // Function 4 returns the channel bipolar settings
        case(4)
        {
          If(LLess(Arg1,1)){ break; } // Revision 0 not supported.
          // Return the target NEG_PWR flags.
          Name(BUF4, Package()
          {
            0x00 // CH1:CH2:CH3:CH4-BIDI : CH1:CH2:CH3:CH4-BIDV
          })
          return(BUF4)
        }

        // Function 5 returns the SPS for ACTIVE and IDLE
        case(5)
        {
        If(LLess(Arg1,1)){ break; } // Revision 0 not supported.
          Name(BUF5, Package()
          {
            1024, // ACTIVE sps - accepted values = {1024, 256, 64, 8}
               8  // IDLE sps - accepted values = {1024, 256, 64, 8}
          })
          return(BUF5)
        }

        // Function 6 returns the watchdog interval
        case(6)
        {
          If(LLess(Arg1,1)){ break; } // Revision 0 not supported.
          Name(BUF6, Package()
          {
            900 // seconds (min=60, max=900)
          })
          return(BUF6)
        }

      } // switch(Arg2)
      //
      // Return an error (a buffer with a value of zero)
      // if we didn't return anything else above
      //
      return(Buffer() {0x0})
    } // _DSM
  } // PowerMeter Device PA05 Scope End

  //
  // PA06 Device: Defines how the PAC193x is connected to the I2C Controller.
  //
  Device(PA06)
  {
    Name(_HID, "MCHP1930")
    Name(_UID, 6)

    // Lowest power D-State supported by the device is D3
    Name (_S0W, 3)

    // Device Status: present, enabled, and functioning properly
    Method(_STA, 0x0, NotSerialized)
    {
      If(POME)
      {
        Switch (ToInteger(PLID)) {
          Case (Package () {BoardIdAdlSAdpSDdr4UDimm2DCrb, BoardIdAdlSAdpSDdr4UDimm2DCrbEv, BoardIdAdlSAdpSDdr4UDimm2DCrbCpv,
                            BoardIdAdlSAdpSDdr5UDimm1DCrb, BoardIdAdlSAdpSDdr5UDimm1DCrbPpv, BoardIdAdlSAdpSDdr5SODimmCrb,
                            BoardIdAdlSAdpSDdr4SODimmCrb}) {
            Return(0x0F)
          }
        }
      }
      Return (0x00)
    }

    //
    // Current Resources Settings
    //
    Method(_CRS, 0x0, NotSerialized)
    {
      Name(RBUF, ResourceTemplate()
      {
        I2CSerialBus(0x0,                   // 7-bit Peripheral Address
                    ControllerInitiated,    // Peripheral or Controller?
                    400000,                 // Connection Speed in hz
                    AddressingMode7Bit,     // 7-bit or 10-bit addressing?
                    "\\_SB.PC00.I2C0",      // I2C Controller to which PAC is connected
                    0,                      // Resource Index
                    ResourceConsumer,       // Consumer or Producer?
                    I2CB)                   // Descriptor name
      })
      CreateWordField(RBUF,I2CB._ADR,BADR)
      Switch (ToInteger(PLID)) {
        Case (Package () {BoardIdAdlSAdpSDdr4UDimm2DCrb, BoardIdAdlSAdpSDdr4UDimm2DCrbEv, BoardIdAdlSAdpSDdr4UDimm2DCrbCpv,
                            BoardIdAdlSAdpSDdr5UDimm1DCrb, BoardIdAdlSAdpSDdr5UDimm1DCrbPpv, BoardIdAdlSAdpSDdr5SODimmCrb,
                            BoardIdAdlSAdpSDdr4SODimmCrb}) {
          Store (0x1A, BADR)
        }
        Default {
          Store (0x0, BADR)
        }
      }
      Return(RBUF)
    }

    //
    // _DSM - Device Specific Method
    //
    // This method returns configuration information that tells the driver
    // which devices each line in the PAC193x is wired to monitor.
    // Names of the monitored devices must come from the Microsoft-defined
    // power rail taxonomy.
    //
    // The UUID for the Microchip PAC193x's DSM is {033771E0-1705-47B4-9535-D1BBE14D9A09}.
    // This is unique to the device, and must match what the Windows PAC193x driver expects.
    //
    // Returns:
    //         Either: A Buffer (for Functions 0 and 2, or an error)
    //                 A Package containing EMI resources (rail/resistor pairs)
    //
    // Input Arguments (per _DSM standard):
    //
    //     Arg0: UUID - Function Identifier
    //     Arg1: Integer - Revision
    //     Arg2: Integer - Function Index
    //     Arg3: Package - Parameters (not used in our implementation)
    //
    Function(_DSM, {BuffObj, PkgObj}, {BuffObj, IntObj, IntObj, PkgObj})
    {
      //
      //  Is our UUID being invoked?
      //
      if(LNotEqual(Arg0, ToUUID("033771E0-1705-47B4-9535-D1BBE14D9A09")))
      {
        return(Buffer() {0x0})  // incorrect UUID, return NULL for error
      }

      //
      // Switch based on the function number requested...
      //
      switch(ToInteger(Arg2))
      {
        //
        // Function zero returns a bit-mask of supported functions
        //
        case(0)
        {
          switch(ToInteger(Arg1)) // revision check
          {
            case(0)
            {
              return (Buffer() {0x3}) // Revision 0: function 1 is supported
            }
            case(1)
            {
              return (Buffer() {0x7f}) // Revision 1: functions 1->6 are supported
            }
          }
          break;
        }

        // Function 1 returns the channel "Rail Name" and the Resistor Value expressed in milli-Ohms.
        case(1)
        {
          // All Revisions supported (0 and 1 presently defined)
          Name(PKG1, Package()
          {
            // Rail Name, Resistor Value
            Package () {
              "",     0, // Channel 1
              "",     0, // Channel 2
              "",     0, // Channel 3
              "",     0  // Channel 4
            },
            Package () {
              "V5DSW",     10, // Channel 1
              "V5S",       2,  // Channel 2
              "V3P3S",     2,  // Channel 3
              "V12S",      2   // Channel 4
            }
          })
          Switch (ToInteger(PLID)) {
            Case (Package () {BoardIdAdlSAdpSDdr4UDimm2DCrb, BoardIdAdlSAdpSDdr4UDimm2DCrbEv, BoardIdAdlSAdpSDdr4UDimm2DCrbCpv,
                              BoardIdAdlSAdpSDdr5UDimm1DCrb, BoardIdAdlSAdpSDdr5UDimm1DCrbPpv, BoardIdAdlSAdpSDdr5SODimmCrb,
                              BoardIdAdlSAdpSDdr4SODimmCrb}) {
              return(DerefOf(Index(PKG1, 1)))
            }
            Default {
              return(DerefOf(Index(PKG1, 0)))
            }
          }
        }

        // Function 2 returns the Resistor values expressed in micro-Ohms.
        case(2)
        {
          If(LLess(Arg1,1)){ break; } // Revision 0 not supported.
          // Return enhanced precision resistor values.
          Name(PKG2, Package()
          {
            // Value
            Package () {
              0, // Channel 1
              0, // Channel 2
              0, // Channel 3
              0  // Channel 4
            },
            Package () {
              10000, // Channel 1
              2000,  // Channel 2
              2000,  // Channel 3
              2000   // Channel 4
            }
          })
          Switch (ToInteger(PLID)) {
            Case (Package () {BoardIdAdlSAdpSDdr4UDimm2DCrb, BoardIdAdlSAdpSDdr4UDimm2DCrbEv, BoardIdAdlSAdpSDdr4UDimm2DCrbCpv,
                              BoardIdAdlSAdpSDdr5UDimm1DCrb, BoardIdAdlSAdpSDdr5UDimm1DCrbPpv, BoardIdAdlSAdpSDdr5SODimmCrb,
                              BoardIdAdlSAdpSDdr4SODimmCrb}) {
              return(DerefOf(Index(PKG2, 1)))
            }
            Default {
              return(DerefOf(Index(PKG2, 0)))
            }
          }
        }

        // Function 3 returns the EMI enabled/disabled flags
        case(3)
        {
          If(LLess(Arg1,1)){ break; } // Revision 0 not supported.
          // Return ON/OFF flags.
          Name(BUF3, Package()
          {
            0xF // driver EMI ENABLE mask, CH1:CH2:CH3:CH4 - 1b=ON, 0b=OFF
            // EMI mask bit set but rail name is NULL -> EMI is not created (private channel)
            // channel name valid but mask bit not set -> EMI is not created (private channel)
          })
          return(BUF3)
        }

        // Function 4 returns the channel bipolar settings
        case(4)
        {
          If(LLess(Arg1,1)){ break; } // Revision 0 not supported.
          // Return the target NEG_PWR flags.
          Name(BUF4, Package()
          {
            0x00 // CH1:CH2:CH3:CH4-BIDI : CH1:CH2:CH3:CH4-BIDV
          })
          return(BUF4)
        }

        // Function 5 returns the SPS for ACTIVE and IDLE
        case(5)
        {
        If(LLess(Arg1,1)){ break; } // Revision 0 not supported.
          Name(BUF5, Package()
          {
            1024, // ACTIVE sps - accepted values = {1024, 256, 64, 8}
               8  // IDLE sps - accepted values = {1024, 256, 64, 8}
          })
          return(BUF5)
        }

        // Function 6 returns the watchdog interval
        case(6)
        {
          If(LLess(Arg1,1)){ break; } // Revision 0 not supported.
          Name(BUF6, Package()
          {
            900 // seconds (min=60, max=900)
          })
          return(BUF6)
        }

      } // switch(Arg2)
      //
      // Return an error (a buffer with a value of zero)
      // if we didn't return anything else above
      //
      return(Buffer() {0x0})
    } // _DSM
  } // PowerMeter Device PA06 Scope End

  //
  // PA07 Device: Defines how the PAC193x is connected to the I2C Controller.
  //
  Device(PA07)
  {
    Name(_HID, "MCHP1930")
    Name(_UID, 7)

    // Lowest power D-State supported by the device is D3
    Name (_S0W, 3)

    // Device Status: present, enabled, and functioning properly
    Method(_STA, 0x0, NotSerialized)
    {
      If(POME)
      {
        Switch (ToInteger(PLID)) {
          Case (Package () {BoardIdAdlSAdpSDdr4UDimm2DCrb, BoardIdAdlSAdpSDdr4UDimm2DCrbEv, BoardIdAdlSAdpSDdr4UDimm2DCrbCpv,
                            BoardIdAdlSAdpSDdr5UDimm1DCrb, BoardIdAdlSAdpSDdr5UDimm1DCrbPpv, BoardIdAdlSAdpSDdr5SODimmCrb,
                            BoardIdAdlSAdpSDdr4SODimmCrb}) {
            Return(0x0F)
          }
        }
      }
      Return (0x00)
    }

    //
    // Current Resources Settings
    //
    Method(_CRS, 0x0, NotSerialized)
    {
      Name(RBUF, ResourceTemplate()
      {
        I2CSerialBus(0x0,                   // 7-bit Peripheral Address
                    ControllerInitiated,    // Peripheral or Controller?
                    400000,                 // Connection Speed in hz
                    AddressingMode7Bit,     // 7-bit or 10-bit addressing?
                    "\\_SB.PC00.I2C0",      // I2C Controller to which PAC is connected
                    0,                      // Resource Index
                    ResourceConsumer,       // Consumer or Producer?
                    I2CB)                   // Descriptor name
      })
      CreateWordField(RBUF,I2CB._ADR,BADR)
      Switch (ToInteger(PLID)) {
        Case (Package () {BoardIdAdlSAdpSDdr4UDimm2DCrb, BoardIdAdlSAdpSDdr4UDimm2DCrbEv, BoardIdAdlSAdpSDdr4UDimm2DCrbCpv,
                          BoardIdAdlSAdpSDdr5UDimm1DCrb, BoardIdAdlSAdpSDdr5UDimm1DCrbPpv, BoardIdAdlSAdpSDdr5SODimmCrb,
                          BoardIdAdlSAdpSDdr4SODimmCrb}) {
          Store (0x1C, BADR)
        }
        Default {
          Store (0x0, BADR)
        }
      }
      Return(RBUF)
    }

    //
    // _DSM - Device Specific Method
    //
    // This method returns configuration information that tells the driver
    // which devices each line in the PAC193x is wired to monitor.
    // Names of the monitored devices must come from the Microsoft-defined
    // power rail taxonomy.
    //
    // The UUID for the Microchip PAC193x's DSM is {033771E0-1705-47B4-9535-D1BBE14D9A09}.
    // This is unique to the device, and must match what the Windows PAC193x driver expects.
    //
    // Returns:
    //         Either: A Buffer (for Functions 0 and 2, or an error)
    //                 A Package containing EMI resources (rail/resistor pairs)
    //
    // Input Arguments (per _DSM standard):
    //
    //     Arg0: UUID - Function Identifier
    //     Arg1: Integer - Revision
    //     Arg2: Integer - Function Index
    //     Arg3: Package - Parameters (not used in our implementation)
    //
    Function(_DSM, {BuffObj, PkgObj}, {BuffObj, IntObj, IntObj, PkgObj})
    {
      //
      //  Is our UUID being invoked?
      //
      if(LNotEqual(Arg0, ToUUID("033771E0-1705-47B4-9535-D1BBE14D9A09")))
      {
        return(Buffer() {0x0})  // incorrect UUID, return NULL for error
      }

      //
      // Switch based on the function number requested...
      //
      switch(ToInteger(Arg2))
      {
        //
        // Function zero returns a bit-mask of supported functions
        //
        case(0)
        {
          switch(ToInteger(Arg1)) // revision check
          {
            case(0)
            {
              return (Buffer() {0x3}) // Revision 0: function 1 is supported
            }
            case(1)
            {
              return (Buffer() {0x7f}) // Revision 1: functions 1->6 are supported
            }
          }
          break;
        }

        // Function 1 returns the channel "Rail Name" and the Resistor Value expressed in milli-Ohms.
        case(1)
        {
          // All Revisions supported (0 and 1 presently defined)
          Name(PKG1, Package()
          {
            // Rail Name, Resistor Value
            Package () {
              "",     0, // Channel 1
              "",     0, // Channel 2
              "",     0, // Channel 3
              "",     0  // Channel 4
            },
            Package () {
              "V3P3DX_CPU_SSD",     10, // Channel 1
              "",                   0,  // Channel 2
              "VCCDD2_CPU",         2,  // Channel 3
              "VPP",                10  // Channel 4
            },
            Package () {
              "V3P3DX_CPU_SSD",     10, // Channel 1
              "",                   0,  // Channel 2
              "VCCDD2_CPU",         5,  // Channel 3
              "V5DUAL_DDR5",        2   // Channel 4
            },
            Package () {
              "V3P3DX_CPU_SSD",     10, // Channel 1
              "",                   0,  // Channel 2
              "VCCDD2_CPU",         5,  // Channel 3
              "VDDQ",               2   // Channel 4
            }
          })
          Switch (ToInteger(PLID)) {
            Case (Package () {BoardIdAdlSAdpSDdr4UDimm2DCrb, BoardIdAdlSAdpSDdr4UDimm2DCrbEv, BoardIdAdlSAdpSDdr4UDimm2DCrbCpv}) {
              return(DerefOf(Index(PKG1, 1)))
            }
            Case (Package () {BoardIdAdlSAdpSDdr5UDimm1DCrb, BoardIdAdlSAdpSDdr5UDimm1DCrbPpv, BoardIdAdlSAdpSDdr5SODimmCrb}) {
              return(DerefOf(Index(PKG1, 2)))
            }
            Case (Package () {BoardIdAdlSAdpSDdr4SODimmCrb}) {
              return(DerefOf(Index(PKG1, 3)))
            }
            Default {
              return(DerefOf(Index(PKG1, 0)))
            }
          }
        }

        // Function 2 returns the Resistor values expressed in micro-Ohms.
        case(2)
        {
          If(LLess(Arg1,1)){ break; } // Revision 0 not supported.
          // Return enhanced precision resistor values.
          Name(PKG2, Package()
          {
            // Value
            Package () {
              0, // Channel 1
              0, // Channel 2
              0, // Channel 3
              0  // Channel 4
            },
            Package () {
              10000, // Channel 1
              0,     // Channel 2
              2000,  // Channel 3
              10000  // Channel 4
            },
            Package () {
              10000, // Channel 1
              0,     // Channel 2
              5000,  // Channel 3
              2000   // Channel 4
            },
            Package () {
              10000, // Channel 1
              0,     // Channel 2
              5000,  // Channel 3
              2000   // Channel 4
            }
          })
          Switch (ToInteger(PLID)) {
            Case (Package () {BoardIdAdlSAdpSDdr4UDimm2DCrb, BoardIdAdlSAdpSDdr4UDimm2DCrbEv, BoardIdAdlSAdpSDdr4UDimm2DCrbCpv}) {
              return(DerefOf(Index(PKG2, 1)))
            }
            Case (Package () {BoardIdAdlSAdpSDdr5UDimm1DCrb, BoardIdAdlSAdpSDdr5UDimm1DCrbPpv, BoardIdAdlSAdpSDdr5SODimmCrb}) {
              return(DerefOf(Index(PKG2, 2)))
            }
            Case (Package () {BoardIdAdlSAdpSDdr4SODimmCrb}) {
              return(DerefOf(Index(PKG2, 3)))
            }
            Default {
              return(DerefOf(Index(PKG2, 0)))
            }
          }
        }

        // Function 3 returns the EMI enabled/disabled flags
        case(3)
        {
          If(LLess(Arg1,1)){ break; } // Revision 0 not supported.
          // Return ON/OFF flags.
          Name(BUF3, Package()
          {
            0xF // driver EMI ENABLE mask, CH1:CH2:CH3:CH4 - 1b=ON, 0b=OFF
            // EMI mask bit set but rail name is NULL -> EMI is not created (private channel)
            // channel name valid but mask bit not set -> EMI is not created (private channel)
          })
          return(BUF3)
        }

        // Function 4 returns the channel bipolar settings
        case(4)
        {
          If(LLess(Arg1,1)){ break; } // Revision 0 not supported.
          // Return the target NEG_PWR flags.
          Name(BUF4, Package()
          {
            0x00 // CH1:CH2:CH3:CH4-BIDI : CH1:CH2:CH3:CH4-BIDV
          })
          return(BUF4)
        }

        // Function 5 returns the SPS for ACTIVE and IDLE
        case(5)
        {
        If(LLess(Arg1,1)){ break; } // Revision 0 not supported.
          Name(BUF5, Package()
          {
            1024, // ACTIVE sps - accepted values = {1024, 256, 64, 8}
               8  // IDLE sps - accepted values = {1024, 256, 64, 8}
          })
          return(BUF5)
        }

        // Function 6 returns the watchdog interval
        case(6)
        {
          If(LLess(Arg1,1)){ break; } // Revision 0 not supported.
          Name(BUF6, Package()
          {
            900 // seconds (min=60, max=900)
          })
          return(BUF6)
        }

      } // switch(Arg2)
      //
      // Return an error (a buffer with a value of zero)
      // if we didn't return anything else above
      //
      return(Buffer() {0x0})
    } // _DSM
  } // PowerMeter Device PA07 Scope End

  //
  // PM01 Device: Defines how the PAC195x is connected to the I2C Controller.
  //
  Device(PM01)
  {
    Name(_HID, "MCHP1940")
    Name(_UID, 1)

    // Lowest power D-State supported by the device is D3
    Name (_S0W, 3)

    // Device Status: present, enabled, and functioning properly
    Method(_STA, 0x0, NotSerialized)
    {
      If(POME)
      {
        Switch (ToInteger(PLID)) {
          Case (Package () {BoardIdAdlSAdpSSbgaDdr4SODimmCrb}) {
            Return(0x0F)
          }
        }
      }
      Return(0x00)
    }
    // Current Resources Settings
    Method(_CRS, 0x0, NotSerialized)
    {
      Name(RBUF, ResourceTemplate()
      {
        I2CSerialBus(0x0,                   // 7-bit Peripheral Address
                     ControllerInitiated,    // Controller or peripheral?
                     400000,                 // Connection Speed in Hz
                     AddressingMode7Bit,     // 7-bit or 10-bit addressing?
                     "\\_SB.PC00.I2C0",      // I2C Controller to which PAC is connected
                     0,                      // Resource Index
                     ResourceConsumer,       // Consumer or Producer?
                     I2CB)                   // Descriptor name
      })
      CreateWordField(RBUF,I2CB._ADR,BADR)
      Switch (ToInteger(PLID)) {
        Case (Package () {BoardIdAdlSAdpSSbgaDdr4SODimmCrb}) {
          Store (0x10, BADR)
        }
        Default {
          Store (0x0, BADR)
        }
      }
      Return(RBUF)
    }
    // _DSM - Device Specific Method
    //
    // This method returns configuration information that PAC194x/5x driver
    // uses for the device initial configuration.
    //
    // The DSM UUID for the Microchip PAC194x/5x: {721F1534-5D27-4B60-9DF4-41A3C4B7DA3A}.
    // This must match what the Windows PAC194x/5x driver expects.
    //
    // Returns:
    //         Either: A Buffer (for supported Functions, or an error)
    //                 A Package containing PAC194x/5x resources (configuration values)
    //
    // Input Arguments (per _DSM standard):
    //     Arg0: UUID - Function Identifier
    //     Arg1: Integer - Revision
    //     Arg2: Integer - Function Index
    //     Arg3: Package - Parameters (not used in our implementation)
    //
    Function(_DSM, {BuffObj, PkgObj}, {BuffObj, IntObj, IntObj, PkgObj})
    {
        //  Is PAC194x/5x DSM UUID being invoked?
        if(LNotEqual(Arg0, ToUUID("721F1534-5D27-4B60-9DF4-41A3C4B7DA3A")))
        {
            return(Buffer() {0x0})    // incorrect UUID, return NULL for error
        }
        // Function number check
        switch(ToInteger(Arg2))
        {
            // Function 0 - returns a bit-mask of supported functions
            case(0)
            {
                switch(ToInteger(Arg1))    // revision check
                {
                    // Revision 0: function 1->7 are supported
                    case(0) {return (Buffer() {0xFF})}
                }
                break;
            }
            // Function 1 - returns the names of the monitored power rails.
            // The names should be compliant with Windows EMI power rail taxonomy.
            // If the rail name is NULL ("") -> no EMI created for the channel ("private channel").
            case(1)
            {
              Name(PKG1, Package()
              {
                Package()
                {
                  // Rail Names set
                  "",        // Channel 1 name
                  "",        // Channel 2 name
                  "",        // Channel 3 name
                  ""         // Channel 4 name
                },
                Package()
                {
                  // Rail Names set
                  "CPU_VCCINAUX1",        // Channel 1 name
                  "CPU_VCCINAUX2",        // Channel 2 name
                  "CPU_VCCCORE1" ,        // Channel 3 name
                  "CPU_VCCCORE2"          // Channel 4 name
                }
              })
              Switch (ToInteger(PLID)) {
                Case (Package () {BoardIdAdlSAdpSSbgaDdr4SODimmCrb}) {
                  return(DerefOf(Index(PKG1, 1)))
                }
                Default {
                  return(DerefOf(Index(PKG1, 0)))
                }
              }
            }

            // Function 2 - returns the sense resistor values, in micro-Ohms.
            case(2)
            {
              Name(PKG2, Package()
              {
                Package()
                {
                  // Sense resistors values (micro-Ohms)
                  0,        // Channel 1 resistor
                  0,        // Channel 2 resistor
                  0,        // Channel 3 resistor
                  0         // Channel 4 resistor
                },
                Package()
                {
                  // Sense resistors values (micro-Ohms)
                  2000,        // Channel 1 resistor
                  2000,        // Channel 2 resistor
                  1000,        // Channel 3 resistor
                  1000         // Channel 4 resistor
                }
              })
              Switch (ToInteger(PLID)) {
                Case (Package () {BoardIdAdlSAdpSSbgaDdr4SODimmCrb}) {
                  return(DerefOf(Index(PKG2, 1)))
                }
                Default {
                  return(DerefOf(Index(PKG2, 0)))
                }
              }
            }
            // Function 3 - returns the EMI enable/disable bit-mask
            case(3)
            {
                Name(BUF3, Package()
                {   // EMI bitmask - CH1:CH2:CH3:CH4
                    0xF           // CHn = 1: EMI enable for channel 'n'
                                  // CHn = 0: EMI disable for channel 'n' ("private channel")
                })
                return(BUF3)
            }
            // Function 4 - returns Vsense and Vbus polarity and full scale range configs
            case(4)
            {
                Name(BUF4, Package()
                {   // Vsense configs
                    0x0, 0x0, 0x0, 0x0,   // CFG_VS1, CFG_VS2, CFG_VS3, CFG_VS4
                                          // CFG_VSn: Vsense configuration for channel
                                          // {0x0 = unipolar; 0x1 = bipolar; 0x2 = half scale bipolar}
                    // Vbus configs
                    0x0, 0x0, 0x0, 0x0    // CFG_VB1, CFG_VB2, CFG_VB3, CFG_VB4
                                          // CFG_VBn: Vbus configuration for channel
                                          // {0x0 = unipolar; 0x1 = bipolar; 0x2 = half scale bipolar}
                })
                return(BUF4)
            }
            // Function 5 - returns the sample frequencies for ACTIVE and IDLE modes
            case(5)
            {
                Name(BUF5, Package()
                {
                    1024,   // ACTIVE sps - accepted values = {1024, 256, 64, 8}
                       8    // IDLE sps   - accepted values = {1024, 256, 64, 8}
                })
                return(BUF5)
            }
            // Function 6 - returns the REFRESH watchdog timer interval
            case(6)
            {
                Name(BUF6, Package()
                {
                    900     // seconds (min=60, max=60000)
                })
                return(BUF6)
            }
            // Function 7 - returns the Vbus multiplication factors for PAC194x/5x-2
            // PAC194x/5x-2 power meter VBUS+ pins may be connected to voltage dividers
            // to measure rail voltage higher than device Vbus FSR.
            case(7)
            {
                Name(BUF7, Package()
                {   // K = 1000 * (Vrail / Vbus). Must be integer, positive value.
                    1000,        // Channel 1 K-factor
                    1000,        // Channel 2 K-factor
                    1000,        // Channel 3 K-factor
                    1000         // Channel 4 K-factor
                })
                return(BUF7)
            }
        } // switch(Arg2)
        // Return an error (a buffer with a value of zero)
        // if we didn't return anything else above
        return(Buffer() {0x0})
    } // _DSM
  } // Device(PM01)

  //
  // PM02 Device: Defines how the PAC195x is connected to the I2C Controller.
  //
  Device(PM02)
  {
    Name(_HID, "MCHP1940")
    Name(_UID, 2)

    // Lowest power D-State supported by the device is D3
    Name (_S0W, 3)

    // Device Status: present, enabled, and functioning properly
    Method(_STA, 0x0, NotSerialized)
    {
      If(POME)
      {
        Switch (ToInteger(PLID)) {
          Case (Package () {BoardIdAdlSAdpSSbgaDdr4SODimmCrb}) {
            Return(0x0F)
          }
        }
      }
      Return(0x00)
    }
    // Current Resources Settings
    Method(_CRS, 0x0, NotSerialized)
    {
      Name(RBUF, ResourceTemplate()
      {
        I2CSerialBus(0x0,                   // 7-bit Peripheral Address
                     ControllerInitiated,    // Controller or peripheral?
                     400000,                 // Connection Speed in Hz
                     AddressingMode7Bit,     // 7-bit or 10-bit addressing?
                     "\\_SB.PC00.I2C0",      // I2C Controller to which PAC is connected
                     0,                      // Resource Index
                     ResourceConsumer,       // Consumer or Producer?
                     I2CB)                   // Descriptor name
      })
      CreateWordField(RBUF,I2CB._ADR,BADR)
      Switch (ToInteger(PLID)) {
        Case (Package () {BoardIdAdlSAdpSSbgaDdr4SODimmCrb}) {
          Store (0x14, BADR)
        }
        Default {
          Store (0x0, BADR)
        }
      }
      Return(RBUF)
    }
    // _DSM - Device Specific Method
    //
    // This method returns configuration information that PAC194x/5x driver
    // uses for the device initial configuration.
    //
    // The DSM UUID for the Microchip PAC194x/5x: {721F1534-5D27-4B60-9DF4-41A3C4B7DA3A}.
    // This must match what the Windows PAC194x/5x driver expects.
    //
    // Returns:
    //         Either: A Buffer (for supported Functions, or an error)
    //                 A Package containing PAC194x/5x resources (configuration values)
    //
    // Input Arguments (per _DSM standard):
    //     Arg0: UUID - Function Identifier
    //     Arg1: Integer - Revision
    //     Arg2: Integer - Function Index
    //     Arg3: Package - Parameters (not used in our implementation)
    //
    Function(_DSM, {BuffObj, PkgObj}, {BuffObj, IntObj, IntObj, PkgObj})
    {
        //  Is PAC194x/5x DSM UUID being invoked?
        if(LNotEqual(Arg0, ToUUID("721F1534-5D27-4B60-9DF4-41A3C4B7DA3A")))
        {
            return(Buffer() {0x0})    // incorrect UUID, return NULL for error
        }
        // Function number check
        switch(ToInteger(Arg2))
        {
            // Function 0 - returns a bit-mask of supported functions
            case(0)
            {
                switch(ToInteger(Arg1))    // revision check
                {
                    // Revision 0: function 1->7 are supported
                    case(0) {return (Buffer() {0xFF})}
                }
                break;
            }
            // Function 1 - returns the names of the monitored power rails.
            // The names should be compliant with Windows EMI power rail taxonomy.
            // If the rail name is NULL ("") -> no EMI created for the channel ("private channel").
            case(1)
            {
              Name(PKG1, Package()
              {
                Package()
                {
                  // Rail Names set
                  "",        // Channel 1 name
                  "",        // Channel 2 name
                  "",        // Channel 3 name
                  ""         // Channel 4 name
                },
                Package()
                {
                  // Rail Names set
                  "CPU_VCCCORE3",         // Channel 1 name
                  "CPU_VCCCORE4",         // Channel 2 name
                  "CPU_VCCCORE5" ,        // Channel 3 name
                  "CPU_VCCCORE6"          // Channel 4 name
                }
              })
              Switch (ToInteger(PLID)) {
                Case (Package () {BoardIdAdlSAdpSSbgaDdr4SODimmCrb}) {
                  return(DerefOf(Index(PKG1, 1)))
                }
                Default {
                  return(DerefOf(Index(PKG1, 0)))
                }
              }
            }

            // Function 2 - returns the sense resistor values, in micro-Ohms.
            case(2)
            {
              Name(PKG2, Package()
              {
                Package()
                {
                  // Sense resistors values (micro-Ohms)
                  0,        // Channel 1 resistor
                  0,        // Channel 2 resistor
                  0,        // Channel 3 resistor
                  0         // Channel 4 resistor
                },
                Package()
                {
                  // Sense resistors values (micro-Ohms)
                  1000,        // Channel 1 resistor
                  1000,        // Channel 2 resistor
                  1000,        // Channel 3 resistor
                  1000         // Channel 4 resistor
                }
              })
              Switch (ToInteger(PLID)) {
                Case (Package () {BoardIdAdlSAdpSSbgaDdr4SODimmCrb}) {
                  return(DerefOf(Index(PKG2, 1)))
                }
                Default {
                  return(DerefOf(Index(PKG2, 0)))
                }
              }
            }
            // Function 3 - returns the EMI enable/disable bit-mask
            case(3)
            {
                Name(BUF3, Package()
                {   // EMI bitmask - CH1:CH2:CH3:CH4
                    0xF           // CHn = 1: EMI enable for channel 'n'
                                  // CHn = 0: EMI disable for channel 'n' ("private channel")
                })
                return(BUF3)
            }
            // Function 4 - returns Vsense and Vbus polarity and full scale range configs
            case(4)
            {
                Name(BUF4, Package()
                {   // Vsense configs
                    0x0, 0x0, 0x0, 0x0,   // CFG_VS1, CFG_VS2, CFG_VS3, CFG_VS4
                                          // CFG_VSn: Vsense configuration for channel
                                          // {0x0 = unipolar; 0x1 = bipolar; 0x2 = half scale bipolar}
                    // Vbus configs
                    0x0, 0x0, 0x0, 0x0    // CFG_VB1, CFG_VB2, CFG_VB3, CFG_VB4
                                          // CFG_VBn: Vbus configuration for channel
                                          // {0x0 = unipolar; 0x1 = bipolar; 0x2 = half scale bipolar}
                })
                return(BUF4)
            }
            // Function 5 - returns the sample frequencies for ACTIVE and IDLE modes
            case(5)
            {
                Name(BUF5, Package()
                {
                    1024,   // ACTIVE sps - accepted values = {1024, 256, 64, 8}
                       8    // IDLE sps   - accepted values = {1024, 256, 64, 8}
                })
                return(BUF5)
            }
            // Function 6 - returns the REFRESH watchdog timer interval
            case(6)
            {
                Name(BUF6, Package()
                {
                    900     // seconds (min=60, max=60000)
                })
                return(BUF6)
            }
            // Function 7 - returns the Vbus multiplication factors for PAC194x/5x-2
            // PAC194x/5x-2 power meter VBUS+ pins may be connected to voltage dividers
            // to measure rail voltage higher than device Vbus FSR.
            case(7)
            {
                Name(BUF7, Package()
                {   // K = 1000 * (Vrail / Vbus). Must be integer, positive value.
                    1000,        // Channel 1 K-factor
                    1000,        // Channel 2 K-factor
                    1000,        // Channel 3 K-factor
                    1000         // Channel 4 K-factor
                })
                return(BUF7)
            }
        } // switch(Arg2)
        // Return an error (a buffer with a value of zero)
        // if we didn't return anything else above
        return(Buffer() {0x0})
    } // _DSM
  } // Device(PM02)

  //
  // PM03 Device: Defines how the PAC195x is connected to the I2C Controller.
  //
  Device(PM03)
  {
    Name(_HID, "MCHP1940")
    Name(_UID, 3)

    // Lowest power D-State supported by the device is D3
    Name (_S0W, 3)

    // Device Status: present, enabled, and functioning properly
    Method(_STA, 0x0, NotSerialized)
    {
      If(POME)
      {
        Switch (ToInteger(PLID)) {
          Case (Package () {BoardIdAdlSAdpSSbgaDdr4SODimmCrb}) {
            Return(0x0F)
          }
        }
      }
      Return(0x00)
    }
    // Current Resources Settings
    Method(_CRS, 0x0, NotSerialized)
    {
      Name(RBUF, ResourceTemplate()
      {
        I2CSerialBus(0x0,                   // 7-bit Peripheral Address
                     ControllerInitiated,    // Controller or peripheral?
                     400000,                 // Connection Speed in Hz
                     AddressingMode7Bit,     // 7-bit or 10-bit addressing?
                     "\\_SB.PC00.I2C0",      // I2C Controller to which PAC is connected
                     0,                      // Resource Index
                     ResourceConsumer,       // Consumer or Producer?
                     I2CB)                   // Descriptor name
      })
      CreateWordField(RBUF,I2CB._ADR,BADR)
      Switch (ToInteger(PLID)) {
        Case (Package () {BoardIdAdlSAdpSSbgaDdr4SODimmCrb}) {
          Store (0x16, BADR)
        }
        Default {
          Store (0x0, BADR)
        }
      }
      Return(RBUF)
    }
    // _DSM - Device Specific Method
    //
    // This method returns configuration information that PAC194x/5x driver
    // uses for the device initial configuration.
    //
    // The DSM UUID for the Microchip PAC194x/5x: {721F1534-5D27-4B60-9DF4-41A3C4B7DA3A}.
    // This must match what the Windows PAC194x/5x driver expects.
    //
    // Returns:
    //         Either: A Buffer (for supported Functions, or an error)
    //                 A Package containing PAC194x/5x resources (configuration values)
    //
    // Input Arguments (per _DSM standard):
    //     Arg0: UUID - Function Identifier
    //     Arg1: Integer - Revision
    //     Arg2: Integer - Function Index
    //     Arg3: Package - Parameters (not used in our implementation)
    //
    Function(_DSM, {BuffObj, PkgObj}, {BuffObj, IntObj, IntObj, PkgObj})
    {
        //  Is PAC194x/5x DSM UUID being invoked?
        if(LNotEqual(Arg0, ToUUID("721F1534-5D27-4B60-9DF4-41A3C4B7DA3A")))
        {
            return(Buffer() {0x0})    // incorrect UUID, return NULL for error
        }
        // Function number check
        switch(ToInteger(Arg2))
        {
            // Function 0 - returns a bit-mask of supported functions
            case(0)
            {
                switch(ToInteger(Arg1))    // revision check
                {
                    // Revision 0: function 1->7 are supported
                    case(0) {return (Buffer() {0xFF})}
                }
                break;
            }
            // Function 1 - returns the names of the monitored power rails.
            // The names should be compliant with Windows EMI power rail taxonomy.
            // If the rail name is NULL ("") -> no EMI created for the channel ("private channel").
            case(1)
            {
              Name(PKG1, Package()
              {
                Package()
                {
                  // Rail Names set
                  "",        // Channel 1 name
                  "",        // Channel 2 name
                  "",        // Channel 3 name
                  ""         // Channel 4 name
                },
                Package()
                {
                  // Rail Names set
                  "CPU_VCCCORE7",           // Channel 1 name
                  "STORAGE_PCH_SSD",        // Channel 2 name
                  "CPU_VCCGT" ,             // Channel 3 name
                  "PCH_V0P82A"              // Channel 4 name
                }
              })
              Switch (ToInteger(PLID)) {
                Case (Package () {BoardIdAdlSAdpSSbgaDdr4SODimmCrb}) {
                  return(DerefOf(Index(PKG1, 1)))
                }
                Default {
                  return(DerefOf(Index(PKG1, 0)))
                }
              }
            }

            // Function 2 - returns the sense resistor values, in micro-Ohms.
            case(2)
            {
              Name(PKG2, Package()
              {
                Package()
                {
                  // Sense resistors values (micro-Ohms)
                  0,        // Channel 1 resistor
                  0,        // Channel 2 resistor
                  0,        // Channel 3 resistor
                  0         // Channel 4 resistor
                },
                Package()
                {
                  // Sense resistors values (micro-Ohms)
                  1000,        // Channel 1 resistor
                  5000,        // Channel 2 resistor
                  1000,        // Channel 3 resistor
                  2000         // Channel 4 resistor
                }
              })
              Switch (ToInteger(PLID)) {
                Case (Package () {BoardIdAdlSAdpSSbgaDdr4SODimmCrb}) {
                  return(DerefOf(Index(PKG2, 1)))
                }
                Default {
                  return(DerefOf(Index(PKG2, 0)))
                }
              }
            }
            // Function 3 - returns the EMI enable/disable bit-mask
            case(3)
            {
                Name(BUF3, Package()
                {   // EMI bitmask - CH1:CH2:CH3:CH4
                    0xF           // CHn = 1: EMI enable for channel 'n'
                                  // CHn = 0: EMI disable for channel 'n' ("private channel")
                })
                return(BUF3)
            }
            // Function 4 - returns Vsense and Vbus polarity and full scale range configs
            case(4)
            {
                Name(BUF4, Package()
                {   // Vsense configs
                    0x0, 0x0, 0x0, 0x0,   // CFG_VS1, CFG_VS2, CFG_VS3, CFG_VS4
                                          // CFG_VSn: Vsense configuration for channel
                                          // {0x0 = unipolar; 0x1 = bipolar; 0x2 = half scale bipolar}
                    // Vbus configs
                    0x0, 0x0, 0x0, 0x0    // CFG_VB1, CFG_VB2, CFG_VB3, CFG_VB4
                                          // CFG_VBn: Vbus configuration for channel
                                          // {0x0 = unipolar; 0x1 = bipolar; 0x2 = half scale bipolar}
                })
                return(BUF4)
            }
            // Function 5 - returns the sample frequencies for ACTIVE and IDLE modes
            case(5)
            {
                Name(BUF5, Package()
                {
                    1024,   // ACTIVE sps - accepted values = {1024, 256, 64, 8}
                       8    // IDLE sps   - accepted values = {1024, 256, 64, 8}
                })
                return(BUF5)
            }
            // Function 6 - returns the REFRESH watchdog timer interval
            case(6)
            {
                Name(BUF6, Package()
                {
                    900     // seconds (min=60, max=60000)
                })
                return(BUF6)
            }
            // Function 7 - returns the Vbus multiplication factors for PAC194x/5x-2
            // PAC194x/5x-2 power meter VBUS+ pins may be connected to voltage dividers
            // to measure rail voltage higher than device Vbus FSR.
            case(7)
            {
                Name(BUF7, Package()
                {   // K = 1000 * (Vrail / Vbus). Must be integer, positive value.
                    1000,        // Channel 1 K-factor
                    1000,        // Channel 2 K-factor
                    1000,        // Channel 3 K-factor
                    1000         // Channel 4 K-factor
                })
                return(BUF7)
            }
        } // switch(Arg2)
        // Return an error (a buffer with a value of zero)
        // if we didn't return anything else above
        return(Buffer() {0x0})
    } // _DSM
  } // Device(PM03)

  //
  // PM04 Device: Defines how the PAC195x is connected to the I2C Controller.
  //
  Device(PM04)
  {
    Name(_HID, "MCHP1940")
    Name(_UID, 4)

    // Lowest power D-State supported by the device is D3
    Name (_S0W, 3)

    // Device Status: present, enabled, and functioning properly
    Method(_STA, 0x0, NotSerialized)
    {
      If(POME)
      {
        Switch (ToInteger(PLID)) {
          Case (Package () {BoardIdAdlSAdpSSbgaDdr4SODimmCrb}) {
            Return(0x0F)
          }
        }
      }
      Return(0x00)
    }
    // Current Resources Settings
    Method(_CRS, 0x0, NotSerialized)
    {
      Name(RBUF, ResourceTemplate()
      {
        I2CSerialBus(0x0,                   // 7-bit Peripheral Address
                     ControllerInitiated,    // Controller or peripheral?
                     400000,                 // Connection Speed in Hz
                     AddressingMode7Bit,     // 7-bit or 10-bit addressing?
                     "\\_SB.PC00.I2C0",      // I2C Controller to which PAC is connected
                     0,                      // Resource Index
                     ResourceConsumer,       // Consumer or Producer?
                     I2CB)                   // Descriptor name
      })
      CreateWordField(RBUF,I2CB._ADR,BADR)
      Switch (ToInteger(PLID)) {
        Case (Package () {BoardIdAdlSAdpSSbgaDdr4SODimmCrb}) {
          Store (0x18, BADR)
        }
        Default {
          Store (0x0, BADR)
        }
      }
      Return(RBUF)
    }
    // _DSM - Device Specific Method
    //
    // This method returns configuration information that PAC194x/5x driver
    // uses for the device initial configuration.
    //
    // The DSM UUID for the Microchip PAC194x/5x: {721F1534-5D27-4B60-9DF4-41A3C4B7DA3A}.
    // This must match what the Windows PAC194x/5x driver expects.
    //
    // Returns:
    //         Either: A Buffer (for supported Functions, or an error)
    //                 A Package containing PAC194x/5x resources (configuration values)
    //
    // Input Arguments (per _DSM standard):
    //     Arg0: UUID - Function Identifier
    //     Arg1: Integer - Revision
    //     Arg2: Integer - Function Index
    //     Arg3: Package - Parameters (not used in our implementation)
    //
    Function(_DSM, {BuffObj, PkgObj}, {BuffObj, IntObj, IntObj, PkgObj})
    {
        //  Is PAC194x/5x DSM UUID being invoked?
        if(LNotEqual(Arg0, ToUUID("721F1534-5D27-4B60-9DF4-41A3C4B7DA3A")))
        {
            return(Buffer() {0x0})    // incorrect UUID, return NULL for error
        }
        // Function number check
        switch(ToInteger(Arg2))
        {
            // Function 0 - returns a bit-mask of supported functions
            case(0)
            {
                switch(ToInteger(Arg1))    // revision check
                {
                    // Revision 0: function 1->7 are supported
                    case(0) {return (Buffer() {0xFF})}
                }
                break;
            }
            // Function 1 - returns the names of the monitored power rails.
            // The names should be compliant with Windows EMI power rail taxonomy.
            // If the rail name is NULL ("") -> no EMI created for the channel ("private channel").
            case(1)
            {
              Name(PKG1, Package()
              {
                Package()
                {
                  // Rail Names set
                  "",        // Channel 1 name
                  "",        // Channel 2 name
                  "",        // Channel 3 name
                  ""         // Channel 4 name
                },
                Package()
                {
                  // Rail Names set
                  "STORAGE_SATA",            // Channel 1 name
                  "CPU_V1P05A",              // Channel 2 name
                  "PCH_VCCPRIM_1P8" ,        // Channel 3 name
                  "PCH_VCCPDSW_3P3"          // Channel 4 name
                }
              })
              Switch (ToInteger(PLID)) {
                Case (Package () {BoardIdAdlSAdpSSbgaDdr4SODimmCrb}) {
                  return(DerefOf(Index(PKG1, 1)))
                }
                Default {
                  return(DerefOf(Index(PKG1, 0)))
                }
              }
            }

            // Function 2 - returns the sense resistor values, in micro-Ohms.
            case(2)
            {
              Name(PKG2, Package()
              {
                Package()
                {
                  // Sense resistors values (micro-Ohms)
                  0,        // Channel 1 resistor
                  0,        // Channel 2 resistor
                  0,        // Channel 3 resistor
                  0         // Channel 4 resistor
                },
                Package()
                {
                  // Sense resistors values (micro-Ohms)
                  5000,         // Channel 1 resistor
                  5000,         // Channel 2 resistor
                  22000,        // Channel 3 resistor
                  22000         // Channel 4 resistor
                }
              })
              Switch (ToInteger(PLID)) {
                Case (Package () {BoardIdAdlSAdpSSbgaDdr4SODimmCrb}) {
                  return(DerefOf(Index(PKG2, 1)))
                }
                Default {
                  return(DerefOf(Index(PKG2, 0)))
                }
              }
            }
            // Function 3 - returns the EMI enable/disable bit-mask
            case(3)
            {
                Name(BUF3, Package()
                {   // EMI bitmask - CH1:CH2:CH3:CH4
                    0xF           // CHn = 1: EMI enable for channel 'n'
                                  // CHn = 0: EMI disable for channel 'n' ("private channel")
                })
                return(BUF3)
            }
            // Function 4 - returns Vsense and Vbus polarity and full scale range configs
            case(4)
            {
                Name(BUF4, Package()
                {   // Vsense configs
                    0x0, 0x0, 0x0, 0x0,   // CFG_VS1, CFG_VS2, CFG_VS3, CFG_VS4
                                          // CFG_VSn: Vsense configuration for channel
                                          // {0x0 = unipolar; 0x1 = bipolar; 0x2 = half scale bipolar}
                    // Vbus configs
                    0x0, 0x0, 0x0, 0x0    // CFG_VB1, CFG_VB2, CFG_VB3, CFG_VB4
                                          // CFG_VBn: Vbus configuration for channel
                                          // {0x0 = unipolar; 0x1 = bipolar; 0x2 = half scale bipolar}
                })
                return(BUF4)
            }
            // Function 5 - returns the sample frequencies for ACTIVE and IDLE modes
            case(5)
            {
                Name(BUF5, Package()
                {
                    1024,   // ACTIVE sps - accepted values = {1024, 256, 64, 8}
                       8    // IDLE sps   - accepted values = {1024, 256, 64, 8}
                })
                return(BUF5)
            }
            // Function 6 - returns the REFRESH watchdog timer interval
            case(6)
            {
                Name(BUF6, Package()
                {
                    900     // seconds (min=60, max=60000)
                })
                return(BUF6)
            }
            // Function 7 - returns the Vbus multiplication factors for PAC194x/5x-2
            // PAC194x/5x-2 power meter VBUS+ pins may be connected to voltage dividers
            // to measure rail voltage higher than device Vbus FSR.
            case(7)
            {
                Name(BUF7, Package()
                {   // K = 1000 * (Vrail / Vbus). Must be integer, positive value.
                    1000,        // Channel 1 K-factor
                    1000,        // Channel 2 K-factor
                    1000,        // Channel 3 K-factor
                    1000         // Channel 4 K-factor
                })
                return(BUF7)
            }
        } // switch(Arg2)
        // Return an error (a buffer with a value of zero)
        // if we didn't return anything else above
        return(Buffer() {0x0})
    } // _DSM
  } // Device(PM04)

  //
  // PM05 Device: Defines how the PAC195x is connected to the I2C Controller.
  //
  Device(PM05)
  {
    Name(_HID, "MCHP1940")
    Name(_UID, 5)

    // Lowest power D-State supported by the device is D3
    Name (_S0W, 3)

    // Device Status: present, enabled, and functioning properly
    Method(_STA, 0x0, NotSerialized)
    {
      If(POME)
      {
        Switch (ToInteger(PLID)) {
          Case (Package () {BoardIdAdlSAdpSSbgaDdr4SODimmCrb}) {
            Return(0x0F)
          }
        }
      }
      Return(0x00)
    }
    // Current Resources Settings
    Method(_CRS, 0x0, NotSerialized)
    {
      Name(RBUF, ResourceTemplate()
      {
        I2CSerialBus(0x0,                   // 7-bit Peripheral Address
                     ControllerInitiated,    // Controller or peripheral?
                     400000,                 // Connection Speed in Hz
                     AddressingMode7Bit,     // 7-bit or 10-bit addressing?
                     "\\_SB.PC00.I2C0",      // I2C Controller to which PAC is connected
                     0,                      // Resource Index
                     ResourceConsumer,       // Consumer or Producer?
                     I2CB)                   // Descriptor name
      })
      CreateWordField(RBUF,I2CB._ADR,BADR)
      Switch (ToInteger(PLID)) {
        Case (Package () {BoardIdAdlSAdpSSbgaDdr4SODimmCrb}) {
          Store (0x1A, BADR)
        }
        Default {
          Store (0x0, BADR)
        }
      }
      Return(RBUF)
    }
    // _DSM - Device Specific Method
    //
    // This method returns configuration information that PAC194x/5x driver
    // uses for the device initial configuration.
    //
    // The DSM UUID for the Microchip PAC194x/5x: {721F1534-5D27-4B60-9DF4-41A3C4B7DA3A}.
    // This must match what the Windows PAC194x/5x driver expects.
    //
    // Returns:
    //         Either: A Buffer (for supported Functions, or an error)
    //                 A Package containing PAC194x/5x resources (configuration values)
    //
    // Input Arguments (per _DSM standard):
    //     Arg0: UUID - Function Identifier
    //     Arg1: Integer - Revision
    //     Arg2: Integer - Function Index
    //     Arg3: Package - Parameters (not used in our implementation)
    //
    Function(_DSM, {BuffObj, PkgObj}, {BuffObj, IntObj, IntObj, PkgObj})
    {
        //  Is PAC194x/5x DSM UUID being invoked?
        if(LNotEqual(Arg0, ToUUID("721F1534-5D27-4B60-9DF4-41A3C4B7DA3A")))
        {
            return(Buffer() {0x0})    // incorrect UUID, return NULL for error
        }
        // Function number check
        switch(ToInteger(Arg2))
        {
            // Function 0 - returns a bit-mask of supported functions
            case(0)
            {
                switch(ToInteger(Arg1))    // revision check
                {
                    // Revision 0: function 1->7 are supported
                    case(0) {return (Buffer() {0xFF})}
                }
                break;
            }
            // Function 1 - returns the names of the monitored power rails.
            // The names should be compliant with Windows EMI power rail taxonomy.
            // If the rail name is NULL ("") -> no EMI created for the channel ("private channel").
            case(1)
            {
              Name(PKG1, Package()
              {
                Package()
                {
                  // Rail Names set
                  "",        // Channel 1 name
                  "",        // Channel 2 name
                  "",        // Channel 3 name
                  ""         // Channel 4 name
                },
                Package()
                {
                  // Rail Names set
                  "SYSTEM_V5DSW",        // Channel 1 name
                  "SYSTEM_V5S",          // Channel 2 name
                  "SYSTEM_V3P3S" ,       // Channel 3 name
                  "SYSTEM_V12S"          // Channel 4 name
                }
              })
              Switch (ToInteger(PLID)) {
                Case (Package () {BoardIdAdlSAdpSSbgaDdr4SODimmCrb}) {
                  return(DerefOf(Index(PKG1, 1)))
                }
                Default {
                  return(DerefOf(Index(PKG1, 0)))
                }
              }
            }

            // Function 2 - returns the sense resistor values, in micro-Ohms.
            case(2)
            {
              Name(PKG2, Package()
              {
                Package()
                {
                  // Sense resistors values (micro-Ohms)
                  0,        // Channel 1 resistor
                  0,        // Channel 2 resistor
                  0,        // Channel 3 resistor
                  0         // Channel 4 resistor
                },
                Package()
                {
                  // Sense resistors values (micro-Ohms)
                  10000,        // Channel 1 resistor
                  2000,         // Channel 2 resistor
                  2000,         // Channel 3 resistor
                  2000          // Channel 4 resistor
                }
              })
              Switch (ToInteger(PLID)) {
                Case (Package () {BoardIdAdlSAdpSSbgaDdr4SODimmCrb}) {
                  return(DerefOf(Index(PKG2, 1)))
                }
                Default {
                  return(DerefOf(Index(PKG2, 0)))
                }
              }
            }
            // Function 3 - returns the EMI enable/disable bit-mask
            case(3)
            {
                Name(BUF3, Package()
                {   // EMI bitmask - CH1:CH2:CH3:CH4
                    0xF           // CHn = 1: EMI enable for channel 'n'
                                  // CHn = 0: EMI disable for channel 'n' ("private channel")
                })
                return(BUF3)
            }
            // Function 4 - returns Vsense and Vbus polarity and full scale range configs
            case(4)
            {
                Name(BUF4, Package()
                {   // Vsense configs
                    0x0, 0x0, 0x0, 0x0,   // CFG_VS1, CFG_VS2, CFG_VS3, CFG_VS4
                                          // CFG_VSn: Vsense configuration for channel
                                          // {0x0 = unipolar; 0x1 = bipolar; 0x2 = half scale bipolar}
                    // Vbus configs
                    0x0, 0x0, 0x0, 0x0    // CFG_VB1, CFG_VB2, CFG_VB3, CFG_VB4
                                          // CFG_VBn: Vbus configuration for channel
                                          // {0x0 = unipolar; 0x1 = bipolar; 0x2 = half scale bipolar}
                })
                return(BUF4)
            }
            // Function 5 - returns the sample frequencies for ACTIVE and IDLE modes
            case(5)
            {
                Name(BUF5, Package()
                {
                    1024,   // ACTIVE sps - accepted values = {1024, 256, 64, 8}
                       8    // IDLE sps   - accepted values = {1024, 256, 64, 8}
                })
                return(BUF5)
            }
            // Function 6 - returns the REFRESH watchdog timer interval
            case(6)
            {
                Name(BUF6, Package()
                {
                    900     // seconds (min=60, max=60000)
                })
                return(BUF6)
            }
            // Function 7 - returns the Vbus multiplication factors for PAC194x/5x-2
            // PAC194x/5x-2 power meter VBUS+ pins may be connected to voltage dividers
            // to measure rail voltage higher than device Vbus FSR.
            case(7)
            {
                Name(BUF7, Package()
                {   // K = 1000 * (Vrail / Vbus). Must be integer, positive value.
                    1000,        // Channel 1 K-factor
                    1000,        // Channel 2 K-factor
                    1000,        // Channel 3 K-factor
                    1000         // Channel 4 K-factor
                })
                return(BUF7)
            }
        } // switch(Arg2)
        // Return an error (a buffer with a value of zero)
        // if we didn't return anything else above
        return(Buffer() {0x0})
    } // _DSM
  } // Device(PM05)

  //
  // PM06 Device: Defines how the PAC195x is connected to the I2C Controller.
  //
  Device(PM06)
  {
    Name(_HID, "MCHP1940")
    Name(_UID, 6)

    // Lowest power D-State supported by the device is D3
    Name (_S0W, 3)

    // Device Status: present, enabled, and functioning properly
    Method(_STA, 0x0, NotSerialized)
    {
      If(POME)
      {
        Switch (ToInteger(PLID)) {
          Case (Package () {BoardIdAdlSAdpSSbgaDdr4SODimmCrb}) {
            Return(0x0F)
          }
        }
      }
      Return(0x00)
    }
    // Current Resources Settings
    Method(_CRS, 0x0, NotSerialized)
    {
      Name(RBUF, ResourceTemplate()
      {
        I2CSerialBus(0x0,                   // 7-bit Peripheral Address
                     ControllerInitiated,    // Controller or peripheral?
                     400000,                 // Connection Speed in Hz
                     AddressingMode7Bit,     // 7-bit or 10-bit addressing?
                     "\\_SB.PC00.I2C0",      // I2C Controller to which PAC is connected
                     0,                      // Resource Index
                     ResourceConsumer,       // Consumer or Producer?
                     I2CB)                   // Descriptor name
      })
      CreateWordField(RBUF,I2CB._ADR,BADR)
      Switch (ToInteger(PLID)) {
        Case (Package () {BoardIdAdlSAdpSSbgaDdr4SODimmCrb}) {
          Store (0x1C, BADR)
        }
        Default {
          Store (0x0, BADR)
        }
      }
      Return(RBUF)
    }
    // _DSM - Device Specific Method
    //
    // This method returns configuration information that PAC194x/5x driver
    // uses for the device initial configuration.
    //
    // The DSM UUID for the Microchip PAC194x/5x: {721F1534-5D27-4B60-9DF4-41A3C4B7DA3A}.
    // This must match what the Windows PAC194x/5x driver expects.
    //
    // Returns:
    //         Either: A Buffer (for supported Functions, or an error)
    //                 A Package containing PAC194x/5x resources (configuration values)
    //
    // Input Arguments (per _DSM standard):
    //     Arg0: UUID - Function Identifier
    //     Arg1: Integer - Revision
    //     Arg2: Integer - Function Index
    //     Arg3: Package - Parameters (not used in our implementation)
    //
    Function(_DSM, {BuffObj, PkgObj}, {BuffObj, IntObj, IntObj, PkgObj})
    {
        //  Is PAC194x/5x DSM UUID being invoked?
        if(LNotEqual(Arg0, ToUUID("721F1534-5D27-4B60-9DF4-41A3C4B7DA3A")))
        {
            return(Buffer() {0x0})    // incorrect UUID, return NULL for error
        }
        // Function number check
        switch(ToInteger(Arg2))
        {
            // Function 0 - returns a bit-mask of supported functions
            case(0)
            {
                switch(ToInteger(Arg1))    // revision check
                {
                    // Revision 0: function 1->7 are supported
                    case(0) {return (Buffer() {0xFF})}
                }
                break;
            }
            // Function 1 - returns the names of the monitored power rails.
            // The names should be compliant with Windows EMI power rail taxonomy.
            // If the rail name is NULL ("") -> no EMI created for the channel ("private channel").
            case(1)
            {
              Name(PKG1, Package()
              {
                Package()
                {
                  // Rail Names set
                  "",        // Channel 1 name
                  "",        // Channel 2 name
                  "",        // Channel 3 name
                  ""         // Channel 4 name
                },
                Package()
                {
                  // Rail Names set
                  "STORAGE_CPU_SSD",        // Channel 1 name
                  "DRAM_VPP",               // Channel 2 name
                  "CPU_VCCDD2" ,            // Channel 3 name
                  "DRAM_VDD2"               // Channel 4 name
                }
              })
              Switch (ToInteger(PLID)) {
                Case (Package () {BoardIdAdlSAdpSSbgaDdr4SODimmCrb}) {
                  return(DerefOf(Index(PKG1, 1)))
                }
                Default {
                  return(DerefOf(Index(PKG1, 0)))
                }
              }
            }

            // Function 2 - returns the sense resistor values, in micro-Ohms.
            case(2)
            {
              Name(PKG2, Package()
              {
                Package()
                {
                  // Sense resistors values (micro-Ohms)
                  0,        // Channel 1 resistor
                  0,        // Channel 2 resistor
                  0,        // Channel 3 resistor
                  0         // Channel 4 resistor
                },
                Package()
                {
                  // Sense resistors values (micro-Ohms)
                  5000,        // Channel 1 resistor
                  10000,       // Channel 2 resistor
                  2000,        // Channel 3 resistor
                  2000         // Channel 4 resistor
                }
              })
              Switch (ToInteger(PLID)) {
                Case (Package () {BoardIdAdlSAdpSSbgaDdr4SODimmCrb}) {
                  return(DerefOf(Index(PKG2, 1)))
                }
                Default {
                  return(DerefOf(Index(PKG2, 0)))
                }
              }
            }
            // Function 3 - returns the EMI enable/disable bit-mask
            case(3)
            {
                Name(BUF3, Package()
                {   // EMI bitmask - CH1:CH2:CH3:CH4
                    0xF           // CHn = 1: EMI enable for channel 'n'
                                  // CHn = 0: EMI disable for channel 'n' ("private channel")
                })
                return(BUF3)
            }
            // Function 4 - returns Vsense and Vbus polarity and full scale range configs
            case(4)
            {
                Name(BUF4, Package()
                {   // Vsense configs
                    0x0, 0x0, 0x0, 0x0,   // CFG_VS1, CFG_VS2, CFG_VS3, CFG_VS4
                                          // CFG_VSn: Vsense configuration for channel
                                          // {0x0 = unipolar; 0x1 = bipolar; 0x2 = half scale bipolar}
                    // Vbus configs
                    0x0, 0x0, 0x0, 0x0    // CFG_VB1, CFG_VB2, CFG_VB3, CFG_VB4
                                          // CFG_VBn: Vbus configuration for channel
                                          // {0x0 = unipolar; 0x1 = bipolar; 0x2 = half scale bipolar}
                })
                return(BUF4)
            }
            // Function 5 - returns the sample frequencies for ACTIVE and IDLE modes
            case(5)
            {
                Name(BUF5, Package()
                {
                    1024,   // ACTIVE sps - accepted values = {1024, 256, 64, 8}
                       8    // IDLE sps   - accepted values = {1024, 256, 64, 8}
                })
                return(BUF5)
            }
            // Function 6 - returns the REFRESH watchdog timer interval
            case(6)
            {
                Name(BUF6, Package()
                {
                    900     // seconds (min=60, max=60000)
                })
                return(BUF6)
            }
            // Function 7 - returns the Vbus multiplication factors for PAC194x/5x-2
            // PAC194x/5x-2 power meter VBUS+ pins may be connected to voltage dividers
            // to measure rail voltage higher than device Vbus FSR.
            case(7)
            {
                Name(BUF7, Package()
                {   // K = 1000 * (Vrail / Vbus). Must be integer, positive value.
                    1000,        // Channel 1 K-factor
                    1000,        // Channel 2 K-factor
                    1000,        // Channel 3 K-factor
                    1000         // Channel 4 K-factor
                })
                return(BUF7)
            }
        } // switch(Arg2)
        // Return an error (a buffer with a value of zero)
        // if we didn't return anything else above
        return(Buffer() {0x0})
    } // _DSM
  } // Device(PM06)

} // Scope I2C0


Scope(\_SB.PC00.I2C3)
{
  //
  // PA01 Device: Defines how the PAC193x is connected to the I2C Controller.
  //
  Device(PA01)
  {
    Name(_HID, "MCHP1930")
    Name(_UID, 1)

    // Lowest power D-State supported by the device is D3
    Name (_S0W, 3)

    // Device Status: present, enabled, and functioning properly
    Method(_STA, 0x0, NotSerialized)
    {
      If(POME)
      {
        Switch (ToInteger(PLID)) {
          Case (Package () {BoardIdAdlSAdpSSbgaDdr5SODimmAep}) {
            Return(0x0F)
          }
        }
      }
      Return (0x00)
    }

    //
    // Current Resources Settings
    //
    Method(_CRS, 0x0, NotSerialized)
    {
      Name(RBUF, ResourceTemplate()
      {
        I2CSerialBus(0x0,                   // 7-bit Peripheral Address
                    ControllerInitiated,    // Peripheral or Controller?
                    400000,                 // Connection Speed in hz
                    AddressingMode7Bit,     // 7-bit or 10-bit addressing?
                    "\\_SB.PC00.I2C3",      // I2C Controller to which PAC is connected
                    0,                      // Resource Index
                    ResourceConsumer,       // Consumer or Producer?
                    I2CB)                   // Descriptor name
      })
      CreateWordField(RBUF,I2CB._ADR,BADR)
      Switch (ToInteger(PLID)) {
        Case (Package () {BoardIdAdlSAdpSSbgaDdr5SODimmAep}) {
          Store (0x20, BADR)
        }
        Default {
          Store (0x0, BADR)
        }
      }
      Return(RBUF)
    }

    //
    // _DSM - Device Specific Method
    //
    // This method returns configuration information that tells the driver
    // which devices each line in the PAC193x is wired to monitor.
    // Names of the monitored devices must come from the Microsoft-defined
    // power rail taxonomy.
    //
    // The UUID for the Microchip PAC193x's DSM is {033771E0-1705-47B4-9535-D1BBE14D9A09}.
    // This is unique to the device, and must match what the Windows PAC193x driver expects.
    //
    // Returns:
    //         Either: A Buffer (for Functions 0 and 2, or an error)
    //                 A Package containing EMI resources (rail/resistor pairs)
    //
    // Input Arguments (per _DSM standard):
    //
    //     Arg0: UUID - Function Identifier
    //     Arg1: Integer - Revision
    //     Arg2: Integer - Function Index
    //     Arg3: Package - Parameters (not used in our implementation)
    //
    Function(_DSM, {BuffObj, PkgObj}, {BuffObj, IntObj, IntObj, PkgObj})
    {
      //
      //  Is our UUID being invoked?
      //
      if(LNotEqual(Arg0, ToUUID("033771E0-1705-47B4-9535-D1BBE14D9A09")))
      {
        return(Buffer() {0x0})  // incorrect UUID, return NULL for error
      }

      //
      // Switch based on the function number requested...
      //
      switch(ToInteger(Arg2))
      {
        //
        // Function zero returns a bit-mask of supported functions
        //
        case(0)
        {
          switch(ToInteger(Arg1)) // revision check
          {
            case(0)
            {
              return (Buffer() {0x3}) // Revision 0: function 1 is supported
            }
            case(1)
            {
              return (Buffer() {0x7f}) // Revision 1: functions 1->6 are supported
            }
          }
          break;
        }

        // Function 1 returns the channel "Rail Name" and the Resistor Value expressed in milli-Ohms.
        case(1)
        {
          // All Revisions supported (0 and 1 presently defined)
          Name(PKG1, Package()
          {
            Package()
            {
              // Rail Name, Resistor Value
              "",      0, // Channel 1
              "",      0, // Channel 2
              "",      0, // Channel 3
              "",      0  // Channel 4
            },
            Package()
            {
              // Rail Name, Resistor Value
              "VBATA_VCCGT_GPU",         5,  // Channel 1
              "VBATA_VCCSAMC_GPU",       5,  // Channel 2
              "VBATA_DGPU",              5,  // Channel 3
              "VBATA_VDD_GDDR6_GPU",     5   // Channel 4
            }
          })
          Switch (ToInteger(PLID)) {
            Case (Package () {BoardIdAdlSAdpSSbgaDdr5SODimmAep}) {
              return(DerefOf(Index(PKG1, 1)))
            }
            Default {
              return(DerefOf(Index(PKG1, 0)))
            }
          }
        }

        // Function 2 returns the Resistor values expressed in micro-Ohms.
        case(2)
        {
          If(LLess(Arg1,1)){ break; } // Revision 0 not supported.
          // Return enhanced precision resistor values.
          Name(PKG2, Package()
          {
            Package()
            {
              // Value
              0, // Channel 1
              0, // Channel 2
              0, // Channel 3
              0  // Channel 4
            },
            Package()
            {
              // Value
              5000,   // Channel 1
              5000,   // Channel 2
              5000,   // Channel 3
              5000    // Channel 4
            }
          })
          Switch (ToInteger(PLID)) {
            Case (Package () {BoardIdAdlSAdpSSbgaDdr5SODimmAep}) {
              return(DerefOf(Index(PKG2, 1)))
            }
            Default {
              return(DerefOf(Index(PKG2, 0)))
            }
          }
        }

        // Function 3 returns the EMI enabled/disabled flags
        case(3)
        {
          If(LLess(Arg1,1)){ break; } // Revision 0 not supported.
          // Return ON/OFF flags.
          Name(BUF3, Package()
          {
            0xF // driver EMI ENABLE mask, CH1:CH2:CH3:CH4 - 1b=ON, 0b=OFF
            // EMI mask bit set but rail name is NULL -> EMI is not created (private channel)
            // channel name valid but mask bit not set -> EMI is not created (private channel)
          })
          return(BUF3)
        }

        // Function 4 returns the channel bipolar settings
        case(4)
        {
          If(LLess(Arg1,1)){ break; } // Revision 0 not supported.
          // Return the target NEG_PWR flags.
          Name(BUF4, Package()
          {
            0x00 // CH1:CH2:CH3:CH4-BIDI : CH1:CH2:CH3:CH4-BIDV
          })
          return(BUF4)
        }

        // Function 5 returns the SPS for ACTIVE and IDLE
        case(5)
        {
        If(LLess(Arg1,1)){ break; } // Revision 0 not supported.
          Name(BUF5, Package()
          {
            1024, // ACTIVE sps - accepted values = {1024, 256, 64, 8}
            8 // IDLE sps - accepted values = {1024, 256, 64, 8}
          })
          return(BUF5)
        }

        // Function 6 returns the watchdog interval
        case(6)
        {
          If(LLess(Arg1,1)){ break; } // Revision 0 not supported.
          Name(BUF6, Package()
          {
            900 // seconds (min=60, max=900)
          })
          return(BUF6)
        }

      } // switch(Arg2)
      //
      // Return an error (a buffer with a value of zero)
      // if we didn't return anything else above
      //
      return(Buffer() {0x0})
    } // _DSM
  } // PowerMeter Device PA01 Scope End

  //
  // PA02 Device: Defines how the PAC193x is connected to the I2C Controller.
  //
  Device(PA02)
  {
    Name(_HID, "MCHP1930")
    Name(_UID, 2)

    // Lowest power D-State supported by the device is D3
    Name (_S0W, 3)

    // Device Status: present, enabled, and functioning properly
    Method(_STA, 0x0, NotSerialized)
    {
      If(POME)
      {
        Switch (ToInteger(PLID)) {
          Case (Package () {BoardIdAdlSAdpSSbgaDdr5SODimmAep}) {
            Return(0x0F)
          }
        }
      }
      Return (0x00)
    }

    //
    // Current Resources Settings
    //
    Method(_CRS, 0x0, NotSerialized)
    {
      Name(RBUF, ResourceTemplate()
      {
        I2CSerialBus(0x0,                   // 7-bit Peripheral Address
                    ControllerInitiated,    // Peripheral or Controller?
                    400000,                 // Connection Speed in hz
                    AddressingMode7Bit,     // 7-bit or 10-bit addressing?
                    "\\_SB.PC00.I2C3",      // I2C Controller to which PAC is connected
                    0,                      // Resource Index
                    ResourceConsumer,       // Consumer or Producer?
                    I2CB)                   // Descriptor name
      })
      CreateWordField(RBUF,I2CB._ADR,BADR)
      Switch (ToInteger(PLID)) {
        Case (Package () {BoardIdAdlSAdpSSbgaDdr5SODimmAep}) {
          Store (0x22, BADR)
        }
        Default {
          Store (0x0, BADR)
        }
      }
      Return(RBUF)
    }

    //
    // _DSM - Device Specific Method
    //
    // This method returns configuration information that tells the driver
    // which devices each line in the PAC193x is wired to monitor.
    // Names of the monitored devices must come from the Microsoft-defined
    // power rail taxonomy.
    //
    // The UUID for the Microchip PAC193x's DSM is {033771E0-1705-47B4-9535-D1BBE14D9A09}.
    // This is unique to the device, and must match what the Windows PAC193x driver expects.
    //
    // Returns:
    //         Either: A Buffer (for Functions 0 and 2, or an error)
    //                 A Package containing EMI resources (rail/resistor pairs)
    //
    // Input Arguments (per _DSM standard):
    //
    //     Arg0: UUID - Function Identifier
    //     Arg1: Integer - Revision
    //     Arg2: Integer - Function Index
    //     Arg3: Package - Parameters (not used in our implementation)
    //
    Function(_DSM, {BuffObj, PkgObj}, {BuffObj, IntObj, IntObj, PkgObj})
    {
      //
      //  Is our UUID being invoked?
      //
      if(LNotEqual(Arg0, ToUUID("033771E0-1705-47B4-9535-D1BBE14D9A09")))
      {
        return(Buffer() {0x0})  // incorrect UUID, return NULL for error
      }

      //
      // Switch based on the function number requested...
      //
      switch(ToInteger(Arg2))
      {
        //
        // Function zero returns a bit-mask of supported functions
        //
        case(0)
        {
          switch(ToInteger(Arg1)) // revision check
          {
            case(0)
            {
              return (Buffer() {0x3}) // Revision 0: function 1 is supported
            }
            case(1)
            {
              return (Buffer() {0x7f}) // Revision 1: functions 1->6 are supported
            }
          }
          break;
        }

        // Function 1 returns the channel "Rail Name" and the Resistor Value expressed in milli-Ohms.
        case(1)
        {
          // All Revisions supported (0 and 1 presently defined)
          Name(PKG1, Package()
          {
            Package()
            {
              // Rail Name, Resistor Value
              "", 0, // Channel 1
              "", 0, // Channel 2
              "", 0, // Channel 3
              "", 0  // Channel 4
            },
            Package()
            {
              // Rail Name, Resistor Value
              "VBATA_VCCIN",             5,  // Channel 1
              "VBATA_VCCIN_AUX",         5,  // Channel 2
              "VBATA_VCCGT",             5,  // Channel 3
              "V5A_DDR5",                2   // Channel 4
            }
          })
          Switch (ToInteger(PLID)) {
            Case (Package () {BoardIdAdlSAdpSSbgaDdr5SODimmAep}) {
              return(DerefOf(Index(PKG1, 1)))
            }
            Default {
              return(DerefOf(Index(PKG1, 0)))
            }
          }
        }

        // Function 2 returns the Resistor values expressed in micro-Ohms.
        case(2)
        {
          If(LLess(Arg1,1)){ break; } // Revision 0 not supported.
          // Return enhanced precision resistor values.
          Name(PKG2, Package()
          {
            Package()
            {
              // Value
              0,       // Channel 1
              0,       // Channel 2
              0,       // Channel 3
              0        // Channel 4
            },
            Package()
            {
              // Value
              5000,     // Channel 1
              5000,     // Channel 2
              5000,     // Channel 3
              2000      // Channel 4
            }
          })
          Switch (ToInteger(PLID)) {
            Case (Package () {BoardIdAdlSAdpSSbgaDdr5SODimmAep}) {
              return(DerefOf(Index(PKG2, 1)))
            }
            Default {
              return(DerefOf(Index(PKG2, 0)))
            }
          }
        }

        // Function 3 returns the EMI enabled/disabled flags
        case(3)
        {
          If(LLess(Arg1,1)){ break; } // Revision 0 not supported.
          // Return ON/OFF flags.
          Name(BUF3, Package()
          {
            0xF // driver EMI ENABLE mask, CH1:CH2:CH3:CH4 - 1b=ON, 0b=OFF
            // EMI mask bit set but rail name is NULL -> EMI is not created (private channel)
            // channel name valid but mask bit not set -> EMI is not created (private channel)
          })
          return(BUF3)
        }

        // Function 4 returns the channel bipolar settings
        case(4)
        {
          If(LLess(Arg1,1)){ break; } // Revision 0 not supported.
          // Return the target NEG_PWR flags.
          Name(BUF4, Package()
          {
            0x00 // CH1:CH2:CH3:CH4-BIDI : CH1:CH2:CH3:CH4-BIDV
          })
          return(BUF4)
        }

        // Function 5 returns the SPS for ACTIVE and IDLE
        case(5)
        {
        If(LLess(Arg1,1)){ break; } // Revision 0 not supported.
          Name(BUF5, Package()
          {
            1024, // ACTIVE sps - accepted values = {1024, 256, 64, 8}
            8 // IDLE sps - accepted values = {1024, 256, 64, 8}
          })
          return(BUF5)
        }

        // Function 6 returns the watchdog interval
        case(6)
        {
          If(LLess(Arg1,1)){ break; } // Revision 0 not supported.
          Name(BUF6, Package()
          {
            900 // seconds (min=60, max=900)
          })
          return(BUF6)
        }
      } // switch(Arg2)
      //
      // Return an error (a buffer with a value of zero)
      // if we didn't return anything else above
      //
      return(Buffer() {0x0})
    } // _DSM
  } // PowerMeter Device PA02 Scope End

  //
  // PA03 Device: Defines how the PAC193x is connected to the I2C Controller.
  //
  Device(PA03)
  {
    Name(_HID, "MCHP1930")
    Name(_UID, 3)

    // Lowest power D-State supported by the device is D3
    Name (_S0W, 3)

    // Device Status: present, enabled, and functioning properly
    Method(_STA, 0x0, NotSerialized)
    {
      If(POME)
      {
        Switch (ToInteger(PLID)) {
          Case (Package () {BoardIdAdlSAdpSSbgaDdr5SODimmAep}) {
            Return(0x0F)
          }
        }
      }
      Return (0x00)
    }

    //
    // Current Resources Settings
    //
    Method(_CRS, 0x0, NotSerialized)
    {
      Name(RBUF, ResourceTemplate()
      {
        I2CSerialBus(0x0,                   // 7-bit Peripheral Address
                    ControllerInitiated,    // Peripheral or Controller?
                    400000,                 // Connection Speed in hz
                    AddressingMode7Bit,     // 7-bit or 10-bit addressing?
                    "\\_SB.PC00.I2C3",      // I2C Controller to which PAC is connected
                    0,                      // Resource Index
                    ResourceConsumer,       // Consumer or Producer?
                    I2CB)                   // Descriptor name
      })
      CreateWordField(RBUF,I2CB._ADR,BADR)
      Switch (ToInteger(PLID)) {
        Case (Package () {BoardIdAdlSAdpSSbgaDdr5SODimmAep}) {
          Store (0x24, BADR)
        }
        Default {
          Store (0x0, BADR)
        }
      }
      Return(RBUF)
    }

    //
    // _DSM - Device Specific Method
    //
    // This method returns configuration information that tells the driver
    // which devices each line in the PAC193x is wired to monitor.
    // Names of the monitored devices must come from the Microsoft-defined
    // power rail taxonomy.
    //
    // The UUID for the Microchip PAC193x's DSM is {033771E0-1705-47B4-9535-D1BBE14D9A09}.
    // This is unique to the device, and must match what the Windows PAC193x driver expects.
    //
    // Returns:
    //         Either: A Buffer (for Functions 0 and 2, or an error)
    //                 A Package containing EMI resources (rail/resistor pairs)
    //
    // Input Arguments (per _DSM standard):
    //
    //     Arg0: UUID - Function Identifier
    //     Arg1: Integer - Revision
    //     Arg2: Integer - Function Index
    //     Arg3: Package - Parameters (not used in our implementation)
    //
    Function(_DSM, {BuffObj, PkgObj}, {BuffObj, IntObj, IntObj, PkgObj})
    {
      //
      //  Is our UUID being invoked?
      //
      if(LNotEqual(Arg0, ToUUID("033771E0-1705-47B4-9535-D1BBE14D9A09")))
      {
        return(Buffer() {0x0})  // incorrect UUID, return NULL for error
      }

      //
      // Switch based on the function number requested...
      //
      switch(ToInteger(Arg2))
      {
        //
        // Function zero returns a bit-mask of supported functions
        //
        case(0)
        {
          switch(ToInteger(Arg1)) // revision check
          {
            case(0)
            {
              return (Buffer() {0x3}) // Revision 0: function 1 is supported
            }
            case(1)
            {
              return (Buffer() {0x7f}) // Revision 1: functions 1->6 are supported
            }
          }
          break;
        }

        // Function 1 returns the channel "Rail Name" and the Resistor Value expressed in milli-Ohms.
        case(1)
        {
          // All Revisions supported (0 and 1 presently defined)
          Name(PKG1, Package()
          {
            Package()
            {
              // Rail Name, Resistor Value
              "", 0, // Channel 1
              "", 0, // Channel 2
              "", 0, // Channel 3
              "", 0  // Channel 4
            },
            Package()
            {
              // Rail Name, Resistor Value
              "V3P3DX_PCH_SSD1",        10,  // Channel 1
              "V3P3DX_CPU_SSD",         10,  // Channel 2
              "VBATTERY",                5,  // Channel 3
              "V3P3A_WWAN",             10   // Channel 4
            }
          })
          Switch (ToInteger(PLID)) {
            Case (Package () {BoardIdAdlSAdpSSbgaDdr5SODimmAep}) {
              return(DerefOf(Index(PKG1, 1)))
            }
            Default {
              return(DerefOf(Index(PKG1, 0)))
            }
          }
        }

        // Function 2 returns the Resistor values expressed in micro-Ohms.
        case(2)
        {
          If(LLess(Arg1,1)){ break; } // Revision 0 not supported.
          // Return enhanced precision resistor values.
          Name(PKG2, Package()
          {
            Package()
            {
              // Value
              0, // Channel 1
              0, // Channel 2
              0, // Channel 3
              0  // Channel 4
            },
            Package()
            {
              // Value
             10000,  // Channel 1
             10000,  // Channel 2
              5000,  // Channel 3
             10000   // Channel 4
            }
          })
          Switch (ToInteger(PLID)) {
            Case (Package () {BoardIdAdlSAdpSSbgaDdr5SODimmAep}) {
              return(DerefOf(Index(PKG2, 1)))
            }
            Default {
              return(DerefOf(Index(PKG2, 0)))
            }
          }
        }

        // Function 3 returns the EMI enabled/disabled flags
        case(3)
        {
          If(LLess(Arg1,1)){ break; } // Revision 0 not supported.
          // Return ON/OFF flags.
          Name(BUF3, Package()
          {
            0xF // driver EMI ENABLE mask, CH1:CH2:CH3:CH4 - 1b=ON, 0b=OFF
            // EMI mask bit set but rail name is NULL -> EMI is not created (private channel)
            // channel name valid but mask bit not set -> EMI is not created (private channel)
          })
          return(BUF3)
        }

        // Function 4 returns the channel bipolar settings
        case(4)
        {
          If(LLess(Arg1,1)){ break; } // Revision 0 not supported.
          // Return the target NEG_PWR flags.
          Name(BUF4, Package()
          {
            0x00 // CH1:CH2:CH3:CH4-BIDI : CH1:CH2:CH3:CH4-BIDV
          })
          return(BUF4)
        }

        // Function 5 returns the SPS for ACTIVE and IDLE
        case(5)
        {
        If(LLess(Arg1,1)){ break; } // Revision 0 not supported.
          Name(BUF5, Package()
          {
            1024, // ACTIVE sps - accepted values = {1024, 256, 64, 8}
               8  // IDLE sps - accepted values = {1024, 256, 64, 8}
          })
          return(BUF5)
        }

        // Function 6 returns the watchdog interval
        case(6)
        {
          If(LLess(Arg1,1)){ break; } // Revision 0 not supported.
          Name(BUF6, Package()
          {
            900 // seconds (min=60, max=900)
          })
          return(BUF6)
        }

      } // switch(Arg2)
      //
      // Return an error (a buffer with a value of zero)
      // if we didn't return anything else above
      //
      return(Buffer() {0x0})
    } // _DSM
  } // PowerMeter Device PA03 Scope End

  //
  // PA04 Device: Defines how the PAC193x is connected to the I2C Controller.
  //
  Device(PA04)
  {
    Name(_HID, "MCHP1930")
    Name(_UID, 4)

    // Lowest power D-State supported by the device is D3
    Name (_S0W, 3)

    // Device Status: present, enabled, and functioning properly
    Method(_STA, 0x0, NotSerialized)
    {
      If(POME)
      {
        Switch (ToInteger(PLID)) {
          Case (Package () {BoardIdAdlSAdpSSbgaDdr5SODimmAep}) {
            Return(0x0F)
          }
        }
      }
      Return (0x00)
    }

    //
    // Current Resources Settings
    //
    Method(_CRS, 0x0, NotSerialized)
    {
      Name(RBUF, ResourceTemplate()
      {
        I2CSerialBus(0x0,                   // 7-bit Peripheral Address
                    ControllerInitiated,    // Peripheral or Controller?
                    400000,                 // Connection Speed in hz
                    AddressingMode7Bit,     // 7-bit or 10-bit addressing?
                    "\\_SB.PC00.I2C3",      // I2C Controller to which PAC is connected
                    0,                      // Resource Index
                    ResourceConsumer,       // Consumer or Producer?
                    I2CB)                   // Descriptor name
      })
      CreateWordField(RBUF,I2CB._ADR,BADR)
      Switch (ToInteger(PLID)) {
        Case (Package () {BoardIdAdlSAdpSSbgaDdr5SODimmAep}) {
          Store (0x26, BADR)
        }
        Default {
          Store (0x0, BADR)
        }
      }
      Return(RBUF)
    }

    //
    // _DSM - Device Specific Method
    //
    // This method returns configuration information that tells the driver
    // which devices each line in the PAC193x is wired to monitor.
    // Names of the monitored devices must come from the Microsoft-defined
    // power rail taxonomy.
    //
    // The UUID for the Microchip PAC193x's DSM is {033771E0-1705-47B4-9535-D1BBE14D9A09}.
    // This is unique to the device, and must match what the Windows PAC193x driver expects.
    //
    // Returns:
    //         Either: A Buffer (for Functions 0 and 2, or an error)
    //                 A Package containing EMI resources (rail/resistor pairs)
    //
    // Input Arguments (per _DSM standard):
    //
    //     Arg0: UUID - Function Identifier
    //     Arg1: Integer - Revision
    //     Arg2: Integer - Function Index
    //     Arg3: Package - Parameters (not used in our implementation)
    //
    Function(_DSM, {BuffObj, PkgObj}, {BuffObj, IntObj, IntObj, PkgObj})
    {
      //
      //  Is our UUID being invoked?
      //
      if(LNotEqual(Arg0, ToUUID("033771E0-1705-47B4-9535-D1BBE14D9A09")))
      {
        return(Buffer() {0x0})  // incorrect UUID, return NULL for error
      }

      //
      // Switch based on the function number requested...
      //
      switch(ToInteger(Arg2))
      {
        //
        // Function zero returns a bit-mask of supported functions
        //
        case(0)
        {
          switch(ToInteger(Arg1)) // revision check
          {
            case(0)
            {
              return (Buffer() {0x3}) // Revision 0: function 1 is supported
            }
            case(1)
            {
              return (Buffer() {0x7f}) // Revision 1: functions 1->6 are supported
            }
          }
          break;
        }

        // Function 1 returns the channel "Rail Name" and the Resistor Value expressed in milli-Ohms.
        case(1)
        {
          // All Revisions supported (0 and 1 presently defined)
          Name(PKG1, Package()
          {
            // Rail Name, Resistor Value
            Package () {
              "",     0, // Channel 1
              "",     0, // Channel 2
              "",     0, // Channel 3
              "",     0  // Channel 4
            },
            Package () {
              "V3P3_SLOTE",             10, // Channel 1
              "V3P3_LAN",               10, // Channel 2
              "V3P3DX_EDP",             10, // Channel 3
              "VCC_EDP_BKLT",           10  // Channel 4
            }
          })
          Switch (ToInteger(PLID)) {
            Case (Package () {BoardIdAdlSAdpSSbgaDdr5SODimmAep}) {
              return(DerefOf(Index(PKG1, 1)))
            }
            Default {
              return(DerefOf(Index(PKG1, 0)))
            }
          }
        }

        // Function 2 returns the Resistor values expressed in micro-Ohms.
        case(2)
        {
          If(LLess(Arg1,1)){ break; } // Revision 0 not supported.
          // Return enhanced precision resistor values.
          Name(PKG2, Package()
          {
            // Value
            Package () {
              0, // Channel 1
              0, // Channel 2
              0, // Channel 3
              0  // Channel 4
            },
            Package () {
             10000, // Channel 1
             10000, // Channel 2
             10000, // Channel 3
             10000  // Channel 4
            }
          })
          Switch (ToInteger(PLID)) {
            Case (Package () {BoardIdAdlSAdpSSbgaDdr5SODimmAep}) {
              return(DerefOf(Index(PKG2, 1)))
            }
            Default {
              return(DerefOf(Index(PKG2, 0)))
            }
          }
        }

        // Function 3 returns the EMI enabled/disabled flags
        case(3)
        {
          If(LLess(Arg1,1)){ break; } // Revision 0 not supported.
          // Return ON/OFF flags.
          Name(BUF3, Package()
          {
            0xF // driver EMI ENABLE mask, CH1:CH2:CH3:CH4 - 1b=ON, 0b=OFF
            // EMI mask bit set but rail name is NULL -> EMI is not created (private channel)
            // channel name valid but mask bit not set -> EMI is not created (private channel)
          })
          return(BUF3)
        }

        // Function 4 returns the channel bipolar settings
        case(4)
        {
          If(LLess(Arg1,1)){ break; } // Revision 0 not supported.
          // Return the target NEG_PWR flags.
          Name(BUF4, Package()
          {
            0x00 // CH1:CH2:CH3:CH4-BIDI : CH1:CH2:CH3:CH4-BIDV
          })
          return(BUF4)
        }

        // Function 5 returns the SPS for ACTIVE and IDLE
        case(5)
        {
        If(LLess(Arg1,1)){ break; } // Revision 0 not supported.
          Name(BUF5, Package()
          {
            1024, // ACTIVE sps - accepted values = {1024, 256, 64, 8}
               8  // IDLE sps - accepted values = {1024, 256, 64, 8}
          })
          return(BUF5)
        }

        // Function 6 returns the watchdog interval
        case(6)
        {
          If(LLess(Arg1,1)){ break; } // Revision 0 not supported.
          Name(BUF6, Package()
          {
            900 // seconds (min=60, max=900)
          })
          return(BUF6)
        }

      } // switch(Arg2)
      //
      // Return an error (a buffer with a value of zero)
      // if we didn't return anything else above
      //
      return(Buffer() {0x0})
    } // _DSM
  } // PowerMeter Device PA04 Scope End
} // Scope I2C3


Scope(\_SB.PC00.I2C5)
{
  //
  // PA01 Device: Defines how the PAC193x is connected to the I2C Controller.
  //
  Device(PA01)
  {
    Name(_HID, "MCHP1930")
    Name(_UID, 1)

    // Lowest power D-State supported by the device is D3
    Name (_S0W, 3)

    // Device Status: present, enabled, and functioning properly
    Method(_STA, 0x0, NotSerialized)
    {
      If(POME)
      {
        Switch (ToInteger(PLID)) {
          Case (Package () {BoardIdAdlSAdpSSbgaDdr5SODimmCrb}) {
            Return(0x0F)
          }
        }
      }
      Return (0x00)
    }

    //
    // Current Resources Settings
    //
    Method(_CRS, 0x0, NotSerialized)
    {
      Name(RBUF, ResourceTemplate()
      {
        I2CSerialBus(0x0,                   // 7-bit Peripheral Address
                    ControllerInitiated,    // Peripheral or Controller?
                    400000,                 // Connection Speed in hz
                    AddressingMode7Bit,     // 7-bit or 10-bit addressing?
                    "\\_SB.PC00.I2C5",      // I2C Controller to which PAC is connected
                    0,                      // Resource Index
                    ResourceConsumer,       // Consumer or Producer?
                    I2CB)                   // Descriptor name
      })
      CreateWordField(RBUF,I2CB._ADR,BADR)
      Switch (ToInteger(PLID)) {
        Case (Package () {BoardIdAdlSAdpSSbgaDdr5SODimmCrb}) {
          Store (0x10, BADR)
        }
        Default {
          Store (0x0, BADR)
        }
      }
      Return(RBUF)
    }

    //
    // _DSM - Device Specific Method
    //
    // This method returns configuration information that tells the driver
    // which devices each line in the PAC193x is wired to monitor.
    // Names of the monitored devices must come from the Microsoft-defined
    // power rail taxonomy.
    //
    // The UUID for the Microchip PAC193x's DSM is {033771E0-1705-47B4-9535-D1BBE14D9A09}.
    // This is unique to the device, and must match what the Windows PAC193x driver expects.
    //
    // Returns:
    //         Either: A Buffer (for Functions 0 and 2, or an error)
    //                 A Package containing EMI resources (rail/resistor pairs)
    //
    // Input Arguments (per _DSM standard):
    //
    //     Arg0: UUID - Function Identifier
    //     Arg1: Integer - Revision
    //     Arg2: Integer - Function Index
    //     Arg3: Package - Parameters (not used in our implementation)
    //
    Function(_DSM, {BuffObj, PkgObj}, {BuffObj, IntObj, IntObj, PkgObj})
    {
      //
      //  Is our UUID being invoked?
      //
      if(LNotEqual(Arg0, ToUUID("033771E0-1705-47B4-9535-D1BBE14D9A09")))
      {
        return(Buffer() {0x0})  // incorrect UUID, return NULL for error
      }

      //
      // Switch based on the function number requested...
      //
      switch(ToInteger(Arg2))
      {
        //
        // Function zero returns a bit-mask of supported functions
        //
        case(0)
        {
          switch(ToInteger(Arg1)) // revision check
          {
            case(0)
            {
              return (Buffer() {0x3}) // Revision 0: function 1 is supported
            }
            case(1)
            {
              return (Buffer() {0x7f}) // Revision 1: functions 1->6 are supported
            }
          }
          break;
        }

        // Function 1 returns the channel "Rail Name" and the Resistor Value expressed in milli-Ohms.
        case(1)
        {
          // All Revisions supported (0 and 1 presently defined)
          Name(PKG1, Package()
          {
            Package()
            {
              // Rail Name, Resistor Value
              "",      0, // Channel 1
              "",      0, // Channel 2
              "",      0, // Channel 3
              "",      0  // Channel 4
            },
            Package()
            {
              // Rail Name, Resistor Value
              "AUDIO_V1P8",         10,  // Channel 1
              "AUDIO_V5",           10,  // Channel 2
              "AUDIO_V12S",         10,  // Channel 3
              "STORAGE_PCH_SSD",    10   // Channel 4
            }
          })
          Switch (ToInteger(PLID)) {
            Case (Package () {BoardIdAdlSAdpSSbgaDdr5SODimmCrb}) {
              return(DerefOf(Index(PKG1, 1)))
            }
            Default {
              return(DerefOf(Index(PKG1, 0)))
            }
          }
        }

        // Function 2 returns the Resistor values expressed in micro-Ohms.
        case(2)
        {
          If(LLess(Arg1,1)){ break; } // Revision 0 not supported.
          // Return enhanced precision resistor values.
          Name(PKG2, Package()
          {
            Package()
            {
              // Value
              0, // Channel 1
              0, // Channel 2
              0, // Channel 3
              0  // Channel 4
            },
            Package()
            {
              // Value
              10000,   // Channel 1
              10000,   // Channel 2
              10000,   // Channel 3
              10000    // Channel 4
            }
          })
          Switch (ToInteger(PLID)) {
            Case (Package () {BoardIdAdlSAdpSSbgaDdr5SODimmCrb}) {
              return(DerefOf(Index(PKG2, 1)))
            }
            Default {
              return(DerefOf(Index(PKG2, 0)))
            }
          }
        }

        // Function 3 returns the EMI enabled/disabled flags
        case(3)
        {
          If(LLess(Arg1,1)){ break; } // Revision 0 not supported.
          // Return ON/OFF flags.
          Name(BUF3, Package()
          {
            0xF // driver EMI ENABLE mask, CH1:CH2:CH3:CH4 - 1b=ON, 0b=OFF
            // EMI mask bit set but rail name is NULL -> EMI is not created (private channel)
            // channel name valid but mask bit not set -> EMI is not created (private channel)
          })
          return(BUF3)
        }

        // Function 4 returns the channel bipolar settings
        case(4)
        {
          If(LLess(Arg1,1)){ break; } // Revision 0 not supported.
          // Return the target NEG_PWR flags.
          Name(BUF4, Package()
          {
            0x00 // CH1:CH2:CH3:CH4-BIDI : CH1:CH2:CH3:CH4-BIDV
          })
          return(BUF4)
        }

        // Function 5 returns the SPS for ACTIVE and IDLE
        case(5)
        {
        If(LLess(Arg1,1)){ break; } // Revision 0 not supported.
          Name(BUF5, Package()
          {
            1024, // ACTIVE sps - accepted values = {1024, 256, 64, 8}
            8 // IDLE sps - accepted values = {1024, 256, 64, 8}
          })
          return(BUF5)
        }

        // Function 6 returns the watchdog interval
        case(6)
        {
          If(LLess(Arg1,1)){ break; } // Revision 0 not supported.
          Name(BUF6, Package()
          {
            900 // seconds (min=60, max=900)
          })
          return(BUF6)
        }

      } // switch(Arg2)
      //
      // Return an error (a buffer with a value of zero)
      // if we didn't return anything else above
      //
      return(Buffer() {0x0})
    } // _DSM
  } // PowerMeter Device PA01 Scope End

  //
  // PA02 Device: Defines how the PAC193x is connected to the I2C Controller.
  //
  Device(PA02)
  {
    Name(_HID, "MCHP1930")
    Name(_UID, 2)

    // Lowest power D-State supported by the device is D3
    Name (_S0W, 3)

    // Device Status: present, enabled, and functioning properly
    Method(_STA, 0x0, NotSerialized)
    {
      If(POME)
      {
        Switch (ToInteger(PLID)) {
          Case (Package () {BoardIdAdlSAdpSSbgaDdr5SODimmCrb}) {
            Return(0x0F)
          }
        }
      }
      Return (0x00)
    }

    //
    // Current Resources Settings
    //
    Method(_CRS, 0x0, NotSerialized)
    {
      Name(RBUF, ResourceTemplate()
      {
        I2CSerialBus(0x0,                   // 7-bit Peripheral Address
                    ControllerInitiated,    // Peripheral or Controller?
                    400000,                 // Connection Speed in hz
                    AddressingMode7Bit,     // 7-bit or 10-bit addressing?
                    "\\_SB.PC00.I2C5",      // I2C Controller to which PAC is connected
                    0,                      // Resource Index
                    ResourceConsumer,       // Consumer or Producer?
                    I2CB)                   // Descriptor name
      })
      CreateWordField(RBUF,I2CB._ADR,BADR)
      Switch (ToInteger(PLID)) {
        Case (Package () {BoardIdAdlSAdpSSbgaDdr5SODimmCrb}) {
          Store (0x14, BADR)
        }
        Default {
          Store (0x0, BADR)
        }
      }
      Return(RBUF)
    }

    //
    // _DSM - Device Specific Method
    //
    // This method returns configuration information that tells the driver
    // which devices each line in the PAC193x is wired to monitor.
    // Names of the monitored devices must come from the Microsoft-defined
    // power rail taxonomy.
    //
    // The UUID for the Microchip PAC193x's DSM is {033771E0-1705-47B4-9535-D1BBE14D9A09}.
    // This is unique to the device, and must match what the Windows PAC193x driver expects.
    //
    // Returns:
    //         Either: A Buffer (for Functions 0 and 2, or an error)
    //                 A Package containing EMI resources (rail/resistor pairs)
    //
    // Input Arguments (per _DSM standard):
    //
    //     Arg0: UUID - Function Identifier
    //     Arg1: Integer - Revision
    //     Arg2: Integer - Function Index
    //     Arg3: Package - Parameters (not used in our implementation)
    //
    Function(_DSM, {BuffObj, PkgObj}, {BuffObj, IntObj, IntObj, PkgObj})
    {
      //
      //  Is our UUID being invoked?
      //
      if(LNotEqual(Arg0, ToUUID("033771E0-1705-47B4-9535-D1BBE14D9A09")))
      {
        return(Buffer() {0x0})  // incorrect UUID, return NULL for error
      }

      //
      // Switch based on the function number requested...
      //
      switch(ToInteger(Arg2))
      {
        //
        // Function zero returns a bit-mask of supported functions
        //
        case(0)
        {
          switch(ToInteger(Arg1)) // revision check
          {
            case(0)
            {
              return (Buffer() {0x3}) // Revision 0: function 1 is supported
            }
            case(1)
            {
              return (Buffer() {0x7f}) // Revision 1: functions 1->6 are supported
            }
          }
          break;
        }

        // Function 1 returns the channel "Rail Name" and the Resistor Value expressed in milli-Ohms.
        case(1)
        {
          // All Revisions supported (0 and 1 presently defined)
          Name(PKG1, Package()
          {
            Package()
            {
              // Rail Name, Resistor Value
              "", 0, // Channel 1
              "", 0, // Channel 2
              "", 0, // Channel 3
              "", 0  // Channel 4
            },
            Package()
            {
              // Rail Name, Resistor Value
              "SYSTEM_VCCCORE_VIN",   2,   // Channel 1
              "SYSTEM_VCCGT_VIN",     2,   // Channel 2
              "CPU_V1P8",             10,  // Channel 3
              "CPU_V1P05",            5    // Channel 4
            }
          })
          Switch (ToInteger(PLID)) {
            Case (Package () {BoardIdAdlSAdpSSbgaDdr5SODimmCrb}) {
              return(DerefOf(Index(PKG1, 1)))
            }
            Default {
              return(DerefOf(Index(PKG1, 0)))
            }
          }
        }

        // Function 2 returns the Resistor values expressed in micro-Ohms.
        case(2)
        {
          If(LLess(Arg1,1)){ break; } // Revision 0 not supported.
          // Return enhanced precision resistor values.
          Name(PKG2, Package()
          {
            Package()
            {
              // Value
              0,       // Channel 1
              0,       // Channel 2
              0,       // Channel 3
              0        // Channel 4
            },
            Package()
            {
              // Value
              2000,     // Channel 1
              2000,     // Channel 2
              10000,    // Channel 3
              5000      // Channel 4
            }
          })
          Switch (ToInteger(PLID)) {
            Case (Package () {BoardIdAdlSAdpSSbgaDdr5SODimmCrb}) {
              return(DerefOf(Index(PKG2, 1)))
            }
            Default {
              return(DerefOf(Index(PKG2, 0)))
            }
          }
        }

        // Function 3 returns the EMI enabled/disabled flags
        case(3)
        {
          If(LLess(Arg1,1)){ break; } // Revision 0 not supported.
          // Return ON/OFF flags.
          Name(BUF3, Package()
          {
            0xF // driver EMI ENABLE mask, CH1:CH2:CH3:CH4 - 1b=ON, 0b=OFF
            // EMI mask bit set but rail name is NULL -> EMI is not created (private channel)
            // channel name valid but mask bit not set -> EMI is not created (private channel)
          })
          return(BUF3)
        }

        // Function 4 returns the channel bipolar settings
        case(4)
        {
          If(LLess(Arg1,1)){ break; } // Revision 0 not supported.
          // Return the target NEG_PWR flags.
          Name(BUF4, Package()
          {
            0x00 // CH1:CH2:CH3:CH4-BIDI : CH1:CH2:CH3:CH4-BIDV
          })
          return(BUF4)
        }

        // Function 5 returns the SPS for ACTIVE and IDLE
        case(5)
        {
        If(LLess(Arg1,1)){ break; } // Revision 0 not supported.
          Name(BUF5, Package()
          {
            1024, // ACTIVE sps - accepted values = {1024, 256, 64, 8}
            8 // IDLE sps - accepted values = {1024, 256, 64, 8}
          })
          return(BUF5)
        }

        // Function 6 returns the watchdog interval
        case(6)
        {
          If(LLess(Arg1,1)){ break; } // Revision 0 not supported.
          Name(BUF6, Package()
          {
            900 // seconds (min=60, max=900)
          })
          return(BUF6)
        }
      } // switch(Arg2)
      //
      // Return an error (a buffer with a value of zero)
      // if we didn't return anything else above
      //
      return(Buffer() {0x0})
    } // _DSM
  } // PowerMeter Device PA02 Scope End

  //
  // PA03 Device: Defines how the PAC193x is connected to the I2C Controller.
  //
  Device(PA03)
  {
    Name(_HID, "MCHP1930")
    Name(_UID, 3)

    // Lowest power D-State supported by the device is D3
    Name (_S0W, 3)

    // Device Status: present, enabled, and functioning properly
    Method(_STA, 0x0, NotSerialized)
    {
      If(POME)
      {
        Switch (ToInteger(PLID)) {
          Case (Package () {BoardIdAdlSAdpSSbgaDdr5SODimmCrb}) {
            Return(0x0F)
          }
        }
      }
      Return (0x00)
    }

    //
    // Current Resources Settings
    //
    Method(_CRS, 0x0, NotSerialized)
    {
      Name(RBUF, ResourceTemplate()
      {
        I2CSerialBus(0x0,                   // 7-bit Peripheral Address
                    ControllerInitiated,    // Peripheral or Controller?
                    400000,                 // Connection Speed in hz
                    AddressingMode7Bit,     // 7-bit or 10-bit addressing?
                    "\\_SB.PC00.I2C5",      // I2C Controller to which PAC is connected
                    0,                      // Resource Index
                    ResourceConsumer,       // Consumer or Producer?
                    I2CB)                   // Descriptor name
      })
      CreateWordField(RBUF,I2CB._ADR,BADR)
      Switch (ToInteger(PLID)) {
        Case (Package () {BoardIdAdlSAdpSSbgaDdr5SODimmCrb}) {
          Store (0x16, BADR)
        }
        Default {
          Store (0x0, BADR)
        }
      }
      Return(RBUF)
    }

    //
    // _DSM - Device Specific Method
    //
    // This method returns configuration information that tells the driver
    // which devices each line in the PAC193x is wired to monitor.
    // Names of the monitored devices must come from the Microsoft-defined
    // power rail taxonomy.
    //
    // The UUID for the Microchip PAC193x's DSM is {033771E0-1705-47B4-9535-D1BBE14D9A09}.
    // This is unique to the device, and must match what the Windows PAC193x driver expects.
    //
    // Returns:
    //         Either: A Buffer (for Functions 0 and 2, or an error)
    //                 A Package containing EMI resources (rail/resistor pairs)
    //
    // Input Arguments (per _DSM standard):
    //
    //     Arg0: UUID - Function Identifier
    //     Arg1: Integer - Revision
    //     Arg2: Integer - Function Index
    //     Arg3: Package - Parameters (not used in our implementation)
    //
    Function(_DSM, {BuffObj, PkgObj}, {BuffObj, IntObj, IntObj, PkgObj})
    {
      //
      //  Is our UUID being invoked?
      //
      if(LNotEqual(Arg0, ToUUID("033771E0-1705-47B4-9535-D1BBE14D9A09")))
      {
        return(Buffer() {0x0})  // incorrect UUID, return NULL for error
      }

      //
      // Switch based on the function number requested...
      //
      switch(ToInteger(Arg2))
      {
        //
        // Function zero returns a bit-mask of supported functions
        //
        case(0)
        {
          switch(ToInteger(Arg1)) // revision check
          {
            case(0)
            {
              return (Buffer() {0x3}) // Revision 0: function 1 is supported
            }
            case(1)
            {
              return (Buffer() {0x7f}) // Revision 1: functions 1->6 are supported
            }
          }
          break;
        }

        // Function 1 returns the channel "Rail Name" and the Resistor Value expressed in milli-Ohms.
        case(1)
        {
          // All Revisions supported (0 and 1 presently defined)
          Name(PKG1, Package()
          {
            Package()
            {
              // Rail Name, Resistor Value
              "", 0, // Channel 1
              "", 0, // Channel 2
              "", 0, // Channel 3
              "", 0  // Channel 4
            },
            Package()
            {
              // Rail Name, Resistor Value
              "SYSTEM_VCCINAUX_VIN",        5,  // Channel 1
              "CPU_VCCDD2",                 5,  // Channel 2
              "DRAM_V5A",                   2,  // Channel 3
              "SYSTEM_VBATA",               1   // Channel 4
            }
          })
          Switch (ToInteger(PLID)) {
            Case (Package () {BoardIdAdlSAdpSSbgaDdr5SODimmCrb}) {
              return(DerefOf(Index(PKG1, 1)))
            }
            Default {
              return(DerefOf(Index(PKG1, 0)))
            }
          }
        }

        // Function 2 returns the Resistor values expressed in micro-Ohms.
        case(2)
        {
          If(LLess(Arg1,1)){ break; } // Revision 0 not supported.
          // Return enhanced precision resistor values.
          Name(PKG2, Package()
          {
            Package()
            {
              // Value
              0, // Channel 1
              0, // Channel 2
              0, // Channel 3
              0  // Channel 4
            },
            Package()
            {
              // Value
             5000,  // Channel 1
             5000,  // Channel 2
             2000,  // Channel 3
             1000   // Channel 4
            }
          })
          Switch (ToInteger(PLID)) {
            Case (Package () {BoardIdAdlSAdpSSbgaDdr5SODimmCrb}) {
              return(DerefOf(Index(PKG2, 1)))
            }
            Default {
              return(DerefOf(Index(PKG2, 0)))
            }
          }
        }

        // Function 3 returns the EMI enabled/disabled flags
        case(3)
        {
          If(LLess(Arg1,1)){ break; } // Revision 0 not supported.
          // Return ON/OFF flags.
          Name(BUF3, Package()
          {
            0xF // driver EMI ENABLE mask, CH1:CH2:CH3:CH4 - 1b=ON, 0b=OFF
            // EMI mask bit set but rail name is NULL -> EMI is not created (private channel)
            // channel name valid but mask bit not set -> EMI is not created (private channel)
          })
          return(BUF3)
        }

        // Function 4 returns the channel bipolar settings
        case(4)
        {
          If(LLess(Arg1,1)){ break; } // Revision 0 not supported.
          // Return the target NEG_PWR flags.
          Name(BUF4, Package()
          {
            0x00 // CH1:CH2:CH3:CH4-BIDI : CH1:CH2:CH3:CH4-BIDV
          })
          return(BUF4)
        }

        // Function 5 returns the SPS for ACTIVE and IDLE
        case(5)
        {
        If(LLess(Arg1,1)){ break; } // Revision 0 not supported.
          Name(BUF5, Package()
          {
            1024, // ACTIVE sps - accepted values = {1024, 256, 64, 8}
               8  // IDLE sps - accepted values = {1024, 256, 64, 8}
          })
          return(BUF5)
        }

        // Function 6 returns the watchdog interval
        case(6)
        {
          If(LLess(Arg1,1)){ break; } // Revision 0 not supported.
          Name(BUF6, Package()
          {
            900 // seconds (min=60, max=900)
          })
          return(BUF6)
        }

      } // switch(Arg2)
      //
      // Return an error (a buffer with a value of zero)
      // if we didn't return anything else above
      //
      return(Buffer() {0x0})
    } // _DSM
  } // PowerMeter Device PA03 Scope End

  //
  // PA04 Device: Defines how the PAC193x is connected to the I2C Controller.
  //
  Device(PA04)
  {
    Name(_HID, "MCHP1930")
    Name(_UID, 4)

    // Lowest power D-State supported by the device is D3
    Name (_S0W, 3)

    // Device Status: present, enabled, and functioning properly
    Method(_STA, 0x0, NotSerialized)
    {
      If(POME)
      {
        Switch (ToInteger(PLID)) {
          Case (Package () {BoardIdAdlSAdpSSbgaDdr5SODimmCrb}) {
            Return(0x0F)
          }
        }
      }
      Return (0x00)
    }

    //
    // Current Resources Settings
    //
    Method(_CRS, 0x0, NotSerialized)
    {
      Name(RBUF, ResourceTemplate()
      {
        I2CSerialBus(0x0,                   // 7-bit Peripheral Address
                    ControllerInitiated,    // Peripheral or Controller?
                    400000,                 // Connection Speed in hz
                    AddressingMode7Bit,     // 7-bit or 10-bit addressing?
                    "\\_SB.PC00.I2C5",      // I2C Controller to which PAC is connected
                    0,                      // Resource Index
                    ResourceConsumer,       // Consumer or Producer?
                    I2CB)                   // Descriptor name
      })
      CreateWordField(RBUF,I2CB._ADR,BADR)
      Switch (ToInteger(PLID)) {
        Case (Package () {BoardIdAdlSAdpSSbgaDdr5SODimmCrb}) {
          Store (0x18, BADR)
        }
        Default {
          Store (0x0, BADR)
        }
      }
      Return(RBUF)
    }

    //
    // _DSM - Device Specific Method
    //
    // This method returns configuration information that tells the driver
    // which devices each line in the PAC193x is wired to monitor.
    // Names of the monitored devices must come from the Microsoft-defined
    // power rail taxonomy.
    //
    // The UUID for the Microchip PAC193x's DSM is {033771E0-1705-47B4-9535-D1BBE14D9A09}.
    // This is unique to the device, and must match what the Windows PAC193x driver expects.
    //
    // Returns:
    //         Either: A Buffer (for Functions 0 and 2, or an error)
    //                 A Package containing EMI resources (rail/resistor pairs)
    //
    // Input Arguments (per _DSM standard):
    //
    //     Arg0: UUID - Function Identifier
    //     Arg1: Integer - Revision
    //     Arg2: Integer - Function Index
    //     Arg3: Package - Parameters (not used in our implementation)
    //
    Function(_DSM, {BuffObj, PkgObj}, {BuffObj, IntObj, IntObj, PkgObj})
    {
      //
      //  Is our UUID being invoked?
      //
      if(LNotEqual(Arg0, ToUUID("033771E0-1705-47B4-9535-D1BBE14D9A09")))
      {
        return(Buffer() {0x0})  // incorrect UUID, return NULL for error
      }

      //
      // Switch based on the function number requested...
      //
      switch(ToInteger(Arg2))
      {
        //
        // Function zero returns a bit-mask of supported functions
        //
        case(0)
        {
          switch(ToInteger(Arg1)) // revision check
          {
            case(0)
            {
              return (Buffer() {0x3}) // Revision 0: function 1 is supported
            }
            case(1)
            {
              return (Buffer() {0x7f}) // Revision 1: functions 1->6 are supported
            }
          }
          break;
        }

        // Function 1 returns the channel "Rail Name" and the Resistor Value expressed in milli-Ohms.
        case(1)
        {
          // All Revisions supported (0 and 1 presently defined)
          Name(PKG1, Package()
          {
            // Rail Name, Resistor Value
            Package () {
              "",     0, // Channel 1
              "",     0, // Channel 2
              "",     0, // Channel 3
              "",     0  // Channel 4
            },
            Package () {
              "PLATFORM_V1P8A",             5,  // Channel 1
              "PCH_V0P82A",                 2 , // Channel 2
              "PLATFORM_V3P3A_DSW",         1,  // Channel 3
              "PLATFORM_V5A_DS3",           1   // Channel 4
            }
          })
          Switch (ToInteger(PLID)) {
            Case (Package () {BoardIdAdlSAdpSSbgaDdr5SODimmCrb}) {
              return(DerefOf(Index(PKG1, 1)))
            }
            Default {
              return(DerefOf(Index(PKG1, 0)))
            }
          }
        }

        // Function 2 returns the Resistor values expressed in micro-Ohms.
        case(2)
        {
          If(LLess(Arg1,1)){ break; } // Revision 0 not supported.
          // Return enhanced precision resistor values.
          Name(PKG2, Package()
          {
            // Value
            Package () {
              0, // Channel 1
              0, // Channel 2
              0, // Channel 3
              0  // Channel 4
            },
            Package () {
             5000,  // Channel 1
             2000,  // Channel 2
             1000,  // Channel 3
             1000   // Channel 4
            }
          })
          Switch (ToInteger(PLID)) {
            Case (Package () {BoardIdAdlSAdpSSbgaDdr5SODimmCrb}) {
              return(DerefOf(Index(PKG2, 1)))
            }
            Default {
              return(DerefOf(Index(PKG2, 0)))
            }
          }
        }

        // Function 3 returns the EMI enabled/disabled flags
        case(3)
        {
          If(LLess(Arg1,1)){ break; } // Revision 0 not supported.
          // Return ON/OFF flags.
          Name(BUF3, Package()
          {
            0xF // driver EMI ENABLE mask, CH1:CH2:CH3:CH4 - 1b=ON, 0b=OFF
            // EMI mask bit set but rail name is NULL -> EMI is not created (private channel)
            // channel name valid but mask bit not set -> EMI is not created (private channel)
          })
          return(BUF3)
        }

        // Function 4 returns the channel bipolar settings
        case(4)
        {
          If(LLess(Arg1,1)){ break; } // Revision 0 not supported.
          // Return the target NEG_PWR flags.
          Name(BUF4, Package()
          {
            0x00 // CH1:CH2:CH3:CH4-BIDI : CH1:CH2:CH3:CH4-BIDV
          })
          return(BUF4)
        }

        // Function 5 returns the SPS for ACTIVE and IDLE
        case(5)
        {
        If(LLess(Arg1,1)){ break; } // Revision 0 not supported.
          Name(BUF5, Package()
          {
            1024, // ACTIVE sps - accepted values = {1024, 256, 64, 8}
               8  // IDLE sps - accepted values = {1024, 256, 64, 8}
          })
          return(BUF5)
        }

        // Function 6 returns the watchdog interval
        case(6)
        {
          If(LLess(Arg1,1)){ break; } // Revision 0 not supported.
          Name(BUF6, Package()
          {
            900 // seconds (min=60, max=900)
          })
          return(BUF6)
        }

      } // switch(Arg2)
      //
      // Return an error (a buffer with a value of zero)
      // if we didn't return anything else above
      //
      return(Buffer() {0x0})
    } // _DSM
  } // PowerMeter Device PA04 Scope End

  //
  // PA05 Device: Defines how the PAC193x is connected to the I2C Controller.
  //
  Device(PA05)
  {
    Name(_HID, "MCHP1930")
    Name(_UID, 5)

    // Lowest power D-State supported by the device is D3
    Name (_S0W, 3)

    // Device Status: present, enabled, and functioning properly
    Method(_STA, 0x0, NotSerialized)
    {
      If(POME)
      {
        Switch (ToInteger(PLID)) {
          Case (Package () {BoardIdAdlSAdpSSbgaDdr5SODimmCrb}) {
            Return(0x0F)
          }
        }
      }
      Return (0x00)
    }

    //
    // Current Resources Settings
    //
    Method(_CRS, 0x0, NotSerialized)
    {
      Name(RBUF, ResourceTemplate()
      {
        I2CSerialBus(0x0,                   // 7-bit Peripheral Address
                    ControllerInitiated,    // Peripheral or Controller?
                    400000,                 // Connection Speed in hz
                    AddressingMode7Bit,     // 7-bit or 10-bit addressing?
                    "\\_SB.PC00.I2C5",      // I2C Controller to which PAC is connected
                    0,                      // Resource Index
                    ResourceConsumer,       // Consumer or Producer?
                    I2CB)                   // Descriptor name
      })
      CreateWordField(RBUF,I2CB._ADR,BADR)
      Switch (ToInteger(PLID)) {
        Case (Package () {BoardIdAdlSAdpSSbgaDdr5SODimmCrb}) {
          Store (0x1C, BADR)
        }
        Default {
          Store (0x0, BADR)
        }
      }
      Return(RBUF)
    }

    //
    // _DSM - Device Specific Method
    //
    // This method returns configuration information that tells the driver
    // which devices each line in the PAC193x is wired to monitor.
    // Names of the monitored devices must come from the Microsoft-defined
    // power rail taxonomy.
    //
    // The UUID for the Microchip PAC193x's DSM is {033771E0-1705-47B4-9535-D1BBE14D9A09}.
    // This is unique to the device, and must match what the Windows PAC193x driver expects.
    //
    // Returns:
    //         Either: A Buffer (for Functions 0 and 2, or an error)
    //                 A Package containing EMI resources (rail/resistor pairs)
    //
    // Input Arguments (per _DSM standard):
    //
    //     Arg0: UUID - Function Identifier
    //     Arg1: Integer - Revision
    //     Arg2: Integer - Function Index
    //     Arg3: Package - Parameters (not used in our implementation)
    //
    Function(_DSM, {BuffObj, PkgObj}, {BuffObj, IntObj, IntObj, PkgObj})
    {
      //
      //  Is our UUID being invoked?
      //
      if(LNotEqual(Arg0, ToUUID("033771E0-1705-47B4-9535-D1BBE14D9A09")))
      {
        return(Buffer() {0x0})  // incorrect UUID, return NULL for error
      }

      //
      // Switch based on the function number requested...
      //
      switch(ToInteger(Arg2))
      {
        //
        // Function zero returns a bit-mask of supported functions
        //
        case(0)
        {
          switch(ToInteger(Arg1)) // revision check
          {
            case(0)
            {
              return (Buffer() {0x3}) // Revision 0: function 1 is supported
            }
            case(1)
            {
              return (Buffer() {0x7f}) // Revision 1: functions 1->6 are supported
            }
          }
          break;
        }

        // Function 1 returns the channel "Rail Name" and the Resistor Value expressed in milli-Ohms.
        case(1)
        {
          // All Revisions supported (0 and 1 presently defined)
          Name(PKG1, Package()
          {
            // Rail Name, Resistor Value
            Package () {
              "",     0, // Channel 1
              "",     0, // Channel 2
              "",     0, // Channel 3
              "",     0  // Channel 4
            },
            Package () {
              "STORAGE_CPU_SSD",             10,  // Channel 1
              "CONNECTIVITY_WLAN",           2,   // Channel 2
              "CONNECTIVITY_LAN",            10,  // Channel 3
              "CONNECTIVITY_WWAN",           10   // Channel 4
            }
          })
          Switch (ToInteger(PLID)) {
            Case (Package () {BoardIdAdlSAdpSSbgaDdr5SODimmCrb}) {
              return(DerefOf(Index(PKG1, 1)))
            }
            Default {
              return(DerefOf(Index(PKG1, 0)))
            }
          }
        }

        // Function 2 returns the Resistor values expressed in micro-Ohms.
        case(2)
        {
          If(LLess(Arg1,1)){ break; } // Revision 0 not supported.
          // Return enhanced precision resistor values.
          Name(PKG2, Package()
          {
            // Value
            Package () {
              0, // Channel 1
              0, // Channel 2
              0, // Channel 3
              0  // Channel 4
            },
            Package () {
             10000,  // Channel 1
             2000,   // Channel 2
             10000,  // Channel 3
             10000   // Channel 4
            }
          })
          Switch (ToInteger(PLID)) {
            Case (Package () {BoardIdAdlSAdpSSbgaDdr5SODimmCrb}) {
              return(DerefOf(Index(PKG2, 1)))
            }
            Default {
              return(DerefOf(Index(PKG2, 0)))
            }
          }
        }

        // Function 3 returns the EMI enabled/disabled flags
        case(3)
        {
          If(LLess(Arg1,1)){ break; } // Revision 0 not supported.
          // Return ON/OFF flags.
          Name(BUF3, Package()
          {
            0xF // driver EMI ENABLE mask, CH1:CH2:CH3:CH4 - 1b=ON, 0b=OFF
            // EMI mask bit set but rail name is NULL -> EMI is not created (private channel)
            // channel name valid but mask bit not set -> EMI is not created (private channel)
          })
          return(BUF3)
        }

        // Function 4 returns the channel bipolar settings
        case(4)
        {
          If(LLess(Arg1,1)){ break; } // Revision 0 not supported.
          // Return the target NEG_PWR flags.
          Name(BUF4, Package()
          {
            0x00 // CH1:CH2:CH3:CH4-BIDI : CH1:CH2:CH3:CH4-BIDV
          })
          return(BUF4)
        }

        // Function 5 returns the SPS for ACTIVE and IDLE
        case(5)
        {
        If(LLess(Arg1,1)){ break; } // Revision 0 not supported.
          Name(BUF5, Package()
          {
            1024, // ACTIVE sps - accepted values = {1024, 256, 64, 8}
               8  // IDLE sps - accepted values = {1024, 256, 64, 8}
          })
          return(BUF5)
        }

        // Function 6 returns the watchdog interval
        case(6)
        {
          If(LLess(Arg1,1)){ break; } // Revision 0 not supported.
          Name(BUF6, Package()
          {
            900 // seconds (min=60, max=900)
          })
          return(BUF6)
        }

      } // switch(Arg2)
      //
      // Return an error (a buffer with a value of zero)
      // if we didn't return anything else above
      //
      return(Buffer() {0x0})
    } // _DSM
  } // PowerMeter Device PA05 Scope End

} // Scope I2C5