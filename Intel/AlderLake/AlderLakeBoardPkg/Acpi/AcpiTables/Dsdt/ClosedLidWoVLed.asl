/** @file
  Closed Lid WoV LED Lighting Driver Support

@copyright
  INTEL CONFIDENTIAL
  Copyright 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains a 'Sample Driver' and is licensed as such under the terms
  of your license agreement with Intel or your vendor. This file may be modified
  by the user, subject to the additional terms of the license agreement.

@par Specification Reference:
**/

#define I2C_BUS_V2_RES_SOU_CON_STR_OFF 31 // Offset to update ResourceSource Controller number string

Scope (\_SB) {

  //
  // TI LP5012 LED controller over I2C
  //
  Device(LP5X) {

    Name(_HID, "INTC1090")
    Name(_UID, 1)

    Method(_STA, 0x0, NotSerialized)
    {
      If (LEqual(LP5E, 1)) { // TI LP5012 LED controller feature is enabled
        Return (0x0F)
      } Else {
        Return (0x00)
      }
    }

    Method(_CRS, 0x0, NotSerialized)
    {

      Name (RBUF , ResourceTemplate () { I2cSerialBusV2 (0x0, ControllerInitiated, 400000, AddressingMode7Bit,
          "\\_SB.PC00.I2Cx", 0x00, ResourceConsumer, DEV0,) })

      /* RBUF array holds I2cSerialBusV2() encoded opcode and data parameters - which is 35 bytes array
         0x8E,0x1E,0x00,0x02,0x00,0x01,0x02,0x00,
         0x00,0x01,0x06,0x00,0x80,0x1A,0x06,0x00,
         0x00,0x00,0x5C,0x5F,0x53,0x42,0x2E,0x50,
         0x43,0x30,0x30,0x2E,0x49,0x32,0x43,0x78,
         0x00,0x79,0x00

         Slave address is located at offset 16
         ResourceSource or Controller scope(\\_SB.PC00.I2Cx) is located at offset 18 to 31
      */

      CreateByteField(RBUF, I2C_BUS_V2_RES_SOU_CON_STR_OFF, CNST)     // Controller string offset
      CreateWordField (RBUF, DEV0._ADR, SLAD)                         // Device slave address offset

      Store (CLCN, CNST)                                              // Update controller number string
      Store (CLSA, SLAD)                                              // Update slave address


      Return(RBUF)
    }

  } // Device(LP5X)
}  // \_SB