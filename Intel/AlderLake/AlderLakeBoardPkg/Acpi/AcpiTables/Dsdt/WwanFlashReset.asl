/** @file
  Intel ACPI Sample Code for WWAN Flash Reset Method

@copyright
  INTEL CONFIDENTIAL
  Copyright 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains a 'Sample Driver' and is licensed as such under the terms
  of your license agreement with Intel or your vendor. This file may be modified
  by the user, subject to the additional terms of the license agreement.

@par Specification Reference:
**/

If (LEqual (Arg0, 0)) {
} Else {
}
// 1. Perform First Half of Warm/Cold Reset Flow
If (CondRefOf (WWAN_PCIE_ROOT_PORT.PXSX.FHRF)) {
  WWAN_PCIE_ROOT_PORT.PXSX.FHRF (Arg0)
}
Sleep (Arg1)
Notify (WWAN_PCIE_ROOT_PORT.PXSX, 1)
// 2. Perform Second Half of Warm/Cold Reset (WWAN ON) Flow
If (CondRefOf (WWAN_PCIE_ROOT_PORT.PXSX.SHRF)) {
  WWAN_PCIE_ROOT_PORT.PXSX.SHRF ()
}
Sleep (Arg2)
Notify (WWAN_PCIE_ROOT_PORT.PXSX, 1)
// 3. Release Reset mutex
Release (\WWMT)
Store (0, Local0)
If (CondRefOf (WWAN_PCIE_ROOT_PORT.PXSX.DOSV)) {
  WWAN_PCIE_ROOT_PORT.PXSX.DOSV ()              // Workaround for OEM only
}
// 4. Optional - Sleep for Secondary Bus Reset Delay.
// Sleep(0)
