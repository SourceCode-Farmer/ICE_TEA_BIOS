/** @file
  CloverFalls (CVF) Driver
  This ASL file will support the CVF companion die which is interfaced
  via Native IOs or USB IOs Expansion.

@copyright
  INTEL CONFIDENTIAL
  Copyright 2020 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains a 'Sample Driver' and is licensed as such under the terms
  of your license agreement with Intel or your vendor. This file may be modified
  by the user, subject to the additional terms of the license agreement.

@par Specification Reference:
**/
External(\SB.PC00.HCID, MethodObj)
Include("GpioAcpiDefinesAdl.h")
Include("GpioAcpiDefinesTgl.h")

#define CBUF_GPIO GPIO_VER2_LP_DRIVER_GPP_B23
#define GBUF_GPIO GPIO_VER2_LP_DRIVER_GPP_R5


Scope(\_SB.PC00.SPI1) {

  Device(SPFD) { //CVF SPI Fucntional Device
    //
    //This object is used to supply OSPM with the devices PNP ID or ACPI ID.
    //
    Method (_HID)
    {
        Return ("INTC1094")
    }

    //
    //DOS Device name
    //This object is used to associate a logical name (for example, COM1) with a device.
    //This name can be used by applications to connect to the device.
    //
    Name(_DDN, "Intel SPI OED Device")

    Name(PKG0, Package() {"\\_SB.PC00.XHCI.RHUB.HS07.VGPO", "\\_SB.PC00.XHCI.RHUB.HS07.VSPI"})
    Name(PKG1, Package() {"\\_SB.PC00.XHCI.RHUB.HS06.VGPO", "\\_SB.PC00.XHCI.RHUB.HS06.VSPI"})
    Name(PKG2, Package() {"\\_SB.PC00.XHCI.RHUB.HS08.VGPO", "\\_SB.PC00.XHCI.RHUB.HS08.VSPI"})
    Name(PKG3, Package() {"\\_SB.PC00.XHCI.RHUB.HS03.VGPO", "\\_SB.PC00.XHCI.RHUB.HS03.VSPI"})
    Method (_DEP, 0, NotSerialized) {  // _DEP: Operation Region Dependencies
      If(LEqual (CVFS,2)) {
        //
        // USB Port Mapping:
        // 3 : ADL M AEP
        // 6 : ADL M
        // 7 : ADL P
        // 8 : ADL P AEP
        //
        If(LEqual (CUPN,0x06)) {
          Return (PKG1)
        } ElseIf(LEqual (CUPN,0x07)) {
          Return (PKG0)
        } ElseIf(LEqual (CUPN,0x08)) {
          Return (PKG2)
        } ElseIf(LEqual (CUPN,0x03)) {
          Return (PKG3)
        }
        Return (Package() {})
      }
      Else {
        Return (Package() {\_SB.PC00})
      }
    }

    Method (_CRS, 0, Serialized) {
      Name (CBUF, ResourceTemplate () {
        //
        //Host IRQ
        //
        GpioIo (Shared, PullDefault, 0, 0, IoRestrictionInputOnly,  "\\_SB.GPI0",,, HIRQ) {CBUF_GPIO}

        //
        //Interrupt to Host
        //
        GpioInt (Edge, ActiveLow, Shared, PullDefault, 0x0000, "\\_SB.GPI0", 0x00, ResourceConsumer, GIN0 ) {  CBUF_GPIO}
      })

      Name (CABF, ResourceTemplate () {
        //
        //Host IRQ
        //
        GpioIo (Shared, PullDefault, 0, 0, IoRestrictionInputOnly,  "\\_SB.GPI0",,, HIRR) {GPIO_VER2_LP_DRIVER_GPP_A7}

        //
        //Interrupt to Host
        //
        GpioInt (Edge, ActiveLow, Shared, PullDefault, 0x0000, "\\_SB.GPI0", 0x00, ResourceConsumer, GIN1 ) {  GPIO_VER2_LP_DRIVER_GPP_A7}
      })

      Name (GBUF, ResourceTemplate () {
        //
        //CVF Reset - To reset CVF
        //
        GpioIo(Exclusive, PullDefault, 0, 0, IoRestrictionOutputOnly, "\\_SB.GPI0",,,HRST) { GBUF_GPIO}

        //
        //Host Wake - To implement HECI Protocol - Sync
        //
        GpioIo(Exclusive, PullDefault, 0, 0, IoRestrictionOutputOnly, "\\_SB.GPI0",,,WKCF) { GPIO_VER2_LP_DRIVER_GPP_B14}
      })

      Name (VGBF, ResourceTemplate () {
        //
        // Virtual GPIOs from USB chip - To support GPIO interrupt.
        //
        GpioIo(Exclusive, PullDefault, 0, 0, IoRestrictionOutputOnly, "\\_SB.PC00.XHCI.RHUB.HS07.VGPO",,,VRST) { 0x0}
        GpioIo(Exclusive, PullDefault, 0, 0, IoRestrictionOutputOnly, "\\_SB.PC00.XHCI.RHUB.HS07.VGPO",,,VWAK) { 0x1}
      })

      Name (VGB1, ResourceTemplate () {
        //
        // Virtual GPIOs from USB chip - To support GPIO interrupt.
        //
        GpioIo(Exclusive, PullDefault, 0, 0, IoRestrictionOutputOnly, "\\_SB.PC00.XHCI.RHUB.HS06.VGPO",,,VRT1) { 0x0}
        GpioIo(Exclusive, PullDefault, 0, 0, IoRestrictionOutputOnly, "\\_SB.PC00.XHCI.RHUB.HS06.VGPO",,,VWK1) { 0x1}
      })

      Name (VGB2, ResourceTemplate () {
        //
        // Virtual GPIOs from USB chip - To support GPIO interrupt.
        //
        GpioIo(Exclusive, PullDefault, 0, 0, IoRestrictionOutputOnly, "\\_SB.PC00.XHCI.RHUB.HS08.VGPO",,,VRT2) { 0x0}
        GpioIo(Exclusive, PullDefault, 0, 0, IoRestrictionOutputOnly, "\\_SB.PC00.XHCI.RHUB.HS08.VGPO",,,VWK2) { 0x1}
      })

      Name (VGB3, ResourceTemplate () {
        //
        // Virtual GPIOs from USB chip - To support GPIO interrupt.
        //
        GpioIo(Exclusive, PullDefault, 0, 0, IoRestrictionOutputOnly, "\\_SB.PC00.XHCI.RHUB.HS03.VGPO",,,VRT3) { 0x0}
        GpioIo(Exclusive, PullDefault, 0, 0, IoRestrictionOutputOnly, "\\_SB.PC00.XHCI.RHUB.HS03.VGPO",,,VWK3) { 0x1}
      })

      Name (SBUF, ResourceTemplate () {
        //
        // SPI Peripheral for 14MHz
        //
        SPISerialBus(0x00,PolarityLow,FourWireMode,8,ControllerInitiated,14000000,ClockPolarityHigh,ClockPhaseSecond,"\\_SB.PC00.SPI1",,,SPII)
      })

      Name (VSBF, ResourceTemplate () {
        //
        // SPI Peripheral for 14MHz
        //
        SPISerialBus(0x00,PolarityLow,FourWireMode,8,ControllerInitiated,14000000,ClockPolarityHigh,ClockPhaseSecond,"\\_SB.PC00.XHCI.RHUB.HS07.VSPI",,,VSII)
      })

      Name (VSB1, ResourceTemplate () {
        //
        // SPI Peripheral for 14MHz
        //
        SPISerialBus(0x00,PolarityLow,FourWireMode,8,ControllerInitiated,14000000,ClockPolarityHigh,ClockPhaseSecond,"\\_SB.PC00.XHCI.RHUB.HS06.VSPI",,,VSI1)
      })

      Name (VSB2, ResourceTemplate () {
        //
        //SPI Slave for 14MHz
        //
        SPISerialBus(0x00,PolarityLow,FourWireMode,8,ControllerInitiated,14000000,ClockPolarityHigh,ClockPhaseSecond,"\\_SB.PC00.XHCI.RHUB.HS08.VSPI",,,VSI2)
      })

      Name (VSB3, ResourceTemplate () {
        //
        //SPI Slave for 14MHz
        //
        SPISerialBus(0x00,PolarityLow,FourWireMode,8,ControllerInitiated,14000000,ClockPolarityHigh,ClockPhaseSecond,"\\_SB.PC00.XHCI.RHUB.HS03.VSPI",,,VSI3)
      })

      If (LEqual(CVFS, CVF_INTERFACE_USB)) {
        //
        // USB Port Mapping:
        // 3 : ADL M AEP
        // 6 : ADL M
        // 7 : ADL P
        //
        If(LEqual (CUPN,0x06)) {
          Return(ConcatenateResTemplate(ConcatenateResTemplate(CBUF, VGB1), VSB1))
        } ElseIf(LEqual (CUPN,0x07)) {
          Return(ConcatenateResTemplate(ConcatenateResTemplate(CBUF, VGBF), VSBF))
        } ElseIf(LEqual (CUPN,0x08)) {
          If(ToInteger(PLID) == BoardIdAdlPLp5Dg128Aep){
            Return(ConcatenateResTemplate(ConcatenateResTemplate(CABF, VGB2), VSB2))
          }
          Return(ConcatenateResTemplate(ConcatenateResTemplate(CBUF, VGB2), VSB2))
        } ElseIf(LEqual (CUPN,0x03)) {
          Return(ConcatenateResTemplate(ConcatenateResTemplate(CBUF, VGB3), VSB3))
        }
        Return (ResourceTemplate () {GpioIo(Exclusive, PullDefault, 0, 0, IoRestrictionNone, "\\_SB.PC00.XHCI.RHUB.HSXX.VGPO",,,DUMM) {0}}) // Dummy Resource
      } Else {
        Return(ConcatenateResTemplate(ConcatenateResTemplate(CBUF, GBUF), SBUF))
      }
    }

    Device(CVFD) {

      Method (_HID)
      {
          Return ("INTC1095")
      }

      Name(_DDN, "Intel CVF Device")

      //
      // Returns the Camera Sensor HID which is configured on LNK1(CVF POR)
      //
      Method(SID,1, Serialized) {
        Return(HCID(1))
      }

    } // End of CVF Device
  } // End of Device(SPD)
} // End of Scope (\_SB.PCI0.SPI1)

//
// USB Port Mapping:
// 3 : ADL M AEP
// 6 : ADL M
// 7 : ADL P
// 8 : ADL XEP
//
If (LEqual (CUPN,0x06)) {
  Scope (\_SB.PC00.XHCI.RHUB.HS06) {
    Include ("CvfVirtualDevice.asl")
  } // End of Scope(\_SB.PC00.XHCI.RHUB)
} ElseIf (LEqual (CUPN,0x07)) {
  Scope (\_SB.PC00.XHCI.RHUB.HS07) {
    Include ("CvfVirtualDevice.asl")
  } // End of Scope(\_SB.PC00.XHCI.RHUB)
} ElseIf (LEqual (CUPN,0x08)) {
  Scope (\_SB.PC00.XHCI.RHUB.HS08) {
    Include ("CvfVirtualDevice.asl")
  } // End of Scope(\_SB.PC00.XHCI.RHUB)
} ElseIf (LEqual (CUPN,0x03)) {
  Scope (\_SB.PC00.XHCI.RHUB.HS03) {
    Include ("CvfVirtualDevice.asl")
  } // End of Scope(\_SB.PC00.XHCI.RHUB)
}