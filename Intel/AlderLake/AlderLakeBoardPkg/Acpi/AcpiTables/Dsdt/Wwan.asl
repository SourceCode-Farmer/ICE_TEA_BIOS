/** @file
  Intel ACPI Sample Code for discrete connectivity modules (WWAN)

@copyright
  INTEL CONFIDENTIAL
  Copyright 2017 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains a 'Sample Driver' and is licensed as such under the terms
  of your license agreement with Intel or your vendor. This file may be modified
  by the user, subject to the additional terms of the license agreement.

@par Specification Reference:
**/

//
// First Half of Reset Flow: WWAN Reset or OFF flow for Reset Recovery
// Arg0 - Indicates Reset Method : Warm Reset - 0  ,  Power Cycle Reset - 1
//
Method (FHRF, 1) {
  If (LNotEqual (\_SB.GGOV (\PRST), \WPRP)) {
    // 1. Trigger L2/L3 ready entry flow in root port
    DL23()
    // 2. Assert PERST GPIO.
    \_SB.SGOV (\PRST, \WPRP)
  } Else {
    ADBG ("Assume WWAN DL23() is already done")
  }

  // 3. Sleep for TR2B ms.
  Sleep (TR2B)
  // 4. Assert RESET GPIO.
  \_SB.SGOV (\WBRS, \PBRS)

  If (LEqual (Arg0, 0)) {                                      // For Warm Reset Flow
    // 5. Sleep for TBTG ms
    Sleep (TBTG)
  } Else {
    If (LEqual (Arg0, 1)) {                                    // For Power Cycle Reset Flow
      // 5. Disable SRC Clock.
      SPCO (\WCLK, 0)
      // 6. Sleep for TB2F ms.
      Sleep (TB2F)
      // 7. Assert FULL_CARD_POWER_OFF# GPIO.
      \_SB.SGOV (\WFCP, And (Not (\PFCP), 1))
      // 8. Sleep for TFDI ms.
      Sleep (TFDI)
    } Else {
    }
  }
}

//
// 2nd Half of Reset Flow: WWAN ON flow for Reset Recovery
//
Method (SHRF) {
  // 1. Disable ModPHY Power Gating.
  \_SB.PSD0 (SLOT)
  // 2. Enable SRC Clock.
  SPCO (\WCLK, 1)
  // 3. De-assert FULL_CARD_POWER_OFF# GPIO.
  \_SB.SGOV (\WFCP, \PFCP)
  // 4. Sleep for TN2B ms.
  Sleep (TN2B)
  // 5. De-assert RESET GPIO.
  \_SB.SGOV (\WBRS, And (Not (\PBRS), 1))
  // 6. Sleep for TB2R ms.
  Sleep (TB2R)
  // 7. De-assert PERST GPIO.
  \_SB.SGOV (\PRST, And (Not (\WPRP), 1))
  // 8. Trigger L2/L3 ready exit flow in root port - transition link to Detect.
  L23D ()
}

//
// Detect OEM SVID
// This is workaroud for OEM only and it's disabled by default.
//
Method (DOSV) {
  Store (0, Local0)
  While (LAnd (LNotEqual (PCIE_ROOT_PORT.PXSX.SVID, WSID), LLess (Local0, WSTO))) {
    Increment (Local0)
    Sleep (1)
  }
}

Method (_RST, 0, Serialized) {

  If (LEqual (WWEN, 2)) {
    //
    // Reset flow for 5G WWAN
    //
    // 1. Acquire FLDR Mutex and save result to check for acquire Mutex
    Store (Acquire (\WWMT, 1000), Local0)
    // 2. Check for FLDR Mutex acquired
    If (LEqual (Local0, Zero))
    {
      // 3. Perform 1st Half of FLDR Flow
      FHRF (0);
      // 4. Perform 2nd Half of FLDR Flow
      SHRF ();
      // 5. Release FLDR mutex
      Release (\WWMT)
      DOSV ();                                              // Workaround for OEM only
    }
  } Else {
    //
    // Reset flow for 4G WWAN
    //
    \_SB.SGOV (\WBRS, \PBRS)                // Drive BB RESET Pin low
    Sleep (200)                             // Safe 200ms
    Notify (PCIE_ROOT_PORT.PXSX, 1)
    \_SB.SGOV (\PRST, And (Not (\WPRP), 1)) // De-assert PERST
    \_SB.SGOV (\WBRS, And (Not (\PBRS), 1)) // Drive BB RESET Pin high
    Sleep (200)
    Store (0, Local0)
    While (LEqual (PCIE_ROOT_PORT.LASX, 0)) {
      If (Lgreater (Local0, 20)) {          // Wait for 300ms
        Break
      }
      Sleep (16)
      Increment (Local0)
    }
    Sleep (10)
    Notify (PCIE_ROOT_PORT.PXSX, 1)
  }
}

PowerResource (MRST, 5, 0)
{
  // Define the PowerResource for the WWAN device
  // PowerResource expects to have _STA, _ON and _OFF Method per ACPI Spec. Not having one of them will cause BSOD

  //
  // Method _STA: Dummy _STA () to comply with ACPI Spec
  //
  Method (_STA, 0, Serialized) {
    return (1)
  }

  //
  // Method _ON: Dummy _ON () to comply with ACPI Spec
  //
  Method (_ON, 0, Serialized) {
  }

  //
  // Method _OFF: Dummy _OFF () to comply with ACPI Spec
  //
  Method (_OFF, 0, Serialized) {
  }

  //
  // Method _RST (): executes a WWAN device power-cycle reset using FULL_CARD_POWER_OFF# and RESET# signals
  //
  Method (_RST, 0, Serialized) {
    // 1. Acquire _PRR Mutex and save result to check for acquire Mutex
    Store (Acquire (\WWMT, 1000), Local0)
    // 2. Check for _PRR Mutex acquired
    If (LEqual (Local0, Zero))
    {
      // 3. Perform 1st Half of PLDR Flow
      FHRF (1);
      // 4. Perform 2nd Half of PLDR Flow
      SHRF ();
      // 5. Release _PRR mutex
      Release (\WWMT)
      DOSV (); // Workaround for OEM only
    }
  }
} // End MRST
