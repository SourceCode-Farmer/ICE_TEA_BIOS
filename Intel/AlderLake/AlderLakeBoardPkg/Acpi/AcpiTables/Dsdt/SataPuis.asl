/** @file
  ACPI Sata PUIS Enablement

@copyright
  INTEL CONFIDENTIAL
  Copyright 2020 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains a 'Sample Driver' and is licensed as such under the terms
  of your license agreement with Intel or your vendor. This file may be modified
  by the user, subject to the additional terms of the license agreement.

@par Specification Reference:
**/

Name (GTF0, Buffer (0x7) {0x00,0x00,0x00,0x00,0x00,0xA0,0x00})

//
// Set Device Data Method
// Arg0 - A Buffer containing an ATA drive identify block, contents described by the ATA
//
Method (_SDD, 0x1, NotSerialized) {
  CreateByteField(GTF0, 0x0, FEAT)
  CreateByteField(GTF0, 0x6, CMMD)
  CreateWordField(Arg0, 0xA6, W083)  // Identify Device Data Word 83
  CreateWordField(Arg0, 0xAC, W086)  // Identify Device Data Word 86

  If (LEqual(SizeOf(Arg0), 0x200)) {  // 256 WORD Data Block by OSPM to send 0xEC Command to Device
    // Check if PUIS feature is supported (Word 83 Bit5)
    If (LEqual (And (W083, 0x0020), 0x0020)) {
      // Check enable state (Word 86 Bit5)
      If (LEqual (And (W086, 0x0020), 0)) {
        // Issue Set Feature EFh, subcommand 06h to enable PUIS
        Store (0x06, FEAT)
        Store (0xEF, CMMD)
      }
    }
  }
}

//
// Get Task File Method
//
// @return 0 - A Buffer containing a byte stream of ATA commands for the drive
//
Method(_GTF, 0, NotSerialized) {
  return (GTF0)

}
