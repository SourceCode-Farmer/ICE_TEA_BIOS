/**@file
 Touch Panel Device of ACPI

@copyright
  INTEL CONFIDENTIAL
  Copyright 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains a 'Sample Driver' and is licensed as such under the terms
  of your license agreement with Intel or your vendor. This file may be modified
  by the user, subject to the additional terms of the license agreement.

@par Specification Reference:
**/

//
// TPL Connected Device
//
#define ATMEL3432_PANEL               1
#define ATMEL2952_PANEL               2
#define ELAN9048_PANEL                3
#define NTRIG_SAMSUNG_PANEL           4
#define NTRIG_SHARP_PANEL             5
#define WACOM_PANEL                   6
#define SERIAL_IO_TPL_CUSTOM_DEVICE   7 // Custom TouchPanel device

//------------------------
// Touch Panels on I2C for common use
// Note: instead of adding more touch panels, parametrize this one with appropriate _HID value and GPIO numbers
//------------------------
//[-start-210729-Kebin00040-modify]//
#ifdef LCFC_SUPPORT
#ifdef C970_SUPPORT
  Device (TPL1) {
    Name (HID2,0)
//[-start-210902-YUNLEI0128-modify]//
    Name (SBFB, ResourceTemplate () { I2cSerialBus (0,ControllerInitiated,1000000,AddressingMode7Bit,"\\_SB.PC00.I2C0",,,I2CA) })
//[-end-210902-YUNLEI0128-modify]//
    Name (SBFG, ResourceTemplate () {
      GpioInt (Level, ActiveLow, ExclusiveAndWake, PullDefault, 0x0000, "\\_SB.GPI0", 0x00, ResourceConsumer, INTG) {0}
    })
    Name (SBFI, ResourceTemplate () {
      Interrupt (ResourceConsumer, Level, ActiveLow, ExclusiveAndWake,,,INTI) {0}
    })
    CreateWordField(SBFB,I2CA._ADR,BADR)
    CreateDWordField(SBFB,I2CA._SPE,SPED)
    CreateWordField (SBFG,INTG._PIN,INT1)
    CreateDWordField(SBFI,INTI._INT,INT2)

    Method(_INI) {
      If (LLess(OSYS,2012)) { SRXO(GPIO_VER2_LP_GPP_D16,1) }
      Store(GNUM(GPIO_VER2_LP_GPP_D16),INT1)
      Store(INUM(GPIO_VER2_LP_GPP_D16),INT2)
      if(LEqual(TPNP,0x52B6)||(LEqual(TPNP,0x52B9))||(LEqual(TPNP,0x52BD))||(LEqual(TPNP,0x52BE))||(LEqual(TPNP,0x52C2))||(LEqual(TPNP,0x52C3))) { // WCOM touch chip
        store("WACF2200",_HID)
        store(0x01,HID2)
        store(0x0A,BADR)
//[-start-210902-YUNLEI0128-modify]//
        store(1000000,SPED)
//[-end-210902-YUNLEI0128-modify]//
        Return
    }
    }
    Name (_HID, "XXXX0000")
    Name (_CID, "PNP0C50")
    Name (_S0W, 3)
    Method(_DSM, 0x4, Serialized) {
      If (LEqual(Arg0,HIDG)) { Return(HIDD(Arg0,Arg1,Arg2,Arg3,HID2)) }
      If (LEqual(Arg0,TP7G)) { Return(TP7D(Arg0,Arg1,Arg2,Arg3,SBFB,SBFG)) }
      Return (Buffer(1){0})
    }

    Method (_STA, 0, NotSerialized) {
      If (LEqual(TPNY,0x00)) {
        Return (0x00)
      }Else {
        Return (0x0F)
      }
    }

    Method (_CRS, 0, NotSerialized) {
      If (LLess(OSYS, 2012)) { Return (SBFI) } // For Windows 7 only report Interrupt; it doesn't support ACPI5.0 and wouldn't understand GpioInt nor I2cBus
      If (LEqual(TPLM,0)) { Return (ConcatenateResTemplate(I2CM(I2CX,BADR,SPED), SBFG)) }
      Return (ConcatenateResTemplate(I2CM(I2CX,BADR,SPED), SBFI))
    }

  } // Device (TPL1)
#endif
#ifdef C770_SUPPORT
  Device (TPL1) {
    Name (HID2,0)
//[-start-210902-YUNLEI0128-modify]//
    Name (SBFB, ResourceTemplate () { I2cSerialBus (0,ControllerInitiated,1000000,AddressingMode7Bit,"\\_SB.PC00.I2C0",,,I2CA) })
//[-end-210902-YUNLEI0128-modify]//
    Name (SBFG, ResourceTemplate () {
      GpioInt (Level, ActiveLow, ExclusiveAndWake, PullDefault, 0x0000, "\\_SB.GPI0", 0x00, ResourceConsumer, INTG) {0}
    })
    Name (SBFI, ResourceTemplate () {
      Interrupt (ResourceConsumer, Level, ActiveLow, ExclusiveAndWake,,,INTI) {0}
    })
    CreateWordField(SBFB,I2CA._ADR,BADR)
    CreateDWordField(SBFB,I2CA._SPE,SPED)
    CreateWordField (SBFG,INTG._PIN,INT1)
    CreateDWordField(SBFI,INTI._INT,INT2)

    Method(_INI) {
      If (LLess(OSYS,2012)) { SRXO(GPIO_VER2_LP_GPP_D16,1) }
      Store(GNUM(GPIO_VER2_LP_GPP_D16),INT1)
      Store(INUM(GPIO_VER2_LP_GPP_D16),INT2)
//[-start-210922-YUNLEI0137-modify]//
      if(LEqual(TPNP,0x52D1) || (LEqual(TPNP,0x52D2)) || (LEqual(TPNP,0x52D3)) || (LEqual(TPNP,0x52D4)) || (LEqual(TPNP,0x52D5))) { // WCOM touch chip
        store("WACF2200",_HID)
//[-end-210922-YUNLEI0137-modify]//
        store(0x01,HID2)
        store(0x0A,BADR)
//[-start-210902-YUNLEI0128-modify]//
        store(1000000,SPED)
//[-end-210902-YUNLEI0128-modify]//
        Return
    }
    }
    Name (_HID, "XXXX0000")
    Name (_CID, "PNP0C50")
    Name (_S0W, 3)
    Method(_DSM, 0x4, Serialized) {
      If (LEqual(Arg0,HIDG)) { Return(HIDD(Arg0,Arg1,Arg2,Arg3,HID2)) }
      If (LEqual(Arg0,TP7G)) { Return(TP7D(Arg0,Arg1,Arg2,Arg3,SBFB,SBFG)) }
      Return (Buffer(1){0})
    }

    Method (_STA, 0, NotSerialized) {
      If (LEqual(TPNY,0x00)) {
        Return (0x00)
      }Else {
        Return (0x0F)
      }
    }

    Method (_CRS, 0, NotSerialized) {
      If (LLess(OSYS, 2012)) { Return (SBFI) } // For Windows 7 only report Interrupt; it doesn't support ACPI5.0 and wouldn't understand GpioInt nor I2cBus
      If (LEqual(TPLM,0)) { Return (ConcatenateResTemplate(I2CM(I2CX,BADR,SPED), SBFG)) }
      Return (ConcatenateResTemplate(I2CM(I2CX,BADR,SPED), SBFI))
    }

  } // Device (TPL1)
#endif

//[-start-210916-xinwei0002-add]//  
#ifdef S370_SUPPORT
    Device (TPL1) {
      External(\_SB.PC00.I2C0, DeviceObj) //I2C0 Controller
      Name (HID2,0)
//[-start-210902-YUNLEI0128-modify]//
      Name (SBFB, ResourceTemplate () { I2cSerialBus (0,ControllerInitiated,1000000,AddressingMode7Bit,"\\_SB.PC00.I2C0",,,I2CA) })
//[-end-210902-YUNLEI0128-modify]//
      Name (SBFG, ResourceTemplate () {
        GpioInt (Level, ActiveLow, ExclusiveAndWake, PullDefault, 0x0000, "\\_SB.GPI0", 0x00, ResourceConsumer, INTG) {0}
      })
      Name (SBFI, ResourceTemplate () {
        Interrupt (ResourceConsumer, Level, ActiveLow, ExclusiveAndWake,,,INTI) {0}
      })
      CreateWordField(SBFB,I2CA._ADR,BADR)
      CreateDWordField(SBFB,I2CA._SPE,SPED)
      CreateWordField (SBFG,INTG._PIN,INT1)
      CreateDWordField(SBFI,INTI._INT,INT2)
  
      Method(_INI) {
        If (LLess(OSYS,2012)) { SRXO(GPIO_VER2_LP_GPP_D16,1) }
        Store(GNUM(GPIO_VER2_LP_GPP_D16),INT1)
        Store(INUM(GPIO_VER2_LP_GPP_D16),INT2)
//[-start-220106-QINGLIN0142-modify]//
        if(LEqual(TPNP,0xD64D)) { // BOE touchpanel AC600013N30/SD10W73240 AC60001HR00/SD11C74689
          store("GTCH7503",_HID)
          store(0x01,HID2)
          store(0x10,BADR)
          store(1000000,SPED)
          Return
        }
        if(LEqual(TPNP,0x2D72)) { // IVO touchpanel AC60001AJ70/SD11F99464
          store("ELAN901C",_HID)
          store(0x01,HID2)
          store(0x10,BADR)
          store(1000000,SPED)
          Return
        }
        if(LEqual(TPNP,0x2EE3)) { // IVO touchpanel AC60001Q000/SD11D69016
          store("ELAN901C",_HID)
          store(0x01,HID2)
          store(0x10,BADR)
          store(1000000,SPED)
          Return
        }
        if(LEqual(TPNP,0x2EFD)) { // AUO touchpanel AC600017E20/SD11D69013
          store("GTCH7503",_HID)
          store(0x01,HID2)
          store(0x10,BADR)
          store(1000000,SPED)
          Return
        }
        if(LEqual(TPNP,0xA815)) { // BOE touchpanel AC600012UP0/SD11D69015
          store("GTCH7503",_HID)
          store(0x01,HID2)
          store(0x10,BADR)
          store(1000000,SPED)
          Return
        }
//[-end-220106-QINGLIN0142-modify]//
      }
      Name (_HID, "XXXX0000")
      Name (_CID, "PNP0C50")
      Name (_S0W, 3)
      Method(_DSM, 0x4, Serialized) {
        If (LEqual(Arg0,HIDG)) { Return(HIDD(Arg0,Arg1,Arg2,Arg3,HID2)) }
        If (LEqual(Arg0,TP7G)) { Return(TP7D(Arg0,Arg1,Arg2,Arg3,SBFB,SBFG)) }
        Return (Buffer(1){0})
      }
  
      Method (_STA, 0, NotSerialized) {
        If (LEqual(TPNY,0x00)) {
          Return (0x00)
        }Else {
          Return (0x0F)
        }
      }
  
      Method (_CRS, 0, NotSerialized) {
        If (LLess(OSYS, 2012)) { Return (SBFI) } // For Windows 7 only report Interrupt; it doesn't support ACPI5.0 and wouldn't understand GpioInt nor I2cBus
        If (LEqual(TPLM,0)) { Return (ConcatenateResTemplate(I2CM(I2CX,BADR,SPED), SBFG)) }
        Return (ConcatenateResTemplate(I2CM(I2CX,BADR,SPED), SBFI))
      }

    } // Device (TPL1)
#endif
//[-end-210916-xinwei0002-add]//  
//[-start-211213-GEORGE0031-modify]//
//[-start-210918-GEORGE0005-add]//
#if defined(S77014_SUPPORT) || defined(S77014IAH_SUPPORT)
  Device (TPL1) {
    Name (HID2,0)
  //[-start-210902-YUNLEI0128-modify]//
    Name (SBFB, ResourceTemplate () { I2cSerialBus (0,ControllerInitiated,400000,AddressingMode7Bit,"\\_SB.PC00.I2C0",,,I2CA) })
  //[-end-210902-YUNLEI0128-modify]//
    Name (SBFG, ResourceTemplate () {
      GpioInt (Level, ActiveLow, ExclusiveAndWake, PullDefault, 0x0000, "\\_SB.GPI0", 0x00, ResourceConsumer, INTG) {0}
    })
    Name (SBFI, ResourceTemplate () {
      Interrupt (ResourceConsumer, Level, ActiveLow, ExclusiveAndWake,,,INTI) {0}
    })
    CreateWordField(SBFB,I2CA._ADR,BADR)
    CreateDWordField(SBFB,I2CA._SPE,SPED)
    CreateWordField (SBFG,INTG._PIN,INT1)
    CreateDWordField(SBFI,INTI._INT,INT2)
  
    Method(_INI) {
      If (LLess(OSYS,2012)) { SRXO(GPIO_VER2_LP_GPP_D16,1) }
      Store(GNUM(GPIO_VER2_LP_GPP_D16),INT1)
      Store(INUM(GPIO_VER2_LP_GPP_D16),INT2)
//[-start-220216-GEORGE0051-add]// 
      if(LEqual(TPNP,0x4008)) { // IVO touchpanel
        store("ELAN901C",_HID) 	
        store(0x01,HID2)
        store(0x10,BADR)
        store(400000,SPED)
        Return
      }
//[-end-220216-GEORGE0051-add]//
//[-start-211105-GEORGE0019-add]//  
      if(LEqual(TPNP,0x4009)) { // IVO touchpanel
        store("ELAN901C",_HID) 	
        store(0x01,HID2)
        store(0x10,BADR)
        store(400000,SPED)
        Return
      }
//[-end-211105-GEORGE0019-add]//    
      if(LEqual(TPNP,0x2E8A)) { // IVO touchpanel
//        if(LEqual(TPNY,0xA3)) { // IVO touchpanel
        store("ELANXXXX",_HID)
        store(0x01,HID2)
        store(0x10,BADR)
        store(400000,SPED)
        Return
      }
    }
    Name (_HID, "XXXX0000")
    Name (_CID, "PNP0C50")
    Name (_S0W, 3)
    Method(_DSM, 0x4, Serialized) {
      If (LEqual(Arg0,HIDG)) { Return(HIDD(Arg0,Arg1,Arg2,Arg3,HID2)) }
      If (LEqual(Arg0,TP7G)) { Return(TP7D(Arg0,Arg1,Arg2,Arg3,SBFB,SBFG)) }
      Return (Buffer(1){0})
    }
  
    Method (_STA, 0, NotSerialized) {
      If (LEqual(TPNY,0x00)) {
        Return (0x00)
      }Else {
        Return (0x0F)
      }
    }
  
    Method (_CRS, 0, NotSerialized) {
      If (LLess(OSYS, 2012)) { Return (SBFI) } // For Windows 7 only report Interrupt; it doesn't support ACPI5.0 and wouldn't understand GpioInt nor I2cBus
      If (LEqual(TPLM,0)) { Return (ConcatenateResTemplate(I2CM(I2CX,BADR,SPED), SBFG)) }
      Return (ConcatenateResTemplate(I2CM(I2CX,BADR,SPED), SBFI))
    }
  } // Device (TPL1)
#endif
//[-end-210918-GEORGE0005-add]//
//[-end-211213-GEORGE0031-modify]//
//[-start-210919-QINGLIN0076-add]//
#ifdef S570_SUPPORT
    Device (TPL1) {
      External(\_SB.PC00.I2C0, DeviceObj) //I2C0 Controller
      Name (HID2,0)
//[-start-210902-YUNLEI0128-modify]//
      Name (SBFB, ResourceTemplate () { I2cSerialBus (0,ControllerInitiated,1000000,AddressingMode7Bit,"\\_SB.PC00.I2C0",,,I2CA) })
//[-end-210902-YUNLEI0128-modify]//
      Name (SBFG, ResourceTemplate () {
        GpioInt (Level, ActiveLow, ExclusiveAndWake, PullDefault, 0x0000, "\\_SB.GPI0", 0x00, ResourceConsumer, INTG) {0}
      })
      Name (SBFI, ResourceTemplate () {
        Interrupt (ResourceConsumer, Level, ActiveLow, ExclusiveAndWake,,,INTI) {0}
      })
      CreateWordField(SBFB,I2CA._ADR,BADR)
      CreateDWordField(SBFB,I2CA._SPE,SPED)
      CreateWordField (SBFG,INTG._PIN,INT1)
      CreateDWordField(SBFI,INTI._INT,INT2)
  
      Method(_INI) {
        If (LLess(OSYS,2012)) { SRXO(GPIO_VER2_LP_GPP_D16,1) }
        Store(GNUM(GPIO_VER2_LP_GPP_D16),INT1)
        Store(INUM(GPIO_VER2_LP_GPP_D16),INT2)
        if(LEqual(TPNP,0xD64D)) { // BOE touchpanel
          store("GTCH7503",_HID)
          store(0x01,HID2)
          store(0x10,BADR)
          store(1000000,SPED)
          Return
        }
//[-start-210922-QINGLIN0078-add]//
        if(LEqual(TPNP,0x2D72)) { // IVO touchpanel
          store("ELAN901C",_HID)
          store(0x01,HID2)
          store(0x10,BADR)
          store(1000000,SPED)
          Return
        }
//[-end-210922-QINGLIN0078-add]//
//[-start-220802-OWENWU0044-add]//
	if(LEqual(TPNP,0x40D2)) { // INX touchpanel
          store("ELAN901C",_HID)
          store(0x01,HID2)
          store(0x10,BADR)
          store(400000,SPED)
          Return
        }
//[-end-220802-OWENWU0044-add]//	
      }
      Name (_HID, "XXXX0000")
      Name (_CID, "PNP0C50")
      Name (_S0W, 3)
      Method(_DSM, 0x4, Serialized) {
        If (LEqual(Arg0,HIDG)) { Return(HIDD(Arg0,Arg1,Arg2,Arg3,HID2)) }
        If (LEqual(Arg0,TP7G)) { Return(TP7D(Arg0,Arg1,Arg2,Arg3,SBFB,SBFG)) }
        Return (Buffer(1){0})
      }
  
      Method (_STA, 0, NotSerialized) {
        If (LEqual(TPNY,0x00)) {
          Return (0x00)
        } Else {
          Return (0x0F)
        }
      }
  
      Method (_CRS, 0, NotSerialized) {
        If (LLess(OSYS, 2012)) { Return (SBFI) } // For Windows 7 only report Interrupt; it doesn't support ACPI5.0 and wouldn't understand GpioInt nor I2cBus
        If (LEqual(TPLM,0)) { Return (ConcatenateResTemplate(I2CM(I2CX,BADR,SPED), SBFG)) }
        Return (ConcatenateResTemplate(I2CM(I2CX,BADR,SPED), SBFI))
      }

    } // Device (TPL1)
#endif
//[-end-210919-QINGLIN0076-add]//
//[-start-211213-GEORGE0031-modify]//
//[-start-211110-GEORGE0020-add]//
#ifdef S77013_SUPPORT
  Device (TPL1) {
    Name (HID2,0)
  //[-start-210902-YUNLEI0128-modify]//
    Name (SBFB, ResourceTemplate () { I2cSerialBus (0,ControllerInitiated,400000,AddressingMode7Bit,"\\_SB.PC00.I2C0",,,I2CA) })
  //[-end-210902-YUNLEI0128-modify]//
    Name (SBFG, ResourceTemplate () {
      GpioInt (Level, ActiveLow, ExclusiveAndWake, PullDefault, 0x0000, "\\_SB.GPI0", 0x00, ResourceConsumer, INTG) {0}
    })
    Name (SBFI, ResourceTemplate () {
      Interrupt (ResourceConsumer, Level, ActiveLow, ExclusiveAndWake,,,INTI) {0}
    })
    CreateWordField(SBFB,I2CA._ADR,BADR)
    CreateDWordField(SBFB,I2CA._SPE,SPED)
    CreateWordField (SBFG,INTG._PIN,INT1)
    CreateDWordField(SBFI,INTI._INT,INT2)
  
    Method(_INI) {
      If (LLess(OSYS,2012)) { SRXO(GPIO_VER2_LP_GPP_D16,1) }
      Store(GNUM(GPIO_VER2_LP_GPP_D16),INT1)
      Store(INUM(GPIO_VER2_LP_GPP_D16),INT2)
//      if(LEqual(TPNP,0x4009)) { // IVO touchpanel
        if(LEqual(TPNY,0xA3)) { // three ELAN touch IC use the same HID
        store("ELAN901C",_HID) 	
        store(0x01,HID2)
        store(0x10,BADR)
        store(400000,SPED)
        Return
      }
    }
    Name (_HID, "XXXX0000")
    Name (_CID, "PNP0C50")
    Name (_S0W, 3)
    Method(_DSM, 0x4, Serialized) {
      If (LEqual(Arg0,HIDG)) { Return(HIDD(Arg0,Arg1,Arg2,Arg3,HID2)) }
      If (LEqual(Arg0,TP7G)) { Return(TP7D(Arg0,Arg1,Arg2,Arg3,SBFB,SBFG)) }
      Return (Buffer(1){0})
    }
  
    Method (_STA, 0, NotSerialized) {
      If (LEqual(TPNY,0x00)) {
        Return (0x00)
      }Else {
        Return (0x0F)
      }
    }
  
    Method (_CRS, 0, NotSerialized) {
      If (LLess(OSYS, 2012)) { Return (SBFI) } // For Windows 7 only report Interrupt; it doesn't support ACPI5.0 and wouldn't understand GpioInt nor I2cBus
      If (LEqual(TPLM,0)) { Return (ConcatenateResTemplate(I2CM(I2CX,BADR,SPED), SBFG)) }
      Return (ConcatenateResTemplate(I2CM(I2CX,BADR,SPED), SBFI))
    }
  } // Device (TPL1)
#endif
//[-end-211110-GEORGE0020-add]//
//[-end-211213-GEORGE0031-modify]//
#else
  Device (TPL1) {
    Name (HID2,0)
    Name (SBFB, ResourceTemplate () { I2cSerialBus (0,ControllerInitiated,400000,AddressingMode7Bit,"NULL",,,I2CB) })
    Name (SBFG, ResourceTemplate () {
      GpioInt (Level, ActiveLow, Exclusive, PullUp, 0x0000, "\\_SB.GPI0", 0x00, ResourceConsumer,INTG ) { 0 }
    })
    Name (SBFI, ResourceTemplate () {
      Interrupt(ResourceConsumer, Level, ActiveLow, Exclusive,,,INTI) {0}
    })
    CreateWordField(SBFB,I2CB._ADR,BADR)
    CreateDWordField(SBFB,I2CB._SPE,SPED)
    CreateWordField (SBFG,INTG._PIN,INT1)
    CreateDWordField(SBFI,INTI._INT,INT2)
    Method(_INI) {
      If (LLess(OSYS,2012)) { SRXO(GPLI,1) }
      Store (GNUM(GPLI),INT1)
      Store (INUM(GPLI),INT2)
      If (LEqual(TPLM,0)) {
        SHPO(GPLI,1) // configure gpio pad in gpio driver mode
      }
      If (LEqual(TPLT, ATMEL3432_PANEL)) {
        Store ("ATML3432",_HID)
        Store (0,HID2)
        Store (0x4C,BADR)
        Store (400000,SPED)
        Return
      }
      If (LEqual(TPLT, ATMEL2952_PANEL)) {
        Store ("ATML2952",_HID)
        Store (0,HID2)
        Store (0x4A,BADR)
        Store (400000,SPED)
        Return
      }
      If (LEqual(TPLT, ELAN9048_PANEL)) {
        Store ("ELAN9048",_HID)
        Store (1,HID2)
        Store (0x10,BADR)
        Store (400000,SPED)
        Return
      }
      If (LEqual(TPLT, NTRIG_SAMSUNG_PANEL)) {
        Store ("NTRG0001",_HID)
        Store (1,HID2)
        Store (0x07,BADR)
        Store (400000,SPED)
        Return
      }
      If (LEqual(TPLT, NTRIG_SHARP_PANEL)) {
        Store ("NTRG0002",_HID)
        Store (1,HID2)
        Store (0x64,BADR)
        Store (400000,SPED)
        Return
      }
      If(LEqual(TPLT, WACOM_PANEL)) {
        Store("WCOM508E",_HID)
        Store(1,HID2)
        Store(0x0A,BADR)
        If (LEqual(TPLS,0)) { Store( 400000,SPED) } // Fast mode
        If (LEqual(TPLS,1)) { Store( 400000,SPED) } // Fast mode
        If (LEqual(TPLS,2)) { Store(1000000,SPED) } // Fast mode
        Return
      }
      If(LEqual(TPLT, SERIAL_IO_TPL_CUSTOM_DEVICE)) { // Custom TouchPanel
        Store("CUST0000",_HID)
        Store(TPLH,HID2)
        Store(TPLB,BADR)
        If (LEqual(TPLS,0)) { Store( 100000,SPED) }
        If (LEqual(TPLS,1)) { Store( 400000,SPED) }
        If (LEqual(TPLS,2)) { Store(1000000,SPED) }
        Return
      }
    }

    Name (_HID, "XXXX0000")
    Name (_CID, "PNP0C50")
    Method (_UID, 0) {
      Return (I2CI)
    }

    Name (_S0W, 4)
    Method(_DSM, 0x4, Serialized) {
      If (LEqual(Arg0,HIDG)) { Return(HIDD(Arg0,Arg1,Arg2,Arg3,HID2)) }
      If (LEqual(Arg0,TP7G)) { Return(TP7D(Arg0,Arg1,Arg2,Arg3,SBFB,SBFG)) }
      Return (Buffer(1){0})
    }

    Method (_STA, 0, NotSerialized) {
      If (LAnd(LNotEqual(TPLT,0),And( I2CN, SERIAL_IO_I2C_TOUCHPANEL))) { Return (0x0F) }
      Return (0x00)
    }

    Method (_CRS, 0, NotSerialized) {
      If (LLess(OSYS, 2012)) { Return (SBFI) } // For Windows 7 only report Interrupt; it doesn't support ACPI5.0 and wouldn't understand GpioInt nor I2cBus
      If (LEqual(TPLM,0)) { Return (ConcatenateResTemplate(I2CM(I2CX,BADR,SPED), SBFG)) }
      Return (ConcatenateResTemplate(I2CM(I2CX,BADR,SPED), SBFI))
    }
  } // Device (TPL1)
#endif
//[-end-210729-Kebin00040-modify]//
