/** @file
  Intel ACPI Reference Code for _DSM BT Feature Enabling Implementation

@copyright
  INTEL CONFIDENTIAL
  Copyright 2020 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains a 'Sample Driver' and is licensed as such under the terms
  of your license agreement with Intel or your vendor. This file may be modified
  by the user, subject to the additional terms of the license agreement.

@par Specification Reference:
  **/

//--------------------------------------------------
//  Intel Proprietary BT Feature Enabling Solution
//--------------------------------------------------
//
// _DSM (Device Specific Method)
//
// This optional object is a control method that enables devices to provide device specific control
// functions that are consumed by the device driver.
//
// Arguments: (4)
//   Arg0 - A Buffer containing a UUID
//   Arg1 - An Integer containing the Revision ID
//   Arg2 - An Integer containing the Function Index
//   Arg3 - A Package that contains function-specific arguments
// Return Value:
//   If Function Index = 0, a Buffer containing a function index bit-field which has set of supported function indexes.
//   Otherwise, the return feature value based on set of supported function type which depends on the UUID and revision ID.
//

Name(RDLY, 105)

Method(_DSM, 0x4, Serialized, 0, {IntObj, BuffObj}, {BuffObj, IntObj, IntObj, PkgObj})
{
  If(LEqual(Arg0, ToUUID("AA10F4E0-81AC-4233-ABF6-3B2AC50E28D9"))) {
    //
    // Function 0 : Query Function/Get supported functions (based on revision)
    //              For Revision 0,
    //              -- Return value would contain one bit for each function index. Starting with zero.
    //              -- Bit 0 indicates whether there is support for any functions other than function 0 for the specified UUID and Revision ID.
    //              -- If set to zero, no functions are supported.
    //
    If(LEqual(Arg2, Zero)) {
      // Revision 0
      If(LEqual(Arg1, Zero)){ // The current revision is 0
        //
        // Below are functions defined.
        // Bit0    - Indicates whether its support for any other functions
        // Bit1    - Set BT reset timing
        // Bit31:2 - Reserved for future options
        Return(Buffer(){0x03})
      } Else {
        Return(Buffer(){0}) // No supported functions.
      }
    }

    //
    // Function 1 : Set BT reset timing
    //
    If(LEqual(Arg2, One)) {
      Store(Arg3, RDLY)
    }

    // No other supported functions, but normal return.
    Return(0)
  } ElseIf (LEqual(Arg0, ToUUID("2D19D3E1-5708-4696-BD5B-2C3DBAE2D6A9"))) {
    //
    // Function 0 : Query Function/Get supported functions (based on revision)
    //              For Revision 0,
    //              -- Return value would contain one bit for each function index. Starting with zero.
    //              -- Bit 0 indicates whether there is support for any functions other than function 0 for the specified UUID and Revision ID.
    //              -- If set to zero, no functions are supported.
    //
    If(LEqual(Arg2, Zero)) {
      // Revision 0
      If(LEqual(Arg1, Zero)){ // The current revision is 0
        //
        // Below are functions defined.
        // Bit0    - Indicates whether its support for any other functions
        // Bit1    - Set Tile Activation
        // Bit31:2 - Reserved for future options
        Return(Buffer(){0x03})
      } Else {
        Return(Buffer(){0}) // No supported functions.
      }
    }

    //
    // Function 1 : Set Tile Activation
    //
    // Currently no action to BIOS in function 1. OEM can define if anything is needed when setting Tile activation.
    //
    If(LEqual(Arg2, One)) {
    }

    // No other supported functions, but normal return.
    Return(0)
  } Else {
    Return(Buffer(){0}) // Guid mismatch
  }
}
