/**@file
 PMAX Devices ACPI table

@copyright
  INTEL CONFIDENTIAL
  Copyright 2020 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains a 'Sample Driver' and is licensed as such under the terms
  of your license agreement with Intel or your vendor. This file may be modified
  by the user, subject to the additional terms of the license agreement.

**/
#include <Include/PmaxDevDef.h>

DefinitionBlock (
  "PMAX.aml",
  "SSDT",
  0x02,
  "PmaxDv",
  "Pmax_Dev",
  0x1
)
{
  External(\PXAC, IntObj)
  External(\PXWC, IntObj)
  External(\PXUC, IntObj)
  External(\PXWC, IntObj)
  External(\PXFD, IntObj)
  External(\PXDC, IntObj)

  Scope(\_SB) {
  //------------------------
  // PMAX device description
  // Note: This is a sample table describing device peak power values for ADL platform for the PMAX driver to consume
  //------------------------
    Device (PMAX)
    {
      Name(_HID, "INT3533")

      Name(RTKS, DEFAULT_REALTEK_CODEC_DEVICE_STRING)
      Name(RTK0, DEFAULT_REALTEK_CODEC_DEVICE_D0_PEAK_POWER)
      Name(RTKX, DEFAULT_REALTEK_CODEC_DEVICE_DX_PEAK_POWER)

      Name(WFCS, DEFAULT_WF_CAMERA_STRING)
      Name(WFC0, DEFAULT_WF_CAMERA_D0_PEAK_POWER)
      Name(WFCX, DEFAULT_WF_CAMERA_DX_PEAK_POWER)

      Name(UFCS, DEFAULT_UF_CAMERA_STRING)
      Name(UFC0, DEFAULT_UF_CAMERA_D0_PEAK_POWER)
      Name(UFCX, DEFAULT_UF_CAMERA_DX_PEAK_POWER)

      Name(FLDS, DEFAULT_FLASH_DEVICE_STRING)
      Name(FLD0, DEFAULT_FLASH_DEVICE_D0_PEAK_POWER)
      Name(FLDX, DEFAULT_FLASH_DEVICE_DX_PEAK_POWER)

      //
      // Package and names above are place holders, will be updated in boot time.
      //
      Name(DSMP, Package() {
        Package() {
          "",                       // Realtek codec device
          Package() {
            Package() {0, 0xFFFF},  // D0 peak power in mW
            Package() {1, 0xFFFF}   // Dx peak power in mW
          }
        },
        Package() {
          "",                       // WF Camera
          Package() {
            Package() {0, 0xFFFF},  // D0 peak power in mW without accounting for flash
            Package() {1, 0xFFFF}   // Dx peak power in mW
          }
        },
        Package() {
          "",                       // UF Camera
          Package() {
            Package() {0, 0xFFFF},  // D0 peak power in mW without accounting for flash
            Package() {1, 0xFFFF}   // Dx peak power in mW
          }
        },
        Package() {
          "",                       // Flash device
          Package() {
            Package() {0, 0xFFFF},  // D0 peak power in mW
            Package() {1, 0xFFFF}   // Dx peak power in mW
          }
        }
      })

      Method (_DSM, 4, NotSerialized) {   // _DSM: Device-Specific Method
        If (LEqual(Arg0,ToUUID("2256364F-A3A9-4c9a-BCEC-A34A34A57144"))) {
          //
          // Update device name and peak power values.
          //
          Store(RTKS, Index (DeRefOf (Index (DSMP, 0)), 0))
          Store(RTK0, Index (DeRefOf (Index (DeRefOf (Index (DeRefOf (Index (DSMP, 0)), 1)), 0)), 1))
          Store(RTKX, Index (DeRefOf (Index (DeRefOf (Index (DeRefOf (Index (DSMP, 0)), 1)), 1)), 1))

          Store(WFCS, Index (DeRefOf (Index (DSMP, 1)), 0))
          Store(WFC0, Index (DeRefOf (Index (DeRefOf (Index (DeRefOf (Index (DSMP, 1)), 1)), 0)), 1))
          Store(WFCX, Index (DeRefOf (Index (DeRefOf (Index (DeRefOf (Index (DSMP, 1)), 1)), 1)), 1))

          Store(UFCS, Index (DeRefOf (Index (DSMP, 2)), 0))
          Store(UFC0, Index (DeRefOf (Index (DeRefOf (Index (DeRefOf (Index (DSMP, 2)), 1)), 0)), 1))
          Store(UFCX, Index (DeRefOf (Index (DeRefOf (Index (DeRefOf (Index (DSMP, 2)), 1)), 1)), 1))

          Store(FLDS, Index (DeRefOf (Index (DSMP, 3)), 0))
          Store(FLD0, Index (DeRefOf (Index (DeRefOf (Index (DeRefOf (Index (DSMP, 3)), 1)), 0)), 1))
          Store(FLDX, Index (DeRefOf (Index (DeRefOf (Index (DeRefOf (Index (DSMP, 3)), 1)), 1)), 1))

          If (LEqual (Arg1, Zero)) {
            If (LEqual (Arg2, 0)) {
              //- Standard query - A bitmask of functions supported
              //- Supports functions 0-1
              Return ( Buffer() {0x3})
            }
            If (LEqual (Arg2, 1)) {
              // Config PMAX Audio Codec
              If (PXAC == 1) {
                Store(Package(){Package() {0, 0},Package() {1, 0}},Index (DeRefOf (Index (DSMP, 0)), 1)) // Clear D0 peak power in mW
              }
              // Config PMAX WF Camera
              If (PXWC == 1) {
                Store(Package(){Package() {0, 0},Package() {1, 0}},Index (DeRefOf (Index (DSMP, 1)), 1)) // Clear D0 peak power in mW
              }
              // Config PMAX UF Camera
              If (PXUC == 1) {
                Store(Package(){Package() {0, 0},Package() {1, 0}},Index (DeRefOf (Index (DSMP, 2)), 1)) // Clear D0 peak power in mW
              }
              // Config PMAX Flash device
              If (PXFD == 1) {
                Store(Package(){Package() {0, 0},Package() {1, 0}},Index (DeRefOf (Index (DSMP, 3)), 1)) // Clear D0 peak power in mW
              }
              // Power plane request
              // Return a list of packages with device power information
              Return (DSMP)
            }
          }
          Return (0)
        }
        Return (0)
      }

      Method (_STA) {
        If (PXDC == 0) {
          Return (0x0)
        } else {
          Return (0xF)
        }
      }

    }
  }
}
