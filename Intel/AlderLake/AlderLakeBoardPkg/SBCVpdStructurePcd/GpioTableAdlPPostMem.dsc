## @file
#  GPIO definition table for AlderLake P
#
# @copyright
#  INTEL CONFIDENTIAL
#  Copyright 2020 - 2021 Intel Corporation.
#
#  The source code contained or described herein and all documents related to the
#  source code ("Material") are owned by Intel Corporation or its suppliers or
#  licensors. Title to the Material remains with Intel Corporation or its suppliers
#  and licensors. The Material may contain trade secrets and proprietary and
#  confidential information of Intel Corporation and its suppliers and licensors,
#  and is protected by worldwide copyright and trade secret laws and treaty
#  provisions. No part of the Material may be used, copied, reproduced, modified,
#  published, uploaded, posted, transmitted, distributed, or disclosed in any way
#  without Intel's prior express written permission.
#
#  No license under any patent, copyright, trade secret or other intellectual
#  property right is granted to or conferred upon you by disclosure or delivery
#  of the Materials, either expressly, by implication, inducement, estoppel or
#  otherwise. Any license under such intellectual property rights must be
#  express and approved by Intel in writing.
#
#  Unless otherwise agreed by Intel in writing, you may not remove or alter
#  this notice or any other notice embedded in Materials by Intel or
#  Intel's suppliers or licensors in any way.
#
#  This file contains a 'Sample Driver' and is licensed as such under the terms
#  of your license agreement with Intel or your vendor. This file may be modified
#  by the user, subject to the additional terms of the license agreement.
#
# @par Specification
##

###
### !!! GPIOs designated to Native Functions shall not be configured by Platform Code.
### Native Pins shall be configured by Silicon Code (based on BIOS policies setting) or soft straps(set by CSME in FITc).
###
###

#mGpioTableAdlPLp4Rvp
[PcdsDynamicExVpd.common.SkuIdAdlPLp4Rvp]
gBoardModuleTokenSpaceGuid.VpdPcdBoardGpioTable|*|{CODE({
  // CPU M.2 SSD1
  {GPIO_VER2_LP_GPP_D14, {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutHigh,   GpioIntDis,GpioPlatformReset,  GpioTermNone}},  // CPU SSD1 PWREN
  {GPIO_VER2_LP_GPP_F20, {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutHigh,   GpioIntDis,GpioPlatformReset,  GpioTermNone}},  //CPU SSD1 RESET

  // CPU M.2 SSD2
  {GPIO_VER2_LP_GPP_C2, {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutHigh,   GpioIntDis,GpioPlatformReset,  GpioTermNone}},  //CPU SSD2 PWREN
  {GPIO_VER2_LP_GPP_H1, {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutHigh,   GpioIntDis,GpioPlatformReset,  GpioTermNone}},  // CPU SSD2 RESET

  // M.2 Key-E - WLAN/BT
  {GPIO_VER2_LP_GPP_A13, {GpioPadModeGpio, GpioHostOwnDefault, GpioDirOut,    GpioOutHigh,       GpioIntDis,              GpioPlatformReset,  GpioTermNone, GpioPadConfigUnlock }}, // BT_RF_KILL_N
  {GPIO_VER2_LP_GPP_E3,  {GpioPadModeGpio, GpioHostOwnDefault, GpioDirOut,    GpioOutHigh,       GpioIntDis,              GpioPlatformReset,  GpioTermNone, GpioPadConfigUnlock }}, // WIFI_RF_KILL_N
  {GPIO_VER2_LP_GPP_H2,  {GpioPadModeGpio, GpioHostOwnAcpi,    GpioDirOut,    GpioOutHigh,       GpioIntDis,              GpioPlatformReset,  GpioTermNone, GpioPadConfigUnlock }}, // WLAN_RST_N
  {GPIO_VER2_LP_GPP_D13, {GpioPadModeGpio, GpioHostOwnAcpi,    GpioDirInInv,  GpioOutDefault,    GpioIntLevel|GpioIntSci, GpioHostDeepReset,  GpioTermNone, GpioPadConfigUnlock }}, // WIFI_WAKE_N
  {GPIO_VER2_LP_GPP_E0,  {GpioPadModeGpio, GpioHostOwnAcpi,    GpioDirInInv,  GpioOutDefault,    GpioIntLevel|GpioIntSci, GpioHostDeepReset,  GpioTermNone, GpioPadConfigUnlock }}, // UART_BT_WAKE_N

  // X4 Pcie Slot for Gen3 and Gen 4
  {GPIO_VER2_LP_GPP_H17,  {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutLow,   GpioIntDis,GpioPlatformReset,  GpioTermNone}},//ONBOARD_X4_PCIE_SLOT1_PWREN_N
  {GPIO_VER2_LP_GPP_F10, {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutHigh,   GpioIntDis,GpioPlatformReset,  GpioTermNone}},//ONBOARD_X4_PCIE_SLOT1_RESET_N
  //Shared with PEG_SLOT_WAKE. Available after rework
  //{GPIO_VER2_LP_GPP_A20,  {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirInInv,  GpioOutDefault,GpioIntLevel|GpioIntSci,GpioHostDeepReset,  GpioTermNone,  GpioPadConfigUnlock  }},  //ONBOARD_X4_PCIE_SLOT1_WAKE_N

  //Shared pin with peg slot. Avilable after rework.
  // Battery Charger Vmin to PCH PROCHOT, derived from ICL
  //{GPIO_VER2_LP_GPP_B2,  {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirInInv,  GpioOutDefault,GpioIntEdge|GpioIntSci,GpioPlatformReset,  GpioTermNone,  GpioPadConfigUnlock  }},  //BC_PROCHOT_N

  // PCH M.2 SSD
  {GPIO_VER2_LP_GPP_D16, {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutHigh,   GpioIntDis,GpioPlatformReset,  GpioTermNone}},  //M2_PCH_SSD_PWREN
  {GPIO_VER2_LP_GPP_H0,  {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutHigh,   GpioIntDis,GpioPlatformReset,  GpioTermNone}},  //M2_SSD_RST_N

  // Camera
  {GPIO_VER2_LP_GPP_B23, {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutHigh,   GpioIntDis,GpioPlatformReset,  GpioTermNone}},  //CRD1_PWREN_CVF_IRQ - CAM1
  {GPIO_VER2_LP_GPP_R5, {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutHigh,   GpioIntDis,GpioPlatformReset,  GpioTermNone}},  //CRD1_RST_N_CVF_RST_N - CAM1
  {GPIO_VER2_LP_GPP_H15,  {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutHigh,   GpioIntDis,GpioPlatformReset,  GpioTermNone}},  //CRD1_CLK_E
  {GPIO_VER2_LP_GPP_E16, {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutLow,   GpioIntDis,GpioPlatformReset,  GpioTermNone}},  //CAM2_PWREN
  {GPIO_VER2_LP_GPP_E15, {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutHigh,   GpioIntDis,GpioPlatformReset,  GpioTermNone}},  //CAM2_RST_N

  // Camera Common GPIO's for all Camera, Rework Options
  {GPIO_VER2_LP_GPP_B18, {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutLow,   GpioIntDis,GpioPlatformReset,  GpioTermNone}},  //CRD_CAM_STROBE
  {GPIO_VER2_LP_GPP_B14, {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutDefault,   GpioIntDis,GpioPlatformReset,  GpioTermNone}},  //PRIVACY_LED

  // Audio
  {GPIO_VER2_LP_GPP_H3, {GpioPadModeGpio, GpioHostOwnGpio, GpioDirInInv,  GpioOutDefault,GpioIntLevel|GpioIntApic,GpioPlatformReset,  GpioTermNone,  GpioPadConfigUnlock  }},  // CODEC_INT_N

  //Touch Pad
  // Touch Pad and Touch Panel 2 share the same Power Enable, default is Touch pad
  {GPIO_VER2_LP_GPP_F7,  {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutHigh,   GpioIntDis,GpioPlatformReset,  GpioTermNone}},  //TCH_PAD_LS_EN - PWR_En
  {GPIO_VER2_LP_GPP_A15,  {GpioPadModeGpio, GpioHostOwnGpio, GpioDirInInv,  GpioOutDefault,GpioIntEdge|GpioIntApic,GpioPlatformReset,  GpioTermNone,  GpioPadConfigUnlock  }},  //TCH_PAD_INT_N
  {GPIO_VER2_LP_GPP_H1,  {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutHigh,GpioIntDis,GpioPlatformReset,  GpioTermNone  }},  //TCH_PAD_TCH_PNL2_LS_EN / AUDIO_PWREN

  // EC
  {GPIO_VER2_LP_GPP_E7,  {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirInInv,  GpioOutDefault,GpioIntLevel|GpioIntSmi,GpioPlatformReset,  GpioTermNone,  GpioPadConfigUnlock  }},  //EC_SMI_N
  {GPIO_VER2_LP_GPP_F9,  {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutHigh,   GpioIntDis,GpioPlatformReset,  GpioTermNone}},  //EC_SLP_S0_CS_N

  //Shared pin with wlan pin. Avilable after rework.
  // SPI TPM
  //{GPIO_VER2_LP_GPP_E3, {GpioPadModeGpio, GpioHostOwnGpio, GpioDirInInv,  GpioOutDefault,GpioIntLevel|GpioIntApic,GpioHostDeepReset,  GpioTermNone,  GpioPadConfigUnlock  }},  //SPI_TPM_INT_N

 {0x0}  // terminator
})}

#mGpioTableAdlPLp4Bep
[PcdsDynamicExVpd.common.SkuIdAdlPLp4Bep]
gBoardModuleTokenSpaceGuid.VpdPcdBoardGpioTable|*|{CODE({
  // CPU M.2 SSD
  { GPIO_VER2_LP_GPP_D16, { GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutHigh,    GpioIntDis,                   GpioPlatformReset, GpioTermNone } },  //CPU SSD PWREN
  { GPIO_VER2_LP_GPP_F20, { GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutHigh,    GpioIntDis,                   GpioPlatformReset, GpioTermNone } },  //CPU SSD RST_N

  // M.2 Key-E - WLAN/BT
  { GPIO_VER2_LP_GPP_A13, { GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutHigh,    GpioIntDis,                   GpioPlatformReset, GpioTermNone } },  //BT_RF_KILL_N
  { GPIO_VER2_LP_GPP_F14, { GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutHigh,    GpioIntDis,                   GpioPlatformReset, GpioTermNone } },  //WIFI_RF_KILL_N

  // Click Pad
  { GPIO_VER2_LP_GPP_A20, { GpioPadModeGpio, GpioHostOwnGpio, GpioDirInInv,   GpioOutDefault, GpioIntEdge | GpioIntApic,    GpioPlatformReset, GpioTermNone,   GpioPadConfigUnlock } },  //TCH_PAD_INT_N

  // TBT Re-Timers
  { GPIO_VER2_LP_GPD7,    { GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutHigh,    GpioIntDis,                   GpioPlatformReset, GpioTermNone } },  //DEBUG_TRACE_PNP

  // Battery Charger Vmin to PCH PROCHOT, derived from ICL
  //{ GPIO_VER2_LP_GPP_B2,  { GpioPadModeGpio, GpioHostOwnAcpi, GpioDirInInv,   GpioOutDefault, GpioIntEdge | GpioIntSci,     GpioPlatformReset, GpioTermNone,   GpioPadConfigUnlock } },  //BC_PROCHOT_N

  // Camera
  { GPIO_VER2_LP_GPP_B23, { GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutHigh,    GpioIntDis,                   GpioPlatformReset, GpioTermNone } },  //CVF_HOST_IRQ
  { GPIO_VER2_LP_GPP_H15, { GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutHigh,    GpioIntDis,                   GpioPlatformReset, GpioTermNone } },  //CAM_CLK_EN
  { GPIO_VER2_LP_GPP_R5,  { GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutHigh,    GpioIntDis,                   GpioPlatformReset, GpioTermNone } },  //CRD1_RST_N_CVF_RST_N

  // Camera Common GPIO's for all Camera, Rework Options
  { GPIO_VER2_LP_GPP_B18, { GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutLow,     GpioIntDis,                   GpioPlatformReset, GpioTermNone } },  //CRD_CAM_STROBE

  // Audio WoV
  { GPIO_VER2_LP_GPP_A7,  { GpioPadModeGpio, GpioHostOwnGpio, GpioDirDefault, GpioOutDefault, GpioIntDis,                   GpioResetDefault,  GpioTermNone } },  // Audio_WoV_LED

  // EC
  { GPIO_VER2_LP_GPP_E7,  { GpioPadModeGpio, GpioHostOwnGpio, GpioDirInInv,   GpioOutDefault, GpioIntLevel | GpioIntSmi,    GpioPlatformReset, GpioTermNone,   GpioPadConfigUnlock } },  //EC_SMI_N
  { GPIO_VER2_LP_GPP_F9,  { GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutHigh,    GpioIntDis,                   GpioPlatformReset, GpioTermNone } },  //EC_SLP_S0_CS_N

  // Sensor HDR/CVF
  { GPIO_VER2_LP_GPP_B14, { GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutHigh,    GpioIntDis,                   GpioPlatformReset, GpioTermNone } },  //CVF_Host_Wakes

  // SPI TPM
  //{ GPIO_VER2_LP_GPP_E3,  { GpioPadModeGpio, GpioHostOwnGpio, GpioDirInInv,   GpioOutDefault, GpioIntLevel | GpioIntApic,    GpioHostDeepReset, GpioTermWpu20K, GpioPadConfigUnlock } },  //SPI_TPM_INT_N

  {0x0}  // terminator
})}

#mGpioTableAdlPMMAep
[PcdsDynamicExVpd.common.SkuIdAdlPMMAep]
gBoardModuleTokenSpaceGuid.VpdPcdBoardGpioTable|*|{CODE({

  // CPU M.2 SSD1
  {GPIO_VER2_LP_GPP_F21,  {GpioPadModeGpio, GpioHostOwnAcpi,    GpioDirOut,   GpioOutHigh,    GpioIntDis,              GpioPlatformReset,  GpioTermNone}},  //CPU SSD1 RESET

  // PCH M.2 SSD2
  {GPIO_VER2_LP_GPP_H0,   {GpioPadModeGpio, GpioHostOwnAcpi,    GpioDirOut,   GpioOutHigh,    GpioIntDis,              GpioPlatformReset,  GpioTermNone}},  // CPU SSD2 PWREN

  //TCH PAD
  {GPIO_VER2_LP_GPP_F18,  {GpioPadModeGpio, GpioHostOwnGpio,    GpioDirInInv, GpioOutDefault, GpioIntEdge|GpioIntApic, GpioPlatformReset,  GpioTermNone, GpioPadConfigUnlock}},  //TCH_PAD_INT_N

  // M.2 Key-E - WLAN/BT
  {GPIO_VER2_LP_GPP_A13,  {GpioPadModeGpio, GpioHostOwnDefault, GpioDirOut,   GpioOutHigh,    GpioIntDis,              GpioPlatformReset,  GpioTermNone, GpioPadConfigUnlock }}, // BT_RF_KILL_N
  {GPIO_VER2_LP_GPP_D15,  {GpioPadModeGpio, GpioHostOwnDefault, GpioDirOut,   GpioOutHigh,    GpioIntDis,              GpioPlatformReset,  GpioTermNone, GpioPadConfigUnlock }}, // WIFI_RF_KILL_N
  //{GPIO_VER2_LP_GPP_D13,  {GpioPadModeGpio, GpioHostOwnAcpi,    GpioDirInInv, GpioOutDefault, GpioIntLevel|GpioIntSci, GpioHostDeepReset,  GpioTermNone, GpioPadConfigUnlock }}, // WIFI_WAKE_N

  // Battery Charger Vmin to PCH PROCHOT, derived from ICL
  {GPIO_VER2_LP_GPP_B2,   {GpioPadModeGpio, GpioHostOwnAcpi,    GpioDirInInv, GpioOutDefault, GpioIntEdge|GpioIntSci,  GpioPlatformReset,  GpioTermNone, GpioPadConfigUnlock  }},  //BC_PROCHOT_N

  //DG02 PEG Slot
  {GPIO_VER2_LP_GPP_A15,  {GpioPadModeGpio, GpioHostOwnDefault, GpioDirOut,   GpioOutHigh,    GpioIntDefault,          GpioPlatformReset,  GpioTermNone}},  // GPU_PERST_N

  // EC
  {GPIO_VER2_LP_GPP_E7,   {GpioPadModeGpio, GpioHostOwnAcpi,    GpioDirInInv, GpioOutDefault, GpioIntLevel|GpioIntSmi, GpioPlatformReset,  GpioTermNone, GpioPadConfigUnlock  }},  //EC_SMI_N
  {GPIO_VER2_LP_GPP_F9,   {GpioPadModeGpio, GpioHostOwnAcpi,    GpioDirOut,   GpioOutHigh,    GpioIntDis,              GpioPlatformReset,  GpioTermNone}},  //EC_SLP_S0_CS_N

 {0x0}  // terminator
})}

#mGpioTableAdlPLp5Rvp
[PcdsDynamicExVpd.common.SkuIdAdlPLp5Rvp]
gBoardModuleTokenSpaceGuid.VpdPcdBoardGpioTable|*|{CODE({

  // CPU M.2 SSD1
  {GPIO_VER2_LP_GPP_D14, {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutHigh,   GpioIntDis,GpioPlatformReset,  GpioTermNone}},  // CPU SSD1 PWREN
  {GPIO_VER2_LP_GPP_F20, {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutHigh,   GpioIntDis,GpioPlatformReset,  GpioTermNone}},  //CPU SSD1 RESET

  // CPU M.2 SSD2
  {GPIO_VER2_LP_GPP_C2, {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutHigh,   GpioIntDis,GpioPlatformReset,  GpioTermNone}},  //CPU SSD2 PWREN
  {GPIO_VER2_LP_GPP_H1, {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutHigh,   GpioIntDis,GpioPlatformReset,  GpioTermNone}},  // CPU SSD2 RESET

  // M.2 Key-E - WLAN/BT
  {GPIO_VER2_LP_GPP_A13, {GpioPadModeGpio, GpioHostOwnDefault, GpioDirOut,    GpioOutHigh,    GpioIntDis,              GpioPlatformReset,  GpioTermNone, GpioPadConfigUnlock }}, // BT_RF_KILL_N
  {GPIO_VER2_LP_GPP_E3,  {GpioPadModeGpio, GpioHostOwnDefault, GpioDirOut,    GpioOutHigh,    GpioIntDis,              GpioPlatformReset,  GpioTermNone, GpioPadConfigUnlock }}, // WIFI_RF_KILL_N
  {GPIO_VER2_LP_GPP_H2,  {GpioPadModeGpio, GpioHostOwnAcpi,    GpioDirOut,    GpioOutHigh,    GpioIntDis,              GpioPlatformReset,  GpioTermNone, GpioPadConfigUnlock }}, // WLAN_RST_N
  {GPIO_VER2_LP_GPP_D13, {GpioPadModeGpio, GpioHostOwnAcpi,    GpioDirInInv,  GpioOutDefault, GpioIntLevel|GpioIntSci, GpioHostDeepReset,  GpioTermNone, GpioPadConfigUnlock }}, // WIFI_WAKE_N
  {GPIO_VER2_LP_GPP_E0,  {GpioPadModeGpio, GpioHostOwnAcpi,    GpioDirInInv,  GpioOutDefault, GpioIntLevel|GpioIntSci, GpioHostDeepReset,  GpioTermNone, GpioPadConfigUnlock }}, // UART_BT_WAKE_N

  // X4 Pcie Slot for Gen3 and Gen 4
  {GPIO_VER2_LP_GPP_H17,  {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutLow,   GpioIntDis,GpioPlatformReset,  GpioTermNone}},//ONBOARD_X4_PCIE_SLOT1_PWREN_N
  {GPIO_VER2_LP_GPP_F10, {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutHigh,   GpioIntDis,GpioPlatformReset,  GpioTermNone}},//ONBOARD_X4_PCIE_SLOT1_RESET_N
  // Shared with PEG_SLOT_WAKE. Available after rework.
  // {GPIO_VER2_LP_GPP_A20,  {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirInInv,  GpioOutDefault,GpioIntLevel|GpioIntSci,GpioHostDeepReset,  GpioTermNone,  GpioPadConfigUnlock  }},  //ONBOARD_X4_PCIE_SLOT1_WAKE_N

  //Shared pin with peg slot. Avilable after rework.
  // Battery Charger Vmin to PCH PROCHOT, derived from ICL
  //{GPIO_VER2_LP_GPP_B2,  {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirInInv,  GpioOutDefault,GpioIntEdge|GpioIntSci,GpioPlatformReset,  GpioTermNone,  GpioPadConfigUnlock  }},  //BC_PROCHOT_N

  // PCH M.2 SSD
  {GPIO_VER2_LP_GPP_D16, {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutHigh,   GpioIntDis,GpioPlatformReset,  GpioTermNone}},  //M2_PCH_SSD_PWREN
  {GPIO_VER2_LP_GPP_H0,  {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutHigh,   GpioIntDis,GpioPlatformReset,  GpioTermNone}},  //M2_SSD_RST_N

  // Camera
  {GPIO_VER2_LP_GPP_B23, {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutHigh,   GpioIntDis,GpioPlatformReset,  GpioTermNone}},  //CRD1_PWREN_CVF_IRQ - CAM1
  {GPIO_VER2_LP_GPP_R5, {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutHigh,   GpioIntDis,GpioPlatformReset,  GpioTermNone}},  //CRD1_RST_N_CVF_RST_N - CAM1
  {GPIO_VER2_LP_GPP_H15,  {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutHigh,   GpioIntDis,GpioPlatformReset,  GpioTermNone}},  //CRD1_CLK_E
  {GPIO_VER2_LP_GPP_E16, {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutLow,   GpioIntDis,GpioPlatformReset,  GpioTermNone}},  //CAM2_PWREN
  {GPIO_VER2_LP_GPP_E15, {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutHigh,   GpioIntDis,GpioPlatformReset,  GpioTermNone}},  //CAM2_RST_N


  // Camera Common GPIO's for all Camera, Rework Options
  {GPIO_VER2_LP_GPP_B18, {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutLow,   GpioIntDis,GpioPlatformReset,  GpioTermNone}},  //CRD_CAM_STROBE
  {GPIO_VER2_LP_GPP_B14, {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutDefault,   GpioIntDis,GpioPlatformReset,  GpioTermNone}},  //PRIVACY_LED

  // Audio
  {GPIO_VER2_LP_GPP_H3, {GpioPadModeGpio, GpioHostOwnGpio, GpioDirInInv,  GpioOutDefault,GpioIntLevel|GpioIntApic,GpioPlatformReset,  GpioTermNone,  GpioPadConfigUnlock  }},  // CODEC_INT_N

  // Touch Pad
  // Touch Pad and Touch Panel 2 share the same Power Enable, default is Touch pad
  {GPIO_VER2_LP_GPP_F7,  {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutHigh,   GpioIntDis,GpioPlatformReset,  GpioTermNone}},  //TCH_PAD_LS_EN - PWR_En
  {GPIO_VER2_LP_GPP_A15,  {GpioPadModeGpio, GpioHostOwnGpio, GpioDirInInv,  GpioOutDefault,GpioIntEdge|GpioIntApic,GpioPlatformReset,  GpioTermNone,  GpioPadConfigUnlock  }},  //TCH_PAD_INT_N
  {GPIO_VER2_LP_GPP_H1,  {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutHigh,GpioIntDis,GpioPlatformReset,  GpioTermNone  }},  //TCH_PAD_TCH_PNL2_LS_EN / AUDIO_PWREN

  // EC
  {GPIO_VER2_LP_GPP_E7,  {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirInInv,  GpioOutDefault,GpioIntLevel|GpioIntSmi,GpioPlatformReset,  GpioTermNone,  GpioPadConfigUnlock  }},  //EC_SMI_N
  {GPIO_VER2_LP_GPP_F9,  {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutHigh,   GpioIntDis,GpioPlatformReset,  GpioTermNone}},  //EC_SLP_S0_CS_N
  {GPIO_VER2_LP_GPP_B14,  {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutHigh,   GpioIntDis,GpioPlatformReset,  GpioTermNone}},  //Strap Only

  // SPI TPM
  {GPIO_VER2_LP_GPP_E3, {GpioPadModeGpio, GpioHostOwnGpio, GpioDirInInv,  GpioOutDefault,GpioIntLevel|GpioIntApic,GpioHostDeepReset,  GpioTermNone,  GpioPadConfigUnlock  }},  //SPI_TPM_INT_N

 {0x0}  // terminator
})}

#mGpioTableAdlPDdr5Rvp
[PcdsDynamicExVpd.common.SkuIdAdlPDdr5Rvp]
gBoardModuleTokenSpaceGuid.VpdPcdBoardGpioTable|*|{CODE({
  // CPU M.2 SSD1
  {GPIO_VER2_LP_GPP_D14, {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutHigh,   GpioIntDis,GpioPlatformReset,  GpioTermNone}},  // CPU SSD1 PWREN
  {GPIO_VER2_LP_GPP_F20, {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutHigh,   GpioIntDis,GpioPlatformReset,  GpioTermNone}},  //CPU SSD1 RESET

  // CPU M.2 SSD2
  {GPIO_VER2_LP_GPP_C2, {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutHigh,   GpioIntDis,GpioPlatformReset,  GpioTermNone}},  //CPU SSD2 PWREN
  {GPIO_VER2_LP_GPP_H1, {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutHigh,   GpioIntDis,GpioPlatformReset,  GpioTermNone}},  // CPU SSD2 RESET

  // M.2 Key-E - WLAN/BT
  {GPIO_VER2_LP_GPP_A13, {GpioPadModeGpio, GpioHostOwnDefault, GpioDirOut,    GpioOutHigh,    GpioIntDis,              GpioPlatformReset,  GpioTermNone, GpioPadConfigUnlock }}, // BT_RF_KILL_N
  {GPIO_VER2_LP_GPP_E3,  {GpioPadModeGpio, GpioHostOwnDefault, GpioDirOut,    GpioOutHigh,    GpioIntDis,              GpioPlatformReset,  GpioTermNone, GpioPadConfigUnlock }}, // WIFI_RF_KILL_N
  {GPIO_VER2_LP_GPP_H2,  {GpioPadModeGpio, GpioHostOwnAcpi,    GpioDirOut,    GpioOutHigh,    GpioIntDis,              GpioPlatformReset,  GpioTermNone, GpioPadConfigUnlock }}, // WLAN_RST_N
  {GPIO_VER2_LP_GPP_D13, {GpioPadModeGpio, GpioHostOwnAcpi,    GpioDirInInv,  GpioOutDefault, GpioIntLevel|GpioIntSci, GpioHostDeepReset,  GpioTermNone, GpioPadConfigUnlock }}, // WIFI_WAKE_N
  {GPIO_VER2_LP_GPP_E0,  {GpioPadModeGpio, GpioHostOwnAcpi,    GpioDirInInv,  GpioOutDefault, GpioIntLevel|GpioIntSci, GpioHostDeepReset,  GpioTermNone, GpioPadConfigUnlock }}, // UART_BT_WAKE_N

  // X4 Pcie Slot for Gen3 and Gen 4
  {GPIO_VER2_LP_GPP_H17,  {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutLow,   GpioIntDis,GpioPlatformReset,  GpioTermNone}},//ONBOARD_X4_PCIE_SLOT1_PWREN_N
  {GPIO_VER2_LP_GPP_F10, {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutHigh,   GpioIntDis,GpioPlatformReset,  GpioTermNone}},//ONBOARD_X4_PCIE_SLOT1_RESET_N
  //Shared with PEG_WAKE_SLOT. Available after rework.
  //{GPIO_VER2_LP_GPP_A20,  {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirInInv,  GpioOutDefault,GpioIntLevel|GpioIntSci,GpioHostDeepReset,  GpioTermNone,  GpioPadConfigUnlock  }},  //ONBOARD_X4_PCIE_SLOT1_WAKE_N
  {GPIO_VER2_LP_GPP_A22,  {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutLow,   GpioIntDis,GpioPlatformReset,  GpioTermNone}},//ONBOARD_X4_PCIE_SLOT1_PWREN_N_DDR5

  //Shared pin with peg slot. Avilable after rework.
  // Battery Charger Vmin to PCH PROCHOT, derived from ICL
  //{GPIO_VER2_LP_GPP_B2,  {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirInInv,  GpioOutDefault,GpioIntEdge|GpioIntSci,GpioPlatformReset,  GpioTermNone,  GpioPadConfigUnlock  }},  //BC_PROCHOT_N

  // PCH M.2 SSD
  {GPIO_VER2_LP_GPP_D16, {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutHigh,   GpioIntDis,GpioPlatformReset,  GpioTermNone}},  //M2_PCH_SSD_PWREN
  {GPIO_VER2_LP_GPP_H0,  {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutHigh,   GpioIntDis,GpioPlatformReset,  GpioTermNone}},  //M2_SSD_RST_N

  // Camera
  {GPIO_VER2_LP_GPP_B23, {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutHigh,   GpioIntDis,GpioPlatformReset,  GpioTermNone}},  //CRD1_PWREN_CVF_IRQ - CAM1
  {GPIO_VER2_LP_GPP_R5, {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutHigh,   GpioIntDis,GpioPlatformReset,  GpioTermNone}},  //CRD1_RST_N_CVF_RST_N - CAM1
  {GPIO_VER2_LP_GPP_H15,  {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutHigh,   GpioIntDis,GpioPlatformReset,  GpioTermNone}},  //CRD1_CLK_E
  {GPIO_VER2_LP_GPP_A21,  {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutHigh,   GpioIntDis,GpioPlatformReset,  GpioTermNone}},  //CRD1_CLK_E_DDR5
  {GPIO_VER2_LP_GPP_E16, {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutLow,   GpioIntDis,GpioPlatformReset,  GpioTermNone}},  //CAM2_PWREN
  {GPIO_VER2_LP_GPP_E15, {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutHigh,   GpioIntDis,GpioPlatformReset,  GpioTermNone}},  //CAM2_RST_N

  // Camera Common GPIO's for all Camera, Rework Options
  {GPIO_VER2_LP_GPP_B18, {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutLow,   GpioIntDis,GpioPlatformReset,  GpioTermNone}},  //CRD_CAM_STROBE
  {GPIO_VER2_LP_GPP_B14, {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutDefault,   GpioIntDis,GpioPlatformReset,  GpioTermNone}},  //PRIVACY_LED

  // Audio
  {GPIO_VER2_LP_GPP_H3, {GpioPadModeGpio, GpioHostOwnGpio, GpioDirInInv,  GpioOutDefault,GpioIntLevel|GpioIntApic,GpioPlatformReset,  GpioTermNone,  GpioPadConfigUnlock  }},  // CODEC_INT_N

  // Touch Pad
  // Touch Pad and Touch Panel 2 share the same Power Enable, default is Touch pad
  {GPIO_VER2_LP_GPP_F7,  {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutHigh,   GpioIntDis,GpioPlatformReset,  GpioTermNone}},  //TCH_PAD_LS_EN - PWR_En
  {GPIO_VER2_LP_GPP_A15,  {GpioPadModeGpio, GpioHostOwnGpio, GpioDirInInv,  GpioOutDefault,GpioIntEdge|GpioIntApic,GpioPlatformReset,  GpioTermNone,  GpioPadConfigUnlock  }},  //TCH_PAD_INT_N
  {GPIO_VER2_LP_GPP_H1,  {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutHigh,GpioIntDis,GpioPlatformReset,  GpioTermNone  }},  //TCH_PAD_TCH_PNL2_LS_EN / AUDIO_PWREN

  // EC
  {GPIO_VER2_LP_GPP_E7,  {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirInInv,  GpioOutDefault,GpioIntLevel|GpioIntSmi,GpioPlatformReset,  GpioTermNone,  GpioPadConfigUnlock  }},  //EC_SMI_N
  {GPIO_VER2_LP_GPP_F9,  {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutHigh,   GpioIntDis,GpioPlatformReset,  GpioTermNone}},  //EC_SLP_S0_CS_N

  //Shared pin with wlan pin. Avilable after rework.
  // SPI TPM
  //{GPIO_VER2_LP_GPP_E3, {GpioPadModeGpio, GpioHostOwnGpio, GpioDirInInv,  GpioOutDefault,GpioIntLevel|GpioIntApic,GpioHostDeepReset,  GpioTermNone,  GpioPadConfigUnlock  }},  //SPI_TPM_INT_N

 {0x0}  // terminator
})}


#mGpioTableAdlPDdr4Rvp
[PcdsDynamicExVpd.common.SkuIdAdlPDdr4Rvp]
gBoardModuleTokenSpaceGuid.VpdPcdBoardGpioTable|*|{CODE({
  // CPU M.2 SSD1
  {GPIO_VER2_LP_GPP_D14, {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutHigh,   GpioIntDis,GpioPlatformReset,  GpioTermNone}},  // CPU SSD1 PWREN
  {GPIO_VER2_LP_GPP_F20, {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutHigh,   GpioIntDis,GpioPlatformReset,  GpioTermNone}},  //CPU SSD1 RESET

  // CPU M.2 SSD2
  {GPIO_VER2_LP_GPP_C2, {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutHigh,   GpioIntDis,GpioPlatformReset,  GpioTermNone}},  //CPU SSD2 PWREN
  {GPIO_VER2_LP_GPP_H1, {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutHigh,   GpioIntDis,GpioPlatformReset,  GpioTermNone}},  // CPU SSD2 RESET

  // M.2 Key-E - WLAN/BT
  {GPIO_VER2_LP_GPP_A13, {GpioPadModeGpio, GpioHostOwnDefault, GpioDirOut,    GpioOutHigh,    GpioIntDis,              GpioPlatformReset,  GpioTermNone, GpioPadConfigUnlock }}, // BT_RF_KILL_N
  {GPIO_VER2_LP_GPP_E3,  {GpioPadModeGpio, GpioHostOwnDefault, GpioDirOut,    GpioOutHigh,    GpioIntDis,              GpioPlatformReset,  GpioTermNone, GpioPadConfigUnlock }}, // WIFI_RF_KILL_N
  {GPIO_VER2_LP_GPP_H2,  {GpioPadModeGpio, GpioHostOwnAcpi,    GpioDirOut,    GpioOutHigh,    GpioIntDis,              GpioPlatformReset,  GpioTermNone, GpioPadConfigUnlock }}, // WLAN_RST_N
  {GPIO_VER2_LP_GPP_D13, {GpioPadModeGpio, GpioHostOwnAcpi,    GpioDirInInv,  GpioOutDefault, GpioIntLevel|GpioIntSci, GpioHostDeepReset,  GpioTermNone, GpioPadConfigUnlock }}, // WIFI_WAKE_N
  {GPIO_VER2_LP_GPP_E0,  {GpioPadModeGpio, GpioHostOwnAcpi,    GpioDirInInv,  GpioOutDefault, GpioIntLevel|GpioIntSci, GpioHostDeepReset,  GpioTermNone, GpioPadConfigUnlock }}, // UART_BT_WAKE_N

  // X4 Pcie Slot for Gen3 and Gen 4
  {GPIO_VER2_LP_GPP_A22,  {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutLow,   GpioIntDis,GpioPlatformReset,  GpioTermNone}},//ONBOARD_X4_PCIE_SLOT1_PWREN_N
  {GPIO_VER2_LP_GPP_F10, {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutHigh,   GpioIntDis,GpioPlatformReset,  GpioTermNone}},//ONBOARD_X4_PCIE_SLOT1_RESET_N

  //Shared pin with peg slot. Avilable after rework.
  // Battery Charger Vmin to PCH PROCHOT, derived from ICL
  //{GPIO_VER2_LP_GPP_B2,  {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirInInv,  GpioOutDefault,GpioIntEdge|GpioIntSci,GpioPlatformReset,  GpioTermNone,  GpioPadConfigUnlock  }},  //BC_PROCHOT_N

  // PCH M.2 SSD
  {GPIO_VER2_LP_GPP_D16, {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutHigh,   GpioIntDis,GpioPlatformReset,  GpioTermNone}},  //M2_PCH_SSD_PWREN
  {GPIO_VER2_LP_GPP_H0,  {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutHigh,   GpioIntDis,GpioPlatformReset,  GpioTermNone}},  //M2_SSD_RST_N

  // Camera
  {GPIO_VER2_LP_GPP_B23, {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutHigh,   GpioIntDis,GpioPlatformReset,  GpioTermNone}},  //CRD1_PWREN_CVF_IRQ - CAM1
  {GPIO_VER2_LP_GPP_R5, {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutHigh,   GpioIntDis,GpioPlatformReset,  GpioTermNone}},  //CRD1_RST_N_CVF_RST_N - CAM1
  {GPIO_VER2_LP_GPP_A21,  {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutHigh,   GpioIntDis,GpioPlatformReset,  GpioTermNone}},  //CRD1_CLK_E
  {GPIO_VER2_LP_GPP_E16, {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutLow,   GpioIntDis,GpioPlatformReset,  GpioTermNone}},  //CAM2_PWREN
  {GPIO_VER2_LP_GPP_E15, {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutHigh,   GpioIntDis,GpioPlatformReset,  GpioTermNone}},  //CAM2_RST_N

  // Camera Common GPIO's for all Camera, Rework Options
  {GPIO_VER2_LP_GPP_B18, {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutLow,   GpioIntDis,GpioPlatformReset,  GpioTermNone}},  //CRD_CAM_STROBE
  {GPIO_VER2_LP_GPP_B14, {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutDefault,   GpioIntDis,GpioPlatformReset,  GpioTermNone}},  //PRIVACY_LED

  // Audio
  {GPIO_VER2_LP_GPP_H3, {GpioPadModeGpio, GpioHostOwnGpio, GpioDirInInv,  GpioOutDefault,GpioIntLevel|GpioIntApic,GpioPlatformReset,  GpioTermNone,  GpioPadConfigUnlock  }},  // CODEC_INT_N

  // Touch Pad
  // Touch Pad and Touch Panel 2 share the same Power Enable, default is Touch pad
  {GPIO_VER2_LP_GPP_F7,  {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutHigh,   GpioIntDis,GpioPlatformReset,  GpioTermNone}},  //TCH_PAD_LS_EN - PWR_En
  {GPIO_VER2_LP_GPP_A17,  {GpioPadModeGpio, GpioHostOwnGpio, GpioDirInInv,  GpioOutDefault,GpioIntEdge|GpioIntApic,GpioPlatformReset,  GpioTermNone,  GpioPadConfigUnlock  }},  //TCH_PAD_INT_N
  {GPIO_VER2_LP_GPP_H1,  {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutHigh,GpioIntDis,GpioPlatformReset,  GpioTermNone  }},  //TCH_PAD_TCH_PNL2_LS_EN / AUDIO_PWREN

  // EC
  {GPIO_VER2_LP_GPP_E7,  {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirInInv,  GpioOutDefault,GpioIntLevel|GpioIntSmi,GpioPlatformReset,  GpioTermNone,  GpioPadConfigUnlock  }},  //EC_SMI_N
  {GPIO_VER2_LP_GPP_F9,  {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutHigh,   GpioIntDis,GpioPlatformReset,  GpioTermNone}},  //EC_SLP_S0_CS_N

  //Shared pin with wlan pin. Avilable after rework.
  // SPI TPM
  //{GPIO_VER2_LP_GPP_E3, {GpioPadModeGpio, GpioHostOwnGpio, GpioDirInInv,  GpioOutDefault,GpioIntLevel|GpioIntApic,GpioHostDeepReset,  GpioTermNone,  GpioPadConfigUnlock  }},  //SPI_TPM_INT_N

 {0x0}  // terminator
})}

# mGpioTableAdlPDdr5Dg384Aep
[PcdsDynamicExVpd.common.SkuIdAdlPDdr5Dg384Aep]
gBoardModuleTokenSpaceGuid.VpdPcdBoardGpioTable|*|{CODE({
  // CPU M.2 SSD2
  {GPIO_VER2_LP_GPP_F21, {GpioPadModeGpio, GpioHostOwnGpio,    GpioDirOut,   GpioOutHigh,    GpioIntDis,              GpioPlatformReset, GpioTermNone}},  //CPU SSD2 EXT_PWR_GATE2B

  // PCH M.2 SSD
  {GPIO_VER2_LP_GPP_H0,  {GpioPadModeGpio, GpioHostOwnGpio,    GpioDirOut,   GpioOutHigh,    GpioIntDis,              GpioPlatformReset, GpioTermNone}},  //CPU SSD RESET

  // M.2 Key-E - WLAN/BT
  {GPIO_VER2_LP_GPP_A13, {GpioPadModeGpio, GpioHostOwnDefault, GpioDirOut,   GpioOutHigh,    GpioIntDis,              GpioPlatformReset, GpioTermNone,   GpioPadConfigUnlock}},  // BT_RF_KILL_N
  {GPIO_VER2_LP_GPP_D15, {GpioPadModeGpio, GpioHostOwnDefault, GpioDirOut,   GpioOutHigh,    GpioIntDis,              GpioPlatformReset, GpioTermNone,   GpioPadConfigUnlock}},  // WIFI_RF_KILL_N
  //{GPIO_VER2_LP_GPP_D13, {GpioPadModeGpio, GpioHostOwnAcpi,    GpioDirInInv, GpioOutDefault, GpioIntLevel|GpioIntSci, GpioHostDeepReset, GpioTermNone,   GpioPadConfigUnlock}},  // WIFI_WAKE_N

  // Battery Charger Vmin to PCH PROCHOT, derived from ICL
  //{GPIO_VER2_LP_GPP_B2,  {GpioPadModeGpio, GpioHostOwnAcpi,    GpioDirInInv, GpioOutDefault, GpioIntEdge|GpioIntSci,  GpioPlatformReset, GpioTermNone,   GpioPadConfigUnlock}},  //BC_PROCHOT_N

  // Camera
  {GPIO_VER2_LP_GPP_B23, {GpioPadModeGpio, GpioHostOwnGpio,    GpioDirIn,    GpioOutDefault, GpioIntDis,              GpioPlatformReset, GpioTermNone}},  //CVF_HOST_IRQ_N
  {GPIO_VER2_LP_GPP_R5,  {GpioPadModeGpio, GpioHostOwnGpio,    GpioDirOut,   GpioOutHigh,    GpioIntDis,              GpioPlatformReset, GpioTermNone}},  //CVF_RST_N
  {GPIO_VER2_LP_GPP_H15, {GpioPadModeGpio, GpioHostOwnGpio,    GpioDirOut,   GpioOutHigh,    GpioIntDis,              GpioPlatformReset, GpioTermNone}},  //CAM_BRD_RST_N

  // IRLED
  {GPIO_VER2_LP_GPP_B18, {GpioPadModeGpio, GpioHostOwnGpio,    GpioDirOut,   GpioOutHigh,     GpioIntDis,             GpioPlatformReset, GpioTermNone}},  //UF_CAM_STROBE

  //TCH PAD
  {GPIO_VER2_LP_GPP_E12, {GpioPadModeGpio, GpioHostOwnGpio,    GpioDirInInv, GpioOutDefault, GpioIntEdge|GpioIntApic, GpioPlatformReset, GpioTermNone,   GpioPadConfigUnlock}},  //TCH_PAD_INT_N

  // EC
  {GPIO_VER2_LP_GPP_E7,  {GpioPadModeGpio, GpioHostOwnAcpi,    GpioDirInInv, GpioOutDefault, GpioIntLevel|GpioIntSmi, GpioPlatformReset, GpioTermNone,   GpioPadConfigUnlock}},  //EC_SMI_N
  {GPIO_VER2_LP_GPP_F9,  {GpioPadModeGpio, GpioHostOwnGpio,    GpioDirOut,   GpioOutHigh,    GpioIntDis,              GpioPlatformReset, GpioTermNone}},  //EC_SLP_S0_CS_N

  // Audio WoV
  {GPIO_VER2_LP_GPP_A7,  {GpioPadModeGpio, GpioHostOwnGpio,    GpioDirOut,   GpioOutHigh,    GpioIntDis,              GpioPlatformReset, GpioTermNone}},  // Audio_WoV_LED

  // SPI TPM
  {GPIO_VER2_LP_GPP_E3,  {GpioPadModeGpio, GpioHostOwnGpio,    GpioDirInInv, GpioOutDefault, GpioIntLevel|GpioIntApic, GpioHostDeepReset, GpioTermWpu20K, GpioPadConfigUnlock}},  //SPI_TPM_INT_N

  // SD Controller
  {GPIO_VER2_LP_GPP_A8,  {GpioPadModeGpio, GpioHostOwnAcpi,    GpioDirIn,    GpioOutDefault, GpioIntLevel|GpioIntSci, GpioHostDeepReset, GpioTermNone,   GpioPadConfigUnlock}},  //SD_WAKE_N
  {GPIO_VER2_LP_GPP_F10, {GpioPadModeGpio, GpioHostOwnGpio,    GpioDirOut,   GpioOutHigh,    GpioIntDis,              GpioPlatformReset, GpioTermNone}},  //SD_PCIE_RST_N

  //RTD3 GPIO header for PEG Slot
  {GPIO_VER2_LP_GPP_A15, {GpioPadModeGpio, GpioHostOwnDefault, GpioDirOut,   GpioOutHigh,    GpioIntDefault,          GpioPlatformReset, GpioTermNone}},  // GPU_PERST_N

  {0x0} // terminator
})}

[PcdsDynamicExVpd.common.SkuIdAdlPLp5Aep]
gBoardModuleTokenSpaceGuid.VpdPcdBoardGpioTable|*|{CODE({

  // CPU M.2 SSD1
  {GPIO_VER2_LP_GPP_F20, {GpioPadModeGpio,  GpioHostOwnGpio,  GpioDirOut,    GpioOutHigh,    GpioIntDis,                GpioPlatformReset,  GpioTermNone  }},  //CPU SSD1 RESET

  // M.2 Key-E - WLAN/BT
  {GPIO_VER2_LP_GPP_A13, {GpioPadModeGpio,  GpioHostOwnGpio,  GpioDirOut,    GpioOutHigh,    GpioIntDis,                GpioPlatformReset,  GpioTermNone  }},  //BT_RF_KILL_N
  {GPIO_VER2_LP_GPP_F14, {GpioPadModeGpio,  GpioHostOwnGpio,  GpioDirOut,    GpioOutHigh,    GpioIntDis,                GpioPlatformReset,  GpioTermNone  }},  //WIFI_RF_KILL_N

  // Battery Charger Vmin to PCH PROCHOT, derived from ICL
  {GPIO_VER2_LP_GPP_B2,  {GpioPadModeGpio,  GpioHostOwnAcpi,  GpioDirInInv,  GpioOutDefault, GpioIntEdge|GpioIntSci,    GpioPlatformReset,  GpioTermNone, GpioPadConfigUnlock  }},  //BC_PROCHOT_N

  // Camera
  {GPIO_VER2_LP_GPP_B23, {GpioPadModeGpio,  GpioHostOwnGpio,  GpioDirIn,     GpioOutDefault, GpioIntDis,                GpioPlatformReset,  GpioTermNone  }},  //CRD1_PWREN_CVF_IRQ - CAM1
  {GPIO_VER2_LP_GPP_R5,  {GpioPadModeGpio,  GpioHostOwnGpio,  GpioDirOut,    GpioOutHigh,    GpioIntDis,                GpioPlatformReset,  GpioTermNone  }},  //CRD1_RST_N_CVF_RST_N - CAM1
  {GPIO_VER2_LP_GPP_H15, {GpioPadModeGpio,  GpioHostOwnGpio,  GpioDirOut,    GpioOutHigh,    GpioIntDis,                GpioPlatformReset,  GpioTermNone  }},  //CRD1_CLK_E

  // Camera Common GPIO's for all Camera, Rework Options
  {GPIO_VER2_LP_GPP_B18, {GpioPadModeGpio,  GpioHostOwnGpio,  GpioDirOut,    GpioOutLow,     GpioIntDis,                GpioPlatformReset,  GpioTermNone  }},  //CRD_CAM_STROBE

  // Audio
  {GPIO_VER2_LP_GPP_H3,  {GpioPadModeGpio,  GpioHostOwnGpio,  GpioDirInInv,  GpioOutDefault, GpioIntLevel|GpioIntApic,  GpioPlatformReset,  GpioTermNone, GpioPadConfigUnlock  }},  // CODEC_INT_N

  // Touch Pad
  {GPIO_VER2_LP_GPP_A15, {GpioPadModeGpio,  GpioHostOwnGpio,  GpioDirInInv,  GpioOutDefault, GpioIntEdge|GpioIntApic,   GpioPlatformReset,  GpioTermNone, GpioPadConfigUnlock  }},  //TCH_PAD_INT_N

  // EC
  {GPIO_VER2_LP_GPP_E7,  {GpioPadModeGpio,  GpioHostOwnAcpi,  GpioDirInInv,  GpioOutDefault, GpioIntLevel|GpioIntSmi,   GpioPlatformReset,  GpioTermNone, GpioPadConfigUnlock  }},  //EC_SMI_N
  {GPIO_VER2_LP_GPP_F9,  {GpioPadModeGpio,  GpioHostOwnGpio,  GpioDirOut,    GpioOutHigh,    GpioIntDis,                GpioPlatformReset,  GpioTermNone  }},  //EC_SLP_S0_CS_N

  {0x0}

})}

# mGpioTableAdlPLp5Dg128Aep
[PcdsDynamicExVpd.common.SkuIdAdlPLp5Dg128Aep]
gBoardModuleTokenSpaceGuid.VpdPcdBoardGpioTable|*|{CODE({
  // CPU M.2 SSD2
  {GPIO_VER2_LP_GPP_F21, {GpioPadModeGpio, GpioHostOwnGpio,    GpioDirOut,   GpioOutHigh,    GpioIntDis,              GpioPlatformReset, GpioTermNone}},  //CPU SSD2 EXT_PWR_GATE2B

  // M.2 Key-E - WLAN/BT
  {GPIO_VER2_LP_GPP_A13, {GpioPadModeGpio, GpioHostOwnGpio,    GpioDirOut,   GpioOutHigh,    GpioIntDis,              GpioPlatformReset, GpioTermNone,   GpioPadConfigUnlock}}, //BT_RF_KILL_N
  {GPIO_VER2_LP_GPP_D15, {GpioPadModeGpio, GpioHostOwnGpio,    GpioDirOut,   GpioOutHigh,    GpioIntDis,              GpioPlatformReset, GpioTermNone,   GpioPadConfigUnlock}}, // WIFI_RF_KILL_N
  //{GPIO_VER2_LP_GPP_D13, {GpioPadModeGpio, GpioHostOwnAcpi,    GpioDirInInv, GpioOutDefault, GpioIntLevel|GpioIntSci, GpioHostDeepReset, GpioTermNone,   GpioPadConfigUnlock}}, // WIFI_WAKE_N

  // Battery Charger Vmin to PCH PROCHOT, derived from ICL
  //{GPIO_VER2_LP_GPP_B2,  {GpioPadModeGpio, GpioHostOwnAcpi,    GpioDirInInv, GpioOutDefault, GpioIntEdge|GpioIntSci,  GpioPlatformReset, GpioTermNone,   GpioPadConfigUnlock}},  //BC_PROCHOT_N

  // Camera
  {GPIO_VER2_LP_GPP_A7, {GpioPadModeGpio, GpioHostOwnGpio,    GpioDirIn,    GpioOutDefault, GpioIntDis,              GpioPlatformReset, GpioTermNone}}, //CRD1_PWREN_CVF_IRQ - CAM1
  {GPIO_VER2_LP_GPP_R5,  {GpioPadModeGpio, GpioHostOwnGpio,    GpioDirOut,   GpioOutHigh,    GpioIntDis,              GpioPlatformReset, GpioTermNone}},  //CVF_RST_N
  {GPIO_VER2_LP_GPP_H15, {GpioPadModeGpio, GpioHostOwnGpio,    GpioDirOut,   GpioOutHigh,    GpioIntDis,              GpioPlatformReset, GpioTermNone}},  //CAM_BRD_RST_N

  // IRLED
  {GPIO_VER2_LP_GPP_B18, {GpioPadModeGpio, GpioHostOwnGpio,    GpioDirOut,   GpioOutHigh,    GpioIntDis,              GpioPlatformReset, GpioTermNone}},  //UF_CAM_STROBE

  //TCH PAD
  {GPIO_VER2_LP_GPP_E12, {GpioPadModeGpio, GpioHostOwnGpio,    GpioDirInInv, GpioOutDefault, GpioIntEdge|GpioIntApic, GpioPlatformReset, GpioTermNone,   GpioPadConfigUnlock}},  //TCH_PAD_INT_N

  // I2C_TCH_IO_SEL
  {GPIO_VER2_LP_GPP_D14, {GpioPadModeGpio, GpioHostOwnGpio,    GpioDirOut,   GpioOutLow,     GpioIntDis,              GpioPlatformReset, GpioTermNone}},  //CVF_Host_Wake

  // EC
  {GPIO_VER2_LP_GPP_E7,  {GpioPadModeGpio, GpioHostOwnAcpi,    GpioDirInInv, GpioOutDefault, GpioIntLevel|GpioIntSmi, GpioPlatformReset, GpioTermNone,   GpioPadConfigUnlock}},  //EC_SMI_N
  {GPIO_VER2_LP_GPP_F9,  {GpioPadModeGpio, GpioHostOwnGpio,    GpioDirOut,   GpioOutHigh,    GpioIntDis,              GpioPlatformReset, GpioTermNone}},  //EC_SLP_S0_CS_N

  // Audio WoV
  //{GPIO_VER2_LP_GPP_A7,  {GpioPadModeGpio, GpioHostOwnGpio,    GpioDirOut,   GpioOutHigh,    GpioIntDis,              GpioPlatformReset, GpioTermNone}},  // Audio_WoV_LED

  // SPI TPM
  {GPIO_VER2_LP_GPP_E3,  {GpioPadModeGpio, GpioHostOwnGpio,    GpioDirInInv, GpioOutDefault, GpioIntLevel|GpioIntApic, GpioHostDeepReset, GpioTermWpu20K, GpioPadConfigUnlock}},  //SPI_TPM_INT_N

  //RTD3 GPIO header for PEG Slot
  {GPIO_VER2_LP_GPP_A15, {GpioPadModeGpio, GpioHostOwnDefault, GpioDirOut,   GpioOutHigh,    GpioIntDefault,          GpioPlatformReset, GpioTermNone}},  // GPU_PERST_N

  {0x0} // terminator
})}

[PcdsDynamicExVpd.common.SkuIdAdlPLp5Gcs]
gBoardModuleTokenSpaceGuid.VpdPcdBoardGpioTable|*|{CODE({

  // CPU M.2 SSD1
  {GPIO_VER2_LP_GPP_F20, {GpioPadModeGpio, GpioHostOwnAcpi,  GpioDirOut,   GpioOutHigh,    GpioIntDis,              GpioPlatformReset, GpioTermNone  }},  //CPU SSD RESET

  // Touch Pad
  {GPIO_VER2_LP_GPP_A15, {GpioPadModeGpio, GpioHostOwnGpio,  GpioDirInInv, GpioOutDefault, GpioIntEdge|GpioIntApic, GpioPlatformReset, GpioTermNone, GpioPadConfigUnlock }},  //TCH_PAD_INT_N

  // M.2 Key-E - WLAN/BT
  {GPIO_VER2_LP_GPP_A13, {GpioPadModeGpio, GpioHostOwnGpio,  GpioDirOut,   GpioOutHigh,    GpioIntDis,              GpioPlatformReset, GpioTermNone  }},                      // BT_RF_KILL_N
  {GPIO_VER2_LP_GPP_A19, {GpioPadModeGpio, GpioHostOwnAcpi,  GpioDirInInv, GpioOutDefault, GpioIntLevel|GpioIntSci, GpioPlatformReset, GpioTermNone, GpioPadConfigUnlock }},  // WIFI_WAKE_N
  {GPIO_VER2_LP_GPP_E3,  {GpioPadModeGpio, GpioHostOwnGpio,  GpioDirOut,   GpioOutHigh,    GpioIntDis,              GpioPlatformReset, GpioTermNone  }},                      // WIFI_RF_KILL_N

  // EC
  {GPIO_VER2_LP_GPP_B14, {GpioPadModeGpio, GpioHostOwnAcpi,  GpioDirOut,   GpioOutHigh,    GpioIntEdge|GpioIntSci,  GpioPlatformReset, GpioTermNone, GpioPadConfigUnlock }},  //CVF_Host_Wake
  {GPIO_VER2_LP_GPP_E7,  {GpioPadModeGpio, GpioHostOwnAcpi,  GpioDirInInv, GpioOutDefault, GpioIntLevel|GpioIntSmi, GpioPlatformReset, GpioTermNone, GpioPadConfigUnlock }},  //EC_SMI_N
  {GPIO_VER2_LP_GPP_F9,  {GpioPadModeGpio, GpioHostOwnGpio,  GpioDirOut,   GpioOutHigh,    GpioIntDis,              GpioPlatformReset, GpioTermNone  }},  //EC_SLP_S0_CS_N

  // CAM Conn
  {GPIO_VER2_LP_GPP_B18, {GpioPadModeGpio, GpioHostOwnGpio,  GpioDirOut,   GpioOutLow,     GpioIntDis,              GpioPlatformReset, GpioTermNone  }},  //CRD_CAM_STROBE
  {GPIO_VER2_LP_GPP_H15, {GpioPadModeGpio, GpioHostOwnGpio,  GpioDirOut,   GpioOutHigh,    GpioIntDis,              GpioPlatformReset, GpioTermNone  }},  //CRD1_CLK_E

  // SPI TPM
  {GPIO_VER2_LP_GPP_F14, {GpioPadModeGpio, GpioHostOwnGpio,  GpioDirOut,   GpioOutHigh,    GpioIntDis,              GpioPlatformReset, GpioTermNone  }},  //SPI_TPM_INT_N
  {GPIO_VER2_LP_GPP_F7,  {GpioPadModeGpio, GpioHostOwnAcpi,  GpioDirOut,   GpioOutHigh,    GpioIntDis,              GpioPlatformReset, GpioTermNone  }},  //TCH_PAD_LS_EN - PWR_En

  {0x0}
})}

#mGpioTableAdlPDdr5MRRvp
[PcdsDynamicExVpd.common.SkuIdAdlPDdr5MRRvp]
gBoardModuleTokenSpaceGuid.VpdPcdBoardGpioTable|*|{CODE({
  // CPU M.2 SSD1
  {GPIO_VER2_LP_GPP_D14, {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutHigh,   GpioIntDis,GpioPlatformReset,  GpioTermNone}},  // CPU SSD1 PWREN
  {GPIO_VER2_LP_GPP_F20, {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutHigh,   GpioIntDis,GpioPlatformReset,  GpioTermNone}},  //CPU SSD1 RESET

  // CPU M.2 SSD2
  {GPIO_VER2_LP_GPP_C2, {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutHigh,   GpioIntDis,GpioPlatformReset,  GpioTermNone}},  //CPU SSD2 PWREN
  {GPIO_VER2_LP_GPP_H1, {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutHigh,   GpioIntDis,GpioPlatformReset,  GpioTermNone}},  // CPU SSD2 RESET

  // M.2 Key-E - WLAN/BT
  {GPIO_VER2_LP_GPP_A13, {GpioPadModeGpio, GpioHostOwnDefault, GpioDirOut,    GpioOutHigh,    GpioIntDis,              GpioPlatformReset,  GpioTermNone, GpioPadConfigUnlock }}, // BT_RF_KILL_N
  {GPIO_VER2_LP_GPP_E3, {GpioPadModeGpio,  GpioHostOwnDefault, GpioDirOut,    GpioOutHigh,    GpioIntDis,              GpioPlatformReset,  GpioTermNone, GpioPadConfigUnlock }}, // WIFI_RF_KILL_N
  //{GPIO_VER2_LP_GPP_H2, {GpioPadModeGpio,  GpioHostOwnAcpi,    GpioDirOut,    GpioOutHigh,    GpioIntDis,              GpioPlatformReset,  GpioTermNone, GpioPadConfigUnlock }}, // WLAN_RST_N
  //{GPIO_VER2_LP_GPP_D13, {GpioPadModeGpio, GpioHostOwnAcpi,    GpioDirInInv,  GpioOutDefault, GpioIntLevel|GpioIntSci, GpioHostDeepReset,  GpioTermNone, GpioPadConfigUnlock }}, // WIFI_WAKE_N
  {GPIO_VER2_LP_GPP_E0, {GpioPadModeGpio,  GpioHostOwnAcpi,    GpioDirInInv,  GpioOutDefault, GpioIntLevel|GpioIntSci, GpioHostDeepReset,  GpioTermNone, GpioPadConfigUnlock }}, // UART_BT_WAKE_N

  // Camera
  {GPIO_VER2_LP_GPP_B23, {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutHigh,   GpioIntDis,GpioPlatformReset,  GpioTermNone}},  //CRD1_PWREN_CVF_IRQ - CAM1
  {GPIO_VER2_LP_GPP_R5, {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutHigh,   GpioIntDis,GpioPlatformReset,  GpioTermNone}},  //CRD1_RST_N_CVF_RST_N - CAM1
  {GPIO_VER2_LP_GPP_A21,  {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutHigh,   GpioIntDis,GpioPlatformReset,  GpioTermNone}},  //CRD1_CLK_E
  {GPIO_VER2_LP_GPP_E16, {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutLow,   GpioIntDis,GpioPlatformReset,  GpioTermNone}},  //CAM2_PWREN
  {GPIO_VER2_LP_GPP_E15, {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutHigh,   GpioIntDis,GpioPlatformReset,  GpioTermNone}},  //CAM2_RST_N

  // Camera Common GPIO's for all Camera, Rework Options
  {GPIO_VER2_LP_GPP_B18, {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutLow,   GpioIntDis,GpioPlatformReset,  GpioTermNone}},  //CRD_CAM_STROBE
  {GPIO_VER2_LP_GPP_B14, {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutDefault,   GpioIntDis,GpioPlatformReset,  GpioTermNone}},  //PRIVACY_LED

  // Audio
  {GPIO_VER2_LP_GPP_H3, {GpioPadModeGpio, GpioHostOwnGpio, GpioDirInInv,  GpioOutDefault,GpioIntLevel|GpioIntApic,GpioPlatformReset,  GpioTermNone,  GpioPadConfigUnlock  }},  // CODEC_INT_N

  // EC
  {GPIO_VER2_LP_GPP_E7,  {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirInInv,  GpioOutDefault,GpioIntLevel|GpioIntSmi,GpioPlatformReset,  GpioTermNone,  GpioPadConfigUnlock  }},  //EC_SMI_N
  {GPIO_VER2_LP_GPP_F9,  {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutHigh,   GpioIntDis,GpioPlatformReset,  GpioTermNone}},  //EC_SLP_S0_CS_N

  //Shared pin with wlan pin. Avilable after rework.
  // SPI TPM
  //{GPIO_VER2_LP_GPP_E3, {GpioPadModeGpio, GpioHostOwnGpio, GpioDirInInv,  GpioOutDefault,GpioIntLevel|GpioIntApic,GpioHostDeepReset,  GpioTermNone,  GpioPadConfigUnlock  }},  //SPI_TPM_INT_N

  // DNX/Type C Bias
  {GPIO_VER2_LP_GPP_E23, {GpioPadModeGpio,GpioHostOwnGpio,GpioDirOut,GpioOutHigh,   GpioIntDis, GpioPlatformReset,  GpioTermNone}},  //DISP_AUX_N_BIAS_GPIO

  // DP MUX Control
  {GPIO_VER2_LP_GPP_A7, {GpioPadModeGpio,GpioHostOwnGpio,GpioDirOut,GpioOutLow,   GpioIntDis, GpioPlatformReset,  GpioTermNone}},  //DISP_AUX_N_BIAS_GPIO

  // Thunderbolt
// Moving to EarlyPremem  {GPIO_VER2_LP_GPP_A12,    {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutHigh,     GpioIntDefault,          GpioHostDeepReset,     GpioTermNone}},//MR1 RTD3 enable
// Moving to EarlyPremem  {GPIO_VER2_LP_GPP_E4,     {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutHigh,     GpioIntDefault,          GpioHostDeepReset,     GpioTermNone}},//MR2 RTD3 enable
  {GPIO_VER2_LP_GPP_H15,    {GpioPadModeGpio, GpioHostOwnGpio, GpioDirInInv,   GpioOutDefault,  GpioIntLevel|GpioIntSci, GpioHostDeepReset,   GpioTermNone,  GpioPadConfigUnlock}},//MR1_PE_WAKE_N
  {GPIO_VER2_LP_GPP_H17,    {GpioPadModeGpio, GpioHostOwnGpio, GpioDirInInv,   GpioOutDefault,  GpioIntLevel|GpioIntSci, GpioHostDeepReset,   GpioTermNone,  GpioPadConfigUnlock}},//MR2_PE_WAKE_N
// Moving to Premem  {GPIO_VER2_LP_GPP_D9,     {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutHigh,      GpioIntDefault,          GpioPlatformReset,   GpioTermNone}},//MR1_PCH_PERST_N
// Moving to Premem  {GPIO_VER2_LP_GPP_E18,    {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutHigh,      GpioIntDefault,          GpioPlatformReset,   GpioTermNone}},//MR2_PCH_PERST_N
  {GPIO_VER2_LP_GPP_D12,    {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutLow,      GpioIntDefault,          GpioPlatformReset,   GpioTermNone}},//MR_FRC_PWR
  // Unused GPIO  {GPIO_VER2_LP_GPP_D11,    {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutHigh,     GpioIntDefault,          GpioPlatformReset,  GpioTermNone}},//MR USB Force Power
  // Unused GPIO  {GPIO_VER2_LP_GPP_E5,     {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutHigh,     GpioIntDefault,          GpioPlatformReset,  GpioTermNone}},//MR1_PCH GPIO_1
  // Unused GPIO  {GPIO_VER2_LP_GPP_E21,    {GpioPadModeGpio, GpioHostOwnGpio, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//MR1_PCH GPIO_0
  {GPIO_VER2_LP_GPP_E19,    {GpioPadModeGpio, GpioHostOwnGpio, GpioDirIn,      GpioOutDefault,  GpioIntDis,              GpioHostDeepReset,   GpioTermNone, GpioPadConfigUnlock}},//MR2_CIO_PLUG_Event_N
  {GPIO_VER2_LP_GPP_D10,    {GpioPadModeGpio, GpioHostOwnGpio, GpioDirIn,      GpioOutDefault,  GpioIntDis,              GpioHostDeepReset,   GpioTermNone, GpioPadConfigUnlock}},//MR1_CIO_PLUG_Event_N
  // Unused GPIO  {GPIO_VER2_LP_GPP_H0,     {GpioPadModeGpio, GpioHostOwnGpio, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//MR2_GPIO0

 {0x0}  // terminator
})}

# mGpioTableAdlPLp5MbAep
[PcdsDynamicExVpd.common.SkuIdAdlPLp5MbAep]
gBoardModuleTokenSpaceGuid.VpdPcdBoardGpioTable|*|{CODE({
  // CPU M.2 SSD1
  {GPIO_VER2_LP_GPP_F20, {GpioPadModeGpio, GpioHostOwnGpio,    GpioDirOut,   GpioOutHigh,    GpioIntDis,              GpioPlatformReset, GpioTermNone}},  //CPU SSD EXT_PWR_GATEB

  // CPU M.2 SSD2
  {GPIO_VER2_LP_GPP_F21, {GpioPadModeGpio, GpioHostOwnGpio,    GpioDirOut,   GpioOutHigh,    GpioIntDis,              GpioPlatformReset, GpioTermNone}},  //CPU SSD2 EXT_PWR_GATE2B

  // M.2 Key-E - WLAN/BT
  {GPIO_VER2_LP_GPP_A13, {GpioPadModeGpio, GpioHostOwnGpio,    GpioDirOut,   GpioOutHigh,    GpioIntDis,              GpioPlatformReset, GpioTermNone,   GpioPadConfigUnlock}}, //BT_RF_KILL_N
  {GPIO_VER2_LP_GPP_D15, {GpioPadModeGpio, GpioHostOwnGpio,    GpioDirOut,   GpioOutHigh,    GpioIntDis,              GpioPlatformReset, GpioTermNone,   GpioPadConfigUnlock}}, //WIFI_RF_KILL_N

  // Battery Charger Vmin to PCH PROCHOT, derived from ICL
  //{GPIO_VER2_LP_GPP_B2,  {GpioPadModeGpio, GpioHostOwnAcpi,    GpioDirInInv, GpioOutDefault, GpioIntEdge|GpioIntSci,  GpioPlatformReset, GpioTermNone,   GpioPadConfigUnlock}},  //BC_PROCHOT_N

  // Camera
  {GPIO_VER2_LP_GPP_B23, {GpioPadModeGpio, GpioHostOwnGpio,    GpioDirIn,    GpioOutDefault, GpioIntDis,              GpioPlatformReset, GpioTermNone}},  //CRD1_PWREN_CVF_IRQ - CAM1
  {GPIO_VER2_LP_GPP_R5,  {GpioPadModeGpio, GpioHostOwnGpio,    GpioDirOut,   GpioOutHigh,    GpioIntDis,              GpioPlatformReset, GpioTermNone}},  //CVF_RST_N
  {GPIO_VER2_LP_GPP_H15, {GpioPadModeGpio, GpioHostOwnGpio,    GpioDirOut,   GpioOutHigh,    GpioIntDis,              GpioPlatformReset, GpioTermNone}},  //CAM_BRD_RST_N

  //UF_CAM_STROBE
  {GPIO_VER2_LP_GPP_B18, {GpioPadModeGpio, GpioHostOwnGpio,    GpioDirOut,   GpioOutHigh,    GpioIntDis,              GpioPlatformReset, GpioTermNone}},  //UF_CAM_STROBE

  //TCH PAD
  {GPIO_VER2_LP_GPP_A15, {GpioPadModeGpio, GpioHostOwnGpio,    GpioDirInInv, GpioOutDefault, GpioIntEdge|GpioIntApic, GpioPlatformReset, GpioTermNone,   GpioPadConfigUnlock}},  //TCH_PAD_INT_N

  // EC
  {GPIO_VER2_LP_GPP_E7,  {GpioPadModeGpio, GpioHostOwnAcpi,    GpioDirInInv, GpioOutDefault, GpioIntLevel|GpioIntSmi, GpioPlatformReset, GpioTermNone,   GpioPadConfigUnlock}},  //EC_SMI_N
  {GPIO_VER2_LP_GPP_F9,  {GpioPadModeGpio, GpioHostOwnGpio,    GpioDirOut,   GpioOutHigh,    GpioIntDis,              GpioPlatformReset, GpioTermNone}},  //EC_SLP_S0_CS_N

  // Audio WoV
  {GPIO_VER2_LP_GPP_A7,  {GpioPadModeGpio, GpioHostOwnGpio,    GpioDirOut,   GpioOutHigh,    GpioIntDis,              GpioPlatformReset, GpioTermNone}},  // Audio_WoV_LED

  // SPI TPM
  {GPIO_VER2_LP_GPP_E3,  {GpioPadModeGpio, GpioHostOwnGpio,    GpioDirInInv, GpioOutDefault, GpioIntLevel|GpioIntApic, GpioHostDeepReset, GpioTermWpu20K, GpioPadConfigUnlock}},  //SPI_TPM_INT_N

  {0x0} // terminator
})}

#mGpioTableAdlPT3Lp5Rvp
[PcdsDynamicExVpd.common.SkuIdAdlPT3Lp5Rvp]
gBoardModuleTokenSpaceGuid.VpdPcdBoardGpioTable|*|{CODE({

  // CPU M.2 SSD1
  {GPIO_VER2_LP_GPP_D14, {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutHigh,   GpioIntDis,GpioPlatformReset,  GpioTermNone}},  // CPU SSD1 PWREN
  {GPIO_VER2_LP_GPP_F20, {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutHigh,   GpioIntDis,GpioPlatformReset,  GpioTermNone}},  //CPU SSD1 RESET

  // CPU M.2 SSD2
  {GPIO_VER2_LP_GPP_C2, {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutHigh,   GpioIntDis,GpioPlatformReset,  GpioTermNone}},  //CPU SSD2 PWREN
  {GPIO_VER2_LP_GPP_H1, {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutHigh,   GpioIntDis,GpioPlatformReset,  GpioTermNone}},  // CPU SSD2 RESET

  // M.2 Key-E - WLAN/BT
  {GPIO_VER2_LP_GPP_A13, {GpioPadModeGpio, GpioHostOwnDefault, GpioDirOut,    GpioOutHigh,    GpioIntDis,              GpioPlatformReset,  GpioTermNone, GpioPadConfigUnlock }}, // BT_RF_KILL_N
  {GPIO_VER2_LP_GPP_E3,  {GpioPadModeGpio, GpioHostOwnDefault, GpioDirOut,    GpioOutHigh,    GpioIntDis,              GpioPlatformReset,  GpioTermNone, GpioPadConfigUnlock }}, // WIFI_RF_KILL_N
  {GPIO_VER2_LP_GPP_H2,  {GpioPadModeGpio, GpioHostOwnAcpi,    GpioDirOut,    GpioOutHigh,    GpioIntDis,              GpioPlatformReset,  GpioTermNone, GpioPadConfigUnlock }}, // WLAN_RST_N
  {GPIO_VER2_LP_GPP_D13, {GpioPadModeGpio, GpioHostOwnAcpi,    GpioDirInInv,  GpioOutDefault, GpioIntLevel|GpioIntSci, GpioHostDeepReset,  GpioTermNone, GpioPadConfigUnlock }}, // WIFI_WAKE_N
  {GPIO_VER2_LP_GPP_E0,  {GpioPadModeGpio, GpioHostOwnAcpi,    GpioDirInInv,  GpioOutDefault, GpioIntLevel|GpioIntSci, GpioHostDeepReset,  GpioTermNone, GpioPadConfigUnlock }}, // UART_BT_WAKE_N

  // X4 Pcie Slot for Gen3 and Gen 4
  {GPIO_VER2_LP_GPP_H17,  {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutLow,   GpioIntDis,GpioPlatformReset,  GpioTermNone}},//ONBOARD_X4_PCIE_SLOT1_PWREN_N
  {GPIO_VER2_LP_GPP_F10, {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutHigh,   GpioIntDis,GpioPlatformReset,  GpioTermNone}},//ONBOARD_X4_PCIE_SLOT1_RESET_N
  // Shared with PEG_SLOT_WAKE. Available after rework.
  // {GPIO_VER2_LP_GPP_A20,  {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirInInv,  GpioOutDefault,GpioIntLevel|GpioIntSci,GpioHostDeepReset,  GpioTermNone,  GpioPadConfigUnlock  }},  //ONBOARD_X4_PCIE_SLOT1_WAKE_N

  //Shared pin with peg slot. Avilable after rework.
  // Battery Charger Vmin to PCH PROCHOT, derived from ICL
  //{GPIO_VER2_LP_GPP_B2,  {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirInInv,  GpioOutDefault,GpioIntEdge|GpioIntSci,GpioPlatformReset,  GpioTermNone,  GpioPadConfigUnlock  }},  //BC_PROCHOT_N

  // PCH M.2 SSD
  {GPIO_VER2_LP_GPP_D16, {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutHigh,   GpioIntDis,GpioPlatformReset,  GpioTermNone}},  //M2_PCH_SSD_PWREN
  {GPIO_VER2_LP_GPP_H0,  {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutHigh,   GpioIntDis,GpioPlatformReset,  GpioTermNone}},  //M2_SSD_RST_N

  // Camera
  {GPIO_VER2_LP_GPP_B23, {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutHigh,   GpioIntDis,GpioPlatformReset,  GpioTermNone}},  //CRD1_PWREN_CVF_IRQ - CAM1
  {GPIO_VER2_LP_GPP_R5, {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutHigh,   GpioIntDis,GpioPlatformReset,  GpioTermNone}},  //CRD1_RST_N_CVF_RST_N - CAM1
  {GPIO_VER2_LP_GPP_H15,  {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutHigh,   GpioIntDis,GpioPlatformReset,  GpioTermNone}},  //CRD1_CLK_E
  {GPIO_VER2_LP_GPP_E16, {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutLow,   GpioIntDis,GpioPlatformReset,  GpioTermNone}},  //CAM2_PWREN
  {GPIO_VER2_LP_GPP_E15, {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutHigh,   GpioIntDis,GpioPlatformReset,  GpioTermNone}},  //CAM2_RST_N


  // Camera Common GPIO's for all Camera, Rework Options
  {GPIO_VER2_LP_GPP_B18, {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutLow,   GpioIntDis,GpioPlatformReset,  GpioTermNone}},  //CRD_CAM_STROBE
  {GPIO_VER2_LP_GPP_B14, {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutDefault,   GpioIntDis,GpioPlatformReset,  GpioTermNone}},  //PRIVACY_LED

  // Audio
  {GPIO_VER2_LP_GPP_H3, {GpioPadModeGpio, GpioHostOwnGpio, GpioDirInInv,  GpioOutDefault,GpioIntLevel|GpioIntApic,GpioPlatformReset,  GpioTermNone,  GpioPadConfigUnlock  }},  // CODEC_INT_N

  // Touch Pad
  // Touch Pad and Touch Panel 2 share the same Power Enable, default is Touch pad
  {GPIO_VER2_LP_GPP_F7,  {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutHigh,   GpioIntDis,GpioPlatformReset,  GpioTermNone}},  //TCH_PAD_LS_EN - PWR_En
  {GPIO_VER2_LP_GPP_A15,  {GpioPadModeGpio, GpioHostOwnGpio, GpioDirInInv,  GpioOutDefault,GpioIntEdge|GpioIntApic,GpioPlatformReset,  GpioTermNone,  GpioPadConfigUnlock  }},  //TCH_PAD_INT_N
  {GPIO_VER2_LP_GPP_H1,  {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutHigh,GpioIntDis,GpioPlatformReset,  GpioTermNone  }},  //TCH_PAD_TCH_PNL2_LS_EN / AUDIO_PWREN

  // DNX/Type C Bias
  {GPIO_VER2_LP_GPP_D11, {GpioPadModeGpio,GpioHostOwnGpio,GpioDirOut,GpioOutHigh,   GpioIntDis, GpioPlatformReset,  GpioTermNone}},  //DISP_AUX_P_BIAS_GPIO - Port 3
  {GPIO_VER2_LP_GPP_D12, {GpioPadModeGpio,GpioHostOwnGpio,GpioDirOut,GpioOutLow,    GpioIntDis, GpioPlatformReset,  GpioTermNone}},  //DISP_AUX_N_BIAS_GPIO - Port 3
  {GPIO_VER2_LP_GPP_E20, {GpioPadModeGpio,GpioHostOwnGpio,GpioDirOut,GpioOutHigh,   GpioIntDis, GpioPlatformReset,  GpioTermNone}},  //DISP_AUX_P_BIAS_GPIO - Port 1
  {GPIO_VER2_LP_GPP_E21, {GpioPadModeGpio,GpioHostOwnGpio,GpioDirOut,GpioOutLow,    GpioIntDis, GpioPlatformReset,  GpioTermNone}},  //DISP_AUX_N_BIAS_GPIO - Port 1
  {GPIO_VER2_LP_GPP_E22, {GpioPadModeGpio,GpioHostOwnGpio,GpioDirOut,GpioOutHigh,   GpioIntDis, GpioPlatformReset,  GpioTermNone}},  //DISP_AUX_P_BIAS_GPIO - Port 2
  {GPIO_VER2_LP_GPP_E23, {GpioPadModeGpio,GpioHostOwnGpio,GpioDirOut,GpioOutLow,    GpioIntDis, GpioPlatformReset,  GpioTermNone}},  //DISP_AUX_N_BIAS_GPIO - Port 2


  // EC
  {GPIO_VER2_LP_GPP_E7,  {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirInInv,  GpioOutDefault,GpioIntLevel|GpioIntSmi,GpioPlatformReset,  GpioTermNone,  GpioPadConfigUnlock  }},  //EC_SMI_N
  {GPIO_VER2_LP_GPP_F9,  {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutHigh,   GpioIntDis,GpioPlatformReset,  GpioTermNone}},  //EC_SLP_S0_CS_N

  // SPI TPM
  {GPIO_VER2_LP_GPP_E3, {GpioPadModeGpio, GpioHostOwnGpio, GpioDirInInv,  GpioOutDefault,GpioIntLevel|GpioIntApic,GpioHostDeepReset,  GpioTermNone,  GpioPadConfigUnlock  }},  //SPI_TPM_INT_N

  {0x0}  // terminator
})}
