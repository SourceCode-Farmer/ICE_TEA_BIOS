## @file
#  ADL P SPD DATA configuration file.
#
# @copyright
#  INTEL CONFIDENTIAL
#  Copyright 2020 - 2021 Intel Corporation.
#
#  The source code contained or described herein and all documents related to the
#  source code ("Material") are owned by Intel Corporation or its suppliers or
#  licensors. Title to the Material remains with Intel Corporation or its suppliers
#  and licensors. The Material may contain trade secrets and proprietary and
#  confidential information of Intel Corporation and its suppliers and licensors,
#  and is protected by worldwide copyright and trade secret laws and treaty
#  provisions. No part of the Material may be used, copied, reproduced, modified,
#  published, uploaded, posted, transmitted, distributed, or disclosed in any way
#  without Intel's prior express written permission.
#
#  No license under any patent, copyright, trade secret or other intellectual
#  property right is granted to or conferred upon you by disclosure or delivery
#  of the Materials, either expressly, by implication, inducement, estoppel or
#  otherwise. Any license under such intellectual property rights must be
#  express and approved by Intel in writing.
#
#  Unless otherwise agreed by Intel in writing, you may not remove or alter
#  this notice or any other notice embedded in Materials by Intel or
#  Intel's suppliers or licensors in any way.
#
#  This file contains a 'Sample Driver' and is licensed as such under the terms
#  of your license agreement with Intel or your vendor. This file may be modified
#  by the user, subject to the additional terms of the license agreement.
#
# @par Specification
#

[PcdsDynamicExVpd.common.SkuIdAdlPLp4Rvp]
gBoardModuleTokenSpaceGuid.VpdPcdMrcSpdData|*|{CODE(
{
// LPDDR4X 200b 8Gb die, 4 die, 2 Channels per package
// Micron MT53E1G32D4NQ-046
// 4267
  1,
 {0x23,                                 ///< 0 384 SPD bytes used, 512 total
  0x11,                                 ///< 1 SPD Revision 1.1
  0x11,                                 ///< 2 DRAM Type: LPDDR4X SDRAM
  0x0E,                                 ///< 3 Module Type: Not Hybrid (DRAM only) / Non-DIMM Solution (on-board DRAM)
  0x15,                                 ///< 4 8 Banks, no bank groups, 8 Gb SDRAM density
  0x21,                                 ///< 5 16 Rows, 10 Columns
  0xB5,                                 ///< 6 Non-Monolithic DRAM Device, 4 die, 2 Channels per package, Signal Loading Matrix 1
  0x08,                                 ///< 7 SDRAM Optional Features: tMAW = 8192 * tREFI, Unlimited MAC
  0x00,                                 ///< 8 SDRAM Thermal / Refresh options: Reserved
  0x40,                                 ///< 9 Other SDRAM Optional Features: Post package repair supported, one row per bank group, Soft PPR not supported
  0x00,                                 ///< 10 Reserved
  0x00,                                 ///< 11 Module Nominal Voltage: Reserved
  0x0A,                                 ///< 12 Module Organization: 2 Ranks, x16 Device Width per Channel
  0x01,                                 ///< 13 Module Memory Bus width: 1 System Channel, 16 bits channel width, no ECC
  0x00,                                 ///< 14 Module Thermal Sensor: none
  0x00,                                 ///< 15 Extended Module Type: Reserved
  0x48,                                 ///< 16 Signal Loading: Data/Strobe/Mask: 2 loads, CAC: 2 loads, CS: 1 load
  0x00,                                 ///< 17 MTB = 0.125ns, FTB = 1 ps
  0x04,                                 ///< 18  tCKAVGmin = 0.469ns (LPDDR4-4264)
  0xFF,                                 ///< 19  tCKAVGmax = 32.002 ns
  0x92,                                 ///< 20  CAS Latencies supported (First Byte) : 14, 10, 6
  0x55,                                 ///< 21  CAS Latencies supported (Second Byte): 28, 24, 20, 16
  0x00,                                 ///< 22  CAS Latencies supported (Third Byte) :
  0x00,                                 ///< 23 CAS Latencies supported (Fourth Byte):
  0x8C,                                 ///< 24  Minimum CAS Latency (tAAmin) = 17.5 ns
  0x00,                                 ///< 25 Read and Write Latency Set options: Write Latency Set A and DBI-Read Disabled
  0x90,                                 ///< 26 Minimum RAS-to-CAS delay (tRCDmin) = 18 ns
  0xA8,                                 ///< 27 Row precharge time for all banks (tRPab) = 21 ns
  0x90,                                 ///< 28 Minimum row precharge time (tRPmin) = 18 ns
  0xC0,                                 ///< 29 tRFCab = 280 ns (8 Gb dual-channel die)
  0x08,                                 ///< 30 tRFCab MSB
  0x60,                                 ///< 31 tRFCpb = 140 ns (8 Gb dual-channel die)
  0x04,                                 ///< 32 tRFCpb MSB
  0, 0, 0, 0, 0, 0, 0,                  ///< 33 - 39
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 40 - 49
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 50 - 59
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 60 - 69
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 70 - 79
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 80 - 89
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 90 - 99
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 100 - 109
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 110 - 119
  0x00,                                 ///< 120 FTB for Row precharge time per bank (tRPpb) = 18 ns
  0x00,                                 ///< 121 FTB for Row precharge time for all banks (tRPab) = 21 ns
  0x00,                                 ///< 122 FTB for Minimum RAS-to-CAS delay (tRCDmin) = 18 ns
  0x00,                                 ///< 123 FTB for tAAmin = 17.5 ns
  0x7F,                                 ///< 124 FTB for tCKAVGmax = 32.002 ns
  0xE1,                                 ///< 125 FTB for tCKAVGmin = 0.469 ns (LPDDR4-4264)
  0x00,                                 ///< 126 CRC A
  0x00,                                 ///< 127 CRC B
  0, 0,                                 ///< 128 - 129
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 130 - 139
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 140 - 149
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 150 - 159
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 160 - 169
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 170 - 179
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 180 - 189
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 190 - 199
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 200 - 209
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 210 - 219
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 220 - 229
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 230 - 239
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 240 - 249
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 250 - 259
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 260 - 269
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 270 - 279
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 280 - 289
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 290 - 299
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 300 - 309
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 310 - 319
  0x00,                                 ///< 320 Module Manufacturer ID Code, Least Significant Byte
  0x00,                                 ///< 321 Module Manufacturer ID Code, Most Significant Byte
  0x00,                                 ///< 322 Module Manufacturing Location
  0x00,                                 ///< 323 Module Manufacturing Date Year
  0x00,                                 ///< 324 Module Manufacturing Date Week
  0x55,                                 ///< 325 Module Serial Number A
  0x00,                                 ///< 326 Module Serial Number B
  0x00,                                 ///< 327 Module Serial Number C
  0x00,                                 ///< 328 Module Serial Number D
  0x20, 0x20, 0x20, 0x20, 0x20,         ///< 329 - 333 Module Part Number: Unused bytes coded as ASCII Blanks (0x20)
  0x20, 0x20, 0x20, 0x20, 0x20,         ///< 334 - 338 Module Part Number
  0x20, 0x20, 0x20, 0x20, 0x20,         ///< 339 - 343 Module Part Number
  0x20, 0x20, 0x20, 0x20, 0x20,         ///< 344 - 348 Module Part Number
  0x00,                                 ///< 349 Module Revision Code
  0x00,                                 ///< 350 DRAM Manufacturer ID Code, Least Significant Byte
  0x00,                                 ///< 351 DRAM Manufacturer ID Code, Most Significant Byte
  0x00,                                 ///< 352 DRAM Stepping
  0, 0, 0, 0, 0, 0, 0,                  ///< 353 - 359
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 360 - 369
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 370 - 379
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 380 - 389
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 390 - 399
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 400 - 409
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 410 - 419
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 420 - 429
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 430 - 439
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 440 - 449
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 450 - 459
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 460 - 469
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 470 - 479
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 480 - 489
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 490 - 499
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 500 - 509
  0, 0                                  ///< 510 - 511
}})}
[PcdsDynamicExVpd.common.SkuIdAdlPDdr5Rvp]
gBoardModuleTokenSpaceGuid.VpdPcdMrcSpdData|*|{CODE(
{
// DDR5 1Rx16 - 4800 MHz
  1,
 {0x30,                                      ///< 0   1024 SPD bytes total
  0x08,                                      ///< 1   SPD Revision 0.8
  0x12,                                      ///< 2   DRAM Type: DDR5 SDRAM
  0x03,                                      ///< 3   Module Type: Not Hybrid (DRAM only) / SO-DIMM Solution
  0x04,                                      ///< 4   Monolithic SDRAM, 16 Gb SDRAM density
  0x00,                                      ///< 5   16 Rows, 10 Columns
  0x40,                                      ///< 6   x16 SDRAM I/O Width
  0x42,                                      ///< 7   4 Bank Groups, 4 Banks per Bank Group
  0x00,                                      ///< 8   Secondary SDRAM Density and Package
  0x00,                                      ///< 9   Secondary SDRAM Addressing
  0x00,                                      ///< 10  Secondary SDRAM I/O Width
  0x00,                                      ///< 11  Secondary BankGroups and Banks per Bank Group
  0x60,                                      ///< 12  PPR Supported, One row per bank group, Soft PPR Supported
  0x00,                                      ///< 13  Commercial Temperature Grade, 0 to 85 C
  0x00,                                      ///< 14  Reserved
  0x00,                                      ///< 15  Reserved
  0x00,                                      ///< 16  SDRAM Nominal Voltage VDD:  1.1V
  0x00,                                      ///< 17  SDRAM Nominal Voltage VDDQ: 1.1V
  0x00,                                      ///< 18  SDRAM Nominal Voltage VPP:  1.8V
  0x00,                                      ///< 19  Reserved
  0xA1,                                      ///< 20  tCKAVGmin LSB
  0x01,                                      ///< 21  tCKAVGmin MSB
  0xE8,                                      ///< 22  tCKAVGmax LSB
  0x03,                                      ///< 23  tCKAVGmax MSB
  0x72,                                      ///< 24  CAS Latencies supported (First Byte) : 32, 30, 28, 22
  0x15,                                      ///< 25  CAS Latencies supported (Second Byte): 44, 40, 36
  0x00,                                      ///< 26  CAS Latencies supported (Third Byte) :
  0x00,                                      ///< 27  CAS Latencies supported (Fourth Byte):
  0x00,                                      ///< 28  CAS Latencies supported (Fifth Byte) :
  0x00,                                      ///< 29  Reserved
  0x1E,                                      ///< 30  Minimum CAS Latency (tAAmin) LSB
  0x41,                                      ///< 31  Minimum CAS Latency (tAAmin) MSB
  0x1E,                                      ///< 32  Minimum RAS-to-CAS delay (tRCDmin) LSB
  0x41,                                      ///< 33  Minimum RAS-to-CAS delay (tRCDmin) MSB
  0x1E,                                      ///< 34  Minimum Row Precharge delay (tRPmin) LSB
  0x41,                                      ///< 35  Minimum Row Precharge delay (tRPmin) MSB
  0x00,                                      ///< 36  Minimum Active to Precharge delay (tRASmin) LSB
  0x7D,                                      ///< 37  Minimum Active to Precharge delay (tRASmin) MSB
  0x1E,                                      ///< 38  Minimum Active to Active/Refresh delay (tRCmin) LSB
  0xBE,                                      ///< 39  Minimum Active to Active/Refresh delay (tRCmin) MSB
  0x30,                                      ///< 40  Minimum Write Recovery time (tWRmin) LSB
  0x75,                                      ///< 41  Minimum Write Recovery time (tWRmin) MSB
  0x27,                                      ///< 42  Refresh Recovery Delay (tRFC1min) LSB
  0x01,                                      ///< 43  Refresh Recovery Delay (tRFC1min) MSB
  0xA0,                                      ///< 44  Refresh Recovery Delay (tRFC2min) MSB
  0x00,                                      ///< 45  Refresh Recovery Delay (tRFC2min) MSB
  0x82,                                      ///< 46  Refresh Recovery Delay (tRFCsbmin) MSB
  0x00,                                      ///< 47  Refresh Recovery Delay (tRFCsbmin) MSB
  0,  0,                                     ///< 48 - 49
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,              ///< 50 - 59
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,              ///< 60 - 69
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,              ///< 70 - 79
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,              ///< 80 - 89
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,              ///< 90 - 99
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,              ///< 100 - 109
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,              ///< 110 - 119
  0, 0, 0, 0, 0, 0,                          ///< 120 - 125
  0x47,                                      ///< 126 CRC Bytes 0 - 127 LSB
  0xAE,                                      ///< 127 CRC Bytes 0 - 127 MSB
  0, 0,                                      ///< 128 - 129
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,              ///< 130 - 139
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,              ///< 140 - 149
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,              ///< 150 - 159
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,              ///< 160 - 169
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,              ///< 170 - 179
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,              ///< 180 - 189
  0, 0,                                      ///< 190 - 191
  0x08,                                      ///< 192 SPD Revision for Module Information: 0.8
  0x00,                                      ///< 193 Reserved
  0xC2,                                      ///< 194 SPD Manufacturer ID First Byte
  0xC4,                                      ///< 195 SPD Manufacturer ID Second Byte
  0x80,                                      ///< 196 SPD Device Type
  0x00,                                      ///< 197 SPD Device Revision
  0x80,                                      ///< 198 PMIC0 Manufacturer ID First Byte
  0xB3,                                      ///< 199 PMIC0 Manufacturer ID Second Byte
  0x80,                                      ///< 200 PMIC0 Device Type
  0x11,                                      ///< 201 PMIC0 Device Revision
  0, 0, 0, 0,                                ///< 202 - 205 PMIC1
  0, 0, 0, 0,                                ///< 206 - 209 PMIC2
  0x80,                                      ///< 210 Thermal Sensors Manufacturer ID First Byte
  0xB3,                                      ///< 211 Thermal Sensors Manufacturer ID First Byte
  0x80,                                      ///< 212 Thermal Sensors Device Type
  0x11,                                      ///< 213 Thermal Sensors Device Revision
  0, 0, 0, 0, 0, 0,                          ///< 214 - 219
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,              ///< 220 - 229
  0x0F,                                      ///< 230 Module Nominal Height
  0x10,                                      ///< 231 Module Nominal Thickness
  0x00,                                      ///< 232 Reference Raw Card Used
  0x01,                                      ///< 233 1 Row of DRAM on Module
  0x01,                                      ///< 234 1 Rank, 8 bits SDRAM data width per channel
  0x22,                                      ///< 235 2 Channels per DIMM, 32 bits per Channel
  0, 0, 0, 0,                                ///< 236 - 239
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,              ///< 240 - 249
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,              ///< 250 - 259
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,              ///< 260 - 269
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,              ///< 270 - 279
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,              ///< 280 - 289
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,              ///< 290 - 299
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,              ///< 300 - 309
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,              ///< 310 - 319
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,              ///< 320 - 329
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,              ///< 330 - 339
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,              ///< 340 - 349
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,              ///< 350 - 359
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,              ///< 360 - 369
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,              ///< 370 - 379
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,              ///< 380 - 389
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,              ///< 390 - 399
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,              ///< 400 - 409
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,              ///< 410 - 419
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,              ///< 420 - 429
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,              ///< 430 - 439
  0x00,  0x00,  0x00,  0x00,  0x00,  0x00,   ///< 440 - 445
  0x9C,                                      ///< 446 CRC for Bytes 128 - 253 LSB
  0xAD,                                      ///< 447 CRC for Bytes 128 - 253 MSB
  0, 0,                                      ///< 448 - 449
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,              ///< 450 - 459
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,              ///< 460 - 469
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,              ///< 470 - 479
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,              ///< 480 - 489
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,              ///< 490 - 499
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,              ///< 500 - 509
  0, 0                                       ///< 510 - 511
        ///< Ignore bytes 512-1023, @todo_adl: support 1024 bytes SPD array
}})}

[PcdsDynamicExVpd.common.SkuIdAdlPDdr5MRRvp]
gBoardModuleTokenSpaceGuid.VpdPcdMrcSpdData|*|{CODE(
{
// DDR5 1Rx8 - 4800 MHz
  1,
 {0x30,                                                                ///< 0   1024 SPD bytes total
  0x08,                                                                ///< 1   SPD Revision 0.8
  0x12,                                                                ///< 2   DRAM Type: DDR5 SDRAM
  0x03,                                                                ///< 3   Module Type: Not Hybrid (DRAM only) / SO-DIMM Solution
  0x04,                                                                ///< 4   Monolithic SDRAM, 16 Gb SDRAM density
  0x00,                                                                ///< 5   16 Rows, 10 Columns
  0x20,                                                                ///< 6   x8 SDRAM I/O Width
  0x62,                                                                ///< 7   8 Bank Groups, 4 Banks per Bank Group
  0x00,                                                                ///< 8   Secondary SDRAM Density and Package
  0x00,                                                                ///< 9   Secondary SDRAM Addressing
  0x00,                                                                ///< 10  Secondary SDRAM I/O Width
  0x00,                                                                ///< 11  Secondary BankGroups and Banks per Bank Group
  0x60,                                                                ///< 12  PPR Supported, One row per bank group, Soft PPR Supported
  0x00,                                                                ///< 13  Commercial Temperature Grade, 0 to 85 C
  0x00,                                                                ///< 14  Reserved
  0x00,                                                                ///< 15  Reserved
  0x00,                                                                ///< 16  SDRAM Nominal Voltage VDD:  1.1V
  0x00,                                                                ///< 17  SDRAM Nominal Voltage VDDQ: 1.1V
  0x00,                                                                ///< 18  SDRAM Nominal Voltage VPP:  1.8V
  0x00,                                                                ///< 19  Reserved
  0xA1,                                                                ///< 20  tCKAVGmin LSB
  0x01,                                                                ///< 21  tCKAVGmin MSB
  0xE8,                                                                ///< 22  tCKAVGmax LSB
  0x03,                                                                ///< 23  tCKAVGmax MSB
  0x72,                                                                ///< 24  CAS Latencies supported (First Byte) : 32, 30, 28, 22
  0x15,                                                                ///< 25  CAS Latencies supported (Second Byte): 44, 40, 36
  0x00,                                                                ///< 26  CAS Latencies supported (Third Byte) :
  0x00,                                                                ///< 27  CAS Latencies supported (Fourth Byte):
  0x00,                                                                ///< 28  CAS Latencies supported (Fifth Byte) :
  0x00,                                                                ///< 29  Reserved
  0x1E,                                                                ///< 30  Minimum CAS Latency (tAAmin) LSB
  0x41,                                                                ///< 31  Minimum CAS Latency (tAAmin) MSB
  0x1E,                                                                ///< 32  Minimum RAS-to-CAS delay (tRCDmin) LSB
  0x41,                                                                ///< 33  Minimum RAS-to-CAS delay (tRCDmin) MSB
  0x1E,                                                                ///< 34  Minimum Row Precharge delay (tRPmin) LSB
  0x41,                                                                ///< 35  Minimum Row Precharge delay (tRPmin) MSB
  0x00,                                                                ///< 36  Minimum Active to Precharge delay (tRASmin) LSB
  0x7D,                                                                ///< 37  Minimum Active to Precharge delay (tRASmin) MSB
  0x1E,                                                                ///< 38  Minimum Active to Active/Refresh delay (tRCmin) LSB
  0xBE,                                                                ///< 39  Minimum Active to Active/Refresh delay (tRCmin) MSB
  0x30,                                                                ///< 40  Minimum Write Recovery time (tWRmin) LSB
  0x75,                                                                ///< 41  Minimum Write Recovery time (tWRmin) MSB
  0x27,                                                                ///< 42  Refresh Recovery Delay (tRFC1min) LSB
  0x01,                                                                ///< 43  Refresh Recovery Delay (tRFC1min) MSB
  0xA0,                                                                ///< 44  Refresh Recovery Delay (tRFC2min) MSB
  0x00,                                                                ///< 45  Refresh Recovery Delay (tRFC2min) MSB
  0x82,                                                                ///< 46  Refresh Recovery Delay (tRFCsbmin) MSB
  0x00,                                                                ///< 47  Refresh Recovery Delay (tRFCsbmin) MSB
  0,  0,                                                               ///< 48 - 49
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,                                        ///< 50 - 59
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,                                        ///< 60 - 69
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,                                        ///< 70 - 79
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,                                        ///< 80 - 89
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,                                        ///< 90 - 99
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,                                        ///< 100 - 109
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,                                        ///< 110 - 119
  0, 0, 0, 0, 0, 0,                                                    ///< 120 - 125
  0x47,                                                                ///< 126 CRC Bytes 0 - 127 LSB
  0xAE,                                                                ///< 127 CRC Bytes 0 - 127 MSB
  0, 0,                                                                ///< 128 - 129
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,                                        ///< 130 - 139
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,                                        ///< 140 - 149
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,                                        ///< 150 - 159
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,                                        ///< 160 - 169
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,                                        ///< 170 - 179
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,                                        ///< 180 - 189
  0, 0,                                                                ///< 190 - 191
  0x08,                                                                ///< 192 SPD Revision for Module Information: 0.8
  0x00,                                                                ///< 193 Reserved
  0xC2,                                                                ///< 194 SPD Manufacturer ID First Byte
  0xC4,                                                                ///< 195 SPD Manufacturer ID Second Byte
  0x80,                                                                ///< 196 SPD Device Type
  0x00,                                                                ///< 197 SPD Device Revision
  0x80,                                                                ///< 198 PMIC0 Manufacturer ID First Byte
  0xB3,                                                                ///< 199 PMIC0 Manufacturer ID Second Byte
  0x80,                                                                ///< 200 PMIC0 Device Type
  0x11,                                                                ///< 201 PMIC0 Device Revision
  0, 0, 0, 0,                                                          ///< 202 - 205 PMIC1
  0, 0, 0, 0,                                                          ///< 206 - 209 PMIC2
  0x80,                                                                ///< 210 Thermal Sensors Manufacturer ID First Byte
  0xB3,                                                                ///< 211 Thermal Sensors Manufacturer ID First Byte
  0x80,                                                                ///< 212 Thermal Sensors Device Type
  0x11,                                                                ///< 213 Thermal Sensors Device Revision
  0, 0, 0, 0, 0, 0,                                                    ///< 214 - 219
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,                                        ///< 220 - 229
  0x0F,                                                                ///< 230 Module Nominal Height
  0x10,                                                                ///< 231 Module Nominal Thickness
  0x00,                                                                ///< 232 Reference Raw Card Used
  0x01,                                                                ///< 233 1 Row of DRAM on Module
  0x01,                                                                ///< 234 1 Rank, 8 bits SDRAM data width per channel
  0x22,                                                                ///< 235 2 Channels per DIMM, 32 bits per Channel
  0, 0, 0, 0,                                                          ///< 236 - 239
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,                                        ///< 240 - 249
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,                                        ///< 250 - 259
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,                                        ///< 260 - 269
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,                                        ///< 270 - 279
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,                                        ///< 280 - 289
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,                                        ///< 290 - 299
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,                                        ///< 300 - 309
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,                                        ///< 310 - 319
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,                                        ///< 320 - 329
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,                                        ///< 330 - 339
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,                                        ///< 340 - 349
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,                                        ///< 350 - 359
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,                                        ///< 360 - 369
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,                                        ///< 370 - 379
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,                                        ///< 380 - 389
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,                                        ///< 390 - 399
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,                                        ///< 400 - 409
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,                                        ///< 410 - 419
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,                                        ///< 420 - 429
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,                                        ///< 430 - 439
  0x00,  0x00,  0x00,  0x00,  0x00,  0x00,                             ///< 440 - 445
  0x9C,                                                                ///< 446 CRC for Bytes 128 - 253 LSB
  0xAD,                                                                ///< 447 CRC for Bytes 128 - 253 MSB
  0, 0,                                                                ///< 448 - 449
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,                                        ///< 450 - 459
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,                                        ///< 460 - 469
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,                                        ///< 470 - 479
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,                                        ///< 480 - 489
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,                                        ///< 490 - 499
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,                                        ///< 500 - 509
  0, 0                                                                 ///< 510 - 511
        ///< Ignore bytes 512-1023, @todo_adl: support 1024 bytes SPD array
}})}


[PcdsDynamicExVpd.common.SkuIdAdlPLp5Rvp]

#[-start-210526-KEBIN00005-add]#
!if $(C970_SUPPORT_ENABLE) == YES
gBoardModuleTokenSpaceGuid.Yogac970MO1Q1R16GSpdData|*|{CODE(
{
// LPDDR5 315b 16Gb die, DDP 2x16
// Micron MT62F512M32D2-031
// 6400, ??-??-??-??
// 8 Banks, no bank groups, 8Gb SDRAM density
// 15 Row bits, 11 Column bits
// Non-Monolithic DRAM Device, 2 dies, 2 Channels per package,
// mLpddr5xDdp8Gb315b1r6400Spd
  1,
 {0x23,                                 ///< 0   384 SPD bytes used, 512 total
  0x10,                                 ///< 1   SPD Revision 1.0
  0x13,                                 ///< 2   DRAM Type: LPDDR5 SDRAM
  0x0E,                                 ///< 3   Module Type: Not Hybrid (DRAM only) / Non-DIMM Solution (on-board DRAM)
  0x16,                                 ///< 4   8 Banks, no bank groups, 8 Gb SDRAM density
  0x22,                                 ///< 5   15 Rows, 11 Columns
  0x95,                                 ///< 6   Non-Monolithic DRAM Device, 2 die, 2 Channels per package, Signal Loading Matrix 1
  0x08,                                 ///< 7   SDRAM Optional Features: tMAW = 8192 * tREFI, Unlimited MAC
  0x00,                                 ///< 8   SDRAM Thermal / Refresh options: Reserved
  0x40,                                 ///< 9   Other SDRAM Optional Features: Post package repair supported, one row per bank group, Soft PPR not supported
  0x00,                                 ///< 10  Reserved
  0x00,                                 ///< 11  Module Nominal Voltage: Reserved
  0x02,                                 ///< 12  Module Organization: 1 Rank, x16 Device Width per Channel
  0x01,                                 ///< 13  Module Memory Bus width: 1 Channels, 16 bits channel width, no ECC
  0x00,                                 ///< 14  Module Thermal Sensor: none
  0x00,                                 ///< 15  Extended Module Type: Reserved
  0x48,                                 ///< 16  Signal Loading: Data/Strobe/Mask: 2 loads, CAC: 2 loads, CS: 1 load
  0x00,                                 ///< 17  MTB = 0.125ns, FTB = 1 ps
  0x0A,                                 ///< 18  tCKAVGmin = 1.25 ns (LPDDR5-6400)
  0xFF,                                 ///< 19  tCKAVGmax = 32.002 ns
  0x92,                                 ///< 20  CAS Latencies supported (First Byte) : 14, 10, 6
  0x55,                                 ///< 21  CAS Latencies supported (Second Byte): 28, 24, 20, 16
  0x05,                                 ///< 22  CAS Latencies supported (Third Byte) : 36, 32
  0x00,                                 ///< 23  CAS Latencies supported (Fourth Byte):
  0xAA,                                 ///< 24  Minimum CAS Latency (tAAmin) = 21.25 ns
  0x00,                                 ///< 25  Read and Write Latency Set options: Write Latency Set A and DBI-Read Disabled
  0x98,                                 ///< 26  Minimum RAS-to-CAS delay (tRCDmin) = 19 ns
  0xA8,                                 ///< 27  Row precharge time for all banks (tRPab) = 21 ns
  0x90,                                 ///< 28  Minimum row precharge time (tRPpb) = 18 ns
  0xC0,                                 ///< 29  tRFCab = 210 ns (8 Gb)
  0x08,                                 ///< 30  tRFCab MSB
  0x60,                                 ///< 31  tRFCpb = 120 ns (8 Gb)
  0x04,                                 ///< 32  tRFCpb MSB
  0, 0, 0, 0, 0, 0, 0,                  ///< 33 - 39
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 40 - 49
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 50 - 59
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 60 - 69
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 70 - 79
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 80 - 89
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 90 - 99
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 100 - 109
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 110 - 119
  0x00,                                 ///< 120 FTB for Row precharge time per bank (tRPpb) = 18 ns
  0x00,                                 ///< 121 FTB for Row precharge time for all banks (tRPab) = 21 ns
  0x00,                                 ///< 122 FTB for Minimum RAS-to-CAS delay (tRCDmin) = 19 ns
  0x00,                                 ///< 123 FTB for tAAmin = 21.25 ns
  0x7F,                                 ///< 124 FTB for tCKAVGmax = 32.002 ns
  0x00,                                 ///< 125 FTB for tCKAVGmin = 1.25 ns (LPDDR5-6400)
  0x00,                                 ///< 126 CRC A
  0x00,                                 ///< 127 CRC B
  0, 0,                                 ///< 128 - 129
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 130 - 139
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 140 - 149
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 150 - 159
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 160 - 169
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 170 - 179
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 180 - 189
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 190 - 199
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 200 - 209
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 210 - 219
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 220 - 229
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 230 - 239
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 240 - 249
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 250 - 259
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 260 - 269
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 270 - 279
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 280 - 289
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 290 - 299
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 300 - 309
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 310 - 319
  0x00,                                 ///< 320 Module Manufacturer ID Code, Least Significant Byte
  0x00,                                 ///< 321 Module Manufacturer ID Code, Most Significant Byte
  0x00,                                 ///< 322 Module Manufacturing Location
  0x00,                                 ///< 323 Module Manufacturing Date Year
  0x00,                                 ///< 324 Module Manufacturing Date Week
  0x20,                                 ///< 325 Module ID: Module Serial Number
  0x00,                                 ///< 326 Module Serial Number B
  0x00,                                 ///< 327 Module Serial Number C
  0x00,                                 ///< 328 Module Serial Number D
  0x20, 0x20, 0x20, 0x20, 0x20,         ///< 329 - 333 Module Part Number: Unused bytes coded as ASCII Blanks (0x20)
  0x20, 0x20, 0x20, 0x20, 0x20,         ///< 334 - 338 Module Part Number
  0x20, 0x20, 0x20, 0x20, 0x20,         ///< 339 - 343 Module Part Number
  0x20, 0x20, 0x20, 0x20, 0x20,         ///< 344 - 348 Module Part Number
  0x00,                                 ///< 349 Module Revision Code
  0x00,                                 ///< 350 DRAM Manufacturer ID Code, Least Significant Byte
  0x00,                                 ///< 351 DRAM Manufacturer ID Code, Most Significant Byte
  0x00,                                 ///< 352 DRAM Stepping
  0, 0, 0, 0, 0, 0, 0,                  ///< 353 - 359
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 360 - 369
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 370 - 379
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 380 - 389
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 390 - 399
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 400 - 409
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 410 - 419
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 420 - 429
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 430 - 439
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 440 - 449
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 450 - 459
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 460 - 469
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 470 - 479
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 480 - 489
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 490 - 499
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 500 - 509
  0, 0                                  ///< 510 - 511
}})}
!endif
#[-start-210526-KEBIN00005-add]#
#[-start-210519-KEBIN00001-modify]#
#[-start-210825-TAMT000002-modify]#
#[-start-210914-DABING0006-modify]#
!if ($(C970_SUPPORT_ENABLE) == YES) OR ($(C770_SUPPORT_ENABLE) == YES)  OR ($(S77014_SUPPORT_ENABLE) == YES) OR ($(S77013_SUPPORT_ENABLE) == YES) OR ($(S77014IAH_SUPPORT_ENABLE) == YES)
#[-start-210914-DABING0006-modify]#
#[-end-210825-TAMT000002-modify]#
#[-start-210709-YUNLEI0111-modify]
gBoardModuleTokenSpaceGuid.Yogac970MO1Q2R16GSpdData|*|{CODE(
{
// LPDDR5 315b 8Gb die, DDP 2x16
// Micron MT62F1G32D4DR-031 WT:B
// 6400, ??-??-??-??
// 8 Banks, no bank groups, 8Gb SDRAM density
// 15 Row bits, 11 Column bits
// Non-Monolithic DRAM Device, 2 dies, 2 Channels per package,
// mLpddr5xDdp8Gb315b1r6400Spd
  1,
 {0x23,                                 ///< 0   384 SPD bytes used, 512 total
  0x10,                                 ///< 1   SPD Revision 1.0
  0x13,                                 ///< 2   DRAM Type: LPDDR5 SDRAM
  0x0E,                                 ///< 3   Module Type: Not Hybrid (DRAM only) / Non-DIMM Solution (on-board DRAM)
  0x15,                                 ///< 4   8 Banks, no bank groups, 8 Gb SDRAM density
  0x1A,                                 ///< 5   15 Rows, 11 Columns
  0xB5,                                 ///< 6   Non-Monolithic DRAM Device, 2 die, 2 Channels per package, Signal Loading Matrix 1
  0x08,                                 ///< 7   SDRAM Optional Features: tMAW = 8192 * tREFI, Unlimited MAC
  0x00,                                 ///< 8   SDRAM Thermal / Refresh options: Reserved
  0x40,                                 ///< 9   Other SDRAM Optional Features: Post package repair supported, one row per bank group, Soft PPR not supported
  0x00,                                 ///< 10  Reserved
  0x00,                                 ///< 11  Module Nominal Voltage: Reserved
  0x0A,                                 ///< 12  Module Organization: 1 Rank, x16 Device Width per Channel
  0x01,                                 ///< 13  Module Memory Bus width: 1 Channels, 16 bits channel width, no ECC
  0x00,                                 ///< 14  Module Thermal Sensor: none
  0x00,                                 ///< 15  Extended Module Type: Reserved
  0x48,                                 ///< 16  Signal Loading: Data/Strobe/Mask: 2 loads, CAC: 2 loads, CS: 1 load
  0x00,                                 ///< 17  MTB = 0.125ns, FTB = 1 ps
  0x0A,                                 ///< 18  tCKAVGmin = 1.25 ns (LPDDR5-6400)
  0xFF,                                 ///< 19  tCKAVGmax = 32.002 ns
  0x92,                                 ///< 20  CAS Latencies supported (First Byte) : 14, 10, 6
  0x55,                                 ///< 21  CAS Latencies supported (Second Byte): 28, 24, 20, 16
  0x05,                                 ///< 22  CAS Latencies supported (Third Byte) : 36, 32
  0x00,                                 ///< 23  CAS Latencies supported (Fourth Byte):
  0xAA,                                 ///< 24  Minimum CAS Latency (tAAmin) = 21.25 ns
  0x00,                                 ///< 25  Read and Write Latency Set options: Write Latency Set A and DBI-Read Disabled
  0x90,                                 ///< 26  Minimum RAS-to-CAS delay (tRCDmin) = 19 ns
  0xA8,                                 ///< 27  Row precharge time for all banks (tRPab) = 21 ns
  0x90,                                 ///< 28  Minimum row precharge time (tRPpb) = 18 ns
  0x90,                                 ///< 29  tRFCab = 210 ns (8 Gb)
  0x06,                                 ///< 30  tRFCab MSB
  0xC0,                                 ///< 31  tRFCpb = 120 ns (8 Gb)
  0x03,                                 ///< 32  tRFCpb MSB
  0, 0, 0, 0, 0, 0, 0,                  ///< 33 - 39
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 40 - 49
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 50 - 59
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 60 - 69
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 70 - 79
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 80 - 89
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 90 - 99
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 100 - 109
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 110 - 119
  0x00,                                 ///< 120 FTB for Row precharge time per bank (tRPpb) = 18 ns
  0x00,                                 ///< 121 FTB for Row precharge time for all banks (tRPab) = 21 ns
  0x00,                                 ///< 122 FTB for Minimum RAS-to-CAS delay (tRCDmin) = 19 ns
  0x00,                                 ///< 123 FTB for tAAmin = 21.25 ns
  0x7F,                                 ///< 124 FTB for tCKAVGmax = 32.002 ns
  0x00,                                 ///< 125 FTB for tCKAVGmin = 1.25 ns (LPDDR5-6400)
  0x00,                                 ///< 126 CRC A
  0x00,                                 ///< 127 CRC B
  0, 0,                                 ///< 128 - 129
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 130 - 139
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 140 - 149
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 150 - 159
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 160 - 169
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 170 - 179
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 180 - 189
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 190 - 199
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 200 - 209
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 210 - 219
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 220 - 229
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 230 - 239
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 240 - 249
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 250 - 259
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 260 - 269
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 270 - 279
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 280 - 289
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 290 - 299
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 300 - 309
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 310 - 319
  0x80,                                 ///< 320 Module Manufacturer ID Code, Least Significant Byte
  0x2C,                                 ///< 321 Module Manufacturer ID Code, Most Significant Byte
  0x00,                                 ///< 322 Module Manufacturing Location
  0x00,                                 ///< 323 Module Manufacturing Date Year
  0x00,                                 ///< 324 Module Manufacturing Date Week
  0x00,                                 ///< 325 Module ID: Module Serial Number
  0x00,                                 ///< 326 Module Serial Number B
  0x00,                                 ///< 327 Module Serial Number C
  0x00,                                 ///< 328 Module Serial Number D
  0x4D, 0x54, 0x36, 0x32, 0x46,         ///< 329 - 333 Module Part Number: Unused bytes coded as ASCII Blanks (0x20)
  0x31, 0x47, 0x33, 0x32, 0x44,         ///< 334 - 338 Module Part Number
  0x34, 0x44, 0x52, 0x2D, 0x30,         ///< 339 - 343 Module Part Number
  0x33, 0x31, 0x20, 0x20, 0x20,         ///< 344 - 348 Module Part Number
  0x00,                                 ///< 349 Module Revision Code
  0x80,                                 ///< 350 DRAM Manufacturer ID Code, Least Significant Byte
  0x2C,                                 ///< 351 DRAM Manufacturer ID Code, Most Significant Byte
  0x00,                                 ///< 352 DRAM Stepping
  0, 0, 0, 0, 0, 0, 0,                  ///< 353 - 359
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 360 - 369
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 370 - 379
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 380 - 389
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 390 - 399
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 400 - 409
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 410 - 419
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 420 - 429
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 430 - 439
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 440 - 449
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 450 - 459
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 460 - 469
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 470 - 479
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 480 - 489
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 490 - 499
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 500 - 509
  0, 0                                  ///< 510 - 511
}})}

gBoardModuleTokenSpaceGuid.Yogac970MO1D1R8GSpdData|*|{CODE(
{
// LPDDR5 315b 8Gb die, DDP 2x16
// Micron MT62F512M32D2DR-031 WT:B
// 6400, ??-??-??-??
// 8 Banks, no bank groups, 8Gb SDRAM density
// 15 Row bits, 11 Column bits
// Non-Monolithic DRAM Device, 2 dies, 2 Channels per package,
// mLpddr5xDdp8Gb315b1r6400Spd
  1,
 {0x23,                                 ///< 0   384 SPD bytes used, 512 total
  0x10,                                 ///< 1   SPD Revision 1.0
  0x13,                                 ///< 2   DRAM Type: LPDDR5 SDRAM
  0x0E,                                 ///< 3   Module Type: Not Hybrid (DRAM only) / Non-DIMM Solution (on-board DRAM)
  0x15,                                 ///< 4   8 Banks, no bank groups, 8 Gb SDRAM density
  0x1A,                                 ///< 5   15 Rows, 11 Columns
  0x95,                                 ///< 6   Non-Monolithic DRAM Device, 2 die, 2 Channels per package, Signal Loading Matrix 1
  0x08,                                 ///< 7   SDRAM Optional Features: tMAW = 8192 * tREFI, Unlimited MAC
  0x00,                                 ///< 8   SDRAM Thermal / Refresh options: Reserved
  0x40,                                 ///< 9   Other SDRAM Optional Features: Post package repair supported, one row per bank group, Soft PPR not supported
  0x00,                                 ///< 10  Reserved
  0x00,                                 ///< 11  Module Nominal Voltage: Reserved
  0x02,                                 ///< 12  Module Organization: 1 Rank, x16 Device Width per Channel
  0x01,                                 ///< 13  Module Memory Bus width: 1 Channels, 16 bits channel width, no ECC
  0x00,                                 ///< 14  Module Thermal Sensor: none
  0x00,                                 ///< 15  Extended Module Type: Reserved
  0x48,                                 ///< 16  Signal Loading: Data/Strobe/Mask: 2 loads, CAC: 2 loads, CS: 1 load
  0x00,                                 ///< 17  MTB = 0.125ns, FTB = 1 ps
  0x0A,                                 ///< 18  tCKAVGmin = 1.25 ns (LPDDR5-6400)
  0xFF,                                 ///< 19  tCKAVGmax = 32.002 ns
  0x92,                                 ///< 20  CAS Latencies supported (First Byte) : 14, 10, 6
  0x55,                                 ///< 21  CAS Latencies supported (Second Byte): 28, 24, 20, 16
  0x05,                                 ///< 22  CAS Latencies supported (Third Byte) : 36, 32
  0x00,                                 ///< 23  CAS Latencies supported (Fourth Byte):
  0xAA,                                 ///< 24  Minimum CAS Latency (tAAmin) = 21.25 ns
  0x00,                                 ///< 25  Read and Write Latency Set options: Write Latency Set A and DBI-Read Disabled
  0x90,                                 ///< 26  Minimum RAS-to-CAS delay (tRCDmin) = 19 ns
  0xA8,                                 ///< 27  Row precharge time for all banks (tRPab) = 21 ns
  0x90,                                 ///< 28  Minimum row precharge time (tRPpb) = 18 ns
  0x90,                                 ///< 29  tRFCab = 210 ns (8 Gb)
  0x06,                                 ///< 30  tRFCab MSB
  0xC0,                                 ///< 31  tRFCpb = 120 ns (8 Gb)
  0x03,                                 ///< 32  tRFCpb MSB
  0, 0, 0, 0, 0, 0, 0,                  ///< 33 - 39
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 40 - 49
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 50 - 59
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 60 - 69
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 70 - 79
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 80 - 89
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 90 - 99
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 100 - 109
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 110 - 119
  0x00,                                 ///< 120 FTB for Row precharge time per bank (tRPpb) = 18 ns
  0x00,                                 ///< 121 FTB for Row precharge time for all banks (tRPab) = 21 ns
  0x00,                                 ///< 122 FTB for Minimum RAS-to-CAS delay (tRCDmin) = 19 ns
  0x00,                                 ///< 123 FTB for tAAmin = 21.25 ns
  0x7F,                                 ///< 124 FTB for tCKAVGmax = 32.002 ns
  0x00,                                 ///< 125 FTB for tCKAVGmin = 1.25 ns (LPDDR5-6400)
  0x00,                                 ///< 126 CRC A
  0x00,                                 ///< 127 CRC B
  0, 0,                                 ///< 128 - 129
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 130 - 139
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 140 - 149
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 150 - 159
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 160 - 169
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 170 - 179
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 180 - 189
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 190 - 199
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 200 - 209
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 210 - 219
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 220 - 229
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 230 - 239
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 240 - 249
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 250 - 259
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 260 - 269
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 270 - 279
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 280 - 289
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 290 - 299
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 300 - 309
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 310 - 319
  0x80,                                 ///< 320 Module Manufacturer ID Code, Least Significant Byte
  0x2C,                                 ///< 321 Module Manufacturer ID Code, Most Significant Byte
  0x00,                                 ///< 322 Module Manufacturing Location
  0x00,                                 ///< 323 Module Manufacturing Date Year
  0x00,                                 ///< 324 Module Manufacturing Date Week
  0x00,                                 ///< 325 Module ID: Module Serial Number
  0x00,                                 ///< 326 Module Serial Number B
  0x00,                                 ///< 327 Module Serial Number C
  0x00,                                 ///< 328 Module Serial Number D
  0x4D, 0x54, 0x36, 0x32, 0x46,         ///< 329 - 333 Module Part Number: Unused bytes coded as ASCII Blanks (0x20)
  0x35, 0x31, 0x32, 0x4D, 0x33,         ///< 334 - 338 Module Part Number
  0x32, 0x44, 0x32, 0x44, 0x52,         ///< 339 - 343 Module Part Number
  0x2D, 0x30, 0x33, 0x31, 0x20,         ///< 344 - 348 Module Part Number
  0x00,                                 ///< 349 Module Revision Code
  0x80,                                 ///< 350 DRAM Manufacturer ID Code, Least Significant Byte
  0x2C,                                 ///< 351 DRAM Manufacturer ID Code, Most Significant Byte
  0x00,                                 ///< 352 DRAM Stepping
  0, 0, 0, 0, 0, 0, 0,                  ///< 353 - 359
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 360 - 369
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 370 - 379
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 380 - 389
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 390 - 399
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 400 - 409
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 410 - 419
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 420 - 429
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 430 - 439
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 440 - 449
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 450 - 459
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 460 - 469
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 470 - 479
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 480 - 489
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 490 - 499
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 500 - 509
  0, 0                                  ///< 510 - 511
}})}

gBoardModuleTokenSpaceGuid.Yogac970MO2O2R32GSpdData|*|{CODE(
{
// LPDDR5 315b 8Gb die, DDP 2x16
// Micron MT62F2G32D8DR-031 WT:B 
// 6400, ??-??-??-??
// 8 Banks, no bank groups, 8Gb SDRAM density
// 15 Row bits, 11 Column bits
// Non-Monolithic DRAM Device, 2 dies, 2 Channels per package,
// mLpddr5xDdp8Gb315b1r6400Spd
  1,
 {0x23,                                 ///< 0   384 SPD bytes used, 512 total
  0x10,                                 ///< 1   SPD Revision 1.0
  0x13,                                 ///< 2   DRAM Type: LPDDR5 SDRAM
  0x0E,                                 ///< 3   Module Type: Not Hybrid (DRAM only) / Non-DIMM Solution (on-board DRAM)
  0x15,                                 ///< 4   8 Banks, no bank groups, 8 Gb SDRAM density
  0x22,                                 ///< 5   15 Rows, 11 Columns
  0xF9,                                 ///< 6   Non-Monolithic DRAM Device, 2 die, 2 Channels per package, Signal Loading Matrix 1
  0x08,                                 ///< 7   SDRAM Optional Features: tMAW = 8192 * tREFI, Unlimited MAC
  0x00,                                 ///< 8   SDRAM Thermal / Refresh options: Reserved
  0x40,                                 ///< 9   Other SDRAM Optional Features: Post package repair supported, one row per bank group, Soft PPR not supported
  0x00,                                 ///< 10  Reserved
  0x00,                                 ///< 11  Module Nominal Voltage: Reserved
  0x09,                                 ///< 12  Module Organization: 1 Rank, x16 Device Width per Channel
  0x01,                                 ///< 13  Module Memory Bus width: 1 Channels, 16 bits channel width, no ECC
  0x00,                                 ///< 14  Module Thermal Sensor: none
  0x00,                                 ///< 15  Extended Module Type: Reserved
  0x48,                                 ///< 16  Signal Loading: Data/Strobe/Mask: 2 loads, CAC: 2 loads, CS: 1 load
  0x00,                                 ///< 17  MTB = 0.125ns, FTB = 1 ps
  0x0A,                                 ///< 18  tCKAVGmin = 1.25 ns (LPDDR5-6400)
  0xFF,                                 ///< 19  tCKAVGmax = 32.002 ns
  0x92,                                 ///< 20  CAS Latencies supported (First Byte) : 14, 10, 6
  0x55,                                 ///< 21  CAS Latencies supported (Second Byte): 28, 24, 20, 16
  0x05,                                 ///< 22  CAS Latencies supported (Third Byte) : 36, 32
  0x00,                                 ///< 23  CAS Latencies supported (Fourth Byte):
  0xAA,                                 ///< 24  Minimum CAS Latency (tAAmin) = 21.25 ns
  0x00,                                 ///< 25  Read and Write Latency Set options: Write Latency Set A and DBI-Read Disabled
  0x90,                                 ///< 26  Minimum RAS-to-CAS delay (tRCDmin) = 19 ns
  0xA8,                                 ///< 27  Row precharge time for all banks (tRPab) = 21 ns
  0x90,                                 ///< 28  Minimum row precharge time (tRPpb) = 18 ns
  0x90,                                 ///< 29  tRFCab = 210 ns (8 Gb)
  0x06,                                 ///< 30  tRFCab MSB
  0xC0,                                 ///< 31  tRFCpb = 120 ns (8 Gb)
  0x03,                                 ///< 32  tRFCpb MSB
  0, 0, 0, 0, 0, 0, 0,                  ///< 33 - 39
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 40 - 49
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 50 - 59
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 60 - 69
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 70 - 79
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 80 - 89
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 90 - 99
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 100 - 109
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 110 - 119
  0x00,                                 ///< 120 FTB for Row precharge time per bank (tRPpb) = 18 ns
  0x00,                                 ///< 121 FTB for Row precharge time for all banks (tRPab) = 21 ns
  0x00,                                 ///< 122 FTB for Minimum RAS-to-CAS delay (tRCDmin) = 19 ns
  0x00,                                 ///< 123 FTB for tAAmin = 21.25 ns
  0x7F,                                 ///< 124 FTB for tCKAVGmax = 32.002 ns
  0x00,                                 ///< 125 FTB for tCKAVGmin = 1.25 ns (LPDDR5-6400)
  0x00,                                 ///< 126 CRC A
  0x00,                                 ///< 127 CRC B
  0, 0,                                 ///< 128 - 129
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 130 - 139
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 140 - 149
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 150 - 159
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 160 - 169
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 170 - 179
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 180 - 189
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 190 - 199
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 200 - 209
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 210 - 219
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 220 - 229
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 230 - 239
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 240 - 249
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 250 - 259
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 260 - 269
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 270 - 279
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 280 - 289
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 290 - 299
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 300 - 309
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 310 - 319
  0x80,                                 ///< 320 Module Manufacturer ID Code, Least Significant Byte
  0x2C,                                 ///< 321 Module Manufacturer ID Code, Most Significant Byte
  0x00,                                 ///< 322 Module Manufacturing Location
  0x00,                                 ///< 323 Module Manufacturing Date Year
  0x00,                                 ///< 324 Module Manufacturing Date Week
  0x00,                                 ///< 325 Module ID: Module Serial Number
  0x00,                                 ///< 326 Module Serial Number B
  0x00,                                 ///< 327 Module Serial Number C
  0x00,                                 ///< 328 Module Serial Number D
  0x4D, 0x54, 0x36, 0x32, 0x46,         ///< 329 - 333 Module Part Number: Unused bytes coded as ASCII Blanks (0x20)
  0x32, 0x47, 0x33, 0x32, 0x44,         ///< 334 - 338 Module Part Number
  0x38, 0x44, 0x52, 0x2D, 0x30,         ///< 339 - 343 Module Part Number
  0x33, 0x31, 0x20, 0x20, 0x20,         ///< 344 - 348 Module Part Number
  0x00,                                 ///< 349 Module Revision Code
  0x80,                                 ///< 350 DRAM Manufacturer ID Code, Least Significant Byte
  0x2C,                                 ///< 351 DRAM Manufacturer ID Code, Most Significant Byte
  0x00,                                 ///< 352 DRAM Stepping
  0, 0, 0, 0, 0, 0, 0,                  ///< 353 - 359
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 360 - 369
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 370 - 379
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 380 - 389
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 390 - 399
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 400 - 409
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 410 - 419
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 420 - 429
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 430 - 439
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 440 - 449
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 450 - 459
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 460 - 469
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 470 - 479
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 480 - 489
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 490 - 499
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 500 - 509
  0, 0                                  ///< 510 - 511
}})}
#[-end-210709-YUNLEI0111-modify]
#[-end-210519-KEBIN00001-modify]


#[-start-210708-YUNLEI0110-modify]
gBoardModuleTokenSpaceGuid.Yogac970Samsung16GSpdData|*|{CODE(
{
// LPDDR5 315F 32Gb die, DDP 2x16
// Samsung K3LKBKB0BM-MGCP
  1,
 {0x23,                                 ///< 0   384 SPD bytes used, 512 total
  0x10,                                 ///< 1   SPD Revision 1.0
  0x13,                                 ///< 2   DRAM Type: LPDDR5 SDRAM
  0x0E,                                 ///< 3   Module Type: Not Hybrid (DRAM only) / Non-DIMM Solution (on-board DRAM)
  0x16,                                 ///< 4   8 Banks, no bank groups, 8 Gb SDRAM density
  0x22,                                 ///< 5   15 Rows, 11 Columns
  0x95,                                 ///< 6   Non-Monolithic DRAM Device, 2 die, 2 Channels per package, Signal Loading Matrix 1
  0x08,                                 ///< 7   SDRAM Optional Features: tMAW = 8192 * tREFI, Unlimited MAC
  0x00,                                 ///< 8   SDRAM Thermal / Refresh options: Reserved
  0x00,                                 ///< 9   Other SDRAM Optional Features: Post package repair supported, one row per bank group, Soft PPR not supported
  0x00,                                 ///< 10  Reserved
  0x00,                                 ///< 11  Module Nominal Voltage: Reserved
  0x02,                                 ///< 12  Module Organization: 1 Rank, x16 Device Width per Channel
  0x01,                                 ///< 13  Module Memory Bus width: 1 Channels, 16 bits channel width, no ECC
  0x00,                                 ///< 14  Module Thermal Sensor: none
  0x00,                                 ///< 15  Extended Module Type: Reserved
  0x00,                                 ///< 16  Signal Loading: Data/Strobe/Mask: 2 loads, CAC: 2 loads, CS: 1 load
  0x00,                                 ///< 17  MTB = 0.125ns, FTB = 1 ps
  0x0A,                                 ///< 18  tCKAVGmin = 1.25 ns (LPDDR5-6400)
  0xFF,                                 ///< 19  tCKAVGmax = 32.002 ns
  0x92,                                 ///< 20  CAS Latencies supported (First Byte) : 14, 10, 6
  0x55,                                 ///< 21  CAS Latencies supported (Second Byte): 28, 24, 20, 16
  0x05,                                 ///< 22  CAS Latencies supported (Third Byte) : 36, 32
  0x00,                                 ///< 23  CAS Latencies supported (Fourth Byte):
  0xAA,                                 ///< 24  Minimum CAS Latency (tAAmin) = 21.25 ns
  0x00,                                 ///< 25  Read and Write Latency Set options: Write Latency Set A and DBI-Read Disabled
  0x90,                                 ///< 26  Minimum RAS-to-CAS delay (tRCDmin) = 19 ns
  0xA8,                                 ///< 27  Row precharge time for all banks (tRPab) = 21 ns
  0x90,                                 ///< 28  Minimum row precharge time (tRPpb) = 18 ns
  0xC0,                                 ///< 29  tRFCab = 210 ns (8 Gb)
  0x08,                                 ///< 30  tRFCab MSB
  0x60,                                 ///< 31  tRFCpb = 120 ns (8 Gb)
  0x04,                                 ///< 32  tRFCpb MSB
  0, 0, 0, 0, 0, 0, 0,                  ///< 33 - 39
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 40 - 49
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 50 - 59
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 60 - 69
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 70 - 79
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 80 - 89
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 90 - 99
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 100 - 109
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 110 - 119
  0x00,                                 ///< 120 FTB for Row precharge time per bank (tRPpb) = 18 ns
  0x00,                                 ///< 121 FTB for Row precharge time for all banks (tRPab) = 21 ns
  0x00,                                 ///< 122 FTB for Minimum RAS-to-CAS delay (tRCDmin) = 19 ns
  0x00,                                 ///< 123 FTB for tAAmin = 21.25 ns
  0x7F,                                 ///< 124 FTB for tCKAVGmax = 32.002 ns
  0x00,                                 ///< 125 FTB for tCKAVGmin = 1.25 ns (LPDDR5-6400)
  0x60,                                 ///< 126 CRC A
  0x3F,                                 ///< 127 CRC B
  0, 0,                                 ///< 128 - 129
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 130 - 139
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 140 - 149
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 150 - 159
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 160 - 169
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 170 - 179
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 180 - 189
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 190 - 199
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 200 - 209
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 210 - 219
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 220 - 229
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 230 - 239
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 240 - 249
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 250 - 259
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 260 - 269
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 270 - 279
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 280 - 289
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 290 - 299
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 300 - 309
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 310 - 319
  0x80,                                 ///< 320 Module Manufacturer ID Code, Least Significant Byte
  0xCE,                                 ///< 321 Module Manufacturer ID Code, Most Significant Byte
  0x00,                                 ///< 322 Module Manufacturing Location
  0x00,                                 ///< 323 Module Manufacturing Date Year
  0x00,                                 ///< 324 Module Manufacturing Date Week
  0x00,                                 ///< 325 Module ID: Module Serial Number
  0x00,                                 ///< 326 Module Serial Number B
  0x00,                                 ///< 327 Module Serial Number C
  0x00,                                 ///< 328 Module Serial Number D
  0x4B, 0x33, 0x4C, 0x4B, 0x42,         ///< 329 - 333 Module Part Number: Unused bytes coded as ASCII Blanks (0x20)
  0x4B, 0x42, 0x30, 0x42, 0x4D,         ///< 334 - 338 Module Part Number
  0x2D, 0x4D, 0x47, 0x43, 0x50,         ///< 339 - 343 Module Part Number
  0x00, 0x00, 0x00, 0x00, 0x00,         ///< 344 - 348 Module Part Number
  0x00,                                 ///< 349 Module Revision Code
  0x80,                                 ///< 350 DRAM Manufacturer ID Code, Least Significant Byte
  0xCE,                                 ///< 351 DRAM Manufacturer ID Code, Most Significant Byte
  0x00,                                 ///< 352 DRAM Stepping
  0, 0, 0, 0, 0, 0, 0,                  ///< 353 - 359
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 360 - 369
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 370 - 379
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 380 - 389
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 390 - 399
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 400 - 409
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 410 - 419
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 420 - 429
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 430 - 439
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 440 - 449
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 450 - 459
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 460 - 469
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 470 - 479
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 480 - 489
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 490 - 499
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 500 - 509
  0, 0                                  ///< 510 - 511
}})}

gBoardModuleTokenSpaceGuid.Yogac970hynix16GSpdData|*|{CODE(
{
// LPDDR5 315b 8Gb QDP 4GB 6400 (2ch 4pcs)
// hynix H9JCNNNCP3MLYR-N6E
  1,
 {0x23,                                 ///< 0   384 SPD bytes used, 512 total
  0x10,                                 ///< 1   SPD Revision 1.0
  0x13,                                 ///< 2   DRAM Type: LPDDR5 SDRAM
  0x0E,                                 ///< 3   Module Type: Not Hybrid (DRAM only) / Non-DIMM Solution (on-board DRAM)
  0x15,                                 ///< 4   8 Banks, no bank groups, 8 Gb SDRAM density
  0x1A,                                 ///< 5   15 Rows, 11 Columns
  0xB5,                                 ///< 6   Non-Monolithic DRAM Device, 2 die, 2 Channels per package, Signal Loading Matrix 1
  0x08,                                 ///< 7   SDRAM Optional Features: tMAW = 8192 * tREFI, Unlimited MAC
  0x00,                                 ///< 8   SDRAM Thermal / Refresh options: Reserved
  0x40,                                 ///< 9   Other SDRAM Optional Features: Post package repair supported, one row per bank group, Soft PPR not supported
  0x00,                                 ///< 10  Reserved
  0x00,                                 ///< 11  Module Nominal Voltage: Reserved
  0x0A,                                 ///< 12  Module Organization: 1 Rank, x16 Device Width per Channel
  0x01,                                 ///< 13  Module Memory Bus width: 1 Channels, 16 bits channel width, no ECC
  0x00,                                 ///< 14  Module Thermal Sensor: none
  0x00,                                 ///< 15  Extended Module Type: Reserved
  0x48,                                 ///< 16  Signal Loading: Data/Strobe/Mask: 2 loads, CAC: 2 loads, CS: 1 load
  0x00,                                 ///< 17  MTB = 0.125ns, FTB = 1 ps
  0x0A,                                 ///< 18  tCKAVGmin = 1.25 ns (LPDDR5-6400)
  0xFF,                                 ///< 19  tCKAVGmax = 32.002 ns
  0x92,                                 ///< 20  CAS Latencies supported (First Byte) : 14, 10, 6
  0x55,                                 ///< 21  CAS Latencies supported (Second Byte): 28, 24, 20, 16
  0x05,                                 ///< 22  CAS Latencies supported (Third Byte) : 36, 32
  0x00,                                 ///< 23  CAS Latencies supported (Fourth Byte):
  0xAA,                                 ///< 24  Minimum CAS Latency (tAAmin) = 21.25 ns
  0x00,                                 ///< 25  Read and Write Latency Set options: Write Latency Set A and DBI-Read Disabled
  0x90,                                 ///< 26  Minimum RAS-to-CAS delay (tRCDmin) = 19 ns
  0xA8,                                 ///< 27  Row precharge time for all banks (tRPab) = 21 ns
  0x90,                                 ///< 28  Minimum row precharge time (tRPpb) = 18 ns
  0x90,                                 ///< 29  tRFCab = 210 ns (8 Gb)
  0x06,                                 ///< 30  tRFCab MSB
  0xC0,                                 ///< 31  tRFCpb = 120 ns (8 Gb)
  0x03,                                 ///< 32  tRFCpb MSB
  0, 0, 0, 0, 0, 0, 0,                  ///< 33 - 39
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 40 - 49
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 50 - 59
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 60 - 69
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 70 - 79
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 80 - 89
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 90 - 99
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 100 - 109
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 110 - 119
  0x00,                                 ///< 120 FTB for Row precharge time per bank (tRPpb) = 18 ns
  0x00,                                 ///< 121 FTB for Row precharge time for all banks (tRPab) = 21 ns
  0x00,                                 ///< 122 FTB for Minimum RAS-to-CAS delay (tRCDmin) = 19 ns
  0x00,                                 ///< 123 FTB for tAAmin = 21.25 ns
  0x7F,                                 ///< 124 FTB for tCKAVGmax = 32.002 ns
  0x00,                                 ///< 125 FTB for tCKAVGmin = 1.25 ns (LPDDR5-6400)
  0x00,                                 ///< 126 CRC A
  0x00,                                 ///< 127 CRC B
  0, 0,                                 ///< 128 - 129
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 130 - 139
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 140 - 149
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 150 - 159
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 160 - 169
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 170 - 179
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 180 - 189
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 190 - 199
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 200 - 209
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 210 - 219
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 220 - 229
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 230 - 239
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 240 - 249
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 250 - 259
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 260 - 269
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 270 - 279
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 280 - 289
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 290 - 299
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 300 - 309
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 310 - 319
  0x80,                                 ///< 320 Module Manufacturer ID Code, Least Significant Byte
  0xAD,                                 ///< 321 Module Manufacturer ID Code, Most Significant Byte
  0x00,                                 ///< 322 Module Manufacturing Location
  0x00,                                 ///< 323 Module Manufacturing Date Year
  0x00,                                 ///< 324 Module Manufacturing Date Week
  0x00,                                 ///< 325 Module ID: Module Serial Number
  0x00,                                 ///< 326 Module Serial Number B
  0x00,                                 ///< 327 Module Serial Number C
  0x00,                                 ///< 328 Module Serial Number D
  0x48, 0x39, 0x4A, 0x43, 0x4E,         ///< 329 - 333 Module Part Number: Unused bytes coded as ASCII Blanks (0x20)
  0x4E, 0x4E, 0x43, 0x50, 0x33,         ///< 334 - 338 Module Part Number
  0x4D, 0x4C, 0x59, 0x52, 0x2D,         ///< 339 - 343 Module Part Number
  0x4E, 0x36, 0x45, 0x20, 0x20,         ///< 344 - 348 Module Part Number
  0x00,                                 ///< 349 Module Revision Code
  0x80,                                 ///< 350 DRAM Manufacturer ID Code, Least Significant Byte
  0xAD,                                 ///< 351 DRAM Manufacturer ID Code, Most Significant Byte
  0x00,                                 ///< 352 DRAM Stepping
  0, 0, 0, 0, 0, 0, 0,                  ///< 353 - 359
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 360 - 369
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 370 - 379
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 380 - 389
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 390 - 399
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 400 - 409
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 410 - 419
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 420 - 429
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 430 - 439
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 440 - 449
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 450 - 459
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 460 - 469
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 470 - 479
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 480 - 489
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 490 - 499
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 500 - 509
  0, 0                                  ///< 510 - 511
}})}

gBoardModuleTokenSpaceGuid.Yogac970hynix8GSpdData|*|{CODE(
{
// LPDDR5 315b 8Gb DDP 2GB 6400 (2ch 2pcs)
// hynix H9JCNNNBK3MLYR-N6E
  1,
 {0x23,                                 ///< 0   384 SPD bytes used, 512 total
  0x10,                                 ///< 1   SPD Revision 1.0
  0x13,                                 ///< 2   DRAM Type: LPDDR5 SDRAM
  0x0E,                                 ///< 3   Module Type: Not Hybrid (DRAM only) / Non-DIMM Solution (on-board DRAM)
  0x15,                                 ///< 4   8 Banks, no bank groups, 8 Gb SDRAM density
  0x1A,                                 ///< 5   15 Rows, 11 Columns
  0x95,                                 ///< 6   Non-Monolithic DRAM Device, 2 die, 2 Channels per package, Signal Loading Matrix 1
  0x08,                                 ///< 7   SDRAM Optional Features: tMAW = 8192 * tREFI, Unlimited MAC
  0x00,                                 ///< 8   SDRAM Thermal / Refresh options: Reserved
  0x40,                                 ///< 9   Other SDRAM Optional Features: Post package repair supported, one row per bank group, Soft PPR not supported
  0x00,                                 ///< 10  Reserved
  0x00,                                 ///< 11  Module Nominal Voltage: Reserved
  0x02,                                 ///< 12  Module Organization: 1 Rank, x16 Device Width per Channel
  0x01,                                 ///< 13  Module Memory Bus width: 1 Channels, 16 bits channel width, no ECC
  0x00,                                 ///< 14  Module Thermal Sensor: none
  0x00,                                 ///< 15  Extended Module Type: Reserved
  0x00,                                 ///< 16  Signal Loading: Data/Strobe/Mask: 2 loads, CAC: 2 loads, CS: 1 load
  0x00,                                 ///< 17  MTB = 0.125ns, FTB = 1 ps
  0x0A,                                 ///< 18  tCKAVGmin = 1.25 ns (LPDDR5-6400)
  0xFF,                                 ///< 19  tCKAVGmax = 32.002 ns
  0x92,                                 ///< 20  CAS Latencies supported (First Byte) : 14, 10, 6
  0x55,                                 ///< 21  CAS Latencies supported (Second Byte): 28, 24, 20, 16
  0x05,                                 ///< 22  CAS Latencies supported (Third Byte) : 36, 32
  0x00,                                 ///< 23  CAS Latencies supported (Fourth Byte):
  0xAA,                                 ///< 24  Minimum CAS Latency (tAAmin) = 21.25 ns
  0x00,                                 ///< 25  Read and Write Latency Set options: Write Latency Set A and DBI-Read Disabled
  0x90,                                 ///< 26  Minimum RAS-to-CAS delay (tRCDmin) = 19 ns
  0xA8,                                 ///< 27  Row precharge time for all banks (tRPab) = 21 ns
  0x90,                                 ///< 28  Minimum row precharge time (tRPpb) = 18 ns
  0x90,                                 ///< 29  tRFCab = 210 ns (8 Gb)
  0x06,                                 ///< 30  tRFCab MSB
  0xC0,                                 ///< 31  tRFCpb = 120 ns (8 Gb)
  0x03,                                 ///< 32  tRFCpb MSB
  0, 0, 0, 0, 0, 0, 0,                  ///< 33 - 39
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 40 - 49
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 50 - 59
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 60 - 69
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 70 - 79
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 80 - 89
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 90 - 99
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 100 - 109
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 110 - 119
  0x00,                                 ///< 120 FTB for Row precharge time per bank (tRPpb) = 18 ns
  0x00,                                 ///< 121 FTB for Row precharge time for all banks (tRPab) = 21 ns
  0x00,                                 ///< 122 FTB for Minimum RAS-to-CAS delay (tRCDmin) = 19 ns
  0x00,                                 ///< 123 FTB for tAAmin = 21.25 ns
  0x7F,                                 ///< 124 FTB for tCKAVGmax = 32.002 ns
  0x00,                                 ///< 125 FTB for tCKAVGmin = 1.25 ns (LPDDR5-6400)
  0x00,                                 ///< 126 CRC A
  0x00,                                 ///< 127 CRC B
  0, 0,                                 ///< 128 - 129
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 130 - 139
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 140 - 149
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 150 - 159
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 160 - 169
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 170 - 179
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 180 - 189
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 190 - 199
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 200 - 209
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 210 - 219
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 220 - 229
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 230 - 239
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 240 - 249
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 250 - 259
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 260 - 269
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 270 - 279
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 280 - 289
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 290 - 299
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 300 - 309
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 310 - 319
  0x80,                                 ///< 320 Module Manufacturer ID Code, Least Significant Byte
  0xAD,                                 ///< 321 Module Manufacturer ID Code, Most Significant Byte
  0x00,                                 ///< 322 Module Manufacturing Location
  0x00,                                 ///< 323 Module Manufacturing Date Year
  0x00,                                 ///< 324 Module Manufacturing Date Week
  0x00,                                 ///< 325 Module ID: Module Serial Number
  0x00,                                 ///< 326 Module Serial Number B
  0x00,                                 ///< 327 Module Serial Number C
  0x00,                                 ///< 328 Module Serial Number D
  0x48, 0x39, 0x4A, 0x43, 0x4E,         ///< 329 - 333 Module Part Number: Unused bytes coded as ASCII Blanks (0x20)
  0x4E, 0x4E, 0x42, 0x4B, 0x33,         ///< 334 - 338 Module Part Number
  0x4D, 0x4C, 0x59, 0x52, 0x2D,         ///< 339 - 343 Module Part Number
  0x4E, 0x36, 0x45, 0x20, 0x20,         ///< 344 - 348 Module Part Number
  0x00,                                 ///< 349 Module Revision Code
  0x80,                                 ///< 350 DRAM Manufacturer ID Code, Least Significant Byte
  0xAD,                                 ///< 351 DRAM Manufacturer ID Code, Most Significant Byte
  0x00,                                 ///< 352 DRAM Stepping
  0, 0, 0, 0, 0, 0, 0,                  ///< 353 - 359
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 360 - 369
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 370 - 379
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 380 - 389
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 390 - 399
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 400 - 409
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 410 - 419
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 420 - 429
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 430 - 439
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 440 - 449
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 450 - 459
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 460 - 469
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 470 - 479
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 480 - 489
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 490 - 499
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 500 - 509
  0, 0                                  ///< 510 - 511
}})}
!endif
#[-start-210825-TAMT000002-modify]#
# end C970_SUPPORT_ENABLE Or C770_SUPPORT_ENABLE or S77014_SUPPORT_ENABLE or S77013_SUPPORT_ENABLE
#[-end-210825-TAMT000002-modify]#

#[start-210720-STORM1100-modify]#
!if $(C770_SUPPORT_ENABLE) == YES
#[-start-210726-YUNLEI0113-modify]
gBoardModuleTokenSpaceGuid.Yogac770Samsung32GSpdData|*|{CODE(
{
// LPDDR5 315F 32Gb die, DDP 2x16
// Samsung  LPDDR5 315F 64Gb(QDP) K3LKCKC0BM-MGCP
  1,
 {0x23,                                 ///< 0   384 SPD bytes used, 512 total
  0x10,                                 ///< 1   SPD Revision 1.0
  0x13,                                 ///< 2   DRAM Type: LPDDR5 SDRAM
  0x0E,                                 ///< 3   Module Type: Not Hybrid (DRAM only) / Non-DIMM Solution (on-board DRAM)
  0x16,                                 ///< 4   8 Banks, no bank groups, 8 Gb SDRAM density
  0x22,                                 ///< 5   15 Rows, 11 Columns
  0xB5,                                 ///< 6   Non-Monolithic DRAM Device, 2 die, 2 Channels per package, Signal Loading Matrix 1
  0x08,                                 ///< 7   SDRAM Optional Features: tMAW = 8192 * tREFI, Unlimited MAC
  0x00,                                 ///< 8   SDRAM Thermal / Refresh options: Reserved
  0x00,                                 ///< 9   Other SDRAM Optional Features: Post package repair supported, one row per bank group, Soft PPR not supported
  0x00,                                 ///< 10  Reserved
  0x00,                                 ///< 11  Module Nominal Voltage: Reserved
  0x0A,                                 ///< 12  Module Organization: 1 Rank, x16 Device Width per Channel
  0x01,                                 ///< 13  Module Memory Bus width: 1 Channels, 16 bits channel width, no ECC
  0x00,                                 ///< 14  Module Thermal Sensor: none
  0x00,                                 ///< 15  Extended Module Type: Reserved
  0x48,                                 ///< 16  Signal Loading: Data/Strobe/Mask: 2 loads, CAC: 2 loads, CS: 1 load
  0x00,                                 ///< 17  MTB = 0.125ns, FTB = 1 ps
  0x0A,                                 ///< 18  tCKAVGmin = 1.25 ns (LPDDR5-6400)
  0xFF,                                 ///< 19  tCKAVGmax = 32.002 ns
  0x92,                                 ///< 20  CAS Latencies supported (First Byte) : 14, 10, 6
  0x55,                                 ///< 21  CAS Latencies supported (Second Byte): 28, 24, 20, 16
  0x05,                                 ///< 22  CAS Latencies supported (Third Byte) : 36, 32
  0x00,                                 ///< 23  CAS Latencies supported (Fourth Byte):
  0xAA,                                 ///< 24  Minimum CAS Latency (tAAmin) = 21.25 ns
  0x00,                                 ///< 25  Read and Write Latency Set options: Write Latency Set A and DBI-Read Disabled
  0x90,                                 ///< 26  Minimum RAS-to-CAS delay (tRCDmin) = 19 ns
  0xA8,                                 ///< 27  Row precharge time for all banks (tRPab) = 21 ns
  0x90,                                 ///< 28  Minimum row precharge time (tRPpb) = 18 ns
  0xC0,                                 ///< 29  tRFCab = 210 ns (8 Gb)
  0x08,                                 ///< 30  tRFCab MSB
  0x60,                                 ///< 31  tRFCpb = 120 ns (8 Gb)
  0x04,                                 ///< 32  tRFCpb MSB
  0, 0, 0, 0, 0, 0, 0,                  ///< 33 - 39
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 40 - 49
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 50 - 59
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 60 - 69
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 70 - 79
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 80 - 89
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 90 - 99
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 100 - 109
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 110 - 119
  0x00,                                 ///< 120 FTB for Row precharge time per bank (tRPpb) = 18 ns
  0x00,                                 ///< 121 FTB for Row precharge time for all banks (tRPab) = 21 ns
  0x00,                                 ///< 122 FTB for Minimum RAS-to-CAS delay (tRCDmin) = 19 ns
  0x00,                                 ///< 123 FTB for tAAmin = 21.25 ns
  0x7F,                                 ///< 124 FTB for tCKAVGmax = 32.002 ns
  0x00,                                 ///< 125 FTB for tCKAVGmin = 1.25 ns (LPDDR5-6400)
  0x0E,                                 ///< 126 CRC A
  0x3D,                                 ///< 127 CRC B
  0, 0,                                 ///< 128 - 129
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 130 - 139
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 140 - 149
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 150 - 159
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 160 - 169
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 170 - 179
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 180 - 189
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 190 - 199
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 200 - 209
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 210 - 219
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 220 - 229
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 230 - 239
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 240 - 249
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 250 - 259
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 260 - 269
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 270 - 279
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 280 - 289
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 290 - 299
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 300 - 309
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 310 - 319
  0x80,                                 ///< 320 Module Manufacturer ID Code, Least Significant Byte
  0xCE,                                 ///< 321 Module Manufacturer ID Code, Most Significant Byte
  0x00,                                 ///< 322 Module Manufacturing Location
  0x00,                                 ///< 323 Module Manufacturing Date Year
  0x00,                                 ///< 324 Module Manufacturing Date Week
  0x00,                                 ///< 325 Module ID: Module Serial Number
  0x00,                                 ///< 326 Module Serial Number B
  0x00,                                 ///< 327 Module Serial Number C
  0x00,                                 ///< 328 Module Serial Number D
  0x4B, 0x33, 0x4C, 0x4B, 0x43,         ///< 329 - 333 Module Part Number: Unused bytes coded as ASCII Blanks (0x20)
  0x4B, 0x43, 0x30, 0x42, 0x4D,         ///< 334 - 338 Module Part Number
  0x2D, 0x4D, 0x47, 0x43, 0x50,         ///< 339 - 343 Module Part Number
  0x00, 0x00, 0x00, 0x00, 0x00,         ///< 344 - 348 Module Part Number
  0x00,                                 ///< 349 Module Revision Code
  0x80,                                 ///< 350 DRAM Manufacturer ID Code, Least Significant Byte
  0xCE,                                 ///< 351 DRAM Manufacturer ID Code, Most Significant Byte
  0x00,                                 ///< 352 DRAM Stepping
  0, 0, 0, 0, 0, 0, 0,                  ///< 353 - 359
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 360 - 369
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 370 - 379
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 380 - 389
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 390 - 399
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 400 - 409
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 410 - 419
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 420 - 429
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 430 - 439
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 440 - 449
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 450 - 459
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 460 - 469
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 470 - 479
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 480 - 489
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 490 - 499
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 500 - 509
  0, 0                                  ///< 510 - 511
}})}

gBoardModuleTokenSpaceGuid.Yogac770hynix32GSpdData|*|{CODE(
{
// LPDDR5 315b 8Gb ODP 8GB 6400 (2ch 8pcs)
// hynix H9JCNNNFA5MLYR-N6E
  1,
 {0x23,                                 ///< 0   384 SPD bytes used, 512 total
  0x10,                                 ///< 1   SPD Revision 1.0
  0x13,                                 ///< 2   DRAM Type: LPDDR5 SDRAM
  0x0E,                                 ///< 3   Module Type: Not Hybrid (DRAM only) / Non-DIMM Solution (on-board DRAM)
  0x15,                                 ///< 4   8 Banks, no bank groups, 8 Gb SDRAM density
  0x22,                                 ///< 5   15 Rows, 11 Columns
  0xF9,                                 ///< 6   Non-Monolithic DRAM Device, 2 die, 2 Channels per package, Signal Loading Matrix 1
  0x08,                                 ///< 7   SDRAM Optional Features: tMAW = 8192 * tREFI, Unlimited MAC
  0x00,                                 ///< 8   SDRAM Thermal / Refresh options: Reserved
  0x40,                                 ///< 9   Other SDRAM Optional Features: Post package repair supported, one row per bank group, Soft PPR not supported
  0x00,                                 ///< 10  Reserved
  0x00,                                 ///< 11  Module Nominal Voltage: Reserved
  0x09,                                 ///< 12  Module Organization: 1 Rank, x16 Device Width per Channel
  0x01,                                 ///< 13  Module Memory Bus width: 1 Channels, 16 bits channel width, no ECC
  0x00,                                 ///< 14  Module Thermal Sensor: none
  0x00,                                 ///< 15  Extended Module Type: Reserved
  0x51,                                 ///< 16  Signal Loading: Data/Strobe/Mask: 2 loads, CAC: 2 loads, CS: 1 load
  0x00,                                 ///< 17  MTB = 0.125ns, FTB = 1 ps
  0x0A,                                 ///< 18  tCKAVGmin = 1.25 ns (LPDDR5-6400)
  0xFF,                                 ///< 19  tCKAVGmax = 32.002 ns
  0x92,                                 ///< 20  CAS Latencies supported (First Byte) : 14, 10, 6
  0x55,                                 ///< 21  CAS Latencies supported (Second Byte): 28, 24, 20, 16
  0x05,                                 ///< 22  CAS Latencies supported (Third Byte) : 36, 32
  0x00,                                 ///< 23  CAS Latencies supported (Fourth Byte):
  0xB4,                                 ///< 24  Minimum CAS Latency (tAAmin) = 21.25 ns
  0x00,                                 ///< 25  Read and Write Latency Set options: Write Latency Set A and DBI-Read Disabled
  0x90,                                 ///< 26  Minimum RAS-to-CAS delay (tRCDmin) = 19 ns
  0xA8,                                 ///< 27  Row precharge time for all banks (tRPab) = 21 ns
  0x90,                                 ///< 28  Minimum row precharge time (tRPpb) = 18 ns
  0x90,                                 ///< 29  tRFCab = 210 ns (8 Gb)
  0x06,                                 ///< 30  tRFCab MSB
  0xC0,                                 ///< 31  tRFCpb = 120 ns (8 Gb)
  0x03,                                 ///< 32  tRFCpb MSB
  0, 0, 0, 0, 0, 0, 0,                  ///< 33 - 39
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 40 - 49
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 50 - 59
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 60 - 69
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 70 - 79
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 80 - 89
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 90 - 99
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 100 - 109
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 110 - 119
  0x00,                                 ///< 120 FTB for Row precharge time per bank (tRPpb) = 18 ns
  0x00,                                 ///< 121 FTB for Row precharge time for all banks (tRPab) = 21 ns
  0x00,                                 ///< 122 FTB for Minimum RAS-to-CAS delay (tRCDmin) = 19 ns
  0x00,                                 ///< 123 FTB for tAAmin = 21.25 ns
  0x7F,                                 ///< 124 FTB for tCKAVGmax = 32.002 ns
  0x00,                                 ///< 125 FTB for tCKAVGmin = 1.25 ns (LPDDR5-6400)
  0x00,                                 ///< 126 CRC A
  0x00,                                 ///< 127 CRC B
  0, 0,                                 ///< 128 - 129
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 130 - 139
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 140 - 149
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 150 - 159
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 160 - 169
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 170 - 179
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 180 - 189
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 190 - 199
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 200 - 209
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 210 - 219
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 220 - 229
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 230 - 239
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 240 - 249
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 250 - 259
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 260 - 269
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 270 - 279
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 280 - 289
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 290 - 299
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 300 - 309
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 310 - 319
  0x80,                                 ///< 320 Module Manufacturer ID Code, Least Significant Byte
  0xAD,                                 ///< 321 Module Manufacturer ID Code, Most Significant Byte
  0x00,                                 ///< 322 Module Manufacturing Location
  0x00,                                 ///< 323 Module Manufacturing Date Year
  0x00,                                 ///< 324 Module Manufacturing Date Week
  0x00,                                 ///< 325 Module ID: Module Serial Number
  0x00,                                 ///< 326 Module Serial Number B
  0x00,                                 ///< 327 Module Serial Number C
  0x00,                                 ///< 328 Module Serial Number D
  0x48, 0x39, 0x4A, 0x43, 0x4E,         ///< 329 - 333 Module Part Number: Unused bytes coded as ASCII Blanks (0x20)
  0x4E, 0x4E, 0x46, 0x41, 0x35,         ///< 334 - 338 Module Part Number
  0x4D, 0x4C, 0x59, 0x52, 0x2D,         ///< 339 - 343 Module Part Number
  0x4E, 0x36, 0x45, 0x20, 0x20,         ///< 344 - 348 Module Part Number
  0x00,                                 ///< 349 Module Revision Code
  0x80,                                 ///< 350 DRAM Manufacturer ID Code, Least Significant Byte
  0xAD,                                 ///< 351 DRAM Manufacturer ID Code, Most Significant Byte
  0x00,                                 ///< 352 DRAM Stepping
  0, 0, 0, 0, 0, 0, 0,                  ///< 353 - 359
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 360 - 369
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 370 - 379
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 380 - 389
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 390 - 399
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 400 - 409
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 410 - 419
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 420 - 429
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 430 - 439
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 440 - 449
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 450 - 459
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 460 - 469
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 470 - 479
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 480 - 489
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 490 - 499
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 500 - 509
  0, 0                                  ///< 510 - 511
}})}
#[-end-210726-YUNLEI0113-modify]
!endif
# end C770_SUPPORT_ENABLE
#[end-210720-STORM1100-modify]#

#[-start-210825-TAMT000002-remove]#
##[-start-210817-DABING0002-modify]#
#!if $(S77014_SUPPORT_ENABLE) == YES
#gBoardModuleTokenSpaceGuid.Yogac970Samsung16GSpdData|*|{CODE(
#{
#// LPDDR5 315F 32Gb die, DDP 2x16
#// Samsung K3LKBKB0BM-MGCP
#  1,
# {0x23,                                 ///< 0   384 SPD bytes used, 512 total
#  0x10,                                 ///< 1   SPD Revision 1.0
#  0x13,                                 ///< 2   DRAM Type: LPDDR5 SDRAM
#  0x0E,                                 ///< 3   Module Type: Not Hybrid (DRAM only) / Non-DIMM Solution (on-board DRAM)
#  0x16,                                 ///< 4   8 Banks, no bank groups, 8 Gb SDRAM density
#  0x22,                                 ///< 5   15 Rows, 11 Columns
#  0x95,                                 ///< 6   Non-Monolithic DRAM Device, 2 die, 2 Channels per package, Signal Loading Matrix 1
#  0x08,                                 ///< 7   SDRAM Optional Features: tMAW = 8192 * tREFI, Unlimited MAC
#  0x00,                                 ///< 8   SDRAM Thermal / Refresh options: Reserved
#  0x00,                                 ///< 9   Other SDRAM Optional Features: Post package repair supported, one row per bank group, Soft PPR not supported
#  0x00,                                 ///< 10  Reserved
#  0x00,                                 ///< 11  Module Nominal Voltage: Reserved
#  0x02,                                 ///< 12  Module Organization: 1 Rank, x16 Device Width per Channel
#  0x01,                                 ///< 13  Module Memory Bus width: 1 Channels, 16 bits channel width, no ECC
#  0x00,                                 ///< 14  Module Thermal Sensor: none
#  0x00,                                 ///< 15  Extended Module Type: Reserved
#  0x00,                                 ///< 16  Signal Loading: Data/Strobe/Mask: 2 loads, CAC: 2 loads, CS: 1 load
#  0x00,                                 ///< 17  MTB = 0.125ns, FTB = 1 ps
#  0x0A,                                 ///< 18  tCKAVGmin = 1.25 ns (LPDDR5-6400)
#  0xFF,                                 ///< 19  tCKAVGmax = 32.002 ns
#  0x92,                                 ///< 20  CAS Latencies supported (First Byte) : 14, 10, 6
#  0x55,                                 ///< 21  CAS Latencies supported (Second Byte): 28, 24, 20, 16
#  0x05,                                 ///< 22  CAS Latencies supported (Third Byte) : 36, 32
#  0x00,                                 ///< 23  CAS Latencies supported (Fourth Byte):
#  0xAA,                                 ///< 24  Minimum CAS Latency (tAAmin) = 21.25 ns
#  0x00,                                 ///< 25  Read and Write Latency Set options: Write Latency Set A and DBI-Read Disabled
#  0x90,                                 ///< 26  Minimum RAS-to-CAS delay (tRCDmin) = 19 ns
#  0xA8,                                 ///< 27  Row precharge time for all banks (tRPab) = 21 ns
#  0x90,                                 ///< 28  Minimum row precharge time (tRPpb) = 18 ns
#  0xC0,                                 ///< 29  tRFCab = 210 ns (8 Gb)
#  0x08,                                 ///< 30  tRFCab MSB
#  0x60,                                 ///< 31  tRFCpb = 120 ns (8 Gb)
#  0x04,                                 ///< 32  tRFCpb MSB
#  0, 0, 0, 0, 0, 0, 0,                  ///< 33 - 39
#  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 40 - 49
#  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 50 - 59
#  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 60 - 69
#  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 70 - 79
#  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 80 - 89
#  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 90 - 99
#  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 100 - 109
#  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 110 - 119
#  0x00,                                 ///< 120 FTB for Row precharge time per bank (tRPpb) = 18 ns
#  0x00,                                 ///< 121 FTB for Row precharge time for all banks (tRPab) = 21 ns
#  0x00,                                 ///< 122 FTB for Minimum RAS-to-CAS delay (tRCDmin) = 19 ns
#  0x00,                                 ///< 123 FTB for tAAmin = 21.25 ns
#  0x7F,                                 ///< 124 FTB for tCKAVGmax = 32.002 ns
#  0x00,                                 ///< 125 FTB for tCKAVGmin = 1.25 ns (LPDDR5-6400)
#  0x60,                                 ///< 126 CRC A
#  0x3F,                                 ///< 127 CRC B
#  0, 0,                                 ///< 128 - 129
#  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 130 - 139
#  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 140 - 149
#  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 150 - 159
#  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 160 - 169
#  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 170 - 179
#  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 180 - 189
#  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 190 - 199
#  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 200 - 209
#  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 210 - 219
#  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 220 - 229
#  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 230 - 239
#  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 240 - 249
#  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 250 - 259
#  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 260 - 269
#  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 270 - 279
#  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 280 - 289
#  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 290 - 299
#  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 300 - 309
#  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 310 - 319
#  0x80,                                 ///< 320 Module Manufacturer ID Code, Least Significant Byte
#  0xCE,                                 ///< 321 Module Manufacturer ID Code, Most Significant Byte
#  0x00,                                 ///< 322 Module Manufacturing Location
#  0x00,                                 ///< 323 Module Manufacturing Date Year
#  0x00,                                 ///< 324 Module Manufacturing Date Week
#  0x00,                                 ///< 325 Module ID: Module Serial Number
#  0x00,                                 ///< 326 Module Serial Number B
#  0x00,                                 ///< 327 Module Serial Number C
#  0x00,                                 ///< 328 Module Serial Number D
#  0x4B, 0x33, 0x4C, 0x4B, 0x42,         ///< 329 - 333 Module Part Number: Unused bytes coded as ASCII Blanks (0x20)
#  0x4B, 0x42, 0x30, 0x42, 0x4D,         ///< 334 - 338 Module Part Number
#  0x2D, 0x4D, 0x47, 0x43, 0x50,         ///< 339 - 343 Module Part Number
#  0x00, 0x00, 0x00, 0x00, 0x00,         ///< 344 - 348 Module Part Number
#  0x00,                                 ///< 349 Module Revision Code
#  0x80,                                 ///< 350 DRAM Manufacturer ID Code, Least Significant Byte
#  0xCE,                                 ///< 351 DRAM Manufacturer ID Code, Most Significant Byte
#  0x00,                                 ///< 352 DRAM Stepping
#  0, 0, 0, 0, 0, 0, 0,                  ///< 353 - 359
#  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 360 - 369
#  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 370 - 379
#  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 380 - 389
#  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 390 - 399
# 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 400 - 409
#  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 410 - 419
#  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 420 - 429
#  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 430 - 439
#  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 440 - 449
#  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 450 - 459
#  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 460 - 469
#  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 470 - 479
#  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 480 - 489
#  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 490 - 499
#  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 500 - 509
#  0, 0                                  ///< 510 - 511
#}})}
#!endif
##[-end-210817-DABING0002-modify]#
#[-end-210825-TAMT000002-remove]#
	
gBoardModuleTokenSpaceGuid.VpdPcdMrcSpdData|*|{CODE(
{
// LPDDR5 315b 8Gb die, DDP 2x16
// Micron MT62F512M32D2-031
// 6400, ??-??-??-??
// 8 Banks, no bank groups, 8Gb SDRAM density
// 15 Row bits, 11 Column bits
// Non-Monolithic DRAM Device, 2 dies, 2 Channels per package,
// mLpddr5xDdp8Gb315b1r6400Spd
  1,
 {0x23,                                 ///< 0   384 SPD bytes used, 512 total
  0x10,                                 ///< 1   SPD Revision 1.0
  0x13,                                 ///< 2   DRAM Type: LPDDR5 SDRAM
  0x0E,                                 ///< 3   Module Type: Not Hybrid (DRAM only) / Non-DIMM Solution (on-board DRAM)
  0x15,                                 ///< 4   8 Banks, no bank groups, 8 Gb SDRAM density
  0x1A,                                 ///< 5   15 Rows, 11 Columns
  0x95,                                 ///< 6   Non-Monolithic DRAM Device, 2 die, 2 Channels per package, Signal Loading Matrix 1
  0x08,                                 ///< 7   SDRAM Optional Features: tMAW = 8192 * tREFI, Unlimited MAC
  0x00,                                 ///< 8   SDRAM Thermal / Refresh options: Reserved
  0x40,                                 ///< 9   Other SDRAM Optional Features: Post package repair supported, one row per bank group, Soft PPR not supported
  0x00,                                 ///< 10  Reserved
  0x00,                                 ///< 11  Module Nominal Voltage: Reserved
  0x02,                                 ///< 12  Module Organization: 1 Rank, x16 Device Width per Channel
  0x01,                                 ///< 13  Module Memory Bus width: 1 Channels, 16 bits channel width, no ECC
  0x00,                                 ///< 14  Module Thermal Sensor: none
  0x00,                                 ///< 15  Extended Module Type: Reserved
  0x48,                                 ///< 16  Signal Loading: Data/Strobe/Mask: 2 loads, CAC: 2 loads, CS: 1 load
  0x00,                                 ///< 17  MTB = 0.125ns, FTB = 1 ps
  0x0A,                                 ///< 18  tCKAVGmin = 1.25 ns (LPDDR5-6400)
  0xFF,                                 ///< 19  tCKAVGmax = 32.002 ns
  0x92,                                 ///< 20  CAS Latencies supported (First Byte) : 14, 10, 6
  0x55,                                 ///< 21  CAS Latencies supported (Second Byte): 28, 24, 20, 16
  0x05,                                 ///< 22  CAS Latencies supported (Third Byte) : 36, 32
  0x00,                                 ///< 23  CAS Latencies supported (Fourth Byte):
  0xAA,                                 ///< 24  Minimum CAS Latency (tAAmin) = 21.25 ns
  0x00,                                 ///< 25  Read and Write Latency Set options: Write Latency Set A and DBI-Read Disabled
  0x90,                                 ///< 26  Minimum RAS-to-CAS delay (tRCDmin) = 18 ns
  0xA8,                                 ///< 27  Row precharge time for all banks (tRPab) = 21 ns
  0x90,                                 ///< 28  Minimum row precharge time (tRPpb) = 18 ns
  0x90,                                 ///< 29  tRFCab = 210 ns (8 Gb)
  0x06,                                 ///< 30  tRFCab MSB
  0xC0,                                 ///< 31  tRFCpb = 120 ns (8 Gb)
  0x03,                                 ///< 32  tRFCpb MSB
  0, 0, 0, 0, 0, 0, 0,                  ///< 33 - 39
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 40 - 49
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 50 - 59
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 60 - 69
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 70 - 79
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 80 - 89
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 90 - 99
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 100 - 109
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 110 - 119
  0x00,                                 ///< 120 FTB for Row precharge time per bank (tRPpb) = 18 ns
  0x00,                                 ///< 121 FTB for Row precharge time for all banks (tRPab) = 21 ns
  0x00,                                 ///< 122 FTB for Minimum RAS-to-CAS delay (tRCDmin) = 18 ns
  0x00,                                 ///< 123 FTB for tAAmin = 21.25 ns
  0x7F,                                 ///< 124 FTB for tCKAVGmax = 32.002 ns
  0x00,                                 ///< 125 FTB for tCKAVGmin = 1.25 ns (LPDDR5-6400)
  0x00,                                 ///< 126 CRC A
  0x00,                                 ///< 127 CRC B
  0, 0,                                 ///< 128 - 129
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 130 - 139
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 140 - 149
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 150 - 159
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 160 - 169
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 170 - 179
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 180 - 189
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 190 - 199
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 200 - 209
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 210 - 219
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 220 - 229
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 230 - 239
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 240 - 249
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 250 - 259
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 260 - 269
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 270 - 279
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 280 - 289
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 290 - 299
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 300 - 309
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 310 - 319
  0x00,                                 ///< 320 Module Manufacturer ID Code, Least Significant Byte
  0x00,                                 ///< 321 Module Manufacturer ID Code, Most Significant Byte
  0x00,                                 ///< 322 Module Manufacturing Location
  0x00,                                 ///< 323 Module Manufacturing Date Year
  0x00,                                 ///< 324 Module Manufacturing Date Week
  0x20,                                 ///< 325 Module ID: Module Serial Number
  0x00,                                 ///< 326 Module Serial Number B
  0x00,                                 ///< 327 Module Serial Number C
  0x00,                                 ///< 328 Module Serial Number D
  0x20, 0x20, 0x20, 0x20, 0x20,         ///< 329 - 333 Module Part Number: Unused bytes coded as ASCII Blanks (0x20)
  0x20, 0x20, 0x20, 0x20, 0x20,         ///< 334 - 338 Module Part Number
  0x20, 0x20, 0x20, 0x20, 0x20,         ///< 339 - 343 Module Part Number
  0x20, 0x20, 0x20, 0x20, 0x20,         ///< 344 - 348 Module Part Number
  0x00,                                 ///< 349 Module Revision Code
  0x00,                                 ///< 350 DRAM Manufacturer ID Code, Least Significant Byte
  0x00,                                 ///< 351 DRAM Manufacturer ID Code, Most Significant Byte
  0x00,                                 ///< 352 DRAM Stepping
  0, 0, 0, 0, 0, 0, 0,                  ///< 353 - 359
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 360 - 369
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 370 - 379
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 380 - 389
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 390 - 399
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 400 - 409
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 410 - 419
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 420 - 429
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 430 - 439
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 440 - 449
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 450 - 459
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 460 - 469
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 470 - 479
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 480 - 489
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 490 - 499
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 500 - 509
  0, 0                                  ///< 510 - 511
}})}

[PcdsDynamicExVpd.common.SkuIdAdlPLp4Bep]
gBoardModuleTokenSpaceGuid.VpdPcdMrcSpdData|*|{CODE(
{
// LPDDR4X 200b 8Gb die, 4 die, 2 Channels per package
// MT53E512M32D2NP-046
// 4267
  1,
 {0x23,                                 ///< 0 384 SPD bytes used, 512 total
  0x11,                                 ///< 1 SPD Revision 1.1
  0x11,                                 ///< 2 DRAM Type: LPDDR4X SDRAM
  0x0E,                                 ///< 3 Module Type: Not Hybrid (DRAM only) / Non-DIMM Solution (on-board DRAM)
  0x15,                                 ///< 4 8 Banks, no bank groups, 8 Gb SDRAM density
  0x19,                                 ///< 5 16 Rows, 10 Columns
  0x95,                                 ///< 6 Non-Monolithic DRAM Device, 4 die, 2 Channels per package, Signal Loading Matrix 1
  0x08,                                 ///< 7 SDRAM Optional Features: tMAW = 8192 * tREFI, Unlimited MAC
  0x00,                                 ///< 8 SDRAM Thermal / Refresh options: Reserved
  0x40,                                 ///< 9 Other SDRAM Optional Features: Post package repair supported, one row per bank group, Soft PPR not supported
  0x00,                                 ///< 10 Reserved
  0x00,                                 ///< 11 Module Nominal Voltage: Reserved
  0x02,                                 ///< 12 Module Organization: 2 Ranks, x16 Device Width per Channel
  0x21,                                 ///< 13 Module Memory Bus width: 2 Channels, 16 bits channel width, no ECC
  0x00,                                 ///< 14 Module Thermal Sensor: none
  0x00,                                 ///< 15 Extended Module Type: Reserved
  0x48,                                 ///< 16 Signal Loading: Data/Strobe/Mask: 2 loads, CAC: 2 loads, CS: 1 load
  0x00,                                 ///< 17 MTB = 0.125ns, FTB = 1 ps
  0x04,                                 ///< 18  tCKAVGmin = 0.469ns (LPDDR4-4267)
  0xFF,                                 ///< 19  tCKAVGmax = 32.002 ns
  0x92,                                 ///< 20  CAS Latencies supported (First Byte) : 14, 10, 6
  0x55,                                 ///< 21  CAS Latencies supported (Second Byte): 28, 24, 20, 16
  0x00,                                 ///< 22  CAS Latencies supported (Third Byte) :
  0x00,                                 ///< 23 CAS Latencies supported (Fourth Byte):
  0x8C,                                 ///< 24  Minimum CAS Latency (tAAmin) = 17.5 ns
  0x00,                                 ///< 25 Read and Write Latency Set options: Write Latency Set A and DBI-Read Disabled
  0x90,                                 ///< 26 Minimum RAS-to-CAS delay (tRCDmin) = 18 ns
  0xA8,                                 ///< 27 Row precharge time for all banks (tRPab) = 21 ns
  0x90,                                 ///< 28 Minimum row precharge time (tRPmin) = 18 ns
  0xC0,                                 ///< 29 tRFCab = 280 ns (8 Gb dual-channel die)
  0x08,                                 ///< 30 tRFCab MSB
  0x60,                                 ///< 31 tRFCpb = 140 ns (8 Gb dual-channel die)
  0x04,                                 ///< 32 tRFCpb MSB
  0, 0, 0, 0, 0, 0, 0,                  ///< 33 - 39
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 40 - 49
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 50 - 59
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 60 - 69
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 70 - 79
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 80 - 89
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 90 - 99
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 100 - 109
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 110 - 119
  0x00,                                 ///< 120 FTB for Row precharge time per bank (tRPpb) = 18 ns
  0x00,                                 ///< 121 FTB for Row precharge time for all banks (tRPab) = 21 ns
  0x00,                                 ///< 122 FTB for Minimum RAS-to-CAS delay (tRCDmin) = 18 ns
  0x00,                                 ///< 123 FTB for tAAmin = 17.5 ns
  0x7F,                                 ///< 124 FTB for tCKAVGmax = 32.002 ns
  0xE1,                                 ///< 125 FTB for tCKAVGmin = 0.469 ns (LPDDR4-4267)
  0x00,                                 ///< 126 CRC A
  0x00,                                 ///< 127 CRC B
  0, 0,                                 ///< 128 - 129
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 130 - 139
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 140 - 149
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 150 - 159
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 160 - 169
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 170 - 179
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 180 - 189
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 190 - 199
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 200 - 209
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 210 - 219
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 220 - 229
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 230 - 239
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 240 - 249
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 250 - 259
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 260 - 269
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 270 - 279
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 280 - 289
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 290 - 299
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 300 - 309
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 310 - 319
  0x00,                                 ///< 320 Module Manufacturer ID Code, Least Significant Byte
  0x00,                                 ///< 321 Module Manufacturer ID Code, Most Significant Byte
  0x00,                                 ///< 322 Module Manufacturing Location
  0x00,                                 ///< 323 Module Manufacturing Date Year
  0x00,                                 ///< 324 Module Manufacturing Date Week
  0x55,                                 ///< 325 Module Serial Number A
  0x00,                                 ///< 326 Module Serial Number B
  0x00,                                 ///< 327 Module Serial Number C
  0x00,                                 ///< 328 Module Serial Number D
  0x20, 0x20, 0x20, 0x20, 0x20,         ///< 329 - 333 Module Part Number: Unused bytes coded as ASCII Blanks (0x20)
  0x20, 0x20, 0x20, 0x20, 0x20,         ///< 334 - 338 Module Part Number
  0x20, 0x20, 0x20, 0x20, 0x20,         ///< 339 - 343 Module Part Number
  0x20, 0x20, 0x20, 0x20, 0x20,         ///< 344 - 348 Module Part Number
  0x00,                                 ///< 349 Module Revision Code
  0x00,                                 ///< 350 DRAM Manufacturer ID Code, Least Significant Byte
  0x00,                                 ///< 351 DRAM Manufacturer ID Code, Most Significant Byte
  0x00,                                 ///< 352 DRAM Stepping
  0, 0, 0, 0, 0, 0, 0,                  ///< 353 - 359
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 360 - 369
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 370 - 379
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 380 - 389
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 390 - 399
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 400 - 409
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 410 - 419
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 420 - 429
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 430 - 439
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 440 - 449
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 450 - 459
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 460 - 469
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 470 - 479
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 480 - 489
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 490 - 499
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 500 - 509
  0, 0                                  ///< 510 - 511
}})}

[PcdsDynamicExVpd.common.SkuIdAdlPLp5Aep]
gBoardModuleTokenSpaceGuid.VpdPcdMrcSpdData|*|{CODE(
{
// LPDDR5 315b 8Gb die, QDP 2x16
// Micron MT62F1G32D4-031
// 6400, ??-??-??-??
// 8 Banks, no bank groups, 8Gb SDRAM density
// 15 Row bits, 11 Column bits
// Non-Monolithic DRAM Device, 4 dies, 2 Channels per package
  1,
 {0x23,                                 ///< 0   384 SPD bytes used, 512 total
  0x10,                                 ///< 1   SPD Revision 1.0
  0x13,                                 ///< 2   DRAM Type: LPDDR5 SDRAM
  0x0E,                                 ///< 3   Module Type: Not Hybrid (DRAM only) / Non-DIMM Solution (on-board DRAM)
  0x15,                                 ///< 4   8 Banks, no bank groups, 8 Gb SDRAM density
  0x1A,                                 ///< 5   15 Rows, 11 Columns
  0xB5,                                 ///< 6   Non-Monolithic DRAM Device, 4 die, 2 Channels per package, Signal Loading Matrix 1
  0x08,                                 ///< 7   SDRAM Optional Features: tMAW = 8192 * tREFI, Unlimited MAC
  0x00,                                 ///< 8   SDRAM Thermal / Refresh options: Reserved
  0x40,                                 ///< 9   Other SDRAM Optional Features: Post package repair supported, one row per bank group, Soft PPR not supported
  0x00,                                 ///< 10  Reserved
  0x00,                                 ///< 11  Module Nominal Voltage: Reserved
  0x0A,                                 ///< 12  Module Organization: 2 Ranks, x16 Device Width per Channel
  0x01,                                 ///< 13  Module Memory Bus width: 1 Channels, 16 bits channel width, no ECC
  0x00,                                 ///< 14  Module Thermal Sensor: none
  0x00,                                 ///< 15  Extended Module Type: Reserved
  0x48,                                 ///< 16  Signal Loading: Data/Strobe/Mask: 2 loads, CAC: 2 loads, CS: 1 load
  0x00,                                 ///< 17  MTB = 0.125ns, FTB = 1 ps
  0x0A,                                 ///< 18  tCKAVGmin = 1.25 ns (LPDDR5-6400)
  0xFF,                                 ///< 19  tCKAVGmax = 32.002 ns
  0x92,                                 ///< 20  CAS Latencies supported (First Byte) : 14, 10, 6
  0x55,                                 ///< 21  CAS Latencies supported (Second Byte): 28, 24, 20, 16
  0x05,                                 ///< 22  CAS Latencies supported (Third Byte) : 36, 32
  0x00,                                 ///< 23  CAS Latencies supported (Fourth Byte):
  0xAA,                                 ///< 24  Minimum CAS Latency (tAAmin) = 21.25 ns
  0x00,                                 ///< 25  Read and Write Latency Set options: Write Latency Set A and DBI-Read Disabled
  0x90,                                 ///< 26  Minimum RAS-to-CAS delay (tRCDmin) = 18 ns
  0xA8,                                 ///< 27  Row precharge time for all banks (tRPab) = 21 ns
  0x90,                                 ///< 28  Minimum row precharge time (tRPpb) = 18 ns
  0x90,                                 ///< 29  tRFCab = 210 ns (8 Gb)
  0x06,                                 ///< 30  tRFCab MSB
  0xC0,                                 ///< 31  tRFCpb = 120 ns (8 Gb)
  0x03,                                 ///< 32  tRFCpb MSB
  0, 0, 0, 0, 0, 0, 0,                  ///< 33 - 39
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 40 - 49
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 50 - 59
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 60 - 69
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 70 - 79
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 80 - 89
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 90 - 99
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 100 - 109
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 110 - 119
  0x00,                                 ///< 120 FTB for Row precharge time per bank (tRPpb) = 18 ns
  0x00,                                 ///< 121 FTB for Row precharge time for all banks (tRPab) = 21 ns
  0x00,                                 ///< 122 FTB for Minimum RAS-to-CAS delay (tRCDmin) = 18 ns
  0x00,                                 ///< 123 FTB for tAAmin = 21.25 ns
  0x7F,                                 ///< 124 FTB for tCKAVGmax = 32.002 ns
  0x00,                                 ///< 125 FTB for tCKAVGmin = 1.25 ns (LPDDR5-6400)
  0x00,                                 ///< 126 CRC A
  0x00,                                 ///< 127 CRC B
  0, 0,                                 ///< 128 - 129
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 130 - 139
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 140 - 149
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 150 - 159
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 160 - 169
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 170 - 179
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 180 - 189
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 190 - 199
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 200 - 209
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 210 - 219
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 220 - 229
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 230 - 239
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 240 - 249
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 250 - 259
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 260 - 269
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 270 - 279
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 280 - 289
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 290 - 299
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 300 - 309
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 310 - 319
  0x00,                                 ///< 320 Module Manufacturer ID Code, Least Significant Byte
  0x00,                                 ///< 321 Module Manufacturer ID Code, Most Significant Byte
  0x00,                                 ///< 322 Module Manufacturing Location
  0x00,                                 ///< 323 Module Manufacturing Date Year
  0x00,                                 ///< 324 Module Manufacturing Date Week
  0x20,                                 ///< 325 Module ID: Module Serial Number
  0x00,                                 ///< 326 Module Serial Number B
  0x00,                                 ///< 327 Module Serial Number C
  0x00,                                 ///< 328 Module Serial Number D
  0x20, 0x20, 0x20, 0x20, 0x20,         ///< 329 - 333 Module Part Number: Unused bytes coded as ASCII Blanks (0x20)
  0x20, 0x20, 0x20, 0x20, 0x20,         ///< 334 - 338 Module Part Number
  0x20, 0x20, 0x20, 0x20, 0x20,         ///< 339 - 343 Module Part Number
  0x20, 0x20, 0x20, 0x20, 0x20,         ///< 344 - 348 Module Part Number
  0x00,                                 ///< 349 Module Revision Code
  0x00,                                 ///< 350 DRAM Manufacturer ID Code, Least Significant Byte
  0x00,                                 ///< 351 DRAM Manufacturer ID Code, Most Significant Byte
  0x00,                                 ///< 352 DRAM Stepping
  0, 0, 0, 0, 0, 0, 0,                  ///< 353 - 359
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 360 - 369
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 370 - 379
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 380 - 389
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 390 - 399
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 400 - 409
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 410 - 419
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 420 - 429
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 430 - 439
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 440 - 449
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 450 - 459
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 460 - 469
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 470 - 479
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 480 - 489
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 490 - 499
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 500 - 509
  0, 0                                  ///< 510 - 511
}})}

[PcdsDynamicExVpd.common.SkuIdAdlPLp5Gcs]
gBoardModuleTokenSpaceGuid.VpdPcdMrcSpdData|*|{CODE(
{
// LPDDR5 315b 8Gb die, QDP 2x16
// Micron MT62F1G32D4-031
// 6400, ??-??-??-??
// 8 Banks, no bank groups, 8Gb SDRAM density
// 15 Row bits, 11 Column bits
// Non-Monolithic DRAM Device, 4 dies, 2 Channels per package,
  1,
 {0x23,                                 ///< 0   384 SPD bytes used, 512 total
  0x10,                                 ///< 1   SPD Revision 1.0
  0x13,                                 ///< 2   DRAM Type: LPDDR5 SDRAM
  0x0E,                                 ///< 3   Module Type: Not Hybrid (DRAM only) / Non-DIMM Solution (on-board DRAM)
  0x15,                                 ///< 4   8 Banks, no bank groups, 8 Gb SDRAM density
  0x1A,                                 ///< 5   15 Rows, 11 Columns
  0xB5,                                 ///< 6   Non-Monolithic DRAM Device, 4 die, 2 Channels per package, Signal Loading Matrix 1
  0x08,                                 ///< 7   SDRAM Optional Features: tMAW = 8192 * tREFI, Unlimited MAC
  0x00,                                 ///< 8   SDRAM Thermal / Refresh options: Reserved
  0x40,                                 ///< 9   Other SDRAM Optional Features: Post package repair supported, one row per bank group, Soft PPR not supported
  0x00,                                 ///< 10  Reserved
  0x00,                                 ///< 11  Module Nominal Voltage: Reserved
  0x0A,                                 ///< 12  Module Organization: 2 Ranks, x16 Device Width per Channel
  0x01,                                 ///< 13  Module Memory Bus width: 1 Channels, 16 bits channel width, no ECC
  0x00,                                 ///< 14  Module Thermal Sensor: none
  0x00,                                 ///< 15  Extended Module Type: Reserved
  0x48,                                 ///< 16  Signal Loading: Data/Strobe/Mask: 2 loads, CAC: 2 loads, CS: 1 load
  0x00,                                 ///< 17  MTB = 0.125ns, FTB = 1 ps
  0x0A,                                 ///< 18  tCKAVGmin = 1.25 ns (LPDDR5-6400)
  0xFF,                                 ///< 19  tCKAVGmax = 32.002 ns
  0x92,                                 ///< 20  CAS Latencies supported (First Byte) : 14, 10, 6
  0x55,                                 ///< 21  CAS Latencies supported (Second Byte): 28, 24, 20, 16
  0x05,                                 ///< 22  CAS Latencies supported (Third Byte) : 36, 32
  0x00,                                 ///< 23  CAS Latencies supported (Fourth Byte):
  0xAA,                                 ///< 24  Minimum CAS Latency (tAAmin) = 21.25 ns
  0x00,                                 ///< 25  Read and Write Latency Set options: Write Latency Set A and DBI-Read Disabled
  0x90,                                 ///< 26  Minimum RAS-to-CAS delay (tRCDmin) = 18 ns
  0xA8,                                 ///< 27  Row precharge time for all banks (tRPab) = 21 ns
  0x90,                                 ///< 28  Minimum row precharge time (tRPpb) = 18 ns
  0x90,                                 ///< 29  tRFCab = 210 ns (8 Gb)
  0x06,                                 ///< 30  tRFCab MSB
  0xC0,                                 ///< 31  tRFCpb = 120 ns (8 Gb)
  0x03,                                 ///< 32  tRFCpb MSB
  0, 0, 0, 0, 0, 0, 0,                  ///< 33 - 39
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 40 - 49
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 50 - 59
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 60 - 69
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 70 - 79
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 80 - 89
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 90 - 99
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 100 - 109
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 110 - 119
  0x00,                                 ///< 120 FTB for Row precharge time per bank (tRPpb) = 18 ns
  0x00,                                 ///< 121 FTB for Row precharge time for all banks (tRPab) = 21 ns
  0x00,                                 ///< 122 FTB for Minimum RAS-to-CAS delay (tRCDmin) = 18 ns
  0x00,                                 ///< 123 FTB for tAAmin = 21.25 ns
  0x7F,                                 ///< 124 FTB for tCKAVGmax = 32.002 ns
  0x00,                                 ///< 125 FTB for tCKAVGmin = 1.25 ns (LPDDR5-6400)
  0x00,                                 ///< 126 CRC A
  0x00,                                 ///< 127 CRC B
  0, 0,                                 ///< 128 - 129
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 130 - 139
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 140 - 149
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 150 - 159
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 160 - 169
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 170 - 179
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 180 - 189
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 190 - 199
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 200 - 209
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 210 - 219
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 220 - 229
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 230 - 239
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 240 - 249
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 250 - 259
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 260 - 269
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 270 - 279
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 280 - 289
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 290 - 299
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 300 - 309
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 310 - 319
  0x00,                                 ///< 320 Module Manufacturer ID Code, Least Significant Byte
  0x00,                                 ///< 321 Module Manufacturer ID Code, Most Significant Byte
  0x00,                                 ///< 322 Module Manufacturing Location
  0x00,                                 ///< 323 Module Manufacturing Date Year
  0x00,                                 ///< 324 Module Manufacturing Date Week
  0x20,                                 ///< 325 Module ID: Module Serial Number
  0x00,                                 ///< 326 Module Serial Number B
  0x00,                                 ///< 327 Module Serial Number C
  0x00,                                 ///< 328 Module Serial Number D
  0x20, 0x20, 0x20, 0x20, 0x20,         ///< 329 - 333 Module Part Number: Unused bytes coded as ASCII Blanks (0x20)
  0x20, 0x20, 0x20, 0x20, 0x20,         ///< 334 - 338 Module Part Number
  0x20, 0x20, 0x20, 0x20, 0x20,         ///< 339 - 343 Module Part Number
  0x20, 0x20, 0x20, 0x20, 0x20,         ///< 344 - 348 Module Part Number
  0x00,                                 ///< 349 Module Revision Code
  0x00,                                 ///< 350 DRAM Manufacturer ID Code, Least Significant Byte
  0x00,                                 ///< 351 DRAM Manufacturer ID Code, Most Significant Byte
  0x00,                                 ///< 352 DRAM Stepping
  0, 0, 0, 0, 0, 0, 0,                  ///< 353 - 359
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 360 - 369
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 370 - 379
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 380 - 389
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 390 - 399
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 400 - 409
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 410 - 419
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 420 - 429
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 430 - 439
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 440 - 449
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 450 - 459
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 460 - 469
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 470 - 479
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 480 - 489
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 490 - 499
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 500 - 509
  0, 0                                  ///< 510 - 511
}})}

[PcdsDynamicExVpd.common.SkuIdAdlPLp5PpvRVP]
gBoardModuleTokenSpaceGuid.VpdPcdMrcSpdData|*|{CODE(
{
// LPDDR5 315b 16Gb die, DDP 2x16
// 6400, ??-??-??-??
// 8 Banks, no bank groups, 16Gb SDRAM density
// 16 Row bits, 11 Column bits
// Non-Monolithic DRAM Device, 2 dies, 2 Channels per package
// mLpddr5xDdp16Gb315b1r6400Spd
  1,
 {0x23,                                 ///< 0   384 SPD bytes used, 512 total
  0x10,                                 ///< 1   SPD Revision 1.0
  0x13,                                 ///< 2   DRAM Type: LPDDR5 SDRAM
  0x0E,                                 ///< 3   Module Type: Not Hybrid (DRAM only) / Non-DIMM Solution (on-board DRAM)
  0x16,                                 ///< 4   8 Banks, no bank groups, 16 Gb SDRAM density
  0x22,                                 ///< 5   16 Rows, 11 Columns
  0x95,                                 ///< 6   Non-Monolithic DRAM Device, 2 die, 2 Channels per package, Signal Loading Matrix 1
  0x08,                                 ///< 7   SDRAM Optional Features: tMAW = 8192 * tREFI, Unlimited MAC
  0x00,                                 ///< 8   SDRAM Thermal / Refresh options: Reserved
  0x40,                                 ///< 9   Other SDRAM Optional Features: Post package repair supported, one row per bank group, Soft PPR not supported
  0x00,                                 ///< 10  Reserved
  0x00,                                 ///< 11  Module Nominal Voltage: Reserved
  0x02,                                 ///< 12  Module Organization: 1 Rank, x16 Device Width per Channel
  0x01,                                 ///< 13  Module Memory Bus width: 1 Channels, 16 bits channel width, no ECC
  0x00,                                 ///< 14  Module Thermal Sensor: none
  0x00,                                 ///< 15  Extended Module Type: Reserved
  0x48,                                 ///< 16  Signal Loading: Data/Strobe/Mask: 2 loads, CAC: 2 loads, CS: 1 load
  0x00,                                 ///< 17  MTB = 0.125ns, FTB = 1 ps
  0x0A,                                 ///< 18  tCKAVGmin = 1.25 ns (LPDDR5-6400)
  0xFF,                                 ///< 19  tCKAVGmax = 32.002 ns
  0x92,                                 ///< 20  CAS Latencies supported (First Byte) : 14, 10, 6
  0x55,                                 ///< 21  CAS Latencies supported (Second Byte): 28, 24, 20, 16
  0x05,                                 ///< 22  CAS Latencies supported (Third Byte) : 36, 32
  0x00,                                 ///< 23  CAS Latencies supported (Fourth Byte):
  0xAA,                                 ///< 24  Minimum CAS Latency (tAAmin) = 21.25 ns
  0x00,                                 ///< 25  Read and Write Latency Set options: Write Latency Set A and DBI-Read Disabled
  0x90,                                 ///< 26  Minimum RAS-to-CAS delay (tRCDmin) = 18 ns
  0xA8,                                 ///< 27  Row precharge time for all banks (tRPab) = 21 ns
  0x90,                                 ///< 28  Minimum row precharge time (tRPpb) = 18 ns
  0xC0,                                 ///< 29  tRFCab = 280 ns (16 Gb)
  0x08,                                 ///< 30  tRFCab MSB
  0x60,                                 ///< 31  tRFCpb = 140 ns (16 Gb)
  0x04,                                 ///< 32  tRFCpb MSB
  0, 0, 0, 0, 0, 0, 0,                  ///< 33 - 39
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 40 - 49
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 50 - 59
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 60 - 69
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 70 - 79
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 80 - 89
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 90 - 99
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 100 - 109
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 110 - 119
  0x00,                                 ///< 120 FTB for Row precharge time per bank (tRPpb) = 18 ns
  0x00,                                 ///< 121 FTB for Row precharge time for all banks (tRPab) = 21 ns
  0x00,                                 ///< 122 FTB for Minimum RAS-to-CAS delay (tRCDmin) = 18 ns
  0x00,                                 ///< 123 FTB for tAAmin = 21.25 ns
  0x7F,                                 ///< 124 FTB for tCKAVGmax = 32.002 ns
  0x00,                                 ///< 125 FTB for tCKAVGmin = 1.25 ns (LPDDR5-6400)
  0x00,                                 ///< 126 CRC A
  0x00,                                 ///< 127 CRC B
  0, 0,                                 ///< 128 - 129
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 130 - 139
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 140 - 149
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 150 - 159
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 160 - 169
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 170 - 179
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 180 - 189
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 190 - 199
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 200 - 209
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 210 - 219
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 220 - 229
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 230 - 239
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 240 - 249
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 250 - 259
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 260 - 269
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 270 - 279
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 280 - 289
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 290 - 299
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 300 - 309
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 310 - 319
  0x00,                                 ///< 320 Module Manufacturer ID Code, Least Significant Byte
  0x00,                                 ///< 321 Module Manufacturer ID Code, Most Significant Byte
  0x00,                                 ///< 322 Module Manufacturing Location
  0x00,                                 ///< 323 Module Manufacturing Date Year
  0x00,                                 ///< 324 Module Manufacturing Date Week
  0x20,                                 ///< 325 Module ID: Module Serial Number
  0x00,                                 ///< 326 Module Serial Number B
  0x00,                                 ///< 327 Module Serial Number C
  0x00,                                 ///< 328 Module Serial Number D
  0x20, 0x20, 0x20, 0x20, 0x20,         ///< 329 - 333 Module Part Number: Unused bytes coded as ASCII Blanks (0x20)
  0x20, 0x20, 0x20, 0x20, 0x20,         ///< 334 - 338 Module Part Number
  0x20, 0x20, 0x20, 0x20, 0x20,         ///< 339 - 343 Module Part Number
  0x20, 0x20, 0x20, 0x20, 0x20,         ///< 344 - 348 Module Part Number
  0x00,                                 ///< 349 Module Revision Code
  0x00,                                 ///< 350 DRAM Manufacturer ID Code, Least Significant Byte
  0x00,                                 ///< 351 DRAM Manufacturer ID Code, Most Significant Byte
  0x00,                                 ///< 352 DRAM Stepping
  0, 0, 0, 0, 0, 0, 0,                  ///< 353 - 359
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 360 - 369
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 370 - 379
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 380 - 389
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 390 - 399
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 400 - 409
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 410 - 419
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 420 - 429
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 430 - 439
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 440 - 449
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 450 - 459
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 460 - 469
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 470 - 479
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 480 - 489
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 490 - 499
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,         ///< 500 - 509
  0, 0                                  ///< 510 - 511
}})}

#[-start-210910-QINGLIN0060-add]#
[PcdsDynamicExVpd.common.SkuIdAdlPDdr4Rvp]
!if ($(S370_SUPPORT_ENABLE) == YES) OR ($(S570_SUPPORT_ENABLE) == YES)
gBoardModuleTokenSpaceGuid.DDR4Samsung4GSpdData|*|{CODE(
{
 //Samsung ddr4 DRAM SA0000AC810 K4A8G165WC-BCWE
  1,
 { 
  //0----7
  0x23, 0x11, 0x0C, 0x03, 0x45, 0x21, 0x00, 0x08, 
  //8----15
  0x00, 0x60, 0x00, 0x03, 0x02, 0x03, 0x00, 0x00, 
  //16----23
  0x00, 0x00, 0x05, 0x0D, 0xF8, 0xFF, 0x02, 0x00, 
  //24----31
  0x6E, 0x6E, 0x6E, 0x11, 0x00, 0x6E, 0xF0, 0x0A, 
  //32----39
  0x20, 0x08, 0x00, 0x05, 0x00, 0xF0, 0x2B, 0x34, 
  //40----47
  0x28, 0x00, 0x78, 0x00, 0x14, 0x3C, 0x00, 0x00, 
  //48----55
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //56----63
  0x00, 0x00, 0x00, 0x00, 0x16, 0x36, 0x0B, 0x35, 
  //64----71
  0x16, 0x36, 0x0B, 0x35, 0x00, 0x00, 0x16, 0x36, 
  //72----79
  0x0B, 0x35, 0x16, 0x36, 0x0B, 0x35, 0x00, 0x00, 
  //80----87
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //88----95
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //96----103
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //104----111
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //112----119
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x9C, 0xB5, 
  //120----127
  0x00, 0x00, 0x00, 0x00, 0xE7, 0x00, 0x14, 0x98, 
  //128----135
  0x0F, 0x11, 0x02, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //136----143
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //144----151
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //152----159
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //160----167
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //168----175
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //176----183
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //184----191
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //192----199
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //200----207
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //208----215
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //216----223
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //224----231
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //232----239
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //240----247
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //248----255
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xDB, 0x08, 
  //256----263
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //264----271
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //272----279
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //280----287
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //288----295
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //296----303
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //304----311
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //312----319
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //320----327
  0x80, 0xCE, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //328----335
  0x00, 0x4D, 0x34, 0x37, 0x31, 0x41, 0x35, 0x32, 
  //336----343
  0x34, 0x34, 0x43, 0x42, 0x30, 0x2D, 0x43, 0x57, 
  //344----351
  0x45, 0x20, 0x20, 0x20, 0x20, 0x00, 0x80, 0xCE, 
  //352----359
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //360----367
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //368----375
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //376----383
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //384----391
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //392----399
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //400----407
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //408----415
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //416----423
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //424----431
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //432----439
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //440----447
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //448----455
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //456----463
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //464----471
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //472----479
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //480----487
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //488----495
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //496----503
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //504----511
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 
}})}

gBoardModuleTokenSpaceGuid.DDR4Samsung8GSpdData|*|{CODE(
{
 //Samsung ddr4 DRAM SA0000C6N10 K4AAG165WB-BCWE
  1,
 { 
  //0----7
  0x23, 0x11, 0x0C, 0x03, 0x46, 0x29, 0x00, 0x08, 
  //8----15
  0x00, 0x60, 0x00, 0x03, 0x02, 0x03, 0x00, 0x00, 
  //16----23
  0x00, 0x00, 0x05, 0x0D, 0xF8, 0xFF, 0x02, 0x00, 
  //24----31
  0x6E, 0x6E, 0x6E, 0x11, 0x00, 0x6E, 0x30, 0x11, 
  //32----39
  0xF0, 0x0A, 0x20, 0x08, 0x00, 0xF0, 0x2B, 0x34, 
  //40----47
  0x28, 0x00, 0x78, 0x00, 0x14, 0x3C, 0x00, 0x00, 
  //48----55
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //56----63
  0x00, 0x00, 0x00, 0x00, 0x16, 0x36, 0x0B, 0x35, 
  //64----71
  0x16, 0x36, 0x0B, 0x35, 0x00, 0x00, 0x16, 0x36, 
  //72----79
  0x0B, 0x35, 0x16, 0x36, 0x0B, 0x35, 0x00, 0x00, 
  //80----87
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //88----95
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //96----103
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //104----111
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //112----119
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x9C, 0xB5, 
  //120----127
  0x00, 0x00, 0x00, 0x00, 0xE7, 0x00, 0xDE, 0xDE, 
  //128----135
  0x0F, 0x11, 0x02, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //136----143
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //144----151
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //152----159
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //160----167
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //168----175
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //176----183
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //184----191
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //192----199
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //200----207
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //208----215
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //216----223
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //224----231
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //232----239
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //240----247
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //248----255
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xDB, 0x08, 
  //256----263
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //264----271
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //272----279
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //280----287
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //288----295
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //296----303
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //304----311
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //312----319
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //320----327
  0x80, 0xCE, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //328----335
  0x00, 0x4D, 0x34, 0x37, 0x31, 0x41, 0x31, 0x47, 
  //336----343
  0x34, 0x34, 0x42, 0x42, 0x30, 0x2D, 0x43, 0x57, 
  //344----351
  0x45, 0x20, 0x20, 0x20, 0x20, 0x00, 0x80, 0xCE, 
  //352----359
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //360----367
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //368----375
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //376----383
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //384----391
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //392----399
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //400----407
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //408----415
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //416----423
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //424----431
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //432----439
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //440----447
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //448----455
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //456----463
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //464----471
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //472----479
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //480----487
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //488----495
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //496----503
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //504----511
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 
}})}

gBoardModuleTokenSpaceGuid.DDR4Hynix4GSpdData|*|{CODE(
{
 //Hynix ddr4 DRAM SA0000AT510 H5AN8G6NDJR-XNC
  1,
 { 
  //0----7
  0x23, 0x11, 0x0C, 0x03, 0x45, 0x21, 0x00, 0x08, 
  //8----15
  0x00, 0x60, 0x00, 0x03, 0x02, 0x03, 0x00, 0x00, 
  //16----23
  0x00, 0x00, 0x05, 0x0D, 0xF8, 0xFF, 0x02, 0x00, 
  //24----31
  0x6E, 0x6E, 0x6E, 0x11, 0x00, 0x6E, 0xF0, 0x0A, 
  //32----39
  0x20, 0x08, 0x00, 0x05, 0x00, 0xF0, 0x2B, 0x34, 
  //40----47
  0x28, 0x00, 0x78, 0x00, 0x14, 0x3C, 0x00, 0x00, 
  //48----55
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //56----63
  0x00, 0x00, 0x00, 0x00, 0x16, 0x36, 0x0B, 0x35, 
  //64----71
  0x16, 0x36, 0x0B, 0x35, 0x00, 0x00, 0x16, 0x36, 
  //72----79
  0x0B, 0x35, 0x16, 0x36, 0x0B, 0x35, 0x00, 0x00, 
  //80----87
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //88----95
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //96----103
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //104----111
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //112----119
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x9C, 0xB4, 
  //120----127
  0x00, 0x00, 0x00, 0x00, 0xE7, 0x00, 0x75, 0x20, 
  //128----135
  0x0F, 0x01, 0x02, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //136----143
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //144----151
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //152----159
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //160----167
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //168----175
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //176----183
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //184----191
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //192----199
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //200----207
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //208----215
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //216----223
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //224----231
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //232----239
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //240----247
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //248----255
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xC0, 0xE2, 
  //256----263
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //264----271
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //272----279
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //280----287
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //288----295
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //296----303
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //304----311
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //312----319
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //320----327
  0x80, 0xAD, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //328----335
  0x00, 0x48, 0x4D, 0x41, 0x38, 0x35, 0x31, 0x53, 
  //336----343
  0x36, 0x44, 0x4A, 0x52, 0x36, 0x4E, 0x2D, 0x58, 
  //344----351
  0x4E, 0x20, 0x20, 0x20, 0x20, 0x00, 0x80, 0xAD, 
  //352----359
  0xFF, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //360----367
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //368----375
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //376----383
  0x00, 0x00, 0x00, 0x00, 0x00, 0xDD, 0x00, 0x00, 
  //384----391
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //392----399
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //400----407
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //408----415
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //416----423
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //424----431
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //432----439
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //440----447
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //448----455
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //456----463
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //464----471
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //472----479
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //480----487
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //488----495
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //496----503
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //504----511
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 
}})}

gBoardModuleTokenSpaceGuid.DDR4Hynix8GSpdData|*|{CODE(
{
 //Hynix ddr4 DRAM SA0000B5K10 H5ANAG6NCJR-XNC
  1,
 { 
  //0----7
  0x23, 0x12, 0x0C, 0x03, 0x46, 0x29, 0x00, 0x08, 
  //8----15
  0x00, 0x60, 0x00, 0x03, 0x02, 0x03, 0x00, 0x00, 
  //16----23
  0x00, 0x00, 0x05, 0x0D, 0xF8, 0xFF, 0x02, 0x00, 
  //24----31
  0x6E, 0x6E, 0x6E, 0x11, 0x00, 0x6E, 0xF0, 0x0A, 
  //32----39
  0x20, 0x08, 0x00, 0x05, 0x00, 0xF0, 0x2B, 0x34, 
  //40----47
  0x28, 0x00, 0x78, 0x00, 0x14, 0x3C, 0x00, 0x00, 
  //48----55
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //56----63
  0x00, 0x00, 0x00, 0x00, 0x16, 0x36, 0x0B, 0x35, 
  //64----71
  0x16, 0x36, 0x0B, 0x35, 0x00, 0x00, 0x16, 0x36, 
  //72----79
  0x0B, 0x35, 0x16, 0x36, 0x0B, 0x35, 0x00, 0x00, 
  //80----87
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //88----95
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //96----103
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //104----111
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //112----119
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x9C, 0xB5, 
  //120----127
  0x00, 0x00, 0x00, 0x00, 0xE7, 0x00, 0x9C, 0x33, 
  //128----135
  0x0F, 0x01, 0x02, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //136----143
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //144----151
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //152----159
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //160----167
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //168----175
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //176----183
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //184----191
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //192----199
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //200----207
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //208----215
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //216----223
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //224----231
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //232----239
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //240----247
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //248----255
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xC0, 0xE2, 
  //256----263
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //264----271
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //272----279
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //280----287
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //288----295
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //296----303
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //304----311
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //312----319
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //320----327
  0x80, 0xAD, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //328----335
  0x00, 0x48, 0x4D, 0x41, 0x41, 0x31, 0x47, 0x53, 
  //336----343
  0x36, 0x43, 0x4A, 0x52, 0x36, 0x4E, 0x2D, 0x58, 
  //344----351
  0x4E, 0x20, 0x20, 0x20, 0x20, 0x00, 0x80, 0xAD, 
  //352----359
  0xFF, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //360----367
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //368----375
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //376----383
  0x00, 0x00, 0x00, 0x00, 0x00, 0xDD, 0x00, 0x00, 
  //384----391
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //392----399
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //400----407
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //408----415
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //416----423
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //424----431
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //432----439
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //440----447
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //448----455
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //456----463
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //464----471
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //472----479
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //480----487
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //488----495
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //496----503
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //504----511
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 
}})}

gBoardModuleTokenSpaceGuid.DDR4Micron4GSpdData|*|{CODE(
{
 //Micron ddr4 DRAM SA0000C2700 MT40A512M16TB-062E:R
  1,
 { 
  //0----7
  0x23, 0x11, 0x0C, 0x03, 0x45, 0x21, 0x00, 0x08, 
  //8----15
  0x00, 0x60, 0x00, 0x03, 0x02, 0x03, 0x00, 0x00, 
  //16----23
  0x00, 0x00, 0x05, 0x0D, 0xF8, 0xFF, 0x2F, 0x00, 
  //24----31
  0x6E, 0x6E, 0x6E, 0x11, 0x00, 0x6E, 0xF0, 0x0A, 
  //32----39
  0x20, 0x08, 0x00, 0x05, 0x00, 0xF0, 0x2B, 0x34, 
  //40----47
  0x28, 0x00, 0x78, 0x00, 0x14, 0x3C, 0x00, 0x00, 
  //48----55
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //56----63
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //64----71
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //72----79
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //80----87
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //88----95
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //96----103
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //104----111
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //112----119
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x9C, 0xB5, 
  //120----127
  0x00, 0x00, 0x00, 0x00, 0xE7, 0x00, 0x01, 0x44, 
  //128----135
  0x00, 0x00, 0x1F, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //136----143
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //144----151
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //152----159
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //160----167
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //168----175
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //176----183
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //184----191
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //192----199
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //200----207
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //208----215
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //216----223
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //224----231
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //232----239
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //240----247
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //248----255
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x23, 0x66, 
  //256----263
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //264----271
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //272----279
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //280----287
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //288----295
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //296----303
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //304----311
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //312----319
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //320----327
  0x80, 0x2C, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //328----335
  0x00, 0x4D, 0x54, 0x34, 0x30, 0x41, 0x35, 0x31, 
  //336----343
  0x32, 0x4D, 0x31, 0x36, 0x54, 0x42, 0x2D, 0x30, 
  //344----351
  0x36, 0x32, 0x45, 0x3A, 0x52, 0x00, 0x80, 0x2C, 
  //352----359
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //360----367
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //368----375
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //376----383
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //384----391
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //392----399
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //400----407
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //408----415
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //416----423
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //424----431
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //432----439
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //440----447
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //448----455
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //456----463
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //464----471
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //472----479
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //480----487
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //488----495
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //496----503
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //504----511
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 
}})}

gBoardModuleTokenSpaceGuid.DDR4Micron8GSpdData|*|{CODE(
{
  //Micron  ddr4 DRAM SA0000A4K00 MT40A1G16RC-062E:B
  1,
 { 
  //0----7
  0x23, 0x11, 0x0C, 0x03, 0x46, 0x29, 0x00, 0x08, 
  //8----15
  0x00, 0x60, 0x00, 0x03, 0x02, 0x03, 0x00, 0x00, 
  //16----23
  0x00, 0x00, 0x05, 0x0D, 0xF8, 0xFF, 0x2F, 0x00, 
  //24----31
  0x6E, 0x6E, 0x6E, 0x11, 0x00, 0x6E, 0xF0, 0x0A, 
  //32----39
  0x20, 0x08, 0x00, 0x05, 0x00, 0xF0, 0x2B, 0x34, 
  //40----47
  0x28, 0x00, 0x78, 0x00, 0x14, 0x3C, 0x00, 0x00, 
  //48----55
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //56----63
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //64----71
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //72----79
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //80----87
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //88----95
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //96----103
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //104----111
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //112----119
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x9C, 0xB5, 
  //120----127
  0x00, 0x00, 0x00, 0x00, 0xE7, 0x00, 0x01, 0x44, 
  //128----135
  0x00, 0x00, 0x1F, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //136----143
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //144----151
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //152----159
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //160----167
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //168----175
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //176----183
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //184----191
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //192----199
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //200----207
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //208----215
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //216----223
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //224----231
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //232----239
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //240----247
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //248----255
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x23, 0x66, 
  //256----263
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //264----271
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //272----279
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //280----287
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //288----295
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //296----303
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //304----311
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //312----319
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //320----327
  0x80, 0x2C, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //328----335
  0x00, 0x4D, 0x54, 0x34, 0x30, 0x41, 0x31, 0x47, 
  //336----343
  0x31, 0x36, 0x52, 0x43, 0x2D, 0x30, 0x36, 0x32, 
  //344----351
  0x45, 0x3A, 0x42, 0x20, 0x20, 0x00, 0x80, 0x2C, 
  //352----359
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //360----367
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //368----375
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //376----383
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //384----391
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //392----399
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //400----407
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //408----415
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //416----423
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //424----431
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //432----439
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //440----447
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //448----455
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //456----463
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //464----471
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //472----479
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //480----487
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //488----495
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //496----503
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //504----511
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 
}})}

gBoardModuleTokenSpaceGuid.DDR4NullSpdData|*|{CODE(
{
  //Null
  1,
 {
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00
}})}
!endif

#[-start-211230-QINGLIN0140-modify]#
!if ($(S570_SUPPORT_ENABLE) == YES) OR ($(S370_SUPPORT_ENABLE) == YES)
#[-end-211230-QINGLIN0140-modify]#
gBoardModuleTokenSpaceGuid.DDR4Micron8G2ndSpdData|*|{CODE(
{
  //Micron ddr4 DRAM SA0000CDU00 MT40A1G16TB-062E:F
  1,
 { 
  //0----7
  0x23, 0x11, 0x0C, 0x03, 0x46, 0x29, 0x00, 0x08, 
  //8----15
  0x00, 0x60, 0x00, 0x03, 0x02, 0x03, 0x00, 0x00, 
  //16----23
  0x00, 0x00, 0x05, 0x0D, 0xF8, 0xFF, 0x2F, 0x00, 
  //24----31
  0x6E, 0x6E, 0x6E, 0x11, 0x00, 0x6E, 0xF0, 0x0A, 
  //32----39
  0x20, 0x08, 0x00, 0x05, 0x00, 0xF0, 0x2B, 0x34, 
  //40----47
  0x28, 0x00, 0x78, 0x00, 0x14, 0x3C, 0x00, 0x00, 
  //48----55
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //56----63
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //64----71
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //72----79
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //80----87
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //88----95
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //96----103
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //104----111
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //112----119
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x9C, 0xB5, 
  //120----127
  0x00, 0x00, 0x00, 0x00, 0xE7, 0x00, 0x01, 0x44, 
  //128----135
  0x00, 0x00, 0x1F, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //136----143
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //144----151
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //152----159
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //160----167
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //168----175
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //176----183
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //184----191
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //192----199
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //200----207
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //208----215
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //216----223
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //224----231
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //232----239
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //240----247
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //248----255
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x23, 0x66, 
  //256----263
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //264----271
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //272----279
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //280----287
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //288----295
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //296----303
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //304----311
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //312----319
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //320----327
  0x80, 0x2C, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //328----335
  0x00, 0x4D, 0x54, 0x34, 0x30, 0x41, 0x31, 0x47, 
  //336----343
  0x31, 0x36, 0x54, 0x42, 0x2D, 0x30, 0x36, 0x32, 
  //344----351
  0x45, 0x3A, 0x46, 0x20, 0x20, 0x00, 0x80, 0x2C, 
  //352----359
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //360----367
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //368----375
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //376----383
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //384----391
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //392----399
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //400----407
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //408----415
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //416----423
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //424----431
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //432----439
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //440----447
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //448----455
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //456----463
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //464----471
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //472----479
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //480----487
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //488----495
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //496----503
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //504----511
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 
}})}
!endif

#[-start-211228-QINGLIN0136-add]#
!if ($(S370_SUPPORT_ENABLE) == YES)
gBoardModuleTokenSpaceGuid.DDR4Smart4GSpdData|*|{CODE(
{
 //Smart ddr4 DRAM K4A8G165WC-BCWE
  1,
 { 
  //0----7
  0x23, 0x11, 0x0C, 0x03, 0x45, 0x21, 0x00, 0x08, 
  //8----15
  0x00, 0x60, 0x00, 0x03, 0x02, 0x03, 0x00, 0x00, 
  //16----23
  0x00, 0x00, 0x05, 0x0D, 0xF8, 0xFF, 0x02, 0x00, 
  //24----31
  0x6E, 0x6E, 0x6E, 0x11, 0x00, 0x6E, 0xF0, 0x0A, 
  //32----39
  0x20, 0x08, 0x00, 0x05, 0x00, 0xF0, 0x2B, 0x34, 
  //40----47
  0x28, 0x00, 0x78, 0x00, 0x14, 0x3C, 0x00, 0x00, 
  //48----55
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //56----63
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //64----71
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //72----79
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //80----87
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //88----95
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //96----103
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //104----111
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //112----119
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x9C, 0xB5, 
  //120----127
  0x00, 0x00, 0x00, 0x00, 0xE7, 0x00, 0xF4, 0xD2, 
  //128----135
  0x0F, 0x11, 0x02, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //136----143
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //144----151
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //152----159
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //160----167
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //168----175
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //176----183
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //184----191
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //192----199
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //200----207
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //208----215
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //216----223
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //224----231
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //232----239
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //240----247
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //248----255
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xDB, 0x08, 
  //256----263
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //264----271
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //272----279
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //280----287
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //288----295
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //296----303
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //304----311
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //312----319
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //320----327
  0x01, 0x94, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //328----335
  0x00, 0x53, 0x44, 0x51, 0x43, 0x38, 0x47, 0x38, 
  //336----343
  0x57, 0x31, 0x36, 0x58, 0x43, 0x57, 0x45, 0x39, 
  //344----351
  0x4E, 0x31, 0x54, 0x20, 0x20, 0x00, 0x01, 0x94, 
  //352----359
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //360----367
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //368----375
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //376----383
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //384----391
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //392----399
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //400----407
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //408----415
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //416----423
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //424----431
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //432----439
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //440----447
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //448----455
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //456----463
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //464----471
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //472----479
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //480----487
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //488----495
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //496----503
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //504----511
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 
}})}

gBoardModuleTokenSpaceGuid.DDR4Adata4GSpdData|*|{CODE(
{
 //Adata ddr4 DRAM ADS4021HY
  1,
 { 
  //0----7
  0x23, 0x11, 0x0C, 0x03, 0x45, 0x21, 0x00, 0x08, 
  //8----15
  0x00, 0x60, 0x00, 0x03, 0x02, 0x03, 0x00, 0x00, 
  //16----23
  0x00, 0x00, 0x05, 0x0D, 0xF8, 0xFF, 0x02, 0x00, 
  //24----31
  0x6E, 0x6E, 0x6E, 0x11, 0x00, 0x6E, 0xF0, 0x0A, 
  //32----39
  0x20, 0x08, 0x00, 0x05, 0x00, 0xF0, 0x2B, 0x34, 
  //40----47
  0x28, 0x00, 0x78, 0x00, 0x14, 0x3C, 0x00, 0x00, 
  //48----55
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //56----63
  0x00, 0x00, 0x00, 0x00, 0x16, 0x36, 0x0B, 0x35, 
  //64----71
  0x16, 0x36, 0x0B, 0x35, 0x00, 0x00, 0x16, 0x36, 
  //72----79
  0x0B, 0x35, 0x16, 0x36, 0x0B, 0x35, 0x00, 0x00, 
  //80----87
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //88----95
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //96----103
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //104----111
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //112----119
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x9C, 0xB5, 
  //120----127
  0x00, 0x00, 0x00, 0x00, 0xE7, 0x00, 0x14, 0x98, 
  //128----135
  0x0F, 0x11, 0x02, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //136----143
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //144----151
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //152----159
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //160----167
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //168----175
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //176----183
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //184----191
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //192----199
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //200----207
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //208----215
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //216----223
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //224----231
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //232----239
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //240----247
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //248----255
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xDB, 0x08, 
  //256----263
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //264----271
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //272----279
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //280----287
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //288----295
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //296----303
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //304----311
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //312----319
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //320----327
  0x04, 0xCB, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //328----335
  0x00, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 
  //336----343
  0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 
  //344----351
  0x20, 0x20, 0x20, 0x20, 0x20, 0x00, 0x80, 0xAD, 
  //352----359
  0xFF, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //360----367
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //368----375
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //376----383
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //384----391
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //392----399
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //400----407
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //408----415
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //416----423
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //424----431
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //432----439
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //440----447
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //448----455
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //456----463
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //464----471
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //472----479
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //480----487
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //488----495
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //496----503
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  //504----511
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 
}})}


gBoardModuleTokenSpaceGuid.DDR4Micron8G170SpdData|*|{CODE(
{
//SM31H94915 Micron HTH5AN8G6NDJR-XND
  1,
 { 
	//0-7
0x23, 0x11, 0x0C, 0x03, 0x45, 0x21, 0x00, 0x08,
//8-15
0x00, 0x60, 0x00, 0x03, 0x02, 0x03, 0x00, 0x00,
//16-23
0x00, 0x00, 0x05, 0x0D, 0xF8, 0xFF, 0x02, 0x00,
//24-31
0x6E, 0x6E, 0x6E, 0x11, 0x00, 0x6E, 0xF0, 0x0A, 
//32-39
0x20, 0x08, 0x00 ,0x05, 0x00, 0xF0, 0x2B, 0x34,
//40-47
0x28, 0x00, 0x78, 0x00, 0x14, 0x3C, 0x00, 0x00,
//48-55
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,	
//56-63
0x00, 0x00, 0x00, 0x00, 0x16, 0x36, 0x0B, 0x35,
//64-71
0x16, 0x36, 0x0B, 0x35, 0x00, 0x00, 0x16, 0x36,
//72-79
0x0B, 0x35, 0x16, 0x36, 0x0B, 0x35, 0x00, 0x00,
//80-87
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
//88-95
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
//96-103
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
//104-111
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
//112-119
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x9C, 0xB4,
//120-127
0x00, 0x00, 0x00, 0x00, 0xE7, 0x00, 0x75, 0x20,
//128-135
0x0F, 0x01, 0x02, 0x00, 0x00, 0x00, 0x00, 0x00,
//136-143
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
//144-151
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
//152-159
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
//160-199
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
//200-239
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
//240-255

0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xC0, 0xE2,
//256-295
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
//296-319
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
//320-327
0x07, 0x92, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
//328-335
0x00, 0x48, 0x54, 0x48, 0x35, 0x41, 0x4E, 0x38, 
//336-343
0x47, 0x36, 0x4E, 0x44, 0x4A, 0x52, 0x2D, 0x58, 
//344--351
0x4E, 0x44, 0x20, 0x20, 0x20, 0x00, 0x07, 0x92, 
//352-391
0x44, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
//392-431
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
//432-471
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,	
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
//472-487
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,	
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,	
//488-495
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,		
//496-511
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00
}})}



gBoardModuleTokenSpaceGuid.DDR4Smart8G170SpdData|*|{CODE(
{
//SM31K40193   Smart 
  1,
 { 
	//0-7
0x23, 0x11, 0x0C, 0x03, 0x46, 0x29, 0x00, 0x08,
//8-15
0x00, 0x60, 0x00, 0x03, 0x02, 0x03, 0x00, 0x00,
//16-23
0x00, 0x00, 0x05, 0x0D, 0xF8, 0xFF, 0x02, 0x00,
//24-31
0x6E, 0x6E, 0x6E, 0x11, 0x00, 0x6E, 0x30, 0x11, 
//32-39
0xF0, 0x0A, 0x20 ,0x08, 0x00, 0xF0, 0x2B, 0x34,
//40-47
0x28, 0x00, 0x78, 0x00, 0x14, 0x3C, 0x00, 0x00,
//48-55
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,	
//56-63
0x00, 0x00, 0x00, 0x00, 0x16, 0x36, 0x0B, 0x35,
//64-71
0x16, 0x36, 0x0B, 0x35, 0x00, 0x00, 0x16, 0x36,
//72-79
0x0B, 0x35, 0x16, 0x36, 0x0B, 0x35, 0x00, 0x00,
//80-87
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
//88-95
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
//96-103
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
//104-111
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
//112-119
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x9C, 0xB5,
//120-127
0x00, 0x00, 0x00, 0x00, 0xE7, 0x00, 0xDE, 0xDE,
//128-135
0x0F, 0x11, 0x02, 0x00, 0x00, 0x00, 0x00, 0x00,
//136-143
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
//144-151
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
//152-159
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
//160-199
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
//200-239
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
//240-255

0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xDB, 0x08,
//256-295
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
//296-319
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
//320-327
0x01, 0x94, 0x06, 0x00, 0x00, 0x00, 0x00, 0x00,
//328-335
0x00, 0x53, 0x44, 0x51, 0x41, 0x41, 0x47, 0x36, 
//336-343
0x57, 0x31, 0x36, 0x58, 0x43, 0x57, 0x45, 0x39, 
//344--351
0x4E, 0x31, 0x54, 0x20, 0x20, 0x00, 0x01, 0x94, 
//352-391
0x44, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
//392-431
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
//432-471
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,	
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
//472-487
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,	
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,	
//488-495
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,		
//496-511
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00
}})}


!endif
#[-end-211228-QINGLIN0136-add]#
#[-end-210910-QINGLIN0060-add]#
