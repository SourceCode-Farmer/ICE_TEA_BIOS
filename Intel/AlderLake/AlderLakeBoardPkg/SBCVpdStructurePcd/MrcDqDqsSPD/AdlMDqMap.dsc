## @file
#  ADL M DQ configuration file.
#
# @copyright
#  INTEL CONFIDENTIAL
#  Copyright 2021 Intel Corporation.
#
#  The source code contained or described herein and all documents related to the
#  source code ("Material") are owned by Intel Corporation or its suppliers or
#  licensors. Title to the Material remains with Intel Corporation or its suppliers
#  and licensors. The Material may contain trade secrets and proprietary and
#  confidential information of Intel Corporation and its suppliers and licensors,
#  and is protected by worldwide copyright and trade secret laws and treaty
#  provisions. No part of the Material may be used, copied, reproduced, modified,
#  published, uploaded, posted, transmitted, distributed, or disclosed in any way
#  without Intel's prior express written permission.
#
#  No license under any patent, copyright, trade secret or other intellectual
#  property right is granted to or conferred upon you by disclosure or delivery
#  of the Materials, either expressly, by implication, inducement, estoppel or
#  otherwise. Any license under such intellectual property rights must be
#  express and approved by Intel in writing.
#
#  Unless otherwise agreed by Intel in writing, you may not remove or alter
#  this notice or any other notice embedded in Materials by Intel or
#  Intel's suppliers or licensors in any way.
#
#  This file contains a 'Sample Driver' and is licensed as such under the terms
#  of your license agreement with Intel or your vendor. This file may be modified
#  by the user, subject to the additional terms of the license agreement.
#
# @par Specification
##

[PcdsDynamicExVpd.common.SkuIdAdlMLp4Rvp]
gBoardModuleTokenSpaceGuid.VpdPcdMrcDqMapCpu2Dram|*|{CODE({{
  //
  // ADL-M LPDDR4 RVP DQ bit swizzling between CPU and DRAM
  //
  //Controller 0
  {{ 11, 15, 10,  9,  8, 14, 13, 12 },   // Byte 0
   {  0,  5,  4,  1,  6,  7,  2,  3 }},  // Byte 1
  {{ 11, 15, 10,  9, 12,  8, 14, 13 },   // Byte 2
   {  0,  1,  5,  4,  7,  6,  2,  3 }},  // Byte 3
  {{  3,  2,  7,  6,  1,  0,  4,  5 },   // Byte 4
   { 12, 14,  8, 13,  9, 15, 10, 11 }},  // Byte 5
  {{  7,  6,  2,  3,  1,  5,  0,  4 },   // Byte 6
   {  8, 12, 14, 13, 15, 10, 11,  9 }},  // Byte 7
  //Controller 1
  {{ 11,  9, 15, 10, 12,  8, 14, 13 },   // Byte 0
   {  0,  5,  4,  1,  6,  7,  2,  3 }},  // Byte 1
  {{ 15, 11,  9, 10, 14, 12, 13,  8 },   // Byte 2
   {  0,  1,  5,  4,  6,  7,  3,  2 }},  // Byte 3
  {{  2,  7,  6,  3,  4,  1,  5,  0 },   // Byte 4
   { 12, 13,  8, 14,  9, 10, 15, 11 }},  // Byte 5
  {{  7,  3,  6,  2,  5,  4,  0,  1 },   // Byte 6
   { 13, 14,  8, 12, 11,  9, 10, 15 }}   // Byte 7
}})}

[PcdsDynamicExVpd.common.SkuIdAdlMLp5Rvp]
gBoardModuleTokenSpaceGuid.VpdPcdMrcDqMapCpu2Dram|*|{CODE({{
  //
  // ADL-M LPDDR5 RVP2/RVP3 DQ bit swizzling between CPU and DRAM
  //
  //Controller 0
  {{  1,  0,  2,  3,  4,  7,  6,  5 },   // Byte 0
   { 13, 14,  9, 11, 12, 15, 10,  8 }},  // Byte 1
  {{  1,  0,  2,  3,  7,  4,  5,  6 },   // Byte 2
   { 14, 15, 13, 12,  8, 10, 11,  9 }},  // Byte 3
  {{  0,  2,  7,  4,  1,  6,  3,  5 },   // Byte 4
   { 13, 15, 12, 14, 11,  8, 10,  9 }},  // Byte 5
  {{  0,  2,  3,  1,  7,  5,  6,  4 },   // Byte 6
   { 12, 15, 13, 14,  8, 10,  9, 11 }},  // Byte 7
  //Controller 1
  {{  1,  3,  0,  2,  5,  4,  7,  6 },   // Byte 0
   { 13, 14,  9, 11, 12, 15, 10,  8 }},  // Byte 1
  {{  0,  1,  3,  2,  5,  7,  6,  4 },   // Byte 2
   { 14, 15, 13, 12, 10,  8,  9, 11 }},  // Byte 3
  {{  2,  7,  4,  0,  1,  3,  6,  5 },   // Byte 4
   { 13, 15, 12, 14, 11, 10,  8,  9 }},  // Byte 5
  {{  0,  1,  2,  3,  5,  4,  6,  7 },   // Byte 6
   { 14, 13, 12, 15,  9, 11, 10,  8 }}   // Byte 7
}})}

[PcdsDynamicExVpd.common.SkuIdAdlMLp5Aep]
gBoardModuleTokenSpaceGuid.VpdPcdMrcDqMapCpu2Dram|*|{CODE({{
  //
  // ADL-M LPDDR5 AEP DQ bit swizzling between CPU and DRAM
  //
  //Controller 0
  {{  3,  1,  0,  5,  4,  2,  7,  6 },   // Byte 0
   { 14, 13,  9, 11,  8, 12, 10, 15 }},  // Byte 1
  {{  3,  0,  1,  2,  5,  4,  7,  6 },   // Byte 2
   { 13, 12, 14,  9, 15,  2, 11,  8 }},  // Byte 3
  {{  7,  2,  4,  0,  1,  6,  3,  5 },   // Byte 4
   { 13, 14, 12, 15, 10,  9, 11,  8 }},  // Byte 5
  {{  7,  2,  3,  0,  4,  6,  5,  1 },   // Byte 6
   { 12, 13, 14, 15,  8, 10,  9, 11 }},  // Byte 7
  //Controller 1
  {{  0,  2,  1,  3,  5,  4,  7,  6 },   // Byte 0
   { 13, 11, 14,  9, 12, 15,  8, 10 }},  // Byte 1
  {{  1,  0,  2,  3,  7,  5,  6,  4 },   // Byte 2
   { 14, 12, 13,  9, 10, 15,  0, 11 }},  // Byte 3
  {{  2,  4,  0,  7,  1,  3,  6,  5 },   // Byte 4
   { 13, 14, 12, 15, 10, 11,  9,  8 }},  // Byte 5
  {{  2,  3,  7,  0,  4,  1,  5,  6 },   // Byte 6
   { 15, 14, 12, 13,  9, 10, 11,  8 }}   // Byte 7
}})}
