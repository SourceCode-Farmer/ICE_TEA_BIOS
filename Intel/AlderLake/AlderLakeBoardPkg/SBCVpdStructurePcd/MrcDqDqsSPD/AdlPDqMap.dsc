## @file
#  ADL P DQ configuration file.
#
# @copyright
#  INTEL CONFIDENTIAL
#  Copyright 2020 - 2021 Intel Corporation.
#
#  The source code contained or described herein and all documents related to the
#  source code ("Material") are owned by Intel Corporation or its suppliers or
#  licensors. Title to the Material remains with Intel Corporation or its suppliers
#  and licensors. The Material may contain trade secrets and proprietary and
#  confidential information of Intel Corporation and its suppliers and licensors,
#  and is protected by worldwide copyright and trade secret laws and treaty
#  provisions. No part of the Material may be used, copied, reproduced, modified,
#  published, uploaded, posted, transmitted, distributed, or disclosed in any way
#  without Intel's prior express written permission.
#
#  No license under any patent, copyright, trade secret or other intellectual
#  property right is granted to or conferred upon you by disclosure or delivery
#  of the Materials, either expressly, by implication, inducement, estoppel or
#  otherwise. Any license under such intellectual property rights must be
#  express and approved by Intel in writing.
#
#  Unless otherwise agreed by Intel in writing, you may not remove or alter
#  this notice or any other notice embedded in Materials by Intel or
#  Intel's suppliers or licensors in any way.
#
#  This file contains a 'Sample Driver' and is licensed as such under the terms
#  of your license agreement with Intel or your vendor. This file may be modified
#  by the user, subject to the additional terms of the license agreement.
#
# @par Specification
##

[PcdsDynamicExVpd.common.SkuIdAdlPLp4Rvp]
gBoardModuleTokenSpaceGuid.VpdPcdMrcDqMapCpu2Dram|*|{CODE({{
  //Controller 0
  {{  2,  0,  3,  1,  7,  4,  6,  5 },   // Byte 0
   {  8, 10, 11,  9, 15, 13, 14, 12 }},  // Byte 1
  {{  8,  9, 11, 10, 13, 15, 12, 14 },   // Byte 2
   {  7,  6,  4,  5,  3,  2,  1,  0 }},  // Byte 3
  {{  3,  2,  0,  1,  7,  6,  5,  4 },   // Byte 4
   { 13, 14, 15, 12,  8,  9, 10, 11 }},  // Byte 5
  {{  2,  0,  3,  1,  5,  7,  6,  4 },   // Byte 6
   { 10,  8,  9, 11, 12, 13, 15, 14 }},  // Byte 7
  //Controller 1
  {{  3,  2,  1,  0,  7,  4,  6,  5 },   // Byte 0
   {  8, 10, 11,  9, 14, 13, 12, 15 }},  // Byte 1
  {{  8,  9, 10, 11, 12, 13, 14, 15 },   // Byte 2
   {  7,  6,  5,  4,  3,  2,  1,  0 }},  // Byte 3
  {{ 13, 12, 15, 14,  9,  8, 11, 10 },   // Byte 4
   {  5,  4,  7,  6,  3,  1,  2,  0 }},  // Byte 5
  {{  3,  1,  2,  0,  4,  6,  5,  7 },   // Byte 6
   { 15, 12,  9,  8, 11, 10, 13, 14 }}   // Byte 7
}})}

[PcdsDynamicExVpd.common.SkuIdAdlPLp5Rvp]
gBoardModuleTokenSpaceGuid.VpdPcdMrcDqMapCpu2Dram|*|{CODE({{
  //Controller 0
  {{  3,  2,  1,  0,  5,  4,  6,  7 },   // Byte 0
   { 15, 14, 12, 13,  8,  9, 10, 11 }},  // Byte 1
  {{  0,  2,  3,  1,  5,  7,  4,  6 },   // Byte 2
   { 14, 13, 15, 12,  8,  9, 11, 10 }},  // Byte 3
  {{  1,  2,  0,  3,  4,  6,  5,  7 },   // Byte 4
   { 15, 13, 12, 14,  9, 10,  8, 11 }},  // Byte 5
  {{  2,  1,  3,  0,  7,  4,  5,  6 },   // Byte 6
   { 13, 12, 15, 14,  9, 11,  8, 10 }},  // Byte 7
  //Controller 1
  {{  1,  2,  3,  0,  6,  4,  5,  7 },   // Byte 0
   { 15, 13, 14, 12, 10,  9,  8, 11 }},  // Byte 1
  {{  1,  0,  3,  2,  6,  7,  4,  5 },   // Byte 2
   { 14, 12, 15, 13,  8,  9, 10, 11 }},  // Byte 3
  {{  0,  2,  1,  3,  4,  7,  5,  6 },   // Byte 4
   { 12, 13, 15, 14,  9, 11, 10,  8 }},  // Byte 5
  {{  3,  2,  1,  0,  5,  4,  6,  7 },   // Byte 6
   { 13, 15, 11, 12, 10,  9, 14,  8 }}   // Byte 7
}})}

#[-start-210519-KEBIN00001-modify]#
gBoardModuleTokenSpaceGuid.YogaC9VpdPcdMrcDqMapCpu2Dram|*|{CODE({{
  //Controller 0
  {{  1,  2,  0,  3,  5,  4,  6,  7},   // Byte 0
   { 10, 11,  9,  8, 14, 12, 13, 15}},  // Byte 1 
 {{  7,  5,  6,  3,  4,  0,  1,  2 },   // Byte 2
   { 12, 14, 13, 15, 10,  9,  8, 11}},  // Byte 3
  {{  2,  7,  3,  6,  4,  0,  5,  1},   // Byte 4
   { 13, 15, 12, 14,  9, 11,  8, 10}},  // Byte 5
  {{  1,  0,  2,  3,  7,  5 , 6,  4},   // Byte 6
   { 11,  9, 10,  8, 14, 15, 13, 12}},  // Byte 7

  //Controller 1
  {{  1,  0,  2,  3, 7,  4,  6, 5},   // Byte 0
   { 12, 15, 14, 13, 9, 11, 10, 8}},  // Byte 1
  {{  3,  0,  1,  2, 7,  5,  6, 4},   // Byte 2
   { 13, 14, 11, 15,12,  9, 10, 8}},  // Byte 3
  {{  1,  2,  6,  0, 4,  7,  5, 3},   // Byte 4
   { 15, 14, 12, 13, 9,  8, 11, 10}}, // Byte 5
  {{  0, 2,   1,  3, 4,  7,  6, 5},   // Byte 6
   { 14, 10, 12, 13, 9, 11, 15, 8}}   // Byte 7
}})}
#[-end-210519-KEBIN00001-modify]#

#[start-210720-STORM1100-modify]#
#[-start-210807-BAIN000028-modify]#
# C770  16
gBoardModuleTokenSpaceGuid.YogaC716VpdPcdMrcDqMapCpu2Dram|*|{CODE({{
// Controller 0
  {{12, 14, 13, 15, 10, 9, 11, 8},  // Byte 0
   {0, 2, 1, 3, 7, 5, 4, 6}},       // Byte 1
  {{0,  2, 3, 1, 6, 4, 7, 5},       // Byte 2
   {13, 15, 12, 14, 10, 11, 8, 9}}, // Byte 3
  {{14, 12, 15, 13, 11, 8, 9, 10},  // Byte 4
   {3, 0, 2, 1, 7, 5, 4, 6}},       // Byte 5
  {{0,  1, 3, 2, 7, 5, 6, 4},       // Byte 6
   {14, 13, 12, 15, 8, 9, 10, 11}}, // Byte 7
// controller 1
  {{14,  13, 12, 15, 9, 10, 8, 11}, // Byte 0
   {0, 2, 1, 3, 4, 7, 5, 6}},       // Byte 1
  {{0,  3,  2,  1, 6, 4, 7, 5},     // Byte 2
   {13, 14, 12, 15, 11, 9, 10, 8}}, // Byte 3
  {{2,  1, 0,  3, 7, 5, 6, 4},      // Byte 4
   {12, 15, 14, 13, 11, 8, 10, 9}}, // Byte 5
  {{12,  15, 14,  13, 8, 10, 9, 11},// Byte 6
   {4, 5, 3, 2, 6, 1, 0, 7}}        // Byte 7
}})}
#[-end-210807-BAIN000028-modify]#


# C770 14
#[-start-210807-BAIN000027-modify]#
gBoardModuleTokenSpaceGuid.YogaC714VpdPcdMrcDqMapCpu2Dram|*|{CODE({{
// Controller 0
  {{4, 6, 5, 7, 2, 0, 3, 1},         //Byte 0
   {11, 8, 10, 9, 15, 13, 12, 14}},  //Byte 1
  {{8 ,10, 9, 11, 14, 12, 15, 13},   //Byte 2
   {3 , 7, 4, 6, 0, 1, 2, 5}},       //Byte 3
  {{6, 4, 5, 7, 1, 2, 0, 3},         //Byte 4
   {11, 8, 10, 9, 12, 14, 15, 13}},  //Byte 5
  {{10 ,8 ,9 , 11, 14, 13, 12, 15},  //Byte 6
   {7, 5, 4, 6, 2, 3, 0, 1}},        //Byte 7

// controller 1
  {{7, 5, 6, 4, 0, 2, 1, 3},         //Byte 0
   {12, 14, 13, 15, 11, 8, 10, 9}},  //Byte 1
  {{8, 11, 9, 10, 14, 12, 13, 15},   //Byte 2
   {7, 5, 4, 6, 0, 3, 2, 1}},        //Byte 3
  {{1, 2, 3, 0, 6, 5, 7, 4},         //Byte 4
   {9, 11, 10,  8, 13, 15, 14, 12}}, //Byte 5
  {{9, 8, 11 , 10, 13, 15, 14, 12},  //Byte 6
   {3, 1 , 6, 5, 0, 4, 7, 2}}        //Byte 7
}})}
#[-end-210807-BAIN000027-modify]#
#[end-210720-STORM1100-modify]#

#[-start-210817-DABING0002-modify]#
#[-start-210831-TAMT000005-modify]#
#[-start-210831-TAMT000005-A-modify]#
gBoardModuleTokenSpaceGuid.S77014VpdPcdMrcDqMapCpu2Dram|*|{CODE({{
  //Controller 0
  {{  12, 14, 13, 15, 10, 9,  11, 8},  // Byte 0
   {  0,  2,  1,  3,  7,  5,  4,  6}}, // Byte 1 
  {{  0,  2,  3,  1,  6,  4,  7,  5 }, // Byte 2
   { 13,  15, 12, 14, 10, 11, 8,  9}}, // Byte 3
  {{ 14,  12, 15, 13, 11, 8,  9,  10}, // Byte 4
   {  3,  0,  2,  1,  7,  5,  4,  6}}, // Byte 5
  {{  2,  1,  3,  0,  7,  5,  6,  4},  // Byte 6
   { 14,  13, 12, 15, 11, 8, 10,  9}}, // Byte 7

  //Controller 1
  {{ 14,  13, 12, 15,  9, 10,  8, 11},  // Byte 0
   {  0,   2,  1,  3,  4,  7,  5, 6}},  // Byte 1
  {{  1,   2,  3,  0,  6,  4,  7, 5},   // Byte 2
   { 13,  14, 12, 15, 11,  9, 10, 8}},  // Byte 3
  {{  2,   1,  0,  3,  7,  5,  6, 4},   // Byte 4
   { 12,  15, 14, 13,  11, 8, 10, 9}},  // Byte 5
  {{ 12,  15, 14, 13,  8, 10,  9, 11},  // Byte 6
   {  4,   5,  3,  2,  6,  0,  1, 7}}   // Byte 7
}})}
#[-end-210831-TAMT000005-A-modify]#
#[-end-210831-TAMT000005-modify]#
#[-end-210817-DABING0002-modify]#

#[-start-210914-DABING0006-modify]#
#[-start-210918-DABING0008-modify]#
#[-start-210922-DABING0008-A-modify]#
gBoardModuleTokenSpaceGuid.S77013VpdPcdMrcDqMapCpu2Dram|*|{CODE({{
  //Controller 0
  {{  2,   1,  3,  0,  6,  5,  4,  7},  // Byte 0
   { 15,  12, 14, 13,  8, 11,  9, 10}}, // Byte 1
  {{  2,   0,  3,  7,  6,  4,  1,  5},  // Byte 2
   { 14,  15, 12, 13, 10,  8, 11,  9}}, // Byte 3 
  {{  2,   1,  3,  0,  4,  6,  5,  7 }, // Byte 4
   { 15,  12, 13, 14,  8, 11, 10,  9}}, // Byte 5
  {{  1,   0,  2,  3,  6,  7,  4,  5},  // Byte 6
   { 14,  12, 13, 15, 10, 11,  8,  9}}, // Byte 7

  //Controller 1
  {{  2,   0,  3,  1,  7,  5,  6,  4},  // Byte 0
   { 15,  12, 13, 14,  8, 11,  9, 10}},  // Byte 1
  {{  1,   2,  0,  3,  6,  4,  7,  5},   // Byte 2
   { 12,  14, 15, 13, 10,  9,  8, 11}},  // Byte 3
  {{ 12,  13, 15, 14, 10,  8, 11,  9},   // Byte 4
   {  1,   2,  0,  3,  6,  5,  7,  4}},  // Byte 5
  {{  3,   0,  2,  1,  5,  7,  4,  6},  // Byte 6
   {  8,  10, 14, 13,  9, 12, 15, 11}}   // Byte 7
}})}
#[-end-210922-DABING0008-A-modify]#
#[-end-210918-DABING0008-modify]#
#[-end-210914-DABING0006-modify]#

[PcdsDynamicExVpd.common.SkuIdAdlPLp4Bep]
gBoardModuleTokenSpaceGuid.VpdPcdMrcDqMapCpu2Dram|*|{CODE({{
  //Controller 0
  {{ 15, 12, 13, 14,  9, 11, 10,  8 },   //Byte 0
   {  0,  1,  3,  2,  6,  5,  7,  4 }},  //Byte 1
  {{ 10,  8, 11,  9, 12, 14, 13, 15 },   //Byte 2
   {  6,  7,  4,  5,  0,  3,  2,  1 }},  //Byte 3
  {{  3,  2,  0,  1,  7,  6,  4,  5 },   //Byte 4
   { 11,  9, 13, 12, 15, 14, 10,  8 }},  //Byte 5
  {{  3,  1,  2,  0,  4,  7,  5,  6 },   //Byte 6
   { 14, 13, 16, 12, 11,  8, 10,  9 }},  //Byte 7
  //Controller 1
  {{  1,  5,  7,  4,  3,  0,  2,  6 },   //Byte 0
   { 11,  9, 10,  8, 13, 15, 14, 12 }},  //Byte 1
  {{ 13, 12, 14, 15,  9,  8, 10, 11 },   //Byte 2
   {  6,  4,  5,  7,  0,  2,  3,  1 }},  //Byte 3
  {{ 15, 13, 12, 14,  9, 11, 10,  8 },   //Byte 4
   {  1,  0,  2,  3,  6,  4,  5,  7 }},  //Byte 5
  {{  0,  2,  3,  1,  4,  5,  6,  7 },   //Byte 6
   {  9, 12, 15,  8, 13, 10, 11, 14 }}   //Byte 7
}})}

[PcdsDynamicExVpd.common.SkuIdAdlPLp5Aep]
gBoardModuleTokenSpaceGuid.VpdPcdMrcDqMapCpu2Dram|*|{CODE({{
  //Controller 0
  {{  5,  0,  4,  1,  2,  6,  7,  3 },   // Byte 0
   {  9, 12,  8, 13, 15, 10, 14, 11 }},  // Byte 1
  {{  9, 10, 11,  8, 13, 15, 14, 12 },   // Byte 2
   {  0,  3,  1,  2,  7,  5,  6,  4 }},  // Byte 3
  {{  3,  7,  2,  6,  4,  1,  5,  0 },   // Byte 4
   { 12, 14, 15, 13, 11, 10,  8,  9 }},  // Byte 5
  {{  9, 13,  8, 12, 15, 10, 14, 11 },   // Byte 6
   {  7,  6,  4,  5,  0,  3,  1,  2 }},  // Byte 7
  //Controller 1
  {{ 15, 14, 12, 13, 10,  9,  8, 11 },   // Byte 0
   {  7,  5,  4,  6,  2,  0,  1,  3 }},  // Byte 1
  {{  9, 10, 11,  8, 12, 15, 13, 14 },   // Byte 2
   {  3,  7,  2,  6,  0,  4,  5,  1 }},  // Byte 3
  {{ 11,  8, 10,  9, 12, 14, 13, 15 },   // Byte 4
   {  1,  7,  0,  2,  5,  3,  4,  6 }},  // Byte 5
  {{  3,  2,  1,  0,  7,  5,  6,  4 },   // Byte 6
   { 12, 13, 10,  9, 14, 11,  8, 15 }}   // Byte 7
}})}

[PcdsDynamicExVpd.common.SkuIdAdlPLp5Dg128Aep]
gBoardModuleTokenSpaceGuid.VpdPcdMrcDqMapCpu2Dram|*|{CODE({{
  //Controller 0
  {{  0,  3,  2,  1,  6,  5,  7,  4 },   // Byte 0
   { 14, 12, 15, 13, 11,  8,  10, 9 }},  // Byte 1
  {{  1,  2,  3,  0,  6,  4,  7,  5 },   // Byte 2
   { 13, 15, 12, 14,  8, 11,  9, 10 }},  // Byte 3
  {{  0,  3,  2,  1,  6,  5,  7,  4 },   // Byte 4
   { 14, 12, 15, 13,  8, 10,  9, 11 }},  // Byte 5
  {{  1,  2,  0,  3,  4,  6,  7,  5 },   // Byte 6
   { 13, 15, 12, 14,  8,  9, 10, 11 }},  // Byte 7
  //Controller 1
  {{  0,  3,  2,  1,  6,  5,  7,  4 },   // Byte 0
   { 12, 14, 15, 13,  8,  9, 10, 11 }},  // Byte 1
  {{  2,  1,  3,  0,  6,  4,  7,  5 },   // Byte 2
   { 13, 15, 12, 14,  8, 11,  9, 10 }},  // Byte 3
  {{ 14, 12, 15, 13,  8, 11, 10,  9 },   // Byte 4
   {  1,  2,  3,  0,  5,  6,  7,  4 }},  // Byte 5
  {{  1,  3,  0,  2,  6,  4,  7,  5 },   // Byte 6
   { 14, 15, 10, 11, 13,  8,  9, 12 }}   // Byte 7
}})}

[PcdsDynamicExVpd.common.SkuIdAdlPLp5Gcs]
gBoardModuleTokenSpaceGuid.VpdPcdMrcDqMapCpu2Dram|*|{CODE({{
  //Controller 0
  {{  5,  0,  4,  1,  2,  6,  7,  3 },   // Byte 0
   {  9, 12,  8, 13, 15, 10, 14, 11 }},  // Byte 1
  {{  9, 10, 11,  8, 13, 15, 14, 12 },   // Byte 2
   {  0,  3,  1,  2,  7,  5,  6,  4 }},  // Byte 3
  {{  3,  7,  2,  6,  4,  1,  5,  0 },   // Byte 4
   { 12, 14, 15, 13, 11, 10,  8,  9 }},  // Byte 5
  {{  9, 13,  8, 12, 15, 10, 14, 11 },   // Byte 6
   {  7,  6,  4,  5,  0,  3,  1,  2 }},  // Byte 7
  //Controller 1
  {{ 15, 14, 12, 13, 10,  9,  8, 11 },   // Byte 0
   {  7,  5,  4,  6,  2,  0,  1,  3 }},  // Byte 1
  {{  9, 10, 11,  8, 12, 15, 13, 14 },   // Byte 2
   {  3,  7,  2,  6,  0,  4,  5,  1 }},  // Byte 3
  {{ 11,  8, 10,  9, 12, 14, 13, 15 },   // Byte 4
   {  1,  7,  0,  2,  5,  3,  4,  6 }},  // Byte 5
  {{  3,  2,  1,  0,  7,  5,  6,  4 },   // Byte 6
   { 12, 13, 10,  9, 14, 11,  8, 15 }}   // Byte 7
}})}

[PcdsDynamicExVpd.common.SkuIdAdlPLp5MbAep]
gBoardModuleTokenSpaceGuid.VpdPcdMrcDqMapCpu2Dram|*|{CODE({{
  //Controller 0
  {{  5,  0,  4,  1,  2,  6,  7,  3 },   // Byte 0
   { 11, 15, 13, 12, 10, 14,  8,  9 }},  // Byte 1
  {{  9, 10, 11,  8, 13, 14, 12, 15 },   // Byte 2
   {  0,  2,  1,  3,  7,  5,  6,  4 }},  // Byte 3
  {{  3,  7,  2,  6,  4,  1,  5,  0 },   // Byte 4
   { 12, 14, 15, 13, 11, 10,  8,  9 }},  // Byte 5
  {{ 15, 14, 12, 13, 10,  9, 11,  8 },   // Byte 6
   {  7,  6,  4,  5,  0,  3,  1,  2 }},  // Byte 7
  //Controller 1
  {{ 15, 14, 12, 13, 10,  9,  8, 11 },   // Byte 0
   {  1,  3,  0,  2,  5,  6,  7,  4 }},  // Byte 1
  {{  9, 10, 11,  8, 12, 15, 13, 14 },   // Byte 2
   {  3,  7,  2,  6,  0,  4,  5,  1 }},  // Byte 3
  {{ 11,  8, 10,  9, 12, 14, 13, 15 },   // Byte 4
   {  0,  7,  1,  2,  6,  4,  3,  5 }},  // Byte 5
  {{  1,  2,  3,  0,  7,  5,  6,  4 },   // Byte 6
   { 15, 14, 11, 13,  8, 12,  9, 10 }}   // Byte 7
}})}

[PcdsDynamicExVpd.common.SkuIdAdlPT3Lp5Rvp]
gBoardModuleTokenSpaceGuid.VpdPcdMrcDqMapCpu2Dram|*|{CODE({{
  //Controller 0
  {{  0,  3,  2,  1,  6,  4,  5,  7 },   // Byte 0
   { 14, 12, 13, 15, 11,  8, 10,  9 }},  // Byte 1
  {{  1,  0,  2,  3,  6,  4,  7,  5 },   // Byte 2
   { 15, 13, 12, 14,  8, 11, 10,  9 }},  // Byte 3
  {{  2,  1,  3,  0,  7,  6,  4,  5 },   // Byte 4
   { 14, 12, 13, 15,  8,  9, 10, 11 }},  // Byte 5
  {{  1,  2,  3,  0,  6,  4,  7,  5 },   // Byte 6
   { 13, 15, 12, 14,  8, 11, 10,  9 }},  // Byte 7
  //Controller 1
  {{  2,  3,  0,  1,  6,  5,  7,  4 },   // Byte 0
   { 14, 12, 13, 15,  8, 10,  9, 11 }},  // Byte 1
  {{  1,  2,  3,  0,  6,  4,  7,  5 },   // Byte 2
   { 15, 13, 12, 14, 11,  9, 10,  8 }},  // Byte 3
  {{  3,  1,  2,  0,  5,  7,  6,  4 },   // Byte 4
   { 15, 13, 14, 12, 10,  9, 11,  8 }},  // Byte 5
  {{  2,  3,  1,  0,  5,  4,  7,  6 },   // Byte 6
   { 14, 15, 10, 11, 13,  8,  9, 12 }}   // Byte 7
}})}