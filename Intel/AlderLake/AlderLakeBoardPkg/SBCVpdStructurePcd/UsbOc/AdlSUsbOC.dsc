## @file
#  Alderlake S USB OC configuration file.
#
# @copyright
#  INTEL CONFIDENTIAL
#  Copyright 2020 - 2021 Intel Corporation.
#
#  The source code contained or described herein and all documents related to the
#  source code ("Material") are owned by Intel Corporation or its suppliers or
#  licensors. Title to the Material remains with Intel Corporation or its suppliers
#  and licensors. The Material may contain trade secrets and proprietary and
#  confidential information of Intel Corporation and its suppliers and licensors,
#  and is protected by worldwide copyright and trade secret laws and treaty
#  provisions. No part of the Material may be used, copied, reproduced, modified,
#  published, uploaded, posted, transmitted, distributed, or disclosed in any way
#  without Intel's prior express written permission.
#
#  No license under any patent, copyright, trade secret or other intellectual
#  property right is granted to or conferred upon you by disclosure or delivery
#  of the Materials, either expressly, by implication, inducement, estoppel or
#  otherwise. Any license under such intellectual property rights must be
#  express and approved by Intel in writing.
#
#  Unless otherwise agreed by Intel in writing, you may not remove or alter
#  this notice or any other notice embedded in Materials by Intel or
#  Intel's suppliers or licensors in any way.
#
#  This file contains a 'Sample Driver' and is licensed as such under the terms
#  of your license agreement with Intel or your vendor. This file may be modified
#  by the user, subject to the additional terms of the license agreement.
#
# @par Specification
##

[PcdsDynamicExVpd.common.SkuIdAdlSAdpSDdr5UDimm1DAep]
gBoardModuleTokenSpaceGuid.VpdPcdUSB2OCMap|*|{CODE(
{ 14,
  { 0,              // Port 1
    1,              // Port 2
    7,              // Port 3
    7,              // Port 4
    USB_OC_SKIP,    // Port 5
    USB_OC_SKIP,    // Port 6
    USB_OC_SKIP,    // Port 7
    2,              // Port 8
    6,              // Port 9
    6,              // Port 10
    4,              // Port 11
    3,              // Port 12
    5,              // Port 13
    USB_OC_SKIP}    // Port 14
})}

[PcdsDynamicExVpd.common.SkuIdAdlSAdpSDdr5UDimm1DAep]
gBoardModuleTokenSpaceGuid.VpdPcdUSB3OCMap|*|{CODE(
{ 10,
  { 5,              // Port 1
    4,              // Port 2
    3,              // Port 3
    2,              // Port 4
    USB_OC_SKIP,    // Port 5
    USB_OC_SKIP,    // Port 6
    0,              // Port 7
    1,              // Port 8
    USB_OC_SKIP,    // Port 8
    USB_OC_SKIP}    // Port 10
})}

[PcdsDynamicExVpd.common.SkuIdAdlSAdpSDdr4UDimm2DCrb]
gBoardModuleTokenSpaceGuid.VpdPcdUSB2OCMap|*|{CODE(
{ 14,
  { 1,              // Port 1
    1,              // Port 2
    0,              // Port 3
    0,              // Port 4
    3,              // Port 5
    3,              // Port 6
    7,              // Port 7
    0,              // Port 8
    6,              // Port 9
    7,              // Port 10
    0,              // Port 11
    0,              // Port 12
    0,              // Port 13
    USB_OC_SKIP }   // Port 14
})}

[PcdsDynamicExVpd.common.SkuIdAdlSAdpSDdr4UDimm2DCrb]
gBoardModuleTokenSpaceGuid.VpdPcdUSB3OCMap|*|{CODE(
{ 10,
  { 6,              // Port 1
    6,              // Port 2
    0,              // Port 3
    0,              // Port 4
    7,              // Port 5
    7,              // Port 6
    1,              // Port 7
    1,              // Port 8
    0,              // Port 9
    0 }             // Port 10
})}

[PcdsDynamicExVpd.common.SkuIdAdlSAdpSDdr5UDimm1DOc]
gBoardModuleTokenSpaceGuid.VpdPcdUSB2OCMap|*|{CODE(
{ 14,
  { 1,              // Port 1
    1,              // Port 2
    0,              // Port 3
    0,              // Port 4
    3,              // Port 5
    3,              // Port 6
    7,              // Port 7
    0,              // Port 8
    USB_OC_SKIP,    // Port 9
    7,              // Port 10
    0,              // Port 11
    0,              // Port 12
    0,              // Port 13
    USB_OC_SKIP }   // Port 14
})}

[PcdsDynamicExVpd.common.SkuIdAdlSAdpSDdr5UDimm1DOc]
gBoardModuleTokenSpaceGuid.VpdPcdUSB3OCMap|*|{CODE(
{ 10,
  { USB_OC_SKIP,    // Port 1
    USB_OC_SKIP,    // Port 2
    0,              // Port 3
    0,              // Port 4
    7,              // Port 5
    7,              // Port 6
    1,              // Port 7
    1,              // Port 8
    0,              // Port 9
    0 }             // Port 10
})}


[PcdsDynamicExVpd.common.SkuIdAdlSAdpSDdr5SODimmCrb]
gBoardModuleTokenSpaceGuid.VpdPcdUSB2OCMap|*|{CODE(
{ 14,
  { 1,              // Port 1
    1,              // Port 2
    0,              // Port 3
    0,              // Port 4
    3,              // Port 5
    3,              // Port 6
    7,              // Port 7
    0,              // Port 8
    6,              // Port 9
    7,              // Port 10
    0,              // Port 11
    0,              // Port 12
    0,              // Port 13
    USB_OC_SKIP }   // Port 14
})}

[PcdsDynamicExVpd.common.SkuIdAdlSAdpSDdr5SODimmCrb]
gBoardModuleTokenSpaceGuid.VpdPcdUSB3OCMap|*|{CODE(
{ 10,
  { 6,              // Port 1
    6,              // Port 2
    0,              // Port 3
    0,              // Port 4
    7,              // Port 5
    7,              // Port 6
    1,              // Port 7
    1,              // Port 8
    0,              // Port 9
    0 }             // Port 10
})}

[PcdsDynamicExVpd.common.SkuIdAdlSAdpSSbgaDdr5SODimmErb]
gBoardModuleTokenSpaceGuid.VpdPcdUSB2OCMap|*|{CODE(
{ 14,
  { 1,              // Port 1,  Not Used
    USB_OC_SKIP,    // Port 2,  FPS header
    0,              // Port 3,  USB 2.0 Vertical Type A 1
    0,              // Port 4,  USB 3.2 Vertical Type A 2
    2,              // Port 5,  Thunderbolt -1 Port B
    2,              // Port 6,  Thunderbolt -1 Port A
    USB_OC_SKIP,    // Port 7,  TTK3 Header
    7,              // Port 8,  Back Panel Type-A 2
    1,              // Port 9,  Thunderbolt -0 Port B
    7,              // Port 10, Not Used
    1,              // Port 11, Thunderbolt -0 Port A
    6,              // Port 12, Back Panel Type-A 1
    USB_OC_SKIP,    // Port 13, WWAN M.2 module
    USB_OC_SKIP }   // Port 14, WLAN/BT
})}

[PcdsDynamicExVpd.common.SkuIdAdlSAdpSSbgaDdr5SODimmErb]
gBoardModuleTokenSpaceGuid.VpdPcdUSB3OCMap|*|{CODE(
{ 10,
  { USB_OC_SKIP,    // Port 1
    USB_OC_SKIP,    // Port 2
    6,              // Port 3,  Back Panel Type-A 1
    7,              // Port 4,  Back Panel Type-A 2
    USB_OC_SKIP,    // Port 5
    USB_OC_SKIP,    // Port 6
    USB_OC_SKIP,    // Port 7
    USB_OC_SKIP,    // Port 8
    USB_OC_SKIP,    // Port 9
    0 }             // Port 10, USB 3.2 Vertical Type A 2
})}

[PcdsDynamicExVpd.common.SkuIdAdlSAdpSSbgaDdr4SODimmCrb]
gBoardModuleTokenSpaceGuid.VpdPcdUSB2OCMap|*|{CODE(
{ 14,
  { 1,              // Port 1,  FP USB3.2 Type-C
    USB_OC_SKIP,    // Port 2,  FP USB3.2 FP AIC
    0,              // Port 3,  Vertical Type A
    USB_OC_SKIP,    // Port 4,  TTK3 HDR
    3,              // Port 5,  Thunderbolt Port
    3,              // Port 6,  Thunderbolt Porr
    0,              // Port 7,  BP Type-A 3
    0,              // Port 8,  BP Type-A 2
    USB_OC_SKIP,    // Port 9
    1,              // Port 10, FP USB2.0 P1
    1,              // Port 11, FP USB2.0 P2
    0,              // Port 12, BP Type-A 1
    USB_OC_SKIP,    // Port 13, WWAN M.2 module
    USB_OC_SKIP }   // Port 14, WLAN/BT
})}

[PcdsDynamicExVpd.common.SkuIdAdlSAdpSSbgaDdr4SODimmCrb]
gBoardModuleTokenSpaceGuid.VpdPcdUSB3OCMap|*|{CODE(
{ 10,
  { USB_OC_SKIP,    // Port 1
    USB_OC_SKIP,    // Port 2
    0,              // Port 3, Back Panel Type-A 1
    0,              // Port 4, Back Panel Type-A 2
    0,              // Port 5, Back Panel Type-A 3
    USB_OC_SKIP,    // Port 6
    1,              // Port 7, FP USB3.2 Type-C
    USB_OC_SKIP,    // Port 8, FP USB3.2 FP AIC
    0,              // Port 9, Vertical Type-A 1
    USB_OC_SKIP }   // Port 10
})}

[PcdsDynamicExVpd.common.SkuIdAdlSAdpSSbgaDdr5SODimmAep]
gBoardModuleTokenSpaceGuid.VpdPcdUSB2OCMap|*|{CODE(
{ 14,
  { USB_OC_SKIP,    // Port 1, Not Used
    USB_OC_SKIP,    // Port 2, Not Used
    USB_OC_SKIP,    // Port 3, Not Used
    6,              // Port 4, USB 3.2 Vertical Type A 2
    USB_OC_SKIP,    // Port 5, Not Used
    USB_OC_SKIP,    // Port 6, Not Used
    USB_OC_SKIP,    // Port 7, Not Used
    USB_OC_SKIP,    // Port 8, Not Used
    USB_OC_SKIP,    // Port 9, Thunderbolt -0 Port B
    USB_OC_SKIP,    // Port 10, Not Used
    USB_OC_SKIP,    // Port 11, Thunderbolt -0 Port A
    7,              // Port 12, Back Panel Type-A 1
    USB_OC_SKIP,    // Port 13, WWAN M.2 module
    USB_OC_SKIP }   // Port 14, WLAN/BT
})}

[PcdsDynamicExVpd.common.SkuIdAdlSAdpSSbgaDdr5SODimmAep]
gBoardModuleTokenSpaceGuid.VpdPcdUSB3OCMap|*|{CODE(
{ 10,
  { USB_OC_SKIP,    // Port 1
    USB_OC_SKIP,    // Port 2
    6,              // Port 3,  Back Panel Type-A 1
    7,              // Port 4,  Back Panel Type-A 2
    USB_OC_SKIP,    // Port 5
    USB_OC_SKIP,    // Port 6
    USB_OC_SKIP,    // Port 7
    USB_OC_SKIP,    // Port 8
    USB_OC_SKIP,    // Port 9
    USB_OC_SKIP }   // Port 10
})}