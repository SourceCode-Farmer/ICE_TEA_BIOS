## @file
#  Alderlake P USB OC configuration file.
#
# @copyright
#  INTEL CONFIDENTIAL
#  Copyright 2020 - 2021 Intel Corporation.
#
#  The source code contained or described herein and all documents related to the
#  source code ("Material") are owned by Intel Corporation or its suppliers or
#  licensors. Title to the Material remains with Intel Corporation or its suppliers
#  and licensors. The Material may contain trade secrets and proprietary and
#  confidential information of Intel Corporation and its suppliers and licensors,
#  and is protected by worldwide copyright and trade secret laws and treaty
#  provisions. No part of the Material may be used, copied, reproduced, modified,
#  published, uploaded, posted, transmitted, distributed, or disclosed in any way
#  without Intel's prior express written permission.
#
#  No license under any patent, copyright, trade secret or other intellectual
#  property right is granted to or conferred upon you by disclosure or delivery
#  of the Materials, either expressly, by implication, inducement, estoppel or
#  otherwise. Any license under such intellectual property rights must be
#  express and approved by Intel in writing.
#
#  Unless otherwise agreed by Intel in writing, you may not remove or alter
#  this notice or any other notice embedded in Materials by Intel or
#  Intel's suppliers or licensors in any way.
#
#  This file contains a 'Sample Driver' and is licensed as such under the terms
#  of your license agreement with Intel or your vendor. This file may be modified
#  by the user, subject to the additional terms of the license agreement.
#
# @par Specification
##

[PcdsDynamicExVpd.common.SkuIdAdlPLp4Rvp]
gBoardModuleTokenSpaceGuid.VpdPcdUSB2OCMap|*|{CODE(
{ 10,
  { 4,             // Port 1. VW Overcurrent PIN:4
    5,             // Port 2. VW Overcurrent PIN:5
    6,             // Port 3. VW Overcurrent PIN:6
    USB_OC_SKIP,   // Port 4
    7,             // Port 5. VW Overcurrent PIN:7
    USB_OC_SKIP,   // Port 6
    0,             // Port 7
    0,             // Port 8
    3,             // Port 9
    USB_OC_SKIP }  // Port 10
})}

[PcdsDynamicExVpd.common.SkuIdAdlPLp4Rvp]
gBoardModuleTokenSpaceGuid.VpdPcdUSB3OCMap|*|{CODE(
{ 4,
  { 3,             // Port 1
    0,             // Port 2
    0,             // Port 3
    USB_OC_SKIP }  // Port 4
})}

[PcdsDynamicExVpd.common.SkuIdAdlPLp4Rvp]
gBoardModuleTokenSpaceGuid.VpdPcdCpuUsb3OcMap|*|{CODE(
{ MAX_TCSS_USB3_PORTS,
  { 4,             // Port 1. VW Overcurrent PIN:4
    5,             // Port 2. VW Overcurrent PIN:5
    6,             // Port 3. VW Overcurrent PIN:6
    7 }            // Port 4. VW Overcurrent PIN:7
})}


[PcdsDynamicExVpd.common.SkuIdAdlPLp4Bep]
gBoardModuleTokenSpaceGuid.VpdPcdUSB2OCMap|*|{CODE(
{ 10,
  { 4,             // Port 1
    5,             // Port 2
    USB_OC_SKIP,   // Port 3
    USB_OC_SKIP,   // Port 4
    USB_OC_SKIP,   // Port 5
    USB_OC_SKIP,   // Port 6
    USB_OC_SKIP,   // Port 7
    3,             // Port 8
    USB_OC_SKIP,   // Port 9
    USB_OC_SKIP }  // Port 10
})}

[PcdsDynamicExVpd.common.SkuIdAdlPLp4Bep]
gBoardModuleTokenSpaceGuid.VpdPcdUSB3OCMap|*|{CODE(
{ 4,
  { USB_OC_SKIP,   // Port 1
    USB_OC_SKIP,   // Port 2
    3,             // Port 3
    USB_OC_SKIP }  // Port 4
})}

[PcdsDynamicExVpd.common.SkuIdAdlPLp4Bep]
gBoardModuleTokenSpaceGuid.VpdPcdCpuUsb3OcMap|*|{CODE(
{ MAX_TCSS_USB3_PORTS,
  { 4,             // Port 1. VW Overcurrent PIN:4
    5,             // Port 2. VW Overcurrent PIN:5
    USB_OC_SKIP,   // Port 3. VW Overcurrent PIN:6
    USB_OC_SKIP }  // Port 4. VW Overcurrent PIN:7
})}

[PcdsDynamicExVpd.common.SkuIdAdlPLp5Rvp]
#[-start-210628-BAIN000018-modify]#
!if $(LCFC_SUPPORT_ENABLE) == YES
!if $(C970_SUPPORT_ENABLE) == YES
  gBoardModuleTokenSpaceGuid.VpdPcdUSB2OCMap|*|{CODE(
  { 10,
    { USB_OC_SKIP,   // Port 1.
      1,             // Port 2. TYPE-C PORT1     USB_OC_1
      1,             // Port 3. TYPE-C PORT2     USB_OC_1
      2,             // Port 4. TYPE-C PORT3     USB_OC_2
      USB_OC_SKIP,   // Port 5.
      USB_OC_SKIP,   // Port 6.
      USB_OC_SKIP,   // Port 7.
      USB_OC_SKIP, // Port    8.
      0,             // Port 9. TYPE-A PORT1  USB_OC_0
      USB_OC_SKIP }  // Port 10.
  })}
!endif
#end C970_SUPPORT_ENABLE

#[start-210720-STORM1100-modify]#
!if $(C770_SUPPORT_ENABLE) == YES
# storm_poweron
#[-start-210728-YUNLEI0116-modify]
  gBoardModuleTokenSpaceGuid.VpdPcdUSB2OCMap|*|{CODE(
  { 10,
    { USB_OC_SKIP,   // Port 1.
      1,             // Port 2. TYPE-C PORT1     USB_OC_1
      1,             // Port 3. TYPE-C PORT2     USB_OC_1
      USB_OC_SKIP,   // Port 4. 
      USB_OC_SKIP,   // Port 5.
      USB_OC_SKIP,   // Port 6.
      USB_OC_SKIP,   // Port 7. 
      USB_OC_SKIP,   // Port 8.
      0,             // Port 9. TYPE-A PORT1  USB_OC_0
      USB_OC_SKIP }  // Port 10.
  })}
  gBoardModuleTokenSpaceGuid.VpdPcd16USB2OCMap|*|{CODE(
  { 10,
    { USB_OC_SKIP,   // Port 1.
      1,             // Port 2. TYPE-C PORT1     USB_OC_1
      1,             // Port 3. TYPE-C PORT2     USB_OC_1
      USB_OC_SKIP,   // Port 4. 
      USB_OC_SKIP,   // Port 5.
      USB_OC_SKIP,   // Port 6.
      3,             // Port 7. TYPE-A PORT2  USB_OC_3
      USB_OC_SKIP,   // Port 8.
      0,             // Port 9. TYPE-A PORT1  USB_OC_0
      USB_OC_SKIP }  // Port 10.
  })}
#[-end-210728-YUNLEI0116-modify]
!endif
#end C770_SUPPORT_ENABLE
#[end-210720-STORM1100-modify]#

#[-start-211014-Ching000013-modify]#
!if ($(S77014_SUPPORT_ENABLE) == YES) OR ($(S77014IAH_SUPPORT_ENABLE) == YES)
  gBoardModuleTokenSpaceGuid.VpdPcdUSB2OCMap|*|{CODE(
  { 10,
    { USB_OC_SKIP,   // Port 1.
      1,             // Port 2. TYPE-C PORTA     USB_OC_1
      1,             // Port 3. TYPE-C PORTB     USB_OC_1
      USB_OC_SKIP,   // Port 4.
      USB_OC_SKIP,   // Port 5.
      USB_OC_SKIP,   // Port 6.
      USB_OC_SKIP,   // Port 7.
      USB_OC_SKIP,   // Port 8.
      0,             // Port 9. TYPE-A PORT1     USB_OC_0
      USB_OC_SKIP }  // Port 10.
  })}
!endif
#end S77014_SUPPORT_ENABLE

!if $(S77013_SUPPORT_ENABLE) == YES
  gBoardModuleTokenSpaceGuid.VpdPcdUSB2OCMap|*|{CODE(
  { 10,
    { USB_OC_SKIP,   // Port 1.
      1,             // Port 2. TYPE-C PORT1     USB_OC_1
      USB_OC_SKIP,   // Port 3.
      2,             // Port 4. TYPE-C PORT2     USB_OC_2
      USB_OC_SKIP,   // Port 5.
      USB_OC_SKIP,   // Port 6.
      USB_OC_SKIP,   // Port 7.
      USB_OC_SKIP,   // Port 8.
      USB_OC_SKIP,   // Port 9.
      USB_OC_SKIP }  // Port 10.
  })}
!endif
#end S77013_SUPPORT_ENABLE
#[-end-211014-Ching000013-modify]#

!else
  gBoardModuleTokenSpaceGuid.VpdPcdUSB2OCMap|*|{CODE(
  { 10,
    { 4,             // Port 1. VW Overcurrent PIN:4
      5,             // Port 2. VW Overcurrent PIN:5
      6,             // Port 3. VW Overcurrent PIN:6
      USB_OC_SKIP,   // Port 4
      7,             // Port 5. VW Overcurrent PIN:7
      USB_OC_SKIP,   // Port 6
      0,             // Port 7
      0,             // Port 8
      3,             // Port 9
      USB_OC_SKIP }  // Port 10
  })}
!endif
#[-end-210628-BAIN000018-modify]#

[PcdsDynamicExVpd.common.SkuIdAdlPDdr5Rvp]
gBoardModuleTokenSpaceGuid.VpdPcdUSB2OCMap|*|{CODE(
{ 10,
  { 4,             // Port 1. VW Overcurrent PIN:4
    5,             // Port 2. VW Overcurrent PIN:5
    6,             // Port 3. VW Overcurrent PIN:6
    USB_OC_SKIP,   // Port 4
    7,             // Port 5. VW Overcurrent PIN:7
    USB_OC_SKIP,   // Port 6
    0,             // Port 7
    0,             // Port 8
    3,             // Port 9
    USB_OC_SKIP }  // Port 10
})}


[PcdsDynamicExVpd.common.SkuIdAdlPDdr4Rvp]
#[-start-210721-QINGLIN0001-modify]#
!if $(LCFC_SUPPORT_ENABLE) == YES
!if $(S570_SUPPORT_ENABLE) == YES
  gBoardModuleTokenSpaceGuid.VpdPcdUSB2OCMap|*|{CODE(
  { 10,
    { USB_OC_SKIP,   // Port 1.
      1,             // Port 2. TYPE-C PORT1        USB_OC_1
      1,             // Port 3. TYPE-C PORT2        USB_OC_1
      USB_OC_SKIP,   // Port 4.
      USB_OC_SKIP,   // Port 5.
      USB_OC_SKIP,   // Port 6.
      0,             // Port 7. USB3.0 PORT2 AOU    USB_OC_0
      USB_OC_SKIP,   // Port 8.
      3,             // Port 9. USB3.0 PORT1 Normal USB_OC_3
      USB_OC_SKIP }  // Port 10.
  })}
!endif
#[-start-210803-QINGLIN0008-add]#
#[-start-210802-SHAONN0003-add]#
!if ($(S370_SUPPORT_ENABLE) == YES)
  gBoardModuleTokenSpaceGuid.VpdPcdUSB2OCMap|*|{CODE(
  { 10,
    { 3,             // Port 1. USBA                USB_OC_3
      1,             // Port 2. TYPE-C PORT1        USB_OC_1
      USB_OC_SKIP,   // Port 3.
      USB_OC_SKIP,   // Port 4.
      USB_OC_SKIP,   // Port 5.
      USB_OC_SKIP,   // Port 6.
      USB_OC_SKIP,   // Port 7.
      USB_OC_SKIP,   // Port 8.
      0,             // Port 9. USB3.0 PORT1 AOU    USB_OC_0
      USB_OC_SKIP }  // Port 10.
  })}
!endif
#[-end-210802-SHAONN0003-add]#
#[-end-210803-QINGLIN0008-add]#
!else
  gBoardModuleTokenSpaceGuid.VpdPcdUSB2OCMap|*|{CODE(
  { 10,
    { 4,             // Port 1. VW Overcurrent PIN:4
      5,             // Port 2. VW Overcurrent PIN:5
      6,             // Port 3. VW Overcurrent PIN:6
      USB_OC_SKIP,   // Port 4
      7,             // Port 5. VW Overcurrent PIN:7
      USB_OC_SKIP,   // Port 6
      0,             // Port 7
      0,             // Port 8
      3,             // Port 9
      USB_OC_SKIP }  // Port 10
  })}
!endif
#[-end-210721-QINGLIN0001-modify]#
[PcdsDynamicExVpd.common.SkuIdAdlPLp5Rvp]
#[-start-210628-BAIN000018-modify]#
!if $(LCFC_SUPPORT_ENABLE) == YES
!if $(C970_SUPPORT_ENABLE) == YES
  gBoardModuleTokenSpaceGuid.VpdPcdUSB3OCMap|*|{CODE(
  { 4,
    { 0,             // Port 1 TYPE-A PORT1 USB_OC_0
      USB_OC_SKIP,   // Port 2
      USB_OC_SKIP,   // Port 3
      USB_OC_SKIP }  // Port 4
  })}
!endif
# end C970_SUPPORT_ENABLE
#[start-210720-STORM1100-modify]#
!if $(C770_SUPPORT_ENABLE) == YES
# storm_poweron
#[-start-210728-YUNLEI0116-modify]
  gBoardModuleTokenSpaceGuid.VpdPcdUSB3OCMap|*|{CODE(
  { 4,
    { 0,             // Port 1 TYPE-A PORT1 USB_OC_0
      USB_OC_SKIP,   // Port 2
      USB_OC_SKIP,   // Port 3
      USB_OC_SKIP }  // Port 4
  })}
  gBoardModuleTokenSpaceGuid.VpdPcd16USB3OCMap|*|{CODE(
  { 4,
    { 0,             // Port 1 TYPE-A PORT1 USB_OC_0
      3,             // Port 2 TYPE-A PORT2 USB_OC_3
      USB_OC_SKIP,   // Port 3
      USB_OC_SKIP }  // Port 4
  })}
#[-end-210728-YUNLEI0116-modify]
!endif
#end C770_SUPPORT_ENABLE
#[end-210720-STORM1100-modify]#
#[-start-211014-Ching000013-modify]#
!if ($(S77014_SUPPORT_ENABLE) == YES) OR ($(S77014IAH_SUPPORT_ENABLE) == YES)
  gBoardModuleTokenSpaceGuid.VpdPcdUSB3OCMap|*|{CODE(
  { 4,
    { 0,             // Port 1 TYPE-A PORT1 USB_OC_0
      USB_OC_SKIP,   // Port 2
      USB_OC_SKIP,   // Port 3
      USB_OC_SKIP }  // Port 4
  })}
!endif
#end S77014_SUPPORT_ENABLE

!if $(S77013_SUPPORT_ENABLE) == YES
  gBoardModuleTokenSpaceGuid.VpdPcdUSB3OCMap|*|{CODE(
  { 4,
    { USB_OC_SKIP,   // Port 1
      USB_OC_SKIP,   // Port 2
      USB_OC_SKIP,   // Port 3
      USB_OC_SKIP }  // Port 4
  })}
!endif
#end S77013_SUPPORT_ENABLE
#[-end-211014-Ching000013-modify]#
!else
  gBoardModuleTokenSpaceGuid.VpdPcdUSB3OCMap|*|{CODE(
  { 4,
    { 3,             // Port 1
      0,             // Port 2
      0,             // Port 3
      USB_OC_SKIP }  // Port 4
  })}
!endif
#[-end-210628-BAIN000018-modify]#

[PcdsDynamicExVpd.common.SkuIdAdlPDdr5Rvp]
gBoardModuleTokenSpaceGuid.VpdPcdUSB3OCMap|*|{CODE(
{ 4,
  { 3,             // Port 1
    0,             // Port 2
    0,             // Port 3
    USB_OC_SKIP }  // Port 4
})}

[PcdsDynamicExVpd.common.SkuIdAdlPDdr4Rvp]
#[-start-210721-QINGLIN0001-modify]#
!if $(LCFC_SUPPORT_ENABLE) == YES
!if $(S570_SUPPORT_ENABLE) == YES
  gBoardModuleTokenSpaceGuid.VpdPcdUSB3OCMap|*|{CODE(
  { 4,
    { 3,             // Port 1 USB3.0 PORT1 Normal USB_OC_3
      USB_OC_SKIP,   // Port 2
      0,             // Port 3 USB3.0 PORT2 AOU    USB_OC_0
      USB_OC_SKIP }  // Port 4
  })}
!endif
#[-start-210803-QINGLIN0008-add]#
#[-start-210802-SHAONN0003-add]#
!if ($(S370_SUPPORT_ENABLE) == YES)
  gBoardModuleTokenSpaceGuid.VpdPcdUSB3OCMap|*|{CODE(
  { 4,
    { 0,             // Port 1 USB3.0 PORT1 AOU    USB_OC_0
      USB_OC_SKIP,   // Port 2
      USB_OC_SKIP,   // Port 3
      USB_OC_SKIP }  // Port 4
  })}
!endif
#[-end-210802-SHAONN0003-add]#
#[-end-210803-QINGLIN0008-add]#
!else
  gBoardModuleTokenSpaceGuid.VpdPcdUSB3OCMap|*|{CODE(
  { 4,
    { 3,             // Port 1
      0,             // Port 2
      0,             // Port 3
      USB_OC_SKIP }  // Port 4
  })}
!endif
#[-end-210721-QINGLIN0001-modify]#
[PcdsDynamicExVpd.common.SkuIdAdlPLp5Rvp]
#[-start-210628-BAIN000018-modify]#
!if $(LCFC_SUPPORT_ENABLE) == YES
!if $(C970_SUPPORT_ENABLE) == YES
  gBoardModuleTokenSpaceGuid.VpdPcdCpuUsb3OcMap|*|{CODE(
  { MAX_TCSS_USB3_PORTS,
    { 1,             // Port 1. TBT PortA     USB_OC_1
      1,             // Port 2. TBT PortB     USB_OC_1
      2,             // Port 3. USB-C PortC     USB_OC_2
      USB_OC_SKIP }  // Port 4.
  })}
!endif
# end C970_SUPPORT_ENABLE
#[start-210720-STORM1100-modify]#
!if $(C770_SUPPORT_ENABLE) == YES
# storm_poweron
#[-start-210728-YUNLEI0116-modify]
  gBoardModuleTokenSpaceGuid.VpdPcdCpuUsb3OcMap|*|{CODE(
  { MAX_TCSS_USB3_PORTS,
    { 1,             // Port 1. TBT PortA     USB_OC_1
      USB_OC_SKIP,   // Port 2. 
      1,             // Port 3. TBT PortB     USB_OC_1
      USB_OC_SKIP }  // Port 4.
  })}
#[-end-210728-YUNLEI0116-modify]
!endif
# end C770_SUPPORT_ENABLE
#[end-210720-STORM1100-modify]#
#[-start-211014-Ching000013-modify]#
!if ($(S77014_SUPPORT_ENABLE) == YES) OR ($(S77014IAH_SUPPORT_ENABLE) == YES)
  gBoardModuleTokenSpaceGuid.VpdPcdCpuUsb3OcMap|*|{CODE(
  { MAX_TCSS_USB3_PORTS,
    { 1,             // Port 1. TBT PortB     USB_OC_1 
      USB_OC_SKIP,   // Port 2.
      1,             // Port 3. TBT PortA     USB_OC_1
      USB_OC_SKIP }  // Port 4.
  })}
!endif
#end S77014_SUPPORT_ENABLE

!if $(S77013_SUPPORT_ENABLE) == YES
  gBoardModuleTokenSpaceGuid.VpdPcdCpuUsb3OcMap|*|{CODE(
  { MAX_TCSS_USB3_PORTS,
    { USB_OC_SKIP,   // Port 1.  
      1,             // Port 2. TBT PortA     USB_OC_1
      USB_OC_SKIP,   // Port 3.
      2 }            // Port 4. TBT PortB     USB_OC_2
  })}
!endif
#end S77013_SUPPORT_ENABLE
#[-end-211014-Ching000013-modify]#

!else
  gBoardModuleTokenSpaceGuid.VpdPcdCpuUsb3OcMap|*|{CODE(
  { MAX_TCSS_USB3_PORTS,
    { 4,             // Port 1. VW Overcurrent PIN:4
      5,             // Port 2. VW Overcurrent PIN:5
      6,             // Port 3. VW Overcurrent PIN:6
      7 }            // Port 4. VW Overcurrent PIN:7
  })}
!endif
#[-end-210628-BAIN000018-modify]#

[PcdsDynamicExVpd.common.SkuIdAdlPDdr5Rvp]
gBoardModuleTokenSpaceGuid.VpdPcdCpuUsb3OcMap|*|{CODE(
{ MAX_TCSS_USB3_PORTS,
  { 4,             // Port 1. VW Overcurrent PIN:4
    5,             // Port 2. VW Overcurrent PIN:5
    6,             // Port 3. VW Overcurrent PIN:6
    7 }            // Port 4. VW Overcurrent PIN:7
})}

[PcdsDynamicExVpd.common.SkuIdAdlPDdr4Rvp]
#[-start-210721-QINGLIN0001-modify]#
!if $(LCFC_SUPPORT_ENABLE) == YES
!if $(S570_SUPPORT_ENABLE) == YES
gBoardModuleTokenSpaceGuid.VpdPcdCpuUsb3OcMap|*|{CODE(
{ MAX_TCSS_USB3_PORTS,
  { 1,             // Port 1. TYPE-C PORT1        USB_OC_1
    1,             // Port 2. TYPE-C PORT1        USB_OC_1
    USB_OC_SKIP,   // Port 3.
    USB_OC_SKIP }  // Port 4.
})}
!endif
#[-start-210803-QINGLIN0008-add]#
#[-start-210802-SHAONN0003-add]#
!if ($(S370_SUPPORT_ENABLE) == YES)
gBoardModuleTokenSpaceGuid.VpdPcdCpuUsb3OcMap|*|{CODE(
{ MAX_TCSS_USB3_PORTS,
  { 1,             // Port 1. TYPE-C PORT1        USB_OC_1
    USB_OC_SKIP,   // Port 2.
    USB_OC_SKIP,   // Port 3.
    USB_OC_SKIP }  // Port 4.
})}
!endif
#[-end-210802-SHAONN0003-add]#
#[-end-210803-QINGLIN0008-add]#
!else
gBoardModuleTokenSpaceGuid.VpdPcdCpuUsb3OcMap|*|{CODE(
{ MAX_TCSS_USB3_PORTS,
  { 4,             // Port 1. VW Overcurrent PIN:4
    5,             // Port 2. VW Overcurrent PIN:5
    6,             // Port 3. VW Overcurrent PIN:6
    7 }            // Port 4. VW Overcurrent PIN:7
})}
!endif
#[-end-210721-QINGLIN0001-modify]#
[PcdsDynamicExVpd.common.SkuIdAdlPDdr5Dg384Aep]
gBoardModuleTokenSpaceGuid.VpdPcdUSB2OCMap|*|{CODE(
{ 10,
  { USB_OC_SKIP,   // Port 1
    USB_OC_SKIP,   // Port 2
    6,             // Port 3. VW Overcurrent PIN:6
    USB_OC_SKIP,   // Port 4
    7,             // Port 5. VW Overcurrent PIN:7
    USB_OC_SKIP,   // Port 6
    0,             // Port 7
    USB_OC_SKIP,   // Port 8
    3,             // Port 9
    USB_OC_SKIP }  // Port 10
})}

[PcdsDynamicExVpd.common.SkuIdAdlPDdr5Dg384Aep]
gBoardModuleTokenSpaceGuid.VpdPcdUSB3OCMap|*|{CODE(
{ 4,
  { 3,             // Port 1
    0,             // Port 2
    USB_OC_SKIP,   // Port 3
    USB_OC_SKIP }  // Port 4
})}

[PcdsDynamicExVpd.common.SkuIdAdlPDdr5Dg384Aep]
gBoardModuleTokenSpaceGuid.VpdPcdCpuUsb3OcMap|*|{CODE(
{ MAX_TCSS_USB3_PORTS,
  { USB_OC_SKIP,   // Port 1
    USB_OC_SKIP,   // Port 2
    6,             // Port 3. VW Overcurrent PIN:6
    7 }            // Port 4. VW Overcurrent PIN:7
})}

[PcdsDynamicExVpd.common.SkuIdAdlPLp5Aep]
gBoardModuleTokenSpaceGuid.VpdPcdUSB2OCMap|*|{CODE(
{ 10,
  { 4,             // Port 1. VW Overcurrent PIN:4
    5,             // Port 2. VW Overcurrent PIN:5
    6,             // Port 3. VW Overcurrent PIN:6
    USB_OC_SKIP,   // Port 4
    7,             // Port 5. VW Overcurrent PIN:7
    USB_OC_SKIP,   // Port 6
    USB_OC_SKIP,   // Port 7
    USB_OC_SKIP,   // Port 8
    USB_OC_SKIP,   // Port 9
    USB_OC_SKIP }  // Port 10
})}

[PcdsDynamicExVpd.common.SkuIdAdlPLp5Aep]
gBoardModuleTokenSpaceGuid.VpdPcdUSB3OCMap|*|{CODE(
{ 4,
  { USB_OC_SKIP,   // Port 1
    USB_OC_SKIP,   // Port 2
    USB_OC_SKIP,   // Port 3
    USB_OC_SKIP }  // Port 4
})}

[PcdsDynamicExVpd.common.SkuIdAdlPLp5Aep]
gBoardModuleTokenSpaceGuid.VpdPcdCpuUsb3OcMap|*|{CODE(
{ MAX_TCSS_USB3_PORTS,
  { 4,             // Port 1. VW Overcurrent PIN:4
    5,             // Port 2. VW Overcurrent PIN:5
    6,             // Port 3. VW Overcurrent PIN:6
    7 }            // Port 4. VW Overcurrent PIN:7
})}

[PcdsDynamicExVpd.common.SkuIdAdlPMMAep]
gBoardModuleTokenSpaceGuid.VpdPcdUSB2OCMap|*|{CODE(
{ 10,
  { USB_OC_SKIP,             // Port 1. VW Overcurrent PIN:4
    USB_OC_SKIP,             // Port 2. VW Overcurrent PIN:5
    6,             // Port 3. VW Overcurrent PIN:6
    USB_OC_SKIP,   // Port 4
    7,             // Port 5. VW Overcurrent PIN:7
    USB_OC_SKIP,   // Port 6
    4,             // Port 7
    USB_OC_SKIP,   // Port 8
    USB_OC_SKIP,   // Port 9
    USB_OC_SKIP }  // Port 10
})}

[PcdsDynamicExVpd.common.SkuIdAdlPMMAep]
gBoardModuleTokenSpaceGuid.VpdPcdUSB3OCMap|*|{CODE(
{ 4,
  { USB_OC_SKIP,   // Port 1
    4,             // Port 2
    USB_OC_SKIP,   // Port 3
    USB_OC_SKIP }  // Port 4
})}

[PcdsDynamicExVpd.common.SkuIdAdlPLp5Dg128Aep]
gBoardModuleTokenSpaceGuid.VpdPcdUSB2OCMap|*|{CODE(
{ 10,
  { USB_OC_SKIP,   // Port 1
    5,             // Port 2. VW Overcurrent PIN:5
    6,             // Port 3. VW Overcurrent PIN:6
    USB_OC_SKIP,   // Port 4
    7,             // Port 5. VW Overcurrent PIN:7
    USB_OC_SKIP,   // Port 6
    3,             // Port 7
    USB_OC_SKIP,   // Port 8
    USB_OC_SKIP,   // Port 9
    USB_OC_SKIP }  // Port 10
})}

[PcdsDynamicExVpd.common.SkuIdAdlPLp5Dg128Aep]
gBoardModuleTokenSpaceGuid.VpdPcdUSB3OCMap|*|{CODE(
{ 4,
  { USB_OC_SKIP,   // Port 1
    USB_OC_SKIP,   // Port 2
    USB_OC_SKIP,   // Port 3
    3 }            // Port 4
})}

[PcdsDynamicExVpd.common.SkuIdAdlPLp5Dg128Aep]
gBoardModuleTokenSpaceGuid.VpdPcdCpuUsb3OcMap|*|{CODE(
{ MAX_TCSS_USB3_PORTS,
  { USB_OC_SKIP,   // Port 1
    5,             // Port 2. VW Overcurrent PIN:5
    6,             // Port 3. VW Overcurrent PIN:6
    7 }            // Port 4. VW Overcurrent PIN:7
})}

[PcdsDynamicExVpd.common.SkuIdAdlPLp5Gcs]
gBoardModuleTokenSpaceGuid.VpdPcdUSB2OCMap|*|{CODE(
{ 10,
  { USB_OC_SKIP,   // Port 1.
    USB_OC_SKIP,   // Port 2.
    6,             // Port 3. VW Overcurrent PIN:6
    USB_OC_SKIP,   // Port 4
    7,             // Port 5. VW Overcurrent PIN:7
    USB_OC_SKIP,   // Port 6
    USB_OC_SKIP,   // Port 7
    0,             // Port 8
    USB_OC_SKIP,   // Port 9
    USB_OC_SKIP }  // Port 10
})}

[PcdsDynamicExVpd.common.SkuIdAdlPLp5Gcs]
gBoardModuleTokenSpaceGuid.VpdPcdUSB3OCMap|*|{CODE(
{ 4,
  { USB_OC_SKIP,   // Port 1
    USB_OC_SKIP,   // Port 2
    0          ,   // Port 3
    USB_OC_SKIP }  // Port 4
})}

[PcdsDynamicExVpd.common.SkuIdAdlPLp5Gcs]
gBoardModuleTokenSpaceGuid.VpdPcdCpuUsb3OcMap|*|{CODE(
{ MAX_TCSS_USB3_PORTS,
  { USB_OC_SKIP,   // Port 1. VW Overcurrent PIN:4
    USB_OC_SKIP,   // Port 2. VW Overcurrent PIN:5
    6,             // Port 3. VW Overcurrent PIN:6
    7 }            // Port 4. VW Overcurrent PIN:7
})}

[PcdsDynamicExVpd.common.SkuIdAdlPDdr5MRRvp]
gBoardModuleTokenSpaceGuid.VpdPcdUSB2OCMap|*|{CODE(
{ 10,
  { 0,             // Port 1.
    0,             // Port 2.
    3,             // Port 3.
    USB_OC_SKIP,   // Port 4
    3,             // Port 5.
    USB_OC_SKIP,   // Port 6
    0,             // Port 7
    0,             // Port 8.
    3,             // Port 9
    USB_OC_SKIP }  // Port 10
})}

[PcdsDynamicExVpd.common.SkuIdAdlPDdr5MRRvp]
gBoardModuleTokenSpaceGuid.VpdPcdUSB3OCMap|*|{CODE(
{ 4,
  { 3,             // Port 1
    0,             // Port 2
    USB_OC_SKIP,   // Port 3
    USB_OC_SKIP }  // Port 4
})}

[PcdsDynamicExVpd.common.SkuIdAdlPDdr5MRRvp]
gBoardModuleTokenSpaceGuid.VpdPcdCpuUsb3OcMap|*|{CODE(
{ MAX_TCSS_USB3_PORTS,
  { 0,             // Port 1.
    USB_OC_SKIP,   // Port 2.
    USB_OC_SKIP,   // Port 3.
    USB_OC_SKIP}   // Port 4.
})}