## @file
#  Alderlake M USB OC configuration file.
#
# @copyright
#  INTEL CONFIDENTIAL
#  Copyright 2020 - 2021 Intel Corporation.
#
#  The source code contained or described herein and all documents related to the
#  source code ("Material") are owned by Intel Corporation or its suppliers or
#  licensors. Title to the Material remains with Intel Corporation or its suppliers
#  and licensors. The Material may contain trade secrets and proprietary and
#  confidential information of Intel Corporation and its suppliers and licensors,
#  and is protected by worldwide copyright and trade secret laws and treaty
#  provisions. No part of the Material may be used, copied, reproduced, modified,
#  published, uploaded, posted, transmitted, distributed, or disclosed in any way
#  without Intel's prior express written permission.
#
#  No license under any patent, copyright, trade secret or other intellectual
#  property right is granted to or conferred upon you by disclosure or delivery
#  of the Materials, either expressly, by implication, inducement, estoppel or
#  otherwise. Any license under such intellectual property rights must be
#  express and approved by Intel in writing.
#
#  Unless otherwise agreed by Intel in writing, you may not remove or alter
#  this notice or any other notice embedded in Materials by Intel or
#  Intel's suppliers or licensors in any way.
#
#  This file contains a 'Sample Driver' and is licensed as such under the terms
#  of your license agreement with Intel or your vendor. This file may be modified
#  by the user, subject to the additional terms of the license agreement.
#
# @par Specification
##

[PcdsDynamicExVpd.common.SkuIdAdlMLp4Rvp]
gBoardModuleTokenSpaceGuid.VpdPcdUSB2OCMap|*|{CODE(
{ 10,
  { 4,            // Port 1 VW OC PIN: 4
    5,            // Port 2 VW OC PIN: 5
    USB_OC_SKIP,  // Port 3
    USB_OC_SKIP,  // Port 4
    2,            // Port 5
    2,            // Port 6
    USB_OC_SKIP,  // Port 7
    USB_OC_SKIP,  // Port 8
    USB_OC_SKIP,  // Port 9
    USB_OC_SKIP}  // Port 10
})}

[PcdsDynamicExVpd.common.SkuIdAdlMLp4Rvp]
gBoardModuleTokenSpaceGuid.VpdPcdUSB3OCMap|*|{CODE(
{ 4,
  { 2,            // Port 1
    2,            // Port 2
    USB_OC_SKIP,  // Port 3
    USB_OC_SKIP}  // Port 4
})}

[PcdsDynamicExVpd.common.SkuIdAdlMLp4Rvp]
gBoardModuleTokenSpaceGuid.VpdPcdCpuUsb3OcMap|*|{CODE(
{ MAX_TCSS_USB3_PORTS,
  { 4,            // Port 1
    5,            // Port 2
    USB_OC_SKIP,  // Port 3
    USB_OC_SKIP}  // Port 4
})}

[PcdsDynamicExVpd.common.SkuIdAdlMLp5Aep]
gBoardModuleTokenSpaceGuid.VpdPcdUSB2OCMap|*|{CODE(
{ 10,
  { 4,            // Port 1 VW OC PIN: 4
    5,            // Port 2 VW OC PIN: 5
    USB_OC_SKIP,  // Port 3
    USB_OC_SKIP,  // Port 4
    USB_OC_SKIP,  // Port 5
    USB_OC_SKIP,  // Port 6
    USB_OC_SKIP,  // Port 7
    USB_OC_SKIP,  // Port 8
    USB_OC_SKIP,  // Port 9
    USB_OC_SKIP}  // Port 10
})}

[PcdsDynamicExVpd.common.SkuIdAdlMLp5Aep]
gBoardModuleTokenSpaceGuid.VpdPcdUSB3OCMap|*|{CODE(
{ 4,
  { USB_OC_SKIP,  // Port 1
    USB_OC_SKIP,  // Port 2
    USB_OC_SKIP,  // Port 3
    USB_OC_SKIP}  // Port 4
})}

