## @file
#  Alderlake P RVP GPIO definition table for Pre-Memory Initialization
#
# @copyright
#  INTEL CONFIDENTIAL
#  Copyright 2020 - 2021 Intel Corporation.
#
#  The source code contained or described herein and all documents related to the
#  source code ("Material") are owned by Intel Corporation or its suppliers or
#  licensors. Title to the Material remains with Intel Corporation or its suppliers
#  and licensors. The Material may contain trade secrets and proprietary and
#  confidential information of Intel Corporation and its suppliers and licensors,
#  and is protected by worldwide copyright and trade secret laws and treaty
#  provisions. No part of the Material may be used, copied, reproduced, modified,
#  published, uploaded, posted, transmitted, distributed, or disclosed in any way
#  without Intel's prior express written permission.
#
#  No license under any patent, copyright, trade secret or other intellectual
#  property right is granted to or conferred upon you by disclosure or delivery
#  of the Materials, either expressly, by implication, inducement, estoppel or
#  otherwise. Any license under such intellectual property rights must be
#  express and approved by Intel in writing.
#
#  Unless otherwise agreed by Intel in writing, you may not remove or alter
#  this notice or any other notice embedded in Materials by Intel or
#  Intel's suppliers or licensors in any way.
#
#  This file contains a 'Sample Driver' and is licensed as such under the terms
#  of your license agreement with Intel or your vendor. This file may be modified
#  by the user, subject to the additional terms of the license agreement.
#
# @par Specification
##

###
### !!! GPIOs designated to Native Functions shall not be configured by Platform Code.
### Native Pins shall be configured by Silicon Code (based on BIOS policies setting) or soft straps(set by CSME in FITc).
###
###

# mGpioTablePreMemAdlPLp4Rvp
[PcdsDynamicExVpd.common.SkuIdAdlPLp4Rvp]
gBoardModuleTokenSpaceGuid.VpdPcdBoardGpioTablePreMem|*|{CODE({
  // CPU M.2 SSD
  { GPIO_VER2_LP_GPP_D14, { GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut, GpioOutHigh, GpioIntDis, GpioPlatformReset, GpioTermNone } },  //CPU SSD PWREN
  { GPIO_VER2_LP_GPP_F20, { GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut, GpioOutHigh, GpioIntDis, GpioPlatformReset, GpioTermNone } },  //CPU SSD RESET

  // CPU M.2 SSD2
  {GPIO_VER2_LP_GPP_C2, {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutHigh,   GpioIntDis,GpioPlatformReset,  GpioTermNone}},  //CPU SSD2 PWREN
  {GPIO_VER2_LP_GPP_H1, {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutHigh,   GpioIntDis,GpioPlatformReset,  GpioTermNone}},  //CPU SSD2 RESET

  // X4 Pcie Slot for Gen3 and Gen 4
  {GPIO_VER2_LP_GPP_H17,  {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutLow,   GpioIntDis,GpioPlatformReset,  GpioTermNone}},//ONBOARD_X4_PCIE_SLOT1_PWREN_N
  {GPIO_VER2_LP_GPP_F10, {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutHigh,   GpioIntDis,GpioPlatformReset,  GpioTermNone}},//ONBOARD_X4_PCIE_SLOT1_RESET_N
  //X4_PCIE_SLOT_WAKE_N : Will be used as WAKE only based on specfic request,if we are validating Non POR config, By default Wake for x4 slot will be GPD_2_LAN_WAKEB
  //{GPIO_VER2_LP_GPP_A20,  {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirInInv,  GpioOutDefault,GpioIntLevel|GpioIntSci,GpioHostDeepReset,  GpioTermNone,  GpioPadConfigUnlock  }},  //ONBOARD_X4_PCIE_SLOT1_WAKE_N

  //RTD3 GPIO header for PEG Slot
  {GPIO_VER2_LP_GPP_A11,   {GpioPadModeGpio,  GpioHostOwnDefault, GpioDirOut,   GpioOutHigh,    GpioIntDefault,                 GpioPlatformReset, GpioTermNone, GpioPadConfigUnlock }}, //PEG_SLOT_DGPU_SEL_N
  {GPIO_VER2_LP_GPP_A14,   {GpioPadModeGpio,  GpioHostOwnAcpi,    GpioDirOut,   GpioOutLow,     GpioIntDefault,                 GpioPlatformReset, GpioTermNone }}, // PEG_SLOT_DGPU_PWR_EN_N
  {GPIO_VER2_LP_GPP_B2,    {GpioPadModeGpio,  GpioHostOwnDefault, GpioDirOut,   GpioOutHigh,    GpioIntDefault,                 GpioPlatformReset, GpioTermNone }}, // PEG_SLOT_RST_N
  {GPIO_VER2_LP_GPP_A19,   {GpioPadModeGpio,  GpioHostOwnDefault, GpioDirIn,    GpioOutDefault, GpioIntDefault,                 GpioPlatformReset, GpioTermNone }}, // PEG_SLOT_DGPU_PWR_OK
  {GPIO_VER2_LP_GPP_A20,   {GpioPadModeGpio,  GpioHostOwnAcpi,    GpioDirInInv, GpioOutDefault, GpioIntEdge | GpioIntSci,       GpioHostDeepReset, GpioTermNone , GpioPadConfigUnlock }}, // PEG_SLOT_WAKE_N
  {GPIO_VER2_LP_GPP_D17,   {GpioPadModeGpio,  GpioHostOwnDefault, GpioDirOut,   GpioOutHigh,    GpioIntDefault,                 GpioHostDeepReset, GpioTermNone, GpioPadConfigUnlock }}, // PEG_RTD3_COLD_MOD

  // TBT Re-Timers
  {GPIO_VER2_LP_GPP_E4, {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutLow,   GpioIntDis,GpioPlatformReset,  GpioTermNone}},  //TC_RETIMER_FORCE_PWR

  {0x0}  // terminator
})}


# mGpioTablePreMemAdlPLp4Bep
[PcdsDynamicExVpd.common.SkuIdAdlPLp4Bep]
gBoardModuleTokenSpaceGuid.VpdPcdBoardGpioTablePreMem|*|{CODE({
  // CPU M.2 SSD
  { GPIO_VER2_LP_GPP_D16, { GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutHigh,    GpioIntDis,                   GpioPlatformReset, GpioTermNone } },  //CPU SSD PWREN
  { GPIO_VER2_LP_GPP_F20, { GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutLow,    GpioIntDis,                   GpioPlatformReset, GpioTermNone } },  //CPU SSD RESET

  // TBT Re-Timer
  { GPIO_VER2_LP_GPP_E4,  { GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutLow,     GpioIntDis,                   GpioPlatformReset, GpioTermNone } },  //TC_RETIMER_FORCE_PWR


  {0x0}  // terminator
})}

# mGpioTablePreMemAdlPLp5Rvp
[PcdsDynamicExVpd.common.SkuIdAdlPLp5Rvp]
gBoardModuleTokenSpaceGuid.VpdPcdBoardGpioTablePreMem|*|{CODE({
  // CPU M.2 SSD
  { GPIO_VER2_LP_GPP_D14, { GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut, GpioOutHigh, GpioIntDis, GpioPlatformReset, GpioTermNone } },  //CPU SSD PWREN
  { GPIO_VER2_LP_GPP_F20, { GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut, GpioOutHigh, GpioIntDis, GpioPlatformReset, GpioTermNone } },  //CPU SSD RESET

  // CPU M.2 SSD2
  {GPIO_VER2_LP_GPP_C2, {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutHigh,   GpioIntDis,GpioPlatformReset,  GpioTermNone}},  //CPU SSD2 PWREN
  {GPIO_VER2_LP_GPP_H1, {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutHigh,   GpioIntDis,GpioPlatformReset,  GpioTermNone}},  //CPU SSD2 RESET

  // X4 Pcie Slot for Gen3 and Gen 4
  {GPIO_VER2_LP_GPP_H17,  {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutLow,   GpioIntDis,GpioPlatformReset,  GpioTermNone}},//ONBOARD_X4_PCIE_SLOT1_PWREN_N
  {GPIO_VER2_LP_GPP_F10, {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutHigh,   GpioIntDis,GpioPlatformReset,  GpioTermNone}},//ONBOARD_X4_PCIE_SLOT1_RESET_N
  //X4_PCIE_SLOT_WAKE_N : Will be used as WAKE only based on specfic request,if we are validating Non POR config, By default Wake for x4 slot will be GPD_2_LAN_WAKEB
  //{GPIO_VER2_LP_GPP_A20,  {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirInInv,  GpioOutDefault,GpioIntLevel|GpioIntSci,GpioHostDeepReset,  GpioTermNone,  GpioPadConfigUnlock  }},  //ONBOARD_X4_PCIE_SLOT1_WAKE_N

  //RTD3 GPIO header for PEG Slot
  {GPIO_VER2_LP_GPP_A11,   {GpioPadModeGpio,  GpioHostOwnDefault, GpioDirOut,   GpioOutHigh,    GpioIntDefault,                 GpioPlatformReset, GpioTermNone, GpioPadConfigUnlock }}, //PEG_SLOT_DGPU_SEL_N
  {GPIO_VER2_LP_GPP_A14,   {GpioPadModeGpio,  GpioHostOwnAcpi,    GpioDirOut,   GpioOutLow,     GpioIntDefault,                 GpioPlatformReset, GpioTermNone }}, // PEG_SLOT_DGPU_PWR_EN_N
  {GPIO_VER2_LP_GPP_B2,    {GpioPadModeGpio,  GpioHostOwnDefault, GpioDirOut,   GpioOutHigh,    GpioIntDefault,                 GpioPlatformReset, GpioTermNone }}, // PEG_SLOT_RST_N
  {GPIO_VER2_LP_GPP_A19,   {GpioPadModeGpio,  GpioHostOwnDefault, GpioDirIn,    GpioOutDefault, GpioIntDefault,                 GpioPlatformReset, GpioTermNone }}, // PEG_SLOT_DGPU_PWR_OK
  {GPIO_VER2_LP_GPP_A20,   {GpioPadModeGpio,  GpioHostOwnAcpi,    GpioDirInInv, GpioOutDefault, GpioIntEdge | GpioIntSci,       GpioHostDeepReset, GpioTermNone , GpioPadConfigUnlock }}, // PEG_SLOT_WAKE_N
  {GPIO_VER2_LP_GPP_D17,   {GpioPadModeGpio,  GpioHostOwnDefault, GpioDirOut,   GpioOutHigh,    GpioIntDefault,                 GpioHostDeepReset, GpioTermNone, GpioPadConfigUnlock }}, // PEG_RTD3_COLD_MOD

  // TBT Re-Timers
  {GPIO_VER2_LP_GPP_E4, {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutLow,   GpioIntDis,GpioPlatformReset,  GpioTermNone}},  //TC_RETIMER_FORCE_PWR

  {0x0}  // terminator
})}

# mGpioTablePreMemAdlPDdr5Rvp
[PcdsDynamicExVpd.common.SkuIdAdlPDdr5Rvp]
gBoardModuleTokenSpaceGuid.VpdPcdBoardGpioTablePreMem|*|{CODE({
  // CPU M.2 SSD
  { GPIO_VER2_LP_GPP_D14, { GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut, GpioOutHigh, GpioIntDis, GpioPlatformReset, GpioTermNone } },  //CPU SSD PWREN
  { GPIO_VER2_LP_GPP_F20, { GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut, GpioOutHigh, GpioIntDis, GpioPlatformReset, GpioTermNone } },  //CPU SSD RESET

  // CPU M.2 SSD2
  {GPIO_VER2_LP_GPP_C2, {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutHigh,   GpioIntDis,GpioPlatformReset,  GpioTermNone}},  //CPU SSD2 PWREN
  {GPIO_VER2_LP_GPP_H1, {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutHigh,   GpioIntDis,GpioPlatformReset,  GpioTermNone}},  //CPU SSD2 RESET

  // X4 Pcie Slot for Gen3 and Gen 4
  {GPIO_VER2_LP_GPP_H17,  {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutLow,   GpioIntDis,GpioPlatformReset,  GpioTermNone}},//ONBOARD_X4_PCIE_SLOT1_PWREN_N
  {GPIO_VER2_LP_GPP_F10, {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutHigh,   GpioIntDis,GpioPlatformReset,  GpioTermNone}},//ONBOARD_X4_PCIE_SLOT1_RESET_N
  //X4_PCIE_SLOT_WAKE_N : Will be used as WAKE only based on specfic request,if we are validating Non POR config, By default Wake for x4 slot will be GPD_2_LAN_WAKEB
  //{GPIO_VER2_LP_GPP_A20,  {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirInInv,  GpioOutDefault,GpioIntLevel|GpioIntSci,GpioHostDeepReset,  GpioTermNone,  GpioPadConfigUnlock  }},  //ONBOARD_X4_PCIE_SLOT1_WAKE_N

  //RTD3 GPIO header for PEG Slot
  {GPIO_VER2_LP_GPP_A11,   {GpioPadModeGpio,  GpioHostOwnDefault, GpioDirOut,   GpioOutHigh,    GpioIntDefault,                 GpioPlatformReset, GpioTermNone, GpioPadConfigUnlock }}, //PEG_SLOT_DGPU_SEL_N
  {GPIO_VER2_LP_GPP_A14,   {GpioPadModeGpio,  GpioHostOwnAcpi,    GpioDirOut,   GpioOutLow,     GpioIntDefault,                 GpioPlatformReset, GpioTermNone }}, // PEG_SLOT_DGPU_PWR_EN_N
  {GPIO_VER2_LP_GPP_B2,    {GpioPadModeGpio,  GpioHostOwnDefault, GpioDirOut,   GpioOutHigh,    GpioIntDefault,                 GpioPlatformReset, GpioTermNone }}, // PEG_SLOT_RST_N
  {GPIO_VER2_LP_GPP_A19,   {GpioPadModeGpio,  GpioHostOwnDefault, GpioDirIn,    GpioOutDefault, GpioIntDefault,                 GpioPlatformReset, GpioTermNone }}, // PEG_SLOT_DGPU_PWR_OK
  {GPIO_VER2_LP_GPP_A20,   {GpioPadModeGpio,  GpioHostOwnAcpi,    GpioDirInInv, GpioOutDefault, GpioIntEdge | GpioIntSci,       GpioHostDeepReset, GpioTermNone, GpioPadConfigUnlock }}, // PEG_SLOT_WAKE_N
  {GPIO_VER2_LP_GPP_D17,   {GpioPadModeGpio,  GpioHostOwnDefault, GpioDirOut,   GpioOutHigh,    GpioIntDefault,                 GpioHostDeepReset, GpioTermNone, GpioPadConfigUnlock }}, // PEG_RTD3_COLD_MOD

  // TBT Re-Timers
  {GPIO_VER2_LP_GPP_E4,    {GpioPadModeGpio,  GpioHostOwnAcpi,     GpioDirOut,  GpioOutLow,      GpioIntDis,                    GpioPlatformReset, GpioTermNone}},  //TC_RETIMER_FORCE_PWR

  {0x0}  // terminator
})}



# mGpioTablePreMemAdlPDdr4Rvp
[PcdsDynamicExVpd.common.SkuIdAdlPDdr4Rvp]
gBoardModuleTokenSpaceGuid.VpdPcdBoardGpioTablePreMem|*|{CODE({
  // CPU M.2 SSD
  { GPIO_VER2_LP_GPP_D14, { GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut, GpioOutHigh, GpioIntDis, GpioPlatformReset, GpioTermNone } },  //CPU SSD PWREN
  { GPIO_VER2_LP_GPP_F20, { GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut, GpioOutHigh, GpioIntDis, GpioPlatformReset, GpioTermNone } },  //CPU SSD RESET

  // CPU M.2 SSD2
  {GPIO_VER2_LP_GPP_C2, {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutHigh,   GpioIntDis,GpioPlatformReset,  GpioTermNone}},  //CPU SSD2 PWREN
  {GPIO_VER2_LP_GPP_H1, {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutHigh,   GpioIntDis,GpioPlatformReset,  GpioTermNone}},  //CPU SSD2 RESET

  // X4 Pcie Slot for Gen3 and Gen 4
  {GPIO_VER2_LP_GPP_H17,  {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutLow,   GpioIntDis,GpioPlatformReset,  GpioTermNone}},//ONBOARD_X4_PCIE_SLOT1_PWREN_N
  {GPIO_VER2_LP_GPP_F10, {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutHigh,   GpioIntDis,GpioPlatformReset,  GpioTermNone}},//ONBOARD_X4_PCIE_SLOT1_RESET_N
  //X4_PCIE_SLOT_WAKE_N : Will be used as WAKE only based on specfic request,if we are validating Non POR config, By default Wake for x4 slot will be GPD_2_LAN_WAKEB
  //{GPIO_VER2_LP_GPP_A20,  {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirInInv,  GpioOutDefault,GpioIntLevel|GpioIntSci,GpioHostDeepReset,  GpioTermNone,  GpioPadConfigUnlock  }},  //ONBOARD_X4_PCIE_SLOT1_WAKE_N

  //RTD3 GPIO header for PEG Slot
  {GPIO_VER2_LP_GPP_A11,   {GpioPadModeGpio,  GpioHostOwnDefault, GpioDirOut,   GpioOutHigh,    GpioIntDefault,                 GpioPlatformReset, GpioTermNone, GpioPadConfigUnlock }}, //PEG_SLOT_DGPU_SEL_N
  {GPIO_VER2_LP_GPP_A14,   {GpioPadModeGpio,  GpioHostOwnAcpi,    GpioDirOut,   GpioOutLow,     GpioIntDefault,                 GpioPlatformReset, GpioTermNone }}, // PEG_SLOT_DGPU_PWR_EN_N
  {GPIO_VER2_LP_GPP_B2,    {GpioPadModeGpio,  GpioHostOwnDefault, GpioDirOut,   GpioOutHigh,    GpioIntDefault,                 GpioPlatformReset, GpioTermNone }}, // PEG_SLOT_RST_N
  {GPIO_VER2_LP_GPP_A19,   {GpioPadModeGpio,  GpioHostOwnDefault, GpioDirIn,    GpioOutDefault, GpioIntDefault,                 GpioPlatformReset, GpioTermNone }}, // PEG_SLOT_DGPU_PWR_OK
  {GPIO_VER2_LP_GPP_A20,   {GpioPadModeGpio,  GpioHostOwnAcpi,    GpioDirInInv, GpioOutDefault, GpioIntEdge | GpioIntSci,       GpioHostDeepReset, GpioTermNone, GpioPadConfigUnlock }}, // PEG_SLOT_WAKE_N
  {GPIO_VER2_LP_GPP_D17,   {GpioPadModeGpio,  GpioHostOwnDefault, GpioDirOut,   GpioOutHigh,    GpioIntDefault,                 GpioHostDeepReset, GpioTermNone, GpioPadConfigUnlock }}, // PEG_RTD3_COLD_MOD

  // TBT Re-Timers
  {GPIO_VER2_LP_GPP_E4,    {GpioPadModeGpio,  GpioHostOwnAcpi,     GpioDirOut,  GpioOutLow,      GpioIntDis,                    GpioPlatformReset, GpioTermNone}},  //TC_RETIMER_FORCE_PWR

  {0x0}  // terminator
})}

# mGpioTablePreMemAdlPDdr5Dg384Aep
[PcdsDynamicExVpd.common.SkuIdAdlPDdr5Dg384Aep]
gBoardModuleTokenSpaceGuid.VpdPcdBoardGpioTablePreMem|*|{CODE({
  // CPU M.2 SSD2
  {GPIO_VER2_LP_GPP_C2,  {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut, GpioOutHigh, GpioIntDis, GpioPlatformReset, GpioTermNone}},  //CPU SSD2 PWREN
  {GPIO_VER2_LP_GPP_F21, {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut, GpioOutLow,  GpioIntDis, GpioPlatformReset, GpioTermNone}},  //CPU SSD2 EXT_PWR_GATE2B

  // PCH M.2 SSD
  {GPIO_VER2_LP_GPP_D16, {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut, GpioOutHigh, GpioIntDis, GpioPlatformReset, GpioTermNone}},  //CPU SSD PWREN
  {GPIO_VER2_LP_GPP_H0,  {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut, GpioOutLow,  GpioIntDis, GpioPlatformReset, GpioTermNone}},  //CPU SSD RESET

  // TBT Re-Timers
  {GPIO_VER2_LP_GPP_E4,  {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut, GpioOutLow,  GpioIntDis, GpioPlatformReset, GpioTermNone}},  //TC_RETIMER_FORCE_PWR

  //RTD3 GPIO header for PEG Slot
  {GPIO_VER2_LP_GPP_A11, {GpioPadModeGpio, GpioHostOwnDefault, GpioDirOut,   GpioOutHigh,    GpioIntDefault,          GpioPlatformReset, GpioTermNone, GpioPadConfigUnlock}},  //GPU_PROCHOT_N
  {GPIO_VER2_LP_GPP_A14, {GpioPadModeGpio, GpioHostOwnGpio,    GpioDirOut,   GpioOutHigh,    GpioIntDefault,          GpioPlatformReset, GpioTermNone}},  //RTD3_COLD_POR
  {GPIO_VER2_LP_GPP_A15, {GpioPadModeGpio, GpioHostOwnDefault, GpioDirOut,   GpioOutLow,     GpioIntDefault,          GpioPlatformReset, GpioTermNone}},  //GPU_PERST_N
  {GPIO_VER2_LP_GPP_A19, {GpioPadModeGpio, GpioHostOwnDefault, GpioDirIn,    GpioOutDefault, GpioIntDefault,          GpioPlatformReset, GpioTermNone}},  //GPU_VCCGT_VR_PG_R
  {GPIO_VER2_LP_GPP_A20, {GpioPadModeGpio, GpioHostOwnAcpi,    GpioDirInInv, GpioOutDefault, GpioIntEdge|GpioIntSci,  GpioHostDeepReset, GpioTermNone, GpioPadConfigUnlock}},  //GPU_PCIE_WAKE_N

  {0x0}  // terminator
})}

# mGpioTablePreMemAdlPMMAep
[PcdsDynamicExVpd.common.SkuIdAdlPMMAep]
gBoardModuleTokenSpaceGuid.VpdPcdBoardGpioTablePreMem|*|{CODE({
  // CPU M.2 SSD2
  {GPIO_VER2_LP_GPP_F13, {GpioPadModeGpio, GpioHostOwnGpio,    GpioDirOut,   GpioOutHigh,    GpioIntDis,              GpioPlatformReset, GpioTermNone}},  //CPU SSD2 PWREN
  {GPIO_VER2_LP_GPP_F21, {GpioPadModeGpio, GpioHostOwnGpio,    GpioDirOut,   GpioOutLow,     GpioIntDis,              GpioPlatformReset, GpioTermNone}},  //CPU SSD2 EXT_PWR_GATE2B

  // PCH M.2 SSD ok
  {GPIO_VER2_LP_GPP_D16, {GpioPadModeGpio, GpioHostOwnGpio,    GpioDirOut,   GpioOutHigh,    GpioIntDis,              GpioPlatformReset, GpioTermNone}},  //CPU SSD PWREN
  {GPIO_VER2_LP_GPP_H0,  {GpioPadModeGpio, GpioHostOwnGpio,    GpioDirOut,   GpioOutLow,     GpioIntDis,              GpioPlatformReset, GpioTermNone}},  //CPU SSD RESET

  // TBT Re-Timers
  {GPIO_VER2_LP_GPP_E4,  {GpioPadModeGpio, GpioHostOwnGpio,    GpioDirOut,   GpioOutLow,     GpioIntDis,              GpioPlatformReset, GpioTermNone}},  //TC_RETIMER_FORCE_PWR

  //RTD3 GPIO header for PEG Slot
  {GPIO_VER2_LP_GPP_A11, {GpioPadModeGpio, GpioHostOwnDefault, GpioDirOut,   GpioOutHigh,    GpioIntDefault,          GpioPlatformReset, GpioTermNone, GpioPadConfigUnlock}},  //GPU_PROCHOT_N
  {GPIO_VER2_LP_GPP_A14, {GpioPadModeGpio, GpioHostOwnGpio,    GpioDirOut,   GpioOutHigh,    GpioIntDefault,          GpioPlatformReset, GpioTermNone}},  //RTD3_COLD_POR
  {GPIO_VER2_LP_GPP_A15, {GpioPadModeGpio, GpioHostOwnDefault, GpioDirOut,   GpioOutLow,     GpioIntDefault,          GpioPlatformReset, GpioTermNone}},  //GPU_PERST_N
  {GPIO_VER2_LP_GPP_A19, {GpioPadModeGpio, GpioHostOwnDefault, GpioDirIn,    GpioOutDefault, GpioIntDefault,          GpioPlatformReset, GpioTermNone}},  //GPU_VCCGT_VR_PG_R
  {GPIO_VER2_LP_GPP_A20, {GpioPadModeGpio, GpioHostOwnAcpi,    GpioDirInInv, GpioOutDefault, GpioIntEdge|GpioIntSci, GpioHostDeepReset, GpioTermNone, GpioPadConfigUnlock}},  //GPU_PCIE_WAKE_N

  {0x0}  // terminator
})}

[PcdsDynamicExVpd.common.SkuIdAdlPLp5Aep]
gBoardModuleTokenSpaceGuid.VpdPcdBoardGpioTablePreMem|*|{CODE({

// CPU M.2 SSD
  { GPIO_VER2_LP_GPP_D16, { GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut, GpioOutHigh, GpioIntDis, GpioPlatformReset, GpioTermNone } },  //CPU SSD PWREN
  { GPIO_VER2_LP_GPP_F20, { GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut, GpioOutLow,  GpioIntDis, GpioPlatformReset, GpioTermNone } },  //CPU SSD RESET

  // TBT Re-Timers
  {GPIO_VER2_LP_GPP_E4,  {GpioPadModeGpio,  GpioHostOwnGpio,  GpioDirOut, GpioOutLow,  GpioIntDis, GpioPlatformReset, GpioTermNone  }},  //TC_RETIMER_FORCE_PWR

  {0x0}  // terminator
})}

# mGpioTablePreMemAdlPLp5Dg128Aep
[PcdsDynamicExVpd.common.SkuIdAdlPLp5Dg128Aep]
gBoardModuleTokenSpaceGuid.VpdPcdBoardGpioTablePreMem|*|{CODE({
  // CPU M.2 SSD2
  {GPIO_VER2_LP_GPP_D16, {GpioPadModeGpio, GpioHostOwnAcpi,    GpioDirOut,   GpioOutHigh,    GpioIntDis,              GpioPlatformReset, GpioTermNone}},  //CPU SSD2 PWREN
  {GPIO_VER2_LP_GPP_F21, {GpioPadModeGpio, GpioHostOwnAcpi,    GpioDirOut,   GpioOutLow,     GpioIntDis,              GpioPlatformReset, GpioTermNone}},  //CPU SSD2 EXT_PWR_GATE2B

  // TBT Re-Timers
  {GPIO_VER2_LP_GPP_E4,  {GpioPadModeGpio, GpioHostOwnGpio,    GpioDirOut,   GpioOutLow,     GpioIntDis,              GpioPlatformReset, GpioTermNone}},  //TC_RETIMER_FORCE_PWR

  //RTD3 GPIO header for PEG Slot
  {GPIO_VER2_LP_GPP_A11, {GpioPadModeGpio, GpioHostOwnDefault, GpioDirOut,   GpioOutHigh,    GpioIntDefault,          GpioPlatformReset, GpioTermNone, GpioPadConfigUnlock}}, //GPU_PROCHOT_N
  {GPIO_VER2_LP_GPP_A14, {GpioPadModeGpio, GpioHostOwnAcpi,    GpioDirOut,   GpioOutHigh,    GpioIntDefault,          GpioPlatformReset, GpioTermNone}}, // RTD3_COLD_POR
  {GPIO_VER2_LP_GPP_A15, {GpioPadModeGpio, GpioHostOwnAcpi,    GpioDirOut,   GpioOutLow,     GpioIntDefault,          GpioPlatformReset, GpioTermNone}}, // GPU_PERST_N
  {GPIO_VER2_LP_GPP_D17, {GpioPadModeGpio, GpioHostOwnDefault, GpioDirIn,    GpioOutDefault, GpioIntDefault,          GpioPlatformReset, GpioTermNone}}, // GPU_VCCGT_VR_PG_R
  {GPIO_VER2_LP_GPP_A20, {GpioPadModeGpio, GpioHostOwnAcpi,    GpioDirInInv, GpioOutDefault, GpioIntEdge|GpioIntSci,  GpioHostDeepReset, GpioTermNone, GpioPadConfigUnlock}}, //GPU_PCIE_WAKE_N

  {0x0}  // terminator
})}

[PcdsDynamicExVpd.common.SkuIdAdlPLp5Gcs]
gBoardModuleTokenSpaceGuid.VpdPcdBoardGpioTablePreMem|*|{CODE({

  // CPU M.2 SSD
  { GPIO_VER2_LP_GPP_D16, { GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut, GpioOutHigh, GpioIntDis, GpioPlatformReset, GpioTermNone } },  //CPU SSD PWREN
  { GPIO_VER2_LP_GPP_F20, { GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut, GpioOutHigh, GpioIntDis, GpioPlatformReset, GpioTermNone } },  //CPU SSD RESET

  // TBT Re-Timers
  { GPIO_VER2_LP_GPP_E4,  { GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut, GpioOutLow,  GpioIntDis, GpioPlatformReset, GpioTermNone  }},  //TC_RETIMER_FORCE_PWR

  {0x0}  // terminator
})}

# mGpioTablePreMemAdlPMRDdr5Rvp
[PcdsDynamicExVpd.common.SkuIdAdlPDdr5MRRvp]
gBoardModuleTokenSpaceGuid.VpdPcdBoardGpioTablePreMem|*|{CODE({
  // CPU M.2 SSD
  { GPIO_VER2_LP_GPP_D14, { GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut, GpioOutHigh, GpioIntDis, GpioPlatformReset, GpioTermNone } },  //CPU SSD PWREN
  { GPIO_VER2_LP_GPP_F20, { GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut, GpioOutHigh, GpioIntDis, GpioPlatformReset, GpioTermNone } },  //CPU SSD RESET

  // CPU M.2 SSD2
  {GPIO_VER2_LP_GPP_C2, {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutHigh,   GpioIntDis,GpioPlatformReset,  GpioTermNone}},  //CPU SSD2 PWREN
  {GPIO_VER2_LP_GPP_H1, {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutHigh,   GpioIntDis,GpioPlatformReset,  GpioTermNone}},  //CPU SSD2 RESET

  // X4 Pcie Slot for Gen3 and Gen 4
  {GPIO_VER2_LP_GPP_A22, {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutLow,    GpioIntDis,GpioPlatformReset,  GpioTermNone}},//ONBOARD_X4_PCIE_SLOT1_PWREN_N
  {GPIO_VER2_LP_GPP_F10, {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutHigh,   GpioIntDis,GpioPlatformReset,  GpioTermNone}},//ONBOARD_X4_PCIE_SLOT1_RESET_N

  // Discrete Thunderbolt MR1, MR2 Reset GPIO. Asserting Reset GPIO or Releasing the Device Out of reset
  {GPIO_VER2_LP_GPP_D9,     {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutHigh,      GpioIntDefault,          GpioHostDeepReset,   GpioTermNone}},//MR1_PCH_PERST_N
  {GPIO_VER2_LP_GPP_E18,    {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutHigh,      GpioIntDefault,          GpioHostDeepReset,   GpioTermNone}},//MR2_PCH_PERST_N

  //RTD3 GPIO header for PEG X8 PCIe DG2 Slot
  {GPIO_VER2_LP_GPP_A11,   {GpioPadModeGpio,  GpioHostOwnAcpi, GpioDirOut,   GpioOutHigh,    GpioIntDefault,                 GpioPlatformReset, GpioTermNone, GpioPadConfigUnlock }}, //PEG_SLOT_DGPU_SEL_N
  {GPIO_VER2_LP_GPP_A8,   {GpioPadModeGpio,  GpioHostOwnAcpi,    GpioDirOut,   GpioOutLow,     GpioIntDefault,                 GpioPlatformReset, GpioTermNone }}, // PEG_SLOT_DGPU_PWR_EN_N
  {GPIO_VER2_LP_GPP_B2,   {GpioPadModeGpio,  GpioHostOwnAcpi, GpioDirOut,   GpioOutHigh,    GpioIntDefault,                 GpioPlatformReset, GpioTermNone }}, // PEG_SLOT_RST_N
  {GPIO_VER2_LP_GPP_A19,   {GpioPadModeGpio,  GpioHostOwnAcpi, GpioDirIn,    GpioOutDefault, GpioIntDefault,                 GpioPlatformReset, GpioTermNone }}, // PEG_SLOT_DGPU_PWR_OK
  {GPIO_VER2_LP_GPP_A17,   {GpioPadModeGpio,  GpioHostOwnAcpi,    GpioDirInInv, GpioOutDefault,  GpioIntEdge | GpioIntSci,     GpioHostDeepReset, GpioTermNone, GpioPadConfigUnlock }}, // PEG_SLOT_WAKE_N
  {GPIO_VER2_LP_GPP_D17,   {GpioPadModeGpio,  GpioHostOwnAcpi, GpioDirOut,   GpioOutHigh,     GpioIntDefault,                GpioHostDeepReset, GpioTermNone, GpioPadConfigUnlock }}, // PEG_RTD3_COLD_MOD

  {0x0}  // terminator
})}

# mGpioTablePreMemAdlPLp5MbAep
[PcdsDynamicExVpd.common.SkuIdAdlPLp5MbAep]
gBoardModuleTokenSpaceGuid.VpdPcdBoardGpioTablePreMem|*|{CODE({
  // CPU M.2 SSD1
  {GPIO_VER2_LP_GPP_H13, {GpioPadModeGpio, GpioHostOwnAcpi,    GpioDirOut,   GpioOutHigh,    GpioIntDis,              GpioPlatformReset, GpioTermNone}},  //CPU SSD2 PWREN
  {GPIO_VER2_LP_GPP_F20, {GpioPadModeGpio, GpioHostOwnAcpi,    GpioDirOut,   GpioOutLow,     GpioIntDis,              GpioPlatformReset, GpioTermNone}},  //CPU SSD2 EXT_PWR_GATE2B

  // CPU M.2 SSD2
  {GPIO_VER2_LP_GPP_D16, {GpioPadModeGpio, GpioHostOwnAcpi,    GpioDirOut,   GpioOutHigh,    GpioIntDis,              GpioPlatformReset, GpioTermNone}},  //CPU SSD2 PWREN
  {GPIO_VER2_LP_GPP_F21, {GpioPadModeGpio, GpioHostOwnAcpi,    GpioDirOut,   GpioOutLow,     GpioIntDis,              GpioPlatformReset, GpioTermNone}},  //CPU SSD2 EXT_PWR_GATE2B

  // TBT Re-Timers
  {GPIO_VER2_LP_GPP_E4,  {GpioPadModeGpio, GpioHostOwnGpio,    GpioDirOut,   GpioOutLow,     GpioIntDis,              GpioPlatformReset, GpioTermNone}},  //TC_RETIMER_FORCE_PWR

  {0x0}  // terminator
})}