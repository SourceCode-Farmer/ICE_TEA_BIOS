## @file
#  AlderLake S RVP GPIO definition table for Post-Memory Initialization
#
# @copyright
#  INTEL CONFIDENTIAL
#  Copyright 2020 - 2021 Intel Corporation.
#
#  The source code contained or described herein and all documents related to the
#  source code ("Material") are owned by Intel Corporation or its suppliers or
#  licensors. Title to the Material remains with Intel Corporation or its suppliers
#  and licensors. The Material may contain trade secrets and proprietary and
#  confidential information of Intel Corporation and its suppliers and licensors,
#  and is protected by worldwide copyright and trade secret laws and treaty
#  provisions. No part of the Material may be used, copied, reproduced, modified,
#  published, uploaded, posted, transmitted, distributed, or disclosed in any way
#  without Intel's prior express written permission.
#
#  No license under any patent, copyright, trade secret or other intellectual
#  property right is granted to or conferred upon you by disclosure or delivery
#  of the Materials, either expressly, by implication, inducement, estoppel or
#  otherwise. Any license under such intellectual property rights must be
#  express and approved by Intel in writing.
#
#  Unless otherwise agreed by Intel in writing, you may not remove or alter
#  this notice or any other notice embedded in Materials by Intel or
#  Intel's suppliers or licensors in any way.
#
#  This file contains a 'Sample Driver' and is licensed as such under the terms
#  of your license agreement with Intel or your vendor. This file may be modified
#  by the user, subject to the additional terms of the license agreement.
#
# @par Specification
##

###
### !!! GPIOs designated to Native Functions shall not be configured by Platform Code.
### Native Pins shall be configured by Silicon Code (based on BIOS policies setting) or soft straps(set by CSME in FITc).
###
###

#  mGpioTableAdlSDdr5UDimm1DCrb
[PcdsDynamicExVpd.common.SkuIdAdlSAdpSDdr5UDimm1DCrb]
gBoardModuleTokenSpaceGuid.VpdPcdBoardGpioTable|*|{CODE({
  // Codec
  {GPIO_VER4_S_GPP_F23,   {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirInInv,   GpioOutDefault,  GpioIntEdge|GpioIntSci,  GpioPlatformReset,  GpioTermNone, GpioPadConfigUnlock}},//Codec Interrupt
  {GPIO_VER4_S_GPP_R10,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutHigh,     GpioIntDefault,          GpioPlatformReset,  GpioTermNone}},//Codec Power enable

  // EC
  {GPIO_VER4_S_GPP_B4,    {GpioPadModeGpio, GpioHostOwnGpio, GpioDirIn,      GpioOutDefault,  GpioIntEdge,             GpioPlatformReset,  GpioTermNone}},// EC SMI
  {GPIO_VER4_S_GPP_B14,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},// EC Speaker (Rework)Strap

  // GPIO Automation (Rework)
  {GPIO_VER4_S_GPP_C5,    {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},// GPIO Automation (Rework) Strap

  // LAN Power enable
  {GPIO_VER4_S_GPP_R20,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutHigh,     GpioIntDefault,          GpioPlatformReset,  GpioTermNone}},//LAN Power enable

  // M.2 Key-E Slot for Wifi
  {GPIO_VER4_S_GPP_B2,    {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirInInv,   GpioOutDefault,  GpioIntEdge|GpioIntSci,  GpioHostDeepReset,  GpioTermNone, GpioPadConfigUnlock}},// M.2 Key-E Slot  Wifi Wake-> UART_BT_WAKE_N
  {GPIO_VER4_S_GPP_B3,    {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutHigh,     GpioIntDefault,          GpioPlatformReset,  GpioTermNone}},// M.2 Key-E Slot  BT RF Kill
  {GPIO_VER4_S_GPP_B19,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutHigh,     GpioIntDefault,          GpioPlatformReset,  GpioTermNone}},// M.2 Key-E Slot  RF Kill
  {GPIO_VER4_S_GPP_G6,    {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutDefault,  GpioIntDefault,          GpioHostDeepReset,  GpioTermNone}},// M.2 Key-E slot for Wifi -> CNVI_EN_N
  {GPIO_VER4_S_GPP_H2,    {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirInInv,   GpioOutDefault,  GpioIntEdge|GpioIntSci,  GpioResumeReset,    GpioTermNone, GpioPadConfigUnlock}},//M.2 Key-E slot for Wifi Wake
  {GPIO_VER4_S_GPP_H17,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutHigh,     GpioIntDefault,          GpioPlatformReset,  GpioTermNone}},//M.2 Key-E slot for Wifi Reset

  // HBR SSD
  {GPIO_VER4_S_GPP_F18,   {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,     GpioOutHigh,     GpioIntDefault,          GpioPlatformReset,  GpioTermNone}},//HBR SSD Reset

  // M2_SSD_1
  {GPIO_VER4_S_GPP_C10,   {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,     GpioOutHigh,     GpioIntDefault,          GpioPlatformReset,  GpioTermNone}},// SSD_1 RTD3 Reset LPSS UART-0 header (Rework

  // M.2 SSD_2
  {GPIO_VER4_S_GPP_F16,   {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,     GpioOutHigh,     GpioIntDefault,          GpioPlatformReset,  GpioTermNone}},//M.2 SSD_2 RTD3 Reset MIPI60 (Rework)

  // M.2 SSD_3
  {GPIO_VER4_S_GPP_B6,    {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,     GpioOutHigh,     GpioIntDefault,          GpioPlatformReset,  GpioTermNone}},// SSD_3 RTD3 Reset

  // PCIe SLOT_1
  {GPIO_VER4_S_GPP_F0,    {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirInInv,   GpioOutDefault,  GpioIntEdge|GpioIntSci,  GpioHostDeepReset,  GpioTermNone, GpioPadConfigUnlock}},//PCIe SLOT_1 Wake MIPI60 (Rework)

  // PCIe SLOT_2
  {GPIO_VER4_S_GPP_F1,    {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirInInv,   GpioOutDefault,  GpioIntEdge|GpioIntSci,  GpioResetDefault,   GpioTermNone, GpioPadConfigUnlock}},//PCIe SLOT_2 Wake MIPI60 (Rework)

  // PCIe SLOT_3
  {GPIO_VER4_S_GPP_F2,    {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirInInv,   GpioOutDefault,  GpioIntEdge|GpioIntSci,  GpioHostDeepReset,  GpioTermNone, GpioPadConfigUnlock}},//PCIe SLOT_3 Wake MIPI60 (Rework)

  // PEG SLOT_1
  {GPIO_VER4_S_GPP_E6,    {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirInInv,   GpioOutDefault,  GpioIntEdge|GpioIntSci,  GpioHostDeepReset,  GpioTermNone, GpioPadConfigUnlock}},//PEG SLOT_1 Wake
  {GPIO_VER4_S_GPP_F6,    {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutDefault,  GpioIntDefault,          GpioPlatformReset,  GpioTermNone}},//PEG SLOT_1  MIPI60 (Rework)
  {GPIO_VER4_S_GPP_F7,    {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutDefault,  GpioIntDefault,          GpioPlatformReset,  GpioTermNone}},//PEG SLOT_1  MIPI60 (Rework)
  {GPIO_VER4_S_GPP_F9,    {GpioPadModeGpio, GpioHostOwnGpio, GpioDirInInv,   GpioOutDefault,  GpioIntDefault,          GpioPlatformReset,  GpioTermNone}},//PEG SLOT_1  MIPI60 (Rework)

  // SATA
  {GPIO_VER4_S_GPP_R11,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutHigh,     GpioIntDefault,          GpioPlatformReset,  GpioTermNone}},//SATA Direct power enable

  // Thunderbolt
  {GPIO_VER4_S_GPP_G0,    {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutLow,      GpioIntDefault,          GpioPlatformReset,  GpioTermNone}},//Thunderbolt FRC_PWR
  //{GPIO_VER4_S_GPP_G1,    {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutHigh,     GpioIntDefault,          GpioHostDeepReset,  GpioTermNone}},//Thunderbolt CIO_PWR_EN, moved to PreMem
  {GPIO_VER4_S_GPP_G7,    {GpioPadModeGpio, GpioHostOwnGpio, GpioDirIn,      GpioOutDefault,  GpioIntDefault,          GpioPlatformReset,  GpioTermNone}},//Thunderbolt

  // Type C
  {GPIO_VER4_S_GPP_B20,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirIn,      GpioOutDefault,  GpioIntEdge,             GpioResetDefault,   GpioTermNone}},//Type-C PD Detection
  {GPIO_VER4_S_GPP_F22,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirInInv,   GpioOutDefault,  GpioIntEdge,             GpioPlatformReset,  GpioTermNone}},//USBC_PSON_OVERRIDE_N
  {GPIO_VER4_S_GPP_H1,    {GpioPadModeGpio, GpioHostOwnGpio, GpioDirIn,      GpioOutDefault,  GpioIntDefault,          GpioPlatformReset,  GpioTermNone, GpioPadConfigUnlock}},//Type-C Alert
  {GPIO_VER4_S_GPP_K4,    {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutLow,      GpioIntDefault,          GpioHostDeepReset,  GpioTermNone}},//PLX_SX_ENTRY_G1_PCH
  {GPIO_VER4_S_GPP_K3,    {GpioPadModeGpio, GpioHostOwnGpio, GpioDirInInv,   GpioOutDefault,  GpioIntEdge,             GpioHostDeepReset,  GpioTermNone}},//PD_SX_ACK_G2_PCH

  // TPM module
  {GPIO_VER4_S_GPP_F17,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//TPM module MIPI60 (Rework)

  // Touch panel
  {GPIO_VER4_S_GPP_D22,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutHigh,     GpioIntDefault,          GpioPlatformReset,  GpioTermNone, GpioPadUnlock}},//GPP_D_22_UART3_RTSB_THC1_SPI2_RST_N
  {GPIO_VER4_S_GPP_D23,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirInInv,   GpioOutDefault,  GpioIntEdge|GpioIntApic, GpioResumeReset,    GpioTermNone, GpioPadUnlock}},//GPP_D_23_UART3_CTSB_THC1_SPI2_INT_N
  {GPIO_VER4_S_GPP_R21,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutHigh,     GpioIntDefault,          GpioPlatformReset,  GpioTermNone}},//Touch panel power enable

  //PCIe Aux Power Enable
  {GPIO_VER4_S_GPP_I0,    {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutHigh,     GpioIntDefault,          GpioResumeReset,    GpioTermNone}},//GPP_I_0_EXT_PWR_GATE_N

  //PLN
  {GPIO_VER4_S_GPP_H15,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutHigh,     GpioIntDefault,          GpioPlatformReset,  GpioTermNone}},//PCH SSD PLN Circuit Strap
  {GPIO_VER4_S_GPP_H23,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutDefault,  GpioIntDefault,          GpioPlatformReset,  GpioTermNone}},//PCH SSD PLN Circuit

  //Others
  {GPIO_VER4_S_GPP_K5,    {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutDefault,  GpioIntDefault,          GpioResetDefault,    GpioTermNone}},//DNX_IN_PROG_AUX_NBIAS
  {GPIO_VER4_S_GPP_K1,    {GpioPadModeGpio, GpioHostOwnGpio, GpioDirIn,      GpioOutDefault,  GpioIntDefault,          GpioPlatformReset,   GpioTermNone}},//Platform
  {GPIO_VER4_S_GPP_H10,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirIn,      GpioOutDefault,  GpioIntEdge,             GpioResetDefault,    GpioTermNone}},//Power supply detect
  {GPIO_VER4_S_GPP_K0,    {GpioPadModeGpio, GpioHostOwnGpio, GpioDirInInv,   GpioOutDefault,  GpioIntDefault,          GpioPlatformReset,   GpioTermNone}},//Thunderbolt P1 P2 Alert
  {GPIO_VER4_S_GPP_H0,    {GpioPadModeGpio, GpioHostOwnGpio, GpioDirDefault, GpioOutDefault,  GpioIntDefault,          GpioResetDefault,    GpioTermNone}},//VAL_SV_ADVANCE_STRAP
  {GPIO_VER4_S_GPP_H14,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirInInv,   GpioOutDefault,  GpioIntEdge,             GpioPlatformReset,   GpioTermNone}},//VOL_UP

  //Not used
  {GPIO_VER4_S_GPD0,      {GpioPadModeGpio, GpioHostOwnGpio, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPD1,      {GpioPadModeGpio, GpioHostOwnGpio, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioPlatformReset,  GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPD7,      {GpioPadModeGpio, GpioHostOwnGpio, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPD12,     {GpioPadModeGpio, GpioHostOwnGpio, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPP_A14,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPP_B23,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPP_C2,    {GpioPadModeGpio, GpioHostOwnGpio, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPP_D3,    {GpioPadModeGpio, GpioHostOwnGpio, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//GFX_DETECT_STRAP
  {GPIO_VER4_S_GPP_D11,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPP_D13,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPP_D14,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPP_E3,    {GpioPadModeGpio, GpioHostOwnGpio, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPP_E7,    {GpioPadModeGpio, GpioHostOwnGpio, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPP_F3,    {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirInInv,   GpioOutDefault,  GpioIntEdge|GpioIntSci,  GpioPlatformReset,  GpioTermNone, GpioPadConfigUnlock}},//MIPI60 (Rework)
  {GPIO_VER4_S_GPP_F5,    {GpioPadModeGpio, GpioHostOwnGpio, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ, MIPI60 (Rework)
  {GPIO_VER4_S_GPP_F8,    {GpioPadModeGpio, GpioHostOwnGpio, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ, MIPI60 (Rework)
  {GPIO_VER4_S_GPP_F10,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ, MIPI60 (Rework)
  {GPIO_VER4_S_GPP_H12,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ, strap
  {GPIO_VER4_S_GPP_H13,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPP_H18,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ, strap
  {GPIO_VER4_S_GPP_J8 ,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioPlatformReset,  GpioTermNone}},//HiZ, strap
  {GPIO_VER4_S_GPP_J9 ,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioPlatformReset,  GpioTermNone}},//HiZ, strap

  {0x0}  // terminator

})}

#  mGpioTableAdlSDdr5UDimm1DCrbFab2
[PcdsDynamicExVpd.common.SkuIdAdlSAdpSDdr5UDimm1DCrbFab2]
gBoardModuleTokenSpaceGuid.VpdPcdBoardGpioTable|*|{CODE({
  // Codec
  {GPIO_VER4_S_GPP_F23,   {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirInInv,   GpioOutDefault,  GpioIntEdge|GpioIntSci,  GpioPlatformReset,  GpioTermNone, GpioPadConfigUnlock}},//Codec Interrupt
  {GPIO_VER4_S_GPP_R10,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutHigh,     GpioIntDefault,          GpioPlatformReset,  GpioTermNone}},//Codec Power enable

  // EC
  {GPIO_VER4_S_GPP_B4,    {GpioPadModeGpio, GpioHostOwnGpio, GpioDirIn,      GpioOutDefault,  GpioIntEdge,             GpioPlatformReset,  GpioTermNone}},// EC SMI
  {GPIO_VER4_S_GPP_B14,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},// EC Speaker (Rework)Strap

  // GPIO Automation (Rework)
  {GPIO_VER4_S_GPP_C5,    {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},// GPIO Automation (Rework) Strap

  // LAN Power enable
  {GPIO_VER4_S_GPP_R20,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutHigh,     GpioIntDefault,          GpioPlatformReset,  GpioTermNone}},//LAN Power enable

  // M.2 Key-E Slot for Wifi
  {GPIO_VER4_S_GPP_B2,    {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirInInv,   GpioOutDefault,  GpioIntEdge|GpioIntSci,  GpioHostDeepReset,  GpioTermNone, GpioPadConfigUnlock}},// M.2 Key-E Slot  Wifi Wake-> UART_BT_WAKE_N
  {GPIO_VER4_S_GPP_B3,    {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutHigh,     GpioIntDefault,          GpioPlatformReset,  GpioTermNone}},// M.2 Key-E Slot  BT RF Kill
  {GPIO_VER4_S_GPP_B19,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutHigh,     GpioIntDefault,          GpioPlatformReset,  GpioTermNone}},// M.2 Key-E Slot  RF Kill
  {GPIO_VER4_S_GPP_G6,    {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutDefault,  GpioIntDefault,          GpioHostDeepReset,  GpioTermNone}},// M.2 Key-E slot for Wifi -> CNVI_EN_N
  {GPIO_VER4_S_GPP_H2,    {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirInInv,   GpioOutDefault,  GpioIntEdge|GpioIntSci,  GpioResumeReset,    GpioTermNone, GpioPadConfigUnlock}},//M.2 Key-E slot for Wifi Wake
  {GPIO_VER4_S_GPP_H17,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutHigh,     GpioIntDefault,          GpioPlatformReset,  GpioTermNone}},//M.2 Key-E slot for Wifi Reset

  // HBR SSD
  {GPIO_VER4_S_GPP_F18,   {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,     GpioOutHigh,     GpioIntDefault,          GpioPlatformReset,  GpioTermNone}},//HBR SSD Reset

  // M2_SSD_1
  {GPIO_VER4_S_GPP_C10,   {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,     GpioOutHigh,     GpioIntDefault,          GpioPlatformReset,  GpioTermNone}},// SSD_1 RTD3 Reset LPSS UART-0 header (Rework

  // M.2 SSD_2
  {GPIO_VER4_S_GPP_F16,   {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,     GpioOutHigh,     GpioIntDefault,          GpioPlatformReset,  GpioTermNone}},//M.2 SSD_2 RTD3 Reset MIPI60 (Rework)

  // M.2 SSD_3
  {GPIO_VER4_S_GPP_B6,    {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,     GpioOutHigh,     GpioIntDefault,          GpioPlatformReset,  GpioTermNone}},// SSD_3 RTD3 Reset

  // PCIe SLOT_1
  {GPIO_VER4_S_GPP_F0,    {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirInInv,   GpioOutDefault,  GpioIntEdge|GpioIntSci,  GpioHostDeepReset,  GpioTermNone, GpioPadConfigUnlock}},//PCIe SLOT_1 Wake MIPI60 (Rework)

  // PCIe SLOT_2
  {GPIO_VER4_S_GPP_F1,    {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirInInv,   GpioOutDefault,  GpioIntEdge|GpioIntSci,  GpioResetDefault,   GpioTermNone, GpioPadConfigUnlock}},//PCIe SLOT_2 Wake MIPI60 (Rework)

  // PCIe SLOT_3
  {GPIO_VER4_S_GPP_F2,    {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirInInv,   GpioOutDefault,  GpioIntEdge|GpioIntSci,  GpioHostDeepReset,  GpioTermNone, GpioPadConfigUnlock}},//PCIe SLOT_3 Wake MIPI60 (Rework)

  // PEG SLOT_1
  {GPIO_VER4_S_GPP_E6,    {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirInInv,   GpioOutDefault,  GpioIntEdge|GpioIntSci,  GpioHostDeepReset,  GpioTermNone, GpioPadConfigUnlock}},//PEG SLOT_1 Wake
  {GPIO_VER4_S_GPP_F6,    {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutDefault,  GpioIntDefault,          GpioPlatformReset,  GpioTermNone}},//PEG SLOT_1  MIPI60 (Rework)
  {GPIO_VER4_S_GPP_F7,    {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutHigh,     GpioIntDefault,          GpioPlatformReset,  GpioTermNone}},//PEG SLOT_1  MIPI60 (Rework)
  {GPIO_VER4_S_GPP_F9,    {GpioPadModeGpio, GpioHostOwnGpio, GpioDirInInv,   GpioOutDefault,  GpioIntDefault,          GpioPlatformReset,  GpioTermNone}},//PEG SLOT_1  MIPI60 (Rework)

  // SATA
  {GPIO_VER4_S_GPP_R11,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutHigh,     GpioIntDefault,          GpioPlatformReset,  GpioTermNone}},//SATA Direct power enable

  // Thunderbolt
  {GPIO_VER4_S_GPP_G0,    {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutLow,      GpioIntDefault,          GpioPlatformReset,  GpioTermNone}},//Thunderbolt FRC_PWR
  //{GPIO_VER4_S_GPP_G1,    {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutHigh,     GpioIntDefault,          GpioHostDeepReset,  GpioTermNone}},//Thunderbolt CIO_PWR_EN, moved to PreMem
  {GPIO_VER4_S_GPP_G7,    {GpioPadModeGpio, GpioHostOwnGpio, GpioDirIn,      GpioOutDefault,  GpioIntDefault,          GpioPlatformReset,  GpioTermNone}},//Thunderbolt

  // Type C
  {GPIO_VER4_S_GPP_B20,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirIn,      GpioOutDefault,  GpioIntEdge,             GpioResetDefault,   GpioTermNone}},//Type-C PD Detection
  {GPIO_VER4_S_GPP_F22,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirInInv,   GpioOutDefault,  GpioIntEdge,             GpioPlatformReset,  GpioTermNone}},//USBC_PSON_OVERRIDE_N
  {GPIO_VER4_S_GPP_H1,    {GpioPadModeGpio, GpioHostOwnGpio, GpioDirIn,      GpioOutDefault,  GpioIntDefault,          GpioPlatformReset,  GpioTermNone, GpioPadConfigUnlock}},//I2C_ALERT_AICP2_TYPEC_PCH_N
  {GPIO_VER4_S_GPP_K4,    {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutLow,      GpioIntDefault,          GpioHostDeepReset,  GpioTermNone}},//PLX_SX_ENTRY_G1_PCH
  {GPIO_VER4_S_GPP_K3,    {GpioPadModeGpio, GpioHostOwnGpio, GpioDirInInv,   GpioOutDefault,  GpioIntEdge,             GpioHostDeepReset,  GpioTermNone}},//PD_SX_ACK_G2_PCH

  // TPM module
  {GPIO_VER4_S_GPP_F17,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//TPM module MIPI60 (Rework)

  // Touch panel
  {GPIO_VER4_S_GPP_D22,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutHigh,     GpioIntDefault,          GpioPlatformReset,  GpioTermNone, GpioPadUnlock}},//GPP_D_22_UART3_RTSB_THC1_SPI2_RST_N
  {GPIO_VER4_S_GPP_D23,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirInInv,   GpioOutDefault,  GpioIntEdge|GpioIntApic, GpioResumeReset,    GpioTermNone, GpioPadUnlock}},//GPP_D_23_UART3_CTSB_THC1_SPI2_INT_N
  {GPIO_VER4_S_GPP_R21,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutHigh,     GpioIntDefault,          GpioPlatformReset,  GpioTermNone}},//Touch panel power enable

  //PCIe Aux Power Enable
  {GPIO_VER4_S_GPP_I0,    {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutHigh,     GpioIntDefault,          GpioResumeReset,    GpioTermNone}},//GPP_I_0_EXT_PWR_GATE_N

  //PLN
  {GPIO_VER4_S_GPP_H15,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutHigh,     GpioIntDefault,          GpioPlatformReset,   GpioTermNone}},//PCH SSD PLN Circuit Strap
  {GPIO_VER4_S_GPP_H23,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutDefault,  GpioIntDefault,          GpioPlatformReset,   GpioTermNone}},//PCH SSD PLN Circuit

  //Others
  {GPIO_VER4_S_GPP_K5,    {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutDefault,  GpioIntDefault,          GpioResetDefault,    GpioTermNone}},//DNX_IN_PROG_AUX_NBIAS
  {GPIO_VER4_S_GPP_K1,    {GpioPadModeGpio, GpioHostOwnGpio, GpioDirIn,      GpioOutDefault,  GpioIntDefault,          GpioPlatformReset,   GpioTermNone}},//Platform
  {GPIO_VER4_S_GPP_H10,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirIn,      GpioOutDefault,  GpioIntEdge,             GpioResetDefault,    GpioTermNone}},//Power supply detect
  {GPIO_VER4_S_GPP_K0,    {GpioPadModeGpio, GpioHostOwnGpio, GpioDirInInv,   GpioOutDefault,  GpioIntDefault,          GpioPlatformReset,   GpioTermNone}},//Thunderbolt P1 P2 Alert
  {GPIO_VER4_S_GPP_H0,    {GpioPadModeGpio, GpioHostOwnGpio, GpioDirDefault, GpioOutDefault,  GpioIntDefault,          GpioResetDefault,    GpioTermNone}},//VAL_SV_ADVANCE_STRAP
  {GPIO_VER4_S_GPP_H14,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirInInv,   GpioOutDefault,  GpioIntEdge,             GpioPlatformReset,   GpioTermNone}},//VOL_UP

  //Not used
  {GPIO_VER4_S_GPD0,      {GpioPadModeGpio, GpioHostOwnGpio, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPD1,      {GpioPadModeGpio, GpioHostOwnGpio, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioPlatformReset,  GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPD7,      {GpioPadModeGpio, GpioHostOwnGpio, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPD12,     {GpioPadModeGpio, GpioHostOwnGpio, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPP_A14,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPP_B23,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPP_C2,    {GpioPadModeGpio, GpioHostOwnGpio, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPP_D3,    {GpioPadModeGpio, GpioHostOwnGpio, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//GFX_DETECT_STRAP
  {GPIO_VER4_S_GPP_D11,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPP_D13,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPP_D14,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPP_E3,    {GpioPadModeGpio, GpioHostOwnGpio, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPP_E7,    {GpioPadModeGpio, GpioHostOwnGpio, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPP_F3,    {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirInInv,   GpioOutDefault,  GpioIntEdge|GpioIntSci,  GpioPlatformReset,  GpioTermWpd20K, GpioPadConfigUnlock}},//MIPI60 (Rework)
  {GPIO_VER4_S_GPP_F5,    {GpioPadModeGpio, GpioHostOwnGpio, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ, MIPI60 (Rework)
  {GPIO_VER4_S_GPP_F8,    {GpioPadModeGpio, GpioHostOwnGpio, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ, MIPI60 (Rework)
  {GPIO_VER4_S_GPP_F10,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ, MIPI60 (Rework)
  {GPIO_VER4_S_GPP_H12,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ, strap
  {GPIO_VER4_S_GPP_H13,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPP_H18,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ, strap
  {GPIO_VER4_S_GPP_J8 ,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioPlatformReset,  GpioTermNone}},//HiZ, strap
  {GPIO_VER4_S_GPP_J9 ,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioPlatformReset,  GpioTermNone}},//HiZ, strap

  {0x0}  // terminator

})}

[PcdsDynamicExVpd.common.SkuIdAdlSAdpSDdr5SODimmCrb]
gBoardModuleTokenSpaceGuid.VpdPcdBoardGpioTable|*|{CODE({
  // Codec
  {GPIO_VER4_S_GPP_F23,   {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirInInv,   GpioOutDefault,  GpioIntEdge|GpioIntSci,  GpioPlatformReset,  GpioTermNone, GpioPadConfigUnlock}},//Codec Interrupt
  {GPIO_VER4_S_GPP_R10,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutHigh,     GpioIntDefault,          GpioPlatformReset,  GpioTermNone}},//Codec Power enable

  // EC
  {GPIO_VER4_S_GPP_B4,    {GpioPadModeGpio, GpioHostOwnGpio, GpioDirIn,      GpioOutDefault,  GpioIntEdge,             GpioPlatformReset,  GpioTermNone}},// EC SMI
  {GPIO_VER4_S_GPP_B14,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},// EC Speaker (Rework)Strap

  // GPIO Automation (Rework)
  {GPIO_VER4_S_GPP_C5,    {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},// GPIO Automation (Rework) Strap

  //Foxville LAN
  {GPIO_VER4_S_GPD1,      {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutHigh,     GpioIntDefault,          GpioResumeReset,    GpioTermNone}}, // FOX_LAN_DISABLE_N
  {GPIO_VER4_S_GPP_G1,    {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutHigh,     GpioIntDefault,          GpioPlatformReset,  GpioTermNone}}, // PLTRST_FOX_N
  {GPIO_VER4_S_GPP_R20,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutLow,      GpioIntDefault,          GpioResumeReset,    GpioTermNone}}, // LAN_FOX_ON. Set Output high based on the setup option.
  {GPIO_VER4_S_GPD2,      {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirInInv,   GpioOutDefault,  GpioIntEdge,             GpioHostDeepReset,  GpioTermNone, GpioPadConfigUnlock}}, // FOX_WAKE_N. Set to SCI based on the setup option.

  // M.2 Key-E Slot for Wifi
  {GPIO_VER4_S_GPP_B2,    {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirInInv,   GpioOutDefault,  GpioIntEdge|GpioIntSci,  GpioHostDeepReset,  GpioTermNone, GpioPadConfigUnlock}},// M.2 Key-E Slot  Wifi Wake-> UART_BT_WAKE_N
  {GPIO_VER4_S_GPP_B3,    {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutHigh,     GpioIntDefault,          GpioPlatformReset,  GpioTermNone}},// M.2 Key-E Slot  BT RF Kill
  {GPIO_VER4_S_GPP_B19,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutHigh,     GpioIntDefault,          GpioPlatformReset,  GpioTermNone}},// M.2 Key-E Slot  RF Kill
  {GPIO_VER4_S_GPP_G6,    {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutDefault,  GpioIntDefault,          GpioHostDeepReset,  GpioTermNone}},// M.2 Key-E slot for Wifi -> CNVI_EN_N
  {GPIO_VER4_S_GPP_H2,    {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirInInv,   GpioOutDefault,  GpioIntEdge|GpioIntSci,  GpioResumeReset,    GpioTermNone, GpioPadConfigUnlock}},//M.2 Key-E slot for Wifi Wake
  {GPIO_VER4_S_GPP_H17,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutHigh,     GpioIntDefault,          GpioPlatformReset,  GpioTermNone}},//M.2 Key-E slot for Wifi Reset

  // HBR SSD
  {GPIO_VER4_S_GPP_F18,   {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,     GpioOutHigh,     GpioIntDefault,          GpioPlatformReset,  GpioTermNone}},//HBR SSD Reset

  // M2_SSD_1
  {GPIO_VER4_S_GPP_C10,   {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,     GpioOutHigh,     GpioIntDefault,          GpioPlatformReset,  GpioTermNone}},// SSD_1 RTD3 Reset LPSS UART-0 header (Rework

  // M.2 SSD_3
  {GPIO_VER4_S_GPP_B6,    {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,     GpioOutHigh,     GpioIntDefault,          GpioPlatformReset,  GpioTermNone}},// SSD_3 RTD3 Reset

  // PCIe SLOT_1
  {GPIO_VER4_S_GPP_F0,    {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirInInv,   GpioOutDefault,  GpioIntEdge|GpioIntSci,  GpioHostDeepReset,  GpioTermNone, GpioPadConfigUnlock}},//PCIe SLOT_1 Wake MIPI60 (Rework)

  // PCIe SLOT_2
  {GPIO_VER4_S_GPP_F1,    {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirInInv,   GpioOutDefault,  GpioIntEdge|GpioIntSci,  GpioResetDefault,   GpioTermNone, GpioPadConfigUnlock}},//PCIe SLOT_2 Wake MIPI60 (Rework)

  // PCIe SLOT_3
  {GPIO_VER4_S_GPP_F2,    {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirInInv,   GpioOutDefault,  GpioIntEdge|GpioIntSci,  GpioHostDeepReset,  GpioTermNone, GpioPadConfigUnlock}},//PCIe SLOT_3 Wake MIPI60 (Rework)

  // PEG SLOT_1
  {GPIO_VER4_S_GPP_E6,    {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirInInv,   GpioOutDefault,  GpioIntEdge|GpioIntSci,  GpioHostDeepReset,  GpioTermNone, GpioPadConfigUnlock}},//PEG SLOT_1 Wake
  {GPIO_VER4_S_GPP_F6,    {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutDefault,  GpioIntDefault,          GpioPlatformReset,  GpioTermNone}},//PEG SLOT_1  MIPI60 (Rework)
  {GPIO_VER4_S_GPP_F7,    {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutDefault,  GpioIntDefault,          GpioPlatformReset,  GpioTermNone}},//PEG SLOT_1  MIPI60 (Rework)
  {GPIO_VER4_S_GPP_F9,    {GpioPadModeGpio, GpioHostOwnGpio, GpioDirInInv,   GpioOutDefault,  GpioIntDefault,          GpioPlatformReset,  GpioTermNone}},//PEG SLOT_1  MIPI60 (Rework)

  // SATA
  {GPIO_VER4_S_GPP_R11,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutHigh,     GpioIntDefault,          GpioPlatformReset,  GpioTermNone}},//SATA Direct power enable

  // Thunderbolt
  {GPIO_VER4_S_GPP_F3,    {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutHigh,     GpioIntDefault,          GpioPlatformReset,  GpioTermNone}},//POR Thunderbolt USB Force Power, MIPI60 (Rework)
  //{GPIO_VER4_S_GPP_F5,    {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutHigh,     GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//POR Thunderbolt RTD3 enable and CIO_PWR_EN
  {GPIO_VER4_S_GPP_F8,    {GpioPadModeGpio, GpioHostOwnGpio, GpioDirIn,      GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//POR Thunderbolt Wake, MIPI60 (Rework)
  {GPIO_VER4_S_GPP_F10,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirIn,      GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//POR PD AIC detect, MIPI60 (Rework)
  {GPIO_VER4_S_GPP_F16,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutHigh,     GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//POR Thunderbolt Reset,MIPI60 (Rework)
  {GPIO_VER4_S_GPP_G0,    {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutLow,      GpioIntDefault,          GpioResumeReset,    GpioTermNone}},//POR Thunderbolt FRC_PWR
  {GPIO_VER4_S_GPP_G7,    {GpioPadModeGpio, GpioHostOwnGpio, GpioDirIn,      GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//Thunderbolt Plug event
  {GPIO_VER4_S_GPP_K0,    {GpioPadModeGpio, GpioHostOwnGpio, GpioDirInInv,   GpioOutDefault,  GpioIntDefault,          GpioPlatformReset,  GpioTermNone}},//Thunderbolt P1 P2 Alert

  // Type C
  {GPIO_VER4_S_GPP_B20,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirIn,      GpioOutDefault,  GpioIntEdge,             GpioResetDefault,   GpioTermNone}},//Type-C PD Detection
  {GPIO_VER4_S_GPP_F22,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirInInv,   GpioOutDefault,  GpioIntEdge,             GpioPlatformReset,  GpioTermNone}},//USBC_PSON_OVERRIDE_N
  {GPIO_VER4_S_GPP_H1,    {GpioPadModeGpio, GpioHostOwnGpio, GpioDirIn,      GpioOutDefault,  GpioIntDefault,          GpioPlatformReset,  GpioTermNone, GpioPadConfigUnlock}},//Type-C Alert
  {GPIO_VER4_S_GPP_K4,    {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutLow,      GpioIntDefault,          GpioHostDeepReset,  GpioTermNone}},//PLX_SX_ENTRY_G1_PCH_N
  {GPIO_VER4_S_GPP_K3,    {GpioPadModeGpio, GpioHostOwnGpio, GpioDirInInv,   GpioOutDefault,  GpioIntEdge,             GpioHostDeepReset,  GpioTermNone, GpioPadConfigUnlock}},//PD_SX_ACK_G2_PCH

  // TPM module
  {GPIO_VER4_S_GPP_F17,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//TPM module MIPI60 (Rework)

  // Touch panel
  {GPIO_VER4_S_GPP_D22,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutHigh,     GpioIntDefault,          GpioPlatformReset,  GpioTermNone, GpioPadUnlock}},//GPP_D_22_UART3_RTSB_THC1_SPI2_RST_N
  {GPIO_VER4_S_GPP_D23,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirInInv,   GpioOutDefault,  GpioIntEdge|GpioIntApic, GpioResumeReset,    GpioTermNone, GpioPadUnlock}},//GPP_D_23_UART3_CTSB_THC1_SPI2_INT_N
  {GPIO_VER4_S_GPP_R21,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutHigh,     GpioIntDefault,          GpioPlatformReset,  GpioTermNone}},//Touch panel power enable

  //PCIe Aux Power Enable
  {GPIO_VER4_S_GPP_I0,    {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutHigh,     GpioIntDefault,          GpioResumeReset,    GpioTermNone}},//GPP_I_0_EXT_PWR_GATE_N

  //PLN
  {GPIO_VER4_S_GPP_H15,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutHigh,     GpioIntDefault,          GpioPlatformReset,  GpioTermNone}},//PCH SSD PLN Circuit Strap
  {GPIO_VER4_S_GPP_H23,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutDefault,  GpioIntDefault,          GpioPlatformReset,  GpioTermNone}},//PCH SSD PLN Circuit

  //Others
  {GPIO_VER4_S_GPP_K5,    {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//DNX_IN_PROG_AUX_NBIAS
  {GPIO_VER4_S_GPP_K1,    {GpioPadModeGpio, GpioHostOwnGpio, GpioDirIn,      GpioOutDefault,  GpioIntDefault,          GpioPlatformReset,  GpioTermNone}},//Platform
  {GPIO_VER4_S_GPP_H10,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirIn,      GpioOutDefault,  GpioIntEdge,             GpioResetDefault,   GpioTermNone}},//Power supply detect
  {GPIO_VER4_S_GPP_H0,    {GpioPadModeGpio, GpioHostOwnGpio, GpioDirDefault, GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//VAL_SV_ADVANCE_STRAP
  {GPIO_VER4_S_GPP_H14,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirInInv,   GpioOutDefault,  GpioIntEdge,             GpioPlatformReset,  GpioTermNone}},//VOL_UP

  //Not used
  {GPIO_VER4_S_GPD0,      {GpioPadModeGpio, GpioHostOwnGpio, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPD7,      {GpioPadModeGpio, GpioHostOwnGpio, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPD12,     {GpioPadModeGpio, GpioHostOwnGpio, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPP_A14,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPP_B23,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPP_C2,    {GpioPadModeGpio, GpioHostOwnGpio, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPP_D3,    {GpioPadModeGpio, GpioHostOwnGpio, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//GFX_DETECT_STRAP
  {GPIO_VER4_S_GPP_D11,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPP_D13,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPP_D14,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPP_E3,    {GpioPadModeGpio, GpioHostOwnGpio, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPP_E7,    {GpioPadModeGpio, GpioHostOwnGpio, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPP_H12,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ, strap
  {GPIO_VER4_S_GPP_H13,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPP_H18,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ, strap
  {GPIO_VER4_S_GPP_J8 ,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioPlatformReset,  GpioTermNone}},//HiZ, strap
  {GPIO_VER4_S_GPP_J9 ,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioPlatformReset,  GpioTermNone}},//HiZ, strap

  {0x0}  // terminator

})}

#  mGpioTableAdlSDdr4UDimm2DCrb
[PcdsDynamicExVpd.common.SkuIdAdlSAdpSDdr4UDimm2DCrb]
gBoardModuleTokenSpaceGuid.VpdPcdBoardGpioTable|*|{CODE({
  {GPIO_VER4_S_GPD1,      {GpioPadModeGpio, GpioHostOwnGpio, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioPlatformReset,  GpioTermNone}},//HiZ, not used
  {GPIO_VER4_S_GPD7,      {GpioPadModeGpio, GpioHostOwnGpio, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ, not used
  {GPIO_VER4_S_GPD12,     {GpioPadModeGpio, GpioHostOwnGpio, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ, not used

//  {GPIO_VER4_S_GPP_A14,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioPlatformReset,  GpioTermNone}},//HiZ, not used. PCIe Slot1 enabled by Pull up on Fab1 board.
  {GPIO_VER4_S_GPP_B2,    {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirInInv,   GpioOutDefault,  GpioIntEdge|GpioIntSci,  GpioHostDeepReset,  GpioTermNone, GpioPadConfigUnlock}},// M.2 Key-E Slot  Wifi Wake-> UART_BT_WAKE_N
  {GPIO_VER4_S_GPP_B3,    {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutHigh,     GpioIntDefault,          GpioPlatformReset,  GpioTermNone}},// M.2 Key-E Slot  BT RF Kill
  {GPIO_VER4_S_GPP_B4,    {GpioPadModeGpio, GpioHostOwnGpio, GpioDirIn,      GpioOutDefault,  GpioIntEdge,             GpioPlatformReset,  GpioTermNone}},// EC SMI
  {GPIO_VER4_S_GPP_B6,    {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,     GpioOutHigh,     GpioIntDefault,          GpioPlatformReset,  GpioTermNone}},// SSD_3 RTD3 Reset
  {GPIO_VER4_S_GPP_B14,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}}, //EC Speaker (Rework)Strap
  {GPIO_VER4_S_GPP_B19,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutHigh,     GpioIntDefault,          GpioPlatformReset,  GpioTermNone}},// M.2 Key-E Slot  RF Kill
  {GPIO_VER4_S_GPP_B20,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirIn,      GpioOutDefault,  GpioIntEdge,             GpioResetDefault,   GpioTermNone}},// Type-C PD Detection
  {GPIO_VER4_S_GPP_B23,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioPlatformReset,  GpioTermNone}},//HiZ, not used. Strap

  {GPIO_VER4_S_GPP_C2,    {GpioPadModeGpio, GpioHostOwnGpio, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ, not used
  {GPIO_VER4_S_GPP_C5,    {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},// GPIO Automation (Rework) Strap
  {GPIO_VER4_S_GPP_C10,   {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,     GpioOutHigh,     GpioIntDefault,          GpioPlatformReset,  GpioTermNone}},// SSD_1 RTD3 Reset LPSS UART-0 header (Rework

  {GPIO_VER4_S_GPP_D3,    {GpioPadModeGpio, GpioHostOwnGpio, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ, not used
  {GPIO_VER4_S_GPP_D11,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutLow,      GpioIntDefault,          GpioHostDeepReset,  GpioTermNone}},//Type-C PD Controller SX Entry notification
  {GPIO_VER4_S_GPP_D13,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirInInv,   GpioOutDefault,  GpioIntEdge,             GpioHostDeepReset,  GpioTermNone}},//Type-C PD Controller SX ACK notification
  {GPIO_VER4_S_GPP_D14,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirInInv,   GpioOutDefault,  GpioIntEdge,             GpioPlatformReset,   GpioTermNone}},//PS_ON Override

  {GPIO_VER4_S_GPP_D22,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutHigh,     GpioIntDefault,          GpioPlatformReset,  GpioTermNone, GpioPadUnlock}},//GPP_D_22_UART3_RTSB_THC1_SPI2_RST_N
  {GPIO_VER4_S_GPP_D23,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirInInv,   GpioOutDefault,  GpioIntEdge|GpioIntApic, GpioResumeReset,    GpioTermNone, GpioPadUnlock}},//GPP_D_23_UART3_CTSB_THC1_SPI2_INT_N

  {GPIO_VER4_S_GPP_E6,    {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirInInv,   GpioOutDefault,  GpioIntEdge|GpioIntSci,  GpioHostDeepReset,  GpioTermNone, GpioPadConfigUnlock}},//PEG SLOT_1 Wake
  {GPIO_VER4_S_GPP_E7,    {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirInInv,   GpioOutDefault,  GpioIntEdge|GpioIntSci,  GpioHostDeepReset,  GpioTermNone, GpioPadConfigUnlock}},//PEG SLOT_2 Wake  Sinai DR0 (Rework)

  {GPIO_VER4_S_GPP_F0,    {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirInInv,   GpioOutDefault,  GpioIntEdge|GpioIntSci,  GpioHostDeepReset,  GpioTermNone, GpioPadConfigUnlock}},//PCIe SLOT_1 Wake MIPI60 (Rework)
  {GPIO_VER4_S_GPP_F1,    {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirInInv,   GpioOutDefault,  GpioIntEdge|GpioIntSci,  GpioHostDeepReset,  GpioTermNone, GpioPadConfigUnlock}},//PCIe SLOT_2 Wake MIPI60 (Rework)
  {GPIO_VER4_S_GPP_F2,    {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirInInv,   GpioOutDefault,  GpioIntEdge|GpioIntSci,  GpioHostDeepReset,  GpioTermNone, GpioPadConfigUnlock}},//PCIe SLOT_3 Wake MIPI60 (Rework)
  {GPIO_VER4_S_GPP_F3,    {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutHigh,     GpioIntDefault,          GpioPlatformReset,  GpioTermNone}},//Not Used MIPI60 (Rework)
  {GPIO_VER4_S_GPP_F5,    {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutDefault,  GpioIntDefault,          GpioHostDeepReset,  GpioTermNone}},//PEG SLOT_2  MIPI60 (Rework)
  {GPIO_VER4_S_GPP_F6,    {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutDefault,  GpioIntDefault,          GpioPlatformReset,  GpioTermNone}},//PEG SLOT_1  MIPI60 (Rework)
  {GPIO_VER4_S_GPP_F7,    {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutDefault,  GpioIntDefault,          GpioPlatformReset,  GpioTermNone}},//PEG SLOT_1  MIPI60 (Rework)
  {GPIO_VER4_S_GPP_F8,    {GpioPadModeGpio, GpioHostOwnGpio, GpioDirIn,      GpioOutDefault,  GpioIntDefault,          GpioPlatformReset,  GpioTermNone}},//PEG SLOT_2  MIPI60 (Rework)
  {GPIO_VER4_S_GPP_F9,    {GpioPadModeGpio, GpioHostOwnGpio, GpioDirInInv,   GpioOutDefault,  GpioIntDefault,          GpioPlatformReset,  GpioTermNone}},//PEG SLOT_1  MIPI60 (Rework)
  {GPIO_VER4_S_GPP_F10,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutDefault,  GpioIntDefault,          GpioPlatformReset,  GpioTermNone}},//PEG SLOT_2   MIPI60 (Rework)
  {GPIO_VER4_S_GPP_F16,   {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,     GpioOutHigh,     GpioIntDefault,          GpioPlatformReset,  GpioTermNone}},//M.2 SSD_2 RTD3 Reset MIPI60 (Rework)
  {GPIO_VER4_S_GPP_F17,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioPlatformReset,  GpioTermNone}},//TPM module MIPI60 (Rework)
  {GPIO_VER4_S_GPP_F18,   {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,     GpioOutHigh,     GpioIntDefault,          GpioPlatformReset,  GpioTermNone}},//HBR SSD Reset
  {GPIO_VER4_S_GPP_F22,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioPlatformReset,  GpioTermNone}},//HiZ, not used
  {GPIO_VER4_S_GPP_F23,   {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirInInv,   GpioOutDefault,  GpioIntEdge|GpioIntSci,  GpioPlatformReset,  GpioTermNone, GpioPadConfigUnlock}},//Codec Interrupt

  {GPIO_VER4_S_GPP_G0,    {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutLow,      GpioIntDefault,          GpioPlatformReset,  GpioTermNone}},//Thunderbolt FRC_PWR
  //{GPIO_VER4_S_GPP_G1,    {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutHigh,     GpioIntDefault,          GpioHostDeepReset,  GpioTermNone}},//Thunderbolt CIO_PWR_EN, moved to PreMem
  {GPIO_VER4_S_GPP_G6,    {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutDefault,  GpioIntDefault,          GpioHostDeepReset,  GpioTermNone}},//M.2 Key-E slot for Wifi -> CNVI_EN_N
  {GPIO_VER4_S_GPP_G7,    {GpioPadModeGpio, GpioHostOwnGpio, GpioDirIn,      GpioOutDefault,  GpioIntDefault,          GpioPlatformReset,  GpioTermNone}},//Thunderbolt CIO_PLUG_EVENT_AIC_N

  {GPIO_VER4_S_GPP_H0,    {GpioPadModeGpio, GpioHostOwnGpio, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ, not used
  {GPIO_VER4_S_GPP_H1,    {GpioPadModeGpio, GpioHostOwnGpio, GpioDirIn,      GpioOutDefault,  GpioIntDefault,          GpioPlatformReset,  GpioTermNone, GpioPadConfigUnlock}},//Type-C Alert
  {GPIO_VER4_S_GPP_H2,    {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirInInv,   GpioOutDefault,  GpioIntEdge|GpioIntSci,  GpioResumeReset,    GpioTermNone, GpioPadConfigUnlock}},//M.2 Key-E slot for Wifi Wake
  {GPIO_VER4_S_GPP_H10,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirIn,      GpioOutDefault,  GpioIntEdge,             GpioResetDefault,   GpioTermNone}},//Power supply detect
  {GPIO_VER4_S_GPP_H12,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ, not used strap
  {GPIO_VER4_S_GPP_H13,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ, not used
  {GPIO_VER4_S_GPP_H14,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirInInv,   GpioOutDefault,  GpioIntEdge,             GpioPlatformReset,  GpioTermNone}},//VOL_UP
  {GPIO_VER4_S_GPP_H15,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutHigh,     GpioIntDefault,          GpioPlatformReset,  GpioTermNone}},//PCH SSD PLN Circuit Strap
  {GPIO_VER4_S_GPP_H17,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutHigh,     GpioIntDefault,          GpioPlatformReset,  GpioTermNone}},//M.2 Key-E slot for Wifi Reset
  {GPIO_VER4_S_GPP_H18,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ, not used
  {GPIO_VER4_S_GPP_H23,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutDefault,  GpioIntDefault,          GpioPlatformReset,  GpioTermNone}},//PCH SSD PLN Circuit

  {GPIO_VER4_S_GPP_I0,    {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutHigh,     GpioIntDefault,          GpioResumeReset,    GpioTermNone}},//PCIE AUX Power enable

  {GPIO_VER4_S_GPP_J8,    {GpioPadModeGpio, GpioHostOwnGpio, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioPlatformReset,  GpioTermNone}},//HiZ, not used
  {GPIO_VER4_S_GPP_J9,    {GpioPadModeGpio, GpioHostOwnGpio, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioPlatformReset,  GpioTermNone}},//HiZ, not used

  {GPIO_VER4_S_GPP_K0,    {GpioPadModeGpio, GpioHostOwnGpio, GpioDirInInv,   GpioOutDefault,  GpioIntDefault,          GpioPlatformReset,  GpioTermNone}},//Thunderbolt P1 P2 Alert
  {GPIO_VER4_S_GPP_K1,    {GpioPadModeGpio, GpioHostOwnGpio, GpioDirIn,      GpioOutDefault,  GpioIntDefault,          GpioPlatformReset,  GpioTermNone}},//Platform
  {GPIO_VER4_S_GPP_K3,    {GpioPadModeGpio, GpioHostOwnGpio, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioPlatformReset,  GpioTermNone}},//HiZ, not used
  {GPIO_VER4_S_GPP_K4,    {GpioPadModeGpio, GpioHostOwnGpio, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ, not used
  {GPIO_VER4_S_GPP_K5,    {GpioPadModeGpio, GpioHostOwnGpio, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ, not used
  {GPIO_VER4_S_GPP_K11,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ, not used

  {GPIO_VER4_S_GPP_R10,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutHigh,     GpioIntDefault,          GpioPlatformReset,  GpioTermNone}},//Codec Power enable
  {GPIO_VER4_S_GPP_R11,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutHigh,     GpioIntDefault,          GpioPlatformReset,  GpioTermNone}},//SATA Direct power enable
  {GPIO_VER4_S_GPP_R20,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutHigh,     GpioIntDefault,          GpioPlatformReset,  GpioTermNone}},//LAN Power enable
  {GPIO_VER4_S_GPP_R21,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutHigh,     GpioIntDefault,          GpioPlatformReset,  GpioTermNone}},//Touch panel power enable
  //{GPIO_VER4_S_GPP_F11,   {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,     GpioOutHigh,     GpioIntDefault,          GpioPlatformReset,  GpioTermNone}},//PCIe SLOT_1 RTD3 Reset MIPI60 (Rework)

  {0x0}  // terminator
})}

#  mGpioTableAdlSDdr4UDimm2DCrbFab2
[PcdsDynamicExVpd.common.SkuIdAdlSAdpSDdr4UDimm2DCrbFab2]
gBoardModuleTokenSpaceGuid.VpdPcdBoardGpioTable|*|{CODE({

  {GPIO_VER4_S_GPD1,      {GpioPadModeGpio, GpioHostOwnGpio, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioPlatformReset,  GpioTermNone}},//HiZ, not used
  {GPIO_VER4_S_GPD7,      {GpioPadModeGpio, GpioHostOwnGpio, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ, not used
  {GPIO_VER4_S_GPD12,     {GpioPadModeGpio, GpioHostOwnGpio, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ, not used

  {GPIO_VER4_S_GPP_A14,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioPlatformReset,  GpioTermNone}},//HiZ, not used
  {GPIO_VER4_S_GPP_B2,    {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirInInv,   GpioOutDefault,  GpioIntEdge|GpioIntSci,  GpioHostDeepReset,  GpioTermNone, GpioPadConfigUnlock}},// M.2 Key-E Slot  Wifi Wake-> UART_BT_WAKE_N
  {GPIO_VER4_S_GPP_B3,    {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutHigh,     GpioIntDefault,          GpioPlatformReset,  GpioTermNone}},// M.2 Key-E Slot  BT RF Kill
  {GPIO_VER4_S_GPP_B4,    {GpioPadModeGpio, GpioHostOwnGpio, GpioDirIn,      GpioOutDefault,  GpioIntEdge,             GpioPlatformReset,  GpioTermNone}},// EC SMI
  {GPIO_VER4_S_GPP_B6,    {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,     GpioOutHigh,     GpioIntDefault,          GpioPlatformReset,  GpioTermNone}},// SSD_3 RTD3 Reset
  {GPIO_VER4_S_GPP_B14,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}}, //EC Speaker (Rework)Strap
  {GPIO_VER4_S_GPP_B19,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutHigh,     GpioIntDefault,          GpioPlatformReset,  GpioTermNone}},// M.2 Key-E Slot  RF Kill
  {GPIO_VER4_S_GPP_B20,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirIn,      GpioOutDefault,  GpioIntEdge,             GpioResetDefault,   GpioTermNone}},// Type-C PD Detection
  {GPIO_VER4_S_GPP_B23,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioPlatformReset,  GpioTermNone}},//HiZ, not used. Strap

  {GPIO_VER4_S_GPP_C2,    {GpioPadModeGpio, GpioHostOwnGpio, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ, not used
  {GPIO_VER4_S_GPP_C5,    {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},// GPIO Automation (Rework) Strap
  {GPIO_VER4_S_GPP_C10,   {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,     GpioOutHigh,     GpioIntDefault,          GpioPlatformReset,  GpioTermNone}},// SSD_1 RTD3 Reset LPSS UART-0 header (Rework

  {GPIO_VER4_S_GPP_D3,    {GpioPadModeGpio, GpioHostOwnGpio, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ, not used
  {GPIO_VER4_S_GPP_D11,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutLow,      GpioIntDefault,          GpioHostDeepReset,  GpioTermNone}},//Type-C PD Controller SX Entry notification
  {GPIO_VER4_S_GPP_D13,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirInInv,   GpioOutDefault,  GpioIntEdge,             GpioHostDeepReset,  GpioTermNone}},//Type-C PD Controller SX ACK notification
  {GPIO_VER4_S_GPP_D14,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirInInv,   GpioOutDefault,  GpioIntEdge,             GpioPlatformReset,  GpioTermNone}},//PS_ON Override
  {GPIO_VER4_S_GPP_D22,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutHigh,     GpioIntDefault,          GpioPlatformReset,  GpioTermNone, GpioPadUnlock}},//GPP_D_22_UART3_RTSB_THC1_SPI2_RST_N
  {GPIO_VER4_S_GPP_D23,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirInInv,   GpioOutDefault,  GpioIntEdge|GpioIntApic, GpioResumeReset,    GpioTermNone, GpioPadUnlock}},//GPP_D_23_UART3_CTSB_THC1_SPI2_INT_N

  {GPIO_VER4_S_GPP_E6,    {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirInInv,   GpioOutDefault,  GpioIntEdge|GpioIntSci,  GpioHostDeepReset,  GpioTermNone, GpioPadConfigUnlock}},//PEG SLOT_1 Wake
  {GPIO_VER4_S_GPP_E7,    {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirInInv,   GpioOutDefault,  GpioIntEdge|GpioIntSci,  GpioHostDeepReset,  GpioTermNone, GpioPadConfigUnlock}},//PEG SLOT_2 Wake  Sinai DR0 (Rework)

  {GPIO_VER4_S_GPP_F0,    {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirInInv,   GpioOutDefault,  GpioIntEdge|GpioIntSci,  GpioHostDeepReset,  GpioTermNone, GpioPadConfigUnlock}},//PCIe SLOT_1 Wake MIPI60 (Rework)
  {GPIO_VER4_S_GPP_F1,    {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirInInv,   GpioOutDefault,  GpioIntEdge|GpioIntSci,  GpioHostDeepReset,  GpioTermNone, GpioPadConfigUnlock}},//PCIe SLOT_2 Wake MIPI60 (Rework)
  {GPIO_VER4_S_GPP_F2,    {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirInInv,   GpioOutDefault,  GpioIntEdge|GpioIntSci,  GpioHostDeepReset,  GpioTermNone, GpioPadConfigUnlock}},//PCIe SLOT_3 Wake MIPI60 (Rework)
  {GPIO_VER4_S_GPP_F3,    {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirInInv,   GpioOutDefault,  GpioIntEdge|GpioIntSci,  GpioPlatformReset,  GpioTermNone, GpioPadConfigUnlock}},//Not Used MIPI60 (Rework)
  {GPIO_VER4_S_GPP_F5,    {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutDefault,  GpioIntDefault,          GpioHostDeepReset,  GpioTermNone}},//PEG SLOT_2  MIPI60 (Rework)
  {GPIO_VER4_S_GPP_F7,    {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutDefault,  GpioIntDefault,          GpioPlatformReset,  GpioTermNone}},//PEG SLOT_1  MIPI60 (Rework)
  {GPIO_VER4_S_GPP_F8,    {GpioPadModeGpio, GpioHostOwnGpio, GpioDirIn,      GpioOutDefault,  GpioIntDefault,          GpioPlatformReset,  GpioTermNone}},//PEG SLOT_2  MIPI60 (Rework)
  {GPIO_VER4_S_GPP_F9,    {GpioPadModeGpio, GpioHostOwnGpio, GpioDirInInv,   GpioOutDefault,  GpioIntDefault,          GpioPlatformReset,  GpioTermNone}},//PEG SLOT_1  MIPI60 (Rework)
  {GPIO_VER4_S_GPP_F10,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutDefault,  GpioIntDefault,          GpioPlatformReset,  GpioTermNone}},//PEG SLOT_2   MIPI60 (Rework)
  {GPIO_VER4_S_GPP_F15,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirDefault, GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//SKTOCC MIPI60 (Rework)
  {GPIO_VER4_S_GPP_F16,   {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,     GpioOutHigh,     GpioIntDefault,          GpioPlatformReset,  GpioTermNone}},//M.2 SSD_2 RTD3 Reset MIPI60 (Rework)
  {GPIO_VER4_S_GPP_F17,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//TPM module MIPI60 (Rework)
  {GPIO_VER4_S_GPP_F18,   {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,     GpioOutHigh,     GpioIntDefault,          GpioPlatformReset,  GpioTermNone}},//HBR SSD Reset
  {GPIO_VER4_S_GPP_F23,   {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirInInv,   GpioOutDefault,  GpioIntEdge|GpioIntSci,  GpioPlatformReset,  GpioTermNone, GpioPadConfigUnlock}},//Codec Interrupt

  {GPIO_VER4_S_GPP_G0,    {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutLow,      GpioIntDefault,          GpioPlatformReset,  GpioTermNone}},//Thunderbolt FRC_PWR
  //{GPIO_VER4_S_GPP_G1,    {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutHigh,     GpioIntDefault,          GpioHostDeepReset,  GpioTermNone}},//Thunderbolt CIO_PWR_EN, moved to PreMem
  {GPIO_VER4_S_GPP_G6,    {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutDefault,  GpioIntDefault,          GpioHostDeepReset,  GpioTermNone}},//M.2 Key-E slot for Wifi -> CNVI_EN_N
  {GPIO_VER4_S_GPP_G7,    {GpioPadModeGpio, GpioHostOwnGpio, GpioDirIn,      GpioOutDefault,  GpioIntDefault,          GpioPlatformReset,  GpioTermNone}},//Thunderbolt CIO_PLUG_EVENT_AIC_N

  {GPIO_VER4_S_GPP_H0,    {GpioPadModeGpio, GpioHostOwnGpio, GpioDirIn,      GpioOutDefault,  GpioIntDefault,          GpioPlatformReset,  GpioTermNone}},//PCIe SLOT_1 Clock req
  {GPIO_VER4_S_GPP_H1,    {GpioPadModeGpio, GpioHostOwnGpio, GpioDirIn,      GpioOutDefault,  GpioIntDefault,          GpioPlatformReset,  GpioTermNone, GpioPadConfigUnlock}},//Type-C Alert
  {GPIO_VER4_S_GPP_H2,    {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirInInv,   GpioOutDefault,  GpioIntEdge|GpioIntSci,  GpioResumeReset,    GpioTermNone, GpioPadConfigUnlock}},//M.2 Key-E slot for Wifi Wake
  {GPIO_VER4_S_GPP_H10,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirIn,      GpioOutDefault,  GpioIntEdge,             GpioResetDefault,   GpioTermNone}},//Power supply detect
  {GPIO_VER4_S_GPP_H12,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ, not used strap
  {GPIO_VER4_S_GPP_H13,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ, not used
  {GPIO_VER4_S_GPP_H14,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirInInv,   GpioOutDefault,  GpioIntEdge,             GpioPlatformReset,  GpioTermNone}},//VOL_UP
  {GPIO_VER4_S_GPP_H15,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutHigh,     GpioIntDefault,          GpioPlatformReset,  GpioTermNone}},//PCH SSD PLN Circuit Strap
  {GPIO_VER4_S_GPP_H17,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutHigh,     GpioIntDefault,          GpioPlatformReset,  GpioTermNone}},//M.2 Key-E slot for Wifi Reset
  {GPIO_VER4_S_GPP_H18,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ, not used
  {GPIO_VER4_S_GPP_H23,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutDefault,  GpioIntDefault,          GpioPlatformReset,  GpioTermNone}},//PCH SSD PLN Circuit

  {GPIO_VER4_S_GPP_I0,    {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutHigh,     GpioIntDefault,          GpioResumeReset,    GpioTermNone}},//PCIE AUX Power enable

  {GPIO_VER4_S_GPP_J8,    {GpioPadModeGpio, GpioHostOwnGpio, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioPlatformReset,  GpioTermNone}},//HiZ, not used
  {GPIO_VER4_S_GPP_J9,    {GpioPadModeGpio, GpioHostOwnGpio, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioPlatformReset,  GpioTermNone}},//HiZ, not used

  {GPIO_VER4_S_GPP_K0,    {GpioPadModeGpio, GpioHostOwnGpio, GpioDirInInv,   GpioOutDefault,  GpioIntDefault,          GpioPlatformReset,  GpioTermNone}},//Thunderbolt P1 P2 Alert
  {GPIO_VER4_S_GPP_K1,    {GpioPadModeGpio, GpioHostOwnGpio, GpioDirIn,      GpioOutDefault,  GpioIntDefault,          GpioPlatformReset,  GpioTermNone}},//Platform
  {GPIO_VER4_S_GPP_K3,    {GpioPadModeGpio, GpioHostOwnGpio, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioPlatformReset,  GpioTermNone}},//HiZ, not used
  {GPIO_VER4_S_GPP_K4,    {GpioPadModeGpio, GpioHostOwnGpio, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ, not used
  {GPIO_VER4_S_GPP_K5,    {GpioPadModeGpio, GpioHostOwnGpio, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ, not used
  {GPIO_VER4_S_GPP_K11,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ, not used

  {GPIO_VER4_S_GPP_R10,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutHigh,     GpioIntDefault,          GpioPlatformReset,  GpioTermNone}},//Codec Power enable
  {GPIO_VER4_S_GPP_R11,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutHigh,     GpioIntDefault,          GpioPlatformReset,  GpioTermNone}},//SATA Direct power enable
  {GPIO_VER4_S_GPP_R20,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutHigh,     GpioIntDefault,          GpioPlatformReset,  GpioTermNone}},//LAN Power enable
  {GPIO_VER4_S_GPP_R21,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutHigh,     GpioIntDefault,          GpioPlatformReset,  GpioTermNone}},//Touch panel power enable

  {0x0}  // terminator
})}

#  mGpioTableAdlSDdr5UDimm1DAep
[PcdsDynamicExVpd.common.SkuIdAdlSAdpSDdr5UDimm1DAep]
gBoardModuleTokenSpaceGuid.VpdPcdBoardGpioTable|*|{CODE({
  {GPIO_VER4_S_GPP_B2,   {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirInInv,   GpioOutDefault, GpioIntEdge | GpioIntSci, GpioHostDeepReset, GpioTermNone, GpioPadConfigUnlock }},// M.2 Key-E Slot  Wifi Wake-> UART_BT_WAKE_N
  {GPIO_VER4_S_GPP_B3,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutHigh,    GpioIntDefault,           GpioPlatformReset, GpioTermNone }},// M.2 Key-E Slot Wifi BT_RF_KILL_N
  {GPIO_VER4_S_GPP_B4,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirInInv,   GpioOutDefault, GpioIntEdge,              GpioPlatformReset, GpioTermNone }},// EC SMI
  {GPIO_VER4_S_GPP_B18,  {GpioPadModeGpio, GpioHostOwnGpio, GpioDirInInv,   GpioOutDefault, GpioIntEdge,              GpioPlatformReset, GpioTermNone }},// PD Controller 2 (Front Panel) I2C_ALERT_PD_FP_N
  {GPIO_VER4_S_GPP_B19,  {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutHigh,    GpioIntDefault,           GpioPlatformReset, GpioTermNone }},// M.2 Key-E Slot Wifi WIFI_RF_KILL_N
  {GPIO_VER4_S_GPP_B21,  {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutHigh,    GpioIntDefault,           GpioPlatformReset, GpioTermNone }},// Maple Ridge 2 (Front Panel) FP_USB_FRC_PWR
  {GPIO_VER4_S_GPP_B22,  {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutHigh,    GpioIntDefault,           GpioPlatformReset, GpioTermNone }},// Maple Ridge 1 (Back Panel) BP_USB_FRC_PWR

  {GPIO_VER4_S_GPP_C2,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutHigh,    GpioIntDefault,           GpioPlatformReset, GpioTermNone }},// M.2 SSD_1 Power enable (PCH SSD)
  {GPIO_VER4_S_GPP_C8,   {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirInInv,   GpioOutDefault, GpioIntEdge | GpioIntSci, GpioHostDeepReset, GpioTermNone, GpioPadConfigUnlock }},// FOXLAN Wake enable (Wired LAN - Foxville)
  {GPIO_VER4_S_GPP_C9,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutHigh,    GpioIntDefault,           GpioPlatformReset, GpioTermNone }},// FOXLAN  Reset (Wired LAN - Foxville)
  {GPIO_VER4_S_GPP_C10,  {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutHigh,    GpioIntDefault,           GpioPlatformReset, GpioTermNone }},// SSD_1 RTD3 Reset LPSS UART-0 header
  {GPIO_VER4_S_GPP_C11,  {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutHigh,    GpioIntDefault,           GpioPlatformReset, GpioTermNone }},// M.2 SSD_1 Power Off (PCH SSD)

  {GPIO_VER4_S_GPP_D11,  {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutHigh,    GpioIntDefault,           GpioPlatformReset, GpioTermNone }},// Maple Ridge 2 (Front Panel) TBT_FP_CIO_PWREN
  {GPIO_VER4_S_GPP_D13,  {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutHigh,    GpioIntDefault,           GpioPlatformReset, GpioTermNone }},// Maple Ridge 2 (Front Panel) TBT_FP_FRC_PWR
  {GPIO_VER4_S_GPP_D14,  {GpioPadModeGpio, GpioHostOwnGpio, GpioDirInInv,   GpioOutDefault, GpioIntEdge,              GpioPlatformReset, GpioTermNone }},// Maple Ridge 2 (Front Panel) TBT_FP_CIO_EVENT_N

  {GPIO_VER4_S_GPP_E5,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutHigh,    GpioIntDefault,           GpioPlatformReset, GpioTermNone }},// PEG SLOT DGPU_PWREN_N
  {GPIO_VER4_S_GPP_E6,   {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirInInv,   GpioOutDefault, GpioIntEdge | GpioIntSci, GpioHostDeepReset, GpioTermNone, GpioPadConfigUnlock }},// PEG SLOT WAKE_N

  {GPIO_VER4_S_GPP_F2,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirIn,      GpioOutDefault, GpioIntDefault,           GpioResetDefault,  GpioTermNone }},// PEG SLOT EDSFF PRSNT_N
  {GPIO_VER4_S_GPP_F3,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutHigh,    GpioIntDefault,           GpioPlatformReset, GpioTermNone }},// FOXLAN Disable N (Wired LAN - Foxville)
  {GPIO_VER4_S_GPP_F4,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutLow,    GpioIntDefault,           GpioResetDefault,  GpioTermNone }},// PEG SLOT EDSFF LED_N
  {GPIO_VER4_S_GPP_F7,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutHigh,    GpioIntDefault,           GpioPlatformReset, GpioTermNone }},// PEG SLOT PCH_DGPU_SEL_N
  {GPIO_VER4_S_GPP_F9,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirIn,      GpioOutDefault, GpioIntLevel,             GpioPlatformReset, GpioTermNone }},// PEG SLOT PCH_DGPU_PWR_OK
  {GPIO_VER4_S_GPP_F12,  {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutHigh,    GpioIntDefault,           GpioResetDefault,  GpioTermNone }},// EDSFF RST_N
  {GPIO_VER4_S_GPP_F13,  {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutLow,    GpioIntDefault,           GpioResetDefault,  GpioTermNone }},// EDSFF  PWRDIS
  {GPIO_VER4_S_GPP_F15,  {GpioPadModeGpio, GpioHostOwnGpio, GpioDirInInv,   GpioOutDefault, GpioIntLevel,             GpioPlatformReset, GpioTermNone }},// CPU Socket SKTOCC_N
  {GPIO_VER4_S_GPP_F16,  {GpioPadModeGpio, GpioHostOwnGpio, GpioDirIn,      GpioOutDefault, GpioIntEdge,              GpioPlatformReset, GpioTermNone }},// PD Controller2 (FP) TBT_FP_SOIX_ENTRY_ACK
  {GPIO_VER4_S_GPP_F17,  {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutHigh,    GpioIntDefault,           GpioPlatformReset, GpioTermNone }},// TPM module TPM_PIRQ
  {GPIO_VER4_S_GPP_F18,  {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutHigh,    GpioIntDefault,           GpioPlatformReset, GpioTermNone }},// M.2 Key-M Slot(CPU SSD) CPUSSD_RST_N
  {GPIO_VER4_S_GPP_F19,  {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutHigh,    GpioIntDefault,           GpioResetDefault,  GpioTermNone }},// M.2 SSD1 CPUSSD_PWROFF_N
  {GPIO_VER4_S_GPP_F20,  {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutHigh,    GpioIntDefault,           GpioResetDefault,  GpioTermNone }},// EDSFF SMBRST_N

  {GPIO_VER4_S_GPP_G0,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutHigh,    GpioIntDefault,           GpioPlatformReset, GpioTermNone }},// Maple Ridge 2 (Back Panel) TBT_BP_FRC_PWR
  {GPIO_VER4_S_GPP_G1,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutHigh,    GpioIntDefault,           GpioHostDeepReset, GpioTermNone }},// Maple Ridge 2 (Back Panel) TBT_BP_CIO_PWREN
  {GPIO_VER4_S_GPP_G3,   {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirInInv,   GpioOutDefault, GpioIntSci | GpioIntDis,  GpioHostDeepReset, GpioTermNone, GpioPadConfigUnlock }},// TBT_PAB_WAKE_GPIO
  {GPIO_VER4_S_GPP_G4,   {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirInInv,   GpioOutDefault, GpioIntSci | GpioIntDis,  GpioHostDeepReset, GpioTermNone, GpioPadConfigUnlock }},// TBT_PCD_WAKE_GPIO
  {GPIO_VER4_S_GPP_G6,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutLow,     GpioIntDefault,           GpioResumeReset,   GpioTermNone }},// CNVi VR PWREN_N
  {GPIO_VER4_S_GPP_G7,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirInInv,   GpioOutDefault, GpioIntEdge,              GpioPlatformReset, GpioTermNone }},// Maple Ridge 1 (Back Panel) TBT_CIO_EVENT_PAB_N

  {GPIO_VER4_S_GPP_H1,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirInInv,   GpioOutDefault, GpioIntEdge,              GpioHostDeepReset, GpioTermNone }},// PD Controller1(Back Panel) I2C_ALERT_PD_BP_N
  {GPIO_VER4_S_GPP_H2,   {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirInInv,   GpioOutDefault, GpioIntEdge | GpioIntSci, GpioHostDeepReset, GpioTermNone, GpioPadConfigUnlock }},// M.2 Key-E slot for Wifi WAKE_N
  {GPIO_VER4_S_GPP_H9,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirIn,      GpioOutDefault, GpioIntEdge,              GpioResetDefault,  GpioTermNone }},// PD Controller 1&2 USBC_PSON_OVERRIDE_N
  {GPIO_VER4_S_GPP_H10,  {GpioPadModeGpio, GpioHostOwnGpio, GpioDirInInv,   GpioOutDefault, GpioIntLevel,             GpioPlatformReset, GpioTermNone }},// 2x4 Power supply detect
  {GPIO_VER4_S_GPP_H16,  {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutHigh,    GpioIntDefault,           GpioPlatformReset, GpioTermNone }},// M.2 Key-M Slot (CPU SSD) CPUSSD_PWREN
  {GPIO_VER4_S_GPP_H17,  {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutHigh,    GpioIntDefault,           GpioPlatformReset, GpioTermNone }},// M.2 Key-E slot for Wifi WLAN_RST_N

  {GPIO_VER4_S_GPP_I0,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutHigh,    GpioIntDefault,           GpioResumeReset,   GpioTermNone }},// PEG Slot PEG_AUX_PWREN

  {GPIO_VER4_S_GPP_J8,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirIn,      GpioOutDefault, GpioIntLevel,             GpioPlatformReset, GpioTermNone }},// Test Setup Menu  bias resistors TEST_SETUP_MENU
  {GPIO_VER4_S_GPP_J9,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirIn,      GpioOutDefault, GpioIntLevel,             GpioPlatformReset, GpioTermNone }},// PU/PD resistors (default PD) SV_DETECT

  {GPIO_VER4_S_GPP_K0,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirIn,      GpioOutDefault, GpioIntDefault,           GpioPlatformReset, GpioTermNone }},// Maple Ridge 1 (Back Panel) TBT_BP_SOIX_ENTRY_ACK
  {GPIO_VER4_S_GPP_K1,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutHigh,    GpioIntDefault,           GpioPlatformReset, GpioTermNone }},// EC_SLP_S0_CS_N
  {GPIO_VER4_S_GPP_K2,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutHigh,    GpioIntDefault,           GpioPlatformReset, GpioTermNone }},// PD Controllers 1 & 2 DG_S0IX_ENTRY_REQ_PD_N
  {GPIO_VER4_S_GPP_K3,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutHigh,    GpioIntDefault,           GpioResetDefault,  GpioTermNone }},// Maple Ridge 2 (Front Panel) TBT_FP_RST_N
  {GPIO_VER4_S_GPP_K4,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutHigh,    GpioIntDefault,           GpioResetDefault,  GpioTermNone }},// Maple Ridge 1 (Back Panel) TBT_BP_RST_N

  {GPIO_VER4_S_GPP_R20,  {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutHigh,    GpioIntDefault,           GpioResumeReset,   GpioTermNone }},// FOXLAN Power EN(Wired LAN - Foxville)

  // HI_Z GPIO Pins
  // M.2 Key-E Slot for Wifi
  {GPIO_VER4_S_GPP_D6,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirNone,    GpioOutDefault, GpioIntDefault,           GpioResetDefault,  GpioTermNone }},// I2S2_TXD
  {GPIO_VER4_S_GPP_D7,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirNone,    GpioOutDefault, GpioIntDefault,           GpioResetDefault,  GpioTermNone }},// I2S2_RXD
  {GPIO_VER4_S_GPP_D8,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirNone,    GpioOutDefault, GpioIntDefault,           GpioResetDefault,  GpioTermNone }},// I2S2_SCLK
  //STRAP ONLY
  {GPIO_VER4_S_GPP_B23,  {GpioPadModeGpio, GpioHostOwnGpio, GpioDirNone,    GpioOutDefault, GpioIntDefault,           GpioResetDefault,  GpioTermNone }},// SML1ALERTB_PCHHOT_N
  {GPIO_VER4_S_GPP_D2,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirNone,    GpioOutDefault, GpioIntDefault,           GpioResetDefault,  GpioTermNone }},// SV_ADVANCE_STRAP
  {GPIO_VER4_S_GPP_D3,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirNone,    GpioOutDefault, GpioIntDefault,           GpioResetDefault,  GpioTermNone }},// GFX_DETECT_STRAP
  {GPIO_VER4_S_GPP_H12,  {GpioPadModeGpio, GpioHostOwnGpio, GpioDirNone,    GpioOutDefault, GpioIntDefault,           GpioResetDefault,  GpioTermNone }},// SML2ALERT_N
  {GPIO_VER4_S_GPP_H18,  {GpioPadModeGpio, GpioHostOwnGpio, GpioDirNone,    GpioOutDefault, GpioIntDefault,           GpioResetDefault,  GpioTermNone }},// SML4ALERT_N
                                                                                                                                                          //NC
  {GPIO_VER4_S_GPP_H12,  {GpioPadModeGpio, GpioHostOwnGpio, GpioDirNone,    GpioOutDefault, GpioIntDefault,           GpioResetDefault,  GpioTermNone }},
  {GPIO_VER4_S_GPP_K5,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirNone,    GpioOutDefault, GpioIntDefault,           GpioResetDefault,  GpioTermNone }},
  {GPIO_VER4_S_GPP_K11,  {GpioPadModeGpio, GpioHostOwnGpio, GpioDirNone,    GpioOutDefault, GpioIntDefault,           GpioResetDefault,  GpioTermNone }},

  {0x0}  // terminator
})}

#  mGpioTableAdlSDdr5UDimm1DAepFab2
[PcdsDynamicExVpd.common.SkuIdAdlSAdpSDdr5UDimm1DAepFab2]
gBoardModuleTokenSpaceGuid.VpdPcdBoardGpioTable|*|{CODE({
  {GPIO_VER4_S_GPP_B1,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirInInv,   GpioOutDefault, GpioIntEdge,              GpioPlatformReset, GpioTermNone }},// PD Controller 2 (Front Panel) I2C_ALERT_PD_FP_N
  {GPIO_VER4_S_GPP_B2,   {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirInInv,   GpioOutDefault, GpioIntEdge | GpioIntSci, GpioHostDeepReset, GpioTermNone, GpioPadConfigUnlock }},// M.2 Key-E Slot  Wifi Wake-> UART_BT_WAKE_N
  {GPIO_VER4_S_GPP_B3,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutHigh,    GpioIntDefault,           GpioPlatformReset, GpioTermNone }},// M.2 Key-E Slot Wifi BT_RF_KILL_N
  {GPIO_VER4_S_GPP_B4,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirInInv,   GpioOutDefault, GpioIntEdge,              GpioPlatformReset, GpioTermNone }},// EC SMI
  {GPIO_VER4_S_GPP_B19,  {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutHigh,    GpioIntDefault,           GpioPlatformReset, GpioTermNone }},// M.2 Key-E Slot Wifi WIFI_RF_KILL_N
  {GPIO_VER4_S_GPP_B21,  {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutHigh,    GpioIntDefault,           GpioPlatformReset, GpioTermNone }},// Maple Ridge 2 (Front Panel) FP_USB_FRC_PWR
  {GPIO_VER4_S_GPP_B22,  {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutHigh,    GpioIntDefault,           GpioPlatformReset, GpioTermNone }},// Maple Ridge 1 (Back Panel) BP_USB_FRC_PWR

  {GPIO_VER4_S_GPP_C2,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutHigh,    GpioIntDefault,           GpioPlatformReset, GpioTermNone }},// M.2 SSD_1 Power enable (PCH SSD)
  {GPIO_VER4_S_GPP_C8,   {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirInInv,   GpioOutDefault, GpioIntEdge | GpioIntSci, GpioHostDeepReset, GpioTermNone, GpioPadConfigUnlock }},// FOXLAN Wake enable (Wired LAN - Foxville)
  {GPIO_VER4_S_GPP_C9,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutHigh,    GpioIntDefault,           GpioPlatformReset, GpioTermNone }},// FOXLAN  Reset (Wired LAN - Foxville)
  {GPIO_VER4_S_GPP_C10,  {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutHigh,    GpioIntDefault,           GpioPlatformReset, GpioTermNone }},// SSD_1 RTD3 Reset LPSS UART-0 header
  {GPIO_VER4_S_GPP_C11,  {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutHigh,    GpioIntDefault,           GpioPlatformReset, GpioTermNone }},// M.2 SSD_1 Power Off (PCH SSD)

  {GPIO_VER4_S_GPP_D11,  {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutHigh,    GpioIntDefault,           GpioPlatformReset, GpioTermNone }},// Maple Ridge 2 (Front Panel) TBT_FP_CIO_PWREN
  {GPIO_VER4_S_GPP_D13,  {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutHigh,    GpioIntDefault,           GpioPlatformReset, GpioTermNone }},// Maple Ridge 2 (Front Panel) TBT_FP_FRC_PWR
  {GPIO_VER4_S_GPP_D14,  {GpioPadModeGpio, GpioHostOwnGpio, GpioDirInInv,   GpioOutDefault, GpioIntEdge,              GpioPlatformReset, GpioTermNone }},// Maple Ridge 2 (Front Panel) TBT_FP_CIO_EVENT_N

  {GPIO_VER4_S_GPP_E5,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutHigh,    GpioIntDefault,           GpioPlatformReset, GpioTermNone }},// PEG SLOT DGPU_PWREN_N
  {GPIO_VER4_S_GPP_E6,   {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirInInv,   GpioOutDefault, GpioIntEdge | GpioIntSci, GpioHostDeepReset, GpioTermNone, GpioPadConfigUnlock }},// PEG SLOT WAKE_N

  {GPIO_VER4_S_GPP_F2,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirIn,      GpioOutDefault, GpioIntDefault,           GpioResetDefault,  GpioTermNone }},// PEG SLOT EDSFF PRSNT_N
  {GPIO_VER4_S_GPP_F3,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutHigh,    GpioIntDefault,           GpioPlatformReset, GpioTermNone }},// FOXLAN Disable N (Wired LAN - Foxville)
  {GPIO_VER4_S_GPP_F4,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutLow,    GpioIntDefault,           GpioResetDefault,  GpioTermNone }},// PEG SLOT EDSFF LED_N
  {GPIO_VER4_S_GPP_F7,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutHigh,    GpioIntDefault,           GpioPlatformReset, GpioTermNone }},// PEG SLOT PCH_DGPU_SEL_N
  {GPIO_VER4_S_GPP_F9,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirIn,      GpioOutDefault, GpioIntLevel,             GpioPlatformReset, GpioTermNone }},// PEG SLOT PCH_DGPU_PWR_OK
  {GPIO_VER4_S_GPP_F12,  {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutHigh,    GpioIntDefault,           GpioResetDefault,  GpioTermNone }},// EDSFF RST_N
  {GPIO_VER4_S_GPP_F13,  {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutLow,    GpioIntDefault,           GpioResetDefault,  GpioTermNone }},// EDSFF  PWRDIS
  {GPIO_VER4_S_GPP_F15,  {GpioPadModeGpio, GpioHostOwnGpio, GpioDirInInv,   GpioOutDefault, GpioIntLevel,             GpioPlatformReset, GpioTermNone }},// CPU Socket SKTOCC_N
  {GPIO_VER4_S_GPP_F16,  {GpioPadModeGpio, GpioHostOwnGpio, GpioDirIn,      GpioOutDefault, GpioIntEdge,              GpioPlatformReset, GpioTermNone }},// PD Controller2 (FP) TBT_FP_SOIX_ENTRY_ACK
  {GPIO_VER4_S_GPP_F17,  {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutHigh,    GpioIntDefault,           GpioPlatformReset, GpioTermNone }},// TPM module TPM_PIRQ
  {GPIO_VER4_S_GPP_F18,  {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutHigh,    GpioIntDefault,           GpioPlatformReset, GpioTermNone }},// M.2 Key-M Slot(CPU SSD) CPUSSD_RST_N
  {GPIO_VER4_S_GPP_F19,  {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutHigh,    GpioIntDefault,           GpioResetDefault,  GpioTermNone }},// M.2 SSD1 CPUSSD_PWROFF_N
  {GPIO_VER4_S_GPP_F20,  {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutHigh,    GpioIntDefault,           GpioResetDefault,  GpioTermNone }},// EDSFF SMBRST_N

  {GPIO_VER4_S_GPP_G0,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutHigh,    GpioIntDefault,           GpioPlatformReset, GpioTermNone }},// Maple Ridge 2 (Back Panel) TBT_BP_FRC_PWR
  {GPIO_VER4_S_GPP_G1,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutHigh,    GpioIntDefault,           GpioHostDeepReset, GpioTermNone }},// Maple Ridge 2 (Back Panel) TBT_BP_CIO_PWREN
  {GPIO_VER4_S_GPP_G3,   {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirInInv,   GpioOutDefault, GpioIntSci | GpioIntDis,  GpioHostDeepReset, GpioTermNone, GpioPadConfigUnlock }},// TBT_PAB_WAKE_GPIO
  {GPIO_VER4_S_GPP_G4,   {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirInInv,   GpioOutDefault, GpioIntSci | GpioIntDis,  GpioHostDeepReset, GpioTermNone, GpioPadConfigUnlock }},// TBT_PCD_WAKE_GPIO
  {GPIO_VER4_S_GPP_G6,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutLow,     GpioIntDefault,           GpioResumeReset,   GpioTermNone }},// CNVi VR PWREN_N
  {GPIO_VER4_S_GPP_G7,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirInInv,   GpioOutDefault, GpioIntEdge,              GpioPlatformReset, GpioTermNone }},// Maple Ridge 1 (Back Panel) TBT_CIO_EVENT_PAB_N

  {GPIO_VER4_S_GPP_H1,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirInInv,   GpioOutDefault, GpioIntEdge,              GpioHostDeepReset, GpioTermNone }},// PD Controller1(Back Panel) I2C_ALERT_PD_BP_N
  {GPIO_VER4_S_GPP_H2,   {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirInInv,   GpioOutDefault, GpioIntEdge | GpioIntSci, GpioHostDeepReset, GpioTermNone, GpioPadConfigUnlock }},// M.2 Key-E slot for Wifi WAKE_N
  {GPIO_VER4_S_GPP_H9,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirIn,      GpioOutDefault, GpioIntEdge,              GpioResetDefault,  GpioTermNone }},// PD Controller 1&2 USBC_PSON_OVERRIDE_N
  {GPIO_VER4_S_GPP_H10,  {GpioPadModeGpio, GpioHostOwnGpio, GpioDirInInv,   GpioOutDefault, GpioIntLevel,             GpioPlatformReset, GpioTermNone }},// 2x4 Power supply detect
  {GPIO_VER4_S_GPP_H16,  {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutHigh,    GpioIntDefault,           GpioPlatformReset, GpioTermNone }},// M.2 Key-M Slot (CPU SSD) CPUSSD_PWREN
  {GPIO_VER4_S_GPP_H17,  {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutHigh,    GpioIntDefault,           GpioPlatformReset, GpioTermNone }},// M.2 Key-E slot for Wifi WLAN_RST_N

  {GPIO_VER4_S_GPP_I0,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutHigh,    GpioIntDefault,           GpioResumeReset,   GpioTermNone }},// PEG Slot PEG_AUX_PWREN

  {GPIO_VER4_S_GPP_J8,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirIn,      GpioOutDefault, GpioIntLevel,             GpioPlatformReset, GpioTermNone }},// Test Setup Menu  bias resistors TEST_SETUP_MENU
  {GPIO_VER4_S_GPP_J9,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirIn,      GpioOutDefault, GpioIntLevel,             GpioPlatformReset, GpioTermNone }},// PU/PD resistors (default PD) SV_DETECT

  {GPIO_VER4_S_GPP_K0,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirIn,      GpioOutDefault, GpioIntDefault,           GpioPlatformReset, GpioTermNone }},// Maple Ridge 1 (Back Panel) TBT_BP_SOIX_ENTRY_ACK
  {GPIO_VER4_S_GPP_K1,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutHigh,    GpioIntDefault,           GpioPlatformReset, GpioTermNone }},// EC_SLP_S0_CS_N
  {GPIO_VER4_S_GPP_K2,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutHigh,    GpioIntDefault,           GpioPlatformReset, GpioTermNone }},// PD Controllers 1 & 2 DG_S0IX_ENTRY_REQ_PD_N
  {GPIO_VER4_S_GPP_K3,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutHigh,    GpioIntDefault,           GpioResetDefault,  GpioTermNone }},// Maple Ridge 2 (Front Panel) TBT_FP_RST_N
  {GPIO_VER4_S_GPP_K4,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutHigh,    GpioIntDefault,           GpioResetDefault,  GpioTermNone }},// Maple Ridge 1 (Back Panel) TBT_BP_RST_N

  {GPIO_VER4_S_GPP_R20,  {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutHigh,    GpioIntDefault,           GpioResumeReset,   GpioTermNone }},// FOXLAN Power EN(Wired LAN - Foxville)

  // HI_Z GPIO Pins
  // M.2 Key-E Slot for Wifi
  {GPIO_VER4_S_GPP_D6,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirNone,    GpioOutDefault, GpioIntDefault,           GpioResetDefault,  GpioTermNone }},// I2S2_TXD
  {GPIO_VER4_S_GPP_D7,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirNone,    GpioOutDefault, GpioIntDefault,           GpioResetDefault,  GpioTermNone }},// I2S2_RXD
  {GPIO_VER4_S_GPP_D8,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirNone,    GpioOutDefault, GpioIntDefault,           GpioResetDefault,  GpioTermNone }},// I2S2_SCLK
  //STRAP ONLY
  {GPIO_VER4_S_GPP_B23,  {GpioPadModeGpio, GpioHostOwnGpio, GpioDirNone,    GpioOutDefault, GpioIntDefault,           GpioResetDefault,  GpioTermNone }},// SML1ALERTB_PCHHOT_N
  {GPIO_VER4_S_GPP_D2,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirNone,    GpioOutDefault, GpioIntDefault,           GpioResetDefault,  GpioTermNone }},// SV_ADVANCE_STRAP
  {GPIO_VER4_S_GPP_D3,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirNone,    GpioOutDefault, GpioIntDefault,           GpioResetDefault,  GpioTermNone }},// GFX_DETECT_STRAP
  {GPIO_VER4_S_GPP_H12,  {GpioPadModeGpio, GpioHostOwnGpio, GpioDirNone,    GpioOutDefault, GpioIntDefault,           GpioResetDefault,  GpioTermNone }},// SML2ALERT_N
  {GPIO_VER4_S_GPP_H18,  {GpioPadModeGpio, GpioHostOwnGpio, GpioDirNone,    GpioOutDefault, GpioIntDefault,           GpioResetDefault,  GpioTermNone }},// SML4ALERT_N
                                                                                                                                                          //NC
  {GPIO_VER4_S_GPP_H12,  {GpioPadModeGpio, GpioHostOwnGpio, GpioDirNone,    GpioOutDefault, GpioIntDefault,           GpioResetDefault,  GpioTermNone }},
  {GPIO_VER4_S_GPP_K5,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirNone,    GpioOutDefault, GpioIntDefault,           GpioResetDefault,  GpioTermNone }},
  {GPIO_VER4_S_GPP_K11,  {GpioPadModeGpio, GpioHostOwnGpio, GpioDirNone,    GpioOutDefault, GpioIntDefault,           GpioResetDefault,  GpioTermNone }},

  {0x0}  // terminator
})}

#  mGpioTableAdlSDdr5UDimm1DOc
[PcdsDynamicExVpd.common.SkuIdAdlSAdpSDdr5UDimm1DOc]
gBoardModuleTokenSpaceGuid.VpdPcdBoardGpioTable|*|{CODE({
  // EC
  {GPIO_VER4_S_GPP_B4,    {GpioPadModeGpio, GpioHostOwnGpio, GpioDirIn,      GpioOutDefault,  GpioIntEdge,             GpioPlatformReset,  GpioTermNone}},// EC SMI
  {GPIO_VER4_S_GPP_B14,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},// EC Speaker (Rework)Strap

  // GPIO Automation (Rework)
  {GPIO_VER4_S_GPP_C5,    {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},// GPIO Automation (Rework) Strap

  // LAN Power enable
  {GPIO_VER4_S_GPP_R20,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutHigh,     GpioIntDefault,          GpioPlatformReset,  GpioTermNone}},//LAN Power enable

  // M.2 Key-E Slot for Wifi
  {GPIO_VER4_S_GPP_B2,    {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirInInv,   GpioOutDefault,  GpioIntEdge|GpioIntSci,  GpioHostDeepReset,  GpioTermNone, GpioPadConfigUnlock}},// M.2 Key-E Slot  Wifi Wake-> UART_BT_WAKE_N
  {GPIO_VER4_S_GPP_B3,    {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutHigh,     GpioIntDefault,          GpioPlatformReset,  GpioTermNone}},// M.2 Key-E Slot  BT RF Kill
  {GPIO_VER4_S_GPP_B19,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutHigh,     GpioIntDefault,          GpioPlatformReset,  GpioTermNone}},// M.2 Key-E Slot  RF Kill
  {GPIO_VER4_S_GPP_G6,    {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutDefault,  GpioIntDefault,          GpioHostDeepReset,  GpioTermNone}},// M.2 Key-E slot for Wifi -> CNVI_EN_N
  {GPIO_VER4_S_GPP_H2,    {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirInInv,   GpioOutDefault,  GpioIntEdge|GpioIntSci,  GpioResumeReset,    GpioTermNone, GpioPadConfigUnlock}},//M.2 Key-E slot for Wifi Wake
  {GPIO_VER4_S_GPP_H17,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutHigh,     GpioIntDefault,          GpioPlatformReset,  GpioTermNone}},//M.2 Key-E slot for Wifi Reset

  // HBR SSD
  {GPIO_VER4_S_GPP_F18,   {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,     GpioOutHigh,     GpioIntDefault,          GpioPlatformReset,  GpioTermNone}},//HBR SSD Reset

  // M2_SSD_1
  {GPIO_VER4_S_GPP_C10,   {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,     GpioOutHigh,     GpioIntDefault,          GpioPlatformReset,  GpioTermNone}},// SSD_1 RTD3 Reset LPSS UART-0 header (Rework

  // M.2 SSD_2
  {GPIO_VER4_S_GPP_F16,   {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,     GpioOutHigh,     GpioIntDefault,          GpioPlatformReset,  GpioTermNone}},//M.2 SSD_2 RTD3 Reset MIPI60 (Rework)

  // M.2 SSD_3
  {GPIO_VER4_S_GPP_B6,    {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,     GpioOutHigh,     GpioIntDefault,          GpioPlatformReset,  GpioTermNone}},// SSD_3 RTD3 Reset

  // PCIe SLOT_1
  {GPIO_VER4_S_GPP_F0,    {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirInInv,   GpioOutDefault,  GpioIntEdge|GpioIntSci,  GpioHostDeepReset,  GpioTermNone, GpioPadConfigUnlock}},//PCIe SLOT_1 Wake MIPI60 (Rework)

  // PCIe SLOT_2
  {GPIO_VER4_S_GPP_F1,    {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirInInv,   GpioOutDefault,  GpioIntEdge|GpioIntSci,  GpioResetDefault,   GpioTermNone, GpioPadConfigUnlock}},//PCIe SLOT_2 Wake MIPI60 (Rework)

  // PCIe SLOT_3
  {GPIO_VER4_S_GPP_F2,    {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirInInv,   GpioOutDefault,  GpioIntEdge|GpioIntSci,  GpioHostDeepReset,  GpioTermNone, GpioPadConfigUnlock}},//PCIe SLOT_3 Wake MIPI60 (Rework)

  // PEG SLOT_1
  {GPIO_VER4_S_GPP_E6,    {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirInInv,   GpioOutDefault,  GpioIntEdge|GpioIntSci,  GpioHostDeepReset,  GpioTermNone, GpioPadConfigUnlock}},//PEG SLOT_1 Wake
  {GPIO_VER4_S_GPP_F7,    {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutDefault,  GpioIntDefault,          GpioPlatformReset,  GpioTermNone}},//PEG SLOT_1  MIPI60 (Rework)
  {GPIO_VER4_S_GPP_F9,    {GpioPadModeGpio, GpioHostOwnGpio, GpioDirInInv,   GpioOutDefault,  GpioIntDefault,          GpioPlatformReset,  GpioTermNone}},//PEG SLOT_1  MIPI60 (Rework)

  // Type C
  {GPIO_VER4_S_GPP_B20,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirIn,      GpioOutDefault,  GpioIntEdge,             GpioResetDefault,   GpioTermNone}},//Type-C PD Detection

  // TPM module
  {GPIO_VER4_S_GPP_F17,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirInInv,   GpioOutDefault,  GpioIntEdge|GpioIntApic,  GpioPlatformReset,  GpioTermNone, GpioPadConfigUnlock}},//TPM module MIPI60 (Rework)

  //PCIe Aux Power Enable
  {GPIO_VER4_S_GPP_I0,    {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutHigh,     GpioIntDefault,          GpioResumeReset,    GpioTermNone}},//GPP_I_0_EXT_PWR_GATE_N

  //PLN
  {GPIO_VER4_S_GPP_H15,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutHigh,     GpioIntDefault,          GpioPlatformReset,    GpioTermNone}},//PCH SSD PLN Circuit Strap
  {GPIO_VER4_S_GPP_H23,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutDefault,  GpioIntDefault,          GpioPlatformReset,    GpioTermNone}},//PCH SSD PLN Circuit

  //Others
  {GPIO_VER4_S_GPP_K5,    {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutDefault,  GpioIntDefault,          GpioResetDefault,    GpioTermNone}},//DNX_IN_PROG_AUX_NBIAS
  {GPIO_VER4_S_GPP_K1,    {GpioPadModeGpio, GpioHostOwnGpio, GpioDirIn,      GpioOutDefault,  GpioIntDefault,          GpioPlatformReset,   GpioTermNone}},//Platform
  {GPIO_VER4_S_GPP_H10,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirIn,      GpioOutDefault,  GpioIntEdge,             GpioResetDefault,    GpioTermNone}},//Power supply detect
  {GPIO_VER4_S_GPP_H0,    {GpioPadModeGpio, GpioHostOwnGpio, GpioDirDefault, GpioOutDefault,  GpioIntDefault,          GpioResetDefault,    GpioTermNone}},//VAL_SV_ADVANCE_STRAP
  {GPIO_VER4_S_GPP_H14,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirInInv,   GpioOutDefault,  GpioIntEdge,             GpioPlatformReset,   GpioTermNone}},//VOL_UP

  //Not used
  {GPIO_VER4_S_GPD1,      {GpioPadModeGpio, GpioHostOwnGpio, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioPlatformReset,  GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPD7,      {GpioPadModeGpio, GpioHostOwnGpio, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPD12,     {GpioPadModeGpio, GpioHostOwnGpio, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPP_A14,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPP_B1,    {GpioPadModeGpio, GpioHostOwnGpio, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPP_B5,    {GpioPadModeGpio, GpioHostOwnGpio, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPP_B7,    {GpioPadModeGpio, GpioHostOwnGpio, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPP_B8,    {GpioPadModeGpio, GpioHostOwnGpio, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPP_B9,    {GpioPadModeGpio, GpioHostOwnGpio, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPP_B10,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPP_B11,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPP_B15,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPP_B16,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPP_B17,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPP_B18,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPP_B23,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPP_C2,    {GpioPadModeGpio, GpioHostOwnGpio, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPP_C6,    {GpioPadModeGpio, GpioHostOwnGpio, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPP_C7,    {GpioPadModeGpio, GpioHostOwnGpio, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPP_C12,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPP_C13,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPP_C14,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPP_C15,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPP_D3,    {GpioPadModeGpio, GpioHostOwnGpio, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//GFX_DETECT_STRAP
  {GPIO_VER4_S_GPP_D11,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPP_D13,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPP_D14,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPP_D16,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPP_D17,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPP_D18,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPP_D19,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPP_D20,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPP_D21,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPP_D22,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPP_D23,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPP_E3,    {GpioPadModeGpio, GpioHostOwnGpio, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPP_E7,    {GpioPadModeGpio, GpioHostOwnGpio, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPP_E13,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPP_E14,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPP_E15,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPP_E16,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPP_E17,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPP_E18,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPP_E19,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPP_E20,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPP_F3,    {GpioPadModeGpio, GpioHostOwnGpio, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPP_F5,    {GpioPadModeGpio, GpioHostOwnGpio, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPP_F6,    {GpioPadModeGpio, GpioHostOwnGpio, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPP_F8,    {GpioPadModeGpio, GpioHostOwnGpio, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPP_F10,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPP_F19,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPP_F20,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPP_F21,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPP_F22,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPP_F23,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPP_G0,    {GpioPadModeGpio, GpioHostOwnGpio, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPP_G1,    {GpioPadModeGpio, GpioHostOwnGpio, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPP_G7,    {GpioPadModeGpio, GpioHostOwnGpio, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPP_H1,    {GpioPadModeGpio, GpioHostOwnGpio, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPP_H12,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPP_H13,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPP_H18,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPP_H19,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPP_H20,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPP_H21,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPP_H22,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPP_I9,    {GpioPadModeGpio, GpioHostOwnGpio, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPP_I10,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPP_I11,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPP_I12,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPP_I14,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPP_I15,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPP_I16,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPP_I17,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPP_I18,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPP_I19,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPP_I20,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPP_I21,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPP_J8 ,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioPlatformReset,  GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPP_J9 ,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioPlatformReset,  GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPP_J10,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioPlatformReset,  GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPP_J11,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioPlatformReset,  GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPP_K0,    {GpioPadModeGpio, GpioHostOwnGpio, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioPlatformReset,  GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPP_K3,    {GpioPadModeGpio, GpioHostOwnGpio, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioPlatformReset,  GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPP_K4,    {GpioPadModeGpio, GpioHostOwnGpio, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPP_R0,    {GpioPadModeGpio, GpioHostOwnGpio, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPP_R1,    {GpioPadModeGpio, GpioHostOwnGpio, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPP_R2,    {GpioPadModeGpio, GpioHostOwnGpio, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPP_R3,    {GpioPadModeGpio, GpioHostOwnGpio, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPP_R5,    {GpioPadModeGpio, GpioHostOwnGpio, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPP_R6,    {GpioPadModeGpio, GpioHostOwnGpio, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPP_R8,    {GpioPadModeGpio, GpioHostOwnGpio, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPP_R9,    {GpioPadModeGpio, GpioHostOwnGpio, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPP_R10,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPP_R11,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPP_R21,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ

  {0x0}  // terminator

})}

#  mGpioTableAdlSSbgaDdr5SODimmErb
[PcdsDynamicExVpd.common.SkuIdAdlSAdpSSbgaDdr5SODimmErb]
gBoardModuleTokenSpaceGuid.VpdPcdBoardGpioTable|*|{CODE({
  // Codec
  {GPIO_VER4_S_GPP_R10,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutHigh,     GpioIntDefault,          GpioPlatformReset,  GpioTermNone}}, // Codec Power enable
  {GPIO_VER4_S_GPP_H13,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutHigh,     GpioIntDefault,          GpioResetDefault,   GpioTermNone}}, // BBR FRC power

  // EC
  {GPIO_VER4_S_GPP_B14,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirIn,      GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}}, // EC top swap override
  {GPIO_VER4_S_GPP_B20,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirInInv,   GpioOutDefault,  GpioIntEdge,             GpioResetDefault,   GpioTermNone, GpioPadConfigUnlock}}, // EC SMI
  {GPIO_VER4_S_GPP_K1,    {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutDefault,  GpioIntDefault,          GpioPlatformReset,  GpioTermNone}}, // MEC1521H SLP EC

  // SSD
  {GPIO_VER4_S_GPP_E5,    {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,     GpioOutHigh,     GpioIntDefault,          GpioResetDefault,   GpioTermNone}}, // CPU SSD power disable

  // DP signal
  {GPIO_VER4_S_GPP_F0,    {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutLow,      GpioIntDefault,          GpioHostDeepReset,   GpioTermNone}}, // DP MUXB signal

  // LAN
  {GPIO_VER4_S_GPP_R20,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutLow,      GpioIntDefault,          GpioResumeReset,    GpioTermNone}}, // LAN Power enable, Set Output high based on the setup option.
  {GPIO_VER4_S_GPP_G1,    {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutHigh,     GpioIntDefault,          GpioPlatformReset,  GpioTermNone}}, // Foxville Reset
  {GPIO_VER4_S_GPP_B21,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutHigh,     GpioIntDefault,          GpioResumeReset,    GpioTermNone}}, // Lan Disable, low to enable ULP
  {GPIO_VER4_S_GPD2,      {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirInInv,   GpioOutDefault,  GpioIntEdge,             GpioHostDeepReset,  GpioTermNone, GpioPadConfigUnlock}}, // FOX_WAKE_N. Set to SCI based on the setup option.
  {GPIO_VER4_S_GPP_C2,    {GpioPadModeGpio, GpioHostOwnGpio, GpioDirIn,      GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}}, // Foxville wake signal (Rework)

  // GPIO Automation (Rework)
  {GPIO_VER4_S_GPP_C5,    {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},// GPIO Automation (Rework) Strap

  // WLAN
  {GPIO_VER4_S_GPP_B2,    {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirInInv,   GpioOutDefault,  GpioIntEdge|GpioIntSci,  GpioHostDeepReset,  GpioTermNone, GpioPadConfigUnlock}},// UART BT Wake
  {GPIO_VER4_S_GPP_B3,    {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutHigh,     GpioIntDefault,          GpioPlatformReset,  GpioTermNone}},// M.2 Key-E Slot  BT RF Kill
  {GPIO_VER4_S_GPP_B19,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutHigh,     GpioIntDefault,          GpioPlatformReset,  GpioTermNone}},// M.2 Key-E Slot  RF Kill
  {GPIO_VER4_S_GPP_H2,    {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirInInv,   GpioOutDefault,  GpioIntEdge|GpioIntSci,  GpioResumeReset,    GpioTermNone, GpioPadConfigUnlock}},// M.2 Key-E slot for Wifi Wake
  {GPIO_VER4_S_GPP_H17,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutHigh,     GpioIntDefault,          GpioPlatformReset,  GpioTermNone}},// M.2 Key-E slot for Wifi Reset
  {GPIO_VER4_S_GPP_G6,    {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutHigh,     GpioIntDefault,          GpioHostDeepReset,  GpioTermNone}},// WLAN rail generation enable

  // WWAN
  {GPIO_VER4_S_GPP_G3,    {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirInInv,   GpioOutDefault,  GpioIntEdge|GpioIntSci,  GpioResetDefault,   GpioTermNone, GpioPadConfigUnlock}},//SAR_NIRQ_PCH

  //PLN
  {GPIO_VER4_S_GPP_H15,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutHigh,     GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//PCH SSD PLN Circuit Strap
  {GPIO_VER4_S_GPP_H23,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//PCH SSD PLN Circuit Delay selection

  //PCIe Aux Power Enable
  {GPIO_VER4_S_GPP_I0,    {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutHigh,     GpioIntDefault,          GpioResumeReset,    GpioTermNone}},//GPP_I_0_EXT_PWR_GATE_N

  // Thunderbolt
  {GPIO_VER4_S_GPP_B4,    {GpioPadModeGpio, GpioHostOwnGpio, GpioDirIn,      GpioOutDefault,  GpioIntDefault,          GpioPlatformReset,  GpioTermNone, GpioPadConfigUnlock}},// PD AIC presence detect
  {GPIO_VER4_S_GPP_F3,    {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutHigh,     GpioIntDefault,          GpioPlatformReset,  GpioTermNone}},//POR Thunderbolt USB Force Power, MIPI60 (Rework)
//  {GPIO_VER4_S_GPP_F5,    {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutHigh,     GpioIntDefault,          GpioHostDeepReset,   GpioTermNone}},//POR Thunderbolt RTD3 enable, MIPI60 (Rework)
  {GPIO_VER4_S_GPP_F9,    {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirInInv,   GpioOutDefault,  GpioIntEdge|GpioIntSci,  GpioHostDeepReset,   GpioTermNone, GpioPadConfigUnlock}},//POR Thunderbolt Wake , MIPI60 (Rework)
  {GPIO_VER4_S_GPP_F10,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirIn,      GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//POR PD AIC detect, MIPI60 (Rework)
  {GPIO_VER4_S_GPP_F16,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutHigh,     GpioIntDefault,          GpioHostDeepReset,  GpioTermNone}},//POR Thunderbolt Reset,MIPI60 (Rework)
  {GPIO_VER4_S_GPP_G0,    {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutLow,      GpioIntDefault,          GpioResumeReset,    GpioTermNone}},//POR Thunderbolt 0 and 1 FRC_PWR
  {GPIO_VER4_S_GPP_B22,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirInInv,   GpioOutDefault,  GpioIntEdge,             GpioPlatformReset,  GpioTermNone}},//Thunderbolt 1 plug event
//  {GPIO_VER4_S_GPP_E7,    {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutHigh,     GpioIntDefault,          GpioHostDeepReset,  GpioTermNone}},//Thunderbolt 1 RTD3 Power enable
  {GPIO_VER4_S_GPP_F2,    {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutHigh,     GpioIntDefault,          GpioHostDeepReset,  GpioTermNone}},//Thunderbolt RTD3 reset
  {GPIO_VER4_S_GPP_F13,   {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirInInv,   GpioOutDefault,  GpioIntEdge|GpioIntSci,  GpioHostDeepReset,   GpioTermNone, GpioPadConfigUnlock}},//Thunderbolt Wake signal
  {GPIO_VER4_S_GPP_G7,    {GpioPadModeGpio, GpioHostOwnGpio, GpioDirInInv,   GpioOutDefault,  GpioIntEdge,             GpioResetDefault,   GpioTermNone}},//Thunderbolt Plug event
  {GPIO_VER4_S_GPP_H1,    {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirInInv,   GpioOutDefault,  GpioIntEdge|GpioIntSci,  GpioPlatformReset,  GpioTermNone, GpioPadConfigUnlock}},//Thunderbolt interrupt (Rework)
  {GPIO_VER4_S_GPP_K0,    {GpioPadModeGpio, GpioHostOwnGpio, GpioDirInInv,   GpioOutDefault,  GpioIntEdge,             GpioPlatformReset,  GpioTermNone}},//Thunderbolt 0 AIC interrupt (PD alert)

  // Touch pad & panel
  {GPIO_VER4_S_GPP_E3,    {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutHigh,     GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//Touch pad power enable
  {GPIO_VER4_S_GPP_F22,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirInInv,   GpioOutDefault,  GpioIntEdge|GpioIntApic, GpioResetDefault,   GpioTermNone, GpioPadConfigUnlock}},//Touch pad interrupt
  {GPIO_VER4_S_GPP_A14,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutHigh,     GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//Touch panel 2 power enable
  {GPIO_VER4_S_GPP_R16,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirInInv,   GpioOutDefault,  GpioIntEdge|GpioIntApic, GpioResetDefault,   GpioTermNone, GpioPadConfigUnlock}},//Touch panel 2 interrupt
  {GPIO_VER4_S_GPP_R17,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutHigh,     GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//Touch panel 2 reset
  {GPIO_VER4_S_GPP_R21,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutHigh,     GpioIntDefault,          GpioPlatformReset,  GpioTermNone}},//Touch panel 1 power enable
  {GPIO_VER4_S_GPP_D23,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirInInv,   GpioOutDefault,  GpioIntEdge|GpioIntApic, GpioResumeReset,    GpioTermNone, GpioPadConfigUnlock}},//Touch panel 1 interrupt
  {GPIO_VER4_S_GPP_D22,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutHigh,     GpioIntDefault,          GpioPlatformReset,  GpioTermNone, GpioPadUnlock}},//Touch panel 1 reset

  // PEG
  {GPIO_VER4_S_GPP_E2,    {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,     GpioOutHigh,     GpioIntDefault,          GpioPlatformReset,  GpioTermNone}},//PEG Slot Reset
  {GPIO_VER4_S_GPP_E4,    {GpioPadModeGpio, GpioHostOwnGpio, GpioDirIn,      GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//PEG SLOT DGPU power status
  {GPIO_VER4_S_GPP_E6,    {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirInInv,   GpioOutDefault,  GpioIntEdge|GpioIntSci,  GpioHostDeepReset,  GpioTermNone, GpioPadConfigUnlock}},//PEG SLOT_1 Wake
  {GPIO_VER4_S_GPP_F7,    {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutDefault,  GpioIntDefault,          GpioPlatformReset,  GpioTermNone}},//PEG SLOT DGPU select

  //PCH x4 Slot
  {GPIO_VER4_S_GPP_E16,   {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirInInv,   GpioOutDefault,  GpioIntEdge|GpioIntSci,  GpioPlatformReset,  GpioTermNone, GpioPadConfigUnlock}}, //PCIe SLOT1 Wake
  {GPIO_VER4_S_GPP_E17,   {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,     GpioOutHigh,     GpioIntDefault,          GpioPlatformReset,  GpioTermNone}}, //PCH x4 Slot Reset

  // SATA
  {GPIO_VER4_S_GPP_R11,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutHigh,     GpioIntDefault,          GpioPlatformReset,  GpioTermNone}},//SATA Direct power enable

  // TPM module
  {GPIO_VER4_S_GPP_F17,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirInInv,   GpioOutDefault,  GpioIntEdge|GpioIntApic,  GpioPlatformReset,  GpioTermNone, GpioPadConfigUnlock}}, //TPM module - PIRQ

  // MIPI60
  {GPIO_VER4_S_GPP_F4,    {GpioPadModeGpio, GpioHostOwnGpio, GpioDirInOut,   GpioOutHigh,     GpioIntDefault,          GpioPlatformReset,  GpioTermNone}},//MIPI60 - data (Rework)
  {GPIO_VER4_S_GPP_F15,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirInOut,   GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//MIPI60 - data (Rework)

  // Others
  {GPIO_VER4_S_GPP_K5,    {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}}, // DNX_IN_PROG_AUX_NBIAS
  {GPIO_VER4_S_GPP_D3,    {GpioPadModeGpio, GpioHostOwnGpio, GpioDirIn,      GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}}, // GFX_DETECT_STRAP
  {GPIO_VER4_S_GPD7,      {GpioPadModeGpio, GpioHostOwnGpio, GpioDirIn,      GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}}, // XTAL input mode Strap pin
  {GPIO_VER4_S_GPP_H0,    {GpioPadModeGpio, GpioHostOwnGpio, GpioDirIn,      GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}}, // VAL_SV_ADVANCE_STRAP
  {GPIO_VER4_S_GPP_H14,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirInInv,   GpioOutDefault,  GpioIntEdge,             GpioPlatformReset,  GpioTermNone}}, // VOL_UP
  {GPIO_VER4_S_GPP_J8 ,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirIn,      GpioOutDefault,  GpioIntDefault,          GpioPlatformReset,  GpioTermNone}}, // Validation test setup menu
  {GPIO_VER4_S_GPP_J9 ,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirIn,      GpioOutDefault,  GpioIntDefault,          GpioPlatformReset,  GpioTermNone}}, // Validation SV detect
  {GPIO_VER4_S_GPP_K4 ,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirIn,      GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}}, // SKTOCC
  {GPIO_VER4_S_GPP_E13,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutDefault,  GpioIntDefault,          GpioHostDeepReset,  GpioTermNone}}, // GPIO Automation (Rework)
  {GPIO_VER4_S_GPP_E14,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutDefault,  GpioIntDefault,          GpioPlatformReset,  GpioTermNone}}, // GPIO Automation (Rework)

  // Test Point
  {GPIO_VER4_S_GPP_D13,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPP_D14,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPP_E15,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPP_E18,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPP_E19,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPP_E20,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPP_F14,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPD12,     {GpioPadModeGpio, GpioHostOwnGpio, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPP_I5,    {GpioPadModeGpio, GpioHostOwnGpio, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioPlatformReset,  GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPP_I6,    {GpioPadModeGpio, GpioHostOwnGpio, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioPlatformReset,  GpioTermNone}},//HiZ

  // not used
  {GPIO_VER4_S_GPP_B23,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPP_D1,    {GpioPadModeGpio, GpioHostOwnGpio, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPP_D7,    {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//not used
  {GPIO_VER4_S_GPP_D11,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPP_E0,    {GpioPadModeGpio, GpioHostOwnGpio, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioPlatformReset,  GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPP_E12,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioPlatformReset,  GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPP_F1,    {GpioPadModeGpio, GpioHostOwnGpio, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioHostDeepReset,  GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPP_F11,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioHostDeepReset,  GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPP_F12,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioHostDeepReset,  GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPP_H9,    {GpioPadModeGpio, GpioHostOwnGpio, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioPlatformReset,  GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPP_H12,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirIn,      GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//not used
  {GPIO_VER4_S_GPP_H18,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirIn,      GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//not used
  {GPIO_VER4_S_GPP_I8,    {GpioPadModeGpio, GpioHostOwnGpio, GpioDirIn,      GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//not used
  {GPIO_VER4_S_GPP_R6,    {GpioPadModeGpio, GpioHostOwnGpio, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPP_R7,    {GpioPadModeGpio, GpioHostOwnGpio, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPP_R8,    {GpioPadModeGpio, GpioHostOwnGpio, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPP_R18,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPP_R19,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ

  {0x0}  // terminator

})}

[PcdsDynamicExVpd.common.SkuIdAdlSAdpSSbgaDdr4SODimmCrb]
gBoardModuleTokenSpaceGuid.VpdPcdBoardGpioTable|*|{CODE({
  // Codec
  {GPIO_VER4_S_GPP_I8,    {GpioPadModeGpio, GpioHostOwnGpio, GpioDirInInv,   GpioOutDefault,  GpioIntEdge|GpioIntApic, GpioResetDefault,   GpioTermNone, GpioPadConfigUnlock}},// Codec Interrupt
  {GPIO_VER4_S_GPP_R10,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutHigh,     GpioIntDefault,          GpioPlatformReset,  GpioTermNone}},// Codec Power enable

  // EC
  {GPIO_VER4_S_GPP_B4,    {GpioPadModeGpio, GpioHostOwnGpio, GpioDirIn,      GpioOutDefault,  GpioIntEdge,             GpioPlatformReset,  GpioTermNone}},// EC SMI
  {GPIO_VER4_S_GPP_B14,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},// EC Speaker (Rework)Strap

  // GPIO Automation (Rework)
  {GPIO_VER4_S_GPP_C5,    {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},// GPIO Automation (Rework) Strap

  //Foxville LAN
  {GPIO_VER4_S_GPP_R20,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutLow,      GpioIntDefault,          GpioResumeReset,    GpioTermNone}}, // LAN_FOX_ON. Set Output high based on the setup option.
  {GPIO_VER4_S_GPP_G1,    {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutHigh,     GpioIntDefault,          GpioPlatformReset,  GpioTermNone}}, // PLTRST_FOX_N
  {GPIO_VER4_S_GPD1,      {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutHigh,     GpioIntDefault,          GpioResumeReset,    GpioTermNone}}, // FOX_LAN_DISABLE_N, low to enable ULP
  {GPIO_VER4_S_GPD2,      {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirInInv,   GpioOutDefault,  GpioIntEdge,             GpioHostDeepReset,  GpioTermNone, GpioPadConfigUnlock}}, // FOX_WAKE_N. Set to SCI based on the setup option.
  {GPIO_VER4_S_GPP_C2,    {GpioPadModeGpio, GpioHostOwnGpio, GpioDirIn,      GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}}, // Foxville wake signal (Rework)

  // M.2 Key-E Slot for Wifi
  {GPIO_VER4_S_GPP_B2,    {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirInInv,   GpioOutDefault,  GpioIntEdge|GpioIntSci,  GpioHostDeepReset,  GpioTermNone, GpioPadConfigUnlock}},// M.2 Key-E Slot  Wifi Wake-> UART_BT_WAKE_N
  {GPIO_VER4_S_GPP_B3,    {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutHigh,     GpioIntDefault,          GpioPlatformReset,  GpioTermNone}},// M.2 Key-E Slot  BT RF Kill
  {GPIO_VER4_S_GPP_B19,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutHigh,     GpioIntDefault,          GpioPlatformReset,  GpioTermNone}},// M.2 Key-E Slot  RF Kill
  {GPIO_VER4_S_GPP_G6,    {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutHigh,     GpioIntDefault,          GpioHostDeepReset,  GpioTermNone}},// M.2 Key-E slot for Wifi -> CNVI_EN_N
  {GPIO_VER4_S_GPP_H2,    {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirInInv,   GpioOutDefault,  GpioIntEdge|GpioIntSci,  GpioResumeReset,    GpioTermNone, GpioPadConfigUnlock}},//M.2 Key-E slot for Wifi Wake
  {GPIO_VER4_S_GPP_H17,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutHigh,     GpioIntDefault,          GpioPlatformReset,  GpioTermNone}},//M.2 Key-E slot for Wifi Reset

  {GPIO_VER4_S_GPP_K3,    {GpioPadModeGpio, GpioHostOwnGpio, GpioDirInInv,   GpioOutDefault,  GpioIntEdge,             GpioHostDeepReset,  GpioTermNone}},// Sx entry acknowledge
  {GPIO_VER4_S_GPP_K4,    {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutLow,      GpioIntDefault,          GpioHostDeepReset,  GpioTermNone}},// Sx entry request

  // M2_SSD
  {GPIO_VER4_S_GPP_K2,    {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,     GpioOutHigh,     GpioIntDefault,          GpioPlatformReset,  GpioTermNone}},//M.2 PCH/CPU SSD Full poweroff

  // PCIe SLOT_1
  {GPIO_VER4_S_GPP_F23,   {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirInInv,   GpioOutDefault,  GpioIntEdge|GpioIntSci,  GpioHostDeepReset,  GpioTermNone, GpioPadConfigUnlock}},//PCIe SLOT_1 Wake

  // PEG SLOT_1
  {GPIO_VER4_S_GPP_E6,    {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirInInv,   GpioOutDefault,  GpioIntEdge|GpioIntSci,  GpioHostDeepReset,  GpioTermNone, GpioPadConfigUnlock}},//PEG SLOT_1 Wake
  {GPIO_VER4_S_GPP_F7,    {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutDefault,  GpioIntDefault,          GpioPlatformReset,  GpioTermNone}},//PEG SLOT DGPU SEL
  {GPIO_VER4_S_GPP_F9,    {GpioPadModeGpio, GpioHostOwnGpio, GpioDirInInv,   GpioOutDefault,  GpioIntDefault,          GpioPlatformReset,  GpioTermNone}},//PEG SLOT_DGPU_PWR_OK
  {GPIO_VER4_S_GPP_G3,    {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//PEG_RTD3_COLD_MOD

  // SATA
  {GPIO_VER4_S_GPP_R11,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutHigh,     GpioIntDefault,          GpioPlatformReset,  GpioTermNone}},//SATA Direct power enable

  // Thunderbolt
  {GPIO_VER4_S_GPP_E4,    {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirInInv,   GpioOutDefault,  GpioIntEdge|GpioIntSci,  GpioHostDeepReset,  GpioTermNone, GpioPadConfigUnlock}},//POR Thunderbolt Wake , MIPI60 (Rework)
  {GPIO_VER4_S_GPP_F10,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirIn,      GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//POR PD AIC detect, MIPI60 (Rework)
  {GPIO_VER4_S_GPP_F16,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutHigh,     GpioIntDefault,          GpioHostDeepReset,   GpioTermNone}},//POR Thunderbolt Reset,MIPI60 (Rework)
  {GPIO_VER4_S_GPP_G0,    {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutHigh,     GpioIntDefault,          GpioResumeReset,    GpioTermNone}},//POR Thunderbolt FRC_PWR
  {GPIO_VER4_S_GPP_G7,    {GpioPadModeGpio, GpioHostOwnGpio, GpioDirInInv,   GpioOutDefault,  GpioIntEdge,             GpioResetDefault,   GpioTermNone}},//Thunderbolt Plug event
  {GPIO_VER4_S_GPP_K0,    {GpioPadModeGpio, GpioHostOwnGpio, GpioDirInInv,   GpioOutDefault,  GpioIntEdge,             GpioPlatformReset,  GpioTermNone}},//Thunderbolt P1 P2 Alert
  {GPIO_VER4_S_GPP_F2,    {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutLow,      GpioIntDefault,          GpioHostDeepReset,  GpioTermNone}},//EDP_MUX_SW

  // Type C
  {GPIO_VER4_S_GPP_B20,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirIn,      GpioOutDefault,  GpioIntEdge,             GpioResetDefault,   GpioTermNone}},//Type-C PD Detection
  {GPIO_VER4_S_GPP_F0,    {GpioPadModeGpio, GpioHostOwnGpio, GpioDirInInv,   GpioOutDefault,  GpioIntEdge,             GpioPlatformReset,  GpioTermNone}},//Type-C PSON OVERRIDE
  {GPIO_VER4_S_GPP_H1,    {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirInInv,   GpioOutDefault,  GpioIntEdge|GpioIntSci,  GpioPlatformReset,  GpioTermNone, GpioPadConfigUnlock}},//Type-C Alert

  // TPM module
  {GPIO_VER4_S_GPP_F17,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirInInv,   GpioOutDefault,  GpioIntEdge|GpioIntApic, GpioPlatformReset,  GpioTermNone, GpioPadConfigUnlock}},//TPM module PIRQ

  // Touch panel
  {GPIO_VER4_S_GPP_A14,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutHigh,     GpioIntDefault,          GpioResetDefault,   GpioTermNone}},// Touch Panel 2 Power Enable
  {GPIO_VER4_S_GPP_I5,    {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutHigh,     GpioIntDefault,          GpioPlatformReset,  GpioTermNone}},// Touch Panel 2 Reset
  {GPIO_VER4_S_GPP_I6,    {GpioPadModeGpio, GpioHostOwnGpio, GpioDirInInv,   GpioOutDefault,  GpioIntEdge|GpioIntApic, GpioPlatformReset,  GpioTermNone, GpioPadUnlock}},// Touch Panel 2 Interrupt
  {GPIO_VER4_S_GPP_D22,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutHigh,     GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//I2C1 Touch Panel 1 Reset
  {GPIO_VER4_S_GPP_D23,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirInInv,   GpioOutDefault,  GpioIntEdge|GpioIntApic, GpioResetDefault,   GpioTermNone, GpioPadUnlock}},//I2C1 Touch Panel 1 Interrupt
  {GPIO_VER4_S_GPP_R21,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutHigh,     GpioIntDefault,          GpioPlatformReset,  GpioTermNone}},//Touch panel 1 power enable

  //PCIe Aux Power Enable
  {GPIO_VER4_S_GPP_I0,    {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutHigh,     GpioIntDefault,          GpioResumeReset,    GpioTermNone}},//GPP_I_0_EXT_PWR_GATE_N

  //PLN
  {GPIO_VER4_S_GPP_H15,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutHigh,     GpioIntDefault,          GpioPlatformReset,  GpioTermNone}},//PCH SSD PLN Circuit Strap
  {GPIO_VER4_S_GPP_H23,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutDefault,  GpioIntDefault,          GpioPlatformReset,  GpioTermNone}},//PCH SSD PLN Circuit

  //Others
  {GPIO_VER4_S_GPP_K5,    {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//DNX_IN_PROG_AUX_NBIAS
  {GPIO_VER4_S_GPP_K1,    {GpioPadModeGpio, GpioHostOwnGpio, GpioDirIn,      GpioOutDefault,  GpioIntDefault,          GpioPlatformReset,  GpioTermNone}},//Platform SLP_S0
  {GPIO_VER4_S_GPP_H10,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirIn,      GpioOutDefault,  GpioIntEdge,             GpioResetDefault,   GpioTermNone}},//Power supply detect
  {GPIO_VER4_S_GPP_H0,    {GpioPadModeGpio, GpioHostOwnGpio, GpioDirDefault, GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//VAL_SV_ADVANCE_STRAP
  {GPIO_VER4_S_GPP_H14,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirInInv,   GpioOutDefault,  GpioIntEdge,             GpioPlatformReset,  GpioTermNone}},//VOL_UP
  {GPIO_VER4_S_GPP_E7,    {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirInInv,   GpioOutDefault,  GpioIntEdge|GpioIntSci,  GpioResetDefault,   GpioTermNone, GpioPadConfigUnlock}}, //SAR NIRQ PCH
  {GPIO_VER4_S_GPP_F1,    {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirInInv,   GpioOutDefault,  GpioIntEdge|GpioIntSci,  GpioPlatformReset,  GpioTermNone, GpioPadConfigUnlock}},//DG_I2C INT
  {GPIO_VER4_S_GPP_F12,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutHigh,     GpioIntDefault,          GpioPlatformReset,  GpioTermNone}},//VAL_EXTTS_SNI_DRV0_PCH
  {GPIO_VER4_S_GPP_F13,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutHigh,     GpioIntDefault,          GpioPlatformReset,  GpioTermNone}},//VAL_EXTTS_SNI_DRV1_PCH
  {GPIO_VER4_S_GPP_J8 ,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirIn,      GpioOutDefault,  GpioIntDefault,          GpioPlatformReset,  GpioTermNone}},//test menu
  {GPIO_VER4_S_GPP_J9 ,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirIn,      GpioOutDefault,  GpioIntDefault,          GpioPlatformReset,  GpioTermNone}},//SV Detect

  //Not used
  {GPIO_VER4_S_GPD0,      {GpioPadModeGpio, GpioHostOwnGpio, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPD7,      {GpioPadModeGpio, GpioHostOwnGpio, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPD12,     {GpioPadModeGpio, GpioHostOwnGpio, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPP_B22,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutHigh,     GpioIntDefault,          GpioPlatformReset,  GpioTermNone}},//TP6F4
  {GPIO_VER4_S_GPP_B23,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPP_D3,    {GpioPadModeGpio, GpioHostOwnGpio, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//GFX_DETECT_STRAP
  {GPIO_VER4_S_GPP_D11,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPP_D13,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPP_D14,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPP_F3,    {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutDefault,  GpioIntDefault,          GpioPlatformReset,  GpioTermNone}},//Not used, MIPI60(rework)
  {GPIO_VER4_S_GPP_H11,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutHigh,     GpioIntDefault,          GpioPlatformReset,  GpioTermNone}},//Not used
  {GPIO_VER4_S_GPP_H12,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ, strap
  {GPIO_VER4_S_GPP_H18,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ, strap


  {0x0}  // terminator

})}

#  mGpioTableAdlSAdpSSbgaDdr5SODimmAep
[PcdsDynamicExVpd.common.SkuIdAdlSAdpSSbgaDdr5SODimmAep]
gBoardModuleTokenSpaceGuid.VpdPcdBoardGpioTable|*|{CODE({
  // Camera Backlight
  {GPIO_VER4_S_GPP_B22,   {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,     GpioOutLow,      GpioIntDefault,          GpioResetDefault,   GpioTermNone}},

  // EC
  {GPIO_VER4_S_GPP_B14,   {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirIn,      GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}}, // EC top swap override
  {GPIO_VER4_S_GPP_B20,   {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirInInv,   GpioOutDefault,  GpioIntEdge,             GpioResetDefault,   GpioTermNone,  GpioPadConfigUnlock}}, // EC SMI
  {GPIO_VER4_S_GPP_K1,    {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,     GpioOutDefault,  GpioIntDefault,          GpioPlatformReset,  GpioTermNone}}, // MEC1521H SLP EC

  // SSD
  {GPIO_VER4_S_GPP_E5,    {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,     GpioOutHigh,     GpioIntDefault,          GpioResetDefault,   GpioTermNone}}, // CPU SSD power disable

  // Foxville LAN
  {GPIO_VER4_S_GPP_R20,   {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,     GpioOutLow,      GpioIntDefault,          GpioResumeReset,    GpioTermNone}}, // LAN Power enable, Set Output high based on the setup option.
  {GPIO_VER4_S_GPP_G1,    {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,     GpioOutHigh,     GpioIntDefault,          GpioPlatformReset,  GpioTermNone}}, // Foxville Reset
  {GPIO_VER4_S_GPP_B21,   {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,     GpioOutHigh,     GpioIntDefault,          GpioResumeReset,    GpioTermNone}}, // Lan Disable, low to enable ULP
  {GPIO_VER4_S_GPD2,      {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirInInv,   GpioOutDefault,  GpioIntEdge,             GpioHostDeepReset,  GpioTermNone,  GpioPadConfigUnlock}}, // FOX_WAKE_N. Set to SCI based on the setup option.

  // WLAN
  {GPIO_VER4_S_GPP_B2,    {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirInInv,   GpioOutDefault,  GpioIntEdge|GpioIntSci,  GpioHostDeepReset,  GpioTermNone,  GpioPadConfigUnlock}},// UART BT Wake
  {GPIO_VER4_S_GPP_B3,    {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutHigh,     GpioIntDefault,          GpioPlatformReset,  GpioTermNone}},// M.2 Key-E Slot  BT RF Kill
  {GPIO_VER4_S_GPP_B19,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutHigh,     GpioIntDefault,          GpioPlatformReset,  GpioTermNone}},// M.2 Key-E Slot  WiFi RF Kill
  {GPIO_VER4_S_GPP_H2,    {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirInInv,   GpioOutDefault,  GpioIntEdge|GpioIntSci,  GpioResumeReset,    GpioTermNone,  GpioPadConfigUnlock}},// M.2 Key-E slot for Wifi Wake
  {GPIO_VER4_S_GPP_H17,   {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,     GpioOutHigh,     GpioIntDefault,          GpioPlatformReset,  GpioTermNone}},// M.2 Key-E slot for Wifi Reset

  //PLN
  {GPIO_VER4_S_GPP_H15,   {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,     GpioOutHigh,     GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//PCH SSD PLN Circuit Strap
  {GPIO_VER4_S_GPP_H23,   {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,     GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//PCH SSD PLN Circuit Delay selection

  // Thunderbolt
  {GPIO_VER4_S_GPP_B4,    {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirIn,      GpioOutDefault,  GpioIntDefault,          GpioPlatformReset,  GpioTermNone,  GpioPadConfigUnlock}},// PD AIC presence detect
  {GPIO_VER4_S_GPP_F3,    {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,     GpioOutHigh,     GpioIntDefault,          GpioPlatformReset,  GpioTermNone}},//POR Thunderbolt USB Force Power
  {GPIO_VER4_S_GPP_F9,    {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirInInv,   GpioOutDefault,  GpioIntEdge|GpioIntSci,  GpioHostDeepReset,  GpioTermNone,  GpioPadConfigUnlock}},//POR Thunderbolt Wake
  {GPIO_VER4_S_GPP_F10,   {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirIn,      GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//POR PD AIC detect,
  {GPIO_VER4_S_GPP_F16,   {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,     GpioOutHigh,     GpioIntDefault,          GpioHostDeepReset,  GpioTermNone}},//POR Thunderbolt Reset,
  {GPIO_VER4_S_GPP_G0,    {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,     GpioOutLow,      GpioIntDefault,          GpioResumeReset,    GpioTermNone}},//POR Thunderbolt 0 and 1 FRC_PWR
  {GPIO_VER4_S_GPP_G7,    {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirInInv,   GpioOutDefault,  GpioIntEdge,             GpioResetDefault,   GpioTermNone}},//Thunderbolt Plug event
  {GPIO_VER4_S_GPP_K0,    {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirInInv,   GpioOutDefault,  GpioIntEdge,             GpioPlatformReset,  GpioTermNone}},//Thunderbolt 0 AIC interrupt (PD alert)

  // Touch pad
  {GPIO_VER4_S_GPP_F22,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirInInv,   GpioOutDefault,  GpioIntEdge,             GpioResetDefault,   GpioTermNone,  GpioPadConfigUnlock}},//Touch pad interrupt
  {GPIO_VER4_S_GPP_E3,    {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,     GpioOutHigh,     GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//Touch pad power enable

  // PEG
  {GPIO_VER4_S_GPP_E2,    {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,     GpioOutHigh,     GpioIntDefault,          GpioPlatformReset,  GpioTermNone}},//PEG Slot Reset
  {GPIO_VER4_S_GPP_E4,    {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirIn,      GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//PEG SLOT DGPU power status
  {GPIO_VER4_S_GPP_E6,    {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirInInv,   GpioOutDefault,  GpioIntEdge|GpioIntSci,  GpioHostDeepReset,  GpioTermNone,  GpioPadConfigUnlock}},//PEG SLOT_1 Wake

  // Others
  {GPIO_VER4_S_GPP_D3,    {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirIn,      GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},// GFX_DETECT_STRAP
  {GPIO_VER4_S_GPP_H0,    {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirIn,      GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},// VAL_SV_ADVANCE_STRAP
  {GPIO_VER4_S_GPP_J8 ,   {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirIn,      GpioOutDefault,  GpioIntDefault,          GpioPlatformReset,  GpioTermNone}},// Validation test setup menu
  {GPIO_VER4_S_GPP_J9 ,   {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirIn,      GpioOutDefault,  GpioIntDefault,          GpioPlatformReset,  GpioTermNone}},// Validation SV detect
  {GPIO_VER4_S_GPP_K4 ,   {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirIn,      GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},// SKTOCC

  // Test Points
  {GPIO_VER4_S_GPP_D13,   {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPP_F0,    {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPP_F1,    {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPP_F2,    {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPP_F4,    {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPP_F6,    {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPP_F7,    {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPP_F11,   {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPP_F12,   {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPP_F13,   {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPP_F14,   {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPP_F15,   {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPP_F17,   {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPP_H8,    {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPP_H9,    {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPP_H18,   {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPP_J8,    {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPD12,     {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ

  //Not used
  {GPIO_VER4_S_GPP_A7,    {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPP_A8,    {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPP_A9,    {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPP_A11,   {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPP_A12,   {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPP_A13,   {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPP_A14,   {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPP_B0,    {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPP_B1,    {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPP_B6,    {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPP_B7,    {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPP_B8,    {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPP_B9,    {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPP_B10,   {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPP_B11,   {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPP_B15,   {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPP_B16,   {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPP_B17,   {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPP_B23,   {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPP_C8,    {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPP_C9,    {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPP_C11,   {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPP_C12,   {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPP_C13,   {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPP_C14,   {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPP_C15,   {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPP_C18,   {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPP_C19,   {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPP_C22,   {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPP_C23,   {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPP_D1,    {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPP_D2,    {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPP_D7,    {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPP_D8,    {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPP_D11,   {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPP_E0,    {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPP_E7,    {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPP_E8,    {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPP_E12,   {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPP_E13,   {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPP_E14,   {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPP_E15,   {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPP_E16,   {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPP_E17,   {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPP_E18,   {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPP_E19,   {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPP_E20,   {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPP_E21,   {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPP_G2,    {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPP_G3,    {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPP_G5,    {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPP_G6,    {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPP_H1,    {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPP_H12,   {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPP_H13,   {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPP_H14,   {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPP_H19,   {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPP_H20,   {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPP_H21,   {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPP_H22,   {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPP_I0,    {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPP_I1,    {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPP_I2,    {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPP_I3,    {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPP_I4,    {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPP_I5,    {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPP_I6,    {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPP_I8,    {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPP_I9,    {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPP_I10,   {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPP_I11,   {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPP_I12,   {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPP_I15,   {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPP_I16,   {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPP_I17,   {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPP_I19,   {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPP_I20,   {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPP_I21,   {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPP_J10,   {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPP_J11,   {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPP_K5,    {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPP_R4,    {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPP_R5,    {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPP_R6,    {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPP_R7,    {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPP_R8,    {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPP_R10,   {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPP_R11,   {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPP_R12,   {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPP_R13,   {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPP_R14,   {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPP_R15,   {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPP_R16,   {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPP_R17,   {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPP_R18,   {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPP_R19,   {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPP_R21,   {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPP_S0,    {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPP_S1,    {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPP_S4,    {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPP_S5,    {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPD7,      {GpioPadModeGpio, GpioHostOwnGpio, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ
  {GPIO_VER4_S_GPD11,     {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirNone,    GpioOutDefault,  GpioIntDefault,          GpioResetDefault,   GpioTermNone}},//HiZ

  {0x0}  // terminator

})}