## @file
#  GPIO definition table for AlderLake M
#
# @copyright
#  INTEL CONFIDENTIAL
#  Copyright 2020 - 2021 Intel Corporation.
#
#  The source code contained or described herein and all documents related to the
#  source code ("Material") are owned by Intel Corporation or its suppliers or
#  licensors. Title to the Material remains with Intel Corporation or its suppliers
#  and licensors. The Material may contain trade secrets and proprietary and
#  confidential information of Intel Corporation and its suppliers and licensors,
#  and is protected by worldwide copyright and trade secret laws and treaty
#  provisions. No part of the Material may be used, copied, reproduced, modified,
#  published, uploaded, posted, transmitted, distributed, or disclosed in any way
#  without Intel's prior express written permission.
#
#  No license under any patent, copyright, trade secret or other intellectual
#  property right is granted to or conferred upon you by disclosure or delivery
#  of the Materials, either expressly, by implication, inducement, estoppel or
#  otherwise. Any license under such intellectual property rights must be
#  express and approved by Intel in writing.
#
#  Unless otherwise agreed by Intel in writing, you may not remove or alter
#  this notice or any other notice embedded in Materials by Intel or
#  Intel's suppliers or licensors in any way.
#
#  This file contains a 'Sample Driver' and is licensed as such under the terms
#  of your license agreement with Intel or your vendor. This file may be modified
#  by the user, subject to the additional terms of the license agreement.
#
# @par Specification
##

###
### !!! GPIOs designated to Native Functions shall not be configured by Platform Code.
### Native Pins shall be configured by Silicon Code (based on BIOS policies setting) or soft straps(set by CSME in FITc).
###
###

#mGpioTableAdlMLp4Rvp
[PcdsDynamicExVpd.common.SkuIdAdlMLp4Rvp]
gBoardModuleTokenSpaceGuid.VpdPcdBoardGpioTable|*|{CODE({

  // CPU M.2 SSD
  {GPIO_VER2_LP_GPP_H13, {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutHigh,   GpioIntDis,GpioPlatformReset,  GpioTermNone}},  //M2_CPU_SSD1_RESET_N

  // M.2 Key-E - WLAN/BT
  {GPIO_VER2_LP_GPP_A12, {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,    GpioOutHigh,     GpioIntDis,              GpioResumeReset,    GpioTermNone}},  //BT_RF_KILL_N
  {GPIO_VER2_LP_GPP_D12, {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,    GpioOutHigh,     GpioIntDis,              GpioResumeReset,    GpioTermNone}},  //WIFI_RF_KILL_N
  {GPIO_VER2_LP_GPP_H2,  {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,    GpioOutHigh,     GpioIntDis,              GpioResumeReset,    GpioTermNone}},  //WLAN_RST_N
  {GPIO_VER2_LP_GPP_D13, {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirInInv,  GpioOutDefault,  GpioIntLevel|GpioIntSci, GpioHostDeepReset,  GpioTermNone,  GpioPadConfigUnlock  }},  //WIFI_WAKE_N
  {GPIO_VER2_LP_GPP_E0,  {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirInInv,  GpioOutDefault,  GpioIntLevel|GpioIntSci, GpioHostDeepReset,  GpioTermNone,  GpioPadConfigUnlock  }},  //UART_BT_WAKE_N

  // x1 PCIe DT Slot
  {GPIO_VER2_LP_GPP_F10, {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,    GpioOutHigh,     GpioIntDis,               GpioPlatformReset,  GpioTermNone}},  //X1_Slot_RESET
  {GPIO_VER2_LP_GPP_D17, {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirInInv,  GpioOutDefault,  GpioIntLevel|GpioIntSci,  GpioHostDeepReset,  GpioTermNone,  GpioPadConfigUnlock  }},  //X1_PCIE_SLOT_WAKE_N

  // TBT Re-Timers
  //{GPIO_VER2_LP_GPP_E4, {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutLow,   GpioIntDis,GpioPlatformReset,  GpioTermNone}},  //TC_RETIMER_FORCE_PWR


  // PCH M.2 SSD
  {GPIO_VER2_LP_GPP_D10, {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutHigh,   GpioIntDis,GpioPlatformReset,  GpioTermNone}},  //M2_PCH_SSD_PWREN
  {GPIO_VER2_LP_GPP_H0,  {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutHigh,   GpioIntDis,GpioPlatformReset,  GpioTermNone}},  //M2_SSD_RST_N

  // Camera Conn1
  {GPIO_VER2_LP_GPP_B23, {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutHigh,   GpioIntDis,GpioPlatformReset,  GpioTermNone}}, //CRD1_PWREN_CVF_IRQ,
  {GPIO_VER2_LP_GPP_R5,  {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutHigh,   GpioIntDis,GpioPlatformReset,  GpioTermNone}},  //CRD1_RST_N_CVF_RST_N,
  {GPIO_VER2_LP_GPP_C2,  {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutHigh,   GpioIntDis,GpioPlatformReset,  GpioTermNone}},  //CRD1_CLK_EN - same pin for both CAM Conn1&2

  //Camera Conn2
  {GPIO_VER2_LP_GPP_E16, {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutLow,   GpioIntDis,GpioPlatformReset,  GpioTermDefault}},  //CRD2_PWREN
  {GPIO_VER2_LP_GPP_E15, {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutHigh,   GpioIntDis,GpioPlatformReset,  GpioTermDefault}}, //CRD2_RST_N

  // Camera Common GPIO's for all Camera, Rework Options
  {GPIO_VER2_LP_GPP_B18, {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutLow,   GpioIntDis,GpioPlatformReset,  GpioTermNone}},  //CRD_CAM_STROBE
  {GPIO_VER2_LP_GPP_B14, {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutDefault,   GpioIntDis,GpioPlatformReset,  GpioTermNone}},  //PRIVACY_LED

   // Audio
  {GPIO_VER2_LP_GPP_H3,  {GpioPadModeGpio, GpioHostOwnGpio, GpioDirInInv,  GpioOutDefault,GpioIntLevel|GpioIntApic,GpioPlatformReset,  GpioTermNone,  GpioPadConfigUnlock  }},  //CODEC_INT_N
  {GPIO_VER2_LP_GPP_A13, {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutHigh,   GpioIntDis,GpioPlatformReset,  GpioTermNone}},  //AUDIO_PWREN

  // Touch Pad
  {GPIO_VER2_LP_GPP_H1, {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutHigh,   GpioIntDis,GpioPlatformReset,  GpioTermNone}},  //TCH_PAD_TCH_PNL2_LS_EN
  {GPIO_VER2_LP_GPP_D11,{GpioPadModeGpio, GpioHostOwnGpio, GpioDirInInv,  GpioOutDefault,GpioIntEdge|GpioIntApic,GpioPlatformReset,  GpioTermNone,  GpioPadConfigUnlock }},  //TCH_PAD_INT_N,

  // Common Power Pin for I2c and SPI Touch
  {GPIO_VER2_LP_GPP_F7,    {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutHigh,     GpioIntDis,  GpioPlatformReset,  GpioTermNone}},     //TCH_PNL_PWR_EN Con1/2

  {GPIO_VER2_LP_GPP_E6,    {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutHigh,     GpioIntDis,  GpioPlatformReset,  GpioTermDefault}},  //THC0_SPI1_RST_N
  {GPIO_VER2_LP_GPP_E17,   {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirIn,   GpioOutDefault,  GpioIntDis,  GpioPlatformReset,  GpioTermDefault}},  //THC0_SPI1_INTB

  {GPIO_VER2_LP_GPP_F17,  {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,    GpioOutHigh,    GpioIntDis,GpioPlatformReset,  GpioTermNone}},  //TCH_PNL2_RST_N
  {GPIO_VER2_LP_GPP_F18,  {GpioPadModeGpio, GpioHostOwnGpio, GpioDirInInv,  GpioOutDefault, GpioIntEdge|GpioIntApic,GpioPlatformReset,  GpioTermNone,  GpioPadConfigUnlock  }},  //THC1_SPI2_INT_N_TCH_PNL2

  // EC
  {GPIO_VER2_LP_GPP_E7, {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirInInv,  GpioOutDefault,GpioIntLevel|GpioIntSmi,GpioPlatformReset,  GpioTermNone,  GpioPadConfigUnlock  }},  //GPPC_E7_EC_SMI_N
  {GPIO_VER2_LP_GPP_A7, {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutHigh,   GpioIntDis,GpioPlatformReset,  GpioTermNone}}, //EC_SLP_S0_CS_N

  //SPI TPM HDR
  {GPIO_VER2_LP_GPP_E3, {GpioPadModeGpio, GpioHostOwnGpio, GpioDirInInv,  GpioOutDefault,GpioIntLevel|GpioIntApic,GpioHostDeepReset,  GpioTermWpu20K,  GpioPadConfigUnlock  }},  //SPI_TPM_INT_N

  //Display - for use by VPG validation team
  {GPIO_VER2_LP_GPP_A11, {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutHigh,   GpioIntDis,GpioPlatformReset,  GpioTermNone}},  //GPPC_A11_HPD1_VAL

  //LID SWITCH
  {GPIO_VER2_LP_GPD2,    {GpioPadModeGpio,  GpioHostOwnAcpi,  GpioDirInInv,  GpioOutDefault,  GpioIntLevel|GpioIntSci,   GpioHostDeepReset,  GpioTermNone,  GpioPadConfigUnlock  }}, //SMC_WAKE_SCI_N


  {0x0}  // terminator
})}

#mGpioTableAdlMLp5Rvp
[PcdsDynamicExVpd.common.SkuIdAdlMLp5Rvp]
gBoardModuleTokenSpaceGuid.VpdPcdBoardGpioTable|*|{CODE({

  // CPU M.2 SSD
  {GPIO_VER2_LP_GPP_H13, {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutHigh,   GpioIntDis,GpioPlatformReset,  GpioTermNone}},  //M2_CPU_SSD1_RESET_N

  // M.2 Key-E - WLAN/BT
  {GPIO_VER2_LP_GPP_A12, {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,    GpioOutHigh,     GpioIntDis,              GpioResumeReset,    GpioTermNone}},  //BT_RF_KILL_N
  {GPIO_VER2_LP_GPP_D12, {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,    GpioOutHigh,     GpioIntDis,              GpioResumeReset,    GpioTermNone}},  //WIFI_RF_KILL_N
  {GPIO_VER2_LP_GPP_H2,  {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,    GpioOutHigh,     GpioIntDis,              GpioResumeReset,    GpioTermNone}},  //WLAN_RST_N
  {GPIO_VER2_LP_GPP_D13, {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirInInv,  GpioOutDefault,  GpioIntLevel|GpioIntSci, GpioHostDeepReset,  GpioTermNone,  GpioPadConfigUnlock  }},  //WIFI_WAKE_N
  {GPIO_VER2_LP_GPP_E0,  {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirInInv,  GpioOutDefault,  GpioIntLevel|GpioIntSci, GpioHostDeepReset,  GpioTermNone,  GpioPadConfigUnlock  }},  //UART_BT_WAKE_N

  // x1 PCIe DT Slot
  {GPIO_VER2_LP_GPP_F10, {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,    GpioOutHigh,     GpioIntDis,               GpioPlatformReset,  GpioTermNone}},  //X1_Slot_RESET
  {GPIO_VER2_LP_GPP_D17, {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirInInv,  GpioOutDefault,  GpioIntLevel|GpioIntSci,  GpioHostDeepReset,  GpioTermNone,  GpioPadConfigUnlock  }},  //X1_PCIE_SLOT_WAKE_N

  // PCH M.2 SSD
  {GPIO_VER2_LP_GPP_D10, {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutHigh,   GpioIntDis, GpioPlatformReset,  GpioTermNone}},  //M2_PCH_SSD_PWREN
  {GPIO_VER2_LP_GPP_H0,  {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutHigh,   GpioIntDis, GpioPlatformReset,  GpioTermNone}},  //M2_SSD_RST_N

  // Camera Conn1
  {GPIO_VER2_LP_GPP_B23, {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutHigh,   GpioIntDis, GpioPlatformReset,  GpioTermNone}}, //CRD1_PWREN_CVF_IRQ,
  {GPIO_VER2_LP_GPP_R5,  {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutHigh,   GpioIntDis, GpioPlatformReset,  GpioTermNone}},  //CRD1_RST_N_CVF_RST_N,
  {GPIO_VER2_LP_GPP_A7,  {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutHigh,   GpioIntDis, GpioPlatformReset,  GpioTermNone}},  //CRD1_CLK_EN- same pin for both CAM Conn1&2

  //Camera Conn2
  {GPIO_VER2_LP_GPP_E16, {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutLow,   GpioIntDis, GpioPlatformReset,  GpioTermDefault}},  //CRD2_PWREN
  {GPIO_VER2_LP_GPP_E15, {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutHigh,   GpioIntDis, GpioPlatformReset,  GpioTermDefault}}, //CRD2_RST_N

  // Camera Common GPIO's for all Camera, Rework Options
  {GPIO_VER2_LP_GPP_B18, {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutLow,   GpioIntDis, GpioPlatformReset,  GpioTermNone}},  //CRD_CAM_STROBE
  {GPIO_VER2_LP_GPP_B14, {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutDefault,   GpioIntDis,GpioPlatformReset,  GpioTermNone}},  //PRIVACY_LED

   // Audio
  {GPIO_VER2_LP_GPP_H3,  {GpioPadModeGpio, GpioHostOwnGpio, GpioDirInInv,  GpioOutDefault, GpioIntLevel|GpioIntApic, GpioPlatformReset,  GpioTermNone,  GpioPadConfigUnlock  }},  //CODEC_INT_N
  {GPIO_VER2_LP_GPP_A13, {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,    GpioOutHigh,    GpioIntDis,               GpioPlatformReset,  GpioTermNone}},  //AUDIO_PWREN

  // Touch Pad
  {GPIO_VER2_LP_GPP_H1,  {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,    GpioOutHigh,    GpioIntDis,               GpioPlatformReset,  GpioTermNone}},  //TCH_PAD_TCH_PNL2_LS_EN
  {GPIO_VER2_LP_GPP_D11, {GpioPadModeGpio, GpioHostOwnGpio, GpioDirInInv,  GpioOutDefault, GpioIntEdge|GpioIntApic,  GpioPlatformReset,  GpioTermNone,  GpioPadConfigUnlock }},  //TCH_PAD_INT_N,

  // Common Power Pin for I2c and SPI Touch
  {GPIO_VER2_LP_GPP_F7,    {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutHigh,     GpioIntDis,  GpioPlatformReset,  GpioTermNone}},     //TCH_PNL_PWR_EN Con1/2

  // EC
  {GPIO_VER2_LP_GPP_E7,  {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirInInv, GpioOutDefault, GpioIntLevel|GpioIntSmi, GpioPlatformReset,  GpioTermNone,  GpioPadConfigUnlock  }},  //GPPC_E7_EC_SMI_N
  {GPIO_VER2_LP_GPP_C2,  {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,   GpioOutHigh,    GpioIntDis, GpioPlatformReset,  GpioTermNone}},  //EC_SLP_S0_CS_N

  //SPI TPM HDR
  {GPIO_VER2_LP_GPP_E3, {GpioPadModeGpio, GpioHostOwnGpio, GpioDirInInv,  GpioOutDefault, GpioIntLevel|GpioIntApic, GpioHostDeepReset,  GpioTermWpu20K,  GpioPadConfigUnlock  }},  //SPI_TPM_INT_N

  //Display
  {GPIO_VER2_LP_GPP_A11, {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirIn,  GpioOutDefault, GpioIntDis, GpioResetDefault,  GpioTermNone}},  //DDIB_HPD_DP_DEBUG

  //LID switch
  {GPIO_VER2_LP_GPD2,    {GpioPadModeGpio,  GpioHostOwnAcpi,  GpioDirInInv,  GpioOutDefault,  GpioIntLevel|GpioIntSci,   GpioHostDeepReset,  GpioTermNone,  GpioPadConfigUnlock  }}, //SMC_WAKE_SCI_N

  {0x0}  // terminator
})}

#mGpioTableAdlMLp5PmicRvp
[PcdsDynamicExVpd.common.SkuIdAdlMLp5PmicRvp]
gBoardModuleTokenSpaceGuid.VpdPcdBoardGpioTable|*|{CODE({

  // CPU M.2 SSD
  {GPIO_VER2_LP_GPP_H13, {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutHigh,   GpioIntDis,GpioPlatformReset,  GpioTermNone}},  //M2_CPU_SSD1_RESET_N

  // M.2 Key-E - WLAN/BT
  {GPIO_VER2_LP_GPP_A12, {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,    GpioOutHigh,     GpioIntDis,              GpioResumeReset,    GpioTermNone}},  //BT_RF_KILL_N
  {GPIO_VER2_LP_GPP_D12, {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,    GpioOutHigh,     GpioIntDis,              GpioResumeReset,    GpioTermNone}},  //WIFI_RF_KILL_N
  {GPIO_VER2_LP_GPP_H2,  {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,    GpioOutHigh,     GpioIntDis,              GpioResumeReset,    GpioTermNone}},  //WLAN_RST_N
  {GPIO_VER2_LP_GPP_D13, {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirInInv,  GpioOutDefault,  GpioIntLevel|GpioIntSci, GpioHostDeepReset,  GpioTermNone,  GpioPadConfigUnlock  }},  //WIFI_WAKE_N
  {GPIO_VER2_LP_GPP_E0,  {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirInInv,  GpioOutDefault,  GpioIntLevel|GpioIntSci, GpioHostDeepReset,  GpioTermNone,  GpioPadConfigUnlock  }},  //UART_BT_WAKE_N

  // x1 PCIe DT Slot
  {GPIO_VER2_LP_GPP_F10, {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,    GpioOutHigh,     GpioIntDis,               GpioPlatformReset,  GpioTermNone}},  //X1_Slot_RESET
  {GPIO_VER2_LP_GPP_D17, {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirInInv,  GpioOutDefault,  GpioIntLevel|GpioIntSci,  GpioHostDeepReset,  GpioTermNone,  GpioPadConfigUnlock  }},  //X1_PCIE_SLOT_WAKE_N

  //x1_SLOT
  {GPIO_VER2_LP_GPP_H19, {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutHigh,   GpioIntDis, GpioPlatformReset,  GpioTermDefault}},  //x1_SLOT/WWAN

  // PCH M.2 SSD
  {GPIO_VER2_LP_GPP_D10, {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutHigh,   GpioIntDis, GpioPlatformReset,  GpioTermNone}},  //M2_PCH_SSD_PWREN
  {GPIO_VER2_LP_GPP_H0,  {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutHigh,   GpioIntDis, GpioPlatformReset,  GpioTermNone}},  //M2_SSD_RST_N

  // Camera Conn1
  {GPIO_VER2_LP_GPP_B23, {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutHigh,   GpioIntDis, GpioPlatformReset,  GpioTermNone}}, //CRD1_PWREN_CVF_IRQ,
  {GPIO_VER2_LP_GPP_R5,  {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutHigh,   GpioIntDis, GpioPlatformReset,  GpioTermNone}},  //CRD1_RST_N_CVF_RST_N,
  {GPIO_VER2_LP_GPP_A7,  {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutHigh,   GpioIntDis, GpioPlatformReset,  GpioTermNone}},  //CRD1_CLK_EN- same pin for both CAM Conn1&2

  //Camera Conn2
  {GPIO_VER2_LP_GPP_E16, {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutLow,   GpioIntDis, GpioPlatformReset,  GpioTermDefault}},  //CRD2_PWREN
  {GPIO_VER2_LP_GPP_E15, {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutHigh,   GpioIntDis, GpioPlatformReset,  GpioTermDefault}}, //CRD2_RST_N

  // Camera Common GPIO's for all Camera, Rework Options
  {GPIO_VER2_LP_GPP_B18, {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutLow,   GpioIntDis, GpioPlatformReset,  GpioTermNone}},  //CRD_CAM_STROBE
  {GPIO_VER2_LP_GPP_B14, {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutDefault,   GpioIntDis,GpioPlatformReset,  GpioTermNone}},  //PRIVACY_LED

   // Audio
  {GPIO_VER2_LP_GPP_H3,  {GpioPadModeGpio, GpioHostOwnGpio, GpioDirInInv,  GpioOutDefault, GpioIntLevel|GpioIntApic, GpioPlatformReset,  GpioTermNone,  GpioPadConfigUnlock  }},  //CODEC_INT_N

  // Touch Pad
  {GPIO_VER2_LP_GPP_H1,  {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,    GpioOutHigh,    GpioIntDis,               GpioPlatformReset,  GpioTermNone}},  //TCH_PAD_TCH_PNL2_LS_EN
  {GPIO_VER2_LP_GPP_D11, {GpioPadModeGpio, GpioHostOwnGpio, GpioDirInInv,  GpioOutDefault, GpioIntEdge|GpioIntApic,  GpioPlatformReset,  GpioTermNone,  GpioPadConfigUnlock }},  //TCH_PAD_INT_N,

  // Common Power Pin for I2c and SPI Touch
  {GPIO_VER2_LP_GPP_F7,    {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutHigh,     GpioIntDis,  GpioPlatformReset,  GpioTermNone}},     //TCH_PNL_PWR_EN Con1/2

  // EC
  {GPIO_VER2_LP_GPP_E7,  {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirInInv, GpioOutDefault, GpioIntLevel|GpioIntSmi, GpioPlatformReset,  GpioTermNone,  GpioPadConfigUnlock  }},  //GPPC_E7_EC_SMI_N
  {GPIO_VER2_LP_GPP_C2,  {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,   GpioOutHigh,    GpioIntDis, GpioPlatformReset,  GpioTermNone}},  //EC_SLP_S0_CS_N

  //SPI TPM HDR
  {GPIO_VER2_LP_GPP_E3, {GpioPadModeGpio, GpioHostOwnGpio, GpioDirInInv,  GpioOutDefault, GpioIntLevel|GpioIntApic, GpioHostDeepReset,  GpioTermWpu20K,  GpioPadConfigUnlock  }},  //SPI_TPM_INT_N

  //LID switch
  {GPIO_VER2_LP_GPD2,    {GpioPadModeGpio,  GpioHostOwnAcpi,  GpioDirInInv,  GpioOutDefault,  GpioIntLevel|GpioIntSci,   GpioHostDeepReset,  GpioTermNone,  GpioPadConfigUnlock  }}, //SMC_WAKE_SCI_N
  {0x0}  // terminator
})}

#mGpioTableAdlMLp5Aep
[PcdsDynamicExVpd.common.SkuIdAdlMLp5Aep]
gBoardModuleTokenSpaceGuid.VpdPcdBoardGpioTable|*|{CODE({

  // CPU M.2 SSD
  {GPIO_VER2_LP_GPP_H13, {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,   GpioOutHigh,    GpioIntDis,              GpioPlatformReset, GpioTermNone}},  //M2_CPU_SSD1_RESET_N

  // M.2 Key-E - WLAN/BT
  {GPIO_VER2_LP_GPP_A12, {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,   GpioOutHigh,    GpioIntDis,              GpioResumeReset,   GpioTermNone}},  //BT_RF_KILL_N
  {GPIO_VER2_LP_GPP_D12, {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,   GpioOutHigh,    GpioIntDis,              GpioResumeReset,   GpioTermNone}},  //WIFI_RF_KILL_N

  // Battery Charger Vmin to PCH PROCHOT
  {GPIO_VER2_LP_GPP_B2,  {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirInInv, GpioOutDefault, GpioIntEdge|GpioIntSci,  GpioPlatformReset, GpioTermNone, GpioPadConfigUnlock  }},  //BC_PROCHOT_N

  // Camera Conn1
  {GPIO_VER2_LP_GPP_B23, {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,   GpioOutHigh,    GpioIntDis,              GpioPlatformReset, GpioTermNone}}, //CRD1_PWREN_CVF_IRQ,
  {GPIO_VER2_LP_GPP_R5,  {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,   GpioOutHigh,    GpioIntDis,              GpioPlatformReset, GpioTermNone}},  //CRD1_RST_N_CVF_RST_N,

  // Camera Common GPIO's for all Camera, Rework Options
  {GPIO_VER2_LP_GPP_B18, {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,   GpioOutLow,     GpioIntDis,              GpioPlatformReset, GpioTermNone}},  //CRD_CAM_STROBE

  // Touch Pad
  {GPIO_VER2_LP_GPP_D11, {GpioPadModeGpio, GpioHostOwnGpio, GpioDirInInv, GpioOutDefault, GpioIntEdge|GpioIntApic, GpioPlatformReset, GpioTermNone,  GpioPadConfigUnlock }},  //TCH_PAD_INT_N,

  // Common Power Pin for I2c and SPI Touch
  {GPIO_VER2_LP_GPP_F7,    {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutHigh,     GpioIntDis,  GpioPlatformReset,  GpioTermNone}},     //TCH_PNL_PWR_EN Con1/2

  // EC
  {GPIO_VER2_LP_GPP_E7,  {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirInInv, GpioOutDefault, GpioIntLevel|GpioIntSmi, GpioPlatformReset, GpioTermNone, GpioPadConfigUnlock  }},  //GPPC_E7_EC_SMI_LP_N
  {GPIO_VER2_LP_GPP_A7,  {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,   GpioOutHigh,    GpioIntDis,              GpioPlatformReset, GpioTermNone}}, //EC_SLP_S0ix_N

  //SPI TPM HDR
  {GPIO_VER2_LP_GPP_E3,  {GpioPadModeGpio, GpioHostOwnGpio, GpioDirInInv, GpioOutDefault, GpioIntLevel|GpioIntApic, GpioHostDeepReset, GpioTermWpu20K,  GpioPadConfigUnlock  }},  //SPI_TPM_INT_N

  {0x0}  // terminator
})}

