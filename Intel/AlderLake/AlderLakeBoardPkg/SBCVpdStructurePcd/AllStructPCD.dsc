## @file
#  Include All Board Gpio configuration file.
#
# @copyright
#  INTEL CONFIDENTIAL
#  Copyright 2020 - 2021 Intel Corporation.
#
#  The source code contained or described herein and all documents related to the
#  source code ("Material") are owned by Intel Corporation or its suppliers or
#  licensors. Title to the Material remains with Intel Corporation or its suppliers
#  and licensors. The Material may contain trade secrets and proprietary and
#  confidential information of Intel Corporation and its suppliers and licensors,
#  and is protected by worldwide copyright and trade secret laws and treaty
#  provisions. No part of the Material may be used, copied, reproduced, modified,
#  published, uploaded, posted, transmitted, distributed, or disclosed in any way
#  without Intel's prior express written permission.
#
#  No license under any patent, copyright, trade secret or other intellectual
#  property right is granted to or conferred upon you by disclosure or delivery
#  of the Materials, either expressly, by implication, inducement, estoppel or
#  otherwise. Any license under such intellectual property rights must be
#  express and approved by Intel in writing.
#
#  Unless otherwise agreed by Intel in writing, you may not remove or alter
#  this notice or any other notice embedded in Materials by Intel or
#  Intel's suppliers or licensors in any way.
#
#  This file contains a 'Sample Driver' and is licensed as such under the terms
#  of your license agreement with Intel or your vendor. This file may be modified
#  by the user, subject to the additional terms of the license agreement.
#
# @par Specification
##
!if gSiPkgTokenSpaceGuid.PcdAdlLpSupport == FALSE
!include $(PLATFORM_BOARD_PACKAGE)/SBCVpdStructurePcd/GpioTableAdlSPostMem.dsc
!include $(PLATFORM_BOARD_PACKAGE)/SBCVpdStructurePcd/GpioTableAdlSPreMem.dsc
!else
!include $(PLATFORM_BOARD_PACKAGE)/SBCVpdStructurePcd/GpioTableAdlSimicsPostMem.dsc
!include $(PLATFORM_BOARD_PACKAGE)/SBCVpdStructurePcd/GpioTableAdlSimicsPreMem.dsc
!include $(PLATFORM_BOARD_PACKAGE)/SBCVpdStructurePcd/GpioTableAdlPPostMem.dsc
!include $(PLATFORM_BOARD_PACKAGE)/SBCVpdStructurePcd/GpioTableAdlPPreMem.dsc
!include $(PLATFORM_BOARD_PACKAGE)/SBCVpdStructurePcd/GpioTableAdlMPostMem.dsc
!include $(PLATFORM_BOARD_PACKAGE)/SBCVpdStructurePcd/GpioTableAdlMPreMem.dsc
!endif


# PCIe clock mapping
!if gSiPkgTokenSpaceGuid.PcdAdlLpSupport == FALSE
!include $(PLATFORM_BOARD_PACKAGE)/SBCVpdStructurePcd/PcieClocks/AdlSPcieClocks.dsc
!else
!include $(PLATFORM_BOARD_PACKAGE)/SBCVpdStructurePcd/PcieClocks/AdlPPcieClocks.dsc
!include $(PLATFORM_BOARD_PACKAGE)/SBCVpdStructurePcd/PcieClocks/AdlMPcieClocks.dsc
!include $(PLATFORM_BOARD_PACKAGE)/SBCVpdStructurePcd/PcieClocks/AdlSimicsPcieClocks.dsc
!endif

# USB OC mapping
!if gSiPkgTokenSpaceGuid.PcdAdlLpSupport == FALSE
!include $(PLATFORM_BOARD_PACKAGE)/SBCVpdStructurePcd/UsbOc/AdlSUsbOC.dsc
!else
!include $(PLATFORM_BOARD_PACKAGE)/SBCVpdStructurePcd/UsbOc/AdlSimicsUsbOC.dsc
!include $(PLATFORM_BOARD_PACKAGE)/SBCVpdStructurePcd/UsbOc/AdlPUsbOC.dsc
!include $(PLATFORM_BOARD_PACKAGE)/SBCVpdStructurePcd/UsbOc/AdlMUsbOC.dsc
!endif

# MRC DQS DQ and SPD mapping
!if gSiPkgTokenSpaceGuid.PcdAdlLpSupport == TRUE
!include $(PLATFORM_BOARD_PACKAGE)/SBCVpdStructurePcd/MrcDqDqsSPD/AdlMDqMap.dsc
!include $(PLATFORM_BOARD_PACKAGE)/SBCVpdStructurePcd/MrcDqDqsSPD/AdlMDqsMap.dsc
!include $(PLATFORM_BOARD_PACKAGE)/SBCVpdStructurePcd/MrcDqDqsSPD/AdlMSpdMap.dsc
!include $(PLATFORM_BOARD_PACKAGE)/SBCVpdStructurePcd/MrcDqDqsSPD/AdlPDqMap.dsc
!include $(PLATFORM_BOARD_PACKAGE)/SBCVpdStructurePcd/MrcDqDqsSPD/AdlPDqsMap.dsc
!include $(PLATFORM_BOARD_PACKAGE)/SBCVpdStructurePcd/MrcDqDqsSPD/AdlPSpdMap.dsc
!endif


