## @file
#  Alderlake M RVP GPIO definition table for Pre-Memory Initialization
#
# @copyright
#  INTEL CONFIDENTIAL
#  Copyright 2020 - 2021 Intel Corporation.
#
#  The source code contained or described herein and all documents related to the
#  source code ("Material") are owned by Intel Corporation or its suppliers or
#  licensors. Title to the Material remains with Intel Corporation or its suppliers
#  and licensors. The Material may contain trade secrets and proprietary and
#  confidential information of Intel Corporation and its suppliers and licensors,
#  and is protected by worldwide copyright and trade secret laws and treaty
#  provisions. No part of the Material may be used, copied, reproduced, modified,
#  published, uploaded, posted, transmitted, distributed, or disclosed in any way
#  without Intel's prior express written permission.
#
#  No license under any patent, copyright, trade secret or other intellectual
#  property right is granted to or conferred upon you by disclosure or delivery
#  of the Materials, either expressly, by implication, inducement, estoppel or
#  otherwise. Any license under such intellectual property rights must be
#  express and approved by Intel in writing.
#
#  Unless otherwise agreed by Intel in writing, you may not remove or alter
#  this notice or any other notice embedded in Materials by Intel or
#  Intel's suppliers or licensors in any way.
#
#  This file contains a 'Sample Driver' and is licensed as such under the terms
#  of your license agreement with Intel or your vendor. This file may be modified
#  by the user, subject to the additional terms of the license agreement.
#
# @par Specification
##

###
### !!! GPIOs designated to Native Functions shall not be configured by Platform Code.
### Native Pins shall be configured by Silicon Code (based on BIOS policies setting) or soft straps(set by CSME in FITc).
###
###

# mGpioTablePreMemAdlMLp4Rvp
[PcdsDynamicExVpd.common.SkuIdAdlMLp4Rvp]
gBoardModuleTokenSpaceGuid.VpdPcdBoardGpioTablePreMem|*|{CODE({

  // CPU M.2 SSD
  {GPIO_VER2_LP_GPP_D16, {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,  GpioOutHigh,   GpioIntDis,  GpioPlatformReset,  GpioTermNone}}, //M.2_CPU_SSD_PWREN
  {GPIO_VER2_LP_GPP_H13, {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutLow,    GpioIntDis,  GpioPlatformReset,  GpioTermNone}},  //M2_CPU_SSD1_RESET_N

  // x1 PCIe DT Slot
  {GPIO_VER2_LP_GPP_D14, {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutHigh,  GpioIntDis, GpioPlatformReset,  GpioTermNone}},  //X1_PCIE_SLOT1_PWR_EN
  {GPIO_VER2_LP_GPP_F10, {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutLow,   GpioIntDis, GpioPlatformReset,  GpioTermNone}},  //X1_Slot_RESET

  // TBT Re-Timers
  {GPIO_VER2_LP_GPP_E4,    {GpioPadModeGpio,  GpioHostOwnAcpi,     GpioDirOut,  GpioOutLow,      GpioIntDis,                    GpioPlatformReset, GpioTermNone}},  //TC_RETIMER_FORCE_PWR

  {0x0}  // terminator
})}

# mGpioTablePreMemAdlMLp5Rvp
[PcdsDynamicExVpd.common.SkuIdAdlMLp5Rvp]
gBoardModuleTokenSpaceGuid.VpdPcdBoardGpioTablePreMem|*|{CODE({

  // CPU M.2 SSD
  {GPIO_VER2_LP_GPP_D16, {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,  GpioOutHigh,   GpioIntDis,  GpioPlatformReset,  GpioTermNone}}, //M.2_CPU_SSD_PWREN
  {GPIO_VER2_LP_GPP_H13, {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutLow,    GpioIntDis,  GpioPlatformReset,  GpioTermNone}},  //M2_CPU_SSD1_RESET_N

  // x1 PCIe DT Slot
  {GPIO_VER2_LP_GPP_D14, {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutHigh,  GpioIntDis, GpioPlatformReset,  GpioTermNone}},  //X1_PCIE_SLOT1_PWR_EN
  {GPIO_VER2_LP_GPP_F10, {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutLow,   GpioIntDis, GpioPlatformReset,  GpioTermNone}},  //X1_Slot_RESET

  // TBT Re-Timers
  {GPIO_VER2_LP_GPP_E4,    {GpioPadModeGpio,  GpioHostOwnAcpi,     GpioDirOut,  GpioOutLow,      GpioIntDis,                    GpioPlatformReset, GpioTermNone}},  //TC_RETIMER_FORCE_PWR

  {0x0}  // terminator
})}

# PMIC I2C mux control GPIO
#mGpioTableAdlMLp5PmicRvp
[PcdsDynamicExVpd.common.SkuIdAdlMLp5PmicRvp]
gBoardModuleTokenSpaceGuid.VpdPcdBoardGpioTablePreMem|*|{CODE({
  {GPIO_VER2_LP_GPP_A13, {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault,    GpioOutDefault,    GpioIntDefault,               GpioResetDefault,  GpioTermDefault}},  //Added for PMIC update
  {GPIO_VER2_LP_GPP_A11, {GpioPadModeNative1, GpioHostOwnDefault, GpioDirDefault, GpioOutDefault, GpioIntDefault, GpioResetDefault, GpioTermDefault}},  //Added for PMIC update
  {GPIO_VER2_LP_GPP_A19, {GpioPadModeGpio,  GpioHostOwnGpio, GpioDirOut,    GpioOutHigh, GpioIntDefault,    GpioPlatformReset, GpioTermDefault }},
  {GPIO_VER2_LP_GPP_H17, {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,  GpioOutHigh,   GpioIntDefault, GpioPlatformReset,  GpioTermDefault}},   //CSC2_I2C_EN
  {0x0}  // terminator
})}

# mGpioTablePreMemAdlMLp5Aep
[PcdsDynamicExVpd.common.SkuIdAdlMLp5Aep]
gBoardModuleTokenSpaceGuid.VpdPcdBoardGpioTablePreMem|*|{CODE({

  // CPU M.2 SSD
  {GPIO_VER2_LP_GPP_D16, {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,  GpioOutHigh,   GpioIntDis,  GpioPlatformReset,  GpioTermNone}}, //M.2_CPU_SSD_PWREN
  {GPIO_VER2_LP_GPP_H13, {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutLow,    GpioIntDis,  GpioPlatformReset,  GpioTermNone}},  //M2_CPU_SSD1_RESET_N

  // TBT Re-Timers
  {GPIO_VER2_LP_GPP_E4,  {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutLow,    GpioIntDis,  GpioPlatformReset,  GpioTermNone}},  //TC_RETIMER_FORCE_PWR

  {0x0}  // terminator
})}
