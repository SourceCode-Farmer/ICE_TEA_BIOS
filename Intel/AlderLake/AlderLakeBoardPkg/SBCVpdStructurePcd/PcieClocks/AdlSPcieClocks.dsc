## @file
#  Alderlake S Pcie Clock configuration file.
#
# @copyright
#  INTEL CONFIDENTIAL
#  Copyright 2020 - 2021 Intel Corporation.
#
#  The source code contained or described herein and all documents related to the
#  source code ("Material") are owned by Intel Corporation or its suppliers or
#  licensors. Title to the Material remains with Intel Corporation or its suppliers
#  and licensors. The Material may contain trade secrets and proprietary and
#  confidential information of Intel Corporation and its suppliers and licensors,
#  and is protected by worldwide copyright and trade secret laws and treaty
#  provisions. No part of the Material may be used, copied, reproduced, modified,
#  published, uploaded, posted, transmitted, distributed, or disclosed in any way
#  without Intel's prior express written permission.
#
#  No license under any patent, copyright, trade secret or other intellectual
#  property right is granted to or conferred upon you by disclosure or delivery
#  of the Materials, either expressly, by implication, inducement, estoppel or
#  otherwise. Any license under such intellectual property rights must be
#  express and approved by Intel in writing.
#
#  Unless otherwise agreed by Intel in writing, you may not remove or alter
#  this notice or any other notice embedded in Materials by Intel or
#  Intel's suppliers or licensors in any way.
#
#  This file contains a 'Sample Driver' and is licensed as such under the terms
#  of your license agreement with Intel or your vendor. This file may be modified
#  by the user, subject to the additional terms of the license agreement.
#
# @par Specification
##

[PcdsDynamicExVpd.common.SkuIdAdlSAdpSDdr5UDimm1DAep]
gBoardModuleTokenSpaceGuid.VpdPcdPcieClkUsageMap|*|{CODE(
{{
  PCIE_PEG + 1,    // 1x16 PEG slot
  NOT_USED,
  NOT_USED,
  NOT_USED,
  NOT_USED,
  PCIE_PCH + 6,    // LAN Foxville
  NOT_USED,
  NOT_USED,
  NOT_USED,
  PCIE_PCH + 7,    // M.2 Key E WLAN
  PCIE_PCH + 20,  // Maple Ridge (BP)
  PCIE_PEG,       // M.2 Key M PEG Slot
  PCIE_PCH + 12,  // M.2 Socket3 Key M SSD 1
  PCIE_PCH + 24,  // Maple Ridge (FP)
  PCIE_PCH + 8,   // EDSFF Hot Swappable Ruler storage
  NOT_USED,
  NOT_USED,
  NOT_USED
}}
)}

[PcdsDynamicExVpd.common.SkuIdAdlSAdpSDdr5UDimm1DCrb]
gBoardModuleTokenSpaceGuid.VpdPcdPcieClkUsageMap|*|{CODE(
{{
  PCIE_PEG + 1,
  NOT_USED,
  PCIE_PCH + 20,  // PCIE x4 Slot 1
  NOT_USED,
  NOT_USED,
  LAN_CLOCK,
  NOT_USED,
  NOT_USED,
  NOT_USED,
  PCIE_PCH + 7,   // M.2_WLAN
  PCIE_PCH,       // M.2_SSD_3
  PCIE_PEG,       // CPU_M.2_SSD
  PCIE_PCH + 12,  // M.2_SSD
  PCIE_PCH + 24,  // M.2_SSD_2
  PCIE_PCH + 8,   // PCIE X4_SLOT_2
  PCIE_PCH + 4,    // PCIE X2_SLOT_3
  NOT_USED,
  NOT_USED
}}
)}

[PcdsDynamicExVpd.common.SkuIdAdlSAdpSDdr4UDimm2DCrb]
gBoardModuleTokenSpaceGuid.VpdPcdPcieClkUsageMap|*|{CODE(
{{
  PCIE_PEG + 1,
  PCIE_PEG + 2,
  PCIE_PCH + 20,  // PCIE x4 Slot 1
  NOT_USED,
  NOT_USED,
  LAN_CLOCK,
  NOT_USED,
  NOT_USED,
  NOT_USED,
  PCIE_PCH + 7,   // M.2_WLAN
  PCIE_PCH,       // M.2_SSD_3
  PCIE_PEG,       // CPU_M.2_SSD
  PCIE_PCH + 12,  // M.2_SSD
  PCIE_PCH + 24,  // M.2_SSD_2
  PCIE_PCH + 8,   // PCIE X4_SLOT_2
  PCIE_PCH + 4,   // PCIE X2_SLOT_3
  NOT_USED,
  NOT_USED
}}
)}

[PcdsDynamicExVpd.common.SkuIdAdlSAdpSDdr4SODimmCrb]
gBoardModuleTokenSpaceGuid.VpdPcdPcieClkUsageMap|*|{CODE(
{{
  PCIE_PEG + 1,
  NOT_USED,
  PCIE_PCH + 20,  // PCIE x4 Slot 1
  NOT_USED,
  NOT_USED,
  PCIE_PCH + 6,   // Foxville
  NOT_USED,
  NOT_USED,
  NOT_USED,
  PCIE_PCH + 7,   // M.2_WLAN
  PCIE_PCH,       // M.2_SSD_3
  PCIE_PEG,       // CPU_M.2_SSD
  PCIE_PCH + 12,  // M.2_SSD
  PCIE_PCH + 24,  // Thunderbolt
  PCIE_PCH + 8,   // PCIE X4_SLOT_2
  PCIE_PCH + 4,    // PCIE X2_SLOT_3
  NOT_USED,
  NOT_USED
}}
)}

[PcdsDynamicExVpd.common.SkuIdAdlSAdpSDdr5SODimmCrb]
gBoardModuleTokenSpaceGuid.VpdPcdPcieClkUsageMap|*|{CODE(
{{
  PCIE_PEG + 1,
  NOT_USED,
  PCIE_PCH + 20,  // PCIE x4 Slot 1
  NOT_USED,
  NOT_USED,
  PCIE_PCH + 6,   // Foxville
  NOT_USED,
  NOT_USED,
  NOT_USED,
  PCIE_PCH + 7,   // M.2_WLAN
  PCIE_PCH,       // M.2_SSD_3
  PCIE_PEG,       // CPU_M.2_SSD
  PCIE_PCH + 12,  // M.2_SSD
  PCIE_PCH + 24,  // TBT
  PCIE_PCH + 8,   // PCIE X4_SLOT_2
  PCIE_PCH + 4,   // PCIE X2_SLOT_3
  NOT_USED,
  NOT_USED
}}
)}


[PcdsDynamicExVpd.common.SkuIdAdlSAdpSSbgaDdr5SODimmErb]
gBoardModuleTokenSpaceGuid.VpdPcdPcieClkUsageMap|*|{CODE(
{{
  PCIE_PEG + 1,   // x16 PEG Slot
  NOT_USED,
  PCIE_PCH + 12,  // PCIE x4 Slot 1
  NOT_USED,
  NOT_USED,
  PCIE_PCH + 6,   // Foxville
  NOT_USED,
  NOT_USED,
  NOT_USED,
  PCIE_PCH + 7,   // M.2_WLAN
  PCIE_PCH + 1,   // M.2_WWAN
  PCIE_PEG,       // CPU_M.2 Key M
  PCIE_PCH + 20,  // M.2_SSD 1 Key M
  PCIE_PCH + 24,  // TBT 0
  PCIE_PCH + 8,   // TBT 1
  NOT_USED,
  NOT_USED,
  NOT_USED
}}
)}

[PcdsDynamicExVpd.common.SkuIdAdlSAdpSSbgaDdr4SODimmCrb]
gBoardModuleTokenSpaceGuid.VpdPcdPcieClkUsageMap|*|{CODE(
{{
  PCIE_PEG + 1,   // x16 PEG Slot
  NOT_USED,
  PCIE_PCH + 12,  // PCIE x4 Slot 1
  NOT_USED,
  NOT_USED,
  PCIE_PCH + 6,   // Foxville
  NOT_USED,
  NOT_USED,
  NOT_USED,
  PCIE_PCH + 7,   // M.2_WLAN
  PCIE_PCH + 1,   // M.2_WWAN
  PCIE_PEG,       // CPU_M.2 Key M
  PCIE_PCH + 20,  // M.2_SSD 1 Key M
  PCIE_PCH + 24,  // TBT
  NOT_USED,
  NOT_USED,
  NOT_USED,
  NOT_USED
}}
)}

[PcdsDynamicExVpd.common.SkuIdAdlSAdpSSbgaDdr5SODimmAep]
gBoardModuleTokenSpaceGuid.VpdPcdPcieClkUsageMap|*|{CODE(
{{
  PCIE_PEG + 1,   // x16 PEG Slot
  NOT_USED,
  NOT_USED,
  NOT_USED,
  NOT_USED,
  PCIE_PCH + 6,   // Foxville
  NOT_USED,
  NOT_USED,
  NOT_USED,
  PCIE_PCH + 24,  // M.2_WLAN
  PCIE_PCH + 1,   // M.2_WWAN
  PCIE_PCH + 20,  // PCH SSD2
  PCIE_PCH + 12,  // PCH SSD1
  PCIE_PCH + 8,   // TBT 0
  NOT_USED,
  NOT_USED,
  NOT_USED,
  NOT_USED
}}
)}
