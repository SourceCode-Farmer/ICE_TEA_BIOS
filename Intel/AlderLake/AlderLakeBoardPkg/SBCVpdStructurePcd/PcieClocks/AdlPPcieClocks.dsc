## @file
#  Alderlake P Pcie Clock configuration file.
#
# @copyright
#  INTEL CONFIDENTIAL
#  Copyright 2020 - 2021 Intel Corporation.
#
#  The source code contained or described herein and all documents related to the
#  source code ("Material") are owned by Intel Corporation or its suppliers or
#  licensors. Title to the Material remains with Intel Corporation or its suppliers
#  and licensors. The Material may contain trade secrets and proprietary and
#  confidential information of Intel Corporation and its suppliers and licensors,
#  and is protected by worldwide copyright and trade secret laws and treaty
#  provisions. No part of the Material may be used, copied, reproduced, modified,
#  published, uploaded, posted, transmitted, distributed, or disclosed in any way
#  without Intel's prior express written permission.
#
#  No license under any patent, copyright, trade secret or other intellectual
#  property right is granted to or conferred upon you by disclosure or delivery
#  of the Materials, either expressly, by implication, inducement, estoppel or
#  otherwise. Any license under such intellectual property rights must be
#  express and approved by Intel in writing.
#
#  Unless otherwise agreed by Intel in writing, you may not remove or alter
#  this notice or any other notice embedded in Materials by Intel or
#  Intel's suppliers or licensors in any way.
#
#  This file contains a 'Sample Driver' and is licensed as such under the terms
#  of your license agreement with Intel or your vendor. This file may be modified
#  by the user, subject to the additional terms of the license agreement.
#
# @par Specification
##

[PcdsDynamicExVpd.common.SkuIdAdlPLp4Rvp]
gBoardModuleTokenSpaceGuid.VpdPcdPcieClkUsageMap|*|{CODE(
{{
  PCIE_PEG,              // CPU M.2 SSD 1
  PCIE_PCH + 8,          // PCH M.2 SSD
  PCIE_PCH + 4,
  PCIE_PEG + 1,          // X8 DG/DG2
  PCIE_PEG + 2,          // CPU M.2 SSD 2
  PCIE_PCH + 5,          // M.2 KEY B WWAN - PCIe P6
  LAN_CLOCK,
  // Default Case:
  // - PCIe P7 mapped to GBELAN
  // - PCIe P8 mapped to x4 PCIe DT Slot (Pair 1)
  PCIE_PCH + 7,          // x4 PCIe DT Slot (x1)
  // Reworked Case: with rework and soft strap changes
  // - PCIe P7 mapped to x4 PCIe DT Slot (Pair 2)
  // - PCIe P8 mapped to x4 PCIe DT Slot (Pair 1)
  // @todo: To support reworked case, enable the below code.
  // PCIE_PCH + 6,       // x4 PCIe DT Slot (x2)
  NOT_USED,
  NOT_USED
}}
)}


[PcdsDynamicExVpd.common.SkuIdAdlPDdr5Rvp]
gBoardModuleTokenSpaceGuid.VpdPcdPcieClkUsageMap|*|{CODE(
{{
  PCIE_PEG,              // CPU M.2 SSD 1
  PCIE_PCH + 8,          // PCH M.2 SSD
  PCIE_PCH + 4,
  PCIE_PEG + 1,          // X8 DG/DG2
  PCIE_PEG + 2,          // CPU M.2 SSD 2
  PCIE_PCH + 5,          // M.2 KEY B WWAN - PCIe P6
  LAN_CLOCK,
  // Default Case:
  // - PCIe P7 mapped to GBELAN
  // - PCIe P8 mapped to x4 PCIe DT Slot (Pair 1)
  PCIE_PCH + 7,          // x4 PCIe DT Slot (x1)
  // Reworked Case: with rework and soft strap changes
  // - PCIe P7 mapped to x4 PCIe DT Slot (Pair 2)
  // - PCIe P8 mapped to x4 PCIe DT Slot (Pair 1)
  // @todo: To support reworked case, enable the below code.
  // PCIE_PCH + 6,       // x4 PCIe DT Slot (x2)
  NOT_USED,
  NOT_USED
}}
)}


[PcdsDynamicExVpd.common.SkuIdAdlPDdr4Rvp]
#[-start-210721-QINGLIN0001-modify]#
!if $(LCFC_SUPPORT_ENABLE) == YES 
!if $(S570_SUPPORT_ENABLE) == YES 
gBoardModuleTokenSpaceGuid.VpdPcdPcieClkUsageMap|*|{CODE(
{{
  PCIE_PEG,              // CPU M.2 SSD 1
  PCIE_PCH + 8,          // WLAN
  NOT_USED,
  PCIE_PEG + 2,          // CPU dGPU
  NOT_USED,
  NOT_USED,
  NOT_USED,
  NOT_USED
}}
)}
!endif 
#[-start-210803-QINGLIN0008-add]#
!if $(S370_SUPPORT_ENABLE) == YES 
gBoardModuleTokenSpaceGuid.VpdPcdPcieClkUsageMap|*|{CODE(
{{
  PCIE_PEG,              // CPU M.2 SSD 1
  PCIE_PCH + 8,          // WLAN
  PCIE_PCH + 9,          // LAN or Card Reader
  PCIE_PEG + 2,          // CPU dGPU
  NOT_USED,
  NOT_USED,
  NOT_USED,
  NOT_USED
}}
)}
!endif 
#[-end-210803-QINGLIN0008-add]#
!else 
gBoardModuleTokenSpaceGuid.VpdPcdPcieClkUsageMap|*|{CODE(
{
  PCIE_PEG,              // CPU M.2 SSD 1
  PCIE_PCH + 8,          // PCH M.2 SSD
  PCIE_PCH + 4,
  PCIE_PEG + 1,          // X8 DG/DG2
  PCIE_PEG + 2,          // CPU M.2 SSD 2
  PCIE_PCH + 5,          // M.2 KEY B WWAN - PCIe P6
  LAN_CLOCK,
  // Default Case:
  // - PCIe P7 mapped to GBELAN
  // - PCIe P8 mapped to x4 PCIe DT Slot (Pair 1)
  PCIE_PCH + 7,          // x4 PCIe DT Slot (x1)
  // Reworked Case: with rework and soft strap changes
  // - PCIe P7 mapped to x4 PCIe DT Slot (Pair 2)
  // - PCIe P8 mapped to x4 PCIe DT Slot (Pair 1)
  // @todo: To support reworked case, enable the below code.
  // PCIE_PCH + 6,       // x4 PCIe DT Slot (x2)
  NOT_USED,
  NOT_USED
}
)}
!endif
#[-end-210721-QINGLIN0001-modify]#

[PcdsDynamicExVpd.common.SkuIdAdlPLp4Bep]
gBoardModuleTokenSpaceGuid.VpdPcdPcieClkUsageMap|*|{CODE(
{{
  PCIE_PEG,          // CPU M.2 SSD
  NOT_USED,
  NOT_USED,
  NOT_USED,
  NOT_USED,
  PCIE_PCH + 5,      // M.2 KEY B WWAN - PCIe P6
  NOT_USED,
  NOT_USED,
  NOT_USED,
  NOT_USED
}}
)}

[PcdsDynamicExVpd.common.SkuIdAdlPLp5Rvp]
#[-start-210519-KEBIN00001-modify]#
!if $(LCFC_SUPPORT_ENABLE) == YES 
 !if $(C970_SUPPORT_ENABLE) == YES 
gBoardModuleTokenSpaceGuid.VpdPcdPcieClkUsageMap|*|{CODE(
{{
  PCIE_PEG,              // CPU M.2 SSD 1
  PCIE_PCH + 8,          // WLAN
  NOT_USED,
  NOT_USED,           
  NOT_USED,           
  NOT_USED,           
  NOT_USED,
  NOT_USED          // x4 PCIe DT Slot (x1)
}}
)}
 !endif

#[start-210720-STORM1100-modify]#
 !if $(C770_SUPPORT_ENABLE) == YES 
gBoardModuleTokenSpaceGuid.VpdC770DISPcdPcieClkUsageMap|*|{CODE(
{{
  PCIE_PEG,              // CPU M.2 SSD 1
  PCIE_PCH + 8,          // WLAN
  PCIE_PCH + 9,          // card reader
  PCIE_PEG + 2,          // CPU dGPU
  NOT_USED,
  NOT_USED,
  NOT_USED,
  NOT_USED
}}
)}

gBoardModuleTokenSpaceGuid.VpdC770UMAPcdPcieClkUsageMap|*|{CODE(
{{
  PCIE_PEG,              // CPU M.2 SSD 1
  PCIE_PCH + 8,          // WLAN
  PCIE_PCH + 9,          // card reader
  NOT_USED,
  NOT_USED,
  NOT_USED,
  NOT_USED,
  NOT_USED
}}
)}

 !endif
#[end-210720-STORM1100-modify]#

#[-start-210817-DABING0002-modify]#
 !if ($(S77014_SUPPORT_ENABLE) == YES) OR ($(S77014IAH_SUPPORT_ENABLE) == YES)
#[-start-210927-TAMT000015-modify]#
gBoardModuleTokenSpaceGuid.VpdS77014DISPcdPcieClkUsageMap|*|{CODE(
{{
  PCIE_PEG,              // CPU M.2 SSD 1
  PCIE_PCH + 8,          // WLAN
  NOT_USED,
  PCIE_PEG + 2,          // NV DGPU
  NOT_USED,           
  NOT_USED,           
  NOT_USED,
  NOT_USED         
}}
)}

gBoardModuleTokenSpaceGuid.VpdS77014UMAPcdPcieClkUsageMap|*|{CODE(
{{
  PCIE_PEG,              // CPU M.2 SSD 1
  PCIE_PCH + 8,          // WLAN
  NOT_USED,
  NOT_USED,  
  NOT_USED,           
  NOT_USED,           
  NOT_USED,
  NOT_USED         
}}
)}
#[-end-210927-TAMT000015-modify]#
 !endif
#[-end-210817-DABING0002-modify]# 

#[-start-210914-DABING0006-modify]#
 !if $(S77013_SUPPORT_ENABLE) == YES 
gBoardModuleTokenSpaceGuid.VpdPcdPcieClkUsageMap|*|{CODE(
{{
  PCIE_PEG,              // CPU M.2 SSD 1
  PCIE_PCH + 8,          // WLAN
  NOT_USED,
  NOT_USED,        
  NOT_USED,           
  NOT_USED,           
  NOT_USED,
  NOT_USED         
}}
)}
 !endif
#[-end-210914-DABING0006-modify]#
 
!else 
gBoardModuleTokenSpaceGuid.VpdPcdPcieClkUsageMap|*|{CODE(
{{
  PCIE_PEG,              // CPU M.2 SSD 1
  PCIE_PCH + 8,          // PCH M.2 SSD
  PCIE_PCH + 4,
  PCIE_PEG + 1,          // X8 DG/DG2
  PCIE_PEG + 2,          // CPU M.2 SSD 2
  PCIE_PCH + 5,          // M.2 KEY B WWAN - PCIe P6
  LAN_CLOCK,
  // Default Case:
  // - PCIe P7 mapped to GBELAN
  // - PCIe P8 mapped to x4 PCIe DT Slot (Pair 1)
  PCIE_PCH + 7,          // x4 PCIe DT Slot (x1)
  // Reworked Case: with rework and soft strap changes
  // - PCIe P7 mapped to x4 PCIe DT Slot (Pair 2)
  // - PCIe P8 mapped to x4 PCIe DT Slot (Pair 1)
  // @todo: To support reworked case, enable the below code.
  // PCIE_PCH + 6,       // x4 PCIe DT Slot (x2)
  NOT_USED,
  NOT_USED
}}
)}
!endif
#[-end-210519-KEBIN00001-modify]#

[PcdsDynamicExVpd.common.SkuIdAdlPLp4Bep]
gBoardModuleTokenSpaceGuid.VpdPcdPcieClkUsageMap|*|{CODE(
{{
  PCIE_PEG,          // CPU M.2 SSD
  NOT_USED,
  NOT_USED,
  NOT_USED,
  NOT_USED,
  PCIE_PCH + 5,      // M.2 KEY B WWAN - PCIe P6
  NOT_USED,
  NOT_USED,
  NOT_USED,
  NOT_USED
}}
)}

[PcdsDynamicExVpd.common.SkuIdAdlPLp5Rvp]
gBoardModuleTokenSpaceGuid.VpdPcdPcieClkUsageMap|*|{CODE(
{{
  PCIE_PEG,              // CPU M.2 SSD 1
  PCIE_PCH + 8,          // PCH M.2 SSD
  PCIE_PCH + 4,
  PCIE_PEG + 1,          // X8 DG/DG2
  PCIE_PEG + 2,          // CPU M.2 SSD 2
  PCIE_PCH + 5,          // M.2 KEY B WWAN - PCIe P6
  LAN_CLOCK,
  // Default Case:
  // - PCIe P7 mapped to GBELAN
  // - PCIe P8 mapped to x4 PCIe DT Slot (Pair 1)
  PCIE_PCH + 7,          // x4 PCIe DT Slot (x1)
  // Reworked Case: with rework and soft strap changes
  // - PCIe P7 mapped to x4 PCIe DT Slot (Pair 2)
  // - PCIe P8 mapped to x4 PCIe DT Slot (Pair 1)
  // @todo: To support reworked case, enable the below code.
  // PCIE_PCH + 6,       // x4 PCIe DT Slot (x2)
  NOT_USED,
  NOT_USED
}}
)}

[PcdsDynamicExVpd.common.SkuIdAdlPDdr5Dg384Aep]
gBoardModuleTokenSpaceGuid.VpdPcdPcieClkUsageMap|*|{CODE(
{{
  NOT_USED,
  PCIE_PCH + 8, //PCIe X4 Nvme / x1 M.2 SATA/ 2x2 Hybrid
  NOT_USED,
  PCIE_PEG + 1, // X8 DG2-384
  PCIE_PEG + 2, // X4 SSD2
  NOT_USED,
  PCIE_PCH + 6, // SD controller
  NOT_USED,
  NOT_USED,
  NOT_USED
}}
)}

[PcdsDynamicExVpd.common.SkuIdAdlPMMAep]
gBoardModuleTokenSpaceGuid.VpdPcdPcieClkUsageMap|*|{CODE(
{{
  NOT_USED,
  PCIE_PCH + 8, // PCIe X4 Nvme / x1 M.2 SATA/ 2x2 Hybrid
  PCIE_PCH + 4, // M.2 WLAN
  PCIE_PEG + 1, // X8 DG2-384
  PCIE_PEG + 2, // X4 SSD2
  PCIE_PCH + 5, // WWAN
  PCIE_PCH + 7, // Foxville lan
  NOT_USED,
  NOT_USED,
  NOT_USED
}}
)}

[PcdsDynamicExVpd.common.SkuIdAdlPLp5Aep]
gBoardModuleTokenSpaceGuid.VpdPcdPcieClkUsageMap|*|{CODE(
{{
  PCIE_PEG,              // CPU M.2 SSD 1
  NOT_USED,
  NOT_USED,
  NOT_USED,
  NOT_USED,
  PCIE_PCH + 5,          // M.2 KEY B WWAN - PCIe P6
  NOT_USED,
  NOT_USED,
  NOT_USED,
  NOT_USED
}}
)}

[PcdsDynamicExVpd.common.SkuIdAdlPLp5Dg128Aep]
gBoardModuleTokenSpaceGuid.VpdPcdPcieClkUsageMap|*|{CODE(
{{
  NOT_USED,
  NOT_USED,
  NOT_USED,
  PCIE_PEG + 1, // X8 DG2-128
  NOT_USED,
  PCIE_PCH + 5, // M.2 KEY B WWAN - PCIe P6
  PCIE_PEG + 2, // X4 SSD2
  NOT_USED,
  NOT_USED,
  NOT_USED
}}
)}

[PcdsDynamicExVpd.common.SkuIdAdlPLp5Gcs]
gBoardModuleTokenSpaceGuid.VpdPcdPcieClkUsageMap|*|{CODE(
{{
  PCIE_PEG,              // CPU M.2 SSD 1
  NOT_USED,
  PCIE_PCH + 4,          // M.2 KEY E WLAN - PCIe P5
  NOT_USED,
  NOT_USED,
  NOT_USED,
  NOT_USED,
  NOT_USED,
  NOT_USED,
  NOT_USED
}}
)}

[PcdsDynamicExVpd.common.SkuIdAdlPDdr5MRRvp]
gBoardModuleTokenSpaceGuid.VpdPcdPcieClkUsageMap|*|{CODE(
{{
  PCIE_PEG,              // CPU M.2 SSD 1
  PCIE_PCH + 4,          // X4 to Mapple Ridge1
  PCIE_PCH + 8,          //X4 to Mapple Ridge2
  PCIE_PEG + 1,          // X8 DG/DG2
  PCIE_PEG + 2,          // CPU M.2 SSD 2
  PCIE_PCH + 2,          // M.2 KEY B
  PCIE_PCH + 3,          // X1 PCIe connector
  NOT_USED,
  NOT_USED,
  NOT_USED
}}
)}

[PcdsDynamicExVpd.common.SkuIdAdlPLp5MbAep]
gBoardModuleTokenSpaceGuid.VpdPcdPcieClkUsageMap|*|{CODE(
{{
  NOT_USED,
  NOT_USED,
  NOT_USED,
  NOT_USED,
  NOT_USED,
  PCIE_PCH + 5, // M.2 KEY B WWAN - PCIe P6
  PCIE_PEG + 2, // X4 SSD2
  NOT_USED,
  PCIE_PEG,     // X4 SSD
  NOT_USED
}}
)}