## @file
# 
#******************************************************************************
#* Copyright 2021 Insyde Software Corp. All Rights Reserved.
#*
#* You may not reproduce, distribute, publish, display, perform, modify, adapt,
#* transmit, broadcast, present, recite, release, license or otherwise exploit
#* any part of this publication in any form, by any means, without the prior
#* written permission of Insyde Software Corp.
#*
#******************************************************************************
## @file
#  Board description file initializes configuration (PCD) settings for the project.
#
# @copyright
#  INTEL CONFIDENTIAL
#  Copyright 2020 - 2021 Intel Corporation.
#
#  The source code contained or described herein and all documents related to the
#  source code ("Material") are owned by Intel Corporation or its suppliers or
#  licensors. Title to the Material remains with Intel Corporation or its suppliers
#  and licensors. The Material may contain trade secrets and proprietary and
#  confidential information of Intel Corporation and its suppliers and licensors,
#  and is protected by worldwide copyright and trade secret laws and treaty
#  provisions. No part of the Material may be used, copied, reproduced, modified,
#  published, uploaded, posted, transmitted, distributed, or disclosed in any way
#  without Intel's prior express written permission.
#
#  No license under any patent, copyright, trade secret or other intellectual
#  property right is granted to or conferred upon you by disclosure or delivery
#  of the Materials, either expressly, by implication, inducement, estoppel or
#  otherwise. Any license under such intellectual property rights must be
#  express and approved by Intel in writing.
#
#  Unless otherwise agreed by Intel in writing, you may not remove or alter
#  this notice or any other notice embedded in Materials by Intel or
#  Intel's suppliers or licensors in any way.
#
#  This file contains a 'Sample Driver' and is licensed as such under the terms
#  of your license agreement with Intel or your vendor. This file may be modified
#  by the user, subject to the additional terms of the license agreement.
#
# @par Specification
##

[Packages]
  Usb3DebugFeaturePkg/Usb3DebugFeaturePkg.dec
  PostCodeDebugFeaturePkg/PostCodeDebugFeaturePkg.dec
  BeepDebugFeaturePkg/BeepDebugFeaturePkg.dec
  MebxFeaturePkg/MebxFeaturePkg.dec

[PcdsFeatureFlag]
  ## This PCD specified whether ACPI SDT protocol is installed.
  # ROYAL_PARK_OVERRIDE: PcdInstallAcpiSdtProtocol
  gEfiMdeModulePkgTokenSpaceGuid.PcdInstallAcpiSdtProtocol|TRUE
  gEfiMdeModulePkgTokenSpaceGuid.PcdPeiCoreImageLoaderSearchTeSectionFirst|FALSE
  gEfiMdeModulePkgTokenSpaceGuid.PcdPciDegradeResourceForOptionRom|FALSE
  # Disabling PcdBrowserGrayOutTextStatement causes all empty/disabled rows active
  gEfiMdeModulePkgTokenSpaceGuid.PcdBrowserGrayOutTextStatement|TRUE

  gMinPlatformPkgTokenSpaceGuid.PcdStopAfterMemInit|FALSE
  gMinPlatformPkgTokenSpaceGuid.PcdBootToShellOnly|FALSE
  gMinPlatformPkgTokenSpaceGuid.PcdSmiHandlerProfileEnable|FALSE
  gMinPlatformPkgTokenSpaceGuid.PcdTpm2Enable|FALSE
  # Build scripts override the value of this PCD, update value in scripts for the change to take effect.
  gMinPlatformPkgTokenSpaceGuid.PcdUefiSecureBootEnable|FALSE
  gMinPlatformPkgTokenSpaceGuid.PcdPerformanceEnable|FALSE

  gBoardModuleTokenSpaceGuid.PcdSecurityEnable|FALSE
  gBoardModuleTokenSpaceGuid.PcdAdvancedFeatureEnable|FALSE
  gBoardModuleTokenSpaceGuid.PcdOptimizationEnable|FALSE
  gBoardModuleTokenSpaceGuid.PcdIntelGopEnable|FALSE                  #LegacyVideoRom.bin and IntelGopDriver.efi
  gBoardModuleTokenSpaceGuid.PcdUefiShellEnable|FALSE

  gNhltFeaturePkgTokenSpaceGuid.PcdNhltFeatureEnable|FALSE

  gSndwFeatureModuleTokenSpaceGuid.PcdSndwFeatureEnable|FALSE

  gMebxFeaturePkgTokenSpaceGuid.PcdMebxFeatureEnable|TRUE


  gUefiCpuPkgTokenSpaceGuid.PcdCpuSmmEnableBspElection|FALSE

  # XmlCli: Initialize Feature Pcd
  gXmlCliFeaturePkgTokenSpaceGuid.PcdXmlCliFeatureEnable|TRUE

[PcdsFeatureFlag.X64]
  # Optimze Driver init time in FastBoot Mode
  # If set PcdPs2KbdExtendedVerification to False, we can save 380 ms for Ps2KeyboardDxe driver initialize time
  gEfiMdeModulePkgTokenSpaceGuid.PcdPs2KbdExtendedVerification|FALSE
  #
  # Enabling PcdHiiOsRuntimeSupport casuses S4 failure due to
  # insuficient Runtime memory allocation
  #
  gEfiMdeModulePkgTokenSpaceGuid.PcdHiiOsRuntimeSupport|FALSE

[PcdsFixedAtBuild]
  gSiPkgTokenSpaceGuid.PcdAcpiEnable|FALSE
  gSiPkgTokenSpaceGuid.PcdSmbiosEnable|FALSE
  gSiPkgTokenSpaceGuid.PcdS3Enable|FALSE
  gSiPkgTokenSpaceGuid.PcdITbtEnable|FALSE
  gSiPkgTokenSpaceGuid.PcdBiosGuardEnable|FALSE
#[-start-200420-IB17800056-modify]#
  # ADL PO Temporary remove
  gSiPkgTokenSpaceGuid.PcdHgEnable|TRUE
#[-end-200420-IB17800056-modify]#
  gSiPkgTokenSpaceGuid.PcdBootGuardEnable|FALSE
  gSiPkgTokenSpaceGuid.PcdAtaEnable|FALSE
  gSiPkgTokenSpaceGuid.PcdFspWrapperEnable|TRUE
  gSiPkgTokenSpaceGuid.PcdOcWdtEnable|FALSE
  gSiPkgTokenSpaceGuid.PcdSerialIoUartEnable|FALSE
  gSiPkgTokenSpaceGuid.PcdIpuEnable|FALSE
  gSiPkgTokenSpaceGuid.PcdPeiDisplayEnable|FALSE
  gSiPkgTokenSpaceGuid.PcdVtdEnable|FALSE
  gSiPkgTokenSpaceGuid.PcdGnaEnable|FALSE
  gSiPkgTokenSpaceGuid.PcdThcEnable|FALSE
  gSiPkgTokenSpaceGuid.PcdPpmEnable|FALSE
  gSiPkgTokenSpaceGuid.PcdPpamEnable|FALSE
  gSiPkgTokenSpaceGuid.PcdSpaEnable|FALSE
  gSiPkgTokenSpaceGuid.PcdPsmiEnable|FALSE
  gSiPkgTokenSpaceGuid.PcdVmdEnable|FALSE
  gSiPkgTokenSpaceGuid.PcdCpuPcieEnable|FALSE
  gSiPkgTokenSpaceGuid.PcdTraceHubDebugLibMaster|0x48
  gSiPkgTokenSpaceGuid.PcdTraceHubDebugLibChannel|0xE
  gSiPkgTokenSpaceGuid.PcdAcpiDebugEnableFlag|TRUE   # Acpi debug enable flag
  #
  # 16MB TSEG.
  #
  gSiPkgTokenSpaceGuid.PcdTsegSize|0x1000000

  #
  # Default BootStage is set here.
  # Stage 1 - enable debug (system deadloop after debug init)
  # Stage 2 - mem init (system deadloop after mem init)
  # Stage 3 - boot to shell only
  # Stage 4 - boot to OS
  # Stage 5 - boot to OS with security boot enabled
  # Stage 6 - boot with advanced features
  # stage 7 - tuning
  #
  gMinPlatformPkgTokenSpaceGuid.PcdBootStage|6
  gMinPlatformPkgTokenSpaceGuid.PcdFspWrapperBootMode|TRUE
  gMinPlatformPkgTokenSpaceGuid.PcdMaxCpuSocketCount|1
  gMinPlatformPkgTokenSpaceGuid.PcdPlatformEfiAcpiReclaimMemorySize|0xA2
  gMinPlatformPkgTokenSpaceGuid.PcdPlatformEfiAcpiNvsMemorySize|0xE8
  gMinPlatformPkgTokenSpaceGuid.PcdPlatformEfiReservedMemorySize|0x3100
  gMinPlatformPkgTokenSpaceGuid.PcdPlatformEfiRtDataMemorySize|0x6E
  gMinPlatformPkgTokenSpaceGuid.PcdPlatformEfiRtCodeMemorySize|0x99
  #
  # The PCDs are used to control the Windows SMM Security Mitigations Table - Protection Flags
  #
  # BIT0: If set, expresses that for all synchronous SMM entries,SMM will validate that input and output buffers lie entirely within the expected fixed memory regions.
  # BIT1: If set, expresses that for all synchronous SMM entries, SMM will validate that input and output pointers embedded within the fixed communication buffer only refer to address ranges \
  #       that lie entirely within the expected fixed memory regions.
  # BIT2: Firmware setting this bit is an indication that it will not allow reconfiguration of system resources via non-architectural mechanisms.
  # BIT3-31: Reserved
  #
  gMinPlatformPkgTokenSpaceGuid.PcdWsmtProtectionFlags|0x07

  gIntelFsp2WrapperTokenSpaceGuid.PcdFspModeSelection|0

  ## This PCD decides how FSP is measured
  # 1) The BootGuard ACM may already measured the FSP component, such as FSPT/FSPM.
  # We need a flag (PCD) to indicate if there is need to do such FSP measurement or NOT.
  # 2) The FSP binary includes FSP code and FSP UPD region. The UPD region is considered
  # as configuration block, and it may be updated by OEM by design.
  # This flag (PCD) is to indicate if we need isolate the the UPD region from the FSP code region.
  # BIT0: Need measure FSP.  (for FSP 1.x) - reserved in FSP2.
  # BIT1: Need measure FSPT. (for FSP 2.x)
  # BIT2: Need measure FSPM. (for FSP 2.x)
  # BIT3: Need measure FSPS. (for FSP 2.x)
  # BIT4~30: reserved.
  # BIT31: Need isolate UPD region measurement.
  #   0: measure FSP[T|M|S] as one binary in one record (PCR0).
  #   1: measure FSP UPD region in one record (PCR1),
  #      measure the FSP code without UPD in another record (PCR0).
  #
  gIntelFsp2WrapperTokenSpaceGuid.PcdFspMeasurementConfig|0x80000008

  gBoardModuleTokenSpaceGuid.PcdAcpiDebugFeatureEnable|FALSE
  gBoardModuleTokenSpaceGuid.PcdS4Enable|FALSE
  gBoardModuleTokenSpaceGuid.PcdBiosGuardBinEnable|FALSE              #BiosGuardModule.bin
  gBoardModuleTokenSpaceGuid.PcdGopConfigBin|FALSE
  gBoardModuleTokenSpaceGuid.PcdNhltBinEnable|FALSE                   #NhltIcl.bin
  gBoardModuleTokenSpaceGuid.PcdRaidDriverEfiEnable|FALSE             #RaidDriver.efi
  gBoardModuleTokenSpaceGuid.PcdRsteDriverEfiEnable|FALSE             #SataDriverRste.efi
  gBoardModuleTokenSpaceGuid.PcdNvmeEnable|FALSE
  gBoardModuleTokenSpaceGuid.PcdIntelRaidEnable|FALSE
  gBoardModuleTokenSpaceGuid.PcdTerminalEnable|FALSE
  gBoardModuleTokenSpaceGuid.PcdEcEnable|FALSE
  !if gSiPkgTokenSpaceGuid.PcdAdlLpSupport == TRUE
    gBoardModuleTokenSpaceGuid.PcdDefaultBoardId|0x13
  !else
    gBoardModuleTokenSpaceGuid.PcdDefaultBoardId|0x27
  !endif

  gBoardModuleTokenSpaceGuid.PcdFFUEnable|FALSE
  gBoardModuleTokenSpaceGuid.PcdFspBinGccBuildEnable|FALSE
  # Build scripts override the value of this PCD, update value in scripts for the change to take effect.
  gBoardModuleTokenSpaceGuid.PcdSetupEnable|TRUE
  gBoardModuleTokenSpaceGuid.PcdStartupAcmBinEnable|FALSE           #StartupAcm.bin
  #
  # LiteBios related PCDs must be moved to LiteBios board package when it is  created
  #
  gBoardModuleTokenSpaceGuid.PcdMicrocodeBinEnable|TRUE       #Microcode
  gBoardModuleTokenSpaceGuid.PcdEcEnable|TRUE
  gBoardModuleTokenSpaceGuid.PcdSipkgBinaryEnable|FALSE

  gBoardModulePkgTokenSpaceGuid.PcdPs2KbMsEnable|0x01
  gBoardModulePkgTokenSpaceGuid.PcdSuperIoPciIsaBridgeDevice|{0x00, 0x00, 0x1F, 0x00}
  gBoardModulePkgTokenSpaceGuid.PcdUart1Enable|0x01
  gBoardModulePkgTokenSpaceGuid.PcdUart1IrqMask|0x0010
  gBoardModulePkgTokenSpaceGuid.PcdUart1IoPort|0x03F8
  gBoardModulePkgTokenSpaceGuid.PcdUart1Length|0x08

  gPlatformModuleTokenSpaceGuid.PcdPlatformCmosAccessSupport|FALSE
  gPlatformModuleTokenSpaceGuid.PcdEnableSecureErase|FALSE
  gPlatformModuleTokenSpaceGuid.PcdDTbtEnable|FALSE
  gPlatformModuleTokenSpaceGuid.PcdCapsuleEnable|FALSE
  gPlatformModuleTokenSpaceGuid.PcdI2cTouchDriverEnable|FALSE
  gPlatformModuleTokenSpaceGuid.PcdPiI2cStackEnable|FALSE
  gPlatformModuleTokenSpaceGuid.PcdUsb3SerialStatusCodeEnable|FALSE
  gPlatformModuleTokenSpaceGuid.PcdUserAuthenticationEnable|FALSE
  gPlatformModuleTokenSpaceGuid.PcdPciHotplugEnable|FALSE
  gPlatformModuleTokenSpaceGuid.PcdUsbTypeCEnable|FALSE
  gPlatformModuleTokenSpaceGuid.PcdVirtualKeyboardEnable|FALSE
  gPlatformModuleTokenSpaceGuid.PcdEbcEnable|FALSE
  gPlatformModuleTokenSpaceGuid.PcdHddPasswordEnable|FALSE
  gPlatformModuleTokenSpaceGuid.PcdNetworkEnable|FALSE
  gPlatformModuleTokenSpaceGuid.PcdGigUndiEnable|FALSE
  gPlatformModuleTokenSpaceGuid.PcdMouseEnable|FALSE
  gPlatformModuleTokenSpaceGuid.PcdSinitAcmBinEnable|FALSE
  gPlatformModuleTokenSpaceGuid.PcdAcmProdBinEnable|FALSE
  gPlatformModuleTokenSpaceGuid.PcdScsiEnable|FALSE
  gPlatformModuleTokenSpaceGuid.PcdJpgEnable|FALSE
  gPlatformModuleTokenSpaceGuid.PcdUsbEnable|FALSE
  gPlatformModuleTokenSpaceGuid.PcdNetworkIp6Enable|FALSE
  gPlatformModuleTokenSpaceGuid.PcdNetworkIscsiEnable|FALSE
  gPlatformModuleTokenSpaceGuid.PcdNetworkVlanEnable|FALSE
  gPlatformModuleTokenSpaceGuid.PcdFatEnable|FALSE
  gPlatformModuleTokenSpaceGuid.PcdCryptoEnable|FALSE    # Current Smbios implementation needs this
  gPlatformModuleTokenSpaceGuid.PcdLzmaEnable|TRUE
  gPlatformModuleTokenSpaceGuid.PcdDxeCompressEnable|TRUE
  gPlatformModuleTokenSpaceGuid.PcdVtioEnable|FALSE
  gPlatformModuleTokenSpaceGuid.PcdUserIdentificationEnable|FALSE
  gPlatformModuleTokenSpaceGuid.PcdDnxSupportEnable|FALSE
  gPlatformModuleTokenSpaceGuid.PcdUsbFnEnable|FALSE
  gPlatformModuleTokenSpaceGuid.PcdH8S2113Present|FALSE
  gPlatformModuleTokenSpaceGuid.PcdNat87393Present|FALSE
  gPlatformModuleTokenSpaceGuid.PcdNct677FPresent|FALSE
  gPlatformModuleTokenSpaceGuid.PcdSkipFspTempRamInitAndExit|FALSE
  gPlatformModuleTokenSpaceGuid.PcdTdsEnable|FALSE
  gPlatformModuleTokenSpaceGuid.PcdOpalPasswordEnable|FALSE
  gPlatformModuleTokenSpaceGuid.PcdMemoryTestEnable|FALSE
  gPlatformModuleTokenSpaceGuid.PcdTpmEnable|FALSE
  gPlatformModuleTokenSpaceGuid.PcdBeepStatusCodeEnable|FALSE
  gPlatformModuleTokenSpaceGuid.PcdPostCodeStatusCodeEnable|FALSE
  gPlatformModuleTokenSpaceGuid.PcdDgrPolicyOverride|FALSE
  gPlatformModuleTokenSpaceGuid.PcdSymbolInReleaseEnable|FALSE
  gPlatformModuleTokenSpaceGuid.PcdResiliencyEnable|FALSE
  gPlatformModuleTokenSpaceGuid.PcdMeResiliencyEnable|FALSE
  gPlatformModuleTokenSpaceGuid.PcdRemotePlatformEraseSupport|FALSE
  gPlatformModuleTokenSpaceGuid.PcdExtendedBiosRegionSupport|FALSE

  ## This flag is used to initialize debug output interface.
  #  BIT0 - RAM debug interface.
  #  BIT1 - UART debug interface.
  #  BIT2 - USB debug interface.
  #  BIT3 - USB3 debug interface.
  #  BIT4 - Serial IO debug interface.
  #  BIT5 - TraceHub debug interface.
  #  BIT6 - Reserved.
  #  BIT7 - CMOS control.
  gPlatformModuleTokenSpaceGuid.PcdStatusCodeFlags|0x32
  gPlatformModuleTokenSpaceGuid.PcdRamDebugEnable|TRUE
  #
  # BIOS build switches configuration
  #
  gPlatformModuleTokenSpaceGuid.PcdDeprecatedFunctionRemove|TRUE
  gPlatformModuleTokenSpaceGuid.PcdSerialPortEnable|TRUE
  gPlatformModuleTokenSpaceGuid.PcdGmAdrAddress|(gEfiMdePkgTokenSpaceGuid.PcdPciExpressBaseAddress - 0x10000000) # 0xB0000000
  gPlatformModuleTokenSpaceGuid.PcdGttMmAddress|(gEfiMdePkgTokenSpaceGuid.PcdPciExpressBaseAddress - 0x11000000) # 0xAF000000
  #
  # PlatformTemporaryMmioAssignmentBegin
  #
  # When gEfiPeiBootInRecoveryModePpiGuid is installed, below MMIO resource would be
  # temporarily assigned to NVME/AHCI host controller after FspSiInitDone and be released at EndOfPei.
  # Please take care of platform resource assignment to avoid conflicts.
  #
  gPlatformModuleTokenSpaceGuid.PcdAhciPeiMmioBase|(gEfiMdePkgTokenSpaceGuid.PcdPciExpressBaseAddress - 0x30000000)    # 0x90000000
  gPlatformModuleTokenSpaceGuid.PcdAhciPeiMmioLimit|(gPlatformModuleTokenSpaceGuid.PcdAhciPeiMmioBase + 0x1000000)     # 0x91000000
  gPlatformModuleTokenSpaceGuid.PcdNvmeHcPeiMmioBase|(gPlatformModuleTokenSpaceGuid.PcdAhciPeiMmioLimit)               # 0x91000000
  gPlatformModuleTokenSpaceGuid.PcdNvmeHcPeiMmioLimit|(gPlatformModuleTokenSpaceGuid.PcdNvmeHcPeiMmioBase + 0x1000000) # 0x92000000

  # The bit width of data to be written to Port80 set to 16
  gEfiMdePkgTokenSpaceGuid.PcdPort80DataWidth|16

  gEfiMdeModulePkgTokenSpaceGuid.PcdFirmwareVendor|L"Intel"
  # Change PcdBootManagerMenuFile to point to UiApp
  gEfiMdeModulePkgTokenSpaceGuid.PcdBootManagerMenuFile|{ 0x8b, 0x7d, 0x9a, 0xd8, 0x16, 0xd0, 0x26, 0x4d, 0x93, 0xe3, 0xea, 0xb6, 0xb4, 0xd3, 0xb0, 0xa2 }
  # Do not support output status code to memory
  gEfiMdeModulePkgTokenSpaceGuid.PcdPeiCoreMaxPeiStackSize|0x80000
  gEfiMdeModulePkgTokenSpaceGuid.PcdMaxVariableSize|0x10000
  gEfiMdeModulePkgTokenSpaceGuid.PcdHwErrStorageSize|0x00000800
  gEfiMdeModulePkgTokenSpaceGuid.PcdMaxHardwareErrorVariableSize|0x400
  gEfiMdeModulePkgTokenSpaceGuid.PcdAriSupport|FALSE
  gEfiMdeModulePkgTokenSpaceGuid.PcdLoadModuleAtFixAddressEnable|$(TOP_MEMORY_ADDRESS)
  gEfiMdeModulePkgTokenSpaceGuid.PcdBrowserSubtitleTextColor|0x0
  gEfiMdeModulePkgTokenSpaceGuid.PcdBrowserFieldTextColor|0x01
  gEfiMdeModulePkgTokenSpaceGuid.PcdReclaimVariableSpaceAtEndOfDxe|TRUE

  #
  # Telemetry Feature PCDs
  #
  ## Major Version from EDKII.
  gTelemetryTokenSpaceGuid.PcdEDKIIVersionMajor|0xFF
  ## Minor Version from EDKII.
  gTelemetryTokenSpaceGuid.PcdEDKIIVersionMinor|0xFF
  ## Revision from EDKII.
  gTelemetryTokenSpaceGuid.PcdEDKIIVersionRevision|0xFF
  ## Build Number from EDKII.
  gTelemetryTokenSpaceGuid.PcdEDKIIVersionBuild|0xFF

# gino: need to update EDK2 >>
  #
  # Crypto Modulization
  #
  ## Crypto Modulization Feature Enabling
  gBoardModuleTokenSpaceGuid.PcdModularCryptoEnable|FALSE
  ## Crypto Modulization Scope
  #gEfiCryptoPkgTokenSpaceGuid.PcdCryptoServiceFamilyEnable.HmacMd5.Family    | FALSE
  #gEfiCryptoPkgTokenSpaceGuid.PcdCryptoServiceFamilyEnable.HmacSha1.Family   | FALSE
  #gEfiCryptoPkgTokenSpaceGuid.PcdCryptoServiceFamilyEnable.HmacSha256.Family | FALSE
  #gEfiCryptoPkgTokenSpaceGuid.PcdCryptoServiceFamilyEnable.Md4.Family        | FALSE
  #gEfiCryptoPkgTokenSpaceGuid.PcdCryptoServiceFamilyEnable.Md5.Family        | FALSE
  #gEfiCryptoPkgTokenSpaceGuid.PcdCryptoServiceFamilyEnable.Pkcs.Family       | FALSE
  #gEfiCryptoPkgTokenSpaceGuid.PcdCryptoServiceFamilyEnable.Dh.Family         | FALSE
  #gEfiCryptoPkgTokenSpaceGuid.PcdCryptoServiceFamilyEnable.Random.Family     | FALSE
  #gEfiCryptoPkgTokenSpaceGuid.PcdCryptoServiceFamilyEnable.Rsa.Family        | FALSE
  #gEfiCryptoPkgTokenSpaceGuid.PcdCryptoServiceFamilyEnable.Sha1.Family       | FALSE
  #gEfiCryptoPkgTokenSpaceGuid.PcdCryptoServiceFamilyEnable.Sha256.Family     | FALSE
  #gEfiCryptoPkgTokenSpaceGuid.PcdCryptoServiceFamilyEnable.Sha384.Family     | FALSE
  #gEfiCryptoPkgTokenSpaceGuid.PcdCryptoServiceFamilyEnable.Sha512.Family     | FALSE
  #gEfiCryptoPkgTokenSpaceGuid.PcdCryptoServiceFamilyEnable.X509.Family       | FALSE
  #gEfiCryptoPkgTokenSpaceGuid.PcdCryptoServiceFamilyEnable.Tdes.Family       | FALSE
  #gEfiCryptoPkgTokenSpaceGuid.PcdCryptoServiceFamilyEnable.Aes.Family        | FALSE
  #gEfiCryptoPkgTokenSpaceGuid.PcdCryptoServiceFamilyEnable.Arc4.Family       | FALSE
  #gEfiCryptoPkgTokenSpaceGuid.PcdCryptoServiceFamilyEnable.Sm3.Family        | FALSE
  #gEfiCryptoPkgTokenSpaceGuid.PcdCryptoServiceFamilyEnable.Hkdf.Family       | FALSE
  #gEfiCryptoPkgTokenSpaceGuid.PcdCryptoServiceFamilyEnable.Tls.Family        | FALSE
  #gEfiCryptoPkgTokenSpaceGuid.PcdCryptoServiceFamilyEnable.TlsSet.Family     | FALSE
  #gEfiCryptoPkgTokenSpaceGuid.PcdCryptoServiceFamilyEnable.TlsGet.Family     | FALSE
# gino: need to update EDK2 <<

  gNhltFeaturePkgTokenSpaceGuid.NhltConfigurationByPcdEnabled|FALSE

!if gXmlCliFeaturePkgTokenSpaceGuid.PcdXmlCliFeatureEnable == TRUE
  # Set Pcd Value to Client Platform, Refer README for more valid options
  gXmlCliFeaturePkgTokenSpaceGuid.PcdPlatformXmlCli|0x1
  # XmlCli Feature Package Token Space defining Setup Values
  gXmlCliFeaturePkgTokenSpaceGuid.PcdXmlCliSupport|0x0
  gXmlCliFeaturePkgTokenSpaceGuid.PcdPublishSetupPgPtr|0x1
  gXmlCliFeaturePkgTokenSpaceGuid.PcdEnableXmlCliLite|0x0
!endif

  ## Specifies the number of variable MTRRs reserved for OS use. The default number of
  #  MTRRs reserved for OS use is 0.
  # @Prompt Number of reserved variable MTRRs.
  gUefiCpuPkgTokenSpaceGuid.PcdCpuNumberOfReservedVariableMtrrs|0x0
  gUefiCpuPkgTokenSpaceGuid.PcdCpuSmmApSyncTimeout|10000
  gUefiCpuPkgTokenSpaceGuid.PcdCpuSmmStackSize|0x20000

  gPcAtChipsetPkgTokenSpaceGuid.PcdAcpiIoPciBusNumber|0x0
  gPcAtChipsetPkgTokenSpaceGuid.PcdAcpiIoPciDeviceNumber|0x1F
  gPcAtChipsetPkgTokenSpaceGuid.PcdAcpiIoPciFunctionNumber|0x2
  gPcAtChipsetPkgTokenSpaceGuid.PcdAcpiIoPciEnableRegisterOffset|0x44
  gPcAtChipsetPkgTokenSpaceGuid.PcdAcpiIoBarEnableMask|0x80
  gPcAtChipsetPkgTokenSpaceGuid.PcdAcpiIoPciBarRegisterOffset|0x00
  gPcAtChipsetPkgTokenSpaceGuid.PcdAcpiIoPortBaseAddress|0x1800
  gPcAtChipsetPkgTokenSpaceGuid.PcdAcpiPm1TmrOffset|0x08
  gPcAtChipsetPkgTokenSpaceGuid.PcdAcpiIoPortBaseAddressMask|0xFFFC

  gMinPlatformPkgTokenSpaceGuid.PcdFspDispatchModeUseFspPeiMain|TRUE

#[-start-191111-IB10189001-modify]#
# gino: kernel issue, this is W/A
[PcdsFixedAtBuild]
  gMinPlatformPkgTokenSpaceGuid.PcdPeiPhaseStackTop|0xA0000

[PcdsFixedAtBuild.IA32]
  gEfiMdeModulePkgTokenSpaceGuid.PcdVpdBaseAddress|0x0
#  gMinPlatformPkgTokenSpaceGuid.PcdPeiPhaseStackTop|0xA0000
#[-end-191111-IB10189001-modify]#

[PcdsFixedAtBuild.X64]

  # Default platform supported RFC 4646 languages: (American) English
  gEfiMdePkgTokenSpaceGuid.PcdUefiVariableDefaultPlatformLangCodes|"en-US"

[PcdsPatchableInModule]
#[-start-191111-IB10189001-remove]#
  # Insyde had defined this pcd
#  gEfiMdeModulePkgTokenSpaceGuid.PcdSmbiosVersion|0x0304
#[-end-191111-IB10189001-remove]#
  gEfiMdePkgTokenSpaceGuid.PcdDebugPrintErrorLevel|0x80000046

[PcdsDynamicHii.common.DEFAULT]
  gPlatformModuleTokenSpaceGuid.PcdPlatformMemoryCheck|L"MemoryCheck"|gPlatformModuleTokenSpaceGuid|0x0|0
  gPlatformModuleTokenSpaceGuid.PcdComPortAttributes0IsEnabled|L"ComAttributes"|gSetupVariableGuid|0x16|0
  gEfiMdePkgTokenSpaceGuid.PcdDefaultTerminalType|L"ComAttributes"|gSetupVariableGuid|0x13|0
  gEfiSecurityPkgTokenSpaceGuid.PcdTpm2AcpiTableRev|L"TCG2_VERSION"|gTcg2ConfigFormSetGuid|0x8|4|NV,BS
  gPlatformModuleTokenSpaceGuid.PcdFastBootEnable|L"Setup"|gSetupVariableGuid|0x0|0
  gPlatformModuleTokenSpaceGuid.PcdSystemFirmwareFmpSupportedMode|L"SysFmpMode"|gSysFwUpdateProgressGuid|0x0|0xFF|NV,BS

[PcdsDynamicHii.X64.DEFAULT]
  gEfiMdePkgTokenSpaceGuid.PcdPlatformBootTimeOut|L"Timeout"|gEfiGlobalVariableGuid|0x0|5 # Variable: L"Timeout"
  gEfiMdeModulePkgTokenSpaceGuid.PcdSetupConOutColumn|L"ConOutConfig"|gPlatformModuleTokenSpaceGuid|0x0|100
  gEfiMdeModulePkgTokenSpaceGuid.PcdSetupConOutRow|L"ConOutConfig"|gPlatformModuleTokenSpaceGuid|0x4|31
  gEfiMdePkgTokenSpaceGuid.PcdHardwareErrorRecordLevel|L"HwErrRecSupport"|gEfiGlobalVariableGuid|0x0|1 # Variable: L"HwErrRecSupport"

[PcdsDynamicDefault]
  gEfiSecurityPkgTokenSpaceGuid.PcdCRBIdleByPass|0xFF
  ## This PCD defines initial setting of TCG2 Persistent Firmware Management Flags
  # Currently enabled flags:
  # TCG2_BIOS_TPM_MANAGEMENT_FLAG_PP_REQUIRED_FOR_TURN_OFF                (BIT5)
  # TCG2_BIOS_TPM_MANAGEMENT_FLAG_PP_REQUIRED_FOR_CHANGE_EPS              (BIT6)
  # TCG2_BIOS_TPM_MANAGEMENT_FLAG_PP_REQUIRED_FOR_CHANGE_PCRS             (BIT7)
  # TCG2_BIOS_STORAGE_MANAGEMENT_FLAG_PP_REQUIRED_FOR_DISABLE_BLOCK_SID   (BIT17)
  # @Prompt Initial setting of TCG2 Persistent Firmware Management Flags
  gEfiSecurityPkgTokenSpaceGuid.PcdTcg2PhysicalPresenceFlags|0x200E0
  gEfiSecurityPkgTokenSpaceGuid.PcdTpm2CurrentIrqNum|0x00

  gEfiSecurityPkgTokenSpaceGuid.PcdSkipOpalPasswordPrompt|FALSE

  gEfiMdeModulePkgTokenSpaceGuid.PcdSrIovSupport|FALSE
  gEfiMdeModulePkgTokenSpaceGuid.PcdConOutColumn|0x0
  gEfiMdeModulePkgTokenSpaceGuid.PcdConOutRow|0x0
  gEfiMdeModulePkgTokenSpaceGuid.PcdAtaSmartEnable|TRUE
  gEfiMdeModulePkgTokenSpaceGuid.PcdConInConnectOnDemand|FALSE
  #
  #  Set video to native resolution as Windows 8 WHCK requirement.
  #
  gEfiMdeModulePkgTokenSpaceGuid.PcdVideoHorizontalResolution|0x0
  gEfiMdeModulePkgTokenSpaceGuid.PcdVideoVerticalResolution|0x0
  ## This is recover file name in PEI phase.
  #  The file must be in the root directory.
  #  The file name must be the 8.3 format.
  #  The PCD data must be in UNICODE format.
  # @Prompt Recover file name in PEI phase
  gEfiMdeModulePkgTokenSpaceGuid.PcdRecoveryFileName|L"Capsule.cap"|VOID*|0x20
  ## Default OEM Table ID for ACPI table creation, it is "EDK2    ".
  #  According to ACPI specification, this field is particularly useful when
  #  defining a definition block to distinguish definition block functions.
  #  The OEM assigns each dissimilar table a new OEM Table ID.
  #  This PCD is ignored for definition block.
  # @Prompt Default OEM Table ID for ACPI table creation.
  # default set to "ICL     ", will be patched by AcpiPlatform per CPU family
  gEfiMdeModulePkgTokenSpaceGuid.PcdAcpiDefaultOemTableId|0x20202020204C4349

  gMinPlatformPkgTokenSpaceGuid.PcdPciReservedMemAbove4GBBase|0xFFFFFFFFFFFFFFFF
  gMinPlatformPkgTokenSpaceGuid.PcdPciReservedMemAbove4GBLimit|0x0000000000000000
  gMinPlatformPkgTokenSpaceGuid.PcdPciSegmentCount|0x1

  gPlatformModuleTokenSpaceGuid.PcdTopSwapEnableSwSmi|0xFF
  gPlatformModuleTokenSpaceGuid.PcdTopSwapDisableSwSmi|0xFF
  gPlatformModuleTokenSpaceGuid.PcdPostIbbVerificationEnable|TRUE

  gSiPkgTokenSpaceGuid.PcdAcpiDefaultOemTableId|0x20202020204C4349

  gEfiSecurityPkgTokenSpaceGuid.PcdFirmwareDebuggerInitialized|FALSE
  gEfiSecurityPkgTokenSpaceGuid.PcdSkipHddPasswordPrompt|FALSE
  gEfiSecurityPkgTokenSpaceGuid.PcdSkipOpalPasswordPrompt|FALSE

  gBoardModuleTokenSpaceGuid.PcdUsbcEcPdNegotiation|TRUE


  ## The mask is used to control VTd behavior.<BR><BR>
  #  BIT0: Enable IOMMU during boot (If DMAR table is installed in DXE. If VTD_INFO_PPI is installed in PEI.)
  #  BIT1: Enable IOMMU when transfer control to OS (ExitBootService in normal boot. EndOfPEI in S3)
  # @Prompt The policy for VTd driver behavior.
  gIntelSiliconPkgTokenSpaceGuid.PcdVTdPolicyPropertyMask|0x00000000

  gPlatformModuleTokenSpaceGuid.PcdVTdDisablePeiPmrProtection|FALSE

  ## Enable PCIe Resizable BAR Capability Support.
  gEfiMdeModulePkgTokenSpaceGuid.PcdPcieResizableBarSupport|FALSE

[PcdsDynamicExDefault]
  gEfiMdeModulePkgTokenSpaceGuid.PcdS3BootScriptTablePrivateDataPtr|0x0
  gEfiMdeModulePkgTokenSpaceGuid.PcdFastPS2Detection|FALSE

  gMinPlatformPkgTokenSpaceGuid.PcdPciExpressRegionLength|0x10000000
  #
  # Some of the PCD consumed by both FSP and bootloader should be defined
  # here for bootloader to consume.
  #

  gBoardModuleTokenSpaceGuid.PcdBoardGpioTablePreMem|{0x0}
  gBoardModuleTokenSpaceGuid.PcdBoardGpioTablePreMem.GpioConfig[60].GpioPad|0x0
  gBoardModuleTokenSpaceGuid.PcdBoardGpioTable|{0x0}
  gBoardModuleTokenSpaceGuid.PcdBoardGpioTable.GpioConfig[130].GpioPad|0x0
  gBoardModuleTokenSpaceGuid.PcdBoardGpioTableEarlyPreMem|{0x0}
  gBoardModuleTokenSpaceGuid.PcdBoardGpioTableEarlyPreMem.GpioConfig[40].GpioPad|0x0
  gBoardModuleTokenSpaceGuid.PcdBoardAcpiData|{0x0}


[PcdsDynamicExVpd.common.DEFAULT]
  gBoardModuleTokenSpaceGuid.VpdPcdBoardGpioTablePreMem| * |{CODE({
    {0x0}  // terminator
  })}

  gBoardModuleTokenSpaceGuid.VpdPcdBoardGpioTable| * |{CODE({
    {0x0}  // terminator
  })}

  gBoardModuleTokenSpaceGuid.VpdPcdPcieClkUsageMap| * |{CODE(
    {0x0}
  )}

  gBoardModuleTokenSpaceGuid.VpdPcdUSB2OCMap| * |{CODE(
  { 16,
    {  USB_OC_SKIP,    // Port 1
       USB_OC_SKIP,    // Port 2
       USB_OC_SKIP,    // Port 3
       USB_OC_SKIP,    // Port 4
       USB_OC_SKIP,    // Port 5
       USB_OC_SKIP,    // Port 6
       USB_OC_SKIP,    // Port 7
       USB_OC_SKIP,    // Port 8
       USB_OC_SKIP,    // Port 9
       USB_OC_SKIP,    // Port 10
       USB_OC_SKIP,    // Port 11
       USB_OC_SKIP,    // Port 12
       USB_OC_SKIP,    // Port 13
       USB_OC_SKIP,    // Port 14
       USB_OC_SKIP,    // Port 15
       USB_OC_SKIP  }  // Port 16
  })}

  gBoardModuleTokenSpaceGuid.VpdPcdUSB3OCMap| * |{CODE(
  { 10,
    {  USB_OC_SKIP,    // Port 1
       USB_OC_SKIP,    // Port 2
       USB_OC_SKIP,    // Port 3
       USB_OC_SKIP,    // Port 4
       USB_OC_SKIP,    // Port 5
       USB_OC_SKIP,    // Port 6
       USB_OC_SKIP,    // Port 7
       USB_OC_SKIP,    // Port 8
       USB_OC_SKIP,    // Port 9
       USB_OC_SKIP  }  // Port 10
  })}

!if gSiPkgTokenSpaceGuid.PcdAdlLpSupport == TRUE
  gBoardModuleTokenSpaceGuid.VpdPcdMrcSpdData| * |{CODE(
    {0x0}
  )}

  gBoardModuleTokenSpaceGuid.VpdPcdMrcDqsMapCpu2Dram| * |{CODE(
    {0x0}
  )}

#[-start-210526-KEBIN00005-modify]#
!if $(C970_SUPPORT_ENABLE) == YES
  gBoardModuleTokenSpaceGuid.Yogac970MO1Q1R16GSpdData| * |{CODE(
    {0x0}
  )}
!endif
#[-end-210526-KEBIN00005-modify]#
#[-start-210519-KEBIN00001-modify]#
!if $(C970_SUPPORT_ENABLE) == YES
  gBoardModuleTokenSpaceGuid.Yogac970MO1Q2R16GSpdData| * |{CODE(
    {0x0}
  )}
  gBoardModuleTokenSpaceGuid.Yogac970MO1D1R8GSpdData| * |{CODE(
    {0x0}
  )}  
    gBoardModuleTokenSpaceGuid.Yogac970MO2O2R32GSpdData| * |{CODE(
    {0x0}
  )}  
  gBoardModuleTokenSpaceGuid.YogaC9VpdPcdMrcDqsMapCpu2Dram| * |{CODE(
    {0x0}
  )}
  
  gBoardModuleTokenSpaceGuid.YogaC9VpdPcdMrcDqMapCpu2Dram| * |{CODE(
    {0x0}
  )}
!endif
#[-end-210519-KEBIN00001-modify]#
#[-start-210702-YUNLEI0109-add]
!if $(C970_SUPPORT_ENABLE) == YES
    gBoardModuleTokenSpaceGuid.Yogac970Samsung16GSpdData| * |{CODE(
    {0x0}
  )}
      gBoardModuleTokenSpaceGuid.Yogac970hynix16GSpdData| * |{CODE(
    {0x0}
  )}
      gBoardModuleTokenSpaceGuid.Yogac970hynix8GSpdData| * |{CODE(
    {0x0}
  )}
!endif
#[-end-210702-YUNLEI0109-add]

#[-start-210910-QINGLIN0060-add]#
!if ($(S370_SUPPORT_ENABLE) == YES) OR ($(S570_SUPPORT_ENABLE) == YES)
  gBoardModuleTokenSpaceGuid.DDR4Samsung4GSpdData| * |{CODE(
    {0x0}
  )}
  gBoardModuleTokenSpaceGuid.DDR4Samsung8GSpdData| * |{CODE(
    {0x0}
  )}
  gBoardModuleTokenSpaceGuid.DDR4Hynix4GSpdData| * |{CODE(
    {0x0}
  )}
  gBoardModuleTokenSpaceGuid.DDR4Hynix8GSpdData| * |{CODE(
    {0x0}
  )}
  gBoardModuleTokenSpaceGuid.DDR4Micron4GSpdData| * |{CODE(
    {0x0}
  )}
  gBoardModuleTokenSpaceGuid.DDR4Micron8GSpdData| * |{CODE(
    {0x0}
  )}
  gBoardModuleTokenSpaceGuid.DDR4NullSpdData| * |{CODE(
    {0x0}
  )}
!endif
#[-start-211230-QINGLIN0140-modify]#
!if ($(S570_SUPPORT_ENABLE) == YES) OR ($(S370_SUPPORT_ENABLE) == YES)
#[-end-211230-QINGLIN0140-modify]#
  gBoardModuleTokenSpaceGuid.DDR4Micron8G2ndSpdData| * |{CODE(
    {0x0}
  )}
!endif
#[-end-210910-QINGLIN0060-add]#
#[-start-211228-QINGLIN0136-add]#
!if ($(S370_SUPPORT_ENABLE) == YES)
  gBoardModuleTokenSpaceGuid.DDR4Smart4GSpdData| * |{CODE(
    {0x0}
  )}
  gBoardModuleTokenSpaceGuid.DDR4Adata4GSpdData| * |{CODE(
    {0x0}
  )}

  gBoardModuleTokenSpaceGuid.DDR4Micron8G170SpdData| * |{CODE(
    {0x0}
  )}
  gBoardModuleTokenSpaceGuid.DDR4Smart8G170SpdData| * |{CODE(
    {0x0}
  )}  
!endif
#[-end-211228-QINGLIN0136-add]#

#[start-210720-STORM1100-modify]#
!if $(C770_SUPPORT_ENABLE) == YES

  gBoardModuleTokenSpaceGuid.VpdC770DISPcdPcieClkUsageMap| * |{CODE(
    {0x0}
  )}

  gBoardModuleTokenSpaceGuid.VpdC770UMAPcdPcieClkUsageMap| * |{CODE(
    {0x0}
  )}

  gBoardModuleTokenSpaceGuid.YogaC716VpdPcdMrcDqsMapCpu2Dram| * |{CODE(
    {0x0}
  )}

  gBoardModuleTokenSpaceGuid.YogaC714VpdPcdMrcDqsMapCpu2Dram| * |{CODE(
    {0x0}
  )}
  
  gBoardModuleTokenSpaceGuid.YogaC716VpdPcdMrcDqMapCpu2Dram| * |{CODE(
    {0x0}
  )}

  gBoardModuleTokenSpaceGuid.YogaC714VpdPcdMrcDqMapCpu2Dram| * |{CODE(
    {0x0}
  )}


#[-start-210726-YUNLEI0113-modify]
  gBoardModuleTokenSpaceGuid.Yogac970MO1Q2R16GSpdData| * |{CODE(
    {0x0}
  )}
  gBoardModuleTokenSpaceGuid.Yogac970MO1D1R8GSpdData| * |{CODE(
    {0x0}
  )}  
    gBoardModuleTokenSpaceGuid.Yogac970MO2O2R32GSpdData| * |{CODE(
    {0x0}
  )}  
    gBoardModuleTokenSpaceGuid.Yogac970Samsung16GSpdData| * |{CODE(
    {0x0}
  )}
      gBoardModuleTokenSpaceGuid.Yogac970hynix16GSpdData| * |{CODE(
    {0x0}
  )}
      gBoardModuleTokenSpaceGuid.Yogac970hynix8GSpdData| * |{CODE(
    {0x0}
  )}
  gBoardModuleTokenSpaceGuid.Yogac770Samsung32GSpdData| * |{CODE(
    {0x0}
  )}
  gBoardModuleTokenSpaceGuid.Yogac770hynix32GSpdData| * |{CODE(
    {0x0}
  )}

#[-end-210726-YUNLEI0113-modify]


!endif
# end C770_SUPPORT_ENABLE
#[end-210720-STORM1100-modify]#

#[-start-210817-DABING0002-modify]#
!if ($(S77014_SUPPORT_ENABLE) == YES) OR ($(S77014IAH_SUPPORT_ENABLE) == YES)
#[-start-210825-TAMT000002-add]#
#[-start-210927-TAMT000015-add]#
  gBoardModuleTokenSpaceGuid.VpdS77014DISPcdPcieClkUsageMap| * |{CODE(
    {0x0}
  )}

  gBoardModuleTokenSpaceGuid.VpdS77014UMAPcdPcieClkUsageMap| * |{CODE(
    {0x0}
  )}
#[-end-210927-TAMT000015-add]#
#[-start-210831-TAMT000005-add]#
  gBoardModuleTokenSpaceGuid.S77014VpdPcdMrcDqsMapCpu2Dram| * |{CODE(
    {0x0}
  )}
  
  gBoardModuleTokenSpaceGuid.S77014VpdPcdMrcDqMapCpu2Dram| * |{CODE(
    {0x0}
#[-end-210831-TAMT000005-add]#  
	
  gBoardModuleTokenSpaceGuid.Yogac970MO1Q2R16GSpdData| * |{CODE(
    {0x0}
  )}
  gBoardModuleTokenSpaceGuid.Yogac970MO1D1R8GSpdData| * |{CODE(
    {0x0}
  )}  
  gBoardModuleTokenSpaceGuid.Yogac970Samsung16GSpdData| * |{CODE(
    {0x0}
  )}
  gBoardModuleTokenSpaceGuid.Yogac970hynix16GSpdData| * |{CODE(
    {0x0}
  )}
  gBoardModuleTokenSpaceGuid.Yogac970hynix8GSpdData| * |{CODE(
#[-end-210825-TAMT000002-add]#	
    {0x0}
  )}
!endif  
#[-end-210817-DABING0002-modify]#

#[-start-210914-DABING0006-modify]#
!if $(S77013_SUPPORT_ENABLE) == YES
  gBoardModuleTokenSpaceGuid.S77013VpdPcdMrcDqsMapCpu2Dram| * |{CODE(
    {0x0}
  )}
  gBoardModuleTokenSpaceGuid.S77013VpdPcdMrcDqMapCpu2Dram| * |{CODE(
    {0x0}
	
  gBoardModuleTokenSpaceGuid.Yogac970MO1Q2R16GSpdData| * |{CODE(
    {0x0}
  )}
  gBoardModuleTokenSpaceGuid.Yogac970MO1D1R8GSpdData| * |{CODE(
    {0x0}
  )}  
  gBoardModuleTokenSpaceGuid.Yogac970Samsung16GSpdData| * |{CODE(
    {0x0}
  )}
  gBoardModuleTokenSpaceGuid.Yogac970hynix16GSpdData| * |{CODE(
    {0x0}
  )}
  gBoardModuleTokenSpaceGuid.Yogac970hynix8GSpdData| * |{CODE(
    {0x0}
  )}
!endif  
#[-end-210914-DABING0006-modify]#

  gBoardModuleTokenSpaceGuid.VpdPcdMrcDqMapCpu2Dram| * |{CODE(
    {0x0}
  )}

  gBoardModuleTokenSpaceGuid.VpdPcdCpuUsb3OcMap| * |{CODE(
  { 6,
    {  USB_OC_SKIP,    // Port 1
       USB_OC_SKIP,    // Port 2
       USB_OC_SKIP,    // Port 3
       USB_OC_SKIP,    // Port 4
       USB_OC_SKIP,    // Port 5
       USB_OC_SKIP  }  // Port 6
  })}
!endif

  gBoardModuleTokenSpaceGuid.VpdPcdPmaxDevices| * |{CODE(
  {{
      {
         "\\_SB.PC00.HDAS"  // Realtek codec device string
      },
      0xBB8,                     // D0 peak power in mW
      0x00                       // Dx peak power in mW
   },
   {
      {
         "\\_SB.PC00.LNK0"       // WF Camera string
      },
      0x32A,                     // D0 peak power in mW without accounting for flash
      0x00                       // Dx peak power in mW
   },
   {
      {
         "\\_SB.PC00.LNK1"       // UF Camera string
      },
      0x33E,                     // D0 peak power in mW without accounting for flash
      0x00                       // Dx peak power in mW
   },
   {
      {
         "\\_SB.PC00.FLM0"       // Flash device string
      },
      0x2328,                    // D0 peak power in mW
      0x00                       // Dx peak power in mW
   }
  })}

!if gSiPkgTokenSpaceGuid.PcdAdlLpSupport == TRUE
## Update in SKU dsc file like below for changing the value as per Board
  ##  [PcdsDynamicExVpd.common.SkuIdAdlMLp4Rvp]
  ##  gBoardModuleTokenSpaceGuid.PcdClwlI2cController|*|{'1'}
  ##  gBoardModuleTokenSpaceGuid.PcdClwlI2cSlaveAddress|*|{0x23}
gBoardModuleTokenSpaceGuid.PcdClwlI2cController|*|{'0'}
gBoardModuleTokenSpaceGuid.PcdClwlI2cSlaveAddress|*|{0x14}
!endif
