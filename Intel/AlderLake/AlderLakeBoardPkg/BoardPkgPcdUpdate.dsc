## @file
# 
#******************************************************************************
#* Copyright 2021 Insyde Software Corp. All Rights Reserved.
#*
#* You may not reproduce, distribute, publish, display, perform, modify, adapt,
#* transmit, broadcast, present, recite, release, license or otherwise exploit
#* any part of this publication in any form, by any means, without the prior
#* written permission of Insyde Software Corp.
#*
#******************************************************************************
## @file
#  Board description file updates configuration (PCD) settings by if-statement for this project.
#
# @copyright
#  INTEL CONFIDENTIAL
#  Copyright 2020 - 2021 Intel Corporation.
#
#  The source code contained or described herein and all documents related to the
#  source code ("Material") are owned by Intel Corporation or its suppliers or
#  licensors. Title to the Material remains with Intel Corporation or its suppliers
#  and licensors. The Material may contain trade secrets and proprietary and
#  confidential information of Intel Corporation and its suppliers and licensors,
#  and is protected by worldwide copyright and trade secret laws and treaty
#  provisions. No part of the Material may be used, copied, reproduced, modified,
#  published, uploaded, posted, transmitted, distributed, or disclosed in any way
#  without Intel's prior express written permission.
#
#  No license under any patent, copyright, trade secret or other intellectual
#  property right is granted to or conferred upon you by disclosure or delivery
#  of the Materials, either expressly, by implication, inducement, estoppel or
#  otherwise. Any license under such intellectual property rights must be
#  express and approved by Intel in writing.
#
#  Unless otherwise agreed by Intel in writing, you may not remove or alter
#  this notice or any other notice embedded in Materials by Intel or
#  Intel's suppliers or licensors in any way.
#
#  This file contains a 'Sample Driver' and is licensed as such under the terms
#  of your license agreement with Intel or your vendor. This file may be modified
#  by the user, subject to the additional terms of the license agreement.
#
# @par Specification
##

[PcdsFeatureFlag]

!if gMinPlatformPkgTokenSpaceGuid.PcdBootStage >= 1
  gMinPlatformPkgTokenSpaceGuid.PcdStopAfterDebugInit|TRUE
!endif

!if gMinPlatformPkgTokenSpaceGuid.PcdBootStage >= 2
  gMinPlatformPkgTokenSpaceGuid.PcdStopAfterDebugInit|FALSE
  gMinPlatformPkgTokenSpaceGuid.PcdStopAfterMemInit|TRUE
!endif

!if gMinPlatformPkgTokenSpaceGuid.PcdBootStage >= 3
  gMinPlatformPkgTokenSpaceGuid.PcdStopAfterMemInit|FALSE
  gMinPlatformPkgTokenSpaceGuid.PcdBootToShellOnly|TRUE
  gBoardModuleTokenSpaceGuid.PcdUefiShellEnable|TRUE
!endif

!if gMinPlatformPkgTokenSpaceGuid.PcdBootStage >= 4
  gMinPlatformPkgTokenSpaceGuid.PcdBootToShellOnly|FALSE
!endif

!if gMinPlatformPkgTokenSpaceGuid.PcdBootStage >= 5
  gBoardModuleTokenSpaceGuid.PcdSecurityEnable|TRUE
!endif

!if gMinPlatformPkgTokenSpaceGuid.PcdBootStage >= 6
  gBoardModuleTokenSpaceGuid.PcdAdvancedFeatureEnable|TRUE
!endif

!if gMinPlatformPkgTokenSpaceGuid.PcdBootStage == 7
  gBoardModuleTokenSpaceGuid.PcdOptimizationEnable|TRUE
!endif

!if gBoardModuleTokenSpaceGuid.PcdSecurityEnable == TRUE
  gMinPlatformPkgTokenSpaceGuid.PcdTpm2Enable|TRUE
  !if $(TARGET) == DEBUG
    gMinPlatformPkgTokenSpaceGuid.PcdSmiHandlerProfileEnable|TRUE
  !endif
!endif  #PcdSecurityEnable

!if gBoardModuleTokenSpaceGuid.PcdOptimizationEnable == TRUE
  gMinPlatformPkgTokenSpaceGuid.PcdPerformanceEnable|TRUE
!endif  #PcdOptimizationEnable

  gBoardModuleTokenSpaceGuid.PcdIntelGopEnable|TRUE                  #LegacyVideoRom.bin and IntelGopDriver.efi

!if gBoardModuleTokenSpaceGuid.PcdAdvancedFeatureEnable == TRUE

  gNhltFeaturePkgTokenSpaceGuid.PcdNhltFeatureEnable|TRUE



!endif

  # XmlCli: Update Pcd Value
  gXmlCliFeaturePkgTokenSpaceGuid.PcdXmlCliFeatureEnable|TRUE

[PcdsFixedAtBuild]
#
# Setting feature PCDs based on boot stages set above
#
!if gMinPlatformPkgTokenSpaceGuid.PcdBootToShellOnly == FALSE
  gSiPkgTokenSpaceGuid.PcdAcpiEnable|TRUE
  gBoardModuleTokenSpaceGuid.PcdAcpiDebugFeatureEnable|TRUE
!endif  #PcdBootToShellOnly

!if gBoardModuleTokenSpaceGuid.PcdAdvancedFeatureEnable == TRUE

  gBoardModuleTokenSpaceGuid.PcdGopConfigBin|FALSE
  gSiPkgTokenSpaceGuid.PcdPpmEnable|TRUE
  gSiPkgTokenSpaceGuid.PcdSmbiosEnable|TRUE
  gSiPkgTokenSpaceGuid.PcdS3Enable|TRUE
  !if gSiPkgTokenSpaceGuid.PcdAdlLpSupport == TRUE
    gSiPkgTokenSpaceGuid.PcdITbtEnable|TRUE
  !else
    gSiPkgTokenSpaceGuid.PcdITbtEnable|FALSE
  !endif
  gSiPkgTokenSpaceGuid.PcdBtgTxtLegacyPkgEnable|TRUE
  gSiPkgTokenSpaceGuid.PcdBootGuardEnable|TRUE
  gSiPkgTokenSpaceGuid.PcdTxtEnable|TRUE
  gSiPkgTokenSpaceGuid.PcdSiCatalogDebugEnable|FALSE

  gSiPkgTokenSpaceGuid.PcdMrcTraceMessageSupported|TRUE

  gPlatformModuleTokenSpaceGuid.PcdBeepStatusCodeEnable|FALSE
  gPlatformModuleTokenSpaceGuid.PcdPostCodeStatusCodeEnable|TRUE
  gPlatformModuleTokenSpaceGuid.PcdTraceHubPostCodeEnable|TRUE
  gSiPkgTokenSpaceGuid.PcdSourceDebugEnable|FALSE
  gSiPkgTokenSpaceGuid.PcdBiosGuardEnable|TRUE
  gSiPkgTokenSpaceGuid.PcdHgEnable|TRUE
  gBoardModuleTokenSpaceGuid.PcdS4Enable|TRUE

  gBoardModuleTokenSpaceGuid.PcdNvmeEnable|TRUE
  gBoardModuleTokenSpaceGuid.PcdIntelRaidEnable|TRUE
  gBoardModuleTokenSpaceGuid.PcdTerminalEnable|TRUE
  gSiPkgTokenSpaceGuid.PcdOverclockEnable|TRUE

  gSiPkgTokenSpaceGuid.PcdEmbeddedEnable|0x0
#[-start-200420-IB17800066-modify]#
  gSiPkgTokenSpaceGuid.PcdAmtEnable|$(AMT_ENABLE)
#[-end-200420-IB17800066-modify]#
  gSiPkgTokenSpaceGuid.PcdAtaEnable|TRUE
  gPlatformModuleTokenSpaceGuid.PcdOneClickRecoveryEnable|TRUE
  gPlatformModuleTokenSpaceGuid.PcdWifiProfileSyncEnable|FALSE

  !if gSiPkgTokenSpaceGuid.PcdAmtEnable == TRUE && gPlatformModuleTokenSpaceGuid.PcdOneClickRecoveryEnable == TRUE
    !if (gSiPkgTokenSpaceGuid.PcdAdlLpSupport == TRUE)
      gPlatformModuleTokenSpaceGuid.PcdWifiProfileSyncEnable|TRUE
    !endif
  !endif

  gPlatformModuleTokenSpaceGuid.PcdIgdIntHookEnable|TRUE
  gPlatformModuleTokenSpaceGuid.PcdAhciEnable|TRUE
  gPlatformModuleTokenSpaceGuid.PcdDTbtEnable|TRUE
#[-start-191111-IB10189001-modify]#
  #
  #  Disable Intel CapsuleEnable, this featuree was owned by Insyde Kernel
  #
  gPlatformModuleTokenSpaceGuid.PcdCapsuleEnable|FALSE
#[-end-191111-IB10189001-modify]#
  gPlatformModuleTokenSpaceGuid.PcdResiliencyEnable|FALSE
  gPlatformModuleTokenSpaceGuid.PcdMeResiliencyEnable|FALSE
  gPlatformModuleTokenSpaceGuid.PcdI2cTouchDriverEnable|TRUE
  gPlatformModuleTokenSpaceGuid.PcdPiI2cStackEnable|TRUE
  gPlatformModuleTokenSpaceGuid.PcdUsb3SerialStatusCodeEnable|FALSE
  gPlatformModuleTokenSpaceGuid.PcdUserAuthenticationEnable|TRUE

  !if (gSiPkgTokenSpaceGuid.PcdAdlLpSupport == TRUE)
    gPlatformModuleTokenSpaceGuid.PcdRemotePlatformEraseSupport|TRUE
  !endif

  gPlatformModuleTokenSpaceGuid.PcdPciHotplugEnable|TRUE
  gPlatformModuleTokenSpaceGuid.PcdUsbTypeCEnable|TRUE
  gPlatformModuleTokenSpaceGuid.PcdVirtualKeyboardEnable|TRUE
  gPlatformModuleTokenSpaceGuid.PcdEbcEnable|TRUE
  gPlatformModuleTokenSpaceGuid.PcdHddPasswordEnable|TRUE
  gPlatformModuleTokenSpaceGuid.PcdNetworkEnable|TRUE
  gPlatformModuleTokenSpaceGuid.PcdGigUndiEnable|TRUE
  gPlatformModuleTokenSpaceGuid.PcdMouseEnable|FALSE
  gPlatformModuleTokenSpaceGuid.PcdSinitAcmBinEnable|FALSE
  gPlatformModuleTokenSpaceGuid.PcdAcmProdBinEnable|FALSE
  gPlatformModuleTokenSpaceGuid.PcdScsiEnable|TRUE
  gPlatformModuleTokenSpaceGuid.PcdJpgEnable|TRUE
  gPlatformModuleTokenSpaceGuid.PcdSmiVariableEnable|TRUE
  gPlatformModuleTokenSpaceGuid.PcdUsbEnable|TRUE
  gPlatformModuleTokenSpaceGuid.PcdUsbFnEnable|TRUE
  gPlatformModuleTokenSpaceGuid.PcdDnxSupportEnable|TRUE
  gPlatformModuleTokenSpaceGuid.PcdPlatformCmosAccessSupport|TRUE
  gPlatformModuleTokenSpaceGuid.PcdEnableSecureErase|TRUE
  gPlatformModuleTokenSpaceGuid.PcdUnitoolEnable|TRUE
  gBoardModuleTokenSpaceGuid.PcdFFUEnable|TRUE

  #
  # Build Switch for bin files.
  #
  !if gSiPkgTokenSpaceGuid.PcdBiosGuardEnable == TRUE
    gBoardModuleTokenSpaceGuid.PcdBiosGuardBinEnable|TRUE            #BiosGuardModule.bin
  !else
    gBoardModuleTokenSpaceGuid.PcdBiosGuardBinEnable|FALSE
  !endif
  gBoardModuleTokenSpaceGuid.PcdStartupAcmProdBinEnable|FALSE        #StartupAcmProd.bin
  gBoardModuleTokenSpaceGuid.PcdNhltBinEnable|TRUE                   #NhltIcl.bin
  gPlatformModuleTokenSpaceGuid.PcdSredirBinEnable|TRUE              #Sredir.bin
  gPlatformModuleTokenSpaceGuid.PcdSinitAcmBinEnable|FALSE           #SinitAc.bin
  gPlatformModuleTokenSpaceGuid.PcdH8S2113Present|FALSE
  gPlatformModuleTokenSpaceGuid.PcdNat87393Present|FALSE
  gPlatformModuleTokenSpaceGuid.PcdNct677FPresent|FALSE

  #
  # Build Switch for efi files.
  #
  gBoardModuleTokenSpaceGuid.PcdRaidDriverEfiEnable|TRUE             #RaidDriver.efi
  gBoardModuleTokenSpaceGuid.PcdRsteDriverEfiEnable|TRUE             #SataDriverRste.efi
  gPlatformModuleTokenSpaceGuid.PcdLegacySredirBinEnable|TRUE        #LegacySredir.efi

  gPlatformModuleTokenSpaceGuid.PcdNetworkIp6Enable|TRUE
  gPlatformModuleTokenSpaceGuid.PcdNetworkIscsiEnable|TRUE
  gPlatformModuleTokenSpaceGuid.PcdNetworkVlanEnable|TRUE
  gPlatformModuleTokenSpaceGuid.PcdFatEnable|TRUE
  gPlatformModuleTokenSpaceGuid.PcdCryptoEnable|TRUE    # Current Smbios implementation needs this
  gPlatformModuleTokenSpaceGuid.PcdLzmaEnable|TRUE
  gPlatformModuleTokenSpaceGuid.PcdDxeCompressEnable|TRUE
  gPlatformModuleTokenSpaceGuid.PcdVtioEnable|TRUE
  gPlatformModuleTokenSpaceGuid.PcdUserIdentificationEnable|FALSE
  gPlatformModuleTokenSpaceGuid.PcdTdsEnable|TRUE
  gPlatformModuleTokenSpaceGuid.PcdOpalPasswordEnable|TRUE
  gPlatformModuleTokenSpaceGuid.PcdMemoryTestEnable|TRUE
  gPlatformModuleTokenSpaceGuid.PcdDgrPolicyOverride|FALSE
  !if (gPlatformModuleTokenSpaceGuid.PcdCryptoEnable == TRUE)
    gBoardModuleTokenSpaceGuid.PcdModularCryptoEnable|TRUE
  !endif

  gSiPkgTokenSpaceGuid.PcdOcWdtEnable|TRUE
  gSiPkgTokenSpaceGuid.PcdSerialIoUartEnable|TRUE
  gSiPkgTokenSpaceGuid.PcdIpuEnable|TRUE
  gSiPkgTokenSpaceGuid.PcdIgdEnable|TRUE
  gSiPkgTokenSpaceGuid.PcdPeiDisplayEnable|TRUE
  gSiPkgTokenSpaceGuid.PcdVtdEnable|TRUE
  gSiPkgTokenSpaceGuid.PcdCpuPowerOnConfigEnable|TRUE
  gSiPkgTokenSpaceGuid.PcdSaDmiEnable|TRUE
  gSiPkgTokenSpaceGuid.PcdGnaEnable|TRUE
  gSiPkgTokenSpaceGuid.PcdVmdEnable|TRUE
    gSiPkgTokenSpaceGuid.PcdCpuPcieEnable|TRUE
  gSiPkgTokenSpaceGuid.PcdPttEnable|TRUE
  gSiPkgTokenSpaceGuid.PcdJhiEnable|TRUE
  gSiPkgTokenSpaceGuid.PcdSmmVariableEnable|TRUE
  gSiPkgTokenSpaceGuid.PcdThcEnable|TRUE
  gSiPkgTokenSpaceGuid.PcdOptimizeCompilerEnable|TRUE
  gSiPkgTokenSpaceGuid.PcdPpmEnable|TRUE
  gSiPkgTokenSpaceGuid.PcdPsmiEnable|TRUE
  gSiPkgTokenSpaceGuid.PcdAdlLpSupport|FALSE
  gSiPkgTokenSpaceGuid.PcdSpsStateSaveEnable|TRUE
  gSiPkgTokenSpaceGuid.PcdHybridStorageSupport|TRUE
  gSiPkgTokenSpaceGuid.PcdPpamEnable|TRUE
  gSiPkgTokenSpaceGuid.PcdStatusCodeUseTraceHub|TRUE
!endif #PcdAdvancedFeatureEnable

!if $(TARGET) == RELEASE
  gEfiMdePkgTokenSpaceGuid.PcdDebugPropertyMask|0x0
  gEfiMdePkgTokenSpaceGuid.PcdReportStatusCodePropertyMask|0x3
!else
  gEfiMdePkgTokenSpaceGuid.PcdDebugPropertyMask|0x2F
  gEfiMdeModulePkgTokenSpaceGuid.PcdSerialUseHardwareFlowControl|FALSE
  gEfiMdePkgTokenSpaceGuid.PcdReportStatusCodePropertyMask|0x07
!endif
#
# Silicon feature settings
#
!if gBoardModuleTokenSpaceGuid.PcdSecurityEnable == TRUE
  gPlatformModuleTokenSpaceGuid.PcdTpmEnable|TRUE
!endif



#
# SMBIOS settings
#
# Disabling 64 bit Smbios Entrypoint Structure and populated tables
# Bit0 is to enable 32 bit and Bit1 is to enable 64 bit Smbios tables
!if gSiPkgTokenSpaceGuid.PcdSmbiosEnable == TRUE
  gEfiMdeModulePkgTokenSpaceGuid.PcdSmbiosEntryPointProvideMethod|0x01
!endif

!if gPlatformModuleTokenSpaceGuid.PcdBeepStatusCodeEnable == TRUE
  gBeepDebugFeaturePkgTokenSpaceGuid.PcdStatusCodeUseBeep|TRUE
!else
  gBeepDebugFeaturePkgTokenSpaceGuid.PcdStatusCodeUseBeep|FALSE
!endif

!if gPlatformModuleTokenSpaceGuid.PcdPostCodeStatusCodeEnable == TRUE
  gPostCodeDebugFeaturePkgTokenSpaceGuid.PcdStatusCodeUsePostCode|TRUE
!else
  gPostCodeDebugFeaturePkgTokenSpaceGuid.PcdStatusCodeUsePostCode|FALSE
!endif

!if gMinPlatformPkgTokenSpaceGuid.PcdPerformanceEnable == TRUE
  gEfiMdePkgTokenSpaceGuid.PcdPerformanceLibraryPropertyMask|0x1
  gEfiMdeModulePkgTokenSpaceGuid.PcdMaxPeiPerformanceLogEntries|140
  gEfiMdeModulePkgTokenSpaceGuid.PcdEdkiiFpdtStringRecordEnableOnly|TRUE
!endif

!if gMinPlatformPkgTokenSpaceGuid.PcdSmiHandlerProfileEnable == TRUE
  gEfiMdeModulePkgTokenSpaceGuid.PcdSmiHandlerProfilePropertyMask|0x1
!endif

!if (gIntelFsp2WrapperTokenSpaceGuid.PcdFspModeSelection == 1)
  #
  # In FSP API mode, the PciExpressBaseAddress is not necessary DynamicEx
  #
  gSiPkgTokenSpaceGuid.PcdSiPciExpressBaseAddress|gEfiMdePkgTokenSpaceGuid.PcdPciExpressBaseAddress
!endif

# Temporary RAM Size for FSP build
gIntelFsp2PkgTokenSpaceGuid.PcdTemporaryRamBase|0xFEF00000
gIntelFsp2PkgTokenSpaceGuid.PcdTemporaryRamSize|0x00100000
!if gIntelFsp2WrapperTokenSpaceGuid.PcdFspModeSelection == 1
  #
  # FSP API mode will only establish separate Heap.
  #
  gIntelFsp2PkgTokenSpaceGuid.PcdFspTemporaryRamSize|0x00020000
!else
  #
  # FSP Dispatch mode will not establish separate Stack or Heap.
  #
  gIntelFsp2PkgTokenSpaceGuid.PcdFspTemporaryRamSize|0
!endif

!if $(TARGET) == DEBUG
  !if gSiPkgTokenSpaceGuid.PcdSerialIoUartEnable == TRUE
    gSiPkgTokenSpaceGuid.PcdSerialIoUartDebugEnable|1
    gPlatformModuleTokenSpaceGuid.PcdStatusCodeUseSerialIoUart|TRUE
  !endif
!endif

!if gSiPkgTokenSpaceGuid.PcdAdlLpSupport == TRUE
  gSiPkgTokenSpaceGuid.PcdSerialIoUartNumber|0
!endif


#
# FSP Binary base address will be set in FDF basing on flash map
#
#[-start-191111-IB10189001-remove]#
# gino: kernel issue, we cannot follow RC
#  gIntelFsp2WrapperTokenSpaceGuid.PcdFsptBaseAddress|0
#  gIntelFsp2WrapperTokenSpaceGuid.PcdFspmBaseAddress|0
#[-end-191111-IB10189001-remove]#


!if gMinPlatformPkgTokenSpaceGuid.PcdUefiSecureBootEnable == TRUE
  ## Pcd for OptionRom.
  #  Image verification policy settings:
  #  ALWAYS_EXECUTE                         0x00000000
  #  NEVER_EXECUTE                          0x00000001
  #  ALLOW_EXECUTE_ON_SECURITY_VIOLATION    0x00000002
  #  DEFER_EXECUTE_ON_SECURITY_VIOLATION    0x00000003
  #  DENY_EXECUTE_ON_SECURITY_VIOLATION     0x00000004
  #  QUERY_USER_ON_SECURITY_VIOLATION       0x00000005
  gEfiSecurityPkgTokenSpaceGuid.PcdOptionRomImageVerificationPolicy|0x00000004
  gEfiSecurityPkgTokenSpaceGuid.PcdRsa2048Sha256PublicKeyBuffer|{0x8A, 0xF3, 0x87, 0x6B, 0x0F, 0xD4, 0xA3, 0x90, 0x15, 0xD7, 0x40, 0xC5, 0x3A, 0x94, 0x9B, 0xF4, 0xE0, 0x58, 0x53, 0x58, 0x87, 0x89, 0x67, 0x84, 0x60, 0xAF, 0x8E, 0xB4, 0x16, 0x1F, 0x52, 0x51}
!endif

!if gSiPkgTokenSpaceGuid.PcdS3Enable == TRUE
  ## Indicates if to shadow PEIM on S3 boot path after memory is ready.<BR><BR>
  #   TRUE  - Shadow PEIM on S3 boot path after memory is ready.<BR>
  #   FALSE - Not shadow PEIM on S3 boot path after memory is ready.<BR>
  # @Prompt Shadow Peim On S3 Boot.
  gEfiMdeModulePkgTokenSpaceGuid.PcdShadowPeimOnS3Boot|TRUE

  #
  # For RS5 (1 core, 2 thread), increase page number to avoid S3 exit fail
  #
  gEfiMdeModulePkgTokenSpaceGuid.PcdS3BootScriptRuntimeTableReservePageNumber|0x4
!endif

!if gSiPkgTokenSpaceGuid.PcdPpamEnable == TRUE
  gUefiCpuPkgTokenSpaceGuid.PcdCpuMsegSize|0x11A000
!endif

#
# PlatformTemporaryMmioAssignmentEnd
#


!if gBoardModuleTokenSpaceGuid.PcdAdvancedFeatureEnable == FALSE
  gMinPlatformPkgTokenSpaceGuid.PcdPciReservedMemBase |0x80000000
  gMinPlatformPkgTokenSpaceGuid.PcdPciReservedMemLimit|0xC0000000
!endif

!if gPlatformModuleTokenSpaceGuid.PcdCapsuleEnable == TRUE
  gPlatformModuleTokenSpaceGuid.PcdPlatformMeEDebugCapsuleImageTypeIdGuid|{GUID(gFmpDevicePlatformMonolithicGuid), GUID(gFmpDevicePlatformMeGuid)}
  gEfiMdeModulePkgTokenSpaceGuid.PcdSystemFmpCapsuleImageTypeIdGuid|{GUID(gFmpDevicePlatformMonolithicGuid), GUID(gFmpDevicePlatformBiosGuid)}
!endif

#
# Overriding this PCD to reduce the NVRAM Reclaim frequency
#
!if gPlatformModuleTokenSpaceGuid.PcdNetworkEnable == TRUE
    gEfiNetworkPkgTokenSpaceGuid.PcdMaxIScsiAttemptNumber|0x04
!endif

[PcdsFixedAtBuild.IA32]
gIntelFsp2PkgTokenSpaceGuid.PcdGlobalDataPointerAddress|0xFED00148
gIntelFsp2WrapperTokenSpaceGuid.PcdPeiMinMemSize|0x3800000

[PcdsFixedAtBuild.X64]
!if gSiPkgTokenSpaceGuid.PcdS3Enable == TRUE
  # @todo the size needs to be optimized.
  # Increase the memory size to resolve S3 exit failure due to memory resource shortage
  !if $(TARGET) == DEBUG
    gPlatformModuleTokenSpaceGuid.PcdS3AcpiReservedMemorySize|0x2600000
  !else
    gPlatformModuleTokenSpaceGuid.PcdS3AcpiReservedMemorySize|0x2600000
  !endif
!endif

!if gPlatformModuleTokenSpaceGuid.PcdNetworkEnable == TRUE
  gEfiNetworkPkgTokenSpaceGuid.PcdAllowHttpConnections|TRUE
!endif


# Update PcdsFeatureFlag acording to PcdsFixedAtBuild changes
[PcdsFeatureFlag]
!if gPlatformModuleTokenSpaceGuid.PcdCapsuleEnable == TRUE
  gEfiMdeModulePkgTokenSpaceGuid.PcdSupportUpdateCapsuleReset|TRUE
!else
  gEfiMdeModulePkgTokenSpaceGuid.PcdSupportUpdateCapsuleReset|FALSE
!endif

#[-start-200420-IB17800056-modify]#
  # ADL PO Temporary remove
!if gBoardModuleTokenSpaceGuid.PcdAcpiDebugFeatureEnable == TRUE
#  gAcpiDebugFeaturePkgTokenSpaceGuid.PcdAcpiDebugFeatureEnable|TRUE
!else
#  gAcpiDebugFeaturePkgTokenSpaceGuid.PcdAcpiDebugFeatureEnable|FALSE
!endif
#[-end-200420-IB17800056-modify]#

[PcdsDynamicHii.X64.DEFAULT]

!if gMinPlatformPkgTokenSpaceGuid.PcdPerformanceEnable == TRUE
  gEfiMdePkgTokenSpaceGuid.PcdPlatformBootTimeOut|L"Timeout"|gEfiGlobalVariableGuid|0x0|1 # Variable: L"Timeout"
!endif

[PcdsDynamicDefault]
#[-start-191111-IB10189001-remove]#
# gino: kernel issue, we cannot follow RC
#  gIntelFsp2WrapperTokenSpaceGuid.PcdFspsBaseAddress|0xFFD20000
#[-end-191111-IB10189001-remove]#
gIntelFsp2WrapperTokenSpaceGuid.PcdFspmUpdDataAddress|0x00000000
gIntelFsp2WrapperTokenSpaceGuid.PcdFspsUpdDataAddress|0x00000000

#[-start-201217-IB16560234-remove]#
# H2O use type PcdsFixedAtBuild
#!if $(TARGET) == DEBUG
#  gEfiMdeModulePkgTokenSpaceGuid.PcdStatusCodeUseSerial|TRUE
#!else
#  gEfiMdeModulePkgTokenSpaceGuid.PcdStatusCodeUseSerial|FALSE
#!endif
#[-end-201217-IB16560234-remove]#

!if gBoardModuleTokenSpaceGuid.PcdAdvancedFeatureEnable == TRUE
  gMinPlatformPkgTokenSpaceGuid.PcdPciReservedMemBase |0x80000000
  gMinPlatformPkgTokenSpaceGuid.PcdPciReservedMemLimit|0xC0000000
!endif

!if (gIntelFsp2WrapperTokenSpaceGuid.PcdFspModeSelection == 1)
  ## Specifies max supported number of Logical Processors.
  # @Prompt Configure max supported number of Logical Processorss
  gUefiCpuPkgTokenSpaceGuid.PcdCpuMaxLogicalProcessorNumber|16

  ## Specifies the AP wait loop state during POST phase.
  #  The value is defined as below.
  #  1: Place AP in the Hlt-Loop state.
  #  2: Place AP in the Mwait-Loop state.
  #  3: Place AP in the Run-Loop state.
  # @Prompt The AP wait loop state.
  gUefiCpuPkgTokenSpaceGuid.PcdCpuApLoopMode|2

  ## Specifies the AP target C-state for Mwait during POST phase.
  #  The default value 0 means C1 state.
  #  The value is defined as below.<BR><BR>
  # @Prompt The specified AP target C-state for Mwait.
  gUefiCpuPkgTokenSpaceGuid.PcdCpuApTargetCstate|0

  gSiPkgTokenSpaceGuid.PcdPciExpressRegionLength|0x10000000
!endif

!if gBoardModuleTokenSpaceGuid.PcdS4Enable == FALSE
  gEfiMdeModulePkgTokenSpaceGuid.PcdResetOnMemoryTypeInformationChange|FALSE
!else
  gEfiMdeModulePkgTokenSpaceGuid.PcdResetOnMemoryTypeInformationChange|TRUE
!endif

!if gMinPlatformPkgTokenSpaceGuid.PcdTpm2Enable == TRUE
  gEfiSecurityPkgTokenSpaceGuid.PcdTpmInstanceGuid|{0x5a, 0xf2, 0x6b, 0x28, 0xc3, 0xc2, 0x8c, 0x40, 0xb3, 0xb4, 0x25, 0xe6, 0x75, 0x8b, 0x73, 0x17}

  gEfiSecurityPkgTokenSpaceGuid.PcdTcg2HashAlgorithmBitmap|0xFFFFFFFF
  gEfiSecurityPkgTokenSpaceGuid.PcdTpm2HashMask|0x0000001F

  gEfiSecurityPkgTokenSpaceGuid.PcdTpm2InitializationPolicy|1
  gEfiSecurityPkgTokenSpaceGuid.PcdTpm2SelfTestPolicy|0
  gEfiSecurityPkgTokenSpaceGuid.PcdTpm2ScrtmPolicy|1

  gBoardModuleTokenSpaceGuid.PcdTpm2HierarchyChangeAuthPlatform|1
  gBoardModuleTokenSpaceGuid.PcdTpm2HierarchyControlPlatform|1
  gBoardModuleTokenSpaceGuid.PcdTpm2HierarchyControlEndorsement|1
  gBoardModuleTokenSpaceGuid.PcdTpm2HierarchyControlOwner|1
  gBoardModuleTokenSpaceGuid.PcdTpm2ChangeEps|0
  gBoardModuleTokenSpaceGuid.PcdTpm2ChangePps|0
  gBoardModuleTokenSpaceGuid.PcdTpm2Clear|0

  !if gSiPkgTokenSpaceGuid.PcdTxtEnable == TRUE
    ## The PCD is used to specify if Tcg2Platform module supports TXT provision.
    gBoardModuleTokenSpaceGuid.PcdTpm2TxtProvisionSupport|TRUE
  !endif

!endif

!if gPlatformModuleTokenSpaceGuid.PcdTpmEnable == TRUE
  gEfiSecurityPkgTokenSpaceGuid.PcdTpmInitializationPolicy|1
  gEfiSecurityPkgTokenSpaceGuid.PcdTpmScrtmPolicy|1
!endif

!if gPlatformModuleTokenSpaceGuid.PcdOneClickRecoveryEnable == TRUE
  ## This value is used to configure the request and response timeout when getting the recovery image from
  #  remote source. The value of 5000 will be the default 5 second timeout value
  # @Prompt HTTP Boot OS Image Request and Response Timeout
  gEfiNetworkPkgTokenSpaceGuid.PcdHttpIoTimeout|5000
!endif

[PcdsDynamicExDefault]

!if gIntelFsp2WrapperTokenSpaceGuid.PcdFspModeSelection == 0
  #
  # Include FSP PCD settings.
  #
  !include $(PLATFORM_FSP_BIN_PACKAGE)/FspPkgPcdShare.dsc
!endif

#[-start-191111-IB10189001-remove]#
# gino: kernel issue, we cannot follow RC
#
# FSP Binary base address will be set in FDF basing on flash map
#
#  gIntelFsp2WrapperTokenSpaceGuid.PcdFspsBaseAddress|0
#[-end-191111-IB10189001-remove]#

#[-start-201207-IB10181021-remove]#
# Move this line to Project.dsc for the customer use
  # !include $(PLATFORM_BOARD_PACKAGE)/SBCVpdStructurePcd/AllStructPCD.dsc
#[-end-201207-IB10181021-remove]#
