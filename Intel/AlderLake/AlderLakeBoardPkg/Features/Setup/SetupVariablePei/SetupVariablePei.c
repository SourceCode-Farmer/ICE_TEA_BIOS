/** @file
  Implementation of Setup variable initialization in PEI.

@copyright
  INTEL CONFIDENTIAL
  Copyright 2018 - 2020 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains a 'Sample Driver' and is licensed as such under the terms
  of your license agreement with Intel or your vendor. This file may be modified
  by the user, subject to the additional terms of the license agreement.

@par Specification Reference:
**/

#include <Library/BaseLib.h>
#include <Library/DebugLib.h>
#include <Library/PeiServicesLib.h>
#include <Library/SetupInitLib.h>
#include <Library/UefiVariableDefaultHobLib.h>
#include <Library/SetupDataCacheLib.h>

EFI_STATUS
EFIAPI
SetupVariableCallback (
  IN EFI_PEI_SERVICES           **PeiServices,
  IN EFI_PEI_NOTIFY_DESCRIPTOR  *NotifyDescriptor,
  IN VOID                       *Ppi
  );

static EFI_PEI_NOTIFY_DESCRIPTOR mSetupVariableNotifyList = {
  (EFI_PEI_PPI_DESCRIPTOR_NOTIFY_CALLBACK | EFI_PEI_PPI_DESCRIPTOR_TERMINATE_LIST),
  &gEfiPeiMasterBootModePpiGuid,
  SetupVariableCallback
};

/**
  PPI to be installed once default setup variables are created,
  so the variable patch table based on the board configuration
  can be applied in Board initialization.
**/
static EFI_PEI_PPI_DESCRIPTOR mPatchConfigurationDataPreMemPpi = {
  (EFI_PEI_PPI_DESCRIPTOR_PPI | EFI_PEI_PPI_DESCRIPTOR_TERMINATE_LIST),
  &gPatchConfigurationDataPreMemPpiGuid,
  NULL
};

/**
  Callback caches variables.
**/
EFI_STATUS
EFIAPI
SetupVariableCacheCallback (
  IN EFI_PEI_SERVICES           **PeiServices,
  IN EFI_PEI_NOTIFY_DESCRIPTOR  *NotifyDescriptor,
  IN VOID                       *Ppi
  );

/**
  PPI to inidicate setup variables are ready.
**/
static EFI_PEI_PPI_DESCRIPTOR mSetupVariablesReadyPpi = {
  (EFI_PEI_PPI_DESCRIPTOR_PPI | EFI_PEI_PPI_DESCRIPTOR_TERMINATE_LIST),
  &gSetupVariablesReadyPpiGuid,
  NULL
};

/**
  Notify caches setup variables on variable readiness.
**/
static EFI_PEI_NOTIFY_DESCRIPTOR mSetupVariableCacheNotifyList = {
  (EFI_PEI_PPI_DESCRIPTOR_NOTIFY_CALLBACK | EFI_PEI_PPI_DESCRIPTOR_TERMINATE_LIST),
  &gSetupVariablesReadyPpiGuid,
  SetupVariableCacheCallback
};

/**
  Callback on gEfiPeiMasterBootModePpiGuidiGuid so Setup Variables are initialized.

  @param[in] PeiServices          General purpose services available to every PEIM.
  @param[in] NotifyDescriptor     The notification structure this PEIM registered on install.
  @param[in] Ppi                  The MasterBootMode PPI.  Not used.

  @retval EFI_SUCCESS          - The function completed successfully.
  @retval EFI_NOT_FOUND        - The matched default data is not found on CreateDefaultVariableHob
  @retval EFI_OUT_OF_RESOURCES - No enough resource to create HOB on CreateDefaultVariableHob
  @retval Others               - An error occurred locating the UEFI variables.
**/
EFI_STATUS
EFIAPI
SetupVariableCallback (
  IN EFI_PEI_SERVICES           **PeiServices,
  IN EFI_PEI_NOTIFY_DESCRIPTOR  *NotifyDescriptor,
  IN VOID                       *Ppi
  )
{
  EFI_STATUS                      Status;
  EFI_BOOT_MODE                   BootMode;
  BOOLEAN                         CreateUefiVariableDefaultHob;

  DEBUG ((DEBUG_INFO, "Preparing to initialize the Setup UEFI variable...\n"));

  CreateUefiVariableDefaultHob = FALSE;
  Status = PeiServicesGetBootMode (&BootMode);
  ASSERT_EFI_ERROR (Status);
  if (!EFI_ERROR (Status)) {
    if (BootMode == BOOT_IN_RECOVERY_MODE || BootMode == BOOT_WITH_DEFAULT_SETTINGS) {
      CreateUefiVariableDefaultHob = TRUE;
      DEBUG ((DEBUG_INFO, "Creating default UEFI variable HOB due to boot mode.\n"));
    }
  }

  // @todo: Add CMOS corruption check (checksum is bad) and clear diagnostic status
  //        Hint: Need ResetBoardDefaultVariableHob () in PlatformNvRamHookLibCmos.c

  if (CreateUefiVariableDefaultHob) {
    Status = CreateDefaultVariableHob (EFI_HII_DEFAULT_CLASS_STANDARD);
    if (EFI_ERROR (Status)) {
      ASSERT_EFI_ERROR (Status);
      return Status;
    }

    // Update revision ID for the Setup variable structures
    Status = UpdateAllSetupVariableRevisions ();
    ASSERT_EFI_ERROR (Status);
  }

  if (IsSetupStructuresFound ()) {
    DEBUG ((DEBUG_INFO, "Setup structures successfully found.\n"));

    DEBUG ((DEBUG_INFO, "Dumping Setup structures:\n"));
    Status = PrintSetupStructures ();
  } else {
    Status = EFI_NOT_FOUND;
    DEBUG ((DEBUG_ERROR, "Setup structures not found at end of Setup initialization.\n"));
  }

  if (CreateUefiVariableDefaultHob) {
    PeiServicesInstallPpi (&mPatchConfigurationDataPreMemPpi);
  } else {
    PeiServicesInstallPpi (&mSetupVariablesReadyPpi);
  }

  return Status;
}

/**
  Callback caches variables.

  @param[in] PeiServices          General purpose services available to every PEIM.
  @param[in] NotifyDescriptor     The notification structure this PEIM registered on install.
  @param[in] Ppi                  The gSetupVariablesReadyPpiGuid PPI. Not used.

  @retval EFI_SUCCESS          - The function completed successfully.
  @retval Others               - An error occurred locating the UEFI variables.
**/
EFI_STATUS
EFIAPI
SetupVariableCacheCallback (
  IN EFI_PEI_SERVICES           **PeiServices,
  IN EFI_PEI_NOTIFY_DESCRIPTOR  *NotifyDescriptor,
  IN VOID                       *Ppi
  )
{
  EFI_STATUS     Status;
  DEBUG ((DEBUG_INFO, "Caching Setup Data.\n"));
  Status = SetupDataCacheInit ();
  ASSERT_EFI_ERROR (Status);

  return Status;
}

/**
  Entry point of this module.

  @param[in] FileHandle   Handle of the file being invoked.
  @param[in] PeiServices  Describes the list of possible PEI Services.

  @retval EFI_SUCCESS     The function completed successfully.

**/
EFI_STATUS
EFIAPI
SetupVariableEntryPoint (
  IN       EFI_PEI_FILE_HANDLE      FileHandle,
  IN CONST EFI_PEI_SERVICES         **PeiServices
  )
{
  PeiServicesNotifyPpi(&mSetupVariableNotifyList);

  PeiServicesNotifyPpi(&mSetupVariableCacheNotifyList);

  return EFI_SUCCESS;
}
