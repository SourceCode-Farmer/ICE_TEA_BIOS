/** @file

@copyright
  INTEL CONFIDENTIAL
  Copyright 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains a 'Sample Driver' and is licensed as such under the terms
  of your license agreement with Intel or your vendor. This file may be modified
  by the user, subject to the additional terms of the license agreement.

**/

#ifndef _PMAX_DEV_DEF_H_
#define _PMAX_DEV_DEF_H_

#define MAX_PEAK_POWER_VALUE                        0x3A98

#define DEFAULT_REALTEK_CODEC_DEVICE_STRING         "\\_SB.PC00.HDAS                "
#define DEFAULT_REALTEK_CODEC_DEVICE_D0_PEAK_POWER  0xFFFF
#define DEFAULT_REALTEK_CODEC_DEVICE_DX_PEAK_POWER  0xFFFF

#define DEFAULT_WF_CAMERA_STRING                    "\\_SB.PC00.LNK0                "
#define DEFAULT_WF_CAMERA_D0_PEAK_POWER             0xFFFF
#define DEFAULT_WF_CAMERA_DX_PEAK_POWER             0xFFFF

#define DEFAULT_UF_CAMERA_STRING                    "\\_SB.PC00.LNK1                "
#define DEFAULT_UF_CAMERA_D0_PEAK_POWER             0xFFFF
#define DEFAULT_UF_CAMERA_DX_PEAK_POWER             0xFFFF

#define DEFAULT_FLASH_DEVICE_STRING                 "\\_SB.PC00.FLM0                "
#define DEFAULT_FLASH_DEVICE_D0_PEAK_POWER          0xFFFF
#define DEFAULT_FLASH_DEVICE_DX_PEAK_POWER          0xFFFF

extern  EFI_GUID gPmaxAcpiTableStorageGuid;

#endif
