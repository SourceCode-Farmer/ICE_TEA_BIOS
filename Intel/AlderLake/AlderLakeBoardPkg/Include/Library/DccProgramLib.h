/** @file
  Header file for DccProgramLib.

@copyright
  INTEL CONFIDENTIAL
  Copyright 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains a 'Sample Driver' and is licensed as such under the terms
  of your license agreement with Intel or your vendor. This file may be modified
  by the user, subject to the additional terms of the license agreement.

@par Specification Reference:
**/
#ifndef _DCC_PROGRAM_H_
#define _DCC_PROGRAM_H_

#pragma pack(1)
typedef union {
  struct {
    //
    // Recovery mode when watchdog timer reaches the end
    // of the period without being reset.
    // 0x0: soft recovery, assert SFTRST# and maintain its
    //      output frequency
    // 0x1: hard recovery, assert HRDRST# and immediately
    //      revert to either the previous frequency or the
    //      programmed revert frequency based on revert_mode.
    //
    UINT8  RecoverMode       : 1;

    //
    // When the watchdog timer reaches the end of the
    // period without being reset, and recover_mode is set to 1:
    // 0x0: the output frequency reverts to the starting
    //      frequency of the last over-clock ramp
    // 0x1: the output frequency of FOD0 and FOD1 reverts
    //      according to the FOD divide ratio programmed in
    //      oc_hrdrst_int and oc_hrdrst_frac.
    //
    UINT8  RevertMode        : 1;

    //
    // A 0 to 1 transition will generate a reset pulse on the SFTRST# pin.
    //
    UINT8  SftrstReg         : 1;

    //
    // A 0 to 1 transition will generate a reset pulse on the HRDRST# pin.
    //
    UINT8  HrdrstReq         : 1;

    //
    // Sets the reset pulse width.
    // 0x0: 150 msec
    // 0x1: 300 msec
    // 0x2: 600 msec
    // 0x3: 1200 msec
    //
    UINT8  OcRstPulse        : 2;

    //
    // Selects pin or CSR triggering of over-clocking frequency transitions.
    //
    UINT8  OcTrigMode        : 1;

    //
    // Selects the FOD controlled by the OC engine.
    //
    UINT8  OcFodSel          : 1;
  } Bits;
  UINT8  Uint8;
} REGISTER_2C0H;

typedef union {
  struct {
    //
    // Set over-clocking mode. This bit must not be changed
    // while a frequency transition is in progress.
    // 0x0: SFT mode
    // 0x1: Target mode
    //
    UINT8  OcMode            : 1;

    //
    // Enable/disable the over-clocking block. This bit must
    // not be cleared while a frequency transition is in progress.
    // 0x0: disable
    // 0x1: enable
    //
    UINT8  OcEnable          : 1;

    //
    // Selects whether the watchdog timer is restarted after
    // the first expiry. On the first expiry, it always triggers a
    // reset according to recover_mode and revert_mode.
    // 0x0: The watchdog timer stops running after it expires.
    // 0x1: The watchdog timer is restarted after it first expires.
    //
    UINT8  WdtDouble         : 1;

    //
    // Reserved.
    //
    UINT8  Rsvd              : 5;
  } Bits;
  UINT8  Uint8;
} REGISTER_2C1H;

typedef union {
  struct {
    //
    // Trigger ramp up in all oc modes when oc_trig_mode
    // selects register control. In Target mode, set this bit if
    // the target divide ratio is smaller (faster) than the
    // current divide ratio.
    //
    UINT8  OcIncr            : 1;

    //
    // Trigger ramp down in all oc modes when oc_trig_mode
    // selects register control. In Target mode, set this bit if
    // the target divide ratio is larger (slower) than the current
    // divide ratio.
    //
    UINT8  OcDecr            : 1;

    //
    // When this bit is written to 1,oc_target_int and
    // oc_target_frac are updated according to
    // oc_shadow_sel.
    //
    UINT8  OcCopyShadow      : 1;

    //
    // Reserved.
    //
    UINT8  Rsvd3             : 1;

    //
    // Selects the OC shadow divider setting to copy to
    // oc_target_int and oc_target_frac when oc_copy_shadow is triggered.
    // 0x0: oc_shadow0_int ,oc_shadow0_frac
    // 0x1: oc_shadow1_int ,oc_shadow1_frac
    // 0x2: oc_hrdrst_int ,oc_hrdrst_frac
    // 0x3: reserved
    UINT8  OcShadowSel       : 2;

    //
    // Reserved.
    //
    UINT8  Rsvd67            : 2;
  } Bits;
  UINT8  Uint8;
} REGISTER_2CCH;

typedef union {
  struct {
    //
    // OUTx and/or OUTxb Driver disable
    // 0x0 = OUTx and/or OUTxb Driver is enabled if not disabled by other means
    // 0x1 = OUTx and/or OUTxb Driver is disabled
    //
    UINT8  OutDis            : 1;

    //
    // OUT Driver disabled state
    // Controls the state of OUTx / OUTxb when the output driver is
    // disabled.
    //
    UINT8  OutDisState       : 2;

    //
    // Output Driver OE Group Select and Global Output Enable Exclusion
    // Sets which OE group this driver is in, and if not assigned to a group,
    // can also exclude global output enables from applying to the clock.
    //
    UINT8  OutDisGroup       : 3;

    //
    // Output Driver OE Mode
    // Controls whether the output enable acts synchronously or
    // asynchronously with respect to the output divider clock. Must be set to
    // asynchronous mode when outputting SYSREF.
    //
    UINT8  OutOeMode         : 1;

    //
    // Output Driver Power Down
    // Powers down the output clock driver.
    // 0x0 = output driver is powered up
    // 0x1 = output driver is powered down
    //
    UINT8  OutPd             : 1;
  } Bits;
  UINT8 Uint8;
} REGISTER_ODRV_EN;

typedef union {
  //
  // The RC260X provide 9 bits to apply integer settings.
  //
  struct {
    UINT8  Byte0             : 8;
    UINT8  Byte1             : 1;
    UINT8  Rsvd              : 7;
  } Parts;
  UINT16  Uint16;
} REGISTER_INTEGER;

typedef union {
  //
  // The RC260X provide 28 bits to apply fraction settings.
  //
  struct {
    UINT8  Byte0             : 8;
    UINT8  Byte1             : 8;
    UINT8  Byte2             : 8;
    UINT8  Byte3             : 4;
    UINT8  Rsvd              : 4;
  } Parts;
  UINT32  Uint32;
} REGISTER_FRACTIONAL;
#pragma pack()

#define CPU_BCLK_OC_FOD                               0
#define PEG_DMI_CLK_FOD                               1
#define RENESAS_260X_1B_MODE_SET_PAGE_CMD             0xFD
#define RENESAS_260X_ADDRESS                          0x09
#define RENESAS_260X_CHIP_FREQUENCY                   10000     // 10Ghz = 10000Mhz
#define RENESAS_260X_OUTPUT_DISABLE                   BIT0

/**
  Set register page address.

  @param[in]  Page              Page address of the register.

  @retval Status

**/
EFI_STATUS
DccRc260xSetPage (
  IN UINT8 Page
  );

/**
  Read data from RC260X register.

  @param[in]  Register          Register address

  @retval Data                  Data read from RC260X register.

**/
UINT8
DccRegisterValueRead (
  IN UINT8 Register
  );

/**
  Write data to RC260X register.

  @param[in]  Register          Register address
  @param[in]  Data              Data that will be set.

**/
VOID
DccRegisterValueWrite (
  IN UINT8 Register,
  IN UINT8 Data
  );

/**
  Set divider value to shadow register0.

  @param[in]  Integer           The integer parts of divider register.
  @param[in]  Fractional        The fractional parts of divider register.

**/
VOID
SetDividerValueToShadowReg0 (
  IN REGISTER_INTEGER      Integer,
  IN REGISTER_FRACTIONAL   Fractional
  );

/**
  Set divider value to shadow register1.

  @param[in]  Integer           The integer parts of divider register.
  @param[in]  Fractional        The fractional parts of divider register.

**/
VOID
SetDividerValueToShadowReg1 (
  IN REGISTER_INTEGER      Integer,
  IN REGISTER_FRACTIONAL   Fractional
  );

/**
  Apply current divider settings to a register.

  @param[in]  OcFodSelect       Choose Fractional Output Divider that will be set.
  @param[in]  Integer           The integer parts of divider register.
  @param[in]  Fractional        The fractional parts of divider register.

**/
VOID
ApplyDividerValue (
  IN UINT8                 OcFodSelect,
  IN REGISTER_INTEGER      Integer,
  IN REGISTER_FRACTIONAL   Fractional
  );

/**
  Set divider value to divider register.

  @param[in]  Integer           The integer parts of divider register.
  @param[in]  Fractional        The fractional parts of divider register.

**/
VOID
SetDivider (
  IN REGISTER_INTEGER      Integer,
  IN REGISTER_FRACTIONAL   Fractional
  );

/**
  Get original divider setting from the shadow register0

  @param[in, out]  Integer           The integer parts of divider register.
  @param[in, out]  Fractional        The fractional parts of divider register.

**/
VOID
GetCurrentDividerSettingsFromShadowReg0 (
  IN REGISTER_INTEGER      *Integer,
  IN REGISTER_FRACTIONAL   *Fractional
  );

/**
  Get original divider setting from the shadow register1

  @param[in, out]  Integer           The integer parts of divider register.
  @param[in, out]  Fractional        The fractional parts of divider register.

**/
VOID
GetCurrentDividerSettingsFromShadowReg1 (
  IN REGISTER_INTEGER      *Integer,
  IN REGISTER_FRACTIONAL   *Fractional
  );

/**
  Get original divider setting from a register.

  @param[in]       OcFodSelect       Choose Fractional Output Divider that will be read.
  @param[in, out]  Integer           The integer parts of divider register.
  @param[in, out]  Fractional        The fractional parts of divider register.

**/
VOID
GetCurrentDividerSettings (
  IN UINT8                 OcFodSelect,
  IN REGISTER_INTEGER      *Integer,
  IN REGISTER_FRACTIONAL   *Fractional
  );

/**
  Calculate the divider value that will be set.
  DividerFreq = ChipFreq / TargetFreq.

  @param[in]       TargetFreq        Frequency of the target device that will be set.
  @param[in, out]  Integer           The integer parts of divider register.
  @param[in, out]  Fractional        The fractional parts of divider register.

**/
VOID
GetNextDividerSettings (
  IN UINT16                TargetFreq,
  IN REGISTER_INTEGER      *Integer,
  IN REGISTER_FRACTIONAL   *Fractional
  );

/**
  Program chip to adjust clock frequencey for target device.

  @param[in]  OcFodSelect       Choose Fractional Output Divider which will be modified.
  @param[in]  TargetFreq        Frequency of the target device that will be set.

**/
VOID
ProgramBclkFreq (
  IN UINT8  OcFodSelect,
  IN UINT16 TargetFreq
  );

/**
  Program Dcc chip for Cpu Bclk

  @param[in]  TargetFreq        Frequency of the target device that will be set.

**/
VOID
ProgramCpuBclkFreq (
  IN UINT16                TargetFreq
  );

/**
  Program Dcc chip for Peg/Dmi Clk

  @param[in]  TargetFreq        Frequency of the target device that will be set.

**/
VOID
ProgramPegDmiClkFreq (
  IN UINT16                TargetFreq
  );

/**
  Check if Renesas RC260X exist or not.

**/
BOOLEAN
CheckDccClkChipExist (
  VOID
  );

/**
  Disable Output Clock on all Out[X]/Out[X]b

**/
VOID
DisableExternalClockOutput (
  VOID
  );

/**
  Enable Output Clock on all Out[X]/Out[X]b

**/
VOID
EnableExternalClockOutput (
  VOID
  );
#endif

