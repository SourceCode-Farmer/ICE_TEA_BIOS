/** @file
Defines Platform BoardIds

@copyright
  INTEL CONFIDENTIAL
  Copyright 2017 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains a 'Sample Driver' and is licensed as such under the terms
  of your license agreement with Intel or your vendor. This file may be modified
  by the user, subject to the additional terms of the license agreement.

@par Specification Reference:
**/

#ifndef _PLATFORM_BOARD_ID_H_
#define _PLATFORM_BOARD_ID_H_

#define FlavorUnknown                       0x0
#define FlavorMobile                        0x1
#define FlavorDesktop                       0x2
#define FlavorWorkstation                   0x3
#define FlavorUpServer                      0x4
#define FlavorEmbedded                      0x5
#define FlavorMobileWorkstation             0x6
#define FlavorPlatformMax                   0x7

#define TypeUnknown                         0x0
#define TypeTrad                            0x1
#define TypeUltUlx                          0x2

// AlderLake TCSS PD types
#define TcssPdTypeUnknown                   0x0
#define TcssPdTypeCypress                   0x1
#define TcssPdTypeTi                        0x2

//
// AlderLake Board Id 0x00 - 0x3F
//

// BoardId Reserved for PCD Default SkuId usage
// SkuId = PcdBoardBomId << 16 | PcdBoardRev << 8 | PcdBoardId

#define BoardIdReserved                       0x00
#define SkuIdAdlSAdpSDdr5UDimm1DCrb           0x00002B
#define SkuIdAdlSAdpSDdr5UDimm1DCrbPpv        0x00002C
#define SkuIdAdlSAdpSDdr5UDimm1DCrbFab2       0x00012B
#define SkuIdAdlSAdpSDdr5UDimm1DCrbPpvFab2    0x00012C
#define SkuIdAdlSAdpSDdr5UDimm1DSr            0x000034
#define SkuIdAdlSAdpSDdr5UDimm1DSrFab2        0x000134
#define SkuIdAdlSAdpSDdr5UDimm2DCrb           0x00002E
#define SkuIdAdlSAdpSDdr5SODimmCrb            0x000030
#define SkuIdAdlSAdpSDdr5SODimmCrbBom8        0x070030
#define SkuIdAdlSAdpSDdr4UDimm2DCrb           0x000027
#define SkuIdAdlSAdpSDdr4UDimm2DCrbEv         0x000028
#define SkuIdAdlSAdpSDdr4UDimm2DCrbCpv        0x000029
#define SkuIdAdlSAdpSDdr4UDimm2DCrbFab2       0x000127
#define SkuIdAdlSAdpSDdr4UDimm2DCrbEvFab2     0x000128
#define SkuIdAdlSAdpSDdr4UDimm2DCrbCpvFab2    0x000129
#define SkuIdAdlSAdpSDdr4SODimmCrb            0x000035
#define SkuIdAdlSAdpSDdr5UDimm1DAep           0x000033
#define SkuIdAdlSAdpSDdr5UDimm1DAepFab2       0x000133
#define SkuIdAdlSAdpSDdr5UDimm1DOc            0x000036
#define SkuIdAdlSAdpSSbgaDdr5SODimmErb        0x00003A
#define SkuIdAdlSAdpSSbgaDdr5SODimmCrb        0x00003B
#define SkuIdAdlSAdpSSbgaDdr4SODimmCrb        0x00003E
#define SkuIdAdlSAdpSSbgaDdr5SODimmAep        0x00002F
#define SkuIdAdlPLp4Rvp                       0x000010
#define SkuIdAdlPDdr5Rvp                      0x000012
#define SkuIdAdlPDdr4Rvp                      0x000014
#define SkuIdAdlPLp5Rvp                       0x000013
#define SkuIdAdlPT3Lp5Rvp                     0x000015
#define SkuIdAdlPDdr5MRRvp                    0x000016
#define SkuIdAdlPSimics                       0x00003F
#define SkuIdAdlPLp4Bep                       0x000019
#define SkuIdAdlPLp5Aep                       0x00001A
#define SkuIdAdlPLp5Gcs                       0x00001B
#define SkuIdAdlPMMAep                        0x00001C
#define SkuIdAdlPDdr5Dg384Aep                 0x00001E
#define SkuIdAdlPLp5Dg128Aep                  0x00001F
#define SkuIdAdlPLp5MbAep                     0x00001D
#define SkuIdAdlMLp4Rvp                       0x000001
#define SkuIdAdlMLp5Rvp                       0x000002
#define SkuIdAdlMLp5PmicRvp                   0x000003
#define SkuIdAdlMLp5Rvp2a                     0x000004
#define SkuIdAdlMLp5Rvp2aPpv                  0x020004
#define SkuIdAdlMLp5Aep                       0x00000F


#define AdlSSkuType      1
#define AdlPSkuType      2
#define AdlMSkuType      3
#define AdlSimicsSkuType 4

// AlderLake P Board IDs
#define BoardIdAdlPSimics                   0x3F
#define BoardIdAdlPLp4Rvp                   0x10
#define BoardIdAdlPLp5Rvp                   0x13
#define BoardIdAdlPT3Lp5Rvp                 0x15
#define BoardIdAdlPLp4Bep                   0x19
#define BoardIdAdlPLp5Aep                   0x1A
#define BoardIdAdlPLp5Gcs                   0x1B
#define BoardIdAdlPMMAep                    0x1C
#define BoardIdAdlPDdr5Rvp                  0x12
#define BoardIdAdlPDdr4Rvp                  0x14
#define BoardIdAdlPDdr5MRRvp                0x16
#define BoardIdAdlPDdr5Dg384Aep             0x1E
#define BoardIdAdlPLp5Dg128Aep              0x1F
#define BoardIdAdlPLp5MbAep                 0x1D

#define DisplayIdAdlpmDualEdp               0x0
#define DisplayIdAdlpmMipiEdp               BIT0
#define DisplayIdAdlpmEdpMipi               BIT1
#define DisplayIdAdlpmDualMipi              (BIT0 | BIT1)
#define DisplayIdAdlpmEdpExtDisp            BIT2
#define DisplayIdAdlpmMipiExtDisp           (BIT0 | BIT2)

// AlderLake S Board IDs
#define BoardIdAdlSTgpHDdr4SODimm1DErb2     0x20
#define BoardIdAdlSTgpHDdr4SODimm1DCrb      0x21
#define BoardIdAdlSTgpHDdr4SODimm1DCrbPpv   0x22
#define BoardIdAdlSTgpHDdr5UDimm2DCrb       0x24
#define BoardIdAdlSAdpSDdr4UDimm2DErb1      0x26
#define BoardIdAdlSAdpSDdr4UDimm2DCrb       0x27
#define BoardIdAdlSAdpSDdr4UDimm2DCrbEv     0x28
#define BoardIdAdlSAdpSDdr4UDimm2DCrbCpv    0x29
#define   FabIdAdlSAdpSDdr4UDimm2DCrbRev1   0x01
#define BoardIdAdlSAdpSDdr5UDimm1DCrb       0x2B
#define BoardIdAdlSAdpSDdr5UDimm1DCrbPpv    0x2C
#define   FabIdAdlSAdpSDdr5UDimm1DCrbRev1   0x01
#define BoardIdAdlSAdpSDdr5UDimm1DSr        0x34
#define BoardIdAdlSAdpSDdr5UDimm2DCrb       0x2E

#define BoardIdAdlSAdpSDdr5SODimmCrb        0x30
#define BoardIdAdlSAdpSDdr4SODimmCrb        0x35
#define BoardIdAdlSAdpSDdr5UDimm1DAep       0x33
#define BoardIdAdlSAdpSDdr5UDimm1DOc        0x36

#define BoardIdAdlSAdpSSbgaDdr5SODimmErb    0x3A
#define BoardIdAdlSAdpSSbgaDdr5SODimmCrb    0x3B
#define BoardIdAdlSAdpSSbgaDdr5SODimmCrbPpv 0x3C
#define BoardIdAdlSAdpSSbgaDdr4SODimmCrb    0x3E

#define BoardIdAdlSAdpSSbgaDdr5SODimmAep    0x2F

// AlderLake-M Boards
#define BoardIdAdlMLp4Rvp                   0x01
#define BoardIdAdlMLp5Rvp                   0x02
#define BoardIdAdlMLp5PmicRvp               0x03
#define BoardIdAdlMLp5Rvp2a                 0x04
#define BoardIdAdlMLp5Aep                   0x0F

#define BomIdAdlMLp5PmicRvp                 0x02

// AlderLake-N Boards


#define BoardIdUnknown1                     0xffff

#endif // _PLATFORM_BOARD_ID_H_
