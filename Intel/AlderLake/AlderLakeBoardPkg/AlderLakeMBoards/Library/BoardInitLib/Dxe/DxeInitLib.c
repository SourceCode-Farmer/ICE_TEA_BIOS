/** @file

@copyright
  INTEL CONFIDENTIAL
  Copyright 2020 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains a 'Sample Driver' and is licensed as such under the terms
  of your license agreement with Intel or your vendor. This file may be modified
  by the user, subject to the additional terms of the license agreement.

@par Specification Reference:
**/

#include <Uefi.h>

#include <Library/HobLib.h>
#include <Library/PcdLib.h>
#include <Library/UefiBootServicesTableLib.h>
#include <Library/DxeUpdatePlatformInfoLib.h>
#include <Library/SetupInitLib.h>
#include <MemInfoHob.h>
#include <PlatformBoardConfig.h>
#include <PlatformBoardId.h>
#include <Features/UsbTypeC/UsbTypeCDxe/UsbTypeCDxe.h>
#include <Pins/GpioPinsVer2Lp.h>
#include <PcieRegs.h>
#include <Register/PchRegs.h>
#include <Library/PciSegmentLib.h>
#include <Library/MemoryAllocationLib.h>
#include <Library/BoardConfigLib.h>
#include <Library/PreSiliconEnvDetectLib.h>

#include <AlderLakeMRvpBoardConfigPatchTable.h>

/**
   Retimer Platform Specific Data
**/

STATIC RETIMER_PLATFORM_DATA                       mRetimerPlatformDataTable[MAX_TBT_RETIMER_DEVICE];

STATIC TBT_PD_RETIMER_PLATFORM_DATA                mRetimerPlatformDataTableAdlMTwoTbtPdRetimer[] =
{
  { TbtPdRetimerType, TRUE, 0, 0, 0, 0x00, 0x00, 0x00 },
  { TbtPdRetimerType, TRUE, 1, 0, 0, 0x00, 0x00, 0x00 }
};


EFI_STATUS
PatchConfigurationDataInit (
  IN CONFIG_PATCH_STRUCTURE  *ConfigPatchStruct,
  IN UINTN                   ConfigPatchStructSize
  );

VOID
AdlMInitPatchConfigurationData (
  VOID
  )
{
  UINT16    BoardId;
  UINTN     ConfigPatchStructSize;
  CONFIG_PATCH_STRUCTURE *PatchTable;
  UINTN                  PatchTableSize;

  BoardId = PcdGet16(PcdBoardId);
  PatchTable     = NULL;
  PatchTableSize = 0;

  switch (BoardId) {
    case BoardIdAdlMLp4Rvp:
    case BoardIdAdlMLp5Rvp:
    case BoardIdAdlMLp5Rvp2a:
      ConfigPatchStructSize = SIZE_OF_TABLE (mAlderLakeMLp4RvpConfigPatchStruct, CONFIG_PATCH_STRUCTURE);
      UpdatePatchTable (&PatchTable, &PatchTableSize, mAlderLakeMLp4RvpConfigPatchStruct, ConfigPatchStructSize);
      ConfigPatchStructSize = SIZE_OF_TABLE (mAlderLakeMAuxOverrideEnableConfigPatchStruct, CONFIG_PATCH_STRUCTURE);
      UpdatePatchTable (&PatchTable, &PatchTableSize, mAlderLakeMAuxOverrideEnableConfigPatchStruct, ConfigPatchStructSize);
      ConfigPatchStructSize = SIZE_OF_TABLE (mAlderLakeMWoVEnableConfigPatchStruct, CONFIG_PATCH_STRUCTURE);
      UpdatePatchTable (&PatchTable, &PatchTableSize, mAlderLakeMWoVEnableConfigPatchStruct, ConfigPatchStructSize);
      break;
    case BoardIdAdlMLp5Aep:
      ConfigPatchStructSize = SIZE_OF_TABLE (mAlderLakeMLp4RvpConfigPatchStruct, CONFIG_PATCH_STRUCTURE);
      UpdatePatchTable (&PatchTable, &PatchTableSize, mAlderLakeMLp4RvpConfigPatchStruct, ConfigPatchStructSize);
      break;
    case BoardIdAdlMLp5PmicRvp:
      ConfigPatchStructSize = SIZE_OF_TABLE (mAlderLakeMLp5PmicRvpConfigPatchStruct, CONFIG_PATCH_STRUCTURE);
      UpdatePatchTable (&PatchTable, &PatchTableSize, mAlderLakeMLp5PmicRvpConfigPatchStruct, ConfigPatchStructSize);
      ConfigPatchStructSize = SIZE_OF_TABLE (mAlderLakeMWoVEnableConfigPatchStruct, CONFIG_PATCH_STRUCTURE);
      UpdatePatchTable (&PatchTable, &PatchTableSize, mAlderLakeMWoVEnableConfigPatchStruct, ConfigPatchStructSize);
      break;
    default:
      break;
  }

  //
  //

  if (IsSimicsEnvironment ()) {
    ConfigPatchStructSize = SIZE_OF_TABLE (mAlderlakeMSimicsConfigPatchStruct, CONFIG_PATCH_STRUCTURE);
    UpdatePatchTable (&PatchTable, &PatchTableSize, mAlderlakeMSimicsConfigPatchStruct, ConfigPatchStructSize);
  }
  if (PchStepping () >= PCH_A1) {
    ConfigPatchStructSize = SIZE_OF_TABLE (mAlderLakeMPchSteppingConfigPatchStruct, CONFIG_PATCH_STRUCTURE);
    UpdatePatchTable (&PatchTable, &PatchTableSize, mAlderLakeMPchSteppingConfigPatchStruct, ConfigPatchStructSize);
  }
  PatchConfigurationDataInit (PatchTable, PatchTableSize);
  FreePatchTable (&PatchTable, &PatchTableSize);
}

VOID
AdlMInitCommonPlatformPcd (
  VOID
  )
{
  UINT16    BoardId;
  BoardId = PcdGet16 (PcdBoardId);

  PcdSet32S (PcdEcSmiGpio, GPIO_VER2_LP_GPP_E7);
  PcdSetBoolS (PcdPssReadSN, TRUE);
  PcdSet8S (PcdPssI2cSlaveAddress, 0x6E);
  PcdSet8S (PcdPssI2cBusNumber, 0x05);
  PcdSetBoolS (PcdSpdAddressOverride, FALSE);
  switch (BoardId) {
    case BoardIdAdlMLp4Rvp:
       PcdSet32S (PcdEcLowPowerExitGpio, GPIO_VER2_LP_GPP_A7);
       break;
    case BoardIdAdlMLp5Rvp:
    case BoardIdAdlMLp5Rvp2a:
    case BoardIdAdlMLp5PmicRvp:
       PcdSet32S (PcdEcLowPowerExitGpio, GPIO_VER2_LP_GPP_C2);
       break;
    default:
       break;
  }

}

STATIC
EFI_STATUS
BoardHookPlatformSetup (
  VOID * Content
  )
{
  ((SETUP_VOLATILE_DATA *)Content)->PlatId = PcdGet16 (PcdBoardId);

  return RETURN_SUCCESS;
}

/**
 Init Misc Platform Board Config Block.

 @param[in]  VOID

 @retval VOID
**/
VOID
AdlMBoardMiscInit (
  VOID
)
{
  PcdSet64S (PcdFuncBoardHookPlatformSetupOverride, (UINT64) (UINTN) BoardHookPlatformSetup);
}

VOID
AdlMBoardSmbiosInit (
  VOID
  )
{
  PcdSet64S (PcdSmbiosFabBoardName, (UINTN) PcdGetPtr (PcdBoardName));
  //
  // @todo : Update Slot Entry Table for all the supported boards using below PCD.
  //  This PCD follows SYSTEM_SLOT_ENTRY structure in \Include\SmbiosPlatformInfoDefinition.h
  //PcdSet64S (PcdSmbiosMainSlotEntry, NULL);
}

VOID
AdlMUpdateDimmPopulation (
  VOID
  )
{
  MEMORY_INFO_DATA_HOB    *MemInfo;
  UINT8                   Slot0;
  UINT8                   Slot1;
  UINT8                   Slot2;
  UINT8                   Slot3;
  CONTROLLER_INFO         *ControllerInfo;
  EFI_HOB_GUID_TYPE       *GuidHob;

  GuidHob = NULL;
  MemInfo = NULL;
  GuidHob = GetFirstGuidHob (&gSiMemoryInfoDataGuid);
  ASSERT (GuidHob != NULL);
  if (GuidHob != NULL) {
    MemInfo = (MEMORY_INFO_DATA_HOB *) GET_GUID_HOB_DATA (GuidHob);
  }
  if (MemInfo != NULL) {
    if ( PcdGet8 (PcdPlatformFlavor) == FlavorDesktop ||
         PcdGet8 (PcdPlatformFlavor) == FlavorUpServer ||
         PcdGet8 (PcdPlatformFlavor) == FlavorWorkstation
        ) {
        ControllerInfo = &MemInfo->Controller[0];
        Slot0 = ControllerInfo->ChannelInfo[0].DimmInfo[0].Status;
        Slot1 = ControllerInfo->ChannelInfo[0].DimmInfo[1].Status;
        Slot2 = ControllerInfo->ChannelInfo[1].DimmInfo[0].Status;
        Slot3 = ControllerInfo->ChannelInfo[1].DimmInfo[1].Status;

      //
      // Channel 0          Channel 1
      // Slot0   Slot1      Slot0   Slot1      - Population            AIO board
      // 0          0          0          0          - Invalid        - Invalid
      // 0          0          0          1          - Valid          - Invalid
      // 0          0          1          0          - Invalid        - Valid
      // 0          0          1          1          - Valid          - Valid
      // 0          1          0          0          - Valid          - Invalid
      // 0          1          0          1          - Valid          - Invalid
      // 0          1          1          0          - Invalid        - Invalid
      // 0          1          1          1          - Valid          - Invalid
      // 1          0          0          0          - Invalid        - Valid
      // 1          0          0          1          - Invalid        - Invalid
      // 1          0          1          0          - Invalid        - Valid
      // 1          0          1          1          - Invalid        - Valid
      // 1          1          0          0          - Valid          - Valid
      // 1          1          0          1          - Valid          - Invalid
      // 1          1          1          0          - Invalid        - Valid
      // 1          1          1          1          - Valid          - Valid
      //
      if ((Slot0 && (Slot1 == 0)) || (Slot2 && (Slot3 == 0))) {
        PcdSetBoolS (PcdDimmPopulationError, TRUE);
      }
    }
  }
}

/**
  Enable Tier2 GPIO Sci wake capability.

  @retval EFI_SUCCESS   The function completed successfully.
**/
EFI_STATUS
AdlMTier2GpioWakeSupport (
  VOID
  )
{
  BOOLEAN Tier2GpioWakeEnable;

  Tier2GpioWakeEnable = FALSE;

  //
  // Root port #03: M.2 WLAN
  //
  if (IsPcieEndPointPresent (2)) {
   Tier2GpioWakeEnable = TRUE;
  }
  PcdSetBoolS (PcdGpioTier2WakeEnable, Tier2GpioWakeEnable);

  return EFI_SUCCESS;
}

/**
  Copy Tcss Retimer Platform Data.

  @param[in|out]  DestinationTableCurrentIndex   The pointer to the destination buffer Current Index.
  @param[in]      SourceTable                    The pointer to the source buffer of the memory copy.
  @param[in]      SourceTableSize                The number of bytes to copy from SourceBuffer to DestinationBuffer.
  @param[in]      SourceTableType                Source Table Buffer Type

  @retval     EFI_SUCCESS            The function completed successfully.
  @retval     EFI_INVALID_PARAMETER  Invalid Parameter Passed.
**/

EFI_STATUS
AdlMCopyTcssRetimerPlatformData (
  IN OUT UINT32                               *DestinationTableCurrentIndex,
  IN  VOID                                    *SourceTable,
  IN  UINT32                                  SourceTableSize,
  IN  RETIMER_TYPE                            SourceTableType
  )
{
  UINT32                                      TableIndex;
  UINT32                                      SourceTableElementSize;
  UINT32                                      SourceTableElementCount;

  SourceTableElementSize = 0;

  if (DestinationTableCurrentIndex == NULL) {
    return EFI_INVALID_PARAMETER;
  }

  if (SourceTable == NULL) {
    return EFI_INVALID_PARAMETER;
  }

  if (SourceTableSize == 0) {
    return EFI_INVALID_PARAMETER;
  }

  if ((*DestinationTableCurrentIndex) >= MAX_TBT_RETIMER_DEVICE) {
    // Return EFI_INVALID_PARAMETER as Invalid Destination Table Current Index is passed
    return EFI_INVALID_PARAMETER;
  }
  if (SourceTableType == TbtPdRetimerType) {
    SourceTableElementSize = sizeof (TBT_PD_RETIMER_PLATFORM_DATA);
  } else if (SourceTableType == NonTbtI2cRetimerType) {
    SourceTableElementSize = sizeof (NON_TBT_I2C_RETIMER_PLATFORM_DATA);
  } else {
    // Return EFI_INVALID_PARAMETER as Invalid Source Table Type is passed
    return EFI_INVALID_PARAMETER;
  }

  SourceTableElementCount = SourceTableSize / SourceTableElementSize;

  if (((*DestinationTableCurrentIndex) + SourceTableElementCount) > MAX_TBT_RETIMER_DEVICE) {
    // Return EFI_INVALID_PARAMETER as Destination Buffer Size is small and Array boundary condition will be reached
    return EFI_INVALID_PARAMETER;
  }

  for (TableIndex = 0; TableIndex < SourceTableElementCount; TableIndex++) {
    if (((RETIMER_PLATFORM_DATA*)SourceTable)->RetimerType == SourceTableType) { // Only add entry to Dastination Table if Source Table Type Match
      gBS->CopyMem (&mRetimerPlatformDataTable[(*DestinationTableCurrentIndex)], SourceTable, SourceTableElementSize);
      *DestinationTableCurrentIndex += 1;
    }
    SourceTable = (VOID*) ((UINTN)SourceTable + SourceTableElementSize);
  }

  return EFI_SUCCESS;
}

/**
  Init Tcss Retimer Platform Data Pcd.

  @param[in]  BoardId           An unsigned integrer represent the board id.

  @retval     EFI_SUCCESS       The function completed successfully.
**/

EFI_STATUS
AdlMInitTcssRetimerPlatformDataPcd (
  VOID
)
{
  UINT32                               RetimerDataTableIndex;
  EFI_STATUS                           Status = EFI_SUCCESS;

  DEBUG((DEBUG_INFO, "AdlMInitTcssRetimerPlatformDataPcd \n"));
  //
  // Map Retimer and PD Controller Available on Board
  //
  RetimerDataTableIndex = 0; // Initialize Destination Table Index to 0 to Start from Begining
  Status = AdlMCopyTcssRetimerPlatformData (
             &RetimerDataTableIndex,
             (VOID *) mRetimerPlatformDataTableAdlMTwoTbtPdRetimer,
             sizeof(mRetimerPlatformDataTableAdlMTwoTbtPdRetimer),
             TbtPdRetimerType
           );

  if (EFI_ERROR (Status)) {
    DEBUG((DEBUG_ERROR, "mRetimerPlatformDataTableAdlMTwoTbtPdRetimer fail. \n"));
  }

  if ( !EFI_ERROR (Status)) {
    // Saving Retimer Platform Data Table into PCD
    PcdSet64S(PcdBoardRetimerDataTablePtr, (UINTN)mRetimerPlatformDataTable);
    PcdSet32S(PcdBoardRetimerDataTableEntryCount, RetimerDataTableIndex);
  }

  return Status;
}

/**
  A hook for board-specific initialization after PCI enumeration.

  @retval EFI_SUCCESS   The board initialization was successful.
**/
EFI_STATUS
EFIAPI
AdlMBoardInitAfterPciEnumeration (
  VOID
  )
{
  DEBUG ((DEBUG_INFO, "AdlMBoardInitAfterPciEnumeration\n"));

  AdlMInitPatchConfigurationData ();

  AdlMInitTcssRetimerPlatformDataPcd ();

  AdlMTier2GpioWakeSupport ();

  AdlMInitCommonPlatformPcd ();

  AdlMBoardMiscInit();

  AdlMBoardSmbiosInit ();

  UpdatePlatformInfo ();

  return EFI_SUCCESS;
}

/**
  A hook for board-specific functionality for the ReadyToBoot event.

  @retval EFI_SUCCESS   The board initialization was successful.
**/
EFI_STATUS
EFIAPI
AdlMBoardInitReadyToBoot (
  VOID
  )
{
  DEBUG ((DEBUG_INFO, "AdlMBoardInitReadyToBoot\n"));

  AdlMUpdateDimmPopulation ();

  return EFI_SUCCESS;
}

/**
  A hook for board-specific functionality for the ExitBootServices event.

  @retval EFI_SUCCESS   The board initialization was successful.
**/
EFI_STATUS
EFIAPI
AdlMBoardInitEndOfFirmware (
  VOID
  )
{
  DEBUG ((DEBUG_INFO, "AdlMBoardInitEndOfFirmware\n"));

  return EFI_SUCCESS;
}
