/** @file
  GPIO definition table for AlderLake M

@copyright
  Copyright (c) 2020 - 2021 Intel Corporation. All rights reserved
  This software and associated documentation (if any) is furnished
  under a license and may only be used or copied in accordance
  with the terms of the license. Except as permitted by such
  license, no part of this software or documentation may be
  reproduced, stored in a retrieval system, or transmitted in any
  form or by any means without the express written consent of
  Intel Corporation.
  This file contains an 'Intel Peripheral Driver' and is
  licensed for Intel CPUs and chipsets under the terms of your
  license agreement with Intel or your vendor. This file may
  be modified by the user, subject to additional terms of the
  license agreement.
**/
#ifndef _ALDERLAKE_M_GPIO_TABLE_H_
#define _ALDERLAKE_M_GPIO_TABLE_H_

#include <Pins/GpioPinsVer2Lp.h>
#include <Library/GpioLib.h>
#include <Library/GpioConfig.h>

///
/// !!! GPIOs designated to Native Functions shall not be configured by Platform Code.
/// Native Pins shall be configured by Silicon Code (based on BIOS policies setting) or soft straps(set by CSME in FITc).
/// Configuring pins to native function in GPIO table would override the pin settings resulting in unexpected behavior.
///

GLOBAL_REMOVE_IF_UNREFERENCED GPIO_INIT_CONFIG mAdlMTouchPanel1GpioTable[] =
{

 // I2C Touch pannel
  {GPIO_VER2_LP_GPP_F7,  {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,    GpioOutHigh,    GpioIntDis,GpioPlatformReset,  GpioTermNone,  GpioOutputStateUnlock}},  //TCH_PNL_PWR_EN
  {GPIO_VER2_LP_GPP_F17, {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,    GpioOutHigh,    GpioIntDis,GpioPlatformReset,  GpioTermNone, GpioOutputStateUnlock}},  //TCH_PNL2_RST_N
  {GPIO_VER2_LP_GPP_F18, {GpioPadModeGpio, GpioHostOwnGpio, GpioDirInInv,  GpioOutDefault, GpioIntEdge|GpioIntApic,GpioPlatformReset,  GpioTermNone,  GpioPadConfigUnlock  }},  //THC1_SPI2_INT_N_TCH_PNL2

};

GLOBAL_REMOVE_IF_UNREFERENCED GPIO_INIT_CONFIG mAdlMCvfGpioTable[] =
{
  {GPIO_VER2_LP_GPP_B23, {GpioPadModeGpio, GpioHostOwnGpio, GpioDirIn,   GpioOutHigh,  GpioIntEdge, GpioPlatformReset,  GpioTermNone, GpioPadConfigUnlock}},  //CVF_HOST_IRQ
  {GPIO_VER2_LP_GPP_R5,  {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,  GpioOutLow,   GpioIntDis,  GpioPlatformReset,  GpioTermNone, GpioPadConfigUnlock}},  //CVF_RST_N
  {GPIO_VER2_LP_GPP_B14, {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,  GpioOutLow,  GpioIntDis,  GpioPlatformReset,  GpioTermNone, GpioPadConfigUnlock}},  //CVF_HOST_WAKE
};

GLOBAL_REMOVE_IF_UNREFERENCED GPIO_INIT_CONFIG mAdlMLp5AepTouchPanel1GpioTable[] =
{
  // Touch Panel 1
  { GPIO_VER2_LP_GPP_E6, { GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,     GpioOutHigh,     GpioIntDis,                GpioPlatformReset,  GpioTermDefault } },  //THC0_SPI1_RST_N
  { GPIO_VER2_LP_GPP_E17,{ GpioPadModeGpio, GpioHostOwnGpio, GpioDirInInv,   GpioOutDefault,  GpioIntEdge | GpioIntApic, GpioPlatformReset,  GpioTermDefault } },  //THC0_SPI1_INTB
};

GLOBAL_REMOVE_IF_UNREFERENCED GPIO_INIT_CONFIG mAdlMTypeCAuxOverrideEnableGpioTable[] =
{
  // TypeC BIAS : Not used by default in RVP Platfrom Code. Used for Type-C Aux Override enable case.
  {GPIO_VER2_LP_GPP_E22, {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutLow,    GpioIntDis, GpioPlatformReset,  GpioTermNone}},  //DISP_AUX_P_BIAS_GPIO
  {GPIO_VER2_LP_GPP_E23, {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutHigh,   GpioIntDis, GpioPlatformReset,  GpioTermNone}},  //DISP_AUX_N_BIAS_GPIO
};

GLOBAL_REMOVE_IF_UNREFERENCED GPIO_INIT_CONFIG mAdlMDnxEnableGpioTable[] =
{
  // DNX
  {GPIO_VER2_LP_GPP_E23, {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutHigh,   GpioIntDis, GpioPlatformReset,  GpioTermNone}},  //DISP_AUX_N_BIAS_GPIO
};

#endif // _ALDERLAKE_M_GPIO_TABLE_H_
