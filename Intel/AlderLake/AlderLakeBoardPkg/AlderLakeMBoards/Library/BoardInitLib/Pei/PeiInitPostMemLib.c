/** @file

;******************************************************************************
;* Copyright (c) 2020 - 2021, Insyde Software Corporation. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/
/** @file

@copyright
  INTEL CONFIDENTIAL
  Copyright 2020 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains a 'Sample Driver' and is licensed as such under the terms
  of your license agreement with Intel or your vendor. This file may be modified
  by the user, subject to the additional terms of the license agreement.

@par Specification Reference:
**/

#include <PiPei.h>
#include <Library/DebugLib.h>
#include <Library/PcdLib.h>

#include <Register/PttPtpRegs.h>
#include <Library/SiliconInitLib.h>
#include <Library/PreSiliconEnvDetectLib.h>
#include <Library/BoardConfigLib.h>
#include "GpioTableAdlMPostMem.h"
#include <Library/PeiServicesLib.h>
#include <Library/GpioLib.h>
#include <Library/IoLib.h>
#include <PlatformBoardId.h>
#include <PlatformBoardConfig.h>
#include <UsbTypeC.h>
#include <Library/PeiHdaVerbTables.h>
//[-start-210809-IB18410076-add]//
#include <Library/PeiOemSvcChipsetLib.h>
//[-end-210809-IB18410076-add]//
#include <Ppi/ReadOnlyVariable2.h>
#include <SetupVariable.h>
//[-start-210809-IB18410076-add]//
#include <Library/EcMiscLib.h>
#include <Library/GpioCfgLib.h>
#include <Library/GpioNativeLib.h>
#include <Library/MemoryAllocationLib.h>
#include <Library/BaseMemoryLib.h>
#include <Library/PeiOemSvcKernelLib.h>
//[-end-210809-IB18410076-add]//

extern EFI_GUID gSetupVariableGuid;
extern EFI_GUID gPchSetupVariableGuid;

/**
  Configures GPIO

  @param[in]  GpioTable       Point to Platform Gpio table
  @param[in]  GpioTableCount  Number of Gpio table entries

**/
VOID
ConfigureGpio (
  IN GPIO_INIT_CONFIG                 *GpioDefinition,
  IN UINT16                           GpioTableCount
  );

/**
  Alderlake M boards configuration init function for PEI post memory phase.

  @retval EFI_SUCCESS             The function completed successfully.
**/
EFI_STATUS
EFIAPI
AdlMInit (
  VOID
  )
{
  UINT16            GpioCount;
  UINTN             Size;
  EFI_STATUS        Status;
  GPIO_INIT_CONFIG  *GpioTable;
  //
  // GPIO Table Init
  //
  Status = EFI_SUCCESS;
  GpioCount = 0;
  Size = 0;
  GpioTable = NULL;
  //
  // GPIO Table Init
  //
  GpioTable = (GPIO_INIT_CONFIG *)PcdGetPtr(VpdPcdBoardGpioTable);

  GetGpioTableSize (GpioTable, &GpioCount);
  //
  // Increase GpioCount for the zero terminator.
  //
  GpioCount ++;
  Size = (UINTN) (GpioCount * sizeof (GPIO_INIT_CONFIG));
  Status = PcdSetPtrS (PcdBoardGpioTable, &Size, GpioTable);
  ASSERT_EFI_ERROR (Status);

  PcdSet8S (PcdSataPortsEnable0, 0x1);

  return Status;
}

/**
  Board I2C pads termination configuration init function for PEI pre-memory phase.
**/
VOID
AdlMSerialIoI2cPadsTerminationInit (
  VOID
  )
{
}

/**
  Touch panel GPIO init function for PEI post memory phase.
**/
VOID
AdlMTouchPanelGpioInit (
  VOID
  )
{
  UINT16  BoardId;
  BoardId = PcdGet16 (PcdBoardId);
  switch (BoardId) {
    case BoardIdAdlMLp5Aep:
      PcdSet32S (PcdBoardGpioTableTouchPanel1, (UINTN) mAdlMLp5AepTouchPanel1GpioTable);
      PcdSet16S (PcdBoardGpioTableTouchPanel1Size, sizeof (mAdlMLp5AepTouchPanel1GpioTable) / sizeof (GPIO_INIT_CONFIG));
      break;
    default:
      PcdSet32S (PcdBoardGpioTableTouchPanel1,  (UINTN) mAdlMTouchPanel1GpioTable);
      PcdSet16S (PcdBoardGpioTableTouchPanel1Size,  sizeof (mAdlMTouchPanel1GpioTable)/ sizeof (GPIO_INIT_CONFIG));
     break;
  }
}

/**
  CVF GPIO init function for PEI post memory phase.
**/
VOID
AdlMCvfGpioInit (
  VOID
  )
{
  UINT16  BoardId;
  BoardId = PcdGet16 (PcdBoardId);
  switch (BoardId) {
    case BoardIdAdlMLp4Rvp:
    case BoardIdAdlMLp5Rvp:
    case BoardIdAdlMLp5Rvp2a:
    case BoardIdAdlMLp5PmicRvp:
    case BoardIdAdlMLp5Aep:
      PcdSet32S (PcdBoardGpioTableCvf,  (UINTN) mAdlMCvfGpioTable);
      PcdSet16S (PcdBoardGpioTableCvfSize,  sizeof(mAdlMCvfGpioTable)/ sizeof(GPIO_INIT_CONFIG));
      break;
    default:
      //
      // Do nothing.
      //
      break;
  }
}

/**
  Tsn device GPIO init function for PEI post memory phase.
**/
VOID
AdlMTsnDeviceGpioInit (
  VOID
  )
{
}


/**
  HDA VerbTable init function for PEI post memory phase.
**/
VOID
AdlMHdaVerbTableInit (
  VOID
  )
{
  PCH_SETUP                         PchSetup;
  EFI_STATUS                        Status;
  EFI_PEI_READ_ONLY_VARIABLE2_PPI   *VariableServices;
  UINTN                             VariableSize;

  VariableServices = NULL;
  Status = PeiServicesLocatePpi (
              &gEfiPeiReadOnlyVariable2PpiGuid,
              0,
              NULL,
              (VOID **) &VariableServices
              );
  if (VariableServices != NULL) {
    VariableSize = sizeof (PCH_SETUP);
    Status = VariableServices->GetVariable (VariableServices, L"PchSetup", &gPchSetupVariableGuid, NULL, &VariableSize, &PchSetup);

    if (Status == EFI_SUCCESS) {
      if (PchSetup.PchHdaAlc245DmicConfiguration == 1) {
        PcdSet32S (PcdHdaVerbTableDatabase, (UINT32) &HdaVerbTableDbAdlDmic);
      } else {
        PcdSet32S (PcdHdaVerbTableDatabase, (UINT32) &HdaVerbTableDbAdlNoDmic);
      }
    } else {
      ASSERT_EFI_ERROR (Status);
    }
  }
}

/**
  Return Type-C Aux Override Value of Setup variable
**/
UINT8
AdlMAuxOverrideSetup (VOID)
{
#if FixedPcdGetBool (PcdSetupEnable) == 1
  EFI_PEI_READ_ONLY_VARIABLE2_PPI   *VariableServices;
  EFI_STATUS                        Status;
  SETUP_DATA                        Setup;
  UINTN                             DataSize;
  Status = PeiServicesLocatePpi (
             &gEfiPeiReadOnlyVariable2PpiGuid,
             0,
             NULL,
             (VOID **) &VariableServices
             );
  if (Status == EFI_SUCCESS) {
    DataSize = sizeof (SETUP_DATA);
    Status = VariableServices->GetVariable (
                                 VariableServices,
                                 L"Setup",
                                 &gSetupVariableGuid,
                                 NULL,
                                 &DataSize,
                                 &Setup
                                 );
    if (Status == EFI_SUCCESS) {
      DEBUG ((DEBUG_INFO, "AuxOriOverride= %d\n", Setup.AuxOriOverride));
      return Setup.AuxOriOverride;
    }
  }
#endif
  return 0;
}

/**
  Misc. init function for PEI post memory phase.
**/
VOID
AdlMBoardMiscInit (
  VOID
  )
{
  UINT16    BoardId;
  BoardId = PcdGet16(PcdBoardId);

  EFI_PEI_READ_ONLY_VARIABLE2_PPI   *VariableServices;
  EFI_STATUS                        Status;
  SETUP_DATA                        Setup;
  UINTN                             DataSize;
  PCH_SETUP                         PchSetup;

  Status = PeiServicesLocatePpi (
             &gEfiPeiReadOnlyVariable2PpiGuid,
             0,
             NULL,
             (VOID **) &VariableServices
             );
  if (EFI_ERROR(Status)) {
    DEBUG((DEBUG_ERROR, "AdlMboardMiscInit: PeiServicesLocatePpi failed\n"));
    ASSERT_EFI_ERROR (Status);
  }

    DataSize = sizeof (SETUP_DATA);
    Status = VariableServices->GetVariable (
                                 VariableServices,
                                 L"Setup",
                                 &gSetupVariableGuid,
                                 NULL,
                                 &DataSize,
                                 &Setup
                                 );
  if (EFI_ERROR(Status)) {
    DEBUG((DEBUG_ERROR, "AdlMboardMiscInit: setupdata failed\n"));
    ASSERT_EFI_ERROR (Status);
  }

    DataSize = sizeof (PCH_SETUP);
    Status = VariableServices->GetVariable (
                                 VariableServices,
                                 L"PchSetup",
                                 &gPchSetupVariableGuid,
                                 NULL,
                                 &DataSize,
                                 &PchSetup
                                 );
  if (EFI_ERROR(Status)) {
    DEBUG((DEBUG_ERROR, "AdlMboardMiscInit: pch setup failed\n"));
    ASSERT_EFI_ERROR (Status);
  }
  PcdSetBoolS (PcdSataLedEnable, FALSE);
  PcdSetBoolS (PcdVrAlertEnable, TRUE);
  PcdSetBoolS (PcdPchThermalHotEnable, FALSE);
  PcdSetBoolS (PcdMemoryThermalSensorGpioCPmsyncEnable, FALSE);
  PcdSetBoolS (PcdMemoryThermalSensorGpioDPmsyncEnable, FALSE);

  //
  // MIPI CSI Camera
  //
  PcdSetBoolS (PcdMipiCamGpioEnable, TRUE);

  switch (BoardId) {
    case BoardIdAdlMLp4Rvp:
    case BoardIdAdlMLp5Rvp:
    case BoardIdAdlMLp5Rvp2a:
    case BoardIdAdlMLp5PmicRvp:
      //
      // For PCIe SLOT 1 - x1 Connector
      //
      PcdSet32S (PcdPcieSlot1WakeGpioPin, GPIO_VER2_LP_GPP_D17);
      PcdSet32S (PcdPcieSlot1PwrEnableGpioNo, GPIO_VER2_LP_GPP_D14);
      PcdSet32S (PcdPcieSlot1HoldRstGpioNo, GPIO_VER2_LP_GPP_F10);
      PcdSetBoolS (PcdPcieSlot1PwrEnableGpioPolarity, PIN_GPIO_ACTIVE_HIGH);
      PcdSetBoolS (PcdPcieSlot1HoldRstGpioPolarity, PIN_GPIO_ACTIVE_LOW);

      //
      // PCH M.2 SSD port
      //
      PcdSet32S (PcdPchSsd1PwrEnableGpioNo, GPIO_VER2_LP_GPP_D10);               // PCH M.2 SSD power enable gpio pin
      PcdSet32S (PcdPchSsd1RstGpioNo, GPIO_VER2_LP_GPP_H0);                      // PCH M.2 SSD reset gpio pin
      PcdSetBoolS (PcdPchSsd1PwrEnableGpioPolarity, PIN_GPIO_ACTIVE_HIGH);       // PCH M.2 SSD power enable gpio pin polarity
      PcdSetBoolS (PcdPchSsd1RstGpioPolarity, PIN_GPIO_ACTIVE_LOW);             // PCH M.2 SSD reset gpio pin polarity
      //
      // Touch Pad
      //
      PcdSet32S (PcdTouchpadIrqGpio, GPIO_VER2_LP_GPP_D11);                      // Touch Pad Interrupt Pin
      PcdSetBoolS (PcdTouchpadIrqGpioPolarity, PIN_GPIO_ACTIVE_LOW);            // Touch Pad Interrupt pin polarity
      //
      // Touch Panels 0
      //
      if ((PchSetup.ThcPort0Assignment == ThcAssignmentNone) && (Setup.PchI2cTouchPanelType != 0)) {
        PcdSet32S (PcdTouchpanelIrqGpio, GPIO_VER2_LP_GPP_F18);                    // Touch 0 Interrupt Pin
        PcdSet32S (PcdTouchpanelPwrEnableGpio, GPIO_VER2_LP_GPP_F7);              // Touch 0 power enable pin
        PcdSet32S (PcdTouchpanelRstGpio, GPIO_VER2_LP_GPP_F17);                    // Touch 0 reset pin
        PcdSetBoolS (PcdTouchpanelIrqGpioPolarity, PIN_GPIO_ACTIVE_HIGH);          // Touch 0 Interrupt Pin polarity
        PcdSetBoolS (PcdTouchpanelPwrEnableGpioPolarity, PIN_GPIO_ACTIVE_HIGH);    // Touch 0 power enable pin polarity
        PcdSetBoolS (PcdTouchpanelRstGpioPolarity, PIN_GPIO_ACTIVE_HIGH);          // Touch 0 reset pin polarity
      }
      //
      // WLAN
      //
      PcdSet32S(PcdWlanWakeGpio, GPIO_VER2_LP_GPP_D13);  // WiFi Wake pin

      //
      // LID WAKE
      //
      PcdSet32S(PcdLidSwitchWakeGpio, GPIO_VER2_LP_GPD2);

      if(BoardId == BoardIdAdlMLp5Rvp2a) {
        PcdSet8S (PcdPcieSlot1RootPort, 7);
        PcdSet8S(PcdWlanRootPortNumber, 8);
      } else {
        PcdSet8S(PcdWlanRootPortNumber, 10);
        PcdSet8S (PcdPcieSlot1RootPort, 9);
      }
      break;
    case BoardIdAdlMLp5Aep:
      //
      // Touch Pad
      //
      PcdSet32S (PcdTouchpadIrqGpio, GPIO_VER2_LP_GPP_D11);                      // Touch Pad Interrupt Pin
      PcdSetBoolS (PcdTouchpadIrqGpioPolarity, PIN_GPIO_ACTIVE_LOW);             // Touch Pad Interrupt pin polarity
      //
      // Touch Panel 0
      //
      PcdSet32S (PcdTouchpanelIrqGpio, GPIO_VER2_LP_GPP_E17);                    // Touch 0 Interrupt Pin
      PcdSet32S (PcdTouchpanelRstGpio, GPIO_VER2_LP_GPP_E6);                     // Touch 0 reset pin
      PcdSetBoolS (PcdTouchpanelIrqGpioPolarity, PIN_GPIO_ACTIVE_HIGH);          // Touch 0 Interrupt Pin polarity
      PcdSetBoolS (PcdTouchpanelRstGpioPolarity, PIN_GPIO_ACTIVE_HIGH);          // Touch 0 reset pin polarity
      break;
  }

  //
  // Type-C Aux Override GPIO Configuration or DNX GPIO Configuration.
  //
  switch (BoardId) {
    case BoardIdAdlMLp4Rvp:
    case BoardIdAdlMLp5Rvp:
    case BoardIdAdlMLp5Rvp2a:
      if (AdlMAuxOverrideSetup () != 0) {
        DEBUG ((DEBUG_INFO, "Type-C Aux Override is Enable for Adl-M Board\n"));
        ConfigureGpio ((VOID *) (UINTN) mAdlMTypeCAuxOverrideEnableGpioTable, (UINTN) (sizeof (mAdlMTypeCAuxOverrideEnableGpioTable) / sizeof (GPIO_INIT_CONFIG)));
      } else {
        DEBUG ((DEBUG_INFO, "DNX is Enable for Adl-M Board\n"));
        ConfigureGpio ((VOID *) (UINTN) mAdlMDnxEnableGpioTable, (UINTN) (sizeof (mAdlMDnxEnableGpioTable) / sizeof (GPIO_INIT_CONFIG)));
      }
      break;

    case BoardIdAdlMLp5PmicRvp:
      DEBUG ((DEBUG_INFO, "DNX is Enable for Adl-M Board\n"));
      ConfigureGpio ((VOID *) (UINTN) mAdlMDnxEnableGpioTable, (UINTN) (sizeof (mAdlMDnxEnableGpioTable) / sizeof (GPIO_INIT_CONFIG)));
      break;

  }
  return;
}

/**
  Tbt init function for PEI post memory phase.
**/
VOID
AdlMBoardTbtInit (
  VOID
  )
{
  //
  // Set ALL port setting by default from VPD.
  //
  PcdSet32S (PcdCpuUsb30OverCurrentPinTable, (UINT32) (PcdGetPtr (VpdPcdCpuUsb3OcMap)));

  PcdSet8S (PcdTcssPdType, TcssPdTypeTi);
  PcdSet8S (PcdTcssPdNumber, 1);
}

/**
  Board Specific Init for PEI post memory phase.
**/
VOID
PeiAdlMBoardSpecificInitPostMemNull (
  VOID
  )
{
}

/**
   Security GPIO init function for PEI post memory phase.
**/
VOID
AdlMBoardSecurityInit (
  VOID
  )
{
  GPIO_PAD  GpioPad;
  UINT32    GpioIrqNumber;
  UINT32    VerFtif;
  UINT16    BoardId;
  BoardId = PcdGet16 (PcdBoardId);

  if (FeaturePcdGet (PcdTpm2Enable) == TRUE) {
    //
    // If dTPM is connected to the SPI, configure interrupt
    //
    VerFtif = MmioRead32 (R_PTT_TXT_STS_FTIF);

    if ((VerFtif & FTIF_FT_LOC_MASK) == V_FTIF_SPI_DTPM_PRESENT) {
      switch (BoardId) {
      case BoardIdAdlMLp4Rvp:
      case BoardIdAdlMLp5Rvp:
      case BoardIdAdlMLp5Rvp2a:
      case BoardIdAdlMLp5PmicRvp:
      case BoardIdAdlMLp5Aep:
      default:
        GpioPad = (GPIO_PAD) GPIO_VER2_LP_GPP_E3;
        break;
      }

      GpioGetPadIoApicIrqNumber (GpioPad, &GpioIrqNumber);

      DEBUG ((DEBUG_INFO, "TPM Interrupt Number: %d\n", GpioIrqNumber));
      PcdSet32S (PcdTpm2CurrentIrqNum, GpioIrqNumber);
    } else {
      PcdSet32S (PcdTpm2CurrentIrqNum, 0);
    }
  }
}

/**
  Board's PCD function hook init function for PEI post memory phase.
**/
VOID
AdlMBoardFunctionInit (
  VOID
)
{
  PcdSet32S (PcdFuncPeiBoardSpecificInitPostMem, (UINTN) PeiAdlMBoardSpecificInitPostMemNull);
}

/**
  PMC-PD solution enable init lib
**/
VOID
AdlMBoardPmcPdInit (
  VOID
  )
{
  PcdSetBoolS (PcdBoardPmcPdEnable, 1);
}

/**
  USB TypeC init function for before silicon initialisation.
**/
VOID
AdlMBoardTypeCPortMapping (
  VOID
  )
{
  UINT16                          BoardId;

  BoardId = PcdGet16(PcdBoardId);

  switch (BoardId) {
    case BoardIdAdlMLp4Rvp:
    case BoardIdAdlMLp5Rvp:
    case BoardIdAdlMLp5Rvp2a:
    case BoardIdAdlMLp5PmicRvp:
    case BoardIdAdlMLp5Aep:
      PcdSetBoolS (PcdUsbTypeCSupport, TRUE);
      // Number of ports supported
      PcdSet8S(PcdTypeCPortsSupported, 2);
      // Type C port 1 mapping
      PcdSet8S(PcdUsbTypeCPort1, 0);
      PcdSet8S(PcdUsbTypeCPort1Pch, 1);
      PcdSet8S(PcdUsbCPort1Properties, (0 << TCSS_TYPEC_ROOT_PORT_INDEX_BIT_OFFSET | USB_TYPEC_CPU << 1 | SPLIT_SUPPORTED));
      // Type C port 2 mapping
      PcdSet8S(PcdUsbTypeCPort2, 1);
      PcdSet8S(PcdUsbTypeCPort2Pch, 2);
      PcdSet8S(PcdUsbCPort2Properties, (1 << TCSS_TYPEC_ROOT_PORT_INDEX_BIT_OFFSET | USB_TYPEC_CPU << 1 | SPLIT_SUPPORTED));
      break;
    default:
      PcdSetBoolS (PcdUsbTypeCSupport, FALSE);
      break;
  }
}

/**
  Notify Ec To Pd firmware for enabling Vpro.
**/
VOID
AdlMNotifyEcToPdForVpro (
  VOID
  )
{
  EFI_STATUS                        Status;
  EFI_BOOT_MODE                     BootMode;
  EFI_PEI_READ_ONLY_VARIABLE2_PPI   *VariableServices;
  ME_SETUP                          MeSetup;
  UINTN                             VariableSize;

  PeiServicesGetBootMode (&BootMode);

  //
  // PD will keep original contents during Sx state.
  // It doesn't need to do anything during these states.
  //
  if ((BootMode == BOOT_ON_S3_RESUME) ||
      (BootMode == BOOT_ON_S4_RESUME) ||
      (BootMode == BOOT_ON_S5_RESUME)) {
    return;
  }

  Status = PeiServicesLocatePpi (
             &gEfiPeiReadOnlyVariable2PpiGuid,
             0,
             NULL,
             (VOID **) &VariableServices
             );
  ASSERT_EFI_ERROR (Status);

  VariableSize = sizeof (ME_SETUP);
  Status = VariableServices->GetVariable (
                               VariableServices,
                               L"MeSetup",
                               &gMeSetupVariableGuid,
                               NULL,
                               &VariableSize,
                               &MeSetup
                               );
  ASSERT_EFI_ERROR (Status);

  if (MeSetup.vProTbtDock == 1) {
    NotifyEcToPdForVpro ();
  }

  return;
}

//[-start-210809-IB18410076-add]//
STATIC
EFI_STATUS
GpioCfgPei (
//[-start-210120-IB16560239-modify]//
  IN H2O_BOARD_ID               Board,
//[-end-210120-IB16560239-modify]//
  IN BOOLEAN                    PreMemFlag
  )
{
  EFI_STATUS                    Status;
  H2O_GPIO_INIT_CONFIG          *GpioInitStruct;
  UINT32                        GpioInitStructCount;
  UINT32                        GpioTableCount;
  GPIO_INIT_CONFIG              *GpioDefinitionTable;
  H2O_GPIO_INIT_CONFIG          SingleGpioInitStruct = {0};
  UINT32                        Index;
  UINT32                        PadMaxSize;
  GPIO_GROUP                    Group;

  PadMaxSize = 0;
  for (Group = GpioGetLowestGroup(); Group <= GpioGetHighestGroup(); Group ++) {
    PadMaxSize += GpioGetPadPerGroup (Group);
  }

  GpioDefinitionTable = NULL;
  GpioDefinitionTable = (GPIO_INIT_CONFIG *)AllocateZeroPool (PadMaxSize * sizeof (GPIO_INIT_CONFIG));
  if (GpioDefinitionTable == NULL) {
    return EFI_OUT_OF_RESOURCES;
  }
  //
  // Use GetGpioCfg in GpioCfgLib to get build-time GPIO configuration
  // Input Board value corresponds to Skuids in Project.dsc
  //
  Status = GetGpioCfg (Board, &GpioInitStruct, &GpioInitStructCount);
  ASSERT_EFI_ERROR (Status);
  if (EFI_ERROR (Status)) {
    return EFI_NOT_FOUND;
  }

  GpioTableCount = 0;
  Status = EFI_UNSUPPORTED;
  for (Index = 0; Index < GpioInitStructCount; Index++) {
    if (GpioInitStruct[Index].Flag != PreMemFlag) {
      continue;
    }
    CopyMem (&SingleGpioInitStruct, &GpioInitStruct[Index], sizeof(H2O_GPIO_INIT_CONFIG));
    //
    // OemServices
    // This function works when PCT being FALSE or PCT's setting being null.
    // Please refer to Power On Guide 0.4.
    //
    DEBUG ((DEBUG_INFO, "OemKernelServices Call: OemSvcUpdateGpioCfg \n"));
    Status = OemSvcUpdateGpioCfg (&SingleGpioInitStruct);
    DEBUG ((DEBUG_INFO, "OemKernelServices OemSvcUpdateGpioCfg Status: %r\n", Status));
    if (Status != EFI_SUCCESS) {
     CopyMem (&GpioDefinitionTable[GpioTableCount], &SingleGpioInitStruct, sizeof(GPIO_INIT_CONFIG));
     GpioTableCount++;
    }
  }
  if ((Status == EFI_SUCCESS) && (GpioTableCount == 0x00)) {
    return EFI_SUCCESS;
  }

  //
  // Batch process GPIO configuraions.
  //
  DEBUG ((DEBUG_INFO, "ConfigureGpio() Start\n"));
  Status = GpioConfigurePads (GpioTableCount, GpioDefinitionTable);
  DEBUG ((DEBUG_INFO, "ConfigureGpio() End\n"));

  return EFI_SUCCESS;
}
//[-end-210809-IB18410076-add]//

/**
  Configure GPIO, TouchPanel, HDA, PMC, TBT etc.

  @retval  EFI_SUCCESS   Operation success.
**/
EFI_STATUS
EFIAPI
AdlMBoardInitBeforeSiliconInit (
  VOID
  )
{
//[-start-210809-IB18410076-add]//  
  EFI_STATUS       Status;
  UINT16           GpioTableCount;
  GPIO_INIT_CONFIG *GpioTable;
  INT16            BoardId;
//[-end-210809-IB18410076-add]//  
  
//[-start-210809-IB18410076-add]//  
  BoardId = PcdGet16(PcdBoardId);
//[-end-210809-IB18410076-add]//
  AdlMInit ();

  AdlMSerialIoI2cPadsTerminationInit ();
  AdlMTouchPanelGpioInit ();
  AdlMCvfGpioInit();
  AdlMHdaVerbTableInit ();
  AdlMBoardMiscInit ();
  AdlMBoardTbtInit ();
  AdlMBoardFunctionInit();
  AdlMBoardSecurityInit();
  AdlMBoardPmcPdInit ();
  AdlMBoardTypeCPortMapping ();

//[-start-210809-IB18410076-modify]//
  if (PcdGetBool (PcdH2OGpioCfgSupported) && !PcdGetBool(PcdDisableVpdGpioTable)) {
    //
    // Program GPIOs, Input Board value corresponds to Skuids in Project.dsc
    //
    DEBUG ((DEBUG_INFO, "GpioCfgPei() Start\n"));
//[-start-210120-IB16560239-modify]//
    GpioCfgPei ((H2O_BOARD_ID)BoardId, FALSE);
//[-end-210120-IB16560239-modify]//
    DEBUG ((DEBUG_INFO, "GpioCfgPei() END\n"));
  } else {

    GpioTable = PcdGetPtr (PcdBoardGpioTable);
    GetGpioTableSize (GpioTable, &GpioTableCount);
    DEBUG_OEM_SVC ((DEBUG_INFO, "Pei OemChipsetServices Call: OemSvcModifyGpioSettingTable \n"));
    Status = OemSvcModifyGpioSettingTable (&GpioTable, &GpioTableCount);
    DEBUG_OEM_SVC ((DEBUG_INFO, "Pei OemChipsetServices OemSvcModifyGpioSettingTable Status: %r\n", Status));
    if (GpioTableCount != 0 && Status != EFI_SUCCESS) {
      GpioConfigurePads ((UINT32) GpioTableCount, GpioTable);
    }
  }
//[-end-210809-IB18410076-modify]//
  ///
  /// Do Late PCH init
  ///
  LateSiliconInit ();

  //
  // Notify Ec To Pd firmware for enabling Vpro
  //
  AdlMNotifyEcToPdForVpro ();

  return EFI_SUCCESS;
}
VOID
AdlMBoardSpecificGpioInitPostMem (
  VOID
  )
{
  UINT16    BoardId;
  BoardId = PcdGet16 (PcdBoardId);
  //
  // Assign WiFi, Bluetooth & Audio relative GPIO.
  //
  switch (BoardId) {
    case BoardIdAdlMLp4Rvp:
    case BoardIdAdlMLp5Rvp:
    case BoardIdAdlMLp5Rvp2a:
    case BoardIdAdlMLp5PmicRvp:
      PcdSet32S (PcdHdaI2sCodecIrqGpio,         GPIO_VER2_LP_GPP_H3); // Audio I2S Codec IRQ Pin
      PcdSet8S  (PcdHdaI2sCodecI2cBusNumber,    0); // Audio I2S Codec conntected to I2C0
      PcdSet32S (PcdWwanModemBaseBandResetGpio, GPIO_VER2_LP_GPP_E5); // WWAN/Modem Base Band Reset pin
      //
      //Update PcdBoardRtd3TableSignature per Setup
      //
      if(BoardId == BoardIdAdlMLp5Rvp2a) {
        PcdSet64S (PcdBoardRtd3TableSignature, SIGNATURE_64 ('A', 'd', 'l', 'M', '_', '2', 'a', 'B'));
      } else {
        PcdSet64S (PcdBoardRtd3TableSignature, SIGNATURE_64 ('A', 'd', 'l', 'M', '_', 'R', 'v', 'p'));
      }
      //
      // Update OEM table ID
      //
      PcdSet64S (PcdXhciAcpiTableSignature, SIGNATURE_64 ('x', 'h', '_', 'a', 'd', 'l', '_', 'M'));
      //
      // Configure GPIOs for BT modules - UART or USB
      //
      PcdSet32S (PcdBtRfKillGpio, GPIO_VER2_LP_GPP_A12);    // Bluetooth RF-KILL# pin
      if (PcdGet8 (PcdDiscreteBtModule) == 2) {             // Only for BT Over UART Selection
        PcdSet32S (PcdBtIrqGpio, GPIO_VER2_LP_GPP_E0);      // Bluetooth IRQ Pin
      }
      break;
    case BoardIdAdlMLp5Aep:
      PcdSet32S (PcdTouchpanelIrqGpio, GPIO_VER2_LP_GPP_E17);          // Touch Interrupt Pin
      PcdSet32S (PcdTouchpadIrqGpio, GPIO_VER2_LP_GPP_D11);            // Force Pad Interrupt Pin
      PcdSet32S (PcdWwanModemBaseBandResetGpio, GPIO_VER2_LP_GPP_E5);  // WWAN/Modem Base Band Reset pin
      PcdSet32S (PcdBtRfKillGpio, GPIO_VER2_LP_GPP_A12);               // Bluetooth RF-KILL# pin
      //
      //Update PcdBoardRtd3TableSignature per Setup
      //
      PcdSet64S (PcdBoardRtd3TableSignature, SIGNATURE_64 ('A', 'd', 'l', 'M', '_', 'R', 'v', 'p'));
      //
      // Update OEM table ID
      //
      PcdSet64S (PcdXhciAcpiTableSignature, SIGNATURE_64 ('x', 'h', '_', 'a', 'd', 'l', '_', 'M'));
      break;
  }
  //
  //Modify Preferred_PM_Profile field based on Board SKU's. Default is set to Mobile
  //
  PcdSet8S (PcdPreferredPmProfile, EFI_ACPI_2_0_PM_PROFILE_MOBILE);
  if (PcdGet8 (PcdPlatformFlavor) == FlavorUpServer) {
    PcdSet8S (PcdPreferredPmProfile, EFI_ACPI_2_0_PM_PROFILE_ENTERPRISE_SERVER);
  }
}

VOID
AdlMInitCommonPlatformPcdPostMem (
  VOID
  )
{
  PcdSet32S (PcdEcSmiGpio, GPIO_VER2_LP_GPP_E7);
  PcdSet32S (PcdEcLowPowerExitGpio, GPIO_VER2_LP_GPP_A7);
  PcdSetBoolS (PcdPssReadSN, TRUE);
  PcdSet8S (PcdPssI2cSlaveAddress, 0x6E);
  PcdSet8S (PcdPssI2cBusNumber, 0x05);
  PcdSetBoolS (PcdSpdAddressOverride, FALSE);

  //
  // Battery Present
  // Real & Virtual battery is need to supported in all except Desktop
  //
  PcdSet8S (PcdBatteryPresent, BOARD_REAL_BATTERY_SUPPORTED | BOARD_VIRTUAL_BATTERY_SUPPORTED);
  //
  // Real Battery 1 Control & Real Battery 2 Control
  //
  PcdSet8S (PcdRealBattery1Control, 1);
  PcdSet8S (PcdRealBattery2Control, 2);
  //
  // Sky Camera Sensor
  //
  PcdSetBoolS (PcdMipiCamSensor, FALSE);

  //
  // H8S2113 SIO,UART
  //
  PcdSetBoolS (PcdH8S2113SIO, FALSE);
  PcdSetBoolS (PcdH8S2113UAR, FALSE);
  //
  // NCT6776F COM, SIO & HWMON
  //
  PcdSetBoolS (PcdNCT6776FCOM, FALSE);
  PcdSetBoolS (PcdNCT6776FSIO, FALSE);
  PcdSetBoolS (PcdNCT6776FHWMON, FALSE);
  //
  // ZPODD
  //
  PcdSet8S (PcdZPoddConfig, 0);
  //
  // SMC Runtime Sci Pin
  // EC will use eSpi interface to generate SCI
  //
  PcdSet32S (PcdSmcRuntimeSciPin, 0x00);
  //
  // Convertable Dock Support
  // Not supported only for S & H SKU's
  PcdSetBoolS (PcdConvertableDockSupport, TRUE);
  //
  // Ec Hotkey F3, F4, F5, F6, F7 and F8 Support
  //
  PcdSet8S (PcdEcHotKeyF3Support, TRUE);
  PcdSet8S (PcdEcHotKeyF4Support, TRUE);
  PcdSet8S (PcdEcHotKeyF5Support, TRUE);
  PcdSet8S (PcdEcHotKeyF6Support, TRUE);
  PcdSet8S (PcdEcHotKeyF7Support, TRUE);
  PcdSet8S (PcdEcHotKeyF8Support, TRUE);

  //
  // Virtual Button Volume Up & Done Support
  // Virtual Button Home Button Support
  // Virtual Button Rotation Lock Support
  //
  PcdSetBoolS (PcdVirtualButtonVolumeUpSupport, TRUE);
  PcdSetBoolS (PcdVirtualButtonVolumeDownSupport, TRUE);
  PcdSetBoolS (PcdVirtualButtonHomeButtonSupport, TRUE);
  PcdSetBoolS (PcdVirtualButtonRotationLockSupport, TRUE);
  //
  // Slate Mode Switch Support
  //
  PcdSetBoolS (PcdSlateModeSwitchSupport, TRUE);
  //
  // Virtual Gpio Button Support
  //
  PcdSetBoolS (PcdVirtualGpioButtonSupport, TRUE);
  //
  // Ac Dc Auto Switch Support
  //
  PcdSetBoolS (PcdAcDcAutoSwitchSupport, TRUE);

  //
  // Pm Power Button Gpio Pin
  //
  PcdSet32S (PcdPmPowerButtonGpioPin, GPIO_VER2_LP_GPD3);
  //
  // Acpi Enable All Button Support
  //
  PcdSetBoolS (PcdAcpiEnableAllButtonSupport, TRUE);
  //
  // Acpi Hid Driver Button Support
  //
  PcdSetBoolS (PcdAcpiHidDriverButtonSupport, TRUE);
}

/**
  Board init for PEI after Silicon initialized

  @retval  EFI_SUCCESS   Operation success.
**/
EFI_STATUS
EFIAPI
AdlMBoardInitAfterSiliconInit (
  VOID
  )
{
  DEBUG ((DEBUG_INFO, "AdlMBoardInitAfterSiliconInit \n"));
  AdlMBoardSpecificGpioInitPostMem ();
  AdlMInitCommonPlatformPcdPostMem ();

  return EFI_SUCCESS;
}
