/** @file
  Alderlake M RVP GPIO definition table for Pre-Memory Initialization

@copyright
  Copyright (c) 2020 - 2021 Intel Corporation. All rights reserved
  This software and associated documentation (if any) is furnished
  under a license and may only be used or copied in accordance
  with the terms of the license. Except as permitted by such
  license, no part of this software or documentation may be
  reproduced, stored in a retrieval system, or transmitted in any
  form or by any means without the express written consent of
  Intel Corporation.
  This file contains an 'Intel Peripheral Driver' and is
  licensed for Intel CPUs and chipsets under the terms of your
  license agreement with Intel or your vendor. This file may
  be modified by the user, subject to additional terms of the
  license agreement.
**/
#ifndef _ALDERLAKE_M_PREMEM_GPIO_TABLE_H_
#define _ALDERLAKE_M_PREMEM_GPIO_TABLE_H_

#include <Pins/GpioPinsVer2Lp.h>
#include <Library/GpioLib.h>
#include <Library/GpioConfig.h>

///
/// !!! GPIOs designated to Native Functions shall not be configured by Platform Code.
/// Native Pins shall be configured by Silicon Code (based on BIOS policies setting) or soft straps(set by CSME in FITc).
/// Configuring pins to native function in GPIO table would override the pin settings resulting in unexpected behavior.
///

GLOBAL_REMOVE_IF_UNREFERENCED GPIO_INIT_CONFIG mGpioTableAdlMLp4RvpWwanOnEarlyPreMem[] =
{
  {GPIO_VER2_LP_GPP_A8,  {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,   GpioOutHigh,     GpioIntDis,                GpioResumeReset,    GpioTermNone,  GpioOutputStateUnlock }},  //WWAN_PWRE
  {GPIO_VER2_LP_GPP_D9,  {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,   GpioOutHigh,     GpioIntDis,                GpioResumeReset,    GpioTermNone,  GpioOutputStateUnlock }},  //WWAN_FCP_OFF_N
  {GPIO_VER2_LP_GPP_E5,  {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,   GpioOutHigh,     GpioIntDis,                GpioResumeReset,    GpioTermNone,  GpioOutputStateUnlock }},  //WWAN_RST_N
  {GPIO_VER2_LP_GPP_C5,  {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,   GpioOutHigh,     GpioIntDis,                GpioResumeReset,    GpioTermNone,  GpioOutputStateUnlock }},  //WWAN_PERST_N
  {GPIO_VER2_LP_GPP_D18, {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirInInv, GpioOutDefault,  GpioIntLevel|GpioIntSci,   GpioHostDeepReset,  GpioTermNone,  GpioPadConfigUnlock  }},  //WWAN_WAKE_N
  {GPIO_VER2_LP_GPP_D15, {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,   GpioOutHigh,     GpioIntDis,                GpioResumeReset,    GpioTermNone,  GpioOutputStateUnlock }},  //WWAN_DISABLE_N
  {GPIO_VER2_LP_GPP_A6,  {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirIn,    GpioOutDefault,  GpioIntLevel|GpioIntSci,   GpioHostDeepReset,  GpioTermNone,  GpioPadConfigUnlock }}, // SAE_DPR_PCH
};

GLOBAL_REMOVE_IF_UNREFERENCED GPIO_INIT_CONFIG mGpioTableAdlMLp4RvpWwanOffEarlyPreMem[] =
{
   {GPIO_VER2_LP_GPP_E5, {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutLow,   GpioIntDis, GpioResumeReset,  GpioTermNone,  GpioOutputStateUnlock }},  //WWAN_RST_N
   {GPIO_VER2_LP_GPP_C5, {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutLow,   GpioIntDis, GpioResumeReset,  GpioTermNone,  GpioOutputStateUnlock }},  //WWAN_PERST_N
   {GPIO_VER2_LP_GPP_D9, {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutLow,   GpioIntDis, GpioResumeReset,  GpioTermNone,  GpioOutputStateUnlock }},  //WWAN_FCP_OFF_N
};

GLOBAL_REMOVE_IF_UNREFERENCED GPIO_INIT_CONFIG mGpioTableAdlMM80WwanEarlyPreMem[] =
{
  {GPIO_VER2_LP_GPP_A8,  { GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,    GpioOutHigh,     GpioIntDis,               GpioResumeReset,    GpioTermNone,  GpioOutputStateUnlock}},  //WWAN_PWREN
  {GPIO_VER2_LP_GPP_D9,  { GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,    GpioOutHigh,     GpioIntDis,               GpioResumeReset,    GpioTermNone,  GpioOutputStateUnlock}},  //WWAN_FCP_OFF_N
  {GPIO_VER2_LP_GPP_E5,  { GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,    GpioOutLow,      GpioIntDis,               GpioResumeReset,    GpioTermNone,  GpioOutputStateUnlock}},  //WWAN_RST_N
  {GPIO_VER2_LP_GPP_C5,  { GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,    GpioOutLow,      GpioIntDis,               GpioResumeReset,    GpioTermNone,  GpioOutputStateUnlock}},  //WWAN_PERST_N
  {GPIO_VER2_LP_GPP_D18, { GpioPadModeGpio, GpioHostOwnGpio, GpioDirInInv,  GpioOutDefault,  GpioIntLevel|GpioIntSci,  GpioHostDeepReset,  GpioTermNone,  GpioPadConfigUnlock}},    //WWAN_WAKE_N
  {GPIO_VER2_LP_GPP_D15, { GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,    GpioOutHigh,     GpioIntDis,               GpioResumeReset,    GpioTermNone,  GpioOutputStateUnlock}},  //WWAN_DISABLE_N
  {GPIO_VER2_LP_GPP_A6,  { GpioPadModeGpio, GpioHostOwnAcpi, GpioDirInInv,  GpioOutDefault,  GpioIntLevel|GpioIntSci,  GpioHostDeepReset,  GpioTermNone,  GpioPadConfigUnlock  }},  //SAR_DPR_PCH
};

GLOBAL_REMOVE_IF_UNREFERENCED GPIO_INIT_CONFIG mGpioTableAdlMLp5AepWwanOnEarlyPreMem[] =
{
  { GPIO_VER2_LP_GPP_A8,  { GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,   GpioOutHigh,     GpioIntDis,                 GpioResumeReset,    GpioTermNone,  GpioOutputStateUnlock } },  //WWAN_PWRE
  { GPIO_VER2_LP_GPP_D9,  { GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,   GpioOutHigh,     GpioIntDis,                 GpioResumeReset,    GpioTermNone,  GpioOutputStateUnlock } },  //WWAN_FCP_OFF_N
  { GPIO_VER2_LP_GPP_E5,  { GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,   GpioOutHigh,     GpioIntDis,                 GpioResumeReset,    GpioTermNone,  GpioOutputStateUnlock } },  //WWAN_RST_N
  { GPIO_VER2_LP_GPP_C5,  { GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,   GpioOutHigh,     GpioIntDis,                 GpioResumeReset,    GpioTermNone,  GpioOutputStateUnlock } },  //WWAN_PERST_N
  { GPIO_VER2_LP_GPP_D18, { GpioPadModeGpio, GpioHostOwnAcpi, GpioDirInInv, GpioOutDefault,  GpioIntLevel | GpioIntSci,  GpioHostDeepReset,  GpioTermNone,  GpioPadConfigUnlock   } },  //WWAN_WAKE_N
  { GPIO_VER2_LP_GPP_D15, { GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,   GpioOutHigh,     GpioIntDis,                 GpioResumeReset,    GpioTermNone,  GpioOutputStateUnlock } },  //WWAN_DISABLE_N
  { GPIO_VER2_LP_GPP_A6,  { GpioPadModeGpio, GpioHostOwnAcpi, GpioDirInInv, GpioOutDefault,  GpioIntLevel | GpioIntSci,  GpioHostDeepReset,  GpioTermNone,  GpioPadConfigUnlock   } },  //SAE_DPR_PCH
  { GPIO_VER2_LP_GPP_F7,  { GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,   GpioOutHigh,     GpioIntDis,                 GpioResumeReset,    GpioTermNone,  GpioPadConfigUnlock   } },  //GNSS_DISABLE
};

#endif // _ALDERLAKE_M_PREMEM_GPIO_TABLE_H_
