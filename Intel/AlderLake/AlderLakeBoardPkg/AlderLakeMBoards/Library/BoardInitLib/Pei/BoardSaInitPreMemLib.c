/** @file
 Source code for the board SA configuration Pcd init functions in Pre-Memory init phase.

@copyright
  INTEL CONFIDENTIAL
  Copyright 2020 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains a 'Sample Driver' and is licensed as such under the terms
  of your license agreement with Intel or your vendor. This file may be modified
  by the user, subject to the additional terms of the license agreement.

@par Specification Reference:
**/

#include "BoardSaConfigPreMem.h"
#include <Library/CpuPlatformLib.h>
#include <Pins/GpioPinsVer2Lp.h>
#include <PlatformBoardId.h>
#include <Library/PreSiliconEnvDetectLib.h>
#include <PlatformBoardConfig.h>
#include <Library/PcdLib.h>
#include <Library/DebugLib.h>

/**
  MRC configuration init function for PEI pre-memory phase.

  @param[in]  VOID

  @retval VOID
**/
VOID
AdlMSaMiscConfigInit (
  VOID
  )
{
  PcdSet8S (PcdSaMiscUserBd, 6);              // btUlxUltType4
  PcdSet16S (PcdSaDdrFreqLimit, 0);

  return;
}

/**
  Board Memory Init related configuration init function for PEI pre-memory phase.

  @param[in]  VOID

  @retval VOID
**/
VOID
AdlMMrcConfigInit (
  VOID
  )
{
  UINT16    BoardId;
  BOOLEAN   ExternalSpdPresent;
  MRC_DQS   *MrcDqs;
  MRC_DQ    *MrcDq;
  SPD_DATA  *SpdData;

  BoardId = PcdGet16(PcdBoardId);

  // SPD is the same size for all boards
  PcdSet16S (PcdMrcSpdDataSize, 512);

  ExternalSpdPresent = PcdGetBool (PcdSpdPresent);

  // Assume internal SPD is used
  PcdSet8S (PcdMrcSpdAddressTable0,  0);
  PcdSet8S (PcdMrcSpdAddressTable1,  0);
  PcdSet8S (PcdMrcSpdAddressTable2,  0);
  PcdSet8S (PcdMrcSpdAddressTable3,  0);
  PcdSet8S (PcdMrcSpdAddressTable4,  0);
  PcdSet8S (PcdMrcSpdAddressTable5,  0);
  PcdSet8S (PcdMrcSpdAddressTable6,  0);
  PcdSet8S (PcdMrcSpdAddressTable7,  0);
  PcdSet8S (PcdMrcSpdAddressTable8,  0);
  PcdSet8S (PcdMrcSpdAddressTable9,  0);
  PcdSet8S (PcdMrcSpdAddressTable10, 0);
  PcdSet8S (PcdMrcSpdAddressTable11, 0);
  PcdSet8S (PcdMrcSpdAddressTable12, 0);
  PcdSet8S (PcdMrcSpdAddressTable13, 0);
  PcdSet8S (PcdMrcSpdAddressTable14, 0);
  PcdSet8S (PcdMrcSpdAddressTable15, 0);

  // Check for external SPD presence on LPDDR4/5 boards
  if (ExternalSpdPresent){
    switch (BoardId) {
      case BoardIdAdlMLp4Rvp:
      case BoardIdAdlMLp5Aep:
      case BoardIdAdlMLp5Rvp:
      case BoardIdAdlMLp5Rvp2a:
      case BoardIdAdlMLp5PmicRvp:
        PcdSet8S (PcdMrcSpdAddressTable0,  0xA8);
        PcdSet8S (PcdMrcSpdAddressTable2,  0xA8);
        PcdSet8S (PcdMrcSpdAddressTable4,  0xA8);
        PcdSet8S (PcdMrcSpdAddressTable6,  0xA8);
        PcdSet8S (PcdMrcSpdAddressTable8,  0xA8);
        PcdSet8S (PcdMrcSpdAddressTable10, 0xA8);
        PcdSet8S (PcdMrcSpdAddressTable12, 0xA8);
        PcdSet8S (PcdMrcSpdAddressTable14, 0xA8);
        break;
      default:
        break;
    }
  }

  // Setting the default DQ Byte Map. It may be overriden to board specific settings below.
  PcdSet32S (PcdMrcDqByteMap, (UINTN) DqByteMapAdlM);
  PcdSet16S (PcdMrcDqByteMapSize, sizeof (DqByteMapAdlM));

  // ADL uses the same RCOMP resistors for all DDR types
  PcdSet32S (PcdMrcRcompResistor, (UINTN) AdlMRcompResistorZero);

  // Use default RCOMP target values for all boards
  PcdSet32S (PcdMrcRcompTarget, (UINTN) RcompTargetAdlM);

  // Default is NIL
  PcdSetBoolS (PcdMrcDqPinsInterleavedControl, TRUE);
  PcdSetBoolS (PcdMrcDqPinsInterleaved, FALSE);

  // DqsMapCpu2Dram is the same size for all boards
  PcdSet16S (PcdMrcDqsMapCpu2DramSize, sizeof (MRC_DQS));
  // DqMapCpu2Dram is the same size for all boards
  PcdSet16S (PcdMrcDqMapCpu2DramSize, sizeof (MRC_DQ));

  switch (BoardId) {
    case BoardIdAdlMLp4Rvp:
      PcdSet8S (PcdMrcCmdMirror, 0xCC);             // Channel 2 and 3 CA bus mirrored on both MCs
      break;
    default:
      PcdSet8S (PcdMrcCmdMirror, 0x00);
      break;
  }

  switch (BoardId) {
   case BoardIdAdlMLp5Rvp:
   case BoardIdAdlMLp5Rvp2a:
   case BoardIdAdlMLp5PmicRvp:
   case BoardIdAdlMLp5Aep:
     PcdSet8S (PcdMrcLp5CccConfig, 0xFF);           // ADL-M LP5 RVP uses descending CCC order
     break;
   default:
     PcdSet8S (PcdMrcLp5CccConfig, 0x0);
     break;
  }

  // CPU-DRAM DQ mapping
  MrcDq = PcdGetPtr (VpdPcdMrcDqMapCpu2Dram);
  if (MrcDq != NULL) {
    PcdSet32S (PcdMrcDqMapCpu2Dram, (UINTN)MrcDq->DqMapCpu2Dram);
  }

  // CPU-DRAM DQS mapping
  MrcDqs = PcdGetPtr (VpdPcdMrcDqsMapCpu2Dram);
  if (MrcDqs != NULL) {
    PcdSet32S (PcdMrcDqsMapCpu2Dram, (UINTN)MrcDqs->DqsMapCpu2Dram);
  }

  // DRAM SPD Data
  SpdData = PcdGetPtr (VpdPcdMrcSpdData);
  if (SpdData != NULL) {
    if (SpdData->OverrideSpd == TRUE) {
      PcdSet32S (PcdMrcSpdData, (UINTN)SpdData->SpdData);
    }
  }

  //
  // CA Vref routing: board-dependent
  // 0 - VREF_CA goes to both CH_A and CH_B (LPDDR3/DDR3L)
  // 1 - VREF_CA to CH_A, VREF_DQ_A to CH_B (should not be used)
  // 2 - VREF_CA to CH_A, VREF_DQ_B to CH_B (DDR4)
  //
  // Set it to 2 for all our DDR4 boards; it is ignored for LPDDR4
  //
  PcdSet8S (PcdMrcCaVrefConfig, 2);

  return;
}

/**
  Board SA related GPIO configuration init function for PEI pre-memory phase.

  @param[in]  VOID

  @retval VOID
**/
VOID
AdlMSaGpioConfigInit (
  VOID
  )
{
  UINT16    BoardId;
  BoardId = PcdGet16(PcdBoardId);

  switch (BoardId) {
    case BoardIdAdlMLp4Rvp:
    case BoardIdAdlMLp5Rvp:
    case BoardIdAdlMLp5Rvp2a:
    case BoardIdAdlMLp5PmicRvp:
    case BoardIdAdlMLp5Aep:
      //
      // Configure CPU M.2 SSD GPIO PCDs
      //
      PcdSet32S (PcdPcieSsd2PwrEnableGpioNo, GPIO_VER2_LP_GPP_D16);
      PcdSet32S (PcdPcieSsd2RstGpioNo, GPIO_VER2_LP_GPP_H13);
      PcdSetBoolS (PcdPcieSsd2PwrEnableGpioPolarity, PIN_GPIO_ACTIVE_HIGH);
      PcdSetBoolS (PcdPcieSsd2RstGpioPolarity, PIN_GPIO_ACTIVE_LOW);
      break;
    default:
      break;
  }

  //
  // PEG PCIE RTD3 GPIO
  //
  PcdSet8S(PcdPcie0GpioSupport, PchGpio);
  PcdSet8S(PcdPcie0HoldRstExpanderNo, 0);
  PcdSet32S(PcdPcie0HoldRstGpioNo, 0);
  PcdSetBoolS(PcdPcie0HoldRstActive, FALSE);
  PcdSet8S(PcdPcie0PwrEnableExpanderNo, 0);
  PcdSet32S(PcdPcie0PwrEnableGpioNo, 0);
  PcdSetBoolS(PcdPcie0PwrEnableActive, FALSE);

  PcdSet8S(PcdPcie1GpioSupport, NotSupported);
  PcdSet8S(PcdPcie1HoldRstExpanderNo, 0);
  PcdSet32S(PcdPcie1HoldRstGpioNo, 0);
  PcdSetBoolS(PcdPcie1HoldRstActive, FALSE);
  PcdSet8S(PcdPcie1PwrEnableExpanderNo, 0);
  PcdSet32S(PcdPcie1PwrEnableGpioNo, 0);
  PcdSetBoolS(PcdPcie1PwrEnableActive, FALSE);

  PcdSet8S(PcdPcie2GpioSupport, NotSupported);
  PcdSet8S(PcdPcie2HoldRstExpanderNo, 0);
  PcdSet32S(PcdPcie2HoldRstGpioNo, 0);
  PcdSetBoolS(PcdPcie2HoldRstActive, FALSE);
  PcdSet8S(PcdPcie2PwrEnableExpanderNo, 0);
  PcdSet32S(PcdPcie2PwrEnableGpioNo, 0);
  PcdSetBoolS(PcdPcie2PwrEnableActive, FALSE);

  PcdSet8S(PcdPcie3GpioSupport, NotSupported);
  PcdSet8S(PcdPcie3HoldRstExpanderNo, 0);
  PcdSet32S(PcdPcie3HoldRstGpioNo, 0);
  PcdSetBoolS(PcdPcie3HoldRstActive, FALSE);
  PcdSet8S(PcdPcie3PwrEnableExpanderNo, 0);
  PcdSet32S(PcdPcie3PwrEnableGpioNo, 0);
  PcdSetBoolS(PcdPcie3PwrEnableActive, FALSE);
  return;
}

/**
  SA Display DDI configuration init function for PEI pre-memory phase.

  @param[in]  VOID

  @retval     VOID
**/
VOID
AdlMSaDisplayConfigInit (
  VOID
  )
{
  UINT16    BoardId;
  UINT16    DisplayId;

  BoardId = PcdGet16(PcdBoardId);
  DisplayId = PcdGet16 (PcdDisplayId);
  DEBUG ((DEBUG_INFO, "BoardId = 0x%x DisplayId = 0x%x\n",BoardId,DisplayId));

  switch (BoardId) {
    case BoardIdAdlMLp4Rvp:
      if (DisplayId == DisplayIdAdlpmDualEdp) {
        DEBUG ((DEBUG_INFO, "DDI Configuration ADLM LP4 Dual EDP\n"));
        PcdSet32S (PcdSaDisplayConfigTable, (UINTN) mAdlMDualEdpDisplayDdiConfig);
        PcdSet16S (PcdSaDisplayConfigTableSize, sizeof (mAdlMDualEdpDisplayDdiConfig));
      } else if (DisplayId == DisplayIdAdlpmMipiExtDisp) {
        DEBUG ((DEBUG_INFO, "DDI Configuration ADLM LP4 MIPI + HDMI\n"));
        PcdSet32S (PcdSaDisplayConfigTable, (UINTN) mAdlMLp4MipiHdmiDisplayDdiConfig);
        PcdSet16S (PcdSaDisplayConfigTableSize, sizeof (mAdlMLp4MipiHdmiDisplayDdiConfig));
      } else if (DisplayId == DisplayIdAdlpmMipiEdp) {
        DEBUG ((DEBUG_INFO, "DDI Configuration ADLM LP4 MIPI + EDP\n"));
        PcdSet32S (PcdSaDisplayConfigTable, (UINTN) mAdlMMipiEdpDisplayDdiConfig);
        PcdSet16S (PcdSaDisplayConfigTableSize, sizeof (mAdlMMipiEdpDisplayDdiConfig));
      } else {
        DEBUG ((DEBUG_INFO, "DDI Configuration ADLM LP4 EDP + HDMI\n"));
        PcdSet32S (PcdSaDisplayConfigTable, (UINTN) mAdlMLp4EdpHdmiDisplayDdiConfig);
        PcdSet16S (PcdSaDisplayConfigTableSize, sizeof (mAdlMLp4EdpHdmiDisplayDdiConfig));
      }
      break;
    case BoardIdAdlMLp5Rvp:
    case BoardIdAdlMLp5Rvp2a:
      if (DisplayId == DisplayIdAdlpmDualEdp) {
        DEBUG ((DEBUG_INFO, "DDI Configuration ADLM LP5 Dual EDP\n"));
        PcdSet32S (PcdSaDisplayConfigTable, (UINTN) mAdlMDualEdpDisplayDdiConfig);
        PcdSet16S (PcdSaDisplayConfigTableSize, sizeof (mAdlMDualEdpDisplayDdiConfig));
      } else if (DisplayId == DisplayIdAdlpmMipiExtDisp) {
        DEBUG ((DEBUG_INFO, "DDI Configuration ADLM LP5 MIPI + DP\n"));
        PcdSet32S (PcdSaDisplayConfigTable, (UINTN) mAdlMMipiDpDisplayDdiConfig);
        PcdSet16S (PcdSaDisplayConfigTableSize, sizeof (mAdlMMipiDpDisplayDdiConfig));
      } else if (DisplayId == DisplayIdAdlpmMipiEdp) {
        DEBUG ((DEBUG_INFO, "DDI Configuration ADLM LP5 MIPI + EDP\n"));
        PcdSet32S (PcdSaDisplayConfigTable, (UINTN) mAdlMMipiEdpDisplayDdiConfig);
        PcdSet16S (PcdSaDisplayConfigTableSize, sizeof (mAdlMMipiEdpDisplayDdiConfig));
      } else {
        DEBUG ((DEBUG_INFO, "DDI Configuration ADLM LP5 EDP + DP\n"));
        PcdSet32S (PcdSaDisplayConfigTable, (UINTN) mAdlMEdpDpDisplayDdiConfig);
        PcdSet16S (PcdSaDisplayConfigTableSize, sizeof (mAdlMEdpDpDisplayDdiConfig));
      }
      break;
    case BoardIdAdlMLp5Aep:
      DEBUG ((DEBUG_INFO, "DDI Configuration ADLM LP5 AEP EDP\n"));
      PcdSet32S (PcdSaDisplayConfigTable, (UINTN) mAdlMEdpDisplayDdiConfig);
      PcdSet16S (PcdSaDisplayConfigTableSize, sizeof (mAdlMEdpDisplayDdiConfig));
      break;
    case BoardIdAdlMLp5PmicRvp:
      DEBUG ((DEBUG_INFO, "DDI Configuration ADLM LP5 Dual MIPI\n"));
      PcdSet32S (PcdSaDisplayConfigTable, (UINTN) mAdlMDualMipiDisplayDdiConfig);
      PcdSet16S (PcdSaDisplayConfigTableSize, sizeof (mAdlMDualMipiDisplayDdiConfig));
    default:
      break;
  }

  return;
}

/**
  Board USB related configuration init function for PEI pre-memory phase.

  @param[in]  VOID

  @retval VOID
**/
VOID
AdlMSaUsbConfigInit (
  VOID
  )
{
  UINT16    BoardId;
  BoardId = PcdGet16(PcdBoardId);

  switch (BoardId) {
    //
    // Override ALL port setting if required.
    //
    case BoardIdAdlMLp4Rvp:
    case BoardIdAdlMLp5Rvp:
    case BoardIdAdlMLp5Rvp2a:
    case BoardIdAdlMLp5PmicRvp:
      PcdSet8S (PcdCpuXhciPortSupportMap, 0x03);
      PcdSet8S (PcdITbtRootPortNumber, 2);
      PcdSet8S (PcdCvfUsbPort, 0x06);
      break;
    case BoardIdAdlMLp5Aep:
      PcdSet8S (PcdCpuXhciPortSupportMap, 0x03);
      PcdSet8S (PcdITbtRootPortNumber, 2);
      PcdSet8S (PcdCvfUsbPort, 0x03);
      break;
    default:
      break;
  }
  //
  // Update Cpu Xhci Port Enable Map PCD based on SaSetup Data and PcdCpuXhciPortSupportMap.
  //
  TcssUpdateCpuXhciPortEnableMapPcd ();

  return;
}
