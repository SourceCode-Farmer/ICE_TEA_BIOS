/** @file

;******************************************************************************
;* Copyright (c) 2021, Insyde Software Corporation. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/
/** @file

@copyright
  INTEL CONFIDENTIAL
  Copyright 2020 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains a 'Sample Driver' and is licensed as such under the terms
  of your license agreement with Intel or your vendor. This file may be modified
  by the user, subject to the additional terms of the license agreement.

@par Specification Reference:
**/

#include <PiPei.h>
#include <Library/BaseMemoryLib.h>
#include <Library/BiosIdLib.h>
#include <Library/PcdLib.h>
#include <Library/DebugLib.h>
#include <Library/PciLib.h>
#include <Library/MemoryAllocationLib.h>
#include <Library/PeiLib.h>
#include <Guid/MemoryOverwriteControl.h>
#include <Library/MmioInitLib.h>
#include <PlatformBoardConfig.h>
#include <Library/SiliconInitLib.h>
#include <Library/PchCycleDecodingLib.h>
#include <Register/PmcRegs.h>
#include <Library/PmcLib.h>
#include <Library/PeiBootModeLib.h>
#include <Ppi/ReadOnlyVariable2.h>
#include <Library/PeiServicesLib.h>
#include <Library/WakeupEventLib.h>
#include <Library/GpioLib.h>
#include <Library/BoardConfigLib.h>
#include <Library/TimerLib.h>
#include <AlderLakeMRvpBoardConfigPatchTable.h>
#include <PlatformBoardId.h>
#include <SioRegs.h>
#include <Library/IoLib.h>
#include <Pins/GpioPinsVer2Lp.h>
#include <Library/PmcLib.h>
#include <CastroCovePmicRegisters.h>
#include <PlatformBoardId.h>
#include <Library/PreSiliconEnvDetectLib.h>
#include <Library/PchInfoLib.h>
#include <Library/EcMiscLib.h>
//[-start-210809-IB18410076-add]//
#include <Library/GpioCfgLib.h>
#include <Library/GpioNativeLib.h>
#include <Library/PeiOemSvcKernelLib.h>
#include <Library/PeiOemSvcChipsetLib.h>
//[-end-210809-IB18410076-add]//

#define SIO_RUNTIME_REG_BASE_ADDRESS      0x0680

static PMIC_REG_INIT mCsc1ImonPmicTableRvp[] = {
  // Configure CSC1 PMIC IMON
  { R_CASTRO_COVE_CM_TSPAN_CTRL_REG, RegOverride, 0x60, 0x00, 00 },
  { R_CASTRO_COVE_CM_SMPS1_CTRL_REG, RegOverride, 0xE0, 0x05, 00 },
  { R_CASTRO_COVE_CM_SMPS7_CTRL_REG, RegOverride, 0xE0, 0x05, 00 },
  { R_CASTRO_COVE_CM_SMPS8_CTRL_REG, RegOverride, 0xE0, 0x05, 00 }
};

static PMIC_REG_INIT mCsc2ImonPmicTableRvp[] = {
  // Configure CSC2 PMIC IMON
  { R_CASTRO_COVE_CM_TSPAN_CTRL_REG, RegOverride, 0x60, 0x00, 00 },
  { R_CASTRO_COVE_CM_SMPS3_CTRL_REG, RegOverride, 0xE0, 0x05, 00 },
  { R_CASTRO_COVE_CM_SMPS5_CTRL_REG, RegOverride, 0xE0, 0x05, 00 },
  { R_CASTRO_COVE_CM_SMPS6_CTRL_REG, RegOverride, 0xE0, 0x05, 00 },
  { R_CASTRO_COVE_CM_SMPS7_CTRL_REG, RegOverride, 0xE0, 0x05, 00 },
  { R_CASTRO_COVE_CM_SMPS8_CTRL_REG, RegOverride, 0xE0, 0x05, 00 },
  { R_CASTRO_COVE_CM_SMPS9_CTRL_REG, RegOverride, 0xE0, 0x05, 00 }
};

static PMIC_REG_INIT mCsc3ImonPmicTableRvp[] = {
  // Configure CSC3 PMIC IMON
  { R_CASTRO_COVE_CM_TSPAN_CTRL_REG, RegOverride, 0x60, 0x00, 00 },
  { R_CASTRO_COVE_CM_SMPS1_CTRL_REG, RegOverride, 0xE0, 0x05, 00 },
  { R_CASTRO_COVE_CM_SMPS2_CTRL_REG, RegOverride, 0xE0, 0x05, 00 },
  { R_CASTRO_COVE_CM_SMPS3_CTRL_REG, RegOverride, 0xE0, 0x05, 00 },
  { R_CASTRO_COVE_CM_SMPS4_CTRL_REG, RegOverride, 0xE0, 0x05, 00 },
  { R_CASTRO_COVE_CM_SMPS5_CTRL_REG, RegOverride, 0xE0, 0x05, 00 },
  { R_CASTRO_COVE_CM_SMPS6_CTRL_REG, RegOverride, 0xE0, 0x05, 00 },
  { R_CASTRO_COVE_CM_SMPS7_CTRL_REG, RegOverride, 0xE0, 0x05, 00 }
};

static EFI_PEI_PPI_DESCRIPTOR mSetupVariablesReadyPpi = {
  (EFI_PEI_PPI_DESCRIPTOR_PPI | EFI_PEI_PPI_DESCRIPTOR_TERMINATE_LIST),
  &gSetupVariablesReadyPpiGuid,
  NULL
};

/**
  Alderlake M boards configuration init function for PEI pre-memory phase.

  @retval EFI_SUCCESS             The function completed successfully.
**/
EFI_STATUS
EFIAPI
AdlMInitPreMem (
  VOID
  )
{
  EFI_STATUS                        Status;
  UINTN                             VariableSize;
  VOID                              *MemorySavedData;
  UINT8                             MorControl;
  VOID                              *MorControlPtr;
  EFI_PEI_READ_ONLY_VARIABLE2_PPI   *VariableServices;

  Status = PeiServicesLocatePpi (
             &gEfiPeiReadOnlyVariable2PpiGuid,
             0,
             NULL,
             (VOID **) &VariableServices
             );
  ASSERT_EFI_ERROR (Status);
  //
  // Initialize S3 Data variable (S3DataPtr). It may be used for warm and fast boot paths.
  //
  VariableSize = 0;
  MemorySavedData = NULL;
  Status = VariableServices->GetVariable (
                               VariableServices,
                               L"MemoryConfig",
                               &gFspNonVolatileStorageHobGuid,
                               NULL,
                               &VariableSize,
                               MemorySavedData
                               );
  if (Status == EFI_BUFFER_TOO_SMALL) {
    //
    // Set the DISB bit
    // after memory Data is saved to NVRAM.
    //
    PmcSetDramInitScratchpad ();
  }

  //
  // MOR
  //
  MorControl = 0;
  MorControlPtr = &MorControl;
  VariableSize = sizeof (MorControl);
  Status = PeiGetVariable (
             MEMORY_OVERWRITE_REQUEST_VARIABLE_NAME,
             &gEfiMemoryOverwriteControlDataGuid,
             &MorControlPtr,
             &VariableSize
             );
  DEBUG ((DEBUG_INFO, "MorControl - 0x%x (%r)\n", MorControl, Status));
  if (MOR_CLEAR_MEMORY_VALUE (MorControl)) {
    PcdSet8S (PcdCleanMemory, MorControl & MOR_CLEAR_MEMORY_BIT_MASK);
  }

  PcdSet32S (PcdStackBase, PcdGet32 (PcdTemporaryRamBase) + PcdGet32 (PcdTemporaryRamSize) - (PcdGet32 (PcdFspTemporaryRamSize) + PcdGet32 (PcdFspReservedBufferSize)));
  PcdSet32S (PcdStackSize, PcdGet32 (PcdFspTemporaryRamSize));

  PcdSet8S (PcdCpuRatio, 0x0);
  PcdSet8S (PcdBiosGuard, 0x0);

  return EFI_SUCCESS;
}

/**
  Updates the wakeupType.
**/
VOID
AdlMWakeUpTypeUpdate (
  VOID
  )
{
  UINT8   WakeupType;
  //
  // Updates the wakeupType which will be used to update the same in Smbios table 01
  //
  GetWakeupEvent (&WakeupType);
  PcdSet8S (PcdWakeupType, WakeupType);
}

VOID
AdlMMrcConfigInit (
  VOID
  );

VOID
AdlMSaMiscConfigInit (
  VOID
  );

VOID
AdlMSaGpioConfigInit (
  VOID
  );

VOID
AdlMSaDisplayConfigInit (
  VOID
  );

VOID
AdlMSaUsbConfigInit (
  VOID
  );

EFI_STATUS
AdlMRootPortClkInfoInit (
  VOID
  );

EFI_STATUS
AdlMUsbConfigInit (
  VOID
  );

VOID
AdlMGpioTablePreMemInit(
  VOID
  );

VOID
AdlMGpioGroupTierInit (
  VOID
  );

/**
  HSIO init function for PEI pre-memory phase.
**/
VOID
AdlMHsioInit (
  VOID
  )
{
}

/**
  Configure Super IO
**/
STATIC
VOID
AdlMSioInit (
  VOID
  )
{
  //
  // Program and Enable Default Super IO Configuration Port Addresses and range
  //
  PchLpcGenIoRangeSet (PcdGet16 (PcdLpcSioConfigDefaultPort) & (~0xF), 0x10);

  //
  // Enable LPC decode for KCS and mailbox SIO for iBMC communication
  //
  if (PcdGet8 (PcdPlatformFlavor) == FlavorUpServer) {
    PchLpcGenIoRangeSet (BMC_KCS_BASE_ADDRESS, 0x10);
    PchLpcGenIoRangeSet (PILOTIII_MAILBOX_BASE_ADDRESS, 0x10);
  } else {
  //
  // 128 Byte Boundary and SIO Runtime Register Range is 0x0 to 0xF;
  //
    PchLpcGenIoRangeSet (SIO_RUNTIME_REG_BASE_ADDRESS  & (~0x7F), 0x10);
  }

  //
  // We should not depend on SerialPortLib to initialize KBC for legacy USB
  // So initialize KBC for legacy USB driver explicitly here.
  // After we find how to enable mobile KBC, we will add enabling code for mobile then.
  //
  if ((PcdGet8 (PcdPlatformFlavor) == FlavorDesktop) ||
      (PcdGet8 (PcdPlatformFlavor) == FlavorWorkstation)) {
    //
    // Enable KBC for National PC8374 SIO
    //
    if (PcdGetBool (PcdPc8374SioKbcPresent)) {
      IoWrite8 (PcdGet16 (PcdDesktopLpcSioIndexDefaultPort), 0x07);
      IoWrite8 (PcdGet16 (PcdDesktopLpcSioDataDefaultPort), 0x06);
      IoWrite8 (PcdGet16 (PcdDesktopLpcSioIndexDefaultPort), 0x30);
      IoWrite8 (PcdGet16 (PcdDesktopLpcSioDataDefaultPort), 0x01);
    }
  }

  return;
}

/**
  Notifies the gPatchConfigurationDataPreMemPpiGuid has been Installed

  @param[in] PeiServices          General purpose services available to every PEIM.
  @param[in] NotifyDescriptor     The notification structure this PEIM registered on install.
  @param[in] Ppi                  The memory discovered PPI.  Not used.

  @retval EFI_SUCCESS             The function completed successfully.
**/
EFI_STATUS
EFIAPI
AdlMBoardPatchConfigurationDataPreMemCallback (
  IN EFI_PEI_SERVICES           **PeiServices,
  IN EFI_PEI_NOTIFY_DESCRIPTOR  *NotifyDescriptor,
  IN VOID                       *Ppi
  )
{
//[-start-200225-IB14630334-remove]//
  // UINTN   ConfigPatchStructSize;
  // UINT16    BoardId;
  // BoardId = PcdGet16(PcdBoardId);

  // switch (BoardId) {
  //   case BoardIdAdlMLp4Rvp:
  //   case BoardIdAdlMLp5Rvp:
  //   case BoardIdAdlMLp5Rvp2a:
  //     ConfigPatchStructSize = SIZE_OF_TABLE (mAlderLakeMLp4RvpConfigPatchStruct, CONFIG_PATCH_STRUCTURE);
  //     PatchSetupConfigurationDataPreMem (mAlderLakeMLp4RvpConfigPatchStruct, ConfigPatchStructSize);
  //     ConfigPatchStructSize = SIZE_OF_TABLE (mAlderLakeMAuxOverrideEnableConfigPatchStruct, CONFIG_PATCH_STRUCTURE);
  //     PatchSetupConfigurationDataPreMem (mAlderLakeMAuxOverrideEnableConfigPatchStruct, ConfigPatchStructSize);
  //    ConfigPatchStructSize = SIZE_OF_TABLE (mAlderLakeMWoVEnableConfigPatchStruct, CONFIG_PATCH_STRUCTURE);
  //    PatchSetupConfigurationDataPreMem (mAlderLakeMWoVEnableConfigPatchStruct, ConfigPatchStructSize);
  //     break;
  //   case BoardIdAdlMLp5Aep:
  //     ConfigPatchStructSize = SIZE_OF_TABLE (mAlderLakeMLp4RvpConfigPatchStruct, CONFIG_PATCH_STRUCTURE);
  //     PatchSetupConfigurationDataPreMem (mAlderLakeMLp4RvpConfigPatchStruct, ConfigPatchStructSize);
  //     break;
  //   case BoardIdAdlMLp5PmicRvp:
  //     ConfigPatchStructSize = SIZE_OF_TABLE (mAlderLakeMLp5PmicRvpConfigPatchStruct, CONFIG_PATCH_STRUCTURE);
  //     PatchSetupConfigurationDataPreMem (mAlderLakeMLp5PmicRvpConfigPatchStruct, ConfigPatchStructSize);
  //    ConfigPatchStructSize = SIZE_OF_TABLE (mAlderLakeMWoVEnableConfigPatchStruct, CONFIG_PATCH_STRUCTURE);
  //    PatchSetupConfigurationDataPreMem (mAlderLakeMWoVEnableConfigPatchStruct, ConfigPatchStructSize);
  //     break;
  //   default:
  //     break;
  // }

  // //
  // //
  // if (IsSimicsEnvironment ()) {
  //   ConfigPatchStructSize = SIZE_OF_TABLE (mAlderlakeMSimicsConfigPatchStruct, CONFIG_PATCH_STRUCTURE);
  //   PatchSetupConfigurationDataPreMem (mAlderlakeMSimicsConfigPatchStruct, ConfigPatchStructSize);
  // }

  // if (PchStepping () >= PCH_A1) {
  //   ConfigPatchStructSize = SIZE_OF_TABLE (mAlderLakeMPchSteppingConfigPatchStruct, CONFIG_PATCH_STRUCTURE);
  //   PatchSetupConfigurationDataPreMem (mAlderLakeMPchSteppingConfigPatchStruct, ConfigPatchStructSize);
  // }
//[-end-200225-IB14630334-remove]//
  PeiServicesInstallPpi (&mSetupVariablesReadyPpi);

  return RETURN_SUCCESS;
}

/**
  Board Misc init function for PEI pre-memory phase.
**/
VOID
AdlMBoardMiscInitPreMem (
  VOID
  )
{
  PCD64_BLOB PcdData;
  UINT16    BoardId;
  BoardId = PcdGet16(PcdBoardId);

  //
  // RecoveryMode GPIO
  //
  PcdData.Blob = 0;
  PcdData.BoardGpioConfig.Type = BoardGpioTypeNotSupported;
  PcdSet64S (PcdRecoveryModeGpio, PcdData.Blob);

  //
  // OddPower Init
  //
  PcdSetBoolS (PcdOddPowerInitEnable, FALSE);

  //
  // Pc8374SioKbc Present
  //
  PcdSetBoolS (PcdPc8374SioKbcPresent, FALSE);

  //
  // Smbus Alert function Init.
  //
  PcdSetBoolS (PcdSmbusAlertEnable, FALSE);

  //
  // Configure WWAN Full Card Power Off and reset pins
  //
  switch (BoardId) {
    case BoardIdAdlMLp4Rvp:
    case BoardIdAdlMLp5Rvp:
    case BoardIdAdlMLp5Rvp2a:
    case BoardIdAdlMLp5PmicRvp:
    case BoardIdAdlMLp5Aep:
      PcdSet32S (PcdWwanFullCardPowerOffGpio, GPIO_VER2_LP_GPP_D9);
      PcdSet32S (PcdWwanBbrstGpio, GPIO_VER2_LP_GPP_E5);
      PcdSet32S (PcdWwanPerstGpio, GPIO_VER2_LP_GPP_C5);
      PcdSet32S (PcdWwanWakeGpio, GPIO_VER2_LP_GPP_D18);
      PcdSetBoolS (PcdWwanPerstGpioPolarity, PIN_GPIO_ACTIVE_LOW);
      PcdSetBoolS (PcdWwanFullCardPowerOffGpioPolarity, PIN_GPIO_ACTIVE_HIGH);
      PcdSetBoolS (PcdWwanBbrstGpioPolarity, PIN_GPIO_ACTIVE_LOW);
      PcdSet8S (PcdWwanSourceClock, 5);
      PcdSet8S (PcdWwanRootPortNumber, 0x04);
      break;
  }

  //EC FailSafe Cpu Temp and Fan Speed Setting
  PcdSet8S (PcdEcFailSafeActionCpuTemp, 85);
  PcdSet8S (PcdEcFailSafeActionFanPwm, 65);
}

/**
CscPmicImon Configure
**/
EFI_STATUS
EFIAPI
AdlMBoardCscImonPmicConfgiure (
  IN PMIC_REG_INIT   *PmicRegInit,
  IN UINT16          Length,
  IN UINT8           PmicId
  )
{
  EFI_STATUS                Status;
  UINT8                     PmicRegValue;
  UINT8                     Value;

  Status = EFI_SUCCESS;

  DEBUG((DEBUG_ERROR, "PmicRegisterInit :%r", Status));
  if (!(PmicRegInit) || !(Length)) {
    Status = EFI_INVALID_PARAMETER;
    DEBUG ((DEBUG_ERROR, "PmicRegisterInit :%r", Status));
    return Status;
  }

  for (UINT8 Index = 0; Index < Length; Index++) {
    DEBUG ((DEBUG_INFO, "PmicRegisterInit Offset:0x%X Value to write :0x%X\n", PmicRegInit[Index].Offset, PmicRegInit[Index].Value));
    Status = PmicRead8 (PmicRegInit[Index].Offset, &PmicRegValue, PmicId);
    if (EFI_ERROR(Status)) {
      continue;
    }
    if (!PmicRegInit[Index].Mask) {
      continue;
    }
    switch (PmicRegInit[Index].Action) {
      case RegOverride: {
        PmicRegValue &= PmicRegInit[Index].Mask;
        PmicRegValue |= (PmicRegInit[Index].Value & ~(PmicRegInit[Index].Mask));
        Status = PmicWrite8 (PmicRegInit[Index].Offset, PmicRegValue, PmicId);
        break;
      }
      case RegClear: {
        PmicRegValue &= PmicRegInit[Index].Mask;
        Status = PmicWrite8 (PmicRegInit[Index].Offset, PmicRegValue, PmicId);
        break;
      }
      case RegAnd: {
        PmicRegValue &= (PmicRegInit[Index].Value & ~(PmicRegInit[Index].Mask));
        Status = PmicWrite8 (PmicRegInit[Index].Offset, PmicRegValue, PmicId);
        break;
      }
      case RegOr: {
        PmicRegValue |= (PmicRegInit[Index].Value & ~(PmicRegInit[Index].Mask));
        Status = PmicWrite8 (PmicRegInit[Index].Offset, PmicRegValue, PmicId);
        break;
      }
    }
    if (EFI_ERROR(Status)) {
      continue;
    }
    if (PmicRegInit[Index].Delay != 0) {
      MicroSecondDelay(PmicRegInit[Index].Delay);
    }
    Status = PmicRead8 (PmicRegInit[Index].Offset, &Value, PmicId);
    DEBUG((DEBUG_INFO, "PmicRegisterInit Offset:0x%X Value:0x%X Rbuf:0x%X\n", \
    PmicRegInit[Index].Offset, PmicRegValue, Value));
  }
  return Status;
}

/**
This function configures PMIC Update Flow Based on setup Configuration.
**/
VOID
PmicUpdateSetupConfiguration (
  VOID
)
{
  EFI_STATUS                       Status;
  SETUP_DATA                       SetupData;
  UINTN                            VariableSize;
  EFI_PEI_READ_ONLY_VARIABLE2_PPI  *VariableServices;

  // Updating PMIC NVM update control settings to PCD

  Status = PeiServicesLocatePpi (
    &gEfiPeiReadOnlyVariable2PpiGuid,  // GUID
    0,                                 // INSTANCE
    NULL,                              // EFI_PEI_PPI_DESCRIPTOR
    (VOID **)&VariableServices         // PPI
    );
  ASSERT_EFI_ERROR(Status);

  VariableSize = sizeof (SETUP_DATA);
  Status = VariableServices->GetVariable (
    VariableServices,
    L"Setup",
    &gSetupVariableGuid,
    NULL,
    &VariableSize,
    &SetupData
    );
  ASSERT_EFI_ERROR(Status);

  PcdSetBoolS (PcdCrcPmicNvmUpdateEnable, SetupData.CrcPmicNvmUpdateEnable);
  PcdSetBoolS (PcdCrcPmicCustomizationNvmUpdateEnable, SetupData.CrcPmicCustomizationNvmUpdateEnable);
  PcdSetBoolS(PcdPmicNvmWriteLockEnable, SetupData.PmicNvmWriteLockEnable);
  DEBUG((DEBUG_INFO, "SetupData.CrcPmicNvmUpdateEnable %x \n", SetupData.CrcPmicNvmUpdateEnable));
  DEBUG((DEBUG_INFO, "SetupData.CrcPmicCustomizationNvmUpdateEnable %x \n", SetupData.CrcPmicCustomizationNvmUpdateEnable));
  DEBUG((DEBUG_INFO, "SetupData.PmicNvmWriteLockEnable %x \n", SetupData.PmicNvmWriteLockEnable));
}

/**
  Notify Ec To Pd firmware for enabling or disabling Pcie.
**/
VOID
AdlMNotifyEcToPdForPcieTunnel (
  VOID
  )
{
  EFI_STATUS                        Status;
  EFI_BOOT_MODE                     BootMode;
  EFI_PEI_READ_ONLY_VARIABLE2_PPI   *VariableServices;
  SETUP_DATA                        SetupData;
  UINTN                             VariableSize;

  PeiServicesGetBootMode (&BootMode);

  //
  // PD will keep original contents during Sx state.
  // It doesn't need to do anything during these states.
  //
  if ((BootMode == BOOT_ON_S3_RESUME) ||
      (BootMode == BOOT_ON_S4_RESUME) ||
      (BootMode == BOOT_ON_S5_RESUME)) {
    return;
  }

  Status = PeiServicesLocatePpi (
             &gEfiPeiReadOnlyVariable2PpiGuid,
             0,
             NULL,
             (VOID **) &VariableServices
             );
  ASSERT_EFI_ERROR (Status);

  VariableSize = sizeof (SETUP_DATA);
  Status = VariableServices->GetVariable (
                               VariableServices,
                               L"Setup",
                               &gSetupVariableGuid,
                               NULL,
                               &VariableSize,
                               (VOID *) &SetupData
                               );

  ASSERT_EFI_ERROR (Status);

  if (EFI_ERROR (Status)) {
    DEBUG ((DEBUG_ERROR, "Get Setup variable fail Status = %r\n", Status));
    return;
  }

  //
  // PCIE Tunnel En/Dis function. PD is set enable PCIE Tunnel by default.
  // Ideally BIOS send command when option is disable.
  //
  NotifyEcToPdForPcieTunnel (SetupData.EnablePcieTunnelingOverUsb4);

  return;
}

//[-start-210809-IB18410076-add]//
STATIC
EFI_STATUS
GpioCfgPei (
//[-start-210120-IB16560239-modify]//
  IN H2O_BOARD_ID               Board,
//[-end-210120-IB16560239-modify]//
  IN BOOLEAN                    PreMemFlag
  )
{
  EFI_STATUS                    Status;
  H2O_GPIO_INIT_CONFIG          *GpioInitStruct;
  UINT32                        GpioInitStructCount;
  UINT32                        GpioTableCount;
  GPIO_INIT_CONFIG              *GpioDefinitionTable;
  H2O_GPIO_INIT_CONFIG          SingleGpioInitStruct = {0};
  UINT32                        Index;
  UINT32                        PadMaxSize;
  GPIO_GROUP                    Group;

  PadMaxSize = 0;
  for (Group = GpioGetLowestGroup(); Group <= GpioGetHighestGroup(); Group ++) {    
    PadMaxSize += GpioGetPadPerGroup (Group);
  }

  GpioDefinitionTable = NULL;
  GpioDefinitionTable = (GPIO_INIT_CONFIG *)AllocateZeroPool (PadMaxSize * sizeof (GPIO_INIT_CONFIG));
  if (GpioDefinitionTable == NULL) {
    return EFI_OUT_OF_RESOURCES;
  }
  //
  // Use GetGpioCfg in GpioCfgLib to get build-time GPIO configuration
  // Input Board value corresponds to Skuids in Project.dsc
  //
  Status = GetGpioCfg (Board, &GpioInitStruct, &GpioInitStructCount);
  ASSERT_EFI_ERROR (Status);
  if (EFI_ERROR (Status)) {
    return EFI_NOT_FOUND;
  }

  GpioTableCount = 0;
  Status = EFI_UNSUPPORTED;
  for (Index = 0; Index < GpioInitStructCount; Index++) {
    if (GpioInitStruct[Index].Flag != PreMemFlag) {
      continue;
    }
    CopyMem (&SingleGpioInitStruct, &GpioInitStruct[Index], sizeof(H2O_GPIO_INIT_CONFIG));
    //
    // OemServices
    // This function works when PCT being FALSE or PCT's setting being null.
    // Please refer to Power On Guide 0.4.
    //
    DEBUG ((DEBUG_INFO, "OemKernelServices Call: OemSvcUpdateGpioCfg \n"));
    Status = OemSvcUpdateGpioCfg (&SingleGpioInitStruct);
    DEBUG ((DEBUG_INFO, "OemKernelServices OemSvcUpdateGpioCfg Status: %r\n", Status));
    if (Status != EFI_SUCCESS) {
     CopyMem (&GpioDefinitionTable[GpioTableCount], &SingleGpioInitStruct, sizeof(GPIO_INIT_CONFIG));
     GpioTableCount++;
    }
  }
  if ((Status == EFI_SUCCESS) && (GpioTableCount == 0x00)) {
    return EFI_SUCCESS;
  }

  //
  // Batch process GPIO configuraions.
  //
  DEBUG ((DEBUG_INFO, "ConfigureGpio() Start\n"));
  Status = GpioConfigurePads (GpioTableCount, GpioDefinitionTable);
  DEBUG ((DEBUG_INFO, "ConfigureGpio() End\n"));

  return EFI_SUCCESS;
}
//[-end-210809-IB18410076-add]//
/**
  A hook for board-specific initialization prior to memory initialization.

  @retval EFI_SUCCESS   The board initialization was successful.
**/
EFI_STATUS
EFIAPI
AdlMBoardInitBeforeMemoryInit (
  VOID
  )
{
  EFI_STATUS                        Status;
  UINT16                            BoardId;
  UINT16                            BomId;
  EFI_PEI_READ_ONLY_VARIABLE2_PPI   *VariableServices;
  PCH_SETUP                         PchSetup;
  UINTN                             DataSize;
  GPIO_CONFIG                       PadConfig;
//[-start-210809-IB18410076-add]//  
  EFI_STATUS	      OemSvcStatus;
  UINT16            GpioTableSizePreMem;
  GPIO_INIT_CONFIG  *GpioTablePreMem;
  
  BoardId = PcdGet16(PcdBoardId);
//[-end-210809-IB18410076-add]//  

  Status = PeiServicesLocatePpi (
             &gEfiPeiReadOnlyVariable2PpiGuid,
             0,
             NULL,
             (VOID **) &VariableServices
             );
  DataSize = sizeof (PCH_SETUP);
  Status = VariableServices->GetVariable (
                               VariableServices,
                               L"PchSetup",
                               &gPchSetupVariableGuid,
                               NULL,
                               &DataSize,
                               &PchSetup
                               );
  DEBUG ((DEBUG_INFO, "AdlMBoardInitBeforeMemoryInit\n"));

  //[-start-191111-IB10189001-remove]//
  // GetBiosId (NULL);
  //[-end-191111-IB10189001-remove]//
  BoardId = PcdGet16 (PcdBoardId);
  BomId   = PcdGet16 (PcdBoardBomId);

  ZeroMem (&PadConfig, sizeof (GPIO_CONFIG));
  if (PchSetup.CpuRootportUsedForHybridStorage != 0xFF) {
    PadConfig.Direction = GpioDirOut;
    PadConfig.PadMode = GpioPadModeGpio;
    if (PchSetup.CpuHybridStorageConnected == 0) {
      PadConfig.OutputState = GpioOutHigh;
    } else {
      PadConfig.OutputState = GpioOutLow;
    }
    Status = GpioSetPadConfig(GPIO_VER2_LP_GPP_H15, &PadConfig);
  }
  AdlMInitPreMem ();

  AdlMWakeUpTypeUpdate ();
  AdlMBoardMiscInitPreMem ();
  AdlMSioInit ();

  ///
  /// Do basic PCH init
  ///
  SiliconInit ();

  AdlMGpioGroupTierInit ();
  AdlMGpioTablePreMemInit ();

  AdlMHsioInit ();
  AdlMMrcConfigInit ();
  AdlMSaGpioConfigInit ();
  AdlMSaMiscConfigInit ();
  Status = AdlMRootPortClkInfoInit ();
  Status = AdlMUsbConfigInit ();
  AdlMSaDisplayConfigInit ();
  AdlMSaUsbConfigInit ();
  PmicUpdateSetupConfiguration();

  // Configure GPIO Before Memory is not ready
//[-start-210120-IB16560239-remove]//
//  GpioInit (PcdGetPtr (PcdBoardGpioTablePreMem));
//[-end-210120-IB16560239-remove]//

//[-start-210809-IB18410076-add]//
  if (PcdGetBool (PcdH2OGpioCfgSupported) && !PcdGetBool(PcdDisableVpdGpioTable)) {
    //
    // Program GPIOs, Input Board value corresponds to Skuids in Project.dsc
    //
    DEBUG ((DEBUG_INFO, "GpioCfgPei() Start\n"));
//[-start-210120-IB16560239-modify]//
    GpioCfgPei ((H2O_BOARD_ID)BoardId, TRUE);
//[-end-210120-IB16560239-modify]//
    DEBUG ((DEBUG_INFO, "GpioCfgPei() END\n"));
  } else {
    GpioTablePreMem = PcdGetPtr (PcdBoardGpioTablePreMem);
    GetGpioTableSize (GpioTablePreMem, &GpioTableSizePreMem);
    DEBUG_OEM_SVC ((DEBUG_INFO, "Pei OemChipsetServices Call: OemSvcModifyGpioSettingTablePreMem \n"));
    OemSvcStatus = OemSvcModifyGpioSettingTablePreMem (&GpioTablePreMem, &GpioTableSizePreMem);
    DEBUG_OEM_SVC ((DEBUG_INFO, "Pei OemChipsetServices OemSvcModifyGpioSettingTablePreMem Status: %r\n", OemSvcStatus));
    if (GpioTableSizePreMem != 0 && OemSvcStatus != EFI_SUCCESS) {
      GpioConfigurePads ((UINT32) GpioTableSizePreMem, GpioTablePreMem);
    }
  }
//[-end-210809-IB18410076-add]//
  // Configure GPIO group GPE tier
  GpioGroupTierInit ();
  //
  //BoardID=0x2 is the board,which have 1 PMIC,
  //BoardID=0x3 is the board,which have 2 PMIC
  //
  DEBUG((DEBUG_ERROR, "AdlMBoardCscImonPmicConfgiure 1 "));
  if (BoardId == BoardIdAdlMLp5PmicRvp) {
    DEBUG((DEBUG_ERROR, "AdlMBoardCscImonPmicConfgiure 2 "));
    AdlMBoardCscImonPmicConfgiure((VOID *)(UINTN)mCsc1ImonPmicTableRvp, (UINT16)sizeof(mCsc1ImonPmicTableRvp) / sizeof(PMIC_REG_INIT), (UINT8)0x1);
    AdlMBoardCscImonPmicConfgiure((VOID *)(UINTN)mCsc2ImonPmicTableRvp, (UINT16)sizeof(mCsc2ImonPmicTableRvp) / sizeof(PMIC_REG_INIT), (UINT8)0x2);
  }
  //Temporary fix. Debug Bios is asserting on ADL M RVP2 VR based board.
  else if ((BoardId == BoardIdAdlMLp5Rvp) && (BomId == BomIdAdlMLp5PmicRvp)) {
    DEBUG((DEBUG_ERROR, "AdlMBoardCscImonPmicConfgiure 3 "));
    AdlMBoardCscImonPmicConfgiure((VOID *)(UINTN)mCsc3ImonPmicTableRvp, (UINT16)sizeof(mCsc3ImonPmicTableRvp) / sizeof(PMIC_REG_INIT), (UINT8)0x3);
  }

  // Notify Ec To Pd firmware for enabling or disabling Pcie.
  AdlMNotifyEcToPdForPcieTunnel ();

  ASSERT(Status == EFI_SUCCESS);
  return EFI_SUCCESS;
}

/**
  A hook for board-specific initialization after memory initialization.

  @retval EFI_SUCCESS   The board initialization was successful.
**/
EFI_STATUS
EFIAPI
AdlMBoardInitAfterMemoryInit(
  VOID
  )
{
  DEBUG ((DEBUG_INFO, "AdlMBoardInitAfterMemoryInit\n"));

  return MmioInit ();
}

/**
  This board service initializes board-specific debug devices.

  @retval EFI_SUCCESS   Board-specific debug initialization was successful.
**/
EFI_STATUS
EFIAPI
AdlMBoardDebugInit (
  VOID
  )
{
  DEBUG ((DEBUG_INFO, "AdlMBoardDebugInit\n"));

  return EFI_SUCCESS;
}

/**
  This board service detects the boot mode.

  @retval EFI_BOOT_MODE The boot mode.
**/
EFI_BOOT_MODE
EFIAPI
AdlMBoardBootModeDetect (
  VOID
  )
{
  EFI_BOOT_MODE                             BootMode;

  DEBUG ((DEBUG_INFO, "AdlMBoardBootModeDetect\n"));
  BootMode = DetectBootMode ();
  DEBUG ((DEBUG_INFO, "BootMode: 0x%02X\n", BootMode));

  return BootMode;
}
