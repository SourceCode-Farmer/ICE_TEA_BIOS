/** @file

 @copyright
  INTEL CONFIDENTIAL
  Copyright 2020 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains a 'Sample Driver' and is licensed as such under the terms
  of your license agreement with Intel or your vendor. This file may be modified
  by the user, subject to the additional terms of the license agreement.

@par Specification Reference:
**/

#ifndef _ALDERLAKE_M_CONFIG_PATCH_TABLE_H_
#define _ALDERLAKE_M_CONFIG_PATCH_TABLE_H_

#include <Library/SetupInitLib.h>
#include <SetupVariable.h>
#include <Protocol/RetimerCapsuleUpdate.h>

CONFIG_PATCH_TABLE  mAlderLakeMLp4RvpSetupConfigPatchTable[] = {
//{-Type------------------Size-------------------------------------------Offset------------------------------------Data-------------},
  { SIZE_OF_FIELD (SETUP_DATA, DiscreteTbtPlatformConfigurationSupport), OFFSET_OF (SETUP_DATA, DiscreteTbtPlatformConfigurationSupport), 0 },
  { SIZE_OF_FIELD (SETUP_DATA, PchI2cSensorDevicePort[SERIAL_IO_I2C0]),  OFFSET_OF (SETUP_DATA, PchI2cSensorDevicePort[SERIAL_IO_I2C0]),  (SERIAL_IO_I2C_TOUCHPAD | SERIAL_IO_I2C_TOUCHPANEL) },
  { SIZE_OF_FIELD (SETUP_DATA, AuxOriOverrideSupport),                   OFFSET_OF (SETUP_DATA, AuxOriOverrideSupport),                   0 },
  { SIZE_OF_FIELD (SETUP_DATA, HebcValueSupport),                        OFFSET_OF (SETUP_DATA, HebcValueSupport),                        1 },
  { SIZE_OF_FIELD (SETUP_DATA, SensorHubType),                           OFFSET_OF (SETUP_DATA, SensorHubType),                           1 }
};

CONFIG_PATCH_TABLE  mAlderLakeMLp4RvpPchSetupConfigPatchTable[] = {
//{-Type------------------Size-------------------------------------------Offset------------------------------------Data-------------},
  { SIZE_OF_FIELD(PCH_SETUP, PchSerialIoSpi[SERIAL_IO_SPI1]),     OFFSET_OF(PCH_SETUP, PchSerialIoSpi[SERIAL_IO_SPI1]),     1 },
  { SIZE_OF_FIELD(PCH_SETUP, PchUartHwFlowCtrl[SERIAL_IO_UART0]), OFFSET_OF(PCH_SETUP, PchUartHwFlowCtrl[SERIAL_IO_UART0]), 0 },
  { SIZE_OF_FIELD(PCH_SETUP, PchSerialIoI2c[SERIAL_IO_I2C0]),     OFFSET_OF(PCH_SETUP, PchSerialIoI2c[SERIAL_IO_I2C0]),     1 },
  { SIZE_OF_FIELD(PCH_SETUP, PchSerialIoI2c[SERIAL_IO_I2C1]),     OFFSET_OF(PCH_SETUP, PchSerialIoI2c[SERIAL_IO_I2C1]),     1 },
  { SIZE_OF_FIELD(PCH_SETUP, PchSerialIoI2c[SERIAL_IO_I2C2]),     OFFSET_OF(PCH_SETUP, PchSerialIoI2c[SERIAL_IO_I2C2]),     0 },
  { SIZE_OF_FIELD(PCH_SETUP, PchSerialIoI2c[SERIAL_IO_I2C3]),     OFFSET_OF(PCH_SETUP, PchSerialIoI2c[SERIAL_IO_I2C3]),     0 },
  { SIZE_OF_FIELD(PCH_SETUP, PchSerialIoI2c[SERIAL_IO_I2C4]),     OFFSET_OF(PCH_SETUP, PchSerialIoI2c[SERIAL_IO_I2C4]),     0 },
  { SIZE_OF_FIELD(PCH_SETUP, PchSerialIoI2c[SERIAL_IO_I2C5]),     OFFSET_OF(PCH_SETUP, PchSerialIoI2c[SERIAL_IO_I2C5]),     1 },
  { SIZE_OF_FIELD(PCH_SETUP, PchIshGpEnable[7]),                  OFFSET_OF(PCH_SETUP, PchIshGpEnable[7]),                  1 },
  { SIZE_OF_FIELD(PCH_SETUP, PmcLpmS0i2p1En),                     OFFSET_OF(PCH_SETUP, PmcLpmS0i2p1En),                     0 },
  { SIZE_OF_FIELD(PCH_SETUP, CnviBtAudioOffload),                 OFFSET_OF(PCH_SETUP, CnviBtAudioOffload),                 1 },
};

CONFIG_PATCH_TABLE  mAlderLakeMPchSteppingPatchTable[] = {
  //{-Type------------------Size-------------------------------------------Offset------------------------------------Data-------------},
  { SIZE_OF_FIELD(PCH_SETUP, PchRetToLowCurModeVolTranTime),     OFFSET_OF(PCH_SETUP, PchRetToLowCurModeVolTranTime),  0x2B},
  { SIZE_OF_FIELD(PCH_SETUP, PchRetToHighCurModeVolTranTime),    OFFSET_OF(PCH_SETUP, PchRetToHighCurModeVolTranTime), 0x36 },
  { 0, 0, 0 }  // End of Table
};


CONFIG_PATCH_TABLE  mAlderLakeMLp4RvpSaSetupConfigPatchTable[] = {
//{-Type------------------Size-------------------------------------------Offset------------------------------------Data-------------},
  { 0, 0, 0 }  // End of Table
};

CONFIG_PATCH_TABLE  mAlderLakeMLp5PmicRvpSaSetupConfigPatchTable[] = {
//{-Type------------------Size-------------------------------------------Offset------------------------------------Data-------------},
  { SIZE_OF_FIELD(SA_SETUP, VbtSelect),   OFFSET_OF(SA_SETUP, VbtSelect),  VBT_SELECT_MIPI },  //Vbt Select set to MIPI Dispaly
};

///
///

CONFIG_PATCH_STRUCTURE mAlderLakeMLp4RvpConfigPatchStruct[] = {
  { L"Setup",    &gSetupVariableGuid,    sizeof(SETUP_DATA), mAlderLakeMLp4RvpSetupConfigPatchTable,     SIZE_OF_TABLE(mAlderLakeMLp4RvpSetupConfigPatchTable,     CONFIG_PATCH_TABLE) },
  { L"SaSetup",  &gSaSetupVariableGuid,  sizeof(SA_SETUP),   mAlderLakeMLp4RvpSaSetupConfigPatchTable,   SIZE_OF_TABLE(mAlderLakeMLp4RvpSaSetupConfigPatchTable,   CONFIG_PATCH_TABLE) },
  { L"PchSetup", &gPchSetupVariableGuid, sizeof(PCH_SETUP),  mAlderLakeMLp4RvpPchSetupConfigPatchTable,  SIZE_OF_TABLE(mAlderLakeMLp4RvpPchSetupConfigPatchTable,  CONFIG_PATCH_TABLE) },
  { L"MeSetup",  &gMeSetupVariableGuid,  sizeof(ME_SETUP),   NULL, 0 }
};

CONFIG_PATCH_STRUCTURE mAlderLakeMPchSteppingConfigPatchStruct[] = {
  { L"PchSetup", &gPchSetupVariableGuid, sizeof(PCH_SETUP),  mAlderLakeMPchSteppingPatchTable,  SIZE_OF_TABLE(mAlderLakeMPchSteppingPatchTable,  CONFIG_PATCH_TABLE) },
};

CONFIG_PATCH_STRUCTURE mAlderLakeMLp5PmicRvpConfigPatchStruct[] = {
  { L"Setup",    &gSetupVariableGuid,    sizeof(SETUP_DATA), mAlderLakeMLp4RvpSetupConfigPatchTable,     SIZE_OF_TABLE(mAlderLakeMLp4RvpSetupConfigPatchTable,     CONFIG_PATCH_TABLE) },
  { L"SaSetup",  &gSaSetupVariableGuid,  sizeof(SA_SETUP),   mAlderLakeMLp5PmicRvpSaSetupConfigPatchTable,   SIZE_OF_TABLE(mAlderLakeMLp4RvpSaSetupConfigPatchTable,   CONFIG_PATCH_TABLE) },
  { L"PchSetup", &gPchSetupVariableGuid, sizeof(PCH_SETUP),  mAlderLakeMLp4RvpPchSetupConfigPatchTable,  SIZE_OF_TABLE(mAlderLakeMLp4RvpPchSetupConfigPatchTable,  CONFIG_PATCH_TABLE) },
  { L"MeSetup",  &gMeSetupVariableGuid,  sizeof(ME_SETUP),   NULL, 0 }
};

CONFIG_PATCH_TABLE  mAlderlakeMSimicsSaSetupConfigPatchTable[] = {
//{---------------Type------Size-----------------------------------Offset----------------------------------------------------Data},
  { SIZE_OF_FIELD(SA_SETUP, VmdEnable),                            OFFSET_OF(SA_SETUP, VmdEnable),                           0x0 },
  { SIZE_OF_FIELD(SA_SETUP, VmdGlobalMapping),                     OFFSET_OF(SA_SETUP, VmdGlobalMapping),                    0x0 }
};

CONFIG_PATCH_STRUCTURE mAlderlakeMSimicsConfigPatchStruct[] = {
  { L"SaSetup",  &gSaSetupVariableGuid,  sizeof(SA_SETUP),   mAlderlakeMSimicsSaSetupConfigPatchTable,  SIZE_OF_TABLE(mAlderlakeMSimicsSaSetupConfigPatchTable,  CONFIG_PATCH_TABLE) }
};

CONFIG_PATCH_TABLE  mAlderLakeMAuxOverrideEnableSetupConfigPatchTable[] = {
//{-Type------------------Size-------------------------------------------Offset------------------------------------Data-------------},
  { SIZE_OF_FIELD (SETUP_DATA, AuxOriOverrideSupport),                  OFFSET_OF (SETUP_DATA, AuxOriOverrideSupport),       1 }
};

CONFIG_PATCH_STRUCTURE mAlderLakeMAuxOverrideEnableConfigPatchStruct[] = {
  { L"Setup", &gSetupVariableGuid, sizeof (SETUP_DATA), mAlderLakeMAuxOverrideEnableSetupConfigPatchTable, SIZE_OF_TABLE (mAlderLakeMAuxOverrideEnableSetupConfigPatchTable, CONFIG_PATCH_TABLE) },
};

CONFIG_PATCH_TABLE  mAlderLakeMWoVEnableSetupConfigPatchTable[] = {
//{-Type------------------Size-------------------------------------------Offset------------------------------------Data-------------},
  { SIZE_OF_FIELD (SETUP_DATA, ClosedLidWovLightingSupport),           OFFSET_OF (SETUP_DATA, ClosedLidWovLightingSupport),       1 }
};

CONFIG_PATCH_STRUCTURE mAlderLakeMWoVEnableConfigPatchStruct[] = {
  { L"Setup", &gSetupVariableGuid, sizeof (SETUP_DATA), mAlderLakeMWoVEnableSetupConfigPatchTable, SIZE_OF_TABLE (mAlderLakeMWoVEnableSetupConfigPatchTable, CONFIG_PATCH_TABLE) },
};

#endif // _ALDERLAKE_M_CONFIG_PATCH_TABLE_H_
