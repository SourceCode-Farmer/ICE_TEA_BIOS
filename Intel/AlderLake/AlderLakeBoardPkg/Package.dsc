## @file
# Component description file for the AlderLakeBoardPkg.
#
#******************************************************************************
#* Copyright (c) 2020 - 2021, Insyde Software Corp. All Rights Reserved.
#*
#* You may not reproduce, distribute, publish, display, perform, modify, adapt,
#* transmit, broadcast, present, recite, release, license or otherwise exploit
#* any part of this publication in any form, by any means, without the prior
#* written permission of Insyde Software Corporation.
#*
#******************************************************************************
#
# @copyright
#  INTEL CONFIDENTIAL
#  Copyright 2020 - 2021 Intel Corporation.
#
#  The source code contained or described herein and all documents related to the
#  source code ("Material") are owned by Intel Corporation or its suppliers or
#  licensors. Title to the Material remains with Intel Corporation or its suppliers
#  and licensors. The Material may contain trade secrets and proprietary and
#  confidential information of Intel Corporation and its suppliers and licensors,
#  and is protected by worldwide copyright and trade secret laws and treaty
#  provisions. No part of the Material may be used, copied, reproduced, modified,
#  published, uploaded, posted, transmitted, distributed, or disclosed in any way
#  without Intel's prior express written permission.
#
#  No license under any patent, copyright, trade secret or other intellectual
#  property right is granted to or conferred upon you by disclosure or delivery
#  of the Materials, either expressly, by implication, inducement, estoppel or
#  otherwise. Any license under such intellectual property rights must be
#  express and approved by Intel in writing.
#
#  Unless otherwise agreed by Intel in writing, you may not remove or alter
#  this notice or any other notice embedded in Materials by Intel or
#  Intel's suppliers or licensors in any way.
#
#  This file contains a 'Sample Driver' and is licensed as such under the terms
#  of your license agreement with Intel or your vendor. This file may be modified
#  by the user, subject to the additional terms of the license agreement.
#
# @par Specification
##


[Defines]
  PLATFORM_NAME = AlderLakeBoardPkg
  PLATFORM_GUID = EB89E595-7D9D-4422-A277-A50B5AFD3E16
  PLATFORM_VERSION = 0.4
  DSC_SPECIFICATION = 0x00010005
  OUTPUT_DIRECTORY = Build/AlderLakeBoardPkg
  SUPPORTED_ARCHITECTURES = IA32|X64
  BUILD_TARGETS = DEBUG|RELEASE
  SKUID_IDENTIFIER = DEFAULT

  DEFINE      TOP_MEMORY_ADDRESS         = 0x0
#[-start-200917-IB06462159-remove]#
  #
  # Remove definition for PLATFORM_FSP_BIN_PACKAGE, because that we need to define our owned path
  #
  # DEFINE      PLATFORM_FSP_BIN_PACKAGE   = AlderLakeFspBinPkg
#[-end-200917-IB06462159-remove]#

#
# Defines Boards paths
#
  DEFINE      BOARD_ADL_S_BOARDS              = AlderLakeSBoards
  DEFINE      PROJECT_ADL_S_BOARDS            = AlderLakeBoardPkg/$(BOARD_ADL_S_BOARDS)
  DEFINE      BOARD_ADL_U_SIMICS_BOARDS       = AlderLakeUSimicsBoards
  DEFINE      PROJECT_ADL_U_SIMICS_BOARDS     = AlderLakeBoardPkg/$(BOARD_ADL_U_SIMICS_BOARDS)
  DEFINE      BOARD_ADL_P_BOARDS              = AlderLakePBoards
  DEFINE      PROJECT_ADL_P_BOARDS            = AlderLakeBoardPkg/$(BOARD_ADL_P_BOARDS)
  DEFINE      BOARD_ADL_M_BOARDS              = AlderLakeMBoards
  DEFINE      PROJECT_ADL_M_BOARDS            = AlderLakeBoardPkg/$(BOARD_ADL_M_BOARDS)
#[-start-201223-IB19010009-add]#
  DEFINE      PLATFORM_FULL_PACKAGE           = AlderLakePlatSamplePkg
  DEFINE      PLATFORM_FEATURES_PATH          = $(PLATFORM_FULL_PACKAGE)/Features
#[-end-201223-IB19010009-add]#
#
# Platform On/Off features are defined here
#
#  !include AlderLakeBoardPkg/BoardPkgPcdInit.dsc
#  !include AlderLakeBoardPkg/BoardPkgPcdUpdate.dsc

#[-start-200420-17800058-2-modify]#
#ADL RC 1181.1 added , but temporary remove
################################################################################
#
# Feature includes
# Features need to be included before platform specify it's own library classes
# so that platform choices are prioritized over feature libraries
#
################################################################################

# XmlCli: Include Feature based on Pcd Value
#[-start-211108-IB19370023-modify]#
!if gBoardModuleTokenSpaceGuid.PcdIntelXmlCliFeatureEnable == TRUE
#[-end-211108-IB19370023-modify]#
  !include XmlCliFeaturePkg/Include/XmlCliFeature.dsc
!endif

!include NhltFeaturePkg/Include/NhltFeature.dsc
#[-start-201223-IB19010010-removed]#
# !include MebxFeaturePkg/Include/MebxFeature.dsc
#[-end-201223-IB19010010-removed]#
#[-end-200420-17800058-2-modify]#

###
### BoardPkgPcdInit.dsc Start
###

[Packages]
  Usb3DebugFeaturePkg/Usb3DebugFeaturePkg.dec
  PostCodeDebugFeaturePkg/PostCodeDebugFeaturePkg.dec
  BeepDebugFeaturePkg/BeepDebugFeaturePkg.dec
  MebxFeaturePkg/MebxFeaturePkg.dec

[PcdsFeatureFlag]
  ## This PCD specified whether ACPI SDT protocol is installed.
  # ROYAL_PARK_OVERRIDE: PcdInstallAcpiSdtProtocol
  gEfiMdeModulePkgTokenSpaceGuid.PcdInstallAcpiSdtProtocol|TRUE
  gEfiMdeModulePkgTokenSpaceGuid.PcdPeiCoreImageLoaderSearchTeSectionFirst|FALSE
  gEfiMdeModulePkgTokenSpaceGuid.PcdPciDegradeResourceForOptionRom|FALSE
  # Disabling PcdBrowserGrayOutTextStatement causes all empty/disabled rows active
  gEfiMdeModulePkgTokenSpaceGuid.PcdBrowserGrayOutTextStatement|TRUE

  gMinPlatformPkgTokenSpaceGuid.PcdStopAfterMemInit|FALSE
  gMinPlatformPkgTokenSpaceGuid.PcdBootToShellOnly|FALSE
  gMinPlatformPkgTokenSpaceGuid.PcdSmiHandlerProfileEnable|FALSE
  gMinPlatformPkgTokenSpaceGuid.PcdTpm2Enable|FALSE
  # Build scripts override the value of this PCD, update value in scripts for the change to take effect.
  gMinPlatformPkgTokenSpaceGuid.PcdUefiSecureBootEnable|FALSE
  gMinPlatformPkgTokenSpaceGuid.PcdPerformanceEnable|FALSE

  gBoardModuleTokenSpaceGuid.PcdSecurityEnable|FALSE
  gBoardModuleTokenSpaceGuid.PcdAdvancedFeatureEnable|FALSE
  gBoardModuleTokenSpaceGuid.PcdOptimizationEnable|FALSE
  gBoardModuleTokenSpaceGuid.PcdIntelGopEnable|FALSE                  #LegacyVideoRom.bin and IntelGopDriver.efi
  gBoardModuleTokenSpaceGuid.PcdUefiShellEnable|FALSE

  gNhltFeaturePkgTokenSpaceGuid.PcdNhltFeatureEnable|FALSE

  gSndwFeatureModuleTokenSpaceGuid.PcdSndwFeatureEnable|FALSE

  gMebxFeaturePkgTokenSpaceGuid.PcdMebxFeatureEnable|TRUE


  gUefiCpuPkgTokenSpaceGuid.PcdCpuSmmEnableBspElection|FALSE

  # XmlCli: Initialize Feature Pcd
#[-start-211108-IB19370023-remove]#
#  gXmlCliFeaturePkgTokenSpaceGuid.PcdXmlCliFeatureEnable|TRUE
#[-end-211108-IB19370023-remove]#

[PcdsFeatureFlag.X64]
  # Optimze Driver init time in FastBoot Mode
  # If set PcdPs2KbdExtendedVerification to False, we can save 380 ms for Ps2KeyboardDxe driver initialize time
  gEfiMdeModulePkgTokenSpaceGuid.PcdPs2KbdExtendedVerification|FALSE
  #
  # Enabling PcdHiiOsRuntimeSupport casuses S4 failure due to
  # insuficient Runtime memory allocation
  #
  gEfiMdeModulePkgTokenSpaceGuid.PcdHiiOsRuntimeSupport|FALSE

[PcdsFixedAtBuild]
  gSiPkgTokenSpaceGuid.PcdAcpiEnable|FALSE
  gSiPkgTokenSpaceGuid.PcdSmbiosEnable|FALSE
  gSiPkgTokenSpaceGuid.PcdS3Enable|FALSE
  gSiPkgTokenSpaceGuid.PcdITbtEnable|FALSE
  gSiPkgTokenSpaceGuid.PcdBiosGuardEnable|FALSE
#[-start-200420-IB17800056-modify]#
  # ADL PO Temporary remove
  gSiPkgTokenSpaceGuid.PcdHgEnable|TRUE
#[-end-200420-IB17800056-modify]#
  gSiPkgTokenSpaceGuid.PcdBootGuardEnable|FALSE
  gSiPkgTokenSpaceGuid.PcdAtaEnable|FALSE
  gSiPkgTokenSpaceGuid.PcdFspWrapperEnable|TRUE
  gSiPkgTokenSpaceGuid.PcdOcWdtEnable|FALSE
  gSiPkgTokenSpaceGuid.PcdSerialIoUartEnable|FALSE
  gSiPkgTokenSpaceGuid.PcdIpuEnable|FALSE
  gSiPkgTokenSpaceGuid.PcdPeiDisplayEnable|FALSE
  gSiPkgTokenSpaceGuid.PcdVtdEnable|FALSE
  gSiPkgTokenSpaceGuid.PcdGnaEnable|FALSE
  gSiPkgTokenSpaceGuid.PcdThcEnable|FALSE
  gSiPkgTokenSpaceGuid.PcdPpmEnable|FALSE
  gSiPkgTokenSpaceGuid.PcdPpamEnable|FALSE
  gSiPkgTokenSpaceGuid.PcdSpaEnable|FALSE
  gSiPkgTokenSpaceGuid.PcdPsmiEnable|FALSE
  gSiPkgTokenSpaceGuid.PcdVmdEnable|FALSE
  gSiPkgTokenSpaceGuid.PcdCpuPcieEnable|FALSE
  gSiPkgTokenSpaceGuid.PcdTraceHubDebugLibMaster|0x48
  gSiPkgTokenSpaceGuid.PcdTraceHubDebugLibChannel|0xE
  gSiPkgTokenSpaceGuid.PcdAcpiDebugEnableFlag|TRUE   # Acpi debug enable flag
  #
  # 16MB TSEG.
  #
  gSiPkgTokenSpaceGuid.PcdTsegSize|0x1000000

  #
  # Default BootStage is set here.
  # Stage 1 - enable debug (system deadloop after debug init)
  # Stage 2 - mem init (system deadloop after mem init)
  # Stage 3 - boot to shell only
  # Stage 4 - boot to OS
  # Stage 5 - boot to OS with security boot enabled
  # Stage 6 - boot with advanced features
  # stage 7 - tuning
  #
  gMinPlatformPkgTokenSpaceGuid.PcdBootStage|6
  gMinPlatformPkgTokenSpaceGuid.PcdFspWrapperBootMode|TRUE
  gMinPlatformPkgTokenSpaceGuid.PcdMaxCpuSocketCount|1
  gMinPlatformPkgTokenSpaceGuid.PcdPlatformEfiAcpiReclaimMemorySize|0xA2
  gMinPlatformPkgTokenSpaceGuid.PcdPlatformEfiAcpiNvsMemorySize|0xE8
  gMinPlatformPkgTokenSpaceGuid.PcdPlatformEfiReservedMemorySize|0x3100
  gMinPlatformPkgTokenSpaceGuid.PcdPlatformEfiRtDataMemorySize|0x6E
  gMinPlatformPkgTokenSpaceGuid.PcdPlatformEfiRtCodeMemorySize|0x99
  #
  # The PCDs are used to control the Windows SMM Security Mitigations Table - Protection Flags
  #
  # BIT0: If set, expresses that for all synchronous SMM entries,SMM will validate that input and output buffers lie entirely within the expected fixed memory regions.
  # BIT1: If set, expresses that for all synchronous SMM entries, SMM will validate that input and output pointers embedded within the fixed communication buffer only refer to address ranges \
  #       that lie entirely within the expected fixed memory regions.
  # BIT2: Firmware setting this bit is an indication that it will not allow reconfiguration of system resources via non-architectural mechanisms.
  # BIT3-31: Reserved
  #
  gMinPlatformPkgTokenSpaceGuid.PcdWsmtProtectionFlags|0x07

  gIntelFsp2WrapperTokenSpaceGuid.PcdFspModeSelection|0

  ## This PCD decides how FSP is measured
  # 1) The BootGuard ACM may already measured the FSP component, such as FSPT/FSPM.
  # We need a flag (PCD) to indicate if there is need to do such FSP measurement or NOT.
  # 2) The FSP binary includes FSP code and FSP UPD region. The UPD region is considered
  # as configuration block, and it may be updated by OEM by design.
  # This flag (PCD) is to indicate if we need isolate the the UPD region from the FSP code region.
  # BIT0: Need measure FSP.  (for FSP 1.x) - reserved in FSP2.
  # BIT1: Need measure FSPT. (for FSP 2.x)
  # BIT2: Need measure FSPM. (for FSP 2.x)
  # BIT3: Need measure FSPS. (for FSP 2.x)
  # BIT4~30: reserved.
  # BIT31: Need isolate UPD region measurement.
  #   0: measure FSP[T|M|S] as one binary in one record (PCR0).
  #   1: measure FSP UPD region in one record (PCR1),
  #      measure the FSP code without UPD in another record (PCR0).
  #
  gIntelFsp2WrapperTokenSpaceGuid.PcdFspMeasurementConfig|0x80000008

  gBoardModuleTokenSpaceGuid.PcdAcpiDebugFeatureEnable|FALSE
  gBoardModuleTokenSpaceGuid.PcdS4Enable|FALSE
  gBoardModuleTokenSpaceGuid.PcdBiosGuardBinEnable|FALSE              #BiosGuardModule.bin
  gBoardModuleTokenSpaceGuid.PcdGopConfigBin|FALSE
  gBoardModuleTokenSpaceGuid.PcdNhltBinEnable|FALSE                   #NhltIcl.bin
  gBoardModuleTokenSpaceGuid.PcdRaidDriverEfiEnable|FALSE             #RaidDriver.efi
  gBoardModuleTokenSpaceGuid.PcdRsteDriverEfiEnable|FALSE             #SataDriverRste.efi
  gBoardModuleTokenSpaceGuid.PcdNvmeEnable|FALSE
  gBoardModuleTokenSpaceGuid.PcdIntelRaidEnable|FALSE
  gBoardModuleTokenSpaceGuid.PcdTerminalEnable|FALSE
  gBoardModuleTokenSpaceGuid.PcdEcEnable|FALSE
  !if gSiPkgTokenSpaceGuid.PcdAdlLpSupport == TRUE
    gBoardModuleTokenSpaceGuid.PcdDefaultBoardId|0x13
  !else
    gBoardModuleTokenSpaceGuid.PcdDefaultBoardId|0x27
  !endif

  gBoardModuleTokenSpaceGuid.PcdFFUEnable|FALSE
  gBoardModuleTokenSpaceGuid.PcdFspBinGccBuildEnable|FALSE
  # Build scripts override the value of this PCD, update value in scripts for the change to take effect.
  gBoardModuleTokenSpaceGuid.PcdSetupEnable|TRUE
  gBoardModuleTokenSpaceGuid.PcdStartupAcmBinEnable|FALSE           #StartupAcm.bin
  #
  # LiteBios related PCDs must be moved to LiteBios board package when it is  created
  #
  gBoardModuleTokenSpaceGuid.PcdMicrocodeBinEnable|TRUE       #Microcode
  gBoardModuleTokenSpaceGuid.PcdEcEnable|TRUE
  gBoardModuleTokenSpaceGuid.PcdSipkgBinaryEnable|FALSE

#[-start-201118-IB14630443-modify]#
  gBoardModulePkgTokenSpaceGuid.PcdPs2KbMsEnable|0x00
#[-end-201118-IB14630443-modify]#
  gBoardModulePkgTokenSpaceGuid.PcdSuperIoPciIsaBridgeDevice|{0x00, 0x00, 0x1F, 0x00}
  gBoardModulePkgTokenSpaceGuid.PcdUart1Enable|0x01
  gBoardModulePkgTokenSpaceGuid.PcdUart1IrqMask|0x0010
  gBoardModulePkgTokenSpaceGuid.PcdUart1IoPort|0x03F8
  gBoardModulePkgTokenSpaceGuid.PcdUart1Length|0x08

  gPlatformModuleTokenSpaceGuid.PcdPlatformCmosAccessSupport|FALSE
  gPlatformModuleTokenSpaceGuid.PcdEnableSecureErase|FALSE
  gPlatformModuleTokenSpaceGuid.PcdDTbtEnable|FALSE
  gPlatformModuleTokenSpaceGuid.PcdCapsuleEnable|FALSE
  gPlatformModuleTokenSpaceGuid.PcdI2cTouchDriverEnable|FALSE
  gPlatformModuleTokenSpaceGuid.PcdPiI2cStackEnable|FALSE
  gPlatformModuleTokenSpaceGuid.PcdUsb3SerialStatusCodeEnable|FALSE
  gPlatformModuleTokenSpaceGuid.PcdUserAuthenticationEnable|FALSE
  gPlatformModuleTokenSpaceGuid.PcdPciHotplugEnable|FALSE
  gPlatformModuleTokenSpaceGuid.PcdUsbTypeCEnable|FALSE
  gPlatformModuleTokenSpaceGuid.PcdVirtualKeyboardEnable|FALSE
  gPlatformModuleTokenSpaceGuid.PcdEbcEnable|FALSE
  gPlatformModuleTokenSpaceGuid.PcdHddPasswordEnable|FALSE
  gPlatformModuleTokenSpaceGuid.PcdNetworkEnable|FALSE
  gPlatformModuleTokenSpaceGuid.PcdGigUndiEnable|FALSE
  gPlatformModuleTokenSpaceGuid.PcdMouseEnable|FALSE
  gPlatformModuleTokenSpaceGuid.PcdSinitAcmBinEnable|FALSE
  gPlatformModuleTokenSpaceGuid.PcdAcmProdBinEnable|FALSE
  gPlatformModuleTokenSpaceGuid.PcdScsiEnable|FALSE
  gPlatformModuleTokenSpaceGuid.PcdJpgEnable|FALSE
  gPlatformModuleTokenSpaceGuid.PcdUsbEnable|FALSE
  gPlatformModuleTokenSpaceGuid.PcdNetworkIp6Enable|FALSE
  gPlatformModuleTokenSpaceGuid.PcdNetworkIscsiEnable|FALSE
  gPlatformModuleTokenSpaceGuid.PcdNetworkVlanEnable|FALSE
  gPlatformModuleTokenSpaceGuid.PcdFatEnable|FALSE
  gPlatformModuleTokenSpaceGuid.PcdCryptoEnable|FALSE    # Current Smbios implementation needs this
  gPlatformModuleTokenSpaceGuid.PcdLzmaEnable|TRUE
  gPlatformModuleTokenSpaceGuid.PcdDxeCompressEnable|TRUE
  gPlatformModuleTokenSpaceGuid.PcdVtioEnable|FALSE
  gPlatformModuleTokenSpaceGuid.PcdUserIdentificationEnable|FALSE
  gPlatformModuleTokenSpaceGuid.PcdDnxSupportEnable|FALSE
  gPlatformModuleTokenSpaceGuid.PcdUsbFnEnable|FALSE
  gPlatformModuleTokenSpaceGuid.PcdH8S2113Present|FALSE
  gPlatformModuleTokenSpaceGuid.PcdNat87393Present|FALSE
  gPlatformModuleTokenSpaceGuid.PcdNct677FPresent|FALSE
  gPlatformModuleTokenSpaceGuid.PcdSkipFspTempRamInitAndExit|FALSE
  gPlatformModuleTokenSpaceGuid.PcdTdsEnable|FALSE
  gPlatformModuleTokenSpaceGuid.PcdOpalPasswordEnable|FALSE
  gPlatformModuleTokenSpaceGuid.PcdMemoryTestEnable|FALSE
  gPlatformModuleTokenSpaceGuid.PcdTpmEnable|FALSE
  gPlatformModuleTokenSpaceGuid.PcdBeepStatusCodeEnable|FALSE
  gPlatformModuleTokenSpaceGuid.PcdPostCodeStatusCodeEnable|FALSE
  gPlatformModuleTokenSpaceGuid.PcdDgrPolicyOverride|FALSE
  gPlatformModuleTokenSpaceGuid.PcdSymbolInReleaseEnable|FALSE
  gPlatformModuleTokenSpaceGuid.PcdResiliencyEnable|FALSE
  gPlatformModuleTokenSpaceGuid.PcdMeResiliencyEnable|FALSE
  gPlatformModuleTokenSpaceGuid.PcdRemotePlatformEraseSupport|FALSE
  gPlatformModuleTokenSpaceGuid.PcdExtendedBiosRegionSupport|FALSE

  ## This flag is used to initialize debug output interface.
  #  BIT0 - RAM debug interface.
  #  BIT1 - UART debug interface.
  #  BIT2 - USB debug interface.
  #  BIT3 - USB3 debug interface.
  #  BIT4 - Serial IO debug interface.
  #  BIT5 - TraceHub debug interface.
  #  BIT6 - Reserved.
  #  BIT7 - CMOS control.
  gPlatformModuleTokenSpaceGuid.PcdStatusCodeFlags|0x32
  gPlatformModuleTokenSpaceGuid.PcdRamDebugEnable|TRUE
  #
  # BIOS build switches configuration
  #
  gPlatformModuleTokenSpaceGuid.PcdDeprecatedFunctionRemove|TRUE
  gPlatformModuleTokenSpaceGuid.PcdSerialPortEnable|TRUE
  gPlatformModuleTokenSpaceGuid.PcdGmAdrAddress|(gEfiMdePkgTokenSpaceGuid.PcdPciExpressBaseAddress - 0x10000000) # 0xB0000000
  gPlatformModuleTokenSpaceGuid.PcdGttMmAddress|(gEfiMdePkgTokenSpaceGuid.PcdPciExpressBaseAddress - 0x11000000) # 0xAF000000
  #
  # PlatformTemporaryMmioAssignmentBegin
  #
  # When gEfiPeiBootInRecoveryModePpiGuid is installed, below MMIO resource would be
  # temporarily assigned to NVME/AHCI host controller after FspSiInitDone and be released at EndOfPei.
  # Please take care of platform resource assignment to avoid conflicts.
  #
  gPlatformModuleTokenSpaceGuid.PcdAhciPeiMmioBase|(gEfiMdePkgTokenSpaceGuid.PcdPciExpressBaseAddress - 0x30000000)    # 0x90000000
  gPlatformModuleTokenSpaceGuid.PcdAhciPeiMmioLimit|(gPlatformModuleTokenSpaceGuid.PcdAhciPeiMmioBase + 0x1000000)     # 0x91000000
  gPlatformModuleTokenSpaceGuid.PcdNvmeHcPeiMmioBase|(gPlatformModuleTokenSpaceGuid.PcdAhciPeiMmioLimit)               # 0x91000000
  gPlatformModuleTokenSpaceGuid.PcdNvmeHcPeiMmioLimit|(gPlatformModuleTokenSpaceGuid.PcdNvmeHcPeiMmioBase + 0x1000000) # 0x92000000

  # The bit width of data to be written to Port80 set to 16
  gEfiMdePkgTokenSpaceGuid.PcdPort80DataWidth|16

  gEfiMdeModulePkgTokenSpaceGuid.PcdFirmwareVendor|L"Intel"
  # Change PcdBootManagerMenuFile to point to UiApp
  gEfiMdeModulePkgTokenSpaceGuid.PcdBootManagerMenuFile|{ 0x8b, 0x7d, 0x9a, 0xd8, 0x16, 0xd0, 0x26, 0x4d, 0x93, 0xe3, 0xea, 0xb6, 0xb4, 0xd3, 0xb0, 0xa2 }
  # Do not support output status code to memory
  gEfiMdeModulePkgTokenSpaceGuid.PcdPeiCoreMaxPeiStackSize|0x80000
  gEfiMdeModulePkgTokenSpaceGuid.PcdMaxVariableSize|0x10000
  gEfiMdeModulePkgTokenSpaceGuid.PcdHwErrStorageSize|0x00000800
  gEfiMdeModulePkgTokenSpaceGuid.PcdMaxHardwareErrorVariableSize|0x400
  gEfiMdeModulePkgTokenSpaceGuid.PcdAriSupport|FALSE
  gEfiMdeModulePkgTokenSpaceGuid.PcdLoadModuleAtFixAddressEnable|$(TOP_MEMORY_ADDRESS)
  gEfiMdeModulePkgTokenSpaceGuid.PcdBrowserSubtitleTextColor|0x0
  gEfiMdeModulePkgTokenSpaceGuid.PcdBrowserFieldTextColor|0x01
  gEfiMdeModulePkgTokenSpaceGuid.PcdReclaimVariableSpaceAtEndOfDxe|TRUE

  #
  # Telemetry Feature PCDs
  #
  ## Major Version from EDKII.
  gTelemetryTokenSpaceGuid.PcdEDKIIVersionMajor|0xFF
  ## Minor Version from EDKII.
  gTelemetryTokenSpaceGuid.PcdEDKIIVersionMinor|0xFF
  ## Revision from EDKII.
  gTelemetryTokenSpaceGuid.PcdEDKIIVersionRevision|0xFF
  ## Build Number from EDKII.
  gTelemetryTokenSpaceGuid.PcdEDKIIVersionBuild|0xFF

# gino: need to update EDK2 >>
  #
  # Crypto Modulization
  #
  ## Crypto Modulization Feature Enabling
  gBoardModuleTokenSpaceGuid.PcdModularCryptoEnable|FALSE
  ## Crypto Modulization Scope
  #gEfiCryptoPkgTokenSpaceGuid.PcdCryptoServiceFamilyEnable.HmacMd5.Family    | FALSE
  #gEfiCryptoPkgTokenSpaceGuid.PcdCryptoServiceFamilyEnable.HmacSha1.Family   | FALSE
  #gEfiCryptoPkgTokenSpaceGuid.PcdCryptoServiceFamilyEnable.HmacSha256.Family | FALSE
  #gEfiCryptoPkgTokenSpaceGuid.PcdCryptoServiceFamilyEnable.Md4.Family        | FALSE
  #gEfiCryptoPkgTokenSpaceGuid.PcdCryptoServiceFamilyEnable.Md5.Family        | FALSE
  #gEfiCryptoPkgTokenSpaceGuid.PcdCryptoServiceFamilyEnable.Pkcs.Family       | FALSE
  #gEfiCryptoPkgTokenSpaceGuid.PcdCryptoServiceFamilyEnable.Dh.Family         | FALSE
  #gEfiCryptoPkgTokenSpaceGuid.PcdCryptoServiceFamilyEnable.Random.Family     | FALSE
  #gEfiCryptoPkgTokenSpaceGuid.PcdCryptoServiceFamilyEnable.Rsa.Family        | FALSE
  #gEfiCryptoPkgTokenSpaceGuid.PcdCryptoServiceFamilyEnable.Sha1.Family       | FALSE
  #gEfiCryptoPkgTokenSpaceGuid.PcdCryptoServiceFamilyEnable.Sha256.Family     | FALSE
  #gEfiCryptoPkgTokenSpaceGuid.PcdCryptoServiceFamilyEnable.Sha384.Family     | FALSE
  #gEfiCryptoPkgTokenSpaceGuid.PcdCryptoServiceFamilyEnable.Sha512.Family     | FALSE
  #gEfiCryptoPkgTokenSpaceGuid.PcdCryptoServiceFamilyEnable.X509.Family       | FALSE
  #gEfiCryptoPkgTokenSpaceGuid.PcdCryptoServiceFamilyEnable.Tdes.Family       | FALSE
  #gEfiCryptoPkgTokenSpaceGuid.PcdCryptoServiceFamilyEnable.Aes.Family        | FALSE
  #gEfiCryptoPkgTokenSpaceGuid.PcdCryptoServiceFamilyEnable.Arc4.Family       | FALSE
  #gEfiCryptoPkgTokenSpaceGuid.PcdCryptoServiceFamilyEnable.Sm3.Family        | FALSE
  #gEfiCryptoPkgTokenSpaceGuid.PcdCryptoServiceFamilyEnable.Hkdf.Family       | FALSE
  #gEfiCryptoPkgTokenSpaceGuid.PcdCryptoServiceFamilyEnable.Tls.Family        | FALSE
  #gEfiCryptoPkgTokenSpaceGuid.PcdCryptoServiceFamilyEnable.TlsSet.Family     | FALSE
  #gEfiCryptoPkgTokenSpaceGuid.PcdCryptoServiceFamilyEnable.TlsGet.Family     | FALSE
# gino: need to update EDK2 <<

  gNhltFeaturePkgTokenSpaceGuid.NhltConfigurationByPcdEnabled|FALSE

#[-start-210525-IB18770029-modify]#
#[-start-211108-IB19370023-modify]#
!if gBoardModuleTokenSpaceGuid.PcdIntelXmlCliFeatureEnable == TRUE
#[-end-211108-IB19370023-modify]#
  # Set Pcd Value to Client Platform, Refer README for more valid options
  gXmlCliFeaturePkgTokenSpaceGuid.PcdPlatformXmlCli|0x1
  # XmlCli Feature Package Token Space defining Setup Values
  gXmlCliFeaturePkgTokenSpaceGuid.PcdXmlCliSupport|0x0
  gXmlCliFeaturePkgTokenSpaceGuid.PcdPublishSetupPgPtr|0x1
  gXmlCliFeaturePkgTokenSpaceGuid.PcdEnableXmlCliLite|0x0
!endif
#[-end-210525-IB18770029-modify]#

  ## Specifies the number of variable MTRRs reserved for OS use. The default number of
  #  MTRRs reserved for OS use is 0.
  # @Prompt Number of reserved variable MTRRs.
  gUefiCpuPkgTokenSpaceGuid.PcdCpuNumberOfReservedVariableMtrrs|0x0
  gUefiCpuPkgTokenSpaceGuid.PcdCpuSmmApSyncTimeout|10000
  gUefiCpuPkgTokenSpaceGuid.PcdCpuSmmStackSize|0x20000

  gPcAtChipsetPkgTokenSpaceGuid.PcdAcpiIoPciBusNumber|0x0
  gPcAtChipsetPkgTokenSpaceGuid.PcdAcpiIoPciDeviceNumber|0x1F
  gPcAtChipsetPkgTokenSpaceGuid.PcdAcpiIoPciFunctionNumber|0x2
  gPcAtChipsetPkgTokenSpaceGuid.PcdAcpiIoPciEnableRegisterOffset|0x44
  gPcAtChipsetPkgTokenSpaceGuid.PcdAcpiIoBarEnableMask|0x80
  gPcAtChipsetPkgTokenSpaceGuid.PcdAcpiIoPciBarRegisterOffset|0x00
  gPcAtChipsetPkgTokenSpaceGuid.PcdAcpiIoPortBaseAddress|0x1800
  gPcAtChipsetPkgTokenSpaceGuid.PcdAcpiPm1TmrOffset|0x08
  gPcAtChipsetPkgTokenSpaceGuid.PcdAcpiIoPortBaseAddressMask|0xFFFC

  gMinPlatformPkgTokenSpaceGuid.PcdFspDispatchModeUseFspPeiMain|TRUE
#[-start-210723-IB09480146-add]#
  gEfiMdePkgTokenSpaceGuid.PcdDebugPrintErrorLevel|0x80000046
#[-end-210723-IB09480146-add]#

#[-start-191111-IB10189001-modify]#
# gino: kernel issue, this is W/A
[PcdsFixedAtBuild]
  gMinPlatformPkgTokenSpaceGuid.PcdPeiPhaseStackTop|0xA0000

[PcdsFixedAtBuild.IA32]
  gEfiMdeModulePkgTokenSpaceGuid.PcdVpdBaseAddress|0x0
#  gMinPlatformPkgTokenSpaceGuid.PcdPeiPhaseStackTop|0xA0000
#[-end-191111-IB10189001-modify]#

[PcdsFixedAtBuild.X64]

#[-start-201123-IB18410024-remove]#
#  # Default platform supported RFC 4646 languages: (American) English
#  gEfiMdePkgTokenSpaceGuid.PcdUefiVariableDefaultPlatformLangCodes|"en-US"
#[-end-201123-IB18410024-remove]#

[PcdsPatchableInModule]
#[-start-191111-IB10189001-remove]#
  # Insyde had defined this pcd
#  gEfiMdeModulePkgTokenSpaceGuid.PcdSmbiosVersion|0x0304
#[-end-191111-IB10189001-remove]#
#[-start-210723-IB09480146-remove]#
#  gEfiMdePkgTokenSpaceGuid.PcdDebugPrintErrorLevel|0x80000046
#[-end-210723-IB09480146-remove]#

[PcdsDynamicHii.common.DEFAULT]
  gPlatformModuleTokenSpaceGuid.PcdPlatformMemoryCheck|L"MemoryCheck"|gPlatformModuleTokenSpaceGuid|0x0|0
  gPlatformModuleTokenSpaceGuid.PcdComPortAttributes0IsEnabled|L"ComAttributes"|gSetupVariableGuid|0x16|0
  gEfiMdePkgTokenSpaceGuid.PcdDefaultTerminalType|L"ComAttributes"|gSetupVariableGuid|0x13|0
  gEfiSecurityPkgTokenSpaceGuid.PcdTpm2AcpiTableRev|L"TCG2_VERSION"|gTcg2ConfigFormSetGuid|0x8|4|NV,BS
  gPlatformModuleTokenSpaceGuid.PcdFastBootEnable|L"Setup"|gSetupVariableGuid|0x0|0
  gPlatformModuleTokenSpaceGuid.PcdSystemFirmwareFmpSupportedMode|L"SysFmpMode"|gSysFwUpdateProgressGuid|0x0|0xFF|NV,BS

[PcdsDynamicHii.X64.DEFAULT]
  gEfiMdePkgTokenSpaceGuid.PcdPlatformBootTimeOut|L"Timeout"|gEfiGlobalVariableGuid|0x0|5 # Variable: L"Timeout"
  gEfiMdeModulePkgTokenSpaceGuid.PcdSetupConOutColumn|L"ConOutConfig"|gPlatformModuleTokenSpaceGuid|0x0|100
  gEfiMdeModulePkgTokenSpaceGuid.PcdSetupConOutRow|L"ConOutConfig"|gPlatformModuleTokenSpaceGuid|0x4|31
  gEfiMdePkgTokenSpaceGuid.PcdHardwareErrorRecordLevel|L"HwErrRecSupport"|gEfiGlobalVariableGuid|0x0|1 # Variable: L"HwErrRecSupport"

[PcdsDynamicDefault]
  gEfiSecurityPkgTokenSpaceGuid.PcdCRBIdleByPass|0xFF
  ## This PCD defines initial setting of TCG2 Persistent Firmware Management Flags
  # Currently enabled flags:
  # TCG2_BIOS_TPM_MANAGEMENT_FLAG_PP_REQUIRED_FOR_TURN_OFF                (BIT5)
  # TCG2_BIOS_TPM_MANAGEMENT_FLAG_PP_REQUIRED_FOR_CHANGE_EPS              (BIT6)
  # TCG2_BIOS_TPM_MANAGEMENT_FLAG_PP_REQUIRED_FOR_CHANGE_PCRS             (BIT7)
  # TCG2_BIOS_STORAGE_MANAGEMENT_FLAG_PP_REQUIRED_FOR_DISABLE_BLOCK_SID   (BIT17)
  # @Prompt Initial setting of TCG2 Persistent Firmware Management Flags
  gEfiSecurityPkgTokenSpaceGuid.PcdTcg2PhysicalPresenceFlags|0x200E0
  gEfiSecurityPkgTokenSpaceGuid.PcdTpm2CurrentIrqNum|0x00

  gEfiSecurityPkgTokenSpaceGuid.PcdSkipOpalPasswordPrompt|FALSE

  gEfiMdeModulePkgTokenSpaceGuid.PcdSrIovSupport|FALSE
  gEfiMdeModulePkgTokenSpaceGuid.PcdConOutColumn|0x0
  gEfiMdeModulePkgTokenSpaceGuid.PcdConOutRow|0x0
  gEfiMdeModulePkgTokenSpaceGuid.PcdAtaSmartEnable|TRUE
  gEfiMdeModulePkgTokenSpaceGuid.PcdConInConnectOnDemand|FALSE
  #
  #  Set video to native resolution as Windows 8 WHCK requirement.
  #
  gEfiMdeModulePkgTokenSpaceGuid.PcdVideoHorizontalResolution|0x0
  gEfiMdeModulePkgTokenSpaceGuid.PcdVideoVerticalResolution|0x0
  ## This is recover file name in PEI phase.
  #  The file must be in the root directory.
  #  The file name must be the 8.3 format.
  #  The PCD data must be in UNICODE format.
  # @Prompt Recover file name in PEI phase
  gEfiMdeModulePkgTokenSpaceGuid.PcdRecoveryFileName|L"Capsule.cap"|VOID*|0x20
  ## Default OEM Table ID for ACPI table creation, it is "EDK2    ".
  #  According to ACPI specification, this field is particularly useful when
  #  defining a definition block to distinguish definition block functions.
  #  The OEM assigns each dissimilar table a new OEM Table ID.
  #  This PCD is ignored for definition block.
  # @Prompt Default OEM Table ID for ACPI table creation.
  # default set to "ICL     ", will be patched by AcpiPlatform per CPU family
  gEfiMdeModulePkgTokenSpaceGuid.PcdAcpiDefaultOemTableId|0x20202020204C4349

  gMinPlatformPkgTokenSpaceGuid.PcdPciReservedMemAbove4GBBase|0xFFFFFFFFFFFFFFFF
  gMinPlatformPkgTokenSpaceGuid.PcdPciReservedMemAbove4GBLimit|0x0000000000000000
  gMinPlatformPkgTokenSpaceGuid.PcdPciSegmentCount|0x1

  gPlatformModuleTokenSpaceGuid.PcdTopSwapEnableSwSmi|0xFF
  gPlatformModuleTokenSpaceGuid.PcdTopSwapDisableSwSmi|0xFF
  gPlatformModuleTokenSpaceGuid.PcdPostIbbVerificationEnable|TRUE

  gSiPkgTokenSpaceGuid.PcdAcpiDefaultOemTableId|0x20202020204C4349

  gEfiSecurityPkgTokenSpaceGuid.PcdFirmwareDebuggerInitialized|FALSE
  gEfiSecurityPkgTokenSpaceGuid.PcdSkipHddPasswordPrompt|FALSE
  gEfiSecurityPkgTokenSpaceGuid.PcdSkipOpalPasswordPrompt|FALSE

  gBoardModuleTokenSpaceGuid.PcdUsbcEcPdNegotiation|TRUE


  ## The mask is used to control VTd behavior.<BR><BR>
  #  BIT0: Enable IOMMU during boot (If DMAR table is installed in DXE. If VTD_INFO_PPI is installed in PEI.)
  #  BIT1: Enable IOMMU when transfer control to OS (ExitBootService in normal boot. EndOfPEI in S3)
  # @Prompt The policy for VTd driver behavior.
  gIntelSiliconPkgTokenSpaceGuid.PcdVTdPolicyPropertyMask|0x00000000

  gPlatformModuleTokenSpaceGuid.PcdVTdDisablePeiPmrProtection|FALSE

  ## Enable PCIe Resizable BAR Capability Support.
  gEfiMdeModulePkgTokenSpaceGuid.PcdPcieResizableBarSupport|FALSE

[PcdsDynamicExDefault]
  gEfiMdeModulePkgTokenSpaceGuid.PcdS3BootScriptTablePrivateDataPtr|0x0
  gEfiMdeModulePkgTokenSpaceGuid.PcdFastPS2Detection|FALSE

  gMinPlatformPkgTokenSpaceGuid.PcdPciExpressRegionLength|0x10000000
  #
  # Some of the PCD consumed by both FSP and bootloader should be defined
  # here for bootloader to consume.
  #

  gBoardModuleTokenSpaceGuid.PcdBoardGpioTablePreMem|{0x0}
  gBoardModuleTokenSpaceGuid.PcdBoardGpioTablePreMem.GpioConfig[60].GpioPad|0x0
  gBoardModuleTokenSpaceGuid.PcdBoardGpioTable|{0x0}
  gBoardModuleTokenSpaceGuid.PcdBoardGpioTable.GpioConfig[130].GpioPad|0x0
  gBoardModuleTokenSpaceGuid.PcdBoardGpioTableEarlyPreMem|{0x0}
  gBoardModuleTokenSpaceGuid.PcdBoardGpioTableEarlyPreMem.GpioConfig[40].GpioPad|0x0
  gBoardModuleTokenSpaceGuid.PcdBoardAcpiData|{0x0}


[PcdsDynamicExVpd.common.DEFAULT]
  gBoardModuleTokenSpaceGuid.VpdPcdBoardGpioTablePreMem| * |{CODE({
    {0x0}  // terminator
  })}

  gBoardModuleTokenSpaceGuid.VpdPcdBoardGpioTable| * |{CODE({
    {0x0}  // terminator
  })}

  gBoardModuleTokenSpaceGuid.VpdPcdPcieClkUsageMap| * |{CODE(
    {0x0}
  )}

  gBoardModuleTokenSpaceGuid.VpdPcdUSB2OCMap| * |{CODE(
  { 16,
    {  USB_OC_SKIP,    // Port 1
       USB_OC_SKIP,    // Port 2
       USB_OC_SKIP,    // Port 3
       USB_OC_SKIP,    // Port 4
       USB_OC_SKIP,    // Port 5
       USB_OC_SKIP,    // Port 6
       USB_OC_SKIP,    // Port 7
       USB_OC_SKIP,    // Port 8
       USB_OC_SKIP,    // Port 9
       USB_OC_SKIP,    // Port 10
       USB_OC_SKIP,    // Port 11
       USB_OC_SKIP,    // Port 12
       USB_OC_SKIP,    // Port 13
       USB_OC_SKIP,    // Port 14
       USB_OC_SKIP,    // Port 15
       USB_OC_SKIP  }  // Port 16
  })}
#[-start-210728-YUNLEI0116-modify]
!if $(C770_SUPPORT_ENABLE) == YES
  gBoardModuleTokenSpaceGuid.VpdPcd16USB2OCMap| * |{CODE(
  { 16,
    {  USB_OC_SKIP,    // Port 1
       USB_OC_SKIP,    // Port 2
       USB_OC_SKIP,    // Port 3
       USB_OC_SKIP,    // Port 4
       USB_OC_SKIP,    // Port 5
       USB_OC_SKIP,    // Port 6
       USB_OC_SKIP,    // Port 7
       USB_OC_SKIP,    // Port 8
       USB_OC_SKIP,    // Port 9
       USB_OC_SKIP,    // Port 10
       USB_OC_SKIP,    // Port 11
       USB_OC_SKIP,    // Port 12
       USB_OC_SKIP,    // Port 13
       USB_OC_SKIP,    // Port 14
       USB_OC_SKIP,    // Port 15
       USB_OC_SKIP  }  // Port 16
  })}

    gBoardModuleTokenSpaceGuid.VpdPcd16USB3OCMap| * |{CODE(
  { 10,
    {  USB_OC_SKIP,    // Port 1
       USB_OC_SKIP,    // Port 2
       USB_OC_SKIP,    // Port 3
       USB_OC_SKIP,    // Port 4
       USB_OC_SKIP,    // Port 5
       USB_OC_SKIP,    // Port 6
       USB_OC_SKIP,    // Port 7
       USB_OC_SKIP,    // Port 8
       USB_OC_SKIP,    // Port 9
       USB_OC_SKIP  }  // Port 10
  })}
!endif
#[-end-210728-YUNLEI0116-modify]
  gBoardModuleTokenSpaceGuid.VpdPcdUSB3OCMap| * |{CODE(
  { 10,
    {  USB_OC_SKIP,    // Port 1
       USB_OC_SKIP,    // Port 2
       USB_OC_SKIP,    // Port 3
       USB_OC_SKIP,    // Port 4
       USB_OC_SKIP,    // Port 5
       USB_OC_SKIP,    // Port 6
       USB_OC_SKIP,    // Port 7
       USB_OC_SKIP,    // Port 8
       USB_OC_SKIP,    // Port 9
       USB_OC_SKIP  }  // Port 10
  })}

!if gSiPkgTokenSpaceGuid.PcdAdlLpSupport == TRUE
  gBoardModuleTokenSpaceGuid.VpdPcdMrcSpdData| * |{CODE(
    {0x0}
  )}

  gBoardModuleTokenSpaceGuid.VpdPcdMrcDqsMapCpu2Dram| * |{CODE(
    {0x0}
  )}

  gBoardModuleTokenSpaceGuid.VpdPcdMrcDqMapCpu2Dram| * |{CODE(
    {0x0}
  )}

  gBoardModuleTokenSpaceGuid.VpdPcdCpuUsb3OcMap| * |{CODE(
  { 6,
    {  USB_OC_SKIP,    // Port 1
       USB_OC_SKIP,    // Port 2
       USB_OC_SKIP,    // Port 3
       USB_OC_SKIP,    // Port 4
       USB_OC_SKIP,    // Port 5
       USB_OC_SKIP  }  // Port 6
  })}
!endif

  gBoardModuleTokenSpaceGuid.VpdPcdPmaxDevices| * |{CODE(
  {{
      {
         "\\_SB.PC00.HDAS"  // Realtek codec device string
      },
      0xBB8,                     // D0 peak power in mW
      0x00                       // Dx peak power in mW
   },
   {
      {
         "\\_SB.PC00.LNK0"       // WF Camera string
      },
      0x32A,                     // D0 peak power in mW without accounting for flash
      0x00                       // Dx peak power in mW
   },
   {
      {
         "\\_SB.PC00.LNK1"       // UF Camera string
      },
      0x33E,                     // D0 peak power in mW without accounting for flash
      0x00                       // Dx peak power in mW
   },
   {
      {
         "\\_SB.PC00.FLM0"       // Flash device string
      },
      0x2328,                    // D0 peak power in mW
      0x00                       // Dx peak power in mW
   }
  })}

!if gSiPkgTokenSpaceGuid.PcdAdlLpSupport == TRUE
## Update in SKU dsc file like below for changing the value as per Board
  ##  [PcdsDynamicExVpd.common.SkuIdAdlMLp4Rvp]
  ##  gBoardModuleTokenSpaceGuid.PcdClwlI2cController|*|{'1'}
  ##  gBoardModuleTokenSpaceGuid.PcdClwlI2cSlaveAddress|*|{0x23}
gBoardModuleTokenSpaceGuid.PcdClwlI2cController|*|{'0'}
gBoardModuleTokenSpaceGuid.PcdClwlI2cSlaveAddress|*|{0x14}
!endif
###
### BoardPkgPcdUpdate.dsc Start
###


[PcdsFeatureFlag]

!if gMinPlatformPkgTokenSpaceGuid.PcdBootStage >= 1
  gMinPlatformPkgTokenSpaceGuid.PcdStopAfterDebugInit|TRUE
!endif

!if gMinPlatformPkgTokenSpaceGuid.PcdBootStage >= 2
  gMinPlatformPkgTokenSpaceGuid.PcdStopAfterDebugInit|FALSE
  gMinPlatformPkgTokenSpaceGuid.PcdStopAfterMemInit|TRUE
!endif

!if gMinPlatformPkgTokenSpaceGuid.PcdBootStage >= 3
  gMinPlatformPkgTokenSpaceGuid.PcdStopAfterMemInit|FALSE
  gMinPlatformPkgTokenSpaceGuid.PcdBootToShellOnly|TRUE
  gBoardModuleTokenSpaceGuid.PcdUefiShellEnable|TRUE
!endif

!if gMinPlatformPkgTokenSpaceGuid.PcdBootStage >= 4
  gMinPlatformPkgTokenSpaceGuid.PcdBootToShellOnly|FALSE
!endif

!if gMinPlatformPkgTokenSpaceGuid.PcdBootStage >= 5
  gBoardModuleTokenSpaceGuid.PcdSecurityEnable|TRUE
!endif

!if gMinPlatformPkgTokenSpaceGuid.PcdBootStage >= 6
  gBoardModuleTokenSpaceGuid.PcdAdvancedFeatureEnable|TRUE
!endif

!if gMinPlatformPkgTokenSpaceGuid.PcdBootStage == 7
  gBoardModuleTokenSpaceGuid.PcdOptimizationEnable|TRUE
!endif

!if gBoardModuleTokenSpaceGuid.PcdSecurityEnable == TRUE
  gMinPlatformPkgTokenSpaceGuid.PcdTpm2Enable|TRUE
  !if $(TARGET) == DEBUG
    gMinPlatformPkgTokenSpaceGuid.PcdSmiHandlerProfileEnable|TRUE
  !endif
!endif  #PcdSecurityEnable

!if gBoardModuleTokenSpaceGuid.PcdOptimizationEnable == TRUE
  gMinPlatformPkgTokenSpaceGuid.PcdPerformanceEnable|TRUE
!endif  #PcdOptimizationEnable

  gBoardModuleTokenSpaceGuid.PcdIntelGopEnable|TRUE                  #LegacyVideoRom.bin and IntelGopDriver.efi

!if gBoardModuleTokenSpaceGuid.PcdAdvancedFeatureEnable == TRUE

  gNhltFeaturePkgTokenSpaceGuid.PcdNhltFeatureEnable|TRUE



!endif

  # XmlCli: Update Pcd Value
#[-start-211108-IB19370023-remove]#
#  gXmlCliFeaturePkgTokenSpaceGuid.PcdXmlCliFeatureEnable|TRUE
#[-end-211108-IB19370023-remove]#

[PcdsFixedAtBuild]
#[-start-211208-TAMT000035-add]#
!if $(S77013_SUPPORT_ENABLE) == YES
  # IPCM Support PCD
  gPlatformModuleTokenSpaceGuid.PcdIPCMSupportEnable|TRUE
!endif
#[-end-211208-TAMT000035-add]
#
# Setting feature PCDs based on boot stages set above
#
!if gMinPlatformPkgTokenSpaceGuid.PcdBootToShellOnly == FALSE
  gSiPkgTokenSpaceGuid.PcdAcpiEnable|TRUE
  gBoardModuleTokenSpaceGuid.PcdAcpiDebugFeatureEnable|TRUE
!endif  #PcdBootToShellOnly

!if gBoardModuleTokenSpaceGuid.PcdAdvancedFeatureEnable == TRUE

  gBoardModuleTokenSpaceGuid.PcdGopConfigBin|FALSE
  gSiPkgTokenSpaceGuid.PcdPpmEnable|TRUE
  gSiPkgTokenSpaceGuid.PcdSmbiosEnable|TRUE
  gSiPkgTokenSpaceGuid.PcdS3Enable|TRUE
  !if gSiPkgTokenSpaceGuid.PcdAdlLpSupport == TRUE
    gSiPkgTokenSpaceGuid.PcdITbtEnable|TRUE
  !else
    gSiPkgTokenSpaceGuid.PcdITbtEnable|FALSE
  !endif
  gSiPkgTokenSpaceGuid.PcdBtgTxtLegacyPkgEnable|TRUE
  gSiPkgTokenSpaceGuid.PcdBootGuardEnable|TRUE
  gSiPkgTokenSpaceGuid.PcdTxtEnable|TRUE
  gSiPkgTokenSpaceGuid.PcdSiCatalogDebugEnable|FALSE

  gSiPkgTokenSpaceGuid.PcdMrcTraceMessageSupported|TRUE

  gPlatformModuleTokenSpaceGuid.PcdBeepStatusCodeEnable|FALSE
  gPlatformModuleTokenSpaceGuid.PcdPostCodeStatusCodeEnable|TRUE
  gPlatformModuleTokenSpaceGuid.PcdTraceHubPostCodeEnable|TRUE
  gSiPkgTokenSpaceGuid.PcdSourceDebugEnable|FALSE
  gSiPkgTokenSpaceGuid.PcdBiosGuardEnable|TRUE
  gSiPkgTokenSpaceGuid.PcdHgEnable|TRUE
  gBoardModuleTokenSpaceGuid.PcdS4Enable|TRUE

  gBoardModuleTokenSpaceGuid.PcdNvmeEnable|TRUE
  gBoardModuleTokenSpaceGuid.PcdIntelRaidEnable|TRUE
  gBoardModuleTokenSpaceGuid.PcdTerminalEnable|TRUE
  gSiPkgTokenSpaceGuid.PcdOverclockEnable|TRUE

  gSiPkgTokenSpaceGuid.PcdEmbeddedEnable|0x0
#[-start-200420-IB17800066-modify]#
  gSiPkgTokenSpaceGuid.PcdAmtEnable|$(AMT_ENABLE)
#[-end-200420-IB17800066-modify]#
  gSiPkgTokenSpaceGuid.PcdAtaEnable|TRUE
  gPlatformModuleTokenSpaceGuid.PcdOneClickRecoveryEnable|TRUE
  gPlatformModuleTokenSpaceGuid.PcdWifiProfileSyncEnable|FALSE

  !if gSiPkgTokenSpaceGuid.PcdAmtEnable == TRUE && gPlatformModuleTokenSpaceGuid.PcdOneClickRecoveryEnable == TRUE
    !if (gSiPkgTokenSpaceGuid.PcdAdlLpSupport == TRUE)
      gPlatformModuleTokenSpaceGuid.PcdWifiProfileSyncEnable|TRUE
    !endif
  !endif

  gPlatformModuleTokenSpaceGuid.PcdIgdIntHookEnable|TRUE
  gPlatformModuleTokenSpaceGuid.PcdAhciEnable|TRUE
  gPlatformModuleTokenSpaceGuid.PcdDTbtEnable|TRUE
#[-start-191111-IB10189001-modify]#
  #
  #  Disable Intel CapsuleEnable, this featuree was owned by Insyde Kernel
  #
  gPlatformModuleTokenSpaceGuid.PcdCapsuleEnable|FALSE
#[-end-191111-IB10189001-modify]#
  gPlatformModuleTokenSpaceGuid.PcdResiliencyEnable|FALSE
  gPlatformModuleTokenSpaceGuid.PcdMeResiliencyEnable|FALSE
  gPlatformModuleTokenSpaceGuid.PcdI2cTouchDriverEnable|TRUE
  gPlatformModuleTokenSpaceGuid.PcdPiI2cStackEnable|TRUE
  gPlatformModuleTokenSpaceGuid.PcdUsb3SerialStatusCodeEnable|FALSE
  gPlatformModuleTokenSpaceGuid.PcdUserAuthenticationEnable|TRUE


  !if (gSiPkgTokenSpaceGuid.PcdAdlLpSupport == TRUE)
  !endif

  gPlatformModuleTokenSpaceGuid.PcdPciHotplugEnable|TRUE
  gPlatformModuleTokenSpaceGuid.PcdUsbTypeCEnable|TRUE
  gPlatformModuleTokenSpaceGuid.PcdVirtualKeyboardEnable|TRUE
  gPlatformModuleTokenSpaceGuid.PcdEbcEnable|TRUE
  gPlatformModuleTokenSpaceGuid.PcdHddPasswordEnable|TRUE
  gPlatformModuleTokenSpaceGuid.PcdNetworkEnable|TRUE
  gPlatformModuleTokenSpaceGuid.PcdGigUndiEnable|TRUE
  gPlatformModuleTokenSpaceGuid.PcdMouseEnable|FALSE
  gPlatformModuleTokenSpaceGuid.PcdSinitAcmBinEnable|FALSE
  gPlatformModuleTokenSpaceGuid.PcdAcmProdBinEnable|FALSE
  gPlatformModuleTokenSpaceGuid.PcdScsiEnable|TRUE
  gPlatformModuleTokenSpaceGuid.PcdJpgEnable|TRUE
  gPlatformModuleTokenSpaceGuid.PcdSmiVariableEnable|TRUE
  gPlatformModuleTokenSpaceGuid.PcdUsbEnable|TRUE
  gPlatformModuleTokenSpaceGuid.PcdUsbFnEnable|TRUE
  gPlatformModuleTokenSpaceGuid.PcdDnxSupportEnable|TRUE
  gPlatformModuleTokenSpaceGuid.PcdPlatformCmosAccessSupport|TRUE
  gPlatformModuleTokenSpaceGuid.PcdEnableSecureErase|TRUE
  gPlatformModuleTokenSpaceGuid.PcdUnitoolEnable|TRUE
  gBoardModuleTokenSpaceGuid.PcdFFUEnable|TRUE

  #
  # Build Switch for bin files.
  #
  !if gSiPkgTokenSpaceGuid.PcdBiosGuardEnable == TRUE
    gBoardModuleTokenSpaceGuid.PcdBiosGuardBinEnable|TRUE            #BiosGuardModule.bin
  !else
    gBoardModuleTokenSpaceGuid.PcdBiosGuardBinEnable|FALSE
  !endif
  gBoardModuleTokenSpaceGuid.PcdStartupAcmProdBinEnable|FALSE        #StartupAcmProd.bin
#[-start-210112-IB18410052-modify]#
  gBoardModuleTokenSpaceGuid.PcdNhltBinEnable|FALSE                  #NhltIcl.bin
#[-end-210112-IB18410052-modify]#
  gPlatformModuleTokenSpaceGuid.PcdSredirBinEnable|TRUE              #Sredir.bin
  gPlatformModuleTokenSpaceGuid.PcdSinitAcmBinEnable|FALSE           #SinitAc.bin
  gPlatformModuleTokenSpaceGuid.PcdH8S2113Present|FALSE
  gPlatformModuleTokenSpaceGuid.PcdNat87393Present|FALSE
  gPlatformModuleTokenSpaceGuid.PcdNct677FPresent|FALSE

  #
  # Build Switch for efi files.
  #
  gBoardModuleTokenSpaceGuid.PcdRaidDriverEfiEnable|TRUE             #RaidDriver.efi
  gBoardModuleTokenSpaceGuid.PcdRsteDriverEfiEnable|TRUE             #SataDriverRste.efi
  gPlatformModuleTokenSpaceGuid.PcdLegacySredirBinEnable|TRUE        #LegacySredir.efi

  gPlatformModuleTokenSpaceGuid.PcdNetworkIp6Enable|TRUE
  gPlatformModuleTokenSpaceGuid.PcdNetworkIscsiEnable|TRUE
  gPlatformModuleTokenSpaceGuid.PcdNetworkVlanEnable|TRUE
  gPlatformModuleTokenSpaceGuid.PcdFatEnable|TRUE
  gPlatformModuleTokenSpaceGuid.PcdCryptoEnable|TRUE    # Current Smbios implementation needs this
  gPlatformModuleTokenSpaceGuid.PcdLzmaEnable|TRUE
  gPlatformModuleTokenSpaceGuid.PcdDxeCompressEnable|TRUE
  gPlatformModuleTokenSpaceGuid.PcdVtioEnable|TRUE
  gPlatformModuleTokenSpaceGuid.PcdUserIdentificationEnable|FALSE
  gPlatformModuleTokenSpaceGuid.PcdTdsEnable|TRUE
  gPlatformModuleTokenSpaceGuid.PcdOpalPasswordEnable|TRUE
  gPlatformModuleTokenSpaceGuid.PcdMemoryTestEnable|TRUE
  gPlatformModuleTokenSpaceGuid.PcdDgrPolicyOverride|FALSE
  !if (gPlatformModuleTokenSpaceGuid.PcdCryptoEnable == TRUE)
    gBoardModuleTokenSpaceGuid.PcdModularCryptoEnable|TRUE
  !endif

  gSiPkgTokenSpaceGuid.PcdOcWdtEnable|TRUE
  gSiPkgTokenSpaceGuid.PcdSerialIoUartEnable|TRUE
  gSiPkgTokenSpaceGuid.PcdIpuEnable|TRUE
  gSiPkgTokenSpaceGuid.PcdIgdEnable|TRUE
  gSiPkgTokenSpaceGuid.PcdPeiDisplayEnable|TRUE
  gSiPkgTokenSpaceGuid.PcdVtdEnable|TRUE
  gSiPkgTokenSpaceGuid.PcdCpuPowerOnConfigEnable|TRUE
  gSiPkgTokenSpaceGuid.PcdSaDmiEnable|TRUE
  gSiPkgTokenSpaceGuid.PcdGnaEnable|TRUE
  gSiPkgTokenSpaceGuid.PcdVmdEnable|TRUE
  gSiPkgTokenSpaceGuid.PcdCpuPcieEnable|TRUE
  gSiPkgTokenSpaceGuid.PcdPttEnable|TRUE
  gSiPkgTokenSpaceGuid.PcdJhiEnable|TRUE
  gSiPkgTokenSpaceGuid.PcdSmmVariableEnable|TRUE
  gSiPkgTokenSpaceGuid.PcdThcEnable|TRUE
  gSiPkgTokenSpaceGuid.PcdOptimizeCompilerEnable|TRUE
  gSiPkgTokenSpaceGuid.PcdPpmEnable|TRUE
  gSiPkgTokenSpaceGuid.PcdPsmiEnable|TRUE
  gSiPkgTokenSpaceGuid.PcdAdlLpSupport|FALSE
  gSiPkgTokenSpaceGuid.PcdSpsStateSaveEnable|TRUE
  gSiPkgTokenSpaceGuid.PcdHybridStorageSupport|TRUE
  gSiPkgTokenSpaceGuid.PcdPpamEnable|TRUE
  gSiPkgTokenSpaceGuid.PcdStatusCodeUseTraceHub|TRUE
!endif #PcdAdvancedFeatureEnable

!if $(TARGET) == RELEASE
  gEfiMdePkgTokenSpaceGuid.PcdDebugPropertyMask|0x0
  gEfiMdePkgTokenSpaceGuid.PcdReportStatusCodePropertyMask|0x3
!else
  gEfiMdePkgTokenSpaceGuid.PcdDebugPropertyMask|0x2F
  gEfiMdeModulePkgTokenSpaceGuid.PcdSerialUseHardwareFlowControl|FALSE
  gEfiMdePkgTokenSpaceGuid.PcdReportStatusCodePropertyMask|0x07
!endif
#
# Silicon feature settings
#
!if gBoardModuleTokenSpaceGuid.PcdSecurityEnable == TRUE
  gPlatformModuleTokenSpaceGuid.PcdTpmEnable|TRUE
!endif



#
# SMBIOS settings
#
# Disabling 64 bit Smbios Entrypoint Structure and populated tables
# Bit0 is to enable 32 bit and Bit1 is to enable 64 bit Smbios tables
!if gSiPkgTokenSpaceGuid.PcdSmbiosEnable == TRUE
  gEfiMdeModulePkgTokenSpaceGuid.PcdSmbiosEntryPointProvideMethod|0x01
!endif

!if gPlatformModuleTokenSpaceGuid.PcdBeepStatusCodeEnable == TRUE
  gBeepDebugFeaturePkgTokenSpaceGuid.PcdStatusCodeUseBeep|TRUE
!else
  gBeepDebugFeaturePkgTokenSpaceGuid.PcdStatusCodeUseBeep|FALSE
!endif

!if gPlatformModuleTokenSpaceGuid.PcdPostCodeStatusCodeEnable == TRUE
  gPostCodeDebugFeaturePkgTokenSpaceGuid.PcdStatusCodeUsePostCode|TRUE
!else
  gPostCodeDebugFeaturePkgTokenSpaceGuid.PcdStatusCodeUsePostCode|FALSE
!endif

!if gMinPlatformPkgTokenSpaceGuid.PcdPerformanceEnable == TRUE
  gEfiMdePkgTokenSpaceGuid.PcdPerformanceLibraryPropertyMask|0x1
  gEfiMdeModulePkgTokenSpaceGuid.PcdMaxPeiPerformanceLogEntries|140
  gEfiMdeModulePkgTokenSpaceGuid.PcdEdkiiFpdtStringRecordEnableOnly|TRUE
!endif

!if gMinPlatformPkgTokenSpaceGuid.PcdSmiHandlerProfileEnable == TRUE
  gEfiMdeModulePkgTokenSpaceGuid.PcdSmiHandlerProfilePropertyMask|0x1
!endif

!if (gIntelFsp2WrapperTokenSpaceGuid.PcdFspModeSelection == 1)
  #
  # In FSP API mode, the PciExpressBaseAddress is not necessary DynamicEx
  #
  gSiPkgTokenSpaceGuid.PcdSiPciExpressBaseAddress|gEfiMdePkgTokenSpaceGuid.PcdPciExpressBaseAddress
!endif

# Temporary RAM Size for FSP build
gIntelFsp2PkgTokenSpaceGuid.PcdTemporaryRamBase|0xFEF00000
gIntelFsp2PkgTokenSpaceGuid.PcdTemporaryRamSize|0x00100000
!if gIntelFsp2WrapperTokenSpaceGuid.PcdFspModeSelection == 1
  #
  # FSP API mode will only establish separate Heap.
  #
  gIntelFsp2PkgTokenSpaceGuid.PcdFspTemporaryRamSize|0x00020000
!else
  #
  # FSP Dispatch mode will not establish separate Stack or Heap.
  #
  gIntelFsp2PkgTokenSpaceGuid.PcdFspTemporaryRamSize|0
!endif

!if $(TARGET) == DEBUG
  !if gSiPkgTokenSpaceGuid.PcdSerialIoUartEnable == TRUE
#[-start-210927-IB09480158-modify]#
  #
  # Linking MDE PCD that comes from kernel and client common PCD that comes from intel for efidebug build (namke efidebug, $(EFI_DEBUG) == YES)
  #
    !if $(FIRMWARE_PERFORMANCE) == NO
      gSiPkgTokenSpaceGuid.PcdSerialIoUartDebugEnable|1
      gPlatformModuleTokenSpaceGuid.PcdStatusCodeUseSerialIoUart|TRUE
    !endif
#[-end-210927-IB09480158-modify]#
  !endif
!endif

!if gSiPkgTokenSpaceGuid.PcdAdlLpSupport == TRUE
  gSiPkgTokenSpaceGuid.PcdSerialIoUartNumber|0
!endif


#
# FSP Binary base address will be set in FDF basing on flash map
#
#[-start-191111-IB10189001-remove]#
# gino: kernel issue, we cannot follow RC
#  gIntelFsp2WrapperTokenSpaceGuid.PcdFsptBaseAddress|0
#  gIntelFsp2WrapperTokenSpaceGuid.PcdFspmBaseAddress|0
#[-end-191111-IB10189001-remove]#


!if gMinPlatformPkgTokenSpaceGuid.PcdUefiSecureBootEnable == TRUE
  ## Pcd for OptionRom.
  #  Image verification policy settings:
  #  ALWAYS_EXECUTE                         0x00000000
  #  NEVER_EXECUTE                          0x00000001
  #  ALLOW_EXECUTE_ON_SECURITY_VIOLATION    0x00000002
  #  DEFER_EXECUTE_ON_SECURITY_VIOLATION    0x00000003
  #  DENY_EXECUTE_ON_SECURITY_VIOLATION     0x00000004
  #  QUERY_USER_ON_SECURITY_VIOLATION       0x00000005
  gEfiSecurityPkgTokenSpaceGuid.PcdOptionRomImageVerificationPolicy|0x00000004
  gEfiSecurityPkgTokenSpaceGuid.PcdRsa2048Sha256PublicKeyBuffer|{0x8A, 0xF3, 0x87, 0x6B, 0x0F, 0xD4, 0xA3, 0x90, 0x15, 0xD7, 0x40, 0xC5, 0x3A, 0x94, 0x9B, 0xF4, 0xE0, 0x58, 0x53, 0x58, 0x87, 0x89, 0x67, 0x84, 0x60, 0xAF, 0x8E, 0xB4, 0x16, 0x1F, 0x52, 0x51}
!endif

!if gSiPkgTokenSpaceGuid.PcdS3Enable == TRUE
  ## Indicates if to shadow PEIM on S3 boot path after memory is ready.<BR><BR>
  #   TRUE  - Shadow PEIM on S3 boot path after memory is ready.<BR>
  #   FALSE - Not shadow PEIM on S3 boot path after memory is ready.<BR>
  # @Prompt Shadow Peim On S3 Boot.
  gEfiMdeModulePkgTokenSpaceGuid.PcdShadowPeimOnS3Boot|TRUE

  #
  # For RS5 (1 core, 2 thread), increase page number to avoid S3 exit fail
  #
  gEfiMdeModulePkgTokenSpaceGuid.PcdS3BootScriptRuntimeTableReservePageNumber|0x4
!endif

!if gSiPkgTokenSpaceGuid.PcdPpamEnable == TRUE
  gUefiCpuPkgTokenSpaceGuid.PcdCpuMsegSize|0x11A000
#[-start-211001-IB16810166-add]#
  #
  # 32MB TSEG.
  #
  gSiPkgTokenSpaceGuid.PcdTsegSize|0x2000000
#[-end-211001-IB16810166-add]#
!endif

#
# PlatformTemporaryMmioAssignmentEnd
#


!if gBoardModuleTokenSpaceGuid.PcdAdvancedFeatureEnable == FALSE
  gMinPlatformPkgTokenSpaceGuid.PcdPciReservedMemBase |0x80000000
  gMinPlatformPkgTokenSpaceGuid.PcdPciReservedMemLimit|0xC0000000
!endif

!if gPlatformModuleTokenSpaceGuid.PcdCapsuleEnable == TRUE
  gPlatformModuleTokenSpaceGuid.PcdPlatformMeEDebugCapsuleImageTypeIdGuid|{GUID(gFmpDevicePlatformMonolithicGuid), GUID(gFmpDevicePlatformMeGuid)}
  gEfiMdeModulePkgTokenSpaceGuid.PcdSystemFmpCapsuleImageTypeIdGuid|{GUID(gFmpDevicePlatformMonolithicGuid), GUID(gFmpDevicePlatformBiosGuid)}
!endif

#
# Overriding this PCD to reduce the NVRAM Reclaim frequency
#
!if gPlatformModuleTokenSpaceGuid.PcdNetworkEnable == TRUE
    gEfiNetworkPkgTokenSpaceGuid.PcdMaxIScsiAttemptNumber|0x04
!endif

[PcdsFixedAtBuild.IA32]
gIntelFsp2PkgTokenSpaceGuid.PcdGlobalDataPointerAddress|0xFED00148
gIntelFsp2WrapperTokenSpaceGuid.PcdPeiMinMemSize|0x3800000

[PcdsFixedAtBuild.X64]
!if gSiPkgTokenSpaceGuid.PcdS3Enable == TRUE
  # @todo the size needs to be optimized.
  # Increase the memory size to resolve S3 exit failure due to memory resource shortage
  !if $(TARGET) == DEBUG
    gPlatformModuleTokenSpaceGuid.PcdS3AcpiReservedMemorySize|0x2600000
  !else
    gPlatformModuleTokenSpaceGuid.PcdS3AcpiReservedMemorySize|0x2600000
  !endif
!endif

!if gPlatformModuleTokenSpaceGuid.PcdNetworkEnable == TRUE
  gEfiNetworkPkgTokenSpaceGuid.PcdAllowHttpConnections|TRUE
!endif


# Update PcdsFeatureFlag acording to PcdsFixedAtBuild changes
[PcdsFeatureFlag]
#[-start-201120-IB16810139-remove]#
#!if gPlatformModuleTokenSpaceGuid.PcdCapsuleEnable == TRUE
#  gEfiMdeModulePkgTokenSpaceGuid.PcdSupportUpdateCapsuleReset|TRUE
#!else
#  gEfiMdeModulePkgTokenSpaceGuid.PcdSupportUpdateCapsuleReset|FALSE
#!endif
#[-end-201120-IB16810139-remove]#

#[-start-200420-IB17800056-modify]#
  # ADL PO Temporary remove
!if gBoardModuleTokenSpaceGuid.PcdAcpiDebugFeatureEnable == TRUE
#  gAcpiDebugFeaturePkgTokenSpaceGuid.PcdAcpiDebugFeatureEnable|TRUE
!else
#  gAcpiDebugFeaturePkgTokenSpaceGuid.PcdAcpiDebugFeatureEnable|FALSE
!endif
#[-end-200420-IB17800056-modify]#

[PcdsDynamicHii.X64.DEFAULT]

!if gMinPlatformPkgTokenSpaceGuid.PcdPerformanceEnable == TRUE
  gEfiMdePkgTokenSpaceGuid.PcdPlatformBootTimeOut|L"Timeout"|gEfiGlobalVariableGuid|0x0|1 # Variable: L"Timeout"
!endif

[PcdsDynamicDefault]
#[-start-191111-IB10189001-remove]#
# gino: kernel issue, we cannot follow RC
#  gIntelFsp2WrapperTokenSpaceGuid.PcdFspsBaseAddress|0xFFD20000
#[-end-191111-IB10189001-remove]#
gIntelFsp2WrapperTokenSpaceGuid.PcdFspmUpdDataAddress|0x00000000
gIntelFsp2WrapperTokenSpaceGuid.PcdFspsUpdDataAddress|0x00000000

#[-start-201217-IB16560234-remove]#
# H2O use type PcdsFixedAtBuild
#!if $(TARGET) == DEBUG
#  gEfiMdeModulePkgTokenSpaceGuid.PcdStatusCodeUseSerial|TRUE
#!else
#  gEfiMdeModulePkgTokenSpaceGuid.PcdStatusCodeUseSerial|FALSE
#!endif
#[-end-201217-IB16560234-remove]#

!if gBoardModuleTokenSpaceGuid.PcdAdvancedFeatureEnable == TRUE
  gMinPlatformPkgTokenSpaceGuid.PcdPciReservedMemBase |0x80000000
  gMinPlatformPkgTokenSpaceGuid.PcdPciReservedMemLimit|0xC0000000
!endif

!if (gIntelFsp2WrapperTokenSpaceGuid.PcdFspModeSelection == 1)
  ## Specifies max supported number of Logical Processors.
  # @Prompt Configure max supported number of Logical Processorss
  gUefiCpuPkgTokenSpaceGuid.PcdCpuMaxLogicalProcessorNumber|16

  ## Specifies the AP wait loop state during POST phase.
  #  The value is defined as below.
  #  1: Place AP in the Hlt-Loop state.
  #  2: Place AP in the Mwait-Loop state.
  #  3: Place AP in the Run-Loop state.
  # @Prompt The AP wait loop state.
  gUefiCpuPkgTokenSpaceGuid.PcdCpuApLoopMode|2

  ## Specifies the AP target C-state for Mwait during POST phase.
  #  The default value 0 means C1 state.
  #  The value is defined as below.<BR><BR>
  # @Prompt The specified AP target C-state for Mwait.
  gUefiCpuPkgTokenSpaceGuid.PcdCpuApTargetCstate|0

  gSiPkgTokenSpaceGuid.PcdPciExpressRegionLength|0x10000000
!endif

!if gBoardModuleTokenSpaceGuid.PcdS4Enable == FALSE
  gEfiMdeModulePkgTokenSpaceGuid.PcdResetOnMemoryTypeInformationChange|FALSE
!else
  gEfiMdeModulePkgTokenSpaceGuid.PcdResetOnMemoryTypeInformationChange|TRUE
!endif

!if gMinPlatformPkgTokenSpaceGuid.PcdTpm2Enable == TRUE
  gEfiSecurityPkgTokenSpaceGuid.PcdTpmInstanceGuid|{0x5a, 0xf2, 0x6b, 0x28, 0xc3, 0xc2, 0x8c, 0x40, 0xb3, 0xb4, 0x25, 0xe6, 0x75, 0x8b, 0x73, 0x17}

  gEfiSecurityPkgTokenSpaceGuid.PcdTcg2HashAlgorithmBitmap|0xFFFFFFFF
  gEfiSecurityPkgTokenSpaceGuid.PcdTpm2HashMask|0x0000001F

  gEfiSecurityPkgTokenSpaceGuid.PcdTpm2InitializationPolicy|1
  gEfiSecurityPkgTokenSpaceGuid.PcdTpm2SelfTestPolicy|0
  gEfiSecurityPkgTokenSpaceGuid.PcdTpm2ScrtmPolicy|1

  gBoardModuleTokenSpaceGuid.PcdTpm2HierarchyChangeAuthPlatform|1
  gBoardModuleTokenSpaceGuid.PcdTpm2HierarchyControlPlatform|1
  gBoardModuleTokenSpaceGuid.PcdTpm2HierarchyControlEndorsement|1
  gBoardModuleTokenSpaceGuid.PcdTpm2HierarchyControlOwner|1
  gBoardModuleTokenSpaceGuid.PcdTpm2ChangeEps|0
  gBoardModuleTokenSpaceGuid.PcdTpm2ChangePps|0
  gBoardModuleTokenSpaceGuid.PcdTpm2Clear|0

  !if gSiPkgTokenSpaceGuid.PcdTxtEnable == TRUE
    ## The PCD is used to specify if Tcg2Platform module supports TXT provision.
    gBoardModuleTokenSpaceGuid.PcdTpm2TxtProvisionSupport|TRUE
  !endif

!endif

!if gPlatformModuleTokenSpaceGuid.PcdTpmEnable == TRUE
  gEfiSecurityPkgTokenSpaceGuid.PcdTpmInitializationPolicy|1
  gEfiSecurityPkgTokenSpaceGuid.PcdTpmScrtmPolicy|1
!endif

#[-start-210908-IB18770039-remove]#
# PcdHttpIoTimeout already defined in NetworkPkg.dec cause build error, 
# but RC 2347 has removed it from NetworkPkg.dec and define in BoardPkgUpdate.dsc.
#!if gPlatformModuleTokenSpaceGuid.PcdOneClickRecoveryEnable == TRUE
#  ## This value is used to configure the request and response timeout when getting the recovery image from
#  #  remote source. The value of 5000 will be the default 5 second timeout value
#  # @Prompt HTTP Boot OS Image Request and Response Timeout
#  gEfiNetworkPkgTokenSpaceGuid.PcdHttpIoTimeout|5000
#!endif
#[-end-210908-IB18770039-remove]#

#[-start-210422-IB14630477-add]#
  gBoardModuleTokenSpaceGuid.PcdEcBiosGuardEnable|FALSE
#[-end-210422-IB14630477-add]#
[PcdsDynamicExDefault]

!if gIntelFsp2WrapperTokenSpaceGuid.PcdFspModeSelection == 0
  #
  # Include FSP PCD settings.
  #
  !include $(PLATFORM_FSP_BIN_PACKAGE)/FspPkgPcdShare.dsc
!endif

#[-start-191111-IB10189001-remove]#
# gino: kernel issue, we cannot follow RC
#
# FSP Binary base address will be set in FDF basing on flash map
#
#  gIntelFsp2WrapperTokenSpaceGuid.PcdFspsBaseAddress|0
#[-end-191111-IB10189001-remove]#

#[-start-201207-IB10181021-remove]#
# Move this line to Project.dsc for the customer use
  # !include $(PLATFORM_BOARD_PACKAGE)/SBCVpdStructurePcd/AllStructPCD.dsc
#[-end-201207-IB10181021-remove]#
#
# dsc config file done.
# ====================================================================================================================
# ====================================================================================================================


#[-start-200420-IB17800056-add]#
  # ADL RC Add
[LibraryClasses.common.PEIM]
  FirmwareBootMediaInfoLib|$(PLATFORM_BOARD_PACKAGE)/Library/PeiFirmwareBootMediaInfoLib/PeiFirmwareBootMediaInfoLib.inf
#[-end-200420-IB17800056-add]#

[LibraryClasses.common]
  PciSegmentInfoLib|MinPlatformPkg/Pci/Library/PciSegmentInfoLibSimple/PciSegmentInfoLibSimple.inf
  MmioInitLib|AlderLakeBoardPkg/Library/BaseMmioInitLib/BaseMmioInitLib.inf
  SiliconInitLib|AlderLakeBoardPkg/Library/SiliconInitLib/SiliconInitLib.inf
  PeiBootModeLib|AlderLakeBoardPkg/Library/PeiBootModeLib/PeiBootModeLib.inf
  BoardIdsLib|$(PLATFORM_BOARD_PACKAGE)/Library/PeiDxeBoardIdsLib/PeiDxeBoardIdsLib.inf
  WakeupEventLib|AlderLakeBoardPkg/Library/WakeupEventLib/WakeupEventLib.inf
  PlatformHookLib|$(PLATFORM_BOARD_PACKAGE)/Library/BasePlatformHookLib/BasePlatformHookLib.inf

!if gSiPkgTokenSpaceGuid.PcdEmbeddedEnable == 0x1
#
# Sub Region FVs
#
  PeiSubRegionLib|$(PLATFORM_SAMPLE_CODE_DEC_NAME)/Features/SubRegion/PeiSubRegionLib/PeiSubRegionLib.inf
#
# TSN
#
  PeiTsnFvLib|$(PLATFORM_SAMPLE_CODE_DEC_NAME)/Features/Tsn/PeiTsnFvLib/PeiTsnFvLib.inf
!else
  PeiSubRegionLib|$(PLATFORM_SAMPLE_CODE_DEC_NAME)/Features/SubRegion/PeiSubRegionLib/PeiSubRegionLibNull.inf
  PeiTsnFvLib|$(PLATFORM_SAMPLE_CODE_DEC_NAME)/Features/Tsn/PeiTsnFvLib/PeiTsnFvLibNull.inf
!endif
#[-start-211208-TAMT000035-add]#
#For IPCM
!if $(S77013_SUPPORT_ENABLE) == YES
  I2cAccessLib|$(PLATFORM_FULL_PACKAGE)/E3/Library/PeiI2cAccessLib/PeiI2cAccessLib.inf
!endif
#[-end-211208-TAMT000035-add]#
[LibraryClasses.IA32]
#
# PEI phase common
#
  TraceHubDebugLib|$(PLATFORM_SI_PACKAGE)/Library/TraceHubDebugLibSvenTx/PeiTraceHubDebugLibSvenTx.inf
#
# Features
#
#[-start-200420-IB17800056-add]#
  # ADL RC add
  FirmwareBootMediaLib|IntelSiliconPkg/Library/PeiDxeSmmBootMediaLib/PeiFirmwareBootMediaLib.inf
  PeiGetFvInfoLib|$(PLATFORM_FULL_PACKAGE)/Library/PeiGetFvInfoLib/PeiGetFvInfoLib.inf
  BiosIdLib|BoardModulePkg/Library/BiosIdLib/PeiBiosIdLib.inf
#[-end-200420-IB17800056-add]#
#!if gBoardModuleTokenSpaceGuid.PcdSetupEnable == TRUE
  !include AlderLakeBoardPkg/Features/Setup/Include/Dsc/SetupPeiLib.dsc
#!endif
#[-start-200420-IB17800056-add]#
  FirmwareBootMediaLib|IntelSiliconPkg/Library/PeiDxeSmmBootMediaLib/PeiFirmwareBootMediaLib.inf
#[-end-200420-IB17800056-add]#
  DccProgramLib|$(PLATFORM_BOARD_PACKAGE)/Library/DccProgramLib/DccProgramLib.inf

[LibraryClasses.IA32.PEIM]
  #
  # This is library instance of Cpu Hob Library, replacing the library instance from MinPlatform.
  #
  ReportCpuHobLib|$(PLATFORM_SI_PACKAGE)/Fru/AdlCpu/PeiReportCpuHob/Library/PeiReportCpuHobLib/PeiReportCpuHobLib.inf

[LibraryClasses.X64]

  TraceHubDebugLib|$(PLATFORM_SI_PACKAGE)/Library/TraceHubDebugLibSvenTx/DxeTraceHubDebugLibSvenTx.inf

  BoardAcpiTableLib|MinPlatformPkg/Acpi/Library/MultiBoardAcpiSupportLib/DxeMultiBoardAcpiSupportLib.inf

  DccProgramLib|$(PLATFORM_BOARD_PACKAGE)/Library/DccProgramLib/DccProgramLib.inf
#[-start-200420-IB17800056-add]#
  # ADL RC add
  BiosIdLib|BoardModulePkg/Library/BiosIdLib/DxeBiosIdLib.inf
!if gSiPkgTokenSpaceGuid.PcdSmbiosEnable == TRUE
  DxeSmbiosProcessorLib|$(PLATFORM_SI_PACKAGE)/Library/DxeSmbiosProcessorLib/DxeSmbiosProcessorLib.inf
!else
  DxeSmbiosProcessorLib|$(PLATFORM_SI_PACKAGE)/Library/DxeSmbiosProcessorLibNull/DxeSmbiosProcessorLibNull.inf
!endif
#[-end-200420-IB17800056-add]#

#[-start-200420-IB17800056-add]#
  FirmwareBootMediaLib|IntelSiliconPkg/Library/PeiDxeSmmBootMediaLib/DxeSmmFirmwareBootMediaLib.inf
#[-end-200420-IB17800056-add]#

[Components.IA32]
#
# Platform
#
#[-start-200420-IB17800056-modify]#
  # ADL PO Temporary remove
  #$(PLATFORM_BOARD_PACKAGE)/BootMediaInfo/BootMediaInfoPei.inf
#[-end-200420-IB17800056-modify]#
  BoardModulePkg/FirmwareBootMediaInfo/FirmwareBootMediaInfoPei.inf

#[-start-210421-IB11790414-modify]#
  $(PLATFORM_BOARD_PACKAGE)/BiosInfo/BiosInfo.inf {
   <LibraryClasses>
    NULL|$(PLATFORM_FULL_PACKAGE)/Library/PeiBootGuardEventLogLib/PeiBootGuardEventLogLib.inf
  }
#[-end-210421-IB11790414-modify]#
#
# PCT PEIM Parser
#
  $(PLATFORM_SAMPLE_CODE_DEC_NAME)/Features/Pct/PlatformConfigTool.inf {
#    !if gPlatformModuleTokenSpaceGuid.PcdResiliencyEnable == TRUE
    !if gInsydeTokenSpaceGuid.PcdH2OBiosUpdateFaultToleranceResiliencyEnabled == TRUE
    # To reduce FvPrememory size in Resiliency debug build
      <LibraryClasses>
        DebugLib|MdePkg/Library/BaseDebugLibNull/BaseDebugLibNull.inf
    !endif
  }

#[-start-210730-IB16560268-modify]#
#[-start-201229-IB06462251-modify]
#[-start-200420-IB17800058-1-modify]
  $(PLATFORM_FEATURES_PATH)/PciePhyFwLoading/PciePhyFwLoadingInit/Pei/PeiPciePhyFwLoadingInit.inf {
    <Depex>
    gEfiPeiMemoryDiscoveredPpiGuid
  }
#[-end-200420-IB17800058-1-modify]
#[-end-201229-IB06462251-modify]
#[-end-210730-IB16560268-modify]#
#[-start-201223-IB19010009-add]#
!if gSiPkgTokenSpaceGuid.PcdAmtEnable == TRUE
  $(PLATFORM_FEATURES_PATH)/Amt/AmtStatusCodePei/AmtStatusCodePei.inf
!endif
#[-end-201223-IB19010009-add]#

#
# Extended BIOS Region validation PEIM
#
!if gPlatformModuleTokenSpaceGuid.PcdExtendedBiosRegionSupport == TRUE
$(PLATFORM_FULL_PACKAGE)/Platform/ValidateExtendedBiosRegion/Pei/ValidateExtendedBiosRegionPostMem.inf
!endif
#[-start-211208-TAMT000035-add]#
!if $(S77013_SUPPORT_ENABLE) == YES
###
### IPCM (Intel Integrated Power Consumption Measurement
###
!if gPlatformModuleTokenSpaceGuid.PcdIPCMSupportEnable == TRUE
  $(PLATFORM_FULL_PACKAGE)/E3/Pei/E3DonglePei/E3DonglePei.inf {
      <LibraryClasses>
        E3DongleLib|$(PLATFORM_FULL_PACKAGE)/E3/Library/E3DongleLib/E3DongleLib.inf
        EcMiscLib|$(PLATFORM_FULL_PACKAGE)/Library/BaseEcMiscLib/BaseEcMiscLib.inf
        EcLib|$(PLATFORM_FULL_PACKAGE)/Library/BaseEcLib/BaseEcLib.inf
        EcHwLib|$(PLATFORM_FULL_PACKAGE)/Library/BaseEcHwLib/BaseEcHwLib.inf
  }
!endif  
!endif
#[-end-211208-TAMT000035-add]#
[Components.X64]
!if gMinPlatformPkgTokenSpaceGuid.PcdBootToShellOnly == FALSE
!if gBoardModuleTokenSpaceGuid.PcdAdvancedFeatureEnable == TRUE
  $(PLATFORM_BOARD_PACKAGE)/Acpi/AdvancedAcpiDxe/AdvancedAcpiDxe.inf
  $(PLATFORMSAMPLE_PACKAGE)/Features/Acpi/AcpiTables/AcpiFeatures.inf
  $(PLATFORM_BOARD_PACKAGE)/Acpi/AcpiTables/AcpiTables.inf
  $(PLATFORM_BOARD_PACKAGE)/Acpi/AdvancedAcpiDxe/AdvancedPmaxDxe.inf
!else
  MinPlatformPkg/Acpi/AcpiTables/AcpiPlatform.inf {
    <LibraryClasses>
!if gSiPkgTokenSpaceGuid.PcdAdlLpSupport == FALSE
      NULL|$(PROJECT_ADL_S_BOARDS)/Library/BoardAcpiLib/BaseMultiBoardAcpiSupportLib.inf
!else
      NULL|$(PROJECT_ADL_P_BOARDS)/Library/BoardAcpiLib/BaseMultiBoardAcpiSupportLib.inf
      NULL|$(PROJECT_ADL_U_SIMICS_BOARDS)/Library/BoardAcpiLib/BaseMultiBoardAcpiSupportLib.inf
      NULL|$(PROJECT_ADL_M_BOARDS)/Library/BoardAcpiLib/BaseMultiBoardAcpiSupportLib.inf
!endif
  }
  $(PLATFORM_BOARD_PACKAGE)/Acpi/BoardAcpiDxe/BoardAcpiDxe.inf {
    <LibraryClasses>
!if gSiPkgTokenSpaceGuid.PcdAdlLpSupport == FALSE
      NULL|$(PROJECT_ADL_S_BOARDS)/Library/BoardAcpiLib/BaseMultiBoardAcpiSupportLib.inf
!else
      NULL|$(PROJECT_ADL_P_BOARDS)/Library/BoardAcpiLib/BaseMultiBoardAcpiSupportLib.inf
      NULL|$(PROJECT_ADL_M_BOARDS)/Library/BoardAcpiLib/BaseMultiBoardAcpiSupportLib.inf
      NULL|$(PROJECT_ADL_U_SIMICS_BOARDS)/Library/BoardAcpiLib/BaseMultiBoardAcpiSupportLib.inf
!endif
  }
!endif # PcdAdvancedFeatureEnable
!endif # PcdBootToShellOnly

#[-start-200420-IB17800056-add]#
  # ADL RC add
  $(PLATFORM_BOARD_PACKAGE)/BootMediaInfo/BootMediaInfo.inf
#[-end-200420-IB17800056-add]#

#[-start-201223-IB19010009-add]#
!if gSiPkgTokenSpaceGuid.PcdAmtEnable == TRUE
#[-start-201223-IB19010010-add]#
  !include MebxFeaturePkg/Include/MebxFeature.dsc
#[-end-201223-IB19010010-add]#
  $(PLATFORM_FEATURES_PATH)/Amt/AmtSaveMebxConfigDxe/AmtSaveMebxConfigDxe.inf
  $(PLATFORM_FEATURES_PATH)/Amt/AmtPetAlertDxe/AmtPetAlertDxe.inf
  $(PLATFORM_FEATURES_PATH)/Amt/AsfTable/AsfTable.inf
  $(PLATFORM_FEATURES_PATH)/Amt/AmtMacPassThrough/AmtMacPassThrough.inf
  $(PLATFORM_FEATURES_PATH)/Amt/AmtWrapperDxe/AmtWrapperDxe.inf
#[-start-210112-IB16560236-modify]#
!if gPlatformModuleTokenSpaceGuid.PcdOneClickRecoveryEnable == TRUE
   $(PLATFORM_FEATURES_PATH)/Amt/OneClickRecovery/OneClickRecovery.inf {
     <LibraryClasses>
       UefiBootManagerLib|MdeModulePkg/Library/UefiBootManagerLib/UefiBootManagerLib.inf

   }
!endif
#[-end-210112-IB16560236-modify]#
!if gPlatformModuleTokenSpaceGuid.PcdEnableSecureErase == TRUE
  $(PLATFORM_FEATURES_PATH)/Amt/SecureEraseDxe/SecureEraseDxe.inf
!endif
!endif
!if gPlatformModuleTokenSpaceGuid.PcdRemotePlatformEraseSupport == TRUE
  $(PLATFORM_FEATURES_PATH)/Rpe/RemotePlatformErase.inf
!endif
#[-end-201223-IB19010009-add]#
