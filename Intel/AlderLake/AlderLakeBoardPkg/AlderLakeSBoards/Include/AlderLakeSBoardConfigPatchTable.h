/** @file


 @copyright
  INTEL CONFIDENTIAL
  Copyright 2016 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains a 'Sample Driver' and is licensed as such under the terms
  of your license agreement with Intel or your vendor. This file may be modified
  by the user, subject to the additional terms of the license agreement.

@par Specification Reference:
**/

#ifndef _ALDER_LAKE_CONFIG_PATCH_TABLE_H_
#define _ALDER_LAKE_CONFIG_PATCH_TABLE_H_

#include <Library/SetupInitLib.h>
#include <SetupVariable.h>
#include <Protocol/RetimerCapsuleUpdate.h>

CONFIG_PATCH_TABLE  mAlderlakeSDdr4RvpSetupConfigPatchTable[] = {
//{---------------Type--------Size-------------------------------------Offset----------------------------------------------------------Data----------------------},
  { SIZE_OF_FIELD(SETUP_DATA, TbtLegacyModeSupport),                   OFFSET_OF(SETUP_DATA, TbtLegacyModeSupport),                    0                         },
  { SIZE_OF_FIELD(SETUP_DATA, AuxOriOverrideSupport),                  OFFSET_OF(SETUP_DATA, AuxOriOverrideSupport),                   0                         },
  { SIZE_OF_FIELD(SETUP_DATA, HebcValueSupport),                       OFFSET_OF(SETUP_DATA, HebcValueSupport),                        1                         },
  { SIZE_OF_FIELD(SETUP_DATA, PchI2cSensorDevicePort[SERIAL_IO_I2C0]), OFFSET_OF(SETUP_DATA, PchI2cSensorDevicePort[SERIAL_IO_I2C0]),  SERIAL_IO_I2C_TOUCHPAD    },
  { SIZE_OF_FIELD(SETUP_DATA, PchI2cSensorDevicePort[SERIAL_IO_I2C1]), OFFSET_OF(SETUP_DATA, PchI2cSensorDevicePort[SERIAL_IO_I2C1]),  SERIAL_IO_I2C_TOUCHPANEL  },
  { SIZE_OF_FIELD(SETUP_DATA, PchSpiSensorDevicePort[SERIAL_IO_SPI1]), OFFSET_OF(SETUP_DATA, PchSpiSensorDevicePort[SERIAL_IO_SPI1]),  SERIAL_IO_SPI_FINGERPRINT },
  { SIZE_OF_FIELD(SETUP_DATA, NativeAspmEnable),                       OFFSET_OF(SETUP_DATA, NativeAspmEnable),                        0                         },
  { SIZE_OF_FIELD(SETUP_DATA, PepGfx),                                 OFFSET_OF(SETUP_DATA, PepGfx),                                  1                         },
  { SIZE_OF_FIELD(SETUP_DATA, PepPeg0),                                OFFSET_OF(SETUP_DATA, PepPeg0),                                 1                         },
  { SIZE_OF_FIELD(SETUP_DATA, PepSataEnumeration),                     OFFSET_OF(SETUP_DATA, PepSataEnumeration),                      1                         },
  { SIZE_OF_FIELD(SETUP_DATA, PepI2c0),                                OFFSET_OF(SETUP_DATA, PepI2c0),                                 1                         },
  { SIZE_OF_FIELD(SETUP_DATA, PepI2c1),                                OFFSET_OF(SETUP_DATA, PepI2c1),                                 1                         },
  { SIZE_OF_FIELD(SETUP_DATA, PepI2c2),                                OFFSET_OF(SETUP_DATA, PepI2c2),                                 1                         },
  { SIZE_OF_FIELD(SETUP_DATA, PepI2c3),                                OFFSET_OF(SETUP_DATA, PepI2c3),                                 1                         },
  { SIZE_OF_FIELD(SETUP_DATA, PepI2c4),                                OFFSET_OF(SETUP_DATA, PepI2c4),                                 1                         },
  { SIZE_OF_FIELD(SETUP_DATA, PepI2c5),                                OFFSET_OF(SETUP_DATA, PepI2c5),                                 1                         },
  { SIZE_OF_FIELD(SETUP_DATA, PepSpi),                                 OFFSET_OF(SETUP_DATA, PepSpi),                                  1                         },
  { SIZE_OF_FIELD(SETUP_DATA, PepXhci),                                OFFSET_OF(SETUP_DATA, PepXhci),                                 1                         },
  { SIZE_OF_FIELD(SETUP_DATA, PepAudio),                               OFFSET_OF(SETUP_DATA, PepAudio),                                1                         },
  { SIZE_OF_FIELD(SETUP_DATA, SysFwUpdateSkipPowerCheck),              OFFSET_OF (SETUP_DATA, SysFwUpdateSkipPowerCheck),              1                         },
  { SIZE_OF_FIELD(SETUP_DATA, TbtSetupFormSupport),                    OFFSET_OF(SETUP_DATA, TbtSetupFormSupport),                     1                         },
  { SIZE_OF_FIELD(SETUP_DATA, PepCsme),                                OFFSET_OF(SETUP_DATA, PepCsme),                                 1                         },
  { SIZE_OF_FIELD(SETUP_DATA, PepGbe),                                 OFFSET_OF(SETUP_DATA, PepGbe),                                  1                         },
  { SIZE_OF_FIELD(SETUP_DATA, PepTcss),                                OFFSET_OF(SETUP_DATA, PepTcss),                                 1                         },
  { SIZE_OF_FIELD(SETUP_DATA, MipiCam_ControlLogic0),                  OFFSET_OF(SETUP_DATA, MipiCam_ControlLogic0),                   0                         },
  { SIZE_OF_FIELD(SETUP_DATA, MipiCam_ControlLogic1),                  OFFSET_OF(SETUP_DATA, MipiCam_ControlLogic1),                   0                         },
  { SIZE_OF_FIELD(SETUP_DATA, MipiCam_ControlLogic2),                  OFFSET_OF(SETUP_DATA, MipiCam_ControlLogic2),                   0                         },
  { SIZE_OF_FIELD(SETUP_DATA, MipiCam_ControlLogic3),                  OFFSET_OF(SETUP_DATA, MipiCam_ControlLogic3),                   0                         },
  { SIZE_OF_FIELD(SETUP_DATA, MipiCam_ControlLogic4),                  OFFSET_OF(SETUP_DATA, MipiCam_ControlLogic3),                   0                         },
  { SIZE_OF_FIELD(SETUP_DATA, MipiCam_ControlLogic5),                  OFFSET_OF(SETUP_DATA, MipiCam_ControlLogic3),                   0                         },
  { SIZE_OF_FIELD(SETUP_DATA, MipiCam_Link0),                          OFFSET_OF(SETUP_DATA, MipiCam_Link0),                           0                         },
  { SIZE_OF_FIELD(SETUP_DATA, MipiCam_Link1),                          OFFSET_OF(SETUP_DATA, MipiCam_Link1),                           0                         },
  { SIZE_OF_FIELD(SETUP_DATA, MipiCam_Link2),                          OFFSET_OF(SETUP_DATA, MipiCam_Link2),                           0                         },
  { SIZE_OF_FIELD(SETUP_DATA, MipiCam_Link3),                          OFFSET_OF(SETUP_DATA, MipiCam_Link3),                           0                         },
  { SIZE_OF_FIELD(SETUP_DATA, MipiCam_Link4),                          OFFSET_OF(SETUP_DATA, MipiCam_Link4),                           0                         },
  { SIZE_OF_FIELD(SETUP_DATA, MipiCam_Link5),                          OFFSET_OF(SETUP_DATA, MipiCam_Link5),                           0                         },
  { SIZE_OF_FIELD(SETUP_DATA, PchI2cSensorDevicePort[SERIAL_IO_I2C2]), OFFSET_OF (SETUP_DATA, PchI2cSensorDevicePort[SERIAL_IO_I2C2]), SERIAL_IO_I2C_UCMC        },
  { SIZE_OF_FIELD(SETUP_DATA, TcssUcmDevice),                          OFFSET_OF (SETUP_DATA, TcssUcmDevice),                          2                         },
  { SIZE_OF_FIELD(SETUP_DATA, DiscreteTbtPlatformConfigurationSupport),OFFSET_OF (SETUP_DATA, DiscreteTbtPlatformConfigurationSupport),1                         },
  { SIZE_OF_FIELD(SETUP_DATA, EcPeciModeUnsupport),                    OFFSET_OF (SETUP_DATA, EcPeciModeUnsupport),                    1                         }
};

CONFIG_PATCH_TABLE  mAlderlakeSDdr4RvpPchSetupConfigPatchTable[] = {
//{---------------Type-------Size----------------------------------Offset----------------------------------------------------Data},
  { SIZE_OF_FIELD(PCH_SETUP, PxDevSlp[0]),                         OFFSET_OF(PCH_SETUP, PxDevSlp[0]),                        0x1 },
  { SIZE_OF_FIELD(PCH_SETUP, PxDevSlp[1]),                         OFFSET_OF(PCH_SETUP, PxDevSlp[1]),                        0x1 },
  { SIZE_OF_FIELD(PCH_SETUP, PchSerialIoSpi[SERIAL_IO_SPI0]),      OFFSET_OF(PCH_SETUP, PchSerialIoSpi[SERIAL_IO_SPI0]),     0x0 },
  { SIZE_OF_FIELD(PCH_SETUP, PchSerialIoSpi[SERIAL_IO_SPI1]),      OFFSET_OF(PCH_SETUP, PchSerialIoSpi[SERIAL_IO_SPI1]),     0x1 },
  { SIZE_OF_FIELD(PCH_SETUP, PchSerialIoSpi[SERIAL_IO_SPI2]),      OFFSET_OF(PCH_SETUP, PchSerialIoSpi[SERIAL_IO_SPI2]),     0x0 },
  { SIZE_OF_FIELD(PCH_SETUP, PchSerialIoUart[SERIAL_IO_UART0]),    OFFSET_OF(PCH_SETUP, PchSerialIoUart[SERIAL_IO_UART0]),   0x0 },
  { SIZE_OF_FIELD(PCH_SETUP, PchSerialIoUart[SERIAL_IO_UART1]),    OFFSET_OF(PCH_SETUP, PchSerialIoUart[SERIAL_IO_UART1]),   0x0 },
  { SIZE_OF_FIELD(PCH_SETUP, PchUartHwFlowCtrl[2]),                OFFSET_OF(PCH_SETUP, PchUartHwFlowCtrl[2]),               0x0 },
  { SIZE_OF_FIELD(PCH_SETUP, PchSerialIoI2c[SERIAL_IO_I2C2]),      OFFSET_OF(PCH_SETUP, PchSerialIoI2c[SERIAL_IO_I2C2]),     0x1 },
  { SIZE_OF_FIELD(PCH_SETUP, PchSerialIoI2c[SERIAL_IO_I2C3]),      OFFSET_OF(PCH_SETUP, PchSerialIoI2c[SERIAL_IO_I2C3]),     0x1 },
  { SIZE_OF_FIELD(PCH_SETUP, PchSerialIoI2c[SERIAL_IO_I2C4]),      OFFSET_OF(PCH_SETUP, PchSerialIoI2c[SERIAL_IO_I2C4]),     0x1 },
  { SIZE_OF_FIELD(PCH_SETUP, PchSerialIoI2c[SERIAL_IO_I2C5]),      OFFSET_OF(PCH_SETUP, PchSerialIoI2c[SERIAL_IO_I2C5]),     0x1 },
  { SIZE_OF_FIELD(PCH_SETUP, PchHdAudioI2sCodecSelect),            OFFSET_OF(PCH_SETUP, PchHdAudioI2sCodecSelect),           0x0 }
};

CONFIG_PATCH_TABLE  mAlderlakeSDdr4RvpSaSetupConfigPatchTable[] = {
//{---------------Type------Size-----------------------------------Offset----------------------------------------------------Data},
  { SIZE_OF_FIELD(SA_SETUP, TcssXhciEn),                           OFFSET_OF(SA_SETUP, TcssXhciEn),                          0x0 }
};

CONFIG_PATCH_TABLE  mAlderlakeSCpuSetupConfigPatchTable[] = {
//{---------------Type------Size-----------------------------------Offset----------------------------------------------------Data},
  { SIZE_OF_FIELD(CPU_SETUP, Psi1Threshold[0]),                    OFFSET_OF(CPU_SETUP, Psi1Threshold[0]),                   0x0 },
  { SIZE_OF_FIELD(CPU_SETUP, Psi2Threshold[0]),                    OFFSET_OF(CPU_SETUP, Psi2Threshold[0]),                   0x0 },
  { SIZE_OF_FIELD(CPU_SETUP, Psi3Threshold[0]),                    OFFSET_OF(CPU_SETUP, Psi3Threshold[0]),                   0x0 },
  { SIZE_OF_FIELD(CPU_SETUP, Psi1Threshold[1]),                    OFFSET_OF(CPU_SETUP, Psi1Threshold[1]),                   0x0 },
  { SIZE_OF_FIELD(CPU_SETUP, Psi2Threshold[1]),                    OFFSET_OF(CPU_SETUP, Psi2Threshold[1]),                   0x0 },
  { SIZE_OF_FIELD(CPU_SETUP, Psi3Threshold[1]),                    OFFSET_OF(CPU_SETUP, Psi3Threshold[1]),                   0x0 }
};

CONFIG_PATCH_STRUCTURE mAlderlakeSDdr4RvpRealSiliconConfigPatchStruct[] = {
  { L"CpuSetup", &gCpuSetupVariableGuid, sizeof(CPU_SETUP),  NULL,        0 },
};

CONFIG_PATCH_STRUCTURE mAlderlakeSDdr4RvpConfigPatchStruct[] = {
  { L"Setup",    &gSetupVariableGuid,    sizeof(SETUP_DATA), mAlderlakeSDdr4RvpSetupConfigPatchTable,    SIZE_OF_TABLE(mAlderlakeSDdr4RvpSetupConfigPatchTable,    CONFIG_PATCH_TABLE) },
  { L"SaSetup",  &gSaSetupVariableGuid,  sizeof(SA_SETUP),   mAlderlakeSDdr4RvpSaSetupConfigPatchTable,  SIZE_OF_TABLE(mAlderlakeSDdr4RvpSaSetupConfigPatchTable,  CONFIG_PATCH_TABLE) },
  { L"CpuSetup", &gCpuSetupVariableGuid, sizeof(CPU_SETUP),  NULL,                                       0,                                                                            },
  { L"PchSetup", &gPchSetupVariableGuid, sizeof(PCH_SETUP),  mAlderlakeSDdr4RvpPchSetupConfigPatchTable, SIZE_OF_TABLE(mAlderlakeSDdr4RvpPchSetupConfigPatchTable, CONFIG_PATCH_TABLE) },
  { L"MeSetup",  &gMeSetupVariableGuid,  sizeof(ME_SETUP),   NULL,                                       0                                                                             }
};

CONFIG_PATCH_TABLE  mAlderlakeSSodimmRvpPchSetupConfigPatchTable[] = {
//{---------------Type-------Size----------------------------------Offset----------------------------------------------------Data},
  { SIZE_OF_FIELD(PCH_SETUP, PxDevSlp[0]),                         OFFSET_OF(PCH_SETUP, PxDevSlp[0]),                        0x1 },
  { SIZE_OF_FIELD(PCH_SETUP, PxDevSlp[1]),                         OFFSET_OF(PCH_SETUP, PxDevSlp[1]),                        0x1 },
  { SIZE_OF_FIELD(PCH_SETUP, PchSerialIoSpi[SERIAL_IO_SPI0]),      OFFSET_OF(PCH_SETUP, PchSerialIoSpi[SERIAL_IO_SPI0]),     0x0 },
  { SIZE_OF_FIELD(PCH_SETUP, PchSerialIoSpi[SERIAL_IO_SPI1]),      OFFSET_OF(PCH_SETUP, PchSerialIoSpi[SERIAL_IO_SPI1]),     0x1 },
  { SIZE_OF_FIELD(PCH_SETUP, PchSerialIoSpi[SERIAL_IO_SPI2]),      OFFSET_OF(PCH_SETUP, PchSerialIoSpi[SERIAL_IO_SPI2]),     0x0 },
  { SIZE_OF_FIELD(PCH_SETUP, PchSerialIoUart[SERIAL_IO_UART0]),    OFFSET_OF(PCH_SETUP, PchSerialIoUart[SERIAL_IO_UART0]),   0x0 },
  { SIZE_OF_FIELD(PCH_SETUP, PchSerialIoUart[SERIAL_IO_UART1]),    OFFSET_OF(PCH_SETUP, PchSerialIoUart[SERIAL_IO_UART1]),   0x0 },
  { SIZE_OF_FIELD(PCH_SETUP, PchUartHwFlowCtrl[2]),                OFFSET_OF(PCH_SETUP, PchUartHwFlowCtrl[2]),               0x0 },
  { SIZE_OF_FIELD(PCH_SETUP, PchSerialIoI2c[SERIAL_IO_I2C2]),      OFFSET_OF(PCH_SETUP, PchSerialIoI2c[SERIAL_IO_I2C2]),     0x1 },
  { SIZE_OF_FIELD(PCH_SETUP, PchSerialIoI2c[SERIAL_IO_I2C3]),      OFFSET_OF(PCH_SETUP, PchSerialIoI2c[SERIAL_IO_I2C3]),     0x1 },
  { SIZE_OF_FIELD(PCH_SETUP, PchSerialIoI2c[SERIAL_IO_I2C4]),      OFFSET_OF(PCH_SETUP, PchSerialIoI2c[SERIAL_IO_I2C4]),     0x1 },
  { SIZE_OF_FIELD(PCH_SETUP, PchSerialIoI2c[SERIAL_IO_I2C5]),      OFFSET_OF(PCH_SETUP, PchSerialIoI2c[SERIAL_IO_I2C5]),     0x1 },
  { SIZE_OF_FIELD(PCH_SETUP, PchHdAudioI2sCodecSelect),            OFFSET_OF(PCH_SETUP, PchHdAudioI2sCodecSelect),           0x0 },
  { SIZE_OF_FIELD(PCH_SETUP, PchLan),                              OFFSET_OF(PCH_SETUP, PchLan),                             0x0 },
  { SIZE_OF_FIELD(PCH_SETUP, FoxvilleLanSupport),                  OFFSET_OF(PCH_SETUP, FoxvilleLanSupport),                 0x1 },
  { SIZE_OF_FIELD(PCH_SETUP, FoxvilleWakeOnLan),                   OFFSET_OF(PCH_SETUP, FoxvilleWakeOnLan),                  0x1 }
};

CONFIG_PATCH_STRUCTURE mAlderlakeSSodimmRvpConfigPatchStruct[] = {
  { L"Setup",    &gSetupVariableGuid,    sizeof(SETUP_DATA), mAlderlakeSDdr4RvpSetupConfigPatchTable,      SIZE_OF_TABLE(mAlderlakeSDdr4RvpSetupConfigPatchTable,      CONFIG_PATCH_TABLE) },
  { L"SaSetup",  &gSaSetupVariableGuid,  sizeof(SA_SETUP),   mAlderlakeSDdr4RvpSaSetupConfigPatchTable,    SIZE_OF_TABLE(mAlderlakeSDdr4RvpSaSetupConfigPatchTable,    CONFIG_PATCH_TABLE) },
  { L"CpuSetup", &gCpuSetupVariableGuid, sizeof(CPU_SETUP),  NULL,                                         0,                                                                              },
  { L"PchSetup", &gPchSetupVariableGuid, sizeof(PCH_SETUP),  mAlderlakeSSodimmRvpPchSetupConfigPatchTable, SIZE_OF_TABLE(mAlderlakeSSodimmRvpPchSetupConfigPatchTable, CONFIG_PATCH_TABLE) },
  { L"MeSetup",  &gMeSetupVariableGuid,  sizeof(ME_SETUP),   NULL,                                         0                                                                               }
};

CONFIG_PATCH_TABLE  mAlderlakeSAdpSDdr5UDimm1DAepConfigPatchTable[] = {
//{---------------Type------Size-----------------------------------Offset----------------------------------------------------Data},
  { SIZE_OF_FIELD(SETUP_DATA, TbtLegacyModeSupport),                   OFFSET_OF(SETUP_DATA, TbtLegacyModeSupport),                    0                         },
  { SIZE_OF_FIELD(SETUP_DATA, AuxOriOverrideSupport),                  OFFSET_OF(SETUP_DATA, AuxOriOverrideSupport),                   0                         },
  { SIZE_OF_FIELD(SETUP_DATA, HebcValueSupport),                       OFFSET_OF(SETUP_DATA, HebcValueSupport),                        1                         },
  { SIZE_OF_FIELD(SETUP_DATA, PchI2cSensorDevicePort[SERIAL_IO_I2C0]), OFFSET_OF(SETUP_DATA, PchI2cSensorDevicePort[SERIAL_IO_I2C0]),  SERIAL_IO_I2C_TOUCHPAD    },
  { SIZE_OF_FIELD(SETUP_DATA, PchI2cSensorDevicePort[SERIAL_IO_I2C1]), OFFSET_OF(SETUP_DATA, PchI2cSensorDevicePort[SERIAL_IO_I2C1]),  SERIAL_IO_I2C_TOUCHPANEL  },
  { SIZE_OF_FIELD(SETUP_DATA, PchSpiSensorDevicePort[SERIAL_IO_SPI1]), OFFSET_OF(SETUP_DATA, PchSpiSensorDevicePort[SERIAL_IO_SPI1]),  SERIAL_IO_SPI_FINGERPRINT },
  { SIZE_OF_FIELD(SETUP_DATA, NativeAspmEnable),                       OFFSET_OF(SETUP_DATA, NativeAspmEnable),                        0                         },
  { SIZE_OF_FIELD(SETUP_DATA, PepGfx),                                 OFFSET_OF(SETUP_DATA, PepGfx),                                  1                         },
  { SIZE_OF_FIELD(SETUP_DATA, PepPeg0),                                OFFSET_OF(SETUP_DATA, PepPeg0),                                 1                         },
  { SIZE_OF_FIELD(SETUP_DATA, PepSataEnumeration),                     OFFSET_OF(SETUP_DATA, PepSataEnumeration),                      1                         },
  { SIZE_OF_FIELD(SETUP_DATA, PepI2c0),                                OFFSET_OF(SETUP_DATA, PepI2c0),                                 1                         },
  { SIZE_OF_FIELD(SETUP_DATA, PepI2c1),                                OFFSET_OF(SETUP_DATA, PepI2c1),                                 1                         },
  { SIZE_OF_FIELD(SETUP_DATA, PepI2c2),                                OFFSET_OF(SETUP_DATA, PepI2c2),                                 1                         },
  { SIZE_OF_FIELD(SETUP_DATA, PepI2c3),                                OFFSET_OF(SETUP_DATA, PepI2c3),                                 1                         },
  { SIZE_OF_FIELD(SETUP_DATA, PepI2c4),                                OFFSET_OF(SETUP_DATA, PepI2c4),                                 1                         },
  { SIZE_OF_FIELD(SETUP_DATA, PepI2c5),                                OFFSET_OF(SETUP_DATA, PepI2c5),                                 1                         },
  { SIZE_OF_FIELD(SETUP_DATA, PepSpi),                                 OFFSET_OF(SETUP_DATA, PepSpi),                                  1                         },
  { SIZE_OF_FIELD(SETUP_DATA, PepXhci),                                OFFSET_OF(SETUP_DATA, PepXhci),                                 1                         },
  { SIZE_OF_FIELD(SETUP_DATA, PepAudio),                               OFFSET_OF(SETUP_DATA, PepAudio),                                1                         },
  { SIZE_OF_FIELD(SETUP_DATA, SysFwUpdateSkipPowerCheck),              OFFSET_OF(SETUP_DATA, SysFwUpdateSkipPowerCheck),               1                         },
  { SIZE_OF_FIELD(SETUP_DATA, TbtSetupFormSupport),                    OFFSET_OF(SETUP_DATA, TbtSetupFormSupport),                     1                         },
  { SIZE_OF_FIELD(SETUP_DATA, PepCsme),                                OFFSET_OF(SETUP_DATA, PepCsme),                                 1                         },
  { SIZE_OF_FIELD(SETUP_DATA, PepGbe),                                 OFFSET_OF(SETUP_DATA, PepGbe),                                  1                         },
  { SIZE_OF_FIELD(SETUP_DATA, PepTcss),                                OFFSET_OF(SETUP_DATA, PepTcss),                                 1                         },
  { SIZE_OF_FIELD(SETUP_DATA, MipiCam_ControlLogic0),                  OFFSET_OF(SETUP_DATA, MipiCam_ControlLogic0),                   0                         },
  { SIZE_OF_FIELD(SETUP_DATA, MipiCam_ControlLogic1),                  OFFSET_OF(SETUP_DATA, MipiCam_ControlLogic1),                   0                         },
  { SIZE_OF_FIELD(SETUP_DATA, MipiCam_ControlLogic2),                  OFFSET_OF(SETUP_DATA, MipiCam_ControlLogic2),                   0                         },
  { SIZE_OF_FIELD(SETUP_DATA, MipiCam_ControlLogic3),                  OFFSET_OF(SETUP_DATA, MipiCam_ControlLogic3),                   0                         },
  { SIZE_OF_FIELD(SETUP_DATA, MipiCam_ControlLogic4),                  OFFSET_OF(SETUP_DATA, MipiCam_ControlLogic3),                   0                         },
  { SIZE_OF_FIELD(SETUP_DATA, MipiCam_ControlLogic5),                  OFFSET_OF(SETUP_DATA, MipiCam_ControlLogic3),                   0                         },
  { SIZE_OF_FIELD(SETUP_DATA, MipiCam_Link0),                          OFFSET_OF(SETUP_DATA, MipiCam_Link0),                           0                         },
  { SIZE_OF_FIELD(SETUP_DATA, MipiCam_Link1),                          OFFSET_OF(SETUP_DATA, MipiCam_Link1),                           0                         },
  { SIZE_OF_FIELD(SETUP_DATA, MipiCam_Link2),                          OFFSET_OF(SETUP_DATA, MipiCam_Link2),                           0                         },
  { SIZE_OF_FIELD(SETUP_DATA, MipiCam_Link3),                          OFFSET_OF(SETUP_DATA, MipiCam_Link3),                           0                         },
  { SIZE_OF_FIELD(SETUP_DATA, MipiCam_Link4),                          OFFSET_OF(SETUP_DATA, MipiCam_Link4),                           0                         },
  { SIZE_OF_FIELD(SETUP_DATA, MipiCam_Link5),                          OFFSET_OF(SETUP_DATA, MipiCam_Link5),                           0                         },
  { SIZE_OF_FIELD(SETUP_DATA, AdlSAepBoard),                           OFFSET_OF(SETUP_DATA, AdlSAepBoard),                            1                         },
  { SIZE_OF_FIELD(SETUP_DATA, DiscreteTbtPlatformConfigurationSupport),OFFSET_OF(SETUP_DATA, DiscreteTbtPlatformConfigurationSupport), 1                         }
};

CONFIG_PATCH_STRUCTURE mAlderlakeSAdpSDdr5UDimm1DAepConfigPatchStruct[] = {
  { L"Setup",    &gSetupVariableGuid,    sizeof(SETUP_DATA), mAlderlakeSAdpSDdr5UDimm1DAepConfigPatchTable, SIZE_OF_TABLE(mAlderlakeSAdpSDdr5UDimm1DAepConfigPatchTable, CONFIG_PATCH_TABLE) },
  { L"SaSetup",  &gSaSetupVariableGuid,  sizeof(SA_SETUP),   mAlderlakeSDdr4RvpSaSetupConfigPatchTable,  SIZE_OF_TABLE(mAlderlakeSDdr4RvpSaSetupConfigPatchTable,  CONFIG_PATCH_TABLE) },
  { L"CpuSetup", &gCpuSetupVariableGuid, sizeof(CPU_SETUP),  NULL,                                       0,                                                                            },
  { L"PchSetup", &gPchSetupVariableGuid, sizeof(PCH_SETUP),  mAlderlakeSDdr4RvpPchSetupConfigPatchTable, SIZE_OF_TABLE(mAlderlakeSDdr4RvpPchSetupConfigPatchTable, CONFIG_PATCH_TABLE) },
  { L"MeSetup",  &gMeSetupVariableGuid,  sizeof(ME_SETUP),   NULL,                                       0                                                                             }
};

CONFIG_PATCH_TABLE  mAlderlakeSbgaDdr5SODimmErbConfigPatchTable[] = {
//{---------------Type------Size-----------------------------------Offset----------------------------------------------------Data},
  { SIZE_OF_FIELD(SETUP_DATA, TbtLegacyModeSupport),                   OFFSET_OF(SETUP_DATA, TbtLegacyModeSupport),                    0                         },//no used
  { SIZE_OF_FIELD(SETUP_DATA, AuxOriOverrideSupport),                  OFFSET_OF(SETUP_DATA, AuxOriOverrideSupport),                   0                         },
  { SIZE_OF_FIELD(SETUP_DATA, HebcValueSupport),                       OFFSET_OF(SETUP_DATA, HebcValueSupport),                        1                         },
  { SIZE_OF_FIELD(SETUP_DATA, PchI2cSensorDevicePort[SERIAL_IO_I2C0]), OFFSET_OF(SETUP_DATA, PchI2cSensorDevicePort[SERIAL_IO_I2C0]),  SERIAL_IO_I2C_TOUCHPAD    },
  { SIZE_OF_FIELD(SETUP_DATA, PchI2cSensorDevicePort[SERIAL_IO_I2C1]), OFFSET_OF(SETUP_DATA, PchI2cSensorDevicePort[SERIAL_IO_I2C1]),  SERIAL_IO_I2C_TOUCHPANEL  },
  { SIZE_OF_FIELD(SETUP_DATA, PchSpiSensorDevicePort[SERIAL_IO_SPI1]), OFFSET_OF(SETUP_DATA, PchSpiSensorDevicePort[SERIAL_IO_SPI1]),  SERIAL_IO_SPI_FINGERPRINT },
  { SIZE_OF_FIELD(SETUP_DATA, NativeAspmEnable),                       OFFSET_OF(SETUP_DATA, NativeAspmEnable),                        0                         },
  { SIZE_OF_FIELD(SETUP_DATA, PepGfx),                                 OFFSET_OF(SETUP_DATA, PepGfx),                                  1                         },
  { SIZE_OF_FIELD(SETUP_DATA, PepPeg0),                                OFFSET_OF(SETUP_DATA, PepPeg0),                                 1                         },
  { SIZE_OF_FIELD(SETUP_DATA, PepSataEnumeration),                     OFFSET_OF(SETUP_DATA, PepSataEnumeration),                      1                         },
  { SIZE_OF_FIELD(SETUP_DATA, PepI2c0),                                OFFSET_OF(SETUP_DATA, PepI2c0),                                 1                         },
  { SIZE_OF_FIELD(SETUP_DATA, PepI2c1),                                OFFSET_OF(SETUP_DATA, PepI2c1),                                 1                         },
  { SIZE_OF_FIELD(SETUP_DATA, PepI2c2),                                OFFSET_OF(SETUP_DATA, PepI2c2),                                 1                         },
  { SIZE_OF_FIELD(SETUP_DATA, PepI2c3),                                OFFSET_OF(SETUP_DATA, PepI2c3),                                 1                         },
  { SIZE_OF_FIELD(SETUP_DATA, PepI2c4),                                OFFSET_OF(SETUP_DATA, PepI2c4),                                 1                         },
  { SIZE_OF_FIELD(SETUP_DATA, PepI2c5),                                OFFSET_OF(SETUP_DATA, PepI2c5),                                 1                         },
  { SIZE_OF_FIELD(SETUP_DATA, PepSpi),                                 OFFSET_OF(SETUP_DATA, PepSpi),                                  1                         },
  { SIZE_OF_FIELD(SETUP_DATA, PepXhci),                                OFFSET_OF(SETUP_DATA, PepXhci),                                 1                         },
  { SIZE_OF_FIELD(SETUP_DATA, PepAudio),                               OFFSET_OF(SETUP_DATA, PepAudio),                                1                         },
  { SIZE_OF_FIELD(SETUP_DATA, SysFwUpdateSkipPowerCheck),              OFFSET_OF(SETUP_DATA, SysFwUpdateSkipPowerCheck),               1                         },
  { SIZE_OF_FIELD(SETUP_DATA, TbtSetupFormSupport),                    OFFSET_OF(SETUP_DATA, TbtSetupFormSupport),                     1                         },//no used
  { SIZE_OF_FIELD(SETUP_DATA, PepCsme),                                OFFSET_OF(SETUP_DATA, PepCsme),                                 1                         },
  { SIZE_OF_FIELD(SETUP_DATA, PepGbe),                                 OFFSET_OF(SETUP_DATA, PepGbe),                                  1                         },
  { SIZE_OF_FIELD(SETUP_DATA, PepTcss),                                OFFSET_OF(SETUP_DATA, PepTcss),                                 1                         },
  { SIZE_OF_FIELD(SETUP_DATA, MipiCam_ControlLogic0),                  OFFSET_OF(SETUP_DATA, MipiCam_ControlLogic0),                   0                         },
  { SIZE_OF_FIELD(SETUP_DATA, MipiCam_ControlLogic1),                  OFFSET_OF(SETUP_DATA, MipiCam_ControlLogic1),                   0                         },
  { SIZE_OF_FIELD(SETUP_DATA, MipiCam_ControlLogic2),                  OFFSET_OF(SETUP_DATA, MipiCam_ControlLogic2),                   0                         },
  { SIZE_OF_FIELD(SETUP_DATA, MipiCam_ControlLogic3),                  OFFSET_OF(SETUP_DATA, MipiCam_ControlLogic3),                   0                         },
  { SIZE_OF_FIELD(SETUP_DATA, MipiCam_ControlLogic4),                  OFFSET_OF(SETUP_DATA, MipiCam_ControlLogic3),                   0                         },
  { SIZE_OF_FIELD(SETUP_DATA, MipiCam_ControlLogic5),                  OFFSET_OF(SETUP_DATA, MipiCam_ControlLogic3),                   0                         },
  { SIZE_OF_FIELD(SETUP_DATA, MipiCam_Link0),                          OFFSET_OF(SETUP_DATA, MipiCam_Link0),                           0                         },
  { SIZE_OF_FIELD(SETUP_DATA, MipiCam_Link1),                          OFFSET_OF(SETUP_DATA, MipiCam_Link1),                           0                         },
  { SIZE_OF_FIELD(SETUP_DATA, MipiCam_Link2),                          OFFSET_OF(SETUP_DATA, MipiCam_Link2),                           0                         },
  { SIZE_OF_FIELD(SETUP_DATA, MipiCam_Link3),                          OFFSET_OF(SETUP_DATA, MipiCam_Link3),                           0                         },
  { SIZE_OF_FIELD(SETUP_DATA, MipiCam_Link4),                          OFFSET_OF(SETUP_DATA, MipiCam_Link4),                           0                         },
  { SIZE_OF_FIELD(SETUP_DATA, MipiCam_Link5),                          OFFSET_OF(SETUP_DATA, MipiCam_Link5),                           0                         },
  { SIZE_OF_FIELD(SETUP_DATA, DiscreteTbtPlatformConfigurationSupport),OFFSET_OF(SETUP_DATA, DiscreteTbtPlatformConfigurationSupport), 1                         },// Platfrom dTBT code support capablilities
  { SIZE_OF_FIELD(SETUP_DATA, DiscreteTbtSupport),                     OFFSET_OF(SETUP_DATA, DiscreteTbtSupport),                      1                         },// dTBT feature switch if Platfrom dTBT code support it
  { SIZE_OF_FIELD(SETUP_DATA, DTbtRtd3ClkReq),                         OFFSET_OF(SETUP_DATA, DTbtRtd3ClkReq),                          1                         },// Enabled it after TBT v31
  { SIZE_OF_FIELD(SETUP_DATA, DTbtController[0]),                      OFFSET_OF(SETUP_DATA, DTbtController[0]),                       1                         },// SBGA has 2 onboard MR Controllers (POR)
  { SIZE_OF_FIELD(SETUP_DATA, DTbtController[1]),                      OFFSET_OF(SETUP_DATA, DTbtController[1]),                       1                         },// SBGA has 2 onboard MR Controllers (POR)
  { SIZE_OF_FIELD(SETUP_DATA, DTbtRtd3),                               OFFSET_OF(SETUP_DATA, DTbtRtd3),                                1                         },// Set dTBT RTD3 enabled by default for SBGA
  { SIZE_OF_FIELD(SETUP_DATA, ITbtRootPort[0]),                        OFFSET_OF(SETUP_DATA, ITbtRootPort[0]),                         0                         },// Disable ITbt for mmio reservation
  { SIZE_OF_FIELD(SETUP_DATA, ITbtRootPort[1]),                        OFFSET_OF(SETUP_DATA, ITbtRootPort[1]),                         0                         },// Disable ITbt for mmio reservation
  { SIZE_OF_FIELD(SETUP_DATA, ITbtRootPort[2]),                        OFFSET_OF(SETUP_DATA, ITbtRootPort[2]),                         0                         },// Disable ITbt for mmio reservation
  { SIZE_OF_FIELD(SETUP_DATA, ITbtRootPort[3]),                        OFFSET_OF(SETUP_DATA, ITbtRootPort[3]),                         0                         } // Disable ITbt for mmio reservation
};

CONFIG_PATCH_TABLE  mAlderlakeSbgaDdr4SODimmErbConfigPatchTable[] = {
//{---------------Type------Size-----------------------------------Offset----------------------------------------------------Data},
  { SIZE_OF_FIELD(SETUP_DATA, TbtLegacyModeSupport),                   OFFSET_OF(SETUP_DATA, TbtLegacyModeSupport),                    0                         },//no used
  { SIZE_OF_FIELD(SETUP_DATA, AuxOriOverrideSupport),                  OFFSET_OF(SETUP_DATA, AuxOriOverrideSupport),                   0                         },
  { SIZE_OF_FIELD(SETUP_DATA, HebcValueSupport),                       OFFSET_OF(SETUP_DATA, HebcValueSupport),                        1                         },
  { SIZE_OF_FIELD(SETUP_DATA, PchI2cSensorDevicePort[SERIAL_IO_I2C0]), OFFSET_OF(SETUP_DATA, PchI2cSensorDevicePort[SERIAL_IO_I2C0]),  SERIAL_IO_I2C_TOUCHPAD    },
  { SIZE_OF_FIELD(SETUP_DATA, PchI2cSensorDevicePort[SERIAL_IO_I2C1]), OFFSET_OF(SETUP_DATA, PchI2cSensorDevicePort[SERIAL_IO_I2C1]),  SERIAL_IO_I2C_TOUCHPANEL  },
  { SIZE_OF_FIELD(SETUP_DATA, PchSpiSensorDevicePort[SERIAL_IO_SPI1]), OFFSET_OF(SETUP_DATA, PchSpiSensorDevicePort[SERIAL_IO_SPI1]),  SERIAL_IO_SPI_FINGERPRINT },
  { SIZE_OF_FIELD(SETUP_DATA, NativeAspmEnable),                       OFFSET_OF(SETUP_DATA, NativeAspmEnable),                        0                         },
  { SIZE_OF_FIELD(SETUP_DATA, PepGfx),                                 OFFSET_OF(SETUP_DATA, PepGfx),                                  1                         },
  { SIZE_OF_FIELD(SETUP_DATA, PepPeg0),                                OFFSET_OF(SETUP_DATA, PepPeg0),                                 1                         },
  { SIZE_OF_FIELD(SETUP_DATA, PepSataEnumeration),                     OFFSET_OF(SETUP_DATA, PepSataEnumeration),                      1                         },
  { SIZE_OF_FIELD(SETUP_DATA, PepI2c0),                                OFFSET_OF(SETUP_DATA, PepI2c0),                                 1                         },
  { SIZE_OF_FIELD(SETUP_DATA, PepI2c1),                                OFFSET_OF(SETUP_DATA, PepI2c1),                                 1                         },
  { SIZE_OF_FIELD(SETUP_DATA, PepI2c2),                                OFFSET_OF(SETUP_DATA, PepI2c2),                                 1                         },
  { SIZE_OF_FIELD(SETUP_DATA, PepI2c3),                                OFFSET_OF(SETUP_DATA, PepI2c3),                                 1                         },
  { SIZE_OF_FIELD(SETUP_DATA, PepI2c4),                                OFFSET_OF(SETUP_DATA, PepI2c4),                                 1                         },
  { SIZE_OF_FIELD(SETUP_DATA, PepI2c5),                                OFFSET_OF(SETUP_DATA, PepI2c5),                                 1                         },
  { SIZE_OF_FIELD(SETUP_DATA, PepSpi),                                 OFFSET_OF(SETUP_DATA, PepSpi),                                  1                         },
  { SIZE_OF_FIELD(SETUP_DATA, PepXhci),                                OFFSET_OF(SETUP_DATA, PepXhci),                                 1                         },
  { SIZE_OF_FIELD(SETUP_DATA, PepAudio),                               OFFSET_OF(SETUP_DATA, PepAudio),                                1                         },
  { SIZE_OF_FIELD(SETUP_DATA, SysFwUpdateSkipPowerCheck),              OFFSET_OF(SETUP_DATA, SysFwUpdateSkipPowerCheck),               1                         },
  { SIZE_OF_FIELD(SETUP_DATA, TbtSetupFormSupport),                    OFFSET_OF(SETUP_DATA, TbtSetupFormSupport),                     1                         },//no used
  { SIZE_OF_FIELD(SETUP_DATA, PepCsme),                                OFFSET_OF(SETUP_DATA, PepCsme),                                 1                         },
  { SIZE_OF_FIELD(SETUP_DATA, PepGbe),                                 OFFSET_OF(SETUP_DATA, PepGbe),                                  1                         },
  { SIZE_OF_FIELD(SETUP_DATA, PepTcss),                                OFFSET_OF(SETUP_DATA, PepTcss),                                 1                         },
  { SIZE_OF_FIELD(SETUP_DATA, MipiCam_ControlLogic0),                  OFFSET_OF(SETUP_DATA, MipiCam_ControlLogic0),                   0                         },
  { SIZE_OF_FIELD(SETUP_DATA, MipiCam_ControlLogic1),                  OFFSET_OF(SETUP_DATA, MipiCam_ControlLogic1),                   0                         },
  { SIZE_OF_FIELD(SETUP_DATA, MipiCam_ControlLogic2),                  OFFSET_OF(SETUP_DATA, MipiCam_ControlLogic2),                   0                         },
  { SIZE_OF_FIELD(SETUP_DATA, MipiCam_ControlLogic3),                  OFFSET_OF(SETUP_DATA, MipiCam_ControlLogic3),                   0                         },
  { SIZE_OF_FIELD(SETUP_DATA, MipiCam_ControlLogic4),                  OFFSET_OF(SETUP_DATA, MipiCam_ControlLogic3),                   0                         },
  { SIZE_OF_FIELD(SETUP_DATA, MipiCam_ControlLogic5),                  OFFSET_OF(SETUP_DATA, MipiCam_ControlLogic3),                   0                         },
  { SIZE_OF_FIELD(SETUP_DATA, MipiCam_Link0),                          OFFSET_OF(SETUP_DATA, MipiCam_Link0),                           0                         },
  { SIZE_OF_FIELD(SETUP_DATA, MipiCam_Link1),                          OFFSET_OF(SETUP_DATA, MipiCam_Link1),                           0                         },
  { SIZE_OF_FIELD(SETUP_DATA, MipiCam_Link2),                          OFFSET_OF(SETUP_DATA, MipiCam_Link2),                           0                         },
  { SIZE_OF_FIELD(SETUP_DATA, MipiCam_Link3),                          OFFSET_OF(SETUP_DATA, MipiCam_Link3),                           0                         },
  { SIZE_OF_FIELD(SETUP_DATA, MipiCam_Link4),                          OFFSET_OF(SETUP_DATA, MipiCam_Link4),                           0                         },
  { SIZE_OF_FIELD(SETUP_DATA, MipiCam_Link5),                          OFFSET_OF(SETUP_DATA, MipiCam_Link5),                           0                         },
  { SIZE_OF_FIELD(SETUP_DATA, DiscreteTbtPlatformConfigurationSupport),OFFSET_OF(SETUP_DATA, DiscreteTbtPlatformConfigurationSupport), 1                         }// Platfrom dTBT code support capablilities
};

CONFIG_PATCH_TABLE  mAlderlakeSbgaDdr4SODimmSaSetupConfigPatchTable[] = {
//{---------------Type------Size-----------------------------------Offset----------------------------------------------------Data},
  { SIZE_OF_FIELD(SA_SETUP, TcssXhciEn),                           OFFSET_OF(SA_SETUP, TcssXhciEn),                          0x0 }
};


CONFIG_PATCH_TABLE  mAlderlakeSbgaDdr5SODimmSaSetupConfigPatchTable[] = {
//{---------------Type------Size-----------------------------------Offset----------------------------------------------------Data},
  { SIZE_OF_FIELD(SA_SETUP, TcssXhciEn),                           OFFSET_OF(SA_SETUP, TcssXhciEn),                          0x0 }
};

CONFIG_PATCH_TABLE  mAlderlakeSbgaDdr5SODimmCpuSetupConfigPatchTable[] = {
  { SIZE_OF_FIELD(CPU_SETUP, Irms),                                OFFSET_OF(CPU_SETUP, Irms),                               0x1 },
  { SIZE_OF_FIELD(CPU_SETUP, EnergyEfficientTurbo),                OFFSET_OF(CPU_SETUP, EnergyEfficientTurbo),               0x1 }
};

CONFIG_PATCH_TABLE  mAlderlakeSbgaDdr5SodimmRvpPchSetupConfigPatchTable[] = {
//{---------------Type-------Size----------------------------------Offset----------------------------------------------------Data},
  { SIZE_OF_FIELD(PCH_SETUP, PxDevSlp[0]),                         OFFSET_OF(PCH_SETUP, PxDevSlp[0]),                        0x1 },
  { SIZE_OF_FIELD(PCH_SETUP, PxDevSlp[1]),                         OFFSET_OF(PCH_SETUP, PxDevSlp[1]),                        0x1 },
  { SIZE_OF_FIELD(PCH_SETUP, PchSerialIoSpi[SERIAL_IO_SPI0]),      OFFSET_OF(PCH_SETUP, PchSerialIoSpi[SERIAL_IO_SPI0]),     0x0 },
  { SIZE_OF_FIELD(PCH_SETUP, PchSerialIoSpi[SERIAL_IO_SPI1]),      OFFSET_OF(PCH_SETUP, PchSerialIoSpi[SERIAL_IO_SPI1]),     0x1 },
  { SIZE_OF_FIELD(PCH_SETUP, PchSerialIoSpi[SERIAL_IO_SPI2]),      OFFSET_OF(PCH_SETUP, PchSerialIoSpi[SERIAL_IO_SPI2]),     0x0 },
  { SIZE_OF_FIELD(PCH_SETUP, PchSerialIoUart[SERIAL_IO_UART0]),    OFFSET_OF(PCH_SETUP, PchSerialIoUart[SERIAL_IO_UART0]),   0x0 },
  { SIZE_OF_FIELD(PCH_SETUP, PchSerialIoUart[SERIAL_IO_UART1]),    OFFSET_OF(PCH_SETUP, PchSerialIoUart[SERIAL_IO_UART1]),   0x0 },
  { SIZE_OF_FIELD(PCH_SETUP, PchUartHwFlowCtrl[2]),                OFFSET_OF(PCH_SETUP, PchUartHwFlowCtrl[2]),               0x0 },
  { SIZE_OF_FIELD(PCH_SETUP, PchSerialIoI2c[SERIAL_IO_I2C2]),      OFFSET_OF(PCH_SETUP, PchSerialIoI2c[SERIAL_IO_I2C2]),     0x1 },
  { SIZE_OF_FIELD(PCH_SETUP, PchSerialIoI2c[SERIAL_IO_I2C3]),      OFFSET_OF(PCH_SETUP, PchSerialIoI2c[SERIAL_IO_I2C3]),     0x1 },
  { SIZE_OF_FIELD(PCH_SETUP, PchSerialIoI2c[SERIAL_IO_I2C4]),      OFFSET_OF(PCH_SETUP, PchSerialIoI2c[SERIAL_IO_I2C4]),     0x1 },
  { SIZE_OF_FIELD(PCH_SETUP, PchSerialIoI2c[SERIAL_IO_I2C5]),      OFFSET_OF(PCH_SETUP, PchSerialIoI2c[SERIAL_IO_I2C5]),     0x1 },
  { SIZE_OF_FIELD(PCH_SETUP, PchHdAudioI2sCodecSelect),            OFFSET_OF(PCH_SETUP, PchHdAudioI2sCodecSelect),           0x0 },
  { SIZE_OF_FIELD(PCH_SETUP, PchLan),                              OFFSET_OF(PCH_SETUP, PchLan),                             0x0 },
  { SIZE_OF_FIELD(PCH_SETUP, PcieRootPortHPE[8]),                  OFFSET_OF(PCH_SETUP, PcieRootPortHPE[8]),                 0x1 },//SBGA has 2 onboard MR Controllers (POR), set hotplug to enable
  { SIZE_OF_FIELD(PCH_SETUP, PcieRootPortHPE[24]),                 OFFSET_OF(PCH_SETUP, PcieRootPortHPE[24]),                0x1 },//SBGA has 2 onboard MR Controllers (POR), set hotplug to enable
  { SIZE_OF_FIELD(PCH_SETUP, PsOnEnable),                          OFFSET_OF(PCH_SETUP, PsOnEnable),                         0x0 },//PCH PSON is not a POR feature for SBGA
  { SIZE_OF_FIELD(PCH_SETUP, FoxvilleLanSupport),                  OFFSET_OF(PCH_SETUP, FoxvilleLanSupport),                 0x1 },
  { SIZE_OF_FIELD(PCH_SETUP, FoxvilleWakeOnLan),                   OFFSET_OF(PCH_SETUP, FoxvilleWakeOnLan),                  0x1 }
};

CONFIG_PATCH_TABLE  mAlderlakeSbgaDdr4SodimmRvpPchSetupConfigPatchTable[] = {
//{---------------Type-------Size----------------------------------Offset----------------------------------------------------Data},
  { SIZE_OF_FIELD(PCH_SETUP, PxDevSlp[0]),                         OFFSET_OF(PCH_SETUP, PxDevSlp[0]),                        0x1 },
  { SIZE_OF_FIELD(PCH_SETUP, PxDevSlp[1]),                         OFFSET_OF(PCH_SETUP, PxDevSlp[1]),                        0x1 },
  { SIZE_OF_FIELD(PCH_SETUP, PchSerialIoSpi[SERIAL_IO_SPI0]),      OFFSET_OF(PCH_SETUP, PchSerialIoSpi[SERIAL_IO_SPI0]),     0x0 },
  { SIZE_OF_FIELD(PCH_SETUP, PchSerialIoSpi[SERIAL_IO_SPI1]),      OFFSET_OF(PCH_SETUP, PchSerialIoSpi[SERIAL_IO_SPI1]),     0x1 },
  { SIZE_OF_FIELD(PCH_SETUP, PchSerialIoSpi[SERIAL_IO_SPI2]),      OFFSET_OF(PCH_SETUP, PchSerialIoSpi[SERIAL_IO_SPI2]),     0x0 },
  { SIZE_OF_FIELD(PCH_SETUP, PchSerialIoUart[SERIAL_IO_UART0]),    OFFSET_OF(PCH_SETUP, PchSerialIoUart[SERIAL_IO_UART0]),   0x0 },
  { SIZE_OF_FIELD(PCH_SETUP, PchSerialIoUart[SERIAL_IO_UART1]),    OFFSET_OF(PCH_SETUP, PchSerialIoUart[SERIAL_IO_UART1]),   0x0 },
  { SIZE_OF_FIELD(PCH_SETUP, PchUartHwFlowCtrl[2]),                OFFSET_OF(PCH_SETUP, PchUartHwFlowCtrl[2]),               0x0 },
  { SIZE_OF_FIELD(PCH_SETUP, PchSerialIoI2c[SERIAL_IO_I2C2]),      OFFSET_OF(PCH_SETUP, PchSerialIoI2c[SERIAL_IO_I2C2]),     0x1 },
  { SIZE_OF_FIELD(PCH_SETUP, PchSerialIoI2c[SERIAL_IO_I2C3]),      OFFSET_OF(PCH_SETUP, PchSerialIoI2c[SERIAL_IO_I2C3]),     0x1 },
  { SIZE_OF_FIELD(PCH_SETUP, PchSerialIoI2c[SERIAL_IO_I2C4]),      OFFSET_OF(PCH_SETUP, PchSerialIoI2c[SERIAL_IO_I2C4]),     0x1 },
  { SIZE_OF_FIELD(PCH_SETUP, PchSerialIoI2c[SERIAL_IO_I2C5]),      OFFSET_OF(PCH_SETUP, PchSerialIoI2c[SERIAL_IO_I2C5]),     0x1 },
  { SIZE_OF_FIELD(PCH_SETUP, PchHdAudioI2sCodecSelect),            OFFSET_OF(PCH_SETUP, PchHdAudioI2sCodecSelect),           0x0 },
  { SIZE_OF_FIELD(PCH_SETUP, PchLan),                              OFFSET_OF(PCH_SETUP, PchLan),                             0x0 },
  { SIZE_OF_FIELD(PCH_SETUP, FoxvilleLanSupport),                  OFFSET_OF(PCH_SETUP, FoxvilleLanSupport),                 0x1 },
  { SIZE_OF_FIELD(PCH_SETUP, FoxvilleWakeOnLan),                   OFFSET_OF(PCH_SETUP, FoxvilleWakeOnLan),                  0x1 }
};

CONFIG_PATCH_STRUCTURE mAlderlakeSbgaDdr4SODimmErbConfigPatchStruct[] = {
  { L"Setup",    &gSetupVariableGuid,    sizeof(SETUP_DATA), mAlderlakeSbgaDdr4SODimmErbConfigPatchTable,         SIZE_OF_TABLE(mAlderlakeSbgaDdr4SODimmErbConfigPatchTable,         CONFIG_PATCH_TABLE)    },
  { L"SaSetup",  &gSaSetupVariableGuid,  sizeof(SA_SETUP),   mAlderlakeSbgaDdr4SODimmSaSetupConfigPatchTable,     SIZE_OF_TABLE(mAlderlakeSbgaDdr4SODimmSaSetupConfigPatchTable,     CONFIG_PATCH_TABLE)    },
  { L"CpuSetup", &gCpuSetupVariableGuid, sizeof(CPU_SETUP),  mAlderlakeSbgaDdr5SODimmCpuSetupConfigPatchTable,  SIZE_OF_TABLE(mAlderlakeSbgaDdr5SODimmCpuSetupConfigPatchTable,      CONFIG_PATCH_TABLE)    },
  { L"PchSetup", &gPchSetupVariableGuid, sizeof(PCH_SETUP),  mAlderlakeSbgaDdr4SodimmRvpPchSetupConfigPatchTable, SIZE_OF_TABLE(mAlderlakeSbgaDdr4SodimmRvpPchSetupConfigPatchTable, CONFIG_PATCH_TABLE)    },
  { L"MeSetup",  &gMeSetupVariableGuid,  sizeof(ME_SETUP),   NULL,                                                0                                                                                         }
};

CONFIG_PATCH_STRUCTURE mAlderlakeSbgaDdr5SODimmErbConfigPatchStruct[] = {
  { L"Setup",    &gSetupVariableGuid,    sizeof(SETUP_DATA), mAlderlakeSbgaDdr5SODimmErbConfigPatchTable, SIZE_OF_TABLE(mAlderlakeSbgaDdr5SODimmErbConfigPatchTable, CONFIG_PATCH_TABLE) },
  { L"SaSetup",  &gSaSetupVariableGuid,  sizeof(SA_SETUP),   mAlderlakeSbgaDdr5SODimmSaSetupConfigPatchTable,  SIZE_OF_TABLE(mAlderlakeSbgaDdr5SODimmSaSetupConfigPatchTable,  CONFIG_PATCH_TABLE) },
  { L"CpuSetup", &gCpuSetupVariableGuid, sizeof(CPU_SETUP),  mAlderlakeSbgaDdr5SODimmCpuSetupConfigPatchTable,  SIZE_OF_TABLE(mAlderlakeSbgaDdr5SODimmCpuSetupConfigPatchTable,  CONFIG_PATCH_TABLE) },
  { L"PchSetup", &gPchSetupVariableGuid, sizeof(PCH_SETUP),  mAlderlakeSbgaDdr5SodimmRvpPchSetupConfigPatchTable, SIZE_OF_TABLE(mAlderlakeSbgaDdr5SodimmRvpPchSetupConfigPatchTable, CONFIG_PATCH_TABLE) },
  { L"MeSetup",  &gMeSetupVariableGuid,  sizeof(ME_SETUP),   NULL,                                       0}
};

CONFIG_PATCH_TABLE  mAlderlakeSbgaDdr5SODimmAepConfigPatchTable[] = {
//{---------------Type------Size-----------------------------------Offset----------------------------------------------------Data},
  { SIZE_OF_FIELD(SETUP_DATA, DTbtController[1]),                  OFFSET_OF(SETUP_DATA, DTbtController[1]),                   0 },// SBGA has 1 onboard MR Controllers (POR)
};

CONFIG_PATCH_TABLE  mAlderlakeSbgaDdr5SodimmAepPchSetupConfigPatchTable[] = {
//{---------------Type-------Size----------------------------------Offset----------------------------------------------------Data},
  { SIZE_OF_FIELD(PCH_SETUP, PcieRootPortHPE[24]),                 OFFSET_OF(PCH_SETUP, PcieRootPortHPE[24]),                0x0 },
  { SIZE_OF_FIELD(PCH_SETUP, FoxvilleLanSupport),                  OFFSET_OF(PCH_SETUP, FoxvilleLanSupport),                 0x1 },
  { SIZE_OF_FIELD(PCH_SETUP, FoxvilleWakeOnLan),                   OFFSET_OF(PCH_SETUP, FoxvilleWakeOnLan),                  0x1 }
};

CONFIG_PATCH_TABLE  mAlderlakeSbgaDdr5SODimmAepSaSetupConfigPatchTable[] = {
//{---------------Type------Size-----------------------------------Offset----------------------------------------------------Data},
{ SIZE_OF_FIELD(SA_SETUP, PrimaryDisplay),                         OFFSET_OF(SA_SETUP, PrimaryDisplay),                        4 }
};

CONFIG_PATCH_STRUCTURE mAlderlakeSbgaDdr5SODimmAepConfigPatchStruct[] = {
  { L"Setup",    &gSetupVariableGuid,    sizeof(SETUP_DATA), mAlderlakeSbgaDdr5SODimmAepConfigPatchTable,         SIZE_OF_TABLE(mAlderlakeSbgaDdr5SODimmAepConfigPatchTable,         CONFIG_PATCH_TABLE) },
  { L"SaSetup",  &gSaSetupVariableGuid,  sizeof(SA_SETUP),   mAlderlakeSbgaDdr5SODimmAepSaSetupConfigPatchTable,  SIZE_OF_TABLE(mAlderlakeSbgaDdr5SODimmAepSaSetupConfigPatchTable,  CONFIG_PATCH_TABLE) },
  { L"CpuSetup", &gCpuSetupVariableGuid, sizeof(CPU_SETUP),  mAlderlakeSbgaDdr5SODimmCpuSetupConfigPatchTable,  SIZE_OF_TABLE(mAlderlakeSbgaDdr5SODimmCpuSetupConfigPatchTable,      CONFIG_PATCH_TABLE) },
  { L"PchSetup", &gPchSetupVariableGuid, sizeof(PCH_SETUP),  mAlderlakeSbgaDdr5SodimmAepPchSetupConfigPatchTable, SIZE_OF_TABLE(mAlderlakeSbgaDdr5SodimmAepPchSetupConfigPatchTable, CONFIG_PATCH_TABLE) },
  { L"MeSetup",  &gMeSetupVariableGuid,  sizeof(ME_SETUP),   NULL,                                       0                                                                                               }
};


CONFIG_PATCH_TABLE  mAlderlakeSS10OCBoardConfigPatchTable[] = {
  { SIZE_OF_FIELD(SI_SETUP, PlatformDebugConsent),                 OFFSET_OF(SI_SETUP, PlatformDebugConsent),                2   }
};

CONFIG_PATCH_STRUCTURE mAlderlakeSS10OCBoardConfigPatchStruct[] = {
  { L"SiSetup",  &gSiSetupVariableGuid,  sizeof(SI_SETUP),   mAlderlakeSS10OCBoardConfigPatchTable,      SIZE_OF_TABLE(mAlderlakeSS10OCBoardConfigPatchTable,      CONFIG_PATCH_TABLE) }
};

CONFIG_PATCH_TABLE  mAlderlakeSSimicsSaSetupConfigPatchTable[] = {
//{---------------Type------Size-----------------------------------Offset----------------------------------------------------Data},
  { SIZE_OF_FIELD(SA_SETUP, VmdEnable),                            OFFSET_OF(SA_SETUP, VmdEnable),                           0x0 },
  { SIZE_OF_FIELD(SA_SETUP, VmdGlobalMapping),                     OFFSET_OF(SA_SETUP, VmdGlobalMapping),                    0x0 }
};

CONFIG_PATCH_STRUCTURE mAlderlakeSSimicsConfigPatchStruct[] = {
  { L"SaSetup",  &gSaSetupVariableGuid,  sizeof(SA_SETUP),   mAlderlakeSSimicsSaSetupConfigPatchTable,   SIZE_OF_TABLE(mAlderlakeSSimicsSaSetupConfigPatchTable,   CONFIG_PATCH_TABLE) }
};


CONFIG_PATCH_TABLE  mAlderlakeSTdpSaSetupConfigPatchTable[] = {
//{---------------Type------Size-----------------------------------Offset----------------------------------------------------Data},
  { SIZE_OF_FIELD(SA_SETUP, SaGv),                                 OFFSET_OF(SA_SETUP, SaGv),                                0x5 }
};

CONFIG_PATCH_STRUCTURE mAlderlakeSTdpPatchStruct[] = {
  { L"SaSetup",  &gSaSetupVariableGuid,  sizeof(SA_SETUP),   mAlderlakeSTdpSaSetupConfigPatchTable,      SIZE_OF_TABLE(mAlderlakeSTdpSaSetupConfigPatchTable,    CONFIG_PATCH_TABLE) }
};

CONFIG_PATCH_TABLE  mFlavorDesktopPs2ConfigPatchTable[] = {
//{---------------Type------Size-----------------------------------Offset----------------------------------------------------Data},
  { SIZE_OF_FIELD(SETUP_DATA, Ps2KbMsEnable),                      OFFSET_OF(SETUP_DATA, Ps2KbMsEnable),                     0   }
};

CONFIG_PATCH_STRUCTURE mFlavorDesktopPs2ConfigPatchStruct[] = {
  { L"Setup",    &gSetupVariableGuid,    sizeof(SETUP_DATA), mFlavorDesktopPs2ConfigPatchTable,    SIZE_OF_TABLE(mFlavorDesktopPs2ConfigPatchTable, CONFIG_PATCH_TABLE) }
};



#endif // _ALDER_LAKE_CONFIG_PATCH_TABLE_H_
