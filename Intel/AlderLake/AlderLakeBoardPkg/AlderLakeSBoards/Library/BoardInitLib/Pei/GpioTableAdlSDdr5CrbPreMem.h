/** @file
  AlderLake S RVP GPIO definition table for Pre-Memory Initialization

@copyright
  INTEL CONFIDENTIAL
  Copyright (c) 2020 - 2021 Intel Corporation. All rights reserved
  This software and associated documentation (if any) is furnished
  under a license and may only be used or copied in accordance
  with the terms of the license. Except as permitted by such
  license, no part of this software or documentation may be
  reproduced, stored in a retrieval system, or transmitted in any
  form or by any means without the express written consent of
  Intel Corporation.
  This file contains an 'Intel Peripheral Driver' and is
  licensed for Intel CPUs and chipsets under the terms of your
  license agreement with Intel or your vendor. This file may
  be modified by the user, subject to additional terms of the
  license agreement.
**/
#ifndef _ALDER_LAKE_S_DDR5_CRB_PREMEM_GPIO_TABLE_H_
#define _ALDER_LAKE_S_DDR5_CRB_PREMEM_GPIO_TABLE_H_

#include <Pins/GpioPinsVer4S.h>
#include <Library/GpioLib.h>
#include <Library/GpioConfig.h>

///
/// !!! GPIOs designated to Native Functions shall not be configured by Platform Code.
/// Native Pins shall be configured by Silicon Code (based on BIOS policies setting) or soft straps(set by CSME in FITc).
/// Configuring pins to native function in GPIO table would override the pin settings resulting in unexpected behavior.
///

GLOBAL_REMOVE_IF_UNREFERENCED GPIO_INIT_CONFIG mGpioTableEarlyPreMemAdlSDdr5UDimm1DCrb[] =
{
  {GPIO_VER4_S_GPP_E0,   {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,     GpioOutHigh,     GpioIntDefault,          GpioPlatformReset,  GpioTermNone }}, //CPU PEG Slot1 Power enable
  {GPIO_VER4_S_GPP_E2,   {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,     GpioOutLow,      GpioIntDefault,          GpioPlatformReset,  GpioTermNone }}, //CPU PEG Slot1 Reset

  {GPIO_VER4_S_GPP_H16,  {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,     GpioOutHigh,     GpioIntDefault,          GpioPlatformReset,  GpioTermNone }}, //CPU M.2 SSD Slot Power enable

  {GPIO_VER4_S_GPP_E1,   {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,     GpioOutHigh,     GpioIntDefault,          GpioPlatformReset,  GpioTermNone }}, //PCIE SLOT 1 - X4 CONNECTOR Power enable
  {GPIO_VER4_S_GPP_F11,  {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,     GpioOutLow,      GpioIntDefault,          GpioPlatformReset,  GpioTermNone }}, //PCIE SLOT 1 - X4 CONNECTOR Reset

  {GPIO_VER4_S_GPP_H11,  {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,     GpioOutHigh,     GpioIntDefault,          GpioPlatformReset,  GpioTermNone }}, //PCIE SLOT 2 - X4 CONNECTOR Power enable
  {GPIO_VER4_S_GPP_F12,  {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,     GpioOutLow,      GpioIntDefault,          GpioHostDeepReset,  GpioTermNone }}, //PCIE SLOT 2 - X4 CONNECTOR Reset

  {GPIO_VER4_S_GPP_B21,  {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,     GpioOutHigh,     GpioIntDefault,          GpioPlatformReset,  GpioTermNone }}, //PCIE SLOT 3 - X2 CONNECTOR Power enable
  {GPIO_VER4_S_GPP_F13,  {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,     GpioOutLow,      GpioIntDefault,          GpioPlatformReset,  GpioTermNone }}, //PCIE SLOT 3 - X2 CONNECTOR Reset

  {GPIO_VER4_S_GPP_K11,  {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,     GpioOutHigh,     GpioIntDefault,          GpioPlatformReset,  GpioTermNone }}, //PCH M.2 SSD Slot 1 Power enable
  //{GPIO_VER4_S_GPP_C10,   {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,     GpioOutLow,      GpioIntDefault,          GpioPlatformReset,  GpioTermNone}},//PCH M.2 SSD Slot 1 Reset

  {GPIO_VER4_S_GPP_B22,  {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,     GpioOutHigh,     GpioIntDefault,          GpioPlatformReset,  GpioTermNone }}, //PCH M.2 SSD Slot 2 Power enable
  //{GPIO_VER4_S_GPP_F16,   {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,     GpioOutLow,      GpioIntDefault,          GpioPlatformReset,  GpioTermNone}},//PCH M.2 SSD Slot 2 Reset

  {GPIO_VER4_S_GPP_K2,   {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,     GpioOutHigh,     GpioIntDefault,          GpioPlatformReset,  GpioTermNone }}, //PCH M.2 SSD Slot 3 Power enable
  //{GPIO_VER4_S_GPP_B6,   {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,     GpioOutLow,      GpioIntDefault,          GpioPlatformReset,  GpioTermNone}},//PCH M.2 SSD Slot 3 Reset

  { GPIO_VER4_S_GPP_G1,  {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutHigh,     GpioIntDefault,          GpioHostDeepReset,  GpioTermNone }}, //Thunderbolt CIO_PWR_EN (CIO_PWR_EN needs set to high before PCIe SLOT_2 PERST set to high; at least 10ms)
  { GPIO_VER4_S_GPP_F4,  {GpioPadModeGpio, GpioHostOwnGpio, GpioDirIn,      GpioOutDefault,  GpioIntDefault,          GpioPlatformReset,  GpioTermWpu20K }}, //WA:20K weak pull-up on S06 Fab1
  {0x0}
};

GLOBAL_REMOVE_IF_UNREFERENCED GPIO_INIT_CONFIG mGpioTableEarlyPreMemAdlSDdr5UDimm1DCrbFab2[] =
{
  {GPIO_VER4_S_GPP_F4,   {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,     GpioOutHigh,     GpioIntDefault,          GpioPlatformReset,  GpioTermNone }}, //CPU PEG Slot1 Power enable
  {GPIO_VER4_S_GPP_E2,   {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,     GpioOutLow,      GpioIntDefault,          GpioPlatformReset,  GpioTermNone }}, //CPU PEG Slot1 Reset

  {GPIO_VER4_S_GPP_H16,  {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,     GpioOutHigh,     GpioIntDefault,          GpioPlatformReset,  GpioTermNone }}, //CPU M.2 SSD Slot Power enable

  {GPIO_VER4_S_GPP_E1,   {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,     GpioOutHigh,     GpioIntDefault,          GpioPlatformReset,  GpioTermNone }}, //PCIE SLOT 1 - X4 CONNECTOR Power enable
  {GPIO_VER4_S_GPP_F11,  {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,     GpioOutLow,      GpioIntDefault,          GpioPlatformReset,  GpioTermNone }}, //PCIE SLOT 1 - X4 CONNECTOR Reset

  {GPIO_VER4_S_GPP_H11,  {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,     GpioOutHigh,     GpioIntDefault,          GpioPlatformReset,  GpioTermNone }}, //PCIE SLOT 2 - X4 CONNECTOR Power enable
  {GPIO_VER4_S_GPP_F12,  {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,     GpioOutLow,      GpioIntDefault,          GpioHostDeepReset,  GpioTermNone }}, //PCIE SLOT 2 - X4 CONNECTOR Reset

  {GPIO_VER4_S_GPP_B21,  {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,     GpioOutHigh,     GpioIntDefault,          GpioPlatformReset,  GpioTermNone }}, //PCIE SLOT 3 - X2 CONNECTOR Power enable
  {GPIO_VER4_S_GPP_F13,  {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,     GpioOutLow,      GpioIntDefault,          GpioPlatformReset,  GpioTermNone }}, //PCIE SLOT 3 - X2 CONNECTOR Reset

  {GPIO_VER4_S_GPP_K11,  {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,     GpioOutHigh,     GpioIntDefault,          GpioPlatformReset,  GpioTermNone }}, //PCH M.2 SSD Slot 1 Power enable
  //{GPIO_VER4_S_GPP_C10,   {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,     GpioOutLow,      GpioIntDefault,          GpioPlatformReset,  GpioTermNone}},//PCH M.2 SSD Slot 1 Reset

  {GPIO_VER4_S_GPP_B22,  {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,     GpioOutHigh,     GpioIntDefault,          GpioPlatformReset,  GpioTermNone }}, //PCH M.2 SSD Slot 2 Power enable,not used on S08
  //{GPIO_VER4_S_GPP_F16,   {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,     GpioOutLow,      GpioIntDefault,          GpioPlatformReset,  GpioTermNone}},//PCH M.2 SSD Slot 2 Reset

  {GPIO_VER4_S_GPP_K2,   {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,     GpioOutHigh,     GpioIntDefault,          GpioPlatformReset,  GpioTermNone }}, //PCH M.2 SSD Slot 3 Power enable
  //{GPIO_VER4_S_GPP_B6,   {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,     GpioOutLow,      GpioIntDefault,          GpioPlatformReset,  GpioTermNone}},//PCH M.2 SSD Slot 3 Reset
  {GPIO_VER4_S_GPP_G1,  {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutHigh,     GpioIntDefault,          GpioHostDeepReset,  GpioTermNone }}, //Thunderbolt CIO_PWR_EN (CIO_PWR_EN needs set to high before PCIe SLOT_2 PERST set to high; at least 10ms)
  {0x0}
};

GLOBAL_REMOVE_IF_UNREFERENCED GPIO_INIT_CONFIG mGpioTableEarlyPreMemAdlSDdr5SODimmCrb[] =
{
  {GPIO_VER4_S_GPP_F4,   {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,     GpioOutHigh,     GpioIntDefault,          GpioPlatformReset,  GpioTermNone }}, //CPU PEG Slot1 Power enable
  {GPIO_VER4_S_GPP_E2,   {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,     GpioOutLow,      GpioIntDefault,          GpioPlatformReset,  GpioTermNone }}, //CPU PEG Slot1 Reset

  {GPIO_VER4_S_GPP_H16,  {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,     GpioOutHigh,     GpioIntDefault,          GpioPlatformReset,  GpioTermNone }}, //CPU M.2 SSD Slot Power enable

  {GPIO_VER4_S_GPP_E1,   {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,     GpioOutHigh,     GpioIntDefault,          GpioPlatformReset,  GpioTermNone }}, //PCIE SLOT 1 - X4 CONNECTOR Power enable
  {GPIO_VER4_S_GPP_F11,  {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,     GpioOutLow,      GpioIntDefault,          GpioPlatformReset,  GpioTermNone }}, //PCIE SLOT 1 - X4 CONNECTOR Reset

  {GPIO_VER4_S_GPP_H11,  {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,     GpioOutHigh,     GpioIntDefault,          GpioPlatformReset,  GpioTermNone }}, //PCIE SLOT 2 - X4 CONNECTOR Power enable
  {GPIO_VER4_S_GPP_F12,  {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,     GpioOutLow,      GpioIntDefault,          GpioPlatformReset,  GpioTermNone }}, //PCIE SLOT 2 - X4 CONNECTOR Reset

  {GPIO_VER4_S_GPP_B21,  {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,     GpioOutHigh,     GpioIntDefault,          GpioPlatformReset,  GpioTermNone }}, //PCIE SLOT 3 - X2 CONNECTOR Power enable
  {GPIO_VER4_S_GPP_F13,  {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,     GpioOutLow,      GpioIntDefault,          GpioPlatformReset,  GpioTermNone }}, //PCIE SLOT 3 - X2 CONNECTOR Reset

  {GPIO_VER4_S_GPP_K11,  {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,     GpioOutHigh,     GpioIntDefault,          GpioPlatformReset,  GpioTermNone }}, //PCH M.2 SSD Slot 1 Power enable
  //{GPIO_VER4_S_GPP_C10,   {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,     GpioOutLow,      GpioIntDefault,          GpioPlatformReset,  GpioTermNone}},//PCH M.2 SSD Slot 1 Reset

  {GPIO_VER4_S_GPP_B22,  {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,     GpioOutHigh,     GpioIntDefault,          GpioPlatformReset,  GpioTermNone }}, //PCH M.2 SSD Slot 2 Power enable,not used on S08
  //{GPIO_VER4_S_GPP_F16,   {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,     GpioOutLow,      GpioIntDefault,          GpioPlatformReset,  GpioTermNone}},//PCH M.2 SSD Slot 2 Reset

  {GPIO_VER4_S_GPP_K2,   {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,     GpioOutHigh,     GpioIntDefault,          GpioPlatformReset,  GpioTermNone }}, //PCH M.2 SSD Slot 3 Power enable
  //{GPIO_VER4_S_GPP_B6,   {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,     GpioOutLow,      GpioIntDefault,          GpioPlatformReset,  GpioTermNone}},//PCH M.2 SSD Slot 3 Reset
  {GPIO_VER4_S_GPP_F5,    {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutHigh,     GpioIntDefault,          GpioResetDefault,   GpioTermNone}}, //POR Thunderbolt RTD3 enable,and CIO_PWR_EN
  {0x0}
};

GLOBAL_REMOVE_IF_UNREFERENCED GPIO_INIT_CONFIG mGpioTableEarlyPreMemAdlSDdr5UDimm1DAep[] =
{
  {GPIO_VER4_S_GPP_B22,  {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutHigh,    GpioIntDefault,           GpioPlatformReset, GpioTermNone }},// Maple Ridge 1 (Back Panel) BP_USB_FRC_PWR
  {GPIO_VER4_S_GPP_G0,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutHigh,    GpioIntDefault,           GpioPlatformReset, GpioTermNone }},// Maple Ridge 1 (Back Panel) TBT_BP_FRC_PWR
  {GPIO_VER4_S_GPP_G1,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutHigh,    GpioIntDefault,           GpioHostDeepReset, GpioTermNone }},// Maple Ridge 1 (Back Panel) TBT_BP_CIO_PWREN
  {GPIO_VER4_S_GPP_K4,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutHigh,    GpioIntDefault,           GpioResetDefault,  GpioTermNone }},// Maple Ridge 1 (Back Panel) TBT_BP_RST_N

  {GPIO_VER4_S_GPP_B21,  {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutHigh,    GpioIntDefault,           GpioPlatformReset, GpioTermNone }},// Maple Ridge 2 (Front Panel) FP_USB_FRC_PWR
  {GPIO_VER4_S_GPP_D11,  {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutHigh,    GpioIntDefault,           GpioHostDeepReset, GpioTermNone }},// Maple Ridge 2 (Front Panel) TBT_FP_CIO_PWREN
  {GPIO_VER4_S_GPP_D13,  {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutHigh,    GpioIntDefault,           GpioPlatformReset, GpioTermNone }},// Maple Ridge 2 (Front Panel) TBT_FP_FRC_PWR
  {GPIO_VER4_S_GPP_K3,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutHigh,    GpioIntDefault,           GpioResetDefault,  GpioTermNone }},// Maple Ridge 2 (Front Panel) TBT_FP_RST_N

  {GPIO_VER4_S_GPP_C2,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutHigh,    GpioIntDefault,           GpioPlatformReset, GpioTermNone }},// M.2 SSD_1 Power enable (PCH SSD)
  {GPIO_VER4_S_GPP_C10,  {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutHigh,    GpioIntDefault,           GpioPlatformReset, GpioTermNone }},// M.2 SSD_1 RTD3 Reset LPSS UART-0 header

  {GPIO_VER4_S_GPP_E0,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutHigh,    GpioIntDefault,           GpioPlatformReset, GpioTermNone }}, // PEG SLOT Power enable
  {GPIO_VER4_S_GPP_E2,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutLow,     GpioIntDefault,           GpioPlatformReset, GpioTermNone }}, // PEG_1 RTD3 Reset

  {GPIO_VER4_S_GPP_F12,  {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutHigh,    GpioIntDefault,           GpioResetDefault,  GpioTermNone }},// EDSFF RST_N
  {GPIO_VER4_S_GPP_F20,  {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutHigh,    GpioIntDefault,           GpioResetDefault,  GpioTermNone }},// EDSFF SMBRST_N

  {GPIO_VER4_S_GPP_F18,  {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutHigh,    GpioIntDefault,           GpioPlatformReset, GpioTermNone }},// M.2 Key-M Slot(CPU SSD) CPUSSD_RST_N
  {GPIO_VER4_S_GPP_H16,  {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutHigh,    GpioIntDefault,           GpioPlatformReset, GpioTermNone }},// M.2 Key-M Slot (CPU SSD) CPUSSD_PWREN
  {0x0}
};

GLOBAL_REMOVE_IF_UNREFERENCED GPIO_INIT_CONFIG mGpioTableEarlyPreMemAdlSDdr5UDimm1DAepFab2[] =
{
  {GPIO_VER4_S_GPP_B22,  {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutHigh,    GpioIntDefault,           GpioPlatformReset, GpioTermNone }},// Maple Ridge 1 (Back Panel) BP_USB_FRC_PWR
  {GPIO_VER4_S_GPP_G0,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutHigh,    GpioIntDefault,           GpioPlatformReset, GpioTermNone }},// Maple Ridge 1 (Back Panel) TBT_BP_FRC_PWR
  {GPIO_VER4_S_GPP_G1,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutHigh,    GpioIntDefault,           GpioPlatformReset, GpioTermNone }},// Maple Ridge 1 (Back Panel) TBT_BP_CIO_PWREN
  {GPIO_VER4_S_GPP_K4,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutHigh,    GpioIntDefault,           GpioResetDefault,  GpioTermNone }},// Maple Ridge 1 (Back Panel) TBT_BP_RST_N

  {GPIO_VER4_S_GPP_B21,  {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutHigh,    GpioIntDefault,           GpioPlatformReset, GpioTermNone }},// Maple Ridge 2 (Front Panel) FP_USB_FRC_PWR
  {GPIO_VER4_S_GPP_D11,  {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutHigh,    GpioIntDefault,           GpioPlatformReset, GpioTermNone }},// Maple Ridge 2 (Front Panel) TBT_FP_CIO_PWREN
  {GPIO_VER4_S_GPP_D13,  {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutHigh,    GpioIntDefault,           GpioPlatformReset, GpioTermNone }},// Maple Ridge 2 (Front Panel) TBT_FP_FRC_PWR
  {GPIO_VER4_S_GPP_K3,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutHigh,    GpioIntDefault,           GpioResetDefault,  GpioTermNone }},// Maple Ridge 2 (Front Panel) TBT_FP_RST_N

  {GPIO_VER4_S_GPP_C2,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutHigh,    GpioIntDefault,           GpioPlatformReset, GpioTermNone }},// M.2 SSD_1 Power enable (PCH SSD)
  {GPIO_VER4_S_GPP_C10,  {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutHigh,    GpioIntDefault,           GpioPlatformReset, GpioTermNone }},// M.2 SSD_1 RTD3 Reset LPSS UART-0 header

  {GPIO_VER4_S_GPP_E3,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutHigh,    GpioIntDefault,           GpioPlatformReset, GpioTermNone }},// PEG SLOT Power enable
  {GPIO_VER4_S_GPP_E2,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutLow,     GpioIntDefault,           GpioPlatformReset, GpioTermNone }},// PEG_1 RTD3 Reset

  {GPIO_VER4_S_GPP_F12,  {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutHigh,    GpioIntDefault,           GpioResetDefault, GpioTermNone }},// EDSFF RST_N
  {GPIO_VER4_S_GPP_F20,  {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutHigh,    GpioIntDefault,           GpioResetDefault, GpioTermNone }},// EDSFF SMBRST_N

  {GPIO_VER4_S_GPP_F18,  {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutHigh,    GpioIntDefault,           GpioPlatformReset, GpioTermNone }},// M.2 Key-M Slot(CPU SSD) CPUSSD_RST_N
  {GPIO_VER4_S_GPP_H16,  {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutHigh,    GpioIntDefault,           GpioPlatformReset, GpioTermNone }},// M.2 Key-M Slot (CPU SSD) CPUSSD_PWREN
  {0x0}
};

GLOBAL_REMOVE_IF_UNREFERENCED GPIO_INIT_CONFIG mGpioTableEarlyPreMemAdlSSbgaDdr5SODimmErb[] =
{
  {GPIO_VER4_S_GPP_K11,  {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,     GpioOutHigh,     GpioIntDefault,          GpioPlatformReset,  GpioTermNone}}, // PCH M.2 SSD Power enable
  {GPIO_VER4_S_GPP_H16,  {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,     GpioOutHigh,     GpioIntDefault,          GpioPlatformReset,  GpioTermNone}}, // CPU M.2 SSD Power enable
  {GPIO_VER4_S_GPP_K3,   {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,     GpioOutLow,      GpioIntDefault,          GpioPlatformReset,  GpioTermNone}}, // PEG slot power enable, with on-board inverter
  {GPIO_VER4_S_GPP_E2,   {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,     GpioOutLow,      GpioIntDefault,          GpioPlatformReset,  GpioTermNone}}, // PEG slot Reset
  {GPIO_VER4_S_GPP_B6,   {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,     GpioOutHigh,     GpioIntDefault,          GpioPlatformReset,  GpioTermNone}}, // PCH x4 Slot Power enable
  {GPIO_VER4_S_GPP_E17,  {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,     GpioOutLow,      GpioIntDefault,          GpioPlatformReset,  GpioTermNone}}, // PCH x4 Slot Reset
  {GPIO_VER4_S_GPP_C10,  {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,     GpioOutLow,      GpioIntDefault,          GpioPlatformReset,  GpioTermNone}}, // PCH SSD Reset
  {GPIO_VER4_S_GPP_F18,  {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,     GpioOutLow,      GpioIntDefault,          GpioPlatformReset,  GpioTermNone}}, // CPU SSD reset
  {GPIO_VER4_S_GPP_F5,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutHigh,     GpioIntDefault,          GpioHostDeepReset,   GpioTermNone}},//POR Thunderbolt RTD3 enable, MIPI60 (Rework)
  {GPIO_VER4_S_GPP_E7,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutHigh,     GpioIntDefault,          GpioHostDeepReset,   GpioTermNone}},//Thunderbolt 1 RTD3 Power enable
  {GPIO_VER4_S_GPP_F16,  {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutLow,      GpioIntDefault,          GpioHostDeepReset,   GpioTermNone}},//POR Thunderbolt Reset,MIPI60 (Rework)
  {GPIO_VER4_S_GPP_F2,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutLow,      GpioIntDefault,          GpioHostDeepReset,   GpioTermNone}},//Thunderbolt RTD3 reset

  {0x0}
};

GLOBAL_REMOVE_IF_UNREFERENCED GPIO_INIT_CONFIG mGpioTableEarlyPreMemAdlSSbgaDdr5SODimmAep[] =
{
 //PCH SSD1
  {GPIO_VER4_S_GPP_H16,  {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,     GpioOutHigh,     GpioIntDefault,          GpioPlatformReset,  GpioTermNone}}, // PCH SSD1 Power enable
  {GPIO_VER4_S_GPP_F18,  {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,     GpioOutLow,      GpioIntDefault,          GpioPlatformReset,  GpioTermNone}}, // PCH SSD1 reset

 //PCH SSD2
  {GPIO_VER4_S_GPP_K11,  {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,     GpioOutHigh,     GpioIntDefault,          GpioPlatformReset,  GpioTermNone}}, // PCH M.2 SSD2 Power enable
  {GPIO_VER4_S_GPP_C10,  {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,     GpioOutLow,      GpioIntDefault,          GpioPlatformReset,  GpioTermNone}}, // PCH SSD2 Reset

 //DG
  {GPIO_VER4_S_GPP_K3,   {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,     GpioOutHigh,     GpioIntDefault,          GpioPlatformReset,  GpioTermNone}}, // PEG slot power enable
  {GPIO_VER4_S_GPP_E2,   {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,     GpioOutLow,      GpioIntDefault,          GpioPlatformReset,  GpioTermNone}}, // PEG slot Reset

 //TBT
  {GPIO_VER4_S_GPP_F5,   {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,     GpioOutHigh,     GpioIntDefault,          GpioHostDeepReset,   GpioTermNone}},//POR Thunderbolt RTD3 enable
  {GPIO_VER4_S_GPP_F16,  {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,     GpioOutLow,      GpioIntDefault,          GpioHostDeepReset,   GpioTermNone}},//POR Thunderbolt Reset
  {0x0}
};

GLOBAL_REMOVE_IF_UNREFERENCED GPIO_INIT_CONFIG mGpioTableAdlSSbgaWwanOffEarlyPreMem[] =
{
  { GPIO_VER4_S_GPP_I7,  {GpioPadModeGpio, GpioHostOwnGpio,     GpioDirOut,  GpioOutLow,       GpioIntDefault,        GpioResetDefault,  GpioTermNone, GpioOutputStateUnlock }}, // WWAN_RST_N
  { GPIO_VER4_S_GPP_H11, {GpioPadModeGpio, GpioHostOwnGpio,     GpioDirOut,  GpioOutLow,       GpioIntDefault,        GpioPlatformReset, GpioTermNone, GpioOutputStateUnlock }}, // WWAN_PERST_N
  { GPIO_VER4_S_GPP_F23, {GpioPadModeGpio, GpioHostOwnGpio,     GpioDirOut,  GpioOutLow,       GpioIntDefault,        GpioPlatformReset, GpioTermNone, GpioOutputStateUnlock }}, // WWAN_FCP_OFF_N
};

GLOBAL_REMOVE_IF_UNREFERENCED GPIO_INIT_CONFIG mGpioTableAdlSSbgaWwanOnEarlyPreMem[] =
{
  // WWAN
  // Please don't change the sequence of Gpio Initialization
  {GPIO_VER4_S_GPP_K2,   {GpioPadModeGpio, GpioHostOwnGpio,     GpioDirOut,   GpioOutHigh,     GpioIntDefault,          GpioPlatformReset, GpioTermNone, GpioOutputStateUnlock }},  // WWAN_PWREN
  {GPIO_VER4_S_GPP_F23,  {GpioPadModeGpio, GpioHostOwnGpio,     GpioDirOut,   GpioOutHigh,     GpioIntDefault,          GpioPlatformReset, GpioTermNone, GpioOutputStateUnlock }},  // WWAN_FCP_OFF_N
  {GPIO_VER4_S_GPP_I7,   {GpioPadModeGpio, GpioHostOwnGpio,     GpioDirOut,   GpioOutHigh,     GpioIntDefault,          GpioResetDefault,  GpioTermNone, GpioOutputStateUnlock }},  // WWAN_RST_N
  {GPIO_VER4_S_GPP_H11,  {GpioPadModeGpio, GpioHostOwnGpio,     GpioDirOut,   GpioOutHigh,     GpioIntDefault,          GpioPlatformReset, GpioTermNone, GpioOutputStateUnlock }},  // WWAN_PERST_N
  {GPIO_VER4_S_GPP_H10,  {GpioPadModeGpio, GpioHostOwnAcpi,     GpioDirInInv, GpioOutDefault,  GpioIntEdge|GpioIntSci,  GpioResetDefault,  GpioTermNone, GpioPadConfigUnlock   }},  // WWAN_WAKE_N
  {GPIO_VER4_S_GPP_E1,   {GpioPadModeGpio, GpioHostOwnGpio,     GpioDirOut,   GpioOutHigh,     GpioIntDefault,          GpioPlatformReset, GpioTermNone, GpioOutputStateUnlock }},  // WWAN_DISABLE_N
  {GPIO_VER4_S_GPP_G4,   {GpioPadModeGpio, GpioHostOwnGpio,     GpioDirOut,   GpioOutDefault,  GpioIntDefault,          GpioResetDefault,  GpioTermNone, GpioPadConfigUnlock   }},  // SAR_DPR_PCH
};

GLOBAL_REMOVE_IF_UNREFERENCED GPIO_INIT_CONFIG mGpioTableAdlSSbgaWwanM80EarlyPreMem[] =
{
  // WWAN
  // Please don't change the sequence of Gpio Initialization
  {GPIO_VER4_S_GPP_K2,   {GpioPadModeGpio, GpioHostOwnGpio,     GpioDirOut,   GpioOutHigh,     GpioIntDefault,          GpioPlatformReset, GpioTermNone, GpioOutputStateUnlock }},  // WWAN_PWREN
  {GPIO_VER4_S_GPP_F23,  {GpioPadModeGpio, GpioHostOwnGpio,     GpioDirOut,   GpioOutHigh,     GpioIntDefault,          GpioPlatformReset, GpioTermNone, GpioOutputStateUnlock }},  // WWAN_FCP_OFF_N
  {GPIO_VER4_S_GPP_I7,   {GpioPadModeGpio, GpioHostOwnGpio,     GpioDirOut,   GpioOutLow,      GpioIntDefault,          GpioResetDefault,  GpioTermNone, GpioOutputStateUnlock }},  // WWAN_RST_N
  {GPIO_VER4_S_GPP_H11,  {GpioPadModeGpio, GpioHostOwnGpio,     GpioDirOut,   GpioOutLow,      GpioIntDefault,          GpioPlatformReset, GpioTermNone, GpioOutputStateUnlock }},  // WWAN_PERST_N
  {GPIO_VER4_S_GPP_H10,  {GpioPadModeGpio, GpioHostOwnAcpi,     GpioDirInInv, GpioOutDefault,  GpioIntEdge|GpioIntSci,  GpioResetDefault,  GpioTermNone, GpioPadConfigUnlock   }},  // WWAN_WAKE_N
  {GPIO_VER4_S_GPP_E1,   {GpioPadModeGpio, GpioHostOwnGpio,     GpioDirOut,   GpioOutHigh,     GpioIntDefault,          GpioPlatformReset, GpioTermNone, GpioOutputStateUnlock }},  // WWAN_DISABLE_N
  {GPIO_VER4_S_GPP_G4,   {GpioPadModeGpio, GpioHostOwnGpio,     GpioDirOut,   GpioOutDefault,  GpioIntDefault,          GpioResetDefault,  GpioTermNone, GpioPadConfigUnlock   }},  // SAR_DPR_PCH
};
#endif // _ALDER_LAKE_S_DDR4_CRB_PREMEM_GPIO_TABLE_H_
