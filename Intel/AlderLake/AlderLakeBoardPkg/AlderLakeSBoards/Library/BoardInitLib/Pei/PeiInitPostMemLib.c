/** @file

;******************************************************************************
;* Copyright (c) 2020 - 2021, Insyde Software Corporation. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/
/** @file

@copyright
  INTEL CONFIDENTIAL
  Copyright 2017 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains a 'Sample Driver' and is licensed as such under the terms
  of your license agreement with Intel or your vendor. This file may be modified
  by the user, subject to the additional terms of the license agreement.

@par Specification Reference:
**/

#include <PiPei.h>
#include <Library/DebugLib.h>
#include <Library/PcdLib.h>

#include <Register/PttPtpRegs.h>
#include <Library/SiliconInitLib.h>
#include <Library/PreSiliconEnvDetectLib.h>
#include <Library/BoardConfigLib.h>

#include "GpioTableAdlSDdr4CrbPostMem.h"
#include <Pins/GpioPinsVer4S.h>
#include <Library/PeiServicesLib.h>
#include <Library/GpioLib.h>
#include <Library/IoLib.h>
#include <Library/PeiHdaVerbTables.h>
#include <PlatformBoardId.h>
#include <PlatformBoardConfig.h>
#include <UsbTypeC.h>
#if FixedPcdGetBool (PcdDTbtEnable) == 1
#include <DTbtInfoHob.h>
#include <Library/HobLib.h>
#endif
#if FixedPcdGetBool (PcdSetupEnable) == 1
  #include <Ppi/ReadOnlyVariable2.h>
  #include <SetupVariable.h>
  extern EFI_GUID gSetupVariableGuid;
#endif
//[-start-201104-IB17510119-add]//
#include <Library/GpioCfgLib.h>
#include <Library/GpioNativeLib.h>
#include <Library/MemoryAllocationLib.h>
#include <Library/BaseMemoryLib.h>
#include <Library/PeiOemSvcKernelLib.h>
//[-end-201104-IB17510119-add]//
//[-start-191001-16990100-add]//
#include <Library/PeiOemSvcChipsetLib.h>
//[-end-191001-16990100-add]//

/**
  Alderlake S boards configuration init function for PEI post memory phase.

  @retval EFI_SUCCESS             The function completed successfully.
**/
EFI_STATUS
EFIAPI
AdlSGpioTableInit (
  VOID
  )
{
  GPIO_INIT_CONFIG  *GpioTable;
  GpioTable = NULL;

  //
  // GPIO Table Init, Update PostMem GPIO table to PcdBoardGpioTable
  //


  GpioTable = (GPIO_INIT_CONFIG *)PcdGetPtr(VpdPcdBoardGpioTable);
  ConfigureGpioTabletoPCD (GpioTable, POST_MEM);

  return EFI_SUCCESS;
}

/**
  Touch panel GPIO init function for PEI post memory phase.
**/
VOID
AdlSTouchPanelGpioInit (
  VOID
  )
{
  UINT16     BoardId;
  BoardId =  PcdGet16 (PcdBoardId);

  DEBUG ((DEBUG_INFO, "TouchPanelGpioInit()\n"));
  //PcdSet32S (PcdBoardGpioTableTouchPanel, 0);
  switch (BoardId) {
    case BoardIdAdlSAdpSDdr5UDimm1DAep:
    case BoardIdAdlSAdpSDdr5UDimm1DOc:
      break;
    case BoardIdAdlSAdpSDdr4UDimm2DCrb:
    case BoardIdAdlSAdpSDdr5UDimm1DCrb:
    case BoardIdAdlSAdpSDdr5UDimm1DCrbPpv:
    case BoardIdAdlSAdpSDdr5UDimm2DCrb:
    case BoardIdAdlSAdpSDdr4UDimm2DCrbEv:
    case BoardIdAdlSAdpSDdr4UDimm2DCrbCpv:
    case BoardIdAdlSAdpSDdr5SODimmCrb:
    case BoardIdAdlSAdpSDdr4SODimmCrb:
    case BoardIdAdlSAdpSDdr5UDimm1DSr:
      //I2C Touch Panel GPIO
      PcdSet32S (PcdTouchpanelIrqGpio, GPIO_VER4_S_GPP_D23);  // Touch 0 Interrupt Pin
      PcdSet32S (PcdTouchpanelPwrEnableGpio, GPIO_VER4_S_GPP_R21);  // Touch 0 power enable pin
      PcdSet32S (PcdTouchpanelRstGpio, GPIO_VER4_S_GPP_D22);  // Touch 0 reset pin
      PcdSetBoolS (PcdTouchpanelIrqGpioPolarity, PIN_GPIO_ACTIVE_HIGH);  // Touch 0 Interrupt Pin polarity
      PcdSetBoolS (PcdTouchpanelPwrEnableGpioPolarity, PIN_GPIO_ACTIVE_HIGH);  // Touch 0 power enable pin polarity
      PcdSetBoolS (PcdTouchpanelRstGpioPolarity, PIN_GPIO_ACTIVE_HIGH);  // Touch 0 reset pin polarity;
      PcdSetBoolS (PcdTouchpanel1RstGpioPolarity, 0);
      break;
    case BoardIdAdlSAdpSSbgaDdr5SODimmErb:
    case BoardIdAdlSAdpSSbgaDdr5SODimmCrb:
      // Touch Pad
      PcdSet32S (PcdTouchpadIrqGpio, GPIO_VER4_S_GPP_F22);                     // Touch Pad Interrupt Pin
      PcdSetBoolS (PcdTouchpadIrqGpioPolarity, PIN_GPIO_ACTIVE_LOW);            // Touch Pad Interrupt pin polarity

      //I2C Touch Panel 0 GPIO
      PcdSet32S (PcdTouchpanelIrqGpio, GPIO_VER4_S_GPP_D23);  // Touch 0 Interrupt Pin
      PcdSet32S (PcdTouchpanelPwrEnableGpio, GPIO_VER4_S_GPP_R21);  // Touch 0 power enable pin
      PcdSet32S (PcdTouchpanelRstGpio, GPIO_VER4_S_GPP_D22);  // Touch 0 reset pin
      PcdSetBoolS (PcdTouchpanelIrqGpioPolarity, PIN_GPIO_ACTIVE_HIGH);  // Touch 0 Interrupt Pin polarity
      PcdSetBoolS (PcdTouchpanelPwrEnableGpioPolarity, PIN_GPIO_ACTIVE_HIGH);  // Touch 0 power enable pin polarity
      PcdSetBoolS (PcdTouchpanelRstGpioPolarity, PIN_GPIO_ACTIVE_HIGH);  // Touch 0 reset pin polarity;

      //I2C Touch Panel 1 GPIO
      PcdSet32S (PcdTouchpanel1IrqGpio, GPIO_VER4_S_GPP_R16);  // Touch 1 Interrupt Pin
      PcdSet32S (PcdTouchpanel1PwrEnableGpio, GPIO_VER4_S_GPP_A14);  // Touch 1 power enable pin
      PcdSet32S (PcdTouchpanel1RstGpio, GPIO_VER4_S_GPP_R17);  // Touch 1 reset pin
      PcdSetBoolS (PcdTouchpanel1IrqGpioPolarity, PIN_GPIO_ACTIVE_HIGH);  // Touch 1 Interrupt Pin polarity
      PcdSetBoolS (PcdTouchpanel1PwrEnableGpioPolarity, PIN_GPIO_ACTIVE_HIGH);  // Touch 11 power enable pin polarity
      PcdSetBoolS (PcdTouchpanel1RstGpioPolarity, PIN_GPIO_ACTIVE_HIGH);  // Touch 1 reset pin polarity;
      break;

    case BoardIdAdlSAdpSSbgaDdr4SODimmCrb:
      //I2C Touch Panel 0 GPIO
      PcdSet32S (PcdTouchpanelIrqGpio, GPIO_VER4_S_GPP_D23);  // Touch 0 Interrupt Pin
      PcdSet32S (PcdTouchpanelPwrEnableGpio, GPIO_VER4_S_GPP_R21);  // Touch 0 power enable pin
      PcdSet32S (PcdTouchpanelRstGpio, GPIO_VER4_S_GPP_D22);  // Touch 0 reset pin
      PcdSetBoolS (PcdTouchpanelIrqGpioPolarity, PIN_GPIO_ACTIVE_HIGH);  // Touch 0 Interrupt Pin polarity
      PcdSetBoolS (PcdTouchpanelPwrEnableGpioPolarity, PIN_GPIO_ACTIVE_HIGH);  // Touch 0 power enable pin polarity
      PcdSetBoolS (PcdTouchpanelRstGpioPolarity, PIN_GPIO_ACTIVE_HIGH);  // Touch 0 reset pin polarity;
      PcdSetBoolS (PcdTouchpanel1RstGpioPolarity, 0);

      //I2C Touch Panel 1 GPIO
      PcdSet32S (PcdTouchpanel1IrqGpio, GPIO_VER4_S_GPP_I6);  // Touch 1 Interrupt Pin
      PcdSet32S (PcdTouchpanel1PwrEnableGpio, GPIO_VER4_S_GPP_A14);  // Touch 1 power enable pin
      PcdSet32S (PcdTouchpanel1RstGpio, GPIO_VER4_S_GPP_I5);  // Touch 1 reset pin
      PcdSetBoolS (PcdTouchpanel1IrqGpioPolarity, PIN_GPIO_ACTIVE_HIGH);  // Touch 1 Interrupt Pin polarity
      PcdSetBoolS (PcdTouchpanel1PwrEnableGpioPolarity, PIN_GPIO_ACTIVE_HIGH);  // Touch 11 power enable pin polarity
      PcdSetBoolS (PcdTouchpanel1RstGpioPolarity, PIN_GPIO_ACTIVE_HIGH);  // Touch 1 reset pin polarity;
      break;

    case BoardIdAdlSAdpSSbgaDdr5SODimmAep:
      // Touch Pad
      PcdSet32S (PcdTouchpadIrqGpio, GPIO_VER4_S_GPP_F22);                     // Touch Pad Interrupt Pin
      PcdSetBoolS (PcdTouchpadIrqGpioPolarity, PIN_GPIO_ACTIVE_LOW);           // Touch Pad Interrupt pin polarity
      break;

    default:
      PcdSet32S (PcdTouchpanel1PwrEnableGpio, GPIO_VER4_S_GPP_R21);
      PcdSetBoolS (PcdTouchpanel1PwrEnableGpioPolarity, 1);
      PcdSet32S (PcdTouchpanel1IrqGpio, GPIO_VER4_S_GPP_D23);
      PcdSetBoolS (PcdTouchpanel1IrqGpioPolarity, 0);
      PcdSet32S (PcdTouchpanel1RstGpio, GPIO_VER4_S_GPP_D22);
      PcdSetBoolS (PcdTouchpanel1RstGpioPolarity, 0);
      break;
  }
}

/**
  HDA VerbTable init function for PEI post memory phase.
**/
VOID
AdlSHdaVerbTableInit (
  VOID
  )
{
  PcdSet32S (PcdHdaVerbTableDatabase, (UINT32) &HdaVerbTableDbAdlNoDmic);
}

/**
  Misc. init function for PEI post memory phase.
**/
VOID
AdlSBoardMiscInit (
  VOID
  )
{
  UINT16        BoardId;
  UINT16        FabId;

  BoardId       = PcdGet16 (PcdBoardId);
  FabId         = PcdGet16 (PcdBoardRev);

  PcdSet8S (PcdSataPortsEnable0, 0x1);
  PcdSetBoolS (PcdSataLedEnable, TRUE);
  PcdSetBoolS (PcdVrAlertEnable, FALSE);
  PcdSetBoolS (PcdPchThermalHotEnable, FALSE);
  PcdSetBoolS (PcdMemoryThermalSensorGpioCPmsyncEnable, FALSE);
  PcdSetBoolS (PcdMemoryThermalSensorGpioDPmsyncEnable, FALSE);

  PcdSet32S (PcdBoardGpioTableCnvd, (UINTN) mAdlSCnvdGpioTable);
  PcdSet16S (PcdBoardGpioTableCnvdSize, sizeof (mAdlSCnvdGpioTable) / sizeof (GPIO_INIT_CONFIG));

  switch (BoardId) {
    case BoardIdAdlSAdpSDdr5SODimmCrb:
    case BoardIdAdlSAdpSDdr4SODimmCrb:
      // CPU PEG Slot1 RTD3
      PcdSet32S (PcdPegSlot1PwrEnableGpioNo, GPIO_VER4_S_GPP_F4);
      PcdSetBoolS (PcdPegSlot1PwrEnableGpioPolarity, 1);
      PcdSet32S (PcdPegSlot1RstGpioNo, GPIO_VER4_S_GPP_E2);
      PcdSetBoolS (PcdPegSlot1RstGpioPolarity, 0);
      PcdSet32S (PcdPegSlot1WakeGpioPin, GPIO_VER4_S_GPP_E6);
      //PcdSetBoolS (PcdPegSlot1RootPort, value);

      // CPU PEG Slot2 RTD3
      // BoardIdAdlSAdpSDdr5UDimm1DCrb has NO PEG2 slot. Set Pwr En Polarity
      // to zero intentionally here, so that Adl-s RVP RTD3 code can skip PEG2 handling
      PcdSetBoolS (PcdPegSlot2PwrEnableGpioPolarity, 0);

      // CPU M.2 SSD Slot RTD3
      PcdSet32S (PcdPcieSsd2PwrEnableGpioNo, GPIO_VER4_S_GPP_H16);
      PcdSetBoolS (PcdPcieSsd2PwrEnableGpioPolarity, 1);
      PcdSet32S (PcdPcieSsd2RstGpioNo, GPIO_VER4_S_GPP_F18);
      PcdSetBoolS (PcdPcieSsd2RstGpioPolarity, 0);

      // PCIE SLOT 1 - X4 CONNECTOR RTD3
      PcdSet32S (PcdPcieSlot1PwrEnableGpioNo, GPIO_VER4_S_GPP_E1);
      PcdSetBoolS (PcdPcieSlot1PwrEnableGpioPolarity, 1);
      PcdSet32S (PcdPcieSlot1HoldRstGpioNo, GPIO_VER4_S_GPP_F11);
      PcdSetBoolS (PcdPcieSlot1HoldRstGpioPolarity, 0);
      PcdSet32S (PcdPcieSlot1WakeGpioPin, GPIO_VER4_S_GPP_F0);
      PcdSet8S (PcdPcieSlot1RootPort, 21);

      // PCIE SLOT 2 - X4 CONNECTOR RTD3
      PcdSet32S (PcdPchPCIeSlot2PwrEnableGpioNo, GPIO_VER4_S_GPP_H11);
      PcdSetBoolS (PcdPchPCIeSlot2PwrEnableGpioPolarity, 1);
      PcdSet32S (PcdPchPCIeSlot2RstGpioNo, GPIO_VER4_S_GPP_F12);
      PcdSetBoolS (PcdPchPCIeSlot2RstGpioPolarity, 0);
      PcdSet32S (PcdPcieSlot2WakeGpioPin, GPIO_VER4_S_GPP_F1);
      PcdSet8S (PcdPcieSlot2RootPort, 9);

      // PCIE SLOT 3 - X2 CONNECTOR RTD3
      PcdSet32S (PcdPchPCIeSlot3PwrEnableGpioNo, GPIO_VER4_S_GPP_B21);
      PcdSetBoolS (PcdPchPCIeSlot3PwrEnableGpioPolarity, 1);
      PcdSet32S (PcdPchPCIeSlot3RstGpioNo, GPIO_VER4_S_GPP_F13);
      PcdSetBoolS (PcdPchPCIeSlot3RstGpioPolarity, 0);
      PcdSet32S (PcdPcieSlot3WakeGpioPin, GPIO_VER4_S_GPP_F2);
      PcdSet8S (PcdPcieSlot3RootPort, 5);

      // PCH M.2 SSD Slot 1 RTD3
      PcdSet32S (PcdPchSsd1PwrEnableGpioNo, GPIO_VER4_S_GPP_K11);
      PcdSetBoolS (PcdPchSsd1PwrEnableGpioPolarity, 1);
      PcdSet32S (PcdPchSsd1RstGpioNo, GPIO_VER4_S_GPP_C10);
      PcdSetBoolS (PcdPchSsd1RstGpioPolarity, 0);

      // Onboard MR 1 RTD3  (Onboard maple Ridge used for ADL S8 only)
      PcdSet32S (PcdOnBoardMr1PowerEnableGpioNo, GPIO_VER4_S_GPP_F5);
      PcdSetBoolS (PcdOnBoardMr1PowerEnableGpioPolarity, 1);
      PcdSet32S (PcdOnBoardMr1RstGpioNo, GPIO_VER4_S_GPP_F16);
      PcdSetBoolS (PcdOnBoardMr1RstGpioPolarity, 0);
      PcdSet32S (PcdOnBoardMr1WakeGpioPin, GPIO_VER4_S_GPP_F8);
      PcdSet8S (PcdOnBoardMr1RootPort, 25);

      // PCH M.2 SSD Slot 3 RTD3
      PcdSet32S (PcdPchSsd3PwrEnableGpioNo, GPIO_VER4_S_GPP_K2);
      PcdSetBoolS (PcdPchSsd3PwrEnableGpioPolarity, 1);
      PcdSet32S (PcdPchSsd3RstGpioNo, GPIO_VER4_S_GPP_B6);
      PcdSetBoolS (PcdPchSsd3RstGpioPolarity, 0);

      //  PCH SATA port RTD3
      PcdSet32S (PcdPchSataPortPwrEnableGpioNo, GPIO_VER4_S_GPP_R11);
      PcdSetBoolS (PcdPchSataPortPwrEnableGpioPolarity, 1);

      // Foxville I225 LAN
      //
      PcdSet32S (PcdFoxLanEnableGpio, GPIO_VER4_S_GPP_R20);             // Foxville I225 LAN Enable Pin
      PcdSet32S (PcdFoxLanWakeGpio,   GPIO_VER4_S_GPD2);                // Foxville I225 LAN Wake Pin
      PcdSet32S (PcdFoxLanDisableNGpio,  GPIO_VER4_S_GPD1);             // Foxville I225 LAN Power Pin
      PcdSet32S (PcdFoxLanResetGpio,  GPIO_VER4_S_GPP_G1);              // Foxville I225 LAN Reset Pin
      PcdSetBoolS (PcdFoxLanDisableNGpioPolarity, PIN_GPIO_ACTIVE_HIGH);// Foxville I225 power gpio pin polarity
      PcdSetBoolS (PcdFoxLanResetGpioPolarity, PIN_GPIO_ACTIVE_LOW);    // Foxville I225 reset gpio pin polarity
      PcdSet8S (PcdFoxlanRootPortNumber, 7);                            // Foxville I225 PCIe root port number

      // WLAN
      PcdSet32S (PcdWlanWakeGpio, GPIO_VER4_S_GPP_H2);
      PcdSet8S (PcdWlanRootPortNumber, 8);
      break;

    case BoardIdAdlSAdpSDdr5UDimm1DCrb:
    case BoardIdAdlSAdpSDdr5UDimm1DCrbPpv:
    case BoardIdAdlSAdpSDdr5UDimm2DCrb:
    case BoardIdAdlSAdpSDdr5UDimm1DOc:
    case BoardIdAdlSAdpSDdr5UDimm1DSr:
      // CPU PEG Slot1 RTD3
      if ((FabId < FabIdAdlSAdpSDdr5UDimm1DCrbRev1) &&
          (BoardId != BoardIdAdlSAdpSDdr5UDimm2DCrb) &&
          (BoardId != BoardIdAdlSAdpSDdr5UDimm1DOc) &&
          (BoardId != BoardIdAdlSAdpSDdr5UDimm1DSr)) {
          PcdSet32S (PcdPegSlot1PwrEnableGpioNo, GPIO_VER4_S_GPP_E0);
      } else {
//[-start-210127-IB05660155-modify]//
          //PcdSet32S (PcdPegSlot1PwrEnableGpioNo, GPIO_VER4_S_GPP_F4);
          PcdSet32S (PcdPegSlot1PwrEnableGpioNo, GPIO_VER4_S_GPP_E0);
//[-end-210127-IB05660155-modify]//
      }
      PcdSetBoolS (PcdPegSlot1PwrEnableGpioPolarity, 1);
      PcdSet32S (PcdPegSlot1RstGpioNo, GPIO_VER4_S_GPP_E2);
      PcdSetBoolS (PcdPegSlot1RstGpioPolarity, 0);
      PcdSet32S (PcdPegSlot1WakeGpioPin, GPIO_VER4_S_GPP_E6);
      //PcdSetBoolS (PcdPegSlot1RootPort, value);

      // CPU PEG Slot2 RTD3
      // BoardIdAdlSAdpSDdr5UDimm1DCrb has NO PEG2 slot. Set Pwr En Polarity
      // to zero intentionally here, so that Adl-s RVP RTD3 code can skip PEG2 handling
      PcdSetBoolS (PcdPegSlot2PwrEnableGpioPolarity, 0);

      // CPU M.2 SSD Slot RTD3
      PcdSet32S (PcdPcieSsd2PwrEnableGpioNo, GPIO_VER4_S_GPP_H16);
      PcdSetBoolS (PcdPcieSsd2PwrEnableGpioPolarity, 1);
      PcdSet32S (PcdPcieSsd2RstGpioNo, GPIO_VER4_S_GPP_F18);
      PcdSetBoolS (PcdPcieSsd2RstGpioPolarity, 0);

      // PCIE SLOT 1 - X4 CONNECTOR RTD3
      PcdSet32S (PcdPcieSlot1PwrEnableGpioNo, GPIO_VER4_S_GPP_E1);
      PcdSetBoolS (PcdPcieSlot1PwrEnableGpioPolarity, 1);
      PcdSet32S (PcdPcieSlot1HoldRstGpioNo, GPIO_VER4_S_GPP_F11);
      PcdSetBoolS (PcdPcieSlot1HoldRstGpioPolarity, 0);
      PcdSet32S (PcdPcieSlot1WakeGpioPin, GPIO_VER4_S_GPP_F0);
      PcdSet8S (PcdPcieSlot1RootPort, 21);

      // PCIE SLOT 2 - X4 CONNECTOR RTD3
      PcdSet32S (PcdPchPCIeSlot2PwrEnableGpioNo, GPIO_VER4_S_GPP_H11);
      PcdSetBoolS (PcdPchPCIeSlot2PwrEnableGpioPolarity, 1);
      PcdSet32S (PcdPchPCIeSlot2RstGpioNo, GPIO_VER4_S_GPP_F12);
      PcdSetBoolS (PcdPchPCIeSlot2RstGpioPolarity, 0);
      PcdSet32S (PcdPcieSlot2WakeGpioPin, GPIO_VER4_S_GPP_F1);
      PcdSet8S (PcdPcieSlot2RootPort, 9);

      // PCIE SLOT 3 - X2 CONNECTOR RTD3
      PcdSet32S (PcdPchPCIeSlot3PwrEnableGpioNo, GPIO_VER4_S_GPP_B21);
      PcdSetBoolS (PcdPchPCIeSlot3PwrEnableGpioPolarity, 1);
      PcdSet32S (PcdPchPCIeSlot3RstGpioNo, GPIO_VER4_S_GPP_F13);
      PcdSetBoolS (PcdPchPCIeSlot3RstGpioPolarity, 0);
      PcdSet32S (PcdPcieSlot3WakeGpioPin, GPIO_VER4_S_GPP_F2);
      PcdSet8S (PcdPcieSlot3RootPort, 5);

      // PCH M.2 SSD Slot 1 RTD3
      PcdSet32S (PcdPchSsd1PwrEnableGpioNo, GPIO_VER4_S_GPP_K11);
      PcdSetBoolS (PcdPchSsd1PwrEnableGpioPolarity, 1);
      PcdSet32S (PcdPchSsd1RstGpioNo, GPIO_VER4_S_GPP_C10);
      PcdSetBoolS (PcdPchSsd1RstGpioPolarity, 0);

      // PCH M.2 SSD Slot 2 RTD3
      PcdSet32S (PcdPchSsd2PwrEnableGpioNo, GPIO_VER4_S_GPP_B22);
      PcdSetBoolS (PcdPchSsd2PwrEnableGpioPolarity, 1);
      PcdSet32S (PcdPchSsd2RstGpioNo, GPIO_VER4_S_GPP_F16);
      PcdSetBoolS (PcdPchSsd2RstGpioPolarity, 0);

      // PCH M.2 SSD Slot 3 RTD3
      PcdSet32S (PcdPchSsd3PwrEnableGpioNo, GPIO_VER4_S_GPP_K2);
      PcdSetBoolS (PcdPchSsd3PwrEnableGpioPolarity, 1);
      PcdSet32S (PcdPchSsd3RstGpioNo, GPIO_VER4_S_GPP_B6);
      PcdSetBoolS (PcdPchSsd3RstGpioPolarity, 0);

      //  PCH SATA port RTD3
      if (BoardId != BoardIdAdlSAdpSDdr5UDimm1DOc) {
        PcdSet32S (PcdPchSataPortPwrEnableGpioNo, GPIO_VER4_S_GPP_R11);
        PcdSetBoolS (PcdPchSataPortPwrEnableGpioPolarity, 1);
      }
      // WLAN
      PcdSet32S (PcdWlanWakeGpio, GPIO_VER4_S_GPP_H2);
      PcdSet8S (PcdWlanRootPortNumber, 8);

      //
      // UCMC
      //
      PcdSet8S (PcdUsbTypeCEcLess, 1);  // POR setting. PD vender 0 = disable , 1 = TI , 2 = Cypress
      PcdSet32S (PcdBoardUcmcGpio1, GPIO_VER4_S_GPP_H1);
      break;
    case BoardIdAdlSAdpSDdr4UDimm2DCrb:
    case BoardIdAdlSAdpSDdr4UDimm2DCrbEv:
    case BoardIdAdlSAdpSDdr4UDimm2DCrbCpv:
      // CPU PEG Slot1 RTD3
      PcdSet32S (PcdPegSlot1PwrEnableGpioNo, GPIO_VER4_S_GPP_E0);
      PcdSetBoolS (PcdPegSlot1PwrEnableGpioPolarity, 1);
      PcdSet32S (PcdPegSlot1RstGpioNo, GPIO_VER4_S_GPP_E2);
      PcdSetBoolS (PcdPegSlot1RstGpioPolarity, 0);
      PcdSet32S (PcdPegSlot1WakeGpioPin, GPIO_VER4_S_GPP_E6);
      //PcdSetBoolS (PcdPegSlot1RootPort, value);

      // CPU PEG Slot2 RTD3
      PcdSet32S (PcdPegSlot2PwrEnableGpioNo, GPIO_VER4_S_GPP_E1);
      PcdSetBoolS (PcdPegSlot2PwrEnableGpioPolarity, 1);
      PcdSet32S (PcdPegSlot2RstGpioNo, GPIO_VER4_S_GPP_E3);
      PcdSetBoolS (PcdPegSlot2RstGpioPolarity, 0);
      PcdSet32S (PcdPegSlot2WakeGpioPin, GPIO_VER4_S_GPP_E7);
      //PcdSetBoolS (PcdPegSlot2RootPort, value);

      // CPU M.2 SSD Slot RTD3
      PcdSet32S (PcdPcieSsd2PwrEnableGpioNo, GPIO_VER4_S_GPP_H16);
      PcdSetBoolS (PcdPcieSsd2PwrEnableGpioPolarity, 1);
      PcdSet32S (PcdPcieSsd2RstGpioNo, GPIO_VER4_S_GPP_F18);
      PcdSetBoolS (PcdPcieSsd2RstGpioPolarity, 0);

      // PCIE SLOT 1 - X4 CONNECTOR RTD3
      if ( BoardId == BoardIdAdlSAdpSDdr4UDimm2DCrbCpv) {
        PcdSet32S (PcdPcieSlot1PwrEnableGpioNo, GPIO_VER4_S_GPP_F22);
      } else {
        if (FabId < FabIdAdlSAdpSDdr4UDimm2DCrbRev1) {
          PcdSet32S (PcdPcieSlot1PwrEnableGpioNo, GPIO_VER4_S_GPP_A14);
        } else {
          PcdSet32S (PcdPcieSlot1PwrEnableGpioNo, GPIO_VER4_S_GPP_F22);
        }
      }
      PcdSetBoolS (PcdPcieSlot1PwrEnableGpioPolarity, 1);
      PcdSet32S (PcdPcieSlot1HoldRstGpioNo, GPIO_VER4_S_GPP_F11);
      PcdSetBoolS (PcdPcieSlot1HoldRstGpioPolarity, 0);
      PcdSet32S (PcdPcieSlot1WakeGpioPin, GPIO_VER4_S_GPP_F0);
      PcdSet8S (PcdPcieSlot1RootPort, 21);

      // PCIE SLOT 2 - X4 CONNECTOR RTD3
      PcdSet32S (PcdPchPCIeSlot2PwrEnableGpioNo, GPIO_VER4_S_GPP_H11);
      PcdSetBoolS (PcdPchPCIeSlot2PwrEnableGpioPolarity, 1);
      PcdSet32S (PcdPchPCIeSlot2RstGpioNo, GPIO_VER4_S_GPP_F12);
      PcdSetBoolS (PcdPchPCIeSlot2RstGpioPolarity, 0);
      PcdSet32S (PcdPcieSlot2WakeGpioPin, GPIO_VER4_S_GPP_F1);
      PcdSet8S (PcdPcieSlot2RootPort, 9);

      // PCIE SLOT 3 - X2 CONNECTOR RTD3
      PcdSet32S (PcdPchPCIeSlot3PwrEnableGpioNo, GPIO_VER4_S_GPP_B21);
      PcdSetBoolS (PcdPchPCIeSlot3PwrEnableGpioPolarity, 1);
      PcdSet32S (PcdPchPCIeSlot3RstGpioNo, GPIO_VER4_S_GPP_F13);
      PcdSetBoolS (PcdPchPCIeSlot3RstGpioPolarity, 0);
      PcdSet32S (PcdPcieSlot3WakeGpioPin, GPIO_VER4_S_GPP_F2);
      PcdSet8S (PcdPcieSlot3RootPort, 5);

      // PCH M.2 SSD Slot 1 RTD3
      PcdSet32S (PcdPchSsd1PwrEnableGpioNo, GPIO_VER4_S_GPP_C2);
      PcdSetBoolS (PcdPchSsd1PwrEnableGpioPolarity, 1);
      PcdSet32S (PcdPchSsd1RstGpioNo, GPIO_VER4_S_GPP_C10);
      PcdSetBoolS (PcdPchSsd1RstGpioPolarity, 0);

      // PCH M.2 SSD Slot 2 RTD3
      PcdSet32S (PcdPchSsd2PwrEnableGpioNo, GPIO_VER4_S_GPP_B22);
      PcdSetBoolS (PcdPchSsd2PwrEnableGpioPolarity, 1);
      PcdSet32S (PcdPchSsd2RstGpioNo, GPIO_VER4_S_GPP_F16);
      PcdSetBoolS (PcdPchSsd2RstGpioPolarity, 0);

      // PCH M.2 SSD Slot 3 RTD3
      PcdSet32S (PcdPchSsd3PwrEnableGpioNo, GPIO_VER4_S_GPP_K2);
      PcdSetBoolS (PcdPchSsd3PwrEnableGpioPolarity, 1);
      PcdSet32S (PcdPchSsd3RstGpioNo, GPIO_VER4_S_GPP_B6);
      PcdSetBoolS (PcdPchSsd3RstGpioPolarity, 0);

      //  PCH SATA port RTD3
      PcdSet32S (PcdPchSataPortPwrEnableGpioNo, GPIO_VER4_S_GPP_R11);
      PcdSetBoolS (PcdPchSataPortPwrEnableGpioPolarity, 1);

      // WLAN
      PcdSet32S (PcdWlanWakeGpio, GPIO_VER4_S_GPP_H2);
      PcdSet8S (PcdWlanRootPortNumber, 8);

      //
      // UCMC
      //
      PcdSet8S (PcdUsbTypeCEcLess, 1);  // POR setting. PD vender 0 = disable , 1 = TI , 2 = Cypress
      PcdSet32S (PcdBoardUcmcGpio1, GPIO_VER4_S_GPP_H1);
      break;

    case BoardIdAdlSAdpSSbgaDdr5SODimmErb:
    case BoardIdAdlSAdpSSbgaDdr5SODimmCrb:
      // CPU PEG Slot1 RTD3
      PcdSet32S (PcdPegSlot1PwrEnableGpioNo, GPIO_VER4_S_GPP_K3);
      PcdSetBoolS (PcdPegSlot1PwrEnableGpioPolarity, 0);
      PcdSet32S (PcdPegSlot1RstGpioNo, GPIO_VER4_S_GPP_E2);
      PcdSetBoolS (PcdPegSlot1RstGpioPolarity, 0);
      PcdSet32S (PcdPegSlot1WakeGpioPin, GPIO_VER4_S_GPP_E6);
      //PcdSetBoolS (PcdPegSlot1RootPort, value);

      // CPU M.2 SSD Slot RTD3
      PcdSet32S (PcdPcieSsd2PwrEnableGpioNo, GPIO_VER4_S_GPP_H16);
      PcdSetBoolS (PcdPcieSsd2PwrEnableGpioPolarity, 1);
      PcdSet32S (PcdPcieSsd2RstGpioNo, GPIO_VER4_S_GPP_F18);
      PcdSetBoolS (PcdPcieSsd2RstGpioPolarity, 0);

      // PCIE SLOT 1 - X4 CONNECTOR RTD3
      PcdSet32S (PcdPcieSlot1PwrEnableGpioNo, GPIO_VER4_S_GPP_B6);
      PcdSetBoolS (PcdPcieSlot1PwrEnableGpioPolarity, 1);
      PcdSet32S (PcdPcieSlot1HoldRstGpioNo, GPIO_VER4_S_GPP_E17);
      PcdSetBoolS (PcdPcieSlot1HoldRstGpioPolarity, 0);
      PcdSet32S (PcdPcieSlot1WakeGpioPin, GPIO_VER4_S_GPP_E16);
      PcdSet8S (PcdPcieSlot1RootPort, 13);

      // PCIE SLOT 2 - Mapple ridge Thunderbolt Controller 0
      PcdSet32S (PcdPchPCIeSlot2PwrEnableGpioNo, GPIO_VER4_S_GPP_F5);
      PcdSetBoolS (PcdPchPCIeSlot2PwrEnableGpioPolarity, 1);
      PcdSet32S (PcdPchPCIeSlot2RstGpioNo, GPIO_VER4_S_GPP_F16);
      PcdSetBoolS (PcdPchPCIeSlot2RstGpioPolarity, 0);
      PcdSet32S (PcdPcieSlot2WakeGpioPin, GPIO_VER4_S_GPP_F9);
      PcdSet8S (PcdPcieSlot2RootPort, 25);

      // PCIE SLOT 3 - Mapple ridge Thunderbolt Controller 1
      PcdSet32S (PcdPchPCIeSlot3PwrEnableGpioNo, GPIO_VER4_S_GPP_E7);
      PcdSetBoolS (PcdPchPCIeSlot3PwrEnableGpioPolarity, 1);
      PcdSet32S (PcdPchPCIeSlot3RstGpioNo, GPIO_VER4_S_GPP_F2);
      PcdSetBoolS (PcdPchPCIeSlot3RstGpioPolarity, 0);
      PcdSet32S (PcdPcieSlot3WakeGpioPin, GPIO_VER4_S_GPP_F13);
      PcdSet8S (PcdPcieSlot3RootPort, 9);

      // PCH M.2 SSD Slot 1 RTD3
      PcdSet32S (PcdPchSsd1PwrEnableGpioNo, GPIO_VER4_S_GPP_K11);
      PcdSetBoolS (PcdPchSsd1PwrEnableGpioPolarity, 1);
      PcdSet32S (PcdPchSsd1RstGpioNo, GPIO_VER4_S_GPP_C10);
      PcdSetBoolS (PcdPchSsd1RstGpioPolarity, 0);

      //  PCH SATA port RTD3
      PcdSet32S (PcdPchSataPortPwrEnableGpioNo, GPIO_VER4_S_GPP_R11);
      PcdSetBoolS (PcdPchSataPortPwrEnableGpioPolarity, 1);

      // WLAN
      PcdSet32S (PcdWlanWakeGpio, GPIO_VER4_S_GPP_H2);
      PcdSet8S (PcdWlanRootPortNumber, 8);

      //
      // UCMC
      //
      PcdSet8S (PcdUsbTypeCEcLess, 1);  // POR setting. PD vender 0 = disable , 1 = TI , 2 = Cypress
      PcdSet32S (PcdBoardUcmcGpio1, GPIO_VER4_S_GPP_H1);

      // Foxville I225 LAN
      //
      PcdSet32S (PcdFoxLanEnableGpio, GPIO_VER4_S_GPP_R20);             // Foxville I225 LAN Enable Pin
      PcdSet32S (PcdFoxLanWakeGpio,   GPIO_VER4_S_GPD2);                // Foxville I225 LAN Wake Pin
      PcdSet32S (PcdFoxLanDisableNGpio,  GPIO_VER4_S_GPP_B21);          // Foxville I225 LAN Power Pin
      PcdSet32S (PcdFoxLanResetGpio,  GPIO_VER4_S_GPP_G1);              // Foxville I225 LAN Reset Pin
      PcdSetBoolS (PcdFoxLanDisableNGpioPolarity, PIN_GPIO_ACTIVE_HIGH);// Foxville I225 power gpio pin polarity
      PcdSetBoolS (PcdFoxLanResetGpioPolarity, PIN_GPIO_ACTIVE_LOW);    // Foxville I225 reset gpio pin polarity
      PcdSet8S (PcdFoxlanRootPortNumber, 7);                            // Foxville I225 PCIe root port number

      // DashG
      PcdSetBoolS (PcdDashGEnable, TRUE);
      break;

    case BoardIdAdlSAdpSSbgaDdr4SODimmCrb:
      // CPU PEG Slot1 RTD3
      PcdSet32S (PcdPegSlot1PwrEnableGpioNo, GPIO_VER4_S_GPP_F18);
      PcdSetBoolS (PcdPegSlot1PwrEnableGpioPolarity, 0);
      PcdSet32S (PcdPegSlot1RstGpioNo, GPIO_VER4_S_GPP_E2);
      PcdSetBoolS (PcdPegSlot1RstGpioPolarity, 0);
      PcdSet32S (PcdPegSlot1WakeGpioPin, GPIO_VER4_S_GPP_E6);
      //PcdSetBoolS (PcdPegSlot1RootPort, value);

      // CPU M.2 SSD Slot RTD3
      PcdSet32S (PcdPcieSsd2PwrEnableGpioNo, GPIO_VER4_S_GPP_H16);
      PcdSetBoolS (PcdPcieSsd2PwrEnableGpioPolarity, 1);
      PcdSet32S (PcdPcieSsd2RstGpioNo, GPIO_VER4_S_GPP_F11);
      PcdSetBoolS (PcdPcieSsd2RstGpioPolarity, 0);

      // PCIE SLOT 1 - X4 CONNECTOR RTD3
      PcdSet32S (PcdPcieSlot1PwrEnableGpioNo, GPIO_VER4_S_GPP_E1);
      PcdSetBoolS (PcdPcieSlot1PwrEnableGpioPolarity, 1);
      PcdSet32S (PcdPcieSlot1HoldRstGpioNo, GPIO_VER4_S_GPP_F22);
      PcdSetBoolS (PcdPcieSlot1HoldRstGpioPolarity, 0);
      PcdSet32S (PcdPcieSlot1WakeGpioPin, GPIO_VER4_S_GPP_F23);
      PcdSet8S (PcdPcieSlot1RootPort, 13);

      // PCIE SLOT 2 - Mapple ridge Thunderbolt Controller 0
      PcdSet32S (PcdPchPCIeSlot2PwrEnableGpioNo, GPIO_VER4_S_GPP_F5);
      PcdSetBoolS (PcdPchPCIeSlot2PwrEnableGpioPolarity, 1);
      PcdSet32S (PcdPchPCIeSlot2RstGpioNo, GPIO_VER4_S_GPP_F16);
      PcdSetBoolS (PcdPchPCIeSlot2RstGpioPolarity, 0);
      PcdSet32S (PcdPcieSlot2WakeGpioPin, GPIO_VER4_S_GPP_E4);
      PcdSet8S (PcdPcieSlot2RootPort, 25);

      // PCH M.2 SSD Slot 1 RTD3
      PcdSet32S (PcdPchSsd1PwrEnableGpioNo, GPIO_VER4_S_GPP_K11);
      PcdSetBoolS (PcdPchSsd1PwrEnableGpioPolarity, 1);
      PcdSet32S (PcdPchSsd1RstGpioNo, GPIO_VER4_S_GPP_C10);
      PcdSetBoolS (PcdPchSsd1RstGpioPolarity, 0);

      //  PCH SATA port RTD3
      PcdSet32S (PcdPchSataPortPwrEnableGpioNo, GPIO_VER4_S_GPP_R11);
      PcdSetBoolS (PcdPchSataPortPwrEnableGpioPolarity, 1);

      // WLAN
      PcdSet32S (PcdWlanWakeGpio, GPIO_VER4_S_GPP_H2);
      PcdSet8S (PcdWlanRootPortNumber, 8);

      //
      // UCMC
      //
      PcdSet8S (PcdUsbTypeCEcLess, 1);  // POR setting. PD vender 0 = disable , 1 = TI , 2 = Cypress
      PcdSet32S (PcdBoardUcmcGpio1, GPIO_VER4_S_GPP_H1);

      // Foxville I225 LAN
      //
      PcdSet32S (PcdFoxLanEnableGpio, GPIO_VER4_S_GPP_R20);             // Foxville I225 LAN Enable Pin
      PcdSet32S (PcdFoxLanWakeGpio,   GPIO_VER4_S_GPD2);                // Foxville I225 LAN Wake Pin
      PcdSet32S (PcdFoxLanDisableNGpio,  GPIO_VER4_S_GPD1);             // Foxville I225 LAN Power Pin
      PcdSet32S (PcdFoxLanResetGpio,  GPIO_VER4_S_GPP_G1);              // Foxville I225 LAN Reset Pin
      PcdSetBoolS (PcdFoxLanDisableNGpioPolarity, PIN_GPIO_ACTIVE_HIGH);// Foxville I225 power gpio pin polarity
      PcdSetBoolS (PcdFoxLanResetGpioPolarity, PIN_GPIO_ACTIVE_LOW);    // Foxville I225 reset gpio pin polarity
      PcdSet8S (PcdFoxlanRootPortNumber, 7);                            // Foxville I225 PCIe root port number

      // eDP Display Mux GPIO
      PcdSet32S (PcdDisplayMuxGpioNo, GPIO_VER4_S_GPP_F2);

      // DashG
      PcdSetBoolS (PcdDashGEnable, TRUE);
      break;

    case BoardIdAdlSAdpSDdr5UDimm1DAep:
      // CPU PEG Slot RTD3
      if ((FabId < FabIdAdlSAdpSDdr5UDimm1DCrbRev1)) {
        PcdSet32S (PcdPegSlot1PwrEnableGpioNo, GPIO_VER4_S_GPP_E0);                 // CPU PEG Power Enable Gpio pin
      } else {
        PcdSet32S (PcdPegSlot1PwrEnableGpioNo, GPIO_VER4_S_GPP_E3);                 // CPU PEG Power Enable Gpio pin
      }
      PcdSet32S (PcdPegSlot1RstGpioNo, GPIO_VER4_S_GPP_E2);                       // CPU PEG Reset Gpio pin
      PcdSet32S (PcdPegSlot1WakeGpioPin, GPIO_VER4_S_GPP_E6);                     // CPU PEG Wake Gpio pin
      PcdSetBoolS (PcdPegSlot1PwrEnableGpioPolarity, PIN_GPIO_ACTIVE_HIGH);       // CPU PEG Power Enable Gpio pin polarity
      PcdSetBoolS (PcdPegSlot1RstGpioPolarity, PIN_GPIO_ACTIVE_LOW);             // CPU PEG Reset Gpio pin polarity

      // CPU M.2 SSD Slot RTD3
      PcdSet32S (PcdPcieSsd2PwrEnableGpioNo, GPIO_VER4_S_GPP_H16);                // CPU SSD Power Enable Gpio pin
      PcdSet32S (PcdPcieSsd2RstGpioNo, GPIO_VER4_S_GPP_F18);                      // CPU SSD Reset Gpio pin
      PcdSetBoolS (PcdPcieSsd2PwrEnableGpioPolarity, PIN_GPIO_ACTIVE_HIGH);       // CPU SSD Power Enable Gpio pin polarity
      PcdSetBoolS (PcdPcieSsd2RstGpioPolarity, PIN_GPIO_ACTIVE_LOW);              // CPU SSD Rest Gpio pin polarity

      // Maple Ridge 1 (Back Panel) Slot 1
      PcdSet32S (PcdPcieSlot1PwrEnableGpioNo, GPIO_VER4_S_GPP_G1);                // Maple Ridge BP Power Enable Gpio pin
      PcdSetBoolS (PcdPcieSlot1PwrEnableGpioPolarity, PIN_GPIO_ACTIVE_HIGH);      // Maple Ridge BP Power Enable Gpio pin polarity
      PcdSet32S (PcdPcieSlot1HoldRstGpioNo, GPIO_VER4_S_GPP_K4);                  // Maple Ridge BP Reset Gpio pin
      PcdSetBoolS (PcdPcieSlot1HoldRstGpioPolarity, PIN_GPIO_ACTIVE_LOW);         // Maple Ridge BP Reset Gpio pin polarity
      PcdSet32S (PcdPcieSlot1WakeGpioPin, GPIO_VER4_S_GPP_G3);                    // Maple Ridge BP Wake Gpio Pin
      PcdSet8S (PcdPcieSlot1RootPort, 21);                                        // Maple Ridge BP Root Port number

      // Maple Ridge 2 (Front Panel) Slot 2
      PcdSet32S (PcdPchPCIeSlot2PwrEnableGpioNo, GPIO_VER4_S_GPP_D11);            // Maple Ridge FP Power Enable Gpio pin
      PcdSetBoolS (PcdPchPCIeSlot2PwrEnableGpioPolarity, PIN_GPIO_ACTIVE_HIGH);   // Maple Ridge FP Power Enable Gpio pin polarity
      PcdSet32S (PcdPchPCIeSlot2RstGpioNo, GPIO_VER4_S_GPP_K3);                   // Maple Ridge FP Reset Gpio pin
      PcdSetBoolS (PcdPchPCIeSlot2RstGpioPolarity, PIN_GPIO_ACTIVE_LOW);          // Maple Ridge FP Reset Gpio pin polarity
      PcdSet32S (PcdPcieSlot2WakeGpioPin, GPIO_VER4_S_GPP_G4);                    // Maple Ridge FP Wake Gpio Pin
      PcdSet8S (PcdPcieSlot2RootPort, 25);                                        // Maple Ridge FP Root Port number

      //
      // Foxville I225 LAN
      //
      PcdSet32S (PcdFoxLanEnableGpio, GPIO_VER4_S_GPP_R20);                       // Foxville I225 LAN Enable Pin
      PcdSet32S (PcdFoxLanWakeGpio, GPIO_VER4_S_GPP_C8);                          // Foxville I225 LAN Wake Pin
      PcdSet32S (PcdFoxLanDisableNGpio, GPIO_VER4_S_GPP_F3);                      // Foxville I225 LAN Power Pin
      PcdSet32S (PcdFoxLanResetGpio, GPIO_VER4_S_GPP_C9);                         // Foxville I225 LAN Reset Pin
      PcdSetBoolS (PcdFoxLanDisableNGpioPolarity, PIN_GPIO_ACTIVE_HIGH);          // Foxville I225 power gpio pin polarity
      PcdSetBoolS (PcdFoxLanResetGpioPolarity, PIN_GPIO_ACTIVE_LOW);              // Foxville I225 reset gpio pin polarity
      PcdSet8S (PcdFoxlanRootPortNumber, 7);                                      // Foxville I225 PCIe root port number

      // No RTD3 Support for EDSFF Slot 3

      // PCH M.2 SSD1
      PcdSet32S (PcdPchSsd1PwrEnableGpioNo, GPIO_VER4_S_GPP_C2);                  // PCH M.2 SSD Power Enable Gpio pin
      PcdSet32S (PcdPchSsd1RstGpioNo, GPIO_VER4_S_GPP_C10);                       // PCH M.2 SSD Reset Gpio pin
      PcdSetBoolS (PcdPchSsd1PwrEnableGpioPolarity, PIN_GPIO_ACTIVE_HIGH);        // PCH M.2 SSD Power Enable Gpio pin polarity
      PcdSetBoolS (PcdPchSsd1RstGpioPolarity, PIN_GPIO_ACTIVE_LOW);               // PCH M.2 SSD Reset Gpio pin polarity

      // WLAN (Wired LAN M.2)
      PcdSet32S (PcdWlanWakeGpio, GPIO_VER4_S_GPP_H2);                            // WLAN Wake Gpio pin
      PcdSet8S (PcdWlanRootPortNumber, 8);                                        // WLAN Root Port number

      //
      // UCMC
      //
      PcdSet8S (PcdUsbTypeCEcLess, 1);  // POR setting. PD vender 0 = disable , 1 = TI , 2 = Cypress
      PcdSet32S (PcdBoardUcmcGpio1, GPIO_VER4_S_GPP_H1);
      break;

    case BoardIdAdlSAdpSSbgaDdr5SODimmAep:
      // CPU PEG Slot1 RTD3
      PcdSet32S (PcdPegSlot1PwrEnableGpioNo, GPIO_VER4_S_GPP_K3);
      PcdSetBoolS (PcdPegSlot1PwrEnableGpioPolarity, PIN_GPIO_ACTIVE_HIGH);
      PcdSet32S (PcdPegSlot1RstGpioNo, GPIO_VER4_S_GPP_E2);
      PcdSetBoolS (PcdPegSlot1RstGpioPolarity, PIN_GPIO_ACTIVE_LOW);
      PcdSet32S (PcdPegSlot1WakeGpioPin, GPIO_VER4_S_GPP_E6);

      // PCIE SLOT 2 - Mapple ridge Thunderbolt Controller 0
      PcdSet32S (PcdPchPCIeSlot2PwrEnableGpioNo, GPIO_VER4_S_GPP_F5);
      PcdSetBoolS (PcdPchPCIeSlot2PwrEnableGpioPolarity, PIN_GPIO_ACTIVE_HIGH);
      PcdSet32S (PcdPchPCIeSlot2RstGpioNo, GPIO_VER4_S_GPP_F16);
      PcdSetBoolS (PcdPchPCIeSlot2RstGpioPolarity, PIN_GPIO_ACTIVE_LOW);
      PcdSet32S (PcdPcieSlot2WakeGpioPin, GPIO_VER4_S_GPP_F9);
      PcdSet8S (PcdPcieSlot2RootPort, 9);

      // PCH M.2 SSD Slot 1 RTD3
      PcdSet32S (PcdPchSsd1PwrEnableGpioNo, GPIO_VER4_S_GPP_H16);
      PcdSetBoolS (PcdPchSsd1PwrEnableGpioPolarity, PIN_GPIO_ACTIVE_HIGH);
      PcdSet32S (PcdPchSsd1RstGpioNo, GPIO_VER4_S_GPP_F18);
      PcdSetBoolS (PcdPchSsd1RstGpioPolarity, PIN_GPIO_ACTIVE_LOW);

      // PCH M.2 SSD Slot 2 RTD3
      PcdSet32S (PcdPchSsd2PwrEnableGpioNo, GPIO_VER4_S_GPP_K11);
      PcdSetBoolS (PcdPchSsd2PwrEnableGpioPolarity, PIN_GPIO_ACTIVE_HIGH);
      PcdSet32S (PcdPchSsd2RstGpioNo, GPIO_VER4_S_GPP_C10);
      PcdSetBoolS (PcdPchSsd2RstGpioPolarity, PIN_GPIO_ACTIVE_LOW);

      // Foxville I225 LAN
      //
      PcdSet32S (PcdFoxLanEnableGpio, GPIO_VER4_S_GPP_R20);             // Foxville I225 LAN Enable Pin
      PcdSet32S (PcdFoxLanWakeGpio,   GPIO_VER4_S_GPD2);                // Foxville I225 LAN Wake Pin
      PcdSet32S (PcdFoxLanDisableNGpio,  GPIO_VER4_S_GPP_B21);          // Foxville I225 LAN Power Pin
      PcdSet32S (PcdFoxLanResetGpio,  GPIO_VER4_S_GPP_G1);              // Foxville I225 LAN Reset Pin
      PcdSetBoolS (PcdFoxLanDisableNGpioPolarity, PIN_GPIO_ACTIVE_HIGH);// Foxville I225 power gpio pin polarity
      PcdSetBoolS (PcdFoxLanResetGpioPolarity, PIN_GPIO_ACTIVE_LOW);    // Foxville I225 reset gpio pin polarity
      PcdSet8S (PcdFoxlanRootPortNumber, 7);                            // Foxville I225 PCIe root port number

      // WLAN
      PcdSet32S (PcdWlanWakeGpio, GPIO_VER4_S_GPP_H2);
      PcdSet8S (PcdWlanRootPortNumber, 25);
      break;

    default:
      break;
  }

  return;
}

/**
  Return DTbtRtd3 element value of Setup variable
**/
UINT8
AdlSGetDTbtRtd3Setup (
  VOID
  )
{
#if FixedPcdGetBool (PcdSetupEnable) == 1
#if FixedPcdGetBool (PcdDTbtEnable) == 1
  EFI_PEI_READ_ONLY_VARIABLE2_PPI   *VariableServices;
  EFI_STATUS                        Status;
  SETUP_DATA                        Setup;
  UINTN                             DataSize;

  Status = PeiServicesLocatePpi (
             &gEfiPeiReadOnlyVariable2PpiGuid,
             0,
             NULL,
             (VOID **) &VariableServices
             );
  if (Status == EFI_SUCCESS) {
    DataSize = sizeof (SETUP_DATA);
    Status = VariableServices->GetVariable (
                                 VariableServices,
                                 L"Setup",
                                 &gSetupVariableGuid,
                                 NULL,
                                 &DataSize,
                                 &Setup
                                 );
    if (Status == EFI_SUCCESS) {
      DEBUG ((DEBUG_INFO, "AdlSGetDTbtRtd3Setup = %d\n", Setup.DTbtRtd3));
      return Setup.DTbtRtd3;
    }
  }
  return 0;
#else
  return 0;
#endif
#else
  return 0;
#endif
}

/**
  Tbt init function for PEI post memory phase.
**/
VOID
AdlSBoardTbtInit (
  VOID
  )
{
#if FixedPcdGetBool (PcdDTbtEnable) == 1
  UINT16     BoardId;
  BoardId =  PcdGet16 (PcdBoardId);

  switch (BoardId) {
    case BoardIdAdlSAdpSDdr5UDimm1DAep:
      PcdSet8S (PcdDTbtControllerNumber, 2);
      break;
    case BoardIdAdlSAdpSSbgaDdr5SODimmErb:
    case BoardIdAdlSAdpSSbgaDdr5SODimmCrb:
      PcdSet8S (PcdDTbtControllerNumber, 2);
      break;
    case BoardIdAdlSAdpSSbgaDdr5SODimmAep:
      PcdSet8S (PcdDTbtControllerNumber, 1);
      break;
    default:
      ///To do: Will need to program iTBT overcurrent here for ADL-P
      PcdSet8S (PcdDTbtControllerNumber, 1);
      break;
  }
#endif
}

/**
   Security GPIO init function for PEI post memory phase.
**/
VOID
AdlSBoardSecurityInit (
  VOID
  )
{
  GPIO_PAD  GpioPad;
  UINT32    GpioIrqNumber;
  UINT32    VerFtif;
  UINT16    BoardId;
  BoardId = PcdGet16 (PcdBoardId);

  if (FeaturePcdGet (PcdTpm2Enable) == TRUE) {
    //
    // If dTPM is connected to the SPI I/F, configure interrupt
    //
    VerFtif = MmioRead32 (R_PTT_TXT_STS_FTIF);

    if ((VerFtif & FTIF_FT_LOC_MASK) == V_FTIF_SPI_DTPM_PRESENT) {

      switch (BoardId) {
      case BoardIdAdlSAdpSDdr4UDimm2DCrb:
      case BoardIdAdlSAdpSDdr4UDimm2DCrbCpv:
      case BoardIdAdlSAdpSDdr5SODimmCrb:
      case BoardIdAdlSAdpSDdr4SODimmCrb:
      case BoardIdAdlSAdpSDdr5UDimm1DCrb:
      case BoardIdAdlSAdpSDdr5UDimm1DCrbPpv:
      case BoardIdAdlSAdpSDdr5UDimm1DAep:
      default:
        GpioPad = (GPIO_PAD) GPIO_VER4_S_GPP_F17;
        break;
      }

      GpioGetPadIoApicIrqNumber (GpioPad, &GpioIrqNumber);

      DEBUG ((DEBUG_INFO, "TPM Interrupt Number: %d\n", GpioIrqNumber));
      PcdSet32S (PcdTpm2CurrentIrqNum, GpioIrqNumber);
    } else {
      PcdSet32S (PcdTpm2CurrentIrqNum, 0);
    }
  }
}

/**
  Board Specific Init for PEI post memory phase.
**/
VOID
PeiAdlSBoardSpecificInitPostMemNull (
  VOID
  )
{
}

/**
  Board's PCD function hook init function for PEI post memory phase.
**/
VOID
AdlSBoardFunctionInit (
  VOID
)
{
  PcdSet32S (PcdFuncPeiBoardSpecificInitPostMem, (UINTN) PeiAdlSBoardSpecificInitPostMemNull);
}

/**
  PMC-PD solution enable init lib
**/
VOID
AdlSBoardPmcPdInit (
  VOID
  )
{
  PcdSetBoolS (PcdBoardPmcPdEnable, 1);
}

//[-start-201104-IB17510119-add]//
STATIC
EFI_STATUS
GpioCfgPei (
//[-start-210120-IB16560239-modify]//
  IN H2O_BOARD_ID               Board,
//[-end-210120-IB16560239-modify]//
  IN BOOLEAN                    PreMemFlag
  )
{
  EFI_STATUS                    Status;
  H2O_GPIO_INIT_CONFIG          *GpioInitStruct;
  UINT32                        GpioInitStructCount;
  UINT32                        GpioTableCount;
  GPIO_INIT_CONFIG              *GpioDefinitionTable;
  H2O_GPIO_INIT_CONFIG          SingleGpioInitStruct = {0};
  UINT32                        Index;
  UINT32                        PadMaxSize;
  GPIO_GROUP                    Group;

  PadMaxSize = 0;
  for (Group = GpioGetLowestGroup(); Group <= GpioGetHighestGroup(); Group ++) {
    PadMaxSize += GpioGetPadPerGroup (Group);
  }

  GpioDefinitionTable = NULL;
  GpioDefinitionTable = (GPIO_INIT_CONFIG *)AllocateZeroPool (PadMaxSize * sizeof (GPIO_INIT_CONFIG));
  if (GpioDefinitionTable == NULL) {
    return EFI_OUT_OF_RESOURCES;
  }
  //
  // Use GetGpioCfg in GpioCfgLib to get build-time GPIO configuration
  // Input Board value corresponds to Skuids in Project.dsc
  //
  Status = GetGpioCfg (Board, &GpioInitStruct, &GpioInitStructCount);
  ASSERT_EFI_ERROR (Status);
  if (EFI_ERROR (Status)) {
    return EFI_NOT_FOUND;
  }

  GpioTableCount = 0;
  Status = EFI_UNSUPPORTED;
  for (Index = 0; Index < GpioInitStructCount; Index++) {
    if (GpioInitStruct[Index].Flag != PreMemFlag) {
      continue;
    }
    CopyMem (&SingleGpioInitStruct, &GpioInitStruct[Index], sizeof(H2O_GPIO_INIT_CONFIG));
    //
    // OemServices
    // This function works when PCT being FALSE or PCT's setting being null.
    // Please refer to Power On Guide 0.4.
    //
    DEBUG ((DEBUG_INFO, "OemKernelServices Call: OemSvcUpdateGpioCfg \n"));
    Status = OemSvcUpdateGpioCfg (&SingleGpioInitStruct);
    DEBUG ((DEBUG_INFO, "OemKernelServices OemSvcUpdateGpioCfg Status: %r\n", Status));
    if (Status != EFI_SUCCESS) {
     CopyMem (&GpioDefinitionTable[GpioTableCount], &SingleGpioInitStruct, sizeof(GPIO_INIT_CONFIG));
     GpioTableCount++;
    }
  }
  if ((Status == EFI_SUCCESS) && (GpioTableCount == 0x00)) {
    return EFI_SUCCESS;
  }

  //
  // Batch process GPIO configuraions.
  //
  DEBUG ((DEBUG_INFO, "ConfigureGpio() Start\n"));
  Status = GpioConfigurePads (GpioTableCount, GpioDefinitionTable);
  DEBUG ((DEBUG_INFO, "ConfigureGpio() End\n"));

  return EFI_SUCCESS;
}
//[-end-201104-IB17510119-add]//

/**
  Configure GPIO, TouchPanel, HDA, PMC, TBT etc.

  @retval  EFI_SUCCESS   Operation success.
**/
EFI_STATUS
EFIAPI
AdlSBoardInitBeforeSiliconInit (
  VOID
  )
{
//[-start-191001-16990100-add]//
  EFI_STATUS       Status;
  UINT16           GpioTableCount;
  GPIO_INIT_CONFIG *GpioTable;
//[-end-191001-16990100-add]//
//[-start-201104-IB17510119-add]//
  UINT16            BoardId;

  BoardId = PcdGet16(PcdBoardId);
//[-end-201104-IB17510119-add]//
  AdlSGpioTableInit ();
  AdlSTouchPanelGpioInit ();
  AdlSHdaVerbTableInit ();
  AdlSBoardMiscInit ();
  AdlSBoardTbtInit ();
  AdlSBoardFunctionInit();
  AdlSBoardSecurityInit();
  AdlSBoardPmcPdInit ();
//[-start-201104-IB17510119-remove]//
  // GpioInit (PcdGetPtr (PcdBoardGpioTable));
//[-end-201104-IB17510119-remove]//
//[-start-201104-IB17510119-modify]//
  if (PcdGetBool (PcdH2OGpioCfgSupported) && !PcdGetBool(PcdDisableVpdGpioTable)) {
    //
    // Program GPIOs, Input Board value corresponds to Skuids in Project.dsc
    //
    DEBUG ((DEBUG_INFO, "GpioCfgPei() Start\n"));
//[-start-210120-IB16560239-modify]//
    GpioCfgPei ((H2O_BOARD_ID)BoardId, FALSE);
//[-end-210120-IB16560239-modify]//
    DEBUG ((DEBUG_INFO, "GpioCfgPei() END\n"));
  } else {
    GpioTable = PcdGetPtr (PcdBoardGpioTable);
    GetGpioTableSize (GpioTable, &GpioTableCount);
    DEBUG_OEM_SVC ((DEBUG_INFO, "Pei OemChipsetServices Call: OemSvcModifyGpioSettingTable \n"));
    Status = OemSvcModifyGpioSettingTable (&GpioTable, &GpioTableCount);
    DEBUG_OEM_SVC ((DEBUG_INFO, "Pei OemChipsetServices OemSvcModifyGpioSettingTable Status: %r\n", Status));
    if (GpioTableCount != 0 && Status != EFI_SUCCESS) {
      GpioConfigurePads ((UINT32) GpioTableCount, GpioTable);
    }
  }
//[-end-201104-IB17510119-modify]//
  ///
  /// Do Late PCH init
  ///
  LateSiliconInit ();

  return EFI_SUCCESS;
}

VOID
AdlSBoardSpecificGpioInitPostMem (
  VOID
  )
{
  UINT16     BoardId;
  UINT8      DTbtRtd3;

  BoardId =  PcdGet16 (PcdBoardId);
  DTbtRtd3 = AdlSGetDTbtRtd3Setup ();

  switch (BoardId) {
    case BoardIdAdlSAdpSDdr5UDimm1DAep:
      //
      // Update OEM table ID
      //
      PcdSet64S (PcdXhciAcpiTableSignature, SIGNATURE_64 ('x', 'h', '_', 'a', 's', 'd', 'd', '5'));
      //
      // Update PcdBoardRtd3TableSignature per Setup
      //
      if (DTbtRtd3) {
        PcdSet64S (PcdBoardRtd3TableSignature, SIGNATURE_64 ('A', 'd', 'l', 'S', 'A', 'T', 'b', 't'));
      } else {
        PcdSet64S (PcdBoardRtd3TableSignature, SIGNATURE_64 ('A', 'd', 'l', 'S', '_', 'A', 'e', 'p'));
      }
      break;
    case BoardIdAdlSAdpSDdr5UDimm1DOc:
      //
      // Update OEM table ID
      //
      PcdSet64S (PcdXhciAcpiTableSignature, SIGNATURE_64 ('x', 'h', '_', 'a', 'd', 's', '1', '0'));
      //
      // Update PcdBoardRtd3TableSignature per Setup
      //
      PcdSet64S (PcdBoardRtd3TableSignature, SIGNATURE_64 ('A', 'd', 'l', 'S', '_', 'R', 'v', 'p'));
      break;
    case BoardIdAdlSAdpSSbgaDdr5SODimmErb:
    case BoardIdAdlSAdpSSbgaDdr5SODimmCrb:
      //
      // Update OEM table ID
      //
      PcdSet64S (PcdXhciAcpiTableSignature, SIGNATURE_64 ('x', 'h', '_', 'a', 'd', 'l', 's', 'b'));
      //
      // Update PcdBoardRtd3TableSignature per Setup
      //
      if (DTbtRtd3) {
        PcdSet64S (PcdBoardRtd3TableSignature, SIGNATURE_64 ('A', 'd', 'l', 'S', 'B', 'T', 'b', 't'));
      } else {
        PcdSet64S (PcdBoardRtd3TableSignature, SIGNATURE_64 ('A', 'd', 'l', 'S', 'B', 'R', 'v', 'p'));
      }
      break;
    case BoardIdAdlSAdpSSbgaDdr4SODimmCrb:
      //
      // Update OEM table ID
      //
      PcdSet64S (PcdXhciAcpiTableSignature, SIGNATURE_64 ('x', 'h', '_','a', 'd', 's', 'b','3'));
      //
      // Update PcdBoardRtd3TableSignature per Setup
      //
      if (DTbtRtd3) {
        PcdSet64S (PcdBoardRtd3TableSignature, SIGNATURE_64 ('A', 'd', 'l', 'S', 'B', '3', 'T', 'b'));
      } else {
        PcdSet64S (PcdBoardRtd3TableSignature, SIGNATURE_64 ('A', 'd', 'l', 'S', 'B', '3', 'R', 'v'));
      }
      break;
    case BoardIdAdlSAdpSDdr5SODimmCrb:
    case BoardIdAdlSAdpSDdr4SODimmCrb:
      //
      // Update OEM table ID
      //
      PcdSet64S (PcdXhciAcpiTableSignature, SIGNATURE_64 ('x', 'h', '_', 'a', 'd', 'l', 's', '8'));
      //
      // Update PcdBoardRtd3TableSignature per Setup
      //
      if (DTbtRtd3) {
        PcdSet64S (PcdBoardRtd3TableSignature, SIGNATURE_64 ('A', 'd', 'l', 'S', '8', 'T', 'b', 't'));
      } else {
        PcdSet64S (PcdBoardRtd3TableSignature, SIGNATURE_64 ('A', 'd', 'l', 'S', '8', 'R', 'v', 'p'));
      }
      //
      // Config GPIO for Audio I2S Codec
      //
      PcdSet32S (PcdHdaI2sCodecIrqGpio, GPIO_VER4_S_GPP_F23); // Audio I2S Codec IRQ Pin
      PcdSet8S  (PcdHdaI2sCodecI2cBusNumber, 4);              // Audio I2S Codec conntected to I2C4
      break;
    case BoardIdAdlSAdpSSbgaDdr5SODimmAep:
      //
      // Update OEM table ID
      //
      PcdSet64S (PcdXhciAcpiTableSignature, SIGNATURE_64 ('x', 'h', '_', 'a', 'd', 'l', 's', 'b'));
      //
      // Update PcdBoardRtd3TableSignature
      //
      PcdSet64S (PcdBoardRtd3TableSignature, SIGNATURE_64 ('A', 'd', 'l', 'S', 'B', 'A', 'e', 'p'));
      break;
    case BoardIdAdlSAdpSDdr4UDimm2DCrb:
    case BoardIdAdlSAdpSDdr4UDimm2DCrbCpv:
    case BoardIdAdlSAdpSDdr5UDimm1DCrb:
    case BoardIdAdlSAdpSDdr5UDimm1DCrbPpv:
    default:
      //
      // Update OEM table ID
      //
      PcdSet64S (PcdXhciAcpiTableSignature, SIGNATURE_64 ('x', 'h', '_', 'a', 'd', 'l', 's', '3'));
      //
      // Update PcdBoardRtd3TableSignature per Setup
      //
      if (DTbtRtd3) {
        PcdSet64S (PcdBoardRtd3TableSignature, SIGNATURE_64 ('A', 'd', 'l', 'S', '_', 'T', 'b', 't'));
      } else {
        PcdSet64S (PcdBoardRtd3TableSignature, SIGNATURE_64 ('A', 'd', 'l', 'S', '_', 'R', 'v', 'p'));
      }
      //
      // Config GPIO for Audio I2S Codec
      //
      PcdSet32S (PcdHdaI2sCodecIrqGpio, GPIO_VER4_S_GPP_F23); // Audio I2S Codec IRQ Pin
      PcdSet8S  (PcdHdaI2sCodecI2cBusNumber, 4);              // Audio I2S Codec conntected to I2C4
      break;
  }
  //
  // Assign Bluetooth & Wifi relative GPIO.
  //
  //
  // Configure GPIOs for BT modules - UART or USB
  //
  PcdSet32S (PcdBtRfKillGpio, GPIO_VER4_S_GPP_B3); // Bluetooth RF-KILL# pin
  if (PcdGet8 (PcdDiscreteBtModule) == 2) {        // Only for BT Over UART Selection
    PcdSet32S (PcdBtIrqGpio, GPIO_VER4_S_GPP_B2);  // Bluetooth IRQ Pin
  }
  //
  // Modify Preferred_PM_Profile field based on Board SKU's. Default is set to Desktop.
  //
  PcdSet8S (PcdPreferredPmProfile, EFI_ACPI_2_0_PM_PROFILE_DESKTOP);
  if (PcdGet8 (PcdPlatformFlavor) == FlavorUpServer) {
    PcdSet8S (PcdPreferredPmProfile, EFI_ACPI_2_0_PM_PROFILE_ENTERPRISE_SERVER);
  } else if ((PcdGet8 (PcdPlatformFlavor) == FlavorMobile) || (PcdGet8 (PcdPlatformFlavor) == FlavorMobileWorkstation)) {
    PcdSet8S (PcdPreferredPmProfile, EFI_ACPI_2_0_PM_PROFILE_MOBILE);
  }
}

VOID
AdlSInitCommonPlatformPcdPostMem (
  VOID
  )
{
  UINT16          BoardId;
#if FixedPcdGetBool (PcdDTbtEnable) == 1
  DTBT_INFO_HOB*  TbtInfoHob;

  TbtInfoHob = (DTBT_INFO_HOB*) GetFirstGuidHob (&gDTbtInfoHobGuid);
#endif
  BoardId = PcdGet16 (PcdBoardId);

  switch (BoardId) {
    case BoardIdAdlSAdpSSbgaDdr5SODimmErb:
    case BoardIdAdlSAdpSSbgaDdr5SODimmCrb:
    case BoardIdAdlSAdpSSbgaDdr5SODimmAep:
      PcdSet32S (PcdEcSmiGpio, GPIO_VER4_S_GPP_B20);
      break;
    case BoardIdAdlSAdpSDdr4UDimm2DCrb:
    case BoardIdAdlSAdpSDdr4UDimm2DCrbCpv:
    case BoardIdAdlSAdpSDdr5UDimm1DAep:
    case BoardIdAdlSAdpSDdr5UDimm1DCrb:
    case BoardIdAdlSAdpSDdr5UDimm1DCrbPpv:
    default:
      PcdSet32S (PcdEcSmiGpio, GPIO_VER4_S_GPP_B4);
      //PcdSet32S (PcdEcLowPowerExitGpio, value);   // Todo for ADL RVP
      break;
  }
  switch (BoardId) {
    case BoardIdAdlSAdpSDdr5UDimm1DAep:
      break;
    case BoardIdAdlSAdpSDdr4UDimm2DCrb:
    case BoardIdAdlSAdpSDdr4UDimm2DCrbCpv:
    case BoardIdAdlSAdpSDdr5UDimm1DCrb:
    case BoardIdAdlSAdpSDdr5UDimm1DCrbPpv:
    default:
      PcdSetBoolS (PcdPssReadSN, TRUE);
      PcdSet8S (PcdPssI2cSlaveAddress, 0x6E);
      PcdSet8S (PcdPssI2cBusNumber, 0x02);
      break;
  }

  PcdSetBoolS (PcdSpdAddressOverride, FALSE);

  //
  //   Board             Type C port     CPU Port    PCH Port    Split support
  //   TGL Y LP DDR4          1            0            1            Yes
  //                          2            1            5            Yes
  //                          3            2            6            Yes
  //                          4            3            7            Yes
  //   TGL H DDR4             1            0            7            Yes
  //                          2            1            8            Yes
  //                          3            2            9            Yes
  //                          4            3           10            Yes
  //   ADL S DDR5 AEP         1            0           10            Yes
  //                          2            1            9            Yes
  //                          3            2            3            Yes
  //                          4            3            4            Yes

  PcdSetBoolS(PcdUsbTypeCSupport, TRUE);
#if FixedPcdGetBool (PcdDTbtEnable) == 1
  switch (BoardId) {
    case BoardIdAdlSAdpSDdr5UDimm1DAep:
      if (TbtInfoHob != NULL) {
        // Number of ports supported
        PcdSet8S (PcdTypeCPortsSupported, 4);
        if (TbtInfoHob->DTbtControllerConfig[0].DTbtControllerEn == 1) {
          // TBT Port A  mapping and properties [TBT 1 Onboard]
          PcdSet8S (PcdUsbTypeCPort1, 1);
          PcdSet8S (PcdUsbTypeCPort1Pch, 10);
          PcdSet8S (PcdUsbCPort1Properties, (21 << 3 | USB_TYPEC_TBT << 1 | SPLIT_SUPPORTED));
          // TBT Port B  mapping and properties [TBT 1 Onboard]
          PcdSet8S (PcdUsbTypeCPort2, 2);
          PcdSet8S (PcdUsbTypeCPort2Pch, 9);
          PcdSet8S (PcdUsbCPort2Properties, (21 << 3 | USB_TYPEC_TBT << 1 | SPLIT_SUPPORTED));
        } else {
          // PCH USB Port A  mapping and properties
          PcdSet8S (PcdUsbTypeCPort1, 1);
          PcdSet8S (PcdUsbTypeCPort1Pch, 10);
          PcdSet8S (PcdUsbCPort1Properties, (0 << 3 | USB_TYPEC_PCH << 1 | SPLIT_NOT_SUPPORTED));
          // PCH USB Port B  mapping and properties
          PcdSet8S (PcdUsbTypeCPort2, 2);
          PcdSet8S (PcdUsbTypeCPort2Pch, 9);
          PcdSet8S (PcdUsbCPort2Properties, (0 << 3 | USB_TYPEC_PCH << 1 | SPLIT_NOT_SUPPORTED));
        }
        if (TbtInfoHob->DTbtControllerConfig[1].DTbtControllerEn == 1) {
          // TBT Port C  mapping and properties [TBT 2 Onboard]
          PcdSet8S (PcdUsbTypeCPort3, 3);
          PcdSet8S (PcdUsbTypeCPort3Pch, 3);
          PcdSet8S (PcdUsbCPort3Properties, (25 << 3 | USB_TYPEC_TBT << 1 | SPLIT_SUPPORTED));
          // TBT Port D  mapping and properties [TBT 2 Onboard]
          PcdSet8S (PcdUsbTypeCPort4, 4);
          PcdSet8S (PcdUsbTypeCPort4Pch, 4);
          PcdSet8S (PcdUsbCPort4Properties, (25 << 3 | USB_TYPEC_TBT << 1 | SPLIT_SUPPORTED));
        } else {
          // PCH USB Port C  mapping and properties
          PcdSet8S (PcdUsbTypeCPort3, 3);
          PcdSet8S (PcdUsbTypeCPort3Pch, 3);
          PcdSet8S (PcdUsbCPort3Properties, (0 << 3 | USB_TYPEC_PCH << 1 | SPLIT_NOT_SUPPORTED));
          // PCH USB Port D  mapping and properties
          PcdSet8S (PcdUsbTypeCPort4, 4);
          PcdSet8S (PcdUsbTypeCPort4Pch, 4);
          PcdSet8S (PcdUsbCPort4Properties, (0 << 3 | USB_TYPEC_PCH << 1 | SPLIT_NOT_SUPPORTED));
        }
      }
      break;
    case BoardIdAdlSAdpSSbgaDdr5SODimmErb:
    case BoardIdAdlSAdpSSbgaDdr5SODimmCrb:
      // Number of ports supported
      PcdSet8S (PcdTypeCPortsSupported, 4);

      // TBT Port A mapping and properties [TBT 1 Onboard]
      PcdSet8S (PcdUsbTypeCPort1, 1);
      PcdSet8S (PcdUsbTypeCPort1Pch, 11);
      PcdSet8S (PcdUsbCPort1Properties, (25 << 3 | USB_TYPEC_TBT << 1 | SPLIT_SUPPORTED));
      // TBT Port B mapping and properties [TBT 1 Onboard]
      PcdSet8S (PcdUsbTypeCPort2, 2);
      PcdSet8S (PcdUsbTypeCPort2Pch, 9);
      PcdSet8S (PcdUsbCPort2Properties, (25 << 3 | USB_TYPEC_TBT << 1 | SPLIT_SUPPORTED));

      // TBT Port C mapping and properties [TBT 2 Onboard]
      PcdSet8S (PcdUsbTypeCPort3, 3);
      PcdSet8S (PcdUsbTypeCPort3Pch, 6);
      PcdSet8S (PcdUsbCPort3Properties, (9 << 3 | USB_TYPEC_TBT << 1 | SPLIT_SUPPORTED));
      // TBT Port D mapping and properties [TBT 2 Onboard]
      PcdSet8S (PcdUsbTypeCPort4, 4);
      PcdSet8S (PcdUsbTypeCPort4Pch, 5);
      PcdSet8S (PcdUsbCPort4Properties, (9 << 3 | USB_TYPEC_TBT << 1 | SPLIT_SUPPORTED));
      break;
    case BoardIdAdlSAdpSSbgaDdr5SODimmAep:
      if (TbtInfoHob != NULL) {
        // Number of ports supported
        PcdSet8S (PcdTypeCPortsSupported, 2);
        if (TbtInfoHob->DTbtControllerConfig[0].DTbtControllerEn == 1) {
          // TBT Port A  mapping and properties [TBT 1 Onboard]
          PcdSet8S (PcdUsbTypeCPort1, 1);
          PcdSet8S (PcdUsbTypeCPort1Pch, 11);
          PcdSet8S (PcdUsbCPort1Properties, (9 << 3 | USB_TYPEC_TBT << 1 | SPLIT_SUPPORTED));
          // TBT Port B  mapping and properties [TBT 2 Onboard]
          PcdSet8S (PcdUsbTypeCPort2, 2);
          PcdSet8S (PcdUsbTypeCPort2Pch, 9);
          PcdSet8S (PcdUsbCPort1Properties, (9 << 3 | USB_TYPEC_TBT << 1 | SPLIT_SUPPORTED));
        } else{
          // PCH USB Port A  mapping and properties
          PcdSet8S (PcdUsbTypeCPort1, 1);
          PcdSet8S (PcdUsbTypeCPort1Pch, 11);
          PcdSet8S (PcdUsbCPort2Properties, (0 << 3 | USB_TYPEC_TBT << 1 | SPLIT_SUPPORTED));
          // PCH USB Port B  mapping and properties
          PcdSet8S (PcdUsbTypeCPort2, 2);
          PcdSet8S (PcdUsbTypeCPort2Pch, 9);
          PcdSet8S (PcdUsbCPort2Properties, (0 << 3 | USB_TYPEC_TBT << 1 | SPLIT_SUPPORTED));
        }
      }
      break;
    case BoardIdAdlSAdpSDdr5UDimm1DOc:
      PcdSetBoolS(PcdUsbTypeCSupport, FALSE);
      break;
    case BoardIdAdlSAdpSDdr5SODimmCrb:
    case BoardIdAdlSAdpSDdr4SODimmCrb:
      // Number of ports supported
      PcdSet8S (PcdTypeCPortsSupported, 4);
      // Type C port 1 mapping on MR port 1, dTBT SS01 maps to PCH HS port 5
      PcdSet8S (PcdUsbTypeCPort1, 1);
      PcdSet8S (PcdUsbTypeCPort1Pch, 5);
      PcdSet8S (PcdUsbCPort1Properties, (25 << 3 | USB_TYPEC_TBT << 1 | SPLIT_SUPPORTED));
      // Type C port 2 mapping on MR port 2, dTBT SS02 maps to PCH HS port 6
      PcdSet8S (PcdUsbTypeCPort2, 2);
      PcdSet8S (PcdUsbTypeCPort2Pch, 6);
      PcdSet8S (PcdUsbCPort2Properties, (25 << 3 | USB_TYPEC_TBT << 1 | SPLIT_SUPPORTED));

      // Type C port 3 mapping on PCH, SS07 maps to HS01
      PcdSet8S (PcdUsbTypeCPort3, 3);
      PcdSet8S (PcdUsbTypeCPort3Pch, 1);
      PcdSet8S (PcdUsbCPort3Properties, (0 << 3 | USB_TYPEC_PCH << 1 | SPLIT_NOT_SUPPORTED));

      // Type C port 4 mapping on PCH, SS01 maps to HS09
      PcdSet8S (PcdUsbTypeCPort4, 4);
      PcdSet8S (PcdUsbTypeCPort4Pch, 9);
      PcdSet8S (PcdUsbCPort4Properties, (0 << 3 | USB_TYPEC_PCH << 1 | SPLIT_NOT_SUPPORTED));
      break;
    case BoardIdAdlSAdpSSbgaDdr4SODimmCrb:
      // Number of ports supported
      PcdSet8S (PcdTypeCPortsSupported, 3);
      // Type C port 1 mapping on MR port 1, dTBT SS01 maps to PCH HS port 5
      PcdSet8S (PcdUsbTypeCPort1, 1);
      PcdSet8S (PcdUsbTypeCPort1Pch, 5);
      PcdSet8S (PcdUsbCPort1Properties, (25 << 3 | USB_TYPEC_TBT << 1 | SPLIT_SUPPORTED));
      // Type C port 2 mapping on MR port 2, dTBT SS02 maps to PCH HS port 6
      PcdSet8S (PcdUsbTypeCPort2, 2);
      PcdSet8S (PcdUsbTypeCPort2Pch, 6);
      PcdSet8S (PcdUsbCPort2Properties, (25 << 3 | USB_TYPEC_TBT << 1 | SPLIT_SUPPORTED));

      // Type C port 3 mapping on PCH, SS07 maps to HS01
      PcdSet8S (PcdUsbTypeCPort3, 3);
      PcdSet8S (PcdUsbTypeCPort3Pch, 1);
      PcdSet8S (PcdUsbCPort3Properties, (0 << 3 | USB_TYPEC_PCH << 1 | SPLIT_NOT_SUPPORTED));
      break;

    default:
      // Number of ports supported
      PcdSet8S (PcdTypeCPortsSupported, 4);
      // Type C port 1 mapping on MR port 1, dTBT SS01 maps to PCH HS port 11
      PcdSet8S (PcdUsbTypeCPort1, 1);
      PcdSet8S (PcdUsbTypeCPort1Pch, 11);
      PcdSet8S (PcdUsbCPort1Properties, (9 << 3 | USB_TYPEC_TBT << 1 | SPLIT_SUPPORTED));
      // Type C port 2 mapping on MR port 2, dTBT SS02 maps to PCH HS port 13
      PcdSet8S (PcdUsbTypeCPort2, 2);
      PcdSet8S (PcdUsbTypeCPort2Pch, 13);
      PcdSet8S (PcdUsbCPort2Properties, (9 << 3 | USB_TYPEC_TBT << 1 | SPLIT_SUPPORTED));

      // Type C port 3 mapping on PCH, SS06 maps to HS01
      PcdSet8S (PcdUsbTypeCPort3, 3);
      PcdSet8S (PcdUsbTypeCPort3Pch, 1);
      PcdSet8S (PcdUsbCPort3Properties, (0 << 3 | USB_TYPEC_PCH << 1 | SPLIT_NOT_SUPPORTED));

      // Type C port 4 mapping on PCH, SS01 maps to HS09
      PcdSet8S (PcdUsbTypeCPort4, 4);
      PcdSet8S (PcdUsbTypeCPort4Pch, 9);
      PcdSet8S (PcdUsbCPort4Properties, (0 << 3 | USB_TYPEC_PCH << 1 | SPLIT_NOT_SUPPORTED));
      break;
  }
#endif
  //
  // PD PS_ON GPIO
  //
  switch (BoardId) {
    case BoardIdAdlSAdpSDdr5UDimm1DAep:
      break;
    case BoardIdAdlSAdpSDdr4UDimm2DCrb:
    case BoardIdAdlSAdpSDdr4UDimm2DCrbCpv:
      PcdSet32S (PcdUsbCPsonOverrideN, GPIO_VER4_S_GPP_D14);
      PcdSet32S (PcdUsbCPsonS0ixEntryReq, GPIO_VER4_S_GPP_D11);
      PcdSet32S (PcdUsbCPsonS0ixEntryAck, GPIO_VER4_S_GPP_D13);
      break;
    case BoardIdAdlSAdpSDdr5UDimm1DCrb:
    case BoardIdAdlSAdpSDdr5UDimm1DCrbPpv:
    case BoardIdAdlSAdpSDdr5SODimmCrb:
    case BoardIdAdlSAdpSDdr4SODimmCrb:
      PcdSet32S (PcdUsbCPsonOverrideN, GPIO_VER4_S_GPP_F22);
      PcdSet32S (PcdUsbCPsonS0ixEntryReq, GPIO_VER4_S_GPP_K4);
      PcdSet32S (PcdUsbCPsonS0ixEntryAck, GPIO_VER4_S_GPP_K3);
      break;
    default:
      break;
  }

  //
  // Battery Present
  // Real & Virtual battery needs to be supported in all except Desktop
  //
  switch (BoardId) {
    case BoardIdAdlSAdpSSbgaDdr5SODimmErb:
    case BoardIdAdlSAdpSSbgaDdr5SODimmCrb:
    case BoardIdAdlSAdpSSbgaDdr5SODimmAep:
      PcdSet8S (PcdBatteryPresent,  BOARD_REAL_BATTERY_SUPPORTED | BOARD_VIRTUAL_BATTERY_SUPPORTED);
      break;
    default:
      PcdSet8S (PcdBatteryPresent, BOARD_NO_BATTERY_SUPPORT);
      break;
  }
  PcdSet8S (PcdRealBattery1Control, 1);
  PcdSet8S (PcdRealBattery2Control, 2);

  //
  // Sky Camera Sensor
  //
  PcdSetBoolS (PcdMipiCamSensor, FALSE);

  //
  // H8S2113 SIO,UART
  //
  PcdSetBoolS (PcdH8S2113SIO, TRUE);
  switch (BoardId) {
    case BoardIdAdlSAdpSDdr5UDimm1DCrbPpv:
    case BoardIdAdlSAdpSSbgaDdr5SODimmCrbPpv:
    case BoardIdAdlSAdpSDdr5UDimm1DCrb:
      PcdSetBoolS (PcdH8S2113UAR, TRUE);
      break;
    default:
      PcdSetBoolS (PcdH8S2113UAR, FALSE);
      break;
  }

  //
  // NCT6776F COM, SIO & HWMON
  //
  PcdSetBoolS (PcdNCT6776FCOM, FALSE);
  PcdSetBoolS (PcdNCT6776FSIO, FALSE);
  PcdSetBoolS (PcdNCT6776FHWMON, FALSE);
  //
  // ZPODD
  //
  PcdSet8S (PcdZPoddConfig, 0);

  //
  // Convertable Dock Support
  // Not supported only for S & H SKU's
  PcdSetBoolS (PcdConvertableDockSupport, TRUE);
  //
  // Ec Hotkey F3, F4, F5, F6, F7 and F8 Support
  //
  PcdSet8S (PcdEcHotKeyF3Support, TRUE);
  PcdSet8S (PcdEcHotKeyF4Support, TRUE);
  PcdSet8S (PcdEcHotKeyF5Support, TRUE);
  PcdSet8S (PcdEcHotKeyF6Support, TRUE);
  PcdSet8S (PcdEcHotKeyF7Support, TRUE);
  PcdSet8S (PcdEcHotKeyF8Support, TRUE);

  //
  // Virtual Button Volume Up & Done Support
  // Virtual Button Home Button Support
  // Virtual Button Rotation Lock Support
  //
  PcdSetBoolS (PcdVirtualButtonVolumeUpSupport, TRUE);
  PcdSetBoolS (PcdVirtualButtonVolumeDownSupport, TRUE);
  PcdSetBoolS (PcdVirtualButtonHomeButtonSupport, TRUE);
  PcdSetBoolS (PcdVirtualButtonRotationLockSupport, TRUE);
  //
  // Slate Mode Switch Support
  //
  PcdSetBoolS (PcdSlateModeSwitchSupport, TRUE);
  //
  // Virtual Gpio Button Support
  //
  PcdSetBoolS (PcdVirtualGpioButtonSupport, TRUE);
  //
  // Ac Dc Auto Switch Support
  //
  PcdSetBoolS (PcdAcDcAutoSwitchSupport, FALSE);

  //
  // Acpi Enable All Button Support
  //
  PcdSetBoolS (PcdAcpiEnableAllButtonSupport, TRUE);
  //
  // Acpi Hid Driver Button Support
  //
  PcdSetBoolS (PcdAcpiHidDriverButtonSupport, TRUE);

  //ADL-S SKU is Ecless-PD design, no communication between EC and PD

  PcdSetBoolS (PcdUsbcEcPdNegotiation, FALSE);
}

/**
  Board init for PEI after Silicon initialized

  @retval  EFI_SUCCESS   Operation success.
**/EFI_STATUS
EFIAPI
AdlSBoardInitAfterSiliconInit (
  VOID
  )
{
  DEBUG ((DEBUG_INFO, "AdlSBoardInitAfterSiliconInit \n"));
  AdlSBoardSpecificGpioInitPostMem ();
  AdlSInitCommonPlatformPcdPostMem ();
  return EFI_SUCCESS;
}
