/** @file
  PEI Multi-Board Initialization in Pre-Memory PEI Library

@copyright
  INTEL CONFIDENTIAL
  Copyright 2017 - 2020 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains a 'Sample Driver' and is licensed as such under the terms
  of your license agreement with Intel or your vendor. This file may be modified
  by the user, subject to the additional terms of the license agreement.

@par Specification Reference:
**/

#include <PiPei.h>
#include <Library/BaseLib.h>
#include <Library/IoLib.h>
#include <Library/BoardInitLib.h>
#include <Library/MultiBoardInitSupportLib.h>
#include <Library/PcdLib.h>
#include <Library/DebugLib.h>
#include <Library/BoardConfigLib.h>
#include <Library/EcMiscLib.h>
#include <PlatformBoardId.h>
#include <Library/PeiServicesLib.h>

EFI_STATUS
EFIAPI
AdlSBoardDetect (
  VOID
  );

EFI_STATUS
EFIAPI
AdlSMultiBoardDetect (
  VOID
  );

EFI_BOOT_MODE
EFIAPI
AdlSBoardBootModeDetect (
  VOID
  );

EFI_STATUS
EFIAPI
AdlSBoardDebugInit (
  VOID
  );

EFI_STATUS
EFIAPI
AdlSBoardInitBeforeMemoryInit (
  VOID
  );

EFI_STATUS
EFIAPI
AdlSBoardInitAfterMemoryInit (
  VOID
  );

EFI_STATUS
EFIAPI
AdlSBoardPatchConfigurationDataPreMemCallback (
  IN EFI_PEI_SERVICES           **PeiServices,
  IN EFI_PEI_NOTIFY_DESCRIPTOR  *NotifyDescriptor,
  IN VOID                       *Ppi
  );

static EFI_PEI_NOTIFY_DESCRIPTOR mOtherBoardPatchConfigurationDataPreMemNotifyList = {
  (EFI_PEI_PPI_DESCRIPTOR_NOTIFY_CALLBACK | EFI_PEI_PPI_DESCRIPTOR_TERMINATE_LIST),
  &gPatchConfigurationDataPreMemPpiGuid,
  AdlSBoardPatchConfigurationDataPreMemCallback
};

BOARD_DETECT_FUNC  mAdlSBoardDetectFunc = {
  AdlSMultiBoardDetect
};

BOARD_PRE_MEM_INIT_FUNC  mAdlSBoardPreMemInitFunc = {
  AdlSBoardDebugInit,
  AdlSBoardBootModeDetect,
  AdlSBoardInitBeforeMemoryInit,
  AdlSBoardInitAfterMemoryInit,
  NULL, // BoardInitBeforeTempRamExit
  NULL, // BoardInitAfterTempRamExit
};

EFI_STATUS
EFIAPI
AdlSMultiBoardDetect (
  VOID
  )
{
  UINT8  SkuType;
  DEBUG ((DEBUG_INFO, " In AdlSMultiBoardDetect \n"));

  AdlSBoardDetect ();
  SkuType = PcdGet8 (PcdSkuType);
  if (SkuType==AdlSSkuType) {
    RegisterBoardPreMemInit (&mAdlSBoardPreMemInitFunc);
    PeiServicesNotifyPpi (&mOtherBoardPatchConfigurationDataPreMemNotifyList);
  } else {
    DEBUG ((DEBUG_WARN,"Not supporting AlderLake S Board\n"));
  }

  return EFI_SUCCESS;
}

EFI_STATUS
EFIAPI
PeiAdlSMultiBoardInitPreMemLibConstructor (
  VOID
  )
{
  return RegisterBoardDetect (&mAdlSBoardDetectFunc);
}
