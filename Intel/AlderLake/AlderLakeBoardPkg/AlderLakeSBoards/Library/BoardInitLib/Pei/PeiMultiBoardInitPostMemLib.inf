## @file
#
#******************************************************************************
#* Copyright (c) 2019 - 2020, Insyde Software Corporation. All Rights Reserved.
#*
#* You may not reproduce, distribute, publish, display, perform, modify, adapt,
#* transmit, broadcast, present, recite, release, license or otherwise exploit
#* any part of this publication in any form, by any means, without the prior
#* written permission of Insyde Software Corporation.
#*
#******************************************************************************
## @file
# Component information file for AlderLake Multi-Board Initialization in PEI post memory phase.
#
#@copyright
#  INTEL CONFIDENTIAL
#  Copyright 2017 - 2021 Intel Corporation.
#
#  The source code contained or described herein and all documents related to the
#  source code ("Material") are owned by Intel Corporation or its suppliers or
#  licensors. Title to the Material remains with Intel Corporation or its suppliers
#  and licensors. The Material may contain trade secrets and proprietary and
#  confidential information of Intel Corporation and its suppliers and licensors,
#  and is protected by worldwide copyright and trade secret laws and treaty
#  provisions. No part of the Material may be used, copied, reproduced, modified,
#  published, uploaded, posted, transmitted, distributed, or disclosed in any way
#  without Intel's prior express written permission.
#
#  No license under any patent, copyright, trade secret or other intellectual
#  property right is granted to or conferred upon you by disclosure or delivery
#  of the Materials, either expressly, by implication, inducement, estoppel or
#  otherwise. Any license under such intellectual property rights must be
#  express and approved by Intel in writing.
#
#  Unless otherwise agreed by Intel in writing, you may not remove or alter
#  this notice or any other notice embedded in Materials by Intel or
#  Intel's suppliers or licensors in any way.
#
#  This file contains a 'Sample Driver' and is licensed as such under the terms
#  of your license agreement with Intel or your vendor. This file may be modified
#  by the user, subject to the additional terms of the license agreement.
#
# @par Specification Reference:
#
# @par Glossary:
###

[Defines]
  INF_VERSION                    = 0x00010005
  BASE_NAME                      = PeiAdlSMultiBoardInitLib
  FILE_GUID                      = B86D79DF-8BA8-4F95-9E37-ED773EB8FEEB
  MODULE_TYPE                    = BASE
  VERSION_STRING                 = 1.0
  LIBRARY_CLASS                  = NULL
  CONSTRUCTOR                    = PeiAdlSMultiBoardInitLibConstructor

[LibraryClasses]
  BaseLib
  DebugLib
  BaseMemoryLib
  MemoryAllocationLib
  PcdLib
  SiliconInitLib
  MultiBoardInitSupportLib
  PciSegmentLib
  PreSiliconEnvDetectLib
  GpioLib
  HobLib
  BoardConfigLib
#[-start-191001-16990100-add]#
  PeiOemSvcChipsetLibDefault
#[-end-191001-16990100-add]#
#[-start-201104-IB17510119-add]#
  GpioCfgLib
  PeiOemSvcKernelLibDefault
#[-end-201104-IB17510119-add]#

[Packages]
  MinPlatformPkg/MinPlatformPkg.dec
  AlderLakeBoardPkg/BoardPkg.dec
  MdePkg/MdePkg.dec
  MdeModulePkg/MdeModulePkg.dec
  ClientOneSiliconPkg/SiPkg.dec
  AlderLakePlatSamplePkg/PlatformPkg.dec
  SecurityPkg/SecurityPkg.dec
#[-start-191001-16990100-add]#
  SioDummyPkg/SioDummyPkg.dec
  InsydeModulePkg/InsydeModulePkg.dec
#[-end-191001-16990100-add]#
#[-start-191218-IB16740000-add]#
  $(CHIPSET_PKG)/$(CHIPSET_PKG).dec
#[-end-191218-IB16740000-add]#
#[-start-201104-IB17510119-add]#
  InsydeOemServicesPkg/InsydeOemServicesPkg.dec
#[-end-201104-IB17510119-add]#

[Sources]
  PeiInitPostMemLib.c
  PeiMultiBoardInitPostMemLib.c

[Guids]
  gDTbtInfoHobGuid
  gPchSetupVariableGuid

[FixedPcd]
  gSiPkgTokenSpaceGuid.PcdEmbeddedEnable
  gPlatformModuleTokenSpaceGuid.PcdDTbtEnable

[Pcd]
  # Board GPIO Table
  gBoardModuleTokenSpaceGuid.PcdBoardGpioTable
  gBoardModuleTokenSpaceGuid.PcdSataPortsEnable0

  gBoardModuleTokenSpaceGuid.PcdBoardPmcPdEnable

  # HSIO

  #Tbt config
  gBoardModuleTokenSpaceGuid.PcdDTbtControllerNumber
  gBoardModuleTokenSpaceGuid.PcdITbtRootPortNumber
  gBoardModuleTokenSpaceGuid.PcdUsbcEcPdNegotiation

  # TPM interrupt
  gMinPlatformPkgTokenSpaceGuid.PcdTpm2Enable                       ## CONSUMES
  gEfiSecurityPkgTokenSpaceGuid.PcdTpm2CurrentIrqNum

  #Misc Config
  gBoardModuleTokenSpaceGuid.PcdSataLedEnable
  gBoardModuleTokenSpaceGuid.PcdVrAlertEnable
  gBoardModuleTokenSpaceGuid.PcdPchThermalHotEnable
  gBoardModuleTokenSpaceGuid.PcdMemoryThermalSensorGpioCPmsyncEnable
  gBoardModuleTokenSpaceGuid.PcdMemoryThermalSensorGpioDPmsyncEnable

  #HDA config
  gBoardModuleTokenSpaceGuid.PcdHdaVerbTableDatabase
  gBoardModuleTokenSpaceGuid.PcdHdaI2sCodecIrqGpio             ## PRODUCE
  gBoardModuleTokenSpaceGuid.PcdHdaI2sCodecI2cBusNumber        ## PRODUCE

  #TouchPanel Config
  gBoardModuleTokenSpaceGuid.PcdBoardGpioTableTouchPanel1
  gBoardModuleTokenSpaceGuid.PcdBoardGpioTableTouchPanel1Size
  gBoardModuleTokenSpaceGuid.PcdBoardGpioTableTouchPanel2
  gBoardModuleTokenSpaceGuid.PcdBoardGpioTableTouchPanel2Size

  # Pcd Hook Function
  gBoardModuleTokenSpaceGuid.PcdFuncPeiBoardSpecificInitPostMem

  # CPU PEG Slot1 RTD3
  gBoardModuleTokenSpaceGuid.PcdPegSlot1PwrEnableGpioNo
  gBoardModuleTokenSpaceGuid.PcdPegSlot1PwrEnableGpioPolarity
  gBoardModuleTokenSpaceGuid.PcdPegSlot1RstGpioNo
  gBoardModuleTokenSpaceGuid.PcdPegSlot1RstGpioPolarity
  gBoardModuleTokenSpaceGuid.PcdPegSlot1WakeGpioPin
  gBoardModuleTokenSpaceGuid.PcdPegSlot1RootPort

  # CPU PEG Slot2 RTD3
  gBoardModuleTokenSpaceGuid.PcdPegSlot2PwrEnableGpioNo
  gBoardModuleTokenSpaceGuid.PcdPegSlot2PwrEnableGpioPolarity
  gBoardModuleTokenSpaceGuid.PcdPegSlot2RstGpioNo
  gBoardModuleTokenSpaceGuid.PcdPegSlot2RstGpioPolarity
  gBoardModuleTokenSpaceGuid.PcdPegSlot2WakeGpioPin
  gBoardModuleTokenSpaceGuid.PcdPegSlot2RootPort

  # CPU M.2 SSD Slot RTD3
  gBoardModuleTokenSpaceGuid.PcdPcieSsd2PwrEnableGpioNo
  gBoardModuleTokenSpaceGuid.PcdPcieSsd2RstGpioNo
  gBoardModuleTokenSpaceGuid.PcdPcieSsd2PwrEnableGpioPolarity
  gBoardModuleTokenSpaceGuid.PcdPcieSsd2RstGpioPolarity

  # PCIE SLOT 1 - X4 CONNECTOR RTD3
  gBoardModuleTokenSpaceGuid.PcdPcieSlot1HoldRstGpioNo
  gBoardModuleTokenSpaceGuid.PcdPcieSlot1HoldRstGpioPolarity
  gBoardModuleTokenSpaceGuid.PcdPcieSlot1PwrEnableGpioNo
  gBoardModuleTokenSpaceGuid.PcdPcieSlot1PwrEnableGpioPolarity
  gBoardModuleTokenSpaceGuid.PcdPcieSlot1WakeGpioPin
  gBoardModuleTokenSpaceGuid.PcdPcieSlot1RootPort

  # PCIE SLOT 2 - X4 CONNECTOR RTD3
  gBoardModuleTokenSpaceGuid.PcdPchPCIeSlot2PwrEnableGpioNo
  gBoardModuleTokenSpaceGuid.PcdPchPCIeSlot2PwrEnableGpioPolarity
  gBoardModuleTokenSpaceGuid.PcdPchPCIeSlot2RstGpioNo
  gBoardModuleTokenSpaceGuid.PcdPchPCIeSlot2RstGpioPolarity
  gBoardModuleTokenSpaceGuid.PcdPcieSlot2WakeGpioPin
  gBoardModuleTokenSpaceGuid.PcdPcieSlot2RootPort

  # PCIE SLOT 3 - X2 CONNECTOR RTD3
  gBoardModuleTokenSpaceGuid.PcdPchPCIeSlot3PwrEnableGpioNo
  gBoardModuleTokenSpaceGuid.PcdPchPCIeSlot3PwrEnableGpioPolarity
  gBoardModuleTokenSpaceGuid.PcdPchPCIeSlot3RstGpioNo
  gBoardModuleTokenSpaceGuid.PcdPchPCIeSlot3RstGpioPolarity
  gBoardModuleTokenSpaceGuid.PcdPcieSlot3WakeGpioPin
  gBoardModuleTokenSpaceGuid.PcdPcieSlot3RootPort

  # PCH M.2 SSD Slot 1RTD3
  gBoardModuleTokenSpaceGuid.PcdPchSsd1PwrEnableGpioNo
  gBoardModuleTokenSpaceGuid.PcdPchSsd1PwrEnableGpioPolarity
  gBoardModuleTokenSpaceGuid.PcdPchSsd1RstGpioNo
  gBoardModuleTokenSpaceGuid.PcdPchSsd1RstGpioPolarity

  # PCH M.2 SSD Slot 2RTD3
  gBoardModuleTokenSpaceGuid.PcdPchSsd2PwrEnableGpioNo
  gBoardModuleTokenSpaceGuid.PcdPchSsd2RstGpioNo
  gBoardModuleTokenSpaceGuid.PcdPchSsd2PwrEnableGpioPolarity
  gBoardModuleTokenSpaceGuid.PcdPchSsd2RstGpioPolarity

  # PCH M.2 SSD Slot 3RTD3
  gBoardModuleTokenSpaceGuid.PcdPchSsd3PwrEnableGpioNo
  gBoardModuleTokenSpaceGuid.PcdPchSsd3RstGpioNo
  gBoardModuleTokenSpaceGuid.PcdPchSsd3PwrEnableGpioPolarity
  gBoardModuleTokenSpaceGuid.PcdPchSsd3RstGpioPolarity

  # Onboard MR 1 RTD3
  gBoardModuleTokenSpaceGuid.PcdOnBoardMr1PowerEnableGpioNo        ## PRODUCES
  gBoardModuleTokenSpaceGuid.PcdOnBoardMr1PowerEnableGpioPolarity  ## PRODUCES
  gBoardModuleTokenSpaceGuid.PcdOnBoardMr1RstGpioNo                ## PRODUCES
  gBoardModuleTokenSpaceGuid.PcdOnBoardMr1RstGpioPolarity          ## PRODUCES
  gBoardModuleTokenSpaceGuid.PcdOnBoardMr1WakeGpioPin              ## PRODUCES
  gBoardModuleTokenSpaceGuid.PcdOnBoardMr1RootPort                 ## PRODUCES

  # PCH SATA port RTD3
  gBoardModuleTokenSpaceGuid.PcdPchSataPortPwrEnableGpioNo
  gBoardModuleTokenSpaceGuid.PcdPchSataPortPwrEnableGpioPolarity


  # WLAN
  gBoardModuleTokenSpaceGuid.PcdWlanWakeGpio
  gBoardModuleTokenSpaceGuid.PcdWlanRootPortNumber
  gBoardModuleTokenSpaceGuid.PcdBoardGpioTableCnvd
  gBoardModuleTokenSpaceGuid.PcdBoardGpioTableCnvdSize

  # FOXVILLE I225 LAN
  gBoardModuleTokenSpaceGuid.PcdFoxLanEnableGpio               ## PRODUCES
  gBoardModuleTokenSpaceGuid.PcdFoxLanWakeGpio                 ## PRODUCES
  gBoardModuleTokenSpaceGuid.PcdFoxLanDisableNGpio             ## PRODUCES
  gBoardModuleTokenSpaceGuid.PcdFoxLanResetGpio                ## PRODUCES
  gBoardModuleTokenSpaceGuid.PcdFoxLanDisableNGpioPolarity     ## PRODUCES
  gBoardModuleTokenSpaceGuid.PcdFoxLanResetGpioPolarity        ## PRODUCES
  gBoardModuleTokenSpaceGuid.PcdFoxlanRootPortNumber           ## PRODUCES

  # I2C Touch Panel 0 & 1 RTD3
  gBoardModuleTokenSpaceGuid.PcdTouchpanelPwrEnableGpio
  gBoardModuleTokenSpaceGuid.PcdTouchpanelRstGpio
  gBoardModuleTokenSpaceGuid.PcdTouchpanelPwrEnableGpioPolarity
  gBoardModuleTokenSpaceGuid.PcdTouchpanelRstGpioPolarity
  gBoardModuleTokenSpaceGuid.PcdTouchpadIrqGpioPolarity
  gBoardModuleTokenSpaceGuid.PcdTouchpanelIrqGpioPolarity
  gBoardModuleTokenSpaceGuid.PcdTouchpadIrqGpio
  gBoardModuleTokenSpaceGuid.PcdTouchpanelIrqGpio
  gBoardModuleTokenSpaceGuid.PcdTouchpanel1PwrEnableGpio
  gBoardModuleTokenSpaceGuid.PcdTouchpanel1RstGpio
  gBoardModuleTokenSpaceGuid.PcdTouchpanel1IrqGpio
  gBoardModuleTokenSpaceGuid.PcdTouchpanel1PwrEnableGpioPolarity
  gBoardModuleTokenSpaceGuid.PcdTouchpanel1RstGpioPolarity
  gBoardModuleTokenSpaceGuid.PcdTouchpanel1IrqGpioPolarity
  gBoardModuleTokenSpaceGuid.PcdWwanFullCardPowerOffGpio
  gBoardModuleTokenSpaceGuid.PcdWwanPerstGpio
  gBoardModuleTokenSpaceGuid.PcdWwanBbrstGpio
  gBoardModuleTokenSpaceGuid.PcdWwanPerstGpioPolarity

  gBoardModuleTokenSpaceGuid.PcdBoardId
  gBoardModuleTokenSpaceGuid.PcdBoardRev
  gBoardModuleTokenSpaceGuid.PcdPlatformFlavor            ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdSkuType
  gBoardModuleTokenSpaceGuid.VpdPcdBoardGpioTable

  gBoardModuleTokenSpaceGuid.PcdXhciAcpiTableSignature
  gBoardModuleTokenSpaceGuid.PcdPreferredPmProfile

  gBoardModuleTokenSpaceGuid.PcdFingerPrintIrqGpio
  gBoardModuleTokenSpaceGuid.PcdFingerPrintSleepGpio
  gBoardModuleTokenSpaceGuid.PcdTouchpanelIrqGpio
  gBoardModuleTokenSpaceGuid.PcdTouchpadIrqGpio

  gBoardModuleTokenSpaceGuid.PcdWwanModemBaseBandResetGpio
  gBoardModuleTokenSpaceGuid.PcdDiscreteBtModule
  gBoardModuleTokenSpaceGuid.PcdBtRfKillGpio
  gBoardModuleTokenSpaceGuid.PcdBtIrqGpio

  gBoardModuleTokenSpaceGuid.PcdPssReadSN
  gBoardModuleTokenSpaceGuid.PcdPssI2cBusNumber
  gBoardModuleTokenSpaceGuid.PcdPssI2cSlaveAddress

  gBoardModuleTokenSpaceGuid.PcdBoardRtd3TableSignature

  gBoardModuleTokenSpaceGuid.PcdEcSmiGpio
  gBoardModuleTokenSpaceGuid.PcdEcLowPowerExitGpio

  gBoardModuleTokenSpaceGuid.PcdSpdAddressOverride

  gBoardModuleTokenSpaceGuid.PcdUsbTypeCSupport
  gBoardModuleTokenSpaceGuid.PcdUsbTypeCEcLess
  gBoardModuleTokenSpaceGuid.PcdTypeCPortsSupported
  gBoardModuleTokenSpaceGuid.PcdUsbTypeCPort1
  gBoardModuleTokenSpaceGuid.PcdUsbTypeCPort1Pch
  gBoardModuleTokenSpaceGuid.PcdUsbCPort1Properties
  gBoardModuleTokenSpaceGuid.PcdUsbTypeCPort2
  gBoardModuleTokenSpaceGuid.PcdUsbTypeCPort2Pch
  gBoardModuleTokenSpaceGuid.PcdUsbCPort2Properties
  gBoardModuleTokenSpaceGuid.PcdUsbTypeCPort3
  gBoardModuleTokenSpaceGuid.PcdUsbTypeCPort3Pch
  gBoardModuleTokenSpaceGuid.PcdUsbCPort3Properties
  gBoardModuleTokenSpaceGuid.PcdUsbTypeCPort4
  gBoardModuleTokenSpaceGuid.PcdUsbTypeCPort4Pch
  gBoardModuleTokenSpaceGuid.PcdUsbCPort4Properties

  # PD PS_ON GPIO
  gBoardModuleTokenSpaceGuid.PcdUsbCPsonOverrideN            ## PRODUCE
  gBoardModuleTokenSpaceGuid.PcdUsbCPsonS0ixEntryReq         ## PRODUCE
  gBoardModuleTokenSpaceGuid.PcdUsbCPsonS0ixEntryAck         ## PRODUCE

  gBoardModuleTokenSpaceGuid.PcdBatteryPresent
  gBoardModuleTokenSpaceGuid.PcdRealBattery1Control
  gBoardModuleTokenSpaceGuid.PcdRealBattery2Control

  gBoardModuleTokenSpaceGuid.PcdMipiCamSensor
  gBoardModuleTokenSpaceGuid.PcdH8S2113SIO
  gBoardModuleTokenSpaceGuid.PcdH8S2113UAR
  gBoardModuleTokenSpaceGuid.PcdNCT6776FCOM
  gBoardModuleTokenSpaceGuid.PcdNCT6776FSIO
  gBoardModuleTokenSpaceGuid.PcdNCT6776FHWMON
  gBoardModuleTokenSpaceGuid.PcdZPoddConfig

  gBoardModuleTokenSpaceGuid.PcdSmcRuntimeSciPin
  gBoardModuleTokenSpaceGuid.PcdConvertableDockSupport

  gBoardModuleTokenSpaceGuid.PcdEcHotKeyF3Support
  gBoardModuleTokenSpaceGuid.PcdEcHotKeyF4Support
  gBoardModuleTokenSpaceGuid.PcdEcHotKeyF5Support
  gBoardModuleTokenSpaceGuid.PcdEcHotKeyF6Support
  gBoardModuleTokenSpaceGuid.PcdEcHotKeyF7Support
  gBoardModuleTokenSpaceGuid.PcdEcHotKeyF8Support

  gBoardModuleTokenSpaceGuid.PcdVirtualButtonVolumeUpSupport
  gBoardModuleTokenSpaceGuid.PcdVirtualButtonVolumeDownSupport
  gBoardModuleTokenSpaceGuid.PcdVirtualButtonHomeButtonSupport
  gBoardModuleTokenSpaceGuid.PcdVirtualButtonRotationLockSupport

  gBoardModuleTokenSpaceGuid.PcdSlateModeSwitchSupport
  gBoardModuleTokenSpaceGuid.PcdVirtualGpioButtonSupport
  gBoardModuleTokenSpaceGuid.PcdAcDcAutoSwitchSupport
  gBoardModuleTokenSpaceGuid.PcdPmPowerButtonGpioPin
  gBoardModuleTokenSpaceGuid.PcdAcpiEnableAllButtonSupport
  gBoardModuleTokenSpaceGuid.PcdAcpiHidDriverButtonSupport

  gBoardModuleTokenSpaceGuid.PcdBoardUcmcGpio1
  gBoardModuleTokenSpaceGuid.PcdBoardUcmcGpio2
  gBoardModuleTokenSpaceGuid.PcdBoardUcmcGpio3
  gBoardModuleTokenSpaceGuid.PcdBoardUcmcGpio4

  # eDP Display Mux GPIO
  gBoardModuleTokenSpaceGuid.PcdDisplayMuxGpioNo

  #DashG Enable
  gBoardModuleTokenSpaceGuid.PcdDashGEnable
#[-start-201104-IB17510119-add]#
  gChipsetPkgTokenSpaceGuid.PcdH2OGpioCfgSupported
  gBoardModuleTokenSpaceGuid.PcdDisableVpdGpioTable
#[-end-201104-IB17510119-add]#

[FixedPcd]
  gBoardModuleTokenSpaceGuid.PcdSetupEnable
