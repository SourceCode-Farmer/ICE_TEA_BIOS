/** @file
 Source code for the board SA configuration Pcd init functions in Pre-Memory init phase.

@copyright
  INTEL CONFIDENTIAL
  Copyright 2015 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains a 'Sample Driver' and is licensed as such under the terms
  of your license agreement with Intel or your vendor. This file may be modified
  by the user, subject to the additional terms of the license agreement.

@par Specification Reference:
**/

#include "BoardSaConfigPreMem.h"
#include <Library/CpuPlatformLib.h>

#include <Pins/GpioPinsVer4S.h>

#include <PlatformBoardId.h>
#include <Library/PreSiliconEnvDetectLib.h>
#include <PlatformBoardConfig.h>

/**
  MRC configuration init function for PEI pre-memory phase.

  @param[in]  VOID

  @retval VOID
**/
VOID
AdlSSaMiscConfigInit (
  VOID
  )
{
  PcdSet8S (PcdSaMiscFirstDimmBitMask, 0x0A); // Default is standard daisy-chain, Far DIMM is DIMM1 on each MC.
  PcdSet8S (PcdSaMiscFirstDimmBitMaskEcc, 0x0A); // Default is standard daisy-chain, Far DIMM is DIMM1 on each MC.

  switch (PcdGet16(PcdBoardId)) {
    case BoardIdAdlSAdpSDdr4UDimm2DCrbEv:
    case BoardIdAdlSAdpSDdr4UDimm2DCrbCpv:
    case BoardIdAdlSTgpHDdr5UDimm2DCrb:
    case BoardIdAdlSAdpSDdr5UDimm2DCrb:
    case BoardIdAdlSAdpSSbgaDdr5SODimmErb:
    case BoardIdAdlSAdpSSbgaDdr5SODimmCrbPpv:
      PcdSet8S (PcdSaMiscUserBd, 2);       // Default is standard daisy-chain, btDesktop2DpcDaisyChain
      break;

    case BoardIdAdlSAdpSSbgaDdr4SODimmCrb:
      PcdSet8S (PcdSaMiscUserBd, 3);       // btDesktop2DpcTeeTopologyAsymmetrical: T-topology with asymmetrical DIMM0/1 routing, has particular DIMM population order
      PcdSet8S (PcdSaMiscFirstDimmBitMask, 0x06); // This board has special population rules: first should be DIMM1 on MC0 and DIMM0 on MC1 - '0110'
      PcdSet8S (PcdSaMiscFirstDimmBitMaskEcc, 0x06); // This board has special population rules: first should be DIMM1 on MC0 and DIMM0 on MC1 - '0110'
      break;

    case BoardIdAdlSAdpSSbgaDdr5SODimmCrb:
      PcdSet8S (PcdSaMiscUserBd, 2);       // Default is standard daisy-chain, btDesktop2DpcDaisyChain
      PcdSet8S (PcdSaMiscFirstDimmBitMask, 0x0D); // This board has special population rules: 0x0D is 1101b and it means MC0 D0, and MC1 D0 or MC1 D1 need to be populated firstly.
      PcdSet8S (PcdSaMiscFirstDimmBitMaskEcc, 0x09); // This board has special population rules: first should be DIMM0 on MC0 and DIMM1 on MC1 - '1001'
      break;

    case BoardIdAdlSAdpSDdr4UDimm2DCrb:
      PcdSet8S (PcdSaMiscUserBd, 4);       // btDesktop2DpcTeeTopology: Desktop 2DPC, T-topology with symmetrical DIMM0/1 routing, no particular DIMM population order
      break;

    case BoardIdAdlSAdpSSbgaDdr5SODimmAep:
    default:
      // DDR4 / DDR5 1DPC
      PcdSet8S (PcdSaMiscUserBd, 1);       // btDesktop1Dpc
      break;
  }
  PcdSet16S (PcdSaDdrFreqLimit, 0);

  return;
}

/**
  Board Memory Init related configuration init function for PEI pre-memory phase.

  @param[in]  VOID

  @retval VOID
**/
VOID
AdlSMrcConfigInit (
  VOID
  )
{
  UINT16    BoardId;

  BoardId = PcdGet16(PcdBoardId);

  switch (BoardId) {
    case BoardIdAdlSAdpSDdr4UDimm2DCrb:
    case BoardIdAdlSAdpSDdr4UDimm2DCrbCpv:
    case BoardIdAdlSAdpSDdr5UDimm1DCrb:
    case BoardIdAdlSAdpSDdr5UDimm1DCrbPpv:
    case BoardIdAdlSAdpSDdr5UDimm1DAep:
    default:
      PcdSet8S (PcdMrcSpdAddressTable0, 0xA0);
      PcdSet8S (PcdMrcSpdAddressTable1, 0xA2);
      PcdSet8S (PcdMrcSpdAddressTable2, 0x0);
      PcdSet8S (PcdMrcSpdAddressTable3, 0x0);
      PcdSet8S (PcdMrcSpdAddressTable4, 0x0);
      PcdSet8S (PcdMrcSpdAddressTable5, 0x0);
      PcdSet8S (PcdMrcSpdAddressTable6, 0x0);
      PcdSet8S (PcdMrcSpdAddressTable7, 0x0);
      PcdSet8S (PcdMrcSpdAddressTable8, 0xA4);
      PcdSet8S (PcdMrcSpdAddressTable9, 0xA6);
      PcdSet8S (PcdMrcSpdAddressTable10, 0x0);
      PcdSet8S (PcdMrcSpdAddressTable11, 0x0);
      PcdSet8S (PcdMrcSpdAddressTable12, 0x0);
      PcdSet8S (PcdMrcSpdAddressTable13, 0x0);
      PcdSet8S (PcdMrcSpdAddressTable14, 0x0);
      PcdSet8S (PcdMrcSpdAddressTable15, 0x0);
      break;
  }

  // Setting the default DQ Byte Map. It may be overriden to board specific settings below.
  PcdSet32S (PcdMrcDqByteMap, (UINTN) DqByteMapAdlS);
  PcdSet16S (PcdMrcDqByteMapSize, sizeof (DqByteMapAdlS));

  // ICL uses the same RCOMP resistors for all DDR types
  PcdSet32S (PcdMrcRcompResistor, (UINTN) AdlSRcompResistorZero);

  // Use default RCOMP target values for all boards
  PcdSet32S (PcdMrcRcompTarget, (UINTN) RcompTargetAdlS);

  // Default is Interleaved
  PcdSetBoolS (PcdMrcDqPinsInterleavedControl, TRUE);
  PcdSetBoolS (PcdMrcDqPinsInterleaved, TRUE);

  // SPD is the same size for all boards
  PcdSet16S (PcdMrcSpdDataSize, sizeof (mLpddr4Ddp8Gb200bSpd));
  // DqsMapCpu2Dram is the same size for all boards
  PcdSet16S (PcdMrcDqsMapCpu2DramSize, sizeof (DqsMapCpu2DramAdlSLp4Type4Rvp));
  // DqMapCpu2Dram is the same size for all boards
  //PcdSet16S (PcdMrcDqMapCpu2DramSize, sizeof (DqMapCpu2DramAdlSLp4Type4));

  //
  // CA Vref routing: board-dependent
  // 0 - VREF_CA goes to both CH_A and CH_B (LPDDR3/DDR3L)
  // 1 - VREF_CA to CH_A, VREF_DQ_A to CH_B (should not be used)
  // 2 - VREF_CA to CH_A, VREF_DQ_B to CH_B (DDR4)
  //
  // Set it to 2 for all our DDR4 boards; it is ignored for LPDDR4
  //
  PcdSet8S (PcdMrcCaVrefConfig, 2);

  return;
}

/**
  Board SA related GPIO configuration init function for PEI pre-memory phase.

  @param[in]  VOID

  @retval VOID
**/
VOID
AdlSSaGpioConfigInit (
  VOID
  )
{
  UINT16    BoardId;
  BoardId = PcdGet16(PcdBoardId);

  //
  // Assigning default values to PCIE RTD3 GPIOs
  //
  PcdSet8S(PcdRootPortIndex, 0xFF);
  PcdSet8S(PcdPcieSlot1GpioSupport, NotSupported);
  PcdSet8S(PcdPcieSlot1HoldRstExpanderNo, 0);
  PcdSet32S(PcdPcieSlot1HoldRstGpioNo, 0);
  PcdSetBoolS(PcdPcieSlot1HoldRstGpioPolarity, 0);
  PcdSet8S(PcdPcieSlot1PwrEnableExpanderNo, 0);
  PcdSet32S(PcdPcieSlot1PwrEnableGpioNo, 0);
  PcdSetBoolS(PcdPcieSlot1PwrEnableGpioPolarity, 1);


  // @TODO Need to update the PEG RESET GPIOs

  //
  // PCIE RTD3 GPIO
  //
  PcdSet8S(PcdPcie0GpioSupport, NotSupported);
  PcdSet8S(PcdPcie0HoldRstExpanderNo, 0);
  PcdSet32S(PcdPcie0HoldRstGpioNo, 0);
  PcdSetBoolS(PcdPcie0HoldRstActive, FALSE);
  PcdSet8S(PcdPcie0PwrEnableExpanderNo, 0);
  PcdSet32S(PcdPcie0PwrEnableGpioNo, 0);
  PcdSetBoolS(PcdPcie0PwrEnableActive, FALSE);

  PcdSet8S(PcdPcie1GpioSupport, NotSupported);
  PcdSet8S(PcdPcie1HoldRstExpanderNo, 0);
  PcdSet32S(PcdPcie1HoldRstGpioNo, 0);
  PcdSetBoolS(PcdPcie1HoldRstActive, FALSE);
  PcdSet8S(PcdPcie1PwrEnableExpanderNo, 0);
  PcdSet32S(PcdPcie1PwrEnableGpioNo, 0);
  PcdSetBoolS(PcdPcie1PwrEnableActive, FALSE);

  PcdSet8S(PcdPcie2GpioSupport, NotSupported);
  PcdSet8S(PcdPcie2HoldRstExpanderNo, 0);
  PcdSet32S(PcdPcie2HoldRstGpioNo, 0);
  PcdSetBoolS(PcdPcie2HoldRstActive, FALSE);
  PcdSet8S(PcdPcie2PwrEnableExpanderNo, 0);
  PcdSet32S(PcdPcie2PwrEnableGpioNo, 0);
  PcdSetBoolS(PcdPcie2PwrEnableActive, FALSE);

  PcdSet8S(PcdPcie3GpioSupport, NotSupported);
  PcdSet8S(PcdPcie3HoldRstExpanderNo, 0);
  PcdSet32S(PcdPcie3HoldRstGpioNo, 0);
  PcdSetBoolS(PcdPcie3HoldRstActive, FALSE);
  PcdSet8S(PcdPcie3PwrEnableExpanderNo, 0);
  PcdSet32S(PcdPcie3PwrEnableGpioNo, 0);
  PcdSetBoolS(PcdPcie3PwrEnableActive, FALSE);

  switch (BoardId) {
    case BoardIdAdlSAdpSSbgaDdr5SODimmErb:
    case BoardIdAdlSAdpSSbgaDdr5SODimmCrb:
    case BoardIdAdlSAdpSSbgaDdr4SODimmCrb:
    case BoardIdAdlSAdpSSbgaDdr5SODimmAep:
      PcdSet8S(PcdPcie1GpioSupport, PchGpio);
      break;
    default:
      PcdSet8S(PcdPcie1GpioSupport, NotSupported);
      break;
  }
  return;
}

/**
  SA Display DDI configuration init function for PEI pre-memory phase.

  @param[in]  VOID

  @retval     VOID
**/
VOID
AdlSSaDisplayConfigInit (
  VOID
  )
{

  UINT16    BoardId;
  BoardId = PcdGet16(PcdBoardId);

  switch (BoardId) {
    case BoardIdAdlSAdpSSbgaDdr5SODimmErb:
    case BoardIdAdlSAdpSSbgaDdr5SODimmCrb:
      PcdSet32S (PcdSaDisplayConfigTable, (UINTN) mAdlSSbgaDdr5SodimmRowDisplayDdiConfig);
      PcdSet16S (PcdSaDisplayConfigTableSize, sizeof (mAdlSSbgaDdr5SodimmRowDisplayDdiConfig));
     break;
    case BoardIdAdlSAdpSSbgaDdr5SODimmAep:
      PcdSet32S (PcdSaDisplayConfigTable, (UINTN) mAdlSSbgaDdr5SodimmAepRowDisplayDdiConfig);
      PcdSet16S (PcdSaDisplayConfigTableSize, sizeof (mAdlSSbgaDdr5SodimmAepRowDisplayDdiConfig));
     break;
    case BoardIdAdlSAdpSSbgaDdr4SODimmCrb: // Same as ADL-S DDR5 SODIMM CRB
    case BoardIdAdlSAdpSDdr5SODimmCrb:
      PcdSet32S (PcdSaDisplayConfigTable, (UINTN) mAdlSDdr5SodimmRowDisplayDdiConfig);
      PcdSet16S (PcdSaDisplayConfigTableSize, sizeof (mAdlSDdr5SodimmRowDisplayDdiConfig));
     break;
    case BoardIdAdlSAdpSDdr4SODimmCrb:
      PcdSet32S (PcdSaDisplayConfigTable, (UINTN) mAdlSDdr4SodimmRowDisplayDdiConfig);
      PcdSet16S (PcdSaDisplayConfigTableSize, sizeof (mAdlSDdr4SodimmRowDisplayDdiConfig));
     break;
    case BoardIdAdlSAdpSDdr5UDimm1DAep:
      PcdSet32S (PcdSaDisplayConfigTable, (UINTN) mAdlSDdr5AepRowDisplayDdiConfig);
      PcdSet16S (PcdSaDisplayConfigTableSize, sizeof (mAdlSDdr5AepRowDisplayDdiConfig));
     break;
    case BoardIdAdlSAdpSDdr5UDimm1DOc:
      PcdSet32S (PcdSaDisplayConfigTable, (UINTN) mAdlSDdr5OcRowDisplayDdiConfig);
      PcdSet16S (PcdSaDisplayConfigTableSize, sizeof (mAdlSDdr5OcRowDisplayDdiConfig));
     break;
    case BoardIdAdlSAdpSDdr5UDimm2DCrb:
    case BoardIdAdlSAdpSDdr4UDimm2DCrb:
    case BoardIdAdlSAdpSDdr4UDimm2DCrbCpv:
    case BoardIdAdlSAdpSDdr5UDimm1DCrb:
    case BoardIdAdlSAdpSDdr5UDimm1DCrbPpv:
    default:
      PcdSet32S (PcdSaDisplayConfigTable, (UINTN) mAdlSDdr4RowDisplayDdiConfig);
      PcdSet16S (PcdSaDisplayConfigTableSize, sizeof (mAdlSDdr4RowDisplayDdiConfig));
      break;
  }
  return;
}

/**
  Board USB related configuration init function for PEI pre-memory phase.

  @param[in]  VOID

  @retval VOID
**/
VOID
AdlSSaUsbConfigInit (
  VOID
  )
{
  PcdSet8S (PcdCpuXhciPortSupportMap, 0x00);
  //
  // Update Cpu Xhci Port Enable Map PCD based on SaSetup Data and PcdCpuXhciPortSupportMap.
  //
  TcssUpdateCpuXhciPortEnableMapPcd ();

  return;
}
