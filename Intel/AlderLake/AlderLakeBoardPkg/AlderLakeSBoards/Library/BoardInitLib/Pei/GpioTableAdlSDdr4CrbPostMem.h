/** @file
  GPIO definition table for AlderLake S

@copyright
  INTEL CONFIDENTIAL
  Copyright (c) 2020 Intel Corporation. All rights reserved
  This software and associated documentation (if any) is furnished
  under a license and may only be used or copied in accordance
  with the terms of the license. Except as permitted by such
  license, no part of this software or documentation may be
  reproduced, stored in a retrieval system, or transmitted in any
  form or by any means without the express written consent of
  Intel Corporation.
  This file contains an 'Intel Peripheral Driver' and is
  licensed for Intel CPUs and chipsets under the terms of your
  license agreement with Intel or your vendor. This file may
  be modified by the user, subject to additional terms of the
  license agreement.
**/
#ifndef _ALDER_LAKE_S_DDR4_CRB_GPIO_TABLE_H_
#define _ALDER_LAKE_S_DDR4_CRB_GPIO_TABLE_H_

#include <Pins/GpioPinsVer4S.h>
#include <Library/GpioLib.h>
#include <Library/GpioConfig.h>

///
/// !!! GPIOs designated to Native Functions shall not be configured by Platform Code.
/// Native Pins shall be configured by Silicon Code (based on BIOS policies setting) or soft straps(set by CSME in FITc).
/// Configuring pins to native function in GPIO table would override the pin settings resulting in unexpected behavior.
///


static GPIO_INIT_CONFIG mAdlSCnvdGpioTable[] =
{
  {GPIO_VER4_S_GPP_J7,   {GpioPadModeGpio,    GpioHostOwnAcpi, GpioDirIn,    GpioOutDefault, GpioIntDefault,           GpioResumeReset,   GpioTermNone }} // NC
};

#endif //_ALDER_LAKE_S_DDR4_CRB_GPIO_TABLE_H_
