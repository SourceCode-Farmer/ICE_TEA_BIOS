/** @file
  AlderLake S RVP GPIO definition table for Pre-Memory Initialization

@copyright
  INTEL CONFIDENTIAL
  Copyright (c) 2020 - 2021 Intel Corporation. All rights reserved
  This software and associated documentation (if any) is furnished
  under a license and may only be used or copied in accordance
  with the terms of the license. Except as permitted by such
  license, no part of this software or documentation may be
  reproduced, stored in a retrieval system, or transmitted in any
  form or by any means without the express written consent of
  Intel Corporation.
  This file contains an 'Intel Peripheral Driver' and is
  licensed for Intel CPUs and chipsets under the terms of your
  license agreement with Intel or your vendor. This file may
  be modified by the user, subject to additional terms of the
  license agreement.
**/
#ifndef _ALDER_LAKE_S_DDR4_CRB_PREMEM_GPIO_TABLE_H_
#define _ALDER_LAKE_S_DDR4_CRB_PREMEM_GPIO_TABLE_H_

#include <Pins/GpioPinsVer4S.h>
#include <Library/GpioLib.h>
#include <Library/GpioConfig.h>

///
/// !!! GPIOs designated to Native Functions shall not be configured by Platform Code.
/// Native Pins shall be configured by Silicon Code (based on BIOS policies setting) or soft straps(set by CSME in FITc).
/// Configuring pins to native function in GPIO table would override the pin settings resulting in unexpected behavior.
///

GLOBAL_REMOVE_IF_UNREFERENCED GPIO_INIT_CONFIG mGpioTableEarlyPreMemAdlSDdr4UDimm2DCrb[] =
{
  { GPIO_VER4_S_GPP_F11,  {GpioPadModeGpio,   GpioHostOwnAcpi,   GpioDirOut,     GpioOutLow,      GpioIntDefault,          GpioPlatformReset,  GpioTermNone }}, // PCIe SLOT_1 RTD3 Reset MIPI60 (Rework)
  { GPIO_VER4_S_GPP_F12,  {GpioPadModeGpio,   GpioHostOwnAcpi,   GpioDirOut,     GpioOutLow,      GpioIntDefault,          GpioHostDeepReset,  GpioTermNone }}, // PCIe SLOT_2 RTD3 Reset MIPI60 (Rework)
  { GPIO_VER4_S_GPP_F13,  {GpioPadModeGpio,   GpioHostOwnAcpi,   GpioDirOut,     GpioOutLow,      GpioIntDefault,          GpioPlatformReset,  GpioTermNone }}, // PCIe SLOT_3 RTD3 Reset MIPI60 (Rework)
  { GPIO_VER4_S_GPP_E2,   {GpioPadModeGpio,   GpioHostOwnAcpi,   GpioDirOut,     GpioOutLow,      GpioIntDefault,          GpioPlatformReset,  GpioTermNone }}, // PEG_1 RTD3 Reset
  { GPIO_VER4_S_GPP_E3,   {GpioPadModeGpio,   GpioHostOwnAcpi,   GpioDirOut,     GpioOutLow,      GpioIntDefault,          GpioPlatformReset,  GpioTermNone }}, // PEG_2 RTD3 Reset Sinai DR0 (Rework)
  { GPIO_VER4_S_GPP_H16,  {GpioPadModeGpio,   GpioHostOwnAcpi,   GpioDirOut,     GpioOutHigh,     GpioIntDefault,          GpioPlatformReset,  GpioTermNone }}, // M.2 HBR SSD Power enable
  { GPIO_VER4_S_GPP_C2,   {GpioPadModeGpio,   GpioHostOwnAcpi,   GpioDirOut,     GpioOutHigh,     GpioIntDefault,          GpioResetDefault,   GpioTermNone }}, // M.2 SSD_1 Power enable Strap
  { GPIO_VER4_S_GPP_B22,  {GpioPadModeGpio,   GpioHostOwnAcpi,   GpioDirOut,     GpioOutHigh,     GpioIntDefault,          GpioPlatformReset,  GpioTermNone }}, // M.2 SSD_2 Power enable
  { GPIO_VER4_S_GPP_K2,   {GpioPadModeGpio,   GpioHostOwnAcpi,   GpioDirOut,     GpioOutHigh,     GpioIntDefault,          GpioPlatformReset,  GpioTermNone }}, // M.2 SSD_3 Power enable
  { GPIO_VER4_S_GPP_E0,   {GpioPadModeGpio,   GpioHostOwnAcpi,   GpioDirOut,     GpioOutHigh,     GpioIntDefault,          GpioPlatformReset,  GpioTermNone }}, // PEG SLOT_1 Power enable
  { GPIO_VER4_S_GPP_E1,   {GpioPadModeGpio,   GpioHostOwnAcpi,   GpioDirOut,     GpioOutHigh,     GpioIntDefault,          GpioPlatformReset,  GpioTermNone }}, // PEG SLOT_2 Power enable
  { GPIO_VER4_S_GPP_A14,  {GpioPadModeGpio,   GpioHostOwnAcpi,   GpioDirOut,     GpioOutHigh,     GpioIntDefault,          GpioResetDefault,   GpioTermNone }}, // PCIe SLOT_1 Power enable, enabled by Pull up on Fab1 board.
  { GPIO_VER4_S_GPP_H11,  {GpioPadModeGpio,   GpioHostOwnAcpi,   GpioDirOut,     GpioOutHigh,     GpioIntDefault,          GpioPlatformReset,  GpioTermNone }}, // PCIe SLOT_2 Power enable
  { GPIO_VER4_S_GPP_B21,  {GpioPadModeGpio,   GpioHostOwnAcpi,   GpioDirOut,     GpioOutHigh,     GpioIntDefault,          GpioPlatformReset,  GpioTermNone }}, // PCIe SLOT_3 Power enable
  { GPIO_VER4_S_GPP_G1,   {GpioPadModeGpio,   GpioHostOwnGpio,   GpioDirOut,     GpioOutHigh,     GpioIntDefault,          GpioHostDeepReset,  GpioTermNone }}, // Thunderbolt CIO_PWR_EN (CIO_PWR_EN needs set to high before PCIe SLOT_2 PERST set to high; at least 10ms)
  { GPIO_VER4_S_GPP_F4,   {GpioPadModeGpio,   GpioHostOwnGpio,   GpioDirIn,      GpioOutDefault,  GpioIntDefault,          GpioPlatformReset,  GpioTermWpu20K }}, //WA:20K weak pull-up on S03 Fab1
  { 0x0 }
};

GLOBAL_REMOVE_IF_UNREFERENCED GPIO_INIT_CONFIG mGpioTableEarlyPreMemAdlSDdr4UDimm2DCrbFab2[] =
{
  { GPIO_VER4_S_GPP_F22,  {GpioPadModeGpio,   GpioHostOwnAcpi,   GpioDirOut,     GpioOutHigh,     GpioIntDefault,          GpioResetDefault,   GpioTermNone }}, // PCIe SLOT_1  Power enable
  { GPIO_VER4_S_GPP_B21,  {GpioPadModeGpio,   GpioHostOwnAcpi,   GpioDirOut,     GpioOutHigh,     GpioIntDefault,          GpioPlatformReset,  GpioTermNone }}, // PCIe SLOT_3  Power enable
  { GPIO_VER4_S_GPP_H11,  {GpioPadModeGpio,   GpioHostOwnAcpi,   GpioDirOut,     GpioOutHigh,     GpioIntDefault,          GpioPlatformReset,  GpioTermNone }}, // PCIe SLOT_2 Power enable
  { GPIO_VER4_S_GPP_F11,  {GpioPadModeGpio,   GpioHostOwnAcpi,   GpioDirOut,     GpioOutLow,      GpioIntDefault,          GpioPlatformReset,  GpioTermNone }}, // PCIe SLOT_1 RTD3 Reset MIPI60 (Rework)
  { GPIO_VER4_S_GPP_F12,  {GpioPadModeGpio,   GpioHostOwnAcpi,   GpioDirOut,     GpioOutLow,      GpioIntDefault,          GpioHostDeepReset,  GpioTermNone }}, // PCIe SLOT_2 RTD3 Reset MIPI60 (Rework)
  { GPIO_VER4_S_GPP_F13,  {GpioPadModeGpio,   GpioHostOwnAcpi,   GpioDirOut,     GpioOutLow,      GpioIntDefault,          GpioPlatformReset,  GpioTermNone }}, // PCIe SLOT_3 RTD3 Reset MIPI60 (Rework)
  { GPIO_VER4_S_GPP_E0,   {GpioPadModeGpio,   GpioHostOwnAcpi,   GpioDirOut,     GpioOutHigh,     GpioIntDefault,          GpioPlatformReset,  GpioTermNone }}, // PEG SLOT_1 Power enable
  { GPIO_VER4_S_GPP_E1,   {GpioPadModeGpio,   GpioHostOwnAcpi,   GpioDirOut,     GpioOutHigh,     GpioIntDefault,          GpioPlatformReset,  GpioTermNone }}, // PEG SLOT_2 Power enable
  { GPIO_VER4_S_GPP_E2,   {GpioPadModeGpio,   GpioHostOwnAcpi,   GpioDirOut,     GpioOutLow,      GpioIntDefault,          GpioPlatformReset,  GpioTermNone }}, // PEG_1 RTD3 Reset
  { GPIO_VER4_S_GPP_E3,   {GpioPadModeGpio,   GpioHostOwnAcpi,   GpioDirOut,     GpioOutLow,      GpioIntDefault,          GpioPlatformReset,  GpioTermNone }}, // PEG_2 RTD3 Reset Sinai DR0 (Rework)
  { GPIO_VER4_S_GPP_H16,  {GpioPadModeGpio,   GpioHostOwnAcpi,   GpioDirOut,     GpioOutHigh,     GpioIntDefault,          GpioPlatformReset,  GpioTermNone }}, // M.2 HBR SSD Power enable
  { GPIO_VER4_S_GPP_C2,   {GpioPadModeGpio,   GpioHostOwnAcpi,   GpioDirOut,     GpioOutHigh,     GpioIntDefault,          GpioResetDefault,   GpioTermNone }}, // M.2 SSD_1 Power enable Strap
  { GPIO_VER4_S_GPP_B22,  {GpioPadModeGpio,   GpioHostOwnAcpi,   GpioDirOut,     GpioOutHigh,     GpioIntDefault,          GpioPlatformReset,  GpioTermNone }}, // M.2 SSD_2 Power enable
  { GPIO_VER4_S_GPP_K2,   {GpioPadModeGpio,   GpioHostOwnAcpi,   GpioDirOut,     GpioOutHigh,     GpioIntDefault,          GpioPlatformReset,  GpioTermNone }}, // M.2 SSD_3 Power enable
  { GPIO_VER4_S_GPP_G1,   {GpioPadModeGpio,   GpioHostOwnGpio,   GpioDirOut,     GpioOutHigh,     GpioIntDefault,          GpioHostDeepReset,  GpioTermNone }}, // Thunderbolt CIO_PWR_EN (CIO_PWR_EN needs set to high before PCIe SLOT_2 PERST set to high; at least 10ms)
  { GPIO_VER4_S_GPP_F4,   {GpioPadModeGpio,   GpioHostOwnGpio,   GpioDirIn,      GpioOutDefault,  GpioIntDefault,          GpioPlatformReset,  GpioTermWpu20K }}, //WA:20K weak pull-up on S03 Fab2
  { 0x0 }
};


GLOBAL_REMOVE_IF_UNREFERENCED GPIO_INIT_CONFIG mGpioTableEarlyPreMemAdlSSB3Ddr4SODimmCrb[] =
{
  {GPIO_VER4_S_GPP_K11,  {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,     GpioOutHigh,     GpioIntDefault,          GpioPlatformReset,  GpioTermNone}}, // PCH M.2 SSD Power enable
  {GPIO_VER4_S_GPP_H16,  {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,     GpioOutHigh,     GpioIntDefault,          GpioPlatformReset,  GpioTermNone}}, // CPU M.2 SSD Power enable
  {GPIO_VER4_S_GPP_F18,  {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,     GpioOutLow,      GpioIntDefault,          GpioPlatformReset,  GpioTermNone}}, // PEG_RTD3_COLD_OFF
  {GPIO_VER4_S_GPP_E2,   {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,     GpioOutLow,      GpioIntDefault,          GpioPlatformReset,  GpioTermNone}}, // PEG slot Reset
  {GPIO_VER4_S_GPP_E1,   {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,     GpioOutHigh,     GpioIntDefault,          GpioPlatformReset,  GpioTermNone}}, // PCH x4 Slot Power enable
  {GPIO_VER4_S_GPP_F22,  {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,     GpioOutLow,      GpioIntDefault,          GpioResetDefault,   GpioTermNone}}, // PCIESLOT_1_RST_N
  {GPIO_VER4_S_GPP_C10,  {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,     GpioOutLow,      GpioIntDefault,          GpioPlatformReset,  GpioTermNone}}, // PCH SSD Reset
  {GPIO_VER4_S_GPP_F11,  {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,     GpioOutLow,      GpioIntDefault,          GpioPlatformReset,  GpioTermNone}}, // CPU SSD reset
  {GPIO_VER4_S_GPP_F5,   {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutHigh,     GpioIntDefault,          GpioHostDeepReset,  GpioTermNone}}, // POR Thunderbolt RTD3 enable, MIPI60 (Rework)
  {GPIO_VER4_S_GPP_F16,  {GpioPadModeGpio, GpioHostOwnGpio, GpioDirOut,     GpioOutLow,      GpioIntDefault,          GpioHostDeepReset,  GpioTermNone}}, // POR Thunderbolt Reset,MIPI60 (Rework)
  {0x0}
};


GLOBAL_REMOVE_IF_UNREFERENCED GPIO_INIT_CONFIG mGpioTableAdlSSB3WwanOffEarlyPreMem[] =
{
  { GPIO_VER4_S_GPP_I7,  {GpioPadModeGpio, GpioHostOwnGpio,     GpioDirOut,  GpioOutLow,       GpioIntDefault,        GpioResetDefault,  GpioTermNone, GpioOutputStateUnlock }}, // WWAN_RST_N
  { GPIO_VER4_S_GPP_B6,  {GpioPadModeGpio, GpioHostOwnGpio,     GpioDirOut,  GpioOutLow,       GpioIntDefault,        GpioPlatformReset, GpioTermNone, GpioOutputStateUnlock }}, // WWAN_PERST_N
  { GPIO_VER4_S_GPP_F4,  {GpioPadModeGpio, GpioHostOwnGpio,     GpioDirOut,  GpioOutLow,       GpioIntDefault,        GpioPlatformReset, GpioTermNone, GpioOutputStateUnlock }}, // WWAN_FCP_OFF_N
};

GLOBAL_REMOVE_IF_UNREFERENCED GPIO_INIT_CONFIG mGpioTableAdlSSB3WwanOnEarlyPreMem[] =
{
  // WWAN
  // Please don't change the sequence of Gpio Initialization
  {GPIO_VER4_S_GPP_B21,  {GpioPadModeGpio, GpioHostOwnGpio,     GpioDirOut,   GpioOutHigh,     GpioIntDefault,          GpioResetDefault,  GpioTermNone, GpioOutputStateUnlock }},  // WWAN_PWREN
  {GPIO_VER4_S_GPP_F4,   {GpioPadModeGpio, GpioHostOwnGpio,     GpioDirOut,   GpioOutHigh,     GpioIntDefault,          GpioPlatformReset, GpioTermNone, GpioOutputStateUnlock }},  // WWAN_FCP_OFF_N
  {GPIO_VER4_S_GPP_I7,   {GpioPadModeGpio, GpioHostOwnGpio,     GpioDirOut,   GpioOutHigh,     GpioIntDefault,          GpioResetDefault,  GpioTermNone, GpioOutputStateUnlock }},  // WWAN_RST_N
  {GPIO_VER4_S_GPP_B6,   {GpioPadModeGpio, GpioHostOwnGpio,     GpioDirOut,   GpioOutHigh,     GpioIntDefault,          GpioPlatformReset, GpioTermNone, GpioOutputStateUnlock }},  // WWAN_PERST_N
  {GPIO_VER4_S_GPP_H13,  {GpioPadModeGpio, GpioHostOwnAcpi,     GpioDirIn,    GpioOutDefault,  GpioIntLevel|GpioIntSci, GpioResetDefault,  GpioTermNone, GpioPadConfigUnlock   }},  // WWAN_WAKE_N
  {GPIO_VER4_S_GPP_E3,   {GpioPadModeGpio, GpioHostOwnGpio,     GpioDirOut,   GpioOutHigh,     GpioIntDefault,          GpioPlatformReset, GpioTermNone, GpioOutputStateUnlock }},  // WWAN_DISABLE_N
  {GPIO_VER4_S_GPP_G4,   {GpioPadModeGpio, GpioHostOwnGpio,     GpioDirOut,   GpioOutDefault,  GpioIntDefault,          GpioResetDefault,  GpioTermNone, GpioOutputStateUnlock }},  // SAR_DPR_PCH
};

GLOBAL_REMOVE_IF_UNREFERENCED GPIO_INIT_CONFIG mGpioTableAdlSSB3WwanM80EarlyPreMem[] =
{
  // WWAN
  // Please don't change the sequence of Gpio Initialization
  {GPIO_VER4_S_GPP_B21,  {GpioPadModeGpio, GpioHostOwnGpio,     GpioDirOut,   GpioOutHigh,     GpioIntDefault,          GpioResetDefault,  GpioTermNone, GpioOutputStateUnlock }},  // WWAN_PWREN
  {GPIO_VER4_S_GPP_F4 ,  {GpioPadModeGpio, GpioHostOwnGpio,     GpioDirOut,   GpioOutHigh,     GpioIntDefault,          GpioPlatformReset, GpioTermNone, GpioOutputStateUnlock }},  // WWAN_FCP_OFF_N
  {GPIO_VER4_S_GPP_I7,   {GpioPadModeGpio, GpioHostOwnGpio,     GpioDirOut,   GpioOutLow,      GpioIntDefault,          GpioResetDefault,  GpioTermNone, GpioOutputStateUnlock }},  // WWAN_RST_N
  {GPIO_VER4_S_GPP_B6,   {GpioPadModeGpio, GpioHostOwnGpio,     GpioDirOut,   GpioOutLow,      GpioIntDefault,          GpioPlatformReset, GpioTermNone, GpioOutputStateUnlock }},  // WWAN_PERST_N
  {GPIO_VER4_S_GPP_H13,  {GpioPadModeGpio, GpioHostOwnAcpi,     GpioDirIn,    GpioOutDefault,  GpioIntLevel|GpioIntSci, GpioResetDefault,  GpioTermNone, GpioPadConfigUnlock   }},  // WWAN_WAKE_N
  {GPIO_VER4_S_GPP_E3,   {GpioPadModeGpio, GpioHostOwnGpio,     GpioDirOut,   GpioOutHigh,     GpioIntDefault,          GpioPlatformReset, GpioTermNone, GpioOutputStateUnlock }},  // WWAN_DISABLE_N
  {GPIO_VER4_S_GPP_G4,   {GpioPadModeGpio, GpioHostOwnGpio,     GpioDirOut,   GpioOutDefault,  GpioIntDefault,          GpioResetDefault,  GpioTermNone, GpioOutputStateUnlock }},  // SAR_DPR_PCH
};


#endif // _ALDER_LAKE_S_DDR4_CRB_PREMEM_GPIO_TABLE_H_
