/** @file

;******************************************************************************
;* Copyright (c) 2020, Insyde Software Corporation. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/
/** @file

@copyright
  INTEL CONFIDENTIAL
  Copyright 2017 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains a 'Sample Driver' and is licensed as such under the terms
  of your license agreement with Intel or your vendor. This file may be modified
  by the user, subject to the additional terms of the license agreement.

@par Specification Reference:
**/

#include <PiPei.h>
#include <Library/BaseMemoryLib.h>
#include <Library/BiosIdLib.h>
#include <Library/PcdLib.h>
#include <Library/DebugLib.h>
#include <Library/PciLib.h>
#include <Library/MemoryAllocationLib.h>
#include <Library/PeiLib.h>
#include <Guid/MemoryOverwriteControl.h>
#include <Library/MmioInitLib.h>
#include <PlatformBoardConfig.h>
#include <Library/SiliconInitLib.h>
#include <Library/PchCycleDecodingLib.h>
#include <Register/PmcRegs.h>
#include <Library/PmcLib.h>
#include <Library/PeiBootModeLib.h>
#include <Ppi/ReadOnlyVariable2.h>
#include <Library/PeiServicesLib.h>
#include <Library/WakeupEventLib.h>
#include <AlderLakeSBoardConfigPatchTable.h>
#include <Library/GpioLib.h>
#include <Library/BoardConfigLib.h>
#include <Library/TimerLib.h>
#include <SioRegs.h>
#include <Library/IoLib.h>
#include <Library/PreSiliconEnvDetectLib.h>
#include <PlatformBoardId.h>
#include <Library/CpuPlatformLib.h>
#include <Pins/GpioPinsVer4S.h>
//[-start-210512-18770027-modify]//
//#include <Library/UefiVariableDefaultHobLib.h>
//[-end-210512-18770027-modify]//
#include <Library/DccProgramLib.h>

//[-start-201104-IB17510119-add]//
#include <Library/GpioCfgLib.h>
#include <Library/GpioNativeLib.h>
#include <Library/PeiOemSvcKernelLib.h>
//[-end-201104-IB17510119-add]//
//[-start-191001-16990100-add]//
#include <Library/PeiOemSvcChipsetLib.h>
//[-end-191001-16990100-add]//

#define SIO_RUNTIME_REG_BASE_ADDRESS      0x0680

static EFI_PEI_PPI_DESCRIPTOR mSetupVariablesReadyPpi = {
  (EFI_PEI_PPI_DESCRIPTOR_PPI | EFI_PEI_PPI_DESCRIPTOR_TERMINATE_LIST),
  &gSetupVariablesReadyPpiGuid,
  NULL
};

//[-start-211217-IB19270004-add]//
STATIC
EFI_STATUS
GpioCfgPei (
  IN H2O_BOARD_ID               Board,
  IN BOOLEAN                    PreMemFlag
  );
//[-end-211217-IB19270004-add]//

#if FixedPcdGet8(PcdFspModeSelection) == 0

//[-start-210120-IB16560239-modify]//
/**
  Pcie rst pin init function for PEI pre-memory phase.
**/
EFI_STATUS
EFIAPI
AdlSPciePinInit (
  IN EFI_PEI_SERVICES          **PeiServices,
  IN EFI_PEI_NOTIFY_DESCRIPTOR *NotifyDesc,
  IN VOID                      *Ppi
  )
{
  UINT16                GpioTableSizePreMem;
  GPIO_INIT_CONFIG      *GpioTablePreMem;
  EFI_STATUS	          Status;
  EFI_STATUS	          OemSvcStatus;
  UINT64                BoardId;
  
  GpioTablePreMem     = NULL;
  GpioTableSizePreMem = 0;
  BoardId             = 0;

//  GpioInit (PcdGetPtr (PcdBoardGpioTablePreMem));
  if (PcdGetBool (PcdH2OGpioCfgSupported) && !PcdGetBool(PcdDisableVpdGpioTable)) {
    //
    // Program GPIOs, Input Board value corresponds to Skuids in Project.dsc
    //
    BoardId = PcdGet16(PcdBoardId);
    DEBUG ((DEBUG_INFO, "GpioCfgPei() Start\n"));
    Status = GpioCfgPei ((H2O_BOARD_ID)BoardId, TRUE);
    DEBUG ((DEBUG_INFO, "GpioCfgPei(), Status = %r END\n", Status));
  } else {
    GpioTablePreMem = PcdGetPtr (PcdBoardGpioTablePreMem);
    GetGpioTableSize (GpioTablePreMem, &GpioTableSizePreMem);
    DEBUG_OEM_SVC ((DEBUG_INFO, "Pei OemChipsetServices Call: OemSvcModifyGpioSettingTablePreMem \n"));
    OemSvcStatus = OemSvcModifyGpioSettingTablePreMem (&GpioTablePreMem, &GpioTableSizePreMem);
    DEBUG_OEM_SVC ((DEBUG_INFO, "Pei OemChipsetServices OemSvcModifyGpioSettingTablePreMem Status: %r\n", OemSvcStatus));

    if (GpioTableSizePreMem != 0 && OemSvcStatus != EFI_SUCCESS) {
      GpioConfigurePads ((UINT32) GpioTableSizePreMem, GpioTablePreMem);
    }
  }
  return EFI_SUCCESS;
}
//[-end-210120-IB16560239-modify]//

static EFI_PEI_NOTIFY_DESCRIPTOR mNotifyPciePinPpi = {
  EFI_PEI_PPI_DESCRIPTOR_NOTIFY_CALLBACK | EFI_PEI_PPI_DESCRIPTOR_TERMINATE_LIST,
  &gPeiBeforeGraphicsDetectionPpiGuid,
  AdlSPciePinInit
};

#endif

/**
  Alderlake S boards configuration init function for PEI pre-memory phase.

  @retval EFI_SUCCESS             The function completed successfully.
**/
EFI_STATUS
EFIAPI
AdlSInitPreMem (
  VOID
  )
{
  EFI_STATUS                        Status;
  UINTN                             VariableSize;
  VOID                              *MemorySavedData;
  UINT8                             MorControl;
  VOID                              *MorControlPtr;
  EFI_PEI_READ_ONLY_VARIABLE2_PPI   *VariableServices;

  Status = PeiServicesLocatePpi (
             &gEfiPeiReadOnlyVariable2PpiGuid,
             0,
             NULL,
             (VOID **) &VariableServices
             );
  ASSERT_EFI_ERROR (Status);
  //
  // Initialize S3 Data variable (S3DataPtr). It may be used for warm and fast boot paths.
  //
  VariableSize = 0;
  MemorySavedData = NULL;
  Status = VariableServices->GetVariable (
                               VariableServices,
                               L"MemoryConfig",
                               &gFspNonVolatileStorageHobGuid,
                               NULL,
                               &VariableSize,
                               MemorySavedData
                               );
  if (Status == EFI_BUFFER_TOO_SMALL) {
    //
    // Set the DISB bit
    // after memory Data is saved to NVRAM.
    //
    PmcSetDramInitScratchpad ();
  }

  //
  // MOR
  //
  MorControl = 0;
  MorControlPtr = &MorControl;
  VariableSize = sizeof (MorControl);
  Status = PeiGetVariable (
             MEMORY_OVERWRITE_REQUEST_VARIABLE_NAME,
             &gEfiMemoryOverwriteControlDataGuid,
             &MorControlPtr,
             &VariableSize
             );
  DEBUG ((DEBUG_INFO, "MorControl - 0x%x (%r)\n", MorControl, Status));
  if (MOR_CLEAR_MEMORY_VALUE (MorControl)) {
    PcdSet8S (PcdCleanMemory, MorControl & MOR_CLEAR_MEMORY_BIT_MASK);
  }

  PcdSet32S (PcdStackBase, PcdGet32 (PcdTemporaryRamBase) + PcdGet32 (PcdTemporaryRamSize) - (PcdGet32 (PcdFspTemporaryRamSize) + PcdGet32 (PcdFspReservedBufferSize)));
  PcdSet32S (PcdStackSize, PcdGet32 (PcdFspTemporaryRamSize));

  PcdSet8S (PcdCpuRatio, 0x0);
  PcdSet8S (PcdBiosGuard, 0x0);

  return EFI_SUCCESS;
}

/**
  Updates the wakeupType.
**/
VOID
AdlSWakeUpTypeUpdate (
  VOID
  )
{
  UINT8   WakeupType;
  //
  // Updates the wakeupType which will be used to update the same in Smbios table 01
  //
  GetWakeupEvent (&WakeupType);
  PcdSet8S (PcdWakeupType, WakeupType);
}

VOID
AdlSMrcConfigInit (
  VOID
  );

VOID
AdlSSaMiscConfigInit (
  VOID
  );

VOID
AdlSSaGpioConfigInit (
  VOID
  );

VOID
AdlSSaDisplayConfigInit (
  VOID
  );

VOID
AdlSSaUsbConfigInit (
  VOID
  );

EFI_STATUS
AdlSRootPortClkInfoInit (
  VOID
  );

EFI_STATUS
AdlSUsbConfigInit (
  VOID
  );

VOID
AdlSGpioTablePreMemInit(
  VOID
  );

VOID
AdlSGpioGroupTierInit (
  VOID
  );

/**
  HSIO init function for PEI pre-memory phase.
**/
VOID
AdlSHsioInit (
  VOID
  )
{
}

/**
  Configure Super IO
**/
STATIC
VOID
AdlSSioInit (
  VOID
  )
{
  //
  // Program and Enable Default Super IO Configuration Port Addresses and range
  //
  PchLpcGenIoRangeSet (PcdGet16 (PcdLpcSioConfigDefaultPort) & (~0xF), 0x10);

  //
  // Enable LPC decode for KCS and mailbox SIO for iBMC communication
  //
  if (PcdGet8 (PcdPlatformFlavor) == FlavorUpServer) {
    PchLpcGenIoRangeSet (BMC_KCS_BASE_ADDRESS, 0x10);
    PchLpcGenIoRangeSet (PILOTIII_MAILBOX_BASE_ADDRESS, 0x10);
  } else {
  //
  // 128 Byte Boundary and SIO Runtime Register Range is 0x0 to 0xF;
  //
    PchLpcGenIoRangeSet (SIO_RUNTIME_REG_BASE_ADDRESS  & (~0x7F), 0x10);
  }

  //
  // We should not depend on SerialPortLib to initialize KBC for legacy USB
  // So initialize KBC for legacy USB driver explicitly here.
  // After we find how to enable mobile KBC, we will add enabling code for mobile then.
  //
  if ((PcdGet8 (PcdPlatformFlavor) == FlavorDesktop) ||
      (PcdGet8 (PcdPlatformFlavor) == FlavorWorkstation)) {
    //
    // Enable KBC for National PC8374 SIO
    //
    if (PcdGetBool (PcdPc8374SioKbcPresent)) {
      IoWrite8 (PcdGet16 (PcdDesktopLpcSioIndexDefaultPort), 0x07);
      IoWrite8 (PcdGet16 (PcdDesktopLpcSioDataDefaultPort), 0x06);
      IoWrite8 (PcdGet16 (PcdDesktopLpcSioIndexDefaultPort), 0x30);
      IoWrite8 (PcdGet16 (PcdDesktopLpcSioDataDefaultPort), 0x01);
    }
  }

  return;
}

/**
  Detect clock source from Integrated CPU BCLK or discrete BCLK

  @reval  TRUE               BCLK from discrete clock
  @reval  FALSE              BCLK from integrated clock
**/
BOOLEAN
AdlSExternalClockSupport (
  VOID
)
{
  if (CheckDccClkChipExist () == FALSE) {
    DEBUG ((DEBUG_INFO, "Discrete Clock Controller doesn't exist ... \n"));
    return FALSE;
  }

  if (PmcDetectIntegratedClockSource () == TRUE) {
    DEBUG ((DEBUG_INFO, "CPU BCLK Source is from integrated clock\n"));
    DisableExternalClockOutput ();
    return FALSE;
  }

  DEBUG ((DEBUG_INFO, "Discrete Clock Supported\n"));
  return TRUE;
}

/**
  This function is the entry point of discrete clocking module.

**/
//[-start-210512-18770027-modify]//
/*
//[-end-210512-18770027-modify]//
VOID
DiscreteClockConfigInit (
  VOID
 )
{
  EFI_PEI_READ_ONLY_VARIABLE2_PPI *ReadOnlyVariable;
  SETUP_DATA                      Setupdata;
  UINTN                           VariableSize;
  EFI_STATUS                      Status;
  EFI_BOOT_MODE                   BootMode;
  UINT32                          Attributes;


  if (PcdGet16 (PcdBoardId) != BoardIdAdlSAdpSDdr5UDimm1DOc) {
    DEBUG ((DEBUG_INFO, "It's not ADL S OC RVP ... \n"));
    return;
  }

  DEBUG ((DEBUG_INFO, "Pei Discrete Clock Controller module Entry ... \n"));

  PeiServicesGetBootMode (&BootMode);

  if (BootMode == BOOT_IN_RECOVERY_MODE || BootMode == BOOT_WITH_DEFAULT_SETTINGS) {
    VariableSize = sizeof (SETUP_DATA);
    Status = GetVariableFromHob (
               L"Setup",
               &gSetupVariableGuid,
               &Attributes,
               &VariableSize,
               &Setupdata
               );

    if (EFI_ERROR (Status)) {
      DEBUG ((DEBUG_ERROR, "Fail to GetVariableFromHob, Status = %r\n", Status));
      return;
    }

    Setupdata.DccClkCtrl = AdlSExternalClockSupport ();

    Status = SetVariableToHob (
             L"Setup",
             &gSetupVariableGuid,
             &Attributes,
             VariableSize,
             &Setupdata
             );

    if (EFI_ERROR (Status)) {
      DEBUG ((DEBUG_ERROR, "Fail to SetVariableToHob, Status = %r\n", Status));
      return;
    }
  }

  //
  // Get the user preferences from setup to local data structures
  //

  Status = PeiServicesLocatePpi (
             &gEfiPeiReadOnlyVariable2PpiGuid,
             0,
             NULL,
             (VOID **) &ReadOnlyVariable
             );

  if (!EFI_ERROR (Status)) {
    VariableSize = sizeof (SETUP_DATA);
    Status = ReadOnlyVariable->GetVariable (
                                 ReadOnlyVariable,
                                 L"Setup",
                                 &gSetupVariableGuid,
                                 NULL,
                                 &VariableSize,
                                 &Setupdata
                                 );
  }

  if (EFI_ERROR (Status)) {
    DEBUG ((DEBUG_ERROR, "Failed to Get setup data ... \n"));
    return;
  }

  if (Setupdata.DccClkCtrl == 0) {
    return;
  }

  ProgramCpuBclkFreq (Setupdata.CpuBclkFreq);
  ProgramPegDmiClkFreq (Setupdata.PegDmiClkFreq);

  return;
}
//[-start-210512-18770027-modify]//
*/
//[-end-210512-18770027-modify]//

/**
  Notifies the gPatchConfigurationDataPreMemPpiGuid has been Installed

  @param[in] PeiServices          General purpose services available to every PEIM.
  @param[in] NotifyDescriptor     The notification structure this PEIM registered on install.
  @param[in] Ppi                  The memory discovered PPI.  Not used.

  @retval EFI_SUCCESS             The function completed successfully.
**/
EFI_STATUS
EFIAPI
AdlSBoardPatchConfigurationDataPreMemCallback (
  IN EFI_PEI_SERVICES           **PeiServices,
  IN EFI_PEI_NOTIFY_DESCRIPTOR  *NotifyDescriptor,
  IN VOID                       *Ppi
  )
{
//[-start-200225-IB14630334-remove]//
//  UINTN   ConfigPatchStructSize;
//  UINT16     BoardId;
//  BoardId = PcdGet16(PcdBoardId);
//  switch (BoardId){
//    case BoardIdAdlSAdpSDdr5UDimm1DAep:
//      ConfigPatchStructSize = SIZE_OF_TABLE (mAlderlakeSAdpSDdr5UDimm1DAepConfigPatchStruct, CONFIG_PATCH_STRUCTURE);
//      PatchSetupConfigurationDataPreMem (mAlderlakeSAdpSDdr5UDimm1DAepConfigPatchStruct, ConfigPatchStructSize);
//      break;
//
//  case BoardIdAdlSAdpSDdr5SODimmCrb:
//  case BoardIdAdlSAdpSDdr4SODimmCrb:
//      ConfigPatchStructSize = SIZE_OF_TABLE (mAlderlakeSSodimmRvpConfigPatchStruct, CONFIG_PATCH_STRUCTURE);
//      PatchSetupConfigurationDataPreMem (mAlderlakeSSodimmRvpConfigPatchStruct, ConfigPatchStructSize);
//      break;
//
//  case BoardIdAdlSAdpSSbgaDdr5SODimmErb:
//  case BoardIdAdlSAdpSSbgaDdr5SODimmCrb:
//  case BoardIdAdlSAdpSSbgaDdr5SODimmAep:
//  case BoardIdAdlSAdpSSbgaDdr5SODimmCrbPpv:
//      ConfigPatchStructSize = SIZE_OF_TABLE (mAlderlakeSbgaDdr5SODimmErbConfigPatchStruct, CONFIG_PATCH_STRUCTURE);
//      PatchSetupConfigurationDataPreMem (mAlderlakeSbgaDdr5SODimmErbConfigPatchStruct, ConfigPatchStructSize);
//      if(BoardId == BoardIdAdlSAdpSSbgaDdr5SODimmAep) {
//        ConfigPatchStructSize = SIZE_OF_TABLE (mAlderlakeSbgaDdr5SODimmAepConfigPatchStruct, CONFIG_PATCH_STRUCTURE);
//        PatchSetupConfigurationDataPreMem (mAlderlakeSbgaDdr5SODimmAepConfigPatchStruct, ConfigPatchStructSize);
//      }
//    break;
//
//  case BoardIdAdlSAdpSSbgaDdr4SODimmCrb:
//      ConfigPatchStructSize = SIZE_OF_TABLE (mAlderlakeSbgaDdr4SODimmErbConfigPatchStruct, CONFIG_PATCH_STRUCTURE);
//      PatchSetupConfigurationDataPreMem (mAlderlakeSbgaDdr4SODimmErbConfigPatchStruct, ConfigPatchStructSize);
//      break;
//
//  case BoardIdAdlSAdpSDdr5UDimm1DOc:
//    default:
//      ConfigPatchStructSize = SIZE_OF_TABLE(mAlderlakeSDdr4RvpConfigPatchStruct, CONFIG_PATCH_STRUCTURE);
//      PatchSetupConfigurationDataPreMem(mAlderlakeSDdr4RvpConfigPatchStruct, ConfigPatchStructSize);
//      break;
//  }
//
//  if (!IsSimicsEnvironment ()) {
//    ConfigPatchStructSize = SIZE_OF_TABLE (mAlderlakeSDdr4RvpRealSiliconConfigPatchStruct, CONFIG_PATCH_STRUCTURE);
//    PatchSetupConfigurationDataPreMem (mAlderlakeSDdr4RvpRealSiliconConfigPatchStruct, ConfigPatchStructSize);
//  }
//
//  //
//  //
//  if (IsSimicsEnvironment ()) {
//    switch (BoardId) {
//      default:
//        ConfigPatchStructSize = SIZE_OF_TABLE (mAlderlakeSSimicsConfigPatchStruct, CONFIG_PATCH_STRUCTURE);
//        PatchSetupConfigurationDataPreMem (mAlderlakeSSimicsConfigPatchStruct, ConfigPatchStructSize);
//        break;
//    }
//  }
//
//#if FixedPcdGet8(PcdFspModeSelection) == 0
//  if (GetPackageTdp () != CPU_TDP_125_WATTS && GetPackageTdp () != CPU_TDP_150_WATTS) {
//    ConfigPatchStructSize = SIZE_OF_TABLE (mAlderlakeSTdpPatchStruct, CONFIG_PATCH_STRUCTURE);
//    PatchSetupConfigurationDataPreMem (mAlderlakeSTdpPatchStruct, ConfigPatchStructSize);
//  }
//#endif
//if(BoardId == BoardIdAdlSAdpSDdr5UDimm1DOc) {
//  ConfigPatchStructSize = SIZE_OF_TABLE (mAlderlakeSS10OCBoardConfigPatchStruct, CONFIG_PATCH_STRUCTURE);
//  PatchSetupConfigurationDataPreMem (mAlderlakeSS10OCBoardConfigPatchStruct, ConfigPatchStructSize);
//}
//[-end-200225-IB14630334-remove]//

  //
  // Disable Ps2KbMsEnable for FlavorDesktop only.
  //
//  if (PcdGet8 (PcdPlatformFlavor) == FlavorDesktop) {
//    ConfigPatchStructSize = SIZE_OF_TABLE (mFlavorDesktopPs2ConfigPatchStruct, CONFIG_PATCH_STRUCTURE);
//    PatchSetupConfigurationDataPreMem (mFlavorDesktopPs2ConfigPatchStruct, ConfigPatchStructSize);
//  }

  PeiServicesInstallPpi (&mSetupVariablesReadyPpi);

  return RETURN_SUCCESS;
}

/**
  Board Misc init function for PEI pre-memory phase.
**/
VOID
AdlSBoardMiscInitPreMem (
  VOID
  )
{
  UINT16     BoardId;
  BoardId =  PcdGet16 (PcdBoardId);

  PCD64_BLOB PcdData;

  //
  // RecoveryMode GPIO
  //
  PcdData.Blob = 0;
  PcdData.BoardGpioConfig.Type = BoardGpioTypeNotSupported;
  PcdSet64S (PcdRecoveryModeGpio, PcdData.Blob);

  //
  // OddPower Init
  //
  PcdSetBoolS (PcdOddPowerInitEnable, FALSE);

  //
  // Pc8374SioKbc Present
  //
  PcdSetBoolS (PcdPc8374SioKbcPresent, FALSE);

  //
  // Smbus Alert function Init.
  //
  PcdSetBoolS (PcdSmbusAlertEnable, FALSE);


  //EC FailSafe Cpu Temp and Fan Speed Setting
  PcdSet8S (PcdEcFailSafeActionCpuTemp, 85);
  PcdSet8S (PcdEcFailSafeActionFanPwm, 65);

  switch (BoardId) {
    case BoardIdAdlSAdpSDdr4UDimm2DCrb:
    case BoardIdAdlSAdpSDdr4UDimm2DCrbCpv:
      // Todo check for ADL RVP.
      //PcdSet32S (PcdWwanPerstGpio, value);
      //PcdSetBoolS (PcdWwanPerstGpioPolarity, value);
      //PcdSet32S (PcdWwanBbrstGpio, value);
      //PcdSetBoolS (PcdWwanBbrstGpioPolarity, value);
      //PcdSet32S (PcdWwanWakeGpio, value);
      //PcdSet32S (PcdWwanFullCardPowerOffGpio, value);
      //PcdSetBoolS (PcdWwanFullCardPowerOffGpioPolarity, value);
      //PcdSet8S (PcdWwanSourceClock, value);
      //PcdSet8S (PcdWwanRootPortNumber, value);
     break;
    case BoardIdAdlSAdpSSbgaDdr5SODimmErb:
    case BoardIdAdlSAdpSSbgaDdr5SODimmCrb:
    case BoardIdAdlSAdpSSbgaDdr5SODimmAep:
      PcdSet32S (PcdWwanFullCardPowerOffGpio, GPIO_VER4_S_GPP_F23);
      PcdSet32S (PcdWwanBbrstGpio, GPIO_VER4_S_GPP_I7);
      PcdSet32S (PcdWwanPerstGpio, GPIO_VER4_S_GPP_H11);
      PcdSet32S (PcdWwanWakeGpio, GPIO_VER4_S_GPP_H10);
      PcdSetBoolS (PcdWwanPerstGpioPolarity, PIN_GPIO_ACTIVE_LOW);
      PcdSetBoolS (PcdWwanFullCardPowerOffGpioPolarity, PIN_GPIO_ACTIVE_HIGH);
      PcdSetBoolS (PcdWwanBbrstGpioPolarity, PIN_GPIO_ACTIVE_LOW);
      PcdSet8S (PcdWwanSourceClock, 10);
      PcdSet8S (PcdWwanRootPortNumber, 0x02);
     break;
    case BoardIdAdlSAdpSSbgaDdr4SODimmCrb:
      PcdSet32S (PcdWwanFullCardPowerOffGpio, GPIO_VER4_S_GPP_F4);
      PcdSet32S (PcdWwanBbrstGpio, GPIO_VER4_S_GPP_I7);
      PcdSet32S (PcdWwanPerstGpio, GPIO_VER4_S_GPP_B6);
      PcdSet32S (PcdWwanWakeGpio, GPIO_VER4_S_GPP_H13);
      PcdSetBoolS (PcdWwanPerstGpioPolarity, PIN_GPIO_ACTIVE_LOW);
      PcdSetBoolS (PcdWwanFullCardPowerOffGpioPolarity, PIN_GPIO_ACTIVE_HIGH);
      PcdSetBoolS (PcdWwanBbrstGpioPolarity, PIN_GPIO_ACTIVE_LOW);
      PcdSet8S (PcdWwanSourceClock, 10);
      PcdSet8S (PcdWwanRootPortNumber, 0x02);
     break;
    case BoardIdAdlSAdpSDdr5UDimm1DOc:
      PcdSetBoolS (PcdDiscreteClockOcSupport, TRUE);
     break;
  }
}

//[-start-201104-IB17510119-add]//
STATIC
EFI_STATUS
GpioCfgPei (
//[-start-210120-IB16560239-modify]//
  IN H2O_BOARD_ID               Board,
//[-end-210120-IB16560239-modify]//
  IN BOOLEAN                    PreMemFlag
  )
{
  EFI_STATUS                    Status;
  H2O_GPIO_INIT_CONFIG          *GpioInitStruct;
  UINT32                        GpioInitStructCount;
  UINT32                        GpioTableCount;
  GPIO_INIT_CONFIG              *GpioDefinitionTable;
  H2O_GPIO_INIT_CONFIG          SingleGpioInitStruct = {0};
  UINT32                        Index;
  UINT32                        PadMaxSize;
  GPIO_GROUP                    Group;

  PadMaxSize = 0;
  for (Group = GpioGetLowestGroup(); Group <= GpioGetHighestGroup(); Group ++) {    
    PadMaxSize += GpioGetPadPerGroup (Group);
  }

  GpioDefinitionTable = NULL;
  GpioDefinitionTable = (GPIO_INIT_CONFIG *)AllocateZeroPool (PadMaxSize * sizeof (GPIO_INIT_CONFIG));
  if (GpioDefinitionTable == NULL) {
    return EFI_OUT_OF_RESOURCES;
  }
  //
  // Use GetGpioCfg in GpioCfgLib to get build-time GPIO configuration
  // Input Board value corresponds to Skuids in Project.dsc
  //
  Status = GetGpioCfg (Board, &GpioInitStruct, &GpioInitStructCount);
  ASSERT_EFI_ERROR (Status);
  if (EFI_ERROR (Status)) {
    return EFI_NOT_FOUND;
  }

  GpioTableCount = 0;
  Status = EFI_UNSUPPORTED;
  for (Index = 0; Index < GpioInitStructCount; Index++) {
    if (GpioInitStruct[Index].Flag != PreMemFlag) {
      continue;
    }
    CopyMem (&SingleGpioInitStruct, &GpioInitStruct[Index], sizeof(H2O_GPIO_INIT_CONFIG));
    //
    // OemServices
    // This function works when PCT being FALSE or PCT's setting being null.
    // Please refer to Power On Guide 0.4.
    //
    DEBUG ((DEBUG_INFO, "OemKernelServices Call: OemSvcUpdateGpioCfg \n"));
    Status = OemSvcUpdateGpioCfg (&SingleGpioInitStruct);
    DEBUG ((DEBUG_INFO, "OemKernelServices OemSvcUpdateGpioCfg Status: %r\n", Status));
    if (Status != EFI_SUCCESS) {
     CopyMem (&GpioDefinitionTable[GpioTableCount], &SingleGpioInitStruct, sizeof(GPIO_INIT_CONFIG));
     GpioTableCount++;
    }
  }
  if ((Status == EFI_SUCCESS) && (GpioTableCount == 0x00)) {
    return EFI_SUCCESS;
  }

  //
  // Batch process GPIO configuraions.
  //
  DEBUG ((DEBUG_INFO, "ConfigureGpio() Start\n"));
  Status = GpioConfigurePads (GpioTableCount, GpioDefinitionTable);
  DEBUG ((DEBUG_INFO, "ConfigureGpio() End\n"));

  return EFI_SUCCESS;
}
//[-end-201104-IB17510119-add]//

/**
  A hook for board-specific initialization prior to memory initialization.

  @retval EFI_SUCCESS   The board initialization was successful.
**/
EFI_STATUS
EFIAPI
AdlSBoardInitBeforeMemoryInit (
  VOID
  )
{
  EFI_STATUS                        Status;
  UINT16                            PchGraphicsRstPinDelay;

  DEBUG ((DEBUG_INFO, "AdlSBoardInitBeforeMemoryInit\n"));

  //[-start-191111-IB10189001-remove]//
  // GetBiosId (NULL);
  //[-end-191111-IB10189001-remove]//
  PchGraphicsRstPinDelay = 0;


  AdlSInitPreMem ();

  AdlSWakeUpTypeUpdate ();
  AdlSBoardMiscInitPreMem ();
  AdlSSioInit ();

  ///
  /// Do basic PCH init
  ///
  SiliconInit ();
  AdlSGpioGroupTierInit ();
  AdlSGpioTablePreMemInit ();

  GpioInit (PcdGetPtr(PcdBoardGpioTableEarlyPreMem));
  MicroSecondDelay (PchGraphicsRstPinDelay * 1000);
  AdlSHsioInit ();
  AdlSMrcConfigInit ();
  AdlSSaGpioConfigInit ();
  AdlSSaMiscConfigInit ();
  Status = AdlSRootPortClkInfoInit ();
  Status = AdlSUsbConfigInit ();
  AdlSSaDisplayConfigInit ();
  AdlSSaUsbConfigInit ();
  //
  // Install Notify
  //
#if FixedPcdGet8(PcdFspModeSelection) == 0
  Status = PeiServicesNotifyPpi (&mNotifyPciePinPpi);
  ASSERT_EFI_ERROR (Status);
#endif
  // Configure GPIO group GPE tier
  GpioGroupTierInit ();

  ASSERT(Status == EFI_SUCCESS);

  //[-start-210512-18770027-modify]//
  //DiscreteClockConfigInit ();
  //[-end-210512-18770027-modify]//
  return EFI_SUCCESS;
}

/**
  A hook for board-specific initialization after memory initialization.

  @retval EFI_SUCCESS   The board initialization was successful.
**/
EFI_STATUS
EFIAPI
AdlSBoardInitAfterMemoryInit (
  VOID
  )
{
  DEBUG ((DEBUG_INFO, "AdlSBoardInitAfterMemoryInit\n"));

  return MmioInit ();
}

/**
  This board service initializes board-specific debug devices.

  @retval EFI_SUCCESS   Board-specific debug initialization was successful.
**/
EFI_STATUS
EFIAPI
AdlSBoardDebugInit (
  VOID
  )
{
  DEBUG ((DEBUG_INFO, "AdlSBoardDebugInit\n"));
  return EFI_SUCCESS;
}

/**
  This board service detects the boot mode.

  @retval EFI_BOOT_MODE The boot mode.
**/
EFI_BOOT_MODE
EFIAPI
AdlSBoardBootModeDetect (
  VOID
  )
{
  EFI_BOOT_MODE                             BootMode;

  DEBUG ((DEBUG_INFO, "AdlSBoardBootModeDetect\n"));
  BootMode = DetectBootMode ();
  DEBUG ((DEBUG_INFO, "BootMode: 0x%02X\n", BootMode));

  return BootMode;
}
