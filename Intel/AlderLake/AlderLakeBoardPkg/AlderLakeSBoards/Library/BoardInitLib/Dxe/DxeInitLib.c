/** @file

@copyright
  INTEL CONFIDENTIAL
  Copyright 2018 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains a 'Sample Driver' and is licensed as such under the terms
  of your license agreement with Intel or your vendor. This file may be modified
  by the user, subject to the additional terms of the license agreement.

@par Specification Reference:
**/

#include <Uefi.h>
#include <Library/HobLib.h>
#include <Library/PcdLib.h>
#include <Library/UefiBootServicesTableLib.h>
#include <Library/DxeUpdatePlatformInfoLib.h>
#include <Library/SetupInitLib.h>
#include <Library/PreSiliconEnvDetectLib.h>
#include <MemInfoHob.h>
#include <PlatformBoardConfig.h>
#include <PlatformBoardId.h>
#include <Pins/GpioPinsVer4S.h>
#include <AlderLakeSBoardConfigPatchTable.h>
#include <PcieRegs.h>
#include <Register/PchRegs.h>
#include <Library/PciSegmentLib.h>
#include <Library/MemoryAllocationLib.h>
#include <Library/BoardConfigLib.h>
#include <Protocol/RetimerCapsuleUpdate.h>
#include <Library/CpuPlatformLib.h>

/**
   Retimer Platform Specific Data
**/

RETIMER_PLATFORM_DATA                                mRetimerPlatformDataTableAdlS[MAX_TBT_RETIMER_DEVICE];
/*
STATIC   TBT_PD_RETIMER_PLATFORM_DATA                mRetimerPlatformDataTableAdlSFourTbtPdRetimer[] =
{
  { TbtPdRetimerType, TRUE, 0, 0, 0, 0x00, 0x00, 0x00 },
  { TbtPdRetimerType, TRUE, 1, 0, 0, 0x00, 0x00, 0x00 },
  { TbtPdRetimerType, TRUE, 2, 1, 0, 0x00, 0x00, 0x00 },
  { TbtPdRetimerType, TRUE, 3, 1, 0, 0x00, 0x00, 0x00 }
};
*/
STATIC   TBT_PD_RETIMER_PLATFORM_DATA                mRetimerPlatformDataTableAdlSThreeTbtPdRetimer[] =
{
  { TbtPdRetimerType, TRUE, 0, 0, 0, 0x00, 0x00, 0x00 },
  { TbtPdRetimerType, TRUE, 1, 0, 0, 0x00, 0x00, 0x00 },
  { TbtPdRetimerType, TRUE, 2, 1, 0, 0x00, 0x00, 0x00 }
};

STATIC   TBT_PD_RETIMER_PLATFORM_DATA                mRetimerPlatformDataTableAdlSTwoTbtPdRetimer[] =
{
  { TbtPdRetimerType, TRUE, 0, 0, 0, 0x00, 0x00, 0x00 },
  { TbtPdRetimerType, TRUE, 1, 0, 0, 0x00, 0x00, 0x00 }
};



EFI_STATUS
PatchConfigurationDataInit (
  IN CONFIG_PATCH_STRUCTURE  *ConfigPatchStruct,
  IN UINTN                   ConfigPatchStructSize
  );

VOID
AdlSInitPatchConfigurationData (
  VOID
  )
{
  UINTN     ConfigPatchStructSize;
  CONFIG_PATCH_STRUCTURE *PatchTable;
  UINTN                  PatchTableSize;
  UINT16                 BoardId;

  BoardId = PcdGet16 (PcdBoardId);
  PatchTable     = NULL;
  PatchTableSize = 0;

  switch (BoardId) {
    case BoardIdAdlSAdpSDdr5UDimm1DAep:
      ConfigPatchStructSize = SIZE_OF_TABLE (mAlderlakeSAdpSDdr5UDimm1DAepConfigPatchStruct, CONFIG_PATCH_STRUCTURE);
      UpdatePatchTable (&PatchTable, &PatchTableSize, mAlderlakeSAdpSDdr5UDimm1DAepConfigPatchStruct, ConfigPatchStructSize);
      break;

    case BoardIdAdlSAdpSDdr5SODimmCrb:
    case BoardIdAdlSAdpSDdr4SODimmCrb:
      ConfigPatchStructSize = SIZE_OF_TABLE (mAlderlakeSSodimmRvpConfigPatchStruct, CONFIG_PATCH_STRUCTURE);
      UpdatePatchTable (&PatchTable, &PatchTableSize, mAlderlakeSSodimmRvpConfigPatchStruct, ConfigPatchStructSize);
      break;

    case BoardIdAdlSAdpSSbgaDdr5SODimmErb:
    case BoardIdAdlSAdpSSbgaDdr5SODimmCrb:
    case BoardIdAdlSAdpSSbgaDdr5SODimmAep:
      ConfigPatchStructSize = SIZE_OF_TABLE (mAlderlakeSbgaDdr5SODimmErbConfigPatchStruct, CONFIG_PATCH_STRUCTURE);
      UpdatePatchTable (&PatchTable, &PatchTableSize, mAlderlakeSbgaDdr5SODimmErbConfigPatchStruct, ConfigPatchStructSize);
      if(BoardId == BoardIdAdlSAdpSSbgaDdr5SODimmAep) {
        ConfigPatchStructSize = SIZE_OF_TABLE (mAlderlakeSbgaDdr5SODimmAepConfigPatchStruct, CONFIG_PATCH_STRUCTURE);
        UpdatePatchTable (&PatchTable, &PatchTableSize, mAlderlakeSbgaDdr5SODimmAepConfigPatchStruct, ConfigPatchStructSize);
      }
      break;

    case BoardIdAdlSAdpSSbgaDdr4SODimmCrb:
      ConfigPatchStructSize = SIZE_OF_TABLE (mAlderlakeSbgaDdr4SODimmErbConfigPatchStruct, CONFIG_PATCH_STRUCTURE);
      UpdatePatchTable (&PatchTable, &PatchTableSize, mAlderlakeSbgaDdr4SODimmErbConfigPatchStruct, ConfigPatchStructSize);
      break;

    case BoardIdAdlSAdpSDdr5UDimm1DOc:
    default:
      ConfigPatchStructSize = SIZE_OF_TABLE (mAlderlakeSDdr4RvpConfigPatchStruct, CONFIG_PATCH_STRUCTURE);
      UpdatePatchTable (&PatchTable, &PatchTableSize, mAlderlakeSDdr4RvpConfigPatchStruct, ConfigPatchStructSize);
      break;
  }

  if (!IsSimicsEnvironment ()) {
    ConfigPatchStructSize = SIZE_OF_TABLE (mAlderlakeSDdr4RvpRealSiliconConfigPatchStruct, CONFIG_PATCH_STRUCTURE);
    UpdatePatchTable (&PatchTable, &PatchTableSize, mAlderlakeSDdr4RvpRealSiliconConfigPatchStruct, ConfigPatchStructSize);
  }

  //
  //
  if (IsSimicsEnvironment ()) {
    switch (BoardId) {
      default:
        ConfigPatchStructSize = SIZE_OF_TABLE (mAlderlakeSSimicsConfigPatchStruct, CONFIG_PATCH_STRUCTURE);
        UpdatePatchTable (&PatchTable, &PatchTableSize, mAlderlakeSSimicsConfigPatchStruct, ConfigPatchStructSize);
        break;
    }
  }

#if FixedPcdGet8(PcdFspModeSelection) == 0
  if (GetPackageTdp () != CPU_TDP_125_WATTS && GetPackageTdp () != CPU_TDP_150_WATTS) {
    ConfigPatchStructSize = SIZE_OF_TABLE (mAlderlakeSTdpPatchStruct, CONFIG_PATCH_STRUCTURE);
    UpdatePatchTable (&PatchTable, &PatchTableSize, mAlderlakeSTdpPatchStruct, ConfigPatchStructSize);
  }
#endif
  if(BoardId == BoardIdAdlSAdpSDdr5UDimm1DOc){
    ConfigPatchStructSize = SIZE_OF_TABLE (mAlderlakeSS10OCBoardConfigPatchStruct, CONFIG_PATCH_STRUCTURE);
    UpdatePatchTable (&PatchTable, &PatchTableSize, mAlderlakeSS10OCBoardConfigPatchStruct, ConfigPatchStructSize);
  }

  //
  // Disable Ps2KbMsEnable for FlavorDesktop only.
  //
  if (PcdGet8 (PcdPlatformFlavor) == FlavorDesktop) {
    ConfigPatchStructSize = SIZE_OF_TABLE (mFlavorDesktopPs2ConfigPatchStruct, CONFIG_PATCH_STRUCTURE);
    UpdatePatchTable (&PatchTable, &PatchTableSize, mFlavorDesktopPs2ConfigPatchStruct, ConfigPatchStructSize);
  }

  PatchConfigurationDataInit (PatchTable, PatchTableSize);
  FreePatchTable (&PatchTable, &PatchTableSize);
}

STATIC
EFI_STATUS
BoardHookPlatformSetup (
  VOID * Content
  )
{
  ((SETUP_VOLATILE_DATA *)Content)->PlatId = PcdGet16 (PcdBoardId);

  return RETURN_SUCCESS;
}

/**
 Init Misc Platform Board Config Block.

 @param[in]  VOID

 @retval VOID
**/
VOID
AdlSBoardMiscInit (
  VOID
)
{
  UINT16    BoardId;

  BoardId = PcdGet16 (PcdBoardId);

  PcdSet64S (PcdFuncBoardHookPlatformSetupOverride, (UINT64) (UINTN) BoardHookPlatformSetup);
  PcdSet8S (PcdHdaI2sCodecI2cBusNumber, 4);

  //
  // PLN SUPPORT
  //
  switch (BoardId) {
    case BoardIdAdlSAdpSDdr5UDimm1DAep:
      break;
    default:
      //
      // This PCDs are only needed for discrete logic.
      //
      PcdSet32S (PcdPlnDelayEnableGpio, (UINT32) GPIO_VER4_S_GPP_H15);
      PcdSet32S (PcdPlnDelaySelectionGpio, (UINT32) GPIO_VER4_S_GPP_H23);
      break;
  }

}

VOID
AdlSBoardSmbiosInit (
  VOID
  )
{
  PcdSet64S (PcdSmbiosFabBoardName, (UINTN) PcdGetPtr (PcdBoardName));
  //
  // @todo : Update Slot Entry Table for all the supported boards using below PCD.
  //  This PCD follows SYSTEM_SLOT_ENTRY structure in \Include\SmbiosPlatformInfoDefinition.h
  //PcdSet64S (PcdSmbiosMainSlotEntry, NULL);
}

VOID
AdlSUpdateDimmPopulation (
  VOID
  )
{
  MEMORY_INFO_DATA_HOB    *MemInfo;
  UINT8                   Slot0;
  UINT8                   Slot1;
  UINT8                   Slot2;
  UINT8                   Slot3;
  CONTROLLER_INFO         *ControllerInfo;
  EFI_HOB_GUID_TYPE       *GuidHob;

  GuidHob = NULL;
  MemInfo = NULL;

  GuidHob = GetFirstGuidHob (&gSiMemoryInfoDataGuid);
  ASSERT (GuidHob != NULL);

  if (GuidHob != NULL) {
    MemInfo = (MEMORY_INFO_DATA_HOB *) GET_GUID_HOB_DATA (GuidHob);
  }

  if (MemInfo != NULL) {
    if (PcdGet8 (PcdPlatformFlavor) == FlavorDesktop ||
        PcdGet8 (PcdPlatformFlavor) == FlavorUpServer ||
        PcdGet8 (PcdPlatformFlavor) == FlavorWorkstation
        ) {
      DEBUG ((DEBUG_INFO, "Checking Dimm Population\n"));

      ControllerInfo = &MemInfo->Controller[0];
      Slot0 = ControllerInfo->ChannelInfo[0].DimmInfo[0].Status;
      Slot1 = ControllerInfo->ChannelInfo[0].DimmInfo[1].Status;
      Slot2 = ControllerInfo->ChannelInfo[1].DimmInfo[0].Status;
      Slot3 = ControllerInfo->ChannelInfo[1].DimmInfo[1].Status;

      if ((Slot0 && (Slot1 == 0)) || (Slot2 && (Slot3 == 0))) {
        PcdSetBoolS (PcdDimmPopulationError, TRUE);
      }
    }
  }
}

/**
  Enable Tier2 GPIO Sci wake capability.

  @retval EFI_SUCCESS   The function completed successfully.
**/
EFI_STATUS
AdlSTier2GpioWakeSupport (
  VOID
  )
{
  BOOLEAN Tier2GpioWakeEnable;

  Tier2GpioWakeEnable = FALSE;

  //
  // Root port #03: M.2 WLAN
  //
  if (IsPcieEndPointPresent (2)) {
   Tier2GpioWakeEnable = TRUE;
  }
  PcdSetBoolS (PcdGpioTier2WakeEnable, Tier2GpioWakeEnable);

  return EFI_SUCCESS;
}

/**
  Copy Tcss Retimer Platform Data.

  @param[in|out]  DestinationTableCurrentIndex   The pointer to the destination buffer Current Index.
  @param[in]      SourceTable                    The pointer to the source buffer of the memory copy.
  @param[in]      SourceTableSize                The number of bytes to copy from SourceBuffer to DestinationBuffer.
  @param[in]      SourceTableType                Source Table Buffer Type

  @retval     EFI_SUCCESS            The function completed successfully.
  @retval     EFI_INVALID_PARAMETER  Invalid Parameter Passed.
**/

EFI_STATUS
AdlSCopyTcssRetimerPlatformData (
  IN OUT UINT32                               *DestinationTableCurrentIndex,
  IN  VOID                                    *SourceTable,
  IN  UINT32                                  SourceTableSize,
  IN  RETIMER_TYPE                            SourceTableType
  )
{
  UINT32                                      TableIndex;
  UINT32                                      SourceTableElementSize;
  UINT32                                      SourceTableElementCount;

  SourceTableElementSize = 0;

  if (DestinationTableCurrentIndex == NULL) {
    return EFI_INVALID_PARAMETER;
  }

  if (SourceTable == NULL) {
    return EFI_INVALID_PARAMETER;
  }

  if (SourceTableSize == 0) {
    return EFI_INVALID_PARAMETER;
  }

  if ((*DestinationTableCurrentIndex) >= MAX_TBT_RETIMER_DEVICE) {
    // Return EFI_INVALID_PARAMETER as Invalid Destination Table Current Index is passed
    return EFI_INVALID_PARAMETER;
  }
  if (SourceTableType == TbtPdRetimerType) {
    SourceTableElementSize = sizeof (TBT_PD_RETIMER_PLATFORM_DATA);
  } else if (SourceTableType == NonTbtI2cRetimerType) {
    SourceTableElementSize = sizeof (NON_TBT_I2C_RETIMER_PLATFORM_DATA);
  } else {
    // Return EFI_INVALID_PARAMETER as Invalid Source Table Type is passed
    return EFI_INVALID_PARAMETER;
  }

  SourceTableElementCount = SourceTableSize / SourceTableElementSize;

  if (((*DestinationTableCurrentIndex) + SourceTableElementCount) > MAX_TBT_RETIMER_DEVICE) {
    // Return EFI_INVALID_PARAMETER as Destination Buffer Size is small and Array boundary condition will be reached
    return EFI_INVALID_PARAMETER;
  }

  for (TableIndex = 0; TableIndex < SourceTableElementCount; TableIndex++) {
    if (((RETIMER_PLATFORM_DATA*)SourceTable)->RetimerType == SourceTableType) { // Only add entry to Dastination Table if Source Table Type Match
      gBS->CopyMem (&mRetimerPlatformDataTableAdlS[(*DestinationTableCurrentIndex)], SourceTable, SourceTableElementSize);
      *DestinationTableCurrentIndex += 1;
    }
    SourceTable = (VOID*) ((UINTN)SourceTable + SourceTableElementSize);
  }

  return EFI_SUCCESS;
}

/**
  Init Tcss Retimer Platform Data Pcd.

  @param[in]  BoardId           An unsigned integrer represent the board id.

  @retval     EFI_SUCCESS       The function completed successfully.
**/

EFI_STATUS
AdlSInitTcssRetimerPlatformDataPcd (
  VOID
)
{
  UINT32                               RetimerDataTableIndex;
  EFI_STATUS                           Status = EFI_SUCCESS;

  DEBUG((DEBUG_INFO, "AdlSInitTcssRetimerPlatformDataPcd \n"));
  //
  // Map Retimer and PD Controller Available on Board
  //
  RetimerDataTableIndex = 0; // Initialize Destination Table Index to 0 to Start from Begining
  Status = AdlSCopyTcssRetimerPlatformData (&RetimerDataTableIndex, (VOID *) mRetimerPlatformDataTableAdlSThreeTbtPdRetimer, sizeof(mRetimerPlatformDataTableAdlSThreeTbtPdRetimer), TbtPdRetimerType);
  if (EFI_ERROR (Status)) {
    DEBUG((DEBUG_INFO, "mRetimerPlatformDataTableAdlSThreeTbtPdRetimer fail. \n"));

    // Updating Mapping of Retimer and PD Controller as Retimer Device is not Available
    RetimerDataTableIndex = 0; // Initialize Destination Table Index to 0 to Start from Begining
    Status = AdlSCopyTcssRetimerPlatformData (&RetimerDataTableIndex, (VOID *) mRetimerPlatformDataTableAdlSTwoTbtPdRetimer, sizeof(mRetimerPlatformDataTableAdlSTwoTbtPdRetimer), TbtPdRetimerType);

    if (EFI_ERROR (Status)) {
      DEBUG((DEBUG_INFO, "mRetimerPlatformDataTableAdlSTwoTbtPdRetimer fail. \n"));
    }
  }

  if ( !EFI_ERROR (Status)) {
    // Saving Retimer Platform Data Table into PCD
    PcdSet64S(PcdBoardRetimerDataTablePtr, (UINTN)mRetimerPlatformDataTableAdlS);
    PcdSet32S(PcdBoardRetimerDataTableEntryCount, RetimerDataTableIndex);
  }

  return Status;
}

/**
  A hook for board-specific initialization after PCI enumeration.

  @retval EFI_SUCCESS   The board initialization was successful.
**/
EFI_STATUS
EFIAPI
AdlSBoardInitAfterPciEnumeration (
  VOID
  )
{
  DEBUG ((DEBUG_INFO, "AdlSBoardInitAfterPciEnumeration\n"));

  AdlSInitPatchConfigurationData ();

  AdlSInitTcssRetimerPlatformDataPcd ();

  AdlSTier2GpioWakeSupport ();

  AdlSBoardMiscInit();

  AdlSBoardSmbiosInit ();

  UpdatePlatformInfo ();

  return EFI_SUCCESS;
}

/**
  A hook for board-specific functionality for the ReadyToBoot event.

  @retval EFI_SUCCESS   The board initialization was successful.
**/
EFI_STATUS
EFIAPI
AdlSBoardInitReadyToBoot (
  VOID
  )
{
  DEBUG ((DEBUG_INFO, "AdlSBoardInitReadyToBoot\n"));

  AdlSUpdateDimmPopulation ();

  return EFI_SUCCESS;
}

/**
  A hook for board-specific functionality for the ExitBootServices event.

  @retval EFI_SUCCESS   The board initialization was successful.
**/
EFI_STATUS
EFIAPI
AdlSBoardInitEndOfFirmware (
  VOID
  )
{
  DEBUG ((DEBUG_INFO, "AdlSBoardInitEndOfFirmware\n"));

  return EFI_SUCCESS;
}
