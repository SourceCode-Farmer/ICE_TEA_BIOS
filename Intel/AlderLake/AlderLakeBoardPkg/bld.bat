@REM @file
@REM
@REM @copyright
@REM  INTEL CONFIDENTIAL
@REM  Copyright 2018 - 2021 Intel Corporation.
@REM
@REM  The source code contained or described herein and all documents related to the
@REM  source code ("Material") are owned by Intel Corporation or its suppliers or
@REM  licensors. Title to the Material remains with Intel Corporation or its suppliers
@REM  and licensors. The Material may contain trade secrets and proprietary and
@REM  confidential information of Intel Corporation and its suppliers and licensors,
@REM  and is protected by worldwide copyright and trade secret laws and treaty
@REM  provisions. No part of the Material may be used, copied, reproduced, modified,
@REM  published, uploaded, posted, transmitted, distributed, or disclosed in any way
@REM  without Intel's prior express written permission.
@REM
@REM  No license under any patent, copyright, trade secret or other intellectual
@REM  property right is granted to or conferred upon you by disclosure or delivery
@REM  of the Materials, either expressly, by implication, inducement, estoppel or
@REM  otherwise. Any license under such intellectual property rights must be
@REM  express and approved by Intel in writing.
@REM
@REM  Unless otherwise agreed by Intel in writing, you may not remove or alter
@REM  this notice or any other notice embedded in Materials by Intel or
@REM  Intel's suppliers or licensors in any way.
@REM
@REM  This file contains a 'Sample Driver' and is licensed as such under the terms
@REM  of your license agreement with Intel or your vendor. This file may be modified
@REM  by the user, subject to the additional terms of the license agreement.
@REM
@REM @par Specification Reference:
@REM

:: Useage: bld [/s] [/f <FEATURE_PCD_NAME> <FALSE or TRUE>] [/r]
::
:: For a given build command, 3 options may be passed into this batch file via command prompt:
:: 1) /s = Redirects all output to a file called EDK2.log(Prep.log must be existed), which will be located at the root.
:: 2) /f = Defines the passing in of a single override to a feature PCD that is used in the platform
::    DSC file.  If this parameter is used, it is to be followed immediately after by both the feature
::    pcd name and value. FeaturePcd is the full PCD name, like gMinPlatformPkgTokenSpaceGuid.PcdOptimizeCompilerEnable
:: 3) /r = Useful for faster rebuilds when no changes have been made to .inf files. Passes -u to
::    build.exe to skip the generation of makefiles.
:: 4) rom = Build Bios.rom only and building SPIs will be skipped.
::
@set BLDTIMESTAMP=%time%

@if not defined PYTHON_COMMAND (
  set PYTHON_COMMAND=py -3
)

@set FLASHMAP_FDF=
@if %RESILIENCY_BUILD% EQU TRUE (
  @set FLASHMAP_FDF=%WORKSPACE_PLATFORM%\%PLATFORM_BOARD_PACKAGE%\Include\Fdf\FlashMapIncludeRes.fdf
) else (
  @if %EMBEDDED_BUILD% EQU TRUE (
    @set FLASHMAP_FDF=%WORKSPACE_PLATFORM%\%PLATFORM_BOARD_PACKAGE%\Include\Fdf\FlashMapEmbeddedInclude.fdf
  ) else (
    @if %FSPBGCC_BUILD% EQU TRUE (
        @set FLASHMAP_FDF=%WORKSPACE_PLATFORM%\%PLATFORM_BOARD_PACKAGE%\Include\Fdf\FlashMapIncludeGcc.fdf
    ) else (
      @if %ADLP_BUILD% EQU TRUE (
        @set FLASHMAP_FDF=%WORKSPACE_PLATFORM%\%PLATFORM_BOARD_PACKAGE%\Include\Fdf\FlashMapInclude.fdf
        ) else (
          @set FLASHMAP_FDF=%WORKSPACE_PLATFORM%\%PLATFORM_BOARD_PACKAGE%\Include\Fdf\FlashMapInclude.fdf
        )
      )
    )
  )
)
@REM
@REM Generating Flashmap FDF for Extended BIOS Region
@REM Note : the flashmap is being generated based on the default flashmap specificed by build option
@REM
@if %EXTENDEDREGION_BUILD% NEQ TRUE @goto NoExtendedBiosRegionGeneration
@set EXTENDEDREGION_TEMPLATE_FDF=%WORKSPACE_PLATFORM%\%PLATFORM_BOARD_PACKAGE%\Include\Fdf\FlashMapIncludeExtended.fdf.template
@set EXTENDEDREGION_FLASHMAP_FDF=%WORKSPACE_PLATFORM%\%PLATFORM_BOARD_PACKAGE%\Include\Fdf\FlashMapIncludeExtended_autogen.fdf
@%PYTHON_COMMAND% %WORKSPACE_PLATFORM%\%PLATFORM_BOARD_PACKAGE%\Tools\GenFlashmap\GenFlashmap.py ^
  -b %FLASHMAP_FDF% ^
  -t %EXTENDEDREGION_TEMPLATE_FDF% ^
  -o %EXTENDEDREGION_FLASHMAP_FDF% ^
  extended
@if %ERRORLEVEL% NEQ 0 (
  @echo ERROR: Failure in generating %EXTENDEDREGION_FLASHMAP_FDF%
  set SCRIPT_ERROR=1
  goto :BldFail
)
@set FLASHMAP_FDF=%EXTENDEDREGION_FLASHMAP_FDF%
:NoExtendedBiosRegionGeneration

@echo.
@echo  Call this script to padd each Microcode patch under AlderLakeBoardPkg\Binaries\Microcode\
@echo  And also create MicrocodeVersion.data
@echo.
py -3 microcode_padding.py --opt padding --fw-version %FW_VERSION% --lsv %LSV% --fw-version-string %FW_VERSION_STRING% --slotsize %SLOT_SIZE%
@if %ERRORLEVEL% NEQ 0 (
  @echo !!! ERROR: microcode_padding.py execute failure !!!
  @echo py -3 microcode_padding.py --opt padding --fw-version %FW_VERSION% --lsv %LSV% --fw-version-string %FW_VERSION_STRING% --slotsize %SLOT_SIZE%
  set SCRIPT_ERROR=1
  goto :BldFail
)

@echo ***********************************
@echo.
@echo Parsing FlashMapInclude FDFs and checking if all offset and size requirements are met
@echo.

@REM ACM alignment check
  @call %PYTHON_COMMAND% %WORKSPACE_PLATFORM%\%PLATFORM_BOARD_PACKAGE%\Tools\FvAlignment\FvAlignment.py --flashmap %FLASHMAP_FDF% -cl all acm_alignment_check
  @if %ERRORLEVEL% NEQ 0 goto :BldFail

  @REM FSP build
  @set BB_CHECK=TRUE
  @set TOPSWAP_LIMIT=0x00400000
  @set FIRST_MAP_PARAM=
  @if %FSPBGCC_BUILD% EQU TRUE (
    @set BB_CHECK=FALSE
  )
  @if %BB_CHECK% EQU TRUE (
    @if /I "%TARGET%" == "DEBUG" @if %RESILIENCY_BUILD% EQU FALSE (
        @set TOPSWAP_LIMIT=0x00440000
      @set FIRST_MAP_PARAM=--firstmap
    )
    @call %PYTHON_COMMAND% %WORKSPACE_PLATFORM%\%PLATFORM_BOARD_PACKAGE%\Tools\FvAlignment\FvAlignment.py --flashmap %FLASHMAP_FDF% --topswap_size %TOPSWAP_LIMIT% -cl all fv_alignment_check %FIRST_MAP_PARAM%
  )
  @if %ERRORLEVEL% NEQ 0 goto :BldFail
@echo.
@echo PASS all flash map quick check successfully!
@echo ***********************************

@echo on

@echo ********************************************************************
@echo ******************    BLD.BAT    ***********************************
@echo ********************************************************************

@rem cd %WORKSPACE%
@echo %CD%

@SET SILENT_MODE=FALSE
@SET REBUILD_MODE=
@SET BUILD_ROM_ONLY=

@if not defined PYTHON_COMMAND (
  set PYTHON_COMMAND=py -3
)

:: Loop through arguements until all are processed
:BUILD_FLAGS_LOOP
@if "%~1" == "" goto BUILD_FLAGS_LOOP_DONE

@if "%~1" == "/f" (
  shift
  goto BUILD_FLAGS_LOOP
)
@if "%~1" == "/s" (
  SET SILENT_MODE=TRUE
  @if exist %WORKSPACE%\Build.log del %WORKSPACE%\Build.log
  shift
  goto BUILD_FLAGS_LOOP
)
@if "%~1" == "/r" (
  SET REBUILD_MODE=-u
  shift
  goto BUILD_FLAGS_LOOP
)
@if "%~1" == "rom" (
  SET BUILD_ROM_ONLY=rom
  shift
  goto BUILD_FLAGS_LOOP
)
:: Unknown build flag.
shift
goto BUILD_FLAGS_LOOP
:BUILD_FLAGS_LOOP_DONE

:: Output the build variables the user has selected.
@echo.
@echo  User Selected build options:
@echo    SILENT_MODE = %SILENT_MODE%
@echo    REBUILD_MODE = %REBUILD_MODE%
@echo    BUILD_ROM_ONLY = %BUILD_ROM_ONLY%
@echo.

:: Start the FSP build process
@echo FSP_BINARY_BUILD=%FSP_BINARY_BUILD%
@REM
@echo Build FSP Binary
@REM
@if not defined FSP_BINARY_BUILD goto :SkipFspBinaryBuild
@if %FSP_BINARY_BUILD% EQU FALSE goto :SkipFspBinaryBuild
@set FSP_BUILD_PARAMETER=/d
@set FSP_PKG_NAME=AlderLakeFspPkg
@if /I "%TARGET%" == "RELEASE" (
  @if "%FSP_TEST_RELEASE%"=="TRUE" (
    set FSP_BUILD_PARAMETER=/tr
  ) else (
    set FSP_BUILD_PARAMETER=/r
  )
  @if "%FSP_PDB_RELEASE%"=="TRUE" (
    set FSP_BUILD_PARAMETER=/rp
  )
)
@if /I "%NOTIMESTAMP%" == "1" (
  set FSP_BUILD_PARAMETER=%FSP_BUILD_PARAMETER% notimestamp
)
set WORKSPACE_FSP_BIN=%WORKSPACE%\Intel

@echo %CD%
@rem
@rem  By Default Build for ADL
@rem

@set FspTargetOption=AlderLake
@if /I "%ADLP_BUILD%" EQU "TRUE" (
  @set FspTargetOption=AlderLakeP
)
@if not "%FspTargetOption%" == "" (
  @echo FSP build target is %FspTargetOption%
) else (
  @echo FSP build target is not specified.
  @goto :BldFail
)

@set FSPBINARYTIMESTAMP=%time%
@if %SILENT_MODE% EQU TRUE goto FspSilent

call %WORKSPACE_COMMON%\ClientOneSiliconPkg\Fsp\BuildFsp.cmd %TARGET_PLATFORM% %FspTargetOption% %FSP_BUILD_PARAMETER%
@goto FspBuildEnd

:FspSilent
call %WORKSPACE_COMMON%\ClientOneSiliconPkg\Fsp\BuildFsp.cmd %TARGET_PLATFORM% %FspTargetOption% %FSP_BUILD_PARAMETER% 1>> %WORKSPACE%\Build.log 2>&1

:FspBuildEnd
@if %ERRORLEVEL% NEQ 0 (
  @echo !!! ERROR:FSP build Failed !!!
  @echo %WORKSPACE_COMMON%\ClientOneSiliconPkg\Fsp\BuildFsp.cmd %TARGET_PLATFORM% %FspTargetOption% %FSP_BUILD_PARAMETER%
  set SCRIPT_ERROR=1
  goto :BldFail
) else (
  @echo FSP build has succeeded
  @echo FSP build has succeeded >> %WORKSPACE%\Build.log 2>&1
  @set FSPBINARYTIMESTAMP=%FSPBINARYTIMESTAMP% -%time%
)

@if %FSP_BINARY_BUILD_ONLY% EQU TRUE goto :EndPreBuild

:SkipFspBinaryBuild
  @if %FSP_BINARY_BUILD_ONLY% EQU TRUE goto :SkipPatchFspBinFvsBaseAddress
del /f %WORKSPACE_SILICON%\%PLATFORM_FSP_BIN_PACKAGE%\Fsp_Rebased*.fd

cd %WORKSPACE%
@echo %CD%
@echo ********************* BLD.BAT *****************************
@echo WORKSPACE_PLATFORM     = %WORKSPACE_PLATFORM%
@echo WORKSPACE_SILICON      = %WORKSPACE_SILICON%
@echo PROJECT                = %PROJECT%
@echo PLATFORM_PACKAGE       = %PLATFORM_PACKAGE%
@echo PLATFORM_BOARD_PACKAGE = %PLATFORM_BOARD_PACKAGE%
@echo WORKSPACE_FSP_BIN      = %WORKSPACE_FSP_BIN%

@if exist %WORKSPACE_PLATFORM%\%PLATFORM_BOARD_PACKAGE%\BoardPkgPcd.dsc attrib -r %WORKSPACE_PLATFORM%\%PLATFORM_BOARD_PACKAGE%\BoardPkgPcd.dsc
@echo .
@if %ADL_BUILD% EQU TRUE (
  @REM Remove unwanted lines in FDF according to build target.
  @if %FSPBGCC_BUILD% EQU TRUE (
      @call %PYTHON_COMMAND% %WORKSPACE_CORE_PLATFORM%\%PLATFORM_PACKAGE%\Tools\Fsp\RebaseFspBinBaseAddress.py %WORKSPACE_PLATFORM%\%PLATFORM_BOARD_PACKAGE%\Include\Fdf\FlashMapIncludeGcc.fdf %WORKSPACE_FSP_BIN%\AlderLakeFspBinPkg Fsp.fd 0x0
  ) else (
    @call %PYTHON_COMMAND% %WORKSPACE_PLATFORM%\%PLATFORM_BOARD_PACKAGE%\Tools\StripFlashmap.py "%TARGET%" %FLASHMAP_FDF% %WORKSPACE%\FlashMapInclude_Temp.fdf
    @call %PYTHON_COMMAND% %WORKSPACE_CORE_PLATFORM%\%PLATFORM_PACKAGE%\Tools\Fsp\RebaseFspBinBaseAddress.py %WORKSPACE%\FlashMapInclude_Temp.fdf %WORKSPACE_FSP_BIN%\AlderLakeFspBinPkg Fsp.fd 0x0
    @del %WORKSPACE_ROOT%\FlashMapInclude_Temp.fdf
  )
)

@echo .
@if %ERRORLEVEL% NEQ 0 (
  @echo !!! ERROR:RebaseFspBinBaseAddress failed!!!
  set SCRIPT_ERROR=1
  goto :BldFail
)

@REM FSP binary size check
if defined FSPM_SIZE_LIMIT (
  @call %PYTHON_COMMAND% %WORKSPACE_PLATFORM%\%PLATFORM_BOARD_PACKAGE%\Tools\FvAlignment\FvAlignment.py --fsp_bin_pkg %WORKSPACE_FSP_BIN%\AlderLakeFspBinPkg --fspm_size_limit %FSPM_SIZE_LIMIT% -cl all fsp_size_check
)
@if %ERRORLEVEL% NEQ 0 goto :BldFail

cd %WORKSPACE_PLATFORM%\%PLATFORM_BOARD_PACKAGE%

copy /y /b %WORKSPACE_SILICON%\%PLATFORM_FSP_BIN_PACKAGE%\Fsp_Rebased_S.fd+%WORKSPACE_SILICON%\%PLATFORM_FSP_BIN_PACKAGE%\Fsp_Rebased_M.fd+%WORKSPACE_SILICON%\%PLATFORM_FSP_BIN_PACKAGE%\Fsp_Rebased_T.fd %WORKSPACE_SILICON%\%PLATFORM_FSP_BIN_PACKAGE%\Fsp_Rebased.fd
:SkipPatchFspBinFvsBaseAddress

@echo Current Directory = %CD%
@echo ********************* BLD.BAT *****************************
@echo SILENT_MODE          = %SILENT_MODE%
@echo NUMBER_OF_PROCESSORS = %NUMBER_OF_PROCESSORS%
@echo REBUILD_MODE         = %REBUILD_MODE%
@echo EXT_BUILD_FLAGS      = %EXT_BUILD_FLAGS%


@if %SILENT_MODE% EQU TRUE goto BldSilent
@set BUILDTIMESTAMP=%time%
call build  %SI_BUILD_OPTION_PCD% %BUILD_OPTION_PCD% -n %NUMBER_OF_PROCESSORS% %REBUILD_MODE% %EXT_BUILD_FLAGS%

@echo %CD%
@echo ********************* BLD.BAT *****************************
@echo ERRORLEVEL=%ERRORLEVEL%

@if %ERRORLEVEL% NEQ 0 goto BldFail
@set BUILDTIMESTAMP=%BUILDTIMESTAMP% -%time%

@set POSTBUILDTIMESTAMP=%time%
@echo.
@echo Running postbuild.bat to complete the build process.
@echo.
call %WORKSPACE_PLATFORM%\%PLATFORM_BOARD_PACKAGE%\postbuild.bat %BUILD_ROM_ONLY%
echo Errorlevel= %errorlevel%
echo SCRIPT_ERROR= %SCRIPT_ERROR%
@if %SCRIPT_ERROR% NEQ 0 goto BldFail
@if %errorlevel% NEQ 0 goto BldFail
@set POSTBUILDTIMESTAMP=%POSTBUILDTIMESTAMP% -%time%
@goto BldSuccess

:BldSilent
@echo.
@echo ********************************************************************
@echo ***********            Build is launched here             **********
@echo ********************************************************************
@echo.
@set BUILDTIMESTAMP=%time%
call build  %SI_BUILD_OPTION_PCD% %BUILD_OPTION_PCD% -n %NUMBER_OF_PROCESSORS% %REBUILD_MODE% %EXT_BUILD_FLAGS% 1>>%WORKSPACE%\Build.log 2>&1
@echo ERRORLEVEL=%ERRORLEVEL% 1>>%WORKSPACE%\Build.log 2>&1

@if %ERRORLEVEL% NEQ 0 goto BldFail
@set BUILDTIMESTAMP=%BUILDTIMESTAMP% -%time%

@set POSTBUILDTIMESTAMP=%time%
@echo. >>%WORKSPACE%\Build.log
@echo Running postbuild.bat to complete the build process. >> %WORKSPACE%\Build.log
@echo. >>%WORKSPACE%\Build.log
call %WORKSPACE_PLATFORM%\%PLATFORM_BOARD_PACKAGE%\postbuild.bat %BUILD_ROM_ONLY% 1>>%WORKSPACE%\Build.log 2>&1
echo Errorlevel= %errorlevel%
echo SCRIPT_ERROR= %SCRIPT_ERROR%
@If %SCRIPT_ERROR% NEQ 0 goto BldFail
@if %errorlevel% NEQ 0 goto BldFail
@set POSTBUILDTIMESTAMP=%POSTBUILDTIMESTAMP% -%time%

:BldSuccess
@echo.
@echo TARGET:               %TARGET%
@echo TOOL_CHAIN_TAG:       %TOOL_CHAIN_TAG%
@echo BIOS location:        %BUILD_DIR%\FV
@echo ROM Images location:  %WORKSPACE%\RomImages
@echo.
@echo The BIOS build has successfully completed!
@echo.
@REM

@goto BldEnd

:BldFail
cd %WORKSPACE_PLATFORM%\%PLATFORM_BOARD_PACKAGE%
@echo.
@echo ----------------
@findstr "ACTIVE_PLATFORM" %WORKSPACE%\Conf\target.txt
@echo ACTIVE FLASH MAP FDF = %FLASHMAP_FDF%
@echo ----------------
@echo.
@echo.
@echo The BIOS Build has failed!
@echo.
@exit /b 1

:BldEnd
@if %SILENT_MODE% EQU TRUE (
  @if exist EDK2.log del EDK2.log
  @if exist Prep.log if exist Build.log copy Prep.log+Build.log EDK2.log
)
@set BLDTIMESTAMP=%BLDTIMESTAMP% -%time%
@echo -- timestamp ------------------------
@echo   prep          %PREPTIMESTAMP%
@echo   bld           %BLDTIMESTAMP%
@echo   - fsp binary  %FSPBINARYTIMESTAMP%
@echo   - build       %BUILDTIMESTAMP%
@echo   - postbuild   %POSTBUILDTIMESTAMP%
@echo -------------------------------------
cd %WORKSPACE_PLATFORM%\%PLATFORM_BOARD_PACKAGE%

py -3 microcode_padding.py --opt revert
