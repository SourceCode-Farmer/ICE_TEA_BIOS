@echo off

echo %0: Generating TigerLakeSimicsLP image

@REM Set Simics Rom Generation Tool Path and Simics Rom Output Path
if not defined WORKSPACE echo ERROR - WORKSPACE is not defined
if not defined WORKSPACE_PLATFORM echo ERROR - WORKSPACE_PLATFORM is not defined && exit /b 1
if not defined PROJECT echo ERROR - PROJECT is not defined && exit /b 1
if not defined TGL_SIMICS_TOOL_PATH set TGL_SIMICS_TOOL_PATH=%WORKSPACE_PLATFORM%\%PROJECT%\Tools\RomImage\TigerLakeSimicsLP
if not defined SIMICS_ROM_LOCATION set SIMICS_ROM_LOCATION=%WORKSPACE%\RomImages\MinPlatform\TigerLakeSimicsLP

@REM Copy BIOS to Simics Rom Generation Tool
if not defined BUILD_DIR_PATH echo ERROR - BUILD_DIR_PATH is not defined && exit /b 1
if not defined BOARD echo ERROR - BOARD is not defined && exit /b 1
if not exist "%TGL_SIMICS_TOOL_PATH%\fit\bin\" mkdir %TGL_SIMICS_TOOL_PATH%\fit\bin
copy %BUILD_DIR_PATH%\Fv\%BOARD%.fd %TGL_SIMICS_TOOL_PATH%\fit\bin\bios.bin /Y

@REM Generate Simics Rom
@REM
@REM This section is removed since the IFWI Binaries are not stored in the Repo
@REM
@REM pushd %TGL_SIMICS_TOOL_PATH%\fit
@REM fit -b -f tgl_spi_16mb.xml -w . -s . -o %SIMICS_ROM_LOCATION%\TGL_Simics_%TARGET%.bin
@REM popd

@echo on
