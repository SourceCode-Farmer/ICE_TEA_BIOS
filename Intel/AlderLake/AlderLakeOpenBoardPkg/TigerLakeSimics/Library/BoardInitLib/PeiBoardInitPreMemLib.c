/** @file
  Platform Hook Library instances

Copyright (c) 2017 - 2018, Intel Corporation. All rights reserved.<BR>
This program and the accompanying materials are licensed and made available under
the terms and conditions of the BSD License that accompanies this distribution.
The full text of the license may be found at
http://opensource.org/licenses/bsd-license.php.

THE PROGRAM IS DISTRIBUTED UNDER THE BSD LICENSE ON AN "AS IS" BASIS,
WITHOUT WARRANTIES OR REPRESENTATIONS OF ANY KIND, EITHER EXPRESS OR IMPLIED.

**/

#include <PiPei.h>
#include <Library/BaseLib.h>
#include <Library/IoLib.h>
#include <Library/BoardInitLib.h>
#include <Library/PcdLib.h>
#include <Library/DebugLib.h>

EFI_STATUS
EFIAPI
TigerLakeSimicsBoardDetect (
  VOID
  );

EFI_BOOT_MODE
EFIAPI
TigerLakeSimicsBoardBootModeDetect (
  VOID
  );

EFI_STATUS
EFIAPI
TigerLakeSimicsBoardDebugInit (
  VOID
  );

EFI_STATUS
EFIAPI
TigerLakeSimicsBoardInitBeforeMemoryInit (
  VOID
  );

EFI_STATUS
EFIAPI
TigerLakeSimicsBoardInitAfterMemoryInit (
  VOID
  );

EFI_STATUS
EFIAPI
BoardDetect (
  VOID
  )
{
  return TigerLakeSimicsBoardDetect ();
}

EFI_STATUS
EFIAPI
BoardDebugInit (
  VOID
  )
{
  return TigerLakeSimicsBoardDebugInit ();
}

EFI_BOOT_MODE
EFIAPI
BoardBootModeDetect (
  VOID
  )
{
  return TigerLakeSimicsBoardBootModeDetect ();
}

EFI_STATUS
EFIAPI
BoardInitBeforeMemoryInit (
  VOID
  )
{
  return TigerLakeSimicsBoardInitBeforeMemoryInit ();
}

EFI_STATUS
EFIAPI
BoardInitAfterMemoryInit (
  VOID
  )
{
  return TigerLakeSimicsBoardInitAfterMemoryInit ();
}

EFI_STATUS
EFIAPI
BoardInitBeforeTempRamExit (
  VOID
  )
{
  return EFI_SUCCESS;
}

EFI_STATUS
EFIAPI
BoardInitAfterTempRamExit (
  VOID
  )
{
  return EFI_SUCCESS;
}

