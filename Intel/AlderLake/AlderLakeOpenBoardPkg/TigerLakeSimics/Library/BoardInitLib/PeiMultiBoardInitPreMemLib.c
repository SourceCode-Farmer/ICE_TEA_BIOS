/** @file
  Platform Hook Library instances

Copyright (c) 2017 - 2018, Intel Corporation. All rights reserved.<BR>
This program and the accompanying materials are licensed and made available under
the terms and conditions of the BSD License that accompanies this distribution.
The full text of the license may be found at
http://opensource.org/licenses/bsd-license.php.

THE PROGRAM IS DISTRIBUTED UNDER THE BSD LICENSE ON AN "AS IS" BASIS,
WITHOUT WARRANTIES OR REPRESENTATIONS OF ANY KIND, EITHER EXPRESS OR IMPLIED.

**/

#include <PiPei.h>
#include <Library/BaseLib.h>
#include <Library/IoLib.h>
#include <Library/BoardInitLib.h>
#include <Library/MultiBoardInitSupportLib.h>
#include <Library/PcdLib.h>
#include <Library/DebugLib.h>

#include <TigerLakeId.h>

EFI_STATUS
EFIAPI
TigerLakeSimicsBoardDetect (
  VOID
  );

EFI_STATUS
EFIAPI
TigerLakeSimicsMultiBoardDetect (
  VOID
  );

EFI_BOOT_MODE
EFIAPI
TigerLakeSimicsBoardBootModeDetect (
  VOID
  );

EFI_STATUS
EFIAPI
TigerLakeSimicsBoardDebugInit (
  VOID
  );

EFI_STATUS
EFIAPI
TigerLakeSimicsBoardInitBeforeMemoryInit (
  VOID
  );

EFI_STATUS
EFIAPI
TigerLakeSimicsBoardInitAfterMemoryInit (
  VOID
  );

BOARD_DETECT_FUNC  mTigerLakeSimicsBoardDetectFunc = {
  TigerLakeSimicsMultiBoardDetect
};

BOARD_PRE_MEM_INIT_FUNC  mTigerLakeSimicsBoardPreMemInitFunc = {
  TigerLakeSimicsBoardDebugInit,
  TigerLakeSimicsBoardBootModeDetect,
  TigerLakeSimicsBoardInitBeforeMemoryInit,
  TigerLakeSimicsBoardInitAfterMemoryInit,
  NULL, // BoardInitBeforeTempRamExit
  NULL, // BoardInitAfterTempRamExit
};

EFI_STATUS
EFIAPI
TigerLakeSimicsMultiBoardDetect (
  VOID
  )
{
  TigerLakeSimicsBoardDetect ();
  if (LibPcdGetSku () == BoardIdTigerLakeSimics) {
    RegisterBoardPreMemInit (&mTigerLakeSimicsBoardPreMemInitFunc);
  } else {
    DEBUG ((DEBUG_ERROR, "ERROR - Unsupported Board\n"));
    ASSERT (FALSE);
  }
  return EFI_SUCCESS;
}

EFI_STATUS
EFIAPI
PeiTigerLakeSimicsMultiBoardInitPreMemLibConstructor (
  VOID
  )
{
  return RegisterBoardDetect (&mTigerLakeSimicsBoardDetectFunc);
}
