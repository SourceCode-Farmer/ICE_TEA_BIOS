/** @file
  Platform Hook Library instances

Copyright (c) 2017 - 2018, Intel Corporation. All rights reserved.<BR>
This program and the accompanying materials are licensed and made available under
the terms and conditions of the BSD License that accompanies this distribution.
The full text of the license may be found at
http://opensource.org/licenses/bsd-license.php.

THE PROGRAM IS DISTRIBUTED UNDER THE BSD LICENSE ON AN "AS IS" BASIS,
WITHOUT WARRANTIES OR REPRESENTATIONS OF ANY KIND, EITHER EXPRESS OR IMPLIED.

**/

#include <PiPei.h>
#include <Library/BaseLib.h>
#include <Library/IoLib.h>
#include <Library/BoardInitLib.h>
#include <Library/MultiBoardInitSupportLib.h>
#include <Library/PcdLib.h>
#include <Library/DebugLib.h>

#include <TigerLakeId.h>

EFI_STATUS
EFIAPI
TigerLakeSimicsBoardInitBeforeSiliconInit (
  VOID
  );

EFI_STATUS
EFIAPI
TigerLakeSimicsBoardInitAfterSiliconInit (
  VOID
  );

BOARD_POST_MEM_INIT_FUNC  mTigerLakeSimicsBoardInitFunc = {
  TigerLakeSimicsBoardInitBeforeSiliconInit,
  TigerLakeSimicsBoardInitAfterSiliconInit,
};

EFI_STATUS
EFIAPI
PeiTigerLakeSimicsMultiBoardInitLibConstructor (
  VOID
  )
{
  if (LibPcdGetSku () == BoardIdTigerLakeSimics) {
    return RegisterBoardPostMemInit (&mTigerLakeSimicsBoardInitFunc);
  }
  return EFI_SUCCESS;
}
