/** @file

Copyright (c) 2017 - 2018, Intel Corporation. All rights reserved.<BR>
This program and the accompanying materials are licensed and made available under
the terms and conditions of the BSD License that accompanies this distribution.
The full text of the license may be found at
http://opensource.org/licenses/bsd-license.php.

THE PROGRAM IS DISTRIBUTED UNDER THE BSD LICENSE ON AN "AS IS" BASIS,
WITHOUT WARRANTIES OR REPRESENTATIONS OF ANY KIND, EITHER EXPRESS OR IMPLIED.

**/

#include <PiPei.h>
#include <Library/DebugLib.h>
#include <Library/PcdLib.h>

#include <Library/SiliconInitLib.h>

#include "PeiGpioTable.h"

/**
  Tigerlake Simics board configuration init function for PEI post memory phase.

  PEI_BOARD_CONFIG_PCD_INIT

  @param  Content  pointer to the buffer contain init information for board init.

  @retval EFI_SUCCESS             The function completed successfully.
  @retval EFI_INVALID_PARAMETER   The parameter is NULL.
**/
EFI_STATUS
EFIAPI
TigerLakeSimicsInit (
  VOID
  )
{
  PcdSet32S (PcdBoardGpioTable, (UINTN) mGpioTableTglSimics);
  PcdSet16S (PcdBoardGpioTableSize, sizeof (mGpioTableTglSimics) / sizeof(GPIO_INIT_CONFIG));


  PcdSet8S (PcdUsb20OverCurrentPinPort0, 0x0);
  PcdSet8S (PcdUsb20OverCurrentPinPort1, 0x0);
  PcdSet8S (PcdUsb20OverCurrentPinPort2, 0x0);
  PcdSet8S (PcdUsb20OverCurrentPinPort3, 0x0);
  PcdSet8S (PcdUsb20OverCurrentPinPort4, 0x0);
  PcdSet8S (PcdUsb20OverCurrentPinPort5, 0x0);
  PcdSet8S (PcdUsb20OverCurrentPinPort6, 0x0);
  PcdSet8S (PcdUsb20OverCurrentPinPort7, 0x0);
  PcdSet8S (PcdUsb20OverCurrentPinPort8, 0x0);
  PcdSet8S (PcdUsb20OverCurrentPinPort9, 0x0);
  PcdSet8S (PcdUsb20OverCurrentPinPort10, 0x0);
  PcdSet8S (PcdUsb20OverCurrentPinPort11, 0x0);
  PcdSet8S (PcdUsb20OverCurrentPinPort12, 0x0);
  PcdSet8S (PcdUsb20OverCurrentPinPort13, 0x0);
  PcdSet8S (PcdUsb20OverCurrentPinPort14, 0x0);
  PcdSet8S (PcdUsb20OverCurrentPinPort15, 0x0);

  PcdSet8S (PcdUsb30OverCurrentPinPort0, 0x0);
  PcdSet8S (PcdUsb30OverCurrentPinPort1, 0x0);
  PcdSet8S (PcdUsb30OverCurrentPinPort2, 0x0);
  PcdSet8S (PcdUsb30OverCurrentPinPort3, 0x0);
  PcdSet8S (PcdUsb30OverCurrentPinPort4, 0x0);
  PcdSet8S (PcdUsb30OverCurrentPinPort5, 0x0);
  PcdSet8S (PcdUsb30OverCurrentPinPort6, 0x0);
  PcdSet8S (PcdUsb30OverCurrentPinPort7, 0x0);
  PcdSet8S (PcdUsb30OverCurrentPinPort8, 0x0);
  PcdSet8S (PcdUsb30OverCurrentPinPort9, 0x0);

  PcdSet8S (PcdCpuUsb30PortEnable, 0x0F);

  PcdSet8S (PcdSataPortsEnable0, 0x1);

  return EFI_SUCCESS;
}

/**
  Configures GPIO

  @param[in]  GpioTable       Point to Platform Gpio table
  @param[in]  GpioTableCount  Number of Gpio table entries

**/
VOID
ConfigureGpio (
  IN GPIO_INIT_CONFIG                 *GpioDefinition,
  IN UINT16                           GpioTableCount
  )
{
  EFI_STATUS          Status;

  DEBUG ((DEBUG_INFO, "ConfigureGpio() Start\n"));

  Status = GpioConfigurePads (GpioTableCount, GpioDefinition);

  DEBUG ((DEBUG_INFO, "ConfigureGpio() End\n"));
}

/**
  Configure GPIO

**/
VOID
GpioInit (
  VOID
  )
{
  ConfigureGpio ((VOID *) (UINTN) PcdGet32 (PcdBoardGpioTable), (UINTN) PcdGet16 (PcdBoardGpioTableSize));

  return;
}

/**
  Configure GPIO and SIO

  @retval  EFI_SUCCESS   Operation success.
**/
EFI_STATUS
EFIAPI
TigerLakeSimicsBoardInitBeforeSiliconInit (
  VOID
  )
{
  TigerLakeSimicsInit ();

  GpioInit ();

  ///
  /// Do Late PCH init
  ///
  LateSiliconInit ();

  return EFI_SUCCESS;
}

EFI_STATUS
EFIAPI
TigerLakeSimicsBoardInitAfterSiliconInit (
  VOID
  )
{
  return EFI_SUCCESS;
}
