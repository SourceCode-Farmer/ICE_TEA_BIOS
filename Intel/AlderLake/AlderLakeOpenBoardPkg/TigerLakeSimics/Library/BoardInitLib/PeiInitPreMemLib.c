/** @file

Copyright (c) 2017 - 2018, Intel Corporation. All rights reserved.<BR>
This program and the accompanying materials are licensed and made available under
the terms and conditions of the BSD License that accompanies this distribution.
The full text of the license may be found at
http://opensource.org/licenses/bsd-license.php.

THE PROGRAM IS DISTRIBUTED UNDER THE BSD LICENSE ON AN "AS IS" BASIS,
WITHOUT WARRANTIES OR REPRESENTATIONS OF ANY KIND, EITHER EXPRESS OR IMPLIED.

**/

#include <PiPei.h>
#include <Library/BaseMemoryLib.h>
#include <Library/PcdLib.h>
#include <Library/DebugLib.h>
#include <Library/PciLib.h>
#include <Library/MemoryAllocationLib.h>
#include <Library/PeiLib.h>
#include <Guid/MemoryOverwriteControl.h>

#include <Library/MmioInitLib.h>
#include <Library/SiliconInitLib.h>
#include <Library/PchCycleDecodingLib.h>
#include <Register/PmcRegs.h>
#include <Library/PmcLib.h>

#include "PeiSpdTable.h"

/**
  Tigerlake Simics board configuration init function for PEI pre-memory phase.

  PEI_BOARD_CONFIG_PCD_INIT

  @param  Content  pointer to the buffer contain init information for board init.

  @retval EFI_SUCCESS             The function completed successfully.
  @retval EFI_INVALID_PARAMETER   The parameter is NULL.
**/
EFI_STATUS
EFIAPI
TigerLakeSimicsInitPreMem (
  VOID
  )
{
  EFI_STATUS                        Status;
  UINTN                             VariableSize;
  VOID                              *MemorySavedData;
  UINT8                             MorControl;
  VOID                              *MorControlPtr;

  //
  // Initialize S3 Data variable (S3DataPtr). It may be used for warm and fast boot paths.
  //
  VariableSize = 0;
  MemorySavedData = NULL;
  Status = PeiGetVariable (
             L"MemoryConfig",
             &gFspNonVolatileStorageHobGuid,
             &MemorySavedData,
             &VariableSize
             );
  DEBUG ((DEBUG_INFO, "Get L\"MemoryConfig\" gFspNonVolatileStorageHobGuid - %r\n", Status));
  DEBUG ((DEBUG_INFO, "MemoryConfig Size - 0x%x\n", VariableSize));
  if (MemorySavedData != NULL) {
    PcdSet32S (PcdNvsBufferPtr, (UINT32)(UINTN)MemorySavedData);
    //
    // Set the DISB bit
    // after memory Data is saved to NVRAM.
    //
    PmcSetDramInitScratchpad ();
  }

  //
  // MOR
  //
  MorControl = 0;
  MorControlPtr = &MorControl;
  VariableSize = sizeof (MorControl);
  Status = PeiGetVariable (
             MEMORY_OVERWRITE_REQUEST_VARIABLE_NAME,
             &gEfiMemoryOverwriteControlDataGuid,
             &MorControlPtr,
             &VariableSize
             );
  DEBUG ((DEBUG_INFO, "MorControl - 0x%x (%r)\n", MorControl, Status));
  if (MOR_CLEAR_MEMORY_VALUE (MorControl)) {
    PcdSet8S (PcdCleanMemory, MorControl & MOR_CLEAR_MEMORY_BIT_MASK);
  }

  PcdSet32S (PcdStackBase, PcdGet32 (PcdTemporaryRamBase) + PcdGet32 (PcdTemporaryRamSize) - (PcdGet32 (PcdFspTemporaryRamSize) + PcdGet32 (PcdFspReservedBufferSize)));
  PcdSet32S (PcdStackSize, PcdGet32 (PcdFspTemporaryRamSize));
  PcdSet8S (PcdMrcSpdAddressTable0, 0xA0);
  PcdSet8S (PcdMrcSpdAddressTable1, 0xA2);
  PcdSet8S (PcdMrcSpdAddressTable2, 0xA4);
  PcdSet8S (PcdMrcSpdAddressTable3, 0xA6);
  PcdSet8S (PcdCpuRatio, 0x0);
  PcdSet8S (PcdBiosGuard, 0x0);

  return EFI_SUCCESS;
}

#define SIO_RUNTIME_REG_BASE_ADDRESS                          0x0680

/**
  Configure Super IO

**/
VOID
SioInit (
  VOID
  )
{
  //
  // Program and Enable Default Super IO Configuration Port Addresses and range
  //
  PchLpcGenIoRangeSet (PcdGet16 (PcdLpcSioConfigDefaultPort) & (~0xF), 0x10);

  //
  // 128 Byte Boundary and SIO Runtime Register Range is 0x0 to 0xF;
  //
  PchLpcGenIoRangeSet (SIO_RUNTIME_REG_BASE_ADDRESS  & (~0x7F), 0x10);

  return;
}

/**
  Configure GPIO and SIO before memory ready

  @retval  EFI_SUCCESS   Operation success.
**/
EFI_STATUS
EFIAPI
TigerLakeSimicsBoardInitBeforeMemoryInit (
  VOID
  )
{
  TigerLakeSimicsInitPreMem ();

  SioInit ();

  ///
  /// Do basic PCH init
  ///
  SiliconInit ();

  return EFI_SUCCESS;
}

/**
  Configure Memory-Mapped IO ranges

  @retval  EFI_SUCCESS   Operation success.
**/
EFI_STATUS
EFIAPI
TigerLakeSimicsBoardInitAfterMemoryInit (
  VOID
  )
{
  return MmioInit ();
}

EFI_STATUS
EFIAPI
TigerLakeSimicsBoardDebugInit (
  VOID
  )
{
  ///
  /// Do Early PCH init
  ///
  EarlySiliconInit ();
  return EFI_SUCCESS;
}

EFI_BOOT_MODE
EFIAPI
TigerLakeSimicsBoardBootModeDetect (
  VOID
  )
{
  return BOOT_WITH_DEFAULT_SETTINGS;
}

