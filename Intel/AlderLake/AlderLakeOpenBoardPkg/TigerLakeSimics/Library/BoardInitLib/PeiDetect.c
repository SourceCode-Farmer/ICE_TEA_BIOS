/** @file

Copyright (c) 2017 - 2018, Intel Corporation. All rights reserved.<BR>
This program and the accompanying materials are licensed and made available under
the terms and conditions of the BSD License that accompanies this distribution.
The full text of the license may be found at
http://opensource.org/licenses/bsd-license.php.

THE PROGRAM IS DISTRIBUTED UNDER THE BSD LICENSE ON AN "AS IS" BASIS,
WITHOUT WARRANTIES OR REPRESENTATIONS OF ANY KIND, EITHER EXPRESS OR IMPLIED.

**/

#include <PiPei.h>
#include <Library/PcdLib.h>
#include <Library/DebugLib.h>

#include <TigerLakeId.h>

BOOLEAN
IsTigerLakeSimics (
  VOID
  )
{
  return TRUE;
}

EFI_STATUS
EFIAPI
TigerLakeSimicsBoardDetect (
  VOID
  )
{
  if (LibPcdGetSku () != 0) {
    return EFI_SUCCESS;
  }

  DEBUG ((EFI_D_INFO, "TigerLakeSimicsDetectionCallback\n"));

  if (IsTigerLakeSimics ()) {
    LibPcdSetSku (BoardIdTigerLakeSimics);

    DEBUG ((DEBUG_INFO, "SKU_ID: 0x%x\n", LibPcdGetSku()));
    ASSERT (LibPcdGetSku() == BoardIdTigerLakeSimics);
  }
  return EFI_SUCCESS;
}
