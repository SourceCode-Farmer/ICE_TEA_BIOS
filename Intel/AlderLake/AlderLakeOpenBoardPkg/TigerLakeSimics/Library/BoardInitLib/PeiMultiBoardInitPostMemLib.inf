## @file
# Component information file for AlderLake Multi-Board Initialization in PEI post memory phase.
#
# Copyright (c) 2017 - 2018, Intel Corporation. All rights reserved.<BR>
#
# This program and the accompanying materials are licensed and made available under
# the terms and conditions of the BSD License which accompanies this distribution.
# The full text of the license may be found at
# http://opensource.org/licenses/bsd-license.php
#
# THE PROGRAM IS DISTRIBUTED UNDER THE BSD LICENSE ON AN "AS IS" BASIS,
# WITHOUT WARRANTIES OR REPRESENTATIONS OF ANY KIND, EITHER EXPRESS OR IMPLIED.
#
##

[Defines]
  INF_VERSION                    = 0x00010005
  BASE_NAME                      = PeiAlderLakeSimicsMultiBoardInitLib
  FILE_GUID                      = C7D39F17-E5BA-41D9-8DFE-FF9017499280
  MODULE_TYPE                    = BASE
  VERSION_STRING                 = 1.0
  LIBRARY_CLASS                  = NULL
  CONSTRUCTOR                    = PeiAlderLakeSimicsMultiBoardInitLibConstructor

[LibraryClasses]
  BaseLib
  DebugLib
  BaseMemoryLib
  MemoryAllocationLib
  PcdLib
  SiliconInitLib
  MultiBoardInitSupportLib
  PciSegmentLib
  GpioLib

[Packages]
  MinPlatformPkg/MinPlatformPkg.dec
  AlderLakeOpenBoardPkg/OpenBoardPkg.dec
  MdePkg/MdePkg.dec
  MdeModulePkg/MdeModulePkg.dec
  ClientOneSiliconPkg/SiPkg.dec

[Sources]
  PeiInitPostMemLib.c
  PeiMultiBoardInitPostMemLib.c

[FixedPcd]

[Pcd]
  gBoardModuleTokenSpaceGuid.PcdBoardGpioTable
  gBoardModuleTokenSpaceGuid.PcdBoardGpioTableSize

  # USB 2.0 Port Over Current Pin
  gBoardModuleTokenSpaceGuid.PcdUsb20OverCurrentPinPort0
  gBoardModuleTokenSpaceGuid.PcdUsb20OverCurrentPinPort1
  gBoardModuleTokenSpaceGuid.PcdUsb20OverCurrentPinPort2
  gBoardModuleTokenSpaceGuid.PcdUsb20OverCurrentPinPort3
  gBoardModuleTokenSpaceGuid.PcdUsb20OverCurrentPinPort4
  gBoardModuleTokenSpaceGuid.PcdUsb20OverCurrentPinPort5
  gBoardModuleTokenSpaceGuid.PcdUsb20OverCurrentPinPort6
  gBoardModuleTokenSpaceGuid.PcdUsb20OverCurrentPinPort7
  gBoardModuleTokenSpaceGuid.PcdUsb20OverCurrentPinPort8
  gBoardModuleTokenSpaceGuid.PcdUsb20OverCurrentPinPort9
  gBoardModuleTokenSpaceGuid.PcdUsb20OverCurrentPinPort10
  gBoardModuleTokenSpaceGuid.PcdUsb20OverCurrentPinPort11
  gBoardModuleTokenSpaceGuid.PcdUsb20OverCurrentPinPort12
  gBoardModuleTokenSpaceGuid.PcdUsb20OverCurrentPinPort13
  gBoardModuleTokenSpaceGuid.PcdUsb20OverCurrentPinPort14
  gBoardModuleTokenSpaceGuid.PcdUsb20OverCurrentPinPort15

  # USB 3.0 Port Over Current Pin
  gBoardModuleTokenSpaceGuid.PcdUsb30OverCurrentPinPort0
  gBoardModuleTokenSpaceGuid.PcdUsb30OverCurrentPinPort1
  gBoardModuleTokenSpaceGuid.PcdUsb30OverCurrentPinPort2
  gBoardModuleTokenSpaceGuid.PcdUsb30OverCurrentPinPort3
  gBoardModuleTokenSpaceGuid.PcdUsb30OverCurrentPinPort4
  gBoardModuleTokenSpaceGuid.PcdUsb30OverCurrentPinPort5
  gBoardModuleTokenSpaceGuid.PcdUsb30OverCurrentPinPort6
  gBoardModuleTokenSpaceGuid.PcdUsb30OverCurrentPinPort7
  gBoardModuleTokenSpaceGuid.PcdUsb30OverCurrentPinPort8
  gBoardModuleTokenSpaceGuid.PcdUsb30OverCurrentPinPort9
  gBoardModuleTokenSpaceGuid.PcdCpuUsb30PortEnable

  # SATA
  gBoardModuleTokenSpaceGuid.PcdSataPortsEnable0
