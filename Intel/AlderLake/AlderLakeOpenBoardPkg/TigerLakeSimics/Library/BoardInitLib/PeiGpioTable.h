/** @file
  GPIO definition table for Tigerlake Simics

Copyright (c) 2017 - 2020, Intel Corporation. All rights reserved.<BR>
This program and the accompanying materials are licensed and made available under
the terms and conditions of the BSD License that accompanies this distribution.
The full text of the license may be found at
http://opensource.org/licenses/bsd-license.php.

THE PROGRAM IS DISTRIBUTED UNDER THE BSD LICENSE ON AN "AS IS" BASIS,
WITHOUT WARRANTIES OR REPRESENTATIONS OF ANY KIND, EITHER EXPRESS OR IMPLIED.

**/

#ifndef _TIGERLAKE_SIMICS_GPIO_TABLE_H_
#define _TIGERLAKE_SIMICS_GPIO_TABLE_H_

#include <Pins/GpioPinsVer2Lp.h>
#include <Library/GpioLib.h>
#include <Library/GpioConfig.h>

///
/// !!! GPIOs designated to Native Functions shall not be configured by Platform Code.
/// Native Pins shall be configured by Silicon Code (based on BIOS policies setting) or soft straps(set by CSME in FITc).
/// Configuring pins to native function in GPIO table would override the pin settings resulting in unexpected behavior.
///

static GPIO_INIT_CONFIG mGpioTableTglSimics[] =
{
  // CPU M.2 SSD
  {GPIO_VER2_LP_GPP_D16, {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutHigh,   GpioIntDis,GpioPlatformReset,  GpioTermNone}},  //CPU SSD PWREN
  {GPIO_VER2_LP_GPP_A11, {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutHigh,   GpioIntDis,GpioPlatformReset,  GpioTermNone}},  //CPU SSD RESET

  // M.2 Key-E - WLAN/BT
  {GPIO_VER2_LP_GPP_A13, {GpioPadModeGpio, GpioHostOwnDefault, GpioDirOut, GpioOutHigh, GpioIntDis, GpioHostDeepReset, GpioTermNone, GpioPadConfigUnlock }}, // BT_RF_KILL_N
  {GPIO_VER2_LP_GPP_B15, {GpioPadModeGpio, GpioHostOwnDefault, GpioDirOut, GpioOutHigh, GpioIntDis, GpioHostDeepReset, GpioTermNone, GpioPadConfigUnlock }}, // WIFI_RF_KILL_N
  {GPIO_VER2_LP_GPP_C22, {GpioPadModeGpio, GpioHostOwnDefault, GpioDirOut, GpioOutHigh, GpioIntDis, GpioHostDeepReset, GpioTermNone, GpioPadConfigUnlock }}, // WLAN_RST_N
//  All SCI GPIO's are not programmed due to HFPGA issue : 1505908653
//  {GPIO_VER2_LP_GPP_C23, {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirInInv,  GpioOutDefault,GpioIntLevel|GpioIntSci,GpioHostDeepReset,  GpioTermNone,  GpioPadConfigUnlock  }},  //WIFI_WAKE_N
//  {GPIO_VER2_LP_GPP_H19, {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirInInv,  GpioOutDefault,GpioIntLevel|GpioIntSci,GpioHostDeepReset,  GpioTermNone,  GpioPadConfigUnlock  }},  //UART_BT_WAKE_N : Not default POR

  // X4 Pcie Slot for Gen3 and Gen 4
  {GPIO_VER2_LP_GPP_A14, {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutLow,   GpioIntDis,GpioPlatformReset,  GpioTermNone}},  //ONBOARD_X4_PCIE_SLOT1_PWREN_N
  {GPIO_VER2_LP_GPP_C13, {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutHigh,   GpioIntDis,GpioPlatformReset,  GpioTermNone}},  //ONBOARD_X4_PCIE_SLOT1_RESET_N
//  {GPIO_VER2_LP_GPP_B3,  {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirInInv,  GpioOutDefault,GpioIntLevel|GpioIntSci,GpioHostDeepReset,  GpioTermNone,  GpioPadConfigUnlock  }},  //ONBOARD_X4_PCIE_SLOT1_WAKE_N
  {GPIO_VER2_LP_GPP_F20, {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutLow,   GpioIntDis,GpioPlatformReset,  GpioTermNone}},  //ONBOARD_X4_PCIE_SLOT1_DGPU_SEL
  {GPIO_VER2_LP_GPP_F21, {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirIn,  GpioOutDefault,GpioIntDis,GpioPlatformReset,  GpioTermNone}},  //ONBOARD_X4_PCIE_SLOT1_DGPU_PWROK

  // TBT Re-Timers
  {GPIO_VER2_LP_GPP_A23, {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutLow,   GpioIntDis,GpioPlatformReset,  GpioTermNone}},  //TC_RETIMER_FORCE_PWR
  {GPIO_VER2_LP_GPD7,    {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutHigh,  GpioIntDis,GpioDswReset,  GpioTermNone}},  //TCP_RETIMER_PERST_N

  // Battery Charger Vmin to PCH PROCHOT, derived from TGL
//  {GPIO_VER2_LP_GPP_B2,  {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirInInv,  GpioOutDefault,GpioIntEdge|GpioIntSci,GpioPlatformReset,  GpioTermNone,  GpioPadConfigUnlock  }},  //BC_PROCHOT_N

  // SATA Direct Connect
  {GPIO_VER2_LP_GPP_B4,  {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutHigh,   GpioIntDis,GpioPlatformReset,  GpioTermNone}},  //SATA_DIRECT_PWREN

  // FPS
  {GPIO_VER2_LP_GPP_B14, {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutHigh,   GpioIntDis,GpioPlatformReset,  GpioTermNone}},  //FPS_RST_N
  {GPIO_VER2_LP_GPP_E3,  {GpioPadModeGpio, GpioHostOwnGpio, GpioDirIn,  GpioOutDefault,GpioIntLevel|GpioIntApic,GpioPlatformReset,  GpioTermNone,  GpioPadConfigUnlock  }},  //FPS_INT

  // PCH M.2 SSD
  {GPIO_VER2_LP_GPP_B16, {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutHigh,   GpioIntDis,GpioPlatformReset,  GpioTermNone}},  //M2_PCH_SSD_PWREN
  {GPIO_VER2_LP_GPP_H0,  {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutHigh,   GpioIntDis,GpioPlatformReset,  GpioTermNone}},  //M2_SSD_RST_N

  // Camera
  {GPIO_VER2_LP_GPP_B23, {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutHigh,   GpioIntDis,GpioPlatformReset,  GpioTermNone}},  //CRD_CAM_PWREN - CAM1
  {GPIO_VER2_LP_GPP_C15, {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutHigh,   GpioIntDis,GpioPlatformReset,  GpioTermNone}},  //WF_CAM_RST_N  - CAM1

  {GPIO_VER2_LP_GPP_R6,  {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutHigh,   GpioIntDis,GpioPlatformReset,  GpioTermNone}},  //CAM2_PWREN/BIOS_REC
  {GPIO_VER2_LP_GPP_H12, {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutHigh,   GpioIntDis,GpioPlatformReset,  GpioTermNone}},  //CAM2_RST_N

  {GPIO_VER2_LP_GPP_H15, {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutHigh,   GpioIntDis,GpioPlatformReset,  GpioTermNone}},  //CAM3_PWREN
  {GPIO_VER2_LP_GPP_H13, {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutHigh,   GpioIntDis,GpioPlatformReset,  GpioTermNone}},  //CAM3_RST_N

  // Camera Common GPIO's for all Camera, Rework Options
  {GPIO_VER2_LP_GPP_B18, {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutLow,   GpioIntDis,GpioPlatformReset,  GpioTermNone}},  //CRD_CAM_STROBE_1
  {GPIO_VER2_LP_GPP_R5,  {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutLow,   GpioIntDis,GpioPlatformReset,  GpioTermNone}},  //CRD_CAM_PRIVACY_LED_1
  {GPIO_VER2_LP_GPP_C2,  {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutHigh,   GpioIntDis,GpioPlatformReset,  GpioTermNone}},  //WF_CAM_CLK_EN

  // Audio
  {GPIO_VER2_LP_GPP_C5,  {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutHigh,   GpioIntDis,GpioPlatformReset,  GpioTermNone}},  //SPKR_PD_N
  {GPIO_VER2_LP_GPP_C12, {GpioPadModeGpio, GpioHostOwnGpio, GpioDirInInv,  GpioOutDefault,GpioIntLevel|GpioIntApic,GpioPlatformReset,  GpioTermNone,  GpioPadConfigUnlock  }},  // CODEC_INT_N

  // Touch Pad
  // Touch Pad and Touch Panel 2 share the same Power Enable, default is Touch pad
  {GPIO_VER2_LP_GPP_H1,  {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutHigh,   GpioIntDis,GpioPlatformReset,  GpioTermNone}},  //TCH_PAD_LS_EN - PWR_En
  {GPIO_VER2_LP_GPP_C8,  {GpioPadModeGpio, GpioHostOwnGpio, GpioDirInInv,  GpioOutDefault,GpioIntEdge|GpioIntApic,GpioPlatformReset,  GpioTermNone,  GpioPadConfigUnlock  }},  //TCH_PAD_INT_N

  // EC
 // {GPIO_VER2_LP_GPP_E7,  {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirInInv,  GpioOutDefault,GpioIntLevel|GpioIntSmi,GpioPlatformReset,  GpioTermNone,  GpioPadConfigUnlock  }},  //EC_SMI_N
  {GPIO_VER2_LP_GPP_E8,  {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutHigh,   GpioIntDis,GpioPlatformReset,  GpioTermNone}},  //EC_SLP_S0_CS_N

  // SPI TPM, derived from TGL
 // {GPIO_VER2_LP_GPP_C14, {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirInInv,  GpioOutDefault,GpioIntLevel|GpioIntSci,GpioHostDeepReset,  GpioTermNone,  GpioPadConfigUnlock  }},  //SPI_TPM_INT_N

  // TypeC BIAS : Not used by default in RVP, derived from TGL
  {GPIO_VER2_LP_GPP_E22, {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutLow,   GpioIntDis,GpioPlatformReset,  GpioTermNone}},  //DISP_AUX_P_BIAS_GPIO
  {GPIO_VER2_LP_GPP_E23, {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutHigh,   GpioIntDis,GpioPlatformReset,  GpioTermNone}},  //DISP_AUX_N_BIAS_GPIO

  // LAN : Not used by Default in RVP
  {GPIO_VER2_LP_GPP_F7,  {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutHigh,   GpioIntDis,GpioPlatformReset,  GpioTermNone}},  //LAN_RST_N

  // X1 Pcie Slot
  {GPIO_VER2_LP_GPP_H2,  {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutHigh,   GpioIntDis,GpioPlatformReset,  GpioTermNone}},  //X1 Slot PWREN
 // {GPIO_VER2_LP_GPP_F9,  {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirInInv,  GpioOutDefault,GpioIntLevel|GpioIntSci,GpioHostDeepReset,  GpioTermNone,  GpioPadConfigUnlock  }},  //X1 Slot WAKE
  {GPIO_VER2_LP_GPP_F10, {GpioPadModeGpio, GpioHostOwnAcpi, GpioDirOut,  GpioOutHigh,   GpioIntDis,GpioPlatformReset,  GpioTermNone}},  //X1 Slot RESET
};

#endif // _TIGERLAKE_SIMICS_GPIO_TABLE_H_
