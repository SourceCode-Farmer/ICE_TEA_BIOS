@REM @file
@REM
@REM Copyright (c) 2018 - 2020, Intel Corporation. All rights reserved.<BR>
@REM This program and the accompanying materials
@REM are licensed and made available under the terms and conditions of the BSD License
@REM which accompanies this distribution.  The full text of the license may be found at
@REM http://opensource.org/licenses/bsd-license.php
@REM
@REM THE PROGRAM IS DISTRIBUTED UNDER THE BSD LICENSE ON AN "AS IS" BASIS,
@REM WITHOUT WARRANTIES OR REPRESENTATIONS OF ANY KIND, EITHER EXPRESS OR IMPLIED.
@REM

@echo off

pushd ..\..\..\

@REM Set WORKSPACE environment.
@REM %__CD__% appends a backslash to every directory, not just the root as %CD% does
@REM This allows directory concatenation when the workspace is a drive root
@set WORKSPACE=%__CD__%.
echo.
echo Set WORKSPACE as: %WORKSPACE%
echo.

@REM Create the Conf directory under WORKSPACE
if not exist %WORKSPACE%\Conf (
  mkdir Conf
)

popd

set WORKSPACE_PLATFORM=%WORKSPACE%\Intel
set WORKSPACE_SILICON=%WORKSPACE%\Intel
set WORKSPACE_COMMON=%WORKSPACE%\Intel
set WORKSPACE_PLATFORM_BIN=%WORKSPACE%\Intel
set WORKSPACE_SILICON_BIN=%WORKSPACE%\Intel
set WORKSPACE_FSP_BIN=%WORKSPACE%\Intel
set WORKSPACE_CORE=%WORKSPACE%\Edk2
set WORKSPACE_CORE_SILICON=%WORKSPACE%\Edk2Platforms\Silicon\Intel
set WORKSPACE_CORE_PLATFORM=%WORKSPACE%\Edk2Platforms\Platform\Intel
set WORKSPACE_CORE_FEATURES=%WORKSPACE%\Edk2Platforms\Features\Intel
set EDK_TOOLS_BIN=%WORKSPACE_CORE%\BaseTools\Bin\Win32
set EDK_TOOLS_PATH=%WORKSPACE_CORE%\BaseTools

if not defined NASM_PREFIX set NASM_PREFIX=%SystemDrive%\nasm\
%NASM_PREFIX%\nasm.exe -v
if errorlevel 1 (
  @echo ERROR - NASM is not found at %NASM_PREFIX%
  exit \b 1
)

if not defined IASL_PREFIX set IASL_PREFIX=C:\ASL\
%IASL_PREFIX%\iasl.exe -v
if errorlevel 1 (
  @echo ERROR - IASL is not found at %IASL_PREFIX%
  exit \b 1
)

@if not defined PYTHON_COMMAND (
  set PYTHON_COMMAND=py -3
  %PYTHON_COMMAND% --version
  if not %ERRORLEVEL% == 0 (
    @echo ERROR - PYTHON_COMMAND is not defined and py -3 is invalid
    exit \b 1
  )
)

%PYTHON_COMMAND% --version
if not %ERRORLEVEL% == 0 (
  @echo ERROR - %PYTHON_COMMAND% is invalid
  exit \b 1
)

set PACKAGES_PATH=
for /f %%i in (
  '%PYTHON_COMMAND% %WORKSPACE_CORE_PLATFORM%\Tools\AppendPackagesPath\GetPackagesPath.py ^
  %WORKSPACE_CORE_SILICON% %WORKSPACE_CORE_PLATFORM% %WORKSPACE_CORE_FEATURES% ^
  %WORKSPACE_CORE% %WORKSPACE%\Intel ^
') do (
  set "PACKAGES_PATH=%%i"
)

if not defined PACKAGES_PATH (
  @echo ERROR - Failed to set PACKAGES_PATH
  exit \b 1
)

if not exist %WORKSPACE%\Conf mkdir %WORKSPACE%\Conf

@echo Compile build tools provided by BaseTools
call %WORKSPACE_PLATFORM%\AlderLakeOpenBoardPkg\PrepareBaseTools.cmd

@echo on
