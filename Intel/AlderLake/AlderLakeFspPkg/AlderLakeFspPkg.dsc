## @file
# 
#******************************************************************************
#* Copyright 2021 Insyde Software Corp. All Rights Reserved.
#*
#* You may not reproduce, distribute, publish, display, perform, modify, adapt,
#* transmit, broadcast, present, recite, release, license or otherwise exploit
#* any part of this publication in any form, by any means, without the prior
#* written permission of Insyde Software Corp.
#*
#******************************************************************************
## @file
#  Platform description.
#
# @copyright
#  INTEL CONFIDENTIAL
#  Copyright 2016 - 2021 Intel Corporation.
#
#  The source code contained or described herein and all documents related to the
#  source code ("Material") are owned by Intel Corporation or its suppliers or
#  licensors. Title to the Material remains with Intel Corporation or its suppliers
#  and licensors. The Material may contain trade secrets and proprietary and
#  confidential information of Intel Corporation and its suppliers and licensors,
#  and is protected by worldwide copyright and trade secret laws and treaty
#  provisions. No part of the Material may be used, copied, reproduced, modified,
#  published, uploaded, posted, transmitted, distributed, or disclosed in any way
#  without Intel's prior express written permission.
#
#  No license under any patent, copyright, trade secret or other intellectual
#  property right is granted to or conferred upon you by disclosure or delivery
#  of the Materials, either expressly, by implication, inducement, estoppel or
#  otherwise. Any license under such intellectual property rights must be
#  express and approved by Intel in writing.
#
#  Unless otherwise agreed by Intel in writing, you may not remove or alter
#  this notice or any other notice embedded in Materials by Intel or
#  Intel's suppliers or licensors in any way.
#
#  This file contains an 'Intel Peripheral Driver' and is uniquely identified as
#  "Intel Reference Module" and is licensed for Intel CPUs and chipsets under
#  the terms of your license agreement with Intel or your vendor. This file may
#  be modified by the user, subject to additional terms of the license agreement.
#
# @par Specification
##

[Defines]
  #
  # Set platform specific package/folder name, same as passed from PREBUILD script.
  # PLATFORM_PACKAGE would be the same as PLATFORM_NAME as well as package build folder
  # DEFINE only takes effect at R9 DSC and FDF.
  #
  DEFINE      FSP_PACKAGE                     = AlderLakeFspPkg
  DEFINE      PLATFORM_SI_PACKAGE             = ClientOneSiliconPkg
  DEFINE      C1S_FSP_PATH                    = $(PLATFORM_SI_PACKAGE)/Fsp
  DEFINE      C1S_PRODUCT_PATH                = $(PLATFORM_SI_PACKAGE)/Product/AlderLake
#[-start-190328-IB10860300-modify]#
  DEFINE      CHIPSET_PKG                     = AlderLakeChipsetPkg
#[-end-190328-IB10860300-modify]#

#[-start-190719-IB04530025-modify]#
  !include $(PROJECT_PKG_ROOT)/Project.env
#[-end-190719-IB04530025-modify]#

  #
  # Silicon On/Off feature are defined here
  # Should put it before a new section, since it also has section.
  #
  !include $(C1S_PRODUCT_PATH)/SiPkgPcdInit.dsc

  #
  # BIOS build switches configuration
  #
  !include $(FSP_PACKAGE)/FspPkgPcdInit.dsc
  !include $(FSP_PACKAGE)/FspPkgPcdUpdate.dsc

[PcdsFixedAtBuild]
#[-start-201125-IB16560229-add]#
  gSiPkgTokenSpaceGuid.PcdTxtEnable|$(TXT_SUPPORT)
#[-end-201125-IB16560229-add]#
#[-start-210113-IB16560238-add]#
  gSiPkgTokenSpaceGuid.PcdAmtEnable|$(AMT_ENABLE)
#[-end-210113-IB16560238-add]#
#[-start-210429-IB16560244-add]#
  gSiPkgTokenSpaceGuid.PcdOverclockEnable|$(OVERCLOCK_ENABLE)
#[-end-210429-IB16560244-add]#
################################################################################
#
# Defines Section - statements that will be processed to create a Makefile.
#
################################################################################
[Defines]
  PLATFORM_NAME                  = $(FSP_PACKAGE)
  PLATFORM_GUID                  = 1BEDB57A-7904-406e-8486-C89FC7FB39EE
  PLATFORM_VERSION               = 0.1
  DSC_SPECIFICATION              = 0x00010005
  OUTPUT_DIRECTORY               = $(CFG_OUTDIR)/$(FSP_PACKAGE)
  SUPPORTED_ARCHITECTURES        = IA32|X64
  BUILD_TARGETS                  = DEBUG|RELEASE
  SKUID_IDENTIFIER               = DEFAULT
  FLASH_DEFINITION               = $(FSP_PACKAGE)/$(FSP_PACKAGE).fdf

  #
  # UPD tool definition
  #
  FSP_T_UPD_TOOL_GUID            = 34686CA3-34F9-4901-B82A-BA630F0714C6
  FSP_M_UPD_TOOL_GUID            = 39A250DB-E465-4DD1-A2AC-E2BD3C0E2385
  FSP_S_UPD_TOOL_GUID            = CAE3605B-5B34-4C85-B3D7-27D54273C40F
  FSP_T_UPD_FFS_GUID             = 70BCF6A5-FFB1-47D8-B1AE-EFE5508E23EA
  FSP_M_UPD_FFS_GUID             = D5B86AEA-6AF7-40D4-8014-982301BC3D89
  FSP_S_UPD_FFS_GUID             = E3CD9B18-998C-4F76-B65E-98B154E5446F

  DEFINE  PCH = Cnl

################################################################################
#
# SKU Identification section - list of all SKU IDs supported by this
#                              Platform.
#
################################################################################
[SkuIds]
  0|DEFAULT              # The entry: 0|DEFAULT is reserved and always required.

################################################################################
#
# Library Class section - list of all Library Classes needed by this Platform.
#
################################################################################
!include MdePkg/MdeLibs.dsc.inc

[LibraryClasses.IA32]
  !include $(PLATFORM_SI_PACKAGE)/Fru/AdlPch/PeiLib.dsc
#
# UEFI & PI
#
  PeiServicesTablePointerLib|MdePkg/Library/PeiServicesTablePointerLibIdt/PeiServicesTablePointerLibIdt.inf
  PeiServicesLib|MdePkg/Library/PeiServicesLib/PeiServicesLib.inf
  ExtractGuidedSectionLib|MdePkg/Library/PeiExtractGuidedSectionLib/PeiExtractGuidedSectionLib.inf
  PeiPolicyUpdatePreMemLib|$(FSP_PACKAGE)/Library/PeiPolicyUpdatePreMemLib/PeiPolicyUpdatePreMemLib.inf
  PeiPolicyUpdateLib|$(FSP_PACKAGE)/Library/PeiPolicyUpdateLib/PeiPolicyUpdateLib.inf
  ResetSystemLib|MdeModulePkg/Library/PeiResetSystemLib/PeiResetSystemLib.inf
  FspHelperLib|$(C1S_FSP_PATH)/Library/FspHelperLib/FspHelperLib.inf

#
# Silicon Init Pei Library
#
  PeiPcieRpLib|$(PLATFORM_SI_PACKAGE)/IpBlock/PcieRp/Library/PeiPcieRpLib/PeiPcieRpLib.inf
  SiPolicyLib|$(PLATFORM_SI_PACKAGE)/Product/AlderLake/Library/PeiSiPolicyLib/PeiSiPolicyLib.inf
  SiConfigBlockLib|$(PLATFORM_SI_PACKAGE)/Library/BaseSiConfigBlockLib/BaseSiConfigBlockLib.inf
  SiFviInitLib|$(PLATFORM_SI_PACKAGE)/LibraryPrivate/PeiSiFviInitLib/PeiSiFviInitLib.inf
  StallPpiLib|$(PLATFORM_SI_PACKAGE)/Library/PeiInstallStallPpiLib/PeiStallPpiLib.inf
  SiPolicyOverrideLib|$(PLATFORM_SI_PACKAGE)/LibraryPrivate/PeiPolicyOverrideLib/PeiSiPolicyOverrideLib.inf
  PeiSiSsidLib|$(PLATFORM_SI_PACKAGE)/LibraryPrivate/PeiSiSsidLib/PeiSiSsidLib.inf
  SiMtrrLib|$(PLATFORM_SI_PACKAGE)/Library/SiMtrrLib/SiMtrrLib.inf
  PeiFiaPolicyLib|$(PLATFORM_SI_PACKAGE)/IpBlock/Sps/Library/PeiPchFiaPolicyNullLib/PeiFiaPolicyNullLib.inf
  PeiMeServerPreMemPolicyLib|$(PLATFORM_SI_PACKAGE)/IpBlock/Sps/Library/PeiMeServerPreMemPolicyLib/PeiMeServerPreMemPolicyNullLib.inf

#
# Pch
#

  PchPolicyLib|$(PLATFORM_SI_PACKAGE)/Pch/Library/PeiPchPolicyLib/PeiPchPolicyLib.inf
  TraceHubDebugLib|$(PLATFORM_SI_PACKAGE)/Library/TraceHubDebugLibSvenTx/BaseTraceHubDebugLibSvenTx.inf
  PeiCpuTraceHubPolicyLib|$(PLATFORM_SI_PACKAGE)/IpBlock/TraceHub/LibraryPrivate/PeiCpuTraceHubPolicyLib/PeiCpuTraceHubPolicyLib.inf
  SerialIoI2cMasterLib|$(PLATFORM_SI_PACKAGE)/IpBlock/SerialIo/I2c/Library/PeiSerialIoI2cMasterLib/PeiSerialIoI2cMasterLib.inf
  SerialIoI2cMasterCommonLib|$(PLATFORM_SI_PACKAGE)/IpBlock/SerialIo/I2c/LibraryPrivate/PeiDxeSerialIoI2cMasterCommonLib/PeiDxeSerialIoI2cMasterCommonLib.inf
  SerialIoI2cLib|$(PLATFORM_SI_PACKAGE)/IpBlock/SerialIo/I2c/Library/BaseSerialIoI2cLib/BaseSerialIoI2cLib.inf
  SerialIoI2cPrivateLib|$(PLATFORM_SI_PACKAGE)/IpBlock/SerialIo/I2c/LibraryPrivate/SerialIoI2cPrivateLib/SerialIoI2cPrivateLib.inf
  SerialIoSpiLib|$(PLATFORM_SI_PACKAGE)/IpBlock/SerialIo/Spi/Library/BaseSerialIoSpiLib/BaseSerialIoSpiLib.inf
  PeiHsioLib|$(PLATFORM_SI_PACKAGE)/IpBlock/Hsio/LibraryPrivate/PeiHsioLib/PeiHsioLib.inf
  ChipsetInitLib|$(PLATFORM_SI_PACKAGE)/IpBlock/Hsio/LibraryPrivate/ChipsetInitLib/ChipsetInitLib.inf
  HsioSocLib|$(PLATFORM_SI_PACKAGE)/Fru/AdlPch/LibraryPrivate/HsioSocLib/HsioSocLibAdl.inf
!if gSiPkgTokenSpaceGuid.PcdAdlLpSupport == TRUE
  PchInitLib|$(PLATFORM_SI_PACKAGE)/Fru/AdlPch/LibraryPrivate/PeiPchInitLib/PeiPchInitLibFspTgl.inf
!else
  PchInitLib|$(PLATFORM_SI_PACKAGE)/Fru/AdlPch/LibraryPrivate/PeiPchInitLib/PeiPchInitLibFspAdl.inf
!endif

  PeiPchPcieClocksLib|$(PLATFORM_SI_PACKAGE)/IpBlock/PcieRp/LibraryPrivate/PeiPchPcieClocksLib/PeiPchPcieClocksLibVer2.inf
  SiScheduleResetLib|$(PLATFORM_SI_PACKAGE)/Pch/LibraryPrivate/BaseSiScheduleResetLib/BaseSiScheduleResetLib.inf

  PeiHybridStorageLib|$(PLATFORM_SI_PACKAGE)/IpBlock/Rst/LibraryPrivate/PeiHybridStorageLib/PeiHybridStorageLib.inf
  PeiHybridStoragePolicyLib|$(PLATFORM_SI_PACKAGE)/IpBlock/Rst/LibraryPrivate/PeiHybridStoragePolicyLib/PeiHybridStoragePolicyLib.inf
  PeiRstPrivateLib|$(PLATFORM_SI_PACKAGE)/IpBlock/Rst/LibraryPrivate/PeiRstPrivateLib/PeiRstPrivateLib.inf
  PeiSpiPolicyLib|$(PLATFORM_SI_PACKAGE)/IpBlock/Spi/LibraryPrivate/PeiSpiPolicyLib/PeiSpiPolicyLib.inf
#
# Cpu
#
  CpuInitLib|$(PLATFORM_SI_PACKAGE)/Cpu/LibraryPrivate/PeiCpuInitLib/PeiCpuInitLib.inf
  CpuPowerMgmtLib|$(PLATFORM_SI_PACKAGE)/Cpu/LibraryPrivate/PeiCpuPowerMgmtLib/PeiCpuPowerMgmtLib.inf
!if gSiPkgTokenSpaceGuid.PcdTxtEnable == TRUE
  PeiTxtLib|$(PLATFORM_SI_PACKAGE)/Cpu/LibraryPrivate/PeiTxtLib/PeiTxtLib.inf
!else
  PeiTxtLib|$(PLATFORM_SI_PACKAGE)/Cpu/LibraryPrivate/PeiTxtLibNull/PeiTxtLibNull.inf
!endif
!if gSiPkgTokenSpaceGuid.PcdCpuPowerOnConfigEnable == TRUE
  CpuPowerOnConfigLib|$(PLATFORM_SI_PACKAGE)/Cpu/LibraryPrivate/PeiCpuPowerOnConfigLib/PeiCpuPowerOnConfigLib.inf
!else
  CpuPowerOnConfigLib|$(PLATFORM_SI_PACKAGE)/Cpu/LibraryPrivate/PeiCpuPowerOnConfigLibDisable/PeiCpuPowerOnConfigLibDisable.inf
!endif
#
#@todo Update PeiOcPolicyLib.inf solution once config block redesign is finalized for optional IP's
#
  PeiOcPolicyLib|$(PLATFORM_SI_PACKAGE)/IpBlock/Overclocking/LibraryPrivate/PeiOcPolicyLib/PeiOcPolicyLib.inf
!if gSiPkgTokenSpaceGuid.PcdOverclockEnable == TRUE
  PeiOcLib|$(PLATFORM_SI_PACKAGE)/IpBlock/Overclocking/LibraryPrivate/PeiOcLib/PeiOcLib.inf
  PeiOcInitLib|$(PLATFORM_SI_PACKAGE)/IpBlock/Overclocking/LibraryPrivate/PeiOcInitLib/PeiOcInitLib.inf
!else
  PeiOcLib|$(PLATFORM_SI_PACKAGE)/IpBlock/Overclocking/LibraryPrivate/PeiOcLibNull/PeiOcLibNull.inf
  PeiOcInitLib|$(PLATFORM_SI_PACKAGE)/IpBlock/Overclocking/LibraryPrivate/PeiOcInitLibNull/PeiOcInitLibNull.inf
!endif
  OcFruLib|$(PLATFORM_SI_PACKAGE)/Fru/AdlCpu/Overclocking/LibraryPrivate/BaseOcFruLib/BaseOcFruLib.inf
  PeiCpuInitFruLib|$(PLATFORM_SI_PACKAGE)/Fru/AdlCpu/LibraryPrivate/PeiCpuInitFruLib/PeiCpuInitFruLib.inf

#
# Intel VR IpBlock and Fru Library
#
  PeiVrPolicyLib|$(PLATFORM_SI_PACKAGE)/IpBlock/VoltageRegulator/LibraryPrivate/PeiVrPolicyLib/PeiVrPolicyLib.inf
  PeiVrDomainLib|$(PLATFORM_SI_PACKAGE)/IpBlock/VoltageRegulator/Library/PeiVrDomainLib/PeiVrDomainLib.inf
  PeiVrLib|$(PLATFORM_SI_PACKAGE)/IpBlock/VoltageRegulator/LibraryPrivate/PeiVrLib/PeiVrLib.inf
  PeiVrFruLib|$(PLATFORM_SI_PACKAGE)/Fru/AdlCpu/VoltageRegulator/LibraryPrivate/PeiVrFruLib/PeiVrFruLib.inf

!if gSiPkgTokenSpaceGuid.PcdBiosGuardEnable == TRUE
  BiosGuardLib|$(PLATFORM_SI_PACKAGE)/IpBlock/BiosGuard/LibraryPrivate/PeiBiosGuardLib/PeiBiosGuardLib.inf
!else
  BiosGuardLib|$(PLATFORM_SI_PACKAGE)/IpBlock/BiosGuard/LibraryPrivate/PeiBiosGuardLibNull/PeiBiosGuardLibNull.inf
!endif
!if gSiPkgTokenSpaceGuid.PcdSmbiosEnable == TRUE
  SmbiosCpuLib|$(PLATFORM_SI_PACKAGE)/Cpu/LibraryPrivate/PeiSmbiosCpuLib/PeiSmbiosCpuLib.inf
!else
  SmbiosCpuLib|$(PLATFORM_SI_PACKAGE)/Cpu/LibraryPrivate/PeiSmbiosCpuLibNull/PeiSmbiosCpuLibNull.inf
!endif

#
# SA
#
!if gSiPkgTokenSpaceGuid.PcdHgEnable == TRUE
  PeiHybridGraphicsInitLib|$(PLATFORM_SI_PACKAGE)/IpBlock/HybridGraphics/LibraryPrivate/PeiHybridGraphicsInitLib/PeiHybridGraphicsInitLib.inf
  PeiHybridGraphicsPolicyLib|$(PLATFORM_SI_PACKAGE)/IpBlock/HybridGraphics/LibraryPrivate/PeiHybridGraphicsPolicyLib/PeiHybridGraphicsPolicyLib.inf
!else
  PeiHybridGraphicsInitLib|$(PLATFORM_SI_PACKAGE)/IpBlock/HybridGraphics/LibraryPrivate/PeiHybridGraphicsInitLibNull/PeiHybridGraphicsInitLibNull.inf
  PeiHybridGraphicsPolicyLib|$(PLATFORM_SI_PACKAGE)/IpBlock/HybridGraphics/LibraryPrivate/PeiHybridGraphicsPolicyLibNull/PeiHybridGraphicsPolicyLibNull.inf
!endif
  PeiCpuPcieVgaInitLib|$(PLATFORM_SI_PACKAGE)/IpBlock/CpuPcieRp/LibraryPrivate/PeiCpuPcieVgaInitLib/PeiCpuPcieVgaInitLib.inf
  SaInitLib|$(PLATFORM_SI_PACKAGE)/SystemAgent/LibraryPrivate/PeiSaInitLib/PeiSaInitLibFsp.inf
  PeiSaInitFruLib|$(PLATFORM_SI_PACKAGE)/Fru/AdlCpu/LibraryPrivate/PeiSaInitFruLib/PeiSaInitFruLib.inf

#
# Host Bridge
#
  PeiHostBridgeInitLib|$(PLATFORM_SI_PACKAGE)/IpBlock/HostBridge/LibraryPrivate/PeiHostBridgeInitLib/PeiHostBridgeInitLib.inf
  PeiHostBridgePolicyLib|$(PLATFORM_SI_PACKAGE)/IpBlock/HostBridge/LibraryPrivate/PeiHostBridgePolicyLib/PeiHostBridgePolicyLibVer1.inf
  PeiHostBridgeInitFruLib|$(PLATFORM_SI_PACKAGE)/Fru/AdlCpu/LibraryPrivate/PeiHostBridgeInitFruLib/PeiHostBridgeInitFruLib.inf

!if gSiPkgTokenSpaceGuid.PcdSaDmiEnable == TRUE
  PeiCpuDmiInitLib|$(PLATFORM_SI_PACKAGE)/IpBlock/CpuDmi/LibraryPrivate/PeiCpuDmiInitLibVer2/PeiCpuDmiInitLib.inf
  DmiDekelInitLib|$(PLATFORM_SI_PACKAGE)/Fru/AdlCpu/CpuDmi/LibraryPrivate/PeiCpuDmiDekelInitLib/PeiCpuDmiDekelInitLib.inf
!endif

#
#  Telemetry
#
  TelemetryLib|$(PLATFORM_SI_PACKAGE)/IpBlock/Telemetry/Library/PeiDxeTelemetryLib/PeiDxeTelemetryLib.inf
  TelemetryPrivateLib|$(PLATFORM_SI_PACKAGE)/IpBlock/Telemetry/LibraryPrivate/PeiDxeTelemetryPrivateLib/PeiDxeTelemetryPrivateLib.inf
  PeiTelemetryPolicyLib|$(PLATFORM_SI_PACKAGE)/IpBlock/Telemetry/LibraryPrivate/PeiTelemetryPolicyLib/PeiTelemetryPolicyLib.inf
  PeiDxeTelemetryFruLib|$(PLATFORM_SI_PACKAGE)/Fru/AdlCpu/Telemetry/LibraryPrivate/PeiDxeTelemetryFruLib/PeiDxeTelemetryFruLib.inf
#
# IPU
#
IpuLib|$(PLATFORM_SI_PACKAGE)/IpBlock/Ipu/Library/PeiDxeSmmIpuLib/IpuLib.inf
BaseIpuFruLib|$(PLATFORM_SI_PACKAGE)/Fru/AdlCpu/Ipu/LibraryPrivate/BaseIpuFruLib/BaseIpuFruLib.inf

!if gSiPkgTokenSpaceGuid.PcdIpuEnable == TRUE
  PeiIpuPolicyPrivateLib|$(PLATFORM_SI_PACKAGE)/IpBlock/Ipu/LibraryPrivate/PeiIpuPolicyPrivateLib/PeiIpuPolicyPrivateLib.inf
  IpuInitLib|$(PLATFORM_SI_PACKAGE)/IpBlock/Ipu/LibraryPrivate/PeiIpuInitPrivateLib/PeiIpuInitLib.inf
!else
  PeiIpuPolicyPrivateLib|$(PLATFORM_SI_PACKAGE)/IpBlock/Ipu/LibraryPrivate/PeiIpuPolicyPrivateLibNull/PeiIpuPolicyPrivateLibNull.inf
  IpuInitLib|$(PLATFORM_SI_PACKAGE)/IpBlock/Ipu/LibraryPrivate/PeiIpuInitPrivateLibNull/PeiIpuInitLibNull.inf
!endif

#
# Graphics
#

  PeiGraphicsPolicyLib|$(PLATFORM_SI_PACKAGE)/IpBlock/Graphics/LibraryPrivate/PeiGraphicsPolicyLibGen12/PeiGraphicsPolicyLib.inf

!if gSiPkgTokenSpaceGuid.PcdIgdEnable == TRUE
  GraphicsInitLib|$(PLATFORM_SI_PACKAGE)/IpBlock/Graphics/LibraryPrivate/PeiGraphicsInitLibGen12/PeiGraphicsInitLib.inf
  !if gSiPkgTokenSpaceGuid.PcdAdlLpSupport == TRUE
    DisplayInitLib|$(PLATFORM_SI_PACKAGE)/IpBlock/Graphics/LibraryPrivate/PeiDisplayInitLibGen13/PeiDisplayInitLib.inf
  !else
    DisplayInitLib|$(PLATFORM_SI_PACKAGE)/IpBlock/Graphics/LibraryPrivate/PeiDisplayInitLibGen12/PeiDisplayInitLib.inf
  !endif
  DisplayInitFruLib|$(PLATFORM_SI_PACKAGE)/Fru/AdlCpu/Graphics/LibraryPrivate/PeiDisplayInitFruLib/PeiDisplayInitFruLib.inf
!else
  GraphicsInitLib|$(PLATFORM_SI_PACKAGE)/IpBlock/Graphics/LibraryPrivate/PeiGraphicsDisableInitLib/PeiGraphicsDisableInitLib.inf
  DisplayInitLib|$(PLATFORM_SI_PACKAGE)/IpBlock/Graphics/LibraryPrivate/PeiDisplayInitLibNull/PeiDisplayInitLibNull.inf
!endif

!if gSiPkgTokenSpaceGuid.PcdPeiDisplayEnable == TRUE
  DisplayLib|$(PLATFORM_SI_PACKAGE)/IpBlock/Graphics/LibraryPrivate/PeiDisplayLib/PeiDisplayLib.inf
!else
  DisplayLib|$(PLATFORM_SI_PACKAGE)/IpBlock/Graphics/LibraryPrivate/PeiDisplayLibNull/PeiDisplayLibNull.inf
!endif

#
# GNA
#
GnaInfoLib|$(PLATFORM_SI_PACKAGE)/IpBlock/Gna/Library/PeiDxeSmmGnaInfoLib/PeiDxeSmmGnaInfoLib.inf
!if gSiPkgTokenSpaceGuid.PcdGnaEnable == TRUE
  GnaInitLib|$(PLATFORM_SI_PACKAGE)/IpBlock/Gna/LibraryPrivate/PeiGnaInitLib/PeiGnaInitLib.inf
  PeiGnaPolicyLib|$(PLATFORM_SI_PACKAGE)/IpBlock/Gna/LibraryPrivate/PeiGnaPolicyLib/PeiGnaPolicyLib.inf
!else
  GnaInitLib|$(PLATFORM_SI_PACKAGE)/IpBlock/Gna/LibraryPrivate/PeiGnaInitLibNull/PeiGnaInitLibNull.inf
  PeiGnaPolicyLib|$(PLATFORM_SI_PACKAGE)/IpBlock/Gna/LibraryPrivate/PeiGnaPolicyLibNull/PeiGnaPolicyLibNull.inf
!endif

#
# VT-D
#
VtdInfoLib|$(PLATFORM_SI_PACKAGE)/IpBlock/Vtd/Library/PeiDxeSmmVtdInfoLib/PeiDxeSmmVtdInfoLibVer1.inf
!if gSiPkgTokenSpaceGuid.PcdVtdEnable == TRUE
  PeiVtdPolicyLib|$(PLATFORM_SI_PACKAGE)/IpBlock/Vtd/LibraryPrivate/PeiVtdPolicyLib/PeiVtdPolicyLib.inf
  VtdInitLib|$(PLATFORM_SI_PACKAGE)/IpBlock/Vtd/LibraryPrivate/PeiVtdInitLib/PeiVtdInitLibVer1.inf
  PeiVtdInitFruLib|$(PLATFORM_SI_PACKAGE)/Fru/AdlCpu/Vtd/LibraryPrivate/PeiVtdInitFruLib/PeiVtdInitFruLib.inf
!else
  PeiVtdPolicyLib|$(PLATFORM_SI_PACKAGE)/IpBlock/Vtd/LibraryPrivate/PeiVtdPolicyLibNull/PeiVtdPolicyLibNull.inf
  VtdInitLib|$(PLATFORM_SI_PACKAGE)/IpBlock/Vtd/LibraryPrivate/PeiVtdInitLibNull/PeiVtdInitLibNull.inf
!endif
  PeiGetVtdPmrAlignmentLib|IntelSiliconPkg/Library/PeiGetVtdPmrAlignmentLib/PeiGetVtdPmrAlignmentLib.inf
#
# PSMI
#
!if gSiPkgTokenSpaceGuid.PcdPsmiEnable == TRUE
  PeiPsmiInitLib|$(PLATFORM_SI_PACKAGE)/IpBlock/Psmi/LibraryPrivate/PeiPsmiInitLib/PeiPsmiInitLibVer2.inf
  PeiPsmiInitFruLib|$(PLATFORM_SI_PACKAGE)/Fru/AdlCpu/LibraryPrivate/PeiPsmiInitFruLib/PeiPsmiInitFruLib.inf
!else
  PeiPsmiInitLib|$(PLATFORM_SI_PACKAGE)/IpBlock/Psmi/LibraryPrivate/PeiPsmiInitLibNull/PeiPsmiInitLibNull.inf
  PeiPsmiInitFruLib|$(PLATFORM_SI_PACKAGE)/Fru/AdlCpu/LibraryPrivate/PeiPsmiInitFruLibNull/PeiPsmiInitFruLibNull.inf
!endif

  PeiPcieRpPolicyLib|$(PLATFORM_SI_PACKAGE)/IpBlock/PcieRp/LibraryPrivate/PeiPcieRpPolicyLib/PeiPcieRpPolicyLib.inf

  MemoryInitLib|$(PLATFORM_SI_PACKAGE)/IpBlock/MemoryInit/Adl/LibraryPrivate/PeiMemoryInitLib/PeiMemoryInitLibFsp.inf

  MemoryAddressEncodeLib|$(PLATFORM_SI_PACKAGE)/IpBlock/MemoryInit/Adl/Library/PeiDxeSmmMemAddrEncodeLib/PeiDxeSmmMemAddrEncodeLib.inf

#
# MkTme IpBlock
#
  TmeLib|$(PLATFORM_SI_PACKAGE)/IpBlock/Tme/LibraryPrivate/MkTmeLib/MkTmeLib.inf

  PeiCpuTraceHubLib|$(PLATFORM_SI_PACKAGE)/IpBlock/TraceHub/LibraryPrivate/PeiCpuTraceHubLib/PeiCpuTraceHubLib.inf
  PeiDpInPolicyLib|$(PLATFORM_SI_PACKAGE)/IpBlock/Tcss/LibraryPrivate/PeiDpInPolicyLibNull/PeiDpInPolicyLibNull.inf
  TcssInitFruLib|$(PLATFORM_SI_PACKAGE)/Fru/AdlCpu/Tcss/LibraryPrivate/PeiTcssInitFruLib/PeiTcssInitFruLib.inf
!if gSiPkgTokenSpaceGuid.PcdITbtEnable == TRUE
  PeiTcssPolicyLib|$(PLATFORM_SI_PACKAGE)/IpBlock/Tcss/LibraryPrivate/PeiTcssPolicyLib/PeiTcssPolicyLib.inf
  TcssInfoLib|$(PLATFORM_SI_PACKAGE)/IpBlock/Tcss/Library/PeiDxeSmmTcssInfoLib/TcssInfoLib.inf
  PeiTcssInitLib|$(PLATFORM_SI_PACKAGE)/IpBlock/Tcss/LibraryPrivate/PeiTcssInitLib/PeiTcssInitLib.inf
  ItbtPcieRpInitLib|$(PLATFORM_SI_PACKAGE)/IpBlock/CpuPcieRp/LibraryPrivate/PeiItbtPcieRpInitLib/ItbtPcieRpInitLib.inf
  TcssPmcLib|$(PLATFORM_SI_PACKAGE)/IpBlock/Tcss/LibraryPrivate/PeiDxeSmmTcssPmcLib/PeiDxeSmmTcssPmcLib.inf
!else
  PeiTcssPolicyLib|$(PLATFORM_SI_PACKAGE)/IpBlock/Tcss/LibraryPrivate/PeiTcssPolicyLibNull/PeiTcssPolicyLibNull.inf
  TcssInfoLib|$(PLATFORM_SI_PACKAGE)/IpBlock/Tcss/Library/PeiDxeSmmTcssInfoLibNull/TcssInfoLibNull.inf
  PeiTcssInitLib|$(PLATFORM_SI_PACKAGE)/IpBlock/Tcss/LibraryPrivate/PeiTcssInitLibNull/PeiTcssInitLibNull.inf
  ItbtPcieRpInitLib|$(PLATFORM_SI_PACKAGE)/IpBlock/CpuPcieRp/LibraryPrivate/PeiItbtPcieRpInitLibNull/ItbtPcieRpInitLibNull.inf
  TcssPmcLib|$(PLATFORM_SI_PACKAGE)/IpBlock/Tcss/LibraryPrivate/PeiTcssPmcLibNull/PeiTcssPmcLibNull.inf
!endif

#
# Cpu
#
  CpuPolicyLib|$(PLATFORM_SI_PACKAGE)/Cpu/Library/PeiCpuPolicyLib/PeiCpuPolicyLib.inf
#
# System Agent
#
  PeiSaPolicyLib|$(PLATFORM_SI_PACKAGE)/SystemAgent/Library/PeiSaPolicyLib/PeiSaPolicyLibFsp.inf
  PeiMemPolicyLib|$(PLATFORM_SI_PACKAGE)/IpBlock/MemoryInit/Adl/Library/PeiMemPolicyLib/PeiMemPolicyLibFsp.inf

#
# TBT
#
  PeiITbtPolicyLib|$(PLATFORM_SI_PACKAGE)/IpBlock/Tbt/Library/PeiITbtPolicyLib/PeiITbtPolicyLib.inf

  TbtCommonLib|$(PLATFORM_SI_PACKAGE)/IpBlock/Tbt/Library/PeiDxeSmmTbtCommonLib/TbtCommonLib.inf
  DxeTbtDisBmeLib|$(PLATFORM_SI_PACKAGE)/IpBlock/Tbt/Library/DxeTbtDisBmeLib/DxeTbtDisBmeLib.inf
!if gSiPkgTokenSpaceGuid.PcdITbtEnable == TRUE
  PeiTbtTaskDispatchLib|$(PLATFORM_SI_PACKAGE)/IpBlock/Tbt/Library/PeiTbtTaskDispatchLib/PeiTbtTaskDispatchLib.inf
  PeiITbtInitLib|$(PLATFORM_SI_PACKAGE)/IpBlock/Tbt/LibraryPrivate/PeiITbtInitLib/PeiITbtInitLib.inf
!else
  PeiTbtTaskDispatchLib|$(PLATFORM_SI_PACKAGE)/IpBlock/Tbt/Library/PeiTbtTaskDispatchLib/PeiTbtTaskDispatchLib.inf
  PeiITbtInitLib|$(PLATFORM_SI_PACKAGE)/IpBlock/Tbt/LibraryPrivate/PeiITbtInitLibNull/PeiITbtInitLibNull.inf
!endif


[LibraryClasses]
  !include $(PLATFORM_SI_PACKAGE)/Fru/AdlPch/CommonLib.dsc

  #
  # Entry point
  #
  PeiCoreEntryPoint|MdePkg/Library/PeiCoreEntryPoint/PeiCoreEntryPoint.inf
  PeimEntryPoint|MdePkg/Library/PeimEntryPoint/PeimEntryPoint.inf
  #
  # Basic
  #
  DebugPrintErrorLevelLib|$(C1S_FSP_PATH)/Library/DebugPrintErrorLevelLib/FspDebugPrintErrorLevelLib.inf
#[-start-190610-IB16990036-add]#
  BaseLib|MdePkg/Library/BaseLib/BaseLib.inf{
    <SOURCE_OVERRIDE_PATH>
      MdePkg/Override/Library/BaseLib
  }
#[-end-190610-IB16990036-add]#
  IoLib|MdePkg/Library/BaseIoLibIntrinsic/BaseIoLibIntrinsic.inf
  PciCf8Lib|MdePkg/Library/BasePciCf8Lib/BasePciCf8Lib.inf
  PciExpressLib|$(PLATFORM_SI_PACKAGE)/Library/BasePciExpressMultiSegLib/BasePciExpressMultiSegLib.inf
  PciSegmentLib|$(PLATFORM_SI_PACKAGE)/Library/BasePciSegmentMultiSegLibPci/BasePciSegmentMultiSegLibPci.inf
  BaseMemoryLib|MdePkg/Library/BaseMemoryLibRepStr/BaseMemoryLibRepStr.inf
  PrintLib|MdePkg/Library/BasePrintLib/BasePrintLib.inf
  PeCoffGetEntryPointLib|MdePkg/Library/BasePeCoffGetEntryPointLib/BasePeCoffGetEntryPointLib.inf
  CacheMaintenanceLib|MdePkg/Library/BaseCacheMaintenanceLib/BaseCacheMaintenanceLib.inf
  PeCoffLib|MdePkg/Library/BasePeCoffLib/BasePeCoffLib.inf
  #[-start-190625-IB10180001-modify]#
  # PeCoffExtraActionLib|MdePkg/Library/BasePeCoffExtraActionLibNull/BasePeCoffExtraActionLibNull.inf
  PeCoffExtraActionLib|InsydeModulePkg/H2ODebug/Library/PeCoffExtraActionLib/PeCoffExtraActionLib.inf
  #[-start-190625-IB10180001-modify]#
  UefiDecompressLib|MdePkg/Library/BaseUefiDecompressLib/BaseUefiDecompressLib.inf
  SynchronizationLib|MdePkg/Library/BaseSynchronizationLib/BaseSynchronizationLib.inf
  CpuLib|MdePkg/Library/BaseCpuLib/BaseCpuLib.inf
  PreSiliconEnvDetectLib|$(PLATFORM_SI_PACKAGE)/Library/BasePreSiliconEnvDetectLib/BasePreSiliconEnvDetectLib.inf
  SystemTimeLib|$(PLATFORM_SI_PACKAGE)/Library/BaseSystemTimeLib/BaseSystemTimeLib.inf
  S3PciSegmentLib|MdePkg/Library/BaseS3PciSegmentLib/BaseS3PciSegmentLib.inf
  SortLib|MdeModulePkg/Library/BaseSortLib/BaseSortLib.inf

  #
  # Generic Modules
  #
  OemHookStatusCodeLib|MdeModulePkg/Library/OemHookStatusCodeLibNull/OemHookStatusCodeLibNull.inf

  #
  # Misc
  #
  MtrrLib|UefiCpuPkg/Library/MtrrLib/MtrrLib.inf
  LocalApicLib|UefiCpuPkg/Library/BaseXApicX2ApicLib/BaseXApicX2ApicLib.inf
  ### Base on debug/release mode, choose one of the DebugLib
  ####  DebugLib|MdePkg/Library/BaseDebugLibNull/BaseDebugLibNull.inf
  DebugLib|MdePkg/Library/BaseDebugLibSerialPort/BaseDebugLibSerialPort.inf
  SerialIoUartDebugPropertyPcdLib|$(PLATFORM_SI_PACKAGE)/IpBlock/SerialIo/Uart/Library/SerialIoUartDebugPropertyPcdLib/SerialIoUartDebugPropertyPcdLib.inf
  FspSerialIoUartDebugHelperLib|$(C1S_FSP_PATH)/Library/FspSerialIoUartDebugHelperLib/FspSerialIoUartDebugHelperLib.inf
  UefiCpuLib|UefiCpuPkg/Library/BaseUefiCpuLib/BaseUefiCpuLib.inf
  CpuExceptionHandlerLib|UefiCpuPkg/Library/CpuExceptionHandlerLib/SecPeiCpuExceptionHandlerLib.inf
  VmgExitLib|UefiCpuPkg/Library/VmgExitLibNull/VmgExitLibNull.inf
  MpInitLib|UefiCpuPkg/Library/MpInitLib/PeiMpInitLib.inf
  MicrocodeLib|UefiCpuPkg/Library/MicrocodeLib/MicrocodeLib.inf
  RngLib|MdePkg/Library/BaseRngLib/BaseRngLib.inf

!if gAlderLakeFspPkgTokenSpaceGuid.PcdMonoStatusCode == TRUE
  MonoStatusCodeLib|$(C1S_FSP_PATH)/Library/MonoStatusCode/MonoStatusCode.inf
!else
  MonoStatusCodeLib|$(C1S_FSP_PATH)/Library/MonoStatusCodeNull/MonoStatusCodeNull.inf
!endif

  FspInfoLib|$(C1S_FSP_PATH)/Library/FspInfoLib/FspInfoLib.inf
  MmPciLib|$(PLATFORM_SI_PACKAGE)/Library/PeiDxeSmmMmPciLib/PeiDxeSmmMmPciLib.inf
  PeiCpuPsfInitFruLib|$(PLATFORM_SI_PACKAGE)/Fru/AdlCpu/LibraryPrivate/PeiCpuPsfInitFruLib/PeiCpuPsfInitFruLib.inf
  TccLib|$(PLATFORM_SI_PACKAGE)/IpBlock/Tcc/Library/PeiDxeSmmTccLib/PeiDxeSmmTccLib.inf

  #
  #  VMD IpBlock
  #
  VmdInfoLib|$(PLATFORM_SI_PACKAGE)/IpBlock/Vmd/Library/VmdInfoLib/VmdInfoLib.inf
!if gSiPkgTokenSpaceGuid.PcdVmdEnable == TRUE
  PeiVmdInitLib|$(PLATFORM_SI_PACKAGE)/IpBlock/Vmd/LibraryPrivate/PeiVmdInitLib/PeiVmdInitLib.inf
  PeiVmdPolicyLib|$(PLATFORM_SI_PACKAGE)/IpBlock/Vmd/LibraryPrivate/PeiVmdPolicyLib/PeiVmdPolicyLib.inf
  PeiVmdInitFruLib|$(PLATFORM_SI_PACKAGE)/Fru/AdlCpu/Vmd/LibraryPrivate/PeiVmdInitFruLib/PeiVmdInitFruLib.inf
!else
  PeiVmdInitLib|$(PLATFORM_SI_PACKAGE)/IpBlock/Vmd/LibraryPrivate/PeiVmdInitLibNull/PeiVmdInitLibNull.inf
  PeiVmdPolicyLib|$(PLATFORM_SI_PACKAGE)/IpBlock/Vmd/LibraryPrivate/PeiVmdPolicyLibNull/PeiVmdPolicyLibNull.inf
!endif

  #
  #  Tme IpBlock
  #
  TmeInfoLib|$(PLATFORM_SI_PACKAGE)/IpBlock/Tme/Library/TmeInfoLib.inf

  #
  # Silicon library
  #
  ConfigBlockLib|IntelSiliconPkg/Library/BaseConfigBlockLib/BaseConfigBlockLib.inf
  SerialPortLib|MdeModulePkg/Library/BaseSerialPortLib16550/BaseSerialPortLib16550.inf
  PlatformHookLib|MdeModulePkg/Library/BasePlatformHookLibNull/BasePlatformHookLibNull.inf
  FspErrorInfoLib|$(PLATFORM_SI_PACKAGE)/Library/FspErrorInfoLib/PeiDxeFspErrorInfoLib.inf

  #
  # Pch
  #
  GpioHelpersLib|$(PLATFORM_SI_PACKAGE)/IpBlock/Gpio/LibraryPrivate/BaseGpioHelpersLibNull/BaseGpioHelpersLibNull.inf
  SerialIoAccessLib|$(PLATFORM_SI_PACKAGE)/IpBlock/SerialIo/Library/PeiDxeSmmSerialIoAccessLib/PeiDxeSmmSerialIoAccessLib.inf
  SataSocLib|$(PLATFORM_SI_PACKAGE)/Fru/AdlPch/Library/SataSocLib/SataSocLib.inf
  !if gSiPkgTokenSpaceGuid.PcdAdlLpSupport == TRUE
    SerialIoPrivateLib|$(PLATFORM_SI_PACKAGE)/IpBlock/SerialIo/LibraryPrivate/PeiDxeSmmSerialIoPrivateLib/PeiDxeSmmSerialIoPrivateLibVer2.inf
  !else
    SerialIoPrivateLib|$(PLATFORM_SI_PACKAGE)/IpBlock/SerialIo/LibraryPrivate/PeiDxeSmmSerialIoPrivateLib/PeiDxeSmmSerialIoPrivateLibVer4.inf
  !endif
  PchCycleDecodingLib|$(PLATFORM_SI_PACKAGE)/Pch/Library/PeiDxeSmmPchCycleDecodingLib/PeiDxeSmmPchCycleDecodingLib.inf
  SerialIoUartLib|$(PLATFORM_SI_PACKAGE)/IpBlock/SerialIo/Uart/Library/PeiDxeSmmSerialIoUartLib/PeiDxeSmmSerialIoUartLib.inf
  PchPciBdfLib|$(PLATFORM_SI_PACKAGE)/Pch/Library/BasePchPciBdfLib/BasePchPciBdfLib.inf
  PeiSpiExtendedDecodeLib|$(PLATFORM_SI_PACKAGE)/IpBlock/Spi/LibraryPrivate/PeiSpiExtendedDecodeLib/PeiSpiExtendedDecodeLib.inf

  #
  # CPU
  #
!if gSiPkgTokenSpaceGuid.PcdBtgTxtLegacyPkgEnable == TRUE
  BootGuardLib|$(PLATFORM_SI_PACKAGE)/Cpu/Library/PeiDxeSmmBootGuardLib/PeiDxeSmmBootGuardLib.inf
!else
  BootGuardLib|$(PLATFORM_SI_PACKAGE)/IpBlock/BootGuard/LibraryPrivate/PeiDxeSmmBootGuardLib/PeiDxeSmmBootGuardLib.inf
!endif

  CpuPlatformLib|$(PLATFORM_SI_PACKAGE)/Cpu/Library/PeiDxeSmmCpuPlatformLib/PeiDxeSmmCpuPlatformLib.inf
  CpuMailboxLib|$(PLATFORM_SI_PACKAGE)/Library/PeiDxeSmmCpuMailboxLib/PeiDxeSmmCpuMailboxLib.inf
  MsrFruLib|$(PLATFORM_SI_PACKAGE)/Fru/AdlCpu/LibraryPrivate/BaseMsrFruLib/BaseMsrFruLib.inf
  CpuInfoFruLib|$(PLATFORM_SI_PACKAGE)/Fru/AdlCpu/LibraryPrivate/BaseCpuInfoFruLib/BaseCpuInfoFruLib.inf
  CpuCommonLib|$(PLATFORM_SI_PACKAGE)/Cpu/LibraryPrivate/PeiDxeSmmCpuCommonLib/PeiDxeSmmCpuCommonLib.inf

  #
  # SA
  #
  SaPlatformLib|$(PLATFORM_SI_PACKAGE)/SystemAgent/Library/PeiDxeSmmSaPlatformLib/PeiDxeSmmSaPlatformLib.inf
  GraphicsInfoLib|$(PLATFORM_SI_PACKAGE)/IpBlock/Graphics/Library/PeiDxeSmmGraphicsInfoLib/GraphicsInfoLibVer1.inf
  GraphicsInfoFruLib|$(PLATFORM_SI_PACKAGE)/Fru/AdlCpu/Graphics/Library/PeiDxeSmmGraphicsInfoFruLib/GraphicsInfoFruLib.inf
  CpuPcieInfoFruLib|$(PLATFORM_SI_PACKAGE)/Fru/AdlCpu/CpuPcieRp/Library/PeiDxeSmmCpuPcieInfoFruLib/PeiDxeSmmCpuPcieInfoFruLib.inf
  CpuDmiInfoFruLib|$(PLATFORM_SI_PACKAGE)/Fru/AdlCpu/CpuDmi/Library/PeiDxeSmmCpuDmiInfoFruLib/PeiDxeSmmCpuDmiInfoFruLib.inf
  HybridGraphicsInfoFruLib|$(PLATFORM_SI_PACKAGE)/Fru/AdlCpu/HybridGraphics/Library/PeiDxeSmmHybridGraphicsInfoFruLib/PeiDxeSmmHybridGraphicsInfoFruLib.inf
  CpuPcieRpLib|$(PLATFORM_SI_PACKAGE)/IpBlock/CpuPcieRp/Library/PeiDxeSmmCpuPcieRpLib/PeiDxeSmmCpuPcieRpLib.inf
  CpuRegbarAccessLib|$(PLATFORM_SI_PACKAGE)/IpBlock/P2sb/Library/PeiDxeSmmCpuRegbarAccessLib/PeiDxeSmmCpuRegbarAccessLib.inf
  CpuSbiAccessLib|$(PLATFORM_SI_PACKAGE)/IpBlock/P2sb/Library/PeiDxeSmmCpuSbiAccessLib/PeiDxeSmmCpuSbiAccessLib.inf
  CpuPcieInitCommonLib|$(PLATFORM_SI_PACKAGE)/IpBlock/CpuPcieRp/Library/PeiDxeSmmCpuPcieInitCommonLib/PeiDxeSmmCpuPcieInitCommonLib.inf
  HostBridgeInfoFruLib|$(PLATFORM_SI_PACKAGE)/Fru/AdlCpu/HostBridge/Library/PeiDxeSmmHostBridgeInfoFruLib/HostBridgeInfoFruLib.inf
  CpuDmiInfoLib|$(PLATFORM_SI_PACKAGE)/IpBlock/CpuDmi/Library/CpuDmiInfoLib/CpuDmiInfoLib.inf
  BaseTraceHubInfoFruLib|$(PLATFORM_SI_PACKAGE)/Fru/AdlCpu/TraceHub/LibraryPrivate/BaseTraceHubInfoFruLib/BaseTraceHubInfoFruLib.inf

  #
  # Overclocking
  #
!if gSiPkgTokenSpaceGuid.PcdOverclockEnable == TRUE
  OcCommonLib|$(PLATFORM_SI_PACKAGE)/IpBlock/Overclocking/LibraryPrivate/PeiDxeSmmOcCommonLib/PeiDxeSmmOcCommonLib.inf
!else
  OcCommonLib|$(PLATFORM_SI_PACKAGE)/IpBlock/Overclocking/LibraryPrivate/PeiDxeSmmOcCommonLibNull/PeiDxeSmmOcCommonLibNull.inf
!endif

[LibraryClasses.IA32.SEC]
  MmPciLib|$(C1S_FSP_PATH)/Library/MmPciCf8Lib/MmPciCf8Lib.inf
  PciLib|MdePkg/Library/BasePciLibCf8/BasePciLibCf8.inf
  TimerLib|UefiCpuPkg/Library/CpuTimerLib/BaseCpuTimerLib.inf
  SecFspSiliconLib|$(C1S_FSP_PATH)/Library/SecFspSiliconLib/SecFspSiliconLib.inf
  SecGetFsptApiParameterLib|$(PLATFORM_SI_PACKAGE)/Library/SecGetFsptApiParameterLib/SecGetFsptApiParameterLib.inf
#[-start-190625-IB10180002-modify]#
!if $(FSP_DEBUG) == YES
#[-start-190625-IB10180002-modify]#
  SecSerialPortLib|$(C1S_FSP_PATH)/Library/SecSerialPortInitLib/SecSerialPortInitLib.inf
!else
  SecSerialPortLib|$(C1S_FSP_PATH)/Library/SecSerialPortInitLib/SecSerialPortInitLibNull.inf
!endif
  GpioHelpersLib|$(PLATFORM_SI_PACKAGE)/IpBlock/Gpio/LibraryPrivate/BaseGpioHelpersLibNull/BaseGpioHelpersLibNull.inf

[LibraryClasses.IA32.SEC,LibraryClasses.IA32.PEI_CORE]
  ResetSystemLib|$(PLATFORM_SI_PACKAGE)/Pch/Library/BaseResetSystemLib/BaseResetSystemLib.inf
#[-start-190625-IB10180002-modify]#
!if $(FSP_DEBUG) == YES
#[-start-190625-IB10180002-modify]#
  DebugLib|$(C1S_FSP_PATH)/Library/DebugLibDebugPort/Pei/DebugLibDebugPort.inf
!else
  DebugLib|MdePkg/Library/BaseDebugLibNull/BaseDebugLibNull.inf
!endif

[LibraryClasses.IA32.PEIM]
#[-start-190625-IB10180002-modify]#
!if $(FSP_DEBUG) == YES
#[-start-190625-IB10180002-modify]#
  DebugLib|$(C1S_FSP_PATH)/Library/FspDebugLibService/FspDebugLibService.inf
!else
  DebugLib|MdePkg/Library/BaseDebugLibNull/BaseDebugLibNull.inf
!endif
  CpuCacheInfoLib|UefiCpuPkg/Library/CpuCacheInfoLib/PeiCpuCacheInfoLib.inf

[LibraryClasses.IA32.PEIM,LibraryClasses.IA32.PEI_CORE]
  PciLib|MdePkg/Library/BasePciLibPciExpress/BasePciLibPciExpress.inf
  TimerLib|UefiCpuPkg/Library/CpuTimerLib/BaseCpuTimerLib.inf

[LibraryClasses.IA32.PEIM,LibraryClasses.IA32.PEI_CORE,LibraryClasses.IA32.SEC]
  #
  # SEC and PEI phase common
  #
  S3BootScriptLib|MdePkg/Library/BaseS3BootScriptLibNull/BaseS3BootScriptLibNull.inf
  PcdLib|MdePkg/Library/PeiPcdLib/PeiPcdLib.inf
  HobLib|MdePkg/Library/PeiHobLib/PeiHobLib.inf
  FspSwitchStackLib|IntelFsp2Pkg/Library/BaseFspSwitchStackLib/BaseFspSwitchStackLib.inf
  SecCpuLib|$(PLATFORM_SI_PACKAGE)/Cpu/Library/SecCpuLib/SecCpuLibFsp.inf
  SecPchLib|$(PLATFORM_SI_PACKAGE)/Pch/Library/SecPchLib/SecPchLibFsp.inf
  SecHostBridgeLib|$(PLATFORM_SI_PACKAGE)/IpBlock/HostBridge/Library/SecHostBridgeLib/SecHostBridgeLib.inf
  CacheAsRamLib|$(PLATFORM_SI_PACKAGE)/Library/BaseCacheAsRamLib/BaseCacheAsRamLib.inf
  FspCommonLib|IntelFsp2Pkg/Library/BaseFspCommonLib/BaseFspCommonLib.inf
  FspPlatformLib|IntelFsp2Pkg/Library/BaseFspPlatformLib/BaseFspPlatformLib.inf

#[-start-180709-IB15590108-modify]#
!if $(FSP_DEBUG) == YES
  SerialPortLib|$(C1S_FSP_PATH)/Library/SerialPortLib/SerialPortLib.inf
!else
  SerialPortLib|MdePkg/Library/BaseSerialPortLibNull/BaseSerialPortLibNull.inf
!endif
#[-end-180709-IB15590108-modify]#

  MemoryAllocationLib|MdePkg/Library/PeiMemoryAllocationLib/PeiMemoryAllocationLib.inf
  PostCodeLib|$(C1S_FSP_PATH)/Library/BasePostCodeLibPort80TraceHub/BasePostCodeLibPort80TraceHub.inf
  PostCodeToScratchPadLib|$(PLATFORM_SI_PACKAGE)/Library/BasePostCodeToScratchPadLibNull/BasePostCodeToScratchPadLibNull.inf
  ReportStatusCodeLib|MdeModulePkg/Library/PeiReportStatusCodeLib/PeiReportStatusCodeLib.inf
#[-start-200226-IB14630336-modify]#
  PerformanceLib|MdeModulePkg/Library/PeiPerformanceLib/PeiPerformanceLib.inf {
    <SOURCE_OVERRIDE_PATH>
      MdeModulePkg/Override/Library/PeiPerformanceLib
  }
#[-end-200226-IB14630336-modify]#

  #
  # Silicon initialization library
  #

!if gSiPkgTokenSpaceGuid.PcdITbtEnable == TRUE
  ItbtPcieRpLib|$(PLATFORM_SI_PACKAGE)/IpBlock/CpuPcieRp/Library/PeiDxeSmmItbtPcieRpLib/PeiDxeSmmItbtPcieRpLib.inf
  DxeTcssInitLib|$(PLATFORM_SI_PACKAGE)/IpBlock/Tcss/LibraryPrivate/DxeTcssInitLib/DxeTcssInitLib.inf
!else
  ItbtPcieRpLib|$(PLATFORM_SI_PACKAGE)/IpBlock/CpuPcieRp/Library/PeiDxeSmmItbtPcieRpLibNull/PeiDxeSmmItbtPcieRpLibNull.inf
  DxeTcssInitLib|$(PLATFORM_SI_PACKAGE)/IpBlock/Tcss/LibraryPrivate/DxeTcssInitLibNull/DxeTcssInitLibNull.inf
!endif

  #
  # CPU DMI IpBlock
  #
!if gSiPkgTokenSpaceGuid.PcdSaDmiEnable == TRUE
  PeiCpuDmiPolicyLib|$(PLATFORM_SI_PACKAGE)/IpBlock/CpuDmi/LibraryPrivate/PeiCpuDmiPolicyLib/PeiCpuDmiPolicyLib.inf
!endif

  #
  #  CPU PCIe IpBlock
  #
!if gSiPkgTokenSpaceGuid.PcdCpuPcieEnable == TRUE
  PeiCpuPciePreMemRpInitLib|$(PLATFORM_SI_PACKAGE)/IpBlock/CpuPcieRp/LibraryPrivate/PeiCpuPcieRpInitLib/PeiCpuPciePreMemRpInitLibVer2.inf
  PeiCpuPcieVgaInitLib|$(PLATFORM_SI_PACKAGE)/IpBlock/CpuPcieRp/LibraryPrivate/PeiCpuPcieVgaInitLib/PeiCpuPcieVgaInitLib.inf
  PeiCpuPcieRpInitLib|$(PLATFORM_SI_PACKAGE)/IpBlock/CpuPcieRp/LibraryPrivate/PeiCpuPcieRpInitLib/PeiCpuPcieRpInitLibVer2.inf
  PeiCpuPciePolicyLib|$(PLATFORM_SI_PACKAGE)/IpBlock/CpuPcieRp/LibraryPrivate/PeiCpuPciePolicyLib/PeiCpuPciePolicyLib.inf
  PeiCpuPcieSip16InitLib|$(PLATFORM_SI_PACKAGE)/IpBlock/CpuPcieRp/LibraryPrivate/PeiCpuPcieSip16InitLib/PeiCpuPcieSip16InitLib.inf
  PeiCpuPcieSip17InitLib|$(PLATFORM_SI_PACKAGE)/IpBlock/CpuPcieRp/LibraryPrivate/PeiCpuPcieSip17InitLib/PeiCpuPcieSip17InitLib.inf
  DekelInitLib|$(PLATFORM_SI_PACKAGE)/Fru/AdlCpu/CpuPcieRp/LibraryPrivate/PeiCpuPcieDekelInitLib/PeiCpuPcieDekelInitLib.inf
  HsPhyInfoLib|$(PLATFORM_SI_PACKAGE)/Fru/AdlCpu/CpuPcieRp/LibraryPrivate/PeiCpuPcieHsPhyInfoLib/PeiCpuPcieHsPhyInfoLib.inf
!else
  PeiCpuPciePreMemRpInitLib|$(PLATFORM_SI_PACKAGE)/IpBlock/CpuPcieRp/LibraryPrivate/PeiCpuPcieRpInitLibNull/PeiCpuPciePreMemRpInitLibNull.inf
  PeiCpuPcieVgaInitLib|$(PLATFORM_SI_PACKAGE)/IpBlock/CpuPcieRp/LibraryPrivate/PeiCpuPcieVgaInitLibNull/PeiCpuPcieVgaInitLibNull.inf
  PeiCpuPcieRpInitLib|$(PLATFORM_SI_PACKAGE)/IpBlock/CpuPcieRp/LibraryPrivate/PeiCpuPcieRpInitLibNull/PeiCpuPcieRpInitLibNull.inf
  PeiCpuPciePolicyLib|$(PLATFORM_SI_PACKAGE)/IpBlock/CpuPcieRp/LibraryPrivate/PeiCpuPciePolicyLibNull/PeiCpuPciePolicyLibNull.inf
  DekelInitLib|$(PLATFORM_SI_PACKAGE)/IpBlock/CpuPcieRp/LibraryPrivate/PeiCpuPcieDekelInitLibNull/PeiCpuPcieDekelInitLibNull.inf
  HsPhyInfoLib|$(PLATFORM_SI_PACKAGE)/IpBlock/CpuPcieRp/LibraryPrivate/PeiCpuPcieHsPhyInfoLibNull/PeiCpuPcieHsPhyInfoLibNull.inf
!endif
  SaPcieInitLib|$(PLATFORM_SI_PACKAGE)/IpBlock/CpuPcieRp/LibraryPrivate/LegacyGen3/PeiSaPcieInitLibNull/PeiSaPcieInitLibNull.inf
  SaDmiInitLib|$(PLATFORM_SI_PACKAGE)/IpBlock/CpuPcieRp/LibraryPrivate/LegacyGen3/PeiSaDmiInitLibNull/PeiSaDmiInitLibNull.inf
  PeiSaPciePolicyLib|$(PLATFORM_SI_PACKAGE)/IpBlock/CpuPcieRp/LibraryPrivate/LegacyGen3/PeiSaPciePolicyLibNull/PeiSaPciePolicyLibNull.inf

  TxtLib|$(PLATFORM_SI_PACKAGE)/Cpu/Library/PeiDxeTxtLib/PeiDxeTxtLib.inf

[LibraryClasses.X64]
  #
  # DXE phase common
  #
  HobLib|MdePkg/Library/DxeHobLib/DxeHobLib.inf
#[-start-181105-IB0672-add]#
  PcdLib|MdePkg/Library/DxePcdLib/DxePcdLib.inf {
    <SOURCE_OVERRIDE_PATH>
    MdePkg/Override/Library/DxePcdLib
  }
#[-end-181105-IB0672-add]#
  PciLib|MdePkg/Library/BasePciLibPciExpress/BasePciLibPciExpress.inf
  TimerLib|UefiCpuPkg/Library/CpuTimerLib/BaseCpuTimerLib.inf
  DebugPrintErrorLevelLib|MdePkg/Library/BaseDebugPrintErrorLevelLib/BaseDebugPrintErrorLevelLib.inf
  DxeServicesTableLib|MdePkg/Library/DxeServicesTableLib/DxeServicesTableLib.inf
  UefiDriverEntryPoint|MdePkg/Library/UefiDriverEntryPoint/UefiDriverEntryPoint.inf
  MemoryAllocationLib|MdePkg/Library/UefiMemoryAllocationLib/UefiMemoryAllocationLib.inf
  DevicePathLib|MdePkg/Library/UefiDevicePathLib/UefiDevicePathLib.inf
  LockBoxLib|MdeModulePkg/Library/SmmLockBoxLib/SmmLockBoxDxeLib.inf
#[-start-200226-IB14630336-modify]#
!if gAlderLakeFspPkgTokenSpaceGuid.PcdFspPerformanceEnable == TRUE
  PerformanceLib|MdeModulePkg/Library/DxePerformanceLib/DxePerformanceLib.inf {
    <SOURCE_OVERRIDE_PATH>
      MdeModulePkg/Override/Library/DxePerformanceLib
  }
!else
  PerformanceLib|MdePkg/Library/BasePerformanceLibNull/BasePerformanceLibNull.inf
!endif
#[-end-200226-IB14630336-modify]#
  PostCodeLib|MdePkg/Library/BasePostCodeLibPort80/BasePostCodeLibPort80.inf
  ReportStatusCodeLib|MdeModulePkg/Library/DxeReportStatusCodeLib/DxeReportStatusCodeLib.inf
  #
  # UEFI & PI
  #
  UefiBootServicesTableLib|MdePkg/Library/UefiBootServicesTableLib/UefiBootServicesTableLib.inf
  UefiRuntimeServicesTableLib|MdePkg/Library/UefiRuntimeServicesTableLib/UefiRuntimeServicesTableLib.inf
  UefiLib|MdePkg/Library/UefiLib/UefiLib.inf
!if $(TARGET) == RELEASE
  DebugLib|MdePkg/Library/BaseDebugLibNull/BaseDebugLibNull.inf
!else
  DebugLib|$(C1S_FSP_PATH)/Library/DebugLibDebugPort/Dxe/DebugLibDebugPort.inf
!endif
  #
  # Framework
  #
!if gSiPkgTokenSpaceGuid.PcdS3Enable == TRUE
  S3BootScriptLib|MdeModulePkg/Library/PiDxeS3BootScriptLib/DxeS3BootScriptLib.inf
!else
  S3BootScriptLib|MdePkg/Library/BaseS3BootScriptLibNull/BaseS3BootScriptLibNull.inf
!endif
  #
  # Silicon initialization library
  #

  DxeMeLib|$(PLATFORM_SI_PACKAGE)/IpBlock/Me/Library/DxeMeLib/DxeMeLib.inf
  MeUtilsLib|$(PLATFORM_SI_PACKAGE)/IpBlock/Me/Library/PeiDxeSmmMeUtilsLib/MeUtilsLib.inf
  PciExpressLib|$(PLATFORM_SI_PACKAGE)/Library/FspPciExpressMultiSegLib/FspPciExpressMultiSegLib.inf
  GpioNameBufferLib|$(PLATFORM_SI_PACKAGE)/IpBlock/Gpio/LibraryPrivate/DxeGpioNameBufferLib/DxeGpioNameBufferLib.inf
  ResetSystemLib|MdeModulePkg/Library/DxeResetSystemLib/DxeResetSystemLib.inf
  SiScheduleResetLib|$(PLATFORM_SI_PACKAGE)/Pch/LibraryPrivate/BaseSiScheduleResetLib/BaseSiScheduleResetLib.inf
  #
  #  CPU PCIe IpBlock
  #
!if gSiPkgTokenSpaceGuid.PcdCpuPcieEnable == TRUE
  DxeCpuPcieRpLib|$(PLATFORM_SI_PACKAGE)/IpBlock/CpuPcieRp/LibraryPrivate/DxeCpuPcieRpLib/DxeCpuPcieRpLib.inf
!else
  DxeCpuPcieRpLib|$(PLATFORM_SI_PACKAGE)/IpBlock/CpuPcieRp/LibraryPrivate/DxeCpuPcieRpLibNull/DxeCpuPcieRpLibNull.inf
!endif
  #
  #  Telemetry
  #
  TelemetryLib|$(PLATFORM_SI_PACKAGE)/IpBlock/Telemetry/Library/PeiDxeTelemetryLib/PeiDxeTelemetryLib.inf
  TelemetryPrivateLib|$(PLATFORM_SI_PACKAGE)/IpBlock/Telemetry/LibraryPrivate/PeiDxeTelemetryPrivateLib/PeiDxeTelemetryPrivateLib.inf
  PeiTelemetryPolicyLib|$(PLATFORM_SI_PACKAGE)/IpBlock/Telemetry/LibraryPrivate/PeiTelemetryPolicyLib/PeiTelemetryPolicyLib.inf
  PeiDxeTelemetryFruLib|$(PLATFORM_SI_PACKAGE)/Fru/AdlCpu/Telemetry/LibraryPrivate/PeiDxeTelemetryFruLib/PeiDxeTelemetryFruLib.inf

[PcdsFixedAtBuild]
#[-start-200226-IB14630336-modify]#
!if gAlderLakeFspPkgTokenSpaceGuid.PcdFspPerformanceEnable == TRUE
  gEfiMdePkgTokenSpaceGuid.PcdPerformanceLibraryPropertyMask|0x1
  gEfiMdeModulePkgTokenSpaceGuid.PcdMaxPeiPerformanceLogEntries|150
!endif
#[-end-200226-IB14630336-modify]#
[PcdsDynamicVpd.Upd]
  #
  # This section is not used by the normal build process
  # Howerver, FSP will use dedicated tool to handle it and generate a
  # VPD simliar binary block (User Configuration Data). This block will
  # be accessed through a generated data structure directly rather than
  # PCD services. This is for size consideration.
  # Format:
  #   gPlatformFspPkgTokenSpaceGuid.Updxxxxxxxxxxxxn        | OFFSET | LENGTH | VALUE
  # Only simple data type is supported
  #

  # offset 0000 ~ 00B0
  !include $(FSP_PACKAGE)/Upd/FsptUpd.dsc

  # offset 0000 ~ 06AE
  !include $(FSP_PACKAGE)/Upd/FspmUpd.dsc

  # offset 0000 ~ 0D10
  !include $(FSP_PACKAGE)/Upd/FspsUpd.dsc

###################################################################################################
#
# Components Section - list of the modules and components that will be processed by compilation
#                      tools and the EDK II tools to generate PE32/PE32+/Coff image files.
#
# Note: The EDK II DSC file is not used to specify how compiled binary images get placed
#       into firmware volume images. This section is just a list of modules to compile from
#       source into UEFI-compliant binaries.
#       It is the FDF file that contains information on combining binary files into firmware
#       volume images, whose concept is beyond UEFI and is described in PI specification.
#       Binary modules do not need to be listed in this section, as they should be
#       specified in the FDF file. For example: Shell binary (Shell_Full.efi), FAT binary (Fat.efi),
#       Logo (Logo.bmp), and etc.
#       There may also be modules listed in this section that are not required in the FDF file,
#       When a module listed here is excluded from FDF file, then UEFI-compliant binary will be
#       generated for it, but the binary will not be put into any firmware volume.
#
###################################################################################################

[Components.IA32]
  #
  # SEC
  #
  IntelFsp2Pkg/FspSecCore/FspSecCoreT.inf {
    <LibraryClasses>
      MmPciLib|$(C1S_FSP_PATH)/Library/MmPciCf8Lib/MmPciCf8Lib.inf
      PciLib|MdePkg/Library/BasePciLibCf8/BasePciLibCf8.inf
      FspSecPlatformLib|$(PLATFORM_SI_PACKAGE)/Library/PlatformSecLib/FspTPlatformSecLib.inf
    <PcdsFixedAtBuild>
      gSiPkgTokenSpaceGuid.PcdFspValidatePeiServiceTablePointer|FALSE
  }
  IntelFsp2Pkg/FspSecCore/FspSecCoreM.inf {
    <LibraryClasses>
      MmPciLib|$(C1S_FSP_PATH)/Library/MmPciCf8Lib/MmPciCf8Lib.inf
      PciLib|MdePkg/Library/BasePciLibCf8/BasePciLibCf8.inf
      FspSecPlatformLib|$(PLATFORM_SI_PACKAGE)/Library/PlatformSecLib/FspMPlatformSecLib.inf
    <PcdsFixedAtBuild>
      gSiPkgTokenSpaceGuid.PcdFspValidatePeiServiceTablePointer|FALSE
  }
  IntelFsp2Pkg/FspSecCore/Fsp22SecCoreS.inf {
    <LibraryClasses>
      MmPciLib|$(C1S_FSP_PATH)/Library/MmPciCf8Lib/MmPciCf8Lib.inf
      PciLib|MdePkg/Library/BasePciLibCf8/BasePciLibCf8.inf
      FspSecPlatformLib|$(PLATFORM_SI_PACKAGE)/Library/PlatformSecLib/FspSPlatformSecLib.inf
    <PcdsFixedAtBuild>
      gSiPkgTokenSpaceGuid.PcdFspValidatePeiServiceTablePointer|FALSE
  }

  #
  # PEI Core
  #
  MdeModulePkg/Core/Pei/PeiMain.inf {
#[-start-200226-IB14630336-add]#
    <SOURCE_OVERRIDE_PATH>
#[-start-211028-IB05660184-add]#
!if $(FIRMWARE_PERFORMANCE) == YES
      Intel/AlderLake/$(CHIPSET_PKG)/Override/EDK2/MdeModulePkg/Core/Pei
!endif
#[-end-211028-IB05660184-add]#
      MdeModulePkg/Override/Core/Pei
#[-end-200226-IB14630336-add]#
    <LibraryClasses>
      MmPciLib|$(C1S_FSP_PATH)/Library/MmPciCf8Lib/MmPciCf8Lib.inf
      PciLib|MdePkg/Library/BasePciLibCf8/BasePciLibCf8.inf
    <PcdsFixedAtBuild>
      gEfiMdePkgTokenSpaceGuid.PcdDebugPropertyMask|0x27
  }

  #
  # PCD
  #
  MdeModulePkg/Universal/PCD/Pei/Pcd.inf {
    <LibraryClasses>
      PcdLib|MdePkg/Library/BasePcdLibNull/BasePcdLibNull.inf
      DebugLib|MdePkg/Library/BaseDebugLibNull/BaseDebugLibNull.inf
      NULL|$(C1S_FSP_PATH)/Library/FspEmptyLib/FspEmptyLib.inf
  }
  #
  # FSP Binary Components
  #
#[-start-180803-IB15410172-modify]#
  $(C1S_FSP_PATH)/FspGlobalDataInit/Pei/FspGlobalDataInitPei.inf {
    <LibraryClasses>
      MmPciLib|$(C1S_FSP_PATH)/Library/MmPciCf8Lib/MmPciCf8Lib.inf
      PciLib|MdePkg/Library/BasePciLibCf8/BasePciLibCf8.inf
!if $(FSP_DEBUG) == YES
      DebugLib|MdePkg/Library/BaseDebugLibSerialPort/BaseDebugLibSerialPort.inf
!else
      DebugLib|MdePkg/Library/BaseDebugLibNull/BaseDebugLibNull.inf
!endif
#[-end-180803-IB15410172-modify]#
  }
#[-start-180709-IB15590108-modify]#
!if $(FSP_DEBUG) == YES
  $(C1S_FSP_PATH)/FspDebugInit/Pei/FspDebugServicePei.inf {
    # library for display FSP information
    <LibraryClasses>
      DebugLib|MdePkg/Library/BaseDebugLibSerialPort/BaseDebugLibSerialPort.inf
      SerialPortLib|$(C1S_FSP_PATH)/Library/SerialPortLib/SerialPortLib.inf
    <PcdsFixedAtBuild>
      gEfiMdePkgTokenSpaceGuid.PcdDebugPropertyMask|0x2F
    <PcdsPatchableInModule>
      gEfiMdePkgTokenSpaceGuid.PcdDebugPrintErrorLevel|0x80080046
  }
!endif
#[-end-180709-IB15590108-modify]#
  $(FSP_PACKAGE)/FspHeader/FspHeader.inf
  $(C1S_PRODUCT_PATH)/SiInit/Pei/SiInitPreMemFsp.inf {
!if gSiPkgTokenSpaceGuid.PcdAdlLpSupport == FALSE
    <LibraryClasses>
      NULL|$(FSP_PACKAGE)/Library/PciePinInitPreMemLib/PciePinInitPreMemLib.inf
!endif
  }
  MdeModulePkg/Universal/PcatSingleSegmentPciCfg2Pei/PcatSingleSegmentPciCfg2Pei.inf {
    <LibraryClasses>
      NULL|$(C1S_FSP_PATH)/Library/FspEmptyLib/FspEmptyLib.inf
  }


  #
  # CpuMp PEIM for MpService PPI
  # In Dispatch mode: if MpServicePpi already installed by boot loader
  #                   this will be skipped.
  # In API mode     : If external MpService stack passed by UPD, this will
  #                   be skipped
  #
  # If not skipped, this module must be dispatched earlier than PostMem
  # silicon policy PPI installed, for PostMem silicon initialization to
  # consume MpService PPI.
  #
  UefiCpuPkg/CpuMpPei/CpuMpPei.inf {
    <LibraryClasses>
      NULL|$(PLATFORM_SI_PACKAGE)/Library/PeiReadyToInstallMpLib/PeiReadyToInstallMpLib.inf
      DebugLib|MdePkg/Library/BaseDebugLibSerialPort/BaseDebugLibSerialPort.inf
  }

  #
  # CpuFeatures PEIM
  #
  UefiCpuPkg/CpuFeatures/CpuFeaturesPei.inf {
    <LibraryClasses>
      RegisterCpuFeaturesLib|UefiCpuPkg/Library/RegisterCpuFeaturesLib/PeiRegisterCpuFeaturesLib.inf
      NULL|UefiCpuPkg/Library/CpuCommonFeaturesLib/CpuCommonFeaturesLib.inf
      NULL|$(PLATFORM_SI_PACKAGE)/Cpu/LibraryPrivate/PeiCpuSpecificFeaturesLib/PeiCpuSpecificFeaturesLib.inf
      DebugLib|MdePkg/Library/BaseDebugLibSerialPort/BaseDebugLibSerialPort.inf
  }

  $(C1S_PRODUCT_PATH)/SiInit/Pei/SiInitFsp.inf

  MdeModulePkg/Core/DxeIplPeim/DxeIpl.inf {
    <LibraryClasses>
      DebugAgentLib|MdeModulePkg/Library/DebugAgentLibNull/DebugAgentLibNull.inf
      ResetSystemLib|MdeModulePkg/Library/BaseResetSystemLibNull/BaseResetSystemLibNull.inf
      NULL|$(C1S_FSP_PATH)/Library/FspEmptyLib/FspEmptyLib.inf
  }

  MdeModulePkg/Universal/ResetSystemPei/ResetSystemPei.inf {
    <LibraryClasses>
      ResetSystemLib|$(PLATFORM_SI_PACKAGE)/Pch/Library/BaseResetSystemLib/BaseResetSystemLib.inf
      NULL|$(C1S_FSP_PATH)/Library/FspEmptyLib/FspEmptyLib.inf
  }
  $(C1S_FSP_PATH)/FspInit/Pei/FspInitPreMem.inf
  $(C1S_FSP_PATH)/FspPcdInit/Pei/FspPcdInit.inf
  $(C1S_FSP_PATH)/FspInit/Pei/FspInit.inf {
    <LibraryClasses>
      CpuPolicyLib|$(PLATFORM_SI_PACKAGE)/Cpu/Library/PeiCpuPolicyLib/PeiCpuPolicyLib.inf
  }

  #
  # Silicon policy should be dependency on all of Notify phase code for dispatching later than
  # SiInit done.
  #
  $(PLATFORM_SI_PACKAGE)/SystemAgent/SaInit/Dxe/SaInitFsp.inf

  $(PLATFORM_SI_PACKAGE)/Pch/PchInit/Dxe/PchInitDxeFspTgl.inf

  $(PLATFORM_SI_PACKAGE)/IpBlock/Me/HeciInit/Dxe/HeciInitFsp.inf

  IntelFsp2Pkg/FspNotifyPhase/FspNotifyPhasePeim.inf {
    <LibraryClasses>
      NULL|$(PLATFORM_SI_PACKAGE)/Library/PeiSiliconPolicyInitLibDependency/PeiPostMemSiliconPolicyInitLibDependency.inf
  }
  $(C1S_FSP_PATH)/FspInit/Pei/FspS3Notify.inf
  $(C1S_FSP_PATH)/FspEndOfPei2/FspEndOfPei2Peim.inf

[Components.X64]
  $(PLATFORM_SI_PACKAGE)/Pch/PchInit/Dxe/PchInitDxeFsp.inf
  $(PLATFORM_SI_PACKAGE)/IpBlock/Me/HeciInit/Dxe/HeciInitDxeFsp.inf
  $(PLATFORM_SI_PACKAGE)/SystemAgent/SaInit/Dxe/SaInitDxeFsp.inf

!include $(C1S_PRODUCT_PATH)/SiPkgBuildOption.dsc
!include $(FSP_PACKAGE)/FspPkgBuildOption.dsc

###################################################################################################
#
# BuildOptions Section - Define the module specific tool chain flags that should be used as
#                        the default flags for a module. These flags are appended to any
#                        standard flags that are defined by the build process. They can be
#                        applied for any modules or only those modules with the specific
#                        module style (EDK or EDKII) specified in [Components] section.
#
###################################################################################################
[BuildOptions]
# Append build options for EDK and EDKII drivers (= is Append, == is Replace)

[BuildOptions.Common.EDKII]

!if gAlderLakeFspPkgTokenSpaceGuid.PcdMiniBiosEnable == TRUE
  DEFINE EDKII_DSC_MINIBIOS_BUILD_OPTIONS = -DMRC_MINIBIOS_BUILD
!else
  DEFINE EDKII_DSC_MINIBIOS_BUILD_OPTIONS =
!endif

!if gSiPkgTokenSpaceGuid.PcdSsaFlagEnable == TRUE
!if gSiPkgTokenSpaceGuid.PcdEvLoaderEnable == TRUE
  DEFINE EV_LOADER_BUILD_OPTIONS = -DSSA_FLAG=1
!else
  DEFINE EV_LOADER_BUILD_OPTIONS =
!endif
!else
  DEFINE EV_LOADER_BUILD_OPTIONS =
!endif

  DEFINE EDKII_DSC_FEATURE_BUILD_OPTIONS = $(DSC_SIPKG_FEATURE_BUILD_OPTIONS) $(EDKII_DSC_MINIBIOS_BUILD_OPTIONS) -DFSP_FLAG

  DEFINE EDKII_DSC_ALL_BUILD_OPTIONS = $(EDKII_DSC_FEATURE_BUILD_OPTIONS)

#[-start-190925-IB04530033-modify]#
#
# Use FSP_DEBUG to decide build EFI debug message or not
# /d : Inculde debug message
# /ddt : Not Include debug message (To avoid com port DDT disconnect in FSP modules)
#
!if ("GCC49" in $(TOOL_CHAIN_TAG)) OR ("GCC5" in $(TOOL_CHAIN_TAG))
GCC:   *_*_IA32_ASM_FLAGS   = $(EDKII_DSC_FEATURE_BUILD_OPTIONS) -Wa,--defsym,FSP_FLAG=1
  *_*_IA32_PP_FLAGS = $(EDKII_DSC_ALL_BUILD_OPTIONS)
!else
GCC:   *_*_IA32_ASM_FLAGS   = $(EDKII_DSC_FEATURE_BUILD_OPTIONS) -DFSP_FLAG=1
GCC:   *_*_IA32_PP_FLAGS = $(EDKII_DSC_ALL_BUILD_OPTIONS)
!endif
MSFT:  *_*_IA32_ASM_FLAGS   = $(EDKII_DSC_FEATURE_BUILD_OPTIONS)
  *_*_IA32_NASM_FLAGS  = $(EDKII_DSC_FEATURE_BUILD_OPTIONS) -DFSP_FLAG=1
!if $(FSP_DEBUG) == YES
#[-start-201215-IB16560234-modify]#
# Set flag FSP_DEBUG for FSP debug mode
  *_*_IA32_CC_FLAGS    = $(EDKII_DSC_ALL_BUILD_OPTIONS) -DFSP_FLAG -DFSP_DEBUG=1
!else
  *_*_IA32_CC_FLAGS    = $(EDKII_DSC_ALL_BUILD_OPTIONS) -DFSP_FLAG /D MDEPKG_NDEBUG -DFSP_DEBUG=0
!endif
#[-end-201215-IB16560234-modify]#
  *_*_IA32_VFRPP_FLAGS = $(EDKII_DSC_ALL_BUILD_OPTIONS)
  *_*_IA32_APP_FLAGS   = $(EDKII_DSC_ALL_BUILD_OPTIONS)
  *_*_IA32_PP_FLAGS    = -DFSP_FLAG=1

!if ("GCC49" in $(TOOL_CHAIN_TAG)) OR ("GCC5" in $(TOOL_CHAIN_TAG))
GCC:   *_*_X64_ASM_FLAGS   = $(EDKII_DSC_FEATURE_BUILD_OPTIONS) -Wa,--defsym,FSP_FLAG=1
!else
GCC:   *_*_X64_ASM_FLAGS   = $(EDKII_DSC_FEATURE_BUILD_OPTIONS) -DFSP_FLAG=1
!endif
MSFT:  *_*_X64_ASM_FLAGS   = $(EDKII_DSC_FEATURE_BUILD_OPTIONS)
  *_*_X64_NASM_FLAGS  = $(EDKII_DSC_FEATURE_BUILD_OPTIONS) -DFSP_FLAG=1
!if $(FSP_DEBUG) == YES
  *_*_X64_CC_FLAGS    = $(EDKII_DSC_ALL_BUILD_OPTIONS) -DFSP_FLAG
!else
  *_*_X64_CC_FLAGS    = $(EDKII_DSC_ALL_BUILD_OPTIONS) -DFSP_FLAG /D MDEPKG_NDEBUG
!endif
  *_*_X64_VFRPP_FLAGS = $(EDKII_DSC_ALL_BUILD_OPTIONS)
  *_*_X64_APP_FLAGS   = $(EDKII_DSC_ALL_BUILD_OPTIONS)
  *_*_X64_PP_FLAGS    = -DFSP_FLAG=1
#[-end-190925-IB04530033-modify]#

#[-start-210608-KEBIN00010-add]#
!if $(LCFC_SUPPORT_ENABLE) == YES
  MSFT:*_*_*_CC_FLAGS     = $(LCFC_CC_FLAGS)
  MSFT:*_*_*_VFRPP_FLAGS  = $(LCFC_CC_FLAGS)
  MSFT:*_*_*_ASLPP_FLAGS  = $(LCFC_CC_FLAGS)
  MSFT:*_*_*_ASLCC_FLAGS  = $(LCFC_CC_FLAGS)
  MSFT:*_*_*_VFCFPP_FLAGS = $(LCFC_CC_FLAGS)
!endif
#[-end-210608-KEBIN00010-add]#
