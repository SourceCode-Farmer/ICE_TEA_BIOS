## @file
#  Platform description.
#
# @copyright
#  INTEL CONFIDENTIAL
#  Copyright 2017 - 2022 Intel Corporation.
#
#  The source code contained or described herein and all documents related to the
#  source code ("Material") are owned by Intel Corporation or its suppliers or
#  licensors. Title to the Material remains with Intel Corporation or its suppliers
#  and licensors. The Material may contain trade secrets and proprietary and
#  confidential information of Intel Corporation and its suppliers and licensors,
#  and is protected by worldwide copyright and trade secret laws and treaty
#  provisions. No part of the Material may be used, copied, reproduced, modified,
#  published, uploaded, posted, transmitted, distributed, or disclosed in any way
#  without Intel's prior express written permission.
#
#  No license under any patent, copyright, trade secret or other intellectual
#  property right is granted to or conferred upon you by disclosure or delivery
#  of the Materials, either expressly, by implication, inducement, estoppel or
#  otherwise. Any license under such intellectual property rights must be
#  express and approved by Intel in writing.
#
#  Unless otherwise agreed by Intel in writing, you may not remove or alter
#  this notice or any other notice embedded in Materials by Intel or
#  Intel's suppliers or licensors in any way.
#
#  This file contains an 'Intel Peripheral Driver' and is uniquely identified as
#  "Intel Reference Module" and is licensed for Intel CPUs and chipsets under
#  the terms of your license agreement with Intel or your vendor. This file may
#  be modified by the user, subject to additional terms of the license agreement.
#
# @par Specification
##

  ################################################################################
  #
  # UPDs consumed in FspMemoryInit Api
  #
  ################################################################################
  # !BSF FIND:{ADLUPD_M}
  # !HDR COMMENT:{FSP_UPD_HEADER:FSP UPD Header}
  # !HDR EMBED:{FSP_UPD_HEADER:FspUpdHeader:START}
  # FspmUpdSignature: {ADLUPD_M}
  gPlatformFspPkgTokenSpaceGuid.Signature                   | * | 0x08 | 0x4D5F4450554C4441
  # !BSF NAME:{FspmUpdRevision}  TYPE:{None}
  gPlatformFspPkgTokenSpaceGuid.Revision                    | * | 0x01 | 0x02
  # !HDR EMBED:{FSP_UPD_HEADER:FspUpdHeader:END}
  gPlatformFspPkgTokenSpaceGuid.Reserved                    | * | 0x17 | {0x00}

  # !HDR COMMENT:{FSPM_ARCH_UPD:Fsp M Architecture UPD}
  # !HDR EMBED:{FSPM_ARCH_UPD:FspmArchUpd:START}
  gPlatformFspPkgTokenSpaceGuid.Revision                    | * | 0x01 | 0x01

  gPlatformFspPkgTokenSpaceGuid.Reserved                    | * | 0x03 | {0x00}

  # !HDR STRUCT:{VOID *}
  gPlatformFspPkgTokenSpaceGuid.NvsBufferPtr                | * | 0x04 | 0x00000000

  # !HDR STRUCT:{VOID *}
  gPlatformFspPkgTokenSpaceGuid.StackBase                   | * | 0x04 | 0xFEF17F00

  gPlatformFspPkgTokenSpaceGuid.StackSize                   | * | 0x04 | 0x28000

  gPlatformFspPkgTokenSpaceGuid.BootLoaderTolumSize         | * | 0x04 | 0x00000000

  gPlatformFspPkgTokenSpaceGuid.BootMode                    | * | 0x04 | 0x00

  # !HDR EMBED:{FSPM_ARCH_UPD:FspmArchUpd:END}
  gPlatformFspPkgTokenSpaceGuid.Reserved1                   | * | 0x08 | {0x00}

  # !HDR COMMENT:{FSP_M_CONFIG:Fsp M Configuration}
  # !HDR EMBED:{FSP_M_CONFIG:FspmConfig:START}
  # !BSF PAGE:{MRC}
  # !BSF NAME:{Platform Reserved Memory Size} TYPE:{EditNum, HEX, (0x00,0xFFFFFFFFFFFFFFFF)}
  # !BSF HELP:{The minimum platform memory size required to pass control into DXE}
  gPlatformFspPkgTokenSpaceGuid.PlatformMemorySize          | * | 0x08 | 0x550000

  # !BSF NAME:{SPD Data Length} TYPE:{Combo}
  # !BSF OPTION:{0x100:256 Bytes, 0x200:512 Bytes, 0x400:1024 Bytes}
  # !BSF HELP:{Length of SPD Data}
  gPlatformFspPkgTokenSpaceGuid.MemorySpdDataLen            | * | 0x02 | 0x200

  # !BSF NAME:{Enable above 4GB MMIO resource support} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Enable/disable above 4GB MMIO resource support}
  gPlatformFspPkgTokenSpaceGuid.EnableAbove4GBMmio          | * | 0x01 | 0x01


  # !BSF PAGE:{SA1}
  # !BSF NAME:{Enable/Disable CrashLog Device 10} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Enable(Default): Enable CPU CrashLog Device 10, Disable: Disable CPU CrashLog}
  gPlatformFspPkgTokenSpaceGuid.CpuCrashLogDevice           | * | 0x01 | 0x1

  # !BSF PAGE:{MRC}
  # !BSF NAME:{Memory SPD Pointer Controller 0 Channel 0 Dimm 0}  TYPE:{EditNum, HEX, (0x00,0xFFFFFFFF)}
  # !BSF HELP:{Pointer to SPD data, will be used only when SpdAddressTable SPD Address are marked as 00}
  gPlatformFspPkgTokenSpaceGuid.MemorySpdPtr000              | * | 0x04 | 0x00000000

  # !BSF NAME:{Memory SPD Pointer Controller 0 Channel 0 Dimm 1}  TYPE:{EditNum, HEX, (0x00,0xFFFFFFFF)}
  # !BSF HELP:{Pointer to SPD data, will be used only when SpdAddressTable SPD Address are marked as 00}
  gPlatformFspPkgTokenSpaceGuid.MemorySpdPtr001              | * | 0x04 | 0x00000000

  # !BSF NAME:{Memory SPD Pointer Controller 0 Channel 1 Dimm 0}  TYPE:{EditNum, HEX, (0x00,0xFFFFFFFF)}
  # !BSF HELP:{Pointer to SPD data, will be used only when SpdAddressTable SPD Address are marked as 00}
  gPlatformFspPkgTokenSpaceGuid.MemorySpdPtr010              | * | 0x04 | 0x00000000

  # !BSF NAME:{Memory SPD Pointer Controller 0 Channel 1 Dimm 1}  TYPE:{EditNum, HEX, (0x00,0xFFFFFFFF)}
  # !BSF HELP:{Pointer to SPD data, will be used only when SpdAddressTable SPD Address are marked as 00}
  gPlatformFspPkgTokenSpaceGuid.MemorySpdPtr011              | * | 0x04 | 0x00000000

  # !BSF NAME:{Memory SPD Pointer Controller 0 Channel 2 Dimm 0}  TYPE:{EditNum, HEX, (0x00,0xFFFFFFFF)}
  # !BSF HELP:{Pointer to SPD data, will be used only when SpdAddressTable SPD Address are marked as 00}
  gPlatformFspPkgTokenSpaceGuid.MemorySpdPtr020              | * | 0x04 | 0x00000000

  # !BSF NAME:{Memory SPD Pointer Controller 0 Channel 2 Dimm 1}  TYPE:{EditNum, HEX, (0x00,0xFFFFFFFF)}
  # !BSF HELP:{Pointer to SPD data, will be used only when SpdAddressTable SPD Address are marked as 00}
  gPlatformFspPkgTokenSpaceGuid.MemorySpdPtr021              | * | 0x04 | 0x00000000

  # !BSF NAME:{Memory SPD Pointer Controller 0 Channel 3 Dimm 0}  TYPE:{EditNum, HEX, (0x00,0xFFFFFFFF)}
  # !BSF HELP:{Pointer to SPD data, will be used only when SpdAddressTable SPD Address are marked as 00}
  gPlatformFspPkgTokenSpaceGuid.MemorySpdPtr030              | * | 0x04 | 0x00000000

  # !BSF NAME:{Memory SPD Pointer Controller 0 Channel 3 Dimm 1}  TYPE:{EditNum, HEX, (0x00,0xFFFFFFFF)}
  # !BSF HELP:{Pointer to SPD data, will be used only when SpdAddressTable SPD Address are marked as 00}
  gPlatformFspPkgTokenSpaceGuid.MemorySpdPtr031              | * | 0x04 | 0x00000000

  # !BSF NAME:{Memory SPD Pointer Controller 1 Channel 0 Dimm 0}  TYPE:{EditNum, HEX, (0x00,0xFFFFFFFF)}
  # !BSF HELP:{Pointer to SPD data, will be used only when SpdAddressTable SPD Address are marked as 00}
  gPlatformFspPkgTokenSpaceGuid.MemorySpdPtr100              | * | 0x04 | 0x00000000

  # !BSF NAME:{Memory SPD Pointer Controller 1 Channel 0 Dimm 1}  TYPE:{EditNum, HEX, (0x00,0xFFFFFFFF)}
  # !BSF HELP:{Pointer to SPD data, will be used only when SpdAddressTable SPD Address are marked as 00}
  gPlatformFspPkgTokenSpaceGuid.MemorySpdPtr101              | * | 0x04 | 0x00000000

  # !BSF NAME:{Memory SPD Pointer Controller 1 Channel 1 Dimm 0}  TYPE:{EditNum, HEX, (0x00,0xFFFFFFFF)}
  # !BSF HELP:{Pointer to SPD data, will be used only when SpdAddressTable SPD Address are marked as 00}
  gPlatformFspPkgTokenSpaceGuid.MemorySpdPtr110              | * | 0x04 | 0x00000000

  # !BSF NAME:{Memory SPD Pointer Controller 1 Channel 1 Dimm 1}  TYPE:{EditNum, HEX, (0x00,0xFFFFFFFF)}
  # !BSF HELP:{Pointer to SPD data, will be used only when SpdAddressTable SPD Address are marked as 00}
  gPlatformFspPkgTokenSpaceGuid.MemorySpdPtr111              | * | 0x04 | 0x00000000

  # !BSF NAME:{Memory SPD Pointer Controller 1 Channel 2 Dimm 0}  TYPE:{EditNum, HEX, (0x00,0xFFFFFFFF)}
  # !BSF HELP:{Pointer to SPD data, will be used only when SpdAddressTable SPD Address are marked as 00}
  gPlatformFspPkgTokenSpaceGuid.MemorySpdPtr120              | * | 0x04 | 0x00000000

  # !BSF NAME:{Memory SPD Pointer Controller 1 Channel 2 Dimm 1}  TYPE:{EditNum, HEX, (0x00,0xFFFFFFFF)}
  # !BSF HELP:{Pointer to SPD data, will be used only when SpdAddressTable SPD Address are marked as 00}
  gPlatformFspPkgTokenSpaceGuid.MemorySpdPtr121              | * | 0x04 | 0x00000000

  # !BSF NAME:{Memory SPD Pointer Controller 1 Channel 3 Dimm 0}  TYPE:{EditNum, HEX, (0x00,0xFFFFFFFF)}
  # !BSF HELP:{Pointer to SPD data, will be used only when SpdAddressTable SPD Address are marked as 00}
  gPlatformFspPkgTokenSpaceGuid.MemorySpdPtr130              | * | 0x04 | 0x00000000

  # !BSF NAME:{Memory SPD Pointer Controller 1 Channel 3 Dimm 1}  TYPE:{EditNum, HEX, (0x00,0xFFFFFFFF)}
  # !BSF HELP:{Pointer to SPD data, will be used only when SpdAddressTable SPD Address are marked as 00}
  gPlatformFspPkgTokenSpaceGuid.MemorySpdPtr131              | * | 0x04 | 0x00000000

  # !BSF NAME:{RcompResistor settings}  TYPE:{EditNum, HEX, (0x00, 0xFFFF)}
  # !BSF HELP:{Indicates  RcompResistor settings: Board-dependent}
  gPlatformFspPkgTokenSpaceGuid.RcompResistor               | * | 0x02 | 0

  # !BSF NAME:{RcompTarget settings} TYPE:{EditNum, HEX, (0x00,0xFFFFFFFFFFFFFFFFFFFF)}
  # !HDR STRUCT:{UINT16}
  # !BSF HELP:{RcompTarget settings: board-dependent}
  gPlatformFspPkgTokenSpaceGuid.RcompTarget                 | * | 0x0A | { 0, 0, 0, 0, 0 }

  # !BSF NAME:{Dqs Map CPU to DRAM MC 0 CH 0} TYPE:{EditNum, HEX, (0x00,0xFFFF)}
  # !BSF HELP:{Set Dqs mapping relationship between CPU and DRAM, Channel 0: board-dependent}
  gPlatformFspPkgTokenSpaceGuid.DqsMapCpu2DramMc0Ch0           | * | 0x02 | { 0, 1 }

  # !BSF NAME:{Dqs Map CPU to DRAM MC 0 CH 1} TYPE:{EditNum, HEX, (0x00,0xFFFF)}
  # !BSF HELP:{Set Dqs mapping relationship between CPU and DRAM, Channel 1: board-dependent}
  gPlatformFspPkgTokenSpaceGuid.DqsMapCpu2DramMc0Ch1           | * | 0x02 | { 0, 1 }

  # !BSF NAME:{Dqs Map CPU to DRAM MC 0 CH 2} TYPE:{EditNum, HEX, (0x00,0xFFFF)}
  # !BSF HELP:{Set Dqs mapping relationship between CPU and DRAM, Channel 2: board-dependent}
  gPlatformFspPkgTokenSpaceGuid.DqsMapCpu2DramMc0Ch2           | * | 0x02 | { 0, 1 }

  # !BSF NAME:{Dqs Map CPU to DRAM MC 0 CH 3} TYPE:{EditNum, HEX, (0x00,0xFFFF)}
  # !BSF HELP:{Set Dqs mapping relationship between CPU and DRAM, Channel 3: board-dependent}
  gPlatformFspPkgTokenSpaceGuid.DqsMapCpu2DramMc0Ch3           | * | 0x02 | { 0, 1 }

  # !BSF NAME:{Dqs Map CPU to DRAM MC 1 CH 0} TYPE:{EditNum, HEX, (0x00,0xFFFF)}
  # !BSF HELP:{Set Dqs mapping relationship between CPU and DRAM, Channel 0: board-dependent}
  gPlatformFspPkgTokenSpaceGuid.DqsMapCpu2DramMc1Ch0           | * | 0x02 | { 0, 1 }

  # !BSF NAME:{Dqs Map CPU to DRAM MC 1 CH 1} TYPE:{EditNum, HEX, (0x00,0xFFFF)}
  # !BSF HELP:{Set Dqs mapping relationship between CPU and DRAM, Channel 1: board-dependent}
  gPlatformFspPkgTokenSpaceGuid.DqsMapCpu2DramMc1Ch1           | * | 0x02 | { 0, 1 }

  # !BSF NAME:{Dqs Map CPU to DRAM MC 1 CH 2} TYPE:{EditNum, HEX, (0x00,0xFFFF)}
  # !BSF HELP:{Set Dqs mapping relationship between CPU and DRAM, Channel 2: board-dependent}
  gPlatformFspPkgTokenSpaceGuid.DqsMapCpu2DramMc1Ch2           | * | 0x02 | { 0, 1 }

  # !BSF NAME:{Dqs Map CPU to DRAM MC 1 CH 3} TYPE:{EditNum, HEX, (0x00,0xFFFF)}
  # !BSF HELP:{Set Dqs mapping relationship between CPU and DRAM, Channel 3: board-dependent}
  gPlatformFspPkgTokenSpaceGuid.DqsMapCpu2DramMc1Ch3           | * | 0x02 | { 0, 1 }

  # !BSF NAME:{Dq Map CPU to DRAM MC 0 CH 0} TYPE:{EditNum, HEX, (0x00,0xFFFF)}
  # !BSF HELP:{Set Dq mapping relationship between CPU and DRAM, Channel 0: board-dependent}
  gPlatformFspPkgTokenSpaceGuid.DqMapCpu2DramMc0Ch0            | * | 0x10 | { 0, 1, 2, 3, 4, 5, 6, 7,  8, 9, 10, 11, 12, 13, 14, 15 }

  # !BSF NAME:{Dq Map CPU to DRAM MC 0 CH 1} TYPE:{EditNum, HEX, (0x00,0xFFFF)}
  # !BSF HELP:{Set Dq mapping relationship between CPU and DRAM, Channel 1: board-dependent}
  gPlatformFspPkgTokenSpaceGuid.DqMapCpu2DramMc0Ch1            | * | 0x10 | { 0, 1, 2, 3, 4, 5, 6, 7,  8, 9, 10, 11, 12, 13, 14, 15 }

  # !BSF NAME:{Dq Map CPU to DRAM MC 0 CH 2} TYPE:{EditNum, HEX, (0x00,0xFFFF)}
  # !BSF HELP:{Set Dq mapping relationship between CPU and DRAM, Channel 2: board-dependet}
  gPlatformFspPkgTokenSpaceGuid.DqMapCpu2DramMc0Ch2            | * | 0x10 | { 0, 1, 2, 3, 4, 5, 6, 7,  8, 9, 10, 11, 12, 13, 14, 15 }

  # !BSF NAME:{Dq Map CPU to DRAM MC 0 CH 3} TYPE:{EditNum, HEX, (0x00,0xFFFF)}
  # !BSF HELP:{Set Dq mapping relationship between CPU and DRAM, Channel 3: board-dependent}
  gPlatformFspPkgTokenSpaceGuid.DqMapCpu2DramMc0Ch3            | * | 0x10 | { 0, 1, 2, 3, 4, 5, 6, 7,  8, 9, 10, 11, 12, 13, 14, 15 }

  # !BSF NAME:{Dq Map CPU to DRAM MC 1 CH 0} TYPE:{EditNum, HEX, (0x00,0xFFFF)}
  # !BSF HELP:{Set Dq mapping relationship between CPU and DRAM, Channel 0: board-dependent}
  gPlatformFspPkgTokenSpaceGuid.DqMapCpu2DramMc1Ch0            | * | 0x10 | { 0, 1, 2, 3, 4, 5, 6, 7,  8, 9, 10, 11, 12, 13, 14, 15 }

  # !BSF NAME:{Dq Map CPU to DRAM MC 1 CH 1} TYPE:{EditNum, HEX, (0x00,0xFFFF)}
  # !BSF HELP:{Set Dq mapping relationship between CPU and DRAM, Channel 1: board-dependent}
  gPlatformFspPkgTokenSpaceGuid.DqMapCpu2DramMc1Ch1            | * | 0x10 | { 0, 1, 2, 3, 4, 5, 6, 7,  8, 9, 10, 11, 12, 13, 14, 15 }

  # !BSF NAME:{Dq Map CPU to DRAM MC 1 CH 2} TYPE:{EditNum, HEX, (0x00,0xFFFF)}
  # !BSF HELP:{Set Dq mapping relationship between CPU and DRAM, Channel 2: board-dependent}
  gPlatformFspPkgTokenSpaceGuid.DqMapCpu2DramMc1Ch2            | * | 0x10 | { 0, 1, 2, 3, 4, 5, 6, 7,  8, 9, 10, 11, 12, 13, 14, 15 }

  # !BSF NAME:{Dq Map CPU to DRAM MC 1 CH 3} TYPE:{EditNum, HEX, (0x00,0xFFFF)}
  # !BSF HELP:{Set Dq mapping relationship between CPU and DRAM, Channel 3: board-dependent}
  gPlatformFspPkgTokenSpaceGuid.DqMapCpu2DramMc1Ch3            | * | 0x10 | { 0, 1, 2, 3, 4, 5, 6, 7,  8, 9, 10, 11, 12, 13, 14, 15 }

  # !BSF NAME:{Dqs Pins Interleaved Setting} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Indicates DqPinsInterleaved setting: board-dependent}
  gPlatformFspPkgTokenSpaceGuid.DqPinsInterleaved           | * | 0x01 | 0x0

  # !BSF NAME:{Smram Mask} TYPE:{Combo}
  # !BSF OPTION:{0: Neither, 1:AB-SEG, 2:H-SEG, 3: Both}
  # !BSF HELP:{The SMM Regions AB-SEG and/or H-SEG reserved}
  gPlatformFspPkgTokenSpaceGuid.SmramMask                   | * | 0x01 | 0x0

  # !BSF NAME:{Ibecc} TYPE:{Combo}
  # !BSF OPTION:{$EN_DIS}
  # !BSF HELP:{Enable/Disable Ibecc}
  gPlatformFspPkgTokenSpaceGuid.Ibecc                       | * | 0x01 | 0x0

  # !BSF NAME:{IbeccOperationMode}
  # !BSF TYPE:{Combo} OPTION:{0:Protect base on address range, 1:Non-protected, 2:All protected}
  # !BSF HELP:{In-Band ECC Operation Mode}
  gPlatformFspPkgTokenSpaceGuid.IbeccOperationMode          | * | 0x01 | 0x02

  # !BSF NAME:{IbeccProtectedRangeEnable}
  # !BSF TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{In-Band ECC Protected Region Enable}
  gPlatformFspPkgTokenSpaceGuid.IbeccProtectedRangeEnable   | * | 0x8 | { 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0 }

  # !BSF NAME:{IbeccProtectedRangeBase} TYPE:{EditNum, HEX, (0x00000000,0x03FFFFFF)}
  # !HDR STRUCT:{UINT32}
  # !BSF HELP:{IBECC Protected Region Base}
  gPlatformFspPkgTokenSpaceGuid.IbeccProtectedRangeBase     | * | 0x20 | { 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000 }

  # !BSF NAME:{IbeccProtectedRangeMask} TYPE:{EditNum, HEX, (0x00000001,0x03FFFFFF)}
  # !HDR STRUCT:{UINT32}
  # !BSF HELP:{IBECC Protected Region Mask}
  gPlatformFspPkgTokenSpaceGuid.IbeccProtectedRangeMask     | * | 0x20 | { 0x03FFFFE0, 0x03FFFFE0, 0x03FFFFE0, 0x03FFFFE0, 0x03FFFFE0, 0x03FFFFE0, 0x03FFFFE0, 0x03FFFFE0 }

  # !BSF NAME:{MRC Fast Boot} TYPE:{Combo}
  # !BSF OPTION:{$EN_DIS}
  # !BSF HELP:{Enables/Disable the MRC fast path thru the MRC}
  gPlatformFspPkgTokenSpaceGuid.MrcFastBoot                 | * | 0x01 | 0x1

  # !BSF NAME:{Rank Margin Tool per Task}
  # !BSF TYPE:{Combo}
  # !BSF OPTION:{$EN_DIS}
  # !BSF HELP:{This option enables the user to execute Rank Margin Tool per major training step in the MRC.}
  gPlatformFspPkgTokenSpaceGuid.RmtPerTask                  | * | 0x01 | 0x0

  # !BSF NAME:{Training Trace}
  # !BSF TYPE:{Combo}
  # !BSF OPTION:{$EN_DIS}
  # !BSF HELP:{This option enables the trained state tracing feature in MRC.  This feature will print out the key training parameters state across major training steps.}
  gPlatformFspPkgTokenSpaceGuid.TrainTrace                  | * | 0x01 | 0x0

  # !BSF NAME:{Tseg Size} TYPE:{Combo}
  # !BSF OPTION:{0x0400000:4MB, 0x01000000:16MB}
  # !BSF HELP:{Size of SMRAM memory reserved. 0x400000 for Release build and 0x1000000 for Debug build}
!if $(TARGET) == DEBUG
  gPlatformFspPkgTokenSpaceGuid.TsegSize                    | * | 0x04 | 0x01000000
!else
  gPlatformFspPkgTokenSpaceGuid.TsegSize                    | * | 0x04 | 0x0400000
!endif

  # !BSF NAME:{MMIO Size} TYPE:{EditNum, HEX, (0,0xC00)}
  # !BSF HELP:{Size of MMIO space reserved for devices. 0(Default)=Auto, non-Zero=size in MB}
  gPlatformFspPkgTokenSpaceGuid.MmioSize                    | * | 0x02 | 0x0

  # !BSF NAME:{Probeless Trace}
  # !BSF TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Probeless Trace: 0=Disabled, 1=Enable. Enabling Probeless Trace will reserve 128MB. This also requires IED to be enabled.}
  gPlatformFspPkgTokenSpaceGuid.ProbelessTrace              | * | 0x01 | 0x00

  # PCH configuration
  # !BSF PAGE:{PCH1}
  # !BSF NAME:{Enable SMBus} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Enable/disable SMBus controller.}
  gPlatformFspPkgTokenSpaceGuid.SmbusEnable                  | * | 0x01 | 0x01

  # !BSF PAGE:{MRC}
  # !BSF NAME:{Spd Address Tabl} TYPE:{EditNum, HEX, (0x00,0xFFFFFFFF)}
  # !HDR STRUCT:{UINT8}
  # !BSF HELP:{Specify SPD Address table for CH0D0/CH0D1/CH1D0&CH1D1. MemorySpdPtr will be used if SPD Address is 00}
  gPlatformFspPkgTokenSpaceGuid.SpdAddressTable              | * | 0x10 | { 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0 }

  # !BSF PAGE:{DBG}
  # !BSF NAME:{Platform Debug Consent} TYPE:{Combo} OPTION:{0:Disabled, 2:Enabled (All Probes+TraceHub), 6:Enable (Low Power), 7:Manual}
  # !BSF HELP:{Enabled(All Probes+TraceHub) supports all probes with TraceHub enabled and blocks s0ix\n\nEnabled(Low Power) does not support DCI OOB 4-wire and Tracehub is powergated by default, s0ix is viable\n\nManual:user needs to configure Advanced Debug Settings manually, aimed at advanced users}
  gPlatformFspPkgTokenSpaceGuid.PlatformDebugConsent         | * | 0x01 | 0x00

  # !BSF NAME:{DCI Enable} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Determine if to enable DCI debug from host}
  gPlatformFspPkgTokenSpaceGuid.DciEn                        | * | 0x01 | 0x00

  # !BSF NAME:{DCI DbC Mode} TYPE:{Combo} OPTION:{0:Disabled, 1:USB2 DbC, 2:USB3 DbC, 3:Both, 4:No Change}
  # !BSF HELP:{Disabled: Clear both USB2/3DBCEN; USB2: set USB2DBCEN; USB3: set USB3DBCEN; Both: Set both USB2/3DBCEN; No Change: Comply with HW value}
  gPlatformFspPkgTokenSpaceGuid.DciDbcMode                   | * | 0x01 | 0x04

  # !BSF NAME:{Enable DCI ModPHY Power Gate} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{DEPRECATED}
  gPlatformFspPkgTokenSpaceGuid.DciModphyPg                  | * | 0x01 | 0x00

  # !BSF NAME:{USB3 Type-C UFP2DFP Kernel/Platform Debug Support} TYPE:{Combo} OPTION:{0:Disabled, 1:Enabled, 2:No Change}
  # !BSF HELP:{This BIOS option enables kernel and platform debug for USB3 interface over a UFP Type-C receptacle, select 'No Change' will do nothing to UFP2DFP setting.}
  gPlatformFspPkgTokenSpaceGuid.DciUsb3TypecUfpDbg           | * | 0x01 | 0x02

  # !BSF PAGE:{PCH1}
  # !BSF NAME:{PCH Trace Hub Mode} TYPE:{Combo} OPTION:{0: Disable, 1: Target Debugger Mode, 2: Host Debugger Mode}
  # !BSF HELP:{Select 'Host Debugger' if Trace Hub is used with host debugger tool or 'Target Debugger' if Trace Hub is used by target debugger software or 'Disable' trace hub functionality.}
  gPlatformFspPkgTokenSpaceGuid.PchTraceHubMode              | * | 0x01 | 0x00

  # !BSF NAME:{PCH Trace Hub Memory Region 0 buffer Size} TYPE:{Combo} OPTION:{0:0, 1:1MB, 2:8MB, 3:64MB, 4:128MB, 5:256MB, 6:512MB}
  # !BSF HELP:{Specify size of Pch trace memory region 0 buffer, the size can be 0, 1MB, 8MB, 64MB, 128MB, 256MB, 512MB}
  gPlatformFspPkgTokenSpaceGuid.PchTraceHubMemReg0Size       | * | 0x01 | 0x00

  # !BSF NAME:{PCH Trace Hub Memory Region 1 buffer Size} TYPE:{Combo} OPTION:{0:0, 1:1MB, 2:8MB, 3:64MB, 4:128MB, 5:256MB, 6:512MB}
  # !BSF HELP:{Specify size of Pch trace memory region 1 buffer, the size can be 0, 1MB, 8MB, 64MB, 128MB, 256MB, 512MB}
  gPlatformFspPkgTokenSpaceGuid.PchTraceHubMemReg1Size       | * | 0x01 | 0x00

  # !BSF NAME:{HD Audio DMIC Link Clock Select} TYPE:{Combo} OPTION:{0: Both, 1: ClkA, 2: ClkB}
  # !BSF HELP:{Determines DMIC<N> Clock Source. 0: Both, 1: ClkA, 2: ClkB}
  gPlatformFspPkgTokenSpaceGuid.PchHdaAudioLinkDmicClockSelect  | * | 0x02 | { 0x00, 0x00 }

  # !BSF NAME:{PchPreMemRsvd} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Reserved for PCH Pre-Mem Reserved}
  gPlatformFspPkgTokenSpaceGuid.PchPreMemRsvd                | * | 0x05 | {0x00}

  # !BSF PAGE:{SA1}
  # !BSF NAME:{State of X2APIC_OPT_OUT bit in the DMAR table} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{0=Disable/Clear, 1=Enable/Set}
  gPlatformFspPkgTokenSpaceGuid.X2ApicOptOut                | * | 0x1 | 0x0

  # !BSF NAME:{State of DMA_CONTROL_GUARANTEE bit in the DMAR table} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{0=Disable/Clear, 1=Enable/Set}
  gPlatformFspPkgTokenSpaceGuid.DmaControlGuarantee         | * | 0x1 | 0x1

  # !BSF NAME:{Base addresses for VT-d function MMIO access} TYPE:{EditNum, HEX, (0, 0xFFFFFFFF)}
  # !HDR STRUCT:{UINT32}
  # !BSF HELP:{Base addresses for VT-d MMIO access per VT-d engine}
  gPlatformFspPkgTokenSpaceGuid.VtdBaseAddress              | * | 0x24 | {0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000}

  # !BSF NAME:{Disable VT-d} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{0=Enable/FALSE(VT-d enabled), 1=Disable/TRUE (VT-d disabled)}
  gPlatformFspPkgTokenSpaceGuid.VtdDisable                  | * | 0x1 | 0x0

  # !BSF NAME:{Vtd Programming for Igd} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{1=Enable/TRUE (Igd VT-d Bar programming enabled), 0=Disable/FLASE (Igd VT-d Bar programming disabled)}
  gPlatformFspPkgTokenSpaceGuid.VtdIgdEnable                  | * | 0x1 | 0x1

  # !BSF NAME:{Vtd Programming for Ipu} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{1=Enable/TRUE (Ipu VT-d Bar programming enabled), 0=Disable/FLASE (Ipu VT-d Bar programming disabled)}
  gPlatformFspPkgTokenSpaceGuid.VtdIpuEnable                  | * | 0x1 | 0x1

  # !BSF NAME:{Vtd Programming for Iop} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{1=Enable/TRUE (Iop VT-d Bar programming enabled), 0=Disable/FLASE (Iop VT-d Bar programming disabled)}
  gPlatformFspPkgTokenSpaceGuid.VtdIopEnable                  | * | 0x1 | 0x1

  # !BSF NAME:{Vtd Programming for ITbt} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{DEPRECATED}
  gPlatformFspPkgTokenSpaceGuid.VtdItbtEnable                  | * | 0x1 | 0x1

  # !BSF NAME:{Internal Graphics Pre-allocated Memory} TYPE:{Combo}
  # !BSF OPTION:{0x00:0MB, 0x01:32MB, 0x02:64MB, 0x03:96MB, 0x04:128MB, 0x05:160MB, 0xF0:4MB, 0xF1:8MB, 0xF2:12MB, 0xF3:16MB, 0xF4:20MB, 0xF5:24MB, 0xF6:28MB, 0xF7:32MB, 0xF8:36MB, 0xF9:40MB, 0xFA:44MB, 0xFB:48MB, 0xFC:52MB, 0xFD:56MB, 0xFE:60MB}
  # !BSF HELP:{Size of memory preallocated for internal graphics.}
  gPlatformFspPkgTokenSpaceGuid.IgdDvmt50PreAlloc           | * | 0x01 | 0xFE

  # !BSF NAME:{Internal Graphics} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Enable/disable internal graphics.}
  gPlatformFspPkgTokenSpaceGuid.InternalGfx                 | * | 0x01 | 0x01

  # !BSF NAME:{Aperture Size} TYPE:{Combo}
  # !BSF OPTION:{0:128 MB, 1:256 MB, 3:512 MB, 7:1024 MB, 15: 2048 MB}
  # !BSF HELP:{Select the Aperture Size.}
  gPlatformFspPkgTokenSpaceGuid.ApertureSize                | * | 0x01 | 0x01

  # !BSF PAGE:{MRC}
  # !BSF NAME:{Board Type}

  # !BSF TYPE:{Combo} OPTION:{0:Mobile, 1:Desktop1Dpc, 2:Desktop2DpcDaisyChain, 3:Desktop2DpcTeeTopologyAsymmetrical, 4:Desktop2DpcTeeTopology, 5:UltMobile, 7:UP Server}
  # !BSF HELP:{MrcBoardType, Options are 0:Mobile, 1:Desktop1Dpc, 2:Desktop2DpcDaisyChain, 3:Desktop2DpcTeeTopologyAsymmetrical, 4:Desktop2DpcTeeTopology, 5:UltMobile, 7:UP Server}
  gPlatformFspPkgTokenSpaceGuid.UserBd                      | * | 0x01 | 0x00

  # !BSF NAME:{DDR Frequency Limit}
  # !BSF TYPE:{Combo} OPTION:{1067:1067, 1333:1333, 1600:1600, 1867:1867, 2133:2133, 2400:2400, 2667:2667, 2933:2933, 0:Auto}
  # !BSF HELP:{Maximum Memory Frequency Selections in Mhz. Options are 1067, 1333, 1600, 1867, 2133, 2400, 2667, 2933 and 0 for Auto.}
  gPlatformFspPkgTokenSpaceGuid.DdrFreqLimit                | * | 0x02 | 0

!if gSiPkgTokenSpaceGuid.PcdAdlLpSupport == TRUE
  # !BSF NAME:{SA GV} TYPE:{Combo}
  # !BSF OPTION:{0:Disabled, 1:FixedPoint0, 2:FixedPoint1, 3:FixedPoint2, 4:FixedPoint3, 5:Enabled}
  # !BSF HELP:{System Agent dynamic frequency support and when enabled memory will be training at four different frequencies.}
  gPlatformFspPkgTokenSpaceGuid.SaGv                        | * | 0x01 | 0x05
!else
  # !BSF NAME:{SA GV} TYPE:{Combo}
  # !BSF OPTION:{0:Disabled, 1:FixedPoint0, 2:FixedPoint1, 3:FixedPoint2, 4:FixedPoint3, 5:Enabled}
  # !BSF HELP:{System Agent dynamic frequency support and when enabled memory will be training at four different frequencies.}
  gPlatformFspPkgTokenSpaceGuid.SaGv                        | * | 0x01 | 0x00
!endif

  # !BSF NAME:{Memory Test on Warm Boot}
  # !BSF TYPE:{Combo} OPTION:{0:Disable, 1:Enable}
  # !BSF HELP:{Run Base Memory Test on Warm Boot}
  gPlatformFspPkgTokenSpaceGuid.MemTestOnWarmBoot         | * | 0x01 | 0x01

  # !BSF NAME:{DDR Speed Control} TYPE:{Combo}
  # !BSF OPTION:{0:Auto, 1:Manual}
  # !BSF HELP:{DDR Frequency and Gear control for all SAGV points.}
  gPlatformFspPkgTokenSpaceGuid.DdrSpeedControl             | * | 0x01 | 0x00

  # !BSF NAME:{Rank Margin Tool}
  # !BSF TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Enable/disable Rank Margin Tool.}
  gPlatformFspPkgTokenSpaceGuid.RMT                         | * | 0x01 | 0x00

  # !BSF NAME:{Controller 0 Channel 0 DIMM Control}
  # !BSF TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Enable / Disable DIMMs on Controller 0 Channel 0}
  gPlatformFspPkgTokenSpaceGuid.DisableMc0Ch0               | * | 0x01 | 0x00

  # !BSF NAME:{Controller 0 Channel 1 DIMM Control}
  # !BSF TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Enable / Disable DIMMs on Controller 0 Channel 1}
  gPlatformFspPkgTokenSpaceGuid.DisableMc0Ch1               | * | 0x01 | 0x00

  # !BSF NAME:{Controller 0 Channel 2 DIMM Control}
  # !BSF TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Enable / Disable DIMMs on Controller 0 Channel 2}
  gPlatformFspPkgTokenSpaceGuid.DisableMc0Ch2               | * | 0x01 | 0x00

  # !BSF NAME:{Controller 0 Channel 3 DIMM Control}
  # !BSF TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Enable / Disable DIMMs on Controller 0 Channel 3}
  gPlatformFspPkgTokenSpaceGuid.DisableMc0Ch3               | * | 0x01 | 0x00

  # !BSF NAME:{Controller 1 Channel 0 DIMM Control}
  # !BSF TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Enable / Disable DIMMs on Controller 1 Channel 0}
  gPlatformFspPkgTokenSpaceGuid.DisableMc1Ch0               | * | 0x01 | 0x00

  # !BSF NAME:{Controller 1 Channel 1 DIMM Control}
  # !BSF TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Enable / Disable DIMMs on Controller 1 Channel 1}
  gPlatformFspPkgTokenSpaceGuid.DisableMc1Ch1               | * | 0x01 | 0x00

  # !BSF NAME:{Controller 1 Channel 2 DIMM Control}
  # !BSF TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Enable / Disable DIMMs on Controller 1 Channel 2}
  gPlatformFspPkgTokenSpaceGuid.DisableMc1Ch2               | * | 0x01 | 0x00

  # !BSF NAME:{Controller 1 Channel 3 DIMM Control}
  # !BSF TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Enable / Disable DIMMs on Controller 1 Channel 3}
  gPlatformFspPkgTokenSpaceGuid.DisableMc1Ch3               | * | 0x01 | 0x00

  # !BSF NAME:{Scrambler Support}
  # !BSF TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{This option enables data scrambling in memory.}
  gPlatformFspPkgTokenSpaceGuid.ScramblerSupport            | * | 0x01 | 0x1

  # !BSF NAME:{SPD Profile Selected}
  # !BSF TYPE:{Combo} OPTION:{0:Default SPD Profile, 1:Custom Profile, 2:XMP Profile 1, 3:XMP Profile 2, 4:XMP Profile 3, 5:XMP User Profile 4, 6:XMP User Profile 5}
  # !BSF HELP:{Select DIMM timing profile. Options are 0:Default SPD Profile, 1:Custom Profile, 2:XMP Profile 1, 3:XMP Profile 2, 4:XMP Profile 3, 5:XMP User Profile 4, 6:XMP User Profile 5}
  gPlatformFspPkgTokenSpaceGuid.SpdProfileSelected          | * | 0x01 | 0x00

  # !BSF NAME:{Memory Reference Clock}
  # !BSF TYPE:{Combo} OPTION:{0:133MHz, 1:100MHz}
  # !BSF HELP:{100MHz, 133MHz.}
  gPlatformFspPkgTokenSpaceGuid.RefClk                      | * | 0x01 | 0x00

  # !BSF NAME:{Memory Voltage}
  # !BSF TYPE:{EditNum, DEC, (0,1435)}
  # !BSF HELP:{DRAM voltage (Vdd) (supply voltage for input buffers and core logic of the DRAM chips) in millivolts from 0 - default to 1435mv.}
  gPlatformFspPkgTokenSpaceGuid.VddVoltage                  | * | 0x02 | 0x0000

  # !BSF NAME:{Memory Ratio}
  # !BSF TYPE:{Combo} OPTION:{0:Auto, 4:4, 5:5, 6:6, 7:7, 8:8, 9:9, 10:10, 11:11, 12:12, 13:13, 14:14, 15:15}
  # !BSF HELP:{Automatic or the frequency will equal ratio times reference clock. Set to Auto to recalculate memory timings listed below.}
  gPlatformFspPkgTokenSpaceGuid.Ratio                       | * | 0x01 | 0x00

  # !BSF NAME:{tCL}
  # !BSF TYPE:{EditNum, HEX, (0x00,0x1F)}
  # !BSF HELP:{CAS Latency, 0: AUTO, max: 31. Only used if FspmUpd->FspmConfig.SpdProfileSelected == 1 (Custom Profile).}
  gPlatformFspPkgTokenSpaceGuid.tCL                         | * | 0x01 | 0x00

  # !BSF NAME:{tCWL}
  # !BSF TYPE:{EditNum, HEX, (0x00,0x22)}
  # !BSF HELP:{Min CAS Write Latency Delay Time, 0: AUTO, max: 34. Only used if FspmUpd->FspmConfig.SpdProfileSelected == 1 (Custom Profile).}
  gPlatformFspPkgTokenSpaceGuid.tCWL                        | * | 0x01 | 0x00

  # !BSF NAME:{tFAW}
  # !BSF TYPE:{EditNum, HEX, (0x00,0x3F)}
  # !BSF HELP:{Min Four Activate Window Delay Time, 0: AUTO, max: 63. Only used if FspmUpd->FspmConfig.SpdProfileSelected == 1 (Custom Profile).}
  gPlatformFspPkgTokenSpaceGuid.tFAW                        | * | 0x02 | 0x0000

  # !BSF NAME:{tRAS}
  # !BSF TYPE:{EditNum, HEX, (0x00,0x40)}
  # !BSF HELP:{RAS Active Time, 0: AUTO, max: 64. Only used if FspmUpd->FspmConfig.SpdProfileSelected == 1 (Custom Profile).}
  gPlatformFspPkgTokenSpaceGuid.tRAS                        | * | 0x02 | 0x0000

  # !BSF NAME:{tRCD/tRP}
  # !BSF TYPE:{EditNum, HEX, (0x00,0x3F)}
  # !BSF HELP:{RAS to CAS delay time and Row Precharge delay time, 0: AUTO, max: 63. Only used if FspmUpd->FspmConfig.SpdProfileSelected == 1 (Custom Profile).}
  gPlatformFspPkgTokenSpaceGuid.tRCDtRP                     | * | 0x01 | 0x00

  # !BSF NAME:{tREFI}
  # !BSF TYPE:{EditNum, HEX, (0x00,0xFFFF)}
  # !BSF HELP:{Refresh Interval, 0: AUTO, max: 65535. Only used if FspmUpd->FspmConfig.SpdProfileSelected == 1 (Custom Profile).}
  gPlatformFspPkgTokenSpaceGuid.tREFI                       | * | 0x02 | 0x0000

  # !BSF NAME:{tRFC}
  # !BSF TYPE:{EditNum, HEX, (0x00,0x3FF)}
  # !BSF HELP:{Min Refresh Recovery Delay Time, 0: AUTO, max: 1023. Only used if FspmUpd->FspmConfig.SpdProfileSelected == 1 (Custom Profile).}
  gPlatformFspPkgTokenSpaceGuid.tRFC                        | * | 0x02 | 0x0000

  # !BSF NAME:{tRRD}
  # !BSF TYPE:{EditNum, HEX, (0x00,0x0F)}
  # !BSF HELP:{Min Row Active to Row Active Delay Time, 0: AUTO, max: 15. Only used if FspmUpd->FspmConfig.SpdProfileSelected == 1 (Custom Profile).}
  gPlatformFspPkgTokenSpaceGuid.tRRD                        | * | 0x01 | 0x00

  # !BSF NAME:{tRTP}
  # !BSF TYPE:{EditNum, HEX, (0x00,0x0F)}
  # !BSF HELP:{Min Internal Read to Precharge Command Delay Time, 0: AUTO, max: 15. DDR4 legal values: 5, 6, 7, 8, 9, 10, 12. Only used if FspmUpd->FspmConfig.SpdProfileSelected == 1 (Custom Profile).}
  gPlatformFspPkgTokenSpaceGuid.tRTP                        | * | 0x01 | 0x00

  # !BSF NAME:{tWR}
  # !BSF TYPE:{Combo} OPTION:{0:Auto, 5:5, 6:6, 7:7, 8:8, 10:10, 12:12, 14:14, 16:16, 18:18, 20:20, 24:24, 30:30, 34:34, 40:40}
  # !BSF HELP:{Min Write Recovery Time, 0: AUTO, legal values: 5, 6, 7, 8, 10, 12, 14, 16, 18, 20, 24, 30, 34, 40. Only used if FspmUpd->FspmConfig.SpdProfileSelected == 1 (Custom Profile).}
  gPlatformFspPkgTokenSpaceGuid.tWR                         | * | 0x01 | 0x00

  # !BSF NAME:{tWTR}
  # !BSF TYPE:{EditNum, HEX, (0x00,0x1C)}
  # !BSF HELP:{Min Internal Write to Read Command Delay Time, 0: AUTO, max: 28. Only used if FspmUpd->FspmConfig.SpdProfileSelected == 1 (Custom Profile).}
  gPlatformFspPkgTokenSpaceGuid.tWTR                        | * | 0x01 | 0x00

  # !BSF NAME:{NMode}
  # !BSF TYPE:{EditNum, HEX, (0x00,0x02)}
  # !BSF HELP:{System command rate, range 0-2, 0 means auto, 1 = 1N, 2 = 2N}
  gPlatformFspPkgTokenSpaceGuid.NModeSupport                | * | 0x01 | 0x00

  # !BSF PAGE:{PCH1}
  # !BSF NAME:{Enable Intel HD Audio (Azalia)} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{0: Disable, 1: Enable (Default) Azalia controller}
  gPlatformFspPkgTokenSpaceGuid.PchHdaEnable                | * | 0x01 | 0x01

  # !BSF NAME:{Enable PCH ISH Controller} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{0: Disable, 1: Enable (Default) ISH Controller}
  gPlatformFspPkgTokenSpaceGuid.PchIshEnable                | * | 0x01 | 0x01

  # !BSF PAGE:{CPU1}
  # !BSF NAME:{CPU Trace Hub Mode} TYPE:{Combo}
  # !BSF OPTION:{0: Disable, 1:Target Debugger Mode, 2:Host Debugger Mode}
  # !BSF HELP:{Select 'Host Debugger' if Trace Hub is used with host debugger tool or 'Target Debugger' if Trace Hub is used by target debugger software or 'Disable' trace hub functionality.}
  gPlatformFspPkgTokenSpaceGuid.CpuTraceHubMode             | * | 0x01 | 0x00

  # !BSF NAME:{CPU Trace Hub Memory Region 0} TYPE:{Combo} OPTION:{0:0, 1:1MB, 2:8MB, 3:64MB, 4:128MB, 5:256MB, 6:512MB}
  # !BSF HELP:{CPU Trace Hub Memory Region 0, The avaliable memory size is : 0MB, 1MB, 8MB, 64MB, 128MB, 256MB, 512MB}
  gPlatformFspPkgTokenSpaceGuid.CpuTraceHubMemReg0Size      | * | 0x01 | 0x00

  # !BSF NAME:{CPU Trace Hub Memory Region 1} TYPE:{Combo} OPTION:{0:0, 1:1MB, 2:8MB, 3:64MB, 4:128MB, 5:256MB, 6:512MB}
  # !BSF HELP:{CPU Trace Hub Memory Region 1. The avaliable memory size is : 0MB, 1MB, 8MB, 64MB, 128MB, 256MB, 512MB}
  gPlatformFspPkgTokenSpaceGuid.CpuTraceHubMemReg1Size      | * | 0x01 | 0x00

  # !BSF PAGE:{MRC}
  # !BSF NAME:{SAGV Gear Ratio} TYPE:{EditNum, HEX, (0x00,0xFF)}
  # !BSF STRUCT:{UINT8}
  # !BSF HELP:{Gear Selection for SAGV points. 0 - Auto, 1-1 Gear 1, 2-Gear 2}
  gPlatformFspPkgTokenSpaceGuid.SaGvGear                | * | 0x04 | {0x00, 0x00, 0x00, 0x00}

  # !BSF NAME:{SAGV Frequency} TYPE:{EditNum, HEX, (0x00, 0xFFFF)}
  # !HDR STRUCT:{UINT16}
  # !BSF HELP:{SAGV Frequency per point in Mhz. 0 for Auto and a ratio of 133/100MHz: 1333/1300.}
  gPlatformFspPkgTokenSpaceGuid.SaGvFreq                 | * | 0x08 | {0x0000, 0x0000, 0x0000, 0x0000}

  # !BSF NAME:{SAGV Disabled Gear Ratio} TYPE:{EditNum, HEX, (0x00,0xFF)}
  # !BSF STRUCT:{UINT8}
  # !BSF HELP:{Gear Selection for SAGV Disabled. 0 - Auto, 1-1 Gear 1, 2-Gear 2}
  gPlatformFspPkgTokenSpaceGuid.GearRatio                | * | 0x01 | 0x00

  #
  # ME Pre-Mem Configuration Block Start
  #
  # !BSF PAGE:{ME1}
  # !BSF NAME:{HECI Timeouts} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{0: Disable, 1: Enable (Default) timeout check for HECI}
  gPlatformFspPkgTokenSpaceGuid.HeciTimeouts                | * | 0x01 | 0x01

  # !BSF NAME:{HECI1 BAR address}  TYPE:{EditNum, HEX, (0x00,0xFFFFFFFFF)}
  # !BSF HELP:{BAR address of HECI1}
  gPlatformFspPkgTokenSpaceGuid.Heci1BarAddress             | * | 0x04 | 0xFEDA2000

  # !BSF NAME:{HECI2 BAR address}  TYPE:{EditNum, HEX, (0x00,0xFFFFFFFFF)}
  # !BSF HELP:{BAR address of HECI2}
  gPlatformFspPkgTokenSpaceGuid.Heci2BarAddress             | * | 0x04 | 0xFEDA3000

  # !BSF NAME:{HECI3 BAR address}  TYPE:{EditNum, HEX, (0x00,0xFFFFFFFFF)}
  # !BSF HELP:{BAR address of HECI3}
  gPlatformFspPkgTokenSpaceGuid.Heci3BarAddress             | * | 0x04 | 0xFEDA4000

  #
  # ME Pre-Mem Configuration Block End
  #

  #
  # SA Pre-Mem Production Block Start
  #
  # !BSF PAGE:{SA1}
  # !BSF NAME:{HG dGPU Power Delay} TYPE:{EditNum, HEX, (0x00, 0x3E8)}
  # !BSF HELP:{HG dGPU delay interval after power enabling: 0=Minimal, 1000=Maximum, default is 300=300 microseconds}
  gPlatformFspPkgTokenSpaceGuid.HgDelayAfterPwrEn           | * | 0x02 | 300

  # !BSF NAME:{HG dGPU Reset Delay} TYPE:{EditNum, HEX, (0x00, 0x3E8)}
  # !BSF HELP:{HG dGPU delay interval for Reset complete: 0=Minimal, 1000=Maximum, default is 100=100 microseconds}
  gPlatformFspPkgTokenSpaceGuid.HgDelayAfterHoldReset       | * | 0x02 | 100

  # !BSF NAME:{MMIO size adjustment for AUTO mode} TYPE:{EditNum, HEX, (0, 0xFFFF)}
  # !BSF HELP:{Positive number means increasing MMIO size, Negative value means decreasing MMIO size: 0 (Default)=no change to AUTO mode MMIO size}
  gPlatformFspPkgTokenSpaceGuid.MmioSizeAdjustment          | * | 0x02 | 0

  # !BSF NAME:{PCIe ASPM programming will happen in relation to the Oprom} TYPE:{Combo}
  # !BSF OPTION:{0:Before, 1:After}
  # !BSF HELP:{Select when PCIe ASPM programming will happen in relation to the Oprom. Before(0x0)(Default): Do PCIe ASPM programming before Oprom, After(0x1): Do PCIe ASPM programming after Oprom, requires an SMI handler to save/restore ASPM settings during S3 resume}
  gPlatformFspPkgTokenSpaceGuid.InitPcieAspmAfterOprom      | * | 0x01 | 0x0

  # !BSF NAME:{Selection of the primary display device} TYPE:{Combo}
  # !BSF OPTION:{0:iGFX, 1:PEG, 2:PCIe Graphics on PCH, 3:AUTO, 4:Hybrid Graphics}
  # !BSF HELP:{0=iGFX, 1=PEG, 2=PCIe Graphics on PCH, 3(Default)=AUTO, 4=Hybrid Graphics}
  gPlatformFspPkgTokenSpaceGuid.PrimaryDisplay              | * | 0x01 | 0x3

  # !BSF NAME:{Selection of PSMI Region size} TYPE:{Combo}
  # !BSF OPTION:{0:32MB, 1:288MB, 2:544MB, 3:800MB, 4:1024MB}
  # !BSF HELP:{0=32MB, 1=288MB, 2=544MB, 3=800MB, 4=1024MB Default is 0}
  gPlatformFspPkgTokenSpaceGuid.PsmiRegionSize              | * | 0x01 | 0x0

  # !BSF NAME:{Temporary MMIO address for GMADR} TYPE:{EditNum, HEX, (0x00, 0xFFFFFFFF)}
  # !BSF HELP:{Obsolete field now and it has been extended to 64 bit address, used GmAdr64 }
  gPlatformFspPkgTokenSpaceGuid.GmAdr                       | * | 0x4 | 0xB0000000

  # !BSF NAME:{Temporary MMIO address for GTTMMADR} TYPE:{EditNum, HEX, (0x00, 0xFFFFFFFF)}
  # !BSF HELP:{The reference code will use this as Temporary MMIO address space to access GTTMMADR Registers.Platform should provide conflict free Temporary MMIO Range: GttMmAdr to (GttMmAdr + 2MB MMIO + 6MB Reserved + GttSize). Default is (GmAdr - (2MB MMIO + 6MB Reserved + GttSize)) to (GmAdr - 0x1) (Where GttSize = 8MB)}
  gPlatformFspPkgTokenSpaceGuid.GttMmAdr                    | * | 0x4 | 0xAF000000

  # !BSF NAME:{Selection of iGFX GTT Memory size} TYPE:{Combo}
  # !BSF OPTION:{1:2MB, 2:4MB, 3:8MB}
  # !BSF HELP:{1=2MB, 2=4MB, 3=8MB, Default is 3}
  gPlatformFspPkgTokenSpaceGuid.GttSize                     | * | 0x02 | 0x3

  # !BSF NAME:{Hybrid Graphics GPIO information for PEG 0} TYPE:{EditNum, HEX, (0x00, 0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF)}
  # !HDR STRUCT:{UINT8}
  # !BSF HELP:{Hybrid Graphics GPIO information for PEG 0, for Reset, power and wake GPIOs}
  gPlatformFspPkgTokenSpaceGuid.CpuPcie0Rtd3Gpio             | * | 0x18 | {0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0}

  # !BSF PAGE:{MRC}
  # !BSF NAME:{Enable/Disable MRC TXT dependency} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{When enabled MRC execution will wait for TXT initialization to be done first. Disabled(0x0)(Default): MRC will not wait for TXT initialization, Enabled(0x1): MRC will wait for TXT initialization}
  gPlatformFspPkgTokenSpaceGuid.TxtImplemented              | * | 0x01 | 0x1

  # !BSF PAGE:{SA1}
  # !BSF NAME:{Enable/Disable SA OcSupport} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Enable: Enable SA OcSupport, Disable(Default): Disable SA OcSupport}
  gPlatformFspPkgTokenSpaceGuid.SaOcSupport                 | * | 0x01 | 0x0

  # !BSF NAME:{GT slice Voltage Mode} TYPE:{Combo}
  # !BSF OPTION:{0: Adaptive, 1: Override}
  # !BSF HELP:{0(Default): Adaptive, 1: Override}
  gPlatformFspPkgTokenSpaceGuid.GtVoltageMode               | * | 0x01 | 0x0

  # !BSF NAME:{Maximum GTs turbo ratio override} TYPE:{EditNum, HEX, (0x00, 0x3C)}
  # !BSF HELP:{0(Default)=Minimal/Auto, 60=Maximum}
  gPlatformFspPkgTokenSpaceGuid.GtMaxOcRatio                | * | 0x1 | 0

  # !BSF NAME:{The voltage offset applied to GT slice} TYPE:{EditNum, HEX, (0x00, 0x3E8)}
  # !BSF HELP:{0(Default)=Minimal, 1000=Maximum}
  gPlatformFspPkgTokenSpaceGuid.GtVoltageOffset             | * | 0x2 | 0

  # !BSF NAME:{The GT slice voltage override which is applied to the entire range of GT frequencies} TYPE:{EditNum, HEX, (0x00, 0x7D0)}
  # !BSF HELP:{0(Default)=Minimal, 2000=Maximum}
  gPlatformFspPkgTokenSpaceGuid.GtVoltageOverride           | * | 0x2 | 0

  # !BSF NAME:{adaptive voltage applied during turbo frequencies} TYPE:{EditNum, HEX, (0x00, 0x7D0)}
  # !BSF HELP:{0(Default)=Minimal, 2000=Maximum}
  gPlatformFspPkgTokenSpaceGuid.GtExtraTurboVoltage         | * | 0x2 | 0

  # !BSF NAME:{voltage offset applied to the SA} TYPE:{EditNum, HEX, (0x00, 0x3E8)}
  # !BSF HELP:{0(Default)=Minimal, 1000=Maximum}
  gPlatformFspPkgTokenSpaceGuid.SaVoltageOffset             | * | 0x2 | 0

  # !BSF NAME:{PCIe root port Function number for Hybrid Graphics dGPU} TYPE:{EditNum, HEX, (0x00, 0xFF)}
  # !BSF HELP:{Root port Index number to indicate which PCIe root port has dGPU}
  gPlatformFspPkgTokenSpaceGuid.RootPortIndex               | * | 0x1 | 0x0

  # !BSF PAGE:{MRC}
  # !BSF NAME:{Realtime Memory Timing} TYPE:{Combo}
  # !BSF OPTION:{0: Disabled, 1: Enabled}
  # !BSF HELP:{0(Default): Disabled, 1: Enabled. When enabled, it will allow the system to perform realtime memory timing changes after MRC_DONE.}
  gPlatformFspPkgTokenSpaceGuid.RealtimeMemoryTiming        | * | 0x01 | 0x0

  # !BSF PAGE:{TCSS1}
  # !BSF NAME:{iTBT PCIe Multiple Segment setting}
  # !BSF OPTION:{$EN_DIS}
  # !BSF HELP:{DEPRECATED}
  gPlatformFspPkgTokenSpaceGuid.PcieMultipleSegmentEnabled  | * | 0x1 | 0x0

  # !BSF PAGE:{SA1}
  # !BSF NAME:{Enable/Disable SA IPU} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Enable(Default): Enable SA IPU, Disable: Disable SA IPU}
  gPlatformFspPkgTokenSpaceGuid.SaIpuEnable                | * | 0x01 | 0x1

  # !BSF NAME:{Lane Used of CSI port} TYPE:{Combo}
  # !BSF OPTION:{ 1:x1, 2:x2, 3:x3, 4:x4, 8:x8}
  # !BSF HELP:{ Lane Used of each CSI port}
  gPlatformFspPkgTokenSpaceGuid.IpuLaneUsed                 | * | 0x08 | { 0x04, 0x01, 0x02, 0x04, 0x04, 0x04, 0x00, 0x00 }

!if gSiPkgTokenSpaceGuid.PcdAdlLpSupport == TRUE
  # !BSF NAME:{Lane Used of CSI port} TYPE:{Combo}
  # !BSF OPTION:{ 0:Sensor default, 1:<416Mbps, 2:<1.5Gbps, 3:<2Gbps, 4:<2.5Gbps, 5:<4Gbps, 6:>4Gbps}
  # !BSF HELP:{ Speed of each CSI port}
  gPlatformFspPkgTokenSpaceGuid.CsiSpeed                    | * | 0x08 | { 0x2, 0x1, 0x2, 0x2, 0x2, 0x2, 0x0, 0x0 }
!else
  # !BSF NAME:{Lane Used of CSI port} TYPE:{Combo}
  # !BSF OPTION:{ 0:Sensor default, 1:<416Mbps, 2:<1.5Gbps, 3:<2Gbps, 4:<2.5Gbps, 5:<4Gbps, 6:>4Gbps}
  # !BSF HELP:{ Speed of each CSI port}
  gPlatformFspPkgTokenSpaceGuid.CsiSpeed                    | * | 0x08 | { 0x2, 0x2, 0x2, 0x2, 0x2, 0x2, 0x0, 0x0 }
!endif

  # !BSF NAME:{IMGU CLKOUT Configuration}
  # !BSF OPTION:{$EN_DIS}
  # !BSF HELP:{The configuration of IMGU CLKOUT, 0: Disable;<b>1: Enable</b>.}
  gPlatformFspPkgTokenSpaceGuid.ImguClkOutEn                | * | 0x06 | {0x1, 0x1, 0x1, 0x1, 0x0, 0x0}

  # !BSF NAME:{Enable PCIE RP Mask} TYPE:{EditNum, HEX, (0x00,0x00FFFFFF)}
  # !BSF HELP:{Enable/disable PCIE Root Ports. 0: disable, 1: enable. One bit for each port, bit0 for port1, bit1 for port2, and so on.}
  gPlatformFspPkgTokenSpaceGuid.CpuPcieRpEnableMask         | * | 0x04 | 0x0

  # !BSF NAME:{Assertion on Link Down GPIOs} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF OPTION:{0:Disable, 1:Enable}
  # !BSF HELP:{GPIO Assertion on Link Down. Disabled(0x0)(Default): Disable assertion on Link Down GPIOs, Enabled(0x1): Enable assertion on Link Down GPIOs}
  gPlatformFspPkgTokenSpaceGuid.CpuPcieRpLinkDownGpios      | * | 0x01 | 0x0

  # !BSF NAME:{Enable ClockReq Messaging } TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF OPTION:{0:Disable, 1:Enable}
  # !BSF HELP:{ClockReq Messaging. Disabled(0x0): Disable ClockReq Messaging, Enabled(0x1)(Default): Enable ClockReq Messaging}
  gPlatformFspPkgTokenSpaceGuid.CpuPcieRpClockReqMsgEnable  | * | 0x03 | { 0x1, 0x1, 0x1 }

  # !BSF NAME:{PCIE RP Pcie Speed} TYPE:{EditNum, HEX, (0x00,0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF)}
  # !BSF HELP:{Determines each PCIE Port speed capability. 0: Auto; 1: Gen1; 2: Gen2; 3: Gen3; 4: Gen4 (see: CPU_PCIE_SPEED).}
  gPlatformFspPkgTokenSpaceGuid.CpuPcieRpPcieSpeed             | * | 0x4 | { 0x00, 0x00, 0x00, 0x00 }

  # !BSF NAME:{Selection of PSMI Support On/Off}
  # !BSF TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{0(Default) = FALSE, 1 = TRUE. When TRUE, it will allow the PSMI Support}
  gPlatformFspPkgTokenSpaceGuid.GtPsmiSupport              | * | 0x01 | 0x0

  # !BSF NAME:{Program GPIOs for LFP on DDI port-A device} TYPE:{Combo}
  # !BSF OPTION:{0:Disabled, 1:eDP, 2:MIPI DSI}
  # !BSF HELP:{0=Disabled,1(Default)=eDP, 2=MIPI DSI}
  gPlatformFspPkgTokenSpaceGuid.DdiPortAConfig              | * | 0x1 | 0x1

  # !BSF NAME:{Program GPIOs for LFP on DDI port-B device} TYPE:{Combo}
  # !BSF OPTION:{0:Disabled, 1:eDP, 2:MIPI DSI}
  # !BSF HELP:{0(Default)=Disabled,1=eDP, 2=MIPI DSI}
  gPlatformFspPkgTokenSpaceGuid.DdiPortBConfig              | * | 0x1 | 0x0

  # !BSF NAME:{Enable or disable HPD of DDI port A} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{0(Default)=Disable, 1=Enable}
  gPlatformFspPkgTokenSpaceGuid.DdiPortAHpd                 | * | 0x1 | 0x0

  # !BSF NAME:{Enable or disable HPD of DDI port B} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{0=Disable, 1(Default)=Enable}
  gPlatformFspPkgTokenSpaceGuid.DdiPortBHpd                 | * | 0x1 | 0x1

  # !BSF NAME:{Enable or disable HPD of DDI port C} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{0(Default)=Disable, 1=Enable}
  gPlatformFspPkgTokenSpaceGuid.DdiPortCHpd                 | * | 0x1 | 0x0

  # !BSF NAME:{Enable or disable HPD of DDI port 1} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{0=Disable, 1(Default)=Enable}
  gPlatformFspPkgTokenSpaceGuid.DdiPort1Hpd                 | * | 0x1 | 0x0

  # !BSF NAME:{Enable or disable HPD of DDI port 2} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{0(Default)=Disable, 1=Enable}
  gPlatformFspPkgTokenSpaceGuid.DdiPort2Hpd                 | * | 0x1 | 0x0

  # !BSF NAME:{Enable or disable HPD of DDI port 3} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{0(Default)=Disable, 1=Enable}
  gPlatformFspPkgTokenSpaceGuid.DdiPort3Hpd                 | * | 0x1 | 0x0

  # !BSF NAME:{Enable or disable HPD of DDI port 4} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{0(Default)=Disable, 1=Enable}
  gPlatformFspPkgTokenSpaceGuid.DdiPort4Hpd                 | * | 0x1 | 0x0

  # !BSF NAME:{Enable or disable DDC of DDI port A} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{0(Default)=Disable, 1=Enable}
  gPlatformFspPkgTokenSpaceGuid.DdiPortADdc                 | * | 0x1 | 0x0

  # !BSF NAME:{Enable or disable DDC of DDI port B} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{0=Disable, 1(Default)=Enable}
  gPlatformFspPkgTokenSpaceGuid.DdiPortBDdc                 | * | 0x1 | 0x1

  # !BSF NAME:{Enable or disable DDC of DDI port C} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{0(Default)=Disable, 1=Enable}
  gPlatformFspPkgTokenSpaceGuid.DdiPortCDdc                 | * | 0x1 | 0x0

  # !BSF NAME:{Enable DDC setting of DDI Port 1} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{0(Default)=Disable, 1=Enable}
  gPlatformFspPkgTokenSpaceGuid.DdiPort1Ddc                 | * | 0x1 | 0x0

  # !BSF NAME:{Enable DDC setting of DDI Port 2} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{0(Default)=Disable, 1=Enable}
  gPlatformFspPkgTokenSpaceGuid.DdiPort2Ddc                 | * | 0x1 | 0x0

  # !BSF NAME:{Enable DDC setting of DDI Port 3} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{0(Default)=Disable, 1=Enable}
  gPlatformFspPkgTokenSpaceGuid.DdiPort3Ddc                 | * | 0x1 | 0x0

  # !BSF NAME:{Enable DDC setting of DDI Port 4} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{0(Default)=Disable, 1=Enable}
  gPlatformFspPkgTokenSpaceGuid.DdiPort4Ddc                 | * | 0x1 | 0x0

  # !BSF NAME:{Temporary MMIO address for GMADR} TYPE:{EditNum, HEX, (0x00, 0xFFFFFFFF)}
  # !BSF HELP:{The reference code will use this as Temporary MMIO address space to access GMADR Registers.Platform should provide conflict free Temporary MMIO Range: GmAdr to (GmAdr + ApertureSize). Default is (PciExpressBaseAddress - ApertureSize) to (PciExpressBaseAddress - 0x1) (Where ApertureSize = 256MB, 512MB, 1024MB and 2048MB)}
  gPlatformFspPkgTokenSpaceGuid.GmAdr64                     | * | 0x8 | 0xB0000000

  # !BSF NAME:{Per-core HT Disable}
  # !BSF TYPE:{EditNum, HEX, (0x00, 0x7F)}
  # !BSF HELP:{Defines the per-core HT disable mask where: 1 - Disable selected logical core HT, 0 - is ignored. Input is in HEX and each bit maps to a logical core. Ex. A value of '1F' would disable HT for cores 4,3,2,1 and 0. Default is 0, all cores have HT enabled. Range is 0 - 0x7F for max 8 cores. You can only disable up to MAX_CORE_COUNT - 1.}
  gPlatformFspPkgTokenSpaceGuid.PerCoreHtDisable            | * | 0x02 | 0x0000

  # !BSF NAME:{SA/Uncore voltage mode} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{SA/Uncore voltage mode; <b>0: Adaptive</b>; 1: Override.}
  gPlatformFspPkgTokenSpaceGuid.SaVoltageMode               | * | 0x01 | 0x00

  # !BSF NAME:{SA/Uncore Voltage Override}
  # !BSF TYPE:{EditNum, HEX, (0x00, 0x7D0)}
  # !BSF HELP:{The SA/Uncore voltage override applicable when SA/Uncore voltage mode is in Override mode. Valid Range 0 to 2000}
  gPlatformFspPkgTokenSpaceGuid.SaVoltageOverride           | * | 0x02 | 0x00

  # !BSF NAME:{SA/Uncore Extra Turbo voltage}
  # !BSF TYPE:{EditNum, HEX, (0x00, 0x7D0)}
  # !BSF HELP:{Extra Turbo voltage applicable when SA/Uncore voltage mode is in Adaptive mode. Valid Range 0 to 2000}
  gPlatformFspPkgTokenSpaceGuid.SaExtraTurboVoltage         | * | 0x02 | 0x00

  # !BSF NAME:{Thermal Velocity Boost Ratio clipping} TYPE:{Combo}
  # !BSF OPTION:{0: Disabled, 1: Enabled}
  # !BSF HELP:{0(Default): Disabled, 1: Enabled. This service controls Core frequency reduction caused by high package temperatures for processors that implement the Intel Thermal Velocity Boost (TVB) feature}
  gPlatformFspPkgTokenSpaceGuid.TvbRatioClipping            | * | 0x01 | 0x0

  # !BSF NAME:{Thermal Velocity Boost voltage optimization} TYPE:{Combo}
  # !BSF OPTION:{0: Disabled, 1: Enabled}
  # !BSF HELP:{0: Disabled, 1: Enabled(Default). This service controls thermal based voltage optimizations for processors that implement the Intel Thermal Velocity Boost (TVB) feature.}
  gPlatformFspPkgTokenSpaceGuid.TvbVoltageOptimization      | * | 0x01 | 0x1

  # !BSF NAME:{Enable/Disable Display Audio Link in Pre-OS} TYPE:{Combo}
  # !BSF OPTION:{0: Disabled, 1: Enabled}
  # !BSF HELP:{0(Default)= Disable, 1 = Enable}
  gPlatformFspPkgTokenSpaceGuid.DisplayAudioLink            | * | 0x01 | 0x0

  # !BSF PAGE:{MRC}
  # !BSF NAME:{Memory VDDQ Voltage}
  # !BSF TYPE:{EditNum, DEC, (0,1435)}
  # !BSF HELP:{DRAM voltage (Vddq) (supply voltage for DQ/DQS of the DRAM chips) in millivolts from 0 - default to 1435mv.}
  gPlatformFspPkgTokenSpaceGuid.VddqVoltage                 | * | 0x02 | 0x0000

  # !BSF PAGE:{MRC}
  # !BSF NAME:{Memory VPP Voltage}
  # !BSF TYPE:{EditNum, DEC, (0,2135)}
  # !BSF HELP:{DRAM voltage (Vpp) (supply voltage for VPP of the DRAM chips) in millivolts from 0 - default to 2135mv.}
  gPlatformFspPkgTokenSpaceGuid.VppVoltage                  | * | 0x02 | 0x0000

  # !BSF NAME:{CPU PCIe New FOM} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Enable/Disable NewFom for DEKEL Programming. 0: Disable(Default); 1: Enable}
  gPlatformFspPkgTokenSpaceGuid.CpuPcieNewFom               | * | 0x4 | { 0x00, 0x00, 0x00, 0x00 }

  # !BSF NAME:{DMI DEKEL New FOM} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Enable/Disable NewFom for DEKEL Programming. 0: Disable(Default); 1: Enable}
  gPlatformFspPkgTokenSpaceGuid.DmiNewFom                   | * | 0x1 | 0x0

  # !BSF PAGE:{MRC}
  # !BSF NAME:{Dynamic Memory Boost} TYPE:{Combo}
  # !BSF OPTION:{$EN_DIS}
  # !BSF HELP:{0(Default): Disable, 1: Enable. When enabled, MRC will train the Default SPD Profile, and also the profile selected by SpdProfileSelected, to allow automatic switching during runtime. Only valid if SpdProfileSelected is an XMP Profile, otherwise ignored.}
  gPlatformFspPkgTokenSpaceGuid.DynamicMemoryBoost          | * | 0x01 | 0x0

  # !BSF NAME:{Hybrid Graphics Support } TYPE:{EditNum, HEX, (0x00, 0xFF)}
  # !BSF HELP:{0(Default): PEG10, 1: PEG60, 2:PEG62. Help to select Hybrid Graphics Support on Peg Port}
  gPlatformFspPkgTokenSpaceGuid.HgSupport                   | * | 0x1 | 0x0

  # !BSF PAGE:{MRC}
  # !BSF NAME:{Realtime Memory Frequency} TYPE:{Combo}
  # !BSF OPTION:{$EN_DIS}
  # !BSF HELP:{0(Default): Disabled, 1: Enabled. Ignored unless SpdProfileSelected is an XMP Profile. If enabled, MRC will train the Default SPD Profile, and also the selected XMP Profile, to allow manually triggered switching between frequencies at runtime.}
  gPlatformFspPkgTokenSpaceGuid.RealtimeMemoryFrequency     | * | 0x01 | 0x0

  # !BSF NAME:{SaPreMemProductionRsvd} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Reserved for SA Pre-Mem Production}
  gPlatformFspPkgTokenSpaceGuid.SaPreMemProductionRsvd      | * | 0x61 | {0x00}

  # !BSF NAME:{Enable Gt CLOS}
  # !BSF TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{0(Default)=Disable, 1=Enable}
  gPlatformFspPkgTokenSpaceGuid.GtClosEnable               | * | 0x01 | 0x0


#
# DMI IP Block
#

  # !BSF NAME:{DMI Max Link Speed} TYPE:{Combo}
  # !BSF OPTION:{0:Auto, 1:Gen1, 2:Gen2, 3:Gen3}
  # !BSF HELP:{Auto (Default)(0x0): Maximum possible link speed, Gen1(0x1): Limit Link to Gen1 Speed, Gen2(0x2): Limit Link to Gen2 Speed, Gen3(0x3):Limit Link to Gen3 Speed}
  gPlatformFspPkgTokenSpaceGuid.DmiMaxLinkSpeed             | * | 0x01 | 0x01

  # !BSF NAME:{DMI Equalization Phase 2} TYPE:{Combo}
  # !BSF OPTION:{0:Disable phase2, 1:Enable phase2, 2:Auto}
  # !BSF HELP:{DMI Equalization Phase 2. (0x0): Disable phase 2, (0x1): Enable phase 2, (0x2)(Default): AUTO - Use the current default method}
  gPlatformFspPkgTokenSpaceGuid.DmiGen3EqPh2Enable          | * | 0x01 | 0x02

  # !BSF NAME:{DMI Gen3 Equalization Phase3} TYPE:{Combo}
  # !BSF OPTION:{0:Auto, 1:HwEq, 2:SwEq, 3:StaticEq, 4:BypassPhase3}
  # !BSF HELP:{DMI Gen3 Equalization Phase3. Auto(0x0)(Default): Use the current default method, HwEq(0x1): Use Adaptive Hardware Equalization, SwEq(0x2): Use Adaptive Software Equalization (Implemented in BIOS Reference Code), Static(0x3): Use the Static EQs provided in DmiGen3EndPointPreset array for Phase1 AND Phase3 (Instead of just Phase1), Disabled(0x4): Bypass Equalization Phase 3}
  gPlatformFspPkgTokenSpaceGuid.DmiGen3EqPh3Method          | * | 0x01 | 0x0

  # !BSF NAME:{Enable/Disable DMI GEN3 Static EQ Phase1 programming} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Program DMI Gen3 EQ Phase1 Static Presets. Disabled(0x0): Disable EQ Phase1 Static Presets Programming, Enabled(0x1)(Default): Enable  EQ Phase1 Static Presets Programming}
  gPlatformFspPkgTokenSpaceGuid.DmiGen3ProgramStaticEq      | * | 0x01 | 0x1

  # !BSF NAME:{DeEmphasis control for DMI} TYPE:{Combo}
  # !BSF OPTION:{0: -6dB, 1: -3.5dB}
  # !BSF HELP:{DeEmphasis control for DMI. 0=-6dB, 1(Default)=-3.5 dB}
  gPlatformFspPkgTokenSpaceGuid.DmiDeEmphasis               | * | 0x01 | 0x1

  # !BSF NAME:{DMI Gen3 Root port preset values per lane} TYPE:{EditNum, HEX, (0x00, 0xFFFFFFFF)}
  # !HDR STRUCT:{UINT8}
  # !BSF HELP:{Used for programming DMI Gen3 preset values per lane. Range: 0-9, 8 is default for each lane}
  gPlatformFspPkgTokenSpaceGuid.DmiGen3RootPortPreset       | * | 0x8 | {0x08, 0x08, 0x08, 0x08, 0x08, 0x08, 0x08, 0x08}

  # !BSF NAME:{DMI Gen3 End port preset values per lane} TYPE:{EditNum, HEX, (0x00, 0xFFFFFFFF)}
  # !HDR STRUCT:{UINT8}
  # !BSF HELP:{Used for programming DMI Gen3 preset values per lane. Range: 0-9, 7 is default for each lane}
  gPlatformFspPkgTokenSpaceGuid.DmiGen3EndPointPreset       | * | 0x8 | {0x7, 0x7, 0x7, 0x7, 0x7, 0x7, 0x7, 0x7}

  # !BSF NAME:{DMI Gen3 End port Hint values per lane} TYPE:{EditNum, HEX, (0x00, 0xFFFFFFFF)}
  # !HDR STRUCT:{UINT8}
  # !BSF HELP:{Used for programming DMI Gen3 Hint values per lane. Range: 0-6, 2 is default for each lane}
  gPlatformFspPkgTokenSpaceGuid.DmiGen3EndPointHint         | * | 0x8 | {0x2, 0x2, 0x2, 0x2, 0x2, 0x2, 0x2, 0x2}

  # !BSF NAME:{DMI Gen3 RxCTLEp per-Bundle control} TYPE:{EditNum, HEX, (0x00, 0xFFFF)}
  # !HDR STRUCT:{UINT8}
  # !BSF HELP:{Range: 0-15, 0 is default for each bundle, must be specified based upon platform design}
  gPlatformFspPkgTokenSpaceGuid.DmiGen3RxCtlePeaking        | * | 0x4 | {0x0, 0x0, 0x0, 0x0}

  # !BSF NAME:{DMI ASPM Configuration:{Combo}
  # !BSF OPTION:{0:Disabled, 1:L0s, 2:L1, 3:L1L0s}
  # !BSF HELP:{Set ASPM Configuration}
  gPlatformFspPkgTokenSpaceGuid.DmiAspm      | * | 0x01 | 0x2

  # !BSF NAME:{Enable/Disable DMI GEN3 Hardware Eq} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Enable/Disable DMI GEN3 Hardware Eq. Disabled(0x0)(Default): Disable Hardware Eq, Enabled(0x1): Enable  EQ Phase1 Static Presets Programming}
  gPlatformFspPkgTokenSpaceGuid.DmiHweq      | * | 0x01 | 0x1

  # !BSF NAME:{Enable/Disable CPU DMI GEN3 Phase 23 Bypass} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{CPU DMI GEN3 Phase 23 Bypass. Disabled(0x0)(Default): Disable Phase 23 Bypass, Enabled(0x1): Enable  Phase 23 Bypass}
  gPlatformFspPkgTokenSpaceGuid.Gen3EqPhase23Bypass      | * | 0x01 | 0x0

  # !BSF NAME:{Enable/Disable CPU DMI GEN3 Phase 3 Bypass} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{CPU DMI GEN3 Phase 3 Bypass. Disabled(0x0)(Default): Disable Phase 3 Bypass, Enabled(0x1): Enable  Phase 3 Bypass}
  gPlatformFspPkgTokenSpaceGuid.Gen3EqPhase3Bypass      | * | 0x01 | 0x0

  # !BSF NAME:{Enable/Disable CPU DMI Gen3 EQ Local Transmitter Coefficient Override Enable} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Program Gen3 EQ Local Transmitter Coefficient Override. Disabled(0x0)(Default): Disable Local Transmitter Coefficient Override, Enabled(0x1): Enable  Local Transmitter Coefficient Override}
  gPlatformFspPkgTokenSpaceGuid.Gen3LtcoEnable      | * | 0x01 | 0x0

  # !BSF NAME:{Enable/Disable CPU DMI Gen3 EQ Remote Transmitter Coefficient/Preset Override Enable} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Program Remote Transmitter Coefficient/Preset Override. Disabled(0x0)(Default): Disable Remote Transmitter Coefficient/Preset Override, Enabled(0x1): Enable  Remote Transmitter Coefficient/Preset Override}
  gPlatformFspPkgTokenSpaceGuid.Gen3RtcoRtpoEnable      | * | 0x01 | 0x1

  # !BSF NAME:{DMI Gen3 Transmitter Pre-Cursor Coefficient  } TYPE:{EditNum, HEX, (0x00, 0xFFFFFFFF)}
  # !HDR STRUCT:{UINT8}
  # !BSF HELP:{Used for programming DMI Gen3 Transmitter Pre-Cursor Coefficient . Range: 0-10, 2 is default for each lane}
  gPlatformFspPkgTokenSpaceGuid.DmiGen3Ltcpre       | * | 0x8 | {0x2, 0x2, 0x2, 0x2, 0x2, 0x2, 0x2, 0x2}

  # !BSF NAME:{DMI Gen3 Transmitter Post-Cursor Coefficient} TYPE:{EditNum, HEX, (0x00, 0xFFFFFFFF)}
  # !HDR STRUCT:{UINT8}
  # !BSF HELP:{Used for programming Transmitter Post-Cursor Coefficient. Range: 0-9, 2 is default for each lane}
  gPlatformFspPkgTokenSpaceGuid.DmiGen3Ltcpo       | * | 0x8 | {0x2, 0x2, 0x2, 0x2, 0x2, 0x2, 0x2, 0x2}

  # !BSF NAME:{PCIE Hw Eq Gen3 CoeffList Cm} TYPE:{EditNum, HEX, (0x00,0xFFFFFFFFFF)}
  # !BSF HELP:{CPU_PCIE_EQ_PARAM. Coefficient C-1.}
  gPlatformFspPkgTokenSpaceGuid.CpuDmiHwEqGen3CoeffListCm         | * | 0x08 | {0x00, 0x00, 0x01, 0x01, 0x01, 0x00, 0x00, 0x00}

  # !BSF NAME:{PCIE Hw Eq Gen3 CoeffList Cp} TYPE:{EditNum, HEX, (0x00,0xFFFFFFFFFF)}
  # !BSF HELP:{CPU_PCIE_EQ_PARAM. Coefficient C+1.}
  gPlatformFspPkgTokenSpaceGuid.CpuDmiHwEqGen3CoeffListCp         | * | 0x08 | {0x00, 0x00, 0x01, 0x01, 0x01, 0x00, 0x00, 0x00}

  # !BSF NAME:{Enable/Disable DMI GEN3 DmiGen3DsPresetEnable} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Enable/Disable DMI GEN3 DmiGen3DsPreset. Auto(0x0)(Default): DmiGen3DsPresetEnable, Manual(0x1): Enable DmiGen3DsPresetEnable}
  gPlatformFspPkgTokenSpaceGuid.DmiGen3DsPresetEnable      | * | 0x01 | 0x0

  # !BSF NAME:{DMI Gen3 Root port preset Rx values per lane} TYPE:{EditNum, HEX, (0x00, 0xFFFFFFFF)}
  # !HDR STRUCT:{UINT8}
  # !BSF HELP:{Used for programming DMI Gen3 preset values per lane. Range: 0-10, 1 is default for each lane}
  gPlatformFspPkgTokenSpaceGuid.DmiGen3DsPortRxPreset       | * | 0x8 | {0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01}

  # !BSF NAME:{DMI Gen3 Root port preset Tx values per lane} TYPE:{EditNum, HEX, (0x00, 0xFFFFFFFF)}
  # !HDR STRUCT:{UINT8}
  # !BSF HELP:{Used for programming DMI Gen3 preset values per lane. Range: 0-10, 7 is default for each lane}
  gPlatformFspPkgTokenSpaceGuid.DmiGen3DsPortTxPreset       | * | 0x8 | {0x07, 0x07, 0x07, 0x07, 0x07, 0x07, 0x07, 0x07}

  # !BSF NAME:{Enable/Disable DMI GEN3 DmiGen3UsPresetEnable} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Enable/Disable DMI GEN3 DmiGen3UsPreset. Auto(0x0)(Default): DmiGen3UsPresetEnable, Manual(0x1): Enable DmiGen3UsPresetEnable}
  gPlatformFspPkgTokenSpaceGuid.DmiGen3UsPresetEnable      | * | 0x01 | 0x0

  # !BSF NAME:{DMI Gen3 Root port preset Rx values per lane} TYPE:{EditNum, HEX, (0x00, 0xFFFFFFFF)}
  # !HDR STRUCT:{UINT8}
  # !BSF HELP:{Used for programming DMI Gen3 preset values per lane. Range: 0-10, 7 is default for each lane}
  gPlatformFspPkgTokenSpaceGuid.DmiGen3UsPortRxPreset       | * | 0x8 | {0x07, 0x07, 0x07, 0x07, 0x07, 0x07, 0x07, 0x07}

  # !BSF NAME:{DMI Gen3 Root port preset Tx values per lane} TYPE:{EditNum, HEX, (0x00, 0xFFFFFFFF)}
  # !HDR STRUCT:{UINT8}
  # !BSF HELP:{Used for programming DMI Gen3 preset values per lane. Range: 0-10, 7 is default for each lane}
  gPlatformFspPkgTokenSpaceGuid.DmiGen3UsPortTxPreset       | * | 0x8 | {0x7, 0x7, 0x7, 0x7, 0x7, 0x7, 0x7, 0x7}

  # !BSF NAME:{DMI Hw Eq Gen4 CoeffList Cm} TYPE:{EditNum, HEX, (0x00,0xFFFFFFFFFF)}
  # !BSF HELP:{CPU_PCIE_EQ_PARAM. Coefficient C-1.}
  gPlatformFspPkgTokenSpaceGuid.CpuDmiHwEqGen4CoeffListCm         | * | 0x08 | {0x0, 0x7, 0x6, 0x7, 0x7, 0x7, 0x7, 0x7}

  # !BSF NAME:{DMI Hw Eq Gen4 CoeffList Cp} TYPE:{EditNum, HEX, (0x00,0xFFFFFFFFFF)}
  # !BSF HELP:{CPU_PCIE_EQ_PARAM. Coefficient C+1.}
  gPlatformFspPkgTokenSpaceGuid.CpuDmiHwEqGen4CoeffListCp         | * | 0x08 | {0x0, 0xE, 0xA, 0x7, 0x7, 0x7, 0x7, 0x7}

  # !BSF NAME:{Enable/Disable CPU DMI GEN4 Phase 23 Bypass} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{CPU DMI GEN4 Phase 23 Bypass. Disabled(0x0)(Default): Disable Phase 23 Bypass, Enabled(0x1): Enable  Phase 23 Bypass}
  gPlatformFspPkgTokenSpaceGuid.Gen4EqPhase23Bypass      | * | 0x01 | 0x0

  # !BSF NAME:{Enable/Disable CPU DMI GEN4 Phase 3 Bypass} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{CPU DMI GEN3 Phase 4 Bypass. Disabled(0x0)(Default): Disable Phase 3 Bypass, Enabled(0x1): Enable  Phase 3 Bypass}
  gPlatformFspPkgTokenSpaceGuid.Gen4EqPhase3Bypass      | * | 0x01 | 0x0

  # !BSF NAME:{Enable/Disable DMI GEN4 DmiGen4DsPresetEnable} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Enable/Disable DMI GEN4 DmiGen4DsPreset. Auto(0x0)(Default): DmiGen4DsPresetEnable, Manual(0x1): Enable DmiGen4DsPresetEnable}
  gPlatformFspPkgTokenSpaceGuid.DmiGen4DsPresetEnable      | * | 0x01 | 0x0

  # !BSF NAME:{DMI Gen4 Root port preset Tx values per lane} TYPE:{EditNum, HEX, (0x00, 0xFFFFFFFF)}
  # !HDR STRUCT:{UINT8}
  # !BSF HELP:{Used for programming DMI Gen4 preset values per lane. Range: 0-10, 7 is default for each lane}
  gPlatformFspPkgTokenSpaceGuid.DmiGen4DsPortTxPreset       | * | 0x8 | {0x07, 0x07, 0x07, 0x07, 0x07, 0x07, 0x07, 0x07}

  # !BSF NAME:{Enable/Disable CPU DMI Gen4 EQ Remote Transmitter Coefficient/Preset Override Enable} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Program Remote Transmitter Coefficient/Preset Override. Disabled(0x0)(Default): Disable Remote Transmitter Coefficient/Preset Override, Enabled(0x1): Enable  Remote Transmitter Coefficient/Preset Override}
  gPlatformFspPkgTokenSpaceGuid.Gen4RtcoRtpoEnable      | * | 0x01 | 0x1

  # !BSF NAME:{Enable/Disable CPU DMI Gen4 EQ Local Transmitter Coefficient Override Enable} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Program Gen3 EQ Local Transmitter Coefficient Override. Disabled(0x0)(Default): Disable Local Transmitter Coefficient Override, Enabled(0x1): Enable  Local Transmitter Coefficient Override}
  gPlatformFspPkgTokenSpaceGuid.Gen4LtcoEnable      | * | 0x01 | 0x0

  # !BSF NAME:{DMI Gen4 Transmitter Pre-Cursor Coefficient  } TYPE:{EditNum, HEX, (0x00, 0xFFFFFFFF)}
  # !HDR STRUCT:{UINT8}
  # !BSF HELP:{Used for programming DMI Gen4 Transmitter Pre-Cursor Coefficient . Range: 0-10, 7 is default for each lane}
  gPlatformFspPkgTokenSpaceGuid.DmiGen4Ltcpre       | * | 0x8 | {0x07, 0x07, 0x07, 0x07, 0x07, 0x07, 0x07, 0x07}

  # !BSF NAME:{DMI Gen4 Transmitter Post-Cursor Coefficient} TYPE:{EditNum, HEX, (0x00, 0xFFFFFFFF)}
  # !HDR STRUCT:{UINT8}
  # !BSF HELP:{Used for programming DMI Gen4 Transmitter Post-Cursor Coefficient. Range: 0-9, 7 is default for each lane}
  gPlatformFspPkgTokenSpaceGuid.DmiGen4Ltcpo       | * | 0x8 | {0x07, 0x07, 0x07, 0x07, 0x07, 0x07, 0x07, 0x07}

  # !BSF NAME:{Enable/Disable DMI GEN4 DmiGen4UsPresetEnable} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Enable/Disable DMI GEN4 DmiGen4UsPreset. Auto(0x0)(Default): DmiGen4UsPresetEnable, Manual(0x1): Enable DmiGen4UsPresetEnable}
  gPlatformFspPkgTokenSpaceGuid.DmiGen4UsPresetEnable      | * | 0x01 | 0x0

  # !BSF NAME:{DMI Gen4 Root port preset Tx values per lane} TYPE:{EditNum, HEX, (0x00, 0xFFFFFFFF)}
  # !HDR STRUCT:{UINT8}
  # !BSF HELP:{Used for programming DMI Gen4 preset values per lane. Range: 0-10, 1 is default for each lane}
  gPlatformFspPkgTokenSpaceGuid.DmiGen4UsPortTxPreset       | * | 0x8 | {0x07, 0x07, 0x07, 0x07, 0x07, 0x07, 0x07, 0x07}

  # !BSF NAME:{DMI ASPM Control Configuration:{Combo}
  # !BSF OPTION:{0:Disabled, 1:L0s, 2:L1, 3:L1L0s}
  # !BSF HELP:{Set ASPM Control configuration}
  gPlatformFspPkgTokenSpaceGuid.DmiAspmCtrl      | * | 0x01 | 0x2

  # !BSF NAME:{DMI ASPM L1 exit Latency}
  # !BSF TYPE:{EditNum, HEX, (0x00, 0x07)}
  # !BSF HELP:{Range: 0-7, 4 is default L1 exit Latency}
  gPlatformFspPkgTokenSpaceGuid.DmiAspmL1ExitLatency      | * | 0x01 | 0x4

  #
  # SA Pre-Mem Production Block End
  #

  #
  # CPU Pre-Mem Production Block Start
  #
  # !BSF PAGE:{CPU1}
  # !BSF NAME:{BIST on Reset} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Enable or Disable BIST on Reset; <b>0: Disable</b>; 1: Enable.}
  gPlatformFspPkgTokenSpaceGuid.BistOnReset                 | * | 0x01 | 0x00

  # !BSF NAME:{Skip Stop PBET Timer Enable/Disable} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Skip Stop PBET Timer; <b>0: Disable</b>; 1: Enable}
  gPlatformFspPkgTokenSpaceGuid.SkipStopPbet                | * | 0x01 | 0x00


  # !BSF NAME:{C6DRAM power gating feature} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{This policy indicates whether or not BIOS should allocate PRMRR memory for C6DRAM power gating feature.- 0: Don't allocate any PRMRR memory for C6DRAM power gating feature.- <b>1: Allocate PRMRR memory for C6DRAM power gating feature</b>.}
  gPlatformFspPkgTokenSpaceGuid.EnableC6Dram                | * | 0x01 | 0x01

  # !BSF NAME:{Over clocking support} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Over clocking support; <b>0: Disable</b>; 1: Enable}
  gPlatformFspPkgTokenSpaceGuid.OcSupport                   | * | 0x01 | 0x00

  # !BSF NAME:{Over clocking Lock} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Over clocking Lock Enable/Disable; 0: Disable; <b>1: Enable</b>}
  gPlatformFspPkgTokenSpaceGuid.OcLock                      | * | 0x01 | 0x01

  # !BSF NAME:{Maximum Core Turbo Ratio Override}
  # !BSF TYPE:{EditNum, HEX, (0x00, 0x53)}
  # !BSF HELP:{Maximum core turbo ratio override allows to increase CPU core frequency beyond the fused max turbo ratio limit. <b>0: Hardware defaults.</b> Range: 0-85}
  gPlatformFspPkgTokenSpaceGuid.CoreMaxOcRatio              | * | 0x01 | 0x00

  # !BSF NAME:{Core voltage mode} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Core voltage mode; <b>0: Adaptive</b>; 1: Override.}
  gPlatformFspPkgTokenSpaceGuid.CoreVoltageMode             | * | 0x01 | 0x00

  # !BSF NAME:{Maximum clr turbo ratio override}
  # !BSF TYPE:{EditNum, HEX, (0x00, 0x53)}
  # !BSF HELP:{Maximum clr turbo ratio override allows to increase CPU clr frequency beyond the fused max turbo ratio limit. <b>0: Hardware defaults.</b>  Range: 0-85}
  gPlatformFspPkgTokenSpaceGuid.RingMaxOcRatio              | * | 0x01 | 0x00

  # !BSF NAME:{Hyper Threading Enable/Disable} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Enable or Disable Hyper Threading; 0: Disable; <b>1: Enable</b>}
  gPlatformFspPkgTokenSpaceGuid.HyperThreading              | * | 0x01 | 0x01

  # !BSF NAME:{Enable or Disable CPU Ratio Override} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Enable or Disable CPU Ratio Override; <b>0: Disable</b>; 1: Enable.}
  gPlatformFspPkgTokenSpaceGuid.CpuRatioOverride            | * | 0x01 | 0x00

  # !BSF NAME:{CPU ratio value}
  # !BSF TYPE:{EditNum, HEX, (0x00, 0x3F)}
  # !BSF HELP:{CPU ratio value. Valid Range 0 to 63}
  gPlatformFspPkgTokenSpaceGuid.CpuRatio                    | * | 0x01 | 0x1C

  # !BSF NAME:{Boot frequency}
  # !BSF TYPE:{Combo} OPTION:{0:0, 1:1, 2:2}
  # !BSF HELP:{Sets the boot frequency starting from reset vector.- 0: Maximum battery performance. 1: Maximum non-turbo performance. <b>2: Turbo performance </b>}
  gPlatformFspPkgTokenSpaceGuid.BootFrequency               | * | 0x01 | 0x02

  # !BSF NAME:{Number of active big cores}
  # !BSF TYPE:{Combo} OPTION:{0:Disable all big cores, 1:1, 2:2, 3:3, 0xFF:Active all big cores}
  # !BSF HELP:{Number of active big cores(Depends on Number of big cores). Default 0xFF means to active all system supported big cores. <b>0xFF: Active all big cores</b>; 0: Disable all big cores; 1: 1; 2: 2; 3: 3;}
  gPlatformFspPkgTokenSpaceGuid.ActiveCoreCount             | * | 0x01 | 0xFF

  # !BSF NAME:{Processor Early Power On Configuration FCLK setting}
  # !BSF TYPE:{Combo} OPTION:{0:800 MHz, 1: 1 GHz, 2: 400 MHz, 3: Reserved }
  # !BSF HELP:{ <b>0: 800 MHz (ULT/ULX)</b>. <b>1: 1 GHz (DT/Halo)</b>. Not supported on ULT/ULX.- 2: 400 MHz. - 3: Reserved}
  gPlatformFspPkgTokenSpaceGuid.FClkFrequency               | * | 0x01 | 0x00

  # !BSF NAME:{Set JTAG power in C10 and deeper power states} TYPE:{Combo} OPTION:{0: False, 1: True}
  # !BSF HELP:{False: JTAG is power gated in C10 state. True: keeps the JTAG power up during C10 and deeper power states for debug purpose. <b>0: False</b>; 1: True.}
  gPlatformFspPkgTokenSpaceGuid.JtagC10PowerGateDisable     | * | 0x01 | 0x00

  # !BSF NAME:{Enable or Disable VMX} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Enable or Disable VMX; 0: Disable; <b>1: Enable</b>.}
  gPlatformFspPkgTokenSpaceGuid.VmxEnable                   | * | 0x01 | 0x01

  # !BSF NAME:{AVX2 Ratio Offset} TYPE:{EditNum, HEX, (0x00, 0x1F)}
  # !BSF HELP:{0(Default)= No Offset. Range 0 - 31. Specifies number of bins to decrease AVX ratio vs. Core Ratio. Uses Mailbox MSR 0x150, cmd 0x1B.}
  gPlatformFspPkgTokenSpaceGuid.Avx2RatioOffset             | * | 0x1 | 0

  # !BSF NAME:{AVX3 Ratio Offset} TYPE:{EditNum, HEX, (0x00, 0x1F)}
  # !BSF HELP:{DEPRECATED}
  gPlatformFspPkgTokenSpaceGuid.Avx3RatioOffset             | * | 0x1 | 0

  # !BSF NAME:{BCLK Adaptive Voltage Enable} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{When enabled, the CPU V/F curves are aware of BCLK frequency when calculated. </b>0: Disable;<b> 1: Enable}
  gPlatformFspPkgTokenSpaceGuid.BclkAdaptiveVoltage     | * | 0x01 | 0x00

  # !BSF NAME:{core voltage override}
  # !BSF TYPE:{EditNum, HEX, (0x00, 0x7D0)}
  # !BSF HELP:{The core voltage override which is applied to the entire range of cpu core frequencies. Valid Range 0 to 2000}
  gPlatformFspPkgTokenSpaceGuid.CoreVoltageOverride         | * | 0x02 | 0x00

  # !BSF NAME:{Core Turbo voltage Adaptive}
  # !BSF TYPE:{EditNum, HEX, (0x00, 0x7D0)}
  # !BSF HELP:{Extra Turbo voltage applied to the cpu core when the cpu is operating in turbo mode. Valid Range 0 to 2000}
  gPlatformFspPkgTokenSpaceGuid.CoreVoltageAdaptive         | * | 0x02 | 0x00

  # !BSF NAME:{Core Turbo voltage Offset}
  # !BSF TYPE:{EditNum, HEX, (0x00, 0x3E8)}
  # !BSF HELP:{The voltage offset applied to the core while operating in turbo mode.Valid Range 0 to 1000}
  gPlatformFspPkgTokenSpaceGuid.CoreVoltageOffset           | * | 0x02 | 0x00

  # !BSF NAME:{Core PLL voltage offset}
  # !BSF TYPE:{EditNum, HEX, (0x00, 0x0F)}
  # !BSF HELP:{Core PLL voltage offset. <b>0: No offset</b>. Range 0-15}
  gPlatformFspPkgTokenSpaceGuid.CorePllVoltageOffset        | * | 0x01 | 0x00

  # !BSF NAME:{Atom Core PLL voltage offset}
  # !BSF TYPE:{EditNum, HEX, (0x00, 0x0F)}
  # !BSF HELP:{Atom Core PLL voltage offset. <b>0: No offset</b>. Range 0-15}
  gPlatformFspPkgTokenSpaceGuid.AtomPllVoltageOffset        | * | 0x01 | 0x00

  # !BSF NAME:{Ring Downbin} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Ring Downbin enable/disable. When enabled, CPU will ensure the ring ratio is always lower than the core ratio.0: Disable; <b>1: Enable.</b>}
  gPlatformFspPkgTokenSpaceGuid.RingDownBin                 | * | 0x01 | 0x01

  # !BSF NAME:{Ring voltage mode} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Ring voltage mode; <b>0: Adaptive</b>; 1: Override.}
  gPlatformFspPkgTokenSpaceGuid.RingVoltageMode             | * | 0x01 | 0x00

  # !BSF NAME:{TjMax Offset}
  # !BSF TYPE:{EditNum, HEX, (0x0A,0x3F)}
  # !BSF HELP:{TjMax offset.Specified value here is clipped by pCode (125 - TjMax Offset) to support TjMax in the range of 62 to 115 deg Celsius. Valid Range 10 - 63}
  gPlatformFspPkgTokenSpaceGuid.TjMaxOffset                 | * | 0x01 | 0x00

  # !BSF NAME:{Ring voltage override}
  # !BSF TYPE:{EditNum, HEX, (0x00, 0x7D0)}
  # !BSF HELP:{The ring voltage override which is applied to the entire range of cpu ring frequencies. Valid Range 0 to 2000}
  gPlatformFspPkgTokenSpaceGuid.RingVoltageOverride         | * | 0x02 | 0x00

  # !BSF NAME:{Ring Turbo voltage Adaptive}
  # !BSF TYPE:{EditNum, HEX, (0x00, 0x7D0)}
  # !BSF HELP:{Extra Turbo voltage applied to the cpu ring when the cpu is operating in turbo mode. Valid Range 0 to 2000}
  gPlatformFspPkgTokenSpaceGuid.RingVoltageAdaptive         | * | 0x02 | 0x00

  # !BSF NAME:{Ring Turbo voltage Offset}
  # !BSF TYPE:{EditNum, HEX, (0x00, 0x3E8)}
  # !BSF HELP:{The voltage offset applied to the ring while operating in turbo mode. Valid Range 0 to 1000}
  gPlatformFspPkgTokenSpaceGuid.RingVoltageOffset           | * | 0x02 | 0x00

  # !BSF NAME:{Enable or Disable TME} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Enable or Disable TME; <b>0: Disable</b>; 1: Enable.}
  gPlatformFspPkgTokenSpaceGuid.TmeEnable                   | * | 0x01 | 0x00

  # !BSF NAME:{Enable CPU CrashLog} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Enable or Disable CPU CrashLog; 0: Disable; <b>1: Enable</b>.}
  gPlatformFspPkgTokenSpaceGuid.CpuCrashLogEnable           | * | 0x01 | 0x01

  # !BSF NAME:{CPU Run Control} TYPE:{Combo} OPTION:{0:Disabled, 1:Enabled, 2:No Change}
  # !BSF HELP:{Enable, Disable or Do not configure CPU Run Control; 0: Disable; 1: Enable ; <b>2: No Change</b>}
  gPlatformFspPkgTokenSpaceGuid.DebugInterfaceEnable        | * | 0x01 | 0x02

  # !BSF NAME:{CPU Run Control Lock} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Lock or Unlock CPU Run Control; 0: Disable; <b>1: Enable</b>.}
  gPlatformFspPkgTokenSpaceGuid.DebugInterfaceLockEnable    | * | 0x01 | 0x01

  # !BSF NAME:{Atom L2 voltage mode} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Atom L2 voltage mode; <b>0: Adaptive</b>; 1: Override.}
  gPlatformFspPkgTokenSpaceGuid.AtomL2VoltageMode             | * | 0x01 | 0x00

  # !BSF NAME:{Atom L2 Voltage Override}
  # !BSF TYPE:{EditNum, HEX, (0x00, 0x7D0)}
  # !BSF HELP:{The atom L2 voltage override which is applied to the entire range of atom L2 frequencies. Valid Range 0 to 2000}
  gPlatformFspPkgTokenSpaceGuid.AtomL2VoltageOverride         | * | 0x02 | 0x00

  # !BSF NAME:{Atom L2 Turbo voltage Adaptive}
  # !BSF TYPE:{EditNum, HEX, (0x00, 0x7D0)}
  # !BSF HELP:{Extra Turbo voltage applied to the atom L2 when the atom L2 is operating in turbo mode. Valid Range 0 to 2000}
  gPlatformFspPkgTokenSpaceGuid.AtomL2VoltageAdaptive         | * | 0x02 | 0x00

  # !BSF NAME:{Atom L2 Turbo voltage Offset}
  # !BSF TYPE:{EditNum, HEX, (0x00, 0x3E8)}
  # !BSF HELP:{The voltage offset applied to the atom while operating in turbo mode.Valid Range 0 to 1000}
  gPlatformFspPkgTokenSpaceGuid.AtomL2VoltageOffset           | * | 0x02 | 0x00

  # !BSF NAME:{Per-Atom-Cluster VF Offset} TYPE:{EditNum, HEX, (0x00,0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF)}
  # !HDR STRUCT:{UINT16}
  # !BSF HELP:{Array used to specifies the selected Atom Core Cluster Offset Voltage. This voltage is specified in millivolts.}
  gPlatformFspPkgTokenSpaceGuid.PerAtomClusterVoltageOffset        | * | 0x8 | { 0x00, 0x00, 0x00, 0x00 }

  # !BSF NAME:{Per-Atom-Cluster VF Offset Prefix} TYPE:{EditNum, HEX, (0x00,0xFFFFFFFFFFFFFFFF)}
  # !BSF HELP:{Sets the PerAtomClusterVoltageOffset value as positive or negative for the selected Core; <b>0: Positive </b>; 1: Negative.}
  gPlatformFspPkgTokenSpaceGuid.PerAtomClusterVoltageOffsetPrefix  | * | 0x4 | { 0x00, 0x00, 0x00, 0x00 }

  # !BSF NAME:{Enable IA CEP} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Control for enabling/disabling IA CEP (Current Excursion Protection)). <b>1: Enable</b>; 0: Disable}
  gPlatformFspPkgTokenSpaceGuid.IaCepEnable                 | * | 0x01 | 0x1

  # !BSF NAME:{Enable GT CEP} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Control for enabling/disabling GT CEP (Current Excursion Protection)). <b>1: Enable</b>; 0: Disable}
  gPlatformFspPkgTokenSpaceGuid.GtCepEnable                 | * | 0x01 | 0x1

  # !BSF NAME:{Enable CPU DLVR bypass mode support} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Control for enabling/disabling CPU DLVR bypass mode). <b>0: Disable</b>; 1: Enable}
  gPlatformFspPkgTokenSpaceGuid.DlvrBypassModeEnable        | * | 0x01 | 0x0

  # !BSF NAME:{Number of active small cores}
  # !BSF TYPE:{Combo} OPTION:{0:Disable all small cores, 1:1, 2:2, 3:3, 0xFF:Active all small cores}
  # !BSF HELP:{Number of active small cores(Depends on Number of small cores). Default 0xFF means to active all system supported small cores. <b>0xFF: Active all small cores</b>; 0: Disable all small cores; 1: 1; 2: 2; 3: 3;}
  gPlatformFspPkgTokenSpaceGuid.ActiveSmallCoreCount        | * | 0x01 | 0xFF

  # !BSF NAME:{Core VF Point Offset Mode} TYPE:{Combo}  OPTION:{0:Legacy, 1:Selection}
  # !BSF HELP:{Selects Core Voltage & Frequency Offset mode between Legacy and Selection modes. In Legacy Mode, setting a global offset for the entire VF curve. In Selection Mode, setting a selected VF point; <b>0: Legacy</b>; 1: Selection.}
  gPlatformFspPkgTokenSpaceGuid.CoreVfPointOffsetMode             | * | 0x01 | 0x00

  # !BSF NAME:{Core VF Point Offset} TYPE:{EditNum, HEX, (0x00,0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF)}
  # !HDR STRUCT:{UINT16}
  # !BSF HELP:{Array used to specifies the Core Voltage Offset applied to the each selected VF Point. This voltage is specified in millivolts.}
  gPlatformFspPkgTokenSpaceGuid.CoreVfPointOffset       | * | 0x1E | { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 }

  # !BSF NAME:{Core VF Point Offset Prefix} TYPE:{Combo}  OPTION:{0:Positive, 1:Negative}
  # !BSF HELP:{Sets the CoreVfPointOffset value as positive or negative for corresponding core VF Point; <b>0: Positive </b>; 1: Negative.}
  gPlatformFspPkgTokenSpaceGuid.CoreVfPointOffsetPrefix | * | 0x0F | { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 }

  # !BSF NAME:{Core VF Point Ratio} TYPE:{EditNum, HEX, (0x00,0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF)}
  # !BSF HELP:{Array for the each selected Core VF Point to display the ration.}
  gPlatformFspPkgTokenSpaceGuid.CoreVfPointRatio       | * | 0x0F | { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 }

  # !BSF NAME:{Core VF Point Count}
  # !BSF TYPE:{EditNum, HEX, (0x0, 0xFF)}
  # !BSF HELP:{Number of supported Core Voltage & Frequency Point Offset}
  gPlatformFspPkgTokenSpaceGuid.CoreVfPointCount       | * | 0x01 | 0x00

  # !BSF NAME:{Core VF Configuration Scope} TYPE:{Combo}  OPTION:{0:All-core, 1:Per-core}
  # !BSF HELP:{Alows both all-core VF curve or per-core VF curve configuration; <b>0: All-core</b>; 1: Per-core.}
  gPlatformFspPkgTokenSpaceGuid.CoreVfConfigScope           | * | 0x01 | 0x00

  # !BSF NAME:{Per-core VF Offset} TYPE:{EditNum, HEX, (0x00,0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF)}
  # !HDR STRUCT:{UINT16}
  # !BSF HELP:{Array used to specifies the selected Core Offset Voltage. This voltage is specified in millivolts.}
  gPlatformFspPkgTokenSpaceGuid.PerCoreVoltageOffset        | * | 0x10 | { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 }

  # !BSF NAME:{Per-core VF Offset Prefix} TYPE:{EditNum, HEX, (0x00,0xFFFFFFFFFFFFFFFF)}
  # !BSF HELP:{Sets the PerCoreVoltageOffset value as positive or negative for the selected Core; <b>0: Positive </b>; 1: Negative.}
  gPlatformFspPkgTokenSpaceGuid.PerCoreVoltageOffsetPrefix  | * | 0x08 | { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 }

  # !BSF NAME:{Per Core Max Ratio override} TYPE:{Combo}  OPTION:{$EN_DIS}
  # !BSF HELP:{Enable or disable Per Core PState OC supported by writing OCMB 0x1D to program new favored core ratio to each Core. <b>0: Disable</b>, 1: enable}
  gPlatformFspPkgTokenSpaceGuid.PerCoreRatioOverride        | * | 0x01 | 0x00

  # !BSF NAME:{Per Core Current Max Ratio} TYPE:{EditNum, HEX, (0x00,0xFFFFFFFFFFFFFFFF)}
  # !BSF HELP:{Array for the Per Core Max Ratio}
  gPlatformFspPkgTokenSpaceGuid.PerCoreRatio                | * | 0x08 | { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 }

  # !BSF NAME:{Atom Cluster Max Ratio} TYPE:{EditNum, HEX, (0x00,0xFFFFFFFF)}
  # !BSF HELP:{Array for Atom Cluster Max Ratio, 4 ATOM cores are in the same Cluster and their max core ratio will be aligned.}
  gPlatformFspPkgTokenSpaceGuid.AtomClusterRatio            | * | 0x04 | { 0x00, 0x00, 0x00, 0x00 }

  # !BSF NAME:{Core Ratio Extension Mode} TYPE:{Combo}  OPTION:{$EN_DIS}
  # !BSF HELP:{Enable or disable Core Ratio above 85 Extension Mode by writing BIOS MB 0x37 to enable FULL_RANGE_MULTIPLIER_UNLOCK_EN. <b>0: Disable</b>, 1: enable}
  gPlatformFspPkgTokenSpaceGuid.CoreRatioExtensionMode      | * | 0x01 | 0x00

  # !BSF NAME:{Pvd Ratio Threshold} TYPE:{EditNum, HEX, (0x0, 0x28)}
  # !BSF HELP:{Select PVD Ratio Threshold Value from Range 1 to 40. 0 - Auto/Default.}
  gPlatformFspPkgTokenSpaceGuid.PvdRatioThreshold           | * | 0x01 | 0x00

  # !BSF NAME:{Support Unlimited ICCMAX} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{DEPRECATED}
  gPlatformFspPkgTokenSpaceGuid.UnlimitedIccMax             | * | 0x01 | 0x00

  # !BSF NAME:{Enable CPU CrashLog GPRs dump} TYPE:{Combo} OPTION:{0:Disabled, 1:Enabled, 2:Only Smm GPRs Disabled}
  # !BSF HELP:{Enable or Disable CPU CrashLog GPRs dump; <b>0: Disable</b>; 1: Enable; 2: Only disable Smm GPRs dump}
  gPlatformFspPkgTokenSpaceGuid.CrashLogGprs             | * | 0x01 | 0x00

  # !BSF NAME:{Ring VF Point Offset Mode} TYPE:{Combo}  OPTION:{0:Legacy, 1:Selection}
  # !BSF HELP:{Selects Ring Voltage & Frequency Offset mode between Legacy and Selection modes. In Legacy Mode, setting a global offset for the entire VF curve. In Selection Mode, setting a selected VF point; <b>0: Legacy</b>; 1: Selection.}
  gPlatformFspPkgTokenSpaceGuid.RingVfPointOffsetMode             | * | 0x01 | 0x00

  # !BSF NAME:{Ring VF Point Offset} TYPE:{EditNum, HEX, (0x00,0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF)}
  # !HDR STRUCT:{UINT16}
  # !BSF HELP:{Array used to specifies the Ring Voltage Offset applied to the each selected VF Point. This voltage is specified in millivolts.}
  gPlatformFspPkgTokenSpaceGuid.RingVfPointOffset       | * | 0x1E | { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 }

  # !BSF NAME:{Ring VF Point Offset Prefix} TYPE:{EditNum, HEX, (0x00,0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF)}
  # !BSF HELP:{Sets the RingVfPointOffset value as positive or negative for corresponding core VF Point; <b>0: Positive </b>; 1: Negative.}
  gPlatformFspPkgTokenSpaceGuid.RingVfPointOffsetPrefix | * | 0x0F | { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 }

  # !BSF NAME:{Ring VF Point Ratio} TYPE:{EditNum, HEX, (0x00,0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF)}
  # !BSF HELP:{Array for the each selected Ring VF Point to display the ration.}
  gPlatformFspPkgTokenSpaceGuid.RingVfPointRatio       | * | 0x0F | { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 }

  # !BSF NAME:{Ring VF Point Count}
  # !BSF TYPE:{EditNum, HEX, (0x0, 0xFF)}
  # !BSF HELP:{Number of supported Ring Voltage & Frequency Point Offset}
  gPlatformFspPkgTokenSpaceGuid.RingVfPointCount       | * | 0x01 | 0x00

  # !BSF NAME:{BCLK Frequency Source} TYPE:{Combo} OPTION:{1:CPU BCLK, 2:PCH BCLK, 3:External CLK}
  # !BSF HELP:{Clock source of BCLK OC frequency, <b>1:CPU BCLK</b>, 2:PCH BCLK, 3:External CLK}
  gPlatformFspPkgTokenSpaceGuid.BclkSource                  | * | 0x01 | 0x01

  # !BSF NAME:{GPIO Override}  TYPE:{EditNum, HEX, (0x00, 0x7)}
  # !BSF HELP:{Gpio Override Level - FSP will not configure any GPIOs and rely on GPIO setings before moved to FSP. Available configurations 0: Disable; 1: Level 1 - Skips GPIO configuration in PEI/FSPM/FSPT phase;2: Level 2 - Reserved for future use}
  gPlatformFspPkgTokenSpaceGuid.GpioOverride                | * | 0x01  |  0x00

  # !BSF NAME:{CPU BCLK OC Frequency} TYPE:{EditNum, HEX, (0x00,0xFFFFFFFF)}
  # !BSF HELP:{CPU BCLK OC Frequency in 10KHz units increasing. Value 9800 (10KHz) = 98MHz <b>0 - Auto</b>. Range is 8000-50000 (10KHz).}
  gPlatformFspPkgTokenSpaceGuid.CpuBclkOcFrequency          | * | 0x4 | 0x00000000

  # !BSF NAME:{Bitmask of disable cores}
  # !BSF TYPE:{EditNum, HEX, (0x0, 0xFFFFFFFF)}
  # !BSF HELP:{Core mask is a bitwise indication of which core should be disabled. <b>0x00=Default</b>; Bit 0 - core 0, bit 7 - core 7.}
  gPlatformFspPkgTokenSpaceGuid.DisablePerCoreMask          | * | 0x04 | 0x00

  # !BSF NAME:{Bitmask of disable atoms}
  # !BSF TYPE:{EditNum, HEX, (0x0, 0xFFFFFFFF)}
  # !BSF HELP:{DEPRECATED}
  gPlatformFspPkgTokenSpaceGuid.DisablePerAtomMask          | * | 0x04 | 0x00

  # !BSF NAME:{Sa PLL Frequency} TYPE:{Combo}  OPTION:{0: 3200MHz, 1: 1600MHz}
  # !BSF HELP:{Configure Sa PLL Frequency. <b>0: 3200MHz </b>, 1: 1600MHz}
  gPlatformFspPkgTokenSpaceGuid.SaPllFreqOverride           | * | 0x01 | 0x00

  # !BSF PAGE:{MRC}
  # !BSF NAME:{Skip override boot mode When Fw Update.} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{When set to TRUE and boot mode is BOOT_ON_FLASH_UPDATE, skip setting boot mode to BOOT_WITH_FULL_CONFIGURATION in PEI memory init.}
  gPlatformFspPkgTokenSpaceGuid.SiSkipOverrideBootModeWhenFwUpdate  | * | 0x01 | 0x00

  # !BSF NAME:{TSC HW Fixup disable} TYPE:{Combo} OPTION:{0:Enable, 1:Disable}
  # !BSF HELP:{TSC HW Fixup disable during TSC copy from PMA to APIC. <b>0: Enable</b>; 1: Disable}
  gPlatformFspPkgTokenSpaceGuid.TscDisableHwFixup           | * | 0x01 | 0x00

  # !BSF NAME:{Support IA Unlimited ICCMAX} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Support IA Unlimited ICCMAX up to maximum value 512A; <b>0: Disabled</b>; 1: Enabled.}
  gPlatformFspPkgTokenSpaceGuid.IaIccUnlimitedMode          | * | 0x01 | 0x00

  # !BSF NAME:{IA ICCMAX} TYPE:{EditNum, HEX, (0x00,0x7FF)}
  # !BSF HELP:{IA ICCMAX value is represented in 1/4 A increments. A value of 400 = 100A. <b>4 </b>. Range is 4-2047.}
  gPlatformFspPkgTokenSpaceGuid.IaIccMax                    | * | 0x02 | 0x04

  # !BSF NAME:{Support GT Unlimited ICCMAX} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Support GT Unlimited ICCMAX up to maximum value 512A; <b>0: Disabled</b>; 1: Enabled.}
  gPlatformFspPkgTokenSpaceGuid.GtIccUnlimitedMode          | * | 0x01 | 0x00

  # !BSF NAME:{GT ICCMAX} TYPE:{EditNum, HEX, (0x00,0x7FF)}
  # !BSF HELP:{GT ICCMAX value is represented in 1/4 A increments. A value of 400 = 100A. <b>4 </b>. Range is 4-2047.}
  gPlatformFspPkgTokenSpaceGuid.GtIccMax                    | * | 0x02 | 0x04

  # !BSF NAME:{TVB Down Bins for Temp Threshold 0} TYPE:{EditNum, DEC, (0, 10)}
  # !BSF HELP:{Down Bins (delta) for Temperature Threshold 0. When running above Temperature Threshold 0, the ratio will be clipped by MAX_RATIO[n]-This value, when TVB ratio clipping is enabled. Default is 1.}
  gPlatformFspPkgTokenSpaceGuid.TvbDownBinsTempThreshold0   | * | 0x01 | 0x01

  # !BSF NAME:{TVB Temperature Threshold 0} TYPE:{EditNum, DEC, (0, 100)}
  # !BSF HELP:{TVB Temp (degrees C) - Temperature Threshold 0. Running ABOVE this temperature will clip delta Down Bins for Threshold 0 from the resolved OC Ratio, when TVB ratio clipping is enabled. Default is 70.}
  gPlatformFspPkgTokenSpaceGuid.TvbTempThreshold0           | * | 0x01 | 0x46

  # !BSF NAME:{TVB Temperature Threshold 1} TYPE:{EditNum, DEC, (0, 100)}
  # !BSF HELP:{TVB Temp (degrees C) - Temperature Threshold 1. Running ABOVE this temperature will clip delta Down Bins for Threshold 1 from the resolved OC Ratio, when TVB ratio clipping is enabled. Default is 100.}
  gPlatformFspPkgTokenSpaceGuid.TvbTempThreshold1           | * | 0x01 | 0x64

  # !BSF NAME:{TVB Down Bins for Temp Threshold 1} TYPE:{EditNum, DEC, (0, 10)}
  # !BSF HELP:{Down Bins (delta) for Temperature Threshold 1. When running above Temperature Threshold 1, the ratio will be clipped by MAX_RATIO[n]-Down Bin Threshold 1-This value, when TVB ratio clipping is enabled. Default is 2.}
  gPlatformFspPkgTokenSpaceGuid.TvbDownBinsTempThreshold1   | * | 0x01 | 0x02

  # !BSF NAME:{FLL Overclock Mode Enable} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Enable/Disable FLL Mode override with the value in FllOcMode <b>0: Disabled</b>; 1: Enabled.}
  # !BSF HELP:{Select FLL Mode Value from 0 to 3. 0x0 = no overclocking, 0x1 = ratio overclocking with nominal (0.5-1x) reference clock frequency, 0x2 = BCLK overclocking with elevated (1-3x) reference clock frequency, 0x3 = BCLK overclocking with extreme elevated (3-5x) reference clock frequency and ratio limited to 63.}
  gPlatformFspPkgTokenSpaceGuid.FllOcModeEn                 | * | 0x01 | 0x00

  # !BSF NAME:{FLL Overclock Mode} TYPE:{EditNum, HEX, (0x0, 0x3)}
  # !BSF HELP:{Select FLL Mode Value from 0 to 3. 0x0 = no overclocking, 0x1 = ratio overclocking with nominal (0.5-1x) reference clock frequency, 0x2 = BCLK overclocking with elevated (1-3x) reference clock frequency, 0x3 = BCLK overclocking with extreme elevated (3-5x) reference clock frequency and ratio limited to 63.}
  gPlatformFspPkgTokenSpaceGuid.FllOverclockMode            | * | 0x01 | 0x00

  # !BSF NAME:{Configuration for boot TDP selection}
  # !BSF TYPE:{EditNum, HEX, (0x00, 0xFF)}
  # !BSF HELP:{Configuration for boot TDP selection; <b>0: TDP Nominal</b>; 1: TDP Down; 2: TDP Up;0xFF : Deactivate}
  gPlatformFspPkgTokenSpaceGuid.ConfigTdpLevel              | * | 0x01 | 0x00

  # !BSF NAME:{Short term Power Limit value for custom cTDP level 1}
  # !BSF TYPE:{EditNum, HEX, (0x00, 0x3E7F83)}
  # !BSF HELP:{Short term Power Limit value for custom cTDP level 1. Units are based on POWER_MGMT_CONFIG.CustomPowerUnit.Valid Range 0 to 4095875 in Step size of 125}
  gPlatformFspPkgTokenSpaceGuid.CustomPowerLimit1           | * | 0x04 | 0x00

  # !BSF NAME:{Enhanced Thermal Turbo Mode} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{When eTVB mode is enabled user will be clipped when temperatures reach 70C <b>0: Disabled</b>; 1: Enabled.}
  gPlatformFspPkgTokenSpaceGuid.Etvb                 | * | 0x01 | 0x01

  # !BSF NAME:{ReservedCpuPreMem} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Reserved for Cpu Pre-Mem}
  gPlatformFspPkgTokenSpaceGuid.ReservedCpuPreMem           | * | 0x7 | {0x00}

  #
  # CPU Pre-Mem Production Block End
  #

  #
  #  Security Pre-Mem start
  #
  # !BSF PAGE:{SEC1}
  # !BSF NAME:{BiosGuard} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Enable/Disable. 0: Disable, Enable/Disable BIOS Guard feature, 1: enable}
  gPlatformFspPkgTokenSpaceGuid.BiosGuard                   | * | 0x01 | 0x01

  # !BSF NAME :{BiosGuardToolsInterface} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Enable/Disable. 0: Disable, Enable/Disable BiosGuardToolsInterface feature, 1: enable}
  gPlatformFspPkgTokenSpaceGuid.BiosGuardToolsInterface     | * | 0x01 | 0x01

  # !BSF NAME:{Txt} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Enable/Disable. 0: Disable, Enable/Disable Txt feature, 1: enable}
  gPlatformFspPkgTokenSpaceGuid.Txt                         | * | 0x01 | 0x00

  # !BSF NAME:{PrmrrSize}  TYPE:{EditNum, HEX, (0x00,0xFFFFFFFF)}
  # !BSF HELP:{Enable/Disable. 0: Disable, define default value of PrmrrSize , 1: enable}
  # Supported values: 32MB: 0x2000000, 64MB: 0x4000000, 128 MB: 0x8000000, 256 MB: 0x10000000, 512 MB: 0x20000000
  gPlatformFspPkgTokenSpaceGuid.PrmrrSize                   | * | 0x04 | 0x00000000

  # !BSF NAME:{SinitMemorySize}  TYPE:{EditNum, HEX, (0x00,0xFFFFFFFF)}
  # !BSF HELP:{Enable/Disable. 0: Disable, define default value of SinitMemorySize , 1: enable}
  # TODO: Check if default 0 is ok
  gPlatformFspPkgTokenSpaceGuid.SinitMemorySize             | * | 0x04 | 0x00000000

  # !BSF NAME:{TxtDprMemoryBase}  TYPE:{EditNum, HEX, (0x00,0xFFFFFFFFFFFFFFFF)}
  # !BSF HELP:{Enable/Disable. 0: Disable, define default value of TxtDprMemoryBase , 1: enable}
  # TODO: Check if default 0 is ok
  gPlatformFspPkgTokenSpaceGuid.TxtDprMemoryBase            | * | 0x08 | 0x0000000000000000

  # !BSF NAME:{TxtHeapMemorySize}  TYPE:{EditNum, HEX, (0x00,0xFFFFFFFF)}
  # !BSF HELP:{Enable/Disable. 0: Disable, define default value of TxtHeapMemorySize , 1: enable}
  # TODO: Check if default 0 is ok
  gPlatformFspPkgTokenSpaceGuid.TxtHeapMemorySize           | * | 0x04 | 0x00000000

  # !BSF NAME:{TxtDprMemorySize}  TYPE:{EditNum, HEX, (0x00,0xFFFFFFFF)}
  # !BSF HELP:{Enable/Disable. 0: Disable, define default value of TxtDprMemorySize , 1: enable}
  # TODO: Check if default 0 is ok
  gPlatformFspPkgTokenSpaceGuid.TxtDprMemorySize            | * | 0x04 | 0x00000000

  # !BSF NAME:{BiosAcmBase}  TYPE:{EditNum, HEX, (0x00,0xFFFFFFFFFFFFFFFF)}
  # !BSF HELP:{Enable/Disable. 0: Disable, define default value of BiosAcmBase , 1: enable}
  #TODO: This value is obtained from flash using GUID - so keeping the default value as 0
  gPlatformFspPkgTokenSpaceGuid.BiosAcmBase                 | * | 0x04 | 0x00000000

  # !BSF NAME:{BiosAcmSize}  TYPE:{EditNum, HEX, (0x00,0xFFFFFFFF)}
  # !BSF HELP:{Enable/Disable. 0: Disable, define default value of BiosAcmSize , 1: enable}
  gPlatformFspPkgTokenSpaceGuid.BiosAcmSize                 | * | 0x04 | 0x00000000

  # !BSF NAME:{ApStartupBase}  TYPE:{EditNum, HEX, (0x00,0xFFFFFFFF)}
  # !BSF HELP:{Enable/Disable. 0: Disable, define default value of BiosAcmBase , 1: enable}
  #TODO: This value is obtained from flash using GUID - so keeping the default value as 0
  gPlatformFspPkgTokenSpaceGuid.ApStartupBase               | * | 0x04 | 0x00000000

  # !BSF NAME:{TgaSize}  TYPE:{EditNum, HEX, (0x00,0xFFFFFFFF)}
  # !BSF HELP:{Enable/Disable. 0: Disable, define default value of TgaSize , 1: enable}
  #TODO: Check if default 0 is ok
  gPlatformFspPkgTokenSpaceGuid.TgaSize                     | * | 0x04 | 0x00000000

  # !BSF NAME:{TxtLcpPdBase}  TYPE:{EditNum, HEX, (0x00,0xFFFFFFFFFFFFFFFF)}
  # !BSF HELP:{Enable/Disable. 0: Disable, define default value of TxtLcpPdBase , 1: enable}
  #TODO: Check if default 0 is ok
  gPlatformFspPkgTokenSpaceGuid.TxtLcpPdBase                | * | 0x08 | 0x0000000000000000

  # !BSF NAME:{TxtLcpPdSize}  TYPE:{EditNum, HEX, (0x00,0xFFFFFFFFFFFFFFFF)}
  # !BSF HELP:{Enable/Disable. 0: Disable, define default value of TxtLcpPdSize , 1: enable}
  #TODO: Check if default 0 is ok
  gPlatformFspPkgTokenSpaceGuid.TxtLcpPdSize                | * | 0x08 | 0x0000000000000000

  # !BSF NAME:{IsTPMPresence} TYPE:{EditNum, HEX, (0x00,0xFF)}
  # !BSF HELP:{IsTPMPresence default values}
  gPlatformFspPkgTokenSpaceGuid.IsTPMPresence               | * | 0x1 | 0x0

  # !BSF NAME:{ReservedSecurityPreMem} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Reserved for Security Pre-Mem}
  gPlatformFspPkgTokenSpaceGuid.ReservedSecurityPreMem      | * | 0x20 | {0x00}

  #
  #  Security Pre-Mem End
  #

  #
  #  PCH Pre-Mem start
  #
  # !BSF PAGE:{PCH1}
  # !BSF NAME:{Enable PCH HSIO PCIE Rx Set Ctle} TYPE:{EditNum, HEX, (0x00,0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF)}
  # !BSF HELP:{Enable PCH PCIe Gen 3 Set CTLE Value.}
  gPlatformFspPkgTokenSpaceGuid.PchPcieHsioRxSetCtleEnable          | * | 0x1C | { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 }

  # !BSF NAME:{PCH HSIO PCIE Rx Set Ctle Value} TYPE:{EditNum, HEX, (0x00,0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF)}
  # !BSF HELP:{PCH PCIe Gen 3 Set CTLE Value.}
  gPlatformFspPkgTokenSpaceGuid.PchPcieHsioRxSetCtle                | * | 0x1C | { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 }

  # !BSF NAME:{Enble PCH HSIO PCIE TX Gen 1 Downscale Amplitude Adjustment value override} TYPE:{EditNum, HEX, (0x00,0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF)}
  # !BSF HELP:{0: Disable; 1: Enable.}
  gPlatformFspPkgTokenSpaceGuid.PchPcieHsioTxGen1DownscaleAmpEnable | * | 0x1C | { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 }

  # !BSF NAME:{PCH HSIO PCIE Gen 2 TX Output Downscale Amplitude Adjustment value} TYPE:{EditNum, HEX, (0x00,0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF)}
  # !BSF HELP:{PCH PCIe Gen 2 TX Output Downscale Amplitude Adjustment value.}
  gPlatformFspPkgTokenSpaceGuid.PchPcieHsioTxGen1DownscaleAmp       | * | 0x1C | { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 }

  # !BSF NAME:{Enable PCH HSIO PCIE TX Gen 2 Downscale Amplitude Adjustment value override} TYPE:{EditNum, HEX, (0x00,0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF)}
  # !BSF HELP:{0: Disable; 1: Enable.}
  gPlatformFspPkgTokenSpaceGuid.PchPcieHsioTxGen2DownscaleAmpEnable | * | 0x1C | { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 }

  # !BSF NAME:{PCH HSIO PCIE Gen 2 TX Output Downscale Amplitude Adjustment value} TYPE:{EditNum, HEX, (0x00,0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF)}
  # !BSF HELP:{PCH PCIe Gen 2 TX Output Downscale Amplitude Adjustment value.}
  gPlatformFspPkgTokenSpaceGuid.PchPcieHsioTxGen2DownscaleAmp       | * | 0x1C | { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 }

  # !BSF NAME:{Enable PCH HSIO PCIE TX Gen 3 Downscale Amplitude Adjustment value override} TYPE:{EditNum, HEX, (0x00,0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF)}
  # !BSF HELP:{0: Disable; 1: Enable.}
  gPlatformFspPkgTokenSpaceGuid.PchPcieHsioTxGen3DownscaleAmpEnable | * | 0x1C | { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 }

  # !BSF NAME:{PCH HSIO PCIE Gen 3 TX Output Downscale Amplitude Adjustment value} TYPE:{EditNum, HEX, (0x00,0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF)}
  # !BSF HELP:{PCH PCIe Gen 3 TX Output Downscale Amplitude Adjustment value.}
  gPlatformFspPkgTokenSpaceGuid.PchPcieHsioTxGen3DownscaleAmp       | * | 0x1C | { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 }

  # !BSF NAME:{Enable PCH HSIO PCIE Gen 1 TX Output De-Emphasis Adjustment Setting value override} TYPE:{EditNum, HEX, (0x00,0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF)}
  # !BSF HELP:{0: Disable; 1: Enable.}
  gPlatformFspPkgTokenSpaceGuid.PchPcieHsioTxGen1DeEmphEnable       | * | 0x1C | { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 }

  # !BSF NAME:{PCH HSIO PCIE Gen 1 TX Output De-Emphasis Adjustment value} TYPE:{EditNum, HEX, (0x00,0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF)}
  # !BSF HELP:{PCH PCIe Gen 1 TX Output De-Emphasis Adjustment Setting.}
  gPlatformFspPkgTokenSpaceGuid.PchPcieHsioTxGen1DeEmph             | * | 0x1C | { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 }

  # !BSF NAME:{Enable PCH HSIO PCIE Gen 2 TX Output -3.5dB De-Emphasis Adjustment Setting value override} TYPE:{EditNum, HEX, (0x00,0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF)}
  # !BSF HELP:{0: Disable; 1: Enable.}
  gPlatformFspPkgTokenSpaceGuid.PchPcieHsioTxGen2DeEmph3p5Enable    | * | 0x1C | { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 }

  # !BSF NAME:{PCH HSIO PCIE Gen 2 TX Output -3.5dB De-Emphasis Adjustment value} TYPE:{EditNum, HEX, (0x00,0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF)}
  # !BSF HELP:{PCH PCIe Gen 2 TX Output -3.5dB De-Emphasis Adjustment Setting.}
  gPlatformFspPkgTokenSpaceGuid.PchPcieHsioTxGen2DeEmph3p5          | * | 0x1C | { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 }

  # !BSF NAME:{Enable PCH HSIO PCIE Gen 2 TX Output -6.0dB De-Emphasis Adjustment Setting value override} TYPE:{EditNum, HEX, (0x00,0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF)}
  # !BSF HELP:{0: Disable; 1: Enable.}
  gPlatformFspPkgTokenSpaceGuid.PchPcieHsioTxGen2DeEmph6p0Enable    | * | 0x1C | { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 }

  # !BSF NAME:{PCH HSIO PCIE Gen 2 TX Output -6.0dB De-Emphasis Adjustment value} TYPE:{EditNum, HEX, (0x00,0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF)}
  # !BSF HELP:{PCH PCIe Gen 2 TX Output -6.0dB De-Emphasis Adjustment Setting.}
  gPlatformFspPkgTokenSpaceGuid.PchPcieHsioTxGen2DeEmph6p0          | * | 0x1C | { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 }

  # !BSF NAME:{Enable PCH HSIO SATA Receiver Equalization Boost Magnitude Adjustment Value override} TYPE:{EditNum, HEX, (0x00,0xFFFFFFFFFFFFFFFF)}
  # !BSF HELP:{0: Disable; 1: Enable.}
  gPlatformFspPkgTokenSpaceGuid.PchSataHsioRxGen1EqBoostMagEnable   | * | 0x08 | { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 }

  # !BSF NAME:{PCH HSIO SATA 1.5 Gb/s Receiver Equalization Boost Magnitude Adjustment value} TYPE:{EditNum, HEX, (0x00,0xFFFFFFFFFFFFFFFF)}
  # !BSF HELP:{PCH HSIO SATA 1.5 Gb/s Receiver Equalization Boost Magnitude Adjustment value.}
  gPlatformFspPkgTokenSpaceGuid.PchSataHsioRxGen1EqBoostMag         | * | 0x08 | { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 }

  # !BSF NAME:{Enable PCH HSIO SATA Receiver Equalization Boost Magnitude Adjustment Value override} TYPE:{EditNum, HEX, (0x00,0xFFFFFFFFFFFFFFFF)}
  # !BSF HELP:{0: Disable; 1: Enable.}
  gPlatformFspPkgTokenSpaceGuid.PchSataHsioRxGen2EqBoostMagEnable   | * | 0x08 | { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 }

  # !BSF NAME:{PCH HSIO SATA 3.0 Gb/s Receiver Equalization Boost Magnitude Adjustment value} TYPE:{EditNum, HEX, (0x00,0xFFFFFFFFFFFFFFFF)}
  # !BSF HELP:{PCH HSIO SATA 3.0 Gb/s Receiver Equalization Boost Magnitude Adjustment value.}
  gPlatformFspPkgTokenSpaceGuid.PchSataHsioRxGen2EqBoostMag         | * | 0x08 | { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 }

  # !BSF NAME:{Enable PCH HSIO SATA Receiver Equalization Boost Magnitude Adjustment Value override} TYPE:{EditNum, HEX, (0x00,0xFFFFFFFFFFFFFFFF)}
  # !BSF HELP:{0: Disable; 1: Enable.}
  gPlatformFspPkgTokenSpaceGuid.PchSataHsioRxGen3EqBoostMagEnable   | * | 0x08 | { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 }

  # !BSF NAME:{PCH HSIO SATA 6.0 Gb/s Receiver Equalization Boost Magnitude Adjustment value} TYPE:{EditNum, HEX, (0x00,0xFFFFFFFFFFFFFFFF)}
  # !BSF HELP:{PCH HSIO SATA 6.0 Gb/s Receiver Equalization Boost Magnitude Adjustment value.}
  gPlatformFspPkgTokenSpaceGuid.PchSataHsioRxGen3EqBoostMag         | * | 0x08 | { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 }

  # !BSF NAME:{Enable PCH HSIO SATA 1.5 Gb/s TX Output Downscale Amplitude Adjustment value override} TYPE:{EditNum, HEX, (0x00,0xFFFFFFFFFFFFFFFF)}
  # !BSF HELP:{0: Disable; 1: Enable.}
  gPlatformFspPkgTokenSpaceGuid.PchSataHsioTxGen1DownscaleAmpEnable | * | 0x08 | { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 }

  # !BSF NAME:{PCH HSIO SATA 1.5 Gb/s TX Output Downscale Amplitude Adjustment value} TYPE:{EditNum, HEX, (0x00,0xFFFFFFFFFFFFFFFF)}
  # !BSF HELP:{PCH HSIO SATA 1.5 Gb/s TX Output Downscale Amplitude Adjustment value.}
  gPlatformFspPkgTokenSpaceGuid.PchSataHsioTxGen1DownscaleAmp       | * | 0x08 | { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 }

  # !BSF NAME:{Enable PCH HSIO SATA 3.0 Gb/s TX Output Downscale Amplitude Adjustment value override} TYPE:{EditNum, HEX, (0x00,0xFFFFFFFFFFFFFFFF)}
  # !BSF HELP:{0: Disable; 1: Enable.}
  gPlatformFspPkgTokenSpaceGuid.PchSataHsioTxGen2DownscaleAmpEnable | * | 0x08 | { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 }

  # !BSF NAME:{PCH HSIO SATA 3.0 Gb/s TX Output Downscale Amplitude Adjustment value} TYPE:{EditNum, HEX, (0x00,0xFFFFFFFFFFFFFFFF)}
  # !BSF HELP:{PCH HSIO SATA 3.0 Gb/s TX Output Downscale Amplitude Adjustment value.}
  gPlatformFspPkgTokenSpaceGuid.PchSataHsioTxGen2DownscaleAmp       | * | 0x08 | { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 }

  # !BSF NAME:{Enable PCH HSIO SATA 6.0 Gb/s TX Output Downscale Amplitude Adjustment value override} TYPE:{EditNum, HEX, (0x00,0xFFFFFFFFFFFFFFFF)}
  # !BSF HELP:{0: Disable; 1: Enable.}
  gPlatformFspPkgTokenSpaceGuid.PchSataHsioTxGen3DownscaleAmpEnable | * | 0x08 | { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 }

  # !BSF NAME:{PCH HSIO SATA 6.0 Gb/s TX Output Downscale Amplitude Adjustment value} TYPE:{EditNum, HEX, (0x00,0xFFFFFFFFFFFFFFFF)}
  # !BSF HELP:{PCH HSIO SATA 6.0 Gb/s TX Output Downscale Amplitude Adjustment value.}
  gPlatformFspPkgTokenSpaceGuid.PchSataHsioTxGen3DownscaleAmp       | * | 0x08 | { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 }

  # !BSF NAME:{Enable PCH HSIO SATA 1.5 Gb/s TX Output De-Emphasis Adjustment Setting value override} TYPE:{EditNum, HEX, (0x00,0xFFFFFFFFFFFFFFFF)}
  # !BSF HELP:{0: Disable; 1: Enable.}
  gPlatformFspPkgTokenSpaceGuid.PchSataHsioTxGen1DeEmphEnable       | * | 0x08 | { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 }

  # !BSF NAME:{PCH HSIO SATA 1.5 Gb/s TX Output De-Emphasis Adjustment Setting} TYPE:{EditNum, HEX, (0x00,0xFFFFFFFFFFFFFFFF)}
  # !BSF HELP:{PCH HSIO SATA 1.5 Gb/s TX Output De-Emphasis Adjustment Setting.}
  gPlatformFspPkgTokenSpaceGuid.PchSataHsioTxGen1DeEmph             | * | 0x08 | { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 }

  # !BSF NAME:{Enable PCH HSIO SATA 3.0 Gb/s TX Output De-Emphasis Adjustment Setting value override} TYPE:{EditNum, HEX, (0x00,0xFFFFFFFFFFFFFFFF)}
  # !BSF HELP:{0: Disable; 1: Enable.}
  gPlatformFspPkgTokenSpaceGuid.PchSataHsioTxGen2DeEmphEnable       | * | 0x08 | { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 }

  # !BSF NAME:{PCH HSIO SATA 3.0 Gb/s TX Output De-Emphasis Adjustment Setting} TYPE:{EditNum, HEX, (0x00,0xFFFFFFFFFFFFFFFF)}
  # !BSF HELP:{PCH HSIO SATA 3.0 Gb/s TX Output De-Emphasis Adjustment Setting.}
  gPlatformFspPkgTokenSpaceGuid.PchSataHsioTxGen2DeEmph             | * | 0x08 | { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 }

  # !BSF NAME:{Enable PCH HSIO SATA 6.0 Gb/s TX Output De-Emphasis Adjustment Setting value override} TYPE:{EditNum, HEX, (0x00,0xFFFFFFFFFFFFFFFF)}
  # !BSF HELP:{0: Disable; 1: Enable.}
  gPlatformFspPkgTokenSpaceGuid.PchSataHsioTxGen3DeEmphEnable       | * | 0x08 | { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 }

  # !BSF NAME:{PCH HSIO SATA 6.0 Gb/s TX Output De-Emphasis Adjustment Setting} TYPE:{EditNum, HEX, (0x00,0xFFFFFFFFFFFFFFFF)}
  # !BSF HELP:{PCH HSIO SATA 6.0 Gb/s TX Output De-Emphasis Adjustment Setting.}
  gPlatformFspPkgTokenSpaceGuid.PchSataHsioTxGen3DeEmph             | * | 0x08 | { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 }

  # !BSF PAGE:{DBG}
  # !BSF NAME:{PCH LPC Enhanced Port 80 Decoding} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Original LPC only decodes one byte of port 80h.}
  gPlatformFspPkgTokenSpaceGuid.PchLpcEnhancePort8xhDecoding        | * | 0x01 | 0x01

  # !BSF NAME:{PCH Port80 Route} TYPE:{Combo} OPTION:{0:LPC, 1:PCI}
  # !BSF HELP:{Control where the Port 80h cycles are sent, 0: LPC; 1: PCI.}
  gPlatformFspPkgTokenSpaceGuid.PchPort80Route              | * | 0x01 | 0x00
  # !BSF PAGE:{PCH1}


  # !BSF NAME:{Enable SMBus ARP support} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Enable SMBus ARP support.}
  gPlatformFspPkgTokenSpaceGuid.SmbusArpEnable              | * | 0x01 | 0x00

  # !BSF NAME:{Number of RsvdSmbusAddressTable.} TYPE:{EditNum, HEX, (0x00,0xFF)}
  # !BSF HELP:{The number of elements in the RsvdSmbusAddressTable.}
  gPlatformFspPkgTokenSpaceGuid.PchNumRsvdSmbusAddresses    | * | 0x01 | 0x00

  # !BSF NAME:{SMBUS Base Address} TYPE:{EditNum, HEX, (0x00,0xFFFF)}
  # !BSF HELP:{SMBUS Base Address (IO space).}
  gPlatformFspPkgTokenSpaceGuid.PchSmbusIoBase              | * | 0x02 | 0xEFA0

  # !BSF NAME:{Enable SMBus Alert Pin} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Enable SMBus Alert Pin.}
  gPlatformFspPkgTokenSpaceGuid.PchSmbAlertEnable           | * | 0x01 | 0x00

  # !BSF NAME:{Usage type for ClkSrc} TYPE:{EditNum, HEX, (0x00,0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF)}
  # !BSF HELP:{0-23: PCH rootport, 0x40-0x43: PEG port, 0x70:LAN, 0x80: unspecified but in use (free running), 0xFF: not used}
  gPlatformFspPkgTokenSpaceGuid.PcieClkSrcUsage             | * | 0x12 | { 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF }
  gPlatformFspPkgTokenSpaceGuid.PcieClkSrcUsageRsvd         | * | 0x0E | {0x00}


  # !BSF NAME:{ClkReq-to-ClkSrc mapping} TYPE:{EditNum, HEX, (0x00,0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF)}
  # !BSF HELP:{Number of ClkReq signal assigned to ClkSrc}
  gPlatformFspPkgTokenSpaceGuid.PcieClkSrcClkReq            | * | 0x12 | { 0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0A, 0x0B, 0x0C, 0x0D, 0x0E, 0x0F, 0x00, 0x01 }
  gPlatformFspPkgTokenSpaceGuid.PcieClkSrcClkReqRsvd        | * | 0x0E | {0x00}

  # !BSF NAME:{Clk Req GPIO Pin} TYPE:{EditNum, HEX, (0,0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF)}
  # !HDR STRUCT:{UINT32}
  # !BSF HELP:{Select Clk Req Pin. Refer to GPIO_*_MUXING_SRC_CLKREQ_x* for possible values.}
  gPlatformFspPkgTokenSpaceGuid.PcieClkReqGpioMux     | * | 0x48 | {0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0 }

  ## only need 4 bytes to use point for this policy
  # !BSF NAME:{Point of RsvdSmbusAddressTable} TYPE:{EditNum, HEX, (0x00,0xFFFFFFFF)}
  # !BSF HELP:{Array of addresses reserved for non-ARP-capable SMBus devices.}
  gPlatformFspPkgTokenSpaceGuid.RsvdSmbusAddressTablePtr    | * | 0x04 | 0x00000000

  # !BSF NAME:{Enable PCIE RP Mask} TYPE:{EditNum, HEX, (0x00,0x00FFFFFF)}
  # !BSF HELP:{Enable/disable PCIE Root Ports. 0: disable, 1: enable. One bit for each port, bit0 for port1, bit1 for port2, and so on.}
  gPlatformFspPkgTokenSpaceGuid.PcieRpEnableMask            | * | 0x04 | 0x00FFFFFF

  # !BSF NAME:{VC Type} TYPE:{Combo} OPTION:{0: VC0, 1: VC1}
  # !BSF HELP:{Virtual Channel Type Select: 0: VC0, 1: VC1.}
  gPlatformFspPkgTokenSpaceGuid.PchHdaVcType                | * | 0x01 | 0x00

  # !BSF NAME:{Universal Audio Architecture compliance for DSP enabled system} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{0: Not-UAA Compliant (Intel SST driver supported only), 1: UAA Compliant (HDA Inbox driver or SST driver supported).}
  gPlatformFspPkgTokenSpaceGuid.PchHdaDspUaaCompliance      | * | 0x01 | 0x00

  # !BSF NAME:{Enable HD Audio Link} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Enable/disable HD Audio Link. Muxed with SSP0/SSP1/SNDW1.}
  gPlatformFspPkgTokenSpaceGuid.PchHdaAudioLinkHdaEnable    | * | 0x01 | 0x01

  # !BSF NAME:{Enable HDA SDI lanes} TYPE:{EditNum, HEX, (0x00,0xFFFF)}
  # !BSF HELP:{Enable/disable HDA SDI lanes.}^M
  gPlatformFspPkgTokenSpaceGuid.PchHdaSdiEnable            | * | 0x02 | { 0x01, 0x00}

  # !BSF NAME:{HDA Power/Clock Gating (PGD/CGD)} TYPE:{Combo}
  # !BSF OPTION:{0: POR, 1: Force Enable, 2: Force Disable}
  # !BSF HELP:{Enable/Disable HD Audio Power and Clock Gating(POR: Enable). 0: PLATFORM_POR, 1: FORCE_ENABLE, 2: FORCE_DISABLE.}
  gPlatformFspPkgTokenSpaceGuid.PchHdaTestPowerClockGating  | * | 0x01 | 0x00

  # !BSF NAME:{Enable HD Audio DMIC_N Link} TYPE:{EditNum, HEX, (0x00,0xFFFF)}
  # !BSF HELP:{Enable/disable HD Audio DMIC1 link. Muxed with SNDW3.}
  gPlatformFspPkgTokenSpaceGuid.PchHdaAudioLinkDmicEnable  | * | 0x02 | { 0x01, 0x01}

  # !BSF NAME:{DMIC<N> ClkA Pin Muxing (N - DMIC number)} TYPE:{EditNum, HEX, (0x00,0xFFFFFFFFFFFFFFFF)}
  # !BSF HELP:{Determines DMIC<N> ClkA Pin muxing. See  GPIO_*_MUXING_DMIC<N>_CLKA_*}
  # !HDR STRUCT:{UINT32}
  gPlatformFspPkgTokenSpaceGuid.PchHdaAudioLinkDmicClkAPinMux | * | 0x8 | { 0x0, 0x0 }

  # !BSF NAME:{DMIC<N> ClkB Pin Muxing} TYPE:{EditNum, HEX, (0x00,0xFFFFFFFFFFFFFFFF)}
  # !BSF HELP:{Determines DMIC<N> ClkA Pin muxing. See GPIO_*_MUXING_DMIC<N>_CLKB_*}
  # !HDR STRUCT:{UINT32}^M
  gPlatformFspPkgTokenSpaceGuid.PchHdaAudioLinkDmicClkBPinMux | * | 0x8 | { 0x0, 0x0 }

  # !BSF NAME:{Enable HD Audio DSP} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Enable/disable HD Audio DSP feature.}
  gPlatformFspPkgTokenSpaceGuid.PchHdaDspEnable             | * | 0x01 | 0x01

  # !BSF NAME:{DMIC<N> Data Pin Muxing} TYPE:{EditNum, HEX, (0x00,0xFFFFFFFFFFFFFFFF)}
  # !BSF HELP:{Determines DMIC<N> Data Pin muxing. See GPIO_*_MUXING_DMIC<N>_DATA_*}
  # !HDR STRUCT:{UINT32}
  gPlatformFspPkgTokenSpaceGuid.PchHdaAudioLinkDmicDataPinMux | * | 0x8 | {0x00000000, 0x00000000}

  # !BSF NAME:{Enable HD Audio SSP0 Link} TYPE:{EditNum, HEX, (0x00, 0xFFFFFFFFFFFF)}
  # !BSF HELP:{Enable/disable HD Audio SSP_N/I2S link. Muxed with HDA. N-number 0-5}
  gPlatformFspPkgTokenSpaceGuid.PchHdaAudioLinkSspEnable    | * | 0x06 | { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 }

  # !BSF NAME:{Enable HD Audio SoundWire#N Link} TYPE:{EditNum, HEX, (0x00,0xFFFFFFFF)}
  # !BSF HELP:{Enable/disable HD Audio SNDW#N link. Muxed with HDA.}
  gPlatformFspPkgTokenSpaceGuid.PchHdaAudioLinkSndwEnable  | * | 0x04 | {0x01, 0x00, 0x00, 0x00}

  # !BSF NAME:{iDisp-Link Frequency} TYPE:{Combo} OPTION:{4: 96MHz, 3: 48MHz}
  # !BSF HELP:{iDisp-Link Freq (PCH_HDAUDIO_LINK_FREQUENCY enum): 4: 96MHz, 3: 48MHz.}
  gPlatformFspPkgTokenSpaceGuid.PchHdaIDispLinkFrequency    | * | 0x01 | 0x04

  # !BSF NAME:{iDisp-Link T-mode} TYPE:{Combo} OPTION:{0: 2T, 2: 4T, 3: 8T, 4: 16T}
  # !BSF HELP:{iDisp-Link T-Mode (PCH_HDAUDIO_IDISP_TMODE enum): 0: 2T, 2: 4T, 3: 8T, 4: 16T}
  gPlatformFspPkgTokenSpaceGuid.PchHdaIDispLinkTmode        | * | 0x01 | 0x03

  # !BSF NAME:{iDisplay Audio Codec disconnection} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{0: Not disconnected, enumerable, 1: Disconnected SDI, not enumerable.}
  gPlatformFspPkgTokenSpaceGuid.PchHdaIDispCodecDisconnect  | * | 0x01 | 0x00

  # !BSF NAME:{CNVi DDR RFI Mitigation} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Enable/Disable DDR RFI Mitigation. Default is ENABLE. 0: DISABLE, 1: ENABLE}
  gPlatformFspPkgTokenSpaceGuid.CnviDdrRfim                 | * | 0x01 | 0x01

  #
  #  PCH Pre-Mem End
  #

  # !BSF PAGE:{DBG}
  # !BSF NAME:{Debug Interfaces} TYPE:{EditNum, HEX, (0x00,0x3F)}
  # !BSF HELP:{Debug Interfaces. BIT0-RAM, BIT1-UART, BIT3-USB3, BIT4-Serial IO, BIT5-TraceHub, BIT2 - Not used.}
  gPlatformFspPkgTokenSpaceGuid.PcdDebugInterfaceFlags                  | * | 0x01 | 0x32

!if gSiPkgTokenSpaceGuid.PcdAdlLpSupport == TRUE
  # !BSF NAME:{Serial Io Uart Debug Controller Number} TYPE:{Combo}
  # !BSF OPTION:{0:SerialIoUart0, 1:SerialIoUart1, 2:SerialIoUart2}
  # !BSF HELP:{Select SerialIo Uart Controller for debug. Note: If UART0 is selected as CNVi BT Core interface, it cannot be used for debug purpose.}
  gPlatformFspPkgTokenSpaceGuid.SerialIoUartDebugControllerNumber   | * | 0x01 | 0x0
!else
  # !BSF NAME:{Serial Io Uart Debug Controller Number} TYPE:{Combo}
  # !BSF OPTION:{0:SerialIoUart0, 1:SerialIoUart1, 2:SerialIoUart2}
  # !BSF HELP:{Select SerialIo Uart Controller for debug. Note: If UART0 is selected as CNVi BT Core interface, it cannot be used for debug purpose.}
  gPlatformFspPkgTokenSpaceGuid.SerialIoUartDebugControllerNumber   | * | 0x01 | 0x2
!endif

  # !BSF NAME:{Serial Io Uart Debug Auto Flow} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Enables UART hardware flow control, CTS and RTS lines.}
  gPlatformFspPkgTokenSpaceGuid.SerialIoUartDebugAutoFlow           | * | 0x01 | 0x0

  # !BSF NAME:{Serial Io Uart Debug BaudRate} TYPE:{EditNum, DEC, (0,6000000)}
  # !BSF HELP:{Set default BaudRate Supported from 0 - default to 6000000. Recommended values 9600, 19200, 57600, 115200, 460800, 921600, 1500000, 1843200, 3000000, 3686400, 6000000}
  gPlatformFspPkgTokenSpaceGuid.SerialIoUartDebugBaudRate           | * | 0x4 | 115200

  # !BSF NAME:{Serial Io Uart Debug Parity} TYPE:{Combo}
  # !BSF OPTION:{0: DefaultParity, 1: NoParity, 2: EvenParity, 3: OddParity}
  # !BSF HELP:{Set default Parity.}
  gPlatformFspPkgTokenSpaceGuid.SerialIoUartDebugParity             | * | 0x01 | 0x1

  # !BSF NAME:{Serial Io Uart Debug Stop Bits} TYPE:{Combo}
  # !BSF OPTION:{0: DefaultStopBits, 1: OneStopBit, 2: OneFiveStopBits, 3: TwoStopBits}
  # !BSF HELP:{Set default stop bits.}
  gPlatformFspPkgTokenSpaceGuid.SerialIoUartDebugStopBits           | * | 0x01 | 0x1

  # !BSF NAME:{Serial Io Uart Debug Data Bits} TYPE:{EditNum, HEX, (0x0,0x08)}
  # !BSF OPTION:{5:5BITS, 6:6BITS, 7:7BITS, 8:8BITS}
  # !BSF HELP:{Set default word length. 0: Default, 5,6,7,8}
  gPlatformFspPkgTokenSpaceGuid.SerialIoUartDebugDataBits           | * | 0x01 | 0x8

  # !BSF NAME:{Serial Io Uart Debug Mmio Base} TYPE:{EditNum, HEX, (0,0xFFFFFFFF)}
  # !BSF HELP:{Select SerialIo Uart default MMIO resource in SEC/PEI phase when PcdSerialIoUartMode = SerialIoUartPci.}
  gPlatformFspPkgTokenSpaceGuid.SerialIoUartDebugMmioBase           | * | 0x04 | 0xFE036000

  # Pch Serial IO Uart Debug Configuration End

  # !BSF PAGE:{PCH1}
  # !BSF NAME:{ISA Serial Base selection} TYPE:{Combo}
  # !BSF OPTION:{0:0x3F8, 1:0x2F8}
  # !BSF HELP:{Select ISA Serial Base address. Default is 0x3F8.}
  gPlatformFspPkgTokenSpaceGuid.PcdIsaSerialUartBase                 | * | 0x01 | 0x00

  # !BSF PAGE:{SA1}
  # !BSF NAME:{GT PLL voltage offset}
  # !BSF TYPE:{EditNum, HEX, (0x00,0x0F)}
  # !BSF HELP:{Core PLL voltage offset. <b>0: No offset</b>. Range 0-15}
  gPlatformFspPkgTokenSpaceGuid.GtPllVoltageOffset        | * | 0x01 | 0x00

  # !BSF NAME:{Ring PLL voltage offset}
  # !BSF TYPE:{EditNum, HEX, (0x00,0x0F)}
  # !BSF HELP:{Core PLL voltage offset. <b>0: No offset</b>. Range 0-15}
  gPlatformFspPkgTokenSpaceGuid.RingPllVoltageOffset        | * | 0x01 | 0x00

  # !BSF NAME:{System Agent PLL voltage offset}
  # !BSF TYPE:{EditNum, HEX, (0x00,0x0F)}
  # !BSF HELP:{Core PLL voltage offset. <b>0: No offset</b>. Range 0-15}
  gPlatformFspPkgTokenSpaceGuid.SaPllVoltageOffset        | * | 0x01 | 0x00

  # !BSF NAME:{Memory Controller PLL voltage offset}
  # !BSF TYPE:{EditNum, HEX, (0x00,0x0F)}
  # !BSF HELP:{Core PLL voltage offset. <b>0: No offset</b>. Range 0-15}
  gPlatformFspPkgTokenSpaceGuid.McPllVoltageOffset        | * | 0x01 | 0x00

  # !BSF PAGE:{MRC}
  # !BSF NAME:{MRC Safe Config}
  # !BSF OPTION:{$EN_DIS}
  # !BSF HELP:{Enables/Disable MRC Safe Config}
  gPlatformFspPkgTokenSpaceGuid.MrcSafeConfig             | * | 0x01 | 0x00

  # !BSF PAGE:{TCSS1}
  # !BSF NAME:{TCSS Thunderbolt PCIE Root Port 0 Enable}
  # !BSF TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Set TCSS Thunderbolt PCIE Root Port 0. 0:Disabled  1:Enabled}
  gPlatformFspPkgTokenSpaceGuid.TcssItbtPcie0En           | * | 0x01 | 0x01

  # !BSF NAME:{TCSS Thunderbolt PCIE Root Port 1 Enable}}
  # !BSF TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Set TCSS Thunderbolt PCIE Root Port 1. 0:Disabled  1:Enabled}
  gPlatformFspPkgTokenSpaceGuid.TcssItbtPcie1En           | * | 0x01 | 0x01

  # !BSF NAME:{TCSS Thunderbolt PCIE Root Port 2 Enable}}
  # !BSF TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Set TCSS Thunderbolt PCIE Root Port 2. 0:Disabled  1:Enabled}
  gPlatformFspPkgTokenSpaceGuid.TcssItbtPcie2En           | * | 0x01 | 0x01

  # !BSF NAME:{TCSS Thunderbolt PCIE Root Port 3 Enable}}
  # !BSF TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Set TCSS Thunderbolt PCIE Root Port 3. 0:Disabled  1:Enabled}
  gPlatformFspPkgTokenSpaceGuid.TcssItbtPcie3En           | * | 0x01 | 0x01

  # !BSF NAME:{TCSS USB HOST (xHCI) Enable}}
  # !BSF TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Set TCSS XHCI. 0:Disabled  1:Enabled - Must be enabled if xDCI is enabled below}
  gPlatformFspPkgTokenSpaceGuid.TcssXhciEn                | * | 0x01 | 0x01

  # !BSF NAME:{TCSS USB DEVICE (xDCI) Enable}}
  # !BSF TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Set TCSS XDCI. 0:Disabled  1:Enabled - xHCI must be enabled if xDCI is enabled}
  gPlatformFspPkgTokenSpaceGuid.TcssXdciEn                | * | 0x01 | 0x00

  # !BSF NAME:{TCSS DMA0 Enable}}
  # !BSF TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Set TCSS DMA0. 0:Disabled  1:Enabled}
  gPlatformFspPkgTokenSpaceGuid.TcssDma0En                | * | 0x01 | 0x01

  # !BSF NAME:{TCSS DMA1 Enable}}
  # !BSF TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Set TCSS DMA1. 0:Disabled  1:Enabled}
  gPlatformFspPkgTokenSpaceGuid.TcssDma1En                | * | 0x01 | 0x01

  # !BSF PAGE:{DBG}
  # !BSF NAME:{PcdSerialDebugBaudRate} TYPE:{Combo}
  # !BSF OPTION:{3:9600, 4:19200, 6:56700, 7:115200}
  # !BSF HELP:{Baud Rate for Serial Debug Messages. 3:9600, 4:19200, 6:56700, 7:115200.}
  gPlatformFspPkgTokenSpaceGuid.PcdSerialDebugBaudRate    | * | 0x01 | 0x07

  # !BSF PAGE:{MRC}
  # !BSF NAME:{HobBufferSize} TYPE:{Combo}
  # !BSF OPTION:{0:Default, 1: 1 Byte, 2: 1 KB, 3: Max value}
  # !BSF HELP:{Size to set HOB Buffer. 0:Default, 1: 1 Byte, 2: 1 KB, 3: Max value(assuming 63KB total HOB size).}
  gPlatformFspPkgTokenSpaceGuid.HobBufferSize             | * | 0x01 | 0x00

  # !BSF NAME:{Early Command Training}
  # !BSF TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Enables/Disable Early Command Training}
  gPlatformFspPkgTokenSpaceGuid.ECT                      | * | 0x01 | 0x01

  # !BSF NAME:{SenseAmp Offset Training}
  # !BSF TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Enables/Disable SenseAmp Offset Training}
  gPlatformFspPkgTokenSpaceGuid.SOT                      | * | 0x01 | 0x00

  # !BSF NAME:{Early ReadMPR Timing Centering 2D}
  # !BSF TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Enables/Disable Early ReadMPR Timing Centering 2D}
  gPlatformFspPkgTokenSpaceGuid.ERDMPRTC2D               | * | 0x01 | 0x01

  # !BSF NAME:{Read MPR Training}
  # !BSF TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Enables/Disable Read MPR Training}
  gPlatformFspPkgTokenSpaceGuid.RDMPRT                   | * | 0x01 | 0x00

  # !BSF NAME:{Receive Enable Training}
  # !BSF TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Enables/Disable Receive Enable Training}
  gPlatformFspPkgTokenSpaceGuid.RCVET                    | * | 0x01 | 0x01

  # !BSF NAME:{Jedec Write Leveling}
  # !BSF TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Enables/Disable Jedec Write Leveling}
  gPlatformFspPkgTokenSpaceGuid.JWRL                     | * | 0x01 | 0x01

  # !BSF NAME:{Early Write Time Centering 2D}
  # !BSF TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Enables/Disable Early Write Time Centering 2D}
  gPlatformFspPkgTokenSpaceGuid.EWRTC2D                  | * | 0x01 | 0x01

  # !BSF NAME:{Early Read Time Centering 2D}
  # !BSF TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Enables/Disable Early Read Time Centering 2D}
  gPlatformFspPkgTokenSpaceGuid.ERDTC2D                  | * | 0x01 | 0x01

  # !BSF NAME:{Write Timing Centering 1D}
  # !BSF TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Enables/Disable Write Timing Centering 1D}
  gPlatformFspPkgTokenSpaceGuid.WRTC1D                   | * | 0x01 | 0x01

  # !BSF NAME:{Write Voltage Centering 1D}
  # !BSF TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Enables/Disable Write Voltage Centering 1D}
  gPlatformFspPkgTokenSpaceGuid.WRVC1D                   | * | 0x01 | 0x01

  # !BSF NAME:{Read Timing Centering 1D}
  # !BSF TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Enables/Disable Read Timing Centering 1D}
  gPlatformFspPkgTokenSpaceGuid.RDTC1D                   | * | 0x01 | 0x01

  # !BSF NAME:{Dimm ODT Training}
  # !BSF TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Enables/Disable Dimm ODT Training}
  gPlatformFspPkgTokenSpaceGuid.DIMMODTT                 | * | 0x01 | 0x01

  # !BSF NAME:{DIMM RON Training}
  # !BSF TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Enables/Disable DIMM RON Training}
  gPlatformFspPkgTokenSpaceGuid.DIMMRONT                 | * | 0x01 | 0x00

  # !BSF NAME:{Write Drive Strength/Equalization 2D}
  # !BSF TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Enables/Disable Write Drive Strength/Equalization 2D}
  gPlatformFspPkgTokenSpaceGuid.WRDSEQT                  | * | 0x01 | 0x00

  # !BSF NAME:{Write Slew Rate Training}
  # !BSF TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Enables/Disable Write Slew Rate Training}
  gPlatformFspPkgTokenSpaceGuid.WRSRT                    | * | 0x01 | 0x01

  # !BSF NAME:{Read ODT Training}
  # !BSF TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Enables/Disable Read ODT Training}
  gPlatformFspPkgTokenSpaceGuid.RDODTT                   | * | 0x01 | 0x01

  # !BSF NAME:{Read Equalization Training}
  # !BSF TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Enables/Disable Read Equalization Training}
  gPlatformFspPkgTokenSpaceGuid.RDEQT                    | * | 0x01 | 0x01

  # !BSF NAME:{Read Amplifier Training}
  # !BSF TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Enables/Disable Read Amplifier Training}
  gPlatformFspPkgTokenSpaceGuid.RDAPT                    | * | 0x01 | 0x01

  # !BSF NAME:{Write Timing Centering 2D}
  # !BSF TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Enables/Disable Write Timing Centering 2D}
  gPlatformFspPkgTokenSpaceGuid.WRTC2D                   | * | 0x01 | 0x01

  # !BSF NAME:{Read Timing Centering 2D}
  # !BSF TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Enables/Disable Read Timing Centering 2D}
  gPlatformFspPkgTokenSpaceGuid.RDTC2D                   | * | 0x01 | 0x01

  # !BSF NAME:{Write Voltage Centering 2D}
  # !BSF TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Enables/Disable Write Voltage Centering 2D}
  gPlatformFspPkgTokenSpaceGuid.WRVC2D                   | * | 0x01 | 0x01

  # !BSF NAME:{Read Voltage Centering 2D}
  # !BSF TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Enables/Disable Read Voltage Centering 2D}
  gPlatformFspPkgTokenSpaceGuid.RDVC2D                   | * | 0x01 | 0x01

  # !BSF NAME:{Command Voltage Centering}
  # !BSF TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Enables/Disable Command Voltage Centering}
  gPlatformFspPkgTokenSpaceGuid.CMDVC                    | * | 0x01 | 0x01

  # !BSF NAME:{Late Command Training}
  # !BSF TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Enables/Disable Late Command Training}
  gPlatformFspPkgTokenSpaceGuid.LCT                      | * | 0x01 | 0x01

  # !BSF NAME:{Round Trip Latency Training}
  # !BSF TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Enables/Disable Round Trip Latency Training}
  gPlatformFspPkgTokenSpaceGuid.RTL                      | * | 0x01 | 0x01

  # !BSF NAME:{Turn Around Timing Training}
  # !BSF TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Enables/Disable Turn Around Timing Training}
  gPlatformFspPkgTokenSpaceGuid.TAT                      | * | 0x01 | 0x00

  # !BSF NAME:{Memory Test}
  # !BSF TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Enables/Disable Memory Test}
  gPlatformFspPkgTokenSpaceGuid.MEMTST                   | * | 0x01 | 0x00

  # !BSF NAME:{DIMM SPD Alias Test}
  # !BSF TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Enables/Disable DIMM SPD Alias Test}
  gPlatformFspPkgTokenSpaceGuid.ALIASCHK                 | * | 0x01 | 0x00

  # !BSF NAME:{Receive Enable Centering 1D}
  # !BSF TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Enables/Disable Receive Enable Centering 1D}
  gPlatformFspPkgTokenSpaceGuid.RCVENC1D                 | * | 0x01 | 0x01

  # !BSF NAME:{Retrain Margin Check}
  # !BSF TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Enables/Disable Retrain Margin Check}
  gPlatformFspPkgTokenSpaceGuid.RMC                      | * | 0x01 | 0x00

  # !BSF NAME:{Write Drive Strength Up/Dn independently}
  # !BSF TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Enables/Disable Write Drive Strength Up/Dn independently}
  gPlatformFspPkgTokenSpaceGuid.WRDSUDT                  | * | 0x01 | 0x01

  # !BSF NAME:{ECC Support}
  # !BSF TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Enables/Disable ECC Support}
  gPlatformFspPkgTokenSpaceGuid.EccSupport               | * | 0x01 | 0x01

  # !BSF NAME:{Memory Remap}
  # !BSF TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Enables/Disable Memory Remap}
  gPlatformFspPkgTokenSpaceGuid.RemapEnable              | * | 0x01 | 0x01

  # !BSF NAME:{Rank Interleave support}
  # !BSF TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Enables/Disable Rank Interleave support. NOTE: RI and HORI can not be enabled at the same time.}
  gPlatformFspPkgTokenSpaceGuid.RankInterleave           | * | 0x01 | 0x01

  # !BSF NAME:{Enhanced Interleave support}
  # !BSF TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Enables/Disable Enhanced Interleave support}
  gPlatformFspPkgTokenSpaceGuid.EnhancedInterleave       | * | 0x01 | 0x01

  # !BSF NAME:{Ch Hash Support}
  # !BSF TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Enable/Disable Channel Hash Support. NOTE: ONLY if Memory interleaved Mode}
  gPlatformFspPkgTokenSpaceGuid.ChHashEnable             | * | 0x01 | 0x01

  # !BSF NAME:{Ch Hash Settings Override}
  # !BSF TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Channel Hash Settings Override}
  gPlatformFspPkgTokenSpaceGuid.ChHashOverride           | * | 0x01 | 0x00

  # !BSF NAME:{Extern Therm Status}
  # !BSF TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Enables/Disable Extern Therm Status}
  gPlatformFspPkgTokenSpaceGuid.EnableExtts              | * | 0x01 | 0x00

  # !BSF NAME:{DDR PowerDown and idle counter}
  # !BSF TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Enables/Disable DDR PowerDown and idle counter(For LPDDR Only)}
  gPlatformFspPkgTokenSpaceGuid.EnablePwrDn              | * | 0x01 | 0x01

  # !BSF NAME:{DDR PowerDown and idle counter}
  # !BSF TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Enables/Disable DDR PowerDown and idle counter(For LPDDR Only)}
  gPlatformFspPkgTokenSpaceGuid.EnablePwrDnLpddr         | * | 0x01 | 0x01

  # !BSF NAME:{SelfRefresh Enable}
  # !BSF TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Enables/Disable SelfRefresh Enable}
  gPlatformFspPkgTokenSpaceGuid.SrefCfgEna               | * | 0x01 | 0x01

  # !BSF NAME:{Throttler CKEMin Defeature}
  # !BSF TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Enables/Disable Throttler CKEMin Defeature(For LPDDR Only)}
  gPlatformFspPkgTokenSpaceGuid.ThrtCkeMinDefeatLpddr    | * | 0x01 | 0x01

  # !BSF NAME:{Throttler CKEMin Defeature}
  # !BSF TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Enables/Disable Throttler CKEMin Defeature}
  gPlatformFspPkgTokenSpaceGuid.ThrtCkeMinDefeat         | * | 0x01 | 0x01

  # !BSF NAME:{Row Hammer Select}
  # !BSF TYPE:{Combo} OPTION:{0:Disable, 1:RFM, 2:pTRR}
  # !BSF HELP:{Row Hammer Select}
  gPlatformFspPkgTokenSpaceGuid.RhSelect                 | * | 0x01 | 0x01

  # !BSF NAME:{Exit On Failure (MRC)}
  # !BSF TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Enables/Disable Exit On Failure (MRC)}
  gPlatformFspPkgTokenSpaceGuid.ExitOnFailure            | * | 0x01 | 0x01

  # !BSF NAME:{New Features 1 - MRC}
  # !BSF TYPE:{Combo} OPTION:{0:Disable, 1:Enable}
  # !BSF HELP:{New Feature Enabling 1, <b>0:Disable</b>, 1:Enable}
  gPlatformFspPkgTokenSpaceGuid.NewFeatureEnable1        | * | 0x01 | 0x00

  # !BSF NAME:{New Features 2 - MRC}
  # !BSF TYPE:{Combo} OPTION:{0:Disable, 1:Enable}
  # !BSF HELP:{New Feature Enabling 2, <b>0:Disable</b>, 1:Enable}
  gPlatformFspPkgTokenSpaceGuid.NewFeatureEnable2        | * | 0x01 | 0x00

  # !BSF NAME:{Duty Cycle Correction Training}
  # !BSF TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Enable/Disable Duty Cycle Correction Training}
  gPlatformFspPkgTokenSpaceGuid.DCC                     | * | 0x01 | 0x01

  # !BSF NAME:{Read Voltage Centering 1D}
  # !BSF TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Enable/Disable Read Voltage Centering 1D}
  gPlatformFspPkgTokenSpaceGuid.RDVC1D                  | * | 0x01 | 0x01

  # !BSF NAME:{TxDqTCO Comp Training}
  # !BSF TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Enable/Disable TxDqTCO Comp Training}
  gPlatformFspPkgTokenSpaceGuid.TXTCO                   | * | 0x01 | 0x01

  # !BSF NAME:{ClkTCO Comp Training}
  # !BSF TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Enable/Disable ClkTCO Comp Training}
  gPlatformFspPkgTokenSpaceGuid.CLKTCO                  | * | 0x01 | 0x01

  # !BSF NAME:{CMD Slew Rate Training}
  # !BSF TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Enable/Disable CMD Slew Rate Training}
  gPlatformFspPkgTokenSpaceGuid.CMDSR                   | * | 0x01 | 0x01

  # !BSF NAME:{CMD Drive Strength and Tx Equalization}
  # !BSF TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Enable/Disable CMD Drive Strength and Tx Equalization}
  gPlatformFspPkgTokenSpaceGuid.CMDDSEQ                 | * | 0x01 | 0x01

  # !BSF NAME:{DIMM CA ODT Training}
  # !BSF TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Enable/Disable DIMM CA ODT Training}
  gPlatformFspPkgTokenSpaceGuid.DIMMODTCA               | * | 0x01 | 0x01

  # !BSF NAME:{TxDqsTCO Comp Training}
  # !BSF TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Enable/Disable TxDqsTCO Comp Training}
  gPlatformFspPkgTokenSpaceGuid.TXTCODQS                | * | 0x01 | 0x01

  # !BSF NAME:{CMD/CTL Drive Strength Up/Dn 2D}
  # !BSF TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Enable/Disable CMD/CTL Drive Strength Up/Dn 2D}
  gPlatformFspPkgTokenSpaceGuid.CMDDRUD                 | * | 0x01 | 0x01

  # !BSF NAME:{VccDLL Bypass Training}
  # !BSF TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Enable/Disable VccDLL Bypass Training}
  gPlatformFspPkgTokenSpaceGuid.VCCDLLBP                | * | 0x01 | 0x00

  # !BSF NAME:{PanicVttDnLp Training}
  # !BSF TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Enable/Disable PanicVttDnLp Training}
  gPlatformFspPkgTokenSpaceGuid.PVTTDNLP                | * | 0x01 | 0x01

  # !BSF NAME:{Read Vref Decap Training*}
  # !BSF TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Enable/Disable Read Vref Decap Training*}
  gPlatformFspPkgTokenSpaceGuid.RDVREFDC                | * | 0x01 | 0x01

  # !BSF NAME:{Vddq Training}
  # !BSF TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Enable/Disable Vddq Training}
  gPlatformFspPkgTokenSpaceGuid.VDDQT                   | * | 0x01 | 0x00

  # !BSF NAME:{Rank Margin Tool Per Bit}
  # !BSF TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Enable/Disable Rank Margin Tool Per Bit}
  gPlatformFspPkgTokenSpaceGuid.RMTBIT                  | * | 0x01 | 0x00

  # !BSF NAME:{ECC DFT feature}
  # !BSF TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Enables/Disable ECC DFT feature}
  gPlatformFspPkgTokenSpaceGuid.EccDftEn                | * | 0x01 | 0x00

  # !BSF NAME:{Write0 feature}
  # !BSF TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Enables/Disable Write0 feature}
  gPlatformFspPkgTokenSpaceGuid.Write0                  | * | 0x01 | 0x01

  # !BSF NAME:{Select if CLK0 is shared between Rank0 and Rank1 in DDR4 DDP}
  # !BSF TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Select if CLK0 is shared between Rank0 and Rank1 in DDR4 DDP}
  gPlatformFspPkgTokenSpaceGuid.Ddr4DdpSharedClock      | * | 0x01 | 0x00

  # !BSF NAME:{Select if ZQ pin is shared between Rank0 and Rank1 in DDR4 DDP}
  # !BSF TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{ESelect if ZQ pin is shared between Rank0 and Rank1 in DDR4 DDP}
  gPlatformFspPkgTokenSpaceGuid.Ddr4DdpSharedZq         | * | 0x01 | 0x00

  # !BSF NAME:{Ch Hash Interleaved Bit}
  # !BSF TYPE:{Combo} OPTION:{0:BIT6, 1:BIT7, 2:BIT8, 3:BIT9, 4:BIT10, 5:BIT11, 6:BIT12, 7:BIT13}
  # !BSF HELP:{Select the BIT to be used for Channel Interleaved mode. NOTE: BIT7 will interlave the channels at a 2 cacheline granularity, BIT8 at 4 and BIT9 at 8. Default is BIT8}
  gPlatformFspPkgTokenSpaceGuid.ChHashInterleaveBit     | * | 0x01 | 0x02

  # !BSF NAME:{Ch Hash Mask}
  # !BSF TYPE:{EditNum, HEX, (0x0000, 0x3FFF)}
  # !BSF HELP:{Set the BIT(s) to be included in the XOR function. NOTE BIT mask corresponds to BITS [19:6] Default is 0x30CC}
  gPlatformFspPkgTokenSpaceGuid.ChHashMask              | * | 0x02 | 0x00

  # !BSF NAME:{Base reference clock value}
  # !BSF TYPE:{Combo} OPTION:{100000000:100Hz, 125000000:125Hz, 167000000:167Hz, 250000000:250Hz}
  # !BSF HELP:{Base reference clock value, in Hertz(Default is 100Hz)}
  gPlatformFspPkgTokenSpaceGuid.BClkFrequency           | * | 0x04 | 100000000

  # !BSF NAME:{EPG DIMM Idd3N}
  # !BSF TYPE:{EditNum, HEX, (0x00, 0x7D0)}
  # !BSF HELP:{Active standby current (Idd3N) in milliamps from datasheet. Must be calculated on a per DIMM basis. Default is 26}
  gPlatformFspPkgTokenSpaceGuid.Idd3n                   | * | 0x02 | 0x1A

  # !BSF NAME:{EPG DIMM Idd3P}
  # !BSF TYPE:{EditNum, HEX, (0x00, 0x7D0)}
  # !BSF HELP:{Active power-down current (Idd3P) in milliamps from datasheet. Must be calculated on a per DIMM basis. Default is 11}
  gPlatformFspPkgTokenSpaceGuid.Idd3p                   | * | 0x02 | 0x0B

  # !BSF NAME:{CMD Normalization}
  # !BSF TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Enable/Disable CMD Normalization}
  gPlatformFspPkgTokenSpaceGuid.CMDNORM                 | * | 0x01 | 0x00

  # !BSF NAME:{Early DQ Write Drive Strength and Equalization Training}
  # !BSF TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Enable/Disable Early DQ Write Drive Strength and Equalization Training}
  gPlatformFspPkgTokenSpaceGuid.EWRDSEQ                 | * | 0x01 | 0x00

  # !BSF NAME:{MC_REFRESH_2X_MODE}
  # !BSF TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{DEPRECATED}
  gPlatformFspPkgTokenSpaceGuid.McRefresh2X             | * | 0x01 | 0x00

  # !BSF NAME:{Idle Energy Mc0Ch0Dimm0}
  # !BSF TYPE:{EditNum, HEX, (0x0, 0x3F)}
  # !BSF HELP:{Idle Energy Consumed for 1 clk w/dimm idle/cke on, range[63;0],(10= Def)}
  gPlatformFspPkgTokenSpaceGuid.IdleEnergyMc0Ch0Dimm0      | * | 0x01 | 0x0A

  # !BSF NAME:{Idle Energy Mc0Ch0Dimm1}
  # !BSF TYPE:{EditNum, HEX, (0x0, 0x3F)}
  # !BSF HELP:{Idle Energy Consumed for 1 clk w/dimm idle/cke on, range[63;0],(10= Def)}
  gPlatformFspPkgTokenSpaceGuid.IdleEnergyMc0Ch0Dimm1      | * | 0x01 | 0x0A

  # !BSF NAME:{Idle Energy Mc0Ch1Dimm0}
  # !BSF TYPE:{EditNum, HEX, (0x0, 0x3F)}
  # !BSF HELP:{Idle Energy Consumed for 1 clk w/dimm idle/cke on, range[63;0],(10= Def)}
  gPlatformFspPkgTokenSpaceGuid.IdleEnergyMc0Ch1Dimm0      | * | 0x01 | 0x0A

  # !BSF NAME:{Idle Energy Mc0Ch1Dimm1}
  # !BSF TYPE:{EditNum, HEX, (0x0, 0x3F)}
  # !BSF HELP:{Idle Energy Consumed for 1 clk w/dimm idle/cke on, range[63;0],(10= Def)}
  gPlatformFspPkgTokenSpaceGuid.IdleEnergyMc0Ch1Dimm1      | * | 0x01 | 0x0A

  # !BSF NAME:{Idle Energy Mc1Ch0Dimm0}
  # !BSF TYPE:{EditNum, HEX, (0x0, 0x3F)}
  # !BSF HELP:{Idle Energy Consumed for 1 clk w/dimm idle/cke on, range[63;0],(10= Def)}
  gPlatformFspPkgTokenSpaceGuid.IdleEnergyMc1Ch0Dimm0      | * | 0x01 | 0x0A

  # !BSF NAME:{Idle Energy Mc1Ch0Dimm1}
  # !BSF TYPE:{EditNum, HEX, (0x0, 0x3F)}
  # !BSF HELP:{Idle Energy Consumed for 1 clk w/dimm idle/cke on, range[63;0],(10= Def)}
  gPlatformFspPkgTokenSpaceGuid.IdleEnergyMc1Ch0Dimm1      | * | 0x01 | 0x0A

  # !BSF NAME:{Idle Energy Mc1Ch1Dimm0}
  # !BSF TYPE:{EditNum, HEX, (0x0, 0x3F)}
  # !BSF HELP:{Idle Energy Consumed for 1 clk w/dimm idle/cke on, range[63;0],(10= Def)}
  gPlatformFspPkgTokenSpaceGuid.IdleEnergyMc1Ch1Dimm0      | * | 0x01 | 0x0A

  # !BSF NAME:{Idle Energy Mc1Ch1Dimm1}
  # !BSF TYPE:{EditNum, HEX, (0x0, 0x3F)}
  # !BSF HELP:{Idle Energy Consumed for 1 clk w/dimm idle/cke on, range[63;0],(10= Def)}
  gPlatformFspPkgTokenSpaceGuid.IdleEnergyMc1Ch1Dimm1      | * | 0x01 | 0x0A

  # !BSF NAME:{PowerDown Energy Mc0Ch0Dimm0}
  # !BSF TYPE:{EditNum, HEX, (0x0, 0x3F)}
  # !BSF HELP:{PowerDown Energy Consumed w/dimm idle/cke off, range[63;0],(6= Def)}
  gPlatformFspPkgTokenSpaceGuid.PdEnergyMc0Ch0Dimm0         | * | 0x01 | 0x06

  # !BSF NAME:{PowerDown Energy Mc0Ch0Dimm1}
  # !BSF TYPE:{EditNum, HEX, (0x0, 0x3F)}
  # !BSF HELP:{PowerDown Energy Consumed w/dimm idle/cke off, range[63;0],(6= Def)}
  gPlatformFspPkgTokenSpaceGuid.PdEnergyMc0Ch0Dimm1         | * | 0x01 | 0x06

  # !BSF NAME:{PowerDown Energy Mc0Ch1Dimm0}
  # !BSF TYPE:{EditNum, HEX, (0x0, 0x3F)}
  # !BSF HELP:{PowerDown Energy Consumed w/dimm idle/cke off, range[63;0],(6= Def)}
  gPlatformFspPkgTokenSpaceGuid.PdEnergyMc0Ch1Dimm0         | * | 0x01 | 0x06

  # !BSF NAME:{PowerDown Energy Mc0Ch1Dimm1}
  # !BSF TYPE:{EditNum, HEX, (0x0, 0x3F)}
  # !BSF HELP:{PowerDown Energy Consumed w/dimm idle/cke off, range[63;0],(6= Def)}
  gPlatformFspPkgTokenSpaceGuid.PdEnergyMc0Ch1Dimm1         | * | 0x01 | 0x06

  # !BSF NAME:{PowerDown Energy Mc1Ch0Dimm0}
  # !BSF TYPE:{EditNum, HEX, (0x0, 0x3F)}
  # !BSF HELP:{PowerDown Energy Consumed w/dimm idle/cke off, range[63;0],(6= Def)}
  gPlatformFspPkgTokenSpaceGuid.PdEnergyMc1Ch0Dimm0         | * | 0x01 | 0x06

  # !BSF NAME:{PowerDown Energy Mc1Ch0Dimm1}
  # !BSF TYPE:{EditNum, HEX, (0x0, 0x3F)}
  # !BSF HELP:{PowerDown Energy Consumed w/dimm idle/cke off, range[63;0],(6= Def)}
  gPlatformFspPkgTokenSpaceGuid.PdEnergyMc1Ch0Dimm1         | * | 0x01 | 0x06

  # !BSF NAME:{PowerDown Energy Mc1Ch1Dimm0}
  # !BSF TYPE:{EditNum, HEX, (0x0, 0x3F)}
  # !BSF HELP:{PowerDown Energy Consumed w/dimm idle/cke off, range[63;0],(6= Def)}
  gPlatformFspPkgTokenSpaceGuid.PdEnergyMc1Ch1Dimm0         | * | 0x01 | 0x06

  # !BSF NAME:{PowerDown Energy Mc1Ch1Dimm1}
  # !BSF TYPE:{EditNum, HEX, (0x0, 0x3F)}
  # !BSF HELP:{PowerDown Energy Consumed w/dimm idle/cke off, range[63;0],(6= Def)}
  gPlatformFspPkgTokenSpaceGuid.PdEnergyMc1Ch1Dimm1         | * | 0x01 | 0x06

  # !BSF NAME:{Activate Energy Mc0Ch0Dimm0}
  # !BSF TYPE:{EditNum, HEX, (0x0, 0xFF)}
  # !BSF HELP:{Activate Energy Contribution, range[255;0],(172= Def)}
  gPlatformFspPkgTokenSpaceGuid.ActEnergyMc0Ch0Dimm0         | * | 0x01 | 0xAC

  # !BSF NAME:{Activate Energy Mc0Ch0Dimm1}
  # !BSF TYPE:{EditNum, HEX, (0x0, 0xFF)}
  # !BSF HELP:{Activate Energy Contribution, range[255;0],(172= Def)}
  gPlatformFspPkgTokenSpaceGuid.ActEnergyMc0Ch0Dimm1         | * | 0x01 | 0xAC

  # !BSF NAME:{Activate Energy Mc0Ch1Dimm0}
  # !BSF TYPE:{EditNum, HEX, (0x0, 0xFF)}
  # !BSF HELP:{Activate Energy Contribution, range[255;0],(172= Def)}
  gPlatformFspPkgTokenSpaceGuid.ActEnergyMc0Ch1Dimm0         | * | 0x01 | 0xAC

  # !BSF NAME:{Activate Energy Mc0Ch1Dimm1}
  # !BSF TYPE:{EditNum, HEX, (0x0, 0xFF)}
  # !BSF HELP:{Activate Energy Contribution, range[255;0],(172= Def)}
  gPlatformFspPkgTokenSpaceGuid.ActEnergyMc0Ch1Dimm1         | * | 0x01 | 0xAC

  # !BSF NAME:{Activate Energy Mc1Ch0Dimm0}
  # !BSF TYPE:{EditNum, HEX, (0x0, 0xFF)}
  # !BSF HELP:{Activate Energy Contribution, range[255;0],(172= Def)}
  gPlatformFspPkgTokenSpaceGuid.ActEnergyMc1Ch0Dimm0         | * | 0x01 | 0xAC

  # !BSF NAME:{Activate Energy Mc1Ch0Dimm1}
  # !BSF TYPE:{EditNum, HEX, (0x0, 0xFF)}
  # !BSF HELP:{Activate Energy Contribution, range[255;0],(172= Def)}
  gPlatformFspPkgTokenSpaceGuid.ActEnergyMc1Ch0Dimm1         | * | 0x01 | 0xAC

  # !BSF NAME:{Activate Energy Mc1Ch1Dimm0}
  # !BSF TYPE:{EditNum, HEX, (0x0, 0xFF)}
  # !BSF HELP:{Activate Energy Contribution, range[255;0],(172= Def)}
  gPlatformFspPkgTokenSpaceGuid.ActEnergyMc1Ch1Dimm0         | * | 0x01 | 0xAC

  # !BSF NAME:{Activate Energy Mc1Ch1Dimm1}
  # !BSF TYPE:{EditNum, HEX, (0x0, 0xFF)}
  # !BSF HELP:{Activate Energy Contribution, range[255;0],(172= Def)}
  gPlatformFspPkgTokenSpaceGuid.ActEnergyMc1Ch1Dimm1         | * | 0x01 | 0xAC

  # !BSF NAME:{Read Energy Mc0Ch0Dimm0}
  # !BSF TYPE:{EditNum, HEX, (0x0, 0xFF)}
  # !BSF HELP:{Read Energy Contribution, range[255;0],(212= Def)}
  gPlatformFspPkgTokenSpaceGuid.RdEnergyMc0Ch0Dimm0         | * | 0x01 | 0xD4

  # !BSF NAME:{Read Energy Mc0Ch0Dimm1}
  # !BSF TYPE:{EditNum, HEX, (0x0, 0xFF)}
  # !BSF HELP:{Read Energy Contribution, range[255;0],(212= Def)}
  gPlatformFspPkgTokenSpaceGuid.RdEnergyMc0Ch0Dimm1         | * | 0x01 | 0xD4

  # !BSF NAME:{Read Energy Mc0Ch1Dimm0}
  # !BSF TYPE:{EditNum, HEX, (0x0, 0xFF)}
  # !BSF HELP:{Read Energy Contribution, range[255;0],(212= Def)}
  gPlatformFspPkgTokenSpaceGuid.RdEnergyMc0Ch1Dimm0         | * | 0x01 | 0xD4

  # !BSF NAME:{Read Energy Mc0Ch1Dimm1}
  # !BSF TYPE:{EditNum, HEX, (0x0, 0xFF)}
  # !BSF HELP:{Read Energy Contribution, range[255;0],(212= Def)}
  gPlatformFspPkgTokenSpaceGuid.RdEnergyMc0Ch1Dimm1         | * | 0x01 | 0xD4

  # !BSF NAME:{Read Energy Mc1Ch0Dimm0}
  # !BSF TYPE:{EditNum, HEX, (0x0, 0xFF)}
  # !BSF HELP:{Read Energy Contribution, range[255;0],(212= Def)}
  gPlatformFspPkgTokenSpaceGuid.RdEnergyMc1Ch0Dimm0         | * | 0x01 | 0xD4

  # !BSF NAME:{Read Energy Mc1Ch0Dimm1}
  # !BSF TYPE:{EditNum, HEX, (0x0, 0xFF)}
  # !BSF HELP:{Read Energy Contribution, range[255;0],(212= Def)}
  gPlatformFspPkgTokenSpaceGuid.RdEnergyMc1Ch0Dimm1         | * | 0x01 | 0xD4

  # !BSF NAME:{Read Energy Mc1Ch1Dimm0}
  # !BSF TYPE:{EditNum, HEX, (0x0, 0xFF)}
  # !BSF HELP:{Read Energy Contribution, range[255;0],(212= Def)}
  gPlatformFspPkgTokenSpaceGuid.RdEnergyMc1Ch1Dimm0         | * | 0x01 | 0xD4

  # !BSF NAME:{Read Energy Mc1Ch1Dimm1}
  # !BSF TYPE:{EditNum, HEX, (0x0, 0xFF)}
  # !BSF HELP:{Read Energy Contribution, range[255;0],(212= Def)}
  gPlatformFspPkgTokenSpaceGuid.RdEnergyMc1Ch1Dimm1         | * | 0x01 | 0xD4

  # !BSF NAME:{Write Energy Mc0Ch0Dimm0}
  # !BSF TYPE:{EditNum, HEX, (0x0, 0xFF)}
  # !BSF HELP:{Write Energy Contribution, range[255;0],(221= Def)}
  gPlatformFspPkgTokenSpaceGuid.WrEnergyMc0Ch0Dimm0         | * | 0x01 | 0xDD

  # !BSF NAME:{Write Energy Mc0Ch0Dimm1}
  # !BSF TYPE:{EditNum, HEX, (0x0, 0xFF)}
  # !BSF HELP:{Write Energy Contribution, range[255;0],(221= Def)}
  gPlatformFspPkgTokenSpaceGuid.WrEnergyMc0Ch0Dimm1         | * | 0x01 | 0xDD

  # !BSF NAME:{Write Energy Mc0Ch1Dimm0}
  # !BSF TYPE:{EditNum, HEX, (0x0, 0xFF)}
  # !BSF HELP:{Write Energy Contribution, range[255;0],(221= Def)}
  gPlatformFspPkgTokenSpaceGuid.WrEnergyMc0Ch1Dimm0         | * | 0x01 | 0xDD

  # !BSF NAME:{Write Energy Mc0Ch1Dimm1}
  # !BSF TYPE:{EditNum, HEX, (0x0, 0xFF)}
  # !BSF HELP:{Write Energy Contribution, range[255;0],(221= Def)}
  gPlatformFspPkgTokenSpaceGuid.WrEnergyMc0Ch1Dimm1         | * | 0x01 | 0xDD

  # !BSF NAME:{Write Energy Mc1Ch0Dimm0}
  # !BSF TYPE:{EditNum, HEX, (0x0, 0xFF)}
  # !BSF HELP:{Write Energy Contribution, range[255;0],(221= Def)}
  gPlatformFspPkgTokenSpaceGuid.WrEnergyMc1Ch0Dimm0         | * | 0x01 | 0xDD

  # !BSF NAME:{Write Energy Mc1Ch0Dimm1}
  # !BSF TYPE:{EditNum, HEX, (0x0, 0xFF)}
  # !BSF HELP:{Write Energy Contribution, range[255;0],(221= Def)}
  gPlatformFspPkgTokenSpaceGuid.WrEnergyMc1Ch0Dimm1         | * | 0x01 | 0xDD

  # !BSF NAME:{Write Energy Mc1Ch1Dimm0}
  # !BSF TYPE:{EditNum, HEX, (0x0, 0xFF)}
  # !BSF HELP:{Write Energy Contribution, range[255;0],(221= Def)}
  gPlatformFspPkgTokenSpaceGuid.WrEnergyMc1Ch1Dimm0         | * | 0x01 | 0xDD

  # !BSF NAME:{Write Energy Mc1Ch1Dimm1}
  # !BSF TYPE:{EditNum, HEX, (0x0, 0xFF)}
  # !BSF HELP:{Write Energy Contribution, range[255;0],(221= Def)}
  gPlatformFspPkgTokenSpaceGuid.WrEnergyMc1Ch1Dimm1         | * | 0x01 | 0xDD

  # !BSF NAME:{Throttler CKEMin Timer}
  # !BSF TYPE:{EditNum, HEX, (0x0, 0xFF)}
  # !BSF HELP:{Timer value for CKEMin, range[255;0]. Req'd min of SC_ROUND_T + BYTE_LENGTH (4). Dfault is 0x00}
  gPlatformFspPkgTokenSpaceGuid.ThrtCkeMinTmr         | * | 0x01 | 0x00

  # !BSF NAME:{Allow Opp Ref Below Write Threhold} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Allow opportunistic refreshes while we don't exit power down.}
  gPlatformFspPkgTokenSpaceGuid.AllowOppRefBelowWriteThrehold    | * | 0x01 | 0x0

  # !BSF NAME:{Write Threshold} TYPE:{EditNum, HEX, (0x00, 0x3F)}
  # !BSF HELP:{Number of writes that can be accumulated while CKE is low before CKE is asserted.}
  gPlatformFspPkgTokenSpaceGuid.WriteThreshold    | * | 0x01 | 0x0

  # !BSF NAME:{Rapl Power Floor Ch0}
  # !BSF TYPE:{EditNum, HEX, (0x0, 0xFF)}
  # !BSF HELP:{Power budget ,range[255;0],(0= 5.3W Def)}
  gPlatformFspPkgTokenSpaceGuid.RaplPwrFlCh0         | * | 0x01 | 0x00

  # !BSF NAME:{Rapl Power Floor Ch1}
  # !BSF TYPE:{EditNum, HEX, (0x0, 0xFF)}
  # !BSF HELP:{Power budget ,range[255;0],(0= 5.3W Def)}
  gPlatformFspPkgTokenSpaceGuid.RaplPwrFlCh1         | * | 0x01 | 0x00

  # !BSF NAME:{Command Rate Support}
  # !BSF TYPE:{Combo} OPTION:{0:Disable, 5:2 CMDS, 7:3 CMDS, 9:4 CMDS, 11:5 CMDS, 13:6 CMDS, 15:7 CMDS}
  # !BSF HELP:{CMD Rate and Limit Support Option. NOTE: ONLY supported in 1N Mode, Default is 3 CMDs}

  gPlatformFspPkgTokenSpaceGuid.EnCmdRate         | * | 0x01 | 0x07

  # !BSF NAME:{REFRESH_2X_MODE}
  # !BSF TYPE:{Combo} OPTION:{0:Disable, 1:Enabled for WARM or HOT, 2:Enabled HOT only}
  # !BSF HELP:{0- (Default)Disabled 1-iMC enables 2xRef when Warm and Hot 2- iMC enables 2xRef when Hot}
  gPlatformFspPkgTokenSpaceGuid.Refresh2X         | * | 0x01 | 0x00

  # !BSF NAME:{Energy Performance Gain}
  # !BSF TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Enable/disable(default) Energy Performance Gain.}
  gPlatformFspPkgTokenSpaceGuid.EpgEnable         | * | 0x01 | 0x00

  # !BSF NAME:{RH pTRR LFSR0 Mask}
  # !BSF TYPE:{EditNum, HEX, (0x01, 0xF)}
  # !BSF HELP:{Row Hammer pTRR LFSR0 Mask, 1/2^(value)}
  gPlatformFspPkgTokenSpaceGuid.Lfsr0Mask         | * | 0x01 | 0xB

  # !BSF NAME:{User Manual Threshold}
  # !BSF TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Disabled: Predefined threshold will be used.\nEnabled: User Input will be used.}
  gPlatformFspPkgTokenSpaceGuid.UserThresholdEnable         | * | 0x01 | 0x00

  # !BSF NAME:{User Manual Budget}
  # !BSF TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Disabled: Configuration of memories will defined the Budget value.\nEnabled: User Input will be used.}
  gPlatformFspPkgTokenSpaceGuid.UserBudgetEnable         | * | 0x01 | 0x00

  # !BSF NAME:{Power Down Mode}
  # !BSF TYPE:{Combo} OPTION:{0x0:No Power Down, 0x1:APD, 0x6:PPD DLL OFF, 0xFF:Auto}
  # !BSF HELP:{This option controls command bus tristating during idle periods}
  gPlatformFspPkgTokenSpaceGuid.PowerDownMode            | * | 0x01 | 0xFF

  # !BSF NAME:{Pwr Down Idle Timer}
  # !BSF TYPE:{EditNum, HEX, (0x0, 0xFF)}
  # !BSF HELP:{The minimum value should = to the worst case Roundtrip delay + Burst_Length. 0 means AUTO: 64 for ULX/ULT, 128 for DT/Halo}
  gPlatformFspPkgTokenSpaceGuid.PwdwnIdleCounter         | * | 0x01 | 0x00

  # !BSF NAME:{Page Close Idle Timeout}
  # !BSF TYPE:{Combo} OPTION:{0:Enabled, 1:Disabled}
  # !BSF HELP:{This option controls Page Close Idle Timeout}
  gPlatformFspPkgTokenSpaceGuid.DisPgCloseIdleTimeout            | * | 0x01 | 0x0

  # !BSF NAME:{Bitmask of ranks that have CA bus terminated}
  # !BSF TYPE:{EditNum, HEX, (0x0, 0xFF)}
  # !BSF HELP:{Offset 225 LPDDR4: Bitmask of ranks that have CA bus terminated. <b>0x01=Default, Rank0 is terminating and Rank1 is non-terminating</b>}
  gPlatformFspPkgTokenSpaceGuid.CmdRanksTerminated         | * | 0x01 | 0x01

  # !BSF PAGE:{DBG}
  # !BSF NAME:{PcdSerialDebugLevel} TYPE:{Combo}
  # !BSF OPTION:{0:Disable, 1:Error Only, 2:Error and Warnings, 3:Load Error Warnings and Info, 4:Load Error Warnings and Info & Event, 5:Load Error Warnings Info and Verbose}
  # !BSF HELP:{Serial Debug Message Level. 0:Disable, 1:Error Only, 2:Error & Warnings, 3:Load, Error, Warnings & Info, 4:Load, Error, Warnings, Info & Event, 5:Load, Error, Warnings, Info & Verbose.}
  gPlatformFspPkgTokenSpaceGuid.PcdSerialDebugLevel       | * | 0x01 | 0x03

  # !BSF PAGE:{MRC}
  # !BSF NAME:{Safe Mode Support}
  # !BSF TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{This option configures the varous items in the IO and MC to be more conservative.(def=Disable)}
  gPlatformFspPkgTokenSpaceGuid.SafeMode          | * | 0x01 | 0x00

  # !BSF NAME:{Ask MRC to clear memory content} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Ask MRC to clear memory content <b>0: Do not Clear Memory;</b> 1: Clear Memory.}
  gPlatformFspPkgTokenSpaceGuid.CleanMemory                 | * | 0x01 | 0x00

  # !BSF NAME:{LpDdrDqDqsReTraining}
  # !BSF TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Enable/Disable TxDqDqs ReTraining for LP4/5 and DDR5}
  gPlatformFspPkgTokenSpaceGuid.LpDdrDqDqsReTraining        | * | 0x01 | 0x01

  # !BSF PAGE:{TCSS1}
  # !BSF NAME:{TCSS USB Port Enable} TYPE:{EditNum, HEX, (0x0,0x000F)}
  # !BSF HELP:{Bitmap for per port enabling}
  gPlatformFspPkgTokenSpaceGuid.UsbTcPortEnPreMem                 | * | 0x01 | 0x00

  # !BSF PAGE:{DBG}
  # !BSF NAME:{Post Code Output Port}
  # !BSF TYPE:{EditNum, HEX, (0x0, 0xFFFF)}
  # !BSF HELP:{This option configures Post Code Output Port}
  gPlatformFspPkgTokenSpaceGuid.PostCodeOutputPort          | * | 0x02 | 0x80

  # !BSF PAGE:{MRC}
  # !BSF NAME:{RMTLoopCount} TYPE:{EditNum, HEX, (0, 0x20)}
  # !BSF HELP:{Specifies the Loop Count to be used during Rank Margin Tool Testing. 0 - AUTO}
  gPlatformFspPkgTokenSpaceGuid.RMTLoopCount                | * | 0x01 | 0x00

  # !BSF PAGE:{SA1}
  # !BSF NAME:{Enable/Disable SA CRID} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Enable: SA CRID, Disable (Default): SA CRID}
  gPlatformFspPkgTokenSpaceGuid.CridEnable                  | * | 0x01 | 0x0

#!if (gSiPkgTokenSpaceGuid.PcdEmbeddedEnable == TRUE)
  # !BSF NAME:{WRC Feature} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Enable/Disable WRC (Write Cache) feature of IOP. When feature is enabled, supports IO devices allocating onto the ring and into LLC. WRC is fused on by default.}
  gPlatformFspPkgTokenSpaceGuid.WrcFeatureEnable            | * | 0x01 | 0x01
#!endif

  # !BSF PAGE:{MRC}
  # !BSF NAME:{BCLK RFI Frequency} TYPE:{EditNum, HEX, (0x00,0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF)}
  # !HDR STRUCT:{UINT32}
  # !BSF HELP:{Bclk RFI Frequency for each SAGV point in Hz units. 98000000Hz = 98MHz <b>0 - No RFI Tuning</b>. Range is 98Mhz-100Mhz.}
  gPlatformFspPkgTokenSpaceGuid.BclkRfiFreq               | * | 0x10 | {0x00000000, 0x00000000, 0x00000000, 0x00000000}

  # !BSF PAGE:{SA1}
  # !BSF NAME:{Size of PCIe IMR.} TYPE:{EditNum, HEX, (0x00,0xFFFF)}
  # !BSF HELP:{Size of PCIe IMR in megabytes}
  gPlatformFspPkgTokenSpaceGuid.PcieImrSize               | * | 0x02 | 0x00

  # !BSF NAME:{Enable PCIe IMR} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{0: Disable(AUTO), 1: Enable}
  gPlatformFspPkgTokenSpaceGuid.PcieImrEnabled            | * | 0x01 | 0x00

  # !BSF NAME:{Enable PCIe IMR} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{1: PCH PCIE, 2: SA PCIE. If PCIeImrEnabled is TRUE then this will use to select the Root port location from PCH PCIe or SA PCIe}
  gPlatformFspPkgTokenSpaceGuid.PcieImrRpLocation         | * | 0x01 | 0x01

  # !BSF NAME:{Root port number for IMR.} TYPE:{EditNum, HEX, (0x00,0x17)}
  # !BSF HELP:{Root port number for IMR.If PCieImrRpLocation is PCH PCIe then select root port from 0 to 23 and if it is SA PCIe then select root port from 0 to 3}
  gPlatformFspPkgTokenSpaceGuid.PcieImrRpSelection          | * | 0x01 | 0x00

  # !BSF PAGE:{DBG}
  # !BSF NAME:{SerialDebugMrcLevel} TYPE:{Combo}
  # !BSF OPTION:{0:Disable, 1:Error Only, 2:Error and Warnings, 3:Load Error Warnings and Info, 4:Load Error Warnings and Info & Event, 5:Load Error Warnings Info and Verbose}
  # !BSF HELP:{MRC Serial Debug Message Level. 0:Disable, 1:Error Only, 2:Error & Warnings, 3:Load, Error, Warnings & Info, 4:Load, Error, Warnings, Info & Event, 5:Load, Error, Warnings, Info & Verbose.}
  gPlatformFspPkgTokenSpaceGuid.SerialDebugMrcLevel         | * | 0x01 | 0x03

  # !BSF NAME:{Ddr4OneDpc} TYPE:{Combo}
  # !BSF OPTION:{0: Disabled, 1: Enabled on DIMM0 only, 2: Enabled on DIMM1 only, 3: Enabled}
  # !BSF HELP:{DDR4 1DPC performance feature for 2R DIMMs. Can be enabled on DIMM0 or DIMM1 only, or on both (default)}
  gPlatformFspPkgTokenSpaceGuid.Ddr4OneDpc                  | * | 0x01 | 0x01

  # !BSF NAME:{RH pTRR LFSR1 Mask}
  # !BSF TYPE:{EditNum, HEX, (0x01, 0xF)}
  # !BSF HELP:{Row Hammer pTRR LFSR1 Mask, 1/2^(value)}
  gPlatformFspPkgTokenSpaceGuid.Lfsr1Mask                   | * | 0x01 | 0xB

  # !BSF NAME:{LPDDR ODT RttWr}
  # !BSF TYPE:{EditNum, HEX, (0x00,0xFF)}
  # !BSF HELP:{Initial RttWr for LP4/5 in Ohms. 0x0 - Auto}
  gPlatformFspPkgTokenSpaceGuid.LpddrRttWr                  | * | 0x01 | 0x00

  # !BSF NAME:{LPDDR ODT RttCa}
  # !BSF TYPE:{EditNum, HEX, (0x00,0xFF)}
  # !BSF HELP:{Initial RttCa for LP4/5 in Ohms. 0x0 - Auto}
  gPlatformFspPkgTokenSpaceGuid.LpddrRttCa                  | * | 0x01 | 0x00

  # !BSF PAGE:{MRC}
  # !BSF NAME:{REFRESH_PANIC_WM}
  # !BSF HELP:{DEPRECATED}
  gPlatformFspPkgTokenSpaceGuid.RefreshPanicWm              | * | 0x01 | 0x08

  # !BSF PAGE:{MRC}
  # !BSF NAME:{REFRESH_HP_WM}
  # !BSF HELP:{DEPRECATED}
  gPlatformFspPkgTokenSpaceGuid.RefreshHpWm                 | * | 0x01 | 0x07

  # !BSF PAGE:{MRC}
  # !BSF NAME:{Command Pins Mapping}
  # !BSF TYPE:{EditNum, HEX, (0x00,0xFF)}
  # !BSF HELP:{BitMask where bits [3:0] are Controller 0 Channel [3:0] and bits [7:4] are Controller 1 Channel [3:0]. 0 = CCC pin mapping is Ascending, 1 = CCC pin mapping is Descending.}
  gPlatformFspPkgTokenSpaceGuid.Lp5CccConfig                | * | 0x1 | 0x00

  # !BSF PAGE:{MRC}
  # !BSF NAME:{Command Pins Mirrored}
  # !BSF TYPE:{EditNum, HEX, (0x00, 0xFF)}
  # !BSF HELP:{BitMask where bits [3:0] are Controller 0 Channel [3:0] and bits [7:4] are Controller 1 Channel [3:0]. 0 = No Command Mirror and 1 = Command Mirror.}
  gPlatformFspPkgTokenSpaceGuid.CmdMirror                   | * | 0x1 | 0x00

  # !BSF PAGE:{MRC}
  # !BSF NAME:{DIMM DFE Training}
  # !BSF TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Enable/Disable DIMM DFE Training}
  gPlatformFspPkgTokenSpaceGuid.DIMMDFE                     | * | 0x01 | 0x01

  # !BSF PAGE:{MRC}
  # !BSF NAME:{Extended Bank Hashing}
  # !BSF TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Enable/Disable Extended Bank Hashing}
  gPlatformFspPkgTokenSpaceGuid.ExtendedBankHashing         | * | 0x01 | 0x01

  # !BSF PAGE:{MRC}
  # !BSF NAME:{Refresh Watermarks}
  # !BSF TYPE:{Combo} OPTION:{0:Set Refresh Watermarks to Low, 1:Set Refresh Watermarks to High (Default)}
  # !BSF HELP:{Refresh Watermarks: 0-Low, 1-High (default)}
  gPlatformFspPkgTokenSpaceGuid.RefreshWm                   | * | 0x01 | 0x01

  # !BSF PAGE:{MRC}
  # !BSF NAME:{MC_REFRESH_RATE}
  # !BSF TYPE:{Combo} OPTION:{0:NORMAL Refresh, 1:1x Refresh, 2:2x Refresh, 3:4x Refresh}
  # !BSF HELP:{Type of Refresh Rate used to prevent Row Hammer. Default is NORMAL Refresh}
  gPlatformFspPkgTokenSpaceGuid.McRefreshRate               | * | 0x01 | 0x00

  # !BSF PAGE:{MRC}
  # !BSF NAME:{Periodic DCC}
  # !BSF TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Enable/Disable Periodic DCC; default: Disabled}
  gPlatformFspPkgTokenSpaceGuid.PeriodicDcc                 | * | 0x01 | 0x00

  # !BSF PAGE:{MRC}
  # !BSF NAME:{LpMode} TYPE:{Combo}
  # !BSF OPTION:{0: Auto (default), 1: Enabled, 2: Disabled, 3: Reserved}
  # !BSF HELP:{LpMode feature}
  gPlatformFspPkgTokenSpaceGuid.LpMode                      | * | 0x01 | 0x00

  # !BSF PAGE:{MRC}

  # !BSF NAME:{TX DQS DCC Training}
  # !BSF TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Enable/Disable TX DQS DCC Training}
  gPlatformFspPkgTokenSpaceGuid.TXDQSDCC                     | * | 0x01 | 0x01

  # !BSF PAGE:{MRC}
  # !BSF NAME:{DRAM DCA Training}
  # !BSF TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Enable/Disable DRAM DCA Training}
  gPlatformFspPkgTokenSpaceGuid.DRAMDCA                     | * | 0x01 | 0x01

  # !BSF PAGE:{MRC}
  # !BSF NAME:{EARLY DIMM DFE Training}
  # !BSF TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Enable/Disable EARLY DIMM DFE Training}
  gPlatformFspPkgTokenSpaceGuid.EARLYDIMMDFE                | * | 0x01 | 0x00

  #
  # SA Pre-Mem Block Start
  #
  # !BSF PAGE:{SA1}
  # !BSF NAME:{Skip external display device scanning} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Enable: Do not scan for external display device, Disable (Default): Scan external display devices}
  gPlatformFspPkgTokenSpaceGuid.SkipExtGfxScan              | * | 0x01 | 0x1

  # !BSF PAGE:{MRC}
  # !BSF NAME:{Generate BIOS Data ACPI Table} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Enable: Generate BDAT for MRC RMT or SA PCIe data. Disable (Default): Do not generate it}
  gPlatformFspPkgTokenSpaceGuid.BdatEnable                  | * | 0x01 | 0x00

  # !BSF NAME:{Lock PCU Thermal Management registers} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Lock PCU Thermal Management registers. Enable(Default)=1, Disable=0}
  gPlatformFspPkgTokenSpaceGuid.LockPTMregs                 | * | 0x01 | 0x01

  # !BSF NAME:{Rsvd} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Disable(0x0)(Default): Normal Operation - RxCTLE adaptive behavior enabled, Enable(0x1): Override RxCTLE - Disable RxCTLE adaptive behavior to keep the configured RxCTLE peak values unmodified}
  gPlatformFspPkgTokenSpaceGuid.PegGen3Rsvd                 | * | 0x01 | 0x00

  # !BSF NAME:{Panel Power Enable} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Control for enabling/disabling VDD force bit (Required only for early enabling of eDP panel). 0=Disable, 1(Default)=Enable}
  gPlatformFspPkgTokenSpaceGuid.PanelPowerEnable            | * | 0x01 | 0x01

  # !BSF PAGE:{MRC}
  # !BSF NAME:{BdatTestType} TYPE:{Combo}
  # !BSF OPTION:{0:RMT per Rank, 1:RMT per Bit, 2:Margin2D}
  # !BSF HELP:{Indicates the type of Memory Training data to populate into the BDAT ACPI table.}
  gPlatformFspPkgTokenSpaceGuid.BdatTestType                | * | 0x1 | 0x00

  # !BSF PAGE:{SA1}
  # !BSF NAME:{PMR Size} TYPE:{Combo} TYPE:{EditNum, HEX, (0x00,0xFFFFFFFF)}
  # !BSF HELP:{Size of PMR memory buffer. 0x400000 for normal boot and 0x200000 for S3 boot}
  gPlatformFspPkgTokenSpaceGuid.DmaBufferSize               | * | 0x04 | 0x0400000

  # !BSF NAME:{VT-d/IOMMU Boot Policy} TYPE:{EditNum, HEX, (0x00,0xFF)}
  # !BSF HELP:{BIT0: Enable IOMMU during boot, BIT1: Enable IOMMU when transfer control to OS}
  gPlatformFspPkgTokenSpaceGuid.PreBootDmaMask              | * | 0x1 | 0x0

  # !BSF NAME:{Delta T12 Power Cycle Delay required in ms} TYPE:{Combo}
  # !BSF OPTION:{0 : No Delay, 0xFFFF : Auto Calulate T12 Delay}
  # !BSF HELP:{Select the value for delay required. 0= No delay, 0xFFFF(Default) = Auto calculate T12 Delay to max 500ms}
  gPlatformFspPkgTokenSpaceGuid.DeltaT12PowerCycleDelay     | * | 0x2 | 0x0

  # !BSF PAGE:{MRC}
  # !BSF NAME:{Reuse Adl DDR5 Board or not} TYPE:{Combo}
  # !BSF OPTION:{0 : no, 1 : yes}
  # !BSF HELP:{Indicate whether adl ddr5 board is reused.}
  gPlatformFspPkgTokenSpaceGuid.ReuseAdlSDdr5Board          | * | 0x1 | 0x0

  # !BSF NAME:{Oem T12 Delay Override} TYPE:{Combo}  OPTION:{$EN_DIS}
  # !BSF HELP:{Oem T12 Delay Override. 0(Default)=Disable  1=Enable }
  gPlatformFspPkgTokenSpaceGuid.OemT12DelayOverride         | * | 0x1 | 0x0

  # !BSF NAME:{SaPreMemTestRsvd} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Reserved for SA Pre-Mem Test}
  gPlatformFspPkgTokenSpaceGuid.SaPreMemTestRsvd            | * | 0x59 | {0x00}


  #
  # SA Pre-Mem Block End
  #

  #
  # Security Block Start
  #
  # !BSF PAGE:{SEC1}
  # !BSF NAME:{TotalFlashSize}  TYPE:{EditNum, HEX, (0x00,0xFFFF)}
  # !BSF HELP:{Enable/Disable. 0: Disable, define default value of TotalFlashSize , 1: enable}
  #TODO: change the value
  gPlatformFspPkgTokenSpaceGuid.TotalFlashSize               | * | 0x02 | 0x0000

  # !BSF NAME:{BiosSize}  TYPE:{EditNum, HEX, (0x00,0xFFFF)}
  # !BSF HELP:{The size of the BIOS region of the IFWI. Used if FspmUpd->FspmConfig.BiosGuard != 0. If BiosGuard is enabled, MRC will increase the size of the DPR (DMA Protected Range) so that a BIOS Update Script can be stored in the DPR.}
  gPlatformFspPkgTokenSpaceGuid.BiosSize                     | * | 0x02 | 0x2800

  # !BSF NAME:{SecurityTestRsvd} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Reserved for SA Pre-Mem Test}
  gPlatformFspPkgTokenSpaceGuid.SecurityTestRsvd             | * | 0x0C | {0x00}

  #
  # Security Block End
  #

  #
  # PCH Pre-Mem Block start
  #
  # !BSF PAGE:{PCH1}
  # !BSF NAME:{Smbus dynamic power gating} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Disable or Enable Smbus dynamic power gating.}
  gPlatformFspPkgTokenSpaceGuid.SmbusDynamicPowerGating     | * | 0x01 | 0x01

  # !BSF NAME:{Disable and Lock Watch Dog Register} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Set 1 to clear WDT status, then disable and lock WDT registers.}
  gPlatformFspPkgTokenSpaceGuid.WdtDisableAndLock           | * | 0x01 | 0x00

  # !BSF NAME:{SMBUS SPD Write Disable} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Set/Clear Smbus SPD Write Disable. 0: leave SPD Write Disable bit; 1: set SPD Write Disable bit. For security recommendations, SPD write disable bit must be set.}
  gPlatformFspPkgTokenSpaceGuid.SmbusSpdWriteDisable        | * | 0x01 | 0x01

  #
  # PCH Pre-Mem Block End
  #

  #
  # ME Pre-Mem Block start
  #
  # !BSF PAGE:{ME1}
  # !BSF NAME:{Force ME DID Init Status} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Test, 0: disable, 1: Success, 2: No Memory in Channels, 3: Memory Init Error, Set ME DID init stat value}
  gPlatformFspPkgTokenSpaceGuid.DidInitStat                 | * | 0x01 | 0x0

  # !BSF NAME:{CPU Replaced Polling Disable} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Test, 0: disable, 1: enable, Setting this option disables CPU replacement polling loop}
  gPlatformFspPkgTokenSpaceGuid.DisableCpuReplacedPolling   | * | 0x01 | 0x0

  # !BSF NAME:{Check HECI message before send} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Test, 0: disable, 1: enable, Enable/Disable message check.}
  gPlatformFspPkgTokenSpaceGuid.DisableMessageCheck         | * | 0x01 | 0x0

  # !BSF NAME:{Skip MBP HOB} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Test, 0: disable, 1: enable, Enable/Disable MOB HOB.}
  gPlatformFspPkgTokenSpaceGuid.SkipMbpHob                  | * | 0x01 | 0x0

  # !BSF NAME:{HECI2 Interface Communication} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Test, 0: disable, 1: enable, Adds or Removes HECI2 Device from PCI space.}
  gPlatformFspPkgTokenSpaceGuid.HeciCommunication2          | * | 0x01 | 0x0

  # !BSF NAME:{Enable KT device} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Test, 0: disable, 1: enable, Enable or Disable KT device.}
  gPlatformFspPkgTokenSpaceGuid.KtDeviceEnable              | * | 0x01 | 0x1

  # !BSF NAME:{Skip CPU replacement check} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Test, 0: disable, 1: enable, Setting this option to skip CPU replacement check}
  gPlatformFspPkgTokenSpaceGuid.SkipCpuReplacementCheck     | * | 0x01 | 0x00

  #
  # ME Pre-Mem Test Block End
  #

  # !BSF PAGE:{SA1}
  # !BSF NAME:{Hybrid Graphics GPIO information for PEG 1} TYPE:{EditNum, HEX, (0x00, 0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF)}
  # !HDR STRUCT:{UINT32}
  # !BSF HELP:{Hybrid Graphics GPIO information for PEG 1, for Reset, power and wake GPIOs}
  gPlatformFspPkgTokenSpaceGuid.CpuPcie1Rtd3Gpio             | * | 0x60 | {0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0}

  # !BSF NAME:{Hybrid Graphics GPIO information for PEG 2} TYPE:{EditNum, HEX, (0x00, 0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF)}
  # !HDR STRUCT:{UINT32}
  # !BSF HELP:{Hybrid Graphics GPIO information for PEG 2, for Reset, power and wake GPIOs}
  gPlatformFspPkgTokenSpaceGuid.CpuPcie2Rtd3Gpio             | * | 0x60 | {0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0}

  # !BSF NAME:{Hybrid Graphics GPIO information for PEG 3} TYPE:{EditNum, HEX, (0x00, 0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF)}
  # !HDR STRUCT:{UINT32}
  # !BSF HELP:{Hybrid Graphics GPIO information for PEG 3, for Reset, power and wake GPIOs}
  gPlatformFspPkgTokenSpaceGuid.CpuPcie3Rtd3Gpio             | * | 0x60 | {0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0}


  # !BSF NAME:{Avx2 Voltage Guardband Scaling Factor}
  # !BSF TYPE:{EditNum, HEX, (0x00, 0xC8)}
  # !BSF HELP:{AVX2 Voltage Guardband Scale factor applied to AVX2 workloads. Range is 0-200 in 1/100 units, where a value of 125 would apply a 1.25 scale factor.}
  gPlatformFspPkgTokenSpaceGuid.Avx2VoltageScaleFactor      | * | 0x01 | 0x00

  # !BSF NAME:{Avx512 Voltage Guardband Scaling Factor}
  # !BSF TYPE:{EditNum, HEX, (0x00, 0xC8)}
  # !BSF HELP:{DEPRECATED}
  gPlatformFspPkgTokenSpaceGuid.Avx512VoltageScaleFactor    | * | 0x01 | 0x00

  #
  # PCH - Additional policies post API freeze
  #

  # !BSF PAGE:{PCH1}
  # !BSF NAME:{Serial Io Uart Debug Mode} TYPE:{Combo}
  # !BSF OPTION:{0:SerialIoUartDisabled, 1:SerialIoUartPci, 2:SerialIoUartHidden, 3:SerialIoUartCom, 4:SerialIoUartSkipInit}
  # !BSF HELP:{Select SerialIo Uart Controller mode}
  gPlatformFspPkgTokenSpaceGuid.SerialIoUartDebugMode               | * | 0x01 | 0x02

  # !BSF NAME:{SerialIoUartDebugRxPinMux - FSPT} TYPE:{EditNum, HEX, (0x00,0xFFFFFFFF)}
  # !BSF HELP:{Select RX pin muxing for SerialIo UART used for debug}
  gPlatformFspPkgTokenSpaceGuid.SerialIoUartDebugRxPinMux           | * | 0x04 | 0x0

  # !BSF NAME:{SerialIoUartDebugTxPinMux - FSPM} TYPE:{EditNum, HEX, (0x00,0xFFFFFFFF)}
  # !BSF HELP:{Select TX pin muxing for SerialIo UART used for debug}
  gPlatformFspPkgTokenSpaceGuid.SerialIoUartDebugTxPinMux           | * | 0x04 | 0x0

  # !BSF NAME:{SerialIoUartDebugRtsPinMux - FSPM} TYPE:{EditNum, HEX, (0,0xFFFFFFFF)}
  # !BSF HELP:{Select SerialIo Uart used for debug Rts pin muxing. Refer to GPIO_*_MUXING_SERIALIO_UARTx_RTS* for possible values.}
  gPlatformFspPkgTokenSpaceGuid.SerialIoUartDebugRtsPinMux          | * | 0x04 | 0x0

  # !BSF NAME:{SerialIoUartDebugCtsPinMux - FSPM} TYPE:{EditNum, HEX, (0,0xFFFFFFFF)}
  # !BSF HELP:{Select SerialIo Uart used for debug Cts pin muxing. Refer to GPIO_*_MUXING_SERIALIO_UARTx_CTS* for possible values.}
  gPlatformFspPkgTokenSpaceGuid.SerialIoUartDebugCtsPinMux          | * | 0x04 | 0x0

  # !BSF NAME:{Ppr Enable Type} TYPE:{Combo} OPTION:{0:Disable, 2:Hard PPR}
  # !BSF HELP:{Enable Soft or Hard PPR <b>0:Disable</b>, 2:Hard PPR}
  gPlatformFspPkgTokenSpaceGuid.PprEnable                  | * | 0x01 | 0x00

  # !BSF PAGE:{MRC}
  # !BSF NAME:{Margin Limit Check}
  # !BSF TYPE:{Combo} OPTION:{0:Disable, 1:L1, 2:L2, 3:Both}
  # !BSF HELP:{Margin Limit Check. Choose level of margin check}
  gPlatformFspPkgTokenSpaceGuid.MarginLimitCheck            | * | 0x01 | 0x00

  # !BSF PAGE:{MRC}
  # !BSF NAME:{Margin Limit L2}
  # !BSF TYPE:{EditNum, HEX, (0x01, 0x12C)}
  # !BSF HELP:{% of L1 check for margin limit check}
  gPlatformFspPkgTokenSpaceGuid.MarginLimitL2               | * | 0x02 | 100

  # !BSF NAME:{DEKEL CDR Relock} TYPE:{EditNum, HEX, (0x00,0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF)}
  # !BSF HELP:{Enable/Disable CDR Relock. 0: Disable(Default); 1: Enable}
  gPlatformFspPkgTokenSpaceGuid.CpuPcieRpCdrRelock          | * | 0x4 | { 0x01, 0x01, 0x01, 0x01 }

  # !BSF NAME:{DMI DEKEL CDR Relock} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Enable/Disable CPU DMI CDR Relock. 0: Disable(Default); 1: Enable}
  gPlatformFspPkgTokenSpaceGuid.DmiCdrRelock                 | * | 0x1 | 0x0

  # !BSF PAGE:{MRC}
  # !BSF NAME:{IbeccErrInjControl} TYPE:{Combo}
  # !BSF OPTION:{0: No Error Injection, 1:Inject Correctable Error Address match, 3:Inject Correctable Error on insertion counter, 5: Inject Uncorrectable Error Address match, 7:Inject Uncorrectable Error on insertion counter}
  # !BSF HELP:{IBECC Error Injection Control}
  gPlatformFspPkgTokenSpaceGuid.IbeccErrInjControl          | * | 0x01 | 0x0

  # !BSF PAGE:{MRC}
  # !BSF NAME:{IbeccErrInjAddress} TYPE:{EditNum, HEX, (0x0,0x3FFFFFFFFFC0)}
  # !BSF HELP:{Address to match against for ECC error injection}
  gPlatformFspPkgTokenSpaceGuid.IbeccErrInjAddress          | * | 0x08 | 0x0

  # !BSF PAGE:{MRC}
  # !BSF NAME:{IbeccErrInjMask} TYPE:{EditNum, HEX, (0x0,0x3FFFFFFFFFC0)}
  # !BSF HELP:{Mask to match against for ECC error injection}
  gPlatformFspPkgTokenSpaceGuid.IbeccErrInjMask             | * | 0x08 | 0x0

  # !BSF PAGE:{MRC}
  # !BSF NAME:{IbeccErrInjCount} TYPE:{EditNum, HEX, (0x0,0xFFFFFFFF)}
  # !BSF HELP:{Number of transactions between ECC error injection}
  gPlatformFspPkgTokenSpaceGuid.IbeccErrInjCount            | * | 0x04 | 0x0

  # !BSF NAME:{Pointer EnableDmaBuffer} TYPE:{Combo} TYPE:{EditNum, HEX, (0x00,0xFFFFFFFFFFFFFFFF)}
  # !BSF HELP:{Pointer of EnableDmaBuffer Callback Function.}
  gPlatformFspPkgTokenSpaceGuid.EnableDmaBuffer             | * | 0x08 | { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 }

  # !BSF NAME:{PLL Max Banding Ratio} TYPE:{EditNum, HEX, (0x0, 0x78)}
  # !BSF HELP:{DEPRECATED}
  gPlatformFspPkgTokenSpaceGuid.PllMaxBandingRatio          | * | 0x01 | 0x00

  # !BSF PAGE:{MRC}
  # !BSF NAME:{Debug Value}
  # !BSF TYPE:{EditNum, HEX, (0x00,0xFFFFFFFF)}
  # !BSF HELP:{Debug Value}
  gPlatformFspPkgTokenSpaceGuid.DebugValue                  | * | 0x04 | 0x00000000
  # !BSF NAME:{Pre-Mem GPIO table address}
  # !BSF TYPE:{EditNum, HEX, (0x0,0xFFFFFFFF)}
  # !BSF HELP:{AlderLake S needs to assert PCIe SLOT RTD3 and PEG reset pins in early PreMem phase. 0: Skip FSP PCIe pins programming. Refer to mAdlSPcieRstPinGpioTable[] in GpioSampleDef.h.}
  gPlatformFspPkgTokenSpaceGuid.BoardGpioTablePreMemAddress        | * | 0x04 | 0x00000000

  # !BSF NAME:{tRFCpb}
  # !BSF TYPE:{EditNum, HEX, (0x00,0xFFFF)}
  # !BSF HELP:{Min Internal per bank refresh recovery delay time, 0: AUTO, max: 0xFFFF. Only used if FspmUpd->FspmConfig.SpdProfileSelected == 1 (Custom Profile).}
  gPlatformFspPkgTokenSpaceGuid.tRFCpb                      | * | 0x02 | 0x00

  # !BSF NAME:{tRFC2}
  # !BSF TYPE:{EditNum, HEX, (0x00,0xFFFF)}
  # !BSF HELP:{Min Internal refresh recovery delay time, 0: AUTO, max: 0xFFFF. Only used if FspmUpd->FspmConfig.SpdProfileSelected == 1 (Custom Profile).}
  gPlatformFspPkgTokenSpaceGuid.tRFC2                       | * | 0x02 | 0x00

  # !BSF NAME:{tRFC4}
  # !BSF TYPE:{EditNum, HEX, (0x00,0xFFFF)}
  # !BSF HELP:{Min Internal refresh recovery delay time, 0: AUTO, max: 0xFFFF. Only used if FspmUpd->FspmConfig.SpdProfileSelected == 1 (Custom Profile).}
  gPlatformFspPkgTokenSpaceGuid.tRFC4                       | * | 0x02 | 0x00

  # !BSF NAME:{tRRD_L}
  # !BSF TYPE:{EditNum, HEX, (0x00,0xFF)}
  # !BSF HELP:{Min Internal row active to row active delay time for same bank groups, 0: AUTO, max: 80. Only used if FspmUpd->FspmConfig.SpdProfileSelected == 1 (Custom Profile).}
  gPlatformFspPkgTokenSpaceGuid.tRRD_L                      | * | 0x01 | 0x00

  # !BSF NAME:{tRRD_S}
  # !BSF TYPE:{EditNum, HEX, (0x00,0xFF)}
  # !BSF HELP:{Min Internal row active to row active delay time for different bank groups, 0: AUTO, max: 80. Only used if FspmUpd->FspmConfig.SpdProfileSelected == 1 (Custom Profile).}
  gPlatformFspPkgTokenSpaceGuid.tRRD_S                      | * | 0x01 | 0x00

  # !BSF NAME:{tWTR_L}
  # !BSF TYPE:{EditNum, HEX, (0x00,0xFF)}
  # !BSF HELP:{Min Internal write to read command delay time for same bank groups, 0: AUTO, max: 127. Only used if FspmUpd->FspmConfig.SpdProfileSelected == 1 (Custom Profile).}
  gPlatformFspPkgTokenSpaceGuid.tWTR_L                      | * | 0x01 | 0x00

  # !BSF NAME:{tCCD_L}
  # !BSF TYPE:{EditNum, HEX, (0x00,0xFF)}
  # !BSF HELP:{Min Internal CAS-to-CAS delay for same bank group, 0: AUTO, max: 80. Only used if FspmUpd->FspmConfig.SpdProfileSelected == 1 (Custom Profile).}
  gPlatformFspPkgTokenSpaceGuid.tCCD_L                      | * | 0x01 | 0x00

  # !BSF NAME:{tWTR_S}
  # !BSF TYPE:{EditNum, HEX, (0x00,0xFF)}
  # !BSF HELP:{Min Internal write to read command delay time for different bank groups, 0: AUTO, max: 50. Only used if FspmUpd->FspmConfig.SpdProfileSelected == 1 (Custom Profile).}
  gPlatformFspPkgTokenSpaceGuid.tWTR_S                      | * | 0x01 | 0x00

  # !BSF PAGE:{MRC}
  # !BSF NAME:{EccErrInjAddress} TYPE:{EditNum, HEX, (0x0,0x1FFFFFFFF)}
  # !BSF HELP:{Address to match against for ECC error injection}
  gPlatformFspPkgTokenSpaceGuid.EccErrInjAddress            | * | 0x08 | 0x0

  # !BSF PAGE:{MRC}
  # !BSF NAME:{EccErrInjMask} TYPE:{EditNum, HEX, (0x0,0x1FFFFFFFF)}
  # !BSF HELP:{Mask to match against for ECC error injection}
  gPlatformFspPkgTokenSpaceGuid.EccErrInjMask               | * | 0x08 | 0x0

  # !BSF PAGE:{MRC}
  # !BSF NAME:{EccErrInjCount} TYPE:{EditNum, HEX, (0x0,0xFFFFFFFF)}
  # !BSF HELP:{Number of transactions between ECC error injection}
  gPlatformFspPkgTokenSpaceGuid.EccErrInjCount              | * | 0x04 | 0xF

  # !BSF NAME:{Frequency Limit for 2DPC Mixed or non-POR Config}
  # !BSF TYPE:{EditNum, DEC, (0, 10000)}
  # !BSF HELP:{Frequency Limit for 2DPC Mixed or non-POR Config. 0: Auto (default), otherwise a frequency in MT/s}
  gPlatformFspPkgTokenSpaceGuid.FreqLimitMixedConfig        | * | 0x02 | 0x0000

  # !BSF PAGE:{MRC}
  # !BSF NAME:{First Dimm BitMask}
  # !BSF TYPE:{EditNum, HEX, (0x00, 0x0F)}
  # !BSF HELP:{Defines which DIMM should be populated first on a 2DPC board. Bit0: MC0 DIMM0, Bit1: MC0 DIMM1, Bit2: MC1 DIMM0, Bit3: MC1 DIMM1. For each MC, the first DIMM to be populated should be set to '1'}
  gPlatformFspPkgTokenSpaceGuid.FirstDimmBitMask            | * | 0x01 | 0x0A

  # !BSF PAGE:{MRC}
  # !BSF NAME:{SAGV Switch Factor IA DDR BW}
  # !BSF TYPE:{EditNum, DEC, (1,50)}
  # !BSF HELP:{SAGV Switch Factor IA DDR BW: IA DDR load percentage when system switch to high SAGV point from 1 to 50%.}
  gPlatformFspPkgTokenSpaceGuid.SagvSwitchFactorIA          | * | 0x01 | 0x1E

  # !BSF PAGE:{MRC}
  # !BSF NAME:{SAGV Switch Factor GT DDR BW}
  # !BSF TYPE:{EditNum, DEC, (1,50)}
  # !BSF HELP:{SAGV Switch Factor GT DDR BW: GT DDR load percentage when system switch to high SAGV point from 1 to 50%.}
  gPlatformFspPkgTokenSpaceGuid.SagvSwitchFactorGT          | * | 0x01 | 0x1E

  # !BSF PAGE:{MRC}
  # !BSF NAME:{SAGV Switch Factor IO DDR BW}
  # !BSF TYPE:{EditNum, DEC, (1,50)}
  # !BSF HELP:{SAGV Switch Factor IO DDR BW: IO DDR load percentage when system switch to high SAGV point from 1 to 50%.}
  gPlatformFspPkgTokenSpaceGuid.SagvSwitchFactorIO          | * | 0x01 | 0x1E

  # !BSF PAGE:{MRC}
  # !BSF NAME:{SAGV Switch Factor IA and GT Stall}
  # !BSF TYPE:{EditNum, DEC, (1,50)}
  # !BSF HELP:{SAGV Switch Factor IA and GT Stall: IA and GT percentage when system switch to high SAGV point from 1 to 50%.}
  gPlatformFspPkgTokenSpaceGuid.SagvSwitchFactorStall       | * | 0x01 | 0x1E

  # !BSF PAGE:{MRC}
  # !BSF NAME:{Threshold For Switch Down}
  # !BSF TYPE:{EditNum, DEC, (1,50)}
  # !BSF HELP:{"SAGV heuristics down control: Duration in ms of low activity after which SAGV will switch down, from 1 to 50ms.}
  gPlatformFspPkgTokenSpaceGuid.SagvHeuristicsDownControl   | * | 0x01 | 0x01

  # !BSF PAGE:{MRC}
  # !BSF NAME:{Threshold For Switch Up}
  # !BSF TYPE:{EditNum, DEC, (1,50)}
  # !BSF HELP:{"SAGV heuristics up control: Duration in ms of low activity after which SAGV will switch up, from 1 to 50ms.}
  gPlatformFspPkgTokenSpaceGuid.SagvHeuristicsUpControl     | * | 0x01 | 0x01

  # !BSF NAME:{Frequency Limit for Mixed 2DPC DDR5 1 Rank 8GB and 8GB}
  # !BSF TYPE:{EditNum, DEC, (0, 10000)}
  # !BSF HELP:{Frequency Limit for 2DPC Mixed or non-POR Config. 0: Auto, otherwise a frequency in MT/s, default is 2000}
  gPlatformFspPkgTokenSpaceGuid.FreqLimitMixedConfig_1R1R_8GB    | * | 0x02 | 0x07D0

  # !BSF NAME:{Frequency Limit for Mixed 2DPC DDR5 1 Rank 16GB and 16GB}
  # !BSF TYPE:{EditNum, DEC, (0, 10000)}
  # !BSF HELP:{Frequency Limit for 2DPC Mixed or non-POR Config. 0: Auto, otherwise a frequency in MT/s, default is 2000}
  gPlatformFspPkgTokenSpaceGuid.FreqLimitMixedConfig_1R1R_16GB    | * | 0x02 | 0x07D0

  # !BSF NAME:{Frequency Limit for Mixed 2DPC DDR5 1 Rank 8GB and 16GB}
  # !BSF TYPE:{EditNum, DEC, (0, 10000)}
  # !BSF HELP:{Frequency Limit for 2DPC Mixed or non-POR Config. 0: Auto, otherwise a frequency in MT/s, default is 2000}
  gPlatformFspPkgTokenSpaceGuid.FreqLimitMixedConfig_1R1R_8GB_16GB    | * | 0x02 | 0x07D0

  # !BSF NAME:{Frequency Limit for Mixed 2DPC DDR5 2 Rank}
  # !BSF TYPE:{EditNum, DEC, (0, 10000)}
  # !BSF HELP:{Frequency Limit for 2DPC Mixed or non-POR Config. 0: Auto, otherwise a frequency in MT/s, default is 2000}
  gPlatformFspPkgTokenSpaceGuid.FreqLimitMixedConfig_2R2R    | * | 0x02 | 0x07D0

  # !BSF NAME:{DMI Hw Eq Gen3 CoeffList Cm} TYPE:{EditNum, HEX, (0x00,0xFFFFFFFFFF)}
  # !BSF HELP:{PCH_DMI_EQ_PARAM. Coefficient C-1.}
  gPlatformFspPkgTokenSpaceGuid.PchDmiHwEqGen3CoeffListCm         | * | 0x08 | { 0x06, 0x05, 0x02, 0x09, 0x00, 0x00, 0x00, 0x00 }

  # !BSF NAME:{DMI Hw Eq Gen3 CoeffList Cp} TYPE:{EditNum, HEX, (0x00,0xFFFFFFFFFF)}
  # !BSF HELP:{PCH_DMI_EQ_PARAM. Coefficient C+1.}
  gPlatformFspPkgTokenSpaceGuid.PchDmiHwEqGen3CoeffListCp    | * | 0x08 | { 0x05, 0x03, 0x07, 0x08, 0x00, 0x00, 0x00, 0x00 }

  # !BSF NAME:{ LCT Command eyewidth}
  # !BSF TYPE:{EditNum, DEC, (0, 256)}
  # !BSF HELP:{ LCT Command eyewidth. 0: Auto, otherwise eyewidth , default is 96}
  gPlatformFspPkgTokenSpaceGuid.LctCmdEyeWidth    | * | 0x02 | 0x60

  # !BSF NAME:{For LPDDR Only: Throttler CKEMin Timer}
  # !BSF TYPE:{EditNum, HEX, (0x0, 0xFF)}
  # !BSF HELP:{For LPDDR Only: Timer value for CKEMin, range[255;0]. Reqd min of SC_ROUND_T + BYTE_LENGTH (4). Dfault is 0x00}
  gPlatformFspPkgTokenSpaceGuid.ThrtCkeMinTmrLpddr         | * | 0x01 | 0x00

  # !BSF PAGE:{MRC}
  # !BSF NAME:{First ECC Dimm BitMask}
  # !BSF TYPE:{EditNum, HEX, (0x00, 0x0F)}
  # !BSF HELP:{Defines which ECC DIMM should be populated first on a 2DPC board. Bit0: MC0 DIMM0, Bit1: MC0 DIMM1, Bit2: MC1 DIMM0, Bit3: MC1 DIMM1. For each MC, the first DIMM to be populated should be set to '1'}
  gPlatformFspPkgTokenSpaceGuid.FirstDimmBitMaskEcc        | * | 0x01 | 0x0A

  # !BSF NAME:{ LP5 Bank Mode}
  # !BSF TYPE:{Combo} OPTION:{0:Auto, 1:8 Bank Mode, 2:16 Bank Mode, 3:BG Mode}
  # !BSF HELP:{ LP5 Bank Mode. 0: Auto, 1: 8 Bank Mode, 2: 16 Bank Mode, 3: BG Mode, default is 0}
  gPlatformFspPkgTokenSpaceGuid.Lp5BankMode    | * | 0x01 | 0x00

  # !BSF PAGE:{MRC}
  # !BSF NAME:{Write DS Training}
  # !BSF TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Enable/Disable Write DS Training}
  gPlatformFspPkgTokenSpaceGuid.WRDS                        | * | 0x01 | 0x01

   # !HDR EMBED:{FSP_M_CONFIG:FspmConfig:END}
  gPlatformFspPkgTokenSpaceGuid.ReservedFspmUpd2           | * | 0x07 | {0x00}


  # Note please keep "UpdTerminator" at the end of each UPD region.
  # The tool will use this field to determine the actual end of the UPD data
  # structure.

  gPlatformFspPkgTokenSpaceGuid.UpdTerminator               | * | 0x02 | 0x55AA
