## @file
#  Platform description.
#
# @copyright
#  INTEL CONFIDENTIAL
#  Copyright 2017 - 2021 Intel Corporation.
#
#  The source code contained or described herein and all documents related to the
#  source code ("Material") are owned by Intel Corporation or its suppliers or
#  licensors. Title to the Material remains with Intel Corporation or its suppliers
#  and licensors. The Material may contain trade secrets and proprietary and
#  confidential information of Intel Corporation and its suppliers and licensors,
#  and is protected by worldwide copyright and trade secret laws and treaty
#  provisions. No part of the Material may be used, copied, reproduced, modified,
#  published, uploaded, posted, transmitted, distributed, or disclosed in any way
#  without Intel's prior express written permission.
#
#  No license under any patent, copyright, trade secret or other intellectual
#  property right is granted to or conferred upon you by disclosure or delivery
#  of the Materials, either expressly, by implication, inducement, estoppel or
#  otherwise. Any license under such intellectual property rights must be
#  express and approved by Intel in writing.
#
#  Unless otherwise agreed by Intel in writing, you may not remove or alter
#  this notice or any other notice embedded in Materials by Intel or
#  Intel's suppliers or licensors in any way.
#
#  This file contains an 'Intel Peripheral Driver' and is uniquely identified as
#  "Intel Reference Module" and is licensed for Intel CPUs and chipsets under
#  the terms of your license agreement with Intel or your vendor. This file may
#  be modified by the user, subject to additional terms of the license agreement.
#
# @par Specification
##

  ################################################################################
  #
  # UPDs consumed in FspSiliconInit Api
  #
  ################################################################################
  # !BSF FIND:{ADLUPD_S}
  # !HDR COMMENT:{FSP_UPD_HEADER:FSP UPD Header}
  # !HDR EMBED:{FSP_UPD_HEADER:FspUpdHeader:START}
  # FspsUpdSignature: {ADLUPD_S}
  gPlatformFspPkgTokenSpaceGuid.Signature                   | * | 0x08 | 0x535F4450554C4441
  # !BSF NAME:{FspsUpdRevision}  TYPE:{None}
  gPlatformFspPkgTokenSpaceGuid.Revision                    | * | 0x01 | 0x02
  # !HDR EMBED:{FSP_UPD_HEADER:FspUpdHeader:END}
  gPlatformFspPkgTokenSpaceGuid.Reserved                    | * | 0x17 | {0x00}

  # !HDR COMMENT:{FSPS_ARCH_UPD:FSPS_ARCH_UPD}
  # !HDR EMBED:{FSPS_ARCH_UPD:FspsArchUpd:START}
  gPlatformFspPkgTokenSpaceGuid.Revision                    | * | 0x01 | 0x01
  gPlatformFspPkgTokenSpaceGuid.Reserved                    | * | 0x03 | {0x00}
  gPlatformFspPkgTokenSpaceGuid.Length                      | * | 0x04 | 0x00000020
  gPlatformFspPkgTokenSpaceGuid.FspEventHandler             | * | 0x04 | 0x00000000
  gPlatformFspPkgTokenSpaceGuid.EnableMultiPhaseSiliconInit | * | 0x01 | 0x00
  # !HDR EMBED:{FSPS_ARCH_UPD:FspsArchUpd:END}
  gPlatformFspPkgTokenSpaceGuid.Reserved1                   | * | 0x13 | {0x00}

  # !HDR COMMENT:{FSP_S_CONFIG:Fsp S Configuration}
  # !HDR EMBED:{FSP_S_CONFIG:FspsConfig:START}
  # !BSF PAGE:{SA2}
  # !BSF NAME:{Logo Pointer}
  # !BSF TYPE:{EditNum, HEX, (0x0,0xFFFFFFFF)}
  # !BSF HELP:{Points to PEI Display Logo Image}
  gPlatformFspPkgTokenSpaceGuid.LogoPtr                     | * | 0x04 | 0x00000000

  # !BSF NAME:{Logo Size}
  # !BSF TYPE:{EditNum, HEX, (0x0,0xFFFFFFFF)}
  # !BSF HELP:{Size of PEI Display Logo Image}
  gPlatformFspPkgTokenSpaceGuid.LogoSize                    | * | 0x04 | 0x00000000

  # !BSF NAME:{Blt Buffer Address}
  # !BSF TYPE:{EditNum, HEX, (0x0,0xFFFFFFFF)}
  # !BSF HELP:{Address of Blt buffer}
  gPlatformFspPkgTokenSpaceGuid.BltBufferAddress            | * | 0x04 | 0x00000000

  # !BSF NAME:{Blt Buffer Size}
  # !BSF TYPE:{EditNum, HEX, (0x0,0xFFFFFFFF)}
  # !BSF HELP:{Size of Blt Buffer, is equal to PixelWidth * PixelHeight * 4 bytes (the size of EFI_GRAPHICS_OUTPUT_BLT_PIXEL)}
  gPlatformFspPkgTokenSpaceGuid.BltBufferSize               | * | 0x04 | 0x00000000

  # !BSF NAME:{Graphics Configuration Ptr}
  # !BSF TYPE:{EditNum, HEX, (0x0,0xFFFFFFFF)}
  # !BSF HELP:{Points to VBT}
  gPlatformFspPkgTokenSpaceGuid.GraphicsConfigPtr           | * | 0x04 | 0x00000000

  # !BSF NAME:{Enable Device 4}
  # !BSF TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Enable/disable Device 4}
  gPlatformFspPkgTokenSpaceGuid.Device4Enable               | * | 0x01 | 0x00

  # !BSF PAGE:{PCH2}
  # !BSF NAME:{Show SPI controller} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Enable/disable to show SPI controller.}
  gPlatformFspPkgTokenSpaceGuid.ShowSpiController           | * | 0x01 | 0x00

  # !BSF PAGE:{CPU2}
  # !BSF NAME:{MicrocodeRegionBase} TYPE:{EditNum, HEX, (0x0,0xFFFFFFFF)}
  # !BSF HELP:{Memory Base of Microcode Updates}
  gPlatformFspPkgTokenSpaceGuid.MicrocodeRegionBase         | * | 0x04 | 0x0

  # !BSF NAME:{MicrocodeRegionSize} TYPE:{EditNum, HEX, (0x0,0xFFFFFFFF)}
  # !BSF HELP:{Size of Microcode Updates}
  gPlatformFspPkgTokenSpaceGuid.MicrocodeRegionSize         | * | 0x04 | 0x0

  # !BSF NAME:{Turbo Mode} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Enable/Disable Turbo mode. 0: disable, 1: enable}
  gPlatformFspPkgTokenSpaceGuid.TurboMode                   | * | 0x01 | 0x1

  # !BSF PAGE:{PCH2}
  # !BSF NAME:{Enable SATA SALP Support} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Enable/disable SATA Aggressive Link Power Management.}
  gPlatformFspPkgTokenSpaceGuid.SataSalpSupport             | * | 0x01 | 0x01

  # !BSF NAME:{Enable SATA ports} TYPE:{EditNum, HEX, (0x00,0xFFFFFFFFFFFFFFFF)}
  # !BSF HELP:{Enable/disable SATA ports. One byte for each port, byte0 for port0, byte1 for port1, and so on.}
  gPlatformFspPkgTokenSpaceGuid.SataPortsEnable             | * | 0x08 | { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00}

  # !BSF NAME:{Enable SATA DEVSLP Feature} TYPE:{EditNum, HEX, (0x00,0xFFFFFFFFFFFFFFFF)}
  # !BSF HELP:{Enable/disable SATA DEVSLP per port. 0 is disable, 1 is enable. One byte for each port, byte0 for port0, byte1 for port1, and so on.}
  gPlatformFspPkgTokenSpaceGuid.SataPortsDevSlp             | * | 0x08 | { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00}

  # !BSF NAME:{SATA DEVSLP GPIO Pin} TYPE:{EditNum, HEX, (0,0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF)}
  # !HDR STRUCT:{UINT32}
  # !BSF HELP:{Select SATA DEVSLP Pin. Refer to GPIO_*_MUXING_SATA_DEVSLP_x* for possible values.}
  gPlatformFspPkgTokenSpaceGuid.SataPortDevSlpPinMux     | * | 0x20 | { 0, 0, 0, 0, 0, 0, 0, 0 }

  # !BSF NAME:{Enable USB2 ports} TYPE:{EditNum, HEX, (0x00,0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF)}
  # !BSF HELP:{Enable/disable per USB2 ports. One byte for each port, byte0 for port0, byte1 for port1, and so on.}
  gPlatformFspPkgTokenSpaceGuid.PortUsb20Enable             | * | 0x10 | { 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01}

  # !BSF NAME:{Enable USB3 ports} TYPE:{EditNum, HEX, (0x00,0xFFFFFFFFFFFFFFFFFFFF)}
  # !BSF HELP:{Enable/disable per USB3 ports. One byte for each port, byte0 for port0, byte1 for port1, and so on.}
  gPlatformFspPkgTokenSpaceGuid.PortUsb30Enable             | * | 0x0A | { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00}

  # !BSF NAME:{Enable xDCI controller} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Enable/disable to xDCI controller.}
  gPlatformFspPkgTokenSpaceGuid.XdciEnable                  | * | 0x01 | 0x00

  # !BSF NAME:{Address of PCH_DEVICE_INTERRUPT_CONFIG table.} TYPE:{EditNum, HEX, (0x00, 0xFFFFFFFF)}
  # !BSF HELP:{The address of the table of PCH_DEVICE_INTERRUPT_CONFIG.}
  gPlatformFspPkgTokenSpaceGuid.DevIntConfigPtr             | * | 0x04 | 0x00

  # !BSF NAME:{Number of DevIntConfig Entry} TYPE:{EditNum, HEX, (0x00,0x40)}
  # !BSF HELP:{Number of Device Interrupt Configuration Entry. If this is not zero, the DevIntConfigPtr must not be NULL.}
  gPlatformFspPkgTokenSpaceGuid.NumOfDevIntConfig           | * | 0x01 | 0x00

  # !BSF NAME:{PIRQx to IRQx Map Config} TYPE:{EditNum, HEX, (0x00,0xFFFFFFFFFFFFFFFF)}
  # !BSF HELP:{PIRQx to IRQx mapping. The valid value is 0x00 to 0x0F for each. First byte is for PIRQA, second byte is for PIRQB, and so on. The setting is only available in Legacy 8259 PCI mode.}
  gPlatformFspPkgTokenSpaceGuid.PxRcConfig                  | * | 0x08 | { 0x0B, 0x0A, 0x0B, 0x0B, 0x0B, 0x0B, 0x0B, 0x0B}

  # !BSF NAME:{Select GPIO IRQ Route}
  # !BSF TYPE:{EditNum, HEX, (0x00,0x0F)}
  # !BSF HELP:{GPIO IRQ Select. The valid value is 14 or 15.}
  gPlatformFspPkgTokenSpaceGuid.GpioIrqRoute                | * | 0x01 | 0x0E

  # !BSF NAME:{Select SciIrqSelect}
  # !BSF TYPE:{EditNum, HEX, (0x00,0x17)}
  # !BSF HELP:{SCI IRQ Select. The valid value is 9, 10, 11, and 20, 21, 22, 23 for APIC only.}
  gPlatformFspPkgTokenSpaceGuid.SciIrqSelect                | * | 0x01 | 0x09

  # !BSF NAME:{Select TcoIrqSelect}
  # !BSF TYPE:{EditNum, HEX, (0x00,0x17)}
  # !BSF HELP:{TCO IRQ Select. The valid value is 9, 10, 11, 20, 21, 22, 23.}
  gPlatformFspPkgTokenSpaceGuid.TcoIrqSelect                | * | 0x01 | 0x09

  # !BSF NAME:{Enable/Disable Tco IRQ}
  # !BSF TYPE:{Combo} OPTION:{$EN_DIS}}
  # !BSF HELP:{Enable/disable TCO IRQ}
  gPlatformFspPkgTokenSpaceGuid.TcoIrqEnable                | * | 0x01 | 0x00

  # !BSF NAME:{PCH HDA Verb Table Entry Number} TYPE:{EditNum, HEX, (0x0,0xFF)}
  # !BSF HELP:{Number of Entries in Verb Table.}
  gPlatformFspPkgTokenSpaceGuid.PchHdaVerbTableEntryNum     | * | 1 | 0

  # !BSF NAME:{PCH HDA Verb Table Pointer} TYPE:{EditNum, HEX, (0x0,0xFFFFFFFF)}
  # !BSF HELP:{Pointer to Array of pointers to Verb Table.}
  gPlatformFspPkgTokenSpaceGuid.PchHdaVerbTablePtr          | * | 4 | 0

  # !BSF NAME:{PCH HDA Codec Sx Wake Capability} TYPE:{$EN_DIS}
  # !BSF HELP:{Capability to detect wake initiated by a codec in Sx}
  gPlatformFspPkgTokenSpaceGuid.PchHdaCodecSxWakeCapability | * | 0x01 | 0x00

  # !BSF NAME:{Enable SATA} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Enable/disable SATA controller.}
  gPlatformFspPkgTokenSpaceGuid.SataEnable                  | * | 0x01 | 0x01

  # !BSF NAME:{SATA Mode} TYPE:{Combo}
  # !BSF OPTION:{0:AHCI, 1:RAID}
  # !BSF HELP:{Select SATA controller working mode.}
  gPlatformFspPkgTokenSpaceGuid.SataMode                    | * | 0x01 | 0x00

  # !BSF NAME:{SPIn Device Mode} TYPE:{EditNum, HEX, (0x00,0xFFFFFFFFFFFFFF)}
  # !BSF HELP:{Selects SPI operation mode. N represents controller index: SPI0, SPI1, ... Available modes: 0:SerialIoSpiDisabled, 1:SerialIoSpiPci, 2:SerialIoSpiHidden}
  gPlatformFspPkgTokenSpaceGuid.SerialIoSpiMode             | * | 0x07 | { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 }

  # !BSF NAME:{SPI<N> Chip Select Polarity} TYPE:{EditNum, HEX, (0x00,0xFFFFFFFFFFFFFFFFFFFFFFFFFFFF)}
  # !BSF HELP:{Sets polarity for each chip Select. Available options: 0:SerialIoSpiCsActiveLow, 1:SerialIoSpiCsActiveHigh}
  gPlatformFspPkgTokenSpaceGuid.SerialIoSpiCsPolarity       | * | 0x0E | { 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01}

  # !BSF NAME:{SPI<N> Chip Select Enable} TYPE:{EditNum, HEX, (0x00,0xFFFFFFFFFFFFFFFFFFFFFFFFFFFF)}
  # !BSF HELP:{0:Disabled, 1:Enabled. Enables GPIO for CS0 or CS1 if it is Enabled}
  gPlatformFspPkgTokenSpaceGuid.SerialIoSpiCsEnable         | * | 0x0E | { 0x01, 0x00, 0x01, 0x00, 0x01, 0x00, 0x01, 0x00, 0x01, 0x00, 0x01, 0x00, 0x01, 0x00}

  # !BSF NAME:{SPIn Default Chip Select Output} TYPE:{EditNum, HEX, (0x00,0xFFFFFFFFFFFFFF)}
  # !BSF HELP:{Sets Default CS as Output. N represents controller index: SPI0, SPI1, ... Available options: 0:CS0, 1:CS1}
  gPlatformFspPkgTokenSpaceGuid.SerialIoSpiDefaultCsOutput  | * | 0x07 | { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 }

  # !BSF NAME:{SPIn Default Chip Select Mode HW/SW} TYPE:{EditNum, HEX, (0x00,0xFFFFFFFFFFFFFF)}
  # !BSF HELP:{Sets Default CS Mode Hardware or Software. N represents controller index: SPI0, SPI1, ... Available options: 0:HW, 1:SW}
  gPlatformFspPkgTokenSpaceGuid.SerialIoSpiCsMode           | * | 0x07 | { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 }

  # !BSF NAME:{SPIn Default Chip Select State Low/High} TYPE:{EditNum, HEX, (0x00,0xFFFFFFFFFFFFFF)}
  # !BSF HELP:{Sets Default CS State Low or High. N represents controller index: SPI0, SPI1, ... Available options: 0:Low, 1:High}
  gPlatformFspPkgTokenSpaceGuid.SerialIoSpiCsState          | * | 0x07 | { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 }

  # !BSF NAME:{UARTn Device Mode} TYPE:{EditNum, HEX, (0x00,0xFFFFFFFFFFFFFF)}
  # !BSF HELP:{Selects Uart operation mode. N represents controller index: Uart0, Uart1, ... Available modes: 0:SerialIoUartDisabled, 1:SerialIoUartPci, 2:SerialIoUartHidden, 3:SerialIoUartCom, 4:SerialIoUartSkipInit}
  gPlatformFspPkgTokenSpaceGuid.SerialIoUartMode            | * | 0x07 | { 0x00, 0x00, 0x02, 0x00, 0x00, 0x00, 0x00}

  # !BSF NAME:{Default BaudRate for each Serial IO UART} TYPE:{EditNum, HEX, (0x0,0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF)}
  # !HDR STRUCT:{UINT32}
  # !BSF HELP:{Set default BaudRate Supported from 0 - default to 6000000}
  gPlatformFspPkgTokenSpaceGuid.SerialIoUartBaudRate        | * | 0x1C | { 115200, 115200, 115200, 115200, 115200, 115200, 115200 }

  # !BSF NAME:{Default ParityType for each Serial IO UART} TYPE:{EditNum, HEX, (0x0,0xFFFFFFFFFFFFFF)}
  # !BSF HELP:{Set default Parity. 0: DefaultParity, 1: NoParity, 2: EvenParity, 3: OddParity}
  gPlatformFspPkgTokenSpaceGuid.SerialIoUartParity          | * | 0x07 | { 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01 }

  # !BSF NAME:{Default DataBits for each Serial IO UART} TYPE:{EditNum, HEX, (0x0,0xFFFFFFFFFFFFFF)}
  # !BSF HELP:{Set default word length. 0: Default, 5,6,7,8}
  gPlatformFspPkgTokenSpaceGuid.SerialIoUartDataBits        | * | 0x07 | { 0x08, 0x08, 0x08, 0x08, 0x08, 0x08, 0x08 }

  # !BSF NAME:{Default StopBits for each Serial IO UART} TYPE:{EditNum, HEX, (0x0,0xFFFFFFFFFFFFFF)}
  # !BSF HELP:{Set default stop bits. 0: DefaultStopBits, 1: OneStopBit, 2: OneFiveStopBits, 3: TwoStopBits}
  gPlatformFspPkgTokenSpaceGuid.SerialIoUartStopBits        | * | 0x07 | { 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01 }

  # !BSF NAME:{Power Gating mode for each Serial IO UART that works in COM mode} TYPE:{EditNum, HEX, (0x0,0xFFFFFFFFFFFFFF)}
  # !BSF HELP:{Set Power Gating. 0: Disabled, 1: Enabled, 2: Auto}
  gPlatformFspPkgTokenSpaceGuid.SerialIoUartPowerGating     | * | 0x07 | { 0x02, 0x02, 0x02, 0x02, 0x02, 0x02, 0x02 }

  # !BSF NAME:{Enable Dma for each Serial IO UART that supports it} TYPE:{EditNum, HEX, (0x0,0xFFFFFFFFFFFFFF)}
  # !BSF HELP:{Set DMA/PIO mode. 0: Disabled, 1: Enabled}
  gPlatformFspPkgTokenSpaceGuid.SerialIoUartDmaEnable       | * | 0x07 | { 0x01, 0x01, 0x00, 0x01, 0x01, 0x00, 0x00 }

  # !BSF NAME:{Enables UART hardware flow control, CTS and RTS lines} TYPE:{EditNum, HEX, (0x00,0xFFFFFFFFFFFFFF)}
  # !BSF HELP:{Enables UART hardware flow control, CTS and RTS lines.}
  gPlatformFspPkgTokenSpaceGuid.SerialIoUartAutoFlow        | * | 0x07 | { 0x01, 0x01, 0x00, 0x01, 0x01, 0x00, 0x00 }

  # !BSF NAME:{SerialIoUartRtsPinMuxPolicy} TYPE:{EditNum, HEX, (0,0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF)}
  # !HDR STRUCT:{UINT32}
  # !BSF HELP:{Select SerialIo Uart Rts pin muxing. Refer to GPIO_*_MUXING_SERIALIO_UARTx_RTS* for possible values.}
  gPlatformFspPkgTokenSpaceGuid.SerialIoUartRtsPinMuxPolicy | * | 0x1C | { 0, 0, 0, 0, 0, 0, 0 }

  # !BSF NAME:{SerialIoUartCtsPinMuxPolicy} TYPE:{EditNum, HEX, (0,0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF)}
  # !HDR STRUCT:{UINT32}
  # !BSF HELP:{Select SerialIo Uart Cts pin muxing. Refer to GPIO_*_MUXING_SERIALIO_UARTx_CTS* for possible values.}
  gPlatformFspPkgTokenSpaceGuid.SerialIoUartCtsPinMuxPolicy | * | 0x1C | { 0, 0, 0, 0, 0, 0, 0 }

  # !BSF NAME:{SerialIoUartRxPinMuxPolicy} TYPE:{EditNum, HEX, (0,0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF)}
  # !HDR STRUCT:{UINT32}
  # !BSF HELP:{Select SerialIo Uart Rx pin muxing. Refer to GPIO_*_MUXING_SERIALIO_UARTx_RX* for possible values.}
  gPlatformFspPkgTokenSpaceGuid.SerialIoUartRxPinMuxPolicy  | * | 0x1C | { 0, 0, 0, 0, 0, 0, 0 }

  # !BSF NAME:{SerialIoUartTxPinMuxPolicy} TYPE:{EditNum, HEX, (0,0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF)}
  # !HDR STRUCT:{UINT32}
  # !BSF HELP:{Select SerialIo Uart Tx pin muxing. Refer to GPIO_*_MUXING_SERIALIO_UARTx_TX* for possible values.}
  gPlatformFspPkgTokenSpaceGuid.SerialIoUartTxPinMuxPolicy  | * | 0x1C | { 0, 0, 0, 0, 0, 0, 0 }

!if gSiPkgTokenSpaceGuid.PcdAdlLpSupport == TRUE
  # !BSF PAGE:{DBG}
  # !BSF NAME:{UART Number For Debug Purpose} TYPE:{Combo} OPTION:{ 0:UART0, 1:UART1, 2:UART2, 3:UART3, 4:UART4, 5:UART5, 6:UART6 }
  # !BSF HELP:{UART number for debug purpose. 0:UART0, 1:UART1, 2:UART2, 3:UART3, 4:UART4, 5:UART5, 6:UART6. Note: If UART0 is selected as CNVi BT Core interface, it cannot be used for debug purpose.}
  gPlatformFspPkgTokenSpaceGuid.SerialIoDebugUartNumber     | * | 0x01 | 0x00
!else
  # !BSF PAGE:{DBG}
  # !BSF NAME:{UART Number For Debug Purpose} TYPE:{Combo} OPTION:{ 0:UART0, 1:UART1, 2:UART2, 3:UART3, 4:UART4, 5:UART5, 6:UART6 }
  # !BSF HELP:{UART number for debug purpose. 0:UART0, 1:UART1, 2:UART2, 3:UART3, 4:UART4, 5:UART5, 6:UART6. Note: If UART0 is selected as CNVi BT Core interface, it cannot be used for debug purpose.}
  gPlatformFspPkgTokenSpaceGuid.SerialIoDebugUartNumber     | * | 0x01 | 0x02
!endif

  # !BSF NAME:{Serial IO UART DBG2 table} TYPE:{EditNum, HEX, (0x00,0xFFFFFFFFFFFFFF)}
  # !BSF HELP:{Enable or disable Serial Io UART DBG2 table, default is Disable; <b>0: Disable;</b> 1: Enable.}
  gPlatformFspPkgTokenSpaceGuid.SerialIoUartDbg2            | * | 0x07 | { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 }

  # !BSF PAGE:{PCH2}
  # !BSF NAME:{I2Cn Device Mode} TYPE:{EditNum, HEX,  (0x00,0xFFFFFFFFFFFFFFFFFFFF)}
  # !BSF HELP:{Selects I2c operation mode. N represents controller index: I2c0, I2c1, ... Available modes: 0:SerialIoI2cDisabled, 1:SerialIoI2cPci, 2:SerialIoI2cHidden}
  gPlatformFspPkgTokenSpaceGuid.SerialIoI2cMode             | * | 0x08 | { 0x01, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 }

  # !BSF NAME:{Serial IO I2C SDA Pin Muxing} TYPE:{EditNum, HEX, (0,0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF)}
  # !HDR STRUCT:{UINT32}
  # !BSF HELP:{Select SerialIo I2c Sda pin muxing. Refer to GPIO_*_MUXING_SERIALIO_I2Cx_SDA* for possible values.}
  gPlatformFspPkgTokenSpaceGuid.PchSerialIoI2cSdaPinMux     | * | 0x20 | { 0, 0, 0, 0, 0, 0, 0, 0 }

  # !BSF NAME:{Serial IO I2C SCL Pin Muxing} TYPE:{EditNum, HEX, (0,0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF)}
  # !HDR STRUCT:{UINT32}
  # !BSF HELP:{Select SerialIo I2c Scl pin muxing. Refer to GPIO_*_MUXING_SERIALIO_I2Cx_SCL* for possible values.}
  gPlatformFspPkgTokenSpaceGuid.PchSerialIoI2cSclPinMux     | * | 0x20 | { 0, 0, 0, 0, 0, 0, 0, 0 }

  # !BSF NAME:{PCH SerialIo I2C Pads Termination} TYPE:{EditNum, HEX, (0x00, 0xFFFFFFFFFFFFFFFF)}
  # !BSF HELP:{0x0: Hardware default, 0x1: None, 0x13: 1kOhm weak pull-up, 0x15: 5kOhm weak pull-up, 0x19: 20kOhm weak pull-up - Enable/disable SerialIo I2C0,I2C1,... pads termination respectively. One byte for each controller, byte0 for I2C0, byte1 for I2C1, and so on.}
  gPlatformFspPkgTokenSpaceGuid.PchSerialIoI2cPadsTermination | * | 0x08 | { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00  }

  # !BSF NAME:{ISH GP GPIO Pin Muxing} TYPE:{EditNum, HEX, (0, 0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF)}
  # !HDR STRUCT:{UINT32}
  # !BSF HELP:{Determines ISH GP GPIO Pin muxing. See GPIO_*_MUXING_ISH_GP_x_GPIO_*. 'x' are GP_NUMBER}
  gPlatformFspPkgTokenSpaceGuid.IshGpGpioPinMuxing          | * | 0x20 | { 0, 0, 0, 0, 0, 0, 0, 0 }

  # !BSF NAME:{ISH UART Rx Pin Muxing} TYPE:{EditNum, HEX, (0x00,0xFFFFFFFFFFFFFFFFFFFFFFFF)}
  # !HDR STRUCT:{UINT32}
  # !BSF HELP:{Determines ISH UART Rx Pin muxing. See GPIO_*_MUXING_ISH_UARTx_TXD_*}
  gPlatformFspPkgTokenSpaceGuid.IshUartRxPinMuxing          | * | 0xC | { 0, 0, 0 }

  # !BSF NAME:{ISH UART Tx Pin Muxing} TYPE:{EditNum, HEX, (0x00,0xFFFFFFFFFFFFFFFFFFFFFFFF)}
  # !HDR STRUCT:{UINT32}
  # !BSF HELP:{Determines ISH UART Tx Pin muxing. See GPIO_*_MUXING_ISH_UARTx_RXD_*}
  gPlatformFspPkgTokenSpaceGuid.IshUartTxPinMuxing          | * | 0xC | { 0, 0, 0 }

  # !BSF NAME:{ISH UART Rts Pin Muxing} TYPE:{EditNum, HEX, (0x00,0xFFFFFFFFFFFFFFFFFFFFFFFF)}
  # !HDR STRUCT:{UINT32}
  # !BSF HELP:{Select ISH UART Rts Pin muxing. Refer to GPIO_*_MUXING_ISH_UARTx_RTS_* for possible values.}
  gPlatformFspPkgTokenSpaceGuid.IshUartRtsPinMuxing         | * | 0xC | { 0, 0, 0 }

  # !BSF NAME:{ISH UART Rts Pin Muxing} TYPE:{EditNum, HEX, (0x00,0xFFFFFFFFFFFFFFFFFFFFFFFF)}
  # !HDR STRUCT:{UINT32}
  # !BSF HELP:{Select ISH UART Cts Pin muxing. Refer to GPIO_*_MUXING_ISH_UARTx_CTS_* for possible values.}
  gPlatformFspPkgTokenSpaceGuid.IshUartCtsPinMuxing         | * | 0xC | { 0, 0, 0 }

  # !BSF NAME:{ISH I2C SDA Pin Muxing} TYPE:{EditNum, HEX, (0x00,0xFFFFFFFFFFFFFFFFFFFFFFFF)}
  # !HDR STRUCT:{UINT32}
  # !BSF HELP:{Select ISH I2C SDA Pin muxing. Refer to GPIO_*_MUXING_ISH_I2Cx_SDA_* for possible values.}
  gPlatformFspPkgTokenSpaceGuid.IshI2cSdaPinMuxing          | * | 0xC | { 0, 0, 0 }

  # !BSF NAME:{ISH I2C SCL Pin Muxing} TYPE:{EditNum, HEX, (0x00,0xFFFFFFFFFFFFFFFFFFFFFFFF)}
  # !HDR STRUCT:{UINT32}
  # !BSF HELP:{Select ISH I2C SCL Pin muxing. Refer to GPIO_*_MUXING_ISH_I2Cx_SCL_* for possible values.}
  gPlatformFspPkgTokenSpaceGuid.IshI2cSclPinMuxing          | * | 0xC | { 0, 0, 0 }

  # !BSF NAME:{ISH SPI MOSI Pin Muxing} TYPE:{EditNum, HEX, (0x00,0xFFFFFFFFFFFFFFFF)}
  # !HDR STRUCT:{UINT32}
  # !BSF HELP:{Select ISH SPI MOSI Pin muxing. Refer to GPIO_*_MUXING_ISH_SPIx_MOSI_* for possible values.}
  gPlatformFspPkgTokenSpaceGuid.IshSpiMosiPinMuxing         | * | 0x8 | { 0, 0 }

  # !BSF NAME:{ISH SPI MISO Pin Muxing} TYPE:{EditNum, HEX, (0x00,0xFFFFFFFFFFFFFFFF)}
  # !HDR STRUCT:{UINT32}
  # !BSF HELP:{Select ISH SPI MISO Pin muxing. Refer to GPIO_*_MUXING_ISH_SPIx_MISO_* for possible values.}
  gPlatformFspPkgTokenSpaceGuid.IshSpiMisoPinMuxing         | * | 0x8 | { 0, 0 }

  # !BSF NAME:{ISH SPI CLK Pin Muxing} TYPE:{EditNum, HEX, (0x00,0xFFFFFFFFFFFFFFFF)}
  # !HDR STRUCT:{UINT32}
  # !BSF HELP:{Select ISH SPI CLK Pin muxing. Refer to GPIO_*_MUXING_ISH_SPIx_CLK_* for possible values.}
  gPlatformFspPkgTokenSpaceGuid.IshSpiClkPinMuxing          | * | 0x8 | { 0, 0 }

  # !BSF NAME:{ISH SPI CS#N Pin Muxing} TYPE:{EditNum, HEX, (0x00,0xFFFFFFFFFFFFFFFF)}
  # !HDR STRUCT:{UINT32}
  # !BSF HELP:{Select ISH SPI CS#N Pin muxing. Refer to GPIO_*_MUXING_ISH_SPIx_CS<N>_* for possible values. N-SPI number, 0-1.}
  gPlatformFspPkgTokenSpaceGuid.IshSpiCsPinMuxing           | * | 0x10 | { 0, 0 , 0, 0}

  # !BSF NAME:{ISH GP GPIO Pad termination} TYPE:{EditNum, HEX, (0, 0xFFFFFFFFFFFFFFFF)}
  # !BSF HELP:{0x0: Hardware default, 0x1: None, 0x13: 1kOhm weak pull-up, 0x15: 5kOhm weak pull-up, 0x19: 20kOhm weak pull-up - Enable/disable SerialIo GP#N GPIO pads termination respectively. #N are GP_NUMBER, not strictly relate to indexes of this table. Index 0-23 -> ISH_GP_0-23, Index 24-25 -> ISH_GP_30-31}
  gPlatformFspPkgTokenSpaceGuid.IshGpGpioPadTermination          | * | 0x08 | { 0, 0, 0, 0, 0, 0, 0, 0 }

  # !BSF NAME:{ISH UART Rx Pad termination} TYPE:{EditNum, HEX, (0x00,0xFFFFFF)}
  # !BSF HELP:{0x0: Hardware default, 0x1: None, 0x13: 1kOhm weak pull-up, 0x15: 5kOhm weak pull-up, 0x19: 20kOhm weak pull-up - Enable/disable SerialIo UART#N Rx pads termination respectively. #N-byte for each controller, byte0 for UART0 Rx, byte1 for UART1 Rx, and so on.}
  gPlatformFspPkgTokenSpaceGuid.IshUartRxPadTermination          | * | 0x3 | { 0, 0, 0 }

  # !BSF NAME:{ISH UART Tx Pad termination} TYPE:{EditNum, HEX, (0x00,0xFFFFFF)}
  # !BSF HELP:{0x0: Hardware default, 0x1: None, 0x13: 1kOhm weak pull-up, 0x15: 5kOhm weak pull-up, 0x19: 20kOhm weak pull-up - Enable/disable SerialIo UART#N Tx pads termination respectively. #N-byte for each controller, byte0 for UART0 Tx, byte1 for UART1 Tx, and so on.}
  gPlatformFspPkgTokenSpaceGuid.IshUartTxPadTermination          | * | 0x3 | { 0, 0, 0 }

  # !BSF NAME:{ISH UART Rts Pad termination} TYPE:{EditNum, HEX, (0x00,0xFFFFFF)}
  # !BSF HELP:{0x0: Hardware default, 0x1: None, 0x13: 1kOhm weak pull-up, 0x15: 5kOhm weak pull-up, 0x19: 20kOhm weak pull-up - Enable/disable SerialIo UART#N Rts pads termination respectively. #N-byte for each controller, byte0 for UART0 Rts, byte1 for UART1 Rts, and so on.}
  gPlatformFspPkgTokenSpaceGuid.IshUartRtsPadTermination         | * | 0x3 | { 0, 0, 0 }

  # !BSF NAME:{ISH UART Rts Pad termination} TYPE:{EditNum, HEX, (0x00,0xFFFFFF)}
  # !BSF HELP:{0x0: Hardware default, 0x1: None, 0x13: 1kOhm weak pull-up, 0x15: 5kOhm weak pull-up, 0x19: 20kOhm weak pull-up - Enable/disable SerialIo UART#N Cts pads termination respectively. #N-byte for each controller, byte0 for UART0 Cts, byte1 for UART1 Cts, and so on.}
  gPlatformFspPkgTokenSpaceGuid.IshUartCtsPadTermination         | * | 0x3 | { 0, 0, 0 }

  # !BSF NAME:{ISH I2C SDA Pad termination} TYPE:{EditNum, HEX, (0x00,0xFFFFFF)}
  # !BSF HELP:{0x0: Hardware default, 0x1: None, 0x13: 1kOhm weak pull-up, 0x15: 5kOhm weak pull-up, 0x19: 20kOhm weak pull-up - Enable/disable SerialIo I2C#N Sda pads termination respectively. #N-byte for each controller, byte0 for I2C0 Sda, byte1 for I2C1 Sda, and so on.}
  gPlatformFspPkgTokenSpaceGuid.IshI2cSdaPadTermination          | * | 0x3 | { 0, 0, 0 }

  # !BSF NAME:{ISH I2C SCL Pad termination} TYPE:{EditNum, HEX, (0x00,0xFFFFFF)}
  # !BSF HELP:{0x0: Hardware default, 0x1: None, 0x13: 1kOhm weak pull-up, 0x15: 5kOhm weak pull-up, 0x19: 20kOhm weak pull-up - Enable/disable SerialIo I2C#N Scl pads termination respectively. #N-byte for each controller, byte0 for I2C0 Scl, byte1 for I2C1 Scl, and so on.}
  gPlatformFspPkgTokenSpaceGuid.IshI2cSclPadTermination          | * | 0x3 | { 0, 0, 0 }

  # !BSF NAME:{ISH SPI MOSI Pad termination} TYPE:{EditNum, HEX, (0x00,0xFFFF)}
  # !BSF HELP:{0x0: Hardware default, 0x1: None, 0x13: 1kOhm weak pull-up, 0x15: 5kOhm weak pull-up, 0x19: 20kOhm weak pull-up - Enable/disable SerialIo SPI#N Mosi pads termination respectively. #N-byte for each controller, byte0 for SPI0 Mosi, byte1 for SPI1 Mosi, and so on.}
  gPlatformFspPkgTokenSpaceGuid.IshSpiMosiPadTermination         | * | 0x2 | { 0, 0 }

  # !BSF NAME:{ISH SPI MISO Pad termination} TYPE:{EditNum, HEX, (0x00,0xFFFF)}
  # !BSF HELP:{0x0: Hardware default, 0x1: None, 0x13: 1kOhm weak pull-up, 0x15: 5kOhm weak pull-up, 0x19: 20kOhm weak pull-up - Enable/disable SerialIo SPI#N Miso pads termination respectively. #N-byte for each controller, byte0 for SPI0 Miso, byte1 for SPI1 Miso, and so on.}
  gPlatformFspPkgTokenSpaceGuid.IshSpiMisoPadTermination         | * | 0x2 | { 0, 0 }

  # !BSF NAME:{ISH SPI CLK Pad termination} TYPE:{EditNum, HEX, (0x00,0xFFFF)}
  # !BSF HELP:{0x0: Hardware default, 0x1: None, 0x13: 1kOhm weak pull-up, 0x15: 5kOhm weak pull-up, 0x19: 20kOhm weak pull-up - Enable/disable SerialIo SPI#N Clk pads termination respectively. #N-byte for each controller, byte0 for SPI0 Clk, byte1 for SPI1 Clk, and so on.}
  gPlatformFspPkgTokenSpaceGuid.IshSpiClkPadTermination          | * | 0x2 | { 0, 0 }

  # !BSF NAME:{ISH SPI CS#N Pad termination} TYPE:{EditNum, HEX, (0x00,0xFFFFFFFF)}
  # !BSF HELP:{0x0: Hardware default, 0x1: None, 0x13: 1kOhm weak pull-up, 0x15: 5kOhm weak pull-up, 0x19: 20kOhm weak pull-up - Enable/disable SerialIo SPI#N Cs#M pads termination respectively. N*M-byte for each controller, byte0 for SPI0 Cs0, byte1 for SPI1 Cs1, SPI1 Cs0, byte2, SPI1 Cs1, byte3}
  gPlatformFspPkgTokenSpaceGuid.IshSpiCsPadTermination           | * | 0x4 | { 0, 0, 0, 0 }

  # !BSF NAME:{Enable PCH ISH SPI Cs#N pins assigned} TYPE:{EditNum, HEX, (0x0,0xFFFFFFFF)}
  # !BSF HELP:{Set if ISH SPI Cs#N pins are to be enabled by BIOS. 0: Disable; 1: Enable. N-Cs number: 0-1}
  gPlatformFspPkgTokenSpaceGuid.PchIshSpiCsEnable           | * | 0x04 | { 0x00, 0x00, 0x00, 0x00 }

  # !BSF NAME:{USB Per Port HS Preemphasis Bias} TYPE:{EditNum, HEX, (0x00,0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF)}
  # !BSF HELP:{USB Per Port HS Preemphasis Bias. 000b-0mV, 001b-11.25mV, 010b-16.9mV, 011b-28.15mV, 100b-28.15mV, 101b-39.35mV, 110b-45mV, 111b-56.3mV. One byte for each port.}
  gPlatformFspPkgTokenSpaceGuid.Usb2PhyPetxiset             | * | 0x10 | {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00}

  # !BSF NAME:{USB Per Port HS Transmitter Bias} TYPE:{EditNum, HEX, (0x00,0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF)}
  # !BSF HELP:{USB Per Port HS Transmitter Bias. 000b-0mV, 001b-11.25mV, 010b-16.9mV, 011b-28.15mV, 100b-28.15mV, 101b-39.35mV, 110b-45mV, 111b-56.3mV, One byte for each port.}
  gPlatformFspPkgTokenSpaceGuid.Usb2PhyTxiset               | * | 0x10 | {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x05, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00}

  # !BSF NAME:{USB Per Port HS Transmitter Emphasis} TYPE:{EditNum, HEX, (0x00,0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF)}
  # !BSF HELP:{USB Per Port HS Transmitter Emphasis. 00b - Emphasis OFF, 01b - De-emphasis ON, 10b - Pre-emphasis ON, 11b - Pre-emphasis & De-emphasis ON. One byte for each port.}
  gPlatformFspPkgTokenSpaceGuid.Usb2PhyPredeemp             | * | 0x10 | {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x05, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00}

  # !BSF NAME:{USB Per Port Half Bit Pre-emphasis} TYPE:{EditNum, HEX, (0x00,0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF)}
  # !BSF HELP:{USB Per Port Half Bit Pre-emphasis. 1b - half-bit pre-emphasis, 0b - full-bit pre-emphasis. One byte for each port.}
  gPlatformFspPkgTokenSpaceGuid.Usb2PhyPehalfbit            | * | 0x10 | {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x05, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00}

  # !BSF NAME:{Enable the write to USB 3.0 TX Output -3.5dB De-Emphasis Adjustment} TYPE:{EditNum, HEX, (0x00,0x01010101010101010101)}
  # !BSF HELP:{Enable the write to USB 3.0 TX Output -3.5dB De-Emphasis Adjustment. Each value in arrary can be between 0-1. One byte for each port.}
  gPlatformFspPkgTokenSpaceGuid.Usb3HsioTxDeEmphEnable      | * | 0x0A | {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00}

  # !BSF NAME:{USB 3.0 TX Output -3.5dB De-Emphasis Adjustment Setting} TYPE:{EditNum, HEX, (0x00,0xFFFFFFFFFFFFFFFFFFFF)}
  # !BSF HELP:{USB 3.0 TX Output -3.5dB De-Emphasis Adjustment Setting, HSIO_TX_DWORD5[21:16], <b>Default = 29h</b> (approximately -3.5dB De-Emphasis). One byte for each port.}
  gPlatformFspPkgTokenSpaceGuid.Usb3HsioTxDeEmph            | * | 0x0A | {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00}

  # !BSF NAME:{Enable the write to USB 3.0 TX Output Downscale Amplitude Adjustment} TYPE:{EditNum, HEX, (0x00,0x01010101010101010101)}
  # !BSF HELP:{Enable the write to USB 3.0 TX Output Downscale Amplitude Adjustment, Each value in arrary can be between 0-1. One byte for each port.}
  gPlatformFspPkgTokenSpaceGuid.Usb3HsioTxDownscaleAmpEnable  | * | 0x0A | {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00}

  # !BSF NAME:{USB 3.0 TX Output Downscale Amplitude Adjustment} TYPE:{EditNum, HEX, (0x00,0xFFFFFFFFFFFFFFFFFFFF)}
  # !BSF HELP:{USB 3.0 TX Output Downscale Amplitude Adjustment, HSIO_TX_DWORD8[21:16], <b>Default = 00h</b>. One byte for each port.}
  gPlatformFspPkgTokenSpaceGuid.Usb3HsioTxDownscaleAmp      | * | 0x0A | {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00}

  # !BSF NAME: {USB 3.0 adapted linear equalization parameters programming enable} TYPE:{EditNum, HEX, (0x00,0xFFFFFFFFFFFFFFFFFFFF)}
  # !BSF HELP: {After enabling BIOS will program adapter linear equalization parameters <b>Default = 0h</b>.}
  gPlatformFspPkgTokenSpaceGuid.PchUsb3HsioCtrlAdaptOffsetCfgEnable   | * | 0xA | {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00}

  # !BSF NAME: {USB 3.0 LFPS sensitivity levels parameter programming enable N} TYPE:{EditNum, HEX, (0x00,0xFFFFFFFFFFFFFFFFFFFF)}
  # !BSF HELP: {Enabling this option will enable LFPS parameters programming<b>Default = 0h</b>.}
  gPlatformFspPkgTokenSpaceGuid.PchUsb3HsioFilterSelNEnable   | * | 0xA | {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00}

  # !BSF NAME: {USB 3.0 LFPS sensitivity levels parameter programming enable P} TYPE:{EditNum, HEX, (0x00,0xFFFFFFFFFFFFFFFFFFFF)}
  # !BSF HELP: {Enabling this option will enable LFPS parameters programming<b>Default = 0h</b>.}
  gPlatformFspPkgTokenSpaceGuid.PchUsb3HsioFilterSelPEnable   | * | 0xA | {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00}

  # !BSF NAME: {USB 3.0 input offset control enable} TYPE:{EditNum, HEX, (0x00,0xFFFFFFFFFFFFFFFFFFFF)}
  # !BSF HELP: {Enables programming of input offset parameter, <b>Default = 0h</b>.}
  gPlatformFspPkgTokenSpaceGuid.PchUsb3HsioOlfpsCfgPullUpDwnResEnable   | * | 0xA | {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00}

  # !BSF NAME: {USB 3.0 adapted linear equalization parameters value} TYPE:{EditNum, HEX, (0x00,0xFFFFFFFFFFFFFFFFFFFF)}
  # !BSF HELP: {This will change the actual adapted linear equalization value CTLE_ADAPT, <b>Default = 0h</b>.}
  gPlatformFspPkgTokenSpaceGuid.PchUsb3HsioCtrlAdaptOffsetCfg   | * | 0xA | {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00}

  # !BSF NAME: {USB 3.0 Controls the input offset} TYPE:{EditNum, HEX, (0x00,0xFFFFFFFFFFFFFFFFFFFF)}
  # !BSF HELP: {Controls the input offset, works in conjunction with LFPS sensitivity level, <b>Default = 3h</b>.}
  gPlatformFspPkgTokenSpaceGuid.PchUsb3HsioOlfpsCfgPullUpDwnRes   | * | 0xA | {0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03}

  # !BSF NAME: {USB 3.0 LFPS sensitivity level for N} TYPE:{EditNum, HEX, (0x00,0xFFFFFFFFFFFFFFFFFFFF)}
  # !BSF HELP: {Sensitivity level for LFPS circuitry, <b>Default = 0h</b>.}
  gPlatformFspPkgTokenSpaceGuid.PchUsb3HsioFilterSelN   | * | 0xA | {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00}

  # !BSF NAME: {USB 3.0 LFPS sensitivity level for P} TYPE:{EditNum, HEX, (0x00,0xFFFFFFFFFFFFFFFFFFFF)}
  # !BSF HELP: {Sensitivity level for LFPS circuitry, <b>Default = 0h</b>.}
  gPlatformFspPkgTokenSpaceGuid.PchUsb3HsioFilterSelP   | * | 0xA | {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00}

  # !BSF NAME:{Enable LAN} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Enable/disable LAN controller.}
  gPlatformFspPkgTokenSpaceGuid.PchLanEnable                | * | 0x01 | 0x01

  # !BSF NAME:{Enable PCH TSN} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Enable/disable TSN on the PCH.}
  gPlatformFspPkgTokenSpaceGuid.PchTsnEnable                | * | 0x01 | 0x01
  # !BSF NAME:{TSN Link Speed} TYPE:{Combo} OPTION:{0: 24Mhz 2.5Gbps, 1: 24Mhz 1Gbps, 2: 38.4Mhz 2.5Gbps, 3: 38.4Mhz 1Gbps}
  # !BSF HELP:{Set TSN Link Speed.}
  gPlatformFspPkgTokenSpaceGuid.PchTsnLinkSpeed         | * | 0x01 | 0x02
  # !BSF NAME:{PCH TSN MAC Address High Bits} TYPE:{EditNum, HEX, (0x0,0xFFFFFFFF)}
  # !BSF HELP:{Set TSN MAC Address High.}
  gPlatformFspPkgTokenSpaceGuid.PchTsnMacAddressHigh        | * | 0x04 | 0x00000000
  # !BSF NAME:{PCH TSN MAC Address Low Bits} TYPE:{EditNum, HEX, (0x0,0xFFFFFFFF)}
  # !BSF HELP:{Set TSN MAC Address Low.}
  gPlatformFspPkgTokenSpaceGuid.PchTsnMacAddressLow         | * | 0x04 | 0x00000000

  # !BSF NAME:{PCIe PTM enable/disable} TYPE:{EditNum, HEX, (0x00,0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF)}
  # !BSF HELP:{Enable/disable Precision Time Measurement for PCIE Root Ports.}
  gPlatformFspPkgTokenSpaceGuid.PciePtm                    | * | 0x1C | { 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01 }

  # !BSF NAME:{PCIe DPC enable/disable} TYPE:{EditNum, HEX, (0x00,0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF)}
  # !BSF HELP:{Enable/disable Downstream Port Containment for PCIE Root Ports.}
  gPlatformFspPkgTokenSpaceGuid.PcieDpc                    | * | 0x1C | { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 }

  # !BSF NAME:{PCIe DPC extensions enable/disable} TYPE:{EditNum, HEX, (0x00,0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF)}
  # !BSF HELP:{Enable/disable Downstream Port Containment Extensions for PCIE Root Ports.}
  gPlatformFspPkgTokenSpaceGuid.PcieEdpc                   | * | 0x1C | { 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01 }

  # !BSF NAME:{USB PDO Programming} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Enable/disable PDO programming for USB in PEI phase. Disabling will allow for programming during later phase. 1: enable, 0: disable}
  gPlatformFspPkgTokenSpaceGuid.UsbPdoProgramming                   | * | 0x01 | 0x01

  # !BSF NAME:{Power button debounce configuration} TYPE:{EditNum, HEX, (0x00,0x009C4000)}
  # !BSF HELP:{Debounce time for PWRBTN in microseconds. For values not supported by HW, they will be rounded down to closest supported on. 0: disable, 250-1024000us: supported range}
  gPlatformFspPkgTokenSpaceGuid.PmcPowerButtonDebounce              | * | 0x04 | 0x00

  # !BSF NAME:{PCH eSPI Host and Device BME enabled} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{PCH eSPI Host and Device BME enabled}
  gPlatformFspPkgTokenSpaceGuid.PchEspiBmeMasterSlaveEnabled        | * | 0x01 | 0x01

  # !BSF NAME:{PCH eSPI Link Configuration Lock (SBLCL)} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Enable/Disable lock of communication through SET_CONFIG/GET_CONFIG to eSPI slaves addresseses from range 0x0 - 0x7FF}
  gPlatformFspPkgTokenSpaceGuid.PchEspiLockLinkConfiguration        | * | 0x01 | 0x01

  # !BSF NAME:{Mask to enable the usage of external V1p05 VR rail in specific S0ix or Sx states} TYPE:{EditNum, HEX, (0x00,0x3F)}
  # !BSF HELP:{Enable External V1P05 Rail in: BIT0:S0i1/S0i2, BIT1:S0i3, BIT2:S3, BIT3:S4, BIT4:S5, BIT5:S0}
  gPlatformFspPkgTokenSpaceGuid.PchFivrExtV1p05RailEnabledStates    | * | 0x1 | 0x0

  # !BSF NAME:{Mask to enable the platform configuration of external V1p05 VR rail} TYPE:{EditNum, HEX, (0x00,0x0F)}
  # !BSF HELP:{External V1P05 Rail Supported Configuration}
  gPlatformFspPkgTokenSpaceGuid.PchFivrExtV1p05RailSupportedVoltageStates  | * | 0x1 | 0x2

  # !BSF NAME:{External V1P05 Voltage Value that will be used in S0i2/S0i3 states} TYPE:{EditNum, HEX, (0x0,0x07FF)}
  # !BSF HELP:{Value is given in 2.5mV increments (0=0mV, 1=2.5mV, 2=5mV...)}
  gPlatformFspPkgTokenSpaceGuid.PchFivrExtV1p05RailVoltage          | * | 0x2 | 0x01A4

  # !BSF NAME:{External V1P05 Icc Max Value} TYPE:{EditNum, HEX, (0x0,0xC8)}
  # !BSF HELP:{Granularity of this setting is 1mA and maximal possible value is 200mA}
  gPlatformFspPkgTokenSpaceGuid.PchFivrExtV1p05RailIccMax           | * | 0x1 | 0x64

  # !BSF NAME:{Mask to enable the usage of external Vnn VR rail in specific S0ix or Sx states} TYPE:{EditNum, HEX, (0x00,0x1F)}
  # !BSF HELP:{Enable External Vnn Rail in: BIT0:S0i1/S0i2, BIT1:S0i3, BIT2:S3, BIT3:S4, BIT5:S5}
  gPlatformFspPkgTokenSpaceGuid.PchFivrExtVnnRailEnabledStates      | * | 0x1 | 0x0

  # !BSF NAME:{Mask to enable the platform configuration of external Vnn VR rail} TYPE:{EditNum, HEX, (0x00,0x0F)}
  # !BSF HELP:{External Vnn Rail Supported Configuration}
  gPlatformFspPkgTokenSpaceGuid.PchFivrExtVnnRailSupportedVoltageStates  | * | 0x1 | 0x2

  # !BSF NAME:{External Vnn Voltage Value that will be used in S0ix/Sx states} TYPE:{EditNum, HEX, (0x0,0x07FF)}
  # !BSF HELP:{Value is given in 2.5mV increments (0=0mV, 1=2.5mV, 2=5mV...), Default is set to 420}}
  gPlatformFspPkgTokenSpaceGuid.PchFivrExtVnnRailVoltage            | * | 0x2 | 0x01A4

  # !BSF NAME:{External Vnn Icc Max Value that will be used in S0ix/Sx states} TYPE:{EditNum, HEX, (0x0,0xC8)}
  # !BSF HELP:{Granularity of this setting is 1mA and maximal possible value is 200mA}
  gPlatformFspPkgTokenSpaceGuid.PchFivrExtVnnRailIccMax             | * | 0x1 | 0xC8

  # !BSF NAME:{Mask to enable the usage of external Vnn VR rail in Sx states} TYPE:{EditNum, HEX, (0x00,0x3F)}
  # !BSF HELP:{Use only if Ext Vnn Rail config is different in Sx. Enable External Vnn Rail in Sx: BIT0-1:Reserved, BIT2:S3, BIT3:S4, BIT4:S5, BIT5:S0}
  gPlatformFspPkgTokenSpaceGuid.PchFivrExtVnnRailSxEnabledStates    | * | 0x1 | 0x0

  # !BSF NAME:{External Vnn Voltage Value that will be used in Sx states} TYPE:{EditNum, HEX, (0x0,0x07FF)}
  # !BSF HELP:{Use only if Ext Vnn Rail config is different in Sx. Value is given in 2.5mV increments (0=0mV, 1=2.5mV, 2=5mV...)}
  gPlatformFspPkgTokenSpaceGuid.PchFivrExtVnnRailSxVoltage          | * | 0x2 | 0x01A4

  # !BSF NAME:{External Vnn Icc Max Value that will be used in Sx states} TYPE:{EditNum, HEX, (0x0,0xC8)}
  # !BSF HELP:{Use only if Ext Vnn Rail config is different in Sx. Granularity of this setting is 1mA and maximal possible value is 200mA}
  gPlatformFspPkgTokenSpaceGuid.PchFivrExtVnnRailSxIccMax           | * | 0x1 | 0xC8

  # !BSF NAME:{Transition time in microseconds from Low Current Mode Voltage to High Current Mode Voltage} TYPE:{EditNum, HEX, (0x0,0xFF)}
  # !BSF HELP:{This field has 1us resolution. When value is 0 PCH will not transition VCCIN_AUX to low current mode voltage.}
  gPlatformFspPkgTokenSpaceGuid.PchFivrVccinAuxLowToHighCurModeVolTranTime  | * | 0x1 | 0x0C

  # !BSF NAME:{Transition time in microseconds from Retention Mode Voltage to High Current Mode Voltage} TYPE:{EditNum, HEX, (0x0,0xFF)}
  # !BSF HELP:{This field has 1us resolution. When value is 0 PCH will not transition VCCIN_AUX to retention mode voltage.}
  gPlatformFspPkgTokenSpaceGuid.PchFivrVccinAuxRetToHighCurModeVolTranTime  | * | 0x1 | 0x036

  # !BSF NAME:{Transition time in microseconds from Retention Mode Voltage to Low Current Mode Voltage} TYPE:{EditNum, HEX, (0x0,0xFF)}
  # !BSF HELP:{This field has 1us resolution. When value is 0 PCH will not transition VCCIN_AUX to retention mode voltage.}
  gPlatformFspPkgTokenSpaceGuid.PchFivrVccinAuxRetToLowCurModeVolTranTime   | * | 0x1 | 0x2B

  # !BSF NAME:{Transition time in microseconds from Off (0V) to High Current Mode Voltage} TYPE:{EditNum, HEX, (0x0,0x7FF)}
  # !BSF HELP:{This field has 1us resolution. When value is 0 Transition to 0V is disabled.}
  gPlatformFspPkgTokenSpaceGuid.PchFivrVccinAuxOffToHighCurModeVolTranTime  | * | 0x2 | 0x0096

  # !BSF NAME:{PMC Debug Message Enable} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{When Enabled, PMC HW will send debug messages to trace hub; When Disabled, PMC HW will never send debug meesages to trace hub. Noted: When Enabled, may not enter S0ix}
  gPlatformFspPkgTokenSpaceGuid.PmcDbgMsgEn                                 | * | 0x01 | 0x00

  # !BSF PAGE:{PCH2}
  # !BSF NAME:{Pointer of ChipsetInit Binary} TYPE:{EditNum, HEX, (0x00,0xFFFFFFFF)}
  # !BSF HELP:{ChipsetInit Binary Pointer.}
  gPlatformFspPkgTokenSpaceGuid.ChipsetInitBinPtr           | * | 0x04 | 0x00000000

  # !BSF NAME:{Length of ChipsetInit Binary} TYPE:{EditNum, HEX, (0x00,0xFFFFFFFF)}
  # !BSF HELP:{ChipsetInit Binary Length.}
  gPlatformFspPkgTokenSpaceGuid.ChipsetInitBinLen           | * | 0x04 | 0x00000000

  # !BSF NAME:{FIVR Dynamic Power Management} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Enable/Disable FIVR Dynamic Power Management.}
  gPlatformFspPkgTokenSpaceGuid.PchFivrDynPm                            | * | 0x01 | 0x01

  # !BSF NAME:{External V1P05 Icc Max Value} TYPE:{EditNum, HEX, (0x0,0x1F4)}
  # !BSF HELP:{Granularity of this setting is 1mA and maximal possible value is 500mA}
  gPlatformFspPkgTokenSpaceGuid.PchFivrExtV1p05RailIccMaximum           | * | 0x2 | 0x1F4

  # !BSF NAME:{External Vnn Icc Max Value that will be used in S0ix/Sx states} TYPE:{EditNum, HEX, (0x0,0x1F4)}
  # !BSF HELP:{Granularity of this setting is 1mA and maximal possible value is 500mA}
  gPlatformFspPkgTokenSpaceGuid.PchFivrExtVnnRailIccMaximum             | * | 0x2 | 0x1F4

  # !BSF NAME:{External Vnn Icc Max Value that will be used in Sx states} TYPE:{EditNum, HEX, (0x0,0x1F4)}
  # !BSF HELP:{Use only if Ext Vnn Rail config is different in Sx. Granularity of this setting is 1mA and maximal possible value is 500mA}
  gPlatformFspPkgTokenSpaceGuid.PchFivrExtVnnRailSxIccMaximum           | * | 0x2 | 0x1F4

  # !BSF NAME:{Extented BIOS Direct Read Decode enable} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Enable/Disable access to bigger than 16MB BIOS Region through Direct Memory Reads. 0: disabled (default), 1: enabled}
  gPlatformFspPkgTokenSpaceGuid.PchSpiExtendedBiosDecodeRangeEnable | * | 0x01 | 0x00

  # !BSF NAME:{Extended BIOS Direct Read Decode Range base} TYPE:{EditNum, HEX, (0x0,0xFFFFFFFF)}
  # !BSF HELP:{Bits of 31:16 of a memory address that'll be a base for Extended BIOS Direct Read Decode.}
  gPlatformFspPkgTokenSpaceGuid.PchSpiExtendedBiosDecodeRangeBase | * | 0x04 | 0xF8000000

  # !BSF NAME:{Extended BIOS Direct Read Decode Range limit} TYPE:{EditNum, HEX, (0x0,0xFFFFFFFF)}
  # !BSF HELP:{Bits of 31:16 of a memory address that'll be a limit for Extended BIOS Direct Read Decode.}
  gPlatformFspPkgTokenSpaceGuid.PchSpiExtendedBiosDecodeRangeLimit | * | 0x04 | 0xF9FFFFFF

  # !BSF NAME:{USB Audio Offload enable} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Enable/Disable USB Audio Offload capabilites. 0: disabled, 1: enabled (default)}
  gPlatformFspPkgTokenSpaceGuid.PchXhciUaolEnable | * | 0x01 | 0x01

  # !BSF NAME:{Pointer of SYNPS PHY Binary} TYPE:{EditNum, HEX, (0x00,0xFFFFFFFF)}
  # !BSF HELP:{ChipsetInit Binary Pointer.}
  gPlatformFspPkgTokenSpaceGuid.SynpsPhyBinPtr               | * | 0x04 | 0x00000000

  # !BSF NAME:{Length of SYNPS PHY Binary} TYPE:{EditNum, HEX, (0x00,0xFFFFFFFF)}
  # !BSF HELP:{ChipsetInit Binary Length.}
  gPlatformFspPkgTokenSpaceGuid.SynpsPhyBinLen               | * | 0x04 | 0x00000000

  # !BSF NAME:{CNVi Configuration} TYPE:{Combo} OPTION:{0:Disable, 1:Auto}
  # !BSF HELP:{This option allows for automatic detection of Connectivity Solution. [Auto Detection] assumes that CNVi will be enabled when available, [Disable] allows for disabling CNVi.}
  gPlatformFspPkgTokenSpaceGuid.CnviMode                    | * | 0x01 | 0x01

  # !BSF NAME:{CNVi Wi-Fi Core} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Enable/Disable CNVi Wi-Fi Core, Default is ENABLE. 0: DISABLE, 1: ENABLE}
  gPlatformFspPkgTokenSpaceGuid.CnviWifiCore                | * | 0x01 | 0x01

  # !BSF NAME:{CNVi BT Core} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Enable/Disable CNVi BT Core, Default is ENABLE. 0: DISABLE, 1: ENABLE}
  gPlatformFspPkgTokenSpaceGuid.CnviBtCore                  | * | 0x01 | 0x01

  # !BSF NAME:{CNVi BT Audio Offload} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Enable/Disable BT Audio Offload, Default is DISABLE. 0: DISABLE, 1: ENABLE}
  gPlatformFspPkgTokenSpaceGuid.CnviBtAudioOffload          | * | 0x01 | 0x00

  # !BSF NAME:{CNVi RF_RESET pin muxing} TYPE:{EditNum, HEX, (0,0xFFFFFFFF)}
  # !BSF HELP:{Select CNVi RF_RESET# pin depending on board routing. ADP-P/M: GPP_A8 = 0x2942E408(default) or GPP_F4 = 0x194CE404. ADP-S: 0. Refer to GPIO_*_MUXING_CNVI_RF_RESET_* in GpioPins*.h.}
  gPlatformFspPkgTokenSpaceGuid.CnviRfResetPinMux           | * | 0x04 | 0x0

  # !BSF NAME:{CNVi CLKREQ pin muxing} TYPE:{EditNum, HEX, (0,0xFFFFFFFF)}
  # !BSF HELP:{Select CNVi CLKREQ pin depending on board routing. ADP-P/M: GPP_A9 = 0x3942E609(default) or GPP_F5 = 0x394CE605. ADP-S: 0. Refer to GPIO_*_MUXING_CNVI_CRF_XTAL_CLKREQ_* in GpioPins*.h.}
  gPlatformFspPkgTokenSpaceGuid.CnviClkreqPinMux            | * | 0x04 | 0x0

  # !BSF NAME:{Enable Host C10 reporting through eSPI} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Enable/disable Host C10 reporting to Device via eSPI Virtual Wire.}
  gPlatformFspPkgTokenSpaceGuid.PchEspiHostC10ReportEnable  | * | 0x01 | 0x0

  # !BSF NAME:{PCH USB2 PHY Power Gating enable} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{1: Will enable USB2 PHY SUS Well Power Gating, 0: Will not enable PG of USB2 PHY Sus Well PG}
  gPlatformFspPkgTokenSpaceGuid.PmcUsb2PhySusPgEnable       | * | 0x01 | 0x01

  # !BSF NAME:{PCH USB OverCurrent mapping enable} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{1: Will program USB OC pin mapping in xHCI controller memory, 0: Will clear OC pin mapping allow for NOA usage of OC pins}
  gPlatformFspPkgTokenSpaceGuid.PchUsbOverCurrentEnable     | * | 0x01 | 0x01

  # !BSF NAME:{Espi Lgmr Memory Range decode } TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{This option enables or disables espi lgmr }
  gPlatformFspPkgTokenSpaceGuid.PchEspiLgmrEnable           | * | 0x01 | 0x01

  # !BSF NAME:{External V1P05 Control Ramp Timer value} TYPE:{EditNum, HEX, (0x0,0xFF)}
  # !BSF HELP:{Hold off time to be used when changing the v1p05_ctrl for external bypass value in us}
  gPlatformFspPkgTokenSpaceGuid.PchFivrExtV1p05RailCtrlRampTmr           | * | 0x1 | 0x1

  # !BSF NAME:{External VNN Control Ramp Timer value} TYPE:{EditNum, HEX, (0x0,0xFF)}
  # !BSF HELP:{Hold off time to be used when changing the vnn_ctrl for external bypass value in us}
  gPlatformFspPkgTokenSpaceGuid.PchFivrExtVnnRailCtrlRampTmr           | * | 0x1 | 0x1

  # !BSF NAME:{Set SATA DEVSLP GPIO Reset Config} TYPE:{EditNum, HEX, (0x00,0xFFFFFFFFFFFFFFFF)}
  # !BSF HELP:{Set SATA DEVSLP GPIO Reset Config per port. 0x00 - GpioResetDefault, 0x01 - GpioResumeReset, 0x03 - GpioHostDeepReset, 0x05 - GpioPlatformReset, 0x07 - GpioDswReset. One byte for each port, byte0 for port0, byte1 for port1, and so on.}
  gPlatformFspPkgTokenSpaceGuid.SataPortsDevSlpResetConfig  | * | 0x08 | { 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01 }

  #
  # PCH
  #

  #
  # !BSF NAME:{PCHHOT# pin} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Enable PCHHOT# pin assertion when temperature is higher than PchHotLevel. 0: disable, 1: enable}
  gPlatformFspPkgTokenSpaceGuid.PchHotEnable                | * | 0x01 | 0x00

  #
  # !BSF NAME:{SATA LED} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{SATA LED indicating SATA controller activity. 0: disable, 1: enable}
  gPlatformFspPkgTokenSpaceGuid.SataLedEnable               | * | 0x01 | 0x00

  #
  # !BSF NAME:{VRAlert# Pin} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{When VRAlert# feature pin is enabled and its state is '0', the PMC requests throttling to a T3 Tstate to the PCH throttling unit.. 0: disable, 1: enable}
  gPlatformFspPkgTokenSpaceGuid.PchPmVrAlert                | * | 0x01 | 0x00

  #
  # AMT
  #
  # !BSF PAGE:{ME2}
  # !BSF NAME:{AMT Switch} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Enable/Disable. 0: Disable, 1: enable, Enable or disable AMT functionality.}
  gPlatformFspPkgTokenSpaceGuid.AmtEnabled                  | * | 0x01 | 0x01

  # !BSF NAME:{WatchDog Timer Switch} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Enable/Disable. 0: Disable, 1: enable, Enable or disable WatchDog timer. Setting is invalid if AmtEnabled is 0.}
  gPlatformFspPkgTokenSpaceGuid.WatchDogEnabled             | * | 0x01 | 0x0

  # !BSF NAME:{PET Progress} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Enable/Disable. 0: Disable, 1: enable, Enable/Disable PET Events Progress to receive PET Events. Setting is invalid if AmtEnabled is 0.}
  gPlatformFspPkgTokenSpaceGuid.FwProgress                  | * | 0x01 | 0x0

  # !BSF NAME:{SOL Switch} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Enable/Disable. 0: Disable, 1: enable, Serial Over Lan enable/disable state by Mebx. Setting is invalid if AmtEnabled is 0.}
  gPlatformFspPkgTokenSpaceGuid.AmtSolEnabled               | * | 0x01 | 0x0

  # !BSF NAME:{OS Timer} TYPE:{EditNum, HEX, (0x00, 0xFFFF)}
  # !BSF HELP:{16 bits Value, Set OS watchdog timer. Setting is invalid if AmtEnabled is 0.}
  gPlatformFspPkgTokenSpaceGuid.WatchDogTimerOs             | * | 0x02 | 0x0

  # !BSF NAME:{BIOS Timer} TYPE:{EditNum, HEX, (0x00, 0xFFFF)}
  # !BSF HELP:{16 bits Value, Set BIOS watchdog timer. Setting is invalid if AmtEnabled is 0.}
  gPlatformFspPkgTokenSpaceGuid.WatchDogTimerBios           | * | 0x02 | 0x0

  # !BSF NAME:{Force MEBX execution} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Enable/Disable. 0: Disable, 1: enable, Force MEBX execution.}
  gPlatformFspPkgTokenSpaceGuid.ForcMebxSyncUp              | * | 0x01 | 0x0

  # !BSF PAGE:{PCH2}
  # !BSF NAME:{PCH PCIe root port connection type} TYPE:{EditNum, HEX, (0x00,0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF)}
  # !BSF HELP:{0: built-in device, 1:slot}
  gPlatformFspPkgTokenSpaceGuid.PcieRpSlotImplemented       | * | 0x1C | { 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01 }

  # !BSF NAME:{PCIE RP Access Control Services Extended Capability} TYPE:{EditNum, HEX, (0x00,0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF)}
  # !BSF HELP:{Enable/Disable PCIE RP Access Control Services Extended Capability}
  gPlatformFspPkgTokenSpaceGuid.PcieRpAcsEnabled            | * | 0x1C | { 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01 }

  # !BSF NAME:{PCIE RP Clock Power Management} TYPE:{EditNum, HEX, (0x00,0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF)}
  # !BSF HELP:{Enable/Disable PCIE RP Clock Power Management, even if disabled, CLKREQ# signal can still be controlled by L1 PM substates mechanism}
  gPlatformFspPkgTokenSpaceGuid.PcieRpEnableCpm             | * | 0x1C | { 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01 }

  # !BSF NAME:{PCIE RP Detect Timeout Ms} TYPE:{EditNum, HEX, (0x00,0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF)}
  # !HDR STRUCT:{UINT16}
  # !BSF HELP:{The number of milliseconds within 0~65535 in reference code will wait for link to exit Detect state for enabled ports before assuming there is no device and potentially disabling the port.}
  gPlatformFspPkgTokenSpaceGuid.PcieRpDetectTimeoutMs       | * | 0x38 | { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 }

  # !BSF NAME:{ModPHY SUS Power Domain Dynamic Gating} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Enable/Disable ModPHY SUS Power Domain Dynamic Gating. Setting not supported on PCH-H. 0: disable, 1: enable}
  gPlatformFspPkgTokenSpaceGuid.PmcModPhySusPgEnable        | * | 0x01 | 0x01

  # !BSF NAME:{V1p05-PHY supply external FET control} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Enable/Disable control using EXT_PWR_GATE# pin of external FET to power gate v1p05-PHY supply. 0: disable, 1: enable}
  gPlatformFspPkgTokenSpaceGuid.PmcV1p05PhyExtFetControlEn  | * | 0x01 | 0x00

  # !BSF NAME:{V1p05-IS supply external FET control} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Enable/Disable control using EXT_PWR_GATE2# pin of external FET to power gate v1p05-IS supply. 0: disable, 1: enable}
  gPlatformFspPkgTokenSpaceGuid.PmcV1p05IsExtFetControlEn   | * | 0x01 | 0x00

  #
  # SA Post-Mem Production Block Start
  #

  # !BSF PAGE:{SA2}
  # !BSF NAME:{Enable/Disable PavpEnable} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Enable(Default): Enable PavpEnable, Disable: Disable PavpEnable}
  gPlatformFspPkgTokenSpaceGuid.PavpEnable                  | * | 0x01 | 0x1

  # !BSF NAME:{CdClock Frequency selection} TYPE:{Combo}
  # !BSF OPTION:{0xFF: Auto (Max based on reference clock frequency), 0: 192, 1: 307.2, 2: 312 Mhz, 3: 324Mhz, 4: 326.4 Mhz, 5: 552 Mhz, 6: 556.8 Mhz, 7: 648 Mhz, 8: 652.8 Mhz}
  # !BSF HELP:{0 (Default) Auto (Max based on reference clock frequency),  0: 192, 1: 307.2, 2: 312 Mhz, 3: 324Mhz, 4: 326.4 Mhz, 5: 552 Mhz, 6: 556.8 Mhz, 7: 648 Mhz, 8: 652.8 Mhz}
  gPlatformFspPkgTokenSpaceGuid.CdClock                     | * | 0x1 | 0xFF

  # !BSF NAME:{Enable/Disable PeiGraphicsPeimInit} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{<b>Enable(Default):</b> FSP will initialize the framebuffer and provide it via EFI_PEI_GRAPHICS_INFO_HOB. Disable: FSP will NOT initialize the framebuffer.}
  gPlatformFspPkgTokenSpaceGuid.PeiGraphicsPeimInit         | * | 0x01 | 0x1

  # !BSF PAGE:{TCSS2}
  # !BSF NAME:{Enable D3 Hot in TCSS } TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{This policy will enable/disable D3 hot support in IOM}
  gPlatformFspPkgTokenSpaceGuid.D3HotEnable                 | * | 0x01 | 0x00

  # !BSF PAGE:{SA2}
  # !BSF NAME:{Enable or disable GNA device} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{0=Disable, 1(Default)=Enable}
  gPlatformFspPkgTokenSpaceGuid.GnaEnable                   | * | 0x1 | 0x1

  # !BSF PAGE:{TCSS2}
  # !BSF NAME:{TypeC port GPIO setting} TYPE:{EditNum, HEX, (0, 0xFFFFFFFF)}
  # !HDR STRUCT:{UINT32}
  # !BSF HELP:{GPIO Ping number for Type C Aux Oritation setting, use the GpioPad that is defined in GpioPinsXXXH.h and GpioPinsXXXLp.h as argument.(XXX is platform name, Ex: Adl = AlderLake)}
  gPlatformFspPkgTokenSpaceGuid.IomTypeCPortPadCfg          | * | 0x20 | {0x00000000,0x00000000, 0x06040000, 0x06040011, 0x00000000, 0x00000000, 0x00000000, 0x00000000}

  # !BSF NAME:{CPU USB3 Port Over Current Pin} TYPE:{EditNum, HEX, (0x00,0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF)}
  # !BSF HELP:{Describe the specific over current pin number of USBC Port N.}
  gPlatformFspPkgTokenSpaceGuid.CpuUsb3OverCurrentPin       | * | 0x8 | { 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF }

  # !BSF NAME:{Enable D3 Cold in TCSS } TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{This policy will enable/disable D3 cold support in IOM}
  gPlatformFspPkgTokenSpaceGuid.D3ColdEnable                | * | 0x01 | 0x01

  # !BSF NAME:{Enable/Disable PCIe tunneling for USB4} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Enable/Disable PCIe tunneling for USB4, default is enable}
  gPlatformFspPkgTokenSpaceGuid.ITbtPcieTunnelingForUsb4    | * | 0x01 | 0x01

  # !BSF PAGE:{SA2}
  # !BSF NAME:{Enable/Disable SkipFspGop} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Enable: Skip FSP provided GOP driver, Disable(Default): Use FSP provided GOP driver}
  gPlatformFspPkgTokenSpaceGuid.SkipFspGop                  | * | 0x01 | 0x0

  # !BSF PAGE:{TCSS2}
  # !BSF NAME:{TC State in TCSS } TYPE:{EditNum, HEX, (0x00,0xFF)}
  # !BSF HELP:{This TC C-State Limit in IOM}
  gPlatformFspPkgTokenSpaceGuid.TcCstateLimit               | * | 0x01 | 0x0A

  # !BSF NAME:{Intel Graphics VBT (Video BIOS Table) Size}
  # !BSF TYPE:{EditNum, HEX, (0x0,0xFFFFFFFF)}
  # !BSF HELP:{Size of Internal Graphics VBT Image}
  gPlatformFspPkgTokenSpaceGuid.VbtSize                     | * | 0x04 | 0x00000000

  # !BSF NAME:{Platform LID Status for LFP Displays.} TYPE:{Combo}
  # !BSF OPTION:{0: LidClosed, 1: LidOpen}
  # !BSF HELP:{LFP Display Lid Status (LID_STATUS enum): 0 (Default): LidClosed, 1: LidOpen.}
  gPlatformFspPkgTokenSpaceGuid.LidStatus                   | * | 0x01 | 0x0

  # !BSF PAGE:{TCSS2}
  # !BSF NAME:{Set Iom stay in TC cold seconds in TCSS } TYPE:{EditNum, HEX, (0x00,0xFF)}
  # !BSF HELP:{Set Iom stay in TC cold seconds in IOM}
  gPlatformFspPkgTokenSpaceGuid.IomStayInTCColdSeconds     | * | 0x01 | 0x32

  # !BSF PAGE:{TCSS2}
  # !BSF NAME:{Set Iom before entering TC cold seconds in TCSS } TYPE:{EditNum, HEX, (0x00,0xFF)}
  # !BSF HELP:{Set Iom before entering TC cold seconds in IOM}
  gPlatformFspPkgTokenSpaceGuid.IomBeforeEnteringTCColdSeconds         | * | 0x01 | 0x0A

  # !BSF PAGE:{SA2}
  # !BSF NAME:{SaPostMemRsvd} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Reserved for PCH Post-Mem}
  gPlatformFspPkgTokenSpaceGuid.SaPostMemRsvd               | * | 0x5 | {0x00}

  # !BSF PAGE:{PCH2}
  # !BSF NAME:{PCH xHCI enable HS Interrupt IN Alarm} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{PCH xHCI enable HS Interrupt IN Alarm. 0: disabled (default), 1: enabled}
  gPlatformFspPkgTokenSpaceGuid.PchXhciHsiiEnable                  | * | 0x1 | 0x1

  #
  #VMD post-mem config
  #
  # !BSF PAGE:{TCSS2}
  # !BSF NAME:{Enable VMD controller} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Enable/disable to VMD controller.0: Disable; 1: Enable(Default)}
  gPlatformFspPkgTokenSpaceGuid.VmdEnable                 | * | 0x01 | 0x01

  # !BSF NAME:{Map port under VMD} TYPE:{Combo} OPTION:{$EN_DIS}
  # !HDR STRUCT:{UINT8}
  # !BSF HELP:{Map/UnMap port under VMD}
  gPlatformFspPkgTokenSpaceGuid.VmdPort         | * | 0x1F | {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00}

  # !BSF NAME:{VMD Port Device} TYPE:{EditNum, DEC, (0,31)}
  # !HDR STRUCT:{UINT8}
  # !BSF HELP:{VMD Root port device number.}
  gPlatformFspPkgTokenSpaceGuid.VmdPortDev      | * | 0x1F | {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00}

  # !BSF NAME:{VMD Port Func} TYPE:{EditNum, DEC, (0,7)}
  # !HDR STRUCT:{UINT8}
  # !BSF HELP:{VMD Root port function number.}
  gPlatformFspPkgTokenSpaceGuid.VmdPortFunc     | * | 0x1F | {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00}

  # !BSF NAME:{VMD Config Bar size} TYPE:{EditNum, DEC, (20,28)}
  # !BSF HELP:{Set The VMD Config Bar Size.}
  gPlatformFspPkgTokenSpaceGuid.VmdCfgBarSize             | * | 0x01 | 0x00

  # !BSF NAME:{VMD Config Bar Attributes} TYPE:{Combo}
  # !BSF OPTION:{0: VMD_32BIT_NONPREFETCH, 1: VMD_64BIT_NONPREFETCH, 2: VMD_64BIT_PREFETCH}
  # !BSF HELP:{0: VMD_32BIT_NONPREFETCH, 1: VMD_64BIT_NONPREFETCH(Default), 2: VMD_64BIT_PREFETCH}
  gPlatformFspPkgTokenSpaceGuid.VmdCfgBarAttr             | * | 0x01 | 0x00

  # !BSF NAME:{VMD Mem Bar1 size} TYPE:{EditNum, DEC, (12,47)}
  # !BSF HELP:{Set The VMD Mem Bar1 Size.}
  gPlatformFspPkgTokenSpaceGuid.VmdMemBarSize1            | * | 0x01 | 0x00

  # !BSF NAME:{VMD Mem Bar1 Attributes} TYPE:{Combo}
  # !BSF OPTION:{0: VMD_32BIT_NONPREFETCH, 1: VMD_64BIT_NONPREFETCH, 2: VMD_64BIT_PREFETCH}
  # !BSF HELP:{0: VMD_32BIT_NONPREFETCH(Default), 1: VMD_64BIT_NONPREFETCH, 2: VMD_64BIT_PREFETCH}
  gPlatformFspPkgTokenSpaceGuid.VmdMemBar1Attr            | * | 0x01 | 0x00

  # !BSF NAME:{VMD Mem Bar2 size} TYPE:{EditNum, DEC, (12,47)}
  # !BSF HELP:{Set The VMD Mem Bar2 Size.}
  gPlatformFspPkgTokenSpaceGuid.VmdMemBarSize2            | * | 0x01 | 0x00

  # !BSF NAME:{VMD Mem Bar2 Attributes} TYPE:{Combo}
  # !BSF OPTION:{0: VMD_32BIT_NONPREFETCH, 1: VMD_64BIT_NONPREFETCH, 2: VMD_64BIT_PREFETCH}
  # !BSF HELP:{0: VMD_32BIT_NONPREFETCH, 1: VMD_64BIT_NONPREFETCH(Default), 2: VMD_64BIT_PREFETCH}
  gPlatformFspPkgTokenSpaceGuid.VmdMemBar2Attr            | * | 0x01 | 0x00

  # !BSF NAME:{VMD Variable} TYPE:{EditNum, HEX, (0x00,0xFFFFFFFF)}
  # !BSF HELP:{VMD Variable Pointer.}
  gPlatformFspPkgTokenSpaceGuid.VmdVariablePtr    | * | 0x4 | 0x00

  # !BSF NAME:{Temporary CfgBar address for VMD} TYPE:{EditNum, HEX, (0x00, 0xFFFFFFFF)}
  # !BSF HELP:{The reference code will use this as Temporary Cfg Bar address space to access Cfg space of mapped devices. Platform should provide conflict free Temporary MMIO Range to (CfgBarAddr + 32MB MMIO).
  gPlatformFspPkgTokenSpaceGuid.VmdCfgBarBase                    | * | 0x4 | 0xA0000000

  # !BSF NAME:{Temporary MemBar1 address for VMD} TYPE:{EditNum, HEX, (0x00, 0xFFFFFFFF)}
  # !BSF HELP:{The reference code will use this as Temporary MemBar1 address space to access MMIO space of mapped devices. Platform should provide conflict free Temporary MMIO Range to (MemBar1Addr + 32MB MMIO).
  gPlatformFspPkgTokenSpaceGuid.VmdMemBar1Base                    | * | 0x4 | 0xA2000000

  # !BSF NAME:{Temporary MemBar2 address for VMD} TYPE:{EditNum, HEX, (0x00, 0xFFFFFFFF)}
  # !BSF HELP:{The reference code will use this as Temporary MemBar2 address space to access MMIO space of mapped devices. Platform should provide conflict free Temporary MMIO Range to (MemBar2Addr + 1MB MMIO).
  gPlatformFspPkgTokenSpaceGuid.VmdMemBar2Base                    | * | 0x4 | 0xA4000000


  # !BSF PAGE:{TCSS2}
  # !BSF NAME:{TCSS CPU USB PDO Programming} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Enable/disable PDO programming for TCSS CPU USB in PEI phase. Disabling will allow for programming during later phase. 1: enable, 0: disable}
  gPlatformFspPkgTokenSpaceGuid.TcssCpuUsbPdoProgramming    | * | 0x01 | 0x01

  # !BSF NAME:{Enable/Disable PMC-PD Solution } TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{This policy will enable/disable PMC-PD Solution vs EC-TCPC Solution}
  gPlatformFspPkgTokenSpaceGuid.PmcPdEnable                 | * | 0x01 | 0x01

  # !BSF NAME:{TCSS Aux Orientation Override Enable} TYPE:{EditNum, HEX, (0x0,0x0FFF)}
  # !BSF HELP:{Bits 0, 2, ... 10 control override enables, bits 1, 3, ... 11 control overrides}
  gPlatformFspPkgTokenSpaceGuid.TcssAuxOri                  | * | 0x02 | 0x0000

  # !BSF NAME:{TCSS HSL Orientation Override Enable} TYPE:{EditNum, HEX, (0x0,0x0FFF)}
  # !BSF HELP:{Bits 0, 2, ... 10 control override enables, bits 1, 3, ... 11 control overrides}
  gPlatformFspPkgTokenSpaceGuid.TcssHslOri                  | * | 0x02 | 0x0000

  # !BSF NAME:{USB override in IOM } TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{This policy will enable/disable USB Connect override in IOM}
  gPlatformFspPkgTokenSpaceGuid.UsbOverride                 | * | 0x01 | 0x00

  # !BSF NAME:{ITBT Root Port Enable} TYPE:{EditNum, HEX, (0x00, 0xFFFFFFFFFF)}
  # !BSF OPTION:{0:Disable, 1:Enable}
  # !HDR STRUCT:{UINT8}
  # !BSF HELP:{ITBT Root Port Enable, 0:Disable, 1:Enable}
  gPlatformFspPkgTokenSpaceGuid.ITbtPcieRootPortEn          | * | 0x4 | {0, 0, 0, 0}

  # !BSF NAME:{TCSS USB Port Enable} TYPE:{EditNum, HEX, (0x0,0x000F)}
  # !BSF HELP:{Bits 0, 1, ... max Type C port control enables}
  gPlatformFspPkgTokenSpaceGuid.UsbTcPortEn                 | * | 0x01 | 0x00

  # !BSF NAME:{ITBTForcePowerOn Timeout value} TYPE:{EditNum, HEX, (0x00, 0xFFFF)}
  # !BSF HELP:{ITBTForcePowerOn value. Specified increment values in miliseconds. Range is 0-1000. 100 = 100 ms.}
  gPlatformFspPkgTokenSpaceGuid.ITbtForcePowerOnTimeoutInMs | * | 0x02 | 0x1F4

  # !BSF NAME:{ITbtConnectTopology Timeout value} TYPE:{EditNum, HEX, (0x00, 0xFFFF)}
  # !BSF HELP:{ITbtConnectTopologyTimeout value. Specified increment values in miliseconds. Range is 0-10000. 100 = 100 ms.}
  gPlatformFspPkgTokenSpaceGuid.ITbtConnectTopologyTimeoutInMs  | * | 0x02 | 0x1388

  # !BSF NAME:{VCCST request for IOM } TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{This policy will enable/disable VCCST and also decides if message would be replayed in S4/S5}
  gPlatformFspPkgTokenSpaceGuid.VccSt                       | * | 0x01 | 0x00

  # !BSF NAME:{ITBT DMA LTR}
  # !BSF TYPE:{EditNum, HEX, (0x0,0xFFFF)}
  # !HDR STRUCT:{UINT16}
  # !BSF HELP:{TCSS DMA1, DMA2 LTR value}
  gPlatformFspPkgTokenSpaceGuid.ITbtDmaLtr                     | * | 0x04 | {0x97FF, 0x97FF}

  # !BSF PAGE:{DBG}
  # !BSF NAME:{Enable/Disable CrashLog} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Enable(Default): Enable CPU CrashLog, Disable: Disable CPU CrashLog}
  gPlatformFspPkgTokenSpaceGuid.CpuCrashLogEnable           | * | 0x01 | 0x1

  #
  # Itbt PCIe RootPort Configuration
  #
  # !BSF PAGE:{TCSS2}
  # !BSF NAME:{Enable/Disable PTM} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{This policy will enable/disable Precision Time Measurement for TCSS PCIe Root Ports}
  gPlatformFspPkgTokenSpaceGuid.PtmEnabled                     | * | 0x04 | {0, 0, 0, 0}

  # !BSF NAME:{PCIE RP Ltr Enable} TYPE:{EditNum, HEX, (0x00,0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF)}
  # !BSF HELP:{Latency Tolerance Reporting Mechanism.}
  gPlatformFspPkgTokenSpaceGuid.SaPcieItbtRpLtrEnable                         | * | 0x4 | { 0x01, 0x01, 0x01, 0x01 }

  # !BSF NAME:{PCIE RP Snoop Latency Override Mode} TYPE:{EditNum, HEX, (0x00,0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF)}
  # !BSF HELP:{Latency Tolerance Reporting, Snoop Latency Override Mode.}
  gPlatformFspPkgTokenSpaceGuid.SaPcieItbtRpSnoopLatencyOverrideMode          | * | 0x4 | { 0x01, 0x01, 0x01, 0x01 }

  # !BSF NAME:{PCIE RP Snoop Latency Override Multiplier} TYPE:{EditNum, HEX, (0x00,0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF)}
  # !BSF HELP:{Latency Tolerance Reporting, Snoop Latency Override Multiplier.}
  gPlatformFspPkgTokenSpaceGuid.SaPcieItbtRpSnoopLatencyOverrideMultiplier    | * | 0x4 | { 0x02, 0x02, 0x02, 0x02 }

  # !BSF NAME:{PCIE RP Snoop Latency Override Value} TYPE:{EditNum, HEX, (0x00,0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF)}
  # !HDR STRUCT:{UINT16}
  # !BSF HELP:{Latency Tolerance Reporting, Snoop Latency Override Value.}
  gPlatformFspPkgTokenSpaceGuid.SaPcieItbtRpSnoopLatencyOverrideValue         | * | 0x8 | { 0x00C8, 0x00C8, 0x003C, 0x00C8 }

  # !BSF NAME:{PCIE RP Non Snoop Latency Override Mode} TYPE:{EditNum, HEX, (0x00,0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF)}
  # !BSF HELP:{Latency Tolerance Reporting, Non-Snoop Latency Override Mode.}
  gPlatformFspPkgTokenSpaceGuid.SaPcieItbtRpNonSnoopLatencyOverrideMode       | * | 0x4 | { 0x01, 0x01, 0x01, 0x01 }

  # !BSF NAME:{PCIE RP Non Snoop Latency Override Multiplier} TYPE:{EditNum, HEX, (0x00,0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF)}
  # !BSF HELP:{Latency Tolerance Reporting, Non-Snoop Latency Override Multiplier.}
  gPlatformFspPkgTokenSpaceGuid.SaPcieItbtRpNonSnoopLatencyOverrideMultiplier | * | 0x4 | { 0x02, 0x02, 0x02, 0x02 }

  # !BSF NAME:{PCIE RP Non Snoop Latency Override Value} TYPE:{EditNum, HEX, (0x00,0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF)}
  # !HDR STRUCT:{UINT16}
  # !BSF HELP:{Latency Tolerance Reporting, Non-Snoop Latency Override Value.}
  gPlatformFspPkgTokenSpaceGuid.SaPcieItbtRpNonSnoopLatencyOverrideValue      | * | 0x8 | { 0x00C8, 0x00C8, 0x00C8, 0x00C8 }

  # !BSF NAME:{Force LTR Override} TYPE:{EditNum, HEX, (0x00,0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF)}
  # !BSF HELP:{Force LTR Override.}
  gPlatformFspPkgTokenSpaceGuid.SaPcieItbtRpForceLtrOverride   | * | 0x4 | { 0x00, 0x00, 0x00, 0x00 }

  # !BSF NAME:{PCIE RP Ltr Config Lock} TYPE:{EditNum, HEX, (0x00,0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF)}
  # !BSF HELP:{0: Disable; 1: Enable.}
  gPlatformFspPkgTokenSpaceGuid.SaPcieItbtRpLtrConfigLock      | * | 0x4 | { 0x00, 0x00, 0x00, 0x00 }

  #
  # SA Post-Mem Production Block End
  #

  #
  # CPU Post-Mem Production Block Start
  #
  # !BSF PAGE:{CPU2}
  # !BSF NAME:{Advanced Encryption Standard (AES) feature} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Enable or Disable Advanced Encryption Standard (AES) feature; </b>0: Disable; <b>1: Enable}
  gPlatformFspPkgTokenSpaceGuid.AesEnable                   | * | 0x01 | 0x01

  # !BSF NAME:{Power State 3 enable/disable} TYPE:{EditNum, HEX, (0x00,0xFFFFFFFFFF)}
  # !BSF HELP:{PCODE MMIO Mailbox: Power State 3 enable/disable; 0: Disable; <b>1: Enable</b>. For all VR Indexes}
  gPlatformFspPkgTokenSpaceGuid.Psi3Enable                  | * | 0x05 | {0x01, 0x01, 0x01, 0x01, 0x01}

  # !BSF NAME:{Power State 4 enable/disable}  TYPE:{EditNum, HEX, (0x00,0xFFFFFFFFFF)}
  # !BSF HELP:{PCODE MMIO Mailbox: Power State 4 enable/disable; 0: Disable; <b>1: Enable</b>.For all VR Indexes}
  gPlatformFspPkgTokenSpaceGuid.Psi4Enable                  | * | 0x05 | {0x01, 0x01, 0x01, 0x01, 0x01}

  # !BSF NAME:{Imon slope correction}  TYPE:{EditNum, HEX, (0x00,0xFFFFFFFFFFFFFFFFFFFF)}
  # !HDR STRUCT:{UINT16}
  # !BSF HELP:{PCODE MMIO Mailbox: Imon slope correction. Specified in 1/100 increment values. Range is 0-200. 125 = 1.25. <b>0: Auto</b>.For all VR Indexes}
  gPlatformFspPkgTokenSpaceGuid.ImonSlope                   | * | 0x0A | {0x0, 0x0, 0x0, 0x0, 0x0}

  # !BSF NAME:{Imon offset correction}  TYPE:{EditNum, HEX, (0x00,0xFFFFFFFFFFFFFFFFFFFF)}
  # !HDR STRUCT:{UINT16}
  # !BSF HELP:{PCODE MMIO Mailbox: Imon offset correction. Value is a 2's complement signed integer. Units 1/1000, Range 0-63999. For an offset = 12.580, use 12580. <b>0: Auto</b>}
  gPlatformFspPkgTokenSpaceGuid.ImonOffset                  | * | 0x0A | {0x0, 0x0, 0x0, 0x0, 0x0}

  # !BSF NAME:{Enable/Disable BIOS configuration of VR}  TYPE:{EditNum, HEX, (0x00,0xFFFFFFFFFF)}
  # !BSF HELP:{Enable/Disable BIOS configuration of VR; <b>0: Disable</b>; 1: Enable.For all VR Indexes}
  gPlatformFspPkgTokenSpaceGuid.VrConfigEnable              | * | 0x05 | {0x01, 0x01, 0x01, 0x01, 0x01}

  # !BSF NAME:{Thermal Design Current enable/disable}  TYPE:{EditNum, HEX, (0x00,0xFFFFFFFFFF)}
  # !BSF HELP:{PCODE MMIO Mailbox: Thermal Design Current enable/disable; <b>0: Disable</b>; 1: Enable.For all VR Indexes}
  gPlatformFspPkgTokenSpaceGuid.TdcEnable                   | * | 0x05 | {0x00, 0x00, 0x00, 0x00, 0x00}

  # !BSF NAME:{Thermal Design Current time window} TYPE:{EditNum, HEX, (0x00,0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF)}
  # !HDR STRUCT:{UINT32}
  # !BSF HELP:{PCODE MMIO Mailbox: Thermal Design Current time window. Defined in milli seconds. Range 1ms to 448s}
  gPlatformFspPkgTokenSpaceGuid.TdcTimeWindow               | * | 0x14 | {0x01, 0x01, 0x01, 0x01, 0x01}

  # !BSF NAME:{Thermal Design Current Lock}  TYPE:{EditNum, HEX, (0x00,0xFFFFFFFFFF)}
  # !BSF HELP:{PCODE MMIO Mailbox: Thermal Design Current Lock; <b>0: Disable</b>; 1: Enable.For all VR Indexes}
  gPlatformFspPkgTokenSpaceGuid.TdcLock                     | * | 0x05 | {0x00, 0x00, 0x00, 0x00, 0x00}

  # !BSF NAME:{Platform Psys slope correction}
  # !BSF TYPE:{EditNum, HEX, (0x00,0xC8)}
  # !BSF HELP:{PCODE MMIO Mailbox: Platform Psys slope correction. <b>0 - Auto</b> Specified in 1/100 increment values. Range is 0-200. 125 = 1.25}
  gPlatformFspPkgTokenSpaceGuid.PsysSlope                   | * | 0x01 | 0x00

  # !BSF NAME:{Platform Psys offset correction}
  # !BSF TYPE:{EditNum, HEX, (0x0000,0xFFFF)}
  # !BSF HELP:{PCODE MMIO Mailbox: Platform Psys offset correction. <b>0 - Auto</b> Units 1/1000, Range 0-63999. For an offset of 25.348, enter 25348.}
  gPlatformFspPkgTokenSpaceGuid.PsysOffset                  | * | 0x02 | 0x00

  # !BSF NAME:{Acoustic Noise Mitigation feature}
  # !BSF TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Enable or Disable Acoustic Noise Mitigation feature. <b>0: Disabled</b>; 1: Enabled}
  gPlatformFspPkgTokenSpaceGuid.AcousticNoiseMitigation     | * | 0x01 | 0x00

  # !BSF NAME:{Disable Fast Slew Rate for Deep Package C States for VR domains}
  # !BSF TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Disable Fast Slew Rate for Deep Package C States based on Acoustic Noise Mitigation feature enabled. <b>0: False</b>; 1: True}
  gPlatformFspPkgTokenSpaceGuid.FastPkgCRampDisable       | * | 0x05 | {0x00, 0x00, 0x00, 0x00, 0x00}

  # !BSF NAME:{Slew Rate configuration for Deep Package C States for VR domains}
  # !BSF TYPE:{Combo} OPTION:{0: Fast/2, 1: Fast/4, 2: Fast/8, 3: Fast/16}
  # !BSF HELP:{Slew Rate configuration for Deep Package C States for VR domains based on Acoustic Noise Mitigation feature enabled. ADL supports VCCIA FAST/2/4/8/16, VCCGT FAST/2/4/8 and VCCSA FAST/2 <b>0: Fast/2</b>; 1: Fast/4; 2: Fast/8; 3: Fast/16}
  gPlatformFspPkgTokenSpaceGuid.SlowSlewRate           | * | 0x05 | {0x00, 0x00, 0x00, 0x00, 0x00}

  # !BSF NAME:{Thermal Design Current current limit} TYPE:{EditNum, HEX, (0x00,0xFFFFFFFFFFFFFFFFFFFF)}
  # !HDR STRUCT:{UINT16}
  # !BSF HELP:{PCODE MMIO Mailbox: Thermal Design Current current limit. Specified in 1/8A units. Range is 0-4095. 1000 = 125A. <b>0: Auto</b>. For all VR Indexes}
  gPlatformFspPkgTokenSpaceGuid.TdcCurrentLimit             | * | 0xA | {0x0 , 0x0, 0x0 , 0x0 , 0x0}

  # !BSF NAME:{AcLoadline}TYPE:{EditNum, HEX, (0x00,0xFFFFFFFFFFFFFFFFFFFF)}
  # !HDR STRUCT:{UINT16}
  # !BSF HELP:{PCODE MMIO Mailbox: AcLoadline in 1/100 mOhms (ie. 1250 = 12.50 mOhm); Range is 0-6249. <b>Intel Recommended Defaults vary by domain and SKU.}
  gPlatformFspPkgTokenSpaceGuid.AcLoadline                  | * | 0xA | {0x0 , 0x0, 0x0 , 0x0 , 0x0}

  # !BSF NAME:{DcLoadline} TYPE:{EditNum, HEX, (0x00,0xFFFFFFFFFFFFFFFFFFFF)}
  # !HDR STRUCT:{UINT16}
  # !BSF HELP:{PCODE MMIO Mailbox: DcLoadline in 1/100 mOhms (ie. 1250 = 12.50 mOhm); Range is 0-6249.<b>Intel Recommended Defaults vary by domain and SKU.</b>}
  gPlatformFspPkgTokenSpaceGuid.DcLoadline                  | * | 0xA | {0x0 , 0x0, 0x0 , 0x0 , 0x0}

  # !BSF NAME:{Power State 1 Threshold current} TYPE:{EditNum, HEX, (0x00,0xFFFFFFFFFFFFFFFFFFFF)}
  # !HDR STRUCT:{UINT16}
  # !BSF HELP:{PCODE MMIO Mailbox: Power State 1 current cuttof in 1/4 Amp increments. Range is 0-128A.}
  gPlatformFspPkgTokenSpaceGuid.Psi1Threshold               | * | 0xA | { 80, 80, 80, 80, 80 }

  # !BSF NAME:{Power State 2 Threshold current} TYPE:{EditNum, HEX, (0x00,0xFFFFFFFFFFFFFFFFFFFF)}
  # !HDR STRUCT:{UINT16}
  # !BSF HELP:{PCODE MMIO Mailbox: Power State 2 current cuttof in 1/4 Amp increments. Range is 0-128A.}
  gPlatformFspPkgTokenSpaceGuid.Psi2Threshold               | * | 0xA | { 20, 20, 20, 20, 20 }

  # !BSF NAME:{Power State 3 Threshold current} TYPE:{EditNum, HEX, (0x00,0xFFFFFFFFFFFFFFFFFFFF)}
  # !HDR STRUCT:{UINT16}
  # !BSF HELP:{PCODE MMIO Mailbox: Power State 3 current cuttof in 1/4 Amp increments. Range is 0-128A.}
  gPlatformFspPkgTokenSpaceGuid.Psi3Threshold               | * | 0xA | { 4, 4, 4, 4, 4 }

  # !BSF NAME:{Icc Max limit} TYPE:{EditNum, HEX, (0x00,0xFFFFFFFFFFFFFFFFFFFF)}
  # !HDR STRUCT:{UINT16}
  # !BSF HELP:{PCODE MMIO Mailbox: VR Icc Max limit. 0-512A in 1/4 A units. 400 = 100A}
  gPlatformFspPkgTokenSpaceGuid.IccMax                      | * | 0xA | {0x0 , 0x0, 0x0 , 0x0 , 0x0}

  # !BSF NAME:{Enable or Disable TXT} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Enable or Disable TXT; 0: Disable; <b>1: Enable</b>.}
  gPlatformFspPkgTokenSpaceGuid.TxtEnable                   | * | 0x01 | 0

  # !BSF NAME:{Skip Multi-Processor Initialization} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{When this is skipped, boot loader must initialize processors before SilicionInit API. </b>0: Initialize; <b>1: Skip}
  gPlatformFspPkgTokenSpaceGuid.SkipMpInit                  | * | 0x01 | 0x00

  # !BSF NAME:{FIVR RFI Frequency}
  # !BSF TYPE:{EditNum, HEX, (0x0,0xFFFF)}
  # !BSF HELP:{PCODE MMIO Mailbox: Set the desired RFI frequency, in increments of 100KHz. <b>0: Auto</b>. Range varies based on XTAL clock: 0-1918 (Up to 191.8HMz) for 24MHz clock; 0-1535 (Up to 153.5MHz) for 19MHz clock.}
  gPlatformFspPkgTokenSpaceGuid.FivrRfiFrequency            | * | 0x02 | 0x00

  # !BSF NAME:{FIVR RFI Spread Spectrum}
  # !BSF TYPE:{EditNum, HEX, (0x0,0xFF)}
  # !BSF HELP:{Set the Spread Spectrum Range. <b>1.5%</b>; Range: 0.5%, 1%, 1.5%, 2%, 3%, 4%, 5%, 6%. Each Range is translated to an encoded value for FIVR register. 0.5% = 0, 1% = 3, 1.5% = 8, 2% = 18, 3% = 28, 4% = 34, 5% = 39, 6% = 44.}
  gPlatformFspPkgTokenSpaceGuid.FivrSpreadSpectrum          | * | 0x01 | 0x08

  # !BSF NAME:{CpuBistData} TYPE:{EditNum, HEX, (0x0,0xFFFFFFFF)}
  # !BSF HELP:{Pointer CPU BIST Data}
  gPlatformFspPkgTokenSpaceGuid.CpuBistData                 | * | 0x4 | 0

  # !BSF NAME:{CpuMpPpi} TYPE:{EditNum, HEX, (0x0,0xFFFFFFFF)}
  # !BSF HELP:{<b>Optional</b> pointer to the boot loader's implementation of EFI_PEI_MP_SERVICES_PPI. If not NULL, FSP will use the boot loader's implementation of multiprocessing. See section 5.1.4 of the FSP Integration Guide for more details.}
  gPlatformFspPkgTokenSpaceGuid.CpuMpPpi                     | * | 0x4 | 0

  # !BSF NAME:{Pre Wake Randomization time}
  # !BSF TYPE:{EditNum, HEX, (0x0,0xFF)}
  # !BSF HELP:{PCODE MMIO Mailbox: Acoustic Noise Mitigation Range.Defines the maximum pre-wake randomization time in micro ticks.This can be programmed only if AcousticNoiseMitigation is enabled. Range 0-255 <b>0</b>.}
  gPlatformFspPkgTokenSpaceGuid.PreWake                      | * | 0x01 | 0x00

  # !BSF NAME:{Ramp Up Randomization time}
  # !BSF TYPE:{EditNum, HEX, (0x0,0xFF)}
  # !BSF HELP:{PCODE MMIO Mailbox: Acoustic Noise Mitigation Range.Defines the maximum Ramp Up randomization time in micro ticks.This can be programmed only if AcousticNoiseMitigation is enabled.Range 0-255 <b>0</b>.}
  gPlatformFspPkgTokenSpaceGuid.RampUp                       | * | 0x01 | 0x00

  # !BSF NAME:{Ramp Down Randomization time}
  # !BSF TYPE:{EditNum, HEX, (0x0,0xFF)}
  # !BSF HELP:{PCODE MMIO Mailbox: Acoustic Noise Mitigation Range.Defines the maximum Ramp Down randomization time in micro ticks.This can be programmed only if AcousticNoiseMitigation is enabled.Range 0-255 <b>0</b>.}
  gPlatformFspPkgTokenSpaceGuid.RampDown                      | * | 0x01 | 0x00

  # !BSF NAME:{VR Voltage Limit} TYPE:{EditNum, HEX, (0x00,0xFFFFFFFFFFFFFFFFFFFF)}
  # !HDR STRUCT:{UINT16}
  # !BSF HELP:{PCODE MMIO Mailbox: Voltage Limit. Range is 0 - 7999mV}
  gPlatformFspPkgTokenSpaceGuid.VrVoltageLimit              | * | 0xA | {0x0 , 0x0, 0x0 , 0x0 , 0x0}

  # !BSF NAME:{VccIn Aux Imon IccMax}
  # !BSF TYPE:{EditNum, HEX, (0x0000,0xFFFF)}
  # !BSF HELP:{PCODE MMIO Mailbox: VccIn Aux Imon IccMax. <b>0 - Auto</b> Values are in 1/4 Amp increments. Range is 0-512.}
  gPlatformFspPkgTokenSpaceGuid.VccInAuxImonIccImax           | * | 0x02 | 160

  # !BSF NAME:{Vsys Critical}
  # !BSF TYPE:{EditNum, HEX, (0x0,0xFF)}
  # !BSF HELP:{PCODE MMIO Mailbox: Vsys Critical. <b>0: Disable</b>; 1: Enable Range is 0-255.}
  gPlatformFspPkgTokenSpaceGuid.EnableVsysCritical              | * | 0x01 | 0x00

  # !BSF NAME:{Vsys Full Scale}
  # !BSF TYPE:{EditNum, HEX, (0x0,0xFF)}
  # !BSF HELP:{Vsys Full Scale, Range is 0-255}
  gPlatformFspPkgTokenSpaceGuid.VsysFullScale                   | * | 0x01 | 0x18

  # !BSF NAME:{Vsys Critical Threshold}
  # !BSF TYPE:{EditNum, HEX, (0x0,0xFF)}
  # !BSF HELP:{Vsys Critical Threshold, Range is 0-255 }
  gPlatformFspPkgTokenSpaceGuid.VsysCriticalThreshold           | * | 0x01 | 0x06

  # !BSF NAME:{Assertion Deglitch Mantissa}
  # !BSF TYPE:{EditNum, HEX, (0x0,0xFF)}
  # !BSF HELP:{Assertion Deglitch Mantissa, Range is 0-255}
  gPlatformFspPkgTokenSpaceGuid.VsysAssertionDeglitchMantissa   | * | 0x01 | 0x01

  # !BSF NAME:{Assertion Deglitch Exponent}
  # !BSF TYPE:{EditNum, HEX, (0x0,0xFF)}
  # !BSF HELP:{Assertion Deglitch Exponent, Range is 0-255}
  gPlatformFspPkgTokenSpaceGuid.VsysAssertionDeglitchExponent   | * | 0x01 | 0x00

  # !BSF NAME:{De assertion Deglitch Mantissa}
  # !BSF TYPE:{EditNum, HEX, (0x0,0xFF)}
  # !BSF HELP:{De assertion Deglitch Mantissa, Range is 0-255}
  gPlatformFspPkgTokenSpaceGuid.VsysDeassertionDeglitchMantissa | * | 0x01 | 0x0D

  # !BSF NAME:{De assertion Deglitch Exponent}
  # !BSF TYPE:{EditNum, HEX, (0x0,0xFF)}
  # !BSF HELP:{De assertion Deglitch Exponent, Range is 0-255}
  gPlatformFspPkgTokenSpaceGuid.VsysDeassertionDeglitchExponent | * | 0x01 | 0x02

  # !BSF NAME:{VccIn Aux Imon slope correction}
  # !BSF TYPE:{EditNum, HEX, (0x00,0xC8)}
  # !BSF HELP:{PCODE MMIO Mailbox: VccIn Aux Imon slope correction. <b>0 - Auto</b> Specified in 1/100 increment values. Range is 0-200. 125 = 1.25}
  gPlatformFspPkgTokenSpaceGuid.VccInAuxImonSlope             | * | 0x01 | 0x01

  # !BSF NAME:{VccIn Aux Imon offset correction}
  # !BSF TYPE:{EditNum, HEX, (0x0000,0xFFFF)}
  # !BSF HELP:{PCODE MMIO Mailbox: VccIn Aux Imon offset correction. <b>0 - Auto</b> Units 1/1000, Range 0-63999. For an offset of 25.348, enter 25348.}
  gPlatformFspPkgTokenSpaceGuid.VccInAuxImonOffset            | * | 0x02 | 0x00

  # !BSF NAME:{FIVR RFI Spread Spectrum Enable or disable}
  # !BSF TYPE:{EditNum, HEX, (0x0,0xFF)}
  # !BSF HELP:{Enable or Disable FIVR RFI Spread Spectrum. 0: Disable ; <b> 1: Enable </b>}
  gPlatformFspPkgTokenSpaceGuid.FivrSpectrumEnable            | * | 0x01 | 0x01

  # !BSF NAME:{VR Fast Vmode ICC Limit support} TYPE:{EditNum, HEX, (0x00,0xFFFFFFFFFFFFFFFFFFFF)}
  # !HDR STRUCT:{UINT16}
  # !BSF HELP:{PCODE MMIO Mailbox: VR Fast Vmode ICC Limit support. 0-255A in 1/4 A units. 400 = 100A}
  gPlatformFspPkgTokenSpaceGuid.IccLimit                      | * | 0xA | {0x0 , 0x0, 0x0 , 0x0 , 0x0}

  gPlatformFspPkgTokenSpaceGuid.CpuPostMemRsvd                | * | 0x2 | {0x00}

  # !BSF NAME:{PpinSupport to view Protected Processor Inventory Number}
  # !BSF TYPE:{Combo} OPTION:{0: Disable, 1: Enable, 2: Auto}
  # !BSF HELP:{Enable or Disable or Auto (Based on End of Manufacturing flag. Disabled if this flag is set) for PPIN Support}
  gPlatformFspPkgTokenSpaceGuid.PpinSupport                 | * | 0x1 | 0x00

  # !BSF NAME:{Enable or Disable Minimum Voltage Override} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Enable or disable Minimum Voltage overrides ; <b>0: Disable</b>; 1: Enable.}
  gPlatformFspPkgTokenSpaceGuid.EnableMinVoltageOverride    | * | 0x01 | 0x0

  # !BSF NAME:{Min Voltage for Runtime }
  # !BSF TYPE:{EditNum, HEX, (0x00,0x7CF)}
  # !BSF HELP:{PCODE MMIO Mailbox: Minimum voltage for runtime. Valid if EnableMinVoltageOverride = 1. Range 0 to 1999mV. <b> 0: 0mV </b>}
  gPlatformFspPkgTokenSpaceGuid.MinVoltageRuntime           | * | 0x02 | 0x00

  # !BSF PAGE:{DBG}
  # !BSF NAME:{Memory size per thread allocated for Processor Trace}
  # !BSF TYPE:{EditNum, HEX, (0x00,0xFF)}
  # !BSF HELP:{Memory size per thread for Processor Trace. Processor Trace requires 2^N alignment and size in bytes per thread, from 4KB to 128MB.\n<b> 0xff:none </b>, 0:4k, 0x1:8k, 0x2:16k, 0x3:32k, 0x4:64k, 0x5:128k, 0x6:256k, 0x7:512k, 0x8:1M, 0x9:2M, 0xa:4M. 0xb:8M, 0xc:16M, 0xd:32M, 0xe:64M, 0xf:128M}
  gPlatformFspPkgTokenSpaceGuid.ProcessorTraceMemSize       | * | 0x1 | 0xff

  # !BSF PAGE:{CPU2}
  # !BSF NAME:{Min Voltage for C8 }
  # !BSF TYPE:{EditNum, HEX, (0x00,0x7CF)}
  # !BSF HELP:{PCODE MMIO Mailbox: Minimum voltage for C8. Valid if EnableMinVoltageOverride = 1. Range 0 to 1999mV. <b> 0: 0mV </b>}
  gPlatformFspPkgTokenSpaceGuid.MinVoltageC8                | * | 0x02 | 0x00

  # !BSF NAME:{Smbios Type4 Max Speed Override}
  # !BSF TYPE:{EditNum, HEX, (0x0000,0xFFFF)}
  # !BSF HELP:{Provide the option for platform to override the MaxSpeed field of Smbios Type 4. If this value is not zero, it dominates the field.}
  gPlatformFspPkgTokenSpaceGuid.SmbiosType4MaxSpeedOverride | * | 0x02 | 0x00

  # !BSF NAME:{Current root mean square}  TYPE:{EditNum, HEX, (0x00,0xFFFFFFFFFF)}
  # !BSF HELP:{PCODE MMIO Mailbox: Current root mean square; <b>0: Disable</b>; 1: Enable.For all VR Indexes}
  gPlatformFspPkgTokenSpaceGuid.Irms                        | * | 0x05 | {0x00, 0x00, 0x00, 0x00, 0x00}

  # !BSF NAME:{AvxDisable}
  # !BSF TYPE:{Combo} OPTION:{0: Enable, 1: Disable}
  # !BSF HELP:{Enable or Disable AVX Support. This only applicable when all small core is disabled.}
  gPlatformFspPkgTokenSpaceGuid.AvxDisable                  | * | 0x1 | 0x00

  # !BSF NAME:{Avx3Disable}
  # !BSF TYPE:{Combo} OPTION:{0: Enable, 1: Disable}
  # !BSF HELP:{DEPRECATED}
  gPlatformFspPkgTokenSpaceGuid.Avx3Disable                 | * | 0x1 | 0x01

  # !BSF NAME:{X2ApicSupport}
  # !BSF TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Enable or Disable X2APIC Support}
  gPlatformFspPkgTokenSpaceGuid.X2ApicSupport               | * | 0x1 | 0x00

  # !BSF NAME:{CPU VR Power Delivery Design}
  # !BSF TYPE:{EditNum, HEX, (0x00, 0xFF)}
  # !BSF HELP:{Used to communicate the power delivery design capability of the board. This value is an enum of the available power delivery segments that are defined in the Platform Design Guide.}
  gPlatformFspPkgTokenSpaceGuid.VrPowerDeliveryDesign       | * | 0x01 | 0x00

!if gSiPkgTokenSpaceGuid.PcdEmbeddedEnable == 0x1
  # !BSF NAME:{AC Split Lock} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Enable/Disable #AC check on split lock. <b>0: Disable</b>; 1: Enable.}
  gPlatformFspPkgTokenSpaceGuid.AcSplitLock                  | * | 0x1 | 0x0
!endif


  # !BSF NAME:{ReservedCpuPostMemProduction} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Reserved for CPU Post-Mem Production}
  gPlatformFspPkgTokenSpaceGuid.ReservedCpuPostMemProduction | * | 0x20 | {0x00}

  #
  # CPU Post-Mem Production Block End
  #

  #
  #  PCH Silicon Offset start
  #
  # !BSF PAGE:{PCH2}
  # !BSF NAME:{Enable Power Optimizer} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Enable DMI Power Optimizer on PCH side.}
  gPlatformFspPkgTokenSpaceGuid.PchPwrOptEnable             | * | 0x01 | 0x00

  # !BSF NAME:{PCH Flash Protection Ranges Write Enble} TYPE:{EditNum, HEX, (0x00,0xFFFFFFFFFF)}
  # !BSF HELP:{Write or erase is blocked by hardware.}
  gPlatformFspPkgTokenSpaceGuid.PchWriteProtectionEnable    | * | 0x05 | { 0x00, 0x00, 0x00, 0x00, 0x00 }

  # !BSF NAME:{PCH Flash Protection Ranges Read Enble} TYPE:{EditNum, HEX, (0x00,0xFFFFFFFFFF)}
  # !BSF HELP:{Read is blocked by hardware.}
  gPlatformFspPkgTokenSpaceGuid.PchReadProtectionEnable     | * | 0x05 | { 0x00, 0x00, 0x00, 0x00, 0x00 }

  # !BSF NAME:{PCH Protect Range Limit} TYPE:{EditNum, HEX, (0x00,0xFFFFFFFFFFFFFFFFFFFF)}
  # !HDR STRUCT:{UINT16}
  # !BSF HELP:{Left shifted address by 12 bits with address bits 11:0 are assumed to be FFFh for limit comparison.}
  gPlatformFspPkgTokenSpaceGuid.PchProtectedRangeLimit      | * | 0x0A | { 0x0000, 0x0000, 0x0000, 0x0000, 0x0000 }

  # !BSF NAME:{PCH Protect Range Base} TYPE:{EditNum, HEX, (0x00,0xFFFFFFFFFFFFFFFFFFFF)}
  # !HDR STRUCT:{UINT16}
  # !BSF HELP:{Left shifted address by 12 bits with address bits 11:0 are assumed to be 0.}
  gPlatformFspPkgTokenSpaceGuid.PchProtectedRangeBase       | * | 0x0A | { 0x0000, 0x0000, 0x0000, 0x0000, 0x0000 }

  # !BSF NAME:{Enable Pme} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Enable Azalia wake-on-ring.}
  gPlatformFspPkgTokenSpaceGuid.PchHdaPme                   | * | 0x01 | 0x00

  # !BSF NAME:{HD Audio Link Frequency} TYPE:{Combo} OPTION:{0: 6MHz, 1: 12MHz, 2: 24MHz}
  # !BSF HELP:{HDA Link Freq (PCH_HDAUDIO_LINK_FREQUENCY enum): 0: 6MHz, 1: 12MHz, 2: 24MHz.}
  gPlatformFspPkgTokenSpaceGuid.PchHdaLinkFrequency         | * | 0x01 | 0x02

  # !BSF NAME:{Enable PCH ISH SPI Cs0 pins assigned} TYPE:{EditNum, HEX, (0x0,0xFF)}
  # !BSF HELP:{Set if ISH SPI Cs0 pins are to be enabled by BIOS. 0: Disable; 1: Enable.}
  gPlatformFspPkgTokenSpaceGuid.PchIshSpiCs0Enable          | * | 0x01 | { 0x00 }

  # !BSF NAME:{Enable PCH Io Apic Entry 24-119} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{0: Disable; 1: Enable.}
  gPlatformFspPkgTokenSpaceGuid.PchIoApicEntry24_119        | * | 0x01 | 0x01

  # !BSF NAME:{PCH Io Apic ID} TYPE:{EditNum, HEX, (0x0,0xFF)}
  # !BSF HELP:{This member determines IOAPIC ID. Default is 0x02.}
  gPlatformFspPkgTokenSpaceGuid.PchIoApicId                 | * | 0x01 | 0x02

  # !BSF NAME:{Enable PCH ISH SPI pins assigned} TYPE:{EditNum, HEX, (0x0,0xFF)}
  # !BSF HELP:{Set if ISH SPI native pins are to be enabled by BIOS. 0: Disable; 1: Enable.}
  gPlatformFspPkgTokenSpaceGuid.PchIshSpiEnable             | * | 0x01 | { 0x00 }

  # !BSF NAME:{Enable PCH ISH UART pins assigned} TYPE:{EditNum, HEX, (0x0,0xFFFF)}
  # !BSF HELP:{Set if ISH UART native pins are to be enabled by BIOS. 0: Disable; 1: Enable.}
  gPlatformFspPkgTokenSpaceGuid.PchIshUartEnable            | * | 0x02 | { 0x00, 0x00 }

  # !BSF NAME:{Enable PCH ISH I2C pins assigned} TYPE:{EditNum, HEX, (0x0,0xFFFFFF)}
  # !BSF HELP:{Set if ISH I2C native pins are to be enabled by BIOS. 0: Disable; 1: Enable.}
  gPlatformFspPkgTokenSpaceGuid.PchIshI2cEnable             | * | 0x03 | { 0x00, 0x00, 0x00 }

  # !BSF NAME:{Enable PCH ISH GP pins assigned} TYPE:{EditNum, HEX, (0x00,0xFFFFFFFFFFFFFFFF)}
  # !BSF HELP:{Set if ISH GP native pins are to be enabled by BIOS. 0: Disable; 1: Enable.}
  gPlatformFspPkgTokenSpaceGuid.PchIshGpEnable              | * | 0x8 | { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 }

  # !BSF NAME:{PCH ISH PDT Unlock Msg} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{0: False; 1: True.}
  gPlatformFspPkgTokenSpaceGuid.PchIshPdtUnlock             | * | 0x01 | 0x00

  # !BSF NAME:{Enable PCH Lan LTR capabilty of PCH internal LAN} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{0: Disable; 1: Enable.}
  gPlatformFspPkgTokenSpaceGuid.PchLanLtrEnable             | * | 0x01 | 0x01

  # !BSF NAME:{Enable LOCKDOWN BIOS LOCK} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Enable the BIOS Lock feature and set EISS bit (D31:F5:RegDCh[5]) for the BIOS region protection.}
  gPlatformFspPkgTokenSpaceGuid.PchLockDownBiosLock         | * | 0x01 | 0x00

  # !BSF NAME:{PCH Compatibility Revision ID} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{This member describes whether or not the CRID feature of PCH should be enabled.}
  gPlatformFspPkgTokenSpaceGuid.PchCrid                     | * | 0x01 | 0x00

  # !BSF NAME:{RTC BIOS Interface Lock}
  # !BSF TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Enable RTC BIOS interface lock. When set, prevents RTC TS (BUC.TS) from being changed.}
  gPlatformFspPkgTokenSpaceGuid.RtcBiosInterfaceLock        | * | 0x01 | 0x01

  # !BSF NAME:{RTC Cmos Memory Lock}
  # !BSF TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Enable RTC lower and upper 128 byte Lock bits to lock Bytes 38h-3Fh in the upper and and lower 128-byte bank of RTC RAM.}
  gPlatformFspPkgTokenSpaceGuid.RtcMemoryLock               | * | 0x01 | 0x01

  # !BSF NAME:{Enable PCIE RP HotPlug} TYPE:{EditNum, HEX, (0x00,0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF)}
  # !BSF HELP:{Indicate whether the root port is hot plug available.}
  gPlatformFspPkgTokenSpaceGuid.PcieRpHotPlug               | * | 0x1C | { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 }

  # !BSF NAME:{Enable PCIE RP Pm Sci} TYPE:{EditNum, HEX, (0x00,0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF)}
  # !BSF HELP:{Indicate whether the root port power manager SCI is enabled.}
  gPlatformFspPkgTokenSpaceGuid.PcieRpPmSci                 | * | 0x1C | { 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01 }

  # !BSF NAME:{Enable PCIE RP Transmitter Half Swing} TYPE:{EditNum, HEX, (0x00,0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF)}
  # !BSF HELP:{Indicate whether the Transmitter Half Swing is enabled.}
  gPlatformFspPkgTokenSpaceGuid.PcieRpTransmitterHalfSwing  | * | 0x1C | { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 }

  # !BSF NAME:{Enable PCIE RP Clk Req Detect} TYPE:{EditNum, HEX, (0x00,0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF)}
  # !BSF HELP:{Probe CLKREQ# signal before enabling CLKREQ# based power management.}
  gPlatformFspPkgTokenSpaceGuid.PcieRpClkReqDetect          | * | 0x1C | { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 }

  # !BSF NAME:{PCIE RP Advanced Error Report} TYPE:{EditNum, HEX, (0x00,0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF)}
  # !BSF HELP:{Indicate whether the Advanced Error Reporting is enabled.}
  gPlatformFspPkgTokenSpaceGuid.PcieRpAdvancedErrorReporting   | * | 0x1C | { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 }

  # !BSF NAME:{PCIE RP Unsupported Request Report} TYPE:{EditNum, HEX, (0x00,0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF)}
  # !BSF HELP:{Indicate whether the Unsupported Request Report is enabled.}
  gPlatformFspPkgTokenSpaceGuid.PcieRpUnsupportedRequestReport | * | 0x1C | { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 }

  # !BSF NAME:{PCIE RP Fatal Error Report} TYPE:{EditNum, HEX, (0x00,0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF)}
  # !BSF HELP:{Indicate whether the Fatal Error Report is enabled.}
  gPlatformFspPkgTokenSpaceGuid.PcieRpFatalErrorReport      | * | 0x1C | { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 }

  # !BSF NAME:{PCIE RP No Fatal Error Report} TYPE:{EditNum, HEX, (0x00,0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF)}
  # !BSF HELP:{Indicate whether the No Fatal Error Report is enabled.}
  gPlatformFspPkgTokenSpaceGuid.PcieRpNoFatalErrorReport    | * | 0x1C | { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 }

  # !BSF NAME:{PCIE RP Correctable Error Report} TYPE:{EditNum, HEX, (0x00,0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF)}
  # !BSF HELP:{Indicate whether the Correctable Error Report is enabled.}
  gPlatformFspPkgTokenSpaceGuid.PcieRpCorrectableErrorReport   | * | 0x1C | { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 }

  # !BSF NAME:{PCIE RP System Error On Fatal Error} TYPE:{EditNum, HEX, (0x00,0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF)}
  # !BSF HELP:{Indicate whether the System Error on Fatal Error is enabled.}
  gPlatformFspPkgTokenSpaceGuid.PcieRpSystemErrorOnFatalError  | * | 0x1C | { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 }

  # !BSF NAME:{PCIE RP System Error On Non Fatal Error} TYPE:{EditNum, HEX, (0x00,0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF)}
  # !BSF HELP:{Indicate whether the System Error on Non Fatal Error is enabled.}
  gPlatformFspPkgTokenSpaceGuid.PcieRpSystemErrorOnNonFatalError | * | 0x1C | { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 }

  # !BSF NAME:{PCIE RP System Error On Correctable Error} TYPE:{EditNum, HEX, (0x00,0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF)}
  # !BSF HELP:{Indicate whether the System Error on Correctable Error is enabled.}
  gPlatformFspPkgTokenSpaceGuid.PcieRpSystemErrorOnCorrectableError | * | 0x1C | { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 }

  # !BSF NAME:{PCIE RP Max Payload} TYPE:{EditNum, HEX, (0x00,0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF)}
  # !BSF HELP:{Max Payload Size supported, Default 128B, see enum PCH_PCIE_MAX_PAYLOAD.}
  gPlatformFspPkgTokenSpaceGuid.PcieRpMaxPayload            | * | 0x1C | { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 }

  # !BSF NAME:{Touch Host Controller Port 0 Assignment} TYPE:{Combo} OPTION:{0x0:ThcAssignmentNone, 0x1:ThcAssignmentThc0}
  # !BSF HELP:{Assign THC Port 0}
  gPlatformFspPkgTokenSpaceGuid.ThcPort0Assignment          | * | 0x01 | 0x0

  # !BSF NAME:{Touch Host Controller Port 0 Interrupt Pin Mux} TYPE:{EditNum, HEX, (0,0xFFFFFFFF)}
  # !BSF HELP:{Set THC Port 0 Pin Muxing Value if signal can be enabled on multiple pads. Refer to GPIO_*_MUXING_THC_SPIx_INTB_* for possible values.}
  gPlatformFspPkgTokenSpaceGuid.ThcPort0InterruptPinMuxing  | * | 0x04 | 0x0

  # !BSF NAME:{Touch Host Controller Port 0 Wake On Touch} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Based on this setting vGPIO for given THC will be in native mode, and additional _CRS for wake will be exposed in ACPI}
  gPlatformFspPkgTokenSpaceGuid.ThcPort0WakeOnTouch         | * | 0x01 | 0x0

  # !BSF NAME:{Touch Host Controller Port 1 Assignment} TYPE:{Combo} OPTION:{0x0:ThcAssignmentNone, 0x1:ThcPort1AssignmentThc0, 0x2:ThcAssignmentThc1}
  # !BSF HELP:{Assign THC Port 1}
  gPlatformFspPkgTokenSpaceGuid.ThcPort1Assignment          | * | 0x01 | 0x0

  # !BSF NAME:{Touch Host Controller Port 1 Interrupt Pin Mux} TYPE:{EditNum, HEX, (0,0xFFFFFFFF)}
  # !BSF HELP:{Set THC Port 1 Pin Muxing Value if signal can be enabled on multiple pads. Refer to GPIO_*_MUXING_THC_SPIx_INTB_* for possible values.}
  gPlatformFspPkgTokenSpaceGuid.ThcPort1InterruptPinMuxing  | * | 0x04 | 0x0

  # !BSF NAME:{Touch Host Controller Port 1 Wake On Touch} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Based on this setting vGPIO for given THC will be in native mode, and additional _CRS for wake will be exposed in ACPI}
  gPlatformFspPkgTokenSpaceGuid.ThcPort1WakeOnTouch         | * | 0x01 | 0x0

  # !BSF NAME:{PCIE RP Pcie Speed} TYPE:{EditNum, HEX, (0x00,0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF)}
  # !BSF HELP:{Determines each PCIE Port speed capability. 0: Auto; 1: Gen1; 2: Gen2; 3: Gen3; 4: Gen4 (see: PCIE_SPEED).}
  gPlatformFspPkgTokenSpaceGuid.PcieRpPcieSpeed             | * | 0x1C | { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 }

  # !BSF NAME:{PCIE RP Physical Slot Number} TYPE:{EditNum, HEX, (0x00,0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF)}
  # !BSF HELP:{Indicates the slot number for the root port. Default is the value as root port index.}
  gPlatformFspPkgTokenSpaceGuid.PcieRpPhysicalSlotNumber    | * | 0x1C | { 0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0A, 0x0B, 0x0C, 0x0D, 0x0E, 0x0F, 0x10, 0x11, 0x12, 0x13, 0x14, 0x15, 0x16, 0x17, 0x18, 0x19, 0x1A, 0x1B }

  # !BSF NAME:{PCIE RP Completion Timeout} TYPE:{EditNum, HEX, (0x00,0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF)}
  # !BSF HELP:{The root port completion timeout(see: PCIE_COMPLETION_TIMEOUT). Default is PchPcieCompletionTO_Default.}
  gPlatformFspPkgTokenSpaceGuid.PcieRpCompletionTimeout     | * | 0x1C | { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 }

  # !BSF NAME:{PCIE RP Aspm} TYPE:{EditNum, HEX, (0x00,0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF)}
  # !BSF HELP:{The ASPM configuration of the root port (see: PCH_PCIE_ASPM_CONTROL). Default is PchPcieAspmAutoConfig.}
  gPlatformFspPkgTokenSpaceGuid.PcieRpAspm                  | * | 0x1C | { 0x04, 0x04, 0x04, 0x04, 0x04, 0x04, 0x04, 0x04, 0x04, 0x04, 0x04, 0x04, 0x04, 0x04, 0x04, 0x04, 0x04, 0x04, 0x04, 0x04, 0x04, 0x04, 0x04, 0x04, 0x04, 0x04, 0x04, 0x04 }

  # !BSF NAME:{PCIE RP L1 Substates} TYPE:{EditNum, HEX, (0x00,0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF)}
  # !BSF HELP:{The L1 Substates configuration of the root port (see: PCH_PCIE_L1SUBSTATES_CONTROL). Default is PchPcieL1SubstatesL1_1_2.}
  gPlatformFspPkgTokenSpaceGuid.PcieRpL1Substates           | * | 0x1C | { 0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03 }

  # !BSF NAME:{PCIE RP L1 Low Substate} TYPE:{EditNum, HEX, (0x00,0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF)}
  # !BSF HELP:{The L1 Low Substate configuration of the root port. 0: Disable; 1: Enable.}
  gPlatformFspPkgTokenSpaceGuid.PcieRpL1Low                 | * | 0x1C | { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 }

  # !BSF NAME:{PCIE RP Ltr Enable} TYPE:{EditNum, HEX, (0x00,0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF)}
  # !BSF HELP:{Latency Tolerance Reporting Mechanism.}
  gPlatformFspPkgTokenSpaceGuid.PcieRpLtrEnable             | * | 0x1C | { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 }

  # !BSF NAME:{PCIE RP Ltr Config Lock} TYPE:{EditNum, HEX, (0x00,0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF)}
  # !BSF HELP:{0: Disable; 1: Enable.}
  gPlatformFspPkgTokenSpaceGuid.PcieRpLtrConfigLock         | * | 0x1C | { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 }

  # !BSF NAME:{PCIe override default settings for EQ} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Choose PCIe EQ method}
  gPlatformFspPkgTokenSpaceGuid.PcieEqOverrideDefault       | * | 0x1 | 0x00

  # !BSF NAME:{PCIe choose EQ method} TYPE:{Combo} OPTION:{0: HardwareEq, 1: FixedEq}
  # !BSF HELP:{Choose PCIe EQ method}
  gPlatformFspPkgTokenSpaceGuid.PcieEqMethod                | * | 0x1 | 0x00

  # !BSF NAME:{PCIe choose EQ mode} TYPE:{Combo} OPTION:{0: PresetEq, 1: CoefficientEq}
  # !BSF HELP:{Choose PCIe EQ mode}
  gPlatformFspPkgTokenSpaceGuid.PcieEqMode                  | * | 0x1 | 0x00

  # !BSF NAME:{PCIe EQ local transmitter override} Type:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Enable/Disable local transmitter override}
  gPlatformFspPkgTokenSpaceGuid.PcieEqLocalTransmitterOverrideEnable | * | 0x1 | 0x00
  # !BSF NAME:{PCIe number of valid list entries} TYPE:{EditNum, HEX, (0, 11)}
  # !BSF HELP:{Select number of presets or coefficients depending on the mode}
  gPlatformFspPkgTokenSpaceGuid.PcieEqPh3NumberOfPresetsOrCoefficients | * | 0x1 | 0x00

  # !BSF NAME:{PCIe pre-cursor coefficient list} TYPE:{EditNum, HEX, (0x0, 0x3F3F3F3F3F3F3F3F3F3F)}
  # !BSF HELP:{Provide a list of pre-cursor coefficients to be used during phase 3 EQ}
  gPlatformFspPkgTokenSpaceGuid.PcieEqPh3PreCursorList         | * | 0xA | {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00}

  # !BSF NAME:{PCIe post-cursor coefficient list} TYPE:{EditNum, HEX, (0, 0x3F3F3F3F3F3F3F3F3F3F)}
  # !BSF HELP:{Provide a list of post-cursor coefficients to be used during phase 3 EQ}
  gPlatformFspPkgTokenSpaceGuid.PcieEqPh3PostCursorList        | * | 0xA | {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00}

  # !BSF NAME:{PCIe preset list} TYPE:{EditNum, HEX, (0, 0x3F3F3F3F3F3F3F3F3F3F3F)}
  # !BSF HELP:{Provide a list of presets to be used during phase 3 EQ}
  gPlatformFspPkgTokenSpaceGuid.PcieEqPh3PresetList        | * | 0xB | {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00}

  # !BSF NAME:{PCIe EQ phase 1 downstream transmitter port preset} TYPE:{EditNum, HEX, (0,0xFFFFFFFF)}
  # !BSF HELP:{Allows to select the downstream port preset value that will be used during phase 1 of equalization}
  gPlatformFspPkgTokenSpaceGuid.PcieEqPh1DownstreamPortTransmitterPreset  | * | 0x4 | 0x0

  # !BSF NAME:{PCIe EQ phase 1 upstream tranmitter port preset} TYPE:{EditNum, HEX, (0,0xFFFFFFFF)}
  # !BSF HELP:{Allows to select the upstream port preset value that will be used during phase 1 of equalization}
  gPlatformFspPkgTokenSpaceGuid.PcieEqPh1UpstreamPortTransmitterPreset    | * | 0x4 | 0x0
  # !BSF NAME:{PCIe EQ phase 2 local transmitter override preset} Type:{EditNum, DEC, (0, 10)}
  # !BSF HELP:{Allows to select the value of the preset used during phase 2 local transmitter override}
  gPlatformFspPkgTokenSpaceGuid.PcieEqPh2LocalTransmitterOverridePreset | * | 0x1 | 0x0

  # !BSF NAME:{PCIE Enable Peer Memory Write} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{This member describes whether Peer Memory Writes are enabled on the platform.}
  gPlatformFspPkgTokenSpaceGuid.PcieEnablePeerMemoryWrite   | * | 0x1C | { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 }

  # !BSF NAME:{PCIE Compliance Test Mode} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Compliance Test Mode shall be enabled when using Compliance Load Board.}
  gPlatformFspPkgTokenSpaceGuid.PcieComplianceTestMode      | * | 0x01 | 0x00

  # !BSF NAME:{PCIE Rp Function Swap} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{DEPRECATED. Allows BIOS to use root port function number swapping when root port of function 0 is disabled.}
  gPlatformFspPkgTokenSpaceGuid.PcieRpFunctionSwap          | * | 0x01 | 0x01

  # !BSF NAME:{Enable/Disable PEG GEN3 Static EQ Phase1 programming} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Program Gen3 EQ Phase1 Static Presets. Disabled(0x0): Disable EQ Phase1 Static Presets Programming, Enabled(0x1)(Default): Enable  EQ Phase1 Static Presets Programming}
  gPlatformFspPkgTokenSpaceGuid.CpuPcieGen3ProgramStaticEq      | * | 0x01 | 0x1

  # !BSF NAME:{Enable/Disable GEN4 Static EQ Phase1 programming} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Program Gen4 EQ Phase1 Static Presets. Disabled(0x0): Disable EQ Phase1 Static Presets Programming, Enabled(0x1)(Default): Enable  EQ Phase1 Static Presets Programming}
  gPlatformFspPkgTokenSpaceGuid.CpuPcieGen4ProgramStaticEq      | * | 0x01 | 0x1

  # !BSF NAME:{PCH Pm PME_B0_S5_DIS} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{When cleared (default), wake events from PME_B0_STS are allowed in S5 if PME_B0_EN = 1.}
  gPlatformFspPkgTokenSpaceGuid.PchPmPmeB0S5Dis             | * | 0x01 | 0x00

  # !BSF NAME:{PCIE IMR} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Enables Isolated Memory Region for PCIe.}
  gPlatformFspPkgTokenSpaceGuid.PcieRpImrEnabled           | * | 0x01 | 0x01

  # !BSF NAME:{PCIE IMR port number} TYPE:{EditNum, HEX, (0x0,23)}
  # !BSF HELP:{Selects PCIE root port number for IMR feature.}
  gPlatformFspPkgTokenSpaceGuid.PcieRpImrSelection         | * | 0x01 | 0x00

  # !BSF NAME:{PCH Pm Wol Enable Override} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Corresponds to the WOL Enable Override bit in the General PM Configuration B (GEN_PMCON_B) register.}
  gPlatformFspPkgTokenSpaceGuid.PchPmWolEnableOverride      | * | 0x01 | 0x01

  # !BSF NAME:{PCH Pm Pcie Wake From DeepSx} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Determine if enable PCIe to wake from deep Sx.}
  gPlatformFspPkgTokenSpaceGuid.PchPmPcieWakeFromDeepSx     | * | 0x01 | 0x00

  # !BSF NAME:{PCH Pm WoW lan Enable} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Determine if WLAN wake from Sx, corresponds to the HOST_WLAN_PP_EN bit in the PWRM_CFG3 register.}
  gPlatformFspPkgTokenSpaceGuid.PchPmWoWlanEnable           | * | 0x01 | 0x00

  # !BSF NAME:{PCH Pm WoW lan DeepSx Enable} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Determine if WLAN wake from DeepSx, corresponds to the DSX_WLAN_PP_EN bit in the PWRM_CFG3 register.}
  gPlatformFspPkgTokenSpaceGuid.PchPmWoWlanDeepSxEnable     | * | 0x01 | 0x00

  # !BSF NAME:{PCH Pm Lan Wake From DeepSx} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Determine if enable LAN to wake from deep Sx.}
  gPlatformFspPkgTokenSpaceGuid.PchPmLanWakeFromDeepSx      | * | 0x01 | 0x01

  # !BSF NAME:{PCH Pm Deep Sx Pol} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Deep Sx Policy.}
  gPlatformFspPkgTokenSpaceGuid.PchPmDeepSxPol              | * | 0x01 | 0x00

  # !BSF NAME:{PCH Pm Slp S3 Min Assert} TYPE:{EditNum, HEX, (0x0,0xFF)}
  # !BSF HELP:{SLP_S3 Minimum Assertion Width Policy. Default is PchSlpS350ms.}
  gPlatformFspPkgTokenSpaceGuid.PchPmSlpS3MinAssert         | * | 0x01 | 0x03

  # !BSF NAME:{PCH Pm Slp S4 Min Assert} TYPE:{EditNum, HEX, (0x0,0xFF)}
  # !BSF HELP:{SLP_S4 Minimum Assertion Width Policy. Default is PchSlpS44s.}
  gPlatformFspPkgTokenSpaceGuid.PchPmSlpS4MinAssert         | * | 0x01 | 0x01

  # !BSF NAME:{PCH Pm Slp Sus Min Assert} TYPE:{EditNum, HEX, (0x0,0xFF)}
  # !BSF HELP:{SLP_SUS Minimum Assertion Width Policy. Default is PchSlpSus4s.}
  gPlatformFspPkgTokenSpaceGuid.PchPmSlpSusMinAssert        | * | 0x01 | 0x04

  # !BSF NAME:{PCH Pm Slp A Min Assert} TYPE:{EditNum, HEX, (0x0,0xFF)}
  # !BSF HELP:{SLP_A Minimum Assertion Width Policy. Default is PchSlpA2s.}
  gPlatformFspPkgTokenSpaceGuid.PchPmSlpAMinAssert          | * | 0x01 | 0x04

  # !BSF NAME:{USB Overcurrent Override for VISA} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{This option overrides USB Over Current enablement state that USB OC will be disabled after enabling this option. Enable when VISA pin is muxed with USB OC}
  gPlatformFspPkgTokenSpaceGuid.PchEnableDbcObs             | * | 0x01 | 0x00

  # !BSF NAME:{PCH Pm Slp Strch Sus Up} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Enable SLP_X Stretching After SUS Well Power Up.}
  gPlatformFspPkgTokenSpaceGuid.PchPmSlpStrchSusUp          | * | 0x01 | 0x00

  # !BSF NAME:{PCH Pm Slp Lan Low Dc} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Enable/Disable SLP_LAN# Low on DC Power.}
  gPlatformFspPkgTokenSpaceGuid.PchPmSlpLanLowDc            | * | 0x01 | 0x01

  # !BSF NAME:{PCH Pm Pwr Btn Override Period} TYPE:{EditNum, HEX, (0x0,0xFF)}
  # !BSF HELP:{PCH power button override period. 000b-4s, 001b-6s, 010b-8s, 011b-10s, 100b-12s, 101b-14s.}
  gPlatformFspPkgTokenSpaceGuid.PchPmPwrBtnOverridePeriod   | * | 0x01 | 0x00

  # !BSF NAME:{PCH Pm Disable Dsx Ac Present Pulldown} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{When Disable, PCH will internal pull down AC_PRESENT in deep SX and during G3 exit.}
  gPlatformFspPkgTokenSpaceGuid.PchPmDisableDsxAcPresentPulldown  | * | 0x01 | 0x00

  # !BSF NAME:{PCH Pm Disable Native Power Button} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Power button native mode disable.}
  gPlatformFspPkgTokenSpaceGuid.PchPmDisableNativePowerButton  | * | 0x01 | 0x00

  # !BSF NAME:{PCH Pm ME_WAKE_STS} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Clear the ME_WAKE_STS bit in the Power and Reset Status (PRSTS) register.}
  gPlatformFspPkgTokenSpaceGuid.PchPmMeWakeSts              | * | 0x01 | 0x01

  # !BSF NAME:{PCH Pm WOL_OVR_WK_STS} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Clear the WOL_OVR_WK_STS bit in the Power and Reset Status (PRSTS) register.}
  gPlatformFspPkgTokenSpaceGuid.PchPmWolOvrWkSts            | * | 0x01 | 0x01

  # !BSF NAME:{PCH Pm Reset Power Cycle Duration} TYPE:{EditNum, HEX, (0x0,0xFF)}
  # !BSF HELP:{Could be customized in the unit of second. Please refer to EDS for all support settings. 0 is default, 1 is 1 second, 2 is 2 seconds, ...}
  gPlatformFspPkgTokenSpaceGuid.PchPmPwrCycDur              | * | 0x01 | 0x00

  # !BSF NAME:{PCH Pm Pcie Pll Ssc} TYPE:{EditNum, HEX, (0x0,0xFF)}
  # !BSF HELP:{Specifies the Pcie Pll Spread Spectrum Percentage. The default is 0xFF: AUTO - No BIOS override.}
  gPlatformFspPkgTokenSpaceGuid.PchPmPciePllSsc             | * | 0x01 | 0xFF

  # !BSF NAME:{PCH Legacy IO Low Latency Enable} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Set to enable low latency of legacy IO. <b>0: Disable</b>, 1: Enable}
  gPlatformFspPkgTokenSpaceGuid.PchLegacyIoLowLatency       | * | 0x01 | 0x00

  # !BSF NAME:{PCH Sata Pwr Opt Enable} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{SATA Power Optimizer on PCH side.}
  gPlatformFspPkgTokenSpaceGuid.SataPwrOptEnable            | * | 0x01 | 0x00

  # !BSF NAME:{PCH Sata eSATA Speed Limit} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{When enabled, BIOS will configure the PxSCTL.SPD to 2 to limit the eSATA port speed.}
  gPlatformFspPkgTokenSpaceGuid.EsataSpeedLimit             | * | 0x01 | 0x00

  # !BSF NAME:{PCH Sata Speed Limit} TYPE:{EditNum, HEX, (0x0,0xFF)}
  # !BSF HELP:{Indicates the maximum speed the SATA controller can support 0h: PchSataSpeedDefault.}
  gPlatformFspPkgTokenSpaceGuid.SataSpeedLimit              | * | 0x01 | 0x00

  # !BSF NAME:{Enable SATA Port HotPlug} TYPE:{EditNum, HEX, (0x00,0xFFFFFFFFFFFFFFFF)}
  # !BSF HELP:{Enable SATA Port HotPlug.}
  gPlatformFspPkgTokenSpaceGuid.SataPortsHotPlug            | * | 0x08 | { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00}

  # !BSF NAME:{Enable SATA Port Interlock Sw} TYPE:{EditNum, HEX, (0x00,0xFFFFFFFFFFFFFFFF)}
  # !BSF HELP:{Enable SATA Port Interlock Sw.}
  gPlatformFspPkgTokenSpaceGuid.SataPortsInterlockSw        | * | 0x08 | { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00}

  # !BSF NAME:{Enable SATA Port External} TYPE:{EditNum, HEX, (0x00,0xFFFFFFFFFFFFFFFF)}
  # !BSF HELP:{Enable SATA Port External.}
  gPlatformFspPkgTokenSpaceGuid.SataPortsExternal           | * | 0x08 | { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00}

  # !BSF NAME:{Enable SATA Port SpinUp} TYPE:{EditNum, HEX, (0x00,0xFFFFFFFFFFFFFFFF)}
  # !BSF HELP:{Enable the COMRESET initialization Sequence to the device.}
  gPlatformFspPkgTokenSpaceGuid.SataPortsSpinUp             | * | 0x08 | { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00}

  # !BSF NAME:{Enable SATA Port Solid State Drive} TYPE:{EditNum, HEX, (0x00,0xFFFFFFFFFFFFFFFF)}
  # !BSF HELP:{0: HDD; 1: SSD.}
  gPlatformFspPkgTokenSpaceGuid.SataPortsSolidStateDrive    | * | 0x08 | { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00}

  # !BSF NAME:{Enable SATA Port Enable Dito Config} TYPE:{EditNum, HEX, (0x00,0xFFFFFFFFFFFFFFFF)}
  # !BSF HELP:{Enable DEVSLP Idle Timeout settings (DmVal, DitoVal).}
  gPlatformFspPkgTokenSpaceGuid.SataPortsEnableDitoConfig   | * | 0x08 | { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00}

  # !BSF NAME:{Enable SATA Port DmVal} TYPE:{EditNum, HEX, (0x00,0xFFFFFFFFFFFFFFFF)}
  # !BSF HELP:{DITO multiplier. Default is 15.}
  gPlatformFspPkgTokenSpaceGuid.SataPortsDmVal              | * | 0x08 | { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00}

  # !BSF NAME:{Enable SATA Port DmVal} TYPE:{EditNum, HEX, (0x00,0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF)}
  # !HDR STRUCT:{UINT16}
  # !BSF HELP:{DEVSLP Idle Timeout (DITO), Default is 625.}
  gPlatformFspPkgTokenSpaceGuid.SataPortsDitoVal            | * | 0x10 | { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00}

  # !BSF NAME:{Enable SATA Port ZpOdd} TYPE:{EditNum, HEX, (0x00,0xFFFFFFFFFFFFFFFF)}
  # !BSF HELP:{Support zero power ODD.}
  gPlatformFspPkgTokenSpaceGuid.SataPortsZpOdd              | * | 0x08 | { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00}

  # !BSF NAME:{PCH Sata Rst Raid Alternate Id} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Enable RAID Alternate ID.}
  gPlatformFspPkgTokenSpaceGuid.SataRstRaidDeviceId         | * | 0x01 | 0x00

  # !BSF NAME:{PCH Sata Rst Pcie Storage Remap enable} TYPE:{EditNum, HEX, (0x00,0xFFFFFF)}
  # !BSF HELP:{Enable Intel RST for PCIe Storage remapping.}
  gPlatformFspPkgTokenSpaceGuid.SataRstPcieEnable           | * | 0x03 | { 0x00, 0x00, 0x00 }

  # !BSF NAME:{PCH Sata Rst Pcie Storage Port} TYPE:{EditNum, HEX, (0x00,0xFFFFFF)}
  # !BSF HELP:{Intel RST for PCIe Storage remapping - PCIe Port Selection (1-based, 0 = autodetect).}
  gPlatformFspPkgTokenSpaceGuid.SataRstPcieStoragePort      | * | 0x03 | { 0x00, 0x00, 0x00 }

  # !BSF NAME:{PCH Sata Rst Pcie Device Reset Delay} TYPE:{EditNum, HEX, (0x00,0xFFFFFF)}
  # !BSF HELP:{PCIe Storage Device Reset Delay in milliseconds. Default value is 100ms}
  gPlatformFspPkgTokenSpaceGuid.SataRstPcieDeviceResetDelay | * | 0x03 | { 100, 100, 100 }

  # !BSF NAME:{UFS enable/disable} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF_HELP:{Enable/Disable UFS controller}
  gPlatformFspPkgTokenSpaceGuid.UfsEnable                   | * | 0x02 | {0, 0}

  # !BSF NAME:{IEH Mode} TYPE:{Combo} OPTION:{0: Bypass, 1:Enable}
  # !BSF HELP:{Integrated Error Handler Mode, 0: Bypass, 1: Enable}
  gPlatformFspPkgTokenSpaceGuid.IehMode | * | 0x01 | 0x00

!if gSiPkgTokenSpaceGuid.PcdEmbeddedEnable == 0x1
  # !BSF NAME:{PSF Tcc} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Psf Tcc (Time Coordinated Computing) Enable will decrease psf transaction latency by disable some psf power management features, 0: Disable, 1: Enable}
  gPlatformFspPkgTokenSpaceGuid.PsfTccEnable | * | 0x01 | 0x00
  # !BSF NAME:{Fusa Display Configuration} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Fusa (Functional Safety) Enable Fusa Feature on Display, 0: Disable, 1: Enable}
  gPlatformFspPkgTokenSpaceGuid.DisplayFusaConfigEnable     | * | 0x01 | 0x01
  # !BSF NAME:{Fusa Graphics Configuration} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Fusa (Functional Safety) Enable Fusa Feature on Graphics, 0: Disable, 1: Enable}
  gPlatformFspPkgTokenSpaceGuid.GraphicFusaConfigEnable     | * | 0x01 | 0x01
  # !BSF NAME:{Fusa Opio Configuration} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Fusa (Functional Safety) Enable Fusa Feature on Opio, 0: Disable, 1: Enable}
  gPlatformFspPkgTokenSpaceGuid.OpioFusaConfigEnable        | * | 0x01 | 0x01
  # !BSF NAME:{Fusa IOP Configuration} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Fusa (Functional Safety) Enable Fusa Feature on IOP, 0: Disable, 1: Enable}
  gPlatformFspPkgTokenSpaceGuid.IopFusaConfigEnable         | * | 0x01 | 0x00
  # !BSF NAME:{Fusa Psf Configuration} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Fusa (Functional Safety) Enable Fusa Feature on Psf, 0: Disable, 1: Enable}
  gPlatformFspPkgTokenSpaceGuid.PsfFusaConfigEnable         | * | 0x01 | 0x00
  # !BSF NAME:{Fusa Configuration} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Fusa (Functional Safety) Enable Fusa Feature, 0: Disable, 1: Enable}
  gPlatformFspPkgTokenSpaceGuid.FusaConfigEnable            | * | 0x01 | 0x00
  # !BSF NAME:{Fusa Run Start Up Array BIST}
  # !BSF TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Enabling this will execute startup array test during boot, 0: Disable, 1: Enable}
  gPlatformFspPkgTokenSpaceGuid.FusaRunStartupArrayBist     | * | 0x01 | 0x00
  # !BSF NAME:{Fusa Run Start Up Scan BIST}
  # !BSF TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Enabling this will execute startup scan test during boot, 0: Disable, 1: Enable}
  gPlatformFspPkgTokenSpaceGuid.FusaRunStartupScanBist      | * | 0x01 | 0x00
  # !BSF NAME:{Fusa Run Periodic Scan BIST}
  # !BSF TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Enabling this will execute periodic scan test during boot, 0: Disable, 1: Enable}
  gPlatformFspPkgTokenSpaceGuid.FusaRunPeriodicScanBist     | * | 0x01 | 0x00
  # !BSF NAME:{Fusa Module 0 Lockstep Configuration}
  # !BSF TYPE:{Combo} OPTION:{0: Disable lockstep, 1: Enable lockstep for Core 0 with Core 1 and Core 2 with Core 3, 2: Enable lockstep for Core 0 with Core 1, 3: Enable lockstep for Core 2 with Core 3}
  # !BSF HELP:{Enable/Disable Lockstep for Atom module 0, which has 4 cores; 0: Disable lockstep; 1: Enable lockstep for Core 0 with Core 1, Core 2 with Core 3; 2: Enable lockstep for Core 0 with Core 1; 3: Enable lockstep for Core 2 with Core 3}
  gPlatformFspPkgTokenSpaceGuid.Module0Lockstep             | * | 0x01 | 0x00
  # !BSF NAME:{Fusa Module 1 Lockstep Configuration}
  # !BSF TYPE:{Combo} OPTION:{0: Disable lockstep, 1: Enable lockstep for Core 0 with Core 1 and Core 2 with Core 3, 2: Enable lockstep for Core 0 with Core 1, 3: Enable lockstep for Core 2 with Core 3}
  # !BSF HELP:{Enable/Disable Lockstep for Atom module 1, which has 4 cores; 0: Disable lockstep; 1: Enable lockstep for Core 0 with Core 1, Core 2 with Core 3; 2: Enable lockstep for Core 0 with Core 1; 3: Enable lockstep for Core 2 with Core 3}
  gPlatformFspPkgTokenSpaceGuid.Module1Lockstep             | * | 0x01 | 0x00
!endif

  # !BSF NAME:{Thermal Throttling Custimized T0Level Value} TYPE:{EditNum, HEX, (0x00,0xFFFF)}
  # !BSF HELP:{Custimized T0Level value.}
  gPlatformFspPkgTokenSpaceGuid.PchT0Level                  | * | 0x02 | 0x0000

  # !BSF NAME:{Thermal Throttling Custimized T1Level Value} TYPE:{EditNum, HEX, (0x00,0xFFFF)}
  # !BSF HELP:{Custimized T1Level value.}
  gPlatformFspPkgTokenSpaceGuid.PchT1Level                  | * | 0x02 | 0x0000

  # !BSF NAME:{Thermal Throttling Custimized T2Level Value} TYPE:{EditNum, HEX, (0x00,0xFFFF)}
  # !BSF HELP:{Custimized T2Level value.}
  gPlatformFspPkgTokenSpaceGuid.PchT2Level                  | * | 0x02 | 0x0000

  # !BSF NAME:{Enable The Thermal Throttle} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Enable the thermal throttle function.}
  gPlatformFspPkgTokenSpaceGuid.PchTTEnable                 | * | 0x01 | 0x00

  # !BSF NAME:{PMSync State 13} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{When set to 1 and the programmed GPIO pin is a 1, then PMSync state 13 will force at least T2 state.}
  gPlatformFspPkgTokenSpaceGuid.PchTTState13Enable          | * | 0x01 | 0x00

  # !BSF NAME:{Thermal Throttle Lock} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Thermal Throttle Lock.}
  gPlatformFspPkgTokenSpaceGuid.PchTTLock                   | * | 0x01 | 0x00

  # !BSF NAME:{Thermal Throttling Suggested Setting} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Thermal Throttling Suggested Setting.}
  gPlatformFspPkgTokenSpaceGuid.TTSuggestedSetting          | * | 0x01 | 0x01

  # !BSF NAME:{Enable PCH Cross Throttling} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Enable/Disable PCH Cross Throttling}
  gPlatformFspPkgTokenSpaceGuid.TTCrossThrottling           | * | 0x01 | 0x01

  # !BSF NAME:{DMI Thermal Sensor Autonomous Width Enable} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{DMI Thermal Sensor Autonomous Width Enable.}
  gPlatformFspPkgTokenSpaceGuid.PchDmiTsawEn                | * | 0x01 | 0x00

  # !BSF NAME:{DMI Thermal Sensor Suggested Setting} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{DMT thermal sensor suggested representative values.}
  gPlatformFspPkgTokenSpaceGuid.DmiSuggestedSetting         | * | 0x01 | 0x01

  # !BSF NAME:{Thermal Sensor 0 Target Width} TYPE:{Combo}
  # !BSF OPTION:{0:x1, 1:x2, 2:x4, 3:x8, 4:x16}
  # !BSF HELP:{Thermal Sensor 0 Target Width.}
  gPlatformFspPkgTokenSpaceGuid.DmiTS0TW                    | * | 0x01 | 0x03

  # !BSF NAME:{Thermal Sensor 1 Target Width} TYPE:{Combo}
  # !BSF OPTION:{0:x1, 1:x2, 2:x4, 3:x8, 4:x16}
  # !BSF HELP:{Thermal Sensor 1 Target Width.}
  gPlatformFspPkgTokenSpaceGuid.DmiTS1TW                    | * | 0x01 | 0x02

  # !BSF NAME:{Thermal Sensor 2 Target Width} TYPE:{Combo}
  # !BSF OPTION:{0:x1, 1:x2, 2:x4, 3:x8, 4:x16}
  # !BSF HELP:{Thermal Sensor 2 Target Width.}
  gPlatformFspPkgTokenSpaceGuid.DmiTS2TW                    | * | 0x01 | 0x01

  # !BSF NAME:{Thermal Sensor 3 Target Width} TYPE:{Combo}
  # !BSF OPTION:{0:x1, 1:x2, 2:x4, 3:x8, 4:x16}
  # !BSF HELP:{Thermal Sensor 3 Target Width.}
  gPlatformFspPkgTokenSpaceGuid.DmiTS3TW                    | * | 0x01 | 0x00

  # !BSF NAME:{Port 0 T1 Multipler} TYPE:{EditNum, HEX, (0x00,0xFF)}
  # !BSF HELP:{Port 0 T1 Multipler.}
  gPlatformFspPkgTokenSpaceGuid.SataP0T1M                   | * | 0x01 | 0x01

  # !BSF NAME:{Port 0 T2 Multipler} TYPE:{EditNum, HEX, (0x00,0xFF)}
  # !BSF HELP:{Port 0 T2 Multipler.}
  gPlatformFspPkgTokenSpaceGuid.SataP0T2M                   | * | 0x01 | 0x02

  # !BSF NAME:{Port 0 T3 Multipler} TYPE:{EditNum, HEX, (0x00,0xFF)}
  # !BSF HELP:{Port 0 T3 Multipler.}
  gPlatformFspPkgTokenSpaceGuid.SataP0T3M                   | * | 0x01 | 0x03

  # !BSF NAME:{Port 0 Tdispatch} TYPE:{EditNum, HEX, (0x00,0xFF)}
  # !BSF HELP:{Port 0 Tdispatch.}
  gPlatformFspPkgTokenSpaceGuid.SataP0TDisp                 | * | 0x01 | 0x00

  # !BSF NAME:{Port 1 T1 Multipler} TYPE:{EditNum, HEX, (0x00,0xFF)}
  # !BSF HELP:{Port 1 T1 Multipler.}
  gPlatformFspPkgTokenSpaceGuid.SataP1T1M                   | * | 0x01 | 0x01

  # !BSF NAME:{Port 1 T2 Multipler} TYPE:{EditNum, HEX, (0x00,0xFF)}
  # !BSF HELP:{Port 1 T2 Multipler.}
  gPlatformFspPkgTokenSpaceGuid.SataP1T2M                   | * | 0x01 | 0x02

  # !BSF NAME:{Port 1 T3 Multipler} TYPE:{EditNum, HEX, (0x00,0xFF)}
  # !BSF HELP:{Port 1 T3 Multipler.}
  gPlatformFspPkgTokenSpaceGuid.SataP1T3M                   | * | 0x01 | 0x03

  # !BSF NAME:{Port 1 Tdispatch} TYPE:{EditNum, HEX, (0x00,0xFF)}
  # !BSF HELP:{Port 1 Tdispatch.}
  gPlatformFspPkgTokenSpaceGuid.SataP1TDisp                 | * | 0x01 | 0x00

  # !BSF NAME:{Port 0 Tinactive} TYPE:{EditNum, HEX, (0x00,0xFF)}
  # !BSF HELP:{Port 0 Tinactive.}
  gPlatformFspPkgTokenSpaceGuid.SataP0Tinact                | * | 0x01 | 0x00

  # !BSF NAME:{Port 0 Alternate Fast Init Tdispatch} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Port 0 Alternate Fast Init Tdispatch.}
  gPlatformFspPkgTokenSpaceGuid.SataP0TDispFinit            | * | 0x01 | 0x00

  # !BSF NAME:{Port 1 Tinactive} TYPE:{EditNum, HEX, (0x00,0xFF)}
  # !BSF HELP:{Port 1 Tinactive.}
  gPlatformFspPkgTokenSpaceGuid.SataP1Tinact                | * | 0x01 | 0x00

  # !BSF NAME:{Port 1 Alternate Fast Init Tdispatch} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Port 1 Alternate Fast Init Tdispatch.}
  gPlatformFspPkgTokenSpaceGuid.SataP1TDispFinit            | * | 0x01 | 0x00

  # !BSF NAME:{Sata Thermal Throttling Suggested Setting} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Sata Thermal Throttling Suggested Setting.}
  gPlatformFspPkgTokenSpaceGuid.SataThermalSuggestedSetting | * | 0x01 | 0x01

  # !BSF NAME:{Enable Memory Thermal Throttling} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Enable Memory Thermal Throttling.}
  gPlatformFspPkgTokenSpaceGuid.PchMemoryThrottlingEnable   | * | 0x01 | 0x00

  # !BSF NAME:{Memory Thermal Throttling} TYPE:{EditNum, HEX, (0x00,0xFFFF)}
  # !BSF HELP:{Enable Memory Thermal Throttling.}
  gPlatformFspPkgTokenSpaceGuid.PchMemoryPmsyncEnable       | * | 0x02 | { 0x00, 0x00 }

  # !BSF NAME:{Enable Memory Thermal Throttling} TYPE:{EditNum, HEX, (0x00,0xFFFF)}
  # !BSF HELP:{Enable Memory Thermal Throttling.}
  gPlatformFspPkgTokenSpaceGuid.PchMemoryC0TransmitEnable   | * | 0x02 | { 0x00, 0x00 }

  # !BSF NAME:{Enable Memory Thermal Throttling} TYPE:{EditNum, HEX, (0x00,0xFFFF)}
  # !BSF HELP:{Enable Memory Thermal Throttling.}
  gPlatformFspPkgTokenSpaceGuid.PchMemoryPinSelection       | * | 0x02 | { 0x00, 0x00 }

  # !BSF NAME:{Thermal Device Temperature} TYPE:{EditNum, HEX, (0x00,0xFFFF)}
  # !BSF HELP:{Decides the temperature.}
  gPlatformFspPkgTokenSpaceGuid.PchTemperatureHotLevel      | * | 0x02 | 0x0073

  # !BSF NAME:{USB2 Port Over Current Pin} TYPE:{EditNum, HEX, (0x00,0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF)}
  # !BSF HELP:{Describe the specific over current pin number of USB 2.0 Port N.}
  gPlatformFspPkgTokenSpaceGuid.Usb2OverCurrentPin          | * | 0x10 | { 0x00, 0x00, 0x01, 0x01, 0x02, 0x02, 0x03, 0x03, 0x04, 0x04, 0x05, 0x05, 0x06, 0x06, 0x07, 0x07 }

  # !BSF NAME:{USB3 Port Over Current Pin} TYPE:{EditNum, HEX, (0x00,0xFFFFFFFFFFFFFFFFFFFF)}
  # !BSF HELP:{Describe the specific over current pin number of USB 3.0 Port N.}
  gPlatformFspPkgTokenSpaceGuid.Usb3OverCurrentPin          | * | 0x0A | { 0x00, 0x00, 0x01, 0x01, 0x02, 0x02, 0x03, 0x03, 0x04, 0x04 }

  # !BSF NAME:{Enable xHCI LTR override} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Enables override of recommended LTR values for xHCI}
  gPlatformFspPkgTokenSpaceGuid.PchUsbLtrOverrideEnable         | * | 0x01 | 0x00

  # !BSF NAME:{Touch Host Controller Mode} TYPE:{EditNum, HEX, (0,0xFFFF)}
  # !BSF HELP:{Switch between Intel THC protocol and Industry standard HID Over SPI protocol. 0x0:Thc, 0x1:Hid}
  gPlatformFspPkgTokenSpaceGuid.ThcMode                         | * | 0x02 | {0x0, 0x0}

  # !BSF NAME:{xHCI High Idle Time LTR override} TYPE:{EditNum, HEX, (0x00,0xFFFFFFFF)}
  # !BSF HELP:{Value used for overriding LTR recommendation for xHCI High Idle Time LTR setting}
  gPlatformFspPkgTokenSpaceGuid.PchUsbLtrHighIdleTimeOverride   | * | 0x04 | 0x00000000

  # !BSF NAME:{xHCI Medium Idle Time LTR override} TYPE:{EditNum, HEX, (0x00,0xFFFFFFFF)}
  # !BSF HELP:{Value used for overriding LTR recommendation for xHCI Medium Idle Time LTR setting}
  gPlatformFspPkgTokenSpaceGuid.PchUsbLtrMediumIdleTimeOverride | * | 0x04 | 0x00000000

  # !BSF NAME:{xHCI Low Idle Time LTR override} TYPE:{EditNum, HEX, (0x00,0xFFFFFFFF)}
  # !BSF HELP:{Value used for overriding LTR recommendation for xHCI Low Idle Time LTR setting}
  gPlatformFspPkgTokenSpaceGuid.PchUsbLtrLowIdleTimeOverride    | * | 0x04 | 0x00000000

  # !BSF NAME:{Enable 8254 Static Clock Gating} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Set 8254CGE=1 is required for SLP_S0 support. However, set 8254CGE=1 in POST time might fail to boot legacy OS using 8254 timer. Make sure it is disabled to support legacy OS using 8254 timer. Also enable this while S0ix is enabled.}
  gPlatformFspPkgTokenSpaceGuid.Enable8254ClockGating       | * | 0x01 | 0x01

  # !BSF NAME:{Enable 8254 Static Clock Gating On S3} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{This is only applicable when Enable8254ClockGating is disabled. FSP will do the 8254 CGE programming on S3 resume when Enable8254ClockGatingOnS3 is enabled. This avoids the SMI requirement for the programming.}
  gPlatformFspPkgTokenSpaceGuid.Enable8254ClockGatingOnS3   | * | 0x01 | 0x01

  # !BSF NAME:{Enable TCO timer.} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{When FALSE, it disables PCH ACPI timer, and stops TCO timer. NOTE: This will have huge power impact when it's enabled. If TCO timer is disabled, uCode ACPI timer emulation must be enabled, and WDAT table must not be exposed to the OS.}
  gPlatformFspPkgTokenSpaceGuid.EnableTcoTimer              | * | 0x01 | 0x00

!if gSiPkgTokenSpaceGuid.PcdEmbeddedEnable == 0x1
  # !BSF NAME:{Enable Timed GPIO 0.} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{When FALSE, it disables Timed GPIO 0.}
  gPlatformFspPkgTokenSpaceGuid.EnableTimedGpio0            | * | 0x01 | 0x00

  # !BSF NAME:{Enable Timed GPIO 1.} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{When FALSE, it disables Timed GPIO 1.}
  gPlatformFspPkgTokenSpaceGuid.EnableTimedGpio1            | * | 0x01 | 0x00
!endif

  # !BSF NAME:{Hybrid Storage Detection and Configuration Mode} TYPE:{Combo} OPTION:{0: Disabled, 1: Dynamic Configuration}
  # !BSF HELP:{Enables support for Hybrid storage devices. 0: Disabled; 1: Dynamic Configuration. Default is 0: Disabled}
  gPlatformFspPkgTokenSpaceGuid.HybridStorageMode         | * | 0x01 | 0x00

  # !BSF NAME:{CPU Root Port used for Hybrid Storage} TYPE:{EditNum, HEX, (0x0,0xFF)}
  # !BSF HELP:{Specifies the CPU root port used for Hybrid storage.}
  gPlatformFspPkgTokenSpaceGuid.CpuRootportUsedForHybridStorage         | * | 0x01 | 0xFF

  # !BSF NAME:{PCH Root Port used for Hybrid Storage when two lanes are connected to CPU} TYPE:{EditNum, HEX, (0x0,0xFF)}
  # !BSF HELP:{Specifies PCH Root Port used for Hybrid Storage when two lanes are connected to CPU.}
  gPlatformFspPkgTokenSpaceGuid.PchRootportUsedForCpuAttach         | * | 0x01 | 0xFF

  # !BSF NAME:{PCH GPE event handler} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Enabled _L6D ACPI handler. PME GPE is shared by multiple devices So BIOS must verify the same in the ASL handler by reading offset for PMEENABLE and PMESTATUS bit}
  gPlatformFspPkgTokenSpaceGuid.PchAcpiL6dPmeHandling               | * | 0x01 | 0x0

  #
  #  PCH Silicon Offset End
  #

  ###########Security Production Policies Start ###########################################

  # Bios Guard
  # !BSF PAGE:{CPU2}
  # !BSF NAME:{BgpdtHash[4]} TYPE:{EditNum, HEX, (0x00,0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF)}
  # !HDR STRUCT:{UINT64}
  # !BSF HELP:{BgpdtHash values}
  gPlatformFspPkgTokenSpaceGuid.BgpdtHash                   | * | 0x20 | {0x0000000000000000, 0x0000000000000000, 0x0000000000000000, 0x0000000000000000}

  # !BSF NAME:{BiosGuardAttr} TYPE:{EditNum, HEX, (0x00,0xFFFFFFFF)}
  # !BSF HELP:{BiosGuardAttr default values}
  gPlatformFspPkgTokenSpaceGuid.BiosGuardAttr               | * | 0x4 | 0xFFFFFFFF

  # !BSF NAME:{BiosGuardModulePtr} TYPE:{EditNum, HEX, (0x00,0xFFFFFFFFFFFFFFFF)}
  # !BSF HELP:{BiosGuardModulePtr default values}
  gPlatformFspPkgTokenSpaceGuid.BiosGuardModulePtr          | * | 0x8 | 0xFFFFFFFFFFFFFFFF

  # !BSF PAGE:{SA2}
  # !BSF NAME:{SendEcCmd} TYPE:{EditNum, HEX, (0x00,0xFFFFFFFFFFFFFFFF)}
  # !BSF HELP:{SendEcCmd function pointer. \n @code typedef EFI_STATUS (EFIAPI *PLATFORM_SEND_EC_COMMAND) (IN EC_COMMAND_TYPE  EcCmdType, IN UINT8  EcCmd, IN UINT8  SendData, IN OUT UINT8  *ReceiveData); @endcode}
  gPlatformFspPkgTokenSpaceGuid.SendEcCmd                   | * | 0x8 | 0xFFFFFFFFFFFFFFFF

  # !BSF NAME:{EcCmdProvisionEav} TYPE:{EditNum, HEX, (0x00,0xFF)}
  # !BSF HELP:{Ephemeral Authorization Value default values. Provisions an ephemeral shared secret to the EC}
  gPlatformFspPkgTokenSpaceGuid.EcCmdProvisionEav           | * | 0x1 | 0xFF

  # !BSF NAME:{EcCmdLock} TYPE:{EditNum, HEX, (0x00,0xFF)}
  # !BSF HELP:{EcCmdLock default values. Locks Ephemeral Authorization Value sent previously}
  gPlatformFspPkgTokenSpaceGuid.EcCmdLock                   | * | 0x1 | 0xFF

  ###########Security Production Policies End ###########################################

  #
  #  SiConfig Silicon Policies Start
  # !BSF NAME:{Skip Ssid Programming.} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{When set to TRUE, silicon code will not do any SSID programming and platform code needs to handle that by itself properly.}
  gPlatformFspPkgTokenSpaceGuid.SiSkipSsidProgramming    | * | 0x01 | 0x00

  # !BSF NAME:{Change Default SVID} TYPE:{EditNum, HEX, (0x00, 0xFFFF)}
  # !BSF HELP:{Change the default SVID used in FSP to programming internal devices. This is only valid when SkipSsidProgramming is FALSE.}
  gPlatformFspPkgTokenSpaceGuid.SiCustomizedSvid    | * | 0x02 | 0x0000

  # !BSF NAME:{Change Default SSID} TYPE:{EditNum, HEX, (0x00, 0xFFFF)}
  # !BSF HELP:{Change the default SSID used in FSP to programming internal devices. This is only valid when SkipSsidProgramming is FALSE.}
  gPlatformFspPkgTokenSpaceGuid.SiCustomizedSsid    | * | 0x02 | 0x0000

  #
  # typedef struct {
  #   UINT16  SubSystemVendorId; // If giving 0, FSP will program default for the device.
  #   UINT16  SubSystemId;       // If giving 0, FSP will program default for the device.
  # } SVID_SID_VALUE;
  #
  # typedef struct {
  #   union {
  #     struct {
  #       UINT64  Register:12;   // SSID register offset
  #       UINT64  Function:3;
  #       UINT64  Device:5;
  #       UINT64  Bus:8;
  #       UINT64  Reserved1:4;
  #       UINT64  Segment:16;
  #       UINT64  Reserved2:16;
  #     } Bits;
  #     UINT64    SegBusDevFuncRegister;
  #   } Address;
  #   SVID_SID_VALUE SvidSidValue;
  #   UINT32 Reserved;
  # } SVID_SID_INIT_ENTRY;
  #
  # Example table structure:
  # SVID_SID_INIT_ENTRY mSsidTablePtr[] = {
  #   {{{Register, Function, Device, Bus, Reserved1, Segment, Reserved2}}, {SubSystemVendorId, SubSystemId}, Reserved},
  #   {{{       0,        5,     31,   0,         0,       0,         0}}, {           0x1234,      0x5678}        ,0}
  # }
  #
  # Note:
  # In the table, only Device, Function, SubSystemVendorId and SubSystemId fields are required.
  # When both SiSsidTablePtr and SiNumberOfSsidTableEntry are non-zero, FSP will follow the table to program SSID for corresponding devices (either given value or default value).
  #
  # !BSF NAME:{SVID SDID table Poniter.} TYPE:{EditNum, HEX, (0x00, 0xFFFFFFFF)}
  # !BSF HELP:{The address of the table of SVID SDID to customize each SVID SDID entry. This is only valid when SkipSsidProgramming is FALSE.}
  gPlatformFspPkgTokenSpaceGuid.SiSsidTablePtr              | * | 0x04 | 0x00000000

  # !BSF NAME:{Number of ssid table.} TYPE:{EditNum, HEX, (0x00, 0xFFFF)}
  # !BSF HELP:{SiNumberOfSsidTableEntry should match the table entries created in SiSsidTablePtr. This is only valid when SkipSsidProgramming is FALSE.}
  gPlatformFspPkgTokenSpaceGuid.SiNumberOfSsidTableEntry    | * | 0x02 | 0x0000

  # !BSF NAME:{USB2 Port Reset Message Enable} TYPE:{EditNum, HEX, (0x00,0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF)}
  # !BSF HELP:{0: Disable USB2 Port Reset Message; 1: Enable USB2 Port Reset Message; This must be enable for USB2 Port those are paired with CPU XHCI Port}
  gPlatformFspPkgTokenSpaceGuid.PortResetMessageEnable      | * | 0x10 | { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 }

  # !BSF NAME:{SATA RST Interrupt Mode} TYPE:{Combo} OPTION:{0:Msix, 1:Msi, 2:Legacy}
  # !BSF HELP:{Allowes to choose which interrupts will be implemented by SATA controller in RAID mode.}
  gPlatformFspPkgTokenSpaceGuid.SataRstInterrupt            | * | 0x01 | 0x00
  #
  #  SiConfig Silicon Policies End
  #

  #
  # ME Post-Mem Production Block Start
  #
  # !BSF PAGE:{ME2}
  # !BSF NAME:{ME Unconfig on RTC clear}
  # !BSF TYPE:{Combo} OPTION:{0: Disable ME Unconfig On Rtc Clear, 1: Enable ME Unconfig On Rtc Clear, 2: Cmos is clear, 3: Reserved}
  # !BSF HELP:{0: Disable ME Unconfig On Rtc Clear. <b>1: Enable ME Unconfig On Rtc Clear</b>. 2: Cmos is clear, status unkonwn. 3: Reserved}
  gPlatformFspPkgTokenSpaceGuid.MeUnconfigOnRtcClear        | * | 0x01 | 0x01

  # !BSF NAME:{Enforce Enhanced Debug Mode} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Determine if ME should enter Enhanced Debug Mode. <b>0: disable</b>, 1: enable}
  gPlatformFspPkgTokenSpaceGuid.EnforceEDebugMode           | * | 0x01 | 0x00
  #
  # ME Post-Mem Production Block End
  #

  # !BSF PAGE:{PCH2}
  # !BSF NAME:{Enable PS_ON.} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{PS_ON is a new C10 state from the CPU on desktop SKUs that enables a lower power target that will be required by the California Energy Commission (CEC). When FALSE, PS_ON is to be disabled.}
  gPlatformFspPkgTokenSpaceGuid.PsOnEnable                  | * | 0x01 | 0x00

  # !BSF NAME:{Pmc Cpu C10 Gate Pin Enable} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Enable/Disable platform support for CPU_C10_GATE# pin to control gating of CPU VccIO and VccSTG rails instead of SLP_S0# pin.}
  gPlatformFspPkgTokenSpaceGuid.PmcCpuC10GatePinEnable      | * | 0x01 | 0x01

  # !BSF NAME:{Pch Dmi Aspm Ctrl} TYPE:{Combo}
  # !BSF OPTION:{0:Disabled, 1:L0s, 2:L1, 3:L0sL1, 4:Auto}
  # !BSF HELP:{ASPM configuration on the PCH side of the DMI/OPI Link. Default is <b>PchPcieAspmL1</b>}
  gPlatformFspPkgTokenSpaceGuid.PchDmiAspmCtrl              | * | 0x01 | 0x02

  # !BSF NAME:{PchDmiCwbEnable} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Central Write Buffer feature configurable and enabled by default}
  gPlatformFspPkgTokenSpaceGuid.PchDmiCwbEnable             | * | 0x1 | 0x01

  # !BSF NAME:{OS IDLE Mode Enable} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Enable/Disable OS Idle Mode}
  gPlatformFspPkgTokenSpaceGuid.PmcOsIdleEnable             | * | 0x01 | 0x01

  # !BSF NAME:{S0ix Auto-Demotion} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Enable/Disable the Low Power Mode Auto-Demotion Host Control feature.}
  gPlatformFspPkgTokenSpaceGuid.PchS0ixAutoDemotion         | * | 0x01 | 0x01

  # !BSF NAME:{Latch Events C10 Exit} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{When this bit is set to 1, SLP_S0# entry events in SLP_S0_DEBUG_REGx registers are captured on C10 exit (instead of C10 entry which is default)}
  gPlatformFspPkgTokenSpaceGuid.PchPmLatchEventsC10Exit     | * | 0x01 | 0x00

  # !BSF NAME:{PMC ADR enable} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Enable/disable asynchronous DRAM refresh}
  gPlatformFspPkgTokenSpaceGuid.PmcAdrEn                    | * | 0x1  | 0x00

  # !BSF NAME:{PMC ADR timer configuration enable} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Enable/disable ADR timer configuration}
  gPlatformFspPkgTokenSpaceGuid.PmcAdrTimerEn               | * | 0x1  | 0x00

  # !BSF NAME:{PMC ADR phase 1 timer value} TYPE:{EditNum, HEX, (0x00, 0xFF)}
  # !BSF HELP:[Specify the timer value phase 1 ADR timer}
  gPlatformFspPkgTokenSpaceGuid.PmcAdrTimer1Val             | * | 0x1  | 0x00

  # !BSF NAME:{PMC ADR phase 1 timer multiplier value} TYPE:{EditNum, HEX, (0x00, 0xFF)}
  # !BSF HELP:{Specify the multiplier value for phase 1 ADR timer}
  gPlatformFspPkgTokenSpaceGuid.PmcAdrMultiplier1Val        | * | 0x1  | 0x00

  # !BSF NAME:{PMC ADR host reset partition enable} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Specify whether PMC should set ADR_RST_STS bit after receiving Reset_Warn_Ack DMI message}
  gPlatformFspPkgTokenSpaceGuid.PmcAdrHostPartitionReset    | * | 0x1  | 0x00

  # !BSF NAME:{PMC ADR source select override enable} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Tells the FSP to update the source select with platform value}
  gPlatformFspPkgTokenSpaceGuid.PmcAdrSrcOverride           | * | 0x1  | 0x00

  # !BSF NAME:{PMC ADR source selection} TYPE:{EditNum, HEX, (0x00, 0xFFFFFFFF)}
  # !BSF HELP:{Specify which sources should cause ADR flow}
  gPlatformFspPkgTokenSpaceGuid.PmcAdrSrcSel                | * | 0x4  | 0x00

  #
  # SA Post-Mem PCIE Policies Start
  #
  # !BSF NAME:{PCIE Eq Ph3 Lane Param Cm} TYPE:{EditNum, HEX, (0x00,0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF)}
  # !BSF HELP:{CPU_PCIE_EQ_LANE_PARAM. Coefficient C-1.}
  gPlatformFspPkgTokenSpaceGuid.CpuPcieEqPh3LaneParamCm        | * | 0x20 | { 0x06, 0x06, 0x06, 0x06, 0x06, 0x06, 0x06, 0x06, 0x06, 0x06, 0x06, 0x06, 0x06, 0x06, 0x06, 0x06, 0x06, 0x06, 0x06, 0x06 }

  # !BSF NAME:{PCIE Eq Ph3 Lane Param Cp} TYPE:{EditNum, HEX, (0x00,0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF)}
  # !BSF HELP:{CPU_PCIE_EQ_LANE_PARAM. Coefficient C+1.}
  gPlatformFspPkgTokenSpaceGuid.CpuPcieEqPh3LaneParamCp        | * | 0x20 | { 0x02, 0x02, 0x02, 0x02, 0x02, 0x02, 0x02, 0x02, 0x02, 0x02, 0x02, 0x02, 0x02, 0x02, 0x02, 0x02, 0x02, 0x02, 0x02, 0x02 }

  # !BSF NAME:{Gen3 Root port preset values per lane} TYPE:{EditNum, HEX, (0x00, 0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF)}
  # !HDR STRUCT:{UINT8}
  # !BSF HELP:{Used for programming Pcie Gen3 preset values per lane. Range: 0-9, 8 is default for each lane}
  gPlatformFspPkgTokenSpaceGuid.CpuPcieGen3RootPortPreset       | * | 0x14 | {0x7, 0x7, 0x7, 0x7, 0x7, 0x7, 0x7, 0x7, 0x7, 0x7, 0x7, 0x7, 0x7, 0x7, 0x7, 0x7, 0x7, 0x7, 0x7, 0x7}

  # !BSF NAME:{Pcie Gen4 Root port preset values per lane} TYPE:{EditNum, HEX, (0x00, 0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF)}
  # !HDR STRUCT:{UINT8}
  # !BSF HELP:{Used for programming Pcie Gen4 preset values per lane. Range: 0-9, 8 is default for each lane}
  gPlatformFspPkgTokenSpaceGuid.CpuPcieGen4RootPortPreset       | * | 0x14 | {0x7, 0x7, 0x7, 0x7, 0x7, 0x7, 0x7, 0x7, 0x7, 0x7, 0x7, 0x7, 0x7, 0x7, 0x7, 0x7, 0x7, 0x7, 0x7, 0x7}

  # !BSF NAME:{Pcie Gen3 End port preset values per lane} TYPE:{EditNum, HEX, (0x00, 0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF)}
  # !HDR STRUCT:{UINT8}
  # !BSF HELP:{Used for programming Pcie Gen3 preset values per lane. Range: 0-9, 7 is default for each lane}
  gPlatformFspPkgTokenSpaceGuid.CpuPcieGen3EndPointPreset       | * | 0x14 | {0x7, 0x7, 0x7, 0x7, 0x7, 0x7, 0x7, 0x7, 0x7, 0x7, 0x7, 0x7, 0x7, 0x7, 0x7, 0x7, 0x7, 0x7, 0x7, 0x7}

  # !BSF NAME:{Pcie Gen4 End port preset values per lane} TYPE:{EditNum, HEX, (0x00, 0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF)}
  # !HDR STRUCT:{UINT8}
  # !BSF HELP:{Used for programming Pcie Gen4 preset values per lane. Range: 0-9, 7 is default for each lane}
  gPlatformFspPkgTokenSpaceGuid.CpuPcieGen4EndPointPreset       | * | 0x14 | {0x7, 0x7, 0x7, 0x7, 0x7, 0x7, 0x7, 0x7, 0x7, 0x7, 0x7, 0x7, 0x7, 0x7, 0x7, 0x7, 0x7, 0x7, 0x7, 0x7}

  # !BSF NAME:{Pcie Gen3 End port Hint values per lane} TYPE:{EditNum, HEX, (0x00, 0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF)}
  # !HDR STRUCT:{UINT8}
  # !BSF HELP:{Used for programming Pcie Gen3 Hint values per lane. Range: 0-6, 2 is default for each lane}
  gPlatformFspPkgTokenSpaceGuid.CpuPcieGen3EndPointHint         | * | 0x14 | {0x2, 0x2, 0x2, 0x2, 0x2, 0x2, 0x2, 0x2, 0x2, 0x2, 0x2, 0x2, 0x2, 0x2, 0x2, 0x2, 0x2, 0x2, 0x2, 0x2}

  # !BSF NAME:{Pcie Gen4 End port Hint values per lane} TYPE:{EditNum, HEX, (0x00, 0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF)}
  # !HDR STRUCT:{UINT8}
  # !BSF HELP:{Used for programming Pcie Gen4 Hint values per lane. Range: 0-6, 2 is default for each lane}
  gPlatformFspPkgTokenSpaceGuid.CpuPcieGen4EndPointHint         | * | 0x14 | {0x2, 0x2, 0x2, 0x2, 0x2, 0x2, 0x2, 0x2, 0x2, 0x2, 0x2, 0x2, 0x2, 0x2, 0x2, 0x2, 0x2, 0x2, 0x2, 0x2}

  # !BSF NAME:{CPU PCIe Fia Programming} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Load Fia configuration if enable. 0: Disable; 1: Enable(Default).}
  gPlatformFspPkgTokenSpaceGuid.CpuPcieFiaProgramming | * | 0x01 | 0x01

  # !BSF NAME:{CPU PCIe RootPort Clock Gating} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Describes whether the PCI Express Clock Gating for each root port is enabled by platform modules. 0: Disable; 1: Enable(Default).}
  gPlatformFspPkgTokenSpaceGuid.CpuPcieClockGating | * | 0x04 | {0x01, 0x01, 0x01, 0x01}

  # !BSF NAME:{CPU PCIe RootPort Power Gating} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Describes whether the PCI Express Power Gating for each root port is enabled by platform modules. 0: Disable; 1: Enable(Default).}
  gPlatformFspPkgTokenSpaceGuid.CpuPciePowerGating | * | 0x04 | {0x01, 0x01, 0x01, 0x01}

  # !BSF NAME:{PCIE Compliance Test Mode} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Compliance Test Mode shall be enabled when using Compliance Load Board.}
  gPlatformFspPkgTokenSpaceGuid.CpuPcieComplianceTestMode      | * | 0x01 | 0x00

  # !BSF NAME:{PCIE Enable Peer Memory Write} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{This member describes whether Peer Memory Writes are enabled on the platform.}
  gPlatformFspPkgTokenSpaceGuid.CpuPcieEnablePeerMemoryWrite   | * | 0x01 | 0x00

  # !BSF NAME:{PCIE Rp Function Swap} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Allows BIOS to use root port function number swapping when root port of function 0 is disabled.}
  gPlatformFspPkgTokenSpaceGuid.CpuPcieRpFunctionSwap          | * | 0x01 | 0x00

  # !BSF NAME:{PCI Express Slot Selection} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Select the PCIe M2 or CEMx4 slot.0: CEMx4 slot; 1: M2 slot(Default).}
  gPlatformFspPkgTokenSpaceGuid.CpuPcieSlotSelection          | * | 0x01 | 0x01

  # !BSF NAME:{CPU PCIE device override table pointer} TYPE:{EditNum, HEX, (0x00,0xFFFFFFFF)}
  # !BSF HELP:{The PCIe device table is being used to override PCIe device ASPM settings. This is a pointer points to a 32bit address. And it's only used in PostMem phase. Please refer to CPU_PCIE_DEVICE_OVERRIDE structure for the table. Last entry VendorId must be 0.}
  gPlatformFspPkgTokenSpaceGuid.CpuPcieDeviceOverrideTablePtr  | * | 0x04 | 0x00000000

  # !BSF NAME:{Enable PCIE RP HotPlug} TYPE:{EditNum, HEX, (0x00,0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF)}
  # !BSF HELP:{Indicate whether the root port is hot plug available.}
  gPlatformFspPkgTokenSpaceGuid.CpuPcieRpHotPlug               | * | 0x4 | { 0x00, 0x00, 0x00, 0x00 }

  # !BSF NAME:{Enable PCIE RP Pm Sci} TYPE:{EditNum, HEX, (0x00,0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF)}
  # !BSF HELP:{Indicate whether the root port power manager SCI is enabled.}
  gPlatformFspPkgTokenSpaceGuid.CpuPcieRpPmSci                 | * | 0x4 | { 0x00, 0x00, 0x00, 0x00 }

  # !BSF NAME:{Enable PCIE RP Transmitter Half Swing} TYPE:{EditNum, HEX, (0x00,0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF)}
  # !BSF HELP:{Indicate whether the Transmitter Half Swing is enabled.}
  gPlatformFspPkgTokenSpaceGuid.CpuPcieRpTransmitterHalfSwing  | * | 0x4 | { 0x00, 0x00, 0x00, 0x00 }

  # !BSF NAME:{PCIE RP Access Control Services Extended Capability} TYPE:{EditNum, HEX, (0x00,0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF)}
  # !BSF HELP:{Enable/Disable PCIE RP Access Control Services Extended Capability}
  gPlatformFspPkgTokenSpaceGuid.CpuPcieRpAcsEnabled           | * | 0x4 | { 0x01, 0x01, 0x01, 0x01 }

  # !BSF NAME:{PCIE RP Clock Power Management} TYPE:{EditNum, HEX, (0x00,0xFFFFFFFF)}
  # !BSF HELP:{Enable/Disable PCIE RP Clock Power Management, even if disabled, CLKREQ# signal can still be controlled by L1 PM substates mechanism}
  gPlatformFspPkgTokenSpaceGuid.CpuPcieRpEnableCpm            | * | 0x04 | { 0x01, 0x01, 0x01, 0x01 }

  # !BSF NAME:{PCIE RP Advanced Error Report} TYPE:{EditNum, HEX, (0x00,0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF)}
  # !BSF HELP:{Indicate whether the Advanced Error Reporting is enabled.}
  gPlatformFspPkgTokenSpaceGuid.CpuPcieRpAdvancedErrorReporting   | * | 0x4 | { 0x01, 0x01, 0x01, 0x01 }

  # !BSF NAME:{PCIE RP Unsupported Request Report} TYPE:{EditNum, HEX, (0x00,0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF)}
  # !BSF HELP:{Indicate whether the Unsupported Request Report is enabled.}
  gPlatformFspPkgTokenSpaceGuid.CpuPcieRpUnsupportedRequestReport | * | 0x4 | { 0x00, 0x00, 0x00, 0x00 }

  # !BSF NAME:{PCIE RP Fatal Error Report} TYPE:{EditNum, HEX, (0x00,0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF)}
  # !BSF HELP:{Indicate whether the Fatal Error Report is enabled.}
  gPlatformFspPkgTokenSpaceGuid.CpuPcieRpFatalErrorReport      | * | 0x4 | { 0x00, 0x00, 0x00, 0x00 }

  # !BSF NAME:{PCIE RP No Fatal Error Report} TYPE:{EditNum, HEX, (0x00,0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF)}
  # !BSF HELP:{Indicate whether the No Fatal Error Report is enabled.}
  gPlatformFspPkgTokenSpaceGuid.CpuPcieRpNoFatalErrorReport    | * | 0x4 | { 0x00, 0x00, 0x00, 0x00 }

  # !BSF NAME:{PCIE RP Correctable Error Report} TYPE:{EditNum, HEX, (0x00,0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF)}
  # !BSF HELP:{Indicate whether the Correctable Error Report is enabled.}
  gPlatformFspPkgTokenSpaceGuid.CpuPcieRpCorrectableErrorReport   | * | 0x4 | { 0x00, 0x00, 0x00, 0x00 }

  # !BSF NAME:{PCIE RP System Error On Fatal Error} TYPE:{EditNum, HEX, (0x00,0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF)}
  # !BSF HELP:{Indicate whether the System Error on Fatal Error is enabled.}
  gPlatformFspPkgTokenSpaceGuid.CpuPcieRpSystemErrorOnFatalError  | * | 0x4 | { 0x00, 0x00, 0x00, 0x00 }

  # !BSF NAME:{PCIE RP System Error On Non Fatal Error} TYPE:{EditNum, HEX, (0x00,0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF)}
  # !BSF HELP:{Indicate whether the System Error on Non Fatal Error is enabled.}
  gPlatformFspPkgTokenSpaceGuid.CpuPcieRpSystemErrorOnNonFatalError | * | 0x4 | { 0x00, 0x00, 0x00, 0x00 }

  # !BSF NAME:{PCIE RP System Error On Correctable Error} TYPE:{EditNum, HEX, (0x00,0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF)}
  # !BSF HELP:{Indicate whether the System Error on Correctable Error is enabled.}
  gPlatformFspPkgTokenSpaceGuid.CpuPcieRpSystemErrorOnCorrectableError | * | 0x4 | { 0x00, 0x00, 0x00, 0x00 }

  # !BSF NAME:{PCIE RP Max Payload} TYPE:{EditNum, HEX, (0x00,0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF)}
  # !BSF HELP:{Max Payload Size supported, Default 128B, see enum CPU_PCIE_MAX_PAYLOAD.}
  gPlatformFspPkgTokenSpaceGuid.CpuPcieRpMaxPayload            | * | 0x4 | { 0x00, 0x00, 0x00, 0x00 }

  # !BSF NAME:{DPC for PCIE RP Mask} TYPE:{EditNum, HEX, (0x00,0x00FFFFFF)}
  # !BSF HELP:{Enable/disable Downstream Port Containment for PCIE Root Ports. 0: disable, 1: enable. One bit for each port, bit0 for port1, bit1 for port2, and so on.}
  gPlatformFspPkgTokenSpaceGuid.CpuPcieRpDpcEnabled               | * | 0x04 | { 0x00, 0x00, 0x00, 0x00 }

  # !BSF NAME:{DPC Extensions PCIE RP Mask} TYPE:{EditNum, HEX, (0x00,0x00FFFFFF)}
  # !BSF HELP:{Enable/disable DPC Extensions for PCIE Root Ports. 0: disable, 1: enable. One bit for each port, bit0 for port1, bit1 for port2, and so on.}
  gPlatformFspPkgTokenSpaceGuid.CpuPcieRpDpcExtensionsEnabled     | * | 0x04 | { 0x01, 0x01, 0x01, 0x01 }

  # !BSF NAME:{CPU PCIe root port connection type} TYPE:{EditNum, HEX, (0x00,0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF)}
  # !BSF HELP:{0: built-in device, 1:slot}
  gPlatformFspPkgTokenSpaceGuid.CpuPcieRpSlotImplemented       | * | 0x4 | { 0x01, 0x01, 0x01, 0x01 }

  # !BSF NAME:{PCIE RP Gen3 Equalization Phase Method} TYPE:{EditNum, HEX, (0x00,0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF)}
  # !BSF HELP:{PCIe Gen3 Eq Ph3 Method (see CPU_PCIE_EQ_METHOD). 0: DEPRECATED, hardware equalization; 1: hardware equalization; 4: Fixed Coeficients.}
  gPlatformFspPkgTokenSpaceGuid.CpuPcieRpGen3EqPh3Method       | * | 0x4 | { 0x01, 0x01, 0x01, 0x01 }

  # !BSF NAME:{PCIE RP Gen4 Equalization Phase Method} TYPE:{EditNum, HEX, (0x00,0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF)}
  # !BSF HELP:{PCIe Gen4 Eq Ph3 Method (see CPU_PCIE_EQ_METHOD). 0: DEPRECATED, hardware equalization; 1: hardware equalization; 4: Fixed Coeficients.}
  gPlatformFspPkgTokenSpaceGuid.CpuPcieRpGen4EqPh3Method       | * | 0x4 | { 0x01, 0x01, 0x01, 0x01 }

  # !BSF NAME:{PCIE RP Physical Slot Number} TYPE:{EditNum, HEX, (0x00,0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF)}
  # !BSF HELP:{Indicates the slot number for the root port. Default is the value as root port index.}
  gPlatformFspPkgTokenSpaceGuid.CpuPcieRpPhysicalSlotNumber    | * | 0x4 | { 0x00, 0x01, 0x02, 0x03 }

  # !BSF NAME:{PCIE RP Aspm} TYPE:{EditNum, HEX, (0x00,0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF)}
  # !BSF HELP:{The ASPM configuration of the root port (see: CPU_PCIE_ASPM_CONTROL).0: Disable; 1: CpuPcieAspmL0s; 2: CpuPcieAspmL1; 3:CpuPcieAspmL0sL1(Default)}
  gPlatformFspPkgTokenSpaceGuid.CpuPcieRpAspm                  | * | 0x4 | { 0x03, 0x03, 0x03, 0x03 }

  # !BSF NAME:{PCIE RP L1 Substates} TYPE:{EditNum, HEX, (0x00,0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF)}
  # !BSF HELP:{The L1 Substates configuration of the root port (see: CPU_PCIE_L1SUBSTATES_CONTROL). Default is CpuPcieL1SubstatesL1_1_2.}
  gPlatformFspPkgTokenSpaceGuid.CpuPcieRpL1Substates           | * | 0x4 | { 0x02, 0x02, 0x02, 0x02 }

  # !BSF NAME:{PCIE RP Ltr Enable} TYPE:{EditNum, HEX, (0x00,0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF)}
  # !BSF HELP:{Latency Tolerance Reporting Mechanism.}
  gPlatformFspPkgTokenSpaceGuid.CpuPcieRpLtrEnable             | * | 0x4 | { 0x00, 0x00, 0x00, 0x00 }

  # !BSF NAME:{PCIE RP Ltr Config Lock} TYPE:{EditNum, HEX, (0x00,0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF)}
  # !BSF HELP:{0: Disable; 1: Enable.}
  gPlatformFspPkgTokenSpaceGuid.CpuPcieRpLtrConfigLock         | * | 0x4 | { 0x00, 0x00, 0x00, 0x00 }

  # !BSF NAME:{PTM for PCIE RP Mask} TYPE:{EditNum, HEX, (0x00,0x00FFFFFF)}
  # !BSF HELP:{Enable/disable Precision Time Measurement for PCIE Root Ports. 0: disable, 1: enable. One bit for each port, bit0 for port1, bit1 for port2, and so on.}
  gPlatformFspPkgTokenSpaceGuid.CpuPcieRpPtmEnabled            | * | 0x04 | { 0x01, 0x01, 0x01, 0x01 }

  # !BSF NAME:{PCIE RP Detect Timeout Ms} TYPE:{EditNum, HEX, (0x00,0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF)}
  # !HDR STRUCT:{UINT16}
  # !BSF HELP:{The number of milliseconds within 0~65535 in reference code will wait for link to exit Detect state for enabled ports before assuming there is no device and potentially disabling the port.}
  gPlatformFspPkgTokenSpaceGuid.CpuPcieRpDetectTimeoutMs      | * | 0x8 | { 0x00, 0x00, 0x00, 0x00 }

  # !BSF NAME:{Multi-VC for PCIE RP Mask} TYPE:{EditNum, HEX, (0x00,0x00FFFFFF)}
  # !BSF HELP:{Enable/disable Multiple Virtual Channel for PCIE Root Ports. 0: disable, 1: enable. One bit for each port, bit0 for port1, bit1 for port2, and so on.}
  gPlatformFspPkgTokenSpaceGuid.CpuPcieRpMultiVcEnabled            | * | 0x04 | { 0x00, 0x00, 0x00, 0x00 }

  #
  # SA Post-Mem PCIE Policies End
  #

  # !BSF PAGE:{PCH2}
  # !BSF NAME:{Enable the write to USB 3.0 TX Output Unique Transition Bit Mode for rate 3} TYPE:{EditNum, HEX, (0x00,0x01010101010101010101)}
  # !BSF HELP:{Enable the write to USB 3.0 TX Output Unique Transition Bit Mode for rate 3, Each value in array can be between 0-1. One byte for each port.}
  gPlatformFspPkgTokenSpaceGuid.Usb3HsioTxRate3UniqTranEnable  | * | 0x0A | {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00}

  # !BSF NAME:{USB 3.0 TX Output Unique Transition Bit Scale for rate 3} TYPE:{EditNum, HEX, (0x00,0xFFFFFFFFFFFFFFFFFFFF)}
  # !BSF HELP:{USB 3.0 TX Output Unique Transition Bit Scale for rate 3, HSIO_TX_DWORD9[6:0], <b>Default = 4Ch</b>. One byte for each port.}
  gPlatformFspPkgTokenSpaceGuid.Usb3HsioTxRate3UniqTran      | * | 0x0A | {0x4C, 0x4C, 0x4C, 0x4C, 0x4C, 0x4C, 0x4C, 0x4C, 0x4C, 0x4C}

  # !BSF NAME:{Enable the write to USB 3.0 TX Output Unique Transition Bit Mode for rate 2} TYPE:{EditNum, HEX, (0x00,0x01010101010101010101)}
  # !BSF HELP:{Enable the write to USB 3.0 TX Output Unique Transition Bit Mode for rate 2, Each value in array can be between 0-1. One byte for each port.}
  gPlatformFspPkgTokenSpaceGuid.Usb3HsioTxRate2UniqTranEnable  | * | 0x0A | {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00}

  # !BSF NAME:{USB 3.0 TX Output Unique Transition Bit Scale for rate 2} TYPE:{EditNum, HEX, (0x00,0xFFFFFFFFFFFFFFFFFFFF)}
  # !BSF HELP:{USB 3.0 TX Output Unique Transition Bit Scale for rate 2, HSIO_TX_DWORD9[14:8], <b>Default = 4Ch</b>. One byte for each port.}
  gPlatformFspPkgTokenSpaceGuid.Usb3HsioTxRate2UniqTran      | * | 0x0A | {0x4C, 0x4C, 0x4C, 0x4C, 0x4C, 0x4C, 0x4C, 0x4C, 0x4C, 0x4C}

  # !BSF NAME:{Enable the write to USB 3.0 TX Output Unique Transition Bit Mode for rate 1} TYPE:{EditNum, HEX, (0x00,0x01010101010101010101)}
  # !BSF HELP:{Enable the write to USB 3.0 TX Output Unique Transition Bit Mode for rate 1, Each value in array can be between 0-1. One byte for each port.}
  gPlatformFspPkgTokenSpaceGuid.Usb3HsioTxRate1UniqTranEnable  | * | 0x0A | {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00}

  # !BSF NAME:{USB 3.0 TX Output Unique Transition Bit Scale for rate 1} TYPE:{EditNum, HEX, (0x00,0xFFFFFFFFFFFFFFFFFFFF)}
  # !BSF HELP:{USB 3.0 TX Output Unique Transition Bit Scale for rate 1, HSIO_TX_DWORD9[22:16], <b>Default = 4Ch</b>. One byte for each port.}
  gPlatformFspPkgTokenSpaceGuid.Usb3HsioTxRate1UniqTran      | * | 0x0A | {0x4C, 0x4C, 0x4C, 0x4C, 0x4C, 0x4C, 0x4C, 0x4C, 0x4C, 0x4C}

  # !BSF NAME:{Enable the write to USB 3.0 TX Output Unique Transition Bit Mode for rate 0} TYPE:{EditNum, HEX, (0x00,0x01010101010101010101)}
  # !BSF HELP:{Enable the write to USB 3.0 TX Output Unique Transition Bit Mode for rate 0, Each value in array can be between 0-1. One byte for each port.}
  gPlatformFspPkgTokenSpaceGuid.Usb3HsioTxRate0UniqTranEnable  | * | 0x0A | {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00}

  # !BSF NAME:{USB 3.0 TX Output Unique Transition Bit Scale for rate 0} TYPE:{EditNum, HEX, (0x00,0xFFFFFFFFFFFFFFFFFFFF)}
  # !BSF HELP:{USB 3.0 TX Output Unique Transition Bit Scale for rate 0, HSIO_TX_DWORD9[30:24], <b>Default = 4Ch</b>. One byte for each port.}
  gPlatformFspPkgTokenSpaceGuid.Usb3HsioTxRate0UniqTran      | * | 0x0A | {0x4C, 0x4C, 0x4C, 0x4C, 0x4C, 0x4C, 0x4C, 0x4C, 0x4C, 0x4C}

  #
  # SA Post-Mem Block Start
  #

  # !BSF PAGE:{SA2}
  # !BSF NAME:{Skip PAM regsiter lock} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Enable: PAM register will not be locked by RC, platform code should lock it, Disable(Default): PAM registers will be locked by RC}
  gPlatformFspPkgTokenSpaceGuid.SkipPamLock                 | * | 0x01 | 0x0

  # !BSF NAME:{EDRAM Test Mode} TYPE:{Combo}
  # !BSF OPTION:{0: EDRAM SW disable, 1: EDRAM SW Enable, 2: EDRAM HW mode}
  gPlatformFspPkgTokenSpaceGuid.EdramTestMode               | * | 0x01 | 0x2

  # !BSF NAME:{Enable/Disable IGFX RenderStandby} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Enable(Default): Enable IGFX RenderStandby, Disable: Disable IGFX RenderStandby}
  gPlatformFspPkgTokenSpaceGuid.RenderStandby               | * | 0x01 | 0x1

  # !BSF NAME:{Enable/Disable IGFX PmSupport} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Enable(Default): Enable IGFX PmSupport, Disable: Disable IGFX PmSupport}
  gPlatformFspPkgTokenSpaceGuid.PmSupport                   | * | 0x01 | 0x1

  # !BSF NAME:{Enable/Disable CdynmaxClamp} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Enable: Enable CdynmaxClamp, Disable(Default): Disable CdynmaxClamp}
  gPlatformFspPkgTokenSpaceGuid.CdynmaxClampEnable          | * | 0x01 | 0x0

  # !BSF NAME:{GT Frequency Limit} TYPE:{Combo}
  # !BSF OPTION:{0xFF: Auto(Default), 2: 100 Mhz, 3: 150 Mhz, 4: 200 Mhz, 5: 250 Mhz, 6: 300 Mhz, 7: 350 Mhz, 8: 400 Mhz, 9: 450 Mhz, 0xA: 500 Mhz, 0xB: 550 Mhz, 0xC: 600 Mhz, 0xD: 650 Mhz, 0xE: 700 Mhz, 0xF: 750 Mhz, 0x10: 800 Mhz, 0x11: 850 Mhz, 0x12:900 Mhz, 0x13: 950 Mhz, 0x14: 1000 Mhz, 0x15: 1050 Mhz, 0x16: 1100 Mhz, 0x17: 1150 Mhz, 0x18: 1200 Mhz}
  # !BSF HELP:{0xFF: Auto(Default), 2: 100 Mhz, 3: 150 Mhz, 4: 200 Mhz, 5: 250 Mhz, 6: 300 Mhz, 7: 350 Mhz, 8: 400 Mhz, 9: 450 Mhz, 0xA: 500 Mhz, 0xB: 550 Mhz, 0xC: 600 Mhz, 0xD: 650 Mhz, 0xE: 700 Mhz, 0xF: 750 Mhz, 0x10: 800 Mhz, 0x11: 850 Mhz, 0x12:900 Mhz, 0x13: 950 Mhz, 0x14: 1000 Mhz, 0x15: 1050 Mhz, 0x16: 1100 Mhz, 0x17: 1150 Mhz, 0x18: 1200 Mhz}
  gPlatformFspPkgTokenSpaceGuid.GtFreqMax                   | * | 0x1 | 0xFF

  # !BSF NAME:{Disable Turbo GT} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{ 0=Disable: GT frequency is not limited, 1=Enable: Disables Turbo GT frequency}
  gPlatformFspPkgTokenSpaceGuid.DisableTurboGt              | * | 0x1 | 0x0

#
  # !BSF NAME:{Enable/Disable CdClock Init} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Enable: Skip Full CD clock initializaton, Disable(Default): Initialize the full CD clock if not initialized by Gfx PEIM}
  gPlatformFspPkgTokenSpaceGuid.SkipCdClockInit             | * | 0x01 | 0x0


  # !BSF NAME:{Enable RC1p frequency request to PMA (provided all other conditions are met)}
  # !BSF TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{0(Default)=Disable, 1=Enable}
  gPlatformFspPkgTokenSpaceGuid.RC1pFreqEnable            | * | 0x01 | 0x0

  # !BSF NAME:{Enable TSN Multi-VC} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Enable/disable Multi Virtual Channels(VC) in TSN.}
  gPlatformFspPkgTokenSpaceGuid.PchTsnMultiVcEnable         | * | 0x01 | 0x0

  # !BSF NAME:{LogoPixelHeight Address}
  # !BSF TYPE:{EditNum, HEX, (0x0,0xFFFFFFFF)}
  # !BSF HELP:{Address of LogoPixelHeight}
  gPlatformFspPkgTokenSpaceGuid.LogoPixelHeight             | * | 0x04 | 0x00000000

  # !BSF NAME:{LogoPixelWidth Address}
  # !BSF TYPE:{EditNum, HEX, (0x0,0xFFFFFFFF)}
  # !BSF HELP:{Address of LogoPixelWidth}
  gPlatformFspPkgTokenSpaceGuid.LogoPixelWidth              | * | 0x04 | 0x00000000

  # !BSF NAME:{ITbt Usb4CmMode value} TYPE:{EditNum, HEX, (0x00, 0xFF)}
  # !BSF HELP:{ITbt Usb4CmMode value. 0:Firmware CM, 1:Software CM}
  gPlatformFspPkgTokenSpaceGuid.Usb4CmMode                  | * | 0x01 | 0x01

  # !BSF NAME:{PCIE Resizable BAR Support} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Enable/Disable PCIE Resizable BAR Support.0: Disable; 1: Enable; 2: Auto(Default).}
  gPlatformFspPkgTokenSpaceGuid.CpuPcieResizableBarSupport          | * | 0x01 | 0x02

  # !BSF NAME:{SaPostMemTestRsvd} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Reserved for SA Post-Mem Test}
  gPlatformFspPkgTokenSpaceGuid.SaPostMemTestRsvd           | * | 0x03 | {0x00}

  #
  # SA Post-Mem Block End
  #

  #
  # CPU Post-Mem Block Start
  #

  # !BSF PAGE:{CPU2}
  # !BSF NAME:{RSR feature} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Enable or Disable RSR feature; 0: Disable; <b>1: Enable </b>}
  gPlatformFspPkgTokenSpaceGuid.EnableRsr                   | * | 0x01 | 0x01

  # !BSF NAME:{ReservedCpuPostMem1} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Reserved for CPU Post-Mem 1}
  gPlatformFspPkgTokenSpaceGuid.ReservedCpuPostMem1         | * | 0x4 | {0x00}

  # !BSF NAME:{Enable or Disable HWP} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Enable or Disable HWP(Hardware P states) Support. 0: Disable; <b>1: Enable;</b> 2-3:Reserved}
  gPlatformFspPkgTokenSpaceGuid.Hwp                         | * | 0x01 | 0x01

  # !BSF NAME:{Hardware Duty Cycle Control} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Hardware Duty Cycle Control configuration. 0: Disabled; <b>1: Enabled</b> 2-3:Reserved}
  gPlatformFspPkgTokenSpaceGuid.HdcControl                  | * | 0x01 | 0x01

  # !BSF NAME:{Package Long duration turbo mode time}
  # !BSF TYPE:{EditNum, HEX, (0x00, 0x80)}
  # !BSF HELP:{Package Long duration turbo mode time window in seconds. Valid values(Unit in seconds) 0 to 8 , 10 , 12 ,14 , 16 , 20 , 24 , 28 , 32 , 40 , 48 , 56 , 64 , 80 , 96 , 112 , 128}
  gPlatformFspPkgTokenSpaceGuid.PowerLimit1Time             | * | 0x01 | 0x00

  # !BSF NAME:{Short Duration Turbo Mode} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Enable or Disable short duration Turbo Mode. </b>0 : Disable; <b>1: Enable</b>}
  gPlatformFspPkgTokenSpaceGuid.PowerLimit2                 | * | 0x01 | 0x01

  # !BSF NAME:{Turbo settings Lock} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Lock all Turbo settings Enable/Disable; <b>0: Disable , </b> 1: Enable}
  gPlatformFspPkgTokenSpaceGuid.TurboPowerLimitLock         | * | 0x01 | 0x00

  # !BSF NAME:{Package PL3 time window}
  # !BSF TYPE:{EditNum, HEX, (0x00, 0xFF)}
  # !BSF HELP:{Package PL3 time window range for this policy from 0 to 64ms}
  gPlatformFspPkgTokenSpaceGuid.PowerLimit3Time             | * | 0x01 | 0x00

  # !BSF NAME:{Package PL3 Duty Cycle}
  # !BSF TYPE:{EditNum, HEX, (0x00, 0x64)}
  # !BSF HELP:{Package PL3 Duty Cycle; Valid Range is 0 to 100}
  gPlatformFspPkgTokenSpaceGuid.PowerLimit3DutyCycle        | * | 0x01 | 0x00

  # !BSF NAME:{Package PL3 Lock} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Package PL3 Lock Enable/Disable; <b>0: Disable</b> ; 1:Enable}
  gPlatformFspPkgTokenSpaceGuid.PowerLimit3Lock             | * | 0x01 | 0x00

  # !BSF NAME:{Package PL4 Lock} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Package PL4 Lock Enable/Disable; <b>0: Disable</b> ; 1:Enable}
  gPlatformFspPkgTokenSpaceGuid.PowerLimit4Lock             | * | 0x01 | 0x00

  # !BSF NAME:{TCC Activation Offset}
  # !BSF TYPE:{EditNum, HEX, (0x00, 0xFF)}
  # !BSF HELP:{TCC Activation Offset. Offset from factory set TCC activation temperature at which the Thermal Control Circuit must be activated. TCC will be activated at TCC Activation Temperature, in volts.For SKL Y SKU, the recommended default for this policy is  <b>10</b>, For all other SKUs the recommended default are <b>0</b>}
  gPlatformFspPkgTokenSpaceGuid.TccActivationOffset         | * | 0x01 | 0x0A

  # !BSF NAME:{Tcc Offset Clamp Enable/Disable} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Tcc Offset Clamp for Runtime Average Temperature Limit (RATL) allows CPU to throttle below P1.For SKL Y SKU, the recommended default for this policy is <b>1: Enabled</b>, For all other SKUs the recommended default are  <b>0: Disabled</b>.}
  gPlatformFspPkgTokenSpaceGuid.TccOffsetClamp              | * | 0x01 | 0x01

  # !BSF NAME:{Tcc Offset Lock} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Tcc Offset Lock for Runtime Average Temperature Limit (RATL) to lock temperature target; <b>0: Disabled</b>; 1: Enabled.}
  gPlatformFspPkgTokenSpaceGuid.TccOffsetLock               | * | 0x01 | 0x00

  # !BSF NAME:{Custom Ratio State Entries}
  # !BSF TYPE:{EditNum, HEX, (0x00, 0x28)}
  # !BSF HELP:{The number of custom ratio state entries, ranges from 0 to 40 for a valid custom ratio table.Sets the number of custom P-states. At least 2 states must be present}
  gPlatformFspPkgTokenSpaceGuid.NumberOfEntries             | * | 0x01 | 0x00

  # !BSF NAME:{Custom Short term Power Limit time window}
  # !BSF TYPE:{EditNum, HEX, (0x00, 0x80)}
  # !BSF HELP:{Short term Power Limit time window value for custom CTDP level 1. Valid Range 0 to 128}
  gPlatformFspPkgTokenSpaceGuid.Custom1PowerLimit1Time      | * | 0x01 | 0x0

  # !BSF NAME:{Custom Turbo Activation Ratio}
  # !BSF TYPE:{EditNum, HEX, (0x00, 0xFF)}
  # !BSF HELP:{Turbo Activation Ratio for custom cTDP level 1. Valid Range 0 to 255}
  gPlatformFspPkgTokenSpaceGuid.Custom1TurboActivationRatio | * | 0x01 | 0x14

  # !BSF NAME:{Custom Config Tdp Control}
  # !BSF TYPE:{EditNum, HEX, (0x00, 0x2)}
  # !BSF HELP:{Config Tdp Control (0/1/2) value for custom cTDP level 1. Valid Range is 0 to 2}
  gPlatformFspPkgTokenSpaceGuid.Custom1ConfigTdpControl     | * | 0x01 | 0x00

  # !BSF NAME:{Custom Short term Power Limit time window}
  # !BSF TYPE:{EditNum, HEX, (0x00, 0x80)}
  # !BSF HELP:{Short term Power Limit time window value for custom CTDP level 2. Valid Range 0 to 128}
  gPlatformFspPkgTokenSpaceGuid.Custom2PowerLimit1Time      | * | 0x01 | 0x0

  # !BSF NAME:{Custom Turbo Activation Ratio}
  # !BSF TYPE:{EditNum, HEX, (0x00, 0xFF)}
  # !BSF HELP:{Turbo Activation Ratio for custom cTDP level 2. Valid Range 0 to 255}
  gPlatformFspPkgTokenSpaceGuid.Custom2TurboActivationRatio | * | 0x01 | 0x14

  # !BSF NAME:{Custom Config Tdp Control}
  # !BSF TYPE:{EditNum, HEX, (0x00, 0x2)}
  # !BSF HELP:{Config Tdp Control (0/1/2) value for custom cTDP level 1. Valid Range is 0 to 2}
  gPlatformFspPkgTokenSpaceGuid.Custom2ConfigTdpControl     | * | 0x01 | 0x00

  # !BSF NAME:{Custom Short term Power Limit time window}
  # !BSF TYPE:{EditNum, HEX, (0x00, 0x80)}
  # !BSF HELP:{Short term Power Limit time window value for custom CTDP level 3. Valid Range 0 to 128}
  gPlatformFspPkgTokenSpaceGuid.Custom3PowerLimit1Time      | * | 0x01 | 0x0

  # !BSF NAME:{Custom Turbo Activation Ratio}
  # !BSF TYPE:{EditNum, HEX, (0x00, 0xFF)}
  # !BSF HELP:{Turbo Activation Ratio for custom cTDP level 3. Valid Range 0 to 255}
  gPlatformFspPkgTokenSpaceGuid.Custom3TurboActivationRatio | * | 0x01 | 0x14

  # !BSF NAME:{Custom Config Tdp Control}
  # !BSF TYPE:{EditNum, HEX, (0x00, 0x2)}
  # !BSF HELP:{Config Tdp Control (0/1/2) value for custom cTDP level 1. Valid Range is 0 to 2}
  gPlatformFspPkgTokenSpaceGuid.Custom3ConfigTdpControl     | * | 0x01 | 0x00

  # !BSF NAME:{ConfigTdp mode settings Lock} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Lock the ConfigTdp mode settings from runtime changes; <b>0: Disable</b>; 1: Enable}
  gPlatformFspPkgTokenSpaceGuid.ConfigTdpLock               | * | 0x01 | 0x00

  # !BSF NAME:{Load Configurable TDP SSDT} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Configure whether to load Configurable TDP SSDT; <b>0: Disable</b>; 1: Enable.}
  gPlatformFspPkgTokenSpaceGuid.ConfigTdpBios               | * | 0x01 | 0x00

  # !BSF NAME:{PL1 Enable value} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{PL1 Enable value to limit average platform power. <b>0: Disable</b>; 1: Enable.}
  gPlatformFspPkgTokenSpaceGuid.PsysPowerLimit1             | * | 0x01 | 0x00

  # !BSF NAME:{PL1 timewindow}
  # !BSF TYPE:{EditNum, HEX, (0x00, 0x80)}
  # !BSF HELP:{PL1 timewindow in seconds.Valid values(Unit in seconds) 0 to 8 , 10 , 12 ,14 , 16 , 20 , 24 , 28 , 32 , 40 , 48 , 56 , 64 , 80 , 96 , 112 , 128}
  gPlatformFspPkgTokenSpaceGuid.PsysPowerLimit1Time         | * | 0x01 | 0x00

  # !BSF NAME:{PL2 Enable Value} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{PL2 Enable activates the PL2 value to limit average platform power.<b>0: Disable</b>; 1: Enable.}
  gPlatformFspPkgTokenSpaceGuid.PsysPowerLimit2             | * | 0x01 | 0x00

  # !BSF NAME:{Enable or Disable MLC Streamer Prefetcher} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Enable or Disable MLC Streamer Prefetcher; 0: Disable; <b>1: Enable</b>.}
  gPlatformFspPkgTokenSpaceGuid.MlcStreamerPrefetcher       | * | 0x01 | 0x01

  # !BSF NAME:{Enable or Disable MLC Spatial Prefetcher} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Enable or Disable MLC Spatial Prefetcher; 0: Disable; <b>1: Enable</b>}
  gPlatformFspPkgTokenSpaceGuid.MlcSpatialPrefetcher        | * | 0x01 | 0x01

  # !BSF NAME:{Enable or Disable Monitor /MWAIT instructions} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Enable or Disable Monitor /MWAIT instructions; 0: Disable; <b>1: Enable</b>.}
  gPlatformFspPkgTokenSpaceGuid.MonitorMwaitEnable          | * | 0x01 | 0x01

  # !BSF NAME:{Enable or Disable initialization of machine check registers} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Enable or Disable initialization of machine check registers; 0: Disable; <b>1: Enable</b>.}
  gPlatformFspPkgTokenSpaceGuid.MachineCheckEnable          | * | 0x01 | 0x01

  # !BSF NAME:{AP Idle Manner of waiting for SIPI}
  # !BSF TYPE:{Combo} OPTION:{1: HALT loop, 2: MWAIT loop, 3: RUN loop}
  # !BSF HELP:{AP Idle Manner of waiting for SIPI; 1: HALT loop; <b>2: MWAIT loop</b>; 3: RUN loop.}
  gPlatformFspPkgTokenSpaceGuid.ApIdleManner                | * | 0x01 | 0x02

  # !BSF NAME:{Control on Processor Trace output scheme}
  # !BSF TYPE:{Combo} OPTION:{0: Single Range Output, 1: ToPA Output}
  # !BSF HELP:{Control on Processor Trace output scheme; <b>0: Single Range Output</b>; 1: ToPA Output.}
  gPlatformFspPkgTokenSpaceGuid.ProcessorTraceOutputScheme  | * | 0x01 | 0x00

  # !BSF NAME:{Enable or Disable Processor Trace feature} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Enable or Disable Processor Trace feature; <b>0: Disable</b>; 1: Enable.}
  gPlatformFspPkgTokenSpaceGuid.ProcessorTraceEnable        | * | 0x01 | 0x00

  # !BSF NAME:{Enable or Disable Intel SpeedStep Technology} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Enable or Disable Intel SpeedStep Technology. 0: Disable; <b>1: Enable</b>}
  gPlatformFspPkgTokenSpaceGuid.Eist                        | * | 0x01 | 0x01

  # !BSF NAME:{Enable or Disable Energy Efficient P-state} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Enable or Disable Energy Efficient P-state will be applied in Turbo mode. Disable; <b>1: Enable</b>}
  gPlatformFspPkgTokenSpaceGuid.EnergyEfficientPState       | * | 0x01 | 0x01

  # !BSF NAME:{Enable or Disable Energy Efficient Turbo} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Enable or Disable Energy Efficient Turbo, will be applied in Turbo mode. <b>0: Disable</b>; 1: Enable}
  gPlatformFspPkgTokenSpaceGuid.EnergyEfficientTurbo        | * | 0x01 | 0x00

  # !BSF NAME:{Enable or Disable T states} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Enable or Disable T states; <b>0: Disable</b>; 1: Enable.}
  gPlatformFspPkgTokenSpaceGuid.TStates                     | * | 0x01 | 0x01

  # !BSF NAME:{Enable or Disable Bi-Directional PROCHOT#} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Enable or Disable Bi-Directional PROCHOT#; 0: Disable; <b>1: Enable</b>}
  gPlatformFspPkgTokenSpaceGuid.BiProcHot                   | * | 0x01 | 0x01

  # !BSF NAME:{Enable or Disable PROCHOT# signal being driven externally} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Enable or Disable PROCHOT# signal being driven externally; 0: Disable; <b>1: Enable</b>.}
  gPlatformFspPkgTokenSpaceGuid.DisableProcHotOut           | * | 0x01 | 0x01

  # !BSF NAME:{Enable or Disable PROCHOT# Response} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Enable or Disable PROCHOT# Response; 0: Disable; <b>1: Enable</b>.}
  gPlatformFspPkgTokenSpaceGuid.ProcHotResponse             | * | 0x01 | 0x01

  # !BSF NAME:{Enable or Disable VR Thermal Alert} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Enable or Disable VR Thermal Alert; <b>0: Disable</b>; 1: Enable.}
  gPlatformFspPkgTokenSpaceGuid.DisableVrThermalAlert       | * | 0x01 | 0x00

  # !BSF NAME:{Enable or Disable Thermal Reporting} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Enable or Disable Thermal Reporting through ACPI tables; 0: Disable; <b>1: Enable</b>.}
  gPlatformFspPkgTokenSpaceGuid.EnableAllThermalFunctions        | * | 0x01 | 0x00

  # !BSF NAME:{Enable or Disable Thermal Monitor} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Enable or Disable Thermal Monitor; 0: Disable; <b>1: Enable</b>}
  gPlatformFspPkgTokenSpaceGuid.ThermalMonitor              | * | 0x01 | 0x01

  # !BSF NAME:{Enable or Disable CPU power states (C-states)} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Enable or Disable CPU power states (C-states). 0: Disable; <b>1: Enable</b>}
  gPlatformFspPkgTokenSpaceGuid.Cx                          | * | 0x01 | 0x01

  # !BSF NAME:{Configure C-State Configuration Lock} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Configure C-State Configuration Lock; 0: Disable; <b>1: Enable</b>.}
  gPlatformFspPkgTokenSpaceGuid.PmgCstCfgCtrlLock           | * | 0x01 | 0x01

  # !BSF NAME:{Enable or Disable Enhanced C-states} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Enable or Disable Enhanced C-states. 0: Disable; <b>1: Enable</b>}
  gPlatformFspPkgTokenSpaceGuid.C1e                         | * | 0x01 | 0x01

  # !BSF NAME:{Enable or Disable Package Cstate Demotion} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Enable or Disable Package Cstate Demotion. 0: Disable; <b>1: Enable</b>}
  gPlatformFspPkgTokenSpaceGuid.PkgCStateDemotion           | * | 0x01 | 0x01

  # !BSF NAME:{Enable or Disable Package Cstate UnDemotion} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Enable or Disable Package Cstate UnDemotion. 0: Disable; <b>1: Enable</b>}
  gPlatformFspPkgTokenSpaceGuid.PkgCStateUnDemotion         | * | 0x01 | 0x01

  # !BSF NAME:{Enable or Disable CState-Pre wake} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Enable or Disable CState-Pre wake. 0: Disable; <b>1: Enable</b>}
  gPlatformFspPkgTokenSpaceGuid.CStatePreWake               | * | 0x01 | 0x01

  # !BSF NAME:{Enable or Disable TimedMwait Support.} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Enable or Disable TimedMwait Support. <b>0: Disable</b>; 1: Enable}
  gPlatformFspPkgTokenSpaceGuid.TimedMwait                  | * | 0x01 | 0x00

  # !BSF NAME:{Enable or Disable IO to MWAIT redirection} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Enable or Disable IO to MWAIT redirection; <b>0: Disable</b>; 1: Enable.}
  gPlatformFspPkgTokenSpaceGuid.CstCfgCtrIoMwaitRedirection | * | 0x01 | 0x00

  # !BSF NAME:{Set the Max Pkg Cstate}
  # !BSF TYPE:{EditNum, HEX, (0x00, 0xFF)}
  # !BSF HELP:{Set the Max Pkg Cstate. Default set to Auto which limits the Max Pkg Cstate to deep C-state. Valid values 0 - C0/C1 , 1 - C2 , 2 - C3 , 3 - C6 , 4 - C7 , 5 - C7S , 6 - C8 , 7 - C9 , 8 - C10 , 254 - CPU Default , 255 - Auto}
  gPlatformFspPkgTokenSpaceGuid.PkgCStateLimit              | * | 0x01 | 0x08

  # !BSF NAME:{TimeUnit for C-State Latency Control0}
  # !BSF TYPE:{EditNum, HEX, (0x00, 0x5)}
  # !BSF HELP:{TimeUnit for C-State Latency Control0; Valid values 0 - 1ns , 1 - 32ns , 2 - 1024ns , 3 - 32768ns , 4 - 1048576ns , 5 - 33554432ns}
  gPlatformFspPkgTokenSpaceGuid.CstateLatencyControl0TimeUnit   | * | 0x01 | 0x02

  # !BSF NAME:{TimeUnit for C-State Latency Control1}
  # !BSF TYPE:{EditNum, HEX, (0x00, 0x5)}
  # !BSF HELP:{TimeUnit for C-State Latency Control1;Valid values 0 - 1ns , 1 - 32ns , 2 - 1024ns , 3 - 32768ns , 4 - 1048576ns , 5 - 33554432ns}
  gPlatformFspPkgTokenSpaceGuid.CstateLatencyControl1TimeUnit   | * | 0x01 | 0x02

  # !BSF NAME:{TimeUnit for C-State Latency Control2}
  # !BSF TYPE:{EditNum, HEX, (0x00, 0x5)}
  # !BSF HELP:{TimeUnit for C-State Latency Control2;Valid values 0 - 1ns , 1 - 32ns , 2 - 1024ns , 3 - 32768ns , 4 - 1048576ns , 5 - 33554432ns}
  gPlatformFspPkgTokenSpaceGuid.CstateLatencyControl2TimeUnit   | * | 0x01 | 0x02

  # !BSF NAME:{TimeUnit for C-State Latency Control3}
  # !BSF TYPE:{EditNum, HEX, (0x00, 0x5)}
  # !BSF HELP:{TimeUnit for C-State Latency Control3;Valid values 0 - 1ns , 1 - 32ns , 2 - 1024ns , 3 - 32768ns , 4 - 1048576ns , 5 - 33554432ns}
  gPlatformFspPkgTokenSpaceGuid.CstateLatencyControl3TimeUnit   | * | 0x01 | 0x02

  # !BSF NAME:{TimeUnit for C-State Latency Control4}
  # !BSF TYPE:{EditNum, HEX, (0x00, 0x5)}
  # !BSF HELP:{Time - 1ns , 1 - 32ns , 2 - 1024ns , 3 - 32768ns , 4 - 1048576ns , 5 - 33554432ns}
  gPlatformFspPkgTokenSpaceGuid.CstateLatencyControl4TimeUnit   | * | 0x01 | 0x02

  # !BSF NAME:{TimeUnit for C-State Latency Control5}
  # !BSF TYPE:{EditNum, HEX, (0x00, 0x5)}
  # !BSF HELP:{TimeUnit for C-State Latency Control5;Valid values 0 - 1ns , 1 - 32ns , 2 - 1024ns , 3 - 32768ns , 4 - 1048576ns , 5 - 33554432ns}
  gPlatformFspPkgTokenSpaceGuid.CstateLatencyControl5TimeUnit   | * | 0x01 | 0x02

  # !BSF NAME:{Interrupt Redirection Mode Select}
  # !BSF TYPE:{EditNum, HEX, (0x00, 0x7)}

  # !BSF HELP:{Interrupt Redirection Mode Select.0: Fixed priority; 1: Round robin;2: Hash vector;7: No change.}
  gPlatformFspPkgTokenSpaceGuid.PpmIrmSetting               | * | 0x01 | 0x00

  # !BSF NAME:{Lock prochot configuration} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Lock prochot configuration Enable/Disable; 0: Disable;<b> 1: Enable</b>}
  gPlatformFspPkgTokenSpaceGuid.ProcHotLock                 | * | 0x01 | 0x01

  # !BSF NAME:{Configuration for boot TDP selection}
  # !BSF TYPE:{EditNum, HEX, (0x00, 0xFF)}
  # !BSF HELP:{Deprecated. Move to premem.}
  gPlatformFspPkgTokenSpaceGuid.ConfigTdpLevel              | * | 0x01 | 0x00

  # !BSF NAME:{Max P-State Ratio}
  # !BSF TYPE:{EditNum, HEX, (0x00, 0x7F)}
  # !BSF HELP:{Max P-State Ratio, Valid Range 0 to 0x7F}
  gPlatformFspPkgTokenSpaceGuid.MaxRatio                    | * | 0x01 | 0x00

  # !BSF NAME:{P-state ratios for custom P-state table} TYPE:{EditNum, HEX, (0x00,0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF)}
  # !HDR STRUCT:{UINT8}
  # !BSF HELP:{P-state ratios for custom P-state table. NumberOfEntries has valid range between 0 to 40. For no. of P-States supported(NumberOfEntries) , StateRatio[NumberOfEntries] are configurable. Valid Range of each entry is 0 to 0x7F}
  gPlatformFspPkgTokenSpaceGuid.StateRatio                  | * | 0x28 | {0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0}

  # !BSF NAME:{P-state ratios for max 16 version of custom P-state table} TYPE:{EditNum, HEX, (0x00,0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF)}
  # !HDR STRUCT:{UINT8}
  # !BSF HELP:{P-state ratios for max 16 version of custom P-state table. This table is used for OS versions limited to a max of 16 P-States. If the first entry of this table is 0, or if Number of Entries is 16 or less, then this table will be ignored, and up to the top 16 values of the StateRatio table will be used instead. Valid Range of each entry is 0 to 0x7F}
  gPlatformFspPkgTokenSpaceGuid.StateRatioMax16             | * | 0x10 | {0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0}

  # !BSF NAME:{Platform Power Pmax}
  # !BSF TYPE:{EditNum, HEX, (0x00, 0x400)}
  # !BSF HELP:{PCODE MMIO Mailbox: Platform Power Pmax. <b>0 - Auto</b> Specified in 1/8 Watt increments. Range 0-1024 Watts. Value of 800 = 100W}
  gPlatformFspPkgTokenSpaceGuid.PsysPmax                    | * | 0x02 | 0xAC


  # !BSF NAME:{Interrupt Response Time Limit of C-State LatencyContol1}
  # !BSF TYPE:{EditNum, HEX, (0x00, 0x3FF)}
  # !BSF HELP:{Interrupt Response Time Limit of C-State LatencyContol1.Range of value 0 to 0x3FF. 0 is Auto.}
  gPlatformFspPkgTokenSpaceGuid.CstateLatencyControl1Irtl   | * | 0x02 | 0

  # !BSF NAME:{Interrupt Response Time Limit of C-State LatencyContol2}
  # !BSF TYPE:{EditNum, HEX, (0x00, 0x3FF)}
  # !BSF HELP:{Interrupt Response Time Limit of C-State LatencyContol2.Range of value 0 to 0x3FF. 0 is Auto.}
  gPlatformFspPkgTokenSpaceGuid.CstateLatencyControl2Irtl   | * | 0x02 | 0

  # !BSF NAME:{Interrupt Response Time Limit of C-State LatencyContol3}
  # !BSF TYPE:{EditNum, HEX, (0x00, 0x3FF)}
  # !BSF HELP:{Interrupt Response Time Limit of C-State LatencyContol3.Range of value 0 to 0x3FF. 0 is Auto.}
  gPlatformFspPkgTokenSpaceGuid.CstateLatencyControl3Irtl   | * | 0x02 | 0

  # !BSF NAME:{Interrupt Response Time Limit of C-State LatencyContol4}
  # !BSF TYPE:{EditNum, HEX, (0x00, 0x3FF)}
  # !BSF HELP:{Interrupt Response Time Limit of C-State LatencyContol4.Range of value 0 to 0x3FF. 0 is Auto.}
  gPlatformFspPkgTokenSpaceGuid.CstateLatencyControl4Irtl   | * | 0x02 | 0

  # !BSF NAME:{Interrupt Response Time Limit of C-State LatencyContol5}
  # !BSF TYPE:{EditNum, HEX, (0x00, 0x3FF)}
  # !BSF HELP:{Interrupt Response Time Limit of C-State LatencyContol5.Range of value 0 to 0x3FF. 0 is Auto.}
  gPlatformFspPkgTokenSpaceGuid.CstateLatencyControl5Irtl   | * | 0x02 | 0

  # !BSF NAME:{Package Long duration turbo mode power limit}
  # !BSF TYPE:{EditNum, HEX, (0x00, 0x3E7F83)}
  # !BSF HELP:{Package Long duration turbo mode power limit. Units are based on POWER_MGMT_CONFIG.CustomPowerUnit. Valid Range 0 to 4095875 in Step size of 125}
  gPlatformFspPkgTokenSpaceGuid.PowerLimit1                 | * | 0x04 | 0x0

  # !BSF NAME:{Package Short duration turbo mode power limit}
  # !BSF TYPE:{EditNum, HEX, (0x00, 0x3E7F83)}
  # !BSF HELP:{Package Short duration turbo mode power limit. Units are based on POWER_MGMT_CONFIG.CustomPowerUnit.Valid Range 0 to 4095875 in Step size of 125}
  gPlatformFspPkgTokenSpaceGuid.PowerLimit2Power            | * | 0x04 | 0x0

  # !BSF NAME:{Package PL3 power limit}
  # !BSF TYPE:{EditNum, HEX, (0x00, 0x3E7F83)}
  # !BSF HELP:{Package PL3 power limit. Units are based on POWER_MGMT_CONFIG.CustomPowerUnit.Valid Range 0 to 4095875 in Step size of 125}
  gPlatformFspPkgTokenSpaceGuid.PowerLimit3                 | * | 0x04 | 0x0

  # !BSF NAME:{Package PL4 power limit}
  # !BSF TYPE:{EditNum, HEX, (0x00, 0x3E7F83)}
  # !BSF HELP:{Package PL4 power limit. Units are based on POWER_MGMT_CONFIG.CustomPowerUnit.Valid Range 0 to 4095875 in Step size of 125}
  gPlatformFspPkgTokenSpaceGuid.PowerLimit4                 | * | 0x04 | 0x0

  # !BSF NAME:{Tcc Offset Time Window for RATL}
  # !BSF TYPE:{EditNum, HEX, (0x00, 0xFFFFFFFF)}
  # !BSF HELP:{Tcc Offset Time Window can range from 5ms to 448000ms for Runtime Average Temperature Limit (RATL).For SKL Y SKU, the recommended default for this policy is <b>5000: 5 seconds</b>, For all other SKUs the recommended default are <b>0: Disabled</b>
  # 0: Disabled,5: 5ms , 10:10ms , 55:55ms , 156:156ms , 375:375ms , 500:500ms,750:750ms ,1000:1s,2000:2s,3000:3s,4000:4s,5000:5s,6000:6s,7000:7s,8000:8s,10000:10s,12000:12s,14000:14s,16000:16s,20000:20s,24000:24s,28000:28s,32000:32s,40000:40s,48000:48s,56000:56s,64000:64s,80000:80s,96000:96s,112000:112s,128000:128s,160000:160s,192000:192s,224000:224s,256000:256s,320000:320s,384000:384s,448000:448s}
  gPlatformFspPkgTokenSpaceGuid.TccOffsetTimeWindowForRatl  | * | 0x04 | 0x00

  # !BSF NAME:{Short term Power Limit value for custom cTDP level 1}
  # !BSF TYPE:{EditNum, HEX, (0x00, 0x3E7F83)}
  # !BSF HELP:{Short term Power Limit value for custom cTDP level 1. Units are based on POWER_MGMT_CONFIG.CustomPowerUnit.Valid Range 0 to 4095875 in Step size of 125}
  gPlatformFspPkgTokenSpaceGuid.Custom1PowerLimit1          | * | 0x04 | 0x0

  # !BSF NAME:{Long term Power Limit value for custom cTDP level 1}
  # !BSF TYPE:{EditNum, HEX, (0x00, 0x3E7F83)}
  # !BSF HELP:{Long term Power Limit value for custom cTDP level 1. Units are based on POWER_MGMT_CONFIG.CustomPowerUnit.Valid Range 0 to 4095875 in Step size of 125}
  gPlatformFspPkgTokenSpaceGuid.Custom1PowerLimit2          | * | 0x04 | 0x0


  # !BSF NAME:{Short term Power Limit value for custom cTDP level 2}
  # !BSF TYPE:{EditNum, HEX, (0x00, 0x3E7F83)}
  # !BSF HELP:{Short term Power Limit value for custom cTDP level 2. Units are based on POWER_MGMT_CONFIG.CustomPowerUnit.Valid Range 0 to 4095875 in Step size of 125}
  gPlatformFspPkgTokenSpaceGuid.Custom2PowerLimit1          | * | 0x04 | 0x0

  # !BSF NAME:{Long term Power Limit value for custom cTDP level 2}
  # !BSF TYPE:{EditNum, HEX, (0x00, 0x3E7F83)}
  # !BSF HELP:{Long term Power Limit value for custom cTDP level 2. Units are based on POWER_MGMT_CONFIG.CustomPowerUnit.Valid Range 0 to 4095875 in Step size of 125}
  gPlatformFspPkgTokenSpaceGuid.Custom2PowerLimit2          | * | 0x04 | 0x0

  # !BSF NAME:{Short term Power Limit value for custom cTDP level 3}
  # !BSF TYPE:{EditNum, HEX, (0x00, 0x3E7F83)}
  # !BSF HELP:{Short term Power Limit value for custom cTDP level 3. Units are based on POWER_MGMT_CONFIG.CustomPowerUnit.Valid Range 0 to 4095875 in Step size of 125}
  gPlatformFspPkgTokenSpaceGuid.Custom3PowerLimit1          | * | 0x04 | 0x0

  # !BSF NAME:{Long term Power Limit value for custom cTDP level 3}
  # !BSF TYPE:{EditNum, HEX, (0x00, 0x3E7F83)}
  # !BSF HELP:{Long term Power Limit value for custom cTDP level 3. Units are based on POWER_MGMT_CONFIG.CustomPowerUnit.Valid Range 0 to 4095875 in Step size of 125}
  gPlatformFspPkgTokenSpaceGuid.Custom3PowerLimit2          | * | 0x04 | 0x0

  # !BSF NAME:{Platform PL1 power}
  # !BSF TYPE:{EditNum, HEX, (0x00, 0x3E7F83)}
  # !BSF HELP:{Platform PL1 power. Units are based on POWER_MGMT_CONFIG.CustomPowerUnit.Valid Range 0 to 4095875 in Step size of 125}
  gPlatformFspPkgTokenSpaceGuid.PsysPowerLimit1Power        | * | 0x04 | 0x00

  # !BSF NAME:{Platform PL2 power}
  # !BSF TYPE:{EditNum, HEX, (0x00, 0x3E7F83)}
  # !BSF HELP:{Platform PL2 power. Units are based on POWER_MGMT_CONFIG.CustomPowerUnit.Valid Range 0 to 4095875 in Step size of 125}
  gPlatformFspPkgTokenSpaceGuid.PsysPowerLimit2Power        | * | 0x04 | 0x00

  # !BSF NAME:{Race To Halt} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Enable/Disable Race To Halt feature. RTH will dynamically increase CPU frequency in order to enter pkg C-State faster to reduce overall power. (RTH is controlled through MSR 1FC bit 20)Disable; <b>1: Enable</b>}
  gPlatformFspPkgTokenSpaceGuid.RaceToHalt                  | * | 0x01 | 0x01

  # !BSF NAME:{Set Three Strike Counter Disable} TYPE:{Combo} OPTION:{0: False, 1: True}
  # !BSF HELP:{False (default): Three Strike counter will be incremented and True: Prevents Three Strike counter from incrementing; <b>0: False</b>; 1: True.}
  gPlatformFspPkgTokenSpaceGuid.ThreeStrikeCounterDisable   | * | 0x01 | 0x00

  # !BSF NAME:{Set HW P-State Interrupts Enabled for for MISC_PWR_MGMT} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Set HW P-State Interrupts Enabled for for MISC_PWR_MGMT; <b>0: Disable</b>; 1: Enable.}
  gPlatformFspPkgTokenSpaceGuid.HwpInterruptControl         | * | 0x01 | 0x01

  # !BSF NAME:{ReservedCpuPostMem2} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Reserved for CPU Post-Mem 2}
  gPlatformFspPkgTokenSpaceGuid.ReservedCpuPostMem2         | * | 0x4 | {0x00}

  # !BSF NAME:{Intel Turbo Boost Max Technology 3.0} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Intel Turbo Boost Max Technology 3.0. 0: Disabled; <b>1: Enabled</b>}
  gPlatformFspPkgTokenSpaceGuid.EnableItbm                   | * | 0x01 | 0x01

  # !BSF NAME:{Enable or Disable C1 Cstate Demotion} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Enable or Disable C1 Cstate Demotion. Disable; <b>1: Enable</b>}
  gPlatformFspPkgTokenSpaceGuid.C1StateAutoDemotion           | * | 0x01 | 0x01

  # !BSF NAME:{Enable or Disable C1 Cstate UnDemotion} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Enable or Disable C1 Cstate UnDemotion. Disable; <b>1: Enable</b>}
  gPlatformFspPkgTokenSpaceGuid.C1StateUnDemotion         | * | 0x01 | 0x01

  # !BSF NAME:{Minimum Ring ratio limit override}
  # !BSF TYPE:{EditNum, HEX, (0x00, 0x53)}
  # !BSF HELP:{Minimum Ring ratio limit override. <b>0: Hardware defaults.</b> Range: 0 - Max turbo ratio limit}
  gPlatformFspPkgTokenSpaceGuid.MinRingRatioLimit           | * | 0x01 | 0x00

  # !BSF NAME:{Maximum Ring ratio limit override}
  # !BSF TYPE:{EditNum, HEX, (0x00, 0x53)}
  # !BSF HELP:{Maximum Ring ratio limit override. <b>0: Hardware defaults.</b> Range: 0 - Max turbo ratio limit}
  gPlatformFspPkgTokenSpaceGuid.MaxRingRatioLimit           | * | 0x01 | 0x00

  # !BSF NAME:{Enable or Disable Per Core P State OS control} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Enable or Disable Per Core P State OS control. 0: Disable; <b>1: Enable</b>}
  gPlatformFspPkgTokenSpaceGuid.EnablePerCorePState        | * | 0x01 | 0x01

  # !BSF NAME:{Enable or Disable HwP Autonomous Per Core P State OS control} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Enable or Disable HwP Autonomous Per Core P State OS control. 0: Disable; <b>1: Enable</b>}
  gPlatformFspPkgTokenSpaceGuid.EnableHwpAutoPerCorePstate | * | 0x01 | 0x01

  # !BSF NAME:{Enable or Disable HwP Autonomous EPP Grouping} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Enable or Disable HwP Autonomous EPP Grouping. 0: Disable; <b>1: Enable</b>}
  gPlatformFspPkgTokenSpaceGuid.EnableHwpAutoEppGrouping | * | 0x01 | 0x01

  # !BSF NAME:{Enable or Disable EPB override over PECI} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Enable or Disable EPB override over PECI. <b>0: Disable;</b> 1: Enable}
  gPlatformFspPkgTokenSpaceGuid.EnableEpbPeciOverride      | * | 0x01 | 0x00

  # !BSF NAME:{Enable or Disable Fast MSR for IA32_HWP_REQUEST} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Enable or Disable Fast MSR for IA32_HWP_REQUEST. 0: Disable;<b> 1: Enable</b>}
  gPlatformFspPkgTokenSpaceGuid.EnableFastMsrHwpReq        | * | 0x01 | 0x00

  # !BSF NAME:{Enable Configurable TDP} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Applies TDP initialization settings based on non-cTDP or cTDP.; 0: Applies to non-cTDP; <b>1: Applies to cTDP</b>}
  gPlatformFspPkgTokenSpaceGuid.ApplyConfigTdp             | * | 0x01 | 0x01

  # !BSF NAME:{Misc Power Management MSR Lock} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Lock Misc Power Management MSR. Enable/Disable; 0: Disable , <b> 1: Enable </b>}
  gPlatformFspPkgTokenSpaceGuid.HwpLock                    | * | 0x01 | 0x01

  # !BSF NAME:{Dual Tau Boost} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Enable, Disable Dual Tau Boost feature. This is only applicable for Desktop; <b>0: Disable</b>; 1: Enable}
  gPlatformFspPkgTokenSpaceGuid.DualTauBoost                | * | 0x01 | 0x00

  # !BSF NAME:{ReservedCpuPostMemTest} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Reserved for CPU Post-Mem Test}
  gPlatformFspPkgTokenSpaceGuid.ReservedCpuPostMemTest      | * | 0x10 | {0x00}

  #
  # CPU Post-Mem Block End
  #

  ###########Security Test Policies Start ###########################################

  gPlatformFspPkgTokenSpaceGuid.SecurityPostMemRsvd         | * | 0x10 | {0x00}
  ###########Security Test Policies End ###########################################

  #
  # ME Post-Mem Block Begin
  #
  # !BSF PAGE:{ME2}
  # !BSF NAME:{End of Post message} TYPE:{Combo}
  # !BSF OPTION:{0:Disable, 1:Send in PEI, 2:Send in DXE, 3:Reserved}
  # !BSF HELP:{Test, Send End of Post message. Disable(0x0): Disable EOP message, Send in PEI(0x1): EOP send in PEI, Send in DXE(0x2)(Default): EOP send in DXE}
  gPlatformFspPkgTokenSpaceGuid.EndOfPostMessage            | * | 0x01 | 0x2

  # !BSF NAME:{D0I3 Setting for HECI Disable} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Test, 0: disable, 1: enable, Setting this option disables setting D0I3 bit for all HECI devices}
  gPlatformFspPkgTokenSpaceGuid.DisableD0I3SettingForHeci   | * | 0x01 | 0x0

  # !BSF NAME:{Mctp Broadcast Cycle} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Test, Determine if MCTP Broadcast is enabled <b>0: Disable</b>; 1: Enable.}
  gPlatformFspPkgTokenSpaceGuid.MctpBroadcastCycle          | * | 0x01 | 0x0
  #
  # ME Post-Mem Block End
  #

  #
  #  PCH Silicon Policies start
  #

  # !BSF PAGE:{PCH2}
  # !BSF NAME:{Enable LOCKDOWN SMI} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Enable SMI_LOCK bit to prevent writes to the Global SMI Enable bit.}
  gPlatformFspPkgTokenSpaceGuid.PchLockDownGlobalSmi        | * | 0x01 | 0x01

  # !BSF NAME:{Enable LOCKDOWN BIOS Interface} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Enable BIOS Interface Lock Down bit to prevent writes to the Backup Control Register.}
  gPlatformFspPkgTokenSpaceGuid.PchLockDownBiosInterface    | * | 0x01 | 0x01

  # !BSF NAME:{Unlock all GPIO pads} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Force all GPIO pads to be unlocked for debug purpose.}
  gPlatformFspPkgTokenSpaceGuid.PchUnlockGpioPads           | * | 0x01 | 0x00

  # !BSF NAME:{PCH Unlock SideBand access} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{The SideBand PortID mask for certain end point (e.g. PSFx) will be locked before 3rd party code execution. 0: Lock SideBand access; 1: Unlock SideBand access.}
  gPlatformFspPkgTokenSpaceGuid.PchSbAccessUnlock           | * | 0x01 | 0x00

  # !BSF NAME:{PCIE RP Ltr Max Snoop Latency} TYPE:{EditNum, HEX, (0x00,0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF)}
  # !HDR STRUCT:{UINT16}
  # !BSF HELP:{Latency Tolerance Reporting, Max Snoop Latency.}
  gPlatformFspPkgTokenSpaceGuid.PcieRpLtrMaxSnoopLatency    | * | 0x38 | { 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000 }

  # !BSF NAME:{PCIE RP Ltr Max No Snoop Latency} TYPE:{EditNum, HEX, (0x00,0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF)}
  # !HDR STRUCT:{UINT16}
  # !BSF HELP:{Latency Tolerance Reporting, Max Non-Snoop Latency.}
  gPlatformFspPkgTokenSpaceGuid.PcieRpLtrMaxNoSnoopLatency  | * | 0x38 | { 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000 }

  # !BSF NAME:{PCIE RP Snoop Latency Override Mode} TYPE:{EditNum, HEX, (0x00,0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF)}
  # !BSF HELP:{Latency Tolerance Reporting, Snoop Latency Override Mode.}
  gPlatformFspPkgTokenSpaceGuid.PcieRpSnoopLatencyOverrideMode          | * | 0x1C | { 0x02, 0x02, 0x02, 0x02, 0x02, 0x02, 0x02, 0x02, 0x02, 0x02, 0x02, 0x02, 0x02, 0x02, 0x02, 0x02, 0x02, 0x02, 0x02, 0x02, 0x02, 0x02, 0x02, 0x02, 0x02, 0x02, 0x02, 0x02 }

  # !BSF NAME:{PCIE RP Snoop Latency Override Multiplier} TYPE:{EditNum, HEX, (0x00,0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF)}
  # !BSF HELP:{Latency Tolerance Reporting, Snoop Latency Override Multiplier.}
  gPlatformFspPkgTokenSpaceGuid.PcieRpSnoopLatencyOverrideMultiplier    | * | 0x1C | { 0x02, 0x02, 0x02, 0x02, 0x02, 0x02, 0x02, 0x02, 0x02, 0x02, 0x02, 0x02, 0x02, 0x02, 0x02, 0x02, 0x02, 0x02, 0x02, 0x02, 0x02, 0x02, 0x02, 0x02, 0x02, 0x02, 0x02, 0x02 }

  # !BSF NAME:{PCIE RP Snoop Latency Override Value} TYPE:{EditNum, HEX, (0x00,0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF)}
  # !HDR STRUCT:{UINT16}
  # !BSF HELP:{Latency Tolerance Reporting, Snoop Latency Override Value.}
  gPlatformFspPkgTokenSpaceGuid.PcieRpSnoopLatencyOverrideValue         | * | 0x38 | { 0x003C, 0x003C, 0x003C, 0x003C, 0x003C, 0x003C, 0x003C, 0x003C, 0x003C, 0x003C, 0x003C, 0x003C, 0x003C, 0x003C, 0x003C, 0x003C, 0x003C, 0x003C, 0x003C, 0x003C, 0x003C, 0x003C, 0x003C, 0x003C, 0x003C, 0x003C, 0x003C, 0x003C }

  # !BSF NAME:{PCIE RP Non Snoop Latency Override Mode} TYPE:{EditNum, HEX, (0x00,0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF)}
  # !BSF HELP:{Latency Tolerance Reporting, Non-Snoop Latency Override Mode.}
  gPlatformFspPkgTokenSpaceGuid.PcieRpNonSnoopLatencyOverrideMode       | * | 0x1C | { 0x02, 0x02, 0x02, 0x02, 0x02, 0x02, 0x02, 0x02, 0x02, 0x02, 0x02, 0x02, 0x02, 0x02, 0x02, 0x02, 0x02, 0x02, 0x02, 0x02, 0x02, 0x02, 0x02, 0x02, 0x02, 0x02, 0x02, 0x02 }

  # !BSF NAME:{PCIE RP Non Snoop Latency Override Multiplier} TYPE:{EditNum, HEX, (0x00,0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF)}
  # !BSF HELP:{Latency Tolerance Reporting, Non-Snoop Latency Override Multiplier.}
  gPlatformFspPkgTokenSpaceGuid.PcieRpNonSnoopLatencyOverrideMultiplier | * | 0x1C | { 0x02, 0x02, 0x02, 0x02, 0x02, 0x02, 0x02, 0x02, 0x02, 0x02, 0x02, 0x02, 0x02, 0x02, 0x02, 0x02, 0x02, 0x02, 0x02, 0x02, 0x02, 0x02, 0x02, 0x02, 0x02, 0x02, 0x02, 0x02 }

  # !BSF NAME:{PCIE RP Non Snoop Latency Override Value} TYPE:{EditNum, HEX, (0x00,0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF)}
  # !HDR STRUCT:{UINT16}
  # !BSF HELP:{Latency Tolerance Reporting, Non-Snoop Latency Override Value.}
  gPlatformFspPkgTokenSpaceGuid.PcieRpNonSnoopLatencyOverrideValue      | * | 0x38 | { 0x003C, 0x003C, 0x003C, 0x003C, 0x003C, 0x003C, 0x003C, 0x003C, 0x003C, 0x003C, 0x003C, 0x003C, 0x003C, 0x003C, 0x003C, 0x003C, 0x003C, 0x003C, 0x003C, 0x003C, 0x003C, 0x003C, 0x003C, 0x003C, 0x003C, 0x003C, 0x003C, 0x003C }

  # !BSF NAME:{PCIE RP Slot Power Limit Scale} TYPE:{EditNum, HEX, (0x00,0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF)}
  # !BSF HELP:{Specifies scale used for slot power limit value. Leave as 0 to set to default.}
  gPlatformFspPkgTokenSpaceGuid.PcieRpSlotPowerLimitScale   | * | 0x1C | { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 }

  # !BSF NAME:{PCIE RP Slot Power Limit Value} TYPE:{EditNum, HEX, (0x00,0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF)}
  # !HDR STRUCT:{UINT16}
  # !BSF HELP:{Specifies upper limit on power supplie by slot. Leave as 0 to set to default.}
  gPlatformFspPkgTokenSpaceGuid.PcieRpSlotPowerLimitValue   | * | 0x38 | { 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000 }

  # !BSF NAME:{PCIE RP Enable Port8xh Decode} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{This member describes whether PCIE root port Port 8xh Decode is enabled. 0: Disable; 1: Enable.}
  gPlatformFspPkgTokenSpaceGuid.PcieEnablePort8xhDecode     | * | 0x01 | 0x00

  # !BSF NAME:{PCIE Port8xh Decode Port Index} TYPE:{EditNum, HEX, (0x0,0xFF)}
  # !BSF HELP:{The Index of PCIe Port that is selected for Port8xh Decode (0 Based).}
  gPlatformFspPkgTokenSpaceGuid.PchPciePort8xhDecodePortIndex  | * | 0x01 | 0x00

  # !BSF NAME:{PCH Energy Reporting} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Disable/Enable PCH to CPU energy report feature.}
  gPlatformFspPkgTokenSpaceGuid.PchPmDisableEnergyReport    | * | 0x01 | 0x00

  # !BSF NAME:{PCH Sata Test Mode} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Allow entrance to the PCH SATA test modes.}
  gPlatformFspPkgTokenSpaceGuid.SataTestMode                | * | 0x01 | 0x00

  # !BSF NAME:{PCH USB OverCurrent mapping lock enable} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{If this policy option is enabled then BIOS will program OCCFDONE bit in xHCI meaning that OC mapping data will be consumed by xHCI and OC mapping registers will be locked.}
  gPlatformFspPkgTokenSpaceGuid.PchXhciOcLock               | * | 0x01 | 0x01

!if gSiPkgTokenSpaceGuid.PcdAdlLpSupport == TRUE
  # !BSF NAME:{Low Power Mode Enable/Disable config mask} TYPE:{EditNum, HEX, (0x00,0xFF)}
  # !BSF HELP:{Configure if respective S0i2/3 sub-states are to be supported. Each bit corresponds to one sub-state (LPMx - BITx): LPM0-s0i2.0, LPM1-s0i2.1, LPM2-s0i2.2, LPM3-s0i3.0, LPM4-s0i3.1, LPM5-s0i3.2, LPM6-s0i3.3, LPM7-s0i3.4.}
  gPlatformFspPkgTokenSpaceGuid.PmcLpmS0ixSubStateEnableMask | * | 0x01 | 0x9
!else
  # !BSF NAME:{Low Power Mode Enable/Disable config mask} TYPE:{EditNum, HEX, (0x00,0xFF)}
  # !BSF HELP:{Configure if respective S0i2/3 sub-states are to be supported. Each bit corresponds to one sub-state (LPMx - BITx): LPM0-s0i2.0, LPM1-s0i2.1, LPM2-s0i2.2, LPM3-s0i3.0, LPM4-s0i3.1, LPM5-s0i3.2, LPM6-s0i3.3, LPM7-s0i3.4.}
  gPlatformFspPkgTokenSpaceGuid.PmcLpmS0ixSubStateEnableMask | * | 0x01 | 0x3
!endif
  #
  #  PCH Silicon Policies End
  #
#--!endif

  #
  #  SA Post-Mem PCIE Policies start
  #
  # !BSF PAGE:{SA2}
  # !BSF NAME:{PCIE RP Ltr Max Snoop Latency} TYPE:{EditNum, HEX, (0x00,0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF)}
  # !HDR STRUCT:{UINT16}
  # !BSF HELP:{Latency Tolerance Reporting, Max Snoop Latency.}
  gPlatformFspPkgTokenSpaceGuid.CpuPcieRpLtrMaxSnoopLatency    | * | 0x8 | { 0x100F, 0x100F, 0x100F, 0x100F }

  # !BSF NAME:{PCIE RP Ltr Max No Snoop Latency} TYPE:{EditNum, HEX, (0x00,0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF)}
  # !HDR STRUCT:{UINT16}
  # !BSF HELP:{Latency Tolerance Reporting, Max Non-Snoop Latency.}
  gPlatformFspPkgTokenSpaceGuid.CpuPcieRpLtrMaxNoSnoopLatency  | * | 0x8 | { 0x100F, 0x100F, 0x100F, 0x100F }

  # !BSF NAME:{PCIE RP Snoop Latency Override Mode} TYPE:{EditNum, HEX, (0x00,0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF)}
  # !BSF HELP:{Latency Tolerance Reporting, Snoop Latency Override Mode.}
  gPlatformFspPkgTokenSpaceGuid.CpuPcieRpSnoopLatencyOverrideMode          | * | 0x4 | { 0x02, 0x02, 0x02, 0x02 }

  # !BSF NAME:{PCIE RP Snoop Latency Override Multiplier} TYPE:{EditNum, HEX, (0x00,0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF)}
  # !BSF HELP:{Latency Tolerance Reporting, Snoop Latency Override Multiplier.}
  gPlatformFspPkgTokenSpaceGuid.CpuPcieRpSnoopLatencyOverrideMultiplier    | * | 0x4 | { 0x02, 0x02, 0x02, 0x02 }

  # !BSF NAME:{PCIE RP Snoop Latency Override Value} TYPE:{EditNum, HEX, (0x00,0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF)}
  # !HDR STRUCT:{UINT16}
  # !BSF HELP:{Latency Tolerance Reporting, Snoop Latency Override Value.}
  gPlatformFspPkgTokenSpaceGuid.CpuPcieRpSnoopLatencyOverrideValue         | * | 0x8 | { 0x003C, 0x003C, 0x003C, 0x003C }

  # !BSF NAME:{PCIE RP Non Snoop Latency Override Mode} TYPE:{EditNum, HEX, (0x00,0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF)}
  # !BSF HELP:{Latency Tolerance Reporting, Non-Snoop Latency Override Mode.}
  gPlatformFspPkgTokenSpaceGuid.CpuPcieRpNonSnoopLatencyOverrideMode       | * | 0x4 | { 0x02, 0x02, 0x02, 0x02 }

  # !BSF NAME:{PCIE RP Non Snoop Latency Override Multiplier} TYPE:{EditNum, HEX, (0x00,0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF)}
  # !BSF HELP:{Latency Tolerance Reporting, Non-Snoop Latency Override Multiplier.}
  gPlatformFspPkgTokenSpaceGuid.CpuPcieRpNonSnoopLatencyOverrideMultiplier | * | 0x4 | { 0x02, 0x02, 0x02, 0x02 }

  # !BSF NAME:{PCIE RP Non Snoop Latency Override Value} TYPE:{EditNum, HEX, (0x00,0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF)}
  # !HDR STRUCT:{UINT16}
  # !BSF HELP:{Latency Tolerance Reporting, Non-Snoop Latency Override Value.}
  gPlatformFspPkgTokenSpaceGuid.CpuPcieRpNonSnoopLatencyOverrideValue      | * | 0x8 | { 0x003C, 0x003C, 0x003C, 0x003C }

  # !BSF NAME:{PCIE RP Upstream Port Transmiter Preset} TYPE:{EditNum, HEX, (0x00,0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF)}
  # !BSF HELP:{Used during Gen3 Link Equalization. Used for all lanes.  Default is 7.}
  gPlatformFspPkgTokenSpaceGuid.CpuPcieRpGen3Uptp                  | * | 0x4 | { 0x07, 0x07, 0x07, 0x07 }

  # !BSF NAME:{PCIE RP Downstream Port Transmiter Preset} TYPE:{EditNum, HEX, (0x00,0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF)}
  # !BSF HELP:{Used during Gen3 Link Equalization. Used for all lanes.  Default is 7.}
  gPlatformFspPkgTokenSpaceGuid.CpuPcieRpGen3Dptp                  | * | 0x4 | { 0x07, 0x07, 0x07, 0x07 }

  # !BSF NAME:{PCIE RP Upstream Port Transmiter Preset} TYPE:{EditNum, HEX, (0x00,0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF)}
  # !BSF HELP:{Used during Gen4 Link Equalization. Used for all lanes.  Default is 8.}
  gPlatformFspPkgTokenSpaceGuid.CpuPcieRpGen4Uptp                  | * | 0x4 | { 0x08, 0x08, 0x08, 0x08 }

  # !BSF NAME:{PCIE RP Downstream Port Transmiter Preset} TYPE:{EditNum, HEX, (0x00,0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF)}
  # !BSF HELP:{Used during Gen4 Link Equalization. Used for all lanes.  Default is 9.}
  gPlatformFspPkgTokenSpaceGuid.CpuPcieRpGen4Dptp                  | * | 0x4 | { 0x09, 0x09, 0x09, 0x09 }

  # !BSF NAME:{PCIE RP Upstream Port Transmiter Preset} TYPE:{EditNum, HEX, (0x00,0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF)}
  # !BSF HELP:{Used during Gen5 Link Equalization. Used for all lanes.  Default is 7.}
  gPlatformFspPkgTokenSpaceGuid.CpuPcieRpGen5Uptp                  | * | 0x4 | { 0x07, 0x07, 0x07, 0x07 }

  # !BSF NAME:{PCIE RP Downstream Port Transmiter Preset} TYPE:{EditNum, HEX, (0x00,0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF)}
  # !BSF HELP:{Used during Gen5 Link Equalization. Used for all lanes.  Default is 7.}
  gPlatformFspPkgTokenSpaceGuid.CpuPcieRpGen5Dptp                  | * | 0x4 | { 0x07, 0x07, 0x07, 0x07 }
  #
  #  SA Post-Mem PCIE Policies End
  #

  # !BSF PAGE:{TCSS2}
  # !BSF NAME:{Type C Port x Convert to TypeA} TYPE:{Combo} OPTION:{$EN_DIS}
  # !HDR STRUCT:{UINT8}
  # !BSF HELP:{Enable / Disable(default) Type C Port x Convert to TypeA}
  gPlatformFspPkgTokenSpaceGuid.EnableTcssCovTypeA     | * | 0x4 | {0x00, 0x00, 0x00, 0x00}

  # !BSF NAME:{PCH xhci port x for Type C Port x mapping} TYPE:{EditNum, HEX, (0x0,0xFF)}
  # !HDR STRUCT:{UINT8}
  # !BSF HELP:{input PCH xhci port x for Type C Port 0 mapping.}
  gPlatformFspPkgTokenSpaceGuid.MappingPchXhciUsbA     | * | 0x4 | {0x00, 0x00, 0x00, 0x00}

  #
  # SA Post-Mem PCIE Policies 2 Start
  #

  # !BSF PAGE:{SA2}
  # !BSF NAME:{FOMS Control Policy} TYPE:{Combo}
  # !BSF OPTION:{0: Auto, 1: Gen3 Foms, 2: Gen4 Foms, 3: Gen3 and Gen4 Foms}
  # !BSF HELP:{Choose the Foms Control Policy, <b>Default = 0 </b>}
  gPlatformFspPkgTokenSpaceGuid.CpuPcieFomsCp  | * | 0x04 | { 0x00, 0x00, 0x00, 0x00 }

  # !BSF NAME:{PMC C10 dynamic threshold dajustment enable} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Set if you want to enable PMC C10 dynamic threshold adjustment. Only works on supported SKUs}
  gPlatformFspPkgTokenSpaceGuid.PmcC10DynamicThresholdAdjustment | * | 0x1 | 0x0

  # !BSF NAME:{P2P mode for PCIE RP} TYPE:{Combo} OPTION:{0: Disable, 1: Enable}
  # !BSF HELP:{Enable/disable peer to peer mode for PCIE Root Ports. 0: Disable, 1: Enable.}
  gPlatformFspPkgTokenSpaceGuid.CpuPcieRpPeerToPeerMode  | * | 0x04 | { 0x00, 0x00, 0x00, 0x00 }

  #
  # SA Post-Mem PCIE Policies 2 End
  #

  #
  # Turbo Ratio Limit Policies Start
  #
  # !BSF NAME:{Turbo Ratio Limit Ratio array} TYPE:{EditNum, HEX, (0x00,0xFFFFFFFFFFFFFFFF)}
  # !BSF HELP:{TurboRatioLimitRatio[7-0] will pair with TurboRatioLimitNumCore[7-0] to determine the active core ranges for each frequency point.}
  gPlatformFspPkgTokenSpaceGuid.TurboRatioLimitRatio        | * | 0x8 | {0x00}
  # !BSF NAME:{Turbo Ratio Limit Num Core array} TTYPE:{EditNum, HEX, (0x00,0xFFFFFFFFFFFFFFFF)}
  # !BSF HELP:{TurboRatioLimitNumCore[7-0] will pair with TurboRatioLimitRatio[7-0] to determine the active core ranges for each frequency point.}
  gPlatformFspPkgTokenSpaceGuid.TurboRatioLimitNumCore      | * | 0x8 | {0x00}
  # !BSF NAME:{ATOM Turbo Ratio Limit Ratio array} TYPE:{EditNum, HEX, (0x00,0xFFFFFFFFFFFFFFFF)}
  # !BSF HELP:{AtomTurboRatioLimitRatio[7-0] will pair with AtomTurboRatioLimitNumCore[7-0] to determine the active core ranges for each frequency point.}
  gPlatformFspPkgTokenSpaceGuid.AtomTurboRatioLimitRatio    | * | 0x8 | {0x00}
  # !BSF NAME:{ATOM Turbo Ratio Limit Num Core array} TYPE:{EditNum, HEX, (0x00,0xFFFFFFFFFFFFFFFF)}
  # !BSF HELP:{AtomTurboRatioLimitNumCore[7-0] will pair with AtomTurboRatioLimitRatio[7-0] to determine the active core ranges for each frequency point.}
  gPlatformFspPkgTokenSpaceGuid.AtomTurboRatioLimitNumCore  | * | 0x8 | {0x00}
  #
  # Turbo Ratio Limit Policies End
  #

  # !BSF NAME:{FspEventHandler} TYPE:{EditNum, HEX, (0x0,0xFFFFFFFF)}
  # !BSF HELP:{<b>Optional</b> pointer to the boot loader's implementation of FSP_EVENT_HANDLER.}
  gPlatformFspPkgTokenSpaceGuid.FspEventHandler              | * | 0x04 | 0x0


!if gSiPkgTokenSpaceGuid.PcdEmbeddedEnable == 0x1
  # !BSF NAME:{Skip setting BIOS_DONE When Fw Update.} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{When set to TRUE and boot mode is BOOT_ON_FLASH_UPDATE,skip setting BIOS_DONE MSR at EndofPei. Note: BIOS_DONE MSR should be set in later phase before executing 3rd party code if SiSkipBiosDoneWhenFwUpdate set to TRUE.}
  gPlatformFspPkgTokenSpaceGuid.SiSkipBiosDoneWhenFwUpdate  | * | 0x01 | 0x00

  # !BSF NAME:{Enable VMD Global Mapping} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Enable/disable to VMD controller.0: Disable; 1: Enable(Default)}
  gPlatformFspPkgTokenSpaceGuid.VmdGlobalMapping            | * | 0x01 | 0x01

  # !BSF NAME:{CPU PCIE Port0 Link Disable} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{CPU PCIE Port0 Link Disable while Device attached into Port0 and Port1.0: Disable(Default); 1: Enable.}
  gPlatformFspPkgTokenSpaceGuid.CpuPcieFunc0LinkDisable          | * | 0x04 |  { 0x00, 0x00, 0x00, 0x00 }

  # !BSF NAME:{CSE Data Resilience Support} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{0: Disable CSE Data Resilience Support. <b>; 1: Enable CSE Data Resilience Support.</b>}
  gPlatformFspPkgTokenSpaceGuid.CseDataResilience           | * | 0x01 | 0x01
!else
  # !BSF NAME:{Enable VMD Global Mapping} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Enable/disable to VMD controller.0: Disable; 1: Enable(Default)}
  gPlatformFspPkgTokenSpaceGuid.VmdGlobalMapping            | * | 0x01 | 0x01

  # !BSF NAME:{CPU PCIE Port0 Link Disable} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{CPU PCIE Port0 Link Disable while Device attached into Port0 and Port1.0: Disable(Default); 1: Enable.}
  gPlatformFspPkgTokenSpaceGuid.CpuPcieFunc0LinkDisable          | * | 0x04 |  { 0x00, 0x00, 0x00, 0x00 }

  # !BSF NAME:{Skip VccIn Configuration} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{Skips VccIn configuration when enabled}
  gPlatformFspPkgTokenSpaceGuid.PmcSkipVccInConfig                            | * | 0x01 | 0x00

  # !BSF NAME:{CSE Data Resilience Support} TYPE:{Combo} OPTION:{$EN_DIS}
  # !BSF HELP:{0: Disable CSE Data Resilience Support. <b>; 1: Enable CSE Data Resilience Support.</b>}
  gPlatformFspPkgTokenSpaceGuid.CseDataResilience           | * | 0x01 | 0x01
!endif

  # !BSF NAME:{HorizontalResolution for PEI Logo}
  # !BSF TYPE:{EditNum, HEX, (0x0,0xFFFFFFFF)}
  # !BSF HELP:{HorizontalResolution from PEIm Gfx for PEI Logo}
  gPlatformFspPkgTokenSpaceGuid.HorizontalResolution        | * | 0x04 | 0x00000000

  # !BSF NAME:{VerticalResolution for PEI Logo}
  # !BSF TYPE:{EditNum, HEX, (0x0,0xFFFFFFFF)}
  # !BSF HELP:{VerticalResolution from PEIm Gfx for PEI Logo}
  gPlatformFspPkgTokenSpaceGuid.VerticalResolution          | * | 0x04 | 0x00000000

  # !BSF NAME:{Touch Host Controller Active Ltr} TYPE:{EditNum, HEX, (0,0xFFFFFFFFFFFFFFFF)}
  # !HDR STRUCT:{UINT32}
  # !BSF HELP:{Expose Active Ltr for OS driver to set}
  gPlatformFspPkgTokenSpaceGuid.ThcActiveLtr           | * | 0x08 | {0xFFFFFFFF, 0xFFFFFFFF}

  # !BSF NAME:{Touch Host Controller Idle Ltr} TYPE:{EditNum, HEX, (0,0xFFFFFFFFFFFFFFFF)}
  # !HDR STRUCT:{UINT32}
  # !BSF HELP:{Expose Idle Ltr for OS driver to set}
  gPlatformFspPkgTokenSpaceGuid.ThcIdleLtr             | * | 0x08 | {0xFFFFFFFF, 0xFFFFFFFF}

  # !BSF NAME:{Touch Host Controller Hid Over Spi ResetPad} TYPE:{EditNum, HEX, (0,0xFFFFFFFFFFFFFFFF)}
  # !HDR STRUCT:{UINT32}
  # !BSF HELP:{Hid Over Spi ResetPad 0x0 - Use THC HW default Pad, For other pad setting refer to GpioPins}
  gPlatformFspPkgTokenSpaceGuid.ThcHidResetPad         | * | 0x08 | {0x0, 0x0}

  # !BSF NAME:{Touch Host Controller Hid Over Spi ResetPad Trigger} TYPE:{EditNum, HEX, (0,0xFFFFFFFFFFFFFFFF)}
  # !HDR STRUCT:{UINT32}
  # !BSF HELP:{Hid Over Spi Reset Pad Trigger 0x0:Low, 0x1:High}
  gPlatformFspPkgTokenSpaceGuid.ThcHidResetPadTrigger  | * | 0x08 | {0x0, 0x0}

  # !BSF NAME:{Touch Host Controller Hid Over Spi Connection Speed} TYPE:{EditNum, HEX, (0,0xFFFFFFFFFFFFFFFF)}
  # !HDR STRUCT:{UINT32}
  # !BSF HELP:{Hid Over Spi Connection Speed - SPI Frequency}
  gPlatformFspPkgTokenSpaceGuid.ThcHidConnectionSpeed              | * | 0x08 | {17000000, 17000000}

  # !BSF NAME:{Touch Host Controller Hid Over Spi Limit PacketSize} TYPE:{EditNum, HEX, (0,0xFFFFFFFFFFFFFFFF)}
  # !HDR STRUCT:{UINT32}
  # !BSF HELP:{When set, limits SPI read & write packet size to 64B. Otherwise, THC uses Max Soc packet size for SPI Read and Write 0x0- Max Soc Packet Size,  0x11 - 64 Bytes}
  gPlatformFspPkgTokenSpaceGuid.ThcLimitPacketSize                 | * | 0x08 | {0x0, 0x0}

  # !BSF NAME:{Touch Host Controller Hid Over Spi Limit PacketSize} TYPE:{EditNum, HEX, (0,0xFFFFFFFFFFFFFFFF)}
  # !HDR STRUCT:{UINT32}
  # !BSF HELP:{Minimum amount of delay the THC/QUICKSPI driver must wait between end of write operation and begin of read operation. This value shall be in 10us multiples 0x0: Disabled, 1-65535 (0xFFFF) - up to 655350 us}
  gPlatformFspPkgTokenSpaceGuid.ThcPerformanceLimitation           | * | 0x08 | {0x0, 0x0}

  # !BSF NAME:{Touch Host Controller Hid Over Spi Input Report Header Address} TYPE:{EditNum, HEX, (0,0xFFFFFFFFFFFFFFFF)}
  # !HDR STRUCT:{UINT32}
  # !BSF HELP:{Hid Over Spi Input Report Header Address}
  gPlatformFspPkgTokenSpaceGuid.ThcHidInputReportHeaderAddress     | * | 0x08 | {0x0, 0x0}

  # !BSF NAME:{Touch Host Controller Hid Over Spi Input Report Body Address} TYPE:{EditNum, HEX, (0,0xFFFFFFFFFFFFFFFF)}
  # !HDR STRUCT:{UINT32}
  # !BSF HELP:{Hid Over Spi Input Report Body Address}
  gPlatformFspPkgTokenSpaceGuid.ThcHidInputReportBodyAddress       | * | 0x08 | {0x0, 0x0}

  # !BSF NAME:{Touch Host Controller Hid Over Spi Output Report Address} TYPE:{EditNum, HEX, (0,0xFFFFFFFFFFFFFFFF)}
  # !HDR STRUCT:{UINT32}
  # !BSF HELP:{Hid Over Spi Output Report Address}
  gPlatformFspPkgTokenSpaceGuid.ThcHidOutputReportAddress          | * | 0x08 | {0x0, 0x0}

  # !BSF NAME:{Touch Host Controller Hid Over Spi Read Opcode} TYPE:{EditNum, HEX, (0,0xFFFFFFFFFFFFFFFF)}
  # !HDR STRUCT:{UINT32}
  # !BSF HELP:{Hid Over Spi Read Opcode}
  gPlatformFspPkgTokenSpaceGuid.ThcHidReadOpcode                   | * | 0x08 | {0x0, 0x0}

  # !BSF NAME:{Touch Host Controller Hid Over Spi Write Opcode} TYPE:{EditNum, HEX, (0,0xFFFFFFFFFFFFFFFF)}
  # !HDR STRUCT:{UINT32}
  # !BSF HELP:{Hid Over Spi Write Opcode}
  gPlatformFspPkgTokenSpaceGuid.ThcHidWriteOpcode                  | * | 0x08 | {0x0, 0x0}

  # !BSF NAME:{Touch Host Controller Hid Over Spi Flags} TYPE:{EditNum, HEX, (0,0xFFFFFFFFFFFFFFFF)}
  # !HDR STRUCT:{UINT32}
  # !BSF HELP:{Hid Over Spi Flags 0x0:Single SPI Mode, 0x4000:Dual SPI Mode, 0x8000:Quad SPI Mode}
  gPlatformFspPkgTokenSpaceGuid.ThcHidFlags                        | * | 0x08 |  {0x0, 0x0}

  # !HDR EMBED:{FSP_S_CONFIG:FspsConfig:END}
  gPlatformFspPkgTokenSpaceGuid.ReservedFspsUpd             | * | 0x2 | {0x00}


  # Note please keep "UpdTerminator" at the end of each UPD region.
  # The tool will use this field to determine the actual end of the UPD data
  # structure.

  gPlatformFspPkgTokenSpaceGuid.UpdTerminator               | * | 0x02 | 0x55AA
