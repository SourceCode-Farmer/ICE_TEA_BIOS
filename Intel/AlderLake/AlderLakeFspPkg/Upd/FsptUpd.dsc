## @file
#  Platform description.
#
# @copyright
#  INTEL CONFIDENTIAL
#  Copyright 2017 - 2021 Intel Corporation.
#
#  The source code contained or described herein and all documents related to the
#  source code ("Material") are owned by Intel Corporation or its suppliers or
#  licensors. Title to the Material remains with Intel Corporation or its suppliers
#  and licensors. The Material may contain trade secrets and proprietary and
#  confidential information of Intel Corporation and its suppliers and licensors,
#  and is protected by worldwide copyright and trade secret laws and treaty
#  provisions. No part of the Material may be used, copied, reproduced, modified,
#  published, uploaded, posted, transmitted, distributed, or disclosed in any way
#  without Intel's prior express written permission.
#
#  No license under any patent, copyright, trade secret or other intellectual
#  property right is granted to or conferred upon you by disclosure or delivery
#  of the Materials, either expressly, by implication, inducement, estoppel or
#  otherwise. Any license under such intellectual property rights must be
#  express and approved by Intel in writing.
#
#  Unless otherwise agreed by Intel in writing, you may not remove or alter
#  this notice or any other notice embedded in Materials by Intel or
#  Intel's suppliers or licensors in any way.
#
#  This file contains an 'Intel Peripheral Driver' and is uniquely identified as
#  "Intel Reference Module" and is licensed for Intel CPUs and chipsets under
#  the terms of your license agreement with Intel or your vendor. This file may
#  be modified by the user, subject to additional terms of the license agreement.
#
# @par Specification
##

  #
  # Comments with !BSF will be used to generate BSF file
  # Comments with !HDR will be used to generate H header file
  #

  # Global definitions in BSF
  # !BSF PAGES:{FSPTUPD:"FSP-T Settings", MRC:"Memory Reference Code", CPU1:"CPU (Pre-Mem)", CPU2:"CPU (Post-Mem)", SA1:"System Agent (Pre-Mem)", SA2:"System Agent (Post-Mem)", PCH1:"PCH (Pre-Mem)", PCH2:"PCH (Post-Mem)", TCSS1:"USB-C/Thunderbolt (Pre-Mem)", TCSS2:"USB-C/Thunderbolt (Post-Mem)", SEC1:"Security (Pre-Mem)", ME1:"ME (Pre-Mem)", ME2:"ME (Post-Mem)", DBG:"Debug"}
  # !BSF BLOCK:{NAME:"Alder Lake Platform", VER:"0.1"}

  # !BSF FIND:{ADLUPD_T}
  # !HDR COMMENT:{FSP_UPD_HEADER:FSP UPD Header}
  # !HDR EMBED:{FSP_UPD_HEADER:FspUpdHeader:START}
  # FsptUpdSignature: {ADLUPD_T}
  gPlatformFspPkgTokenSpaceGuid.Signature                   | * | 0x08 | 0x545F4450554C4441
  # !BSF NAME:{FsptUpdRevision}  TYPE:{None}
  gPlatformFspPkgTokenSpaceGuid.Revision                    | * | 0x01 | 0x02
  # !HDR EMBED:{FSP_UPD_HEADER:FspUpdHeader:END}
  gPlatformFspPkgTokenSpaceGuid.Reserved                    | * | 0x17 | {0x00}

  # !HDR COMMENT:{FSPT_ARCH_UPD:FSPT_ARCH_UPD}
  # !HDR EMBED:{FSPT_ARCH_UPD:FsptArchUpd:START}
  gPlatformFspPkgTokenSpaceGuid.Revision                    | * | 0x01 | 0x01
  gPlatformFspPkgTokenSpaceGuid.Reserved                    | * | 0x03 | {0x00}
  gPlatformFspPkgTokenSpaceGuid.Length                      | * | 0x04 | 0x00000020
  gPlatformFspPkgTokenSpaceGuid.FspDebugHandler             | * | 0x04 | 0x00000000
  # !HDR EMBED:{FSPT_ARCH_UPD:FsptArchUpd:END}
  gPlatformFspPkgTokenSpaceGuid.Reserved1                   | * | 0x14 | {0x00}

  # !HDR COMMENT:{FSPT_CORE_UPD:Fsp T Core UPD}
  # !HDR EMBED:{FSPT_CORE_UPD:FsptCoreUpd:START}
  # Base address of the microcode region.
  gPlatformFspPkgTokenSpaceGuid.MicrocodeRegionBase         | * | 0x04 | 0x00000000

  # Length of the microcode region.
  gPlatformFspPkgTokenSpaceGuid.MicrocodeRegionSize         | * | 0x04 | 0x00000000

  # Base address of the cacheable flash region.
  gPlatformFspPkgTokenSpaceGuid.CodeRegionBase              | * | 0x04 | 0x00000000

  # Length of the cacheable flash region.
  gPlatformFspPkgTokenSpaceGuid.CodeRegionSize              | * | 0x04 | 0x00000000

  # !HDR EMBED:{FSPT_CORE_UPD:FsptCoreUpd:END}
  gPlatformFspPkgTokenSpaceGuid.Reserved                    | * | 0x10 | {0x00}

  # !HDR COMMENT:{FSP_T_CONFIG:Fsp T Configuration}
  # !HDR EMBED:{FSP_T_CONFIG:FsptConfig:START}
  # !BSF PAGE:{FSPTUPD}

  # !BSF NAME:{PcdSerialIoUartDebugEnable} TYPE:{Combo}
  # !BSF OPTION:{0:Disable, 1:Enable and Initialize, 2:Enable without Initializing}
  # !BSF HELP:{Enable SerialIo Uart debug library with/without initializing SerialIo Uart device in FSP. }
!if $(TARGET) == DEBUG
  gPlatformFspPkgTokenSpaceGuid.PcdSerialIoUartDebugEnable           | * | 0x01 | 0x01
!else
  gPlatformFspPkgTokenSpaceGuid.PcdSerialIoUartDebugEnable           | * | 0x01 | 0x00
!endif

!if gSiPkgTokenSpaceGuid.PcdAdlLpSupport == TRUE
  # !BSF NAME:{PcdSerialIoUartNumber} TYPE:{Combo}
  # !BSF OPTION:{0:SerialIoUart0, 1:SerialIoUart1, 2:SerialIoUart2}
  # !BSF HELP:{Select SerialIo Uart Controller for debug. Note: If UART0 is selected as CNVi BT Core interface, it cannot be used for debug purpose.}
  gPlatformFspPkgTokenSpaceGuid.PcdSerialIoUartNumber                | * | 0x01 | 0x00
!else
  # !BSF NAME:{PcdSerialIoUartNumber} TYPE:{Combo}
  # !BSF OPTION:{0:SerialIoUart0, 1:SerialIoUart1, 2:SerialIoUart2}
  # !BSF HELP:{Select SerialIo Uart Controller for debug. Note: If UART0 is selected as CNVi BT Core interface, it cannot be used for debug purpose.}
  gPlatformFspPkgTokenSpaceGuid.PcdSerialIoUartNumber                | * | 0x01 | 0x02
!endif

  # !BSF NAME:{PcdSerialIoUartMode - FSPT} TYPE:{Combo}
  # !BSF OPTION:{0:SerialIoUartDisabled, 1:SerialIoUartPci, 2:SerialIoUartHidden, 3:SerialIoUartCom, 4:SerialIoUartSkipInit}
  # !BSF HELP:{Select SerialIo Uart Controller mode}
  gPlatformFspPkgTokenSpaceGuid.PcdSerialIoUartMode                  | * | 0x01 | 0x02

  # !BSF NAME:{PcdSerialIoUartBaudRate - FSPT} TYPE:{EditNum, DEC, (0,6000000)}
  # !BSF HELP:{Set default BaudRate Supported from 0 - default to 6000000}
  gPlatformFspPkgTokenSpaceGuid.PcdSerialIoUartBaudRate              | * | 0x4 | 115200

  #
  # NOTE: If offset for PcdPciExpressBaseAddress & PcdPciExpressRegion is changed,
  # then please change offset accordingly in SecHostBridgeInit in SecHostBridgeLib.asm & SecHostBridgeLib.nasm
  #

  # !BSF NAME:{Pci Express Base Address}  TYPE:{EditNum, HEX, (0x00,0xFFFFFFFF)}
  # !BSF HELP:{Base address to be programmed for Pci Express }

  gPlatformFspPkgTokenSpaceGuid.PcdPciExpressBaseAddress             | * | 0x08 | 0xC0000000


  # !BSF NAME:{Pci Express Region Length}  TYPE:{EditNum, HEX, (0x00,0xFFFFFFFF)}
  # !BSF HELP:{Region Length to be programmed for Pci Express }
  gPlatformFspPkgTokenSpaceGuid.PcdPciExpressRegionLength            | * | 0x04 | 0x10000000

  # !BSF NAME:{PcdSerialIoUartParity - FSPT} TYPE:{Combo}
  # !BSF OPTION:{0: DefaultParity, 1: NoParity, 2: EvenParity, 3: OddParity}
  # !BSF HELP:{Set default Parity.}
  gPlatformFspPkgTokenSpaceGuid.PcdSerialIoUartParity                | * | 0x01 | 0x1

  # !BSF NAME:{PcdSerialIoUartDataBits - FSPT} TYPE:{EditNum, HEX, (0x0,0x08)}
  # !BSF HELP:{Set default word length. 0: Default, 5,6,7,8}
  gPlatformFspPkgTokenSpaceGuid.PcdSerialIoUartDataBits              | * | 0x01 | 0x08

  # !BSF NAME:{PcdSerialIoUartStopBits - FSPT} TYPE:{Combo}
  # !BSF OPTION:{0: DefaultStopBits, 1: OneStopBit, 2: OneFiveStopBits, 3: TwoStopBits}
  # !BSF HELP:{Set default stop bits.}
  gPlatformFspPkgTokenSpaceGuid.PcdSerialIoUartStopBits              | * | 0x01 | 0x01

  # !BSF NAME:{PcdSerialIoUartAutoFlow - FSPT} TYPE:{Combo}
  # !BSF OPTION:{0: Disable, 1:Enable}
  # !BSF HELP:{Enables UART hardware flow control, CTS and RTS lines.}
  gPlatformFspPkgTokenSpaceGuid.PcdSerialIoUartAutoFlow              | * | 0x01 | 0x0

  # !BSF NAME:{PcdSerialIoUartRxPinMux - FSPT} TYPE:{EditNum, HEX, (0x00,0xFFFFFFFF)}
  # !BSF HELP:{Select RX pin muxing for SerialIo UART used for debug}
  gPlatformFspPkgTokenSpaceGuid.PcdSerialIoUartRxPinMux              | * | 0x04 | 0x0

  # !BSF NAME:{PcdSerialIoUartTxPinMux - FSPT} TYPE:{EditNum, HEX, (0x00,0xFFFFFFFF)}
  # !BSF HELP:{Select TX pin muxing for SerialIo UART used for debug}
  gPlatformFspPkgTokenSpaceGuid.PcdSerialIoUartTxPinMux              | * | 0x04 | 0x0

  # !BSF NAME:{PcdSerialIoUartRtsPinMux - FSPT} TYPE:{EditNum, HEX, (0,0xFFFFFFFF)}
  # !BSF HELP:{Select SerialIo Uart used for debug Rts pin muxing. Refer to GPIO_*_MUXING_SERIALIO_UARTx_RTS* for possible values.}
  gPlatformFspPkgTokenSpaceGuid.PcdSerialIoUartRtsPinMux             | * | 0x04 | 0x0

  # !BSF NAME:{PcdSerialIoUartCtsPinMux - FSPT} TYPE:{EditNum, HEX, (0,0xFFFFFFFF)}
  # !BSF HELP:{Select SerialIo Uart used for debug Cts pin muxing. Refer to GPIO_*_MUXING_SERIALIO_UARTx_CTS* for possible values.}
  gPlatformFspPkgTokenSpaceGuid.PcdSerialIoUartCtsPinMux             | * | 0x04 | 0x0

  # !BSF NAME:{PcdSerialIoUartDebugMmioBase - FSPT} TYPE:{EditNum, HEX, (0,0xFFFFFFFF)}
  # !BSF HELP:{Select SerialIo Uart default MMIO resource in SEC/PEI phase when PcdSerialIoUartMode = SerialIoUartPci.}
  gPlatformFspPkgTokenSpaceGuid.PcdSerialIoUartDebugMmioBase         | * | 0x04 | 0xFE036000
  # !BSF NAME:{PcdLpcUartDebugEnable} TYPE:{Combo}
  # !BSF OPTION:{0:Disable, 1:Enable}
  # !BSF HELP:{Enable to initialize LPC Uart device in FSP.}
!if $(TARGET) == DEBUG
  gPlatformFspPkgTokenSpaceGuid.PcdLpcUartDebugEnable                | * | 0x01 | 0x01
!else
  gPlatformFspPkgTokenSpaceGuid.PcdLpcUartDebugEnable                | * | 0x01 | 0x00
!endif

  # !BSF NAME:{Debug Interfaces} TYPE:{EditNum, HEX, (0x00,0x3F)}
  # !BSF HELP:{Debug Interfaces. BIT0-RAM, BIT1-UART, BIT3-USB3, BIT4-Serial IO, BIT5-TraceHub, BIT2 - Not used.}
  gPlatformFspPkgTokenSpaceGuid.PcdDebugInterfaceFlags          | * | 0x01 | 0x012

  # !BSF NAME:{PcdSerialDebugLevel} TYPE:{Combo}
  # !BSF OPTION:{0:Disable, 1:Error Only, 2:Error and Warnings, 3:Load Error Warnings and Info, 4:Load Error Warnings and Info, 5:Load Error Warnings Info and Verbose}
  # !BSF HELP:{Serial Debug Message Level. 0:Disable, 1:Error Only, 2:Error & Warnings, 3:Load, Error, Warnings & Info, 4:Load, Error, Warnings, Info & Event, 5:Load, Error, Warnings, Info & Verbose.}
  gPlatformFspPkgTokenSpaceGuid.PcdSerialDebugLevel             | * | 0x01 | 0x03

  # !BSF NAME:{ISA Serial Base selection} TYPE:{Combo}
  # !BSF OPTION:{0:0x3F8, 1:0x2F8}
  # !BSF HELP:{Select ISA Serial Base address. Default is 0x3F8.}
  gPlatformFspPkgTokenSpaceGuid.PcdIsaSerialUartBase             | * | 0x01 | 0x00

  # !BSF NAME:{PcdSerialIo2ndUartEnable} TYPE:{Combo}
  # !BSF OPTION:{0:Disable, 1:Enable and Initialize, 2:Enable without Initializing}
  # !BSF HELP:{Enable Additional SerialIo Uart device in FSP.}
  gPlatformFspPkgTokenSpaceGuid.PcdSerialIo2ndUartEnable                | * | 0x01 | 0x00

  # !BSF NAME:{PcdSerialIo2ndUartNumber} TYPE:{Combo}
  # !BSF OPTION:{0:SerialIoUart0, 1:SerialIoUart1, 2:SerialIoUart2}
  # !BSF HELP:{Select SerialIo Uart Controller Number}
  gPlatformFspPkgTokenSpaceGuid.PcdSerialIo2ndUartNumber                | * | 0x01 | 0x02

  # !BSF NAME:{PcdSerialIo2ndUartMode - FSPT} TYPE:{Combo}
  # !BSF OPTION:{0:SerialIoUartDisabled, 1:SerialIoUartPci, 2:SerialIoUartHidden, 3:SerialIoUartCom, 4:SerialIoUartSkipInit}
  # !BSF HELP:{Select SerialIo Uart Controller mode}
  gPlatformFspPkgTokenSpaceGuid.PcdSerialIo2ndUartMode                  | * | 0x01 | 0x02

  # !BSF NAME:{PcdSerialIo2ndUartBaudRate - FSPT} TYPE:{EditNum, DEC, (0,6000000)}
  # !BSF HELP:{Set default BaudRate Supported from 0 - default to 6000000}
  gPlatformFspPkgTokenSpaceGuid.PcdSerialIo2ndUartBaudRate              | * | 0x4 | 115200

  # !BSF NAME:{PcdSerialIo2ndUartParity - FSPT} TYPE:{Combo}
  # !BSF OPTION:{0: DefaultParity, 1: NoParity, 2: EvenParity, 3: OddParity}
  # !BSF HELP:{Set default Parity.}
  gPlatformFspPkgTokenSpaceGuid.PcdSerialIo2ndUartParity                | * | 0x01 | 0x1

  # !BSF NAME:{PcdSerialIo2ndUartDataBits - FSPT} TYPE:{EditNum, HEX, (0x0,0x08)}
  # !BSF HELP:{Set default word length. 0: Default, 5,6,7,8}
  gPlatformFspPkgTokenSpaceGuid.PcdSerialIo2ndUartDataBits              | * | 0x01 | 0x08

  # !BSF NAME:{PcdSerialIo2ndUartStopBits - FSPT} TYPE:{Combo}
  # !BSF OPTION:{0: DefaultStopBits, 1: OneStopBit, 2: OneFiveStopBits, 3: TwoStopBits}
  # !BSF HELP:{Set default stop bits.}
  gPlatformFspPkgTokenSpaceGuid.PcdSerialIo2ndUartStopBits              | * | 0x01 | 0x01

  # !BSF NAME:{PcdSerialIo2ndUartAutoFlow - FSPT} TYPE:{Combo}
  # !BSF OPTION:{0: Disable, 1:Enable}
  # !BSF HELP:{Enables UART hardware flow control, CTS and RTS lines.}
  gPlatformFspPkgTokenSpaceGuid.PcdSerialIo2ndUartAutoFlow              | * | 0x01 | 0x0

  # !BSF NAME:{PcdSerialIo2ndUartRxPinMux - FSPT} TYPE:{EditNum, HEX, (0x00,0xFFFFFFFF)}
  # !BSF HELP:{Select RX pin muxing for SerialIo UART}
  gPlatformFspPkgTokenSpaceGuid.PcdSerialIo2ndUartRxPinMux              | * | 0x04 | 0x0

  # !BSF NAME:{PcdSerialIo2ndUartTxPinMux - FSPT} TYPE:{EditNum, HEX, (0x00,0xFFFFFFFF)}
  # !BSF HELP:{Select TX pin muxing for SerialIo UART}
  gPlatformFspPkgTokenSpaceGuid.PcdSerialIo2ndUartTxPinMux              | * | 0x04 | 0x0

  # !BSF NAME:{PcdSerialIo2ndUartRtsPinMux - FSPT} TYPE:{EditNum, HEX, (0,0xFFFFFFFF)}
  # !BSF HELP:{Select SerialIo Uart Rts pin muxing. Refer to GPIO_*_MUXING_SERIALIO_UARTx_RTS* for possible values.}
  gPlatformFspPkgTokenSpaceGuid.PcdSerialIo2ndUartRtsPinMux             | * | 0x04 | 0x0

  # !BSF NAME:{PcdSerialIo2ndUartCtsPinMux - FSPT} TYPE:{EditNum, HEX, (0,0xFFFFFFFF)}
  # !BSF HELP:{Select SerialIo Uart Cts pin muxing. Refer to GPIO_*_MUXING_SERIALIO_UARTx_CTS* for possible values.}
  gPlatformFspPkgTokenSpaceGuid.PcdSerialIo2ndUartCtsPinMux             | * | 0x04 | 0x0

  # !BSF NAME:{PcdSerialIo2ndUartMmioBase - FSPT} TYPE:{EditNum, HEX, (0,0xFFFFFFFF)}
  # !BSF HELP:{Select SerialIo Uart default MMIO resource in SEC/PEI phase when PcdSerialIo2ndUartMode = SerialIoUartPci.}
  gPlatformFspPkgTokenSpaceGuid.PcdSerialIo2ndUartMmioBase              | * | 0x04 | 0xFE034000

  #
  # NOTE: If offset for TopMemoryCacheSize is changed,
  # then please change FSPT_CFG_TOP_MEMORY_CACHE_SIZE_OFFSET in SecCpuLib.nasm
  #
  # Length of the TopMemory cacheable flash region.
  # This should only be used where the cache size is not able
  # to cover all pre-memory code with one continuous region.
  # NOTE: To simply the algorithm, the value must be power of 2.
  # When value is zero, the feature is disabled.
  # If the value is not zero, FSPT will configure CAR to cover
  # the region between 4G-TopMemoryCacheSize to 4G for the first region,
  # and the CodeRegionBase and CodeRegionSize is used to cover
  # the second region.
  gPlatformFspPkgTokenSpaceGuid.TopMemoryCacheSize                      | * | 0x04 | 0x00000000

  # !BSF NAME:{FspDebugHandler} TYPE:{EditNum, HEX, (0x0,0xFFFFFFFF)}
  # !BSF HELP:{<b>Optional</b> pointer to the boot loader's implementation of FSP_DEBUG_HANDLER.}
  gPlatformFspPkgTokenSpaceGuid.FspDebugHandler                     | * | 0x04 | 0x0

  # !BSF NAME:{Serial Io SPI Chip Select Polarity} TYPE:{EditNum, HEX, (0x00,0xFFFF)}
  # !BSF HELP:{Sets polarity for each chip Select. Available options: 0:SerialIoSpiCsActiveLow, 1:SerialIoSpiCsActiveHigh}
  gPlatformFspPkgTokenSpaceGuid.PcdSerialIoSpiCsPolarity            | * | 0x02 | { 0x0, 0x0}

  # !BSF NAME:{Serial Io SPI Chip Select Enable} TYPE:{EditNum, HEX, (0x00,0xFFFF)}
  # !BSF HELP:{0:Disabled, 1:Enabled. Enables GPIO for CS0 or CS1 if it is Enabled}
  gPlatformFspPkgTokenSpaceGuid.PcdSerialIoSpiCsEnable              | * | 0x02 | { 0x0, 0x0}

  # !BSF NAME:{Serial Io SPI Device Mode} TYPE:{EditNum, HEX, (0x0,0x1}
  # !BSF HELP:{When mode is set to Pci, controller is initalized in early stage. Available modes: 0:SerialIoSpiDisabled, 1:SerialIoSpiPci.}
  gPlatformFspPkgTokenSpaceGuid.PcdSerialIoSpiMode                  | * | 0x01 | 0x0

  # !BSF NAME:{Serial Io SPI Default Chip Select Output} TYPE:{EditNum, HEX, (0x00,0x1)}
  # !BSF HELP:{Sets Default CS as Output. Available options: 0:CS0, 1:CS1}
  gPlatformFspPkgTokenSpaceGuid.PcdSerialIoSpiDefaultCsOutput       | * | 0x01 | 0x0

  # !BSF NAME:{Serial Io SPI Default Chip Select Mode HW/SW} TYPE:{EditNum, HEX, (0x00,0x1)}
  # !BSF HELP:{Sets Default CS Mode Hardware or Software. Available options: 0:HW, 1:SW}
  gPlatformFspPkgTokenSpaceGuid.PcdSerialIoSpiCsMode                | * | 0x01 | 0x0

  # !BSF NAME:{Serial Io SPI Default Chip Select State Low/High} TYPE:{EditNum, HEX, (0x00,0x1)}
  # !BSF HELP:{Sets Default CS State Low or High. Available options: 0:Low, 1:High}
  gPlatformFspPkgTokenSpaceGuid.PcdSerialIoSpiCsState               | * | 0x1 | 0x0

  # !BSF NAME:{Serial Io SPI Device Number} TYPE:{EditNum, HEX, (0x0,0xFF}
  # !BSF HELP:{Select which Serial Io SPI controller is initalized in early stage.}
  gPlatformFspPkgTokenSpaceGuid.PcdSerialIoSpiNumber                | * | 0x01 | 0x0

  # !BSF NAME:{Serial Io SPI Device MMIO Base} TYPE:{EditNum, HEX, (0x0,0xFFFFFFFF)}
  # !BSF HELP:{Assigns MMIO for Serial Io SPI controller usage in early stage.}
  gPlatformFspPkgTokenSpaceGuid.PcdSerialIoSpiMmioBase              | * | 0x04 | 0x0

  # !HDR EMBED:{FSP_T_CONFIG:FsptConfig:END}
  gPlatformFspPkgTokenSpaceGuid.ReservedFsptUpd1                        | * | 0x10 | {0x00}


  # Note please keep "UpdTerminator" at the end of each UPD region.
  # The tool will use this field to determine the actual end of the UPD data
  # structure.

  gPlatformFspPkgTokenSpaceGuid.UpdTerminator               | * | 0x02 | 0x55AA
