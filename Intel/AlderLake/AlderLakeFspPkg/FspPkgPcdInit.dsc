## @file
#  FSP description file initializes configuration (PCD) settings for this project.
#
# @copyright
#  INTEL CONFIDENTIAL
#  Copyright 2020 - 2021 Intel Corporation.
#
#  The source code contained or described herein and all documents related to the
#  source code ("Material") are owned by Intel Corporation or its suppliers or
#  licensors. Title to the Material remains with Intel Corporation or its suppliers
#  and licensors. The Material may contain trade secrets and proprietary and
#  confidential information of Intel Corporation and its suppliers and licensors,
#  and is protected by worldwide copyright and trade secret laws and treaty
#  provisions. No part of the Material may be used, copied, reproduced, modified,
#  published, uploaded, posted, transmitted, distributed, or disclosed in any way
#  without Intel's prior express written permission.
#
#  No license under any patent, copyright, trade secret or other intellectual
#  property right is granted to or conferred upon you by disclosure or delivery
#  of the Materials, either expressly, by implication, inducement, estoppel or
#  otherwise. Any license under such intellectual property rights must be
#  express and approved by Intel in writing.
#
#  Unless otherwise agreed by Intel in writing, you may not remove or alter
#  this notice or any other notice embedded in Materials by Intel or
#  Intel's suppliers or licensors in any way.
#
#  This file contains an 'Intel Peripheral Driver' and is uniquely identified as
#  "Intel Reference Module" and is licensed for Intel CPUs and chipsets under
#  the terms of your license agreement with Intel or your vendor. This file may
#  be modified by the user, subject to additional terms of the license agreement.
#
# @par Specification
##

#
# TRUE is ENABLE. FALSE is DISABLE.
#

#
# BIOS build switches configuration
#

[PcdsFixedAtBuild]
  #
  # CPU
  #
  gSiPkgTokenSpaceGuid.PcdBtgTxtLegacyPkgEnable|TRUE
  gSiPkgTokenSpaceGuid.PcdTxtEnable|TRUE
  gSiPkgTokenSpaceGuid.PcdOverclockEnable|TRUE
  gSiPkgTokenSpaceGuid.PcdCpuPowerOnConfigEnable|TRUE
  #
  # SA
  #
  gSiPkgTokenSpaceGuid.PcdSsaFlagEnable|FALSE
  gSiPkgTokenSpaceGuid.PcdEvLoaderEnable|FALSE
  #
  # ME
  #
  gSiPkgTokenSpaceGuid.PcdPttEnable|TRUE
  gSiPkgTokenSpaceGuid.PcdAmtEnable|TRUE
  #
  # SI
  #
  gSiPkgTokenSpaceGuid.PcdThcEnable|FALSE
  gSiPkgTokenSpaceGuid.PcdFspBinaryEnable|TRUE
  # HybridStorageDevice
  gSiPkgTokenSpaceGuid.PcdHybridStorageSupport|TRUE
  #
  # Since PcdSimicsEnable has been removed and TGL run in Simics Environment currently,
  # so follow original setting which set PcdBdatEnable = FALSE in Simics Environment
  #
  gSiPkgTokenSpaceGuid.PcdFspImageIdString|0x245053464C444124  #$ADLFSP$
  gSiPkgTokenSpaceGuid.PcdFspVersionRevision|0x65
  gSiPkgTokenSpaceGuid.PcdFspVersionBuild|0x70
  gSiPkgTokenSpaceGuid.PcdFspVersionMinor|0x00
  gSiPkgTokenSpaceGuid.PcdStatusCodeFlags|0x32
  gSiPkgTokenSpaceGuid.PcdMultiPhaseSiInitNumberOfPhases|1

  gAlderLakeFspPkgTokenSpaceGuid.PcdMiniBiosEnable|FALSE
  gAlderLakeFspPkgTokenSpaceGuid.PcdCfgRebuild|FALSE
  gAlderLakeFspPkgTokenSpaceGuid.PcdFspPerformanceEnable|FALSE
  gAlderLakeFspPkgTokenSpaceGuid.PcdMonoStatusCode|FALSE
  #
  # Symbol in release build
  #
  gAlderLakeFspPkgTokenSpaceGuid.PcdSymbolInReleaseEnable|FALSE

!if $(TARGET) == RELEASE
!if gSiPkgTokenSpaceGuid.PcdSiCatalogDebugEnable == TRUE
  gEfiMdePkgTokenSpaceGuid.PcdDebugPropertyMask|0x02
!else
  gEfiMdePkgTokenSpaceGuid.PcdDebugPropertyMask|0
!endif
!else
  gEfiMdePkgTokenSpaceGuid.PcdDebugPropertyMask|0x27
!endif

  ## Indicates if to shadow PEIM on S3 boot path after memory is ready.<BR><BR>
  #   TRUE  - Shadow PEIM on S3 boot path after memory is ready.<BR>
  #   FALSE - Not shadow PEIM on S3 boot path after memory is ready.<BR>
  # @Prompt Shadow Peim On S3 Boot.
  gEfiMdeModulePkgTokenSpaceGuid.PcdShadowPeimOnS3Boot|TRUE
  # Temp solution to avoid halt in PeiVariable->GetVariable (PeiGetVariable)
  gEfiMdeModulePkgTokenSpaceGuid.PcdFlashNvStorageVariableBase|0xFFF80000
  gEfiMdeModulePkgTokenSpaceGuid.PcdFlashNvStorageVariableSize|0x10000
  # Use to override gEfiMdeModulePkgTokenSpaceGuid.PcdPeiCoreMaxPeiStackSize
  gEfiMdeModulePkgTokenSpaceGuid.PcdPeiCoreMaxPeiStackSize|0x80000

  gIntelFsp2PkgTokenSpaceGuid.PcdTemporaryRamBase|0xFEF00000
  gIntelFsp2PkgTokenSpaceGuid.PcdTemporaryRamSize|0x00100000
  gIntelFsp2PkgTokenSpaceGuid.PcdFspReservedMemoryLength|0x00500000
  gIntelFsp2PkgTokenSpaceGuid.PcdFspTemporaryRamSize|0x00020000
  ## Specifies the FSP Header Spec Version
  gIntelFsp2PkgTokenSpaceGuid.PcdFspHeaderSpecVersion|0x23
  # This defines how much space will be used for heap in FSP temporary memory
  # x % of FSP temporary memory will be used for heap
  # (100 - x) % of FSP temporary memory will be used for stack
  # 0 means FSP will share the same stack with bootloader
  #   In this case PcdFspTemporaryRamSize is used for Heap
  gIntelFsp2PkgTokenSpaceGuid.PcdFspHeapSizePercentage|0
  # This is a platform specific global pointer used by FSP
  gIntelFsp2PkgTokenSpaceGuid.PcdGlobalDataPointerAddress|0xFED00148
  # Override PcdFspMaxPatchEntry to match FspHeader.aslc
  gIntelFsp2PkgTokenSpaceGuid.PcdFspMaxPatchEntry|0x02
  ## Specifies the number of variable MTRRs reserved for OS use. The default number of
  #  MTRRs reserved for OS use is 0.
  # @Prompt Number of reserved variable MTRRs.
  gUefiCpuPkgTokenSpaceGuid.PcdCpuNumberOfReservedVariableMtrrs|0x0
  ## Specifies timeout value in microseconds for the BSP to detect all APs for the first time.
  # @Prompt Timeout for the BSP to detect all APs for the first time.
  gUefiCpuPkgTokenSpaceGuid.PcdCpuApInitTimeOutInMicroSeconds|10000

[PcdsPatchableInModule]
  gSiPkgTokenSpaceGuid.PcdRegBarBaseAddress|0xFB000000
  #
  # This entry will be patched during the build process
  #
  gEfiMdeModulePkgTokenSpaceGuid.PcdVpdBaseAddress|0x12345678

[PcdsDynamicExDefault]
  #
  # Include FSP PCD settings.
  #
  !include $(FSP_PACKAGE)/FspPkgPcdShare.dsc

  gEfiMdeModulePkgTokenSpaceGuid.PcdS3BootScriptTablePrivateDataPtr|0x0
  gEfiMdeModulePkgTokenSpaceGuid.PcdS3BootScriptTablePrivateSmmDataPtr|0x0