Intel is a trademark or registered trademark of Intel Corporation or its
subsidiaries in the United States and other countries.
*Other names and brands may be claimed as the property of others.
Copyright (c) 2016 - 2020, Intel Corporation. All rights reserved.

# FSP build steps for building AlderLake FSP binary

In order to build AlderLake FSP binary, following packages needs to be
downloaded along with the FSP SDK open source packages
* AlderLakeFspPkg --> contains all the AlderLake FSP Platform code
* ClientOneSiliconPkg --> contains all the AlderLake Silicon initialization code

Using BuildFsp.cmd (or BuildFsp.sh) is recommended.
Please refer to the usage notes by command - "BuildFsp.cmd /h"

## FSP SDK open source packages and hash versions
SDK open source packages may need some customization to build this version of FSP,
please contact Intel to get the github link for the EDK2 packages used.