/** @file
  PCIe pins initialization in Pre-Mem phase.

 @copyright
  INTEL CONFIDENTIAL
  Copyright 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains an 'Intel Peripheral Driver' and is uniquely identified as
  "Intel Reference Module" and is licensed for Intel CPUs and chipsets under
  the terms of your license agreement with Intel or your vendor. This file may
  be modified by the user, subject to additional terms of the license agreement.

@par Specification Reference:
**/

#include <Uefi.h>
#include <PiPei.h>
#include <Library/BaseLib.h>
#include <Library/PcdLib.h>
#include <Library/DebugLib.h>
#include <Library/PrintLib.h>
#include <FspmUpd.h>
#include <Library/GpioLib.h>
#include <Library/FspCommonLib.h>
#include <Base.h>
#include <Library/BaseMemoryLib.h>
#include <Library/PeiServicesLib.h>
#include <Library/PchInfoLib.h>
#include <Library/GpioLib.h>

#define MAX_GPIO_PINS             130

EFI_STATUS
EFIAPI
PciePinPreMemInit (
  IN EFI_PEI_SERVICES          **PeiServices,
  IN EFI_PEI_NOTIFY_DESCRIPTOR *NotifyDesc,
  IN VOID                      *Ppi
  );

static EFI_PEI_NOTIFY_DESCRIPTOR mNotifyPciePinPpi = {
  EFI_PEI_PPI_DESCRIPTOR_NOTIFY_CALLBACK | EFI_PEI_PPI_DESCRIPTOR_TERMINATE_LIST,
  &gPeiBeforeGraphicsDetectionPpiGuid,
  PciePinPreMemInit
};

/**
  Configures GPIO

  @param[in]  GpioTable       Point to Platform Gpio table
  @param[in]  GpioTableCount  Number of Gpio table entries
**/
STATIC
VOID
ConfigureGpio (
  IN GPIO_INIT_CONFIG                 *GpioTable,
  IN UINT16                           GpioTableCount
  )
{
  EFI_STATUS          Status;

  DEBUG ((DEBUG_INFO, "ConfigureGpio() Start\n"));

  Status = GpioConfigurePads (GpioTableCount, GpioTable);
  ASSERT_EFI_ERROR (Status);

  DEBUG ((DEBUG_INFO, "ConfigureGpio() End\n"));
}

/**
  Count the number of GPIO settings in the Table.

  @param[in]  GpioTable   The pointer of GPIO config table
  @param[out] GpioCount   The number of GPIO config entries
**/
VOID
GetGpioTableSize (
  GPIO_INIT_CONFIG   *GpioTable,
  OUT UINT16         *GpioCount
  )
{
  *GpioCount = 0;
  if (GpioTable != NULL) {
    while (GpioTable[*GpioCount].GpioPad != 0 && *GpioCount < MAX_GPIO_PINS) {
      DEBUG ((DEBUG_INFO, "GpioTable[%d]->GpioPad = %x \n", *GpioCount, GpioTable[*GpioCount].GpioPad));
      (*GpioCount) ++;
    }
  } else {
    DEBUG ((DEBUG_INFO, "GpioTable is NULL\n"));
  }
  DEBUG ((DEBUG_INFO, "GetGpioTableSize() GpioCount = %d\n", *GpioCount));
}

/**
  Configure GPIO Before Memory is initialized.

  @param[in]  GpioTable  Pointer to Gpio table
**/
VOID
GpioInit (
  IN GPIO_INIT_CONFIG *GpioTable
  )
{
  UINT16             GpioCount;

  if (GpioTable != 0) {
    GpioCount = 0;
    GetGpioTableSize (GpioTable, &GpioCount);
    if (GpioCount != 0) {
      ConfigureGpio ((VOID *) GpioTable, (UINTN) GpioCount);
    }
  }
}
/**
  Pcie pins initialization in PEI pre-memory phase.

  @param[in] PeiServices   - Pointer to PEI Services Table.
  @param[in] NotifyDesc    - Pointer to the descriptor for the Notification event that
                             caused this function to execute.
  @param[in] Ppi           - Pointer to the PPI data associated with this function.

  @retval EFI_STATUS       - Always return EFI_SUCCESS
**/
EFI_STATUS
EFIAPI
PciePinPreMemInit (
  IN EFI_PEI_SERVICES          **PeiServices,
  IN EFI_PEI_NOTIFY_DESCRIPTOR *NotifyDesc,
  IN VOID                      *Ppi
  )
{
  FSPM_UPD                        *FspmUpd;
  GPIO_INIT_CONFIG                *GpioTable;

  DEBUG ((DEBUG_INFO, "PciePinPreMemInit\n"));
  FspmUpd = NULL;
  ///
  /// Initialize Pre-mem GPIO in FSP API mode.
  ///
  DEBUG ((DEBUG_INFO, "Initialize Pre-mem GPIO in FSP API mode.\n"));
  FspmUpd = (FSPM_UPD *) GetFspMemoryInitUpdDataPointer ();
  ASSERT (FspmUpd != NULL);
  GpioTable = (GPIO_INIT_CONFIG *) (FspmUpd->FspmConfig.BoardGpioTablePreMemAddress);
  GpioInit (GpioTable);
  return EFI_SUCCESS;
}

/**
  Constructor for Pcie pins initialization in PEI pre-memory phase.

  @retval EFI_SUCCESS   The constructor is completed successfully.
**/
EFI_STATUS
EFIAPI
PciePinInitPreMemLibConstructor (
  VOID
  )
{
  EFI_STATUS          Status;
  //
  // In Api mode, assert PCIe SLOT RTD3 and PEG reset pins in early PreMem phase.
  //
  if (GetFspGlobalDataPointer()->FspMode == FSP_IN_API_MODE) {
    DEBUG ((DEBUG_INFO, "PciePinInitPreMemLibConstructor\n"));
    //
    // Install Notify
    //
    Status = PeiServicesNotifyPpi (&mNotifyPciePinPpi);
    ASSERT_EFI_ERROR (Status);
    return Status;
  }
  return EFI_SUCCESS;
}