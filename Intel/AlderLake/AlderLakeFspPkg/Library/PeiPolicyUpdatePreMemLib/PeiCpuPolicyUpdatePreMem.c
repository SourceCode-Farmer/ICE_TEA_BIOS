/** @file
  This file is SampleCode of the library for Intel CPU PEI Policy initialization.

 @copyright
  INTEL CONFIDENTIAL
  Copyright 2015 - 2022 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains an 'Intel Peripheral Driver' and is uniquely identified as
  "Intel Reference Module" and is licensed for Intel CPUs and chipsets under
  the terms of your license agreement with Intel or your vendor. This file may
  be modified by the user, subject to additional terms of the license agreement.

@par Specification Reference:
**/

#include <Library/DebugLib.h>
#include <Ppi/SiPolicy.h>
#include <FspmUpd.h>
#include <Library/ConfigBlockLib.h>
#include <Library/CpuPlatformLib.h>

/**
  This function performs CPU PEI Policy initialization in Pre-memory.

  @param[in] SiPreMemPolicyPpi     The SI Pre-Mem Policy PPI instance
  @param[in] FspmUpd               The pointer of FspmUpd

  @retval EFI_SUCCESS              The PPI is installed and initialized.
  @retval EFI ERRORS               The PPI is not successfully installed.
  @retval EFI_OUT_OF_RESOURCES     Do not have enough resources to initialize the driver
**/
EFI_STATUS
EFIAPI
FspUpdatePeiCpuPolicyPreMem (
  IN OUT SI_PREMEM_POLICY_PPI     *SiPreMemPolicyPpi,
  IN FSPM_UPD                     *FspmUpd
  )
{
  CPU_CONFIG_LIB_PREMEM_CONFIG    *CpuConfigLibPreMemConfig;
  OVERCLOCKING_PREMEM_CONFIG      *OverClockingPreMemConfig;
  EFI_STATUS                      Status;
  UINT8                           Index;
  UINT8                           AllCoreCount;
  UINT8                           AllSmallCoreCount;
  AllCoreCount          = 0;
  AllSmallCoreCount     = 0;

  Status = GetConfigBlock ((VOID *) SiPreMemPolicyPpi, &gOverclockingPreMemConfigGuid, (VOID *) &OverClockingPreMemConfig);
  ASSERT_EFI_ERROR (Status);
  Status = GetConfigBlock ((VOID *) SiPreMemPolicyPpi, &gCpuConfigLibPreMemConfigGuid, (VOID *) &CpuConfigLibPreMemConfig);
  ASSERT_EFI_ERROR (Status);


  //
  //  Update CpuOverClockingPreMem Config Block data
  //
  OverClockingPreMemConfig->OcSupport                   = FspmUpd->FspmConfig.OcSupport;
  OverClockingPreMemConfig->OcLock                      = FspmUpd->FspmConfig.OcLock;
  OverClockingPreMemConfig->PerCoreHtDisable            = FspmUpd->FspmConfig.PerCoreHtDisable;
  OverClockingPreMemConfig->CoreMaxOcRatio              = FspmUpd->FspmConfig.CoreMaxOcRatio;
  OverClockingPreMemConfig->CoreVoltageMode             = FspmUpd->FspmConfig.CoreVoltageMode;
  OverClockingPreMemConfig->RingMaxOcRatio              = FspmUpd->FspmConfig.RingMaxOcRatio;
  OverClockingPreMemConfig->RingDownBin                 = FspmUpd->FspmConfig.RingDownBin;
  OverClockingPreMemConfig->RingVoltageMode             = FspmUpd->FspmConfig.RingVoltageMode;
  OverClockingPreMemConfig->RingVoltageOverride         = FspmUpd->FspmConfig.RingVoltageOverride;
  OverClockingPreMemConfig->RingVoltageAdaptive         = FspmUpd->FspmConfig.RingVoltageAdaptive;
  OverClockingPreMemConfig->RingVoltageOffset           = FspmUpd->FspmConfig.RingVoltageOffset;
  OverClockingPreMemConfig->RingVfPointOffsetMode       = FspmUpd->FspmConfig.RingVfPointOffsetMode;
  OverClockingPreMemConfig->CoreVfPointOffsetMode       = FspmUpd->FspmConfig.CoreVfPointOffsetMode;
  OverClockingPreMemConfig->CoreVoltageOverride         = FspmUpd->FspmConfig.CoreVoltageOverride;
  OverClockingPreMemConfig->CoreVoltageAdaptive         = FspmUpd->FspmConfig.CoreVoltageAdaptive;
  OverClockingPreMemConfig->CoreVoltageOffset           = FspmUpd->FspmConfig.CoreVoltageOffset;
  OverClockingPreMemConfig->CorePllVoltageOffset        = FspmUpd->FspmConfig.CorePllVoltageOffset;
  OverClockingPreMemConfig->GtPllVoltageOffset          = FspmUpd->FspmConfig.GtPllVoltageOffset;
  OverClockingPreMemConfig->RingPllVoltageOffset        = FspmUpd->FspmConfig.RingPllVoltageOffset;
  OverClockingPreMemConfig->SaPllVoltageOffset          = FspmUpd->FspmConfig.SaPllVoltageOffset;
  OverClockingPreMemConfig->AtomPllVoltageOffset        = FspmUpd->FspmConfig.AtomPllVoltageOffset;
  OverClockingPreMemConfig->McPllVoltageOffset          = FspmUpd->FspmConfig.McPllVoltageOffset;
  OverClockingPreMemConfig->BclkAdaptiveVoltage         = FspmUpd->FspmConfig.BclkAdaptiveVoltage;
  OverClockingPreMemConfig->Avx2RatioOffset             = FspmUpd->FspmConfig.Avx2RatioOffset;
  OverClockingPreMemConfig->TjMaxOffset                 = FspmUpd->FspmConfig.TjMaxOffset;
  OverClockingPreMemConfig->Avx2VoltageScaleFactor      = FspmUpd->FspmConfig.Avx2VoltageScaleFactor;
  OverClockingPreMemConfig->TvbRatioClipping            = FspmUpd->FspmConfig.TvbRatioClipping;
  OverClockingPreMemConfig->TvbVoltageOptimization      = FspmUpd->FspmConfig.TvbVoltageOptimization;
  OverClockingPreMemConfig->CoreRatioExtensionMode      = FspmUpd->FspmConfig.CoreRatioExtensionMode;
  OverClockingPreMemConfig->PvdRatioThreshold           = FspmUpd->FspmConfig.PvdRatioThreshold;
  OverClockingPreMemConfig->DlvrBypassModeEnable        = FspmUpd->FspmConfig.DlvrBypassModeEnable;
  OverClockingPreMemConfig->SaPllFreqOverride           = FspmUpd->FspmConfig.SaPllFreqOverride;
  OverClockingPreMemConfig->TscDisableHwFixup           = FspmUpd->FspmConfig.TscDisableHwFixup;
  OverClockingPreMemConfig->TvbDownBinsTempThreshold0   = FspmUpd->FspmConfig.TvbDownBinsTempThreshold0;
  OverClockingPreMemConfig->TvbTempThreshold0           = FspmUpd->FspmConfig.TvbTempThreshold0;
  OverClockingPreMemConfig->TvbTempThreshold1           = FspmUpd->FspmConfig.TvbTempThreshold1;
  OverClockingPreMemConfig->TvbDownBinsTempThreshold1   = FspmUpd->FspmConfig.TvbDownBinsTempThreshold1;
  OverClockingPreMemConfig->FllOcModeEn                 = FspmUpd->FspmConfig.FllOcModeEn;
  OverClockingPreMemConfig->FllOverclockMode            = FspmUpd->FspmConfig.FllOverclockMode;
  OverClockingPreMemConfig->Etvb                        = FspmUpd->FspmConfig.Etvb;

  for (Index = 0; Index < CPU_OC_MAX_VF_POINTS; Index++) {
    OverClockingPreMemConfig->CoreVfPointRatio[Index] = FspmUpd->FspmConfig.CoreVfPointRatio[Index];
    if (FspmUpd->FspmConfig.CoreVfPointOffsetPrefix[Index] == 1) {
      OverClockingPreMemConfig->CoreVfPointOffset[Index] = (INT16)(~(FspmUpd->FspmConfig.CoreVfPointOffset[Index]) + 1);
    } else {
      OverClockingPreMemConfig->CoreVfPointOffset[Index] = (INT16)(FspmUpd->FspmConfig.CoreVfPointOffset[Index]);
    }
  }

  for (Index = 0; Index < CPU_OC_MAX_VF_POINTS; Index++) {
    OverClockingPreMemConfig->RingVfPointRatio[Index] = FspmUpd->FspmConfig.RingVfPointRatio[Index];
    if (FspmUpd->FspmConfig.RingVfPointOffsetPrefix[Index] == 1) {
      OverClockingPreMemConfig->RingVfPointOffset[Index] = (INT16)(~(FspmUpd->FspmConfig.RingVfPointOffset[Index]) + 1);
    } else {
      OverClockingPreMemConfig->RingVfPointOffset[Index] = (INT16)(FspmUpd->FspmConfig.RingVfPointOffset[Index]);
    }
  }
  OverClockingPreMemConfig->RingVfPointCount         = FspmUpd->FspmConfig.RingVfPointCount;
  OverClockingPreMemConfig->CoreVfPointCount         = FspmUpd->FspmConfig.CoreVfPointCount;
  OverClockingPreMemConfig->CoreVfConfigScope        = FspmUpd->FspmConfig.CoreVfConfigScope;
  if (OverClockingPreMemConfig->CoreVfConfigScope) { // Per Core
    for (Index = 0; Index < (FspmUpd->FspmConfig.ActiveCoreCount); Index++) {
      if (FspmUpd->FspmConfig.PerCoreVoltageOffsetPrefix[Index] == 1) {
        OverClockingPreMemConfig->PerCoreVoltageOffset1[Index] = (INT16)(~(FspmUpd->FspmConfig.PerCoreVoltageOffset[Index]) + 1);
      } else {
        OverClockingPreMemConfig->PerCoreVoltageOffset1[Index] = (INT16)(FspmUpd->FspmConfig.PerCoreVoltageOffset[Index]);
      }
    }
    // Per Atom Cluster
    for (Index = 0; Index < (FspmUpd->FspmConfig.ActiveSmallCoreCount/4); Index++) {
      if (FspmUpd->FspmConfig.PerAtomClusterVoltageOffsetPrefix[Index] == 1) {
        OverClockingPreMemConfig->PerAtomClusterVoltageOffset[Index] = (INT16)(~(FspmUpd->FspmConfig.PerAtomClusterVoltageOffset[Index]) + 1);
      } else {
        OverClockingPreMemConfig->PerAtomClusterVoltageOffset[Index] = (INT16)(FspmUpd->FspmConfig.PerAtomClusterVoltageOffset[Index]);
      }
    }
  }

  OverClockingPreMemConfig->DisablePerCoreMask          = FspmUpd->FspmConfig.DisablePerCoreMask;

  //
  // ATOM Overclocking
  //
  OverClockingPreMemConfig->AtomL2VoltageMode          = FspmUpd->FspmConfig.AtomL2VoltageMode;
  OverClockingPreMemConfig->AtomL2VoltageOverride      = FspmUpd->FspmConfig.AtomL2VoltageOverride;
  OverClockingPreMemConfig->AtomL2VoltageAdaptive      = FspmUpd->FspmConfig.AtomL2VoltageAdaptive;
  OverClockingPreMemConfig->AtomL2VoltageOffset        = FspmUpd->FspmConfig.AtomL2VoltageOffset;

  //
  // SA/Uncore Voltage configurations
  //
  OverClockingPreMemConfig->SaVoltageMode            = FspmUpd->FspmConfig.SaVoltageMode;
  OverClockingPreMemConfig->SaVoltageOverride        = FspmUpd->FspmConfig.SaVoltageOverride;
  OverClockingPreMemConfig->SaExtraTurboVoltage      = FspmUpd->FspmConfig.SaExtraTurboVoltage;

  //
  // CPU BCLK OC Frequency
  //
  OverClockingPreMemConfig->CpuBclkOcFrequency       = FspmUpd->FspmConfig.CpuBclkOcFrequency;

  //
  // CEP enable/disable
  //
  OverClockingPreMemConfig->IaCepEnable              = FspmUpd->FspmConfig.IaCepEnable;
  OverClockingPreMemConfig->GtCepEnable              = FspmUpd->FspmConfig.GtCepEnable;

  //
  //  Update CpuConfigLibPreMem Config Block data
  //
  CpuConfigLibPreMemConfig->HyperThreading           = FspmUpd->FspmConfig.HyperThreading;
  CpuConfigLibPreMemConfig->BootFrequency            = FspmUpd->FspmConfig.BootFrequency;

  //
  // Converting 0xFF to the actual total supported core count.
  //
  GetCpuSupportedCoresAndAtomCores (&AllCoreCount, &AllSmallCoreCount);
  if (FspmUpd->FspmConfig.ActiveCoreCount == 0xFF) {
    FspmUpd->FspmConfig.ActiveCoreCount = AllCoreCount;
  }
  if (FspmUpd->FspmConfig.ActiveSmallCoreCount == 0xFF) {
    FspmUpd->FspmConfig.ActiveSmallCoreCount = AllSmallCoreCount;
  }
  if (FspmUpd->FspmConfig.ActiveCoreCount == 0 && FspmUpd->FspmConfig.ActiveSmallCoreCount == 0) {
    FspmUpd->FspmConfig.ActiveCoreCount = AllCoreCount;
    FspmUpd->FspmConfig.ActiveSmallCoreCount = AllSmallCoreCount;
  }

  CpuConfigLibPreMemConfig->ActiveCoreCount1         = FspmUpd->FspmConfig.ActiveCoreCount;
  CpuConfigLibPreMemConfig->ActiveSmallCoreCount     = FspmUpd->FspmConfig.ActiveSmallCoreCount;
  CpuConfigLibPreMemConfig->FClkFrequency            = FspmUpd->FspmConfig.FClkFrequency;
  CpuConfigLibPreMemConfig->JtagC10PowerGateDisable  = FspmUpd->FspmConfig.JtagC10PowerGateDisable;
  CpuConfigLibPreMemConfig->BistOnReset              = FspmUpd->FspmConfig.BistOnReset;
  CpuConfigLibPreMemConfig->VmxEnable                = FspmUpd->FspmConfig.VmxEnable;
  CpuConfigLibPreMemConfig->CpuRatio                 = FspmUpd->FspmConfig.CpuRatio;
  CpuConfigLibPreMemConfig->TmeEnable                = FspmUpd->FspmConfig.TmeEnable;
  CpuConfigLibPreMemConfig->CrashLogEnable           = FspmUpd->FspmConfig.CpuCrashLogEnable;
  CpuConfigLibPreMemConfig->DebugInterfaceEnable     = FspmUpd->FspmConfig.DebugInterfaceEnable;
  CpuConfigLibPreMemConfig->DebugInterfaceLockEnable = FspmUpd->FspmConfig.DebugInterfaceLockEnable;
  CpuConfigLibPreMemConfig->BclkSource               = FspmUpd->FspmConfig.BclkSource;
  CpuConfigLibPreMemConfig->ConfigTdpLevel           = FspmUpd->FspmConfig.ConfigTdpLevel;
  CpuConfigLibPreMemConfig->CustomPowerLimit1        = FspmUpd->FspmConfig.CustomPowerLimit1;

  //
  // Core Ratio Override
  //
  if (OverClockingPreMemConfig->PerCoreRatioOverride) {
    for (Index = 0; Index < (FspmUpd->FspmConfig.ActiveCoreCount); Index++) {
      OverClockingPreMemConfig->PerCoreRatio[Index] = FspmUpd->FspmConfig.PerCoreRatio[Index];
    }
    for (Index = 0; Index < ((FspmUpd->FspmConfig.ActiveSmallCoreCount) / ATOM_QUANTITY_IN_CLUSTER); Index++) {
      OverClockingPreMemConfig->AtomClusterRatio[Index] = FspmUpd->FspmConfig.AtomClusterRatio[Index];
    }
  }

  //
  // Unlimited ICCMAX
  //
  OverClockingPreMemConfig->IaIccUnlimitedMode       = FspmUpd->FspmConfig.IaIccUnlimitedMode;
  OverClockingPreMemConfig->IaIccMax                 = FspmUpd->FspmConfig.IaIccMax;
  OverClockingPreMemConfig->GtIccUnlimitedMode       = FspmUpd->FspmConfig.GtIccUnlimitedMode;
  OverClockingPreMemConfig->GtIccMax                 = FspmUpd->FspmConfig.GtIccMax;

  return EFI_SUCCESS;
}
