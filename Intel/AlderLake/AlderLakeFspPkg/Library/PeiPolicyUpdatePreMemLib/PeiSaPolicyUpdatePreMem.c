/** @file
  Do Platform Stage System Agent initialization.

 @copyright
  INTEL CONFIDENTIAL
  Copyright 2015 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains an 'Intel Peripheral Driver' and is uniquely identified as
  "Intel Reference Module" and is licensed for Intel CPUs and chipsets under
  the terms of your license agreement with Intel or your vendor. This file may
  be modified by the user, subject to additional terms of the license agreement.

@par Specification Reference:
**/

#include <FspEas.h>
#include <Library/BaseMemoryLib.h>
#include <Library/PeiSaPolicyLib.h>
#include <IpuPreMemConfig.h>
#include <Library/FspCommonLib.h>
#include <Library/DebugLib.h>
#include <FspmUpd.h>
#include <Library/PreSiliconEnvDetectLib.h>
#include <Library/CpuPcieInfoFruLib.h>
#include <Library/VtdInfoLib.h>
#if FixedPcdGetBool(PcdITbtEnable) == 1
#include <TcssPeiPreMemConfig.h>
#endif
#include <TelemetryPeiConfig.h>
#include <HostBridgeConfig.h>
#include <CpuDmiPreMemConfig.h>

/**
  UpdatePeiSaPolicy performs SA PEI Policy initialization in pre-memory

  @param[in, out] SiPreMemPolicyPpi       SI_PREMEM_POLICY_PPI
  @param[in]      FspmUpd                 The pointer of FspmUpd

  @retval EFI_SUCCESS              The policy is installed and initialized.
**/
EFI_STATUS
FspUpdatePeiSaPolicyPreMem (
  IN OUT SI_PREMEM_POLICY_PPI       *SiPreMemPolicyPpi,
  IN     FSPM_UPD                   *FspmUpd
  )
{
  EFI_STATUS                    Status;
  SA_MISC_PEI_PREMEM_CONFIG     *MiscPeiPreMemConfig;
  MEMORY_CONFIG_NO_CRC          *MemConfigNoCrc;
  GRAPHICS_PEI_PREMEM_CONFIG    *GtPreMemConfig;
  VTD_CONFIG                    *Vtd;
  MEMORY_CONFIGURATION          *MemConfig;
#if FixedPcdGetBool(PcdCpuPcieEnable) == 1
  PCIE_PEI_PREMEM_CONFIG        *PciePeiPreMemConfig;
  CPU_PCIE_RP_PREMEM_CONFIG     *CpuPcieRpPreMemConfig;
#endif
#if FixedPcdGetBool(PcdHgEnable) == 1
  HYBRID_GRAPHICS_CONFIG        *HgGpioData;
#endif
  OVERCLOCKING_PREMEM_CONFIG    *OcPreMemConfig;
  IPU_PREMEM_CONFIG             *IpuPreMemPolicy;
  CPU_TRACE_HUB_PREMEM_CONFIG   *CpuTraceHubPreMemConfig;
  FSPM_UPD                      *FspmUpdDataPtr;
  UINT8                         Index;
  CPU_DMI_PREMEM_CONFIG         *CpuDmiPreMemConfig;
#if FixedPcdGetBool(PcdITbtEnable) == 1
  TCSS_PEI_PREMEM_CONFIG        *TcssPeiPreMemConfig;
#endif
  TELEMETRY_PEI_PREMEM_CONFIG   *TelemetryPreMemConfig;
  HOST_BRIDGE_PREMEM_CONFIG     *HostBridgePreMemConfig;
  DEBUG ((DEBUG_INFO | DEBUG_INIT, "FSP UpdatePeiSaPolicyPreMem\n"));

  FspmUpdDataPtr = (FSPM_UPD *) GetFspMemoryInitUpdDataPointer ();

  HostBridgePreMemConfig = NULL;
  Status = GetConfigBlock ((VOID *) SiPreMemPolicyPpi, &gHostBridgePeiPreMemConfigGuid, (VOID *) &HostBridgePreMemConfig);
  ASSERT_EFI_ERROR (Status);

  MiscPeiPreMemConfig = NULL;
  Status = GetConfigBlock ((VOID *) SiPreMemPolicyPpi, &gSaMiscPeiPreMemConfigGuid, (VOID *) &MiscPeiPreMemConfig);
  ASSERT_EFI_ERROR (Status);

  GtPreMemConfig = NULL;
  Status = GetConfigBlock ((VOID *) SiPreMemPolicyPpi, &gGraphicsPeiPreMemConfigGuid, (VOID *) &GtPreMemConfig);
  ASSERT_EFI_ERROR (Status);

  MemConfig = NULL;
  Status = GetConfigBlock ((VOID *) SiPreMemPolicyPpi, &gMemoryConfigGuid, (VOID *) &MemConfig);
  ASSERT_EFI_ERROR (Status);

  Vtd = NULL;
  Status = GetConfigBlock ((VOID *) SiPreMemPolicyPpi, &gVtdConfigGuid, (VOID *)&Vtd);
  ASSERT_EFI_ERROR (Status);

  MemConfigNoCrc = NULL;
  Status = GetConfigBlock ((VOID *) SiPreMemPolicyPpi, &gMemoryConfigNoCrcGuid, (VOID *) &MemConfigNoCrc);
  ASSERT_EFI_ERROR (Status);

  CpuDmiPreMemConfig = NULL;
  Status = GetConfigBlock ((VOID *) SiPreMemPolicyPpi, &gCpuDmiPreMemConfigGuid, (VOID *)&CpuDmiPreMemConfig);
  ASSERT_EFI_ERROR (Status);

#if FixedPcdGetBool(PcdCpuPcieEnable) == 1
  PciePeiPreMemConfig = NULL;
  Status = GetConfigBlock ((VOID *) SiPreMemPolicyPpi, &gCpuPciePeiPreMemConfigGuid, (VOID *) &PciePeiPreMemConfig);
  ASSERT_EFI_ERROR (Status);

  CpuPcieRpPreMemConfig = NULL;
  Status = GetConfigBlock ((VOID *)SiPreMemPolicyPpi, &gCpuPcieRpPrememConfigGuid, (VOID *)&CpuPcieRpPreMemConfig);
  ASSERT_EFI_ERROR(Status);
#endif
#if FixedPcdGetBool(PcdHgEnable) == 1
  HgGpioData = NULL;
  Status = GetConfigBlock ((VOID *) SiPreMemPolicyPpi, &gHybridGraphicsConfigGuid, (VOID *) &HgGpioData);
  ASSERT_EFI_ERROR (Status);
#endif

  CpuTraceHubPreMemConfig = NULL;
  Status = GetConfigBlock ((VOID *) SiPreMemPolicyPpi, &gCpuTraceHubPreMemConfigGuid, (VOID *) &CpuTraceHubPreMemConfig);
  ASSERT_EFI_ERROR (Status);

  OcPreMemConfig = NULL;
  Status = GetConfigBlock((VOID *) SiPreMemPolicyPpi, &gOverclockingPreMemConfigGuid, (VOID *) &OcPreMemConfig);
  ASSERT_EFI_ERROR(Status);

  IpuPreMemPolicy = NULL;
  Status = GetConfigBlock((VOID *) SiPreMemPolicyPpi, &gIpuPreMemConfigGuid, (VOID *)&IpuPreMemPolicy);


#if FixedPcdGetBool(PcdITbtEnable) == 1
  TcssPeiPreMemConfig = NULL;
  Status = GetConfigBlock ((VOID *) SiPreMemPolicyPpi, &gTcssPeiPreMemConfigGuid, (VOID *) &TcssPeiPreMemConfig);
  ASSERT_EFI_ERROR (Status);
#endif

  TelemetryPreMemConfig = NULL;
  Status = GetConfigBlock ((VOID *) SiPreMemPolicyPpi, &gTelemetryPeiPreMemConfigGuid, (VOID *) &TelemetryPreMemConfig);
  ASSERT_EFI_ERROR (Status);

  // Update ConfigBlock: MemConfigNoCrc
  //

  // TGL
  // Update SpdData only if SpdAddressTable is zero for each DIMM slot
  // MRC uses the following layout:
  // datOemSpd_0_0_0,      ///< use for get the device address for controller 0 channel 0 dimm 0 - DDR4 - DDR5 - LPDDR4 - LPDDR5
  // datOemSpd_0_0_1,      ///< use for get the device address for controller 0 channel 0 dimm 1 - DDR4
  // datOemSpd_0_1_0,      ///< use for get the device address for controller 0 channel 1 dimm 0 -------- DDR5 - LPDDR4 - LPDDR5
  // datOemSpd_0_1_1,      ///< use for get the device address for controller 0 channel 1 dimm 1 -------- DDR5 2DPC
  // datOemSpd_0_2_0,      ///< use for get the device address for controller 0 channel 2 dimm 0 --------------- LPDDR4 - LPDDR5
  // datOemSpd_0_3_0 = 6,  ///< use for get the device address for controller 0 channel 3 dimm 0 --------------- LPDDR4 - LPDDR5
  // datOemSpd_1_0_0 = 8,  ///< use for get the device address for controller 1 channel 0 dimm 0 - DDR4 - DDR5 - LPDDR4 - LPDDR5
  // datOemSpd_1_0_1,      ///< use for get the device address for controller 1 channel 0 dimm 1 - DDR4
  // datOemSpd_1_1_0,      ///< use for get the device address for controller 1 channel 1 dimm 0 -------- DDR5 - LPDDR4 - LPDDR5
  // datOemSpd_1_1_1,      ///< use for get the device address for controller 1 channel 1 dimm 1 -------- DDR5 2DPC
  // datOemSpd_1_2_0,      ///< use for get the device address for controller 1 channel 2 dimm 0 --------------- LPDDR4 - LPDDR5
  // datOemSpd_1_3_0 = 14, ///< use for get the device address for controller 1 channel 3 dimm 0 --------------- LPDDR4 - LPDDR5
  ZeroMem ((VOID *) MemConfigNoCrc->SpdData->SpdData, sizeof (SPD_DATA_BUFFER));

  if (((VOID *)FspmUpd->FspmConfig.MemorySpdPtr000 != NULL) && (FspmUpd->FspmConfig.SpdAddressTable[0] == 0)) {
    CopyMem ((VOID *) MemConfigNoCrc->SpdData->SpdData[0][0][0], (VOID *)FspmUpd->FspmConfig.MemorySpdPtr000, FspmUpd->FspmConfig.MemorySpdDataLen);
  }

  if (((VOID *)FspmUpd->FspmConfig.MemorySpdPtr001 != NULL) && (FspmUpd->FspmConfig.SpdAddressTable[1] == 0)) {
    CopyMem ((VOID *) MemConfigNoCrc->SpdData->SpdData[0][0][1], (VOID*)FspmUpd->FspmConfig.MemorySpdPtr001, FspmUpd->FspmConfig.MemorySpdDataLen);
  }

  if (((VOID *)FspmUpd->FspmConfig.MemorySpdPtr010 != NULL) && (FspmUpd->FspmConfig.SpdAddressTable[2] == 0)) {
    CopyMem ((VOID *) MemConfigNoCrc->SpdData->SpdData[0][1][0], (VOID*)FspmUpd->FspmConfig.MemorySpdPtr010, FspmUpd->FspmConfig.MemorySpdDataLen);
  }

  if (((VOID *)FspmUpd->FspmConfig.MemorySpdPtr011 != NULL) && (FspmUpd->FspmConfig.SpdAddressTable[3] == 0)) {
    CopyMem ((VOID *) MemConfigNoCrc->SpdData->SpdData[0][1][1], (VOID*)FspmUpd->FspmConfig.MemorySpdPtr011, FspmUpd->FspmConfig.MemorySpdDataLen);
  }
  if (((VOID *)FspmUpd->FspmConfig.MemorySpdPtr020 != NULL) && (FspmUpd->FspmConfig.SpdAddressTable[4] == 0)) {
    CopyMem ((VOID *) MemConfigNoCrc->SpdData->SpdData[0][2][0], (VOID *)FspmUpd->FspmConfig.MemorySpdPtr020, FspmUpd->FspmConfig.MemorySpdDataLen);
  }

  if (((VOID *)FspmUpd->FspmConfig.MemorySpdPtr021 != NULL) && (FspmUpd->FspmConfig.SpdAddressTable[5] == 0)) {
    CopyMem ((VOID *) MemConfigNoCrc->SpdData->SpdData[0][2][1], (VOID*)FspmUpd->FspmConfig.MemorySpdPtr021, FspmUpd->FspmConfig.MemorySpdDataLen);
  }

  if (((VOID *)FspmUpd->FspmConfig.MemorySpdPtr030 != NULL) && (FspmUpd->FspmConfig.SpdAddressTable[6] == 0)) {
    CopyMem ((VOID *) MemConfigNoCrc->SpdData->SpdData[0][3][0], (VOID*)FspmUpd->FspmConfig.MemorySpdPtr030, FspmUpd->FspmConfig.MemorySpdDataLen);
  }

  if (((VOID *)FspmUpd->FspmConfig.MemorySpdPtr031 != NULL) && (FspmUpd->FspmConfig.SpdAddressTable[7] == 0)) {
    CopyMem ((VOID *) MemConfigNoCrc->SpdData->SpdData[0][3][1], (VOID*)FspmUpd->FspmConfig.MemorySpdPtr031, FspmUpd->FspmConfig.MemorySpdDataLen);
  }

  if (((VOID *)FspmUpd->FspmConfig.MemorySpdPtr100 != NULL) && (FspmUpd->FspmConfig.SpdAddressTable[8] == 0)) {
    CopyMem ((VOID *) MemConfigNoCrc->SpdData->SpdData[1][0][0], (VOID *)FspmUpd->FspmConfig.MemorySpdPtr100, FspmUpd->FspmConfig.MemorySpdDataLen);
  }

  if (((VOID *)FspmUpd->FspmConfig.MemorySpdPtr101 != NULL) && (FspmUpd->FspmConfig.SpdAddressTable[9] == 0)) {
    CopyMem ((VOID *) MemConfigNoCrc->SpdData->SpdData[1][0][1], (VOID*)FspmUpd->FspmConfig.MemorySpdPtr101, FspmUpd->FspmConfig.MemorySpdDataLen);
  }

  if (((VOID *)FspmUpd->FspmConfig.MemorySpdPtr110 != NULL) && (FspmUpd->FspmConfig.SpdAddressTable[10] == 0)) {
    CopyMem ((VOID *) MemConfigNoCrc->SpdData->SpdData[1][1][0], (VOID*)FspmUpd->FspmConfig.MemorySpdPtr110, FspmUpd->FspmConfig.MemorySpdDataLen);
  }

  if (((VOID *)FspmUpd->FspmConfig.MemorySpdPtr111 != NULL) && (FspmUpd->FspmConfig.SpdAddressTable[11] == 0)) {
    CopyMem ((VOID *) MemConfigNoCrc->SpdData->SpdData[1][1][1], (VOID*)FspmUpd->FspmConfig.MemorySpdPtr111, FspmUpd->FspmConfig.MemorySpdDataLen);
  }

  if (((VOID *)FspmUpd->FspmConfig.MemorySpdPtr120 != NULL) && (FspmUpd->FspmConfig.SpdAddressTable[12] == 0)) {
    CopyMem ((VOID *) MemConfigNoCrc->SpdData->SpdData[1][2][0], (VOID *)FspmUpd->FspmConfig.MemorySpdPtr120, FspmUpd->FspmConfig.MemorySpdDataLen);
  }

  if (((VOID *)FspmUpd->FspmConfig.MemorySpdPtr121 != NULL) && (FspmUpd->FspmConfig.SpdAddressTable[13] == 0)) {
    CopyMem ((VOID *) MemConfigNoCrc->SpdData->SpdData[1][2][1], (VOID*)FspmUpd->FspmConfig.MemorySpdPtr121, FspmUpd->FspmConfig.MemorySpdDataLen);
  }

  if (((VOID *)FspmUpd->FspmConfig.MemorySpdPtr130 != NULL) && (FspmUpd->FspmConfig.SpdAddressTable[14] == 0)) {
    CopyMem ((VOID *) MemConfigNoCrc->SpdData->SpdData[1][3][0], (VOID*)FspmUpd->FspmConfig.MemorySpdPtr130, FspmUpd->FspmConfig.MemorySpdDataLen);
  }

  if (((VOID *)FspmUpd->FspmConfig.MemorySpdPtr131 != NULL) && (FspmUpd->FspmConfig.SpdAddressTable[15] == 0)) {
    CopyMem ((VOID *) MemConfigNoCrc->SpdData->SpdData[1][3][1], (VOID*)FspmUpd->FspmConfig.MemorySpdPtr131, FspmUpd->FspmConfig.MemorySpdDataLen);
  }

  if (FspmUpd->FspmConfig.DqsMapCpu2DramMc0Ch0 != NULL) {
    CopyMem ((VOID *) MemConfigNoCrc->DqDqsMap->DqsMapCpu2Dram[0][0], &FspmUpd->FspmConfig.DqsMapCpu2DramMc0Ch0[0], MEM_CFG_NUM_BYTES_MAPPED);
  }

  if (FspmUpd->FspmConfig.DqsMapCpu2DramMc0Ch1 != NULL) {
    CopyMem ((VOID *) MemConfigNoCrc->DqDqsMap->DqsMapCpu2Dram[0][1], &FspmUpd->FspmConfig.DqsMapCpu2DramMc0Ch1[0], MEM_CFG_NUM_BYTES_MAPPED);
  }

  if (FspmUpd->FspmConfig.DqsMapCpu2DramMc0Ch2 != NULL) {
    CopyMem ((VOID *) MemConfigNoCrc->DqDqsMap->DqsMapCpu2Dram[0][2], &FspmUpd->FspmConfig.DqsMapCpu2DramMc0Ch2[0], MEM_CFG_NUM_BYTES_MAPPED);
  }

  if (FspmUpd->FspmConfig.DqsMapCpu2DramMc0Ch3 != NULL) {
    CopyMem ((VOID *) MemConfigNoCrc->DqDqsMap->DqsMapCpu2Dram[0][3], &FspmUpd->FspmConfig.DqsMapCpu2DramMc0Ch3[0], MEM_CFG_NUM_BYTES_MAPPED);
  }

  if (FspmUpd->FspmConfig.DqsMapCpu2DramMc1Ch0 != NULL) {
    CopyMem ((VOID *) MemConfigNoCrc->DqDqsMap->DqsMapCpu2Dram[1][0], &FspmUpd->FspmConfig.DqsMapCpu2DramMc1Ch0[0], MEM_CFG_NUM_BYTES_MAPPED);
  }

  if (FspmUpd->FspmConfig.DqsMapCpu2DramMc1Ch1 != NULL) {
    CopyMem ((VOID *) MemConfigNoCrc->DqDqsMap->DqsMapCpu2Dram[1][1], &FspmUpd->FspmConfig.DqsMapCpu2DramMc1Ch1[0], MEM_CFG_NUM_BYTES_MAPPED);
  }

  if (FspmUpd->FspmConfig.DqsMapCpu2DramMc1Ch2 != NULL) {
    CopyMem ((VOID *) MemConfigNoCrc->DqDqsMap->DqsMapCpu2Dram[1][2], &FspmUpd->FspmConfig.DqsMapCpu2DramMc1Ch2[0], MEM_CFG_NUM_BYTES_MAPPED);
  }

  if (FspmUpd->FspmConfig.DqsMapCpu2DramMc1Ch3 != NULL) {
    CopyMem ((VOID *) MemConfigNoCrc->DqDqsMap->DqsMapCpu2Dram[1][3], &FspmUpd->FspmConfig.DqsMapCpu2DramMc1Ch3[0], MEM_CFG_NUM_BYTES_MAPPED);
  }

    if (FspmUpd->FspmConfig.DqMapCpu2DramMc0Ch0 != NULL) {
    CopyMem ((VOID *) MemConfigNoCrc->DqDqsMap->DqMapCpu2Dram[0][0][0], &FspmUpd->FspmConfig.DqMapCpu2DramMc0Ch0[0], MEM_CFG_NUM_BYTES_MAPPED * 8);
  }

  if (FspmUpd->FspmConfig.DqMapCpu2DramMc0Ch1 != NULL) {
    CopyMem ((VOID *) MemConfigNoCrc->DqDqsMap->DqMapCpu2Dram[0][1][0], &FspmUpd->FspmConfig.DqMapCpu2DramMc0Ch1[0], MEM_CFG_NUM_BYTES_MAPPED * 8);
  }

  if (FspmUpd->FspmConfig.DqMapCpu2DramMc0Ch2 != NULL) {
    CopyMem ((VOID *) MemConfigNoCrc->DqDqsMap->DqMapCpu2Dram[0][2][0], &FspmUpd->FspmConfig.DqMapCpu2DramMc0Ch2[0], MEM_CFG_NUM_BYTES_MAPPED * 8);
  }

  if (FspmUpd->FspmConfig.DqMapCpu2DramMc0Ch3 != NULL) {
    CopyMem ((VOID *) MemConfigNoCrc->DqDqsMap->DqMapCpu2Dram[0][3][0], &FspmUpd->FspmConfig.DqMapCpu2DramMc0Ch3[0], MEM_CFG_NUM_BYTES_MAPPED * 8);
  }

  if (FspmUpd->FspmConfig.DqMapCpu2DramMc1Ch0 != NULL) {
    CopyMem ((VOID *) MemConfigNoCrc->DqDqsMap->DqMapCpu2Dram[1][0][0], &FspmUpd->FspmConfig.DqMapCpu2DramMc1Ch0[0], MEM_CFG_NUM_BYTES_MAPPED * 8);
  }

  if (FspmUpd->FspmConfig.DqMapCpu2DramMc1Ch1 != NULL) {
    CopyMem ((VOID *) MemConfigNoCrc->DqDqsMap->DqMapCpu2Dram[1][1][0], &FspmUpd->FspmConfig.DqMapCpu2DramMc1Ch1[0], MEM_CFG_NUM_BYTES_MAPPED * 8);
  }

  if (FspmUpd->FspmConfig.DqMapCpu2DramMc1Ch2 != NULL) {
    CopyMem ((VOID *) MemConfigNoCrc->DqDqsMap->DqMapCpu2Dram[1][2][0], &FspmUpd->FspmConfig.DqMapCpu2DramMc1Ch2[0], MEM_CFG_NUM_BYTES_MAPPED * 8);
  }

  if (FspmUpd->FspmConfig.DqMapCpu2DramMc1Ch3 != NULL) {
    CopyMem ((VOID *) MemConfigNoCrc->DqDqsMap->DqMapCpu2Dram[1][3][0], &FspmUpd->FspmConfig.DqMapCpu2DramMc1Ch3[0], MEM_CFG_NUM_BYTES_MAPPED * 8);
  }

  MemConfigNoCrc->RcompData->RcompResistor = FspmUpd->FspmConfig.RcompResistor;

  if (FspmUpd->FspmConfig.RcompTarget != NULL) {
    CopyMem ((VOID *) &(MemConfigNoCrc->RcompData->RcompTarget[0]), (VOID *) &FspmUpd->FspmConfig.RcompTarget[0], 10);
  }

  MemConfigNoCrc->PlatformMemorySize = FspmUpd->FspmConfig.PlatformMemorySize;
  MemConfigNoCrc->SerialDebugLevel   = FspmUpd->FspmConfig.SerialDebugMrcLevel;
  MemConfigNoCrc->CleanMemory        = FspmUpd->FspmConfig.CleanMemory;
  MemConfigNoCrc->MemTestOnWarmBoot  = FspmUpd->FspmConfig.MemTestOnWarmBoot;
  //
  // Update ConfigBlock: MemConfig
  //
  MemConfig->DqPinsInterleaved      = FspmUpd->FspmConfig.DqPinsInterleaved;
  MemConfig->MrcFastBoot            = FspmUpd->FspmConfig.MrcFastBoot;
  MemConfig->SaGv                   = FspmUpd->FspmConfig.SaGv;
  for (Index = 0; Index < MEM_MAX_SAGV_POINTS; Index++) {
    MemConfig->SaGvFreq[Index]      = FspmUpd->FspmConfig.SaGvFreq[Index];
    MemConfig->SaGvGear[Index]      = FspmUpd->FspmConfig.SaGvGear[Index];
  }
  MemConfig->Lp5BankMode            = FspmUpd->FspmConfig.Lp5BankMode;
  MemConfig->GearRatio              = FspmUpd->FspmConfig.GearRatio;
  MemConfig->DdrFreqLimit           = FspmUpd->FspmConfig.DdrFreqLimit;
  MemConfig->DisableChannel[0][0]   = FspmUpd->FspmConfig.DisableMc0Ch0;
  MemConfig->DisableChannel[0][1]   = FspmUpd->FspmConfig.DisableMc0Ch1;
  MemConfig->DisableChannel[0][2]   = FspmUpd->FspmConfig.DisableMc0Ch2;
  MemConfig->DisableChannel[0][3]   = FspmUpd->FspmConfig.DisableMc0Ch3;
  MemConfig->DisableChannel[1][0]   = FspmUpd->FspmConfig.DisableMc1Ch0;
  MemConfig->DisableChannel[1][1]   = FspmUpd->FspmConfig.DisableMc1Ch1;
  MemConfig->DisableChannel[1][2]   = FspmUpd->FspmConfig.DisableMc1Ch2;
  MemConfig->DisableChannel[1][3]   = FspmUpd->FspmConfig.DisableMc1Ch3;
  MemConfig->MrcSafeConfig          = FspmUpd->FspmConfig.MrcSafeConfig;
  MemConfig->LpDqsOscEn             = FspmUpd->FspmConfig.LpDdrDqDqsReTraining;
  MemConfig->TrainTrace             = FspmUpd->FspmConfig.TrainTrace;
  MemConfig->RmtPerTask             = FspmUpd->FspmConfig.RmtPerTask;
  MemConfig->ScramblerSupport       = FspmUpd->FspmConfig.ScramblerSupport;
  MemConfig->SafeMode               = FspmUpd->FspmConfig.SafeMode;
  MemConfig->Ddr4OneDpc             = FspmUpd->FspmConfig.Ddr4OneDpc;
  //
  // Enabling Probeless Trace will need to enable IED together
  //
  MemConfig->ProbelessTrace = FspmUpd->FspmConfig.ProbelessTrace;

  if (MiscPeiPreMemConfig != NULL) {
    MiscPeiPreMemConfig->UserBd = FspmUpd->FspmConfig.UserBd;
  }

  MemConfig->DynamicMemoryBoost      = FspmUpd->FspmConfig.DynamicMemoryBoost;
  MemConfig->RealtimeMemoryFrequency = FspmUpd->FspmConfig.RealtimeMemoryFrequency;
  MemConfig->SpdProfileSelected      = FspmUpd->FspmConfig.SpdProfileSelected;
  MemConfig->VddVoltage              = FspmUpd->FspmConfig.VddVoltage;
  MemConfig->VddqVoltage             = FspmUpd->FspmConfig.VddqVoltage;
  MemConfig->VppVoltage              = FspmUpd->FspmConfig.VppVoltage;
  MemConfig->RefClk                  = FspmUpd->FspmConfig.RefClk;
  MemConfig->Ratio                   = FspmUpd->FspmConfig.Ratio;
  MemConfig->tCL                     = FspmUpd->FspmConfig.tCL;
  MemConfig->tCWL                    = FspmUpd->FspmConfig.tCWL;
  MemConfig->tFAW                    = FspmUpd->FspmConfig.tFAW;
  MemConfig->tRAS                    = FspmUpd->FspmConfig.tRAS;
  MemConfig->tRCDtRP                 = FspmUpd->FspmConfig.tRCDtRP;
  MemConfig->tREFI                   = FspmUpd->FspmConfig.tREFI;
  MemConfig->tRFC                    = FspmUpd->FspmConfig.tRFC;
  MemConfig->tRRD                    = FspmUpd->FspmConfig.tRRD;
  MemConfig->tRTP                    = FspmUpd->FspmConfig.tRTP;
  MemConfig->tWR                     = FspmUpd->FspmConfig.tWR;
  MemConfig->tWTR                    = FspmUpd->FspmConfig.tWTR;
  MemConfig->tRFCpb                  = FspmUpd->FspmConfig.tRFCpb;
  MemConfig->tRFC2                   = FspmUpd->FspmConfig.tRFC2;
  MemConfig->tRFC4                   = FspmUpd->FspmConfig.tRFC4;
  MemConfig->tRRD_L                  = FspmUpd->FspmConfig.tRRD_L;
  MemConfig->tRRD_S                  = FspmUpd->FspmConfig.tRRD_S;
  MemConfig->tWTR_L                  = FspmUpd->FspmConfig.tWTR_L;
  MemConfig->tCCD_L                  = FspmUpd->FspmConfig.tCCD_L;
  MemConfig->tWTR_S                  = FspmUpd->FspmConfig.tWTR_S;
  MemConfig->NModeSupport            = FspmUpd->FspmConfig.NModeSupport;
  if (IsSimicsEnvironment()) {
    MemConfig->SimicsFlag = 1;
  } else {
    MemConfig->SimicsFlag = 0;
  }
  MemConfig->HobBufferSize           = FspmUpd->FspmConfig.HobBufferSize;

  MemConfig->ECT                     = FspmUpd->FspmConfig.ECT;
  MemConfig->SOT                     = FspmUpd->FspmConfig.SOT;
  MemConfig->ERDMPRTC2D              = FspmUpd->FspmConfig.ERDMPRTC2D;
  MemConfig->RDMPRT                  = FspmUpd->FspmConfig.RDMPRT;
  MemConfig->DRAMDCA                 = FspmUpd->FspmConfig.DRAMDCA;
  MemConfig->WRDS                    = FspmUpd->FspmConfig.WRDS;
  MemConfig->RCVET                   = FspmUpd->FspmConfig.RCVET;
  MemConfig->JWRL                    = FspmUpd->FspmConfig.JWRL;
  MemConfig->EWRTC2D                 = FspmUpd->FspmConfig.EWRTC2D;
  MemConfig->ERDTC2D                 = FspmUpd->FspmConfig.ERDTC2D;
  MemConfig->WRTC1D                  = FspmUpd->FspmConfig.WRTC1D;
  MemConfig->WRVC1D                  = FspmUpd->FspmConfig.WRVC1D;
  MemConfig->RDTC1D                  = FspmUpd->FspmConfig.RDTC1D;
  MemConfig->DIMMODTT                = FspmUpd->FspmConfig.DIMMODTT;
  MemConfig->DIMMRONT                = FspmUpd->FspmConfig.DIMMRONT;
  MemConfig->WRDSEQT                 = FspmUpd->FspmConfig.WRDSEQT;
  MemConfig->WRSRT                   = FspmUpd->FspmConfig.WRSRT;
  MemConfig->RDODTT                  = FspmUpd->FspmConfig.RDODTT;
  MemConfig->RDEQT                   = FspmUpd->FspmConfig.RDEQT;
  MemConfig->RDAPT                   = FspmUpd->FspmConfig.RDAPT;
  MemConfig->WRTC2D                  = FspmUpd->FspmConfig.WRTC2D;
  MemConfig->RDTC2D                  = FspmUpd->FspmConfig.RDTC2D;
  MemConfig->WRVC2D                  = FspmUpd->FspmConfig.WRVC2D;
  MemConfig->RDVC2D                  = FspmUpd->FspmConfig.RDVC2D;
  MemConfig->CMDVC                   = FspmUpd->FspmConfig.CMDVC;
  MemConfig->LCT                     = FspmUpd->FspmConfig.LCT;
  MemConfig->RTL                     = FspmUpd->FspmConfig.RTL;
  MemConfig->TAT                     = FspmUpd->FspmConfig.TAT;
  MemConfig->RCVENC1D                = FspmUpd->FspmConfig.RCVENC1D;
  MemConfig->RMT                     = FspmUpd->FspmConfig.RMT;
  MemConfig->MEMTST                  = FspmUpd->FspmConfig.MEMTST;
  MemConfig->ALIASCHK                = FspmUpd->FspmConfig.ALIASCHK;
  MemConfig->RMC                     = FspmUpd->FspmConfig.RMC;
  MemConfig->WRDSUDT                 = FspmUpd->FspmConfig.WRDSUDT;
  MemConfig->MarginLimitCheck        = FspmUpd->FspmConfig.MarginLimitCheck;
  MemConfig->MarginLimitL2           = FspmUpd->FspmConfig.MarginLimitL2;
  MemConfig->EccSupport              = FspmUpd->FspmConfig.EccSupport;
  MemConfig->CmdMirror               = FspmUpd->FspmConfig.CmdMirror;
  MemConfig->RemapEnable             = FspmUpd->FspmConfig.RemapEnable;
  MemConfig->ScramblerSupport        = FspmUpd->FspmConfig.ScramblerSupport;
  MemConfig->MrcFastBoot             = FspmUpd->FspmConfig.MrcFastBoot;
  MemConfig->RankInterleave          = FspmUpd->FspmConfig.RankInterleave;
  MemConfig->EnhancedInterleave      = FspmUpd->FspmConfig.EnhancedInterleave;
  MemConfig->ChHashOverride          = FspmUpd->FspmConfig.ChHashOverride;
  MemConfig->ChHashEnable            = FspmUpd->FspmConfig.ChHashEnable;
  MemConfig->EnablePwrDn             = FspmUpd->FspmConfig.EnablePwrDn;
  MemConfig->EnablePwrDnLpddr        = FspmUpd->FspmConfig.EnablePwrDnLpddr;
  MemConfig->SrefCfgEna              = FspmUpd->FspmConfig.SrefCfgEna;
  MemConfig->ThrtCkeMinDefeatLpddr   = FspmUpd->FspmConfig.ThrtCkeMinDefeatLpddr;
  MemConfig->ThrtCkeMinDefeat        = FspmUpd->FspmConfig.ThrtCkeMinDefeat;
  MemConfig->ThrtCkeMinTmrLpddr      = FspmUpd->FspmConfig.ThrtCkeMinTmrLpddr;
  MemConfig->ExitOnFailure           = FspmUpd->FspmConfig.ExitOnFailure;
  MemConfig->NewFeatureEnable1       = FspmUpd->FspmConfig.NewFeatureEnable1;
  MemConfig->NewFeatureEnable2       = FspmUpd->FspmConfig.NewFeatureEnable2;
  MemConfig->Ddr4DdpSharedClock      = FspmUpd->FspmConfig.Ddr4DdpSharedClock;
  MemConfig->SharedZqPin             = FspmUpd->FspmConfig.Ddr4DdpSharedZq;
  MemConfig->BClkFrequency           = FspmUpd->FspmConfig.BClkFrequency;
  MemConfig->ChHashInterleaveBit     = FspmUpd->FspmConfig.ChHashInterleaveBit;
  MemConfig->ChHashMask              = FspmUpd->FspmConfig.ChHashMask;
  MemConfig->ExtendedBankHashing     = FspmUpd->FspmConfig.ExtendedBankHashing;
  MemConfig->Idd3n                   = FspmUpd->FspmConfig.Idd3n;
  MemConfig->Idd3p                   = FspmUpd->FspmConfig.Idd3p;
  MemConfig->DCC                     = FspmUpd->FspmConfig.DCC;
  MemConfig->RDVC1D                  = FspmUpd->FspmConfig.RDVC1D;
  MemConfig->TXTCO                   = FspmUpd->FspmConfig.TXTCO;
  MemConfig->CLKTCO                  = FspmUpd->FspmConfig.CLKTCO;
  MemConfig->CMDSR                   = FspmUpd->FspmConfig.CMDSR;
  MemConfig->CMDDSEQ                 = FspmUpd->FspmConfig.CMDDSEQ;
  MemConfig->TXDQSDCC                = FspmUpd->FspmConfig.TXDQSDCC;
  MemConfig->DIMMODTCA               = FspmUpd->FspmConfig.DIMMODTCA;
  MemConfig->DIMMDFE                 = FspmUpd->FspmConfig.DIMMDFE;
  MemConfig->EARLYDIMMDFE            = FspmUpd->FspmConfig.EARLYDIMMDFE;
  MemConfig->TXTCODQS                = FspmUpd->FspmConfig.TXTCODQS;
  MemConfig->CMDDRUD                 = FspmUpd->FspmConfig.CMDDRUD;
  MemConfig->VCCDLLBP                = FspmUpd->FspmConfig.VCCDLLBP;
  MemConfig->PVTTDNLP                = FspmUpd->FspmConfig.PVTTDNLP;
  MemConfig->RDVREFDC                = FspmUpd->FspmConfig.RDVREFDC;
  MemConfig->VDDQT                   = FspmUpd->FspmConfig.VDDQT;
  MemConfig->RMTBIT                  = FspmUpd->FspmConfig.RMTBIT;
  MemConfig->EccDftEn                = FspmUpd->FspmConfig.EccDftEn;
  MemConfig->Write0                  = FspmUpd->FspmConfig.Write0;
  MemConfig->DebugValue              = FspmUpd->FspmConfig.DebugValue;
  MemConfig->PeriodicDcc             = FspmUpd->FspmConfig.PeriodicDcc;
  MemConfig->LpMode                  = FspmUpd->FspmConfig.LpMode;
  MemConfig->ThrtCkeMinTmr           = FspmUpd->FspmConfig.ThrtCkeMinTmr;
  MemConfig->EnCmdRate               = FspmUpd->FspmConfig.EnCmdRate;
  MemConfig->Refresh2X               = FspmUpd->FspmConfig.Refresh2X;
  MemConfig->EpgEnable               = FspmUpd->FspmConfig.EpgEnable;
  MemConfig->UserThresholdEnable     = FspmUpd->FspmConfig.UserThresholdEnable;
  MemConfig->UserBudgetEnable        = FspmUpd->FspmConfig.UserBudgetEnable;
  MemConfig->PowerDownMode           = FspmUpd->FspmConfig.PowerDownMode;
  MemConfig->PwdwnIdleCounter        = FspmUpd->FspmConfig.PwdwnIdleCounter;
  MemConfig->CmdRanksTerminated      = FspmUpd->FspmConfig.CmdRanksTerminated;
  MemConfig->WriteThreshold          = FspmUpd->FspmConfig.WriteThreshold;
  MemConfig->AllowOppRefBelowWriteThrehold   = FspmUpd->FspmConfig.AllowOppRefBelowWriteThrehold;
  MemConfig->DisPgCloseIdleTimeout   = FspmUpd->FspmConfig.DisPgCloseIdleTimeout;
  MemConfig->BclkRfiFreq[0]          = FspmUpd->FspmConfig.BclkRfiFreq[0];
  MemConfig->BclkRfiFreq[1]          = FspmUpd->FspmConfig.BclkRfiFreq[1];
  MemConfig->BclkRfiFreq[2]          = FspmUpd->FspmConfig.BclkRfiFreq[2];
  MemConfig->BclkRfiFreq[3]          = FspmUpd->FspmConfig.BclkRfiFreq[3];

  MemConfig->RhSelect         = FspmUpd->FspmConfig.RhSelect;
  MemConfig->McRefreshRate    = FspmUpd->FspmConfig.McRefreshRate;
  MemConfig->Lfsr0Mask        = FspmUpd->FspmConfig.Lfsr0Mask;
  MemConfig->Lfsr1Mask        = FspmUpd->FspmConfig.Lfsr1Mask;
  MemConfig->LpddrRttWr       = FspmUpd->FspmConfig.LpddrRttWr;
  MemConfig->LpddrRttCa       = FspmUpd->FspmConfig.LpddrRttCa;
  MemConfig->RefreshWm        = FspmUpd->FspmConfig.RefreshWm;
  MemConfig->Lp5CccConfig     = FspmUpd->FspmConfig.Lp5CccConfig;
  MemConfig->PprEnable        = FspmUpd->FspmConfig.PprEnable;
  MemConfig->FirstDimmBitMask     = FspmUpd->FspmConfig.FirstDimmBitMask;
  MemConfig->FirstDimmBitMaskEcc  = FspmUpd->FspmConfig.FirstDimmBitMaskEcc;
  MemConfig->FreqLimitMixedConfig = FspmUpd->FspmConfig.FreqLimitMixedConfig;
  MemConfig->FreqLimitMixedConfig_1R1R_8GB = FspmUpd->FspmConfig.FreqLimitMixedConfig_1R1R_8GB;
  MemConfig->FreqLimitMixedConfig_1R1R_16GB = FspmUpd->FspmConfig.FreqLimitMixedConfig_1R1R_16GB;
  MemConfig->FreqLimitMixedConfig_1R1R_8GB_16GB = FspmUpd->FspmConfig.FreqLimitMixedConfig_1R1R_8GB_16GB;
  MemConfig->FreqLimitMixedConfig_2R2R = FspmUpd->FspmConfig.FreqLimitMixedConfig_2R2R;
  MemConfig->LctCmdEyeWidth      = FspmUpd->FspmConfig.LctCmdEyeWidth;
  MemConfig->ReuseAdlSDdr5Board   = FspmUpd->FspmConfig.ReuseAdlSDdr5Board;
  MemConfig->Ibecc              = FspmUpd->FspmConfig.Ibecc;
  MemConfig->IbeccOperationMode = FspmUpd->FspmConfig.IbeccOperationMode;
  for (Index = 0; Index < MEM_MAX_IBECC_REGIONS; Index++) {
    MemConfig->IbeccProtectedRangeEnable[Index] = FspmUpd->FspmConfig.IbeccProtectedRangeEnable[Index];
    MemConfig->IbeccProtectedRangeBase[Index]   = FspmUpd->FspmConfig.IbeccProtectedRangeBase[Index];
    MemConfig->IbeccProtectedRangeMask[Index]   = FspmUpd->FspmConfig.IbeccProtectedRangeMask[Index];
  }
  MemConfig->IbeccErrInjControl = FspmUpd->FspmConfig.IbeccErrInjControl;
  MemConfig->IbeccErrInjAddress = FspmUpd->FspmConfig.IbeccErrInjAddress;
  MemConfig->IbeccErrInjMask    = FspmUpd->FspmConfig.IbeccErrInjMask;
  MemConfig->IbeccErrInjCount   = FspmUpd->FspmConfig.IbeccErrInjCount;
  MemConfig->EccErrInjAddress   = FspmUpd->FspmConfig.EccErrInjAddress;
  MemConfig->EccErrInjMask      = FspmUpd->FspmConfig.EccErrInjMask;
  MemConfig->EccErrInjCount     = FspmUpd->FspmConfig.EccErrInjCount;

  MemConfig->SagvSwitchFactorIA         = FspmUpd->FspmConfig.SagvSwitchFactorIA;
  MemConfig->SagvSwitchFactorGT         = FspmUpd->FspmConfig.SagvSwitchFactorGT;
  MemConfig->SagvSwitchFactorIO         = FspmUpd->FspmConfig.SagvSwitchFactorIO;
  MemConfig->SagvSwitchFactorStall      = FspmUpd->FspmConfig.SagvSwitchFactorStall;
  MemConfig->SagvHeuristicsUpControl    = FspmUpd->FspmConfig.SagvHeuristicsUpControl;
  MemConfig->SagvHeuristicsDownControl  = FspmUpd->FspmConfig.SagvHeuristicsDownControl;

  if (Vtd != NULL) {
    Vtd->X2ApicOptOut = FspmUpd->FspmConfig.X2ApicOptOut;
    Vtd->DmaControlGuarantee = FspmUpd->FspmConfig.DmaControlGuarantee;
    for (Index = 0; Index < GetMaxVtdEngineNumber(); Index++) {
      Vtd->BaseAddress[Index] = FspmUpd->FspmConfig.VtdBaseAddress[Index];
    }
    Vtd->VtdDisable    = FspmUpd->FspmConfig.VtdDisable;
    Vtd->VtdIgdEnable  = FspmUpd->FspmConfig.VtdIgdEnable;
    Vtd->VtdIpuEnable  = FspmUpd->FspmConfig.VtdIpuEnable;
    Vtd->VtdIopEnable  = FspmUpd->FspmConfig.VtdIopEnable;
    Vtd->PreBootDmaMask = FspmUpd->FspmConfig.PreBootDmaMask;
    Vtd->DmaBufferSize = FspmUpd->FspmConfig.DmaBufferSize;
  }

  //
  // Update ConfigBlock:GtPreMemConfig
  //
  if (GtPreMemConfig != NULL) {
    GtPreMemConfig->IgdDvmt50PreAlloc = FspmUpd->FspmConfig.IgdDvmt50PreAlloc;
    GtPreMemConfig->InternalGraphics  = FspmUpd->FspmConfig.InternalGfx;
    GtPreMemConfig->ApertureSize      = FspmUpd->FspmConfig.ApertureSize;
    GtPreMemConfig->PrimaryDisplay    = FspmUpd->FspmConfig.PrimaryDisplay;
    GtPreMemConfig->GttMmAdr          = FspmUpd->FspmConfig.GttMmAdr;
    GtPreMemConfig->GmAdr64           = FspmUpd->FspmConfig.GmAdr64;
    GtPreMemConfig->GttSize           = FspmUpd->FspmConfig.GttSize;
    GtPreMemConfig->PsmiRegionSize    = FspmUpd->FspmConfig.PsmiRegionSize;
    GtPreMemConfig->GtClosEnable      = FspmUpd->FspmConfig.GtClosEnable;
    GtPreMemConfig->GtPsmiSupport     = FspmUpd->FspmConfig.GtPsmiSupport;
    GtPreMemConfig->PanelPowerEnable  = FspmUpd->FspmConfig.PanelPowerEnable;
    GtPreMemConfig->DeltaT12PowerCycleDelay = FspmUpd->FspmConfig.DeltaT12PowerCycleDelay;
    GtPreMemConfig->DisplayAudioLink  = FspmUpd->FspmConfig.DisplayAudioLink;
    GtPreMemConfig->OemT12DelayOverride = FspmUpd->FspmConfig.OemT12DelayOverride;

    GtPreMemConfig->DdiConfiguration.DdiPortAConfig = FspmUpd->FspmConfig.DdiPortAConfig;
    GtPreMemConfig->DdiConfiguration.DdiPortBConfig = FspmUpd->FspmConfig.DdiPortBConfig;
    GtPreMemConfig->DdiConfiguration.DdiPortAHpd = FspmUpd->FspmConfig.DdiPortAHpd;
    GtPreMemConfig->DdiConfiguration.DdiPortBHpd = FspmUpd->FspmConfig.DdiPortBHpd;
    GtPreMemConfig->DdiConfiguration.DdiPortCHpd = FspmUpd->FspmConfig.DdiPortCHpd;
    GtPreMemConfig->DdiConfiguration.DdiPort1Hpd = FspmUpd->FspmConfig.DdiPort1Hpd;
    GtPreMemConfig->DdiConfiguration.DdiPort2Hpd = FspmUpd->FspmConfig.DdiPort2Hpd;
    GtPreMemConfig->DdiConfiguration.DdiPort3Hpd = FspmUpd->FspmConfig.DdiPort3Hpd;
    GtPreMemConfig->DdiConfiguration.DdiPort4Hpd = FspmUpd->FspmConfig.DdiPort4Hpd;
    GtPreMemConfig->DdiConfiguration.DdiPortADdc = FspmUpd->FspmConfig.DdiPortADdc;
    GtPreMemConfig->DdiConfiguration.DdiPortBDdc = FspmUpd->FspmConfig.DdiPortBDdc;
    GtPreMemConfig->DdiConfiguration.DdiPortCDdc = FspmUpd->FspmConfig.DdiPortCDdc;
    GtPreMemConfig->DdiConfiguration.DdiPort1Ddc = FspmUpd->FspmConfig.DdiPort1Ddc;
    GtPreMemConfig->DdiConfiguration.DdiPort2Ddc = FspmUpd->FspmConfig.DdiPort2Ddc;
    GtPreMemConfig->DdiConfiguration.DdiPort3Ddc = FspmUpd->FspmConfig.DdiPort3Ddc;
    GtPreMemConfig->DdiConfiguration.DdiPort4Ddc = FspmUpd->FspmConfig.DdiPort4Ddc;
  }

#if FixedPcdGetBool(PcdHgEnable) == 1
  if (HgGpioData != NULL) {
    HgGpioData->RootPortIndex                         = FspmUpd->FspmConfig.RootPortIndex;
    HgGpioData->HgSupport                             = FspmUpd->FspmConfig.HgSupport;
    HgGpioData->CpuPcie0Rtd3Gpio.HoldRst.ExpanderNo   = (UINT8) FspmUpd->FspmConfig.CpuPcie0Rtd3Gpio[0];
    HgGpioData->CpuPcie0Rtd3Gpio.HoldRst.Active       = (UINT8) FspmUpd->FspmConfig.CpuPcie0Rtd3Gpio[1];
    HgGpioData->CpuPcie0Rtd3Gpio.HoldRst.GpioNo       = FspmUpd->FspmConfig.CpuPcie0Rtd3Gpio[4];
    HgGpioData->CpuPcie0Rtd3Gpio.PwrEnable.ExpanderNo = (UINT8) FspmUpd->FspmConfig.CpuPcie0Rtd3Gpio[8];
    HgGpioData->CpuPcie0Rtd3Gpio.PwrEnable.Active     = (UINT8) FspmUpd->FspmConfig.CpuPcie0Rtd3Gpio[9];
    HgGpioData->CpuPcie0Rtd3Gpio.PwrEnable.GpioNo     = FspmUpd->FspmConfig.CpuPcie0Rtd3Gpio[12];
    HgGpioData->CpuPcie0Rtd3Gpio.GpioSupport          = (UINT8) FspmUpd->FspmConfig.CpuPcie0Rtd3Gpio[20];

    HgGpioData->CpuPcie1Rtd3Gpio.HoldRst.ExpanderNo   = (UINT8)FspmUpd->FspmConfig.CpuPcie1Rtd3Gpio[0];
    HgGpioData->CpuPcie1Rtd3Gpio.HoldRst.Active       = (UINT8)FspmUpd->FspmConfig.CpuPcie1Rtd3Gpio[1];
    HgGpioData->CpuPcie1Rtd3Gpio.HoldRst.GpioNo       = FspmUpd->FspmConfig.CpuPcie1Rtd3Gpio[4];
    HgGpioData->CpuPcie1Rtd3Gpio.PwrEnable.ExpanderNo = (UINT8)FspmUpd->FspmConfig.CpuPcie1Rtd3Gpio[8];
    HgGpioData->CpuPcie1Rtd3Gpio.PwrEnable.Active     = (UINT8)FspmUpd->FspmConfig.CpuPcie1Rtd3Gpio[9];
    HgGpioData->CpuPcie1Rtd3Gpio.PwrEnable.GpioNo     = FspmUpd->FspmConfig.CpuPcie1Rtd3Gpio[12];
    HgGpioData->CpuPcie1Rtd3Gpio.GpioSupport          = (UINT8)FspmUpd->FspmConfig.CpuPcie1Rtd3Gpio[20];

    HgGpioData->CpuPcie2Rtd3Gpio.HoldRst.ExpanderNo   = (UINT8)FspmUpd->FspmConfig.CpuPcie2Rtd3Gpio[0];
    HgGpioData->CpuPcie2Rtd3Gpio.HoldRst.Active       = (UINT8)FspmUpd->FspmConfig.CpuPcie2Rtd3Gpio[1];
    HgGpioData->CpuPcie2Rtd3Gpio.HoldRst.GpioNo       = FspmUpd->FspmConfig.CpuPcie2Rtd3Gpio[4];
    HgGpioData->CpuPcie2Rtd3Gpio.PwrEnable.ExpanderNo = (UINT8)FspmUpd->FspmConfig.CpuPcie2Rtd3Gpio[8];
    HgGpioData->CpuPcie2Rtd3Gpio.PwrEnable.Active     = (UINT8)FspmUpd->FspmConfig.CpuPcie2Rtd3Gpio[9];
    HgGpioData->CpuPcie2Rtd3Gpio.PwrEnable.GpioNo     = FspmUpd->FspmConfig.CpuPcie2Rtd3Gpio[12];
    HgGpioData->CpuPcie2Rtd3Gpio.GpioSupport          = (UINT8)FspmUpd->FspmConfig.CpuPcie2Rtd3Gpio[20];

    HgGpioData->CpuPcie3Rtd3Gpio.HoldRst.ExpanderNo   = (UINT8)FspmUpd->FspmConfig.CpuPcie3Rtd3Gpio[0];
    HgGpioData->CpuPcie3Rtd3Gpio.HoldRst.Active       = (UINT8)FspmUpd->FspmConfig.CpuPcie3Rtd3Gpio[1];
    HgGpioData->CpuPcie3Rtd3Gpio.HoldRst.GpioNo       = FspmUpd->FspmConfig.CpuPcie3Rtd3Gpio[4];
    HgGpioData->CpuPcie3Rtd3Gpio.PwrEnable.ExpanderNo = (UINT8)FspmUpd->FspmConfig.CpuPcie3Rtd3Gpio[8];
    HgGpioData->CpuPcie3Rtd3Gpio.PwrEnable.Active     = (UINT8)FspmUpd->FspmConfig.CpuPcie3Rtd3Gpio[9];
    HgGpioData->CpuPcie3Rtd3Gpio.PwrEnable.GpioNo     = FspmUpd->FspmConfig.CpuPcie3Rtd3Gpio[12];
    HgGpioData->CpuPcie3Rtd3Gpio.GpioSupport          = (UINT8)FspmUpd->FspmConfig.CpuPcie3Rtd3Gpio[20];

    HgGpioData->HgDelayAfterPwrEn                     = FspmUpd->FspmConfig.HgDelayAfterPwrEn;
    HgGpioData->HgDelayAfterHoldReset                 = FspmUpd->FspmConfig.HgDelayAfterHoldReset;
    // Initialize the Hybrid Graphics Configuration
    //
    if (GtPreMemConfig != NULL) {
      if (GtPreMemConfig->PrimaryDisplay == 4) {
        ///
        /// In Hybrid Gfx mode PCIe needs to be always enabled and IGFX must be set as Primary Display.
        ///
        HgGpioData->HgMode = 2;
        HgGpioData->HgSubSystemId = 0x2112;
        GtPreMemConfig->PrimaryDisplay = 0;
      } else if (GtPreMemConfig->PrimaryDisplay == 0) {
        ///
        /// In IGFX only mode mode set Hybrid Gfx mode as Disabled
        ///
        HgGpioData->HgMode = 0;
        HgGpioData->HgSubSystemId = 0x2212;
      } else {
        ///
        /// In PEG/PCI/Auto mode set Hybrid Gfx mode as dGPU
        ///
        HgGpioData->HgMode = 3;
        HgGpioData->HgSubSystemId = 0x2212;
      }
    }
  }
#endif

  if (HostBridgePreMemConfig != NULL) {
    HostBridgePreMemConfig->MmioSizeAdjustment = FspmUpd->FspmConfig.MmioSizeAdjustment;
    HostBridgePreMemConfig->MmioSize = FspmUpd->FspmConfig.MmioSize;
    HostBridgePreMemConfig->EnableAbove4GBMmio = FspmUpd->FspmConfig.EnableAbove4GBMmio;
  }

  //
  // Update ConfigBlock: MiscPeiPreMemConfig
  //
  if (MiscPeiPreMemConfig != NULL) {
    ZeroMem((VOID *)MiscPeiPreMemConfig->SpdAddressTable, sizeof(MiscPeiPreMemConfig->SpdAddressTable));
    MiscPeiPreMemConfig->TsegSize = FspmUpd->FspmConfig.TsegSize;
    MiscPeiPreMemConfig->S3DataPtr = FspmUpdDataPtr->FspmArchUpd.NvsBufferPtr;
    MiscPeiPreMemConfig->TxtImplemented = 0; // Currently TXT is not ready for FSP Wrapper. MiscPeiPreMemConfig->TxtImplemented = FspmUpd->TxtImplemented;
    MiscPeiPreMemConfig->CridEnable = FspmUpd->FspmConfig.CridEnable;
    MiscPeiPreMemConfig->SkipExtGfxScan = FspmUpd->FspmConfig.SkipExtGfxScan;
    MiscPeiPreMemConfig->BdatEnable = FspmUpd->FspmConfig.BdatEnable;
    MiscPeiPreMemConfig->BdatTestType = FspmUpd->FspmConfig.BdatTestType;
    MiscPeiPreMemConfig->LockPTMregs = FspmUpd->FspmConfig.LockPTMregs;

    for (Index = 0; Index < MEM_CFG_MAX_SOCKETS; Index++) {
      if (FspmUpd->FspmConfig.SpdAddressTable[Index] != 0) {
        MiscPeiPreMemConfig->SpdAddressTable[Index] = FspmUpd->FspmConfig.SpdAddressTable[Index];
      }
    }
    MiscPeiPreMemConfig->WrcFeatureEnable           = FspmUpd->FspmConfig.WrcFeatureEnable;
  }

#if FixedPcdGetBool(PcdCpuPcieEnable) == 1
    if (PciePeiPreMemConfig != NULL) {
      CpuDmiPreMemConfig->DmiGen3ProgramStaticEq = FspmUpd->FspmConfig.DmiGen3ProgramStaticEq;
      CpuDmiPreMemConfig->DmiAspm = FspmUpd->FspmConfig.DmiAspm;
      CpuDmiPreMemConfig->DmiAspmCtrl = FspmUpd->FspmConfig.DmiAspmCtrl;
      CpuDmiPreMemConfig->DmiAspmL1ExitLatency = FspmUpd->FspmConfig.DmiAspmL1ExitLatency;
      CpuDmiPreMemConfig->Gen3EqPhase23Bypass = FspmUpd->FspmConfig.Gen3EqPhase23Bypass;
      CpuDmiPreMemConfig->Gen3EqPhase3Bypass = FspmUpd->FspmConfig.Gen3EqPhase3Bypass;
      CpuDmiPreMemConfig->Gen3LtcoEnable = FspmUpd->FspmConfig.Gen3LtcoEnable;
      CpuDmiPreMemConfig->Gen3RtcoRtpoEnable = FspmUpd->FspmConfig.Gen3RtcoRtpoEnable;
      CpuDmiPreMemConfig->DmiGen3DsPresetEnable = FspmUpd->FspmConfig.DmiGen3DsPresetEnable;
      CpuDmiPreMemConfig->DmiGen3UsPresetEnable = FspmUpd->FspmConfig.DmiGen3UsPresetEnable;
      CpuDmiPreMemConfig->DmiHweq = FspmUpd->FspmConfig.DmiHweq;
      CpuDmiPreMemConfig->Gen4EqPhase23Bypass = FspmUpd->FspmConfig.Gen4EqPhase23Bypass;
      CpuDmiPreMemConfig->Gen4EqPhase3Bypass = FspmUpd->FspmConfig.Gen4EqPhase3Bypass;
      CpuDmiPreMemConfig->DmiGen4DsPresetEnable = FspmUpd->FspmConfig.DmiGen4DsPresetEnable;
      CpuDmiPreMemConfig->DmiGen4UsPresetEnable = FspmUpd->FspmConfig.DmiGen4UsPresetEnable;
      CpuDmiPreMemConfig->Gen4RtcoRtpoEnable = FspmUpd->FspmConfig.Gen4RtcoRtpoEnable;
      CpuDmiPreMemConfig->Gen4LtcoEnable = FspmUpd->FspmConfig.Gen4LtcoEnable;
      CpuDmiPreMemConfig->DmiNewFom      = FspmUpd->FspmConfig.DmiNewFom;
      CpuDmiPreMemConfig->DmiCdrRelock = FspmUpd->FspmConfig.DmiCdrRelock;
      PciePeiPreMemConfig->InitPcieAspmAfterOprom = FspmUpd->FspmConfig.InitPcieAspmAfterOprom;
      for (Index = 0; Index < SA_DMI_MAX_LANE; Index++) {
        CpuDmiPreMemConfig->DmiGen3RootPortPreset[Index] = FspmUpd->FspmConfig.DmiGen3RootPortPreset[Index];
        CpuDmiPreMemConfig->DmiGen3EndPointPreset[Index] = FspmUpd->FspmConfig.DmiGen3EndPointPreset[Index];
        CpuDmiPreMemConfig->DmiGen3EndPointHint[Index] = FspmUpd->FspmConfig.DmiGen3EndPointHint[Index];
        CpuDmiPreMemConfig->DmiGen3DsPortRxPreset[Index] = FspmUpd->FspmConfig.DmiGen3DsPortRxPreset[Index];
        CpuDmiPreMemConfig->DmiGen3DsPortTxPreset[Index] = FspmUpd->FspmConfig.DmiGen3DsPortTxPreset[Index];
        CpuDmiPreMemConfig->DmiGen3UsPortRxPreset[Index] = FspmUpd->FspmConfig.DmiGen3UsPortRxPreset[Index];
        CpuDmiPreMemConfig->DmiGen3UsPortTxPreset[Index] = FspmUpd->FspmConfig.DmiGen3UsPortTxPreset[Index];
        CpuDmiPreMemConfig->DmiGen3Ltcpre[Index] = FspmUpd->FspmConfig.DmiGen3Ltcpre[Index];
        CpuDmiPreMemConfig->DmiGen3Ltcpo[Index] = FspmUpd->FspmConfig.DmiGen3Ltcpo[Index];
        CpuDmiPreMemConfig->DmiHwEqGen3CoeffList[Index].Cm = FspmUpd->FspmConfig.CpuDmiHwEqGen3CoeffListCm[Index];
        CpuDmiPreMemConfig->DmiHwEqGen3CoeffList[Index].Cp = FspmUpd->FspmConfig.CpuDmiHwEqGen3CoeffListCp[Index];
        CpuDmiPreMemConfig->DmiHwEqGen4CoeffList[Index].Cm = FspmUpd->FspmConfig.CpuDmiHwEqGen4CoeffListCm[Index];
        CpuDmiPreMemConfig->DmiHwEqGen4CoeffList[Index].Cp = FspmUpd->FspmConfig.CpuDmiHwEqGen4CoeffListCp[Index];
        CpuDmiPreMemConfig->DmiGen4DsPortTxPreset[Index] = FspmUpd->FspmConfig.DmiGen4DsPortTxPreset[Index];
        CpuDmiPreMemConfig->DmiGen4UsPortTxPreset[Index] = FspmUpd->FspmConfig.DmiGen4UsPortTxPreset[Index];
        CpuDmiPreMemConfig->DmiGen4Ltcpre[Index] = FspmUpd->FspmConfig.DmiGen4Ltcpre[Index];
        CpuDmiPreMemConfig->DmiGen4Ltcpo[Index] = FspmUpd->FspmConfig.DmiGen4Ltcpo[Index];
      }
      for (Index = 0; Index < SA_DMI_MAX_BUNDLE; Index++) {
        CpuDmiPreMemConfig->DmiGen3RxCtlePeaking[Index] = FspmUpd->FspmConfig.DmiGen3RxCtlePeaking[Index];
      }

    CpuDmiPreMemConfig->DmiDeEmphasis = FspmUpd->FspmConfig.DmiDeEmphasis;
    CpuDmiPreMemConfig->DmiMaxLinkSpeed = FspmUpd->FspmConfig.DmiMaxLinkSpeed;
    CpuDmiPreMemConfig->DmiGen3EqPh2Enable = FspmUpd->FspmConfig.DmiGen3EqPh2Enable;
    CpuDmiPreMemConfig->DmiGen3EqPh3Method = FspmUpd->FspmConfig.DmiGen3EqPh3Method;

  }
#endif
  if (IpuPreMemPolicy != NULL) {
    IpuPreMemPolicy->IpuEnable = FspmUpd->FspmConfig.SaIpuEnable;
    for (Index = 0; Index < GPIO_IMGUCLK_NUMBER_OF_PINS; Index ++) {
      IpuPreMemPolicy->ImguClkOutEn[Index] = FspmUpd->FspmConfig.ImguClkOutEn[Index];
    }
    for (Index = 0; Index < MAX_CSI_PORT; Index ++) {
      IpuPreMemPolicy->LaneUsed[Index] = FspmUpd->FspmConfig.IpuLaneUsed[Index];
      IpuPreMemPolicy->CsiSpeed[Index] = FspmUpd->FspmConfig.CsiSpeed[Index];
    }
  }

  if (CpuTraceHubPreMemConfig != NULL) {
    CpuTraceHubPreMemConfig->TraceHub.EnableMode = FspmUpd->FspmConfig.CpuTraceHubMode;
    CpuTraceHubPreMemConfig->TraceHub.MemReg0Size = FspmUpd->FspmConfig.CpuTraceHubMemReg0Size;
    CpuTraceHubPreMemConfig->TraceHub.MemReg1Size = FspmUpd->FspmConfig.CpuTraceHubMemReg1Size;
  }

  if (TelemetryPreMemConfig != NULL) {
    TelemetryPreMemConfig->CpuCrashLogDevice = FspmUpd->FspmConfig.CpuCrashLogDevice;
  }



  //
  // Initialize the Overclocking Configuration
  //
  if (OcPreMemConfig != NULL) {
    OcPreMemConfig->OcSupport = FspmUpd->FspmConfig.SaOcSupport;
    OcPreMemConfig->GtVoltageMode = FspmUpd->FspmConfig.GtVoltageMode;
    OcPreMemConfig->GtMaxOcRatio = FspmUpd->FspmConfig.GtMaxOcRatio;
    OcPreMemConfig->GtVoltageOffset = FspmUpd->FspmConfig.GtVoltageOffset;
    OcPreMemConfig->GtVoltageOverride = FspmUpd->FspmConfig.GtVoltageOverride;
    OcPreMemConfig->GtExtraTurboVoltage = FspmUpd->FspmConfig.GtExtraTurboVoltage;
    OcPreMemConfig->SaVoltageOffset = FspmUpd->FspmConfig.SaVoltageOffset;
    OcPreMemConfig->RealtimeMemoryTiming = FspmUpd->FspmConfig.RealtimeMemoryTiming;
  }

  //
  // TCSS pre-mem configuration
  //
#if FixedPcdGetBool(PcdITbtEnable) == 1
  if(TcssPeiPreMemConfig != NULL) {
    TcssPeiPreMemConfig->DevEnConfig.TcssDevEnBit.TcssItbtPcie0En = FspmUpd->FspmConfig.TcssItbtPcie0En;
    TcssPeiPreMemConfig->DevEnConfig.TcssDevEnBit.TcssItbtPcie1En = FspmUpd->FspmConfig.TcssItbtPcie1En;
    TcssPeiPreMemConfig->DevEnConfig.TcssDevEnBit.TcssItbtPcie2En = FspmUpd->FspmConfig.TcssItbtPcie2En;
    TcssPeiPreMemConfig->DevEnConfig.TcssDevEnBit.TcssItbtPcie3En = FspmUpd->FspmConfig.TcssItbtPcie3En;
    TcssPeiPreMemConfig->DevEnConfig.TcssDevEnBit.TcssXhciEn      = FspmUpd->FspmConfig.TcssXhciEn;
    TcssPeiPreMemConfig->DevEnConfig.TcssDevEnBit.TcssXdciEn      = FspmUpd->FspmConfig.TcssXdciEn;
    TcssPeiPreMemConfig->DevEnConfig.TcssDevEnBit.TcssDma0En      = FspmUpd->FspmConfig.TcssDma0En;
    TcssPeiPreMemConfig->DevEnConfig.TcssDevEnBit.TcssDma1En      = FspmUpd->FspmConfig.TcssDma1En;
    TcssPeiPreMemConfig->UsbTcConfig.UsbTcPortEn                  = FspmUpd->FspmConfig.UsbTcPortEnPreMem;
  }
#endif

#if FixedPcdGetBool(PcdCpuPcieEnable) == 1
  if (CpuPcieRpPreMemConfig != NULL) {
    CpuPcieRpPreMemConfig->RpEnabledMask                                          = FspmUpd->FspmConfig.CpuPcieRpEnableMask;
    CpuPcieRpPreMemConfig->LinkDownGpios                                          = FspmUpd->FspmConfig.CpuPcieRpLinkDownGpios;
    for (Index = 0; Index < GetMaxCpuPciePortNum (); Index ++) {
      CpuPcieRpPreMemConfig->PcieSpeed[Index]                                     = FspmUpd->FspmConfig.CpuPcieRpPcieSpeed[Index];
      CpuPcieRpPreMemConfig->ClkReqMsgEnableRp[Index]                             = FspmUpd->FspmConfig.CpuPcieRpClockReqMsgEnable[Index];
      CpuPcieRpPreMemConfig->NewFom[Index]                                        = FspmUpd->FspmConfig.CpuPcieNewFom[Index];
      CpuPcieRpPreMemConfig->CdrRelock[Index]                                     = FspmUpd->FspmConfig.CpuPcieRpCdrRelock[Index];
    }
  }
#endif
  return EFI_SUCCESS;
}
