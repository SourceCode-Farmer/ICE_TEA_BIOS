/** @file
  This file is SampleCode of the library for Intel PCH PEI Policy initialization.

 @copyright
  INTEL CONFIDENTIAL
  Copyright 2004 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains an 'Intel Peripheral Driver' and is uniquely identified as
  "Intel Reference Module" and is licensed for Intel CPUs and chipsets under
  the terms of your license agreement with Intel or your vendor. This file may
  be modified by the user, subject to additional terms of the license agreement.

@par Specification Reference:
**/

#include <PiPei.h>
#include <Library/DebugLib.h>
#include <Library/ConfigBlockLib.h>
#include <Ppi/SiPolicy.h>
#include <Library/FspCommonLib.h>
#include <FspsUpd.h>
#include <Library/PchInfoLib.h>
#include <Library/BaseMemoryLib.h>
#include <Library/MemoryAllocationLib.h>
#include <Library/PeiServicesLib.h>
#include <Library/SataSocLib.h>
#include <Library/PchPolicyLib.h>

/**
  Update GBE policies.

  @param[in] SiPolicy  Pointer to SI_POLICY_PPI
  @param[in] FspsUpd   Pointer to FSPS_UPD
**/
STATIC
VOID
FspUpdateGbePolicy (
  IN SI_POLICY_PPI  *SiPolicy,
  IN FSPS_UPD       *FspsUpd
  )
{
  GBE_CONFIG        *GbeConfig;
  EFI_STATUS        Status;

  Status = GetConfigBlock ((VOID *) SiPolicy, &gGbeConfigGuid, (VOID *) &GbeConfig);
  ASSERT_EFI_ERROR (Status);
  if (EFI_ERROR (Status)) {
    return;
  }

  GbeConfig->Enable          = FspsUpd->FspsConfig.PchLanEnable;
  GbeConfig->LtrEnable       = FspsUpd->FspsConfig.PchLanLtrEnable;
}

#if FixedPcdGetBool (PcdEmbeddedEnable)
/**
  Update TSN policies.

  @param[in] SiPolicy  Pointer to SI_POLICY_PPI
  @param[in] FspsUpd   Pointer to FSPS_UPD
**/
STATIC
VOID
FspUpdatePchTsnPolicy (
  IN SI_POLICY_PPI  *SiPolicy,
  IN FSPS_UPD       *FspsUpd
  )
{
  TSN_CONFIG    *TsnConfig;
  EFI_STATUS    Status;

  Status = GetConfigBlock ((VOID *) SiPolicy, &gTsnConfigGuid, (VOID *) &TsnConfig);
  ASSERT_EFI_ERROR (Status);
  if (EFI_ERROR (Status)) {
    return;
  }

  TsnConfig->Enable = FspsUpd->FspsConfig.PchTsnEnable;
  TsnConfig->TsnLinkSpeed = FspsUpd->FspsConfig.PchTsnLinkSpeed;
  TsnConfig->MultiVcEnable = FspsUpd->FspsConfig.PchTsnMultiVcEnable;
  TsnConfig->TsnMacAddressHigh  = FspsUpd->FspsConfig.PchTsnMacAddressHigh;
  TsnConfig->TsnMacAddressLow   = FspsUpd->FspsConfig.PchTsnMacAddressLow;
}
#endif

/**
  Update HD Audio policies.

  @param[in] SiPolicy  Pointer to SI_POLICY_PPI
  @param[in] FspsUpd   Pointer to FSPS_UPD
**/
STATIC
VOID
FspUpdateHdAudioPolicy (
  IN SI_POLICY_PPI  *SiPolicy,
  IN FSPS_UPD       *FspsUpd
  )
{
  HDAUDIO_CONFIG    *HdAudioConfig;
  EFI_STATUS        Status;

  Status = GetConfigBlock ((VOID *) SiPolicy, &gHdAudioConfigGuid, (VOID *) &HdAudioConfig);
  ASSERT_EFI_ERROR (Status);
  if (EFI_ERROR (Status)) {
    return;
  }

  HdAudioConfig->Pme                   = FspsUpd->FspsConfig.PchHdaPme;
  HdAudioConfig->HdAudioLinkFrequency  = FspsUpd->FspsConfig.PchHdaLinkFrequency;
  HdAudioConfig->CodecSxWakeCapability = FspsUpd->FspsConfig.PchHdaCodecSxWakeCapability;
  HdAudioConfig->VerbTableEntryNum     = FspsUpd->FspsConfig.PchHdaVerbTableEntryNum;
  HdAudioConfig->VerbTablePtr          = FspsUpd->FspsConfig.PchHdaVerbTablePtr;
}

/**
  Update Hsio policies.

  @param[in] SiPolicy  Pointer to SI_POLICY_PPI
  @param[in] FspsUpd   Pointer to FSPS_UPD
**/
STATIC
VOID
FspUpdateHsioPolicy (
  IN SI_POLICY_PPI  *SiPolicy,
  IN FSPS_UPD       *FspsUpd
  )
{
  PCH_HSIO_CONFIG    *HsioConfig;
  EFI_STATUS        Status;

  HsioConfig = NULL;
  Status = GetConfigBlock ((VOID *) SiPolicy, &gHsioConfigGuid, (VOID *) &HsioConfig);

  if (EFI_ERROR (Status) || HsioConfig == NULL) {
    return;
  }

  //
  // Update Pch HSIO policies
  //
  HsioConfig->ChipsetInitBinPtr = FspsUpd->FspsConfig.ChipsetInitBinPtr;
  HsioConfig->ChipsetInitBinLen = FspsUpd->FspsConfig.ChipsetInitBinLen;

}

/**
  Update Cnvi policies.

  @param[in] SiPolicy  Pointer to SI_POLICY_PPI
  @param[in] FspsUpd   Pointer to FSPS_UPD
**/
STATIC
VOID
FspUpdateCnviPolicy (
  IN SI_POLICY_PPI  *SiPolicy,
  IN FSPS_UPD       *FspsUpd
  )
{
  CNVI_CONFIG       *CnviConfig;
  EFI_STATUS        Status;

  CnviConfig = NULL;
  Status = GetConfigBlock ((VOID *) SiPolicy, &gCnviConfigGuid, (VOID *) &CnviConfig);

  if (EFI_ERROR (Status) || CnviConfig == NULL) {
    return;
  }

  CnviConfig->Mode           = FspsUpd->FspsConfig.CnviMode;
  CnviConfig->WifiCore       = FspsUpd->FspsConfig.CnviWifiCore;
  CnviConfig->BtCore         = FspsUpd->FspsConfig.CnviBtCore;
  CnviConfig->BtAudioOffload = FspsUpd->FspsConfig.CnviBtAudioOffload;
  CnviConfig->PinMux.RfReset = FspsUpd->FspsConfig.CnviRfResetPinMux;
  CnviConfig->PinMux.Clkreq  = FspsUpd->FspsConfig.CnviClkreqPinMux;

}

/**
  Update Espi policies.

  @param[in] SiPolicy  Pointer to SI_POLICY_PPI
  @param[in] FspsUpd   Pointer to FSPS_UPD
**/
STATIC
VOID
FspUpdateEspiPolicy (
  IN SI_POLICY_PPI  *SiPolicy,
  IN FSPS_UPD       *FspsUpd
  )
{
  PCH_ESPI_CONFIG   *EspiConfig;
  EFI_STATUS        Status;

  Status = GetConfigBlock ((VOID *) SiPolicy, &gEspiConfigGuid, (VOID *) &EspiConfig);
  ASSERT_EFI_ERROR (Status);
  if (EFI_ERROR (Status)) {
    return;
  }

  EspiConfig->LgmrEnable = FspsUpd->FspsConfig.PchEspiLgmrEnable;
  EspiConfig->BmeMasterSlaveEnabled = FspsUpd->FspsConfig.PchEspiBmeMasterSlaveEnabled;
  EspiConfig->HostC10ReportEnable = FspsUpd->FspsConfig.PchEspiHostC10ReportEnable;
  EspiConfig->LockLinkConfiguration = FspsUpd->FspsConfig.PchEspiLockLinkConfiguration;
}

#if FixedPcdGetBool(PcdAdlLpSupport) == 1

/**
  Update UFS policies.

  @param[in] SiPolicy  Pointer to SI_POLICY_PPI
  @param[in] FspsUpd   Pointer to FSPS_UPD
**/
STATIC
VOID
FspUpdateUfsPolicy (
  IN SI_POLICY_PPI  *SiPolicy,
  IN FSPS_UPD       *FspsUpd
  )
{
  SCS_UFS_CONFIG  *UfsConfig;
  UINT8           UfsIndex;
  EFI_STATUS      Status;

  Status = GetConfigBlock ((VOID *) SiPolicy, &gUfsConfigGuid, (VOID *) &UfsConfig);
  ASSERT_EFI_ERROR (Status);
  if (EFI_ERROR (Status)) {
    return;
  }

  //
  // FSPS UPD has hard allocated configuration space for only 2 UFS controllers. Be sure to change this loop termination
  // condition after adding more space in FSPS UPD.
  //
  for (UfsIndex = 0; (UfsIndex < PchGetMaxUfsNum ()) && (UfsIndex < 2); UfsIndex++) {
    UfsConfig->UfsControllerConfig[UfsIndex].Enable = FspsUpd->FspsConfig.UfsEnable[UfsIndex];
  }
}
#endif

/**
  Update Ish policies.

  @param[in] SiPolicy  Pointer to SI_POLICY_PPI
  @param[in] FspsUpd   Pointer to FSPS_UPD
**/
STATIC
VOID
FspUpdateIshPolicy (
  IN SI_POLICY_PPI  *SiPolicy,
  IN FSPS_UPD       *FspsUpd
  )
{
  UINTN             Index;
  UINTN             CsIndex;
  ISH_CONFIG        *IshConfig;
  EFI_STATUS        Status;

  Status = GetConfigBlock ((VOID *) SiPolicy, &gIshConfigGuid, (VOID *) &IshConfig);
  ASSERT_EFI_ERROR (Status);
  if (EFI_ERROR (Status)) {
    return;
  }

  for (Index = 0; Index < GetPchMaxIshSpiControllersNum (); Index++) {
    IshConfig->Spi[Index].Enable                 = FspsUpd->FspsConfig.PchIshSpiEnable[Index];
    IshConfig->Spi[Index].PinConfig.Mosi.PinMux  = FspsUpd->FspsConfig.IshSpiMosiPinMuxing[Index];
    IshConfig->Spi[Index].PinConfig.Miso.PinMux  = FspsUpd->FspsConfig.IshSpiMisoPinMuxing[Index];
    IshConfig->Spi[Index].PinConfig.Clk.PinMux   = FspsUpd->FspsConfig.IshSpiClkPinMuxing[Index];

    IshConfig->Spi[Index].PinConfig.Mosi.PadTermination = FspsUpd->FspsConfig.IshSpiMosiPadTermination[Index];
    IshConfig->Spi[Index].PinConfig.Miso.PadTermination = FspsUpd->FspsConfig.IshSpiMisoPadTermination[Index];
    IshConfig->Spi[Index].PinConfig.Clk.PadTermination  = FspsUpd->FspsConfig.IshSpiClkPadTermination[Index];
    for (CsIndex = 0; CsIndex < PCH_MAX_ISH_SPI_CS_PINS; CsIndex++) {
      IshConfig->Spi[Index].CsEnable[CsIndex]            = FspsUpd->FspsConfig.PchIshSpiCsEnable[PCH_MAX_ISH_SPI_CS_PINS * Index + CsIndex];
      IshConfig->Spi[Index].PinConfig.Cs[CsIndex].PinMux = FspsUpd->FspsConfig.IshSpiCsPinMuxing[PCH_MAX_ISH_SPI_CS_PINS * Index + CsIndex];
      IshConfig->Spi[Index].PinConfig.Cs[CsIndex].PadTermination = FspsUpd->FspsConfig.IshSpiCsPadTermination[PCH_MAX_ISH_SPI_CS_PINS * Index + CsIndex];
    }
  }
  for (Index = 0; Index < GetPchMaxIshUartControllersNum (); Index++) {
    IshConfig->Uart[Index].Enable               = FspsUpd->FspsConfig.PchIshUartEnable[Index];
    IshConfig->Uart[Index].PinConfig.Rx.PinMux  = FspsUpd->FspsConfig.IshUartRxPinMuxing[Index];
    IshConfig->Uart[Index].PinConfig.Tx.PinMux  = FspsUpd->FspsConfig.IshUartTxPinMuxing[Index];
    IshConfig->Uart[Index].PinConfig.Rts.PinMux = FspsUpd->FspsConfig.IshUartRtsPinMuxing[Index];
    IshConfig->Uart[Index].PinConfig.Cts.PinMux = FspsUpd->FspsConfig.IshUartCtsPinMuxing[Index];

    IshConfig->Uart[Index].PinConfig.Rx.PadTermination  = FspsUpd->FspsConfig.IshUartRxPadTermination[Index];
    IshConfig->Uart[Index].PinConfig.Tx.PadTermination  = FspsUpd->FspsConfig.IshUartTxPadTermination[Index];
    IshConfig->Uart[Index].PinConfig.Rts.PadTermination = FspsUpd->FspsConfig.IshUartRtsPadTermination[Index];
    IshConfig->Uart[Index].PinConfig.Cts.PadTermination = FspsUpd->FspsConfig.IshUartCtsPadTermination[Index];
  }
  for (Index = 0; Index < GetPchMaxIshI2cControllersNum (); Index++) {
    IshConfig->I2c[Index].Enable               = FspsUpd->FspsConfig.PchIshI2cEnable[Index];
    IshConfig->I2c[Index].PinConfig.Sda.PinMux = FspsUpd->FspsConfig.IshI2cSdaPinMuxing[Index];
    IshConfig->I2c[Index].PinConfig.Scl.PinMux = FspsUpd->FspsConfig.IshI2cSclPinMuxing[Index];

    IshConfig->I2c[Index].PinConfig.Sda.PadTermination = FspsUpd->FspsConfig.IshI2cSdaPadTermination[Index];
    IshConfig->I2c[Index].PinConfig.Scl.PadTermination = FspsUpd->FspsConfig.IshI2cSclPadTermination[Index];
  }
  for (Index = 0; Index < GetPchMaxIshGpNum (); Index++) {
    IshConfig->Gp[Index].Enable           = FspsUpd->FspsConfig.PchIshGpEnable[Index];
    IshConfig->Gp[Index].PinConfig.PinMux = FspsUpd->FspsConfig.IshGpGpioPinMuxing[Index];
    IshConfig->Gp[Index].PinConfig.PadTermination = FspsUpd->FspsConfig.IshGpGpioPadTermination[Index];
  }

  IshConfig->PdtUnlock = FspsUpd->FspsConfig.PchIshPdtUnlock;

}

/**
  Update SATA Controllers policies.

  @param[in] SiPolicy  Pointer to SI_POLICY_PPI
  @param[in] FspsUpd   Pointer to FSPS_UPD
**/
STATIC
VOID
FspUpdateSataPolicy (
  IN SI_POLICY_PPI  *SiPolicy,
  IN FSPS_UPD       *FspsUpd
  )
{
  UINTN             Index;
  UINTN             SataCtrlIndex;
  SATA_CONFIG       *SataConfig;

  for (SataCtrlIndex = 0; SataCtrlIndex < MaxSataControllerNum (); SataCtrlIndex++) {
    SataConfig                  = GetPchSataConfig (SiPolicy, SataCtrlIndex);

    SataConfig->Enable          = FspsUpd->FspsConfig.SataEnable;
    SataConfig->SalpSupport     = FspsUpd->FspsConfig.SataSalpSupport;
    SataConfig->TestMode        = FspsUpd->FspsConfig.SataTestMode;
    SataConfig->PwrOptEnable    = FspsUpd->FspsConfig.SataPwrOptEnable;
    SataConfig->EsataSpeedLimit = FspsUpd->FspsConfig.EsataSpeedLimit;
    SataConfig->LedEnable       = FspsUpd->FspsConfig.SataLedEnable;
    SataConfig->SataMode        = FspsUpd->FspsConfig.SataMode;
    SataConfig->SpeedLimit      = FspsUpd->FspsConfig.SataSpeedLimit;

    for (Index = 0; Index < MaxSataPortNum (SataCtrlIndex); Index++) {
      SataConfig->PortSettings[Index].Enable            = FspsUpd->FspsConfig.SataPortsEnable[Index];
      SataConfig->PortSettings[Index].HotPlug           = FspsUpd->FspsConfig.SataPortsHotPlug[Index];
      SataConfig->PortSettings[Index].InterlockSw       = FspsUpd->FspsConfig.SataPortsInterlockSw[Index];
      SataConfig->PortSettings[Index].External          = FspsUpd->FspsConfig.SataPortsExternal[Index];
      SataConfig->PortSettings[Index].SpinUp            = FspsUpd->FspsConfig.SataPortsSpinUp[Index];
      SataConfig->PortSettings[Index].SolidStateDrive   = FspsUpd->FspsConfig.SataPortsSolidStateDrive[Index];
      SataConfig->PortSettings[Index].DevSlp            = FspsUpd->FspsConfig.SataPortsDevSlp[Index];
      SataConfig->PortSettings[Index].DevSlpResetConfig = FspsUpd->FspsConfig.SataPortsDevSlpResetConfig[Index];
      SataConfig->PortSettings[Index].EnableDitoConfig  = FspsUpd->FspsConfig.SataPortsEnableDitoConfig[Index];
      SataConfig->PortSettings[Index].DmVal             = FspsUpd->FspsConfig.SataPortsDmVal[Index];
      SataConfig->PortSettings[Index].DitoVal           = FspsUpd->FspsConfig.SataPortsDitoVal[Index];
      SataConfig->PortSettings[Index].ZpOdd             = FspsUpd->FspsConfig.SataPortsZpOdd[Index];
      //
      //  SATA DEVSLP GPIO Configuration
      //
      SataConfig->PortSettings[Index].DevSlpGpioPinMux  = FspsUpd->FspsConfig.SataPortDevSlpPinMux[Index];
    }
    SataConfig->RaidDeviceId       = FspsUpd->FspsConfig.SataRstRaidDeviceId;
    SataConfig->SataRstInterrupt   = FspsUpd->FspsConfig.SataRstInterrupt;

    SataConfig->ThermalThrottling.P0T1M            = FspsUpd->FspsConfig.SataP0T1M;
    SataConfig->ThermalThrottling.P0T2M            = FspsUpd->FspsConfig.SataP0T2M;
    SataConfig->ThermalThrottling.P0T3M            = FspsUpd->FspsConfig.SataP0T3M;
    SataConfig->ThermalThrottling.P0TDisp          = FspsUpd->FspsConfig.SataP0TDisp;
    SataConfig->ThermalThrottling.P1T1M            = FspsUpd->FspsConfig.SataP1T1M;
    SataConfig->ThermalThrottling.P1T2M            = FspsUpd->FspsConfig.SataP1T2M;
    SataConfig->ThermalThrottling.P1T3M            = FspsUpd->FspsConfig.SataP1T3M;
    SataConfig->ThermalThrottling.P1TDisp          = FspsUpd->FspsConfig.SataP1TDisp;
    SataConfig->ThermalThrottling.P0Tinact         = FspsUpd->FspsConfig.SataP0Tinact;
    SataConfig->ThermalThrottling.P0TDispFinit     = FspsUpd->FspsConfig.SataP0TDispFinit;
    SataConfig->ThermalThrottling.P1Tinact         = FspsUpd->FspsConfig.SataP1Tinact;
    SataConfig->ThermalThrottling.P1TDispFinit     = FspsUpd->FspsConfig.SataP1TDispFinit;
    SataConfig->ThermalThrottling.SuggestedSetting = FspsUpd->FspsConfig.SataThermalSuggestedSetting;

  }
}

/**
  Update RST policies.

  @param[in] SiPolicy  Pointer to SI_POLICY_PPI
  @param[in] FspsUpd   Pointer to FSPS_UPD
**/
STATIC
VOID
FspUpdateRstPolicy (
  IN SI_POLICY_PPI  *SiPolicy,
  IN FSPS_UPD       *FspsUpd
  )
{
  UINTN             Index;
  RST_CONFIG        *RstConfig;
  EFI_STATUS        Status;

  Status = GetConfigBlock ((VOID *) SiPolicy, &gRstConfigGuid, (VOID *) &RstConfig);
  ASSERT_EFI_ERROR (Status);
  if (EFI_ERROR (Status)) {
    return;
  }

  for (Index = 0; Index < PCH_MAX_RST_PCIE_STORAGE_CR; Index++) {
    RstConfig->HardwareRemappedStorageConfig[Index].Enable             = FspsUpd->FspsConfig.SataRstPcieEnable[Index];
    RstConfig->HardwareRemappedStorageConfig[Index].RstPcieStoragePort = FspsUpd->FspsConfig.SataRstPcieStoragePort[Index];
    RstConfig->HardwareRemappedStorageConfig[Index].DeviceResetDelay   = FspsUpd->FspsConfig.SataRstPcieDeviceResetDelay[Index];
  }
}

/**
  Update Usb policies.

  @param[in] SiPolicy  Pointer to SI_POLICY_PPI
  @param[in] FspsUpd   Pointer to FSPS_UPD
**/
STATIC
VOID
FspUpdateUsbPolicy (
  IN SI_POLICY_PPI  *SiPolicy,
  IN FSPS_UPD       *FspsUpd
  )
{
  UINTN             Index;
  USB_CONFIG        *UsbConfig;
  EFI_STATUS        Status;

  Status = GetConfigBlock ((VOID *) SiPolicy, &gUsbConfigGuid, (VOID *) &UsbConfig);
  ASSERT_EFI_ERROR (Status);
  if (EFI_ERROR (Status)) {
    return;
  }

  UsbConfig->PdoProgramming         = FspsUpd->FspsConfig.UsbPdoProgramming;
  UsbConfig->OverCurrentEnable      = FspsUpd->FspsConfig.PchUsbOverCurrentEnable;
  UsbConfig->UaolEnable             = FspsUpd->FspsConfig.PchXhciUaolEnable;
  UsbConfig->HsiiEnable             = FspsUpd->FspsConfig.PchXhciHsiiEnable;
  if (FspsUpd->FspsConfig.PchEnableDbcObs) {
    UsbConfig->OverCurrentEnable    = FALSE;
  }
  UsbConfig->XhciOcLock             = FspsUpd->FspsConfig.PchXhciOcLock;
  for (Index = 0; Index < GetPchXhciMaxUsb2PortNum (); Index++) {
    UsbConfig->PortUsb20[Index].Enable                 = FspsUpd->FspsConfig.PortUsb20Enable[Index];
    UsbConfig->PortUsb20[Index].OverCurrentPin         = FspsUpd->FspsConfig.Usb2OverCurrentPin[Index];
    UsbConfig->PortUsb20[Index].PortResetMessageEnable = FspsUpd->FspsConfig.PortResetMessageEnable[Index];
  }
  for (Index = 0; Index < GetPchXhciMaxUsb3PortNum (); Index++) {
    UsbConfig->PortUsb30[Index].Enable         = FspsUpd->FspsConfig.PortUsb30Enable[Index];
    UsbConfig->PortUsb30[Index].OverCurrentPin = FspsUpd->FspsConfig.Usb3OverCurrentPin[Index];
  }

  UsbConfig->LtrOverrideEnable           = FspsUpd->FspsConfig.PchUsbLtrOverrideEnable;
  if (UsbConfig->LtrOverrideEnable) {
    UsbConfig->LtrHighIdleTimeOverride   = FspsUpd->FspsConfig.PchUsbLtrHighIdleTimeOverride;
    UsbConfig->LtrMediumIdleTimeOverride = FspsUpd->FspsConfig.PchUsbLtrMediumIdleTimeOverride;
    UsbConfig->LtrLowIdleTimeOverride    = FspsUpd->FspsConfig.PchUsbLtrLowIdleTimeOverride;
  }

  UsbConfig->XdciConfig.Enable  = FspsUpd->FspsConfig.XdciEnable;

}

/**
  Update Usb2Phy policies.

  @param[in] SiPolicy  Pointer to SI_POLICY_PPI
  @param[in] FspsUpd   Pointer to FSPS_UPD
**/
STATIC
VOID
FspUpdateUsb2PhyPolicy (
  IN SI_POLICY_PPI  *SiPolicy,
  IN FSPS_UPD       *FspsUpd
  )
{
  UINTN             Index;
  USB2_PHY_CONFIG   *Usb2PhyConfig;
  EFI_STATUS        Status;

  Status = GetConfigBlock ((VOID *) SiPolicy, &gUsb2PhyConfigGuid, (VOID *) &Usb2PhyConfig);
  ASSERT_EFI_ERROR (Status);
  if (EFI_ERROR (Status)) {
    return;
  }

  for (Index = 0; Index < GetPchUsb2MaxPhysicalPortNum (); Index++) {
    Usb2PhyConfig->Port[Index].Petxiset   = FspsUpd->FspsConfig.Usb2PhyPetxiset[Index];
    Usb2PhyConfig->Port[Index].Txiset     = FspsUpd->FspsConfig.Usb2PhyTxiset[Index];
    Usb2PhyConfig->Port[Index].Predeemp   = FspsUpd->FspsConfig.Usb2PhyPredeemp[Index];
    Usb2PhyConfig->Port[Index].Pehalfbit  = FspsUpd->FspsConfig.Usb2PhyPehalfbit[Index];
  }
}

/**
  Update Usb3Hsio policies.

  @param[in] SiPolicy  Pointer to SI_POLICY_PPI
  @param[in] FspsUpd   Pointer to FSPS_UPD
**/
STATIC
VOID
FspUpdateUsb3HsioPolicy (
  IN SI_POLICY_PPI  *SiPolicy,
  IN FSPS_UPD       *FspsUpd
  )
{
  UINTN             Index;
  USB3_HSIO_CONFIG  *Usb3HsioConfig;
  EFI_STATUS        Status;

  Status = GetConfigBlock ((VOID *) SiPolicy, &gUsb3HsioConfigGuid, (VOID *) &Usb3HsioConfig);
  ASSERT_EFI_ERROR (Status);
  if (EFI_ERROR (Status)) {
    return;
  }

  for (Index = 0; Index < GetPchXhciMaxUsb3PortNum (); Index++) {
    Usb3HsioConfig->Port[Index].HsioTxDeEmphEnable             = FspsUpd->FspsConfig.Usb3HsioTxDeEmphEnable[Index];
    Usb3HsioConfig->Port[Index].HsioTxDeEmph                   = FspsUpd->FspsConfig.Usb3HsioTxDeEmph[Index];

    Usb3HsioConfig->Port[Index].HsioTxDownscaleAmpEnable       = FspsUpd->FspsConfig.Usb3HsioTxDownscaleAmpEnable[Index];
    Usb3HsioConfig->Port[Index].HsioTxDownscaleAmp             = FspsUpd->FspsConfig.Usb3HsioTxDownscaleAmp[Index];

    Usb3HsioConfig->Port[Index].HsioCtrlAdaptOffsetCfgEnable   = FspsUpd->FspsConfig.PchUsb3HsioCtrlAdaptOffsetCfgEnable[Index];
    Usb3HsioConfig->Port[Index].HsioCtrlAdaptOffsetCfg         = FspsUpd->FspsConfig.PchUsb3HsioCtrlAdaptOffsetCfg[Index];

    Usb3HsioConfig->Port[Index].HsioFilterSelPEnable           = FspsUpd->FspsConfig.PchUsb3HsioFilterSelPEnable[Index];
    Usb3HsioConfig->Port[Index].HsioFilterSelP                 = FspsUpd->FspsConfig.PchUsb3HsioFilterSelP[Index];

    Usb3HsioConfig->Port[Index].HsioFilterSelNEnable           = FspsUpd->FspsConfig.PchUsb3HsioFilterSelNEnable[Index];
    Usb3HsioConfig->Port[Index].HsioFilterSelN                 = FspsUpd->FspsConfig.PchUsb3HsioFilterSelN[Index];

    Usb3HsioConfig->Port[Index].HsioOlfpsCfgPullUpDwnResEnable = FspsUpd->FspsConfig.PchUsb3HsioOlfpsCfgPullUpDwnResEnable[Index];
    Usb3HsioConfig->Port[Index].HsioOlfpsCfgPullUpDwnRes       = FspsUpd->FspsConfig.PchUsb3HsioOlfpsCfgPullUpDwnRes[Index];
    Usb3HsioConfig->Port[Index].HsioTxRate0UniqTranEnable      = FspsUpd->FspsConfig.Usb3HsioTxRate0UniqTranEnable[Index];
    Usb3HsioConfig->Port[Index].HsioTxRate0UniqTran            = FspsUpd->FspsConfig.Usb3HsioTxRate0UniqTran[Index];
    Usb3HsioConfig->Port[Index].HsioTxRate1UniqTranEnable      = FspsUpd->FspsConfig.Usb3HsioTxRate1UniqTranEnable[Index];
    Usb3HsioConfig->Port[Index].HsioTxRate1UniqTran            = FspsUpd->FspsConfig.Usb3HsioTxRate1UniqTran[Index];
    Usb3HsioConfig->Port[Index].HsioTxRate2UniqTranEnable      = FspsUpd->FspsConfig.Usb3HsioTxRate2UniqTranEnable[Index];
    Usb3HsioConfig->Port[Index].HsioTxRate2UniqTran            = FspsUpd->FspsConfig.Usb3HsioTxRate2UniqTran[Index];
    Usb3HsioConfig->Port[Index].HsioTxRate3UniqTranEnable      = FspsUpd->FspsConfig.Usb3HsioTxRate3UniqTranEnable[Index];
    Usb3HsioConfig->Port[Index].HsioTxRate3UniqTran            = FspsUpd->FspsConfig.Usb3HsioTxRate3UniqTran[Index];
  }
}

/**
  Update PcieRp policies.

  @param[in] SiPolicy  Pointer to SI_POLICY_PPI
  @param[in] FspsUpd   Pointer to FSPS_UPD
**/
STATIC
VOID
FspUpdatePcieRpPolicy (
  IN SI_POLICY_PPI  *SiPolicy,
  IN FSPS_UPD       *FspsUpd
  )
{
  UINTN             Index;
  UINTN             MaxPcieRootPorts;
  PCH_PCIE_CONFIG   *PchPcieConfig;
  EFI_STATUS        Status;
  UINTN             RpIndex;

  Status = GetConfigBlock ((VOID *) SiPolicy, &gPchPcieConfigGuid, (VOID *) &PchPcieConfig);
  ASSERT_EFI_ERROR (Status);
  if (EFI_ERROR (Status)) {
    return;
  }

  MaxPcieRootPorts = GetPchMaxPciePortNum ();
  for (Index = 0; Index < MaxPcieRootPorts; Index ++) {
    PchPcieConfig->RootPort[Index].PcieRpCommonConfig.HotPlug                  = FspsUpd->FspsConfig.PcieRpHotPlug[Index];
    PchPcieConfig->RootPort[Index].PcieRpCommonConfig.PmSci                    = FspsUpd->FspsConfig.PcieRpPmSci[Index];
    PchPcieConfig->RootPort[Index].PcieRpCommonConfig.TransmitterHalfSwing     = FspsUpd->FspsConfig.PcieRpTransmitterHalfSwing[Index];
    PchPcieConfig->RootPort[Index].PcieRpCommonConfig.ClkReqDetect             = FspsUpd->FspsConfig.PcieRpClkReqDetect[Index];
    PchPcieConfig->RootPort[Index].PcieRpCommonConfig.AdvancedErrorReporting   = FspsUpd->FspsConfig.PcieRpAdvancedErrorReporting[Index];
    PchPcieConfig->RootPort[Index].PcieRpCommonConfig.UnsupportedRequestReport = FspsUpd->FspsConfig.PcieRpUnsupportedRequestReport[Index];
    PchPcieConfig->RootPort[Index].PcieRpCommonConfig.FatalErrorReport         = FspsUpd->FspsConfig.PcieRpFatalErrorReport[Index];
    PchPcieConfig->RootPort[Index].PcieRpCommonConfig.NoFatalErrorReport       = FspsUpd->FspsConfig.PcieRpNoFatalErrorReport[Index];
    PchPcieConfig->RootPort[Index].PcieRpCommonConfig.CorrectableErrorReport   = FspsUpd->FspsConfig.PcieRpCorrectableErrorReport[Index];
    PchPcieConfig->RootPort[Index].PcieRpCommonConfig.SystemErrorOnFatalError  = FspsUpd->FspsConfig.PcieRpSystemErrorOnFatalError[Index];
    PchPcieConfig->RootPort[Index].PcieRpCommonConfig.SystemErrorOnNonFatalError    = FspsUpd->FspsConfig.PcieRpSystemErrorOnNonFatalError[Index];
    PchPcieConfig->RootPort[Index].PcieRpCommonConfig.SystemErrorOnCorrectableError = FspsUpd->FspsConfig.PcieRpSystemErrorOnCorrectableError[Index];
    PchPcieConfig->RootPort[Index].PcieRpCommonConfig.MaxPayload               = FspsUpd->FspsConfig.PcieRpMaxPayload[Index];
    PchPcieConfig->RootPort[Index].PcieRpCommonConfig.PcieSpeed                = FspsUpd->FspsConfig.PcieRpPcieSpeed[Index];
    PchPcieConfig->RootPort[Index].PcieRpCommonConfig.PhysicalSlotNumber       = FspsUpd->FspsConfig.PcieRpPhysicalSlotNumber[Index];
    PchPcieConfig->RootPort[Index].PcieRpCommonConfig.SlotImplemented          = FspsUpd->FspsConfig.PcieRpSlotImplemented[Index];
    PchPcieConfig->RootPort[Index].PcieRpCommonConfig.CompletionTimeout        = FspsUpd->FspsConfig.PcieRpCompletionTimeout[Index];
    PchPcieConfig->RootPort[Index].PcieRpCommonConfig.Aspm                     = FspsUpd->FspsConfig.PcieRpAspm[Index];
    PchPcieConfig->RootPort[Index].PcieRpCommonConfig.L1Substates              = FspsUpd->FspsConfig.PcieRpL1Substates[Index];
    PchPcieConfig->RootPort[Index].PcieRpCommonConfig.L1Low                    = FspsUpd->FspsConfig.PcieRpL1Low[Index];
    PchPcieConfig->RootPort[Index].PcieRpCommonConfig.LtrEnable                = FspsUpd->FspsConfig.PcieRpLtrEnable[Index];
    PchPcieConfig->RootPort[Index].PcieRpCommonConfig.PcieRpLtrConfig.LtrConfigLock  = FspsUpd->FspsConfig.PcieRpLtrConfigLock[Index];
    PchPcieConfig->RootPort[Index].PcieRpCommonConfig.AcsEnabled                       = FspsUpd->FspsConfig.PcieRpAcsEnabled[Index];
    PchPcieConfig->RootPort[Index].PcieRpCommonConfig.EnableCpm     = FspsUpd->FspsConfig.PcieRpEnableCpm[Index];
    PchPcieConfig->RootPort[Index].PcieRpCommonConfig.DetectTimeoutMs                  = FspsUpd->FspsConfig.PcieRpDetectTimeoutMs[Index];
    PchPcieConfig->RootPort[Index].PcieRpCommonConfig.PtmEnabled = FspsUpd->FspsConfig.PciePtm[Index];
    PchPcieConfig->RootPort[Index].PcieRpCommonConfig.DpcEnabled = FspsUpd->FspsConfig.PcieDpc[Index];
    PchPcieConfig->RootPort[Index].PcieRpCommonConfig.RpDpcExtensionsEnabled = FspsUpd->FspsConfig.PcieEdpc[Index];

    PchPcieConfig->RootPort[Index].PcieRpCommonConfig.PcieRpLtrConfig.LtrMaxSnoopLatency       = FspsUpd->FspsConfig.PcieRpLtrMaxSnoopLatency[Index];
    PchPcieConfig->RootPort[Index].PcieRpCommonConfig.PcieRpLtrConfig.LtrMaxNoSnoopLatency     = FspsUpd->FspsConfig.PcieRpLtrMaxNoSnoopLatency[Index];

    PchPcieConfig->RootPort[Index].PcieRpCommonConfig.PcieRpLtrConfig.SnoopLatencyOverrideMode       = FspsUpd->FspsConfig.PcieRpSnoopLatencyOverrideMode[Index];
    PchPcieConfig->RootPort[Index].PcieRpCommonConfig.PcieRpLtrConfig.SnoopLatencyOverrideMultiplier = FspsUpd->FspsConfig.PcieRpSnoopLatencyOverrideMultiplier[Index];
    PchPcieConfig->RootPort[Index].PcieRpCommonConfig.PcieRpLtrConfig.SnoopLatencyOverrideValue      = FspsUpd->FspsConfig.PcieRpSnoopLatencyOverrideValue[Index];

    PchPcieConfig->RootPort[Index].PcieRpCommonConfig.PcieRpLtrConfig.NonSnoopLatencyOverrideMode       = FspsUpd->FspsConfig.PcieRpNonSnoopLatencyOverrideMode[Index];
    PchPcieConfig->RootPort[Index].PcieRpCommonConfig.PcieRpLtrConfig.NonSnoopLatencyOverrideMultiplier = FspsUpd->FspsConfig.PcieRpNonSnoopLatencyOverrideMultiplier[Index];
    PchPcieConfig->RootPort[Index].PcieRpCommonConfig.PcieRpLtrConfig.NonSnoopLatencyOverrideValue      = FspsUpd->FspsConfig.PcieRpNonSnoopLatencyOverrideValue[Index];

    PchPcieConfig->RootPort[Index].PcieRpCommonConfig.SlotPowerLimitScale      = FspsUpd->FspsConfig.PcieRpSlotPowerLimitScale[Index];
    PchPcieConfig->RootPort[Index].PcieRpCommonConfig.SlotPowerLimitValue      = FspsUpd->FspsConfig.PcieRpSlotPowerLimitValue[Index];

    PchPcieConfig->RootPort[Index].PcieRpCommonConfig.EnablePeerMemoryWrite    = FspsUpd->FspsConfig.PcieEnablePeerMemoryWrite[Index];
  }

  //
  // Update PCIE RP policies
  //
  PchPcieConfig->PcieCommonConfig.EnablePort8xhDecode           = FspsUpd->FspsConfig.PcieEnablePort8xhDecode;
  PchPcieConfig->PchPciePort8xhDecodePortIndex                  = FspsUpd->FspsConfig.PchPciePort8xhDecodePortIndex;
  PchPcieConfig->PcieCommonConfig.ComplianceTestMode            = FspsUpd->FspsConfig.PcieComplianceTestMode;
  PchPcieConfig->PcieCommonConfig.RpFunctionSwap                = FspsUpd->FspsConfig.PcieRpFunctionSwap;

  //
  // PCIe EQ settings
  //
  for (RpIndex = 0; RpIndex < MaxPcieRootPorts; RpIndex++) {
    PchPcieConfig->RootPort[RpIndex].PcieRpCommonConfig.OverrideEqualizationDefaults                                    = FspsUpd->FspsConfig.PcieEqOverrideDefault;
    PchPcieConfig->RootPort[RpIndex].PcieRpCommonConfig.PcieGen3LinkEqPlatformSettings.PcieLinkEqMethod                 = FspsUpd->FspsConfig.PcieEqMethod;
    PchPcieConfig->RootPort[RpIndex].PcieRpCommonConfig.PcieGen3LinkEqPlatformSettings.PcieLinkEqMode                   = FspsUpd->FspsConfig.PcieEqMode;
    PchPcieConfig->RootPort[RpIndex].PcieRpCommonConfig.PcieGen3LinkEqPlatformSettings.LocalTransmitterOverrideEnable   = FspsUpd->FspsConfig.PcieEqLocalTransmitterOverrideEnable;
    PchPcieConfig->RootPort[RpIndex].PcieRpCommonConfig.PcieGen3LinkEqPlatformSettings.Ph3NumberOfPresetsOrCoefficients = FspsUpd->FspsConfig.PcieEqPh3NumberOfPresetsOrCoefficients;
    for (Index = 0; Index < PCIE_LINK_EQ_COEFFICIENTS_MAX; Index++) {
      PchPcieConfig->RootPort[RpIndex].PcieRpCommonConfig.PcieGen3LinkEqPlatformSettings.Ph3CoefficientsList[Index].PreCursor  = FspsUpd->FspsConfig.PcieEqPh3PreCursorList[Index];
      PchPcieConfig->RootPort[RpIndex].PcieRpCommonConfig.PcieGen3LinkEqPlatformSettings.Ph3CoefficientsList[Index].PostCursor = FspsUpd->FspsConfig.PcieEqPh3PostCursorList[Index];
    }
    for (Index = 0; Index < PCIE_LINK_EQ_PRESETS_MAX; Index++) {
      PchPcieConfig->RootPort[RpIndex].PcieRpCommonConfig.PcieGen3LinkEqPlatformSettings.Ph3PresetList[Index] = FspsUpd->FspsConfig.PcieEqPh3PresetList[Index];
    }
    PchPcieConfig->RootPort[RpIndex].PcieRpCommonConfig.PcieGen3LinkEqPlatformSettings.Ph1DownstreamPortTransmitterPreset = FspsUpd->FspsConfig.PcieEqPh1DownstreamPortTransmitterPreset;
    PchPcieConfig->RootPort[RpIndex].PcieRpCommonConfig.PcieGen3LinkEqPlatformSettings.Ph1UpstreamPortTransmitterPreset   = FspsUpd->FspsConfig.PcieEqPh1UpstreamPortTransmitterPreset;
    PchPcieConfig->RootPort[RpIndex].PcieRpCommonConfig.PcieGen3LinkEqPlatformSettings.Ph2LocalTransmitterOverridePreset  = FspsUpd->FspsConfig.PcieEqPh2LocalTransmitterOverridePreset;
  }

}

/**
  Update Serial IO policies.

  @param[in] SiPolicy  Pointer to SI_POLICY_PPI
  @param[in] FspsUpd   Pointer to FSPS_UPD
**/
STATIC
VOID
FspUpdateSerialIoPolicy (
  IN SI_POLICY_PPI  *SiPolicy,
  IN FSPS_UPD       *FspsUpd
  )
{
  UINTN                 Index;
  UINTN                 CsIndex;
  SERIAL_IO_CONFIG      *SerialIoConfig;
  EFI_STATUS            Status;

  Status = GetConfigBlock ((VOID *) SiPolicy, &gSerialIoConfigGuid, (VOID *) &SerialIoConfig);
  ASSERT_EFI_ERROR (Status);
  if (EFI_ERROR (Status)) {
    return;
  }

  for (Index = 0; Index < GetPchMaxSerialIoSpiControllersNum (); Index++) {
    SerialIoConfig->SpiDeviceConfig[Index].Mode            = FspsUpd->FspsConfig.SerialIoSpiMode[Index];
    SerialIoConfig->SpiDeviceConfig[Index].DefaultCsOutput = FspsUpd->FspsConfig.SerialIoSpiDefaultCsOutput[Index];
    SerialIoConfig->SpiDeviceConfig[Index].CsMode          = FspsUpd->FspsConfig.SerialIoSpiCsMode[Index];
    SerialIoConfig->SpiDeviceConfig[Index].CsState         = FspsUpd->FspsConfig.SerialIoSpiCsState[Index];
    for (CsIndex = 0; CsIndex < PCH_MAX_SERIALIO_SPI_CHIP_SELECTS; CsIndex++) {
      SerialIoConfig->SpiDeviceConfig[Index].CsEnable[CsIndex]   = FspsUpd->FspsConfig.SerialIoSpiCsEnable[Index * PCH_MAX_SERIALIO_SPI_CHIP_SELECTS + CsIndex];
      SerialIoConfig->SpiDeviceConfig[Index].CsPolarity[CsIndex] = FspsUpd->FspsConfig.SerialIoSpiCsPolarity[Index * PCH_MAX_SERIALIO_SPI_CHIP_SELECTS + CsIndex];
    }
  }

  for (Index = 0; Index < GetPchMaxSerialIoUartControllersNum (); Index++) {
    SerialIoConfig->UartDeviceConfig[Index].Mode                = FspsUpd->FspsConfig.SerialIoUartMode[Index];
    SerialIoConfig->UartDeviceConfig[Index].Attributes.BaudRate = FspsUpd->FspsConfig.SerialIoUartBaudRate[Index];
    SerialIoConfig->UartDeviceConfig[Index].Attributes.Parity   = FspsUpd->FspsConfig.SerialIoUartParity[Index];
    SerialIoConfig->UartDeviceConfig[Index].Attributes.DataBits = FspsUpd->FspsConfig.SerialIoUartDataBits[Index];
    SerialIoConfig->UartDeviceConfig[Index].Attributes.StopBits = FspsUpd->FspsConfig.SerialIoUartStopBits[Index];
    SerialIoConfig->UartDeviceConfig[Index].Attributes.AutoFlow = FspsUpd->FspsConfig.SerialIoUartAutoFlow[Index];
    SerialIoConfig->UartDeviceConfig[Index].PinMux.Rx           = FspsUpd->FspsConfig.SerialIoUartRxPinMuxPolicy[Index];
    SerialIoConfig->UartDeviceConfig[Index].PinMux.Tx           = FspsUpd->FspsConfig.SerialIoUartTxPinMuxPolicy[Index];
    SerialIoConfig->UartDeviceConfig[Index].PinMux.Rts          = FspsUpd->FspsConfig.SerialIoUartRtsPinMuxPolicy[Index];
    SerialIoConfig->UartDeviceConfig[Index].PinMux.Cts          = FspsUpd->FspsConfig.SerialIoUartCtsPinMuxPolicy[Index];
    SerialIoConfig->UartDeviceConfig[Index].PowerGating         = FspsUpd->FspsConfig.SerialIoUartPowerGating[Index];
    SerialIoConfig->UartDeviceConfig[Index].DmaEnable           = FspsUpd->FspsConfig.SerialIoUartDmaEnable[Index];
    SerialIoConfig->UartDeviceConfig[Index].DBG2                = FspsUpd->FspsConfig.SerialIoUartDbg2[Index];
  }

  for (Index = 0; Index < GetPchMaxSerialIoI2cControllersNum (); Index++) {
    SerialIoConfig->I2cDeviceConfig[Index].Mode           = FspsUpd->FspsConfig.SerialIoI2cMode[Index];
    SerialIoConfig->I2cDeviceConfig[Index].PinMux.Sda     = FspsUpd->FspsConfig.PchSerialIoI2cSdaPinMux[Index];
    SerialIoConfig->I2cDeviceConfig[Index].PinMux.Scl     = FspsUpd->FspsConfig.PchSerialIoI2cSclPinMux[Index];
    SerialIoConfig->I2cDeviceConfig[Index].PadTermination = FspsUpd->FspsConfig.PchSerialIoI2cPadsTermination[Index];
  }
}

/**
  Update Interrupt policies.

  @param[in] SiPolicy  Pointer to SI_POLICY_PPI
  @param[in] FspsUpd   Pointer to FSPS_UPD
**/
STATIC
VOID
FspUpdateInterruptPolicy (
  IN SI_POLICY_PPI  *SiPolicy,
  IN FSPS_UPD       *FspsUpd
  )
{
  UINT8                        NumOfDevIntConfig;
  PCH_DEVICE_INTERRUPT_CONFIG  *DevIntConfg;
  PCH_INTERRUPT_CONFIG         *InterruptConfig;
  EFI_STATUS                   Status;

  Status = GetConfigBlock ((VOID *) SiPolicy, &gInterruptConfigGuid, (VOID *) &InterruptConfig);
  ASSERT_EFI_ERROR (Status);
  if (EFI_ERROR (Status)) {
    return;
  }

  if (FspsUpd->FspsConfig.NumOfDevIntConfig != 0) {
    DevIntConfg = (PCH_DEVICE_INTERRUPT_CONFIG *)FspsUpd->FspsConfig.DevIntConfigPtr;
    NumOfDevIntConfig = FspsUpd->FspsConfig.NumOfDevIntConfig;
    ASSERT (NumOfDevIntConfig <= PCH_MAX_DEVICE_INTERRUPT_CONFIG);

    InterruptConfig->NumOfDevIntConfig = NumOfDevIntConfig;
    ZeroMem (
      InterruptConfig->DevIntConfig,
      PCH_MAX_DEVICE_INTERRUPT_CONFIG * sizeof (PCH_DEVICE_INTERRUPT_CONFIG)
      );
    CopyMem (
      InterruptConfig->DevIntConfig,
      DevIntConfg,
      NumOfDevIntConfig * sizeof (PCH_DEVICE_INTERRUPT_CONFIG)
      );
  }
  InterruptConfig->GpioIrqRoute = FspsUpd->FspsConfig.GpioIrqRoute;
  InterruptConfig->SciIrqSelect = FspsUpd->FspsConfig.SciIrqSelect;
  InterruptConfig->TcoIrqSelect = FspsUpd->FspsConfig.TcoIrqSelect;
  InterruptConfig->TcoIrqEnable = FspsUpd->FspsConfig.TcoIrqEnable;
}

/**
  Update LockDown policies.

  @param[in] SiPolicy  Pointer to SI_POLICY_PPI
  @param[in] FspsUpd   Pointer to FSPS_UPD
**/
STATIC
VOID
FspUpdateLockDownPolicy (
  IN SI_POLICY_PPI  *SiPolicy,
  IN FSPS_UPD       *FspsUpd
  )
{
  PCH_LOCK_DOWN_CONFIG   *LockDownConfig;
  EFI_STATUS             Status;

  Status = GetConfigBlock ((VOID *) SiPolicy, &gLockDownConfigGuid, (VOID *) &LockDownConfig);
  ASSERT_EFI_ERROR (Status);
  if (EFI_ERROR (Status)) {
    return;
  }

  LockDownConfig->GlobalSmi       = FspsUpd->FspsConfig.PchLockDownGlobalSmi;
  LockDownConfig->BiosInterface   = FspsUpd->FspsConfig.PchLockDownBiosInterface;
  LockDownConfig->BiosLock        = FspsUpd->FspsConfig.PchLockDownBiosLock;

  LockDownConfig->UnlockGpioPads  = FspsUpd->FspsConfig.PchUnlockGpioPads;
}

/**
  Update Rtc policies.

  @param[in] SiPolicy  Pointer to SI_POLICY_PPI
  @param[in] FspsUpd   Pointer to FSPS_UPD
**/
STATIC
VOID
FspUpdateRtcPolicy (
  IN SI_POLICY_PPI  *SiPolicy,
  IN FSPS_UPD       *FspsUpd
  )
{
  RTC_CONFIG        *RtcConfig;
  EFI_STATUS        Status;

  Status = GetConfigBlock ((VOID *) SiPolicy, &gRtcConfigGuid, (VOID *) &RtcConfig);
  ASSERT_EFI_ERROR (Status);
  if (EFI_ERROR (Status)) {
    return;
  }

  RtcConfig->BiosInterfaceLock    = FspsUpd->FspsConfig.RtcBiosInterfaceLock;
  RtcConfig->MemoryLock           = FspsUpd->FspsConfig.RtcMemoryLock;
}

/**
  Update DMI policies.

  @param[in] SiPolicy  Pointer to SI_POLICY_PPI
  @param[in] FspsUpd   Pointer to FSPS_UPD
**/
STATIC
VOID
FspUpdateDmiPolicy (
  IN SI_POLICY_PPI  *SiPolicy,
  IN FSPS_UPD       *FspsUpd
  )
{
  PCH_DMI_CONFIG    *DmiConfig;
  EFI_STATUS        Status;

  Status = GetConfigBlock ((VOID *) SiPolicy, &gPchDmiConfigGuid, (VOID *) &DmiConfig);
  ASSERT_EFI_ERROR (Status);
  if (EFI_ERROR (Status)) {
    return;
  }

  DmiConfig->DmiAspmCtrl  = FspsUpd->FspsConfig.PchDmiAspmCtrl;
  DmiConfig->PwrOptEnable = FspsUpd->FspsConfig.PchPwrOptEnable;
  DmiConfig->CwbEnable    = FspsUpd->FspsConfig.PchDmiCwbEnable;
}

/**
  Update Thc policies.

  @param[in] SiPolicy  Pointer to SI_POLICY_PPI
  @param[in] FspsUpd   Pointer to FSPS_UPD
**/
STATIC
VOID
FspUpdateThcPolicy (
  IN SI_POLICY_PPI  *SiPolicy,
  IN FSPS_UPD       *FspsUpd
  )
{
  THC_CONFIG    *ThcConfig;
  EFI_STATUS    Status;
  UINT8         ThcIndex;

  ThcIndex = 0;

  Status = GetConfigBlock ((VOID *) SiPolicy, &gThcConfigGuid, (VOID *) &ThcConfig);
  ASSERT_EFI_ERROR (Status);
  if (EFI_ERROR (Status)) {
    return;
  }

  ThcConfig->ThcPort[0].Assignment         = FspsUpd->FspsConfig.ThcPort0Assignment;
  ThcConfig->ThcPort[0].InterruptPinMuxing = FspsUpd->FspsConfig.ThcPort0InterruptPinMuxing;
  ThcConfig->ThcPort[0].WakeOnTouch        = FspsUpd->FspsConfig.ThcPort0WakeOnTouch;
  ThcConfig->ThcPort[1].Assignment         = FspsUpd->FspsConfig.ThcPort1Assignment;
  ThcConfig->ThcPort[1].InterruptPinMuxing = FspsUpd->FspsConfig.ThcPort1InterruptPinMuxing;
  ThcConfig->ThcPort[1].WakeOnTouch        = FspsUpd->FspsConfig.ThcPort1WakeOnTouch;

  for (ThcIndex = 0; ThcIndex < PCH_MAX_THC_CONTROLLERS; ThcIndex++) {
    ThcConfig->ThcPort[ThcIndex].Mode                    = FspsUpd->FspsConfig.ThcMode[ThcIndex];
    ThcConfig->ThcPort[ThcIndex].ActiveLtr               = FspsUpd->FspsConfig.ThcActiveLtr[ThcIndex];
    ThcConfig->ThcPort[ThcIndex].IdleLtr                 = FspsUpd->FspsConfig.ThcIdleLtr[ThcIndex];
    ThcConfig->ThcPort[ThcIndex].LimitPacketSize         = FspsUpd->FspsConfig.ThcLimitPacketSize[ThcIndex];
    ThcConfig->ThcPort[ThcIndex].PerformanceLimitation   = FspsUpd->FspsConfig.ThcPerformanceLimitation[ThcIndex];
    ThcConfig->ThcPort[ThcIndex].HidOverSpi.Flags                     = FspsUpd->FspsConfig.ThcHidFlags[ThcIndex];
    ThcConfig->ThcPort[ThcIndex].HidOverSpi.Frequency                 = FspsUpd->FspsConfig.ThcHidConnectionSpeed[ThcIndex];
    ThcConfig->ThcPort[ThcIndex].HidOverSpi.InputReportBodyAddress    = FspsUpd->FspsConfig.ThcHidInputReportBodyAddress[ThcIndex];
    ThcConfig->ThcPort[ThcIndex].HidOverSpi.InputReportHeaderAddress  = FspsUpd->FspsConfig.ThcHidInputReportHeaderAddress[ThcIndex];
    ThcConfig->ThcPort[ThcIndex].HidOverSpi.OutputReportAddress       = FspsUpd->FspsConfig.ThcHidOutputReportAddress[ThcIndex];
    ThcConfig->ThcPort[ThcIndex].HidOverSpi.ReadOpcode                = FspsUpd->FspsConfig.ThcHidReadOpcode[ThcIndex];
    ThcConfig->ThcPort[ThcIndex].HidOverSpi.WriteOpcode               = FspsUpd->FspsConfig.ThcHidWriteOpcode[ThcIndex];
    ThcConfig->ThcPort[ThcIndex].HidOverSpi.ResetPad                  = FspsUpd->FspsConfig.ThcHidResetPad[ThcIndex];
    ThcConfig->ThcPort[ThcIndex].HidOverSpi.ResetPadTrigger           = FspsUpd->FspsConfig.ThcHidResetPadTrigger[ThcIndex];
  }
}

/**
  Update IEH policies.

  @param[in] SiPolicy  Pointer to SI_POLICY_PPI
  @param[in] FspsUpd   Pointer to FSPS_UPD
**/
STATIC
VOID
FspUpdateIehPolicy (
  IN SI_POLICY_PPI  *SiPolicy,
  IN FSPS_UPD       *FspsUpd
  )
{
  IEH_CONFIG        *IehConfig;
  EFI_STATUS        Status;

  Status = GetConfigBlock ((VOID *) SiPolicy, &gIehConfigGuid, (VOID *) &IehConfig);
  ASSERT_EFI_ERROR (Status);
  if (EFI_ERROR (Status)) {
    return;
  }

  IehConfig->Mode = FspsUpd->FspsConfig.IehMode;
}


/**
  Update FlashProtection policies.

  @param[in] SiPolicy  Pointer to SI_POLICY_PPI
  @param[in] FspsUpd   Pointer to FSPS_UPD
**/
STATIC
VOID
FspUpdateFlashProtectionPolicy (
  IN SI_POLICY_PPI  *SiPolicy,
  IN FSPS_UPD       *FspsUpd
  )
{
  UINTN                        Index;
  PCH_FLASH_PROTECTION_CONFIG  *FlashProtectionConfig;
  EFI_STATUS                   Status;

  Status = GetConfigBlock ((VOID *) SiPolicy, &gFlashProtectionConfigGuid, (VOID *) &FlashProtectionConfig);
  ASSERT_EFI_ERROR (Status);
  if (EFI_ERROR (Status)) {
    return;
  }

  for (Index = 0; Index < PCH_FLASH_PROTECTED_RANGES; Index ++) {
    FlashProtectionConfig->ProtectRange[Index].WriteProtectionEnable = FspsUpd->FspsConfig.PchWriteProtectionEnable[Index];
    FlashProtectionConfig->ProtectRange[Index].ReadProtectionEnable  = FspsUpd->FspsConfig.PchReadProtectionEnable[Index];
    FlashProtectionConfig->ProtectRange[Index].ProtectedRangeLimit   = FspsUpd->FspsConfig.PchProtectedRangeLimit[Index];
    FlashProtectionConfig->ProtectRange[Index].ProtectedRangeBase    = FspsUpd->FspsConfig.PchProtectedRangeBase[Index];
  }
}

/**
  Update IO APIC policies.

  @param[in] SiPolicy  Pointer to SI_POLICY_PPI
  @param[in] FspsUpd   Pointer to FSPS_UPD
**/
STATIC
VOID
FspUpdateIoApicPolicy (
  IN SI_POLICY_PPI  *SiPolicy,
  IN FSPS_UPD       *FspsUpd
  )
{
  PCH_IOAPIC_CONFIG *IoApicConfig;
  EFI_STATUS        Status;

  Status = GetConfigBlock ((VOID *) SiPolicy, &gIoApicConfigGuid, (VOID *) &IoApicConfig);
  ASSERT_EFI_ERROR (Status);
  if (EFI_ERROR (Status)) {
    return;
  }

  IoApicConfig->IoApicEntry24_119          = FspsUpd->FspsConfig.PchIoApicEntry24_119;
  IoApicConfig->Enable8254ClockGating      = FspsUpd->FspsConfig.Enable8254ClockGating;
  IoApicConfig->Enable8254ClockGatingOnS3  = FspsUpd->FspsConfig.Enable8254ClockGatingOnS3;
  IoApicConfig->IoApicId                   = FspsUpd->FspsConfig.PchIoApicId;
}

/**
  Update P2sb policies.

  @param[in] SiPolicy  Pointer to SI_POLICY_PPI
  @param[in] FspsUpd   Pointer to FSPS_UPD
**/
STATIC
VOID
FspUpdateP2sbPolicy (
  IN SI_POLICY_PPI  *SiPolicy,
  IN FSPS_UPD       *FspsUpd
  )
{
  PCH_P2SB_CONFIG   *P2sbConfig;
  EFI_STATUS        Status;

  Status = GetConfigBlock ((VOID *) SiPolicy, &gP2sbConfigGuid, (VOID *) &P2sbConfig);
  ASSERT_EFI_ERROR (Status);
  if (EFI_ERROR (Status)) {
    return;
  }

  P2sbConfig->SbAccessUnlock  = FspsUpd->FspsConfig.PchSbAccessUnlock;
}

/**
  Update PCH General policies.

  @param[in] SiPolicy  Pointer to SI_POLICY_PPI
  @param[in] FspsUpd   Pointer to FSPS_UPD
**/
STATIC
VOID
FspUpdatePchGeneralPolicy (
  IN SI_POLICY_PPI  *SiPolicy,
  IN FSPS_UPD       *FspsUpd
  )
{
  PCH_GENERAL_CONFIG *PchGeneralConfig;
  EFI_STATUS         Status;

  Status = GetConfigBlock ((VOID *) SiPolicy, &gPchGeneralConfigGuid, (VOID *) &PchGeneralConfig);
  ASSERT_EFI_ERROR (Status);
  if (EFI_ERROR (Status)) {
    return;
  }

  PchGeneralConfig->Crid              = FspsUpd->FspsConfig.PchCrid;
  PchGeneralConfig->AcpiL6dPmeHandling = FspsUpd->FspsConfig.PchAcpiL6dPmeHandling;
  PchGeneralConfig->LegacyIoLowLatency = FspsUpd->FspsConfig.PchLegacyIoLowLatency;
}

/**
  Update Power Management policies.

  @param[in] SiPolicy  Pointer to SI_POLICY_PPI
  @param[in] FspsUpd   Pointer to FSPS_UPD
**/
STATIC
VOID
FspUpdatePmPolicy (
  IN SI_POLICY_PPI  *SiPolicy,
  IN FSPS_UPD       *FspsUpd
  )
{
  PCH_PM_CONFIG     *PmConfig;
  ADR_CONFIG        *PmcAdrConfig;
  EFI_STATUS        Status;

  Status = GetConfigBlock ((VOID *) SiPolicy, &gPmConfigGuid, (VOID *) &PmConfig);
  ASSERT_EFI_ERROR (Status);
  if (EFI_ERROR (Status)) {
    return;
  }

  Status = GetConfigBlock ((VOID *) SiPolicy, &gAdrConfigGuid, (VOID *) &PmcAdrConfig);
  ASSERT_EFI_ERROR (Status);
  if (EFI_ERROR (Status)) {
    return;
  }

  PmConfig->WakeConfig.PmeB0S5Dis         = FspsUpd->FspsConfig.PchPmPmeB0S5Dis;
  PmConfig->WakeConfig.WolEnableOverride  = FspsUpd->FspsConfig.PchPmWolEnableOverride;
  PmConfig->WakeConfig.PcieWakeFromDeepSx = FspsUpd->FspsConfig.PchPmPcieWakeFromDeepSx;
  PmConfig->WakeConfig.WoWlanEnable       = FspsUpd->FspsConfig.PchPmWoWlanEnable;
  PmConfig->WakeConfig.WoWlanDeepSxEnable = FspsUpd->FspsConfig.PchPmWoWlanDeepSxEnable;
  PmConfig->WakeConfig.LanWakeFromDeepSx  = FspsUpd->FspsConfig.PchPmLanWakeFromDeepSx;

  PmConfig->PchDeepSxPol       = FspsUpd->FspsConfig.PchPmDeepSxPol;
  PmConfig->PchSlpS3MinAssert  = FspsUpd->FspsConfig.PchPmSlpS3MinAssert;
  PmConfig->PchSlpS4MinAssert  = FspsUpd->FspsConfig.PchPmSlpS4MinAssert;
  PmConfig->PchSlpSusMinAssert = FspsUpd->FspsConfig.PchPmSlpSusMinAssert;
  PmConfig->PchSlpAMinAssert   = FspsUpd->FspsConfig.PchPmSlpAMinAssert;
  PmConfig->SlpStrchSusUp        = FspsUpd->FspsConfig.PchPmSlpStrchSusUp;
  PmConfig->SlpLanLowDc          = FspsUpd->FspsConfig.PchPmSlpLanLowDc;
  PmConfig->PwrBtnOverridePeriod = FspsUpd->FspsConfig.PchPmPwrBtnOverridePeriod;
  PmConfig->DisableEnergyReport  = FspsUpd->FspsConfig.PchPmDisableEnergyReport;
  PmConfig->DisableDsxAcPresentPulldown = FspsUpd->FspsConfig.PchPmDisableDsxAcPresentPulldown;
  PmConfig->DisableNativePowerButton = FspsUpd->FspsConfig.PchPmDisableNativePowerButton;
  PmConfig->PowerButtonDebounce  = FspsUpd->FspsConfig.PmcPowerButtonDebounce;
  PmConfig->MeWakeSts            = FspsUpd->FspsConfig.PchPmMeWakeSts;
  PmConfig->WolOvrWkSts          = FspsUpd->FspsConfig.PchPmWolOvrWkSts;
  PmConfig->EnableTcoTimer       = FspsUpd->FspsConfig.EnableTcoTimer;
#if FixedPcdGet8(PcdEmbeddedEnable) == 0x1
  PmConfig->EnableTimedGpio0     = FspsUpd->FspsConfig.EnableTimedGpio0;
  PmConfig->EnableTimedGpio1     = FspsUpd->FspsConfig.EnableTimedGpio1;
#endif
  PmConfig->VrAlert                   = FspsUpd->FspsConfig.PchPmVrAlert;
  PmConfig->PchPwrCycDur              = FspsUpd->FspsConfig.PchPmPwrCycDur;
  PmConfig->PciePllSsc                = FspsUpd->FspsConfig.PchPmPciePllSsc;
  PmConfig->PmcDbgMsgEn               = FspsUpd->FspsConfig.PmcDbgMsgEn;
  PmConfig->PsOnEnable                = FspsUpd->FspsConfig.PsOnEnable;
  PmConfig->CpuC10GatePinEnable       = FspsUpd->FspsConfig.PmcCpuC10GatePinEnable;
  PmConfig->ModPhySusPgEnable         = FspsUpd->FspsConfig.PmcModPhySusPgEnable;
  PmConfig->Usb2PhySusPgEnable        = FspsUpd->FspsConfig.PmcUsb2PhySusPgEnable;
  PmConfig->OsIdleEnable              = FspsUpd->FspsConfig.PmcOsIdleEnable;
  PmConfig->LpmS0ixSubStateEnable.Val = FspsUpd->FspsConfig.PmcLpmS0ixSubStateEnableMask;
  PmConfig->V1p05PhyExtFetControlEn   = FspsUpd->FspsConfig.PmcV1p05PhyExtFetControlEn;
  PmConfig->V1p05IsExtFetControlEn    = FspsUpd->FspsConfig.PmcV1p05IsExtFetControlEn;
  PmConfig->S0ixAutoDemotion          = FspsUpd->FspsConfig.PchS0ixAutoDemotion;
  PmConfig->LatchEventsC10Exit        = FspsUpd->FspsConfig.PchPmLatchEventsC10Exit;
  PmConfig->C10DynamicThresholdAdjustment = FspsUpd->FspsConfig.PmcC10DynamicThresholdAdjustment;
  PmConfig->SkipVccInConfig               = FspsUpd->FspsConfig.PmcSkipVccInConfig;


  PmcAdrConfig->AdrEn  = FspsUpd->FspsConfig.PmcAdrEn;
  PmcAdrConfig->AdrTimerEn = FspsUpd->FspsConfig.PmcAdrTimerEn;
  PmcAdrConfig->AdrTimer1Val = FspsUpd->FspsConfig.PmcAdrTimer1Val;
  PmcAdrConfig->AdrMultiplier1Val = FspsUpd->FspsConfig.PmcAdrMultiplier1Val;
  PmcAdrConfig->AdrHostPartitionReset = FspsUpd->FspsConfig.PmcAdrHostPartitionReset;
  PmcAdrConfig->AdrSrcOverride = FspsUpd->FspsConfig.PmcAdrSrcOverride;
}
/**
  Update Thermal policies.

  @param[in] SiPolicy  Pointer to SI_POLICY_PPI
  @param[in] FspsUpd   Pointer to FSPS_UPD
**/
STATIC
VOID
FspUpdateThermalPolicy (
  IN SI_POLICY_PPI  *SiPolicy,
  IN FSPS_UPD       *FspsUpd
  )
{
  UINTN             Index;
  THERMAL_CONFIG    *ThermalConfig;
  EFI_STATUS        Status;

  Status = GetConfigBlock ((VOID *) SiPolicy, &gThermalConfigGuid, (VOID *) &ThermalConfig);
  ASSERT_EFI_ERROR (Status);
  if (EFI_ERROR (Status)) {
    return;
  }

  ThermalConfig->PchHotEnable = FspsUpd->FspsConfig.PchHotEnable;

  ThermalConfig->TTLevels.T0Level            = FspsUpd->FspsConfig.PchT0Level;
  ThermalConfig->TTLevels.T1Level            = FspsUpd->FspsConfig.PchT1Level;
  ThermalConfig->TTLevels.T2Level            = FspsUpd->FspsConfig.PchT2Level;
  ThermalConfig->TTLevels.TTEnable           = FspsUpd->FspsConfig.PchTTEnable;
  ThermalConfig->TTLevels.TTState13Enable    = FspsUpd->FspsConfig.PchTTState13Enable;
  ThermalConfig->TTLevels.TTLock             = FspsUpd->FspsConfig.PchTTLock;
  ThermalConfig->TTLevels.SuggestedSetting   = FspsUpd->FspsConfig.TTSuggestedSetting;
  ThermalConfig->TTLevels.PchCrossThrottling = FspsUpd->FspsConfig.TTCrossThrottling;

  ThermalConfig->DmiHaAWC.DmiTsawEn        = FspsUpd->FspsConfig.PchDmiTsawEn;
  ThermalConfig->DmiHaAWC.SuggestedSetting = FspsUpd->FspsConfig.DmiSuggestedSetting;
  ThermalConfig->DmiHaAWC.TS0TW            = FspsUpd->FspsConfig.DmiTS0TW;
  ThermalConfig->DmiHaAWC.TS1TW            = FspsUpd->FspsConfig.DmiTS1TW;
  ThermalConfig->DmiHaAWC.TS2TW            = FspsUpd->FspsConfig.DmiTS2TW;
  ThermalConfig->DmiHaAWC.TS3TW            = FspsUpd->FspsConfig.DmiTS3TW;

  ThermalConfig->MemoryThrottling.Enable = FspsUpd->FspsConfig.PchMemoryThrottlingEnable;

  for (Index = 0; Index < 2; Index++) {
    ThermalConfig->MemoryThrottling.TsGpioPinSetting[Index].PmsyncEnable     = FspsUpd->FspsConfig.PchMemoryPmsyncEnable[Index];
    ThermalConfig->MemoryThrottling.TsGpioPinSetting[Index].C0TransmitEnable = FspsUpd->FspsConfig.PchMemoryC0TransmitEnable[Index];
    ThermalConfig->MemoryThrottling.TsGpioPinSetting[Index].PinSelection     = FspsUpd->FspsConfig.PchMemoryPinSelection[Index];
  }

  ThermalConfig->PchHotLevel = FspsUpd->FspsConfig.PchTemperatureHotLevel;

}

/**
  Update Psf policies.

  @param[in] SiPolicy  Pointer to SI_POLICY_PPI
  @param[in] FspsUpd   Pointer to FSPS_UPD
**/
STATIC
VOID
FspUpdatePsfPolicy (
  IN SI_POLICY_PPI  *SiPolicy,
  IN FSPS_UPD       *FspsUpd
  )
{
  PSF_CONFIG        *PsfConfig;
  EFI_STATUS        Status;

  Status = GetConfigBlock ((VOID *) SiPolicy, &gPsfConfigGuid, (VOID *) &PsfConfig);
  ASSERT_EFI_ERROR (Status);
  if (EFI_ERROR (Status)) {
    return;
  }

#if FixedPcdGet8(PcdEmbeddedEnable) == 0x1
  PsfConfig->TccEnable = FspsUpd->FspsConfig.PsfTccEnable;
#endif
}

/**
  Update Fusa policies.

  @param[in] SiPolicy  Pointer to SI_POLICY_PPI
  @param[in] FspsUpd   Pointer to FSPS_UPD
**/
STATIC
VOID
FspUpdateFusaPolicy (
  IN SI_POLICY_PPI  *SiPolicy,
  IN FSPS_UPD       *FspsUpd
  )
{
#if FixedPcdGetBool(PcdEmbeddedEnable) == 1
  FUSA_CONFIG       *FusaConfig;
  EFI_STATUS        Status;

  Status = GetConfigBlock ((VOID *) SiPolicy, &gFusaConfigGuid, (VOID *) &FusaConfig);
  ASSERT_EFI_ERROR (Status);
  if (EFI_ERROR (Status)) {
    return;
  }

  FusaConfig->PsfFusaConfigEnable = FspsUpd->FspsConfig.PsfFusaConfigEnable;
#endif
}


/**
  Update Fivr policies.

  @param[in] SiPolicy  Pointer to SI_POLICY_PPI
  @param[in] FspsUpd   Pointer to FSPS_UPD
**/
STATIC
VOID
FspUpdateFivrPolicy (
  IN SI_POLICY_PPI  *SiPolicy,
  IN FSPS_UPD       *FspsUpd
  )
{
  PCH_FIVR_CONFIG   *FivrConfig;
  EFI_STATUS        Status;

  Status = GetConfigBlock ((VOID *) SiPolicy, &gFivrConfigGuid, (VOID *) &FivrConfig);
  ASSERT_EFI_ERROR (Status);
  if (EFI_ERROR (Status)) {
    return;
  }

  FivrConfig->ExtV1p05Rail.EnabledStates           = FspsUpd->FspsConfig.PchFivrExtV1p05RailEnabledStates;
  FivrConfig->ExtV1p05Rail.SupportedVoltageStates  = FspsUpd->FspsConfig.PchFivrExtV1p05RailSupportedVoltageStates;
  FivrConfig->ExtV1p05Rail.Voltage                 = FspsUpd->FspsConfig.PchFivrExtV1p05RailVoltage;
  if (FspsUpd->FspsConfig.PchFivrExtV1p05RailIccMaximum == 0 &&
      FspsUpd->FspsConfig.PchFivrExtV1p05RailIccMax != 0) {
    FivrConfig->ExtV1p05Rail.IccMaximum = FspsUpd->FspsConfig.PchFivrExtV1p05RailIccMax;
  } else {
    FivrConfig->ExtV1p05Rail.IccMaximum = FspsUpd->FspsConfig.PchFivrExtV1p05RailIccMaximum;
  }
  // FivrConfig->ExtV1p05Rail.IccMaximum              = FspsUpd->FspsConfig.PchFivrExtV1p05RailIccMaximum;
  // FivrConfig->ExtV1p05Rail.IccMax                  = FspsUpd->FspsConfig.PchFivrExtV1p05RailIccMax;

  FivrConfig->ExtV1p05Rail.CtrlRampTmr   = FspsUpd->FspsConfig.PchFivrExtV1p05RailCtrlRampTmr;
  FivrConfig->ExtVnnRail.EnabledStates          = FspsUpd->FspsConfig.PchFivrExtVnnRailEnabledStates;
  FivrConfig->ExtVnnRail.SupportedVoltageStates = FspsUpd->FspsConfig.PchFivrExtVnnRailSupportedVoltageStates;
  FivrConfig->ExtVnnRail.Voltage                = FspsUpd->FspsConfig.PchFivrExtVnnRailVoltage;
  if (FspsUpd->FspsConfig.PchFivrExtVnnRailIccMaximum == 0 &&
      FspsUpd->FspsConfig.PchFivrExtVnnRailIccMax != 0) {
    FivrConfig->ExtVnnRail.IccMaximum = FspsUpd->FspsConfig.PchFivrExtVnnRailIccMax;
  } else {
    FivrConfig->ExtVnnRail.IccMaximum = FspsUpd->FspsConfig.PchFivrExtVnnRailIccMaximum;
  }

  FivrConfig->ExtVnnRail.CtrlRampTmr     = FspsUpd->FspsConfig.PchFivrExtVnnRailCtrlRampTmr;
  FivrConfig->ExtVnnRailSx.EnabledStates = FspsUpd->FspsConfig.PchFivrExtVnnRailSxEnabledStates;
  FivrConfig->ExtVnnRailSx.Voltage       = FspsUpd->FspsConfig.PchFivrExtVnnRailSxVoltage;
  if (FspsUpd->FspsConfig.PchFivrExtVnnRailSxIccMaximum == 0 &&
      FspsUpd->FspsConfig.PchFivrExtVnnRailSxIccMax != 0) {
    FivrConfig->ExtVnnRailSx.IccMaximum = FspsUpd->FspsConfig.PchFivrExtVnnRailSxIccMax;
  } else {
    FivrConfig->ExtVnnRailSx.IccMaximum = FspsUpd->FspsConfig.PchFivrExtVnnRailSxIccMaximum;
  }

  FivrConfig->VccinAux.LowToHighCurModeVolTranTime = FspsUpd->FspsConfig.PchFivrVccinAuxLowToHighCurModeVolTranTime;
  FivrConfig->VccinAux.RetToHighCurModeVolTranTime = FspsUpd->FspsConfig.PchFivrVccinAuxRetToHighCurModeVolTranTime;
  FivrConfig->VccinAux.RetToLowCurModeVolTranTime  = FspsUpd->FspsConfig.PchFivrVccinAuxRetToLowCurModeVolTranTime;
  FivrConfig->VccinAux.OffToHighCurModeVolTranTime = FspsUpd->FspsConfig.PchFivrVccinAuxOffToHighCurModeVolTranTime;
  FivrConfig->FivrDynPm                            = FspsUpd->FspsConfig.PchFivrDynPm;
}

/**
  Update Hybrid Storage policies.

  @param[in] SiPolicy  Pointer to SI_POLICY_PPI
  @param[in] FspsUpd   Pointer to FSPS_UPD
**/
STATIC
VOID
FspUpdateHybridStoragePolicy (
  IN SI_POLICY_PPI  *SiPolicy,
  IN FSPS_UPD       *FspsUpd
  )
{
  HYBRID_STORAGE_CONFIG   *HybridStorageConfig;
  EFI_STATUS              Status;

  Status = GetConfigBlock ((VOID *) SiPolicy, &gHybridStorageConfigGuid, (VOID *) &HybridStorageConfig);
  ASSERT_EFI_ERROR (Status);
  if (EFI_ERROR (Status)) {
    return;
  }

  HybridStorageConfig->HybridStorageMode                  = FspsUpd->FspsConfig.HybridStorageMode;
  HybridStorageConfig->CpuRootportUsedForHybridStorage    = FspsUpd->FspsConfig.CpuRootportUsedForHybridStorage;
  HybridStorageConfig->PchRootportUsedForCpuAttach        = FspsUpd->FspsConfig.PchRootportUsedForCpuAttach;
}

/**
  This function performs PCH SPI Policy update.

  @param[in]  SiPolicy       The SI Policy PPI instance
  @param[in]  FspsUpd        The pointer of FspsUpd
**/
STATIC
VOID
FspUpdateSpiPolicy (
  IN  SI_POLICY_PPI     *SiPolicy,
  IN  FSPS_UPD          *FspsUpd
  )
{
  EFI_STATUS    Status;
  SPI_CONFIG    *SpiConfig;

  Status = GetConfigBlock ((VOID *) SiPolicy, &gSpiConfigGuid, (VOID *) &SpiConfig);
  ASSERT_EFI_ERROR (Status);
  if (EFI_ERROR (Status)) {
    return;
  }

  SpiConfig->ExtendedBiosDecodeRangeEnable = FspsUpd->FspsConfig.PchSpiExtendedBiosDecodeRangeEnable;
  SpiConfig->ExtendedBiosDecodeRangeBase = (UINT16) FspsUpd->FspsConfig.PchSpiExtendedBiosDecodeRangeBase;
  SpiConfig->ExtendedBiosDecodeRangeLimit = (UINT16) FspsUpd->FspsConfig.PchSpiExtendedBiosDecodeRangeLimit;
}

/**
  This function performs PCH PEI Policy update.

  @param[in, out] SiPolicy       The SI Policy PPI instance
  @param[in]      FspsUpd        The pointer of FspsUpd

  @retval EFI_SUCCESS  The function completed successfully
**/
EFI_STATUS
EFIAPI
FspUpdatePeiPchPolicy (
  IN OUT SI_POLICY_PPI     *SiPolicy,
  IN     FSPS_UPD          *FspsUpd
  )
{
  DEBUG ((DEBUG_INFO | DEBUG_INIT, "FSP UpdatePeiPchPolicy\n"));

  FspUpdateGbePolicy (SiPolicy, FspsUpd);
#if FixedPcdGetBool (PcdEmbeddedEnable)
  FspUpdatePchTsnPolicy (SiPolicy, FspsUpd);
#endif
  FspUpdateHdAudioPolicy (SiPolicy, FspsUpd);
  FspUpdateCnviPolicy (SiPolicy, FspsUpd);
  FspUpdateHsioPolicy (SiPolicy, FspsUpd);
  FspUpdateEspiPolicy (SiPolicy, FspsUpd);
#if FixedPcdGetBool(PcdAdlLpSupport) == 1
  FspUpdateUfsPolicy (SiPolicy, FspsUpd);
#endif
  FspUpdateIshPolicy (SiPolicy, FspsUpd);
  FspUpdateSataPolicy (SiPolicy, FspsUpd);
  FspUpdateRstPolicy (SiPolicy, FspsUpd);
  FspUpdateUsbPolicy (SiPolicy, FspsUpd);
  FspUpdateUsb2PhyPolicy (SiPolicy, FspsUpd);
  FspUpdateUsb3HsioPolicy (SiPolicy, FspsUpd);
  FspUpdatePcieRpPolicy (SiPolicy, FspsUpd);
  FspUpdateSerialIoPolicy (SiPolicy, FspsUpd);
  FspUpdateInterruptPolicy (SiPolicy, FspsUpd);
  FspUpdateLockDownPolicy (SiPolicy, FspsUpd);
  FspUpdateRtcPolicy (SiPolicy, FspsUpd);
  FspUpdateDmiPolicy (SiPolicy, FspsUpd);
  FspUpdateThcPolicy (SiPolicy, FspsUpd);
  FspUpdateIehPolicy (SiPolicy, FspsUpd);
  FspUpdateFlashProtectionPolicy (SiPolicy, FspsUpd);
  FspUpdateIoApicPolicy (SiPolicy, FspsUpd);
  FspUpdateP2sbPolicy (SiPolicy, FspsUpd);
  FspUpdatePchGeneralPolicy (SiPolicy, FspsUpd);
  FspUpdatePmPolicy (SiPolicy, FspsUpd);
  FspUpdateThermalPolicy (SiPolicy, FspsUpd);
  FspUpdatePsfPolicy (SiPolicy, FspsUpd);
  FspUpdateFusaPolicy (SiPolicy, FspsUpd);
  FspUpdateHybridStoragePolicy (SiPolicy, FspsUpd);
  FspUpdateFivrPolicy (SiPolicy, FspsUpd);
  FspUpdateSpiPolicy (SiPolicy, FspsUpd);

  return EFI_SUCCESS;
}

