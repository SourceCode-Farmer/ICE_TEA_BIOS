/** @file
  Do Platform Stage System Agent initialization.

 @copyright
  INTEL CONFIDENTIAL
  Copyright 2013 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains an 'Intel Peripheral Driver' and is uniquely identified as
  "Intel Reference Module" and is licensed for Intel CPUs and chipsets under
  the terms of your license agreement with Intel or your vendor. This file may
  be modified by the user, subject to additional terms of the license agreement.

@par Specification Reference:
**/

#include <Ppi/SiPolicy.h>
#include <Library/DebugLib.h>
#include <FspEas.h>
#include <Library/PeiSaPolicyLib.h>
#include <Library/FspCommonLib.h>
#include <Library/DebugLib.h>
#include <FspsUpd.h>
#include <TcssInfo.h>
#include <TcssPeiConfig.h>
#include <VmdPeiConfig.h>
#include <CpuPcieConfig.h>
#include <Library/CpuPcieInfoFruLib.h>
#include <Library/CpuPcieRpLib.h>
#include <TelemetryPeiConfig.h>

/**
  Update Fusa policies.

  @param[in out] SiPolicy  Pointer to SI_POLICY_PPI
  @param[in] FspsUpd   Pointer to FSPS_UPD
**/
STATIC
VOID
FspUpdateFusaPolicy (
  IN OUT SI_POLICY_PPI  *SiPolicy,
  IN FSPS_UPD       *FspsUpd
  )
{
  FUSA_CONFIG       *FusaConfig;
  EFI_STATUS        Status;

  Status = GetConfigBlock ((VOID *) SiPolicy, &gFusaConfigGuid, (VOID *) &FusaConfig);
  ASSERT_EFI_ERROR (Status);
  if (EFI_ERROR (Status)) {
    return;
  }
#if FixedPcdGetBool(PcdEmbeddedEnable) == 1
  FusaConfig->FusaConfigEnable        = FspsUpd->FspsConfig.FusaConfigEnable;
  FusaConfig->FusaRunStartupArrayBist = FspsUpd->FspsConfig.FusaRunStartupArrayBist;
  FusaConfig->FusaRunStartupScanBist  = FspsUpd->FspsConfig.FusaRunStartupScanBist;
  FusaConfig->FusaRunPeriodicScanBist = FspsUpd->FspsConfig.FusaRunPeriodicScanBist;
  FusaConfig->Module0Lockstep         = FspsUpd->FspsConfig.Module0Lockstep;
  FusaConfig->Module1Lockstep         = FspsUpd->FspsConfig.Module1Lockstep;
  FusaConfig->DisplayFusaConfigEnable = FspsUpd->FspsConfig.DisplayFusaConfigEnable;
  FusaConfig->GraphicFusaConfigEnable = FspsUpd->FspsConfig.GraphicFusaConfigEnable;
  FusaConfig->OpioFusaConfigEnable    = FspsUpd->FspsConfig.OpioFusaConfigEnable;
  FusaConfig->IopFusaConfigEnable     = FspsUpd->FspsConfig.IopFusaConfigEnable;
#endif
  FusaConfig->IopFusaMcaCheckEnable   = 1;
}

/**
  UpdatePeiSaPolicy performs SA PEI Policy initialization

  @param[in out] SiPolicyPpi       SI_POLICY PPI
  @param[in]     FspsUpd           The pointer of FspsUpd

  @retval EFI_SUCCESS              The policy is installed and initialized.
**/
EFI_STATUS
FspUpdatePeiSaPolicy (
  IN OUT SI_POLICY_PPI       *SiPolicyPpi,
  IN     FSPS_UPD            *FspsUpd
  )
{
  EFI_STATUS                    Status;
  FSP_S_CONFIG                  *FspsConfig;
  HOST_BRIDGE_PEI_CONFIG        *HostBridgePeiConfig;
  GRAPHICS_PEI_CONFIG           *GtConfig;
  GNA_CONFIG                    *GnaConfig;
#if FixedPcdGetBool(PcdCpuPcieEnable) == 1
  CPU_PCIE_CONFIG               *CpuPcieRpConfig;
  UINT8                         RpIndex;
  UINTN                         MaxPcieRootPorts;
#endif
#if FixedPcdGetBool(PcdITbtEnable) == 1
  UINT8                         Index;
  TCSS_PEI_CONFIG               *TcssPeiConfig;
#endif  // PcdITbtEnable
#if FixedPcdGetBool(PcdVmdEnable) == 1
  VMD_PEI_CONFIG                *VmdPeiConfig;
#endif
  TELEMETRY_PEI_CONFIG          *TelemetryPeiConfig;

  DEBUG ((DEBUG_INFO | DEBUG_INIT, "FSP UpdatePeiSaPolicy\n"));

  // Use direct pointers below, to reduce debug build code size
  FspsConfig           = &FspsUpd->FspsConfig;


  HostBridgePeiConfig = NULL;
  Status = GetConfigBlock ((VOID *) SiPolicyPpi, &gHostBridgePeiConfigGuid, (VOID *) &HostBridgePeiConfig);
  ASSERT_EFI_ERROR (Status);

  GtConfig = NULL;
  Status = GetConfigBlock ((VOID *) SiPolicyPpi, &gGraphicsPeiConfigGuid, (VOID *)&GtConfig);
  ASSERT_EFI_ERROR (Status);

  GnaConfig = NULL;
  Status = GetConfigBlock ((VOID *) SiPolicyPpi, &gGnaConfigGuid, (VOID *)&GnaConfig);
  ASSERT_EFI_ERROR (Status);

#if FixedPcdGetBool(PcdCpuPcieEnable) == 1
  CpuPcieRpConfig = NULL;
  Status = GetConfigBlock ((VOID *) SiPolicyPpi, &gCpuPcieRpConfigGuid, (VOID *)&CpuPcieRpConfig);
  ASSERT_EFI_ERROR(Status);
#endif

#if FixedPcdGetBool(PcdITbtEnable) == 1
  TcssPeiConfig = NULL;
  Status = GetConfigBlock((VOID *) SiPolicyPpi, &gTcssPeiConfigGuid, (VOID *) &TcssPeiConfig);
  ASSERT_EFI_ERROR(Status);
#endif  // PcdITbtEnable

#if FixedPcdGetBool(PcdVmdEnable) == 1
  VmdPeiConfig = NULL;
  Status = GetConfigBlock ((VOID *) SiPolicyPpi, &gVmdPeiConfigGuid, (VOID *) &VmdPeiConfig);
  // No ASSERT because a config block is not installed when PeiVmdPolicyLibNull is used
#endif

  TelemetryPeiConfig = NULL;
  Status = GetConfigBlock((VOID *)SiPolicyPpi, &gTelemetryPeiConfigGuid, (VOID *)&TelemetryPeiConfig);
  ASSERT_EFI_ERROR(Status);

  if (HostBridgePeiConfig != NULL) {
    HostBridgePeiConfig->Device4Enable    = FspsConfig->Device4Enable;
    HostBridgePeiConfig->SkipPamLock      = FspsConfig->SkipPamLock;
    HostBridgePeiConfig->EdramTestMode    = FspsConfig->EdramTestMode;
  }


  if (TelemetryPeiConfig != NULL) {
    TelemetryPeiConfig->CpuCrashLogEnable = FspsUpd->FspsConfig.CpuCrashLogEnable;
  }

  if (GtConfig != NULL) {
    GtConfig->GraphicsConfigPtr   = (VOID *) FspsConfig->GraphicsConfigPtr;
    GtConfig->LogoPtr             = (VOID *) FspsConfig->LogoPtr;
    GtConfig->LogoSize            = FspsConfig->LogoSize;
    GtConfig->PavpEnable          = FspsConfig->PavpEnable;
    GtConfig->CdClock             = FspsConfig->CdClock;
    GtConfig->PeiGraphicsPeimInit = FspsConfig->PeiGraphicsPeimInit;
    GtConfig->RenderStandby       = FspsConfig->RenderStandby;
    GtConfig->PmSupport           = FspsConfig->PmSupport;
    GtConfig->CdynmaxClampEnable  = FspsConfig->CdynmaxClampEnable;
    GtConfig->GtFreqMax           = FspsConfig->GtFreqMax;
    GtConfig->DisableTurboGt      = FspsConfig->DisableTurboGt;
    GtConfig->SkipCdClockInit     = FspsConfig->SkipCdClockInit;
    GtConfig->LogoPixelHeight     = FspsConfig->LogoPixelHeight;
    GtConfig->LogoPixelWidth      = FspsConfig->LogoPixelWidth;
    GtConfig->BltBufferAddress    = (VOID*) FspsConfig->BltBufferAddress;
    GtConfig->RC1pFreqEnable      = FspsConfig->RC1pFreqEnable;
    GtConfig->SkipFspGop          = FspsConfig->SkipFspGop;
    DEBUG ((DEBUG_INFO | DEBUG_INIT, "GtConfig->GraphicsConfigPtr from FSP UpdatePeiSaPolicy: 0x%x\n", GtConfig->GraphicsConfigPtr));
    DEBUG ((DEBUG_INFO | DEBUG_INIT, "GtConfig->LogoPtr: 0x%x \n", GtConfig->LogoPtr ));
    DEBUG ((DEBUG_INFO | DEBUG_INIT, "GtConfig->LogoSize: 0x%x \n", GtConfig->LogoSize));
    DEBUG ((DEBUG_INFO | DEBUG_INIT, "GtConfig->LogoPixelHeight: 0x%x \n", GtConfig->LogoPixelHeight));
    DEBUG ((DEBUG_INFO | DEBUG_INIT, "GtConfig->LogoPixelWidth: 0x%x \n", GtConfig->LogoPixelWidth));

  }

#if FixedPcdGetBool(PcdCpuPcieEnable) == 1
  //
  // Update PCIE RP RootPort policies
  //
  MaxPcieRootPorts = GetMaxCpuPciePortNum ();
  for (RpIndex = 0; RpIndex < MaxPcieRootPorts; RpIndex++) {
    CpuPcieRpConfig->RootPort[RpIndex].PcieRpCommonConfig.HotPlug                                           = FspsConfig->CpuPcieRpHotPlug[RpIndex];
    CpuPcieRpConfig->RootPort[RpIndex].PcieRpCommonConfig.PmSci                                             = FspsConfig->CpuPcieRpPmSci[RpIndex];
    CpuPcieRpConfig->RootPort[RpIndex].PcieRpCommonConfig.TransmitterHalfSwing                              = FspsConfig->CpuPcieRpTransmitterHalfSwing[RpIndex];
    CpuPcieRpConfig->RootPort[RpIndex].PcieRpCommonConfig.AdvancedErrorReporting                            = FspsConfig->CpuPcieRpAdvancedErrorReporting[RpIndex];
    CpuPcieRpConfig->RootPort[RpIndex].PcieRpCommonConfig.UnsupportedRequestReport                          = FspsConfig->CpuPcieRpUnsupportedRequestReport[RpIndex];
    CpuPcieRpConfig->RootPort[RpIndex].PcieRpCommonConfig.FatalErrorReport                                  = FspsConfig->CpuPcieRpFatalErrorReport[RpIndex];
    CpuPcieRpConfig->RootPort[RpIndex].PcieRpCommonConfig.NoFatalErrorReport                                = FspsConfig->CpuPcieRpNoFatalErrorReport[RpIndex];
    CpuPcieRpConfig->RootPort[RpIndex].PcieRpCommonConfig.CorrectableErrorReport                            = FspsConfig->CpuPcieRpCorrectableErrorReport[RpIndex];
    CpuPcieRpConfig->RootPort[RpIndex].PcieRpCommonConfig.SystemErrorOnFatalError                           = FspsConfig->CpuPcieRpSystemErrorOnFatalError[RpIndex];
    CpuPcieRpConfig->RootPort[RpIndex].PcieRpCommonConfig.SystemErrorOnNonFatalError                        = FspsConfig->CpuPcieRpSystemErrorOnNonFatalError[RpIndex];
    CpuPcieRpConfig->RootPort[RpIndex].PcieRpCommonConfig.SystemErrorOnCorrectableError                     = FspsConfig->CpuPcieRpSystemErrorOnCorrectableError[RpIndex];
    CpuPcieRpConfig->RootPort[RpIndex].PcieRpCommonConfig.MaxPayload                                        = FspsConfig->CpuPcieRpMaxPayload[RpIndex];
    CpuPcieRpConfig->RootPort[RpIndex].PcieRpCommonConfig.DpcEnabled                                        = FspsConfig->CpuPcieRpDpcEnabled[RpIndex];
    CpuPcieRpConfig->RootPort[RpIndex].MultiVcEnabled                                                       = FspsConfig->CpuPcieRpMultiVcEnabled[RpIndex];
    CpuPcieRpConfig->RootPort[RpIndex].PcieRpCommonConfig.EnablePeerMemoryWrite                             = FspsConfig->CpuPcieRpPeerToPeerMode[RpIndex];
    CpuPcieRpConfig->RootPort[RpIndex].FomsCp                                                               = FspsConfig->CpuPcieFomsCp[RpIndex];
    CpuPcieRpConfig->RootPort[RpIndex].PcieRpCommonConfig.RpDpcExtensionsEnabled                            = FspsConfig->CpuPcieRpDpcExtensionsEnabled[RpIndex];
    CpuPcieRpConfig->RootPort[RpIndex].PcieRpCommonConfig.PtmEnabled                                        = FspsConfig->CpuPcieRpPtmEnabled[RpIndex];
    CpuPcieRpConfig->RootPort[RpIndex].PcieRpCommonConfig.Gen3EqPh3Method                                   = FspsConfig->CpuPcieRpGen3EqPh3Method[RpIndex];
    CpuPcieRpConfig->RootPort[RpIndex].PcieRpCommonConfig.PhysicalSlotNumber                                = FspsConfig->CpuPcieRpPhysicalSlotNumber[RpIndex];
    CpuPcieRpConfig->RootPort[RpIndex].PcieRpCommonConfig.SlotImplemented                                   = FspsConfig->CpuPcieRpSlotImplemented[RpIndex];
    CpuPcieRpConfig->RootPort[RpIndex].PcieRpCommonConfig.Aspm                                              = FspsConfig->CpuPcieRpAspm[RpIndex];
    CpuPcieRpConfig->RootPort[RpIndex].PcieRpCommonConfig.L1Substates                                       = FspsConfig->CpuPcieRpL1Substates[RpIndex];
    CpuPcieRpConfig->RootPort[RpIndex].PcieRpCommonConfig.LtrEnable                                         = FspsConfig->CpuPcieRpLtrEnable[RpIndex];
    CpuPcieRpConfig->RootPort[RpIndex].PcieRpCommonConfig.PcieRpLtrConfig.LtrConfigLock                     = FspsConfig->CpuPcieRpLtrConfigLock[RpIndex];
    CpuPcieRpConfig->RootPort[RpIndex].PcieRpCommonConfig.AcsEnabled                                        = FspsConfig->CpuPcieRpAcsEnabled[RpIndex];
    CpuPcieRpConfig->RootPort[RpIndex].PcieRpCommonConfig.DetectTimeoutMs                                   = FspsConfig->CpuPcieRpDetectTimeoutMs[RpIndex];
    CpuPcieRpConfig->RootPort[RpIndex].PcieRpCommonConfig.EnableCpm                                         = FspsConfig->CpuPcieRpEnableCpm[RpIndex];

    CpuPcieRpConfig->RootPort[RpIndex].PcieRpCommonConfig.PcieRpLtrConfig.LtrMaxSnoopLatency                = FspsConfig->CpuPcieRpLtrMaxSnoopLatency[RpIndex];
    CpuPcieRpConfig->RootPort[RpIndex].PcieRpCommonConfig.PcieRpLtrConfig.LtrMaxNoSnoopLatency              = FspsConfig->CpuPcieRpLtrMaxNoSnoopLatency[RpIndex];

    CpuPcieRpConfig->RootPort[RpIndex].PcieRpCommonConfig.PcieRpLtrConfig.SnoopLatencyOverrideMode          = FspsConfig->CpuPcieRpSnoopLatencyOverrideMode[RpIndex];
    CpuPcieRpConfig->RootPort[RpIndex].PcieRpCommonConfig.PcieRpLtrConfig.SnoopLatencyOverrideMultiplier    = FspsConfig->CpuPcieRpSnoopLatencyOverrideMultiplier[RpIndex];
    CpuPcieRpConfig->RootPort[RpIndex].PcieRpCommonConfig.PcieRpLtrConfig.SnoopLatencyOverrideValue         = FspsConfig->CpuPcieRpSnoopLatencyOverrideValue[RpIndex];

    CpuPcieRpConfig->RootPort[RpIndex].PcieRpCommonConfig.PcieRpLtrConfig.NonSnoopLatencyOverrideMode       = FspsConfig->CpuPcieRpNonSnoopLatencyOverrideMode[RpIndex];
    CpuPcieRpConfig->RootPort[RpIndex].PcieRpCommonConfig.PcieRpLtrConfig.NonSnoopLatencyOverrideMultiplier = FspsConfig->CpuPcieRpNonSnoopLatencyOverrideMultiplier[RpIndex];
    CpuPcieRpConfig->RootPort[RpIndex].PcieRpCommonConfig.PcieRpLtrConfig.NonSnoopLatencyOverrideValue      = FspsConfig->PcieRpNonSnoopLatencyOverrideValue[RpIndex];

    CpuPcieRpConfig->RootPort[RpIndex].Gen3Uptp                                                             = FspsConfig->CpuPcieRpGen3Uptp[RpIndex];
    CpuPcieRpConfig->RootPort[RpIndex].Gen3Dptp                                                             = FspsConfig->CpuPcieRpGen3Dptp[RpIndex];

    CpuPcieRpConfig->RootPort[RpIndex].Gen4Uptp                                                             = FspsConfig->CpuPcieRpGen4Uptp[RpIndex];
    CpuPcieRpConfig->RootPort[RpIndex].Gen4Dptp                                                             = FspsConfig->CpuPcieRpGen4Dptp[RpIndex];

    CpuPcieRpConfig->RootPort[RpIndex].Gen5Uptp                                                             = FspsConfig->CpuPcieRpGen5Uptp[RpIndex];
    CpuPcieRpConfig->RootPort[RpIndex].Gen5Dptp                                                             = FspsConfig->CpuPcieRpGen5Dptp[RpIndex];
    CpuPcieRpConfig->RootPort[RpIndex].PcieRpCommonConfig.ClockGating                                       = FspsConfig->CpuPcieClockGating[RpIndex];
    CpuPcieRpConfig->RootPort[RpIndex].PcieRpCommonConfig.PowerGating                                       = FspsConfig->CpuPciePowerGating[RpIndex];
    CpuPcieRpConfig->RootPort2[RpIndex].Func0LinkDisable                                                    = FspsConfig->CpuPcieFunc0LinkDisable [RpIndex];
 }

  //
  // Update PCIE RP policies
  //
  CpuPcieRpConfig->FiaProgramming                      = FspsConfig->CpuPcieFiaProgramming;
  CpuPcieRpConfig->PcieCommonConfig.ComplianceTestMode = FspsConfig->CpuPcieComplianceTestMode;
  CpuPcieRpConfig->SlotSelection                       = FspsConfig->CpuPcieSlotSelection;
#endif
  if (GnaConfig != NULL) {
    GnaConfig->GnaEnable = FspsConfig->GnaEnable;
  }

#if FixedPcdGetBool(PcdVmdEnable) == 1
  //
  // VMD configuration
  //
  if (VmdPeiConfig != NULL) {
    VmdPeiConfig->VmdEnable        = FspsConfig->VmdEnable;
    VmdPeiConfig->VmdGlobalMapping = FspsConfig->VmdGlobalMapping;
    for (int i=0; i < VMD_MAX_DEVICES; ++i) {
      VmdPeiConfig->VmdPortEnable[i].RpEnable    = FspsConfig->VmdPort[i];
      VmdPeiConfig->VmdPortEnable[i].RpDevice    = FspsConfig->VmdPortDev[i];
      VmdPeiConfig->VmdPortEnable[i].RpFunction  = FspsConfig->VmdPortFunc[i];
    }
    VmdPeiConfig->VmdCfgBarSize   = FspsConfig->VmdCfgBarSize;
    VmdPeiConfig->VmdCfgBarAttr   = FspsConfig->VmdCfgBarAttr;
    VmdPeiConfig->VmdMemBarSize1  = FspsConfig->VmdMemBarSize1;
    VmdPeiConfig->VmdMemBar1Attr  = FspsConfig->VmdMemBar1Attr;
    VmdPeiConfig->VmdMemBarSize2  = FspsConfig->VmdMemBarSize2;
    VmdPeiConfig->VmdMemBar2Attr  = FspsConfig->VmdMemBar2Attr;
    VmdPeiConfig->VmdVariablePtr  = (EFI_VMD_OS_DATA*)FspsConfig->VmdVariablePtr;
    VmdPeiConfig->VmdCfgBarBase   = FspsConfig->VmdCfgBarBase;
    VmdPeiConfig->VmdMemBar1Base  = FspsConfig->VmdMemBar1Base;
    VmdPeiConfig->VmdMemBar2Base  = FspsConfig->VmdMemBar2Base;
  }
#endif

#if FixedPcdGetBool(PcdITbtEnable) == 1
  if (TcssPeiConfig != NULL) {
    TcssPeiConfig->IomConfig.IomInterface.UsbOverride        = FspsConfig->UsbOverride;
    TcssPeiConfig->IomConfig.IomInterface.VccSt              = FspsConfig->VccSt;
    TcssPeiConfig->IomConfig.IomInterface.D3HotEnable        = FspsConfig->D3HotEnable;
    TcssPeiConfig->IomConfig.IomInterface.D3ColdEnable       = FspsConfig->D3ColdEnable;
    TcssPeiConfig->IomConfig.TcStateLimit                    = FspsConfig->TcCstateLimit;
    TcssPeiConfig->IomConfig.PmcInterface.PmcPdEnable        = FspsConfig->PmcPdEnable;
    TcssPeiConfig->MiscConfig.IomStayInTCColdSeconds         = FspsConfig->IomStayInTCColdSeconds;
    TcssPeiConfig->MiscConfig.IomBeforeEnteringTCColdSeconds = FspsConfig->IomBeforeEnteringTCColdSeconds;

    TcssPeiConfig->IomConfig.IomAuxPortPad[0].GpioPullN = FspsConfig->IomTypeCPortPadCfg[0];
    TcssPeiConfig->IomConfig.IomAuxPortPad[0].GpioPullP = FspsConfig->IomTypeCPortPadCfg[1];
    TcssPeiConfig->IomConfig.IomAuxPortPad[1].GpioPullN = FspsConfig->IomTypeCPortPadCfg[2];
    TcssPeiConfig->IomConfig.IomAuxPortPad[1].GpioPullP = FspsConfig->IomTypeCPortPadCfg[3];
    TcssPeiConfig->IomConfig.IomAuxPortPad[2].GpioPullN = FspsConfig->IomTypeCPortPadCfg[4];
    TcssPeiConfig->IomConfig.IomAuxPortPad[2].GpioPullP = FspsConfig->IomTypeCPortPadCfg[5];
    TcssPeiConfig->IomConfig.IomAuxPortPad[3].GpioPullN = FspsConfig->IomTypeCPortPadCfg[6];
    TcssPeiConfig->IomConfig.IomAuxPortPad[3].GpioPullP = FspsConfig->IomTypeCPortPadCfg[7];

    TcssPeiConfig->IomConfig.IomOverrides.AuxOri = FspsConfig->TcssAuxOri;
    TcssPeiConfig->IomConfig.IomOverrides.HslOri = FspsConfig->TcssHslOri;

    TcssPeiConfig->UsbConfig.OverCurrentEnable = FspsConfig->PchUsbOverCurrentEnable;
    TcssPeiConfig->UsbConfig.PdoProgramming = FspsUpd->FspsConfig.TcssCpuUsbPdoProgramming;
    for (Index = 0; Index < MAX_TCSS_USB3_PORTS; Index++) {
      TcssPeiConfig->UsbConfig.PortUsb30[Index].OverCurrentPin = FspsConfig->CpuUsb3OverCurrentPin[Index];
      TcssPeiConfig->UsbConfig.PortUsb30[Index].Enable = (FspsConfig->UsbTcPortEn >> Index) & 0x01;
      TcssPeiConfig->MiscConfig.EnableTcssCovTypeA[Index] = FspsConfig->EnableTcssCovTypeA[Index];
      TcssPeiConfig->MiscConfig.MappingPchXhciUsbA[Index] = FspsConfig->MappingPchXhciUsbA[Index];
    }

    //
    // Itbt Pcie Root port Policy update
    //
    for (Index = 0; Index < MAX_ITBT_PCIE_PORT; Index++) {
      if (TcssPeiConfig != NULL) {
        TcssPeiConfig->PciePolicy.PciePortPolicy[Index].PtmEnabled = FspsConfig->PtmEnabled[Index];
        ///
        /// LTR Settings
        ///
        TcssPeiConfig->PciePolicy.PciePortPolicy[Index].LtrEnable                         = FspsConfig->SaPcieItbtRpLtrEnable[Index];
        TcssPeiConfig->PciePolicy.PciePortPolicy[Index].SnoopLatencyOverrideMode          = FspsConfig->SaPcieItbtRpSnoopLatencyOverrideMode[Index];
        TcssPeiConfig->PciePolicy.PciePortPolicy[Index].SnoopLatencyOverrideMultiplier    = FspsConfig->SaPcieItbtRpSnoopLatencyOverrideMultiplier[Index];
        TcssPeiConfig->PciePolicy.PciePortPolicy[Index].SnoopLatencyOverrideValue         = FspsConfig->SaPcieItbtRpSnoopLatencyOverrideValue[Index];
        TcssPeiConfig->PciePolicy.PciePortPolicy[Index].NonSnoopLatencyOverrideMode       = FspsConfig->SaPcieItbtRpNonSnoopLatencyOverrideMode[Index];
        TcssPeiConfig->PciePolicy.PciePortPolicy[Index].NonSnoopLatencyOverrideMultiplier = FspsConfig->SaPcieItbtRpNonSnoopLatencyOverrideMultiplier[Index];
        TcssPeiConfig->PciePolicy.PciePortPolicy[Index].NonSnoopLatencyOverrideValue      = FspsConfig->SaPcieItbtRpNonSnoopLatencyOverrideValue[Index];
        TcssPeiConfig->PciePolicy.PciePortPolicy[Index].ForceLtrOverride                  = FspsConfig->SaPcieItbtRpForceLtrOverride[Index];
        TcssPeiConfig->PciePolicy.PciePortPolicy[Index].LtrConfigLock                     = FspsConfig->SaPcieItbtRpLtrConfigLock[Index];
      }
    }
  }
#endif  // PcdITbtEnable

  //
  // Fusa configuration
  //
  FspUpdateFusaPolicy (SiPolicyPpi, FspsUpd);
  return EFI_SUCCESS;
}
