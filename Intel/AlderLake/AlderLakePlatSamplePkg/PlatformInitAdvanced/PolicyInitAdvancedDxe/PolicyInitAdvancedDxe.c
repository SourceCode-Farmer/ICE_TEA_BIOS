/** @file

;******************************************************************************
;* Copyright (c) 2018, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
**/
/** @file
  This file is a wrapper for Platform Policy Advanced driver. Get Setup
  Value to initialize Intel DXE Platform Policy.

@copyright
  INTEL CONFIDENTIAL
  Copyright 2013 - 2020 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains a 'Sample Driver' and is licensed as such under the terms
  of your license agreement with Intel or your vendor. This file may be modified
  by the user, subject to the additional terms of the license agreement.

@par Specification Reference:
**/

#include "PolicyInitAdvancedDxe.h"
#include <Library/PreSiliconEnvDetectLib.h>

GLOBAL_REMOVE_IF_UNREFERENCED UINT8            mFirmwareConfiguration = 0;

/**
  Initialize  DXE Platform Policy

  @param[in] ImageHandle       Image handle of this driver.
  @param[in] SystemTable       Global system service table.

  @retval EFI_SUCCESS           Initialization complete.
  @exception EFI_UNSUPPORTED    The chipset is unsupported by this driver.
  @retval EFI_OUT_OF_RESOURCES  Do not have enough resources to initialize the driver.
  @retval EFI_DEVICE_ERROR      Device error, driver exits abnormally.
**/
EFI_STATUS
EFIAPI
PolicyInitAdvancedDxeEntryPoint (
  IN EFI_HANDLE       ImageHandle,
  IN EFI_SYSTEM_TABLE *SystemTable
  )
{
  EFI_STATUS           Status;
//[-start-200420-IB17800056-remove]//
//  UINTN                VarDataSize;
//  SETUP_DATA           *SystemConfiguration = NULL;
//[-end-200420-IB17800056-remove]//

  Status = EFI_SUCCESS;

  if (IsSimicsEnvironment()) {
    Status = PcdSetBoolS (PcdResetOnMemoryTypeInformationChange, FALSE);
  } else {
    Status = PcdSetBoolS (PcdResetOnMemoryTypeInformationChange, TRUE);
  }

  if (EFI_ERROR (Status)) {
    DEBUG ((DEBUG_ERROR, "Error: PcdSetBoolS failed @ line %d\n",__LINE__));
    ASSERT_EFI_ERROR (Status);
  } else {
    DEBUG ((DEBUG_INFO, "Info: PcdResetOnMemoryTypeInformationChange is now False\n"));
  }
  
//[-start-200420-IB17800056-remove]//
//  VarDataSize = sizeof (SETUP_DATA);
//
//  SystemConfiguration = (SETUP_DATA *)AllocateZeroPool (VarDataSize);
  
//  mFirmwareConfiguration = 0;
//  if (SystemConfiguration == NULL) {
//    DEBUG ((DEBUG_ERROR, "Fail to allocate memory to get System Configuration!\n"));
//    ASSERT_EFI_ERROR (EFI_OUT_OF_RESOURCES);
//    return EFI_OUT_OF_RESOURCES;
//  } else {
//    Status = gRT->GetVariable (
//                    L"Setup",
//                    &gSystemConfigurationGuid,
//                    NULL,
//                    &VarDataSize,
//                    SystemConfiguration
//                    );
//                    
//    if (EFI_ERROR (Status)) {
//      DEBUG ((DEBUG_ERROR, "Fail to get System Configuration to identify the initial policy mode!\n"));
//    } else {
//      mFirmwareConfiguration = SystemConfiguration->FirmwareConfiguration;
//    }
//  }
//[-end-200420-IB17800056-remove]//
  //
  // SystemAgent Dxe Platform Policy Initialization
  //
  Status = SaPolicyInitDxe (ImageHandle);
  DEBUG ((DEBUG_INFO, "SystemAgent Dxe Platform Policy Initialization status: %r\n", Status));
  ASSERT_EFI_ERROR (Status);

  //
  // PCH Dxe Platform Policy Initialization
  //
  Status = PchPolicyInitDxe (ImageHandle);
  DEBUG ((DEBUG_INFO, "PCH Dxe Platform Policy Initialization status: %r\n", Status));
  ASSERT_EFI_ERROR (Status);

  //
  // ME DXE Policy Initialization
  //
  Status = MePolicyInitDxe (ImageHandle);
  DEBUG ((DEBUG_INFO, "ME DXE Policy Initialization status: %r\n", Status));
  ASSERT_EFI_ERROR (Status);

  //
  // AMT DXE Policy Initialization
  //
#if FixedPcdGetBool (PcdAmtEnable) == 1
  Status = AmtPolicyInitDxe (ImageHandle);
  DEBUG ((DEBUG_INFO, "AMT DXE Policy Initialization status: %r\n", Status));
#endif

  //
  // Silicon Dxe Platform Policy Initialization
  //
  Status = SiliconPolicyInitDxe (ImageHandle);
  DEBUG ((DEBUG_INFO, "Silicon Dxe Platform Policy Initialization status: %r\n", Status));
  ASSERT_EFI_ERROR (Status);

  //
  // CPU DXE Platform Policy Initialization
  //
  Status = CpuPolicyInitDxe (ImageHandle);
  DEBUG ((DEBUG_INFO, "Cpu Dxe Platform Policy Initialization status: %r\n", Status));
  ASSERT_EFI_ERROR (Status);


//[-start-200420-IB17800056-remove]//
  //ADL PO temporary remove
//  if (PcdGetBool(PcdIntelGopEnable)) {
//    //
//    // GOP Dxe Policy Initialization
//    //
//    Status = GopPolicyInitDxe(ImageHandle);
//    DEBUG ((DEBUG_INFO, "GOP Dxe Policy Initialization status: %r\n", Status));
//    ASSERT_EFI_ERROR(Status);
//  }
//[-end-200420-IB17800056-remove]//

  //
  // TBT Dxe Platform Policy Initialization
  //
  Status = TbtPolicyInitDxe (ImageHandle);
  DEBUG((DEBUG_INFO, "TBT Dxe Platform Policy Initialization status: %r\n", Status));
  ASSERT_EFI_ERROR (Status);

  return EFI_SUCCESS;
}
