/** @file
  Platform Setup Routines
;******************************************************************************
;* Copyright (c) 2019, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
**/
#include "SaPolicyInitDxe.h"

#include "Protocol/SaNvsArea.h"

/**
 Get Lid state and update to global NVS area.

 @param        SaGlobalNvsArea

 @retval EFI_SUCCESS            Get Lid state successfully.
 @return Others          Get Lid state failed.

**/
EFI_STATUS
UpdateLidStateToNvs (
  SYSTEM_AGENT_NVS_AREA_PROTOCOL        *SaGlobalNvsArea
  )
{
  EFI_STATUS    Status;
  EFI_STATUS    EcGetLidState;
  BOOLEAN       LidIsOpen;

  EcGetLidState = EFI_SUCCESS;
  LidIsOpen     = TRUE;

  DEBUG_OEM_SVC ((DEBUG_INFO, "Base OemChipsetServices Call: OemSvcEcGetLidState \n"));
  Status = OemSvcEcGetLidState (&EcGetLidState, &LidIsOpen);
  DEBUG_OEM_SVC ((DEBUG_INFO, "Base OemChipsetServices OemSvcEcGetLidState Status: %r\n", Status));
  ASSERT (!EFI_ERROR (EcGetLidState));

  SaGlobalNvsArea->Area->LidState = 1;
  if (!EFI_ERROR (EcGetLidState)) {
    if (!LidIsOpen) {
      //
      // If get lid state form EC successfully and lid is closed.
      //
      SaGlobalNvsArea->Area->LidState = 0;
    }
  } else {
    DEBUG ((DEBUG_INFO | DEBUG_ERROR, "EcGetLidState ERROR in SAPolicy! Status is %r.\n", EcGetLidState));
  }

  return EcGetLidState;
}


/**
 This function updates SYSTEM_AGENT_GLOBAL_NVS_AREA when SA driver finish SA GNVS initialization.

 @retval            None.
*/
VOID
EFIAPI
UpdateSaGlobalNvsCallback (
  IN  EFI_EVENT                Event,
  IN  VOID                     *Context
  )
{
  EFI_STATUS                                   Status;
  SYSTEM_AGENT_NVS_AREA_PROTOCOL               *SaGlobalNvsArea;
//[-start-190606-IB16990032-add]//
  UINT16                                       DeviceIdDataSize;
  UINT32                                       *DeviceIdDataPtr;
  UINT32                                       *DeviceIdPtr;
  UINT16                                       Index;
//[-end-190606-IB16990032-add]//

  Status = gBS->LocateProtocol (&gSaNvsAreaProtocolGuid, NULL, (VOID **)&SaGlobalNvsArea);
  if (EFI_ERROR (Status)) {
    return;
  }
  Status = gBS->CloseEvent (Event);
  ASSERT_EFI_ERROR (Status);

  // SaGlobalNvsArea->Area->DeviceId1             = 0x00000100;           // Device ID 1
  // SaGlobalNvsArea->Area->DeviceId2             = 0x80000410;           // Device ID 2
  // SaGlobalNvsArea->Area->DeviceId3             = 0x80000300;           // Device ID 3
  // SaGlobalNvsArea->Area->DeviceId4             = 0x00000301;           // Device ID 4
  // SaGlobalNvsArea->Area->DeviceId5             = 0x05;                 // Device ID 5
  // SaGlobalNvsArea->Area->NumberOfValidDeviceId = 4;                    // Number of Valid Device IDs
  // SaGlobalNvsArea->Area->CurrentDeviceList     = 0x0F;                 // Default setting
//[-start-190606-IB16990032-add]//
  DeviceIdDataSize = PcdGetSize (PcdSaGlobalNvsDeviceId);

  DeviceIdDataPtr = AllocateZeroPool (DeviceIdDataSize);

  if (DeviceIdDataPtr == NULL) {
    Status = EFI_OUT_OF_RESOURCES;
    ASSERT_EFI_ERROR (Status);
  }

  SaGlobalNvsArea->Area->NumberOfValidDeviceId = (UINT8) DeviceIdDataSize / sizeof(UINT32) - 1;

  CopyMem (DeviceIdDataPtr, PcdGetPtr (PcdSaGlobalNvsDeviceId), DeviceIdDataSize);

  DeviceIdPtr = (UINT32 *) (UINTN)&(SaGlobalNvsArea->Area->DeviceId1);

  for (Index = 0; Index < DeviceIdDataSize / sizeof(UINT32); Index++) {
    *DeviceIdPtr = DeviceIdDataPtr[Index];
    DeviceIdPtr ++;
  }
//[-end-190606-IB16990032-add]//
  Status = UpdateLidStateToNvs (SaGlobalNvsArea);
  if (EFI_ERROR (Status)) {
    DEBUG ((EFI_D_INFO | EFI_D_ERROR, "UpdateLidStateToNvs Error.\n"));
  }
//[-start-190606-IB16990032-add]//
  if (DeviceIdDataPtr != NULL) {
    gBS->FreePool (DeviceIdDataPtr);
  }
//[-end-190606-IB16990032-add]//

}

/**
 This callback function is to update GOP's VBT

 @param [in]   Event            Event whose notification function is being invoked.
 @param [in]   Context          Pointer to the notification function's context.

 @retval None.

**/
VOID
EFIAPI
UpdateVBTCallback (
  IN  EFI_EVENT                Event,
  IN  VOID                     *Context
  )
{
  EFI_STATUS                             Status;
  SA_POLICY_PROTOCOL        *SaPlatformPolicy     = NULL;
  GOP_POLICY_PROTOCOL       *PlatformGopPolicyPtr = NULL;
  GRAPHICS_DXE_CONFIG       *GraphicsDxeConfig = NULL;
  EFI_PHYSICAL_ADDRESS      VbtAddress;
  UINT32                    VbtSize;

  Status = gBS->LocateProtocol (
                  &gSaPolicyProtocolGuid,
                  NULL,
                  (VOID **)&SaPlatformPolicy
                  );
  if (!EFI_ERROR (Status)) {
    Status = GetConfigBlock ((VOID *)SaPlatformPolicy, &gGraphicsDxeConfigGuid, (VOID *)&GraphicsDxeConfig);
    ASSERT_EFI_ERROR (Status);

    Status = gBS->LocateProtocol (
                    &gGopPolicyProtocolGuid,
                    NULL,
                    (VOID **)&PlatformGopPolicyPtr
                    );
    if (!EFI_ERROR (Status)) {
//      PlatformGopPolicyPtr->GetVbtData (&IgdDxeConfig->VbtAddress, &IgdDxeConfig->Size);
      PlatformGopPolicyPtr->GetVbtData (&VbtAddress, &VbtSize);
      GraphicsDxeConfig->VbtAddress                            = VbtAddress;
      GraphicsDxeConfig->Size                                  = VbtSize;
    } else {
      GraphicsDxeConfig->VbtAddress                            = 0x00000000;
      GraphicsDxeConfig->Size                                  = 0;
    }
  }

}

VOID
SaNotifyEventDxe (
  VOID
  )
{
  VOID                *Registration;

  EfiCreateProtocolNotifyEvent (
    &gSaNvsAreaProtocolGuid,
    TPL_CALLBACK,
    UpdateSaGlobalNvsCallback,
    NULL,
    &Registration
  );

//[-start-190722-IB17700055-modify]//
  if (H2OGetBootType () == EFI_BOOT_TYPE) {
//[-end-190722-IB17700055-modify]//
    //
    // Create a event to update VbtAddress.
    //
    EfiCreateProtocolNotifyEvent (
      &gGopPolicyProtocolGuid,
      TPL_CALLBACK,
      UpdateVBTCallback,
      NULL,
      &Registration
    );
  }

  return;
}
