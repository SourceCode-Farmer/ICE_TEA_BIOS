/** @file

;******************************************************************************
;* Copyright (c) 2020, Insyde Software Corporation. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#ifndef _DEVICE_REGISTER_TABLE_H
#define _DEVICE_REGISTER_TABLE_H

#include <PlatformInitAdvancedSmm.h>

#define FILL_PCI_REG_NUM    0x100

//
// PIC
//
#define I8259_CTRL     0x20
#define I8259A_CTRL    0xA0
#define I8259_MASK     0x21
#define I8259A_MASK    0xA1

typedef struct {
  UINTN               RegNum;
  UINT8               PciBus;
  UINT8               PciDev;
  UINT8               PciFun;
  UINT8               *PciRegTable;
  UINT32              *PciRegTableSave;
} SR_DEVICE;

typedef struct {
  UINT8               Bus;
  UINT8               Dev;
  UINT8               Fun;
} P2P_BRIDGE;

typedef struct {
  SR_DEVICE           Device;
  P2P_BRIDGE          P2PB;
} SR_OEM_DEVICE;

typedef struct {
  SR_DEVICE           *Chipset;
  SR_OEM_DEVICE       *Oem;
} SR_TABLE;

//
// Pci device table list
//
extern SR_DEVICE PciDeviceSubResList[];
extern SR_TABLE  PciResTable;
extern SR_DEVICE PciDeviceSubResListUlt[];
extern SR_TABLE  PciResTableUlt;

//
// Edge/Level Control Registers
//
#define ELCR_PORT    0x4D0

extern UINT8 ELCR[];

//
// PM Base IO Register
//
extern UINT8 PMIO_REG[];
extern UINT8 PMIO_REG_SAVE[];

//
// CPU State
//
#define CPU_NUM    8

extern UINT16 CPU_MSR_REG[];
extern UINT64 BSP_MSR_REG_SAVE[];
extern UINT64 AP_MSR_REG_SAVE[][CPU_NUM];

//
// DMA Init
//
#define DMA1_BASE    0
#define DMA2_BASE    0xC0
#define DMA_CMD      8
#define DMA_REQ      9
#define DMA_MASK     0xA
#define DMA_MODE     0xB

extern UINT32 DMIBAR_IO        [];
extern UINT32 DMIBAR_IO_SAVE   [];

//
// AHCI
//
extern UINT32 AHCIBAR_IO_H[];
extern UINT32 AHCIBAR_IO_SAVE_H[];
extern UINT32 AHCIBAR_IO[];
extern UINT32 AHCIBAR_IO_SAVE[];

//
// EP
//
extern UINT32 EPBAR_IO[];
extern UINT32 EPBAR_IO_SAVE[];

//
// HDA
//
extern UINT32 HDA_MMIO[];
extern UINT32 HDA_MMIO_SAVE[];

//
// EDP
//
extern UINT8 EDP_PCI_SAVE[];

//
// SPI
//
extern UINT32 SPI_MMIO[];
extern UINT32 SPI_MMIO_SAVE[];

#endif
