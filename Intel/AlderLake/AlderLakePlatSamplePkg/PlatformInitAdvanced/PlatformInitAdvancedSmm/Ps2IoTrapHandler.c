/**@file

@copyright
  INTEL CONFIDENTIAL
  Copyright 1999 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains a 'Sample Driver' and is licensed as such under the terms
  of your license agreement with Intel or your vendor. This file may be modified
  by the user, subject to the additional terms of the license agreement.

@par Specification Reference:
**/


//
// Module specific Includes
//
#include <Library/DebugLib.h>
#include <Library/SmmServicesTableLib.h>
#include <Library/IoLib.h>
#include <Library/BaseLib.h>
#include <Protocol/SmmIoTrapDispatch2.h>
#include <Protocol/PchSmmIoTrapControl.h>
#include <Protocol/SmmCpu.h>
#include <Library/CpuMailboxLib.h>
#include <Register/SaRegsHostBridge.h>
#include <Library/PciSegmentLib.h>
#include <Library/TimerLib.h>
#include <Protocol/MmMp.h>
#include <Library/SynchronizationLib.h>

#include <Register/Cpuid.h>

#if FixedPcdGetBool (PcdAdlLpSupport) == 0

//
// Global variables
//
GLOBAL_REMOVE_IF_UNREFERENCED EFI_SMM_IO_TRAP_DISPATCH2_PROTOCOL     *mSmmIoTrapDispatch;
GLOBAL_REMOVE_IF_UNREFERENCED PCH_SMM_IO_TRAP_CONTROL_PROTOCOL       *mPchSmmIoTrapControl;
GLOBAL_REMOVE_IF_UNREFERENCED EFI_SMM_IO_TRAP_REGISTER_CONTEXT        mPs2IoTrapContext64;
GLOBAL_REMOVE_IF_UNREFERENCED EFI_HANDLE                              mPs2IoTrapHandle64;

GLOBAL_REMOVE_IF_UNREFERENCED UINT8                             gSmmState; // Track State Machine for SMM IO Trap
GLOBAL_REMOVE_IF_UNREFERENCED UINT8                             gSmmLedState; // Track State Machine for SMM IO Trap
GLOBAL_REMOVE_IF_UNREFERENCED volatile UINT32                   mEfficientCoreCount; //Track the number Efficient Cores Found
GLOBAL_REMOVE_IF_UNREFERENCED volatile UINT32                   mEfficientCoreModuleIndexMap; //Track the CCP Index. Bitwise map. Bits set for all Efficient Core CPP Index. 

#define SMM_MP_DEF_TIMEOUT_US                  000

#define DTT_MAILBOX_INTERFACE_OFFSET      0x5818  // Offset 0x5818 DTT mailbox command/status port
#define DTT_MAILBOX_DATA_OFFSET           0x5810  // Offset 0x5810 DTT mailbox data port
#define MAX_CPP_INDEX                     32      // Maximum number of Bits we can hold in mEfficientCoreModuleIndexMap

/**
  Write to pCode DTT Mailbox to enable/disable core parking

  @param[in] BOOLEAN ParkEnable       - boolean indicating core parlking to be enabled or disabled.

  @retval EFI_SUCCESS         Function successfully executed.
**/
EFI_STATUS DttMailboxCoreParkHint (
  IN BOOLEAN     ParkEnable
  )
{
  UINT32                        MchBar;
  UINT16                        StallCount;
  UINT8                         CPPIndex;

  StallCount = 0;

  MchBar = PciSegmentRead32 (PCI_SEGMENT_LIB_ADDRESS (SA_SEG_NUM, SA_MC_BUS, SA_MC_DEV, SA_MC_FUN, R_SA_MCHBAR)) & ~BIT0;

  //
  // Check for BIT31 (run busy bit) to be cleared before writing
  //
  for (CPPIndex = 0; CPPIndex < MAX_CPP_INDEX; CPPIndex++) {
    if ((mEfficientCoreModuleIndexMap & (1 << CPPIndex)) != 0) {

      while ((MmioRead32(MchBar + DTT_MAILBOX_INTERFACE_OFFSET) & BIT31) && ((StallCount < MAILBOX_WAIT_TIMEOUT))) {
        //
        // Wait for 1us
        //
        MicroSecondDelay(MAILBOX_WAIT_STALL);
        StallCount++;
      }
      if (StallCount >= MAILBOX_WAIT_TIMEOUT) {
        return EFI_TIMEOUT;
      }
      //
      //Set data port with Mailbox data to indicate Core Parking
      //
      if (ParkEnable) {
        MmioWrite32(MchBar + DTT_MAILBOX_DATA_OFFSET, 0xC0000000);
      }
      else {
        MmioWrite32(MchBar + DTT_MAILBOX_DATA_OFFSET, 0x00000000);
      }
      //
      //Set Command Port with Bit[31](trigger execution of command), Bit[23:16](0x0F), Bit[15:8](CCP index-core/module number), Bit[7:0] (0x1b to park)
      //
      MmioWrite32(MchBar + DTT_MAILBOX_INTERFACE_OFFSET, 0x800F001B | (CPPIndex << 8));
    }
  }
  return EFI_SUCCESS;
}

/**
  Traps on writes to IO port 60 and when a write occurs, the funtion will check for the Scroll lock LED command
   and then perform core parking when the LED is ON and remove parking when the LED is OFF.

  @param[in] DispatchHandle       - Handle of this dispatch function
  @param[in] DispatchContext      - Pointer to the dispatch function's context.
                                       The DispatchContext fields are filled in by the dispatching driver
                                       prior to invoke this dispatch function
  @param[in] CommBuffer           - Point to the CommBuffer structure
  @param[in] CommBufferSize       - Point to the Size of CommBuffer structure
**/
VOID
Ps2IoTrapHandler (
  IN EFI_HANDLE                     DispatchHandle,
  IN CONST EFI_SMM_IO_TRAP_CONTEXT  *DispatchContext,
  IN OUT VOID                       *CommBuffer,
  IN OUT UINTN                      *CommBufferSize
)
{
  UINT8                       IoPortValue;
  EFI_STATUS                  Status;

  //
  // write back this port value in the port 60 (get the unmodified port value again)
  //

  IoPortValue = (UINT8)DispatchContext->WriteData;

  Status = mPchSmmIoTrapControl->Pause(
    mPchSmmIoTrapControl,
    mPs2IoTrapHandle64
  );
  DEBUG((DEBUG_INFO, "  SetupPs2IoTrap->Pause returned %r\n", Status));

  gSmst->SmmIo.Io.Write(&gSmst->SmmIo,
    SMM_IO_UINT8,
    0x60,
    1,
    &IoPortValue);
  Status = mPchSmmIoTrapControl->Resume(
    mPchSmmIoTrapControl,
    mPs2IoTrapHandle64
  );
  DEBUG((DEBUG_INFO, "SetupPs2IoTrap->Resume returned %r\n", Status));

  switch (gSmmState) {
    case 0x1:
      //
      // looking for LED command (0xED)
      //
      if (IoPortValue == 0xED) { //0xED SET LED Command
        //
        //Move to State 2 for Next
        //
        gSmmState = 0x2;

      }
      break;
    case 0x2:
      //
      // is it scroll lock on or off?
      //
      IoPortValue = IoPortValue & 0x1;
      if (IoPortValue == 0x1 && gSmmLedState == 0) { //BIT0 is Scroll Lock. Value 0x1 mean to Send pCode Hint
        //
        //Send command to pCode (Core Parking Enabled for Efficient Cores)
        //
        DEBUG((DEBUG_INFO, "Scroll Lock Set ON \n"));
        //
        // enable core parking
        //
        DttMailboxCoreParkHint(TRUE);
        gSmmLedState = 1;
      } else if (IoPortValue == 0x0 && gSmmLedState != 0) {
        //
        //Send command to pCode (Core Parking Disabled for Efficient Cores)
        //
        DEBUG((DEBUG_INFO, "Scroll Lock Set OFF \n"));
        //
        // disable core parking
        //
        DttMailboxCoreParkHint(FALSE);
        gSmmLedState = 0;
      }
      //
      // move state back to waiting on LED command
      //
      gSmmState = 0x1;
      break;
  }

  return;
}

/**
  Get the CPP Inex information for all the Efficient Cores
**/
VOID
GetCppIndexCoreInformation (
  VOID
  )
{
  UINT8    CoreType;
  UINT32   Eax;
  UINT32   Edx;
  UINT32   CppIndex;
  UINT32   Value;

  //
  // Check which is the running core by reading CPUID.(EAX=1AH, ECX=00H):EAX
  //
  AsmCpuid (CPUID_HYBRID_INFORMATION, &Eax, NULL, NULL, NULL);
  CoreType = (UINT8)((Eax & 0xFF000000) >> 24);
  if (CoreType == 0x20) { // Efficient Core
    InterlockedIncrement (&mEfficientCoreCount);
    AsmCpuid (CPUID_THERMAL_POWER_MANAGEMENT, NULL, NULL, NULL, &Edx);
    //
    // Get CCP Index from Edx[31:16]
    //
    CppIndex = ((Edx & 0xFFFF0000) >> 16);
    do {
      Value = mEfficientCoreModuleIndexMap;
    } while (InterlockedCompareExchange32 (
         &mEfficientCoreModuleIndexMap,
         Value,
         Value | (1 << CppIndex)
         ) != Value);
  }
}

/**
  Initialize and register for the IO Trap SMI Handler to deal with Scroll lock presses.

  @retval EFI_SUCCESS         Function successfully executed.
**/
EFI_STATUS
SetupPs2IoTrap (
  VOID
  )
{
  EFI_STATUS                              Status;
  EFI_MM_MP_PROTOCOL                      *mSmmMp;

  //
  // Register SMM IO Trap
  //
  Status = gSmst->SmmLocateProtocol(&gEfiSmmIoTrapDispatch2ProtocolGuid, NULL, (VOID **)&mSmmIoTrapDispatch);
  if (EFI_ERROR(Status)) {
    DEBUG((DEBUG_ERROR, "Locate IoTrap Protocol returned %r\n", Status));
    return Status;
  }

  //
  // Register PS2 IO Trap Handler
  //
  DEBUG ((DEBUG_INFO, "Installing SetupPs2IoTrap Handler \n"));
  mPs2IoTrapContext64.Type         = WriteTrap;//ReadTrap;
  mPs2IoTrapContext64.Length       = 1;
  mPs2IoTrapContext64.Address      = 0x60; //PS2 Keyboard Data Port

  Status = mSmmIoTrapDispatch->Register (
                                mSmmIoTrapDispatch,
                                (EFI_SMM_HANDLER_ENTRY_POINT2)Ps2IoTrapHandler,
                                &mPs2IoTrapContext64,
                                &mPs2IoTrapHandle64
                                );
  DEBUG ((DEBUG_INFO, "  Ps2 IoTrap register Status: %r\n", Status));

  Status = gSmst->SmmLocateProtocol(&gEfiMmMpProtocolGuid, NULL, (VOID**)&mSmmMp);
  if (EFI_ERROR (Status)) {
    ASSERT_EFI_ERROR (Status);
    return Status;
  }

  gSmmState = 0x1; //Start State is 1
  gSmmLedState = 0; // starting LED state is off

  mEfficientCoreCount = 0;
  mEfficientCoreModuleIndexMap = 0;

  GetCppIndexCoreInformation();
  mSmmMp->BroadcastProcedure(
            mSmmMp,
            (EFI_AP_PROCEDURE2)GetCppIndexCoreInformation,
            SMM_MP_DEF_TIMEOUT_US,
            NULL,
            NULL,
            NULL
            );

  DEBUG ((DEBUG_INFO, "  mEfficientCoreModuleIndexMap %x, Efficient Core - %x\n", mEfficientCoreModuleIndexMap, mEfficientCoreCount));
  //
  // Initialize global variable mPchSmmIoTrapControl to pause or resume the trap handler
  //
  Status = gSmst->SmmLocateProtocol (&gPchSmmIoTrapControlGuid, NULL, (VOID **) &mPchSmmIoTrapControl);
  if (EFI_ERROR (Status)) {
    ASSERT_EFI_ERROR (Status);
    return Status;
  }

  return Status;
}
#endif

