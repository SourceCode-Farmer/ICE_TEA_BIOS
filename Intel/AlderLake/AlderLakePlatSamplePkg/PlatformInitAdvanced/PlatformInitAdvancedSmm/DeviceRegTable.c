/** @file

;******************************************************************************
;* Copyright (c) 2020, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#include <DeviceRegTable.h>
#include <CpuRegs.h>
#include <Protocol/Cpu.h>

//INT8 PCH_THERMAL_PCI[] = {
//  0x40,
//  0x3C,
//  };
//UINT32 PCH_THERMAL_SAVE[sizeof (PCH_THERMAL_PCI)];

//
// Pci device table list
//
SR_DEVICE PciDeviceSubResList[] = {
//=======================================================================
//    Register Number | Bus | Dev | Fun | Reg Table | Reg Table Save Area
//=======================================================================
//  { (sizeof (PCH_THERMAL_PCI) / sizeof (UINT8)), 0x00, 0x1F, 0x06, PCH_THERMAL_PCI, PCH_THERMAL_SAVE }, //example
  { 0, 0, 0, 0, 0, 0 },
};

//
// Pci device table list for ULT
//
SR_DEVICE PciDeviceSubResListUlt[] = {
//=======================================================================
//    Register Number | Bus | Dev | Fun | Reg Table | Reg Table Save Area
//=======================================================================
  { 0, 0, 0, 0, 0, 0 },
};

SR_OEM_DEVICE PciDeviceOemSubResList[] = {
//===============================================================================================
//    Register Number | Bus | Dev | Fun | Reg Table | Reg Table Save Area | RpBus | RpDev | RpFun
//===============================================================================================
  { { 0, 0, 0, 0, 0, 0 }, 0, 0, 0 },
};

SR_TABLE PciResTable = {PciDeviceSubResList, PciDeviceOemSubResList};

SR_TABLE PciResTableUlt = {PciDeviceSubResListUlt, PciDeviceOemSubResList};

UINT8 EDP_PCI_SAVE[0x09];

//
// Edge/Level Control Registers
//
UINT8 ELCR[2];

//
// PM Base IO Register
//
UINT8 PMIO_REG[] = {
        0xFF
        };
UINT8 PMIO_REG_SAVE[sizeof (PMIO_REG)];

//
// CPU State
//
UINT16 CPU_MSR_REG[] = {
         0xFFFF
         };
UINT64 BSP_MSR_REG_SAVE[(sizeof (CPU_MSR_REG) / 2)];
UINT64 AP_MSR_REG_SAVE[(sizeof (CPU_MSR_REG) / 2)][CPU_NUM];

//
// DMI
//
UINT32 DMIBAR_IO[] = {
         0xFFFFFFFF
         };
UINT32 DMIBAR_IO_SAVE[sizeof (DMIBAR_IO) / 4];

//
// AHCI
//
UINT32 AHCIBAR_IO_H[] = {
         0xFFFFFFFF
         };
UINT32 AHCIBAR_IO_SAVE_H[sizeof (AHCIBAR_IO_H) / 4];

UINT32 AHCIBAR_IO[] = {
         0xFFFFFFFF
         };
UINT32 AHCIBAR_IO_SAVE[sizeof (AHCIBAR_IO) / 4];

//
// EP
//
UINT32 EPBAR_IO[] = {
         0xFFFFFFFF
         };
UINT32 EPBAR_IO_SAVE[sizeof (EPBAR_IO) / 4];

//
// HDA
//
UINT32 HDA_MMIO[] = {
         0xFFFFFFFF
         };
UINT32 HDA_MMIO_SAVE[sizeof (HDA_MMIO) / 4];

//
// SPI
//
UINT32 SPI_MMIO[] = {
         0xFFFFFFFF
         };
UINT32 SPI_MMIO_SAVE[sizeof (SPI_MMIO) / 4];

