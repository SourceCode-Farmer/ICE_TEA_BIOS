/** @file

;******************************************************************************
;* Copyright (c) 2018 - 2021, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/
/** @file
  Header file for the Platform Advanced SMM Driver.

@copyright
  INTEL CONFIDENTIAL
  Copyright 1999 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains a 'Sample Driver' and is licensed as such under the terms
  of your license agreement with Intel or your vendor. This file may be modified
  by the user, subject to the additional terms of the license agreement.

@par Specification Reference:
**/

#ifndef _PLATFORM_INIT_ADVANCED_SMM_H_
#define _PLATFORM_INIT_ADVANCED_SMM_H_

#include "Platform.h"
#include <SetupVariable.h>
#include <Uefi/UefiBaseType.h>
#include <Library/IoLib.h>
#include <Library/PcdLib.h>
#include <Library/DebugLib.h>
#include <Library/S3BootScriptLib.h>
#include <Library/SmmServicesTableLib.h>
#include <Library/DxeSmmScriptLib/DxeSmmScriptLib.h>
#include <Library/ReportStatusCodeLib.h>
#include <Library/UefiBootServicesTableLib.h>
#include <Library/UefiRuntimeServicesTableLib.h>
#include <Protocol/EcAccess.h>
#include <PlatformBoardType.h>
#include <Protocol/LoadedImage.h>
#include <Protocol/PciRootBridgeIo.h>
#include <Protocol/SmmBase2.h>
#include <Protocol/SmmPowerButtonDispatch2.h>
#include <Protocol/SmmSxDispatch2.h>
#include <Protocol/SmmSwDispatch2.h>
#include <Protocol/SmmIoTrapDispatch2.h>
#include <Protocol/SmmUsbDispatch2.h>
#include <Library/EcMiscLib.h>
#include "Library/DxeSmmAcpiCustomLib.h"
#include <Protocol/PlatformNvsArea.h>
#include <IndustryStandard/Pci30.h>
#include <Protocol/PchEspiSmiDispatch.h>
#include <Library/EcTcssLib.h>
//[-start-200219-IB17800050-add]//
#include <ChipsetSetupConfig.h>
#include <Protocol/OverrideAspm.h>
#include <Library/BaseOemSvcChipsetLib.h>
#include <PchBdfAssignment.h>
#include <ChipsetAccess.h>
#include <Register/UsbRegs.h>
#include <Protocol/HgDataInfo.h>
#include <Library/SmmOemSvcKernelLib.h>
#include <Library/VariableLib.h>

#define PCAT_RTC_ADDRESS_REGISTER       0x70
#define PCAT_RTC_DATA_REGISTER          0x71
#define RTC_ADDRESS_REGISTER_B          0x0B
#define RTC_ADDRESS_REGISTER_D          0x0D
#define ALARM_INTERRUPT_ENABLE          0x20
#define RTC_ADDRESS_SECONDS             0x00
#define RTC_ADDRESS_SECONDS_ALARM       0x01
#define RTC_ADDRESS_MINUTES             0x02
#define RTC_ADDRESS_MINUTES_ALARM       0x03
#define RTC_ADDRESS_HOURS               0x04
#define RTC_ADDRESS_HOURS_ALARM         0x05
//[-end-200219-IB17800050-add]//

//
// Related data structures definition
//
typedef struct _EFI_ACPI_SMM_DEV {
  EFI_PHYSICAL_ADDRESS   RuntimeScriptTableBase;
  UINT32                 BootScriptSaved;
} EFI_ACPI_SMM_DEV;


//
// Callback function prototypes
//
//[-start-200219-IB17800050-modfiy]//
#if 0
EFI_STATUS
EFIAPI
EnableAcpiCallback (
  IN  EFI_HANDLE                    DispatchHandle,
  IN  CONST VOID                    *DispatchContext,
  IN  OUT VOID                      *CommBuffer  OPTIONAL,
  IN  OUT UINTN                     *CommBufferSize  OPTIONAL
  );
#else
EFI_STATUS
EFIAPI
EnableAcpiCallback (
  IN CONST EFI_GUID  *Protocol,
  IN VOID            *Interface,
  IN EFI_HANDLE      Handle
  );
#endif

#if 0
EFI_STATUS
EFIAPI
DisableAcpiCallback (
  IN  EFI_HANDLE                    DispatchHandle,
  IN  CONST VOID                    *DispatchContext,
  IN  OUT VOID                      *CommBuffer  OPTIONAL,
  IN  UINTN                         *CommBufferSize  OPTIONAL
  );
#else
EFI_STATUS
EFIAPI
DisableAcpiCallback (
  IN CONST EFI_GUID  *Protocol,
  IN VOID            *Interface,
  IN EFI_HANDLE      Handle
  );
#endif
//[-end-200219-IB17800050-modify]//

EFI_STATUS
EFIAPI
PowerButtonCallback (
  IN  EFI_HANDLE                              DispatchHandle,
  IN  CONST VOID                              *DispatchContext,
  IN  OUT VOID                                *CommBuffer  OPTIONAL,
  IN  UINTN                                   *CommBufferSize  OPTIONAL
  );

EFI_STATUS
EFIAPI
S3SleepEntryCallBack (
  IN  EFI_HANDLE                              DispatchHandle,
  IN  CONST VOID                              *DispatchContext,
  IN  OUT VOID                                *CommBuffer  OPTIONAL,
  IN  UINTN                                   *CommBufferSize  OPTIONAL
  );

EFI_STATUS
EFIAPI
S4SleepEntryCallBack (
  IN  EFI_HANDLE                              DispatchHandle,
  IN  CONST VOID                              *DispatchContext,
  IN  OUT VOID                                *CommBuffer  OPTIONAL,
  IN  UINTN                                   *CommBufferSize  OPTIONAL
  );

EFI_STATUS
EFIAPI
S5SleepEntryCallBack (
  IN  EFI_HANDLE                              DispatchHandle,
  IN  CONST VOID                              *DispatchContext,
  IN  OUT VOID                                *CommBuffer  OPTIONAL,
  IN  UINTN                                   *CommBufferSize  OPTIONAL
  );

EFI_STATUS
IntelUsb20SmiHandler (
  IN  EFI_HANDLE                              DispatchHandle,
  IN  EFI_SMM_USB_REGISTER_CONTEXT            *DispatchContext
);

VOID
EFIAPI
eSpiEcSmiCallback (
  IN  EFI_HANDLE                              DispatchHandle
  );

VOID
WakeOnRTCS5 (
  VOID
);

#if FixedPcdGetBool (PcdAdlLpSupport) == 0
EFI_STATUS
SetupPs2IoTrap(
  VOID
);
#endif

  
//[-start-200219-IB17800050-add]//
//[-start-210507-IB05660160-modify]//
VOID
SetHgVariable (
  IN PLATFORM_NVS_AREA                  *PlatformNvsAreaPtr,
  IN EFI_HG_NVS_AREA_PROTOCOL           *HGNvsAreaProtocol
  );

VOID
OpenHdAudio (
  IN PLATFORM_NVS_AREA                  *PlatformNvsAreaPtr,
  IN EFI_HG_NVS_AREA_PROTOCOL           *HGNvsAreaProtocol
  );

VOID
CloseHdAudio (
  IN PLATFORM_NVS_AREA                  *PlatformNvsAreaPtr,
  IN EFI_HG_NVS_AREA_PROTOCOL           *HGNvsAreaProtocol
  );
//[-end-210507-IB05660160-modify]//

EFI_STATUS
SaveRestoreState (
  IN BOOLEAN                        SaveRestoreFlag
  );

VOID
SaveCR0 (
  OUT  UINT32                       *Cr0Reg
  );

VOID
ResCR0 (
  IN  UINT32                        Cr0Reg
  );

VOID
EFIAPI
SaveCmosInVariableSmm (
  VOID
  );

VOID
WakeToProcessPendingCapsule (
  IN UINT16                         AcpiBaseAddr,
  IN UINT8                          WakeAfter
  );
//[-end-200219-IB17800050-add]//

//[-start-201030-IB16810136-add]//
/**
  Publishes Swap Address Range Protocol.

  @retval EFI_SUCCESS     The protocol is installed successfully.
  @retval Others          Some error occurs when installing the protocol.

**/
EFI_STATUS
InstallSwapAddressRangeProtocol (
  VOID
  );
//[-end-201030-IB16810136-add]//
#endif

