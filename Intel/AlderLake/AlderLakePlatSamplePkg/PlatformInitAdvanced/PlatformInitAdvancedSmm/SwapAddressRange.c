/** @file
  Provide swap operation of boot block.

;******************************************************************************
;* Copyright (c) 2020, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#include "PlatformInitAdvancedSmm.h"
#include <Library/BaseLib.h>
#include <Library/BaseMemoryLib.h>
#include <Library/DebugLib.h>
#include <Library/TopSwapLib.h>
#include <Protocol/SwapAddressRange.h>
#include <Protocol/SmmBase2.h>

EFI_SWAP_ADDRESS_RANGE_PROTOCOL             mSwapAddressRange;

/**
  This function gets the address range location of
  boot block and backup block.

  @param This             Indicates the calling context.
  @param BootBlockBase    The base address of current boot block.
  @param BootBlockSize    The size (in bytes) of current boot block.
  @param BackupBlockBase  The base address of current backup block.
  @param BackupBlockSize  The size (in bytes) of current backup block.

  @retval EFI_SUCCESS  The call was successful.

**/
EFI_STATUS
EFIAPI
GetRangeLocation (
  IN  EFI_SWAP_ADDRESS_RANGE_PROTOCOL           *This,
  OUT EFI_PHYSICAL_ADDRESS                      *BootBlockBase,
  OUT UINTN                                     *BootBlockSize,
  OUT EFI_PHYSICAL_ADDRESS                      *BackupBlockBase,
  OUT UINTN                                     *BackupBlockSize
  )
{
  *BootBlockBase   = (EFI_PHYSICAL_ADDRESS)PcdGet32 (PcdFlashPbbBase);
  *BootBlockSize   = (UINTN)PcdGet32 (PcdFlashPbbSize);
  *BackupBlockBase = (EFI_PHYSICAL_ADDRESS)PcdGet32 (PcdFlashPbbRBase);
  *BackupBlockSize = (UINTN)PcdGet32 (PcdFlashPbbSize);
  return EFI_SUCCESS;
}

/**
  This service checks if the boot block and backup block has been swapped.

  @param This          Indicates the calling context.
  @param SwapState     True if the boot block and backup block has been swapped.
                       False if the boot block and backup block has not been swapped.

  @retval EFI_SUCCESS  The call was successful.

**/
EFI_STATUS
EFIAPI
GetSwapState (
  IN  EFI_SWAP_ADDRESS_RANGE_PROTOCOL           *This,
  OUT BOOLEAN                                   *SwapState
  )
{

  *SwapState = TopSwapStatus ();
  return EFI_SUCCESS;
}

/**
  This service swaps the boot block and backup block, or swaps them back.

  It also acquires and releases software swap lock during operation. The setting of the new swap state
  is not affected by the old swap state.

  @param This            Indicates the calling context.
  @param NewSwapState    True to swap real boot block and backup block, False to swap them back.

  @retval EFI_SUCCESS  The call was successful.
  @retval EFI_ABORTED  Set swap state error.

**/
EFI_STATUS
EFIAPI
SetSwapState (
  IN EFI_SWAP_ADDRESS_RANGE_PROTOCOL            *This,
  IN BOOLEAN                                    NewSwapState
  )
{
  return TopSwapSet (NewSwapState);
}

/**
  This service checks if a Real Time Clock (RTC) power failure happened.

  If parameter RtcPowerFailed is true after the function returns, RTC power supply failed or was removed.
  It is recommended to check RTC power status before calling GetSwapState().

  @param This             Indicates the calling context.
  @param RtcPowerFailed   True if the RTC (Real Time Clock) power failed or was removed.

  @retval EFI_SUCCESS         The call was successful.
  @retval EFI_UNSUPPORTED     The service is unsupported.

**/
EFI_STATUS
EFIAPI
GetRtcPowerStatus (
  IN  EFI_SWAP_ADDRESS_RANGE_PROTOCOL           *This,
  OUT BOOLEAN                                   *RtcPowerFailed
  )
{
  return EFI_UNSUPPORTED;
}

/**
  This service returns all lock methods for swap operations that the current platform
  supports. Could be software lock, hardware lock, or unsupport lock.
  Note that software and hardware lock methods can be used simultaneously.

  @param This             Indicates the calling context.
  @param LockCapability   The current lock method for swap operations.

  @retval EFI_SUCCESS The call was successful.

**/
EFI_STATUS
EFIAPI
GetSwapLockCapability (
  IN  EFI_SWAP_ADDRESS_RANGE_PROTOCOL           *This,
  OUT EFI_SWAP_LOCK_CAPABILITY                  *LockCapability
  )
{
  *LockCapability = EFI_UNSUPPORT_LOCK;
  return EFI_SUCCESS;
}

/**
  This service is used to acquire or release appointed kind of lock for Swap Address Range operations.

  Note that software and hardware lock mothod can be used simultaneously.

  @param This              Indicates the calling context.
  @param LockCapability    Indicates which lock to acquire or release.
  @param NewLockState      True to acquire lock; False to release lock.

  @retval EFI_SUCCESS The call was successful.

**/
EFI_STATUS
EFIAPI
SetSwapLock (
  IN EFI_SWAP_ADDRESS_RANGE_PROTOCOL            *This,
  IN EFI_SWAP_LOCK_CAPABILITY                   LockCapability,
  IN BOOLEAN                                    NewLockState
  )
{
  return EFI_SUCCESS;
}

/**
  Publishes Swap Address Range Protocol.

  @retval EFI_SUCCESS     The protocol is installed successfully.
  @retval Others          Some error occurs when installing the protocol.

**/
EFI_STATUS
InstallSwapAddressRangeProtocol (
  VOID
  )
{
  EFI_STATUS                       Status;
  EFI_HANDLE                       Handle;

  mSwapAddressRange.GetRangeLocation      = GetRangeLocation;
  mSwapAddressRange.GetRtcPowerStatus     = GetRtcPowerStatus;
  mSwapAddressRange.GetSwapLockCapability = GetSwapLockCapability;
  mSwapAddressRange.GetSwapState          = GetSwapState;
  mSwapAddressRange.SetSwapLock           = SetSwapLock;
  mSwapAddressRange.SetSwapState          = SetSwapState;

  Handle = NULL;
//[-start-201109-IB16810137-modify]//
  Status = gSmst->SmmInstallProtocolInterface (
                   &Handle,
                   &gEfiSmmSwapAddressRangeProtocolGuid,
                   EFI_NATIVE_INTERFACE,
                   &mSwapAddressRange
                   );
//[-end-201109-IB16810137-modify]//
  if (EFI_ERROR (Status)) {
    return Status;
  }

  return EFI_SUCCESS;
}
