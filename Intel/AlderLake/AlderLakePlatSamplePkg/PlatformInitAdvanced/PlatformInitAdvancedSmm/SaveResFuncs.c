/** @file
  Supports functions that saves and restores to the global register table
  information of PIC, KBC, PCI, CpuState, Edge Level, GPIO, and MTRR.

;******************************************************************************
;* Copyright (c) 2018 - 2020, Insyde Software Corporation. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/

#include <ChipsetAccess.h>
#include <CpuDataStruct.h>
#include <CpuRegs.h>
#include <DeviceRegTable.h>
#include <Library/BaseOemSvcChipsetLib.h>
#include <Library/PchInfoLib.h>
#include <Library/PchPciBdfLib.h>
#include <Library/PciSegmentLib.h>
#include <Library/SataLib.h>
#include <Library/SmmOemSvcChipsetLib.h>
#include <Register/Cpuid.h>
#include <Register/PchRegs.h>
#include <Register/SataRegs.h>
#include <Register/SpiRegs.h>
//[-start-200108-IB16740000-modify]// for build error of RC_1014
#include <Register/CommonMsr.h>
//[-end-200108-IB16740000-modify]//
#include <Library/SataSocLib.h>
//[-start-191225-IB16740000-add]//
#include <PchBdfAssignment.h>
#include <Library/PchPciBdfLib.h>
//[-end-191225-IB16740000-add]//
//[-start-200330-IB16740000-add]// for build error (MSR_PLATFORM_INFO -> RKL_MSR_PLATFORM_INFO)
#include <Register/RklMsr.h>
//[-end-200330-IB16740000-add]//

extern  UINT16                          mAcpiBaseAddr;
extern  SA_SETUP                        mSaSetupConfiguration;

/**
  Macro that converts address in PciSegmentLib format to the new address that can be pass
  to the S3 Boot Script Library functions. The Segment is dropped.

  @param Address  Address in PciSegmentLib format.

  @retval New address that can be pass to the S3 Boot Script Library functions.
**/
#define PCI_SEGMENT_LIB_ADDRESS_TO_S3_BOOT_SCRIPT_PCI_ADDRESS(Address) \
          ((((UINT32)(Address) >> 20) & 0xff) << 24) | \
          ((((UINT32)(Address) >> 15) & 0x1f) << 16) | \
          ((((UINT32)(Address) >> 12) & 0x07) <<  8) | \
          LShiftU64 ((Address) & 0xfff, 32)    // Always put Register in high four bytes.

UINT32
GetApicID (
  VOID
  )
{
  CPUID_VERSION_INFO_EBX    Ebx;

  AsmCpuid (CPUID_VERSION_INFO, NULL, &Ebx.Uint32, NULL, NULL);
  return (UINT32)(Ebx.Uint32 >> 24);
}

/**
  This function either writes to or read from PM IO registers.

  @param[in] SaveRestoreFlag    True:  Write data to PM IO.
                                False: Read data from IO to global registers.

  @retval EFI_SUCCESS    If read or write is successful.

**/
EFI_STATUS
SaveRestorePmIo (
  IN  BOOLEAN    SaveRestoreFlag
  )
{
  EFI_STATUS     Status;
  UINTN          Index;

  Status = EFI_SUCCESS;
  Index  = 0;

  while (PMIO_REG[Index] != 0xFF) {
    if (SaveRestoreFlag) {

    } else {
      PMIO_REG_SAVE[Index] = IoRead8 (mAcpiBaseAddr + PMIO_REG[Index]);
      Status = S3BootScriptSaveIoWrite (S3BootScriptWidthUint8, mAcpiBaseAddr + PMIO_REG[Index], 1, (VOID*)(PMIO_REG_SAVE + Index));
      ASSERT_RETURN_ERROR (Status);
    }
    Index++;
  }

  return Status;
}

/**
  This function either writes to or read from PCI registers.

  @param[in] SaveRestoreFlag    True:  Write data to PCI registers.
                                False: Read data from PCI registers to global registers.

  @retval EFI_SUCCESS    If read or write is successful.

**/
EFI_STATUS
SaveRestorePci (
  IN  BOOLEAN      SaveRestoreFlag
  )
{
  EFI_STATUS       Status;
  UINTN            PciRegNum;
  UINT8            *RegTable;
  UINTN            Index;
  UINT8            PciRegister;
  UINT32           *RegTableSave32;
  SR_DEVICE        *SRDevice;
  SR_OEM_DEVICE    *SROemDev;
  UINT8            UpdateBusNum;
  SR_TABLE         *PciResTablePtr;
  SR_OEM_DEVICE    *PciDeviceOemList;
  UINT64           BaseAddress;

  Status           = EFI_SUCCESS;
  PciRegNum        = 0;
  RegTable         = NULL;
  Index            = 0;
  PciRegister      = 0;
  RegTableSave32   = NULL;
  SRDevice         = NULL;
  SROemDev         = NULL;
  UpdateBusNum     = 0;
  PciResTablePtr   = NULL;
  PciDeviceOemList = NULL;
  BaseAddress      = 0;

  if (SaveRestoreFlag) {

  } else {
    if (IsPchH ()) {
      PciResTablePtr = &PciResTable;
    } else {
      PciResTablePtr = &PciResTableUlt;
    }

    DEBUG ((DEBUG_INFO, "Smm OemChipsetServices Call: OemSvcGetSaveRestorePciDeviceOemList \n"));
    Status = OemSvcGetSaveRestorePciDeviceOemList (&PciDeviceOemList);
    DEBUG ((DEBUG_INFO, "Smm OemChipsetServices OemSvcGetSaveRestorePciDeviceOemList Status: %r\n", Status));
    if ((Status == EFI_MEDIA_CHANGED) && (PciDeviceOemList != NULL)) {
      PciResTablePtr->Oem = PciDeviceOemList;
    }

    SROemDev = PciResTablePtr->Oem;
    while (SROemDev->Device.RegNum != 0) {
      if (SROemDev->P2PB.Bus + SROemDev->P2PB.Dev + SROemDev->P2PB.Fun != 0x00) {
        BaseAddress = PCI_SEGMENT_LIB_ADDRESS (
                        0,
                        SROemDev->P2PB.Bus,
                        SROemDev->P2PB.Dev,
                        SROemDev->P2PB.Fun,
                        0
                        );
        UpdateBusNum = PciSegmentRead8 (BaseAddress + (PCI_BRIDGE_PRIMARY_BUS_REGISTER_OFFSET + 1));
        SROemDev->Device.PciBus = UpdateBusNum;
      }
      SROemDev = SROemDev + 1;
    }
    SRDevice = PciResTablePtr->Chipset;
    SROemDev = PciResTablePtr->Oem;
    if (SRDevice->RegNum == 0) {
      SRDevice = (SR_DEVICE*)SROemDev;
      SROemDev = SROemDev + 1;
    }

    while (SRDevice->RegNum != 0) {
      RegTableSave32 = SRDevice->PciRegTableSave;
      BaseAddress = PCI_SEGMENT_LIB_ADDRESS (
                      0,
                      SRDevice->PciBus,
                      SRDevice->PciDev,
                      SRDevice->PciFun,
                      0
                      );
      if (PciSegmentRead32 (BaseAddress) != 0xFFFFFFFF) {
        if (SRDevice->PciRegTable == NULL) {
          //
          // Save/Restore Full Page
          //
          for (PciRegister = 0xFC, Index = 0; Index < FILL_PCI_REG_NUM; PciRegister -= 4, Index += 4) {
            RegTableSave32[Index/4] = PciSegmentRead32 (BaseAddress + PciRegister);
            Status = S3BootScriptSavePciCfg2Write (
                       S3BootScriptWidthUint32,
                       RShiftU64 ((BaseAddress + PciRegister), 32) & 0xffff,
                       PCI_SEGMENT_LIB_ADDRESS_TO_S3_BOOT_SCRIPT_PCI_ADDRESS (BaseAddress + PciRegister),
                       1,
                       (VOID *)(RegTableSave32 + (Index / 4))
                       );
            ASSERT_RETURN_ERROR (Status);
          }
        } else {
          RegTable = SRDevice->PciRegTable;
          PciRegNum = SRDevice->RegNum;

          for (Index = 0; Index < PciRegNum; Index++) {
            RegTableSave32 [Index] = PciSegmentRead32 (BaseAddress + RegTable[Index]);
            Status = S3BootScriptSavePciCfg2Write (
                       S3BootScriptWidthUint32,
                       RShiftU64 ((BaseAddress + RegTable[Index]), 32) & 0xffff,
                       PCI_SEGMENT_LIB_ADDRESS_TO_S3_BOOT_SCRIPT_PCI_ADDRESS (BaseAddress + RegTable[Index]),
                       1,
                       (VOID *)(RegTableSave32 + Index)
                       );
            ASSERT_RETURN_ERROR (Status);
          }
        }
      }

      if (SROemDev == PciResTablePtr->Oem) {
        SRDevice = SRDevice + 1;
      }
      if (SROemDev != PciResTablePtr->Oem || SRDevice->RegNum == 0) {
        SRDevice = (SR_DEVICE*)SROemDev;
        SROemDev = SROemDev + 1;
      }
    }
  }

  return EFI_SUCCESS;
}

/**
  This function either writes to or read from AHCI BAR register.

  @param[in] SaveRestoreFlag    True: write data to AHCI BAR registers.
                                False: read data from AHCI BAR registers to global registers.

  @retval EFI_SUCCESS    If read or write is successful.

**/
EFI_STATUS
SaveRestoreAbar (
  IN  BOOLEAN    SaveRestoreFlag
  )
{
  EFI_STATUS     Status;
  UINTN          Index;
  UINTN          PciSataRegBase;
  UINT32         AhciBar;

  Status         = EFI_SUCCESS;
  Index          = 0;
  PciSataRegBase = PCI_SEGMENT_LIB_ADDRESS (
                     DEFAULT_PCI_SEGMENT_NUMBER_PCH,
                     DEFAULT_PCI_BUS_NUMBER_PCH,
                     SataDevNumber (SATA_1_CONTROLLER_INDEX),
                     SataFuncNumber (SATA_1_CONTROLLER_INDEX),
                     0
                     );
  AhciBar        = 0;

  if (SaveRestoreFlag) {

  } else {
    AhciBar = PciSegmentRead32 (PciSataRegBase + R_SATA_CFG_AHCI_BAR);
    if (IsPchLp()) {
      for (Index = 0; AHCIBAR_IO[Index] != 0xFFFFFFFF; Index++) {
        AHCIBAR_IO_SAVE[Index] = MmioRead32(AhciBar + AHCIBAR_IO[Index]);
        Status = S3BootScriptSaveMemWrite (S3BootScriptWidthUint32, AhciBar + AHCIBAR_IO[Index], 1, (VOID *)(AHCIBAR_IO_SAVE + Index));
        ASSERT_RETURN_ERROR (Status);
      }
    } else {
      for (Index = 0; AHCIBAR_IO_H[Index] != 0xFFFFFFFF; Index++) {
        AHCIBAR_IO_SAVE_H[Index] = MmioRead32(AhciBar + AHCIBAR_IO_H[Index]);
        Status = S3BootScriptSaveMemWrite (S3BootScriptWidthUint32, AhciBar + AHCIBAR_IO_H[Index], 1, (VOID *)(AHCIBAR_IO_SAVE_H + Index));
        ASSERT_RETURN_ERROR (Status);
      }
    }
  }

  return Status;
}

/**
  This function either writes to or read from Pic IO registers.

  @param[in] SaveRestoreFlag    True: Write data to Pic registers.
                                False: Read data from Pic registers to global registers.

  @retval EFI_SUCCESS    If read or write is successful.

**/
EFI_STATUS
SaveRestorePic (
  IN  BOOLEAN    SaveRestoreFlag
  )
{
  EFI_STATUS     Status;
  UINT8          Data;

  Status = EFI_SUCCESS;
  Data   = 0;

  if (SaveRestoreFlag) {

  } else {
    //
    // PIC2
    //
    Data = 0x11;
    Status = S3BootScriptSaveIoWrite (S3BootScriptWidthUint8, I8259A_CTRL, 1, (VOID*)&Data);
    ASSERT_RETURN_ERROR (Status);

    Data = 0x70;
    Status = S3BootScriptSaveIoWrite (S3BootScriptWidthUint8, I8259A_MASK, 1, (VOID*)&Data);
    ASSERT_RETURN_ERROR (Status);

    Data = 0x02;
    Status = S3BootScriptSaveIoWrite (S3BootScriptWidthUint8, I8259A_MASK, 1, (VOID*)&Data);
    ASSERT_RETURN_ERROR (Status);

    Data = 0x01;
    Status = S3BootScriptSaveIoWrite (S3BootScriptWidthUint8, I8259A_MASK, 1, (VOID*)&Data);
    ASSERT_RETURN_ERROR (Status);

    Data = 0;
    Status = S3BootScriptSaveIoWrite (S3BootScriptWidthUint8, I8259A_MASK, 1, (VOID*)&Data);
    ASSERT_RETURN_ERROR (Status);

    //
    // PIC1
    //
    Data = 0x11;
    Status = S3BootScriptSaveIoWrite (S3BootScriptWidthUint8, I8259_CTRL, 1, (VOID*)&Data);
    ASSERT_RETURN_ERROR (Status);

    Data = 0x08;
    Status = S3BootScriptSaveIoWrite (S3BootScriptWidthUint8, I8259_MASK, 1, (VOID*)&Data);
    ASSERT_RETURN_ERROR (Status);

    Data = 0x04;
    Status = S3BootScriptSaveIoWrite (S3BootScriptWidthUint8, I8259_MASK, 1, (VOID*)&Data);
    ASSERT_RETURN_ERROR (Status);

    Data = 0x01;
    Status = S3BootScriptSaveIoWrite (S3BootScriptWidthUint8, I8259_MASK, 1, (VOID*)&Data);
    ASSERT_RETURN_ERROR (Status);

    Data = 0;
    Status = S3BootScriptSaveIoWrite (S3BootScriptWidthUint8, I8259_MASK, 1, (VOID*)&Data);
    ASSERT_RETURN_ERROR (Status);
  }

  return Status;
}

/**
  This function either writes to or read from LevelEdge IO registers.

  @param[in] SaveRestoreFlag    True:  Write data to IO registers.
                                False: Read data from IO registers to global registers.

  @retval EFI_SUCCESS    If read or write is successful.

**/
EFI_STATUS
SaveRestoreLevelEdge (
  IN  BOOLEAN     SaveRestoreFlag
  )
{
  EFI_STATUS      Status;

  Status = EFI_SUCCESS;

  if (SaveRestoreFlag) {

  } else {
    ELCR[0] = IoRead8 (ELCR_PORT);
    Status = S3BootScriptSaveIoWrite (S3BootScriptWidthUint8, ELCR_PORT, 1, (VOID*)ELCR);
    ASSERT_RETURN_ERROR (Status);

    ELCR[1] = IoRead8 (ELCR_PORT + 1);
    Status = S3BootScriptSaveIoWrite (S3BootScriptWidthUint8, ELCR_PORT + 1, 1, (VOID*)(ELCR + 1));
    ASSERT_RETURN_ERROR (Status);
  }

  return Status;
}

/**
  This function either saves or restores CPU state.

  @param[in] SaveRestoreFlag    True: Restores CPU state.
                                False: Save CPU state.

  @retval EFI_SUCCESS    If read or write is successful.

**/
EFI_STATUS
SaveRestoreBSPState (
  IN  BOOLEAN               SaveRestoreFlag
  )
{
  UINT32                    MsrIndex;
  UINT64                    MsrValue;
  UINTN                     Index;
  UINT32                    CpreApicID;
  CPUID_VERSION_INFO_ECX    Ecx;

  MsrIndex   = 0;
  MsrValue   = 0;
  Index      = 0;
  CpreApicID = GetApicID ();
  AsmCpuid (CPUID_VERSION_INFO, NULL, NULL, &Ecx.Uint32, NULL);

  while (CPU_MSR_REG[Index] != 0xFFFF) {
    MsrIndex = (UINT32)CPU_MSR_REG[Index];
    if (SaveRestoreFlag) {
      if (MsrIndex == MSR_FEATURE_CONFIG) {
        if (Ecx.Bits.AESNI == 1) {
          MsrValue = AsmReadMsr64 (MSR_FEATURE_CONFIG);
          if (MsrValue == 0) {
            AsmWriteMsr64 (MsrIndex, BSP_MSR_REG_SAVE[Index]);
          }
        }
      } else {
        //
        // Bootstrap Processor:
        // When MSR restore, we should check PLATFORM_INFO [BIT30] to detect whether "MSR_TEMPERATURE_TARGET" is programmable or not.
        //
        if (MsrIndex != MSR_TEMPERATURE_TARGET || (AsmReadMsr64 (RKL_MSR_PLATFORM_INFO) & BIT30)) {
          AsmWriteMsr64 (MsrIndex, BSP_MSR_REG_SAVE[Index]);
        }
      }
    } else {
      if (MsrIndex == MSR_FEATURE_CONFIG) {
        if (Ecx.Bits.AESNI == 1) {
          BSP_MSR_REG_SAVE[Index] = AsmReadMsr64 (MsrIndex);
        }
      } else {
        BSP_MSR_REG_SAVE[Index] = AsmReadMsr64 (MsrIndex);
      }
    }
    Index++;
  }

  return EFI_SUCCESS;
}

/**
  This function either saves or restores CPU state.

  @param[in] FlagBuffer    True: Restores CPU state.
                           False: Save CPU state.

  @retval EFI_SUCCESS    If read or write is successful.

**/
EFI_STATUS
SaveRestoreAPState (
  IN  VOID                   *FlagBuffer
  )
{
  UINT32                     MsrIndex;
  UINTN                      Index;
  UINT64                     MsrValue;
  UINT32                     CpreApicID;
  CPUID_VERSION_INFO_ECX     Ecx;
  BOOLEAN                    SaveRestoreFlag;
  UINTN                      CurrentCpu;

  MsrIndex   = 0;
  MsrValue   = 0;
  Index      = 0;
  CpreApicID = GetApicID ();
  AsmCpuid (CPUID_VERSION_INFO, NULL, NULL, &Ecx.Uint32, NULL);

  SaveRestoreFlag = *(BOOLEAN *)FlagBuffer;
  CurrentCpu = gSmst->CurrentlyExecutingCpu;

  while (CPU_MSR_REG[Index] != 0xFFFF) {
    MsrIndex = (UINT32)CPU_MSR_REG[Index];
    if (SaveRestoreFlag) {
      if (MsrIndex == MSR_FEATURE_CONFIG) {
        if (Ecx.Bits.AESNI == 1) {
          MsrValue = AsmReadMsr64 (MSR_FEATURE_CONFIG);
          if (MsrValue == 0) {
            if ((CpreApicID == 0x2) || (CpreApicID == 0x4) || (CpreApicID == 0x6)) {
              AsmWriteMsr64 (MsrIndex, AP_MSR_REG_SAVE[Index][CurrentCpu]);
            }
          }
        }
      } else {
        //
        // Application Processor:
        // When MSR restore, we should check PLATFORM_INFO [BIT30] to detect whether "MSR_TEMPERATURE_TARGET" is programmable or not.
        //
        if (MsrIndex != MSR_TEMPERATURE_TARGET || (AsmReadMsr64 (RKL_MSR_PLATFORM_INFO) & BIT30)) {
          AsmWriteMsr64 (MsrIndex, AP_MSR_REG_SAVE[Index][CurrentCpu]);
        }
      }
    } else {
      if (MsrIndex == MSR_FEATURE_CONFIG) {
        if (Ecx.Bits.AESNI == 1) {
          AP_MSR_REG_SAVE[Index][CurrentCpu] = AsmReadMsr64 (MsrIndex);
        }
      } else {
        AP_MSR_REG_SAVE[Index][CurrentCpu] = AsmReadMsr64 (MsrIndex);
      }
    }
    Index++;
  }

  return EFI_SUCCESS;
}

/**
  This function initializes the DMA controller.

  @retval EFI_SUCCESS    Always returned.

**/
EFI_STATUS
DmaInit (
  IN BOOLEAN    SaveRestoreFlag
  )
{
  EFI_STATUS    Status;
  UINTN         Index;
  UINT8         Data;

  Status = EFI_SUCCESS;
  Index  = 0;
  Data   = 0;

  if (SaveRestoreFlag) {

  } else {
    Data = 0;
    Status = S3BootScriptSaveIoWrite (S3BootScriptWidthUint8, DMA1_BASE + DMA_CMD, 1, (VOID*)&Data);
    ASSERT_RETURN_ERROR (Status);

    Data = 0;
    Status = S3BootScriptSaveIoWrite (S3BootScriptWidthUint8, DMA2_BASE + DMA_CMD * 2, 1, (VOID*)&Data);
    ASSERT_RETURN_ERROR (Status);

    for (Index = 0; Index < 4; Index++) {
      Data = (UINT8)(0x40 + Index);

      Status = S3BootScriptSaveIoWrite (S3BootScriptWidthUint8, DMA1_BASE + DMA_CMD, 1, (VOID*)&Data);
      ASSERT_RETURN_ERROR (Status);

      Status = S3BootScriptSaveIoWrite (S3BootScriptWidthUint8, DMA2_BASE + DMA_CMD * 2, 1, (VOID*)&Data);
      ASSERT_RETURN_ERROR (Status);
    }

    Data = 0xC0;
    Status = S3BootScriptSaveIoWrite (S3BootScriptWidthUint8, DMA2_BASE + DMA_MODE * 2, 1, (VOID*)&Data);
    ASSERT_RETURN_ERROR (Status);

    Data = 0;
    Status = S3BootScriptSaveIoWrite (S3BootScriptWidthUint8, DMA2_BASE + DMA_REQ * 2, 1, (VOID*)&Data);
    ASSERT_RETURN_ERROR (Status);

    Data = 0;
    Status = S3BootScriptSaveIoWrite (S3BootScriptWidthUint8, DMA2_BASE + DMA_MASK * 2, 1, (VOID*)&Data);
    ASSERT_RETURN_ERROR (Status);
  }

  return Status;
}

EFI_STATUS
SaveRestoreDmi (
  IN  BOOLEAN    SaveRestoreFlag
  )
{
  EFI_STATUS     Status;
  UINTN          Index;
  UINT64         DmiBar;
  UINT64         BaseAddress;

  Status      = EFI_SUCCESS;
  DmiBar      = 0;
  BaseAddress = PCI_SEGMENT_LIB_ADDRESS (0, SA_MC_BUS, SA_MC_DEV, SA_MC_FUN, 0);

  for (Index = 0; DMIBAR_IO[Index] != 0xFFFFFFFF; Index++) {
    if (SaveRestoreFlag) {

    } else {
      DmiBar = (UINT64)(PciSegmentRead32 (BaseAddress + R_SA_DMIBAR) & ~BIT0);
      DMIBAR_IO_SAVE[Index] = MmioRead32 ((UINTN)(DmiBar + DMIBAR_IO[Index]));
      Status = S3BootScriptSaveMemWrite (S3BootScriptWidthUint32, DmiBar + DMIBAR_IO[Index], 1, (VOID *)(DMIBAR_IO_SAVE + Index));
      ASSERT_RETURN_ERROR (Status);
    }
  }

  return Status;
}

EFI_STATUS
SaveRestoreEp (
  IN  BOOLEAN    SaveRestoreFlag
  )
{
  UINTN          Index;
  UINT64         EgressPortBar;
  EFI_STATUS     Status;

  Status        = EFI_SUCCESS;
  EgressPortBar = PcdGet64 (PcdPciExpressBaseAddress);

  for (Index = 0; EPBAR_IO[Index] != 0xFFFFFFFF; Index++) {
    if (SaveRestoreFlag) {

    } else {
      EPBAR_IO_SAVE[Index] = MmioRead32 ((UINTN)(EgressPortBar + EPBAR_IO[Index]));
      Status = S3BootScriptSaveMemWrite (S3BootScriptWidthUint32, EgressPortBar + EPBAR_IO[Index], 1, (VOID *)(EPBAR_IO_SAVE + Index));
      ASSERT_RETURN_ERROR (Status);
    }
  }

  return Status;
}

EFI_STATUS
SaveRestoreAzalia (
  IN  BOOLEAN    SaveRestoreFlag
  )
{
  EFI_STATUS     Status;
  UINTN          Index;
  UINT64         PciHdaRegBase;

  Status        = EFI_SUCCESS;
  Index         = 0;
  PciHdaRegBase = PCI_SEGMENT_LIB_ADDRESS (
                    DEFAULT_PCI_SEGMENT_NUMBER_PCH,
                    DEFAULT_PCI_BUS_NUMBER_PCH,
                    PCI_DEVICE_NUMBER_PCH_HDA,
                    PCI_FUNCTION_NUMBER_PCH_HDA,
                    0
                    );

  for (Index = 0; HDA_MMIO[Index] != 0xFFFFFFFF; Index++) {
    if (SaveRestoreFlag) {

    } else {
      HDA_MMIO_SAVE[Index] = PciSegmentRead32 (PciHdaRegBase + HDA_MMIO[Index]);
      Status = S3BootScriptSavePciCfg2Write (
                 S3BootScriptWidthUint32,
                 RShiftU64 ((PciHdaRegBase + HDA_MMIO[Index]), 32) & 0xffff,
                 PCI_SEGMENT_LIB_ADDRESS_TO_S3_BOOT_SCRIPT_PCI_ADDRESS (PciHdaRegBase + HDA_MMIO[Index]),
                 1,
                 (VOID *)(HDA_MMIO_SAVE + Index)
                 );
      ASSERT_RETURN_ERROR (Status);
    }
  }

  return Status;
}

EFI_STATUS
SaveEDPReg (
  IN  BOOLEAN    SaveRestoreFlag
  )
{
  EFI_STATUS     Status;
  UINT16         DataOr8;
  UINT16         DataAnd8;
  UINT64         BaseAddress;
  UINT16         Segment;
  UINT64         Address;

  Status      = EFI_SUCCESS;
  DataOr8     = 0;
  DataAnd8    = 0xFF;
  BaseAddress = PCI_SEGMENT_LIB_ADDRESS (0, 0, 1, 0, 0);
  Segment     = 0;
  Address     = 0;

  if (SaveRestoreFlag) {

  } else {
    Segment         = RShiftU64 ((BaseAddress + 0xDFC), 32) & 0xFFFF;
    Address         = PCI_SEGMENT_LIB_ADDRESS_TO_S3_BOOT_SCRIPT_PCI_ADDRESS (BaseAddress + 0xDFC);
    EDP_PCI_SAVE[0] = PciSegmentRead8 (BaseAddress + 0xDFC);
    DataOr8         = EDP_PCI_SAVE[0];
    DataAnd8        = 0xFF;
    Status          = S3BootScriptSavePciCfg2ReadWrite (
                        S3BootScriptWidthUint8,
                        Segment,
                        Address,
                        (VOID*)&DataOr8,
                        (VOID*)&DataAnd8
                        );
    ASSERT_RETURN_ERROR (Status);

    Segment         = RShiftU64 ((BaseAddress + 0xF8A), 32) & 0xFFFF;
    Address         = PCI_SEGMENT_LIB_ADDRESS_TO_S3_BOOT_SCRIPT_PCI_ADDRESS (BaseAddress + 0xF8A);
    EDP_PCI_SAVE[1] = PciSegmentRead8 (BaseAddress + 0xF8A);
    DataOr8         = EDP_PCI_SAVE[1];
    DataAnd8        = 0xFF;
    Status          = S3BootScriptSavePciCfg2ReadWrite (
                        S3BootScriptWidthUint8,
                        Segment,
                        Address,
                        (VOID*)&DataOr8,
                        (VOID*)&DataAnd8
                        );
    ASSERT_RETURN_ERROR (Status);

    Segment         = RShiftU64 ((BaseAddress + 0xF8B), 32) & 0xFFFF;
    Address         = PCI_SEGMENT_LIB_ADDRESS_TO_S3_BOOT_SCRIPT_PCI_ADDRESS (BaseAddress + 0xF8B);
    EDP_PCI_SAVE[2] = PciSegmentRead8 (BaseAddress + 0xF8B);
    DataOr8         = 0;
    DataAnd8        = EDP_PCI_SAVE[2];
    Status          = S3BootScriptSavePciCfg2ReadWrite (
                        S3BootScriptWidthUint8,
                        Segment,
                        Address,
                        (VOID*)&DataOr8,
                        (VOID*)&DataAnd8
                        );
    ASSERT_RETURN_ERROR (Status);

    Segment         = RShiftU64 ((BaseAddress + 0xFAA), 32) & 0xFFFF;
    Address         = PCI_SEGMENT_LIB_ADDRESS_TO_S3_BOOT_SCRIPT_PCI_ADDRESS (BaseAddress + 0xFAA);
    EDP_PCI_SAVE[3] = PciSegmentRead8 (BaseAddress + 0xFAA);
    DataOr8         = EDP_PCI_SAVE[3];
    DataAnd8        = 0xFF;
    Status          = S3BootScriptSavePciCfg2ReadWrite (
                        S3BootScriptWidthUint8,
                        Segment,
                        Address,
                        (VOID*)&DataOr8,
                        (VOID*)&DataAnd8
                        );
    ASSERT_RETURN_ERROR (Status);

    Segment         = RShiftU64 ((BaseAddress + 0xFAB), 32) & 0xFFFF;
    Address         = PCI_SEGMENT_LIB_ADDRESS_TO_S3_BOOT_SCRIPT_PCI_ADDRESS (BaseAddress + 0xFAB);
    EDP_PCI_SAVE[4] = PciSegmentRead8 (BaseAddress + 0xFAB);
    DataOr8         = 0;
    DataAnd8        = EDP_PCI_SAVE[4];
    Status          = S3BootScriptSavePciCfg2ReadWrite (
                        S3BootScriptWidthUint8,
                        Segment,
                        Address,
                        (VOID*)&DataOr8,
                        (VOID*)&DataAnd8
                        );
    ASSERT_RETURN_ERROR (Status);

    Segment         = RShiftU64 ((BaseAddress + 0xFCA), 32) & 0xFFFF;
    Address         = PCI_SEGMENT_LIB_ADDRESS_TO_S3_BOOT_SCRIPT_PCI_ADDRESS (BaseAddress + 0xFCA);
    EDP_PCI_SAVE[5] = PciSegmentRead8 (BaseAddress + 0xFCA);
    DataOr8         = EDP_PCI_SAVE[5];
    DataAnd8        = 0xFF;
    Status          = S3BootScriptSavePciCfg2ReadWrite (
                        S3BootScriptWidthUint8,
                        Segment,
                        Address,
                        (VOID*)&DataOr8,
                        (VOID*)&DataAnd8
                        );
    ASSERT_RETURN_ERROR (Status);

    Segment         = RShiftU64 ((BaseAddress + 0xFCB), 32) & 0xFFFF;
    Address         = PCI_SEGMENT_LIB_ADDRESS_TO_S3_BOOT_SCRIPT_PCI_ADDRESS (BaseAddress + 0xFCB);
    EDP_PCI_SAVE[6] = PciSegmentRead8 (BaseAddress + 0xFCB);
    DataOr8         = 0;
    DataAnd8        = EDP_PCI_SAVE[6];
    Status          = S3BootScriptSavePciCfg2ReadWrite (
                        S3BootScriptWidthUint8,
                        Segment,
                        Address,
                        (VOID*)&DataOr8,
                        (VOID*)&DataAnd8
                        );
    ASSERT_RETURN_ERROR (Status);

    Segment         = RShiftU64 ((BaseAddress + 0xFEA), 32) & 0xFFFF;
    Address         = PCI_SEGMENT_LIB_ADDRESS_TO_S3_BOOT_SCRIPT_PCI_ADDRESS (BaseAddress + 0xFEA);
    EDP_PCI_SAVE[7] = PciSegmentRead8 (BaseAddress + 0xFEA);
    DataOr8         = EDP_PCI_SAVE[7];
    DataAnd8        = 0xFF;
    Status          = S3BootScriptSavePciCfg2ReadWrite (
                        S3BootScriptWidthUint8,
                        Segment,
                        Address,
                        (VOID*)&DataOr8,
                        (VOID*)&DataAnd8
                        );
    ASSERT_RETURN_ERROR (Status);

    Segment         = RShiftU64 ((BaseAddress + 0xFEB), 32) & 0xFFFF;
    Address         = PCI_SEGMENT_LIB_ADDRESS_TO_S3_BOOT_SCRIPT_PCI_ADDRESS (BaseAddress + 0xFEB);
    EDP_PCI_SAVE[8] = PciSegmentRead8 (BaseAddress + 0xFEB);
    DataOr8         = 0;
    DataAnd8        = EDP_PCI_SAVE[8];
    Status          = S3BootScriptSavePciCfg2ReadWrite (
                        S3BootScriptWidthUint8,
                        Segment,
                        Address,
                        (VOID*)&DataOr8,
                        (VOID*)&DataAnd8
                        );
    ASSERT_RETURN_ERROR (Status);
  }

  return Status;
}

/**
  This function save the specific SPI registers and restore it after resume from S3.

  @param[in] SaveRestoreFlag    True: Restore these SPI registers.
                                False: Save these SPI registers.

  @retval EFI_SUCCESS    If save/restore is successful.

**/
EFI_STATUS
SaveRestoreSpiReg (
  IN  BOOLEAN    SaveRestoreFlag
  )
{
  EFI_STATUS     Status;
  UINTN          Index;
  UINTN          PciSpiRegBase;
  UINTN          PchSpiBar0;

  Status        = EFI_SUCCESS;
  Index         = 0;
  PchSpiBar0    = 0;
  PciSpiRegBase = PCI_SEGMENT_LIB_ADDRESS (
                    DEFAULT_PCI_SEGMENT_NUMBER_PCH,
                    DEFAULT_PCI_BUS_NUMBER_PCH,
                    PCI_DEVICE_NUMBER_PCH_SPI,
                    PCI_FUNCTION_NUMBER_PCH_SPI,
                    0
                    );
  //
  // Save SPI Registers for S3 resume usage
  //
  for (Index = 0; SPI_MMIO[Index] != 0xFFFFFFFF; Index++) {
    if (SaveRestoreFlag) {

    } else {
      PchSpiBar0 = PciSegmentRead32 (PciSpiRegBase + R_SPI_CFG_BAR0) &~(B_SPI_CFG_BAR0_MASK);

      //
      // Save these SPI registers
      //
      SPI_MMIO_SAVE[Index] = MmioRead32 (PchSpiBar0 + SPI_MMIO[Index]);

      //
      // Clear HSFS AEL (Hardware Sequencing Flash Status Register - Access Error Log)
      //
      if (SPI_MMIO[Index] == R_SPI_MEM_HSFSC) {
        SPI_MMIO_SAVE[Index] |= B_SPI_MEM_HSFSC_AEL;
      }

      //
      // Set the Vendor Component Lock (VCL) bits
      //
      if (SPI_MMIO[Index] == R_SPI_MEM_SFDP0_VSCC0) {
        SPI_MMIO_SAVE[Index] |= B_SPI_MEM_SFDP0_VSCC0_VCL;
      }

      Status = S3BootScriptSaveMemWrite (S3BootScriptWidthUint32, PchSpiBar0 + SPI_MMIO[Index], 1, (VOID *)(SPI_MMIO_SAVE + Index));
      ASSERT_RETURN_ERROR (Status);
    }
  }

  return Status;
}

/**
  This function either writes to or read from global register table the data of
  GPIO SVID, LevelEdge, PCI, CpuState, MTRR, SIO, KBC, and PIC registers.

  @param[in] SaveRestoreFlag    True: Write data to SMM IO registers.
                                False: Read data from IO to global registers.

  @retval EFI_SUCCESS    If save/restore is successful.

**/
EFI_STATUS
SaveRestoreState (
  IN BOOLEAN    SaveRestoreFlag
  )
{
  EFI_STATUS    Status;
  UINTN         CpuIndex;

  Status   = EFI_SUCCESS;
  CpuIndex = 0;

  if (SaveRestoreFlag) {
    //
    //  For System Security, Suggest write S3 Data by S3BootScript
    //  S3 data will be restored in S3 BootScript Executor
    //
    DEBUG ((DEBUG_WARN, "!! For System Security, Suggest write S3 Data by S3BootScript\n"));
    DEBUG ((DEBUG_WARN, "!! If project restore S3 data in SMM\n"));
    DEBUG ((DEBUG_WARN, "!! The MSR should add in the WhiteListedMsr\n"));
    DEBUG ((DEBUG_WARN, "!! The IO port should add in the WhiteListedIo\n"));
  }

  Status = SaveRestoreBSPState (SaveRestoreFlag);
  if (EFI_ERROR (Status)) {
    return Status;
  }

  Status = SaveRestoreAPState (&SaveRestoreFlag);
  for (CpuIndex = 0; CpuIndex < gSmst->NumberOfCpus; CpuIndex++) {
    Status = gSmst->SmmStartupThisAp (SaveRestoreAPState, CpuIndex, &SaveRestoreFlag);
  }

  if (PcdGetBool (PcdH2OS3SaveRestoreLevelEdgeSupported)) {
    Status = SaveRestoreLevelEdge (SaveRestoreFlag);
    if (EFI_ERROR (Status)) {
      return Status;
    }
  }

  DEBUG_OEM_SVC ((DEBUG_INFO, "Base OemChipsetServices Call: OemSvcEcSaveRestoreKbc \n"));
  Status = OemSvcEcSaveRestoreKbc (SaveRestoreFlag);
  DEBUG_OEM_SVC ((DEBUG_INFO, "Base OemChipsetServices OemSvcEcSaveRestoreKbc Status: %r\n", Status));

  Status = SaveRestorePci (SaveRestoreFlag);
  if (EFI_ERROR (Status)) {
    return Status;
  }

  if (PcdGetBool (PcdH2OS3SaveRestoreEDPRegSupported)) {
    if (mSaSetupConfiguration.ActiveLFP == 3) {
      Status = SaveEDPReg (SaveRestoreFlag);
    }
  }

  Status = SaveRestorePmIo (SaveRestoreFlag);

  Status = SaveRestoreAbar (SaveRestoreFlag);

  if (!SaveRestoreFlag) {
    if (IsPchH()) {
      Status = SaveRestoreDmi (SaveRestoreFlag);
    }
  }

  Status = SaveRestoreEp (SaveRestoreFlag);

  Status = SaveRestoreAzalia (SaveRestoreFlag);

  if (PcdGetBool (PcdH2OS3SaveRestorePicSupported)) {
    Status = SaveRestorePic (SaveRestoreFlag);
    if (EFI_ERROR (Status)) {
      return Status;
    }
  }

  Status = SaveRestoreSpiReg (SaveRestoreFlag);

  if (PcdGetBool (PcdH2OS3DmaInitSupported)) {
    Status = DmaInit (SaveRestoreFlag);
  }

  return Status;
}

