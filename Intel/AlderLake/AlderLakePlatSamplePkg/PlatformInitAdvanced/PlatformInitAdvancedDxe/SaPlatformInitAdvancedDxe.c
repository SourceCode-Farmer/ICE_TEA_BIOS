/** @file
 
;******************************************************************************
;* Copyright 2021 Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corp.
;*
;******************************************************************************
*/
/** @file
  Source code file for the SA Platform Init DXE module

@copyright
  INTEL CONFIDENTIAL
  Copyright 2014 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains a 'Sample Driver' and is licensed as such under the terms
  of your license agreement with Intel or your vendor. This file may be modified
  by the user, subject to the additional terms of the license agreement.

@par Specification Reference:
**/
#include "PlatformInitAdvancedDxe.h"
#include <Protocol/BlockIo.h>
#include <Protocol/GopPolicy.h>
#include <Protocol/SaPolicy.h>
#include <Library/DebugLib.h>
#include <Protocol/Cpu.h>
#include <Protocol/PciEnumerationComplete.h>
//[-start-180316-IB07250307-remove]//
//#include <PlatformNvRamHookLib.h>
//[-end-180316-IB07250307-remove]//
#include <ITbtInfoHob.h>
#include <SaDataHob.h>
#include <Library/GraphicsInfoLib.h>
#include <HostBridgeDataLib.h>
#include <Library/CmosAccessLib.h>
#include <CmosMap.h>
#include <TcssDataHob.h>
//[-start-210527-IB16740147-add]//
#include <Library/CmosLib.h>
#include <ChipsetCmos.h>
//[-end-210527-IB16740147-add]//


GLOBAL_REMOVE_IF_UNREFERENCED EFI_PHYSICAL_ADDRESS                  mAddress;
GLOBAL_REMOVE_IF_UNREFERENCED UINTN                                 mSize;

#define EFI_MAX_ADDRESS       0xFFFFFFFF

EFI_STATUS
EFIAPI
RegisterDisplayDimmPopulationErrMsg (
  VOID
  );

VOID
EFIAPI
DisplayDimmPopulationErrMsg (
  IN EFI_EVENT Event,
  IN VOID      *Context
  );

VOID
UpdateGmAdrRegionToWriteCombine (
  VOID
  );

VOID
EFIAPI
PciEnumCompleteCallback (
  IN EFI_EVENT    Event,
  IN VOID         *Context
  );

/**
  Update Setup variable for keep DMA UUID
**/
VOID
UpdateItbtDmaUuidVariable (
  VOID
  )
{
  UINTN                         VarSize;
  UINT32                        VariableAttr;
  ITBT_INFO_HOB                 *ITbtInfoHobPtr;
  SETUP_DATA                    SystemConfiguration;
  EFI_STATUS                    Status;

  DEBUG ((DEBUG_INFO, "UpdateItbtDmaUuidVariable - Start\n"));
  //
  // Get Setup Data for update ITBT DMA UUID
  //
  VarSize = sizeof (SETUP_DATA);
  Status = gRT->GetVariable (
                  PLATFORM_SETUP_VARIABLE_NAME,
                  &gSetupVariableGuid,
                  &VariableAttr,
                  &VarSize,
                  &SystemConfiguration
                  );
  ASSERT_EFI_ERROR (Status);

  if (EFI_ERROR(Status)){
    DEBUG ((DEBUG_ERROR, "Platform Setup data was not found.\n"));
    return;
  }

  ///
  /// Get ITBT DMA UUID value from HOB of gTcssHobGuid
  ///
  ITbtInfoHobPtr = (ITBT_INFO_HOB *) GetFirstGuidHob (&gITbtInfoHobGuid);

  if (ITbtInfoHobPtr == NULL) {
    DEBUG ((DEBUG_ERROR, "TcssHobPtr is NULL.\n"));
    return;
  }

  ///
  /// Set ITBT DMA UUID to Setup Data variable with updated values
  ///
  Status = gRT->SetVariable (
                  PLATFORM_SETUP_VARIABLE_NAME,
                  &gSetupVariableGuid,
                  VariableAttr,
                  VarSize,
                  &SystemConfiguration
                  );
  ASSERT_EFI_ERROR (Status);
  DEBUG ((DEBUG_INFO, "UpdateItbtDmaUuidVariable - End\n"));
}

/**
  Sync ITBT Variables
**/
VOID
SyncItbtVariables (
  VOID
  )
{
  SA_SETUP                      SaSetup;
  UINTN                         SaSetupVariableSize;
  UINT32                        SaVariableAttributes;
  EFI_STATUS                    Status;
  SETUP_DATA                    SetupData;
  UINTN                         SetupDataVariableSize;
  UINT32                        SetupDataVariableAttributes;
  UINT8                         CmosUsb4CmModeValue;
  UINT8                         EnableSyncSetupData;
  UINT8                         TempITbtRtd3;

  DEBUG ((DEBUG_INFO, "SyncItbtVariables Entry\n"));

  EnableSyncSetupData = 0;

  SetupDataVariableAttributes = 0;
  SetupDataVariableSize = sizeof (SETUP_DATA);
  Status = gRT->GetVariable (
                  PLATFORM_SETUP_VARIABLE_NAME,
                  &gSetupVariableGuid,
                  &SetupDataVariableAttributes,
                  &SetupDataVariableSize,
                  &SetupData);
  if (EFI_ERROR (Status)) {
    DEBUG ((DEBUG_ERROR, "Get Setup variable fail.\n"));
    return;
  }

  SaVariableAttributes = 0;
  SaSetupVariableSize = sizeof (SA_SETUP);
  Status = gRT->GetVariable (
                  L"SaSetup",
                  &gSaSetupVariableGuid,
                  &SaVariableAttributes,
                  &SaSetupVariableSize,
                  &SaSetup
                  );
  if (EFI_ERROR (Status)) {
    DEBUG ((DEBUG_ERROR, "Get SaSetup variable fail.\n"));
    return;
  }

//[-start-210709-IB16740147-modify]//
//  CmosUsb4CmModeValue = CmosRead8 (CMOS_USB4_CM_MODE_REG);
  CmosUsb4CmModeValue = ReadExtCmos8 (R_XCMOS_INDEX, R_XCMOS_DATA, CMOS_USB4_CM_MODE_REG_CHP);
//[-end-210709-IB16740147-modify]//
  if ((SetupData.Usb4CmMode == SETUP_OPTION_USB4_CM_MODE_FW) ||
      ((SetupData.Usb4CmMode == SETUP_OPTION_USB4_CM_MODE_OS) && (CmosUsb4CmModeValue == USB4_CM_MODE_FW_CM))) {
    TempITbtRtd3 = SetupData.ITbtRtd3;
  } else {
    TempITbtRtd3 = 1;
  }

  if (SetupData.ITbtRtd3 != TempITbtRtd3) {
    DEBUG ((DEBUG_INFO, "Sync ITbtRtd3 Variables\n"));
    SetupData.ITbtRtd3 = TempITbtRtd3;
    EnableSyncSetupData = 1;
  }

  if (EnableSyncSetupData == 1) {
    Status = gRT->SetVariable (
                    PLATFORM_SETUP_VARIABLE_NAME,
                    &gSetupVariableGuid,
                    SetupDataVariableAttributes,
                    SetupDataVariableSize,
                    &SetupData);
    if (EFI_ERROR (Status)) {
      DEBUG ((DEBUG_ERROR, "Save SetupData variables fail.\n"));
    }
  }

  //
  // Wake Capability is S3
  //
  if (SetupData.DeepestUSBSleepWakeCapability == 3) {
    if (SaSetup.TcssVccstStatus != 0) {
      DEBUG ((DEBUG_INFO, "Sync TcssIomVccSt Variables\n"));
      SaSetup.TcssVccstStatus = 0;
      Status = gRT->SetVariable (
                      L"SaSetup",
                      &gSaSetupVariableGuid,
                      SaVariableAttributes,
                      SaSetupVariableSize,
                      &SaSetup
                      );
      if (EFI_ERROR (Status)) {
        DEBUG ((DEBUG_ERROR, "Save SaSetup variables fail.\n"));
      }
    }
  }

  DEBUG ((DEBUG_INFO, "SyncItbtVariables Exit\n"));
}

/**
  Updates DIMM slots status for Desktop,server and workstation boards

**/
VOID
UpdateDimmPopulation(
  VOID
  )
{
  if (PcdGetBool (PcdDimmPopulationError)) {
    RegisterDisplayDimmPopulationErrMsg ();
  }
}

/**
  Register DisplayDIMMPopulationErrMsg Notification function on SimpleIn protocol Installation.

  @param[in] SystemTable    Pointer to the System Table

  @retval  EFI_SUCCESS      return EFI_STATUS as success
**/
EFI_STATUS
EFIAPI
RegisterDisplayDimmPopulationErrMsg (
  VOID
  )
{
  EFI_EVENT DimmPopultionErrMsgEvent;
//[-start-180315-IB07250307-remove]//
//  VOID      *Registration;
//[-end-180315-IB07250307-remove]//

  DimmPopultionErrMsgEvent  = NULL;

  //
  // Create event for SimpleInProtocol Callback notification
  //
  gBS->CreateEvent (
        EVT_NOTIFY_SIGNAL,
        TPL_CALLBACK,
        DisplayDimmPopulationErrMsg,
        NULL,
        &DimmPopultionErrMsgEvent
        );
//[-start-180315-IB07250307-remove]//
//  gBS->RegisterProtocolNotify (
//        &gBdsAllDriversConnectedProtocolGuid,
//        DimmPopultionErrMsgEvent,
//        &Registration
//        );
//[-end-180315-IB07250307-remove]//

  return EFI_SUCCESS;
}

/**
  Display DIMM population error message, while Wrong DIMM Population is found.

  @param[in] Event    A pointer to the Event that triggered the callback
  @param[in] Context  A pointer to private data registered with the callback function.

  @retval  EFI_SUCCESS     If Successfully exectuted else returns the error status
**/
VOID
EFIAPI
DisplayDimmPopulationErrMsg (
  IN EFI_EVENT Event,
  IN VOID      *Context
  )
{
  EFI_STATUS                Status;
  EFI_INPUT_KEY             Key;

  DEBUG ((DEBUG_ERROR, "\n DIMMPopulation Error msg\n"));

  if (gST->ConOut != NULL && gST->ConIn != NULL) {
    if (Event != NULL) {
      gBS->CloseEvent (Event);
    }
    //
    // Display the Error Message
    //
    gST->ConOut->OutputString (gST->ConOut, L"");

    gST->ConOut->OutputString (gST->ConOut, L"Error...! Invalid DIMM Population.");

    gST->ConOut->OutputString (gST->ConOut, L"Slot 0 can't be populated when Slot 1 is not populated on the same channel");

    gST->ConOut->OutputString (gST->ConOut, L"Press <Y> to Continue. <N> to Shutdown");

    //
    // Wait for User Input
    //
    do {
      Status = gBS->CheckEvent (gST->ConIn->WaitForKey);
      if (!EFI_ERROR (Status)) {
        Status = gST->ConIn->ReadKeyStroke (gST->ConIn, &Key);
        if (Key.UnicodeChar == L'Y' || Key.UnicodeChar == L'y') {
          DEBUG ((DEBUG_INFO, " Boot Continue\n"));
          break;
        } else if (Key.UnicodeChar == L'N' || Key.UnicodeChar == L'n') {
          DEBUG ((DEBUG_ERROR, " System shutdown\n"));
          gRT->ResetSystem (EfiResetShutdown, EFI_SUCCESS, 0, NULL);
        }
      }
    } while (1);
  }
}


/**
  System Agent platform Initialization

**/
VOID
SaPlatformInitDxe (
  VOID
  )
{

  //
  // Update GT Aperture region memory type as Write Combine.
  //
  UpdateGmAdrRegionToWriteCombine();

#if FixedPcdGetBool (PcdITbtEnable) == 1
    //
    // Save Itbt Dma UUID Data
    //
    UpdateItbtDmaUuidVariable ();
    SyncItbtVariables ();
#endif
}

/**
  This function will update the GT Aperture Region to Write Combining.

**/
VOID
UpdateGmAdrRegionToWriteCombine (
  VOID
  )
{
  VOID                      *Registration;

  DEBUG ((DEBUG_INFO, "Call backs to update GmAdr region as write combine\n"));

  ///
  /// Create PCI Enumeration Completed callback
  ///
  EfiCreateProtocolNotifyEvent (
    &gEfiPciEnumerationCompleteProtocolGuid,
    TPL_NOTIFY,
    PciEnumCompleteCallback,
    NULL,
    &Registration
    );
}

/**
  This function gets registered as a callback to perform SA  functions after PCI enumeration completes.

  @param[in] Event     - A pointer to the Event that triggered the callback.
  @param[in] Context   - A pointer to private data registered with the callback function.
**/
VOID
EFIAPI
PciEnumCompleteCallback (
  IN EFI_EVENT    Event,
  IN VOID         *Context
  )
{
  EFI_STATUS              Status;
  VOID                    *ProtocolPointer;
  UINT32                  Msac;
  UINT64                  McD2BaseAddress;
  UINT64                  GtApertureSize;
  UINT64                  GtApertureAdr;
  EFI_CPU_ARCH_PROTOCOL   *CpuArch;

  DEBUG ((DEBUG_INFO, "SA Platform PciEnumCompleteCallback Start\n"));

  Msac = 0;
  GtApertureAdr = 0;
  GtApertureSize = 0;

  ///
  /// Check if this is first time called by EfiCreateProtocolNotifyEvent() or not,
  /// if it is, we will skip it until real event is triggered
  ///
  Status = gBS->LocateProtocol (&gEfiPciEnumerationCompleteProtocolGuid, NULL, (VOID **) &ProtocolPointer);
  if (EFI_SUCCESS != Status) {
    return;
  }

  gBS->CloseEvent (Event);

  McD2BaseAddress = PCI_SEGMENT_LIB_ADDRESS (SA_SEG_NUM, GetIgdBusNumber(), GetIgdDevNumber(), GetIgdFuncNumber(), 0);
  ///
  /// If device 0:2:0 (Internal Graphics Device, or GT) is not present, skip it.
  ///
  if ((PciSegmentRead16 (McD2BaseAddress + PCI_VENDOR_ID_OFFSET) != 0xFFFF)) {
    Msac = PciSegmentRead32 (McD2BaseAddress + GetIgdMsacOffset());
    GtApertureSize = ((((Msac & (BIT20 | BIT19 | BIT18 | BIT17 | BIT16))) >> 16) + 1) * SIZE_128MB;

    PciSegmentReadBuffer (McD2BaseAddress + GetGmAdrOffset(), sizeof (UINT64), &GtApertureAdr);
    GtApertureAdr &= 0xFFFFFFFFFFFFFFF0;

    Status = gBS->LocateProtocol (&gEfiCpuArchProtocolGuid, NULL, (VOID **) &CpuArch);
    ASSERT_EFI_ERROR (Status);

  //
  // Check Enable Above 4GB MMIO or not
  //
    if (IsAbove4GBMmioEnabled ()) {
      //
      // WORKAROUND for MTRR above 4G issue
      //
      GtApertureSize = SIZE_128GB;
    }

    Status = CpuArch->SetMemoryAttributes (
                        CpuArch,
                        GtApertureAdr,
                        GtApertureSize,
                        EFI_MEMORY_WC
                        );
    if (EFI_SUCCESS != Status) {
      DEBUG((DEBUG_INFO, "Setting GmAdr Memory as WriteCombine failed! Cache the Flash area as UC to release MTRR. \n"));
      Status = CpuArch->SetMemoryAttributes (
                        CpuArch,
                        0xff000000,
                        0x01000000,
                        EFI_MEMORY_UC
                        );
      DEBUG((DEBUG_INFO, "Setting Flash Memory as UnCacheable %r\n", Status));
      ASSERT_EFI_ERROR (Status);
      Status = CpuArch->SetMemoryAttributes (
                        CpuArch,
                        GtApertureAdr,
                        GtApertureSize,
                        EFI_MEMORY_WC
                        );
      DEBUG((DEBUG_INFO, "Setting GmAdr Memory as WriteCombine %r\n", Status));
      ASSERT_EFI_ERROR (Status);
    }
  }

  DEBUG ((DEBUG_INFO, "SA Platform PciEnumCompleteCallback End\n"));

  return;
}
