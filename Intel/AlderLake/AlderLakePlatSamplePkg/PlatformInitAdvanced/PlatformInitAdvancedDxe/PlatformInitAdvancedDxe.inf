## @file
#  Component description file for Platform Policy Initialization driver
#
#******************************************************************************
#* Copyright (c) 2014 - 2020, Insyde Software Corporation. All Rights Reserved.
#*
#* You may not reproduce, distribute, publish, display, perform, modify, adapt,
#* transmit, broadcast, present, recite, release, license or otherwise exploit
#* any part of this publication in any form, by any means, without the prior
#* written permission of Insyde Software Corporation.
#*
#******************************************************************************
### @file
# Component information file for the Platform Init Advanced DXE module.
#
#@copyright
#  INTEL CONFIDENTIAL
#  Copyright 2014 - 2021 Intel Corporation.
#
#  The source code contained or described herein and all documents related to the
#  source code ("Material") are owned by Intel Corporation or its suppliers or
#  licensors. Title to the Material remains with Intel Corporation or its suppliers
#  and licensors. The Material may contain trade secrets and proprietary and
#  confidential information of Intel Corporation and its suppliers and licensors,
#  and is protected by worldwide copyright and trade secret laws and treaty
#  provisions. No part of the Material may be used, copied, reproduced, modified,
#  published, uploaded, posted, transmitted, distributed, or disclosed in any way
#  without Intel's prior express written permission.
#
#  No license under any patent, copyright, trade secret or other intellectual
#  property right is granted to or conferred upon you by disclosure or delivery
#  of the Materials, either expressly, by implication, inducement, estoppel or
#  otherwise. Any license under such intellectual property rights must be
#  express and approved by Intel in writing.
#
#  Unless otherwise agreed by Intel in writing, you may not remove or alter
#  this notice or any other notice embedded in Materials by Intel or
#  Intel's suppliers or licensors in any way.
#
#  This file contains a 'Sample Driver' and is licensed as such under the terms
#  of your license agreement with Intel or your vendor. This file may be modified
#  by the user, subject to the additional terms of the license agreement.
#
# @par Specification Reference:
#
# @par Glossary:
###

[Defines]
  INF_VERSION                    = 0x00010017
  BASE_NAME                      = PlatformInitAdvancedDxe
  FILE_GUID                      = C5046EFD-7BC3-4206-987C-32DA45026E6D
  VERSION_STRING                 = 1.0
  MODULE_TYPE                    = DXE_DRIVER
  ENTRY_POINT                    = PlatformInitAdvancedDxeEntryPoint

[LibraryClasses]
  UefiDriverEntryPoint
  UefiRuntimeServicesTableLib
  UefiBootServicesTableLib
  IoLib
  HobLib
  UefiLib
  DebugLib
  GpioLib
  PmcLib
  BaseMemoryLib
  MemoryAllocationLib
  S3BootScriptLib
#[-start-180316-IB07250307-remove]#
#  PlatformNvRamHookLib
#[-end-180316-IB07250307-remove]#
  CpuPlatformLib
  ConfigBlockLib
  Tpm2CommandLib
  BootGuardLib
#[-start-180316-IB07250307-remove]#
#  BootGuardRevocationLib
#[-end-180316-IB07250307-remove]#
#[-start-180316-IB07250307-remove]#
#  DxeSmbiosProcessorLib
#  DxeFirmwareVersionInfoLib
#  DxeSmbiosDataHobLib
#[-end-180316-IB07250307-remove]#
  PostCodeLib
  PciSegmentLib
  TpmMeasurementLib
  MtrrLib
  SiMtrrLib
  GraphicsInfoLib
  HostBridgeDataLib
  DxeTelemetryAcpiLib
  CpuPcieRpLib
  HostBridgeDataLib
  DxeTbtSecurityLib
  DxeServicesTableLib
  DccProgramLib
  ReportStatusCodeLib
#[-start-180314-IB07250307-add]#
  DxeMeLib
  DxeInsydeChipsetLib
  DxeOemSvcChipsetLibDefault
  PttHeciLib
#[-end-180314-IB07250307-add]#
#[-start-210528-IB16740147-add]#
  CmosLib
#[-end-210528-IB16740147-add]#


[Packages]
  MdePkg/MdePkg.dec
  MdeModulePkg/MdeModulePkg.dec
  UefiCpuPkg/UefiCpuPkg.dec
  AlderLakePlatSamplePkg/PlatformPkg.dec
  ClientOneSiliconPkg/SiPkg.dec
  SecurityPkg/SecurityPkg.dec
  AlderLakeBoardPkg/BoardPkg.dec
#[-start-180314-IB07250307-add]#
  $(PROJECT_PKG)/Project.dec
  $(CHIPSET_PKG)/$(CHIPSET_PKG).dec
  $(CHIPSET_REF_CODE_PKG)/$(CHIPSET_REF_CODE_DEC_NAME).dec
  InsydeModulePkg/InsydeModulePkg.dec
  IntelSiliconPkg/IntelSiliconPkg.dec
#[-end-180314-IB07250307-add]#

#[-start-180314-IB07250307-add]#
[FeaturePcd]
  gChipsetPkgTokenSpaceGuid.PcdMemSpdProtectionSupported
#[-start-181129-IB15410203-add]#
  gInsydeTokenSpaceGuid.PcdH2OBdsCpRecoveryCompleteSupported
#[-end-181129-IB15410203-add]#
#[-end-180314-IB07250307-add]#
#[-start-190412-IB16990024-add]#
  gChipsetPkgTokenSpaceGuid.PcdH2OPreferDtpmBootSupported
#[-end-190412-IB16990024-add]#

[Pcd]
  gBoardModuleTokenSpaceGuid.PcdDDISelection
  gEfiSecurityPkgTokenSpaceGuid.PcdTpmInstanceGuid                 ## CONSUMES
  gPlatformModuleTokenSpaceGuid.PcdSinitAcmBinEnable
  gBoardModuleTokenSpaceGuid.PcdDimmPopulationError
  gPlatformModuleTokenSpaceGuid.PcdDTbtEnable
  gSiPkgTokenSpaceGuid.PcdBtgTxtLegacyPkgEnable                    ## CONSUMES
  gEfiMdePkgTokenSpaceGuid.PcdPciExpressBaseAddress                ## CONSUMES
  gSiPkgTokenSpaceGuid.PcdPciExpressRegionLength                   ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdBoardId                            ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdDiscreteClockOcSupport             ## CONSUMES

  # TCSS iTBT PCD
  gSiPkgTokenSpaceGuid.PcdITbtEnable                               ## CONSUMES
  gPlatformModuleTokenSpaceGuid.PcdExtendedBiosRegionSupport       ## CONSUMES
#[-start-190412-IB16990024-add]#
  gChipsetPkgTokenSpaceGuid.PcdSetupConfigSize
#[-end-190412-IB16990024-add]#
#[-start-180314-IB07250307-add]#
  gInsydeTokenSpaceGuid.PcdPlatformKeyList
  gInsydeTokenSpaceGuid.PcdTpmHide
  gBoardModuleTokenSpaceGuid.PcdEcPresent                         ## CONSUMES
#[-end-180314-IB07250307-add]#

[Sources]
#[-start-180316-IB07250307-modify]#
  PlatformInitAdvancedDxe.c
  PlatformInitAdvancedDxe.h
  SaPlatformInitAdvancedDxe.c
#  GfxInt15Interface.c
  KeyboardSetup.c
  UsbLegacy.c
  UsbLegacy.h
#[-end-180316-IB07250307-modify]#

[Protocols]
  gCpuInfoProtocolGuid                          ## CONSUMES
  gEdkiiVariableLockProtocolGuid                ## CONSUMES
  gSaPolicyProtocolGuid                         ## CONSUMES
  gGopPolicyProtocolGuid                        ## CONSUMES
  gEfiFirmwareVolume2ProtocolGuid               ## CONSUMES
  gEfiPciEnumerationCompleteProtocolGuid        ## CONSUMES
  gEfiCpuArchProtocolGuid                       ## CONSUMES
  gEfiResetNotificationProtocolGuid             ## CONSUMES
  gEfiMpServiceProtocolGuid                     ## CONSUMES
  gPlatformNvsAreaProtocolGuid                  ## CONSUMES
#[-start-180314-IB07250307-add]#
  gPlatformNvsAreaProtocolGuid                         # CONSUMED
  gEfiPciRootBridgeIoProtocolGuid                      # CONSUMED
  gSaPolicyProtocolGuid                                # CONSUMED
  gEfiSmmSmbusProtocolGuid                             # CONSUMED
  gEfiPs2PolicyProtocolGuid                            # PRODUCED
  gEfiUsbLegacyPlatformProtocolGuid                    # PRODUCED
  gCpuInfoProtocolGuid                                 # CONSUMED
#[-end-180314-IB07250307-add]#
#[-start-190412-IB16990024-add]#
  gEfiSetupUtilityProtocolGuid                         # CONSUMED
#[-end-190412-IB16990024-add]#

[Guids]
  gEfiEndOfDxeEventGroupGuid                    ## CONSUMES
  gSetupVariableGuid                            ## CONSUMES
  gSaSetupVariableGuid                          ## CONSUMES
  gCpuSetupVariableGuid                         ## CONSUMES
  gGraphicsDxeConfigGuid                        ## CONSUMES
  gVtdDxeConfigGuid                             ## CONSUMES
  gEfiSecureBootEnableDisableGuid               ## CONSUMES
  gEfiTpmDeviceInstanceNoneGuid                 ## CONSUMES
  gEfiTpmDeviceInstanceTpm12Guid                ## CONSUMES
  gTxtInfoHobGuid                               ## CONSUMES
  gSinitModuleGuid                              ## CONSUMES
  gITbtInfoHobGuid                              ## CONSUMES
  gSaDataHobGuid                                ## CONSUMES
  gSetupVariableGuid                            ## CONSUMES
  gCpuFspErrorTypeCallerId                      ## CONSUMES
#[-start-180314-IB07250307-add]#
  gEfiGenericVariableGuid
  gEfiEndOfDxeEventGroupGuid
  gSystemConfigurationGuid
  gMemoryDxeConfigGuid
  gMeSetupVariableGuid
#[-end-180314-IB07250307-add]#
#[-start-190412-IB16990024-add]#
  gEfiTpmDeviceInstanceTpm20DtpmGuid
#[-end-190412-IB16990024-add]#
#[-start-181129-IB15410203-add]#
  gPchGlobalResetGuid
  gH2OBdsCpRecoveryCompleteGuid
#[-end-181129-IB15410203-add]#
#[-start-181205-IB15410208-add]#
  gSgxSetupVariableGuid
#[-end-181205-IB15410208-add]#
#[-start-190416-IB16990026-add]#
  gMePolicyHobGuid
#[-end-190416-IB16990026-add]#
#[-start-200121-IB10189023-add]#
  gEfiTpmDeviceInstanceTcmGuid
#[-end-200121-IB10189023-add]#

[FixedPcd]

[Depex]
  gCpuInfoProtocolGuid                AND
  gEfiVariableArchProtocolGuid        AND
  gEfiVariableWriteArchProtocolGuid   AND
#[-start-180315-IB07250307-modify]#
#  gDxePolicyProtocolGuid
  gEfiPciRootBridgeIoProtocolGuid     AND
  gSaPolicyProtocolGuid               AND
  gEfiHiiDatabaseProtocolGuid         AND
  gWdtProtocolGuid
#[-end-180315-IB07250307-modify]#

