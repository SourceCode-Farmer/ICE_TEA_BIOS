/** @file

;******************************************************************************
;* Copyright (c) 2014 - 2021, Insyde Software Corporation. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/
/** @file
  Source code file for the Platform Init Advanced DXE module

@copyright
  INTEL CONFIDENTIAL
  Copyright 2014 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains a 'Sample Driver' and is licensed as such under the terms
  of your license agreement with Intel or your vendor. This file may be modified
  by the user, subject to the additional terms of the license agreement.

@par Specification Reference:
**/
#include "PlatformInitAdvancedDxe.h"
#include <CpuRegs.h>
#include <Guid/EventGroup.h>
#include <Library/BootGuardLibVer1.h>
#include <Protocol/CpuInfo.h>
#include <Protocol/PlatformNvsArea.h>
#include <Protocol/VariableLock.h>
#include <Library/CpuPlatformLib.h>
//[-start-180315-IB07250307-remove]//
//#include <Library/SmbiosProcessorLib.h>
//[-end-180315-IB07250307-remove]//
#include <Guid/TpmInstance.h>
#include <Library/Tpm2CommandLib.h>
#include <Library/PostCodeLib.h>
//[-start-180315-IB07250307-remove]//
//#include <PlatformPostCode.h>
//[-end-180315-IB07250307-remove]//
#include <Register/Cpuid.h>
#include <Register/PmcRegs.h>
#include <Protocol/Tcg2Protocol.h>
#include <Library/TpmMeasurementLib.h>
#include <Library/UefiLib.h>
#include <Protocol/ResetNotification.h>
#if FixedPcdGetBool (PcdDTbtEnable) == 1
#include <DTbtInfoHob.h>
#include <Library/DxeTbtSecurityLib.h>
#endif
#include <Library/CpuPcieRpLib.h>
#include <Register/CommonMsr.h>
#include <Register/AdlMsr.h>
#include <Library/DxeServicesTableLib.h>
#include <Library/MtrrLib.h>
#include <Library/SiMtrrLib.h>
#include <Protocol/MpService.h>
#include <PlatformBoardId.h>
#include <Library/DccProgramLib.h>
#include <Pi/PiStatusCode.h>
#include <Library/ReportStatusCodeLib.h>
//[-start-181129-IB15410203-add]//
#include <Guid/H2OCp.h>
#include <Library/H2OCpLib.h>
//[-end-181129-IB15410203-add]//
//[-start-190416-IB16990026-add]//
#include <Library/ConfigBlockLib.h>
#include <MePeiConfig.h>
//[-end-190416-IB16990026-add]//
//[-start-190503-IB17700016-add]//
#include <Register/PttPtpRegs.h>
//[-end-190503-IB17700016-add]//
#include <CpuInitDataHob.h>


GLOBAL_REMOVE_IF_UNREFERENCED EFI_EVENT                         EndOfDxeEvent;

//[-start-180315-IB07250307-modify]//
GLOBAL_REMOVE_IF_UNREFERENCED PLATFORM_NVS_AREA_PROTOCOL        mPlatformNvsAreaProtocol = {0};
GLOBAL_REMOVE_IF_UNREFERENCED EFI_PCI_ROOT_BRIDGE_IO_PROTOCOL   *mPciRootBridgeIo;
GLOBAL_REMOVE_IF_UNREFERENCED EFI_PS2_POLICY_PROTOCOL           mPs2PolicyData = {
  EFI_KEYBOARD_NUMLOCK,
  (EFI_PS2_INIT_HARDWARE) Ps2InitHardware
};
//[-end-180315-IB07250307-modify]//
GLOBAL_REMOVE_IF_UNREFERENCED PLATFORM_NVS_AREA                 *mPlatformNvsAreaPtr;
VOID
EFIAPI
PlatformResetNotificationCallback (
  IN EFI_RESET_TYPE           ResetType,
  IN EFI_STATUS               ResetStatus,
  IN UINTN                    DataSize,
  IN VOID                     *ResetData OPTIONAL
  );

/**
  This function handles PlatformInit task at the end of DXE

  @param[in]  Event     The Event this notify function registered to.
  @param[in]  Context   Pointer to the context data registered to the
                        Event.
**/
VOID
EFIAPI
EndofDxeCallback (
  IN EFI_EVENT Event,
  IN VOID      *Context
  )
{
  MSR_ANC_SACM_INFO_REGISTER    AncSacmInfoMsr;
  static BOOLEAN                S3DataSaved = FALSE;
  UINT16                        ABase;
  UINT32                        SmiEn;
  UINT16                        Pm1Sts;

  gBS->CloseEvent (Event);

  if (S3DataSaved) {
    return ;
  }

  ABase = PmcGetAcpiBase ();
  SmiEn = IoRead32 (ABase + R_ACPI_IO_SMI_EN);
  //
  // Disabled Legacy USB Logic that generates SMI, during S3 Resume.
  //
  SmiEn &= ~(B_ACPI_IO_SMI_EN_LEGACY_USB | B_ACPI_IO_SMI_EN_LEGACY_USB2 | B_ACPI_IO_SMI_EN_PERIODIC | B_ACPI_IO_SMI_EN_SWSMI_TMR);

  S3BootScriptSaveIoWrite (
    S3BootScriptWidthUint32,
    (UINTN) (ABase + R_ACPI_IO_SMI_EN),
    1,
    &SmiEn
    );
  //
  // Clear bust master status bit on S3 resume
  //
  Pm1Sts = B_ACPI_IO_PM1_STS_BM;

  S3BootScriptSaveIoWrite (
    S3BootScriptWidthUint16,
    (UINTN) (ABase + R_ACPI_IO_PM1_STS),
    1,
    &Pm1Sts
    );

  S3DataSaved = TRUE;


  ///
  /// Verify if Boot Guard is supported
  ///
  if (IsBootGuardSupported ()) {
    AncSacmInfoMsr.Uint64 = AsmReadMsr64 (MSR_ANC_SACM_INFO);
    ///
    /// Identify if Revocation is requested by Boot Guard ACM
    ///
    if (AncSacmInfoMsr.Bits.SacmData & BIT7) {
//[-start-180315-IB07250307-remove]//
//      BootGuardOemRevocationHook();
//[-end-180315-IB07250307-remove]//
    }
  }

}

/**
  Registers callback for.PlatformInitEndOfDxe

**/
VOID
RegisterEndOfDxeCallbacks (
  VOID
  )
{
  EFI_STATUS                     Status;

  ///
  /// Performing PlatformInitEndOfDxe after the gEfiEndOfDxeEventGroup is signaled.
  ///
  Status = gBS->CreateEventEx (
                  EVT_NOTIFY_SIGNAL,
                  TPL_CALLBACK,
                  EndofDxeCallback,
                  NULL,
                  &gEfiEndOfDxeEventGroupGuid,
                  &EndOfDxeEvent
                  );
  ASSERT_EFI_ERROR (Status);

}

//[-start-190503-IB17700016-add]//
/**
  Check if PTT is enabled.

  @retval TRUE  PTT enabled
  @retval FALSE PTT not enabled
**/
BOOLEAN
IsPttEnabled (
  VOID
  )
{
  UINT32                         TpmStsFtif;

  TpmStsFtif = MmioRead32 (R_PTT_TXT_STS_FTIF);
  if ((TpmStsFtif & V_FTIF_FTPM_PRESENT) == V_FTIF_FTPM_PRESENT) {
    DEBUG ((DEBUG_INFO, "IsPttEnabled - PTT is enabled\n"));
    return TRUE;
  }
  return FALSE;
}
//[-end-190503-IB17700016-add]//
//[-start-190503-IB17700016-modify]//
/**
  SetupUtility Protocol Installation Event notification handler.
  Disable Setup->PTTEnable.

  @param[in]  Event     Event whose notification function is being invoked
  @param[in]  Context   Pointer to the notification function's context

**/
VOID
EFIAPI
SetupUtilityProtocolInstallationEvent (
  IN      EFI_EVENT                 Event,
  IN      VOID                      *Context
  )
{
  EFI_STATUS                        Status;
  VOID                              *Interface;
  CHIPSET_CONFIGURATION             *SetupVariable;

  Status = gBS->LocateProtocol (&gEfiSetupUtilityProtocolGuid, NULL, (VOID **)&Interface);
  if (EFI_ERROR (Status)) {
    return;
  }

  gBS->CloseEvent (Event);

  SetupVariable = NULL;
  SetupVariable = CommonGetVariableData (SETUP_VARIABLE_NAME, &gSystemConfigurationGuid);
  ASSERT (SetupVariable != NULL);

  if (SetupVariable != NULL && SetupVariable->PTTEnable != 0) {
    //
    // Disable PTT by design.
    //
    SetupVariable->PTTEnable = 0;
    Status = SetVariableToSensitiveVariable (
               SETUP_VARIABLE_NAME,
               &gSystemConfigurationGuid,
               EFI_VARIABLE_NON_VOLATILE | EFI_VARIABLE_BOOTSERVICE_ACCESS | EFI_VARIABLE_RUNTIME_ACCESS,
               PcdGet32 (PcdSetupConfigSize),
               SetupVariable
               );
    DEBUG ((EFI_D_INFO, "EstablishDtpm: Disable PTT by design. Set Setup Variable (SetupVariable->PTTEnable = 0) -%r\n", Status));
    ASSERT_EFI_ERROR (Status);
  }

  gBS->FreePool (SetupVariable);
}
/**
  Disable PTT in order to detect DTPM.

  @param[in,out] SetupVariable   Pointer to Setup variable.

  @retval EFI_SUCCESS            Operation completed successfully
  @retval Others                 Operation failed
**/
STATIC
EFI_STATUS
EstablishDtpm (
  IN OUT CHIPSET_CONFIGURATION      *SetupVariable
  )
{
  EFI_STATUS                        Status;
  BOOLEAN                           PttState;
  PCH_RESET_DATA                    ResetData;
  BOOLEAN                           PttCapability;
  VOID                              *Registration;

  Status = PttHeciGetCapability (&PttCapability);
  DEBUG ((EFI_D_INFO, "EstablishDtpm: PttHeciGetCapability - %r (PttCapability = %x)\n",Status, PttCapability));
  ASSERT_EFI_ERROR (Status);

  Status = PttHeciGetState (&PttState);
  DEBUG ((EFI_D_INFO, "EstablishDtpm: PttHeciGetState - %r (PttState = %x)\n",Status, PttState));
  ASSERT_EFI_ERROR (Status);
  if (PttCapability && PttState) {
    //
    // Disable PTT in order to detect DTPM existence
    //
    Status = PttHeciSetState (FALSE);
    DEBUG ((EFI_D_INFO, "EstablishDtpm: Disable PTT in order to detect DTPM existence\n"));
    DEBUG ((EFI_D_INFO, "EstablishDtpm: SetPttState (FALSE) - %r\n", Status));
    if (!EFI_ERROR (Status)) {
      DEBUG ((EFI_D_INFO, "EstablishDtpm: Need to restart system, since we just changed PTT stater\n"));
      //
      // Need reset system, since we just changed PTT state
      //
      CopyMem (&ResetData.Guid, &gPchGlobalResetGuid, sizeof (EFI_GUID));
      StrCpyS (ResetData.Description, PCH_RESET_DATA_STRING_MAX_LENGTH, PCH_PLATFORM_SPECIFIC_RESET_STRING);
      gRT->ResetSystem (EfiResetPlatformSpecific, EFI_SUCCESS, sizeof (PCH_RESET_DATA), &ResetData);
      CpuDeadLoop ();
    }
  }

//[-start-200121-IB10189023-modify]//
  if (CompareGuid (PcdGetPtr(PcdTpmInstanceGuid), &gEfiTpmDeviceInstanceTpm12Guid) ||
      CompareGuid (PcdGetPtr(PcdTpmInstanceGuid), &gEfiTpmDeviceInstanceTpm20DtpmGuid) ||
      CompareGuid (PcdGetPtr(PcdTpmInstanceGuid), &gEfiTpmDeviceInstanceTcmGuid)) {
//[-end-200121-IB10189023-modify]//
    DEBUG ((EFI_D_INFO, "EstablishDtpm : DTPM presented on first boot.\n"));
    //
    // Disable PTTEnable once SetupUtilityProtocol is ready
    //
    EfiCreateProtocolNotifyEvent (
      &gEfiSetupUtilityProtocolGuid,
      TPL_CALLBACK,
      SetupUtilityProtocolInstallationEvent,
      NULL,
      (VOID **)&Registration
      );
    //
    // Disable PTTEnable for following flow
    //
    SetupVariable->PTTEnable = 0;
  }

  return EFI_SUCCESS;
}
//[-end-190503-IB17700016-modify]//

/**
  A minimal wrapper function that allows MtrrSetAllMtrrs () to be passed to
  EFI_MP_SERVICES_PROTOCOL.StartupAllAPs () as Procedure.

  @param[in] Buffer  Pointer to an MTRR_SETTINGS object, to be passed to
                     MtrrSetAllMtrrs ().
**/
VOID
EFIAPI
SetMtrrsFromBuffer (
  IN VOID *Buffer
  )
{
  MtrrSetAllMtrrs (Buffer);
}


/**
  This function handles PlatformInitDxe task at the ready to boot

  @param[in]  Event     The Event this notify function registered to.
  @param[in]  Context   Pointer to the context data registered to the Event.
**/
VOID
EFIAPI
PlatformDxeReadyToBootCallback (
  IN EFI_EVENT Event,
  IN VOID      *Context
  )
{
  EFI_STATUS                Status;
  MTRR_SETTINGS             MtrrSetting;
  EFI_MP_SERVICES_PROTOCOL  *MpService;

  DEBUG ((DEBUG_INFO, "PlatformDxeReadyToBootCallback start\n"));

  Status = MtrrTransfer2DefaultWB (&MtrrSetting);
  if (EFI_ERROR (Status)) {
    return;
  }

  #if FixedPcdGetBool(PcdExtendedBiosRegionSupport) == 1
    DEBUG ((DEBUG_INFO, "[BIOS] PlatformDxeReadyToBootCallback\n"));
    UINT32            ExtendedBiosRegionBaseAddress;
    UINT32            ExtendedBiosRegionSize;
    MTRR_SETTINGS     ExtendedBiosRegionUCMtrrSetting;

    ExtendedBiosRegionBaseAddress = 0xF8000000;
    ExtendedBiosRegionSize        = SIZE_32MB;
    ///
    /// Reset all MTRR setting.
    ///
    ZeroMem (&ExtendedBiosRegionUCMtrrSetting, sizeof (MTRR_SETTINGS));

    //
    // Get the Current MTRR Settings
    //
    MtrrGetAllMtrrs (&ExtendedBiosRegionUCMtrrSetting);

    Status = MtrrSetMemoryAttributeInMtrrSettings (
                &ExtendedBiosRegionUCMtrrSetting,
                ExtendedBiosRegionBaseAddress,
                ExtendedBiosRegionSize,
                CacheUncacheable
                );

    if (EFI_ERROR (Status)) {
      REPORT_STATUS_CODE_EX (
        (EFI_ERROR_CODE | EFI_ERROR_UNRECOVERED),
        (EFI_COMPUTING_UNIT_HOST_PROCESSOR|EFI_CU_HP_EC_CACHE),
        0,
        &gCpuFspErrorTypeCallerId,
        NULL,
        NULL,
        0
        );
    }

    ///
    /// Update MTRR setting from MTRR buffer
    ///
    MtrrSetAllMtrrs (&ExtendedBiosRegionUCMtrrSetting);
    MtrrDebugPrintAllMtrrs ();
  #endif

  //
  // Synchronize the update with all APs
  //
  Status = gBS->LocateProtocol (
                  &gEfiMpServiceProtocolGuid,
                  NULL,
                  (VOID **)&MpService
                  );
  if (!EFI_ERROR (Status)) {
    MtrrGetAllMtrrs (&MtrrSetting);
    MpService->StartupAllAPs (
                 MpService,          // This
                 SetMtrrsFromBuffer, // Procedure
                 FALSE,              // SingleThread
                 NULL,               // WaitEvent
                 0,                  // TimeoutInMicrosecsond
                 &MtrrSetting,       // ProcedureArgument
                 NULL                // FailedCpuList
                 );
  }

  DEBUG ((DEBUG_INFO, "PlatformDxeReadyToBootCallback end\n"));

  gBS->CloseEvent (Event);

  return;
}

/**
  Registers callback for PlatformDxeReadyToBoot

**/
VOID
RegisterReadyToBootCallback (
  VOID
  )
{
  EFI_STATUS                     Status;
  EFI_EVENT                      Event;

  Status = EfiCreateEventReadyToBootEx (
             TPL_CALLBACK,
             PlatformDxeReadyToBootCallback,
             NULL,
             &Event
             );

//[-start-180315-IB07250307-add]//
  if (FeaturePcdGet (PcdMemSpdProtectionSupported)){
    Status = EfiCreateEventReadyToBootEx (
               TPL_CALLBACK,
               ProtectMemorySPD,
               NULL,
               &Event
               );
    ASSERT_EFI_ERROR (Status);
  }
//[-end-180315-IB07250307-add]//

  ASSERT_EFI_ERROR (Status);
}

/**
  Call back function for reset notification.

  @param[in] ResetType            UEFI defined reset type.
  @param[in] ResetStatus          The status code for the reset.
  @param[in] DataSize             The size of ResetData in bytes.
  @param[in] ResetData            Optional element used to introduce a platform specific reset.
                                  The exact type of the reset is defined by the EFI_GUID that follows
                                  the Null-terminated Unicode string.
**/
VOID
EFIAPI
PlatformResetNotificationCallback (
  IN EFI_RESET_TYPE           ResetType,
  IN EFI_STATUS               ResetStatus,
  IN UINTN                    DataSize,
  IN VOID                     *ResetData OPTIONAL
  )
{
  EFI_STATUS                  Status;

  DEBUG ((DEBUG_INFO, "PlatformResetNotificationCallback\n"));

  Status = Tpm2Shutdown (TPM_SU_CLEAR);
  if (EFI_ERROR (Status)) {
    DEBUG ((DEBUG_ERROR, "Tpm2Shutdown (TPM_SU_CLEAR) failed: %r\n", Status));
  }
}


/**
  Hook to reset notification protocol to properly reset function with TPM.
  @param[in]  Event     Event whose notification function is being invoked
  @param[in]  Context   Pointer to the notification function's context
**/
VOID
EFIAPI
OnResetNotifyInstall (
  IN EFI_EVENT                      Event,
  IN VOID                           *Context
  )
{

  EFI_STATUS  Status;
  EFI_RESET_NOTIFICATION_PROTOCOL   *ResetNotify;

  Status = gBS->LocateProtocol (&gEfiResetNotificationProtocolGuid, NULL, (VOID **) &ResetNotify);
  if (!EFI_ERROR (Status)) {
    ResetNotify->RegisterResetNotify (ResetNotify, PlatformResetNotificationCallback);
    if(Event) gBS->CloseEvent (Event);
  }
}

/**
  Security EndOfDxe CallBack Function
  If the firmware/BIOS has an option to enable and disable DMA protections via a VT-d switch in BIOS options, then the shipping configuration must be with VT-d protection enabled.
  On every boot where VT-d/DMA protection is disabled, or will be disabled, or configured to a lower security state, and a platform has a TPM enabled, then the platform SHALL
  extend an EV_EFI_ACTION event into PCR[7] before enabling external DMA
  The event string SHALL be "DMA Protection Disabled". The platform firmware MUST log this measurement in the event log using the string "DMA Protection Disabled" for the Event Data.
  Measure and log launch of TBT Security, and extend the measurement result into a specific PCR.
  Extend an EV_EFI_ACTION event into PCR[7] before enabling external DMA. The event string SHALL be "DMA Protection Disabled". The platform firmware MUST log this measurement
  in the event log using the string "DMA Protection Disabled" for the Event Data.

  @param[in] Event     - A pointer to the Event that triggered the callback.
  @param[in] Context   - A pointer to private data registered with the callback function.
**/
VOID
EFIAPI
ExtendPCR7CallBack (
  IN EFI_EVENT          Event,
  IN VOID               *Context
  )
{
  UINTN                 Status;
  UINT64                HashDataLen;
  PCR7_DATA             *PCR7DataPtr;

  DEBUG ((DEBUG_INFO, "ExtendPCR7CallBack START\n"));

  if (Context == NULL) {
    DEBUG ((DEBUG_INFO, "ExtendPCR7CallBack function ERROR\n"));
    return;
  }

  PCR7DataPtr = (PCR7_DATA*) Context;

  if (PCR7DataPtr->EnableVtd == FALSE) {
    //
    // When VT-d/DMA protection is disabled and a platform has a TPM enabled,
    // the platform SHALL extend an EV_EFI_ACTION event into PCR[7]
    //
    HashDataLen = SECURITY_EVENT_STRING_LEN;

    Status = TpmMeasureAndLogData (
      7,
      EV_EFI_ACTION,
      SECURITY_EVENT_STRING,
      (UINT32) HashDataLen,
      SECURITY_EVENT_STRING,
      HashDataLen
    );
    if (EFI_ERROR(Status)) {
      DEBUG ((DEBUG_ERROR, "VT-d TpmMeasureAndLogData Status: %d\n", Status));
    } else {
      DEBUG ((DEBUG_INFO, "VT-d TpmMeasureAndLogData Successfully\n"));
    }
  }

#if FixedPcdGetBool (PcdDTbtEnable) == 1
  if (PCR7DataPtr->SLDowngrade == TRUE) {
    //
    // When TBT security mode downgrade to 0 and a platform has a TPM enabled,
    // the platform SHALL extend an EV_EFI_ACTION event into PCR[7]
    //
    HashDataLen = TBT_SECURITY_LEVEL_DOWNGRADED_STRING_LEN;

    Status = TpmMeasureAndLogData (
      7,
      EV_EFI_ACTION,
      TBT_SECURITY_LEVEL_DOWNGRADED_STRING,
      (UINT32)HashDataLen,
      TBT_SECURITY_LEVEL_DOWNGRADED_STRING,
      HashDataLen
    );
    if (EFI_ERROR (Status)) {
      DEBUG ((DEBUG_ERROR, "TBT SL downgrade to 0, TpmMeasureAndLogData Status: %d\n", Status));
    }
    else {
      DEBUG ((DEBUG_INFO, "TBT SL downgrade to 0,TpmMeasureAndLogData Successfully\n"));
    }
  }
#endif
  if (PCR7DataPtr != NULL) {
    FreePool(PCR7DataPtr);
  }

  DEBUG ((DEBUG_INFO, "ExtendPCR7CallBack END\n"));
}

/**
  Register an End of DXE event for extended a TPM log to PCR[7] when vtd is disable
  This feature is introduced by TBT Security requirment
**/
VOID
RegisterExtendPCR7CallBack (
  VOID
  )
{
  EFI_STATUS                Status;
  UINTN                     DataSize;
  SA_SETUP                  *SaSetup;
  BOOLEAN                   SLDowngrade;
#if FixedPcdGetBool (PcdDTbtEnable) == 1
  UINT8                     Index;
  UINT8                     DTbtController[MAX_DTBT_CONTROLLER_NUMBER];
#endif
  PCR7_DATA                 *PCR7DataPtr;
#if FixedPcdGetBool (PcdDTbtEnable) == 1
  DTBT_INFO_HOB             *DTbtInfoHob;
  UINT8                     CurrentSecurityMode;
#endif

  Status                    = EFI_SUCCESS;
  SaSetup                   = NULL;
  DataSize                  = 0;
  SLDowngrade               = FALSE;
  PCR7DataPtr               = NULL;
#if FixedPcdGetBool (PcdDTbtEnable) == 1
  DTbtInfoHob               = NULL;
#endif

  DataSize = sizeof (SA_SETUP);
  SaSetup = AllocateZeroPool (DataSize);
  if (SaSetup == NULL) {
    DEBUG ((DEBUG_ERROR, "Failed to allocate SaSetup size\n"));
    Status = EFI_OUT_OF_RESOURCES;
    goto Exit;
  }

  Status = gRT->GetVariable (
                  L"SaSetup",
                  &gSaSetupVariableGuid,
                  NULL,
                  &DataSize,
                  SaSetup
                  );
  if (EFI_ERROR (Status)) {
    DEBUG ((DEBUG_ERROR, "Get SaSetup Status: %d\n", Status));
    goto Exit;
  }

#if FixedPcdGetBool (PcdDTbtEnable) == 1

  //
  // Get TBT INFO HOB
  //
  DTbtInfoHob = (DTBT_INFO_HOB *) GetFirstGuidHob (&gDTbtInfoHobGuid);
  if (DTbtInfoHob != NULL) {
    if (DTbtInfoHob->DTbtControllerConfig[0].DTbtControllerEn == 1 || DTbtInfoHob->DTbtControllerConfig[1].DTbtControllerEn == 1) {
      for (Index = 0; Index < MAX_DTBT_CONTROLLER_NUMBER; Index++) {
        DTbtController[Index] = DTbtInfoHob->DTbtControllerConfig[Index].DTbtControllerEn;
      }
      //
      // Check current security mode
      //
      CurrentSecurityMode = GetSetSecurityMode (
                              0,
                              DTbtController,
                              MAX_DTBT_CONTROLLER_NUMBER,
                              GET_TBT_SECURITY_MODE
                              );

      if ((CurrentSecurityMode != 0) && (CurrentSecurityMode != 5)) {
        // Code shouldn't run to here
        DEBUG ((DEBUG_ERROR, "Code shouldn't run to here, it means security level got changed unintentionaly\n"));
        SLDowngrade = TRUE;
      }
     }

  }
#endif
  if ((SaSetup->EnableVtd == FALSE) || (SLDowngrade == TRUE)) {
    DataSize = sizeof (PCR7_DATA);
    PCR7DataPtr = AllocateZeroPool (DataSize);
    if (PCR7DataPtr == NULL) {
      DEBUG ((DEBUG_ERROR, "Failed to allocate PCR7 data size\n"));
      Status = EFI_OUT_OF_RESOURCES;
      goto Exit;
    }

    PCR7DataPtr->EnableVtd = SaSetup->EnableVtd;
    PCR7DataPtr->SLDowngrade = SLDowngrade;

    //
    // Register an End of DXE event for extended a TPM log to PCR[7]
    //
    DEBUG ((DEBUG_ERROR, "Register an End of DXE event for extended a TPM log to PCR[7] when Vtd is disable\n"));
    Status = gBS->CreateEventEx (
                    EVT_NOTIFY_SIGNAL,
                    TPL_CALLBACK,
                    ExtendPCR7CallBack,
                    PCR7DataPtr,
                    &gEfiEndOfDxeEventGroupGuid,
                    &EndOfDxeEvent
                    );
    if (EFI_ERROR (Status)) {
      DEBUG ((DEBUG_ERROR, "Failed to register an End of DXE event for extended a TPM log to PCR[7], Status: %d\n", Status));
      gBS->CloseEvent (EndOfDxeEvent);
      goto Exit;
    }
  }

Exit:
  if (SaSetup != NULL) {
    FreePool (SaSetup);
  }

  if (EFI_ERROR (Status)) {
    if (PCR7DataPtr != NULL) {
      FreePool (PCR7DataPtr);
    }
  }
}

//[-start-181129-IB15410203-add]//
/**
 After recovery complete, change reset system type to specific to do PCH global reset.

 @param[in] Event          A pointer to the Event that triggered the callback.
 @param[in] Handle         Checkpoint handle.
**/
VOID
EFIAPI
CpRecoveryCompleteCallback (
  IN EFI_EVENT                         Event,
  IN H2O_CP_HANDLE                     Handle
  )
{
  EFI_STATUS                           Status;
  H2O_BDS_CP_RECOVERY_COMPLETE_DATA    *BdsRecoveryCompleteData;
  STATIC PCH_RESET_DATA                ResetData;

  Status = H2OCpLookup (Handle, (VOID **) &BdsRecoveryCompleteData, &gH2OBdsCpRecoveryCompleteGuid);
  if (EFI_ERROR (Status)) {
//[-start-190724-IB17700055-modify]//
    DEBUG_CP ((DEBUG_ERROR, "Checkpoint Data Not Found: %x (%r)\n", Handle, Status));
    DEBUG_CP ((DEBUG_ERROR, "   %a\n", __FUNCTION__));
//[-end-190724-IB17700055-modify]//
    return;
  }
  if (BdsRecoveryCompleteData->Size < OFFSET_OF(H2O_BDS_CP_RECOVERY_COMPLETE_DATA, ResetDataSize)) {
//[-start-190724-IB17700055-modify]//
    DEBUG_CP ((DEBUG_WARN, "Checkpoint Data Does Not Contain Member (%a)\n", "ResetDataSize"));
    DEBUG_CP ((DEBUG_WARN, "   %a\n", __FUNCTION__));
//[-end-190724-IB17700055-modify]//
    return;
  }

  CopyMem (&ResetData.Guid, &gPchGlobalResetGuid, sizeof (EFI_GUID));
  StrCpyS (ResetData.Description, PCH_RESET_DATA_STRING_MAX_LENGTH, PCH_PLATFORM_SPECIFIC_RESET_STRING);

  BdsRecoveryCompleteData->RequestAction = H2O_BDS_CP_RECOVERY_COMPLETE_PLATFORM_SPECIFIC_RESET;
  BdsRecoveryCompleteData->ResetData     = (UINT8 *) &ResetData;
  BdsRecoveryCompleteData->ResetDataSize = sizeof (ResetData);
  BdsRecoveryCompleteData->Status        = H2O_CP_TASK_UPDATE;
}
//[-end-181129-IB15410203-add]//

/**
  Allocate MemoryType below 4G memory address.

  @param[in] MemoryType    The type of memory to allocate
  @param[in] Size               Size of memory to allocate.
  @param[OUT] Buffer         Allocated address for output.

  @retval EFI_SUCCESS       Memory successfully allocated.
  @retval Other                  Other errors occur.
**/
EFI_STATUS
AllocateMemoryBelow4G(
  IN   EFI_MEMORY_TYPE MemoryType,
  IN   UINTN           Size,
  OUT  VOID           **Buffer
)
{
  UINTN                 Pages;
  EFI_PHYSICAL_ADDRESS  Address;
  EFI_STATUS            Status;

  Pages = EFI_SIZE_TO_PAGES(Size);
  Address = 0xffffffff;

  Status = (gBS->AllocatePages) (
                  AllocateMaxAddress,
                  MemoryType,
                  Pages,
                  &Address
                  );
  DEBUG((DEBUG_INFO, "AllocateMemoryBelow4G: %r\n", Status));
  *Buffer = (VOID *)(UINTN)Address;

  return Status;
};

/**
  Read And load the LPM register in NVS area
**/
EFI_STATUS
ReadLowPowerModeRequirementReg(
  VOID
  )
{
  EFI_STATUS              Status;
  UINT32                  *LpmData;
  UINT32                  LpmDataLen;

//[-start-200901-IB067400787-remove]//
  // Status = gBS->LocateProtocol (
  //               &gPlatformNvsAreaProtocolGuid,
  //               NULL,
  //               (VOID **)&mPlatformNvsAreaProtocol
  //               );
  // DEBUG ((DEBUG_ERROR, "mPlatformNvsAreaProtocol: %r\n", Status));
  // ASSERT_EFI_ERROR (Status);
  // if (EFI_ERROR (Status)) {
  //   return Status;
  // }
//[-end-200901-IB067400787-remove]//

//[-start-180315-IB07250307-modify]//
  mPlatformNvsAreaPtr = mPlatformNvsAreaProtocol.Area;
//[-end-180315-IB07250307-modify]//

  LpmDataLen = V_PMC_PWRM_LPM_REQ_DATA_LEN; //LPM register counter is 48, so length is 192 byte

  // Allocate memory in ACPI NVS
  //
  Status = AllocateMemoryBelow4G (EfiACPIMemoryNVS, LpmDataLen, (VOID **)&LpmData);
  ASSERT_EFI_ERROR (Status);
  if (EFI_ERROR (Status)) {
    return Status;
  }

  ZeroMem (LpmData, LpmDataLen);

  //
  // Read out LPM requiremetn register data
  //
  Status = PmcReadLpmReqData (LpmData, LpmDataLen);
  if (EFI_ERROR (Status)) {
    return Status;
  }

  mPlatformNvsAreaPtr->LpmReqRegAddr = (UINT32) (UINTN) LpmData;

  return Status;
}

/**
  Update memory space attributes.
**/
EFI_STATUS
EFIAPI
UpdateMemorySpaceAttributes (
  )
{
  EFI_STATUS                            Status;
  EFI_GCD_MEMORY_SPACE_DESCRIPTOR       GcdDescriptor;
  EFI_PHYSICAL_ADDRESS                  BaseAddress;
  UINT64                                Length;

  BaseAddress = PcdGet64 (PcdPciExpressBaseAddress);;
  Length      = PcdGet32 (PcdPciExpressRegionLength);

  Status = gDS->GetMemorySpaceDescriptor (BaseAddress, &GcdDescriptor);
  if (EFI_ERROR (Status)) {
    DEBUG ((DEBUG_ERROR, "UpdateMemorySpaceAttributes failed to get memory space descriptor.\n"));
  } else {
    Status = gDS->SetMemorySpaceAttributes (
                    BaseAddress,
                    Length,
                    GcdDescriptor.Attributes | EFI_MEMORY_RUNTIME
                    );
    if (EFI_ERROR (Status)) {
      DEBUG ((DEBUG_ERROR, "UpdateMemorySpaceAttributes failed to add EFI_MEMORY_RUNTIME attribute.\n"));
    }
  }

  return Status;
}

/**
  Get DCC current frequencies.

  @param[in]  Integer           The integer parts of divider register.
  @param[in]  Fractional        The fractional parts of divider register.

  @retval     Frequency         Discrete Clock frequency.
**/
UINT16
GetDccCurrentFreq (
  IN UINT16 Integer,
  IN UINT32 Fractional
  )
{
  UINT64                DividerValue;
  UINT64                DefualtFreq;

  DividerValue = (Integer % BIT9);// The RC260x chip only provide [8:0] for integer register
  DividerValue = LShiftU64 (DividerValue, 28);
  DividerValue += (Fractional % BIT28); // The RC260x chip only provide [27:0] for fraction register

  DefualtFreq = RENESAS_260X_CHIP_FREQUENCY;
  DefualtFreq = LShiftU64 (DefualtFreq, 28);

  return (DividerValue == 0)? 0 : (UINT16)(DefualtFreq / DividerValue);
}


/**
  Entry point for the driver.

  @param[in]  ImageHandle  Image Handle.
  @param[in]  SystemTable  EFI System Table.

  @retval     EFI_SUCCESS  Function has completed successfully.
  @retval     Others       All other error conditions encountered result in an ASSERT.
**/
EFI_STATUS
EFIAPI
PlatformInitAdvancedDxeEntryPoint (
  IN EFI_HANDLE               ImageHandle,
  IN EFI_SYSTEM_TABLE         *SystemTable
  )
{
  EFI_STATUS                  Status;

//[-start-210127-IB16810146-modify]//
  SETUP_DATA                  *SetupData;
  UINTN                       VariableSize;
  UINT32                      VariableAttr;
//  UINT8                       *SecureBootState;
  //VOID                        *Registration;
  //REGISTER_INTEGER            Integer;
  //REGISTER_FRACTIONAL         Fractional;
  CHIPSET_CONFIGURATION       *SetupVariable;

//  PostCode(PLATFORM_INIT_DXE_ENTRY);
  Status = EFI_SUCCESS;
  SetupData = NULL;

  SetupVariable = CommonGetVariableData (SETUP_VARIABLE_NAME, &gSystemConfigurationGuid);
  if (SetupVariable == NULL) {
    DEBUG ((DEBUG_INFO, "DXE platform: Get System Configuration failed.\n"));
    ASSERT (SetupVariable != NULL);
  }
//[-end-180315-IB07250307-modify]//
//[-end-210127-IB16810146-modify]//

#if FixedPcdGetBool (PcdSinitAcmBinEnable) == 1
  Status = TxtSinitAcmLoad ();
  if (Status == EFI_SUCCESS) {
    DEBUG ((DEBUG_INFO, "TXTDXE: Found SINIT ACM In FV\n"));
  } else {
      DEBUG ((DEBUG_INFO, "TXTDXE: Error finding SINIT binary in FVs\n"));
  }
#endif

//[-start-181129-IB15410203-add]//
  if (FeaturePcdGet (PcdH2OBdsCpRecoveryCompleteSupported)) {
    H2O_CP_HANDLE                       CpHandle;

    Status = H2OCpRegisterHandler (
               &gH2OBdsCpRecoveryCompleteGuid,
               CpRecoveryCompleteCallback,
               H2O_CP_MEDIUM,
               &CpHandle
               );
//[-start-190724-IB17700055-modify]//
    if (EFI_ERROR (Status)) {
      DEBUG_CP ((DEBUG_ERROR, "Checkpoint Register Fail: %g (%r)\n", &gH2OBdsCpRecoveryCompleteGuid, Status));
      return Status;
    }
    DEBUG_CP ((DEBUG_INFO, "Checkpoint Registered: %g (%r)\n", &gH2OBdsCpRecoveryCompleteGuid, Status));
//[-end-190724-IB17700055-modify]//
  }
//[-end-181129-IB15410203-add]//

//[-start-180315-IB07250307-remove]//
//  SecureBootState = NULL;
//  GetVariable2 (L"SecureBootEnable", &gEfiSecureBootEnableDisableGuid, (VOID**)&SecureBootState, NULL);
//
//  //
//  // Set CsmControl Setup variable reflecting Secure boot status. Whenever Secure boot is enabled, CSM must be turned off.
//  //
//[-start-210127-IB16810146-modify]//
    VariableSize = 0;
    Status = gRT->GetVariable (
                    L"Setup",
                    &gSetupVariableGuid,
                    &VariableAttr,
                    &VariableSize,
                    SetupData
                    );
    if (Status == EFI_BUFFER_TOO_SMALL) {
      SetupData = (SETUP_DATA *) AllocateZeroPool (VariableSize);
      if (SetupData != NULL) {
        Status = gRT->GetVariable (
                        L"Setup",
                        &gSetupVariableGuid,
                        &VariableAttr,
                        &VariableSize,
                        SetupData
                        );
      }
    }

    if (EFI_ERROR (Status)) {
      DEBUG ((DEBUG_INFO, "DXE platform: Get System Variable failed.\n"));
      ASSERT_EFI_ERROR (Status);
    }
//[-end-210127-IB16810146-modify]//
//  SetupData.OpRomPost  = OPROM_EFI;
//
//  if ((SecureBootState != NULL) && (*SecureBootState)) {
//    if (!EFI_ERROR (Status)) {
//      SetupData.OpRomPost  = OPROM_EFI;
//    }
//  }
//
  //
  // Update Cpu BCLK & PEG/DMI Freq.
  //
//  if (PcdGetBool (PcdDiscreteClockOcSupport) == TRUE) {
//    if (SetupData.DccClkCtrl == 1) {
//    GetCurrentDividerSettings (CPU_BCLK_OC_FOD, &Integer, &Fractional);
//    SetupData.CurrentCpuBclkFreq = GetDccCurrentFreq (Integer.Uint16, Fractional.Uint32);
//
//    GetCurrentDividerSettings (PEG_DMI_CLK_FOD, &Integer, &Fractional);
//    SetupData.CurrentPegDmiClkFreq = GetDccCurrentFreq (Integer.Uint16, Fractional.Uint32);
//    }
//  }
//

//  Status = gRT->SetVariable (
//                  L"Setup",
//                  &gSetupVariableGuid,
//                  VariableAttr,
//                  VariableSize,
//                  &SetupData
//                  );
//  ASSERT_EFI_ERROR (Status);
//
//  if (SecureBootState != NULL) {
//    FreePool (SecureBootState);
//  }


  //
  // Initialize System Agent platform settings
  //
  SaPlatformInitDxe ();

  //
  // Update DIMM funtionalities for desktop and server boards
  //
//  UpdateDimmPopulation ();
//
//  ReadLowPowerModeRequirementReg ();
//
//  RegisterEndOfDxeCallbacks();
//[-end-180315-IB07250307-remove]//
  RegisterReadyToBootCallback ();

//[-start-180315-IB07250307-remove]//
//  //
//  // Add Smbios type 4 and type 7 table.
//  //
//  AddSmbiosProcessorAndCacheTables ();
//
//  //
//  // Check if TPM2.0 exist.
//  //
//  if (CompareGuid (PcdGetPtr (PcdTpmInstanceGuid), &gEfiTpmDeviceInstanceNoneGuid)) {
//  } else if (CompareGuid (PcdGetPtr(PcdTpmInstanceGuid), &gEfiTpmDeviceInstanceTpm12Guid)) {
//  } else {
    //
    // Register Pch reset callback to shutdown TPM
    //
//    EfiCreateProtocolNotifyEvent (
//      &gEfiResetNotificationProtocolGuid,
//      TPL_CALLBACK,
//      OnResetNotifyInstall,
//      NULL,
//      &Registration
//      );
//
//  }
//[-end-180315-IB07250307-remove]//
//[-start-180316-IB07250307-modify]//
  //
  // Inform OS by TPM PCR7 when Vtd is disable
  //
  RegisterExtendPCR7CallBack ();
//[-start-180316-IB07250307-modify]//
  UpdatePttState (SetupVariable);
  InitialSetupForOverclocking (SetupVariable);
//[-start-210127-IB16810146-modify]//
  Status = InitialPlatformNvsAreaProtocol (SetupVariable);
  if (EFI_ERROR (Status)) {
    DEBUG ((DEBUG_INFO, "DXE platform: PlatformNvsAreaProtocol installation failed.\n"));
    ASSERT_EFI_ERROR (Status);       
  } 
//[-end-210127-IB16810146-modify]//
//[-start-200901-IB067400787-add]//
  ReadLowPowerModeRequirementReg ();
//[-end-200901-IB067400787-add]//
  gBS->FreePool (SetupVariable);

  UpdateMemorySpaceAttributes();


//  PostCode (PLATFORM_INIT_DXE_EXIT);
//[-end-180316-IB07250307-modify]//
//[-start-210127-IB16810146-modify]//
  return EFI_SUCCESS;
//[-end-210127-IB16810146-modify]//
}

#if FixedPcdGetBool(PcdSinitAcmBinEnable) == 1
/**

  This function looks for SINIT ACM in FVs and updates TXT HOB
  with SINIT ACM Base and Size.

  @retval EFI_SUCCESS     - SINIT ACM found and copied.
  @retval EFI_NOT_FOUND   - If TxtInfoHob is not found
  @retval EFI_UNSUPPORTED - TXT Device memory not available.

**/
EFI_STATUS
TxtSinitAcmLoad (
  VOID
  )
{
  EFI_STATUS                    Status;
  TXT_INFO_HOB                  *HobList;
  TXT_INFO_DATA                 *TxtInfoData;
  EFI_HANDLE                    *HandleBuffer;
  UINTN                         NumberOfHandles;
  EFI_FIRMWARE_VOLUME2_PROTOCOL *FwVol;
  UINTN                         Size;
  EFI_FV_FILETYPE               FileType;
  UINT32                        FvStatus;
  EFI_FV_FILE_ATTRIBUTES        Attributes;
  UINT64                        TxtHeapMemoryBase;
  UINT64                        TxtSinitMemoryBase;
  UINT64                        *Ptr64;
  EFI_PHYSICAL_ADDRESS          TopAddr;
  BIOS_OS_DATA_REGION           *BiosOsDataRegion;

  HandleBuffer = NULL;
  NumberOfHandles = 0;
  FwVol = NULL;
  FileType = 0;
  Attributes = 0;
  BiosOsDataRegion = NULL;

  ///
  /// Find TXT HOB
  ///
  HobList = (TXT_INFO_HOB *) GetFirstGuidHob (&gTxtInfoHobGuid);
  if (HobList == NULL) {
    return EFI_NOT_FOUND;
  }

  TxtInfoData = &HobList->Data;
  ///
  /// Check TXT mode
  ///
  if (TxtInfoData->TxtMode == 0) {
    return EFI_UNSUPPORTED;
  }

  if ((TxtInfoData == 0) ||
      (TxtInfoData->TxtDprMemoryBase == 0) ||
      (TxtInfoData->TxtDprMemorySize == 0) ||
      (TxtInfoData->TxtHeapMemorySize == 0) ||
      (TxtInfoData->SinitMemorySize == 0)
      ) {
    return EFI_UNSUPPORTED;
  } else {
    ///
    /// Use address passed from PEI
    ///
    TopAddr             = TxtInfoData->TxtDprMemoryBase + TxtInfoData->TxtDprMemorySize;

    TxtHeapMemoryBase   = (UINT64) (TopAddr - TxtInfoData->TxtHeapMemorySize);

    TxtSinitMemoryBase  = TxtHeapMemoryBase - TxtInfoData->SinitMemorySize;
  }
  ///
  /// Start looking for SINIT ACM in FVs
  ///
  DEBUG ((DEBUG_INFO, "TXTDXE: Looking for SINIT ACM in FVs\n"));

  Status = gBS->LocateHandleBuffer (
    ByProtocol,
    &gEfiFirmwareVolume2ProtocolGuid,
    NULL,
    &NumberOfHandles,
    &HandleBuffer
    );
  ASSERT_EFI_ERROR (Status);
  ///
  /// Looking for FV with SinitAC binary
  ///
  for (UINTN i = 0; i < NumberOfHandles; i++) {
    ///
    /// Get the protocol on this handle
    ///
    Status = gBS->HandleProtocol (
      HandleBuffer[i],
      &gEfiFirmwareVolume2ProtocolGuid,
      &FwVol
      );
    ASSERT_EFI_ERROR (Status);
    ///
    /// See if it has the SinitACM file
    ///
    Size = 0;
    FvStatus = 0;
    Status = FwVol->ReadFile (
      FwVol,
      &gSinitModuleGuid,
      NULL,
      &Size,
      &FileType,
      &Attributes,
      &FvStatus
      );
    ///
    /// If the binary was located, then break
    ///
    if (Status == EFI_SUCCESS) {
      DEBUG ((DEBUG_INFO, "TXTDXE: Found SINIT ACM In FV\n"));
      break;
    }
  }
  FreePool (HandleBuffer);
  ///
  /// Sanity check
  ///
  if ( !(FwVol == NULL || Status != EFI_SUCCESS || Size >= TxtInfoData->SinitMemorySize ) ) {
    ///
    /// Read Sinit ACM from the storage file.
    ///
    Ptr64 = (UINT64 *)TxtSinitMemoryBase;
    Status = FwVol->ReadFile (
      FwVol,
      &gSinitModuleGuid,
      &Ptr64,
      &Size,
      &FileType,
      &Attributes,
      &FvStatus
    );
    ASSERT_EFI_ERROR (Status);
    TxtInfoData->SinitAcmSize = (UINT64) Size;

    Ptr64 = (UINT64 *) TxtHeapMemoryBase;
    ///
    /// BiosOsDataSize plus size of data size field itself
    ///
    BiosOsDataRegion                = (BIOS_OS_DATA_REGION *) (Ptr64 + 1);
    BiosOsDataRegion->BiosSinitSize = (UINT32) TxtInfoData->SinitAcmSize;
    DEBUG ((DEBUG_INFO, "TXTDXE: Copy SINIT ACM Done\n"));
  }
  return Status;
}
#endif

//[-start-180316-IB07250307-add]//
EFI_STATUS
UpdatePttState (
  IN CHIPSET_CONFIGURATION          *SetupVariable
  )
{
  BOOLEAN                           PttState;
  BOOLEAN                           PttTargetState;
  BOOLEAN                           PttCapability;
  EFI_BOOT_MODE                     BootMode;
  EFI_STATUS                        Status;
  HECI_PROTOCOL                     *Heci;
  ME_SETUP                          MeSetup;
  ME_SETUP_STORAGE                  MeSetupStorage;
  PCH_RESET_DATA                    ResetData;
  UINT8                             FirstBootAfterFlash;
  UINT32                            MeMode;
  UINT32                            UnconfigOnRtcClear;
  UINTN                             DataSize;
  UINT32                            VariableAttr;
//[-start-211027-IB09480164-add]//
  UINT8                             MeSetupStorageInitFlag;
  UINTN                             MeSetupStorageInitFlagSize;
  EFI_STATUS                        GetMeSetupStorageStatus;
//[-end-211027-IB09480164-add]//

  PttState = FALSE;

  VariableAttr = EFI_VARIABLE_NON_VOLATILE | EFI_VARIABLE_BOOTSERVICE_ACCESS | EFI_VARIABLE_RUNTIME_ACCESS;
  DataSize = sizeof (ME_SETUP_STORAGE);
  ZeroMem (&MeSetupStorage, sizeof (ME_SETUP_STORAGE));
  Status = gRT->GetVariable (L"MeSetupStorage", &gMeSetupVariableGuid, NULL, &DataSize, &MeSetupStorage);
  if (EFI_ERROR (Status)) {
    DEBUG ((DEBUG_INFO, "DXE platform: Get MeSetupStorage variable failed.\n"));
    ASSERT_EFI_ERROR (Status);
  }

  DataSize = sizeof (ME_SETUP);
  ZeroMem (&MeSetup, sizeof (ME_SETUP));
  Status = gRT->GetVariable (L"MeSetup", &gMeSetupVariableGuid, NULL, &DataSize, &MeSetup);
  if (EFI_ERROR (Status)) {
    DEBUG ((DEBUG_INFO, "DXE platform: Get MeSetup variable failed.\n"));
    ASSERT_EFI_ERROR (Status);
  }

  DataSize = sizeof (UINT8);
  Status = gRT->GetVariable (
                  L"FirstBootAfterFlash",
                  &gEfiGenericVariableGuid,
                  NULL,
                  &DataSize,
                  &FirstBootAfterFlash
                  );
  if (EFI_ERROR (Status)) {
    //
    // Prefer using DTPM by design. Process if required.
    //
    if (FeaturePcdGet (PcdH2OPreferDtpmBootSupported)) {
      Status = EstablishDtpm (SetupVariable);
      DEBUG ((DEBUG_INFO, "DxePlatformEntryPoint: EstablishDtpm - %r\n", Status));
    }
    DataSize = sizeof(UINT8);
    FirstBootAfterFlash = 1;
    Status = gRT->SetVariable (
                    L"FirstBootAfterFlash",
                    &gEfiGenericVariableGuid,
                    VariableAttr,
                    sizeof(UINT8),
                    &FirstBootAfterFlash
                    );
    if (EFI_ERROR (Status)) {
      DEBUG ((DEBUG_INFO, "DXE platform: Set FirstBootAfterFlash variable failed.\n"));
      ASSERT_EFI_ERROR (Status);
    }

    Status = gBS->LocateProtocol (&gHeciProtocolGuid, NULL, (VOID **) &Heci);
    if (!EFI_ERROR (Status)) {
      Status = Heci->GetMeMode (&MeMode);
      ASSERT_EFI_ERROR(Status);
      if (MeMode == ME_MODE_NORMAL) {
        MeSetupStorage.MeStateControl = 1;
        Status = HeciGetUnconfigOnRtcClearDisableMsg (&UnconfigOnRtcClear);
        if (!EFI_ERROR (Status)) {
          if (UnconfigOnRtcClear != MeSetup.MeUnconfigOnRtcClear) {
            Status = HeciSetUnconfigOnRtcClearDisableMsg ((UINT32)MeSetup.MeUnconfigOnRtcClear);
            DEBUG ((DEBUG_INFO, "Sync MeUnconfigOnRtcClear of MeSetup to ME: %r\n", Status));
            ASSERT_EFI_ERROR(Status);
          }
        }
      }
    }
  }

//[-start-190503-IB17700016-modify]//
  BootMode = GetBootModeHob();
  if (BootMode != BOOT_IN_RECOVERY_MODE) {
    if ((BOOLEAN)MeSetupStorage.PttState != IsPttEnabled ()) {
      //
      // MeSetupStorage.PttState mismatch the current PTT policy
      // Get the exact PTT state from ME
      //
      Status = PttHeciGetState (&PttState);
      DEBUG ((EFI_D_INFO, "DxePlatformEntryPoint: PttHeciGetState (%x): %r\n", PttState, Status));
      ASSERT_EFI_ERROR (Status);
//[-start-211027-IB09480164-add]//
      GetMeSetupStorageStatus = EFI_SUCCESS;
      if (!IsVariableInVariableStoreRegion (L"MeSetupStorage", &gMeSetupVariableGuid)) {
        GetMeSetupStorageStatus = EFI_NOT_FOUND;
      }
//[-end-211027-IB09480164-add]//

      MeSetupStorage.PttState = PttState;
      Status = gRT->SetVariable (
                      L"MeSetupStorage",
                      &gMeSetupVariableGuid,
                      VariableAttr,
                      sizeof (ME_SETUP_STORAGE),
                      (VOID *)&MeSetupStorage
                      );
      ASSERT_EFI_ERROR (Status);

//[-start-211027-IB09480164-add]//
      if (!EFI_ERROR(Status) && (GetMeSetupStorageStatus == EFI_NOT_FOUND)) {
        MeSetupStorageInitFlagSize = sizeof (MeSetupStorageInitFlag);
        MeSetupStorageInitFlag = 1;
        Status = gRT->SetVariable (
                        L"MeSetupStorageInitFlag",
                        &gMeSetupVariableGuid,
                        EFI_VARIABLE_NON_VOLATILE | EFI_VARIABLE_BOOTSERVICE_ACCESS | EFI_VARIABLE_RUNTIME_ACCESS,
                        MeSetupStorageInitFlagSize,
                        (VOID *)&MeSetupStorageInitFlag
                        );
      }
//[-end-211027-IB09480164-add]//
    }
    //
    // Handle PTT state according to Setup
    // Always disable PTT feature when "TPM Hidden" was choosen
    //
    PttTargetState = (BOOLEAN)SetupVariable->PTTEnable && !PcdGetBool (PcdTpmHide);
    if ((BOOLEAN)MeSetupStorage.PttState != PttTargetState) {
      //
      // Check whether PTT is supported before changing PTT state
      //
      Status = PttHeciGetCapability (&PttCapability);
      DEBUG ((EFI_D_INFO, "DxePlatformEntryPoint: PttHeciGetCapability - %r (PttCapability = %x)\n", Status, PttCapability));
      ASSERT_EFI_ERROR (Status);

      if (PttCapability) {
        Status = PttHeciSetState (PttTargetState);
        DEBUG ((EFI_D_INFO, "DxePlatformEntryPoint: PttHeciSetState (%x): %r\n", PttTargetState, Status));
        ASSERT_EFI_ERROR (Status);
        if (!EFI_ERROR (Status)) {
          CopyMem (&ResetData.Guid, &gPchGlobalResetGuid, sizeof (EFI_GUID));
          StrCpyS (ResetData.Description, PCH_RESET_DATA_STRING_MAX_LENGTH, PCH_PLATFORM_SPECIFIC_RESET_STRING);
          gRT->ResetSystem (EfiResetPlatformSpecific, EFI_SUCCESS, sizeof (PCH_RESET_DATA), &ResetData);
        }
      }
    }
  }
//[-end-190503-IB17700016-modify]//

  return Status;
}

EFI_STATUS
InitialSetupForOverclocking (
  IN CHIPSET_CONFIGURATION          *SetupVariable
  )
{
  BOOLEAN                           Flag;
  EFI_HANDLE                        Handle;
  EFI_STATUS                        Status;

  Status = gBS->LocateProtocol (&gEfiPciRootBridgeIoProtocolGuid, NULL, (VOID **)&mPciRootBridgeIo);
  if (EFI_ERROR (Status)) {
    DEBUG ((DEBUG_INFO, "DXE platform: Locate PciRootBridgeIoProtocol failed.\n"));
    ASSERT_EFI_ERROR (Status);
  }

  //
  // Initialize Setup Configuration for Overclocking
  //
  Flag = TRUE;
  DEBUG_OEM_SVC ((DEBUG_INFO, "Dxe OemChipsetServices Call: OemSvcHookPlatformDxe (Flag = TRUE)\n"));
  Status = OemSvcHookPlatformDxe (SetupVariable, mPciRootBridgeIo, Flag);
  DEBUG_OEM_SVC ((DEBUG_INFO, "Dxe OemChipsetServices OemSvcHookPlatformDxe Status: %r\n", Status));
  if (Status != EFI_SUCCESS) {
    if (PcdGetBool (PcdEcPresent) == TRUE) {
      if (SetupVariable->NumLock == 0) {
        mPs2PolicyData.KeyboardLight &= (~EFI_KEYBOARD_NUMLOCK);
      }
      //
      //
      // Install protocol to to allow access to this Policy.
      //
      Handle = NULL;
      Status = gBS->InstallMultipleProtocolInterfaces (
                      &Handle,
                      &gEfiPs2PolicyProtocolGuid,
                      &mPs2PolicyData,
                      NULL
                      );
      ASSERT_EFI_ERROR(Status);
    }
    //
    // Install Legacy USB setup policy protocol depending upon
    // whether or not Legacy USB setup options are enabled or not.
    //
    UsbLegacyPlatformInstall();
    //
    // TBD or do in other places.
    // 1. Clear BIS Credentials.
    // 2. Chassis Intrusion, Date/Time, EventLog, Pnp O/S,
    // 3. Boot (BBS, USB, PXE, VGA)
    //
    Flag = FALSE;
    DEBUG_OEM_SVC ((DEBUG_INFO, "Dxe OemChipsetServices Call: OemSvcHookPlatformDxe (Flag = FALSE)\n"));
    Status = OemSvcHookPlatformDxe (SetupVariable, mPciRootBridgeIo, Flag);
    DEBUG_OEM_SVC ((DEBUG_INFO, "Dxe OemChipsetServices OemSvcHookPlatformDxe Status: %r\n", Status));
  }

  return EFI_SUCCESS;
}

EFI_STATUS
InitialPlatformNvsAreaProtocol (
  IN CHIPSET_CONFIGURATION          *SetupVariable
  )
{
  EFI_STATUS                        Status;
  EFI_HANDLE                        Handle;
  PLATFORM_NVS_AREA                 *PlatformNvsAreaPtr;
  OEM_PLATFORM_NVS_AREA             *OemPlatformNvsAreaPtr;
  UINTN                             DataSize;

  DataSize = sizeof (PLATFORM_NVS_AREA);
  Status = gBS->AllocatePool(
                  EfiACPIMemoryNVS,
                  DataSize,
                  (VOID **)&PlatformNvsAreaPtr
                  );
  if (EFI_ERROR (Status)) {
    return Status;
  }

  ZeroMem (PlatformNvsAreaPtr, DataSize);
  mPlatformNvsAreaProtocol.Area = PlatformNvsAreaPtr;

  DataSize = sizeof (OEM_PLATFORM_NVS_AREA);
  Status = gBS->AllocatePool(
                  EfiACPIMemoryNVS,
                  DataSize,
                  (VOID **)&OemPlatformNvsAreaPtr
                  );
  if (EFI_ERROR(Status)) {
    return Status;
  }
  ZeroMem (OemPlatformNvsAreaPtr, DataSize);
  mPlatformNvsAreaProtocol.OemArea = OemPlatformNvsAreaPtr;

  Handle = NULL;
  Status = gBS->InstallMultipleProtocolInterfaces (
                  &Handle,
                  &gPlatformNvsAreaProtocolGuid,
                  &mPlatformNvsAreaProtocol,
                  NULL
                  );

  return Status;
}

EFI_STATUS
EFIAPI
ProtectMemorySPD (
  IN EFI_EVENT    Event,
  IN VOID         *Context
  )
{
  EFI_STATUS                       Status;
  EFI_STATUS                       Status1;
  EFI_STATUS                       Status2;
  EFI_SMBUS_HC_PROTOCOL            *SmbusController;
  SA_POLICY_PROTOCOL               *DxePlatformSaPolicy;
  UINT8                            ChannelASlotMap;
  UINT8                            ChannelBSlotMap;
  BOOLEAN                          SlotPresent;
  UINT8                            Dimm;
  EFI_SMBUS_DEVICE_ADDRESS         SmbusSlaveAddress;
  UINTN                            SmbusOffset;
  UINTN                            SmbusLength;
  UINT8                            Data8;
  MEMORY_DXE_CONFIG                *MemoryDxeConfig = NULL;

  DEBUG ( ( EFI_D_INFO | EFI_D_ERROR, "Protecting Memory SPD Process - Start.\n" ) );

  SmbusController       = NULL;
  DxePlatformSaPolicy   = NULL;
  ChannelASlotMap       = 0x03;
  ChannelBSlotMap       = 0x03;
  SlotPresent           = FALSE;
  Dimm                  = 0;
  SmbusOffset           = 0;
  SmbusLength           = 0;
  Data8                 = 0;

  Status = gBS->LocateProtocol (&gEfiSmmSmbusProtocolGuid, NULL, (VOID **)&SmbusController);
  ASSERT_EFI_ERROR (Status);

  Status1 = gBS->LocateProtocol (&gSaPolicyProtocolGuid, NULL, (VOID **)&DxePlatformSaPolicy);
  ASSERT_EFI_ERROR (Status1);

  Status2 = GetConfigBlock ((VOID *)DxePlatformSaPolicy, &gMemoryDxeConfigGuid, (VOID *)&MemoryDxeConfig);
  ASSERT_EFI_ERROR (Status2);

  if (!(EFI_ERROR (Status) || EFI_ERROR (Status1)|| EFI_ERROR (Status2))) {
    ChannelASlotMap = MemoryDxeConfig->ChannelASlotMap;
    ChannelBSlotMap = MemoryDxeConfig->ChannelBSlotMap;
    for (Dimm = 0 ; Dimm < MEM_CFG_MAX_SOCKETS ; Dimm = Dimm + 1) {
      //
      // Use channel slot map to check whether the Socket is supported in this SKU, some SKU only has 2 Sockets totally
      //
      SlotPresent = FALSE;
      if  (Dimm < 2) {
        if (ChannelASlotMap & (1 << Dimm)) {
          SlotPresent = TRUE;
        }
      } else {
        if (ChannelBSlotMap & (1 << (Dimm - 2))) {
          SlotPresent = TRUE;
        }
      }
      if (!SlotPresent) {
        DEBUG ( ( EFI_D_INFO | EFI_D_ERROR, "  DIMM %x is not in channel slot map.\n", Dimm ) );
        continue;
      }
      //
      // Assign default address, then be overrided by platform policy
      //
      SmbusSlaveAddress.SmbusDeviceAddress = (DIMM0_SPD_ADDRESS >> 1) + Dimm;
      if (DxePlatformSaPolicy != NULL) {
        SmbusSlaveAddress.SmbusDeviceAddress = ( MemoryDxeConfig->SpdAddressTable[Dimm] ) >> 1;
      }
      //
      // BUGBUG :
      //
      //   This step is designed by HuronRiver CRB.
      //
      //   The device can only be read with a non-protected Memory SPD.
      //
      //   Take care if your platform has different behavior.
      //
      SmbusSlaveAddress.SmbusDeviceAddress = SmbusSlaveAddress.SmbusDeviceAddress & ( ~BIT6 );
      SmbusSlaveAddress.SmbusDeviceAddress = SmbusSlaveAddress.SmbusDeviceAddress | ( BIT5 );
      SmbusOffset                          = 0x00;
      SmbusLength                          = 0x01;
      Data8                                = 0x00;
      Status = SmbusController->Execute (
                 SmbusController,
                 SmbusSlaveAddress,
                 SmbusOffset,
                 EfiSmbusReadByte,
                 FALSE,
                 &SmbusLength,
                 &Data8
                 );
      if (!EFI_ERROR (Status)) {
        Status = SmbusController->Execute (
                   SmbusController,
                   SmbusSlaveAddress,
                   SmbusOffset,
                   EfiSmbusWriteByte,
                   FALSE,
                   &SmbusLength,
                   &Data8
                   );
        ASSERT_EFI_ERROR (Status);
        if (EFI_ERROR (Status)) {
          DEBUG ((EFI_D_INFO | EFI_D_ERROR, "  Protect command failure on DIMM %x.\n", Dimm));
        }
      } else {
        DEBUG ((EFI_D_INFO | EFI_D_ERROR, "  No memory plugged in DIMM %x, or it has been protected before.\n", Dimm));
      }
    }
  } else if (EFI_ERROR (Status)) {
    DEBUG ((EFI_D_INFO | EFI_D_ERROR, "  Locate SMBUS Protocol Failure!!\n"));
  } else if (EFI_ERROR (Status1)) {
    DEBUG ((EFI_D_INFO | EFI_D_ERROR, "  Locate Sa Policy Protocol Failure!!\n"));
  } else if (EFI_ERROR (Status2)) {
    DEBUG ((EFI_D_INFO | EFI_D_ERROR, "  Locate Memory Dxe Config Failure!!\n"));
  }
  DEBUG ((EFI_D_INFO | EFI_D_ERROR, "Protecting Memory SPD Process - Done.\n"));

  return EFI_SUCCESS;
}
//[-end-180316-IB07250307-add]//
