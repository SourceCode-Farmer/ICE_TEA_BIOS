/** @file

;******************************************************************************
;* Copyright (c) 2018 - 2021, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/
/** @file
  Source code file for Platform Init PEI module

@copyright
  INTEL CONFIDENTIAL
  Copyright 2014 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains a 'Sample Driver' and is licensed as such under the terms
  of your license agreement with Intel or your vendor. This file may be modified
  by the user, subject to the additional terms of the license agreement.

@par Specification Reference:
**/

#include <Register/Msr.h>
#include <CpuRegs.h>
#include <SetupVariable.h>
#include <Library/IoLib.h>
#include <Library/HobLib.h>
#include <Library/DebugLib.h>
#include <Library/PchInfoLib.h>
#include <Library/BaseMemoryLib.h>
#include <Library/PeiServicesLib.h>
#include <Library/PmcLib.h>
#include <Library/GpioLib.h>
#include <Library/GpioNativeLib.h>
#include <Ppi/EndOfPeiPhase.h>
#include <Library/MtrrLib.h>
#include <Ppi/ReadOnlyVariable2.h>
#include <Guid/SmramMemoryReserve.h>
#include <Library/ConfigBlockLib.h>
#include <Ppi/SiPolicy.h>
#include <PchPolicyCommon.h>
#include <Library/SiPolicyLib.h>

#include <Guid/FirmwareFileSystem2.h>
#include <Protocol/FirmwareVolumeBlock.h>
#include <Guid/SystemNvDataGuid.h>

#include <Library/CnviLib.h>
#include <Library/EcLib.h>
#include <Library/EcMiscLib.h>
#include <Library/PostCodeLib.h>
#include <PlatformPostCode.h>
#include <Library/EcTcssLib.h>
#include <Register/ArchMsr.h>
#include <Register/AdlMsr.h>
#if FixedPcdGetBool(PcdITbtEnable) == 1
#include <TcssDataHob.h>
#endif
#include <Library/ItbtPcieRpLib.h>
#include <Ppi/Spi.h>
#include <Register/MeRegs.h>
#include <Register/HeciRegs.h>
#include <Library/MtrrLib.h>
#include <Library/PciSegmentLib.h>
#if FixedPcdGetBool (PcdDTbtEnable) == 1
#include <Ppi/PeiDTbtPolicy.h>
#include <Library/PeiTbtTaskDispatchLib.h>
#include <Private/Library/PeiDTbtInitLib.h>
#endif
#include <Register/PchRegs.h>
#include <Register/PmcRegs.h>
#include <Register/TelemetryRegs.h>
#include <Library/TelemetryLib.h>
#include <Guid/CrashLog.h>
#include <AttemptUsbFirst.h>
#include <PlatformBoardId.h>
#include <Core/Pei/PeiMain.h>
#include <Library/PchPciBdfLib.h>
#include <Ppi/GraphicsPlatformPolicyPpi.h>
#include <Library/PeiGetFvInfoLib.h>
//[-start-200420-IB17800056-modify]//
#include <Guid/H2OCp.h>
#include <Library/H2OCpLib.h>
#include <Library/PeiOemSvcKernelLib.h>
#include <Library/PeiOemSvcChipsetLib.h>
#include <Library/PeiChipsetSvcLib.h>
#include <Library/PeiChipsetSvcLib.h>
#include <Library/KernelConfigLib.h>
#include <Library/MemoryAllocationLib.h>
#include <Library/VariableLib.h>
#include <Ppi/RomCorrupt.h>
#include <FastRecoveryData.h>
#include <Library/PcdLib.h>
#include <PostCode.h>
#include <ChipsetAccess.h>
#include <HybridGraphicsDefine.h>
#include <Library/FlashRegionLib.h>
#include <Library/RetrieveSpecificFfsInFv.h>
#include <Library/BaseOemSvcChipsetLib.h>
#include <Guid/HybridGraphicsVariable.h>
#include <Ppi/S3RestoreAcpiCallback.h>
//[-start-191225-IB16740000-add]//
#include <PchBdfAssignment.h>
//[-end-191225-IB16740000-add]//
//[-end-200420-IB17800056-modify]//
//[-start-200831-IB17800090-add]//
#include <CpuPcieInfo.h>
#include <PchBdfAssignment.h>
#include <Protocol/DevicePath.h>
#include <Library/CpuPcieInfoFruLib.h>
#include <Library/PchInfoLib.h>
#include <Library/PchPcieRpLib.h>
//[-end-200831-IB17800090-add]//
//[-start-200903-IB17040151-add]//
#include <Library/DeviceInfo2Lib.h>
#include <Library/PrintLib.h>
#include <Library/DevicePathLib.h>
//[-end-200903-IB17040151-add]//

//[-start-200831-IB17800090-add]//
#define IS_PCI_DP(a)    ((((EFI_DEVICE_PATH_PROTOCOL *)a)->Type == HARDWARE_DEVICE_PATH) && (((EFI_DEVICE_PATH_PROTOCOL *)a)->SubType == HW_PCI_DP))
//[-end-200831-IB17800090-add]//

//[-start-200831-IB17800090-add]//
typedef struct {
  UINTN    Device;
  UINTN    Function;
} PCIE_RP_INFO;
//[-end-200831-IB17800090-add]//

//[-start-211022-IB16810168-modify]//
//[-start-200831-IB17800090-add]//
GLOBAL_REMOVE_IF_UNREFERENCED PCIE_RP_INFO mCpuPcieRpInfo[] = {
  //
  //     Device         Function
  //
  { SA_PEG3_DEV_NUM, SA_PEG3_FUN_NUM },
  { SA_PEG0_DEV_NUM, SA_PEG0_FUN_NUM },
  { SA_PEG3_DEV_NUM,            0x02 }
};
//[-end-200831-IB17800090-add]//
//[-end-211022-IB16810168-modify]//

//[-start-200831-IB17800090-add]//
GLOBAL_REMOVE_IF_UNREFERENCED PCIE_RP_INFO mPchPcieRpInfo[] = {
  //
  //             Device                                  Function
  //
  { PCI_DEVICE_NUMBER_PCH_PCIE_ROOT_PORT_1, PCI_FUNCTION_NUMBER_PCH_PCIE_ROOT_PORT_1 },
  { PCI_DEVICE_NUMBER_PCH_PCIE_ROOT_PORT_2, PCI_FUNCTION_NUMBER_PCH_PCIE_ROOT_PORT_2 },
  { PCI_DEVICE_NUMBER_PCH_PCIE_ROOT_PORT_3, PCI_FUNCTION_NUMBER_PCH_PCIE_ROOT_PORT_3 },
  { PCI_DEVICE_NUMBER_PCH_PCIE_ROOT_PORT_4, PCI_FUNCTION_NUMBER_PCH_PCIE_ROOT_PORT_4 },
  { PCI_DEVICE_NUMBER_PCH_PCIE_ROOT_PORT_5, PCI_FUNCTION_NUMBER_PCH_PCIE_ROOT_PORT_5 },
  { PCI_DEVICE_NUMBER_PCH_PCIE_ROOT_PORT_6, PCI_FUNCTION_NUMBER_PCH_PCIE_ROOT_PORT_6 },
  { PCI_DEVICE_NUMBER_PCH_PCIE_ROOT_PORT_7, PCI_FUNCTION_NUMBER_PCH_PCIE_ROOT_PORT_7 },
  { PCI_DEVICE_NUMBER_PCH_PCIE_ROOT_PORT_8, PCI_FUNCTION_NUMBER_PCH_PCIE_ROOT_PORT_8 },
  { PCI_DEVICE_NUMBER_PCH_PCIE_ROOT_PORT_9, PCI_FUNCTION_NUMBER_PCH_PCIE_ROOT_PORT_9 },
  { PCI_DEVICE_NUMBER_PCH_PCIE_ROOT_PORT_10, PCI_FUNCTION_NUMBER_PCH_PCIE_ROOT_PORT_10 },
  { PCI_DEVICE_NUMBER_PCH_PCIE_ROOT_PORT_11, PCI_FUNCTION_NUMBER_PCH_PCIE_ROOT_PORT_11 },
  { PCI_DEVICE_NUMBER_PCH_PCIE_ROOT_PORT_12, PCI_FUNCTION_NUMBER_PCH_PCIE_ROOT_PORT_12 },
  { PCI_DEVICE_NUMBER_PCH_PCIE_ROOT_PORT_13, PCI_FUNCTION_NUMBER_PCH_PCIE_ROOT_PORT_13 },
  { PCI_DEVICE_NUMBER_PCH_PCIE_ROOT_PORT_14, PCI_FUNCTION_NUMBER_PCH_PCIE_ROOT_PORT_14 },
  { PCI_DEVICE_NUMBER_PCH_PCIE_ROOT_PORT_15, PCI_FUNCTION_NUMBER_PCH_PCIE_ROOT_PORT_15 },
  { PCI_DEVICE_NUMBER_PCH_PCIE_ROOT_PORT_16, PCI_FUNCTION_NUMBER_PCH_PCIE_ROOT_PORT_16 },
  { PCI_DEVICE_NUMBER_PCH_PCIE_ROOT_PORT_17, PCI_FUNCTION_NUMBER_PCH_PCIE_ROOT_PORT_17 },
  { PCI_DEVICE_NUMBER_PCH_PCIE_ROOT_PORT_18, PCI_FUNCTION_NUMBER_PCH_PCIE_ROOT_PORT_18 },
  { PCI_DEVICE_NUMBER_PCH_PCIE_ROOT_PORT_19, PCI_FUNCTION_NUMBER_PCH_PCIE_ROOT_PORT_19 },
  { PCI_DEVICE_NUMBER_PCH_PCIE_ROOT_PORT_20, PCI_FUNCTION_NUMBER_PCH_PCIE_ROOT_PORT_20 },
  { PCI_DEVICE_NUMBER_PCH_PCIE_ROOT_PORT_21, PCI_FUNCTION_NUMBER_PCH_PCIE_ROOT_PORT_21 },
  { PCI_DEVICE_NUMBER_PCH_PCIE_ROOT_PORT_22, PCI_FUNCTION_NUMBER_PCH_PCIE_ROOT_PORT_22 },
  { PCI_DEVICE_NUMBER_PCH_PCIE_ROOT_PORT_23, PCI_FUNCTION_NUMBER_PCH_PCIE_ROOT_PORT_23 },
  { PCI_DEVICE_NUMBER_PCH_PCIE_ROOT_PORT_24, PCI_FUNCTION_NUMBER_PCH_PCIE_ROOT_PORT_24 }
};
//[-end-200831-IB17800090-add]//

//[-start-200903-IB17040151-add]//
CHAR8    *mPciBarAttribName[] = {
  ATTR_NAME_PCIE_PEI_BAR0,
  ATTR_NAME_PCIE_PEI_BAR1,
  ATTR_NAME_PCIE_PEI_BAR2,
  ATTR_NAME_PCIE_PEI_BAR3,
  ATTR_NAME_PCIE_PEI_BAR4,
  ATTR_NAME_PCIE_PEI_BAR5
};
//[-end-200903-IB17040151-add]//

/**
  Initializes the valid bits mask and valid address mask for MTRRs.

  This function initializes the valid bits mask and valid address mask for MTRRs.

  @param[out]  MtrrValidBitsMask     The mask for the valid bit of the MTRR
  @param[out]  MtrrValidAddressMask  The valid address mask for the MTRR

**/
VOID
MtrrLibInitializeMtrrMask (
  OUT UINT64 *MtrrValidBitsMask,
  OUT UINT64 *MtrrValidAddressMask
  );

/**
  Convert variable MTRRs to a RAW MTRR_MEMORY_RANGE array.
  One MTRR_MEMORY_RANGE element is created for each MTRR setting.
  The routine doesn't remove the overlap or combine the near-by region.

  @param[in]   VariableSettings      The variable MTRR values to shadow
  @param[in]   VariableMtrrCount     The number of variable MTRRs
  @param[in]   MtrrValidBitsMask     The mask for the valid bit of the MTRR
  @param[in]   MtrrValidAddressMask  The valid address mask for MTRR
  @param[out]  VariableMtrr          The array to shadow variable MTRRs content

  @return      Number of MTRRs which has been used.

**/
UINT32
MtrrLibGetRawVariableRanges (
  IN  MTRR_VARIABLE_SETTINGS  *VariableSettings,
  IN  UINTN                   VariableMtrrCount,
  IN  UINT64                  MtrrValidBitsMask,
  IN  UINT64                  MtrrValidAddressMask,
  OUT MTRR_MEMORY_RANGE       *VariableMtrr
  );

/**
  Apply the variable MTRR settings to memory range array.

  @param[in]      VariableMtrr      The variable MTRR array.
  @param[in]      VariableMtrrCount The count of variable MTRRs.
  @param[in, out] Ranges            Return the memory range array with new MTRR settings applied.
  @param[in]      RangeCapacity     The capacity of memory range array.
  @param[in, out] RangeCount        Return the count of memory range.

  @retval RETURN_SUCCESS            The memory range array is returned successfully.
  @retval RETURN_OUT_OF_RESOURCES   The count of memory ranges exceeds capacity.
**/
RETURN_STATUS
MtrrLibApplyVariableMtrrs (
  IN     CONST MTRR_MEMORY_RANGE *VariableMtrr,
  IN     UINT32                  VariableMtrrCount,
  IN OUT MTRR_MEMORY_RANGE       *Ranges,
  IN     UINTN                   RangeCapacity,
  IN OUT UINTN                   *RangeCount
  );

/**
  This function attempts to set the attributes into MTRR setting buffer.

  @param[in, out] MtrrSetting   - A buffer holding all MTRRs content.
  @param[in]      Ranges        - Array holding memory type settings.
  @param[in]      RangeCount    - Memory range count in the array.

  @retval Count of used variable Mtrrs
**/
EFI_STATUS
EFIAPI
EOPSetMemoryAttributesInMtrrSettings (
  IN OUT MTRR_SETTINGS      *MtrrSetting,
  IN     MTRR_MEMORY_RANGE  *Ranges,
  IN     UINTN              RangeCount
  )
{
  EFI_STATUS        Status;
  UINTN             Index;
  UINTN             HighIndex;
  UINT64            TopHighMemory;

  Status = EFI_NOT_FOUND;

  for (Index = 0, HighIndex = 0xFF; Index < RangeCount; Index++) {
    //
    // Set Mtrr variables from 1M.
    //
    if (Ranges[Index].BaseAddress < 0x100000) {
      Ranges[Index].Length -= 0x100000;
      Ranges[Index].BaseAddress = 0x100000;
    }
    if ((Ranges[Index].BaseAddress >= SIZE_4GB) && (Ranges[Index].Type == CacheWriteBack)) {
      HighIndex = Index;                       // Set above 4G attributes at the latest step.
    } else {
      Status = MtrrSetMemoryAttributeInMtrrSettings (
                 MtrrSetting,
                 Ranges[Index].BaseAddress,
                 Ranges[Index].Length,
                 Ranges[Index].Type
                 );
      ASSERT_EFI_ERROR (Status);
    }
  }
  if (HighIndex != 0xFF) {
    TopHighMemory = Ranges[HighIndex].BaseAddress + Ranges[HighIndex].Length;
    //
    // Try to cover memory as mmuch as we can.
    // In later phase boot loader code can re-configure MTRR to exclude flash region and get back above 4GB coverage.
    //
    do {
      Status = MtrrSetMemoryAttributeInMtrrSettings (
                 MtrrSetting,
                 SIZE_4GB,
                 TopHighMemory - SIZE_4GB,
                 CacheWriteBack
                 );
      if (TopHighMemory > SIZE_4GB) {
        TopHighMemory = RShiftU64 (TopHighMemory, 1);
      }
    } while ((EFI_SUCCESS != Status) && (TopHighMemory > SIZE_4GB));
  }

  return Status;
}


EFI_STATUS
EFIAPI
PlatformDebugStateChecksCallback (
  IN CONST EFI_PEI_SERVICES     **PeiServices,
  IN EFI_PEI_NOTIFY_DESCRIPTOR  *NotifyDescriptor,
  IN VOID                       *Ppi
  );

#if FixedPcdGetBool(PcdFspModeSelection) == 1
static EFI_PEI_NOTIFY_DESCRIPTOR  mPlatformDebugStateChecksNotifyList  = {
  (EFI_PEI_PPI_DESCRIPTOR_NOTIFY_CALLBACK | EFI_PEI_PPI_DESCRIPTOR_TERMINATE_LIST),
  &gFspSiliconInitDonePpiGuid,
  (EFI_PEIM_NOTIFY_ENTRY_POINT) PlatformDebugStateChecksCallback
};
#else
static EFI_PEI_NOTIFY_DESCRIPTOR  mPlatformDebugStateChecksNotifyList  = {
  (EFI_PEI_PPI_DESCRIPTOR_NOTIFY_CALLBACK | EFI_PEI_PPI_DESCRIPTOR_TERMINATE_LIST),
  &gEfiEndOfPeiSignalPpiGuid,
  (EFI_PEIM_NOTIFY_ENTRY_POINT) PlatformDebugStateChecksCallback
};
#endif

EFI_STATUS
EFIAPI
PlatformInitAdvancedEndOfPei (
  IN CONST EFI_PEI_SERVICES     **PeiServices,
  IN EFI_PEI_NOTIFY_DESCRIPTOR  *NotifyDescriptor,
  IN VOID                       *Ppi
  );

static EFI_PEI_NOTIFY_DESCRIPTOR  mEndOfPeiNotifyList = {
  (EFI_PEI_PPI_DESCRIPTOR_NOTIFY_CALLBACK | EFI_PEI_PPI_DESCRIPTOR_TERMINATE_LIST),
  &gEfiEndOfPeiSignalPpiGuid,
  (EFI_PEIM_NOTIFY_ENTRY_POINT) PlatformInitAdvancedEndOfPei
};

#if FixedPcdGetBool(PcdAdlLpSupport) == 1
EFI_STATUS
EFIAPI
GetPeiPlatformLidStatus (
  OUT LID_STATUS  *CurrentLidStatus
  );

EFI_STATUS
EFIAPI
GetVbtData (
  OUT EFI_PHYSICAL_ADDRESS *VbtAddress,
  OUT UINT32               *VbtSize
  );

PEI_GRAPHICS_PLATFORM_POLICY_PPI PeiGraphicsPlatform = {
  PEI_GRAPHICS_PLATFORM_POLICY_REVISION,
  GetPeiPlatformLidStatus,
  GetVbtData
};

EFI_PEI_PPI_DESCRIPTOR  mPeiGraphicsPlatformPpi = {
  (EFI_PEI_PPI_DESCRIPTOR_PPI | EFI_PEI_PPI_DESCRIPTOR_TERMINATE_LIST),
  &gPeiGraphicsPlatformPpiGuid,
  &PeiGraphicsPlatform
};

EFI_STATUS
EFIAPI
GetVbtData (
  OUT EFI_PHYSICAL_ADDRESS *VbtAddress,
  OUT UINT32               *VbtSize
  )
{
  EFI_STATUS                      Status;
  EFI_STATUS                      Status1;
  SA_SETUP                        SaSetup;
  UINTN                           VarSize;
  EFI_PEI_READ_ONLY_VARIABLE2_PPI *VariableServices;
  EFI_GUID                        FileGuid;
  EFI_GUID                        BmpImageGuid;
  VOID                            *Buffer;
  UINT32                          Size;

  Size    = 0;
  Buffer  = NULL;
  VarSize = sizeof (SA_SETUP);

  //
  // Locate system configuration variable
  //
  Status1 = PeiServicesLocatePpi (
             &gEfiPeiReadOnlyVariable2PpiGuid, // GUID
             0,                                // INSTANCE
             NULL,                             // EFI_PEI_PPI_DESCRIPTOR
             (VOID **) &VariableServices       // PPI
             );
  ASSERT_EFI_ERROR (Status1);
  Status = VariableServices->GetVariable(
                                VariableServices,
                                L"SaSetup",
                                &gSaSetupVariableGuid,
                                NULL,
                                &VarSize,
                                &SaSetup
                                );
  ASSERT_EFI_ERROR(Status);

  DEBUG((DEBUG_INFO, "GetVbtData Entry\n"));

  if (SaSetup.VbtSelect == VBT_SELECT_MIPI) {
    CopyMem (&BmpImageGuid, PcdGetPtr(PcdVbtMipiGuid), sizeof(BmpImageGuid));
  } else {
    CopyMem (&BmpImageGuid, PcdGetPtr(PcdIntelGraphicsVbtFileGuid), sizeof(BmpImageGuid));
  }

  CopyMem(&FileGuid, &BmpImageGuid, sizeof(FileGuid));
  PeiGetSectionFromFv(FileGuid, &Buffer, &Size);
  if (Buffer == NULL) {
    DEBUG((DEBUG_ERROR, "Could not locate VBT\n"));
  } else {
    DEBUG ((DEBUG_INFO, "GetVbtData Buffer is 0x%x\n", Buffer));
    DEBUG ((DEBUG_INFO, "GetVbtData Size is 0x%x\n", Size));
    *VbtAddress = (EFI_PHYSICAL_ADDRESS)(UINTN)Buffer;
    *VbtSize    = Size;
  }
  DEBUG((DEBUG_INFO, "GetVbtData exit\n"));

  return EFI_SUCCESS;
}
#else
EFI_STATUS
EFIAPI
PeiGraphicsPlatformInit (
  IN CONST EFI_PEI_SERVICES     **PeiServices,
  IN EFI_PEI_NOTIFY_DESCRIPTOR  *NotifyDescriptor,
  IN VOID                       *Ppi
  );

EFI_STATUS
EFIAPI
GetPeiPlatformLidStatus (
  OUT LID_STATUS  *CurrentLidStatus
  );

static EFI_PEI_NOTIFY_DESCRIPTOR  mPeiGraphicsPlatformNotifyList = {
  (EFI_PEI_PPI_DESCRIPTOR_NOTIFY_CALLBACK | EFI_PEI_PPI_DESCRIPTOR_TERMINATE_LIST),
  &gPeiGraphicsPlatformPpiGuid,
  (EFI_PEIM_NOTIFY_ENTRY_POINT) PeiGraphicsPlatformInit
};

/**
  This function will pass data to Pei graphics PEIM.

  @param[in]  PeiServices  Pointer to PEI Services Table.
  @param[in]  NotifyDesc   Pointer to the descriptor for the Notification event that
                           caused this function to execute.
  @param[in]  Ppi          Pointer to the PPI data associated with this function.

  @retval     EFI_SUCCESS  The function completes successfully
  @retval     others
**/
EFI_STATUS
EFIAPI
PeiGraphicsPlatformInit (
  IN CONST EFI_PEI_SERVICES     **PeiServices,
  IN EFI_PEI_NOTIFY_DESCRIPTOR  *NotifyDescriptor,
  IN VOID                       *Ppi
  )
{
  EFI_STATUS                         Status;
  PEI_GRAPHICS_PLATFORM_POLICY_PPI   *GfxPlatformPolicyPpi;

  DEBUG ((DEBUG_INFO, "PeiGraphicsPlatformInit: Begin \n"));

  Status = EFI_SUCCESS;
  ///
  /// Locate GfxPlatformPolicyPpi
  ///
  Status = PeiServicesLocatePpi (&gPeiGraphicsPlatformPpiGuid, 0, NULL, (VOID *) &GfxPlatformPolicyPpi);
  if (EFI_ERROR (Status)) {
    DEBUG ((DEBUG_WARN, "Failed to locate Ppi GfxPlatformPolicyPpi.\n"));
    return Status;
  }

  GfxPlatformPolicyPpi->Revision = PEI_GRAPHICS_PLATFORM_POLICY_REVISION;
  GfxPlatformPolicyPpi->GetPlatformLidStatus = (GET_PLATFORM_LID_STATUS) GetPeiPlatformLidStatus;

  DEBUG ((DEBUG_INFO, "PeiGraphicsPlatformInit: End \n"));

  return Status;
}

#endif

//[-start-200831-IB17800090-add]//
EFI_STATUS
EFIAPI
UpdateDeviceInfoPcd (
  IN CONST EFI_PEI_SERVICES     **PeiServices,
  IN EFI_PEI_NOTIFY_DESCRIPTOR  *NotifyDescriptor,
  IN VOID                       *Ppi
  );
//[-end-200831-IB17800090-add]//

//[-start-210823-IB16810160-modify]//
/**
  This function will return Lid Status in PEI phase.

  @param[out] CurrentLidStatus

  @retval     EFI_SUCCESS
  @retval     EFI_UNSUPPORTED
**/

EFI_STATUS
EFIAPI
GetPeiPlatformLidStatus (
  OUT LID_STATUS  *CurrentLidStatus
  )
{
  EFI_STATUS    Status;
  EFI_STATUS    EcGetLidState;
  BOOLEAN       LidIsOpen;

  EcGetLidState = EFI_SUCCESS;
  LidIsOpen = TRUE;

  DEBUG_OEM_SVC ((DEBUG_INFO, "Base OemChipsetServices Call: OemSvcEcGetLidState \n"));
  Status = OemSvcEcGetLidState (&EcGetLidState, &LidIsOpen);
  DEBUG_OEM_SVC ((DEBUG_INFO, "Base OemChipsetServices OemSvcEcGetLidState Status: %r\n", Status));

  ASSERT (!EFI_ERROR (EcGetLidState));

  *CurrentLidStatus = LidOpen;
  if (!EFI_ERROR (EcGetLidState)) {
    if (!LidIsOpen) {
      //
      // If get lid state form EC successfully and lid is closed.
      //
      *CurrentLidStatus = LidClosed;
    }
  } else {
    DEBUG ((EFI_D_INFO | EFI_D_ERROR, "EcGetLidState ERROR in GopPolicy! Status is %r.\n", EcGetLidState));
  }

  return EcGetLidState;
}
//[-end-210823-IB16810160-modify]//

/**
  Install Firmware Volume Hob's once there is main memory.It installs
  firmware volume Hobs when main memory is available.

  @param PeiServices            General purpose services available to every PEIM.
  @param NotifyDescriptor       Pointer to EFI_PEI_NOTIFY_DESCRIPTOR
  @param Ppi                    EFI_PEI_FV_FILE_LOADER_PPI

  @retval   EFI_SUCCESS         if the interface could be successfully installed.

**/
EFI_STATUS
EFIAPI
MemoryDiscoveredPpiNotifyPlatformInitCallback (
  IN EFI_PEI_SERVICES           **PeiServices,
  IN EFI_PEI_NOTIFY_DESCRIPTOR  *NotifyDescriptor,
  IN VOID                       *Ppi
  )
{
  EFI_STATUS                          Status;
  EFI_BOOT_MODE                       BootMode;
  UINT8                               *PublicKey;
  UINT32                              PublicKeySize;
  EFI_PEI_PCI_CFG2_PPI                *PciCfg;
  EFI_STATUS                          OemSvcStatus;

  POST_CODE (PEI_MEMORY_CALLBACK);
  DEBUG ((EFI_D_INFO, "Enter MemoryDiscoveredPpiNotifyPlatformInitCallback\n"));
  PciCfg = (**PeiServices).PciCfg;

  //
  // Get boot mode
  //
  Status = PeiServicesGetBootMode (&BootMode);
  ASSERT_EFI_ERROR (Status);

  if (BootMode == BOOT_ON_S3_RESUME) {
    POST_CODE (S3_MEMORY_CALLBACK);
  }

  Status = RetrieveSpecificFfsInFv (PcdGetPtr (PcdSecureFlashPublicKeyFile), (VOID **)&PublicKey, &PublicKeySize);
  if (Status == EFI_SUCCESS) {
    BuildGuidDataHob ((EFI_GUID *)PcdGetPtr (PcdSecureFlashPublicKeyFile), PublicKey, PublicKeySize);
  }

  //
  // OemServices
  //
//[-start-190724-IB17700055-modify]//
  DEBUG_OEM_SVC ((DEBUG_INFO, "OemKernelServices Call: OemSvcInitMemoryDiscoverCallback \n"));
  OemSvcStatus = OemSvcInitMemoryDiscoverCallback (&BootMode);
  DEBUG_OEM_SVC ((DEBUG_INFO, "OemKernelServices OemSvcInitMemoryDiscoverCallback Status: %r\n", OemSvcStatus));
//[-end-190724-IB17700055-modify]//

  BuildResourceDescriptorHob (
    EFI_RESOURCE_FIRMWARE_DEVICE,
    (EFI_RESOURCE_ATTRIBUTE_PRESENT | EFI_RESOURCE_ATTRIBUTE_INITIALIZED | EFI_RESOURCE_ATTRIBUTE_UNCACHEABLE),
    FdmGetBaseAddr (),
    FdmGetFlashAreaSize ()
    );

  //
  // Create a CPU hand-off information
  //
  DEBUG ((EFI_D_INFO, "MemoryDiscoveredPpiNotifyPlatformInitCallback Exit\n"));

  return Status;
}

/**
  To force system into Recovery mode.

  @param PeiServices            General purpose services available to every PEIM.
  @param NotifyDescriptor       Pointer to EFI_PEI_NOTIFY_DESCRIPTOR
  @param Ppi                    EFI_PEI_FV_FILE_LOADER_PPI

  @retval EFI_SUCCESS           Enter Recovery mode OK.

**/
EFI_STATUS
ForceEnterRecoveryEntry (
  IN EFI_PEI_SERVICES           **PeiServices,
  IN EFI_PEI_NOTIFY_DESCRIPTOR  *NotifyDescriptor,
  IN VOID                       *Ppi
  )
{
  EFI_STATUS                          OemSvcStatus;
  //
  // Due to there is no common method to set a recovery request, so let OEM to decide the method.
  // (OemServices)
  //
//[-start-190724-IB17700055-modify]//
  DEBUG_OEM_SVC ((DEBUG_INFO, "OemKernelServices Call: OemSvcSetRecoveryRequest \n"));
  OemSvcStatus = OemSvcSetRecoveryRequest ();
  DEBUG_OEM_SVC ((DEBUG_INFO, "OemKernelServices OemSvcSetRecoveryRequest Status: %r\n", OemSvcStatus));
//[-end-190724-IB17700055-modify]//

  //
  // Trigger a reset to force system enter recovery mode.
  //
  PeiServicesResetSystem ();

  return EFI_SUCCESS;
}

/**
  Enable or disable dGPU HD Audio device during S3 resume.

  @param[in]  PeiServices  General purpose services available to every PEIM.

  @retval None.

**/
STATIC
VOID
ControlHdAudio (
  IN CONST EFI_PEI_SERVICES                   **PeiServices
  )
{
  EFI_BOOT_MODE                               BootMode;
  EFI_PEI_READ_ONLY_VARIABLE2_PPI             *VariableServices;
  EFI_STATUS                                  Status;
  HG_VARIABLE_CONFIGURATION                   HgData;
  UINT8                                       HdAudioFlag;
  UINT8                                       DgpuBus;
  UINTN                                       VariableSize;

  //
  // DEVEN register bit 4 is Internal Graphics Engine (D2EN).
  //
  if ((MmPci8 (0, SA_MC_BUS, SA_MC_DEV, SA_MC_FUN, R_SA_DEVEN) & B_SA_DEVEN_D2EN_MASK) == 0) {
    return;
  }

  Status = PeiServicesLocatePpi (
             &gEfiPeiReadOnlyVariable2PpiGuid,
             0,
             NULL,
             (VOID **)&VariableServices
             );
  if (EFI_ERROR (Status)) {
    return;
  }

  VariableSize = sizeof (HG_VARIABLE_CONFIGURATION);
  Status = VariableServices->GetVariable (
                               VariableServices,
                               L"HybridGraphicsVariable",
                               &gH2OHybridGraphicsVariableGuid,
                               NULL,
                               &VariableSize,
                               &HgData
                               );
  if (EFI_ERROR (Status)) {
    return;
  }

  DgpuBus = HgData.OptimusVariable.DgpuBus;
  if (MmPci16 (0, DgpuBus, DGPU_DEVICE_NUM, DGPU_FUNCTION_NUM, PCI_VENDOR_ID_OFFSET) == NVIDIA_VID) {
    Status = PeiServicesGetBootMode (&BootMode);
    if (EFI_ERROR (Status)) {
      return;
    }

    if (BootMode == BOOT_ON_S3_RESUME) {
      if (HgData.OptimusVariable.NvDgpuGen == NV_GEN_17) {
        //
        // N17: Based on the value of OptimusFlag to enable or disable dGPU HDA during S3 resume.
        //
        HdAudioFlag = HgData.OptimusVariable.OptimusFlag;
        if ((HdAudioFlag & BIT0) == 0) {
          MmPci32And (0, DgpuBus, DGPU_DEVICE_NUM, DGPU_FUNCTION_NUM, NVIDIA_DGPU_HDA_REGISTER, ~(BIT25));
        } else {
          MmPci32Or (0, DgpuBus, DGPU_DEVICE_NUM, DGPU_FUNCTION_NUM, NVIDIA_DGPU_HDA_REGISTER, BIT25);
        }
      } else {
        //
        // N18: Enable dGPU HDA during S3 resume.
        //
        MmPci32Or (0, DgpuBus, DGPU_DEVICE_NUM, DGPU_FUNCTION_NUM, NVIDIA_DGPU_HDA_REGISTER, BIT25);
      }
    }
  }
}

/**
 A callback function is triggered by gPeiS3RestoreAcpiCallbackPpiGuid PPI installation.

 @param[in]         PeiServices         General purpose services available to every PEIM.
 @param[in]         NotifyDescriptor    A pointer to notification structure this PEIM registered on install.
 @param[in]         Ppi                 A pointer to S3RestoreAcpiCallback PPI

 @retval            EFI_SUCCESS         Procedure complete.
*/
STATIC
EFI_STATUS
S3RestoreAcpiNotifyCallback (
  IN EFI_PEI_SERVICES            **PeiServices,
  IN EFI_PEI_NOTIFY_DESCRIPTOR   *NotifyDescriptor,
  IN VOID                        *Ppi
  )
{

  if (FeaturePcdGet (PcdNvidiaOptimusSupported)) {
    ControlHdAudio ( (CONST EFI_PEI_SERVICES **)PeiServices);
 }

  return EFI_SUCCESS;
}

EFI_PEI_NOTIFY_DESCRIPTOR mSetS3RestoreAcpiNotify = {
  (EFI_PEI_PPI_DESCRIPTOR_NOTIFY_CALLBACK | EFI_PEI_PPI_DESCRIPTOR_TERMINATE_LIST),
  &gPeiS3RestoreAcpiCallbackPpiGuid,
  S3RestoreAcpiNotifyCallback
  };

static EFI_PEI_NOTIFY_DESCRIPTOR      mNotifyPpiList[] = {
  {
    EFI_PEI_PPI_DESCRIPTOR_NOTIFY_CALLBACK,
    &gPeiRomCorruptPpiGuid,
    ForceEnterRecoveryEntry
  },
  {
    (EFI_PEI_PPI_DESCRIPTOR_NOTIFY_CALLBACK | EFI_PEI_PPI_DESCRIPTOR_TERMINATE_LIST),
    &gEfiPeiMemoryDiscoveredPpiGuid,
    MemoryDiscoveredPpiNotifyPlatformInitCallback
  }
};
//[-end-200420-IB17800056-modify]//
//[-start-200831-IB17800090-add]//
#if FixedPcdGet8 (PcdFspModeSelection) == 1
EFI_PEI_NOTIFY_DESCRIPTOR mUpdateDeviceInfoPcdNotify = {
  (EFI_PEI_PPI_DESCRIPTOR_NOTIFY_DISPATCH | EFI_PEI_PPI_DESCRIPTOR_TERMINATE_LIST),
  &gEfiEndOfPeiSignalPpiGuid,
  (EFI_PEIM_NOTIFY_ENTRY_POINT) UpdateDeviceInfoPcd
};
#else
EFI_PEI_NOTIFY_DESCRIPTOR mUpdateDeviceInfoPcdNotify = {
  (EFI_PEI_PPI_DESCRIPTOR_NOTIFY_DISPATCH | EFI_PEI_PPI_DESCRIPTOR_TERMINATE_LIST),
  &gEfiEndOfPeiSignal2PpiGuid,
  (EFI_PEIM_NOTIFY_ENTRY_POINT) UpdateDeviceInfoPcd
};
#endif
//[-end-200831-IB17800090-add]//

//[-start-190125-IB15410227-add]//
/**
  Get ChipsetInit module pointer and size.

  @param[in] FvHeader        - Pointer to the firmware volume header to be checked.
  @param[in] GuidPtr         - Pointer to the Guid of the binary to find with in the FV.
  @param[in][out] ModulePtr  - Pointer to the specific binary within the FV.
  @param[in][out] ModuleSize - Size of the specific binary.

  @retval EFI_SUCCESS        - ChipsetInit module found.
  @retval EFI_NOT_FOUND      - ChipsetInit module not found.
**/
EFI_STATUS
FindChipsetInitBinaryPtr (
  IN EFI_FIRMWARE_VOLUME_HEADER *FvHeader,
  IN EFI_GUID                   *GuidPtr,
  IN OUT UINT32                 *ModulePtr,
  IN OUT UINT32                 *ModuleSize
  )
{
  EFI_STATUS                    Status;
  UINT32                        ModuleAddr;
  EFI_FFS_FILE_HEADER           *FfsFile;

  ModuleAddr = 0;
  FfsFile    = NULL;

  DEBUG ((DEBUG_INFO, "FindChipsetInitBinaryPtr:\n"));
  while (TRUE) {
    ///
    /// Locate Firmware File System file within Firmware Volume
    ///
    Status = PeiServicesFfsFindNextFile (EFI_FV_FILETYPE_RAW, FvHeader, (VOID **)&FfsFile);
    if (EFI_ERROR(Status)) {
      DEBUG(( DEBUG_ERROR, "Module can not find\n"));
      return EFI_NOT_FOUND;
    }

    ///
    /// Validate that the found Firmware File System file is the ChipsetInit Module
    ///
    if (CompareGuid (&(FfsFile->Name), GuidPtr)) {
      EFI_FV_FILE_INFO2         FileInfo;

      Status = PeiServicesFfsGetFileInfo2 (FfsFile, &FileInfo);
      if (!EFI_ERROR (Status)) {
        *ModulePtr = (UINT32)FileInfo.Buffer;
        *ModuleSize = FileInfo.BufferSize;
      }

      DEBUG(( DEBUG_INFO, "Chipset Init Binary FileInfo Location: %x\n", *ModulePtr));
      DEBUG(( DEBUG_INFO, "Chipset Init Binary FileInfo Size: %x\n", *ModuleSize));
      return Status;
    }
  }
}
//[-end-190125-IB15410227-add]//
#if FixedPcdGetBool (PcdDTbtEnable) == 1
/**
  This function handles TbtInit task at the end of PEI

  @param[in]  PeiServices  Pointer to PEI Services Table.
  @param[in]  NotifyDesc   Pointer to the descriptor for the Notification event that
                           caused this function to execute.
  @param[in]  Ppi          Pointer to the PPI data associated with this function.

  @retval     EFI_SUCCESS        The function completes successfully
  @retval     RETURN_UNSUPPORTED DTBT is not supported
**/
EFI_STATUS
EFIAPI
DTbtInitEndOfPei (
  VOID
  )
{
  EFI_STATUS      Status;
  UINT8           Index;
  BOOLEAN         DTbtExisted;
  PEI_DTBT_POLICY *PeiDTbtConfig;

  DEBUG ((DEBUG_INFO, "DTbtInitEndOfPei Entry\n"));

  Status       = EFI_SUCCESS;
  PeiDTbtConfig = NULL;
  Index        = 0;
  DTbtExisted  = FALSE;

  Status = PeiServicesLocatePpi (
             &gPeiDTbtPolicyPpiGuid,
             0,
             NULL,
             (VOID **) &PeiDTbtConfig
             );
  if (EFI_ERROR(Status)) {
    DEBUG ((DEBUG_ERROR, " gPeiDTbtPolicyPpiGuid Not installed!!!\n"));
    return RETURN_UNSUPPORTED;
  }

  if (PeiDTbtConfig != NULL) {
    for (Index = 0; Index < MAX_DTBT_CONTROLLER_NUMBER; Index++) {
      if (PeiDTbtConfig->DTbtControllerConfig[Index].DTbtControllerEn == 1) {
        DTbtExisted = TRUE;
        break;
      }
    }

    if (DTbtExisted == TRUE) {
      //
      // Dispatch DTBT task table
      //
      TbtTaskDistpach (DTbtCallTable, (VOID *) PeiDTbtConfig);
    }
  }

  return EFI_SUCCESS;
}
#endif

/**
  Update MTRR setting in EndOfPei phase.

  @retval  EFI_SUCCESS  The function completes successfully.
  @retval  Others       Some error occurs.
**/
EFI_STATUS
EFIAPI
SetCacheMtrrAfterEndOfPei (
  VOID
  )
{
  EFI_STATUS                            Status;
  EFI_PEI_HOB_POINTERS                  Hob;
  UINTN                                 Index;
  UINT64                                SmramSize;
  UINT64                                SmramBase;
  EFI_SMRAM_HOB_DESCRIPTOR_BLOCK        *SmramHobDescriptorBlock;
  MTRR_SETTINGS                         MtrrSetting;
  UINTN                                 RangeCount;
  UINT32                                VariableMtrrCount;
  UINT64                                MtrrValidBitsMask;
  UINT64                                MtrrValidAddressMask;
  MTRR_MEMORY_RANGE                     RawVariableRanges[MTRR_NUMBER_OF_VARIABLE_MTRR];
  MTRR_MEMORY_RANGE                     Ranges[MTRR_NUMBER_OF_VARIABLE_MTRR];
  MTRR_SETTINGS                         UCMtrrSetting;
  //
  // PI SMM IPL can't set SMRAM to WB because at that time CPU ARCH protocol is not available.
  // Set cacheability of SMRAM to WB here to improve SMRAM initialization performance.
  //
  SmramSize = 0;
  SmramBase = 0;
  Status = PeiServicesGetHobList ((VOID **) &Hob.Raw);
  while (!END_OF_HOB_LIST (Hob)) {
    if (Hob.Header->HobType == EFI_HOB_TYPE_GUID_EXTENSION) {
      if (CompareGuid (&Hob.Guid->Name, &gEfiSmmSmramMemoryGuid)) {
        SmramHobDescriptorBlock = (EFI_SMRAM_HOB_DESCRIPTOR_BLOCK *) (Hob.Guid + 1);
        for (Index = 0; Index < SmramHobDescriptorBlock->NumberOfSmmReservedRegions; Index++) {
          if (SmramHobDescriptorBlock->Descriptor[Index].PhysicalStart > 0x100000) {
            SmramSize += SmramHobDescriptorBlock->Descriptor[Index].PhysicalSize;
            if (SmramBase == 0 || SmramBase > SmramHobDescriptorBlock->Descriptor[Index].CpuStart) {
              SmramBase = SmramHobDescriptorBlock->Descriptor[Index].CpuStart;
            }
          }
        }
        break;
      }
    }
    Hob.Raw = GET_NEXT_HOB (Hob);
  }

  MtrrGetAllMtrrs (&MtrrSetting);
  VariableMtrrCount = GetVariableMtrrCount ();
  MtrrLibInitializeMtrrMask (&MtrrValidBitsMask, &MtrrValidAddressMask);

  Ranges[0].BaseAddress = 0;
  Ranges[0].Length      = MtrrValidBitsMask + 1;
  Ranges[0].Type        = (MTRR_MEMORY_CACHE_TYPE)(MtrrSetting.MtrrDefType & 0x07); //[Bits 2:0] Default Memory Type.
  RangeCount = 1;

  MtrrLibGetRawVariableRanges (
    &MtrrSetting.Variables, VariableMtrrCount,
    MtrrValidBitsMask, MtrrValidAddressMask, RawVariableRanges
    );

  MtrrLibApplyVariableMtrrs (
    RawVariableRanges, VariableMtrrCount,
    Ranges, ARRAY_SIZE (Ranges), &RangeCount
    );

  //
  // Set SMRAM as CacheWriteBack for performance.
  //
  Ranges[RangeCount].BaseAddress = SmramBase;
  Ranges[RangeCount].Length      = SmramSize;
  Ranges[RangeCount].Type        = CacheWriteBack;
  RangeCount++;

  ZeroMem (&UCMtrrSetting, sizeof (MTRR_SETTINGS));
  UCMtrrSetting.MtrrDefType = MtrrSetting.MtrrDefType;
  Status = EOPSetMemoryAttributesInMtrrSettings (&UCMtrrSetting, Ranges, RangeCount);

  CopyMem (&MtrrSetting.Variables, &UCMtrrSetting.Variables, sizeof (MTRR_VARIABLE_SETTINGS));
  MtrrSetting.MtrrDefType = UCMtrrSetting.MtrrDefType;
  return Status;
}

/**
  Checks if Premium PMIC present (VendorID == 1Fh)

  @retval  TRUE  if present
  @retval  FALSE it discrete/other PMIC
**/
BOOLEAN
IsPremiumPmicPresent (
  VOID
  )
{
  UINT8                           PmicVendorID;

  PmicVendorID = 0;
  //Send KSC Command to detect vendor ID of PMIC is present on the system(Applicable only for ULT/ULX Platforms)
  DetectPmicVendorID (&PmicVendorID);
  DEBUG((DEBUG_INFO, "Vendor ID of the Pmic Present on the system is: %x\n", PmicVendorID));

  if (PmicVendorID == 0x1F) {
    return TRUE;
  }

  return FALSE;
}

/**
Pmic Programming to Enable Voltage Margining
**/
VOID
PremiumPmicEnableSlpS0Voltage (
  VOID
  )
{
  EFI_STATUS                      Status;
  UINT8                           EcDataV085ACNT;
  UINT64                          HdaPciBase;
  BOOLEAN                         PremiumPmicPresent;

  HdaPciBase = HdaPciCfgBase ();
  PremiumPmicPresent = IsPremiumPmicPresent();
  if ((PciSegmentRead16(HdaPciBase + PCI_VENDOR_ID_OFFSET) == 0xFFFF) && (PremiumPmicPresent == TRUE)) { // If HDA Device is not detected & Premium PMIC is present
    DEBUG((DEBUG_INFO, "Enable VM in case Premium PMIC is Detected and HDA Disabled\n"));
    EcDataV085ACNT = 0x7A; //Enable Voltage Margining in case HDA is disabled
    Status = SetSlpS0Voltage(EcDataV085ACNT);
    ASSERT_EFI_ERROR(Status);
  }
}

/**
  Configure PciHostBridge related PCDs
**/
VOID
ConfigurePciHostBridgePcds (
  VOID
  )
{
  EFI_STATUS                       Status;
  EFI_PEI_READ_ONLY_VARIABLE2_PPI  *VariableServices;
  EFI_PHYSICAL_ADDRESS             PciBaseAddress;
  UINT32                           Tolud;
  UINT64                           Length;
  UINT64                           McD0BaseAddress;
  UINTN                            ResMemLimit1;
  UINTN                            SaSetupSize;
  SA_SETUP                         SaSetup;
#if FixedPcdGetBool(PcdITbtEnable) == 1
  TCSS_DATA_HOB                    *TcssHob;

  TcssHob                   = NULL;
#endif
  Status = PeiServicesLocatePpi (
             &gEfiPeiReadOnlyVariable2PpiGuid,  // GUID
             0,                                 // INSTANCE
             NULL,                              // EFI_PEI_PPI_DESCRIPTOR
             (VOID **) &VariableServices        // PPI
             );
  if (EFI_ERROR (Status)) {
    DEBUG ((DEBUG_ERROR, "ConfigurePciHostBridgePcds: PeiServicesLocatePpi failed\n"));
    return;
  }
#if FixedPcdGetBool(PcdITbtEnable) == 1
  //
  // Get Tcss Data HOB
  //
  TcssHob = (TCSS_DATA_HOB *) GetFirstGuidHob (&gTcssHobGuid);
  if (TcssHob != NULL) {
    if (TcssHob->TcssData.PcieMultipleSegmentEnabled) {
      PcdSet8S (PcdPciSegmentCount, 2);
    }
  }
#endif
  //
  // Allocate 56 KB of I/O space [0x2000..0xFFFF]
  //
  DEBUG ((DEBUG_INFO, " Assign IO resource for PCI_ROOT_BRIDGE from 0x%X to 0x%X\n", PcdGet16 (PcdPciReservedIobase) ,PcdGet16 (PcdPciReservedIoLimit)));

  //
  // Read memory map registers
  //
  McD0BaseAddress        = PCI_SEGMENT_LIB_ADDRESS (SA_SEG_NUM, SA_MC_BUS, 0, 0, 0);
  Tolud                  = PciSegmentRead32 (McD0BaseAddress + R_SA_TOLUD) & B_SA_TOLUD_TOLUD_MASK;
  PciBaseAddress         = Tolud;

  ResMemLimit1 = PcdGet32 (PcdPciReservedMemLimit);
  if (ResMemLimit1 == 0) {
    ResMemLimit1 = (UINTN) PcdGet64 (PcdPciExpressBaseAddress);
  }

  Length = ResMemLimit1 - PciBaseAddress;

  if (Length != 0) {
    PcdSet32S (PcdPciReservedMemBase, (UINT32) PciBaseAddress);
    PcdSet32S (PcdPciReservedMemLimit, (UINT32) (PciBaseAddress + Length - 1));
    DEBUG ((DEBUG_INFO, " Assign Memory Resource for PCI_ROOT_BRIDGE from 0x%X", PcdGet32 (PcdPciReservedMemBase)));
    DEBUG ((DEBUG_INFO, " to 0x%X\n", PcdGet32 (PcdPciReservedMemLimit)));
  }

  //
  // Check Enable Above 4GB MMIO or not
  //
  SaSetupSize = sizeof (SA_SETUP);
  Status = VariableServices->GetVariable (
                                VariableServices,
                                L"SaSetup",
                                &gSaSetupVariableGuid,
                                NULL,
                                &SaSetupSize,
                                &SaSetup
                                );
  if (!EFI_ERROR(Status)) {
    if (SaSetup.EnableAbove4GBMmio == 1 || SaSetup.ApertureSize == 15) {
      //
      // Provide 256GB available above 4GB MMIO resource
      // limited to use single variable MTRR to cover this above 4GB MMIO region.
      //
      PcdSet64S (PcdPciReservedMemAbove4GBBase, BASE_256GB);
      PcdSet64S (PcdPciReservedMemAbove4GBLimit, BASE_256GB + SIZE_256GB - 1);
      if (PcdGet64 (PcdPciReservedMemAbove4GBBase) < PcdGet64 (PcdPciReservedMemAbove4GBLimit)) {
        DEBUG ((DEBUG_INFO, " PCI space that above 4GB MMIO is from 0x%lX", PcdGet64 (PcdPciReservedMemAbove4GBBase)));
        DEBUG ((DEBUG_INFO, " to 0x%lX\n", PcdGet64 (PcdPciReservedMemAbove4GBLimit)));
      }
    }
  }
}

/**
  Returns whether the platform is in a debug state, by checking if any of the
  following conditions is true:

    - Using a sample part
    - Feature Control MSR lock is not set
    - Debug interface is enabled
    - SOC Configuration lock is not set (manufacturing mode)

  @retval  TRUE    Platform is in a debug state.
  @retval  FALSE   Platform is NOT in a debug state.
**/
BOOLEAN
PlatformDebugStateChecks (
  VOID
  )
{
  ADL_MSR_PLATFORM_INFO_REGISTER    PlatformInfoMsr;
  MSR_IA32_FEATURE_CONTROL_REGISTER Ia32FeatureControlMsr;
  MSR_IA32_DEBUG_INTERFACE_REGISTER DebugInterfaceMsr;
  HECI_FW_STS6_REGISTER             MeFirmwareStatus;

  BOOLEAN DebugState = FALSE;

  DEBUG ((DEBUG_INFO, "PlatformDebugStateChecks\n"));

  //
  // Check for sample part
  //
  PlatformInfoMsr.Uint64 = AsmReadMsr64 (ADL_MSR_PLATFORM_INFO);
  DEBUG ((DEBUG_INFO, "  PlatformInfoMsr = 0x%x\n", PlatformInfoMsr.Uint64));
  if (PlatformInfoMsr.Bits.SamplePart) {
    DEBUG ((DEBUG_WARN, "  Sample part detected!\n"));
    DebugState = TRUE;
  }

  //
  // Check for MSR Feature Control lock
  //
  Ia32FeatureControlMsr.Uint64 = AsmReadMsr64 (MSR_IA32_FEATURE_CONTROL);
  DEBUG ((DEBUG_INFO, "  Ia32FeatureControlMsr.Uint64 = 0x%x\n", Ia32FeatureControlMsr.Uint64));
  if (Ia32FeatureControlMsr.Bits.Lock == 0) {
    DEBUG ((DEBUG_WARN, "  MSR Feature Control MSR lock is not set!\n"));
    DebugState = TRUE;
  }

  //
  // Check for Debug interface
  //
  DebugInterfaceMsr.Uint64 = AsmReadMsr64 (MSR_IA32_DEBUG_INTERFACE);
  DEBUG ((DEBUG_INFO, "  DebugInterfaceMsr.Uint64 = 0x%x\n", DebugInterfaceMsr.Uint64));
  if (DebugInterfaceMsr.Bits.Enable) {
    DEBUG ((DEBUG_WARN, "  Debug interface is enabled!\n"));
    DebugState = TRUE;
  }

  //
  // Check for Manufacturing Mode
  //
  MeFirmwareStatus.ul = PciSegmentRead32 (PCI_SEGMENT_LIB_ADDRESS (ME_SEGMENT, ME_BUS, ME_DEVICE_NUMBER, HECI_FUNCTION_NUMBER, R_ME_HFS_6));
  DEBUG ((DEBUG_INFO, "  MeFirmwareStatus.ul = 0x%x\n", MeFirmwareStatus.ul));
  if (MeFirmwareStatus.r.FpfSocConfigLock == 0) {
    DEBUG ((DEBUG_WARN, "  SOC Configuration Lock is not set!\n"));
    DebugState = TRUE;
  }

  return DebugState;
}

/**
   This callback function checks the debug state of the platform and
   sets the PcdFirmwareDebuggerInitialized value accordingly.

  @param[in]  PeiServices  Pointer to PEI Services Table.
  @param[in]  NotifyDesc   Pointer to the descriptor for the Notification event that
                           caused this function to execute.
  @param[in]  Ppi          Pointer to the PPI data associated with this function.

  @retval     EFI_SUCCESS  The function completes successfully
  @retval     others
**/
EFI_STATUS
EFIAPI
PlatformDebugStateChecksCallback (
  IN CONST EFI_PEI_SERVICES     **PeiServices,
  IN EFI_PEI_NOTIFY_DESCRIPTOR  *NotifyDescriptor,
  IN VOID                       *Ppi
  )
{
  //
  // Check Platform debug state
  //
  if (PlatformDebugStateChecks () == TRUE) {
    //
    // Extend platform state into TPM PCR[7]
    //
    PcdSetBoolS (PcdFirmwareDebuggerInitialized, TRUE);
  }
  DEBUG ((DEBUG_INFO, "PcdFirmwareDebuggerInitialized = %x\n", PcdGetBool (PcdFirmwareDebuggerInitialized)));
  return EFI_SUCCESS;
}

/**
  This function handles PlatformInit task at the end of PEI

  @param[in]  PeiServices  Pointer to PEI Services Table.
  @param[in]  NotifyDesc   Pointer to the descriptor for the Notification event that
                           caused this function to execute.
  @param[in]  Ppi          Pointer to the PPI data associated with this function.

  @retval     EFI_SUCCESS  The function completes successfully
  @retval     others
**/
EFI_STATUS
EFIAPI
PlatformInitAdvancedEndOfPei (
  IN CONST EFI_PEI_SERVICES     **PeiServices,
  IN EFI_PEI_NOTIFY_DESCRIPTOR  *NotifyDescriptor,
  IN VOID                       *Ppi
  )
{
  EFI_STATUS     Status;

//[-start-180709-IB15410168-modify]//
#if FixedPcdGetBool(PcdFspWrapperEnable) == 0
  Status = SetCacheMtrrAfterEndOfPei ();
  ASSERT_EFI_ERROR (Status);
#else
  Status = MtrrSetMemoryAttribute (0xC0000, 0x40000, CacheWriteProtected);
  ASSERT_EFI_ERROR (Status);
#endif
//[-end-180709-IB15410168-modify]//

  //
  // Function to enable VM in case HD Audio is Disabled
  //
  PremiumPmicEnableSlpS0Voltage ();

#if FixedPcdGetBool (PcdDTbtEnable) == 1
  //
  // Perform DTBT init
  //
  Status = DTbtInitEndOfPei ();
#endif

  //
  // Configure PciHostBridge related PCDs before DXE phase
  //
  ConfigurePciHostBridgePcds ();

  return Status;
}

////@todo Review this functionality and if it is required for SKL SDS
///**
//  Create the HOB for hotkey status for 'Attempt USB First' feature
//
//  @retval  EFI_SUCCESS  HOB Creating successful.
//  @retval  Others       HOB Creating failed.
//**/
//EFI_STATUS
//CreateAttemptUsbFirstHotkeyInfoHob (
//  VOID
//  )
//{
//  EFI_STATUS                     Status;
//  ATTEMPT_USB_FIRST_HOTKEY_INFO  AttemptUsbFirstHotkeyInfo;
//
//  Status = EFI_SUCCESS;
//
//  ZeroMem (
//    &AttemptUsbFirstHotkeyInfo,
//    sizeof (AttemptUsbFirstHotkeyInfo)
//    );
//
//  AttemptUsbFirstHotkeyInfo.RevisonId = 0;
//  AttemptUsbFirstHotkeyInfo.HotkeyTriggered = FALSE;
//
//  ///
//  /// Build HOB for Attempt USB First feature
//  ///
//  BuildGuidDataHob (
//    &gAttemptUsbFirstHotkeyInfoHobGuid,
//    &(AttemptUsbFirstHotkeyInfo),
//    sizeof (ATTEMPT_USB_FIRST_HOTKEY_INFO)
//    );
//
//  return Status;
//}

/**
  Get address of PMC crash log record from descriptor table

  @param[in]  DiscoveryBuffer    PMC IPC discover buffer

  @retval     UINT32             An address of PMC crash log record.
**/
UINT32
GetPmcCrashLogDescriptTblAddr (
  IN PMC_IPC_DISCOVERY_BUF       *DiscoveryBuffer
  )
{
  return PcdGet32 (PcdSiliconInitTempMemBaseAddr) + DiscoveryBuffer->Bits64.DesTableOffset;
}

/**
  Get how many PMC crash log records.

  @param[in]  DiscoveryBuffer    PMC IPC discover buffer

  @retval     UINT32             Amounts of PMC crash log.
**/
UINT32
GetPmcCrashLogAmount (
  IN PMC_IPC_DISCOVERY_BUF       *DiscoveryBuffer
  )
{
  UINT32                         NumberOfRegionsAddress;

  switch (DiscoveryBuffer->Bits64.Mech) {
    case CRASHLOG_MECH_LEGACY:
      return 1;
    case CRASHLOG_MECH_DESCRIPTOR_TABLE:
      NumberOfRegionsAddress = GetPmcCrashLogDescriptTblAddr (DiscoveryBuffer);
      return MmioRead32 (NumberOfRegionsAddress);
    default:
      return 0;
  }
}

/**
  Get offset and log size from PMC crash records.

  @param[in]  DiscoveryBuffer    PMC IPC discover buffer
  @param[in]  Offset             PMC records of region offset

  @retval     UINT32             PMC crash record
**/
UINT32
GetPmcCrashLogRecord (
  IN PMC_IPC_DISCOVERY_BUF       *DiscoveryBuffer,
  IN UINT32                      Offset
  )
{
  PMC_CRASHLOG_RECORDS     Record;
  UINT32                   PmcCrashRecordAddr;

  switch (DiscoveryBuffer->Bits64.Mech) {
    case CRASHLOG_MECH_LEGACY:
      //
      // Legacy Mechanism uses Baseoffset as PmcCrashLogAddr.
      // The BaseOffset is returned from PMC discovery buffer.
      //
      Record.Info.Offset = DiscoveryBuffer->Bits.BaseOffset;
      Record.Info.Size = (DiscoveryBuffer->Bits.Size != 0) ? DiscoveryBuffer->Bits.Size : 0x300;
      break;
    case CRASHLOG_MECH_DESCRIPTOR_TABLE:
      //   Descritpor Table Mechanism
      //---------------------------------
      // Number of records  |   4 bytes
      //  1 record offset   |   2 bytes
      //  1 record size     |   2 bytes
      //  2 record offset   |   2 bytes
      //  2 record size     |   2 bytes
      //---------------------------------
      PmcCrashRecordAddr = GetPmcCrashLogDescriptTblAddr (DiscoveryBuffer) + (4 * Offset);
      Record.Uint32 = MmioRead32 (PmcCrashRecordAddr);
      break;
    default:
      return 0;
  }

  //
  // PMC crash log size is number of DWORDs.
  //
  Record.Info.Size *= sizeof (UINT32);
  return Record.Uint32;
}

/**
  Consider the PMC crash log data is valid or invalid.

  @param[in]  PmcCrashLogAddr    Crash log data of memory address

  @retval     TRUE               Data is valid
  @retval     FALSE              Data is invalid
**/
BOOLEAN
GetPmcValidRecord (
  IN UINT32                      PmcCrashLogAddr
  )
{
  CRASHLOG_VERSION *PmcCrashLogVersion;

  PmcCrashLogVersion = (CRASHLOG_VERSION *) PmcCrashLogAddr;

  if (PmcCrashLogVersion->Uint32 == 0) {
    return FALSE;
  }

  if (PmcCrashLogVersion->Bits.Consumed == 1) {
    return FALSE;
  }

  return TRUE;
}

/**
  Get PMC crash log supported or not.

  @param[in]  DiscoveryBuffer    PMC IPC discover buffer

  @retval     TRUE               PMC crash log support
  @retval     FALSE              PMC crash log not support
**/
BOOLEAN
GetPmcCrashLogSupport (
  IN PMC_IPC_DISCOVERY_BUF       *DiscoveryBuffer
  )
{

  if (DiscoveryBuffer->Bits.Avail != 1) {
    return FALSE;
  }

  switch (DiscoveryBuffer->Bits64.Mech) {
    case CRASHLOG_MECH_LEGACY:
      if (DiscoveryBuffer->Bits.Dis == 1) {
        return FALSE;
      }
      break;
    case CRASHLOG_MECH_DESCRIPTOR_TABLE:
      if (DiscoveryBuffer->Bits64.CrashDisSts) {
        return FALSE;
      }
      break;
    default:
      return FALSE;
  }

  return TRUE;
}

/**
  Get re-arm supported or not.

  @param[in]  CrashlogHeader        CPU Crashlog header structure

  @retval     TRUE                  PMC Re-arm command support
  @retval     FALSE                 PMC Re-arm command not support
**/
BOOLEAN
GetCpuReArmSupport (
  IN CPU_CRASHLOG_HEADER          *CrashlogHeader
  )
{
  EFI_STATUS                        Status;
  EFI_PEI_READ_ONLY_VARIABLE2_PPI   *VariableServices;
  SETUP_DATA                        SetupData;
  UINTN                             VariableSize;

  if (CrashlogHeader == NULL) {
    return FALSE;
  }

  if (CrashlogHeader->CrashlogCapability.Fields.ReArmSupported == 0) {
    return FALSE;
  }

  Status = PeiServicesLocatePpi (
             &gEfiPeiReadOnlyVariable2PpiGuid,  // GUID
             0,                                 // INSTANCE
             NULL,                              // EFI_PEI_PPI_DESCRIPTOR
             (VOID **) &VariableServices        // PPI
             );
  ASSERT_EFI_ERROR (Status);
  if (EFI_ERROR (Status)) {
    DEBUG ((DEBUG_ERROR, "%a: Failed to locate gEfiPeiReadOnlyVariable2Ppi.\n", __FUNCTION__));
    return FALSE;
  }

  VariableSize = sizeof (SETUP_DATA);
  Status = VariableServices->GetVariable (
                               VariableServices,
                               PLATFORM_SETUP_VARIABLE_NAME,
                               &gSetupVariableGuid,
                               NULL,
                               &VariableSize,
                               &SetupData
                               );
  if (EFI_ERROR (Status)) {
    DEBUG ((DEBUG_ERROR, "%a: Failed to GetVariable (%r).\n", __FUNCTION__, Status));
    return FALSE;
  }

  return (BOOLEAN)(SetupData.CrashLogRearmEnable);
}

/**
  Get re-arm supported or not.

  @param[in]  DiscoveryBuffer       PMC IPC discover buffer

  @retval     TRUE                  PMC Re-arm command support
  @retval     FALSE                 PMC Re-arm command not support
**/
BOOLEAN
GetPmcReArmSupport (
  IN PMC_IPC_DISCOVERY_BUF          *DiscoveryBuffer
  )
{
  EFI_STATUS                        Status;
  EFI_PEI_READ_ONLY_VARIABLE2_PPI   *VariableServices;
  SETUP_DATA                        SetupData;
  UINTN                             VariableSize;

  if (DiscoveryBuffer == NULL) {
    return FALSE;
  }

  if (DiscoveryBuffer->Bits64.Mech == CRASHLOG_MECH_DESCRIPTOR_TABLE) {
    if (DiscoveryBuffer->Bits64.ReArm == 0) {
      return FALSE;
    }
  } else {
    return FALSE;
  }

  Status = PeiServicesLocatePpi (
             &gEfiPeiReadOnlyVariable2PpiGuid,  // GUID
             0,                                 // INSTANCE
             NULL,                              // EFI_PEI_PPI_DESCRIPTOR
             (VOID **) &VariableServices        // PPI
             );
  ASSERT_EFI_ERROR (Status);
  if (EFI_ERROR (Status)) {
    DEBUG ((DEBUG_ERROR, "%a: Failed to locate gEfiPeiReadOnlyVariable2Ppi.\n", __FUNCTION__));
    return FALSE;
  }

  VariableSize = sizeof (SETUP_DATA);
  Status = VariableServices->GetVariable (
                               VariableServices,
                               PLATFORM_SETUP_VARIABLE_NAME,
                               &gSetupVariableGuid,
                               NULL,
                               &VariableSize,
                               &SetupData
                               );
  if (EFI_ERROR (Status)) {
    DEBUG ((DEBUG_ERROR, "%a: Failed to GetVariable (%r).\n", __FUNCTION__, Status));
    return FALSE;
  }

  return (BOOLEAN)(SetupData.CrashLogRearmEnable);
}

/*
  Get pmc crash log clear enable or not.

  @param[in]  DiscoveryBuffer       PMC IPC discover buffer

  @retval     TRUE                  PMC crash log clear
  @retval     FALSE                 PMC crash log do not clear
*/
BOOLEAN
GetPmcClearSupport (
  IN PMC_IPC_DISCOVERY_BUF          *DiscoveryBuffer
  )
{
  EFI_STATUS                        Status;
  EFI_PEI_READ_ONLY_VARIABLE2_PPI   *VariableServices;
  SETUP_DATA                        SetupData;
  UINTN                             VariableSize;

  if (DiscoveryBuffer->Bits64.Mech == CRASHLOG_MECH_DESCRIPTOR_TABLE) {
    if (DiscoveryBuffer->Bits64.Clr == 0) {
      return FALSE;
    }
  }

  Status = PeiServicesLocatePpi (
             &gEfiPeiReadOnlyVariable2PpiGuid,  // GUID
             0,                                 // INSTANCE
             NULL,                              // EFI_PEI_PPI_DESCRIPTOR
             (VOID **) &VariableServices        // PPI
             );
  ASSERT_EFI_ERROR (Status);

  VariableSize = sizeof (SETUP_DATA);
  Status = VariableServices->GetVariable (
                               VariableServices,
                               PLATFORM_SETUP_VARIABLE_NAME,
                               &gSetupVariableGuid,
                               NULL,
                               &VariableSize,
                               &SetupData
                               );

  return (BOOLEAN)(SetupData.CrashLogClearEnable);
}

/**
  Collect PCH CrashLog data from Pmc SSRAM and store in HOB .

**/
VOID
CrashLogCollectDataFromPmcSSRAM (
  VOID
  )
{
  EFI_STATUS                  Status;
  PMC_IPC_DISCOVERY_BUF       DiscoveryBuffer;
  UINT64                      PmcSsramBaseAddress;
  UINT32                      PmcSsramBar0;
  PMC_CRASHLOG_LINK           *PmcCrashLog;
  PMC_CRASHLOG_LINK           *PmcCrashLogList;
  PMC_CRASHLOG_LINK           *TempPmcCrashLog;
  CRASHLOG_HOB                *PmcCrashLogHob;
  UINT32                      Index;
  UINT32                      NumberOfRegions = 0;
  UINT32                      ValidRecords;
  UINT32                      PmcCrashLogAddr;
  UINT32                      *Destination = NULL;
  UINT32                      CopiedSize = 0;

  DEBUG ((DEBUG_INFO, "CrashLogCollectDataFromPmcSSRAM - start\n"));

  //
  // Check for the availability of CrashLog feature
  //
  ZeroMem (&DiscoveryBuffer, sizeof (PMC_IPC_DISCOVERY_BUF));

  Status = PmcCrashLogDiscovery (&DiscoveryBuffer);

  if (EFI_ERROR (Status) || (GetPmcCrashLogSupport (&DiscoveryBuffer) == FALSE)) {
    DEBUG ((DEBUG_INFO, "PCH CrashLog feature is not supported\n"));
    return;
  }

  //
  // Start to access PMC SSRAM MMIO
  //
  PmcSsramBaseAddress = PmcSsramPciCfgBase ();

  if (PciSegmentRead16 (PmcSsramBaseAddress + PCI_VENDOR_ID_OFFSET) == 0xFFFF) {
    DEBUG ((DEBUG_ERROR, "PMC SSRAM PCI device is disabled\n"));
    return;
  }

  //
  // Program BAR 0 and enable command register memory space decoding
  //
  PmcSsramBar0 = PcdGet32 (PcdSiliconInitTempMemBaseAddr);
  PciSegmentWrite32 (PmcSsramBaseAddress + PCI_BASE_ADDRESSREG_OFFSET, PmcSsramBar0);
  PciSegmentOr16 (PmcSsramBaseAddress + PCI_COMMAND_OFFSET, (UINT16) (EFI_PCI_COMMAND_MEMORY_SPACE));

  NumberOfRegions = GetPmcCrashLogAmount (&DiscoveryBuffer);
  PmcCrashLogList = NULL;

  for (Index = 1, ValidRecords = 0; Index <= NumberOfRegions; Index++) {
    PmcCrashLog = AllocateZeroPool (sizeof (PMC_CRASHLOG_LINK));
    if (PmcCrashLog == NULL) {
      DEBUG((DEBUG_ERROR, "CrashLogCollectDataFromPmcSSRAM : buffer allocation failure\n"));
      Status = RETURN_OUT_OF_RESOURCES;
      ASSERT_EFI_ERROR(Status);
      return;
    }
    PmcCrashLog->Record.Uint32 = GetPmcCrashLogRecord (&DiscoveryBuffer, Index);
    DEBUG ((DEBUG_INFO, "%d.Crashlog record  = 0x%08x\n", Index, PmcCrashLog->Record.Uint32));

    //
    // Check all records' data are valid or invalid, if not, skip the record.
    //
    PmcCrashLogAddr = PmcSsramBar0 + PmcCrashLog->Record.Info.Offset;
    DEBUG ((DEBUG_INFO, "%d.MmioRead32 (0x%08x) = 0x%08x\n", Index, PmcCrashLogAddr, MmioRead32 (PmcCrashLogAddr)));
    if (GetPmcValidRecord (PmcCrashLogAddr) == FALSE) {
      //
      // Invalid record. Skip this record and check the next.
      //
      FreePool (PmcCrashLog);
      continue;
    }

    //
    // Valid record, prepare memory space for this address of record.
    //
    Status = PeiServicesAllocatePages (
             EfiBootServicesData,
             EFI_SIZE_TO_PAGES (PmcCrashLog->Record.Info.Size),
             &(PmcCrashLog->AllocateAddress)
             );

    if (EFI_ERROR (Status)) {
      DEBUG ((DEBUG_ERROR, "Memory out of resource\n"));
      goto Exit;
    }

    ValidRecords++;
    PmcCrashLog->Next = NULL;
    TempPmcCrashLog = PmcCrashLogList;

    //
    // Link every valid records to PmcCrashLogList
    //
    if (PmcCrashLogList == NULL) {
      PmcCrashLogList = PmcCrashLog;
    } else {
      while (TempPmcCrashLog->Next != NULL) {
        TempPmcCrashLog = TempPmcCrashLog->Next;
      }
      TempPmcCrashLog->Next = PmcCrashLog;
    }
  }

  //
  // If none valid records, exit.
  //
  if (ValidRecords == 0) {
    goto Exit;
  }

  //
  // Build of location for Crash log in Hob
  //
  PmcCrashLogHob = (CRASHLOG_HOB *) BuildGuidHob (
                                      &gPmcCrashLogDataBufferHobGuid,
                                      sizeof (CRASHLOG_HOB) * ValidRecords
                                      );
  DEBUG ((DEBUG_INFO, "CrashLogDataBuffer = 0x%08x\n", PmcCrashLogHob));
  if (PmcCrashLogHob == NULL) {
    //
    // No enough hob resource.
    //
    return;
  }

  for (Index = 0, TempPmcCrashLog = PmcCrashLogList; TempPmcCrashLog != NULL; Index++) {
    //
    // Fill allocate memory address and record size to Hob.
    //
    PmcCrashLogHob[Index].AllocateAddress = TempPmcCrashLog->AllocateAddress;
    PmcCrashLogHob[Index].Size = TempPmcCrashLog->Record.Info.Size;
    DEBUG ((DEBUG_INFO, "%d.CrashLogHob->AllocateAddress = 0x%08x\n", Index, PmcCrashLogHob[Index].AllocateAddress));
    DEBUG ((DEBUG_INFO, "%d.CrashLogHob->Size = 0x%x\n", Index, PmcCrashLogHob[Index].Size));

    //
    // Initial pointers for copy crash log data.
    //
    Destination = (UINT32 *)(UINTN) TempPmcCrashLog->AllocateAddress;
    PmcCrashLogAddr = PmcSsramBar0 + TempPmcCrashLog->Record.Info.Offset;
    CopiedSize = 0;

    //
    // Copy CrashLog data from SSRAM to allocated memory buffer
    //
    while (CopiedSize < TempPmcCrashLog->Record.Info.Size) {
      *Destination = MmioRead32 (PmcCrashLogAddr); // Byte access is not allowed to PMC SSRAM, hence copying DW by DW
      Destination++;
      PmcCrashLogAddr += 4;
      CopiedSize += 4;
    }

    //
    // Point to next PmcCrashLog
    //
    TempPmcCrashLog = TempPmcCrashLog->Next;
  }

  if (GetPmcReArmSupport (&DiscoveryBuffer) == TRUE) {
    //
    // Trigger re-arm command.
    //
    Status = PmcCrashLogReArm ();
    DEBUG ((DEBUG_INFO, "Re-arm Status = %r\n", Status));
  }

  if (GetPmcClearSupport (&DiscoveryBuffer) == TRUE) {
    //
    // Clear the SSRAM region after copying the error log
    //
    Status = PmcCrashLogClear ();
    DEBUG ((DEBUG_INFO, "Clear CrashLog Status = %r\n", Status));
  }

Exit:
  //
  // Disable PMC SSRAM MMIO
  //
  PciSegmentAnd16 (PmcSsramBaseAddress + PCI_COMMAND_OFFSET, (UINT16) ~(EFI_PCI_COMMAND_MEMORY_SPACE | EFI_PCI_COMMAND_IO_SPACE));
  PciSegmentWrite32 (PmcSsramBaseAddress + PCI_BASE_ADDRESSREG_OFFSET, 0);
  DEBUG ((DEBUG_INFO, "CrashLogCollectDataFromPmcSSRAM - end\n"));

  return;
}

/**
  Collect CPU CrashLog data from Telemetry SRAM and store in HOB .

**/
VOID
CpuCrashLogCollectDataFromTelemetrySRAM (
  VOID
  )
{
  EFI_STATUS                  Status;
  TEL_CRASHLOG_DEVSC_CAP      CrashLogDevscCap;
  UINT64                      TelemetryPciDeviceBaseAddr;
  UINT32                      TempBarAddr;
  EFI_PHYSICAL_ADDRESS        MainLogAllocateAddress;
  EFI_PHYSICAL_ADDRESS        TelemetryAllocateAddress;
  EFI_PHYSICAL_ADDRESS        TraceAllocateAddress;
  UINT32                      MainLogSize = 0;
  UINT32                      TelemetrySize = 0;
  UINT32                      TraceSize = 0;
  UINT32                      CpuCrashLogAddr;
  CPU_CRASHLOG_STRUCT         CpuCrashLogStruct;
  CPU_CRASHLOG_HOB            CpuCrashLogHob;
  UINT32                      *Destination = NULL;
  UINT32                      CopiedSize = 0;
  UINT32                      BaseAddress;
  CPU_CRASHLOG_HEADER         CrashlogHeader;
  UINT32                      Data32;

  DEBUG ((DEBUG_INFO, "CpuCrashLogCollectDataFromTelemetrySRAM - start\n"));

  //
  // Check for the availability of CPU CrashLog feature
  //
  ZeroMem (&CrashLogDevscCap, sizeof (TEL_CRASHLOG_DEVSC_CAP));

  Status = GetCpuCrashLogCapability (&CrashLogDevscCap);

  if (EFI_ERROR (Status)) {
    DEBUG ((DEBUG_INFO, "CPU CrashLog feature is not supported\n"));
    return;
  }

  //
  // Start to access Telemetry MMIO
  //
  TelemetryPciDeviceBaseAddr = PCI_SEGMENT_LIB_ADDRESS (SA_SEG_NUM, TEL_BUS_NUM, TEL_DEV_NUM, TEL_FUN_NUM, 0);
  DEBUG ((DEBUG_INFO, "TelemetryPciDeviceBaseAddr = 0x%X\n", TelemetryPciDeviceBaseAddr));

  //
  // Program BAR address and enable command register memory space decoding
  //
  TempBarAddr = PcdGet32 (PcdSiliconInitTempMemBaseAddr);
  DEBUG ((DEBUG_INFO, "TempBarAddr = 0x%X\n", TempBarAddr));
  if (CrashLogDevscCap.DiscoveryData.Fields.TBir == V_TEL_DVSEC_TBIR_BAR0) {
    PciSegmentWrite32 (TelemetryPciDeviceBaseAddr + R_TEL_CFG_BAR0, TempBarAddr);
  } else if (CrashLogDevscCap.DiscoveryData.Fields.TBir == V_TEL_DVSEC_TBIR_BAR1) {
    PciSegmentWrite32 (TelemetryPciDeviceBaseAddr + R_TEL_CFG_BAR1, TempBarAddr);
  }
  PciSegmentOr16 (TelemetryPciDeviceBaseAddr + PCI_COMMAND_OFFSET, (UINT16) (EFI_PCI_COMMAND_MEMORY_SPACE));

  ZeroMem (&CpuCrashLogStruct, sizeof (CPU_CRASHLOG_STRUCT));

  Status = CpuCrashLogDiscovery (&CpuCrashLogStruct);

  MainLogSize = CpuCrashLogStruct.MainBuffer.Fields.DataBuffSize * 4;
  TelemetrySize = CpuCrashLogStruct.TelemetryBuffer.Fields.DataBuffSize * 4;
  TraceSize = CpuCrashLogStruct.TraceBuffer.Fields.DataBuffSize * 4;

  DEBUG ((DEBUG_INFO, "Use MainLog Size as 0x%X \n", MainLogSize));
  DEBUG ((DEBUG_INFO, "Use Telemetry Size as 0x%X \n", TelemetrySize));
  DEBUG ((DEBUG_INFO, "Use Trace Size as 0x%X \n", TraceSize));

  if (MainLogSize == 0) {
    DEBUG ((DEBUG_INFO, "MainLog is not present \n"));
    goto Exit;
  }

  //
  // Check if the first DW of CPU crashlog is valid
  //
  CpuCrashLogAddr = (UINT32) TempBarAddr + CpuCrashLogStruct.MainBuffer.Fields.DataBuffAddress;
  Data32 = MmioRead32 (CpuCrashLogAddr);
  if (Data32 == 0) {
    DEBUG ((DEBUG_INFO, "Cpu MainLog is invalid \n"));
    goto Exit;
  }

  if (TelemetrySize == 0) {
    DEBUG ((DEBUG_INFO, "TelemetryLog is not present \n"));
  }

  if (TraceSize == 0) {
    DEBUG ((DEBUG_INFO, "TraceLog is not present \n"));
  }

  //
  // Allocate memory buffer for MainLog data
  //
  Status = PeiServicesAllocatePages (
             EfiBootServicesData,
             EFI_SIZE_TO_PAGES (MainLogSize),
             &MainLogAllocateAddress
             );
  DEBUG ((DEBUG_INFO, "MainLogAllocateAddress : 0x%X \n", MainLogAllocateAddress));

  if (EFI_ERROR (Status)) {
    DEBUG ((DEBUG_ERROR, "Memory out of resource\n"));
    goto Exit;
  }

  //
  // Allocate memory buffer for Telemetry data
  //
  Status = PeiServicesAllocatePages (
             EfiBootServicesData,
             EFI_SIZE_TO_PAGES (TelemetrySize),
             &TelemetryAllocateAddress
             );
  DEBUG ((DEBUG_INFO, "TelemetryAllocateAddress : 0x%X \n", TelemetryAllocateAddress));

  if (EFI_ERROR (Status)) {
    DEBUG ((DEBUG_ERROR, "Memory out of resource\n"));
    goto Exit;
  }

  //
  // Allocate memory buffer for Trace data
  //
  Status = PeiServicesAllocatePages (
             EfiBootServicesData,
             EFI_SIZE_TO_PAGES (TraceSize),
             &TraceAllocateAddress
             );
  DEBUG ((DEBUG_INFO, "TraceAllocateAddress : 0x%X \n", TraceAllocateAddress));

  if (EFI_ERROR (Status)) {
    DEBUG ((DEBUG_ERROR, "Memory out of resource\n"));
    goto Exit;
  }


  CpuCrashLogHob.Main.AllocateAddress = (UINTN) MainLogAllocateAddress;
  CpuCrashLogHob.Main.Size = MainLogSize;

  CpuCrashLogHob.Telemetry.AllocateAddress = (UINTN) TelemetryAllocateAddress;
  CpuCrashLogHob.Telemetry.Size = TelemetrySize;

  CpuCrashLogHob.Trace.AllocateAddress = (UINTN) TraceAllocateAddress;
  CpuCrashLogHob.Trace.Size = TraceSize;

  //
  // Build of location for Crash log in Hob
  //
  BuildGuidDataHob (
    &gCpuCrashLogDataBufferHobGuid,
    &CpuCrashLogHob,
    sizeof (CPU_CRASHLOG_HOB)
    );
  DEBUG ((DEBUG_INFO, "CpuCrashLogHob = 0x%X, sizeof (CPU_CRASHLOG_HOB) : 0x%X \n", CpuCrashLogHob, sizeof (CPU_CRASHLOG_HOB)));

  ZeroMem ((VOID *) (UINTN) MainLogAllocateAddress, EFI_SIZE_TO_PAGES (MainLogSize) * EFI_PAGE_SIZE);
  ZeroMem ((VOID *) (UINTN) TelemetryAllocateAddress, EFI_SIZE_TO_PAGES (TelemetrySize) * EFI_PAGE_SIZE);
  ZeroMem ((VOID *) (UINTN) TraceAllocateAddress, EFI_SIZE_TO_PAGES (TraceSize) * EFI_PAGE_SIZE);

  //
  // copy Main pointer crashlog data
  //
//  CopyMem ((UINT8 *)(UINTN) MainLogAllocateAddress, &(TelemetryBaseAddress + CpuCrashLogStruct.MainBuffer.Fields.DataBuffAddress), (CpuCrashLogStruct.MainBuffer.Fields.DataBuffSize) * 4);
  CpuCrashLogAddr = (UINT32) TempBarAddr + CpuCrashLogStruct.MainBuffer.Fields.DataBuffAddress;
  Destination =  (UINT32 *)(UINTN) MainLogAllocateAddress;
  while (CopiedSize < MainLogSize) {
    *Destination = MmioRead32 (CpuCrashLogAddr); // Byte access is not allowed to PMC SSRAM, hence copying DW by DW
    if (CopiedSize < CRASHLOG_SIZE_DEBUG_PURPOSE) { // Dumping only few bytes to help debug
      DEBUG ((DEBUG_INFO, "Main CrashData = 0x%x\n", *Destination));
    }
    Destination++;
    CpuCrashLogAddr += 4;
    CopiedSize   += 4;
  }
  CpuCrashLogAddr = (UINT32) TempBarAddr + CpuCrashLogStruct.MainBuffer.Fields.DataBuffAddress;
  DEBUG ((DEBUG_INFO, "TempBarAddr = 0x%x, CpuCrashLogStruct.MainBuffer.Fields.DataBuffAddress = 0x%x,  MainLogCrashLogAddr = 0x%x\n", TempBarAddr, CpuCrashLogStruct.MainBuffer.Fields.DataBuffAddress, CpuCrashLogAddr));

  //
  // copy Telemetry pointer crashlog data
  //
 // CopyMem ((UINT8 *)(UINTN) TelemetryAllocateAddress, &(TelemetryBaseAddress + CpuCrashLogStruct.TelemetryBuffer.Fields.DataBuffAddress), (CpuCrashLogStruct.TelemetryBuffer.Fields.DataBuffSize) * 4);
 CpuCrashLogAddr = (UINT32) TempBarAddr + CpuCrashLogStruct.TelemetryBuffer.Fields.DataBuffAddress;
 DEBUG ((DEBUG_INFO, "TempBarAddr = 0x%x, TelemetryBuffer.Fields.DataBuffAddress = 0x%x,  TelemetryCrashLogAddr = 0x%x\n", TempBarAddr, CpuCrashLogStruct.TelemetryBuffer.Fields.DataBuffAddress, CpuCrashLogAddr));
 Destination =  (UINT32 *)(UINTN) TelemetryAllocateAddress;
 CopiedSize = 0;
  while (CopiedSize < TelemetrySize) {
    *Destination = MmioRead32 (CpuCrashLogAddr); // Byte access is not allowed to PMC SSRAM, hence copying DW by DW
    if (CopiedSize < CRASHLOG_SIZE_DEBUG_PURPOSE) { // Dumping only few bytes to help debug
      DEBUG ((DEBUG_INFO, "Telemetry CrashData = 0x%x\n", *Destination));
    }
    Destination++;
    CpuCrashLogAddr += 4;
    CopiedSize   += 4;
  }
  //
  // copy Trace pointer crashlog data
  //
//  CopyMem ((UINT8 *)(UINTN) TraceAllocateAddress, &(TelemetryBaseAddress + CpuCrashLogStruct.TraceBuffer.Fields.DataBuffAddress), (CpuCrashLogStruct.TraceBuffer.Fields.DataBuffSize) * 4);
  CpuCrashLogAddr = (UINT32) TempBarAddr + CpuCrashLogStruct.TraceBuffer.Fields.DataBuffAddress;
  DEBUG ((DEBUG_INFO, "TempBarAddr = 0x%x, TraceBuffer.Fields.DataBuffAddress = 0x%x,  TraceCrashLogAddr = 0x%x\n", TempBarAddr, CpuCrashLogStruct.TraceBuffer.Fields.DataBuffAddress, CpuCrashLogAddr));
  Destination =  (UINT32 *)(UINTN) TraceAllocateAddress;
  CopiedSize = 0;
  while (CopiedSize < TraceSize) {
    *Destination = MmioRead32 (CpuCrashLogAddr); // Byte access is not allowed to PMC SSRAM, hence copying DW by DW
    if (CopiedSize < CRASHLOG_SIZE_DEBUG_PURPOSE) { // Dumping only few bytes to help debug
      DEBUG ((DEBUG_INFO, "Trace CrashData = 0x%x\n", *Destination));
    }
    Destination++;
    CpuCrashLogAddr += 4;
    CopiedSize   += 4;
  }

  BaseAddress = GetCpuCrashLogBarAddress (&CrashLogDevscCap);
  GetCrashLogHeader (BaseAddress, &CrashlogHeader);

  //
  // Clear the Telemetry SRAM region after copying the error log
  //
  CpuCrashLogClear ();

  //
  // Send re-arm command
  //
  if (GetCpuReArmSupport (&CrashlogHeader) == TRUE) {
    CpuCrashLogReArm ();
    DEBUG ((DEBUG_INFO, "Re-arm Status = %r\n", Status));
  }

Exit:
  //
  // Disable Telemetry SRAM MMIO
  //
  PciSegmentAnd16 (TelemetryPciDeviceBaseAddr + PCI_COMMAND_OFFSET, (UINT16) ~(EFI_PCI_COMMAND_MEMORY_SPACE | EFI_PCI_COMMAND_IO_SPACE));
  if (CrashLogDevscCap.DiscoveryData.Fields.TBir == V_TEL_DVSEC_TBIR_BAR0) {
    PciSegmentWrite32 (TelemetryPciDeviceBaseAddr + R_TEL_CFG_BAR0, 0);
  } else if (CrashLogDevscCap.DiscoveryData.Fields.TBir == V_TEL_DVSEC_TBIR_BAR1) {
    PciSegmentWrite32 (TelemetryPciDeviceBaseAddr + R_TEL_CFG_BAR1, 0);
  }
  DEBUG ((DEBUG_INFO, "CpuCrashLogCollectDataFromTelemetrySRAM - end\n"));

  return;
}

/**
  Configures GPIO

  @param[in]  GpioTable       Point to Platform Gpio table
  @param[in]  GpioTableCount  Number of Gpio table entries

**/
VOID
ConfigureGpio (
  IN GPIO_INIT_CONFIG                 *GpioDefinition,
  IN UINT16                           GpioTableCount
  )
{
  EFI_STATUS          Status;

  DEBUG ((DEBUG_INFO, "ConfigureGpio() Start\n"));

  Status = GpioConfigurePads (GpioTableCount, GpioDefinition);
  ASSERT_EFI_ERROR (Status);

  DEBUG ((DEBUG_INFO, "ConfigureGpio() End\n"));
}

/**
  Configure GPIO pads for Foxville I225 Lan based on the Setup Option
**/
VOID
FoxvilleLanGpioInit (
  VOID
  )
{
  EFI_STATUS                       Status;
  PCH_SETUP                        PchSetup;
  EFI_PEI_READ_ONLY_VARIABLE2_PPI  *VariableServices;
  UINTN                            VarSize;
  GPIO_PAD                         FoxLanEnablePad;
  GPIO_PAD                         FoxLanWakePad;
  GPIO_CONFIG                      FoxLanEnablePadConfig;
  GPIO_CONFIG                      FoxLanWakePadConfig;

  if ((PcdGet32 (PcdFoxLanEnableGpio) != 0) && (PcdGet32 (PcdFoxLanWakeGpio) != 0)) {
    DEBUG ((DEBUG_INFO, "FoxvilleLanGpioInit Start\n"));

    //
    // Locate Setup variables
    //
    Status = PeiServicesLocatePpi (
               &gEfiPeiReadOnlyVariable2PpiGuid,
               0,
               NULL,
               (VOID **) &VariableServices
               );

    if (EFI_ERROR (Status)) {
      DEBUG ((DEBUG_ERROR, "FoxvilleLanGpioInit: PeiServicesLocatePpi failed\n"));
      return;
    }

    VarSize = sizeof (PCH_SETUP);
    Status = VariableServices-> GetVariable (
                                  VariableServices,
                                  L"PchSetup",
                                  &gPchSetupVariableGuid,
                                  NULL,
                                  &VarSize,
                                  &PchSetup
                                  );
    if (EFI_ERROR (Status)) {
      DEBUG ((DEBUG_ERROR, "FoxvilleLanGpioInit: GetVariable (PchSetup) failed\n"));
      return;
    }

    if (PchSetup.FoxvilleLanSupport) {
      DEBUG ((DEBUG_INFO, "Setting Foxville Lan Support Gpio\n"));
      FoxLanEnablePad = PcdGet32 (PcdFoxLanEnableGpio);
      ZeroMem (&FoxLanEnablePadConfig, sizeof (FoxLanEnablePadConfig));
      FoxLanEnablePadConfig.Direction   = GpioDirOut;
      FoxLanEnablePadConfig.OutputState = GpioOutHigh;
      Status = GpioSetPadConfig (FoxLanEnablePad, &FoxLanEnablePadConfig); // Setting FOXVILLE_LAN_EN Output High

      if (PchSetup.FoxvilleWakeOnLan) {
        DEBUG ((DEBUG_INFO, "Setting Foxville Lan Wake Gpio\n"));
        FoxLanWakePad = PcdGet32 (PcdFoxLanWakeGpio);
        ZeroMem (&FoxLanWakePadConfig, sizeof (FoxLanWakePadConfig));
        FoxLanWakePadConfig.Direction       = GpioDirInInv;
        FoxLanWakePadConfig.InterruptConfig = GpioIntEdge|GpioIntSci;
        FoxLanWakePadConfig.LockConfig      = GpioPadConfigUnlock;
        Status = GpioSetPadConfig (FoxLanWakePad, &FoxLanWakePadConfig);   // Setting FOX_WAKE_N interrupt as SCI
      }

      if (EFI_ERROR (Status)) {
        DEBUG ((DEBUG_ERROR, "FoxvilleLanGpioInit: GpioSetPadConfig failed\n"));
        return;
      }
    }
    DEBUG ((DEBUG_INFO, "FoxvilleLanGpioInit End\n"));
  } else {
    DEBUG ((DEBUG_INFO, "FoxvilleLanGpioInit Skipped\n"));
  }
}

/**
  Configure GPIO pads for TouchPanels use
**/
VOID
TouchPanelGpioInit (
  VOID
  )
{
  EFI_STATUS                      Status;
  PCH_SETUP                       PchSetup;
  EFI_PEI_READ_ONLY_VARIABLE2_PPI *VariableServices;
  UINTN                           VarSize;
  UINT16                          BoardId;
  SETUP_DATA                      Setup;

  DEBUG ((DEBUG_INFO, "TouchPanelGpioInit Start\n"));

  //
  // Locate Setup variables
  //
  Status = PeiServicesLocatePpi (
             &gEfiPeiReadOnlyVariable2PpiGuid,
             0,
             NULL,
             (VOID **) &VariableServices
             );
  if (EFI_ERROR (Status)) {
    DEBUG ((DEBUG_ERROR, "TouchPanelGpioInit: PeiServicesLocatePpi failed\n"));
    return;
  }

  VarSize = sizeof (PCH_SETUP);
  Status = VariableServices->GetVariable (
                               VariableServices,
                               L"PchSetup",
                               &gPchSetupVariableGuid,
                               NULL,
                               &VarSize,
                               &PchSetup
                               );
  if (EFI_ERROR (Status)) {
    DEBUG ((DEBUG_ERROR, "TouchPanelGpioInit: GetVariable (PchSetup) failed\n"));
    return;
  }

  VarSize = sizeof (SETUP_DATA);
  Status = VariableServices->GetVariable (
                               VariableServices,
                               L"Setup",
                               &gSetupVariableGuid,
                               NULL,
                               &VarSize,
                               &Setup
                               );
  if (EFI_ERROR (Status)) {
    DEBUG ((DEBUG_ERROR, "TouchPanelGpioInit: GetVariable (Setup) failed\n"));
    return;
  }

  BoardId = PcdGet16(PcdBoardId);

  switch (BoardId) {
    case BoardIdAdlPLp4Bep:
    case BoardIdAdlPLp5Rvp:
    case BoardIdAdlPT3Lp5Rvp:
    case BoardIdAdlPLp4Rvp:
    case BoardIdAdlPDdr5MRRvp:
    case BoardIdAdlPDdr5Rvp:
    case BoardIdAdlPDdr4Rvp:
    case BoardIdAdlPSimics:
    case BoardIdAdlPDdr5Dg384Aep:
    case BoardIdAdlPLp5Aep:
    case BoardIdAdlMLp4Rvp:
    case BoardIdAdlMLp5Rvp:
    case BoardIdAdlMLp5Rvp2a:
    case BoardIdAdlMLp5PmicRvp:
    case BoardIdAdlMLp5Aep:
    case BoardIdAdlPLp5Dg128Aep:
    case BoardIdAdlPLp5Gcs:
      //
      // Verify if THC0 or THC1 panels are enabled before changing GPIO configuration
      //
      if (PchSetup.ThcPort0Assignment == ThcAssignmentNone && (Setup.PchI2cTouchPanelType != 0)) {
        DEBUG ((DEBUG_INFO, "THC0 Disabled. Configuring GPIO Touch Panel 1 set for other controller use\n"));
        ConfigureGpio ((VOID *) (UINTN) PcdGet32 (PcdBoardGpioTableTouchPanel1), (UINTN) PcdGet16 (PcdBoardGpioTableTouchPanel1Size));
      }
      break;
  }
  DEBUG ((DEBUG_INFO, "TouchPanelGpioInit End\n"));
}


/**
  Configure GPIO pads for CVF use
**/
VOID
CvfGpioInit (
  VOID
  )
{
  EFI_STATUS                      Status;
  SETUP_DATA                      SetupData;
  EFI_PEI_READ_ONLY_VARIABLE2_PPI *VariableServices;
  UINTN                           VarSize;

  DEBUG ((DEBUG_INFO, "CvfGpioInit Start\n"));

  //
  // Locate Setup variables
  //
  Status = PeiServicesLocatePpi (
             &gEfiPeiReadOnlyVariable2PpiGuid,
             0,
             NULL,
             (VOID **) &VariableServices
             );
  if (EFI_ERROR (Status)) {
    DEBUG ((DEBUG_ERROR, "CvfGpioInit: PeiServicesLocatePpi failed\n"));
    return;
  }

  VarSize = sizeof (SETUP_DATA);
  Status = VariableServices->GetVariable (
                               VariableServices,
                               L"Setup",
                               &gSetupVariableGuid,
                               NULL,
                               &VarSize,
                               &SetupData
                               );
  if (EFI_ERROR (Status)) {
    DEBUG ((DEBUG_ERROR, "CvfGpioInit: GetVariable (Setup) failed\n"));
    return;
  }

  //
  // Configure CVF GPIO only when its enabled in setup as NIO or USB Bridge.
  //
  if ((SetupData.CvfSupport > 0x00) && (SetupData.CvfSupport <3 )) {
    DEBUG ((DEBUG_INFO, "Configure CVF gpio\n"));
    ConfigureGpio ((VOID *) (UINTN) PcdGet32 (PcdBoardGpioTableCvf), (UINTN) PcdGet16 (PcdBoardGpioTableCvfSize));
  }
  DEBUG ((DEBUG_INFO, "CvfGpioInit End\n"));
}

VOID
ConnectivityGpioInit (
  VOID
  )
{
  EFI_STATUS                      Status;
  PCH_SETUP                       PchSetup;
  SETUP_DATA                      SetupData;
  UINTN                           VarSize;
  EFI_PEI_READ_ONLY_VARIABLE2_PPI *VariableServices;

  //
  // Locate Setup variables
  //
  Status = PeiServicesLocatePpi (
             &gEfiPeiReadOnlyVariable2PpiGuid,
             0,
             NULL,
             (VOID **) &VariableServices
             );
  if (EFI_ERROR (Status)) {
    DEBUG ((DEBUG_ERROR, "ConnectivityGpioInit: PeiServicesLocatePpi failed\n"));
    return;
  }

  VarSize = sizeof (PCH_SETUP);
  Status = VariableServices->GetVariable (
                               VariableServices,
                               L"PchSetup",
                               &gPchSetupVariableGuid,
                               NULL,
                               &VarSize,
                               &PchSetup
                               );
  if (EFI_ERROR (Status)) {
    DEBUG ((DEBUG_ERROR, "ConnectivityGpioInit: GetVariable (PchSetup) failed\n"));
    return;
  }

  VarSize = sizeof (SETUP_DATA);
  Status = VariableServices->GetVariable (
                               VariableServices,
                               L"Setup",
                               &gSetupVariableGuid,
                               NULL,
                               &VarSize,
                               &SetupData
                               );
  if (EFI_ERROR(Status)) {
    DEBUG((DEBUG_ERROR, "ConnectivityGpioInit: GetVariable (SetupData) failed\n"));
    return;
  }
  if (!CnviIsPresent () || (PchSetup.CnviMode == CnviModeDisabled)) {
    //
    // Discrete BT Module Selection as Disabled, Over USB or UART
    //
    PcdSet8S (PcdDiscreteBtModule, SetupData.DiscreteBtModule);
  }
  if (!CnviIsPresent () && PcdGet32 (PcdBoardGpioTableCnvd) != 0 && PcdGet16 (PcdBoardGpioTableCnvdSize) != 0) {
    ConfigureGpio ((VOID *) (UINTN) PcdGet32 (PcdBoardGpioTableCnvd), (UINTN) PcdGet16 (PcdBoardGpioTableCnvdSize));
  }
}

VOID
MipiCamConfigureGpio (
  IN GPIO_CONFIG *GpioConfig
  )
{
  ZeroMem(GpioConfig, sizeof(GPIO_CONFIG));
  GpioConfig->PadMode = GpioPadModeGpio;
  GpioConfig->HostSoftPadOwn = GpioHostOwnGpio;
  GpioConfig->Direction = GpioDirOut;
  GpioConfig->OutputState = GpioOutDefault;
  GpioConfig->InterruptConfig = GpioIntDis;
  GpioConfig->PowerConfig = GpioPlatformReset;
  GpioConfig->ElectricalConfig = GpioTermNone;
}

VOID
MipiCamGpioInit (
  VOID
  )
{
  PCH_SETUP                       PchSetup;
  SETUP_DATA                      SetupData;
  UINTN                           VarSize;
  EFI_PEI_READ_ONLY_VARIABLE2_PPI *VariableServices;
  EFI_STATUS                      Status;
  GPIO_CONFIG                     GpioConfig;
  GPIO_PAD                        GpioPad;
  UINT8                           GpioPin;

  //
  // Locate Setup variables
  //
  Status = PeiServicesLocatePpi (
             &gEfiPeiReadOnlyVariable2PpiGuid,
             0,
             NULL,
             (VOID **) &VariableServices
             );
  if (EFI_ERROR(Status)) {
    DEBUG((DEBUG_ERROR, "MipiCamGpioInit: PeiServicesLocatePpi failed\n"));
    return;
  }

  VarSize = sizeof (PCH_SETUP);
  Status = VariableServices->GetVariable (
                               VariableServices,
                               L"PchSetup",
                               &gPchSetupVariableGuid,
                               NULL,
                               &VarSize,
                               &PchSetup
                               );
  if (EFI_ERROR(Status)) {
    DEBUG((DEBUG_ERROR, "MipiCamGpioInit: GetVariable (PchSetup) failed\n"));
    return;
  }

  VarSize = sizeof (SETUP_DATA);
  Status = VariableServices->GetVariable (
                               VariableServices,
                               L"Setup",
                               &gSetupVariableGuid,
                               NULL,
                               &VarSize,
                               &SetupData
                               );
  if (EFI_ERROR(Status)) {
    DEBUG((DEBUG_ERROR, "MipiCamGpioInit: GetVariable (SetupData) failed\n"));
    return;
  }
  if (SetupData.MipiCam_ControlLogic0) {
    DEBUG((DEBUG_INFO, "MipiCamGpioInit ControlLogic0\n =%X",SetupData.MipiCam_ControlLogic0_Type));
    MipiCamConfigureGpio (&GpioConfig);
    //
    // If PMIC based control logic, configure gpio with TX state high to avoid PMIC goes into reset.
    //
    if((SetupData.MipiCam_ControlLogic0_Type == 2) || (SetupData.MipiCam_ControlLogic0_Type == 3)) {
      GpioConfig.OutputState = GpioOutHigh;
    }
    for (GpioPin = 0; GpioPin < SetupData.MipiCam_ControlLogic0_GpioPinsEnabled; GpioPin ++) {
      GpioPad = GpioGetGpioPadFromGroupIndexAndPadNumber (
                                   (UINT32)SetupData.MipiCam_ControlLogic0_GpioGroupNumber[GpioPin],
                                   (UINT32)SetupData.MipiCam_ControlLogic0_GpioGroupPadNumber[GpioPin]);
      DEBUG((DEBUG_INFO, "MipiCamGpioInit GpioPad 0x%x\n", GpioPad));
      GpioSetPadConfig (GpioPad, &GpioConfig);
    }
  }
  if (SetupData.MipiCam_ControlLogic1) {
    DEBUG((DEBUG_INFO, "MipiCamGpioInit ControlLogic1 =%X\n",SetupData.MipiCam_ControlLogic1_Type));
    MipiCamConfigureGpio (&GpioConfig);
    //
    // If PMIC based control logic, configure gpio with TX state high to avoid PMIC goes into reset.
    //
    if((SetupData.MipiCam_ControlLogic1_Type == 2) || (SetupData.MipiCam_ControlLogic1_Type == 3)) {
      GpioConfig.OutputState = GpioOutHigh;
    }
    for (GpioPin = 0; GpioPin < SetupData.MipiCam_ControlLogic1_GpioPinsEnabled; GpioPin ++) {
      GpioPad = GpioGetGpioPadFromGroupIndexAndPadNumber (
                                   (UINT32)SetupData.MipiCam_ControlLogic1_GpioGroupNumber[GpioPin],
                                   (UINT32)SetupData.MipiCam_ControlLogic1_GpioGroupPadNumber[GpioPin]);
      DEBUG((DEBUG_INFO, "MipiCamGpioInit GpioPad 0x%x\n", GpioPad));
      GpioSetPadConfig (GpioPad, &GpioConfig);
    }
  }
  if (SetupData.MipiCam_ControlLogic2) {
    DEBUG((DEBUG_INFO, "MipiCamGpioInit ControlLogic2 Type = %X\n",SetupData.MipiCam_ControlLogic2_Type));
    MipiCamConfigureGpio (&GpioConfig);
    //
    // If PMIC based control logic, configure gpio with TX state high to avoid PMIC goes into reset.
    //
    if((SetupData.MipiCam_ControlLogic2_Type == 2) || (SetupData.MipiCam_ControlLogic2_Type == 3)) {
      GpioConfig.OutputState = GpioOutHigh;
    }
    for (GpioPin = 0; GpioPin < SetupData.MipiCam_ControlLogic2_GpioPinsEnabled; GpioPin ++) {
      GpioPad = GpioGetGpioPadFromGroupIndexAndPadNumber (
                                   (UINT32)SetupData.MipiCam_ControlLogic2_GpioGroupNumber[GpioPin],
                                   (UINT32)SetupData.MipiCam_ControlLogic2_GpioGroupPadNumber[GpioPin]);
      DEBUG((DEBUG_INFO, "MipiCamGpioInit GpioPad 0x%x\n", GpioPad));
      GpioSetPadConfig (GpioPad, &GpioConfig);
    }
  }
  if (SetupData.MipiCam_ControlLogic3) {
    DEBUG((DEBUG_INFO, "MipiCamGpioInit ControlLogic3 Type = %X\n",SetupData.MipiCam_ControlLogic3_Type));
    MipiCamConfigureGpio (&GpioConfig);
    //
    // If PMIC based control logic, configure gpio with TX state high to avoid PMIC goes into reset.
    //
    if((SetupData.MipiCam_ControlLogic3_Type == 2) || (SetupData.MipiCam_ControlLogic3_Type == 3)) {
      GpioConfig.OutputState = GpioOutHigh;
    }
    for (GpioPin = 0; GpioPin < SetupData.MipiCam_ControlLogic3_GpioPinsEnabled; GpioPin ++) {
      GpioPad = GpioGetGpioPadFromGroupIndexAndPadNumber (
                                   (UINT32)SetupData.MipiCam_ControlLogic3_GpioGroupNumber[GpioPin],
                                   (UINT32)SetupData.MipiCam_ControlLogic3_GpioGroupPadNumber[GpioPin]);
      DEBUG((DEBUG_INFO, "MipiCamGpioInit GpioPad 0x%x\n", GpioPad));
      GpioSetPadConfig (GpioPad, &GpioConfig);
    }
  }
  return;
}


VOID
TsnDeviceGpioInit (
  VOID
  )
{
#if FixedPcdGet8(PcdEmbeddedEnable) == 0x1
  EFI_STATUS                      Status;
  PCH_SETUP                       PchSetup;
  UINTN                           VarSize;
  EFI_PEI_READ_ONLY_VARIABLE2_PPI *VariableServices;

  DEBUG ((DEBUG_INFO, "TsnDeviceGpioInit() Start\n"));

  //
  // Locate Setup variables
  //
  Status = PeiServicesLocatePpi (
             &gEfiPeiReadOnlyVariable2PpiGuid,
             0,
             NULL,
             (VOID **) &VariableServices
             );
  if (EFI_ERROR (Status)) {
    DEBUG ((DEBUG_ERROR, "TsnGpioInit: PeiServicesLocatePpi failed\n"));
    return;
  }

  VarSize = sizeof (PCH_SETUP);
  Status = VariableServices->GetVariable (
                               VariableServices,
                               L"PchSetup",
                               &gPchSetupVariableGuid,
                               NULL,
                               &VarSize,
                               &PchSetup
                               );
  if (EFI_ERROR (Status)) {
    DEBUG ((DEBUG_ERROR, "TsnGpioInit: GetVariable (PchSetup) failed\n"));
    return;
  }

  if (PchSetup.PchTsnEnable == 1) {
    DEBUG ((DEBUG_INFO, "TSN Device GpioInit\n"));
    ConfigureGpio ((VOID *) (UINTN) PcdGet32 (PcdBoardGpioTableTsnDevice), (UINTN) PcdGet16 (PcdBoardGpioTableTsnDeviceSize));
  } else {
    //
    // Disable Tsn Pcs if TsnEnable feature not available
    //
    PchSetup.TsnPcsEnabled = 0;
  }
  DEBUG ((DEBUG_INFO, "TsnDeviceGpioInit() End\n"));
#endif
}

/**
  Configure advanced GPIO

**/
VOID
GpioInitAdvanced (
  VOID
  )
{
  FoxvilleLanGpioInit ();
  TouchPanelGpioInit ();

  // Configure Connectivity options
  ConnectivityGpioInit();
  if (PcdGetBool (PcdMipiCamGpioEnable)) {
    DEBUG ((DEBUG_INFO, "\nMIPI Camera GpioInit\n"));
    MipiCamGpioInit();
    DEBUG ((DEBUG_INFO, "\nCVF GpioInit\n"));
    CvfGpioInit();
  }


  TsnDeviceGpioInit ();

  //
  // Lock pads after initializing platform GPIO.
  // Pads which were requested to be unlocked during configuration
  // will not be locked.
  //
  GpioLockPads ();

  return;
}
//[-start-200831-IB17800091-add]//
/**
  This function can determine CPU or PCH root port.

  @param  RpDevice    Device number of root port.
  @param  IsCpuRp     Is CPU root port.
  @param  IsPchRp     Is PCH root port.

**/
VOID
EFIAPI
DetermineRootPort (
  IN  UINTN      RpDevice,
  OUT BOOLEAN    *IsCpuRp,
  OUT BOOLEAN    *IsPchRp
  )
{
  switch (RpDevice) {
    case SA_PEG_DEV_NUM:
    case SA_PEG3_DEV_NUM:
      *IsCpuRp = TRUE;
      *IsPchRp = FALSE;
      break;
    case PCI_DEVICE_NUMBER_PCH_PCIE_ROOT_PORT_1:
    case PCI_DEVICE_NUMBER_PCH_PCIE_ROOT_PORT_9:
    case PCI_DEVICE_NUMBER_PCH_PCIE_ROOT_PORT_17:
      *IsCpuRp = FALSE;
      *IsPchRp = TRUE;
      break;
    default:
      *IsCpuRp = FALSE;
      *IsPchRp = FALSE;
      break;
  }
}
//[-end-200831-IB17800091-add]//
/**
  Update DeviceInfo PCD before dispatch PciResourceInitPei.inf.

  @retval EFI_SUCCESS    Update DeviceInfo PCDs successfully.
  @retval Others         Failed to update DeviceInfo PCDs.

**/
EFI_STATUS
EFIAPI
UpdateDeviceInfoPcd (
  IN CONST EFI_PEI_SERVICES       **PeiServices,
  IN EFI_PEI_NOTIFY_DESCRIPTOR    *NotifyDescriptor,
  IN VOID                         *Ppi
  )
{
  EFI_STATUS                  Status;
  BOOLEAN                     IsCpuRp;
  BOOLEAN                     IsPchRp;
//[-start-200903-IB17040151-add]//
  UINT32                      BarIndex;
  UINT32                      DeviceAttribCount;
//[-end-200903-IB17040151-add]//
//[-start-200831-IB17800091-add]//
  UINT32                      RootPort;
//[-end-200831-IB17800091-add]//
  UINTN                       TokenNumber;
  UINTN                       RpIndex;
  UINTN                       RpDevice;
  UINTN                       RpFunction;
  UINT64                      CpuRpBase;
//[-start-200903-IB17040151-modify]//
  CHAR8                       *BarStrAddress;
  CHAR8                       *DeviceAttribValue;
//[-end-200903-IB17040151-modify]//
  EFI_DEVICE_PATH_PROTOCOL    *DevicePathNode;
//[-start-200903-IB17040152-modify]//
  EFI_DEVICE_PATH_PROTOCOL    *OrigDevicePathNode;
//[-end-200903-IB17040152-modify]//
  PCI_DEVICE_PATH             *PciDevicePath;

  Status             = EFI_UNSUPPORTED;
  IsCpuRp            = FALSE;
  IsPchRp            = FALSE;
//[-start-200903-IB17040151-add]//
  BarIndex           = 0;
  DeviceAttribCount  = 0;
//[-end-200903-IB17040151-add]//
//[-start-200831-IB17800091-add]//
  RootPort           = 0;
//[-end-200831-IB17800091-add]//
  TokenNumber        = 0;
  RpIndex            = 0;
  RpDevice           = 0;
  RpFunction         = 0;
  CpuRpBase          = 0;
//[-start-200903-IB17040151-modify]//
  BarStrAddress      = NULL;
  DeviceAttribValue  = NULL;
//[-end-200903-IB17040151-modify]//
  DevicePathNode     = NULL;
//[-start-200903-IB17040152-modify]//
  OrigDevicePathNode = NULL;
//[-end-200903-IB17040152-modify]//
  PciDevicePath      = NULL;

  for (TokenNumber = LibPcdGetNextToken (&gH2ODeviceInfo2TokenSpaceGuid, 0)
       ; TokenNumber != 0
       ; TokenNumber = LibPcdGetNextToken (&gH2ODeviceInfo2TokenSpaceGuid, TokenNumber)
       ) {
//[-start-200903-IB17040151-modify]//
//[-start-200903-IB17040152-modify]//
    Status = H2OGetDeviceInfo (TokenNumber, &OrigDevicePathNode, &DeviceAttribCount);
//[-end-200903-IB17040152-modify]//
    if (EFI_ERROR (Status)) {
//[-end-200903-IB17040151-modify]//
      continue;
    }

//[-start-200903-IB17040151-modify]//
//[-start-200903-IB17040152-modify]//
    if (OrigDevicePathNode == NULL) {
      continue;
    }

    for (DevicePathNode = OrigDevicePathNode
         ; !IsDevicePathEnd (DevicePathNode)
         ; DevicePathNode = NextDevicePathNode (DevicePathNode)
         ) {
//[-end-200903-IB17040152-modify]//
//[-end-200903-IB17040151-modify]//
      if (IS_PCI_DP (DevicePathNode)) {
        PciDevicePath = (PCI_DEVICE_PATH *)DevicePathNode;
        RpDevice = (UINTN)PciDevicePath->Device;
        RpFunction = (UINTN)PciDevicePath->Function;
//[-start-200831-IB17800091-modify]//

        //
        // Determine CPU or PCH root port for DeviceInfo PCD.
        //
        DetermineRootPort (RpDevice, &IsCpuRp, &IsPchRp);
        //
        // Determine CPU or PCH root port.
        //
//        switch (RpDevice) {
//          case SA_PEG_DEV_NUM:
//          case SA_PEG3_DEV_NUM:
//            IsCpuRp = TRUE;
//            IsPchRp = FALSE;
//            break;
//          case PCI_DEVICE_NUMBER_PCH_PCIE_ROOT_PORT_1:
//          case PCI_DEVICE_NUMBER_PCH_PCIE_ROOT_PORT_9:
//          case PCI_DEVICE_NUMBER_PCH_PCIE_ROOT_PORT_17:
//            IsCpuRp = FALSE;
//            IsPchRp = TRUE;
//            break;
//          default:
//            IsCpuRp = FALSE;
//            IsPchRp = FALSE;
///            break;
//        }
//[-end-200831-IB17800091-modify]//

        //
        // Continue to get next device path node if it not CPU or PCH root port.
        //
        if ((!IsCpuRp) && (!IsPchRp)) {
          continue;
        }

        //
        // CPU root port.
        //
        if (IsCpuRp) {
//[-start-200831-IB17800091-add]//
          //
          // If PcdNvmeRootPortAddress is set, update the values of RpDevice and
          // RpFunction from this PCD if this root port is CPU.
          //
          if (PcdGet32 (PcdNvmeRootPortAddress) != 0) {
            RootPort = PcdGet32 (PcdNvmeRootPortAddress);
            DetermineRootPort (((UINTN)((RootPort >> 8) & 0xFF)), &IsCpuRp, &IsPchRp);
            if (IsCpuRp) {
              //
              // Update RpDevice and RpFunction from PcdNvmeRootPortAddress.
              //
              RpDevice = (UINTN)((RootPort >> 8) & 0xFF);
              RpFunction = (UINTN)(RootPort & 0xFF);
//[-start-200903-IB17040151-add]//

              //
              // CPU root port:
              // If PcdNvmeMemBaseAddress is set, update the BAR address into device info list HOB.
              //
              if (PcdGet32 (PcdNvmeMemBaseAddress) != 0) {
                for (BarIndex = 0; BarIndex < ARRAY_SIZE (mPciBarAttribName); BarIndex++) {
                  Status = H2OGetDeviceInfoAttribByName (TokenNumber, mPciBarAttribName[BarIndex], &DeviceAttribValue);
                  if (EFI_ERROR (Status)) {
                    continue;
                  }
                  Status = PeiServicesAllocatePool (AsciiStrSize (DeviceAttribValue), (VOID **)&BarStrAddress);
                  ASSERT_EFI_ERROR (Status);
                  AsciiSPrint (BarStrAddress, AsciiStrSize (DeviceAttribValue), "0x%x", PcdGet32 (PcdNvmeMemBaseAddress));
//[-start-200903-IB17040152-modify]//
                  Status = H2OSetDeviceInfoAttribByName (TokenNumber, mPciBarAttribName[BarIndex], BarStrAddress);
                  if (EFI_ERROR (Status)) {
                    continue;
                  }
//[-end-200903-IB17040152-modify]//
                }
              }
//[-end-200903-IB17040151-add]//
            } else {
              //
              // Restore the values of IsCpuRp and IsPchRp.
              //
              IsCpuRp = TRUE;
              IsPchRp = FALSE;
            }
          }

//[-end-200831-IB17800091-add]//
          for (RpIndex = 0; RpIndex < GetMaxCpuPciePortNum (); RpIndex++) {
            if ((RpDevice == mCpuPcieRpInfo[RpIndex].Device) &&
                (RpFunction == mCpuPcieRpInfo[RpIndex].Function)) {
              break;
            }
          }

          Status = GetCpuPcieRpDevFun (RpIndex, &RpDevice, &RpFunction);
          if (EFI_ERROR (Status)) {
            DEBUG ((DEBUG_INFO, "Failed to GetCpuPcieRpDevFun, Status = %r\n", Status));
            return Status;
          }
          DEBUG ((DEBUG_INFO, "CPU RpDevice   = 0x%X\n", RpDevice));
          DEBUG ((DEBUG_INFO, "CPU RpFunction = 0x%X\n", RpFunction));
        }

        //
        // PCH root port.
        //
        if (IsPchRp) {
//[-start-200831-IB17800091-add]//
          //
          // If PcdNvmeRootPortAddress is set, update the values of RpDevice and
          // RpFunction from this PCD if this root port is PCH.
          //
          if (PcdGet32 (PcdNvmeRootPortAddress) != 0) {
            RootPort = PcdGet32 (PcdNvmeRootPortAddress);
            DetermineRootPort (((UINTN)((RootPort >> 8) & 0xFF)), &IsCpuRp, &IsPchRp);
            if (IsPchRp) {
              //
              // Update RpDevice and RpFunction from PcdNvmeRootPortAddress.
              //
              RpDevice = (UINTN)((RootPort >> 8) & 0xFF);
              RpFunction = (UINTN)(RootPort & 0xFF);
//[-start-200903-IB17040151-add]//

              //
              // PCH root port:
              // If PcdNvmeMemBaseAddress is set, update the BAR address into device info list HOB.
              //
              if (PcdGet32 (PcdNvmeMemBaseAddress) != 0) {
                for (BarIndex = 0; BarIndex < ARRAY_SIZE (mPciBarAttribName); BarIndex++) {
                  Status = H2OGetDeviceInfoAttribByName (TokenNumber, mPciBarAttribName[BarIndex], &DeviceAttribValue);
                  if (EFI_ERROR (Status)) {
                    continue;
                  }
                  Status = PeiServicesAllocatePool (AsciiStrSize (DeviceAttribValue), (VOID **)&BarStrAddress);
                  ASSERT_EFI_ERROR (Status);
                  AsciiSPrint (BarStrAddress, AsciiStrSize (DeviceAttribValue), "0x%x", PcdGet32 (PcdNvmeMemBaseAddress));
//[-start-200903-IB17040152-modify]//
                  Status = H2OSetDeviceInfoAttribByName (TokenNumber, mPciBarAttribName[BarIndex], BarStrAddress);
                  if (EFI_ERROR (Status)) {
                    continue;
                  }
//[-end-200903-IB17040152-modify]//
                }
              }
//[-end-200903-IB17040151-add]//
            } else {
              //
              // Restore the values of IsCpuRp and IsPchRp.
              //
              IsCpuRp = FALSE;
              IsPchRp = TRUE;
            }
          }

//[-end-200831-IB17800091-add]//
          for (RpIndex = 0; RpIndex < GetPchMaxPciePortNum (); RpIndex++) {
            if ((RpDevice == mPchPcieRpInfo[RpIndex].Device) &&
                (RpFunction == mPchPcieRpInfo[RpIndex].Function)) {
              break;
            }
          }

          Status = GetPchPcieRpDevFun (RpIndex, &RpDevice, &RpFunction);
          if (EFI_ERROR (Status)) {
            DEBUG ((DEBUG_INFO, "Failed to GetPchPcieRpDevFun, Status = %r\n", Status));
            return Status;
          }
          DEBUG ((DEBUG_INFO, "PCH RpDevice   = 0x%X\n", RpDevice));
          DEBUG ((DEBUG_INFO, "PCH RpFunction = 0x%X\n", RpFunction));
        }

        //
        // Update correct Device/Function number into device info list HOB.
        //
        PciDevicePath->Device = (UINT8)RpDevice;
        PciDevicePath->Function = (UINT8)RpFunction;
//[-start-200903-IB17040152-add]//
        Status = H2OSetDeviceInfoDevicePath (TokenNumber, OrigDevicePathNode);
//[-end-200903-IB17040152-add]//
      }
    }
  }
  return Status;
}
//[-end-200831-IB17800090-add]//

/**
  Platform Init PEI module entry point

  @param[in]  FileHandle           Not used.
  @param[in]  PeiServices          General purpose services available to every PEIM.

  @retval     EFI_SUCCESS          The function completes successfully
  @retval     EFI_OUT_OF_RESOURCES Insufficient resources to create database
**/
EFI_STATUS
EFIAPI
PlatformInitAdvancedPostMemEntryPoint (
  IN       EFI_PEI_FILE_HANDLE  FileHandle,
  IN CONST EFI_PEI_SERVICES     **PeiServices
  )
{
  EFI_STATUS                       Status;
  EFI_PEI_READ_ONLY_VARIABLE2_PPI  *VariableServices;
  SETUP_DATA                       SetupData;
  UINTN                            VariableSize;
  UINT8                            CrashLogCollect;
  PEI_CORE_INSTANCE                *PrivateData;
  UINTN                            CurrentFv;
  PEI_CORE_FV_HANDLE               *CoreFvHandle;
  VOID                             *HobData;
//[-start-200420-IB17800056-modify]//
  SETUP_DATA                       SystemConfiguration;
  BOOLEAN                          SetupVariableExist;
  UINTN                            SetupDataSize;
  EFI_BOOT_MODE                    BootMode;
  EFI_STATUS                       OemSvcStatus;
//[-end-200420-IB17800056-modify]//

  PostCode (PLATFORM_INIT_POSTMEM_ENTRY);

  Status = PeiServicesLocatePpi (
             &gEfiPeiReadOnlyVariable2PpiGuid,  // GUID
             0,                                 // INSTANCE
             NULL,                              // EFI_PEI_PPI_DESCRIPTOR
             (VOID **) &VariableServices        // PPI
             );
  ASSERT_EFI_ERROR (Status);

  VariableSize = sizeof (SETUP_DATA);
  Status = VariableServices->GetVariable (
                               VariableServices,
                               PLATFORM_SETUP_VARIABLE_NAME,
                               &gSetupVariableGuid,
                               NULL,
                               &VariableSize,
                               &SetupData
                               );
  if (EFI_ERROR (Status)) {
    DEBUG ((DEBUG_ERROR, "Fail to get System Configuration and set the configuration to production mode!\n"));
    CrashLogCollect = 1;
  } else {
    CrashLogCollect = SetupData.EnableCrashLog;
  }
  //
  // BIOS shall extract Crash records from PMC SSRAM after memory init
  // and before enabling the Energy Reporting feature
  //
  if (CrashLogCollect == 1) {
    CrashLogCollectDataFromPmcSSRAM ();
    CpuCrashLogCollectDataFromTelemetrySRAM ();
  }
  GpioInitAdvanced();
  //
  // Build a HOB to show current FV location for SA policy update code to consume.
  //
  PrivateData = PEI_CORE_INSTANCE_FROM_PS_THIS(PeiServices);
  CurrentFv = PrivateData->CurrentPeimFvCount;
  CoreFvHandle = &(PrivateData->Fv[CurrentFv]);

  HobData = BuildGuidHob (
             &gPlatformInitFvLocationGuid,
             sizeof (VOID *)
             );
  ASSERT (HobData != NULL);
  CopyMem (HobData, (VOID *) &CoreFvHandle, sizeof (VOID *));

  //
  // Create USB Boot First hotkey information HOB
  //
//[-start-200420-IB17800056-modify]//
  // ADL PO temporary remove
  // GINO: new RC add, maybe dont need it.
  // CreateAttemptUsbFirstHotkeyInfoHob ();
//[-end-200420-IB17800056-modify]//

#if FixedPcdGetBool(PcdAdlLpSupport) == 1
  //
  // Install mPeiGraphicsPlatformPpi
  //
  DEBUG ((DEBUG_INFO, "Install mPeiGraphicsPlatformPpi \n"));
  Status = PeiServicesInstallPpi (&mPeiGraphicsPlatformPpi);
#else
  //
  // Notify mPeiGraphicsPlatformNotifyList
  //
  DEBUG ((DEBUG_INFO, "Notify mPeiGraphicsPlatformNotifyList \n"));
  Status = PeiServicesNotifyPpi (&mPeiGraphicsPlatformNotifyList);
#endif

  Status = PeiServicesNotifyPpi (&mPlatformDebugStateChecksNotifyList);

  //
  // Performing PlatformInitEndOfPei after EndOfPei PPI produced
  //
  Status = PeiServicesNotifyPpi (&mEndOfPeiNotifyList);
  PostCode (PLATFORM_INIT_POSTMEM_EXIT);

//[-start-200420-IB17800056-modify]//
  SetupVariableExist = FALSE;
  SetupDataSize = sizeof (SETUP_DATA);

  Status = CommonGetVariable (
             PLATFORM_SETUP_VARIABLE_NAME,
             &gSetupVariableGuid,
             &SetupDataSize,
             &SystemConfiguration
             );

  if (!EFI_ERROR (Status)) {
    SetupVariableExist = TRUE;
    DEBUG ((EFI_D_INFO, "SystemConfiguration variable exist.\n"));
  } else {
    DEBUG ((EFI_D_INFO, "SystemConfiguration variable get fail.\n"));
    ASSERT_EFI_ERROR (Status);
  }

  //
  // OemServices
  //
  DEBUG ((DEBUG_INFO, "OemKernelServices Call: OemSvcModifySetupVarPlatformStage2 \n"));
  Status = OemSvcModifySetupVarPlatformStage2  (
             (VOID *)&SystemConfiguration,
             SetupVariableExist
             );
  DEBUG ((DEBUG_INFO, "OemKernelServices OemSvcModifySetupVarPlatformStage2 Status: %r\n", Status));

//
//  Chipset EC Lib has same code of LowPowerS0Idle/CSNotifyEC/EcLowPowerMode for EC of CRB.
// .
  if (SystemConfiguration.LowPowerS0Idle && SystemConfiguration.CSNotifyEC && SystemConfiguration.EcLowPowerMode) {
    DEBUG_OEM_SVC ((DEBUG_INFO, "Base OemChipsetServices Call: OemSvcEcSetLowPowerMode (TRUE)\n"));
    Status = OemSvcEcSetLowPowerMode (TRUE);
    DEBUG_OEM_SVC ((DEBUG_INFO, "Base OemChipsetServices OemSvcEcSetLowPowerMode Status: %r\n", Status));
  } else {
    DEBUG_OEM_SVC ((DEBUG_INFO, "Base OemChipsetServices Call: OemSvcEcSetLowPowerMode (FALSE)\n"));
    Status = OemSvcEcSetLowPowerMode (FALSE);
    DEBUG_OEM_SVC ((DEBUG_INFO, "Base OemChipsetServices OemSvcEcSetLowPowerMode Status: %r\n", Status));
  }

  Status = PeiServicesGetBootMode(&BootMode);
  if ((BootMode == BOOT_ON_S3_RESUME)) {
    Status = PeiServicesNotifyPpi(&mSetS3RestoreAcpiNotify);
    ASSERT_EFI_ERROR (Status);
//    return Status;
  }

//[-start-200831-IB17800090-add]//
  if (BootMode == BOOT_IN_RECOVERY_MODE) {
    DEBUG ((DEBUG_INFO, "Start to update DeviceInfo PCD\n"));
    Status = PeiServicesNotifyPpi (&mUpdateDeviceInfoPcdNotify);
    ASSERT_EFI_ERROR (Status);
  }
//[-end-200831-IB17800090-add]//

  //
  // Notify NotifyPpiList
  //
  Status = PeiServicesNotifyPpi (&mNotifyPpiList[0]);
  ASSERT_EFI_ERROR (Status);

  //
  // OemServices
  //
  DEBUG ((DEBUG_INFO, "OemKernelServices Call: OemSvcInitPlatformStage2 \n"));
  OemSvcStatus = OemSvcInitPlatformStage2 (
    (VOID *)&SystemConfiguration,
    SetupVariableExist
    );
  DEBUG ((DEBUG_INFO, "OemKernelServices OemSvcInitPlatformStage2 Status: %r\n", OemSvcStatus));
  ASSERT_EFI_ERROR (Status);

  if (FeaturePcdGet (PcdH2OPeiCpInitPlatformStage2Supported)) {
    H2O_PEI_CP_INIT_PLATFORM_STAGE_2_DATA                      CpInitPlatformStage2Data;

    CpInitPlatformStage2Data.Size   = sizeof (H2O_PEI_CP_INIT_PLATFORM_STAGE_2_DATA);
    CpInitPlatformStage2Data.Status = H2O_CP_TASK_NORMAL;

    DEBUG ((DEBUG_INFO, "Checkpoint Trigger: %g\n", &gH2OPeiCpInitPlatformStage2Guid));
    H2OCpTrigger (&gH2OPeiCpInitPlatformStage2Guid, &CpInitPlatformStage2Data);
    DEBUG ((DEBUG_INFO, "Checkpoint Result: %x\n", CpInitPlatformStage2Data.Status));
  }
//[-end-200420-IB17800056-modify]//

  return Status;
}
