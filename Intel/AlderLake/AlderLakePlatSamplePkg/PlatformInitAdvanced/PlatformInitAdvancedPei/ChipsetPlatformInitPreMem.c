/** @file

;******************************************************************************
;* Copyright (c) 2017 - 2020, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/
#include <PiPei.h>
#include <Uefi.h>
#include <PostCode.h>
#include <Guid/H2OCp.h>
#include <Library/PeiServicesLib.h>
#include <ChipsetSetupConfig.h>
//[-start-190611-IB16990044-add]//
#include <Register/PttPtpRegs.h>
//[-end-190611-IB16990044-add]//
#include <Library/PeiInsydeChipsetLib.h>
#include <Ppi/ReadOnlyVariable2.h>
//[-start-190819-IB17700068-add]//
#include <Ppi/FirmwareVolumeInfo.h>
//[-end-190819-IB17700068-add]//
#include <Library/DebugLib.h>
#include <Library/PeiOemSvcKernelLib.h>
#include <Library/HobLib.h>
#include <Library/BaseMemoryLib.h>
#include <Library/BaseOemSvcChipsetLib.h>
#include <Library/PeiOemSvcChipsetLib.h>
#include <Library/BaseOemSvcKernelLib.h>
//#include <Library/PeiChipsetSvcLib.h>
#include <Library/PchCycleDecodingLib.h>
#include <Library/CmosLib.h>
#include <ChipsetCmos.h>
#include <Library/PmcLib.h>
#include <Library/PmcPrivateLib.h>
#include <Library/FlashRegionLib.h>
#include <Library/H2OCpLib.h>
// gino: add this for IoLib >>
#include <Library/IoLib.h>
// gino: add this for IoLib <<

//[-start-190614-IB17700033-add]//
#include <TpmPolicy.h>
//[-end-190614-IB17700033-add]//
//[-start-210322-IB16740136-add]// for build error, some definition are defined in TcoRegs.h
#include <Register/TcoRegs.h>
//[-end-210322-IB16740136-add]//

//[-start-190611-IB16990044-add]//
EFI_STATUS
PttInit (
  IN EFI_PEI_SERVICES               **PeiServices,
  IN EFI_PEI_NOTIFY_DESCRIPTOR      *NotifyDescriptor,
  IN VOID                           *Ppi
  );

EFI_PEI_NOTIFY_DESCRIPTOR mTpmDeviceSeleted[] = {
  (EFI_PEI_PPI_DESCRIPTOR_NOTIFY_CALLBACK | EFI_PEI_PPI_DESCRIPTOR_TERMINATE_LIST),
  &gEfiEndOfPeiSignalPpiGuid,
  PttInit
};
//[-end-190611-IB16990044-add]//
//[-start-190611-IB16990044-add]//
/**
 Change TPM interface from DTPM to FTPM if PTT is present.

 @param  PeiServices                   PEI Service Table

 @retval EFI_SUCCESS                   PTT is present and PcdTpmInstanceGuid is updated to FTPM successfully.
 @retval EFI_NOT_FOUND                 PTT is not present and PcdTpmInstanceGuid keeps default(DTPM).
**/
EFI_STATUS
PttInit (
  IN EFI_PEI_SERVICES                  **PeiServices,
  IN EFI_PEI_NOTIFY_DESCRIPTOR         *NotifyDescriptor,
  IN VOID                              *Ppi
  )
{
//  UINTN                                Size;
//  UINT32                               TpmInterfaceId;
//
//  TpmInterfaceId = MmioRead8 (R_PTT_HCI_BASE_ADDRESS + R_TPM_INTERFACE_ID);
//  if (TpmInterfaceId == 0xFF) {
//    return EFI_NOT_FOUND;
//  }
//
  UINT32                               TpmStsFtif;
//[-start-190614-IB17700033-add]//
  UINT32                               TpmPolicy;
  EFI_STATUS                           Status;
//[-end-190614-IB17700033-add]//

  TpmStsFtif = MmioRead32 (R_PTT_TXT_STS_FTIF);

  if ((TpmStsFtif & V_FTIF_FTPM_PRESENT) != ((UINT32) V_FTIF_FTPM_PRESENT)) {
    return EFI_NOT_FOUND;
  }

  PcdSet32S (PcdTpm2CurrentIrqNum, 0);

//[-start-190614-IB17700033-add]//
  //
  // Disable TPM FMP support
  //
  TpmPolicy = PcdGet32 (PcdDxeTpmPolicy);
  TpmPolicy |= SKIP_TPM_FMP_INSTALLATION;
  Status = PcdSet32S (PcdDxeTpmPolicy, TpmPolicy);
  DEBUG ((EFI_D_INFO, "Don't support FMP for PTT -%r\n", Status));
//[-end-190614-IB17700033-add]//

  return EFI_SUCCESS;
}
//[-end-190611-IB16990044-add]//

//[-start-200120-IB10189019-add]//
/**
 Provide a callback function for CRB use.
 After FvMain corrupted, trigger system to entry recovery mode by filling CMOS data.

 @param[in] Event          A pointer to the Event that triggered the callback.
 @param[in] Handle         Checkpoint handle.
**/
VOID
EFIAPI
CpFvCorruptedCallback (
  IN EFI_EVENT                         Event,
  IN H2O_CP_HANDLE                     Handle
  )
{
  EFI_STATUS                         Status;
  H2O_PEI_CP_DXE_FV_CORRUPTED_DATA   *DxeFvCorruptedData;

  Status = H2OCpLookup (Handle, (VOID **) &DxeFvCorruptedData, NULL);
  if (EFI_ERROR (Status)) {
    DEBUG_CP ((DEBUG_ERROR, "Checkpoint Data Not Found: %x (%r)\n", Handle, Status));
    DEBUG_CP ((DEBUG_ERROR, "   %a\n", __FUNCTION__));
    return;
  }
  WriteExtCmos8 (R_XCMOS_INDEX, R_XCMOS_DATA, ChipsetRecoveryFlag, RECOVERY_VALUE);
  DxeFvCorruptedData->ResetSystem = TRUE;
  DxeFvCorruptedData->Status      = H2O_CP_TASK_UPDATE;
}
//[-end-200120-IB10189019-add]//

STATIC
VOID
DoCenturyRollover (
  VOID
  )
{
  UINT16  Data16;
  UINT8   Data8;
  BOOLEAN IsSet;
  UINT16  TcoBase;

  PchTcoBaseGet (&TcoBase);
  Data8 = ReadCmos8 (RTC_ADDRESS_CENTURY);
  if ((Data8 & 0x0F) == 0x9) {
    Data8 = Data8 >> 4;
    Data8++;
    Data8 = Data8 << 4;
  } else {
    Data8++;
  }
  WriteCmos8 (RTC_ADDRESS_CENTURY, Data8);

  ///
  /// Don't return until the bit actually clears.
  ///
  IoWrite16 (TcoBase + R_TCO_IO_TCO1_STS, B_TCO_IO_TCO1_STS_NEWCENTURY);
  IsSet = TRUE;
  while (IsSet) {
    Data16 = IoRead16 (TcoBase + R_TCO_IO_TCO1_STS);
    if ((Data16 & B_TCO_IO_TCO1_STS_NEWCENTURY) != B_TCO_IO_TCO1_STS_NEWCENTURY) {
      IsSet = FALSE;
    }
    ///
    /// IsSet will eventually clear -- or else we'll have
    /// an infinite loop.
    ///
  }
}

EFI_STATUS
ChipsetPlatformInitPreMem (
  VOID
  )
{
//[-start-190611-IB16990044-add]//
  CHIPSET_CONFIGURATION                ChipsetConfiguration;
  EFI_STATUS                           Status;
//[-end-190611-IB16990044-add]//
  UINT32                               BaseAddress;
  UINT16                               TcoBase;
  UINT16                               Data16;
  EFI_STATUS                           OemSvcStatus;
//[-start-200120-IB10189019-add]//
  H2O_CP_HANDLE                     CpHandle;
//[-end-200120-IB10189019-add]//

  POST_CODE (PEI_SB_REG_INIT);  //PostCode = 0x76, South Bridge Early Initial

//[-start-200120-IB10189019-add]//
  //
  // Register callback function of Checkpoint to execute recovery BIOS when FvMain is corrupted.
  //
  if (FeaturePcdGet (PcdH2OPeiCpDxeFvCorruptedSupported)) {

    Status = H2OCpRegisterHandler (
               &gH2OPeiCpDxeFvCorruptedGuid,
               CpFvCorruptedCallback,
               H2O_CP_MEDIUM,
               &CpHandle
               );

    if (EFI_ERROR (Status)) {
      DEBUG_CP ((DEBUG_ERROR, "Checkpoint Register Fail: %g (%r)\n", &gH2OPeiCpDxeFvCorruptedGuid, Status));
      return Status;
    }
    DEBUG_CP ((DEBUG_INFO, "Checkpoint Registered: %g (%r)\n", &gH2OPeiCpDxeFvCorruptedGuid, Status));
  }
//[-end-200120-IB10189019-add]//

  //
  // OemServices
  //
//[-start-190724-IB17700055-modify]//
  DEBUG_OEM_SVC ((DEBUG_INFO, "OemKernelServices Call: OemSvcInitPlatformStage1 \n"));
  OemSvcStatus = OemSvcInitPlatformStage1 ();
  DEBUG_OEM_SVC ((DEBUG_INFO, "OemKernelServices OemSvcInitPlatformStage1 Status: %r\n", OemSvcStatus));
//[-end-190724-IB17700055-modify]//

  if (FeaturePcdGet (PcdH2OPeiCpInitPlatformStage1Supported)) {
    H2O_PEI_CP_INIT_PLATFORM_STAGE_1_DATA                      CpInitPlatformStage1Data;

    CpInitPlatformStage1Data.Size   = sizeof (H2O_PEI_CP_INIT_PLATFORM_STAGE_1_DATA);
    CpInitPlatformStage1Data.Status = H2O_CP_TASK_NORMAL;

    DEBUG ((DEBUG_INFO, "Checkpoint Trigger: %g\n", &gH2OPeiCpInitPlatformStage1Guid));
    H2OCpTrigger (&gH2OPeiCpInitPlatformStage1Guid, &CpInitPlatformStage1Data);
    DEBUG ((DEBUG_INFO, "Checkpoint Result: %x\n", CpInitPlatformStage1Data.Status));
  }
//[-start-190611-IB16990044-add]//
  //
  // Change TPM interface from DTPM to FTPM if PTT is present.
  //
  Status = GetChipsetSetupVariablePei (&ChipsetConfiguration);
  if (!EFI_ERROR (Status)) {
    if(ChipsetConfiguration.PTTEnable == 1) {
      Status = PeiServicesNotifyPpi (mTpmDeviceSeleted);
      ASSERT_EFI_ERROR (Status);
    }
  } else {
    ASSERT_EFI_ERROR (Status);
  }
//[-end-190611-IB16990044-add]//
  //
  // Century Rollover according to PCH BIOS Specification 20.4
  // 1. After an RTC battery failure the century rollover status bit is in indeterminate state
  // 2. The century rollover status bit is not a resume event and must be checked by the system BIOS after booting or resume
  // 3  SMI code is done on Y2KRolloverCallBack
  //
  BaseAddress = PmcGetPwrmBase ();
  PchTcoBaseGet (&TcoBase);
  Data16 = IoRead16 (TcoBase + R_TCO_IO_TCO1_STS);

  if (((Data16 & B_TCO_IO_TCO1_STS_NEWCENTURY) == B_TCO_IO_TCO1_STS_NEWCENTURY)
     && ((MmioRead32 (BaseAddress + R_PMC_PWRM_GEN_PMCON_B) & B_PMC_PWRM_GEN_PMCON_B_RTC_PWR_STS) != B_PMC_PWRM_GEN_PMCON_B_RTC_PWR_STS)) {
    DoCenturyRollover();
  }
  return Status;
}
//[-start-190613-IB16990064-add]//
/**
  If boot mode is recovery mode and FSP is API mode, we should not report FSP-S FV.

  @param[in] Event  - A pointer to the Event that triggered the callback.
  @param[in] Handle - Checkpoint handle.
**/
VOID
EFIAPI
PublishFvInRecoveryCallback (
  IN EFI_EVENT                  Event,
  IN H2O_CP_HANDLE              Handle
  )
{
  EFI_STATUS                                     Status;
  H2O_PEI_CP_CRISIS_RECOVERY_PUBLISH_FV_DATA     *CrisisRecoveryPublishFvData;
  EFI_FIRMWARE_VOLUME_EXT_HEADER                 *FwVolExtHeader;

  Status = H2OCpLookup (
             Handle,
             (VOID **)&CrisisRecoveryPublishFvData,
             &gH2OPeiCpCrisisRecoveryPublishFvGuid
             );
  if (EFI_ERROR (Status)) {
//[-start-190724-IB17700055-modify]//
    DEBUG_CP ((DEBUG_ERROR, "Checkpoint Data Not Found: %x (%r)\n", Handle, Status));
    DEBUG_CP ((DEBUG_ERROR, "   %a\n", __FUNCTION__));
//[-end-190724-IB17700055-modify]//
    return;
  }

  FwVolExtHeader = (EFI_FIRMWARE_VOLUME_EXT_HEADER *)((UINT8 *)CrisisRecoveryPublishFvData->FvHeader + CrisisRecoveryPublishFvData->FvHeader->ExtHeaderOffset);
  if (CompareGuid (&FwVolExtHeader->FvName, &gFspSiliconFvGuid)) {
    DEBUG ((DEBUG_INFO, "Do not report FSP-S FV when API mode.\n"));
    CrisisRecoveryPublishFvData->Status = H2O_CP_TASK_SKIP;
  }
}
//[-end-190613-IB16990064-add]//

//[-start-190819-IB17700068-add]//
/**
  Internal function to build FV HOB and verify the FV is whether valid according to information
  in FDM.

  @param[in] RegionType      EFI_GUID that specifies the type of region that N-th entry is.
  @param[in] Instance        Unsigned integer that specifies entry instance of FDM

**/
STATIC
VOID
BuildVerifiedFvHob (
  CONST IN  EFI_GUID  *RegionType,
  CONST IN  UINT8     Instance
  )
{
 EFI_FIRMWARE_VOLUME_HEADER        *FvHeader;
 UINT64                            FvAddr;
 UINT64                            FvSize;
 UINTN                             FvInfoInstance;
 EFI_STATUS                        Status;
 BOOLEAN                           IsFvInoPpiHasInstalled;
 EFI_PEI_FIRMWARE_VOLUME_INFO_PPI  *FvInfoPpi;


//   if (FeaturePcdGet (PcdH2OFdmChainOfTrustSupported)) {
//     FdmVerifyNAt (RegionType, Instance);
//   }

  FvAddr = FdmGetNAtAddr (RegionType, Instance);
  if (FvAddr == 0) {
    return;
  }
  FvSize = FdmGetNAtSize (RegionType, Instance);

  BuildFvHob (
    (EFI_PHYSICAL_ADDRESS) FvAddr,
    FvSize
    );

  FvInfoInstance = 0;
  IsFvInoPpiHasInstalled = FALSE;

  while (TRUE){
    Status = PeiServicesLocatePpi (&gEfiPeiFirmwareVolumeInfoPpiGuid, FvInfoInstance, NULL, &FvInfoPpi);
    if (EFI_ERROR(Status)){
      break;
    }

    if ((FvAddr == (UINT64)(UINTN) FvInfoPpi->FvInfo) && (FvInfoPpi->FvInfoSize == (UINT32) FvSize)){
      IsFvInoPpiHasInstalled = TRUE;
      break;
    }
    FvInfoInstance++;
  }

  if (!IsFvInoPpiHasInstalled){
    FvHeader = (EFI_FIRMWARE_VOLUME_HEADER*)(UINTN) FvAddr;
    PeiServicesInstallFvInfoPpi (
      &FvHeader->FileSystemGuid,
      (VOID*) (UINTN) FvAddr,
      (UINT32) FvSize,
      NULL,
      NULL
      );
  }
}
//[-end-190819-IB17700068-add]//

VOID
BootModeBuildFv(
  EFI_BOOT_MODE                 BootMode
  )
{
  EFI_STATUS                    Status;
  UINT8                         Index;
//[-start-190709-IB16990085-add]//
#if FixedPcdGetBool (PcdFspModeSelection) == 1
  H2O_CP_HANDLE                 CpHandle;
#endif
//[-end-190709-IB16990085-add]//
  //
  // OemServices
  //
//[-start-190724-IB17700055-modify]//
  DEBUG_OEM_SVC ((DEBUG_INFO, "OemKernelServices Call: OemSvcBootModeCreateFv \n"));
  Status = OemSvcBootModeCreateFv (BootMode);
  DEBUG_OEM_SVC ((DEBUG_INFO, "OemKernelServices OemSvcBootModeCreateFv Status: %r\n", Status));
//[-end-190724-IB17700055-modify]//
  if (Status == EFI_MEDIA_CHANGED) {
    return;
  }

//[-start-200709-IB17040128-modify]//
  if (BootMode != BOOT_ON_S3_RESUME) {
    if (BootMode != BOOT_IN_RECOVERY_MODE) {
      for (Index = 1; FdmGetNAtSize (&gH2OFlashMapRegionFvGuid, Index) != 0; Index++){
//[-start-190819-IB17700068-modify]//
        BuildVerifiedFvHob (&gH2OFlashMapRegionFvGuid, Index);
//[-end-190819-IB17700068-modify]//
      }
    }
    BuildFvHob (
      FdmGetNAtAddr (&gH2OFlashMapRegionVarGuid, 1),
      FdmGetNAtSize (&gH2OFlashMapRegionVarGuid, 1)
       + FdmGetNAtSize(&gH2OFlashMapRegionFtwStateGuid, 1)
       + FdmGetNAtSize(&gH2OFlashMapRegionFtwBackupGuid, 1)
       + FdmGetSizeById(&gH2OFlashMapRegionVarDefaultGuid, &gH2OFlashMapRegionFactoryCopyGuid, 1)
    );
  }
//[-end-200709-IB17040128-modify]//
//[-start-190613-IB16990064-add]//
  //
  // Register callback function for gH2OPeiCpCrisisRecoveryPublishFvGuid check point.
  //
#if FixedPcdGetBool (PcdFspModeSelection) == 1
  if (FeaturePcdGet (PcdH2OPeiCpCrisisRecoveryPublishFvSupported)) {
    if (BootMode == BOOT_IN_RECOVERY_MODE) {
      Status = H2OCpRegisterHandler (
                 &gH2OPeiCpCrisisRecoveryPublishFvGuid,
                 PublishFvInRecoveryCallback,
                 H2O_CP_MEDIUM,
                 &CpHandle
                 );
//[-start-190724-IB17700055-modify]//
      if (EFI_ERROR (Status)) {
        DEBUG_CP ((DEBUG_ERROR, "Checkpoint Register Fail: %g (%r)\n", &gH2OPeiCpCrisisRecoveryPublishFvGuid, Status));
        return;
      }
      DEBUG_CP ((DEBUG_INFO, "Checkpoint Registered: %g (%r)\n", &gH2OPeiCpCrisisRecoveryPublishFvGuid, Status));
//[-end-190724-IB17700055-modify]//
    }
  }
#endif
//[-end-190613-IB16990064-add]//
}


