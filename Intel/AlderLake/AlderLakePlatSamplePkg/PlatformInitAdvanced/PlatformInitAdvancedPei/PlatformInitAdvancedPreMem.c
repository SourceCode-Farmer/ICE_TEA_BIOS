/** @file

;******************************************************************************
;* Copyright (c) 2018 - 2021, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/
/** @file
  Source code file for Platform Init Advanced Pre-Memory PEI module

@copyright
  INTEL CONFIDENTIAL
  Copyright 2014 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains a 'Sample Driver' and is licensed as such under the terms
  of your license agreement with Intel or your vendor. This file may be modified
  by the user, subject to the additional terms of the license agreement.

@par Specification Reference:

**/

#include <PiPei.h>
#include <Ppi/Reset.h>
#include <Ppi/ReadOnlyVariable2.h>
//[-start-200420-IB17800056-remove]//
//#include <Guid/GlobalVariable.h>
//[-end-200420-IB17800056-remove]//
#include <Library/BaseMemoryLib.h>
#include <Library/DebugLib.h>
#include <Library/HobLib.h>
#include <Library/IoLib.h>
#include <Library/PcdLib.h>
#include <Library/PciSegmentLib.h>
#include <Library/PostCodeLib.h>
#include <Library/TimerLib.h>
#include <Library/EcTcssLib.h>
#include <Ppi/Wdt.h>
#include <Guid/TcoWdtHob.h>
#include <Library/PmcLib.h>
#include <Library/TcoLib.h>
#include <Library/PchCycleDecodingLib.h>
//[-start-200420-IB17800056-remove]//
//#include <PlatformNvRamHookLib.h>
//#include <Library/BiosIdLib.h>
//[-end-200420-IB17800056-remove]//
#include <Library/EcMiscLib.h>
#include <Library/PeiWdtAppLib.h>
#if FixedPcdGetBool (PcdDTbtEnable) == 1
#include <Library/PeiDTbtPolicyLib.h>
#endif
#include <Register/SaRegsHostBridge.h>
#include <Register/PmcRegs.h>
#include <Register/PchRegs.h>
#include <Register/PchRegsSmbus.h>
#include <PchBdfAssignment.h>
#include <SetupVariable.h>
//[-start-200420-IB17800056-remove]//
//#include <FastBootFunctionEnabledHob.h>
//#include <FastBootExceptionInfo.h>
//#include <FastBootFunctionEnabled.h>
//#include <Setup.h>
//[-end-200420-IB17800056-remove]//
#include <PlatformPostCode.h>
//[-start-200420-IB17800056-remove]//
//#include <FirwmareConfigurations.h>
//[-end-200420-IB17800056-remove]//
#include <Library/GpioLib.h>
#include <Library/PeiPlatformLib.h>
#include <PlatformBoardId.h>
#include <Library/PeiMeLib.h>
#include <Ppi/Smbus2.h>
#include <Library/SmbusLib.h>
#include <Library/SpiAccessLib.h>
#include <Library/MemoryAllocationLib.h>
#include <Ppi/HeciPpi.h>
#include <PcieRegs.h>
#include <Register/HeciRegs.h>
#include <Register/PttPtpRegs.h>
#include <MeDisabledEventHob.h>
#include <Library/BootGuardLibVer1.h>
#include <CpuRegs.h>
//[-start-200420-IB17800056-remove]//
#include <PostCode.h>
#include <Guid/H2OPeiStorage.h>
//[-start-200831-IB17800088-add]//
#include <Library/StallPpiLib.h>
//[-end-200831-IB17800088-add]//
#include <SaDataHob.h>
#include <ChipsetSetupConfig.h>
#include <Library/H2OCpLib.h>
//[-start-190611-IB16990039-add]//
#include <Guid/H2OCp.h>
//[-end-190611-IB16990039-add]//
//[-start-190722-IB17700055-add]//
#include <Library/H2OLib.h>
//[-end-190722-IB17700055-add]//
//[-start-200226-IB0531142-add]//  
#include <Library/BaseCmosAccessLib.h>
//[-end-200226-IB0531142-add]//
//[-end-200420-IB17800056-remove]//
//[-start-210505-IB16810152-add]//
#include <Library/TopSwapLib.h>
#include <Library/SeamlessRecoveryLib.h>
//[-end-210505-IB16810152-add]//

///
/// Reset Generator I/O Port
///
#define RESET_GENERATOR_PORT           0xCF9

///
/// VCCIN_AUX Secondary address
///
#define VCCIN_AUX_SECONDARY_ADDRESS    0x26

///
/// Richtek VR from I2C
///
#define RICHTEK_VR_SECONDARY_ADDRESS   0x40
#define RICHTEK_VFIX_LSB_CORE_MASK     0x00FF
#define RICHTEK_VFIX_MSB_CORE_MASK     0x0100

EFI_STATUS
EFIAPI
PlatformInitPreMem (
  IN CONST EFI_PEI_SERVICES      **PeiServices,
  IN EFI_PEI_NOTIFY_DESCRIPTOR   *NotifyDescriptor,
  IN VOID                        *Ppi
  );

EFI_STATUS
EFIAPI
MemoryDiscoveredPpiNotifyCallback (
  IN CONST EFI_PEI_SERVICES      **PeiServices,
  IN EFI_PEI_NOTIFY_DESCRIPTOR   *NotifyDescriptor,
  IN VOID                        *Ppi
  );

EFI_STATUS
EFIAPI
SiPreMemPolicyPpiNotifyCallback (
  IN CONST EFI_PEI_SERVICES     **PeiServices,
  IN EFI_PEI_NOTIFY_DESCRIPTOR  *NotifyDescriptor,
  IN VOID                       *Ppi
  );

EFI_STATUS
EFIAPI
WdtAppPpiNotifyCallback (
  IN CONST EFI_PEI_SERVICES     **PeiServices,
  IN EFI_PEI_NOTIFY_DESCRIPTOR  *NotifyDescriptor,
  IN VOID                       *Ppi
  );

EFI_STATUS
EFIAPI
PlatformVoltageInit (
  IN CONST EFI_PEI_SERVICES     **PeiServices,
  IN EFI_PEI_NOTIFY_DESCRIPTOR  *NotifyDescriptor,
  IN VOID                       *Ppi
  );

EFI_STATUS
EFIAPI
PchReset (
  IN CONST EFI_PEI_SERVICES    **PeiServices
  );
//[-start-200420-IB17800056-modify]//
EFI_STATUS
ChipsetPlatformInitPreMem (
  VOID
  );
//[-end-200420-IB17800056-modify]//

static EFI_PEI_NOTIFY_DESCRIPTOR mPreMemNotifyList = {
  (EFI_PEI_PPI_DESCRIPTOR_NOTIFY_CALLBACK | EFI_PEI_PPI_DESCRIPTOR_TERMINATE_LIST),
  &gEfiPeiReadOnlyVariable2PpiGuid,
  (EFI_PEIM_NOTIFY_ENTRY_POINT) PlatformInitPreMem
};

static EFI_PEI_NOTIFY_DESCRIPTOR mMemDiscoveredNotifyList = {
  (EFI_PEI_PPI_DESCRIPTOR_NOTIFY_CALLBACK | EFI_PEI_PPI_DESCRIPTOR_TERMINATE_LIST),
  &gEfiPeiMemoryDiscoveredPpiGuid,
  (EFI_PEIM_NOTIFY_ENTRY_POINT) MemoryDiscoveredPpiNotifyCallback
};

// @todo: FvCallBack.c needs to align with MinPlatform FV layout.
//static EFI_PEI_NOTIFY_DESCRIPTOR mGetFvNotifyList = {
//  (EFI_PEI_PPI_DESCRIPTOR_NOTIFY_DISPATCH | EFI_PEI_PPI_DESCRIPTOR_TERMINATE_LIST),
//  &gEfiPeiVirtualBlockIoPpiGuid,
//  (EFI_PEIM_NOTIFY_ENTRY_POINT) GetFvNotifyCallback
//};

static EFI_PEI_NOTIFY_DESCRIPTOR mSiPreMemPolicyNotifyList = {
  (EFI_PEI_PPI_DESCRIPTOR_NOTIFY_CALLBACK | EFI_PEI_PPI_DESCRIPTOR_TERMINATE_LIST),
  &gSiPreMemPolicyReadyPpiGuid,
  (EFI_PEIM_NOTIFY_ENTRY_POINT) SiPreMemPolicyPpiNotifyCallback
};

static EFI_PEI_NOTIFY_DESCRIPTOR mWdtPpiNotifyList = {
  (EFI_PEI_PPI_DESCRIPTOR_NOTIFY_CALLBACK | EFI_PEI_PPI_DESCRIPTOR_TERMINATE_LIST),
  &gWdtPpiGuid,
  (EFI_PEIM_NOTIFY_ENTRY_POINT) WdtAppPpiNotifyCallback
};

static EFI_PEI_NOTIFY_DESCRIPTOR mPlatformVoltageInitOnMemoryNotifyList = {
  (EFI_PEI_PPI_DESCRIPTOR_NOTIFY_CALLBACK | EFI_PEI_PPI_DESCRIPTOR_TERMINATE_LIST),
  &gEfiPeiMasterBootModePpiGuid,
  (EFI_PEIM_NOTIFY_ENTRY_POINT) PlatformVoltageInit
};

static EFI_PEI_RESET_PPI mResetPpi = {
  PchReset
};

static EFI_PEI_PPI_DESCRIPTOR mPreMemPpiList[] = {
  {
    (EFI_PEI_PPI_DESCRIPTOR_PPI| EFI_PEI_PPI_DESCRIPTOR_TERMINATE_LIST),
    &gEfiPeiResetPpiGuid,
    &mResetPpi
  }
};


/**
  Callback function to install and update CSME Measurements HOB before DID in case of CSME in Disabled mode.

  @param[in]  PeiServices       General purpose services available to every PEIM.
  @param[in]  NotifyDescriptor  The typedef structure of the notification
                                descriptor. Not used in this function.
  @param[in]  Ppi               The typedef structure of the PPI descriptor.
                                Not used in this function.

  @retval EFI_SUCCESS           The function completed successfully

**/
EFI_STATUS
EFIAPI
PeiExtendMeMeasuredBootNotifyCallback (
  IN EFI_PEI_SERVICES           **PeiServices,
  IN EFI_PEI_NOTIFY_DESCRIPTOR  *NotifyDescriptor,
  IN VOID                       *Ppi
  )
{
  EFI_STATUS               Status;
  UINT32                   RemainingBufSize;
  UINT32                   ReceivedLogSize;
  UINT8                    LogEntryIndex;
  UINT8                    RemainingEntries;
  UINT8                    *TempEvent;
  UINT8                    Events [MAX_EVENTLOG_SIZE];
  UINT32                   EventLogSize;
  UINT64                   HeciBaseAddress;
  UINT32                   *HerValue;
  UINT8                    HerMaxRegCount;
  UINTN                    HeciMemBar;
  UINT32                   HerOffset;
  UINT8                    Index;
  HECI_FWS_REGISTER        MeFwsts1;
  HECI_GS_SHDW_REGISTER    MeFwsts2;
  ME_DISABLED_EVENT_HOB    *MeDisabledHob;

  Status = EFI_SUCCESS;
  // Get HECI Device MMIO address
  HeciBaseAddress = PCI_SEGMENT_LIB_ADDRESS (ME_SEGMENT, ME_BUS, ME_DEVICE_NUMBER, HECI_FUNCTION_NUMBER, 0);
  if (PciSegmentRead16 (HeciBaseAddress + PCI_DEVICE_ID_OFFSET) == 0xFFFF) {
    DEBUG ((DEBUG_WARN, "HECI device is disabled\n"));
    return EFI_UNSUPPORTED;
  }

  // Read ME status and check for CSME operation mode
  MeFwsts1.ul = PciSegmentRead32 (HeciBaseAddress + R_ME_HFS);
  MeFwsts2.ul = PciSegmentRead32 (HeciBaseAddress + R_ME_HFS_2);

  // Check whether CSME is in Disabled mode
  if ((MeFwsts1.r.MeOperationMode != ME_OPERATION_MODE_NORMAL) || (MeFwsts2.r.CseToDisabled == 1)) {
    EventLogSize = 0;
    RemainingBufSize = MAX_EVENTLOG_SIZE;
    ReceivedLogSize = 0;
    LogEntryIndex = 0;
    TempEvent = Events;

    // Create MeDisabledHob
    Status = PeiServicesCreateHob (
               EFI_HOB_TYPE_GUID_EXTENSION,
               sizeof (ME_DISABLED_EVENT_HOB),
               (VOID **) &MeDisabledHob
               );
    if (EFI_ERROR (Status)) {
      DEBUG ((DEBUG_ERROR, "MeExtMeasurement: Failed to create MeDisabledHob\n"));
      ASSERT_EFI_ERROR (Status);
      return Status;
    }
    DEBUG ((DEBUG_INFO, "MeExtMeasurement: MeDisabledHob Created\n"));

    MeDisabledHob->EfiHobGuidType.Name = gMeDisabledEventHobGuid;

    // Read HER Status register value
    MeDisabledHob->Hers = PciSegmentRead32 (HeciBaseAddress + R_ME_HERS);

    HeciMemBar = PciSegmentRead32 (HeciBaseAddress + PCI_BASE_ADDRESSREG_OFFSET) & 0xFFFFFFF0;
    if ((PciSegmentRead32 (HeciBaseAddress + PCI_BASE_ADDRESSREG_OFFSET) & B_PCI_BAR_MEMORY_TYPE_MASK) == B_PCI_BAR_MEMORY_TYPE_64) {
      HeciMemBar += (UINT64) PciSegmentRead32 (HeciBaseAddress + (PCI_BASE_ADDRESSREG_OFFSET + 4)) << 32;
    }

    if (HeciMemBar == 0) {
      DEBUG ((DEBUG_WARN, "MMIO Bar for HECI device isn't programmed\n"));
      return EFI_UNSUPPORTED;
    }

    // Enable HECI MSE
    PciSegmentOr8 (HeciBaseAddress + PCI_COMMAND_OFFSET, EFI_PCI_COMMAND_MEMORY_SPACE);

    MeDisabledHob->Enh_Hers= MmioRead32 (HeciMemBar + R_HERS);

    ZeroMem (&MeDisabledHob->Her, sizeof (MeDisabledHob->Her));
    HerMaxRegCount = HER_REG_COUNT_SHA384;
    HerOffset = (UINT32) R_ME_HER1;
    HerOffset += R_HER_BASE;
    HerValue = MeDisabledHob->Her;
    // Read HER data from MMIO space
    for (Index = 0; Index < HerMaxRegCount; Index++) {
      *HerValue = MmioRead32 (HeciMemBar + (HerOffset + (Index * sizeof (UINT32))));
      HerValue++;
    }

    MeDisabledHob->LogUnavailable = FALSE;
    TempEvent = MeDisabledHob->Events;

    do {
      RemainingEntries = 0;
      Status = PeiHeciGetErLog (
                 TempEvent,
                 RemainingBufSize,
                 &ReceivedLogSize,
                 &LogEntryIndex,
                 &RemainingEntries
                 );
      if (EFI_ERROR (Status)) {
          DEBUG((DEBUG_INFO, "Failed to get ER Log data\n"));
          MeDisabledHob->LogUnavailable = TRUE;
      } else {
        EventLogSize += ReceivedLogSize;
        if (RemainingEntries) {
          if (EventLogSize >= MAX_EVENTLOG_SIZE) {
            DEBUG((DEBUG_INFO, "Event Log is exceeding the Maximum allocated buffer\n"));
            return EFI_BUFFER_TOO_SMALL;
          }
          TempEvent += ReceivedLogSize;
          RemainingBufSize = MAX_EVENTLOG_SIZE - EventLogSize;
        }
      }
    } while (RemainingEntries != 0);

    MeDisabledHob->EventLogSize = EventLogSize;
  }

  return Status;
}

static EFI_PEI_NOTIFY_DESCRIPTOR  mPeiExtendMeMeasuredBootNotifyList = {
  (EFI_PEI_PPI_DESCRIPTOR_NOTIFY_CALLBACK | EFI_PEI_PPI_DESCRIPTOR_TERMINATE_LIST),
  &gMeBeforeDidSentPpiGuid,
  (EFI_PEIM_NOTIFY_ENTRY_POINT) PeiExtendMeMeasuredBootNotifyCallback
};



//[-start-200420-IB17800056-remove]//
#if 0
EFI_STATUS
EFIAPI
PeiGuidForward (
  VOID
  );
#endif
//[-end-200420-IB17800056-remove]//

/**
  Configures GPIO

  @param[in]  GpioTable       Point to Platform Gpio table
  @param[in]  GpioTableCount  Number of Gpio table entries

**/
VOID
ConfigureGpio (
  IN GPIO_INIT_CONFIG                 *GpioDefinition,
  IN UINT16                           GpioTableCount
  )
{
  DEBUG ((DEBUG_INFO, "ConfigureGpio() Start\n"));

  GpioConfigurePads (GpioTableCount, GpioDefinition);

  DEBUG ((DEBUG_INFO, "ConfigureGpio() End\n"));
}

/**
  Configure TypeC Retimer FP pin for USB compliance mode function.
**/
VOID
USBCRetimerComplianceModePreMem (
  VOID
  )
{
  GPIO_CONFIG                     GpioConfig;
  GPIO_PAD                        GpioPad;
  EFI_STATUS                      Status;
  SETUP_DATA                      SetupData;
  UINTN                           VarSize;
  EFI_PEI_READ_ONLY_VARIABLE2_PPI *VariableServices;
  UINT8                           EcDataBuffer;

  DEBUG ((DEBUG_INFO, "USBCRetimerComplianceModePreMem: Start\n"));
  EcDataBuffer = 0;

  //
  // Locate Setup variables
  //
  Status = PeiServicesLocatePpi (
             &gEfiPeiReadOnlyVariable2PpiGuid,
             0,
             NULL,
             (VOID **) &VariableServices
             );
  if (EFI_ERROR(Status)) {
    DEBUG((DEBUG_ERROR, "PeiServicesLocatePpi failed\n"));
    return;
  }

  VarSize = sizeof (SETUP_DATA);
  Status = VariableServices->GetVariable (
                               VariableServices,
                               L"Setup",
                               &gSetupVariableGuid,
                               NULL,
                               &VarSize,
                               &SetupData
                               );
  if (EFI_ERROR(Status)) {
    DEBUG((DEBUG_ERROR, "GetVariable (SetupData) failed\n"));
    return;
  }
  GpioPad = PcdGet32 (PcdBoardRetimerForcePwrGpio);
  DEBUG((DEBUG_INFO, "Retimer Force Power Gpio is : 0x%08x\n", GpioPad));

  if (GpioPad != 0) {
    GpioGetPadConfig (GpioPad, &GpioConfig);
    GpioConfig.PadMode = GpioPadModeGpio;
    GpioConfig.HostSoftPadOwn = GpioHostOwnAcpi;
    GpioConfig.Direction = GpioDirOut;
    GpioConfig.InterruptConfig = GpioIntDis;
    GpioConfig.PowerConfig = GpioPlatformReset;
    GpioConfig.ElectricalConfig = GpioTermNone;
    GpioConfig.LockConfig = GpioOutputStateUnlock;

    if (SetupData.TypecRetimerTxComplianceModeEn != 0) {
      if (SetupData.TypecRetimerPD0 == 1) { //PD 0 Retimer enable
        EcDataBuffer |= BIT0;
      } else {
        EcDataBuffer &= ~BIT0;
      }

      if (SetupData.TypecRetimerPD1 == 1) { //PD 1 Retimer enable
        EcDataBuffer |= BIT1;
      } else {
        EcDataBuffer &= ~BIT1;
      }

      if (SetupData.TypecRetimerPD2 == 1) { //PD 2 Retimer enable
        EcDataBuffer |= BIT2;
      } else {
        EcDataBuffer &= ~BIT2;
      }

      if (SetupData.TypecRetimerPD3 == 1) { //PD 3 Retimer enable
        EcDataBuffer |= BIT3;
      } else {
        EcDataBuffer &= ~BIT3;
      }

      GpioConfig.OutputState = GpioOutHigh;
      GpioSetPadConfig (GpioPad, &GpioConfig);
      UsbcRetimerCompliancePDMessage (&EcDataBuffer);
      DEBUG((DEBUG_INFO, "Compliance Mode: Enable\nAssert Retimer FP Pin\n"));
    } else {
      EcDataBuffer = 0;
      DEBUG((DEBUG_INFO, "TCompliance Mode: Disable\nNo Action\n"));
    }

  }

  DEBUG ((DEBUG_INFO, "USBCRetimerComplianceModePreMem: End\n"));
}

/**
  Callback once there is main memory

  @param[in]  PeiServices       General purpose services available to every PEIM.
  @param[in]  NotifyDescriptor  Notify that this module published.
  @param[in]  Ppi               PPI that was installed.

  @retval     EFI_SUCCESS       The function completed successfully.
**/
EFI_STATUS
EFIAPI
MemoryDiscoveredPpiNotifyCallback (
  IN CONST EFI_PEI_SERVICES     **PeiServices,
  IN EFI_PEI_NOTIFY_DESCRIPTOR  *NotifyDescriptor,
  IN VOID                       *Ppi
  )
{
  EFI_STATUS                        Status;
  DEBUG_CONFIG_DATA_HOB             *DebugConfigDataHob;
  EFI_PEI_HOB_POINTERS              Hob;
  DEBUG_CONFIG_DATA                 DebugConfigData;
  UINTN                             DebugConfigDataSize;
  EFI_PEI_READ_ONLY_VARIABLE2_PPI   *VarPpi;

  Status = EFI_SUCCESS;

  Hob.Guid = GetFirstGuidHob (&gDebugConfigHobGuid);
  if (Hob.Guid != NULL) {
    Status = PeiServicesLocatePpi (
               &gEfiPeiReadOnlyVariable2PpiGuid,
               0,
               NULL,
               (VOID **) &VarPpi
               );
    ASSERT_EFI_ERROR (Status);
    DebugConfigDataSize = sizeof (DEBUG_CONFIG_DATA);
    Status = VarPpi->GetVariable (
                       VarPpi,
                       L"DebugConfigData",
                       &gDebugConfigVariableGuid,
                       NULL,
                       &DebugConfigDataSize,
                       &DebugConfigData
                       );
    ASSERT_EFI_ERROR (Status);
    DebugConfigDataHob = (DEBUG_CONFIG_DATA_HOB *) GET_GUID_HOB_DATA (Hob.Guid);
    DebugConfigDataHob->RamDebugInterface = (DebugConfigData.RamDebugInterface == 1 && FeaturePcdGet (PcdRamDebugEnable)) ? 1 : 0;
  }
//[-start-210809-IB19010027-modify]//  
//[-start-200420-IB17800056-modify]//
#if FixedPcdGetBool(PcdExtendedBiosRegionSupport) == 1 
  //
  // Report resource HOB for flash FV
  //
//[-start-210924-IB19010029-modify]//  
  BuildResourceDescriptorHob (
    EFI_RESOURCE_MEMORY_MAPPED_IO,
    (EFI_RESOURCE_ATTRIBUTE_PRESENT    |
    EFI_RESOURCE_ATTRIBUTE_INITIALIZED |
    EFI_RESOURCE_ATTRIBUTE_UNCACHEABLE),
    (UINTN) (FixedPcdGet32 (PcdFlashAreaBaseAddress) + FixedPcdGet32 (PcdFlashExtendRegionOffset) + FixedPcdGet32 (PcdFlashExtendRegionSizeInUse)), //  0xFF000000, Only 16MB BIOS
    (UINTN) (FixedPcdGet32 (PcdFlashAreaSize) - FixedPcdGet32 (PcdFlashExtendRegionOffset) - FixedPcdGet32 (PcdFlashExtendRegionSizeInUse))         //  0x1000000
    );

  BuildResourceDescriptorHob (
    EFI_RESOURCE_MEMORY_MAPPED_IO,
    (EFI_RESOURCE_ATTRIBUTE_PRESENT    |
    EFI_RESOURCE_ATTRIBUTE_INITIALIZED |
    EFI_RESOURCE_ATTRIBUTE_UNCACHEABLE),
    (UINTN) (FixedPcdGet32 (PcdFlashFvExtendedAdvancedBase)),   //  0xF9F00000
    (UINTN) (FixedPcdGet32 (PcdFlashFvExtendedAdvancedSize))    //  0x00080000
    );

  BuildResourceDescriptorHob (
    EFI_RESOURCE_MEMORY_MAPPED_IO,
    (EFI_RESOURCE_ATTRIBUTE_PRESENT    |
    EFI_RESOURCE_ATTRIBUTE_INITIALIZED |
    EFI_RESOURCE_ATTRIBUTE_UNCACHEABLE),
    (UINTN) (FixedPcdGet32 (PcdFlashFvExtendedPostMemoryBase)), //  0xF9F80000
    (UINTN) (FixedPcdGet32 (PcdFlashFvExtendedPostMemorySize))  //  0x00080000
    );

  DEBUG ((DEBUG_INFO, "BIOS Base:0x%08x, Size:0x%08x\n", (FixedPcdGet32 (PcdFlashAreaBaseAddress) + FixedPcdGet32 (PcdFlashExtendRegionOffset) + FixedPcdGet32 (PcdFlashExtendRegionSizeInUse)), (FixedPcdGet32 (PcdFlashAreaSize) - FixedPcdGet32 (PcdFlashExtendRegionOffset) - FixedPcdGet32 (PcdFlashExtendRegionSizeInUse)) ));   
  DEBUG ((DEBUG_INFO, "ExtendedAdvancedBase:0x%08x, ExtendedAdvancedSize:0x%08x\n", (FixedPcdGet32 (PcdFlashFvExtendedAdvancedBase)), (FixedPcdGet32 (PcdFlashFvExtendedAdvancedSize))));
  DEBUG ((DEBUG_INFO, "ExtendedPostMemoryBase:0x%08x, ExtendedPostMemorySize:0x%08x\n", (FixedPcdGet32 (PcdFlashFvExtendedPostMemoryBase)), (FixedPcdGet32 (PcdFlashFvExtendedPostMemorySize))));  
//[-end-210924-IB19010029-modify]// 
#else
  BuildResourceDescriptorHob (
    EFI_RESOURCE_MEMORY_MAPPED_IO,
    (EFI_RESOURCE_ATTRIBUTE_PRESENT    |
    EFI_RESOURCE_ATTRIBUTE_INITIALIZED |
    EFI_RESOURCE_ATTRIBUTE_UNCACHEABLE),
    (UINTN) FixedPcdGet32 (PcdFlashAreaBaseAddress),
    (UINTN) FixedPcdGet32 (PcdFlashAreaSize)
    );
#endif  
  BuildMemoryAllocationHob (
    (UINTN) FixedPcdGet32 (PcdFlashAreaBaseAddress),
    (UINTN) FixedPcdGet32 (PcdFlashAreaSize),
    EfiMemoryMappedIO
    );
//[-end-200420-IB17800056-modify]//
//[-end-210809-IB19010027-modify]//  

  return Status;
}
//[-start-200420-IB17800056-modify]//
#if 0
/**
  Check to see the overclocking is enabled or not

  @retval  OverclockingEnabledStatus  TRUE means overclocking is enabled
                                      FALSE means overclocking is disabled
**/
BOOLEAN
EFIAPI
IsOverclockingEnabled (
  VOID
  )
{
  EFI_STATUS                      Status;
  EFI_PEI_READ_ONLY_VARIABLE2_PPI *PeiReadOnlyVarPpi;
  UINTN                           VarSize;
  CPU_SETUP                       CpuSetup;
  BOOLEAN                         OverclockingEnabledStatus;

  OverclockingEnabledStatus = FALSE;

  Status = PeiServicesLocatePpi (
             &gEfiPeiReadOnlyVariable2PpiGuid,
             0,
             NULL,
             (VOID **) &PeiReadOnlyVarPpi
             );
  if (Status == EFI_SUCCESS) {
    VarSize = sizeof (CPU_SETUP);
    Status = PeiReadOnlyVarPpi->GetVariable (
                                  PeiReadOnlyVarPpi,
                                  L"CpuSetup",
                                  &gCpuSetupVariableGuid,
                                  NULL,
                                  &VarSize,
                                  &CpuSetup
                                  );
    if (Status == EFI_SUCCESS) {
      if (CpuSetup.OverclockingSupport != 0) {
        OverclockingEnabledStatus = TRUE;
      }
    }
  }

  return OverclockingEnabledStatus;
}

/**
  Check fast boot is enabled or not

  @retval  FastBootEnabledStatus  TRUE means fast boot is enabled
                                  FALSE means fast boot is disabled
**/
BOOLEAN
EFIAPI
IsFastBootEnabled (
  VOID
  )
{
  EFI_STATUS                      Status;
  EFI_PEI_READ_ONLY_VARIABLE2_PPI *PeiReadOnlyVarPpi;
  UINTN                           VarSize;
  SETUP_DATA                      SystemConfiguration;
  BOOLEAN                         FastBootEnabledStatus;

  FastBootEnabledStatus = FALSE;
  Status = PeiServicesLocatePpi (
             &gEfiPeiReadOnlyVariable2PpiGuid,
             0,
             NULL,
             (VOID **) &PeiReadOnlyVarPpi
             );
  if (Status == EFI_SUCCESS) {
    VarSize = sizeof (SETUP_DATA);
    Status = PeiReadOnlyVarPpi->GetVariable (
                                  PeiReadOnlyVarPpi,
                                  L"Setup",
                                  &gSetupVariableGuid,
                                  NULL,
                                  &VarSize,
                                  &SystemConfiguration
                                  );
    if (Status == EFI_SUCCESS) {
      if (SystemConfiguration.FastBoot != 0) {
        FastBootEnabledStatus = TRUE;
      }
    }
  }

  return FastBootEnabledStatus;
}
#endif
//[-end-200420-IB17800056-modify]//

//[-start-200420-IB17800056-modify]//
#if 0
/**
  Create Fast Boot HOB

  @param[in]  VariableServices  A pointer to this instance of the EFI_PEI_READ_ONLY_VARIABLE2_PPI.

  @retval     EFI_SUCCESS  The function completes successfully
  @retval     others
**/
EFI_STATUS
EFIAPI
CreateFastBootHob (
  IN EFI_PEI_READ_ONLY_VARIABLE2_PPI *VariableServices
  )
{
  EFI_STATUS                      Status;
  UINTN                           VarSize;
  FAST_BOOT_FUNCTION_ENABLED_HOB  *FastBootFunctionEnabledHob;
  FAST_BOOT_EXCEPTION_INFO_HOB    *FastBootExceptionInfoHob;

  if (IsFastBootEnabled ()) {
    ///
    /// If Fast Boot is enabled, create the FAST_BOOT_FUNCTION_ENABLED_HOB for other modules' reference.
    ///
    Status = PeiServicesCreateHob (
               EFI_HOB_TYPE_GUID_EXTENSION,
               sizeof (FAST_BOOT_FUNCTION_ENABLED_HOB),
               (VOID **) &FastBootFunctionEnabledHob
               );
    if (!EFI_ERROR (Status)) {
      FastBootFunctionEnabledHob->Header.Name = gFastBootFunctionEnabledHobGuid;
      FastBootFunctionEnabledHob->FastBootEnabled = TRUE;
    } else {
      return Status;
    }
    ///
    /// When RTC battery is drained (RTC power loss) or Secondary NvRam content is cleared (via jumper short), this bit will get set.
    /// This is the Fast Boot Exception Type 2.
    ///
    if (!PmcIsRtcBatteryGood ()) {
      Status = PeiServicesCreateHob (
                 EFI_HOB_TYPE_GUID_EXTENSION,
                 sizeof (FAST_BOOT_EXCEPTION_INFO_HOB),
                 (VOID **) &FastBootExceptionInfoHob
                 );
      if (!EFI_ERROR (Status)) {
        FastBootExceptionInfoHob->Header.Name = gFastBootExceptionInfoHobGuid;
        FastBootExceptionInfoHob->FbExceptionType = ExceptionType2;
        FastBootExceptionInfoHob->FbExceptionCategory = ContentLost;
      } else {
        return Status;
      }
    }
    ///
    /// Check the EFI Globally-Defined variable which is nonexistent right after updating BIOS, to determine if BIOS was just updated.
    ///
    Status = VariableServices->GetVariable (
                                 VariableServices,
                                 L"ConOut",
                                 &gEfiGlobalVariableGuid,
                                 NULL,
                                 &VarSize,
                                 NULL
                                 );
    if (Status == EFI_NOT_FOUND) {
      Status = PeiServicesCreateHob (
                 EFI_HOB_TYPE_GUID_EXTENSION,
                 sizeof (FAST_BOOT_EXCEPTION_INFO_HOB),
                 (VOID **) &FastBootExceptionInfoHob
                 );
      if (!EFI_ERROR (Status)) {
        FastBootExceptionInfoHob->Header.Name = gFastBootExceptionInfoHobGuid;
        FastBootExceptionInfoHob->FbExceptionType = ExceptionType2;
        FastBootExceptionInfoHob->FbExceptionCategory = FirmwareUpdate;
      } else {
        return Status;
      }
    }
    ///
    /// If overclocking is enabled, then BIOS shall switch back to Full Boot mode
    ///
    if (IsOverclockingEnabled ()) {
      Status = PeiServicesCreateHob (
                 EFI_HOB_TYPE_GUID_EXTENSION,
                 sizeof (FAST_BOOT_EXCEPTION_INFO_HOB),
                 (VOID **) &FastBootExceptionInfoHob
                 );
      if (!EFI_ERROR (Status)) {
        FastBootExceptionInfoHob->Header.Name = gFastBootExceptionInfoHobGuid;
        FastBootExceptionInfoHob->FbExceptionType = ExceptionType2;
        FastBootExceptionInfoHob->FbExceptionCategory = SpecialBoot;
      } else {
        return Status;
      }
    }
  }
  return EFI_SUCCESS;
}
#endif
//[-end-200420-IB17800056-modify]//

//@todo it should be performed in Si Pkg.
/**
  Provide hard reset PPI service.
  To generate full hard reset, write 0x0E to PCH RESET_GENERATOR_PORT (0xCF9).

  @param[in]  PeiServices       General purpose services available to every PEIM.

  @retval     Not return        System reset occured.
  @retval     EFI_DEVICE_ERROR  Device error, could not reset the system.
**/
EFI_STATUS
EFIAPI
PchReset (
  IN CONST EFI_PEI_SERVICES    **PeiServices
  )
{
  DEBUG ((DEBUG_INFO, "Perform Cold Reset\n"));
  IoWrite8 (RESET_GENERATOR_PORT, 0x0E);

  CpuDeadLoop ();

  ///
  /// System reset occured, should never reach at this line.
  ///
  ASSERT_EFI_ERROR (EFI_DEVICE_ERROR);

  return EFI_DEVICE_ERROR;
}

/**
  Early Platform PCH initialization
**/
VOID
EFIAPI
EarlyPlatformPchInit (
  VOID
  )
{
  UINT8        TcoRebootHappened;
  TCO_WDT_HOB  *TcoWdtHobPtr;
  EFI_STATUS   Status;

  if (TcoSecondToHappened ()) {
    TcoRebootHappened = 1;
    DEBUG ((DEBUG_INFO, "PlatformInitPreMem - TCO Second TO status bit is set. This might be a TCO reboot\n"));
  } else {
    TcoRebootHappened = 0;
  }

  ///
  /// Create HOB
  ///
  Status = PeiServicesCreateHob (EFI_HOB_TYPE_GUID_EXTENSION, sizeof (TCO_WDT_HOB), (VOID **) &TcoWdtHobPtr);
  if (!EFI_ERROR (Status)) {
    TcoWdtHobPtr->Header.Name  = gTcoWdtHobGuid;
    TcoWdtHobPtr->TcoRebootHappened = TcoRebootHappened;
  }

  TcoClearSecondToStatus ();
}

/**
  Set the state to go after G3

  @dot
    digraph G {
      subgraph cluster_c0 {
        node [shape = box];
          b1[label="EcForceResetAfterAcRemoval ()" fontsize=12 style=filled color=lightblue];
          b2[label="Force S5" fontsize=12 style=filled color=lightblue];
          b3[label="Force S0" fontsize=12 style=filled color=lightblue];

        node [shape = ellipse];
          e1[label="Start" fontsize=12 style=filled color=lightblue];
          e2[label="End" fontsize=12 style=filled color=lightblue];

        node [shape = diamond,style=filled,color=lightblue];
          d1[label="GetVariable\nFroceResetAfterAcRemoval" fontsize=12];
          d2[label="EcPresent" fontsize=12];
          d3[label="ForceResetAfterAcRemoval" fontsize=12];
          d4[label="GetVariable\nStateAfterG3" fontsize=12];
          d5[label="StateAfterG3" fontsize=12];

        label = "SetTheStateToGoAfterG3 Flow"; fontsize=15; fontcolor=black; color=lightblue;
        e1 -> d1
        d1 -> d2 [label="Success" fontsize=9]
        d1 -> d4 [label="Fail" fontsize=9]
        d2 -> b1 [label="Yes" fontsize=9]
        b1 -> d3
        d2 -> d3 [label="No" fontsize=9]
        d3 -> b3 [label="Yes" fontsize=9]
        d3 -> d4 [label="No" fontsize=9]
        d4 -> d5 [label="Success" fontsize=9]
        d4 -> b3 [label="Fail" fontsize=9]
        d5 -> b2 [label="Set" fontsize=9]
        d5 -> b3 [label="Clear" fontsize=9]
        b3 -> e2
      }
    }
  @enddot

  @param[in]  VariableServices  A pointer to this instance of the EFI_PEI_READ_ONLY_VARIABLE2_PPI.
**/
VOID
EFIAPI
SetTheStateToGoAfterG3 (
  IN EFI_PEI_READ_ONLY_VARIABLE2_PPI *VariableServices
  )
{
  EFI_STATUS            Status;
  UINTN                 VariableSize;
  UINT8                 ForceResetAfterAcRemovalVar;
  UINT8                 StateAfterG3;
  PCH_SETUP             PchSetup;

  ForceResetAfterAcRemovalVar = 0;
  StateAfterG3 = 0;


  VariableSize = sizeof (PCH_SETUP);
  Status = VariableServices->GetVariable (
                               VariableServices,
                               L"PchSetup",
                               &gPchSetupVariableGuid,
                               NULL,
                               &VariableSize,
                               &PchSetup
                               );
  if (!EFI_ERROR(Status)) {
    StateAfterG3 = PchSetup.StateAfterG3;
  }

  if ((StateAfterG3 == 1) && (ForceResetAfterAcRemovalVar == 0)) {
    PmcSetPlatformStateAfterPowerFailure (1); // AfterG3 = S5
  } else {
    PmcSetPlatformStateAfterPowerFailure (0); // AfterG3 = S0
  }
}


// @todo: It should be moved Policy Init.
/**
  Initialize the GPIO IO selection, GPIO USE selection, and GPIO signal inversion registers

  @param[in] VariableServices  A pointer to this instance of the EFI_PEI_READ_ONLY_VARIABLE2_PPI.
**/
VOID
EFIAPI
PlatformPchInit (
  IN EFI_PEI_READ_ONLY_VARIABLE2_PPI *VariableServices
  )
{

  ///
  /// Enable the LPC I/O decoding for 0x6A0~0x6A7 as EC's extra I/O port, where 0x6A0 is the Data port
  /// and 0x6A4 is the Command/Status port.
  ///
  if (PcdGetBool (PcdEcPresent)) {
    PchLpcGenIoRangeSet (PcdGet16 (PcdEcExtraIoBase), 0x10); //PTLMOD+
  }
#if FixedPcdGetBool(PcdNct677FPresent) == 1
  else {
    /// Added Nuvoton HW monitor IO address.
    PchLpcGenIoRangeSet (PcdGet16 (PcdNct6776fHwMonBase), 0x10);
  }
#endif

}

/**
  Configure EC for specific devices

  @param[in] PchLan       - The PchLan of PCH_SETUP variable.
  @param[in] BootMode     - The current boot mode.
**/
VOID
EcInit (
  IN UINT8                PchLan,
  IN EFI_BOOT_MODE        BootMode
  )
{
  EFI_STATUS              Status;

  Status = EFI_SUCCESS;
  if (PchLan != PEI_DEVICE_DISABLED) {
    Status = EnableLanPower (TRUE);
  } else {
    Status = EnableLanPower (FALSE);
  }
  ASSERT_EFI_ERROR (Status);

  //
  // Clear 10sec PB Override mode in EC so that Power button is always available during POST.
  //
  ClearTenSecPwrButtonMode ();
}

/**
  Do platform specific programming. For example, EC init, Chipset programming

  @retval  EFI_SUCCESS       The function completed successfully.
**/
EFI_STATUS
PlatformSpecificInitPreMem (
  VOID
  )
{
  EFI_STATUS                      Status;
  EFI_BOOT_MODE                   BootMode;
  SA_SETUP                        SaSetup;
  PCH_SETUP                       PchSetup;
  SETUP_DATA                      SystemConfiguration;
  UINTN                           VarSize;
  EFI_PEI_READ_ONLY_VARIABLE2_PPI *VariableServices;

  Status = PeiServicesGetBootMode (&BootMode);
  ASSERT_EFI_ERROR (Status);

  //
  // Locate Setup variables
  //
  Status = PeiServicesLocatePpi (
             &gEfiPeiReadOnlyVariable2PpiGuid,
             0,
             NULL,
             (VOID **) &VariableServices
             );
  ASSERT_EFI_ERROR (Status);

  VarSize = sizeof (PCH_SETUP);
  Status = VariableServices->GetVariable (
                               VariableServices,
                               L"PchSetup",
                               &gPchSetupVariableGuid,
                               NULL,
                               &VarSize,
                               &PchSetup
                               );

  if (!EFI_ERROR(Status) && PcdGetBool (PcdEcPresent)) {
    EcInit (PchSetup.PchLan, BootMode);
  }

  VarSize = sizeof (SETUP_DATA);
  Status = VariableServices->GetVariable (
                               VariableServices,
                               L"Setup",
                               &gSetupVariableGuid,
                               NULL,
                               &VarSize,
                               &SystemConfiguration
                               );
  if (!EFI_ERROR(Status)) {
    //
    // Selecting charging method
    //
    if (PcdGetBool (PcdEcPresent)) {
      if (SystemConfiguration.EcChargingMethod == 1) {
        //
        // Fast charging
        //
        DfctFastChargingMode (TRUE);
      } else {
        //
        // Normal charging
        //
        DfctFastChargingMode (FALSE);
      }
    }

    VarSize = sizeof (SA_SETUP);
    Status = VariableServices->GetVariable (
                                 VariableServices,
                                 L"SaSetup",
                                 &gSaSetupVariableGuid,
                                 NULL,
                                 &VarSize,
                                 &SaSetup
                                 );
    if (!EFI_ERROR(Status)) {
      if (PcdGet8 (PcdPlatformType) == TypeUltUlx) {
        // Force Valid Sensor Settings depending on the Option selected in Sensor Hub Type Setup Option.
        if(SystemConfiguration.SensorHubType == 0) {
          SystemConfiguration.UsbSensorHub = 0;
        } else if (SystemConfiguration.SensorHubType == 1) {
          SaSetup.AlsEnable = 0;
          SystemConfiguration.UsbSensorHub = 0;
        } else {
          SaSetup.AlsEnable = 0;
          SystemConfiguration.UsbSensorHub = 1;
        }
      }
    }
  }
  return EFI_SUCCESS;
}

/**
  Run this function after SaPolicy PPI to be installed.

  @param[in]  PeiServices       General purpose services available to every PEIM.
  @param[in]  NotifyDescriptor  Notify that this module published.
  @param[in]  Ppi               PPI that was installed.

  @retval     EFI_SUCCESS       The function completed successfully.
**/
EFI_STATUS
EFIAPI
SiPreMemPolicyPpiNotifyCallback (
  IN CONST EFI_PEI_SERVICES     **PeiServices,
  IN EFI_PEI_NOTIFY_DESCRIPTOR  *NotifyDescriptor,
  IN VOID                       *Ppi
  )
{
  EFI_STATUS Status;

  ///
  /// Platform specific programming. It may have Chipset, EC or some platform specific
  /// programming here.
  ///
  Status = PlatformSpecificInitPreMem ();
  ASSERT_EFI_ERROR (Status);

  return Status;
}

/**
  Run this function after Wdt PPI to be installed.

  @param[in]  PeiServices       General purpose services available to every PEIM.
  @param[in]  NotifyDescriptor  Notify that this module published.
  @param[in]  Ppi               PPI that was installed.

  @retval     EFI_SUCCESS       The function completed successfully.
**/
EFI_STATUS
EFIAPI
WdtAppPpiNotifyCallback (
  IN CONST EFI_PEI_SERVICES     **PeiServices,
  IN EFI_PEI_NOTIFY_DESCRIPTOR  *NotifyDescriptor,
  IN VOID                       *Ppi
  )
{
  EFI_STATUS Status;
  WDT_PPI    *WdtPei;

  WdtPei = (WDT_PPI *) Ppi;

  Status = PeiWdtApp (WdtPei);
  ASSERT_EFI_ERROR (Status);

  return Status;
}

/**
  Programs the overrides for Vcc1p8 voltage which are controlled by UP1816
  chip using SVID/PMBUS interface.

  @param[in] Voltage      Voltage target in mV

  @retval EFI_SUCCESS     Successful to override voltage.
**/
EFI_STATUS
ProgramVcc1p8Override (
  IN  UINT16                Voltage
  )
{
  EFI_STATUS                  Status;
  EFI_SMBUS_DEVICE_ADDRESS    SlaveAddr;
  UINT8                       DeviceCommand;
  UINT8                       Data;
  UINT8                       Prefix;
  UINT8                       CurrentData;

  //
  // Target address 0x2A
  //
  SlaveAddr.SmbusDeviceAddress = (0x2A >> 1);

  //
  // Device Command and set Byte write
  //
  DeviceCommand = 0x2;  // VREF2

  //
  // Data = 0 is 1800 mV
  //
  Prefix = 0;
  if (Voltage >= 1800) {
    Data = (UINT8) (((Voltage - 1800) / 10) | (Prefix << 7));
  } else {
    Prefix = 1;
    Data = (UINT8) (((1800 - Voltage) / 10) | (Prefix << 7));
  }

  CurrentData = SmBusReadDataByte (
                  SMBUS_LIB_ADDRESS (SlaveAddr.SmbusDeviceAddress, DeviceCommand, 0, FALSE),
                  &Status
                  );

  DEBUG ((DEBUG_INFO, "SMBUS transaction request Vcc1p8: Addr=0x%08X Command=0x%08X CurrentData=0x%08X Data=0x%08X\n",
    SlaveAddr.SmbusDeviceAddress, DeviceCommand, CurrentData, Data));

  SmBusWriteDataByte (
    SMBUS_LIB_ADDRESS (SlaveAddr.SmbusDeviceAddress, DeviceCommand, 0, FALSE),
    Data,
    &Status
    );

  DEBUG ((DEBUG_INFO, "SMBUS transaction result : Status=0x%08X\n", Status));

  return Status;
}

/**
  Programs the overrides for Vcc1p05 voltage which are controlled by UP1816
  chip using SVID/PMBUS interface.

  @param[in] Voltage      Voltage target in mV

  @retval EFI_SUCCESS     Successful to override voltage.
**/
EFI_STATUS
ProgramVcc1p05Override (
  IN  UINT16                Voltage
  )
{
  EFI_STATUS                  Status;
  EFI_SMBUS_DEVICE_ADDRESS    SlaveAddr;
  UINT8                       Data;
  UINT8                       DeviceCommand;
  UINT8                       Prefix;
  UINT8                       CurrentData;

  //
  // Target address 0x2A
  //
  SlaveAddr.SmbusDeviceAddress = (0x2A >> 1);

  //
  // Device Command and set Byte write
  //
  DeviceCommand = 0x1; // VREF1

  //
  // Data = 0 is 1050 mV
  //
  Prefix = 0;
  if (Voltage >= 1050) {
    Data = (UINT8) (((Voltage - 1050) / 10) | (Prefix << 7));
  } else {
    Prefix = 1;
    Data = (UINT8) (((1050 - Voltage) / 10) | (Prefix << 7));
  }

  CurrentData = SmBusReadDataByte (
                  SMBUS_LIB_ADDRESS (SlaveAddr.SmbusDeviceAddress, DeviceCommand, 0, FALSE),
                  &Status
                  );

  DEBUG ((DEBUG_INFO, "SMBUS transaction request Vcc1p05: Addr=0x%08X Command=0x%08X CurrentData=0x%08X Data=0x%08X\n",
    SlaveAddr.SmbusDeviceAddress, DeviceCommand, CurrentData, Data));

  SmBusWriteDataByte (
    SMBUS_LIB_ADDRESS (SlaveAddr.SmbusDeviceAddress, DeviceCommand, 0, FALSE),
    Data,
    &Status
    );

  DEBUG ((DEBUG_INFO, "SMBUS transaction result : Status=0x%08X\n", Status));

  return Status;
}

/**
  Programs the overrides for VccDd2 voltage which are controlled by UP1816
  chip using SVID/PMBUS interface.

  @param[in] Voltage      Voltage target in mV

  @retval EFI_SUCCESS     Successful to override voltage.
**/
EFI_STATUS
ProgramVccDd2Override (
  IN  UINT16                Voltage
  )
{
  EFI_STATUS                  Status;
  EFI_SMBUS_DEVICE_ADDRESS    SlaveAddr;
  UINT8                       DeviceCommand;
  UINT8                       Data;
  UINT8                       Prefix;
  UINT8                       CurrentData;

  //
  // Target address 0x28
  //
  SlaveAddr.SmbusDeviceAddress = (0x28 >> 1);

  //
  // Device Command and set Byte write
  //
  DeviceCommand = 0x1;  // VREF1

  //
  // Data = 0 is 1100 mV
  //

  Prefix = 0;
  if (Voltage >= 1100) {
    Data = (UINT8) (((Voltage - 1100) / 10) | (Prefix << 7));
  } else {
    Prefix = 1;
    Data = (UINT8) (((1100 - Voltage) / 10) | (Prefix << 7));
  }

  CurrentData = SmBusReadDataByte (
                  SMBUS_LIB_ADDRESS (SlaveAddr.SmbusDeviceAddress, DeviceCommand, 0, FALSE),
                  &Status
                  );

  DEBUG ((DEBUG_INFO, "SMBUS transaction request VccDD2: Addr=0x%08X Command=0x%08X CurrentData=0x%08X Data=0x%08X\n",
    SlaveAddr.SmbusDeviceAddress, DeviceCommand, CurrentData, Data));

  SmBusWriteDataByte (
    SMBUS_LIB_ADDRESS (SlaveAddr.SmbusDeviceAddress, DeviceCommand, 0, FALSE),
    Data,
    &Status
    );

  DEBUG ((DEBUG_INFO, "SMBUS transaction result : Status=0x%08X\n", Status));

  return Status;
}

/**
  Programs the overrides for VCCIN_AUX_CPU voltage which are controlled by PCA9555 (IO Port expander).

  @param[in] Voltage        Voltage target in mV

  @retval    EFI_SUCCESS   The function completes successfully
  @retval    EFI_STATUS    Fail status return from SmBusWriteDataWord ()
**/
EFI_STATUS
ProgramVccInAuxOverride (
  IN  UINT16                Voltage
  )
{
  EFI_STATUS                  Status;
  EFI_SMBUS_DEVICE_ADDRESS    SecondaryAddr;
  UINT8                       DeviceCommand;
  UINT16                      InputVid;
  UINT16                      Vid1620mV;
  UINT16                      CurrentData;

  Vid1620mV = (UINT16) 0x1E8F;

  //
  // Secondary address 0x26 based on board design
  //
  SecondaryAddr.SmbusDeviceAddress = (VCCIN_AUX_SECONDARY_ADDRESS >> 1);

  //
  // Device Command and set Word write
  //
  DeviceCommand = 0x21;  // VCCIN AUX

  //
  // Send the voltage
  //
  InputVid = (UINT16) (Vid1620mV + ((Voltage - 1620) / 10));

  CurrentData = SmBusReadDataWord (
                  SMBUS_LIB_ADDRESS (SecondaryAddr.SmbusDeviceAddress, DeviceCommand, 0, FALSE),
                  &Status
                  );

  DEBUG ((DEBUG_INFO, "SMBUS transaction request VccCore: Addr=0x%08X Command=0x%08X CurrentData=0x%08X Data=0x%08X\n",
    SecondaryAddr.SmbusDeviceAddress, DeviceCommand, CurrentData, InputVid));

  SmBusWriteDataWord (
    SMBUS_LIB_ADDRESS (SecondaryAddr.SmbusDeviceAddress, DeviceCommand, 0, FALSE),
    InputVid,
    &Status
    );

  DEBUG ((DEBUG_INFO, "SMBUS transaction Status=0x%08X\n", Status));

  return Status;
}

/**
  Programs the overrides for Richtek VccIa voltage which are controlled by Richtek VR (External VR).

  @param[in] VidModeEn      Enabled/ Disable VCC VID Mode
  @param[in] Voltage        Voltage target in mV

  @retval    EFI_SUCCESS   The function completes successfully
  @retval    EFI_STATUS    Fail status return from SmBusWriteDataWord ()
**/
EFI_STATUS
ProgramRichtekVccIaOverride (
  IN  UINT8                 VidModeEn,
  IN  UINT16                Voltage
  )
{
  EFI_STATUS                  Status;
  EFI_SMBUS_DEVICE_ADDRESS    SecondaryAddr;
  UINT8                       DeviceCommand;
  UINT16                      InputVid;
  UINT8                       MsbVid;
  UINT8                       LsbVid;

  //
  // Secondary address 0x20 based on board design
  //
  SecondaryAddr.SmbusDeviceAddress = (RICHTEK_VR_SECONDARY_ADDRESS >> 1);

  //
  // Enable / Disable Fixed VID mode of Core Rail
  //
  DeviceCommand = 0x24;  // EN_VFIX_CORE for IA

  SmBusWriteDataByte (
    SMBUS_LIB_ADDRESS (SecondaryAddr.SmbusDeviceAddress, DeviceCommand, 0, FALSE),
    VidModeEn,
    &Status
    );
  DEBUG ((DEBUG_INFO, "SMBUS transaction result : Status=0x%08X\n", Status));

  if (VidModeEn) {
    //
    // Calculate request VID (voltage)
    //
    InputVid = (UINT16) ((Voltage - 245) / 5);
    MsbVid = (InputVid & RICHTEK_VFIX_MSB_CORE_MASK)? 0x01 : 0x00;
    LsbVid = (UINT8) (InputVid & RICHTEK_VFIX_LSB_CORE_MASK);

    DEBUG ((DEBUG_INFO, "SMBUS transaction request VccIA: Addr=0x%08X Command=0x%08X Data=0x%08X\n",
      SecondaryAddr.SmbusDeviceAddress, DeviceCommand, InputVid));

    DeviceCommand = 0x25;  // VFIX_LSB_CORE
    SmBusWriteDataByte (
      SMBUS_LIB_ADDRESS (SecondaryAddr.SmbusDeviceAddress, DeviceCommand, 0, FALSE),
      LsbVid,
      &Status
      );
    DEBUG ((DEBUG_INFO, "SMBUS transaction LSB Status=0x%08X\n", Status));

    DeviceCommand = 0x26;  // VFIX_MSB_CORE
    SmBusWriteDataByte (
      SMBUS_LIB_ADDRESS (SecondaryAddr.SmbusDeviceAddress, DeviceCommand, 0, FALSE),
      MsbVid,
      &Status
      );
    DEBUG ((DEBUG_INFO, "SMBUS transaction MSB Status=0x%08X\n", Status));
  }
  return Status;
}

/**
  Programs the overrides for Richtek VccGt voltage which are controlled by Richtek VR (External VR).

  @param[in] VidModeEn      Enabled/ Disable VCC VID Mode
  @param[in] Voltage        Voltage target in mV

  @retval    EFI_SUCCESS   The function completes successfully
  @retval    EFI_STATUS    Fail status return from SmBusWriteDataWord ()
**/
EFI_STATUS
ProgramRichtekVccGtOverride (
  IN  UINT8                 VidModeEn,
  IN  UINT16                Voltage
  )
{
  EFI_STATUS                  Status;
  EFI_SMBUS_DEVICE_ADDRESS    SecondaryAddr;
  UINT8                       DeviceCommand;
  UINT16                      InputVid;
  UINT8                       MsbVid;
  UINT8                       LsbVid;

  //
  // Secondary address 0x20 based on board design
  //
  SecondaryAddr.SmbusDeviceAddress = (RICHTEK_VR_SECONDARY_ADDRESS >> 1);

  //
  // Enable / Disable Fixed VID mode of Core Rail
  //
  DeviceCommand = 0x44;  // EN_VFIX_CORE for GT

  SmBusWriteDataByte (
    SMBUS_LIB_ADDRESS (SecondaryAddr.SmbusDeviceAddress, DeviceCommand, 0, FALSE),
    VidModeEn,
    &Status
    );
  DEBUG ((DEBUG_INFO, "SMBUS transaction result : Status=0x%08X\n", Status));

  if (VidModeEn) {
    //
    // Calculate request VID (voltage)
    //
    InputVid = (UINT16) ((Voltage - 245) / 5);
    MsbVid = (InputVid & RICHTEK_VFIX_MSB_CORE_MASK)? 0x01 : 0x00;
    LsbVid = (UINT8) (InputVid & RICHTEK_VFIX_LSB_CORE_MASK);

    DEBUG ((DEBUG_INFO, "SMBUS transaction request VccGt: Addr=0x%08X Command=0x%08X Data=0x%08X\n",
      SecondaryAddr.SmbusDeviceAddress, DeviceCommand, InputVid));

    DeviceCommand = 0x45;  // VFIX_LSB_CORE
    SmBusWriteDataByte (
      SMBUS_LIB_ADDRESS (SecondaryAddr.SmbusDeviceAddress, DeviceCommand, 0, FALSE),
      LsbVid,
      &Status
      );
    DEBUG ((DEBUG_INFO, "SMBUS transaction LSB Status=0x%08X\n", Status));

    DeviceCommand = 0x46;  // VFIX_MSB_CORE
    SmBusWriteDataByte (
      SMBUS_LIB_ADDRESS (SecondaryAddr.SmbusDeviceAddress, DeviceCommand, 0, FALSE),
      MsbVid,
      &Status
      );
    DEBUG ((DEBUG_INFO, "SMBUS transaction MSB Status=0x%08X\n", Status));
  }
  return Status;
}

/**
  Check and apply the override knobs for external voltage rails based on board design

  @param[in] CpuSetup       Setup variables under CPU Setup
**/
VOID
ProgramVoltageOverrides (
  IN  CPU_SETUP             *CpuSetup
  )
{
  EFI_STATUS Status = 0x0;

  //
  // Check for Vcc1p8 voltage override
  //
  if (CpuSetup->Vcc1p8OverrideEnable == 0) {
    DEBUG ((DEBUG_INFO, "Default Vcc1p8 selected. No action required for voltage programming.\n"));
  } else {
    DEBUG ((DEBUG_INFO, "(OCVR) Programming Vcc1p8 Override = %d\n", CpuSetup->Vcc1p8OverrideVoltage));
    Status = ProgramVcc1p8Override (CpuSetup->Vcc1p8OverrideVoltage);

    if (EFI_ERROR (Status)) {
      DEBUG ((DEBUG_INFO, "Vcc1p8 programming failed. Status = %r\n", Status));
    } else {
      DEBUG ((DEBUG_INFO, "Vcc1p8 programming successful. Status = %r\n", Status));
    }
  }

  //
  // Check for Vcc1p05 voltage override
  //
  if (CpuSetup->Vcc1p05OverrideEnable == 0) {
    DEBUG ((DEBUG_INFO, "Default Vcc1p05 selected. No action required for voltage programming.\n"));
  } else {
    DEBUG ((DEBUG_INFO, "(OCVR) Programming Vcc1p05 Override = %d\n", CpuSetup->Vcc1p05OverrideVoltage));
    Status = ProgramVcc1p05Override (CpuSetup->Vcc1p05OverrideVoltage);

    if (EFI_ERROR (Status)) {
      DEBUG ((DEBUG_INFO, "Vcc1p05 programming failed. Status = %r\n", Status));
    } else {
      DEBUG ((DEBUG_INFO, "Vcc1p05 programming successful. Status = %r\n", Status));
    }
  }

  //
  // Check for VccDD2 voltage override
  //
  if (CpuSetup->VccDd2OverrideEnable == 0) {
    DEBUG ((DEBUG_INFO, "Default VccDD2 selected. No action required for voltage programming.\n"));
  } else {
    DEBUG ((DEBUG_INFO, "(OCVR) Programming VccDD2 Override = %d\n", CpuSetup->VccDd2OverrideVoltage));
    Status = ProgramVccDd2Override (CpuSetup->VccDd2OverrideVoltage);

    if (EFI_ERROR (Status)) {
      DEBUG ((DEBUG_INFO, "VccDD2 programming failed. Status = %r\n", Status));
    } else {
      DEBUG ((DEBUG_INFO, "VccDD2 programming successful. Status = %r\n", Status));
    }
  }

  //
  // Check for VCCIN AUX CPU voltage override
  //
  if (CpuSetup->VccInAuxEnable == 0) {
    DEBUG ((DEBUG_INFO, "Default VCCIN AUX CPU selected. No action required for voltage programming.\n"));
  } else {
    DEBUG ((DEBUG_INFO, "(OCVR) Programming VCCIN AUX CPU Override = %d\n", CpuSetup->VccInAuxVoltage));
    Status = ProgramVccInAuxOverride (CpuSetup->VccInAuxVoltage);

    if (EFI_ERROR (Status)) {
      DEBUG ((DEBUG_INFO, "VCCIN AUX programming failed. Status = %r\n", Status));
    } else {
      DEBUG ((DEBUG_INFO, "VCCIN AUX programming successful. Status = %r\n", Status));
    }
  } // VccInAux override

  //
  // Check for RichTek VccIA voltage override
  //
  if (CpuSetup->RichtekVccIaEnable == 0) {
    DEBUG ((DEBUG_INFO, "Default RichTek VccIA selected. No action required for voltage programming.\n"));
  } else {
    DEBUG ((DEBUG_INFO, "(OCVR) Programming RichTek VccIA Override = %d\n", CpuSetup->RichtekVccIaVoltage));
    Status = ProgramRichtekVccIaOverride (CpuSetup->RichtekVccIaFixedVidMode, CpuSetup->RichtekVccIaVoltage);

    if (EFI_ERROR (Status)) {
      DEBUG ((DEBUG_INFO, "RichTek VccIA programming failed. Status = %r\n", Status));
    } else {
      DEBUG ((DEBUG_INFO, "RichTek VccIA programming successful. Status = %r\n", Status));
    }
  } // RichTek VccIA override


  //
  // Check for RichTek VccGT voltage override
  //
  if (CpuSetup->RichtekVccGtEnable == 0) {
    DEBUG ((DEBUG_INFO, "Default RichTek VccGT selected. No action required for voltage programming.\n"));
  } else {
    DEBUG ((DEBUG_INFO, "(OCVR) Programming RichTek VccGT Override = %d\n", CpuSetup->RichtekVccGtVoltage));
    Status = ProgramRichtekVccGtOverride (CpuSetup->RichtekVccGtFixedVidMode, CpuSetup->RichtekVccGtVoltage);

    if (EFI_ERROR (Status)) {
      DEBUG ((DEBUG_INFO, "RichTek VccGT programming failed. Status = %r\n", Status));
    } else {
      DEBUG ((DEBUG_INFO, "RichTek VccGT programming successful. Status = %r\n", Status));
    }
  } // RichTek VccGT override
}

/**
  Platform Voltage Init - Programs platform voltages over SMBUS.
  Run this function after Smbus2Ppi is installed.

  @param[in]  PeiServices       General purpose services available to every PEIM.
  @param[in]  NotifyDescriptor  Notify that this module published.
  @param[in]  Ppi               PPI that was installed.

  @retval     EFI_SUCCESS       The function completed successfully.
**/
EFI_STATUS
EFIAPI
PlatformVoltageInit (
  IN CONST EFI_PEI_SERVICES     **PeiServices,
  IN EFI_PEI_NOTIFY_DESCRIPTOR  *NotifyDescriptor,
  IN VOID                       *Ppi
  )
{
  EFI_STATUS                      Status;
  CPU_SETUP                       CpuSetup;
  EFI_PEI_READ_ONLY_VARIABLE2_PPI *ReadOnlyVariable;
  UINTN                           VariableSize;
  WDT_PPI                         *gWdtPei;
  UINTN                           SmbusPciBase;
  UINT16                          SmbusIoBar;

  DEBUG ((DEBUG_INFO, "(OC) Platform Voltage Init\n"));

  Status = PeiServicesLocatePpi (
             &gEfiPeiReadOnlyVariable2PpiGuid,
             0,
             NULL,
             (VOID **) &ReadOnlyVariable
             );
  ASSERT_EFI_ERROR (Status);

  //
  // Locate WDT_PPI (ICC WDT PPI)
  //
  gWdtPei = NULL;
  Status = PeiServicesLocatePpi (
             &gWdtPpiGuid,
             0,
             NULL,
             (VOID **) &gWdtPei
             );

  VariableSize = sizeof (CPU_SETUP);
  Status = ReadOnlyVariable->GetVariable (
                               ReadOnlyVariable,
                               L"CpuSetup",
                               &gCpuSetupVariableGuid,
                               NULL,
                               &VariableSize,
                               &CpuSetup
                               );

  if (EFI_ERROR (Status)) {
    //
    // If GetVariable fails.
    //
    DEBUG ((DEBUG_INFO, "(OC) Failed to read Setup Data\n"));
    return EFI_SUCCESS;
  }

  if (CpuSetup.OverclockingSupport == 0) {
    DEBUG ((DEBUG_INFO, "(OC) Skipping Platform Voltage Init, overclocking is not enabled in BIOS setup.\n"));
    return EFI_SUCCESS;
  }

  ///
  /// External Voltage rails control
  ///   - Check if we need to override the voltages of the different VRMs using the SVID or PMBUS interfaces
  ///
  if (CpuSetup.OverclockingSupport != 0) {
    //
    // Initial SMBUS
    //
    SmbusIoBar = 0xEFA0;

    //
    // Temporary initialize SMBUS
    //
    SmbusPciBase = PCI_SEGMENT_LIB_ADDRESS (
                     DEFAULT_PCI_SEGMENT_NUMBER_PCH,
                     DEFAULT_PCI_BUS_NUMBER_PCH,
                     PCI_DEVICE_NUMBER_PCH_SMBUS,
                     PCI_FUNCTION_NUMBER_PCH_SMBUS,
                     0
                     );

    DEBUG ((DEBUG_INFO, "SmbusPciBase = %x\n", SmbusPciBase));
    ///
    /// Set SMBus PCI 0x64 = 0x0A0A0000
    ///
    PciSegmentWrite32 (SmbusPciBase + R_SMBUS_CFG_64, 0x0A0A0000);

    //
    // Initialize I/O BAR
    //
    PciSegmentAndThenOr32 (SmbusPciBase + R_SMBUS_CFG_BASE, B_SMBUS_CFG_BASE_BAR, SmbusIoBar);

    //
    // Enable the Smbus I/O Enable
    //
    PciSegmentOr8 (SmbusPciBase + PCI_COMMAND_OFFSET, EFI_PCI_COMMAND_IO_SPACE);

    //
    // Enable the SMBUS Controller
    //
    PciSegmentOr8 (SmbusPciBase + R_SMBUS_CFG_HOSTC, B_SMBUS_CFG_HOSTC_SSRESET);

    PciSegmentAndThenOr8 (
     SmbusPciBase + R_SMBUS_CFG_HOSTC,
     (UINT8) (~(B_SMBUS_CFG_HOSTC_SMI_EN | B_SMBUS_CFG_HOSTC_I2C_EN)),
     B_SMBUS_CFG_HOSTC_HST_EN
     );

    ProgramVoltageOverrides (&CpuSetup);
  }

  return Status;
}

/**
  Build a HOB for debug configure data.

  The DebugConfigHob is built by platform code and consumed by PeiDxeSmmDebugPrintErrorLevelLib and
  PeiDxeSmmSerialPortParameterLib library instances.

  @param[in] DebugConfigData     A pointer to the DEBUG_CONFIG_DATA.

**/
VOID
EFIAPI
BuildDebugConfigDataHob (
  IN DEBUG_CONFIG_DATA     *DebugConfigData
  )
{
  DEBUG_CONFIG_DATA_HOB    *DebugConfigDataHob;

  DebugConfigDataHob = BuildGuidHob (&gDebugConfigHobGuid, sizeof (DEBUG_CONFIG_DATA_HOB));
  ASSERT (DebugConfigDataHob != NULL);
  if (DebugConfigDataHob == NULL) {
    DEBUG ((DEBUG_ERROR, "Build Debug Config Hob failed!\n"));
    return;
  }

  DebugConfigDataHob->SerialDebugMrcLevel    = DebugConfigData->SerialDebugMrcLevel;
  DebugConfigDataHob->SerialDebug            = DebugConfigData->SerialDebug;
  DebugConfigDataHob->SerialDebugBaudRate    = DebugConfigData->SerialDebugBaudRate;
  DebugConfigDataHob->RamDebugInterface      = 0;
  DebugConfigDataHob->UartDebugInterface     = (DebugConfigData->UartDebugInterface == 1 && FeaturePcdGet (PcdSerialPortEnable)) ? 1 : 0;
  DebugConfigDataHob->Usb3DebugInterface     = (DebugConfigData->Usb3DebugInterface == 1 && FeaturePcdGet (PcdUsb3SerialStatusCodeEnable)) ? 1 : 0;
  DebugConfigDataHob->TraceHubDebugInterface = (DebugConfigData->TraceHubDebugInterface == 1) ? 1 : 0;

  DebugConfigDataHob->SerialIoDebugInterface            = (DebugConfigData->SerialIoDebugInterface == 1 && FeaturePcdGet (PcdSerialIoUartEnable)) ? 1 : 0;
  DebugConfigDataHob->SerialIoUartDebugControllerNumber = DebugConfigData->SerialIoUartDebugControllerNumber;
  DebugConfigDataHob->SerialIoUartDebugBaudRate         = DebugConfigData->SerialIoUartDebugBaudRate;
  DebugConfigDataHob->SerialIoUartDebugStopBits         = DebugConfigData->SerialIoUartDebugStopBits;
  DebugConfigDataHob->SerialIoUartDebugParity           = DebugConfigData->SerialIoUartDebugParity;
  DebugConfigDataHob->SerialIoUartDebugFlowControl      = DebugConfigData->SerialIoUartDebugFlowControl;
  DebugConfigDataHob->SerialIoUartDebugDataBits         = DebugConfigData->SerialIoUartDebugDataBits;
}

/**
  Basic GPIO configuration before memory is ready

**/
VOID
GpioInitEarlyWwanPreMem (
  VOID
  )
{
  GPIO_CONFIG                     BbrstConfig;
  UINT32                          WwanBbrstGpio;
  UINT32                          WwanPerstGpio;
  UINT32                          WwanFcpoGpio;

  WwanFcpoGpio  = PcdGet32 (PcdWwanFullCardPowerOffGpio);
  WwanBbrstGpio = PcdGet32 (PcdWwanBbrstGpio);
  WwanPerstGpio = PcdGet32 (PcdWwanPerstGpio);

  if (WwanBbrstGpio) {
    //
    // BIOS needs to reset modem if modem RESET# is not asserted via PLTRST# in the previous sleep state
    //
    GpioGetPadConfig (WwanBbrstGpio, &BbrstConfig);
    if ((PcdGet8 (PcdPcieWwanEnable) == 0) ||
        ((PcdGetBool (PcdWwanResetWorkaround) == TRUE) &&
        (BbrstConfig.Direction == GpioDirOut) &&
        (BbrstConfig.OutputState == GpioOutHigh))) {
      //
      // Assert FULL_CARD_POWER_OFF#, RESET# and PERST# GPIOs
      //
      if (PcdGet8 (PcdPcieWwanEnable) == 1) {   // Turn off 4G modem
        if (PcdGet32 (PcdBoardGpioTableWwanOffEarlyPreMem) != 0 && PcdGet16 (PcdBoardGpioTableWwanOffEarlyPreMemSize) != 0) {
          ConfigureGpio ((VOID *) (UINTN) PcdGet32 (PcdBoardGpioTableWwanOffEarlyPreMem), (UINTN) PcdGet16 (PcdBoardGpioTableWwanOffEarlyPreMemSize));
        }
        MicroSecondDelay (1 * 1000);          // Delay by 1ms
      } else {
        if (PcdGet8 (PcdPcieWwanEnable) == 2) { // Turn off 5G modem
          if (WwanPerstGpio) {
            GpioSetOutputValue (WwanPerstGpio, PcdGetBool (PcdWwanPerstGpioPolarity));
            MicroSecondDelay ((UINTN) PcdGet16 (PcdBoardWwanTPer2ResDelayMs) * 1000);
          }
          if (WwanBbrstGpio) {
            GpioSetOutputValue (WwanBbrstGpio, 0);
            MicroSecondDelay ((UINTN) PcdGet16 (PcdBoardWwanTRes2OffDelayMs) * 1000);
          }
          if (WwanFcpoGpio) {
            GpioSetOutputValue (WwanFcpoGpio, PcdGetBool (PcdWwanPerstGpioPolarity));
            MicroSecondDelay ((UINTN) PcdGet16 (PcdBoardWwanTOffDisDelayMs) * 1000);
          }
        }
      }
      DEBUG((DEBUG_INFO, "Put modem in OFF state\n"));
    }

    if (PcdGet8 (PcdPcieWwanEnable) == 1) { // 4G - Intel Modem 7360/7560
      if (PcdGet32 (PcdBoardGpioTableWwanOnEarlyPreMem) != 0 && PcdGet16 (PcdBoardGpioTableWwanOnEarlyPreMemSize) != 0) {
        ConfigureGpio ((VOID *) (UINTN) PcdGet32 (PcdBoardGpioTableWwanOnEarlyPreMem), (UINTN) PcdGet16 (PcdBoardGpioTableWwanOnEarlyPreMemSize));
      }
    } else if (PcdGet8 (PcdPcieWwanEnable) == 2) { // 5G - Mediatek Modem M80
      if (PcdGet32 (PcdBoardGpioTableM80WwanOnEarlyPreMem) != 0 && PcdGet16 (PcdBoardGpioTableM80WwanOnEarlyPreMemSize) != 0) {
        DEBUG((DEBUG_INFO, "5G M80 Modem: Power On Sequnce Start\n"));
        //
        // 1. Turn on EN_V3.3A_WWAN_LS, Turn off WWAN_RST_N & WWAN_PERST
        //
        ConfigureGpio ((VOID *) (UINTN) PcdGet32(PcdBoardGpioTableM80WwanOnEarlyPreMem), (UINTN) PcdGet16 (PcdBoardGpioTableM80WwanOnEarlyPreMemSize));
        MicroSecondDelay ((UINTN) PcdGet16 (PcdBoardWwanTOn2ResDelayMs) * 1000);
        //
        // 2. De-asserting WWAN_RST_N
        //
        if (WwanBbrstGpio) {
          GpioSetOutputValue (WwanBbrstGpio, 1);
          MicroSecondDelay ((UINTN) PcdGet16 (PcdBoardWwanTOnRes2PerDelayMs) * 1000);
        }
        //
        // 3. De-asserting WWAN_PERST
        //
        if (WwanPerstGpio) {
          GpioSetOutputValue (WwanPerstGpio, (~(PcdGetBool (PcdWwanPerstGpioPolarity))) & BIT0);
        }
        DEBUG((DEBUG_INFO, "5G M80 Modem: Power On Sequnce End\n"));
      }
    }
  } else if(PcdGetBool (PcdPcieDeviceOnWwanSlot) == TRUE) {
      if (PcdGet32 (PcdBoardGpioTableWwanOnEarlyPreMem) != 0 && PcdGet16 (PcdBoardGpioTableWwanOnEarlyPreMemSize) != 0) {
        ConfigureGpio ((VOID *) (UINTN) PcdGet32 (PcdBoardGpioTableWwanOnEarlyPreMem), (UINTN) PcdGet16 (PcdBoardGpioTableWwanOnEarlyPreMemSize));
      }
  }
}

/**
  Update PCDs for debug configure data.

  The DebugConfigHob is built by platform code and consumed by PeiDxeSmmDebugPrintErrorLevelLib and
  PeiDxeSmmSerialPortParameterLib library instances.

  @param[in] DebugConfigData     A pointer to the DEBUG_CONFIG_DATA.

**/
VOID
EFIAPI
UpdateDebugConfigPcds (
  IN DEBUG_CONFIG_DATA     *DebugConfigData
  )
{
//[-start-200721-IB17040134-modify]//
//
//  New EDKII move this pcd to PcdsDynamic from PcdsFeatureFlag
//  But Insyde EDKII haven't update
//  And PcdsFeatureFlag can't be setting in post
//  WA rollback(marked) it.
//
//  PcdSetBoolS (PcdStatusCodeUseSerial, DebugConfigData->UartDebugInterface == 1 && FeaturePcdGet(PcdSerialPortEnable) ? TRUE : FALSE);
//[-end-200721-IB17040134-modify]//

  return ;
}
//[-start-210505-IB16810152-add]//
VOID
EFIAPI
CpBaseVerifyCallback (
  IN EFI_EVENT                         Event,
  IN H2O_CP_HANDLE                     Handle
  )
{
  EFI_STATUS                         Status;
  H2O_BASE_CP_VERIFY_FV_DATA         *VerifyFvData;
  BOOLEAN                            SwapState = 0;

  Status = H2OCpLookup (Handle, (VOID **) &VerifyFvData, NULL);
  if (EFI_ERROR (Status)) {
    DEBUG_CP ((DEBUG_ERROR, "Checkpoint Data Not Found: %x (%r)\n", Handle, Status));
    DEBUG_CP ((DEBUG_ERROR, "   %a\n", __FUNCTION__));
    return;
  }

  if (!VerifyFvData->Trusted) {
    TopSwapSet (TRUE);
    VerifyFvData->Status = H2O_CP_TASK_UPDATE;
    SwapState = TopSwapStatus();
    DEBUG_CP ((DEBUG_INFO, "CpBaseVerifyCallback verify FV fail at FvBase: %x FvLength: %x\n", VerifyFvData->FvBase, VerifyFvData->FvLength));
    DEBUG_CP ((DEBUG_INFO, "CpBaseVerifyCallback set Top swap %x\n", SwapState));
  }
}
//[-end-210505-IB16810152-add]//
//[-start-211105-IB18410122-add]//
VOID
EFIAPI
CpBaseVerifySkipCallback (
  IN EFI_EVENT                         Event,
  IN H2O_CP_HANDLE                     Handle
  )
{
  EFI_STATUS                         Status;
  H2O_BASE_CP_VERIFY_FV_DATA         *VerifyFvData;

  Status = H2OCpLookup (Handle, (VOID **) &VerifyFvData, NULL);
  if (EFI_ERROR (Status)) {
    DEBUG_CP ((DEBUG_ERROR, "Checkpoint Data Not Found: %x (%r)\n", Handle, Status));
    DEBUG_CP ((DEBUG_ERROR, "   %a\n", __FUNCTION__));
    return;
  }

  if (IsFirmwareFailureRecovery() && VerifyFvData->Trusted == FALSE) {
    VerifyFvData->Status = H2O_CP_TASK_BREAK_SKIP;
    DEBUG_CP ((DEBUG_INFO, "During Seamless Recovery, skip hanging at 0xDE even if variable default region verify failed.\n"));
  }
}
//[-end-211105-IB18410122-add]//

/**
  ARM ec wdt and set timeout value

  @retval     EFI_SUCCESS  The function completes successfully
  @retval     others
**/
#if FixedPcdGetBool(PcdResiliencyEnable) == 1
EFI_STATUS
ArmEcWdt (
  VOID
  )
{
  EFI_STATUS                      Status;
  EFI_PEI_READ_ONLY_VARIABLE2_PPI *PeiReadOnlyVariablePpi;
  UINT8                           InitSetupFlag;
  UINTN                           Size;
  EC_WDT_R_PARAMETER              ReadData;
  BOOLEAN                         TopSwapStatus;
  UINT8                           WdtTimer;

  Status = PeiServicesLocatePpi (
             &gEfiPeiReadOnlyVariable2PpiGuid,
             0,
             NULL,
             (VOID **) &PeiReadOnlyVariablePpi
             );
  if (EFI_ERROR (Status)) {
    ASSERT_EFI_ERROR (Status);
    return Status;
  }

  TopSwapStatus = SpiIsTopSwapEnabled ();
  ReadData.EcWdtRead = 0;
  Status = BiosEcWdtReadCommand((UINT8 *)(&ReadData));

  DEBUG((DEBUG_INFO, "check ec wdt data and topswap status: %x %d\n", ReadData.EcWdtRead, TopSwapStatus));
  WdtTimer = EC_WDT_TIMEOUT;
  ///
  /// enable ec wdt for resiliency only case, set different timeout for first and normal boot
  ///
  Size = sizeof (InitSetupFlag);
  Status = PeiReadOnlyVariablePpi->GetVariable (PeiReadOnlyVariablePpi, L"InitSetupVariable", &gSetupVariableGuid, NULL, &Size, &InitSetupFlag);
  if (Status == EFI_NOT_FOUND || (Size != sizeof (InitSetupFlag))) {
    DEBUG((DEBUG_INFO, "First boot, set max timeout value as 255 seconds. \n"));
    WdtTimer = EC_WDT_FIRST_BOOT;
  }

  DEBUG((DEBUG_INFO, "arm ec wdt with timeout: %x\n", WdtTimer));

  Status = ArmEcWdtWithTimeOut (WdtTimer);

  return Status;
}
#endif
/**
  This function handles PlatformInit task after PeiReadOnlyVariable2 PPI produced

  @param[in]  PeiServices  Pointer to PEI Services Table.
  @param[in]  NotifyDesc   Pointer to the descriptor for the Notification event that
                           caused this function to execute.
  @param[in]  Ppi          Pointer to the PPI data associated with this function.

  @retval     EFI_SUCCESS  The function completes successfully
  @retval     others
**/
EFI_STATUS
EFIAPI
PlatformInitPreMem (
  IN CONST EFI_PEI_SERVICES     **PeiServices,
  IN EFI_PEI_NOTIFY_DESCRIPTOR  *NotifyDescriptor,
  IN VOID                       *Ppi
  )
{
  EFI_STATUS                        Status;
  EFI_PEI_READ_ONLY_VARIABLE2_PPI   *VariableServices;
  EFI_BOOT_MODE                     BootMode;
  UINT64                            McD0BaseAddress;
  UINT16                            ABase;
  SETUP_DATA                        SystemConfiguration;
  UINTN                             VariableSize;
//[-start-200420-IB17800056-modify]//
//  CHAR16                            BiosVersion[40];
//  CHAR16                            ReleaseDate[20];
//  CHAR16                            ReleaseTime[20];
//  UINTN                             Size;
//[-end-200420-IB17800056-modify]//
  DEBUG_CONFIG_DATA                 DebugConfigData;
  ME_SETUP                          MeSetup;
  UINT32                            PttFtifReg;
  UINT8                             TpmInterfaceId;
#if (FixedPcdGetBool (PcdBootGuardEnable) == 1) || (FixedPcdGetBool (PcdTxtEnable) == 1)
  BOOT_GUARD_INFO                   BootGuardInfo;
#endif // (FixedPcdGetBool (PcdBootGuardEnable) == 1) || (FixedPcdGetBool (PcdTxtEnable) == 1)
//[-start-210505-IB16810152-add]//
  H2O_CP_HANDLE                     CpHandle;
//[-end-210505-IB16810152-add]//

  PostCode(PLATFORM_INIT_PREMEM_ENTRY);

  if (PcdGetBool (PcdPlatformInitPreMem)) {
    return EFI_SUCCESS;
  } else {
    Status = PcdSetBoolS (PcdPlatformInitPreMem, TRUE);
    ASSERT_EFI_ERROR (Status);
  }

  VariableServices = (EFI_PEI_READ_ONLY_VARIABLE2_PPI *) Ppi;

//[-start-211105-IB18410122-modify]//
//[-start-210505-IB16810152-add]//
  if (PcdGet8(PcdChasmFallsSupport) == 2) {
    if (FeaturePcdGet (PcdH2OBaseCpVerifyFvSupported)) {

      Status = H2OCpRegisterHandler (
               &gH2OBaseCpVerifyFvGuid,
               CpBaseVerifyCallback,
               H2O_CP_MEDIUM_HIGH,
               &CpHandle
               );

      if (EFI_ERROR (Status)) {
        DEBUG_CP ((DEBUG_ERROR, "Checkpoint Register Fail: %g (%r)\n", &gH2OBaseCpVerifyFvGuid, Status));
        return Status;
      }
      DEBUG_CP ((DEBUG_INFO, "Checkpoint Registered: %g (%r)\n", &gH2OBaseCpVerifyFvGuid, Status));
    }
  }
//[-end-210505-IB16810152-add]//
//[-end-211105-IB18410122-modify]//
//[-start-211105-IB18410122-add]//
  if (FeaturePcdGet (PcdH2OBaseCpVerifyFvSupported)) {
    Status = H2OCpRegisterHandler (
               &gH2OBaseCpVerifyFvGuid,
               CpBaseVerifySkipCallback,
               H2O_CP_MEDIUM,
               &CpHandle
               );

    if (EFI_ERROR (Status)) {
      DEBUG_CP ((DEBUG_ERROR, "Checkpoint Register Fail: %g (%r)\n", &gH2OBaseCpVerifyFvGuid, Status));
      return Status;
    }
    DEBUG_CP ((DEBUG_INFO, "Checkpoint Registered: %g (%r)\n", &gH2OBaseCpVerifyFvGuid, Status));
  }
//[-end-211105-IB18410122-add]//
//[-start-200420-IB17800056-modify]//
//  Status = GetBiosVersionDateTime (BiosVersion, ReleaseDate, ReleaseTime);
//  Size = sizeof (BiosVersion);
//  PcdSetPtrS (PcdBiosVersion, &Size, &BiosVersion);
//  Size = sizeof (ReleaseDate);
//  PcdSetPtrS (PcdReleaseDate, &Size, &ReleaseDate);
//  Size = sizeof (ReleaseTime);
//  PcdSetPtrS (PcdReleaseTime, &Size, &ReleaseTime);
//[-end-200420-IB17800056-modify]//

  ///
  /// EDK/FSP dispatch mode switches PcdAcpiS3Enable dynamically on gSiPkgTokenSpaceGuid.PcdS3Enable setting.
  /// FSP API mode has a default PcdAcpiS3Enable setting but does not turn the PCD on/off here.
  ///
#if FixedPcdGetBool (PcdFspModeSelection) == 0
#if FixedPcdGetBool (PcdS3Enable) == 1
  PcdSetBoolS (PcdAcpiS3Enable, TRUE);
#else
  PcdSetBoolS (PcdAcpiS3Enable, FALSE);
#endif
#endif

  PeiServicesGetBootMode (&BootMode);

  //
  // Set FailSafe Critical Temp & Fan Speed setting
  //
  Status = SetFailSafeFanCtrl (
              PcdGet8(PcdEcFailSafeActionCpuTemp),
              PcdGet8(PcdEcFailSafeActionFanPwm)
            );
  DEBUG ((DEBUG_INFO, " EC FailSafe (CpuTemp,Fan) set to (%d,%d), Status = %r\n",\
            PcdGet8(PcdEcFailSafeActionCpuTemp),PcdGet8(PcdEcFailSafeActionFanPwm), Status));

  //
  // Set FailSafe Critical Temp & Fan Speed setting
  //
  Status = SetFailSafeFanCtrl (
              PcdGet8(PcdEcFailSafeActionCpuTemp),
              PcdGet8(PcdEcFailSafeActionFanPwm)
            );
  DEBUG ((DEBUG_INFO, " EC FailSafe (CpuTemp,Fan) set to (%d,%d), Status = %r\n",\
            PcdGet8(PcdEcFailSafeActionCpuTemp),PcdGet8(PcdEcFailSafeActionFanPwm), Status));

  VariableSize = sizeof (SETUP_DATA);
  Status = VariableServices->GetVariable (
                               VariableServices,
                               L"Setup",
                               &gSetupVariableGuid,
                               NULL,
                               &VariableSize,
                               &SystemConfiguration
                               );
  if (EFI_ERROR (Status)) {
    DEBUG ((DEBUG_ERROR, "Fail to get System Configuration and set the configuration to production mode!\n"));
    PcdSet8S (PcdPcieWwanEnable, 0);
    PcdSetBoolS (PcdWwanResetWorkaround, FALSE);
  } else {
    PcdSet8S (PcdPcieWwanEnable, SystemConfiguration.WwanEnable);
    PcdSetBoolS (PcdWwanResetWorkaround, SystemConfiguration.WwanResetWorkaround);
    DEBUG((DEBUG_INFO, "WwanEnable: %d\n", PcdGet8 (PcdPcieWwanEnable)));
    DEBUG((DEBUG_INFO, "WwanTOn2ResDelayMs: %d\n", PcdGet16 (PcdBoardWwanTOn2ResDelayMs)));
    DEBUG((DEBUG_INFO, "WwanTOnRes2PerDelayMs: %d\n", PcdGet16 (PcdBoardWwanTOnRes2PerDelayMs)));
    DEBUG((DEBUG_INFO, "WwanTOnPer2PdsDelayMs: %d\n", PcdGet16 (PcdBoardWwanTOnPer2PdsDelayMs)));
    DEBUG((DEBUG_INFO, "WwanTPer2ResDelayMs: %d\n", PcdGet16 (PcdBoardWwanTRes2OffDelayMs)));
    DEBUG((DEBUG_INFO, "WwanTRes2OffDelayMs: %d\n", PcdGet16 (PcdBoardWwanTRes2OffDelayMs)));
    DEBUG((DEBUG_INFO, "WwanTOffDisDelayMs: %d\n", PcdGet16 (PcdBoardWwanTOffDisDelayMs)));
    DEBUG((DEBUG_INFO, "WwanTResTogDelayMs: %d\n", PcdGet16 (PcdBoardWwanTResTogDelayMs)));
    DEBUG((DEBUG_INFO, "WwanTRes2PdsDelayMs: %d\n", PcdGet16 (PcdBoardWwanTRes2PdsDelayMs)));
  }

  GpioInitEarlyWwanPreMem();
  USBCRetimerComplianceModePreMem();

#if FixedPcdGetBool(PcdDTbtEnable) == 1
  if (SystemConfiguration.DiscreteTbtSupport) {
    InstallPeiDTbtPolicy ();
  }
#endif


DEBUG_CODE_BEGIN();
  VariableSize = sizeof (DEBUG_CONFIG_DATA);
  Status = VariableServices->GetVariable (
                               VariableServices,
                               L"DebugConfigData",
                               &gDebugConfigVariableGuid,
                               NULL,
                               &VariableSize,
                               &DebugConfigData
                               );
  if (EFI_ERROR (Status)) {
    DEBUG ((DEBUG_ERROR, "Failed to get Debug Configuration data variable!\n"));
  } else {
    BuildDebugConfigDataHob (&DebugConfigData);
    UpdateDebugConfigPcds (&DebugConfigData);
  }
DEBUG_CODE_END();

  ///
  ///
  ///
  ///

//[-start-200420-IB17800056-modify]//
//  Status = CreateFastBootHob (VariableServices);
//  ASSERT_EFI_ERROR (Status);
//[-end-200420-IB17800056-modify]//

  Status = PeiServicesNotifyPpi (&mWdtPpiNotifyList);
  ASSERT_EFI_ERROR (Status);

  Status = PeiServicesNotifyPpi (&mSiPreMemPolicyNotifyList);
  ASSERT_EFI_ERROR (Status);

#if FixedPcdGetBool (PcdOverclockEnable) == 1
  Status = PeiServicesNotifyPpi (&mPlatformVoltageInitOnMemoryNotifyList);
  ASSERT_EFI_ERROR (Status);
#endif
  ///
  /// Do basic PCH init
  ///
  PlatformPchInit (VariableServices);

  ///
  /// Set what state (S0/S5) to go to when power is re-applied after a power failure (G3 state)
  ///
  SetTheStateToGoAfterG3 (VariableServices);

  ABase = PmcGetAcpiBase ();

  ///
  /// Clear all pending SMI. On S3 clear power button enable so it will not generate an SMI.
  ///
  IoWrite16 (ABase + R_ACPI_IO_PM1_EN, 0);
  IoWrite32 (ABase + R_ACPI_IO_GPE0_EN_127_96, 0);
  ///----------------------------------------------------------------------------------
  ///
  /// BIOS should check the wake status before memory initialization to determine
  /// if ME has reset the system while the host was in a sleep state. If platform was not in a sleep state,
  /// BIOS should ensure a non-sleep exit path is taken by forcing an s5 exit.
  ///
  if (PmcGetSleepTypeAfterWake () == PmcNotASleepState) {
    PmcSetSleepState (PmcS5SleepState);
  }

  if (BootMode == BOOT_ON_S3_RESUME) {
    //
    // A platform PEIM should enable R/W access to E/F segment in the S3 boot path
    // otherwise, this AP wakeup buffer can't be accessed during CPU S3 operation.
    //
    McD0BaseAddress = PCI_SEGMENT_LIB_ADDRESS (SA_SEG_NUM, SA_MC_BUS, SA_MC_DEV, SA_MC_FUN, 0);
    PciSegmentWrite8 (McD0BaseAddress + R_SA_PAM0, 0x30);
    PciSegmentWrite8 (McD0BaseAddress + R_SA_PAM5, 0x33);
    PciSegmentWrite8 (McD0BaseAddress + R_SA_PAM6, 0x33);
  }

#if (FixedPcdGetBool (PcdBootGuardEnable) == 1) || (FixedPcdGetBool (PcdTxtEnable) == 1)
  //
  // TPM Initialized by Boot Guard
  //
  if (IsBootGuardSupported ()) {
//[-start-210714-IB17040201-add]//
    ZeroMem (&BootGuardInfo, sizeof(BOOT_GUARD_INFO));
//[-end-210714-IB17040201-add]//
    GetBootGuardInfo (&BootGuardInfo);
    DEBUG ((DEBUG_INFO, "PlatformInitAdvancedPreMem: BootGuard supported\n"));
    if (BootGuardInfo.BypassTpmInit == TRUE) {
      DEBUG ((DEBUG_INFO, "PlatformInitAdvancedPreMem: Bypass TPM init\n"));
      PcdSet8S (PcdTpm2InitializationPolicy, 0);
    }
  }
#endif // (FixedPcdGetBool (PcdBootGuardEnable) == 1) || (FixedPcdGetBool (PcdTxtEnable) == 1)

  //
  // If PTT presence detected, disable TPM self test policy
  //
  TpmInterfaceId = MmioRead8 ((UINTN)PcdGet64 (PcdTpmBaseAddress) + 0x30);
  PttFtifReg = MmioRead32 (R_PTT_TXT_STS_FTIF);

  if ((TpmInterfaceId != 0xFF) &&
      ((PttFtifReg & V_FTIF_FTPM_PRESENT) == V_FTIF_FTPM_PRESENT)) {

    DEBUG ((DEBUG_INFO, "PttFtifReg: %x\n", PttFtifReg));
    DEBUG ((DEBUG_INFO, "PTT detected\n"));
    DEBUG ((DEBUG_INFO, "PlatformInitAdvancedPreMem: Disabling TPM Self Test policy for PTT\n"));
    PcdSet8S (PcdTpm2SelfTestPolicy, 0);

  }

  if (BootMode == BOOT_ON_S3_RESUME) {
    PcdSet8S (PcdCRBIdleByPass, 0x0);
  }

  VariableSize = sizeof (ME_SETUP);
  Status = VariableServices->GetVariable (
                               VariableServices,
                               L"MeSetup",
                               &gMeSetupVariableGuid,
                               NULL,
                               &VariableSize,
                               &MeSetup
                               );
  ASSERT_EFI_ERROR (Status);
  if ((BootMode != BOOT_ON_S3_RESUME) && (MeSetup.ExtendMeMeasuredBoot == TRUE)) {
    PeiServicesNotifyPpi (&mPeiExtendMeMeasuredBootNotifyList);
  }

  ///
  /// Install Pre Memory PPIs
  ///
  Status = PeiServicesInstallPpi (&mPreMemPpiList[0]);
  ASSERT_EFI_ERROR (Status);

  PostCode(PLATFORM_INIT_PREMEM_EXIT);

  DisArmEcWdt ();
  DEBUG ((DEBUG_INFO, "Disarm EC WDT\n"));

#if FixedPcdGetBool(PcdResiliencyEnable) == 1
#ifdef MDEPKG_NDEBUG
  if (BootMode != BOOT_ON_S3_RESUME) {
    //
    // Arm EC WDT with a max timeout value (255s) to monitor system boot in release build.
    //
    ArmEcWdtWithTimeOut (0xFF);
  }
#endif
  //[-start-210823-IB18770037-add]//
#if FeaturePcdGet (PcdUseCrbEcFlag)
//[-end-210823-IB18770037-add]//
//[-start-210823-IB18770037-add]//
#endif
//[-start-210823-IB18770037-add]//
#endif

  return Status;
}

/**
  Platform Init before memory PEI module entry point

  @param[in]  FileHandle           Not used.
  @param[in]  PeiServices          General purpose services available to every PEIM.

  @retval     EFI_SUCCESS          The function completes successfully
  @retval     EFI_OUT_OF_RESOURCES Insufficient resources to create database
**/
EFI_STATUS
EFIAPI
PlatformInitAdvancedPreMemEntryPoint (
  IN       EFI_PEI_FILE_HANDLE  FileHandle,
  IN CONST EFI_PEI_SERVICES     **PeiServices
  )
{
  EFI_STATUS Status;
//[-start-200226-IB0531142-add]//  
  BOOLEAN    DefaultsRestored;
  
  DefaultsRestored = FALSE;  
//[-end-200226-IB0531142-add]//  
//[-start-200420-IB17800056-modify]//
#if 0
  BIOS_ID_IMAGE  BiosIdImage;

  Status = GetBiosId (&BiosIdImage);

  if (Status == EFI_SUCCESS) {
    DEBUG ((DEBUG_INFO, "+==================================================+\n"));
    DEBUG ((DEBUG_INFO, "| BIOS version [%s]  |\n", &BiosIdImage.BiosIdString));
    DEBUG ((DEBUG_INFO, "+==================================================+\n"));
  }

  ///
  /// Initialize Nvram to default when checksum computation is failed
  ///
  SecondaryNvRamInit ();
#endif
//[-end-200420-IB17800056-modify]//
//[-start-200226-IB0531142-add]// 
  ///
  /// Perform a checksum computation and verify if the checksum is correct. If the checksum is incorrect
  /// initialize all the CMOS location to their default values and recalculate the checksum.
  ///
  if (!PmcIsRtcBatteryGood ()) {
    DEBUG ((EFI_D_ERROR, "RTC battery is failure. Load CMOS default.\n"));
    InitCmos (TRUE, &DefaultsRestored);
  }

//[-end-200226-IB0531142-add]// 
//[-start-200831-IB17800088-add]//
  ///
  /// Install Stall PPI
  ///
  Status = InstallStallPpi();
  ASSERT_EFI_ERROR (Status);
//[-end-200831-IB17800088-add]//
  ///@todo it should be moved to Si Pkg.
  ///
  /// Do Early PCH init
  ///
  EarlyPlatformPchInit ();

  //
  // Install ME reset call back function.
  //
  RegisterMeReset ();

  ///
  /// Performing PlatformInitPreMem after PeiReadOnlyVariable2 PPI produced
  ///
  Status = PeiServicesNotifyPpi (&mPreMemNotifyList);

//[-start-200420-IB17800056-modify]//
  ///
  /// Do Chipset Platform init
  ///
  ChipsetPlatformInitPreMem();
//[-end-200420-IB17800056-modify]//

  ///
  /// After code reorangized, memorycallback will run because the PPI is already
  /// installed when code run to here, it is supposed that the InstallEfiMemory is
  /// done before.
  ///
  Status = PeiServicesNotifyPpi (&mMemDiscoveredNotifyList);

  return Status;
}
