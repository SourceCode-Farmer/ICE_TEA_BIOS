/** @file
  Init CMOS default.
;*****************************************************************************
;* Copyright (c) 2017 - 2018, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
**/
#include <InitCmos.h>
#include <Library/CmosLib.h>
#include <Library/PciLib.h>
#include <Library/PeiOemSvcChipsetLib.h>
#include <ChipsetCmos.h>
#include <Library/PchCycleDecodingLib.h>
#include <Library/IoLib.h>
#include <Library/MmPciLib.h>
#include <Library/PmcLib.h>
#include <Register/PchRegsLpc.h>
#include <Register/PmcRegs.h>
#include <Register/RtcRegs.h>

CMOS_DEFAULT_DATA mCmosDefaultTable[] = {
//
// Kernel CMOS bank 0
//
{ CmosCheckSum2E,             CMOS_DEFAULT_VALUE,  LENGTH_WORD, BANK_0 },
{ SmartBootWatchdogFlag,      CMOS_DEFAULT_VALUE,  LENGTH_BYTE, BANK_0 },
{ CmosTimeZone,         EFI_UNSPECIFIED_TIMEZONE,  LENGTH_WORD, BANK_0 },
{ CmosDaylight,               CMOS_DEFAULT_VALUE,  LENGTH_BYTE, BANK_0 },
{ CmosYearWakeUp,             CMOS_DEFAULT_VALUE,  LENGTH_BYTE, BANK_0 },
{ CurrentDebugGroup,          CMOS_DEFAULT_VALUE,  LENGTH_BYTE, BANK_0 | DO_NOT_INIT },
{ CurrentDebugCode,           CMOS_DEFAULT_VALUE,  LENGTH_BYTE, BANK_0 | DO_NOT_INIT },
{ BackupDebugGroup1,          CMOS_DEFAULT_VALUE,  LENGTH_BYTE, BANK_0 | DO_NOT_INIT },
{ BackupDebugCode1,           CMOS_DEFAULT_VALUE,  LENGTH_BYTE, BANK_0 | DO_NOT_INIT },
{ BackupDebugGroup2,          CMOS_DEFAULT_VALUE,  LENGTH_BYTE, BANK_0 | DO_NOT_INIT },
{ BackupDebugCode2,           CMOS_DEFAULT_VALUE,  LENGTH_BYTE, BANK_0 | DO_NOT_INIT },
{ BackupDebugGroup3,          CMOS_DEFAULT_VALUE,  LENGTH_BYTE, BANK_0 | DO_NOT_INIT },
{ BackupDebugCode3,           CMOS_DEFAULT_VALUE,  LENGTH_BYTE, BANK_0 | DO_NOT_INIT },
{ MonotonicCount,             CMOS_DEFAULT_VALUE, LENGTH_DWORD, BANK_0 },
{ SimpleBootFlag,             CMOS_DEFAULT_VALUE,  LENGTH_BYTE, BANK_0 },
{ CmosMonthWakeUp,            CMOS_DEFAULT_VALUE,  LENGTH_BYTE, BANK_0 },
{ CmosDayWakeUp,              CMOS_DEFAULT_VALUE,  LENGTH_BYTE, BANK_0 },
{ UserPassword,               CMOS_DEFAULT_VALUE,           12, BANK_0 | DO_NOT_INIT },
{ CmosTimeZoneWakeUp,   EFI_UNSPECIFIED_TIMEZONE,  LENGTH_WORD, BANK_0 },
{ CmosDaylightWakeUp,         CMOS_DEFAULT_VALUE,  LENGTH_BYTE, BANK_0 },
{ LastBootDevice,             CMOS_DEFAULT_VALUE,  LENGTH_BYTE, BANK_0 },
{ SupervisorPassword,         CMOS_DEFAULT_VALUE,           12, BANK_0 | DO_NOT_INIT },
//
// Chipset CMOS bank 0
//
{ RTC_ADDRESS_CENTURY,        CMOS_DEFAULT_VALUE,  LENGTH_BYTE, BANK_0 },
//{ TcoWatchdogTimerStatus,     CMOS_DEFAULT_VALUE,  LENGTH_BYTE, BANK_0 },
//
// Chipset CMOS bank 1
//
{ CMOS_CPU_RATIO_OFFSET,        CMOS_DEFAULT_VALUE,  LENGTH_BYTE, BANK_1 },
{ CMOS_TXT_REG,                 CMOS_DEFAULT_VALUE,  LENGTH_BYTE, BANK_1 },
{ CMOS_OC_SEND_BCLK_RAMP_MSG,   CMOS_DEFAULT_VALUE,  LENGTH_BYTE, BANK_1 },
{ CMOS_ICH_PORT80_OFFSET,       CMOS_DEFAULT_VALUE,  LENGTH_BYTE, BANK_1 },
{ CMOS_OPTIMUS_AUDIO_CODEC_CTL, CMOS_DEFAULT_VALUE,  LENGTH_BYTE, BANK_1 },
{ MEFlashReset,                 CMOS_DEFAULT_VALUE,  LENGTH_BYTE, BANK_1 },
{ PlatformConfigFlag,           CMOS_DEFAULT_VALUE,  LENGTH_BYTE, BANK_1 },
{ PlatformSettingFlag,          CMOS_DEFAULT_VALUE,  LENGTH_BYTE, BANK_1 },
{ S5LongRunTestFlag,            CMOS_DEFAULT_VALUE,  LENGTH_BYTE, BANK_1 },
{ ChipsetRecoveryFlag,          CMOS_DEFAULT_VALUE,  LENGTH_BYTE, BANK_1 },
{ XtuWdtStatus,                 CMOS_DEFAULT_VALUE,  LENGTH_BYTE, BANK_1 },
{ IccWatchdogFlag,              CMOS_DEFAULT_VALUE,  LENGTH_BYTE, BANK_1 },
{ OverclockingFlag,             CMOS_DEFAULT_VALUE,  LENGTH_BYTE, BANK_1 },
{ SetupNVFlag,                  CMOS_DEFAULT_VALUE,  LENGTH_BYTE, BANK_1 },
{ VmxSmxFlag,                   CMOS_DEFAULT_VALUE,  LENGTH_BYTE, BANK_1 },
{ MrcOemDebug1,                 CMOS_DEFAULT_VALUE,  LENGTH_BYTE, BANK_1 | DO_NOT_INIT },
{ MrcOemDebug2,                 CMOS_DEFAULT_VALUE,  LENGTH_BYTE, BANK_1 | DO_NOT_INIT },
{ 0xFF, 0xFF, 0xFF, 0xFF } // end of table
};

/**
  Funtion to get CMOS information from tables.

  @param[in]  BankNumber   The bank number of CMOS.
              CmosAddress  The CMOS address.

  @param[out] DefaultValue CMOS default value which is defined in the tables.
              Length       The CMOS data length.
              DoNotInit    If user do not want to init at the address.

  @retval     Status       EFI_SUCCESS    - the CMOS info is defined in the table.
                           EFI_NOT_FOUND  - the CMOS info is NOT defined in the table.
**/
EFI_STATUS
GetCmosAddressInfo (
  IN  UINT8              BankNumber,
  IN  UINT8              CmosAddress,
  OUT UINT32             *DefaultValue,
  OUT UINT8              *Length,
  OUT BOOLEAN            *DoNotInit
  )
{
  EFI_STATUS             Status;
  EFI_STATUS             OemStatus;
  CMOS_DEFAULT_DATA      *OemCmosTable;
  UINT8                  Index;

  Status = EFI_NOT_FOUND;
  OemStatus = EFI_NOT_FOUND;
  OemCmosTable = NULL;

  //
  // Get OEM CMOS default table from OEM service.
  //
  DEBUG_OEM_SVC ((DEBUG_INFO, "Pei OemChipsetServices Call: OemSvcGetInitCmosTable \n"));
  OemStatus = OemSvcGetInitCmosTable (&OemCmosTable);
  DEBUG_OEM_SVC ((DEBUG_INFO, "Pei OemChipsetServices OemSvcGetInitCmosTable Status: %r\n", OemStatus));

  //
  // Searching loop from OEM table
  //
  for (Index = 0; ; Index++) {
    if ((OemStatus == EFI_UNSUPPORTED) || (OemCmosTable[Index].CmosAddress == 0xFF)) {
      //
      // No OEM table or end of table
      //
      break;
    }
    if (((OemCmosTable[Index].ControlBitMap & BANK_NUMBER) == BankNumber) &&
      (OemCmosTable[Index].CmosAddress == CmosAddress)) {
      OemStatus = EFI_SUCCESS;
      break;
    }
  }
  if (OemStatus == EFI_SUCCESS) {
    //
    // OEM table first priority
    // update the return value and return.
    //
    *DefaultValue = OemCmosTable[Index].DefaultValue;
    *Length = OemCmosTable[Index].LengthInByte;
    if ((OemCmosTable[Index].ControlBitMap & INIT_CONTROL) == DO_NOT_INIT) {
      *DoNotInit = TRUE;
    } else {
      *DoNotInit = FALSE;
    }
    return EFI_SUCCESS;
  }

  //
  // Searching loop by Bank number and address
  //
  for (Index = 0; ; Index++) {
    if (mCmosDefaultTable[Index].CmosAddress == 0xFF) {
      //
      // End of table
      //
      break;
    }
    if (((mCmosDefaultTable[Index].ControlBitMap & BANK_NUMBER) == BankNumber) &&
      (mCmosDefaultTable[Index].CmosAddress == CmosAddress)) {
      Status = EFI_SUCCESS;
      break;
    }
  }
  if (Status == EFI_SUCCESS) {
    //
    // update the return value
    //
    *DefaultValue = mCmosDefaultTable[Index].DefaultValue;
    *Length = mCmosDefaultTable[Index].LengthInByte;
    if ((mCmosDefaultTable[Index].ControlBitMap & INIT_CONTROL) == DO_NOT_INIT) {
      *DoNotInit = TRUE;
    } else {
      *DoNotInit = FALSE;
    }
    return EFI_SUCCESS;
  }

  return EFI_NOT_FOUND;
}

/**
  Funtion to write the default data into CMOS.

  @param[in]  None.

  @param[out] None.

  @retval     None.
**/
VOID
InitCmosDefault (
//[-start-190610-IB16990033-add]//
  VOID
//[-end-190610-IB16990033-add]//
  )
{
  EFI_STATUS         Status;
  UINT8              Address;
  UINT8              Length;
  UINT32             DefaultValue;
  BOOLEAN            DoNotInitFlag;

  //
  // for loop bank 0 fill data
  //
  for (Address = CMOS_BANK_0_START; Address <= CMOS_BANK_0_END;) {
    //
    // Input bank# and address to get the DATA from two tables
    //
    Status = GetCmosAddressInfo (BANK_0, Address, &DefaultValue, &Length, &DoNotInitFlag);
    if (EFI_ERROR (Status)) {
      //
      // Not used / not defined in CMOS table. Clear to 0.
      //
      WriteCmos8 (Address, 0);
      Address++;
    } else {
      if (DoNotInitFlag) {
        Address = Address + Length;
        //
        // Skip.
        //
        continue;
      }
      switch (Length) {
      case LENGTH_BYTE:
          WriteCmos8 (Address, (UINT8) DefaultValue);
          Address = Address + Length;
        break;

      case LENGTH_WORD:
          WriteCmos16 (Address, (UINT16) DefaultValue);
          Address = Address + Length;
        break;

      case LENGTH_DWORD:
          WriteCmos32 (Address, DefaultValue);
          Address = Address + Length;
        break;

      default:
        //
        // Other length
        //
        break;
      }
    }
  }

  //
  // for loop bank 1 fill data
  //
  for (Address = CMOS_BANK_1_START; Address <= CMOS_BANK_1_END;) {
    //
    // Input bank# and address to get the DATA from two tables
    //
    Status = GetCmosAddressInfo (BANK_1, Address, &DefaultValue, &Length, &DoNotInitFlag);
    if (EFI_ERROR (Status)) {
      //
      // Not used / not defined in table, clear to 0.
      //
      WriteExtCmos8 (R_XCMOS_INDEX, R_XCMOS_DATA, Address, 0);
      Address++;
    } else {
      if (DoNotInitFlag) {
        Address = Address + Length;
        //
        // Skip.
        //
        continue;
      }
      switch (Length) {
      case LENGTH_BYTE:
          WriteExtCmos8 (R_XCMOS_INDEX, R_XCMOS_DATA, Address, (UINT8) DefaultValue);
          Address = Address + Length;
        break;

      case LENGTH_WORD:
          WriteExtCmos16 (R_XCMOS_INDEX, R_XCMOS_DATA, Address, (UINT16) DefaultValue);
          Address = Address + Length;
        break;

      case LENGTH_DWORD:
          WriteExtCmos32 (R_XCMOS_INDEX, R_XCMOS_DATA, Address, DefaultValue);
          Address = Address + Length;
        break;

      default:
        //
        // Other length
        //
        break;
      }
    }
  }

  return;
}

/**
  Funtion to Initialize the CMOS.

  Checks the presence of CMOS battery, else it initialize CMOS to default.
  Perform a checksum computation and verify if the checksum is correct.
  If the input parameter ForceInit is TRUE, initialize all the CMOS
  location to their default values

  @param[in]  ForceInit         A boolean variable to initialize the CMOS to its default
                                without checking the RTC_PWR_STS or verifying the checksum.

  @param[out] DefaultsRestored  A boolean variable to indicate if the defaults were restored

  @retval     Status
**/
RETURN_STATUS
EFIAPI
InitCmos (
  IN  BOOLEAN     ForceInit,
  OUT BOOLEAN     *DefaultsRestored
  )
{
  EFI_STATUS         Status;

  Status = ValidateCmosChecksum ();
  if (Status != EFI_SUCCESS) {
    //
    // Corrupted checksum
    //
    ForceInit = TRUE;
  }

  if (ForceInit) {
    InitCmosDefault ();
    *DefaultsRestored = TRUE;
    return EFI_SUCCESS;
  }

  *DefaultsRestored = FALSE;
  return EFI_SUCCESS;
}

