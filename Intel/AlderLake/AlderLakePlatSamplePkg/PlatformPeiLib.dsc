## @file
# platform build option configuration file.
#
# @copyright
#  INTEL CONFIDENTIAL
#  Copyright 2020 - 2021 Intel Corporation.
#
#  The source code contained or described herein and all documents related to the
#  source code ("Material") are owned by Intel Corporation or its suppliers or
#  licensors. Title to the Material remains with Intel Corporation or its suppliers
#  and licensors. The Material may contain trade secrets and proprietary and
#  confidential information of Intel Corporation and its suppliers and licensors,
#  and is protected by worldwide copyright and trade secret laws and treaty
#  provisions. No part of the Material may be used, copied, reproduced, modified,
#  published, uploaded, posted, transmitted, distributed, or disclosed in any way
#  without Intel's prior express written permission.
#
#  No license under any patent, copyright, trade secret or other intellectual
#  property right is granted to or conferred upon you by disclosure or delivery
#  of the Materials, either expressly, by implication, inducement, estoppel or
#  otherwise. Any license under such intellectual property rights must be
#  express and approved by Intel in writing.
#
#  Unless otherwise agreed by Intel in writing, you may not remove or alter
#  this notice or any other notice embedded in Materials by Intel or
#  Intel's suppliers or licensors in any way.
#
#  This file contains a 'Sample Driver' and is licensed as such under the terms
#  of your license agreement with Intel or your vendor. This file may be modified
#  by the user, subject to the additional terms of the license agreement.
#
# @par Specification
##

[LibraryClasses.IA32]

  PeiGetFvInfoLib|$(PLATFORM_FULL_PACKAGE)/Library/PeiGetFvInfoLib/PeiGetFvInfoLib.inf

  SerialPortParameterLib|$(PLATFORM_FULL_PACKAGE)/Library/PeiSerialPortParameterLib/PeiSerialPortParameterLib.inf
!if gIntelFsp2WrapperTokenSpaceGuid.PcdFspModeSelection == 1
  PeiPolicyUpdateLib|$(PLATFORM_FULL_PACKAGE)/Library/PeiPolicyUpdateLib/PeiPolicyUpdateLibFsp.inf
  PeiPolicyDebugLib|$(PLATFORM_FULL_PACKAGE)/Library/PeiPolicyDebugLib/PeiPolicyDebugLibFsp.inf
!else
  PeiPolicyUpdateLib|$(PLATFORM_FULL_PACKAGE)/Library/PeiPolicyUpdateLib/PeiPolicyUpdateLib.inf
  PeiPolicyDebugLib|$(PLATFORM_FULL_PACKAGE)/Library/PeiPolicyDebugLib/PeiPolicyDebugLib.inf
!endif



  PeiWdtAppLib|$(PLATFORM_FULL_PACKAGE)/Library/PeiWdtAppLib/PeiWdtAppLib.inf


#
# TBT
#
!if gPlatformModuleTokenSpaceGuid.PcdDTbtEnable == TRUE
  PeiDTbtInitLib|$(PLATFORM_FEATURES_PATH)/Tbt/Library/Private/PeiDTbtInitLib/PeiDTbtInitLib.inf
  PeiDTbtPolicyLib|$(PLATFORM_FEATURES_PATH)/Tbt/Library/PeiDTbtPolicyLib/PeiDTbtPolicyLib.inf
!else
  PeiDTbtInitLib|$(PLATFORM_FEATURES_PATH)/Tbt/Library/Private/PeiDTbtInitLibNull/PeiDTbtInitLibNull.inf
  PeiDTbtPolicyLib|$(PLATFORM_FEATURES_PATH)/Tbt/Library/PeiDTbtPolicyLibNull/PeiDTbtPolicyLibNull.inf
!endif

[LibraryClasses.IA32.SEC]
  SerialPortParameterLib|$(PLATFORM_FULL_PACKAGE)/Library/BaseSerialPortParameterLibPcd/BaseSerialPortParameterLibPcd.inf

[LibraryClasses.IA32.PEIM]

  FspWrapperExtractGuidedLib|$(PLATFORM_FULL_PACKAGE)/FspWrapper/Library/PeiFspWrapperExtractGuidedLib/PeiFspWrapperExtractGuidedLib.inf

#
# PEI Boot State Library Instance
#
  PeiBootStateLib|$(PLATFORM_FULL_PACKAGE)/Library/PeiBootStateLib/PeiBootStateLib.inf

#
# Telemetry.
#
  TelemetryFviLib|$(PLATFORM_FULL_PACKAGE)/Telemetry/Library/PeiTelemetryFviLib/PeiTelemetryFvilib.inf

!if $(TARGET) == DEBUG
  SerialIoUartDebugPropertyLib|$(PLATFORM_FULL_PACKAGE)/Library/SerialIoUartDebugPropertyLib/PeiSerialIoUartDebugPropertyLib.inf
  !if gPlatformModuleTokenSpaceGuid.PcdSerialPortEnable == TRUE
    SerialPortLib|$(PLATFORM_FULL_PACKAGE)/Library/BaseSerialPortLib/BaseSerialPortLib.inf
  !else
    SerialPortLib|MdePkg/Library/BaseSerialPortLibNull/BaseSerialPortLibNull.inf
  !endif
  DebugPrintErrorLevelLib|$(PLATFORM_FULL_PACKAGE)/Library/PeiDxeSmmDebugPrintErrorLevelLib/PeiDxeSmmDebugPrintErrorLevelLib.inf
!endif
