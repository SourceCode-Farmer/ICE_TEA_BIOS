/** @file

;******************************************************************************
;* Copyright (c) 2012 - 2021, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/
/** @file
  The file contains SA related setup options

@copyright
  INTEL CONFIDENTIAL
  Copyright 2010 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains a 'Sample Driver' and is licensed as such under the terms
  of your license agreement with Intel or your vendor. This file may be modified
  by the user, subject to the additional terms of the license agreement.

@par Specification Reference:
**/

//[-start-191111-IB10189001-remove]//
// #include <SetupPrivate.h>
//[-end-191111-IB10189001-remove]//
#include "OemSetup.h"
#include "SaSetup.h"
#include "PlatformBoardId.h"
#include "CpuRegs.h"
#include "DimmInfo.h"
#include <Library/UefiBootServicesTableLib.h>
#include <Library/UefiRuntimeServicesTableLib.h>
#include <Library/CpuPlatformLib.h>
#include <Protocol/SaPolicy.h>
#include <Protocol/SmbusHc.h>
#include <Protocol/MemInfo.h>
//[-start-200215-IB06462109-add]//
#include <SetupUtility.h>
//[-end-200215-IB06462109-add]//
#include <Protocol/GopPolicy.h>
#if FixedPcdGetBool(PcdITbtEnable) == 1
#include "TcssDataHob.h"
#endif
#include <IpuDataHob.h>
#include <Library/CpuPcieInfoFruLib.h>
#include <Library/CpuPcieInitCommon.h>
#include <CpuPcieHob.h>
#include <Library/CpuRegbarAccessLib.h>
#include <VmdInfoHob.h>
#include <Register/SataRegs.h>
#include <Library/SataSocLib.h>
#include <Library/GraphicsInfoLib.h>
#include <Register/SaPcieDmiRegs.h>
#include <Library/PchPciBdfLib.h>
//[-start-191111-IB10189001-add]//
#include <IndustryStandard/SmBios.h>
//[-end-191111-IB10189001-add]//

typedef union {
  struct {
    UINT32  Low;
    UINT32  High;
  } Data32;
  UINT64 Data;
} UINT64_STRUCT;

static EFI_HII_HANDLE     gHiiHandle;
//[-start-191111-IB10189001-add]//
SA_SETUP                  mSaSetup;
//[-end-191111-IB10189001-add]//

VOID
UpdateDmiInfo (
  EFI_HII_HANDLE HiiHandle,
  UINT16         Class
  );

EFI_STATUS
EFIAPI
UpdateVmdInfo (
  EFI_HII_HANDLE HiiHandle
  );
#define NB_MIN(a, b)  (((a) < (b)) ? (a) : (b))

EFI_STRING_ID  DimmSizeString[SLOT_NUM] = {
  STRING_TOKEN (STR_DIMM_SIZE_MC0_CH0_SLT0_VALUE),
  STRING_TOKEN (STR_DIMM_SIZE_MC0_CH0_SLT1_VALUE),
  STRING_TOKEN (STR_DIMM_SIZE_MC0_CH1_SLT0_VALUE),
  STRING_TOKEN (STR_DIMM_SIZE_MC0_CH1_SLT1_VALUE),
  STRING_TOKEN (STR_DIMM_SIZE_MC0_CH2_SLT0_VALUE),
  STRING_TOKEN (STR_DIMM_SIZE_MC0_CH2_SLT1_VALUE),
  STRING_TOKEN (STR_DIMM_SIZE_MC0_CH3_SLT0_VALUE),
  STRING_TOKEN (STR_DIMM_SIZE_MC0_CH3_SLT1_VALUE),
  STRING_TOKEN (STR_DIMM_SIZE_MC1_CH0_SLT0_VALUE),
  STRING_TOKEN (STR_DIMM_SIZE_MC1_CH0_SLT1_VALUE),
  STRING_TOKEN (STR_DIMM_SIZE_MC1_CH1_SLT0_VALUE),
  STRING_TOKEN (STR_DIMM_SIZE_MC1_CH1_SLT1_VALUE),
  STRING_TOKEN (STR_DIMM_SIZE_MC1_CH2_SLT0_VALUE),
  STRING_TOKEN (STR_DIMM_SIZE_MC1_CH2_SLT1_VALUE),
  STRING_TOKEN (STR_DIMM_SIZE_MC1_CH3_SLT0_VALUE),
  STRING_TOKEN (STR_DIMM_SIZE_MC1_CH3_SLT1_VALUE)
};

EFI_STRING_ID  RankInDimmString[SLOT_NUM] = {
  STRING_TOKEN (STR_DIMM_MC0_CH0_SLT0_RANK_VALUE),
  STRING_TOKEN (STR_DIMM_MC0_CH0_SLT1_RANK_VALUE),
  STRING_TOKEN (STR_DIMM_MC0_CH1_SLT0_RANK_VALUE),
  STRING_TOKEN (STR_DIMM_MC0_CH1_SLT1_RANK_VALUE),
  STRING_TOKEN (STR_DIMM_MC0_CH2_SLT0_RANK_VALUE),
  STRING_TOKEN (STR_DIMM_MC0_CH2_SLT1_RANK_VALUE),
  STRING_TOKEN (STR_DIMM_MC0_CH3_SLT0_RANK_VALUE),
  STRING_TOKEN (STR_DIMM_MC0_CH3_SLT1_RANK_VALUE),
  STRING_TOKEN (STR_DIMM_MC1_CH0_SLT0_RANK_VALUE),
  STRING_TOKEN (STR_DIMM_MC1_CH0_SLT1_RANK_VALUE),
  STRING_TOKEN (STR_DIMM_MC1_CH1_SLT0_RANK_VALUE),
  STRING_TOKEN (STR_DIMM_MC1_CH1_SLT1_RANK_VALUE),
  STRING_TOKEN (STR_DIMM_MC1_CH2_SLT0_RANK_VALUE),
  STRING_TOKEN (STR_DIMM_MC1_CH2_SLT1_RANK_VALUE),
  STRING_TOKEN (STR_DIMM_MC1_CH3_SLT0_RANK_VALUE),
  STRING_TOKEN (STR_DIMM_MC1_CH3_SLT1_RANK_VALUE)
};

EFI_STRING_ID  DimmMfgString[SLOT_NUM] = {
  STRING_TOKEN (STR_DIMM_MC0_CH0_SLT0_MFG_VALUE),
  STRING_TOKEN (STR_DIMM_MC0_CH0_SLT1_MFG_VALUE),
  STRING_TOKEN (STR_DIMM_MC0_CH1_SLT0_MFG_VALUE),
  STRING_TOKEN (STR_DIMM_MC0_CH1_SLT1_MFG_VALUE),
  STRING_TOKEN (STR_DIMM_MC0_CH2_SLT0_MFG_VALUE),
  STRING_TOKEN (STR_DIMM_MC0_CH2_SLT1_MFG_VALUE),
  STRING_TOKEN (STR_DIMM_MC0_CH3_SLT0_MFG_VALUE),
  STRING_TOKEN (STR_DIMM_MC0_CH3_SLT1_MFG_VALUE),
  STRING_TOKEN (STR_DIMM_MC1_CH0_SLT0_MFG_VALUE),
  STRING_TOKEN (STR_DIMM_MC1_CH0_SLT1_MFG_VALUE),
  STRING_TOKEN (STR_DIMM_MC1_CH1_SLT0_MFG_VALUE),
  STRING_TOKEN (STR_DIMM_MC1_CH1_SLT1_MFG_VALUE),
  STRING_TOKEN (STR_DIMM_MC1_CH2_SLT0_MFG_VALUE),
  STRING_TOKEN (STR_DIMM_MC1_CH2_SLT1_MFG_VALUE),
  STRING_TOKEN (STR_DIMM_MC1_CH3_SLT0_MFG_VALUE),
  STRING_TOKEN (STR_DIMM_MC1_CH3_SLT1_MFG_VALUE)
};

EFI_STRING_ID  DimmStatusString[SLOT_NUM] = {
  STRING_TOKEN (STR_DIMM_MC0_CH0_SLT0_STATUS),
  STRING_TOKEN (STR_DIMM_MC0_CH0_SLT1_STATUS),
  STRING_TOKEN (STR_DIMM_MC0_CH1_SLT0_STATUS),
  STRING_TOKEN (STR_DIMM_MC0_CH1_SLT1_STATUS),
  STRING_TOKEN (STR_DIMM_MC0_CH2_SLT0_STATUS),
  STRING_TOKEN (STR_DIMM_MC0_CH2_SLT1_STATUS),
  STRING_TOKEN (STR_DIMM_MC0_CH3_SLT0_STATUS),
  STRING_TOKEN (STR_DIMM_MC0_CH3_SLT1_STATUS),
  STRING_TOKEN (STR_DIMM_MC1_CH0_SLT0_STATUS),
  STRING_TOKEN (STR_DIMM_MC1_CH0_SLT1_STATUS),
  STRING_TOKEN (STR_DIMM_MC1_CH1_SLT0_STATUS),
  STRING_TOKEN (STR_DIMM_MC1_CH1_SLT1_STATUS),
  STRING_TOKEN (STR_DIMM_MC1_CH2_SLT0_STATUS),
  STRING_TOKEN (STR_DIMM_MC1_CH2_SLT1_STATUS),
  STRING_TOKEN (STR_DIMM_MC1_CH3_SLT0_STATUS),
  STRING_TOKEN (STR_DIMM_MC1_CH3_SLT1_STATUS)
};

//[-start-191111-IB10189001-add]//
//== This region code refer to RC platform code "DimmInfo.h" and "DimmInfo.c"
SMBIOS_MEMORY_DEVICE_TYPE_LIST MemoryTypeList[] = {
  {0x18,"DDR5"},
  {0x1A,"DDR4"},
  {0x1D,"LPDDR5"},
  {0x1E,"LPDDR4"},
  {0x02,"UnKnown"}
};

UINT8 MemoryTypeListSize = sizeof (MemoryTypeList) / sizeof (MemoryTypeList[0]);


//
// The order must match DDR_TYPE_xxx defines in MemInfo.h
//
CHAR8 *MemoryTypeStr[] = {
  "DDR4",
  "DDR5",
  "LPDDR5",
  "LPDDR4",
  "UnKnown"
};

//[-start-211116-IB18410140-modify]//
MEMORY_MODULE_MANUFACTURER_LIST MemoryModuleManufacturerList[] = {
  {0x2c00, "Micron"},
  {0xad00, "SK Hynix"},
  {0xce00, "Samsung"},
  {0x4f01, "Transcend"},
  {0x9801, "Kingston"},
  {0xfe02, "Elpida"},
  {0x910a, "CXMT"}
};
//[-end-211116-IB18410140-modify]//

UINT8 MemoryModuleManufacturerListSize = sizeof (MemoryModuleManufacturerList) / sizeof (MemoryModuleManufacturerList[0]);
//[-end-191111-IB10189001-add]//

VOID
InitSaStrings (
  EFI_HII_HANDLE HiiHandle,
  UINT16         Class
  )
{
  UINT8                           MemoryType;
  UINT8                           Profile;
  UINT64                          MemorySize;
  UINT16                          DdrFrequency;
  UINT16                          VendorId;
  EFI_STATUS                      Status;
  UINTN                           SaSetupVariableSize;
  UINTN                           SetupVolVariableSize;
  UINT32                          Value32;
  UINTN                           Slot;
  UINT8                           w;
  UINT8                           x;
  UINT8                           y;
  UINT8                           z;
  UINT8                           Rp0;
  UINT8                           Rpn;
  UINT16                          Data16;
  UINT8                           NodeIndex;
  UINT8                           ChannelIndex;
  UINT8                           DimmIndex;
  UINT8                           MfgIdIndex;
  UINT8                           SkipStringUpdate;

  MEM_INFO_PROTOCOL               *MemInfoProtocol;
  MEMORY_INFO_DATA                *MemInfoData;
  MEMORY_TIMING                   *Timing;
  SA_POLICY_PROTOCOL              *SaPolicy;
  CHAR8                           *DimmStatusStr;
  UINT32                          SaVariableAttributes;
  UINT32                          SetupVolAttributes;
  GRAPHICS_DXE_CONFIG             *GraphicsDxeConfig;
  UINT8                           CpuSku;
  UINT32                          EdramSize;
  UINT64                          IgdPciD2F0RegBase;
  UINT8                           IndexOfMemTypeList;
  IPU_DATA_HOB                    *IpuDataHob;
//[-start-200215-IB06462109-add]//
  KERNEL_CONFIGURATION            KernelConfig;
//[-end-200215-IB06462109-add]//
  UINT32                          DssmClockSel;
  UINT64                          GttMmAdr;
  UINT64_STRUCT                   Data64;
#if FixedPcdGetBool(PcdITbtEnable) == 1
  TCSS_DATA_HOB         *TcssHob;
#endif


  if ((Class != MAIN_FORM_SET_CLASS) && (Class != ADVANCED_FORM_SET_CLASS)) {
    return;
  }

  DEBUG ((DEBUG_INFO, "<InitSaStrings>"));

  CpuSku = GetCpuSku();
  Status = gBS->LocateProtocol (&gSaPolicyProtocolGuid, NULL, (VOID **) &SaPolicy);
  ASSERT_EFI_ERROR (Status);

  Status = GetConfigBlock ((VOID *)SaPolicy, &gGraphicsDxeConfigGuid, (VOID *)&GraphicsDxeConfig);
  ASSERT_EFI_ERROR (Status);

//[-start-200215-IB06462109-add]//
  Status = GetKernelConfiguration (&KernelConfig);
  if (EFI_ERROR (Status)) {
    return;
  }
//[-end-200215-IB06462109-add]//
  SaSetupVariableSize = sizeof (SA_SETUP);
  Status = gRT->GetVariable (
                  L"SaSetup",
                  &gSaSetupVariableGuid,
                  &SaVariableAttributes,
                  &SaSetupVariableSize,
                  &mSaSetup
                  );
  DEBUG((DEBUG_INFO, "GetVariable 'SaSetup' Status = %r", Status));

  SetupVolVariableSize = sizeof (SETUP_VOLATILE_DATA);
  Status = gRT->GetVariable (
                  L"SetupVolatileData",
                  &gSetupVariableGuid,
                  &SetupVolAttributes,
                  &SetupVolVariableSize,
                  &mSetupVolatileData
                  );
  ASSERT_EFI_ERROR (Status);
  if (EFI_ERROR (Status)) {
    return;
  }


  //
  // Get the Memory Info HOB Protocol if it exists.
  //
  Status = gBS->LocateProtocol (&gMemInfoProtocolGuid, NULL, (VOID **) &MemInfoProtocol);
  ASSERT_EFI_ERROR (Status);
  if (EFI_ERROR (Status)) {
    return;
  }

  MemInfoData = &MemInfoProtocol->MemInfoData;

  MemoryType    = MemInfoData->DdrType;
  DdrFrequency  = MemInfoData->ddrFreq;
  Profile       = MemInfoData->Profile;
  Timing        = &MemInfoData->Timing[Profile];

  mSaSetup.XmpProfileEnable                 = MemInfoData->XmpProfileEnable;
  mSaSetup.DynamicMemoryBoostTrainingFailed = MemInfoData->DynamicMemoryBoostTrainingFailed;
  Status = gRT->SetVariable (
                  L"SaSetup",
                  &gSaSetupVariableGuid,
                  SaVariableAttributes,
                  SaSetupVariableSize,
                  &mSaSetup
                  );

  //
  // Update the memory size string for main page and chipset page
  //
  if ((Class == MAIN_FORM_SET_CLASS) || (Class == ADVANCED_FORM_SET_CLASS)) {
    MemorySize = MemInfoData->memSize;
    InitString (HiiHandle, STRING_TOKEN (STR_MEMORY_SIZE_VALUE), L"%5ld MB", MemorySize);
    InitString (HiiHandle, STRING_TOKEN (STR_MEMORY_FREQ_VALUE), L"%5ld MHz", DdrFrequency);

    //
    // MC0_MRC_REVISION_REG - MRC version
    //
    Value32 = MmioRead32 ((UINTN) PcdGet64 (PcdMchBaseAddress) + 0xD834);
    if (Value32 == 0xFFFFFFFF) {
      // If MC0 is not populated, read it from MC1
      Value32 = MmioRead32 ((UINTN) PcdGet64 (PcdMchBaseAddress) + 0x1D834);
    }
    x       = (UINT8) ((Value32 & 0xFF000000) >> 24);
    y       = (UINT8) ((Value32 & 0xFF0000) >> 16);
    z       = (UINT8) ((Value32 & 0xFF00) >> 8);
    w       = (UINT8) (Value32 & 0xFF);

    InitString (HiiHandle, STRING_TOKEN (STR_MRC_REV_VALUE), L"%d.%d.%d.%d", x, y, z, w);
  }

  IgdPciD2F0RegBase = PCI_SEGMENT_LIB_ADDRESS (SA_SEG_NUM, GetIgdBusNumber(), GetIgdDevNumber(), GetIgdFuncNumber(), 0);
  Data64.Data32.High = (PciSegmentRead32 (IgdPciD2F0RegBase + GetGttMmAdrOffset() + 4)) & 0xFFFFFFFF;
  Data64.Data32.Low  = (PciSegmentRead32 (IgdPciD2F0RegBase + GetGttMmAdrOffset())) & 0xFFFFFFF0;
  GttMmAdr = (UINT64)Data64.Data;

//[-start-191111-IB10189001-modify]//
    // if (PciSegmentRead16 (IgdPciD2F0RegBase + 0x2) != 0xFFFF) {
    if (((KernelConfig.BootType == EFI_BOOT_TYPE) || (KernelConfig.BootType == DUAL_BOOT_TYPE)) && (PciSegmentRead16 (IgdPciD2F0RegBase + 0x2) != 0xFFFF)) {
//[-end-191111-IB10189001-modify]//
    InitString (
      HiiHandle,
      STRING_TOKEN (STR_CHIP_IGFX_GOP_REV_VALUE),
      L"%s",
      &GraphicsDxeConfig->GopVersion
    );
  }

  //
  // GT frequency setting
  //
  if ((PciSegmentRead16 (IgdPciD2F0RegBase + 0x2) != 0xFFFF)) {
    Value32 = MmioRead32 ((UINTN) PcdGet64 (PcdMchBaseAddress) + 0x5998);
    Rpn = (UINT8) ((Value32 & 0xFF0000) >> 16);
    Rp0 = (UINT8) (Value32 & 0xFF);
    InitString (HiiHandle, STRING_TOKEN (STR_GT_FREQ_HELP), L"Maximum GT frequency limited by the user. Choose between %dMHz (RPN) and %dMHz (RP0). Value beyond the range will be clipped to min/max supported by SKU", Rpn*50, Rp0*50);
  }

  //
  // Populate CdClockSelector variable here.
  //
  if (!IgfxCmdRegEnabled()) {
    ///
    /// Enable Bus Initiator and Memory access on 0:2:0
    ///
    PciSegmentOr16 (IgdPciD2F0RegBase + PCI_COMMAND_OFFSET, (BIT2 | BIT1));
  }
  DssmClockSel = GetIgdDssmRefClockFreqValue (GttMmAdr);
  DEBUG ((DEBUG_INFO, "DssmClockSel = %d\n ",DssmClockSel));
  switch (DssmClockSel) {
    case 0: // 24Mhz
      mSetupVolatileData.CdClockSelector = 2;
      break;
    case 1: // 19.2MHz
      mSetupVolatileData.CdClockSelector = 0;
      break;
    case 2: // 38.4MHz
      mSetupVolatileData.CdClockSelector = 1;
      break;
    case 3: // 25Mhz
      mSetupVolatileData.CdClockSelector = 3;
      break;
    default:
      mSetupVolatileData.CdClockSelector = 2;
      break;
  }
  DEBUG ((DEBUG_INFO, "mSaSetupVolatileDataetup.CdClockSelector = %d\n",mSetupVolatileData.CdClockSelector));

  Status = gRT->SetVariable (
              L"SetupVolatileData",
              &gSetupVariableGuid,
              SetupVolAttributes,
              SetupVolVariableSize,
              &mSetupVolatileData
              );

//[-start-190709-16990078-modify]//
  if (Class == MAIN_FORM_SET_CLASS || Class == ADVANCED_FORM_SET_CLASS) {
//[-end-190709-16990078-modify]//
    gHiiHandle = HiiHandle;
    Data16 = PciSegmentRead16 (IgdPciD2F0RegBase + 0x2);
    if (Data16 != 0xFFFF) {
      InitString (gHiiHandle, STRING_TOKEN (STR_PROCESSOR_GT_VALUE), L"0x%X", Data16);
    }

    //
    // EDRAM Size display
    //
    if (IsEdramEnable ()) {
      EdramSize = 0;
      if ((CpuSku == EnumCpuUlt) || (CpuSku == EnumCpuUlx)) {
        EdramSize = 64;
      } else if ((CpuSku == EnumCpuTrad) || (CpuSku == EnumCpuHalo)) {
        EdramSize = 128;
      }
      InitString (
        HiiHandle,
        STRING_TOKEN (STR_EDRAM_SIZE_VALUE),
        L"%d MB",
        EdramSize
      );
    }

  }

  if (Class == ADVANCED_FORM_SET_CLASS) {
    //
    //Find the memory type in the List
    //
    for (IndexOfMemTypeList = 0; IndexOfMemTypeList < MemoryTypeListSize; IndexOfMemTypeList++) {
      if (MemoryType == MemoryTypeList[IndexOfMemTypeList].DdrTypeNum) {
        break;
      }
    }

    SetupVolVariableSize = sizeof (SETUP_VOLATILE_DATA);
    Status = gRT->GetVariable (
                    L"SetupVolatileData",
                    &gSetupVariableGuid,
                    &SetupVolAttributes,
                    &SetupVolVariableSize,
                    &mSetupVolatileData
                    );
    ASSERT_EFI_ERROR (Status);
    if (!EFI_ERROR (Status)) {
      Slot = 0;
      SkipStringUpdate = FALSE;
      mSetupVolatileData.MemoryType = MemoryType;
      DEBUG((EFI_D_INFO, "MemoryType : %X\n", MemoryType));
      for (NodeIndex = 0; NodeIndex < NODE_NUM; NodeIndex++) {
        for (ChannelIndex = 0; ChannelIndex < CH_NUM; ChannelIndex++) {
          //
          // Skip the strings init for DDR4 and DDR5 when Channel is equal or above one
          //
          if (((ChannelIndex > 0) && (mSetupVolatileData.MemoryType == MRC_DDR_TYPE_DDR4)) || \
             ((ChannelIndex > 1) && (mSetupVolatileData.MemoryType == MRC_DDR_TYPE_DDR5))) {
            SkipStringUpdate = TRUE;
          }
          for (DimmIndex = 0; DimmIndex < DIMM_NUM; DimmIndex++,Slot++) {
            //
            // Skip the stings init for DDR5, LPDDR5 and LPDRR4 when Dimm is above zero
            //
            if ((DimmIndex > 0) && (mSetupVolatileData.MemoryType != MRC_DDR_TYPE_DDR4)
                                && (mSetupVolatileData.MemoryType != MRC_DDR_TYPE_DDR5)) {
              SkipStringUpdate = TRUE;
            }
            if (!SkipStringUpdate) {
              InitString (
                HiiHandle,
                DimmSizeString[Slot],
                L"%4ld MB (%a)",
                MemInfoData->dimmSize[Slot],
                (mSetupVolatileData.MemoryType < MRC_DDR_TYPE_UNKNOWN) ? MemoryTypeStr[mSetupVolatileData.MemoryType] : "Unknown"
                );
              if (MemInfoData->DimmStatus[Slot] < DIMM_NOT_PRESENT) {
                mSetupVolatileData.DimmPresent[Slot] = 1;
                InitString (
                  HiiHandle,
                  RankInDimmString[Slot],
                  L"%1d",
                  MemInfoData->RankInDimm[Slot]
                  );
                DimmStatusStr = (MemInfoData->DimmStatus[Slot] == DIMM_DISABLED) ? "Populated & Disabled" : "Populated & Enabled";
                /**
                  Get the Memory Module Vendor JEDEC ID
                  Byte 117-118 for DDR3/LPDDR3 and byte 320-321 for DDR4
                  It's from first byte of SPD buffer.
                **/
                VendorId = *(UINT16 *) (UINTN) (MemInfoData->DimmsSpdData[Slot]);
                VendorId &= ~(BIT7);  // Clear the parity bit
                for (MfgIdIndex = 0; MfgIdIndex < MemoryModuleManufacturerListSize; MfgIdIndex++) {
                  if (VendorId == MemoryModuleManufacturerList[MfgIdIndex].MfgId) {
                    InitString (
                      HiiHandle,
                      DimmMfgString[Slot],
                      L"%a",
                      MemoryModuleManufacturerList[MfgIdIndex].String
                     );
                    break;
                  }
                }
              } else {
                mSetupVolatileData.DimmPresent[Slot] = 0;
                DimmStatusStr = "Not Populated / Disabled";
              }

              InitString (
                HiiHandle,
                DimmStatusString[Slot],
                L"%a",
                DimmStatusStr
                );
            } else {
              SkipStringUpdate = FALSE;
            }
          }
        }
      }
      //
      // VT-d status report
      //
      InitString (
        HiiHandle,
        STRING_TOKEN (STR_SA_VTD_VALUE),
        mSetupVolatileData.VTdAvailable ? L"Supported" : L"Unsupported"
        );

      //
      // IPU BIOS Setup option display
      //
      IpuDataHob = (IPU_DATA_HOB *) GetFirstGuidHob (&gIpuDataHobGuid);
      if (IpuDataHob != NULL) {
        mSetupVolatileData.IpuSupport = 1;
      } else {
        mSetupVolatileData.IpuSupport = 0;
      }

      Status = gRT->SetVariable (
                      L"SetupVolatileData",
                      &gSetupVariableGuid,
                      SetupVolAttributes,
                      SetupVolVariableSize,
                      &mSetupVolatileData
                      );
      ASSERT_EFI_ERROR (Status);
    } // if SetupVolatileData found
    //
    // Update the tCL, tRCD, tRP and tRAS string with data obtained from MemInfo protocol
    //
    InitString (
      HiiHandle,
      STRING_TOKEN (STR_MEMORY_VDD_VALUE),
      L"%d",
      MemInfoData->VddVoltage[Profile]
      );

    InitString (
      HiiHandle,
      STRING_TOKEN (STR_MEMORY_TIMINGS_VALUE),
      L"%d-%d-%d-%d",
      Timing->tCL,
      Timing->tRCDtRP,
      Timing->tRCDtRP,
      Timing->tRAS
      );


  } // if ADVANCED_FORM_SET_CLASS

  //
  // Locate Platform Info Protocol.
  //
  if (PcdGet8 (PcdPlatformType) == TypeTrad) {
    UpdateDmiInfo (HiiHandle, Class);
  }


//
//  IOM, TBT firmware version and TBT IMR status
//
#if FixedPcdGetBool(PcdITbtEnable) == 1
  TcssHob = (TCSS_DATA_HOB *) GetFirstGuidHob (&gTcssHobGuid);
  if (TcssHob != NULL) {
    InitString (
      HiiHandle,
      STRING_TOKEN (STR_TCSS_TBT_STATUS_STRING),
      L"TBT FW IMR Status: %08X",
      TcssHob->TcssData.TbtImrStatus.RegValue
      );

    InitString (
      HiiHandle,
      STRING_TOKEN (STR_TCSS_PHY_VERSION_STRING),
      L"PHY FW version: %04X",
      TcssHob->TcssData.MgImrStatus.Bits.MgFwVersion
      );

  if (TcssHob->TcssData.TbtImrStatus.Bits.Done) {
      InitString (
        HiiHandle,
        STRING_TOKEN (STR_TCSS_TBT_VERSION_STRING),
        L"TBT FW version: %04d",
        TcssHob->TcssData.TbtImrStatus.Bits.TbtFwVersion
        );
    }

    if (TcssHob->TcssData.IOMReady) {
      InitString (
        HiiHandle,
        STRING_TOKEN (STR_TCSS_IOM_VERSION_STRING),
        L"IOM FW version: %08X",
        TcssHob->TcssData.IomFwVersion
        );

      InitString (
        HiiHandle,
        STRING_TOKEN (STR_TCSS_IOM_ENGR_VERSION_STRING),
        L"IOM FW version: %08X",
        TcssHob->TcssData.IomFwEngrVersion
        );

      InitString (
        HiiHandle,
        STRING_TOKEN (STR_TC_CSTATE_STATUS),
        L"Deepest TC state: %04X",
        TcssHob->TcssData.DeepstTcState
        );
    }
  }
#endif
  // update VMD info if VMD is supported.
  if (mSaSetup.VmdSupported){
    UpdateVmdInfo(HiiHandle);
  }
}

VOID
UpdateDmiInfo (
  EFI_HII_HANDLE HiiHandle,
  UINT16         Class
  )
{
  UINT16  Data16;
  UINT64  DmiBar;

  //
  // Get DMIBAR
  //
  PciSegmentReadBuffer (PCI_SEGMENT_LIB_ADDRESS (SA_SEG_NUM, SA_MC_BUS, SA_MC_DEV, SA_MC_FUN, R_SA_DMIBAR), sizeof (DmiBar), &DmiBar);
  DmiBar &= ~((UINT64) BIT0);

  //
  // LSTS 9:4 - DMI Link Negotiated Width, 3:0 - Max Link Speed, Gen2/Gen1 Infomation
  //
  Data16 = *(UINT16 *) (UINTN) (DmiBar + 0x52);

  InitString (
    HiiHandle,
    STRING_TOKEN (STR_DMI_INFO_VALUE),
    L"X%d  Gen%1d",
    ((Data16 >> 4) & 0xf),
    (Data16 & 0xf)
    );
}

EFI_STATUS
EFIAPI
TcssPcieRootPortCallback (
  IN CONST EFI_HII_CONFIG_ACCESS_PROTOCOL *This,
  IN EFI_BROWSER_ACTION                   Action,
  IN EFI_QUESTION_ID                      KeyValue,
  IN UINT8                                Type,
  IN EFI_IFR_TYPE_VALUE                   *Value,
  OUT EFI_BROWSER_ACTION_REQUEST          *ActionRequest
  )
{
#if FixedPcdGetBool(PcdITbtEnable) == 1
  SA_SETUP    *SaSetup;
//[-start-200911-IB17040162-add]//
  SA_SETUP    *MySaIfrNVData;
//[-end-200911-IB17040162-add]//
  SETUP_DATA  *SetupData;
//[-start-200911-IB17040162-add]//
  SETUP_DATA  *MySetupDataIfrNVData;
//[-end-200911-IB17040162-add]//
  UINTN       VarSize;
  EFI_STATUS  Status;
  EFI_STRING  RequestString;
  UINT8       TcssRootPort0;
  UINT8       TcssRootPort1;
  UINT8       TcssRootPort2;
  UINT8       TcssRootPort3;

  if (Action != EFI_BROWSER_ACTION_CHANGING && Action != EFI_BROWSER_ACTION_CHANGED) {
    return EFI_UNSUPPORTED;
  }

  TcssRootPort0 = 0;
  TcssRootPort1 = 0;
  TcssRootPort2 = 0;
  TcssRootPort3 = 0;

  DEBUG ((EFI_D_INFO, "TcssPcieRootPortCallback()\n"));

  Status = EFI_SUCCESS;
  VarSize = sizeof (SA_SETUP);
  SaSetup = AllocatePool (VarSize);
  ASSERT (SaSetup != NULL);
  if (SaSetup == NULL) {
    return EFI_OUT_OF_RESOURCES;
  }

//[-start-200911-IB17040162-add]//
  MySaIfrNVData = (SA_SETUP *)gRcSUBrowser->SaSUBrowserData;
  CopyMem (SaSetup, MySaIfrNVData, VarSize);
//[-end-200911-IB17040162-add]//
  if (!HiiGetBrowserData (&gSaSetupVariableGuid, L"SaSetup", VarSize, (UINT8 *) SaSetup)) {
    Status = EFI_NOT_FOUND;
  }
  ASSERT_EFI_ERROR (Status);

  if (!EFI_ERROR (Status)) {
    TcssRootPort0 = SaSetup->TcssItbtPcie0En;
    TcssRootPort1 = SaSetup->TcssItbtPcie1En;
    TcssRootPort2 = SaSetup->TcssItbtPcie2En;
    TcssRootPort3 = SaSetup->TcssItbtPcie3En;

    RequestString = NULL;

    if (RequestString != NULL) {
      if (!HiiSetBrowserData (&gSaSetupVariableGuid, L"SaSetup", VarSize, (UINT8 *) SaSetup, RequestString)) {
        Status = EFI_NOT_FOUND;
      }
      ASSERT_EFI_ERROR (Status);
//[-start-200911-IB17040162-add]//
      VarSize = sizeof (SA_SETUP);
      CopyMem (MySaIfrNVData, SaSetup, VarSize);
//[-end-200911-IB17040162-add]//
      FreePool (RequestString);
    }
  }
  FreePool (SaSetup);

  VarSize = sizeof (SETUP_DATA);
  SetupData = AllocatePool (VarSize);
  ASSERT (SetupData != NULL);
  if (SetupData == NULL) {
    return EFI_OUT_OF_RESOURCES;
  }

//[-start-200911-IB17040162-add]//
  MySetupDataIfrNVData = (SETUP_DATA *)gRcSUBrowser->SetupDataSUBrowserData;
  CopyMem (SetupData, MySetupDataIfrNVData, VarSize);
//[-end-200911-IB17040162-add]//

  if (!HiiGetBrowserData (&gSetupVariableGuid, L"Setup", VarSize, (UINT8 *) SetupData)) {
    Status = EFI_NOT_FOUND;
  }
  ASSERT_EFI_ERROR (Status);

  if (!EFI_ERROR (Status)) {
    SetupData->ITbtRootPort[0] = TcssRootPort0;
    SetupData->ITbtRootPort[1] = TcssRootPort1;
    SetupData->ITbtRootPort[2] = TcssRootPort2;
    SetupData->ITbtRootPort[3] = TcssRootPort3;

    RequestString = NULL;
    RequestString = HiiConstructRequestString (RequestString, OFFSET_OF (SETUP_DATA, ITbtRootPort[0]), sizeof(SetupData->ITbtRootPort[0]));
    RequestString = HiiConstructRequestString (RequestString, OFFSET_OF (SETUP_DATA, ITbtRootPort[1]), sizeof(SetupData->ITbtRootPort[1]));
    RequestString = HiiConstructRequestString (RequestString, OFFSET_OF (SETUP_DATA, ITbtRootPort[2]), sizeof(SetupData->ITbtRootPort[2]));
    RequestString = HiiConstructRequestString (RequestString, OFFSET_OF (SETUP_DATA, ITbtRootPort[3]), sizeof(SetupData->ITbtRootPort[3]));

    if (RequestString != NULL) {
      if (!HiiSetBrowserData (&gSetupVariableGuid, L"Setup", VarSize, (UINT8 *) SetupData, RequestString)) {
        Status = EFI_NOT_FOUND;
      }
      ASSERT_EFI_ERROR (Status);
//[-start-200911-IB17040162-add]//
      VarSize = sizeof (SETUP_DATA);
      CopyMem (MySetupDataIfrNVData, SetupData, VarSize);
//[-end-200911-IB17040162-add]//
      FreePool (RequestString);
    }
  }
  FreePool (SetupData);

  return Status;
#else
  return EFI_SUCCESS;
#endif
}

EFI_STATUS
EFIAPI
TcssXhciCallback (
  IN CONST EFI_HII_CONFIG_ACCESS_PROTOCOL *This,
  IN EFI_BROWSER_ACTION                   Action,
  IN EFI_QUESTION_ID                      KeyValue,
  IN UINT8                                Type,
  IN EFI_IFR_TYPE_VALUE                   *Value,
  OUT EFI_BROWSER_ACTION_REQUEST          *ActionRequest
  )
{
  SA_SETUP    *SaSetup;
  SETUP_DATA  *SetupData;
  UINTN       VarSize;
  EFI_STATUS  Status;
  EFI_STRING  RequestString;

  DEBUG ((EFI_D_INFO, "TcssXhciCallback()\n"));

  if (Action != EFI_BROWSER_ACTION_CHANGING && Action != EFI_BROWSER_ACTION_CHANGED) {
    return EFI_UNSUPPORTED;
  }

  Status = EFI_SUCCESS;
  VarSize = sizeof (SA_SETUP);
  SaSetup = AllocatePool (VarSize);
  ASSERT (SaSetup != NULL);
  if (SaSetup == NULL) {
    return EFI_OUT_OF_RESOURCES;
  }

  if (!HiiGetBrowserData (&gSaSetupVariableGuid, L"SaSetup", VarSize, (UINT8 *) SaSetup)) {
    Status = EFI_NOT_FOUND;
  }
  ASSERT_EFI_ERROR (Status);

  if (!EFI_ERROR (Status)) {
    if (Value->u8 == 0) {
      SaSetup->TcssItbtPcie0En            = 0;
      SaSetup->TcssItbtPcie1En            = 0;
      SaSetup->TcssItbtPcie2En            = 0;
      SaSetup->TcssItbtPcie3En            = 0;
      SaSetup->TcssDma0En                 = 0;
      SaSetup->TcssDma1En                 = 0;
    } else {
      SaSetup->TcssItbtPcie0En            = 0;
      SaSetup->TcssItbtPcie1En            = 0;
      SaSetup->TcssItbtPcie2En            = 0;
      SaSetup->TcssItbtPcie3En            = 0;
      SaSetup->TcssDma0En                 = 0;
      SaSetup->TcssDma1En                 = 0;
      if (PcdGet8(PcdITbtRootPortNumber) >= 1) {
        SaSetup->TcssItbtPcie0En            = 1;
        SaSetup->TcssDma0En                 = 1;
      }
      if (PcdGet8(PcdITbtRootPortNumber) >= 2) {
        SaSetup->TcssItbtPcie1En            = 1;
      }
      if (PcdGet8(PcdITbtRootPortNumber) >= 3) {
        SaSetup->TcssItbtPcie2En            = 1;
        SaSetup->TcssDma1En                 = 1;
      }
      if (PcdGet8(PcdITbtRootPortNumber) >= 3) {
        SaSetup->TcssItbtPcie3En            = 1;
      }
    }
    RequestString = NULL;
    RequestString = HiiConstructRequestString (RequestString, OFFSET_OF (SA_SETUP, TcssItbtPcie0En),            sizeof (SaSetup->TcssItbtPcie0En));
    RequestString = HiiConstructRequestString (RequestString, OFFSET_OF (SA_SETUP, TcssItbtPcie1En),            sizeof (SaSetup->TcssItbtPcie1En));
    RequestString = HiiConstructRequestString (RequestString, OFFSET_OF (SA_SETUP, TcssItbtPcie2En),            sizeof (SaSetup->TcssItbtPcie2En));
    RequestString = HiiConstructRequestString (RequestString, OFFSET_OF (SA_SETUP, TcssItbtPcie3En),            sizeof (SaSetup->TcssItbtPcie3En));
    RequestString = HiiConstructRequestString (RequestString, OFFSET_OF (SA_SETUP, TcssXdciEn),                 sizeof (SaSetup->TcssXdciEn));
    RequestString = HiiConstructRequestString (RequestString, OFFSET_OF (SA_SETUP, TcssDma0En),                 sizeof (SaSetup->TcssDma0En));
    RequestString = HiiConstructRequestString (RequestString, OFFSET_OF (SA_SETUP, TcssDma1En),                 sizeof (SaSetup->TcssDma1En));
    RequestString = HiiConstructRequestString (RequestString, OFFSET_OF (SA_SETUP, TcssVccstStatus),            sizeof (SaSetup->TcssVccstStatus));
    RequestString = HiiConstructRequestString (RequestString, OFFSET_OF (SA_SETUP, UsbOverride),                sizeof (SaSetup->UsbOverride));
    RequestString = HiiConstructRequestString (RequestString, OFFSET_OF (SA_SETUP, D3ColdEnable),               sizeof (SaSetup->D3ColdEnable));
    RequestString = HiiConstructRequestString (RequestString, OFFSET_OF (SA_SETUP, D3HotEnable),                sizeof (SaSetup->D3HotEnable));

    if (RequestString != NULL) {
      if (!HiiSetBrowserData (&gSaSetupVariableGuid, L"SaSetup", VarSize, (UINT8 *) SaSetup, RequestString)) {
        Status = EFI_NOT_FOUND;
      }
      ASSERT_EFI_ERROR (Status);
      FreePool (RequestString);
    }
  }
  FreePool (SaSetup);

  VarSize = sizeof (SETUP_DATA);
  SetupData = AllocatePool (VarSize);
  ASSERT (SetupData != NULL);
  if (SetupData == NULL) {
    return EFI_OUT_OF_RESOURCES;
  }

  if (!HiiGetBrowserData (&gSetupVariableGuid, L"Setup", VarSize, (UINT8 *) SetupData)) {
    Status = EFI_NOT_FOUND;
  }
  ASSERT_EFI_ERROR (Status);

  if (!EFI_ERROR (Status)) {
    if (Value->u8 == 0) {
      SetupData->UsbcBiosTcssHandshake = 0;
      SetupData->ITbtRootPort[0] = 0;
      SetupData->ITbtRootPort[1] = 0;
      SetupData->ITbtRootPort[2] = 0;
      SetupData->ITbtRootPort[3] = 0;
    } else {
      SetupData->UsbcBiosTcssHandshake = 1;
      SetupData->ITbtRootPort[0] = 1;
      SetupData->ITbtRootPort[1] = 1;
      SetupData->ITbtRootPort[2] = 1;
      SetupData->ITbtRootPort[3] = 1;
      if (PcdGet8(PcdITbtRootPortNumber) == 2) {
        SetupData->ITbtRootPort[2] = 0;
        SetupData->ITbtRootPort[3] = 0;
      }
      if (PcdGet8(PcdITbtRootPortNumber) == 3) {
        SetupData->ITbtRootPort[3] = 0;
      }
    }
    RequestString = NULL;
    RequestString = HiiConstructRequestString (RequestString, OFFSET_OF (SETUP_DATA, UsbcBiosTcssHandshake), sizeof (SetupData->UsbcBiosTcssHandshake));
    RequestString = HiiConstructRequestString (RequestString, OFFSET_OF (SETUP_DATA, ITbtRootPort[0]), sizeof (SetupData->ITbtRootPort[0]));
    RequestString = HiiConstructRequestString (RequestString, OFFSET_OF (SETUP_DATA, ITbtRootPort[1]), sizeof (SetupData->ITbtRootPort[1]));
    RequestString = HiiConstructRequestString (RequestString, OFFSET_OF (SETUP_DATA, ITbtRootPort[2]), sizeof (SetupData->ITbtRootPort[2]));
    RequestString = HiiConstructRequestString (RequestString, OFFSET_OF (SETUP_DATA, ITbtRootPort[3]), sizeof (SetupData->ITbtRootPort[3]));

    if (RequestString != NULL) {
      if (!HiiSetBrowserData (&gSetupVariableGuid, L"Setup", VarSize, (UINT8 *) SetupData, RequestString)) {
        Status = EFI_NOT_FOUND;
      }
      ASSERT_EFI_ERROR (Status);
      FreePool (RequestString);
    }
  }
  FreePool (SetupData);

  return Status;
}
EFI_STATUS
EFIAPI
UpdateVmdInfo (
  EFI_HII_HANDLE HiiHandle
  )
{
  EFI_STRING_ID   StrRef;
  VMD_INFO_HOB    *VmdInfoHob;
  UINT8           Index;

  DEBUG ((EFI_D_INFO, "UpdateVmdSetupInfo()\n"));

  VmdInfoHob = (VMD_INFO_HOB *) GetFirstGuidHob (&gVmdInfoHobGuid);
  if (VmdInfoHob == NULL) {
    DEBUG ((EFI_D_INFO, "Vmd Info Hob not found\n"));
    return EFI_NOT_FOUND;
  }

  for ( Index = 0; Index < VMD_MAX_DEVICES; ++Index ) {
    switch(Index) {
      case 0:
        StrRef = STRING_TOKEN (STR_RP1_BDF_VALUE);
        break;
      case 1:
        StrRef = STRING_TOKEN (STR_RP2_BDF_VALUE);
        break;
      case 2:
        StrRef = STRING_TOKEN (STR_RP3_BDF_VALUE);
        break;
      case 3:
        StrRef = STRING_TOKEN (STR_RP4_BDF_VALUE);
        break;
      case 4:
        StrRef = STRING_TOKEN (STR_RP5_BDF_VALUE);
        break;
      case 5:
        StrRef = STRING_TOKEN (STR_RP6_BDF_VALUE);
        break;
      case 6:
        StrRef = STRING_TOKEN (STR_RP7_BDF_VALUE);
        break;
      case 7:
        StrRef = STRING_TOKEN (STR_RP8_BDF_VALUE);
        break;
      case 8:
        StrRef = STRING_TOKEN (STR_RP9_BDF_VALUE);
        break;
      case 9:
        StrRef = STRING_TOKEN (STR_RP10_BDF_VALUE);
        break;
      case 10:
        StrRef = STRING_TOKEN (STR_RP11_BDF_VALUE);
        break;
      case 11:
        StrRef = STRING_TOKEN (STR_RP12_BDF_VALUE);
        break;
      case 12:
        StrRef = STRING_TOKEN (STR_RP13_BDF_VALUE);
        break;
      case 13:
        StrRef = STRING_TOKEN (STR_RP14_BDF_VALUE);
        break;
      case 14:
        StrRef = STRING_TOKEN (STR_RP15_BDF_VALUE);
        break;
      case 15:
        StrRef = STRING_TOKEN (STR_RP16_BDF_VALUE);
        break;
      case 16:
        StrRef = STRING_TOKEN (STR_RP17_BDF_VALUE);
        break;
      case 17:
        StrRef = STRING_TOKEN (STR_RP18_BDF_VALUE);
        break;
      case 18:
        StrRef = STRING_TOKEN (STR_RP19_BDF_VALUE);
        break;
      case 19:
        StrRef = STRING_TOKEN (STR_RP20_BDF_VALUE);
        break;
      case 20:
        StrRef = STRING_TOKEN (STR_RP21_BDF_VALUE);
        break;
      case 21:
        StrRef = STRING_TOKEN (STR_RP22_BDF_VALUE);
        break;
      case 22:
        StrRef = STRING_TOKEN (STR_RP23_BDF_VALUE);
        break;
      case 23:
        StrRef = STRING_TOKEN (STR_RP24_BDF_VALUE);
        break;
      case 24:
        StrRef = STRING_TOKEN (STR_RP25_BDF_VALUE);
        break;
      case 25:
        StrRef = STRING_TOKEN (STR_RP26_BDF_VALUE);
        break;
      case 26:
        StrRef = STRING_TOKEN (STR_RP27_BDF_VALUE);
        break;
      case 27:
        StrRef = STRING_TOKEN (STR_RP28_BDF_VALUE);
        break;
      case 28:
        StrRef = STRING_TOKEN (STR_RP29_BDF_VALUE);
        break;
      case 29:
        StrRef = STRING_TOKEN (STR_RP30_BDF_VALUE);
        break;
      case 30:
        StrRef = STRING_TOKEN (STR_RP31_BDF_VALUE);
        break;
      default:
        StrRef = STRING_TOKEN (STR_RP1_BDF_VALUE);
    }
//[-start-200903-IB17040149-add]//
#if FixedPcdGet8(PcdModifyVmdPortConfigViaScuDefault)	
    if (mSaSetup.VmdPortDev[Index] == SataDevNumber (0)) {
      InitString (
        HiiHandle,
        StrRef,
        L"%s", L"SATA Controller"
        );
    } else {
      InitString (
        HiiHandle,
        StrRef,
        L"%d/%d/%d",
        0,
        mSaSetup.VmdPortDev[Index],
        mSaSetup.VmdPortFunc[Index]
        );
    }
#else
//[-end-200903-IB17040149-add]//
    if (VmdInfoHob->VmdPortInfo.PortInfo[Index].RpDev == SataDevNumber (0)) {
      InitString (
        HiiHandle,
        StrRef,
        L"%s", L"SATA Controller"
        );
    } else {
    InitString (
      HiiHandle,
      StrRef,
      L"%d/%d/%d",
      0,
      VmdInfoHob->VmdPortInfo.PortInfo[Index].RpDev,
      VmdInfoHob->VmdPortInfo.PortInfo[Index].RpFunc
      );
    }
//[-start-200903-IB17040149-add]//
#endif
//[-end-200903-IB17040149-add]//
  }
  return EFI_SUCCESS;
}

/**
  This function performs mapping of all the detected storage devices under
  VMD if Global Mapping is enabled.

  @param  This                   Points to the EFI_HII_CONFIG_ACCESS_PROTOCOL.
  @param  Action                 Specifies the type of action taken by the browser.
  @param  KeyValue               A unique value which is sent to the original
                                 exporting driver so that it can identify the type
                                 of data to expect.
  @param  Type                   The type of value for the question.
  @param  Value                  A pointer to the data being sent to the original
                                 exporting driver.
  @param  ActionRequest          On return, points to the action requested by the
                                 callback function.

  @retval EFI_SUCCESS            The callback successfully handled the action.
  @retval EFI_OUT_OF_RESOURCES   Not enough storage is available to hold the
                                 variable and its data.
  @retval EFI_DEVICE_ERROR       The variable could not be saved.
  @retval EFI_UNSUPPORTED        The specified Action is not supported by the
                                 callback.
**/
EFI_STATUS
EFIAPI
VmdCallback (
  IN CONST EFI_HII_CONFIG_ACCESS_PROTOCOL *This,
  IN EFI_BROWSER_ACTION                   Action,
  IN EFI_QUESTION_ID                      KeyValue,
  IN UINT8                                Type,
  IN EFI_IFR_TYPE_VALUE                   *Value,
  OUT EFI_BROWSER_ACTION_REQUEST          *ActionRequest
  )
{
  SA_SETUP        *SaSetup;
  UINTN           VarSize;
  EFI_STATUS      Status;
  EFI_STRING      RequestString;
  VMD_INFO_HOB    *VmdInfoHob;
  int             i;

  Status = EFI_SUCCESS;
  if (Action != EFI_BROWSER_ACTION_CHANGING && Action != EFI_BROWSER_ACTION_CHANGED) {
    DEBUG ((DEBUG_INFO, "Exited without updating VMD Setup menu\n"));
    return EFI_UNSUPPORTED;
  }

  ASSERT (KeyValue == KEY_SA_VMD_GLOBAL_MAPPING);
  DEBUG ((DEBUG_INFO, "VmdCallback()\n"));

  VarSize = sizeof (SA_SETUP);
  SaSetup = AllocatePool (VarSize);
  ASSERT (SaSetup != NULL);
  if (SaSetup == NULL) {
    return EFI_OUT_OF_RESOURCES;
  }

  if (!HiiGetBrowserData (&gSaSetupVariableGuid, L"SaSetup", VarSize, (UINT8 *) SaSetup)) {
    Status = EFI_NOT_FOUND;
  }
  ASSERT_EFI_ERROR (Status);

  if (!EFI_ERROR (Status) && (SaSetup->VmdGlobalMapping == 1)) {
    VmdInfoHob = (VMD_INFO_HOB *) GetFirstGuidHob (&gVmdInfoHobGuid);
    if (VmdInfoHob == NULL) {
      DEBUG ((DEBUG_INFO, "Vmd Info Hob not found\n"));
    } else {
      DEBUG ((DEBUG_INFO, "Vmd Global Mapping is enabled\n"));
      RequestString = NULL;
      for (i = 0; i < VMD_MAX_DEVICES; ++i) {
        if (VmdInfoHob->VmdPortInfo.PortInfo[i].DeviceDetected) {
          switch(i) {
            case 0:
              SaSetup->VmdPort[0] = 1;
              RequestString = HiiConstructRequestString (RequestString, OFFSET_OF (SA_SETUP, VmdPort[0]), sizeof (SaSetup->VmdPort[0]));
              break;
            case 1:
              SaSetup->VmdPort[1] = 1;
              RequestString = HiiConstructRequestString (RequestString, OFFSET_OF (SA_SETUP, VmdPort[1]), sizeof (SaSetup->VmdPort[1]));
              break;
            case 2:
              SaSetup->VmdPort[2] = 1;
              RequestString = HiiConstructRequestString (RequestString, OFFSET_OF (SA_SETUP, VmdPort[2]), sizeof (SaSetup->VmdPort[2]));
              break;
            case 3:
              SaSetup->VmdPort[3] = 1;
              RequestString = HiiConstructRequestString (RequestString, OFFSET_OF (SA_SETUP, VmdPort[3]), sizeof (SaSetup->VmdPort[3]));
              break;
            case 4:
              SaSetup->VmdPort[4] = 1;
              RequestString = HiiConstructRequestString (RequestString, OFFSET_OF (SA_SETUP, VmdPort[4]), sizeof (SaSetup->VmdPort[4]));
              break;
            case 5:
              SaSetup->VmdPort[5] = 1;
              RequestString = HiiConstructRequestString (RequestString, OFFSET_OF (SA_SETUP, VmdPort[5]), sizeof (SaSetup->VmdPort[5]));
              break;
            case 6:
              SaSetup->VmdPort[6] = 1;
              RequestString = HiiConstructRequestString (RequestString, OFFSET_OF (SA_SETUP, VmdPort[6]), sizeof (SaSetup->VmdPort[6]));
              break;
            case 7:
              SaSetup->VmdPort[7] = 1;
              RequestString = HiiConstructRequestString (RequestString, OFFSET_OF (SA_SETUP, VmdPort[7]), sizeof (SaSetup->VmdPort[7]));
              break;
            case 8:
              SaSetup->VmdPort[8] = 1;
              RequestString = HiiConstructRequestString (RequestString, OFFSET_OF (SA_SETUP, VmdPort[8]), sizeof (SaSetup->VmdPort[8]));
              break;
            case 9:
              SaSetup->VmdPort[9] = 1;
              RequestString = HiiConstructRequestString (RequestString, OFFSET_OF (SA_SETUP, VmdPort[9]), sizeof (SaSetup->VmdPort[9]));
              break;
            case 10:
              SaSetup->VmdPort[10] = 1;
              RequestString = HiiConstructRequestString (RequestString, OFFSET_OF (SA_SETUP, VmdPort[10]), sizeof (SaSetup->VmdPort[10]));
              break;
            case 11:
              SaSetup->VmdPort[11] = 1;
              RequestString = HiiConstructRequestString (RequestString, OFFSET_OF (SA_SETUP, VmdPort[11]), sizeof (SaSetup->VmdPort[11]));
              break;
            case 12:
              SaSetup->VmdPort[12] = 1;
              RequestString = HiiConstructRequestString (RequestString, OFFSET_OF (SA_SETUP, VmdPort[12]), sizeof (SaSetup->VmdPort[12]));
              break;
            case 13:
              SaSetup->VmdPort[13] = 1;
              RequestString = HiiConstructRequestString (RequestString, OFFSET_OF (SA_SETUP, VmdPort[13]), sizeof (SaSetup->VmdPort[13]));
              break;
            case 14:
              SaSetup->VmdPort[14] = 1;
              RequestString = HiiConstructRequestString (RequestString, OFFSET_OF (SA_SETUP, VmdPort[14]), sizeof (SaSetup->VmdPort[14]));
              break;
            case 15:
              SaSetup->VmdPort[15] = 1;
              RequestString = HiiConstructRequestString (RequestString, OFFSET_OF (SA_SETUP, VmdPort[15]), sizeof (SaSetup->VmdPort[15]));
              break;
            case 16:
              SaSetup->VmdPort[16] = 1;
              RequestString = HiiConstructRequestString (RequestString, OFFSET_OF (SA_SETUP, VmdPort[16]), sizeof (SaSetup->VmdPort[16]));
              break;
            case 17:
              SaSetup->VmdPort[17] = 1;
              RequestString = HiiConstructRequestString (RequestString, OFFSET_OF (SA_SETUP, VmdPort[17]), sizeof (SaSetup->VmdPort[17]));
              break;
            case 18:
              SaSetup->VmdPort[18] = 1;
              RequestString = HiiConstructRequestString (RequestString, OFFSET_OF (SA_SETUP, VmdPort[18]), sizeof (SaSetup->VmdPort[18]));
              break;
            case 19:
              SaSetup->VmdPort[19] = 1;
              RequestString = HiiConstructRequestString (RequestString, OFFSET_OF (SA_SETUP, VmdPort[19]), sizeof (SaSetup->VmdPort[19]));
              break;
            case 20:
              SaSetup->VmdPort[20] = 1;
              RequestString = HiiConstructRequestString (RequestString, OFFSET_OF (SA_SETUP, VmdPort[20]), sizeof (SaSetup->VmdPort[20]));
              break;
            case 21:
              SaSetup->VmdPort[21] = 1;
              RequestString = HiiConstructRequestString (RequestString, OFFSET_OF (SA_SETUP, VmdPort[21]), sizeof (SaSetup->VmdPort[21]));
              break;
            case 22:
              SaSetup->VmdPort[22] = 1;
              RequestString = HiiConstructRequestString (RequestString, OFFSET_OF (SA_SETUP, VmdPort[22]), sizeof (SaSetup->VmdPort[22]));
              break;
            case 23:
              SaSetup->VmdPort[23] = 1;
              RequestString = HiiConstructRequestString (RequestString, OFFSET_OF (SA_SETUP, VmdPort[23]), sizeof (SaSetup->VmdPort[23]));
              break;
            case 24:
              SaSetup->VmdPort[24] = 1;
              RequestString = HiiConstructRequestString (RequestString, OFFSET_OF (SA_SETUP, VmdPort[24]), sizeof (SaSetup->VmdPort[24]));
              break;
            case 25:
              SaSetup->VmdPort[25] = 1;
              RequestString = HiiConstructRequestString (RequestString, OFFSET_OF (SA_SETUP, VmdPort[25]), sizeof (SaSetup->VmdPort[25]));
              break;
            case 26:
              SaSetup->VmdPort[26] = 1;
              RequestString = HiiConstructRequestString (RequestString, OFFSET_OF (SA_SETUP, VmdPort[26]), sizeof (SaSetup->VmdPort[26]));
              break;
            case 27:
              SaSetup->VmdPort[27] = 1;
              RequestString = HiiConstructRequestString (RequestString, OFFSET_OF (SA_SETUP, VmdPort[27]), sizeof (SaSetup->VmdPort[27]));
              break;
            case 28:
              SaSetup->VmdPort[28] = 1;
              RequestString = HiiConstructRequestString (RequestString, OFFSET_OF (SA_SETUP, VmdPort[28]), sizeof (SaSetup->VmdPort[28]));
              break;
            case 29:
              SaSetup->VmdPort[29] = 1;
              RequestString = HiiConstructRequestString (RequestString, OFFSET_OF (SA_SETUP, VmdPort[29]), sizeof (SaSetup->VmdPort[29]));
              break;
            case 30:
              SaSetup->VmdPort[30] = 1;
              RequestString = HiiConstructRequestString (RequestString, OFFSET_OF (SA_SETUP, VmdPort[30]), sizeof (SaSetup->VmdPort[30]));
              break;
            default:
              SaSetup->VmdPort[0] = 1;
              RequestString = HiiConstructRequestString (RequestString, OFFSET_OF (SA_SETUP, VmdPort[0]), sizeof (SaSetup->VmdPort[0]));
          } // Switch
        } // if (devicedetected)
      } // for
      if (RequestString != NULL) {
        if (HiiSetBrowserData (&gSaSetupVariableGuid, L"SaSetup", VarSize, (UINT8 *) SaSetup, RequestString)) {
          FreePool (RequestString);
        } else {
          Status = EFI_NOT_FOUND;
          DEBUG ((DEBUG_INFO, "Vmd browser is not set\n"));
        }
        ASSERT_EFI_ERROR (Status);
      }
    } // else
  } // if (VmdGlobalMapping)
  FreePool (SaSetup);

  return Status;
} // vmdcallback
