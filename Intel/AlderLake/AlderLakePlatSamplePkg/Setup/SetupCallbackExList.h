/** @file
  Internal header of the Setup Component.

@copyright
  INTEL CONFIDENTIAL
  Copyright 2009 - 2022 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains a 'Sample Driver' and is licensed as such under the terms
  of your license agreement with Intel or your vendor. This file may be modified
  by the user, subject to the additional terms of the license agreement.

@par Specification Reference:
**/

#define EVAL(a) a

#define TPM_SETUP_ITEM_CALLBACK_LIST_EX \
  ITEM_CALLBACK_EX(ADVANCED_FORM_SET_CLASS,0,KEY_PTT_DEVICE_TOGGLE,TpmOptionCallback), \
  ITEM_CALLBACK_EX(ADVANCED_FORM_SET_CLASS,0,KEY_TPM2_GOTO,TpmOptionCallback), \
  ITEM_CALLBACK_EX(ADVANCED_FORM_SET_CLASS,0,KEY_TPM_GOTO,TpmOptionCallback), \
  ITEM_CALLBACK_EX(ADVANCED_FORM_SET_CLASS,0,KEY_TPM_ENABLE,TpmOptionCallback), \
  ITEM_CALLBACK_EX(ADVANCED_FORM_SET_CLASS,0,KEY_TPM_DISABLE,TpmOptionCallback), \
  ITEM_CALLBACK_EX(ADVANCED_FORM_SET_CLASS,0,KEY_TPM_ACTIVATE,TpmOptionCallback), \
  ITEM_CALLBACK_EX(ADVANCED_FORM_SET_CLASS,0,KEY_TPM_DEACTIVATE,TpmOptionCallback), \
  ITEM_CALLBACK_EX(ADVANCED_FORM_SET_CLASS,0,KEY_TPM_FORCE_CLEAR,TpmOptionCallback),

#ifndef TPM_SETUP_ITEM_CALLBACK_LIST_EX
#define TPM_SETUP_ITEM_CALLBACK_LIST_EX \
  ITEM_CALLBACK_EX(ADVANCED_FORM_SET_CLASS,0,KEY_TPM_GOTO,TpmOptionCallback), \
  ITEM_CALLBACK_EX(ADVANCED_FORM_SET_CLASS,0,KEY_TPM_ENABLE,TpmOptionCallback), \
  ITEM_CALLBACK_EX(ADVANCED_FORM_SET_CLASS,0,KEY_TPM_DISABLE,TpmOptionCallback), \
  ITEM_CALLBACK_EX(ADVANCED_FORM_SET_CLASS,0,KEY_TPM_ACTIVATE,TpmOptionCallback), \
  ITEM_CALLBACK_EX(ADVANCED_FORM_SET_CLASS,0,KEY_TPM_DEACTIVATE,TpmOptionCallback), \
  ITEM_CALLBACK_EX(ADVANCED_FORM_SET_CLASS,0,KEY_TPM_FORCE_CLEAR,TpmOptionCallback),
#endif

#define SETUP_ITEM_CALLBACK_LIST_EX \
  EVAL(TPM_SETUP_ITEM_CALLBACK_LIST_EX) \
  ITEM_CALLBACK_EX(ADVANCED_FORM_SET_CLASS,0,KEY_ICC_PROFILE,IccProfileCallback), \
  ITEM_CALLBACK_EX(ADVANCED_FORM_SET_CLASS,0,KEY_ICC_RATIO2,IccCallback), \
  ITEM_CALLBACK_EX(ADVANCED_FORM_SET_CLASS,0,KEY_ICC_FREQ2,IccCallback), \
  ITEM_CALLBACK_EX(ADVANCED_FORM_SET_CLASS,0,KEY_ICC_FREQ3,IccCallback), \
  ITEM_CALLBACK_EX(ADVANCED_FORM_SET_CLASS,0,KEY_ICC_SPREAD2,IccCallback), \
  ITEM_CALLBACK_EX(ADVANCED_FORM_SET_CLASS,0,KEY_ICC_SPREAD3,IccCallback), \
  ITEM_CALLBACK_EX(ADVANCED_FORM_SET_CLASS,0,KEY_RatioLimitRatio0,CpuFormCallBackFunction), \
  ITEM_CALLBACK_EX(ADVANCED_FORM_SET_CLASS,0,KEY_RatioLimitRatio1,CpuFormCallBackFunction), \
  ITEM_CALLBACK_EX(ADVANCED_FORM_SET_CLASS,0,KEY_RatioLimitRatio2,CpuFormCallBackFunction), \
  ITEM_CALLBACK_EX(ADVANCED_FORM_SET_CLASS,0,KEY_RatioLimitRatio3,CpuFormCallBackFunction), \
  ITEM_CALLBACK_EX(ADVANCED_FORM_SET_CLASS,0,KEY_RatioLimitRatio4,CpuFormCallBackFunction), \
  ITEM_CALLBACK_EX(ADVANCED_FORM_SET_CLASS,0,KEY_RatioLimitRatio5,CpuFormCallBackFunction), \
  ITEM_CALLBACK_EX(ADVANCED_FORM_SET_CLASS,0,KEY_RatioLimitRatio6,CpuFormCallBackFunction), \
  ITEM_CALLBACK_EX(ADVANCED_FORM_SET_CLASS,0,KEY_RatioLimitRatio7,CpuFormCallBackFunction), \
  ITEM_CALLBACK_EX(ADVANCED_FORM_SET_CLASS,0,KEY_RatioLimitNumCore0,CpuFormCallBackFunction), \
  ITEM_CALLBACK_EX(ADVANCED_FORM_SET_CLASS,0,KEY_RatioLimitNumCore1,CpuFormCallBackFunction), \
  ITEM_CALLBACK_EX(ADVANCED_FORM_SET_CLASS,0,KEY_RatioLimitNumCore2,CpuFormCallBackFunction), \
  ITEM_CALLBACK_EX(ADVANCED_FORM_SET_CLASS,0,KEY_RatioLimitNumCore3,CpuFormCallBackFunction), \
  ITEM_CALLBACK_EX(ADVANCED_FORM_SET_CLASS,0,KEY_RatioLimitNumCore4,CpuFormCallBackFunction), \
  ITEM_CALLBACK_EX(ADVANCED_FORM_SET_CLASS,0,KEY_RatioLimitNumCore5,CpuFormCallBackFunction), \
  ITEM_CALLBACK_EX(ADVANCED_FORM_SET_CLASS,0,KEY_RatioLimitNumCore6,CpuFormCallBackFunction), \
  ITEM_CALLBACK_EX(ADVANCED_FORM_SET_CLASS,0,KEY_RatioLimitNumCore7,CpuFormCallBackFunction), \
  ITEM_CALLBACK_EX(ADVANCED_FORM_SET_CLASS,0,KEY_AtomRatioLimitRatio0,CpuFormCallBackFunction), \
  ITEM_CALLBACK_EX(ADVANCED_FORM_SET_CLASS,0,KEY_AtomRatioLimitRatio1,CpuFormCallBackFunction), \
  ITEM_CALLBACK_EX(ADVANCED_FORM_SET_CLASS,0,KEY_AtomRatioLimitRatio2,CpuFormCallBackFunction), \
  ITEM_CALLBACK_EX(ADVANCED_FORM_SET_CLASS,0,KEY_AtomRatioLimitRatio3,CpuFormCallBackFunction), \
  ITEM_CALLBACK_EX(ADVANCED_FORM_SET_CLASS,0,KEY_AtomRatioLimitRatio4,CpuFormCallBackFunction), \
  ITEM_CALLBACK_EX(ADVANCED_FORM_SET_CLASS,0,KEY_AtomRatioLimitRatio5,CpuFormCallBackFunction), \
  ITEM_CALLBACK_EX(ADVANCED_FORM_SET_CLASS,0,KEY_AtomRatioLimitRatio6,CpuFormCallBackFunction), \
  ITEM_CALLBACK_EX(ADVANCED_FORM_SET_CLASS,0,KEY_AtomRatioLimitRatio7,CpuFormCallBackFunction), \
  ITEM_CALLBACK_EX(ADVANCED_FORM_SET_CLASS,0,KEY_AtomRatioLimitNumCore0,CpuFormCallBackFunction), \
  ITEM_CALLBACK_EX(ADVANCED_FORM_SET_CLASS,0,KEY_AtomRatioLimitNumCore1,CpuFormCallBackFunction), \
  ITEM_CALLBACK_EX(ADVANCED_FORM_SET_CLASS,0,KEY_AtomRatioLimitNumCore2,CpuFormCallBackFunction), \
  ITEM_CALLBACK_EX(ADVANCED_FORM_SET_CLASS,0,KEY_AtomRatioLimitNumCore3,CpuFormCallBackFunction), \
  ITEM_CALLBACK_EX(ADVANCED_FORM_SET_CLASS,0,KEY_AtomRatioLimitNumCore4,CpuFormCallBackFunction), \
  ITEM_CALLBACK_EX(ADVANCED_FORM_SET_CLASS,0,KEY_AtomRatioLimitNumCore5,CpuFormCallBackFunction), \
  ITEM_CALLBACK_EX(ADVANCED_FORM_SET_CLASS,0,KEY_AtomRatioLimitNumCore6,CpuFormCallBackFunction), \
  ITEM_CALLBACK_EX(ADVANCED_FORM_SET_CLASS,0,KEY_AtomRatioLimitNumCore7,CpuFormCallBackFunction), \
  ITEM_CALLBACK_EX(ADVANCED_FORM_SET_CLASS,0,KEY_XeVoltage,OcFormCallBackFunction), \
  ITEM_CALLBACK_EX(ADVANCED_FORM_SET_CLASS,0,KEY_MEMINFO_PROFILE,OcFormMemoryTimingCallBackFunction), \
  ITEM_CALLBACK_EX(ADVANCED_FORM_SET_CLASS,0,KEY_CpuRatioLimit,CpuFormCallBackFunction), \
  ITEM_CALLBACK_EX(ADVANCED_FORM_SET_CLASS,0,KEY_RING_MAX_OC_RATIO_LIMIT,OcFormCallBackFunction), \
  ITEM_CALLBACK_EX(ADVANCED_FORM_SET_CLASS,0,KEY_DTBT_SUPPORT,DTbtFormCallBackFunction), \
  ITEM_CALLBACK_EX(ADVANCED_FORM_SET_CLASS,0,KEY_DTBT_CONTROLLER0_HOSTROUTER,DTbtFormCallBackFunction), \
  ITEM_CALLBACK_EX(ADVANCED_FORM_SET_CLASS,0,KEY_DTBT_CONTROLLER0,DTbtFormCallBackFunction), \
  ITEM_CALLBACK_EX(ADVANCED_FORM_SET_CLASS,0,KEY_DTBT_CONTROLLER1_HOSTROUTER,DTbtFormCallBackFunction), \
  ITEM_CALLBACK_EX(ADVANCED_FORM_SET_CLASS,0,KEY_DTBT_CONTROLLER1, DTbtFormCallBackFunction), \
  ITEM_CALLBACK_EX(ADVANCED_FORM_SET_CLASS,0,KEY_DTBT_CONTROLLER2, DTbtFormCallBackFunction), \
  ITEM_CALLBACK_EX(ADVANCED_FORM_SET_CLASS,0,KEY_DTBT_CONTROLLER_TYPE_0, DTbtFormCallBackFunction), \
  ITEM_CALLBACK_EX(ADVANCED_FORM_SET_CLASS,0,KEY_DTBT_CONTROLLER_TYPE_1, DTbtFormCallBackFunction), \
  ITEM_CALLBACK_EX(ADVANCED_FORM_SET_CLASS,0,KEY_DTBT_CONTROLLER_TYPE_2, DTbtFormCallBackFunction), \
  ITEM_CALLBACK_EX(ADVANCED_FORM_SET_CLASS,0,KEY_DTBT_CONTROLLER_TYPE_3, DTbtFormCallBackFunction), \
  ITEM_CALLBACK_EX(ADVANCED_FORM_SET_CLASS,0,KEY_TBT_OS_SELECTOR,TbtOsSelectorFormCallBackFunction), \
  ITEM_CALLBACK_EX(ADVANCED_FORM_SET_CLASS,0,KEY_LOW_POWER_S0_IDLE,LowPowerS0IdleEnableCallback), \
  ITEM_CALLBACK_EX(ADVANCED_FORM_SET_CLASS,0,MNG_STATE_KEY,MeFormCallBackFunction), \
  ITEM_CALLBACK_EX(ADVANCED_FORM_SET_CLASS,0,PTT_TRIGGER_FORM_OPEN_ACTION_KEY,MeFormCallBackFunction), \
  ITEM_CALLBACK_EX(ADVANCED_FORM_SET_CLASS,0,DAM_STATE_KEY,MeFormCallBackFunction), \
  ITEM_CALLBACK_EX(ADVANCED_FORM_SET_CLASS,0,ME_FORM_ACTION_KEY,MeFormCallBackFunction), \
  ITEM_CALLBACK_EX(ADVANCED_FORM_SET_CLASS,0,AMT_FORM_ACTION_KEY,MeFormCallBackFunction), \
  ITEM_CALLBACK_EX(ADVANCED_FORM_SET_CLASS,0,PLATFORM_DEBUG_CONSENT_KEY,DebugFormCallBackFunction), \
  ITEM_CALLBACK_EX(ADVANCED_FORM_SET_CLASS,0,KEY_TjMaxOffset, OcFormCallBackFunction),\
  ITEM_CALLBACK_EX(ADVANCED_FORM_SET_CLASS,0,KEY_RING_MIN_RATIO_LIMIT,OcFormCallBackFunction), \
  ITEM_CALLBACK_EX(ADVANCED_FORM_SET_CLASS,0,KEY_RING_MAX_RATIO_LIMIT,OcFormCallBackFunction), \
  ITEM_CALLBACK_EX(ADVANCED_FORM_SET_CLASS,0,KEY_CORE_MAX_OC_RATIO_LIMIT,OcFormCallBackFunction), \
  ITEM_CALLBACK_EX(ADVANCED_FORM_SET_CLASS,0,KEY_SA_TCSS_XHCI_EN, TcssXhciCallback),\
  ITEM_CALLBACK_EX(ADVANCED_FORM_SET_CLASS,0,KEY_ENABLE_THERMAL_FUN, ThermalFunctionCallback), \
  ITEM_CALLBACK_EX(ADVANCED_FORM_SET_CLASS,0,KEY_BCLK_RFI_FREQ0,IccCallback), \
  ITEM_CALLBACK_EX(ADVANCED_FORM_SET_CLASS,0,KEY_BCLK_RFI_FREQ1,IccCallback), \
  ITEM_CALLBACK_EX(ADVANCED_FORM_SET_CLASS,0,KEY_BCLK_RFI_FREQ2,IccCallback), \
  ITEM_CALLBACK_EX(ADVANCED_FORM_SET_CLASS,0,KEY_BCLK_RFI_FREQ3,IccCallback), \
  ITEM_CALLBACK_EX(ADVANCED_FORM_SET_CLASS,0,KEY_CNV_BT_AUDIO_OFFLOAD, CnvFormCallBackFunction), \
  ITEM_CALLBACK_EX(ADVANCED_FORM_SET_CLASS,0,KEY_TXT_POLICY_FIT, TxtPolicyCallBackFunction), \
  ITEM_CALLBACK_EX(ADVANCED_FORM_SET_CLASS,0,KEY_SA_TCSS_ROOT_PORT_0EN, TcssPcieRootPortCallback), \
  ITEM_CALLBACK_EX(ADVANCED_FORM_SET_CLASS,0,KEY_SA_TCSS_ROOT_PORT_1EN, TcssPcieRootPortCallback), \
  ITEM_CALLBACK_EX(ADVANCED_FORM_SET_CLASS,0,KEY_SA_TCSS_ROOT_PORT_2EN, TcssPcieRootPortCallback), \
  ITEM_CALLBACK_EX(ADVANCED_FORM_SET_CLASS,0,KEY_SA_TCSS_ROOT_PORT_3EN, TcssPcieRootPortCallback), \
  ITEM_CALLBACK_EX(ADVANCED_FORM_SET_CLASS,0,KEY_SdevXhciAcpiPathNameDevice1,VtioFormCallBackFunction), \
  ITEM_CALLBACK_EX(ADVANCED_FORM_SET_CLASS,0,KEY_SdevXhciAcpiPathNameDevice2,VtioFormCallBackFunction), \
  ITEM_CALLBACK_EX(ADVANCED_FORM_SET_CLASS,0,KEY_PCH_STATE_AFTER_G3,StateAfterG3CallBackFunction), \
  ITEM_CALLBACK_EX(ADVANCED_FORM_SET_CLASS,0,KEY_VrPowerDeliveryChange,CpuVrConfigCallBackFunction), \
  ITEM_CALLBACK_EX(ADVANCED_FORM_SET_CLASS,0,KEY_SA_VMD_GLOBAL_MAPPING, VmdCallback), \
  ITEM_CALLBACK_EX(ADVANCED_FORM_SET_CLASS,0,EC_PECI_MODE_KEY,EcPeciModeCallBack), \
  ITEM_CALLBACK_EX(ADVANCED_FORM_SET_CLASS,0,KEY_USB4_CM_MODE, Usb4CmModeCallBack), \
  ITEM_CALLBACK_EX(ADVANCED_FORM_SET_CLASS,0,KEY_DEEPEST_USB_SLEEP_WAKE_CAPABILITY, DeepestUsbSleepWakeCapabilityCallBack), \
  ITEM_CALLBACK_EX(ADVANCED_FORM_SET_CLASS,0,KEY_OC_ENABLE_DEPENDENCY, OcFormCallBackFunction), \
  ITEM_CALLBACK_EX(ADVANCED_FORM_SET_CLASS,0,KEY_PERCORE_DISABLE_DEPENDENCY, OcFormCallBackFunction), \
  ITEM_CALLBACK_EX(ADVANCED_FORM_SET_CLASS,0,EC_CS_DEBUG_LIGHT_KEY,EcCsDebugLightCallback), \
  ITEM_CALLBACK_EX(ADVANCED_FORM_SET_CLASS, 0, RTD3_SUPPORT_QUESTION_ID, Rtd3SupportCallBackFunction), \
  ITEM_CALLBACK_EX(ADVANCED_FORM_SET_CLASS,0, CPU_GAME_COMPATIBILITY_MODE_QUESTION_ID, CpuGameCompatibilityCallback),
