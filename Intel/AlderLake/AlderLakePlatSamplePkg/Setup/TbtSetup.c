/** @file

;******************************************************************************
;* Copyright (c) 2012 - 2020, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/
/** @file
  Thunderbolt(TM) Setup Rountines.

@copyright
  INTEL CONFIDENTIAL
  Copyright 2010 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains a 'Sample Driver' and is licensed as such under the terms
  of your license agreement with Intel or your vendor. This file may be modified
  by the user, subject to the additional terms of the license agreement.

@par Specification Reference:
**/

#include <Library/HiiLib.h>
#include <Library/MemoryAllocationLib.h>
#include <Library/DebugLib.h>
#include "Base.h"
#include <Library/PciSegmentLib.h>
//[-start-200215-IB06462109-remove]//
//#include <SetupPrivate.h>
//[-end-200215-IB06462109-remove]//
#include <TcssInfo.h>
#include <PcieRegs.h>
#include "PlatformBoardId.h"
#include <DTbtInfoHob.h>
#include <Library/TbtCommonLib.h>
#include <Library/DTbtCommonLib.h>
#include <Guid/HobList.h>
#include <Library/TimerLib.h>
#include <Library/PcdLib.h>
#include <Library/DxeTbtSecurityLib.h>
#include "DiscreteTbtResourcesInitValues.h"
#include <Library/CmosAccessLib.h>
#include <CmosMap.h>
#include <TcssDataHob.h>
//[-start-200215-IB06462109-add]//
#include <Advance.h>
#include <SetupUtility.h>
#include <ChipsetSetupConfig.h>
//[-end-200215-IB06462109-add]//
//[-start-210527-IB16740141-add]//
#include <Library/CmosLib.h>
#include <ChipsetCmos.h>
//[-end-210527-IB16740141-add]//

#define LTSSM_FSM_RESTORE 0xC74

GLOBAL_REMOVE_IF_UNREFERENCED DTBT_INFO_HOB   *gDTbtInfoHob = NULL;
GLOBAL_REMOVE_IF_UNREFERENCED BOOLEAN         Entry = FALSE;
GLOBAL_REMOVE_IF_UNREFERENCED BOOLEAN         Usb4CmModeChangeEnable = FALSE;

/**
  Update the Rtd3 setting base on the board type.

  @param[IN]      UINT8                   Rtd3Support,
  @param[IN][OUT] SETUP_DATA              *SetupData

  @retval EFI_SUCCESS             Successfully completed updates.
  @retval Others                  Fail to update.
**/
EFI_STATUS
EFIAPI
UpdateRtd3Setting (
  IN      UINT8               Rtd3Support,
  IN OUT  SETUP_DATA          *SetupData
  );

EFI_STATUS
EFIAPI
DTbtFormCallBackFunction (
  IN CONST EFI_HII_CONFIG_ACCESS_PROTOCOL *This,
  IN EFI_BROWSER_ACTION                   Action,
  IN EFI_QUESTION_ID                      KeyValue,
  IN UINT8                                Type,
  IN EFI_IFR_TYPE_VALUE                   *Value,
  OUT EFI_BROWSER_ACTION_REQUEST          *ActionRequest
  )
{
#if FixedPcdGetBool (PcdDTbtEnable) == 1
  SA_SETUP                    *SaSetup;
  PCH_SETUP                   *PchSetup;
  SETUP_DATA                  *SetupData;
  UINTN                       VarSize;
  EFI_STATUS                  Status;
  BOOLEAN                     SaveOnExit;
//[-start-200215-IB06462109-remove]//
//  EFI_STRING                  RequestString;
//  EFI_STRING                  SaRequestString;
//  EFI_STRING                  PchRequestString;
//[-end-200215-IB06462109-remove]//
  UINT8                       Rootportselected;
  UINT8                       Index;
//[-start-200215-IB06462109-add]//
  SETUP_DATA                  *MyIfrNVData;
  SA_SETUP                    *MySaIfrNVData;
  PCH_SETUP                   *MyPchIfrNVData;
  BOOLEAN                     ActionStatus;
//[-end-200215-IB06462109-add]//

  Status                  = EFI_SUCCESS;
  SaveOnExit              = FALSE;
//[-start-200215-IB06462109-remove]//
//  RequestString           = NULL;
//  SaRequestString         = NULL;
//  PchRequestString        = NULL;
//[-end-200215-IB06462109-remove]//
  Rootportselected        = 0;

  DEBUG ((DEBUG_INFO, "DTbtFormCallBackFunction\n"));

  if (Action != EFI_BROWSER_ACTION_CHANGING && Action != EFI_BROWSER_ACTION_CHANGED)
    return EFI_UNSUPPORTED;

  if (Action == EFI_BROWSER_ACTION_CHANGING)
    return EFI_SUCCESS;

  if ((Value == NULL) || (ActionRequest == NULL))
    return EFI_INVALID_PARAMETER;

  VarSize = sizeof (SETUP_DATA);
  SetupData = AllocatePool (VarSize);
  ASSERT (SetupData != NULL);
  if (SetupData == NULL) {
    return EFI_OUT_OF_RESOURCES;
  }
//[-start-200215-IB06462109-modify]//
  // Init local variable "Setup"
  MyIfrNVData = (SETUP_DATA *)gRcSUBrowser->SetupDataSUBrowserData;
  CopyMem (SetupData, MyIfrNVData, VarSize);
  ActionStatus = HiiGetBrowserData (&gSetupVariableGuid, L"Setup", VarSize, (UINT8*) SetupData);
  if (ActionStatus == FALSE) {
    Status = EFI_ABORTED;
    ASSERT_EFI_ERROR(Status);
  } else {
    Status = EFI_SUCCESS;
  }
//[-end-200215-IB06462109-modify]//

  ASSERT_EFI_ERROR (Status);

  VarSize = sizeof (SA_SETUP);
  SaSetup = AllocatePool (VarSize);
  ASSERT (SaSetup != NULL);
  if (SaSetup == NULL) {
    return EFI_OUT_OF_RESOURCES;
  }
//[-start-200215-IB06462109-modify]//
  // Init local variable "SaSetup"
  MySaIfrNVData = (SA_SETUP *)gRcSUBrowser->SaSUBrowserData;
  CopyMem (SaSetup, MySaIfrNVData, VarSize);
  ActionStatus = HiiGetBrowserData (&gSaSetupVariableGuid, L"SaSetup", VarSize, (UINT8 *) SaSetup);
  if (ActionStatus == FALSE) {
    Status = EFI_ABORTED;
    ASSERT_EFI_ERROR(Status);
  } else {
    Status = EFI_SUCCESS;
  }
//[-end-200215-IB06462109-modify]//

  VarSize = sizeof (PCH_SETUP);
  PchSetup = AllocatePool (VarSize);
  ASSERT (PchSetup != NULL);
  if (PchSetup == NULL) {
    return EFI_OUT_OF_RESOURCES;
  }
//[-start-200215-IB06462109-modify]//
  // Init local variable "PchSetup"
  MyPchIfrNVData = (PCH_SETUP *)gRcSUBrowser->PchSUBrowserData;
  CopyMem (PchSetup, MyPchIfrNVData, VarSize);
  ActionStatus = HiiGetBrowserData (&gPchSetupVariableGuid, L"PchSetup", VarSize, (UINT8 *) PchSetup);
  if (ActionStatus == FALSE) {
    Status = EFI_ABORTED;
    ASSERT_EFI_ERROR(Status);
  } else {
    Status = EFI_SUCCESS;
  }
//[-end-200215-IB06462109-modify]//

  IoWrite16(0x80, (UINT16)KeyValue);

  DEBUG ((DEBUG_INFO, "Discrete Thunderbolt(TM) call back actions begin\n"));

  switch (KeyValue) {
    case KEY_DTBT_SUPPORT:
      DEBUG ((DEBUG_INFO, "KEY_DTBT_SUPPORT CallBack\n"));
      if (SetupData->DiscreteTbtSupport == 1) {
        SetupData->Rtd3Support = 1;
        UpdateRtd3Setting (SetupData->Rtd3Support, SetupData);
        SetupData->DTbtRtd3 = 1;
        SetupData->DTbtController[0] = 1;
      } else {
        SetupData->TbtBootOn = 0;
        SetupData->DTbtRtd3 = 0;
        SetupData->PciExpNative = 1;
        SetupData->DTbtController[0] = 0;
//[-start-200721-IB17040134-remove]//
//        RequestString   = HiiConstructRequestString (RequestString, OFFSET_OF (SETUP_DATA, TbtBootOn), sizeof (SetupData->TbtBootOn));
//        RequestString = HiiConstructRequestString (RequestString, OFFSET_OF (SETUP_DATA, PciExpNative), sizeof (SetupData->PciExpNative));
//[-end-200721-IB17040134-remove]//
      }
//[-start-200721-IB17040134-remove]//
//      RequestString = HiiConstructRequestString (RequestString, OFFSET_OF (SETUP_DATA, DTbtRtd3), sizeof (SetupData->DTbtRtd3));
//      RequestString = HiiConstructRequestString (RequestString, OFFSET_OF (SETUP_DATA, Rtd3Support), sizeof (SetupData->Rtd3Support));
//      RequestString = HiiConstructRequestString (RequestString, OFFSET_OF (SETUP_DATA, StorageRtd3Support), sizeof (SetupData->StorageRtd3Support));
//      RequestString   = HiiConstructRequestString (RequestString, OFFSET_OF (SETUP_DATA, DTbtController[0]), sizeof (SetupData->DTbtController[0]));
//[-end-200721-IB17040134-remove]//
      break;
    case KEY_DTBT_CONTROLLER0:
    case KEY_DTBT_CONTROLLER1:
    case KEY_DTBT_CONTROLLER2:
    case KEY_DTBT_CONTROLLER3:
      DEBUG ((DEBUG_INFO, " KEY_TBT_CONTROLLER0 / KEY_TBT_CONTROLLER1 / KEY_TBT_CONTROLLER2 / KEY_TBT_CONTROLLER3 CallBack\n"));
      Status = EfiGetSystemConfigurationTable (&gEfiHobListGuid, (VOID **) &gDTbtInfoHob);
      if (!EFI_ERROR (Status)) {
        DEBUG ((DEBUG_INFO, "Searching Thunderbolt(TM) information Hob...\n"));
        gDTbtInfoHob = GetNextGuidHob (&gDTbtInfoHobGuid, gDTbtInfoHob);
      }
      if (gDTbtInfoHob != NULL) {
        for (Index = 0; Index < MAX_DTBT_CONTROLLER_NUMBER; Index++) {
          if (SetupData->DTbtController[Index] != 0x01) {
            continue;
          }
          Rootportselected = (gDTbtInfoHob->DTbtControllerConfig[Index].PcieRpNumber - 1);
          if (gDTbtInfoHob->DTbtControllerConfig[Index].Type == 0x01) {
            //
            //if PCH-Rootport is supported for TBT, check for the Port which is selected
            //
            if (SetupData->DiscreteTbtSupport == 1) {
              PchSetup->PcieRootPortHPE[Rootportselected] = 1;
            } else {
              PchSetup->PcieRootPortHPE[Rootportselected] = 0;
            }
//[-start-200721-IB17040134-remove]//
//            PchRequestString = HiiConstructRequestString (PchRequestString, OFFSET_OF (PCH_SETUP, PcieRootPortHPE[Rootportselected]), sizeof (PchSetup->PcieRootPortHPE[Rootportselected]));
//[-end-200721-IB17040134-remove]//
          } else if (gDTbtInfoHob->DTbtControllerConfig[Index].Type == 0x02) {
            if (SetupData->DiscreteTbtSupport == 1) {
              SaSetup->PegRootPortHPE[Rootportselected] = 1;
              SaSetup->PegMaxPayload[Rootportselected] = 0;
              if (Rootportselected == 0) {
                SaSetup->Peg0Enable = 1;
                SaSetup->Peg0PowerDownUnusedLanes = 0;
              } else if (Rootportselected == 1) {
                SaSetup->Peg1Enable = 1;
                SaSetup->Peg1PowerDownUnusedLanes = 0;
              } else if (Rootportselected == 2) {
                SaSetup->Peg2Enable = 1;
                SaSetup->Peg2PowerDownUnusedLanes = 0;
              } else if (Rootportselected == 3) {
                SaSetup->Peg3Enable = 1;
                SaSetup->Peg3PowerDownUnusedLanes = 0;
              }
            } else {
              SaSetup->PegRootPortHPE[Rootportselected] = 0;
              SaSetup->PegMaxPayload[Rootportselected] = 1;
              if (Rootportselected == 0) {
                SaSetup->Peg0Enable = 0;
                SaSetup->Peg0PowerDownUnusedLanes = 1;
              } else if (Rootportselected == 1) {
                SaSetup->Peg1Enable = 0;
                SaSetup->Peg1PowerDownUnusedLanes = 1;
              } else if (Rootportselected == 2) {
                SaSetup->Peg2Enable = 0;
                SaSetup->Peg2PowerDownUnusedLanes = 1;
              } else if (Rootportselected == 3) {
                SaSetup->Peg3Enable = 0;
                SaSetup->Peg3PowerDownUnusedLanes = 1;
              }
            }
//[-start-200721-IB17040134-remove]//
//            SaRequestString = HiiConstructRequestString (SaRequestString, OFFSET_OF (SA_SETUP, Peg0Enable), sizeof (SaSetup->Peg0Enable));
//            SaRequestString = HiiConstructRequestString (SaRequestString, OFFSET_OF (SA_SETUP, Peg1Enable), sizeof (SaSetup->Peg1Enable));
//            SaRequestString = HiiConstructRequestString (SaRequestString, OFFSET_OF (SA_SETUP, Peg2Enable), sizeof (SaSetup->Peg2Enable));
//            SaRequestString = HiiConstructRequestString (SaRequestString, OFFSET_OF (SA_SETUP, Peg3Enable), sizeof (SaSetup->Peg3Enable));
//            SaRequestString = HiiConstructRequestString (SaRequestString, OFFSET_OF (SA_SETUP, PegRootPortHPE[Rootportselected]), sizeof (SaSetup->PegRootPortHPE[Index]));
//            SaRequestString = HiiConstructRequestString (SaRequestString, OFFSET_OF (SA_SETUP, Peg0PowerDownUnusedLanes), sizeof (SaSetup->Peg0PowerDownUnusedLanes));
//[-end-200721-IB17040134-remove]//
          }
        }
      }
      break;
    case KEY_DTBT_CONTROLLER_TYPE_0:
    case KEY_DTBT_CONTROLLER_TYPE_1:
    case KEY_DTBT_CONTROLLER_TYPE_2:
    case KEY_DTBT_CONTROLLER_TYPE_3:
      DEBUG ((DEBUG_INFO, "Discrete KEY_TBT_CONTROLLER_TYPE_0 / KEY_TBT_CONTROLLER_TYPE_1 / KEY_TBT_CONTROLLER_TYPE_2 / KEY_TBT_CONTROLLER_TYPE_3 CallBack\n"));
      for (Index = 0; Index < MAX_DTBT_CONTROLLER_NUMBER; Index++) {
        if (SetupData->DTbtController[Index] != 0x01) {
          continue;
        }
        //Create and Use SETUP option to select PEG Rootport. Default Should be RP 0.
        Rootportselected = 0; // Currently Only PEG 0 PORT is supported. Later It should be dependent on setup question.
        //
        // PCH Root port type
        //
        if (SetupData->DTbtControllerType[Index] == 0x01) {
          continue;
        //
        // PEG Root port type
        //
        } else if (SetupData->DTbtControllerType[Index] == 0x02) {
          if (SetupData->DiscreteTbtSupport == 1) {
            SaSetup->PegRootPortHPE[Rootportselected] = 1;
            SaSetup->PegMaxPayload[Rootportselected] = 0;
            if (Rootportselected == 0) {
              SaSetup->Peg0Enable = 1;
              SaSetup->Peg0PowerDownUnusedLanes = 0;
            } else if (Rootportselected == 1) {
              SaSetup->Peg1Enable = 1;
              SaSetup->Peg1PowerDownUnusedLanes = 0;
            } else if (Rootportselected == 2) {
              SaSetup->Peg2Enable = 1;
              SaSetup->Peg2PowerDownUnusedLanes = 0;
            } else if (Rootportselected == 3) {
              SaSetup->Peg3Enable = 1;
              SaSetup->Peg3PowerDownUnusedLanes = 0;
            }
          } else {
            SaSetup->PegRootPortHPE[Rootportselected] = 0;
            SaSetup->PegMaxPayload[Rootportselected] = 1;
            if (Rootportselected == 0) {
              SaSetup->Peg0Enable = 0;
              SaSetup->Peg0PowerDownUnusedLanes = 1;
            } else if (Rootportselected == 1) {
              SaSetup->Peg1Enable = 0;
              SaSetup->Peg1PowerDownUnusedLanes = 1;
            } else if (Rootportselected == 2) {
              SaSetup->Peg2Enable = 0;
              SaSetup->Peg2PowerDownUnusedLanes = 1;
            } else if (Rootportselected == 3) {
              SaSetup->Peg3Enable = 0;
              SaSetup->Peg3PowerDownUnusedLanes = 1;
            }
          }
//[-start-200721-IB17040134-remove]//
//          SaRequestString = HiiConstructRequestString (SaRequestString, OFFSET_OF (SA_SETUP, Peg0Enable), sizeof (SaSetup->Peg0Enable));
//          SaRequestString = HiiConstructRequestString (SaRequestString, OFFSET_OF (SA_SETUP, Peg1Enable), sizeof (SaSetup->Peg1Enable));
//          SaRequestString = HiiConstructRequestString (SaRequestString, OFFSET_OF (SA_SETUP, Peg2Enable), sizeof (SaSetup->Peg2Enable));
//          SaRequestString = HiiConstructRequestString (SaRequestString, OFFSET_OF (SA_SETUP, Peg3Enable), sizeof (SaSetup->Peg3Enable));
//          SaRequestString = HiiConstructRequestString (SaRequestString, OFFSET_OF (SA_SETUP, PegRootPortHPE[Rootportselected]), sizeof (SaSetup->PegRootPortHPE[Index]));
//          SaRequestString = HiiConstructRequestString (SaRequestString, OFFSET_OF (SA_SETUP, PegMaxPayload[Rootportselected]), sizeof (SaSetup->PegMaxPayload[Index]));
//          SaRequestString = HiiConstructRequestString (SaRequestString, OFFSET_OF (SA_SETUP, Peg0PowerDownUnusedLanes), sizeof (SaSetup->Peg0PowerDownUnusedLanes));
//[-end-200721-IB17040134-remove]//
        }
      }
      break;
    case KEY_DTBT_CONTROLLER0_HOSTROUTER:
      DEBUG ((DEBUG_INFO, "KEY_TBT_HOSTROUTER 0 CallBack\n"));
      //
      //As per the Bios implementation Guide
      //(Table 2.3: Host Router resource allocation/reservation scheme) ,
      //we need to allocate enough  resources based on Host router type
      //one port or two port
      //
      if (SetupData->DTbthostRouterPortNumber[0] == 2) {
        SetupData->DTbtPcieExtraBusRsvd[0] = DTBT_PCIE_EXTRA_BUS_RSVD_TWO_PORT_DEFAULT;
        SetupData->DTbtPcieMemRsvd[0] = DTBT_PCIE_MEM_RSVD_TWO_PORT_DEFAULT;
        SetupData->DTbtPcieMemAddrRngMax[0] = DTBT_PCIE_MEM_ADDRRNGMAX_TWO_PORT_DEFAULT;
        SetupData->DTbtPciePMemRsvd[0] = DTBT_PCIE_PMEM_RSVD_TWO_PORT_DEFAULT;
        SetupData->DTbtPciePMemAddrRngMax[0] = DTBT_PCIE_PMEM_ADDRRNGMAX_TWO_PORT_DEFAULT;
      } else {
        SetupData->DTbtPcieExtraBusRsvd[0] = DTBT_PCIE_EXTRA_BUS_RSVD_ONE_PORT_DEFAULT;
        SetupData->DTbtPcieMemRsvd[0] = DTBT_PCIE_MEM_RSVD_ONE_PORT_DEFAULT;
        SetupData->DTbtPcieMemAddrRngMax[0] = DTBT_PCIE_MEM_ADDRRNGMAX_ONE_PORT_DEFAULT;
        SetupData->DTbtPciePMemRsvd[0] = DTBT_PCIE_PMEM_RSVD_ONE_PORT_DEFAULT;
        SetupData->DTbtPciePMemAddrRngMax[0] = DTBT_PCIE_PMEM_ADDRRNGMAX_ONE_PORT_DEFAULT;
      }
//[-start-200721-IB17040134-remove]//
//     RequestString = HiiConstructRequestString (RequestString, OFFSET_OF (SETUP_DATA, DTbtPcieExtraBusRsvd[0]), sizeof (SetupData->DTbtPcieExtraBusRsvd[0]));
//     RequestString = HiiConstructRequestString (RequestString, OFFSET_OF (SETUP_DATA, DTbtPcieMemRsvd[0]), sizeof (SetupData->DTbtPcieMemRsvd[0]));
//     RequestString = HiiConstructRequestString (RequestString, OFFSET_OF (SETUP_DATA, DTbtPcieMemAddrRngMax[0]), sizeof (SetupData->DTbtPcieMemAddrRngMax[0]));
//     RequestString = HiiConstructRequestString (RequestString, OFFSET_OF (SETUP_DATA, DTbtPciePMemRsvd[0]), sizeof (SetupData->DTbtPciePMemRsvd[0]));
//     RequestString = HiiConstructRequestString (RequestString, OFFSET_OF (SETUP_DATA, DTbtPciePMemAddrRngMax[0]), sizeof (SetupData->DTbtPciePMemAddrRngMax[0]));
//[-end-200721-IB17040134-remove]//

      break;
    case KEY_DTBT_CONTROLLER1_HOSTROUTER:
    //case KEY_DTBT_CONTROLLER2_HOSTROUTER:
    //case KEY_DTBT_CONTROLLER3_HOSTROUTER:
      DEBUG ((DEBUG_INFO, "KEY_TBT_HOSTROUTER 1 CallBack\n"));
      //
      //As per the Bios implementation Guide
      //(Table 2.3: Host Router resource allocation/reservation scheme) ,
      //we need to allocate enough  resources based on Host router type
      //one port or two port
      //
      if (SetupData->DTbthostRouterPortNumber[1] == 2) {
        SetupData->DTbtPcieExtraBusRsvd[1] = DTBT_PCIE_EXTRA_BUS_RSVD_TWO_PORT_DEFAULT;
        SetupData->DTbtPcieMemRsvd[1] = DTBT_PCIE_MEM_RSVD_TWO_PORT_DEFAULT;
        SetupData->DTbtPcieMemAddrRngMax[1] = DTBT_PCIE_MEM_ADDRRNGMAX_TWO_PORT_DEFAULT;
        SetupData->DTbtPciePMemRsvd[1] = DTBT_PCIE_PMEM_RSVD_TWO_PORT_DEFAULT;
        SetupData->DTbtPciePMemAddrRngMax[1] = DTBT_PCIE_PMEM_ADDRRNGMAX_TWO_PORT_DEFAULT;
      } else {
        SetupData->DTbtPcieExtraBusRsvd[1] = DTBT_PCIE_EXTRA_BUS_RSVD_ONE_PORT_DEFAULT;
        SetupData->DTbtPcieMemRsvd[1] = DTBT_PCIE_MEM_RSVD_ONE_PORT_DEFAULT;
        SetupData->DTbtPcieMemAddrRngMax[1] = DTBT_PCIE_MEM_ADDRRNGMAX_ONE_PORT_DEFAULT;
        SetupData->DTbtPciePMemRsvd[1] = DTBT_PCIE_PMEM_RSVD_ONE_PORT_DEFAULT;
        SetupData->DTbtPciePMemAddrRngMax[1] = DTBT_PCIE_PMEM_ADDRRNGMAX_ONE_PORT_DEFAULT;
      }
//[-start-200721-IB17040134-remove]//
//     RequestString = HiiConstructRequestString (RequestString, OFFSET_OF (SETUP_DATA, DTbtPcieExtraBusRsvd[1]), sizeof (SetupData->DTbtPcieExtraBusRsvd[1]));
//     RequestString = HiiConstructRequestString (RequestString, OFFSET_OF (SETUP_DATA, DTbtPcieMemRsvd[1]), sizeof (SetupData->DTbtPcieMemRsvd[1]));
//     RequestString = HiiConstructRequestString (RequestString, OFFSET_OF (SETUP_DATA, DTbtPcieMemAddrRngMax[1]), sizeof (SetupData->DTbtPcieMemAddrRngMax[1]));
//     RequestString = HiiConstructRequestString (RequestString, OFFSET_OF (SETUP_DATA, DTbtPciePMemRsvd[1]), sizeof (SetupData->DTbtPciePMemRsvd[1]));
//     RequestString = HiiConstructRequestString (RequestString, OFFSET_OF (SETUP_DATA, DTbtPciePMemAddrRngMax[1]), sizeof (SetupData->DTbtPciePMemAddrRngMax[1]));
//[-end-200721-IB17040134-remove]//
      break;
    default:
//[-start-200215-IB06462109-add]//
      if (SetupData != NULL) {
        FreePool (SetupData);
      }
      if (SaSetup != NULL) {
        FreePool (SaSetup);
      }
      if (PchSetup != NULL) {
        FreePool (PchSetup);
      }
//[-end-200215-IB06462109-add]//
      DEBUG ((DEBUG_INFO, "Continue\n"));
      break;
  }

//[-start-200215-IB06462109-add]//
#if 0
//[-end-200215-IB06462109-add]//
  DEBUG ((DEBUG_INFO, "Thunderbolt(TM) call back - HiiSetBrowserData begin\n"));
  if (RequestString != NULL) {
    VarSize = sizeof (SETUP_DATA);
    if (!HiiSetBrowserData (&gSetupVariableGuid, L"Setup", VarSize, (UINT8 *) SetupData, RequestString)) {
      Status = EFI_NOT_FOUND;
    }
    ASSERT_EFI_ERROR (Status);
    FreePool (RequestString);
  }
  if (SaRequestString != NULL) {
    VarSize = sizeof (SA_SETUP);
    if (!HiiSetBrowserData (&gSaSetupVariableGuid, L"SaSetup", VarSize, (UINT8 *) SaSetup, SaRequestString)) {
      Status = EFI_NOT_FOUND;
    }
    ASSERT_EFI_ERROR (Status);
    FreePool (SaRequestString);
  }
  if (PchRequestString != NULL) {
    VarSize = sizeof (PCH_SETUP);
    if (!HiiSetBrowserData (&gPchSetupVariableGuid, L"PchSetup", VarSize, (UINT8 *) PchSetup, PchRequestString)) {
      Status = EFI_NOT_FOUND;
    }
    ASSERT_EFI_ERROR (Status);
    FreePool (PchRequestString);
  }
  DEBUG ((DEBUG_INFO, "Thunderbolt(TM) call back - HiiSetBrowserData end\n"));
  FreePool (SaSetup);
  FreePool (PchSetup);
  FreePool (SetupData);

  *ActionRequest = EFI_BROWSER_ACTION_REQUEST_NONE;
  if (SaveOnExit)
    *ActionRequest = EFI_BROWSER_ACTION_REQUEST_SUBMIT;
//[-start-200215-IB06462109-add]//
#endif
//[-end-200215-IB06462109-add]//

//[-start-200215-IB06462109-add]//
  VarSize = sizeof (SETUP_DATA);
  CopyMem (MyIfrNVData, SetupData, VarSize);
  if (SetupData != NULL) {
    FreePool (SetupData);
  }

  VarSize = sizeof (SA_SETUP);
  CopyMem (MySaIfrNVData, SaSetup, VarSize);
  if (SaSetup != NULL) {
    FreePool (SaSetup);
  }

  VarSize = sizeof (PCH_SETUP);
  CopyMem (MyPchIfrNVData, PchSetup, VarSize);
  if (PchSetup != NULL) {
    FreePool (PchSetup);
  }
//[-end-200215-IB06462109-add]//

  DEBUG ((DEBUG_INFO, "Thunderbolt(TM) call back actions end\n"));
#endif
  return EFI_SUCCESS;
}

/**

   This function is called to set dTBT power managment options based on user's selection

   @param  This                   Points to the EFI_HII_CONFIG_ACCESS_PROTOCOL.
   @param  Action                 Specifies the type of action taken by the browser.
   @param  QuestionId             A unique value which is sent to the original
                                  exporting driver so that it can identify the type
                                  of data to expect. The format of the data tends to
                                  vary based on the opcode that generated the callback.
   @param  Type                   The type of value for the question.
   @param  Value                  A pointer to the data being sent to the original
                                  exporting driver.
   @param  ActionRequest          On return, points to the action requested by the
                                  callback function.

   @retval EFI_SUCCESS            The callback successfully handled the action.
   @retval EFI_OUT_OF_RESOURCES   Not enough storage is available to hold the
                                  variable and its data.
   @retval EFI_DEVICE_ERROR       The variable could not be saved.
   @retval EFI_UNSUPPORTED        The specified Action is not supported by the
                                  callback.
 **/
EFI_STATUS
EFIAPI
TbtOsSelectorFormCallBackFunction (
  IN CONST EFI_HII_CONFIG_ACCESS_PROTOCOL *This,
  IN EFI_BROWSER_ACTION                   Action,
  IN EFI_QUESTION_ID                      KeyValue,
  IN UINT8                                Type,
  IN EFI_IFR_TYPE_VALUE                   *Value,
  OUT EFI_BROWSER_ACTION_REQUEST          *ActionRequest
  )
{
  EFI_STATUS                  Status;
  Status = EFI_SUCCESS;

#if FixedPcdGetBool (PcdDTbtEnable) == 1
  SETUP_DATA                 *SetupData;
  EFI_STRING                  RequestString;
  UINTN                       VarSize;
//[-start-200215-IB06462109-add]//
  SETUP_DATA                  *MyIfrNVData;
  BOOLEAN                     ActionStatus;
//[-end-200215-IB06462109-add]//

  RequestString = NULL;
  VarSize = sizeof (SETUP_DATA);
  SetupData = NULL;

  DEBUG ((DEBUG_INFO, "TbtOsSelectorFormCallBackFunction Start\n"));

  if (Action != EFI_BROWSER_ACTION_CHANGED) {
    DEBUG ((DEBUG_INFO, "Action != EFI_BROWSER_ACTION_CHANGED\n"));
    return EFI_UNSUPPORTED;
  }

  SetupData = AllocateZeroPool (VarSize);
  if (SetupData == NULL) {
    DEBUG ((DEBUG_ERROR, "Not enough memory is available\n"));
    return EFI_OUT_OF_RESOURCES;
  }
//[-start-200215-IB06462109-modify]//
  // Init local variable "Setup"
  MyIfrNVData = (SETUP_DATA *)gRcSUBrowser->SetupDataSUBrowserData;
  CopyMem (SetupData, MyIfrNVData, VarSize);
  ActionStatus = HiiGetBrowserData (&gSetupVariableGuid, L"Setup", VarSize, (UINT8*) SetupData);
  if (ActionStatus == FALSE) {
    DEBUG ((DEBUG_ERROR, "The variable could not be read\n"));
    return EFI_DEVICE_ERROR;
  }
//[-end-200215-IB06462109-modify]//

  switch (KeyValue) {
    case KEY_TBT_OS_SELECTOR:
      DEBUG ((DEBUG_INFO, "KEY_TBT_OS_SELECTOR CallBack\n"));
      if (SetupData->Win10Support == 0) {
        SetupData->DTbtRtd3 = 0;
        SetupData->PepTcss = 0;
        DEBUG ((DEBUG_INFO, "Disable DTbtRtd3 and PepTcss\n"));
      } else if (SetupData->Win10Support == 2) {
        SetupData->DTbtRtd3 = 1;
        SetupData->PepTcss = 1;
        DEBUG ((DEBUG_INFO, "Enable DTbtRtd3 and PepTcss\n"));
      }
//[-start-200721-IB17040134-remove]//
//      RequestString = HiiConstructRequestString (RequestString, OFFSET_OF (SETUP_DATA, DTbtRtd3), sizeof (SetupData->DTbtRtd3));
//      RequestString = HiiConstructRequestString (RequestString, OFFSET_OF (SETUP_DATA, PepTcss), sizeof (SetupData->PepTcss));
//[-start-200721-IB17040134-remove]//
      break;
    default:
      Status = EFI_DEVICE_ERROR;
      DEBUG ((DEBUG_INFO, "Continue\n"));
  }

  if (RequestString != NULL) {
    if (!HiiSetBrowserData (&gSetupVariableGuid, L"Setup", VarSize, (UINT8 *) SetupData, RequestString)) {
      DEBUG ((DEBUG_ERROR, "The variable could not be saved\n"));
      Status = EFI_DEVICE_ERROR;
    }
    ASSERT_EFI_ERROR (Status);
    FreePool (RequestString);
  }

  FreePool (SetupData);

  DEBUG ((DEBUG_INFO, "TbtOsSelectorFormCallBackFunction End\n"));
#endif
  return Status;
}

/**

   This function is called to set CM mode value to CMOS

   @param  This                   Points to the EFI_HII_CONFIG_ACCESS_PROTOCOL.
   @param  Action                 Specifies the type of action taken by the browser.
   @param  QuestionId             A unique value which is sent to the original
                                  exporting driver so that it can identify the type
                                  of data to expect. The format of the data tends to
                                  vary based on the opcode that generated the callback.
   @param  Type                   The type of value for the question.
   @param  Value                  A pointer to the data being sent to the original
                                  exporting driver.
   @param  ActionRequest          On return, points to the action requested by the
                                  callback function.

   @retval EFI_SUCCESS            The callback successfully handled the action.
   @retval EFI_OUT_OF_RESOURCES   Not enough storage is available to hold the
                                  variable and its data.
   @retval EFI_UNSUPPORTED        The specified Action is not supported by the
                                  callback.
   @retval EFI_DEVICE_ERROR       The variable could not be saved to browser.
   @retval Other                  Other errors occur.
 **/
EFI_STATUS
EFIAPI
Usb4CmModeCallBack (
  IN CONST EFI_HII_CONFIG_ACCESS_PROTOCOL *This,
  IN EFI_BROWSER_ACTION                   Action,
  IN EFI_QUESTION_ID                      KeyValue,
  IN UINT8                                Type,
  IN EFI_IFR_TYPE_VALUE                   *Value,
  OUT EFI_BROWSER_ACTION_REQUEST          *ActionRequest
  )
{
  SETUP_DATA                 *SetupData;
  UINTN                      SetupDataVarSize;
  UINT32                     SetupDataVarAttributes;
  UINT8                      Usb4CmModeCmosValue;
  UINT8                      TempITbtRtd3;
  EFI_STATUS                 Status;

  DEBUG ((EFI_D_INFO, "Usb4CmModeCallBack() Start\n"));

  if (Action != EFI_BROWSER_ACTION_CHANGED && Action != EFI_BROWSER_ACTION_SUBMITTED) {
    DEBUG ((EFI_D_INFO, "Unsupported Action\n"));
    return EFI_UNSUPPORTED;
  }

  switch (Action) {
    case EFI_BROWSER_ACTION_CHANGED:
      Usb4CmModeChangeEnable = TRUE;
      break;

    case EFI_BROWSER_ACTION_SUBMITTED:

      SetupDataVarAttributes = 0;
      SetupData = NULL;
      SetupDataVarSize = sizeof (SETUP_DATA);
      SetupData = AllocatePool (SetupDataVarSize);
      if (SetupData == NULL) {
        DEBUG ((EFI_D_ERROR, "Usb4CmModeCallBack() - Allocate setup variable memory fail\n"));
        return EFI_OUT_OF_RESOURCES;
      }

      Status = gRT->GetVariable (
                      L"Setup",
                      &gSetupVariableGuid,
                      &SetupDataVarAttributes,
                      &SetupDataVarSize,
                      SetupData
                      );
      if (EFI_ERROR (Status)) {
        DEBUG ((DEBUG_ERROR, "Get SetupData fail, Status = %r\n", Status));
        FreePool (SetupData);
        return Status;
      }

      if (Usb4CmModeChangeEnable == TRUE) {
        Usb4CmModeChangeEnable = FALSE;

        //
        // OS dependent mode -> set SW CM value to CMOS
        //
        if (SetupData->Usb4CmMode == SETUP_OPTION_USB4_CM_MODE_OS) {
//[-start-210527-IB16740141-modify]//
//          CmosWrite8 (CMOS_USB4_CM_MODE_REG, USB4_CM_MODE_SW_CM);
          WriteExtCmos8 (R_XCMOS_INDEX, R_XCMOS_DATA, CMOS_USB4_CM_MODE_REG_CHP, USB4_CM_MODE_SW_CM);
//[-end-210527-IB16740141-modify]//
        }
      }

//[-start-210709-IB16740147-modify]//
//      Usb4CmModeCmosValue = CmosRead8 (CMOS_USB4_CM_MODE_REG);
      Usb4CmModeCmosValue = ReadExtCmos8 (R_XCMOS_INDEX, R_XCMOS_DATA, CMOS_USB4_CM_MODE_REG_CHP);
//[-end-210709-IB16740147-modify]//
      if ((SetupData->Usb4CmMode == SETUP_OPTION_USB4_CM_MODE_FW) ||
          ((SetupData->Usb4CmMode == SETUP_OPTION_USB4_CM_MODE_OS) && (Usb4CmModeCmosValue == USB4_CM_MODE_FW_CM))) {
        TempITbtRtd3 = SetupData->ITbtRtd3;
      } else {
        TempITbtRtd3 = 1;
      }

      if (SetupData->ITbtRtd3 != TempITbtRtd3) {
        SetupData->ITbtRtd3 = TempITbtRtd3;
        Status = gRT->SetVariable (
                        L"Setup",
                        &gSetupVariableGuid,
                        SetupDataVarAttributes,
                        SetupDataVarSize,
                        SetupData
                        );
        if (EFI_ERROR (Status)) {
          DEBUG ((DEBUG_ERROR, "Store SetupData fail, Status = %r\n", Status));
          FreePool (SetupData);
          return Status;
        }
      }

      FreePool (SetupData);

      break;

    default:
      break;
  }

  DEBUG ((EFI_D_INFO, "Usb4CmModeCallBack() End\n"));

  return EFI_SUCCESS;
}
