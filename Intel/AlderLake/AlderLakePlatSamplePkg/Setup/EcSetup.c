/** @file

;******************************************************************************
;* Copyright (c) 2012 - 2020, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/
/** @file
  Extracts system info from EC, for Mobile systems.

@copyright
  INTEL CONFIDENTIAL
  Copyright 1999 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains a 'Sample Driver' and is licensed as such under the terms
  of your license agreement with Intel or your vendor. This file may be modified
  by the user, subject to the additional terms of the license agreement.

@par Specification Reference:
**/


//[-start-200215-IB06462109-remove]//
//#include <SetupPrivate.h>
//[-end-200215-IB06462109-remove]//
#include <Library/EcMiscLib.h>
#include "Setup.h"
#include <Library/EcLib.h>
#include <Library/DebugLib.h>
#include <Library/TimerLib.h>
//[-start-200215-IB06462109-add]//
#include <Library/BaseOemSvcChipsetLib.h>

#include <Protocol/SystemMonitor.h>
#include <ChipsetSetupConfig.h>
#include "PlatformInfo.h"
#include "EcSetup.h"
//[-end-200215-IB06462109-add]//

#define STALL_TIME                          1000000    // 1,000,000 microseconds = 1 second

/**

  @param[in]  Reg
  @param[out] RetValue

  @retval    EFI_STATUS
**/
EFI_STATUS
ReadEcRegister(
    IN  UINT8  Reg,
    OUT UINT8  *RetValue
)
{
  EFI_STATUS Status;
//[-start-200215-IB06462109-add]//
  EFI_STATUS CommandStatus;
//[-end-200215-IB06462109-add]//
  UINT8      DataBuffer[1];

  DataBuffer[0] = Reg;
//[-start-200215-IB06462109-modify]//
//  Status = ReadEcRam (DataBuffer);
  DEBUG_OEM_SVC ((DEBUG_INFO, "Base OemChipsetServices Call: OemSvcEcReadEcRam \n"));
  Status = OemSvcEcReadEcRam (&CommandStatus, DataBuffer);
  DEBUG_OEM_SVC ((DEBUG_INFO, "Base OemChipsetServices OemSvcEcReadEcRam Status: %r\n", Status));
  if (CommandStatus == EFI_SUCCESS) {
    *RetValue = DataBuffer[0];
  }
  return CommandStatus;
//[-end-200215-IB06462109-modify]//
}

/**

@param[in]  RegLsb   LSB Register value.
@param[out] RetValue Temperature in 10ths of degree Celsius.

@retval     EFI_STATUS
**/

EFI_STATUS
GetSensorTemperatures(
    IN UINT8 RegLsb,
    OUT UINT16 *RetValue
)
{
  EFI_STATUS Status;
//[-start-200215-IB06462109-add]//
  EFI_STATUS CommandStatus;
//[-end-200215-IB06462109-add]//
  UINT8      DataBuffer[2];
  DataBuffer[0] = RegLsb; // Read LSB
//[-start-200215-IB06462109-modify]//
//  Status = ReadEcRam(&DataBuffer[0]);
  DEBUG_OEM_SVC ((DEBUG_INFO, "Base OemChipsetServices Call: OemSvcEcReadEcRam \n"));
  Status = OemSvcEcReadEcRam (&CommandStatus, &DataBuffer[0]);
  DEBUG_OEM_SVC ((DEBUG_INFO, "Base OemChipsetServices OemSvcEcReadEcRam Status: %r\n", Status));
//[-end-200215-IB06462109-modify]//

  DataBuffer[1] = RegLsb +1; // Read MSB
//[-start-200215-IB06462109-modify]//
//  Status = ReadEcRam(&DataBuffer[1]);
  DEBUG_OEM_SVC ((DEBUG_INFO, "Base OemChipsetServices Call: OemSvcEcReadEcRam \n"));
  Status = OemSvcEcReadEcRam (&CommandStatus, &DataBuffer[1]);
  DEBUG_OEM_SVC ((DEBUG_INFO, "Base OemChipsetServices OemSvcEcReadEcRam Status: %r\n", Status));
//[-end-200215-IB06462109-modify]//

  *RetValue = (DataBuffer[0] | (DataBuffer[1] << 8));
//[-start-200215-IB06462109-modify]//
   return CommandStatus;
//[-end-200215-IB06462109-modify]//
}

/**
  Returns system monitor values from the EC.

  @param[in]  KeyValue
  @param[out] MonitorValues  Pointer to a structure to return the monitor values in.

  @retval     EFI_INVALID_PARAMETER
  @retval     EFI_SUCCESS
**/
EFI_STATUS
GetSystemMonitorValues (
    IN  UINT16                             KeyValue,
    OUT MOBILE_SYSTEM_MONITOR_INFO*        MonitorValues
  )
{
  UINT16                    Data16;
  UINT8                     Data;
  UINT8                     Register;

  // Check input parameters
  if (MonitorValues == NULL) {
    return EFI_INVALID_PARAMETER;
  }
  if (!PcdGetBool (PcdEcPresent)) {
    return EFI_SUCCESS;
  }

  switch(KeyValue)
  {
    case THERMAL_SENSOR_1_KEY:
    {
      //
      // Read Thermal Sensor from EC
      //
      Register = EC_REG_TSR1_LSB;
      Data = 0;
      GetSensorTemperatures(Register, &Data16);
      MonitorValues->EC_TSR1 = Data16;
      break;
    }
    case THERMAL_SENSOR_2_KEY:
    {
      //
      // Read Thermal Sensor from EC
      //
      Register = EC_REG_TSR2_LSB;
      Data = 0;
      GetSensorTemperatures (Register, &Data16);
      MonitorValues->EC_TSR2 = Data16;
      break;
    }
    case THERMAL_SENSOR_3_KEY:
    {
      //
      // Read Thermal Sensor from EC
      //
      Register = EC_REG_TSR3_LSB;
      Data = 0;
      GetSensorTemperatures (Register, &Data16);
      MonitorValues->EC_TSR3 = Data16;
      break;
    }
    case THERMAL_SENSOR_4_KEY:
    {
      //
      // Read Thermal Sensor from EC
      //
      Register = EC_REG_TSR4_LSB;
      Data = 0;
      GetSensorTemperatures (Register, &Data16);
      MonitorValues->EC_TSR4 = Data16;
      break;
    }
    case THERMAL_SENSOR_5_KEY:
    {
      //
      // Read Thermal Sensor from EC
      //
      Register = EC_REG_TSR5_LSB;
      Data = 0;
      GetSensorTemperatures (Register, &Data16);
      MonitorValues->EC_TSR5 = Data16;
      break;
    }
    case CPU_FAN_KEY :
    {
      //
      // Read CPU Fan Speed from EC
      //
      Register = EC_REG_CPU_FAN_SPEED + 1;
      ReadEcRegister (Register, &Data);
      MonitorValues->CPUFanSpeed = Data;
      Register = EC_REG_CPU_FAN_SPEED;
      ReadEcRegister (Register, &Data);
      MonitorValues->CPUFanSpeed = (MonitorValues->CPUFanSpeed << 8) + Data;
      break;
     }
    case PCH_DTS_TEMP_KEY :
    {
      //
      // Read PCH DTS Temp from EC
      //
      Register = EC_REG_PCH_DTS_TEMP;
      ReadEcRegister (Register, &Data);
      MonitorValues->PCHDTSTemp = Data;
          break;
        }
    case TS_ON_DIMM0_TEMP_KEY :
    {
      //
      // Read TS-on-DIMM0 Temp from EC
      //
      Register = EC_REG_TS_ON_DIMM0_TEMP;
      ReadEcRegister (Register, &Data);
      MonitorValues->TSonDimm0Temp = Data;
          break;
        }
    case TS_ON_DIMM1_TEMP_KEY :
    {
      //
      // Read TS-on-DIMM1 Temp from EC
      //
      Register = EC_REG_TS_ON_DIMM1_TEMP;
      ReadEcRegister (Register, &Data);
      MonitorValues->TSonDimm1Temp = Data;
      break;
    }
    default:
    break;
  }

  return EFI_SUCCESS;
}


/**

  @param[in] HiiHandle
  @param[in] Class
**/
VOID
InitHhmMobileStrings (
  IN EFI_HII_HANDLE HiiHandle,
  IN UINT16         Class
  )
{
  UINT16                        Value1;
  UINT16                        Value2;
  MOBILE_SYSTEM_MONITOR_INFO    MonitorValues = { 0 };

  if (Class != ADVANCED_FORM_SET_CLASS) {
    return ;
  }

  GetSystemMonitorValues (THERMAL_SENSOR_1_KEY, &MonitorValues);
  GetSystemMonitorValues (THERMAL_SENSOR_2_KEY, &MonitorValues);
  GetSystemMonitorValues (THERMAL_SENSOR_3_KEY, &MonitorValues);
  GetSystemMonitorValues (THERMAL_SENSOR_4_KEY, &MonitorValues);
  GetSystemMonitorValues (THERMAL_SENSOR_5_KEY, &MonitorValues);

  GetSystemMonitorValues (CPU_FAN_KEY, &MonitorValues);
  GetSystemMonitorValues (PCH_DTS_TEMP_KEY, &MonitorValues);
  GetSystemMonitorValues (TS_ON_DIMM0_TEMP_KEY, &MonitorValues);
  GetSystemMonitorValues (TS_ON_DIMM1_TEMP_KEY, &MonitorValues);

  Value1 = (UINT16)(MonitorValues.EC_TSR1 / 10);
  Value2 = (UINT16)(MonitorValues.EC_TSR1 % 10);
  InitString(HiiHandle, STRING_TOKEN(STR_THERMAL_SENSOR_1_VALUE), L"%d.%01d C", Value1, Value2);

  Value1 = (UINT16)(MonitorValues.EC_TSR2 / 10);
  Value2 = (UINT16)(MonitorValues.EC_TSR2 % 10);
  InitString(HiiHandle, STRING_TOKEN(STR_THERMAL_SENSOR_2_VALUE), L"%d.%01d C", Value1, Value2);

  Value1 = (UINT16)(MonitorValues.EC_TSR3 / 10);
  Value2 = (UINT16)(MonitorValues.EC_TSR3 % 10);
  InitString(HiiHandle, STRING_TOKEN(STR_THERMAL_SENSOR_3_VALUE), L"%d.%01d C", Value1, Value2);

  Value1 = (UINT16)(MonitorValues.EC_TSR4 / 10);
  Value2 = (UINT16)(MonitorValues.EC_TSR4 % 10);
  InitString(HiiHandle, STRING_TOKEN(STR_THERMAL_SENSOR_4_VALUE), L"%d.%01d C", Value1, Value2);

  Value1 = (UINT16)(MonitorValues.EC_TSR5 / 10);
  Value2 = (UINT16)(MonitorValues.EC_TSR5 % 10);
  InitString(HiiHandle, STRING_TOKEN(STR_THERMAL_SENSOR_5_VALUE), L"%d.%01d C", Value1, Value2);

  InitString(HiiHandle, STRING_TOKEN(STR_CPU_FAN_VALUE), L"%d rpm", MonitorValues.CPUFanSpeed);
  InitString(HiiHandle, STRING_TOKEN(STR_PCH_DTS_TEMP_VALUE), L"%d C", MonitorValues.PCHDTSTemp);

}

/**

  CallBack to Update StateAfterG3 to EC while StateAfterG3 value changed

**/
EFI_STATUS StateAfterG3CallBackFunction(
  IN CONST EFI_HII_CONFIG_ACCESS_PROTOCOL *This,
  IN EFI_BROWSER_ACTION                   Action,
  IN EFI_QUESTION_ID                      KeyValue,
  IN UINT8                                Type,
  IN EFI_IFR_TYPE_VALUE                   *Value,
  OUT EFI_BROWSER_ACTION_REQUEST          *ActionRequest
  )
{
  EFI_STATUS              Status;
  PCH_SETUP               PchSetup;
  UINTN                   DataSize;

  if (Action != EFI_BROWSER_ACTION_SUBMITTED) {
    return EFI_UNSUPPORTED;
  }

  DataSize = sizeof (PCH_SETUP);
  Status = gRT->GetVariable (
                  (CHAR16 *) L"PchSetup",
                  &gPchSetupVariableGuid,
                  (UINT32 *) NULL,
                  &DataSize,
                  &PchSetup
                  );
  ASSERT_EFI_ERROR (Status);
//[-start-200831-IB17800091-add]//
#if FeaturePcdGet (PcdUseCrbEcFlag)
//[-end-200831-IB17800091-add]//
  Status = SendEcCommand (EC_C_SET_G3TOS5);
  if (EFI_ERROR (Status)) {
    return EFI_UNSUPPORTED;
  }

  Status = SendEcData (PchSetup.StateAfterG3);
//[-start-200831-IB17800091-add]//
#endif  
//[-end-200831-IB17800091-add]//
  return Status;
}
/**

  @param[in] HiiHandle
  @param[in] Class
  @param[in] SubClass
  @param[in] Key
**/
VOID
HhmMobileCallBack (
  IN EFI_HII_HANDLE HiiHandle,
  IN UINT16 Class,
  IN UINT16 SubClass,
  IN UINT16 Key
  )
{
  MOBILE_SYSTEM_MONITOR_INFO    MonitorValues = { 0 };
  UINT16                        Value1;
  UINT16                        Value2;
  UINT8                         Index;
  UINT16                        KeyList[] = {
    THERMAL_SENSOR_1_KEY,
    THERMAL_SENSOR_2_KEY,
    THERMAL_SENSOR_3_KEY,
    THERMAL_SENSOR_4_KEY,
    THERMAL_SENSOR_5_KEY,
    CPU_FAN_KEY,
    PCH_DTS_TEMP_KEY,
    TS_ON_DIMM0_TEMP_KEY,
    TS_ON_DIMM1_TEMP_KEY
  };

  if (Key != HHM_SETUP_REFRESH_KEY) {
    return;
  }

  for (Index = 0; Index < sizeof (KeyList) / sizeof (UINT16); Index++) {
    Key = KeyList[Index];
    GetSystemMonitorValues (Key, &MonitorValues);

    switch(Key)
    {
        case THERMAL_SENSOR_1_KEY:
        {
            Value1 = (UINT16)(MonitorValues.EC_TSR1 / 10);
            Value2 = (UINT16)(MonitorValues.EC_TSR1 % 10);
            InitString(HiiHandle, STRING_TOKEN(STR_THERMAL_SENSOR_1_VALUE), L"%d.%01d C", Value1, Value2);
            break;
        }
        case THERMAL_SENSOR_2_KEY:
        {
            Value1 = (UINT16)(MonitorValues.EC_TSR2 / 10);
            Value2 = (UINT16)(MonitorValues.EC_TSR2 % 10);
            InitString(HiiHandle, STRING_TOKEN(STR_THERMAL_SENSOR_2_VALUE), L"%d.%01d C", Value1, Value2);
            break;
        }
        case THERMAL_SENSOR_3_KEY:
        {
            Value1 = (UINT16)(MonitorValues.EC_TSR3 / 10);
            Value2 = (UINT16)(MonitorValues.EC_TSR3 % 10);
            InitString(HiiHandle, STRING_TOKEN(STR_THERMAL_SENSOR_3_VALUE), L"%d.%01d C", Value1, Value2);
            break;
        }
        case THERMAL_SENSOR_4_KEY:
        {
            Value1 = (UINT16)(MonitorValues.EC_TSR4 / 10);
            Value2 = (UINT16)(MonitorValues.EC_TSR4 % 10);
            InitString(HiiHandle, STRING_TOKEN(STR_THERMAL_SENSOR_4_VALUE), L"%d.%01d C", Value1, Value2);
            break;
        }
        case THERMAL_SENSOR_5_KEY:
        {
            Value1 = (UINT16)(MonitorValues.EC_TSR5 / 10);
            Value2 = (UINT16)(MonitorValues.EC_TSR5 % 10);
            InitString(HiiHandle, STRING_TOKEN(STR_THERMAL_SENSOR_5_VALUE), L"%d.%01d C", Value1, Value2);
            break;
        }
        case CPU_FAN_KEY :
        {
            InitString(HiiHandle, STRING_TOKEN(STR_CPU_FAN_VALUE), L"%d rpm", MonitorValues.CPUFanSpeed);
            break;
        }
        case PCH_DTS_TEMP_KEY :
        {
            InitString(HiiHandle, STRING_TOKEN(STR_PCH_DTS_TEMP_VALUE), L"%d C", MonitorValues.PCHDTSTemp);
            break;
        }
        case TS_ON_DIMM0_TEMP_KEY :
        {
            InitString(HiiHandle, STRING_TOKEN(STR_TS_ON_DIMM0_TEMP_VALUE), L"%d C", MonitorValues.TSonDimm0Temp);
            break;
        }
        case TS_ON_DIMM1_TEMP_KEY :
        {
            InitString(HiiHandle, STRING_TOKEN(STR_TS_ON_DIMM1_TEMP_VALUE), L"%d C", MonitorValues.TSonDimm1Temp);
            break;
        }
    }
  }
}

/**
  This function is handling PECI mode option changed action
  and send EC command to switch between legacy and eSPI mode according to user selection.

  @param  This                   Points to the EFI_HII_CONFIG_ACCESS_PROTOCOL.
  @param  Action                 Specifies the type of action taken by the browser.
  @param  KeyValue               A unique value which is sent to the original
                                 exporting driver so that it can identify the type
                                 of data to expect.
  @param  Type                   The type of value for the question.
  @param  Value                  A pointer to the data being sent to the original
                                 exporting driver.
  @param  ActionRequest          On return, points to the action requested by the
                                 callback function.

  @retval EFI_SUCCESS            The callback successfully handled the action.
  @retval EFI_OUT_OF_RESOURCES   Not enough storage is available to hold the
                                 variable and its data.
  @retval EFI_NOT_FOUND          The specified SetupData is not found.
**/
EFI_STATUS
EFIAPI
EcPeciModeCallBack (
  IN CONST EFI_HII_CONFIG_ACCESS_PROTOCOL *This,
  IN EFI_BROWSER_ACTION                   Action,
  IN EFI_QUESTION_ID                      KeyValue,
  IN UINT8                                Type,
  IN EFI_IFR_TYPE_VALUE                   *Value,
  OUT EFI_BROWSER_ACTION_REQUEST          *ActionRequest
  )
{
  SETUP_DATA        SetupData;
  BOARD_INFO_SETUP  BoardInfoSetup;
  UINTN             VarSize;
  UINTN             BoardVarSize;
  EFI_STATUS        Status;
  UINT8             DataBuffer[4];
  UINT32            SetupVariableAttributes;
  UINT32            BoardInfoAttributes;

  DEBUG ((DEBUG_INFO, "%a() start\n", __FUNCTION__));

  if (Action != EFI_BROWSER_ACTION_SUBMITTED) {
    return EFI_UNSUPPORTED;
  }

  VarSize      = sizeof (SETUP_DATA);
  BoardVarSize = sizeof (BOARD_INFO_SETUP);

  Status = gRT->GetVariable (
                  L"Setup",
                  &gSetupVariableGuid,
                  &SetupVariableAttributes,
                  &VarSize,
                  &SetupData
                  );

  Status = gRT->GetVariable (
                  L"BoardInfoSetup",
                  &gBoardInfoVariableGuid,
                  &BoardInfoAttributes,
                  &BoardVarSize,
                  &BoardInfoSetup
                  );

  SendEcCommand (EC_C_SET_PECI_MODE);
  if (SendEcData (SetupData.EcPeciMode) != EFI_SUCCESS){
    DEBUG ((DEBUG_INFO, "Sending EC data  %02X\n", SetupData.EcPeciMode));
    SetupData.EcPeciMode = 0; // default Legacy mode
    DEBUG ((DEBUG_ERROR, "EcPeciModeCallBack failed with status: %r\n", Status));
  } else {
    Status = DetectEcSupport ((UINT8 *)DataBuffer);
    if (RETURN_ERROR (Status)) {
      //
      // EcPeciModeCallBack() failed due to EC no respond,
      // Keep current setting of setupData.EcPeciMode,
      // Sync the value of BoardInfoSetup.EcPeciMode & PcdEcPeciMode with setup variable.
      //
      DEBUG ((EFI_D_INFO, "EcPeciModeCallBack() failed at EC not respond.\n"));
      BoardInfoSetup.EcPeciMode = SetupData.EcPeciMode; // For G3 restore
      PcdSet8S (PcdEcPeciMode, SetupData.EcPeciMode);
    } else {
      //
      // Set PECI mode and read back EC current status successfully,
      // Save the setting depend on EC current status.
      //
      SetupData.EcPeciMode      = (DataBuffer[3] >> 1) & 0x1;
      BoardInfoSetup.EcPeciMode = (DataBuffer[3] >> 1) & 0x1; // For G3 restore
      PcdSet8S (PcdEcPeciMode, (DataBuffer[3] >> 1) & 0x1);
      DEBUG ((DEBUG_INFO, "EC buffer data[3]  %02X\n", DataBuffer[3]));
    }
  }

  Status = gRT->SetVariable (
                  L"Setup",
                  &gSetupVariableGuid,
                  SetupVariableAttributes,
                  VarSize,
                  &SetupData
                  );
  ASSERT_EFI_ERROR (Status);

  Status = gRT->SetVariable (
                  L"BoardInfoSetup",
                  &gBoardInfoVariableGuid,
                  BoardInfoAttributes,
                  sizeof (BOARD_INFO_SETUP),
                  &BoardInfoSetup
                  );
  ASSERT_EFI_ERROR (Status);

  DEBUG ((DEBUG_INFO, "%a() end\n", __FUNCTION__));

  return Status;
}

/**
  This function would update the EC RAM 0x78[bit0] which represent EC CS DEBUG LED
  function.

  @param  CSEnable              Value to indicate the EC CS DEBUG LED enable or not.

  @retval EFI_SUCCESS           Set the value successfully.
  @retval Others                Fail due to EC read or write.

**/
EFI_STATUS
EFIAPI
EcUpdateCsLED (
  IN UINT8        CSEnable
  )
{
  EFI_STATUS        Status;
  UINT8             DataBuffer[2];

  DataBuffer[0] = 0x78;
  Status = ReadEcRam (DataBuffer);
  if (EFI_ERROR (Status)) {
    return Status;
  }
  if (CSEnable == 1) {
    DataBuffer[1] = DataBuffer[0] | BIT0;
  } else {
    DataBuffer[1] = DataBuffer[0] & ~(BIT0);
  }
  DataBuffer[0] = 0x78;
  Status = WriteEcRam (DataBuffer);

  return Status;
}

/**
  This function is handling EC CS debug light, if the EC CS debug light is enabled,
  it would set the related bit in EC RAM.

  @param  This                   Points to the EFI_HII_CONFIG_ACCESS_PROTOCOL.
  @param  Action                 Specifies the type of action taken by the browser.
  @param  KeyValue               A unique value which is sent to the original
                                 exporting driver so that it can identify the type
                                 of data to expect.
  @param  Type                   The type of value for the question.
  @param  Value                  A pointer to the data being sent to the original
                                 exporting driver.
  @param  ActionRequest          On return, points to the action requested by the
                                 callback function.

  @retval EFI_SUCCESS            The callback successfully handled the action.
  @retval EFI_OUT_OF_RESOURCES   Not enough storage is available to hold the
                                 variable and its data.
  @retval EFI_NOT_FOUND          The specified SetupData is not found.
**/
EFI_STATUS
EFIAPI
EcCsDebugLightCallback (
  IN CONST EFI_HII_CONFIG_ACCESS_PROTOCOL *This,
  IN EFI_BROWSER_ACTION                   Action,
  IN EFI_QUESTION_ID                      KeyValue,
  IN UINT8                                Type,
  IN EFI_IFR_TYPE_VALUE                   *Value,
  OUT EFI_BROWSER_ACTION_REQUEST          *ActionRequest
  )
{
  SETUP_DATA        SetupData;
  UINTN             VarSize;
  EFI_STATUS        Status;
  UINT32            SetupVariableAttributes;

  DEBUG ((DEBUG_INFO, "%a() start\n", __FUNCTION__));

  if (Action != EFI_BROWSER_ACTION_SUBMITTED) {
    return EFI_UNSUPPORTED;
  }

  VarSize      = sizeof (SETUP_DATA);

  Status = gRT->GetVariable (
                  L"Setup",
                  &gSetupVariableGuid,
                  &SetupVariableAttributes,
                  &VarSize,
                  &SetupData
                  );

  if (!EFI_ERROR (Status)) {
    Status = EcUpdateCsLED (SetupData.CSDebugLightEC);
  }

  DEBUG ((DEBUG_INFO, "%a() end\n", __FUNCTION__));

  return Status;
}