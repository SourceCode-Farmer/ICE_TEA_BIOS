/** @file

@copyright
  INTEL CONFIDENTIAL
  Copyright 2014 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains a 'Sample Driver' and is licensed as such under the terms
  of your license agreement with Intel or your vendor. This file may be modified
  by the user, subject to the additional terms of the license agreement.

@par Specification
**/

form formid = AUTO_ID(TPM_CONFIG_FORM_ID),
  title     = STRING_TOKEN(STR_TPM_CFG);

  SUBTITLE(STRING_TOKEN(STR_TPM_CFG))
  SEPARATOR

  text
    help   = STRING_TOKEN(STR_TCG2_DEVICE_STATE_HELP),
    text   = STRING_TOKEN(STR_TCG2_DEVICE_STATE_PROMPT),
      text   = STRING_TOKEN(STR_TCG2_DEVICE_STATE_CONTENT);

  grayoutif ideqval ME_SETUP_STORAGE.AfterEoP == 0x1 OR
            ideqval ME_SETUP_STORAGE.RemoteSessionActive == 0x1;
  checkbox varid = TCG_SETUP.PttPresent,
    questionid = KEY_PTT_DEVICE_TOGGLE,
    prompt     = STRING_TOKEN(STR_PTT_DEVICE_TOGGLE),
    help       = STRING_TOKEN(STR_PTT_DEVICE_TOGGLE_HELP),
    flags      = INTERACTIVE | RESET_REQUIRED,
    key        = AUTO_ID(KEY_PTT_DEVICE_TOGGLE),
  endcheckbox;
  endif; // grayoutif

  SEPARATOR

  suppressif (ideqval TCG_SETUP.dTpm12Present == 0x0 OR
              NOT ideqval TCG2_CONFIGURATION.TpmDevice == TPM_DEVICE_1_2);
    goto TPM_CONFIG_FORM_ID,
    prompt = STRING_TOKEN(STR_TCG_TITLE),
    help   = STRING_TOKEN(STR_TCG_HELP),
    flags  = INTERACTIVE,
    key    = AUTO_ID(KEY_TPM_GOTO);
  endif; // suppressif

  suppressif (ideqval TCG_SETUP.PttPresent == 0x0 AND
              ideqval TCG_SETUP.dTpm20Present == 0x0) OR
              NOT ideqval TCG2_CONFIGURATION.TpmDevice == TPM_DEVICE_2_0_DTPM;
    goto TPM_CONFIG_FORM_ID,
    prompt = STRING_TOKEN(STR_TCG2_TITLE),
    help   = STRING_TOKEN(STR_TCG2_HELP),
    flags  = INTERACTIVE,
    key    = AUTO_ID(KEY_TPM2_GOTO);
  endif; // suppressif
endform;
