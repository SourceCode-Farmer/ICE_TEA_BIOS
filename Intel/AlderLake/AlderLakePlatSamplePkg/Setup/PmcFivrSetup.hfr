/** @file

@copyright
  INTEL CONFIDENTIAL
  Copyright 2020 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains a 'Sample Driver' and is licensed as such under the terms
  of your license agreement with Intel or your vendor. This file may be modified
  by the user, subject to the additional terms of the license agreement.

@par Specification
**/

form formid = AUTO_ID(FIVR_CONFIGURATION_FORM_ID),
  title = STRING_TOKEN(STR_PCH_FIVR_CONFIGURATION_FORM_TITLE);

  text
    help   = STRING_TOKEN(STR_PCH_FIVR_EXT_RAIL_V1P05_CONFIG),
    text   = STRING_TOKEN(STR_PCH_FIVR_EXT_RAIL_V1P05_CONFIG),
    flags  = 0,
    key    = 0;

  checkbox varid  = PCH_SETUP.ExternalV1p05StateEnable[0],
    prompt   = STRING_TOKEN(STR_PCH_FIVR_EXT_RAIL_S0I1_S0I2),
    help     = STRING_TOKEN(STR_PCH_FIVR_EXT_RAIL_V1P05_HELP),
    // Flags behavior for checkbox is overloaded so that it equals
    // a DEFAULT value.  1 = ON, 0 = off
    flags    = 0 | RESET_REQUIRED,
    key      = 0,
  endcheckbox;

  checkbox varid  = PCH_SETUP.ExternalV1p05StateEnable[1],
    prompt   = STRING_TOKEN(STR_PCH_FIVR_EXT_RAIL_S0I3),
    help     = STRING_TOKEN(STR_PCH_FIVR_EXT_RAIL_V1P05_HELP),
    // Flags behavior for checkbox is overloaded so that it equals
    // a DEFAULT value.  1 = ON, 0 = off
    flags    = 0 | RESET_REQUIRED,
    key      = 0,
  endcheckbox;

  checkbox varid  = PCH_SETUP.ExternalV1p05StateEnable[2],
    prompt   = STRING_TOKEN(STR_PCH_FIVR_EXT_RAIL_S3),
    help     = STRING_TOKEN(STR_PCH_FIVR_EXT_RAIL_V1P05_HELP),
    // Flags behavior for checkbox is overloaded so that it equals
    // a DEFAULT value.  1 = ON, 0 = off
    flags    = 0 | RESET_REQUIRED,
    key      = 0,
  endcheckbox;

  checkbox varid  = PCH_SETUP.ExternalV1p05StateEnable[3],
    prompt   = STRING_TOKEN(STR_PCH_FIVR_EXT_RAIL_S4),
    help     = STRING_TOKEN(STR_PCH_FIVR_EXT_RAIL_V1P05_HELP),
    // Flags behavior for checkbox is overloaded so that it equals
    // a DEFAULT value.  1 = ON, 0 = off
    flags    = 0 | RESET_REQUIRED,
    key      = 0,
  endcheckbox;

  checkbox varid  = PCH_SETUP.ExternalV1p05StateEnable[4],
    prompt   = STRING_TOKEN(STR_PCH_FIVR_EXT_RAIL_S5),
    help     = STRING_TOKEN(STR_PCH_FIVR_EXT_RAIL_V1P05_HELP),
    // Flags behavior for checkbox is overloaded so that it equals
    // a DEFAULT value.  1 = ON, 0 = off
    flags    = 0 | RESET_REQUIRED,
    key      = 0,
  endcheckbox;

  checkbox varid  = PCH_SETUP.ExternalV1p05StateEnable[5],
    prompt   = STRING_TOKEN(STR_PCH_FIVR_EXT_RAIL_S0),
    help     = STRING_TOKEN(STR_PCH_FIVR_EXT_RAIL_V1P05_HELP),
    // Flags behavior for checkbox is overloaded so that it equals
    // a DEFAULT value.  1 = ON, 0 = off
    flags    = 0 | RESET_REQUIRED,
    key      = 0,
  endcheckbox;

  SEPARATOR

  text
    help   = STRING_TOKEN(STR_PCH_FIVR_EXT_RAIL_VNN_CONFIG),
    text   = STRING_TOKEN(STR_PCH_FIVR_EXT_RAIL_VNN_CONFIG),
    flags  = 0,
    key    = 0;

  checkbox varid  = PCH_SETUP.ExternalVnnStateEnable[0],
    prompt   = STRING_TOKEN(STR_PCH_FIVR_EXT_RAIL_S0I1_S0I2),
    help     = STRING_TOKEN(STR_PCH_FIVR_EXT_RAIL_VNN_HELP),
    // Flags behavior for checkbox is overloaded so that it equals
    // a DEFAULT value.  1 = ON, 0 = off
    flags    = 0 | RESET_REQUIRED,
    key      = 0,
  endcheckbox;

  checkbox varid  = PCH_SETUP.ExternalVnnStateEnable[1],
    prompt   = STRING_TOKEN(STR_PCH_FIVR_EXT_RAIL_S0I3),
    help     = STRING_TOKEN(STR_PCH_FIVR_EXT_RAIL_VNN_HELP),
    // Flags behavior for checkbox is overloaded so that it equals
    // a DEFAULT value.  1 = ON, 0 = off
    flags    = 0 | RESET_REQUIRED,
    key      = 0,
  endcheckbox;

  checkbox varid  = PCH_SETUP.ExternalVnnStateEnable[2],
    prompt   = STRING_TOKEN(STR_PCH_FIVR_EXT_RAIL_S3),
    help     = STRING_TOKEN(STR_PCH_FIVR_EXT_RAIL_VNN_HELP),
    // Flags behavior for checkbox is overloaded so that it equals
    // a DEFAULT value.  1 = ON, 0 = off
    flags    = 0 | RESET_REQUIRED,
    key      = 0,
  endcheckbox;

  checkbox varid  = PCH_SETUP.ExternalVnnStateEnable[3],
    prompt   = STRING_TOKEN(STR_PCH_FIVR_EXT_RAIL_S4),
    help     = STRING_TOKEN(STR_PCH_FIVR_EXT_RAIL_VNN_HELP),
    // Flags behavior for checkbox is overloaded so that it equals
    // a DEFAULT value.  1 = ON, 0 = off
    flags    = 0 | RESET_REQUIRED,
    key      = 0,
  endcheckbox;

  checkbox varid  = PCH_SETUP.ExternalVnnStateEnable[4],
    prompt   = STRING_TOKEN(STR_PCH_FIVR_EXT_RAIL_S5),
    help     = STRING_TOKEN(STR_PCH_FIVR_EXT_RAIL_VNN_HELP),
    // Flags behavior for checkbox is overloaded so that it equals
    // a DEFAULT value.  1 = ON, 0 = off
    flags    = 0 | RESET_REQUIRED,
    key      = 0,
  endcheckbox;

  checkbox varid  = PCH_SETUP.ExternalVnnStateEnable[5],
    prompt   = STRING_TOKEN(STR_PCH_FIVR_EXT_RAIL_S0),
    help     = STRING_TOKEN(STR_PCH_FIVR_EXT_RAIL_VNN_HELP),
    // Flags behavior for checkbox is overloaded so that it equals
    // a DEFAULT value.  1 = ON, 0 = off
    flags    = 0 | RESET_REQUIRED,
    key      = 0,
  endcheckbox;

  oneof varid = PCH_SETUP.ExternalVnnSupportedVoltageConfigurations,
    prompt   = STRING_TOKEN(STR_PCH_FIVR_EXT_RAIL_CONFIG_VOLT),
    help     = STRING_TOKEN(STR_PCH_FIVR_EXT_RAIL_CONFIG_VOLT_HELP),
    option text = STRING_TOKEN(STR_PCH_FIVR_EXT_RAIL_0_78BP_0_78BP_1_05_IN), value = 1, flags = RESET_REQUIRED;
    option text = STRING_TOKEN(STR_PCH_FIVR_EXT_RAIL_1_05BP_1_05_BP_1_05BP), value = 3, flags = DEFAULT | RESET_REQUIRED;
  endoneof;

  SEPARATOR

  text
    help   = STRING_TOKEN(STR_PCH_FIVR_EXT_RAIL_VOLTAGE_CURRENT),
    text   = STRING_TOKEN(STR_PCH_FIVR_EXT_RAIL_VOLTAGE_CURRENT),
    flags  = 0,
    key    = 0;

  numeric varid = PCH_SETUP.ExternalV1p05IccMaximum,
    prompt    = STRING_TOKEN(STR_PCH_FIVR_EXT_V1P05_ICC_MAX),
    help      = STRING_TOKEN(STR_PCH_FIVR_EXT_V1P05_ICC_MAX_HELP),
    flags     = RESET_REQUIRED,
    minimum   = 0,
    maximum   = 500,
    step      = 1,
    default   = 500,
  endnumeric;

  numeric varid = PCH_SETUP.ExternalVnnIccMaximum,
    prompt    = STRING_TOKEN(STR_PCH_FIVR_EXT_VNN_ICC_MAX),
    help      = STRING_TOKEN(STR_PCH_FIVR_EXT_VNN_ICC_MAX_HELP),
    flags     = RESET_REQUIRED,
    minimum   = 0,
    maximum   = 500,
    step      = 1,
    default   = 500,
  endnumeric;

  SEPARATOR

  text
    help   = STRING_TOKEN(STR_PCH_FIVR_VCINN_AUX_CONFIG),
    text   = STRING_TOKEN(STR_PCH_FIVR_VCINN_AUX_CONFIG),
    flags  = 0,
    key    = 0;

  numeric varid = PCH_SETUP.PchRetToLowCurModeVolTranTime,
    prompt    = STRING_TOKEN(STR_PCH_FIVR_RMV_LCM_VOLT_TRANS_TIME),
    help      = STRING_TOKEN(STR_PCH_FIVR_RMV_LCM_VOLT_TRANS_TIME_HELP),
    flags     = RESET_REQUIRED,
    minimum   = 0,
    maximum   = 255,
    step      = 1,
    default   = 0,
  endnumeric;

  numeric varid = PCH_SETUP.PchRetToHighCurModeVolTranTime,
    prompt    = STRING_TOKEN(STR_PCH_FIVR_RMV_HCM_VOLT_TRANS_TIME),
    help      = STRING_TOKEN(STR_PCH_FIVR_RMV_HCM_VOLT_TRANS_TIME_HELP),
    flags     = RESET_REQUIRED,
    minimum   = 0,
    maximum   = 255,
    step      = 1,
    default   = 0,
  endnumeric;

  numeric varid = PCH_SETUP.PchLowToHighCurModeVolTranTime,
    prompt    = STRING_TOKEN(STR_PCH_FIVR_LCM_HCM_VOLT_TRANS_TIME),
    help      = STRING_TOKEN(STR_PCH_FIVR_LCM_HCM_VOLT_TRANS_TIME_HELP),
    flags     = RESET_REQUIRED,
    minimum   = 0,
    maximum   = 255,
    step      = 1,
    default   = 0xC,
  endnumeric;

  numeric varid = PCH_SETUP.PchOffToHighCurModeVolTranTime,
    prompt    = STRING_TOKEN(STR_PCH_FIVR_OFF_HCM_VOLT_TRANS_TIME),
    help      = STRING_TOKEN(STR_PCH_FIVR_OFF_HCM_VOLT_TRANS_TIME_HELP),
    flags     = RESET_REQUIRED,
    minimum   = 0,
    maximum   = 4095,
    step      = 1,
    default   = 0x96,
  endnumeric;

  SEPARATOR

  oneof varid = PCH_SETUP.PchFivrDynPm,
    prompt   = STRING_TOKEN(STR_PCH_FIVR_DYN_PM),
    help     = STRING_TOKEN(STR_PCH_FIVR_DYN_PM_HELP),
    option text = STRING_TOKEN(STR_DISABLED), value = 0, flags = RESET_REQUIRED;
    option text = STRING_TOKEN(STR_ENABLED), value = 1, flags = DEFAULT | RESET_REQUIRED;
  endoneof;

endform; // End of FIVR_CONFIGURATION_FORM_ID
