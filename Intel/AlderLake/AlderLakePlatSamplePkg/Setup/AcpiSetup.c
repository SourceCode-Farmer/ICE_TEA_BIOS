/** @file

;******************************************************************************
;* Copyright (c) 2012 - 2021, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/
/** @file

@copyright
  INTEL CONFIDENTIAL
  Copyright 2012 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains a 'Sample Driver' and is licensed as such under the terms
  of your license agreement with Intel or your vendor. This file may be modified
  by the user, subject to the additional terms of the license agreement.

@par Specification Reference:
**/
#include <Base.h>
#include <Library/AslUpdateLib.h>
#include <Protocol/AcpiSystemDescriptionTable.h>
//[-start-020215-IB06462109-remove]//
//#include <SetupPrivate.h>
//[-start-020215-IB06462109-remove]//
//[-start-020215-IB06462109-add]//
#include <SetupUtility.h>
#include <ChipsetSetupConfig.h>
#include <Library/DebugLib.h>

SETUP_DATA             mSetupData;
//[-end-020215-IB06462109-add]//

/**
  This function initializes the Acpi related setup option values

  @param[in] HiiHandle  HiiHandle Handle to HII database
  @param[in] Class      Indicates the setup class

**/
VOID
InitAcpiStrings (
  IN EFI_HII_HANDLE      HiiHandle,
  IN UINT16              Class
  )
{
  EFI_STATUS             Status;
  CHAR8                  *FadtVersionStr;
  UINTN                  VariableSize;
  UINT32                 VariableAttributes;
//[-start-020215-IB06462109-add]//
  EFI_ACPI_DESCRIPTION_HEADER  *Table;
  UINTN                        Handle;
  CHIPSET_CONFIGURATION        *MyIfrNVData;
//[-start-200831-IB17800087-modify]//
#ifdef ACPI_DEBUG_FLAG  
  UINT32                 AcpiDebugAddress;
  
  AcpiDebugAddress = 0;
#endif  
//[-end-200831-IB17800087-modify]//
//[-start-020215-IB06462109-add]//

  DEBUG ((DEBUG_INFO, "<InitAcpiStrings>"));
  FadtVersionStr = "1.0";

  if (Class == ADVANCED_FORM_SET_CLASS) {
    VariableSize = sizeof (SETUP_DATA);
    Status = gRT->GetVariable (
                    L"Setup",
                    &gSetupVariableGuid,
                    &VariableAttributes,
                    &VariableSize,
                    &mSetupData
                    );
    if (EFI_ERROR (Status)) {
      return;
    }
//[-start-020215-IB06462109-modify]//
//[-start-200831-IB17800087-modify]//
#ifdef ACPI_DEBUG_FLAG
    AcpiDebugAddress = mSetupData.AcpiDebugAddress;
#endif    
//[-end-200831-IB17800087-modify]//
    MyIfrNVData = (CHIPSET_CONFIGURATION *)gSUBrowser->SCBuffer;

    ///
    /// Locate table with matching ID
    ///
    //InitializeAslUpdateLib();
    Handle = 0;
    Table  = NULL;
    Status = LocateAcpiTableBySignature (
                EFI_ACPI_2_0_FIXED_ACPI_DESCRIPTION_TABLE_SIGNATURE,
                (EFI_ACPI_DESCRIPTION_HEADER **)  &Table,
                &Handle
                );
    if((!EFI_ERROR(Status)) && (Table != NULL)) {
      switch (Table->Revision) {
      case EFI_ACPI_2_0_FIXED_ACPI_DESCRIPTION_TABLE_REVISION:
        FadtVersionStr = "2.0";
        break;
      case EFI_ACPI_3_0_FIXED_ACPI_DESCRIPTION_TABLE_REVISION:
        FadtVersionStr = "3.0";
        break;
      case EFI_ACPI_5_0_FIXED_ACPI_DESCRIPTION_TABLE_REVISION:
        FadtVersionStr = "5.0";
        break;
      case EFI_ACPI_6_3_FIXED_ACPI_DESCRIPTION_TABLE_REVISION:  // Major Revision: 6
        FadtVersionStr = "6.3";
        break;
      default:
        break;
      }
    } else {
      //
      // Insyde code will uninstall gEfiAcpiSdtProtocolGuid before booting OS to avoid API modify acpi table.
      //
      switch(MyIfrNVData->AcpiVer) {

        case 0:
          FadtVersionStr = "1.0B";
          break;

        case 1:
          FadtVersionStr = "3.0";
          break;

        case 2:
          FadtVersionStr = "4.0";
          break;

        case 3:
          FadtVersionStr = "5.0";
          break;

//[-start-21523-IB05660169-modify]//
        case 4:
          FadtVersionStr = "6.0";
          break;

        case 5:
          FadtVersionStr = "6.1";
          break;

        case 6:
          FadtVersionStr = "6.2";
          break;

        case 7:
          FadtVersionStr = "6.3";
          break;

        default:
          FadtVersionStr = "6.3";
          break;
      }      
    }
  } // if (Class == ADVANCED_FORM_SET_CLASS)
//[-end-21523-IB05660169-modify]//
//[-end-020215-IB06462109-modify]//

  InitString (
    HiiHandle,
    STRING_TOKEN (STR_ACPI_VERSION_VALUE),
    L"%a",
    FadtVersionStr
  );
//[-start-200831-IB17800087-modify]//
#ifdef ACPI_DEBUG_FLAG
  InitString (
    HiiHandle,
    STRING_TOKEN (STR_ACPI_DEBUG_ADDRESS_VALUE),
    L"0x%x",
    AcpiDebugAddress
  );
#endif //ACPI_DEBUG_FLAG
//[-end-200831-IB17800087-modify]//
}

EFI_STATUS
EFIAPI
CpuGameCompatibilityCallback (
  IN CONST EFI_HII_CONFIG_ACCESS_PROTOCOL *This,
  IN EFI_BROWSER_ACTION                   Action,
  IN EFI_QUESTION_ID                      KeyValue,
  IN UINT8                                Type,
  IN EFI_IFR_TYPE_VALUE                   *Value,
  OUT EFI_BROWSER_ACTION_REQUEST          *ActionRequest
  )
{
  SETUP_DATA              SetupData;
  UINTN                   VarSize;
  EFI_STATUS              Status;
  EFI_STRING              RequestString;
  CPU_SETUP               CpuSetup;

  RequestString = NULL;
  Status = EFI_SUCCESS;

  if (Action != EFI_BROWSER_ACTION_CHANGING && Action != EFI_BROWSER_ACTION_CHANGED) {
    return EFI_UNSUPPORTED;
  }

  ASSERT (KeyValue == CPU_GAME_COMPATIBILITY_MODE_QUESTION_ID);
  DEBUG ((DEBUG_INFO, "CpuGameCompatibilityCallback()\n"));

  VarSize = sizeof (SETUP_DATA);
  if (!HiiGetBrowserData (&gSetupVariableGuid, L"Setup", VarSize, (UINT8 *) &SetupData)) {
    Status = EFI_NOT_FOUND;
  }
  ASSERT_EFI_ERROR (Status);

  VarSize = sizeof (CPU_SETUP);
  if (!HiiGetBrowserData (&gCpuSetupVariableGuid, L"CpuSetup", VarSize, (UINT8 *) &CpuSetup)) {
    Status = EFI_NOT_FOUND;
  }
  ASSERT_EFI_ERROR (Status);


  if (!EFI_ERROR (Status) && (CpuSetup.GameCompatibilityMode == 1)) {
    SetupData.Ps2KbMsEnable = 1;
  } else {  // Game Compatibility is off
    SetupData.Ps2KbMsEnable = 0;
  }
  RequestString = HiiConstructRequestString (RequestString, OFFSET_OF (SETUP_DATA, Ps2KbMsEnable), sizeof (SetupData.Ps2KbMsEnable));
  VarSize = sizeof (SETUP_DATA);
  if (!HiiSetBrowserData (&gSetupVariableGuid, L"Setup", VarSize, (UINT8 *) &SetupData, RequestString)) {
    Status = EFI_NOT_FOUND;
  }
  ASSERT_EFI_ERROR (Status);
  FreePool (RequestString);
  return EFI_SUCCESS;
}

//
// Callback routine for LowPowerS0Idle Enable
//
EFI_STATUS
EFIAPI
LowPowerS0IdleEnableCallback (
  IN CONST EFI_HII_CONFIG_ACCESS_PROTOCOL *This,
  IN EFI_BROWSER_ACTION                   Action,
  IN EFI_QUESTION_ID                      KeyValue,
  IN UINT8                                Type,
  IN EFI_IFR_TYPE_VALUE                   *Value,
  OUT EFI_BROWSER_ACTION_REQUEST          *ActionRequest
  )
{
  SETUP_DATA              SetupData;
  UINTN                   VarSize;
  EFI_STATUS              Status;
//[-start-020215-IB06462109-modify]//
//  EFI_STRING              RequestString;
  SETUP_DATA              *MyIfrNVData;
  BOOLEAN                 ActionStatus = FALSE;

//  RequestString = NULL;
//[-end-020215-IB06462109-modify]//
  Status = EFI_SUCCESS;

  if (Action != EFI_BROWSER_ACTION_CHANGING && Action != EFI_BROWSER_ACTION_CHANGED) {
    return EFI_UNSUPPORTED;
  }

  ASSERT (KeyValue == KEY_LOW_POWER_S0_IDLE);
  DEBUG ((DEBUG_INFO, "LowPowerS0IdleEnableCallback()\n"));

  VarSize = sizeof (SETUP_DATA);
//[-start-020215-IB06462109-modify]//
  ActionStatus = HiiGetBrowserData (&gSetupVariableGuid, L"Setup", VarSize, (UINT8 *) &SetupData);
  MyIfrNVData = (SETUP_DATA *)gRcSUBrowser->SetupDataSUBrowserData;
  CopyMem (&SetupData, MyIfrNVData, VarSize);
  if (ActionStatus == FALSE) {
    Status = EFI_NOT_FOUND;
  }
  ASSERT_EFI_ERROR (Status);
//[-end-020215-IB06462109-modify]//

  if (!EFI_ERROR (Status) && (SetupData.LowPowerS0Idle == 1)) {
    SetupData.OpRomPost = OPROM_EFI;
    SetupData.HidEventFilterDriverEnable = 1;
//[-start-020215-IB06462109-remove]//
//    RequestString = HiiConstructRequestString (RequestString, OFFSET_OF (SETUP_DATA, OpRomPost), sizeof (SetupData.OpRomPost));
//    RequestString = HiiConstructRequestString (RequestString, OFFSET_OF (SETUP_DATA, HidEventFilterDriverEnable), sizeof (SetupData.HidEventFilterDriverEnable));
//    VarSize = sizeof (SETUP_DATA);
//    if (!HiiSetBrowserData (&gSetupVariableGuid, L"Setup", VarSize, (UINT8 *) &SetupData, RequestString)) {
//      Status = EFI_NOT_FOUND;
//    }
//    ASSERT_EFI_ERROR (Status);
//    FreePool (RequestString);
//[-end-020215-IB06462109-remove]//
  }
//[-start-020215-IB06462109-add]//
  CopyMem (MyIfrNVData, &SetupData, VarSize);
//[-end-020215-IB06462109-add]//  
  return EFI_SUCCESS;
}
