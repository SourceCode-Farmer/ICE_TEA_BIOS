## @file
# 
#******************************************************************************
#* Copyright 2020 Insyde Software Corp. All Rights Reserved.
#*
#* You may not reproduce, distribute, publish, display, perform, modify, adapt,
#* transmit, broadcast, present, recite, release, license or otherwise exploit
#* any part of this publication in any form, by any means, without the prior
#* written permission of Insyde Software Corp.
#*
#******************************************************************************
### @file
#  Component description file for Setup module
#
#@copyright
#  INTEL CONFIDENTIAL
#  Copyright 1999 - 2021 Intel Corporation.
#
#  The source code contained or described herein and all documents related to the
#  source code ("Material") are owned by Intel Corporation or its suppliers or
#  licensors. Title to the Material remains with Intel Corporation or its suppliers
#  and licensors. The Material may contain trade secrets and proprietary and
#  confidential information of Intel Corporation and its suppliers and licensors,
#  and is protected by worldwide copyright and trade secret laws and treaty
#  provisions. No part of the Material may be used, copied, reproduced, modified,
#  published, uploaded, posted, transmitted, distributed, or disclosed in any way
#  without Intel's prior express written permission.
#
#  No license under any patent, copyright, trade secret or other intellectual
#  property right is granted to or conferred upon you by disclosure or delivery
#  of the Materials, either expressly, by implication, inducement, estoppel or
#  otherwise. Any license under such intellectual property rights must be
#  express and approved by Intel in writing.
#
#  Unless otherwise agreed by Intel in writing, you may not remove or alter
#  this notice or any other notice embedded in Materials by Intel or
#  Intel's suppliers or licensors in any way.
#
#  This file contains a 'Sample Driver' and is licensed as such under the terms
#  of your license agreement with Intel or your vendor. This file may be modified
#  by the user, subject to the additional terms of the license agreement.
#
# @par Specification Reference:
#
# @par Glossary:
###

[Defines]
  INF_VERSION                    = 0x00010005
  BASE_NAME                      = Setup
  FILE_GUID                      = E6A7A1CE-5881-4B49-80BE-69C91811685C
  MODULE_TYPE                    = DXE_DRIVER
  VERSION_STRING                 = 1.0
  ENTRY_POINT                    = SetupEntry

[Sources.common]
  Setup.uni
  Main.vfr
  Advanced.vfr
  Boot.vfr
  Setup.c
  HiiConfigAccess.c
  BootSetup.c

#Firmware Configuration
  FwConfig.uni

#Sa
  SaSetup.uni
  SaSetup.hfr
  SaSetup.c
  SaPcieSetupSinglePortMenu.hfr
  SaPcieSetupSinglePort.hfr
  SaPcieStringPool.hfr
  ItbtPcieSetupSinglePort.hfr
  ItbtPcieStringPool.hfr
  CpuUsbSingleSsPort.hfr
  CpuUsbStringPool.hfr

#Pcie
  PcieSetup.uni
  PcieSetup.hfr

#Pch
  PchSetup.uni
  PchSetup.hfr
  PchPcieSetupSinglePortMenu.hfr
  PchPcieSetupSinglePort.hfr
  PcieEq.hfr
  PchPcieStringPool.hfr
  PchIshStringPool.hfr
  PchSetupTsnGbe.hfr
  PchUfsSingleController.hfr
  PchUfsStringPool.hfr
  PchSetup.c
  PchUsbSingleHsPort.hfr
  PchUsbSingleSsPort.hfr
  PchUsbStringPool.hfr
  PmcFivrSetup.hfr
  PchThcStringPool.hfr

#CPU
  CpuSetup.uni
  CpuSetup.hfr
  CpuSetup.c

#ME
  MeSetup.uni
  MeSetup.hfr
  MeSetup.c
  AmtSetup.uni
  AmtSetup.hfr

#Platform
  PlatformSetup.uni
  PlatformSetup.hfr
  PlatformSetup.c

#add ICC
  IccSetup.uni
  IccSetup.hfr
  IccSetup.c

#OverClockInit
  OverClockSetup.uni
  OverClockSetup.hfr
  OverClockSetup.c

#PciBus
  PciBusSetup.uni
  PciBusSetup.hfr
  PciBusSetup.c

#Acpi
  AcpiSetup.uni
  AcpiSetup.hfr
  AcpiSetup.c

#Connectivity
  ConnectivitySetup.uni
  ConnectivitySetup.hfr
  ConnectivitySetup.c

#Tcg
  TcgSetup.uni
  TcgSetup.hfr

#Security
  SecuritySetup.c

#NatChip
  SioNat87393VSetup.uni
  SioNat87393VSetup.hfr
  SioNat87393VSetup.c

#WPCN381U
  SioWPCN381USetup.uni
  SioWPCN381USetup.hfr
  SioWPCN381USetup.c

#NctChip
  SioNct6776FSetup.uni
  SioNct6776FSetup.hfr
  SioNct6776FSetup.c

#Hhm
  HhmSetup.uni
  HhmSetup.hfr
  EcSetup.c
  EcVoltage.c

#StatusCode
  StatusCodeSetup.uni
  StatusCodeSetup.hfr

#Debug
  DebugSetup.c
  DebugSetup.uni
  DebugSetup.hfr

#Thunderbolt(TM)
  TbtSetup.uni
  iTbtStringPool.hfr
  dTbtStringPool.hfr

  IntegratedTbtOptions.hfr
  DiscreteTbtOptions.hfr

  TbtSetup.hfr
  TbtSetup.c

#Tcc
  TccSetup.hfr
  TccSetup.uni
  TccSetup.c

#Fusa
  FusaSetup.hfr
  FusaSetup.uni

[Packages]
  AcpiDebugFeaturePkg/AcpiDebugFeaturePkg.dec
  AlderLakePlatSamplePkg/PlatformPkg.dec
  AlderLakeBoardPkg/BoardPkg.dec
  ClientOneSiliconPkg/SiPkg.dec
  MdeModulePkg/MdeModulePkg.dec
  MdePkg/MdePkg.dec
  SecurityPkg/SecurityPkg.dec
  UefiCpuPkg/UefiCpuPkg.dec
  CryptoPkg/CryptoPkg.dec
  #[-start-200917-IB06462159-modify]#
  $(PLATFORM_FSP_BIN_PACKAGE)/AlderLakeFspBinPkg.dec
  #[-end-200917-IB06462159-modify]#
  BoardModulePkg/BoardModulePkg.dec
  IntelFsp2WrapperPkg/IntelFsp2WrapperPkg.dec
  MinPlatformPkg/MinPlatformPkg.dec
  NhltFeaturePkg/NhltFeaturePkg.dec
  SndwFeaturePkg/SndwFeaturePkg.dec
  IntelSiliconPkg/IntelSiliconPkg.dec

[LibraryClasses]
  UefiDriverEntryPoint
  UefiBootServicesTableLib
  UefiRuntimeServicesTableLib
  UefiLib
  BaseMemoryLib
  MemoryAllocationLib
  DebugLib
  DebugPrintErrorLevelLib
  BiosIdLib
  HobLib
  IoLib
  HiiLib
  DxeMeLib
  EcMiscLib
  CpuPlatformLib
  CmosAccessLib
  PlatformNvRamHookLib
  PttHeciLib
  SerialPortParameterLib
  PciSegmentLib
  BootGuardLib
  MeFwStsLib
  DevicePathLib
  BaseLib
  GpioLib
  GbeLib
  TsnLib
  PchPcieRpLib
  PchInfoLib
  AslUpdateLib
  MeChipsetLib
  RngLib
  TbtCommonLib
  DTbtCommonLib
  DimmInfoLib
  ConfigBlockLib
  CpuMailboxLib
  CnviLib
  SataSocLib
  HashLib
  TimerLib
  ReportStatusCodeLib
  PreSiliconEnvDetectLib
  CpuRegbarAccessLib
  BoardIdsLib
  SpiAccessLib
  CpuPcieInitCommonLib
  CpuPcieInfoFruLib
  IshInfoLib
  GraphicsInfoLib
  VmdInfoLib
  PchPciBdfLib
  PcdLib
  EcLib
  TccLib
  PchPciBdfLib
  DxeTbtSecurityLib
  DxeBootStateLib
  DxeAmtHeciLib

[Guids]
  gEfiGlobalVariableGuid                        ## CONSUMES
  gBootStateGuid                                ## PRODUCES
  gEfiIfrTianoGuid                              ## CONSUMES
  gEfiHobListGuid                               ## CONSUMES
  gSetupVariableGuid                            ## PRODUCES
  gSaSetupVariableGuid                          ## PRODUCES
  gMeSetupVariableGuid                          ## PRODUCES
  gCpuSetupVariableGuid                         ## PRODUCES
  gPchSetupVariableGuid                         ## PRODUCES
  gSiSetupVariableGuid                          ## PRODUCES
  gDebugConfigVariableGuid                      ## CONSUMES
  gIccGuid                                      ## PRODUCES
  gOsProfileGuid                                ## PRODUCES
  gSetupEnterGuid                               ## CONSUMES
  gSystemAccessGuid                             ## PRODUCES
  gSetupNvramUpdateGuid                         ## PRODUCES
  gSystemConfigGuid                             ## CONSUMES
  gMeInfoSetupGuid                              ## CONSUMES
  gITbtInfoHobGuid                              ## CONSUMES
  gDTbtInfoHobGuid                              ## CONSUMES
  gMeDxeConfigGuid                              ## CONSUMES
  gGraphicsDxeConfigGuid                        ## CONSUMES
  gPchInfoHobGuid                               ## CONSUMES
  gChipsetInitHobGuid                           ## CONSUMES
  gFspInfoGuid                                  ## CONSUMES
  gWdtPersistentDataGuid                        ## PRODUCES
  gIpuDataHobGuid                               ## CONSUMES
  gSaConfigHobGuid
  gCpuPcieHobGuid
  gPchDmiHobGuid
  gVmdInfoHobGuid                               ## CONSUMES
  gTxtInfoHobGuid                               ## CONSUMES
  gTcssHobGuid                                  ## CONSUMES
  gCpuDataHobGuid                               ## CONSUMES
  gCpuDmiHobGuid
  gDebugTokenDataHobGuid                        ## CONSUMES
  gPdtExistGuid                                 ## CONSUMES
  gEfiConsoleInDeviceGuid                       ## CONSUMES
  gBoardInfoVariableGuid                        ## CONSUMES

[Protocols]
  gEfiHiiConfigAccessProtocolGuid               ## PRODUCES
  gEfiCpuIo2ProtocolGuid                        ## CONSUMES
  gWdtProtocolGuid                              ## CONSUMES
  gEfiSmbusHcProtocolGuid                       ## CONSUMES
  gMemInfoProtocolGuid                          ## CONSUMES
  gEfiDiskInfoProtocolGuid                      ## CONSUMES
  gEfiPciIoProtocolGuid                         ## CONSUMES
  gHeciProtocolGuid                             ## CONSUMES
  gSaPolicyProtocolGuid                         ## CONSUMES
  gEfiDxeSmmReadyToLockProtocolGuid             ## CONSUMES
  gAmtWrapperProtocolGuid                       ## CONSUMES
  gEfiI2cIoProtocolGuid                         ## CONSUMES
  gCpuInfoProtocolGuid                          ## CONSUMES
  gPchSpiProtocolGuid                           ## CONSUMES
  gEfiSmbiosProtocolGuid                        ## CONSUMES
  gBdsAllDriversConnectedProtocolGuid           ## CONSUMES
  gEfiUsbIoProtocolGuid                         ## CONSUMES
  gEdkiiVariableLockProtocolGuid                ## SOMETIMES_CONSUMES
  gPlatformConfigChangeProtocolGuid             ## SOMETIMES_PRODUCES
  gEdkiiPlatformSpecificResetFilterProtocolGuid ## CONSUMES
  gEfiMpServiceProtocolGuid                     ## CONSUMES
  gOneClickRecoveryProtocolGuid                 ## CONSUMES

[Pcd]
  gEfiMdePkgTokenSpaceGuid.PcdPciExpressBaseAddress                      ## CONSUMES
  gSiPkgTokenSpaceGuid.PcdMchBaseAddress                                 ## CONSUMES
  gAcpiDebugFeaturePkgTokenSpaceGuid.PcdAcpiDebugFeatureActive           ## CONSUMES
  gAcpiDebugFeaturePkgTokenSpaceGuid.PcdAcpiDebugAddress                 ## CONSUMES
  gPlatformModuleTokenSpaceGuid.PcdDmiBaseAddress                        ## CONSUMES
  gPlatformModuleTokenSpaceGuid.PcdSmbiosOemTypeFirmwareVersionInfo      ## CONSUMES
  gSiPkgTokenSpaceGuid.PcdSiliconInitVersionMajor                        ## CONSUMES
  gSiPkgTokenSpaceGuid.PcdSiliconInitVersionMinor                        ## CONSUMES
  gSiPkgTokenSpaceGuid.PcdSiliconInitVersionRevision                     ## CONSUMES
  gSiPkgTokenSpaceGuid.PcdSiliconInitVersionBuild                        ## CONSUMES
  gSiPkgTokenSpaceGuid.PcdBtgTxtLegacyPkgEnable                          ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdGfxCrbDetect
  gBoardModuleTokenSpaceGuid.PcdDockAttached
  gBoardModuleTokenSpaceGuid.PcdBoardRev
  gBoardModuleTokenSpaceGuid.PcdBoardType
  gBoardModuleTokenSpaceGuid.PcdPlatformFlavor
  gBoardModuleTokenSpaceGuid.PcdPlatformType
  gBoardModuleTokenSpaceGuid.PcdEcPresent
  gBoardModuleTokenSpaceGuid.PcdEcEspiFlashSharingMode
  gBoardModuleTokenSpaceGuid.PcdITbtRootPortNumber
  gBoardModuleTokenSpaceGuid.PcdDTbtControllerNumber
  gBoardModuleTokenSpaceGuid.PcdTcssPdType                               ## CONSUMES

  gPlatformModuleTokenSpaceGuid.PcdSetupConfigPatchTable
  gPlatformModuleTokenSpaceGuid.PcdSetupConfigPatchTableSize
  gPlatformModuleTokenSpaceGuid.PcdSaSetupConfigPatchTable
  gPlatformModuleTokenSpaceGuid.PcdSaSetupConfigPatchTableSize
  gPlatformModuleTokenSpaceGuid.PcdCpuSetupConfigPatchTable
  gPlatformModuleTokenSpaceGuid.PcdCpuSetupConfigPatchTableSize
  gPlatformModuleTokenSpaceGuid.PcdPchSetupConfigPatchTable
  gPlatformModuleTokenSpaceGuid.PcdPchSetupConfigPatchTableSize
  gPlatformModuleTokenSpaceGuid.PcdMeSetupConfigPatchTable
  gPlatformModuleTokenSpaceGuid.PcdMeSetupConfigPatchTableSize
  gPlatformModuleTokenSpaceGuid.PcdDTbtToPcieRegister
  gPlatformModuleTokenSpaceGuid.PcdPcieToDTbtRegister
  gSiPkgTokenSpaceGuid.PcdOverclockEnable
  gPlatformModuleTokenSpaceGuid.PcdTpmEnable
  gSiPkgTokenSpaceGuid.PcdTxtEnable
  gSiPkgTokenSpaceGuid.PcdAmtEnable                                     ## CONSUMES
  gPlatformModuleTokenSpaceGuid.PcdUpServerEnable                       ## CONSUMES
  gPlatformModuleTokenSpaceGuid.PcdNct677FPresent                       ## CONSUMES
  gPlatformModuleTokenSpaceGuid.PcdNat87393Present                      ## CONSUMES
  gSiPkgTokenSpaceGuid.PcdEmbeddedEnable                                ## CONSUMES
  gSiPkgTokenSpaceGuid.PcdITbtToPcieRegister
  gSiPkgTokenSpaceGuid.PcdPcieToITbtRegister
  gSiPkgTokenSpaceGuid.PcdBiosGuardEnable
  gSiPkgTokenSpaceGuid.PcdMrcTraceMessageSupported                      ## CONSUMES
  gPlatformModuleTokenSpaceGuid.PcdVirtualKeyboardEnable                ## CONSUMES
  gIntelFsp2WrapperTokenSpaceGuid.PcdFspModeSelection
  gMinPlatformPkgTokenSpaceGuid.PcdPerformanceEnable                    ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdUsbcEcPdNegotiation                     ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdEcPeciMode                              ## CONSUMES

  #SA USB Config
  gBoardModuleTokenSpaceGuid.PcdCpuXhciPortSupportMap

  gPlatformModuleTokenSpaceGuid.PcdExtendedBiosRegionSupport            ## CONSUMES
  gBoardModuleTokenSpaceGuid.PcdDpMuxGpio                               ## CONSUMES

[FixedPcd]
  gSiPkgTokenSpaceGuid.PcdITbtEnable                                    ## CONSUMES
  gSiPkgTokenSpaceGuid.PcdAdlLpSupport
  gPlatformModuleTokenSpaceGuid.PcdOneClickRecoveryEnable               ## CONSUMES
  gPlatformModuleTokenSpaceGuid.PcdRemotePlatformEraseSupport           ## CONSUMES
  gPlatformModuleTokenSpaceGuid.PcdDTbtEnable
  gSiPkgTokenSpaceGuid.PcdHybridStorageSupport


[Depex]
  gEfiSmbusHcProtocolGuid             AND
  gMemInfoProtocolGuid                AND
  gEfiVariableArchProtocolGuid        AND
  gEfiVariableWriteArchProtocolGuid   AND
  gEfiHiiDatabaseProtocolGuid         AND
  gDxePolicyProtocolGuid              AND
  gPchSpiProtocolGuid                 AND
  gEfiMpServiceProtocolGuid

