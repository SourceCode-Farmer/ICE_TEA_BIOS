/** @file

;******************************************************************************
;* Copyright (c) 2012 - 2020, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/
/**@file
  ME Setup Implementation

@copyright
  INTEL CONFIDENTIAL
  Copyright 2008 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains a 'Sample Driver' and is licensed as such under the terms
  of your license agreement with Intel or your vendor. This file may be modified
  by the user, subject to the additional terms of the license agreement.

@par Specification Reference:
**/

#include <ChipsetSetupConfig.h>
#include <IndustryStandard/Pci22.h>
#include <Library/Tpm2CommandLib.h>
#include <Library/DxeMeLib.h>
#include <Library/PttHeciLib.h>
#include <Library/BootGuardLibVer1.h>
#include <Library/MeFwStsLib.h>
#include <Library/HobLib.h>
#include <Library/DxeAmtHeciLib.h>
#include <Protocol/AmtWrapperProtocol.h>
#include <Protocol/OneClickRecoveryProtocol.h>
#include <Register/MeRegs.h>
#include <Register/HeciRegs.h>
//[-start-200215-IB06462109-remove]//
//#include <SetupPrivate.h>
//[-end-200215-IB06462109-remove]//
#include <MeSetup.h>
//[-start-200215-IB06462109-remove]//
//#include <OemSetup.h>
//[-end-200215-IB06462109-remove]//
//[-start-200215-IB06462109-remove]//
//#include <TcgSetup.h>
//[-end-200215-IB06462109-remove]//
#include <CpuRegs.h>
#include <MeFwHob.h>

//[-start-200215-IB06462109-add]//
#include <Library/PciLib.h>
#include <Library/DxeAmtHeciLib.h>
#include <SetupUtility.h>
#include <PlatformDefinitions.h>
#include <Library/PciSegmentLib.h>
#include <Library/DxeAsfLib.h>
//[-end-200215-IB06462109-add]//

///
/// HECI PCI Access Macro
///
#define HeciPciSegmentRead32(Register) PciSegmentRead32 (PCI_SEGMENT_LIB_ADDRESS (ME_SEGMENT, ME_BUS, ME_DEVICE_NUMBER, HECI_FUNCTION_NUMBER, Register))

//[-start-200215-IB06462109-add]//
ME_SETUP mMeSetup;
//[-end-200215-IB06462109-add]//


GLOBAL_REMOVE_IF_UNREFERENCED BOOLEAN            mMeReset = FALSE;
GLOBAL_REMOVE_IF_UNREFERENCED EFI_HII_HANDLE     mHiiHandle;
GLOBAL_REMOVE_IF_UNREFERENCED EFI_EVENT          mRemoteSessionEvent;

/**
  Initialize KVM by sending HECI message to ME

  @param[in] Event                The event registered.
  @param[in] Context              Event context. Not used in this event handler.
**/
VOID
EFIAPI
RemoteSessionDetection (
  IN EFI_EVENT                    Event,
  IN VOID                         *Context
  )
{
  EFI_STATUS            Status;
  AMT_WRAPPER_PROTOCOL  *AmtWrapper;
  ME_SETUP_STORAGE      MeSetupStorage;
  EFI_STRING            RequestString;
  EFI_TPL               OldTpl;

  DEBUG ((DEBUG_INFO, "%a () enter\n", __FUNCTION__));
  RequestString = NULL;
  OldTpl = EfiGetCurrentTpl ();

  gBS->RestoreTPL (TPL_APPLICATION);

  Status = gBS->LocateProtocol (&gAmtWrapperProtocolGuid, NULL, (VOID **) &AmtWrapper);
  if (!EFI_ERROR (Status)) {
    RequestString = HiiConstructRequestString (RequestString, OFFSET_OF (ME_SETUP_STORAGE, RemoteSessionActive), sizeof (MeSetupStorage.RemoteSessionActive));
    HiiGetBrowserData (&gMeSetupVariableGuid, L"MeSetupStorage", sizeof (ME_SETUP_STORAGE), (UINT8 *) &MeSetupStorage);
    if ((MeSetupStorage.RemoteSessionActive == 0) && AmtWrapper->IsKvmEnabled ()) {
      CreatePopUp (
        EFI_LIGHTGRAY | EFI_BACKGROUND_BLUE,
        NULL,
        L"",
        L"Remote Session is open in PCH-FW Configuration.",
        L"System needs to reset to set up item attribute.",
        L"Reset system in 5 seconds...",
        L"",
        NULL
        );
      gBS->Stall (5000000);;
      gRT->ResetSystem (EfiResetWarm, EFI_SUCCESS, 0, NULL);
    }
    FreePool (RequestString);
  }
  gBS->RaiseTPL (OldTpl);
  DEBUG ((DEBUG_INFO, "%a () exit\n", __FUNCTION__));

  return;
}

/**
  Check remote session status.

  If remote session is open, keep it as it is.
  If remote session is not open, because it might be open in runtime,
  BIOS needs to take care of the scenario accordingly.

  @param[in] Action          Specifies the type of action taken by the browser.
  @param[in] KeyValue        A unique value which is sent to the original exporting driver
                             so that it can identify the type of data to expect.
**/
VOID
CheckRemoteSession (
  IN EFI_BROWSER_ACTION                   Action,
  IN EFI_QUESTION_ID                      KeyValue
  )
{
  EFI_STATUS            Status;
  AMT_WRAPPER_PROTOCOL  *AmtWrapper;
  EFI_STRING            RequestString;
  ME_SETUP_STORAGE      MeSetupStorage;
  VOID                  *AfterConsolInNotifyReg;
  static BOOLEAN        IsConsoleEventCreated = FALSE;

  DEBUG ((DEBUG_INFO, "%a () enter, Action = 0x%x\n", __FUNCTION__, Action));

  RequestString = NULL;
  Status        = EFI_SUCCESS;

  RequestString = HiiConstructRequestString (RequestString, OFFSET_OF (ME_SETUP_STORAGE, RemoteSessionActive), sizeof (MeSetupStorage.RemoteSessionActive));
  if (RequestString == NULL) {
    return;
  }
  HiiGetBrowserData (&gMeSetupVariableGuid, L"MeSetupStorage", sizeof (ME_SETUP_STORAGE), (UINT8 *)&MeSetupStorage);

  //
  // If remote session is opened or closed after entering setup page, BIOS can update RemoteSessionActive value here.
  //
  if (Action == EFI_BROWSER_ACTION_FORM_OPEN) {
    Status = gBS->LocateProtocol (&gAmtWrapperProtocolGuid, NULL, (VOID **) &AmtWrapper);
    if (!EFI_ERROR (Status)) {
      MeSetupStorage.RemoteSessionActive = AmtWrapper->IsStorageRedirectionEnabled () ||
                                           AmtWrapper->IsSolEnabled () ||
                                           AmtWrapper->IsKvmEnabled ();
      HiiSetBrowserData (&gMeSetupVariableGuid, L"MeSetupStorage", sizeof(ME_SETUP_STORAGE), (UINT8 *)&MeSetupStorage, RequestString);
      FreePool (RequestString);
    }
  }

  if (MeSetupStorage.RemoteSessionActive == 1) {
    return;
  }

  //
  // Remote session could be opened/closed after entering PCH-FW configuration page, BIOS should create event to update RemoteSessionActive in below conditions:
  //   1. Create event to detect remote session open when entering PCH-FW or AMT Configuration
  //   2. Close event only when exiting PCH-FW configuration.
  //
  if (Action == EFI_BROWSER_ACTION_FORM_OPEN &&
      (KeyValue == ME_FORM_ACTION_KEY || KeyValue == AMT_FORM_ACTION_KEY)) {
    //
    // If remote session is opened after entering "PCH-FW Configuration", the "grayoutif" capability can't be changed in real time.
    // Below event is used to detect this scenario and trigger warm reset.
    //
    if ((MeSetupStorage.RemoteSessionActive == 0) && !IsConsoleEventCreated) {
      IsConsoleEventCreated = TRUE;
      DEBUG ((DEBUG_INFO, "Create RemoteSessionDetection event\n"));
      Status = gBS->CreateEvent (
                      EVT_NOTIFY_SIGNAL,
                      TPL_NOTIFY,
                      RemoteSessionDetection,
                      NULL,
                      &mRemoteSessionEvent
                      );
      ASSERT_EFI_ERROR (Status);

      Status = gBS->RegisterProtocolNotify (
                      &gEfiConsoleInDeviceGuid,
                      mRemoteSessionEvent,
                      &AfterConsolInNotifyReg
                      );
      ASSERT_EFI_ERROR (Status);
    }
  } else if (Action == EFI_BROWSER_ACTION_FORM_CLOSE && KeyValue == ME_FORM_ACTION_KEY && IsConsoleEventCreated) {
    DEBUG ((DEBUG_INFO, "Close RemoteSessionDetection event\n"));
    IsConsoleEventCreated = FALSE;
    Status = gBS->CloseEvent (mRemoteSessionEvent);
  }

  return;
}

//[-start-200826-IB16740113-add]// The function exist in HiiConfigAccess.c, RC1341 use it.
/**
  Converts the low nibble of a byte  to hex unicode character.

  @param[in] Nibble - lower nibble of a byte.

  Hex unicode character.

**/
CHAR16
NibbleToHexChar (
  IN UINT8      Nibble
  )
{
  Nibble &= 0x0F;
  if (Nibble <= 0x9) {
    return (CHAR16)(Nibble + L'0');
  }

  return (CHAR16)(Nibble - 0xA + L'A');
}
//[-end-200826-IB16740113-add]//

/**
  Converts the low nibble of a byte to hex unicode character.

  @param[in] Nibble      Lower nibble of a byte

  @retval CHAR16         Hex unicode character
**/
CHAR16
NibbleToHexChar (
  IN UINT8      Nibble
  );

/**
  Converts retrieved byte to string adding a hyphen after every 2nd byte.

  @param[out] String     Unicode String
  @param[in]  Buffer     Hash in hex format
  @param[in]  Length     Max length of hash to convert
**/
VOID
BinaryToHexString (
  OUT UINT16 *String,
  IN UINT8   *Buffer,
  IN UINTN   Length
  )
{
  UINTN   Index;
  UINT8   Byte;

  for (Index = 0; Index < Length; Index++) {

    if ((Index % 2 == 0) && (Index != 0)) {
      *String = '-';
      String++;
    }
    Byte = Buffer[Index];
    *String = NibbleToHexChar ((UINT8)(Byte >> 4));
    *(String + 1) = NibbleToHexChar (Byte);
    String += 2;
  }

  *String = '\0';
}

/**
  Sets One Click Recovery Setup Options are Supported
**/
VOID
SetOcrSetupSupported (
  VOID
  )
{
  EFI_STATUS                  Status;
  UINTN                       VariableSize;
  UINT32                      SetupVolVarAttr;
  SETUP_VOLATILE_DATA         VolatileData;
  ONE_CLICK_RECOVERY_PROTOCOL *OneClickRecovery;
  ONE_CLICK_RECOVERY_CAP      OcrCap;

  Status = gBS->LocateProtocol (&gOneClickRecoveryProtocolGuid, NULL, (VOID **)&OneClickRecovery);
  if (!EFI_ERROR (Status)) {
    OcrCap = OneClickRecovery->OcrCap ();

    VariableSize = sizeof (SETUP_VOLATILE_DATA);
    Status = gRT->GetVariable (
                    L"SetupVolatileData",
                    &gSetupVariableGuid,
                    &SetupVolVarAttr,
                    &VariableSize,
                    &VolatileData
                    );
    ASSERT_EFI_ERROR (Status);

    VolatileData.OcrBootHttpsSupported     = OcrCap.Bits.OcrBootHttps;
    VolatileData.OcrBootPbaSupported       = OcrCap.Bits.OcrBootPba;
    VolatileData.OcrBootWinReSupported     = OcrCap.Bits.OcrBootWinRe;
    VolatileData.OcrAmtDisSecBootSupported = OcrCap.Bits.OcrAmtDisSecBoot;

    Status = gRT->SetVariable (
                    L"SetupVolatileData",
                    &gSetupVariableGuid,
                    SetupVolVarAttr,
                    sizeof (SETUP_VOLATILE_DATA),
                    &VolatileData
                    );
    ASSERT_EFI_ERROR (Status);
  }
}

/**
  Initialize ME strings.

  @param[in]  HiiHandle          Hii Handle.
  @param[in]  Class              Setup form class.
**/
VOID
InitMeInfo (
  IN EFI_HII_HANDLE HiiHandle,
  IN UINT16         Class
  )
{
  EFI_STATUS            Status;
  UINTN                 VariableSize;
  UINT8                 *MeFwSkuValue;
  CHAR16                String[0x100];
  UINTN                 StringLength;
//[-start-200309-IB14630347-add]//
  UINT8               MeFwInfo;
//[-end-200309-IB14630347-add]//
  static  EFI_STRING_ID MeMode[] = {
    STRING_TOKEN(STR_ME_FW_MODE_NORMAL_MODE),
    STRING_TOKEN(STR_ME_FW_MODE_FAILED),
    STRING_TOKEN(STR_ME_FW_MODE_ALT_DISABLED),
    STRING_TOKEN(STR_ME_FW_MODE_TEMP_DISABLED),
    STRING_TOKEN(STR_ME_FW_MODE_SECOVER),
    STRING_TOKEN(STR_ME_FW_MODE_FAILED),
    STRING_TOKEN(STR_ME_FW_MODE_FAILED),
  };
  static  UINT8         *FwMeSku[] = {
     (UINT8*) ME_FW_IMAGE_TYPE_CONSUMER_STRING,
     (UINT8*) ME_FW_IMAGE_TYPE_CORPORATE_STRING,
     (UINT8*) ME_FW_IMAGE_TYPE_UNIDENTIFIED_STRING,
  };
  ME_INFO_SETUP_DATA    MeInfoSetupData;
  ME_FW_HOB             *MeFwHob;
  UINT32                Index;
  UINT32                MeFwStatus;
  UINT32                MeFwStatus2;
  UINT32                MeFwStatus3;
  UINT32                MeFwStatus4;
  UINT32                MeFwStatus5;
  UINT32                MeFwStatus6;

  if ((Class != MAIN_FORM_SET_CLASS) && (Class != ADVANCED_FORM_SET_CLASS)) {
    return;
  }

  mHiiHandle  = HiiHandle;
  MeFwHob     = NULL;
  Index       = 0;
  MeFwStatus  = 0;
  MeFwStatus2 = 0;
  MeFwStatus3 = 0;
  MeFwStatus4 = 0;
  MeFwStatus5 = 0;
  MeFwStatus6 = 0;

  DEBUG ((DEBUG_INFO, "%a() enter\n", __FUNCTION__));

  //
  // Set default string size assumption at no more than 256 bytes
  //
  StringLength = 0x100;

  VariableSize = sizeof (ME_INFO_SETUP_DATA);
  Status = gRT->GetVariable (
                  L"MeInfoSetup",
                  &gMeInfoSetupGuid,
                  NULL,
                  &VariableSize,
                  &MeInfoSetupData
                  );
  if (EFI_ERROR (Status)) {
    return;
  }

  InitString (
    mHiiHandle,
    STRING_TOKEN (STR_ME_FW_VERSION_VALUE),
    L"%d.%d.%d.%d",
    MeInfoSetupData.MeMajor,
    MeInfoSetupData.MeMinor,
    MeInfoSetupData.MeHotFix,
    MeInfoSetupData.MeBuildNo
  );

//[-start-200309-IB14630348-modify]//
  MeFwInfo = MeInfoSetupData.MeFirmwareInfo & ME_MODE_MASK;
  if (MeFwInfo < sizeof (MeMode) / sizeof (EFI_STRING_ID)) {
    StringLength = sizeof (String);
    Status = HiiLibGetString (mHiiHandle, MeMode[MeFwInfo], String, &StringLength);
    InitString (mHiiHandle, STRING_TOKEN (STR_ME_FW_MODE_VALUE), L"%s", String);
  }
//[-end-200309-IB14630348-modify]//

  VariableSize = sizeof (ME_SETUP);
  Status = gRT->GetVariable (
                  L"MeSetup",
                  &gMeSetupVariableGuid,
                  NULL,
                  &VariableSize,
                  &mMeSetup
                  );
  ASSERT_EFI_ERROR (Status);

  if (mMeSetup.MeImageType == ME_IMAGE_CONSUMER_SKU_FW) {
    MeFwSkuValue = FwMeSku[0];
  } else if (mMeSetup.MeImageType == ME_IMAGE_CORPORATE_SKU_FW) {
    MeFwSkuValue = FwMeSku[1];
  } else {
    MeFwSkuValue = FwMeSku[2];
  }
  InitString (
    mHiiHandle,
    STRING_TOKEN (STR_ME_FW_SKU_VALUE),
    L"%a",
    MeFwSkuValue
    );

#if FixedPcdGet8 (PcdOneClickRecoveryEnable) == 0x1
  SetOcrSetupSupported ();
#endif

  //
  // Check if Heci device is enabled
  //
  if (HeciPciSegmentRead32 (PCI_VENDOR_ID_OFFSET) == 0xFFFFFFFF) {
//[-start-190709-16990077-add]//
  	EFI_ME_STATUS_PROTOCOL *MeStatus;

    Status = gBS->LocateProtocol (
                    &gH2OMeStatusProtocolGuid,
                    NULL,
                    (VOID **) &MeStatus
                    );
    if (EFI_ERROR(Status)) {
      DEBUG ((DEBUG_ERROR, "gH2OMeStatusProtocolGuid locate unsuccessfully.\n"));
      return;

    }

    InitString (
      mHiiHandle,
      STRING_TOKEN (STR_ME_FW_STATUS_1_VALUE),
      L"0x%08x",
      MeStatus->Status1
      );

    InitString (
      mHiiHandle,
      STRING_TOKEN (STR_ME_FW_STATUS_2_VALUE),
      L"0x%08x",
      MeStatus->Status2
      );
//[-end-190709-16990077-add]//

    DEBUG ((DEBUG_INFO, "ME is disabled, read status from MeFwHob\n"));
    MeFwHob = GetFirstGuidHob (&gMeFwHobGuid);
    if (MeFwHob != NULL) {
      Index = FindMeFwStsIndex (R_ME_HFS);
      if (Index != 0xFF) {
        MeFwStatus = MeFwHob->Group[HECI1_DEVICE].Reg[Index];
      }

      Index = FindMeFwStsIndex (R_ME_HFS_2);
      if (Index != 0xFF) {
        MeFwStatus2 = MeFwHob->Group[HECI1_DEVICE].Reg[Index];
      }

      Index = FindMeFwStsIndex (R_ME_HFS_3);
      if (Index != 0xFF) {
        MeFwStatus2 = MeFwHob->Group[HECI1_DEVICE].Reg[Index];
      }

      Index = FindMeFwStsIndex (R_ME_HFS_4);
      if (Index != 0xFF) {
        MeFwStatus2 = MeFwHob->Group[HECI1_DEVICE].Reg[Index];
      }

      Index = FindMeFwStsIndex (R_ME_HFS_5);
      if (Index != 0xFF) {
        MeFwStatus2 = MeFwHob->Group[HECI1_DEVICE].Reg[Index];
      }

      Index = FindMeFwStsIndex (R_ME_HFS_6);
      if (Index != 0xFF) {
        MeFwStatus2 = MeFwHob->Group[HECI1_DEVICE].Reg[Index];
      }
    }
  } else {
    MeFwStatus  = HeciPciSegmentRead32 (R_ME_HFS);
    MeFwStatus2 = HeciPciSegmentRead32 (R_ME_HFS_2);
    MeFwStatus3 = HeciPciSegmentRead32 (R_ME_HFS_3);
    MeFwStatus4 = HeciPciSegmentRead32 (R_ME_HFS_4);
    MeFwStatus5 = HeciPciSegmentRead32 (R_ME_HFS_5);
    MeFwStatus6 = HeciPciSegmentRead32 (R_ME_HFS_6);
  }

  InitString (
    mHiiHandle,
    STRING_TOKEN (STR_ME_FW_STATUS_1_VALUE),
    L"0x%08x",
    MeFwStatus
    );

  InitString (
    mHiiHandle,
    STRING_TOKEN (STR_ME_FW_STATUS_2_VALUE),
    L"0x%08x",
    MeFwStatus2
    );

//[-start-200215-IB06462109-add]//
  MeExtractConfig ();
//[-end-200215-IB06462109-add]//
  InitString (
    mHiiHandle,
    STRING_TOKEN (STR_ME_FW_STATUS_3_VALUE),
    L"0x%08x",
    MeFwStatus3
    );

  InitString (
    mHiiHandle,
    STRING_TOKEN (STR_ME_FW_STATUS_4_VALUE),
    L"0x%08x",
    MeFwStatus4
    );

  InitString (
    mHiiHandle,
    STRING_TOKEN (STR_ME_FW_STATUS_5_VALUE),
    L"0x%08x",
    MeFwStatus5
    );

  InitString (
    mHiiHandle,
    STRING_TOKEN (STR_ME_FW_STATUS_6_VALUE),
    L"0x%08x",
    MeFwStatus6
    );
}

VOID
MeRouteConfig (
  VOID
  )
{
  ME_SETUP_STORAGE        MeSetupStorage = {0};
  ME_SETUP_STORAGE        MeBackupStorage = {0};
//[-start-200215-IB06462109-modify]//
//  TCG_SETUP               TcgSetup = {0};
  KERNEL_CONFIGURATION    KernelConfig;
//[-end-200215-IB06462109-modify]//
  UINTN                   Size;
  EFI_STATUS              Status;
  UINT32                  EnableBitmap;
  UINT32                  DisableBitmap;
  HECI_PROTOCOL           *Heci;

  DEBUG ((DEBUG_INFO, "%a () enter\n", __FUNCTION__));

//[-start-201127-IB17510129-add]//
  CHIPSET_CONFIGURATION   ChipsetConfig;
  
  Size = sizeof (CHIPSET_CONFIGURATION);
  Status = gRT->GetVariable (
                  L"Setup",
                  &gSystemConfigurationGuid,
                  NULL,
                  &Size,
                  (VOID *)&ChipsetConfig
                  );
  if (EFI_ERROR (Status)) {
    DEBUG ((DEBUG_ERROR, "ChipsetConfig does not exist\n"));
    ASSERT_EFI_ERROR (Status);
  }
//[-end-201127-IB17510129-add]//

  Status = gBS->LocateProtocol (&gHeciProtocolGuid, NULL, (VOID **) &Heci);
  if (MeIsAfterEndOfPost () || EFI_ERROR (Status)) {
    return;
  }

  Size = sizeof (ME_SETUP_STORAGE);
  gRT->GetVariable (L"MeSetupStorage", &gMeSetupVariableGuid, NULL, &Size, &MeSetupStorage);
  gRT->GetVariable (L"MeBackupStorage", &gMeSetupVariableGuid, NULL, &Size, &MeBackupStorage);
  Size = sizeof (ME_SETUP);
  gRT->GetVariable (L"MeSetup", &gMeSetupVariableGuid, NULL, &Size, &mMeSetup);
//[-start-200215-IB06462109-modify]//
//  Size = sizeof (TCG_SETUP);
//  gRT->GetVariable (L"TcgSetup", &gSetupVariableGuid, NULL, &Size, &TcgSetup);
  Status = GetKernelConfiguration (&KernelConfig);
//[-end-200215-IB06462109-modify]//
  if (MeSetupStorage.InvokeArbSvnCommit != MeBackupStorage.InvokeArbSvnCommit) {
    if ((mMeSetup.AutoArbSvnCommit == 0) && (MeSetupStorage.InvokeArbSvnCommit == 1)) {
      Status = HeciArbSvnCommitMsg ();
    }
  }

  if (MeSetupStorage.MeStateControl != MeBackupStorage.MeStateControl) {
    mMeReset = TRUE;

    if (MeSetupStorage.MeStateControl == 0) {
      Status = HeciSetMeDisableMsg ();
    } else {
      Status = HeciSetMeEnableMsg ();
    }
  }

  if (MeSetupStorage.FwUpdEnabled != MeBackupStorage.FwUpdEnabled) {
    Status = HeciSetFwUpdateState (MeSetupStorage.FwUpdEnabled);
  }

  if (MeSetupStorage.MngState != MeBackupStorage.MngState) {
    mMeReset = TRUE;
    if (MeSetupStorage.MngState == 1) {
      //
      // User wants to enable Manageability Features
      //
      EnableBitmap  = AMT_BITMASK;
      DisableBitmap = CLEAR_FEATURES_BITMASK;
    } else {
      //
      // User wants to disable Manageability Features
      //
      EnableBitmap  = CLEAR_FEATURES_BITMASK;
      DisableBitmap = AMT_BITMASK;
    }
    Status = HeciFwFeatureStateOverride (EnableBitmap, DisableBitmap);
  }

  if (MeSetupStorage.PttState != MeBackupStorage.PttState) {
    mMeReset = TRUE;
  }

  //
  // Pass BIOS TPM Deactivate setting to BtG ACM
  //
  if (MeSetupStorage.TpmDeactivate != MeBackupStorage.TpmDeactivate) {
//[-start-200215-IB06462109-modify]//
//    if (TcgSetup.dTpm12Present == 1) {
    if (KernelConfig.TpmDeviceOk == 1) {
//[-end-200215-IB06462109-modify]//
      DEBUG ((DEBUG_INFO, "TPM is present.\nCalling HeciSetAcmTpmData\n"));
      Status = HeciSetAcmTpmData (MeSetupStorage.TpmDeactivate);
    } else {
      DEBUG ((DEBUG_INFO, "TPM is not present, do not send MKHI ACM TPM Data.\n"));
    }
  }

  if (MeSetupStorage.DelayedAuthenticationMode != MeBackupStorage.DelayedAuthenticationMode) {
    mMeReset = TRUE;
    HeciSetDamState (MeSetupStorage.DelayedAuthenticationMode);
  }

//[-start-201127-IB17510129-add]//
  if (MeSetupStorage.FipsModeSelect == 0) {
    ChipsetConfig.FipsAutoSync = 0;
  }
//[-end-201127-IB17510129-add]//

  if (MeSetupStorage.FipsModeSelect != MeBackupStorage.FipsModeSelect) {
    HeciSetFipsMode (MeSetupStorage.FipsModeSelect);
  }

  if (MeSetupStorage.UpidState != MeBackupStorage.UpidState) {
    HeciSetUpidFeatureState (UpidFeaturePlatId, MeSetupStorage.UpidState);
  }

  if (MeSetupStorage.UpidOsControlState != MeBackupStorage.UpidOsControlState) {
    HeciSetUpidOsControlState (UpidFeaturePlatId, MeSetupStorage.UpidOsControlState);
  }


  //
  // Remember current FW's state to avoid sending unnecessary messages next time
  //
  gRT->SetVariable (
         L"MeBackupStorage",
         &gMeSetupVariableGuid,
         EFI_VARIABLE_BOOTSERVICE_ACCESS,
         sizeof (ME_SETUP_STORAGE),
         &MeSetupStorage
         );
//[-start-201127-IB17510129-add]//
  gRT->SetVariable (
         L"Setup",
         &gSystemConfigurationGuid,
         EFI_VARIABLE_BOOTSERVICE_ACCESS,
         sizeof (CHIPSET_CONFIGURATION),
         &ChipsetConfig
         );
//[-end-201127-IB17510129-add]//
}

/**
  Setup callback executed when user selects PCH-FW configuration from the BIOS UI.
  Changes strings and settings of certain options.

  @param[in] This            Points to the EFI_HII_CONFIG_ACCESS_PROTOCOL.
  @param[in] Action          Specifies the type of action taken by the browser.
  @param[in] KeyValue        A unique value which is sent to the original exporting driver
                             so that it can identify the type of data to expect.
  @param[in] Type            The type of value for the question.
  @param[in] Value           A pointer to the data being sent to the original exporting driver.
  @param[out] ActionRequest  On return, points to the action requested by the callback function.

  @retval EFI_SUCCESS        The callback successfully handled the action
  @retval EFI_UNSUPPORTED    The specified action is not supported by the callback
**/
EFI_STATUS
EFIAPI
MeFormCallBackFunction (
  IN CONST EFI_HII_CONFIG_ACCESS_PROTOCOL *This,
  IN EFI_BROWSER_ACTION                   Action,
  IN EFI_QUESTION_ID                      KeyValue,
  IN UINT8                                Type,
  IN EFI_IFR_TYPE_VALUE                   *Value,
  OUT EFI_BROWSER_ACTION_REQUEST          *ActionRequest
  )
{
  EFI_STATUS            Status;
  BOOLEAN               PttCapability;
  BOOLEAN               PttCurrentState;
  ME_SETUP_STORAGE      MeSetupStorage;
  UINTN                 Size;
  UINT32                VariableAttr;

  DEBUG ((DEBUG_INFO, "%a () enter, Action = 0x%x, KeyValue = 0x%x\n", __FUNCTION__, Action, KeyValue));

  if (Action >= EFI_BROWSER_ACTION_DEFAULT_STANDARD) {
    switch (KeyValue) {
      case MNG_STATE_KEY:
      case DAM_STATE_KEY:
        return EFI_SUCCESS;

     default:
       return EFI_UNSUPPORTED;
    }
  }

  //
  // If it is corporate image and both RemoteSessionActive/AfterEoP are 0, BIOS needs to check remote session status in runtime.
  //
  Size = sizeof (ME_SETUP_STORAGE);
  Status = gRT->GetVariable (L"MeSetupStorage", &gMeSetupVariableGuid, &VariableAttr, &Size, &MeSetupStorage);
  if (!EFI_ERROR (Status) &&
      (mMeSetup.MeImageType == ME_IMAGE_CORPORATE_SKU_FW) &&
      (MeSetupStorage.RemoteSessionActive == 0) &&
      (MeSetupStorage.AfterEoP == 0)) {
    CheckRemoteSession (Action, KeyValue);
  }

  if (Action == EFI_BROWSER_ACTION_FORM_OPEN) {
    switch (KeyValue) {

      case PTT_TRIGGER_FORM_OPEN_ACTION_KEY:
        Status = PttHeciGetCapability (&PttCapability);
        if (EFI_ERROR (Status)) {
          PttCapability = FALSE;
        }
        Status = PttHeciGetState (&PttCurrentState);
        if (EFI_ERROR (Status)) {
          PttCurrentState = FALSE;
        }

        InitString (
          mHiiHandle,
          STRING_TOKEN (STR_PTT_CAP_STATE_VALUE),
          L"%d / %d",
          PttCapability,
          PttCurrentState
          );

        return EFI_SUCCESS;

      default:
         return EFI_UNSUPPORTED;
     }
  }

  return EFI_UNSUPPORTED;
}

VOID
MeExtractConfig (
  VOID
  )
{
  EFI_STATUS              Status;
  UINT8                   FwUpdateState;
  UINT32                  MeMode;
  MEFWCAPS_SKU            CurrentFeatures;
  ME_SETUP_STORAGE        MeSetupStorage;
  UINTN                   Size;
  UINT32                  VariableAttr;
  AMT_WRAPPER_PROTOCOL    *AmtWrapper;
  HECI_FW_STS4_REGISTER   MeFwSts4;
  UINT32                  MsrValue;
  HECI_PROTOCOL           *Heci;
  CHAR16                  String[80]; // Fit 64 chars of UPID String + 15 dashes + Null char
  UINTN                   StringLength;
  EFI_STRING_ID           FipsMode[] = {
    STRING_TOKEN(STR_DISABLED),
    STRING_TOKEN(STR_ENABLED)
  };
  GET_FIPS_MODE_DATA      FipsModeData;
  UINT32                  NumOfEntries;
  ARB_SVN_INFO_ENTRY      *ArbSvnInfo;
  MEFWCAPS_SKU            UserCapabilities;
  UINT32                  Index;
  UINT32                  OemPlatformIdType;
  UINT8                   OemPlatformId[MAX_PLAT_ID_SIZE];
  UINT8                   CsmePlatformId[MAX_PLAT_ID_SIZE];

  DEBUG ((DEBUG_INFO, "%a () enter\n", __FUNCTION__));

  AmtWrapper   = NULL;
  VariableAttr = 0;
  NumOfEntries = MFT_KEY_USAGE_INDEX_UNDEFINED_MANIFEST;
  ArbSvnInfo   = NULL;
  Index        = 0;

  ArbSvnInfo = AllocateZeroPool (NumOfEntries * sizeof (ARB_SVN_INFO_ENTRY));
  if (ArbSvnInfo != NULL) {
    Status = HeciArbSvnGetInfoMsg (&NumOfEntries, ArbSvnInfo);

    if (!EFI_ERROR (Status)) {
      for (Index = 0; Index < NumOfEntries; Index++) {
        //
        // BIOS should always display the entry with MFT_KEY_USAGE_INDEX_CSE_RBE_MANIFEST
        //
        if (ArbSvnInfo[Index].UsageId == MFT_KEY_USAGE_INDEX_CSE_RBE_MANIFEST) {
          InitString (mHiiHandle, STRING_TOKEN (STR_ARB_SVN_MIN_VALUE), L"%d", ArbSvnInfo[Index].MinAllowedSvn);
          InitString (mHiiHandle, STRING_TOKEN (STR_ARB_SVN_CURR_VALUE), L"%d", ArbSvnInfo[Index].ExecutingSvn);
          break;
        }
      }
    }
  }


  Size = sizeof (ME_SETUP_STORAGE);
  Status = gRT->GetVariable (L"MeSetupStorage", &gMeSetupVariableGuid, &VariableAttr, &Size, &MeSetupStorage);
  if (EFI_ERROR (Status)) {
    DEBUG ((DEBUG_ERROR, "MeSetupStorage does not exist. Assuming default values.\n"));
    VariableAttr = EFI_VARIABLE_NON_VOLATILE | EFI_VARIABLE_BOOTSERVICE_ACCESS;
    MeSetupStorage.MngState = 1;
    MeSetupStorage.FwUpdEnabled = 1;
    MeSetupStorage.MeStateControl = 0;
    MeSetupStorage.AfterEoP = 0;
    MeSetupStorage.RemoteSessionActive = 0;
    MeSetupStorage.PttState = 0;
    MeSetupStorage.BootGuardSupport = 0;
    MeSetupStorage.MeasureBoot = 0;
    MeSetupStorage.TpmDeactivate = 0;
    MeSetupStorage.DelayedAuthenticationMode = 0;
    MeSetupStorage.FipsModeSelect = 0;
    MeSetupStorage.AmtState = 1;
    MeSetupStorage.UpidSupport = 0;
    MeSetupStorage.UpidState = 0;
    MeSetupStorage.UpidOsControlState = 0;
    MeSetupStorage.ActiveLanInterface = 0;
  }

  //
  // Always disable InvokeArbSvnCommit setup option
  //
  MeSetupStorage.InvokeArbSvnCommit = 0;

  HeciGetDamState (&MeSetupStorage.DelayedAuthenticationMode);

  if (IsBootGuardSupported ()) {
    MeSetupStorage.BootGuardSupport = 1;
  }

  MsrValue = (UINT32) AsmReadMsr64 (MSR_BOOT_GUARD_SACM_INFO);
  if (MsrValue & B_BOOT_GUARD_SACM_INFO_MEASURED_BOOT) {
    MeSetupStorage.MeasureBoot = 1;
  }

  if (MeIsAfterEndOfPost ()) {
    MeSetupStorage.AfterEoP = 1;
  } else {
    MeSetupStorage.AfterEoP = 0;
  }

  Status = gBS->LocateProtocol (&gHeciProtocolGuid, NULL, (VOID **) &Heci);
  if (EFI_ERROR (Status)) {
    MeSetupStorage.AfterEoP = 1;
    goto Done;
  }

  Heci->GetMeMode (&MeMode);
  if (MeMode == ME_MODE_NORMAL) {
    MeSetupStorage.MeStateControl = 1;
  } else {
    goto Done;
  }

  Status = gBS->LocateProtocol (&gAmtWrapperProtocolGuid, NULL, (VOID **) &AmtWrapper);
  if (!EFI_ERROR (Status)) {
    MeSetupStorage.RemoteSessionActive = AmtWrapper->IsStorageRedirectionEnabled () ||
                                         AmtWrapper->IsSolEnabled () ||
                                         AmtWrapper->IsKvmEnabled ();
  }

  Status = HeciGetFwFeatureStateMsg (&CurrentFeatures);
  if (!EFI_ERROR (Status)) {
    MeSetupStorage.MngState   = !!CurrentFeatures.Fields.Amt;
    MeSetupStorage.PttState   = !!CurrentFeatures.Fields.PTT;
  }

  Status = HeciGetUserCapabilitiesState (&UserCapabilities);
  if (!EFI_ERROR (Status)) {
    MeSetupStorage.AmtState   = !!UserCapabilities.Fields.Amt;
  }

  MeFwSts4.ul = HeciPciSegmentRead32 (R_ME_HFS_4);
  if (MeFwSts4.ul != 0xFFFFFFFF) {
    MeSetupStorage.TpmDeactivate = (UINT8) MeFwSts4.r.dTpm12Deactivated;
  }

  //
  // Skip Local Firmware update if it is not Consumer sku or not in normal mode
  //
  Size = sizeof (ME_SETUP);
  Status = gRT->GetVariable (L"MeSetup", &gMeSetupVariableGuid, NULL, &Size, &mMeSetup);
  ASSERT_EFI_ERROR (Status);

  Status = HeciGetFwUpdateState (&FwUpdateState);
  if (!EFI_ERROR (Status)) {
    MeSetupStorage.FwUpdEnabled = FwUpdateState;
  }

  if (mMeSetup.MeImageType == IntelMeCorporateFw) {
    //
    // FIPS feature
    //
    Status = HeciGetFipsMode (&FipsModeData);
    if (!EFI_ERROR (Status)) {
//[-start-201127-IB17510129-add]//
      MeSetupStorage.FipsModeSelect = (UINT8)FipsModeData.FipsMode;
//[-end-201127-IB17510129-add]//
      StringLength = sizeof (String);
      Status = HiiLibGetString (mHiiHandle, FipsMode[!!FipsModeData.FipsMode], String, &StringLength);
      InitString (mHiiHandle, STRING_TOKEN (STR_ME_FW_FIPS_CURRENT_MODE_VALUE), L"%s", String);
      InitString (
        mHiiHandle,
        STRING_TOKEN (STR_ME_FW_FIPS_CRYPTO_VER_VALUE),
        L"%d.%d.%d.%d",
        FipsModeData.CryptoVersion.Major,
        FipsModeData.CryptoVersion.Minor,
        FipsModeData.CryptoVersion.Build,
        FipsModeData.CryptoVersion.Hotfix
      );
    }

    //
    // UPID Platform Id feature
    //
    Status = HeciGetPlatIdSupportedState (&MeSetupStorage.UpidSupport);
    if (!EFI_ERROR (Status) && MeSetupStorage.UpidSupport == 1) {
      Status = HeciGetUpidFeatureState (UpidFeaturePlatId, &MeSetupStorage.UpidState);
      if (!EFI_ERROR (Status) && MeSetupStorage.UpidState == 1) {
        Status = HeciGetUpidPlatformId (&OemPlatformIdType, OemPlatformId, CsmePlatformId);
        if (!EFI_ERROR (Status)) {
          //
          // Update CSME Platform ID
          //
          ZeroMem (String, sizeof (String));
          BinaryToHexString (String, CsmePlatformId, MAX_PLAT_ID_SIZE);
          InitString (mHiiHandle, STRING_TOKEN (STR_UPID_CSME_PLAT_ID_VALUE), L"%s", String);

          //
          // Update OEM Platform ID
          //
          ZeroMem (String, sizeof (String));
          if (OemPlatformIdType == OemPlatIdTypePrintableString) {
            AsciiStrToUnicodeStrS ((CHAR8 *)OemPlatformId, String, MAX_PLAT_ID_SIZE);
          } else if (OemPlatformIdType == OemPlatIdTypeBinary) {
            BinaryToHexString (String, OemPlatformId, MAX_PLAT_ID_SIZE);
          }
          if (OemPlatformIdType != OemPlatIdTypeNotSet) {
            InitString (mHiiHandle, STRING_TOKEN (STR_UPID_OEM_PLAT_ID_VALUE), L"%s", String);
          }
        }
      }
      Status = HeciGetUpidOsControlState (UpidFeaturePlatId, &MeSetupStorage.UpidOsControlState);
    }

  }

Done:
  if (ArbSvnInfo != NULL) {
    FreePool (ArbSvnInfo);
  }
  Status = gRT->SetVariable (
                  L"MeSetupStorage",
                  &gMeSetupVariableGuid,
                  VariableAttr,
                  sizeof (ME_SETUP_STORAGE),
                  &MeSetupStorage
                  );
  Status = gRT->SetVariable (
                  L"MeBackupStorage",
                  &gMeSetupVariableGuid,
                  EFI_VARIABLE_BOOTSERVICE_ACCESS,
                  sizeof (ME_SETUP_STORAGE),
                  &MeSetupStorage
                  );

  // Since DEBUG() only allow max 12 format parameters, so call it twice.
  DEBUG ((DEBUG_INFO, "MeExtractConfig %d %d %d %d %d %d %d %d %d %d %d %d",
    MeSetupStorage.MngState,
    MeSetupStorage.FwUpdEnabled,
    MeSetupStorage.MeStateControl,
    MeSetupStorage.AfterEoP,
    MeSetupStorage.RemoteSessionActive,
    MeSetupStorage.PttState,
    MeSetupStorage.BootGuardSupport,
    MeSetupStorage.MeasureBoot,
    MeSetupStorage.TpmDeactivate,
    MeSetupStorage.DelayedAuthenticationMode,
    MeSetupStorage.FipsModeSelect,
    MeSetupStorage.AmtState
    ));
  DEBUG ((DEBUG_INFO, " %d %d %d %d\n",
    MeSetupStorage.UpidSupport,
    MeSetupStorage.UpidState,
    MeSetupStorage.UpidOsControlState,
    MeSetupStorage.ActiveLanInterface
    ));

}

