## @file
#  Board description file containing default configuration (PCD) settings for the project.
#  This description file contains default Feature PCDs that is configured to opt-in/out an optional feature.
#  Only PCD type Feature's PCDs is allowed in this file. Other PCD types such "PcdDynamic" are not allowed.
#  This file also configures the default PcdBootStage to control Boot execution flow complying to Intel
#  Minimum Platform Architecture.
#
# @copyright
#  INTEL CONFIDENTIAL
#  Copyright 2020 - 2021 Intel Corporation.
#
#  The source code contained or described herein and all documents related to the
#  source code ("Material") are owned by Intel Corporation or its suppliers or
#  licensors. Title to the Material remains with Intel Corporation or its suppliers
#  and licensors. The Material may contain trade secrets and proprietary and
#  confidential information of Intel Corporation and its suppliers and licensors,
#  and is protected by worldwide copyright and trade secret laws and treaty
#  provisions. No part of the Material may be used, copied, reproduced, modified,
#  published, uploaded, posted, transmitted, distributed, or disclosed in any way
#  without Intel's prior express written permission.
#
#  No license under any patent, copyright, trade secret or other intellectual
#  property right is granted to or conferred upon you by disclosure or delivery
#  of the Materials, either expressly, by implication, inducement, estoppel or
#  otherwise. Any license under such intellectual property rights must be
#  express and approved by Intel in writing.
#
#  Unless otherwise agreed by Intel in writing, you may not remove or alter
#  this notice or any other notice embedded in Materials by Intel or
#  Intel's suppliers or licensors in any way.
#
#  This file contains a 'Sample Driver' and is licensed as such under the terms
#  of your license agreement with Intel or your vendor. This file may be modified
#  by the user, subject to the additional terms of the license agreement.
#
# @par Specification
##
[Packages]
  MinPlatformPkg\MinPlatformPkg.dec

[PcdsFixedAtBuild]
  # Intel\AlderLakePlatSamplePkg\PlatformCommonLib.dsc
  # PlatformBootManagerLib
  gBoardModuleTokenSpaceGuid.PcdSetupEnable|TRUE
  # Intel\AlderLakePlatSamplePkg\PlatformCommonLib.dsc
  # EcLib, EcMiscLib, EcTcssLib, EcHwLib
  gBoardModuleTokenSpaceGuid.PcdEcEnable|TRUE
  # Intel\AlderLakePlatSamplePkg\PlatformDxeLib.dsc
  # DimmInfoLib
  # Intel\AlderLakePlatSamplePkg\PlatformPkg.dsc
  # PcdResetOnMemoryTypeInformationChange
  gBoardModuleTokenSpaceGuid.PcdS4Enable|TRUE
  # Intel\AlderLakePlatSamplePkg\PlatformPkg.dsc
  # $(PLATFORM_FEATURES_PATH)/Sata/IntelUefiRaidDiskInfo/IntelUefiRaidDiskInfo.inf
  gBoardModuleTokenSpaceGuid.PcdIntelRaidEnable|TRUE
  # Intel\AlderLakePlatSamplePkg\PlatformPkg.dsc
  # $(PLATFORM_FEATURES_PATH)/Hsti/HstiResultDxe/HstiResultDxe.inf
  # Intel\AlderLakePlatSamplePkg\PlatformPkg.dsc
  # $(PLATFORM_FEATURES_PATH)/NullStall/NullStall.inf

  gIntelFsp2WrapperTokenSpaceGuid.PcdFspModeSelection|0

  gPlatformModuleTokenSpaceGuid.PcdUsb3SerialStatusCodeEnable|FALSE
  gPlatformModuleTokenSpaceGuid.PcdSerialPortEnable|TRUE
  gPlatformModuleTokenSpaceGuid.PcdCapsuleEnable|TRUE
  gPlatformModuleTokenSpaceGuid.PcdPlatformCmosAccessSupport|TRUE
  gPlatformModuleTokenSpaceGuid.PcdTpmEnable|FALSE
  gPlatformModuleTokenSpaceGuid.PcdVtioEnable|TRUE
  gPlatformModuleTokenSpaceGuid.PcdPiI2cStackEnable|TRUE
  gPlatformModuleTokenSpaceGuid.PcdI2cTouchDriverEnable|TRUE
  gPlatformModuleTokenSpaceGuid.PcdNetworkEnable|TRUE
  gPlatformModuleTokenSpaceGuid.PcdPciHotplugEnable|TRUE
  gPlatformModuleTokenSpaceGuid.PcdUsbTypeCEnable|TRUE
  gPlatformModuleTokenSpaceGuid.PcdDnxSupportEnable|TRUE
  gPlatformModuleTokenSpaceGuid.PcdEnableSecureErase|TRUE
  gPlatformModuleTokenSpaceGuid.PcdUsbFnEnable|FALSE
  gPlatformModuleTokenSpaceGuid.PcdRstOneClickEnable|TRUE
  gPlatformModuleTokenSpaceGuid.PcdDeprecatedFunctionRemove|FALSE
  gPlatformModuleTokenSpaceGuid.PcdSymbolInReleaseEnable|FALSE
  gPlatformModuleTokenSpaceGuid.PcdVirtualKeyboardEnable|TRUE

  gPlatformModuleTokenSpaceGuid.PcdUserAuthenticationEnable|TRUE
  gPlatformModuleTokenSpaceGuid.PcdEbcEnable|TRUE
  gPlatformModuleTokenSpaceGuid.PcdHddPasswordEnable|TRUE
  gPlatformModuleTokenSpaceGuid.PcdGigUndiEnable|TRUE
  gPlatformModuleTokenSpaceGuid.PcdScsiEnable|TRUE
  gPlatformModuleTokenSpaceGuid.PcdJpgEnable|TRUE
  gPlatformModuleTokenSpaceGuid.PcdSmiVariableEnable|TRUE
  gPlatformModuleTokenSpaceGuid.PcdIgdIntHookEnable|TRUE
  gPlatformModuleTokenSpaceGuid.PcdUsbEnable|TRUE
  gPlatformModuleTokenSpaceGuid.PcdSredirBinEnable|TRUE
  gPlatformModuleTokenSpaceGuid.PcdAhciEnable|TRUE
  gPlatformModuleTokenSpaceGuid.PcdLegacySredirBinEnable|TRUE
  gPlatformModuleTokenSpaceGuid.PcdNetworkIp6Enable|TRUE
  gPlatformModuleTokenSpaceGuid.PcdNetworkIscsiEnable|TRUE
  gPlatformModuleTokenSpaceGuid.PcdNetworkVlanEnable|TRUE
  gPlatformModuleTokenSpaceGuid.PcdFatEnable|TRUE
  gPlatformModuleTokenSpaceGuid.PcdCryptoEnable|TRUE                 # Current Smbios implementation needs this
  gPlatformModuleTokenSpaceGuid.PcdLzmaEnable|TRUE
  gPlatformModuleTokenSpaceGuid.PcdDxeCompressEnable|TRUE
  gPlatformModuleTokenSpaceGuid.PcdVtioEnable|TRUE
  gPlatformModuleTokenSpaceGuid.PcdUserIdentificationEnable|FALSE
  gPlatformModuleTokenSpaceGuid.PcdH8S2113Present|FALSE
  gPlatformModuleTokenSpaceGuid.PcdNat87393Present|FALSE
  gPlatformModuleTokenSpaceGuid.PcdNct677FPresent|FALSE
  gPlatformModuleTokenSpaceGuid.PcdSkipFspTempRamInitAndExit|FALSE
  gPlatformModuleTokenSpaceGuid.PcdTdsEnable|TRUE
  gPlatformModuleTokenSpaceGuid.PcdOpalPasswordEnable|TRUE
  gPlatformModuleTokenSpaceGuid.PcdMemoryTestEnable|TRUE
  gPlatformModuleTokenSpaceGuid.PcdDTbtEnable|TRUE

[PcdsFeatureFlag]
  # Intel\AlderLakePlatSamplePkg\PlatformPkg.dsc
  # PeiPlatformRecoveryLib, GuidForwardLib, PeiWdtAppLib, PeiWdtAppLib
  gBoardModuleTokenSpaceGuid.PcdAdvancedFeatureEnable|TRUE
  # Intel\AlderLakePlatSamplePkg\PlatformPkg.dsc
  # $(PLATFORM_FEATURES_PATH)/Gop/GopDebugDxe/GopDebugDxe.inf
  gBoardModuleTokenSpaceGuid.PcdIntelGopEnable|TRUE

  gMinPlatformPkgTokenSpaceGuid.PcdSmiHandlerProfileEnable|FALSE
  gMinPlatformPkgTokenSpaceGuid.PcdUefiSecureBootEnable|FALSE
  gMinPlatformPkgTokenSpaceGuid.PcdPerformanceEnable|FALSE
  gMinPlatformPkgTokenSpaceGuid.PcdTpm2Enable|FALSE

