================================================================================
BIOS ROM Image Generation with PatchXml tool
================================================================================

============
Introduction
============
The generation of BIOS ROM Images is dependent on FITC tool which builds the
images for the whole of BIOS region, including ME, Descriptor, Straps, etc.
Board specific settings are described in xml, which will be passed in to FITC
to generate the final image. Thereafter, the generated image will be strap
edited if needed. This is when FITC xml could not produce the required strap
values. Finally, the image will be copied to the output, which is RomImages

=================
Problem Statement
=================
There are quite a number of board specific rom images to be generated, and
each settings are describe in FITC xmls. All these xmls are based on a FITC
template which can be generated using FITC. However, the problem arises when
FITC's template are changed, which cause all the previous version's board
specific xml cannot be used. All the xmls have to be manually modified.

Aside from that, the process of creating the rom images are written in the
build flow's batch files. As a result, adding a new configuration, involves
the modification of multiple, if not one, batch files.

==========
Build Flow
==========
1) Iterate over all available .p file
2) Modify the FITC xml template using patch tool and .p file and
    generate the xml based on the name of the .p file
3) Run FITC to generate the rom images
4) Run an optional batch file with the same name as the .p file
    a) Does strap editing on the output image
    b) Rename image

=================================
Method to Introduce New Rom Image
=================================
Now, integrating new rom image with different settings will be relatively
easier than before. User would just have to prepare two files, and put them
in the FITC folder, and the rom images will be automatically generated:
1) *.p file,
      - which contains the changes needed to the xml according to board settings
      - guide on p file syntax is commented in PatchXml.js
2) *.bat file,
      - to edit the strap, if needed, and to rename the images to desired name.