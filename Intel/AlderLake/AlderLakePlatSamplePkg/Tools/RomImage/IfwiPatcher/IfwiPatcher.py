import os
import sys
import argparse

version = "0.1.1"

def main():
    file_error = False
    opts = get_args(version)
    # Raise file_error flag and terminate IfwiPathcer if the file paths are invalid.
    if not os.path.isfile(opts.ifwi_filename):
        print("No such file or directory: '{}'".format(opts.ifwi_filename))
        file_error = True
    if not os.path.isfile(opts.bios_filename):
        print("No such file or directory: '{}'".format(opts.bios_filename))
        file_error = True
    if file_error is True:
        sys.exit(1)

    with open(opts.ifwi_filename, 'rb') as ifwi_fileobj:
        ifwi_file = ifwi_fileobj.read()
    with open(opts.bios_filename, 'rb') as bios_fileobj:
        bios_file = bios_fileobj.read()

    # Get BIOS region begin offset address from IFWI BIN file offset 0x44 0x45.
    if sys.version_info.major > 2:
        low_byte = ifwi_file[0x44]
        high_byte = ifwi_file[0x45]
    else:
        low_byte = int(ifwi_file[0x44].encode('hex'), 16)
        high_byte = int(ifwi_file[0x45].encode('hex'), 16)
    bios_begin = (high_byte << 8) | low_byte
    bios_begin = bios_begin << 0x0C

    # Get BIOS region end offset address from IFWI BIN file offset 0x46 0x47.
    if sys.version_info.major > 2:
        low_byte = ifwi_file[0x46]
        high_byte = ifwi_file[0x47]
    else:
        low_byte = int(ifwi_file[0x46].encode('hex'), 16)
        high_byte = int(ifwi_file[0x47].encode('hex'), 16)
    bios_end = (high_byte << 8) | low_byte
    bios_end = (bios_end << 0x0C) | 0xFFF
    bios_len = bios_end - bios_begin + 1

    # Check if BIOS ROM file size matches BIOS region length in IFWI.
    if not (len(bios_file) == bios_len):
        print("The BIOS ROM file size does not match to BIOS region length descriptor in IFWI.")
        sys.exit(1)

    # Stitch the BIOS ROM with IFWI BIN file.
    byte_index = 0
    combined_file = bytearray(len(ifwi_file))
    for index in range(bios_begin):
        combined_file[index] = ifwi_file[index]
        byte_index = byte_index + 1

    for byte in bios_file:
        combined_file[byte_index] = byte
        byte_index = byte_index + 1

    output_path = os.path.dirname(opts.output_filename)
    if not os.path.exists(output_path):
        os.mkdir(output_path)
    with open(opts.output_filename, 'wb') as combined_fileobj:
        combined_fileobj.write(combined_file)

    print("Size of original BIOS ROM is: 0x{:08X}".format(len(bios_file)))
    print("BIOS region starts from: 0x{:08X}".format(bios_begin))
    print("BIOS length is: 0x{:08X}".format(bios_end))
    print("IFWI and BIOS stitched successfully")

def get_args(version):
    parser = argparse.ArgumentParser(description='IfwiPatcher {} (C) 2017 Intel(R) Corporation.'.format(version))
    parser.add_argument(
            '--version',
            action='version',
            version="IfwiPatcher {} (C) 2017 Intel(R) Corporation".format(version))
    parser.add_argument(
            '--ifwi',
            required=True,
            dest='ifwi_filename',
            help='IFWI BIN file relative path to current working directory.')
    parser.add_argument(
            '--bios',
            required=True,
            dest='bios_filename',
            help='BIOS ROM file relative path to current working directory.')
    parser.add_argument(
            '--output',
            required=True,
            dest='output_filename',
            help='Stiched file naeme with relative path to current working directory.')

    args = parser.parse_args()
    return args

class InitError(Exception):
    def __init__(self, error_description):
        self.error_string = error_description
        return

if __name__ == "__main__":
    try:
        main()
    except InitError as e:
        print("IfwiPatcher execute error.")
