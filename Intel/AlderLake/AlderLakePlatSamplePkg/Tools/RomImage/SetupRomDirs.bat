@REM @file
@REM
@REM @copyright
@REM  INTEL CONFIDENTIAL
@REM  Copyright 2016 - 2021 Intel Corporation.
@REM
@REM  The source code contained or described herein and all documents related to the
@REM  source code ("Material") are owned by Intel Corporation or its suppliers or
@REM  licensors. Title to the Material remains with Intel Corporation or its suppliers
@REM  and licensors. The Material may contain trade secrets and proprietary and
@REM  confidential information of Intel Corporation and its suppliers and licensors,
@REM  and is protected by worldwide copyright and trade secret laws and treaty
@REM  provisions. No part of the Material may be used, copied, reproduced, modified,
@REM  published, uploaded, posted, transmitted, distributed, or disclosed in any way
@REM  without Intel's prior express written permission.
@REM
@REM  No license under any patent, copyright, trade secret or other intellectual
@REM  property right is granted to or conferred upon you by disclosure or delivery
@REM  of the Materials, either expressly, by implication, inducement, estoppel or
@REM  otherwise. Any license under such intellectual property rights must be
@REM  express and approved by Intel in writing.
@REM
@REM  Unless otherwise agreed by Intel in writing, you may not remove or alter
@REM  this notice or any other notice embedded in Materials by Intel or
@REM  Intel's suppliers or licensors in any way.
@REM
@REM  This file contains a 'Sample Driver' and is licensed as such under the terms
@REM  of your license agreement with Intel or your vendor. This file may be modified
@REM  by the user, subject to the additional terms of the license agreement.
@REM
@REM @par Specification Reference:
@REM

@echo off
setlocal

@if not defined PYTHON_COMMAND (
  set PYTHON_COMMAND=py -3
)
@rem
@rem The following line must be modified for each new project
@rem
if not exist %BUILD_DIR%\FV\ClientBios.fd goto ErrorStop

@if not exist %WORKSPACE%\RomImages @mkdir %WORKSPACE%\RomImages\

@rem
@rem Build BIOS with Production signed ACM
@rem
%WORKSPACE_PLATFORM%\%PLATFORM_FULL_PACKAGE%\InternalOnly\Tools\BootGuard\anc_bios_gen.exe^
  -b %BUILD_DIR%\FV\ClientBios.fd^
  -a %WORKSPACE_PLATFORM%\%PLATFORM_BOARD_PACKAGE%\Binaries/BootGuard/ACM/StartupAcmProd.bin^
  -o %BUILD_DIR%\FV\Prod_ClientBios.fd

@if %RESILIENCY_BUILD% EQU TRUE (
  call split -f %BUILD_DIR%\FV\ClientBios.fd -s 0xC00000 -t %BUILD_DIR%\FV\IBB.bin -o %BUILD_DIR%\FV\Remaining1.bin
  call split -f %BUILD_DIR%\FV\Remaining1.bin -s 0xC00000 -t %BUILD_DIR%\FV\Dummy.bin -o %BUILD_DIR%\FV\IBBR_OBB.bin
  call split -f %BUILD_DIR%\FV\IBBR_OBB.bin -s 0x800000 -t %BUILD_DIR%\FV\IBBR.bin -o %BUILD_DIR%\FV\REMAINING.bin
  copy /y /b %BUILD_DIR%\FV\REMAINING.bin + %BUILD_DIR%\FV\IBB.bin + %BUILD_DIR%\FV\Dummy.bin + %BUILD_DIR%\FV\IBB.bin %BUILD_DIR%\FV\ClientBios.fd
  del %BUILD_DIR%\FV\IBB.bin
  del %BUILD_DIR%\FV\IBBR.bin
  del %BUILD_DIR%\FV\IBBR_OBB.bin
  del %BUILD_DIR%\FV\REMAINING.bin
  call split -f %BUILD_DIR%\FV\Prod_ClientBios.fd -s 0xC00000 -t %BUILD_DIR%\FV\IBB.bin -o %BUILD_DIR%\FV\Remaining1.bin
  call split -f %BUILD_DIR%\FV\Remaining1.bin -s 0xC00000 -t %BUILD_DIR%\FV\Dummy.bin -o %BUILD_DIR%\FV\IBBR_OBB.bin
  call split -f %BUILD_DIR%\FV\IBBR_OBB.bin -s 0x800000 -t %BUILD_DIR%\FV\IBBR.bin -o %BUILD_DIR%\FV\REMAINING.bin
  copy /y /b %BUILD_DIR%\FV\REMAINING.bin + %BUILD_DIR%\FV\IBB.bin + %BUILD_DIR%\FV\Dummy.bin + %BUILD_DIR%\FV\IBB.bin %BUILD_DIR%\FV\Prod_ClientBios.fd
  del %BUILD_DIR%\FV\IBB.bin
  del %BUILD_DIR%\FV\IBBR.bin
  del %BUILD_DIR%\FV\IBBR_OBB.bin
  del %BUILD_DIR%\FV\REMAINING.bin
)

@set FSP_WRAPPER_TYPE=FSPWRAPPER

@rem
@rem Copy BIOS ROM files to destination folder with proper naming basing on build type.
@rem
@if %EMBEDDED_BUILD% EQU TRUE (
  @set ROM_NAME=%BIOS_PREFIX%_%BUILD_TYPE%_FSPWRAPPER_%BIOS_MAJOR_VERSION%_%BIOS_MIN_VERSION%.rom
) else (
  if %EXTENDEDREGION_BUILD% NEQ TRUE (
    @set ROM_NAME=%BIOS_PREFIX%_%FSP_WRAPPER_TYPE%_%BIOS_MAJOR_VERSION%_%BIOS_MIN_VERSION%_%BUILD_TYPE%.rom
  ) else (
    @set ROM_NAME=%BIOS_PREFIX%_%FSP_WRAPPER_TYPE%_%BIOS_MAJOR_VERSION%_%BIOS_MIN_VERSION%_%BUILD_TYPE%_EXTEND.rom
  )
)
@copy /Y /b %BUILD_DIR%\FV\ClientBios.fd %WORKSPACE%\RomImages\%ROM_NAME% > NUL 2>&1
@copy /Y /b %BUILD_DIR%\FV\Prod_ClientBios.fd %WORKSPACE%\RomImages\Prod_%ROM_NAME% > NUL 2>&1

@rem
@rem Create Simics image
@rem
@set SIM_DIR_EM=
@if /I "%EMBEDDED_BUILD%" EQU "TRUE" (
  @set SIM_DIR_EM=Embedded
  @set SIM_ROM_NAME=LP_%BIOS_PREFIX%%BIOS_MAJOR_VERSION%_%BIOS_MIN_VERSION%_SPI_Simics.bin
) else (
  @set SIM_ROM_NAME=%BIOS_PREFIX%%IFWI_VERSION%_%FSP_WRAPPER_TYPE%_%BIOS_MAJOR_VERSION%_%BIOS_MIN_VERSION%_%BUILD_TYPE%_Simics.bin
)
@set SIM_ROM_DIR=%TARGET_PLATFORM%%SIM_DIR_EM%Simics
@echo Simics image: %WORKSPACE%\RomImages\%SIM_ROM_DIR%\%SIM_ROM_NAME%

@if %RESILIENCY_BUILD% NEQ TRUE @if %EXTENDEDREGION_BUILD% NEQ TRUE (
  @if exist %WORKSPACE_PLATFORM%\%PLATFORM_FULL_PACKAGE%\Tools\RomImage\IfwiPatcher\IfwiPatcher.py (
    @call %PYTHON_COMMAND% %WORKSPACE_PLATFORM%\%PLATFORM_FULL_PACKAGE%\Tools\RomImage\IfwiPatcher\IfwiPatcher.py --ifwi %IFWI_DIR%\%IFWI_PREFIX%_IFWI.bin --bios %BUILD_DIR%\FV\ClientBios.fd  --output %WORKSPACE%\RomImages\%SIM_ROM_DIR%\%SIM_ROM_NAME%
  )
)
echo =========================================================================
echo Simics image generated successfully !
echo =========================================================================


endlocal
goto :EOF

:ErrorStop
endlocal
exit /b 1
