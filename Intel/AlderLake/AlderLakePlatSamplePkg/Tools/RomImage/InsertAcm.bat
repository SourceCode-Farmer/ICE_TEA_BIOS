@REM @file
@REM
@REM @copyright
@REM  INTEL CONFIDENTIAL
@REM  Copyright 2014 - 2016 Intel Corporation.
@REM
@REM  The source code contained or described herein and all documents related to the
@REM  source code ("Material") are owned by Intel Corporation or its suppliers or
@REM  licensors. Title to the Material remains with Intel Corporation or its suppliers
@REM  and licensors. The Material may contain trade secrets and proprietary and
@REM  confidential information of Intel Corporation and its suppliers and licensors,
@REM  and is protected by worldwide copyright and trade secret laws and treaty
@REM  provisions. No part of the Material may be used, copied, reproduced, modified,
@REM  published, uploaded, posted, transmitted, distributed, or disclosed in any way
@REM  without Intel's prior express written permission.
@REM
@REM  No license under any patent, copyright, trade secret or other intellectual
@REM  property right is granted to or conferred upon you by disclosure or delivery
@REM  of the Materials, either expressly, by implication, inducement, estoppel or
@REM  otherwise. Any license under such intellectual property rights must be
@REM  express and approved by Intel in writing.
@REM
@REM  Unless otherwise agreed by Intel in writing, you may not remove or alter
@REM  this notice or any other notice embedded in Materials by Intel or
@REM  Intel's suppliers or licensors in any way.
@REM
@REM  This file contains a 'Sample Driver' and is licensed as such under the terms
@REM  of your license agreement with Intel or your vendor. This file may be modified
@REM  by the user, subject to the additional terms of the license agreement.
@REM
@REM @par Specification Reference:
@REM

@echo on

@rem **************************************************************************
@rem    InsertAcm.bat
@rem      Written by Jeff Hoag
@rem
@rem    This batch file takes all the ROM images in the given output directory,
@rem    inserts the specified ACM module, and places the resulting ROM files
@rem  into a subdirectory under the initially specified ROM directory.
@rem
@rem    Parameters:
@rem    %1 Root directory for where the AnchorCove binaries are located
@rem      %2 Subdirectory for where the AnchorCove binary we want to use is
@rem      located.  Also will be used for the new output directory.
@rem    %3 Name of the AnchorCove binary to swap in
@rem      %4 Directory where the ROM images to modify are located
@rem **************************************************************************


if "%1" == "" (
  echo AnchorCove binary root directory is not set
  goto :BadParametersStop
)

if "%2" == "" (
  echo AnchorCove subdirectory not specified
  goto :BadParametersStop
)

if "%3" == "" (
  echo Module not specified
  goto :BadParametersStop
)

if "%4" == "" (
  echo Rom image directory is not set.
  goto :BadParametersStop
)

setlocal
set ACM_ROOT=%1
set ACM_DIR=%2
set ACM_NAME=%3
set ROM_DIR=%4

set ACM_FULL_DIR=%ACM_ROOT%\%ACM_DIR%
set ACM_FULL_PATH=%ACM_ROOT%\%ACM_DIR%\%ACM_NAME%
set ACM_INSERTION_TOOL=%ACM_FULL_DIR%\anc_bios_gen.exe
set ROM_DEST_DIR=%ROM_DIR%\%ACM_DIR%

if not exist %ACM_FULL_PATH% (
  echo ERROR! AnchorCove module does not exist: %ACM_FULL_PATH%
  goto :BadParametersStop
)

if NOT EXIST %ACM_INSERTION_TOOL%  (
  echo ERROR! ACM insertion tool not found: %ACM_INSERTION_TOOL%
  goto :BadParametersStop
)

if NOT EXIST %ROM_DIR% (
  echo ERROR! Specified ROM dir not found: %ROM_DIR%
  goto :BadParametersStop
)

if exist %ROM_DEST_DIR% ( del /Q %ROM_DEST_DIR%\*.* )
if not exist %ROM_DEST_DIR% ( md %ROM_DEST_DIR% )

for %%i in (%ROM_DIR%\*.bin) do (
  echo processing file:  %%i
  %ACM_INSERTION_TOOL% -b %%i -o %ROM_DEST_DIR%\%%~ni.bin -a %ACM_FULL_PATH%
)

goto :EOF

@rem
@rem Bad Parameter handling
@rem
:BadParametersStop
@echo ==see the header of this file for syntax information==

:Error
:EOF
endlocal

exit /b 1
