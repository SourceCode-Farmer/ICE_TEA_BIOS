################################################################################
## @file
## SPI Images Build Script cx_freeze setup script
##
## @copyright
## Copyright (c) 2016 Intel Corporation. All rights reserved
## This software and associated documentation (if any) is furnished
## under a license and may only be used or copied in accordance
## with the terms of the license. Except as permitted by the
## license, no part of this software or documentation may be
## reproduced, stored in a retrieval system, or transmitted in any
## form or by any means without the express written consent of
## Intel Corporation.
## This file contains 'Framework Code' and is licensed as such
## under the terms of your license agreement with Intel or your
## vendor. This file may not be modified, except as allowed by
## additional terms of your license agreement.
################################################################################

from cx_Freeze import setup, Executable

build_exe_options = {
    "packages" : [ "os", "sys", "subprocess", "json", "fnmatch", "traceback" ],
    "excludes" : [ "BuildRomImages", "Tkinter" ]
    }

base = "Console"

setup ( name = "BuildRomImages",
        version = "1.0.0",
        description = "SPI Images Build Script",
        options = { "build_exe": build_exe_options },
        executables = [Executable("BuildRomImagesExeWrapper.py", base=base, targetName="BuildRomImages.exe")]
        )
