################################################################################
## @file
## SPI Images Build Script EXE Wrapper
##
## @copyright
## Copyright (c) 2016 - 2019 Intel Corporation. All rights reserved
## This software and associated documentation (if any) is furnished
## under a license and may only be used or copied in accordance
## with the terms of the license. Except as permitted by the
## license, no part of this software or documentation may be
## reproduced, stored in a retrieval system, or transmitted in any
## form or by any means without the express written consent of
## Intel Corporation.
## This file contains 'Framework Code' and is licensed as such
## under the terms of your license agreement with Intel or your
## vendor. This file may not be modified, except as allowed by
## additional terms of your license agreement.
################################################################################

import os
import sys
import subprocess
from subprocess import call, check_call
import json
from xml.etree import ElementTree
import fnmatch
import traceback

import BuildRomImages

def main():
    if len(sys.argv) <= 1:
        BuildRomImages.PrintUsage("BuildRomImages.exe")
        sys.exit(1)
    try:
        BuildRomImages.main()
    except Exception, e:
        print ("Error: %s"%str(e))
        print
        print
        print ("Detailed Error Data:")
        print ("-------------------------------------------------------------------------------")
        traceback.print_exc()
        print ("-------------------------------------------------------------------------------")
        sys.exit(1)

if __name__ == "__main__":
    main()
