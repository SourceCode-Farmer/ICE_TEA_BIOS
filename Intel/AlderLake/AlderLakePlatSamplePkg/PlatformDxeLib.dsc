## @file
# platform build option configuration file.
#
# @copyright
#  INTEL CONFIDENTIAL
#  Copyright 2020 - 2021 Intel Corporation.
#
#  The source code contained or described herein and all documents related to the
#  source code ("Material") are owned by Intel Corporation or its suppliers or
#  licensors. Title to the Material remains with Intel Corporation or its suppliers
#  and licensors. The Material may contain trade secrets and proprietary and
#  confidential information of Intel Corporation and its suppliers and licensors,
#  and is protected by worldwide copyright and trade secret laws and treaty
#  provisions. No part of the Material may be used, copied, reproduced, modified,
#  published, uploaded, posted, transmitted, distributed, or disclosed in any way
#  without Intel's prior express written permission.
#
#  No license under any patent, copyright, trade secret or other intellectual
#  property right is granted to or conferred upon you by disclosure or delivery
#  of the Materials, either expressly, by implication, inducement, estoppel or
#  otherwise. Any license under such intellectual property rights must be
#  express and approved by Intel in writing.
#
#  Unless otherwise agreed by Intel in writing, you may not remove or alter
#  this notice or any other notice embedded in Materials by Intel or
#  Intel's suppliers or licensors in any way.
#
#  This file contains a 'Sample Driver' and is licensed as such under the terms
#  of your license agreement with Intel or your vendor. This file may be modified
#  by the user, subject to the additional terms of the license agreement.
#
# @par Specification
##

[LibraryClasses.X64]

  SerialPortParameterLib|$(PLATFORM_FULL_PACKAGE)/Library/DxeSmmSerialPortParameterLib/DxeSmmSerialPortParameterLib.inf

!if gPlatformModuleTokenSpaceGuid.PcdSerialPortEnable == TRUE
  SerialPortLib|$(PLATFORM_FULL_PACKAGE)/Library/BaseSerialPortLib/BaseSerialPortLib.inf
!else
  SerialPortLib|MdePkg/Library/BaseSerialPortLibNull/BaseSerialPortLibNull.inf
!endif

!if $(TARGET) == DEBUG
  DebugLib|$(PLATFORM_FULL_PACKAGE)/Library/BaseDebugLibAllDebugPort/BaseDebugLibAllDebugPort.inf
!endif

  DxeAcpiGnvsInitLib|$(PLATFORM_FULL_PACKAGE)/Library/DxeAcpiGnvsInitLib/DxeAcpiGnvsInitLib.inf

  DxePolicyBoardConfigLib|$(PLATFORM_FULL_PACKAGE)/Library/DxePolicyBoardConfigLib/DxePolicyBoardConfigLib.inf
  DxePolicyUpdateLib|$(PLATFORM_FULL_PACKAGE)/Library/DxePolicyUpdateLib/DxePolicyUpdateLib.inf


#
# DXE Boot State Library Instance
#
  DxeBootStateLib|$(PLATFORM_FULL_PACKAGE)/Library/DxeBootStateLib/DxeBootStateLib.inf

  DxeOpromPatchLib|$(PLATFORM_FULL_PACKAGE)/Library/DxeOpromPatchLib/DxeOpromPatchLib.inf

  SpiFlashCommonLib|$(PLATFORM_FULL_PACKAGE)/Library/DxeSpiFlashCommonLib/DxeSpiFlashCommonLib.inf

  #
  # Platform Telemetry
  #
  PlatformTelemetryLib|$(PLATFORM_FULL_PACKAGE)/Library/PlatformTelemetryLib/PlatformTelemetryLib.inf

  DxeFirmwareVersionInfoLib|$(PLATFORM_FULL_PACKAGE)/Library/DxeFirmwareVersionInfoLib/DxeFirmwareVersionInfoLib.inf

  FspWrapperPlatformLib|$(PLATFORM_FULL_PACKAGE)/FspWrapper/Library/DxeFspWrapperPlatformLib/DxeFspWrapperPlatformLib.inf

  BootLogoLib|MdeModulePkg/Library/BootLogoLib/BootLogoLib.inf
!if gPlatformModuleTokenSpaceGuid.PcdDTbtEnable == TRUE
  DxeDTbtPolicyLib|$(PLATFORM_FEATURES_PATH)/Tbt/Library/DxeDTbtPolicyLib/DxeDTbtPolicyLib.inf
!else
  DxeDTbtPolicyLib|$(PLATFORM_FEATURES_PATH)/Tbt/Library/DxeDTbtPolicyLibNull/DxeDTbtPolicyLibNull.inf
!endif

[LibraryClasses.X64.PEIM]
  SerialPortParameterLib|$(PLATFORM_FULL_PACKAGE)/Library/PeiSerialPortParameterLib/PeiSerialPortParameterLib.inf

[LibraryClasses.X64.DXE_CORE]
  SerialPortParameterLib|$(PLATFORM_FULL_PACKAGE)/Library/DxeCoreSerialPortParameterLib/DxeCoreSerialPortParameterLib.inf
!if $(TARGET) == DEBUG
  SerialIoUartDebugPropertyLib|$(PLATFORM_FULL_PACKAGE)/Library/SerialIoUartDebugPropertyLib/DxeCoreSerialIoUartDebugPropertyLib.inf
  DebugLib|$(PLATFORM_FULL_PACKAGE)/Library/BaseDebugLibAllDebugPort/BaseDebugLibAllDebugPort.inf
!endif

[LibraryClasses.X64.DXE_SMM_DRIVER]
!if gSiPkgTokenSpaceGuid.PcdBiosGuardEnable == TRUE
  SpiFlashCommonLib|$(PLATFORM_FULL_PACKAGE)/Library/SmmSpiFlashCommonLib/SmmSpiFlashCommonLibBiosGuard.inf
!else
  SpiFlashCommonLib|$(PLATFORM_FULL_PACKAGE)/Library/SmmSpiFlashCommonLib/SmmSpiFlashCommonLib.inf
!endif

  DxeSmmAcpiCustomLib|$(PLATFORM_FULL_PACKAGE)/Library/DxeSmmAcpiCustomLib/DxeSmmAcpiCustomLib.inf
  DxeSmmScriptLib|$(PLATFORM_FULL_PACKAGE)/Library/DxeSmmScriptLib/DxeSmmScriptLib.inf

!if $(TARGET) == DEBUG
  SerialIoUartDebugPropertyLib|$(PLATFORM_FULL_PACKAGE)/Library/SerialIoUartDebugPropertyLib/DxeSmmSerialIoUartDebugPropertyLib.inf
  DebugLib|$(PLATFORM_FULL_PACKAGE)/Library/BaseDebugLibAllDebugPort/BaseDebugLibAllDebugPort.inf
!endif

[LibraryClasses.X64.SMM_CORE]
!if $(TARGET) == DEBUG
  DebugLib|$(PLATFORM_FULL_PACKAGE)/Library/BaseDebugLibAllDebugPort/BaseDebugLibAllDebugPort.inf
  SerialIoUartDebugPropertyLib|$(PLATFORM_FULL_PACKAGE)/Library/SerialIoUartDebugPropertyLib/DxeSmmSerialIoUartDebugPropertyLib.inf
!endif

[LibraryClasses.X64.DXE_RUNTIME_DRIVER]

  DebugPrintErrorLevelLib|$(PLATFORM_FULL_PACKAGE)/Library/PeiDxeSmmDebugPrintErrorLevelLib/PeiDxeSmmDebugPrintErrorLevelLib.inf
  SerialPortParameterLib|$(PLATFORM_FULL_PACKAGE)/Library/DxeSmmSerialPortParameterLib/DxeSmmSerialPortParameterLib.inf

!if $(TARGET) == DEBUG
  SerialIoUartDebugPropertyLib|$(PLATFORM_FULL_PACKAGE)/Library/SerialIoUartDebugPropertyLib/DxeSmmSerialIoUartDebugPropertyLib.inf
!endif

[LibraryClasses.X64.UEFI_DRIVER]

[LibraryClasses.X64.UEFI_APPLICATION]


