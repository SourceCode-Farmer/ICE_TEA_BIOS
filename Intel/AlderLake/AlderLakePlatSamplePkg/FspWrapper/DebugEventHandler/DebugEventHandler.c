/** @file

 @copyright
  INTEL CONFIDENTIAL
  Copyright 2020 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains 'Framework Code' and is licensed as such under the terms
  of your license agreement with Intel or your vendor. This file may not be
  modified, except as allowed by additional terms of your license agreement.

@par Specification Reference:
**/

#include <PiPei.h>
#include <Ppi/DebugEventHandler.h>
#include <Library/PeiServicesTablePointerLib.h>
#include <Library/PeiServicesLib.h>
#include <Library/DebugLib.h>

EFI_STATUS
EFIAPI
DebugEventHandler (
  IN EFI_STATUS_CODE_TYPE           CodeType,
  IN EFI_STATUS_CODE_VALUE          Value,
  IN UINT32                         Instance,
  IN EFI_GUID                       *CallerId,
  IN EFI_STATUS_CODE_DATA           *Data OPTIONAL
  );

DEBUG_EVENT_HABDLER_PPI mFspWrapperDebugEventHandlerPpi = {
  DebugEventHandler
};

EFI_PEI_PPI_DESCRIPTOR   mFspWrapperDebugEventHandlerPpiList = {
  EFI_PEI_PPI_DESCRIPTOR_PPI | EFI_PEI_PPI_DESCRIPTOR_TERMINATE_LIST,
  &gDebugEventHandlerPpiGuid,
  (VOID *) &mFspWrapperDebugEventHandlerPpi
};

EFI_STATUS
EFIAPI
DebugEventHandler (
  IN EFI_STATUS_CODE_TYPE           CodeType,
  IN EFI_STATUS_CODE_VALUE          Value,
  IN UINT32                         Instance,
  IN EFI_GUID                       *CallerId,
  IN EFI_STATUS_CODE_DATA           *Data OPTIONAL
  )
{
  CONST EFI_PEI_SERVICES     **PeiServices;
  EFI_STATUS                 Status;

  PeiServices = GetPeiServicesTablePointer ();
  Status = (*PeiServices)->ReportStatusCode (
                                 PeiServices,
                                 CodeType,
                                 Value,
                                 (UINT32) Instance,
                                 (EFI_GUID *) CallerId,
                                 (EFI_STATUS_CODE_DATA *) Data
                                 );
  return Status;
}

/**
  The Entry point of the Debug Event Handle PEIM

  @param[in]  FileHandle   Pointer to image file handle
  @param[in]  PeiServices  Pointer to PEI Services Table

  @return     EFI_SUCCESS
**/
EFI_STATUS
EFIAPI
DebugEventHandlerPeiEntry (
  IN       EFI_PEI_FILE_HANDLE  FileHandle,
  IN CONST EFI_PEI_SERVICES     **PeiServices
  )
{
  EFI_STATUS                 Status;

  Status = PeiServicesInstallPpi (&mFspWrapperDebugEventHandlerPpiList);
  ASSERT_EFI_ERROR(Status);
  return EFI_SUCCESS;
}
