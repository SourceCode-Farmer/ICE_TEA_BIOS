### @file
#
#******************************************************************************
#* Copyright (c) 2014 - 2019, Insyde Software Corporation. All Rights Reserved.
#*
#* You may not reproduce, distribute, publish, display, perform, modify, adapt,
#* transmit, broadcast, present, recite, release, license or otherwise exploit
#* any part of this publication in any form, by any means, without the prior
#* written permission of Insyde Software Corporation.
#*
#******************************************************************************
#
## @file
#  Provide FSP wrapper platform sec related function.
#
#@copyright
#  INTEL CONFIDENTIAL
#  Copyright 2014 - 2021 Intel Corporation.
#
#  The source code contained or described herein and all documents related to the
#  source code ("Material") are owned by Intel Corporation or its suppliers or
#  licensors. Title to the Material remains with Intel Corporation or its suppliers
#  and licensors. The Material may contain trade secrets and proprietary and
#  confidential information of Intel Corporation and its suppliers and licensors,
#  and is protected by worldwide copyright and trade secret laws and treaty
#  provisions. No part of the Material may be used, copied, reproduced, modified,
#  published, uploaded, posted, transmitted, distributed, or disclosed in any way
#  without Intel's prior express written permission.
#
#  No license under any patent, copyright, trade secret or other intellectual
#  property right is granted to or conferred upon you by disclosure or delivery
#  of the Materials, either expressly, by implication, inducement, estoppel or
#  otherwise. Any license under such intellectual property rights must be
#  express and approved by Intel in writing.
#
#  Unless otherwise agreed by Intel in writing, you may not remove or alter
#  this notice or any other notice embedded in Materials by Intel or
#  Intel's suppliers or licensors in any way.
#
#  This file contains a 'Sample Driver' and is licensed as such under the terms
#  of your license agreement with Intel or your vendor. This file may be modified
#  by the user, subject to the additional terms of the license agreement.
#
#@par Specification
##
#
# Defines Section - statements that will be processed to create a Makefile.
#
################################################################################
[Defines]
  INF_VERSION                    = 0x00010005
  BASE_NAME                      = SecFspWrapperPlatformSecLib
  FILE_GUID                      = 4E1C4F95-90EA-47de-9ACC-B8920189A1F5
  MODULE_TYPE                    = SEC
  VERSION_STRING                 = 1.0
  LIBRARY_CLASS                  = PlatformSecLib


#
# The following information is for reference only and not required by the build tools.
#
#  VALID_ARCHITECTURES           = IA32 X64
#

################################################################################
#
# Sources Section - list of files that are required for the build to succeed.
#
################################################################################

[Sources]
  FspWrapperPlatformSecLib.c
  SecRamInitData.c
  SecPlatformInformation.c
  SecGetPerformance.c
  SecTempRamDone.c
  PlatformInit.c

[Sources.IA32]
  Ia32/Fsp.h
  Ia32/SecEntry.nasm
  Ia32/PeiCoreEntry.nasm
  Ia32/Stack.nasm

################################################################################
#
# Package Dependency Section - list of Package files that are required for
#                              this module.
#
################################################################################

[Packages]
  MdePkg/MdePkg.dec
  MdeModulePkg/MdeModulePkg.dec
  UefiCpuPkg/UefiCpuPkg.dec
  IntelFsp2Pkg/IntelFsp2Pkg.dec
  IntelFsp2WrapperPkg/IntelFsp2WrapperPkg.dec
  AlderLakePlatSamplePkg/PlatformPkg.dec
  MinPlatformPkg/MinPlatformPkg.dec
  ClientOneSiliconPkg/SiPkg.dec
  #[-start-200917-IB06462159-modify]#
  $(PLATFORM_FSP_BIN_PACKAGE)/AlderLakeFspBinPkg.dec
  #[-end-200917-IB06462159-modify]#
  #[-start-191111-IB10189001-add]#
  InsydeModulePkg/InsydeModulePkg.dec
  #[-end-191111-IB10189001-add]#
[LibraryClasses]
  LocalApicLib
  SerialPortLib
  PlatformInitLib
  FspWrapperPlatformLib
  FspWrapperApiLib
  PeiServicesTablePointerLib
  SecGetFsptApiParameterLib
  #[-start-191111-IB10189001-add]#
  MtrrLib
  #[-end-191111-IB10189001-add]#

[Ppis]
  gEfiSecPlatformInformationPpiGuid       ## CONSUMES
  gPeiSecPerformancePpiGuid               ## CONSUMES
  gTopOfTemporaryRamPpiGuid               ## PRODUCES
  gEfiPeiCoreFvLocationPpiGuid            ## PRODUCES
  gFspTempRamExitPpiGuid                  ## CONSUMES
  gFsptUpdLocationPpiGuid                 ## CONSUMES

[Pcd]
  gUefiCpuPkgTokenSpaceGuid.PcdPeiTemporaryRamStackSize               ## CONSUMES
  gIntelFsp2WrapperTokenSpaceGuid.PcdFsptBaseAddress                  ## CONSUMES
  gIntelFsp2PkgTokenSpaceGuid.PcdFspTemporaryRamSize                  ## CONSUMES
  gSiPkgTokenSpaceGuid.PcdTcoBaseAddress                              ## CONSUMES

[FixedPcd]
  gMinPlatformPkgTokenSpaceGuid.PcdFlashFvMicrocodeBase               ## CONSUMES
  gMinPlatformPkgTokenSpaceGuid.PcdFlashFvMicrocodeSize               ## CONSUMES
  gMinPlatformPkgTokenSpaceGuid.PcdMicrocodeOffsetInFv                ## CONSUMES
  gIntelFsp2WrapperTokenSpaceGuid.PcdFlashCodeCacheAddress            ## CONSUMES
  gIntelFsp2WrapperTokenSpaceGuid.PcdFlashCodeCacheSize               ## CONSUMES
  gPlatformModuleTokenSpaceGuid.PcdFaultToleranceOffset               ## CONSUMES
  gPlatformModuleTokenSpaceGuid.PcdFtCpuMicrocodePatchAddress         ## CONSUMES
  gIntelFsp2WrapperTokenSpaceGuid.PcdFspModeSelection                 ## CONSUMES
  gSiPkgTokenSpaceGuid.PcdSerialIoUartDebugEnable                     ## CONSUMES
  gSiPkgTokenSpaceGuid.PcdSerialIoUartNumber                          ## CONSUMES
  gSiPkgTokenSpaceGuid.PcdSerialIoUartMode                            ## CONSUMES
  gSiPkgTokenSpaceGuid.PcdSerialIoUartBaudRate                        ## CONSUMES
  gEfiMdePkgTokenSpaceGuid.PcdPciExpressBaseAddress                   ## PRODUCES
  gSiPkgTokenSpaceGuid.PcdTemporaryPciExpressRegionLength             ## PRODUCES
  gSiPkgTokenSpaceGuid.PcdSerialIoUartParity                          ## CONSUMES
  gSiPkgTokenSpaceGuid.PcdSerialIoUartDataBits                        ## CONSUMES
  gSiPkgTokenSpaceGuid.PcdSerialIoUartStopBits                        ## CONSUMES
  gSiPkgTokenSpaceGuid.PcdSerialIoUartAutoFlow                        ## CONSUMES
  gSiPkgTokenSpaceGuid.PcdSerialIoUartRxPinMux                        ## CONSUMES
  gSiPkgTokenSpaceGuid.PcdSerialIoUartTxPinMux                        ## CONSUMES
  gSiPkgTokenSpaceGuid.PcdSerialIoUartRtsPinMux                       ## CONSUMES
  gSiPkgTokenSpaceGuid.PcdSerialIoUartCtsPinMux                       ## CONSUMES
  gSiPkgTokenSpaceGuid.PcdSerialIoUartDebugMmioBase                   ## CONSUMES
  gSiPkgTokenSpaceGuid.PcdLpcUartDebugEnable                          ## CONSUMES
  gSiPkgTokenSpaceGuid.PcdDebugInterfaceFlags                         ## CONSUMES
  gSiPkgTokenSpaceGuid.PcdSerialDebugLevel                            ## CONSUMES
  gSiPkgTokenSpaceGuid.PcdIsaSerialUartBase                           ## CONSUMES
  gSiPkgTokenSpaceGuid.PcdSerialIo2ndUartEnable                       ## CONSUMES
  gSiPkgTokenSpaceGuid.PcdSerialIo2ndUartNumber                       ## CONSUMES
  gSiPkgTokenSpaceGuid.PcdSerialIo2ndUartMode                         ## CONSUMES
  gSiPkgTokenSpaceGuid.PcdSerialIo2ndUartBaudRate                     ## CONSUMES
  gSiPkgTokenSpaceGuid.PcdSerialIo2ndUartParity                       ## CONSUMES
  gSiPkgTokenSpaceGuid.PcdSerialIo2ndUartDataBits                     ## CONSUMES
  gSiPkgTokenSpaceGuid.PcdSerialIo2ndUartStopBits                     ## CONSUMES
  gSiPkgTokenSpaceGuid.PcdSerialIo2ndUartAutoFlow                     ## CONSUMES
  gSiPkgTokenSpaceGuid.PcdSerialIo2ndUartRxPinMux                     ## CONSUMES
  gSiPkgTokenSpaceGuid.PcdSerialIo2ndUartTxPinMux                     ## CONSUMES
  gSiPkgTokenSpaceGuid.PcdSerialIo2ndUartRtsPinMux                    ## CONSUMES
  gSiPkgTokenSpaceGuid.PcdSerialIo2ndUartCtsPinMux                    ## CONSUMES
  gSiPkgTokenSpaceGuid.PcdSerialIo2ndUartMmioBase                     ## CONSUMES
  gSiPkgTokenSpaceGuid.PcdTopMemoryCacheSize                          ## CONSUMES
  gSiPkgTokenSpaceGuid.PcdBiosAreaBaseAddress                         ## CONSUMES
  gMinPlatformPkgTokenSpaceGuid.PcdFlashFvFspMOffset                  ## CONSUMES
  gMinPlatformPkgTokenSpaceGuid.PcdFspDispatchModeUseFspPeiMain       ## CONSUMES
  #[-start-191111-IB10189001-add]#
  gInsydeTokenSpaceGuid.PcdFlashAreaSize
  #[-end-191111-IB10189001-add]#