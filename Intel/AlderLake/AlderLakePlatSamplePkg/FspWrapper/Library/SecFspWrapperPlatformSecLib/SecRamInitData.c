/** @file
  Provide TempRamInitParams data.

@copyright
  INTEL CONFIDENTIAL
  Copyright 2014 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains a 'Sample Driver' and is licensed as such under the terms
  of your license agreement with Intel or your vendor. This file may be modified
  by the user, subject to the additional terms of the license agreement.

@par Specification
**/

#include <Library/PcdLib.h>
#include <FsptUpd.h>
#include <FspUpd.h>

#pragma pack(1)

VOID
BootloaderDebugHandler (
  IN UINT8     *Buffer,
  IN UINTN     NumberOfBytes
  );

GLOBAL_REMOVE_IF_UNREFERENCED CONST FSPT_UPD FsptUpdDataPtr = {
  {
    FSPT_UPD_SIGNATURE,
    0x02,
    { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
      0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
      0x00, 0x00, 0x00
    }
  },
  {
    0x01,
    {
      0x00, 0x00, 0x00
    },
    0x00000020,
    0x00000000,
    {
      0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
      0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00
    }
  },
  {
//[-start-191111-IB10189001-modify]//
    ((UINT32)FixedPcdGet64 (PcdFlashFvMicrocodeBase) + FixedPcdGet32 (PcdMicrocodeOffsetInFv)),
    ((UINT32)FixedPcdGet64 (PcdFlashFvMicrocodeSize) - FixedPcdGet32 (PcdMicrocodeOffsetInFv)),
    0,          // Set CodeRegionBase as 0, so that caching will be 4GB-(CodeRegionSize > LLCSize ? LLCSize : CodeRegionSize) will be used.
    FixedPcdGet32 (PcdFlashAreaSize),
//[-end-191111-IB10189001-modify]//
    { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
      0x00, 0x00, 0x00, 0x00, 0x00, 0x00
    }
  },
  {
    FixedPcdGet8 (PcdSerialIoUartDebugEnable),
    FixedPcdGet8 (PcdSerialIoUartNumber),
    FixedPcdGet8 (PcdSerialIoUartMode),
    0,
    FixedPcdGet32 (PcdSerialIoUartBaudRate),
    FixedPcdGet64 (PcdPciExpressBaseAddress),
    FixedPcdGet32 (PcdTemporaryPciExpressRegionLength),
    FixedPcdGet8 (PcdSerialIoUartParity),
    FixedPcdGet8 (PcdSerialIoUartDataBits),
    FixedPcdGet8 (PcdSerialIoUartStopBits),
    FixedPcdGet8 (PcdSerialIoUartAutoFlow),
    FixedPcdGet32 (PcdSerialIoUartRxPinMux),
    FixedPcdGet32 (PcdSerialIoUartTxPinMux),
    FixedPcdGet32 (PcdSerialIoUartRtsPinMux),
    FixedPcdGet32 (PcdSerialIoUartCtsPinMux),
    FixedPcdGet32 (PcdSerialIoUartDebugMmioBase),
    FixedPcdGet8 (PcdLpcUartDebugEnable),
    FixedPcdGet8 (PcdDebugInterfaceFlags),
    FixedPcdGet8 (PcdSerialDebugLevel),
    FixedPcdGet8 (PcdIsaSerialUartBase),
    FixedPcdGet8 (PcdSerialIo2ndUartEnable),
    FixedPcdGet8 (PcdSerialIo2ndUartNumber),
    FixedPcdGet8 (PcdSerialIo2ndUartMode),
    0, //Needed for UnusedUpdSpace alignment
    FixedPcdGet32 (PcdSerialIo2ndUartBaudRate),
    FixedPcdGet8 (PcdSerialIo2ndUartParity),
    FixedPcdGet8 (PcdSerialIo2ndUartDataBits),
    FixedPcdGet8 (PcdSerialIo2ndUartStopBits),
    FixedPcdGet8 (PcdSerialIo2ndUartAutoFlow),
    FixedPcdGet32 (PcdSerialIo2ndUartRxPinMux),
    FixedPcdGet32 (PcdSerialIo2ndUartTxPinMux),
    FixedPcdGet32 (PcdSerialIo2ndUartRtsPinMux),
    FixedPcdGet32 (PcdSerialIo2ndUartCtsPinMux),
    FixedPcdGet32 (PcdSerialIo2ndUartMmioBase),
    FixedPcdGet32 (PcdTopMemoryCacheSize),
    (UINT32) BootloaderDebugHandler,
    {0, 0}, // (UINT8) PcdSerialIoSpiCsPolarity
    {0, 0}, // (UINT8) PcdSerialIoSpiCsEnable
    0, // (UINT8) PcdSerialIoSpiMode
    0, // (UINT8) PcdSerialIoSpiDefaultCsOutput
    0, // (UINT8) PcdSerialIoSpiCsMode
    0, // (UINT8) PcdSerialIoSpiCsState
    0, // (UINT8) PcdSerialIoSpiNumber
    {0, 0, 0}, //Needed for UnusedUpdSpace alignment
    (UINT32) 0x0, // PcdSerialIoSpiMmioBase
    {
      0x00
    }
  },


  { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00
  },
  0x55AA
};

//
// Used in Top Swap enabled case.
//
GLOBAL_REMOVE_IF_UNREFERENCED CONST FSPT_UPD FsptUpdDataPtrFt = {
  {
    FSPT_UPD_SIGNATURE,
    0x02,
    { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
      0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
      0x00, 0x00, 0x00
    }
  },
  {
    0x01,
    {
      0x00, 0x00, 0x00
    },
    0x00000020,
    0x00000000,
    {
      0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
      0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00
    }
  },
  {
    FixedPcdGet32 (PcdFtCpuMicrocodePatchAddress),
    FixedPcdGet64 (PcdFlashFvMicrocodeSize) - FixedPcdGet32 (PcdMicrocodeOffsetInFv),
// @todo The set of PCDs for FlashFV has been changed in MinPlatform, the relevant code need to be redesign.
// @todo Needs to be updated with capsule feature implementation.
    //FixedPcdGet32 (PcdFtFlashFvRecovery2Base), // Set CodeRegionBase started from backup FvRecovery2
    0x00,
    FixedPcdGet32 (PcdFlashCodeCacheSize),
    { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
      0x00, 0x00, 0x00, 0x00, 0x00, 0x00
    }
  },
  {
    FixedPcdGet8 (PcdSerialIoUartDebugEnable),
    FixedPcdGet8 (PcdSerialIoUartNumber),
    FixedPcdGet8 (PcdSerialIoUartMode),
    0,
    FixedPcdGet32 (PcdSerialIoUartBaudRate),
    FixedPcdGet64 (PcdPciExpressBaseAddress),
    FixedPcdGet32 (PcdTemporaryPciExpressRegionLength),
    FixedPcdGet8 (PcdSerialIoUartParity),
    FixedPcdGet8 (PcdSerialIoUartDataBits),
    FixedPcdGet8 (PcdSerialIoUartStopBits),
    FixedPcdGet8 (PcdSerialIoUartAutoFlow),
    FixedPcdGet32 (PcdSerialIoUartRxPinMux),
    FixedPcdGet32 (PcdSerialIoUartTxPinMux),
    FixedPcdGet32 (PcdSerialIoUartRtsPinMux),
    FixedPcdGet32 (PcdSerialIoUartCtsPinMux),
    FixedPcdGet32 (PcdSerialIoUartDebugMmioBase),
    FixedPcdGet8 (PcdLpcUartDebugEnable),
    FixedPcdGet8 (PcdDebugInterfaceFlags),
    FixedPcdGet8 (PcdSerialDebugLevel),
    FixedPcdGet8 (PcdIsaSerialUartBase),
    FixedPcdGet8 (PcdSerialIo2ndUartEnable),
    FixedPcdGet8 (PcdSerialIo2ndUartNumber),
    FixedPcdGet8 (PcdSerialIo2ndUartMode),
    0, //Needed for UnusedUpdSpace alignment
    FixedPcdGet32 (PcdSerialIo2ndUartBaudRate),
    FixedPcdGet8 (PcdSerialIo2ndUartParity),
    FixedPcdGet8 (PcdSerialIo2ndUartDataBits),
    FixedPcdGet8 (PcdSerialIo2ndUartStopBits),
    FixedPcdGet8 (PcdSerialIo2ndUartAutoFlow),
    FixedPcdGet32 (PcdSerialIo2ndUartRxPinMux),
    FixedPcdGet32 (PcdSerialIo2ndUartTxPinMux),
    FixedPcdGet32 (PcdSerialIo2ndUartRtsPinMux),
    FixedPcdGet32 (PcdSerialIo2ndUartCtsPinMux),
    FixedPcdGet32 (PcdSerialIo2ndUartMmioBase),
    FixedPcdGet32 (PcdTopMemoryCacheSize),
    (UINT32) BootloaderDebugHandler,
    {0, 0}, // (UINT8) PcdSerialIoSpiCsPolarity
    {0, 0}, // (UINT8) PcdSerialIoSpiCsEnable
    0, // (UINT8) PcdSerialIoSpiMode
    0, // (UINT8) PcdSerialIoSpiDefaultCsOutput
    0, // (UINT8) PcdSerialIoSpiCsMode
    0, // (UINT8) PcdSerialIoSpiCsState
    0, // (UINT8) PcdSerialIoSpiNumber
    {0, 0, 0}, //Needed for UnusedUpdSpace alignment
    (UINT32) 0x0, // PcdSerialIoSpiMmioBase
    {
      0x00
    }
  },


  { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00
  },
  0x55AA
};
#pragma pack()