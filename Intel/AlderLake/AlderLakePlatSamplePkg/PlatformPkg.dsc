## @file
#  Platform description.
#
# @copyright
#  INTEL CONFIDENTIAL
#  Copyright 2020 - 2021 Intel Corporation.
#
#  The source code contained or described herein and all documents related to the
#  source code ("Material") are owned by Intel Corporation or its suppliers or
#  licensors. Title to the Material remains with Intel Corporation or its suppliers
#  and licensors. The Material may contain trade secrets and proprietary and
#  confidential information of Intel Corporation and its suppliers and licensors,
#  and is protected by worldwide copyright and trade secret laws and treaty
#  provisions. No part of the Material may be used, copied, reproduced, modified,
#  published, uploaded, posted, transmitted, distributed, or disclosed in any way
#  without Intel's prior express written permission.
#
#  No license under any patent, copyright, trade secret or other intellectual
#  property right is granted to or conferred upon you by disclosure or delivery
#  of the Materials, either expressly, by implication, inducement, estoppel or
#  otherwise. Any license under such intellectual property rights must be
#  express and approved by Intel in writing.
#
#  Unless otherwise agreed by Intel in writing, you may not remove or alter
#  this notice or any other notice embedded in Materials by Intel or
#  Intel's suppliers or licensors in any way.
#
#  This file contains a 'Sample Driver' and is licensed as such under the terms
#  of your license agreement with Intel or your vendor. This file may be modified
#  by the user, subject to the additional terms of the license agreement.
#
# @par Specification
##

[Defines]
  #
  # Set platform specific package/folder name, same as passed from PREBUILD script.
  # PLATFORM_FULL_PACKAGE would be the same as PLATFORM_NAME as well as package build folder
  # DEFINE only takes effect at R9 DSC and FDF.
  #
  DEFINE      PLATFORM_PACKAGE                = MinPlatformPkg
  DEFINE      PLATFORM_FULL_PACKAGE           = AlderLakePlatSamplePkg
  DEFINE      PLATFORM_SI_PACKAGE             = ClientOneSiliconPkg
  DEFINE      C1S_PRODUCT_PATH                = ClientOneSiliconPkg/Product/AlderLake
  DEFINE      PLATFORM_FSP_BIN_PACKAGE        = AlderLakeFspBinPkg
  DEFINE      PLATFORM_BOARD_PACKAGE          = AlderLakeBoardPkg
  DEFINE      PLATFORM_OPEN_BOARD_PACKAGE     = AlderLakeOpenBoardPkg
  DEFINE      PLATFORM_FEATURES_PATH          = $(PLATFORM_FULL_PACKAGE)/Features

#
# Define ESRT GUIDs for Firmware Management Protocol instances
#
  DEFINE FMP_CLIENT_PLATFORM_SYSTEM_MONO          = 7FEB1D5D-33F4-48D3-BD11-C4B36B6D0E57 # gFmpDevicePlatformMonolithicGuid
  DEFINE FMP_CLIENT_PLATFORM_SYSTEM_BIOS          = 6C8E136F-D3E6-4131-AC32-4687CB4ABD27 # gFmpDevicePlatformBiosGuid
  DEFINE FMP_CLIENT_PLATFORM_SYSTEM_BTGACM        = 4E88068B-41B2-4E05-893C-DB0B43F7D348 # gFmpDevicePlatformBtGAcmGuid
  DEFINE FMP_CLIENT_PLATFORM_SYSTEM_UCODE         = 69585D92-B50A-4AD7-B265-2EB1AE066574 # gFmpDevicePlatformuCodeGuid
  DEFINE FMP_CLIENT_PLATFORM_SYSTEM_ME            = 0EAB05C1-766A-4805-A039-3081DE0210C7 # gFmpDevicePlatformMeGuid
  DEFINE FMP_CLIENT_PLATFORM_SYSTEM_EC            = 3DD84775-EC79-4ECB-8404-74DE030C3F77 # gFmpDevicePlatformEcGuid
  DEFINE FMP_CLIENT_PLATFORM_SYSTEM_ISH_PDT       = 76CA0AD8-4A14-4389-B7E5-FD88791762AD # gFmpDevicePlatformIshPdtGuid
  DEFINE FMP_CLIENT_PLATFORM_SYSTEM_TSN_MAC_ADDR  = 6FEE88FF-49ED-48F1-B77B-EAD15771ABE7 # gFmpDevicePlatformTsnMacAddrGuid

  #
  # Platform On/Off features are defined here
  #
  !include $(PLATFORM_FULL_PACKAGE)\PlatformPkgConfig.dsc
  !include ClientOneSiliconPkg\Product\AlderLake\AlderLakeSiliconPkg.dsc

[PcdsFixedAtBuild]

################################################################################
#
# Defines Section - statements that will be processed to create a Makefile.
#
################################################################################
[Defines]
  PLATFORM_NAME                       = $(PLATFORM_FULL_PACKAGE)
  PLATFORM_GUID                       = 465B0A0B-7AC1-443b-8F67-7B8DEC145F90
  PLATFORM_VERSION                    = 0.1
  DSC_SPECIFICATION                   = 0x00010005
  OUTPUT_DIRECTORY                    = Build/$(PLATFORM_FULL_PACKAGE)
  SUPPORTED_ARCHITECTURES             = IA32|X64
  BUILD_TARGETS                       = DEBUG|RELEASE
  SKUID_IDENTIFIER                    = DEFAULT

################################################################################
#
# Library Class section - list of all Library Classes needed by this Platform.
#
################################################################################
!include $(PLATFORM_PACKAGE)/Include/Dsc/CoreCommonLib.dsc
!include $(PLATFORM_FULL_PACKAGE)/PlatformCommonLib.dsc
!include $(PLATFORM_PACKAGE)/Include/Dsc/CorePeiLib.dsc
!include $(PLATFORM_FULL_PACKAGE)/PlatformPeiLib.dsc
!include $(PLATFORM_PACKAGE)/Include/Dsc/CoreDxeLib.dsc
!include $(PLATFORM_FULL_PACKAGE)/PlatformDxeLib.dsc

[LibraryClasses.common]
!include $(C1S_PRODUCT_PATH)/SiPkgCommonLib.dsc

  UserPasswordLib|UserAuthFeaturePkg/Library/UserPasswordLib/UserPasswordLib.inf
  UserPasswordUiLib|UserAuthFeaturePkg/Library/UserPasswordUiLib/UserPasswordUiLib.inf
  PlatformPasswordLib|UserAuthFeaturePkg/Library/PlatformPasswordLibNull/PlatformPasswordLibNull.inf

  DisplayUpdateProgressLib|$(PLATFORM_FEATURES_PATH)/CapsuleUpdate/Library/PlatformDisplayUpdateProgressLib/PlatformDisplayUpdateProgressLib.inf
  PlatformHookLib|MdeModulePkg/Library/BasePlatformHookLibNull/BasePlatformHookLibNull.inf

  ConfigBlockLib|IntelSiliconPkg/Library/BaseConfigBlockLib/BaseConfigBlockLib.inf

  SiMtrrLib|$(PLATFORM_SI_PACKAGE)/Library/SiMtrrLib/SiMtrrLib.inf

  BoardIdsLib|$(PLATFORM_FULL_PACKAGE)/Library/PeiDxeBoardIdsLib/PeiDxeBoardIdsLib.inf

  # For AlderLakePlatSamplePkg\PlatformInitAdvanced\PlatformInitAdvancedPei\PlatformInitAdvancedPreMem.inf
  SiliconInitLib|$(PLATFORM_BOARD_PACKAGE)/Library/SiliconInitLib/SiliconInitLib.inf

  PostCodeMapLib|PostCodeDebugFeaturePkg/Library/PostCodeMapLib/PostCodeMapLib.inf

  Usb3DebugPortParameterLib|$(PLATFORM_FULL_PACKAGE)/Library/BaseUsb3DebugPortParameterLibCmos/BaseUsb3DebugPortParameterLibCmos.inf


  FspCommonLib|IntelFsp2Pkg/Library/BaseFspCommonLib/BaseFspCommonLib.inf
  FspSwitchStackLib|IntelFsp2Pkg/Library/BaseFspSwitchStackLib/BaseFspSwitchStackLib.inf            # RPPO-CNL-0090
  FspWrapperApiLib|IntelFsp2WrapperPkg/Library/BaseFspWrapperApiLib/BaseFspWrapperApiLib.inf

  HsPhyDashboardLib|$(PLATFORM_FEATURES_PATH)/PciePhyFwLoading/Library/PeiDxeSmmHsPhyDashboardLib/PeiDxeSmmHsPhyDashboardLib.inf
  VmgExitLib|UefiCpuPkg/Library/VmgExitLibNull/VmgExitLibNull.inf
[LibraryClasses.IA32]

!include $(C1S_PRODUCT_PATH)/SiPkgPeiLib.dsc

  FirmwareBootMediaLib|IntelSiliconPkg/Library/PeiDxeSmmBootMediaLib/PeiFirmwareBootMediaLib.inf
  SmmAccessLib|IntelSiliconPkg/Feature/SmmAccess/Library/PeiSmmAccessLib/PeiSmmAccessLib.inf

  BiosIdLib|BoardModulePkg/Library/BiosIdLib/PeiBiosIdLib.inf
  MpInitLib|UefiCpuPkg/Library/MpInitLib/PeiMpInitLib.inf

  TraceHubDebugLib|$(PLATFORM_SI_PACKAGE)/Library/TraceHubDebugLibSvenTx/PeiTraceHubDebugLibSvenTx.inf

  # For AlderLakePlatSamplePkg\PlatformInitAdvanced\PlatformInitAdvancedPei\PlatformInitAdvancedPreMem.inf
  BoardConfigLib|$(PLATFORM_BOARD_PACKAGE)/Library/PeiBoardConfigLib/PeiBoardConfigLib.inf
  # SetupDataCacheLib|$(PLATFORM_BOARD_PACKAGE)/Features/Setup/Library/PeiSetupDataCacheLib/PeiSetupDataCacheLib.inf

!if gPlatformModuleTokenSpaceGuid.PcdUsb3SerialStatusCodeEnable == TRUE
  Usb3DebugPortLib|Usb3DebugFeaturePkg/Library/Usb3DebugPortLib/Usb3DebugPortLibPei.inf
!else
  Usb3DebugPortLib|Usb3DebugFeaturePkg/Library/Usb3DebugPortLib/Usb3DebugPortLibNull.inf
!endif

[LibraryClasses.IA32.SEC]
!if $(TARGET) == DEBUG
  DebugPrintErrorLevelLib|MdePkg/Library/BaseDebugPrintErrorLevelLib/BaseDebugPrintErrorLevelLib.inf
!endif

[LibraryClasses.IA32.PEI_CORE]

[LibraryClasses.IA32.PEIM]
!if $(TARGET) == DEBUG
  DebugLib|MdeModulePkg/Library/PeiDebugLibDebugPpi/PeiDebugLibDebugPpi.inf
!endif

  PeiCpuPcieHsPhyInitLib|$(PLATFORM_FEATURES_PATH)/PciePhyFwLoading/Library/PeiCpuPcieHsPhyInitLib/PeiCpuPcieHsPhyInitLib.inf
  PeiCpuPcieHashMiscLib| $(PLATFORM_FEATURES_PATH)/PciePhyFwLoading/Library/PeiCpuPcieHashMiscLib/PeiCpuPcieHashMiscLib.inf

[LibraryClasses.X64]

!include $(C1S_PRODUCT_PATH)/SiPkgDxeLib.dsc

  TcgPhysicalPresenceLib|SecurityPkg/Library/DxeTcgPhysicalPresenceLib/DxeTcgPhysicalPresenceLib.inf
  TcgPpVendorLib|SecurityPkg/Library/TcgPpVendorLibNull/TcgPpVendorLibNull.inf

  BiosIdLib|BoardModulePkg/Library/BiosIdLib/DxeBiosIdLib.inf

  FirmwareBootMediaLib|IntelSiliconPkg/Library/PeiDxeSmmBootMediaLib/DxeSmmFirmwareBootMediaLib.inf

  TraceHubDebugLib|$(PLATFORM_SI_PACKAGE)/Library/TraceHubDebugLibSvenTx/DxeTraceHubDebugLibSvenTx.inf

!if gPlatformModuleTokenSpaceGuid.PcdUsb3SerialStatusCodeEnable == TRUE
  Usb3DebugPortLib|Usb3DebugFeaturePkg/Library/Usb3DebugPortLib/Usb3DebugPortLibDxe.inf
!else
  Usb3DebugPortLib|Usb3DebugFeaturePkg/Library/Usb3DebugPortLib/Usb3DebugPortLibNull.inf
!endif

!if gSiPkgTokenSpaceGuid.PcdSmbiosEnable == TRUE
  DxeSmbiosProcessorLib|$(PLATFORM_SI_PACKAGE)/Library/DxeSmbiosProcessorLib/DxeSmbiosProcessorLib.inf
!else
  DxeSmbiosProcessorLib|$(PLATFORM_SI_PACKAGE)/Library/DxeSmbiosProcessorLibNull/DxeSmbiosProcessorLibNull.inf
!endif

################################################################################
#
# Pcd Section - list of all EDK II PCD Entries defined by this Platform
#
################################################################################
[PcdsFeatureFlag.common]

[PcdsFeatureFlag.X64]

[PcdsFixedAtBuild.common]

[PcdsFeatureFlag]

[PcdsFixedAtBuild.IA32]

[PcdsFixedAtBuild.X64]

[PcdsPatchableInModule.common]

[PcdsDynamicHii.common.DEFAULT]

  gPlatformModuleTokenSpaceGuid.PcdPlatformMemoryCheck|L"MemoryCheck"|gPlatformModuleTokenSpaceGuid|0x0|0

[PcdsDynamicHii.X64.DEFAULT]

  gEfiMdePkgTokenSpaceGuid.PcdPlatformBootTimeOut|L"Timeout"|gEfiGlobalVariableGuid|0x0|5 # Variable: L"Timeout"

[PcdsDynamicDefault]
  gEfiMdeModulePkgTokenSpaceGuid.PcdSrIovSupport|FALSE

!if $(TARGET) == DEBUG
  gEfiMdeModulePkgTokenSpaceGuid.PcdStatusCodeUseSerial|TRUE
!else
  gEfiMdeModulePkgTokenSpaceGuid.PcdStatusCodeUseSerial|FALSE
!endif

[PcdsDynamicDefault.common.DEFAULT]

  gMinPlatformPkgTokenSpaceGuid.PcdPciReservedMemBase |0x80000000
  gMinPlatformPkgTokenSpaceGuid.PcdPciReservedMemLimit|0xC0000000
  gMinPlatformPkgTokenSpaceGuid.PcdPciReservedMemAbove4GBBase  |0xFFFFFFFFFFFFFFFF
  gMinPlatformPkgTokenSpaceGuid.PcdPciReservedMemAbove4GBLimit |0x0000000000000000
  gMinPlatformPkgTokenSpaceGuid.PcdPciSegmentCount|0x1

!if (gBoardModuleTokenSpaceGuid.PcdS4Enable == FALSE)
  gEfiMdeModulePkgTokenSpaceGuid.PcdResetOnMemoryTypeInformationChange|FALSE
!else
  gEfiMdeModulePkgTokenSpaceGuid.PcdResetOnMemoryTypeInformationChange|TRUE
!endif

  gEfiMdeModulePkgTokenSpaceGuid.PcdAtaSmartEnable|TRUE

  ## The mask is used to control VTd behavior.<BR><BR>
  #  BIT0: Enable IOMMU during boot (If DMAR table is installed in DXE. If VTD_INFO_PPI is installed in PEI.)
  #  BIT1: Enable IOMMU when transfer control to OS (ExitBootService in normal boot. EndOfPEI in S3)
  # @Prompt The policy for VTd driver behavior.
  gIntelSiliconPkgTokenSpaceGuid.PcdVTdPolicyPropertyMask|0x00000000
  gEfiMdeModulePkgTokenSpaceGuid.PcdRecoveryFileName|L"Capsule.cap"|VOID*|0x20
  gSiPkgTokenSpaceGuid.PcdPciExpressRegionLength|0x10000000
  gEfiMdeModulePkgTokenSpaceGuid.PcdAcpiDefaultOemTableId|0x20202020204C4349
  gPlatformModuleTokenSpaceGuid.PcdTopSwapEnableSwSmi |0xFF
  gPlatformModuleTokenSpaceGuid.PcdTopSwapDisableSwSmi|0xFF

  gEfiSecurityPkgTokenSpaceGuid.PcdFirmwareDebuggerInitialized|FALSE
  gEfiSecurityPkgTokenSpaceGuid.PcdSkipHddPasswordPrompt|FALSE
  gEfiSecurityPkgTokenSpaceGuid.PcdSkipOpalPasswordPrompt|FALSE

[PcdsDynamicExDefault.common.DEFAULT]

!if (gIntelFsp2WrapperTokenSpaceGuid.PcdFspModeSelection == 1)
  ## Specifies max supported number of Logical Processors.
  # @Prompt Configure max supported number of Logical Processorss
  gUefiCpuPkgTokenSpaceGuid.PcdCpuMaxLogicalProcessorNumber|16
  ## Specifies the AP wait loop state during POST phase.
!else
  ## Specifies max supported number of Logical Processors.
  # @Prompt Configure max supported number of Logical Processorss
  gUefiCpuPkgTokenSpaceGuid.PcdCpuMaxLogicalProcessorNumber|12
!endif

  #  The value is defined as below.
  #  1: Place AP in the Hlt-Loop state.
  #  2: Place AP in the Mwait-Loop state.
  #  3: Place AP in the Run-Loop state.
  # @Prompt The AP wait loop state.
  gUefiCpuPkgTokenSpaceGuid.PcdCpuApLoopMode|2

  ## Specifies the base address of the first microcode Patch in the microcode Region.
  # @Prompt Microcode Region base address.
  gUefiCpuPkgTokenSpaceGuid.PcdCpuMicrocodePatchAddress|0x0

  ## Specifies the size of the microcode Region.
  # @Prompt Microcode Region size.
  gUefiCpuPkgTokenSpaceGuid.PcdCpuMicrocodePatchRegionSize|0x0
  gEfiMdeModulePkgTokenSpaceGuid.PcdFastPS2Detection|FALSE

  #
  # Some of the PCD consumed by both FSP and bootloader should be defined
  # here for bootloader to consume.
  #

[PcdsDynamicDefault.X64.DEFAULT]

  gEfiMdeModulePkgTokenSpaceGuid.PcdConInConnectOnDemand|FALSE

################################################################################
#
# Components Section
#
################################################################################
[Components.IA32]

  $(PLATFORM_FEATURES_PATH)/Pct/PlatformConfigTool.inf
  $(PLATFORM_FEATURES_PATH)/VTd/PlatformVTdInfoSamplePei/PlatformVTdInfoSamplePei.inf

  $(PLATFORM_FEATURES_PATH)/PlatformStatusCodeHandler/Pei/PlatformStatusCodeHandlerPei.inf {
    <LibraryClasses>
      !if $(TARGET) == DEBUG
        DebugLib|$(PLATFORM_FULL_PACKAGE)/Library/BaseDebugLibAllDebugPort/BaseDebugLibAllDebugPort.inf
      !else
        !if gSiPkgTokenSpaceGuid.PcdSiCatalogDebugEnable == TRUE
          DebugLib|$(PLATFORM_FULL_PACKAGE)/Library/BaseDebugLibAllDebugPort/BaseDebugLibAllDebugPort.inf
        !endif
      !endif
  }

!if gSiPkgTokenSpaceGuid.PcdAmtEnable == TRUE
  $(PLATFORM_FEATURES_PATH)/Amt/AmtStatusCodePei/AmtStatusCodePei.inf
!endif

!if gBoardModuleTokenSpaceGuid.PcdAdvancedFeatureEnable == TRUE
  $(PLATFORM_FULL_PACKAGE)/PlatformInitAdvanced/PlatformInitAdvancedPei/PlatformInitAdvancedPreMem.inf {
    <LibraryClasses>
      PeiWdtAppLib|$(PLATFORM_FULL_PACKAGE)/Library/PeiWdtAppLib/PeiWdtAppLib.inf
  }
!endif  # PcdAdvancedFeatureEnable

!if gBoardModuleTokenSpaceGuid.PcdAdvancedFeatureEnable == TRUE
  $(PLATFORM_FULL_PACKAGE)/PlatformInitAdvanced/PlatformInitAdvancedPei/PlatformInitAdvancedPostMem.inf
!endif  # PcdAdvancedFeatureEnable

!if gPlatformModuleTokenSpaceGuid.PcdCapsuleEnable == TRUE
  $(PLATFORM_FULL_PACKAGE)/Features/CapsuleUpdate/PlatformInitRecovery/PlatformInitRecoveryPei.inf {
  <LibraryClasses>
    PeiPlatformRecoveryLib|$(PLATFORM_FULL_PACKAGE)/Features/CapsuleUpdate/Library/PeiPlatformRecoveryLib/PeiPlatformRecoveryLib.inf
  }
!endif

!if gPlatformModuleTokenSpaceGuid.PcdCapsuleEnable == TRUE
  $(PLATFORM_FEATURES_PATH)/CapsuleUpdate/BiosInfoRecovery/BiosInfoRecovery.inf
!endif

!if gMinPlatformPkgTokenSpaceGuid.PcdUefiSecureBootEnable == TRUE
  $(PLATFORM_FULL_PACKAGE)/Platform/BiosInfoChecker/BiosInfoChecker.inf
!endif

  $(PLATFORM_FULL_PACKAGE)/FspWrapper/FspWrapperPeim/FspWrapperPeim.inf
  $(PLATFORM_FULL_PACKAGE)/Telemetry/FspFirmwareVersionPeim/FspFirmwareVersionPeim.inf

!if gIntelFsp2WrapperTokenSpaceGuid.PcdFspModeSelection == 1
  $(PLATFORM_FULL_PACKAGE)/Telemetry/BiosGuardFirmwareVersionPeim/BiosGuardFirmwareVersionPeimFsp.inf
!else
  $(PLATFORM_FULL_PACKAGE)/Telemetry/BiosGuardFirmwareVersionPeim/BiosGuardFirmwareVersionPeim.inf
!endif
 $(PLATFORM_FEATURES_PATH)/PciePhyFwLoading/PciePhyFwLoadingInit/Pei/PeiPciePhyFwLoadingInit.inf

[Components.X64]

  $(PLATFORM_FEATURES_PATH)/Cnv/CnvUefiVariables/CnvUefiVariables.inf
  $(PLATFORM_FEATURES_PATH)/CrashLogDxe/CrashLogDxe.inf
  $(PLATFORM_FULL_PACKAGE)/Library/DxeDriverAddrDecodeTest/AddrDecodeEncodeTest0.inf
  $(PLATFORM_FEATURES_PATH)/FastBoot/FastBootHandlerDxe/FastBootHandlerDxe.inf
  $(PLATFORM_FEATURES_PATH)/FastBoot/FastBootSetupDxe/FastBootSetupDxe.inf
  $(PLATFORM_FEATURES_PATH)/Hsti/HstiIhvSmm/HstiIhvSmm.inf
  $(PLATFORM_FEATURES_PATH)/Rst/RstUefiDriverSupport/RstUefiDriverSupport.inf
  $(PLATFORM_FEATURES_PATH)/VTd/PlatformVTdSampleDxe/PlatformVTdSampleDxe.inf
  $(PLATFORM_FULL_PACKAGE)/Platform/SpiFvbService/SpiFvbServiceDxe.inf
  $(PLATFORM_FULL_PACKAGE)/PlatformInitAdvanced/PolicyInitAdvancedDxe/PolicyInitAdvancedDxe.inf
  $(PLATFORM_FULL_PACKAGE)/Telemetry/PlatformFirmwareVersionDxe/PlatformFirmwareVersionDxe.inf
  $(PLATFORM_FULL_PACKAGE)/Telemetry/SiFirmwareVersionDxe/SiFirmwareVersionDxe.inf

  #
  # TELEMETRY_VERSION_RECORD_DATA has been removed, temporarily remove following driver to avoid internal build fail.
  #
  # $(PLATFORM_FULL_PACKAGE)/Telemetry/GopFirmwareVersionDxe/GopFirmwareVersionDxe.inf
  # $(PLATFORM_FULL_PACKAGE)/Telemetry/UndiFirmwareVersionDxe/UndiFirmwareVersionDxe.inf

  $(PLATFORM_FEATURES_PATH)/Hsti/HstiIhvDxe/HstiIhvDxe.inf {
    <LibraryClasses>
      HstiLib|MdePkg/Library/DxeHstiLib/DxeHstiLib.inf
      Tpm2CommandLib|SecurityPkg/Library/Tpm2CommandLib/Tpm2CommandLib.inf
  }

  $(PLATFORM_FEATURES_PATH)/PlatformStatusCodeHandler/RuntimeDxe/PlatformStatusCodeHandlerRuntimeDxe.inf {
    <LibraryClasses>
        SerialIoUartLib|$(PLATFORM_SI_PACKAGE)/IpBlock/SerialIo/Uart/Library/PeiDxeSmmSerialIoUartLib/DxeRuntimeSerialIoUartLib.inf
      !if $(TARGET) == DEBUG
        DebugLib|$(PLATFORM_FULL_PACKAGE)/Library/BaseDebugLibAllDebugPort/BaseDebugLibAllDebugPort.inf
        SerialIoUartDebugPropertyLib|$(PLATFORM_FULL_PACKAGE)/Library/SerialIoUartDebugPropertyLib/DxeSmmSerialIoUartDebugPropertyLib.inf
      !else
        !if gSiPkgTokenSpaceGuid.PcdSiCatalogDebugEnable == TRUE
          DebugLib|$(PLATFORM_FULL_PACKAGE)/Library/BaseDebugLibAllDebugPort/BaseDebugLibAllDebugPort.inf
        !endif
      !endif
  }

  $(PLATFORM_FULL_PACKAGE)/PlatformInitAdvanced/PlatformInitAdvancedDxe/PlatformInitAdvancedDxe.inf {
    <LibraryClasses>
      BootGuardRevocationLib|$(PLATFORM_FULL_PACKAGE)/Library/BootGuardRevocationLib/BootGuardRevocationLib.inf
      DxeFirmwareVersionInfoLib|$(PLATFORM_FULL_PACKAGE)/Library/DxeFirmwareVersionInfoLib/DxeFirmwareVersionInfoLib.inf
      DxeSmbiosDataHobLib|IntelSiliconPkg/Library/DxeSmbiosDataHobLib/DxeSmbiosDataHobLib.inf
      DxeTelemetryAcpiLib|$(PLATFORM_FULL_PACKAGE)/Telemetry/Library/DxeTelemetryAcpiLib/DxeTelemetryAcpiLib.inf
  }

  $(PLATFORM_FULL_PACKAGE)/Platform/PciPlatform/Dxe/PciPlatform.inf {
    <LibraryClasses>
      DxeOpromPatchLib|$(PLATFORM_FULL_PACKAGE)/Library/DxeOpromPatchLib/DxeOpromPatchLib.inf
  }

!if gSiPkgTokenSpaceGuid.PcdSmmVariableEnable == TRUE
  $(PLATFORM_FULL_PACKAGE)/Platform/SpiFvbService/SpiFvbServiceSmm.inf
!endif # gSiPkgTokenSpaceGuid.PcdSmmVariableEnable

  $(PLATFORM_FEATURES_PATH)/PlatformStatusCodeHandler/Smm/PlatformStatusCodeHandlerSmm.inf

!if gBoardModuleTokenSpaceGuid.PcdIntelRaidEnable == TRUE
  $(PLATFORM_FEATURES_PATH)/Sata/IntelUefiRaidDiskInfo/IntelUefiRaidDiskInfo.inf
!endif

#
# I2C Stack:Start
#
!if gPlatformModuleTokenSpaceGuid.PcdPiI2cStackEnable == TRUE
  $(PLATFORM_FEATURES_PATH)/I2c/I2cPlatformDxe/I2cPlatformDxe.inf
!endif

!if gPlatformModuleTokenSpaceGuid.PcdI2cTouchDriverEnable == TRUE
  $(PLATFORM_FEATURES_PATH)/I2c/I2cTouchPanelDxe/I2cTouchPanelDxe.inf
!endif

!if gSiPkgTokenSpaceGuid.PcdSmbiosEnable == TRUE
  $(PLATFORM_FULL_PACKAGE)/Platform/SmbiosPlatformInfo/SmbiosPlatformInfo.inf
  $(PLATFORM_FEATURES_PATH)/Smbios/SmbiosMiscDxe/SmbiosMiscDxe.inf
  !if gMinPlatformPkgTokenSpaceGuid.PcdTpm2Enable == TRUE
    $(PLATFORM_FEATURES_PATH)/Smbios/SmbiosTpmDeviceInfo/SmbiosTpmDeviceInfo.inf
  !endif
    $(PLATFORM_FEATURES_PATH)/Me/MeSmbiosUpdateConfigDxe/MeSmbiosUpdateConfigDxe.inf
!endif

  $(PLATFORM_FEATURES_PATH)/Hsti/HstiResultDxe/HstiResultDxe.inf


!if gIntelFsp2WrapperTokenSpaceGuid.PcdFspModeSelection == 1
  $(PLATFORM_FULL_PACKAGE)/FspWrapper/CheckFspErrorInfoHob/CheckFspErrorInfoHobDxe.inf {
    <LibraryClasses>
      !if $(TARGET) == DEBUG
        DebugLib|$(PLATFORM_FULL_PACKAGE)/Library/BaseDebugLibAllDebugPort/BaseDebugLibAllDebugPort.inf
      !endif
  }
!endif

!if gPlatformModuleTokenSpaceGuid.PcdTpmEnable == TRUE
  !if gPlatformModuleTokenSpaceGuid.PcdVtioEnable == TRUE
        $(PLATFORM_FULL_PACKAGE)/Vtio/Dxe/VtioDxe.inf
  !endif
  # module to update variable according to TPM HW state, the code could be in TcgDxe driver but better to be separated
  $(PLATFORM_FEATURES_PATH)/Tcg/TcgSetupDxe/TcgSetupDxe.inf
!endif # gPlatformModuleTokenSpaceGuid.PcdTpmEnable

#!if gPlatformModuleTokenSpaceGuid.PcdSaveMemoryConfigEnable == TRUE
  $(PLATFORM_FULL_PACKAGE)/Platform/SaveMemoryConfig/Dxe/SaveMemoryConfig.inf
#!endif

!if gSiPkgTokenSpaceGuid.PcdAcpiEnable == TRUE
  $(PLATFORM_FEATURES_PATH)/Acpi/AcpiTables/AcpiFeatures.inf
!endif

  $(PLATFORM_FEATURES_PATH)/Me/MeUlvCheckDxe/MeUlvCheckDxe.inf

!if gBoardModuleTokenSpaceGuid.PcdSetupEnable == TRUE
  $(PLATFORM_FEATURES_PATH)/UiApp/UiApp.inf {
    <Defines>
      FILE_GUID = D89A7D8B-D016-4D26-93E3-EAB6B4D3B0A2
  }
!endif #PcdSetupEnable == TRUE

# !if gBoardModuleTokenSpaceGuid.PcdSetupEnable == TRUE
#   !include $(PLATFORM_BOARD_PACKAGE)/Features/Setup/Include/Dsc/SetupDxe.dsc
# !endif


#
# AcpiTables
#
!if gSiPkgTokenSpaceGuid.PcdAcpiEnable == TRUE
  $(PLATFORM_FEATURES_PATH)/Dptf/AcpiTables/DptfAcpiTables.inf
  $(PLATFORM_FEATURES_PATH)/Dptf/Dxe/Dptf.inf
!endif

!if gPlatformModuleTokenSpaceGuid.PcdNetworkEnable == TRUE
    !if ("MSFT" in $(FAMILY)) || ($(TOOL_CHAIN_TAG) == "CLANGPDB")
      $(PLATFORM_FEATURES_PATH)/WifiConnectionManagerDxe/WifiConnectionManagerDxe.inf
    !endif
!endif # gPlatformModuleTokenSpaceGuid.PcdNetworkEnable

!if gPlatformModuleTokenSpaceGuid.PcdCapsuleEnable == TRUE
  !include $(PLATFORM_FEATURES_PATH)/CapsuleUpdate/FmpPlatformMonolithic.dsc
  !include $(PLATFORM_FEATURES_PATH)/CapsuleUpdate/FmpPlatformBios.dsc
  !include $(PLATFORM_FEATURES_PATH)/CapsuleUpdate/FmpPlatformBtGAcm.dsc
  !include $(PLATFORM_FEATURES_PATH)/CapsuleUpdate/FmpPlatformMe.dsc
  !include $(PLATFORM_FEATURES_PATH)/CapsuleUpdate/FmpPlatformEc.dsc
  !include $(PLATFORM_FEATURES_PATH)/CapsuleUpdate/FmpPlatformIshPdt.dsc
  !include $(PLATFORM_FEATURES_PATH)/CapsuleUpdate/FmpPlatformuCode.dsc
!endif

!if gPlatformModuleTokenSpaceGuid.PcdPciHotplugEnable == TRUE
  $(PLATFORM_FULL_PACKAGE)/Platform/PciHotPlug/PciHotPlug.inf
!endif

!if gPlatformModuleTokenSpaceGuid.PcdUsbTypeCEnable == TRUE
  $(PLATFORM_FEATURES_PATH)/UsbTypeC/UsbTypeCDxe/UsbTypeCDxe.inf
!endif

!if gPlatformModuleTokenSpaceGuid.PcdDnxSupportEnable == TRUE
  $(PLATFORM_FEATURES_PATH)/Dnx/DnxDxe/DnxDxe.inf
!endif

!if gSiPkgTokenSpaceGuid.PcdAtaEnable == TRUE
  $(PLATFORM_FEATURES_PATH)/Sata/SataController/SataController.inf
!endif

!if gSiPkgTokenSpaceGuid.PcdAmtEnable == TRUE
  $(PLATFORM_FEATURES_PATH)/Amt/AmtSaveMebxConfigDxe/AmtSaveMebxConfigDxe.inf
  $(PLATFORM_FEATURES_PATH)/Amt/AmtPetAlertDxe/AmtPetAlertDxe.inf
  $(PLATFORM_FEATURES_PATH)/Amt/AsfTable/AsfTable.inf
  $(PLATFORM_FEATURES_PATH)/Amt/AmtMacPassThrough/AmtMacPassThrough.inf
  $(PLATFORM_FEATURES_PATH)/Amt/AmtWrapperDxe/AmtWrapperDxe.inf
  !if gPlatformModuleTokenSpaceGuid.PcdEnableSecureErase == TRUE
    $(PLATFORM_FEATURES_PATH)/Amt/SecureEraseDxe/SecureEraseDxe.inf
  !endif
!endif

  $(PLATFORM_FEATURES_PATH)/Me/MeExtMeasurement/Dxe/MeExtMeasurement.inf
  $(PLATFORM_FEATURES_PATH)/TrustedDeviceSetup/TrustedDeviceSetup.inf

  $(PLATFORM_FEATURES_PATH)/Wdt/WdtAppDxe/WdtAppDxe.inf

!if gSiPkgTokenSpaceGuid.PcdEmbeddedEnable == TRUE
  $(PLATFORM_FULL_PACKAGE)/Sio/NuvotonNct6776fDxe/NuvotonNct6776f.inf
  $(PLATFORM_FULL_PACKAGE)/Sio/Nct6776fHwMonDxe/Nct6776fHwMon.inf
  $(PLATFORM_FEATURES_PATH)/LifeCycleState/LifeCycleState.inf
!endif

!if gBoardModuleTokenSpaceGuid.PcdEcEnable == TRUE
  $(PLATFORM_FULL_PACKAGE)/EC/EC.inf
!endif

!if gBoardModuleTokenSpaceGuid.PcdAdvancedFeatureEnable == TRUE
  !if gSiPkgTokenSpaceGuid.PcdAcpiEnable == TRUE
    $(PLATFORM_FULL_PACKAGE)/PlatformInitAdvanced/PlatformInitAdvancedSmm/PlatformInitAdvancedSmm.inf {
      <BuildOptions>
        *_*_X64_CC_FLAGS      = -DDOCK_ENABLE
    }
  !endif
!endif

!if gPlatformModuleTokenSpaceGuid.PcdUsbFnEnable == TRUE
  $(PLATFORM_FEATURES_PATH)/Usb/UsbDeviceDxe/UsbDeviceDxe.inf
!endif

#
# MultiPdt Support Driver
#
$(PLATFORM_FEATURES_PATH)/MultiPdt/MultiPdtDxe.inf {
  <LibraryClasses>
    PdtUpdateLib|$(PLATFORM_FULL_PACKAGE)/Library/PdtUpdateLib/PdtUpdateLib.inf
}

#
# Build Options
#
!include $(C1S_PRODUCT_PATH)/SiPkgBuildOption.dsc
!include $(PLATFORM_FULL_PACKAGE)/PlatformPkgBuildOption.dsc