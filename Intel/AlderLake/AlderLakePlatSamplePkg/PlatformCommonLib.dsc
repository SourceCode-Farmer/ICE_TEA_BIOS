## @file
# platform build option configuration file.
#
# @copyright
#  INTEL CONFIDENTIAL
#  Copyright 2020 - 2021 Intel Corporation.
#
#  The source code contained or described herein and all documents related to the
#  source code ("Material") are owned by Intel Corporation or its suppliers or
#  licensors. Title to the Material remains with Intel Corporation or its suppliers
#  and licensors. The Material may contain trade secrets and proprietary and
#  confidential information of Intel Corporation and its suppliers and licensors,
#  and is protected by worldwide copyright and trade secret laws and treaty
#  provisions. No part of the Material may be used, copied, reproduced, modified,
#  published, uploaded, posted, transmitted, distributed, or disclosed in any way
#  without Intel's prior express written permission.
#
#  No license under any patent, copyright, trade secret or other intellectual
#  property right is granted to or conferred upon you by disclosure or delivery
#  of the Materials, either expressly, by implication, inducement, estoppel or
#  otherwise. Any license under such intellectual property rights must be
#  express and approved by Intel in writing.
#
#  Unless otherwise agreed by Intel in writing, you may not remove or alter
#  this notice or any other notice embedded in Materials by Intel or
#  Intel's suppliers or licensors in any way.
#
#  This file contains a 'Sample Driver' and is licensed as such under the terms
#  of your license agreement with Intel or your vendor. This file may be modified
#  by the user, subject to the additional terms of the license agreement.
#
# @par Specification
##

[LibraryClasses.common.PEI_CORE]
!if $(TARGET) == DEBUG
  DebugLib|$(PLATFORM_FULL_PACKAGE)/Library/BaseDebugLibAllDebugPort/BaseDebugLibAllDebugPort.inf
!endif

[LibraryClasses.common.DXE_CORE]
!if $(TARGET) == DEBUG
  DebugLib|$(PLATFORM_FULL_PACKAGE)/Library/BaseDebugLibAllDebugPort/BaseDebugLibAllDebugPort.inf
!endif

[LibraryClasses.common]
  SiliconPolicyInitLib|$(PLATFORM_PACKAGE)/PlatformInit/Library/SiliconPolicyInitLibNull/SiliconPolicyInitLibNull.inf
  SiliconPolicyUpdateLib|$(PLATFORM_PACKAGE)/PlatformInit/Library/SiliconPolicyUpdateLibNull/SiliconPolicyUpdateLibNull.inf

!if gPlatformModuleTokenSpaceGuid.PcdSerialPortEnable == TRUE
  SerialPortLib|$(PLATFORM_FULL_PACKAGE)/Library/BaseSerialPortLib/BaseSerialPortLib.inf
!else
  SerialPortLib|MdePkg/Library/BaseSerialPortLibNull/BaseSerialPortLibNull.inf
!endif

 #
 # Telemetry
 #
 TelemetryFviLib|$(PLATFORM_FULL_PACKAGE)/Telemetry/Library/DxeTelemetryFviLib/DxeTelemetryFviLib.inf
 DxeTelemetryAcpiLib|$(PLATFORM_FULL_PACKAGE)/Telemetry/Library/DxeTelemetryAcpiLib/DxeTelemetryAcpiLib.inf

#
# Platform
#
!if gBoardModuleTokenSpaceGuid.PcdSetupEnable == TRUE
  PlatformBootManagerLib|$(PLATFORM_FULL_PACKAGE)/Library/DxePlatformBootManagerLib/DxePlatformBootManagerLib.inf
!else
  PlatformBootManagerLib|$(PLATFORM_FULL_PACKAGE)/Library/DxePlatformBootManagerLib/DxePlatformBootManagerHiiFreeLib.inf
!endif

  FspWrapperPlatformLib|$(PLATFORM_FULL_PACKAGE)/FspWrapper/Library/PeiFspWrapperPlatformLib/PeiFspWrapperPlatformLib.inf
  FspWrapperHobProcessLib|$(PLATFORM_FULL_PACKAGE)/FspWrapper/Library/PeiFspWrapperHobProcessLib/PeiFspWrapperHobProcessLib.inf
  PlatformInitLib|$(PLATFORM_FULL_PACKAGE)/Library/PlatformInitLib/PlatformInitLib.inf
  PlatformSecLib|$(PLATFORM_FULL_PACKAGE)/FspWrapper/Library/SecFspWrapperPlatformSecLib/SecFspWrapperPlatformSecLib.inf

  HidI2cPlatformSupportLib|$(PLATFORM_FULL_PACKAGE)/Library/DxeHidI2cPlatformSupportLib/DxeHidI2cPlatformSupportLib.inf

!if gBoardModuleTokenSpaceGuid.PcdEcEnable == TRUE
  EcLib|$(PLATFORM_FULL_PACKAGE)/Library/BaseEcLib/BaseEcLib.inf
  EcMiscLib|$(PLATFORM_FULL_PACKAGE)/Library/BaseEcMiscLib/BaseEcMiscLib.inf
  EcTcssLib|$(PLATFORM_FULL_PACKAGE)/Library/BaseEcTcssLib/BaseEcTcssLib.inf
  EcHwLib|$(PLATFORM_FULL_PACKAGE)/Library/BaseEcHwLib/BaseEcHwLib.inf
!else
  EcLib|$(PLATFORM_FULL_PACKAGE)/Library/BaseEcLibNull/BaseEcLibNull.inf
  EcMiscLib|$(PLATFORM_FULL_PACKAGE)/Library/BaseEcMiscLibNull/BaseEcMiscLibNull.inf
  EcTcssLib|$(PLATFORM_FULL_PACKAGE)/Library/BaseEcTcssLibNull/BaseEcTcssLibNull.inf
!endif

  PssLib|$(PLATFORM_FULL_PACKAGE)/Library/PssLib/PssLib.inf
  BeepLib|$(PLATFORM_FULL_PACKAGE)/Library/BeepLib/BeepLib.inf

  Usb3DebugPortParameterLib|$(PLATFORM_FULL_PACKAGE)/Library/BaseUsb3DebugPortParameterLibCmos/BaseUsb3DebugPortParameterLibCmos.inf

  SerialPortParameterLib|$(PLATFORM_FULL_PACKAGE)/Library/BaseSerialPortParameterLibPcd/BaseSerialPortParameterLibPcd.inf
  SerialIoUartDebugHelperLib|$(PLATFORM_FULL_PACKAGE)/Library/BaseSerialIoUartDebugHelperLib/BaseSerialIoUartDebugHelperLib.inf
  SerialIoUartDebugPropertyLib|$(PLATFORM_FULL_PACKAGE)/Library/SerialIoUartDebugPropertyLib/BaseSerialIoUartDebugPropertyLib.inf
  DimmInfoLib|$(PLATFORM_FULL_PACKAGE)/Setup/DimmInfoLib.inf
  CmosAccessLib|$(PLATFORM_FEATURES_PATH)/Cmos/Library/CmosAccessLib/CmosAccessLib.inf
  PlatformNvRamHookLib|$(PLATFORM_FEATURES_PATH)/Cmos/Library/PlatformNvRamHookLibCmos/PlatformNvRamHookLibCmos.inf

#
# S3
#
!if gPlatformModuleTokenSpaceGuid.PcdCapsuleEnable == TRUE
!if ("MSFT" in $(FAMILY)) || ($(TOOL_CHAIN_TAG) == "CLANGPDB")
  FwUpdateLib|$(PLATFORM_FEATURES_PATH)/CapsuleUpdate/Library/FwUpdateLib/FwUpdateLib.inf
!else
  FwUpdateLib|$(PLATFORM_FEATURES_PATH)/CapsuleUpdate/Library/FwUpdateLib/FwUpdateLibNull.inf
!endif
  PlatformFlashAccessLib|$(PLATFORM_FEATURES_PATH)/CapsuleUpdate/Library/PlatformFlashAccessLib/PlatformFlashAccessLib.inf
  SeamlessRecoverySupportLib|$(PLATFORM_FEATURES_PATH)/CapsuleUpdate/Library/SeamlessRecoverySupportLib/SeamlessRecoverySupportLib.inf
!else
  PlatformFvAddressLib|$(PLATFORM_FULL_PACKAGE)/Library/PlatformFvAddressLib/PlatformFvAddressLib.inf
!endif

  Usb3DebugPortDummyLib|$(PLATFORM_FULL_PACKAGE)/Library/BaseUsb3DebugPortDummyLib/Usb3DebugPortDummyLib.inf

!if gPlatformModuleTokenSpaceGuid.PcdPlatformCmosAccessSupport == TRUE
  PlatformCmosAccessLib|$(PLATFORM_FEATURES_PATH)/Cmos/Library/PlatformCmosAccessLib/PlatformCmosAccessLib.inf
!else
  PlatformCmosAccessLibNull|$(PLATFORM_FEATURES_PATH)/Cmos/Library/PlatformCmosAccessLibNull/PlatformCmosAccessLibNull.inf
!endif

!if gSiPkgTokenSpaceGuid.PcdEmbeddedEnable == 0x1
#
# Sub Region FVs
#
  PeiSubRegionLib|$(PLATFORM_FEATURES_PATH)/SubRegion/PeiSubRegionLib/PeiSubRegionLib.inf
#
# TSN
#
  PeiTsnFvLib|$(PLATFORM_FEATURES_PATH)/Tsn/PeiTsnFvLib/PeiTsnFvLib.inf
!else
  PeiSubRegionLib|$(PLATFORM_FEATURES_PATH)/SubRegion/PeiSubRegionLib/PeiSubRegionLibNull.inf
  PeiTsnFvLib|$(PLATFORM_FEATURES_PATH)/Tsn/PeiTsnFvLib/PeiTsnFvLibNull.inf
!endif

  TcgStorageCoreLib|SecurityPkg/Library/TcgStorageCoreLib/TcgStorageCoreLib.inf
  TcgStorageOpalLib|SecurityPkg/Library/TcgStorageOpalLib/TcgStorageOpalLib.inf

  PlatformCmosAccessLib|$(PLATFORM_FEATURES_PATH)/Cmos/Library/PlatformCmosAccessLib/PlatformCmosAccessLib.inf
!if gPlatformModuleTokenSpaceGuid.PcdDTbtEnable == TRUE
  DTbtCommonLib|$(PLATFORM_FEATURES_PATH)/Tbt/Library/DTbtCommonLib/DTbtCommonLib.inf
  DxeTbtSecurityLib|$(PLATFORM_FEATURES_PATH)/Tbt/Library/DxeTbtSecurityLib/DxeTbtSecurityLib.inf
!else
  DTbtCommonLib|$(PLATFORM_FEATURES_PATH)/Tbt/Library/DTbtCommonLibNull/DTbtCommonLibNull.inf
  DxeTbtSecurityLib|$(PLATFORM_FEATURES_PATH)/Tbt/Library/DxeTbtSecurityLibNull/DxeTbtSecurityLibNull.inf
!endif
