/** @file
# Description file for BootState Variable Information during Dxe Phase.
#
#@copyright
#  INTEL CONFIDENTIAL
#  Copyright 2020 Intel Corporation.
#
#  The source code contained or described herein and all documents related to the
#  source code ("Material") are owned by Intel Corporation or its suppliers or
#  licensors. Title to the Material remains with Intel Corporation or its suppliers
#  and licensors. The Material may contain trade secrets and proprietary and
#  confidential information of Intel Corporation and its suppliers and licensors,
#  and is protected by worldwide copyright and trade secret laws and treaty
#  provisions. No part of the Material may be used, copied, reproduced, modified,
#  published, uploaded, posted, transmitted, distributed, or disclosed in any way
#  without Intel's prior express written permission.
#
#  No license under any patent, copyright, trade secret or other intellectual
#  property right is granted to or conferred upon you by disclosure or delivery
#  of the Materials, either expressly, by implication, inducement, estoppel or
#  otherwise. Any license under such intellectual property rights must be
#  express and approved by Intel in writing.
#
#  Unless otherwise agreed by Intel in writing, you may not remove or alter
#  this notice or any other notice embedded in Materials by Intel or
#  Intel's suppliers or licensors in any way.
#
#  This file contains a 'Sample Driver' and is licensed as such under the terms
#  of your license agreement with Intel or your vendor. This file may be modified
#  by the user, subject to the additional terms of the license agreement.
#
# @par Specification Reference:
**/

#include <Guid/BootStateCapsule.h>
#include <Library/DebugLib.h>
#include <Uefi.h>
#include <Library/UefiLib.h>
#include <Library/UefiRuntimeServicesTableLib.h>
#include <Library/MemoryAllocationLib.h>
#include <BootStateLib.h>

/**
 Set BootStateAfterCapsule variable to indicate the following boot is the very first boot after Capsule Update.

 @param[in]      BootStateAfterCapsule  TRUE, first boot after Capsule Update.
 @retval         EFI_SUCCESS            BootStateAfterCapsule variable set.
**/

EFI_STATUS
EFIAPI
SetBootStateAfterCapsule (
  IN BOOLEAN                        BootStateAfterCapsule
  )
{

  EFI_STATUS                        Status;
  Status= gRT->SetVariable (
                 BOOT_STATE_AFTER_CAPSULE_VARIABLE_NAME,
                 &gBootStateAfterCapsuleGuid,
                 EFI_VARIABLE_NON_VOLATILE | EFI_VARIABLE_BOOTSERVICE_ACCESS,
                 sizeof (BOOLEAN),
                 &BootStateAfterCapsule
                 );
  ASSERT_EFI_ERROR(Status);
  return Status;
}

/**
 This function call Checks BootState variable is NULL or not

 @retval         FALSE                  It's the first boot after reflashing.
 @retval         TRUE                   It's not the first boot after reflashing.
**/
BOOLEAN
EFIAPI
IsBootStatePresent (
  VOID
)
{
  EFI_STATUS                        Status;
  UINT8                             *BootState;

  ///
  /// Get last Boot State Variable From NVRAM
  ///
  BootState = NULL;
  Status = GetVariable2 (
             BOOT_STATE_VARIABLE_NAME,
             &gBootStateGuid,
             (VOID **) &BootState,
             NULL
             );
  if (Status == EFI_SUCCESS) {
    FreePool (BootState);
    DEBUG ((DEBUG_INFO, "DxeIsBootStatePresent : BootState is present.\n"));
    return TRUE;
  } else if (Status == EFI_NOT_FOUND) {
    DEBUG ((DEBUG_INFO, "DxeIsBootStatePresent : BootState is NOT present.\n"));
    return FALSE;
  } else {
    DEBUG ((DEBUG_ERROR, "DxeIsBootStatePresent : Get variable failure.\n"));
    ASSERT_EFI_ERROR (Status);
    return FALSE;
  }
}

/**
 This function call use to set bootstate variable.
 In DXE  phase BootState variable got set using setvariable().From onward it will treat as second boot.

 @param[in]      BootState              Set value for bootstate
**/
VOID
EFIAPI
SetBootState (
  VOID
)
{
  EFI_STATUS                        Status;
  BOOLEAN                           BootState;
  Status = gRT->SetVariable (
                  BOOT_STATE_VARIABLE_NAME,
                  &gBootStateGuid,
                  EFI_VARIABLE_NON_VOLATILE | EFI_VARIABLE_BOOTSERVICE_ACCESS,
                  sizeof(BOOLEAN),
                  &BootState
                  );
  ASSERT_EFI_ERROR(Status);
  return;
}

/**
 Delete BootState variable to force next boot is FullConfiguration boot
**/
VOID
EFIAPI
UnsetBootState (
  VOID
)
{
  EFI_STATUS                        Status;
  Status=gRT->SetVariable (
                BOOT_STATE_VARIABLE_NAME,
                &gBootStateGuid,
                0,
                0,
                NULL
                );
  ASSERT_EFI_ERROR(Status);
  return;
}
