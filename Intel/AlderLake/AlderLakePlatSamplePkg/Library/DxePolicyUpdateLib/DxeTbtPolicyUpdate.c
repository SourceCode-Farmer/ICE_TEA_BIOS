/** @file
 
;******************************************************************************
;* Copyright 2021 Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corp.
;*
;******************************************************************************
*/
/** @file
  This file is the library for TBT DXE Policy initialization.

@copyright
  INTEL CONFIDENTIAL
  Copyright 2017 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains a 'Sample Driver' and is licensed as such under the terms
  of your license agreement with Intel or your vendor. This file may be modified
  by the user, subject to the additional terms of the license agreement.

@par Specification Reference:
**/
#include <DxeTbtPolicyUpdate.h>
#include <Library/CmosAccessLib.h>
#include <CmosMap.h>
#include <TcssDataHob.h>
//[-start-210527-IB16740147-add]//
#include <Library/CmosLib.h>
#include <ChipsetCmos.h>
//[-end-210527-IB16740147-add]//

/**
  Get data for platform policy from setup options.

  @param[in] ITbtPolicy                The pointer to get ITBT Policy protocol instance

  @retval EFI_SUCCESS                  Operation success.

**/
EFI_STATUS
EFIAPI
UpdateDxeTbtPolicy (
  IN OUT  ITBT_POLICY_PROTOCOL   *ITbtPolicy
  )
{
  EFI_STATUS                     Status;
  UINTN                          DataSize;
  SETUP_DATA                     SetupData;
  SA_SETUP                       SaSetup;
  UINT8                          Index;
  DXE_ITBT_CONFIG                *DxeITbtConfig;
  UINT8                          CmosUsb4CmModeValue;
#if FixedPcdGetBool (PcdDTbtEnable) == 1
  DXE_DTBT_POLICY_PROTOCOL       *DxeDTbtConfig;
  DxeDTbtConfig = NULL;
#endif

  DxeITbtConfig = NULL;
  Status = EFI_NOT_FOUND;
  DEBUG ((DEBUG_INFO, "UpdateTbtPolicyCallback\n"));

  DataSize = sizeof (SetupData);
  Status = gRT->GetVariable (
                  L"Setup",
                  &gSetupVariableGuid,
                  NULL,
                  &DataSize,
                  &SetupData
                  );
  ASSERT_EFI_ERROR (Status);
  if (EFI_ERROR (Status)) {
    return Status;
  }

  DataSize = sizeof (SA_SETUP);
  Status = gRT->GetVariable (
                  L"SaSetup",
                  &gSaSetupVariableGuid,
                  NULL,
                  &DataSize,
                  &SaSetup
                  );
  ASSERT_EFI_ERROR(Status);
  if (EFI_ERROR (Status)) {
    return Status;
  }


  //
  // Get requisite IP Config Blocks which needs to be used here
  //
  if (ITbtPolicy != NULL) {
    Status = GetConfigBlock ((VOID *)ITbtPolicy, &gDxeITbtConfigGuid, (VOID *)&DxeITbtConfig);
  }

  if (DxeITbtConfig == NULL) {
    DEBUG ((DEBUG_ERROR, " DxeITbtConfig is not created!!!\n"));
  } else {
    for (Index = 0; Index < MAX_ITBT_PCIE_PORT; Index++) {
      DxeITbtConfig->ITbtResourceConfig[Index].TbtPcieExtraBusRsvd   = SetupData.ITbtPcieExtraBusRsvd[Index];
      DxeITbtConfig->ITbtResourceConfig[Index].TbtPcieMemRsvd        = SetupData.ITbtPcieMemRsvd[Index];
      DxeITbtConfig->ITbtResourceConfig[Index].TbtPcieMemAddrRngMax  = SetupData.ITbtPcieMemAddrRngMax[Index];
      DxeITbtConfig->ITbtResourceConfig[Index].TbtPciePMemRsvd       = SetupData.ITbtPciePMemRsvd[Index];
      DxeITbtConfig->ITbtResourceConfig[Index].TbtPciePMemAddrRngMax = SetupData.ITbtPciePMemAddrRngMax[Index];
    }
    DxeITbtConfig->TbtCommonConfig.TbtVtdBaseSecurity   = (SetupData.DisableTbtPcieTreeBme && SaSetup.EnableVtd);

//[-start-210709-IB16740147-modify]//
//    CmosUsb4CmModeValue = CmosRead8 (CMOS_USB4_CM_MODE_REG);
    CmosUsb4CmModeValue = ReadExtCmos8 (R_XCMOS_INDEX, R_XCMOS_DATA, CMOS_USB4_CM_MODE_REG_CHP);
//[-end-210709-IB16740147-modify]//
    if ((SetupData.Usb4CmMode == SETUP_OPTION_USB4_CM_MODE_FW) ||
        ((SetupData.Usb4CmMode == SETUP_OPTION_USB4_CM_MODE_OS) && (CmosUsb4CmModeValue == USB4_CM_MODE_FW_CM))) {
      DxeITbtConfig->ITbtGenericConfig.ITbtRtd3 = SetupData.ITbtRtd3;
    } else {
      DxeITbtConfig->ITbtGenericConfig.ITbtRtd3 = 1;
    }

    DxeITbtConfig->ITbtGenericConfig.ITbtRtd3ExitDelay  = SetupData.ITbtRtd3ExitDelay;
    DxeITbtConfig->OsNativeResourceBalance              = SetupData.OsNativeResourceBalance;
  }
#if FixedPcdGetBool (PcdDTbtEnable) == 1
  //
  // Get requisite IP Config Blocks which needs to be used here
  //
  Status = gBS->LocateProtocol (
                &gDxeDTbtPolicyProtocolGuid,
                NULL,
                (VOID **) &DxeDTbtConfig
                );

  if (DxeDTbtConfig == NULL) {
    DEBUG ((DEBUG_ERROR, " DxeDTbtConfig is not created!!!\n"));
  } else {
    for (Index = 0; Index < MAX_DTBT_CONTROLLER_NUMBER; Index++) {
      DxeDTbtConfig->DTbtResourceConfig[Index].TbtPcieExtraBusRsvd   = SetupData.DTbtPcieExtraBusRsvd[Index];
      DxeDTbtConfig->DTbtResourceConfig[Index].TbtPcieMemRsvd        = SetupData.DTbtPcieMemRsvd[Index];
      DxeDTbtConfig->DTbtResourceConfig[Index].TbtPcieMemAddrRngMax  = SetupData.DTbtPcieMemAddrRngMax[Index];
      DxeDTbtConfig->DTbtResourceConfig[Index].TbtPciePMemRsvd       = SetupData.DTbtPciePMemRsvd[Index];
      DxeDTbtConfig->DTbtResourceConfig[Index].TbtPciePMemAddrRngMax = SetupData.DTbtPciePMemAddrRngMax[Index];
    }
    DxeDTbtConfig->TbtCommonConfig.TbtVtdBaseSecurity    = (SetupData.DisableTbtPcieTreeBme && SaSetup.EnableVtd);
    DxeDTbtConfig->DTbtGenericConfig.TbtWakeupSupport    = SetupData.TbtWakeupSupport;
    DxeDTbtConfig->DTbtGenericConfig.Gpio5Filter         = SetupData.Gpio5Filter;
    DxeDTbtConfig->DTbtGenericConfig.DTbtRtd3            = SetupData.DTbtRtd3;
    DxeDTbtConfig->DTbtGenericConfig.DTbtRtd3OffDelayOptEn = SetupData.DTbtRtd3OffDelayOptEn;
    DxeDTbtConfig->DTbtGenericConfig.DTbtRtd3OffDelay    = SetupData.DTbtRtd3OffDelay;
    DxeDTbtConfig->DTbtGenericConfig.DTbtRtd3ClkReq      = SetupData.DTbtRtd3ClkReq;
    DxeDTbtConfig->DTbtGenericConfig.DTbtRtd3ClkReqDelay = SetupData.DTbtRtd3ClkReqDelay;
    DxeDTbtConfig->DTbtGenericConfig.DTbtGo2SxCommand    = SetupData.DTbtGo2SxCommand;
  }
  DTbtPrintDxePolicyConfig();
#endif
  return EFI_SUCCESS;
}

