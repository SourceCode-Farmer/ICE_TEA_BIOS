/** @file
 
;******************************************************************************
;* Copyright 2021 Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corp.
;*
;******************************************************************************
*/
/** @file
  This file is the library for SA DXE Policy initialization.

@copyright
  INTEL CONFIDENTIAL
  Copyright 2004 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains a 'Sample Driver' and is licensed as such under the terms
  of your license agreement with Intel or your vendor. This file may be modified
  by the user, subject to the additional terms of the license agreement.

@par Specification Reference:
**/

#include <PlatformBoardId.h>
#include <DxeSaPolicyUpdate.h>
#include "MemoryConfig.h"
#include <Library/CpuPcieInfoFruLib.h>
#include <SaDataHob.h>
#include <Library/PcdLib.h>
#include <MemInfoHob.h>
#include <IndustryStandard/SmBios.h>


/**
  Retrieves a custom string for the SMBIOS Type 17 Table DeviceLocator field.

  @param[in]  This                          A pointer to this instance of MEMORY_DXE_CONFIG.
  @param[in]  Controller                    Desired Controller to get a DeviceLocator string for.
  @param[in]  Dimm                          Desired DIMM to get a DeviceLocator string for.
  @param[in]  MdSocket                      0 = Memory Down, 1 = Socketed.

  @retval                                   The DeviceLocator string
  @retval     NULL                          If the return value is NULL, the default value will be used.
**/
CHAR8*
EFIAPI
GetPlatformDeviceLocatorString (
  IN CONST    MEMORY_DXE_CONFIG   *This,
  IN          UINT8               Controller,
  IN          UINT8               Dimm,
  IN          UINT8               MdSocket
  )
{
  return NULL;
}

/**
  Retrieves a custom string for the SMBIOS Type 17 Table BankLocator field.

  @param[in]  This                          A pointer to this instance of MEMORY_DXE_CONFIG.
  @param[in]  Controller                    Desired Controller to get a BankLocator string for.
  @param[in]  Dimm                          Desired DIMM to get a BankLocator string for.
  @param[in]  MdSocket                      0 = Memory Down, 1 = Socketed.

  @retval                                   The BankLocator string
  @retval     NULL                          If the return value is NULL, the default value will be used.
**/
CHAR8*
EFIAPI
GetPlatformBankLocatorString (
  IN CONST    MEMORY_DXE_CONFIG   *This,
  IN          UINT8               Controller,
  IN          UINT8               Dimm,
  IN          UINT8               MdSocket
  )
{
  return NULL;
}

/**
  Get data for platform policy from setup options.

  @param[in] SaPolicy                  The pointer to get SA Policy protocol instance

  @retval EFI_SUCCESS                  Operation success.

**/
EFI_STATUS
EFIAPI
UpdateDxeSaPolicy (
  IN OUT  SA_POLICY_PROTOCOL    *SaPolicy
  )
{
  UINTN                     VariableSize;
  SA_SETUP                  SaSetup;
  SETUP_DATA                SetupVariables;
  EFI_STATUS                Status;
  UINT8                     pegFn;
  GRAPHICS_DXE_CONFIG       *GraphicsDxeConfig;
  PCIE_DXE_CONFIG           *PcieDxeConfig;
  VTD_DXE_CONFIG            *VtdDxeConfig;
  MEMORY_DXE_CONFIG         *MemoryDxeConfig;
  UINT8                     ControllerIndex;
  UINT16                    BoardId;
  SA_DATA_HOB              *SaDataHob;
  EFI_HOB_GUID_TYPE        *GuidHob;
  MEMORY_INFO_DATA_HOB     *MemInfo;
//[-start-210811-IB09480148-add]//
  UINT8                     ChannelIndex;
  UINT8                     SlotMapTemp;
  UINT8                     *SlotMap;
//[-end-210811-IB09480148-add]//

  GraphicsDxeConfig = NULL;
  PcieDxeConfig = NULL;
  VtdDxeConfig = NULL;
  MemoryDxeConfig = NULL;
  SaDataHob = NULL;
  MemInfo = NULL;
  GuidHob = NULL;
  //
  // Get requisite IP Config Blocks which needs to be used here
  //
  Status = GetConfigBlock ((VOID *)SaPolicy, &gGraphicsDxeConfigGuid, (VOID *)&GraphicsDxeConfig);
  ASSERT_EFI_ERROR (Status);

  Status = GetConfigBlock ((VOID *)SaPolicy, &gVtdDxeConfigGuid, (VOID *)&VtdDxeConfig);
  ASSERT_EFI_ERROR (Status);

  Status = GetConfigBlock ((VOID *)SaPolicy, &gPcieDxeConfigGuid, (VOID *)&PcieDxeConfig);
  ASSERT_EFI_ERROR (Status);

  Status = GetConfigBlock ((VOID *)SaPolicy, &gMemoryDxeConfigGuid, (VOID *)&MemoryDxeConfig);
  ASSERT_EFI_ERROR (Status);

  VariableSize = sizeof (SETUP_DATA);
  ZeroMem (&SetupVariables, VariableSize);
  Status = gRT->GetVariable (
                  L"Setup",
                  &gSetupVariableGuid,
                  NULL,
                  &VariableSize,
                  &SetupVariables
                  );
  ASSERT_EFI_ERROR (Status);

  VariableSize = sizeof (SA_SETUP);
  Status = gRT->GetVariable (
                  L"SaSetup",
                  &gSaSetupVariableGuid,
                  NULL,
                  &VariableSize,
                  &SaSetup
                  );
  ASSERT_EFI_ERROR (Status);


  if (PcdGetBool (PcdSpdAddressOverride)) {
//[-start-200204-IB14630301-modify]//
    MemoryDxeConfig->SpdAddressTable[0] = PcdGet8 (PcdMrcSpdAddressTable0);
    MemoryDxeConfig->SpdAddressTable[1] = PcdGet8 (PcdMrcSpdAddressTable1);
    MemoryDxeConfig->SpdAddressTable[2] = PcdGet8 (PcdMrcSpdAddressTable2);
    MemoryDxeConfig->SpdAddressTable[3] = PcdGet8 (PcdMrcSpdAddressTable3);
//[-end-200204-IB14630301-modify]//
  }
  MemoryDxeConfig->GetDeviceLocatorString = GetPlatformDeviceLocatorString;
  MemoryDxeConfig->GetBankLocatorString   = GetPlatformBankLocatorString;

  if (!EFI_ERROR(Status)) {
    //
    // Initialize the PCIE Configuration
    //
    for (pegFn=0; pegFn < SA_PEG_MAX_FUN; pegFn++ ) {
      PcieDxeConfig->PegAspm[pegFn]        = SaSetup.PegAspm[pegFn];
    }
    for (pegFn = 0; pegFn < GetMaxCpuPciePortNum (); pegFn++) {
      PcieDxeConfig->PegPwrOpt[pegFn].LtrEnable  = SaSetup.CpuPcieLtrEnable[pegFn];
    }
    if(SaSetup.Peg0Enable == 1) {
      PcieDxeConfig->PegRootPortHPE[0] = SaSetup.PegRootPortHPE[0];
    }
    if(SaSetup.Peg1Enable == 1) {
      PcieDxeConfig->PegRootPortHPE[1] = SaSetup.PegRootPortHPE[1];
    }
    if(SaSetup.Peg2Enable == 1) {
      PcieDxeConfig->PegRootPortHPE[2] = SaSetup.PegRootPortHPE[2];
    }
    if(SaSetup.Peg3Enable == 1) {
      PcieDxeConfig->PegRootPortHPE[3] = SaSetup.PegRootPortHPE[3];
    }

    //
    // Global NVS Graphics configuration
    //
    GraphicsDxeConfig->AlsEnable               = SaSetup.AlsEnable;
    GraphicsDxeConfig->BacklightControlSupport = SaSetup.IgdLcdBlc;
    GraphicsDxeConfig->IgdBootType             = SaSetup.IgdBootType;
    GraphicsDxeConfig->IgdPanelType            = SaSetup.LcdPanelType;
    GraphicsDxeConfig->IgdPanelScaling         = SaSetup.LcdPanelScaling;
    GraphicsDxeConfig->IgdBlcConfig            = SaSetup.IgdLcdBlc;
    GraphicsDxeConfig->GfxTurboIMON            = SaSetup.GfxTurboIMON;
    GraphicsDxeConfig->IuerStatusVal          |= SaSetup.SlateIndicatorRT << 6;
    GraphicsDxeConfig->IuerStatusVal          |= SaSetup.DockIndicatorRT << 7;
  }

  //
  // Initialize the Memory Configuration
  //
  for (ControllerIndex = 0; ControllerIndex < MEM_CFG_MAX_CONTROLLERS; ControllerIndex++) {
    ZeroMem (MemoryDxeConfig->SlotMap[ControllerIndex], sizeof (UINT8) * MEM_CFG_MAX_CHANNELS);
  }

  //
  // Retrieve MEMORY_INFO_DATA_HOB from HOB
  //
  GuidHob = GetFirstGuidHob (&gSiMemoryInfoDataGuid);
  ASSERT (GuidHob != NULL);
  if (GuidHob == NULL) {
    return EFI_NOT_FOUND;
  }
  MemInfo = (MEMORY_INFO_DATA_HOB *) GET_GUID_HOB_DATA (GuidHob);
  if (MemInfo != NULL) {
    if ((MemInfo->MemoryType == MemoryTypeLpddr4) || (MemInfo->MemoryType == MemoryTypeLpddr5)) {
      MemoryDxeConfig->SlotMap[0][0] = 0x01;
      MemoryDxeConfig->SlotMap[0][1] = 0x01;
      MemoryDxeConfig->SlotMap[0][2] = 0x01;
      MemoryDxeConfig->SlotMap[0][3] = 0x01;
      MemoryDxeConfig->SlotMap[1][0] = 0x01;
      MemoryDxeConfig->SlotMap[1][1] = 0x01;
      MemoryDxeConfig->SlotMap[1][2] = 0x01;
      MemoryDxeConfig->SlotMap[1][3] = 0x01;
    }
  }

  BoardId = PcdGet16 (PcdBoardId);
  switch (BoardId) {
  // 2DPC boards
  case BoardIdAdlSTgpHDdr5UDimm2DCrb:
  case BoardIdAdlSAdpSDdr4UDimm2DErb1:
  case BoardIdAdlSAdpSDdr4UDimm2DCrb:
  case BoardIdAdlSAdpSDdr4UDimm2DCrbEv:
  case BoardIdAdlSAdpSDdr4UDimm2DCrbCpv:
  case BoardIdAdlSAdpSDdr5UDimm2DCrb:
  case BoardIdAdlSAdpSSbgaDdr5SODimmErb:
  case BoardIdAdlSAdpSSbgaDdr5SODimmCrb:
  case BoardIdAdlSAdpSSbgaDdr5SODimmCrbPpv:
  case BoardIdAdlSAdpSSbgaDdr4SODimmCrb:
  case BoardIdAdlSAdpSSbgaDdr5SODimmAep:
    MemoryDxeConfig->SlotMap[0][0] = 0x03;  // Controller-0, Channel-A, DIMM-0 & DIMM-1
    MemoryDxeConfig->SlotMap[1][0] = 0x03;  // Controller-1, Channel-A, DIMM-0 & DIMM-1
    break;

//[-start-210811-IB09480148-add]//
  case SkuIdAdlPDdr5Rvp:
  case SkuIdAdlSAdpSDdr5UDimm1DCrb:
    MemoryDxeConfig->SlotMap[0][0] = 0x01;    // Controller-0, Channel-A, DIMM-0
    MemoryDxeConfig->SlotMap[0][1] = 0x01;    // Controller-0, Channel-B, DIMM-0
    MemoryDxeConfig->SlotMap[1][0] = 0x01;    // Controller-1, Channel-A, DIMM-0
    MemoryDxeConfig->SlotMap[1][1] = 0x01;    // Controller-1, Channel-B, DIMM-0
    break;

  case SkuIdAdlPLp4Rvp:
  case SkuIdAdlPLp5Rvp:
  case SkuIdAdlMLp4Rvp:
  case SkuIdAdlMLp5Rvp:
    MemoryDxeConfig->SlotMap[0][0] = 0x01;    // Controller-0, Channel-A, DIMM-0
    MemoryDxeConfig->SlotMap[0][1] = 0x01;    // Controller-0, Channel-B, DIMM-0
    MemoryDxeConfig->SlotMap[0][2] = 0x01;    // Controller-0, Channel-C, DIMM-0
    MemoryDxeConfig->SlotMap[0][3] = 0x01;    // Controller-0, Channel-D, DIMM-0
    MemoryDxeConfig->SlotMap[1][0] = 0x01;    // Controller-1, Channel-A, DIMM-0
    MemoryDxeConfig->SlotMap[1][1] = 0x01;    // Controller-1, Channel-B, DIMM-0
    MemoryDxeConfig->SlotMap[1][2] = 0x01;    // Controller-1, Channel-C, DIMM-0
    MemoryDxeConfig->SlotMap[1][3] = 0x01;    // Controller-1, Channel-D, DIMM-0
    break;
//[-end-210811-IB09480148-add]//

  default:
    MemoryDxeConfig->SlotMap[0][0] = 0x01;  // Controller-0, Channel-A, DIMM-0
    MemoryDxeConfig->SlotMap[1][0] = 0x01;  // Controller-1, Channel-A, DIMM-0
    break;
  }
//[-start-210811-IB09480148-add]//
  //
  //  Override Memory Slot Map by PcdOverrideMemorySlotMap
  //
  if (PcdGetBool(PcdOverrideMemorySlotMapEnable) == TRUE) {
    SlotMap = NULL;
    SlotMap = (UINT8 *) PcdGetPtr (PcdOverrideMemorySlotMap);
    if (SlotMap != NULL) {
      for (ControllerIndex = 0; ControllerIndex < MEM_CFG_MAX_CONTROLLERS; ControllerIndex++) {
        for (ChannelIndex = 0; ChannelIndex < MEM_CFG_MAX_CHANNELS; ChannelIndex++) {
          
          SlotMapTemp = SlotMap[(ControllerIndex * MEM_CFG_MAX_CHANNELS) + (ChannelIndex)];
          if ((SlotMapTemp >= 0x1) && (SlotMapTemp <= 0x03)) {
            MemoryDxeConfig->SlotMap[ControllerIndex][ChannelIndex] = SlotMapTemp;
          } else if ((SlotMapTemp == 0xFF) || (SlotMapTemp == 0x00)) {
            MemoryDxeConfig->SlotMap[ControllerIndex][ChannelIndex] = 0x0;
          }
          
        }
      }
    }
  }
//[-end-210811-IB09480148-add]//  
  //
  // Initialize the Platform dependent Graphics configuration.
  // Set BIT0 & BIT1 if Platform is Connected Standby capable & it's capability field is valid respectively.
  // Please refer to IGD's ACPI Opregion spec for other bit definitions.
  //
  if (SetupVariables.LowPowerS0Idle == 0) {
    GraphicsDxeConfig->PlatformConfig |= (BIT1 | BIT0);
  } else {
    GraphicsDxeConfig->PlatformConfig &= (UINT32) (~BIT0);
  }

  GraphicsDxeConfig->IuerStatusVal = 0;
  //
  // Get the HOB for SA Data
  //
  SaDataHob = (SA_DATA_HOB *)GetFirstGuidHob (&gSaDataHobGuid);
  if (SaDataHob == NULL) {
    DEBUG ((EFI_D_ERROR, "SA Data Hob not found\n"));
    return EFI_NOT_FOUND;
  }
  if (SaDataHob->ResizableBarSupport == 1) {
    PcdSetBoolS (PcdPcieResizableBarSupport, TRUE);
    DEBUG ((DEBUG_INFO, "PcdPcieResizableBarSupport = %x\n", PcdGetBool (PcdPcieResizableBarSupport)));
  }
  return EFI_SUCCESS;
}

