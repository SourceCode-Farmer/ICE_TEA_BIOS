/** @file
# Description file for BootState Variable Information during PEI Phase.
#
#@copyright
#  INTEL CONFIDENTIAL
#  Copyright 2020 Intel Corporation.
#
#  The source code contained or described herein and all documents related to the
#  source code ("Material") are owned by Intel Corporation or its suppliers or
#  licensors. Title to the Material remains with Intel Corporation or its suppliers
#  and licensors. The Material may contain trade secrets and proprietary and
#  confidential information of Intel Corporation and its suppliers and licensors,
#  and is protected by worldwide copyright and trade secret laws and treaty
#  provisions. No part of the Material may be used, copied, reproduced, modified,
#  published, uploaded, posted, transmitted, distributed, or disclosed in any way
#  without Intel's prior express written permission.
#
#  No license under any patent, copyright, trade secret or other intellectual
#  property right is granted to or conferred upon you by disclosure or delivery
#  of the Materials, either expressly, by implication, inducement, estoppel or
#  otherwise. Any license under such intellectual property rights must be
#  express and approved by Intel in writing.
#
#  Unless otherwise agreed by Intel in writing, you may not remove or alter
#  this notice or any other notice embedded in Materials by Intel or
#  Intel's suppliers or licensors in any way.
#
#  This file contains a 'Sample Driver' and is licensed as such under the terms
#  of your license agreement with Intel or your vendor. This file may be modified
#  by the user, subject to the additional terms of the license agreement.
#
# @par Specification Reference:
**/

#include <PiPei.h>
#include <Library/DebugLib.h>
#include <BootStateLib.h>
#include <Library/PeiServicesLib.h>
#include <Library/PeiServicesTablePointerLib.h>
#include <Ppi/ReadOnlyVariable2.h>
#include <Guid/BootStateCapsule.h>

/**
 This function call checks if BootState variable is present.
 BootState variable is not present on the first boot after flashing image.
 BootState variable got set end of Dxe phase.

 @retval         FALSE                  It's the first boot after reflashing.
 @retval         TRUE                   It's not the first boot after reflashing.
**/
BOOLEAN
EFIAPI
IsBootStatePresent (
  VOID
)
{
  EFI_STATUS                        Status;
  EFI_PEI_READ_ONLY_VARIABLE2_PPI   *PeiVar;
  BOOLEAN                           BootState;
  UINTN                             DataSize;
  Status = PeiServicesLocatePpi (
             &gEfiPeiReadOnlyVariable2PpiGuid,
             0,
             NULL,
             (VOID **)&PeiVar
             );
  ASSERT_EFI_ERROR(Status);

  ///
  /// Get last Boot State Variable From NVRAM
  ///
  DataSize = sizeof(BOOLEAN);
  Status = PeiVar->GetVariable (
                     PeiVar,
                     BOOT_STATE_VARIABLE_NAME,
                     &gBootStateGuid,
                     NULL,
                     &DataSize,
                     &BootState
                     );
  if (Status == EFI_SUCCESS) {
    DEBUG ((DEBUG_INFO, "PeiIsBootStatePresent : BootState is present.\n"));
    return TRUE;
  } else if (Status == EFI_NOT_FOUND) {
    DEBUG ((DEBUG_INFO, "PeiIsBootStatePresent : BootState is NOT present.\n"));
    return FALSE;
  } else {
    DEBUG ((DEBUG_ERROR, "PeiIsBootStatePresent : Get variable failure.\n"));
    ASSERT_EFI_ERROR (Status);
    return FALSE;
  }
}
/**
 This function checks is the  very first boot after BIOS flash or Capsule update for EC/BIOS.

 @param[in, out] BootStateAfterCapsule  out,0 its second boot after capsule updated
                                        out,1 its very first boot after capsule updated
 @retval         status                 Very first boot after capsule update
 @retval         EFI_SUCCESS            Get Variable BootStateAfterCapsule success.
 @retval         Others                 Returned error status on BootStateAfterCapsule GetVariable ().
**/
EFI_STATUS
EFIAPI
GetBootStateAfterCapsule(
  IN OUT UINT8                      *BootStateAfterCapsule
)
{
  EFI_STATUS                        Status;
  EFI_PEI_READ_ONLY_VARIABLE2_PPI   *PeiVar;
  UINTN                             Size;

  Status = PeiServicesLocatePpi (
             &gEfiPeiReadOnlyVariable2PpiGuid,
             0,
             NULL,
             (VOID **)&PeiVar
             );
  ASSERT_EFI_ERROR(Status);

  Size = sizeof(BOOLEAN);
  Status = PeiVar->GetVariable (
                     PeiVar,
                     BOOT_STATE_AFTER_CAPSULE_VARIABLE_NAME,
                     &gBootStateAfterCapsuleGuid,
                     NULL,
                     &Size,
                     BootStateAfterCapsule
                     );
  if (EFI_ERROR(Status)) {
    return Status;
  }else {
    return EFI_SUCCESS;
  }
}
