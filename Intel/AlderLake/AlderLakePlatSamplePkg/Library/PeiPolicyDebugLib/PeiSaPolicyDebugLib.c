/** @file
  This file is SampleCode of the library for Intel SA PEI Debug Policy initialization.

@copyright
  INTEL CONFIDENTIAL
  Copyright 2014 - 2020 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains a 'Sample Driver' and is licensed as such under the terms
  of your license agreement with Intel or your vendor. This file may be modified
  by the user, subject to the additional terms of the license agreement.

@par Specification Reference:
**/

#include "PeiPolicyDebug.h"
#include <Library/PeiSaPolicyLib.h>
#include <CpuRegs.h>
#include <Library/CpuPlatformLib.h>
#include <Guid/AcpiVariable.h>
#include <Guid/MemoryTypeInformation.h>
#include <Library/HobLib.h>
#include <Platform.h>
#include <Library/PchInfoLib.h>
#include <PolicyUpdateMacro.h>

#include <CpuDmiPreMemConfig.h>
/**
  This function performs SA PEI Debug PreMem Policy initialization.

  @retval EFI_SUCCESS             The PPI is installed and initialized.
  @retval EFI ERRORS              The PPI is not successfully installed.
**/
EFI_STATUS
EFIAPI
UpdatePeiSaPolicyDebugPreMem (
  VOID
  )
{
  EFI_STATUS                      Status;
  EFI_PEI_READ_ONLY_VARIABLE2_PPI *VariableServices;
  SA_SETUP                        SaSetup;
  UINTN                           VarSize;
#if FixedPcdGet8(PcdFspModeSelection) == 1
  VOID                            *FspmUpd;
#else
  SI_PREMEM_POLICY_PPI            *SiPreMemPolicyPpi;
  GRAPHICS_PEI_PREMEM_CONFIG      *GtPreMemConfig;
  PCIE_PEI_PREMEM_CONFIG          *PciePeiPreMemConfig;
  SA_MISC_PEI_PREMEM_CONFIG       *MiscPeiPreMemConfig;
  CPU_DMI_PREMEM_CONFIG           *CpuDmiPreMemConfig;
#endif

  DEBUG ((DEBUG_INFO, "Update PeiSaPolicyDebug Pre-Mem Start\n"));

#if FixedPcdGet8(PcdFspModeSelection) == 1
  FspmUpd = NULL;
#else
  SiPreMemPolicyPpi   = NULL;
  GtPreMemConfig      = NULL;
  PciePeiPreMemConfig = NULL;
  MiscPeiPreMemConfig = NULL;
#endif

#if FixedPcdGet8(PcdFspModeSelection) == 1
  FspmUpd = (FSPM_UPD *) PcdGet32 (PcdFspmUpdDataAddress);
  ASSERT (FspmUpd != NULL);
#else
  Status = PeiServicesLocatePpi (&gSiPreMemPolicyPpiGuid, 0, NULL, (VOID **) &SiPreMemPolicyPpi);
  ASSERT_EFI_ERROR (Status);

  Status = GetConfigBlock ((VOID *) SiPreMemPolicyPpi, &gGraphicsPeiPreMemConfigGuid, (VOID *) &GtPreMemConfig);
  ASSERT_EFI_ERROR (Status);

#if FixedPcdGetBool(PcdCpuPcieEnable) == 1
  Status = GetConfigBlock ((VOID *) SiPreMemPolicyPpi, &gCpuPciePeiPreMemConfigGuid, (VOID *) &PciePeiPreMemConfig);
  ASSERT_EFI_ERROR (Status);
#endif

  Status = GetConfigBlock ((VOID *) SiPreMemPolicyPpi, &gSaMiscPeiPreMemConfigGuid, (VOID *) &MiscPeiPreMemConfig);
  ASSERT_EFI_ERROR (Status);

  Status = GetConfigBlock ((VOID *) SiPreMemPolicyPpi, &gCpuDmiPreMemConfigGuid, (VOID *) &CpuDmiPreMemConfig);
  ASSERT_EFI_ERROR (Status);
#endif

  //
  // Locate system configuration variable
  //
  Status = PeiServicesLocatePpi (
             &gEfiPeiReadOnlyVariable2PpiGuid, // GUID
             0,                                // INSTANCE
             NULL,                             // EFI_PEI_PPI_DESCRIPTOR
             (VOID **) &VariableServices       // PPI
             );
  ASSERT_EFI_ERROR ( Status);

  //
  // Get Setup SA variables
  //
  VarSize = sizeof (SA_SETUP);
  Status = VariableServices->GetVariable (
                               VariableServices,
                               L"SaSetup",
                               &gSaSetupVariableGuid,
                               NULL,
                               &VarSize,
                               &SaSetup
                               );

  if (!EFI_ERROR (Status)) {
    ///
    /// Initialize the DMI Configuration
    ///
    UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.DmiMaxLinkSpeed, CpuDmiPreMemConfig->DmiMaxLinkSpeed, SaSetup.DmiMaxLinkSpeed);
    UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.DmiGen3EqPh2Enable,           CpuDmiPreMemConfig->DmiGen3EqPh2Enable,               SaSetup.DmiGen3EqPh2Enable);
    UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.DmiGen3EqPh3Method,           CpuDmiPreMemConfig->DmiGen3EqPh3Method,               SaSetup.DmiGen3EqPh3Method);

    //
    // Initialize the Graphics configuration
    //
    UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.PanelPowerEnable, GtPreMemConfig->PanelPowerEnable, SaSetup.PanelPowerEnable);
    //
    // Disable PanelPowerEnable if there is no eDP present on DDI-A & B.
    //
#if FixedPcdGet8(PcdFspModeSelection) == 1
    if ((((FSPM_UPD *)FspmUpd)->FspmConfig.DdiPortAConfig != DdiPortEdp) && (((FSPM_UPD *)FspmUpd)->FspmConfig.DdiPortBConfig != DdiPortEdp)) {
#else
    if ((GtPreMemConfig->DdiConfiguration.DdiPortAConfig != DdiPortEdp) && (GtPreMemConfig->DdiConfiguration.DdiPortBConfig != DdiPortEdp)) {
#endif
      UPDATE_POLICY (((FSPM_UPD *)FspmUpd)->FspmConfig.PanelPowerEnable, GtPreMemConfig->PanelPowerEnable, 0x0);
    }

    //
    // Initialize Misc SA Configuration
    //
    UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.LockPTMregs,  MiscPeiPreMemConfig->LockPTMregs,  SaSetup.LockPTMregs);
    UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.BdatEnable,   MiscPeiPreMemConfig->BdatEnable,   SaSetup.BdatEnable);
    UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.BdatTestType,   MiscPeiPreMemConfig->BdatTestType,   SaSetup.BdatTestType);
  }

  return Status;
}

/**
  This function performs SA PEI Debug Policy initialization.

  @retval EFI_SUCCESS        The PPI is installed and initialized.
  @retval EFI ERRORS         The PPI is not successfully installed.
**/
EFI_STATUS
EFIAPI
UpdatePeiSaPolicyDebug(
  VOID
  )
{
  EFI_STATUS                      Status;
#if FixedPcdGetBool(PcdCpuPcieEnable) == 1
  UINT8                           Index;
#endif
  EFI_PEI_READ_ONLY_VARIABLE2_PPI *VariableServices;
  UINTN                           VariableSize;
  SA_SETUP                        SaSetup;
#if FixedPcdGet8(PcdFspModeSelection) == 1
  VOID                            *FspsUpd;
#else
  SI_POLICY_PPI                   *SiPolicyPpi;
  CPU_PCIE_CONFIG                 *CpuPcieRpConfig;
#endif

  DEBUG ((DEBUG_INFO, "Update PeiSaPolicyDebug Post-Mem Start\n"));

#if FixedPcdGet8(PcdFspModeSelection) == 1
  FspsUpd = NULL;
#else
  SiPolicyPpi = NULL;
  CpuPcieRpConfig = NULL;
#endif

#if FixedPcdGet8(PcdFspModeSelection) == 1
  FspsUpd = (FSPS_UPD *)PcdGet32(PcdFspsUpdDataAddress);
  ASSERT(FspsUpd != NULL);
#else
  Status = PeiServicesLocatePpi (&gSiPolicyPpiGuid, 0, NULL, (VOID **) &SiPolicyPpi);
  ASSERT_EFI_ERROR(Status);

#if FixedPcdGetBool(PcdCpuPcieEnable) == 1
  Status = GetConfigBlock ((VOID *) SiPolicyPpi, &gCpuPcieRpConfigGuid, (VOID *) &CpuPcieRpConfig);
  ASSERT_EFI_ERROR(Status);
#endif
#endif
  //
  // Retrieve Setup variable
  //
  Status = PeiServicesLocatePpi (
             &gEfiPeiReadOnlyVariable2PpiGuid, // GUID
             0,                                // INSTANCE
             NULL,                             // EFI_PEI_PPI_DESCRIPTOR
             (VOID **) &VariableServices       // PPI
             );
  ASSERT_EFI_ERROR (Status);

  VariableSize = sizeof (SA_SETUP);
  Status = VariableServices->GetVariable (
                               VariableServices,
                               L"SaSetup",
                               &gSaSetupVariableGuid,
                               NULL,
                               &VariableSize,
                               &SaSetup
                               );
  ASSERT_EFI_ERROR (Status);

#if FixedPcdGetBool(PcdCpuPcieEnable) == 1
  for (Index = 0; Index < GetMaxCpuPciePortNum (); Index++) {
      UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.CpuPcieRpLtrMaxSnoopLatency[Index],                CpuPcieRpConfig->RootPort[Index].PcieRpCommonConfig.PcieRpLtrConfig.LtrMaxSnoopLatency,                0x100F);
      UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.CpuPcieRpLtrMaxNoSnoopLatency[Index],              CpuPcieRpConfig->RootPort[Index].PcieRpCommonConfig.PcieRpLtrConfig.LtrMaxNoSnoopLatency,              0x100F);

    if (SaSetup.CpuPcieLtrEnable[Index] == TRUE) {
      UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.CpuPcieRpLtrMaxSnoopLatency[Index],                CpuPcieRpConfig->RootPort[Index].PcieRpCommonConfig.PcieRpLtrConfig.LtrMaxSnoopLatency,                0x100F);
      UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.CpuPcieRpLtrMaxNoSnoopLatency[Index],              CpuPcieRpConfig->RootPort[Index].PcieRpCommonConfig.PcieRpLtrConfig.LtrMaxNoSnoopLatency,              0x100F);
      UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.CpuPcieRpSnoopLatencyOverrideMode[Index],          CpuPcieRpConfig->RootPort[Index].PcieRpCommonConfig.PcieRpLtrConfig.SnoopLatencyOverrideMode,          SaSetup.CpuPcieSnoopLatencyOverrideMode[Index]);
      UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.CpuPcieRpSnoopLatencyOverrideMultiplier[Index],    CpuPcieRpConfig->RootPort[Index].PcieRpCommonConfig.PcieRpLtrConfig.SnoopLatencyOverrideMultiplier,    SaSetup.CpuPcieSnoopLatencyOverrideMultiplier[Index]);
      UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.CpuPcieRpNonSnoopLatencyOverrideMode[Index],       CpuPcieRpConfig->RootPort[Index].PcieRpCommonConfig.PcieRpLtrConfig.NonSnoopLatencyOverrideMode,       SaSetup.CpuPcieNonSnoopLatencyOverrideMode[Index]);
      UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.CpuPcieRpNonSnoopLatencyOverrideMultiplier[Index], CpuPcieRpConfig->RootPort[Index].PcieRpCommonConfig.PcieRpLtrConfig.NonSnoopLatencyOverrideMultiplier, SaSetup.CpuPcieNonSnoopLatencyOverrideMultiplier[Index]);
      UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.CpuPcieRpSnoopLatencyOverrideValue[Index],         CpuPcieRpConfig->RootPort[Index].PcieRpCommonConfig.PcieRpLtrConfig.SnoopLatencyOverrideValue,         SaSetup.CpuPcieSnoopLatencyOverrideValue[Index]);
      UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.CpuPcieRpNonSnoopLatencyOverrideValue[Index],      CpuPcieRpConfig->RootPort[Index].PcieRpCommonConfig.PcieRpLtrConfig.NonSnoopLatencyOverrideValue,      SaSetup.CpuPcieNonSnoopLatencyOverrideValue[Index]);
    }
  }
#endif
  return Status;
}

