/** @file
  Driver for Platform Boot Options support.

@copyright
  INTEL CONFIDENTIAL
  Copyright 2011 - 2020 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains a 'Sample Driver' and is licensed as such under the terms
  of your license agreement with Intel or your vendor. This file may be modified
  by the user, subject to the additional terms of the license agreement.

@par Specification Reference:
**/

#include "BdsPlatform.h"
#include "AmtSupport.h"
#include "FastBootSupport.h"
#include "String.h"

#include "PlatformBoardId.h"
#include <Library/GpioLib.h>
#include <Library/PcdLib.h>
#include <BootStateLib.h>
#if FixedPcdGetBool(PcdCapsuleEnable) == 1
#include <Guid/SysFwUpdateProgress.h>
#include <Library/HobLib.h>
#endif
#include <Library/FirmwareBootMediaLib.h>
#include <Library/TimerLib.h>
#define MAX_SHELL_INPUT_SIZE  64

BOOLEAN    mContinueBoot  = FALSE;
BOOLEAN    mBootMenuBoot  = FALSE;
BOOLEAN    mPxeBoot       = FALSE;
BOOLEAN    mAnyKeypressed = FALSE;
BOOLEAN    mHotKeypressed = FALSE;
EFI_EVENT  HotKeyEvent    = NULL;
UINTN      mBootMenuOptionNumber;
UINTN      mEfiShellOptionNumber;
BOOLEAN    mDecompressFvOptional = FALSE;


/**
   This function enables FvApp which contains EFI Shell.

   @retval   EFI_SUCCESS Successfully enable FvApp
**/
EFI_STATUS
EnableFvApp (
  VOID
  )
{
  EFI_HANDLE     Handle;
  EFI_STATUS     Status;
  VOID           *FvAppDispatchFlagProtocol;

  FvAppDispatchFlagProtocol = NULL;
  Status = gBS->LocateProtocol (
                  &gFvAppDispatchFlagProtocolGuid,
                  NULL,
                  (VOID **) &FvAppDispatchFlagProtocol
                  );
  if (!EFI_ERROR (Status)) {
    DEBUG ((DEBUG_INFO, "FvApp has been enabled\n"));
    return EFI_SUCCESS;
  }

  Handle = NULL;
  Status = gBS->InstallProtocolInterface (
                &Handle,
                &gFvAppDispatchFlagProtocolGuid,
                EFI_NATIVE_INTERFACE,
                NULL
                );
  if (EFI_ERROR (Status)) {
    DEBUG ((DEBUG_ERROR, "FvApp fails to be enabled, Status = %r\n", Status));
    ASSERT_EFI_ERROR (Status);
    return Status;
  }

  DEBUG ((DEBUG_INFO, "FvApp is successfully enabled\n"));
  return Status;
}

/**
   This function process FvApp which contains EFI Shell.

   @retval   EFI_SUCCESS          Successfully FvApp has been processed.
   @retval   EFI_UNSUPPORTED      FvApp has not been enabled so fails to be processed.
**/
EFI_STATUS
ProcessFvApp (
  VOID
  )
{
  EFI_HANDLE     Handle;
  EFI_STATUS     Status;
  UINT8          Index;

  if (EnableFvApp () != EFI_SUCCESS) {
    DEBUG ((DEBUG_ERROR, "ProcessFvApp : FvApp is not enabled\n"));
    return EFI_UNSUPPORTED;
  }

  Handle = NULL;
  gDS->ProcessFirmwareVolume (
              (VOID *) (UINTN) PcdGet32 (PcdFlashFvOptionalBase),
              PcdGet32 (PcdFlashFvOptionalSize),
              &Handle
              );
  DEBUG ((DEBUG_INFO, "ProcessFvApp : Processing ...\n"));
  for (Index = 0; Index < 10; Index++) {
    Status = gDS->Dispatch ();
    if (Status == EFI_SUCCESS) {
      mDecompressFvOptional = TRUE;
      break;
    }
    DEBUG ((DEBUG_INFO, " processing ... %r\n", Status));
    MicroSecondDelay (50000);
  }

  DEBUG ((DEBUG_INFO, "ProcessFvApp : Status %r\n", Status));

  return EFI_SUCCESS;
}

EFI_DEVICE_PATH_PROTOCOL *
BdsCreateShellDevicePath (
  VOID
  )
/*++

Routine Description:

  This function will create a SHELL BootOption to boot.

Arguments:

  None.

Returns:

  Shell Device path for booting.

--*/
{
  UINTN                             FvHandleCount;
  EFI_HANDLE                        *FvHandleBuffer;
  UINTN                             Index;
  EFI_STATUS                        Status;
  EFI_FIRMWARE_VOLUME2_PROTOCOL     *Fv;
  UINTN                             Size;
  UINT32                            AuthenticationStatus;
  EFI_DEVICE_PATH_PROTOCOL          *DevicePath;
  VOID                              *Buffer;

  DevicePath  = NULL;
  Status      = EFI_SUCCESS;

  DEBUG ((DEBUG_INFO, "BdsCreateShellDevicePath\n"));
  if (!mDecompressFvOptional) {
    Status = ProcessFvApp ();
    if (EFI_ERROR (Status)) {
      DEBUG ((DEBUG_ERROR, "BdsCreateShellDevicePath : FvApp process failure\n"));
      ASSERT_EFI_ERROR (Status);
      return NULL;
    }
  }

  gBS->LocateHandleBuffer (
        ByProtocol,
        &gEfiFirmwareVolume2ProtocolGuid,
        NULL,
        &FvHandleCount,
        &FvHandleBuffer
        );

  for (Index = 0; Index < FvHandleCount; Index++) {
    gBS->HandleProtocol (
          FvHandleBuffer[Index],
          &gEfiFirmwareVolume2ProtocolGuid,
          (VOID **) &Fv
          );

    Buffer  = NULL;
    Size    = 0;
    Status  = Fv->ReadSection (
                    Fv,
                    &gUefiShellFileGuid,
                    EFI_SECTION_PE32,
                    0,
                    &Buffer,
                    &Size,
                    &AuthenticationStatus
                    );
    if (EFI_ERROR (Status)) {
      //
      // Skip if no shell file in the FV
      //
      continue;
    } else {
      //
      // Found the shell
      //
      break;
    }
  }

  if (EFI_ERROR (Status)) {
    //
    // No shell present
    //
    if (FvHandleCount) {
      FreePool (FvHandleBuffer);
    }
    return NULL;
  }
  //
  // Build the shell boot option
  //
  DevicePath = DevicePathFromHandle (FvHandleBuffer[Index]);

  if (FvHandleCount) {
    FreePool (FvHandleBuffer);
  }

  return DevicePath;
}

VOID
ChangeFirstBootDeviceToShell (
  VOID
  );

EFI_STATUS
CreateFvBootOption (
  EFI_GUID                     *FileGuid,
  CHAR16                       *Description,
  EFI_BOOT_MANAGER_LOAD_OPTION *BootOption,
  UINT32                       Attributes,
  UINT8                        *OptionalData,    OPTIONAL
  UINT32                       OptionalDataSize
  )
{
  EFI_STATUS                         Status;
  EFI_DEVICE_PATH_PROTOCOL           *DevicePath;
  EFI_LOADED_IMAGE_PROTOCOL          *LoadedImage;
  MEDIA_FW_VOL_FILEPATH_DEVICE_PATH  FileNode;
  EFI_FIRMWARE_VOLUME2_PROTOCOL      *Fv;
  UINT32                             AuthenticationStatus;
  VOID                               *Buffer;
  UINTN                              Size;

  if ((BootOption == NULL) || (FileGuid == NULL) || (Description == NULL)) {
    return EFI_INVALID_PARAMETER;
  }

  EfiInitializeFwVolDevicepathNode (&FileNode, FileGuid);

  if (!CompareGuid (&gUefiShellFileGuid, FileGuid)) {
    Status = gBS->HandleProtocol (
                    gImageHandle,
                    &gEfiLoadedImageProtocolGuid,
                    (VOID **) &LoadedImage
                    );
    if (!EFI_ERROR (Status)) {
      Status = gBS->HandleProtocol (
                      LoadedImage->DeviceHandle,
                      &gEfiFirmwareVolume2ProtocolGuid,
                      (VOID **) &Fv
                      );
      if (!EFI_ERROR (Status)) {
        Buffer  = NULL;
        Size    = 0;
        Status  = Fv->ReadSection (
                        Fv,
                        FileGuid,
                        EFI_SECTION_PE32,
                        0,
                        &Buffer,
                        &Size,
                        &AuthenticationStatus
                        );
        if (Buffer != NULL) {
          FreePool (Buffer);
        }
      }
    }
    if (EFI_ERROR (Status)) {
      return EFI_NOT_FOUND;
    }

    DevicePath = AppendDevicePathNode (
                   DevicePathFromHandle (LoadedImage->DeviceHandle),
                   (EFI_DEVICE_PATH_PROTOCOL *) &FileNode
                   );
  } else {
    DevicePath = AppendDevicePathNode (
                   BdsCreateShellDevicePath (),
                   (EFI_DEVICE_PATH_PROTOCOL *) &FileNode
                   );
    if (DevicePath == NULL) {
      DEBUG ((DEBUG_ERROR, "AppendDevicePathNode returns DevicePath NULL\n"));
      return EFI_NOT_FOUND;
    }
  }

  Status = EfiBootManagerInitializeLoadOption (
             BootOption,
             LoadOptionNumberUnassigned,
             LoadOptionTypeBoot,
             Attributes,
             Description,
             DevicePath,
             OptionalData,
             OptionalDataSize
             );
  FreePool (DevicePath);
  return Status;
}

/**
  Return the index of the load option in the load option array.

  The function consider two load options are equal when the
  OptionType, Attributes, Description, FilePath and OptionalData are equal.

  @param Key    Pointer to the load option to be found.
  @param Array  Pointer to the array of load options to be found.
  @param Count  Number of entries in the Array.

  @retval -1          Key wasn't found in the Array.
  @retval 0 ~ Count-1 The index of the Key in the Array.
**/
INTN
PlatformFindLoadOption (
  IN CONST EFI_BOOT_MANAGER_LOAD_OPTION *Key,
  IN CONST EFI_BOOT_MANAGER_LOAD_OPTION *Array,
  IN UINTN                              Count
  )
{
  UINTN                             Index;

  for (Index = 0; Index < Count; Index++) {
    if ((Key->OptionType == Array[Index].OptionType) &&
        (Key->Attributes == Array[Index].Attributes) &&
        (StrCmp (Key->Description, Array[Index].Description) == 0) &&
        (CompareMem (Key->FilePath, Array[Index].FilePath, GetDevicePathSize (Key->FilePath)) == 0) &&
        (Key->OptionalDataSize == Array[Index].OptionalDataSize) &&
        (CompareMem (Key->OptionalData, Array[Index].OptionalData, Key->OptionalDataSize) == 0)) {
      return (INTN) Index;
    }
  }

  return -1;
}

UINTN
RegisterFvBootOption (
  EFI_GUID                         *FileGuid,
  CHAR16                           *Description,
  UINTN                            Position,
  UINT32                           Attributes,
  UINT8                            *OptionalData,    OPTIONAL
  UINT32                           OptionalDataSize
  )
{
  EFI_STATUS                       Status;
  UINTN                            OptionIndex;
  EFI_BOOT_MANAGER_LOAD_OPTION     NewOption;
  EFI_BOOT_MANAGER_LOAD_OPTION     *BootOptions;
  UINTN                            BootOptionCount;

  NewOption.OptionNumber = LoadOptionNumberUnassigned;
  Status = CreateFvBootOption (FileGuid, Description, &NewOption, Attributes, OptionalData, OptionalDataSize);
  if (!EFI_ERROR (Status)) {
    BootOptions = EfiBootManagerGetLoadOptions (&BootOptionCount, LoadOptionTypeBoot);

    OptionIndex = PlatformFindLoadOption (&NewOption, BootOptions, BootOptionCount);

    if (OptionIndex == -1) {
      Status = EfiBootManagerAddLoadOptionVariable (&NewOption, Position);
      ASSERT_EFI_ERROR (Status);
    } else {
      NewOption.OptionNumber = BootOptions[OptionIndex].OptionNumber;
    }
    EfiBootManagerFreeLoadOption (&NewOption);
    EfiBootManagerFreeLoadOptions (BootOptions, BootOptionCount);
  }

  return NewOption.OptionNumber;
}

VOID
BootUi (
  VOID
  )
{
  EFI_STATUS                   Status;
  EFI_BOOT_MANAGER_LOAD_OPTION BootOption;

  Status = EfiBootManagerGetBootManagerMenu (&BootOption);
  if (!EFI_ERROR (Status)) {
    EfiBootManagerBoot (&BootOption);
    EfiBootManagerFreeLoadOption (&BootOption);
  }
}

VOID
EFIAPI
PlatformBootManagerWaitCallback (
  UINT16          TimeoutRemain
  )
{
  CHAR16                        *TmpStr;
  EFI_GRAPHICS_OUTPUT_BLT_PIXEL Foreground;
  EFI_GRAPHICS_OUTPUT_BLT_PIXEL Background;
  EFI_GRAPHICS_OUTPUT_BLT_PIXEL Color;
  UINT16                        Timeout;
  EFI_STATUS                    Status;
  EFI_SIMPLE_TEXT_INPUT_EX_PROTOCOL  *TxtInEx;
  EFI_KEY_DATA                  KeyData;
  BOOLEAN                       PausePressed;

  Timeout = PcdGet16 (PcdPlatformBootTimeOut);
  if (GetDisplayBootMode () == NORMAL_BOOT) {
    //
    // Show progress
    //
    SetMem (&Foreground, sizeof (EFI_GRAPHICS_OUTPUT_BLT_PIXEL), 0xff);
    SetMem (&Background, sizeof (EFI_GRAPHICS_OUTPUT_BLT_PIXEL), 0x0);
    SetMem (&Color,      sizeof (EFI_GRAPHICS_OUTPUT_BLT_PIXEL), 0xff);

    TmpStr = GetStringById (STRING_TOKEN (STR_START_BOOT_OPTION));
    if (TmpStr != NULL) {
      BootLogoUpdateProgress (
        Foreground,
        Background,
        TmpStr,
        Color,
        (Timeout - TimeoutRemain) * 100 / Timeout,
        0
        );
      FreePool (TmpStr);
    }
  }

  //
  // Pause on PAUSE key
  //
  Status = gBS->HandleProtocol (gST->ConsoleInHandle, &gEfiSimpleTextInputExProtocolGuid, (VOID **) &TxtInEx);
  ASSERT_EFI_ERROR (Status);

  PausePressed = FALSE;

  while (TRUE) {
    Status = TxtInEx->ReadKeyStrokeEx (TxtInEx, &KeyData);
    if (EFI_ERROR (Status)) {
      break;
    }

    if (KeyData.Key.ScanCode == SCAN_PAUSE) {
      PausePressed = TRUE;
      break;
    }
  }

  //
  // Loop until non-PAUSE key pressed
  //
  while (PausePressed) {
    Status = TxtInEx->ReadKeyStrokeEx (TxtInEx, &KeyData);
    if (!EFI_ERROR (Status)) {
      DEBUG ((
        DEBUG_INFO, "[PauseCallback] %x/%x %x/%x\n",
        KeyData.Key.ScanCode, KeyData.Key.UnicodeChar,
        KeyData.KeyState.KeyShiftState, KeyData.KeyState.KeyToggleState
        ));
      PausePressed = (BOOLEAN) (KeyData.Key.ScanCode == SCAN_PAUSE);
    }
  }
}

/**
  Check if the boot option is a EFI network boot option.

  @param  Option         The boot option need to be processed.

  @retval TRUE           It is a EFI network boot option.
  @retval FALSE          It is not a EFI network boot option.

**/
BOOLEAN
IsEfiNetWorkBootOption (
  IN  EFI_BOOT_MANAGER_LOAD_OPTION     *Option
  )
{
  EFI_DEVICE_PATH_PROTOCOL   *Node;

  for (Node = Option->FilePath; !IsDevicePathEndType (Node); Node = NextDevicePathNode (Node)) {
    if (DevicePathType (Node) == MESSAGING_DEVICE_PATH) {
      switch (DevicePathSubType (Node)) {
      case MSG_MAC_ADDR_DP:
      case MSG_VLAN_DP:
      case MSG_IPv4_DP:
      case MSG_IPv6_DP:
        return TRUE;
      }
    }
  }
  return FALSE;
}

/**
  This funciton uses to get first PXE boot option from boot option list.
  The caller is responsible for free the data in the found option.
**/
EFI_STATUS
GetPxeOption (
  UINT16                           *OptionNumber
  )
{
  UINTN                            Index;
  UINTN                            BootOptionCount;
  EFI_BOOT_MANAGER_LOAD_OPTION     *BootOption;

  ASSERT (OptionNumber != NULL);
  if (OptionNumber == NULL) {
    return EFI_INVALID_PARAMETER;
  }

  //
  // Search PXE device
  //
  BootOption = EfiBootManagerGetLoadOptions (&BootOptionCount, LoadOptionTypeBoot);
  for (Index = 0; Index < BootOptionCount; Index++) {
    if (IsEfiNetWorkBootOption (&BootOption[Index])) {
      *OptionNumber = (UINT16) BootOption[Index].OptionNumber;
      break;
    }
  }
  EfiBootManagerFreeLoadOptions (BootOption, BootOptionCount);

  return (Index == BootOptionCount) ? EFI_NOT_FOUND : EFI_SUCCESS;
}

#define INTERNAL_UEFI_SHELL_NAME      L"Internal UEFI Shell"
#define UEFI_HARD_DRIVE_NAME          L"UEFI Hard Drive"
#define FFU_LOADER_NAME               L"Microsoft FFU Image Flashing Mode"

/**
 Delete the FFU Loader boot option from the Boot List.

**/
VOID
DeleteFfuLoaderBootOption (
  VOID
  )
{
  EFI_BOOT_MANAGER_LOAD_OPTION    *BootOptions;
  UINTN                           BootOptionCount;
  UINT8                           IndexI;

  BootOptions = NULL;

  //
  // Get the Boot options
  //
  BootOptions = EfiBootManagerGetLoadOptions (&BootOptionCount, LoadOptionTypeBoot);
  //
  // Find and delete the FFU Option in the boot list
  //
  for (IndexI=0; IndexI < BootOptionCount; IndexI++) {
    if (StrCmp(BootOptions[IndexI].Description, FFU_LOADER_NAME) == 0) {
      DEBUG ((DEBUG_INFO,"FFU: Deleting the FFU Loader Option.\n"));
      EfiBootManagerDeleteLoadOptionVariable (BootOptions[IndexI].OptionNumber, LoadOptionTypeBoot);
      break;
    }
  }
  EfiBootManagerFreeLoadOptions (BootOptions, BootOptionCount);
}

VOID
RegisterDefaultBootOption (
  VOID
  )
{
  EFI_LOADED_IMAGE_PROTOCOL          *LoadedImage;
  MEDIA_FW_VOL_FILEPATH_DEVICE_PATH  FileNode;
  UINT16                             *ShellDataPtr;
  UINT16                             ShellData[MAX_SHELL_INPUT_SIZE];
  UINT32                             ShellDataSize;
  VOID                               *UsbFunIoProtocol;
  EFI_STATUS                         Status;
  BOOLEAN                            UefiShellEnabled;


  //
  // Check BootState variable, NULL means it's the first boot after reflashing
  // Shell
  UefiShellEnabled = PcdGetBool (PcdUefiShellEnable);
  if (UefiShellEnabled) {
    if (!IsBootStatePresent () || !IsShellInBootList ()) {
      ShellDataPtr = ShellData;
      SetMemN( ShellDataPtr, MAX_SHELL_INPUT_SIZE, 0);
      StrCpyS( ShellDataPtr ,MAX_SHELL_INPUT_SIZE / 2, L"");
      ShellDataSize = (UINT32) StrSize (ShellDataPtr);

      // FvApp contains EFI Shell. Enabling FvApp automatically lets FvApp be processed if FvOptional has been installed
      // and the FVB protocol has been installed.
      // Try registering the boot option first. If fails due to FvApp not being processed, process.
      EnableFvApp ();
      mEfiShellOptionNumber = RegisterFvBootOption (&gUefiShellFileGuid, INTERNAL_UEFI_SHELL_NAME,  (UINTN) -1, LOAD_OPTION_ACTIVE, (UINT8 *)ShellDataPtr, ShellDataSize);
      if (mEfiShellOptionNumber == LoadOptionNumberUnassigned) {
        Status = ProcessFvApp ();
        if (!EFI_ERROR (Status)) {
          mEfiShellOptionNumber = RegisterFvBootOption (&gUefiShellFileGuid, INTERNAL_UEFI_SHELL_NAME,  (UINTN) -1, LOAD_OPTION_ACTIVE, (UINT8 *)ShellDataPtr, ShellDataSize);
          if (mEfiShellOptionNumber == LoadOptionNumberUnassigned) {
            DEBUG ((DEBUG_ERROR, "RegisterDefaultBootOption : mEfiShellOptionNumber (%d) should not be same to LoadOptionNumberUnassigned(%d).\n", mEfiShellOptionNumber, LoadOptionNumberUnassigned));
          } else {
            DEBUG ((DEBUG_INFO, "RegisterDefaultBootOption : EFI Shell is registered.\n"));
          }
        } else {
          DEBUG ((DEBUG_ERROR, "RegisterDefaultBootOption : FvOptional process failure.\n"));
        }
      } else {
        DEBUG ((DEBUG_INFO, "RegisterDefaultBootOption : EFI Shell is registered.\n"));
      }
    }
  }

  //
  // Boot Menu
  //
  mBootMenuOptionNumber = RegisterFvBootOption (&gBootMenuFileGuid, L"Boot Device List",   (UINTN) -1, LOAD_OPTION_CATEGORY_APP | LOAD_OPTION_ACTIVE | LOAD_OPTION_HIDDEN, NULL, 0);

  if (mBootMenuOptionNumber == LoadOptionNumberUnassigned) {
    DEBUG ((DEBUG_ERROR, "BootMenuOptionNumber (%d) should not be same to LoadOptionNumberUnassigned(%d).\n", mBootMenuOptionNumber, LoadOptionNumberUnassigned));
  }

  //
  // Boot Manager Menu
  //
  EfiInitializeFwVolDevicepathNode (&FileNode, &gUiFileGuid);

  gBS->HandleProtocol (
         gImageHandle,
         &gEfiLoadedImageProtocolGuid,
         (VOID **) &LoadedImage
         );
  AppendDevicePathNode (DevicePathFromHandle (LoadedImage->DeviceHandle), (EFI_DEVICE_PATH_PROTOCOL *) &FileNode);
  //
  // Add FFU Loader boot option if UsbFnEnable setup is enabled & USB-FNIO Protocol is installed
  //
  Status = gBS->LocateProtocol (&gUsbFnIoProtocolGuid, NULL, (VOID **) &UsbFunIoProtocol);

  if (mSystemConfiguration.UsbFnEnable && (Status == EFI_SUCCESS)) {
    DEBUG ((DEBUG_INFO, "FFU: Locate UsbFunIoProtocol = %r. Adding FFU Loader as Boot Option.\n", Status));
    RegisterFvBootOption(PcdGetPtr(PcdFfuLoaderFile), FFU_LOADER_NAME, (UINTN)-1, LOAD_OPTION_ACTIVE, NULL, 0);
  } else {
    DeleteFfuLoaderBootOption();
  }
}

VOID
RegisterBootOptionHotkey (
  UINT16                       OptionNumber,
  EFI_INPUT_KEY                *Key,
  BOOLEAN                      Add
  )
{
  EFI_STATUS                   Status;
  FW_BOOT_MEDIA_TYPE           FwBootMediaType;
  FwBootMediaType = FwBootMediaMax;
  if (!Add) {
    //
    // No enter hotkey when force to setup or there is no boot option
    //
    Status = EfiBootManagerDeleteKeyOptionVariable (NULL, 0, Key, NULL);
    ASSERT (Status == EFI_SUCCESS || Status == EFI_NOT_FOUND);
  } else {
    Status = GetFirmwareBootMediaType (&FwBootMediaType);
    if (Status != EFI_SUCCESS) {
      DEBUG ((DEBUG_ERROR, "FirmwareBootMediaLib Error: Cannot Get Firmware Boot Media Type\n"));
      ASSERT_EFI_ERROR (Status);
    } else {
      //
      // Register enter hotkey for the first boot option
      //
      Status = EfiBootManagerAddKeyOptionVariable (NULL, OptionNumber, 0, Key,NULL);
      if (FwBootMediaType == FwBootMediaSpi)  {
        // BFX boot: Need to debug this assert being hit in UEFI variable emulation mode.
        ASSERT (Status == EFI_SUCCESS || Status == EFI_ALREADY_STARTED);
      }
    }
  }
}

EFI_STATUS
EFIAPI
DetectKeypressCallback (
  IN EFI_KEY_DATA     *KeyData
)
{
  if (!mHotKeypressed && HotKeyEvent != NULL) {
    gBS->SignalEvent(HotKeyEvent);
    mHotKeypressed = TRUE;
  } else {
    DEBUG ((DEBUG_INFO, "No hotkey events to be handled until the current hotkey process completes.\n"));
  }

  return EFI_SUCCESS;
}

VOID
EFIAPI
ProcessFirmwareVolumeCallback (
  IN EFI_EVENT    Event,
  IN VOID         *Context
)
{
  EFI_STATUS               Status;
  EFI_TPL                  CurTpl;
#if FixedPcdGetBool(PcdCapsuleEnable) == 1
  EFI_HOB_GUID_TYPE        *GuidHob;

  GuidHob = NULL;
  GuidHob = GetFirstGuidHob (&gSysFwUpdateProgressGuid);
  if ((GuidHob != NULL) && \
      (((SYSTEM_FIRMWARE_UPDATE_PROGRESS *) GET_GUID_HOB_DATA (GuidHob))->Component == UpdatingBios)) {
    mHotKeypressed = FALSE;
    return;
  }
#endif
  if (!mHotKeypressed) {
    return;
  }
  DEBUG ((DEBUG_INFO, "ProcessFirmwareVolumeCallback : Hotkey callback invoked\n"));

  if (!mDecompressFvOptional) {
    DEBUG ((DEBUG_INFO, "ProcessFirmwareVolumeCallback : Enable and process FvApp\n"));
    Status = EnableFvApp ();
    if (EFI_ERROR (Status)) {
      DEBUG ((DEBUG_ERROR, "ProcessFirmwareVolumeCallback : FvApp enable failure\n"));
      ASSERT_EFI_ERROR (Status);
      mHotKeypressed = FALSE;
    return;
  }

    CurTpl = EfiGetCurrentTpl ();
    gBS->RestoreTPL (TPL_APPLICATION);
    Status = ProcessFvApp ();
    if (EFI_ERROR (Status)) {
      DEBUG ((DEBUG_ERROR, "ProcessFirmwareVolumeCallback : FvApp process failure\n"));
      ASSERT_EFI_ERROR (Status);
      mHotKeypressed = FALSE;
      gBS->RaiseTPL (CurTpl);
    return;
  }
    gBS->RaiseTPL (CurTpl);
  }

  mHotKeypressed = FALSE;
  return;
}

VOID
EFIAPI
StopProcessFirmwareVolume (
  IN EFI_EVENT    Event,
  IN VOID         *Context
  )
{
  EFI_STATUS                            Status;
  UINTN                                 Index;
  EFI_HANDLE                            *Handles;
  UINTN                                 HandleCount;
  EFI_SIMPLE_TEXT_INPUT_EX_PROTOCOL     *TxtInEx;
  VOID                                  *NotifyHandle;
  VOID                                  *NotifyHandle2;
  EFI_KEY_DATA                  F2;
  EFI_KEY_DATA                  F7;
  F2.Key.ScanCode    = SCAN_F2;
  F2.Key.UnicodeChar = CHAR_NULL;
  F2.KeyState.KeyShiftState = EFI_SHIFT_STATE_VALID;
  F2.KeyState.KeyToggleState = 0;

  F7.Key.ScanCode    = SCAN_F7;
  F7.Key.UnicodeChar = CHAR_NULL;
  F7.KeyState.KeyShiftState = EFI_SHIFT_STATE_VALID;
  F7.KeyState.KeyToggleState = 0;
  gBS->CloseEvent (Event);

  gBS->LocateHandleBuffer (
          ByProtocol,
          &gEfiSimpleTextInputExProtocolGuid,
          NULL,
          &HandleCount,
          &Handles
          );

  for (Index = 0; Index < HandleCount; Index++) {
    Status = gBS->HandleProtocol (Handles[Index], &gEfiSimpleTextInputExProtocolGuid, (VOID **) &TxtInEx);
    ASSERT_EFI_ERROR (Status);

    Status = TxtInEx->RegisterKeyNotify (
                        TxtInEx,
                        &F2,
                        DetectKeypressCallback,
                        &NotifyHandle
                        );
    if (!EFI_ERROR (Status)) {
      Status = TxtInEx->UnregisterKeyNotify (TxtInEx, NotifyHandle);
      DEBUG ((DEBUG_INFO, "UnregisterKeyNotify: %04x/%04x %r\n", F2.Key.ScanCode, F2.Key.UnicodeChar, Status));
    }

    Status = TxtInEx->RegisterKeyNotify (
                        TxtInEx,
                        &F7,
                        DetectKeypressCallback,
                        &NotifyHandle2
                        );
    if (!EFI_ERROR (Status)) {
      Status = TxtInEx->UnregisterKeyNotify (TxtInEx, NotifyHandle2);
      DEBUG ((DEBUG_INFO, "UnregisterKeyNotify: %04x/%04x %r\n", F7.Key.ScanCode, F7.Key.UnicodeChar, Status));
    }
  }
}

/**
  This function is called after all the boot options are enumerated and ordered properly.
**/
VOID
RegisterStaticHotkey (
  VOID
  )
{

  EFI_INPUT_KEY                 Enter;
  EFI_KEY_DATA                  F2;
  EFI_KEY_DATA                  F7;
  BOOLEAN                       EnterSetup;
  EFI_STATUS                    Status;
  EFI_BOOT_MANAGER_LOAD_OPTION  BootOption;
  VOID                          *NotifyHandle;
  VOID                          *NotifyHandle2;
  EFI_SIMPLE_TEXT_INPUT_EX_PROTOCOL  *TxtInEx;
  EFI_HANDLE                    *Handles;
  UINTN                         HandleCount;
  UINTN                         HandleIndex;
  EFI_EVENT                     Event;

  EnterSetup = AmtEnterSetup ();

  //
  // [Enter]
  //
  mContinueBoot = !EnterSetup;
  if (mContinueBoot) {
    Enter.ScanCode    = SCAN_NULL;
    Enter.UnicodeChar = CHAR_CARRIAGE_RETURN;
    EfiBootManagerRegisterContinueKeyOption (0, &Enter, NULL);
  }

  //
  // [F2]/[F7]
  //
  F2.Key.ScanCode    = SCAN_F2;
  F2.Key.UnicodeChar = CHAR_NULL;
  F2.KeyState.KeyShiftState = EFI_SHIFT_STATE_VALID;
  F2.KeyState.KeyToggleState = 0;
  Status = EfiBootManagerGetBootManagerMenu (&BootOption);
  ASSERT_EFI_ERROR (Status);
  RegisterBootOptionHotkey ((UINT16) BootOption.OptionNumber, &F2.Key, TRUE);
  EfiBootManagerFreeLoadOption (&BootOption);

  F7.Key.ScanCode    = SCAN_F7;
  F7.Key.UnicodeChar = CHAR_NULL;
  F7.KeyState.KeyShiftState = EFI_SHIFT_STATE_VALID;
  F7.KeyState.KeyToggleState = 0;
  mBootMenuBoot  = !EnterSetup;
  RegisterBootOptionHotkey ((UINT16) mBootMenuOptionNumber, &F7.Key, mBootMenuBoot);

  //
  // Register ProcessFirmwareVolumeCallback function of F2 & F7
  //
    DEBUG ((DEBUG_INFO, "Register F2 and F7\n"));
    gBS->LocateHandleBuffer (
              ByProtocol,
              &gEfiSimpleTextInputExProtocolGuid,
              NULL,
              &HandleCount,
              &Handles
              );

    for (HandleIndex = 0; HandleIndex < HandleCount; HandleIndex++) {
      Status = gBS->HandleProtocol (Handles[HandleIndex], &gEfiSimpleTextInputExProtocolGuid, (VOID **) &TxtInEx);
      ASSERT_EFI_ERROR (Status);
    TxtInEx->RegisterKeyNotify (
                    TxtInEx,
                    &F2,
                    DetectKeypressCallback,
                    &NotifyHandle
                    );

    TxtInEx->RegisterKeyNotify (
                    TxtInEx,
                    &F7,
                    DetectKeypressCallback,
                    &NotifyHandle2
                    );
    }

  gBS->CreateEvent (
                EVT_NOTIFY_SIGNAL,
                TPL_CALLBACK,
                ProcessFirmwareVolumeCallback,
                NULL,
                &HotKeyEvent
                );

  EfiCreateEventReadyToBootEx (
                TPL_CALLBACK,
                StopProcessFirmwareVolume,
                NULL,
                &Event
                );
}

/**
  Convert KeyShiftState in EFI_KEY_DATA structure to EFI boot manager modifier.

  @param  Keydata        Pointer to EFI_KEY_DATA structure.

  @retval UINT32         EFI boot manager modifier.

**/
UINT32
GetEfiBootManagerModifier (
  EFI_KEY_DATA         *Keydata
  )
{
  UINT32        Modifier;

  ASSERT (Keydata != NULL);
  if (Keydata == NULL) {
    return 0;
  }

  Modifier = 0;

  if ((Keydata->KeyState.KeyShiftState & EFI_SHIFT_STATE_VALID) == 0) {
    return Modifier;
  }

  if ((Keydata->KeyState.KeyShiftState & (EFI_RIGHT_SHIFT_PRESSED | EFI_LEFT_SHIFT_PRESSED)) != 0) {
    Modifier |= EFI_BOOT_MANAGER_SHIFT_PRESSED;
  }
  if ((Keydata->KeyState.KeyShiftState & (EFI_RIGHT_CONTROL_PRESSED | EFI_LEFT_CONTROL_PRESSED)) != 0) {
    Modifier |= EFI_BOOT_MANAGER_CONTROL_PRESSED;
  }
  if ((Keydata->KeyState.KeyShiftState & (EFI_RIGHT_ALT_PRESSED | EFI_LEFT_ALT_PRESSED)) != 0) {
    Modifier |= EFI_BOOT_MANAGER_ALT_PRESSED;
  }
  if ((Keydata->KeyState.KeyShiftState & (EFI_RIGHT_LOGO_PRESSED | EFI_LEFT_LOGO_PRESSED)) != 0) {
    Modifier |= EFI_BOOT_MANAGER_LOGO_PRESSED;
  }
  if ((Keydata->KeyState.KeyShiftState & EFI_MENU_KEY_PRESSED) != 0) {
    Modifier |= EFI_BOOT_MANAGER_MENU_KEY_PRESSED;
  }
  if ((Keydata->KeyState.KeyShiftState & EFI_SYS_REQ_PRESSED) != 0) {
    Modifier |= EFI_BOOT_MANAGER_SYS_REQ_PRESSED;
  }

  return Modifier;
}

VOID
RegisterDynamicHotkey (
  VOID
  )
{
  EFI_INPUT_KEY                 F12;
  EFI_STATUS                    Status;
  UINT16                        PxeOptionNumber;
  //
  // [F12]
  //
  F12.ScanCode    = SCAN_F12;
  F12.UnicodeChar = CHAR_NULL;

  Status = GetPxeOption (&PxeOptionNumber);
  if (!EFI_ERROR (Status)) {
    mPxeBoot = TRUE;
  }

  if (AmtEnterSetup ()) {
    mPxeBoot = FALSE;
  }

  RegisterBootOptionHotkey (PxeOptionNumber, &F12, mPxeBoot);
}

/**
  Print the boot prompt.
**/
VOID
PrintBootPrompt (
  VOID
  )
{
  EFI_STRING      String;
  EFI_STATUS      Status;
  EFI_INPUT_KEY   Key;

  if ((BOOLEAN) (mSystemConfiguration.BootFirstToShell)) {
    //
    // Modify the boot order and move the Shell to the first in the boot option list
    //
    ChangeFirstBootDeviceToShell();
    Print (L"\nBoot First To Shell enabled...\n");
  }

  if (FastBootEnabled () && !mAnyKeypressed) {
    Status = gST->ConIn->ReadKeyStroke (gST->ConIn, &Key);
    if (!EFI_ERROR (Status)) {
      mAnyKeypressed = TRUE;
    }
  }
  if (GetDisplayBootMode () == NORMAL_BOOT) {
    Print (L"\n\n");
  }

  if (mContinueBoot && GetDisplayBootMode () == NORMAL_BOOT) {
    Print (L"     Press [Enter]  to directly boot.\n");
  }

  if (GetDisplayBootMode () == NORMAL_BOOT) {
    Print (L"     Press [F2]     to enter setup and select boot options.\n");
  }
  if (mBootMenuBoot && GetDisplayBootMode () == NORMAL_BOOT) {
    Print (L"     Press [F7]     to show boot menu options.\n");
  }
  if (mPxeBoot && GetDisplayBootMode () == NORMAL_BOOT) {
    Print (L"     Press [F12]    to PXE boot.\n");
  }

  if (GetDisplayBootMode () == NORMAL_BOOT) {
    do {
      Status = AmtGetSetupPrompt (&String);
      if (!EFI_ERROR (Status) && (String != NULL)) {
        Print (L"     %s\n", String);
      }
    } while (!EFI_ERROR (Status));
  }

  if (GetDisplayBootMode () == NORMAL_BOOT) {
    Print (L"     Press [Pause]  to pause and <any key> to continue booting.\n");
  }

}

UINT8
BootOptionType (
  IN EFI_DEVICE_PATH_PROTOCOL   *DevicePath
  )
{
  EFI_DEVICE_PATH_PROTOCOL      *Node;
  EFI_DEVICE_PATH_PROTOCOL      *NextNode;

  for (Node = DevicePath; !IsDevicePathEndType (Node); Node = NextDevicePathNode (Node)) {
    if (DevicePathType (Node) == MESSAGING_DEVICE_PATH) {
      //
      // Make sure the device path points to the driver device.
      //
      NextNode = NextDevicePathNode (Node);
      if (DevicePathSubType(NextNode) == MSG_DEVICE_LOGICAL_UNIT_DP) {
        //
        // if the next node type is Device Logical Unit, which specify the Logical Unit Number (LUN),
        // skip it
        //
        NextNode = NextDevicePathNode (NextNode);
      }
      if (IsDevicePathEndType (NextNode)) {
        if ((DevicePathType (Node) == MESSAGING_DEVICE_PATH)) {
          return DevicePathSubType (Node);
        } else {
          return MSG_SATA_DP;
        }
      }
    }
  }

  return (UINT8) -1;
}

/**
  Returns the priority number.
  OptionType                     EFI
  ------------------------------------
  PXE                             2
  DVD                             4
  USB                             6
  HDD                             8
  EFI Shell                       9
  Others                          99

  @param BootOption
**/
UINTN
BootOptionPriority (
  CONST EFI_BOOT_MANAGER_LOAD_OPTION *BootOption
  )
{

  //
  // EFI boot options
  //
  switch (BootOptionType (BootOption->FilePath)) {
  case MSG_MAC_ADDR_DP:
  case MSG_VLAN_DP:
  case MSG_IPv4_DP:
  case MSG_IPv6_DP:
    return 2;

  case MSG_SATA_DP:
  case MSG_ATAPI_DP:
  case MSG_UFS_DP:
  case MSG_NVME_NAMESPACE_DP:
    return 4;

  case MSG_USB_DP:
    return 6;

  }
  if (StrCmp (BootOption->Description, INTERNAL_UEFI_SHELL_NAME) == 0) {
    return 9;
  }
  if (StrCmp (BootOption->Description, UEFI_HARD_DRIVE_NAME) == 0) {
    return 8;
  }
  return 99;
}

INTN
EFIAPI
CompareBootOption (
  CONST VOID  *Left,
  CONST VOID  *Right
  )
{
  return BootOptionPriority ((EFI_BOOT_MANAGER_LOAD_OPTION *) Left) -
         BootOptionPriority ((EFI_BOOT_MANAGER_LOAD_OPTION *) Right);
}

/**
  This function is called each when Manufacturing mode is enabled.

  If first bootdevice is Shell, No changes will be done.
  If first bootdevice is not shell,then shell is moved to first in boot
  list and first device to shell's previous position in the boot list.

**/
VOID
ChangeFirstBootDeviceToShell (
  VOID
  )
{
  EFI_BOOT_MANAGER_LOAD_OPTION    *BootOption;
  EFI_BOOT_MANAGER_LOAD_OPTION    TempOption;
  UINTN                           BootOptionCount;
  UINT8                           FirstBootdevice;
  UINT8                           IndexI,IndexJ;
  UINT16                          *OptionOrder;

  BootOption = NULL;
  FirstBootdevice = 0;
  //
  // Get the Boot options
  //
  BootOption = EfiBootManagerGetLoadOptions (&BootOptionCount, LoadOptionTypeBoot);
  //
  // If first boot device is already shell, Dont need to change the bootlist
  //
  if(StrCmp(BootOption[FirstBootdevice].Description, INTERNAL_UEFI_SHELL_NAME)==0) {
    return ;
  }
  //
  // Find the UEFI shell in the boot list
  //
  for(IndexI=1;IndexI<BootOptionCount;IndexI++) {
    //
    // If find the shell, change the boot order
    //
    if(StrCmp(BootOption[IndexI].Description, INTERNAL_UEFI_SHELL_NAME)==0) {
      //
      // Swap the Boot Order
      //
      CopyMem (&TempOption, &BootOption[IndexI], sizeof (EFI_BOOT_MANAGER_LOAD_OPTION));
      CopyMem (&BootOption[IndexI], &BootOption[FirstBootdevice], sizeof (EFI_BOOT_MANAGER_LOAD_OPTION));
      CopyMem (&BootOption[FirstBootdevice], &TempOption, sizeof (EFI_BOOT_MANAGER_LOAD_OPTION));
      //
      // Update the new BootOrder
      //
      OptionOrder = AllocatePool ((BootOptionCount ) * sizeof (UINT16));
      ASSERT (OptionOrder != NULL);
      if (OptionOrder == NULL) return;
      for (IndexJ = 0; IndexJ < BootOptionCount; IndexJ++) {
        OptionOrder[IndexJ ] = (UINT16) BootOption[IndexJ].OptionNumber;
      }

      gRT->SetVariable (
             L"BootOrder",
             &gEfiGlobalVariableGuid,
             EFI_VARIABLE_BOOTSERVICE_ACCESS | EFI_VARIABLE_RUNTIME_ACCESS | EFI_VARIABLE_NON_VOLATILE,
             (BootOptionCount)* sizeof (UINT16),
             OptionOrder
             );
      break;
    }
  }
}

/**
  Return if Shell is in Boot List.

  @retval TRUE  Shell is in Boot List.
  @retval FALSE Shell is in NOT Boot List.
**/
BOOLEAN
IsShellInBootList (
  VOID
  )
{
  EFI_BOOT_MANAGER_LOAD_OPTION    *BootOption;
  UINTN                           BootOptionCount;
  UINT8                           IndexI;

  BootOption = NULL;

  //
  // Get the Boot options
  //
  BootOption = EfiBootManagerGetLoadOptions (&BootOptionCount, LoadOptionTypeBoot);
  //
  // Find the UEFI shell in the boot list
  //
  for(IndexI=0;IndexI<BootOptionCount;IndexI++) {
    if(StrCmp(BootOption[IndexI].Description, INTERNAL_UEFI_SHELL_NAME)==0) {
      return TRUE;
    }
  }
  return FALSE;
}
