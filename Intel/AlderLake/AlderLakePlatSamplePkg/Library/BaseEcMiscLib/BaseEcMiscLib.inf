## @file
#
#******************************************************************************
#* Copyright (c) 2020, Insyde Software Corp. All Rights Reserved.
#*
#* You may not reproduce, distribute, publish, display, perform, modify, adapt,
#* transmit, broadcast, present, recite, release, license or otherwise exploit
#* any part of this publication in any form, by any means, without the prior
#* written permission of Insyde Software Corporation.
#*
#******************************************************************************
### @file
# Component information file for BaseEcMisc library
#
#@copyright
#  INTEL CONFIDENTIAL
#  Copyright 2014 - 2020 Intel Corporation.
#
#  The source code contained or described herein and all documents related to the
#  source code ("Material") are owned by Intel Corporation or its suppliers or
#  licensors. Title to the Material remains with Intel Corporation or its suppliers
#  and licensors. The Material may contain trade secrets and proprietary and
#  confidential information of Intel Corporation and its suppliers and licensors,
#  and is protected by worldwide copyright and trade secret laws and treaty
#  provisions. No part of the Material may be used, copied, reproduced, modified,
#  published, uploaded, posted, transmitted, distributed, or disclosed in any way
#  without Intel's prior express written permission.
#
#  No license under any patent, copyright, trade secret or other intellectual
#  property right is granted to or conferred upon you by disclosure or delivery
#  of the Materials, either expressly, by implication, inducement, estoppel or
#  otherwise. Any license under such intellectual property rights must be
#  express and approved by Intel in writing.
#
#  Unless otherwise agreed by Intel in writing, you may not remove or alter
#  this notice or any other notice embedded in Materials by Intel or
#  Intel's suppliers or licensors in any way.
#
#  This file contains 'Framework Code' and is licensed as such under the terms
#  of your license agreement with Intel or your vendor. This file may not be
#  modified, except as allowed by additional terms of your license agreement.
#
# @par Specification Reference:
#
# @par Glossary:
###

[Defines]
  INF_VERSION                    = 0x00010017
  BASE_NAME                      = BaseEcMiscLib
  FILE_GUID                      = E08B01D2-49BB-4C40-8544-E2A5DA61E6D5
  VERSION_STRING                 = 1.0
  MODULE_TYPE                    = BASE
  LIBRARY_CLASS                  = EcMiscLib

[LibraryClasses]
  BaseLib
  DebugLib
  IoLib
  TimerLib
  EcLib

[Packages]
  MdePkg/MdePkg.dec
  AlderLakePlatSamplePkg/PlatformPkg.dec
  AlderLakeBoardPkg/BoardPkg.dec
  #[-start-191111-IB10189001-add]#
  $(CHIPSET_PKG)/$(CHIPSET_PKG).dec
  #[-end-191111-IB10189001-add]#

[Pcd]
  gBoardModuleTokenSpaceGuid.PcdEcPresent
#[-start-200220-IB14630326-modify]#
  #[-start-191111-IB10189001-add]#
  gChipsetPkgTokenSpaceGuid.PcdCrbBoard
  #[-end-191111-IB10189001-add]#
#[-end-200220-IB14630326-modify]#

[Sources]
  BaseEcMiscLib.c
