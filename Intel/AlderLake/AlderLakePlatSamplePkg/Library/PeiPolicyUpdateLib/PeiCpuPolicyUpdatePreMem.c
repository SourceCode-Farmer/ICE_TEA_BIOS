/** @file

;******************************************************************************
;* Copyright 2020 - 2021 Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corp.
;*
;******************************************************************************
*/
/** @file
  This file is SampleCode of the library for Intel CPU PEI Policy initialization.

@copyright
  INTEL CONFIDENTIAL
  Copyright 2009 - 2022 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains a 'Sample Driver' and is licensed as such under the terms
  of your license agreement with Intel or your vendor. This file may be modified
  by the user, subject to the additional terms of the license agreement.

@par Specification Reference:
**/
#include "PeiCpuPolicyUpdate.h"
#include <Library/ConfigBlockLib.h>
#include <Library/CpuPlatformLib.h>
#include <Library/FirmwareBootMediaLib.h>
#include <Library/HobLib.h>
#include <Library/PchCycleDecodingLib.h>
#include <Library/PciSegmentLib.h>
#include <Library/PeiSiPolicyUpdateLib.h>
#include <Library/PmcLib.h>
#include <Library/PreSiliconEnvDetectLib.h>
#include <Library/SiPolicyLib.h>
#include <Library/SpiAccessLib.h>
#include <Library/SpiLib.h>
#include <Ppi/Spi.h>
#include <Register/AdlMsr.h>
#include <Register/CommonMsr.h>
#include <Register/PchRegs.h>

#include <PolicyUpdateMacro.h>

#if FixedPcdGet8(PcdFspModeSelection) == 1
#include <FspmUpd.h>
#endif
//[-start-181025-IB15590141-add]//
#include <Guid/H2OCpChipset.h>
#include <Library/H2OCpLib.h>
//[-end-181025-IB15590141-add]//

#define GET_OCCUPIED_SIZE(ActualSize, Alignment) \
  ((ActualSize) + (((Alignment) - ((ActualSize) & ((Alignment) - 1))) & ((Alignment) - 1)))

#define EPOCFCLKFREQ_AUTO_VALUE  3

/**
  Calculate the smallest supported PRMRR size that will be
  reported in response to a given EPC size request from OS

  @param[in] EpcSize           - EPC size

  @retval    UINT32            - PRMRR size that is just bigger than the requested EPC
**/
UINT32
ConvertEpcToPrmrr (
  UINT32 EpcSize
  )
{
  UINT32 EpcAlignedPowerOfTwo;
  UINT32 SupportedPrmrr;

  if (EpcSize == 0) {
    return 0;
  }

  ///
  /// Round EPC size to the nearest power of two
  ///
  EpcAlignedPowerOfTwo = GetPowerOfTwo32(EpcSize);

  ///
  /// If given EpcSize is not power of two
  ///
  if (EpcAlignedPowerOfTwo < EpcSize) {
    EpcAlignedPowerOfTwo = (UINT32) (UINTN) LShiftU64(EpcAlignedPowerOfTwo,1);
  }

  /*
    Bits 0 through 31 indicate which sizes are supported. Each bit
    represents one size. A bit at position n indicates whether
    size 2^(n+20) bytes is supported
  */
  SupportedPrmrr = (UINT32) (UINTN) LShiftU64 (AsmReadMsr64 (ADL_MSR_PRMRR_VALID_CONFIG),20);

  ///
  /// Removing sizes that are lower then EPC round size
  ///
  SupportedPrmrr = SupportedPrmrr & ~(EpcAlignedPowerOfTwo - 1);

  ///
  /// If EPC rounding is bigger than highest supported PRMRR size,
  /// then EPC size that was set by OS is not supported
  ///
  if (SupportedPrmrr < EpcAlignedPowerOfTwo) {
    DEBUG ((DEBUG_INFO, "ConvertEpcToPrmrr: Requested EPC size (rounded up to be aligned to power of two) by OS 0x%016llX, is bigger than supported PRMRRs size 0x%016llX\n" ,EpcAlignedPowerOfTwo ,SupportedPrmrr));
    return 0;
  }

  ///
  /// If PRMRR supported size is bigger then EPC size rounded to two
  /// then return the next lowest PRMRR size supported
  ///
  if ((SupportedPrmrr & EpcAlignedPowerOfTwo) == 0) {
    return (UINT32) (UINTN) (LShiftU64(BIT0, LowBitSet32(SupportedPrmrr)));
  }
  return (SupportedPrmrr & EpcAlignedPowerOfTwo);
}

/**
  Function will update the policy for PRMRR size

  @param[in]  VariableServices     Variable Service Ppi
  @param[in]  CpuSetup             The pointer of CPU_SETUP
  @param[in]  FspmUpd              A VOID pointer
  @param[in, out]  CpuSecurityPreMemConfig - will update
                   CpuSecurityPreMemConfig->PrmrrSize
**/
VOID
UpdatePrmrrSize (
  IN     EFI_PEI_READ_ONLY_VARIABLE2_PPI *VariableServices,
  IN     CPU_SETUP                       *CpuSetup,
  IN OUT VOID                            *FspmUpd,
  IN OUT CPU_SECURITY_PREMEM_CONFIG      *CpuSecurityPreMemConfig,
  IN     CPU_CONFIG_LIB_PREMEM_CONFIG    *CpuConfigLibPreMemConfig
  )
{
  CPUID_STRUCTURED_EXTENDED_FEATURE_FLAGS_ECX  CpuidStructuredExtendedFeatureEcx;

  AsmCpuidEx (CPUID_STRUCTURED_EXTENDED_FEATURE_FLAGS,
              CPUID_STRUCTURED_EXTENDED_FEATURE_FLAGS_SUB_LEAF_INFO,
              NULL,
              NULL,
              &CpuidStructuredExtendedFeatureEcx.Uint32,
              NULL
              );
  //
  // If PrmrrSize is 0 then it is not supported or PRMRR size
  // is 0 in setup options then allocate a PRMRR memory size of 2MB,
  //
#if FixedPcdGet8(PcdFspModeSelection) == 1
  if ((((FSPM_UPD *) FspmUpd)->FspmConfig.PrmrrSize == 0) ||
    ((CpuidStructuredExtendedFeatureEcx.Uint32 & BIT23) && (((FSPM_UPD *) FspmUpd)->FspmConfig.ActiveSmallCoreCount != 0))) {
#else
  if ((CpuSecurityPreMemConfig->PrmrrSize == 0) ||
    ((CpuidStructuredExtendedFeatureEcx.Uint32 & BIT23) && (CpuConfigLibPreMemConfig->ActiveSmallCoreCount != 0))) {
#endif
    COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.PrmrrSize, CpuSecurityPreMemConfig->PrmrrSize, SIZE_2MB, NullIndex);
  }


  ///
  /// Check if PRMRR size is not supported.
  /// Bits 0 through 31 indicate which sizes are supported. Each bit represents one size.
  /// A bit at position n indicates whether size 2^(n+20) bytes is supported
  ///
#if FixedPcdGet8(PcdFspModeSelection) == 1
  if ((LShiftU64 (AsmReadMsr64 (ADL_MSR_PRMRR_VALID_CONFIG), 20) & ((FSPM_UPD *) FspmUpd)->FspmConfig.PrmrrSize) == 0x0) {
    DEBUG ((DEBUG_INFO, "InitializePrm: PRMRR setup size 0x%X\n", ((FSPM_UPD *) FspmUpd)->FspmConfig.PrmrrSize));
#else
  if ((LShiftU64 (AsmReadMsr64 (ADL_MSR_PRMRR_VALID_CONFIG), 20) & CpuSecurityPreMemConfig->PrmrrSize) == 0x0) {
    DEBUG ((DEBUG_INFO, "InitializePrm: PRMRR setup size 0x%X\n", CpuSecurityPreMemConfig->PrmrrSize));
#endif
    COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.PrmrrSize, CpuSecurityPreMemConfig->PrmrrSize, 0, NullIndex);
  }
}

/**
  Get the BIOS Guard Module pointer.

  @param[in, out] ModulePtr  - Input is a NULL pointer,
                               and output points BIOS Guard module address if found.
  @param[out]     ModuleSize - UINT32 Input Output the BIOS Guard module size

  @retval EFI_SUCCESS        - BIOS Guard Module found.
  @retval EFI_NOT_FOUND      - BIOS Guard Module size and/or Address equal to 0.
  @retval Others             - BIOS Guard Module not found.
**/
EFI_STATUS
UpdateBiosGuardModulePtr (
  IN OUT EFI_PHYSICAL_ADDRESS   *ModulePtr,
  OUT    UINT32                 *ModuleSize
  )
{
  EFI_STATUS                    Status;
  EFI_FIRMWARE_VOLUME_HEADER    *FvHeader;
  EFI_FV_FILE_INFO              BiosGuardFileInfo;
  EFI_PEI_FILE_HANDLE           *FfsHeader;
  UINT32                        BiosGuardModuleAddr;
  UINTN                         BiosGuardModuleSize;
  EFI_GUID                      *BiosGuardModuleGuidPtr;

  DEBUG ((DEBUG_INFO, "Update BIOS Guard Module Pointer and Size\n"));

  Status                 = EFI_SUCCESS;
  BiosGuardModuleAddr    = 0;
  BiosGuardModuleSize    = 0;
  BiosGuardModuleGuidPtr = &gBiosGuardModuleGuid;

  if (IsSimicsEnvironment ()) {
    BiosGuardModuleGuidPtr = &gBiosGuardModuleSimGuid;
    DEBUG ((DEBUG_INFO, "Searching for Simics BIOS Guard Module binary\n"));
  }

  ///
  /// Locate Firmware Volume header
  ///
//[-start-201021-IB11790397-modify]//
  FvHeader = (EFI_FIRMWARE_VOLUME_HEADER *) (UINTN) FixedPcdGet32 (PcdFlashFirmwareBinariesFvBase);
//[-end-201021-IB11790397-modify]//
  Status = PeiServicesFfsFindFileByName (BiosGuardModuleGuidPtr, FvHeader, (VOID **)&FfsHeader);
  BiosGuardModuleAddr = (UINT32)((UINT8 *) FfsHeader + sizeof (EFI_FFS_FILE_HEADER));
  DEBUG ((DEBUG_ERROR, "BIOS Guard Module Location: 0x%X\n", BiosGuardModuleAddr));

  if (EFI_ERROR (Status)) {
    DEBUG ((DEBUG_ERROR, "Error returned by PeiServicesFfsFindFileByName while retrieving BIOS Guard Module Address\n"));
    ASSERT_EFI_ERROR (Status);
    return Status;
  }

  Status = PeiServicesFfsGetFileInfo (FfsHeader, &BiosGuardFileInfo);
  if (EFI_ERROR (Status)) {
    DEBUG ((DEBUG_ERROR, "Error returned by PeiServicesFfsGetFileInfo while retrieving BIOS Guard Module Size\n"));
  }

  BiosGuardModuleSize = (UINTN) BiosGuardFileInfo.BufferSize;
  DEBUG(( DEBUG_ERROR, "BIOS Guard Module size: 0x%X\n", BiosGuardModuleSize));

  if ((BiosGuardModuleAddr == 0) || (BiosGuardModuleSize == 0)) {
    DEBUG ((DEBUG_ERROR, "BiosGuardModuleSize = 0x%X and/or BiosGuardModule = 0x%X equal to zero\n", BiosGuardModuleAddr, BiosGuardModuleSize));
    ASSERT_EFI_ERROR (Status);
    return EFI_NOT_FOUND;
  }

  *ModulePtr  = BiosGuardModuleAddr;
  *ModuleSize = BiosGuardModuleSize;
  return Status;
}

/**
  Check if BIOS Guard Module is Debug Signed

  @param[in] ModulePtr - Input is the BIOS Guard module address.

  @retval TRUE  - BIOS Guard Module is Debug Signed
  @retval FALSE - BIOS Guard Module is Production Signed
**/
BOOLEAN
IsBiosGuardModuleDebugSigned2 (
  IN EFI_PHYSICAL_ADDRESS   ModulePtr
  )
{
  UINT16                  RevisionIdMajor;

  ///
  /// Offset from beginning 142 Bytes (Size of BIOS HEADER (128 Bytes) + Offset of Internal Header Major Revision ID (14 Bytes))
  ///
  RevisionIdMajor = *(UINT16*)(UINTN)(ModulePtr + BIOSGUARD_INTERNAL_HEADER_MODULE_ID_OFFSET);
  DEBUG ((DEBUG_INFO, "BIOS Guard Revision ID (Major) = %x\n", RevisionIdMajor));

  ///
  /// Check if Module is Debug Signed. Bit 15 being set means it is Debug Signed.
  ///
  return ((RevisionIdMajor & BIT15) != 0) ? TRUE : FALSE;
}

/**
  Create and initialize BIOS Guard HOB

  @retval  EFI_SUCCESS  HOB Creating successful.
  @retval  Others       HOB Creating failed.
**/
EFI_STATUS
BiosGuardHobInit (
  VOID
  )
{
  EFI_STATUS                       Status;
  BIOSGUARD_HOB                    *BiosGuardHobPtr;
//[-start-201021-IB11790397-remove]//
//  STATIC UINT8                     BgpdtPkeySlot0[] = { 0x6a, 0x94, 0x81, 0x87, 0xfd, 0x8c, 0x51, 0x0a, 0x2a, 0x74, 0x9c, 0x7b, 0xe3, 0xf7, 0x5c, 0x3a, 0xbf, 0xce, 0x60, 0x4f, 0x68, 0xeb, 0xc6, 0x8f, 0x69, 0x98, 0x8c, 0xc5, 0xdb, 0xe3, 0x29, 0x70 };
//  STATIC UINT8                     BgpdtPkeySlot1[] = { 0x1c, 0x51, 0x3a, 0xc5, 0x1c, 0x3d, 0x63, 0x10, 0xef, 0xed, 0xfe, 0xc5, 0xf3, 0x4b, 0xf2, 0xe0, 0x9b, 0x22, 0xe8, 0x06, 0xab, 0xd3, 0x19, 0x2e, 0xfa, 0xfb, 0x6e, 0xd9, 0x36, 0x0d, 0x68, 0x18 };
//  STATIC UINT8                     BgpdtPkeySlot2[] = { 0x19, 0x0b, 0x33, 0xf8, 0xde, 0x3a, 0xa7, 0x9b, 0x57, 0xad, 0xb2, 0x45, 0x86, 0x0e, 0x7f, 0x0e, 0x40, 0x62, 0x80, 0x22, 0x8f, 0x04, 0x92, 0xec, 0x87, 0x44, 0x81, 0xd9, 0xef, 0xed, 0x9f, 0xa3 };
//[-end-201021-IB11790397-remove]//
  UINT8                            EcStatus;
//[-start-201021-IB11790397-remove]//
//  UINT8                            PlatIdStr[] = "ALDERLAKE";
//[-end-201021-IB11790397-remove]//
  PCH_SPI_PPI                      *SpiPpi;
  UINT32                           BaseAddr;
  UINT32                           RegionSize;
  UINT32                           Index = 0;

  RegionSize = 0;
  BaseAddr   = 0;

  Status = PeiServicesLocatePpi (
             &gPchSpiPpiGuid,
             0,
             NULL,
             (VOID **)&SpiPpi
             );

  DEBUG ((DEBUG_INFO, "PlatformInit: LocatePpi gPchSpiPpiGuid = %r\n", Status));
  if (!EFI_ERROR (Status)) {
    //
    // Find the base address for flash
    //
    Status = SpiPpi->GetRegionAddress (SpiPpi, FlashRegionBios, &BaseAddr, &RegionSize);
    DEBUG ((DEBUG_INFO, "BIOS Region: %r - Start Address: 0x%x - Size: 0x%x\n", Status, BaseAddr, RegionSize));
    if (EFI_ERROR (Status)) {
      DEBUG ((DEBUG_ERROR, "Unable to find base address for flash\n"));
      ASSERT_EFI_ERROR (Status);
      return Status;
    }
  }

  Status = PeiServicesCreateHob (EFI_HOB_TYPE_GUID_EXTENSION, sizeof (BIOSGUARD_HOB), (VOID **)&BiosGuardHobPtr);
  if (EFI_ERROR (Status)) {
    DEBUG ((DEBUG_ERROR, "Unable to create BIOS Guard Hob Pointer\n"));
    ASSERT_EFI_ERROR (Status);
    return Status;
  }

  ZeroMem (&(BiosGuardHobPtr->Bgpdt), (sizeof (BIOSGUARD_HOB) - sizeof (EFI_HOB_GUID_TYPE)));
  BiosGuardHobPtr->EfiHobGuidType.Name = gBiosGuardHobGuid;

//[-start-201021-IB11790397-modify]//
  BiosGuardHobPtr->Bgpdt.BgpdtMajVer = BGPDT_MAJOR_VERSION;
  BiosGuardHobPtr->Bgpdt.BgpdtMinVer = BGPDT_MINOR_VERSION;
  BiosGuardHobPtr->Bgpdt.BiosSvn = PcdGet32 (PcdBiosGuardConfigBgpdtBiosSvn);

//[-start-210330-IB18770021-add]//
// RC 2127.00 add, but no use currently.
//#if FixedPcdGetBool(PcdExtendedBiosRegionSupport) == 0
//  BiosGuardHobPtr->Bgpdt.LastSfam = 0;
//  BiosGuardHobPtr->Bgpdt.SfamData[0].FirstByte = BaseAddr + FixedPcdGet32 (PcdFlashNvStorageSize);
//  BiosGuardHobPtr->Bgpdt.SfamData[0].LastByte  = (BaseAddr + RegionSize) - 1;
//#else
//  BiosGuardHobPtr->Bgpdt.LastSfam = 1;
//  BiosGuardHobPtr->Bgpdt.SfamData[0].FirstByte = BaseAddr + FixedPcdGet32 (PcdFlashExtendRegionSizeInUse) + FixedPcdGet32 (PcdFlashNvStorageSize);
//  BiosGuardHobPtr->Bgpdt.SfamData[0].LastByte  = (BaseAddr + RegionSize) - 1;
//  BiosGuardHobPtr->Bgpdt.SfamData[1].FirstByte = BaseAddr;
//  BiosGuardHobPtr->Bgpdt.SfamData[1].LastByte  = (BaseAddr + FixedPcdGet32 (PcdFlashExtendRegionSizeInUse)) - 1;
//#endif
//[-end-210330-IB18770021-add]//

  //
  // NV Variable Store, NV FTW Working, NV FTW Spare, DMI, MSDN, these region will need to modify in runtime. Don't set at SfamData table.
  //
 //[-start-210202-IB14630473-modify]//
  BiosGuardHobPtr->Bgpdt.LastSfam              = 1;

  BiosGuardHobPtr->Bgpdt.SfamData[0].FirstByte = BaseAddr + (PcdGet32 (PcdFlashNvStorageFactoryCopyBase) - PcdGet32 (PcdFlashAreaBaseAddress));
  BiosGuardHobPtr->Bgpdt.SfamData[0].LastByte  = BaseAddr + (PcdGet32 (PcdFlashNvStorageFactoryCopyBase) - PcdGet32 (PcdFlashAreaBaseAddress)) + PcdGet32 (PcdFlashNvStorageFactoryCopySize) + PcdGet32(PcdFlashNvStorageBvdtSize) - 1;

  BiosGuardHobPtr->Bgpdt.SfamData[1].FirstByte = BaseAddr + (PcdGet32 (PcdFlashNvStorageVariableDefaultsBase)- PcdGet32 (PcdFlashAreaBaseAddress)) ;
  BiosGuardHobPtr->Bgpdt.SfamData[1].LastByte  = BaseAddr + PcdGet32 (PcdFlashAreaSize) - 1;
//[-end-210202-IB14630473-modify]//
  CopyMem (&BiosGuardHobPtr->Bgpdt.PkeySlot0[0], (UINT8 *)PcdGetPtr (PcdBiosGuardConfigBgpdtPublicKeySlot0), 32);
  CopyMem (&BiosGuardHobPtr->Bgpdt.PkeySlot1[0], (UINT8 *)PcdGetPtr (PcdBiosGuardConfigBgpdtPublicKeySlot1), 32);
  CopyMem (&BiosGuardHobPtr->Bgpdt.PkeySlot2[0], (UINT8 *)PcdGetPtr (PcdBiosGuardConfigBgpdtPublicKeySlot2), 32);
  BiosGuardHobPtr->BiosGuardLog.LastPage       = MAX_BIOSGUARD_LOG_PAGE - 1;
  BiosGuardHobPtr->BiosGuardLog.LoggingOptions = BIOSGUARD_LOG_OPT_DEBUG | BIOSGUARD_LOG_OPT_FLASH_ERROR | BIOSGUARD_LOG_OPT_FLASH_ERASE | BIOSGUARD_LOG_OPT_FLASH_WRITE | BIOSGUARD_LOG_OPT_BRANCH_TRACE | BIOSGUARD_LOG_OPT_STEP_TRACE;
  BiosGuardHobPtr->BiosGuardLog.Version        = BIOSGUARD_LOG_VERSION;

  if (PcdGetBool (PcdEcPresent) && PcdGetBool (PcdEcBiosGuardEnable)) {
    DEBUG ((DEBUG_INFO, "EC is Present\n"));
    BiosGuardHobPtr->Bgpdt.BiosGuardAttr      |= EnumEcPresent;
    BiosGuardHobPtr->Bgpdt.EcCmd               = EC_C_PORT;
    BiosGuardHobPtr->Bgpdt.EcData              = EC_D_PORT;
    Status = SendEcCommand (PcdGet8(PcdBiosGuardConfigEcCmdDiscovery));
    if (Status == EFI_SUCCESS) {
      Status = ReceiveEcData (&EcStatus);
      if (((EcStatus & 0xF8) == 0) && (EcStatus & BIT0)) {
        if (((EcStatus & (BIT2 | BIT1)) == (BIT2 | BIT1)) || (((EcStatus & (BIT2 | BIT1)) == 0))) {
          DEBUG ((DEBUG_INFO, "EC FW supports BiosGuard\n"));
          BiosGuardHobPtr->Bgpdt.BiosGuardAttr |= EnumEcBiosGuardProtected;
          BiosGuardHobPtr->Bgpdt.EcCmdGetSvn   = PcdGet32(PcdBiosGuardConfigBgpdtEcCmdGetSvn);
          BiosGuardHobPtr->Bgpdt.EcCmdOpen     = PcdGet32(PcdBiosGuardConfigBgpdtEcCmdOpen);
          BiosGuardHobPtr->Bgpdt.EcCmdClose    = PcdGet32(PcdBiosGuardConfigBgpdtEcCmdClose);
          BiosGuardHobPtr->Bgpdt.EcCmdPortTest = PcdGet32(PcdBiosGuardConfigBgpdtEcCmdPortTest);
          BiosGuardHobPtr->BgupHeader.EcSvn    = PcdGet32(PcdBiosGuardConfigBgupHeaderEcSvn);
        } else {
          DEBUG ((DEBUG_ERROR, "EC FW Error\n"));
        }
      } else {
        DEBUG ((DEBUG_WARN, "EC FW do not support BiosGuard\n"));
      }
    } else {
      DEBUG ((DEBUG_ERROR, "SendEcCommand (EcCmdDiscovery) Failed\n"));
    }
  }

//[-end-201021-IB11790397-modify]//
  BiosGuardHobPtr->Bgpdt.BiosGuardAttr |= EnumFlashwearoutProtection;
  BiosGuardHobPtr->Bgpdt.BiosGuardAttr |= EnumFtuEnable;

  BiosGuardHobPtr->BiosGuardModulePtr  = 0;
  BiosGuardHobPtr->BiosGuardModuleSize = 0;

  Status = UpdateBiosGuardModulePtr (&BiosGuardHobPtr->BiosGuardModulePtr, &BiosGuardHobPtr->BiosGuardModuleSize);
  if (EFI_ERROR (Status)) {
    DEBUG ((DEBUG_ERROR, "UpdateBiosGuardModulePtr not found\n"));
    ASSERT_EFI_ERROR (Status);
    return Status;
  }

  DEBUG ((DEBUG_INFO, "------------------ BIOS Guard Module ------------------\n"));
  BiosGuardHobPtr->Bgpdt.BgModSvn = *(UINT32*)(UINTN)(BiosGuardHobPtr->BiosGuardModulePtr + BIOSGUARD_INTERNAL_HEADER_SVN_OFFSET);
  DEBUG ((DEBUG_INFO, " BIOS Guard Module:: SVN : 0x%X\n", *(UINT32*)(UINTN)(BiosGuardHobPtr->BiosGuardModulePtr + BIOSGUARD_INTERNAL_HEADER_SVN_OFFSET)));
  DEBUG ((DEBUG_INFO, " BIOS Guard Module:: Product ID : 0x%X\n", *(UINT16*)(UINTN)(BiosGuardHobPtr->BiosGuardModulePtr + BIOSGUARD_PRODUCT_ID_OFFSET)));
  DEBUG ((DEBUG_INFO, " BIOS Guard Module:: Build Number : 0x%X\n", *(UINT16*)(UINTN)(BiosGuardHobPtr->BiosGuardModulePtr + BIOSGUARD_BUILD_NUMBER_OFFSET)));
//[-start-201021-IB11790397-modify]//
  BiosGuardHobPtr->Bgpdt.BgpdtSize = (sizeof (BGPDT) - sizeof (BiosGuardHobPtr->Bgpdt.SfamData) + ((BiosGuardHobPtr->Bgpdt.LastSfam + 1) * sizeof (SFAM_DATA)));
  CopyMem (&BiosGuardHobPtr->Bgpdt.PlatId[0], (UINT8 *)PcdGetPtr (PcdBiosGuardConfigBgpdtPlatformId), PcdGetSize (PcdBiosGuardConfigBgpdtPlatformId));

  BiosGuardHobPtr->BgupHeader.Version        = BGUP_HDR_VERSION;
  CopyMem (&BiosGuardHobPtr->BgupHeader.PlatId[0], (UINT8 *)PcdGetPtr (PcdBiosGuardConfigBgpdtPlatformId), PcdGetSize (PcdBiosGuardConfigBgpdtPlatformId));
//[-end-201021-IB11790397-modify]//
  BiosGuardHobPtr->BgupHeader.PkgAttributes  = 0;
  BiosGuardHobPtr->BgupHeader.PslMajorVer    = PSL_MAJOR_VERSION;
  BiosGuardHobPtr->BgupHeader.PslMinorVer    = PSL_MINOR_VERSION;
  BiosGuardHobPtr->BgupHeader.BiosSvn        = BiosGuardHobPtr->Bgpdt.BiosSvn;
  BiosGuardHobPtr->BgupHeader.VendorSpecific = 0x808655AA;

  DEBUG ((DEBUG_INFO, "------------------ BIOS GUARD Config ------------------\n"));
  DEBUG ((DEBUG_INFO, " BIOS_GUARD_CONFIG:: BGUP_HEADER : Version : 0x%X\n", BiosGuardHobPtr->BgupHeader.Version));
  DEBUG ((DEBUG_INFO, " BIOS_GUARD_CONFIG:: BGUP_HEADER : PlatId[16] :\n"));
  for (Index = 0; Index < 16; Index++) {
      DEBUG ((DEBUG_INFO, " 0x%X", BiosGuardHobPtr->BgupHeader.PlatId[Index]));
  }

  DEBUG ((DEBUG_INFO, " \n"));
  DEBUG ((DEBUG_INFO, " BIOS_GUARD_CONFIG:: BGUP_HEADER : PkgAttributes : 0x%X\n", BiosGuardHobPtr->BgupHeader.PkgAttributes));
  DEBUG ((DEBUG_INFO, " BIOS_GUARD_CONFIG:: BGUP_HEADER : PslMajorVer : 0x%X\n", BiosGuardHobPtr->BgupHeader.PslMajorVer));
  DEBUG ((DEBUG_INFO, " BIOS_GUARD_CONFIG:: BGUP_HEADER : PslMinorVer : 0x%X\n", BiosGuardHobPtr->BgupHeader.PslMinorVer));
  DEBUG ((DEBUG_INFO, " BIOS_GUARD_CONFIG:: BGUP_HEADER : ScriptSectionSize : 0x%X\n", BiosGuardHobPtr->BgupHeader.ScriptSectionSize));
  DEBUG ((DEBUG_INFO, " BIOS_GUARD_CONFIG:: BGUP_HEADER : DataSectionSize : 0x%X\n", BiosGuardHobPtr->BgupHeader.DataSectionSize));
  DEBUG ((DEBUG_INFO, " BIOS_GUARD_CONFIG:: BGUP_HEADER : BiosSvn : 0x%X\n", BiosGuardHobPtr->BgupHeader.BiosSvn));
  DEBUG ((DEBUG_INFO, " BIOS_GUARD_CONFIG:: BGUP_HEADER : EcSvn : 0x%X\n", BiosGuardHobPtr->BgupHeader.EcSvn));
  DEBUG ((DEBUG_INFO, " BIOS_GUARD_CONFIG:: BGUP_HEADER : VendorSpecific : 0x%X\n", BiosGuardHobPtr->BgupHeader.VendorSpecific));
  DEBUG ((DEBUG_INFO, " BIOS_GUARD_CONFIG:: BGPDT : BgpdtSize : 0x%X\n", BiosGuardHobPtr->Bgpdt.BgpdtSize));
  DEBUG ((DEBUG_INFO, " BIOS_GUARD_CONFIG:: BGPDT : BgpdtMajVer : 0x%X\n", BiosGuardHobPtr->Bgpdt.BgpdtMajVer));
  DEBUG ((DEBUG_INFO, " BIOS_GUARD_CONFIG:: BGPDT : BgpdtMinVer : 0x%X\n", BiosGuardHobPtr->Bgpdt.BgpdtMinVer));
  DEBUG ((DEBUG_INFO, " BIOS_GUARD_CONFIG:: BGPDT : PlatId[16] :\n"));
  for (Index = 0; Index < 16; Index++) {
      DEBUG ((DEBUG_INFO, " 0x%X", BiosGuardHobPtr->Bgpdt.PlatId[Index]));
  }

  DEBUG ((DEBUG_INFO, " \n"));
  DEBUG ((DEBUG_INFO, " BIOS_GUARD_CONFIG:: BGPDT : PkeySlot0[32] :\n"));
  for (Index = 0; Index < 32; Index++) {
    if ((Index == 15) || (Index == 31)) {
      DEBUG ((DEBUG_INFO, " 0x%X\n", BiosGuardHobPtr->Bgpdt.PkeySlot0[Index]));
    } else {
      DEBUG ((DEBUG_INFO, " 0x%X", BiosGuardHobPtr->Bgpdt.PkeySlot0[Index]));
    }
  }

  DEBUG ((DEBUG_INFO, " \n"));
  DEBUG ((DEBUG_INFO, " BIOS_GUARD_CONFIG:: BGPDT : PkeySlot1[32] :\n"));
  for (Index = 0; Index < 32; Index++) {
    if ((Index == 15) || (Index == 31)) {
      DEBUG ((DEBUG_INFO, " 0x%X\n", BiosGuardHobPtr->Bgpdt.PkeySlot1[Index]));
    } else {
      DEBUG ((DEBUG_INFO, " 0x%X", BiosGuardHobPtr->Bgpdt.PkeySlot1[Index]));
    }
  }

  DEBUG ((DEBUG_INFO, " \n"));
  DEBUG ((DEBUG_INFO, " BIOS_GUARD_CONFIG:: BGPDT : PkeySlot2[32] :\n"));
  for (Index = 0; Index < 32; Index++) {
    if ((Index == 15) || (Index == 31)) {
      DEBUG ((DEBUG_INFO, " 0x%X\n", BiosGuardHobPtr->Bgpdt.PkeySlot2[Index]));
    } else {
      DEBUG ((DEBUG_INFO, " 0x%X", BiosGuardHobPtr->Bgpdt.PkeySlot2[Index]));
    }
  }

  DEBUG ((DEBUG_INFO, " \n"));
  DEBUG ((DEBUG_INFO, " BIOS_GUARD_CONFIG:: BGPDT : BgModSvn : 0x%X\n", BiosGuardHobPtr->Bgpdt.BgModSvn));
  DEBUG ((DEBUG_INFO, " BIOS_GUARD_CONFIG:: BGPDT : BiosSvn : 0x%X\n", BiosGuardHobPtr->Bgpdt.BiosSvn));
  DEBUG ((DEBUG_INFO, " BIOS_GUARD_CONFIG:: BGPDT : ExecLim : 0x%X\n", BiosGuardHobPtr->Bgpdt.ExecLim));
  DEBUG ((DEBUG_INFO, " BIOS_GUARD_CONFIG:: BGPDT : BiosGuardAttr : 0x%X\n", BiosGuardHobPtr->Bgpdt.BiosGuardAttr));
  DEBUG ((DEBUG_INFO, " BIOS_GUARD_CONFIG:: BGPDT : EcCmd : 0x%X\n", BiosGuardHobPtr->Bgpdt.EcCmd));
  DEBUG ((DEBUG_INFO, " BIOS_GUARD_CONFIG:: BGPDT : EcData : 0x%X\n", BiosGuardHobPtr->Bgpdt.EcData));
  DEBUG ((DEBUG_INFO, " BIOS_GUARD_CONFIG:: BGPDT : EcCmdGetSvn : 0x%X\n", BiosGuardHobPtr->Bgpdt.EcCmdGetSvn));
  DEBUG ((DEBUG_INFO, " BIOS_GUARD_CONFIG:: BGPDT : EcCmdOpen : 0x%X\n", BiosGuardHobPtr->Bgpdt.EcCmdOpen));
  DEBUG ((DEBUG_INFO, " BIOS_GUARD_CONFIG:: BGPDT : EcCmdClose : 0x%X\n", BiosGuardHobPtr->Bgpdt.EcCmdClose));
  DEBUG ((DEBUG_INFO, " BIOS_GUARD_CONFIG:: BGPDT : EcCmdPortTest : 0x%X\n", BiosGuardHobPtr->Bgpdt.EcCmdPortTest));
  DEBUG ((DEBUG_INFO, " BIOS_GUARD_CONFIG:: BGPDT : LastSfam : 0x%X\n", BiosGuardHobPtr->Bgpdt.LastSfam));
  DEBUG ((DEBUG_INFO, " BIOS_GUARD_CONFIG:: BGPDT : SfamData[64] :\n"));

  //
  // Change the array size according to MAX_SFAM_COUNT
  //
  for (Index = 0; Index <= BiosGuardHobPtr->Bgpdt.LastSfam; Index++) {
    if ((Index == 15) || (Index == 31) || (Index == 47) || (Index == 63) ) {
      DEBUG ((DEBUG_INFO, " 0x%X 0x%X\n", BiosGuardHobPtr->Bgpdt.SfamData[Index].FirstByte,
                                          BiosGuardHobPtr->Bgpdt.SfamData[Index].LastByte));
    } else {
      DEBUG ((DEBUG_INFO, " 0x%X 0x%X,", BiosGuardHobPtr->Bgpdt.SfamData[Index].FirstByte,
                                         BiosGuardHobPtr->Bgpdt.SfamData[Index].LastByte));
    }
  }
  DEBUG ((DEBUG_INFO, " \n"));
  BiosGuardHobPtr->BiosGuardMemAddress = 0;
  BiosGuardHobPtr->BiosGuardMemSize = 0;
  BiosGuardHobPtr->BiosGuardIoTrapAddress = 0;
  BiosGuardHobPtr->BiosGuardIoTrapLength = 0;

  return EFI_SUCCESS;
}

/**
  This function performs CPU PEI Policy initialization in Pre-memory.

  @retval EFI_SUCCESS              The PPI is installed and initialized.
  @retval EFI ERRORS               The PPI is not successfully installed.
  @retval EFI_OUT_OF_RESOURCES     Do not have enough resources to initialize the driver
**/
EFI_STATUS
EFIAPI
UpdatePeiCpuPolicyPreMem (
  VOID
  )
{
  EFI_STATUS                      Status;
  EFI_PEI_READ_ONLY_VARIABLE2_PPI *VariableServices;
  UINTN                           VariableSize;
  SETUP_DATA                      SetupData;
  CPU_SETUP                       CpuSetup;
  EFI_BOOT_MODE                   BootMode;
  CPU_SECURITY_PREMEM_CONFIG      *CpuSecurityPreMemConfig;
  CPU_CONFIG_LIB_PREMEM_CONFIG    *CpuConfigLibPreMemConfig;
  OVERCLOCKING_PREMEM_CONFIG      *OverClockingPreMemConfig;
#if FixedPcdGet8(PcdFspModeSelection) == 1
  VOID                            *FspmUpd;
#else
  SI_PREMEM_POLICY_PPI            *SiPreMemPolicyPpi;
  CPU_TXT_PREMEM_CONFIG           *CpuTxtPreMemConfig;
#endif
  UINT32                          MaxLogicProcessors;
  BIOSGUARD_HOB                   *BiosGuardHobPtr;
  UINT16                          BiosSize;
  UINT16                          BiosMemSizeInMb;
  UINT8                           BiosGuardToolsInterface;
  FW_BOOT_MEDIA_TYPE              FwBootMediaType;
  MSR_CORE_THREAD_COUNT_REGISTER  MsrCoreThreadCount;
  UINT8                           Index;
  UINT8                           AllCoreCount;
  UINT8                           AllSmallCoreCount;
  UINT32                          DisablePerCoreMask;
  UINT8                           MaxCores;

  DEBUG ((DEBUG_INFO, "Update PeiCpuPolicyUpdate Pre-Mem Start\n"));

#if FixedPcdGet8(PcdFspModeSelection) == 1
  FspmUpd                     = NULL;
#else
  CpuTxtPreMemConfig          = NULL;
  SiPreMemPolicyPpi           = NULL;
#endif
  CpuSecurityPreMemConfig     = NULL;
  CpuConfigLibPreMemConfig    = NULL;
  OverClockingPreMemConfig    = NULL;
  BiosGuardHobPtr             = NULL;
  BiosSize                    = 0;
  BiosMemSizeInMb             = 0;
  BiosGuardToolsInterface     = FALSE;
  FwBootMediaType             = FwBootMediaMax;
  AllCoreCount                = 0;
  AllSmallCoreCount           = 0;
  DisablePerCoreMask          = 0;

  MaxCores = (CPU_MAX_BIG_CORES + CPU_MAX_ATOM_CORES);


#if FixedPcdGet8(PcdFspModeSelection) == 1
  FspmUpd = (FSPM_UPD *) PcdGet32 (PcdFspmUpdDataAddress);
  ASSERT (FspmUpd != NULL);
#else
  Status = PeiServicesLocatePpi (&gSiPreMemPolicyPpiGuid, 0, NULL, (VOID **) &SiPreMemPolicyPpi);
  ASSERT_EFI_ERROR (Status);
  Status = GetConfigBlock ((VOID *) SiPreMemPolicyPpi, &gCpuSecurityPreMemConfigGuid, (VOID *) &CpuSecurityPreMemConfig);
  ASSERT_EFI_ERROR (Status);
  Status = GetConfigBlock ((VOID *) SiPreMemPolicyPpi, &gCpuTxtPreMemConfigGuid, (VOID *) &CpuTxtPreMemConfig);
  ASSERT_EFI_ERROR (Status);
  Status = GetConfigBlock ((VOID *) SiPreMemPolicyPpi, &gCpuConfigLibPreMemConfigGuid, (VOID *) &CpuConfigLibPreMemConfig);
  ASSERT_EFI_ERROR (Status);
  Status = GetConfigBlock ((VOID *) SiPreMemPolicyPpi, &gOverclockingPreMemConfigGuid, (VOID *) &OverClockingPreMemConfig);
  ASSERT_EFI_ERROR (Status);
#endif

  //
  // Make sure ReadOnlyVariablePpi is available
  //
  Status = PeiServicesLocatePpi (
             &gEfiPeiReadOnlyVariable2PpiGuid,
             0,
             NULL,
             (VOID **) &VariableServices
             );
  ASSERT_EFI_ERROR (Status);

  //
  // Get Setup Variable
  //
  VariableSize = sizeof (CPU_SETUP);
  Status = VariableServices->GetVariable (
                               VariableServices,
                               L"CpuSetup",
                               &gCpuSetupVariableGuid,
                               NULL,
                               &VariableSize,
                               &CpuSetup
                               );
  ASSERT_EFI_ERROR (Status);

  VariableSize = sizeof (SETUP_DATA);
  Status = VariableServices->GetVariable (
                               VariableServices,
                               L"Setup",
                               &gSetupVariableGuid,
                               NULL,
                               &VariableSize,
                               &SetupData
                               );
  ASSERT_EFI_ERROR (Status);

  //
  // Get current boot mode
  //
  Status = PeiServicesGetBootMode (&BootMode);
  ASSERT_EFI_ERROR (Status);

  COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.EnableC6Dram, CpuSecurityPreMemConfig->EnableC6Dram, CpuSetup.EnableC6Dram, NullIndex);

  //
  // Update TXT Platform Policy
  //
  COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.Txt, CpuSecurityPreMemConfig->Txt, CpuSetup.Txt, NullIndex);
  COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.TxtDprMemorySize, CpuTxtPreMemConfig->TxtDprMemorySize, (UINTN)(CpuSetup.DprSize * 0x100000), NullIndex);

  //
  // Check PWR FLR
  //
  if (PmcIsPowerFailureDetected ()) {
    PmcClearPowerFailureStatus ();
  }

  //
  // When the SUS_PWR_FLR bit is set, it indicates VRMPWRGD signal from the CPU VRM went low.
  // Software must clear this bit if set.
  //
  if (PmcIsSusPowerFailureDetected ()) {
    PmcClearSusPowerFailureStatus ();
  }

  if (CpuSetup.SkipStopPbet == 1) {
    UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.SkipStopPbet, CpuSecurityPreMemConfig->SkipStopPbet, TRUE);
  } else {
    UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.SkipStopPbet, CpuSecurityPreMemConfig->SkipStopPbet, FALSE);
  }

  ///
  /// Set BIOS Guard Tools Interface policy
  ///
  if ((BootMode == BOOT_ON_FLASH_UPDATE) || (BootMode == BOOT_IN_RECOVERY_MODE)) {
    UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.BiosGuardToolsInterface, CpuSecurityPreMemConfig->BiosGuardToolsInterface, CPU_FEATURE_ENABLE);
  } else {
    COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.BiosGuardToolsInterface, CpuSecurityPreMemConfig->BiosGuardToolsInterface, CpuSetup.BiosGuardToolsInterface, NullIndex);
  }

  SpiServiceInit ();

#if FixedPcdGetBool(PcdBiosGuardEnable) == 1
  if (IsSimicsEnvironment ()) {
    DEBUG ((DEBUG_INFO, "BiG: Disable BIOS Guard for simics environment\n"));
    UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.BiosGuard, CpuSecurityPreMemConfig->BiosGuard, CPU_FEATURE_DISABLE);
  } else {
    DEBUG ((DEBUG_INFO, "BiG: Enable Bios Guard\n"));
    UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.BiosGuard, CpuSecurityPreMemConfig->BiosGuard, CPU_FEATURE_ENABLE);
  }
#else
  DEBUG ((DEBUG_INFO, "BIOS Guard PCD and Policy are disabled\n"));
  UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.BiosGuard, CpuSecurityPreMemConfig->BiosGuard, CPU_FEATURE_DISABLE);
#endif

  Status = GetFirmwareBootMediaType (&FwBootMediaType);
  if (Status != EFI_SUCCESS) {
    ASSERT_EFI_ERROR (Status);
    DEBUG ((DEBUG_ERROR, "FirmwareBootMediaLib Error: Cannot Get Boot Media Type\n"));
  } else {
    if (FwBootMediaType == FwBootMediaSpi) {
      DEBUG ((DEBUG_INFO, "Firmware Boot Media is SPI \n"));
#if FixedPcdGet8(PcdFspModeSelection) == 1
      if (((FSPM_UPD *) FspmUpd)->FspmConfig.BiosGuard == TRUE) {
#else
      if (CpuSecurityPreMemConfig->BiosGuard == TRUE) {
#endif
        Status = BiosGuardHobInit ();
        ASSERT_EFI_ERROR (Status);
      }

      BiosGuardHobPtr = GetFirstGuidHob (&gBiosGuardHobGuid);
      if (BiosGuardHobPtr == NULL) {
        DEBUG ((DEBUG_ERROR, "BIOS Guard HOB not available\n"));
      }
      else {
#if FixedPcdGet8(PcdFspModeSelection) == 1
        BiosSize = ((FSPM_UPD *) FspmUpd)->FspmConfig.BiosSize;
        BiosGuardToolsInterface |= (UINT8)(((FSPM_UPD *) FspmUpd)->FspmConfig.BiosGuardToolsInterface);
#else // Dispatch Mode
        BiosSize = CpuSecurityPreMemConfig->BiosSize;
        BiosGuardToolsInterface |= (UINT8)CpuSecurityPreMemConfig->BiosGuardToolsInterface;
#endif // #endif API Mode

        ///
        /// Check if BiosGuardToolsInterface policy is enabled
        /// If Enabled, BGUP data will use the memory allocated within DPR
        /// If Disabled, the size allocated to BIOS Guard in DPR is 0MB, BGUP data will be stored in TSEG
        ///
        if (BiosGuardToolsInterface == TRUE) {
          ///
          /// The size allocated for DPR needs to account for the size of the BIOS ROM plus the size of the
          /// BGUP data: BGUP Header, BGUP Script,and BGUP Certificate.
          /// The size allocated needs to aligned to 1MB due to 1MB granularity of DPR.
          ///
          if ((BiosSize % 1024) == 0) {
            BiosMemSizeInMb = (BiosSize / 1024) + 1;
          } else {
            ///
            /// For a BiosSize that is not exactly 1MB, additional space will need to be added
            /// With consideration of the BGUP data, we can add an additional 2MB for a BIOS
            /// with greater than X.5MB, where X represents whole number value of the BiosSize
            ///
            if ((BiosSize % 1024) < 512) {
              BiosMemSizeInMb = (BiosSize / 1024) + 1;
            } else {
              BiosMemSizeInMb = (BiosSize / 1024) + 2;
            }
          }
          ///
          /// Upon MTRR shortage, BiosMemSizeInMb can be 2MB aligned at the expense of losing memory.
          ///
          if ((BiosMemSizeInMb % 2) == 1) {
            BiosMemSizeInMb = BiosMemSizeInMb + 1;
          }
        }

        BiosGuardHobPtr->BiosGuardMemSize = (UINT8)BiosMemSizeInMb;
      }
    }
  }

  //
  // Converting 0xFF to the actual total supported core count.
  //
  GetCpuSupportedCoresAndAtomCores (&AllCoreCount, &AllSmallCoreCount);
  if (CpuSetup.ActiveCoreCount == 0xFF) {
    CpuSetup.ActiveCoreCount = AllCoreCount;
  }
  if (CpuSetup.ActiveSmallCoreCount == 0xFF) {
    CpuSetup.ActiveSmallCoreCount = AllSmallCoreCount;
  }
  if (CpuSetup.ActiveCoreCount == 0 && CpuSetup.ActiveSmallCoreCount == 0) {
    CpuSetup.ActiveCoreCount = AllCoreCount;
    CpuSetup.ActiveSmallCoreCount = AllSmallCoreCount;
  }

  //
  // Update CpuConfigLibPreMem Config Block data
  //
  COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.HyperThreading,          CpuConfigLibPreMemConfig->HyperThreading,          CpuSetup.HyperThreading,          NullIndex);
  COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.BootFrequency,           CpuConfigLibPreMemConfig->BootFrequency,           CpuSetup.BootFrequency,           NullIndex);
  COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.ActiveCoreCount,         CpuConfigLibPreMemConfig->ActiveCoreCount1,        CpuSetup.ActiveCoreCount,         NullIndex);
  COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.ActiveSmallCoreCount,    CpuConfigLibPreMemConfig->ActiveSmallCoreCount,    CpuSetup.ActiveSmallCoreCount,    NullIndex);
  //
  // If EpocFclkFreq is AUTO, then to load policy based on Silicon default
  //
  if (CpuSetup.EpocFclkFreq != EPOCFCLKFREQ_AUTO_VALUE) {
    UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.FClkFrequency,                     CpuConfigLibPreMemConfig->FClkFrequency,           CpuSetup.EpocFclkFreq);
  }
  COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.JtagC10PowerGateDisable, CpuConfigLibPreMemConfig->JtagC10PowerGateDisable, CpuSetup.JtagC10PowerGateDisable, NullIndex);
  COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.BistOnReset,             CpuConfigLibPreMemConfig->BistOnReset,             CpuSetup.BistOnReset,             NullIndex);
  COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.VmxEnable,               CpuConfigLibPreMemConfig->VmxEnable,               CpuSetup.VT,                      NullIndex);
  COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.TmeEnable,               CpuConfigLibPreMemConfig->TmeEnable,               CpuSetup.TmeEnable,               NullIndex);
  COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.CpuCrashLogEnable,       CpuConfigLibPreMemConfig->CrashLogEnable,          SetupData.EnableCrashLog,         NullIndex);
  COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.CrashLogGprs,            CpuConfigLibPreMemConfig->CrashLogGprs,            CpuSetup.CrashLogGprs,            NullIndex);
  COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.DebugInterfaceEnable,    CpuConfigLibPreMemConfig->DebugInterfaceEnable,    CpuSetup.DebugInterfaceEnable,    NullIndex);

  ///
  /// Function will update PrmrrSize
  ///
  UpdatePrmrrSize (
    VariableServices,
    &CpuSetup,
#if FixedPcdGet8(PcdFspModeSelection) == 1 // #if API Mode
    (VOID *) FspmUpd,
#else
    NULL,
#endif
    CpuSecurityPreMemConfig,
    CpuConfigLibPreMemConfig
    );

  if (BiosGuardHobPtr != NULL) {
    if (IsBiosGuardModuleDebugSigned2 (BiosGuardHobPtr->BiosGuardModulePtr)) {
      UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.DebugInterfaceEnable, CpuConfigLibPreMemConfig->DebugInterfaceEnable, CPU_FEATURE_ENABLE);
    }
  }
  COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.DebugInterfaceLockEnable, CpuConfigLibPreMemConfig->DebugInterfaceLockEnable, CpuSetup.DebugInterfaceLockEnable, NullIndex);
  if (CpuSetup.CpuRatioOverride) {
    COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.CpuRatio, CpuConfigLibPreMemConfig->CpuRatio, CpuSetup.CpuRatio, NullIndex);
  } else {
    UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.CpuRatio, CpuConfigLibPreMemConfig->CpuRatio, 0);
  }
  //
  // BCLK Source
  //
  UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.BclkSource,   CpuConfigLibPreMemConfig->BclkSource,    SetupData.BclkSource);
  UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.ConfigTdpLevel, CpuConfigLibPreMemConfig->ConfigTdpLevel, CpuSetup.ConfigTdpLevel);
  UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.CustomPowerLimit1, CpuConfigLibPreMemConfig->CustomPowerLimit1, (CpuSetup.Custom1PowerLimit1Power / 125));

  //
  // Update OverClockingPreMemConfig Config Block data
  //
  COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.OcSupport, OverClockingPreMemConfig->OcSupport, CpuSetup.OverclockingSupport, NullIndex);
  COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.OcLock,    OverClockingPreMemConfig->OcLock,    CpuSetup.OverclockingLock,    NullIndex);

  if (CpuSetup.OverclockingSupport) {
    //
    // IA Core
    //
    for (Index = 0; Index < MaxCores; Index++) {
      DisablePerCoreMask |= (CpuSetup.PerCoreDisable1[Index] << Index);
    }

    //
    // Mapping the core control to the setting mask.
    //
    UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.DisablePerCoreMask, OverClockingPreMemConfig->DisablePerCoreMask, DisablePerCoreMask);

    COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.CoreVoltageMode, OverClockingPreMemConfig->CoreVoltageMode, CpuSetup.CoreVoltageMode, NullIndex);

#if FixedPcdGet8(PcdFspModeSelection) == 1
    if (((FSPM_UPD *) FspmUpd)->FspmConfig.CoreVoltageMode == OC_LIB_OFFSET_ADAPTIVE) {
#else
    if (OverClockingPreMemConfig->CoreVoltageMode == OC_LIB_OFFSET_ADAPTIVE) {
#endif
      UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.CoreVoltageOverride, OverClockingPreMemConfig->CoreVoltageOverride, 0);
      COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.CoreVoltageAdaptive, OverClockingPreMemConfig->CoreVoltageAdaptive, CpuSetup.CoreExtraTurboVoltage, NullIndex);
#if FixedPcdGet8(PcdFspModeSelection) == 1
    } else if (((FSPM_UPD *) FspmUpd)->FspmConfig.CoreVoltageMode == OC_LIB_OFFSET_OVERRIDE) {
#else
    } else if (OverClockingPreMemConfig->CoreVoltageMode == OC_LIB_OFFSET_OVERRIDE) {
#endif
      COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.CoreVoltageOverride, OverClockingPreMemConfig->CoreVoltageOverride, CpuSetup.CoreVoltageOverride, NullIndex);
      UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.CoreVoltageAdaptive, OverClockingPreMemConfig->CoreVoltageAdaptive, 0);
    } else {
      UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.CoreVoltageOverride, OverClockingPreMemConfig->CoreVoltageOverride, 0);
      UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.CoreVoltageAdaptive, OverClockingPreMemConfig->CoreVoltageAdaptive, 0);
    }

    COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.CoreVfPointOffsetMode, OverClockingPreMemConfig->CoreVfPointOffsetMode, CpuSetup.CoreVfPointOffsetMode, NullIndex);
    COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.CoreVfPointCount,      OverClockingPreMemConfig->CoreVfPointCount,      CpuSetup.CoreVfPointCount,      NullIndex);
    COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.CoreVfConfigScope,     OverClockingPreMemConfig->CoreVfConfigScope,     CpuSetup.CoreVfConfigScope,     NullIndex);

    if (CpuSetup.CoreVfPointOffsetMode == 0) {
      if (CpuSetup.CoreVfConfigScope == 0) {
        if (CpuSetup.CoreVoltageOffsetPrefix == 1) {
          COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.CoreVoltageOffset, OverClockingPreMemConfig->CoreVoltageOffset, (INT16)(~CpuSetup.CoreVoltageOffset + 1), NullIndex);
        } else {
          COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.CoreVoltageOffset, OverClockingPreMemConfig->CoreVoltageOffset, CpuSetup.CoreVoltageOffset,               NullIndex);
        }
      } else if (CpuSetup.CoreVfConfigScope == 1) {
        for (Index = 0; Index < CpuSetup.ActiveCoreCount; Index++) {
          if (CpuSetup.PerCoreVoltageOffsetPrefix[Index] == 1) {
            COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.PerCoreVoltageOffset[Index], OverClockingPreMemConfig->PerCoreVoltageOffset1[Index], (INT16)(~CpuSetup.PerCoreVoltageOffset[Index] + 1), Index);
          } else {
            COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.PerCoreVoltageOffset[Index], OverClockingPreMemConfig->PerCoreVoltageOffset1[Index], CpuSetup.PerCoreVoltageOffset[Index],               Index);
          }
        }
        // Per Atom Cluster
        for (Index = 0; Index < (CpuSetup.ActiveSmallCoreCount/ATOM_QUANTITY_IN_CLUSTER); Index++) {
          if (CpuSetup.PerAtomClusterVoltageOffsetPrefix[Index] == 1) {
            COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.PerAtomClusterVoltageOffset[Index], OverClockingPreMemConfig->PerAtomClusterVoltageOffset[Index], (INT16)(~CpuSetup.PerAtomClusterVoltageOffset[Index] + 1), Index);
          } else {
            COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.PerAtomClusterVoltageOffset[Index], OverClockingPreMemConfig->PerAtomClusterVoltageOffset[Index], CpuSetup.PerAtomClusterVoltageOffset[Index],               Index);
          }
        }
      }
    } else if (CpuSetup.CoreVfPointOffsetMode == 1) {
      for (Index = 0; Index < CPU_OC_MAX_VF_POINTS; Index++) {
        if (CpuSetup.CoreVfPointOffsetPrefix[Index] == 1) {
          COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.CoreVfPointOffset[Index], OverClockingPreMemConfig->CoreVfPointOffset[Index], (INT16)(~CpuSetup.CoreVfPointOffset[Index] + 1), Index);
        } else {
          COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.CoreVfPointOffset[Index], OverClockingPreMemConfig->CoreVfPointOffset[Index], CpuSetup.CoreVfPointOffset[Index],               Index);
        }
      }
    }

    //
    // Ring
    //
    COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.RingVoltageMode, OverClockingPreMemConfig->RingVoltageMode, CpuSetup.RingVoltageMode, NullIndex);

#if FixedPcdGet8(PcdFspModeSelection) == 1
    if (((FSPM_UPD *) FspmUpd)->FspmConfig.RingVoltageMode == OC_LIB_OFFSET_ADAPTIVE) {
#else
    if (OverClockingPreMemConfig->RingVoltageMode == OC_LIB_OFFSET_ADAPTIVE) {
#endif
      UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.RingVoltageOverride, OverClockingPreMemConfig->RingVoltageOverride, 0);
      COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.RingVoltageAdaptive, OverClockingPreMemConfig->RingVoltageAdaptive, CpuSetup.RingExtraTurboVoltage, NullIndex);
#if FixedPcdGet8(PcdFspModeSelection) == 1
    } else if (((FSPM_UPD *) FspmUpd)->FspmConfig.RingVoltageMode == OC_LIB_OFFSET_OVERRIDE) {
#else
    } else if (OverClockingPreMemConfig->RingVoltageMode == OC_LIB_OFFSET_OVERRIDE) {
#endif
      COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.RingVoltageOverride, OverClockingPreMemConfig->RingVoltageOverride, CpuSetup.RingVoltageOverride, NullIndex);
      UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.RingVoltageAdaptive, OverClockingPreMemConfig->RingVoltageAdaptive, 0);
    } else {
      UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.RingVoltageOverride, OverClockingPreMemConfig->RingVoltageOverride, 0);
      UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.RingVoltageAdaptive, OverClockingPreMemConfig->RingVoltageAdaptive, 0);
    }

    if (CpuSetup.RingVoltageOffsetPrefix == 1) {
      // Offset is negative, need to convert
      COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.RingVoltageOffset, OverClockingPreMemConfig->RingVoltageOffset, (INT16)(~CpuSetup.RingVoltageOffset + 1), NullIndex);
    } else {
      COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.RingVoltageOffset, OverClockingPreMemConfig->RingVoltageOffset, CpuSetup.RingVoltageOffset,               NullIndex);
    }

    COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.RingVfPointOffsetMode, OverClockingPreMemConfig->RingVfPointOffsetMode, CpuSetup.RingVfPointOffsetMode, NullIndex);
    COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.RingVfPointCount,      OverClockingPreMemConfig->RingVfPointCount,      CpuSetup.RingVfPointCount,      NullIndex);

    if (CpuSetup.RingVfPointOffsetMode == 0) {
      if (CpuSetup.RingVoltageOffsetPrefix == 1) {
        COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.RingVoltageOffset, OverClockingPreMemConfig->RingVoltageOffset, (INT16)(~CpuSetup.RingVoltageOffset + 1), NullIndex);
      } else {
        COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.RingVoltageOffset, OverClockingPreMemConfig->RingVoltageOffset, CpuSetup.RingVoltageOffset,               NullIndex);
      }
    } else if (CpuSetup.RingVfPointOffsetMode == 1) {
      for (Index = 0; Index < CPU_OC_MAX_VF_POINTS; Index++) {
        if (CpuSetup.RingVfPointOffsetPrefix[Index] == 1) {
          COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.RingVfPointOffset[Index], OverClockingPreMemConfig->RingVfPointOffset[Index], (INT16)(~CpuSetup.RingVfPointOffset[Index] + 1), Index);
        } else {
          COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.RingVfPointOffset[Index], OverClockingPreMemConfig->RingVfPointOffset[Index], CpuSetup.RingVfPointOffset[Index],               Index);
        }
      }
    }
    //
    // Atom
    //
    COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.AtomL2VoltageMode, OverClockingPreMemConfig->AtomL2VoltageMode, CpuSetup.AtomL2VoltageMode, NullIndex);

#if FixedPcdGet8(PcdFspModeSelection) == 1
    if (((FSPM_UPD *) FspmUpd)->FspmConfig.AtomL2VoltageMode == OC_LIB_OFFSET_ADAPTIVE) {
#else
    if (OverClockingPreMemConfig->AtomL2VoltageMode == OC_LIB_OFFSET_ADAPTIVE) {
#endif
      UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.AtomL2VoltageOverride, OverClockingPreMemConfig->AtomL2VoltageOverride, 0);
      COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.AtomL2VoltageAdaptive, OverClockingPreMemConfig->AtomL2VoltageAdaptive, CpuSetup.AtomL2ExtraTurboVoltage, NullIndex);
#if FixedPcdGet8(PcdFspModeSelection) == 1
    } else if (((FSPM_UPD *) FspmUpd)->FspmConfig.AtomL2VoltageMode == OC_LIB_OFFSET_OVERRIDE) {
#else
    } else if (OverClockingPreMemConfig->AtomL2VoltageMode == OC_LIB_OFFSET_OVERRIDE) {
#endif
      COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.AtomL2VoltageOverride, OverClockingPreMemConfig->AtomL2VoltageOverride, CpuSetup.AtomL2VoltageOverride, NullIndex);
      UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.AtomL2VoltageAdaptive, OverClockingPreMemConfig->AtomL2VoltageAdaptive, 0);
    } else {
      UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.AtomL2VoltageOverride, OverClockingPreMemConfig->AtomL2VoltageOverride, 0);
      UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.AtomL2VoltageAdaptive, OverClockingPreMemConfig->AtomL2VoltageAdaptive, 0);
    }

    if (CpuSetup.AtomL2VoltageOffsetPrefix == 1) {
      // Offset is negative, need to convert
      COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.AtomL2VoltageOffset, OverClockingPreMemConfig->AtomL2VoltageOffset, (INT16)(~CpuSetup.AtomL2VoltageOffset + 1), NullIndex);
    } else {
      COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.AtomL2VoltageOffset, OverClockingPreMemConfig->AtomL2VoltageOffset, CpuSetup.AtomL2VoltageOffset,               NullIndex);
    }

    COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.PerCoreHtDisable,           OverClockingPreMemConfig->PerCoreHtDisable,           CpuSetup.PerCoreHtDisable,          NullIndex);
    COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.CoreMaxOcRatio,             OverClockingPreMemConfig->CoreMaxOcRatio,             CpuSetup.CoreMaxOcRatio,            NullIndex);
    COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.CorePllVoltageOffset,       OverClockingPreMemConfig->CorePllVoltageOffset,       CpuSetup.CorePllVoltageOffset,      NullIndex);
    COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.GtPllVoltageOffset,         OverClockingPreMemConfig->GtPllVoltageOffset,         CpuSetup.GtPllVoltageOffset,        NullIndex);
    COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.RingPllVoltageOffset,       OverClockingPreMemConfig->RingPllVoltageOffset,       CpuSetup.RingPllVoltageOffset,      NullIndex);
    COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.SaPllVoltageOffset,         OverClockingPreMemConfig->SaPllVoltageOffset,         CpuSetup.SaPllVoltageOffset,        NullIndex);
    COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.AtomPllVoltageOffset,       OverClockingPreMemConfig->AtomPllVoltageOffset,       CpuSetup.AtomPllVoltageOffset,      NullIndex);
    COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.McPllVoltageOffset,         OverClockingPreMemConfig->McPllVoltageOffset,         CpuSetup.McPllVoltageOffset,        NullIndex);
    COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.RingMaxOcRatio,             OverClockingPreMemConfig->RingMaxOcRatio,             CpuSetup.RingMaxOcRatio,            NullIndex);
    COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.RingDownBin,                OverClockingPreMemConfig->RingDownBin,                CpuSetup.RingDownBin,               NullIndex);
    COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.Avx2RatioOffset,            OverClockingPreMemConfig->Avx2RatioOffset,            CpuSetup.Avx2RatioOffset,           NullIndex);
    COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.TjMaxOffset,                OverClockingPreMemConfig->TjMaxOffset,                CpuSetup.TjMaxOffset,               NullIndex);
    COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.BclkAdaptiveVoltage,        OverClockingPreMemConfig->BclkAdaptiveVoltage,        CpuSetup.BclkAdaptiveVoltageEnable, NullIndex);
    COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.Avx2VoltageScaleFactor,     OverClockingPreMemConfig->Avx2VoltageScaleFactor,     CpuSetup.Avx2VoltageScaleFactor,    NullIndex);
    COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.TvbRatioClipping,           OverClockingPreMemConfig->TvbRatioClipping,           CpuSetup.TvbRatioClipping,          NullIndex);
    COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.TvbVoltageOptimization,     OverClockingPreMemConfig->TvbVoltageOptimization,     CpuSetup.TvbVoltageOptimization,    NullIndex);
    COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.CoreRatioExtensionMode,     OverClockingPreMemConfig->CoreRatioExtensionMode,     CpuSetup.CoreRatioExtensionMode,    NullIndex);
    COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.PvdRatioThreshold,          OverClockingPreMemConfig->PvdRatioThreshold,          CpuSetup.PvdRatioThreshold,         NullIndex);
    COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.DlvrBypassModeEnable,       OverClockingPreMemConfig->DlvrBypassModeEnable,       CpuSetup.DlvrBypassModeEnable,      NullIndex);
    COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.SaPllFreqOverride,          OverClockingPreMemConfig->SaPllFreqOverride,          CpuSetup.SaPllFreqOverride,         NullIndex);
    COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.TscDisableHwFixup,          OverClockingPreMemConfig->TscDisableHwFixup,          CpuSetup.TscDisableHwFixup,         NullIndex);
    COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.TvbDownBinsTempThreshold0,  OverClockingPreMemConfig->TvbDownBinsTempThreshold0,  CpuSetup.TvbDownBinsTempThreshold0, NullIndex);
    COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.TvbTempThreshold0,          OverClockingPreMemConfig->TvbTempThreshold0,          CpuSetup.TvbTempThreshold0,         NullIndex);
    COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.TvbTempThreshold1,          OverClockingPreMemConfig->TvbTempThreshold1,          CpuSetup.TvbTempThreshold1,         NullIndex);
    COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.TvbDownBinsTempThreshold1,  OverClockingPreMemConfig->TvbDownBinsTempThreshold1,  CpuSetup.TvbDownBinsTempThreshold1, NullIndex);
    COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.FllOcModeEn,                OverClockingPreMemConfig->FllOcModeEn,                CpuSetup.FllOcModeEn,               NullIndex);
    COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.FllOverclockMode,           OverClockingPreMemConfig->FllOverclockMode,           CpuSetup.FllOverclockMode,          NullIndex);
    COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.Etvb,                       OverClockingPreMemConfig->Etvb,                       CpuSetup.Etvb,                      NullIndex);

    COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.PerCoreRatioOverride,       OverClockingPreMemConfig->PerCoreRatioOverride,       CpuSetup.PerCoreRatioOverride,      NullIndex);
    if (OverClockingPreMemConfig->PerCoreRatioOverride) {
      for (Index = 0; Index < (CpuSetup.ActiveCoreCount); Index++) {
        COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.PerCoreRatio[Index],    OverClockingPreMemConfig->PerCoreRatio[Index],        CpuSetup.PerCoreRatio[Index], Index);
      }
      for (Index = 0; Index < ((CpuSetup.ActiveSmallCoreCount) / ATOM_QUANTITY_IN_CLUSTER); Index++) {
        COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.AtomClusterRatio[Index], OverClockingPreMemConfig->AtomClusterRatio[Index],   CpuSetup.AtomClusterRatio[Index], Index);
      }
    }

    //
    // SA/Uncore voltage configuration
    //
    COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.SaVoltageMode, OverClockingPreMemConfig->SaVoltageMode, CpuSetup.SaVoltageMode, NullIndex);
#if FixedPcdGet8(PcdFspModeSelection) == 1
    if (((FSPM_UPD *) FspmUpd)->FspmConfig.SaVoltageMode == OC_LIB_OFFSET_ADAPTIVE) {
#else
    if (OverClockingPreMemConfig->SaVoltageMode == OC_LIB_OFFSET_ADAPTIVE) {
#endif
      UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.SaVoltageOverride, OverClockingPreMemConfig->SaVoltageOverride, 0);
      COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.SaExtraTurboVoltage, OverClockingPreMemConfig->SaExtraTurboVoltage, CpuSetup.SaExtraTurboVoltage, NullIndex);
#if FixedPcdGet8(PcdFspModeSelection) == 1
    } else if (((FSPM_UPD *) FspmUpd)->FspmConfig.SaVoltageMode == OC_LIB_OFFSET_OVERRIDE) {
#else
    } else if (OverClockingPreMemConfig->SaVoltageMode == OC_LIB_OFFSET_OVERRIDE) {
#endif
      COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.SaVoltageOverride, OverClockingPreMemConfig->SaVoltageOverride, CpuSetup.SaVoltageOverride, NullIndex);
      UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.SaExtraTurboVoltage, OverClockingPreMemConfig->SaExtraTurboVoltage, 0);
    } else {
      UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.SaVoltageOverride, OverClockingPreMemConfig->SaVoltageOverride, 0);
      UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.SaExtraTurboVoltage, OverClockingPreMemConfig->SaExtraTurboVoltage, 0);
    }
    //
    // CPU BCLK OC Frequency
    //
    UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.CpuBclkOcFrequency,   OverClockingPreMemConfig->CpuBclkOcFrequency,    CpuSetup.CpuBclkOcFrequency);

    UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.IaCepEnable,          OverClockingPreMemConfig->IaCepEnable,           CpuSetup.IaCepEnable);
    UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.GtCepEnable,          OverClockingPreMemConfig->GtCepEnable,           CpuSetup.GtCepEnable);
  }

  ///
  /// Set PcdCpuMaxLogicalProcessorNumber to max number of logical processors enabled
  /// Read MSR_CORE_THREAD_COUNT (0x35) to check the total active Threads
  ///
  MsrCoreThreadCount.Uint64 = AsmReadMsr64 (MSR_CORE_THREAD_COUNT);
  MaxLogicProcessors = MsrCoreThreadCount.Bits.Threadcount;
  DEBUG ((DEBUG_INFO, "MaxLogicProcessors = %d\n", MaxLogicProcessors));

#if (FixedPcdGet8(PcdFspModeSelection) == 1)
  PcdSet32S (PcdCpuMaxLogicalProcessorNumber, MaxLogicProcessors);
#else
  PcdSetEx32S (&gUefiCpuPkgTokenSpaceGuid, PcdCpuMaxLogicalProcessorNumber, MaxLogicProcessors);
  //
  // Point PCD to microcode location in flash.
  // CpuMp PEIM will shadow the microcode to memory.
  //
  PcdSetEx64S (
    &gUefiCpuPkgTokenSpaceGuid, PcdCpuMicrocodePatchAddress,
    FixedPcdGet32 (PcdFlashFvMicrocodeBase) + FixedPcdGet32 (PcdMicrocodeOffsetInFv)
    );
  PcdSetEx64S (
    &gUefiCpuPkgTokenSpaceGuid, PcdCpuMicrocodePatchRegionSize,
    FixedPcdGet32 (PcdFlashFvMicrocodeSize) - FixedPcdGet32 (PcdMicrocodeOffsetInFv)
    );
#endif

  COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.IaIccUnlimitedMode,       OverClockingPreMemConfig->IaIccUnlimitedMode,       CpuSetup.IaIccUnlimitedMode,       NullIndex);
  COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.IaIccMax,                 OverClockingPreMemConfig->IaIccMax,                 CpuSetup.IaIccMax,                 NullIndex);
  COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.GtIccUnlimitedMode,       OverClockingPreMemConfig->GtIccUnlimitedMode,       CpuSetup.GtIccUnlimitedMode,       NullIndex);
  COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.GtIccMax,                 OverClockingPreMemConfig->GtIccMax,                 CpuSetup.GtIccMax,                 NullIndex);

  return EFI_SUCCESS;
}
