/** @file
  This file is SampleCode of the library for Intel PCH PEI Policy initialization.
/** @file
;******************************************************************************
;* Copyright (c) 2020, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/
/** @file
@copyright
  INTEL CONFIDENTIAL
  Copyright 2004 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains a 'Sample Driver' and is licensed as such under the terms
  of your license agreement with Intel or your vendor. This file may be modified
  by the user, subject to the additional terms of the license agreement.

@par Specification Reference:
**/

#include "PeiPchPolicyUpdate.h"
#include <Guid/FmpCapsule.h>
#include <Guid/GlobalVariable.h>
#include <Library/BaseMemoryLib.h>
#include <Library/CnviLib.h>
#include <Library/DebugLib.h>
#include <Library/GbeLib.h>
#include <Library/GpioConfig.h>
#include <Library/HobLib.h>
#include <Library/MemoryAllocationLib.h>
#include <Library/PchInfoLib.h>
#include <Library/PchPcieRpLib.h>
#include <Library/PchPolicyLib.h>
#include <Library/PeiSiPolicyUpdateLib.h>
#include <Library/SataSocLib.h>
#include <Library/SerialPortParameterLib.h>
#include <Library/SiPolicyLib.h>
#include <Library/SpiAccessLib.h>
#include <Library/PeiHdaVerbTables.h>
#include <ConfigBlock.h>
#include <UsbTypeC.h>
#if FixedPcdGetBool (PcdDTbtEnable) == 1
#include <Ppi/PeiDTbtPolicy.h>
#endif
#include <Ppi/Spi.h>

#include <PlatformBoardConfig.h>
#include <PolicyUpdateMacro.h>
#include <SerialIoDevices.h>
#include <Setup.h>

#include <Pins/GpioPinsVer2Lp.h>
#include <Pins/GpioPinsVer2H.h>

#if FixedPcdGet8(PcdFspModeSelection) == 1
#include <FspmUpd.h>
#include <FspsUpd.h>
#endif

#if FixedPcdGet8(PcdEmbeddedEnable) == 0x1
#include <Library/PeiTsnFvLib.h>
#endif
#include <Library/SpiAccessLib.h>
//[-start-191111-IB10189001-add]//
#include <Library/PeiOemSvcKernelLib.h>
#include <PortConfig.h>
#include <Library/H2OLib.h>
#include <Guid/H2OSetup.h>
#include <Library/BaseOemSvcChipsetLib.h>
//[-end-191111-IB10189001-add]//
//[-start-201112-IB16810138-remove]//
//#if FixedPcdGetBool(PcdCapsuleEnable) == 1
//[-end-201112-IB16810138-remove]//
#include <Guid/SysFwUpdateProgress.h>
//[-start-201030-IB16810136-add]//
#include <Guid/GlobalVariable.h>
#include <Library/SeamlessRecoveryLib.h>
#include <Library/PeiCapsuleLib.h>
//[-end-201030-IB16810136-add]//

/**
  Return if input ImageGuid belongs to a FMP device which would perform BIOS update

  @param[in] ImageGuid A pointer to GUID

  @retval TRUE  ImageGuid belongs to a FMP which would perform BIOS update
  @retval FALSE ImageGuid does not belong to a FMP which would perform BIOS update

**/
BOOLEAN
IsFmpGuidForBiosUpdate (
  IN  EFI_GUID                  *ImageGuid,
  OUT SYSTEM_FIRMWARE_COMPONENT *UpdatingComponent
  )
{
  if (CompareGuid (ImageGuid, &gFmpDevicePlatformMonolithicGuid)) {
    *UpdatingComponent = UpdatingTypeMax;
    return TRUE;
  } else if (CompareGuid (ImageGuid, &gFmpDevicePlatformBiosGuid)) {
    *UpdatingComponent = UpdatingBios;
    return TRUE;
  } else if (CompareGuid (ImageGuid, &gFmpDevicePlatformBtGAcmGuid)) {
    *UpdatingComponent = UpdatingBtGAcm;
    return TRUE;
  } else if (CompareGuid (ImageGuid, &gFmpDevicePlatformuCodeGuid)) {
    *UpdatingComponent = UpdatinguCode;
    return TRUE;
  }

  return FALSE;
}

/**
  Detect if there is a FMP capsule which would perform BIOS update

  @retval TRUE        Such capsule is detected.
  @retval FALSE       No such capsules there.

**/
BOOLEAN
IsFmpCapsuleForBiosUpdateDetected (
  OUT SYSTEM_FIRMWARE_COMPONENT                 *UpdatingComponent
  )
{
  EFI_PEI_HOB_POINTERS                          HobPointer;
  EFI_CAPSULE_HEADER                            *CapsuleHeader;
  EFI_FIRMWARE_MANAGEMENT_CAPSULE_HEADER        *FmpCapsuleHeader;
  EFI_FIRMWARE_MANAGEMENT_CAPSULE_IMAGE_HEADER  *ImageHeader;
  UINTN                                         Index;
  UINT64                                        *ItemOffsetList;

  //
  // Search all capsule images from hob
  //
  HobPointer.Raw = GetHobList ();
  while ((HobPointer.Raw = GetNextHob (EFI_HOB_TYPE_UEFI_CAPSULE, HobPointer.Raw)) != NULL) {
    CapsuleHeader = (EFI_CAPSULE_HEADER *) (UINTN) HobPointer.Capsule->BaseAddress;

    //
    // Must be a nested FMP capsule or FMP capsule with pre-defined GUIDs
    //
    if (IsFmpGuidForBiosUpdate (&CapsuleHeader->CapsuleGuid, UpdatingComponent)) {
      CapsuleHeader = (EFI_CAPSULE_HEADER *)((UINT8 *)CapsuleHeader + CapsuleHeader->HeaderSize);
    }

    if (!CompareGuid (&gEfiFmpCapsuleGuid, &CapsuleHeader->CapsuleGuid)) {
      HobPointer.Raw = GET_NEXT_HOB (HobPointer);
      continue;
    }

    FmpCapsuleHeader = (EFI_FIRMWARE_MANAGEMENT_CAPSULE_HEADER *)((UINT8 *)CapsuleHeader + CapsuleHeader->HeaderSize);
    ItemOffsetList = (UINT64 *)(FmpCapsuleHeader + 1);
    for (Index = FmpCapsuleHeader->EmbeddedDriverCount; Index < (UINT32)FmpCapsuleHeader->EmbeddedDriverCount + FmpCapsuleHeader->PayloadItemCount; Index++) {
      ImageHeader = (EFI_FIRMWARE_MANAGEMENT_CAPSULE_IMAGE_HEADER *)((UINT8 *)FmpCapsuleHeader + ItemOffsetList[Index]);
      if (IsFmpGuidForBiosUpdate (&ImageHeader->UpdateImageTypeId, UpdatingComponent)) {
        DEBUG ((DEBUG_INFO, "A FMP capsule for BIOS update is detected.\n"));
        return TRUE;
      }
    }

    HobPointer.Raw = GET_NEXT_HOB (HobPointer);
  }

  return FALSE;
}
//[-start-201030-IB16810136-add]//
/**
  Check whether H2O system firmware update is required.

  @retval TRUE       System firmware Update is going to happen during this boot.
  @retval FALSE      No request of system firmware update is detected.

**/
STATIC
BOOLEAN
IsSystemFirmwareUpdatePending (
  VOID
  )
{
  EFI_STATUS                   Status;
  UINT32                       Progress;

  if (!FeaturePcdGet (PcdH2OBiosUpdateFaultToleranceEnabled)) {
    return FALSE;
  }

  if (DetectPendingUpdateImage ()) {
    return TRUE;
  }

  Status = GetFirmwareUpdateProgress (&Progress);
  if (!EFI_ERROR (Status)) {
    return (Progress < FlashMax);
  }

  return FALSE;
}
//[-end-201030-IB16810136-add]//
/**
  Check if BIOS Update is going to happen during this boot.

  This function checks:
  1. If BootMode is BOOT_ON_FLASH_UPDATE and there is a Capsule to update BIOS region detected.
  2. If BIOS update is in progress in the last boot.
  If one of above confictions stands, set a GUID HOB as an indication to be consumed in DXE/BDS
  and return TRUE. Otherwise, return FALSE.

  @retval TRUE       BIOS Update is going to happen during this boot.
  @retval FALSE      No request of BIOS update is detected.

**/
STATIC
BOOLEAN
IsBiosUpdateRequired (
  VOID
  )
{
  EFI_HOB_GUID_TYPE               *GuidHob;
  SYSTEM_FIRMWARE_COMPONENT       UpdatingComponent;
//[-start-211208-IB16810176-add]//
  BOOLEAN                         TopSwapStatus;
//[-end-211208-IB16810176-add]//

  GuidHob                 = NULL;
  UpdatingComponent       = 0xFF;

  GuidHob = GetFirstGuidHob (&gSysFwUpdateProgressGuid);
//[-start-201030-IB16810136-modify]//
  if (((GetBootModeHob () == BOOT_ON_FLASH_UPDATE) && IsFmpCapsuleForBiosUpdateDetected (&UpdatingComponent)) || \
      (SpiIsTopSwapEnabled ()) || \
      ((GuidHob != NULL) && \
       ((((SYSTEM_FIRMWARE_UPDATE_PROGRESS *) GET_GUID_HOB_DATA (GuidHob))->Component == UpdatingBios) || \
       (((SYSTEM_FIRMWARE_UPDATE_PROGRESS *) GET_GUID_HOB_DATA (GuidHob))->Component == UpdatingBtGAcm) || \
       (((SYSTEM_FIRMWARE_UPDATE_PROGRESS *) GET_GUID_HOB_DATA (GuidHob))->Component == UpdatinguCode) || \
       (((SYSTEM_FIRMWARE_UPDATE_PROGRESS *) GET_GUID_HOB_DATA (GuidHob))->Component == UpdatingResiliency))) || \
       IsSystemFirmwareUpdatePending ()) {
//[-end-201030-IB16810136-modify]//
    //
    // Build gSkipBiosLockForSysFwUpdateGuid HOB if it does not exist.
    //
    GuidHob = GetFirstGuidHob (&gSkipBiosLockForSysFwUpdateGuid);
    if (GuidHob == NULL) {
      DEBUG ((DEBUG_INFO, "Build gSkipBiosLockForSysFwUpdateGuid HOB to reset system before EndOfDxe\n"));
      BuildGuidHob (&gSkipBiosLockForSysFwUpdateGuid, 0);
    }
//[-start-211208-IB16810176-add]//
    GuidHob = GetFirstGuidHob (&gChasmfallsTopSwapStatusGuid);
    if (GuidHob == NULL) { 
      TopSwapStatus = SpiIsTopSwapEnabled ();
      BuildGuidDataHob (&gChasmfallsTopSwapStatusGuid, &TopSwapStatus, sizeof(BOOLEAN));
      DEBUG ((DEBUG_INFO, "Build gChasmfallsTopSwapStatusGuid HOB to record the top swap, status = %d\n", TopSwapStatus));
    }
//[-end-211208-IB16810176-add]//
    return TRUE;
  }

  return FALSE;
}
//[-start-201112-IB16810138-remove]//
//#else // FixedPcdGetBool(PcdCapsuleEnable) != 1
///**
//  Check if BIOS Update is going to happen during this boot.
//
//  @retval TRUE       BIOS Update is going to happen during this boot.
//  @retval FALSE      No request of BIOS update is detected.
//
//**/
//STATIC
//BOOLEAN
//IsBiosUpdateRequired (
//  VOID
//  )
//{
//  return FALSE;
//}
//#endif
//[-end-201112-IB16810138-remove]//

/**
  This is helper function for getting I2C Pads Internal Termination settings from Pcd

  @param[in]  Index            I2C Controller Index
**/
UINT8
STATIC
GetSerialIoI2cPadsTerminationFromPcd (
  IN UINT8 Index
  )
{
  switch (Index) {
    case 0:
      return PcdGet8 (PcdPchSerialIoI2c0PadInternalTerm);
    case 1:
      return PcdGet8 (PcdPchSerialIoI2c1PadInternalTerm);
    case 2:
      return PcdGet8 (PcdPchSerialIoI2c2PadInternalTerm);
    case 3:
      return PcdGet8 (PcdPchSerialIoI2c3PadInternalTerm);
    case 4:
      return PcdGet8 (PcdPchSerialIoI2c4PadInternalTerm);
    case 5:
      return PcdGet8 (PcdPchSerialIoI2c5PadInternalTerm);
    case 6:
      return PcdGet8 (PcdPchSerialIoI2c6PadInternalTerm);
    case 7:
      return PcdGet8 (PcdPchSerialIoI2c7PadInternalTerm);
    default:
      ASSERT (FALSE); // Invalid I2C Controller Index
  }
  return 0;
}

/**
  This function performs PCH Serial IO Platform Policy initialization

  @param[in] SiPolicy             Pointer to SI_POLICY_PPI
  @param[in] FspsUpd              A VOID pointer
  @param[in] PchSetup             Pointer to PCH_SETUP buffer
  @param[in] DebugConfigData      Pointer to DEBUG_CONFIG_DATA buffer
  @param[in] SetupVariables       Pointer to SETUP_DATA buffer
**/
VOID
UpdateSerialIoConfig (
  IN SI_POLICY_PPI             *SiPolicy,
  IN VOID                      *FspsUpd,
  IN PCH_SETUP                 *PchSetup,
  IN DEBUG_CONFIG_DATA         *DebugConfigData,
  IN SETUP_DATA                *SetupVariables
  )
{
  UINT8              Index;
  UINT8              CsIndex;
#if FixedPcdGet8(PcdFspModeSelection) == 0
  SERIAL_IO_CONFIG   *SerialIoConfig;
  EFI_STATUS         Status;

  Status = GetConfigBlock ((VOID *) SiPolicy, &gSerialIoConfigGuid, (VOID *) &SerialIoConfig);
  ASSERT_EFI_ERROR (Status);
  if (EFI_ERROR (Status)) {
    return;
  }
#endif

  //
  // SPI
  //
  for (Index = 0; Index < GetPchMaxSerialIoSpiControllersNum (); Index++) {
    UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.SerialIoSpiMode[Index], SerialIoConfig->SpiDeviceConfig[Index].Mode, (UINT8)(SERIAL_IO_SPI_MODE)PchSetup->PchSerialIoSpi[Index]);
  }

  for (Index = 0; Index < GetPchMaxSerialIoSpiControllersNum (); Index++) {
    for (CsIndex = 0; CsIndex < PCH_MAX_SERIALIO_SPI_CHIP_SELECTS; CsIndex++) {
      UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.SerialIoSpiCsPolarity[PCH_MAX_SERIALIO_SPI_CHIP_SELECTS * Index + CsIndex], SerialIoConfig->SpiDeviceConfig[Index].CsPolarity[CsIndex], PchSetup->PchSpiCsPolarity[PCH_MAX_SERIALIO_SPI_CHIP_SELECTS * Index + CsIndex]);
    }
  }

  //
  // SPI1 chip select polarity must be adjusted to connected peripheral device
  //
  switch (SetupVariables->PchSpiFingerPrintType) {
    case 0:
      break;
    case 1: //FPC1011
    case 2: //FPC1020
    case 6: //FPC1021
    default:
      UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.SerialIoSpiCsPolarity[2], SerialIoConfig->SpiDeviceConfig[1].CsPolarity[0], SerialIoSpiCsActiveLow);
      break;
  }

  //
  // I2C
  //
  for (Index = 0; Index < GetPchMaxSerialIoI2cControllersNum (); Index++) {
    UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.SerialIoI2cMode[Index], SerialIoConfig->I2cDeviceConfig[Index].Mode, (UINT8)(SERIAL_IO_I2C_MODE)PchSetup->PchSerialIoI2c[Index]);
    UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.PchSerialIoI2cPadsTermination[Index], SerialIoConfig->I2cDeviceConfig[Index].PadTermination, GetSerialIoI2cPadsTerminationFromPcd (Index));
  }

  if (IsPchP () || IsPchN ()) {
    UPDATE_POLICY(((FSPS_UPD *)FspsUpd)->FspsConfig.PchSerialIoI2cSdaPinMux[0], SerialIoConfig->I2cDeviceConfig[0].PinMux.Sda, GPIO_VER2_P_MUXING_SERIALIO_I2C0_SDA_GPP_H4);
    UPDATE_POLICY(((FSPS_UPD *)FspsUpd)->FspsConfig.PchSerialIoI2cSclPinMux[0], SerialIoConfig->I2cDeviceConfig[0].PinMux.Scl, GPIO_VER2_P_MUXING_SERIALIO_I2C0_SCL_GPP_H5);
    UPDATE_POLICY(((FSPS_UPD *)FspsUpd)->FspsConfig.PchSerialIoI2cSdaPinMux[1], SerialIoConfig->I2cDeviceConfig[1].PinMux.Sda, GPIO_VER2_P_MUXING_SERIALIO_I2C1_SDA_GPP_H6);
    UPDATE_POLICY(((FSPS_UPD *)FspsUpd)->FspsConfig.PchSerialIoI2cSclPinMux[1], SerialIoConfig->I2cDeviceConfig[1].PinMux.Scl, GPIO_VER2_P_MUXING_SERIALIO_I2C1_SCL_GPP_H7);
    UPDATE_POLICY(((FSPS_UPD *)FspsUpd)->FspsConfig.PchSerialIoI2cSdaPinMux[7], SerialIoConfig->I2cDeviceConfig[7].PinMux.Sda, GPIO_VER2_P_MUXING_SERIALIO_I2C7_SDA_GPP_H12);
    UPDATE_POLICY(((FSPS_UPD *)FspsUpd)->FspsConfig.PchSerialIoI2cSclPinMux[7], SerialIoConfig->I2cDeviceConfig[7].PinMux.Scl, GPIO_VER2_P_MUXING_SERIALIO_I2C7_SCL_GPP_H13);
  }

  //
  // UART
  //
  for (Index = 0; Index < GetPchMaxSerialIoUartControllersNum (); Index++) {
    UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.SerialIoUartMode[Index],        SerialIoConfig->UartDeviceConfig[Index].Mode,                (UINT8)(SERIAL_IO_UART_MODE)PchSetup->PchSerialIoUart[Index]);
    UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.SerialIoUartAutoFlow[Index],    SerialIoConfig->UartDeviceConfig[Index].Attributes.AutoFlow, PchSetup->PchUartHwFlowCtrl[Index]);
    UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.SerialIoUartPowerGating[Index], SerialIoConfig->UartDeviceConfig[Index].PowerGating,         PchSetup->PchUartPowerGating[Index]);
    UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.SerialIoUartDmaEnable[Index],   SerialIoConfig->UartDeviceConfig[Index].DmaEnable,           PchSetup->PchUartDmaEnable[Index]);
  }
  //
  // UART0 still can be enabled even if CNVi is present, for example, there are 2 pin sets {C8 - C11} and {F0 - F3} for UART0 pin out on TGP-LP.
  // The reason that need to disable UART0 is because {F0-F3} is occupied by CNVi RF signals and {C8 - C11} are for WWAN and TouchPad.
  //
  if (CnviIsPresent ()) {
    if (IsAdlPch () && (IsPchP () || IsPchN ())) {
      UPDATE_POLICY(((FSPS_UPD *)FspsUpd)->FspsConfig.SerialIoUartMode[2], SerialIoConfig->UartDeviceConfig[2].Mode, SerialIoUartDisabled);
    } else {
      UPDATE_POLICY(((FSPS_UPD *)FspsUpd)->FspsConfig.SerialIoUartMode[0], SerialIoConfig->UartDeviceConfig[0].Mode, SerialIoUartDisabled);
    }
  } else { // Route Uart pin outs to UART0 when CNVi is not present
    if (IsAdlPch ()
       ) {
      UPDATE_POLICY(((FSPS_UPD *)FspsUpd)->FspsConfig.SerialIoUartRxPinMuxPolicy[0], SerialIoConfig->UartDeviceConfig[0].PinMux.Rx, 0);
      UPDATE_POLICY(((FSPS_UPD *)FspsUpd)->FspsConfig.SerialIoUartTxPinMuxPolicy[0], SerialIoConfig->UartDeviceConfig[0].PinMux.Tx, 0);
      UPDATE_POLICY(((FSPS_UPD *)FspsUpd)->FspsConfig.SerialIoUartRtsPinMuxPolicy[0], SerialIoConfig->UartDeviceConfig[0].PinMux.Rts, 0);
      UPDATE_POLICY(((FSPS_UPD *)FspsUpd)->FspsConfig.SerialIoUartCtsPinMuxPolicy[0], SerialIoConfig->UartDeviceConfig[0].PinMux.Cts, 0);
    } else if (IsPchH()) {
      UPDATE_POLICY(((FSPS_UPD *)FspsUpd)->FspsConfig.SerialIoUartRxPinMuxPolicy[0], SerialIoConfig->UartDeviceConfig[0].PinMux.Rx, GPIO_VER2_H_MUXING_SERIALIO_UART0_RXD_GPP_C8);
      UPDATE_POLICY(((FSPS_UPD *)FspsUpd)->FspsConfig.SerialIoUartTxPinMuxPolicy[0], SerialIoConfig->UartDeviceConfig[0].PinMux.Tx, GPIO_VER2_H_MUXING_SERIALIO_UART0_TXD_GPP_C9);
      UPDATE_POLICY(((FSPS_UPD *)FspsUpd)->FspsConfig.SerialIoUartRtsPinMuxPolicy[0], SerialIoConfig->UartDeviceConfig[0].PinMux.Rts, GPIO_VER2_H_MUXING_SERIALIO_UART0_RTS_GPP_C10);
      UPDATE_POLICY(((FSPS_UPD *)FspsUpd)->FspsConfig.SerialIoUartCtsPinMuxPolicy[0], SerialIoConfig->UartDeviceConfig[0].PinMux.Cts, GPIO_VER2_H_MUXING_SERIALIO_UART0_CTS_GPP_C11);
    }
  }

  if ((PchSetup->OsDebugPort != 0) && (PchSetup->OsDebugPort <= GetPchMaxSerialIoUartControllersNum ())) {
    //
    // This UART controller will be used as OS debug port
    //
    UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.SerialIoUartMode[PchSetup->OsDebugPort - 1], SerialIoConfig->UartDeviceConfig[PchSetup->OsDebugPort - 1].Mode, SerialIoUartHidden);
    UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.SerialIoUartDbg2[PchSetup->OsDebugPort - 1], SerialIoConfig->UartDeviceConfig[PchSetup->OsDebugPort - 1].DBG2,               TRUE);
  }
//[-start-180830-IB15590135-add]//
  //
  // Since PCH UART has been initialized, skip re-init in SerialIoUartInit.
  // Re-init PCH UART makes DDT in BUSY state, because it reset MMIO and PCH UART settings.
  //
  if ((FeaturePcdGet (PcdDebugUsePchComPort) == TRUE)) {
    UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.SerialIoUartMode[PcdGet8 (PcdSerialIoUartNumber)], SerialIoConfig->UartDeviceConfig[PchSetup->OsDebugPort - 1].Mode, SerialIoUartSkipInit);
  }
//[-end-180830-IB15590135-add[]//
  //
  // If SerialIO UART is disabled in policy but used for debug, then
  // override the policy setting
  //
DEBUG_CODE_BEGIN();
//[-start-190710-IB11270240-modify]//
//
// For build
//
  //if (GetDebugInterface () & STATUS_CODE_USE_SERIALIO) {
  if (PcdGet8(PcdStatusCodeFlags) & STATUS_CODE_USE_SERIALIO) {
//[-end-190710-IB11270240-modify]//

    UPDATE_POLICY (
      ((FSPS_UPD *) FspsUpd)->FspsConfig.SerialIoUartBaudRate[DebugConfigData->SerialIoUartDebugControllerNumber],
      SerialIoConfig->UartDeviceConfig[DebugConfigData->SerialIoUartDebugControllerNumber].Attributes.BaudRate,
      DebugConfigData->SerialIoUartDebugBaudRate
    );
    UPDATE_POLICY (
      ((FSPS_UPD *) FspsUpd)->FspsConfig.SerialIoUartDataBits[DebugConfigData->SerialIoUartDebugControllerNumber],
      SerialIoConfig->UartDeviceConfig[DebugConfigData->SerialIoUartDebugControllerNumber].Attributes.DataBits,
      DebugConfigData->SerialIoUartDebugDataBits
    );
    UPDATE_POLICY (
      ((FSPS_UPD *) FspsUpd)->FspsConfig.SerialIoUartStopBits[DebugConfigData->SerialIoUartDebugControllerNumber],
      SerialIoConfig->UartDeviceConfig[DebugConfigData->SerialIoUartDebugControllerNumber].Attributes.StopBits,
      DebugConfigData->SerialIoUartDebugStopBits
    );
    UPDATE_POLICY (
      ((FSPS_UPD *) FspsUpd)->FspsConfig.SerialIoUartParity[DebugConfigData->SerialIoUartDebugControllerNumber],
      SerialIoConfig->UartDeviceConfig[DebugConfigData->SerialIoUartDebugControllerNumber].Attributes.Parity,
      DebugConfigData->SerialIoUartDebugParity
    );
    UPDATE_POLICY (
      ((FSPS_UPD *) FspsUpd)->FspsConfig.SerialIoUartAutoFlow[DebugConfigData->SerialIoUartDebugControllerNumber],
      SerialIoConfig->UartDeviceConfig[DebugConfigData->SerialIoUartDebugControllerNumber].Attributes.AutoFlow,
      DebugConfigData->SerialIoUartDebugFlowControl
    );
  }
DEBUG_CODE_END();

}

/**
  This function updates USB OverCurrent policy mapping based on board design

  @param[in]  UsbConfig        Pointer to USB_CONFIG data buffer
  @param[in]  FspsUpd          A VOID pointer
**/
VOID
UpdateUsbOverCurrentPolicy (
  IN USB_CONFIG                 *UsbConfig,
  IN VOID                       *FspsUpd
  )
{
  USB_OC_MAP_TABLE    *UsbOcMappingTable;
  UINT8               PortIndex;
  UINT8               PortCount;

  //
  // Updating USB2 OC pin mapping
  //
  UsbOcMappingTable = (USB_OC_MAP_TABLE *) PcdGet32 (PcdUsb2OverCurrentPinTable);
  if (UsbOcMappingTable == NULL) {
    DEBUG ((DEBUG_ERROR, "UpdateUsbOverCurrentPolicy: No data for USB2 OC pin mapping provided.\n"));
    return;
  }

  //
  // Warning for size mismatch between data and port count
  //
  if (UsbOcMappingTable->Size != GetPchUsb2MaxPhysicalPortNum ()) {
    DEBUG ((
      DEBUG_WARN,
      "UpdateUsbOverCurrentPolicy: Mismatch between USB2 OC mapping table size and port count. Got: %d\n",
      UsbOcMappingTable->Size
      ));
  }

  //
  // To avoid overflow get minimum from either table size or max USB2 port count
  //
  PortCount = MIN (GetPchUsb2MaxPhysicalPortNum (), UsbOcMappingTable->Size);
  DEBUG ((DEBUG_INFO, "UpdateUsbOverCurrentPolicy: Updating USB OC mapping for %d USB2 ports.\n", PortCount));

  for (PortIndex = 0; PortIndex < PortCount; PortIndex++) {
    if ((UsbOcMappingTable->Data[PortIndex] < USB_OC_MAX_PINS) || (UsbOcMappingTable->Data[PortIndex] == USB_OC_SKIP)) {
      UPDATE_POLICY (
        ((FSPS_UPD *)FspsUpd)->FspsConfig.Usb2OverCurrentPin[PortIndex],
        UsbConfig->PortUsb20[PortIndex].OverCurrentPin,
        UsbOcMappingTable->Data[PortIndex]
        );
    } else {
      DEBUG ((DEBUG_ERROR, "UpdateUsbOverCurrentPolicy: Invalid OverCurrent pin specified USB2 port %d\n", PortIndex));
    }
  }

  //
  // Updating USB3 OC pin mapping
  //
  UsbOcMappingTable = (USB_OC_MAP_TABLE *) PcdGet32 (PcdUsb3OverCurrentPinTable);
  if (UsbOcMappingTable == NULL) {
    DEBUG ((DEBUG_INFO, "UpdateUsbOverCurrentPolicy: No data for USB3 OC pin mapping provided.\n"));
    return;
  }

  //
  // Warning for size mismatch between data and port count
  //
  if (UsbOcMappingTable->Size != GetPchXhciMaxUsb3PortNum ()) {
    DEBUG ((
      DEBUG_WARN,
      "UpdateUsbOverCurrentPolicy: Mismatch between USB3 OC mapping table size and port count. Got: %d\n",
      UsbOcMappingTable->Size
      ));
  }

  //
  // To avoid overflow get minimum from either table size or max USB2 port count
  //
  PortCount = MIN (GetPchXhciMaxUsb3PortNum (), UsbOcMappingTable->Size);
  DEBUG ((DEBUG_INFO, "UpdateUsbOverCurrentPolicy: Updating USB OC mapping for %d USB3 ports.\n", PortCount));

  for (PortIndex = 0; PortIndex < PortCount; PortIndex++) {
    if ((UsbOcMappingTable->Data[PortIndex] < USB_OC_MAX_PINS) || (UsbOcMappingTable->Data[PortIndex] == USB_OC_SKIP)) {
      UPDATE_POLICY (
        ((FSPS_UPD *)FspsUpd)->FspsConfig.Usb3OverCurrentPin[PortIndex],
        UsbConfig->PortUsb30[PortIndex].OverCurrentPin,
        UsbOcMappingTable->Data[PortIndex]
        );
    } else {
      DEBUG ((DEBUG_ERROR, "UpdateUsbOverCurrentPolicy: Invalid OverCurrent pin specified USB3 port %d\n", PortIndex));
    }
  }
}

/**
  This function updates USB2 PortResetMessageEnable policy for PCH USB2 Ports
  those are paired with CPU XHCI Ports based on board design.

  @param[in]  UsbConfig        Pointer to USB_CONFIG data buffer
  @param[in]  FspsUpd          A VOID pointer
**/
VOID
UpdateUsb2PortResetMessageEnablePolicy (
  IN USB_CONFIG                 *UsbConfig,
  IN VOID                       *FspsUpd
  )
{
  UINT8           TypeCUsb2PortIndex;
  UINT8           TypeCUsbPortType;
  UINT8           CpuUsb30PortEnableMap;
  UINT8           TcssCpuUsb30PortIndex;
  BOOLEAN         ValidTcssCpuUsb30Port;

  //
  // Get CPU USB30 enable port map.
  //
  CpuUsb30PortEnableMap = PcdGet8 (PcdCpuUsb30PortEnable);

  //
  // Check CPU TypeC port map to USB 2.0
  //
  if (PcdGetBool (PcdUsbTypeCSupport)) {
    //
    // Get TypeC port 1 PCH USB2 Port Index and Properties
    //
    TypeCUsb2PortIndex = PcdGet8 (PcdUsbTypeCPort1Pch);
    TypeCUsbPortType   = ((PcdGet8 (PcdUsbCPort1Properties) >> USB_TYPEC_PORT_TYPE_BIT_OFFSET) & USB_TYPEC_PORT_TYPE_MASK);
    ValidTcssCpuUsb30Port = FALSE;
    if ((TypeCUsbPortType == USB_TYPEC_CPU) || (TypeCUsbPortType == USB_TYPEA_CPU)) {
      TcssCpuUsb30PortIndex = ((PcdGet8 (PcdUsbCPort1Properties) >> TCSS_TYPEC_ROOT_PORT_INDEX_BIT_OFFSET) & TCSS_TYPEC_ROOT_PORT_INDEX_MASK);
      if ((CpuUsb30PortEnableMap & (BIT0 << TcssCpuUsb30PortIndex)) != 0) {
        ValidTcssCpuUsb30Port = TRUE;
      }
    }

    //
    // Check TypeC port PCH USB2 Port Index is non-zero and it is paired with CPU XHCI Port
    //
    if ((TypeCUsb2PortIndex != 0) && ValidTcssCpuUsb30Port) {
      UPDATE_POLICY (
        ((FSPS_UPD *) FspsUpd)->FspsConfig.PortResetMessageEnable [TypeCUsb2PortIndex - 1],
        UsbConfig->PortUsb20[TypeCUsb2PortIndex - 1].PortResetMessageEnable,
        TRUE
        );
    }

    //
    // Get TypeC port 2 PCH USB2 Port Index and Properties
    //
    TypeCUsb2PortIndex = PcdGet8 (PcdUsbTypeCPort2Pch);
    TypeCUsbPortType   = ((PcdGet8 (PcdUsbCPort2Properties) >> USB_TYPEC_PORT_TYPE_BIT_OFFSET) & USB_TYPEC_PORT_TYPE_MASK);
    ValidTcssCpuUsb30Port = FALSE;
    if ((TypeCUsbPortType == USB_TYPEC_CPU) || (TypeCUsbPortType == USB_TYPEA_CPU)) {
      TcssCpuUsb30PortIndex = ((PcdGet8 (PcdUsbCPort2Properties) >> TCSS_TYPEC_ROOT_PORT_INDEX_BIT_OFFSET) & TCSS_TYPEC_ROOT_PORT_INDEX_MASK);
      if ((CpuUsb30PortEnableMap & (BIT0 << TcssCpuUsb30PortIndex)) != 0) {
        ValidTcssCpuUsb30Port = TRUE;
      }
    }

    //
    // Check TypeC port PCH USB2 Port Index is non-zero and it is paired with CPU XHCI Port
    //
    if ((TypeCUsb2PortIndex != 0) && ValidTcssCpuUsb30Port) {
      UPDATE_POLICY (
        ((FSPS_UPD *) FspsUpd)->FspsConfig.PortResetMessageEnable [TypeCUsb2PortIndex - 1],
        UsbConfig->PortUsb20[TypeCUsb2PortIndex - 1].PortResetMessageEnable,
        TRUE
        );
    }

    //
    // Get TypeC port 3 PCH USB2 Port Index and Properties
    //
    TypeCUsb2PortIndex = PcdGet8 (PcdUsbTypeCPort3Pch);
    TypeCUsbPortType   = ((PcdGet8 (PcdUsbCPort3Properties) >> USB_TYPEC_PORT_TYPE_BIT_OFFSET) & USB_TYPEC_PORT_TYPE_MASK);
    ValidTcssCpuUsb30Port = FALSE;
    if ((TypeCUsbPortType == USB_TYPEC_CPU) || (TypeCUsbPortType == USB_TYPEA_CPU)) {
      TcssCpuUsb30PortIndex = ((PcdGet8 (PcdUsbCPort3Properties) >> TCSS_TYPEC_ROOT_PORT_INDEX_BIT_OFFSET) & TCSS_TYPEC_ROOT_PORT_INDEX_MASK);
      if ((CpuUsb30PortEnableMap & (BIT0 << TcssCpuUsb30PortIndex)) != 0) {
        ValidTcssCpuUsb30Port = TRUE;
      }
    }

    //
    // Check TypeC port PCH USB2 Port Index is non-zero and it is paired with CPU XHCI Port
    //
    if ((TypeCUsb2PortIndex != 0) && ValidTcssCpuUsb30Port) {
      UPDATE_POLICY (
        ((FSPS_UPD *) FspsUpd)->FspsConfig.PortResetMessageEnable [TypeCUsb2PortIndex - 1],
        UsbConfig->PortUsb20[TypeCUsb2PortIndex - 1].PortResetMessageEnable,
        TRUE
        );
    }

    //
    // Get TypeC port 4 PCH USB2 Port Index and Properties
    //
    TypeCUsb2PortIndex = PcdGet8 (PcdUsbTypeCPort4Pch);
    TypeCUsbPortType   = ((PcdGet8 (PcdUsbCPort4Properties) >> USB_TYPEC_PORT_TYPE_BIT_OFFSET) & USB_TYPEC_PORT_TYPE_MASK);
    ValidTcssCpuUsb30Port = FALSE;
    if ((TypeCUsbPortType == USB_TYPEC_CPU) || (TypeCUsbPortType == USB_TYPEA_CPU)) {
      TcssCpuUsb30PortIndex = ((PcdGet8 (PcdUsbCPort4Properties) >> TCSS_TYPEC_ROOT_PORT_INDEX_BIT_OFFSET) & TCSS_TYPEC_ROOT_PORT_INDEX_MASK);
      if ((CpuUsb30PortEnableMap & (BIT0 << TcssCpuUsb30PortIndex)) != 0) {
        ValidTcssCpuUsb30Port = TRUE;
      }
    }

    //
    // Check TypeC port PCH USB2 Port Index is non-zero and it is paired with CPU XHCI Port
    //
    if ((TypeCUsb2PortIndex != 0) && ValidTcssCpuUsb30Port) {
      UPDATE_POLICY (
        ((FSPS_UPD *) FspsUpd)->FspsConfig.PortResetMessageEnable [TypeCUsb2PortIndex - 1],
        UsbConfig->PortUsb20[TypeCUsb2PortIndex - 1].PortResetMessageEnable,
        TRUE
        );
    }

    //
    // Get TypeC port 5 PCH USB2 Port Index and Properties
    //
    TypeCUsb2PortIndex = PcdGet8 (PcdUsbTypeCPort5Pch);
    TypeCUsbPortType   = ((PcdGet8 (PcdUsbCPort5Properties) >> USB_TYPEC_PORT_TYPE_BIT_OFFSET) & USB_TYPEC_PORT_TYPE_MASK);
    ValidTcssCpuUsb30Port = FALSE;
    if ((TypeCUsbPortType == USB_TYPEC_CPU) || (TypeCUsbPortType == USB_TYPEA_CPU)) {
      TcssCpuUsb30PortIndex = ((PcdGet8 (PcdUsbCPort5Properties) >> TCSS_TYPEC_ROOT_PORT_INDEX_BIT_OFFSET) & TCSS_TYPEC_ROOT_PORT_INDEX_MASK);
      if ((CpuUsb30PortEnableMap & (BIT0 << TcssCpuUsb30PortIndex)) != 0) {
        ValidTcssCpuUsb30Port = TRUE;
      }
    }

    //
    // Check TypeC port PCH USB2 Port Index is non-zero and it is paired with CPU XHCI Port
    //
    if ((TypeCUsb2PortIndex != 0) && ValidTcssCpuUsb30Port) {
      UPDATE_POLICY (
        ((FSPS_UPD *) FspsUpd)->FspsConfig.PortResetMessageEnable [TypeCUsb2PortIndex - 1],
        UsbConfig->PortUsb20[TypeCUsb2PortIndex - 1].PortResetMessageEnable,
        TRUE
        );
    }

    //
    // Get TypeC port 6 PCH USB2 Port Index and Properties
    //
    TypeCUsb2PortIndex = PcdGet8 (PcdUsbTypeCPort6Pch);
    TypeCUsbPortType   = ((PcdGet8 (PcdUsbCPort6Properties) >> USB_TYPEC_PORT_TYPE_BIT_OFFSET) & USB_TYPEC_PORT_TYPE_MASK);
    ValidTcssCpuUsb30Port = FALSE;
    if ((TypeCUsbPortType == USB_TYPEC_CPU) || (TypeCUsbPortType == USB_TYPEA_CPU)) {
      TcssCpuUsb30PortIndex = ((PcdGet8 (PcdUsbCPort6Properties) >> TCSS_TYPEC_ROOT_PORT_INDEX_BIT_OFFSET) & TCSS_TYPEC_ROOT_PORT_INDEX_MASK);
      if ((CpuUsb30PortEnableMap & (BIT0 << TcssCpuUsb30PortIndex)) != 0) {
        ValidTcssCpuUsb30Port = TRUE;
      }
    }

    //
    // Check TypeC port PCH USB2 Port Index is non-zero and it is paired with CPU XHCI Port
    //
    if ((TypeCUsb2PortIndex != 0) && ValidTcssCpuUsb30Port) {
      UPDATE_POLICY (
        ((FSPS_UPD *) FspsUpd)->FspsConfig.PortResetMessageEnable [TypeCUsb2PortIndex - 1],
        UsbConfig->PortUsb20[TypeCUsb2PortIndex - 1].PortResetMessageEnable,
        TRUE
        );
    }

    //
    // Get TypeC port 7 PCH USB2 Port Index and Properties
    //
    TypeCUsb2PortIndex = PcdGet8 (PcdUsbTypeCPort7Pch);
    TypeCUsbPortType   = ((PcdGet8 (PcdUsbCPort7Properties) >> USB_TYPEC_PORT_TYPE_BIT_OFFSET) & USB_TYPEC_PORT_TYPE_MASK);
    ValidTcssCpuUsb30Port = FALSE;
    if ((TypeCUsbPortType == USB_TYPEC_CPU) || (TypeCUsbPortType == USB_TYPEA_CPU)) {
      TcssCpuUsb30PortIndex = ((PcdGet8 (PcdUsbCPort7Properties) >> TCSS_TYPEC_ROOT_PORT_INDEX_BIT_OFFSET) & TCSS_TYPEC_ROOT_PORT_INDEX_MASK);
      if ((CpuUsb30PortEnableMap & (BIT0 << TcssCpuUsb30PortIndex)) != 0) {
        ValidTcssCpuUsb30Port = TRUE;
      }
    }

    //
    // Check TypeC port PCH USB2 Port Index is non-zero and it is paired with CPU XHCI Port
    //
    if ((TypeCUsb2PortIndex != 0) && ValidTcssCpuUsb30Port) {
      UPDATE_POLICY (
        ((FSPS_UPD *) FspsUpd)->FspsConfig.PortResetMessageEnable [TypeCUsb2PortIndex - 1],
        UsbConfig->PortUsb20[TypeCUsb2PortIndex - 1].PortResetMessageEnable,
        TRUE
        );
    }

    //
    // Get TypeC port 8 PCH USB2 Port Index and Properties
    //
    TypeCUsb2PortIndex = PcdGet8 (PcdUsbTypeCPort8Pch);
    TypeCUsbPortType   = ((PcdGet8 (PcdUsbCPort8Properties) >> USB_TYPEC_PORT_TYPE_BIT_OFFSET) & USB_TYPEC_PORT_TYPE_MASK);
    ValidTcssCpuUsb30Port = FALSE;
    if ((TypeCUsbPortType == USB_TYPEC_CPU) || (TypeCUsbPortType == USB_TYPEA_CPU)) {
      TcssCpuUsb30PortIndex = ((PcdGet8 (PcdUsbCPort8Properties) >> TCSS_TYPEC_ROOT_PORT_INDEX_BIT_OFFSET) & TCSS_TYPEC_ROOT_PORT_INDEX_MASK);
      if ((CpuUsb30PortEnableMap & (BIT0 << TcssCpuUsb30PortIndex)) != 0) {
        ValidTcssCpuUsb30Port = TRUE;
      }
    }

    //
    // Check TypeC port PCH USB2 Port Index is non-zero and it is paired with CPU XHCI Port
    //
    if ((TypeCUsb2PortIndex != 0) && ValidTcssCpuUsb30Port) {
      UPDATE_POLICY (
        ((FSPS_UPD *) FspsUpd)->FspsConfig.PortResetMessageEnable [TypeCUsb2PortIndex - 1],
        UsbConfig->PortUsb20[TypeCUsb2PortIndex - 1].PortResetMessageEnable,
        TRUE
        );
    }

    //
    // Get TypeC port 9 PCH USB2 Port Index and Properties
    //
    TypeCUsb2PortIndex = PcdGet8 (PcdUsbTypeCPort9Pch);
    TypeCUsbPortType   = ((PcdGet8 (PcdUsbCPort9Properties) >> USB_TYPEC_PORT_TYPE_BIT_OFFSET) & USB_TYPEC_PORT_TYPE_MASK);
    ValidTcssCpuUsb30Port = FALSE;
    if ((TypeCUsbPortType == USB_TYPEC_CPU) || (TypeCUsbPortType == USB_TYPEA_CPU)) {
      TcssCpuUsb30PortIndex = ((PcdGet8 (PcdUsbCPort9Properties) >> TCSS_TYPEC_ROOT_PORT_INDEX_BIT_OFFSET) & TCSS_TYPEC_ROOT_PORT_INDEX_MASK);
      if ((CpuUsb30PortEnableMap & (BIT0 << TcssCpuUsb30PortIndex)) != 0) {
        ValidTcssCpuUsb30Port = TRUE;
      }
    }

    //
    // Check TypeC port PCH USB2 Port Index is non-zero and it is paired with CPU XHCI Port
    //
    if ((TypeCUsb2PortIndex != 0) && ValidTcssCpuUsb30Port) {
      UPDATE_POLICY (
        ((FSPS_UPD *) FspsUpd)->FspsConfig.PortResetMessageEnable [TypeCUsb2PortIndex - 1],
        UsbConfig->PortUsb20[TypeCUsb2PortIndex - 1].PortResetMessageEnable,
        TRUE
        );
    }

    //
    // Get TypeC port A PCH USB2 Port Index and Properties
    //
    TypeCUsb2PortIndex = PcdGet8 (PcdUsbTypeCPortAPch);
    TypeCUsbPortType   = ((PcdGet8 (PcdUsbCPortAProperties) >> USB_TYPEC_PORT_TYPE_BIT_OFFSET) & USB_TYPEC_PORT_TYPE_MASK);
    ValidTcssCpuUsb30Port = FALSE;
    if ((TypeCUsbPortType == USB_TYPEC_CPU) || (TypeCUsbPortType == USB_TYPEA_CPU)) {
      TcssCpuUsb30PortIndex = ((PcdGet8 (PcdUsbCPortAProperties) >> TCSS_TYPEC_ROOT_PORT_INDEX_BIT_OFFSET) & TCSS_TYPEC_ROOT_PORT_INDEX_MASK);
      if ((CpuUsb30PortEnableMap & (BIT0 << TcssCpuUsb30PortIndex)) != 0) {
        ValidTcssCpuUsb30Port = TRUE;
      }
    }

    //
    // Check TypeC port PCH USB2 Port Index is non-zero and it is paired with CPU XHCI Port
    //
    if ((TypeCUsb2PortIndex != 0) && ValidTcssCpuUsb30Port) {
      UPDATE_POLICY (
        ((FSPS_UPD *) FspsUpd)->FspsConfig.PortResetMessageEnable [TypeCUsb2PortIndex - 1],
        UsbConfig->PortUsb20[TypeCUsb2PortIndex - 1].PortResetMessageEnable,
        TRUE
        );
    }
  }
}

/**
  Update Usb config

  @param[in] SiPolicy             Pointer to SI_POLICY_PPI
  @param[in] FspsUpd              Pointer to FspsUpd structure
  @param[in] PchSetup             Pointer to PCH_SETUP buffer
**/
STATIC
VOID
UpdateUsbConfig (
  IN SI_POLICY_PPI             *SiPolicy,
  IN VOID                      *FspsUpd,
  IN PCH_SETUP                 *PchSetup
  )
{
  UINTN           PortIndex;
//[-start-190410-IB16990021-add]//
  PORT_CONFIG              UsbPortConfig;
  PORT_CONFIG              Usb3PortConfig;
//[-end-190410-IB16990021-add]//
  USB_CONFIG      *UsbConfig;
#if FixedPcdGet8(PcdFspModeSelection) == 0
  EFI_STATUS                   Status;

  Status = GetConfigBlock ((VOID *) SiPolicy, &gUsbConfigGuid, (VOID *) &UsbConfig);
  ASSERT_EFI_ERROR (Status);
  if (EFI_ERROR (Status)) {
    return;
  }
#else
  UsbConfig = NULL;
#endif

//[-start-190410-IB16990021-add]//
  UsbPortConfig.Value  = PcdGet64 (PcdH2OChipsetUsbPortEnable);
  Usb3PortConfig.Value = PcdGet64 (PcdH2OChipsetUsb3PortEnable);
//[-end-190410-IB16990021-add]//
  UPDATE_POLICY (
    ((FSPS_UPD *) FspsUpd)->FspsConfig.UsbPdoProgramming,
    UsbConfig->PdoProgramming,
    PchSetup->PchUsbPdoProgramming
    );

  UPDATE_POLICY (
    ((FSPS_UPD *) FspsUpd)->FspsConfig.PchUsbOverCurrentEnable,
    UsbConfig->OverCurrentEnable,
    !!PchSetup->PchEnableDbcObs ? FALSE : PchSetup->PchUsbOverCurrentEnable
    );

  UPDATE_POLICY (
    ((FSPS_UPD *) FspsUpd)->FspsConfig.PchXhciUaolEnable,
    UsbConfig->UaolEnable,
    !!PchSetup->PchXhciUaol
    );

  UPDATE_POLICY ((
    (FSPS_UPD *) FspsUpd)->FspsConfig.PchXhciHsiiEnable,
    UsbConfig->HsiiEnable,
    !!PchSetup->PchXhciHsiiEnable
    );

  for (PortIndex = 0; PortIndex < GetPchUsb2MaxPhysicalPortNum (); PortIndex++) {
  //[-start-190410-IB16990021-modify]//
    UPDATE_POLICY (
      ((FSPS_UPD *) FspsUpd)->FspsConfig.PortUsb20Enable[PortIndex],
      UsbConfig->PortUsb20[PortIndex].Enable,
      (UsbPortConfig.Value >> PortIndex) & 0x1
      );
  //[-end-190410-IB16990021-modify]//
  }
  for (PortIndex = 0; PortIndex < GetPchXhciMaxUsb3PortNum (); PortIndex++) {
  //[-start-190410-IB16990021-modify]//
    UPDATE_POLICY (
      ((FSPS_UPD *) FspsUpd)->FspsConfig.PortUsb30Enable[PortIndex],
      UsbConfig->PortUsb30[PortIndex].Enable,
      (Usb3PortConfig.Value >> PortIndex) & 0x1
      );
  //[-end-190410-IB16990021-modify]//
  }

  //
  // xDCI (USB device) related settings from setup variable
  //
  UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.XdciEnable, UsbConfig->XdciConfig.Enable, !!PchSetup->PchXdciSupport);

  //
  // Update both USB2 and USB3 OC pin mapping based on platform design
  //
  UpdateUsbOverCurrentPolicy (UsbConfig, FspsUpd);

  //
  // Update USB2 Port Reset Message Enable Policy based on platform design
  //
  UpdateUsb2PortResetMessageEnablePolicy (UsbConfig, FspsUpd);
}

/**
  Update USB2 PHY config

  @param[in] SiPolicy             Pointer to SI_POLICY_PPI
  @param[in] FspsUpd              Pointer to FspsUpd structure
  @param[in] PchSetup             Pointer to PCH_SETUP buffer
**/
STATIC
VOID
UpdatePchUsb2PhyConfig (
  IN SI_POLICY_PPI             *SiPolicy,
  IN VOID                      *FspsUpd,
  IN PCH_SETUP                 *PchSetup
  )
{
  USB2_PHY_TABLE    *Usb2PhyTuningTable;
  UINT8             PortIndex;
  UINT8             PortCount;
#if FixedPcdGet8(PcdFspModeSelection) == 0
  USB2_PHY_CONFIG   *Usb2PhyConfig;
  EFI_STATUS                      Status;

  Status = GetConfigBlock ((VOID *) SiPolicy, &gUsb2PhyConfigGuid, (VOID *) &Usb2PhyConfig);
  ASSERT_EFI_ERROR (Status);
  if (EFI_ERROR (Status)) {
    return;
  }
#endif

  //
  // Get USB2 PHY tuning table address and verify it's available
  //
  Usb2PhyTuningTable = (USB2_PHY_TABLE *) PcdGet32 (PcdUsb2PhyTuningTable);
  if (Usb2PhyTuningTable == NULL) {
    DEBUG ((DEBUG_INFO, "UpdateUsb2PhyPolicy: No data for USB2 PHY tuning provided.\n"));
    return;
  }

  //
  // Display warning for size difference
  //
  if (Usb2PhyTuningTable->Size != GetPchUsb2MaxPhysicalPortNum ()) {
    DEBUG ((DEBUG_WARN, "UpdateUsb2PhyPolicy: Invalid USB2 PHY tuning table size. Got: %d\n", Usb2PhyTuningTable->Size));
  }

  //
  // To avoid overflow get minimum from either table size or max USB2 port count
  //
  PortCount = MIN (Usb2PhyTuningTable->Size, GetPchUsb2MaxPhysicalPortNum ());
  DEBUG ((DEBUG_INFO, "UpdateUsb2PhyPolicy: USB2 PHY parameters will be updated for %d USB2 ports.\n", PortCount));

  for (PortIndex = 0; PortIndex < PortCount; PortIndex++) {
    UPDATE_POLICY (((FSPS_UPD *)FspsUpd)->FspsConfig.Usb2PhyPetxiset[PortIndex],  Usb2PhyConfig->Port[PortIndex].Petxiset , Usb2PhyTuningTable->Data[PortIndex].Petxiset);
    UPDATE_POLICY (((FSPS_UPD *)FspsUpd)->FspsConfig.Usb2PhyTxiset[PortIndex],    Usb2PhyConfig->Port[PortIndex].Txiset   , Usb2PhyTuningTable->Data[PortIndex].Txiset);
    UPDATE_POLICY (((FSPS_UPD *)FspsUpd)->FspsConfig.Usb2PhyPredeemp[PortIndex],  Usb2PhyConfig->Port[PortIndex].Predeemp , Usb2PhyTuningTable->Data[PortIndex].Predeemp);
    UPDATE_POLICY (((FSPS_UPD *)FspsUpd)->FspsConfig.Usb2PhyPehalfbit[PortIndex], Usb2PhyConfig->Port[PortIndex].Pehalfbit, Usb2PhyTuningTable->Data[PortIndex].Pehalfbit);
  }
}

#if FixedPcdGetBool(PcdAdlLpSupport) == 1
/**
  Update UFS configuration.

  @param[in] SiPolicy  Pointer to SI_POLICY_PPI
  @param[in] FspsUpd   Pointer to FSPS_UPD
  @param[in] PchSetup  Pointer to PCH_SETUP
**/
STATIC
VOID
UpdateUfsConfig (
  IN SI_POLICY_PPI  *SiPolicy,
  IN VOID           *FspsUpd,
  IN PCH_SETUP      *PchSetup
  )
{
  UINT8           UfsIndex;
#if FixedPcdGet8(PcdFspModeSelection) == 0
  SCS_UFS_CONFIG  *UfsConfig;
  EFI_STATUS      Status;
  Status = GetConfigBlock ((VOID *) SiPolicy, &gUfsConfigGuid, (VOID *) &UfsConfig);
  ASSERT_EFI_ERROR (Status);
  if (EFI_ERROR (Status)) {
    return;
  }
#endif

#if FixedPcdGet8(PcdFspModeSelection) == 1
  //
  // FSP has disjoint configuration space allocation so we check for both UFS number supported in code
  // and UFS supported by FSPS_UPD
  //
  for (UfsIndex = 0; (UfsIndex < PchGetMaxUfsNum ()) && UfsIndex < 2; UfsIndex++) {
#else
  for (UfsIndex = 0; UfsIndex < PchGetMaxUfsNum (); UfsIndex++) {
#endif
    COMPARE_AND_UPDATE_POLICY(((FSPS_UPD *) FspsUpd)->FspsConfig.UfsEnable[UfsIndex], UfsConfig->UfsControllerConfig[UfsIndex].Enable, PchSetup->PchScsUfsEnable[UfsIndex], UfsIndex);
  }
}

#endif

//[-start-210311-IB09480137-add]//
VOID
MergeInternalAndOemVerbTable (
  IN      COMMON_CHIPSET_AZALIA_VERB_TABLE    *OemVerbTableHeaderDataAddress,
  IN  OUT HDA_VERB_TABLE_DATABASE             **HdaVerbTableDatabase
  )
{
  COMMON_CHIPSET_AZALIA_VERB_TABLE_HEADER  *OemVerbTableHeader;
  UINT32                                   *OemVerbTableDataBuffer;
  UINT16                                   DataDwords;
  UINT32                                   DataIndex;
  VOID                                     *OemVerbTable;
  EFI_STATUS                               Status;
  UINT8                                    Index;
  HDA_VERB_TABLE_DATABASE                  *HdaVerbTableDatabaseOld;
  HDA_VERB_TABLE_DATABASE                  *HdaVerbTableDatabaseNew;
  UINT8                                    NewVerbTableDBIndex;
  UINT8                                    IGDVerbCount;
  UINT8                                    OemVerbTableCount;
  UINT8                                    TotalVerbTableCount;

  if (OemVerbTableHeaderDataAddress == NULL || 
      HdaVerbTableDatabase == NULL ||
      *HdaVerbTableDatabase == NULL) {
    DEBUG ((DEBUG_INFO, "MergeInternalAndOemVerbTable input parameters not correct\n"));
    return;
  }

  HdaVerbTableDatabaseOld       = *HdaVerbTableDatabase;
  HdaVerbTableDatabaseNew       = NULL;
  IGDVerbCount        = 0;
  OemVerbTableCount   = 0;
  TotalVerbTableCount = 0;
  NewVerbTableDBIndex = 0;

  //
  // Check how many IGD verb data in current verb table
  //
  for (Index = 0; Index < HdaVerbTableDatabaseOld->Size; Index++) {
    if (HdaVerbTableDatabaseOld->HdaVerbTable[Index]->Header.VendorId == 0x8086) {
      IGDVerbCount++;
    }
  }
  //
  // Count Oem verb table number
  // Oem verb table maximum number is depend on DEFINE_VERB_TABLE_LOCAL_HEADER_DATA_4 marco define
  //
  for (Index = 0; (Index < 5) && (OemVerbTableHeaderDataAddress[Index].VerbTableHeader != NULL); Index++) {
    OemVerbTableCount++;
  }

  TotalVerbTableCount = IGDVerbCount + OemVerbTableCount;
  Status = PeiServicesAllocatePool ((sizeof (*HdaVerbTableDatabaseNew) + (sizeof(HDAUDIO_VERB_TABLE*) * TotalVerbTableCount)), (VOID **)&HdaVerbTableDatabaseNew);
  if (EFI_ERROR (Status)) {
    DEBUG ((DEBUG_INFO, "MergeInternalAndOemVerbTable allocate memory fail status = %r\n",Status));
    ASSERT_EFI_ERROR (Status);
    return;
  }
  ZeroMem ((VOID*)(UINTN)HdaVerbTableDatabaseNew, (sizeof (*HdaVerbTableDatabaseNew) + (sizeof(HDAUDIO_VERB_TABLE*) * TotalVerbTableCount)));

  HdaVerbTableDatabaseNew->Size = TotalVerbTableCount;

  //
  // Update IGD verb table to new verb table database from old verb table
  //
  for (Index = 0; (Index < HdaVerbTableDatabaseOld->Size) && (NewVerbTableDBIndex < IGDVerbCount); Index++) {
    if (HdaVerbTableDatabaseOld->HdaVerbTable[Index]->Header.VendorId == 0x8086) {
      HdaVerbTableDatabaseNew->HdaVerbTable[NewVerbTableDBIndex] = HdaVerbTableDatabaseOld->HdaVerbTable[Index];
      NewVerbTableDBIndex++;
    }
  }

  //
  // 1. COMMON_CHIPSET_AZALIA_VERB_TABLE format converted to HDAUDIO_VERB_TABLE format.
  // 2. Update Oem verb table to new verb table database.
  //
  for (Index = 0; (Index < OemVerbTableCount) && (OemVerbTableHeaderDataAddress[Index].VerbTableHeader != NULL); Index++) {
  
    OemVerbTable = NULL;
    OemVerbTableHeader = OemVerbTableHeaderDataAddress[Index].VerbTableHeader;
    OemVerbTableDataBuffer = OemVerbTableHeaderDataAddress[Index].VerbTableData;
    DataDwords = (OemVerbTableHeader->NumberOfFrontJacks + OemVerbTableHeader->NumberOfRearJacks) * 4;
    Status = PeiServicesAllocatePool ((sizeof (HDA_VERB_TABLE_HEADER) + (DataDwords * sizeof(UINT32))), (VOID **)&OemVerbTable);
    if (EFI_ERROR (Status)) {
      ASSERT_EFI_ERROR (Status);
      return;
    }
    ((HDAUDIO_VERB_TABLE *)OemVerbTable)->Header.VendorId = OemVerbTableHeader->VendorDeviceId >> 16;
    ((HDAUDIO_VERB_TABLE *)OemVerbTable)->Header.DeviceId = OemVerbTableHeader->VendorDeviceId & 0xFFFF;
    ((HDAUDIO_VERB_TABLE *)OemVerbTable)->Header.RevisionId = OemVerbTableHeader->RevisionId;
    ((HDAUDIO_VERB_TABLE *)OemVerbTable)->Header.SdiNum = 0xFF;
    ((HDAUDIO_VERB_TABLE *)OemVerbTable)->Header.DataDwords = DataDwords;
    for (DataIndex = 0; DataIndex < ((HDAUDIO_VERB_TABLE *)OemVerbTable)->Header.DataDwords; DataIndex++) {
      ((HDAUDIO_VERB_TABLE *)OemVerbTable)->Data[DataIndex] = *((UINT32 *)OemVerbTableDataBuffer + DataIndex);
    }
    //
    // Update Oem verb table to new verb table database
    //
    HdaVerbTableDatabaseNew->HdaVerbTable[NewVerbTableDBIndex] = (HDAUDIO_VERB_TABLE *)OemVerbTable;
    NewVerbTableDBIndex++;
    if (NewVerbTableDBIndex >= TotalVerbTableCount) {
      //
      // NewVerbTableDBIndex cannot more than the TotalVerbTableCount
      //
      break;
    }
  }

  if (HdaVerbTableDatabaseNew != NULL) {
    *HdaVerbTableDatabase = HdaVerbTableDatabaseNew;
  }
}
//[-end-210311-IB09480137-add]//

/**
  Update HD Audio configuration.

  @param[in] SiPolicy          Pointer to SI_POLICY_PPI
  @param[in] SiPreMemPolicyPpi Pointer to SI_PREMEM_POLICY_PPI
  @param[in] FspmUpd           Pointer to FSPM_UPD
  @param[in] FspsUpd           Pointer to FSPS_UPD
  @param[in] PchSetup          Pointer to PCH_SETUP
**/
STATIC
VOID
UpdateHdAudioConfig (
  IN SI_POLICY_PPI         *SiPolicy,
  IN SI_PREMEM_POLICY_PPI  *SiPreMemPolicyPpi,
  IN VOID                  *FspsUpd,
  IN VOID                  *FspmUpd,
  IN PCH_SETUP             *PchSetup
  )
{
  HDA_VERB_TABLE_DATABASE  *HdaVerbTableDatabase;
//[-start-210311-IB09480137-add]//
  COMMON_CHIPSET_AZALIA_VERB_TABLE         *OemVerbTableHeaderDataAddress;
//[-end-210311-IB09480137-add]//

#if FixedPcdGetBool(PcdFspModeSelection) == 0
  HDAUDIO_CONFIG           *HdAudioConfig;
  HDAUDIO_PREMEM_CONFIG    *HdAudioPreMemConfig;
  EFI_STATUS            Status;
  Status = GetConfigBlock ((VOID *) SiPolicy, &gHdAudioConfigGuid, (VOID *) &HdAudioConfig);
  ASSERT_EFI_ERROR (Status);
  if (EFI_ERROR (Status)) {
    return;
  }
  Status = GetConfigBlock ((VOID *) SiPreMemPolicyPpi, &gHdAudioPreMemConfigGuid, (VOID *) &HdAudioPreMemConfig);
  ASSERT_EFI_ERROR (Status);
  if (EFI_ERROR (Status)) {
    return;
  }
#endif

  COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.PchHdaPme,                   HdAudioConfig->Pme,                   PchSetup->PchHdAudioPme,                   NullIndex);
  COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.PchHdaLinkFrequency,         HdAudioConfig->HdAudioLinkFrequency,  PchSetup->PchHdaHdAudioLinkFreq,           NullIndex);
  COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.PchHdaCodecSxWakeCapability, HdAudioConfig->CodecSxWakeCapability, PchSetup->PchHdAudioCodecSxWakeCapability, NullIndex);
  //
  // Install HDA Link/iDisplay Codec Verb Table
  //
  if (PchSetup->PchHdAudio) {
    HdaVerbTableDatabase = (HDA_VERB_TABLE_DATABASE *) (UINTN) PcdGet32 (PcdHdaVerbTableDatabase);

    if (HdaVerbTableDatabase == NULL) {
      DEBUG ((DEBUG_ERROR, "HdaVerbTableDatabase is NULL!\n"));
      ASSERT (FALSE);
      return;
    }

//[-start-210311-IB09480137-modify]//
    OemVerbTableHeaderDataAddress = NULL;
    DEBUG_OEM_SVC ((DEBUG_INFO, "OemKernelServices Call: OemSvcGetVerbTable \n"));
    Status = OemSvcGetVerbTable (&OemVerbTableHeaderDataAddress);
    DEBUG_OEM_SVC ((DEBUG_INFO, "OemKernelServices OemSvcGetVerbTable Status: %r\n", Status));
  
    if (!EFI_ERROR (Status)) {
      DEBUG ((DEBUG_ERROR, "OemSvcGetVerbTable status is success, not need to update policy!!\n"));
    } else {

      if ((Status == EFI_MEDIA_CHANGED) && (OemVerbTableHeaderDataAddress != NULL)) {
        MergeInternalAndOemVerbTable (OemVerbTableHeaderDataAddress , &HdaVerbTableDatabase);
      }
      UPDATE_POLICY (((FSPS_UPD *)FspsUpd)->FspsConfig.PchHdaVerbTableEntryNum, HdAudioConfig->VerbTableEntryNum, HdaVerbTableDatabase->Size);
      UPDATE_POLICY (((FSPS_UPD *)FspsUpd)->FspsConfig.PchHdaVerbTablePtr, HdAudioConfig->VerbTablePtr, (UINT32) HdaVerbTableDatabase->HdaVerbTable);
    }
//[-end-210311-IB09480137-modify]//
  }
}

/**
  Update IO APIC Configuration

  @param[in] SiPolicy             Pointer to SI_POLICY_PPI
  @param[in] FspsUpd              Pointer to FspsUpd structure
  @param[in] PchSetup             Pointer to PCH_SETUP buffer
  @param[in] SetupVariables       Pointer to SETUP_DATA buffer
**/
VOID
UpdateIoApicConfig (
  IN SI_POLICY_PPI             *SiPolicy,
  IN VOID                      *FspsUpd,
  IN PCH_SETUP                 *PchSetup,
  IN SETUP_DATA                *SetupVariables
  )
{
#if FixedPcdGet8(PcdFspModeSelection) == 0
  PCH_IOAPIC_CONFIG   *IoApicConfig;
  EFI_STATUS          Status;

  Status = GetConfigBlock ((VOID *) SiPolicy, &gIoApicConfigGuid, (VOID *) &IoApicConfig);
  ASSERT_EFI_ERROR (Status);
  if (EFI_ERROR (Status)) {
    return;
  }
#endif

  COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.PchIoApicEntry24_119, IoApicConfig->IoApicEntry24_119, PchSetup->PchIoApic24119Entries, NullIndex);
  //
  // To support SLP_S0, it's required to disable 8254 timer.
  // Note that CSM may require this option to be disabled for correct operation.
  // Once 8254 timer disabled, some legacy OPROM and legacy OS will fail while using 8254 timer.
  // For some OS environment that it needs to set 8254CGE in late state it should
  // set this policy to FALSE and use PmcSet8254ClockGateState (TRUE) in SMM later.
  // This is also required during S3 resume.
  //
  // The Enable8254ClockGatingOnS3 is only applicable when Enable8254ClockGating is disabled.
  // If Enable8254ClockGating is enabled, RC will do 8254 CGE programming on S3 as well.
  // else, RC will do the programming on S3 when Enable8254ClockGatingOnS3 is enabled.
  // This avoids the SMI requirement for the programming.
  //
//[-start-191111-IB10189001-modify]//
  if ((SetupVariables->LowPowerS0Idle == 0) ||
      (PchSetup->Enable8254ClockGating == 0) ||
      (H2OGetBootType () != EFI_BOOT_TYPE)) {
    UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.Enable8254ClockGating, IoApicConfig->Enable8254ClockGating, FALSE);
    UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.Enable8254ClockGatingOnS3, IoApicConfig->Enable8254ClockGatingOnS3, FALSE);
  } else if (PchSetup->Enable8254ClockGating == 1) {
#if FixedPcdGet8(PcdFspModeSelection) == 0
    if ((IoApicConfig->Enable8254ClockGating != TRUE) && (IsPolicyDefaultCheckRequired())) {
      POLICY_DEBUG_WARNING (IoApicConfig->Enable8254ClockGating, PchSetup->Enable8254ClockGating, NullIndex);
    }
#endif
    UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.Enable8254ClockGating, IoApicConfig->Enable8254ClockGating, TRUE);
  } else if (PchSetup->Enable8254ClockGating == 2) {
#if FixedPcdGet8(PcdFspModeSelection) == 0
    if ((IoApicConfig->Enable8254ClockGating != FALSE) && (IsPolicyDefaultCheckRequired())) {
      POLICY_DEBUG_WARNING (IoApicConfig->Enable8254ClockGating, PchSetup->Enable8254ClockGating, NullIndex);
    }
    if ((IoApicConfig->Enable8254ClockGatingOnS3 != TRUE) && (IsPolicyDefaultCheckRequired())) {
      POLICY_DEBUG_WARNING (IoApicConfig->Enable8254ClockGatingOnS3, PchSetup->Enable8254ClockGating, NullIndex);
    }
#endif
    UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.Enable8254ClockGating, IoApicConfig->Enable8254ClockGating, FALSE);
    UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.Enable8254ClockGatingOnS3, IoApicConfig->Enable8254ClockGatingOnS3, TRUE);
  } else {
#if FixedPcdGet8(PcdFspModeSelection) == 0
    if ((IoApicConfig->Enable8254ClockGating != FALSE) && (IsPolicyDefaultCheckRequired())) {
      POLICY_DEBUG_WARNING (IoApicConfig->Enable8254ClockGating, PchSetup->Enable8254ClockGating, NullIndex);
    }
    if ((IoApicConfig->Enable8254ClockGatingOnS3 != FALSE) && (IsPolicyDefaultCheckRequired())) {
      POLICY_DEBUG_WARNING (IoApicConfig->Enable8254ClockGatingOnS3, PchSetup->Enable8254ClockGating, NullIndex);
    }
#endif
    UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.Enable8254ClockGating, IoApicConfig->Enable8254ClockGating, FALSE);
    UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.Enable8254ClockGatingOnS3, IoApicConfig->Enable8254ClockGatingOnS3, FALSE);
  }
//[-end-191111-IB10189001-modify]//

}

/**
  Update PCIe Root Port Configuration

  @param[in] SiPolicy             Pointer to SI_POLICY_PPI
  @param[in] FspsUpd              Pointer to FspsUpd structure
  @param[in] PchSetup             Pointer to PCH_SETUP buffer
  @param[in] SetupVariables       Pointer to SETUP_DATA buffer
**/
VOID
UpdatePcieRpConfig (
  IN SI_POLICY_PPI             *SiPolicy,
  IN VOID                      *FspsUpd,
  IN PCH_SETUP                 *PchSetup,
  IN SETUP_DATA                *SetupVariables
  )
{
  UINT8                           Index;
#if FixedPcdGet8(PcdFspModeSelection) == 0 || FixedPcdGetBool (PcdDTbtEnable) == 1
  EFI_STATUS                      Status;
#endif
  PCH_PCIE_CONFIG                 *PchPcieConfig;
#if FixedPcdGetBool (PcdDTbtEnable) == 1
  PEI_DTBT_POLICY                 *PeiDTbtConfig;
  UINT8                           TbtSelector;
#endif
  UINTN                           MaxPciePorts;
  UINT8                           RpIndex;

  MaxPciePorts = GetPchMaxPciePortNum ();
  PchPcieConfig = NULL;
#if FixedPcdGetBool (PcdDTbtEnable) == 1
  PeiDTbtConfig = NULL;
#endif
#if FixedPcdGet8(PcdFspModeSelection) == 0
  Status = GetConfigBlock ((VOID *) SiPolicy, &gPchPcieConfigGuid, (VOID *) &PchPcieConfig);
  ASSERT_EFI_ERROR (Status);
  if (EFI_ERROR (Status)) {
    return;
  }
#endif

#if FixedPcdGetBool (PcdDTbtEnable) == 1
  if (SetupVariables->DiscreteTbtSupport == 1) {
    //
    // Obtain TBT Info from PEI TBT Policy.
    //
    Status = PeiServicesLocatePpi (
               &gPeiDTbtPolicyPpiGuid,
               0,
               NULL,
               (VOID **) &PeiDTbtConfig
               );
    ASSERT_EFI_ERROR (Status);
    if (EFI_ERROR (Status)) {
      return;
    }
    if (PeiDTbtConfig != NULL) {
      for (Index = 0; Index < MAX_DTBT_CONTROLLER_NUMBER; Index++) {
        TbtSelector = PeiDTbtConfig->DTbtControllerConfig[Index].PcieRpNumber - 1;
        UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.PcieRpMaxPayload[TbtSelector], PchPcieConfig->RootPort[TbtSelector].PcieRpCommonConfig.MaxPayload, PchPcieMaxPayload128);
        DEBUG ((DEBUG_INFO, "PCIe RootPort[%x]= %x\n", TbtSelector, PchPcieConfig->RootPort[TbtSelector].PcieRpCommonConfig.MaxPayload));

        if (PchSetup->PchPcieLtrEnable[TbtSelector] == 1) {
          UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.PcieRpSnoopLatencyOverrideMode[Index],          PchPcieConfig->RootPort[TbtSelector].PcieRpCommonConfig.PcieRpLtrConfig.SnoopLatencyOverrideMode,          1);
          UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.PcieRpSnoopLatencyOverrideMultiplier[Index],    PchPcieConfig->RootPort[TbtSelector].PcieRpCommonConfig.PcieRpLtrConfig.SnoopLatencyOverrideMultiplier,    2);
          UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.PcieRpSnoopLatencyOverrideValue[Index],         PchPcieConfig->RootPort[TbtSelector].PcieRpCommonConfig.PcieRpLtrConfig.SnoopLatencyOverrideValue,        85);
          UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.PcieRpNonSnoopLatencyOverrideMode[Index],       PchPcieConfig->RootPort[TbtSelector].PcieRpCommonConfig.PcieRpLtrConfig.NonSnoopLatencyOverrideMode,       1);
          UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.PcieRpNonSnoopLatencyOverrideMultiplier[Index], PchPcieConfig->RootPort[TbtSelector].PcieRpCommonConfig.PcieRpLtrConfig.NonSnoopLatencyOverrideMultiplier, 2);
          UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.PcieRpNonSnoopLatencyOverrideValue[Index],      PchPcieConfig->RootPort[TbtSelector].PcieRpCommonConfig.PcieRpLtrConfig.NonSnoopLatencyOverrideValue,     85);
        }
      }
    }
  }
#endif

  //
  // PCI express config
  //
  UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.PcieComplianceTestMode,           PchPcieConfig->PcieCommonConfig.ComplianceTestMode,         PchSetup->PcieComplianceTestMode);
  UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.PcieRpFunctionSwap,               PchPcieConfig->PcieCommonConfig.RpFunctionSwap,             PchSetup->RpFunctionSwap);

  for (Index = 0; Index < MaxPciePorts; Index++) {
    UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.PcieRpMaxPayload[Index],                    PchPcieConfig->RootPort[Index].PcieRpCommonConfig.MaxPayload,         PchPcieMaxPayload256);
    UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.PcieRpPhysicalSlotNumber[Index],            PchPcieConfig->RootPort[Index].PcieRpCommonConfig.PhysicalSlotNumber, (UINT8) Index);
    UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.PcieRpClkReqDetect[Index],                  PchPcieConfig->RootPort[Index].PcieRpCommonConfig.ClkReqDetect,       TRUE);
    UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.PcieRpAspm[Index],                          PchPcieConfig->RootPort[Index].PcieRpCommonConfig.Aspm,                          PchSetup->PcieRootPortAspm[Index]);
    UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.PcieRpL1Substates[Index],                   PchPcieConfig->RootPort[Index].PcieRpCommonConfig.L1Substates,                   PchSetup->PcieRootPortL1SubStates[Index]);
    UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.PcieRpPcieSpeed[Index],                     PchPcieConfig->RootPort[Index].PcieRpCommonConfig.PcieSpeed,                     PchSetup->PcieRootPortSpeed[Index]);
    UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.PcieRpAcsEnabled[Index],                    PchPcieConfig->RootPort[Index].PcieRpCommonConfig.AcsEnabled,                    PchSetup->PcieRootPortACS[Index]);
    UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.PciePtm[Index],                             PchPcieConfig->RootPort[Index].PcieRpCommonConfig.PtmEnabled,                    PchSetup->PcieRootPortPTM[Index]);
    UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.PcieDpc[Index],                             PchPcieConfig->RootPort[Index].PcieRpCommonConfig.DpcEnabled,                    PchSetup->PcieRootPortDPC[Index]);
    UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.PcieEdpc[Index],                            PchPcieConfig->RootPort[Index].PcieRpCommonConfig.RpDpcExtensionsEnabled,        PchSetup->PcieRootPortEDPC[Index]);
    UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.PcieRpSlotImplemented[Index],               PchPcieConfig->RootPort[Index].PcieRpCommonConfig.SlotImplemented,               PchSetup->PcieRootPortSI[Index]);
    UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.PcieRpPmSci[Index],                         PchPcieConfig->RootPort[Index].PcieRpCommonConfig.PmSci,                         PchSetup->PcieRootPortPMCE[Index]);
    UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.PcieRpHotPlug[Index],                       PchPcieConfig->RootPort[Index].PcieRpCommonConfig.HotPlug,                       PchSetup->PcieRootPortHPE[Index]);
    UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.PcieRpAdvancedErrorReporting[Index],        PchPcieConfig->RootPort[Index].PcieRpCommonConfig.AdvancedErrorReporting,        PchSetup->PcieRootPortAER[Index]);
    UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.PcieRpUnsupportedRequestReport[Index],      PchPcieConfig->RootPort[Index].PcieRpCommonConfig.UnsupportedRequestReport,      PchSetup->PcieRootPortURE[Index]);
    UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.PcieRpFatalErrorReport[Index],              PchPcieConfig->RootPort[Index].PcieRpCommonConfig.FatalErrorReport,              PchSetup->PcieRootPortFEE[Index]);
    UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.PcieRpNoFatalErrorReport[Index],            PchPcieConfig->RootPort[Index].PcieRpCommonConfig.NoFatalErrorReport,            PchSetup->PcieRootPortNFE[Index]);
    UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.PcieRpCorrectableErrorReport[Index],        PchPcieConfig->RootPort[Index].PcieRpCommonConfig.CorrectableErrorReport,        PchSetup->PcieRootPortCEE[Index]);
    UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.PcieRpSystemErrorOnFatalError[Index],       PchPcieConfig->RootPort[Index].PcieRpCommonConfig.SystemErrorOnFatalError,       PchSetup->PcieRootPortSFE[Index]);
    UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.PcieRpSystemErrorOnNonFatalError[Index],    PchPcieConfig->RootPort[Index].PcieRpCommonConfig.SystemErrorOnNonFatalError,    PchSetup->PcieRootPortSNE[Index]);
    UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.PcieRpSystemErrorOnCorrectableError[Index], PchPcieConfig->RootPort[Index].PcieRpCommonConfig.SystemErrorOnCorrectableError, PchSetup->PcieRootPortSCE[Index]);
    UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.PcieRpTransmitterHalfSwing[Index],          PchPcieConfig->RootPort[Index].PcieRpCommonConfig.TransmitterHalfSwing,          PchSetup->PcieRootPortTHS[Index]);
    UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.PcieRpDetectTimeoutMs[Index],               PchPcieConfig->RootPort[Index].PcieRpCommonConfig.DetectTimeoutMs,               PchSetup->PcieDetectTimeoutMs[Index]);
    UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.PcieEnablePeerMemoryWrite[Index],           PchPcieConfig->RootPort[Index].PcieRpCommonConfig.EnablePeerMemoryWrite,         PchSetup->PcieRootPortPeerMemoryWriteEnable[Index]);
  }

  for (RpIndex = 0; RpIndex < GetPchMaxPciePortNum (); RpIndex++) {
#if FixedPcdGet8(PcdFspModeSelection) == 1
    if (IsPchLp () || IsPchP () || IsPchN ()) {
      ((FSPS_UPD *) FspsUpd)->FspsConfig.PcieRpLtrMaxSnoopLatency[RpIndex]   = 0x1003;
      ((FSPS_UPD *) FspsUpd)->FspsConfig.PcieRpLtrMaxNoSnoopLatency[RpIndex] = 0x1003;
    } else if (IsPchH ()) {
      ((FSPS_UPD *) FspsUpd)->FspsConfig.PcieRpLtrMaxSnoopLatency[RpIndex]   = 0x0846;
      ((FSPS_UPD *) FspsUpd)->FspsConfig.PcieRpLtrMaxNoSnoopLatency[RpIndex] = 0x0846;
    }
#endif
    UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.PcieRpLtrEnable[RpIndex],     PchPcieConfig->RootPort[RpIndex].PcieRpCommonConfig.LtrEnable,                     PchSetup->PchPcieLtrEnable[RpIndex]);
    UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.PcieRpLtrConfigLock[RpIndex], PchPcieConfig->RootPort[RpIndex].PcieRpCommonConfig.PcieRpLtrConfig.LtrConfigLock, PchSetup->PchPcieLtrConfigLock[RpIndex]);

  //
  // Update EQ settigns
  //
    UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.PcieEqMethod,                             PchPcieConfig->RootPort[RpIndex].PcieRpCommonConfig.PcieGen3LinkEqPlatformSettings.PcieLinkEqMethod,                   PchSetup->PcieEqMethod);
    UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.PcieEqMode,                               PchPcieConfig->RootPort[RpIndex].PcieRpCommonConfig.PcieGen3LinkEqPlatformSettings.PcieLinkEqMode,                     PchSetup->PcieEqMode);
    UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.PcieEqLocalTransmitterOverrideEnable,     PchPcieConfig->RootPort[RpIndex].PcieRpCommonConfig.PcieGen3LinkEqPlatformSettings.LocalTransmitterOverrideEnable,     PchSetup->PcieEqPh2LocalTransmitterOverrideEnable);
    UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.PcieEqPh1DownstreamPortTransmitterPreset, PchPcieConfig->RootPort[RpIndex].PcieRpCommonConfig.PcieGen3LinkEqPlatformSettings.Ph1DownstreamPortTransmitterPreset, PchSetup->PcieEqPh1DownstreamPortTransmitterPreset);
    UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.PcieEqPh1UpstreamPortTransmitterPreset,   PchPcieConfig->RootPort[RpIndex].PcieRpCommonConfig.PcieGen3LinkEqPlatformSettings.Ph1UpstreamPortTransmitterPreset,   PchSetup->PcieEqPh1UpstreamPortTransmitterPreset);
    UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.PcieEqPh3NumberOfPresetsOrCoefficients,   PchPcieConfig->RootPort[RpIndex].PcieRpCommonConfig.PcieGen3LinkEqPlatformSettings.Ph3NumberOfPresetsOrCoefficients,   PchSetup->PcieEqPh3NumberOfPresetsOrCoefficients);
    for (Index = 0; Index < PCIE_LINK_EQ_COEFFICIENTS_MAX; Index++) {
      UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.PcieEqPh3PreCursorList[Index],  PchPcieConfig->RootPort[RpIndex].PcieRpCommonConfig.PcieGen3LinkEqPlatformSettings.Ph3CoefficientsList[Index].PreCursor,  PchSetup->PcieEqPh3PreCursorList[Index]);
      UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.PcieEqPh3PostCursorList[Index], PchPcieConfig->RootPort[RpIndex].PcieRpCommonConfig.PcieGen3LinkEqPlatformSettings.Ph3CoefficientsList[Index].PostCursor, PchSetup->PcieEqPh3PostCursorList[Index]);
    }
    for (Index = 0; Index < PCIE_LINK_EQ_PRESETS_MAX; Index++) {
      UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.PcieEqPh3PresetList[Index], PchPcieConfig->RootPort[RpIndex].PcieRpCommonConfig.PcieGen3LinkEqPlatformSettings.Ph3PresetList[Index], PchSetup->PcieEqPh3PresetList[Index]);
    }
    if (PchSetup->PcieEqPh2LocalTransmitterOverrideEnable) {
      UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.PcieEqPh2LocalTransmitterOverridePreset, PchPcieConfig->RootPort[RpIndex].PcieRpCommonConfig.PcieGen3LinkEqPlatformSettings.Ph2LocalTransmitterOverridePreset, PchSetup->PcieEqPh2LocalTransmitterOverridePreset);
    }
      UPDATE_POLICY (((FSPS_UPD *)FspsUpd)->FspsConfig.PcieEqOverrideDefault, PchPcieConfig->RootPort[RpIndex].PcieRpCommonConfig.OverrideEqualizationDefaults, PchSetup->PcieEqOverrideDefault);
  }
}

/**
  Update ISH config

  @param[in] SiPolicy             Pointer to SI_POLICY_PPI
  @param[in] FspsUpd              Pointer to FspsUpd structure
  @param[in] PchSetup             Pointer to PCH_SETUP buffer
**/
STATIC
VOID
UpdateIshConfig (
  IN SI_POLICY_PPI             *SiPolicy,
  IN VOID                      *FspsUpd,
  IN PCH_SETUP                 *PchSetup
  )
{
  UINT8                           Index;

#if FixedPcdGet8(PcdFspModeSelection) == 0
  ISH_CONFIG                      *IshConfig;
  EFI_STATUS                      Status;

  Status = GetConfigBlock ((VOID *) SiPolicy, &gIshConfigGuid, (VOID *) &IshConfig);
  ASSERT_EFI_ERROR (Status);
  if (EFI_ERROR (Status)) {
    return;
  }
#endif

  for (Index = 0; Index < GetPchMaxIshSpiControllersNum (); Index++) {
    UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.PchIshSpiEnable[Index],    IshConfig->Spi[Index].Enable,      PchSetup->PchIshSpiEnable[Index]);
    UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.PchIshSpiCs0Enable[Index], IshConfig->Spi[Index].CsEnable[0], PchSetup->PchIshSpiCs0Enable[Index]);
  }
  for (Index = 0; Index < GetPchMaxIshUartControllersNum (); Index++) {
    UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.PchIshUartEnable[Index],   IshConfig->Uart[Index].Enable,     PchSetup->PchIshUartEnable[Index]);
  }
  for (Index = 0; Index < GetPchMaxIshI2cControllersNum (); Index++) {
    UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.PchIshI2cEnable[Index],    IshConfig->I2c[Index].Enable,      PchSetup->PchIshI2cEnable[Index]);
  }
  for (Index = 0; Index < GetPchMaxIshGpNum (); Index++) {
    UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.PchIshGpEnable[Index],     IshConfig->Gp[Index].Enable,       PchSetup->PchIshGpEnable[Index]);
  }
  if (IsPchP () || IsPchN ()) {
    if (IsPchM ()) {
      UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.IshGpGpioPinMuxing[4],   IshConfig->Gp[4].PinConfig.PinMux,     GPIO_VER2_P_MUXING_ISH_GP_4_E9);
    } else {
      UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.IshGpGpioPinMuxing[4],   IshConfig->Gp[4].PinConfig.PinMux,     GPIO_VER2_P_MUXING_ISH_GP_4_B3);
    }
    UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.IshGpGpioPinMuxing[5],    IshConfig->Gp[5].PinConfig.PinMux,     GPIO_VER2_P_MUXING_ISH_GP_5_B4);
    UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.IshGpGpioPinMuxing[6],    IshConfig->Gp[6].PinConfig.PinMux,     GPIO_VER2_P_MUXING_ISH_GP_6_H12);
    UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.IshGpGpioPinMuxing[7],    IshConfig->Gp[7].PinConfig.PinMux,     GPIO_VER2_P_MUXING_ISH_GP_7_B15);
  }
  COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.PchIshPdtUnlock,             IshConfig->PdtUnlock,              PchSetup->PchIshPdtUnlock,           NullIndex);
}

/**
  Update THC config

  @param[in] SiPolicy             Pointer to SI_POLICY_PPI
  @param[in] FspsUpd              Pointer to FspsUpd structure
  @param[in] PchSetup             Pointer to PCH_SETUP buffer
**/
STATIC
VOID
UpdateThcConfig (
  IN SI_POLICY_PPI             *SiPolicy,
  IN VOID                      *FspsUpd,
  IN PCH_SETUP                 *PchSetup
  )
{
  UINT8       ThcIndex;
#if FixedPcdGet8(PcdFspModeSelection) == 0
  THC_CONFIG  *ThcConfig;
  EFI_STATUS  Status;

  Status = GetConfigBlock ((VOID *) SiPolicy, &gThcConfigGuid, (VOID *) &ThcConfig);
  ASSERT_EFI_ERROR (Status);
  if (EFI_ERROR (Status)) {
    return;
  }
#endif

  COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.ThcPort0Assignment,      ThcConfig->ThcPort[0].Assignment,      PchSetup->ThcPort0Assignment,  NullIndex);
  COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.ThcPort0WakeOnTouch,     ThcConfig->ThcPort[0].WakeOnTouch,     PchSetup->ThcPort0WakeOnTouch, NullIndex);
  COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.ThcPort1Assignment,      ThcConfig->ThcPort[1].Assignment,      PchSetup->ThcPort1Assignment,  NullIndex);
  COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.ThcPort1WakeOnTouch,     ThcConfig->ThcPort[1].WakeOnTouch,     PchSetup->ThcPort1WakeOnTouch, NullIndex);

  for (ThcIndex = 0; ThcIndex < PCH_MAX_THC_CONTROLLERS; ThcIndex++) {
    COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.ThcMode[ThcIndex]                       , ThcConfig->ThcPort[ThcIndex].Mode,                                 PchSetup->ThcMode[ThcIndex], ThcIndex);
    COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.ThcActiveLtr[ThcIndex]                  , ThcConfig->ThcPort[ThcIndex].ActiveLtr,                            PchSetup->ThcActiveLtr[ThcIndex], ThcIndex);
    COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.ThcIdleLtr[ThcIndex]                    , ThcConfig->ThcPort[ThcIndex].IdleLtr,                              PchSetup->ThcIdleLtr[ThcIndex], ThcIndex);
    COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.ThcLimitPacketSize[ThcIndex]            , ThcConfig->ThcPort[ThcIndex].LimitPacketSize ,                     PchSetup->ThcLimitPacketSize[ThcIndex], ThcIndex);
    COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.ThcPerformanceLimitation[ThcIndex]      , ThcConfig->ThcPort[ThcIndex].PerformanceLimitation,                PchSetup->ThcPerformanceLimitation[ThcIndex], ThcIndex);
    //
    // Hid Over SPI Reset Pad configuration is always 0x0, which indicates HW THC default.
    // Platform BIOS code can override it if Board design is not aligned with THC pads.
    //
    COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.ThcHidResetPad[ThcIndex]                , ThcConfig->ThcPort[ThcIndex].HidOverSpi.ResetPad,                  0x0, ThcIndex);
    COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.ThcHidResetPadTrigger[ThcIndex]         , ThcConfig->ThcPort[ThcIndex].HidOverSpi.ResetPadTrigger,           PchSetup->ThcHidResetPadTrigger[ThcIndex], ThcIndex);
    COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.ThcHidConnectionSpeed[ThcIndex]         , ThcConfig->ThcPort[ThcIndex].HidOverSpi.Frequency,                 PchSetup->ThcHidConnectionSpeed[ThcIndex], ThcIndex);
    COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.ThcHidInputReportHeaderAddress[ThcIndex], ThcConfig->ThcPort[ThcIndex].HidOverSpi.InputReportHeaderAddress,  PchSetup->ThcHidInputReportHeaderAddress[ThcIndex], ThcIndex);
    COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.ThcHidInputReportBodyAddress[ThcIndex]  , ThcConfig->ThcPort[ThcIndex].HidOverSpi.InputReportBodyAddress,    PchSetup->ThcHidInputReportBodyAddress[ThcIndex], ThcIndex);
    COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.ThcHidOutputReportAddress[ThcIndex]     , ThcConfig->ThcPort[ThcIndex].HidOverSpi.OutputReportAddress,       PchSetup->ThcHidOutputReportAddress[ThcIndex], ThcIndex);
    COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.ThcHidReadOpcode[ThcIndex]              , ThcConfig->ThcPort[ThcIndex].HidOverSpi.ReadOpcode,                PchSetup->ThcHidReadOpcode[ThcIndex], ThcIndex);
    COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.ThcHidWriteOpcode[ThcIndex]             , ThcConfig->ThcPort[ThcIndex].HidOverSpi.WriteOpcode,               PchSetup->ThcHidWriteOpcode[ThcIndex], ThcIndex);
    COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.ThcHidFlags[ThcIndex]                   , ThcConfig->ThcPort[ThcIndex].HidOverSpi.Flags,                     PchSetup->ThcHidFlags[ThcIndex], ThcIndex);
  }
}

/**
  Update CNVi config

  @param[in] SiPolicy             Pointer to SI_POLICY_PPI
  @param[in] FspsUpd              Pointer to FspsUpd structure
  @param[in] PchSetup             Pointer to PCH_SETUP buffer
**/
STATIC
VOID
UpdateCnviConfig (
  IN SI_POLICY_PPI             *SiPolicy,
  IN VOID                      *FspsUpd,
  IN PCH_SETUP                 *PchSetup
  )
{
#if FixedPcdGet8(PcdFspModeSelection) == 0
  EFI_STATUS                      Status;
  CNVI_CONFIG                     *CnviConfig;

  CnviConfig = NULL;
  Status = GetConfigBlock ((VOID *) SiPolicy, &gCnviConfigGuid, (VOID *) &CnviConfig);

  if (EFI_ERROR (Status) || CnviConfig == NULL) {
    return;
  }
#endif

  COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.CnviMode,           CnviConfig->Mode,           PchSetup->CnviMode,           NullIndex);
  COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.CnviWifiCore,       CnviConfig->WifiCore,       PchSetup->CnviWifiCore,       NullIndex);
  COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.CnviBtCore,         CnviConfig->BtCore,         PchSetup->CnviBtCore,         NullIndex);
  COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.CnviBtAudioOffload, CnviConfig->BtAudioOffload, PchSetup->CnviBtAudioOffload, NullIndex);
  // @note: this config needs to be based on board routing.
  if (IsPchLp ()) {
    UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.CnviRfResetPinMux, CnviConfig->PinMux.RfReset, GPIO_VER2_LP_MUXING_CNVI_RF_RESET_GPP_F4);
    UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.CnviClkreqPinMux,  CnviConfig->PinMux.Clkreq, GPIO_VER2_LP_MUXING_CNVI_MODEM_CLKREQ_GPP_F5);
  }
}

/**
  Update Thermal config

  @param[in] SiPolicy             Pointer to SI_POLICY_PPI
  @param[in] FspsUpd              Pointer to FspsUpd structure
  @param[in] PchSetup             Pointer to PCH_SETUP buffer
  @param[in] SaSetup              Pointer to SA_SETUP
**/
STATIC
VOID
UpdateThermalConfig (
  IN SI_POLICY_PPI             *SiPolicy,
  IN VOID                      *FspsUpd,
  IN PCH_SETUP                 *PchSetup,
  IN SA_SETUP                  *SaSetup
  )
{
#if FixedPcdGet8(PcdFspModeSelection) == 0
  THERMAL_CONFIG                  *ThermalConfig;
  EFI_STATUS                      Status;

  Status = GetConfigBlock ((VOID *) SiPolicy, &gThermalConfigGuid, (VOID *) &ThermalConfig);
  ASSERT_EFI_ERROR (Status);
  if (EFI_ERROR (Status)) {
    return;
  }
#endif

  UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.PchMemoryPmsyncEnable[TsGpioC],     ThermalConfig->MemoryThrottling.TsGpioPinSetting[TsGpioC].PmsyncEnable,     TRUE);
  UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.PchMemoryPmsyncEnable[TsGpioD],     ThermalConfig->MemoryThrottling.TsGpioPinSetting[TsGpioD].PmsyncEnable,     TRUE);
  UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.PchMemoryC0TransmitEnable[TsGpioC], ThermalConfig->MemoryThrottling.TsGpioPinSetting[TsGpioC].C0TransmitEnable, TRUE);
  UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.PchMemoryC0TransmitEnable[TsGpioD], ThermalConfig->MemoryThrottling.TsGpioPinSetting[TsGpioD].C0TransmitEnable, TRUE);

  if ((SaSetup->MemoryThermalManagement != FALSE) && ((SaSetup->ExttsViaTsOnBoard != FALSE) || (SaSetup->ExttsViaTsOnDimm != FALSE))) {
    UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.PchMemoryThrottlingEnable,          ThermalConfig->MemoryThrottling.Enable,                                     TRUE);
    UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.PchMemoryPmsyncEnable[TsGpioC],     ThermalConfig->MemoryThrottling.TsGpioPinSetting[TsGpioC].PmsyncEnable,     PcdGetBool (PcdMemoryThermalSensorGpioCPmsyncEnable));
    UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.PchMemoryPmsyncEnable[TsGpioD],     ThermalConfig->MemoryThrottling.TsGpioPinSetting[TsGpioD].PmsyncEnable,     PcdGetBool (PcdMemoryThermalSensorGpioDPmsyncEnable));
    UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.PchMemoryC0TransmitEnable[TsGpioC], ThermalConfig->MemoryThrottling.TsGpioPinSetting[TsGpioC].C0TransmitEnable, TRUE);
    UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.PchMemoryC0TransmitEnable[TsGpioD], ThermalConfig->MemoryThrottling.TsGpioPinSetting[TsGpioD].C0TransmitEnable, TRUE);
    UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.PchMemoryPinSelection[TsGpioC],     ThermalConfig->MemoryThrottling.TsGpioPinSetting[TsGpioC].PinSelection,     1);
    UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.PchMemoryPinSelection[TsGpioD],     ThermalConfig->MemoryThrottling.TsGpioPinSetting[TsGpioD].PinSelection,     0);
  } else {
    UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.PchMemoryThrottlingEnable,          ThermalConfig->MemoryThrottling.Enable,                                     FALSE);
  }

  UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.PchHotEnable, ThermalConfig->PchHotEnable, PcdGetBool (PcdPchThermalHotEnable));

  //
  // Program Thermal Throttling Level
  //
  COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.TTSuggestedSetting,          ThermalConfig->TTLevels.SuggestedSetting,   PchSetup->PchTtLevelSuggestSet,   NullIndex);
  COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.TTCrossThrottling,           ThermalConfig->TTLevels.PchCrossThrottling, PchSetup->PchCrossThrottling,     NullIndex);
  COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.PchTTLock,                   ThermalConfig->TTLevels.TTLock,             PchSetup->PchThrmTtLock,          NullIndex);
  COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.PchTTState13Enable,          ThermalConfig->TTLevels.TTState13Enable,    PchSetup->PchThrmTtState13Enable, NullIndex);
  COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.PchTTEnable,                 ThermalConfig->TTLevels.TTEnable,           PchSetup->PchThrmTtEnable,        NullIndex);
  COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.PchT2Level,                  ThermalConfig->TTLevels.T2Level,            PchSetup->PchThrmT2Level,         NullIndex);
  COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.PchT1Level,                  ThermalConfig->TTLevels.T1Level,            PchSetup->PchThrmT1Level,         NullIndex);
  COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.PchT0Level,                  ThermalConfig->TTLevels.T0Level,            PchSetup->PchThrmT0Level,         NullIndex);

  //
  // Program DMI Thermal Throttling
  //
  COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.DmiSuggestedSetting, ThermalConfig->DmiHaAWC.SuggestedSetting, PchSetup->PchDmiTsSuggestSet, NullIndex);
  COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.DmiTS3TW,            ThermalConfig->DmiHaAWC.TS3TW,            PchSetup->PchTs3Width,        NullIndex);
  COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.DmiTS2TW,            ThermalConfig->DmiHaAWC.TS2TW,            PchSetup->PchTs2Width,        NullIndex);
  COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.DmiTS1TW,            ThermalConfig->DmiHaAWC.TS1TW,            PchSetup->PchTs1Width,        NullIndex);
  COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.DmiTS0TW,            ThermalConfig->DmiHaAWC.TS0TW,            PchSetup->PchTs0Width,        NullIndex);
  COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.PchDmiTsawEn,        ThermalConfig->DmiHaAWC.DmiTsawEn,        PchSetup->PchDmiTsawEn,       NullIndex);
}

/**
  Update GBE config

  @param[in] SiPolicy             Pointer to SI_POLICY_PPI
  @param[in] FspsUpd              Pointer to FspsUpd structure
  @param[in] PchSetup             Pointer to PCH_SETUP buffer
**/
STATIC
VOID
UpdateGbeConfig (
  IN SI_POLICY_PPI             *SiPolicy,
  IN VOID                      *FspsUpd,
  IN PCH_SETUP                 *PchSetup
  )
{
#if FixedPcdGet8(PcdFspModeSelection) == 0
  GBE_CONFIG                   *GbeConfig;
  EFI_STATUS                   Status;

  Status = GetConfigBlock ((VOID *) SiPolicy, &gGbeConfigGuid, (VOID *) &GbeConfig);
  ASSERT_EFI_ERROR (Status);
  if (EFI_ERROR (Status)) {
    return;
  }
#endif
  //
  // LAN Config
  // Check Gbe Region to determine if hide LAN controller setup option
  //
  if (IsGbePresent ()) {
    COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.PchLanEnable, GbeConfig->Enable, PchSetup->PchLan, NullIndex);
  } else {
    UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.PchLanEnable, GbeConfig->Enable, FALSE);
  }
}

#if FixedPcdGetBool (PcdEmbeddedEnable)
/**
  Update PCH TSN config

  @param[in] SiPolicy             Pointer to SI_POLICY_PPI
  @param[in] FspsUpd              Pointer to FspsUpd structure
  @param[in] PchSetup             Pointer to PCH_SETUP buffer
**/
STATIC
VOID
UpdateTsnConfig (
  IN SI_POLICY_PPI             *SiPolicy,
  IN VOID                      *FspsUpd,
  IN PCH_SETUP                 *PchSetup
  )
{
  TSN_CONFIG               *TsnConfig;
  TSN_SUB_REGION           *TsnSubRegion;

#if FixedPcdGet8(PcdFspModeSelection) == 0
  EFI_STATUS                   Status;

  Status = GetConfigBlock ((VOID *) SiPolicy, &gTsnConfigGuid, (VOID *) &TsnConfig);
  ASSERT_EFI_ERROR (Status);
  if (EFI_ERROR (Status)) {
    return;
  }
#else
  TsnConfig = NULL;
#endif

  // Acquire MAC Addresses from TSN Sub Region
  InstallTsnFv ();
  GetTsnSubRegion (&TsnSubRegion);

  UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.PchTsnMacAddressHigh,  TsnConfig->TsnMacAddressHigh, TsnSubRegion->Port.MacAddressHigh);
  UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.PchTsnMacAddressLow,   TsnConfig->TsnMacAddressLow,  TsnSubRegion->Port.MacAddressLow);
  COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.PchTsnEnable,        TsnConfig->Enable,        PchSetup->PchTsnEnable,     NullIndex);
  COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.PchTsnLinkSpeed,     TsnConfig->TsnLinkSpeed,  PchSetup->PchTsnLinkSpeed,  NullIndex);
  COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.PchTsnMultiVcEnable, TsnConfig->MultiVcEnable, PchSetup->TsnMultiVcEnable, NullIndex);
}
#endif

/**
  Update DMI config

  @param[in] SiPolicy             Pointer to SI_POLICY_PPI
  @param[in] FspsUpd              Pointer to FspsUpd structure
  @param[in] PchSetup             Pointer to PCH_SETUP buffer
**/
STATIC
VOID
UpdateDmiConfig (
  IN SI_POLICY_PPI             *SiPolicy,
  IN VOID                      *FspsUpd,
  IN PCH_SETUP                 *PchSetup
  )
{
#if FixedPcdGet8(PcdFspModeSelection) == 0
  PCH_DMI_CONFIG                  *DmiConfig;
  EFI_STATUS                      Status;

  Status = GetConfigBlock ((VOID *) SiPolicy, &gPchDmiConfigGuid, (VOID *) &DmiConfig);
  ASSERT_EFI_ERROR (Status);
  if (EFI_ERROR (Status)) {
    return;
  }
#endif

  COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.PchDmiAspmCtrl, DmiConfig->DmiAspmCtrl, PchSetup->PchDmiAspm, NullIndex);
}

/**
  Get Bios Guard variable

  @param[in] FspmUpd                   Pointer to FSPM_UPD struct
  @param[in] CpuSecurityPreMemConfig   Pointer to CPU Security PreMem Config
**/
STATIC
UINT32
GetBiosGuardEnable (
  IN VOID                        *FspmUpd,
  IN CPU_SECURITY_PREMEM_CONFIG  *CpuSecurityPreMemConfig
  )
{
  UINT32 BiosGuard;
  GET_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.BiosGuard, CpuSecurityPreMemConfig->BiosGuard, BiosGuard);

  return BiosGuard;
}

/**
  Update LockDown config

  @param[in] SiPolicy             Pointer to SI_POLICY_PPI
  @param[in] FspsUpd              Pointer to FspsUpd structure
  @param[in] PchSetup             Pointer to PCH_SETUP buffer
  @param[in] BiosGuardEnable      BIOS Guard enable
**/
STATIC
VOID
UpdateLockDownConfig (
  IN SI_POLICY_PPI             *SiPolicy,
  IN VOID                      *FspsUpd,
  IN PCH_SETUP                 *PchSetup,
  IN UINT32                    BiosGuardEnable
  )
{
//[-start-201112-IB16810138-remove]//
//#if FixedPcdGetBool(PcdCapsuleEnable) == 1
//[-end-201112-IB16810138-remove]//
  EFI_HOB_GUID_TYPE               *GuidHob;

  GuidHob                 = NULL;
//[-start-201112-IB16810138-remove]//
//#endif
//[-end-201112-IB16810138-remove]//
#if FixedPcdGet8(PcdFspModeSelection) == 0
  PCH_LOCK_DOWN_CONFIG            *LockDownConfig;
  EFI_STATUS                      Status;

  Status = GetConfigBlock ((VOID *) SiPolicy, &gLockDownConfigGuid, (VOID *) &LockDownConfig);
  ASSERT_EFI_ERROR (Status);
  if (EFI_ERROR (Status)) {
    return;
  }
#endif

  if (BiosGuardEnable) {
    DEBUG ((DEBUG_INFO, "Enable InSMM.STS (EISS) and BIOS Lock Enable (BLE) since BIOS Guard is enabled.\n"));
    UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.PchLockDownBiosLock, LockDownConfig->BiosLock, TRUE);
  } else {
    COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.PchLockDownBiosLock, LockDownConfig->BiosLock, PchSetup->PchBiosLock, NullIndex);
  }
//[-start-201112-IB16810138-modify]//
//#if FixedPcdGetBool(PcdCapsuleEnable) == 1
  if (PcdGet8 (PcdChasmFallsSupport) != 0) {
//[-end-201112-IB16810138-modify]//
    GuidHob = GetFirstGuidHob (&gSysFwUpdateProgressGuid);

    if (IsBiosUpdateRequired () || ((GetBootModeHob () == BOOT_IN_RECOVERY_MODE) &&
       ((((SYSTEM_FIRMWARE_UPDATE_PROGRESS *) GET_GUID_HOB_DATA (GuidHob))->Component == UpdatinguCode)||
       (((SYSTEM_FIRMWARE_UPDATE_PROGRESS *) GET_GUID_HOB_DATA (GuidHob))->Component == UpdatingBtGAcm)))) {
      DEBUG ((DEBUG_INFO, "Disabling BIOS lock for BIOS Update Process\n"));
      PchSetup->PchBiosLock = 0;
      COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.PchLockDownBiosLock, LockDownConfig->BiosLock, PchSetup->PchBiosLock, NullIndex);
      DEBUG ((DEBUG_INFO, "Disabling BIOS interface lockdown for supporting Fault tolerant BIOS Update\n"));
      UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.PchLockDownBiosInterface, LockDownConfig->BiosInterface, FALSE);
    }
//[-start-201112-IB16810138-modify]//
  }
//#endif
//[-end-201112-IB16810138-modify]//
  COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.PchUnlockGpioPads, LockDownConfig->UnlockGpioPads, PchSetup->UnlockGpioPads, NullIndex);
}

/**
  Update Pm config

  @param[in] SiPolicy             Pointer to SI_POLICY_PPI
  @param[in] FspsUpd              Pointer to FspsUpd structure
  @param[in] PchSetup             Pointer to PCH_SETUP buffer
  @param[in] SetupVariables       Pointer to SETUP_DATA buffer
**/
STATIC
VOID
UpdatePmConfig (
  IN SI_POLICY_PPI             *SiPolicy,
  IN VOID                      *FspsUpd,
  IN PCH_SETUP                 *PchSetup,
  IN SETUP_DATA                *SetupVariables
  )
{
#if FixedPcdGet8(PcdFspModeSelection) == 0
  PCH_PM_CONFIG                   *PmConfig;
  ADR_CONFIG                      *PmcAdrConfig;
  EFI_STATUS                      Status;

  Status = GetConfigBlock ((VOID *) SiPolicy, &gPmConfigGuid, (VOID *) &PmConfig);
  ASSERT_EFI_ERROR (Status);
  if (EFI_ERROR (Status)) {
    return;
  }

  Status = GetConfigBlock ((VOID*) SiPolicy, &gAdrConfigGuid, (VOID*) &PmcAdrConfig);
  ASSERT_EFI_ERROR (Status);
  if (EFI_ERROR (Status)) {
    return;
  }

#endif
  COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.PchPmDeepSxPol,          PmConfig->PchDeepSxPol,                  PchSetup->DeepSxMode,          NullIndex);
  COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.PchPmWolEnableOverride,  PmConfig->WakeConfig.WolEnableOverride,  PchSetup->PchWakeOnLan,        NullIndex);
  //
  // M.2 WiFi/BT slot power needs to be kept during Sx for CNVi. Hence overriding PchPmWoWlanEnable to 1 when CNVi is present.
  //
  if (CnviIsPresent () == TRUE) {
    UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.PchPmWoWlanEnable, PmConfig->WakeConfig.WoWlanEnable, 1);
  } else {
    COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.PchPmWoWlanEnable, PmConfig->WakeConfig.WoWlanEnable, PchSetup->PchWakeOnWlan, NullIndex);
  }
  COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.PchPmWoWlanDeepSxEnable, PmConfig->WakeConfig.WoWlanDeepSxEnable, PchSetup->PchWakeOnWlanDeepSx, NullIndex);
  COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.PchPmLanWakeFromDeepSx , PmConfig->WakeConfig.LanWakeFromDeepSx,  PchSetup->LanWakeFromDeepSx,   NullIndex);
  COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.PchPmSlpLanLowDc,        PmConfig->SlpLanLowDc,                   PchSetup->SlpLanLowDc,         NullIndex);
  COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.PsOnEnable,              PmConfig->PsOnEnable,                    PchSetup->PsOnEnable,          NullIndex);

  // Power Button Override Period (PBOP): refer to EDS for detail
  // Encoding:
  // 000b - 4 seconds
  // 011b - 10 seconds
  // power button behavior will depend on this setting and other features
  // such as power button SMI which will shutdown system immediately when it is enabled in pre-OS.
//[-start-210618-BAIN000015-modify]//
#ifdef LCFC_SUPPORT
  UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.PchPmPwrBtnOverridePeriod, PmConfig->PwrBtnOverridePeriod, !!SetupVariables->LowPowerS0Idle? 0x0 : 0);
#else
  UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.PchPmPwrBtnOverridePeriod, PmConfig->PwrBtnOverridePeriod, !!SetupVariables->LowPowerS0Idle? 0x3 : 0);
#endif
//[-end-210618-BAIN000015-modify]//
  COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.PchPmDisableDsxAcPresentPulldown, PmConfig->DisableDsxAcPresentPulldown, PchSetup->DisableDsxAcPresentPulldown, NullIndex);
  UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.PchPmMeWakeSts,              PmConfig->MeWakeSts,             TRUE);
  COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.PchPmPciePllSsc,                  PmConfig->PciePllSsc,                  PchSetup->PciePllSsc,                  NullIndex);
  COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.EnableTcoTimer,                   PmConfig->EnableTcoTimer,              PchSetup->EnableTcoTimer,              NullIndex);
#if FixedPcdGet8(PcdEmbeddedEnable) == 0x1
  COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.EnableTimedGpio0,                 PmConfig->EnableTimedGpio0,            PchSetup->EnableTimedGpio0,            NullIndex);
  COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.EnableTimedGpio1,                 PmConfig->EnableTimedGpio1,            PchSetup->EnableTimedGpio1,            NullIndex);
#endif
  COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.PmcDbgMsgEn,                      PmConfig->PmcDbgMsgEn,                 PchSetup->PmcDbgMsgEn,                 NullIndex);
  UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.PchPmVrAlert,                PmConfig->VrAlert,               PcdGetBool (PcdVrAlertEnable));
  UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.PmcCpuC10GatePinEnable,      PmConfig->CpuC10GatePinEnable,   1);
  UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.PmcModPhySusPgEnable,        PmConfig->ModPhySusPgEnable,     IsPchLp ());
  if (IsPchLp ()) {
    COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.PmcUsb2PhySusPgEnable, PmConfig->Usb2PhySusPgEnable, PchSetup->PchUsb2SusWellPgEnable, NullIndex);
  } else {
    UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.PmcUsb2PhySusPgEnable, PmConfig->Usb2PhySusPgEnable, 0);
  }
  COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.PmcOsIdleEnable,                  PmConfig->OsIdleEnable,                  PchSetup->PchPmcOsIdleModeEnable,           NullIndex);
  COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.PchS0ixAutoDemotion,              PmConfig->S0ixAutoDemotion,              PchSetup->PchS0ixAutoDemotion,              NullIndex);
  COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.PchPmLatchEventsC10Exit,          PmConfig->LatchEventsC10Exit,            PchSetup->PchLatchEventsC10Exit,            NullIndex);
  COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.PmcC10DynamicThresholdAdjustment, PmConfig->C10DynamicThresholdAdjustment, PchSetup->PmcC10DynamicThresholdAdjustment, NullIndex);

  COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.PmcAdrEn,                         PmcAdrConfig->AdrEn,                     PchSetup->PmcAdrEn,                         NullIndex);
  COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.PmcSkipVccInConfig,               PmConfig->SkipVccInConfig,               PchSetup->PchSkipVccInConfig,               NullIndex);
  //
  // Below setting will force the RC to always update the timer value to setup values. This is needed as neither HW nor RC
  // know what the value of the timers should be(depends on the memory module on platform.)
  //
  UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.PmcAdrTimerEn, PmcAdrConfig->AdrTimerEn, FORCE_ENABLE);
  UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.PmcAdrSrcOverride, PmcAdrConfig->AdrSrcOverride, FALSE);
  UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.PmcAdrTimer1Val,          PmcAdrConfig->AdrTimer1Val,          PchSetup->PmcAdrTimer1Val);
  UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.PmcAdrMultiplier1Val,     PmcAdrConfig->AdrMultiplier1Val,     PchSetup->PmcAdrMultiplier1Val);
  UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.PmcAdrHostPartitionReset, PmcAdrConfig->AdrHostPartitionReset, PchSetup->PmcAdrHostPartitionReset);

}

#if FixedPcdGetBool(PcdAdlLpSupport) == 1
/**
  Update supported configurations for Vnn Rail.
  @param[in] PchSetup             Pointer to PCH_SETUP buffer
**/
STATIC
VOID
GetExternalVnnSupportedVoltageStates (
  IN PCH_SETUP         *PchSetup
)
{
  switch (PchSetup->ExternalVnnSupportedVoltageConfigurations) {
    case VnnRailVoltageConfig0:                             // 0.7V@Bypass-0.75V@Internal-1.05V@Internal
      PchSetup->ExternalVnnSupportedVoltageStates[0] = 0; // VNN_RET_ACTIVE_SWITCH_SUPPORT
      PchSetup->ExternalVnnSupportedVoltageStates[1] = 0; // VNN_NORM_ACTIVE_VOLT_SUPPORTED
      PchSetup->ExternalVnnSupportedVoltageStates[2] = 0; // VNN_MIN_ACTIVE_VOLT_SUPPORTED
      PchSetup->ExternalVnnSupportedVoltageStates[3] = 1; // VNN_MIN_RET_VOLT_SUPPORTED
      return;
    case VnnRailVoltageConfig1:                             // 0.78V@Bypass-0.78V@Bypass-1.05V@Internal
      PchSetup->ExternalVnnSupportedVoltageStates[0] = 0; // VNN_RET_ACTIVE_SWITCH_SUPPORT
      PchSetup->ExternalVnnSupportedVoltageStates[1] = 0; // VNN_NORM_ACTIVE_VOLT_SUPPORTED
      PchSetup->ExternalVnnSupportedVoltageStates[2] = 1; // VNN_MIN_ACTIVE_VOLT_SUPPORTED
      PchSetup->ExternalVnnSupportedVoltageStates[3] = 0; // VNN_MIN_RET_VOLT_SUPPORTED
      return;
    case VnnRailVoltageConfig2:                             // 0.7V@Bypass-0.75V@Bypass-1.05V@Internal
      PchSetup->ExternalVnnSupportedVoltageStates[0] = 1; // VNN_RET_ACTIVE_SWITCH_SUPPORT
      PchSetup->ExternalVnnSupportedVoltageStates[1] = 0; // VNN_NORM_ACTIVE_VOLT_SUPPORTED
      PchSetup->ExternalVnnSupportedVoltageStates[2] = 1; // VNN_MIN_ACTIVE_VOLT_SUPPORTED
      PchSetup->ExternalVnnSupportedVoltageStates[3] = 1; // VNN_MIN_RET_VOLT_SUPPORTED
      return;
    case VnnRailVoltageConfig3:                             // 1.05V@Bypass-1.05V@Bypass-1.05V@Bypass
      PchSetup->ExternalVnnSupportedVoltageStates[0] = 0; // VNN_RET_ACTIVE_SWITCH_SUPPORT
      PchSetup->ExternalVnnSupportedVoltageStates[1] = 1; // VNN_NORM_ACTIVE_VOLT_SUPPORTED
      PchSetup->ExternalVnnSupportedVoltageStates[2] = 0; // VNN_MIN_ACTIVE_VOLT_SUPPORTED
      PchSetup->ExternalVnnSupportedVoltageStates[3] = 0; // VNN_MIN_RET_VOLT_SUPPORTED
      return;
    default:
      return;
  }
}

/**
  Update Fivr config

  @param[in] SiPolicy             Pointer to SI_POLICY_PPI
  @param[in] FspsUpd              Pointer to FspsUpd structure
  @param[in] PchSetup             Pointer to PCH_SETUP buffer
**/
STATIC
VOID
UpdateFivrConfig (
  IN SI_POLICY_PPI             *SiPolicy,
  IN VOID                      *FspsUpd,
  IN PCH_SETUP                 *PchSetup
  )
{
  UINT8                        Index;
#if FixedPcdGet8(PcdFspModeSelection) == 0
  PCH_FIVR_CONFIG              *FivrConfig;
  EFI_STATUS                   Status;

  Status = GetConfigBlock ((VOID *) SiPolicy, &gFivrConfigGuid, (VOID *) &FivrConfig);
  ASSERT_EFI_ERROR (Status);
  if (EFI_ERROR (Status)) {
    return;
  }
#endif

  UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.PchFivrExtV1p05RailEnabledStates,  FivrConfig->ExtV1p05Rail.EnabledStates, 0);
  UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.PchFivrExtVnnRailEnabledStates,    FivrConfig->ExtVnnRail.EnabledStates,   0);
  UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.PchFivrExtVnnRailSxEnabledStates,  FivrConfig->ExtVnnRailSx.EnabledStates, 0);
  for (Index = 0; Index < FIVR_RAIL_S0IX_SX_STATES_MAX; Index++) {
    OR_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.PchFivrExtV1p05RailEnabledStates,  FivrConfig->ExtV1p05Rail.EnabledStates, (PchSetup->ExternalV1p05StateEnable[Index] << Index));
    OR_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.PchFivrExtVnnRailEnabledStates,    FivrConfig->ExtVnnRail.EnabledStates,   (PchSetup->ExternalVnnStateEnable[Index] << Index));
    OR_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.PchFivrExtVnnRailSxEnabledStates,  FivrConfig->ExtVnnRailSx.EnabledStates, (PchSetup->ExternalVnnSxStateEnable[Index] << Index));
  }

  //
  // By default setting the V1P05_NORM_ACTIVE_VOLT_SUPPORTED
  //
  UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.PchFivrExtV1p05RailSupportedVoltageStates,  FivrConfig->ExtV1p05Rail.SupportedVoltageStates, 0x2);
  UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.PchFivrExtVnnRailSupportedVoltageStates,    FivrConfig->ExtVnnRail.SupportedVoltageStates,   0);
  if (IsPchP () || IsPchN ()) {
    GetExternalVnnSupportedVoltageStates (PchSetup);
  }
  for (Index = 0; Index < FIVR_RAIL_EXT_VOLTAGE_STATES_MAX; Index++) {
    OR_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.PchFivrExtVnnRailSupportedVoltageStates,    FivrConfig->ExtVnnRail.SupportedVoltageStates,   (PchSetup->ExternalVnnSupportedVoltageStates[Index] << Index));
  }
  COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.PchFivrExtV1p05RailIccMaximum,  FivrConfig->ExtV1p05Rail.IccMaximum,  PchSetup->ExternalV1p05IccMaximum, NullIndex);
  COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.PchFivrExtV1p05RailVoltage,     FivrConfig->ExtV1p05Rail.Voltage,     PchSetup->ExternalV1p05Voltage,    NullIndex);

  COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.PchFivrExtVnnRailIccMaximum,    FivrConfig->ExtVnnRail.IccMaximum,    PchSetup->ExternalVnnIccMaximum,   NullIndex);
  COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.PchFivrExtVnnRailVoltage,       FivrConfig->ExtVnnRail.Voltage,       PchSetup->ExternalVnnVoltage,      NullIndex);

  COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.PchFivrExtVnnRailSxIccMaximum,  FivrConfig->ExtVnnRailSx.IccMaximum,  PchSetup->ExternalVnnSxIccMaximum, NullIndex);
  COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.PchFivrExtVnnRailSxVoltage,     FivrConfig->ExtVnnRailSx.Voltage,     PchSetup->ExternalVnnSxVoltage,    NullIndex);

  COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.PchFivrVccinAuxLowToHighCurModeVolTranTime, FivrConfig->VccinAux.LowToHighCurModeVolTranTime, PchSetup->PchLowToHighCurModeVolTranTime, NullIndex);
  COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.PchFivrVccinAuxRetToHighCurModeVolTranTime, FivrConfig->VccinAux.RetToHighCurModeVolTranTime, PchSetup->PchRetToHighCurModeVolTranTime, NullIndex);
  COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.PchFivrVccinAuxRetToLowCurModeVolTranTime,  FivrConfig->VccinAux.RetToLowCurModeVolTranTime , PchSetup->PchRetToLowCurModeVolTranTime,  NullIndex);
  COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.PchFivrVccinAuxOffToHighCurModeVolTranTime, FivrConfig->VccinAux.OffToHighCurModeVolTranTime, PchSetup->PchOffToHighCurModeVolTranTime, NullIndex);

  COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.PchFivrDynPm,                   FivrConfig->FivrDynPm,                PchSetup->PchFivrDynPm,            NullIndex);
}
#endif

/**
  Update Espi config

  @param[in] SiPolicy             Pointer to SI_POLICY_PPI
  @param[in] FspsUpd              Pointer to FspsUpd structure
  @param[in] PchSetup             Pointer to PCH_SETUP buffer
**/
STATIC
VOID
UpdateEspiConfig (
  IN SI_POLICY_PPI             *SiPolicy,
  IN VOID                      *FspsUpd,
  IN PCH_SETUP                 *PchSetup
  )
{
#if FixedPcdGet8(PcdFspModeSelection) == 0
  PCH_ESPI_CONFIG                 *EspiConfig;
  EFI_STATUS                      Status;

  Status = GetConfigBlock ((VOID *) SiPolicy, &gEspiConfigGuid, (VOID *) &EspiConfig);
  ASSERT_EFI_ERROR (Status);
  if (EFI_ERROR (Status)) {
    return;
  }
#endif

  COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.PchEspiLgmrEnable,          EspiConfig->LgmrEnable,          PchSetup->PchEspiLgmrEnable,          NullIndex);
  COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.PchEspiHostC10ReportEnable, EspiConfig->HostC10ReportEnable, PchSetup->PchEspiHostC10ReportEnable, NullIndex);
}

/**
  Update Psf config

  @param[in] SiPolicy             Pointer to SI_POLICY_PPI
  @param[in] FspsUpd              Pointer to FspsUpd structure
  @param[in] PchSetup             Pointer to PCH_SETUP buffer
**/
STATIC
VOID
UpdatePsfConfig (
  IN SI_POLICY_PPI             *SiPolicy,
  IN VOID                      *FspsUpd,
  IN PCH_SETUP                 *PchSetup
  )
{
#if FixedPcdGet8(PcdEmbeddedEnable) == 0x1
  PSF_CONFIG                 *PsfConfig;
  PsfConfig = NULL;
  #if FixedPcdGetBool(PcdFspModeSelection) == 0
    EFI_STATUS                 Status;

    Status = GetConfigBlock ((VOID *) SiPolicy, &gPsfConfigGuid, (VOID *) &PsfConfig);
    ASSERT_EFI_ERROR (Status);
    if (EFI_ERROR (Status)) {
      return;
    }
  #endif
  COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.PsfTccEnable, PsfConfig->TccEnable, PchSetup->PsfTccEnable, NullIndex);
#endif
}

/**
  Update FlashProtection config

  @param[in] SiPolicy             Pointer to SI_POLICY_PPI
  @param[in] FspsUpd              Pointer to FspsUpd structure
  @param[in] PchSetup             Pointer to PCH_SETUP buffer
  @param[in] BiosGuardEnable      BIOS Guard enable
**/
STATIC
VOID
UpdateFlashProtectionConfig (
  IN SI_POLICY_PPI             *SiPolicy,
  IN VOID                      *FspsUpd,
  IN PCH_SETUP                 *PchSetup,
  IN UINT32                    BiosGuardEnable
  )
{
  UINT32                          BaseAddr;
  UINT32                          RegionSize;
  BOOLEAN                         FlashProtectionEnabled;
  EFI_STATUS                      Status;
  PCH_SPI_PPI                     *SpiPpi;
  UINT16                          ProtectedRangeBase;
  UINT16                          ProtectedRangeLimit;
#if FixedPcdGetBool(PcdCapsuleEnable) == 1
  SYSTEM_FIRMWARE_COMPONENT       UpdatingComponent;
#endif

#if FixedPcdGet8(PcdFspModeSelection) == 0
  PCH_FLASH_PROTECTION_CONFIG     *FlashProtectionConfig;
  Status = GetConfigBlock ((VOID *) SiPolicy, &gFlashProtectionConfigGuid, (VOID *) &FlashProtectionConfig);
  ASSERT_EFI_ERROR (Status);
  if (EFI_ERROR (Status)) {
    return;
  }
#endif
  FlashProtectionEnabled = PchSetup->FprrEnable;
#if FixedPcdGetBool(PcdCapsuleEnable) == 1
  UpdatingComponent      = 0xFF;
#endif

#if FixedPcdGetBool(PcdResiliencyEnable) == 1
  if (IsBiosUpdateRequired ()) {
#else
  if (IsBiosUpdateRequired () && !BiosGuardEnable) {
#endif
    DEBUG ((DEBUG_INFO, "Disable Flash Protection Range Registers for BIOS Update Process\n"));
    FlashProtectionEnabled = FALSE;
  }

#if FixedPcdGetBool(PcdCapsuleEnable) == 1
  IsFmpCapsuleForBiosUpdateDetected(&UpdatingComponent);
  if (IsBiosUpdateRequired () &&
     ((UpdatingComponent == (SYSTEM_FIRMWARE_COMPONENT) UpdatingBtGAcm) || \
     (UpdatingComponent == (SYSTEM_FIRMWARE_COMPONENT) UpdatinguCode))) {
    DEBUG ((DEBUG_INFO, "Disable Flash Protection Range Registers for uCode or BtG ACM Update Process\n"));
    FlashProtectionEnabled = FALSE;
  }
#endif

  //
  // Flash Protection Range Register initialization
  //
  if (FlashProtectionEnabled) {
    Status = PeiServicesLocatePpi (
               &gPchSpiPpiGuid,
               0,
               NULL,
               (VOID **)&SpiPpi
               );

    DEBUG ((DEBUG_INFO, "Enabling Flash Protection Range: LocatePpi gPchSpiPpiGuid = %r\n", Status));
    if (EFI_ERROR (Status)) {
      return;
    }
    //
    // Find the base address for BIOS region
    //
    Status = SpiPpi->GetRegionAddress (SpiPpi, FlashRegionBios, &BaseAddr, &RegionSize);
    DEBUG ((DEBUG_INFO, "BIOS Region: %r - Start Address: 0x%x - Size: 0x%x\n", Status, BaseAddr, RegionSize));
    ASSERT_EFI_ERROR (Status);
    if (EFI_ERROR (Status)) {
      return;
    }
    //
    // Initialize flash protection
    //
    UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.PchWriteProtectionEnable[0], FlashProtectionConfig->ProtectRange[0].WriteProtectionEnable, TRUE);
    UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.PchReadProtectionEnable[0],  FlashProtectionConfig->ProtectRange[0].ReadProtectionEnable, FALSE);
    //
    // Assign FPRR ranges
    //
#if FixedPcdGetBool(PcdExtendedBiosRegionSupport) == 1
    UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.PchProtectedRangeBase[0],   FlashProtectionConfig->ProtectRange[0].ProtectedRangeBase,  (UINT16)((BaseAddr + FixedPcdGet32 (PcdFlashExtendRegionSizeInUse) + FixedPcdGet32 (PcdFlashNvStorageSize)) >> 12));
#else
    UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.PchProtectedRangeBase[0],   FlashProtectionConfig->ProtectRange[0].ProtectedRangeBase,  (UINT16)((BaseAddr + FixedPcdGet32 (PcdFlashNvStorageSize)) >> 12));
#endif
    UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.PchProtectedRangeLimit[0],  FlashProtectionConfig->ProtectRange[0].ProtectedRangeLimit, (UINT16)(((BaseAddr + RegionSize) - 1) >> 12));

    GET_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.PchProtectedRangeBase[0], FlashProtectionConfig->ProtectRange[0].ProtectedRangeBase, ProtectedRangeBase);
    GET_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.PchProtectedRangeLimit[0], FlashProtectionConfig->ProtectRange[0].ProtectedRangeLimit, ProtectedRangeLimit);
    DEBUG ((DEBUG_INFO, "FlashProtectionConfig->ProtectRange[0].ProtectedRangeBase  %x\n", ProtectedRangeBase));
    DEBUG ((DEBUG_INFO, "FlashProtectionConfig->ProtectRange[0].ProtectedRangeLimit %x\n", ProtectedRangeLimit));

#if FixedPcdGetBool(PcdExtendedBiosRegionSupport) == 1
    // Initialize flash protection for Extended BIOS Region
    UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.PchWriteProtectionEnable[1], FlashProtectionConfig->ProtectRange[1].WriteProtectionEnable, TRUE);
    UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.PchReadProtectionEnable[1],  FlashProtectionConfig->ProtectRange[1].ReadProtectionEnable, FALSE);
    // Assign FPRR ranges for Extended BIOS Region
    UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.PchProtectedRangeBase[1],   FlashProtectionConfig->ProtectRange[1].ProtectedRangeBase,  (UINT16)(BaseAddr >> 12));
    UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.PchProtectedRangeLimit[1],  FlashProtectionConfig->ProtectRange[1].ProtectedRangeLimit, (UINT16)(((BaseAddr + FixedPcdGet32 (PcdFlashExtendRegionSizeInUse)) - 1) >> 12));
    GET_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.PchProtectedRangeBase[1], FlashProtectionConfig->ProtectRange[1].ProtectedRangeBase, ProtectedRangeBase);
    GET_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.PchProtectedRangeLimit[1], FlashProtectionConfig->ProtectRange[1].ProtectedRangeLimit, ProtectedRangeLimit);
    DEBUG ((DEBUG_INFO, "FlashProtectionConfig->ProtectRange[1].ProtectedRangeBase  %x\n", ProtectedRangeBase));
    DEBUG ((DEBUG_INFO, "FlashProtectionConfig->ProtectRange[1].ProtectedRangeLimit %x\n", ProtectedRangeLimit));
#endif
  }
}

/**
  Update Rtc config

  @param[in] SiPolicy             Pointer to SI_POLICY_PPI
  @param[in] FspsUpd              Pointer to FspsUpd structure
  @param[in] PchSetup             Pointer to PCH_SETUP buffer
**/
STATIC
VOID
UpdateRtcConfig (
  IN SI_POLICY_PPI             *SiPolicy,
  IN VOID                      *FspsUpd,
  IN PCH_SETUP                 *PchSetup
  )
{
  BOOLEAN                      Rtcstatus = TRUE;
#if FixedPcdGet8(PcdFspModeSelection) == 0
  RTC_CONFIG                   *RtcConfig;
  EFI_STATUS                   Status;
  Status = GetConfigBlock ((VOID *) SiPolicy, &gRtcConfigGuid, (VOID *) &RtcConfig);
  ASSERT_EFI_ERROR (Status);
  if (EFI_ERROR (Status)) {
    return;
  }
#endif

  if (IsBiosUpdateRequired ())
    Rtcstatus = FALSE;

#if FixedPcdGetBool (PcdResiliencyEnable) == 1
  Rtcstatus = FALSE;
#endif

  DEBUG ((DEBUG_INFO, "set Rtc Bios Interface Lock based on the condition: %d\n", Rtcstatus));
  UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.RtcBiosInterfaceLock, RtcConfig->BiosInterfaceLock, Rtcstatus);

  COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.RtcMemoryLock, RtcConfig->MemoryLock, PchSetup->PchRtcMemoryLock, NullIndex);
}

//
// SATADevSlpPin GPIO pin muxing deafaults
//
GLOBAL_REMOVE_IF_UNREFERENCED UINT32 mPchPSataDevSlpPinMux[PCH_MAX_SATA_PORTS] = {
  GPIO_VER2_P_MUXING_SATA_DEVSLP0_GPP_H12,   // Port0
  GPIO_VER2_P_MUXING_SATA_DEVSLP1_GPP_H13    // Port1
};

/**
  Update SATA Controller configuration.

  @param[in] SiPolicy  Pointer to SI_POLICY_PPI
  @param[in] FspsUpd   Pointer to FSPS_UPD
  @param[in] PchSetup  Pointer to PCH_SETUP
**/
STATIC
VOID
UpdateSataConfig (
  IN SI_POLICY_PPI  *SiPolicy,
  IN VOID           *FspsUpd,
  IN PCH_SETUP      *PchSetup
  )
{
  UINTN               SataCtrlIndex;
  UINT8               MaxSataPorts;
  UINT8               Index;

//[-start-190410-IB16990021-add]//
  PORT_CONFIG                     SataPortConfig;
//[-end-190410-IB16990021-add]//
#if FixedPcdGet8(PcdFspModeSelection) == 0
  SATA_CONFIG         *SataConfig;
  for (SataCtrlIndex = 0; SataCtrlIndex < MaxSataControllerNum (); SataCtrlIndex++) {
    SataConfig = GetPchSataConfig (SiPolicy, SataCtrlIndex);
#else
  for (SataCtrlIndex = 0; SataCtrlIndex < MaxSataControllerNum () && SataCtrlIndex < 1; SataCtrlIndex++) {
#endif
    UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.SataEnable, SataConfig->Enable,   PchSetup->PchSata);

    MaxSataPorts = MaxSataPortNum (SataCtrlIndex);
    //[-start-190410-IB16990021-add]//
    SataPortConfig.Value = PcdGet64 (PcdH2OChipsetSataPortEnable);
    //[-end-190410-IB16990021-add]//
    UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.SataMode,   SataConfig->SataMode, PchSetup->SataInterfaceMode);
    UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.SataPwrOptEnable, SataConfig->PwrOptEnable, TRUE);

    for (Index = 0; Index < MaxSataPorts; Index++) {
      if (!!PchSetup->SataTestMode) {
        UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.SataPortsEnable[Index], SataConfig->PortSettings[Index].Enable, TRUE);
      } else {
        //[-start-190410-IB16990021-modify]//
        //UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.SataPortsEnable[Index], SataConfig->PortSettings[Index].Enable, PchSetup->SataPort[Index]);
        UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.SataPortsEnable[Index], SataConfig->PortSettings[Index].Enable, (SataPortConfig.Value >> Index) & 0x1);
        //[-end-190410-IB16990021-modify]//
      }
      UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.SataPortsHotPlug[Index],          SataConfig->PortSettings[Index].HotPlug,          PchSetup->SataHotPlug[Index]);
      UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.SataPortsSpinUp[Index],           SataConfig->PortSettings[Index].SpinUp,           PchSetup->SataSpinUp[Index]);
      UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.SataPortsExternal[Index],         SataConfig->PortSettings[Index].External,         PchSetup->SataExternal[Index]);
      UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.SataPortsDevSlp[Index],           SataConfig->PortSettings[Index].DevSlp,           PchSetup->PxDevSlp[Index]);
      // @TODO: Maybe we need to have setup option to setting SATA port GPIO reset configuration.
      UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.SataPortsDevSlpResetConfig[Index], SataConfig->PortSettings[Index].DevSlpResetConfig, 1);
      UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.SataPortsEnableDitoConfig[Index], SataConfig->PortSettings[Index].EnableDitoConfig, PchSetup->EnableDitoConfig[Index]);
      UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.SataPortsDmVal[Index],            SataConfig->PortSettings[Index].DmVal,            PchSetup->DmVal[Index]);
      UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.SataPortsDitoVal[Index],          SataConfig->PortSettings[Index].DitoVal,          PchSetup->DitoVal[Index]);
      UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.SataPortsSolidStateDrive[Index],  SataConfig->PortSettings[Index].SolidStateDrive,  PchSetup->SataType[Index]);
      //
      //  Sata Port DevSlp GPIO configuration
      //
      if (IsPchP () || IsPchN ()) {
        UPDATE_POLICY(((FSPS_UPD *)FspsUpd)->FspsConfig.SataPortDevSlpPinMux[Index], SataConfig->PortSettings[Index].DevSlpGpioPinMux, mPchPSataDevSlpPinMux[Index]);
      }
    }
    if (PchSeries () == PCH_LP) {
      //
      // Only Mobile boards has interlock switches in SATA Port 0, 1 and 2
      //
#if FixedPcdGet8(PcdFspModeSelection) == 0
      for (Index = 0; Index < MaxSataPorts; Index++) {
#else
      for (Index = 0; Index < MaxSataPorts && Index < 8; Index++) {
#endif
        if (PchSetup->SataHotPlug[Index]) {
          UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.SataPortsInterlockSw[Index], SataConfig->PortSettings[Index].InterlockSw, PchSetup->SataMechanicalSw[Index]);
        }
      }
    }

    UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.SataLedEnable,               SataConfig->LedEnable,                          PcdGetBool (PcdSataLedEnable));
    UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.SataSalpSupport,             SataConfig->SalpSupport,                        PchSetup->SataSalp);
    UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.SataThermalSuggestedSetting, SataConfig->ThermalThrottling.SuggestedSetting, PchSetup->PchSataTsSuggestSet);
    UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.SataP1TDispFinit,            SataConfig->ThermalThrottling.P1TDispFinit,     PchSetup->PchP1TDispFinit);
    UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.SataP1Tinact,                SataConfig->ThermalThrottling.P1Tinact,         PchSetup->PchP1Tinact);
    UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.SataP1TDisp,                 SataConfig->ThermalThrottling.P1TDisp,          PchSetup->PchP1TDisp);
    UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.SataP1T3M,                   SataConfig->ThermalThrottling.P1T3M,            PchSetup->PchP1T3M);
    UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.SataP1T2M,                   SataConfig->ThermalThrottling.P1T2M,            PchSetup->PchP1T2M);
    UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.SataP1T1M,                   SataConfig->ThermalThrottling.P1T1M,            PchSetup->PchP1T1M);
    UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.SataP0TDispFinit,            SataConfig->ThermalThrottling.P0TDispFinit,     PchSetup->PchP0TDispFinit);
    UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.SataP0Tinact,                SataConfig->ThermalThrottling.P0Tinact,         PchSetup->PchP0Tinact);
    UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.SataP0TDisp,                 SataConfig->ThermalThrottling.P0TDisp,          PchSetup->PchP0TDisp);
    UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.SataP0T3M,                   SataConfig->ThermalThrottling.P0T3M,            PchSetup->PchP0T3M);
    UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.SataP0T2M,                   SataConfig->ThermalThrottling.P0T2M,            PchSetup->PchP0T2M);
    UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.SataP0T1M,                   SataConfig->ThermalThrottling.P0T1M,            PchSetup->PchP0T1M);
  }
}

#if FixedPcdGet8(PcdFspModeSelection) == 1
/**
  Update DevInt config

  @param[in] FspsUpd              Pointer to FspsUpd structure
**/
STATIC
VOID
UpdateDevIntConfig (
  IN VOID                      *FspsUpd
  )
{
  ((FSPS_UPD *) FspsUpd)->FspsConfig.DevIntConfigPtr = (UINT32) NULL;
}
#endif

/**
  Update Ieh config

  @param[in] SiPolicy             Pointer to SI_POLICY_PPI
  @param[in] FspsUpd              Pointer to FspsUpd structure
  @param[in] PchSetup             Pointer to PCH_SETUP buffer
**/
STATIC
VOID
UpdateIehConfig (
  IN SI_POLICY_PPI             *SiPolicy,
  IN VOID                      *FspsUpd,
  IN PCH_SETUP                 *PchSetup
  )
{
#if FixedPcdGet8(PcdFspModeSelection) == 0
  IEH_CONFIG                  *IehConfig;
  EFI_STATUS                  Status;
  Status = GetConfigBlock ((VOID *) SiPolicy, &gIehConfigGuid, (VOID *) &IehConfig);
  ASSERT_EFI_ERROR (Status);
  if (EFI_ERROR (Status)) {
    return;
  }
#endif
  COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.IehMode, IehConfig->Mode, PchSetup->IehMode, NullIndex);
}

/**
  Update Interrupt config

  @param[in] SiPolicy             Pointer to SI_POLICY_PPI
  @param[in] FspsUpd              Pointer to FspsUpd structure
  @param[in] PchSetup             Pointer to PCH_SETUP buffer
**/
STATIC
VOID
UpdateInterruptConfig (
  IN SI_POLICY_PPI             *SiPolicy,
  IN VOID                      *FspsUpd,
  IN PCH_SETUP                 *PchSetup
  )
{
#if FixedPcdGet8(PcdFspModeSelection) == 0
  PCH_INTERRUPT_CONFIG         *InterruptConfig;
  EFI_STATUS                   Status;
  Status = GetConfigBlock ((VOID *) SiPolicy, &gInterruptConfigGuid, (VOID *) &InterruptConfig);
  ASSERT_EFI_ERROR (Status);
  if (EFI_ERROR (Status)) {
    return;
  }
#endif

  COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.GpioIrqRoute, InterruptConfig->GpioIrqRoute, PchSetup->PchGpioIrqRoute, NullIndex);
}

/**
  Update PCH General config

  @param[in] SiPolicy             Pointer to SI_POLICY_PPI
  @param[in] FspsUpd              Pointer to FspsUpd structure
  @param[in] PchSetup             Pointer to PCH_SETUP buffer
**/
STATIC
VOID
UpdatePchGeneralConfig (
  IN SI_POLICY_PPI             *SiPolicy,
  IN VOID                      *FspsUpd,
  IN PCH_SETUP                 *PchSetup
  )
{
#if FixedPcdGet8(PcdFspModeSelection) == 0
  PCH_GENERAL_CONFIG           *PchGeneralConfig;
  EFI_STATUS                   Status;
  Status = GetConfigBlock ((VOID *) SiPolicy, &gPchGeneralConfigGuid, (VOID *) &PchGeneralConfig);
  ASSERT_EFI_ERROR (Status);
  if (EFI_ERROR (Status)) {
    return;
  }
#endif

  COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.PchCrid,               PchGeneralConfig->Crid,               PchSetup->PchCrid,               NullIndex);
  COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.PchLegacyIoLowLatency, PchGeneralConfig->LegacyIoLowLatency, PchSetup->PchLegacyIoLowLatency, NullIndex);
  COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.PchAcpiL6dPmeHandling, PchGeneralConfig->AcpiL6dPmeHandling, PchSetup->AcpiL6dPmeHandling,    NullIndex);
}

/**
  Correct mismatch configuration of ISH and Serial IO

  @param[in] SiPolicy             Pointer to SI_POLICY_PPI
  @param[in] FspsUpd              Pointer to FspsUpd structure
  @param[in] PchSetup             Pointer to PCH_SETUP buffer
**/
STATIC
VOID
CorrectMismatchConfig (
  IN SI_POLICY_PPI             *SiPolicy,
  IN VOID                      *FspsUpd,
  IN PCH_SETUP                 *PchSetup
  )
{
  UINT8                           I2c2Mode;
  UINT8                           I2c3Mode;
  UINT8                           I2c5Mode;
  UINT8                           Uart1Mode;
#if FixedPcdGet8(PcdFspModeSelection) == 0
  ISH_CONFIG                      *IshConfig;
  SERIAL_IO_CONFIG                *SerialIoConfig;
  EFI_STATUS                      Status;

  Status = GetConfigBlock ((VOID *) SiPolicy, &gIshConfigGuid, (VOID *) &IshConfig);
  ASSERT_EFI_ERROR (Status);
  if (EFI_ERROR (Status)) {
    return;
  }
  Status = GetConfigBlock ((VOID *) SiPolicy, &gSerialIoConfigGuid, (VOID *) &SerialIoConfig);
  ASSERT_EFI_ERROR (Status);
  if (EFI_ERROR (Status)) {
    return;
  }
#endif

  GET_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.SerialIoI2cMode[2], SerialIoConfig->I2cDeviceConfig[2].Mode, I2c2Mode);
  if (IsPchH () && I2c2Mode != SerialIoI2cDisabled) {
    //
    // ISH UART0 use the same GPIO pins as SerialIo I2C2 on PCH-H
    //
    UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.PchIshUartEnable[0], IshConfig->Uart[0].Enable, FALSE);
  }

  GET_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.SerialIoUartMode[1], SerialIoConfig->UartDeviceConfig[1].Mode, Uart1Mode);
  if (Uart1Mode != SerialIoUartDisabled) {
    //
    // ISH UART1 use the same GPIO pins as SerialIo UART1
    //
    UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.PchIshUartEnable[1], IshConfig->Uart[1].Enable, FALSE);
  }

  GET_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.SerialIoI2cMode[3], SerialIoConfig->I2cDeviceConfig[3].Mode, I2c3Mode);
  GET_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.SerialIoI2cMode[5], SerialIoConfig->I2cDeviceConfig[5].Mode, I2c5Mode);
  if ((IsPchLp () && I2c5Mode != SerialIoI2cDisabled) ||
      (IsPchH ()  && I2c3Mode != SerialIoI2cDisabled)) {
    //
    // ISH I2C2 use the same GPIO pins as SerialIo I2C5 on PCH-LP / I2C3 on PCH-H
    //
    UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.PchIshI2cEnable[2], IshConfig->I2c[2].Enable, FALSE);
  }

}

#if FixedPcdGetBool (PcdEmbeddedEnable)
/**
  Update Fusa config

  @param[in] SiPolicy             Pointer to SI_POLICY_PPI
  @param[in] FspsUpd              Pointer to FspsUpd structure
  @param[in] PchSetup             Pointer to PCH_SETUP buffer
  @param[in] SaSetup              Pointer to SA_SETUP buffer
**/
STATIC
VOID
UpdateFusaConfig (
  IN SI_POLICY_PPI             *SiPolicy,
  IN VOID                      *FspsUpd,
  IN PCH_SETUP                 *PchSetup,
  IN SA_SETUP                  *SaSetup
  )
{
  FUSA_CONFIG                *FusaConfig;
  FusaConfig = NULL;

#if FixedPcdGet8(PcdFspModeSelection) == 0
  EFI_STATUS                 Status;

  Status = GetConfigBlock ((VOID *) SiPolicy, &gFusaConfigGuid, (VOID *) &FusaConfig);
  ASSERT_EFI_ERROR (Status);
  if (EFI_ERROR (Status)) {
    return;
  }
#endif
  COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.DisplayFusaConfigEnable,  FusaConfig->DisplayFusaConfigEnable,  SaSetup->DisplayFusaConfigEnable, NullIndex);
  COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.GraphicFusaConfigEnable,  FusaConfig->GraphicFusaConfigEnable,  SaSetup->GraphicFusaConfigEnable, NullIndex);
  COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.OpioFusaConfigEnable,     FusaConfig->OpioFusaConfigEnable,     SaSetup->OpioFusaConfigEnable,    NullIndex);
  COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.PsfFusaConfigEnable,      FusaConfig->PsfFusaConfigEnable,      PchSetup->PsfFusaConfigEnable,    NullIndex);
  COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.IopFusaConfigEnable,      FusaConfig->IopFusaConfigEnable,      PchSetup->IopFusaConfigEnable,    NullIndex);
  COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.FusaConfigEnable,         FusaConfig->FusaConfigEnable,         SaSetup->FusaConfigEnable,        NullIndex);
  COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.FusaRunStartupArrayBist,  FusaConfig->FusaRunStartupArrayBist,  SaSetup->FusaRunStartupArrayBist, NullIndex);
  COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.FusaRunStartupScanBist,   FusaConfig->FusaRunStartupScanBist,   SaSetup->FusaRunStartupScanBist,  NullIndex);
  COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.FusaRunPeriodicScanBist,  FusaConfig->FusaRunPeriodicScanBist,  SaSetup->FusaRunPeriodicScanBist, NullIndex);
  COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.Module0Lockstep,          FusaConfig->Module0Lockstep,          SaSetup->Module0Lockstep,         NullIndex);
  COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.Module1Lockstep,          FusaConfig->Module1Lockstep,          SaSetup->Module1Lockstep,         NullIndex);
}
#endif

/**
  Update Hybrid Storage Config

  @param[in] SiPolicy             Pointer to SI_POLICY_PPI
  @param[in] FspsUpd              Pointer to FspsUpd structure
  @param[in] PchSetup             Pointer to PCH_SETUP buffer
**/
STATIC
VOID
UpdateHybridStorageConfig (
  IN SI_POLICY_PPI             *SiPolicy,
  IN VOID                      *FspsUpd,
  IN PCH_SETUP                 *PchSetup
  )
{
#if FixedPcdGetBool(PcdFspModeSelection) == 0
  EFI_STATUS                   Status;
  HYBRID_STORAGE_CONFIG        *HybridStorageConfig;
  Status = GetConfigBlock ((VOID *) SiPolicy, &gHybridStorageConfigGuid, (VOID *) &HybridStorageConfig);
  ASSERT_EFI_ERROR (Status);
  if (EFI_ERROR (Status)) {
    return;
  }
#endif

  COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.HybridStorageMode, HybridStorageConfig->HybridStorageMode, PchSetup->HybridStorageMode, NullIndex);
  COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.CpuRootportUsedForHybridStorage, HybridStorageConfig->CpuRootportUsedForHybridStorage, PchSetup->CpuRootportUsedForHybridStorage, NullIndex);
  COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.PchRootportUsedForCpuAttach, HybridStorageConfig->PchRootportUsedForCpuAttach, PchSetup->PchRootportUsedForCpuAttach, NullIndex);
}

/**
  Update SPI Config

  @param[in] SiPolicy             Pointer to SI_POLICY_PPI
  @param[in] FspsUpd              Pointer to FspsUpd structure
  @param[in] PchSetup             Pointer to PCH_SETUP buffer
**/
STATIC
VOID
UpdateSpiConfig (
  IN SI_POLICY_PPI             *SiPolicy,
  IN VOID                      *FspsUpd,
  IN PCH_SETUP                 *PchSetup
  )
{

#if FixedPcdGetBool(PcdFspModeSelection) == 0
  EFI_STATUS                   Status;
  SPI_CONFIG                   *SpiConfig;
  Status = GetConfigBlock ((VOID *) SiPolicy, &gSpiConfigGuid, (VOID *) &SpiConfig);
  ASSERT_EFI_ERROR (Status);
  if (EFI_ERROR (Status)) {
    return;
  }
#endif

  COMPARE_AND_UPDATE_POLICY (
    ((FSPS_UPD *) FspsUpd)->FspsConfig.PchSpiExtendedBiosDecodeRangeEnable,
    SpiConfig->ExtendedBiosDecodeRangeEnable,
    PchSetup->ExtendedBiosDecodeRangeEnable,
    NullIndex
    );
}

/**
  Get the ChipsetInit Binary pointer.

  @retval EFI_SUCCESS               - ChipsetInit Binary found.
  @retval EFI_NOT_FOUND             - ChipsetInit Binary not found.
**/
EFI_STATUS
UpdateHsioPolicy (
  IN SI_POLICY_PPI             *SiPolicy,
  IN VOID                      *FspsUpd,
  IN PCH_SETUP                 *PchSetup
  )
{
  EFI_STATUS                    Status;
  PCH_STEPPING                  PchStep;
  EFI_FIRMWARE_VOLUME_HEADER    *FvHeader;
  EFI_FV_FILE_INFO              ChipsetInitFileInfo;
  EFI_PEI_FILE_HANDLE           *FfsHeader;
  EFI_GUID                      *ChipsetInitBinaryGuidPtr;
#if FixedPcdGetBool(PcdFspModeSelection) == 0
  PCH_HSIO_CONFIG               *HsioConfig;
#endif
  UINT32                        ModuleAddr;
  UINT32                        ModuleSize;

  ModuleAddr = 0;
  ModuleSize = 0;
  PchStep    = PchStepping ();

#if FixedPcdGetBool(PcdFspModeSelection) == 1
  FspsUpd = (FSPS_UPD *) PcdGet32 (PcdFspsUpdDataAddress);
  ASSERT (FspsUpd != NULL);
#else
  Status = GetConfigBlock ((VOID *) SiPolicy, &gHsioConfigGuid, (VOID *) &HsioConfig);
  ASSERT_EFI_ERROR (Status);
#endif

  ChipsetInitBinaryGuidPtr = NULL;
  if (IsPchLp ()) {
    switch (PchStep) {
      case PCH_A0:
        ChipsetInitBinaryGuidPtr    = &gAdlPchLpChipsetInitTableAxGuid;;
        DEBUG ((DEBUG_INFO, "Using AdlPchLpChipsetInitTable_Ax table \n"));
      break;
      default:
        return EFI_NOT_FOUND;
    }
  } else {
    return EFI_NOT_FOUND;
  }

  if (ChipsetInitBinaryGuidPtr == NULL) {
    DEBUG ((DEBUG_INFO, "ChipsetInit Binary not found \n"));
    return EFI_NOT_FOUND;
  }
  //
  // Locate Firmware Volume header
  //
  FvHeader = (EFI_FIRMWARE_VOLUME_HEADER *) (UINTN) FixedPcdGet32 (PcdFlashFvFirmwareBinariesBase);
  Status = PeiServicesFfsFindFileByName (ChipsetInitBinaryGuidPtr, FvHeader, (VOID **)&FfsHeader);
  if (EFI_ERROR (Status)) {
    DEBUG ((DEBUG_ERROR, "Error returned by PeiServicesFfsFindFileByName while retrieving ChipsetInit Address\n"));
    ASSERT_EFI_ERROR (Status);
    return Status;
  }

  ModuleAddr = (UINT32)((UINT8 *) FfsHeader + sizeof (EFI_FFS_FILE_HEADER));
  Status = PeiServicesFfsGetFileInfo (FfsHeader, &ChipsetInitFileInfo);
  if (EFI_ERROR (Status)) {
    DEBUG ((DEBUG_ERROR, "Error returned by PeiServicesFfsGetFileInfo while retrieving BIOS Guard Module Size\n"));
  }

  if (ChipsetInitFileInfo.BufferSize == 0) {
    DEBUG ((DEBUG_INFO, "ChipsetInit Binary invalid size \n"));
    return EFI_NOT_FOUND;
  }
  ModuleSize = (UINTN) ChipsetInitFileInfo.BufferSize;

  //
  // Get ChipsetInit Binary Pointer
  //
  UPDATE_POLICY (
    ((FSPS_UPD *) FspsUpd)->FspsConfig.ChipsetInitBinPtr,
    HsioConfig->ChipsetInitBinPtr,
    ModuleAddr
    );

  //
  // Get File Size
  //
  UPDATE_POLICY (
    ((FSPS_UPD *) FspsUpd)->FspsConfig.ChipsetInitBinLen,
    HsioConfig->ChipsetInitBinLen,
    ModuleSize
    );

#if FixedPcdGetBool(PcdFspModeSelection) == 1
  DEBUG ((DEBUG_INFO, "ChipsetInit Binary Location: %x\n", ((FSPS_UPD *) FspsUpd)->FspsConfig.ChipsetInitBinPtr));
  DEBUG ((DEBUG_INFO, "ChipsetInit Binary Size: %x\n", ((FSPS_UPD *) FspsUpd)->FspsConfig.ChipsetInitBinLen));
#else
  DEBUG ((DEBUG_INFO, "ChipsetInit Binary Location: %x\n", HsioConfig->ChipsetInitBinPtr));
  DEBUG ((DEBUG_INFO, "ChipsetInit Binary Size: %x\n", HsioConfig->ChipsetInitBinLen));
#endif

  return Status;
}

/**
  Get the OEM Synps Phy Binary pointer.

  @param[in] SiPolicy             Pointer to SI_POLICY_PPI
  @param[in] FspsUpd              Pointer to FspsUpd structure
**/
VOID
UpdateOemSnpsPhyPtr (
  IN SI_POLICY_PPI             *SiPolicy,
  IN VOID                      *FspsUpd
  )
{
  EFI_STATUS                    Status;
#if FixedPcdGetBool(PcdFspModeSelection) == 0
  PCH_HSIO_CONFIG               *HsioConfig;
#endif
  EFI_FIRMWARE_VOLUME_HEADER    *FvHeader;
  EFI_FFS_FILE_HEADER           *FfsHeader;
  UINT32                        ModuleAddr;
  EFI_FV_FILE_INFO              OemSnpsFileInfo;

  ModuleAddr = 0;
  FvHeader = NULL;

#if FixedPcdGetBool(PcdFspModeSelection) == 0
  Status = GetConfigBlock ((VOID *) SiPolicy, &gHsioConfigGuid, (VOID *) &HsioConfig);
  ASSERT_EFI_ERROR (Status);
#endif

  ///
  /// Find Firmware File System file in Firmware Volume
  /// Locate Firmware Volume header
  ///
  FvHeader = (EFI_FIRMWARE_VOLUME_HEADER *)(UINTN)FixedPcdGet32 (PcdFlashFvFirmwareBinariesBase);
  Status = PeiServicesFfsFindFileByName (&gOemSnpsPhyTableGuid, FvHeader, (VOID **)&FfsHeader);

  if (Status != EFI_SUCCESS) {
    DEBUG ((DEBUG_INFO, "OEM SnpsPhy Binary not found"));
    return;
  }

  ModuleAddr = (UINT32)((UINT8 *) FfsHeader + sizeof (EFI_FFS_FILE_HEADER));
  Status = PeiServicesFfsGetFileInfo (FfsHeader, &OemSnpsFileInfo);
  if (EFI_ERROR (Status)) {
    DEBUG ((DEBUG_ERROR, "Error returned by PeiServicesFfsGetFileInfo while retrieving OEM Synps Phy Binary Size\n"));
  }

  if (OemSnpsFileInfo.BufferSize == 0) {
    DEBUG ((DEBUG_INFO, "OEM Synps Phy Binary invalid size \n"));
    return;
  }

  //
  // Get OEM Synps Phy Binary Pointer
  //
  UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.SynpsPhyBinPtr, HsioConfig->SynpsPhyBinPtr, ModuleAddr);
  //
  // Get File Size
  //
  UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.SynpsPhyBinLen, HsioConfig->SynpsPhyBinLen, OemSnpsFileInfo.BufferSize);

#if FixedPcdGetBool(PcdFspModeSelection) == 1
  DEBUG ((DEBUG_INFO, "OEM SynpsPhy Binary Location: %x\n", ((FSPS_UPD *) FspsUpd)->FspsConfig.SynpsPhyBinPtr));
  DEBUG ((DEBUG_INFO, "OEM SynpsPhy Binary Size: %x\n", ((FSPS_UPD *) FspsUpd)->FspsConfig.SynpsPhyBinLen));
#else
  DEBUG ((DEBUG_INFO, "OEM SynpsPhy Binary Location: %x\n", HsioConfig->SynpsPhyBinPtr));
  DEBUG ((DEBUG_INFO, "OEM SynpsPhy Binary Size: %x\n", HsioConfig->SynpsPhyBinLen));
#endif
}

//[-start-211109-IB18410136-add]//
/**
  Update USB3 HSIO config

  @param[in] SiPolicy             Pointer to SI_POLICY_PPI
  @param[in] FspsUpd              Pointer to FspsUpd structure
**/
STATIC
VOID
UpdateUsb3HsioConfig (
  IN SI_POLICY_PPI    *SiPolicy,
  IN VOID             *FspsUpd
  )
{
  UINTN               PortIndex;
  USB3_HSIO_CONFIG    *Usb3HsioConfig;
  EFI_STATUS          Status;

#if FixedPcdGet8 (PcdFspModeSelection) == 0
  Status = GetConfigBlock ((VOID *)SiPolicy, &gUsb3HsioConfigGuid, (VOID *)&Usb3HsioConfig);
  ASSERT_EFI_ERROR (Status);
  if (EFI_ERROR (Status)) {
    return;
  }
#else
  Usb3HsioConfig = NULL;
#endif

  for (PortIndex = 0; PortIndex < GetPchXhciMaxUsb3PortNum (); PortIndex++) {
    UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.Usb3HsioTxDownscaleAmp[PortIndex],                Usb3HsioConfig->Port[PortIndex].HsioTxDownscaleAmp,             ((UINT8 *)PcdGetPtr (PchUsb3HsioTxDownscaleAmp))[PortIndex]);
    UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.Usb3HsioTxDeEmph[PortIndex],                      Usb3HsioConfig->Port[PortIndex].HsioTxDeEmph,                   ((UINT8 *)PcdGetPtr (PchUsb3HsioTxDeEmph))[PortIndex]);
    UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.PchUsb3HsioCtrlAdaptOffsetCfg[PortIndex],         Usb3HsioConfig->Port[PortIndex].HsioCtrlAdaptOffsetCfg,         ((UINT8 *)PcdGetPtr (PchUsb3HsioCtrlAdaptOffsetCfg))[PortIndex]);
    UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.PchUsb3HsioFilterSelN[PortIndex],                 Usb3HsioConfig->Port[PortIndex].HsioFilterSelN,                 ((UINT8 *)PcdGetPtr (PchUsb3HsioFilterSelN))[PortIndex]);
    UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.PchUsb3HsioFilterSelP[PortIndex],                 Usb3HsioConfig->Port[PortIndex].HsioFilterSelP,                 ((UINT8 *)PcdGetPtr (PchUsb3HsioFilterSelP))[PortIndex]);
    UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.PchUsb3HsioOlfpsCfgPullUpDwnRes[PortIndex],       Usb3HsioConfig->Port[PortIndex].HsioOlfpsCfgPullUpDwnRes,       ((UINT8 *)PcdGetPtr (PchUsb3HsioOlfpsCfgPullUpDwnRes))[PortIndex]);
    UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.Usb3HsioTxDeEmphEnable[PortIndex],                Usb3HsioConfig->Port[PortIndex].HsioTxDeEmphEnable,             ((UINT8 *)PcdGetPtr (PchUsb3HsioTxDeEmphEnable))[PortIndex]);
    UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.Usb3HsioTxDownscaleAmpEnable[PortIndex],          Usb3HsioConfig->Port[PortIndex].HsioTxDownscaleAmpEnable,       ((UINT8 *)PcdGetPtr (PchUsb3HsioTxDownscaleAmpEnable))[PortIndex]);
    UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.PchUsb3HsioCtrlAdaptOffsetCfgEnable[PortIndex],   Usb3HsioConfig->Port[PortIndex].HsioCtrlAdaptOffsetCfgEnable,   ((UINT8 *)PcdGetPtr (PchUsb3HsioCtrlAdaptOffsetCfgEnable))[PortIndex]);
    UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.PchUsb3HsioFilterSelNEnable[PortIndex],           Usb3HsioConfig->Port[PortIndex].HsioFilterSelNEnable,           ((UINT8 *)PcdGetPtr (PchUsb3HsioFilterSelNEnable))[PortIndex]);
    UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.PchUsb3HsioFilterSelPEnable[PortIndex],           Usb3HsioConfig->Port[PortIndex].HsioFilterSelPEnable,           ((UINT8 *)PcdGetPtr (PchUsb3HsioFilterSelPEnable))[PortIndex]);
    UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.PchUsb3HsioOlfpsCfgPullUpDwnResEnable[PortIndex], Usb3HsioConfig->Port[PortIndex].HsioOlfpsCfgPullUpDwnResEnable, ((UINT8 *)PcdGetPtr (PchUsb3HsioOlfpsCfgPullUpDwnResEnable))[PortIndex]);
    UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.Usb3HsioTxRate3UniqTran[PortIndex],               Usb3HsioConfig->Port[PortIndex].HsioTxRate3UniqTran,            ((UINT8 *)PcdGetPtr (PchUsb3HsioTxRate3UniqTran))[PortIndex]);
    UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.Usb3HsioTxRate2UniqTran[PortIndex],               Usb3HsioConfig->Port[PortIndex].HsioTxRate2UniqTran,            ((UINT8 *)PcdGetPtr (PchUsb3HsioTxRate2UniqTran))[PortIndex]);
    UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.Usb3HsioTxRate1UniqTran[PortIndex],               Usb3HsioConfig->Port[PortIndex].HsioTxRate1UniqTran,            ((UINT8 *)PcdGetPtr (PchUsb3HsioTxRate1UniqTran))[PortIndex]);
    UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.Usb3HsioTxRate0UniqTran[PortIndex],               Usb3HsioConfig->Port[PortIndex].HsioTxRate0UniqTran,            ((UINT8 *)PcdGetPtr (PchUsb3HsioTxRate0UniqTran))[PortIndex]);
    UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.Usb3HsioTxRate3UniqTranEnable[PortIndex],         Usb3HsioConfig->Port[PortIndex].HsioTxRate3UniqTranEnable,      ((UINT8 *)PcdGetPtr (PchUsb3HsioTxRate3UniqTranEnable))[PortIndex]);
    UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.Usb3HsioTxRate2UniqTranEnable[PortIndex],         Usb3HsioConfig->Port[PortIndex].HsioTxRate2UniqTranEnable,      ((UINT8 *)PcdGetPtr (PchUsb3HsioTxRate2UniqTranEnable))[PortIndex]);
    UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.Usb3HsioTxRate1UniqTranEnable[PortIndex],         Usb3HsioConfig->Port[PortIndex].HsioTxRate1UniqTranEnable,      ((UINT8 *)PcdGetPtr (PchUsb3HsioTxRate1UniqTranEnable))[PortIndex]);
    UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.Usb3HsioTxRate0UniqTranEnable[PortIndex],         Usb3HsioConfig->Port[PortIndex].HsioTxRate0UniqTranEnable,      ((UINT8 *)PcdGetPtr (PchUsb3HsioTxRate0UniqTranEnable))[PortIndex]);
  }
}
//[-end-211109-IB18410136-add]//

/**
  This function performs PCH PEI Policy initialization.

  @retval EFI_SUCCESS             The PPI is installed and initialized.
  @retval EFI ERRORS              The PPI is not successfully installed.
  @retval EFI_OUT_OF_RESOURCES    Do not have enough resources to initialize the driver
**/
EFI_STATUS
EFIAPI
UpdatePeiPchPolicy (
  VOID
  )
{
  EFI_STATUS                      Status;
  EFI_PEI_READ_ONLY_VARIABLE2_PPI *VariableServices;
  UINTN                           VariableSize;
  SA_SETUP                        SaSetup;
  CPU_SETUP                       CpuSetup;
  PCH_SETUP                       PchSetup;
  SETUP_DATA                      SetupVariables;
  DEBUG_CONFIG_DATA               DebugConfigData;
  VOID                            *FspsUpd;
  SI_POLICY_PPI                   *SiPolicy;
  UINT32                          BiosGuardEnable;
  VOID                            *FspmUpd;
  SI_PREMEM_POLICY_PPI            *SiPreMemPolicyPpi;
  CPU_SECURITY_PREMEM_CONFIG      *CpuSecurityPreMemConfig;
//[-start-191111-IB10189001-add]//
  EFI_STATUS                      CommandStatus;
//[-end-191111-IB10189001-add]//

  DEBUG ((DEBUG_INFO, "Update PeiPchPolicyUpdate Pos-Mem Start\n"));

  FspsUpd                 = NULL;
  FspmUpd                 = NULL;
  SiPolicy                = NULL;
  CpuSecurityPreMemConfig = NULL;
  SiPreMemPolicyPpi       = NULL;

#if FixedPcdGet8(PcdFspModeSelection) == 1
  FspmUpd = (FSPM_UPD *) PcdGet32 (PcdFspmUpdDataAddress);
  ASSERT (FspmUpd != NULL);
  FspsUpd = (FSPS_UPD *) PcdGet32 (PcdFspsUpdDataAddress);
  ASSERT (FspsUpd != NULL);
#else
  Status = PeiServicesLocatePpi (&gSiPolicyPpiGuid, 0, NULL, (VOID **) &SiPolicy);
  ASSERT_EFI_ERROR (Status);
  Status = PeiServicesLocatePpi (&gSiPreMemPolicyPpiGuid, 0, NULL, (VOID **) &SiPreMemPolicyPpi);
  ASSERT_EFI_ERROR (Status);
  Status = GetConfigBlock ((VOID *) SiPreMemPolicyPpi, &gCpuSecurityPreMemConfigGuid, (VOID *) &CpuSecurityPreMemConfig);
  ASSERT_EFI_ERROR (Status);
  if (EFI_ERROR (Status)) {
    return Status;
  }
#endif

  BiosGuardEnable = GetBiosGuardEnable (FspmUpd, CpuSecurityPreMemConfig);

  //
  // Retrieve Setup variable
  //
  Status = PeiServicesLocatePpi (
             &gEfiPeiReadOnlyVariable2PpiGuid, // GUID
             0,                                // INSTANCE
             NULL,                             // EFI_PEI_PPI_DESCRIPTOR
             (VOID **) &VariableServices       // PPI
             );
  ASSERT_EFI_ERROR (Status);

  VariableSize = sizeof (SETUP_DATA);
  Status = VariableServices->GetVariable (
                               VariableServices,
                               L"Setup",
                               &gSetupVariableGuid,
                               NULL,
                               &VariableSize,
                               &SetupVariables
                               );
  ASSERT_EFI_ERROR (Status);

  VariableSize = sizeof (SA_SETUP);
  Status = VariableServices->GetVariable (
                               VariableServices,
                               L"SaSetup",
                               &gSaSetupVariableGuid,
                               NULL,
                               &VariableSize,
                               &SaSetup
                               );
  ASSERT_EFI_ERROR (Status);

  VariableSize = sizeof (CPU_SETUP);
  Status = VariableServices->GetVariable (
                               VariableServices,
                               L"CpuSetup",
                               &gCpuSetupVariableGuid,
                               NULL,
                               &VariableSize,
                               &CpuSetup
                               );
  ASSERT_EFI_ERROR (Status);

  VariableSize = sizeof (PCH_SETUP);
  Status = VariableServices->GetVariable (
                               VariableServices,
                               L"PchSetup",
                               &gPchSetupVariableGuid,
                               NULL,
                               &VariableSize,
                               &PchSetup
                               );
  ASSERT_EFI_ERROR (Status);

  VariableSize = sizeof (DEBUG_CONFIG_DATA);
  Status = VariableServices->GetVariable (
                               VariableServices,
                               L"DebugConfigData",
                               &gDebugConfigVariableGuid,
                               NULL,
                               &VariableSize,
                               &DebugConfigData
                               );
DEBUG_CODE_BEGIN();
  ASSERT_EFI_ERROR (Status);
DEBUG_CODE_END();

#if FixedPcdGet8(PcdFspModeSelection) == 1
  //
  // Interrupt Config
  //
  UpdateDevIntConfig (FspsUpd);
#endif

  UpdatePchGeneralConfig (SiPolicy, FspsUpd, &PchSetup);
  UpdateGbeConfig (SiPolicy, FspsUpd, &PchSetup);
#if FixedPcdGetBool (PcdEmbeddedEnable)
  UpdateTsnConfig (SiPolicy, FspsUpd, &PchSetup);
#endif
  UpdateSataConfig (SiPolicy, FspsUpd, &PchSetup);
  UpdateThcConfig (SiPolicy, FspsUpd, &PchSetup);
  UpdateDmiConfig (SiPolicy, FspsUpd, &PchSetup);
  UpdatePcieRpConfig (SiPolicy, FspsUpd, &PchSetup, &SetupVariables);
  UpdateHdAudioConfig (SiPolicy, SiPreMemPolicyPpi, FspsUpd, FspmUpd, &PchSetup);
  UpdateLockDownConfig (SiPolicy, FspsUpd, &PchSetup, BiosGuardEnable);
  UpdateRtcConfig (SiPolicy, FspsUpd, &PchSetup);
//[-start-191218-IB16740000-remove]//
//  UpdateFlashProtectionConfig (SiPolicy, FspsUpd, &PchSetup, BiosGuardEnable);
//[-end-191218-IB16740000-remove]//
  //
  //  FIVR Configuration
  //
#if FixedPcdGetBool(PcdAdlLpSupport) == 1
  UpdateFivrConfig (SiPolicy, FspsUpd, &PchSetup);
#endif
  UpdateIehConfig (SiPolicy, FspsUpd, &PchSetup);
  UpdateIoApicConfig (SiPolicy, FspsUpd, &PchSetup, &SetupVariables);
  UpdateInterruptConfig (SiPolicy, FspsUpd, &PchSetup);
  UpdateSerialIoConfig (SiPolicy, FspsUpd, &PchSetup, &DebugConfigData, &SetupVariables);
  UpdatePmConfig (SiPolicy, FspsUpd, &PchSetup, &SetupVariables);
  UpdateThermalConfig (SiPolicy, FspsUpd, &PchSetup, &SaSetup);
  UpdateUsbConfig (SiPolicy, FspsUpd, &PchSetup);
  UpdatePchUsb2PhyConfig (SiPolicy, FspsUpd, &PchSetup);
#if FixedPcdGetBool(PcdAdlLpSupport) == 1
  UpdateUfsConfig (SiPolicy, FspsUpd, &PchSetup);
#endif
  UpdateIshConfig (SiPolicy, FspsUpd, &PchSetup);
  CorrectMismatchConfig (SiPolicy, FspsUpd, &PchSetup);
  UpdateCnviConfig (SiPolicy, FspsUpd, &PchSetup);
  UpdateEspiConfig (SiPolicy, FspsUpd, &PchSetup);
  UpdatePsfConfig (SiPolicy, FspsUpd, &PchSetup);
#if FixedPcdGetBool (PcdEmbeddedEnable)
  UpdateFusaConfig (SiPolicy, FspsUpd, &PchSetup, &SaSetup);
#endif
  UpdateHybridStorageConfig (SiPolicy, FspsUpd, &PchSetup);
  UpdateSpiConfig (SiPolicy, FspsUpd, &PchSetup);
  UpdateHsioPolicy (SiPolicy, FspsUpd, &PchSetup);
//[-start-211109-IB18410136-add]//
  UpdateUsb3HsioConfig (SiPolicy, FspsUpd);
//[-end-211109-IB18410136-add]//
  UpdateOemSnpsPhyPtr (SiPolicy, FspsUpd);
//[-start-190813-17560011-add]//
  DEBUG_OEM_SVC ((DEBUG_INFO, "Base OemChipsetServices Call: OemSvcEcSetDswMode \n"));
  Status = OemSvcEcSetDswMode (&CommandStatus, (UINT8)PchSetup.DeepSxMode);
  DEBUG_OEM_SVC ((DEBUG_INFO, "Base OemChipsetServices OemSvcEcSetDswMode Status: %r\n", Status));
  DEBUG ((DEBUG_INFO | DEBUG_ERROR, "EcSetDswMode ERROR in PeiPchPolicyUpdate! Status is %r.\n", CommandStatus));
//[-end-190813-17560011-add]//

  return EFI_SUCCESS;
}
