/** @file

;******************************************************************************
;* Copyright (c) 2018 - 2021, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/
/** @file
Do Platform Stage System Agent initialization.

@copyright
  INTEL CONFIDENTIAL
  Copyright 2013 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains a 'Sample Driver' and is licensed as such under the terms
  of your license agreement with Intel or your vendor. This file may be modified
  by the user, subject to the additional terms of the license agreement.

@par Specification Reference:
**/

#include "PeiSaPolicyUpdate.h"
#include "MemoryConfig.h"
#include <Guid/AcpiVariable.h>
#include <Guid/MemoryOverwriteControl.h>
#include <Guid/MemoryTypeInformation.h>
#include <Guid/S3MemoryVariable.h>
#include <Library/BaseMemoryLib.h>
#include <Library/CpuPcieInfoFruLib.h>
#include <Library/CpuPlatformLib.h>
#include <Library/GpioLib.h>
#include <Library/HobLib.h>
#include <Library/MeFwStsLib.h>
#include <Library/MemoryAllocationLib.h>
#include <Library/PeiSaPolicyLib.h>
#include <Library/PeiSiPolicyUpdateLib.h>
#include <Library/SiPolicyLib.h>
#include <Library/VtdInfoLib.h>
#include <Register/CommonMsr.h>
#include <Register/Cpuid.h>
#include <Register/Msr.h>
#include <CpuDmiPreMemConfig.h>
#include <CpuRegs.h>
#include <HostBridgeConfig.h>
#include <IpuPreMemConfig.h>
#include <Platform.h>
#include <PlatformBoardConfig.h>
#include <PolicyUpdateMacro.h>
#include <Setup.h>
#include <TelemetryPeiConfig.h>
#include <SaDataHob.h>

#if FixedPcdGet8(PcdFspModeSelection) == 1
#include <FspmUpd.h>
#else
#include <Ppi/FspmArchConfigPpi.h>
#endif

#if FixedPcdGetBool (PcdITbtEnable) == 1
#include <TcssPeiPreMemConfig.h>
#endif  // PcdITbtEnable

///
///
//[-start-210127-IB05660155-add]//
#include <Pins/GpioPinsVer4S.h>
//[-end-210127-IB05660155-add]//
//[-start-210318-IB05660157-add]//
#include <ChipsetSetupConfig.h>
//[-end-210318-IB05660157-add]//
//[-start-191111-IB10189001-add]//
#include <HybridGraphicsDefine.h>
#include <Library/PeiOemSvcKernelLib.h>
//[-end-191111-IB10189001-add]//
//[-start-210518-IB05660166-add]//
#include <PortConfig.h>
//[-end-210518-IB05660166-add]//
///
/// Memory Reserved should be between 125% to 150% of the Current required memory
/// otherwise BdsMisc.c would do a reset to make it 125% to avoid s4 resume issues.
///
GLOBAL_REMOVE_IF_UNREFERENCED EFI_MEMORY_TYPE_INFORMATION mDefaultMemoryTypeInformation[] = {
  { EfiACPIReclaimMemory,   FixedPcdGet32 (PcdPlatformEfiAcpiReclaimMemorySize) },  // ASL
  { EfiACPIMemoryNVS,       FixedPcdGet32 (PcdPlatformEfiAcpiNvsMemorySize) },      // ACPI NVS (including S3 related)
  { EfiReservedMemoryType,  FixedPcdGet32 (PcdPlatformEfiReservedMemorySize) },     // BIOS Reserved (including S3 related)
  { EfiRuntimeServicesData, FixedPcdGet32 (PcdPlatformEfiRtDataMemorySize) },       // Runtime Service Data
  { EfiRuntimeServicesCode, FixedPcdGet32 (PcdPlatformEfiRtCodeMemorySize) },       // Runtime Service Code
  { EfiMaxMemoryType, 0 }
};

//[-start-191111-IB10189001-add]//
/**
  Check the input memory type information is whether valid.

  @param[in] MemTypeInfo    Pointer to input EFI_MEMORY_TYPE_INFORMATION array
  @param[in] MemTypeInfoCnt The count of EFI_MEMORY_TYPE_INFORMATION instance.

  @retval TRUE              The input EFI_MEMORY_TYPE_INFORMATION is valid.
  @retval FALSE             Any of EFI_MEMORY_TYPE_INFORMATION instance in input array is invalid.
--*/
STATIC
BOOLEAN
IsMemoryTyepInfoValid (
  IN EFI_MEMORY_TYPE_INFORMATION       *MemTypeInfo,
  IN UINTN                             MemTypeInfoCnt
  )
{
  UINTN         Index;

  if (MemTypeInfo == NULL && MemTypeInfoCnt != 0) {
    return FALSE;
  }

  for (Index = 0; Index < MemTypeInfoCnt; Index++) {
    if ((MemTypeInfo[Index].NumberOfPages & 0x80000000) != 0 || MemTypeInfo[Index].Type > EfiMaxMemoryType) {
      return FALSE;
    }
  }

  return TRUE;
}
//[-end-191111-IB10189001-add]//

/**
  UpdatePeiSaPolicyPreMem performs SA PEI Policy initialization

  @retval EFI_SUCCESS              The policy is installed and initialized.
**/
EFI_STATUS
EFIAPI
UpdatePeiSaPolicyPreMem (
  VOID
  )
{
  EFI_STATUS                                      Status;
  EFI_STATUS                                      Status2;
  EFI_STATUS                                      Status3;
  UINTN                                           Lane;
  EFI_PEI_READ_ONLY_VARIABLE2_PPI                 *VariableServices;
  SI_SETUP                                        SiSetup;
  SETUP_DATA                                      SetupData;
  SA_SETUP                                        SaSetup;
  CPU_SETUP                                       CpuSetup;
  UINTN                                           VariableSize;
#if FixedPcdGet8(PcdFspModeSelection) == 0
  SA_MEMORY_RCOMP                                 *RcompData;
#endif
  WDT_PPI                                         *gWdtPei;
  UINT8                                           WdtTimeout;
#if FixedPcdGetBool(PcdCpuPcieEnable) == 1
  UINT32                                          RpIndex;
#endif
  UINTN                                           Bundle;
  UINT32                                          RoundedBclkFreq;
  UINT8                                           Index;
  S3_MEMORY_VARIABLE                              S3MemVariable;
  UINTN                                           DataSize;
  EFI_MEMORY_TYPE_INFORMATION                     MemoryData[EfiMaxMemoryType + 1];
  EFI_BOOT_MODE                                   BootMode;
  UINT8                                           MorControl;
  PCH_SETUP                                       PchSetup;
  UINT16                                          MmioSize;
  UINT64                                          PlatformMemorySize;
  UINT16                                          GtVoltageOverride;
  UINT16                                          GtExtraTurboVoltage;
  VOID                                            *MemorySavedData;
  VOID                                            *NullSpdPtr;
  UINT32                          RpEnabledMask;
#if FixedPcdGetBool(PcdITbtEnable) == 1
  UINT32                          PciexBarReg;
#endif
#if FixedPcdGet8(PcdFspModeSelection) == 1
  VOID                                            *FspmUpd;
  VOID                                            *MemorySpdPtr000;
  VOID                                            *MemorySpdPtr010;
  VOID                                            *MemorySpdPtr020;
  VOID                                            *MemorySpdPtr030;
  VOID                                            *MemorySpdPtr100;
  VOID                                            *MemorySpdPtr110;
  VOID                                            *MemorySpdPtr120;
  VOID                                            *MemorySpdPtr130;
#else
#if FixedPcdGetBool(PcdHgEnable) == 1
  HYBRID_GRAPHICS_CONFIG                          *HgGpioData;
#endif
  SI_PREMEM_POLICY_PPI                            *SiPreMemPolicyPpi;
  GRAPHICS_PEI_PREMEM_CONFIG                      *GtPreMemConfig;
  MEMORY_CONFIGURATION                            *MemConfig;
#if FixedPcdGetBool(PcdCpuPcieEnable) == 1
  PCIE_PEI_PREMEM_CONFIG                          *PciePeiPreMemConfig;
#endif
#if FixedPcdGetBool(PcdIpuEnable) == 1
  IPU_PREMEM_CONFIG                               *IpuPreMemPolicy;
#endif
  OVERCLOCKING_PREMEM_CONFIG                      *OcPreMemConfig;
  SA_MISC_PEI_PREMEM_CONFIG                       *MiscPeiPreMemConfig;
  MEMORY_CONFIG_NO_CRC                            *MemConfigNoCrc;

#if FixedPcdGetBool(PcdITbtEnable) == 1
  TCSS_PEI_PREMEM_CONFIG                          *TcssPeiPreMemConfig;
#endif

  CPU_TRACE_HUB_PREMEM_CONFIG                     *CpuTraceHubPreMemConfig;
  VTD_CONFIG                                      *Vtd;
#if FixedPcdGetBool(PcdCpuPcieEnable) == 1
  CPU_PCIE_RP_PREMEM_CONFIG                       *CpuPcieRpPreMemConfig;
#endif
  CPU_DMI_PREMEM_CONFIG                           *CpuDmiPreMemConfig;
  TELEMETRY_PEI_PREMEM_CONFIG                     *TelemetryPreMemConfig;
  EFI_PEI_PPI_DESCRIPTOR                          *FspmArchConfigPpiDesc;
  FSPM_ARCH_CONFIG_PPI                            *FspmArchConfigPpi;
  HOST_BRIDGE_PREMEM_CONFIG                       *HostBridgePreMemConfig;
#endif
  EFI_PHYSICAL_ADDRESS                            PlatformVtdEnableDmaBuffer;
  UINT16                                          AdjustedMmioSize;
  UINT8                                           SaDisplayConfigTable[16];
  EFI_BOOT_MODE                                   SysBootMode;
  UINT32                                          ProcessorTraceTotalMemSize;
//[-start-200420-IB17800058-modify]//
//  MSR_CORE_THREAD_COUNT_REGISTER                  MsrCoreThreadCount;
//  CPUID_STRUCTURED_EXTENDED_FEATURE_FLAGS_EBX     Ebx;
//[-end-200420-IB17800058-modify]//
  UINT32                                          CapsuleSupportMemSize;
//[-start-190611-IB16990040-add]//
  UINT8                           *DimmSmbusAddrArray;
//[-end-190611-IB16990040-add]//
//[-start-191111-IB10189001-add]//
  EFI_MEMORY_TYPE_INFORMATION     *MemoryInformation;
//[-end-191111-IB10189001-add]//
//[-start-210428-IB05660158-add]//
  CHIPSET_CONFIGURATION                           ChipsetConfiguration;
//[-end-210428-IB05660158-add]//
//[-start-210518-IB05660166-add]//
  UINT8                           PcieNonCpuPortNumber;
  PORT_CONFIG                     CpuPciePortConfig;
//[-end-210518-IB05660166-add]//

  DEBUG ((DEBUG_INFO, "Update PeiSaPolicyUpdate Pre-Mem Start\n"));
  ZeroMem ((VOID*) SaDisplayConfigTable, sizeof (SaDisplayConfigTable));
  WdtTimeout           = 0;
  SysBootMode          = 0;
#if FixedPcdGet8(PcdFspModeSelection) == 0
  RcompData            = NULL;
#if FixedPcdGetBool(PcdHgEnable) == 1
  HgGpioData           = NULL;
#endif
#endif
  PlatformMemorySize   = 0;
  RpEnabledMask        = 0;
#if FixedPcdGet8(PcdFspModeSelection) == 1
  FspmUpd              = NULL;
  MemorySpdPtr000      = NULL;
  MemorySpdPtr010      = NULL;
  MemorySpdPtr020      = NULL;
  MemorySpdPtr030      = NULL;
  MemorySpdPtr100      = NULL;
  MemorySpdPtr110      = NULL;
  MemorySpdPtr120      = NULL;
  MemorySpdPtr130      = NULL;
#else
  SiPreMemPolicyPpi    = NULL;
  GtPreMemConfig       = NULL;
  MemConfig            = NULL;
#if FixedPcdGetBool(PcdIpuEnable) == 1
  IpuPreMemPolicy      = NULL;
#endif
  MemConfigNoCrc       = NULL;
#if FixedPcdGetBool(PcdCpuPcieEnable) == 1
  PciePeiPreMemConfig  = NULL;
#endif
  OcPreMemConfig       = NULL;

#if FixedPcdGetBool(PcdITbtEnable) == 1
  TcssPeiPreMemConfig  = NULL;
#endif

  MiscPeiPreMemConfig  = NULL;
  CpuTraceHubPreMemConfig = NULL;
  TelemetryPreMemConfig = NULL;
  HostBridgePreMemConfig = NULL;
  FspmArchConfigPpi    = NULL;
#endif

  ProcessorTraceTotalMemSize = 0;
  CapsuleSupportMemSize = 0;

  AdjustedMmioSize = PcdGet16 (PcdSaMiscMmioSizeAdjustment);

#if FixedPcdGet8(PcdFspModeSelection) == 1
  FspmUpd = (FSPM_UPD *) PcdGet32 (PcdFspmUpdDataAddress);
  ASSERT (FspmUpd != NULL);
#else
  Status = PeiServicesLocatePpi (&gSiPreMemPolicyPpiGuid, 0, NULL, (VOID **) &SiPreMemPolicyPpi);
  ASSERT_EFI_ERROR (Status);

  Status = GetConfigBlock ((VOID *) SiPreMemPolicyPpi, &gHostBridgePeiPreMemConfigGuid, (VOID *) &HostBridgePreMemConfig);
  ASSERT_EFI_ERROR (Status);

  Status = GetConfigBlock((VOID *) SiPreMemPolicyPpi, &gSaMiscPeiPreMemConfigGuid, (VOID *) &MiscPeiPreMemConfig);
  ASSERT_EFI_ERROR(Status);

  Status = GetConfigBlock((VOID *) SiPreMemPolicyPpi, &gGraphicsPeiPreMemConfigGuid, (VOID *) &GtPreMemConfig);
  ASSERT_EFI_ERROR(Status);

  Status = GetConfigBlock((VOID *) SiPreMemPolicyPpi, &gMemoryConfigGuid, (VOID *) &MemConfig);
  ASSERT_EFI_ERROR(Status);

#if FixedPcdGetBool(PcdCpuPcieEnable) == 1
  Status = GetConfigBlock((VOID *) SiPreMemPolicyPpi, &gCpuPciePeiPreMemConfigGuid, (VOID *) &PciePeiPreMemConfig);
  ASSERT_EFI_ERROR(Status);
#endif
#if FixedPcdGetBool(PcdHgEnable) == 1
  Status = GetConfigBlock((VOID *) SiPreMemPolicyPpi, &gHybridGraphicsConfigGuid, (VOID *) &HgGpioData);
  ASSERT_EFI_ERROR(Status);
#endif
#if FixedPcdGetBool(PcdIpuEnable) == 1
  Status = GetConfigBlock((VOID *) SiPreMemPolicyPpi, &gIpuPreMemConfigGuid, (VOID *) &IpuPreMemPolicy);
  ASSERT_EFI_ERROR (Status);
#endif
  Status = GetConfigBlock((VOID *) SiPreMemPolicyPpi, &gMemoryConfigNoCrcGuid, (VOID *) &MemConfigNoCrc);
  ASSERT_EFI_ERROR(Status);

  Status = GetConfigBlock ((VOID *) SiPreMemPolicyPpi, &gCpuTraceHubPreMemConfigGuid, (VOID *) &CpuTraceHubPreMemConfig);
  ASSERT_EFI_ERROR(Status);

  Status = GetConfigBlock((VOID *) SiPreMemPolicyPpi, &gOverclockingPreMemConfigGuid, (VOID *) &OcPreMemConfig);
  ASSERT_EFI_ERROR(Status);

#if FixedPcdGetBool(PcdITbtEnable) == 1
  Status = GetConfigBlock ((VOID *)SiPreMemPolicyPpi, &gTcssPeiPreMemConfigGuid, (VOID *) &TcssPeiPreMemConfig);
  ASSERT_EFI_ERROR(Status);
#endif  // PcdITbtEnable

  Status = GetConfigBlock ((VOID *)SiPreMemPolicyPpi, &gTelemetryPeiPreMemConfigGuid, (VOID *) &TelemetryPreMemConfig);
  ASSERT_EFI_ERROR(Status);

  Status = GetConfigBlock((VOID *) SiPreMemPolicyPpi, &gVtdConfigGuid, (VOID *)&Vtd);
  ASSERT_EFI_ERROR(Status);

#if FixedPcdGetBool(PcdCpuPcieEnable) == 1
  Status = GetConfigBlock ((VOID *)SiPreMemPolicyPpi, &gCpuPcieRpPrememConfigGuid, (VOID *)&CpuPcieRpPreMemConfig);
  ASSERT_EFI_ERROR(Status);
#endif

  CpuDmiPreMemConfig = NULL;
  Status = GetConfigBlock ((VOID *) SiPreMemPolicyPpi, &gCpuDmiPreMemConfigGuid, (VOID *)&CpuDmiPreMemConfig);
  ASSERT_EFI_ERROR (Status);
#endif

#if FixedPcdGet8(PcdFspModeSelection) == 0
  RcompData = MemConfigNoCrc->RcompData;
#endif

  //
  // Locate system configuration variable
  //
  Status = PeiServicesLocatePpi(
             &gEfiPeiReadOnlyVariable2PpiGuid, // GUID
             0,                                // INSTANCE
             NULL,                             // EFI_PEI_PPI_DESCRIPTOR
             (VOID **) &VariableServices       // PPI
             );
  ASSERT_EFI_ERROR(Status);

  Status = PeiServicesGetBootMode (&BootMode);
  ASSERT_EFI_ERROR (Status);

  //
  // Initialize S3 Data variable (S3DataPtr)
  //
  VariableSize = 0;
  MemorySavedData = NULL;
  Status = VariableServices->GetVariable (
                               VariableServices,
                               L"MemoryConfig",
                               &gMemoryConfigVariableGuid,
                               NULL,
                               &VariableSize,
                               MemorySavedData
                               );
  if (Status == EFI_BUFFER_TOO_SMALL) {
    MemorySavedData = AllocateZeroPool (VariableSize);
    ASSERT (MemorySavedData != NULL);

    Status = VariableServices->GetVariable (
                                 VariableServices,
                                 L"MemoryConfig",
                                 &gMemoryConfigVariableGuid,
                                 NULL,
                                 &VariableSize,
                                 MemorySavedData
                                 );
    if (EFI_ERROR (Status)) {
      DEBUG ((DEBUG_ERROR, "Fail to retrieve Variable: MemoryConfig, Status = %r\n", Status));
      ASSERT_EFI_ERROR (Status);
    }
  }
#if FixedPcdGet8(PcdFspModeSelection) == 0
  FspmArchConfigPpi = (FSPM_ARCH_CONFIG_PPI *) AllocateZeroPool (sizeof (FSPM_ARCH_CONFIG_PPI));
  if (FspmArchConfigPpi == NULL) {
    ASSERT (FALSE);
    return EFI_OUT_OF_RESOURCES;
  }
  FspmArchConfigPpi->Revision     = 1;
  FspmArchConfigPpi->NvsBufferPtr = MemorySavedData;
  MiscPeiPreMemConfig->S3DataPtr  = MemorySavedData;

  FspmArchConfigPpiDesc = (EFI_PEI_PPI_DESCRIPTOR *) AllocateZeroPool (sizeof (EFI_PEI_PPI_DESCRIPTOR));
  if (FspmArchConfigPpiDesc == NULL) {
    ASSERT (FALSE);
    return EFI_OUT_OF_RESOURCES;
  }
  FspmArchConfigPpiDesc->Flags = EFI_PEI_PPI_DESCRIPTOR_PPI | EFI_PEI_PPI_DESCRIPTOR_TERMINATE_LIST;
  FspmArchConfigPpiDesc->Guid  = &gFspmArchConfigPpiGuid;
  FspmArchConfigPpiDesc->Ppi   = FspmArchConfigPpi;

  //
  // Install FSP-M Arch Config PPI
  //
  Status = PeiServicesInstallPpi (FspmArchConfigPpiDesc);
  ASSERT_EFI_ERROR (Status);

#else
  UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmArchUpd.NvsBufferPtr, MiscPeiPreMemConfig->S3DataPtr, MemorySavedData);
#endif




  //
  // Get S3 Memory Variable
  //
  VariableSize = sizeof (S3_MEMORY_VARIABLE);
  Status = VariableServices->GetVariable(
                               VariableServices,
                               S3_MEMORY_VARIABLE_NAME,
                               &gS3MemoryVariableGuid,
                               NULL,
                               &VariableSize,
                               &S3MemVariable
                               );
  if (Status == EFI_SUCCESS) {
#if FixedPcdGet8(PcdFspModeSelection) == 0
    MiscPeiPreMemConfig->AcpiReservedMemoryBase = S3MemVariable.AcpiReservedMemoryBase;
    MiscPeiPreMemConfig->AcpiReservedMemorySize = S3MemVariable.AcpiReservedMemorySize;
    MiscPeiPreMemConfig->SystemMemoryLength     = S3MemVariable.SystemMemoryLength;
#endif
  }

  VariableSize = sizeof (MorControl);
  Status = VariableServices->GetVariable(
                               VariableServices,
                               MEMORY_OVERWRITE_REQUEST_VARIABLE_NAME,
                               &gEfiMemoryOverwriteControlDataGuid,
                               NULL,
                               &VariableSize,
                               &MorControl
                               );
  if (EFI_ERROR (Status)) {
    MorControl = 0;
  }

  //
  // Get System configuration variables
  //
  VariableSize = sizeof (SI_SETUP);
  Status = VariableServices->GetVariable (
                               VariableServices,
                               L"SiSetup",
                               &gSiSetupVariableGuid,
                               NULL,
                               &VariableSize,
                               &SiSetup
                               );

  VariableSize = sizeof (SETUP_DATA);
  Status = VariableServices->GetVariable (
                               VariableServices,
                               PLATFORM_SETUP_VARIABLE_NAME,
                               &gSetupVariableGuid,
                               NULL,
                               &VariableSize,
                               &SetupData
                               );
  ASSERT_EFI_ERROR(Status);

  VariableSize = sizeof (SA_SETUP);
  Status2 = VariableServices->GetVariable (
                                VariableServices,
                                L"SaSetup",
                                &gSaSetupVariableGuid,
                                NULL,
                                &VariableSize,
                                &SaSetup
                                );
  ASSERT_EFI_ERROR(Status2);

  VariableSize = sizeof (CPU_SETUP);
  Status3 = VariableServices->GetVariable (
                                VariableServices,
                                L"CpuSetup",
                                &gCpuSetupVariableGuid,
                                NULL,
                                &VariableSize,
                                &CpuSetup
                                );
  ASSERT_EFI_ERROR(Status3);

  VariableSize = sizeof (PCH_SETUP);
  VariableServices->GetVariable (
                      VariableServices,
                      L"PchSetup",
                      &gPchSetupVariableGuid,
                      NULL,
                      &VariableSize,
                      &PchSetup
                      );

//[-start-210428-IB05660158-add]//
    VariableSize = sizeof (CHIPSET_CONFIGURATION);
    VariableServices->GetVariable (
                        VariableServices,
                        L"Setup",
                        &gSystemConfigurationGuid,
                        NULL,
                        &VariableSize,
                        &ChipsetConfiguration
                        );
//[-end-210428-IB05660158-add]//

  //
  // Adjust MMIO size for TBT
  //
//[-start-210223-IB17040190-modify]//
#if FixedPcdGetBool(PcdITbtEnable) == 1
  for (Index = 0; Index < ITBT_ROOT_PORTS_SUPPORTED; Index++) {
    if (SetupData.ITbtRootPort[Index] == 1) {
      AdjustedMmioSize += SetupData.ITbtPcieMemRsvd[Index];
    }
  }
#endif

  PcdSet16S (PcdSaMiscMmioSizeAdjustment, AdjustedMmioSize);

#if FixedPcdGetBool (PcdDTbtEnable) == 1
  for (Index = 0; Index < DTBT_CONTROLLER_SUPPORTED; Index++) {
    if (SetupData.DTbtController[Index] == 1) {
      AdjustedMmioSize += SetupData.DTbtPcieMemRsvd[Index];
    }
  }
//[-end-210223-IB17040190-modify]//

  PcdSet16S (PcdSaMiscMmioSizeAdjustment, AdjustedMmioSize);
#endif

  UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.UserBd,     MiscPeiPreMemConfig->UserBd,      0); // It's a CRB mobile board by default (btCRBMB)
  COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.CridEnable, MiscPeiPreMemConfig->CridEnable, SaSetup.CridEnable, NullIndex);

#if (FixedPcdGet8(PcdFspModeSelection) == 0)
  MiscPeiPreMemConfig->TxtImplemented = 0;
#endif

  COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.CpuCrashLogDevice, TelemetryPreMemConfig->CpuCrashLogDevice, SaSetup.CpuCrashLogDevice, NullIndex);

#if FixedPcdGetBool(PcdCpuPcieEnable) == 1
  for (RpIndex = 0; RpIndex < GetMaxCpuPciePortNum (); RpIndex++) {
    COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *)FspmUpd)->FspmConfig.CpuPcieRpPcieSpeed[RpIndex],         CpuPcieRpPreMemConfig->PcieSpeed[RpIndex],         SaSetup.PcieRootPortSpeed[RpIndex],             RpIndex);
    COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *)FspmUpd)->FspmConfig.CpuPcieRpClockReqMsgEnable[RpIndex], CpuPcieRpPreMemConfig->ClkReqMsgEnableRp[RpIndex], SaSetup.PcieRootPortClockReqMsgEnable[RpIndex], RpIndex);
    COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *)FspmUpd)->FspmConfig.CpuPcieNewFom[RpIndex],              CpuPcieRpPreMemConfig->NewFom[RpIndex],            SaSetup.PcieRootPortNewFom[RpIndex],            RpIndex);
//[-start-210518-IB05660166-modify]//
    PcieNonCpuPortNumber    = PcdGet8 (PcdH2OPcieNonCpuPortNumber);
    CpuPciePortConfig.Value = PcdGet64 (PcdH2OChipsetPciePortEnable);
    if ((CpuPciePortConfig.Value >> ((RpIndex + PcieNonCpuPortNumber) << 1)) & 0x3) {
//[-end-210518-IB05660166-modify]//
      RpEnabledMask |=  (UINT32) (1 << RpIndex);
    } else {
      RpEnabledMask &= ~(UINT32) (1 << RpIndex);
    }
  }
  COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *)FspmUpd)->FspmConfig.CpuPcieRpLinkDownGpios,              CpuPcieRpPreMemConfig->LinkDownGpios,                                    SaSetup.PcieRootPortLinkDownGpios,    NullIndex);
  // RpEnabledMask value is related with Setup value, Need to check Policy Default
  COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *)FspmUpd)->FspmConfig.CpuPcieRpEnableMask,                 CpuPcieRpPreMemConfig->RpEnabledMask,                                    RpEnabledMask,                        NullIndex);
  COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *)FspmUpd)->FspmConfig.CpuPcieRpCdrRelock[0],               CpuPcieRpPreMemConfig->CdrRelock[0],                                     SaSetup.PcieRootPortCdrRelock[0],     NullIndex);
#endif

  if (PcdGet32 (PcdMrcRcompTarget)) {
    COPY_POLICY ((VOID *)((FSPM_UPD *) FspmUpd)->FspmConfig.RcompTarget, (VOID *)RcompData->RcompTarget, (VOID *)(UINTN)PcdGet32 (PcdMrcRcompTarget), sizeof (UINT16) * MRC_MAX_RCOMP_TARGETS);
  }

  if (PcdGetBool (PcdMrcDqPinsInterleavedControl)) {
    UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.DqPinsInterleaved, MemConfig->DqPinsInterleaved, PcdGetBool (PcdMrcDqPinsInterleaved));
  }

  UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.SpdAddressTable[0], MiscPeiPreMemConfig->SpdAddressTable[0], PcdGet8 (PcdMrcSpdAddressTable0));
  UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.SpdAddressTable[1], MiscPeiPreMemConfig->SpdAddressTable[1], PcdGet8 (PcdMrcSpdAddressTable1));
  UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.SpdAddressTable[2], MiscPeiPreMemConfig->SpdAddressTable[2], PcdGet8 (PcdMrcSpdAddressTable2));
  UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.SpdAddressTable[3], MiscPeiPreMemConfig->SpdAddressTable[3], PcdGet8 (PcdMrcSpdAddressTable3));
  if (PcdGet8 (PcdMrcLp5CccConfig)) {
    UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.Lp5CccConfig, MemConfig->Lp5CccConfig, PcdGet8 (PcdMrcLp5CccConfig));
  }


  NullSpdPtr = AllocateZeroPool (SPD_DATA_SIZE);
  ASSERT (NullSpdPtr != NULL);
//[-start-190611-IB16990040-add]//
  DimmSmbusAddrArray = AllocateZeroPool (FixedPcdGet8(PcdMaxSockets));
  //
  // Default setting
  //
  if (DimmSmbusAddrArray != NULL) {
    DimmSmbusAddrArray[0] = PcdGet8 (PcdMrcSpdAddressTable0);
    DimmSmbusAddrArray[1] = PcdGet8 (PcdMrcSpdAddressTable1);
    DimmSmbusAddrArray[2] = PcdGet8 (PcdMrcSpdAddressTable2);
    DimmSmbusAddrArray[3] = PcdGet8 (PcdMrcSpdAddressTable3);
  }

  //
  // Build HOB for consumer.
  //
  BuildGuidDataHob (&gDimmSmbusAddrHobGuid, DimmSmbusAddrArray,  FixedPcdGet8(PcdMaxSockets));
//[-end-190611-IB16990040-add]//

#if FixedPcdGet8(PcdFspModeSelection) == 1
  MemorySpdPtr000 = AllocateZeroPool (SPD_DATA_SIZE);
  ASSERT (MemorySpdPtr000 != NULL);
  MemorySpdPtr010 = AllocateZeroPool (SPD_DATA_SIZE);
  ASSERT (MemorySpdPtr010 != NULL);
  MemorySpdPtr020 = AllocateZeroPool (SPD_DATA_SIZE);
  ASSERT (MemorySpdPtr020 != NULL);
  MemorySpdPtr030 = AllocateZeroPool (SPD_DATA_SIZE);
  ASSERT (MemorySpdPtr030 != NULL);
  MemorySpdPtr100 = AllocateZeroPool (SPD_DATA_SIZE);
  ASSERT (MemorySpdPtr100 != NULL);
  MemorySpdPtr110 = AllocateZeroPool (SPD_DATA_SIZE);
  ASSERT (MemorySpdPtr110 != NULL);
  MemorySpdPtr120 = AllocateZeroPool (SPD_DATA_SIZE);
  ASSERT (MemorySpdPtr120 != NULL);
  MemorySpdPtr130 = AllocateZeroPool (SPD_DATA_SIZE);
  ASSERT (MemorySpdPtr130 != NULL);
  ((FSPM_UPD *) FspmUpd)->FspmConfig.MemorySpdPtr000 = (UINT32) MemorySpdPtr000;
  ((FSPM_UPD *) FspmUpd)->FspmConfig.MemorySpdPtr010 = (UINT32) MemorySpdPtr010;
  ((FSPM_UPD *) FspmUpd)->FspmConfig.MemorySpdPtr020 = (UINT32) MemorySpdPtr020;
  ((FSPM_UPD *) FspmUpd)->FspmConfig.MemorySpdPtr030 = (UINT32) MemorySpdPtr030;
  ((FSPM_UPD *) FspmUpd)->FspmConfig.MemorySpdPtr100 = (UINT32) MemorySpdPtr100;
  ((FSPM_UPD *) FspmUpd)->FspmConfig.MemorySpdPtr110 = (UINT32) MemorySpdPtr110;
  ((FSPM_UPD *) FspmUpd)->FspmConfig.MemorySpdPtr120 = (UINT32) MemorySpdPtr120;
  ((FSPM_UPD *) FspmUpd)->FspmConfig.MemorySpdPtr130 = (UINT32) MemorySpdPtr130;
#endif
  UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.SpdAddressTable[4], MiscPeiPreMemConfig->SpdAddressTable[4], PcdGet8 (PcdMrcSpdAddressTable4));
  UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.SpdAddressTable[5], MiscPeiPreMemConfig->SpdAddressTable[5], PcdGet8 (PcdMrcSpdAddressTable5));
  UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.SpdAddressTable[6], MiscPeiPreMemConfig->SpdAddressTable[6], PcdGet8 (PcdMrcSpdAddressTable6));
  UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.SpdAddressTable[7], MiscPeiPreMemConfig->SpdAddressTable[7], PcdGet8 (PcdMrcSpdAddressTable7));
  UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.SpdAddressTable[8], MiscPeiPreMemConfig->SpdAddressTable[8], PcdGet8 (PcdMrcSpdAddressTable8));
  UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.SpdAddressTable[9], MiscPeiPreMemConfig->SpdAddressTable[9], PcdGet8 (PcdMrcSpdAddressTable9));
  UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.SpdAddressTable[10], MiscPeiPreMemConfig->SpdAddressTable[10], PcdGet8 (PcdMrcSpdAddressTable10));
  UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.SpdAddressTable[11], MiscPeiPreMemConfig->SpdAddressTable[11], PcdGet8 (PcdMrcSpdAddressTable11));
  UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.SpdAddressTable[12], MiscPeiPreMemConfig->SpdAddressTable[12], PcdGet8 (PcdMrcSpdAddressTable12));
  UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.SpdAddressTable[13], MiscPeiPreMemConfig->SpdAddressTable[13], PcdGet8 (PcdMrcSpdAddressTable13));
  UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.SpdAddressTable[14], MiscPeiPreMemConfig->SpdAddressTable[14], PcdGet8 (PcdMrcSpdAddressTable14));
  UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.SpdAddressTable[15], MiscPeiPreMemConfig->SpdAddressTable[15], PcdGet8 (PcdMrcSpdAddressTable15));
  if (PcdGet32 (PcdMrcRcompResistor)) {
    UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.RcompResistor, RcompData->RcompResistor, (UINT8) PcdGet32 (PcdMrcRcompResistor));
  }
  if (PcdGet32 (PcdMrcDqsMapCpu2Dram)) {
    COPY_POLICY ((VOID *)((FSPM_UPD *) FspmUpd)->FspmConfig.DqsMapCpu2DramMc0Ch0, (VOID *)MemConfigNoCrc->DqDqsMap->DqsMapCpu2Dram, (VOID *)(UINTN)PcdGet32 (PcdMrcDqsMapCpu2Dram), sizeof (UINT8) * MEM_CFG_MAX_CONTROLLERS * MEM_CFG_MAX_CHANNELS * MEM_CFG_NUM_BYTES_MAPPED);
  }
  if (PcdGet32 (PcdMrcDqMapCpu2Dram)) {
    COPY_POLICY ((VOID *)((FSPM_UPD *) FspmUpd)->FspmConfig.DqMapCpu2DramMc0Ch0, (VOID *)MemConfigNoCrc->DqDqsMap->DqMapCpu2Dram, (VOID *)(UINTN)PcdGet32 (PcdMrcDqMapCpu2Dram), sizeof (UINT8) * MEM_CFG_MAX_CONTROLLERS * MEM_CFG_MAX_CHANNELS * MEM_CFG_NUM_BYTES_MAPPED * 8);
  }

//[-start-210721-QINGLIN0001-modify]//
#if defined(S570_SUPPORT)
  COPY_POLICY ((VOID *)((FSPM_UPD *) FspmUpd)->FspmConfig.MemorySpdPtr000, (VOID *) MemConfigNoCrc->SpdData->SpdData[0][0][0], PcdGetPtr (PcdH2OMemoryDownSpdDataMc0Ch0Dimm0), SPD_DATA_SIZE);
  COPY_POLICY ((VOID *)((FSPM_UPD *) FspmUpd)->FspmConfig.MemorySpdPtr010, (VOID *) MemConfigNoCrc->SpdData->SpdData[0][1][0], PcdGetPtr (PcdH2OMemoryDownSpdDataMc0Ch0Dimm0), SPD_DATA_SIZE);
  COPY_POLICY ((VOID *)((FSPM_UPD *) FspmUpd)->FspmConfig.MemorySpdPtr020, (VOID *) MemConfigNoCrc->SpdData->SpdData[0][2][0], PcdGetPtr (PcdH2OMemoryDownSpdDataMc0Ch0Dimm0), SPD_DATA_SIZE);
  COPY_POLICY ((VOID *)((FSPM_UPD *) FspmUpd)->FspmConfig.MemorySpdPtr030, (VOID *) MemConfigNoCrc->SpdData->SpdData[0][3][0], PcdGetPtr (PcdH2OMemoryDownSpdDataMc0Ch0Dimm0), SPD_DATA_SIZE);
  COPY_POLICY ((VOID *)((FSPM_UPD *) FspmUpd)->FspmConfig.MemorySpdPtr100, (VOID *) MemConfigNoCrc->SpdData->SpdData[1][0][0], PcdGetPtr (PcdH2OMemoryDownSpdDataMc1Ch0Dimm0), SPD_DATA_SIZE);
  COPY_POLICY ((VOID *)((FSPM_UPD *) FspmUpd)->FspmConfig.MemorySpdPtr110, (VOID *) MemConfigNoCrc->SpdData->SpdData[1][1][0], PcdGetPtr (PcdH2OMemoryDownSpdDataMc1Ch0Dimm0), SPD_DATA_SIZE);
  COPY_POLICY ((VOID *)((FSPM_UPD *) FspmUpd)->FspmConfig.MemorySpdPtr120, (VOID *) MemConfigNoCrc->SpdData->SpdData[1][2][0], PcdGetPtr (PcdH2OMemoryDownSpdDataMc1Ch0Dimm0), SPD_DATA_SIZE);
  COPY_POLICY ((VOID *)((FSPM_UPD *) FspmUpd)->FspmConfig.MemorySpdPtr130, (VOID *) MemConfigNoCrc->SpdData->SpdData[1][3][0], PcdGetPtr (PcdH2OMemoryDownSpdDataMc1Ch0Dimm0), SPD_DATA_SIZE);
//[-start-210803-QINGLIN0008-add]//
#elif defined(S370_SUPPORT)
  if (PcdGetBool (PcdSpdPresent)) {
    // Clear SPD data so it can be filled in by the MRC init code
    COPY_POLICY ((VOID *)((FSPM_UPD *) FspmUpd)->FspmConfig.MemorySpdPtr000, (VOID *) MemConfigNoCrc->SpdData->SpdData[0][0][0], (VOID *)(UINT32) NullSpdPtr, SPD_DATA_SIZE);
  } else {
    if (PcdGet32 (PcdMrcSpdData)) {
      COPY_POLICY ((VOID *)((FSPM_UPD *) FspmUpd)->FspmConfig.MemorySpdPtr000, (VOID *)MemConfigNoCrc->SpdData->SpdData[0][0][0], (VOID *)(UINTN)PcdGet32 (PcdMrcSpdData), SPD_DATA_SIZE);
    }
  }
  COPY_POLICY ((VOID *)((FSPM_UPD *) FspmUpd)->FspmConfig.MemorySpdPtr010, (VOID *) MemConfigNoCrc->SpdData->SpdData[0][1][0], PcdGetPtr (PcdH2OMemoryDownSpdDataMc0Ch0Dimm0), SPD_DATA_SIZE);
  COPY_POLICY ((VOID *)((FSPM_UPD *) FspmUpd)->FspmConfig.MemorySpdPtr020, (VOID *) MemConfigNoCrc->SpdData->SpdData[0][2][0], PcdGetPtr (PcdH2OMemoryDownSpdDataMc0Ch0Dimm0), SPD_DATA_SIZE);
  COPY_POLICY ((VOID *)((FSPM_UPD *) FspmUpd)->FspmConfig.MemorySpdPtr030, (VOID *) MemConfigNoCrc->SpdData->SpdData[0][3][0], PcdGetPtr (PcdH2OMemoryDownSpdDataMc0Ch0Dimm0), SPD_DATA_SIZE);
  COPY_POLICY ((VOID *)((FSPM_UPD *) FspmUpd)->FspmConfig.MemorySpdPtr100, (VOID *) MemConfigNoCrc->SpdData->SpdData[1][0][0], PcdGetPtr (PcdH2OMemoryDownSpdDataMc1Ch0Dimm0), SPD_DATA_SIZE);
  COPY_POLICY ((VOID *)((FSPM_UPD *) FspmUpd)->FspmConfig.MemorySpdPtr110, (VOID *) MemConfigNoCrc->SpdData->SpdData[1][1][0], PcdGetPtr (PcdH2OMemoryDownSpdDataMc1Ch0Dimm0), SPD_DATA_SIZE);
  COPY_POLICY ((VOID *)((FSPM_UPD *) FspmUpd)->FspmConfig.MemorySpdPtr120, (VOID *) MemConfigNoCrc->SpdData->SpdData[1][2][0], PcdGetPtr (PcdH2OMemoryDownSpdDataMc1Ch0Dimm0), SPD_DATA_SIZE);
  COPY_POLICY ((VOID *)((FSPM_UPD *) FspmUpd)->FspmConfig.MemorySpdPtr130, (VOID *) MemConfigNoCrc->SpdData->SpdData[1][3][0], PcdGetPtr (PcdH2OMemoryDownSpdDataMc1Ch0Dimm0), SPD_DATA_SIZE);
//[-end-210803-QINGLIN0008-add]//
#else
  if (PcdGetBool (PcdSpdPresent)) {
    // Clear SPD data so it can be filled in by the MRC init code
    COPY_POLICY ((VOID *)((FSPM_UPD *) FspmUpd)->FspmConfig.MemorySpdPtr000, (VOID *) MemConfigNoCrc->SpdData->SpdData[0][0][0], (VOID *)(UINT32) NullSpdPtr, SPD_DATA_SIZE);
    COPY_POLICY ((VOID *)((FSPM_UPD *) FspmUpd)->FspmConfig.MemorySpdPtr010, (VOID *) MemConfigNoCrc->SpdData->SpdData[0][1][0], (VOID *)(UINT32) NullSpdPtr, SPD_DATA_SIZE);
    COPY_POLICY ((VOID *)((FSPM_UPD *) FspmUpd)->FspmConfig.MemorySpdPtr020, (VOID *) MemConfigNoCrc->SpdData->SpdData[0][2][0], (VOID *)(UINT32) NullSpdPtr, SPD_DATA_SIZE);
    COPY_POLICY ((VOID *)((FSPM_UPD *) FspmUpd)->FspmConfig.MemorySpdPtr030, (VOID *) MemConfigNoCrc->SpdData->SpdData[0][3][0], (VOID *)(UINT32) NullSpdPtr, SPD_DATA_SIZE);
    COPY_POLICY ((VOID *)((FSPM_UPD *) FspmUpd)->FspmConfig.MemorySpdPtr100, (VOID *) MemConfigNoCrc->SpdData->SpdData[1][0][0], (VOID *)(UINT32) NullSpdPtr, SPD_DATA_SIZE);
    COPY_POLICY ((VOID *)((FSPM_UPD *) FspmUpd)->FspmConfig.MemorySpdPtr110, (VOID *) MemConfigNoCrc->SpdData->SpdData[1][1][0], (VOID *)(UINT32) NullSpdPtr, SPD_DATA_SIZE);
    COPY_POLICY ((VOID *)((FSPM_UPD *) FspmUpd)->FspmConfig.MemorySpdPtr120, (VOID *) MemConfigNoCrc->SpdData->SpdData[1][2][0], (VOID *)(UINT32) NullSpdPtr, SPD_DATA_SIZE);
    COPY_POLICY ((VOID *)((FSPM_UPD *) FspmUpd)->FspmConfig.MemorySpdPtr130, (VOID *) MemConfigNoCrc->SpdData->SpdData[1][3][0], (VOID *)(UINT32) NullSpdPtr, SPD_DATA_SIZE);
  } else {
    if (PcdGet32 (PcdMrcSpdData)) {
      COPY_POLICY ((VOID *)((FSPM_UPD *) FspmUpd)->FspmConfig.MemorySpdPtr000, (VOID *)MemConfigNoCrc->SpdData->SpdData[0][0][0], (VOID *)(UINTN)PcdGet32 (PcdMrcSpdData), SPD_DATA_SIZE);
      COPY_POLICY ((VOID *)((FSPM_UPD *) FspmUpd)->FspmConfig.MemorySpdPtr010, (VOID *)MemConfigNoCrc->SpdData->SpdData[0][1][0], (VOID *)(UINTN)PcdGet32 (PcdMrcSpdData), SPD_DATA_SIZE);
      COPY_POLICY ((VOID *)((FSPM_UPD *) FspmUpd)->FspmConfig.MemorySpdPtr020, (VOID *)MemConfigNoCrc->SpdData->SpdData[0][2][0], (VOID *)(UINTN)PcdGet32 (PcdMrcSpdData), SPD_DATA_SIZE);
      COPY_POLICY ((VOID *)((FSPM_UPD *) FspmUpd)->FspmConfig.MemorySpdPtr030, (VOID *)MemConfigNoCrc->SpdData->SpdData[0][3][0], (VOID *)(UINTN)PcdGet32 (PcdMrcSpdData), SPD_DATA_SIZE);
      COPY_POLICY ((VOID *)((FSPM_UPD *) FspmUpd)->FspmConfig.MemorySpdPtr100, (VOID *)MemConfigNoCrc->SpdData->SpdData[1][0][0], (VOID *)(UINTN)PcdGet32 (PcdMrcSpdData), SPD_DATA_SIZE);
      COPY_POLICY ((VOID *)((FSPM_UPD *) FspmUpd)->FspmConfig.MemorySpdPtr110, (VOID *)MemConfigNoCrc->SpdData->SpdData[1][1][0], (VOID *)(UINTN)PcdGet32 (PcdMrcSpdData), SPD_DATA_SIZE);
      COPY_POLICY ((VOID *)((FSPM_UPD *) FspmUpd)->FspmConfig.MemorySpdPtr120, (VOID *)MemConfigNoCrc->SpdData->SpdData[1][2][0], (VOID *)(UINTN)PcdGet32 (PcdMrcSpdData), SPD_DATA_SIZE);
      COPY_POLICY ((VOID *)((FSPM_UPD *) FspmUpd)->FspmConfig.MemorySpdPtr130, (VOID *)MemConfigNoCrc->SpdData->SpdData[1][3][0], (VOID *)(UINTN)PcdGet32 (PcdMrcSpdData), SPD_DATA_SIZE);
    }
  }
#endif
//[-end-210721-QINGLIN0001-modify]//

  COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.DisableMc0Ch0, MemConfig->DisableChannel[0][0], SaSetup.DisableMc0Ch0, NullIndex);
  COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.DisableMc0Ch1, MemConfig->DisableChannel[0][1], SaSetup.DisableMc0Ch1, NullIndex);
  COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.DisableMc0Ch2, MemConfig->DisableChannel[0][2], SaSetup.DisableMc0Ch2, NullIndex);
  COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.DisableMc0Ch3, MemConfig->DisableChannel[0][3], SaSetup.DisableMc0Ch3, NullIndex);
  COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.DisableMc1Ch0, MemConfig->DisableChannel[1][0], SaSetup.DisableMc1Ch0, NullIndex);
  COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.DisableMc1Ch1, MemConfig->DisableChannel[1][1], SaSetup.DisableMc1Ch1, NullIndex);
  COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.DisableMc1Ch2, MemConfig->DisableChannel[1][2], SaSetup.DisableMc1Ch2, NullIndex);
  COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.DisableMc1Ch3, MemConfig->DisableChannel[1][3], SaSetup.DisableMc1Ch3, NullIndex);
  COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.GearRatio,     MemConfig->GearRatio,            SaSetup.GearRatio,     NullIndex);

  COPY_POLICY ((VOID *)((FSPM_UPD *) FspmUpd)->FspmConfig.SaGvGear, (VOID *) MemConfig->SaGvGear, (VOID *) SaSetup.SaGvGear, (MEM_MAX_SAGV_POINTS * sizeof(SaSetup.SaGvGear[0])) );
  COPY_POLICY ((VOID *)((FSPM_UPD *) FspmUpd)->FspmConfig.SaGvFreq, (VOID *) MemConfig->SaGvFreq, (VOID *) SaSetup.SaGvFreq, (MEM_MAX_SAGV_POINTS * sizeof(SaSetup.SaGvFreq[0])) );

//[-start-200303-IB14630338-add]//
  if (PcdGetBool (PcdH2OMemoryDownSupported) == TRUE) {
//[-start-200506-IB17800062-modify]//
//[-start-201104-IB14630426-modify]//
    COPY_POLICY ((VOID *)((FSPM_UPD *) FspmUpd)->FspmConfig.MemorySpdPtr000, (VOID *)MemConfigNoCrc->SpdData->SpdData[0][0][0], PcdGetPtr (PcdH2OMemoryDownSpdDataMc0Ch0Dimm0), SPD_DATA_SIZE);
    COPY_POLICY ((VOID *)((FSPM_UPD *) FspmUpd)->FspmConfig.MemorySpdPtr010, (VOID *)MemConfigNoCrc->SpdData->SpdData[0][1][0], PcdGetPtr (PcdH2OMemoryDownSpdDataMc0Ch1Dimm0), SPD_DATA_SIZE);
    COPY_POLICY ((VOID *)((FSPM_UPD *) FspmUpd)->FspmConfig.MemorySpdPtr020, (VOID *)MemConfigNoCrc->SpdData->SpdData[0][2][0], PcdGetPtr (PcdH2OMemoryDownSpdDataMc0Ch2Dimm0), SPD_DATA_SIZE);
    COPY_POLICY ((VOID *)((FSPM_UPD *) FspmUpd)->FspmConfig.MemorySpdPtr030, (VOID *)MemConfigNoCrc->SpdData->SpdData[0][3][0], PcdGetPtr (PcdH2OMemoryDownSpdDataMc0Ch3Dimm0), SPD_DATA_SIZE);
    COPY_POLICY ((VOID *)((FSPM_UPD *) FspmUpd)->FspmConfig.MemorySpdPtr100, (VOID *)MemConfigNoCrc->SpdData->SpdData[1][0][0], PcdGetPtr (PcdH2OMemoryDownSpdDataMc1Ch0Dimm0), SPD_DATA_SIZE);
    COPY_POLICY ((VOID *)((FSPM_UPD *) FspmUpd)->FspmConfig.MemorySpdPtr110, (VOID *)MemConfigNoCrc->SpdData->SpdData[1][1][0], PcdGetPtr (PcdH2OMemoryDownSpdDataMc1Ch1Dimm0), SPD_DATA_SIZE);
    COPY_POLICY ((VOID *)((FSPM_UPD *) FspmUpd)->FspmConfig.MemorySpdPtr120, (VOID *)MemConfigNoCrc->SpdData->SpdData[1][2][0], PcdGetPtr (PcdH2OMemoryDownSpdDataMc1Ch2Dimm0), SPD_DATA_SIZE);
    COPY_POLICY ((VOID *)((FSPM_UPD *) FspmUpd)->FspmConfig.MemorySpdPtr130, (VOID *)MemConfigNoCrc->SpdData->SpdData[1][3][0], PcdGetPtr (PcdH2OMemoryDownSpdDataMc1Ch3Dimm0), SPD_DATA_SIZE);
//[-end-201104-IB14630426-modify]//

//[-end-200506-IB17800062-modify]//
    if (PcdGetBool (PcdH2OMemoryDownRcompResistorSupported)) {
//[-start-200420-IB17800056-modify]//
      UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.RcompResistor, RcompData->RcompResistor, (UINT8) PcdGet32 (PcdH2OMemoryDownRcompResistor));
//[-end-200420-IB17800056-modify]//
    }
    if (PcdGetBool (PcdH2OMemoryDownRcompTargetSupported)) {
#if FixedPcdGetBool(PcdMrcAlderLake) == 1
      COPY_POLICY ((VOID *)((FSPM_UPD *) FspmUpd)->FspmConfig.RcompTarget, (VOID *)RcompData->RcompTarget, (VOID *)(UINTN)PcdGetPtr (PcdH2OMemoryDownRcompTarget), sizeof (UINT16) * SA_MRC_MAX_RCOMP_TARGETS);
#else
      COPY_POLICY ((VOID *)((FSPM_UPD *) FspmUpd)->FspmConfig.RcompTarget, (VOID *)RcompData->RcompTarget, (VOID *)(UINTN)PcdGetPtr (PcdH2OMemoryDownRcompTarget), sizeof (UINT16) * MRC_MAX_RCOMP_TARGETS);
#endif
    }
    if (PcdGetBool (PcdH2OMemoryDownDqPinsInterleavedSupported)) {
      UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.DqPinsInterleaved, MemConfig->DqPinsInterleaved, PcdGetBool (PcdH2OMemoryDownDqPinsInterleaved));
    }
//[-start-200420-IB17800056-modify]//
    if (PcdGetBool (PcdH2OMemoryDownDqByteMapSupported)) {
      COPY_POLICY ((VOID *)((FSPM_UPD *) FspmUpd)->FspmConfig.DqMapCpu2DramMc0Ch0, (VOID *)MemConfigNoCrc->DqDqsMap->DqMapCpu2Dram, (VOID *) (UINTN) PcdGetPtr (PcdH2OMemoryDownDqByteMap), sizeof (UINT8) * MEM_CFG_MAX_CONTROLLERS * MEM_CFG_MAX_CHANNELS * MEM_CFG_NUM_BYTES_MAPPED * 8);
    }
    if (PcdGetBool (PcdH2OMemoryDownDqsMapCpu2DramSupported)) {
      COPY_POLICY ((VOID *)((FSPM_UPD *) FspmUpd)->FspmConfig.DqsMapCpu2DramMc0Ch0, (VOID *)MemConfigNoCrc->DqDqsMap->DqsMapCpu2Dram, (VOID *)(UINTN) PcdGetPtr (PcdH2OMemoryDownDqsMapCpu2Dram), sizeof (UINT8)* MEM_CFG_MAX_CONTROLLERS * MEM_CFG_MAX_CHANNELS * MEM_CFG_NUM_BYTES_MAPPED);
    }
//[-end-200420-IB17800056-modify]//
  }
//[-end-200303-IB14630338-add]//

  if (!EFI_ERROR(Status2)) {
    //
    // Get the Platform Configuration from SetupData
    //
#if FixedPcdGet8(PcdFspModeSelection) == 0
    HostBridgePreMemConfig->MchBar   = (UINTN) PcdGet64 (PcdMchBaseAddress);
    HostBridgePreMemConfig->DmiBar   = (UINTN) PcdGet64 (PcdDmiBaseAddress);
    HostBridgePreMemConfig->EpBar    = (UINTN) PcdGet64 (PcdEpBaseAddress);
    HostBridgePreMemConfig->EdramBar = (UINTN) PcdGet64 (PcdEdramBaseAddress);
    MiscPeiPreMemConfig->SmbusBar = (UINTN) PcdGet16 (PcdSmbusBaseAddress);
#endif
    UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.TsegSize,           MiscPeiPreMemConfig->TsegSize,           PcdGet32 (PcdTsegSize));
    UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.UserBd,             MiscPeiPreMemConfig->UserBd,             PcdGet8 (PcdSaMiscUserBd));
    UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.MmioSizeAdjustment, HostBridgePreMemConfig->MmioSizeAdjustment, PcdGet16 (PcdSaMiscMmioSizeAdjustment));
    COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.EnableAbove4GBMmio, HostBridgePreMemConfig->EnableAbove4GBMmio, SaSetup.EnableAbove4GBMmio, NullIndex);
    //
    // Initialize the Graphics configuration
    //
    COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.IgdDvmt50PreAlloc, GtPreMemConfig->IgdDvmt50PreAlloc, SaSetup.IgdDvmt50PreAlloc, NullIndex);
    COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.InternalGfx,       GtPreMemConfig->InternalGraphics,  SaSetup.InternalGraphics,  NullIndex);
    COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.PrimaryDisplay,    GtPreMemConfig->PrimaryDisplay,    SaSetup.PrimaryDisplay,    NullIndex);
    COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.ApertureSize,      GtPreMemConfig->ApertureSize,      SaSetup.ApertureSize,      NullIndex);
    COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.GttSize,           GtPreMemConfig->GttSize,           SaSetup.GTTSize,           NullIndex);
    UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.GmAdr64,                       GtPreMemConfig->GmAdr64,           (UINTN)PcdGet64(PcdGmAdrAddress));

    UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.GttMmAdr,                     GtPreMemConfig->GttMmAdr,         (UINTN)PcdGet64(PcdGttMmAddress));
    COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.PsmiRegionSize,   GtPreMemConfig->PsmiRegionSize,   SaSetup.GtPsmiRegionSize, NullIndex);
    COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.GtClosEnable,     GtPreMemConfig->GtClosEnable,     SaSetup.GtClosEnable,     NullIndex);
    COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.GtPsmiSupport,    GtPreMemConfig->GtPsmiSupport,    SaSetup.GtPsmiSupport,    NullIndex);
    UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.DeltaT12PowerCycleDelay,      GtPreMemConfig->DeltaT12PowerCycleDelay,       0x0);
    COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.DisplayAudioLink, GtPreMemConfig->DisplayAudioLink, SaSetup.DisplayAudioLink, NullIndex);
    UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.OemT12DelayOverride,          GtPreMemConfig->OemT12DelayOverride,       0x0);

    //
    // Display DDI Initialization ( default Native GPIO as per board during AUTO case)
    //
    CopyMem (SaDisplayConfigTable, (VOID *) (UINTN) PcdGet32 (PcdSaDisplayConfigTable), (UINTN)PcdGet16 (PcdSaDisplayConfigTableSize));
    UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.DdiPortAConfig,          GtPreMemConfig->DdiConfiguration.DdiPortAConfig,       SaDisplayConfigTable[0]);
    UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.DdiPortBConfig,          GtPreMemConfig->DdiConfiguration.DdiPortBConfig,       SaDisplayConfigTable[1]);
    UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.DdiPortAHpd,             GtPreMemConfig->DdiConfiguration.DdiPortAHpd,          SaDisplayConfigTable[2]);
    UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.DdiPortBHpd,             GtPreMemConfig->DdiConfiguration.DdiPortBHpd,          SaDisplayConfigTable[3]);
    UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.DdiPortCHpd,             GtPreMemConfig->DdiConfiguration.DdiPortCHpd,          SaDisplayConfigTable[4]);
    UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.DdiPort1Hpd,             GtPreMemConfig->DdiConfiguration.DdiPort1Hpd,          SaDisplayConfigTable[5]);
    UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.DdiPort2Hpd,             GtPreMemConfig->DdiConfiguration.DdiPort2Hpd,          SaDisplayConfigTable[6]);
    UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.DdiPort3Hpd,             GtPreMemConfig->DdiConfiguration.DdiPort3Hpd,          SaDisplayConfigTable[7]);
    UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.DdiPort4Hpd,             GtPreMemConfig->DdiConfiguration.DdiPort4Hpd,          SaDisplayConfigTable[8]);
    UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.DdiPortADdc,             GtPreMemConfig->DdiConfiguration.DdiPortADdc,          SaDisplayConfigTable[9]);
    UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.DdiPortBDdc,             GtPreMemConfig->DdiConfiguration.DdiPortBDdc,          SaDisplayConfigTable[10]);
    UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.DdiPortCDdc,             GtPreMemConfig->DdiConfiguration.DdiPortCDdc,          SaDisplayConfigTable[11]);
    UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.DdiPort1Ddc,             GtPreMemConfig->DdiConfiguration.DdiPort1Ddc,          SaDisplayConfigTable[12]);
    UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.DdiPort2Ddc,             GtPreMemConfig->DdiConfiguration.DdiPort2Ddc,          SaDisplayConfigTable[13]);
    UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.DdiPort3Ddc,             GtPreMemConfig->DdiConfiguration.DdiPort3Ddc,          SaDisplayConfigTable[14]);
    UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.DdiPort4Ddc,             GtPreMemConfig->DdiConfiguration.DdiPort4Ddc,          SaDisplayConfigTable[15]);


    COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.DmiDeEmphasis,          CpuDmiPreMemConfig->DmiDeEmphasis,           SaSetup.DmiDeEmphasis,          NullIndex);
#if FixedPcdGetBool(PcdCpuPcieEnable) == 1
    COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.InitPcieAspmAfterOprom, PciePeiPreMemConfig->InitPcieAspmAfterOprom, SaSetup.InitAspmAfterOprom,     NullIndex);
#endif
    COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.DmiGen3ProgramStaticEq, CpuDmiPreMemConfig->DmiGen3ProgramStaticEq,  SaSetup.DmiGen3ProgramStaticEq, NullIndex);
    COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.DmiAspm,                CpuDmiPreMemConfig->DmiAspm,                 SaSetup.DmiAspm,                NullIndex);
    COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.DmiAspmCtrl,            CpuDmiPreMemConfig->DmiAspmCtrl,             SaSetup.DmiAspmCtrl,            NullIndex);
    COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.DmiAspmL1ExitLatency,   CpuDmiPreMemConfig->DmiAspmL1ExitLatency,    SaSetup.DmiAspmL1ExitLatency,   NullIndex);
    COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.DmiNewFom,              CpuDmiPreMemConfig->DmiNewFom,               SaSetup.DmiNewFom,              NullIndex);
    COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.DmiHweq,                CpuDmiPreMemConfig->DmiHweq,                 SetupData.DmiHweq,              NullIndex);
    for (Lane = 0; Lane < SA_DMI_MAX_LANE; Lane++) {
      COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.DmiGen3RootPortPreset[Lane], CpuDmiPreMemConfig->DmiGen3RootPortPreset[Lane], SaSetup.DmiGen3RootPortPreset[Lane], Lane);
      COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.DmiGen3EndPointPreset[Lane], CpuDmiPreMemConfig->DmiGen3EndPointPreset[Lane], SaSetup.DmiGen3EndPointPreset[Lane], Lane);
      COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.DmiGen3EndPointHint[Lane],   CpuDmiPreMemConfig->DmiGen3EndPointHint[Lane],   SaSetup.DmiGen3EndPointHint[Lane],   Lane);
      COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.CpuDmiHwEqGen4CoeffListCm[Lane], CpuDmiPreMemConfig->DmiHwEqGen4CoeffList[Lane].Cp, SetupData.DmiGen4RtcoCpre[Lane],       Lane);
      COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.CpuDmiHwEqGen4CoeffListCp[Lane], CpuDmiPreMemConfig->DmiHwEqGen4CoeffList[Lane].Cm, SetupData.DmiGen4RtcoCpo[Lane],        Lane);
    }
    for (Bundle = 0; Bundle < SA_DMI_MAX_BUNDLE; Bundle++) {
      COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.DmiGen3RxCtlePeaking[Bundle], CpuDmiPreMemConfig->DmiGen3RxCtlePeaking[Bundle], SaSetup.DmiGen3RxCtlePeaking[Bundle], Bundle);
    }

    //
    // External Graphics card scan option.
    //
    COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.SkipExtGfxScan,   MiscPeiPreMemConfig->SkipExtGfxScan,   SaSetup.SkipExtGfxScan,   NullIndex);

    COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.WrcFeatureEnable, MiscPeiPreMemConfig->WrcFeatureEnable, SaSetup.WrcFeatureEnable, NullIndex);

#if FixedPcdGetBool(PcdHgEnable) == 1
    //
    // SaRtd3Pcie GPIO configuration
    //
//[-start-191125-IB15410319-modify]//
    if (SaSetup.PrimaryDisplay == 4 || SaSetup.PrimaryDisplay == 1) {
//[-end-191125-IB15410319-modify]//
//[-start-210127-IB05660155-add]//
//[-start-210428-IB05660158-modify]//
      if ((FixedPcdGetBool (PcdUseCrbHgDefaultSettings) == 1) && (PcdGet16 (PcdCrbSkuId) == 1)) {
//[-start-210318-IB05660157-modify]//
        if (ChipsetConfiguration.HgSlot == PCH) {
//[-end-210318-IB05660157-modify]//
          PcdSet8S (PcdPcie0GpioSupport, 1);
          PcdSet8S (PcdRootPortIndex, 4);
          PcdSet8S (PcdPcie0PwrEnableExpanderNo, 0);
          PcdSet32S (PcdPcie0PwrEnableGpioNo, GPIO_VER4_S_GPP_B21);
          PcdSetBoolS (PcdPcie0PwrEnableActive, 1);
          PcdSet8S (PcdPcie0HoldRstExpanderNo, 0);
          PcdSet32S (PcdPcie0HoldRstGpioNo, GPIO_VER4_S_GPP_F13);
          PcdSetBoolS (PcdPcie0HoldRstActive, 0);
//[-start-210318-IB05660157-modify]//
        } else if (ChipsetConfiguration.HgSlot == PEG) {
//[-end-210318-IB05660157-modify]//
          PcdSet8S (PcdPcie0GpioSupport, 1);
        }
      } else if ((FixedPcdGetBool (PcdUseCrbHgDefaultSettings) == 1) && (PcdGet16 (PcdCrbSkuId) == 2)) {
        if (ChipsetConfiguration.HgSlot == PCH) {
          PcdSet8S (PcdPcie0GpioSupport, 1);
          PcdSet8S (PcdRootPortIndex, 7);
        } else if (ChipsetConfiguration.HgSlot == PEG) {
          PcdSet8S (PcdPcie0GpioSupport, 1);
        }
      }
//[-end-210428-IB05660158-modify]//
//[-end-210127-IB05660155-add]//
      UPDATE_POLICY (*(UINT8  *)&((FSPM_UPD *) FspmUpd)->FspmConfig.RootPortIndex,        HgGpioData->RootPortIndex,                         PcdGet8 (PcdRootPortIndex));
      UPDATE_POLICY (*(UINT8  *)&((FSPM_UPD *) FspmUpd)->FspmConfig.HgSupport,            HgGpioData->HgSupport,                             SaSetup.HgSupport);
      UPDATE_POLICY (*(UINT8  *)&((FSPM_UPD *) FspmUpd)->FspmConfig.CpuPcie0Rtd3Gpio[0],  HgGpioData->CpuPcie0Rtd3Gpio.HoldRst.ExpanderNo,   PcdGet8 (PcdPcie0HoldRstExpanderNo));
      UPDATE_POLICY (*(UINT8  *)&((FSPM_UPD *) FspmUpd)->FspmConfig.CpuPcie0Rtd3Gpio[1],  HgGpioData->CpuPcie0Rtd3Gpio.HoldRst.Active,       PcdGetBool (PcdPcie0HoldRstActive));
      UPDATE_POLICY (*(UINT32 *)&((FSPM_UPD *) FspmUpd)->FspmConfig.CpuPcie0Rtd3Gpio[4],  HgGpioData->CpuPcie0Rtd3Gpio.HoldRst.GpioNo,       PcdGet32 (PcdPcie0HoldRstGpioNo));
      UPDATE_POLICY (*(UINT8  *)&((FSPM_UPD *) FspmUpd)->FspmConfig.CpuPcie0Rtd3Gpio[8],  HgGpioData->CpuPcie0Rtd3Gpio.PwrEnable.ExpanderNo, PcdGet8 (PcdPcie0PwrEnableExpanderNo));
      UPDATE_POLICY (*(UINT8  *)&((FSPM_UPD *) FspmUpd)->FspmConfig.CpuPcie0Rtd3Gpio[9],  HgGpioData->CpuPcie0Rtd3Gpio.PwrEnable.Active,     PcdGetBool (PcdPcie0PwrEnableActive));
      UPDATE_POLICY (*(UINT32 *)&((FSPM_UPD *) FspmUpd)->FspmConfig.CpuPcie0Rtd3Gpio[12], HgGpioData->CpuPcie0Rtd3Gpio.PwrEnable.GpioNo,     PcdGet32 (PcdPcie0PwrEnableGpioNo));
      UPDATE_POLICY (*(UINT8  *)&((FSPM_UPD *) FspmUpd)->FspmConfig.CpuPcie0Rtd3Gpio[20], HgGpioData->CpuPcie0Rtd3Gpio.GpioSupport,          PcdGet8 (PcdPcie0GpioSupport));

      UPDATE_POLICY (*(UINT8  *)&((FSPM_UPD *) FspmUpd)->FspmConfig.CpuPcie1Rtd3Gpio[0],  HgGpioData->CpuPcie1Rtd3Gpio.HoldRst.ExpanderNo,   PcdGet8(PcdPcie1HoldRstExpanderNo));
      UPDATE_POLICY (*(UINT8  *)&((FSPM_UPD *) FspmUpd)->FspmConfig.CpuPcie1Rtd3Gpio[1],  HgGpioData->CpuPcie1Rtd3Gpio.HoldRst.Active,       PcdGetBool(PcdPcie1HoldRstActive));
      UPDATE_POLICY (*(UINT32 *)&((FSPM_UPD *) FspmUpd)->FspmConfig.CpuPcie1Rtd3Gpio[4],  HgGpioData->CpuPcie1Rtd3Gpio.HoldRst.GpioNo,       PcdGet32(PcdPcie1HoldRstGpioNo));
      UPDATE_POLICY (*(UINT8  *)&((FSPM_UPD *) FspmUpd)->FspmConfig.CpuPcie1Rtd3Gpio[8],  HgGpioData->CpuPcie1Rtd3Gpio.PwrEnable.ExpanderNo, PcdGet8(PcdPcie1PwrEnableExpanderNo));
      UPDATE_POLICY (*(UINT8  *)&((FSPM_UPD *) FspmUpd)->FspmConfig.CpuPcie1Rtd3Gpio[9],  HgGpioData->CpuPcie1Rtd3Gpio.PwrEnable.Active,     PcdGetBool(PcdPcie1PwrEnableActive));
      UPDATE_POLICY (*(UINT32 *)&((FSPM_UPD *) FspmUpd)->FspmConfig.CpuPcie1Rtd3Gpio[12], HgGpioData->CpuPcie1Rtd3Gpio.PwrEnable.GpioNo,     PcdGet32(PcdPcie1PwrEnableGpioNo));
      UPDATE_POLICY (*(UINT8  *)&((FSPM_UPD *) FspmUpd)->FspmConfig.CpuPcie1Rtd3Gpio[20], HgGpioData->CpuPcie1Rtd3Gpio.GpioSupport,          PcdGet8(PcdPcie1GpioSupport));

      UPDATE_POLICY (*(UINT8  *)&((FSPM_UPD *) FspmUpd)->FspmConfig.CpuPcie2Rtd3Gpio[0],  HgGpioData->CpuPcie2Rtd3Gpio.HoldRst.ExpanderNo,   PcdGet8(PcdPcie2HoldRstExpanderNo));
      UPDATE_POLICY (*(UINT8  *)&((FSPM_UPD *) FspmUpd)->FspmConfig.CpuPcie2Rtd3Gpio[1],  HgGpioData->CpuPcie2Rtd3Gpio.HoldRst.Active,       PcdGetBool(PcdPcie2HoldRstActive));
      UPDATE_POLICY (*(UINT32 *)&((FSPM_UPD *) FspmUpd)->FspmConfig.CpuPcie2Rtd3Gpio[4],  HgGpioData->CpuPcie2Rtd3Gpio.HoldRst.GpioNo,       PcdGet32(PcdPcie2HoldRstGpioNo));
      UPDATE_POLICY (*(UINT8  *)&((FSPM_UPD *) FspmUpd)->FspmConfig.CpuPcie2Rtd3Gpio[8],  HgGpioData->CpuPcie2Rtd3Gpio.PwrEnable.ExpanderNo, PcdGet8(PcdPcie2PwrEnableExpanderNo));
      UPDATE_POLICY (*(UINT8  *)&((FSPM_UPD *) FspmUpd)->FspmConfig.CpuPcie2Rtd3Gpio[9],  HgGpioData->CpuPcie2Rtd3Gpio.PwrEnable.Active,     PcdGetBool(PcdPcie2PwrEnableActive));
      UPDATE_POLICY (*(UINT32 *)&((FSPM_UPD *) FspmUpd)->FspmConfig.CpuPcie2Rtd3Gpio[12], HgGpioData->CpuPcie2Rtd3Gpio.PwrEnable.GpioNo,     PcdGet32(PcdPcie2PwrEnableGpioNo));
      UPDATE_POLICY (*(UINT8  *)&((FSPM_UPD *) FspmUpd)->FspmConfig.CpuPcie2Rtd3Gpio[20], HgGpioData->CpuPcie2Rtd3Gpio.GpioSupport,          PcdGet8(PcdPcie2GpioSupport));

      UPDATE_POLICY (*(UINT8  *)&((FSPM_UPD *) FspmUpd)->FspmConfig.CpuPcie3Rtd3Gpio[0],  HgGpioData->CpuPcie3Rtd3Gpio.HoldRst.ExpanderNo,   PcdGet8(PcdPcie3HoldRstExpanderNo));
      UPDATE_POLICY (*(UINT8  *)&((FSPM_UPD *) FspmUpd)->FspmConfig.CpuPcie3Rtd3Gpio[1],  HgGpioData->CpuPcie3Rtd3Gpio.HoldRst.Active,       PcdGetBool(PcdPcie3HoldRstActive));
      UPDATE_POLICY (*(UINT32 *)&((FSPM_UPD *) FspmUpd)->FspmConfig.CpuPcie3Rtd3Gpio[4],  HgGpioData->CpuPcie3Rtd3Gpio.HoldRst.GpioNo,       PcdGet32(PcdPcie3HoldRstGpioNo));
      UPDATE_POLICY (*(UINT8  *)&((FSPM_UPD *) FspmUpd)->FspmConfig.CpuPcie3Rtd3Gpio[8],  HgGpioData->CpuPcie3Rtd3Gpio.PwrEnable.ExpanderNo, PcdGet8(PcdPcie3PwrEnableExpanderNo));
      UPDATE_POLICY (*(UINT8  *)&((FSPM_UPD *) FspmUpd)->FspmConfig.CpuPcie3Rtd3Gpio[9],  HgGpioData->CpuPcie3Rtd3Gpio.PwrEnable.Active,     PcdGetBool(PcdPcie3PwrEnableActive));
      UPDATE_POLICY (*(UINT32 *)&((FSPM_UPD *) FspmUpd)->FspmConfig.CpuPcie3Rtd3Gpio[12], HgGpioData->CpuPcie3Rtd3Gpio.PwrEnable.GpioNo,     PcdGet32(PcdPcie3PwrEnableGpioNo));
      UPDATE_POLICY (*(UINT8  *)&((FSPM_UPD *) FspmUpd)->FspmConfig.CpuPcie3Rtd3Gpio[20], HgGpioData->CpuPcie3Rtd3Gpio.GpioSupport,          PcdGet8(PcdPcie3GpioSupport));

//[-start-181217-IB15410210-add]//
//[-start-190612-IB16990050-modify]//
//[-start-201113-IB09480112-modify]//
    //
    // Replace CRB settings with Insyde PCD settings.
    //
    if ((FeaturePcdGet (PcdHybridGraphicsSupported) == 1) && (FixedPcdGetBool (PcdUseCrbHgDefaultSettings) == 0)) {
      UPDATE_POLICY (*(UINT8  *)&((FSPM_UPD *) FspmUpd)->FspmConfig.RootPortIndex,        HgGpioData->RootPortIndex,                     FixedPcdGet8 (PcdHgPcieRootPortIndex));
      UPDATE_POLICY (*(UINT8  *)&((FSPM_UPD *) FspmUpd)->FspmConfig.CpuPcie0Rtd3Gpio[1],  HgGpioData->CpuPcie0Rtd3Gpio.HoldRst.Active,   FixedPcdGetBool (PcdHgDgpuHoldRstActive));
      UPDATE_POLICY (*(UINT32 *)&((FSPM_UPD *) FspmUpd)->FspmConfig.CpuPcie0Rtd3Gpio[4],  HgGpioData->CpuPcie0Rtd3Gpio.HoldRst.GpioNo,   FixedPcdGet32 (PcdHgDgpuHoldRstGpioNo));
      UPDATE_POLICY (*(UINT8  *)&((FSPM_UPD *) FspmUpd)->FspmConfig.CpuPcie0Rtd3Gpio[9],  HgGpioData->CpuPcie0Rtd3Gpio.PwrEnable.Active, FixedPcdGetBool (PcdHgDgpuPwrEnableActive));
      UPDATE_POLICY (*(UINT32 *)&((FSPM_UPD *) FspmUpd)->FspmConfig.CpuPcie0Rtd3Gpio[12], HgGpioData->CpuPcie0Rtd3Gpio.PwrEnable.GpioNo, FixedPcdGet32 (PcdHgDgpuPwrEnableGpioNo));
      UPDATE_POLICY (*(UINT32 *)&((FSPM_UPD *) FspmUpd)->FspmConfig.CpuPcie0Rtd3Gpio[16], HgGpioData->CpuPcie0Rtd3Gpio.WakeGpioNo,       FixedPcdGet32 (PcdHgDgpuWakeGpioNo));
      UPDATE_POLICY (*(UINT8  *)&((FSPM_UPD *) FspmUpd)->FspmConfig.CpuPcie0Rtd3Gpio[20], HgGpioData->CpuPcie0Rtd3Gpio.GpioSupport,      FixedPcdGetBool (PcdHgDgpuGpioSupport));

    }
//[-end-201113-IB09480112-modify]//
//[-end-190612-IB16990050-modify]//
//[-end-181217-IB15410210-add]//
    }

    ///
    /// For Elk-Creek card, invert the Power enable signal
    ///
    if (SaSetup.PcieCardSelect == 0) {
      UPDATE_POLICY (*(UINT8  *)&((FSPM_UPD *) FspmUpd)->FspmConfig.CpuPcie0Rtd3Gpio[9],  HgGpioData->CpuPcie0Rtd3Gpio.PwrEnable.Active,     PcdGetBool (PcdPcie0PwrEnableActive));
    }
    //
    // Initialize the Hybrid Graphics Configuration
    //
    if (SaSetup.PrimaryDisplay == 4) {
      ///
      /// In Hybrid Gfx mode PCIe needs to be always enabled and IGFX must be set as Primary Display.
      ///
#if FixedPcdGet8(PcdFspModeSelection) == 0
      HgGpioData->HgMode = 2;  //HgModeMuxless
      HgGpioData->HgSubSystemId = 0x2112;
      GtPreMemConfig->PrimaryDisplay = 0;
      GtPreMemConfig->InternalGraphics = 1;
#endif
    } else if ((SaSetup.PrimaryDisplay == 1) || (SaSetup.PrimaryDisplay == 2) || (SaSetup.PrimaryDisplay == 3)) {
      ///
      /// In PEG or PCI or Auto mode set Hybrid Gfx mode as dGPU
      ///
#if FixedPcdGet8(PcdFspModeSelection) == 0
      HgGpioData->HgMode = 3;  //HgModeDgpu
      HgGpioData->HgSubSystemId = 0x2212;
#endif
    } else if (SaSetup.PrimaryDisplay == 0) {
      ///
      /// In IGFX only mode mode set Hybrid Gfx mode as Disabled
      ///
#if FixedPcdGet8(PcdFspModeSelection) == 0
      HgGpioData->HgMode = 0;  //HgModeDisabled
      HgGpioData->HgSubSystemId = 0x2212;
#endif
    }
//[-start-180808-IB15410175-add]//
    //
    // Base on HG related System Configuration variables to change SA MCH policy.
    //
//[-start-190612-IB16990050-modify]//
    //
    // It can not add HG mode in reference code,
    // so need use setup configuration setting to make up the same action whit HG.
    //
    if (SaSetup.PrimaryDisplay == DisplayModeHg) {
      if (SaSetup.InternalGraphics == IgdDisable) {
        //
        // Set to PEG mode when HG mode to disable IGD.
        //
        UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.PrimaryDisplay, GtPreMemConfig->PrimaryDisplay, DisplayModeDgpu);
      }
    }
    //
    // If project's discrete GPU after power enable sequence still can't be powered on successfully,
    // "always enable PEG of PCIE configuration" policy need be set to always enable in SCU.
    //

//[-end-190612-IB16990050-modify]//
//[-end-180808-IB15410175-add]//
    COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.HgDelayAfterPwrEn,     HgGpioData->HgDelayAfterPwrEn,     SaSetup.DelayAfterPwrEn,     NullIndex);
    COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.HgDelayAfterHoldReset, HgGpioData->HgDelayAfterHoldReset, SaSetup.DelayAfterHoldReset, NullIndex);
#endif

    ///
    /// Initialize the VTD Configuration
    ///
    if (SaSetup.EnableVtd == 0 || (SaSetup.VtdIgdEnable == 0 && SaSetup.VtdIpuEnable == 0 && SaSetup.VtdIopEnable == 0)) {
      UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.VtdDisable,   Vtd->VtdDisable,   1);
      UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.X2ApicOptOut, Vtd->X2ApicOptOut, 1);
      for (Index = 0; Index < GetMaxVtdEngineNumber(); Index++) {
        UPDATE_POLICY(((FSPM_UPD *)FspmUpd)->FspmConfig.VtdBaseAddress[Index], Vtd->BaseAddress[Index], 0);
      }
      UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.PreBootDmaMask, Vtd->PreBootDmaMask, 0);
      UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.DmaBufferSize, Vtd->DmaBufferSize, 0);
    } else {
      UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.VtdDisable,   Vtd->VtdDisable,   0);
      UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.X2ApicOptOut, Vtd->X2ApicOptOut, SaSetup.X2ApicOptOut);
      for (Index = 0; Index < GetMaxVtdEngineNumber(); Index++) {
        UPDATE_POLICY(((FSPM_UPD *)FspmUpd)->FspmConfig.VtdBaseAddress[Index], Vtd->BaseAddress[Index], GetVtdBaseAddress (Index));
      }
      if ((SetupData.ControlIommu) != 0) {
        UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.PreBootDmaMask, Vtd->PreBootDmaMask, 1);
      } else {
        UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.PreBootDmaMask, Vtd->PreBootDmaMask, 0);
      }

      PeiServicesGetBootMode (&SysBootMode);
      if (SysBootMode == BOOT_ON_S3_RESUME) {
        UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.DmaBufferSize, Vtd->DmaBufferSize, PcdGet32 (PcdVTdPeiDmaBufferSizeS3));
      } else {
        UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.DmaBufferSize, Vtd->DmaBufferSize, PcdGet32 (PcdVTdPeiDmaBufferSize));
      }

      PlatformVtdEnableDmaBuffer = 0;
      Status = PeiServicesLocatePpi (
                 &gPlatformVTdEnableDmaBufferPpiGuid,
                 0,
                 NULL,
                 (VOID **) &PlatformVtdEnableDmaBuffer
                 );
      COPY_POLICY (&(((FSPM_UPD *) FspmUpd)->FspmConfig.EnableDmaBuffer), &Vtd->EnableDmaBuffer, &PlatformVtdEnableDmaBuffer, sizeof (EFI_PHYSICAL_ADDRESS));
    }
    COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.DmaControlGuarantee, Vtd->DmaControlGuarantee, SaSetup.DmaControlGuarantee, NullIndex);
    COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.VtdIgdEnable,        Vtd->VtdIgdEnable,        SaSetup.VtdIgdEnable,        NullIndex);
    COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.VtdIpuEnable,        Vtd->VtdIpuEnable,        SaSetup.VtdIpuEnable,        NullIndex);
    COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.VtdIopEnable,        Vtd->VtdIopEnable,        SaSetup.VtdIopEnable,        NullIndex);

#if FixedPcdGetBool(PcdIpuEnable) == 1
    //
    // Initialize IPU PreMem Configuration
    //
    COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.SaIpuEnable, IpuPreMemPolicy->IpuEnable, SaSetup.SaIpuEnable, NullIndex);

    if (SetupData.MipiCam_Link0 == 1) {
      COMPARE_AND_UPDATE_POLICY (
                        ((FSPM_UPD *) FspmUpd)->FspmConfig.IpuLaneUsed[SetupData.MipiCam_Link0_DriverData_LinkUsed],\
                        IpuPreMemPolicy->LaneUsed[SetupData.MipiCam_Link0_DriverData_LinkUsed],\
                        SetupData.MipiCam_Link0_DriverData_LaneUsed,\
                        SetupData.MipiCam_Link0_DriverData_LinkUsed);
      COMPARE_AND_UPDATE_POLICY (
                        ((FSPM_UPD *) FspmUpd)->FspmConfig.CsiSpeed[SetupData.MipiCam_Link0_DriverData_LinkUsed],\
                        IpuPreMemPolicy->CsiSpeed[SetupData.MipiCam_Link0_DriverData_LinkUsed],\
                        SetupData.MipiCam_Link0_DriverData_CsiSpeed,\
                        SetupData.MipiCam_Link0_DriverData_LinkUsed);
    }
    if (SetupData.MipiCam_Link1 == 1) {
      COMPARE_AND_UPDATE_POLICY (
                        ((FSPM_UPD *) FspmUpd)->FspmConfig.IpuLaneUsed[SetupData.MipiCam_Link1_DriverData_LinkUsed],\
                        IpuPreMemPolicy->LaneUsed[SetupData.MipiCam_Link1_DriverData_LinkUsed],\
                        SetupData.MipiCam_Link1_DriverData_LaneUsed,\
                        SetupData.MipiCam_Link1_DriverData_LinkUsed);
      COMPARE_AND_UPDATE_POLICY (\
                        ((FSPM_UPD *) FspmUpd)->FspmConfig.CsiSpeed[SetupData.MipiCam_Link1_DriverData_LinkUsed],\
                        IpuPreMemPolicy->CsiSpeed[SetupData.MipiCam_Link1_DriverData_LinkUsed],\
                        SetupData.MipiCam_Link1_DriverData_CsiSpeed,\
                        SetupData.MipiCam_Link1_DriverData_LinkUsed);
    }
    if (SetupData.MipiCam_Link2 == 1) {
      COMPARE_AND_UPDATE_POLICY (
                        ((FSPM_UPD *) FspmUpd)->FspmConfig.IpuLaneUsed[SetupData.MipiCam_Link2_DriverData_LinkUsed],\
                        IpuPreMemPolicy->LaneUsed[SetupData.MipiCam_Link2_DriverData_LinkUsed],\
                        SetupData.MipiCam_Link2_DriverData_LaneUsed,\
                        SetupData.MipiCam_Link2_DriverData_LinkUsed);
      COMPARE_AND_UPDATE_POLICY (
                        ((FSPM_UPD *) FspmUpd)->FspmConfig.CsiSpeed[SetupData.MipiCam_Link2_DriverData_LinkUsed],\
                        IpuPreMemPolicy->CsiSpeed[SetupData.MipiCam_Link2_DriverData_LinkUsed],\
                        SetupData.MipiCam_Link2_DriverData_CsiSpeed,\
                        SetupData.MipiCam_Link2_DriverData_LinkUsed);
    }
    if (SetupData.MipiCam_Link3 == 1) {
      COMPARE_AND_UPDATE_POLICY (
                        ((FSPM_UPD *) FspmUpd)->FspmConfig.IpuLaneUsed[SetupData.MipiCam_Link3_DriverData_LinkUsed],\
                        IpuPreMemPolicy->LaneUsed[SetupData.MipiCam_Link3_DriverData_LinkUsed],\
                        SetupData.MipiCam_Link3_DriverData_LaneUsed,\
                        SetupData.MipiCam_Link3_DriverData_LinkUsed);
      COMPARE_AND_UPDATE_POLICY (
                        ((FSPM_UPD *) FspmUpd)->FspmConfig.CsiSpeed[SetupData.MipiCam_Link3_DriverData_LinkUsed],\
                        IpuPreMemPolicy->CsiSpeed[SetupData.MipiCam_Link3_DriverData_LinkUsed],\
                        SetupData.MipiCam_Link3_DriverData_CsiSpeed,\
                        SetupData.MipiCam_Link3_DriverData_LinkUsed);
    }
    if (SetupData.MipiCam_Link4 == 1) {
      COMPARE_AND_UPDATE_POLICY (
                        ((FSPM_UPD *) FspmUpd)->FspmConfig.IpuLaneUsed[SetupData.MipiCam_Link4_DriverData_LinkUsed],\
                        IpuPreMemPolicy->LaneUsed[SetupData.MipiCam_Link4_DriverData_LinkUsed],\
                        SetupData.MipiCam_Link4_DriverData_LaneUsed,\
                        SetupData.MipiCam_Link4_DriverData_LinkUsed);
      COMPARE_AND_UPDATE_POLICY (
                        ((FSPM_UPD *) FspmUpd)->FspmConfig.CsiSpeed[SetupData.MipiCam_Link4_DriverData_LinkUsed],\
                        IpuPreMemPolicy->CsiSpeed[SetupData.MipiCam_Link4_DriverData_LinkUsed],\
                        SetupData.MipiCam_Link4_DriverData_CsiSpeed,\
                        SetupData.MipiCam_Link4_DriverData_LinkUsed);
    }
    if (SetupData.MipiCam_Link5 == 1) {
      COMPARE_AND_UPDATE_POLICY (
                        ((FSPM_UPD *) FspmUpd)->FspmConfig.IpuLaneUsed[SetupData.MipiCam_Link5_DriverData_LinkUsed],\
                        IpuPreMemPolicy->LaneUsed[SetupData.MipiCam_Link5_DriverData_LinkUsed],\
                        SetupData.MipiCam_Link5_DriverData_LaneUsed,\
                        SetupData.MipiCam_Link5_DriverData_LinkUsed);
      COMPARE_AND_UPDATE_POLICY (
                        ((FSPM_UPD *) FspmUpd)->FspmConfig.CsiSpeed[SetupData.MipiCam_Link5_DriverData_LinkUsed],\
                        IpuPreMemPolicy->CsiSpeed[SetupData.MipiCam_Link5_DriverData_LinkUsed],\
                        SetupData.MipiCam_Link5_DriverData_CsiSpeed,\
                        SetupData.MipiCam_Link5_DriverData_LinkUsed);
    }

#endif
    COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.HobBufferSize, MemConfig->HobBufferSize, SaSetup.HobBufferSize, NullIndex);
    COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.EccSupport,    MemConfig->EccSupport,    SaSetup.EccSupport,    NullIndex);
    COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.SaGv,          MemConfig->SaGv,          SaSetup.SaGv,          NullIndex);
    COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.EpgEnable,     MemConfig->EpgEnable,     SaSetup.EpgEnable,     NullIndex);
    COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.Idd3n,         MemConfig->Idd3n,         SaSetup.Idd3n,         NullIndex);
    COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.Idd3p,         MemConfig->Idd3p,         SaSetup.Idd3p,         NullIndex);
    COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.EccDftEn,      MemConfig->EccDftEn,      SaSetup.EccDftEn,      NullIndex);
    COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.Write0,        MemConfig->Write0,        SaSetup.Write0,        NullIndex);
    COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.PeriodicDcc,   MemConfig->PeriodicDcc,   SaSetup.PeriodicDcc,   NullIndex);
    COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.LpMode,        MemConfig->LpMode,        SaSetup.LpMode,        NullIndex);
    COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.FreqLimitMixedConfig, MemConfig->FreqLimitMixedConfig, SaSetup.FreqLimitMixedConfig, NullIndex);
    COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.FreqLimitMixedConfig_1R1R_8GB, MemConfig->FreqLimitMixedConfig_1R1R_8GB, SaSetup.FreqLimitMixedConfig_1R1R_8GB, NullIndex);
    COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.FreqLimitMixedConfig_1R1R_16GB, MemConfig->FreqLimitMixedConfig_1R1R_16GB, SaSetup.FreqLimitMixedConfig_1R1R_16GB, NullIndex);
    COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.FreqLimitMixedConfig_1R1R_8GB_16GB, MemConfig->FreqLimitMixedConfig_1R1R_8GB_16GB, SaSetup.FreqLimitMixedConfig_1R1R_8GB_16GB, NullIndex);
    COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.FreqLimitMixedConfig_2R2R, MemConfig->FreqLimitMixedConfig_2R2R, SaSetup.FreqLimitMixedConfig_2R2R, NullIndex);
    COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.LctCmdEyeWidth,             MemConfig->LctCmdEyeWidth,             SaSetup.LctCmdEyeWidth,             NullIndex);
    COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.FirstDimmBitMask,        MemConfig->FirstDimmBitMask,        SaSetup.FirstDimmBitMask,        NullIndex);
    COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.FirstDimmBitMaskEcc,     MemConfig->FirstDimmBitMaskEcc,        SaSetup.FirstDimmBitMaskEcc,        NullIndex);
    COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.SpdProfileSelected,      MemConfig->SpdProfileSelected,      SaSetup.SpdProfileSelected,      NullIndex);
    COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.DynamicMemoryBoost,      MemConfig->DynamicMemoryBoost,      SaSetup.DynamicMemoryBoost,      NullIndex);
    COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.RealtimeMemoryFrequency, MemConfig->RealtimeMemoryFrequency, SaSetup.RealtimeMemoryFrequency, NullIndex);
    COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.SagvSwitchFactorIA,      MemConfig->SagvSwitchFactorIA,      SaSetup.SagvSwitchFactorIA,        NullIndex);
    COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.SagvSwitchFactorGT,      MemConfig->SagvSwitchFactorGT,      SaSetup.SagvSwitchFactorGT,        NullIndex);
    COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.SagvSwitchFactorIO,      MemConfig->SagvSwitchFactorIO,      SaSetup.SagvSwitchFactorIO,        NullIndex);
    COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.SagvSwitchFactorStall,   MemConfig->SagvSwitchFactorStall,   SaSetup.SagvSwitchFactorStall,     NullIndex);
    COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.SagvHeuristicsUpControl, MemConfig->SagvHeuristicsUpControl, SaSetup.SagvHeuristicsUpControl,      NullIndex);
    COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.SagvHeuristicsDownControl, MemConfig->SagvHeuristicsDownControl, SaSetup.SagvHeuristicsDownControl,      NullIndex);

    COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.Ibecc,              MemConfig->Ibecc,              SaSetup.Ibecc,              NullIndex);
    COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.IbeccOperationMode, MemConfig->IbeccOperationMode, SaSetup.IbeccOperationMode, NullIndex);
    for (Index = 0; Index < MAX_IBECC_REGIONS; Index++) {
      COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.IbeccProtectedRangeEnable[Index], MemConfig->IbeccProtectedRangeEnable[Index], SaSetup.IbeccProtectedRangeEnable[Index], NullIndex);
      COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.IbeccProtectedRangeBase[Index],   MemConfig->IbeccProtectedRangeBase[Index],   SaSetup.IbeccProtectedRangeBase[Index],   NullIndex);
      COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.IbeccProtectedRangeMask[Index],   MemConfig->IbeccProtectedRangeMask[Index],   SaSetup.IbeccProtectedRangeMask[Index],   NullIndex);
    }
    COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.IbeccErrInjControl, MemConfig->IbeccErrInjControl, SaSetup.IbeccErrInjControl, NullIndex);
    COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.IbeccErrInjAddress, MemConfig->IbeccErrInjAddress, SaSetup.IbeccErrInjAddress, NullIndex);
    COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.IbeccErrInjMask,    MemConfig->IbeccErrInjMask,    SaSetup.IbeccErrInjMask,    NullIndex);
    COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.IbeccErrInjCount,   MemConfig->IbeccErrInjCount,   SaSetup.IbeccErrInjCount,   NullIndex);

    COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.EccErrInjAddress, MemConfig->EccErrInjAddress, SaSetup.EccErrInjAddress, NullIndex);
    COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.EccErrInjMask,    MemConfig->EccErrInjMask,    SaSetup.EccErrInjMask,    NullIndex);
    COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.EccErrInjCount,   MemConfig->EccErrInjCount,   SaSetup.EccErrInjCount,   NullIndex);

    //
    // If user custom profile is selected, we will send the setup values to the MRC.
    // The setup values will be the current memory settings plus user override values.
    // If any other profile is selected or a WDT timeout has occured, we zero out
    // the settings just to be safe.
    //
    gWdtPei = NULL;
    Status = PeiServicesLocatePpi(
               &gWdtPpiGuid,
               0,
               NULL,
               (VOID **) &gWdtPei
               );
    if (gWdtPei != NULL) {
      WdtTimeout = gWdtPei->CheckStatus();
    } else {
      WdtTimeout = FALSE;
    }

    if ((SaSetup.SpdProfileSelected == CUSTOM_PROFILE) && (WdtTimeout == FALSE)) {
      //
      // If USER custom profile is selected, we will start the WDT.
      //
      if (gWdtPei != NULL) {
        Status = gWdtPei->ReloadAndStart(WDT_TIMEOUT);
      }

      //
      // Read DDR RefClk setting selected in Bios setup, 0 for 133MHz and 1 for 100MHz.
      //
      COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.VddVoltage,   MemConfig->VddVoltage,   SaSetup.MemoryVoltage, NullIndex);     // Vdd  in [mV], 0 = platform default.
      COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.VddqVoltage,  MemConfig->VddqVoltage,  SaSetup.MemoryVddqVoltage, NullIndex); // Vddq in [mV], 0 = platform default.
      COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.VppVoltage,   MemConfig->VppVoltage,   SaSetup.MemoryVppVoltage, NullIndex);  // Vpp  in [mV], 0 = platform default.
      COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.RefClk,       MemConfig->RefClk,       SaSetup.DdrRefClk,     NullIndex);
      COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.Ratio,        MemConfig->Ratio,        SaSetup.DdrRatio,      NullIndex);
      COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.tCL,          MemConfig->tCL,          SaSetup.tCL,           NullIndex);
      COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.tCWL,         MemConfig->tCWL,         SaSetup.tCWL,          NullIndex);
      COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.tFAW,         MemConfig->tFAW,         SaSetup.tFAW,          NullIndex);
      COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.tRAS,         MemConfig->tRAS,         SaSetup.tRAS,          NullIndex);
      COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.tRCDtRP,      MemConfig->tRCDtRP,      SaSetup.tRCDtRP,       NullIndex);
      COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.tREFI,        MemConfig->tREFI,        SaSetup.tREFI,         NullIndex);
      COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.tRFC,         MemConfig->tRFC,         SaSetup.tRFC,          NullIndex);
      COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.tRFCpb,       MemConfig->tRFCpb,       SaSetup.tRFCpb,        NullIndex);
      COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.tRFC2,        MemConfig->tRFC2,        SaSetup.tRFC2,          NullIndex);
      COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.tRFC4,        MemConfig->tRFC4,        SaSetup.tRFC4,          NullIndex);
      COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.tRRD,         MemConfig->tRRD,         SaSetup.tRRD,          NullIndex);
      COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.tRRD_L,       MemConfig->tRRD_L,       SaSetup.tRRD_L,        NullIndex);
      COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.tRRD_S,       MemConfig->tRRD_S,       SaSetup.tRRD_S,        NullIndex);
      COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.tRTP,         MemConfig->tRTP,         SaSetup.tRTP,          NullIndex);
      COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.tWR,          MemConfig->tWR,          SaSetup.tWR,           NullIndex);
      COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.tWTR,         MemConfig->tWTR,         SaSetup.tWTR,          NullIndex);
      COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.tWTR_L,       MemConfig->tWTR_L,       SaSetup.tWTR_L,        NullIndex);
      COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.tWTR_S,       MemConfig->tWTR_S,       SaSetup.tWTR_S,        NullIndex);
      COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.tCCD_L,       MemConfig->tCCD_L,       SaSetup.tCCD_L,        NullIndex);
      COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.NModeSupport, MemConfig->NModeSupport, SaSetup.NModeSupport,  NullIndex);
    } else {
      UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.VddVoltage,   MemConfig->VddVoltage,          0); // Use platform default as the safe value.
      UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.VddqVoltage,  MemConfig->VddqVoltage,         0); // Use platform default as the safe value.
      UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.VppVoltage,   MemConfig->VppVoltage,          0); // Use platform default as the safe value.
      UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.Ratio,        MemConfig->Ratio,               0);
      UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.tCL,          MemConfig->tCL,                 0);
      UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.tCWL,         MemConfig->tCWL,                0);
      UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.tFAW,         MemConfig->tFAW,                0);
      UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.tRAS,         MemConfig->tRAS,                0);
      UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.tRCDtRP,      MemConfig->tRCDtRP,             0);
      UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.tREFI,        MemConfig->tREFI,               0);
      UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.tRFC,         MemConfig->tRFC,                0);
      UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.tRRD,         MemConfig->tRRD,                0);
      UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.tRTP,         MemConfig->tRTP,                0);
      UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.tWR,          MemConfig->tWR,                 0);
      UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.tWTR,         MemConfig->tWTR,                0);
      UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.NModeSupport, MemConfig->NModeSupport,        0);
    }
    if ((SaSetup.SpdProfileSelected == DEFAULT_SPD) && (WdtTimeout == FALSE)) {
      // Allow Voltage override in Default / Standard Profile as well
      COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.VddVoltage,   MemConfig->VddVoltage,   SaSetup.MemoryVoltage,     NullIndex); // Vdd  in [mV], 0 = platform default.
      COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.VddqVoltage,  MemConfig->VddqVoltage,  SaSetup.MemoryVddqVoltage, NullIndex); // Vddq in [mV], 0 = platform default.
      COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.VppVoltage,   MemConfig->VppVoltage,   SaSetup.MemoryVppVoltage,  NullIndex); // Vpp  in [mV], 0 = platform default.
    }

    UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.CmdMirror,                            MemConfig->CmdMirror,             PcdGet8 (PcdMrcCmdMirror)); // BitMask where bits [3:0] are controller 0 Channel [3:0] and [7:4] are Controller 1 Channel [3:0].  0 = No Command Mirror and 1 = Command Mirror.
    COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.RemapEnable,              MemConfig->RemapEnable,           SaSetup.RemapEnable,           NullIndex);
    COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.MrcFastBoot,              MemConfig->MrcFastBoot,           SaSetup.MrcFastBoot,           NullIndex);

    COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.PowerDownMode,            MemConfig->PowerDownMode,         SaSetup.PowerDownMode,         NullIndex);
    COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.PwdwnIdleCounter,         MemConfig->PwdwnIdleCounter,      SaSetup.PwdwnIdleCounter,      NullIndex);
    COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.DisPgCloseIdleTimeout,    MemConfig->DisPgCloseIdleTimeout, SaSetup.DisPgCloseIdleTimeout, NullIndex);
    COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.ScramblerSupport,         MemConfig->ScramblerSupport,      SaSetup.ScramblerSupport,      NullIndex);
    COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.TrainTrace,               MemConfig->TrainTrace,            SaSetup.TrainTrace,            NullIndex);
    COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.RmtPerTask,               MemConfig->RmtPerTask,            SaSetup.RmtPerTask,            NullIndex);
    DEBUG_CODE (
      DEBUG_CONFIG_DATA DebugConfigData;

      VariableSize = sizeof (DEBUG_CONFIG_DATA);
      Status = VariableServices->GetVariable (
                                   VariableServices,
                                   L"DebugConfigData",
                                   &gDebugConfigVariableGuid,
                                   NULL,
                                   &VariableSize,
                                   &DebugConfigData
                                   );
      ASSERT_EFI_ERROR (Status);
      COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.SerialDebugMrcLevel, MemConfigNoCrc->SerialDebugLevel, DebugConfigData.SerialDebugMrcLevel, NullIndex);
    );
    COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.ECT,                           MemConfig->ECT,                 SaSetup.ECT,                  NullIndex);
    COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.SOT,                           MemConfig->SOT,                 SaSetup.SOT,                  NullIndex);
    COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.ERDMPRTC2D,                    MemConfig->ERDMPRTC2D,          SaSetup.ERDMPRTC2D,           NullIndex);
    COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.RDMPRT,                        MemConfig->RDMPRT,              SaSetup.RDMPRT,               NullIndex);
    COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.RCVET,                         MemConfig->RCVET,               SaSetup.RCVET,                NullIndex);
    COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.DCC,                           MemConfig->DCC,                 SaSetup.DCC,                  NullIndex);
    COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.JWRL,                          MemConfig->JWRL,                SaSetup.JWRL,                 NullIndex);
    COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.EWRTC2D,                       MemConfig->EWRTC2D,             SaSetup.EWRTC2D,              NullIndex);
    COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.ERDTC2D,                       MemConfig->ERDTC2D,             SaSetup.ERDTC2D,              NullIndex);
    COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.WRTC1D,                        MemConfig->WRTC1D,              SaSetup.WRTC1D,               NullIndex);
    COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.WRVC1D,                        MemConfig->WRVC1D,              SaSetup.WRVC1D,               NullIndex);
    COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.RDTC1D,                        MemConfig->RDTC1D,              SaSetup.RDTC1D,               NullIndex);
    COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.DIMMODTT,                      MemConfig->DIMMODTT,            SaSetup.DIMMODTT,             NullIndex);
    COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.DIMMRONT,                      MemConfig->DIMMRONT,            SaSetup.DIMMRONT,             NullIndex);
    COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.WRSRT,                         MemConfig->WRSRT,               SaSetup.WRSRT,                NullIndex);
    COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.RDODTT,                        MemConfig->RDODTT,              SaSetup.RDODTT,               NullIndex);
    COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.RDEQT,                         MemConfig->RDEQT,               SaSetup.RDEQT,                NullIndex);
    COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.RDAPT,                         MemConfig->RDAPT,               SaSetup.RDAPT,                NullIndex);
    COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.WRTC2D,                        MemConfig->WRTC2D,              SaSetup.WRTC2D,               NullIndex);
    COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.RDTC2D,                        MemConfig->RDTC2D,              SaSetup.RDTC2D,               NullIndex);
    COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.WRVC2D,                        MemConfig->WRVC2D,              SaSetup.WRVC2D,               NullIndex);
    COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.RDVC2D,                        MemConfig->RDVC2D,              SaSetup.RDVC2D,               NullIndex);
    COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.CMDVC,                         MemConfig->CMDVC,               SaSetup.CMDVC,                NullIndex);
    COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.LCT,                           MemConfig->LCT,                 SaSetup.LCT,                  NullIndex);
    COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.RTL,                           MemConfig->RTL,                 SaSetup.RTL,                  NullIndex);
    COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.TAT,                           MemConfig->TAT,                 SaSetup.TAT,                  NullIndex);
    COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.RCVENC1D,                      MemConfig->RCVENC1D,            SaSetup.RCVENC1D,             NullIndex);
    COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.RMT,                           MemConfig->RMT,                 SaSetup.RMT,                  NullIndex);
    COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.RMTBIT,                        MemConfig->RMTBIT,              SaSetup.RMTBIT,               NullIndex);
    COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.MEMTST,                        MemConfig->MEMTST,              SaSetup.MEMTST,               NullIndex);
    COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.ALIASCHK,                      MemConfig->ALIASCHK,            SaSetup.ALIASCHK,             NullIndex);
    COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.RMC,                           MemConfig->RMC,                 SaSetup.RMC,                  NullIndex);
    COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.WRDSUDT,                       MemConfig->WRDSUDT,             SaSetup.WRDSUDT,              NullIndex);
    COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.WRDSEQT,                       MemConfig->WRDSEQT,             SaSetup.WRDSEQT,              NullIndex);
    COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.DIMMDFE,                       MemConfig->DIMMDFE,             SaSetup.DIMMDFE,              NullIndex);
    COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.EARLYDIMMDFE,                  MemConfig->EARLYDIMMDFE,        SaSetup.EARLYDIMMDFE,         NullIndex);
    COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.TXDQSDCC,                      MemConfig->TXDQSDCC,            SaSetup.TXDQSDCC,             NullIndex);
    COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.DRAMDCA,                       MemConfig->DRAMDCA,             SaSetup.DRAMDCA,              NullIndex);
    COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.WRDS,                          MemConfig->WRDS,                SaSetup.WRDS,                 NullIndex);
    COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.MarginLimitCheck,              MemConfig->MarginLimitCheck,    SaSetup.MarginLimitCheck,     NullIndex);
    COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.MarginLimitL2,                 MemConfig->MarginLimitL2,       SaSetup.MarginLimitL2,        NullIndex);

//[-start-200831-IB17800093-add]//
    COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.CMDDSEQ,               MemConfig->CMDDSEQ,             SaSetup.CMDDSEQ,                      NullIndex);
    COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.CMDDRUD,               MemConfig->CMDDRUD,             SaSetup.CMDDRUD,                      NullIndex);
//[-end-200831-IB17800093-add]//
    COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.DebugValue,                    MemConfig->DebugValue,          SaSetup.DebugValue,           NullIndex);
    COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.MrcSafeConfig,                 MemConfig->MrcSafeConfig,       SaSetup.MrcSafeConfig,        NullIndex);
    COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.SafeMode,                      MemConfig->SafeMode,            SaSetup.SafeMode,             NullIndex);
    COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.SaGv,                          MemConfig->SaGv,                SaSetup.SaGv,                 NullIndex);
    COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.Ddr4OneDpc,                    MemConfig->Ddr4OneDpc,          SaSetup.Ddr4OneDpc,           NullIndex);
    COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.LpDdrDqDqsReTraining,          MemConfig->LpDqsOscEn,          SaSetup.LpDdrDqDqsReTraining, NullIndex);
    COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.ExitOnFailure,                 MemConfig->ExitOnFailure,       SaSetup.ExitOnFailure,        NullIndex);
    COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.NewFeatureEnable1,             MemConfig->NewFeatureEnable1,   SaSetup.NewFeatureEnable1,    NullIndex);
    COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.NewFeatureEnable2,             MemConfig->NewFeatureEnable2,   SaSetup.NewFeatureEnable2,    NullIndex);
    COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.ChHashOverride,                MemConfig->ChHashOverride,      SaSetup.ChHashOverride,       NullIndex);
    COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.ChHashEnable,                  MemConfig->ChHashEnable,        SaSetup.ChHashEnable,         NullIndex);
    COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.ChHashMask,                    MemConfig->ChHashMask,          SaSetup.ChHashMask,           NullIndex);
    COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.ChHashInterleaveBit,           MemConfig->ChHashInterleaveBit, SaSetup.ChHashInterleaveBit,  NullIndex);
    COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.ExtendedBankHashing,           MemConfig->ExtendedBankHashing, SaSetup.ExtendedBankHashing,  NullIndex);


    COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.Lfsr0Mask,                     MemConfig->Lfsr0Mask,           SaSetup.Lfsr0Mask,            NullIndex);
    COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.Lfsr1Mask,                     MemConfig->Lfsr1Mask,           SaSetup.Lfsr1Mask,            NullIndex);
    COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.LpddrRttWr,                    MemConfig->LpddrRttWr,          SaSetup.LpddrRttWr,           NullIndex);
    COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.LpddrRttCa,                    MemConfig->LpddrRttCa,          SaSetup.LpddrRttCa,           NullIndex);
    COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.RhSelect,                      MemConfig->RhSelect,            SaSetup.RhSelect,             NullIndex);
    COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.McRefreshRate,                 MemConfig->McRefreshRate,       SaSetup.McRefreshRate,        NullIndex);
    COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.PprEnable,                     MemConfig->PprEnable,           SaSetup.PprEnable,            NullIndex);
    COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.RefreshWm,                     MemConfig->RefreshWm,           SaSetup.RefreshWm,            NullIndex);

    //
    // Thermal Options
    //
    COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.EnablePwrDn,      MemConfig->EnablePwrDn,      SaSetup.EnablePwrDn,      NullIndex);
    COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.EnablePwrDnLpddr, MemConfig->EnablePwrDnLpddr, SaSetup.EnablePwrDnLpddr, NullIndex);
    COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.Refresh2X,        MemConfig->Refresh2X,        SaSetup.Refresh2X,        NullIndex);

    COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.SrefCfgEna,               MemConfig->SrefCfgEna,            SaSetup.SrefCfgEna,            NullIndex);
    COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.ThrtCkeMinDefeat,         MemConfig->ThrtCkeMinDefeat,      SaSetup.ThrtCkeMinDefeat,      NullIndex);
    COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.ThrtCkeMinTmr,            MemConfig->ThrtCkeMinTmr,         SaSetup.ThrtCkeMinTmr,         NullIndex);
    COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.ThrtCkeMinDefeatLpddr,    MemConfig->ThrtCkeMinDefeatLpddr, SaSetup.ThrtCkeMinDefeatLpddr, NullIndex);
    COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.WriteThreshold,           MemConfig->WriteThreshold,        SaSetup.WriteThreshold,         NullIndex);
    COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.AllowOppRefBelowWriteThrehold, MemConfig->AllowOppRefBelowWriteThrehold, SaSetup.AllowOppRefBelowWriteThrehold, NullIndex);

#if FixedPcdGet8(PcdFspModeSelection) == 0
    MemConfig->ThrtCkeMinTmrLpddr = SaSetup.ThrtCkeMinTmrLpddr;
#endif

#if FixedPcdGet8(PcdFspModeSelection) == 0
    MemConfig->RetrainOnFastFail = SaSetup.RetrainOnFastFail;
#endif

    //
    // Setup BCLK value is in 10kHz units. Convert to Hertz for MRC to use. We need to round
    // the BCLK value to the nearest coarse BCLK freq.
    //
    RoundedBclkFreq = SaSetup.BclkFrequency * 10000;
    if (RoundedBclkFreq > BCLK_MAX) {
      RoundedBclkFreq = BCLK_MAX;
    } else if (RoundedBclkFreq < BCLK_100) {
      RoundedBclkFreq = BCLK_100;
    }

    if ((RoundedBclkFreq % BCLK_GRANULARITY) != 0) {
      RoundedBclkFreq += (BCLK_GRANULARITY - (RoundedBclkFreq % BCLK_GRANULARITY));
    }
    UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.BClkFrequency, MemConfig->BClkFrequency, RoundedBclkFreq);

    //
    // BCLK RFI value is in 10kHz units. Convert to Hertz for MRC to use.
    //
    for (Index = 0; Index < MAX_BCLK_RFI_POINTS; Index++) {
      RoundedBclkFreq = CpuSetup.BclkRfi10KhzFreq[Index] * 10000;
      UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.BclkRfiFreq[Index], MemConfig->BclkRfiFreq[Index], RoundedBclkFreq);
    }
    //
    // DDR Frequency Limit (0 = Auto)
    // The values must match the definitions in IcelakeSiliconPkg\SystemAgent\MemoryInit\Include\MrcInterface.h
    //
    COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.DdrFreqLimit, MemConfig->DdrFreqLimit, SaSetup.DdrFreqLimit, NullIndex);
    COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.Lp5BankMode, MemConfig->Lp5BankMode, SaSetup.Lp5BankMode, NullIndex);

    // FirstDimmBitMask defines which DIMM should be populated first on a 2DPC board
    SaSetup.FirstDimmBitMask = PcdGet8(PcdSaMiscFirstDimmBitMask);
    SaSetup.FirstDimmBitMaskEcc = PcdGet8(PcdSaMiscFirstDimmBitMaskEcc);
    COMPARE_AND_UPDATE_POLICY(((FSPM_UPD *)FspmUpd)->FspmConfig.FirstDimmBitMask, MemConfig->FirstDimmBitMask, SaSetup.FirstDimmBitMask, NullIndex);
    COMPARE_AND_UPDATE_POLICY(((FSPM_UPD *)FspmUpd)->FspmConfig.FirstDimmBitMaskEcc, MemConfig->FirstDimmBitMaskEcc, SaSetup.FirstDimmBitMaskEcc, NullIndex);

    //
    // Update MemTestOnWarmBoot variable. Default is run BaseMemoryTest on warm boot.
    //
    COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.MemTestOnWarmBoot, MemConfigNoCrc->MemTestOnWarmBoot, SaSetup.MemTestOnWarmBoot, NullIndex);

    //
    // Update CleanMemory variable from Memory overwrite request value. Ignore if we are performing capsule update.
    //
    if ((BootMode != BOOT_ON_FLASH_UPDATE) && (BootMode != BOOT_ON_S3_RESUME)) {
      UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.CleanMemory, MemConfigNoCrc->CleanMemory, (BOOLEAN)(MorControl & MOR_CLEAR_MEMORY_BIT_MASK));
    }

    //
    // Based on BIOS setup to determine maximum top of memory size below 4G, and reserved for MMIO
    //
    switch (SaSetup.MaxTolud) {
      case MAX_TOLUD_DYNAMIC:
        MmioSize = 0x0;
        break;
      case MAX_TOLUD_1G:
        MmioSize = 0xC00;
        break;
      case MAX_TOLUD_1_25G:
        MmioSize = 0xB00;
        break;
      case MAX_TOLUD_1_5G:
        MmioSize = 0xA00;
        break;
      case MAX_TOLUD_1_75G:
        MmioSize = 0x900;
        break;
      case MAX_TOLUD_2G:
        MmioSize = 0x800;
        break;
      case MAX_TOLUD_2_25G:
        MmioSize = 0x700;
        break;
      case MAX_TOLUD_2_5G:
        MmioSize = 0x600;
        break;
      case MAX_TOLUD_2_75G:
        MmioSize = 0x500;
        break;
      default:
      case MAX_TOLUD_3G:
        MmioSize = 0x400;
        break;
      case MAX_TOLUD_3_25G:
        MmioSize = 0x300;
        break;
      case MAX_TOLUD_3_5G:
        MmioSize = 0x200;
        break;
    }
    UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.MmioSize, HostBridgePreMemConfig->MmioSize, MmioSize);

    COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.CpuTraceHubMode,        CpuTraceHubPreMemConfig->TraceHub.EnableMode,  SaSetup.CpuTraceHubMode,        NullIndex);
    COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.CpuTraceHubMemReg0Size, CpuTraceHubPreMemConfig->TraceHub.MemReg0Size, SaSetup.CpuTraceHubMemReg0Size, NullIndex);
    COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.CpuTraceHubMemReg1Size, CpuTraceHubPreMemConfig->TraceHub.MemReg1Size, SaSetup.CpuTraceHubMemReg1Size, NullIndex);
  }

#if FixedPcdGet8(PcdFspModeSelection) == 0
  if (BootMode == BOOT_ON_FLASH_UPDATE) {
    CapsuleSupportMemSize = SIZE_32MB; // Reserve 32M for Capsule update/Recovery usage in PEI
  }
#endif

  DataSize = sizeof (MemoryData);
  Status = VariableServices->GetVariable (
                               VariableServices,
                               EFI_MEMORY_TYPE_INFORMATION_VARIABLE_NAME,
                               &gEfiMemoryTypeInformationGuid,
                               NULL,
                               &DataSize,
                               &MemoryData
                               );
  ///
  /// Accumulate maximum amount of memory needed
  ///
#if FixedPcdGet8(PcdFspModeSelection) == 1
  PlatformMemorySize = ((FSPM_UPD *) FspmUpd)->FspmConfig.PlatformMemorySize;
#else
  PlatformMemorySize = MemConfigNoCrc->PlatformMemorySize;
#endif
//[-start-191111-IB10189001-modify]//
  /*
  //
  // Below code from RC but Insyde no use them.
  //
  AsmCpuidEx (CPUID_STRUCTURED_EXTENDED_FEATURE_FLAGS, 0, NULL, &Ebx.Uint32, NULL, NULL);
  if ((Ebx.Bits.IntelProcessorTrace == 1) && (CpuSetup.ProcessorTraceMemSize <= RtitTopaMemorySize128M)) {
    ///
    /// Get the total number of enabled processor threads.
    /// Read MSR 0x35 to get the max enabled number of cores/threads.
    ///
    MsrCoreThreadCount.Uint64 = AsmReadMsr64 (MSR_CORE_THREAD_COUNT);

    //
    // ProcessorTraceTotalMemSize is mutiplied by 2 for adding aligned memory (overhead) for natural alignment.
    // please refer to InternalAllocateAlignedPages (), the overhead will be free when creating memory map, it won't waste
    //
    ProcessorTraceTotalMemSize = 2 * (1 << (CpuSetup.ProcessorTraceMemSize + 12)) * MsrCoreThreadCount.Bits.Threadcount;
    }
  if (EFI_ERROR (Status)) {
    if (BootMode == BOOT_IN_RECOVERY_MODE) {
      PlatformMemorySize = PEI_RECOVERY_MIN_MEMORY_SIZE;
    } else {
      ///
      /// Use default value to avoid memory fragment. Plus tracehub memory if required.
      /// OS boot/installation fails if there is not enough continuous memory available
      ///
      PlatformMemorySize = PEI_MIN_MEMORY_SIZE + TraceHubTotalMemSize + ProcessorTraceTotalMemSize;
      DataSize = sizeof (mDefaultMemoryTypeInformation);
      CopyMem (MemoryData, mDefaultMemoryTypeInformation, DataSize);
    }
  } else {
    ///
    /// Start with at least PEI_MIN_MEMORY_SIZE of memory for the DXE Core and the DXE Stack
    ///
    PlatformMemorySize = PEI_MIN_MEMORY_SIZE + TraceHubTotalMemSize + ProcessorTraceTotalMemSize;
  }
  */

  //
  // Insyde use different PlatformMemorySize and MemoryData
  //
  if (BootMode == BOOT_IN_RECOVERY_MODE) {
    PlatformMemorySize = PcdGet32 (PcdPeiRecoveryMinMemorySize);
  } else {
//[-start-210218-IB16740133-modify]// for build error, TraceHubTotalMemSize has removed in RC2061
    PlatformMemorySize = PcdGet32 (PcdH2OPeiMinMemorySize);
//[-end-210218-IB16740133-modify]//
  }
  if ((EFI_ERROR(Status)) || (!IsMemoryTyepInfoValid (MemoryData, DataSize / sizeof (EFI_MEMORY_TYPE_INFORMATION)))) {
    MemoryInformation = (EFI_MEMORY_TYPE_INFORMATION *)PcdGetPtr (PcdPreserveMemoryTable);
    DataSize = PcdGetSize (PcdPreserveMemoryTable);
    DEBUG_OEM_SVC ((DEBUG_INFO, "OemKernelServices Call: OemSvcUpdateMemoryTypeInformation \n"));
    OemSvcUpdateMemoryTypeInformation (&MemoryInformation, (UINT32*)&DataSize);
    DEBUG_OEM_SVC ((DEBUG_INFO, "OemKernelServices OemSvcUpdateMemoryTypeInformation Status: %r\n", Status));
    CopyMem(MemoryData, MemoryInformation, DataSize);
  }
//[-end-191111-IB10189001-modify]//
  UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.PlatformMemorySize, MemConfigNoCrc->PlatformMemorySize, PlatformMemorySize);

  if (BootMode != BOOT_IN_RECOVERY_MODE) {
    for (Index = 0; Index < DataSize / sizeof (EFI_MEMORY_TYPE_INFORMATION); Index++) {
      PlatformMemorySize += MemoryData[Index].NumberOfPages * EFI_PAGE_SIZE;
    }
    UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.PlatformMemorySize, MemConfigNoCrc->PlatformMemorySize, PlatformMemorySize);
    //
    // Initialize the Overclocking Configuration
    //
    COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.OcSupport, OcPreMemConfig->OcSupport, CpuSetup.OverclockingSupport, NullIndex);

#if FixedPcdGet8(PcdFspModeSelection) == 1
    if (((FSPM_UPD *) FspmUpd)->FspmConfig.OcSupport && (WdtTimeout == FALSE)) {
#else
    if (OcPreMemConfig->OcSupport && (WdtTimeout == FALSE)) {
#endif
      //
      // SA Domain
      //
      if (SaSetup.UncoreVoltageOffsetPrefix == 1) {
        //
        // Offset is negative, need to convert
        //
        UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.SaVoltageOffset, OcPreMemConfig->SaVoltageOffset, (INT16)(~SaSetup.UncoreVoltageOffset + 1));
      } else {
        COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.SaVoltageOffset, OcPreMemConfig->SaVoltageOffset, SaSetup.UncoreVoltageOffset, NullIndex);
      }

      //
      // GT Domain
      //
      COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.GtMaxOcRatio , OcPreMemConfig->GtMaxOcRatio,  SaSetup.GtMaxOcRatio,  NullIndex);
      COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.GtVoltageMode, OcPreMemConfig->GtVoltageMode, SaSetup.GtVoltageMode, NullIndex);

#if FixedPcdGet8(PcdFspModeSelection) == 1
      if (((FSPM_UPD *) FspmUpd)->FspmConfig.GtVoltageMode == OC_LIB_OFFSET_ADAPTIVE) {
#else
      if (OcPreMemConfig->GtVoltageMode == OC_LIB_OFFSET_ADAPTIVE) {
#endif
        GtVoltageOverride   = 0;
        GtExtraTurboVoltage = SaSetup.GtExtraTurboVoltage;
#if FixedPcdGet8(PcdFspModeSelection) == 1
      } else if (((FSPM_UPD *) FspmUpd)->FspmConfig.GtVoltageMode == OC_LIB_OFFSET_OVERRIDE) {
#else
      } else if (OcPreMemConfig->GtVoltageMode == OC_LIB_OFFSET_OVERRIDE) {
#endif
        GtVoltageOverride   = SaSetup.GtVoltageOverride;
        GtExtraTurboVoltage = 0;
      } else {
        GtVoltageOverride   = 0;
        GtExtraTurboVoltage = 0;
      }
      COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.GtVoltageOverride,   OcPreMemConfig->GtVoltageOverride,   GtVoltageOverride,   NullIndex);
      COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.GtExtraTurboVoltage, OcPreMemConfig->GtExtraTurboVoltage, GtExtraTurboVoltage, NullIndex);

      if (SaSetup.GtVoltageOffsetPrefix == 1) {
        //
        // Offset is negative, need to convert
        //
        COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.GtVoltageOffset, OcPreMemConfig->GtVoltageOffset, (INT16)(~SaSetup.GtVoltageOffset + 1), NullIndex);
      } else {
        COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.GtVoltageOffset, OcPreMemConfig->GtVoltageOffset, SaSetup.GtVoltageOffset,               NullIndex);
      }
      COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.RealtimeMemoryTiming, OcPreMemConfig->RealtimeMemoryTiming, SaSetup.RealtimeMemoryTiming, NullIndex);
    } else {
      UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.OcSupport,            OcPreMemConfig->OcSupport,            0);
      UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.GtMaxOcRatio,         OcPreMemConfig->GtMaxOcRatio,         0);
      UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.GtVoltageMode,        OcPreMemConfig->GtVoltageMode,        0);
      UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.GtVoltageOverride,    OcPreMemConfig->GtVoltageOverride,    0);
      UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.GtExtraTurboVoltage,  OcPreMemConfig->GtExtraTurboVoltage,  0);
      UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.GtVoltageOffset,      OcPreMemConfig->GtVoltageOffset,      0);
      UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.SaVoltageOffset,      OcPreMemConfig->SaVoltageOffset,      0);
      UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.RealtimeMemoryTiming, OcPreMemConfig->RealtimeMemoryTiming, 0);
    }

#if FixedPcdGetBool(PcdITbtEnable) == 1
    ///
    /// TCSS DEVEN bits from setup to policy
    ///
    DEBUG ((DEBUG_INFO, "[TCSS] UpdatePeiSaPolicyPreMem Filling TCSS iTBT Policy Start \n"));
    COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.TcssItbtPcie0En, TcssPeiPreMemConfig->DevEnConfig.TcssDevEnBit.TcssItbtPcie0En, SaSetup.TcssItbtPcie0En, NullIndex);
    COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.TcssItbtPcie1En, TcssPeiPreMemConfig->DevEnConfig.TcssDevEnBit.TcssItbtPcie1En, SaSetup.TcssItbtPcie1En, NullIndex);
    COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.TcssItbtPcie2En, TcssPeiPreMemConfig->DevEnConfig.TcssDevEnBit.TcssItbtPcie2En, SaSetup.TcssItbtPcie2En, NullIndex);
    COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.TcssItbtPcie3En, TcssPeiPreMemConfig->DevEnConfig.TcssDevEnBit.TcssItbtPcie3En, SaSetup.TcssItbtPcie3En, NullIndex);
    COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.TcssXhciEn,      TcssPeiPreMemConfig->DevEnConfig.TcssDevEnBit.TcssXhciEn,      SaSetup.TcssXhciEn,      NullIndex);
    COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.TcssXdciEn,      TcssPeiPreMemConfig->DevEnConfig.TcssDevEnBit.TcssXdciEn,      SaSetup.TcssXdciEn,      NullIndex);
    COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.TcssDma0En,      TcssPeiPreMemConfig->DevEnConfig.TcssDevEnBit.TcssDma0En,      SaSetup.TcssDma0En,      NullIndex);
    COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.TcssDma1En,      TcssPeiPreMemConfig->DevEnConfig.TcssDevEnBit.TcssDma1En,      SaSetup.TcssDma1En,      NullIndex);
    DEBUG ((DEBUG_INFO, "[TCSS] UpdatePeiSaPolicyPreMem Filling TCSS iTBT Policy End \n"));

    //
    // Because of the current design of FSP, the PCIEXBAR is an input argument from Wrapper to FSP for
    // supporting Dynamic PCD for PcdPciExpressBaseAddress and PcdPciExpressRegionLength in FSP, so we
    // need to update PCIEXBAR and PcdPciExpressRegionLength to 512MB when PcieMultipleSegmentEnabled is
    // enabled prior to FspmWrapperPeim, then FSP will sync FSP's PcdPciExpressRegionLength and
    // PcdPciExpressBaseAddress with PCIEXBAR in FSPM.
    //
    // If there is no iTBT PCIe RP enabled and PcieMultipleSegmentEnabled still enabled
    // The MMIO resource of Segment1 will be wasted.
    //
    if (SaSetup.PcieMultipleSegmentEnabled) {
      PciexBarReg = PciSegmentRead32 (PCI_SEGMENT_LIB_ADDRESS (SA_SEG_NUM, SA_MC_BUS, SA_MC_DEV, SA_MC_FUN, R_SA_PCIEXBAR));
      PciexBarReg = ((PciexBarReg & ~(B_SA_PCIEXBAR_LENGTH_MASK)) | (V_SA_PCIEXBAR_LENGTH_512MB << N_SA_PCIEXBAR_LENGTH_OFFSET));
      PciSegmentWrite32 (PCI_SEGMENT_LIB_ADDRESS (SA_SEG_NUM, SA_MC_BUS, SA_MC_DEV, SA_MC_FUN, R_SA_PCIEXBAR), PciexBarReg);
      PcdSet32S (PcdPciExpressRegionLength, SIZE_512MB);
    }
#endif

    ///
    /// Build the GUID'd HOB for DXE
    ///
    BuildGuidDataHob (
      &gEfiMemoryTypeInformationGuid,
      MemoryData,
      DataSize
      );
  }
#if FixedPcdGet8(PcdFspModeSelection) == 0
  MiscPeiPreMemConfig->ResizableBarSupport = SaSetup.PcieResizableBarSupport;
#endif

  return EFI_SUCCESS;
}
