/** @file
  CPU PEI Policy Update & initialization.

@copyright
  INTEL CONFIDENTIAL
  Copyright 2009 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains a 'Sample Driver' and is licensed as such under the terms
  of your license agreement with Intel or your vendor. This file may be modified
  by the user, subject to the additional terms of the license agreement.

@par Specification Reference:

**/
#include "PeiCpuPolicyUpdate.h"
#include <Library/ConfigBlockLib.h>
#include <Library/CpuPlatformLib.h>
#include <Library/CpuPolicyLib.h>
#include <Library/HobLib.h>
#include <Library/PeiSiPolicyUpdateLib.h>
#include <Library/PeiVrDomainLib.h>
#include <Library/SiPolicyLib.h>
#include <Library/Tpm12CommandLib.h>
#include <Library/Tpm2CommandLib.h>
#include <Ppi/SecPlatformInformation2.h>

#include <PolicyUpdateMacro.h>

#if FixedPcdGet8(PcdFspModeSelection) == 1
#include <FspmUpd.h>
#include <FspsUpd.h>
#include <Library/FspCommonLib.h>
#endif

/**
  Update Cpu Power Management Policy settings according to the related BIOS Setup options

  @param[in] SetupData             The Setup variables instance
  @param[in] CpuSetup              The Setup variables instance

  @retval EFI_SUCCESS              The PPI is installed and initialized.
  @retval EFI ERRORS               The PPI is not successfully installed.
  @retval EFI_OUT_OF_RESOURCES     Do not have enough resources to initialize the driver
**/
EFI_STATUS
InitCpuPmConfigBySetupValues (
  IN SETUP_DATA                    *SetupData,
  IN CPU_SETUP                     *CpuSetup
  )
{
  UINT8                            Index;
  UINT8                            MaxBusRatio;
  UINT8                            MinBusRatio;
#if FixedPcdGet8(PcdFspModeSelection) == 1
  VOID                             *FspsUpd;
#else
  EFI_STATUS                       Status;
  SI_POLICY_PPI                    *SiPolicyPpi;
  CPU_POWER_MGMT_BASIC_CONFIG      *CpuPowerMgmtBasicConfig;
  CPU_POWER_MGMT_VR_CONFIG         *CpuPowerMgmtVrConfig;
  CPU_POWER_MGMT_CUSTOM_CONFIG     *CpuPowerMgmtCustomConfig;
  CPU_POWER_MGMT_PSYS_CONFIG       *CpuPowerMgmtPsysConfig;
#endif
  UINT16                            MaxNumVrs;

#if FixedPcdGet8(PcdFspModeSelection) == 1
  FspsUpd = NULL;
#else
  Status = EFI_SUCCESS;
  SiPolicyPpi = NULL;
  CpuPowerMgmtBasicConfig = NULL;
  CpuPowerMgmtVrConfig = NULL;
  CpuPowerMgmtCustomConfig = NULL;
  CpuPowerMgmtPsysConfig = NULL;
#endif

  MaxBusRatio = 0;
  MinBusRatio = 0;

#if FixedPcdGet8(PcdFspModeSelection) == 1
  FspsUpd = (FSPS_UPD *) PcdGet32 (PcdFspsUpdDataAddress);
  ASSERT (FspsUpd != NULL);
#else
  Status = PeiServicesLocatePpi (&gSiPolicyPpiGuid, 0, NULL, (VOID **) &SiPolicyPpi);
  ASSERT_EFI_ERROR (Status);

  Status = GetConfigBlock ((VOID *) SiPolicyPpi, &gCpuPowerMgmtBasicConfigGuid, (VOID *) &CpuPowerMgmtBasicConfig);
  ASSERT_EFI_ERROR (Status);

  Status = GetConfigBlock ((VOID *) SiPolicyPpi, &gCpuPowerMgmtVrConfigGuid, (VOID *) &CpuPowerMgmtVrConfig);
  ASSERT_EFI_ERROR (Status);

  Status = GetConfigBlock ((VOID *) SiPolicyPpi, &gCpuPowerMgmtCustomConfigGuid, (VOID *) &CpuPowerMgmtCustomConfig);
  ASSERT_EFI_ERROR (Status);

  Status = GetConfigBlock ((VOID *) SiPolicyPpi, &gCpuPowerMgmtPsysConfigGuid, (VOID *) &CpuPowerMgmtPsysConfig);
  ASSERT_EFI_ERROR (Status);
#endif

  ///
  /// Get Maximum Non-Turbo bus ratio (HFM) from Platform Info MSR Bits[15:8]
  /// Get Maximum Efficiency bus ratio (LFM) from Platform Info MSR Bits[47:40]
  ///
  GetBusRatio (&MaxBusRatio, &MinBusRatio);

  COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.TurboMode,     CpuPowerMgmtBasicConfig->TurboMode,      CpuSetup->TurboMode,     NullIndex);
  COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.PowerLimit2,   CpuPowerMgmtBasicConfig->PowerLimit2,    CpuSetup->PowerLimit2,   NullIndex);
  COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.ConfigTdpLock, CpuPowerMgmtCustomConfig->ConfigTdpLock, CpuSetup->ConfigTdpLock, NullIndex);
  if (CpuSetup->ConfigTdpLock == 1 || SetupData->EnableDptf == 1) {
    UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.ConfigTdpBios, CpuPowerMgmtCustomConfig->ConfigTdpBios, 0);
  } else {
    COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.ConfigTdpBios, CpuPowerMgmtCustomConfig->ConfigTdpBios, CpuSetup->ConfigTdpBios, NullIndex);
  }

  COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.Custom1PowerLimit1,          CpuPowerMgmtCustomConfig->CustomConfigTdpTable[0].CustomPowerLimit1,          (UINT16) (CpuSetup->Custom1PowerLimit1Power / 125), NullIndex);
  COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.Custom1PowerLimit2,          CpuPowerMgmtCustomConfig->CustomConfigTdpTable[0].CustomPowerLimit2,          (UINT16) (CpuSetup->Custom1PowerLimit2Power / 125), NullIndex);
  COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.Custom1PowerLimit1Time,      CpuPowerMgmtCustomConfig->CustomConfigTdpTable[0].CustomPowerLimit1Time,      CpuSetup->Custom1PowerLimit1Time,                   NullIndex);
  COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.Custom1TurboActivationRatio, CpuPowerMgmtCustomConfig->CustomConfigTdpTable[0].CustomTurboActivationRatio, CpuSetup->Custom1TurboActivationRatio,              NullIndex);
  COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.Custom2PowerLimit1,          CpuPowerMgmtCustomConfig->CustomConfigTdpTable[1].CustomPowerLimit1,          (UINT16) (CpuSetup->Custom2PowerLimit1Power / 125), NullIndex);
  COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.Custom2PowerLimit2,          CpuPowerMgmtCustomConfig->CustomConfigTdpTable[1].CustomPowerLimit2,          (UINT16) (CpuSetup->Custom2PowerLimit2Power / 125), NullIndex);
  COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.Custom2PowerLimit1Time,      CpuPowerMgmtCustomConfig->CustomConfigTdpTable[1].CustomPowerLimit1Time,      CpuSetup->Custom2PowerLimit1Time,                   NullIndex);
  COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.Custom2TurboActivationRatio, CpuPowerMgmtCustomConfig->CustomConfigTdpTable[1].CustomTurboActivationRatio, CpuSetup->Custom2TurboActivationRatio,              NullIndex);
  COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.Custom3PowerLimit1,          CpuPowerMgmtCustomConfig->CustomConfigTdpTable[2].CustomPowerLimit1,          (UINT16) (CpuSetup->Custom3PowerLimit1Power / 125), NullIndex);
  COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.Custom3PowerLimit2,          CpuPowerMgmtCustomConfig->CustomConfigTdpTable[2].CustomPowerLimit2,          (UINT16) (CpuSetup->Custom3PowerLimit2Power / 125), NullIndex);
  COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.Custom3PowerLimit1Time,      CpuPowerMgmtCustomConfig->CustomConfigTdpTable[2].CustomPowerLimit1Time,      CpuSetup->Custom3PowerLimit1Time,                   NullIndex);
  COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.Custom3TurboActivationRatio, CpuPowerMgmtCustomConfig->CustomConfigTdpTable[2].CustomTurboActivationRatio, CpuSetup->Custom3TurboActivationRatio,              NullIndex);

  //
  // Turbo Mode setting
  //
  if (CpuSetup->LongDurationPwrLimitOverride) {
    COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.PowerLimit1,     CpuPowerMgmtBasicConfig->PowerLimit1,     (UINT16) (CpuSetup->PowerLimit1 / 125), NullIndex);
    COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.PowerLimit1Time, CpuPowerMgmtBasicConfig->PowerLimit1Time, CpuSetup->PowerLimit1Time,              NullIndex);
  }

  COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.PowerLimit2Power,    CpuPowerMgmtBasicConfig->PowerLimit2Power,    (UINT16) (CpuSetup->PowerLimit2Power / 125), NullIndex);
  COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.TurboPowerLimitLock, CpuPowerMgmtBasicConfig->TurboPowerLimitLock, CpuSetup->TurboPowerLimitLock,               NullIndex);

  if (CpuSetup->PowerLimit3Override) {
    COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.PowerLimit3,          CpuPowerMgmtBasicConfig->PowerLimit3,          (UINT16) (CpuSetup->PowerLimit3 / 125), NullIndex);
    COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.PowerLimit3Time,      CpuPowerMgmtBasicConfig->PowerLimit3Time,      CpuSetup->PowerLimit3Time,              NullIndex);
    COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.PowerLimit3DutyCycle, CpuPowerMgmtBasicConfig->PowerLimit3DutyCycle, CpuSetup->PowerLimit3DutyCycle,         NullIndex);
    COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.PowerLimit3Lock,      CpuPowerMgmtBasicConfig->PowerLimit3Lock,      CpuSetup->PowerLimit3Lock,              NullIndex);
  }

  if (CpuSetup->PowerLimit4Override) {
    COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.PowerLimit4,     CpuPowerMgmtBasicConfig->PowerLimit4,     (UINT16) (CpuSetup->PowerLimit4 / 125), NullIndex);
    COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.PowerLimit4Lock, CpuPowerMgmtBasicConfig->PowerLimit4Lock, CpuSetup->PowerLimit4Lock,              NullIndex);
  }

  COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.PsysPowerLimit1,      CpuPowerMgmtPsysConfig->PsysPowerLimit1,      CpuSetup->PlatformPowerLimit1Enable, NullIndex);
  COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.PsysPowerLimit2,      CpuPowerMgmtPsysConfig->PsysPowerLimit2,      CpuSetup->PlatformPowerLimit2Enable, NullIndex);

  COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.PsysPowerLimit1Power, CpuPowerMgmtPsysConfig->PsysPowerLimit1Power, (UINT16) (CpuSetup->PlatformPowerLimit1Power / 125), NullIndex);
  COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.PsysPowerLimit1Time , CpuPowerMgmtPsysConfig->PsysPowerLimit1Time , CpuSetup->PlatformPowerLimit1Time,                   NullIndex);
  COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.PsysPowerLimit2Power, CpuPowerMgmtPsysConfig->PsysPowerLimit2Power, (UINT16) (CpuSetup->PlatformPowerLimit2Power / 125), NullIndex);

  //
  // Customize HDC
  //
  COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.HdcControl, CpuPowerMgmtBasicConfig->HdcControl, CpuSetup->HdcControl, NullIndex);

  //
  // Intel Speed Shift Technology
  //
  COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.Hwp,                  CpuPowerMgmtBasicConfig->Hwp,                 CpuSetup->EnableHwp,           NullIndex);
  COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.HwpInterruptControl,  CpuPowerMgmtBasicConfig->HwpInterruptControl, CpuSetup->HwpInterruptControl, NullIndex);
  COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.HwpLock,              CpuPowerMgmtBasicConfig->HwpLock,             CpuSetup->HwpLock,             NullIndex);

  //
  // Intel Turbo Boost Max Technology 3.0
  //
  COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.EnableItbm, CpuPowerMgmtBasicConfig->EnableItbm, CpuSetup->EnableItbm, NullIndex);

  //
  // HwP Misc Functions
  //
  COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.EnablePerCorePState,        CpuPowerMgmtBasicConfig->EnablePerCorePState,        CpuSetup->EnablePerCorePState,        NullIndex);
  COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.EnableHwpAutoPerCorePstate, CpuPowerMgmtBasicConfig->EnableHwpAutoPerCorePstate, CpuSetup->EnableHwpAutoPerCorePstate, NullIndex);
  COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.EnableHwpAutoEppGrouping,   CpuPowerMgmtBasicConfig->EnableHwpAutoEppGrouping,   CpuSetup->EnableHwpAutoEppGrouping,   NullIndex);
  COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.EnableEpbPeciOverride,      CpuPowerMgmtBasicConfig->EnableEpbPeciOverride,      CpuSetup->EnableEpbPeciOverride,      NullIndex);
  //
  // Applies TDP to non-cTDP or cTDP
  //
  COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.ApplyConfigTdp, CpuPowerMgmtBasicConfig->ApplyConfigTdp, CpuSetup->ApplyConfigTdp, NullIndex);

  //
  // Dual Tau Boost
  //
  if (SetupData->EnableDptf == 1) {
    UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.DualTauBoost, CpuPowerMgmtBasicConfig->DualTauBoost, 0);
  } else {
    COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.DualTauBoost, CpuPowerMgmtBasicConfig->DualTauBoost, CpuSetup->DualTauBoost, NullIndex);
  }


  //
  // Custom VID table
  //
  if (CpuSetup->StateRatio[0] < MinBusRatio) {
    UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.MaxRatio, CpuPowerMgmtCustomConfig->CustomRatioTable.MaxRatio, MinBusRatio);
  } else {
    COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.MaxRatio, CpuPowerMgmtCustomConfig->CustomRatioTable.MaxRatio, CpuSetup->StateRatio[0], NullIndex);
  }

  COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.NumberOfEntries, CpuPowerMgmtCustomConfig->CustomRatioTable.NumberOfEntries, CpuSetup->NumOfCustomPStates, NullIndex);

  for (Index = 0; Index < CpuSetup->NumOfCustomPStates; Index++) {
    COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.StateRatio[Index], CpuPowerMgmtCustomConfig->CustomRatioTable.StateRatio[Index], CpuSetup->StateRatio[Index], Index);
  }

  if (CpuSetup->NumOfCustomPStates > MAX_16_CUSTOM_RATIO_TABLE_ENTRIES) {
    for (Index = 0; Index < MAX_16_CUSTOM_RATIO_TABLE_ENTRIES; Index++) {
      COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.StateRatioMax16[Index], CpuPowerMgmtCustomConfig->CustomRatioTable.StateRatioMax16[Index], CpuSetup->StateRatioMax16[Index], Index);
    }
  }

  //
  // Update Turbo Ratio limit override table
  //
  if (CpuSetup->IsTurboRatioDefaultsInitialized != 0) {
    for (Index = 0; Index < TURBO_RATIO_LIMIT_ARRAY_SIZE; Index++) {
      COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.TurboRatioLimitNumCore[Index],     CpuPowerMgmtBasicConfig->TurboRatioLimitNumCore[Index],     CpuSetup->RatioLimitNumCore[Index],     Index);
      COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.TurboRatioLimitRatio[Index],       CpuPowerMgmtBasicConfig->TurboRatioLimitRatio[Index],       CpuSetup->RatioLimitRatio[Index],       Index);
      COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.AtomTurboRatioLimitNumCore[Index], CpuPowerMgmtBasicConfig->AtomTurboRatioLimitNumCore[Index], CpuSetup->AtomRatioLimitNumCore[Index], Index);
      COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.AtomTurboRatioLimitRatio[Index],   CpuPowerMgmtBasicConfig->AtomTurboRatioLimitRatio[Index],   CpuSetup->AtomRatioLimitRatio[Index],   Index);
    }
  }

  //
  // Update Ring Ratio limits
  //
  COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.MinRingRatioLimit,   CpuPowerMgmtBasicConfig->MinRingRatioLimit,    CpuSetup->MinRingRatioLimit, NullIndex);
  COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.MaxRingRatioLimit,   CpuPowerMgmtBasicConfig->MaxRingRatioLimit,    CpuSetup->MaxRingRatioLimit, NullIndex);

  //
  // VR Configuration
  //
  COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.PsysSlope, CpuPowerMgmtVrConfig->PsysSlope, CpuSetup->PsysSlope, NullIndex);
  if (CpuSetup->PsysOffsetPrefix == 1) {
    //
    // Number is negative, need to convert to 2's complement representation
    //
    COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.PsysOffset, CpuPowerMgmtVrConfig->PsysOffset1, (~CpuSetup->PsysOffset + 1), NullIndex);
  } else {
    COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.PsysOffset, CpuPowerMgmtVrConfig->PsysOffset1, (CpuSetup->PsysOffset),      NullIndex);
  }
  COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.PsysPmax,                 CpuPowerMgmtPsysConfig->PsysPmax,               CpuSetup->PsysPmax,                 NullIndex);
  COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.FivrRfiFrequency,         CpuPowerMgmtVrConfig->FivrRfiFrequency,         CpuSetup->FivrRfiFrequency,         NullIndex);
  COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.FivrSpreadSpectrum,       CpuPowerMgmtVrConfig->FivrSpreadSpectrum,       CpuSetup->FivrSpreadSpectrum,       NullIndex);
  COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.FivrSpectrumEnable,       CpuPowerMgmtVrConfig->FivrSpectrumEnable,       CpuSetup->FivrSpectrumEnable,       NullIndex);
  COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.EnableMinVoltageOverride, CpuPowerMgmtVrConfig->EnableMinVoltageOverride, CpuSetup->EnableMinVoltageOverride, NullIndex);
  COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.MinVoltageRuntime,        CpuPowerMgmtVrConfig->MinVoltageRuntime,        CpuSetup->MinVoltageRuntime,        NullIndex);
  COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.MinVoltageC8,             CpuPowerMgmtVrConfig->MinVoltageC8,             CpuSetup->MinVoltageC8,             NullIndex);

  COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.VccInAuxImonIccImax,      CpuPowerMgmtVrConfig->VccInAuxImonIccImax,      CpuSetup->VccInAuxImonIccImax,        NullIndex);
  COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.VccInAuxImonSlope,        CpuPowerMgmtVrConfig->VccInAuxImonSlope,        (UINT8) CpuSetup->VccInAuxImonSlope,  NullIndex);

  COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.EnableVsysCritical,        CpuPowerMgmtVrConfig->EnableVsysCritical,      CpuSetup->EnableVsysCritical,        NullIndex);
  COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.VsysFullScale,             CpuPowerMgmtVrConfig->VsysFullScale,           CpuSetup->VsysFullScale,             NullIndex);
  COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.VsysCriticalThreshold,     CpuPowerMgmtVrConfig->VsysCriticalThreshold,   CpuSetup->VsysCriticalThreshold,     NullIndex);
  COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.VsysAssertionDeglitchMantissa,    CpuPowerMgmtVrConfig->VsysAssertionDeglitchMantissa,   CpuSetup->VsysAssertionDeglitchMantissa,    NullIndex);
  COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.VsysAssertionDeglitchExponent,    CpuPowerMgmtVrConfig->VsysAssertionDeglitchExponent,   CpuSetup->VsysAssertionDeglitchExponent,    NullIndex);
  COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.VsysDeassertionDeglitchMantissa,  CpuPowerMgmtVrConfig->VsysDeassertionDeglitchMantissa, CpuSetup->VsysDeassertionDeglitchMantissa,  NullIndex);
  COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.VsysDeassertionDeglitchExponent,  CpuPowerMgmtVrConfig->VsysDeassertionDeglitchExponent, CpuSetup->VsysDeassertionDeglitchExponent,  NullIndex);

  if (CpuSetup->VccInAuxImonOffsetPrefix == 1) {
    //
    // Number is negative, need to convert to 2's complement representation
    //
    COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.VccInAuxImonOffset,     CpuPowerMgmtVrConfig->VccInAuxImonOffset,       (~CpuSetup->VccInAuxImonOffset + 1),  NullIndex);
  } else {
    COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.VccInAuxImonOffset,     CpuPowerMgmtVrConfig->VccInAuxImonOffset,       (CpuSetup->VccInAuxImonOffset),       NullIndex);
  }
  ///
  /// Define Maximum Number of Voltage Regulator Domains.
  ///
  MaxNumVrs = GetMaxNumVrs ();

  for (Index = 0; Index < MaxNumVrs; Index++) {
    //
    // Only update if the user wants to override VR settings
    //
    if (CpuSetup->VrConfigEnable[Index] != 0) {
      COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.VrConfigEnable[Index], CpuPowerMgmtVrConfig->VrConfigEnable[Index], CpuSetup->VrConfigEnable[Index],    Index);
      COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.Psi1Threshold[Index],  CpuPowerMgmtVrConfig->Psi1Threshold[Index],  CpuSetup->Psi1Threshold[Index],     Index);
      COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.Psi2Threshold[Index],  CpuPowerMgmtVrConfig->Psi2Threshold[Index],  CpuSetup->Psi2Threshold[Index],     Index);
      COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.Psi3Threshold[Index],  CpuPowerMgmtVrConfig->Psi3Threshold[Index],  CpuSetup->Psi3Threshold[Index],     Index);
      COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.Psi3Enable[Index],     CpuPowerMgmtVrConfig->Psi3Enable[Index],     CpuSetup->Psi3Enable[Index],        Index);
      COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.Psi4Enable[Index],     CpuPowerMgmtVrConfig->Psi4Enable[Index],     CpuSetup->Psi4Enable[Index],        Index);
      COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.ImonSlope[Index],      CpuPowerMgmtVrConfig->ImonSlope[Index],      (UINT8) CpuSetup->ImonSlope[Index], Index);
      if (CpuSetup->ImonOffsetPrefix[Index] == 1) {
        //
        // Number is negative, need to convert to 2's complement representation
        //
        COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.ImonOffset[Index], CpuPowerMgmtVrConfig->ImonOffset[Index], (~CpuSetup->ImonOffset[Index] + 1), Index);
      } else {
        COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.ImonOffset[Index], CpuPowerMgmtVrConfig->ImonOffset[Index], (CpuSetup->ImonOffset[Index]),      Index);
      }
      //
      // Only update if IccMax is non-zero. This is to distinguish between the default EDS override.
      //
      if (CpuSetup->IccMax[Index] != 0) {
        COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.IccMax[Index], CpuPowerMgmtVrConfig->IccMax[Index], CpuSetup->IccMax[Index], Index);
      }
      //
      // Only update if IccLimit is non-zero. This is to distinguish between the default EDS override.
      //
      if (CpuSetup->IccLimit[Index] != 0) {
        COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.IccLimit[Index], CpuPowerMgmtVrConfig->IccLimit[Index], CpuSetup->IccLimit[Index], Index);
      }
      //
      // Only update if Vr Voltage Limit is non-zero.
      //
      if (CpuSetup->VrVoltageLimit[Index] != 0) {
        COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.VrVoltageLimit[Index], CpuPowerMgmtVrConfig->VrVoltageLimit[Index], CpuSetup->VrVoltageLimit[Index], Index);
      }
      // Only update TDC if current limit is non-zero. TDC Lock should be separated to allow
      // locking of TDC feature.
      //
      if (CpuSetup->TdcCurrentLimit[Index] != 0) {
        COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.TdcCurrentLimit[Index], CpuPowerMgmtVrConfig->TdcCurrentLimit[Index], CpuSetup->TdcCurrentLimit[Index], Index);
      }
      COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.TdcTimeWindow[Index],   CpuPowerMgmtVrConfig->TdcTimeWindow1[Index],  CpuSetup->TdcTimeWindow[Index],   Index);
      COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.Irms[Index],      CpuPowerMgmtVrConfig->Irms[Index],      CpuSetup->Irms[Index],      Index);
      COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.TdcEnable[Index], CpuPowerMgmtVrConfig->TdcEnable[Index], CpuSetup->TdcEnable[Index], Index);
      COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.TdcLock[Index],   CpuPowerMgmtVrConfig->TdcLock[Index],   CpuSetup->TdcLock[Index],   Index);
    }
    // Need to check if the user intends to override through CpuSetup to distinguish
    // between the default EDS override.
    if (CpuSetup->AcLoadline[Index] != 0) {
      COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.AcLoadline[Index], CpuPowerMgmtVrConfig->AcLoadline[Index], CpuSetup->AcLoadline[Index], Index);
    }
    if (CpuSetup->DcLoadline[Index] != 0) {
      COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.DcLoadline[Index], CpuPowerMgmtVrConfig->DcLoadline[Index], CpuSetup->DcLoadline[Index], Index);
    }
    COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.FastPkgCRampDisable[Index], CpuPowerMgmtVrConfig->FastPkgCRampDisable[Index], CpuSetup->FastPkgCRampDisable[Index], Index);
    COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.SlowSlewRate[Index],        CpuPowerMgmtVrConfig->SlowSlewRate[Index],        CpuSetup->SlowSlewRate[Index],        Index);
  }
  //
  // VR Acoustic Noise Mitigation
  //
  COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.AcousticNoiseMitigation, CpuPowerMgmtVrConfig->AcousticNoiseMitigation, CpuSetup->AcousticNoiseMitigation, NullIndex);
  COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.PreWake,                 CpuPowerMgmtVrConfig->PreWake,                 CpuSetup->PreWake,                 NullIndex);
  COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.RampUp,                  CpuPowerMgmtVrConfig->RampUp,                  CpuSetup->RampUp,                  NullIndex);
  COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.RampDown,                CpuPowerMgmtVrConfig->RampDown,                CpuSetup->RampDown,                NullIndex);
  //
  // VR Power Delivery design
  //
  COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.VrPowerDeliveryDesign,   CpuPowerMgmtVrConfig->VrPowerDeliveryDesign,   CpuSetup->VrPowerDeliveryDesign,   NullIndex);
  return EFI_SUCCESS;
}

#if FixedPcdGet8(PcdFspModeSelection) == 1
/**
  This routine is used to get Sec Platform Information Record2 Pointer.

  @param[in] PeiServices    Pointer to the PEI services table

  @retval GetSecPlatformInformation2 - The pointer of Sec Platform Information Record2 Pointer.
**/
EFI_SEC_PLATFORM_INFORMATION_RECORD2 *
GetSecPlatformInformation2 (
  IN EFI_PEI_SERVICES **PeiServices
  )
{
  EFI_SEC_PLATFORM_INFORMATION2_PPI    *SecPlatformInformation2Ppi;
  EFI_SEC_PLATFORM_INFORMATION_RECORD2 *SecPlatformInformation2 = NULL;
  UINT64                               InformationSize;
  EFI_STATUS Status;

  //
  // Get BIST information from Sec Platform Information2 Ppi firstly
  //
  Status = PeiServicesLocatePpi (
             &gEfiSecPlatformInformation2PpiGuid,   // GUID
             0,                                     // Instance
             NULL,                                  // EFI_PEI_PPI_DESCRIPTOR
             (VOID ** ) &SecPlatformInformation2Ppi // PPI
             );
  if (EFI_ERROR(Status)) {
    DEBUG((DEBUG_ERROR, "LocatePpi SecPlatformInformationPpi2 Status - %r\n", Status));
    return NULL;
  }

  InformationSize = 0;

  Status = SecPlatformInformation2Ppi->PlatformInformation2 (
                                         (CONST EFI_PEI_SERVICES **) PeiServices,
                                         &InformationSize,
                                         SecPlatformInformation2
                                         );

  ASSERT (Status == EFI_BUFFER_TOO_SMALL);
  if (Status != EFI_BUFFER_TOO_SMALL) {
    return NULL;
  }

  SecPlatformInformation2 = AllocatePool((UINTN)InformationSize);
  ASSERT (SecPlatformInformation2 != NULL);
  if (SecPlatformInformation2 == NULL) {
    return NULL;
  }

  //
  // Retrieve BIST data from SecPlatform2
  //
  Status = SecPlatformInformation2Ppi->PlatformInformation2 (
                                         (CONST EFI_PEI_SERVICES **) PeiServices,
                                         &InformationSize,
                                         SecPlatformInformation2
                                         );
  DEBUG((DEBUG_INFO, "SecPlatformInformation2Ppi->PlatformInformation2 Status - %r\n", Status));
  ASSERT_EFI_ERROR (Status);
  if (EFI_ERROR (Status)) {
    return NULL;
  }

  return SecPlatformInformation2;
}


/**
  This routine is used to get Sec Platform Information Record Pointer.

  @param[in] PeiServices    Pointer to the PEI services table

  @retval GetSecPlatformInformation2 - The pointer of Sec Platform Information Record Pointer.
**/
EFI_SEC_PLATFORM_INFORMATION_RECORD2 *
GetSecPlatformInformationInfoInFormat2 (
  IN EFI_PEI_SERVICES **PeiServices
  )
{
  EFI_SEC_PLATFORM_INFORMATION_PPI     *SecPlatformInformationPpi;
  EFI_SEC_PLATFORM_INFORMATION_RECORD  *SecPlatformInformation = NULL;
  EFI_SEC_PLATFORM_INFORMATION_RECORD2 *SecPlatformInformation2;
  UINT64                               InformationSize;
  EFI_STATUS                           Status;

  //
  // Get BIST information from Sec Platform Information
  //
  Status = PeiServicesLocatePpi (
             &gEfiSecPlatformInformationPpiGuid,    // GUID
             0,                                     // Instance
             NULL,                                  // EFI_PEI_PPI_DESCRIPTOR
             (VOID ** ) &SecPlatformInformationPpi  // PPI
             );
  if (EFI_ERROR(Status)) {
    DEBUG((DEBUG_ERROR, "LocatePpi SecPlatformInformationPpi Status - %r\n", Status));
    return NULL;
  }

  InformationSize = 0;
  Status = SecPlatformInformationPpi->PlatformInformation (
                                        (CONST EFI_PEI_SERVICES  **) PeiServices,
                                        &InformationSize,
                                        SecPlatformInformation
                                        );

  ASSERT (Status == EFI_BUFFER_TOO_SMALL);
  if (Status != EFI_BUFFER_TOO_SMALL) {
    return NULL;
  }

  SecPlatformInformation = AllocatePool((UINTN)InformationSize);
  ASSERT (SecPlatformInformation != NULL);
  if (SecPlatformInformation == NULL) {
    return NULL;
  }

  //
  // Retrieve BIST data from SecPlatform
  //
  Status = SecPlatformInformationPpi->PlatformInformation (
                                        (CONST EFI_PEI_SERVICES **) PeiServices,
                                        &InformationSize,
                                        SecPlatformInformation
                                        );
  DEBUG((DEBUG_INFO, "FSP  SecPlatformInformation2Ppi->PlatformInformation Status - %r\n", Status));
  ASSERT_EFI_ERROR (Status);
  if (EFI_ERROR (Status)) {
    return NULL;
  }

  SecPlatformInformation2 = AllocatePool(sizeof (EFI_SEC_PLATFORM_INFORMATION_RECORD2));
  ASSERT (SecPlatformInformation2 != NULL);
  if (SecPlatformInformation2 == NULL) {
    return NULL;
  }

  SecPlatformInformation2->NumberOfCpus = 1;
  SecPlatformInformation2->CpuInstance[0].CpuLocation = 0;
  SecPlatformInformation2->CpuInstance[0].InfoRecord.x64HealthFlags.Uint32 = SecPlatformInformation->x64HealthFlags.Uint32;

  FreePool(SecPlatformInformation);

  return SecPlatformInformation2;
}
#endif

#if FixedPcdGet8(PcdFspModeSelection) == 0
/*
  Get the uCode region from PCD settings, and copy the patches to memory.
  This function is used to replace CpuLocateMicrocodePatch due to that function can not works
  with uCode update new design.
  In current uCode update solution, there are some padding data between uCode patches,
  the algorithm in CpuLocateMicrocodePatch can not handle this.
  Besides that, in CpuLocateMicrocodePatch function, the scan algorithm just find the first
  correct uCode patch which is not the highest version uCode.
  This function just copy the uCode region to memory, and in later, the CpuMpInit driver
  will load the correct patch for CPU.

  @param[out] RegionAddress     Pointer to the uCode array.
  @param[out] RegionSize        Size of the microcode FV.

  @retval EFI_SUCCESS           Find uCode patch region and success copy the data to memory.
  @retval EFI_NOT_FOUND         Something wrong with uCode region.
  @retval EFI_OUT_OF_RESOUCES   Memory allocation fail.
  @retval EFI_INVALID_PARAMETER RegionAddres or RegionSize is NULL.

*/
EFI_STATUS
SearchMicrocodeRegion (
  OUT UINTN                *RegionAddress,
  OUT UINTN                *RegionSize
  )
{
  UINTN                MicrocodeStart;
  UINTN                MicrocodeEnd;
  UINT8                *MemoryBuffer;

  if (RegionAddress == NULL || RegionSize == NULL) {
    return EFI_INVALID_PARAMETER;
  }

  *RegionAddress = 0;
  *RegionSize = 0;

  if ((FixedPcdGet32 (PcdFlashFvMicrocodeBase) == 0) || (FixedPcdGet32 (PcdFlashFvMicrocodeSize) == 0)) {
    return EFI_NOT_FOUND;
  }

  MicrocodeStart = (UINTN) FixedPcdGet32 (PcdFlashFvMicrocodeBase) + (UINTN) FixedPcdGet32 (PcdMicrocodeOffsetInFv);
  MicrocodeEnd = (UINTN) FixedPcdGet32 (PcdFlashFvMicrocodeBase) + (UINTN) FixedPcdGet32 (PcdFlashFvMicrocodeSize);
  *RegionSize = MicrocodeEnd - MicrocodeStart;

  DEBUG ((DEBUG_INFO, "[SearchMicrocodeRegion]: Microcode Region Address = %x, Size = %d\n", MicrocodeStart, *RegionSize));

  MemoryBuffer = AllocatePages (EFI_SIZE_TO_PAGES (*RegionSize));
  ASSERT (MemoryBuffer != NULL);
  if (MemoryBuffer == NULL) {
    DEBUG ((DEBUG_ERROR, "Failed to allocate enough memory for Microcode Patch.\n"));
    return EFI_OUT_OF_RESOURCES;
  } else {
    CopyMem (MemoryBuffer, (UINT8 *)MicrocodeStart, *RegionSize);
    *RegionAddress = (UINTN)MemoryBuffer;
    DEBUG ((DEBUG_INFO, "Copy whole uCode region to memory, address = %x, size = %d\n", RegionAddress, *RegionSize));
  }

  return EFI_SUCCESS;
}
#endif

/**
  This function performs CPU PEI Policy initialization.

  @retval EFI_SUCCESS              The PPI is installed and initialized.
  @retval EFI ERRORS               The PPI is not successfully installed.
  @retval EFI_OUT_OF_RESOURCES     Do not have enough resources to initialize the driver
**/
EFI_STATUS
EFIAPI
UpdatePeiCpuPolicy (
  VOID
  )
{
  EFI_STATUS                       Status;
  UINTN                            VariableSize;
  SETUP_DATA                       SetupData;
  CPU_SETUP                        CpuSetup;
  EFI_PEI_READ_ONLY_VARIABLE2_PPI  *VariableServices;
#if FixedPcdGet8(PcdFspModeSelection) == 1
  VOID                                 *FspsUpd;
  VOID                                 *FspmUpd;
  EFI_SEC_PLATFORM_INFORMATION_RECORD2 *SecPlatformInformation2;
#else
  SI_POLICY_PPI                    *SiPolicyPpi;
  SI_PREMEM_POLICY_PPI             *SiPreMemPolicyPpi;
  CPU_CONFIG                       *CpuConfig;
  CPU_POWER_MGMT_BASIC_CONFIG      *CpuPowerMgmtBasicConfig;
  CPU_SECURITY_PREMEM_CONFIG       *CpuSecurityPreMemConfig;
#endif
  CPU_SKU                           CpuSku;

  DEBUG ((DEBUG_INFO, "Update PeiCpuPolicyUpdate Pos-Mem Start\n"));

#if FixedPcdGet8(PcdFspModeSelection) == 1
  FspsUpd = NULL;
  FspmUpd = NULL;
#else
  SiPolicyPpi             = NULL;
  CpuConfig               = NULL;
  SiPreMemPolicyPpi       = NULL;
  CpuPowerMgmtBasicConfig = NULL;
  CpuSecurityPreMemConfig = NULL;
#endif

  CpuSku = GetCpuSku ();
#if FixedPcdGet8(PcdFspModeSelection) == 1
  FspmUpd = (FSPM_UPD *) PcdGet32 (PcdFspmUpdDataAddress);
  ASSERT (FspmUpd != NULL);

  FspsUpd = (FSPS_UPD *) PcdGet32 (PcdFspsUpdDataAddress);
  ASSERT (FspsUpd != NULL);
#else
  Status = PeiServicesLocatePpi (&gSiPreMemPolicyPpiGuid, 0, NULL, (VOID **) &SiPreMemPolicyPpi);
  ASSERT_EFI_ERROR (Status);

  Status = PeiServicesLocatePpi (&gSiPolicyPpiGuid, 0, NULL, (VOID **) &SiPolicyPpi);
  ASSERT_EFI_ERROR (Status);

  Status = GetConfigBlock ((VOID *) SiPreMemPolicyPpi, &gCpuSecurityPreMemConfigGuid, (VOID *) &CpuSecurityPreMemConfig);
  ASSERT_EFI_ERROR (Status);

  Status = GetConfigBlock ((VOID *) SiPolicyPpi, &gCpuConfigGuid, (VOID *) &CpuConfig);
  ASSERT_EFI_ERROR (Status);

  Status = GetConfigBlock ((VOID *) SiPolicyPpi, &gCpuPowerMgmtBasicConfigGuid, (VOID *) &CpuPowerMgmtBasicConfig);
  ASSERT_EFI_ERROR (Status);
#endif

  //
  // Make sure ReadOnlyVariablePpi is available
  //
  Status = PeiServicesLocatePpi (
             &gEfiPeiReadOnlyVariable2PpiGuid,
             0,
             NULL,
             (VOID **) &VariableServices
             );
  ASSERT_EFI_ERROR (Status);

  //
  // Get Setup Variable
  //
  VariableSize = sizeof (SETUP_DATA);
  Status = VariableServices->GetVariable (
                               VariableServices,
                               L"Setup",
                               &gSetupVariableGuid,
                               NULL,
                               &VariableSize,
                               &SetupData
                               );
  ASSERT_EFI_ERROR (Status);

  VariableSize = sizeof (CPU_SETUP);
  Status = VariableServices->GetVariable (
                               VariableServices,
                               L"CpuSetup",
                               &gCpuSetupVariableGuid,
                               NULL,
                               &VariableSize,
                               &CpuSetup
                               );
  ASSERT_EFI_ERROR (Status);

  COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.AesEnable, CpuConfig->AesEnable, CpuSetup.AES, NullIndex);
  COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.TxtEnable, CpuConfig->TxtEnable, CpuSetup.Txt, NullIndex);

  if (CpuSetup.PpinSupport == 0) {
    // Update value is related with Setup value, Need to check Policy Default
    COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.PpinSupport, CpuConfig->PpinSupport, 0, NullIndex); ///< reference code policy is disabled
  } else if ((CpuSetup.PpinSupport == 1) && (CpuSetup.PpinEnableMode == 0)) {
    // Update value is related with Setup value, Need to check Policy Default
    COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.PpinSupport, CpuConfig->PpinSupport, 2, NullIndex); ///< reference code policy is set to Auto. The feature is disabled if End of Manufacturing flag is set.
  } else if ((CpuSetup.PpinSupport == 1) && (CpuSetup.PpinEnableMode == 1)) {
    //Update value is related with Setup value, Need to check Policy Default
    COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.PpinSupport, CpuConfig->PpinSupport, 1, NullIndex); ///< reference code policy is enabled
  }

  COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *)FspsUpd)->FspsConfig.AvxDisable,  CpuConfig->AvxDisable,  CpuSetup.AvxDisable,  NullIndex);
  UPDATE_POLICY (((FSPS_UPD *)FspsUpd)->FspsConfig.X2ApicSupport, CpuConfig->X2ApicSupport, 0);

#if FixedPcdGet8(PcdFspModeSelection) == 0
  Status = SearchMicrocodeRegion (
             (UINTN *)&CpuConfig->MicrocodePatchAddress,
             (UINTN *)&CpuConfig->MicrocodePatchRegionSize
             );
#endif

  if ((CpuSetup.TCCActivationOffset == 0) && ((GetPackageTdp () == CPU_TDP_150_WATTS) || (CpuSku == EnumCpuUlx))) {
    //
    // Update the Tcc Activation Offset for M Segment and 150W TDP
    //
    CpuSetup.TCCActivationOffset = 10;
  }
  COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.TccActivationOffset,        CpuPowerMgmtBasicConfig->TccActivationOffset,        CpuSetup.TCCActivationOffset, NullIndex);
  COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.TccOffsetTimeWindowForRatl, CpuPowerMgmtBasicConfig->TccOffsetTimeWindowForRatl, CpuSetup.TccOffsetTimeWindow, NullIndex);
  COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.TccOffsetClamp,             CpuPowerMgmtBasicConfig->TccOffsetClamp,             CpuSetup.TccOffsetClamp,      NullIndex);
  COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.TccOffsetLock,              CpuPowerMgmtBasicConfig->TccOffsetLock,              CpuSetup.TccOffsetLock,       NullIndex);

  //
  // Init Power Management Policy Variables based on setup values
  //
  InitCpuPmConfigBySetupValues (&SetupData, &CpuSetup);

#if FixedPcdGet8(PcdFspModeSelection) == 1
  //
  // Get BIST information from Sec Platform Information
  //
  SecPlatformInformation2 = GetSecPlatformInformation2 ((EFI_PEI_SERVICES **) GetPeiServicesTablePointer ());
  if (SecPlatformInformation2 == NULL) {
    SecPlatformInformation2 = GetSecPlatformInformationInfoInFormat2 ((EFI_PEI_SERVICES **) GetPeiServicesTablePointer ());
  }

  ASSERT (SecPlatformInformation2 != NULL);

  if (SecPlatformInformation2 != NULL) {
    ((FSPS_UPD *) FspsUpd)->FspsConfig.CpuBistData = (UINT32) SecPlatformInformation2;
    DEBUG((DEBUG_INFO, "SecPlatformInformation NumberOfCpus - %x\n", SecPlatformInformation2->NumberOfCpus));
    DEBUG((DEBUG_INFO, "SecPlatformInformation BIST - %x\n", SecPlatformInformation2->CpuInstance[0].InfoRecord.x64HealthFlags.Uint32));
  }
#endif

#if FixedPcdGet8(PcdEmbeddedEnable) == 0x1
    //
    // AC Split Lock
    //
    COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.AcSplitLock, CpuConfig->AcSplitLock, CpuSetup.AcSplitLock, NullIndex);
#endif


  return EFI_SUCCESS;
}
