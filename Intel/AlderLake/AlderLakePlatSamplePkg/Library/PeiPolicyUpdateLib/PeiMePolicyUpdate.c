/** @file

@copyright
  INTEL CONFIDENTIAL
  Copyright 1999 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains a 'Sample Driver' and is licensed as such under the terms
  of your license agreement with Intel or your vendor. This file may be modified
  by the user, subject to the additional terms of the license agreement.

@par Specification Reference:
**/

#include <Library/ConfigBlockLib.h>
#include <Library/DebugLib.h>
#include <Library/PeiServicesLib.h>
#include <Library/PeiSiPolicyUpdateLib.h>
#include <Library/PmcLib.h>
#include <Library/SiPolicyLib.h>
#include <Ppi/ReadOnlyVariable2.h>

#include <PolicyUpdateMacro.h>
#include <SetupVariable.h>

#if FixedPcdGet8(PcdFspModeSelection) == 1
#include <FspsUpd.h>
#endif

#if FixedPcdGetBool(PcdCapsuleEnable) == 1
#include <Guid/FmpCapsule.h>
#include <Library/BaseMemoryLib.h>
#include <Library/HobLib.h>
#include <Guid/SysFwUpdateProgress.h>

/**
  Return if input ImageGuid belongs to a FMP device which would perform ME update

  @param[in] ImageGuid A pointer to GUID

  @retval TRUE  ImageGuid belongs to a FMP which would perform ME update
  @retval FALSE ImageGuid does not belong to a FMP which would perform ME update

**/
BOOLEAN
IsFmpGuidForMeUpdate (
  IN EFI_GUID   *ImageGuid
  )
{
  GUID   *Guid;
  UINTN  Count;
  UINTN  Index;

  Guid  = PcdGetPtr (PcdPlatformMeEDebugCapsuleImageTypeIdGuid);
  Count = PcdGetSize (PcdPlatformMeEDebugCapsuleImageTypeIdGuid) / sizeof(GUID);

  for (Index = 0; Index < Count; Index++, Guid++) {
    if (CompareGuid (ImageGuid, Guid)) {
      return TRUE;
    }
  }

  return FALSE;
}

/**
  Detect if there is a FMP capsule which would perform ME update

  @retval TRUE        Such capsule is detected.
  @retval FALSE       No such capsules there.

**/
BOOLEAN
IsFmpCapsuleForMeUpdateDetected (
  VOID
  )
{
  EFI_PEI_HOB_POINTERS                          HobPointer;
  EFI_CAPSULE_HEADER                            *CapsuleHeader;
  EFI_FIRMWARE_MANAGEMENT_CAPSULE_HEADER        *FmpCapsuleHeader;
  EFI_FIRMWARE_MANAGEMENT_CAPSULE_IMAGE_HEADER  *ImageHeader;
  UINTN                                         Index;
  UINT64                                        *ItemOffsetList;

  //
  // Search all capsule images from hob
  //
  HobPointer.Raw = GetHobList ();
  while ((HobPointer.Raw = GetNextHob (EFI_HOB_TYPE_UEFI_CAPSULE, HobPointer.Raw)) != NULL) {
    CapsuleHeader = (EFI_CAPSULE_HEADER *) (UINTN) HobPointer.Capsule->BaseAddress;

    //
    // Must be a nested FMP capsule or FMP capsule with pre-defined GUIDs
    //
    if (IsFmpGuidForMeUpdate (&CapsuleHeader->CapsuleGuid)) {
      CapsuleHeader = (EFI_CAPSULE_HEADER *)((UINT8 *)CapsuleHeader + CapsuleHeader->HeaderSize);
    }

    if (!CompareGuid (&gEfiFmpCapsuleGuid, &CapsuleHeader->CapsuleGuid)) {
      HobPointer.Raw = GET_NEXT_HOB (HobPointer);
      continue;
    }

    FmpCapsuleHeader = (EFI_FIRMWARE_MANAGEMENT_CAPSULE_HEADER *)((UINT8 *)CapsuleHeader + CapsuleHeader->HeaderSize);
    ItemOffsetList = (UINT64 *)(FmpCapsuleHeader + 1);
    for (Index = FmpCapsuleHeader->EmbeddedDriverCount; Index < (UINT32)FmpCapsuleHeader->EmbeddedDriverCount + FmpCapsuleHeader->PayloadItemCount; Index++) {
      ImageHeader = (EFI_FIRMWARE_MANAGEMENT_CAPSULE_IMAGE_HEADER *)((UINT8 *)FmpCapsuleHeader + ItemOffsetList[Index]);
      if (IsFmpGuidForMeUpdate (&ImageHeader->UpdateImageTypeId)) {
        DEBUG ((DEBUG_INFO, "A FMP capsule for Me update is detected.\n"));
        return TRUE;
      }
    }

    HobPointer.Raw = GET_NEXT_HOB (HobPointer);
  }

  return FALSE;
}
#endif

/**
  Update the ME Policy Library

  @retval EFI_SUCCESS             Update complete.
  @retval Others                  Update unsuccessful.
**/
EFI_STATUS
EFIAPI
UpdatePeiMePolicy (
  VOID
  )
{
  EFI_STATUS                      Status;
  ME_SETUP                        MeSetup;
  EFI_PEI_READ_ONLY_VARIABLE2_PPI *Variable;
  UINTN                           VariableSize;
#if FixedPcdGet8(PcdFspModeSelection) == 1
  VOID                            *FspsUpd;
#else
  SI_POLICY_PPI                   *SiPolicyPpi;
  ME_PEI_CONFIG                   *MePeiConfig;
#endif
#if FixedPcdGetBool(PcdCapsuleEnable) == 1
  EFI_HOB_GUID_TYPE               *GuidHob;
  BOOLEAN                         MeUpdateInProgress;

  GuidHob                 = NULL;
  MeUpdateInProgress      = FALSE;
#endif

  DEBUG ((DEBUG_INFO, "%a()\n", __FUNCTION__));

#if FixedPcdGet8(PcdFspModeSelection) == 1
  FspsUpd = NULL;
#else
  SiPolicyPpi = NULL;
  MePeiConfig = NULL;
#endif

#if FixedPcdGet8(PcdFspModeSelection) == 1
  FspsUpd = (FSPS_UPD *) PcdGet32 (PcdFspsUpdDataAddress);
  ASSERT (FspsUpd != NULL);
#else
  Status = PeiServicesLocatePpi (&gSiPolicyPpiGuid, 0, NULL, (VOID **) &SiPolicyPpi);
  ASSERT_EFI_ERROR (Status);

  Status = GetConfigBlock ((VOID *) SiPolicyPpi, &gMePeiConfigGuid, (VOID *) &MePeiConfig);
  ASSERT_EFI_ERROR (Status);
  if (EFI_ERROR (Status)) {
    return Status;
  }
#endif

  Status = PeiServicesLocatePpi (
             &gEfiPeiReadOnlyVariable2PpiGuid,
             0,
             NULL,
             (VOID **) &Variable
             );
  if (EFI_ERROR (Status)) {
    return Status;
  }

  VariableSize = sizeof (ME_SETUP);
  Status = Variable->GetVariable (
                       Variable,
                       L"MeSetup",
                       &gMeSetupVariableGuid,
                       NULL,
                       &VariableSize,
                       &MeSetup
                       );
  if (!EFI_ERROR (Status)) {
    COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.EndOfPostMessage,     MePeiConfig->EndOfPostMessage,     MeSetup.EndOfPostMessage,     NullIndex);
    COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.MeUnconfigOnRtcClear, MePeiConfig->MeUnconfigOnRtcClear, MeSetup.MeUnconfigOnRtcClear, NullIndex);
    COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.MctpBroadcastCycle,   MePeiConfig->MctpBroadcastCycle,   MeSetup.MctpBroadcastCycle,   NullIndex);
  }
  if (!PmcIsRtcBatteryGood ()) {
    //
    // For non coin battery design, this can be skipped.
    //
    UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.MeUnconfigOnRtcClear, MePeiConfig->MeUnconfigOnRtcClear, 2);
  }
#if FixedPcdGetBool(PcdCapsuleEnable) == 1
  GuidHob = GetFirstGuidHob (&gSysFwUpdateProgressGuid);
  if ((GuidHob != NULL) && \
      (((SYSTEM_FIRMWARE_UPDATE_PROGRESS *) GET_GUID_HOB_DATA (GuidHob))->Component == UpdatingMe)) {
    MeUpdateInProgress = TRUE;
  }

  if (((GetBootModeHob () == BOOT_ON_FLASH_UPDATE) && (IsFmpCapsuleForMeUpdateDetected ())) || \
      MeUpdateInProgress) {
    UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.EnforceEDebugMode, MePeiConfig->EnforceEDebugMode, 1);
  }
#endif

  return Status;
}

