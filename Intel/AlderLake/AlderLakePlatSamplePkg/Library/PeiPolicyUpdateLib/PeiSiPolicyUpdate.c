/** @file

;******************************************************************************
;* Copyright (c) 2020, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/
/** @file
  This file is SampleCode of the library for Intel Silicon PEI
  Platform Policy initialization.

@copyright
  INTEL CONFIDENTIAL
  Copyright 2014 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains a 'Sample Driver' and is licensed as such under the terms
  of your license agreement with Intel or your vendor. This file may be modified
  by the user, subject to the additional terms of the license agreement.

@par Specification Reference:
**/

#include "PeiSiPolicyUpdate.h"
#include <Library/DebugLib.h>
#include <Library/HobLib.h>
//[-start-210218-IB16740133-add]// for build error, it is used in Insyde solution (IB14630306).
#include <Library/MemoryAllocationLib.h>
//[-end-210218-IB16740133-add]//
#include <Library/PcdLib.h>
#include <Library/PeiServicesLib.h>
#include <Library/PeiSiPolicyUpdateLib.h>
#include <Library/SerialIoUartDebugPropertyPcdLib.h>
//[-start-200210-IB14630306-add]//
#include <Library/PeiOemSvcChipsetLib.h>
//[-end-200210-IB14630306-add]//
#include <Library/SerialPortParameterLib.h>
#include <Library/SiPolicyLib.h>
#include <Ppi/ReadOnlyVariable2.h>

#include <BootStateLib.h>
#include <PciePreMemConfig.h>
#include <PolicyUpdateMacro.h>
#include <Setup.h>
#include <SetupVariable.h>

#if FixedPcdGet8(PcdFspModeSelection) == 1
#include <FspmUpd.h>
#include <FspsUpd.h>
#include <Ppi/DebugEventHandler.h>
#endif

//[-start-210723-QINGLIN0001-add]//
#ifdef LCFC_SUPPORT
#include <Library/PciSegmentLib.h>
#endif
//[-end-210723-QINGLIN0001-add]//

//[-start-210723-QINGLIN0001-add]//
#ifdef S570_SUPPORT
SSID_SVID_PEI_CONFIG S570QXZSHostBridgeSsidTablePtr = {
   0x00, 0x00, 0x00, 0x380717AA
};

SSID_SVID_PEI_CONFIG S570QXZTHostBridgeSsidTablePtr = {
   0x00, 0x00, 0x00, 0x380417AA
};

SSID_SVID_PEI_CONFIG S570QY4THostBridgeSsidTablePtr = {
   0x00, 0x00, 0x00, 0x380217AA
};

SSID_SVID_PEI_CONFIG S570QY4YHostBridgeSsidTablePtr = {
   0x00, 0x00, 0x00, 0x380117AA
};
#endif

//[start-210819-STORM1106-modify]
#if defined(C770_SUPPORT)
SSID_SVID_PEI_CONFIG C7704621HostBridgeSsidTablePtr = {
   0x00, 0x00, 0x00, 0x381C17AA
};

SSID_SVID_PEI_CONFIG C7704641HostBridgeSsidTablePtr = {
   0x00, 0x00, 0x00, 0x381617AA
};

SSID_SVID_PEI_CONFIG C7704601HostBridgeSsidTablePtr = {
   0x00, 0x00, 0x00, 0x382817AA
};

// temporary , will remove below SSID porting after FVT phase which for ES2 CPU
SSID_SVID_PEI_CONFIG C770eSPIControllerESSsidTablePtr = {
   0x00, 0x1F, 0x00, 0x382417AA
};

#endif
//[end-210819-STORM1106-modify]

//[-end-210723-QINGLIN0001-add]//
//[-start-210804-QINGLIN0008-add]//
//[-start-211206-QINGLIN0126-modify]//
#ifdef S370_SUPPORT
SSID_SVID_PEI_CONFIG S370_4601_HostBridgeSsidTablePtr = {
   0x00, 0x00, 0x00, 0x380517AA
};

SSID_SVID_PEI_CONFIG S370_4609_HostBridgeSsidTablePtr = {
   0x00, 0x00, 0x00, 0x380317AA
};

SSID_SVID_PEI_CONFIG S370_4619_HostBridgeSsidTablePtr = {
   0x00, 0x00, 0x00, 0x380217AA
};
#endif
//[-end-211206-QINGLIN0126-modify]//
//[-end-210804-QINGLIN0008-add]//

//[-start-210909-TAMT000007-add]//
#ifdef S77014_SUPPORT
SSID_SVID_PEI_CONFIG S770_14_4621_HostBridgeSsidTablePtr = { //QXZT ES2 J-0
   0x00, 0x00, 0x00, 0x380717AA
};

SSID_SVID_PEI_CONFIG S770_14_4641_HostBridgeSsidTablePtr = { //QXZS ES2 J-0
   0x00, 0x00, 0x00, 0x380A17AA
};
#endif
//[-end-210909-TAMT000007-add]//

//[-start-220318-Ching000036-add]//
#ifdef S77014IAH_SUPPORT
SSID_SVID_PEI_CONFIG S770_14IAH_4621_HostBridgeSsidTablePtr = { //Q07D SQS
   0x00, 0x00, 0x00, 0x380917AA
};

SSID_SVID_PEI_CONFIG S770_14IAH_4641_HostBridgeSsidTablePtr = { //Q07G SQS
   0x00, 0x00, 0x00, 0x381017AA
};

SSID_SVID_PEI_CONFIG S770_14IAH_4649_HostBridgeSsidTablePtr = { //Q07F SQS
   0x00, 0x00, 0x00, 0x380217AA
};
#endif
//[-end-220318-Ching000036-add]//

//[-start-210925-TAMT000014-add]//
#ifdef S77013_SUPPORT
SSID_SVID_PEI_CONFIG S770_13_4621_HostBridgeSsidTablePtr = { //QXZT ES2 J-0
   0x00, 0x00, 0x00, 0x380D17AA
};

SSID_SVID_PEI_CONFIG S770_13_4641_HostBridgeSsidTablePtr = { //QXZS ES2 J-0
   0x00, 0x00, 0x00, 0x380B17AA
};
#endif
//[-end-210925-TAMT000014-add]//
/**
  This function performs Initinalize global variable for PolicyDefaultCheck using

  @retval EFI_SUCCESS      The function completed successfully
**/
EFI_STATUS
EFIAPI
PolicyDefaultCheckInit (
  )
{
  DEBUG ((DEBUG_INFO, "PolicyDefaultCheckInit Entry\n"));
  //
  // Check FirstBoot and RvpSupport for IsPolicyDefaultCheckRequired using
  //
  if (IsBootStatePresent()) {
    PcdSetBoolS (PcdPolicyCheckIsFirstBoot, 0);
  } else {
    PcdSetBoolS (PcdPolicyCheckIsFirstBoot, 1);
  }
  if ((PcdGet16 (PcdBoardId)) == (PcdGet16 (PcdDefaultBoardId))) {
    PcdSetBoolS (PcdPolicyCheckIsRvpSupport, 1);
  } else {
    PcdSetBoolS (PcdPolicyCheckIsRvpSupport, 0);
  }
  PcdSetBoolS (PcdPolicyCheckPcdInitDone, 1);
  DEBUG ((DEBUG_INFO, "IsFirstBoot %x\n", PcdGetBool (PcdPolicyCheckIsFirstBoot)));
  DEBUG ((DEBUG_INFO, "IsRvpSupport %x\n", PcdGetBool (PcdPolicyCheckIsRvpSupport)));
  DEBUG ((DEBUG_INFO, "PolicyDefaultCheckInitDone %x\n", PcdGetBool (PcdPolicyCheckPcdInitDone)));
  DEBUG ((DEBUG_INFO, "PolicyDefaultCheckInit Exist\n"));
  return EFI_SUCCESS;
}

/**
  This function performs Check First Boot and Rvp Support to see if Policy Default Check Required

  @retval BOOLEAN         Required TURE or FALSE
**/
BOOLEAN
EFIAPI
IsPolicyDefaultCheckRequired (
  )
{
  //
  // Check PolicyDefaultCheckInit already been done
  //
  if (!PcdGetBool (PcdPolicyCheckPcdInitDone)) {
    PolicyDefaultCheckInit();
  }
  //
  // Policy Default Check only for First Boot and Rvp Support
  //
  if ((PcdGetBool (PcdPolicyCheckIsFirstBoot)) && (PcdGetBool (PcdPolicyCheckIsRvpSupport))) {
    return TRUE;
  } else {
    return FALSE;
  }
}

/**
  This function performs Silicon PEI Policy initialization.

  @retval EFI_SUCCESS             The PPI is installed and initialized.
  @retval EFI_NOT_FOUND           The FspmUpd is not found.
  @retval EFI_OUT_OF_RESOURCES    Do not have enough resources to initialize the driver
**/
EFI_STATUS
EFIAPI
UpdatePeiSiPolicyPreMem (
  VOID
  )
{
  EFI_STATUS                        Status;
  EFI_PEI_READ_ONLY_VARIABLE2_PPI   *VariableServices;
  UINTN                             VariableSize;
  SI_SETUP                          SiSetup;
#if FixedPcdGet8(PcdFspModeSelection) == 1

  VOID                              *FspmUpd;
  EFI_PEI_HOB_POINTERS              Hob;
  DEBUG_CONFIG_DATA_HOB             *DebugConfigData;
  UINT8                             DebugInterfaces;
  DEBUG_EVENT_HABDLER_PPI           *DebugEventHandlerPpi;
#else
  SI_PREMEM_POLICY_PPI              *SiPreMemPolicy;
  SI_PREMEM_CONFIG                  *SiPreMemConfig;
  PCIE_PREMEM_CONFIG                *PciePreMemConfig;

  SiPreMemPolicy = NULL;
  SiPreMemConfig = NULL;
  PciePreMemConfig = NULL;
#endif

  DEBUG ((DEBUG_INFO, "Update PeiSiPolicyUpdate Pre-Mem Start\n"));

#if FixedPcdGet8(PcdFspModeSelection) == 1
  FspmUpd = NULL;
  FspmUpd = (FSPM_UPD *) PcdGet32 (PcdFspmUpdDataAddress);
  ASSERT (FspmUpd != NULL);
  if (FspmUpd == NULL) {
    return EFI_NOT_FOUND;
  }
  ((FSPM_UPD *)FspmUpd)->FspmArchUpd.StackBase = (VOID *) (UINTN) (PcdGet32 (PcdTemporaryRamBase) + PcdGet32 (PcdTemporaryRamSize) - (PcdGet32 (PcdFspTemporaryRamSize) + PcdGet32 (PcdFspReservedBufferSize)));
  ((FSPM_UPD *)FspmUpd)->FspmArchUpd.StackSize = PcdGet32 (PcdFspTemporaryRamSize);

  Status = PeiServicesGetBootMode (&(((FSPM_UPD *)FspmUpd)->FspmArchUpd.BootMode));
  if (EFI_ERROR (Status)) {
    ((FSPM_UPD *)FspmUpd)->FspmArchUpd.BootMode = BOOT_WITH_FULL_CONFIGURATION;
  }
  ((FSPM_UPD *)FspmUpd)->FspmArchUpd.BootLoaderTolumSize = 0x0;

  Status = PeiServicesLocatePpi (
           &gDebugEventHandlerPpiGuid,       // GUID
           0,                                // INSTANCE
           NULL,                             // EFI_PEI_PPI_DESCRIPTOR
           (VOID **) &DebugEventHandlerPpi   // PPI
           );
  if (!EFI_ERROR(Status)) {
    ((FSPM_UPD *)FspmUpd)->FspmArchUpd.FspEventHandler = (FSP_EVENT_HANDLER *)(UINT32) DebugEventHandlerPpi->DebugEventHandler;
  }

  //
  // Initialize DebugConfigData
  //
  DebugInterfaces = 0;
  Hob.Guid = GetFirstGuidHob (&gDebugConfigHobGuid);
  if (Hob.Guid != NULL) {
    DebugConfigData = (DEBUG_CONFIG_DATA_HOB *) GET_GUID_HOB_DATA (Hob.Guid);
    if (DebugConfigData != NULL) {
      // Debug Interfaces
      if (DebugConfigData->RamDebugInterface)      { DebugInterfaces |= STATUS_CODE_USE_RAM; }
      if (DebugConfigData->UartDebugInterface)     { DebugInterfaces |= STATUS_CODE_USE_ISA_SERIAL; }
      if (DebugConfigData->Usb3DebugInterface)     { DebugInterfaces |= STATUS_CODE_USE_USB3; }
      if (DebugConfigData->TraceHubDebugInterface) { DebugInterfaces |= STATUS_CODE_USE_TRACEHUB; }
      ((FSPM_UPD *)FspmUpd)->FspmConfig.PcdDebugInterfaceFlags  = DebugInterfaces;
      // Serial debug message baud rate
      ((FSPM_UPD *)FspmUpd)->FspmConfig.PcdSerialDebugBaudRate  = DebugConfigData->SerialDebugBaudRate;
      // Serial debug message level
      ((FSPM_UPD *)FspmUpd)->FspmConfig.PcdSerialDebugLevel     = DebugConfigData->SerialDebug;
      // MRC Serial debug message level
      ((FSPM_UPD *)FspmUpd)->FspmConfig.SerialDebugMrcLevel     = DebugConfigData->SerialDebugMrcLevel;
      if (DebugConfigData->SerialIoDebugInterface) {
        ((FSPM_UPD *)FspmUpd)->FspmConfig.PcdDebugInterfaceFlags            |= STATUS_CODE_USE_SERIALIO;
        ((FSPM_UPD *)FspmUpd)->FspmConfig.SerialIoUartDebugControllerNumber = DebugConfigData->SerialIoUartDebugControllerNumber;
        ((FSPM_UPD *)FspmUpd)->FspmConfig.SerialIoUartDebugBaudRate         = DebugConfigData->SerialIoUartDebugBaudRate;
        ((FSPM_UPD *)FspmUpd)->FspmConfig.SerialIoUartDebugStopBits         = DebugConfigData->SerialIoUartDebugStopBits;
        ((FSPM_UPD *)FspmUpd)->FspmConfig.SerialIoUartDebugParity           = DebugConfigData->SerialIoUartDebugParity;
        ((FSPM_UPD *)FspmUpd)->FspmConfig.SerialIoUartDebugAutoFlow         = DebugConfigData->SerialIoUartDebugFlowControl;
        ((FSPM_UPD *)FspmUpd)->FspmConfig.SerialIoUartDebugDataBits         = DebugConfigData->SerialIoUartDebugDataBits;
        // @todo - consider adding GPIO muxes to Setup option
        ((FSPM_UPD *)FspmUpd)->FspmConfig.SerialIoUartDebugRxPinMux         = 0x0;
        ((FSPM_UPD *)FspmUpd)->FspmConfig.SerialIoUartDebugTxPinMux         = 0x0;
        ((FSPM_UPD *)FspmUpd)->FspmConfig.SerialIoUartDebugRtsPinMux        = 0x0;
        ((FSPM_UPD *)FspmUpd)->FspmConfig.SerialIoUartDebugCtsPinMux        = 0x0;
        ((FSPM_UPD *)FspmUpd)->FspmConfig.SerialIoUartDebugMode             = 0x2; //SerialIoUartHidden
      }
    }
  }
  DEBUG ((DEBUG_INFO, "FspmUpd->FspmConfig.PcdDebugInterfaceFlags is 0x%X\n",             ((FSPM_UPD *)FspmUpd)->FspmConfig.PcdDebugInterfaceFlags));
  DEBUG ((DEBUG_INFO, "FspmUpd->FspmConfig.PcdSerialDebugBaudRate is 0x%X\n",             ((FSPM_UPD *)FspmUpd)->FspmConfig.PcdSerialDebugBaudRate));
  DEBUG ((DEBUG_INFO, "FspmUpd->FspmConfig.PcdSerialDebugLevel is 0x%X\n",                ((FSPM_UPD *)FspmUpd)->FspmConfig.PcdSerialDebugLevel));
  DEBUG ((DEBUG_INFO, "FspmUpd->FspmConfig.SerialDebugMrcLevel is 0x%X\n",                ((FSPM_UPD *)FspmUpd)->FspmConfig.SerialDebugMrcLevel));
  DEBUG ((DEBUG_INFO, "FspmUpd->FspmConfig.SerialIoUartDebugControllerNumber is 0x%X\n",  ((FSPM_UPD *)FspmUpd)->FspmConfig.SerialIoUartDebugControllerNumber));
  DEBUG ((DEBUG_INFO, "FspmUpd->FspmConfig.SerialIoUartDebugBaudRate is %d\n",            ((FSPM_UPD *)FspmUpd)->FspmConfig.SerialIoUartDebugBaudRate));
#else
  Status = PeiServicesLocatePpi (&gSiPreMemPolicyPpiGuid, 0, NULL, (VOID **) &SiPreMemPolicy);
  ASSERT_EFI_ERROR (Status);
  Status = GetConfigBlock ((VOID *) SiPreMemPolicy, &gSiPreMemConfigGuid, (VOID *) &SiPreMemConfig);
  ASSERT_EFI_ERROR (Status);
  Status = GetConfigBlock((VOID *)SiPreMemPolicy, &gPciePreMemConfigGuid, (VOID *)&PciePreMemConfig);
  ASSERT_EFI_ERROR(Status);
#endif

  //
  // Retrieve Setup variable
  //
  Status = PeiServicesLocatePpi (
             &gEfiPeiReadOnlyVariable2PpiGuid, // GUID
             0,                                // INSTANCE
             NULL,                             // EFI_PEI_PPI_DESCRIPTOR
             (VOID **) &VariableServices       // PPI
             );
  ASSERT_EFI_ERROR (Status);

  VariableSize = sizeof (SI_SETUP);
  Status = VariableServices->GetVariable (
                               VariableServices,
                               L"SiSetup",
                               &gSiSetupVariableGuid,
                               NULL,
                               &VariableSize,
                               &SiSetup
                               );
  ASSERT_EFI_ERROR (Status);

  if (Status == EFI_SUCCESS) {
    COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.PlatformDebugConsent, SiPreMemConfig->PlatformDebugConsent,     SiSetup.PlatformDebugConsent, NullIndex);
    COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.PcieImrEnabled,       PciePreMemConfig->PcieImr.ImrEnabled,     SiSetup.PcieImrEnable,        NullIndex);
    if (SiSetup.PcieImrEnable) {
      COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.PcieImrRpLocation,  PciePreMemConfig->PcieImr.ImrRpLocation,  SiSetup.PcieImrRpLocation,    NullIndex);
      COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.PcieImrRpSelection, PciePreMemConfig->PcieImr.ImrRpSelection, SiSetup.PcieImrRpSelection,   NullIndex);
      COMPARE_AND_UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.PcieImrSize,        PciePreMemConfig->PcieImr.ImrSize,        SiSetup.PcieImrSize,          NullIndex);
    }
  }

  //
  // Policy to prevent MRC modify the boot mode while in flash update
  //
  UPDATE_POLICY (((FSPM_UPD *) FspmUpd)->FspmConfig.SiSkipOverrideBootModeWhenFwUpdate,  SiPreMemConfig->SkipOverrideBootModeWhenFwUpdate, (UINT8) FALSE);

  return EFI_SUCCESS;
}

/*
STATIC SVID_SID_INIT_ENTRY mSsidTablePtr[SI_MAX_DEVICE_COUNT] = {0};
*/

/**
  This provide sample code to update SSID table policy.

  @param[in] FspsUpd/SiConfig  FSPSUPD structure or SICONFIG policy
**/
STATIC
VOID
UpdateSsidPolicy (
#if FixedPcdGet8(PcdFspModeSelection) == 1
  FSPS_UPD   *FspsUpd
#else
  SI_CONFIG  *SiConfig
#endif
  )
{
/*
  Below provides the sample code to update SSID table.
  This is valid when SkipSsidProgramming is FALSE;
  It doesn't need to contain entries for all Intel internal devices.
  It can only contains the SVID_SID_INIT_ENTRY entries for those Dev# Func# which needs
  to be override.
  In the enties, only Dev, Function, SubSystemVendorId, and SubSystemId are required.

  E.g. Platform only needs to override BDF 0:31:5 to AAAA:BBBB and BDF 0:31:3 to CCCC:DDDD,
  it can be done in platform like this:
*/
/*
  UINT32                             EntryCount;

  UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.SiSkipSsidProgramming,  *(UINT32 *)SiConfig->SkipSsidProgramming, FALSE);
  UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.SiCustomizedSvid,       *(UINT32 *)SiConfig->CustomizedSvid, 0x1234);
  UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.SiCustomizedSsid,       *(UINT32 *)SiConfig->CustomizedSsid, 0x5678);

  EntryCount = 0;
  mSsidTablePtr[EntryCount].Address.Bits.Device   = SpiDeviceNumber ();
  mSsidTablePtr[EntryCount].Address.Bits.Function = SpiFunctionNumber ();
  mSsidTablePtr[EntryCount].SvidSidValue.SubSystemVendorId = 0xAAAA;
  mSsidTablePtr[EntryCount].SvidSidValue.SubSystemId       = 0xBBBB;
  EntryCount ++;
  mSsidTablePtr[EntryCount].Address.Bits.Device   = HdaDevNumber ();
  mSsidTablePtr[EntryCount].Address.Bits.Function = HdaFuncNumber ();
  mSsidTablePtr[EntryCount].SvidSidValue.SubSystemVendorId = 0xCCCC;
  mSsidTablePtr[EntryCount].SvidSidValue.SubSystemId       = 0xDDDD;
  EntryCount ++;
  ASSERT (EntryCount < SI_MAX_DEVICE_COUNT);

  UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.SiSsidTablePtr,           *(UINT32 *)SiConfig->SsidTablePtr, (UINTN) mSsidTablePtr);
  UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.SiNumberOfSsidTableEntry, SiConfig->NumberOfSsidTableEntry,  (UINT16) EntryCount);
*/

//[-start-200210-IB14630306-add]//
  UINT32                   DefaultSsidSvid;
  SSID_SVID_PEI_CONFIG     *CustomizedSsidSvidTable;
  UINTN                    CustomizedSsidSvidTableCount;
  SVID_SID_INIT_ENTRY      *SsidTablePtr;
  UINT16                   Index;

  UPDATE_POLICY (((FSPS_UPD *)FspsUpd)->FspsConfig.SiSkipSsidProgramming, SiConfig->SkipSsidProgramming, FALSE);
  DefaultSsidSvid = PcdGet32 (PcdDefaultSsidSvid);
  UPDATE_POLICY (((FSPS_UPD *)FspsUpd)->FspsConfig.SiCustomizedSvid, SiConfig->CustomizedSvid, (UINT16)(DefaultSsidSvid & 0xFFFF));
  UPDATE_POLICY (((FSPS_UPD *)FspsUpd)->FspsConfig.SiCustomizedSsid, SiConfig->CustomizedSsid, (UINT16)((DefaultSsidSvid >> 16) & 0xFFFF));
  CustomizedSsidSvidTable = (SSID_SVID_PEI_CONFIG *)PcdGetPtr (PcdDefaultSsidSvidPeiTable);
  CustomizedSsidSvidTableCount = PcdGetSize (PcdDefaultSsidSvidPeiTable) / sizeof (SSID_SVID_PEI_CONFIG);
//[-start-210723-QINGLIN0001-modify]//
//[-start-211015-QINGLIN0100-modify]//
#if defined(S570_SUPPORT)
{
    UINT16                      DeviceId;
    BOOLEAN                     IsFound = FALSE;

    DeviceId = PciSegmentRead16 (PCI_SEGMENT_LIB_ADDRESS (0, 0, 0, 0, 0x02));
    if (DeviceId == 0x4641) { //ES2:QXZS  QS:QYY8
      *(CustomizedSsidSvidTable + CustomizedSsidSvidTableCount) = S570QXZSHostBridgeSsidTablePtr;
      IsFound = TRUE;
    }
    else if (DeviceId == 0x4621) { //ES2:QXZT QS:QYY9 QYYD
      *(CustomizedSsidSvidTable + CustomizedSsidSvidTableCount) = S570QXZTHostBridgeSsidTablePtr;
      IsFound = TRUE;
    }
    else if (DeviceId == 0x4601) { //QY4T QY4U
      *(CustomizedSsidSvidTable + CustomizedSsidSvidTableCount) = S570QY4THostBridgeSsidTablePtr;
      IsFound = TRUE;
    }
    else if (DeviceId == 0x4609) { //QY4Y
      *(CustomizedSsidSvidTable + CustomizedSsidSvidTableCount) = S570QY4YHostBridgeSsidTablePtr;
      IsFound = TRUE;
    }
    if (IsFound == TRUE) {
     CustomizedSsidSvidTableCount += 1;
    }
}
#endif
//[-end-211015-QINGLIN0100-modify]//
//[-end-210723-QINGLIN0001-modify]//
//[-start-210804-QINGLIN0008-add]//
//[-start-211206-QINGLIN0126-modify]//
#if defined(S370_SUPPORT)
  {
      UINT16                      DeviceId;
      BOOLEAN                     IsFound = FALSE;

      DeviceId = PciSegmentRead16 (PCI_SEGMENT_LIB_ADDRESS (0, 0, 0, 0, 0x02));
      if (DeviceId == 0x4601) {
        *(CustomizedSsidSvidTable + CustomizedSsidSvidTableCount) = S370_4601_HostBridgeSsidTablePtr;
        IsFound = TRUE;
      }
      else if (DeviceId == 0x4609) {
        *(CustomizedSsidSvidTable + CustomizedSsidSvidTableCount) = S370_4609_HostBridgeSsidTablePtr;
        IsFound = TRUE;
      }
      else if (DeviceId == 0x4619) {
        *(CustomizedSsidSvidTable + CustomizedSsidSvidTableCount) = S370_4619_HostBridgeSsidTablePtr;
        IsFound = TRUE;
      }
      if (IsFound == TRUE) {
       CustomizedSsidSvidTableCount += 1;
      }
  }
#endif
//[-end-211206-QINGLIN0126-modify]//

//[start-210819-STORM1106-modify]
#if defined(C770_SUPPORT)
{
    UINT32                      VendorDeviceId;
    BOOLEAN                     IsFound = FALSE;

    VendorDeviceId = PciSegmentRead32 (PCI_SEGMENT_LIB_ADDRESS (0, 0, 0, 0, 0x00));
    if (0x46218086 == VendorDeviceId) {
      *(CustomizedSsidSvidTable + CustomizedSsidSvidTableCount) = C7704621HostBridgeSsidTablePtr;
      IsFound = TRUE;
    }
    else if (0x46418086 == VendorDeviceId) {
      *(CustomizedSsidSvidTable + CustomizedSsidSvidTableCount) = C7704641HostBridgeSsidTablePtr;
      IsFound = TRUE;
    }
    else if (0x46018086 == VendorDeviceId) {
      *(CustomizedSsidSvidTable + CustomizedSsidSvidTableCount) = C7704601HostBridgeSsidTablePtr;
      IsFound = TRUE;
    }
    if (IsFound == TRUE) {
     CustomizedSsidSvidTableCount += 1;
    }

    IsFound = FALSE;

// eSPI controller
  VendorDeviceId = PciSegmentRead32 (PCI_SEGMENT_LIB_ADDRESS (0, 0, 0x1F, 0, 0x00));
  if (0x51818086 == VendorDeviceId) {
    *(CustomizedSsidSvidTable + CustomizedSsidSvidTableCount) = C770eSPIControllerESSsidTablePtr;
    IsFound = TRUE;
  }
  if (IsFound == TRUE) {
   CustomizedSsidSvidTableCount += 1;
  }
}
#endif
//[end-210819-STORM1106-modify]

//[-end-210804-QINGLIN0008-add]//

//[-start-210909-TAMT000007-add]//
#ifdef S77014_SUPPORT
{
	UINT32					  VendorDeviceId;
	BOOLEAN					  IsFound = FALSE;
  
	VendorDeviceId = PciSegmentRead32 (PCI_SEGMENT_LIB_ADDRESS (0, 0, 0, 0, 0x00));
	if (0x46218086 == VendorDeviceId) {
      *(CustomizedSsidSvidTable + CustomizedSsidSvidTableCount) = S770_14_4621_HostBridgeSsidTablePtr;
	  IsFound = TRUE;
	}
	else if (0x46418086 == VendorDeviceId) {
	  *(CustomizedSsidSvidTable + CustomizedSsidSvidTableCount) = S770_14_4641_HostBridgeSsidTablePtr;
	  IsFound = TRUE;
	}
	if (IsFound == TRUE) {
	 CustomizedSsidSvidTableCount += 1;
	}
  }
#endif
//[-end-210909-TAMT000007-add]//

//[-start-220318-Ching000036-add]//
#ifdef S77014IAH_SUPPORT
{
    UINT32					  VendorDeviceId;
    BOOLEAN					  IsFound = FALSE;

    VendorDeviceId = PciSegmentRead32 (PCI_SEGMENT_LIB_ADDRESS (0, 0, 0, 0, 0x00));
    if (0x46218086 == VendorDeviceId) {
      *(CustomizedSsidSvidTable + CustomizedSsidSvidTableCount) = S770_14IAH_4621_HostBridgeSsidTablePtr;
      IsFound = TRUE;
    }
    else if (0x46418086 == VendorDeviceId) {
      *(CustomizedSsidSvidTable + CustomizedSsidSvidTableCount) = S770_14IAH_4641_HostBridgeSsidTablePtr;
      IsFound = TRUE;
    }
    else if (0x46498086 == VendorDeviceId) {
      *(CustomizedSsidSvidTable + CustomizedSsidSvidTableCount) = S770_14IAH_4649_HostBridgeSsidTablePtr;
      IsFound = TRUE;
    }
    if (IsFound == TRUE) {
      CustomizedSsidSvidTableCount += 1;
    }
  }
#endif
//[-end-220318-Ching000036-add]//

//[-start-210925-TAMT000014-add]//
#ifdef S77013_SUPPORT
  {
	  UINT32					VendorDeviceId;
	  BOOLEAN					IsFound = FALSE;
	
	  VendorDeviceId = PciSegmentRead32 (PCI_SEGMENT_LIB_ADDRESS (0, 0, 0, 0, 0x00));
	  if (0x46218086 == VendorDeviceId) {
		*(CustomizedSsidSvidTable + CustomizedSsidSvidTableCount) = S770_13_4621_HostBridgeSsidTablePtr;
		IsFound = TRUE;
	  }
	  else if (0x46418086 == VendorDeviceId) {
		*(CustomizedSsidSvidTable + CustomizedSsidSvidTableCount) = S770_13_4641_HostBridgeSsidTablePtr;
		IsFound = TRUE;
	  }
	  if (IsFound == TRUE) {
	   CustomizedSsidSvidTableCount += 1;
	  }
	}
#endif
//[-end-210925-TAMT000014-add]//
  SsidTablePtr = AllocateZeroPool (CustomizedSsidSvidTableCount * sizeof (SVID_SID_INIT_ENTRY));
  for (Index = 0; Index < CustomizedSsidSvidTableCount; Index++) {
    SsidTablePtr[Index].Address.Bits.Bus               = CustomizedSsidSvidTable[Index].Bus;
    SsidTablePtr[Index].Address.Bits.Device            = CustomizedSsidSvidTable[Index].Device;
    SsidTablePtr[Index].Address.Bits.Function          = CustomizedSsidSvidTable[Index].Function;
    SsidTablePtr[Index].SvidSidValue.SubSystemVendorId = (UINT16)(CustomizedSsidSvidTable[Index].SsidSvid & 0xFFFF);
    SsidTablePtr[Index].SvidSidValue.SubSystemId       = (UINT16)((CustomizedSsidSvidTable[Index].SsidSvid >> 16) & 0xFFFF);
  }
//[-start-200226-IB14630337-modify]//
#if FixedPcdGet8(PcdFspModeSelection) == 1
  UPDATE_POLICY (((FSPS_UPD *)FspsUpd)->FspsConfig.SiSsidTablePtr,           *(UINT32 *)SiConfig->SsidTablePtr, (UINTN)SsidTablePtr);
#else
  UPDATE_POLICY (((FSPS_UPD *)FspsUpd)->FspsConfig.SiSsidTablePtr,           SiConfig->SsidTablePtr, (UINT32 *)SsidTablePtr);
#endif
//[-end-200226-IB14630337-modify]//
  UPDATE_POLICY (((FSPS_UPD *)FspsUpd)->FspsConfig.SiNumberOfSsidTableEntry, SiConfig->NumberOfSsidTableEntry,  (UINT16)CustomizedSsidSvidTableCount);
//[-end-200210-IB14630306-add]//
}

/**
  This function performs Silicon PEI Policy initialization.

  @retval EFI_SUCCESS  The function completed successfully
**/
EFI_STATUS
EFIAPI
UpdatePeiSiPolicy (
  VOID
  )
{
  EFI_STATUS                         Status;
#if FixedPcdGet8(PcdFspModeSelection) == 1
  VOID                               *FspsUpd;
  DEBUG_EVENT_HABDLER_PPI            *DebugEventHandlerPpi;
#else
  SI_POLICY_PPI                      *SiPolicyPpi;
  SI_CONFIG                          *SiConfig;
#endif

  DEBUG ((DEBUG_INFO, "Update PeiSiPolicyUpdate Pos-Mem Start\n"));

  Status = EFI_SUCCESS;
#if FixedPcdGet8(PcdFspModeSelection) == 1
  FspsUpd = NULL;
#else
  SiPolicyPpi     = NULL;
  SiConfig        = NULL;
#endif

#if FixedPcdGet8(PcdFspModeSelection) == 1
  FspsUpd = (FSPS_UPD *) PcdGet32 (PcdFspsUpdDataAddress);
  ASSERT (FspsUpd != NULL);
#else
  Status = PeiServicesLocatePpi (&gSiPolicyPpiGuid, 0, NULL, (VOID **) &SiPolicyPpi);
  ASSERT_EFI_ERROR (Status);

  Status = GetConfigBlock ((VOID *) SiPolicyPpi, &gSiConfigGuid, (VOID *) &SiConfig);
  ASSERT_EFI_ERROR (Status);
#endif

  UpdateSsidPolicy (
#if FixedPcdGet8(PcdFspModeSelection) == 1
    FspsUpd
#else
    SiConfig
#endif
    );

#if FixedPcdGet8(PcdFspModeSelection) == 1
  Status = PeiServicesLocatePpi (
           &gDebugEventHandlerPpiGuid,       // GUID
           0,                                // INSTANCE
           NULL,                             // EFI_PEI_PPI_DESCRIPTOR
           (VOID **) &DebugEventHandlerPpi   // PPI
           );
  if (!EFI_ERROR(Status)) {
    ((FSPS_UPD *) FspsUpd)->FspsConfig.FspEventHandler = (UINT32) DebugEventHandlerPpi->DebugEventHandler;
  }
#endif

#if FixedPcdGet8(PcdEmbeddedEnable) == 0x1
  UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.SiSkipBiosDoneWhenFwUpdate, SiConfig->SkipBiosDoneWhenFwUpdate, (UINT8) FALSE);
#endif

  return EFI_SUCCESS;
}

