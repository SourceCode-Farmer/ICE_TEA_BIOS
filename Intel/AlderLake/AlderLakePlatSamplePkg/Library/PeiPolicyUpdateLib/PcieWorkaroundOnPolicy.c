/** @file
  This file is SampleCode for Intel SA Workaround on Policy initialization.

@copyright
  INTEL CONFIDENTIAL
  Copyright 1999 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains a 'Sample Driver' and is licensed as such under the terms
  of your license agreement with Intel or your vendor. This file may be modified
  by the user, subject to the additional terms of the license agreement.

@par Specification Reference:
**/

#include <Library/PcieWorkaroundOnPolicy.h>
#if FixedPcdGet8(PcdFspModeSelection) == 1
#include <FspmUpd.h>
#endif

/**
  PCIe GPIO Write

  @param[in] Gpio        - GPIO Number
  @param[in] Active      - GPIO Active Information; High/Low
  @param[in] Level       - Write GPIO value (0/1)

**/
VOID
PcieGpioWrite (
  IN  UINT32                Gpio,
  IN  BOOLEAN               Active,
  IN  BOOLEAN               Level
  )
{
  EFI_STATUS  Status;

  if (Active == 0) {
    Level = (~Level) & 0x1;
  }
  Status = GpioSetOutputValue(Gpio, (UINT32)Level);
  if (Status != EFI_SUCCESS) {
    return;
  }
}


/**
  PcieCardResetWorkAround performs PCIe Card reset on root port

  @retval EFI_SUCCESS              The policy is installed and initialized.
**/
EFI_STATUS
EFIAPI
PcieCardResetWorkAround (
  VOID
  )
{
#if FixedPcdGet8(PcdFspModeSelection) == 1
  VOID                            *FspmUpd;
#else
  EFI_STATUS                      Status;
  SI_PREMEM_POLICY_PPI            *SiPreMemPolicyPpi;
  SA_MISC_PEI_PREMEM_CONFIG       *MiscPeiPreMemConfig;
  HYBRID_GRAPHICS_CONFIG          *HgGpioData;
  Status = EFI_SUCCESS;
#endif

#if FixedPcdGet8(PcdFspModeSelection) == 1
  FspmUpd = NULL;
#else
  HgGpioData          = NULL;
  SiPreMemPolicyPpi   = NULL;
  MiscPeiPreMemConfig = NULL;
#endif

#if FixedPcdGet8(PcdFspModeSelection) == 1
  FspmUpd = (FSPM_UPD *) PcdGet32 (PcdFspmUpdDataAddress);
  ASSERT (FspmUpd != NULL);
#else
  Status = PeiServicesLocatePpi (&gSiPreMemPolicyPpiGuid, 0, NULL, (VOID **) &SiPreMemPolicyPpi);
  ASSERT_EFI_ERROR (Status);

  Status = GetConfigBlock((VOID *)SiPreMemPolicyPpi, &gSaMiscPeiPreMemConfigGuid, (VOID *)&MiscPeiPreMemConfig);
  ASSERT_EFI_ERROR(Status);

  Status = GetConfigBlock((VOID *)SiPreMemPolicyPpi, &gHybridGraphicsConfigGuid, (VOID *)&HgGpioData);
  ASSERT_EFI_ERROR(Status);
#endif

#if FixedPcdGet8(PcdFspModeSelection) == 1
  if (((FSPM_UPD *) FspmUpd)->FspmConfig.CpuPcie0Rtd3Gpio[20] != NotSupported) {
#else
  if (HgGpioData->CpuPcie0Rtd3Gpio.GpioSupport != NotSupported) {
#endif
    ///
    /// dGPU is present.
    ///      If PCIe Mode or HG Muxless
    ///              Power on MXM
    ///              Configure GPIOs to drive MXM in PCIe mode or HG Muxless
    ///      else
    ///              Do Nothing
    ///
#if FixedPcdGet8(PcdFspModeSelection) == 0
    if ((HgGpioData->HgMode == HgModeMuxless) ||
        (HgGpioData->HgMode == HgModeDgpu)) {
#endif
      DEBUG((DEBUG_INFO, "Configure GPIOs for driving the dGPU.\n"));
      ///
      ///  Drive DGPU HOLD RST Enable to make sure we hold reset
      ///
      PcieGpioWrite (
#if FixedPcdGet8(PcdFspModeSelection) == 1
        ((FSPM_UPD *) FspmUpd)->FspmConfig.CpuPcie0Rtd3Gpio[4],
        (BOOLEAN)((FSPM_UPD *) FspmUpd)->FspmConfig.CpuPcie0Rtd3Gpio[1],
#else
        HgGpioData->CpuPcie0Rtd3Gpio.HoldRst.GpioNo,
        HgGpioData->CpuPcie0Rtd3Gpio.HoldRst.Active,
#endif
        GP_ENABLE
        );
      ///
      /// wait 100ms
      ///
#if FixedPcdGet8(PcdFspModeSelection) == 1
      MicroSecondDelay((((FSPM_UPD *) FspmUpd)->FspmConfig.HgDelayAfterHoldReset) * STALL_ONE_MILLI_SECOND);
#else
      MicroSecondDelay((HgGpioData->HgDelayAfterHoldReset) * STALL_ONE_MILLI_SECOND);
#endif

      ///
      /// Drive DGPU PWR EN to Power On MXM
      ///
      PcieGpioWrite (
#if FixedPcdGet8(PcdFspModeSelection) == 1
        ((FSPM_UPD *) FspmUpd)->FspmConfig.CpuPcie0Rtd3Gpio[12],
        (BOOLEAN)((FSPM_UPD *) FspmUpd)->FspmConfig.CpuPcie0Rtd3Gpio[9],
#else
        HgGpioData->CpuPcie0Rtd3Gpio.PwrEnable.GpioNo,
        HgGpioData->CpuPcie0Rtd3Gpio.PwrEnable.Active,
#endif
        GP_ENABLE
        );
      ///
      /// wait 300ms
      ///
#if FixedPcdGet8(PcdFspModeSelection) == 1
      MicroSecondDelay((((FSPM_UPD *) FspmUpd)->FspmConfig.HgDelayAfterPwrEn) * STALL_ONE_MILLI_SECOND);
#else
      MicroSecondDelay((HgGpioData->HgDelayAfterPwrEn) * STALL_ONE_MILLI_SECOND);
#endif

      ///
      /// Drive DGPU HOLD RST Disabled to remove reset
      ///
      PcieGpioWrite (
#if FixedPcdGet8(PcdFspModeSelection) == 1
        ((FSPM_UPD *) FspmUpd)->FspmConfig.CpuPcie0Rtd3Gpio[4],
        (BOOLEAN)((FSPM_UPD *) FspmUpd)->FspmConfig.CpuPcie0Rtd3Gpio[1],
#else
        HgGpioData->CpuPcie0Rtd3Gpio.HoldRst.GpioNo,
        HgGpioData->CpuPcie0Rtd3Gpio.HoldRst.Active,
#endif
        GP_DISABLE
        );
      ///
      /// wait 100ms
      ///
#if FixedPcdGet8(PcdFspModeSelection) == 1
      MicroSecondDelay((((FSPM_UPD *) FspmUpd)->FspmConfig.HgDelayAfterHoldReset) * STALL_ONE_MILLI_SECOND);
#else
      MicroSecondDelay((HgGpioData->HgDelayAfterHoldReset) * STALL_ONE_MILLI_SECOND);
#endif
    }
#if FixedPcdGet8(PcdFspModeSelection) == 0
  }
#endif

  return EFI_SUCCESS;
}
