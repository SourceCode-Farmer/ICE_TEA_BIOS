/** @file

;******************************************************************************
;* Copyright (c) 2020, Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
*/
/** @file
Do Platform Stage System Agent initialization.

@copyright
  INTEL CONFIDENTIAL
  Copyright 2015 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains a 'Sample Driver' and is licensed as such under the terms
  of your license agreement with Intel or your vendor. This file may be modified
  by the user, subject to the additional terms of the license agreement.

@par Specification Reference:
**/

#include "PeiSaPolicyUpdate.h"
#include <Guid/MemoryTypeInformation.h>
#include <Library/BaseMemoryLib.h>
#include <Library/BmpSupportLib.h>
#include <Library/CpuPcieInfoFruLib.h>
#include <Library/HobLib.h>
#include <Library/MemoryAllocationLib.h>
#include <Library/PeiGetFvInfoLib.h>
#include <Library/PeiSaPolicyLib.h>
#include <Library/PeiSiPolicyUpdateLib.h>
#include <Library/SiPolicyLib.h>
#include <Pi/PiFirmwareFile.h>
#include <Protocol/GraphicsOutput.h>

#include <CpuPcieConfig.h>
#include <CpuPcieHob.h>
#include <IndustryStandard/Bmp.h>
#include <Platform.h>
#include <PolicyUpdateMacro.h>
#include <Guid/GraphicsInfoHob.h>

#if FixedPcdGetBool(PcdVmdEnable) == 1
#include <VmdPeiConfig.h>
#endif

#if FixedPcdGetBool(PcdITbtEnable) == 1
#include <TcssPeiConfig.h>
#include <TcssPeiPreMemConfig.h>
#endif

#if FixedPcdGet8(PcdFspModeSelection) == 1
#include <FspsUpd.h>
#endif

#include <TelemetryPeiConfig.h>
#include <Ppi/GraphicsPlatformPolicyPpi.h>

#include <UsbTypeC.h>

EFI_STATUS
EFIAPI
PeiGraphicsPolicyUpdateCallback (
  IN EFI_PEI_SERVICES           **PeiServices,
  IN EFI_PEI_NOTIFY_DESCRIPTOR  *NotifyDescriptor,
  IN VOID                       *Ppi
  )
{
  EFI_STATUS                          Status;
  EFI_PEI_GRAPHICS_INFO_HOB           *PlatformGraphicsOutput;
  EFI_PEI_HOB_POINTERS                 Hob;
  UINT8                               *HobStart;
#if FixedPcdGet8(PcdFspModeSelection) == 1
  VOID                                *FspsUpd;
#else
  GRAPHICS_PEI_CONFIG                 *GtConfig;
  SI_POLICY_PPI                       *SiPolicyPpi;
#endif

  DEBUG ((DEBUG_INFO, "%a: Entry\n",__FUNCTION__));

  PlatformGraphicsOutput = NULL;
  HobStart = NULL;

#if FixedPcdGet8(PcdFspModeSelection) == 1
  FspsUpd = NULL;
  FspsUpd = (FSPS_UPD *) PcdGet32 (PcdFspsUpdDataAddress);
  ASSERT (FspsUpd != NULL);
#else
  GtConfig = NULL;
  SiPolicyPpi = NULL;
  Status = PeiServicesLocatePpi (&gSiPolicyPpiGuid, 0, NULL, (VOID **) &SiPolicyPpi);
  ASSERT_EFI_ERROR(Status);

  Status = GetConfigBlock ((VOID *) SiPolicyPpi, &gGraphicsPeiConfigGuid, (VOID *) &GtConfig);
  ASSERT_EFI_ERROR(Status);
#endif

  Status = PeiServicesGetHobList ((VOID **) &Hob.Raw);
  HobStart = Hob.Raw;

  if (!EFI_ERROR (Status)) {
    if (HobStart != NULL) {
      if ((Hob.Raw = GetNextGuidHob (&gEfiGraphicsInfoHobGuid, HobStart)) != NULL) {
        DEBUG ((DEBUG_INFO, "Found EFI_PEI_GRAPHICS_INFO_HOB\n"));
        PlatformGraphicsOutput = GET_GUID_HOB_DATA (Hob.Guid);
      }
    }
  }

  if (PlatformGraphicsOutput != NULL) {
    UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.HorizontalResolution,   GtConfig->HorizontalResolution, PlatformGraphicsOutput->GraphicsMode.HorizontalResolution);
    UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.VerticalResolution,     GtConfig->VerticalResolution,   PlatformGraphicsOutput->GraphicsMode.VerticalResolution);
  } else {
    DEBUG ((DEBUG_INFO, "Not able to find EFI_PEI_GRAPHICS_INFO_HOB\n"));
  }

  DEBUG((DEBUG_INFO, "%a: Exit\n", __FUNCTION__));
  return Status;
}

STATIC
EFI_PEI_NOTIFY_DESCRIPTOR  mPeiGfxPolicyUpdateNotifyList = {
  (EFI_PEI_PPI_DESCRIPTOR_NOTIFY_CALLBACK | EFI_PEI_PPI_DESCRIPTOR_TERMINATE_LIST),
  &gPeiGraphicsFramebufferReadyPpiGuid,
  PeiGraphicsPolicyUpdateCallback
};

#if FixedPcdGet8(PcdFspModeSelection) == 1
#if FixedPcdGetBool(PcdAdlLpSupport) == 1
EFI_STATUS
EFIAPI
PeiGraphicsPlatformPolicyNotifyCallback (
  IN EFI_PEI_SERVICES           **PeiServices,
  IN EFI_PEI_NOTIFY_DESCRIPTOR  *NotifyDescriptor,
  IN VOID                       *Ppi
  )
{
  EFI_STATUS                        Status;
  VOID                             *FspsUpd;
  PEI_GRAPHICS_PLATFORM_POLICY_PPI *GfxPlatformPolicyPpi;
  LID_STATUS                       CurrentLidStatus;

  DEBUG ((DEBUG_INFO, "PeiGraphicsPlatformPolicyNotifyCallback Entry\n"));

  FspsUpd = (FSPS_UPD *) PcdGet32 (PcdFspsUpdDataAddress);
  ASSERT (FspsUpd != NULL);
  ///
  /// Locate GfxPlatformPolicyPpi
  ///
  Status = PeiServicesLocatePpi (&gPeiGraphicsPlatformPpiGuid, 0, NULL, (VOID *) &GfxPlatformPolicyPpi);
  if (EFI_ERROR (Status)) {
    DEBUG ((DEBUG_WARN, "Failed to locate GfxPlatformPolicyPpi.\n"));
    ASSERT_EFI_ERROR(Status);
  }

  Status = GfxPlatformPolicyPpi->GetPlatformLidStatus (&CurrentLidStatus);
  ASSERT_EFI_ERROR (Status);
  ((FSPS_UPD *) FspsUpd)->FspsConfig.LidStatus = (UINT8) CurrentLidStatus;
  DEBUG ((DEBUG_INFO, "LidStatus from GetPlatformLidStatus is 0x%x\n", ((FSPS_UPD *) FspsUpd)->FspsConfig.LidStatus));
  DEBUG ((DEBUG_INFO, "PeiGraphicsPlatformPolicyNotifyCallback Exit\n"));

  return Status;
}

STATIC
EFI_PEI_NOTIFY_DESCRIPTOR  mPeiGfxPlatformPolicyNotifyList = {
  (EFI_PEI_PPI_DESCRIPTOR_NOTIFY_CALLBACK | EFI_PEI_PPI_DESCRIPTOR_TERMINATE_LIST),
  &gPeiGraphicsPlatformPpiGuid,
  PeiGraphicsPlatformPolicyNotifyCallback
};
#endif
#endif

/**
  Tcss Get PchUsb2 TypeA Port Number Mapped with CpuXhci Port Index

  @IN     UINT8              CpuXhciPortIndex

  @retval UINT8              PchUsb2PortNumber.
**/
UINT8
EFIAPI
TcssGetPchUsb2PortNoMappedWithCpuXhciPortIndex (
  IN UINT8 CpuXhciPortIndex
  )
{
  UINT8          UsbCPortProperties;
  UINT8          UsbTypeCPortPch;
  UINT8          Index;

  if (!PcdGetBool (PcdUsbTypeCSupport)) {
    return 0;
  }

// Initialize the Local Variable
  UsbCPortProperties = 0;
  UsbTypeCPortPch    = 0;
  Index              = 0;

  // Check TypeC Port 1 Field and Match the Value
  UsbCPortProperties = PcdGet8 (PcdUsbCPort1Properties);
  UsbTypeCPortPch    = PcdGet8 (PcdUsbTypeCPort1Pch);
  Index              = 0;
  if ((PcdGet8 (PcdTypeCPortsSupported) > Index) && (UsbTypeCPortPch != 0)) {
    if ((((UsbCPortProperties >> USB_TYPEC_PORT_TYPE_BIT_OFFSET)\
      & USB_TYPEC_PORT_TYPE_MASK) == USB_TYPEA_CPU)\
      ) { // Check if it is CPU Xhci Type-A Port Or Enable Tcss Convert from Type-C to Type-A force Enable
      if (((UsbCPortProperties >> TCSS_TYPEC_ROOT_PORT_INDEX_BIT_OFFSET)\
        & TCSS_TYPEC_ROOT_PORT_INDEX_MASK)\
        == CpuXhciPortIndex\
        ) {// Check if requested RP Index Matching
        DEBUG ((DEBUG_INFO, "Type-C Port[%d] Cpu Xhci Port Index = %d and PchUsb2PortNo = %d\n",\
          Index, CpuXhciPortIndex, UsbTypeCPortPch));
        return UsbTypeCPortPch;
      }
    }
  }

  // Check TypeC Port 2 Field and Match the Value
  UsbCPortProperties = PcdGet8 (PcdUsbCPort2Properties);
  UsbTypeCPortPch    = PcdGet8 (PcdUsbTypeCPort2Pch);
  Index              = 1;
  if ((PcdGet8 (PcdTypeCPortsSupported) > Index) && (UsbTypeCPortPch != 0)) {
    if ((((UsbCPortProperties >> USB_TYPEC_PORT_TYPE_BIT_OFFSET)\
      & USB_TYPEC_PORT_TYPE_MASK) == USB_TYPEA_CPU)\
      ) { // Check if it is CPU Xhci Type-A Port Or Enable Tcss Convert from Type-C to Type-A force Enable
      if (((UsbCPortProperties >> TCSS_TYPEC_ROOT_PORT_INDEX_BIT_OFFSET)\
        & TCSS_TYPEC_ROOT_PORT_INDEX_MASK)\
        == CpuXhciPortIndex\
        ) {// Check if requested RP Index Matching
        DEBUG ((DEBUG_INFO, "Type-C Port[%d] Cpu Xhci Port Index = %d and PchUsb2PortNo = %d\n",\
          Index, CpuXhciPortIndex, UsbTypeCPortPch));
        return UsbTypeCPortPch;
      }
    }
  }

  // Check TypeC Port 3 Field and Match the Value
  UsbCPortProperties = PcdGet8 (PcdUsbCPort3Properties);
  UsbTypeCPortPch    = PcdGet8 (PcdUsbTypeCPort3Pch);
  Index              = 2;
  if ((PcdGet8 (PcdTypeCPortsSupported) > Index) && (UsbTypeCPortPch != 0)) {
    if ((((UsbCPortProperties >> USB_TYPEC_PORT_TYPE_BIT_OFFSET)\
      & USB_TYPEC_PORT_TYPE_MASK) == USB_TYPEA_CPU)\
      ) { // Check if it is CPU Xhci Type-A Port Or Enable Tcss Convert from Type-C to Type-A force Enable
      if (((UsbCPortProperties >> TCSS_TYPEC_ROOT_PORT_INDEX_BIT_OFFSET)\
        & TCSS_TYPEC_ROOT_PORT_INDEX_MASK)\
        == CpuXhciPortIndex\
        ) {// Check if requested RP Index Matching
        DEBUG ((DEBUG_INFO, "Type-C Port[%d] Cpu Xhci Port Index = %d and PchUsb2PortNo = %d\n",\
          Index, CpuXhciPortIndex, UsbTypeCPortPch));
        return UsbTypeCPortPch;
      }
    }
  }

  // Check TypeC Port 4 Field and Match the Value
  UsbCPortProperties = PcdGet8 (PcdUsbCPort4Properties);
  UsbTypeCPortPch    = PcdGet8 (PcdUsbTypeCPort4Pch);
  Index              = 3;
  if ((PcdGet8 (PcdTypeCPortsSupported) > Index) && (UsbTypeCPortPch != 0)) {
    if ((((UsbCPortProperties >> USB_TYPEC_PORT_TYPE_BIT_OFFSET)\
      & USB_TYPEC_PORT_TYPE_MASK) == USB_TYPEA_CPU)\
      ) { // Check if it is CPU Xhci Type-A Port Or Enable Tcss Convert from Type-C to Type-A force Enable
      if (((UsbCPortProperties >> TCSS_TYPEC_ROOT_PORT_INDEX_BIT_OFFSET)\
        & TCSS_TYPEC_ROOT_PORT_INDEX_MASK)\
        == CpuXhciPortIndex\
        ) {// Check if requested RP Index Matching
        DEBUG ((DEBUG_INFO, "Type-C Port[%d] Cpu Xhci Port Index = %d and PchUsb2PortNo = %d\n",\
          Index, CpuXhciPortIndex, UsbTypeCPortPch));
        return UsbTypeCPortPch;
      }
    }
  }

  // Check TypeC Port 5 Field and Match the Value
  UsbCPortProperties = PcdGet8 (PcdUsbCPort5Properties);
  UsbTypeCPortPch    = PcdGet8 (PcdUsbTypeCPort5Pch);
  Index              = 4;
  if ((PcdGet8 (PcdTypeCPortsSupported) > Index) && (UsbTypeCPortPch != 0)) {
    if ((((UsbCPortProperties >> USB_TYPEC_PORT_TYPE_BIT_OFFSET)\
      & USB_TYPEC_PORT_TYPE_MASK) == USB_TYPEA_CPU)\
      ) { // Check if it is CPU Xhci Type-A Port Or Enable Tcss Convert from Type-C to Type-A force Enable
      if (((UsbCPortProperties >> TCSS_TYPEC_ROOT_PORT_INDEX_BIT_OFFSET)\
        & TCSS_TYPEC_ROOT_PORT_INDEX_MASK)\
        == CpuXhciPortIndex\
        ) {// Check if requested RP Index Matching
        DEBUG ((DEBUG_INFO, "Type-C Port[%d] Cpu Xhci Port Index = %d and PchUsb2PortNo = %d\n",\
          Index, CpuXhciPortIndex, UsbTypeCPortPch));
        return UsbTypeCPortPch;
      }
    }
  }

  // Check TypeC Port 6 Field and Match the Value
  UsbCPortProperties = PcdGet8 (PcdUsbCPort6Properties);
  UsbTypeCPortPch    = PcdGet8 (PcdUsbTypeCPort6Pch);
  Index              = 5;
  if ((PcdGet8 (PcdTypeCPortsSupported) > Index) && (UsbTypeCPortPch != 0)) {
    if ((((UsbCPortProperties >> USB_TYPEC_PORT_TYPE_BIT_OFFSET)\
      & USB_TYPEC_PORT_TYPE_MASK) == USB_TYPEA_CPU)\
      ) { // Check if it is CPU Xhci Type-A Port Or Enable Tcss Convert from Type-C to Type-A force Enable
      if (((UsbCPortProperties >> TCSS_TYPEC_ROOT_PORT_INDEX_BIT_OFFSET)\
        & TCSS_TYPEC_ROOT_PORT_INDEX_MASK)\
        == CpuXhciPortIndex\
        ) {// Check if requested RP Index Matching
        DEBUG ((DEBUG_INFO, "Type-C Port[%d] Cpu Xhci Port Index = %d and PchUsb2PortNo = %d\n",\
          Index, CpuXhciPortIndex, UsbTypeCPortPch));
        return UsbTypeCPortPch;
      }
    }
  }

  // Check TypeC Port 7 Field and Match the Value
  UsbCPortProperties = PcdGet8 (PcdUsbCPort7Properties);
  UsbTypeCPortPch    = PcdGet8 (PcdUsbTypeCPort7Pch);
  Index              = 6;
  if ((PcdGet8 (PcdTypeCPortsSupported) > Index) && (UsbTypeCPortPch != 0)) {
    if ((((UsbCPortProperties >> USB_TYPEC_PORT_TYPE_BIT_OFFSET)\
      & USB_TYPEC_PORT_TYPE_MASK) == USB_TYPEA_CPU)\
      ) { // Check if it is CPU Xhci Type-A Port Or Enable Tcss Convert from Type-C to Type-A force Enable
      if (((UsbCPortProperties >> TCSS_TYPEC_ROOT_PORT_INDEX_BIT_OFFSET)\
        & TCSS_TYPEC_ROOT_PORT_INDEX_MASK)\
        == CpuXhciPortIndex\
        ) {// Check if requested RP Index Matching
        DEBUG ((DEBUG_INFO, "Type-C Port[%d] Cpu Xhci Port Index = %d and PchUsb2PortNo = %d\n",\
          Index, CpuXhciPortIndex, UsbTypeCPortPch));
        return UsbTypeCPortPch;
      }
    }
  }

  // Check TypeC Port 8 Field and Match the Value
  UsbCPortProperties = PcdGet8 (PcdUsbCPort8Properties);
  UsbTypeCPortPch    = PcdGet8 (PcdUsbTypeCPort8Pch);
  Index              = 7;
  if ((PcdGet8 (PcdTypeCPortsSupported) > Index) && (UsbTypeCPortPch != 0)) {
    if ((((UsbCPortProperties >> USB_TYPEC_PORT_TYPE_BIT_OFFSET)\
      & USB_TYPEC_PORT_TYPE_MASK) == USB_TYPEA_CPU)\
      ) { // Check if it is CPU Xhci Type-A Port Or Enable Tcss Convert from Type-C to Type-A force Enable
      if (((UsbCPortProperties >> TCSS_TYPEC_ROOT_PORT_INDEX_BIT_OFFSET)\
        & TCSS_TYPEC_ROOT_PORT_INDEX_MASK)\
        == CpuXhciPortIndex\
        ) {// Check if requested RP Index Matching
        DEBUG ((DEBUG_INFO, "Type-C Port[%d] Cpu Xhci Port Index = %d and PchUsb2PortNo = %d\n",\
          Index, CpuXhciPortIndex, UsbTypeCPortPch));
        return UsbTypeCPortPch;
      }
    }
  }

  // Check TypeC Port 9 Field and Match the Value
  UsbCPortProperties = PcdGet8 (PcdUsbCPort9Properties);
  UsbTypeCPortPch    = PcdGet8 (PcdUsbTypeCPort9Pch);
  Index              = 8;
  if ((PcdGet8 (PcdTypeCPortsSupported) > Index) && (UsbTypeCPortPch != 0)) {
    if ((((UsbCPortProperties >> USB_TYPEC_PORT_TYPE_BIT_OFFSET)\
      & USB_TYPEC_PORT_TYPE_MASK) == USB_TYPEA_CPU)\
      ) { // Check if it is CPU Xhci Type-A Port Or Enable Tcss Convert from Type-C to Type-A force Enable
      if (((UsbCPortProperties >> TCSS_TYPEC_ROOT_PORT_INDEX_BIT_OFFSET)\
        & TCSS_TYPEC_ROOT_PORT_INDEX_MASK)\
        == CpuXhciPortIndex\
        ) {// Check if requested RP Index Matching
        DEBUG ((DEBUG_INFO, "Type-C Port[%d] Cpu Xhci Port Index = %d and PchUsb2PortNo = %d\n",\
          Index, CpuXhciPortIndex, UsbTypeCPortPch));
        return UsbTypeCPortPch;
      }
    }
  }

  // Check TypeC Port A Field and Match the Value
  UsbCPortProperties = PcdGet8 (PcdUsbCPortAProperties);
  UsbTypeCPortPch    = PcdGet8 (PcdUsbTypeCPortAPch);
  Index              = 9;
  if ((PcdGet8 (PcdTypeCPortsSupported) > Index) && (UsbTypeCPortPch != 0)) {
    if ((((UsbCPortProperties >> USB_TYPEC_PORT_TYPE_BIT_OFFSET)\
      & USB_TYPEC_PORT_TYPE_MASK) == USB_TYPEA_CPU)\
      ) { // Check if it is CPU Xhci Type-A Port Or Enable Tcss Convert from Type-C to Type-A force Enable
      if (((UsbCPortProperties >> TCSS_TYPEC_ROOT_PORT_INDEX_BIT_OFFSET)\
        & TCSS_TYPEC_ROOT_PORT_INDEX_MASK)\
        == CpuXhciPortIndex\
        ) {// Check if requested RP Index Matching
        DEBUG ((DEBUG_INFO, "Type-C Port[%d] Cpu Xhci Port Index = %d and PchUsb2PortNo = %d\n",\
          Index, CpuXhciPortIndex, UsbTypeCPortPch));
        return UsbTypeCPortPch;
      }
    }
  }

  return 0;
}

/**
  UpdatePeiSaPolicy performs SA PEI Policy initialization

  @retval EFI_SUCCESS              The policy is installed and initialized.
**/
EFI_STATUS
EFIAPI
UpdatePeiSaPolicy (
  VOID
  )
{
  EFI_GUID                        BmpImageGuid;
  EFI_STATUS                      Status;
  EFI_STATUS                      Status2;
  SETUP_DATA                      SetupData;
  EFI_GUID                        FileGuid;
  SA_SETUP                        SaSetup;
  UINTN                           VarSize;
  VOID                            *Buffer;
  UINT32                          Size;
  UINT8                           Index;
  EFI_PEI_READ_ONLY_VARIABLE2_PPI *VariableServices;
  EFI_BOOT_MODE                   BootMode;
  PCH_SETUP                       PchSetup;
#if FixedPcdGetBool(PcdCpuPcieEnable) == 1
  UINTN                           MaxPciePorts;
#endif
#if FixedPcdGetBool(PcdVmdEnable) == 1
  VOID                            *VmdVariablePtr;
#endif
//[-start-200903-IB17040149-add]//
#if FixedPcdGet8(PcdModifyVmdPortConfigViaScuDefault)
  EFI_VMD_OS_DATA                 *VmdVariableDataPtr;
#endif
//[-end-200903-IB17040149-add]//
#if FixedPcdGet8(PcdFspModeSelection) == 1
  VOID                            *FspsUpd;
#else
  GRAPHICS_PEI_CONFIG             *GtConfig;
  SI_POLICY_PPI                   *SiPolicyPpi;
  GNA_CONFIG                      *GnaConfig;
  HOST_BRIDGE_PEI_CONFIG          *HostBridgePeiConfig;
#if FixedPcdGetBool (PcdITbtEnable) == 1
  TCSS_PEI_CONFIG                 *TcssPeiConfig;
  TCSS_PEI_PREMEM_CONFIG          *TcssPeiPreMemConfig;
#endif
  SI_PREMEM_POLICY_PPI            *SiPreMemPolicyPpi;
  CPU_PCIE_CONFIG                 *CpuPcieRpConfig;
  TELEMETRY_PEI_CONFIG            *TelemetryPeiConfig;
#if FixedPcdGetBool(PcdVmdEnable) == 1
  VMD_PEI_CONFIG                  *VmdPeiConfig;
#endif
#endif
  EFI_PEI_PPI_DESCRIPTOR          *ReadyForGopConfigPpiDesc;
  VOID                            *VbtPtr;
  EFI_GRAPHICS_OUTPUT_BLT_PIXEL    *Blt;
  UINTN                            BltSize;
  UINTN                            Height;
  UINTN                            Width;
#if FixedPcdGetBool (PcdITbtEnable) == 1
  UINT8                            PchUsb2PortNo;
#endif

  DEBUG ((DEBUG_INFO, "Update PeiSaPolicyUpdate Pos-Mem Start\n"));

  Size  = 0;
  Blt   = NULL;
  BltSize = 0;
#if FixedPcdGetBool (PcdITbtEnable) == 1
  PchUsb2PortNo         = 0xFF;
#endif
#if FixedPcdGet8(PcdFspModeSelection) == 1
  FspsUpd = NULL;
#else
  GtConfig              = NULL;
  SiPolicyPpi           = NULL;
  GnaConfig             = NULL;
  HostBridgePeiConfig   = NULL;
#if FixedPcdGetBool (PcdITbtEnable) == 1
  TcssPeiConfig         = NULL;
  TcssPeiPreMemConfig   = NULL;
#endif
  SiPreMemPolicyPpi     = NULL;
  CpuPcieRpConfig       = NULL;
  TelemetryPeiConfig    = NULL;
#endif

#if FixedPcdGetBool(PcdVmdEnable) == 1
  VmdVariablePtr        = NULL;
#endif

  Buffer     = NULL;

#if FixedPcdGet8(PcdFspModeSelection) == 1
  FspsUpd = (FSPS_UPD *) PcdGet32 (PcdFspsUpdDataAddress);
  ASSERT (FspsUpd != NULL);
#else
  Status = PeiServicesLocatePpi (&gSiPreMemPolicyPpiGuid, 0, NULL, (VOID **) &SiPreMemPolicyPpi);
  ASSERT_EFI_ERROR (Status);

  Status = PeiServicesLocatePpi (&gSiPolicyPpiGuid, 0, NULL, (VOID **) &SiPolicyPpi);
  ASSERT_EFI_ERROR (Status);

#if FixedPcdGetBool (PcdITbtEnable) == 1
  Status = GetConfigBlock ((VOID *) SiPreMemPolicyPpi, &gTcssPeiPreMemConfigGuid, (VOID *) &TcssPeiPreMemConfig);
  ASSERT_EFI_ERROR(Status);
#endif  // PcdITbtEnable

  Status = GetConfigBlock ((VOID *) SiPolicyPpi, &gGraphicsPeiConfigGuid, (VOID *) &GtConfig);
  ASSERT_EFI_ERROR(Status);

  Status = GetConfigBlock ((VOID *) SiPolicyPpi, &gGnaConfigGuid, (VOID *) &GnaConfig);
  ASSERT_EFI_ERROR(Status);

  Status = GetConfigBlock ((VOID *) SiPolicyPpi, &gHostBridgePeiConfigGuid, (VOID *) &HostBridgePeiConfig);
  ASSERT_EFI_ERROR (Status);

#if FixedPcdGetBool (PcdITbtEnable) == 1
  Status = GetConfigBlock ((VOID *) SiPolicyPpi, &gTcssPeiConfigGuid, (VOID *) &TcssPeiConfig);
  ASSERT_EFI_ERROR(Status);
#endif  // PcdITbtEnable

#if FixedPcdGetBool(PcdCpuPcieEnable) == 1
  Status = GetConfigBlock ((VOID *) SiPolicyPpi, &gCpuPcieRpConfigGuid, (VOID *) &CpuPcieRpConfig);
  ASSERT_EFI_ERROR(Status);
#endif

#if FixedPcdGetBool(PcdVmdEnable) == 1
  VmdPeiConfig          = NULL;
  Status = GetConfigBlock ((VOID *) SiPolicyPpi, &gVmdPeiConfigGuid, (VOID *) &VmdPeiConfig);
  ASSERT_EFI_ERROR(Status);
#endif

  Status = GetConfigBlock((VOID *)SiPolicyPpi, &gTelemetryPeiConfigGuid, (VOID *)&TelemetryPeiConfig);
  ASSERT_EFI_ERROR(Status);

#endif

  Status = PeiServicesGetBootMode (&BootMode);
  ASSERT_EFI_ERROR (Status);

#if FixedPcdGet8(PcdFspModeSelection) == 1
#if FixedPcdGetBool(PcdAdlLpSupport) == 1
  DEBUG ((DEBUG_INFO, "Calling PeiGfxPlatformPolicyNotify\n"));
  Status = PeiServicesNotifyPpi (&mPeiGfxPlatformPolicyNotifyList);
#endif
#endif

  //
  // Locate system configuration variable
  //
  Status = PeiServicesLocatePpi (
             &gEfiPeiReadOnlyVariable2PpiGuid, // GUID
             0,                                // INSTANCE
             NULL,                             // EFI_PEI_PPI_DESCRIPTOR
             (VOID **) &VariableServices       // PPI
             );
  ASSERT_EFI_ERROR (Status);

  VarSize = sizeof (PCH_SETUP);
  Status = VariableServices->GetVariable (
                               VariableServices,
                               L"PchSetup",
                               &gPchSetupVariableGuid,
                               NULL,
                               &VarSize,
                               &PchSetup
                               );
  ASSERT_EFI_ERROR (Status);

#if FixedPcdGetBool (PcdITbtEnable) == 1
  if (!EFI_ERROR (Status)) {
    COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.PchUsbOverCurrentEnable, TcssPeiConfig->UsbConfig.OverCurrentEnable, PchSetup.PchUsbOverCurrentEnable, NullIndex);
  }
#endif  // PcdITbtEnable

  VarSize = sizeof (SETUP_DATA);
  Status = VariableServices->GetVariable (
                               VariableServices,
                               PLATFORM_SETUP_VARIABLE_NAME,
                               &gSetupVariableGuid,
                               NULL,
                               &VarSize,
                               &SetupData
                               );

  VarSize = sizeof (SA_SETUP);
  Status2 = VariableServices->GetVariable(
                                VariableServices,
                                L"SaSetup",
                                &gSaSetupVariableGuid,
                                NULL,
                                &VarSize,
                                &SaSetup
                                );
  ASSERT_EFI_ERROR(Status2);

#if FixedPcdGetBool(PcdITbtEnable) == 1
  if (!EFI_ERROR (Status2)) {
    UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.TcssCpuUsbPdoProgramming, TcssPeiConfig->UsbConfig.PdoProgramming, SaSetup.CpuUsbPdoProgramming);
  }
#endif


  if (SaSetup.VbtSelect == VBT_SELECT_MIPI) {
    CopyMem(&BmpImageGuid, PcdGetPtr(PcdVbtMipiGuid), sizeof(BmpImageGuid));
  } else {
    CopyMem(&BmpImageGuid, PcdGetPtr(PcdIntelGraphicsVbtFileGuid), sizeof(BmpImageGuid));
  }

  if (!EFI_ERROR (Status)) {
    COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.PavpEnable,          GtConfig->PavpEnable,          SaSetup.PavpEnable,          NullIndex);
    COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.CdClock,             GtConfig->CdClock,             SaSetup.CdClock,             NullIndex);
    COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.PeiGraphicsPeimInit, GtConfig->PeiGraphicsPeimInit, SaSetup.PeiGraphicsPeimInit, NullIndex);
    COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.GtFreqMax,           GtConfig->GtFreqMax,           SaSetup.GtFreqMax,           NullIndex);
    COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.DisableTurboGt,      GtConfig->DisableTurboGt,      SaSetup.DisableTurboGt,      NullIndex);
    COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.CdynmaxClampEnable,  GtConfig->CdynmaxClampEnable,  SaSetup.CdynmaxClampEnable,  NullIndex);
    COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.RC1pFreqEnable,      GtConfig->RC1pFreqEnable,      SaSetup.RC1pFreqEnable,      NullIndex);
    COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.SkipCdClockInit,     GtConfig->SkipCdClockInit,     SaSetup.SkipCdClockInit,     NullIndex);
    COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.RenderStandby,       GtConfig->RenderStandby,       SaSetup.EnableRenderStandby, NullIndex);
    COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.PmSupport,           GtConfig->PmSupport,           SaSetup.PmSupport,           NullIndex);
    UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.SkipFspGop,             GtConfig->SkipFspGop,          0x0);

    if (SaSetup.PeiGraphicsPeimInit == 1) {
      Buffer = NULL;

      CopyMem(&FileGuid, &BmpImageGuid, sizeof(FileGuid));
      PeiGetSectionFromFv(FileGuid, &Buffer, &Size);
      if (Buffer == NULL) {
        DEBUG((DEBUG_ERROR, "Could not locate VBT\n"));
      }

      if (BootMode == BOOT_ON_S3_RESUME) {
#if FixedPcdGet8(PcdFspModeSelection) == 1
        UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.GraphicsConfigPtr, GtConfig->GraphicsConfigPtr, 0);
#else
        UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.GraphicsConfigPtr, GtConfig->GraphicsConfigPtr, NULL);
#endif
      } else {
#if FixedPcdGet8(PcdFspModeSelection) == 1
        ((FSPS_UPD *) FspsUpd)->FspsConfig.GraphicsConfigPtr = (UINT32) Buffer;
        DEBUG ((DEBUG_INFO, "Vbt Pointer from PeiGetSectionFromFv is 0x%x\n", ((FSPS_UPD *) FspsUpd)->FspsConfig.GraphicsConfigPtr));
        ((FSPS_UPD *) FspsUpd)->FspsConfig.VbtSize = Size;
#else
        GtConfig->GraphicsConfigPtr = Buffer;
        DEBUG ((DEBUG_INFO, "Vbt Pointer from PeiGetSectionFromFv is 0x%x\n", GtConfig->GraphicsConfigPtr));
#endif
      }
      DEBUG ((DEBUG_INFO, "Vbt Size from PeiGetSectionFromFv is 0x%x\n", Size));
      GET_POLICY ((VOID *) ((FSPS_UPD *) FspsUpd)->FspsConfig.GraphicsConfigPtr, GtConfig->GraphicsConfigPtr, VbtPtr);

      Buffer = NULL;
      PeiGetSectionFromFv (gTianoLogoGuid, &Buffer, &Size);
      if (Buffer == NULL) {
        DEBUG ((DEBUG_WARN, "Could not locate Logo\n"));
      }
#if FixedPcdGet8(PcdFspModeSelection) == 1
      ((FSPS_UPD *) FspsUpd)->FspsConfig.LogoPtr  = (UINT32) Buffer;
      ((FSPS_UPD *) FspsUpd)->FspsConfig.LogoSize = Size;
      DEBUG ((DEBUG_INFO, "LogoPtr from PeiGetSectionFromFv is 0x%x\n", ((FSPS_UPD *) FspsUpd)->FspsConfig.LogoPtr));
      DEBUG ((DEBUG_INFO, "LogoSize from PeiGetSectionFromFv is 0x%x\n", ((FSPS_UPD *) FspsUpd)->FspsConfig.LogoSize));
#else
      GtConfig->LogoPtr  = Buffer;
      GtConfig->LogoSize = Size;
      DEBUG ((DEBUG_INFO, "LogoPtr from PeiGetSectionFromFv is 0x%x\n", GtConfig->LogoPtr));
      DEBUG ((DEBUG_INFO, "LogoSize from PeiGetSectionFromFv is 0x%x\n", GtConfig->LogoSize));
#endif

      //
      // Install ReadyForGopConfig PPI to trigger PEI phase GopConfig callback.
      //
      ReadyForGopConfigPpiDesc = (EFI_PEI_PPI_DESCRIPTOR *) AllocateZeroPool (sizeof (EFI_PEI_PPI_DESCRIPTOR));
      if (ReadyForGopConfigPpiDesc == NULL) {
        ASSERT (FALSE);
        return EFI_OUT_OF_RESOURCES;
      }
      ReadyForGopConfigPpiDesc->Flags = EFI_PEI_PPI_DESCRIPTOR_PPI | EFI_PEI_PPI_DESCRIPTOR_TERMINATE_LIST;
      ReadyForGopConfigPpiDesc->Guid  = &gReadyForGopConfigPpiGuid;
      ReadyForGopConfigPpiDesc->Ppi   = VbtPtr;
      Status = PeiServicesInstallPpi (ReadyForGopConfigPpiDesc);
    }

    Status = TranslateBmpToGopBlt (
              Buffer,
              Size,
              &Blt,
              &BltSize,
              &Height,
              &Width
              );

    if (Status == EFI_BUFFER_TOO_SMALL) {
      Blt = NULL;
      Status = TranslateBmpToGopBlt (
                Buffer,
                Size,
                &Blt,
                &BltSize,
                &Height,
                &Width
                );
      if (EFI_ERROR (Status)) {
        DEBUG ((DEBUG_ERROR, "TranslateBmpToGopBlt, Status = %r\n", Status));
        ASSERT_EFI_ERROR (Status);
        return Status;
      }
    }

    //
    // Initialize Blt, BltSize, LogoPixel Height and Width
    //
#if FixedPcdGet8(PcdFspModeSelection) == 1
    ((FSPS_UPD *) FspsUpd)->FspsConfig.BltBufferAddress = (UINT32) Blt;
#else
    GtConfig->BltBufferAddress = Blt;
#endif
    UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.BltBufferSize,   GtConfig->BltBufferSize,  BltSize);
    UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.LogoPixelHeight, GtConfig->LogoPixelHeight, Height);
    UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.LogoPixelWidth,  GtConfig->LogoPixelWidth,  Width);

    DEBUG ((DEBUG_INFO, "Calling mPeiGfxPolicyUpdateNotifyList\n"));
    Status = PeiServicesNotifyPpi (&mPeiGfxPolicyUpdateNotifyList);
    //
    // Initialize GNA Configuration
    //
    COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.GnaEnable, GnaConfig->GnaEnable, SaSetup.GnaEnable, NullIndex);
    //
    // Initialize Misc SA Configuration
    //
    COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.EdramTestMode, HostBridgePeiConfig->EdramTestMode, SaSetup.EdramTestMode, NullIndex);
    //
    // Intel(R) Dynamic Tuning Technology might need SA thermal device to be enabled.
    //
    if ((SetupData.EnableDptf == 1) && (SetupData.EnableSaDevice == 1)) {
      SaSetup.SaDevice4 = 1;
    }
    COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.Device4Enable, HostBridgePeiConfig->Device4Enable, SaSetup.SaDevice4, NullIndex);
  }

#if FixedPcdGetBool(PcdVmdEnable) == 1
  //
  // VMD related settings from setup variable
  //
  COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.VmdEnable,            VmdPeiConfig->VmdEnable,                       SaSetup.VmdEnable,          NullIndex);
  COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.VmdGlobalMapping,     VmdPeiConfig->VmdGlobalMapping,                SaSetup.VmdGlobalMapping,   NullIndex);
  for ( Index = 0; Index < VMD_MAX_DEVICES; ++Index) {
    COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.VmdPort[Index],     VmdPeiConfig->VmdPortEnable[Index].RpEnable,   SaSetup.VmdPort[Index],     Index);
    //Update dev and Fuc
    COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.VmdPortDev[Index],  VmdPeiConfig->VmdPortEnable[Index].RpDevice,   SaSetup.VmdPortDev[Index],  Index);
    COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.VmdPortFunc[Index], VmdPeiConfig->VmdPortEnable[Index].RpFunction, SaSetup.VmdPortFunc[Index], Index);
   }
  COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.VmdCfgBarSize,  VmdPeiConfig->VmdCfgBarSize,  SaSetup.VmdCfgBarSize,  NullIndex);
  COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.VmdCfgBarAttr,  VmdPeiConfig->VmdCfgBarAttr,  SaSetup.VmdCfgBarAttr,  NullIndex);
  COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.VmdMemBarSize1, VmdPeiConfig->VmdMemBarSize1, SaSetup.VmdMemBarSize1, NullIndex);
  COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.VmdMemBar1Attr, VmdPeiConfig->VmdMemBar1Attr, SaSetup.VmdMemBar1Attr, NullIndex);
  COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.VmdMemBarSize2, VmdPeiConfig->VmdMemBarSize2, SaSetup.VmdMemBarSize2, NullIndex);
  COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.VmdMemBar2Attr, VmdPeiConfig->VmdMemBar2Attr, SaSetup.VmdMemBar2Attr, NullIndex);
  UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.VmdCfgBarBase,  VmdPeiConfig->VmdCfgBarBase,  (UINTN)PcdGet32(PcdVmdCfgBarBase));
  UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.VmdMemBar1Base, VmdPeiConfig->VmdMemBar1Base, (UINTN)PcdGet32(PcdVmdMemBar1Base));
  UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.VmdMemBar2Base, VmdPeiConfig->VmdMemBar2Base, (UINTN)PcdGet32(PcdVmdMemBar2Base));

  // Read VmdVariable
  VarSize = 0;
  VmdVariablePtr = NULL;
  Status = VariableServices->GetVariable (
                              VariableServices,
                              EFI_VMD_OS_VARIABLE_NAME,
                              &gEfiVmdFeatureVariableGuid,
                              NULL,
                              &VarSize,
                              VmdVariablePtr
                              );
  if (Status == EFI_BUFFER_TOO_SMALL) {
    VmdVariablePtr = AllocateZeroPool (VarSize);
    DEBUG ((DEBUG_VERBOSE, "VMD VARIABLE_BUFFER_TOO_SMALL\n"));
    ASSERT (VmdVariablePtr != NULL);

    Status = VariableServices->GetVariable (
                                  VariableServices,
                                  EFI_VMD_OS_VARIABLE_NAME,
                                  &gEfiVmdFeatureVariableGuid,
                                  NULL,
                                  &VarSize,
                                  VmdVariablePtr
                                  );
    if (EFI_ERROR (Status)) {
      DEBUG ((DEBUG_INFO, "Vmd OS Variable not found Status is %r\n", Status));
    }
  }
//[-start-200903-IB17040149-add]//
#if FixedPcdGet8(PcdModifyVmdPortConfigViaScuDefault)
  if (SaSetup.VmdEnable) {
    if (EFI_ERROR (Status)) { // gEfiVmdFeatureVariableGuid status
      //
      // Cannot get EFI_VMD_OS_VARIABLE_NAME, assumes it's first boot,
      // update the VMD port configuration from Setup default setting.
      //
      VarSize = sizeof (EFI_VMD_OS_DATA);
      VmdVariablePtr = AllocateZeroPool (VarSize);
      VmdVariableDataPtr = (EFI_VMD_OS_DATA *)VmdVariablePtr;
      for (Index=0; Index < VMD_MAX_DEVICES; ++Index) {
        if (SaSetup.VmdPortDev[Index] == 0) {
          VmdVariableDataPtr->BYTE0_1.VREC =Index;
          break;
        }
         if(SaSetup.VmdPort[Index]) {
          VmdVariableDataPtr->DevDetails[Index].RpDevice =SaSetup.VmdPortDev[Index];
          VmdVariableDataPtr->DevDetails[Index].RpFunction =SaSetup.VmdPortFunc[Index];
          DEBUG ((DEBUG_INFO, "SaSetup Storage device RP BDF is %d/%d/%d\n",
          VmdVariableDataPtr->DevDetails[Index].RpBusOrIndex, VmdVariableDataPtr->DevDetails[Index].RpDevice, VmdVariableDataPtr->DevDetails[Index].RpFunction));
         }
      }
    } else { // gEfiVmdFeatureVariableGuid status success
      //
      // The EFI_VMD_OS_VARIABLE_NAME variable is exist, but it's empty
      // So, update the VMD port configuration from Setup default setting.
      //
      VmdVariableDataPtr = (EFI_VMD_OS_DATA *)VmdVariablePtr;
      if (VmdVariableDataPtr->BYTE0_1.VREC == 0 && VmdVariableDataPtr->DevDetails[0].RpDevice == 0){
        for (Index=0; Index < VMD_MAX_DEVICES; ++Index) {
          if (SaSetup.VmdPortDev[Index] == 0) {
            VmdVariableDataPtr->BYTE0_1.VREC =Index;
            break;
          }
           if(SaSetup.VmdPort[Index]) {
            VmdVariableDataPtr->DevDetails[Index].RpDevice =SaSetup.VmdPortDev[Index];
            VmdVariableDataPtr->DevDetails[Index].RpFunction =SaSetup.VmdPortFunc[Index];
            DEBUG ((DEBUG_INFO, "SaSetup Storage device RP BDF is %d/%d/%d\n",
            VmdVariableDataPtr->DevDetails[Index].RpBusOrIndex, VmdVariableDataPtr->DevDetails[Index].RpDevice, VmdVariableDataPtr->DevDetails[Index].RpFunction));
           }
        }
      } else {
        DEBUG ((DEBUG_INFO, "Vmd OS Variable has been set by the OS, so follow the system config\n"));
      }
    }
  }
#endif
//[-end-200903-IB17040149-add]//
#if FixedPcdGet8(PcdFspModeSelection) == 1
        ((FSPS_UPD *) FspsUpd)->FspsConfig.VmdVariablePtr = (UINT32) VmdVariablePtr;
        DEBUG ((DEBUG_INFO, "VmdVariablePtr from PeiGetSectionFromFv is 0x%x\n", ((FSPS_UPD *) FspsUpd)->FspsConfig.VmdVariablePtr));
#else
        VmdPeiConfig->VmdVariablePtr = VmdVariablePtr;
        DEBUG ((DEBUG_INFO, "VmdVariablePtr from PeiGetSectionFromFv is 0x%x\n", VmdPeiConfig->VmdVariablePtr));
#endif
#endif

  COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.CpuCrashLogEnable, TelemetryPeiConfig->CpuCrashLogEnable, SetupData.EnableCrashLog, NullIndex);
  //
  // BIOS-IOM Interaction policy update
  //
#if FixedPcdGetBool (PcdITbtEnable) == 1
  COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.UsbOverride,  TcssPeiConfig->IomConfig.IomInterface.UsbOverride,  SaSetup.UsbOverride,     NullIndex);
  if (SetupData.DeepestUSBSleepWakeCapability == 3) {
    //
    // Wake Capability is S3
    //
    COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.VccSt,        TcssPeiConfig->IomConfig.IomInterface.VccSt,        0, NullIndex);
  } else {
    COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.VccSt,        TcssPeiConfig->IomConfig.IomInterface.VccSt,        SaSetup.TcssVccstStatus, NullIndex);
  }
  COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.D3HotEnable,  TcssPeiConfig->IomConfig.IomInterface.D3HotEnable,  SaSetup.D3HotEnable,     NullIndex);
  COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.D3ColdEnable, TcssPeiConfig->IomConfig.IomInterface.D3ColdEnable, SaSetup.D3ColdEnable,    NullIndex);
  COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.TcCstateLimit,TcssPeiConfig->IomConfig.TcStateLimit,              SaSetup.TcStateLimit,    NullIndex);
  if (SaSetup.TcColdPowerSavingFactorSwitch) {
    COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.IomStayInTCColdSeconds, TcssPeiConfig->MiscConfig.IomStayInTCColdSeconds, SaSetup.IomStayInTCColdSeconds, NullIndex);
    COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.IomBeforeEnteringTCColdSeconds, TcssPeiConfig->MiscConfig.IomBeforeEnteringTCColdSeconds, SaSetup.IomBeforeEnteringTCColdSeconds, NullIndex);
  } else {
    COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.IomStayInTCColdSeconds, TcssPeiConfig->MiscConfig.IomStayInTCColdSeconds, 0, NullIndex);
    COMPARE_AND_UPDATE_POLICY (((FSPS_UPD * )FspsUpd)->FspsConfig.IomBeforeEnteringTCColdSeconds, TcssPeiConfig->MiscConfig.IomBeforeEnteringTCColdSeconds, 0, NullIndex);
  }
  //
  // Itbt PCI Root Port Policy Initialization
  //
  for (Index = 0; Index < MAX_ITBT_PCIE_PORT; Index++) {
    COMPARE_AND_UPDATE_POLICY(((FSPS_UPD *)FspsUpd)->FspsConfig.PtmEnabled[Index],                                 TcssPeiConfig->PciePolicy.PciePortPolicy[Index].PtmEnabled, SaSetup.PtmEnabled[Index], NullIndex);
    ///
    /// LTR Settings
    ///
    COMPARE_AND_UPDATE_POLICY(((FSPS_UPD *)FspsUpd)->FspsConfig.SaPcieItbtRpLtrEnable[Index],                         TcssPeiConfig->PciePolicy.PciePortPolicy[Index].LtrEnable,                         SaSetup.SaPcieItbtLtrEnable[Index],                         Index);
    COMPARE_AND_UPDATE_POLICY(((FSPS_UPD *)FspsUpd)->FspsConfig.SaPcieItbtRpSnoopLatencyOverrideMode[Index],          TcssPeiConfig->PciePolicy.PciePortPolicy[Index].SnoopLatencyOverrideMode,          SaSetup.SaPcieItbtSnoopLatencyOverrideMode[Index],          Index);
    COMPARE_AND_UPDATE_POLICY(((FSPS_UPD *)FspsUpd)->FspsConfig.SaPcieItbtRpSnoopLatencyOverrideMultiplier[Index],    TcssPeiConfig->PciePolicy.PciePortPolicy[Index].SnoopLatencyOverrideMultiplier,    SaSetup.SaPcieItbtSnoopLatencyOverrideMultiplier[Index],    Index);
    COMPARE_AND_UPDATE_POLICY(((FSPS_UPD *)FspsUpd)->FspsConfig.SaPcieItbtRpSnoopLatencyOverrideValue[Index],         TcssPeiConfig->PciePolicy.PciePortPolicy[Index].SnoopLatencyOverrideValue,         SaSetup.SaPcieItbtSnoopLatencyOverrideValue[Index],         Index);
    COMPARE_AND_UPDATE_POLICY(((FSPS_UPD *)FspsUpd)->FspsConfig.SaPcieItbtRpNonSnoopLatencyOverrideMode[Index],       TcssPeiConfig->PciePolicy.PciePortPolicy[Index].NonSnoopLatencyOverrideMode,       SaSetup.SaPcieItbtNonSnoopLatencyOverrideMode[Index],       Index);
    COMPARE_AND_UPDATE_POLICY(((FSPS_UPD *)FspsUpd)->FspsConfig.SaPcieItbtRpNonSnoopLatencyOverrideMultiplier[Index], TcssPeiConfig->PciePolicy.PciePortPolicy[Index].NonSnoopLatencyOverrideMultiplier, SaSetup.SaPcieItbtNonSnoopLatencyOverrideMultiplier[Index], Index);
    COMPARE_AND_UPDATE_POLICY(((FSPS_UPD *)FspsUpd)->FspsConfig.SaPcieItbtRpNonSnoopLatencyOverrideValue[Index],      TcssPeiConfig->PciePolicy.PciePortPolicy[Index].NonSnoopLatencyOverrideValue,      SaSetup.SaPcieItbtNonSnoopLatencyOverrideValue[Index],      Index);
    COMPARE_AND_UPDATE_POLICY(((FSPS_UPD *)FspsUpd)->FspsConfig.SaPcieItbtRpForceLtrOverride[Index],                  TcssPeiConfig->PciePolicy.PciePortPolicy[Index].ForceLtrOverride,                  SaSetup.SaPcieItbtForceLtrOverride[Index],                  Index);
    COMPARE_AND_UPDATE_POLICY(((FSPS_UPD *)FspsUpd)->FspsConfig.SaPcieItbtRpLtrConfigLock[Index],                     TcssPeiConfig->PciePolicy.PciePortPolicy[Index].LtrConfigLock,                     SaSetup.SaPcieItbtLtrConfigLock[Index],                     Index);
  }
  //
  // BIOS-PMC Interaction policy update
  //
  if ((SetupData.UsbcBiosTcssHandshake == 1) && (PcdGetBool (PcdBoardPmcPdEnable))) {
    COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.PmcPdEnable, TcssPeiConfig->IomConfig.PmcInterface.PmcPdEnable, SetupData.UsbcBiosTcssHandshake, NullIndex);
  } else {
    UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.PmcPdEnable, TcssPeiConfig->IomConfig.PmcInterface.PmcPdEnable, 0);
  }

  //
  // TCSS Type C Cpu Xhci Port x Convert to TypeA
  //
  DEBUG ((DEBUG_INFO, "Update TCSS Policy to Support TCSS Type C Port x Convert to TypeA\n"));
  for (Index = 0; Index < MAX_TCSS_USB3_PORTS; Index++) {
    PchUsb2PortNo = TcssGetPchUsb2PortNoMappedWithCpuXhciPortIndex (Index);
    if (PchUsb2PortNo > 0 && PchUsb2PortNo < 0x10) { // PCH USB2 Port No may vary from 1 to 15.
      COMPARE_AND_UPDATE_POLICY (\
        ((FSPS_UPD *) FspsUpd)->FspsConfig.EnableTcssCovTypeA[Index],\
        TcssPeiConfig->MiscConfig.EnableTcssCovTypeA[Index],\
        TRUE,\
        Index\
        );
      COMPARE_AND_UPDATE_POLICY (\
        ((FSPS_UPD *) FspsUpd)->FspsConfig.MappingPchXhciUsbA[Index],\
        TcssPeiConfig->MiscConfig.MappingPchXhciUsbA[Index],\
        PchUsb2PortNo,\
        Index\
        );
    }
  }
#endif  // PcdITbtEnable
  //
  // PCI express config
  //
#if FixedPcdGetBool(PcdCpuPcieEnable) == 1
  COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.CpuPcieFiaProgramming, CpuPcieRpConfig->FiaProgramming, SaSetup.PcieFiaProgramming,      NullIndex);
  COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.CpuPcieComplianceTestMode, CpuPcieRpConfig->PcieCommonConfig.ComplianceTestMode, SaSetup.PcieComplianceTestMode, NullIndex);
  COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.CpuPcieSlotSelection, CpuPcieRpConfig->SlotSelection, SaSetup.PcieSlotSelection, NullIndex);
  MaxPciePorts = GetMaxCpuPciePortNum ();
  for (Index = 0; Index < MaxPciePorts; Index++) {
    UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.CpuPcieRpMaxPayload[Index],                     CpuPcieRpConfig->RootPort[Index].PcieRpCommonConfig.MaxPayload,               CpuPcieMaxPayload256);
    UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.CpuPcieRpPhysicalSlotNumber[Index],             CpuPcieRpConfig->RootPort[Index].PcieRpCommonConfig.PhysicalSlotNumber,       (UINT8) Index);
    COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.CpuPcieRpAspm[Index],                          CpuPcieRpConfig->RootPort[Index].PcieRpCommonConfig.Aspm,                          SaSetup.PcieRootPortAspm[Index],              Index);
    COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.CpuPcieRpL1Substates[Index],                   CpuPcieRpConfig->RootPort[Index].PcieRpCommonConfig.L1Substates,                   SaSetup.PcieRootPortL1SubStates[Index],       Index);
    COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.CpuPcieRpGen3EqPh3Method[Index],               CpuPcieRpConfig->RootPort[Index].PcieRpCommonConfig.Gen3EqPh3Method,               SaSetup.PcieRootPortEqPh3Method[Index],       Index);
    COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.CpuPcieRpAcsEnabled[Index],                    CpuPcieRpConfig->RootPort[Index].PcieRpCommonConfig.AcsEnabled,                    SaSetup.PcieRootPortACS[Index],               Index);
    COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.CpuPcieRpPtmEnabled[Index],                    CpuPcieRpConfig->RootPort[Index].PcieRpCommonConfig.PtmEnabled ,                   SaSetup.PcieRootPortPTM[Index],               Index);
    COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.CpuPcieRpDpcEnabled[Index],                    CpuPcieRpConfig->RootPort[Index].PcieRpCommonConfig.DpcEnabled ,                   SaSetup.PcieRootPortDPC[Index],               Index);
    COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.CpuPcieRpDpcExtensionsEnabled[Index],          CpuPcieRpConfig->RootPort[Index].PcieRpCommonConfig.RpDpcExtensionsEnabled ,       SaSetup.PcieRootPortEDPC[Index],              Index);
    COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.CpuPcieRpMultiVcEnabled[Index],                CpuPcieRpConfig->RootPort[Index].MultiVcEnabled ,                                  SaSetup.PcieRootPortMultiVc[Index],           Index);
    COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.CpuPcieRpPeerToPeerMode[Index],                CpuPcieRpConfig->RootPort[Index].PcieRpCommonConfig.EnablePeerMemoryWrite,         SaSetup.CpuPcieRootPortPeerToPeerMode[Index], Index);
    COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.CpuPcieFomsCp[Index],                          CpuPcieRpConfig->RootPort[Index].FomsCp,                                           SaSetup.CpuPcieFomsCp[Index],                 Index);
    COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.CpuPcieRpSlotImplemented[Index],               CpuPcieRpConfig->RootPort[Index].PcieRpCommonConfig.SlotImplemented,               SaSetup.PcieRootPortSI[Index],                Index);
    COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.CpuPcieRpPmSci[Index],                         CpuPcieRpConfig->RootPort[Index].PcieRpCommonConfig.PmSci,                         SaSetup.PcieRootPortPMCE[Index],              Index);
    COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.CpuPcieRpHotPlug[Index],                       CpuPcieRpConfig->RootPort[Index].PcieRpCommonConfig.HotPlug,                       SaSetup.PcieRootPortHPE[Index],               Index);
    COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.CpuPcieRpAdvancedErrorReporting[Index],        CpuPcieRpConfig->RootPort[Index].PcieRpCommonConfig.AdvancedErrorReporting,        SaSetup.PcieRootPortAER[Index],               Index);
    COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.CpuPcieRpUnsupportedRequestReport[Index],      CpuPcieRpConfig->RootPort[Index].PcieRpCommonConfig.UnsupportedRequestReport,      SaSetup.PcieRootPortURE[Index],               Index);
    COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.CpuPcieRpFatalErrorReport[Index],              CpuPcieRpConfig->RootPort[Index].PcieRpCommonConfig.FatalErrorReport,              SaSetup.PcieRootPortFEE[Index],               Index);
    COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.CpuPcieRpNoFatalErrorReport[Index],            CpuPcieRpConfig->RootPort[Index].PcieRpCommonConfig.NoFatalErrorReport,            SaSetup.PcieRootPortNFE[Index],               Index);
    COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.CpuPcieRpCorrectableErrorReport[Index],        CpuPcieRpConfig->RootPort[Index].PcieRpCommonConfig.CorrectableErrorReport,        SaSetup.PcieRootPortCEE[Index],               Index);
    COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.CpuPcieRpSystemErrorOnFatalError[Index],       CpuPcieRpConfig->RootPort[Index].PcieRpCommonConfig.SystemErrorOnFatalError,       SaSetup.PcieRootPortSFE[Index],               Index);
    COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.CpuPcieRpSystemErrorOnNonFatalError[Index],    CpuPcieRpConfig->RootPort[Index].PcieRpCommonConfig.SystemErrorOnNonFatalError,    SaSetup.PcieRootPortSNE[Index],               Index);
    COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.CpuPcieRpSystemErrorOnCorrectableError[Index], CpuPcieRpConfig->RootPort[Index].PcieRpCommonConfig.SystemErrorOnCorrectableError, SaSetup.PcieRootPortSCE[Index],               Index);
    COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.CpuPcieRpTransmitterHalfSwing[Index],          CpuPcieRpConfig->RootPort[Index].PcieRpCommonConfig.TransmitterHalfSwing,          SaSetup.PcieRootPortTHS[Index],               Index);
    COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.CpuPcieRpGen3Uptp[Index],                      CpuPcieRpConfig->RootPort[Index].Gen3Uptp,                                         SaSetup.PcieRootPortGen3Uptp[Index],          Index);
    COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.CpuPcieRpGen3Dptp[Index],                      CpuPcieRpConfig->RootPort[Index].Gen3Dptp,                                         SaSetup.PcieRootPortGen3Dptp[Index],          Index);
    COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.CpuPcieRpGen4Uptp[Index],                      CpuPcieRpConfig->RootPort[Index].Gen4Uptp,                                         SaSetup.PcieRootPortGen4Uptp[Index],          Index);
    COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.CpuPcieRpGen4Dptp[Index],                      CpuPcieRpConfig->RootPort[Index].Gen4Dptp,                                         SaSetup.PcieRootPortGen4Dptp[Index],          Index);
    COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.CpuPcieRpGen5Uptp[Index],                      CpuPcieRpConfig->RootPort[Index].Gen5Uptp,                                         SaSetup.PcieRootPortGen5Uptp[Index],          Index);
    COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.CpuPcieRpGen5Dptp[Index],                      CpuPcieRpConfig->RootPort[Index].Gen5Dptp,                                         SaSetup.PcieRootPortGen5Dptp[Index],          Index);
    COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.CpuPcieRpDetectTimeoutMs[Index],               CpuPcieRpConfig->RootPort[Index].PcieRpCommonConfig.DetectTimeoutMs,               SaSetup.PcieDetectTimeoutMs[Index],           Index);
    COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.CpuPcieClockGating[Index],                     CpuPcieRpConfig->RootPort[Index].PcieRpCommonConfig.ClockGating,                   SaSetup.PcieRootPortClockGating[Index],       Index);
    COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.CpuPcieFunc0LinkDisable[Index],                CpuPcieRpConfig->RootPort2[Index].Func0LinkDisable,                                SaSetup.PcieFunc0LinkDisable[Index],          Index);
    if (SaSetup.PcieComplianceTestMode == 1) {
      DEBUG ((DEBUG_INFO , "Compliance Test Mode is enabled!!! Disabling PCI Express PowerGating\n"));
      UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.CpuPciePowerGating[Index],                               CpuPcieRpConfig->RootPort[Index].PcieRpCommonConfig.PowerGating,                   0);
    } else {
      COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.CpuPciePowerGating[Index],                   CpuPcieRpConfig->RootPort[Index].PcieRpCommonConfig.PowerGating,                   SaSetup.PcieRootPortPowerGating[Index],              Index);
    }
  }
  COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.CpuPcieRpHotPlug[1], CpuPcieRpConfig->RootPort[1].PcieRpCommonConfig.HotPlug, 0, 1);

  for (Index = 0; Index < GetMaxCpuPciePortNum (); Index++) {
    COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.CpuPcieRpLtrConfigLock[Index],                 CpuPcieRpConfig->RootPort[Index].PcieRpCommonConfig.PcieRpLtrConfig.LtrConfigLock, SaSetup.CpuPcieLtrConfigLock[Index],          Index);
#if FixedPcdGet8(PcdFspModeSelection) == 1
      ((FSPS_UPD *) FspsUpd)->FspsConfig.CpuPcieRpLtrMaxSnoopLatency[Index]   = 0x100F;
      ((FSPS_UPD *) FspsUpd)->FspsConfig.CpuPcieRpLtrMaxNoSnoopLatency[Index] = 0x100F;
#endif
    COMPARE_AND_UPDATE_POLICY (((FSPS_UPD *) FspsUpd)->FspsConfig.CpuPcieRpLtrEnable[Index],                     CpuPcieRpConfig->RootPort[Index].PcieRpCommonConfig.LtrEnable,                     SaSetup.CpuPcieLtrEnable[Index],              Index);
  }
#endif
  return EFI_SUCCESS;
}
