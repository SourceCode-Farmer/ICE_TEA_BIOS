/** @file
  BaseEcTcss library implementation.

@copyright
  INTEL CONFIDENTIAL
  Copyright 2017 - 2020 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains 'Framework Code' and is licensed as such under the terms
  of your license agreement with Intel or your vendor. This file may not be
  modified, except as allowed by additional terms of your license agreement.

@par Specification Reference:
**/
#include <Library/EcLib.h>
#include <Library/EcTcssLib.h>
#include <Library/DebugLib.h>

//[-start-211109-BAIN000054-add]//
#if defined(C970_SUPPORT)||defined(C770_SUPPORT)||defined(S77013_SUPPORT)||defined(S77014_SUPPORT)||defined(S77014IAH_SUPPORT)
#include <Library/LfcEcLib.h>
#include <Library/TimerLib.h>
#endif
//[-end-211109-BAIN000054-add]//

/**
  USBC IOM Ready Notify

  @param[in][out]  DataBuffer

  @retval          EFI_SUCCESS       Command success
  @retval          EFI_DEVICE_ERROR  Command error
**/
EFI_STATUS
UsbcIomReadyNotify (
  IN OUT UINT8     *DataBuffer
  )
{
  return EFI_SUCCESS;
}

/**
  Usbc Retimer Compliance Mode

  @param[in][out]  DataBuffer

  @retval          EFI_SUCCESS       Command success
  @retval          EFI_DEVICE_ERROR  Command error
**/
EFI_STATUS
UsbcRetimerCompliancePDMessage  (
  IN OUT UINT8     *DataBuffer
  )
{
  return EFI_SUCCESS;
}

/**
  USBC Get USB Connection Status

  @param[in][out]  DataBuffer
  @param[in]       Timeout

  @retval          EFI_SUCCESS       Command success
  @retval          EFI_DEVICE_ERROR  Command error
**/
EFI_STATUS
UsbcGetUsbConnStatus (
  IN OUT UINT8     *DataBuffer,
  IN     UINT32    Timeout
  )
{
  return EFI_SUCCESS;
}

/**
  USBC Sx Entry Wait

  @param[in][out]  DataBuffer
  @param[in]       Timeout

  @retval          EFI_SUCCESS       Command success
  @retval          EFI_DEVICE_ERROR  Command error
**/
EFI_STATUS
UsbcSxEntry (
  IN OUT UINT8     *DataBuffer,
  IN     UINT32    Timeout
  )
{
  return EFI_SUCCESS;
}

  /**
  USBC Sx Exit Wait

  @param[out]     DataBuffer
  @param[in]       Timeout

  @retval         EFI_SUCCESS         Command success
  @retval         EFI_DEVICE_ERROR    Command error
**/
EFI_STATUS
UsbcSxExit (
  OUT    UINT8     *DataBuffer,
  IN     UINT32    Timeout
  )
{
  return EFI_SUCCESS;
}

/**
  Set PD Controller Mode

  @param[in]      PdControllerMode
  @param[in]      PdControllerNumber

  @retval         EFI_SUCCESS            Command success
  @retval         EFI_TIMEOUT            Command timeout
  @retval         EFI_DEVICE_ERROR       Command error
  @retval         EFI_INVALID_PARAMETER  Command usage error.
**/
EFI_STATUS
SetPdControllerMode (
  IN     UINT8     PdControllerMode,
  IN     UINT8     PdControllerNumber
  )
{
//[-start-211109-BAIN000054-modify]//
#if defined(C970_SUPPORT)||defined(C770_SUPPORT)||defined(S77013_SUPPORT)||defined(S77014_SUPPORT)||defined(S77014IAH_SUPPORT)
  EFI_STATUS    Status = EFI_SUCCESS;
  
  Status = LfcEcLibSetPdControllerMode(PdControllerMode);
  if(EFI_ERROR(Status)){
   return Status;
  }

  MicroSecondDelay(100000);

  return Status;
#else
  return EFI_SUCCESS;
#endif
//[-end-211109-BAIN000054-modify]//
}

/**
  Get PD Controller Mode

  @param[out]     *PdControllerModeBuffer
  @param[in]      PdControllerNumber
  @param[in]      TotalCountOfPdController

  @retval         EFI_SUCCESS            Command success
  @retval         EFI_TIMEOUT            Command timeout
  @retval         EFI_DEVICE_ERROR       Command error
  @retval         EFI_INVALID_PARAMETER  Command usage error.
**/
EFI_STATUS
GetPdControllerMode (
  OUT    UINT8     *PdControllerModeBuffer,
  IN     UINT8     PdControllerNumber,
  IN     UINT32    TotalCountOfPdController
  )

{
//[-start-211109-BAIN000054-modify]//
#if defined(C970_SUPPORT)||defined(C770_SUPPORT)||defined(S77013_SUPPORT)||defined(S77014_SUPPORT)||defined(S77014IAH_SUPPORT)
  EFI_STATUS    Status = EFI_SUCCESS;
  UINT8         PdControllerMode = 0;

  if (PdControllerModeBuffer == NULL) {
    DEBUG ((DEBUG_INFO, "\nGetPdControllerMode Returning as Null Buffer Passed\n"));
    return EFI_INVALID_PARAMETER;
  }

  Status = LfcEcLibGetPdControllerMode(&PdControllerMode);
  if(EFI_ERROR(Status)){
    return Status;
  }

  MicroSecondDelay(100000);

  *PdControllerModeBuffer = PdControllerMode;

  return  Status;
#else
  return EFI_SUCCESS;
#endif
//[-end-211109-BAIN000054-modify]//
}
