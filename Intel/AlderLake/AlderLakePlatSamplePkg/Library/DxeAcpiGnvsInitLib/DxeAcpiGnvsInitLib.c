/** @file
 
;******************************************************************************
;* Copyright 2021 Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corp.
;*
;******************************************************************************
*/
/** @file
  Acpi Gnvs Init Library.

@copyright
  INTEL CONFIDENTIAL
  Copyright 1999 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains a 'Sample Driver' and is licensed as such under the terms
  of your license agreement with Intel or your vendor. This file may be modified
  by the user, subject to the additional terms of the license agreement.

@par Specification Reference:
**/

#include <Uefi.h>
#include <Library/IoLib.h>
#include <Library/DebugLib.h>
#include <Library/BaseMemoryLib.h>
#include <Library/UefiBootServicesTableLib.h>

#include <Protocol/PlatformNvsArea.h>
#include <Library/DxeAcpiGnvsInitLib.h>

/**
@brief
  Global NVS initialize.

  @param[in] PlatformNvsAreaPtr    - Pointer of global NVS area

  @retval EFI_SUCCESS              - Allocate Global NVS completed.
  @retval EFI_OUT_OF_RESOURCES     - Failed to allocate required page for PlatformNvsAreaProtocol.
**/
EFI_STATUS
EFIAPI
AcpiGnvsInit (
  IN OUT VOID               **PlatformNvsAreaPtr
  )
{
//[-start-200925-IB17040171-remove]//
  // UINTN                         Pages;
  // EFI_PHYSICAL_ADDRESS          Address;
  // EFI_STATUS                    Status;
  PLATFORM_NVS_AREA_PROTOCOL    *PlatformNvsAreaProtocol;

// We should not allocate memory space here, because this memory space has been allocated at DxePlatform.
  // Pages = EFI_SIZE_TO_PAGES (sizeof (PLATFORM_NVS_AREA));
  // Address = 0xffffffff; // allocate address below 4G.

  // Status  = gBS->AllocatePages (
  //                  AllocateMaxAddress,
  //                  EfiACPIMemoryNVS,
  //                  Pages,
  //                  &Address
  //                  );
  // ASSERT_EFI_ERROR (Status);
  // if (EFI_ERROR(Status)) {
  //   return Status;
  // }

  // *PlatformNvsAreaPtr = (VOID *) (UINTN) Address;
  // SetMem (*PlatformNvsAreaPtr, sizeof (PLATFORM_NVS_AREA), 0);
//[-end-200925-IB17040171-remove]//
  //
  // PlatformNvsAreaProtocol default value init here...
  //
//[-start-200925-IB17040171-modify]//
  PlatformNvsAreaProtocol = (PLATFORM_NVS_AREA_PROTOCOL *) PlatformNvsAreaPtr;
//[-end-200925-IB17040171-modify]//

  //
  // Thermal trip points
  //
  PlatformNvsAreaProtocol->Area->Ac0TripPoint                = 71;
  PlatformNvsAreaProtocol->Area->Ac1TripPoint                = 55;
  PlatformNvsAreaProtocol->Area->Ac0FanSpeed                 = 100;
  PlatformNvsAreaProtocol->Area->Ac1FanSpeed                 = 75;
  PlatformNvsAreaProtocol->Area->PassiveThermalTripPoint     = 95;
  PlatformNvsAreaProtocol->Area->PassiveTc1Value             = 1;
  PlatformNvsAreaProtocol->Area->PassiveTc2Value             = 5;
  PlatformNvsAreaProtocol->Area->PassiveTspValue             = 10;
  PlatformNvsAreaProtocol->Area->CriticalThermalTripPoint    = 119;

  //
  // Intel(R) Dynamic Tuning Technology Devices and trip points
  //
  PlatformNvsAreaProtocol->Area->EnableDptf                   = 1;
  PlatformNvsAreaProtocol->Area->EnableSaDevice               = 1;
  PlatformNvsAreaProtocol->Area->PpccStepSize                 = 500;

  PlatformNvsAreaProtocol->Area->EnableChargerParticipant     = 1;

  PlatformNvsAreaProtocol->Area->EnableBatteryParticipant    = 1;
  PlatformNvsAreaProtocol->Area->EnableInt3400Device         = 1;

  PlatformNvsAreaProtocol->Area->EnableSen1Participant        = 1;

  PlatformNvsAreaProtocol->Area->EnableSen2Participant        = 1;

  PlatformNvsAreaProtocol->Area->EnableSen3Participant        = 1;

  PlatformNvsAreaProtocol->Area->EnableSen4Participant        = 1;

  PlatformNvsAreaProtocol->Area->EnableSen5Participant        = 1;

  PlatformNvsAreaProtocol->Area->EnableDgpuParticipant        = 1;

  PlatformNvsAreaProtocol->Area->EnablePowerParticipant       = 1;

  PlatformNvsAreaProtocol->Area->EnablePchFivrParticipant = 1;

  PlatformNvsAreaProtocol->Area->OemDesignVariable0           = 0;
  PlatformNvsAreaProtocol->Area->OemDesignVariable1           = 0;
  PlatformNvsAreaProtocol->Area->OemDesignVariable2           = 0;
  PlatformNvsAreaProtocol->Area->OemDesignVariable3           = 0;
  PlatformNvsAreaProtocol->Area->OemDesignVariable4           = 0;
  PlatformNvsAreaProtocol->Area->OemDesignVariable5           = 0;
//[-start-210701-KEBIN00030-modify]//
#ifdef LCFC_SUPPORT
  PlatformNvsAreaProtocol->Area->OemDesignVariable6           = 0;
  PlatformNvsAreaProtocol->Area->OemDesignVariable7           = 0;
  PlatformNvsAreaProtocol->Area->OemDesignVariable8           = 0;
  PlatformNvsAreaProtocol->Area->OemDesignVariable9           = 0;
  PlatformNvsAreaProtocol->Area->OemDesignVariable10          = 0;
  PlatformNvsAreaProtocol->Area->OemDesignVariable11          = 0;
  PlatformNvsAreaProtocol->Area->OemDesignVariable12          = 0;
  PlatformNvsAreaProtocol->Area->OemDesignVariable13          = 0;
  PlatformNvsAreaProtocol->Area->OemDesignVariable14          = 0;
  PlatformNvsAreaProtocol->Area->OemDesignVariable15          = 0;
  PlatformNvsAreaProtocol->Area->OemDesignVariable16          = 0;
  PlatformNvsAreaProtocol->Area->OemDesignVariable17          = 0;
  PlatformNvsAreaProtocol->Area->OemDesignVariable18          = 0;
  PlatformNvsAreaProtocol->Area->OemDesignVariable19          = 0;
  PlatformNvsAreaProtocol->Area->OemDesignVariable20          = 0;
#endif
//[-end-210701-KEBIN00030-modify]//

  //
  // Wireless
  //
  PlatformNvsAreaProtocol->Area->PowerSharingManagerEnable    = 1;
  PlatformNvsAreaProtocol->Area->PsmSplcDomainType1           = 0x09;
  PlatformNvsAreaProtocol->Area->PsmSplcPowerLimit1           = 4000;
  PlatformNvsAreaProtocol->Area->PsmSplcTimeWindow1           = 30000;

  PlatformNvsAreaProtocol->Area->PsmDplcDomainType1           = 9;
  PlatformNvsAreaProtocol->Area->PsmDplcDomainPreference1     = 9;
  PlatformNvsAreaProtocol->Area->PsmDplcPowerLimitIndex1      = 0;
  PlatformNvsAreaProtocol->Area->PsmDplcDefaultPowerLimit1    = 1200;
  PlatformNvsAreaProtocol->Area->PsmDplcDefaultTimeWindow1    = 30000;
  PlatformNvsAreaProtocol->Area->PsmDplcMinimumPowerLimit1    = 1200;
  PlatformNvsAreaProtocol->Area->PsmDplcMaximumPowerLimit1    = 1200;
  PlatformNvsAreaProtocol->Area->PsmDplcMaximumTimeWindow1    = 1000;

  PlatformNvsAreaProtocol->Area->WifiEnable                   = 1;
  PlatformNvsAreaProtocol->Area->WifiDomainType1              = 0x7;
  PlatformNvsAreaProtocol->Area->WifiPowerLimit1              = 0xFFFF;
  PlatformNvsAreaProtocol->Area->WifiTimeWindow1              = 30000;
  PlatformNvsAreaProtocol->Area->TRxDelay0                    = 50;
  PlatformNvsAreaProtocol->Area->TRxCableLength0              = 50;
  PlatformNvsAreaProtocol->Area->TRxDelay1                    = 50;
  PlatformNvsAreaProtocol->Area->TRxCableLength1              = 50;
  PlatformNvsAreaProtocol->Area->WrddDomainType1              = 0x7;
  PlatformNvsAreaProtocol->Area->WrddCountryIndentifier1      = 0x4150; // "AP"
  PlatformNvsAreaProtocol->Area->WrdsWiFiSarEnable            = 0x0;
  PlatformNvsAreaProtocol->Area->WrdsWiFiSarTxPowerSet1Limit1         = 0x0;
  PlatformNvsAreaProtocol->Area->WrdsWiFiSarTxPowerSet1Limit2         = 0x0;
  PlatformNvsAreaProtocol->Area->WrdsWiFiSarTxPowerSet1Limit3         = 0x0;
  PlatformNvsAreaProtocol->Area->WrdsWiFiSarTxPowerSet1Limit4         = 0x0;
  PlatformNvsAreaProtocol->Area->WrdsWiFiSarTxPowerSet1Limit5         = 0x0;
  PlatformNvsAreaProtocol->Area->WrdsWiFiSarTxPowerSet1Limit6         = 0x0;
  PlatformNvsAreaProtocol->Area->WrdsWiFiSarTxPowerSet1Limit7         = 0x0;
  PlatformNvsAreaProtocol->Area->WrdsWiFiSarTxPowerSet1Limit8         = 0x0;
  PlatformNvsAreaProtocol->Area->WrdsWiFiSarTxPowerSet1Limit9         = 0x0;
  PlatformNvsAreaProtocol->Area->WrdsWiFiSarTxPowerSet1Limit10        = 0x0;
  PlatformNvsAreaProtocol->Area->WrdsWiFiSarTxPowerSet1Limit11        = 0x0;
  PlatformNvsAreaProtocol->Area->WrdsWiFiSarTxPowerSet1Limit12        = 0x0;
  PlatformNvsAreaProtocol->Area->WrdsWiFiSarTxPowerSet1Limit13        = 0x0;
  PlatformNvsAreaProtocol->Area->WrdsWiFiSarTxPowerSet1Limit14        = 0x0;
  PlatformNvsAreaProtocol->Area->WrdsWiFiSarTxPowerSet1Limit15        = 0x0;
  PlatformNvsAreaProtocol->Area->WrdsWiFiSarTxPowerSet1Limit16        = 0x0;
  PlatformNvsAreaProtocol->Area->WrdsWiFiSarTxPowerSet1Limit17        = 0x0;
  PlatformNvsAreaProtocol->Area->WrdsWiFiSarTxPowerSet1Limit18        = 0x0;
  PlatformNvsAreaProtocol->Area->WrdsWiFiSarTxPowerSet1Limit19        = 0x0;
  PlatformNvsAreaProtocol->Area->WrdsWiFiSarTxPowerSet1Limit20        = 0x0;
  PlatformNvsAreaProtocol->Area->WrdsWiFiSarTxPowerSet1Limit21        = 0x0;
  PlatformNvsAreaProtocol->Area->WrdsWiFiSarTxPowerSet1Limit22        = 0x0;
  PlatformNvsAreaProtocol->Area->WrdsCdbWiFiSarTxPowerSet1Limit1      = 0x0;
  PlatformNvsAreaProtocol->Area->WrdsCdbWiFiSarTxPowerSet1Limit2      = 0x0;
  PlatformNvsAreaProtocol->Area->WrdsCdbWiFiSarTxPowerSet1Limit3      = 0x0;
  PlatformNvsAreaProtocol->Area->WrdsCdbWiFiSarTxPowerSet1Limit4      = 0x0;
  PlatformNvsAreaProtocol->Area->WrdsCdbWiFiSarTxPowerSet1Limit5      = 0x0;
  PlatformNvsAreaProtocol->Area->WrdsCdbWiFiSarTxPowerSet1Limit6      = 0x0;
  PlatformNvsAreaProtocol->Area->WrdsCdbWiFiSarTxPowerSet1Limit7      = 0x0;
  PlatformNvsAreaProtocol->Area->WrdsCdbWiFiSarTxPowerSet1Limit8      = 0x0;
  PlatformNvsAreaProtocol->Area->WrdsCdbWiFiSarTxPowerSet1Limit9      = 0x0;
  PlatformNvsAreaProtocol->Area->WrdsCdbWiFiSarTxPowerSet1Limit10     = 0x0;
  PlatformNvsAreaProtocol->Area->WrdsCdbWiFiSarTxPowerSet1Limit11     = 0x0;
  PlatformNvsAreaProtocol->Area->WrdsCdbWiFiSarTxPowerSet1Limit12     = 0x0;
  PlatformNvsAreaProtocol->Area->WrdsCdbWiFiSarTxPowerSet1Limit13     = 0x0;
  PlatformNvsAreaProtocol->Area->WrdsCdbWiFiSarTxPowerSet1Limit14     = 0x0;
  PlatformNvsAreaProtocol->Area->WrdsCdbWiFiSarTxPowerSet1Limit15     = 0x0;
  PlatformNvsAreaProtocol->Area->WrdsCdbWiFiSarTxPowerSet1Limit16     = 0x0;
  PlatformNvsAreaProtocol->Area->WrdsCdbWiFiSarTxPowerSet1Limit17     = 0x0;
  PlatformNvsAreaProtocol->Area->WrdsCdbWiFiSarTxPowerSet1Limit18     = 0x0;
  PlatformNvsAreaProtocol->Area->WrdsCdbWiFiSarTxPowerSet1Limit19     = 0x0;
  PlatformNvsAreaProtocol->Area->WrdsCdbWiFiSarTxPowerSet1Limit20     = 0x0;
  PlatformNvsAreaProtocol->Area->WrdsCdbWiFiSarTxPowerSet1Limit21     = 0x0;
  PlatformNvsAreaProtocol->Area->WrdsCdbWiFiSarTxPowerSet1Limit22     = 0x0;

  PlatformNvsAreaProtocol->Area->AntennaDiversity                     = 0x2;

  PlatformNvsAreaProtocol->Area->EwrdWiFiDynamicSarEnable             = 0x0;
  PlatformNvsAreaProtocol->Area->EwrdWiFiDynamicSarRangeSets          = 0x0;
  PlatformNvsAreaProtocol->Area->EwrdWiFiSarTxPowerSet2Limit1         = 0x0;
  PlatformNvsAreaProtocol->Area->EwrdWiFiSarTxPowerSet2Limit2         = 0x0;
  PlatformNvsAreaProtocol->Area->EwrdWiFiSarTxPowerSet2Limit3         = 0x0;
  PlatformNvsAreaProtocol->Area->EwrdWiFiSarTxPowerSet2Limit4         = 0x0;
  PlatformNvsAreaProtocol->Area->EwrdWiFiSarTxPowerSet2Limit5         = 0x0;
  PlatformNvsAreaProtocol->Area->EwrdWiFiSarTxPowerSet2Limit6         = 0x0;
  PlatformNvsAreaProtocol->Area->EwrdWiFiSarTxPowerSet2Limit7         = 0x0;
  PlatformNvsAreaProtocol->Area->EwrdWiFiSarTxPowerSet2Limit8         = 0x0;
  PlatformNvsAreaProtocol->Area->EwrdWiFiSarTxPowerSet2Limit9         = 0x0;
  PlatformNvsAreaProtocol->Area->EwrdWiFiSarTxPowerSet2Limit10        = 0x0;
  PlatformNvsAreaProtocol->Area->EwrdWiFiSarTxPowerSet2Limit11        = 0x0;
  PlatformNvsAreaProtocol->Area->EwrdWiFiSarTxPowerSet2Limit12        = 0x0;
  PlatformNvsAreaProtocol->Area->EwrdWiFiSarTxPowerSet2Limit13        = 0x0;
  PlatformNvsAreaProtocol->Area->EwrdWiFiSarTxPowerSet2Limit14        = 0x0;
  PlatformNvsAreaProtocol->Area->EwrdWiFiSarTxPowerSet2Limit15        = 0x0;
  PlatformNvsAreaProtocol->Area->EwrdWiFiSarTxPowerSet2Limit16        = 0x0;
  PlatformNvsAreaProtocol->Area->EwrdWiFiSarTxPowerSet2Limit17        = 0x0;
  PlatformNvsAreaProtocol->Area->EwrdWiFiSarTxPowerSet2Limit18        = 0x0;
  PlatformNvsAreaProtocol->Area->EwrdWiFiSarTxPowerSet2Limit19        = 0x0;
  PlatformNvsAreaProtocol->Area->EwrdWiFiSarTxPowerSet2Limit20        = 0x0;
  PlatformNvsAreaProtocol->Area->EwrdWiFiSarTxPowerSet2Limit21        = 0x0;
  PlatformNvsAreaProtocol->Area->EwrdWiFiSarTxPowerSet2Limit22        = 0x0;
  PlatformNvsAreaProtocol->Area->EwrdCdbWiFiSarTxPowerSet2Limit1      = 0x00;
  PlatformNvsAreaProtocol->Area->EwrdCdbWiFiSarTxPowerSet2Limit2      = 0x00;
  PlatformNvsAreaProtocol->Area->EwrdCdbWiFiSarTxPowerSet2Limit3      = 0x00;
  PlatformNvsAreaProtocol->Area->EwrdCdbWiFiSarTxPowerSet2Limit4      = 0x00;
  PlatformNvsAreaProtocol->Area->EwrdCdbWiFiSarTxPowerSet2Limit5      = 0x00;
  PlatformNvsAreaProtocol->Area->EwrdCdbWiFiSarTxPowerSet2Limit6      = 0x00;
  PlatformNvsAreaProtocol->Area->EwrdCdbWiFiSarTxPowerSet2Limit7      = 0x00;
  PlatformNvsAreaProtocol->Area->EwrdCdbWiFiSarTxPowerSet2Limit8      = 0x00;
  PlatformNvsAreaProtocol->Area->EwrdCdbWiFiSarTxPowerSet2Limit9      = 0x00;
  PlatformNvsAreaProtocol->Area->EwrdCdbWiFiSarTxPowerSet2Limit10     = 0x00;
  PlatformNvsAreaProtocol->Area->EwrdCdbWiFiSarTxPowerSet2Limit11     = 0x00;
  PlatformNvsAreaProtocol->Area->EwrdCdbWiFiSarTxPowerSet2Limit12     = 0x00;
  PlatformNvsAreaProtocol->Area->EwrdCdbWiFiSarTxPowerSet2Limit13     = 0x00;
  PlatformNvsAreaProtocol->Area->EwrdCdbWiFiSarTxPowerSet2Limit14     = 0x00;
  PlatformNvsAreaProtocol->Area->EwrdCdbWiFiSarTxPowerSet2Limit15     = 0x00;
  PlatformNvsAreaProtocol->Area->EwrdCdbWiFiSarTxPowerSet2Limit16     = 0x00;
  PlatformNvsAreaProtocol->Area->EwrdCdbWiFiSarTxPowerSet2Limit17     = 0x00;
  PlatformNvsAreaProtocol->Area->EwrdCdbWiFiSarTxPowerSet2Limit18     = 0x00;
  PlatformNvsAreaProtocol->Area->EwrdCdbWiFiSarTxPowerSet2Limit19     = 0x00;
  PlatformNvsAreaProtocol->Area->EwrdCdbWiFiSarTxPowerSet2Limit20     = 0x00;
  PlatformNvsAreaProtocol->Area->EwrdCdbWiFiSarTxPowerSet2Limit21     = 0x00;
  PlatformNvsAreaProtocol->Area->EwrdCdbWiFiSarTxPowerSet2Limit22     = 0x00;

  PlatformNvsAreaProtocol->Area->EwrdWiFiSarTxPowerSet3Limit1         = 0x0;
  PlatformNvsAreaProtocol->Area->EwrdWiFiSarTxPowerSet3Limit2         = 0x0;
  PlatformNvsAreaProtocol->Area->EwrdWiFiSarTxPowerSet3Limit3         = 0x0;
  PlatformNvsAreaProtocol->Area->EwrdWiFiSarTxPowerSet3Limit4         = 0x0;
  PlatformNvsAreaProtocol->Area->EwrdWiFiSarTxPowerSet3Limit5         = 0x0;
  PlatformNvsAreaProtocol->Area->EwrdWiFiSarTxPowerSet3Limit6         = 0x0;
  PlatformNvsAreaProtocol->Area->EwrdWiFiSarTxPowerSet3Limit7         = 0x0;
  PlatformNvsAreaProtocol->Area->EwrdWiFiSarTxPowerSet3Limit8         = 0x0;
  PlatformNvsAreaProtocol->Area->EwrdWiFiSarTxPowerSet3Limit9         = 0x0;
  PlatformNvsAreaProtocol->Area->EwrdWiFiSarTxPowerSet3Limit10        = 0x0;
  PlatformNvsAreaProtocol->Area->EwrdWiFiSarTxPowerSet3Limit11        = 0x0;
  PlatformNvsAreaProtocol->Area->EwrdWiFiSarTxPowerSet3Limit12        = 0x0;
  PlatformNvsAreaProtocol->Area->EwrdWiFiSarTxPowerSet3Limit13        = 0x0;
  PlatformNvsAreaProtocol->Area->EwrdWiFiSarTxPowerSet3Limit14        = 0x0;
  PlatformNvsAreaProtocol->Area->EwrdWiFiSarTxPowerSet3Limit15        = 0x0;
  PlatformNvsAreaProtocol->Area->EwrdWiFiSarTxPowerSet3Limit16        = 0x0;
  PlatformNvsAreaProtocol->Area->EwrdWiFiSarTxPowerSet3Limit17        = 0x0;
  PlatformNvsAreaProtocol->Area->EwrdWiFiSarTxPowerSet3Limit18        = 0x0;
  PlatformNvsAreaProtocol->Area->EwrdWiFiSarTxPowerSet3Limit19        = 0x0;
  PlatformNvsAreaProtocol->Area->EwrdWiFiSarTxPowerSet3Limit20        = 0x0;
  PlatformNvsAreaProtocol->Area->EwrdWiFiSarTxPowerSet3Limit21        = 0x0;
  PlatformNvsAreaProtocol->Area->EwrdWiFiSarTxPowerSet3Limit22        = 0x0;
  PlatformNvsAreaProtocol->Area->EwrdCdbWiFiSarTxPowerSet3Limit1      = 0x00;
  PlatformNvsAreaProtocol->Area->EwrdCdbWiFiSarTxPowerSet3Limit2      = 0x00;
  PlatformNvsAreaProtocol->Area->EwrdCdbWiFiSarTxPowerSet3Limit3      = 0x00;
  PlatformNvsAreaProtocol->Area->EwrdCdbWiFiSarTxPowerSet3Limit4      = 0x00;
  PlatformNvsAreaProtocol->Area->EwrdCdbWiFiSarTxPowerSet3Limit5      = 0x00;
  PlatformNvsAreaProtocol->Area->EwrdCdbWiFiSarTxPowerSet3Limit6      = 0x00;
  PlatformNvsAreaProtocol->Area->EwrdCdbWiFiSarTxPowerSet3Limit7      = 0x00;
  PlatformNvsAreaProtocol->Area->EwrdCdbWiFiSarTxPowerSet3Limit8      = 0x00;
  PlatformNvsAreaProtocol->Area->EwrdCdbWiFiSarTxPowerSet3Limit9      = 0x00;
  PlatformNvsAreaProtocol->Area->EwrdCdbWiFiSarTxPowerSet3Limit10     = 0x00;
  PlatformNvsAreaProtocol->Area->EwrdCdbWiFiSarTxPowerSet3Limit11     = 0x00;
  PlatformNvsAreaProtocol->Area->EwrdCdbWiFiSarTxPowerSet3Limit12     = 0x00;
  PlatformNvsAreaProtocol->Area->EwrdCdbWiFiSarTxPowerSet3Limit13     = 0x00;
  PlatformNvsAreaProtocol->Area->EwrdCdbWiFiSarTxPowerSet3Limit14     = 0x00;
  PlatformNvsAreaProtocol->Area->EwrdCdbWiFiSarTxPowerSet3Limit15     = 0x00;
  PlatformNvsAreaProtocol->Area->EwrdCdbWiFiSarTxPowerSet3Limit16     = 0x00;
  PlatformNvsAreaProtocol->Area->EwrdCdbWiFiSarTxPowerSet3Limit17     = 0x00;
  PlatformNvsAreaProtocol->Area->EwrdCdbWiFiSarTxPowerSet3Limit18     = 0x00;
  PlatformNvsAreaProtocol->Area->EwrdCdbWiFiSarTxPowerSet3Limit19     = 0x00;
  PlatformNvsAreaProtocol->Area->EwrdCdbWiFiSarTxPowerSet3Limit20     = 0x00;
  PlatformNvsAreaProtocol->Area->EwrdCdbWiFiSarTxPowerSet3Limit21     = 0x00;
  PlatformNvsAreaProtocol->Area->EwrdCdbWiFiSarTxPowerSet3Limit22     = 0x00;

  PlatformNvsAreaProtocol->Area->EwrdWiFiSarTxPowerSet4Limit1         = 0x0;
  PlatformNvsAreaProtocol->Area->EwrdWiFiSarTxPowerSet4Limit2         = 0x0;
  PlatformNvsAreaProtocol->Area->EwrdWiFiSarTxPowerSet4Limit3         = 0x0;
  PlatformNvsAreaProtocol->Area->EwrdWiFiSarTxPowerSet4Limit4         = 0x0;
  PlatformNvsAreaProtocol->Area->EwrdWiFiSarTxPowerSet4Limit5         = 0x0;
  PlatformNvsAreaProtocol->Area->EwrdWiFiSarTxPowerSet4Limit6         = 0x0;
  PlatformNvsAreaProtocol->Area->EwrdWiFiSarTxPowerSet4Limit7         = 0x0;
  PlatformNvsAreaProtocol->Area->EwrdWiFiSarTxPowerSet4Limit8         = 0x0;
  PlatformNvsAreaProtocol->Area->EwrdWiFiSarTxPowerSet4Limit9         = 0x0;
  PlatformNvsAreaProtocol->Area->EwrdWiFiSarTxPowerSet4Limit10        = 0x0;
  PlatformNvsAreaProtocol->Area->EwrdWiFiSarTxPowerSet4Limit11        = 0x0;
  PlatformNvsAreaProtocol->Area->EwrdWiFiSarTxPowerSet4Limit12        = 0x0;
  PlatformNvsAreaProtocol->Area->EwrdWiFiSarTxPowerSet4Limit13        = 0x0;
  PlatformNvsAreaProtocol->Area->EwrdWiFiSarTxPowerSet4Limit14        = 0x0;
  PlatformNvsAreaProtocol->Area->EwrdWiFiSarTxPowerSet4Limit15        = 0x0;
  PlatformNvsAreaProtocol->Area->EwrdWiFiSarTxPowerSet4Limit16        = 0x0;
  PlatformNvsAreaProtocol->Area->EwrdWiFiSarTxPowerSet4Limit17        = 0x0;
  PlatformNvsAreaProtocol->Area->EwrdWiFiSarTxPowerSet4Limit18        = 0x0;
  PlatformNvsAreaProtocol->Area->EwrdWiFiSarTxPowerSet4Limit19        = 0x0;
  PlatformNvsAreaProtocol->Area->EwrdWiFiSarTxPowerSet4Limit20        = 0x0;
  PlatformNvsAreaProtocol->Area->EwrdWiFiSarTxPowerSet4Limit21        = 0x0;
  PlatformNvsAreaProtocol->Area->EwrdWiFiSarTxPowerSet4Limit22        = 0x0;
  PlatformNvsAreaProtocol->Area->EwrdCdbWiFiSarTxPowerSet4Limit1      = 0x00;
  PlatformNvsAreaProtocol->Area->EwrdCdbWiFiSarTxPowerSet4Limit2      = 0x00;
  PlatformNvsAreaProtocol->Area->EwrdCdbWiFiSarTxPowerSet4Limit3      = 0x00;
  PlatformNvsAreaProtocol->Area->EwrdCdbWiFiSarTxPowerSet4Limit4      = 0x00;
  PlatformNvsAreaProtocol->Area->EwrdCdbWiFiSarTxPowerSet4Limit5      = 0x00;
  PlatformNvsAreaProtocol->Area->EwrdCdbWiFiSarTxPowerSet4Limit6      = 0x00;
  PlatformNvsAreaProtocol->Area->EwrdCdbWiFiSarTxPowerSet4Limit7      = 0x00;
  PlatformNvsAreaProtocol->Area->EwrdCdbWiFiSarTxPowerSet4Limit8      = 0x00;
  PlatformNvsAreaProtocol->Area->EwrdCdbWiFiSarTxPowerSet4Limit9      = 0x00;
  PlatformNvsAreaProtocol->Area->EwrdCdbWiFiSarTxPowerSet4Limit10     = 0x00;
  PlatformNvsAreaProtocol->Area->EwrdCdbWiFiSarTxPowerSet4Limit11     = 0x00;
  PlatformNvsAreaProtocol->Area->EwrdCdbWiFiSarTxPowerSet4Limit12     = 0x00;
  PlatformNvsAreaProtocol->Area->EwrdCdbWiFiSarTxPowerSet4Limit13     = 0x00;
  PlatformNvsAreaProtocol->Area->EwrdCdbWiFiSarTxPowerSet4Limit14     = 0x00;
  PlatformNvsAreaProtocol->Area->EwrdCdbWiFiSarTxPowerSet4Limit15     = 0x00;
  PlatformNvsAreaProtocol->Area->EwrdCdbWiFiSarTxPowerSet4Limit16     = 0x00;
  PlatformNvsAreaProtocol->Area->EwrdCdbWiFiSarTxPowerSet4Limit17     = 0x00;
  PlatformNvsAreaProtocol->Area->EwrdCdbWiFiSarTxPowerSet4Limit18     = 0x00;
  PlatformNvsAreaProtocol->Area->EwrdCdbWiFiSarTxPowerSet4Limit19     = 0x00;
  PlatformNvsAreaProtocol->Area->EwrdCdbWiFiSarTxPowerSet4Limit20     = 0x00;
  PlatformNvsAreaProtocol->Area->EwrdCdbWiFiSarTxPowerSet4Limit21     = 0x00;
  PlatformNvsAreaProtocol->Area->EwrdCdbWiFiSarTxPowerSet4Limit22     = 0x00;

  PlatformNvsAreaProtocol->Area->WiFiDynamicSarAntennaACurrentSet = 0x0;
  PlatformNvsAreaProtocol->Area->WiFiDynamicSarAntennaBCurrentSet = 0x0;

  PlatformNvsAreaProtocol->Area->WgdsWiFiSarDeltaGroup1PowerMax1      = 0xFF;
  PlatformNvsAreaProtocol->Area->WgdsWiFiSarDeltaGroup1PowerChainA1   = 0x0;
  PlatformNvsAreaProtocol->Area->WgdsWiFiSarDeltaGroup1PowerChainB1   = 0x0;
  PlatformNvsAreaProtocol->Area->WgdsWiFiSarDeltaGroup1PowerMax2      = 0xFF;
  PlatformNvsAreaProtocol->Area->WgdsWiFiSarDeltaGroup1PowerChainA2   = 0x0;
  PlatformNvsAreaProtocol->Area->WgdsWiFiSarDeltaGroup1PowerChainB2   = 0x0;
  PlatformNvsAreaProtocol->Area->WgdsWiFiSarDeltaGroup1PowerMax3      = 0XFF;
  PlatformNvsAreaProtocol->Area->WgdsWiFiSarDeltaGroup1PowerChainA3   = 0x00;
  PlatformNvsAreaProtocol->Area->WgdsWiFiSarDeltaGroup1PowerChainB3   = 0x00;
  PlatformNvsAreaProtocol->Area->WgdsWiFiSarDeltaGroup2PowerMax1      = 0xFF;
  PlatformNvsAreaProtocol->Area->WgdsWiFiSarDeltaGroup2PowerChainA1   = 0x0;
  PlatformNvsAreaProtocol->Area->WgdsWiFiSarDeltaGroup2PowerChainB1   = 0x0;
  PlatformNvsAreaProtocol->Area->WgdsWiFiSarDeltaGroup2PowerMax2      = 0xFF;
  PlatformNvsAreaProtocol->Area->WgdsWiFiSarDeltaGroup2PowerChainA2   = 0x0;
  PlatformNvsAreaProtocol->Area->WgdsWiFiSarDeltaGroup2PowerChainB2   = 0x0;
  PlatformNvsAreaProtocol->Area->WgdsWiFiSarDeltaGroup2PowerMax3      = 0XFF;
  PlatformNvsAreaProtocol->Area->WgdsWiFiSarDeltaGroup2PowerChainA3   = 0x00;
  PlatformNvsAreaProtocol->Area->WgdsWiFiSarDeltaGroup2PowerChainB3   = 0x00;
  PlatformNvsAreaProtocol->Area->WgdsWiFiSarDeltaGroup3PowerMax1      = 0xFF;
  PlatformNvsAreaProtocol->Area->WgdsWiFiSarDeltaGroup3PowerChainA1   = 0x0;
  PlatformNvsAreaProtocol->Area->WgdsWiFiSarDeltaGroup3PowerChainB1   = 0x0;
  PlatformNvsAreaProtocol->Area->WgdsWiFiSarDeltaGroup3PowerMax2      = 0xFF;
  PlatformNvsAreaProtocol->Area->WgdsWiFiSarDeltaGroup3PowerChainA2   = 0x0;
  PlatformNvsAreaProtocol->Area->WgdsWiFiSarDeltaGroup3PowerChainB2   = 0x0;
  PlatformNvsAreaProtocol->Area->WgdsWiFiSarDeltaGroup3PowerMax3      = 0XFF;
  PlatformNvsAreaProtocol->Area->WgdsWiFiSarDeltaGroup3PowerChainA3   = 0x00;
  PlatformNvsAreaProtocol->Area->WgdsWiFiSarDeltaGroup3PowerChainB3   = 0x00;


  PlatformNvsAreaProtocol->Area->WifiAntGainEnale                     = 0x0;
  PlatformNvsAreaProtocol->Area->WifiAntGainChainA1                   = 0x18;
  PlatformNvsAreaProtocol->Area->WifiAntGainChainA2                   = 0x28;
  PlatformNvsAreaProtocol->Area->WifiAntGainChainA3                   = 0x28;
  PlatformNvsAreaProtocol->Area->WifiAntGainChainA4                   = 0x28;
  PlatformNvsAreaProtocol->Area->WifiAntGainChainA5                   = 0x28;
  PlatformNvsAreaProtocol->Area->WifiAntGainChainA6                   = 0x28;
  PlatformNvsAreaProtocol->Area->WifiAntGainChainA7                   = 0x28;
  PlatformNvsAreaProtocol->Area->WifiAntGainChainA8                   = 0x28;
  PlatformNvsAreaProtocol->Area->WifiAntGainChainA9                   = 0x28;
  PlatformNvsAreaProtocol->Area->WifiAntGainChainA10                  = 0x28;
  PlatformNvsAreaProtocol->Area->WifiAntGainChainA11                  = 0x28;
  PlatformNvsAreaProtocol->Area->WifiAntGainChainB1                   = 0x18;
  PlatformNvsAreaProtocol->Area->WifiAntGainChainB2                   = 0x28;
  PlatformNvsAreaProtocol->Area->WifiAntGainChainB3                   = 0x28;
  PlatformNvsAreaProtocol->Area->WifiAntGainChainB4                   = 0x28;
  PlatformNvsAreaProtocol->Area->WifiAntGainChainB5                   = 0x28;
  PlatformNvsAreaProtocol->Area->WifiAntGainChainB6                   = 0x28;
  PlatformNvsAreaProtocol->Area->WifiAntGainChainB7                   = 0x28;
  PlatformNvsAreaProtocol->Area->WifiAntGainChainB8                   = 0x28;
  PlatformNvsAreaProtocol->Area->WifiAntGainChainB9                   = 0x28;
  PlatformNvsAreaProtocol->Area->WifiAntGainChainB10                  = 0x28;
  PlatformNvsAreaProtocol->Area->WifiAntGainChainB11                  = 0x28;
  PlatformNvsAreaProtocol->Area->AxSettingUkraine                     = 0x00;
  PlatformNvsAreaProtocol->Area->AxModeUkraine                        = 0x00;
  PlatformNvsAreaProtocol->Area->AxSettingRussia                      = 0x00;
  PlatformNvsAreaProtocol->Area->AxModeRussia                         = 0x00;

  PlatformNvsAreaProtocol->Area->WifiActiveChannelSrd                 = 0x00;
  PlatformNvsAreaProtocol->Area->WifiIndonesia5GhzSupport             = 0x00;
  PlatformNvsAreaProtocol->Area->WifiUltraHighBandSupport             = 0x00;
  PlatformNvsAreaProtocol->Area->WifiRegulatoryConfigurations         = 0x00;
  PlatformNvsAreaProtocol->Area->WifiUartConfigurations               = 0x00;
  PlatformNvsAreaProtocol->Area->WifiUnii4                            = 0x00;
  PlatformNvsAreaProtocol->Area->WifiIndoorControl                    = 0x00;


  PlatformNvsAreaProtocol->Area->WifiTASSelection             = 0x00;
  PlatformNvsAreaProtocol->Area->WifiTASListEntries           = 0x00;
  PlatformNvsAreaProtocol->Area->WTASBlockedListEntry1        = 0x00;
  PlatformNvsAreaProtocol->Area->WTASBlockedListEntry2        = 0x00;
  PlatformNvsAreaProtocol->Area->WTASBlockedListEntry3        = 0x00;
  PlatformNvsAreaProtocol->Area->WTASBlockedListEntry4        = 0x00;
  PlatformNvsAreaProtocol->Area->WTASBlockedListEntry5        = 0x00;
  PlatformNvsAreaProtocol->Area->WTASBlockedListEntry6        = 0x00;
  PlatformNvsAreaProtocol->Area->WTASBlockedListEntry7        = 0x00;
  PlatformNvsAreaProtocol->Area->WTASBlockedListEntry8        = 0x00;
  PlatformNvsAreaProtocol->Area->WTASBlockedListEntry9        = 0x00;
  PlatformNvsAreaProtocol->Area->WTASBlockedListEntry10       = 0x00;
  PlatformNvsAreaProtocol->Area->WTASBlockedListEntry11       = 0x00;
  PlatformNvsAreaProtocol->Area->WTASBlockedListEntry12       = 0x00;
  PlatformNvsAreaProtocol->Area->WTASBlockedListEntry13       = 0x00;
  PlatformNvsAreaProtocol->Area->WTASBlockedListEntry14       = 0x00;
  PlatformNvsAreaProtocol->Area->WTASBlockedListEntry15       = 0x00;
  PlatformNvsAreaProtocol->Area->WTASBlockedListEntry16       = 0x00;


  PlatformNvsAreaProtocol->Area->BluetoothSar                 = 0x0;
  PlatformNvsAreaProtocol->Area->BluetoothSarBr               = 0x0;
  PlatformNvsAreaProtocol->Area->BluetoothSarEdr2             = 0x0;
  PlatformNvsAreaProtocol->Area->BluetoothSarEdr3             = 0x0;
  PlatformNvsAreaProtocol->Area->BluetoothSarLe               = 0x0;
  PlatformNvsAreaProtocol->Area->BluetoothSarLe2Mhz           = 0x0;
  PlatformNvsAreaProtocol->Area->BluetoothSarLeLr             = 0x0;
  PlatformNvsAreaProtocol->Area->CoExistenceManager           = 0x0;
  PlatformNvsAreaProtocol->Area->CnvExtClock                  = 0x0;

  PlatformNvsAreaProtocol->Area->BtTileMode                   = 0x0;
  PlatformNvsAreaProtocol->Area->TileS0                       = 0x0;
  PlatformNvsAreaProtocol->Area->TileS0ix                     = 0x1;
  PlatformNvsAreaProtocol->Area->TileS4                       = 0x0;
  PlatformNvsAreaProtocol->Area->TileS5                       = 0x0;

#if (FixedPcdGetBool(PcdAdlLpSupport) == 1)
  PlatformNvsAreaProtocol->Area->BtLedConfig                  = 0x0;
  PlatformNvsAreaProtocol->Area->BtLedPulseDuration           = 0x1;
  PlatformNvsAreaProtocol->Area->BtLedPulseInterval           = 0xA;
#else
  PlatformNvsAreaProtocol->Area->SpecialLedConfig             = 0;
  PlatformNvsAreaProtocol->Area->LedDuration                  = 100;
  PlatformNvsAreaProtocol->Area->AirplaneMode                 = 0;
#endif

  PlatformNvsAreaProtocol->Area->WwanFwFlashDevice            = 0x0;
  PlatformNvsAreaProtocol->Area->WccdEnable                   = 0x1;

  //
  // Miscellaneous
  //
  PlatformNvsAreaProtocol->Area->ConfigTdpBios = 0;
  PlatformNvsAreaProtocol->Area->PL1LimitCS = 0;
  PlatformNvsAreaProtocol->Area->PL1LimitCSValue = 4500;
  return EFI_SUCCESS;
}

