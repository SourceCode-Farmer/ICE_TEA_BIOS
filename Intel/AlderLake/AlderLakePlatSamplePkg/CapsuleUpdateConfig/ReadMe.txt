This file describe how to build system firmware Capsules with %PLATFORM_FULL_PACKAGE%\capsulebuild.bat

There are several Capsule targets supported:
1. Monolithic Capsule      - A single Capsule to update multiple components on IFWI
   ESRT entry GUID: 7FEB1D5D-33F4-48D3-BD11-C4B36B6D0E57
   Involved payloads: ClientBios.rom (must be a valid BIOS ROM)
                      Me.bin         (could be dummy file started with $DUM. if it's dummy, ME FW update would be skipped)
                      Ec.bin         (could be dummy file started with $DUM. if it's dummy, EC FW update would be skipped)
                      Pdt.bin        (could be dummy file started with $DUM. if it's dummy, PDT update would be skipped)

2. Standalone BIOS Capsule    - A Capsule to update BIOS only.
   ESRT entry GUID: 6C8E136F-D3E6-4131-AC32-4687CB4ABD27
   Involved payloads: ClientBios.rom (must be a valid BIOS ROM)

3. Standalone EC Capsule      - A Capsule to update EC FW only.
   ESRT entry GUID: 3DD84775-EC79-4ECB-8404-74DE030C3F77
   Involved payloads: Ec.bin (must be a valid EC binary)

4. Standalone ME Capsule      - A Capsule to update ME FW only.
   ESRT entry GUID: 0EAB05C1-766A-4805-A039-3081DE0210C7
   Involved payloads: Me.bin (must be a valid ME binary)

5. Standalone ISH PDT Capsule - A Capsule to update ISH PDT only.
   ESRT entry GUID: 76CA0AD8-4A14-4389-B7E5-FD88791762AD
   Involved payloads: Pdt.bin (must be a valid PDT binary)
   P.S. Current PDT Version is awlays 5 which causes some difficulties of validation:
          1. User is hard to tell the difference between different PDT binaries.
          2. WOS would skip PDT Capsule upgrade with the same FW version.
        So we use a virtual version stored in UEFI variable to represent PDT version on current platform.
        It means user needs to assign a virtual PDT version when creating PDT FMP Capsule image.

6. Standalone RETIMER Capsule - A Capsule to update RETIMER only.
   ESRT entry GUID: 2FE2CBFC-B9AA-4A93-AB5B-40173B581C42

   a. Involved payloads: ITbtRetimerPayload.bin (ITbtRetimerPayload.bin is generated according to ITbtRetimerPayloadConfig.ini)
      To build Integrated Thunderbolt RETIMER Capsule, ITbtRetimerPayloadConfig.ini must be a valid config file

   b. Involved payloads: DTbtRetimerPayload.bin (DTbtRetimerPayload.bin is generated according to DTbtRetimerPayloadConfig.ini)
      To build Discrete Thunderbolt RETIMER Capsule, DTbtRetimerPayloadConfig.ini must be a valid config file

7. Standalone Discrete TBT Capsule - A Capsule to update TBT only.
   ESRT entry GUID: 86A885EE-D71E-2ED6-0FC1-9D6CCC9677EB
   Involved payloads: DTbtPayload.bin (DTbtPayload.bin is generated according to DTbtPayloadConfig.ini)
   To build Discrete Thunderbolt Capsule, DTbtPayloadConfig.ini must be a valid config file

8. Standalone Ucode Capsule - A Capsule to update Ucode only.
   ESRT enry GUID: 69585D92-B50A-4AD7-0xB2-652EB1AE066574
   Involved payloads: uCode FFS file (Modify Features\CapsuleUpdate\Tools\NewGenCap\MicrocodeFv\MicrocodeFv.fdf to assign the
   correct ucode FFS file). Different CPU type corresponds to different ucode FFS file. To build Ucode Capsule, uCode FFS file must be valid.

9. Standalone BtGACM Capsule - A Capsule to update BtGACM only.
   ESRT entry GUID: 4E88068B-41B2-4E05-893C-DB0B43F7D348
   Involved payloads: BtGAcm.bin (musht be a valid ACM binary, such as Intel\AlderLakeBoardPkg\Binaries\BootGuard\ACM\StartupAcm.bin)

By default capsulebuild.bat generates a Monolithic Capsule.
See [How to Use] below for further details.

[Pre-Requisites]
1. Windows operating system
2. Having Python3 installed
3. If you want to test Capsule update in Windows, having WDK/SDK installed to generate Windows update driver.
   The script to generate Windows update driver requires below files:
   a. signtool.exe under
      C:\Program Files (x86)\Windows Kits\10\bin\x64
      or
      C:\Program Files (x86)\Windows Kits\8.1\bin\x64
      or
      C:\Program Files (x86)\Windows Kits\10\bin\*\x64

   b. Inf2Cat.exe under
      C:\Program Files (x86)\Windows Kits\10\bin\x86
      or
      C:\Program Files (x86)\Windows Kits\10\bin\*\x86

[How to Use]
1. Replace relative payload images (e.g., ClientBios.rom, Ec.bin, and etc.) of your interests with the real ones in this directory.
2. Under %PLATFORM_FULL_PACKAGE%, open Command prompt, run:
     capsulebuild [target:all/bios/ec/me/pdt/itbtretimer/dtbtretimer/dtbt/ucode/btgacm][-biossvn <Bios Svn>][-mever <ME Build Version>]
     [-mesku <Me SKU>][-pdtver <PDT Version>][-mesku <Me SKU>][-pdtver <PDT Version>][-rtver <Retimer Version>][-dtbtver <TBT Version>]
     [-ucodem <ucode mode>][-ucodet <ucode build type>][-ucodeverstr <string version of ucode payload>]
     [-ucodever <32-bit version of ucode payload>][-ucodelsv <lowest supported version of the ucode payload>][help]

        target   Assign Capsule build targets: all/bios/me/ec/pdt/itbtretimer/dtbtretimer/dtbt/ucode/btgacm, default is building Monolithic Capsule

                 default: If target is NOT assigned, build Monolithic Capsule

                 all          : Build Monolithic Capsule + standalone bios, me, ec and pdt Capsules
                 bios         : Build Standalone BIOS Capsule
                 ec           : Build Standalone EC Capsule
                 me           : Build Standalone ME Capsule
                 pdt          : Build Standalone PDT Capsule
                 itbtretimer  : Build Integrated TBT Retimer Device FW Capsule
                 dtbtretimer  : Build Discrete TBT Retimer Device FW Capsule
                 dtbt         : Build Discrete TBT Device FW Capsule
                 ucode        : Build Standalone Ucode Capsule
                 btgacm       : Build Standalone ACM Capsule

        -biossvn To indicate the Bios SVN
                 It's optional when building Bios capsule, the default value is the major version of ClientBios.rom.
                 It's mandatory when building ACM and bgup and full mode ucode capsule.
                 It's unnecessary when building slot mode ucode capsule.
                 e.g., "capsulebuild.bat bios -biossvn 2849"
        -mever   To indicate ME Build version of CapsuleUpdateConfig\Me.bin.
                 It's mandatory if building capsules with [all] or [me] target
        -mesku   To indicate ME SKU of CapsuleUpdateConfig\Me.bin.
                 It's mandatory if building capsules with [all] or [me] target
                 It should be one of the following string: Lp_Cons/H_Cons/Lp_Corp/H_Corp/default
        -pdtver  To assign a virtual PDT version of CapsuleUpdateConfig\Pdt.bin.
                 It's mandatory if building capsules with [all] or [pdt] target
                 e.g., "capsulebuild.bat pdt -pdtver 2"
        -rtver   To assign a virtual Retimer version for Retimer Capsule
                 It's mandatory if building capsules with [itbtretimer] or [dtbtretimer] target
                 e.g., "capsulebuild.bat itbtretimer -rtver 110", "capsulebuild.bat dtbtretimer -rtver 119"
        -dtbtver To assign a virtual TBT version for Discrete TBT Capsule
                 It's mandatory if building capsules with [dtbt] target
                 e.g., "capsulebuild.bat dtbt -dtbtver 290"
        -ucodem  To indicate the ucode build mode
                 It's mandatory when building capsule with [ucode] target
                 Should be one of the following string: ucodefull/ucodebgup/ucodeslot
        -ucodet  To indicate the ucode build type
                 It's optional when building capsule with [ucode] target
                 Should be one of the following string: debug/release. The default type is debug.
        -ucodeverstr  The version string of the ucode binary payload (e.g. "Version 0.1.2.3").
        -ucodever     The 32-bit version of the ucode binary payload (e.g. 0x11223344 or 5678).
                      The argument -ucodever and -ucodeverstr doesn't overloap, user can assign both.
        -ucodelsv     The 32-bit lowest supported version of the ucode binary payload (e.g. 0x11223344 or 5678).
        Note*: As of now we dont have the knowledge to parse ME version from a given ME FW image.
               So ME version needs to be provided manually by the user.

3. If the build is success, XXXSystemFwxxxx.cap would be observed in CapsuleOutput directory.
4. Windows Update driver (if generated) would be found in CapsuleOutput\WindowsCapsule

Examples:
   *XXX  stands for the shor tname of the platform like ICL etc.
   *vvvv stands for the version of the firmware

   a. capsulebuild.bat
        => Default build. Build a Monolithic capsule. XXXSystemFwMono_vvvv.cap would be observed in CapsuleOutput directory.
           Windows update driver (if generated) would be found in CapsuleOutput\WindowsCapsule\Monolithic

   b. capsulebuild.bat ec
        => Build a standalone ec capsule. XXXSystemFwEc_vvvv.cap would be observed in CapsuleOutput directory.
           Windows update driver (if generated) would be found in CapsuleOutput\WindowsCapsule\Ec

   c. capsulebuild.bat bios me -mever 14.0.20.1033 -mesku H_Corp
        => Build both standalone ME capsule and BIOS capsule. 14.0.20.1033 indicates ME build verion of Me.bin
           XXXMe_14_0_20_1033_H_Corp.cap and XXXSystemFwBios_vvvv.cap would be observed in CapsuleOutput directory.
           Windows update driver (if generated) would be found in CapsuleOutput\WindowsCapsule\Me and CapsuleOutput\WindowsCapsule\Bios

   d. capsulebuild.bat all -mever 14.0.20.1033 -mesku H_Corp -pdtver 5
        => Build Monolithic Capsule and standalone BIOS/ME/EC Capsules. 1033 indicates ME build verion of Me.bin
           XXXSystemFwMono_vvv.cap, XXXSystemFwBios_vvv.cap, XXXMe_14_0_20_1033_H_Corp.cap, XXXSystemFwEc_vvvv.cap and
           XXXSystemFwIshPdt_5.cap would be observed in CapsuleOutput directory.
           Windows update drivers (if generated) would be found in CapsuleOutput\WindowsCapsule

   e. capsulebuild.bat itbtretimer -rtver 110
        => Build Retimer CapsuleCapsule. 110 indicates Retimer build version of Retimer payload
           Adl_ITbtRetimer_110.cap would be observed in CapsuleOutput directory.
           Windows update drivers (if generated) would be found in CapsuleOutput\WindowsCapsule

   f. capsulebuild.bat dtbtretimer -rtver 119
        => Build Retimer CapsuleCapsule. 119 indicates Retimer build version of Retimer payload
           Adl_DTbtRetimer_119.cap would be observed in CapsuleOutput directory.
           Windows update drivers (if generated) would be found in CapsuleOutput\WindowsCapsule

   g. capsulebuild.bat dtbt -dtbtver 290
        => Build Retimer CapsuleCapsule. 290 indicates TBT build version of Discreted TBT payload
           Adl_DTbt_290.cap would be observed in CapsuleOutput directory.
           Windows update drivers (if generated) would be found in CapsuleOutput\WindowsCapsule

   i. capsulebuild.bat ucode -ucodem ucodefull -biossvn 2999 -ucodet debug -ucodeverstr 0.1.2.3 -ucodever 5678 -ucodelsv 5678
        => Build ucode Capsule. Ucode mode is ucodefull, ucode build type is debug and biossvn is 2999
           uCodeFull.capp would be observed in directory AlderLakePlatSamplePkg\Features\CapsuleUpdate\Tools\NewGenCap\Output
           Windows update drivers would be found in AlderLakePlatSamplePkg\Features\CapsuleUpdate\Tools\NewGenCap\Outputt\WindowsCapsule

   j. capsulebuild.bat btgacm -biossvn 2999
        => Build BtGACM Capsule. The bios svn is 2999.
           %CAP_PLATFORM_PREFIX%SystemFwBtGAcm_NNMMnn.Cap would be observed in CapsuleOutput directory. NN is the version number,
           MM is the major number, and mm is minor version. These version info is got from BtGAcm.bin

[Apply Capsule in UEFI Shell]

1. Put Capsule file and CapsuleApp.efi in USB storage.
2. In EFI Shell, switch file system to the USB storage (e.g. Fs0:)
3. fs0:> CapsuleApp <Capsule file name (e.g. XXXSystemFwxxxx.Cap)>
4. System should restart
5. After reboot, a progress bar which indicates the update is in progress should be observed with Intel logo.

[Apply Capsule update package in Windows]

Pre-Requisites:
        1. Disable Secure Boot in BIOS setup question to allow firmware upgrade.
        2. Please check the system date and time if it is correct or not.
        3. Set "testsigning" on through bcdedit using below admin command prompt:
                "bcdedit /set testsigning on" and reboot the system.
        4. Confirm the changes by typing "bcdedit" in cmd prompt. Below status should be observed:
                  testsigning                       Yes

Installing the Firmware driver package:
        1. Copy XXXSystemFwxxxx directory to SUT
        2. Do the below things to install the certificate file.
                  a. Double click on the security catalog file (cat file) from package.
                  b. Click on View Signature.
                  c. Click on View Certificate followed by clicking on Install Certificate.
                  d. For Store Location, choose "Local Machine". Click next and Manually browse and select "Trusted Root Certification Authorities".
                  e. Click OK and next to install the certificate on the system for one time.
                  f. You should get the popup message "The Import was Successful"
        3. Enter to driver package created. Run "pnputil -i -a XXXSystemFwxxxx.inf" as Administrator. Then restart system.