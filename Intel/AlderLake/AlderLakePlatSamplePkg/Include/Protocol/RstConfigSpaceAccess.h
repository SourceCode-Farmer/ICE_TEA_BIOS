/**@file
  RST config space access protocol for bios.

@copyright
  INTEL CONFIDENTIAL
  Copyright 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains a 'Sample Driver' and is licensed as such under the terms
  of your license agreement with Intel or your vendor. This file may be modified
  by the user, subject to the additional terms of the license agreement.

@par Specification Reference:
**/

#ifndef _RST_CONFIG_SPACE_ACCESS_H
#define _RST_CONFIG_SPACE_ACCESS_H

/// Version of EFI_RST_CONFIG_SPACE_ACCESS_PROTOCOL
#define EFI_RST_CONFIG_SPACE_ACCESS_PROTOCOL_VERSION    0x0001

///
/// Global ID for the EFI_RST_CONFIG_SPACE_ACCESS_PROTOCOL
///
#define EFI_RST_CONFIG_SPACE_ACCESS_PROTOCOL_GUID                                \
  {                                                                              \
    0x32370e89, 0x5f4a, 0x4ef0, 0xa5, 0x58, 0xce, 0x76, 0xfe, 0xff, 0x2b, 0xbe } \
  }

typedef struct _EFI_RST_CONFIG_SPACE_ACCESS_PROTOCOL  EFI_RST_CONFIG_SPACE_ACCESS_PROTOCOL;

/**
  Retrieves the address of PCIe Config Space of device (specified by NamespaceId value) managed by VMD controller.

  @param  This                  A pointer to the EFI_RST_CONFIG_SPACE_ACCESS_PROTOCOL instance.
  @param  NamespaceId           A NamespaceId value returned by GetNextNamespaced function of NVMe Pass Thru protocol.
  @param  ConfigSpaceAddress    An address of PCIe Config Space of device managed by VMD controller.


  @retval EFI_SUCCESS           The PCIe Config Space address of the device has been retrived with success.
  @retval EFI_NOT_FOUND         The device specified by NamespaceId couldn't be found.
  @retval EFI_INVALID_PARAMETER ConfigSpaceAdddress buffer is NULL or invalid NamespaceId requested.

**/
typedef
EFI_STATUS
(EFIAPI *EFI_RST_CONFIG_SPACE_ACCESS_GET_ADDRESS) (
  IN  EFI_RST_CONFIG_SPACE_ACCESS_PROTOCOL      *This,
  IN  UINT32                                    NamespaceId,
  OUT EFI_PHYSICAL_ADDRESS                      *ConfigSpaceAddress
);

/**
  Retrieves the PCI bus/device/function number of the root port before device managed by VMD controller.

  @param  This                  A pointer to the EFI_RST_CONFIG_SPACE_ACCESS_PROTOCOL instance.
  @param  NamespaceId           A NamespaceId value returned by GetNextNamespaced function of NVMe Pass Thru protocol.
  @param  BusNumber             PCI bus number of root port
  @param  DeviceNumber          PCI device number of root port
  @param  FunctionNumber        PCI function number of root port

  @retval EFI_SUCCESS           The BusNumber, DeviceNumber and FunctionNumber of the device has been retrived with success.
  @retval EFI_NOT_FOUND         The device specified by NamespaceId couldn't be found.
  @retval EFI_INVALID_PARAMETER BusNumber, DeviceNumber or FunctionNumber is NULL or invalid NamespaceId requested.

**/
typedef
EFI_STATUS
(EFIAPI *EFI_RST_CONFIG_SPACE_ACCESS_GET_BDF)(
  IN     EFI_RST_CONFIG_SPACE_ACCESS_PROTOCOL   *This,
  IN     UINT32                                 NamespaceId,
  OUT    UINTN                                  *BusNumber,
  OUT    UINTN                                  *DeviceNumber,
  OUT    UINTN                                  *FunctionNumber
);


struct _EFI_RST_CONFIG_SPACE_ACCESS_PROTOCOL {
  UINT8                                         Version;
  EFI_RST_CONFIG_SPACE_ACCESS_GET_ADDRESS       GetAddress;
  EFI_RST_CONFIG_SPACE_ACCESS_GET_BDF           GetBDF;
};

extern EFI_GUID gRSTConfigSpaceAccessGuid;

#endif // _RST_CONFIG_SPACE_ACCESS_H
