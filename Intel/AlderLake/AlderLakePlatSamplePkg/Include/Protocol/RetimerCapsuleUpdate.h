/** @file
 
;******************************************************************************
;* Copyright 2021 Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corp.
;*
;******************************************************************************
*/
/** @file
  RetimerCapsuleUpdate driver definitions.

@copyright
  INTEL CONFIDENTIAL
  Copyright 2020 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains a 'Sample Driver' and is licensed as such under the terms
  of your license agreement with Intel or your vendor. This file may be modified
  by the user, subject to the additional terms of the license agreement.

@par Specification Reference:
**/

#ifndef _RETIMER_CAPSULE_UPDATE_H_
#define _RETIMER_CAPSULE_UPDATE_H_

#include <Uefi.h>
#include <Library/BaseLib.h>
#include <Library/PcdLib.h>
#include <PiDxe.h>
#include <Library/DebugLib.h>
#include <Library/BaseMemoryLib.h>
#include <Library/UefiBootServicesTableLib.h>
#include <Library/EcTcssLib.h>
//[-start-210408-IB17800122-remove]//
//#include <Guid/SystemResourceTable.h>
//#include <Protocol/EsrtManagement.h>
//[-end-210408-IB17800122-remove]//
#include <Library/GpioLib.h>
#include <Library/GpioConfig.h>


// Macro Defination
#define    RETIMER_TO_PD_MAPPING_PCD_TOTAL_WIDTH    32
#define    RETIMER_TO_PD_MAPPING_PER_DEVICE_WIDTH   4
//
// Make Sure MAX_TBT_RETIMER_DEVICE must be equal to
// RETIMER_TO_PD_MAPPING_PCD_TOTAL_WIDTH / RETIMER_TO_PD_MAPPING_PER_DEVICE_WIDTH value.
// In this case it is 32/4 = 8.
//
#define    MAX_TBT_RETIMER_DEVICE                   8
#define    INVALID_DEVICE_MODE                      0xFF
#define    INVALID_PD_CONTROLLER_INDEX              0xFF
#define    ALL_PD_CONTROLLER                        0xFE
#define    RETIMER_DEVICE_AVAILABLE_BIT             BIT3
#define    PD_CONTROLLER_INDEX_BIT_MASK             (BIT0 | BIT1 | BIT2)

#define    NO_VALID_GUID_FOUND                      0x00
#define    TBT_RETIMER_VALID_GUID_FOUND             BIT0
#define    NON_TBT_I2C_RETIMER_VALID_GUID_FOUND     BIT1

#define    INVALID_I2C_RETIMER_INDEX                0xFF
#define    ALL_I2C_RETIMER_SLAVE_DEVICE             0xFE
#define    I2C_RETIMER_BUS_CONFIG_0_400_KHZ         400
#define    I2C_RETIMER_HW_CONFIG_VERSION_1          1

#define    INVALID_RETIMER_INDEX                    0xFF
#define    ALL_RETIMER_DEVICE                       0xFE

///
/// Forward declaration for the _TCSS_RETIMER_PROTOCOL.
///
typedef struct _TCSS_RETIMER_PROTOCOL              TCSS_RETIMER_PROTOCOL;
typedef struct _TCSS_RETIMER_PROTOCOL_CAPABILITY   TCSS_RETIMER_PROTOCOL_CAPABILITY;

///
/// Global ID for the ESRT_MANAGEMENT_PROTOCOL.
///
// {B9AC2BD1-D450-4BEC-BD5B-192601119B84}
#define TCSS_RETIMER_PROTOCOL_GUID \
{ \
    0xB9AC2BD1, 0xD450, 0x4BEC, { 0xBD, 0x5B, 0x19, 0x26, 0x1, 0x11, 0x9B, 0x84 } \
}
//EFI_GUID gTcssRetimerProtocolGuid  = { 0xB9AC2BD1, 0xD450, 0x4BEC, { 0xBD, 0x5B, 0x19, 0x26, 0x1, 0x11, 0x9B, 0x84 } };

extern EFI_GUID gTcssRetimerProtocolGuid;

// Structure Declaration

/**
  Get TCSS Retimer Controller info and Capability.

  @param[in]       This                 The TCSS RETIMER PROTOCOL Instance.
  @param[in]       RetimerGuid          GUID from ESRT ACPI Table.

  @retval EFI_SUCCESS                   Successfully Mode is Changed to FW Update Mode.
  @retval EFI_INVALID_PARAMETER         Invalid GUID from ESRT Table is Passed.
  @retval EFI_UNSUPPORTED               This driver does not support.
  @retval EFI_DEVICE_ERROR              This driver cannot be started due to device Error.
  @retval EFI_ALREADY_STARTED           This driver has been started.
  @retval EFI_TIMEOUT                   Tcss Command timeout Happen.

**/
typedef
EFI_STATUS
(EFIAPI *TCSS_RETIMER_INFO)(
  IN  TCSS_RETIMER_PROTOCOL                       *This,
  IN  EFI_GUID                                    RetimerGuid, ///< GUID from ESRT ACPI Table
  OUT TCSS_RETIMER_PROTOCOL_CAPABILITY            *RetimerInfo
  );

/**
  Drive TCSS Retimer Controller into FW Update Mode.

  @param[in]       This                 The TCSS RETIMER PROTOCOL Instance.
  @param[in]       RetimerGuid          GUID from ESRT ACPI Table.

  @param[out]      *RetimerInfo         Out buffer containing Retimer info of guid passed.

  @retval EFI_SUCCESS                   Successfully Mode is Changed to FW Update Mode.
  @retval EFI_INVALID_PARAMETER         Invalid GUID from ESRT Table is Passed.
  @retval EFI_UNSUPPORTED               This driver does not support.
  @retval EFI_DEVICE_ERROR              This driver cannot be started due to device Error.
  @retval EFI_ALREADY_STARTED           This driver has been started.
  @retval EFI_TIMEOUT                   DriveToFwUpdateMode Command timeout Happen.

**/
typedef
EFI_STATUS
(EFIAPI *FW_UPDATE_MODE_ENTRY)(
  IN TCSS_RETIMER_PROTOCOL      *This,
  IN EFI_GUID                   RetimerGuid ///< GUID from ESRT ACPI Table
  );

/**
  Restore TBT PD Controller into original mode.

  @param[in]       This                 The TCSS RETIMER PROTOCOL Instance.
  @param[in]       RetimerGuid          GUID from ESRT ACPI Table.

  @retval EFI_SUCCESS                   Successfully Mode is Restore.
  @retval EFI_INVALID_PARAMETER         Invalid GUID from ESRT Table is Passed.
  @retval EFI_UNSUPPORTED               This driver does not support.
  @retval EFI_DEVICE_ERROR              This driver cannot be started due to device Error.
  @retval EFI_NOT_STARTED               This driver has not been started.
  @retval EFI_TIMEOUT                   RestoreToOriginalMode Command timeout Happen.

**/
typedef
EFI_STATUS
(EFIAPI *FW_UPDATE_MODE_EXIT)(
  IN TCSS_RETIMER_PROTOCOL      *This,
  IN EFI_GUID                   RetimerGuid ///< GUID from ESRT ACPI Table
  );

#pragma pack (push,1)
//
// Interface structure for the TBT retimer Protocol
//
struct _TCSS_RETIMER_PROTOCOL {
  TCSS_RETIMER_INFO                   GetRetimerInfo;   ///< Get TCSS Retimer Device info
  FW_UPDATE_MODE_ENTRY                Drive;            ///< Drive TCSS retimer device into FW Update mode.
  FW_UPDATE_MODE_EXIT                 Restore;          ///< Restore TCSS retimer device into original mode.
};

struct _TCSS_RETIMER_PROTOCOL_CAPABILITY {
  UINT8                         RetimerType;
  UINT8                         RetimerDeviceCount;
  UINT16                        RetimerDeviceIndexBitMap;
  UINT8                         Reserved[12];
};

//
// Defining the Retimer Device Type Enum Variable
//
typedef enum {
  TbtPdRetimerType =0,
  NonTbtI2cRetimerType,
  // Include New Retimer Device type only above this line
  MaxRetimerDeviceType,
  AllRetimerDeviceType = 0xFE,
  InvalidRetimerDeviceType = 0xFF
} RETIMER_TYPE;

//
// Platform Data structure for the TBT PD Retimer Device
//
typedef struct {
  RETIMER_TYPE                         RetimerType;
  BOOLEAN                              RetimerDevicePresent;
  UINT8                                RetimerDeviceIndex;
  UINT8                                PdControllerIndex : 4;
  UINT8                                Reserved4Bits: 4;
  UINT8                                Reserved_Uint8;             ///< Reserved for DWORD alignment
  UINT32                               Reserved_Uint32;            ///< Reserved for DWORD alignment
  UINT64                               Reserved_Uint64;            ///< Reserved for DWORD alignment
} TBT_PD_RETIMER_PLATFORM_DATA;

//
// Platform Data structure for the Non TBT I2C Retimer Device
//
typedef struct {
  RETIMER_TYPE                         RetimerType;
  BOOLEAN                              RetimerDevicePresent;
  UINT8                                RetimerDeviceIndex;
  UINT8                                PdControllerIndex : 4;
  UINT8                                I2cControllerIndex: 4;
  BOOLEAN                              ForcePowerGpioLevel;               ///< 0 = Active Low; 1 = Active High
  GPIO_PAD                             ForcePowerGpioPad;                 ///< GPIO Pad Number
  UINT16                               RetimerBusConfig;
  UINT8                                RetimerHardwareConfig;
  UINT8                                RetimerDeviceUid;
  UINT32                               RetimerDeviceSlaveAddress;
} NON_TBT_I2C_RETIMER_PLATFORM_DATA;

//
// Platform Data Union structure for the Retimer Device
//
typedef union {
  RETIMER_TYPE                         RetimerType;
  TBT_PD_RETIMER_PLATFORM_DATA         TbtPdRetimerData;
  NON_TBT_I2C_RETIMER_PLATFORM_DATA    NonTbtI2cRetimerData;
} RETIMER_PLATFORM_DATA;

#pragma pack (pop)

#endif
