/**@file
  This file is for CastroCove Pmic Nvm update

@copyright
 Copyright (c) 2020 Intel Corporation. All rights reserved
 This software and associated documentation (if any) is furnished
 under a license and may only be used or copied in accordance
 with the terms of the license. Except as permitted by the
 license, no part of this software or documentation may be
 reproduced, stored in a retrieval system, or transmitted in any
 form or by any means without the express written consent of
 Intel Corporation.
 This file contains an 'Intel Peripheral Driver' and is uniquely
 identified as "Intel Reference Module" and is licensed for Intel
 CPUs and chipsets under the terms of your license agreement with
 Intel or your vendor. This file may be modified by the user, subject
 to additional terms of the license agreement.

@par Specification
**/

#ifndef _CASTRO_COVE_PMIC_NVM_H_
#define _CASTRO_COVE_PMIC_NVM_H_

typedef
EFI_STATUS
(EFIAPI *CASTRO_COVE_PMIC_READ8) (
  IN UINT32    RegisterOffset,
  IN UINT8     *Value,
  IN UINT8     PmicId
  );

typedef
EFI_STATUS
(EFIAPI *CASTRO_COVE_PMIC_WRITE8) (
  IN UINT32    RegisterOffset,
  IN UINT8     Value,
  IN UINT8     PmicId
  );

typedef
EFI_STATUS
(EFIAPI *CASTRO_COVE_PMIC_NVM_READ_FROM_FV) (
  IN OUT UINT8                **Buffer,
  IN OUT UINTN                *Size,
  IN EFI_GUID                 *PmicNvmFvFileGuid
  );

typedef
EFI_STATUS
(EFIAPI *CASTRO_COVE_PMIC_NVM_UPDATE_CRITERIA_CHECK) (
  IN UINT8               *Buffer,
  IN UINTN                Size,
  IN UINT8                PmicId
  );

typedef
EFI_STATUS
(EFIAPI *CASTRO_COVE_PMIC_NVM_UPDATE) (
  IN UINT8               *Buffer,
  IN UINTN                Size,
  IN UINT8                DesiredPayloadType,
  IN UINT8                PmicId,
  IN OUT UINT8           *PayloadFvBuffer,
  IN OUT UINT16          *PayloadFvSize
  );

typedef
EFI_STATUS
(EFIAPI *CASTRO_COVE_PMIC_NVM_UPDATE_FROM_APP) (
  IN UINT8               *Buffer,
  IN UINTN                Size
  );

typedef
EFI_STATUS
(EFIAPI *CASTRO_COVE_PMIC_READ_FROM_NVM) (
  IN OUT UINT8                **Buffer,
  IN OUT UINT16                *Size,
  IN UINT16                    StartAddress,
  IN UINT8                     PmicId
  );

typedef
EFI_STATUS
(EFIAPI *CASTRO_COVE_PMIC_WRITE_LOCK) (
  VOID
  );

typedef struct _EFI_CASTRO_COVE_PMIC_NVM_PROTOCOL {
  CASTRO_COVE_PMIC_READ8                        CastroCovePmicRead8;
  CASTRO_COVE_PMIC_WRITE8                       CastroCovePmicWrite8;
  CASTRO_COVE_PMIC_NVM_READ_FROM_FV             CastroCovePmicNvmReadFromFv;
  CASTRO_COVE_PMIC_NVM_UPDATE_CRITERIA_CHECK    CastroCovePmicNvmUpdateCriteriaCheck;
  CASTRO_COVE_PMIC_NVM_UPDATE                   CastroCovePmicNvmUpdate;
  CASTRO_COVE_PMIC_NVM_UPDATE_FROM_APP          CastroCovePmicNvmUpdatefromApp;
  CASTRO_COVE_PMIC_READ_FROM_NVM                CastroCovePmicReadFromNvm;
  CASTRO_COVE_PMIC_WRITE_LOCK                   CastroCovePmicWriteLock;
} EFI_CASTRO_COVE_PMIC_NVM_PROTOCOL;

#endif //_CASTRO_COVE_PMIC_NVM_H_
