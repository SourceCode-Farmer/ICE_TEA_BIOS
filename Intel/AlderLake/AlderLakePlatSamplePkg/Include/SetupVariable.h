/** @file
 
;******************************************************************************
;* Copyright 2021 Insyde Software Corp. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corp.
;*
;******************************************************************************
*/
/** @file

 @copyright
  INTEL CONFIDENTIAL
  Copyright 2004 - 2022 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains a 'Sample Driver' and is licensed as such under the terms
  of your license agreement with Intel or your vendor. This file may be modified
  by the user, subject to the additional terms of the license agreement.

@par Specification Reference:
**/

#ifndef _SETUP_VARIABLE_H_
#define _SETUP_VARIABLE_H_

#include <PchLimits.h>
#include <CpuPcieInfo.h>
#include <TcssInfo.h>
#include <CpuLimits.h>

#define VMD_MAX_DEVICES 31
#ifndef MAX_CUSTOM_P_STATES
#define MAX_CUSTOM_P_STATES 40
#endif // MAX_CUSTOM_P_STATES
#ifndef MAX_16_CUSTOM_P_STATES
#define MAX_16_CUSTOM_P_STATES 16
#endif // MAX_16_CUSTOM_P_STATES

#define MAX_IIO_PCI_EXPRESS_ROOT_PORTS    4
#define MAX_PCI_ROOT_BRIDGES              1
#define SATA_MODE_AHCI                    0
#define SATA_MODE_RAID                    1
#define SATA_TEST_MODE_ENABLE             1
#define NON_CS_DEVICES                    9
#define I2C0_SENSOR_DEVICES               7
#define I2C1_SENSOR_DEVICES               13
#define SERIAL_IO_I2C0                    0x0
#define SERIAL_IO_I2C1                    0x1
#define SERIAL_IO_I2C2                    0x2
#define SERIAL_IO_I2C3                    0x3
#define SERIAL_IO_I2C4                    0x4
#define SERIAL_IO_I2C5                    0x5
#define SERIAL_IO_I2C6                    0x6
#define SERIAL_IO_I2C7                    0x7

#define SERIAL_IO_SPI0                    0x0
#define SERIAL_IO_SPI1                    0x1
#define SERIAL_IO_SPI2                    0x2
#define SERIAL_IO_SPI3                    0x3
#define SERIAL_IO_SPI4                    0x4
#define SERIAL_IO_SPI5                    0x5
#define SERIAL_IO_SPI6                    0x6

#define SERIAL_IO_UART0                   0x0
#define SERIAL_IO_UART1                   0x1
#define SERIAL_IO_UART2                   0x2
#define SERIAL_IO_UART3                   0x3
#define SERIAL_IO_UART4                   0x4
#define SERIAL_IO_UART5                   0x5
#define SERIAL_IO_UART6                   0x6

//
// This is 8 Bits Map of I2C Devices for combined use in BoardConfigPatchTable.h and in PchSetup.hfr
//
#define SERIAL_IO_I2C_TOUCHPAD            0x1
#define SERIAL_IO_I2C_TOUCHPANEL          0x2
#define SERIAL_IO_I2C_UCMC                0x4
#define SERIAL_IO_I2C_PD02                0x8
//#define 5TH_SERIAL_IO_I2C_DEVICE       0x10
//#define 6TH_SERIAL_IO_I2C_DEVICE       0x20
//#define 7TH_SERIAL_IO_I2C_DEVICE       0x40
//#define 8TH_SERIAL_IO_I2C_DEVICE       0x80

//
// This is 8 Bits Map of SPI Devices for combined use in BoardConfigPatchTable.h and in PchSetup.hfr
//
#define SERIAL_IO_SPI_FINGERPRINT         0x1
//#define 2nd_SERIAL_IO_SPI_DEVICE       0x02
//#define 3rd_SERIAL_IO_SPI_DEVICE       0x04
//#define 4TH_SERIAL_IO_SPI_DEVICE       0x08
//#define 5TH_SERIAL_IO_SPI_DEVICE       0x10
//#define 6TH_SERIAL_IO_SPI_DEVICE       0x20
//#define 7TH_SERIAL_IO_SPI_DEVICE       0x40
//#define 8TH_SERIAL_IO_SPI_DEVICE       0x80

#define HDAUDIO_FEATURES                    9
#define HDAUDIO_PP_MODULES                  32

#define HDAUDIO_DMIC_CLOCK_SELECT_BOTH      0
#define HDAUDIO_DMIC_CLOCK_SELECT_CLKA      1
#define HDAUDIO_DMIC_CLOCK_SELECT_CLKB      2

#define GUID_CHARS_NUMBER                   37 // 36 GUID chars + null termination
#define FIVR_RAIL_S0IX_SX_STATES_MAX       6  // Number of availabe S0ix/Sx policy bits in FIVR Rail configuration
#define FIVR_RAIL_EXT_VOLTAGE_STATES_MAX   4  // Number of supported voltage settings for FIVR Rail configuration

#define SETUP_MAX_USB2_PORTS              16
#define SETUP_MAX_USB3_PORTS              10

// DTT EC supported Fans & Sensors
#define MAX_EC_SENSORS                    5
#define MAX_EC_FANS                       3
#define VBT_SELECT_EDP                    0x0
#define VBT_SELECT_MIPI                   0x1

#define OPROM_EFI                         1
#define OPROM_LEGACY                      0

#define MIPICAM_I2C_DEVICES_COUNT         12
#define MIPICAM_GPIO_COUNT                6
#define MIPICAM_MODULE_NAME_LENGTH        16
#define MIPICAM_HID_MIN_LENGTH            6
#define MIPICAM_HID_LENGTH                20
#define DTBT_CONTROLLER_SUPPORTED         2
#define ITBT_ROOT_PORTS_SUPPORTED         6
#define ITBT_HOST_DMA_SUPPORTED           3
#define CPU_FM_TGL_ULT_ULX 0x000006C0
#define CPU_FM_ADL_ULT_ULX 0x000006A0
#define CPU_FM_ADL_DT_HALO 0x00000670
#define CPU_STEPPING_ADL_A0               0

#define IBECC_REGIONS_MAX                 8

//
// Max number of VF point offset
//
#ifndef CPU_OC_MAX_VF_POINTS
#define CPU_OC_MAX_VF_POINTS   0xF
#endif

#define MAX_VR_NUM             5

#ifndef TURBO_RATIO_LIMIT_ARRAY_SIZE
#define TURBO_RATIO_LIMIT_ARRAY_SIZE 8
#endif

#ifndef  BIT0
#define  BIT0     0x00000001
#endif
#ifndef  BIT1
#define  BIT1     0x00000002
#endif
#ifndef  BIT2
#define  BIT2     0x00000004
#endif
#ifndef  BIT3
#define  BIT3     0x00000008
#endif
#ifndef  BIT4
#define  BIT4     0x00000010
#endif
#ifndef  BIT5
#define  BIT5     0x00000020
#endif
#ifndef  BIT6
#define  BIT6     0x00000040
#endif
#ifndef  BIT7
#define  BIT7     0x00000080
#endif
#ifndef  BIT8
#define  BIT8     0x00000100
#endif
#ifndef  BIT9
#define  BIT9     0x00000200
#endif
#ifndef  BIT10
#define  BIT10    0x00000400
#endif
#ifndef  BIT11
#define  BIT11    0x00000800
#endif
#ifndef  BIT12
#define  BIT12    0x00001000
#endif
#ifndef  BIT13
#define  BIT13    0x00002000
#endif
#ifndef  BIT14
#define  BIT14    0x00004000
#endif
#ifndef  BIT15
#define  BIT15    0x00008000
#endif
#ifndef  BIT16
#define  BIT16    0x00010000
#endif
#ifndef  BIT17
#define  BIT17    0x00020000
#endif
#ifndef  BIT18
#define  BIT18    0x00040000
#endif
#ifndef  BIT19
#define  BIT19    0x00080000
#endif
#ifndef  BIT20
#define  BIT20    0x00100000
#endif
#ifndef  BIT21
#define  BIT21    0x00200000
#endif
#ifndef  BIT22
#define  BIT22    0x00400000
#endif
#ifndef  BIT23
#define  BIT23    0x00800000
#endif
#ifndef  BIT24
#define  BIT24    0x01000000
#endif
#ifndef  BIT25
#define  BIT25    0x02000000
#endif
#ifndef  BIT26
#define  BIT26    0x04000000
#endif
#ifndef  BIT27
#define  BIT27    0x08000000
#endif
#ifndef  BIT28
#define  BIT28    0x10000000
#endif
#ifndef  BIT29
#define  BIT29    0x20000000
#endif
#ifndef  BIT30
#define  BIT30    0x40000000
#endif
#ifndef  BIT31
#define  BIT31    0x80000000
#endif
#pragma pack(1)
/**
 Making any setup structure change after code frozen
 will need to maintain backward compatibility, bump up
 structure revision and update below history table\n
  <b>Revision 1</b>:  - Initial version.
  <b>Revision 2</b>:  - Added PepVmd
  <b>Revision 3</b>:  - Added CnvExtClock
  <b>Revision 4</b>:  - Added PPAG support
  <b>Revision 5</b>:  - Added FlashMode
  <b>Revision 6</b>:  - Cpu Pciexpress Gen3 Compatability
  <b>Revision 7</b>:  - Added DeepestUSBSleepWakeCapability
  <b>Revision 8</b>:  - Add PEP constraint support on SATA ports
  <b>Revision 9</b>:  - Added PepHeci3
  <b>Revision 10</b>: - Added 11ax R2 support and UHB channel support
  <b>Revision 11</b>: - Deprecate IccPllShutdownEnabled
  <b>Revision 12</b>: - Deprecated TR A0 workaround setup options
  <b>Revision 13</b>: - Deprecated MipiCam_LanesClkDiv and Added MipiCam_Link(X)_LanesClkDiv for all 6(0-5) Links.
  <b>Revision 14</b>: - Added Fan 2, Fan 3 and dGPU paticipant, AdlSAepBoard setup variable
  <b>Revision 15</b>: - Added WiFi Time Average SAR support
  <b>Revision 16</b>: - Added PLN support
  <b>Revision 17</b>: - Deprecated TbtSkipPciOprom
  <b>Revision 18</b>: - TcssPdPsOnEnable
  <b>Revision 19</b>: - Adding PepPcieLan,PepPcieWlan,PepPcieGfx and PepPcieOther for PCIe root port constraint configuration.
  <b>Revision 20</b>: - Adding AuxRailBudget
  <b>Revision 21</b>: - Added CVF Support
  <b>Revision 22</b>: - Deprecated CurrentSecurityMode, using EnablePcieTunnelingOverUsb4 to represent security level
  <b>Revision 23</b>: - Added WiFi Time Average SAR support
  <b>Revision 24</b>: - Added DTbt Off Method Delay optimization.
  <b>Revision 25</b>: - Replace TbtVtdBaseSecurity with DisableTbtPcieTreeBme.
  <b>Revision 26</b>: - Added StorageRtd3Support;
  <b>Revision 27</b>: - Adding PMAX device settings
  <b>Revision 28</b>: - Deprecated Gpio3ForcePwrDly
  <b>Revision 29</b>: - Added MPdt Support
  <b>Revision 30</b>: - Add DiscreteTbtPlatformConfigurationSupport Setup variable.
  <b>Revision 31</b>: - Deprecated ITbtPcieTunnelingForUsb4
  <b>Revision 32</b>: - Added DtbtCmSupport and Usb4CmMode
  <b>Revision 33</b>: - Added PepUfs0 and PepUfs1
  <b>Revision 34</b>: - Added TestEnableTcssCovTypeA[MAX_TCSS_USB3_PORTS] and TestDisableTcssPortUcxxSupport[10]
  <b>Revision 35</b>: - Deprecated TbtUsbOn
  <b>Revision 36</b>: - Depricated SecondConfigReworkOption.
  <b>Revision 37</b>: - DpInExternalEn
  <b>Revision 38</b>: - Deprecated TestEnableTcssCovTypeA[MAX_TCSS_USB3_PORTS].
  <b>Revision 39</b>: - Added CpuBclkFreq, PegDmiClkFreq, and DccClkCtrl
  <b>Revision 40</b>: - Added UsbcDataRoleSwapPlatformDisable
  <b>Revision 41</b>: - Added TwoMapleRidgeBoard
  <b>Revision 42</b>: - Added FlashID Support for link 0-5
  <b>Revision 43</b>: - Added SerialIoTiming Parameters and PEP Constraint for I2C6 and I2C7
  <b>Revision 44</b>: - Deprecated TwoMapleRidgeBoard add DTbtDpInSupport
  <b>Revision 45</b>: - XmlCli Related Knobs are now removed and reserved bytes added for offset consistency
  <b>Revision 46</b>: - Deprecated WPFC (WiFi Phy Filter Configuration)
  <b>Revision 47</b>: - Added WwanTPer2ResDelayMs
  <b>Revision 48</b>: - Added WwanOemSvid and WwanSvidTimeout
  <b>Revision 49</b>: - Updated BT Tile to Rev 1 (Ring My PC)
  <b>Revision 50</b>: - Added WifiRegulatoryConfigurations, WifiUartConfigurations, WifiUnii4 and WifiIndoorControl
  <b>Revision 51</b>: - Added pep constraint support for EMMC
  <b>Revision 52</b>: - Deprecated PPR interface for MipiCamera
  <b>Revision 53</b>: - Deprecated SdCard D3 Cold Support
  <b>Revision 54</b>: - Added WirelessCnvConfigDevice to enable/disable WCCD
  <b>Revision 55</b>: - Add CurrentCpuBclkFreq, CurrentPegDmiClkFreq for DCC OC
  <b>Revision 56</b>: - Added WLAN/WWAN Antenna Isolation support for WiFi
  <b>Revision 57</b>: - Deprecated SGOM variables.
  <b>Revision 58</b>: - Add WgdsWiFiSarDeltaGroup and more WgdsWifiSarDeltaGroup elements for WGDS revision 3
  <b>Revisiom 59</b>: - RP08 Wake Rework Done
  <b>Revision 60</b>: - Rp08 D3 cold Support
  <b>Revision 61</b>: - Add WifiTASSelectionValue for RPL (WTSY revision 1)
  <b>Revision 62</b>: - Added RplSAepBoard setup variable
  <b>Revision 63</b>: - Added DMI Policies
**/

#define SETUP_DATA_REVISION 63

typedef struct {
  UINT8   Revision;
  UINT8   FastBoot;
  UINT8   FastBootWdt;
  UINT8   SkipHIIUpdate;
  UINT8   ConInBehavior;
  UINT8   DisplayBootMode;
  UINT8   Numlock;
  UINT8   InteractiveText;
  UINT8   LazyConIn;
  UINT8   VirtualKeyboard;
  UINT8   FirmwareConfiguration;
  UINT8   OpRomPost; // [Legacy] \ EFI Compatible
  // General PCI Settings: [] - default
  UINT8   PciLatency; //[32]\ 64 \ 96 \ 128 \ 160 \ 192 \ 224 \ 248
  UINT8   AcpiAuto;
  UINT8   AcpiSleepState;
  UINT8   AcpiHibernate;
  UINT8   AcpiTableRevision;
  UINT8   PciExpNative;
  UINT8   NativeAspmEnable;
  UINT8   PtidSupport;
  // AcpiDebug Setup Options
  UINT8   AcpiDebug;
  UINT32  AcpiDebugAddress;
  UINT8   PciDelayOptimizationEcr;
  // S5 RTC wakeup setup options
  UINT8   WakeOnRTCS5;
  UINT8   PeciAccessMethod;
  UINT8   LowPowerS0Idle;
  UINT8   PepCpu;
  UINT8   PepGfx;
  UINT8   PepSataContraints;
  UINT8   PepUart;
  UINT8   PepI2c0;
  UINT8   PepI2c1;
  UINT8   PepI2c2;
  UINT8   PepI2c3;
  UINT8   PepI2c4;
  UINT8   PepI2c5;
  UINT8   PepSpi;
  UINT8   PepXhci;
  UINT8   PepAudio;
  UINT8   PepIpu;
  UINT8   PepGna;
  UINT8   PepCsme;
  UINT8   PepGbe;
  UINT8   PepThc0;
  UINT8   PepThc1;
  UINT8   PepTcss;
  UINT8   CSNotifyEC;
  UINT8   CSDebugLightEC;
  UINT8   EcLowPowerMode;
  UINT8   EcPeciModeUnsupport;
  UINT8   EcPeciMode;
  UINT8   EcDebugLed;
  UINT8   ECBaseCsPwrPolicy;
  UINT8   SensorStandby;
  UINT8   PL1LimitCS;
  UINT16  PL1LimitCSValue;
  UINT8   TenSecondPowerButtonSupport;
  UINT8   TenSecondPowerButtonEnable;
  UINT8   LpitResidencyCounter;
  UINT8   PseudoG3State;
  // Reading SSDT table from file
  UINT8   LoadSSDTFromFile;
  //
  // On Screen Branding
  //
  UINT16  OperatingSystem;
  UINT16  OemBadgingBrand;
  UINT8   BootFirstToShell;

  UINT8   SensorHubType;
  UINT8   DebugUsbUartSupport;
  UINT8   DebugUsbUart;
  UINT8   EfiNetworkSupport;
  UINT8   Reserved; // Get rid of "EnableClockSpreadSpec"
  UINT8   UsbSensorHub;

  UINT8   HddAcousticPowerManagement;
  UINT8   HddAcousticMode;

  //
  // Recovery Configuration
  //
  UINT8   IsRecoveryJumper;

  //
  // MipiCam sensor configuration menu
  //
  UINT8   MipiCam_ControlLogic0;             // None/discrete/PMIC
  UINT8   MipiCam_ControlLogic1;             // None/discrete/PMIC
  UINT8   MipiCam_ControlLogic2;             // None/discrete/PMIC
  UINT8   MipiCam_ControlLogic3;             // None/discrete/PMIC
  UINT8   MipiCam_ControlLogic4;             // None/discrete/PMIC
  UINT8   MipiCam_ControlLogic5;             // None/discrete/PMIC

  UINT8   MipiCam_Link0;                     // Enabled / Disabled
  UINT8   MipiCam_Link1;                     // Enabled / Disabled
  UINT8   MipiCam_Link2;                     // Enabled / Disabled
  UINT8   MipiCam_Link3;                     // Enabled / Disabled
  UINT8   MipiCam_Link4;                     // Enabled / Disabled
  UINT8   MipiCam_Link5;                     // Enabled / Disabled

  UINT8   MipiCam4Enable;                    // Enable 5th camera
  UINT8   MipiCam5Enable;                    // Enable 6th camera

  UINT8   MipiCam_LanesClkDiv;               // This member is deprecated and should be deleted in next generation

  UINT8   MipiCam_ControlLogic0_Type;
  UINT8   MipiCam_ControlLogic0_CrdVersion;
  UINT32  MipiCam_ControlLogic0_InputClock;
  UINT8   MipiCam_ControlLogic0_PchClockSource;
  UINT8   MipiCam_ControlLogic0_GpioPinsEnabled;
  UINT8   MipiCam_ControlLogic0_I2cChannel;
  UINT16  MipiCam_ControlLogic0_I2cAddress;
  UINT8   MipiCam_ControlLogic0_Pld;
  UINT8   MipiCam_ControlLogic0_Wled1FlashMaxCurrent;
  UINT8   MipiCam_ControlLogic0_Wled1TorchMaxCurrent;
  UINT8   MipiCam_ControlLogic0_Wled2FlashMaxCurrent;
  UINT8   MipiCam_ControlLogic0_Wled2TorchMaxCurrent;
  UINT8   MipiCam_ControlLogic0_SubPlatformId;
  UINT8   MipiCam_ControlLogic0_Wled1Type;
  UINT8   MipiCam_ControlLogic0_Wled2Type;
  UINT8   MipiCam_ControlLogic0_GpioGroupPadNumber[MIPICAM_GPIO_COUNT];
  UINT8   MipiCam_ControlLogic0_GpioGroupNumber[MIPICAM_GPIO_COUNT];
  UINT8   MipiCam_ControlLogic0_GpioFunction[MIPICAM_GPIO_COUNT];
  UINT8   MipiCam_ControlLogic0_GpioActiveValue[MIPICAM_GPIO_COUNT];
  UINT8   MipiCam_ControlLogic0_GpioInitialValue[MIPICAM_GPIO_COUNT];

  UINT8   MipiCam_ControlLogic1_Type;
  UINT8   MipiCam_ControlLogic1_CrdVersion;
  UINT32  MipiCam_ControlLogic1_InputClock;
  UINT8   MipiCam_ControlLogic1_PchClockSource;
  UINT8   MipiCam_ControlLogic1_GpioPinsEnabled;
  UINT8   MipiCam_ControlLogic1_I2cChannel;
  UINT16  MipiCam_ControlLogic1_I2cAddress;
  UINT8   MipiCam_ControlLogic1_Pld;
  UINT8   MipiCam_ControlLogic1_Wled1FlashMaxCurrent;
  UINT8   MipiCam_ControlLogic1_Wled1TorchMaxCurrent;
  UINT8   MipiCam_ControlLogic1_Wled2FlashMaxCurrent;
  UINT8   MipiCam_ControlLogic1_Wled2TorchMaxCurrent;
  UINT8   MipiCam_ControlLogic1_SubPlatformId;
  UINT8   MipiCam_ControlLogic1_Wled1Type;
  UINT8   MipiCam_ControlLogic1_Wled2Type;
  UINT8   MipiCam_ControlLogic1_GpioGroupPadNumber[MIPICAM_GPIO_COUNT];
  UINT8   MipiCam_ControlLogic1_GpioGroupNumber[MIPICAM_GPIO_COUNT];
  UINT8   MipiCam_ControlLogic1_GpioFunction[MIPICAM_GPIO_COUNT];
  UINT8   MipiCam_ControlLogic1_GpioActiveValue[MIPICAM_GPIO_COUNT];
  UINT8   MipiCam_ControlLogic1_GpioInitialValue[MIPICAM_GPIO_COUNT];

  UINT8   MipiCam_ControlLogic2_Type;
  UINT8   MipiCam_ControlLogic2_CrdVersion;
  UINT32  MipiCam_ControlLogic2_InputClock;
  UINT8   MipiCam_ControlLogic2_PchClockSource;
  UINT8   MipiCam_ControlLogic2_GpioPinsEnabled;
  UINT8   MipiCam_ControlLogic2_I2cChannel;
  UINT16  MipiCam_ControlLogic2_I2cAddress;
  UINT8   MipiCam_ControlLogic2_Pld;
  UINT8   MipiCam_ControlLogic2_Wled1FlashMaxCurrent;
  UINT8   MipiCam_ControlLogic2_Wled1TorchMaxCurrent;
  UINT8   MipiCam_ControlLogic2_Wled2FlashMaxCurrent;
  UINT8   MipiCam_ControlLogic2_Wled2TorchMaxCurrent;
  UINT8   MipiCam_ControlLogic2_SubPlatformId;
  UINT8   MipiCam_ControlLogic2_Wled1Type;
  UINT8   MipiCam_ControlLogic2_Wled2Type;
  UINT8   MipiCam_ControlLogic2_GpioGroupPadNumber[MIPICAM_GPIO_COUNT];
  UINT8   MipiCam_ControlLogic2_GpioGroupNumber[MIPICAM_GPIO_COUNT];
  UINT8   MipiCam_ControlLogic2_GpioFunction[MIPICAM_GPIO_COUNT];
  UINT8   MipiCam_ControlLogic2_GpioActiveValue[MIPICAM_GPIO_COUNT];
  UINT8   MipiCam_ControlLogic2_GpioInitialValue[MIPICAM_GPIO_COUNT];

  UINT8   MipiCam_ControlLogic3_Type;
  UINT8   MipiCam_ControlLogic3_CrdVersion;
  UINT32  MipiCam_ControlLogic3_InputClock;
  UINT8   MipiCam_ControlLogic3_PchClockSource;
  UINT8   MipiCam_ControlLogic3_GpioPinsEnabled;
  UINT8   MipiCam_ControlLogic3_I2cChannel;
  UINT16  MipiCam_ControlLogic3_I2cAddress;
  UINT8   MipiCam_ControlLogic3_Pld;
  UINT8   MipiCam_ControlLogic3_Wled1FlashMaxCurrent;
  UINT8   MipiCam_ControlLogic3_Wled1TorchMaxCurrent;
  UINT8   MipiCam_ControlLogic3_Wled2FlashMaxCurrent;
  UINT8   MipiCam_ControlLogic3_Wled2TorchMaxCurrent;
  UINT8   MipiCam_ControlLogic3_SubPlatformId;
  UINT8   MipiCam_ControlLogic3_Wled1Type;
  UINT8   MipiCam_ControlLogic3_Wled2Type;
  UINT8   MipiCam_ControlLogic3_GpioGroupPadNumber[MIPICAM_GPIO_COUNT];
  UINT8   MipiCam_ControlLogic3_GpioGroupNumber[MIPICAM_GPIO_COUNT];
  UINT8   MipiCam_ControlLogic3_GpioFunction[MIPICAM_GPIO_COUNT];
  UINT8   MipiCam_ControlLogic3_GpioActiveValue[MIPICAM_GPIO_COUNT];
  UINT8   MipiCam_ControlLogic3_GpioInitialValue[MIPICAM_GPIO_COUNT];

  UINT8   MipiCam_ControlLogic4_Type;
  UINT8   MipiCam_ControlLogic4_CrdVersion;
  UINT32  MipiCam_ControlLogic4_InputClock;
  UINT8   MipiCam_ControlLogic4_PchClockSource;
  UINT8   MipiCam_ControlLogic4_GpioPinsEnabled;
  UINT8   MipiCam_ControlLogic4_I2cChannel;
  UINT16  MipiCam_ControlLogic4_I2cAddress;
  UINT8   MipiCam_ControlLogic4_Pld;
  UINT8   MipiCam_ControlLogic4_Wled1FlashMaxCurrent;
  UINT8   MipiCam_ControlLogic4_Wled1TorchMaxCurrent;
  UINT8   MipiCam_ControlLogic4_Wled2FlashMaxCurrent;
  UINT8   MipiCam_ControlLogic4_Wled2TorchMaxCurrent;
  UINT8   MipiCam_ControlLogic4_SubPlatformId;
  UINT8   MipiCam_ControlLogic4_Wled1Type;
  UINT8   MipiCam_ControlLogic4_Wled2Type;
  UINT8   MipiCam_ControlLogic4_GpioGroupPadNumber[MIPICAM_GPIO_COUNT];
  UINT8   MipiCam_ControlLogic4_GpioGroupNumber[MIPICAM_GPIO_COUNT];
  UINT8   MipiCam_ControlLogic4_GpioFunction[MIPICAM_GPIO_COUNT];
  UINT8   MipiCam_ControlLogic4_GpioActiveValue[MIPICAM_GPIO_COUNT];
  UINT8   MipiCam_ControlLogic4_GpioInitialValue[MIPICAM_GPIO_COUNT];

  UINT8   MipiCam_ControlLogic5_Type;
  UINT8   MipiCam_ControlLogic5_CrdVersion;
  UINT32  MipiCam_ControlLogic5_InputClock;
  UINT8   MipiCam_ControlLogic5_PchClockSource;
  UINT8   MipiCam_ControlLogic5_GpioPinsEnabled;
  UINT8   MipiCam_ControlLogic5_I2cChannel;
  UINT16  MipiCam_ControlLogic5_I2cAddress;
  UINT8   MipiCam_ControlLogic5_Pld;
  UINT8   MipiCam_ControlLogic5_Wled1FlashMaxCurrent;
  UINT8   MipiCam_ControlLogic5_Wled1TorchMaxCurrent;
  UINT8   MipiCam_ControlLogic5_Wled2FlashMaxCurrent;
  UINT8   MipiCam_ControlLogic5_Wled2TorchMaxCurrent;
  UINT8   MipiCam_ControlLogic5_SubPlatformId;
  UINT8   MipiCam_ControlLogic5_Wled1Type;
  UINT8   MipiCam_ControlLogic5_Wled2Type;
  UINT8   MipiCam_ControlLogic5_GpioGroupPadNumber[MIPICAM_GPIO_COUNT];
  UINT8   MipiCam_ControlLogic5_GpioGroupNumber[MIPICAM_GPIO_COUNT];
  UINT8   MipiCam_ControlLogic5_GpioFunction[MIPICAM_GPIO_COUNT];
  UINT8   MipiCam_ControlLogic5_GpioActiveValue[MIPICAM_GPIO_COUNT];
  UINT8   MipiCam_ControlLogic5_GpioInitialValue[MIPICAM_GPIO_COUNT];

  UINT8   MipiCam_Link0_SensorModel;         // IMX135 / OV5693 / IMX179 / OV8858 / OV2740-IVCAM / IMX208 / OV9728 / IMX188 / User0
  UINT16  MipiCam_Link0_UserHid[MIPICAM_HID_LENGTH];
  UINT8   MipiCam_Link0_CameraPhysicalLocation;
  UINT16  MipiCam_Link0_ModuleName[MIPICAM_MODULE_NAME_LENGTH];
  UINT8   MipiCam_Link0_I2cDevicesEnabled;   // number of I2C devices defined for this link
  UINT8   MipiCam_Link0_I2cChannel;
  UINT16  MipiCam_Link0_I2cAddress[MIPICAM_I2C_DEVICES_COUNT];
  UINT8   MipiCam_Link0_I2cDeviceType[MIPICAM_I2C_DEVICES_COUNT];
  UINT8   MipiCam_Link0_DriverData_Version;
  UINT8   MipiCam_Link0_DriverData_CrdVersion;
  UINT8   MipiCam_Link0_DriverData_LinkUsed;
  UINT8   MipiCam_Link0_DriverData_LaneUsed;
  UINT8   MipiCam_Link0_DriverData_CsiSpeed;
  UINT8   MipiCam_Link0_DriverData_EepromType;
  UINT8   MipiCam_Link0_DriverData_VcmType;
  UINT8   MipiCam_Link0_DriverData_CustomData[4];
  UINT32  MipiCam_Link0_DriverData_Mclk;
  UINT8   MipiCam_Link0_DriverData_ControlLogic;
  UINT8   MipiCam_Link0_DriverData_FlashSupport;
  UINT8   MipiCam_Link0_DriverData_PrivacyLed;
  UINT8   MipiCam_Link0_DriverData_Degree;
  UINT8   MipiCam_Link0_DriverData_PmicPosition;
  UINT8   MipiCam_Link0_DriverData_VoltageRail;
  UINT8   MipiCam_Link0_DriverData_PprValue;         // Deprecated from Revision 52
  UINT8   MipiCam_Link0_DriverData_PprUnit;         // Deprecated from Revision 52
  UINT8   MipiCam_Link0_FlashDriverSelection;

  UINT8   MipiCam_Link1_SensorModel;         // IMX135 / OV5693 / IMX179 / OV8858 / OV2740-IVCAM / IMX208 / OV9728 / IMX188 / User1
  UINT16  MipiCam_Link1_UserHid[MIPICAM_HID_LENGTH];
  UINT8   MipiCam_Link1_CameraPhysicalLocation;
  UINT16  MipiCam_Link1_ModuleName[MIPICAM_MODULE_NAME_LENGTH];
  UINT8   MipiCam_Link1_I2cDevicesEnabled;   // number of I2C devices defined for this link
  UINT8   MipiCam_Link1_I2cChannel;
  UINT16  MipiCam_Link1_I2cAddress[MIPICAM_I2C_DEVICES_COUNT];
  UINT8   MipiCam_Link1_I2cDeviceType[MIPICAM_I2C_DEVICES_COUNT];
  UINT8   MipiCam_Link1_DriverData_Version;
  UINT8   MipiCam_Link1_DriverData_LinkUsed;
  UINT8   MipiCam_Link1_DriverData_CrdVersion;
  UINT8   MipiCam_Link1_DriverData_LaneUsed;
  UINT8   MipiCam_Link1_DriverData_CsiSpeed;
  UINT8   MipiCam_Link1_DriverData_EepromType;
  UINT8   MipiCam_Link1_DriverData_VcmType;
  UINT8   MipiCam_Link1_DriverData_CustomData[4];
  UINT32  MipiCam_Link1_DriverData_Mclk;
  UINT8   MipiCam_Link1_DriverData_ControlLogic;
  UINT8   MipiCam_Link1_DriverData_FlashSupport;
  UINT8   MipiCam_Link1_DriverData_PrivacyLed;
  UINT8   MipiCam_Link1_DriverData_Degree;
  UINT8   MipiCam_Link1_DriverData_PmicPosition;
  UINT8   MipiCam_Link1_DriverData_VoltageRail;
  UINT8   MipiCam_Link1_DriverData_PprValue;         // Deprecated from Revision 52
  UINT8   MipiCam_Link1_DriverData_PprUnit;         // Deprecated from Revision 52
  UINT8   MipiCam_Link1_FlashDriverSelection;

  UINT8   MipiCam_Link2_SensorModel;         // IMX135 / OV5693 / IMX179 / OV8858 / OV2740-IVCAM / IMX208 / OV9728 / IMX188 / User2
  UINT16  MipiCam_Link2_UserHid[MIPICAM_HID_LENGTH];
  UINT8   MipiCam_Link2_CameraPhysicalLocation;
  UINT16  MipiCam_Link2_ModuleName[MIPICAM_MODULE_NAME_LENGTH];
  UINT8   MipiCam_Link2_I2cDevicesEnabled;   // number of I2C devices defined for this link
  UINT8   MipiCam_Link2_I2cChannel;
  UINT16  MipiCam_Link2_I2cAddress[MIPICAM_I2C_DEVICES_COUNT];
  UINT8   MipiCam_Link2_I2cDeviceType[MIPICAM_I2C_DEVICES_COUNT];
  UINT8   MipiCam_Link2_DriverData_Version;
  UINT8   MipiCam_Link2_DriverData_CrdVersion;
  UINT8   MipiCam_Link2_DriverData_LinkUsed;
  UINT8   MipiCam_Link2_DriverData_LaneUsed;
  UINT8   MipiCam_Link2_DriverData_CsiSpeed;
  UINT8   MipiCam_Link2_DriverData_EepromType;
  UINT8   MipiCam_Link2_DriverData_VcmType;
  UINT8   MipiCam_Link2_DriverData_CustomData[4];
  UINT32  MipiCam_Link2_DriverData_Mclk;
  UINT8   MipiCam_Link2_DriverData_ControlLogic;
  UINT8   MipiCam_Link2_DriverData_FlashSupport;
  UINT8   MipiCam_Link2_DriverData_PrivacyLed;
  UINT8   MipiCam_Link2_DriverData_Degree;
  UINT8   MipiCam_Link2_DriverData_PmicPosition;
  UINT8   MipiCam_Link2_DriverData_VoltageRail;
  UINT8   MipiCam_Link2_DriverData_PprValue;         // Deprecated from Revision 52
  UINT8   MipiCam_Link2_DriverData_PprUnit;         // Deprecated from Revision 52
  UINT8   MipiCam_Link2_FlashDriverSelection;

  UINT8   MipiCam_Link3_SensorModel;         // IMX135 / OV5693 / IMX179 / OV8858 / OV2740-IVCAM / IMX208 / OV9728 / IMX188 / User3
  UINT16  MipiCam_Link3_UserHid[MIPICAM_HID_LENGTH];
  UINT8   MipiCam_Link3_CameraPhysicalLocation;
  UINT16  MipiCam_Link3_ModuleName[MIPICAM_MODULE_NAME_LENGTH];
  UINT8   MipiCam_Link3_I2cDevicesEnabled;   // number of I2C devices defined for this link
  UINT8   MipiCam_Link3_I2cChannel;
  UINT16  MipiCam_Link3_I2cAddress[MIPICAM_I2C_DEVICES_COUNT];
  UINT8   MipiCam_Link3_I2cDeviceType[MIPICAM_I2C_DEVICES_COUNT];
  UINT8   MipiCam_Link3_DriverData_Version;
  UINT8   MipiCam_Link3_DriverData_CrdVersion;
  UINT8   MipiCam_Link3_DriverData_LinkUsed;
  UINT8   MipiCam_Link3_DriverData_LaneUsed;
  UINT8   MipiCam_Link3_DriverData_CsiSpeed;
  UINT8   MipiCam_Link3_DriverData_EepromType;
  UINT8   MipiCam_Link3_DriverData_VcmType;
  UINT8   MipiCam_Link3_DriverData_CustomData[4];
  UINT32  MipiCam_Link3_DriverData_Mclk;
  UINT8   MipiCam_Link3_DriverData_ControlLogic;
  UINT8   MipiCam_Link3_DriverData_FlashSupport;
  UINT8   MipiCam_Link3_DriverData_PrivacyLed;
  UINT8   MipiCam_Link3_DriverData_Degree;
  UINT8   MipiCam_Link3_DriverData_PmicPosition;
  UINT8   MipiCam_Link3_DriverData_VoltageRail;
  UINT8   MipiCam_Link3_DriverData_PprValue;         // Deprecated from Revision 52
  UINT8   MipiCam_Link3_DriverData_PprUnit;         // Deprecated from Revision 52
  UINT8   MipiCam_Link3_FlashDriverSelection;

  UINT8   MipiCam_Link4_SensorModel;         // IMX135 / OV5693 / IMX179 / OV8858 / OV2740-IVCAM / IMX208 / OV9728 / IMX188 / User3
  UINT16  MipiCam_Link4_UserHid[MIPICAM_HID_LENGTH];
  UINT8   MipiCam_Link4_CameraPhysicalLocation;
  UINT16  MipiCam_Link4_ModuleName[MIPICAM_MODULE_NAME_LENGTH];
  UINT8   MipiCam_Link4_I2cDevicesEnabled;   // number of I2C devices defined for this link
  UINT8   MipiCam_Link4_I2cChannel;
  UINT16  MipiCam_Link4_I2cAddress[MIPICAM_I2C_DEVICES_COUNT];
  UINT8   MipiCam_Link4_I2cDeviceType[MIPICAM_I2C_DEVICES_COUNT];
  UINT8   MipiCam_Link4_DriverData_Version;
  UINT8   MipiCam_Link4_DriverData_CrdVersion;
  UINT8   MipiCam_Link4_DriverData_LinkUsed;
  UINT8   MipiCam_Link4_DriverData_LaneUsed;
  UINT8   MipiCam_Link4_DriverData_CsiSpeed;
  UINT8   MipiCam_Link4_DriverData_EepromType;
  UINT8   MipiCam_Link4_DriverData_VcmType;
  UINT8   MipiCam_Link4_DriverData_CustomData[4];
  UINT32  MipiCam_Link4_DriverData_Mclk;
  UINT8   MipiCam_Link4_DriverData_ControlLogic;
  UINT8   MipiCam_Link4_DriverData_FlashSupport;
  UINT8   MipiCam_Link4_DriverData_PrivacyLed;
  UINT8   MipiCam_Link4_DriverData_Degree;
  UINT8   MipiCam_Link4_DriverData_PmicPosition;
  UINT8   MipiCam_Link4_DriverData_VoltageRail;
  UINT8   MipiCam_Link4_DriverData_PprValue;         // Deprecated from Revision 52
  UINT8   MipiCam_Link4_DriverData_PprUnit;         // Deprecated from Revision 52
  UINT8   MipiCam_Link4_FlashDriverSelection;

  UINT8   MipiCam_Link5_SensorModel;         // IMX135 / OV5693 / IMX179 / OV8858 / OV2740-IVCAM / IMX208 / OV9728 / IMX188 / User3
  UINT16  MipiCam_Link5_UserHid[MIPICAM_HID_LENGTH];
  UINT8   MipiCam_Link5_CameraPhysicalLocation;
  UINT16  MipiCam_Link5_ModuleName[MIPICAM_MODULE_NAME_LENGTH];
  UINT8   MipiCam_Link5_I2cDevicesEnabled;   // number of I2C devices defined for this link
  UINT8   MipiCam_Link5_I2cChannel;
  UINT16  MipiCam_Link5_I2cAddress[MIPICAM_I2C_DEVICES_COUNT];
  UINT8   MipiCam_Link5_I2cDeviceType[MIPICAM_I2C_DEVICES_COUNT];
  UINT8   MipiCam_Link5_DriverData_Version;
  UINT8   MipiCam_Link5_DriverData_CrdVersion;
  UINT8   MipiCam_Link5_DriverData_LinkUsed;
  UINT8   MipiCam_Link5_DriverData_LaneUsed;
  UINT8   MipiCam_Link5_DriverData_CsiSpeed;
  UINT8   MipiCam_Link5_DriverData_EepromType;
  UINT8   MipiCam_Link5_DriverData_VcmType;
  UINT8   MipiCam_Link5_DriverData_CustomData[4];
  UINT32  MipiCam_Link5_DriverData_Mclk;
  UINT8   MipiCam_Link5_DriverData_ControlLogic;
  UINT8   MipiCam_Link5_DriverData_FlashSupport;
  UINT8   MipiCam_Link5_DriverData_PrivacyLed;
  UINT8   MipiCam_Link5_DriverData_Degree;
  UINT8   MipiCam_Link5_DriverData_PmicPosition;
  UINT8   MipiCam_Link5_DriverData_VoltageRail;
  UINT8   MipiCam_Link5_DriverData_PprValue;         // Deprecated from Revision 52
  UINT8   MipiCam_Link5_DriverData_PprUnit;         // Deprecated from Revision 52
  UINT8   MipiCam_Link5_FlashDriverSelection;

  UINT8   MipiCam_Flash0_Model;                                                    // LM3643 / Internal PMIC - WRC
  UINT16  MipiCam_Flash0_ModuleName[MIPICAM_MODULE_NAME_LENGTH];
  UINT8   MipiCam_Flash0_I2cChannel;
  UINT16  MipiCam_Flash0_I2cAddress;
  UINT8   MipiCam_Flash0_GpioGroupPadNumber;
  UINT16  MipiCam_Flash0_GpioGroupNumber;
  UINT8   MipiCam_Flash0_GpioActiveValue;
  UINT8   MipiCam_Flash0_GpioInitialValue;
  UINT8   MipiCam_Flash0_OperatingMode;

  UINT8   MipiCam_Flash1_Model;                                                    // LM3643 / Internal PMIC - WRC
  UINT16  MipiCam_Flash1_ModuleName[MIPICAM_MODULE_NAME_LENGTH];
  UINT8   MipiCam_Flash1_I2cChannel;
  UINT16  MipiCam_Flash1_I2cAddress;
  UINT8   MipiCam_Flash1_GpioGroupPadNumber;
  UINT16  MipiCam_Flash1_GpioGroupNumber;
  UINT8   MipiCam_Flash1_GpioActiveValue;
  UINT8   MipiCam_Flash1_GpioInitialValue;
  UINT8   MipiCam_Flash1_OperatingMode;

  UINT8   MipiCam_Flash2_Model;                                                    // LM3643 / Internal PMIC - WRC
  UINT16  MipiCam_Flash2_ModuleName[MIPICAM_MODULE_NAME_LENGTH];
  UINT8   MipiCam_Flash2_I2cChannel;
  UINT16  MipiCam_Flash2_I2cAddress;
  UINT8   MipiCam_Flash2_GpioGroupPadNumber;
  UINT16  MipiCam_Flash2_GpioGroupNumber;
  UINT8   MipiCam_Flash2_GpioActiveValue;
  UINT8   MipiCam_Flash2_GpioInitialValue;
  UINT8   MipiCam_Flash2_OperatingMode;

  UINT8   MipiCam_Flash3_Model;                                                    // LM3643 / Internal PMIC - WRC
  UINT16  MipiCam_Flash3_ModuleName[MIPICAM_MODULE_NAME_LENGTH];
  UINT8   MipiCam_Flash3_I2cChannel;
  UINT16  MipiCam_Flash3_I2cAddress;
  UINT8   MipiCam_Flash3_GpioGroupPadNumber;
  UINT16  MipiCam_Flash3_GpioGroupNumber;
  UINT8   MipiCam_Flash3_GpioActiveValue;
  UINT8   MipiCam_Flash3_GpioInitialValue;
  UINT8   MipiCam_Flash3_OperatingMode;

  UINT8   MipiCam_Flash4_Model;                                                    // LM3643 / Internal PMIC - WRC
  UINT16  MipiCam_Flash4_ModuleName[MIPICAM_MODULE_NAME_LENGTH];
  UINT8   MipiCam_Flash4_I2cChannel;
  UINT16  MipiCam_Flash4_I2cAddress;
  UINT8   MipiCam_Flash4_GpioGroupPadNumber;
  UINT16  MipiCam_Flash4_GpioGroupNumber;
  UINT8   MipiCam_Flash4_GpioActiveValue;
  UINT8   MipiCam_Flash4_GpioInitialValue;
  UINT8   MipiCam_Flash4_OperatingMode;

  UINT8   MipiCam_Flash5_Model;                                                    // LM3643 / Internal PMIC - WRC
  UINT16  MipiCam_Flash5_ModuleName[MIPICAM_MODULE_NAME_LENGTH];
  UINT8   MipiCam_Flash5_I2cChannel;
  UINT16  MipiCam_Flash5_I2cAddress;
  UINT8   MipiCam_Flash5_GpioGroupPadNumber;
  UINT16  MipiCam_Flash5_GpioGroupNumber;
  UINT8   MipiCam_Flash5_GpioActiveValue;
  UINT8   MipiCam_Flash5_GpioInitialValue;
  UINT8   MipiCam_Flash5_OperatingMode;

  UINT8   PchI2cSensorDevicePort[PCH_MAX_SERIALIO_I2C_CONTROLLERS];

  UINT8   PchI2cTouchPadType;
  UINT8   PchI2cTouchPadIrqMode;
  UINT8   PchI2cTouchPadBusAddress;
  UINT16  PchI2cTouchPadHidAddress;
  UINT8   PchI2cTouchPadSpeed;

  UINT8   PchI2cTouchPanelType;
  UINT8   PchI2cTouchPanelIrqMode;
  UINT8   PchI2cTouchPanelBusAddress;
  UINT16  PchI2cTouchPanelHidAddress;
  UINT8   PchI2cTouchPanelSpeed;

  UINT8   PchSpiSensorDevicePort[PCH_MAX_SERIALIO_SPI_CONTROLLERS];
  UINT8   PchSpiFingerPrintType;
  UINT8   PchSpiSensorIrqMode;

  UINT8   PchUart1SensorDevice;
  UINT8   PchUart2SensorDevice;
  UINT8   PchI2cWittDevice;
  UINT8   PchI2cWittVersion;
  UINT8   PchUartUtkDevice;
  UINT8   DiscreteBtModule;
  UINT8   DiscreteBtModuleIrqMode;

  UINT8   PchSerialIoUseTimingParameters;

  UINT16  PchSerialIoTimingSSH0;  // [SSH0] SSCN-HIGH for I2C0
  UINT16  PchSerialIoTimingSSL0;  // [SSL0] SSCN-LOW  for I2C0
  UINT16  PchSerialIoTimingSSD0;  // [SSD0] SSCN-HOLD for I2C0
  UINT16  PchSerialIoTimingFMH0;  // [FMH0] FMCN-HIGH for I2C0
  UINT16  PchSerialIoTimingFML0;  // [FML0] FMCN-LOW  for I2C0
  UINT16  PchSerialIoTimingFMD0;  // [FMD0] FMCN-HOLD for I2C0
  UINT16  PchSerialIoTimingFPH0;  // [FPH0] FPCN-HIGH for I2C0
  UINT16  PchSerialIoTimingFPL0;  // [FPL0] FPCN-LOW  for I2C0
  UINT16  PchSerialIoTimingFPD0;  // [FPD0] FPCN-HOLD for I2C0
  UINT16  PchSerialIoTimingHSH0;  // [HSH0] HSCN-HIGH for I2C0
  UINT16  PchSerialIoTimingHSL0;  // [HSL0] HSCN-LOW  for I2C0
  UINT16  PchSerialIoTimingHSD0;  // [HSD0] HSCN-HOLD for I2C0

  UINT16  PchSerialIoTimingSSH1;  // [SSH1] SSCN-HIGH for I2C1
  UINT16  PchSerialIoTimingSSL1;  // [SSL1] SSCN-LOW  for I2C1
  UINT16  PchSerialIoTimingSSD1;  // [SSD1] SSCN-HOLD for I2C1
  UINT16  PchSerialIoTimingFMH1;  // [FMH1] FMCN-HIGH for I2C1
  UINT16  PchSerialIoTimingFML1;  // [FML1] FMCN-LOW  for I2C1
  UINT16  PchSerialIoTimingFMD1;  // [FMD1] FMCN-HOLD for I2C1
  UINT16  PchSerialIoTimingFPH1;  // [FPH1] FPCN-HIGH for I2C1
  UINT16  PchSerialIoTimingFPL1;  // [FPL1] FPCN-LOW  for I2C1
  UINT16  PchSerialIoTimingFPD1;  // [FPD1] FPCN-HOLD for I2C1
  UINT16  PchSerialIoTimingHSH1;  // [HSH1] HSCN-HIGH for I2C1
  UINT16  PchSerialIoTimingHSL1;  // [HSL1] HSCN-LOW  for I2C1
  UINT16  PchSerialIoTimingHSD1;  // [HSD1] HSCN-HOLD for I2C1

  UINT16  PchSerialIoTimingSSH2;  // [SSH2] SSCN-HIGH for I2C2
  UINT16  PchSerialIoTimingSSL2;  // [SSL2] SSCN-LOW  for I2C2
  UINT16  PchSerialIoTimingSSD2;  // [SSD2] SSCN-HOLD for I2C2
  UINT16  PchSerialIoTimingFMH2;  // [FMH2] FMCN-HIGH for I2C2
  UINT16  PchSerialIoTimingFML2;  // [FML2] FMCN-LOW  for I2C2
  UINT16  PchSerialIoTimingFMD2;  // [FMD2] FMCN-HOLD for I2C2
  UINT16  PchSerialIoTimingFPH2;  // [FPH2] FPCN-HIGH for I2C2
  UINT16  PchSerialIoTimingFPL2;  // [FPL2] FPCN-LOW  for I2C2
  UINT16  PchSerialIoTimingFPD2;  // [FPD2] FPCN-HOLD for I2C2
  UINT16  PchSerialIoTimingHSH2;  // [HSH2] HSCN-HIGH for I2C2
  UINT16  PchSerialIoTimingHSL2;  // [HSL2] HSCN-LOW  for I2C2
  UINT16  PchSerialIoTimingHSD2;  // [HSD2] HSCN-HOLD for I2C2

  UINT16  PchSerialIoTimingSSH3;  // [SSH3] SSCN-HIGH for I2C3
  UINT16  PchSerialIoTimingSSL3;  // [SSL3] SSCN-LOW  for I2C3
  UINT16  PchSerialIoTimingSSD3;  // [SSD3] SSCN-HOLD for I2C3
  UINT16  PchSerialIoTimingFMH3;  // [FMH3] FMCN-HIGH for I2C3
  UINT16  PchSerialIoTimingFML3;  // [FML3] FMCN-LOW  for I2C3
  UINT16  PchSerialIoTimingFMD3;  // [FMD3] FMCN-HOLD for I2C3
  UINT16  PchSerialIoTimingFPH3;  // [FPH3] FPCN-HIGH for I2C3
  UINT16  PchSerialIoTimingFPL3;  // [FPL3] FPCN-LOW  for I2C3
  UINT16  PchSerialIoTimingFPD3;  // [FPD3] FPCN-HOLD for I2C3
  UINT16  PchSerialIoTimingHSH3;  // [HSH3] HSCN-HIGH for I2C3
  UINT16  PchSerialIoTimingHSL3;  // [HSL3] HSCN-LOW  for I2C3
  UINT16  PchSerialIoTimingHSD3;  // [HSD3] HSCN-HOLD for I2C3

  UINT16  PchSerialIoTimingSSH4;  // [SSH4] SSCN-HIGH for I2C4
  UINT16  PchSerialIoTimingSSL4;  // [SSL4] SSCN-LOW  for I2C4
  UINT16  PchSerialIoTimingSSD4;  // [SSD4] SSCN-HOLD for I2C4
  UINT16  PchSerialIoTimingFMH4;  // [FMH4] FMCN-HIGH for I2C4
  UINT16  PchSerialIoTimingFML4;  // [FML4] FMCN-LOW  for I2C4
  UINT16  PchSerialIoTimingFMD4;  // [FMD4] FMCN-HOLD for I2C4
  UINT16  PchSerialIoTimingFPH4;  // [FPH4] FPCN-HIGH for I2C4
  UINT16  PchSerialIoTimingFPL4;  // [FPL4] FPCN-LOW  for I2C4
  UINT16  PchSerialIoTimingFPD4;  // [FPD4] FPCN-HOLD for I2C4
  UINT16  PchSerialIoTimingHSH4;  // [HSH4] HSCN-HIGH for I2C4
  UINT16  PchSerialIoTimingHSL4;  // [HSL4] HSCN-LOW  for I2C4
  UINT16  PchSerialIoTimingHSD4;  // [HSD4] HSCN-HOLD for I2C4

  UINT16  PchSerialIoTimingSSH5;  // [SSH5] SSCN-HIGH for I2C5
  UINT16  PchSerialIoTimingSSL5;  // [SSL5] SSCN-LOW  for I2C5
  UINT16  PchSerialIoTimingSSD5;  // [SSD5] SSCN-HOLD for I2C5
  UINT16  PchSerialIoTimingFMH5;  // [FMH5] FMCN-HIGH for I2C5
  UINT16  PchSerialIoTimingFML5;  // [FML5] FMCN-LOW  for I2C5
  UINT16  PchSerialIoTimingFMD5;  // [FMD5] FMCN-HOLD for I2C5
  UINT16  PchSerialIoTimingFPH5;  // [FPH5] FPCN-HIGH for I2C5
  UINT16  PchSerialIoTimingFPL5;  // [FPL5] FPCN-LOW  for I2C5
  UINT16  PchSerialIoTimingFPD5;  // [FPD5] FPCN-HOLD for I2C5
  UINT16  PchSerialIoTimingHSH5;  // [HSH5] HSCN-HIGH for I2C5
  UINT16  PchSerialIoTimingHSL5;  // [HSL5] HSCN-LOW  for I2C5
  UINT16  PchSerialIoTimingHSD5;  // [HSD5] HSCN-HOLD for I2C5

  UINT16  PchSerialIoTimingM0C0;  // [M0C0] M0D3 for I2C0
  UINT16  PchSerialIoTimingM1C0;  // [M1C0] M1D3 for I2C0
  UINT16  PchSerialIoTimingM0C1;  // [M0C1] M0D3 for I2C1
  UINT16  PchSerialIoTimingM1C1;  // [M1C1] M1D3 for I2C1
  UINT16  PchSerialIoTimingM0C2;  // [M0C2] M0D3 for I2C2
  UINT16  PchSerialIoTimingM1C2;  // [M1C2] M1D3 for I2C2
  UINT16  PchSerialIoTimingM0C3;  // [M0C3] M0D3 for I2C3
  UINT16  PchSerialIoTimingM1C3;  // [M1C3] M1D3 for I2C3
  UINT16  PchSerialIoTimingM0C4;  // [M0C4] M0D3 for I2C4
  UINT16  PchSerialIoTimingM1C4;  // [M1C4] M1D3 for I2C4
  UINT16  PchSerialIoTimingM0C5;  // [M0C5] M0D3 for I2C5
  UINT16  PchSerialIoTimingM1C5;  // [M1C5] M1D3 for I2C5
  UINT16  PchSerialIoTimingM0C6;  // [M0C6] M0D3 for SPI0
  UINT16  PchSerialIoTimingM1C6;  // [M1C6] M1D3 for SPI0
  UINT16  PchSerialIoTimingM0C7;  // [M0C7] M0D3 for SPI1
  UINT16  PchSerialIoTimingM1C7;  // [M1C7] M1D3 for SPI1
  UINT16  PchSerialIoTimingM0C8;  // [M0C8] M0D3 for SPI2
  UINT16  PchSerialIoTimingM1C8;  // [M1C8] M1D3 for SPI2
  UINT16  PchSerialIoTimingM0C9;  // [M0C9] M0D3 for UART0
  UINT16  PchSerialIoTimingM1C9;  // [M1C9] M1D3 for UART0
  UINT16  PchSerialIoTimingM0CA;  // [M0CA] M0D3 for UART1
  UINT16  PchSerialIoTimingM1CA;  // [M1CA] M1D3 for UART1
  UINT16  PchSerialIoTimingM0CB;  // [M0CB] M0D3 for UART2
  UINT16  PchSerialIoTimingM1CB;  // [M1CB] M1D3 for UART2

  UINT8   Ps2KbMsEnable;
  UINT8   TouchPanelMuxSelector;

  UINT8   Ac1TripPoint;
  UINT8   Ac0TripPoint;
  UINT8   Ac1FanSpeed;
  UINT8   Ac0FanSpeed;
  UINT8   PassiveThermalTripPoint;
  UINT8   CriticalThermalTripPoint;
  UINT8   PassiveTc1Value;
  UINT8   PassiveTc2Value;
  UINT8   PassiveTspValue;
  UINT8   DisableActiveTripPoints;
  UINT8   DisablePassiveTripPoints;
  UINT8   DisableCriticalTripPoints;
  UINT8   PmicVccLevel;
  UINT8   PmicVddqLevel;
  UINT8   TwoComponents;
  UINT8   CrcPmicNvmUpdateEnable;
  UINT8   CrcPmicCustomizationNvmUpdateEnable;
  UINT8   PmicNvmWriteLockEnable;

  UINT8   CPUTempReadEnable;
  UINT8   CPUEnergyReadEnable;
  UINT8   PCHTempReadEnable;
  UINT8   AlertEnableLock;
  UINT8   PchAlert;
  UINT8   DimmAlert;
  UINT8   CpuTemp;
  UINT8   CpuFanSpeed;

  //
  // Intel(R) Dynamic Tuning Technology SETUP items begin
  //
  UINT8   EnableDptf;
  UINT8   EnableDCFG;

  UINT8   EnableSaDevice;
  UINT32  PpccStepSize;

  UINT8   DetectEcFan[MAX_EC_FANS];
  UINT8   EnableFan1Device;

  UINT8   EnableChargerParticipant;
  UINT8   EnablePowerParticipant;
  UINT16  PowerParticipantPollingRate;

  UINT8   EnableBatteryParticipant;
  UINT8   EnableInt3400Device;

  UINT8   EnableSen1ParticipantSupport; // @deprecated - No longer needed DetectEcSensors[0] is used instead
  UINT8   DetectEcSensors[MAX_EC_SENSORS];
  UINT8   EnableSen1Participant;
  UINT8   EnableSen2Participant;
  UINT8   EnableSen3Participant;
  UINT8   EnableSen4Participant;
  UINT8   EnableSen5Participant;

  UINT8   EnablePchFivrParticipant;

  UINT8   PpccObject;
  UINT8   ArtgObject;
  UINT8   PmaxObject;
  UINT8   Tmp1Object;
  UINT8   Tmp2Object;
  UINT8   Tmp3Object;
  UINT8   Tmp4Object;
  UINT8   Tmp5Object;
  UINT8   Tmp6Object;
  UINT8   Tmp7Object;
  UINT8   Tmp8Object;
  UINT8   OptionalObjects;

  UINT8   OemDesignVariable0;
  UINT8   OemDesignVariable1;
  UINT8   OemDesignVariable2;
  UINT8   OemDesignVariable3;
  UINT8   OemDesignVariable4;
  UINT8   OemDesignVariable5;
//[-start-210701-KEBIN00030-modify]//
#ifdef LCFC_SUPPORT
  UINT8   OemDesignVariable6;
  UINT8   OemDesignVariable7;
  UINT8   OemDesignVariable8;
  UINT8   OemDesignVariable9;
  UINT8   OemDesignVariable10;
  UINT8   OemDesignVariable11;
  UINT8   OemDesignVariable12;
  UINT8   OemDesignVariable13;
  UINT8   OemDesignVariable14;
  UINT8   OemDesignVariable15;
  UINT8   OemDesignVariable16;
  UINT8   OemDesignVariable17;
  UINT8   OemDesignVariable18;
  UINT8   OemDesignVariable19;
  UINT8   OemDesignVariable20;
//[-start-211202-QINGLIN0124-add]//
#if defined(S570_SUPPORT)
  UINT8   OemDesignVariable21;
#endif
//[-end-211202-QINGLIN0124-add]//
#endif
//[-end-210701-KEBIN00030-modify]//

  //
  // Intel(R) Dynamic Tuning Technology SETUP items end
  //
  UINT8   Rtd3Support;
  UINT8   Rtd3P0dl;
  UINT8   Rtd3UsbPt1;
  UINT8   Rtd3UsbPt2;
  UINT16  Rtd3AudioDelay;
  UINT16  Rtd3TouchPadDelay;
  UINT16  Rtd3TouchPanelDelay;
  UINT16  Rtd3SensorHub;
  UINT16  VRRampUpDelay;
  UINT8   PstateCapping;
  UINT8   Rtd3ZpoddSupport;
  UINT8   Rtd3Zpodd;
  UINT8   Rtd3WWAN;
  UINT8   Rtd3SataPort0;
  UINT8   Rtd3SataPort1;
  UINT8   Rtd3SataPort2;
  UINT8   Rtd3SataPort3;
  UINT8   Rtd3SataPort4;
  UINT8   Rtd3SataPort5;
  UINT8   Rtd3SataPort6;
  UINT8   Rtd3SataPort7;
  UINT8   Rtd3WaGpio;
  UINT8   Rtd3RemapCR1;
  UINT8   Rtd3RemapCR2;
  UINT8   Rtd3RemapCR3;
  UINT8   Rtd3I2CTouchPanel;

  UINT8   PowerSharingManagerEnable;
  UINT8   PsmSplcDomainType1;
  UINT32  PsmSplcPowerLimit1;
  UINT32  PsmSplcTimeWindow1;

  UINT8   PsmDplcDomainType1;
  UINT8   PsmDplcDomainPreference1;
  UINT16  PsmDplcPowerLimitIndex1;
  UINT16  PsmDplcDefaultPowerLimit1;
  UINT32  PsmDplcDefaultTimeWindow1;
  UINT16  PsmDplcMinimumPowerLimit1;
  UINT16  PsmDplcMaximumPowerLimit1;
  UINT16  PsmDplcMaximumTimeWindow1;

  UINT8   WifiEnable;
  UINT8   WifiDomainType1;
  UINT16  WifiPowerLimit1;
  UINT32  WifiTimeWindow1;
  UINT8   TRxDelay0;
  UINT8   TRxCableLength0;
  UINT8   TRxDelay1;
  UINT8   TRxCableLength1;
  UINT8   WrddDomainType1;
  UINT16  WrddCountryIndentifier1;
  UINT8   WrddDomainType2;
  UINT16  WrddCountryIndentifier2;
  UINT8   WrdsWiFiSarEnable;
  UINT8   WrdsWiFiSarTxPowerSet1Limit1;
  UINT8   WrdsWiFiSarTxPowerSet1Limit2;
  UINT8   WrdsWiFiSarTxPowerSet1Limit3;
  UINT8   WrdsWiFiSarTxPowerSet1Limit4;
  UINT8   WrdsWiFiSarTxPowerSet1Limit5;
  UINT8   WrdsWiFiSarTxPowerSet1Limit6;
  UINT8   WrdsWiFiSarTxPowerSet1Limit7;
  UINT8   WrdsWiFiSarTxPowerSet1Limit8;
  UINT8   WrdsWiFiSarTxPowerSet1Limit9;
  UINT8   WrdsWiFiSarTxPowerSet1Limit10;

  UINT8   EwrdWiFiDynamicSarEnable;
  UINT8   EwrdWiFiDynamicSarRangeSets;
  UINT8   EwrdWiFiSarTxPowerSet2Limit1;
  UINT8   EwrdWiFiSarTxPowerSet2Limit2;
  UINT8   EwrdWiFiSarTxPowerSet2Limit3;
  UINT8   EwrdWiFiSarTxPowerSet2Limit4;
  UINT8   EwrdWiFiSarTxPowerSet2Limit5;
  UINT8   EwrdWiFiSarTxPowerSet2Limit6;
  UINT8   EwrdWiFiSarTxPowerSet2Limit7;
  UINT8   EwrdWiFiSarTxPowerSet2Limit8;
  UINT8   EwrdWiFiSarTxPowerSet2Limit9;
  UINT8   EwrdWiFiSarTxPowerSet2Limit10;
  UINT8   EwrdWiFiSarTxPowerSet3Limit1;
  UINT8   EwrdWiFiSarTxPowerSet3Limit2;
  UINT8   EwrdWiFiSarTxPowerSet3Limit3;
  UINT8   EwrdWiFiSarTxPowerSet3Limit4;
  UINT8   EwrdWiFiSarTxPowerSet3Limit5;
  UINT8   EwrdWiFiSarTxPowerSet3Limit6;
  UINT8   EwrdWiFiSarTxPowerSet3Limit7;
  UINT8   EwrdWiFiSarTxPowerSet3Limit8;
  UINT8   EwrdWiFiSarTxPowerSet3Limit9;
  UINT8   EwrdWiFiSarTxPowerSet3Limit10;
  UINT8   EwrdWiFiSarTxPowerSet4Limit1;
  UINT8   EwrdWiFiSarTxPowerSet4Limit2;
  UINT8   EwrdWiFiSarTxPowerSet4Limit3;
  UINT8   EwrdWiFiSarTxPowerSet4Limit4;
  UINT8   EwrdWiFiSarTxPowerSet4Limit5;
  UINT8   EwrdWiFiSarTxPowerSet4Limit6;
  UINT8   EwrdWiFiSarTxPowerSet4Limit7;
  UINT8   EwrdWiFiSarTxPowerSet4Limit8;
  UINT8   EwrdWiFiSarTxPowerSet4Limit9;
  UINT8   EwrdWiFiSarTxPowerSet4Limit10;
  UINT8   WgdsWiFiSarDeltaGroup1PowerMax1;
  UINT8   WgdsWiFiSarDeltaGroup1PowerChainA1;
  UINT8   WgdsWiFiSarDeltaGroup1PowerChainB1;
  UINT8   WgdsWiFiSarDeltaGroup1PowerMax2;
  UINT8   WgdsWiFiSarDeltaGroup1PowerChainA2;
  UINT8   WgdsWiFiSarDeltaGroup1PowerChainB2;
  UINT8   WgdsWiFiSarDeltaGroup2PowerMax1;
  UINT8   WgdsWiFiSarDeltaGroup2PowerChainA1;
  UINT8   WgdsWiFiSarDeltaGroup2PowerChainB1;
  UINT8   WgdsWiFiSarDeltaGroup2PowerMax2;
  UINT8   WgdsWiFiSarDeltaGroup2PowerChainA2;
  UINT8   WgdsWiFiSarDeltaGroup2PowerChainB2;
  UINT8   WgdsWiFiSarDeltaGroup3PowerMax1;
  UINT8   WgdsWiFiSarDeltaGroup3PowerChainA1;
  UINT8   WgdsWiFiSarDeltaGroup3PowerChainB1;
  UINT8   WgdsWiFiSarDeltaGroup3PowerMax2;
  UINT8   WgdsWiFiSarDeltaGroup3PowerChainA2;
  UINT8   WgdsWiFiSarDeltaGroup3PowerChainB2;
  UINT8   WiFiDynamicSarAntennaACurrentSet;
  UINT8   WiFiDynamicSarAntennaBCurrentSet;

  UINT8   BluetoothSar;
  UINT8   BluetoothSarBr;
  UINT8   BluetoothSarEdr2;
  UINT8   BluetoothSarEdr3;
  UINT8   BluetoothSarLe;
  UINT8   BluetoothSarLe2Mhz;
  UINT8   BluetoothSarLeLr;

  UINT8   AntennaDiversity;
  UINT8   CoExistenceManager;
  UINT8   PrebootBleEnable;
  UINT8   UsbFnEnable;

  UINT8   FanControl;
  UINT8   CpuFanControl;
  UINT8   LowestFanSpeed;

  //
  // Thunderbolt(TM)
  //
  UINT8   TbtSetupFormSupport;
  UINT8   TbtSetupDTbtPegTypeSupport;
  UINT8   DiscreteTbtSupport;
  UINT8   IntegratedTbtSupport;
  UINT8   TbtPcieSupport;
  UINT8   TbtUsbOn; // Deprecated
  UINT8   TbtBootOn;
  UINT8   TbtWakeupSupport;
  UINT8   Gpio3ForcePwr;
  UINT16  Gpio3ForcePwrDly; // Deprecated
  UINT8   CallSmiBeforeBoot;
  UINT8   Gpio5Filter;
  UINT8   TBTHotNotify;
  UINT8   EnableMsiInFadt;
  UINT8   DTbtController[DTBT_CONTROLLER_SUPPORTED];
  UINT8   DTbtControllerType[DTBT_CONTROLLER_SUPPORTED];
  UINT8   TBTSetClkReq;
  UINT8   TbtAspm;
  UINT8   TbtLtr;
  UINT8   DTbthostRouterPortNumber[DTBT_CONTROLLER_SUPPORTED];
  UINT8   DTbtPcieExtraBusRsvd[DTBT_CONTROLLER_SUPPORTED];
  UINT16  DTbtPcieMemRsvd[DTBT_CONTROLLER_SUPPORTED];
  UINT8   DTbtPcieIoRsvd[DTBT_CONTROLLER_SUPPORTED];
  UINT8   DTbtPcieMemAddrRngMax[DTBT_CONTROLLER_SUPPORTED];
  UINT16  DTbtPciePMemRsvd[DTBT_CONTROLLER_SUPPORTED];
  UINT8   DTbtPciePMemAddrRngMax[DTBT_CONTROLLER_SUPPORTED];
  UINT8   Win10Support;
  UINT8   TrOsup; // Deprecated from Revision 12
  UINT8   TbtL1SubStates;
  UINT8   TbtPtm;
  UINT8   OsNativeResourceBalance;
  UINT8   TbtAcDcSwitch; // Deprecated from Revision 12
  UINT8   TbtLegacyModeSupport;
  // ITBT Specific Setup Options
  UINT8   ITbtPcieRootPortSupported[ITBT_ROOT_PORTS_SUPPORTED];
  UINT8   ITbtRootPort[ITBT_ROOT_PORTS_SUPPORTED];
  UINT8   ITbtPcieExtraBusRsvd[ITBT_ROOT_PORTS_SUPPORTED];
  UINT16  ITbtPcieMemRsvd[ITBT_ROOT_PORTS_SUPPORTED];
  UINT8   ITbtPcieMemAddrRngMax[ITBT_ROOT_PORTS_SUPPORTED];
  UINT16  ITbtPciePMemRsvd[ITBT_ROOT_PORTS_SUPPORTED];
  UINT8   ITbtPciePMemAddrRngMax[ITBT_ROOT_PORTS_SUPPORTED];
  UINT16  ITbtForcePowerOnTimeoutInMs;
  UINT16  ITbtConnectTopologyTimeoutInMs;
  UINT16  ReserveMemoryPerSlot;
  UINT16  ReservePMemoryPerSlot;
  UINT8   ReserveIoPerSlot;
  UINT8   ITbtPcieTunnelingForUsb4;  // Deprecated from Revision 31
  UINT8   TbtSkipPciOprom; // Deprecated from Revision 17
  UINT8   TbtAcpiRemovalSupport;

  UINT8   OcWdtEnabled;
  UINT8   IccLockRegisters;
  UINT8   IccProfile;
  UINT8   ShowProfile;
  UINT8   NumProfiles;

  UINT8   CsModeFirst;
  UINT8   CsMode;
  UINT8   CsModeChanged;

  UINT8   PlatformUnstable;
  UINT8   IsOcDefaultsInitalized;

  UINT8   SecureEraseModeRealMode;
  UINT8   ForceSecureErase;

  UINT8   HidEventFilterDriverEnable;


  UINT8   PmicSlpS0VmSupport;
  UINT8   HebcValueSupport;
  UINT32  HebcValue;

  UINT8   I2cSarResetDelay;

  UINT8   WwanEnable;

  UINT8   UsbcBiosTcssHandshake;
  // EC - BIOS handshake timeouts
  UINT32  UsbConnStatusTimeout;
  UINT32  UsbcSxEntryTimeout;
  UINT32  UsbcSxExitTimeout;


  UINT8   VtioSupport;
  UINT8   SdevCio2Entry;
  UINT8   SdevIspEntry;
  UINT8   SdevHeciEntry;
  UINT8   SdevSpi1Entry;
  UINT8   SdevSpi2Entry;
  UINT8   SdevXhciEntry;
  UINT16  SdevNumberOfSensors[5];
  UINT16  SdevSensorEntry1[5];
  UINT16  SdevSensorEntry2[5];

  UINT8   EcChargingMethod;
  UINT8   AlternateModeSynchDelay;
  UINT8   WakeOnWiGigSupport;

  UINT8   SystemTimeAndAlarmSource; // Select source for system time and alarm

  UINT8   TrustedDeviceSetupBoot;
  UINT8   TestUsbUnlockForNoa;

  UINT8   EnableCrashLog;
  UINT8   CrashLogOnAllReset;
  UINT8   CrashLogClearEnable;
  UINT8   CrashLogRearmEnable;

  UINT8   SysFwUpdateLoadDefault;
  UINT8   SysFwUpdateSkipPowerCheck;

  UINT8   BoardReworkComplainceEnable;
  UINT8   ItpxdpMuxSupport;
  UINT8   ItpxdpMux;

//[-start-180824-IB0672PO-add]//
  UINT8   SerialPortAcpiDebug;
//[-end-180824-IB0672PO-add]//

  UINT8   OsDnx;

  UINT8   PepPeg0;
  // Option to slect UCMC/UCSI device
  UINT8   TcssUcmDevice;
  UINT8   PepPcieStorage;
  UINT8   TbtVtdBaseSecurity; // Deprecated from Revision 25
  UINT8   KernelDebugPatch;
  UINT8   ITbtRtd3;
  UINT16  ITbtRtd3ExitDelay;
  UINT8   AuxOriOverrideSupport;
  UINT8   AuxOriOverride;
  UINT8   TccMode;

  UINT8   BclkSource;
  UINT8   PowermeterDeviceEnable; // powermeter enabling
  UINT8   WwanResetWorkaround;
  UINT8   PuisEnable;
  UINT8   SdevXhciNumberOfDevices;
  UINT8   SdevXhciDeviceAttributes[2];
  UINT8   SdevXhciRootPortNumber[2];
  UINT16  SdevXhciVendorId[2];
  UINT16  SdevXhciProductId[2];
  UINT16  SdevXhciRevision[2];
  UINT8   SdevXhciInterfaceNumber[2];
  UINT8   SdevXhciClass[2];
  UINT8   SdevXhciSubClass[2];
  UINT8   SdevXhciProtocol[2];
  UINT16  SdevXhciAcpiPathStringOffset[2];
  UINT16  SdevXhciAcpiPathStringLength[2];
  UINT64  SdevXhciFirmwareHashDevice1[4];
  UINT64  SdevXhciFirmwareHashDevice2[4];
  UINT16  SdevXhciAcpiPathNameDevice1[49]; // 48 characters + NULL termination
  UINT16  SdevXhciAcpiPathNameDevice2[49]; // 48 characters + NULL termination

  UINT8   CurrentSecurityMode;// Deprecated

  UINT8   ControlIommu;

  UINT8   SdevFlags[5];
  UINT8   SdevXhciFlags;
  UINT8   DevicePasswordSupport;
  UINT8   PepVmd;
  UINT8   DTbtRtd3;
  UINT16  DTbtRtd3OffDelay;
  UINT32  DTbtRtd3ClkReq;
  UINT16  DTbtRtd3ClkReqDelay;
  UINT8   DTbtGo2SxCommand;
  UINT8   EnablePcieTunnelingOverUsb4;
  UINT8   CnvExtClock;

  UINT8   WifiAntGainEnale;
  UINT8   WifiAntGain2400ChainA;
  UINT8   WifiAntGain5150ChainA;
  UINT8   WifiAntGain5350ChainA;
  UINT8   WifiAntGain5470ChainA;
  UINT8   WifiAntGain5725ChainA;
  UINT8   WifiAntGain2400ChainB;
  UINT8   WifiAntGain5150ChainB;
  UINT8   WifiAntGain5350ChainB;
  UINT8   WifiAntGain5470ChainB;
  UINT8   WifiAntGain5725ChainB;

  UINT8   WifiSarGeoMappingMode;  // Deprecated
  UINT8   WifiSarGeoDefault;      // Deprecated
  UINT8   WifiSarGeoFcc;          // Deprecated
  UINT8   WifiSarGeoTwalike;      // Deprecated
  UINT8   WifiSarGeoCanadaIsed;   // Deprecated
  UINT8   WifiSarGeoEtsi5G8Srd;   // Deprecated
  UINT8   WifiSarGeoEtsi5G8Med;   // Deprecated
  UINT8   WifiSarGeoJapan;        // Deprecated
  UINT8   WifiSarGeoBrazil;       // Deprecated
  UINT8   WifiSarGeoEtsi5G8Fcc;   // Deprecated
  UINT8   WifiSarGeoIndonesia;    // Deprecated
  UINT8   WifiSarGeoSouthKorea;   // Deprecated
  UINT8   WifiSarGeoChile;        // Deprecated
  UINT8   WifiSarGeoEtsi5G8Pass;  // Deprecated
  UINT8   WifiSarGeoPakistan;     // Deprecated
  UINT8   WifiSarGeoEgypt;        // Deprecated
  UINT8   WifiSarGeoTunisia;      // Deprecated
  UINT8   WifiSarGeoChinaBios;    // Deprecated
  UINT8   WifiSarGeoRussia;       // Deprecated
  UINT8   WifiSarGeoEuEtsi5G8Srd; // Deprecated
  UINT8   WifiSarGeoUsaOnly;      // Deprecated
  UINT8   WifiSarGeoEuEtsi5G8Dfs; // Deprecated
  UINT8   WifiSarGeoQatar;        // Deprecated

  UINT8   MipiCam_Flash0_Mode;
  UINT8   MipiCam_Flash1_Mode;
  UINT8   MipiCam_Flash2_Mode;
  UINT8   MipiCam_Flash3_Mode;
  UINT8   MipiCam_Flash4_Mode;
  UINT8   MipiCam_Flash5_Mode;
  UINT8   TypecRetimerTxComplianceModeEn;
  UINT8   TypecRetimerPD0;
  UINT8   TypecRetimerPD1;
  UINT8   TypecRetimerPD2;
  UINT8   TypecRetimerPD3;
  UINT8   WifiActiveChannelSrd;
  UINT8   WifiIndonesia5GhzSupport;
  UINT8   CvfSupport;
  UINT8   DeepestUSBSleepWakeCapability;

  UINT8   PepSataEnumeration;
  UINT8   PepHeci3;


  UINT32  WifiUltraHighBandSupport;
  UINT8   WrdsWiFiSarTxPowerSet1Limit11;
  UINT8   WrdsWiFiSarTxPowerSet1Limit12;
  UINT8   WrdsWiFiSarTxPowerSet1Limit13;
  UINT8   WrdsWiFiSarTxPowerSet1Limit14;
  UINT8   WrdsWiFiSarTxPowerSet1Limit15;
  UINT8   WrdsWiFiSarTxPowerSet1Limit16;
  UINT8   WrdsWiFiSarTxPowerSet1Limit17;
  UINT8   WrdsWiFiSarTxPowerSet1Limit18;
  UINT8   WrdsWiFiSarTxPowerSet1Limit19;
  UINT8   WrdsWiFiSarTxPowerSet1Limit20;
  UINT8   WrdsWiFiSarTxPowerSet1Limit21;
  UINT8   WrdsWiFiSarTxPowerSet1Limit22;
  UINT8   EwrdWiFiSarTxPowerSet2Limit11;
  UINT8   EwrdWiFiSarTxPowerSet2Limit12;
  UINT8   EwrdWiFiSarTxPowerSet2Limit13;
  UINT8   EwrdWiFiSarTxPowerSet2Limit14;
  UINT8   EwrdWiFiSarTxPowerSet2Limit15;
  UINT8   EwrdWiFiSarTxPowerSet2Limit16;
  UINT8   EwrdWiFiSarTxPowerSet2Limit17;
  UINT8   EwrdWiFiSarTxPowerSet2Limit18;
  UINT8   EwrdWiFiSarTxPowerSet2Limit19;
  UINT8   EwrdWiFiSarTxPowerSet2Limit20;
  UINT8   EwrdWiFiSarTxPowerSet2Limit21;
  UINT8   EwrdWiFiSarTxPowerSet2Limit22;
  UINT8   EwrdWiFiSarTxPowerSet3Limit11;
  UINT8   EwrdWiFiSarTxPowerSet3Limit12;
  UINT8   EwrdWiFiSarTxPowerSet3Limit13;
  UINT8   EwrdWiFiSarTxPowerSet3Limit14;
  UINT8   EwrdWiFiSarTxPowerSet3Limit15;
  UINT8   EwrdWiFiSarTxPowerSet3Limit16;
  UINT8   EwrdWiFiSarTxPowerSet3Limit17;
  UINT8   EwrdWiFiSarTxPowerSet3Limit18;
  UINT8   EwrdWiFiSarTxPowerSet3Limit19;
  UINT8   EwrdWiFiSarTxPowerSet3Limit20;
  UINT8   EwrdWiFiSarTxPowerSet3Limit21;
  UINT8   EwrdWiFiSarTxPowerSet3Limit22;
  UINT8   EwrdWiFiSarTxPowerSet4Limit11;
  UINT8   EwrdWiFiSarTxPowerSet4Limit12;
  UINT8   EwrdWiFiSarTxPowerSet4Limit13;
  UINT8   EwrdWiFiSarTxPowerSet4Limit14;
  UINT8   EwrdWiFiSarTxPowerSet4Limit15;
  UINT8   EwrdWiFiSarTxPowerSet4Limit16;
  UINT8   EwrdWiFiSarTxPowerSet4Limit17;
  UINT8   EwrdWiFiSarTxPowerSet4Limit18;
  UINT8   EwrdWiFiSarTxPowerSet4Limit19;
  UINT8   EwrdWiFiSarTxPowerSet4Limit20;
  UINT8   EwrdWiFiSarTxPowerSet4Limit21;
  UINT8   EwrdWiFiSarTxPowerSet4Limit22;

  UINT8   WifiAntGain5945ChainA;
  UINT8   WifiAntGain6165ChainA;
  UINT8   WifiAntGain6405ChainA;
  UINT8   WifiAntGain6525ChainA;
  UINT8   WifiAntGain6705ChainA;
  UINT8   WifiAntGain6865ChainA;
  UINT8   WifiAntGain5945ChainB;
  UINT8   WifiAntGain6165ChainB;
  UINT8   WifiAntGain6405ChainB;
  UINT8   WifiAntGain6525ChainB;
  UINT8   WifiAntGain6705ChainB;
  UINT8   WifiAntGain6865ChainB;
  UINT8   AxSettingUkraine;
  UINT8   AxModeUkraine;
  UINT8   AxSettingRussia;
  UINT8   AxModeRussia;

  UINT8   WgdsWiFiSarDeltaGroup1PowerMax3;
  UINT8   WgdsWiFiSarDeltaGroup1PowerChainA3;
  UINT8   WgdsWiFiSarDeltaGroup1PowerChainB3;
  UINT8   WgdsWiFiSarDeltaGroup2PowerMax3;
  UINT8   WgdsWiFiSarDeltaGroup2PowerChainA3;
  UINT8   WgdsWiFiSarDeltaGroup2PowerChainB3;
  UINT8   WgdsWiFiSarDeltaGroup3PowerMax3;
  UINT8   WgdsWiFiSarDeltaGroup3PowerChainA3;
  UINT8   WgdsWiFiSarDeltaGroup3PowerChainB3;

  UINT8   WrdsCdbWiFiSarTxPowerSet1Limit1;
  UINT8   WrdsCdbWiFiSarTxPowerSet1Limit2;
  UINT8   WrdsCdbWiFiSarTxPowerSet1Limit3;
  UINT8   WrdsCdbWiFiSarTxPowerSet1Limit4;
  UINT8   WrdsCdbWiFiSarTxPowerSet1Limit5;
  UINT8   WrdsCdbWiFiSarTxPowerSet1Limit6;
  UINT8   WrdsCdbWiFiSarTxPowerSet1Limit7;
  UINT8   WrdsCdbWiFiSarTxPowerSet1Limit8;
  UINT8   WrdsCdbWiFiSarTxPowerSet1Limit9;
  UINT8   WrdsCdbWiFiSarTxPowerSet1Limit10;
  UINT8   WrdsCdbWiFiSarTxPowerSet1Limit11;
  UINT8   WrdsCdbWiFiSarTxPowerSet1Limit12;
  UINT8   WrdsCdbWiFiSarTxPowerSet1Limit13;
  UINT8   WrdsCdbWiFiSarTxPowerSet1Limit14;
  UINT8   WrdsCdbWiFiSarTxPowerSet1Limit15;
  UINT8   WrdsCdbWiFiSarTxPowerSet1Limit16;
  UINT8   WrdsCdbWiFiSarTxPowerSet1Limit17;
  UINT8   WrdsCdbWiFiSarTxPowerSet1Limit18;
  UINT8   WrdsCdbWiFiSarTxPowerSet1Limit19;
  UINT8   WrdsCdbWiFiSarTxPowerSet1Limit20;
  UINT8   WrdsCdbWiFiSarTxPowerSet1Limit21;
  UINT8   WrdsCdbWiFiSarTxPowerSet1Limit22;

  UINT8   EwrdCdbWiFiSarTxPowerSet2Limit1;
  UINT8   EwrdCdbWiFiSarTxPowerSet2Limit2;
  UINT8   EwrdCdbWiFiSarTxPowerSet2Limit3;
  UINT8   EwrdCdbWiFiSarTxPowerSet2Limit4;
  UINT8   EwrdCdbWiFiSarTxPowerSet2Limit5;
  UINT8   EwrdCdbWiFiSarTxPowerSet2Limit6;
  UINT8   EwrdCdbWiFiSarTxPowerSet2Limit7;
  UINT8   EwrdCdbWiFiSarTxPowerSet2Limit8;
  UINT8   EwrdCdbWiFiSarTxPowerSet2Limit9;
  UINT8   EwrdCdbWiFiSarTxPowerSet2Limit10;
  UINT8   EwrdCdbWiFiSarTxPowerSet2Limit11;
  UINT8   EwrdCdbWiFiSarTxPowerSet2Limit12;
  UINT8   EwrdCdbWiFiSarTxPowerSet2Limit13;
  UINT8   EwrdCdbWiFiSarTxPowerSet2Limit14;
  UINT8   EwrdCdbWiFiSarTxPowerSet2Limit15;
  UINT8   EwrdCdbWiFiSarTxPowerSet2Limit16;
  UINT8   EwrdCdbWiFiSarTxPowerSet2Limit17;
  UINT8   EwrdCdbWiFiSarTxPowerSet2Limit18;
  UINT8   EwrdCdbWiFiSarTxPowerSet2Limit19;
  UINT8   EwrdCdbWiFiSarTxPowerSet2Limit20;
  UINT8   EwrdCdbWiFiSarTxPowerSet2Limit21;
  UINT8   EwrdCdbWiFiSarTxPowerSet2Limit22;

  UINT8   EwrdCdbWiFiSarTxPowerSet3Limit1;
  UINT8   EwrdCdbWiFiSarTxPowerSet3Limit2;
  UINT8   EwrdCdbWiFiSarTxPowerSet3Limit3;
  UINT8   EwrdCdbWiFiSarTxPowerSet3Limit4;
  UINT8   EwrdCdbWiFiSarTxPowerSet3Limit5;
  UINT8   EwrdCdbWiFiSarTxPowerSet3Limit6;
  UINT8   EwrdCdbWiFiSarTxPowerSet3Limit7;
  UINT8   EwrdCdbWiFiSarTxPowerSet3Limit8;
  UINT8   EwrdCdbWiFiSarTxPowerSet3Limit9;
  UINT8   EwrdCdbWiFiSarTxPowerSet3Limit10;
  UINT8   EwrdCdbWiFiSarTxPowerSet3Limit11;
  UINT8   EwrdCdbWiFiSarTxPowerSet3Limit12;
  UINT8   EwrdCdbWiFiSarTxPowerSet3Limit13;
  UINT8   EwrdCdbWiFiSarTxPowerSet3Limit14;
  UINT8   EwrdCdbWiFiSarTxPowerSet3Limit15;
  UINT8   EwrdCdbWiFiSarTxPowerSet3Limit16;
  UINT8   EwrdCdbWiFiSarTxPowerSet3Limit17;
  UINT8   EwrdCdbWiFiSarTxPowerSet3Limit18;
  UINT8   EwrdCdbWiFiSarTxPowerSet3Limit19;
  UINT8   EwrdCdbWiFiSarTxPowerSet3Limit20;
  UINT8   EwrdCdbWiFiSarTxPowerSet3Limit21;
  UINT8   EwrdCdbWiFiSarTxPowerSet3Limit22;

  UINT8   EwrdCdbWiFiSarTxPowerSet4Limit1;
  UINT8   EwrdCdbWiFiSarTxPowerSet4Limit2;
  UINT8   EwrdCdbWiFiSarTxPowerSet4Limit3;
  UINT8   EwrdCdbWiFiSarTxPowerSet4Limit4;
  UINT8   EwrdCdbWiFiSarTxPowerSet4Limit5;
  UINT8   EwrdCdbWiFiSarTxPowerSet4Limit6;
  UINT8   EwrdCdbWiFiSarTxPowerSet4Limit7;
  UINT8   EwrdCdbWiFiSarTxPowerSet4Limit8;
  UINT8   EwrdCdbWiFiSarTxPowerSet4Limit9;
  UINT8   EwrdCdbWiFiSarTxPowerSet4Limit10;
  UINT8   EwrdCdbWiFiSarTxPowerSet4Limit11;
  UINT8   EwrdCdbWiFiSarTxPowerSet4Limit12;
  UINT8   EwrdCdbWiFiSarTxPowerSet4Limit13;
  UINT8   EwrdCdbWiFiSarTxPowerSet4Limit14;
  UINT8   EwrdCdbWiFiSarTxPowerSet4Limit15;
  UINT8   EwrdCdbWiFiSarTxPowerSet4Limit16;
  UINT8   EwrdCdbWiFiSarTxPowerSet4Limit17;
  UINT8   EwrdCdbWiFiSarTxPowerSet4Limit18;
  UINT8   EwrdCdbWiFiSarTxPowerSet4Limit19;
  UINT8   EwrdCdbWiFiSarTxPowerSet4Limit20;
  UINT8   EwrdCdbWiFiSarTxPowerSet4Limit21;
  UINT8   EwrdCdbWiFiSarTxPowerSet4Limit22;

  UINT32  WifiPhyFilterConfigA;    // Deprecated
  UINT32  WifiPhyFilterConfigB;    // Deprecated
  UINT32  WifiPhyFilterConfigC;    // Deprecated
  UINT32  WifiPhyFilterConfigD;    // Deprecated

  UINT8   MipiCam_Link0_LanesClkDiv;
  UINT8   MipiCam_Link1_LanesClkDiv;
  UINT8   MipiCam_Link2_LanesClkDiv;
  UINT8   MipiCam_Link3_LanesClkDiv;
  UINT8   MipiCam_Link4_LanesClkDiv;
  UINT8   MipiCam_Link5_LanesClkDiv;
  UINT8   EnableFan2Device;
  UINT8   EnableFan3Device;
  UINT8   EnableDgpuParticipant;
  UINT8   AdlSAepBoard;

  UINT8   WifiTASSelection;
  UINT8   WifiTASListEntries;
  UINT8   PlnEnable;

  // Option to enable/disable PD PS_ON feature
  UINT8   TcssPdPsOnEnable;

  UINT8   PepPcieLan;
  UINT8   PepPcieWlan;
  UINT8   PepPcieGfx;
  UINT8   PepPcieOther;
  UINT8   AuxRailBudget;
  UINT8   PepPcieDg;
  UINT8   DgPlatformSupport;
  UINT16  WTASBlockedList[16];
  UINT8   DTbtRtd3OffDelayOptEn;
  // Disable TBT PCIE RP and child device Tree BME. It was earlier known as TbtVtdBaseSecurity
  UINT8   DisableTbtPcieTreeBme;

  UINT8   BtTileMode;
  UINT8   TileS0;
  UINT8   TileS0ix;
  UINT8   TileS4;
  UINT8   TileS5;
  UINT8   SpecialLedConfig;
  UINT16  LedDuration;
  UINT8   AirplaneMode;
  UINT8   StorageRtd3Support;
  UINT8   PmaxDevice;
  UINT8   PmaxAudioCodec;
  UINT8   PmaxWfCamera;
  UINT8   PmaxUfCamera;
  UINT8   PmaxFlashDevice;
  UINT8   SecondConfigReworkOption; // Deprecated in revision 36
  UINT8   MPdtSupport;
// Discrete TBT (DTBT) Setup Page Support.
  UINT8   DiscreteTbtPlatformConfigurationSupport;
  UINT8   WwanFwFlashDevice;
  UINT8   DtbtCmSupport;
  UINT8   Usb4CmMode;
  //
  // dTBT Dp-In Setup Option
  //
  UINT8   DpInExternalEn;
  UINT8   PepUfs0;
  UINT8   PepUfs1;
  UINT8   ClosedLidWovLightingSupport;
  //
  // TCSS CPU XHCI Enable Convert of Type-C to Type-A Port
  //
  UINT8   TestEnableTcssCovTypeA[MAX_TCSS_USB3_PORTS]; // Deprecated from Revision 38.
  //
  // TCSS Port Disable UCSI/UCMX Driver Support. 0: Platform POR (For EclessPd it is UCMX, For For EcPd it is USCI), 2: Force Disable the Driver support.
  //
  UINT8   TestDisableTcssPortUcxxSupport[10]; // MAX 10 Type-C Port. Reserve Extra also.
  UINT16  CpuBclkFreq;
  UINT8   PegDmiClkFreq;
  UINT8   DccClkCtrl;
  UINT8   UsbcDataRoleSwapPlatformDisable; // Platform Setup option to disable USBC DataRole Swap
  //
  // Dual Maple Ridge Platfrom.
  //
  UINT8   TwoMapleRidgeBoard;//Deprecated
  //
  // Flash ID support for Link 0-5
  //
  UINT8   MipiCam_Link0_DriverData_FlashID;
  UINT8   MipiCam_Link1_DriverData_FlashID;
  UINT8   MipiCam_Link2_DriverData_FlashID;
  UINT8   MipiCam_Link3_DriverData_FlashID;
  UINT8   MipiCam_Link4_DriverData_FlashID;
  UINT8   MipiCam_Link5_DriverData_FlashID;

  UINT16  PchSerialIoTimingSSH6;  // [SSH6] SSCN-HIGH for I2C6
  UINT16  PchSerialIoTimingSSL6;  // [SSL6] SSCN-LOW  for I2C6
  UINT16  PchSerialIoTimingSSD6;  // [SSD6] SSCN-HOLD for I2C6
  UINT16  PchSerialIoTimingFMH6;  // [FMH6] FMCN-HIGH for I2C6
  UINT16  PchSerialIoTimingFML6;  // [FML6] FMCN-LOW  for I2C6
  UINT16  PchSerialIoTimingFMD6;  // [FMD6] FMCN-HOLD for I2C6
  UINT16  PchSerialIoTimingFPH6;  // [FPH6] FPCN-HIGH for I2C6
  UINT16  PchSerialIoTimingFPL6;  // [FPL6] FPCN-LOW  for I2C6
  UINT16  PchSerialIoTimingFPD6;  // [FPD6] FPCN-HOLD for I2C6
  UINT16  PchSerialIoTimingHSH6;  // [HSH6] HSCN-HIGH for I2C6
  UINT16  PchSerialIoTimingHSL6;  // [HSL6] HSCN-LOW  for I2C6
  UINT16  PchSerialIoTimingHSD6;  // [HSD6] HSCN-HOLD for I2C6
  UINT16  PchSerialIoTimingM0CC;  // [M0CC] M0D3 for I2C6
  UINT16  PchSerialIoTimingM1CC;  // [M1CC] M1D3 for I2C6
  UINT8   PepI2c6;

  UINT16  PchSerialIoTimingSSH7;  // [SSH7] SSCN-HIGH for I2C7
  UINT16  PchSerialIoTimingSSL7;  // [SSL7] SSCN-LOW  for I2C7
  UINT16  PchSerialIoTimingSSD7;  // [SSD7] SSCN-HOLD for I2C7
  UINT16  PchSerialIoTimingFMH7;  // [FMH7] FMCN-HIGH for I2C7
  UINT16  PchSerialIoTimingFML7;  // [FML7] FMCN-LOW  for I2C7
  UINT16  PchSerialIoTimingFMD7;  // [FMD7] FMCN-HOLD for I2C7
  UINT16  PchSerialIoTimingFPH7;  // [FPH7] FPCN-HIGH for I2C7
  UINT16  PchSerialIoTimingFPL7;  // [FPL7] FPCN-LOW  for I2C7
  UINT16  PchSerialIoTimingFPD7;  // [FPD7] FPCN-HOLD for I2C7
  UINT16  PchSerialIoTimingHSH7;  // [HSH7] HSCN-HIGH for I2C7
  UINT16  PchSerialIoTimingHSL7;  // [HSL7] HSCN-LOW  for I2C7
  UINT16  PchSerialIoTimingHSD7;  // [HSD7] HSCN-HOLD for I2C7
  UINT16  PchSerialIoTimingM0CD;  // [M0CD] M0D3 for I2C7
  UINT16  PchSerialIoTimingM1CD;  // [M1CD] M1D3 for I2C7
  UINT8   PepI2c7;
  UINT8   DTbtDpInSupport;
  UINT16  WwanOemSvid;
  UINT16  WwanSvidTimeout;
  UINT8   BtLedConfig;
  UINT8   BtLedPulseDuration;
  UINT8   BtLedPulseInterval;
  UINT32  WifiRegulatoryConfigurations;
  UINT32  WifiUartConfigurations;
  UINT32  WifiUnii4;
  UINT32  WifiIndoorControl;
  UINT8   PepEmmc;
  UINT8   SdCardD3ColdDisable;  //Deprecated SdCardD3ColdDisable
  UINT8   SDCardD3ColdSupport; // Deprecated SDCardD3ColdSupport
  UINT8   WirelessCnvConfigDevice;
  UINT16  CurrentCpuBclkFreq;
  UINT16  CurrentPegDmiClkFreq;
  UINT8   WirelessLowBandIsolation;  // WLAN/WWAN Low Band Isolation
  UINT8   WirelessHighBandIsolation; // WLAN/WWAN High Band Isolation
  UINT8   WgdsWiFiSarDeltaGroupNumber;
  UINT8   WgdsWiFiSarDeltaGroup4PowerMax1;
  UINT8   WgdsWiFiSarDeltaGroup4PowerChainA1;
  UINT8   WgdsWiFiSarDeltaGroup4PowerChainB1;
  UINT8   WgdsWiFiSarDeltaGroup4PowerMax2;
  UINT8   WgdsWiFiSarDeltaGroup4PowerChainA2;
  UINT8   WgdsWiFiSarDeltaGroup4PowerChainB2;
  UINT8   WgdsWiFiSarDeltaGroup4PowerMax3;
  UINT8   WgdsWiFiSarDeltaGroup4PowerChainA3;
  UINT8   WgdsWiFiSarDeltaGroup4PowerChainB3;
  UINT8   WgdsWiFiSarDeltaGroup5PowerMax1;
  UINT8   WgdsWiFiSarDeltaGroup5PowerChainA1;
  UINT8   WgdsWiFiSarDeltaGroup5PowerChainB1;
  UINT8   WgdsWiFiSarDeltaGroup5PowerMax2;
  UINT8   WgdsWiFiSarDeltaGroup5PowerChainA2;
  UINT8   WgdsWiFiSarDeltaGroup5PowerChainB2;
  UINT8   WgdsWiFiSarDeltaGroup5PowerMax3;
  UINT8   WgdsWiFiSarDeltaGroup5PowerChainA3;
  UINT8   WgdsWiFiSarDeltaGroup5PowerChainB3;
  UINT8   WgdsWiFiSarDeltaGroup6PowerMax1;
  UINT8   WgdsWiFiSarDeltaGroup6PowerChainA1;
  UINT8   WgdsWiFiSarDeltaGroup6PowerChainB1;
  UINT8   WgdsWiFiSarDeltaGroup6PowerMax2;
  UINT8   WgdsWiFiSarDeltaGroup6PowerChainA2;
  UINT8   WgdsWiFiSarDeltaGroup6PowerChainB2;
  UINT8   WgdsWiFiSarDeltaGroup6PowerMax3;
  UINT8   WgdsWiFiSarDeltaGroup6PowerChainA3;
  UINT8   WgdsWiFiSarDeltaGroup6PowerChainB3;
  UINT8   WgdsWiFiSarDeltaGroup7PowerMax1;
  UINT8   WgdsWiFiSarDeltaGroup7PowerChainA1;
  UINT8   WgdsWiFiSarDeltaGroup7PowerChainB1;
  UINT8   WgdsWiFiSarDeltaGroup7PowerMax2;
  UINT8   WgdsWiFiSarDeltaGroup7PowerChainA2;
  UINT8   WgdsWiFiSarDeltaGroup7PowerChainB2;
  UINT8   WgdsWiFiSarDeltaGroup7PowerMax3;
  UINT8   WgdsWiFiSarDeltaGroup7PowerChainA3;
  UINT8   WgdsWiFiSarDeltaGroup7PowerChainB3;
  UINT8   WgdsWiFiSarDeltaGroup8PowerMax1;
  UINT8   WgdsWiFiSarDeltaGroup8PowerChainA1;
  UINT8   WgdsWiFiSarDeltaGroup8PowerChainB1;
  UINT8   WgdsWiFiSarDeltaGroup8PowerMax2;
  UINT8   WgdsWiFiSarDeltaGroup8PowerChainA2;
  UINT8   WgdsWiFiSarDeltaGroup8PowerChainB2;
  UINT8   WgdsWiFiSarDeltaGroup8PowerMax3;
  UINT8   WgdsWiFiSarDeltaGroup8PowerChainA3;
  UINT8   WgdsWiFiSarDeltaGroup8PowerChainB3;
  UINT8   Rp08D3ColdSupport;
  UINT8   Rp08D3ColdDisable;
  UINT8   Rp08WakeReworkDone;

  UINT32  WifiTASSelectionValue;
  UINT8   RplSAepBoard;
  UINT8   PchDmiGen3RtcoCpre[PCH_MAX_DMI_LANES];
  UINT8   PchDmiGen3RtcoCpo[PCH_MAX_DMI_LANES];
  UINT8   DmiGen4RtcoCpre[8];
  UINT8   DmiGen4RtcoCpo[8];
  UINT8   DmiHweq;

//[-start-211208-TAMT000035-add]//
#ifdef S77013_SUPPORT
  UINT8   iRPMMode; //iPCM mode  
#endif
//[-end-211208-TAMT000035-add]//
} SETUP_DATA;

/**
 Making any setup structure change after code frozen
 will need to maintain backward compatibility, bump up
 structure revision and update below history table\n
  <b>Revision 1</b>:  - Initial version.
  <b>Revision 2</b>:  - Updated VR related setup options.
  <b>Revision 3</b>:  - Removed Voltage Optimization Feature as not a POR.
  <b>Revision 4</b>:  - Deprecate BiosGuard enabling support
  <b>Revision 5</b>:  - Added PsysOffsetPrefix.
  <b>Revision 6</b>:  - Deprecate Vr Voltage Limit support
  <b>Revision 7</b>:  - Add Avx2VoltageScaleFactor and Avx512VoltageScaleFactor
  <b>Revision 8</b>:  - Add ApplyConfigTdp.
  <b>Revision 9</b>:  - Added Hwp Lock support.
  <b>Revision 10</b>: - Add SmmMsrSaveStateEnable, SmmUseDelayIndication,
                        SmmUseBlockIndication, SmmUseSmmEnableIndication, SmmProcTraceEnable.
  <b>Revision 11</b>: - Added number of active small core support.
  <b>Revision 12</b>: - Add PerCoreHtDisable.
  <b>Revision 13</b>: - Add VF Point Offset Mode support.
  <b>Revision 14</b>: - Updated the VR Topology data.
  <b>Revision 15</b>: - Add irms support
  <b>Revision 16</b>: - Add Per-core VF curve support.
  <b>Revision 17</b>: - Add AllSmallCoreCount/AllBigCoreCount.
  <b>Revision 18</b>: - Deprecate SmmSpsEnable enabling DGR support.
  <b>Revision 19</b>: - Add RatioLimitRatioDefault, RatioLimitRatio, RatioLimitNumCoreDefault,
                        RatioLimitNumCore, AtomRatioLimitRatioDefault, AtomRatioLimitRatio,
                        AtomRatioLimitNumCoreDefault and AtomRatioLimitNumCore to replace
                        RatioLimitXDefault and RatioLimitX.
                        Correct IsTurboRatioDefaultsInitalized to IsTurboRatioDefaultsInitialized.
  <b>Revision 20</b>: - Add DualTauBoost
  <b>Revision 21</b>: - Add AtomL2MaxOcRatio and AtomL2MaxOcRatioDefault
  <b>Revision 22</b>: - Add PerCoreRatioOverride and PerCoreRatio for Per Core Ratio override
  <b>Revision 23</b>: - Add support for Platform Voltage control
  <b>Revision 24</b>: - Add PreWake, RampUp and RampDown for Acoustic Noise Mitigation DPA tuning.
  <b>Revision 25</b>: - Add UnlimitedIccMax
  <b>Revision 26</b>: - Removed EnableItbmDriver.
  <b>Revision 27</b>: - Add IntelSpeedOptimizer.
  <b>Revision 28</b>: - Add AtomL2Voltage related setup variables.
  <b>Revision 29</b>: - Add Avx disable support.
  <b>Revision 30</b>: - Add SA/Uncore voltage configurations
  <b>Revision 31</b>: - add CrashLogGprs.
  <b>Revision 32</b>: - Add TvbRatioClipping, TvbVoltageOptimization
  <b>Revision 33</b>: - Add Vr Voltage Limit
  <b>Revision 34</b>: - Add VF Point Offset Mode support for Ring.
  <b>Revision 35</b>: - Add VccInAux IMON support
  <b>Revision 36</b>: - Add FIVR Spread Spectrum Enable.
  <b>Revision 37</b>: - Add CpuBclkOcFrequency
  <b>Revision 38</b>: - Add processor disable control support to indicate which core/atom should be disabled.
  <b>Revision 39</b>: - Add VR Power Delivery design
  <b>Revision 40</b>: - Add Core Ratio Extension Mode support.
  <b>Revision 41</b>: - Deprecate SGx.
  <b>Revision 42</b>: - Add Pvd Ratio Threshold.
  <b>Revision 43</b>: - Add per Atom Cluster VF support.
  <b>Revision 44</b>: - Add new external voltage rails overrides and deprecated some others unused voltage rails based on ADL OC RVP design.
  <b>Revision 45</b>: - Add VR Fast Vmode ICC Limit support.
  <b>Revision 46</b>: - Add CEP enable/disable support for IA/GT.
  <b>Revision 47</b>: - Add AtomPllVoltageOffset.
  <b>Revision 48</b>: - Removed FivrFaults, FivrEfficiency.
  <b>Revision 49</b>: - Add VCCIN_AUX and Richtek VccIA and VccGT voltage override support
  <b>Revision 50</b>: - Add DlvrBypassModeEnable support.
  <b>Revision 51</b>: - Deprecated AtomL2MaxOcRatio
  <b>Revision 52</b>: - Deprecated Avx3Disable
  <b>Revision 53</b>: - Deprecated Avx512VoltageScaleFactor and Avx3RatioOffset
  <b>Revision 54</b>: - Add SaPllFreqOverride support.
  <b>Revision 55</b>: - Add PllMaxBandingRatio
  <b>Revision 56</b>: - Add TscDisableHwFixup
  <b>Revision 57</b>: - Add IaIccMax, IaIccUnlimitedMode, GtIccMax, GtIccUnlimitedMode, deprecated UnlimitedIccMax
  <b>Revision 58</b>: - Add PerCoreDisableConfiguration
  <b>Revision 59</b>: - Add TvbDownBinsTempThreshold0, TvbTempThreshold0, TvbTempThreshold1, TvbDownBinsTempThreshold1
  <b>Revision 60</b>: - Add GameComptibilityMode
  <b>Revision 61</b>: - Deprecated SafModeDisableBiosGuard
  <b>Revision 62</b>: - Deprecated PllMaxBandingRatio
  <b>Revision 63</b>: - Add FllOcModeEn, FllOverclockMode
  <b>Revision 64</b>: - Added PerCoreDisable1, PerCoreDisableOriMask and Deprecated PerCoreDisable, PerAtomDisable.
  <b>Revision 65</b>: - Add Etvb
**/
#define CPU_SETUP_REVISION  65

typedef struct {
  UINT8   Revision;
  UINT8   CpuRatio;
  UINT8   CpuDefaultRatio;
  UINT8   CpuRatioOverride;
  UINT8   Peci;
  UINT8   HyperThreading;
//
// CPU: Number of active Big core.
//
  UINT8   ActiveCoreCount;
  UINT8   BistOnReset;
  UINT8   JtagC10PowerGateDisable;
  UINT8   EnableGv;
  UINT8   RaceToHalt;
  UINT8   EnableHwp;
  UINT8   EnableItbm;
  UINT8   Reserved;

  UINT8   EnablePerCorePState;
  UINT8   EnableHwpAutoPerCorePstate;
  UINT8   EnableHwpAutoEppGrouping;
  UINT8   EnableEpbPeciOverride;
  UINT8   EnableFastMsrHwpReq;

  UINT8   BootFrequency;
  UINT8   EnableCx;
  UINT8   EnableC1e;
  UINT8   TurboMode;
  UINT32  PowerLimit1;
  UINT8   LongDurationPwrLimitOverride;
  UINT8   PowerLimit1Time;
  UINT8   PowerLimit2;
  UINT32  PowerLimit2Power;
  UINT8   PowerLimit3Override;
  UINT32  PowerLimit3;
  UINT8   PowerLimit3Time;
  UINT8   PowerLimit3DutyCycle;
  UINT8   PowerLimit3Lock;
  UINT8   PowerLimit4Override;
  UINT32  PowerLimit4;
  UINT8   PowerLimit4Lock;
  UINT8   TurboPowerLimitLock;
  UINT8   PlatformPowerLimit1Enable;
  UINT32  PlatformPowerLimit1Power;
  UINT8   PlatformPowerLimit1Time;
  UINT8   PlatformPowerLimit2Enable;
  UINT32  PlatformPowerLimit2Power;
  UINT8   EnergyEfficientPState;
  UINT8   CStatePreWake;
  UINT8   CStateAutoDemotion;
  UINT8   CStateUnDemotion;
  UINT8   PkgCStateDemotion;
  UINT8   PkgCStateUnDemotion;
  UINT8   EnableThermalMonitor;
  UINT8   PmgCstCfgCtrlLock;
  UINT8   ConfigTdpLevel;
  UINT8   ConfigTdpLock;
  UINT8   ConfigTdpBios;
  UINT8   TimedMwait;
  UINT8   PmgCstCfgCtrIoMwaitRedirection;
  UINT8   InterruptRedirectMode;

  UINT8   HdcControl;
  UINT8   PkgCStateLimit;
  UINT8   CstateLatencyControl1TimeUnit;
  UINT8   CstateLatencyControl2TimeUnit;
  UINT8   CstateLatencyControl3TimeUnit;
  UINT8   CstateLatencyControl4TimeUnit;
  UINT8   CstateLatencyControl5TimeUnit;
  UINT16  CstateLatencyControl1Irtl;
  UINT16  CstateLatencyControl2Irtl;
  UINT16  CstateLatencyControl3Irtl;
  UINT16  CstateLatencyControl4Irtl;
  UINT16  CstateLatencyControl5Irtl;

  UINT32  Custom1PowerLimit1Power;
  UINT32  Custom1PowerLimit2Power;
  UINT8   Custom1PowerLimit1Time;
  UINT8   Custom1TurboActivationRatio;
  UINT32  Custom2PowerLimit1Power;
  UINT32  Custom2PowerLimit2Power;
  UINT8   Custom2PowerLimit1Time;
  UINT8   Custom2TurboActivationRatio;
  UINT32  Custom3PowerLimit1Power;
  UINT32  Custom3PowerLimit2Power;
  UINT8   Custom3PowerLimit1Time;
  UINT8   Custom3TurboActivationRatio;
  UINT8   TStatesEnable;
  UINT8   EnableProcHot;
  UINT8   DisableProcHotOut;
  UINT8   DisableVrThermalAlert;
  UINT8   ProcHotLock;
  UINT8   ProcHotResponse;
  UINT8   TCCActivationOffset;
  UINT8   NumOfCustomPStates;
  UINT8   StateRatio[MAX_CUSTOM_P_STATES];
  UINT8   StateRatioMax16[MAX_16_CUSTOM_P_STATES];
  UINT8   VT;
  UINT8   AES;
  UINT8   MachineCheck;
  UINT8   MonitorMwait;
  UINT16  DprSize;
  UINT8   HwPrefetcher;
  UINT8   AclPrefetch;
  UINT8   Txt;
  UINT8   LtDprProgramming;
  UINT8   ResetAux;
  UINT8   AcheckRequest;
  UINT8   MsegOpt;
  UINT8   MsegSize;

  UINT8   EcTurboControlMode;
  UINT8   AcBrickCapacity;
  UINT8   EcPollingPeriod;
  UINT8   EcGuardBandValue;
  UINT8   EcAlgorithmSel;

  UINT8   IsTurboRatioDefaultsInitialized;
  UINT8   FlexRatioOverrideDefault;
  UINT8   RatioLimitRatioDefault[TURBO_RATIO_LIMIT_ARRAY_SIZE];
  UINT8   RatioLimitRatio[TURBO_RATIO_LIMIT_ARRAY_SIZE];
  UINT8   RatioLimitNumCoreDefault[TURBO_RATIO_LIMIT_ARRAY_SIZE];
  UINT8   RatioLimitNumCore[TURBO_RATIO_LIMIT_ARRAY_SIZE];
  UINT8   AtomRatioLimitRatioDefault[TURBO_RATIO_LIMIT_ARRAY_SIZE];
  UINT8   AtomRatioLimitRatio[TURBO_RATIO_LIMIT_ARRAY_SIZE];
  UINT8   AtomRatioLimitNumCoreDefault[TURBO_RATIO_LIMIT_ARRAY_SIZE];
  UINT8   AtomRatioLimitNumCore[TURBO_RATIO_LIMIT_ARRAY_SIZE];
  UINT8   OverclockingLock;

  UINT8   Reserved1;
  UINT8   BiosGuardToolsInterface;

  UINT8   DebugInterfaceEnable;
  UINT8   DebugInterfaceLockEnable;
  UINT8   DebugCpuDisabled;

  UINT8   EnableC6Dram;
  UINT8   EnableSgx;              // Deprecated from Revision 41
  UINT8   EpochUpdate;            // Deprecated from Revision 41
  UINT8   ShowEpoch;              // Deprecated from Revision 41
  UINT32  MaxPrmrrSize;           // Deprecated from Revision 41
  UINT32  PrmrrSize;              // Deprecated from Revision 41
  UINT8   ApIdleManner;

  UINT8   ProcessorTraceOutputScheme;
  UINT8   ProcessorTraceMemSize;
  UINT8   ProcessorTraceEnable;

  UINT8   SkipStopPbet;
  UINT8   ThreeStrikeCounterDisable;
  UINT8   EpocFclkFreq;

  //
  // VR Config. IA = 0, GT =1
  //
  UINT8   PsysSlope;
  UINT16  PsysOffset;
  UINT8   PsysOffsetPrefix;
  UINT16  PsysPmax;
  UINT8   VrConfigEnable[MAX_VR_NUM];
  UINT16  AcLoadline[MAX_VR_NUM];
  UINT16  DcLoadline[MAX_VR_NUM];
  UINT16  Psi1Threshold[MAX_VR_NUM];
  UINT16  Psi2Threshold[MAX_VR_NUM];
  UINT16  Psi3Threshold[MAX_VR_NUM];
  UINT8   Psi3Enable[MAX_VR_NUM];
  UINT8   Psi4Enable[MAX_VR_NUM];
  UINT16  ImonSlope[MAX_VR_NUM];
  UINT16  ImonOffset[MAX_VR_NUM];
  UINT8   ImonOffsetPrefix[MAX_VR_NUM];
  UINT16  IccMax[MAX_VR_NUM];
  UINT16  TdcCurrentLimit[MAX_VR_NUM];
  UINT8   TdcEnable[MAX_VR_NUM];
  UINT32  TdcTimeWindow[MAX_VR_NUM];
  UINT8   TdcLock[MAX_VR_NUM];
  UINT8   Irms[MAX_VR_NUM];
  UINT16  VrVoltageLimit[MAX_VR_NUM];
  UINT32  TccOffsetTimeWindow;
  UINT8   TccOffsetClamp;
  UINT8   TccOffsetLock;

  //
  // CPU related
  //
  UINT8   FlexOverrideEnable;
  UINT8   MaxEfficiencyRatio;
  UINT8   MaxNonTurboRatio;
  UINT8   FlexRatioOverride;

  UINT8   BusSpeedEnable;
  UINT8   ProcessorBusSpeedOverride;

  UINT8   PciePll;
  UINT8   EnergyEfficientTurbo;

  UINT8   WDTSupportforNextOSBoot;
  UINT16  TimeforNextOSBoot;

  //
  // OverClocking setup options
  //
  UINT8   OverclockingSupport;

  UINT8   CoreRatioExtensionMode;
  UINT8   CoreMaxOcRatio;
  UINT8   PvdRatioThreshold;
  UINT8   CoreVoltageMode;
  UINT16  CoreVoltageOverride;
  UINT16  CoreVoltageOffset;
  UINT8   CoreVoltageOffsetPrefix;
  UINT16  CoreExtraTurboVoltage;
  UINT8   Avx2RatioOffset;
  UINT8   Avx3RatioOffset;  // Deprecated from Revision 53
  UINT8   RingMaxOcRatio;
  UINT8   RingDownBin;
  UINT8   RingVoltageMode;
  UINT16  RingVoltageOverride;
  UINT16  RingVoltageOffset;
  UINT8   RingVoltageOffsetPrefix;
  UINT16  RingExtraTurboVoltage;

  UINT8   EnableAllThermalFunctions;
  UINT8   CorePllVoltageOffset;
  UINT8   GtPllVoltageOffset;
  UINT8   RingPllVoltageOffset;
  UINT8   SaPllVoltageOffset;
  UINT8   AtomPllVoltageOffset;
  UINT8   McPllVoltageOffset;
  UINT8   TjMaxOffset;

  //
  // RFI Setup Options
  //
  UINT16 FivrRfiFrequency;
  UINT8  FivrSpreadSpectrum;

  //
  // Acoustic Noise Mitigation
  //
  UINT8   AcousticNoiseMitigation;
  UINT8   FastPkgCRampDisable[MAX_VR_NUM];
  UINT8   SlowSlewRate[MAX_VR_NUM];
  UINT8   EnableMinVoltageOverride;
  UINT16  MinVoltageRuntime;
  UINT16  MinVoltageC8;

  UINT8   HwpInterruptControl;
  UINT8   BclkAdaptiveVoltageEnable;

  UINT8   PpinSupport;
  UINT8   PpinEnableMode;

  UINT8   MinRingRatioLimit;
  UINT8   MaxRingRatioLimit;

  UINT8   SafModeDisableBiosGuard; // Deprecated from Revision 61
  // Bclk RFI Frequency for each SAGV point in 10KHz units.
  UINT32  BclkRfi10KhzFreq[4];
  UINT8   CpuBclkSpread;

  UINT8   AcSplitLock;
  UINT8   Avx2VoltageScaleFactor;
  UINT8   Avx512VoltageScaleFactor;  // Deprecated from Revision 53
  UINT8   ApplyConfigTdp;
  UINT8   HwpLock;
  UINT8   TmeEnable;
  UINT8   SmmMsrSaveStateEnable;
  UINT8   SmmUseDelayIndication;
  UINT8   SmmUseBlockIndication;
  UINT8   SmmUseSmmEnableIndication;
  UINT8   SmmProcTraceEnable;
  //
  // CPU: Number of active small core.
  //
  UINT8   ActiveSmallCoreCount;
  //
  // HyerThreading Disable
  //
  UINT16  PerCoreHtDisable;
//
// Core VF Offset Support
//
  UINT8   CoreVfPointOffsetMode;
  UINT8   CoreVfPointCount;
  UINT16  CoreVfPointOffset[CPU_OC_MAX_VF_POINTS];
  UINT8   CoreVfPointOffsetPrefix[CPU_OC_MAX_VF_POINTS];

  UINT8   CoreVfConfigScope;
  UINT16  PerCoreVoltageOffset[CPU_MAX_BIG_CORES];
  UINT8   PerCoreVoltageOffsetPrefix[CPU_MAX_BIG_CORES];
  UINT8   Reserved2[2];

//
// Ring VF Offset Support
//
  UINT8   RingVfPointOffsetMode;
  UINT8   RingVfPointCount;
  UINT16  RingVfPointOffset[CPU_OC_MAX_VF_POINTS];
  UINT8   RingVfPointOffsetPrefix[CPU_OC_MAX_VF_POINTS];

  //
  // CPU: Record number of all cores including enabled & disabled.
  //
  UINT8   AllSmallCoreCount;
  UINT8   AllBigCoreCount;

  UINT8   DualTauBoost;
  //
  // ATOM Overclocking
  //
  UINT8   AtomL2MaxOcRatio;  ///<Deprecated from revision 51

  UINT8   AtomL2VoltageMode;

  UINT16  AtomL2VoltageOverride;

  UINT16  AtomL2VoltageOffset;

  UINT8   AtomL2VoltageOffsetPrefix;

  UINT16  AtomL2ExtraTurboVoltage;

  UINT8   AtomClusterRatio[CPU_MAX_ATOM_CLUSTERS];

  UINT8   PerCoreRatioOverride;
  UINT8   PerCoreRatio[CPU_MAX_BIG_CORES];
  //
  // Unlimited ICCMAX
  //
  UINT8   UnlimitedIccMax;            // Deprecated from Revision 57

  //
  // Platform Voltage Override setup options
  //
  UINT8   VccCoreOverrideEnable;      // Deprecated from Revision 44
  UINT16  VccCoreOverrideVoltage;     // Deprecated from Revision 44
  UINT8   VccStOverrideEnable;        // Deprecated from Revision 44
  UINT16  VccStOverrideVoltage;       // Deprecated from Revision 44
  UINT8   VccSaOverrideEnable;        // Deprecated from Revision 44
  UINT16  VccSaOverrideVoltage;       // Deprecated from Revision 44
  UINT8   VccSfrOcOverrideEnable;     // Deprecated from Revision 44
  UINT16  VccSfrOcOverrideVoltage;    // Deprecated from Revision 44
  UINT8   VccSfrOverrideEnable;       // Deprecated from Revision 44
  UINT16  VccSfrOverrideVoltage;      // Deprecated from Revision 44
  UINT8   VccIoOverrideEnable;        // Deprecated from Revision 44
  UINT16  VccIoOverrideVoltage;       // Deprecated from Revision 44

  //
  // VR Core Load Line override
  //
  UINT8   CoreVrDcLLOverrideEnable;
  UINT8   CoreDcLoadline;

  //
  // Acoustic Noise Mitigation Range for Dynamic Periodicity Alteration (DPA)Tuning
  //
  UINT8   PreWake;
  UINT8   RampUp;
  UINT8   RampDown;

  UINT8   AvxDisable;
  UINT8   Avx3Disable;  ///<Deprecated from revision 52
  //
  // SA/Uncore voltage configurations
  //
  UINT8  SaVoltageMode;
  UINT16 SaVoltageOverride;
  UINT16 SaExtraTurboVoltage;

  //
  // Thermal Velocity Boost options
  //
  UINT8   TvbRatioClipping;
  UINT8   TvbVoltageOptimization;
  UINT8   Reserved4[3];

  //
  // VccIn AUX IMON
  //
  UINT16  VccInAuxImonIccImax;
  UINT16  VccInAuxImonSlope;
  UINT16  VccInAuxImonOffset;
  UINT8   VccInAuxImonOffsetPrefix;

  //
  // Vsys
  //
  UINT8  EnableVsysCritical;
  UINT8  VsysFullScale;
  UINT8  VsysCriticalThreshold;
  UINT8  VsysAssertionDeglitchMantissa;
  UINT8  VsysAssertionDeglitchExponent;
  UINT8  VsysDeassertionDeglitchMantissa;
  UINT8  VsysDeassertionDeglitchExponent;

  //
  // FIVR SSC Enable
  //
  UINT8   FivrSpectrumEnable;
  //
  // CPU BCLK OC Frequency
  //
  UINT32  CpuBclkOcFrequency;

  //
  // Processor Disable control
  //
  UINT8   PerCoreDisable[CPU_MAX_BIG_CORES];  // Deprecated from version 64
  UINT8   PerAtomDisable[CPU_MAX_ATOM_CORES]; // Deprecated from version 64

  ///
  /// VR Power Delivery Design
  ///
  UINT8   VrPowerDeliveryDesign;

  UINT8   CrashLogGprs;

  UINT16  PerAtomClusterVoltageOffset[CPU_MAX_ATOM_CLUSTERS];
  UINT8   PerAtomClusterVoltageOffsetPrefix[CPU_MAX_ATOM_CLUSTERS];

  //
  // External voltage rails overrides
  //
  UINT8   Vcc1p8OverrideEnable;
  UINT16  Vcc1p8OverrideVoltage;
  UINT8   Vcc1p05OverrideEnable;
  UINT16  Vcc1p05OverrideVoltage;
  UINT8   VccDd2OverrideEnable;
  UINT16  VccDd2OverrideVoltage;
  UINT16  IccLimit[MAX_VR_NUM];

  //
  // IA/GT CEP (Current Excursion Protection) Enable
  //
  UINT8   IaCepEnable;
  UINT8   GtCepEnable;

  UINT8   VccInAuxEnable;
  UINT16  VccInAuxVoltage;
  UINT8   RichtekVccIaEnable;
  UINT8   RichtekVccGtEnable;
  UINT8   RichtekVccIaFixedVidMode;
  UINT8   RichtekVccGtFixedVidMode;
  UINT16  RichtekVccIaVoltage;
  UINT16  RichtekVccGtVoltage;

  //
  // CPU DLVR bypass mode Enable/Disable
  //
  UINT8   DlvrBypassModeEnable;

  //
  // Sa PLL Frequency Configuration
  //
  UINT8   SaPllFreqOverride;

  //
  // PLL Max Banding Ratio overrides.
  //
  UINT8   PllMaxBandingRatio;  // Deprecated from Revision 62

  //
  // TSC HW Fixup disable
  //
  UINT8   TscDisableHwFixup;
  UINT8   Reserved5[1];

  //
  // IA/GT ICC MAX settings
  //
  UINT8   IaIccUnlimitedMode;
  UINT16  IaIccMax;
  UINT8   GtIccUnlimitedMode;
  UINT16  GtIccMax;
  UINT8   PerCoreDisableConfiguration;
  //
  // Legacy Game Compatibility Mode
  //
  UINT8   GameCompatibilityMode;
  //
  // TVB controls for Down Bins and Temperature Thresholds
  //
  UINT8   TvbDownBinsTempThreshold0;
  UINT8   TvbTempThreshold0;
  UINT8   TvbTempThreshold1;
  UINT8   TvbDownBinsTempThreshold1;
  UINT8   FllOcModeEn;
  UINT8   FllOverclockMode;

  UINT32  PerCoreDisableOriMask;
  UINT8   PerCoreDisable1[32];
  UINT8   Etvb;



} CPU_SETUP;

/**
 Making any setup structure change after code frozen
 will need to maintain backward compatibility, bump up
 structure revision and update below history table\n
  <b>Revision 1</b>:  - Initial version.
**/
#define CPU_SETUP_VOLATILE_DATA_REVISION  1

typedef struct {
  UINT16  CpuFamilyModel;
  UINT16  CpuExtendedFamilyModel;
  UINT8   CpuStepping;
  UINT8   EdramSupported;

  //
  // Vr Topology
  //
  UINT8  CoreVrLocked;
  UINT8  GtVrLocked;
  UINT8  SaVrLocked;
  UINT8  TccSupported;
} CPU_SETUP_VOLATILE_DATA;


/**
 Making any setup structure change after code frozen
 will need to maintain backward compatibility, bump up
 structure revision and update below history table\n
  <b>Revision 1</b>:  - Initial version.
**/
#define TBT_SETUP_VOLATILE_DATA_REVISION  1

typedef struct {
  UINT8                DTbtContollersNumber;
  UINT8                ITbtRootPortsNumber;
  UINT8                TcssCpuXhciPortSupport[MAX_TCSS_USB3_PORTS];
} TBT_SETUP_VOLATILE_DATA;

/**
 Making any setup structure change after code frozen
 will need to maintain backward compatibility, bump up
 structure revision and update below history table\n
  <b>Revision 1</b>:  - Initial version.
  <b>Revision 2</b>:  - Add MacPassThrough
  <b>Revision 3</b>:  - Add One Click Recovery setup variables
  <b>Revision 4</b>:  - Add Remote Platform Erase setup variables
  <b>Revision 5</b>:  - Add Cse Data Resilience setup variables
**/

#define ME_SETUP_REVISION   5

typedef struct {
  UINT8  Revision;
  //
  // Intel ME
  //
  UINT8  MeFirmwareInfo;
  UINT8  MeImageType;
  UINT8  MeFwDowngrade;
  UINT8  CommsHubEnable;

  UINT8  HeciTimeouts;
  UINT8  DidInitStat;
  UINT8  DisableCpuReplacedPolling;
  UINT8  DisableMessageCheck;
  UINT8  SkipMbpHob;
  UINT8  HeciCommunication2;
  UINT8  KtDeviceEnable;
  UINT8  DisableD0I3SettingForHeci;
  UINT8  MctpBroadcastCycle;

  UINT8  ChangeEps;
  UINT8  UnconfigOnRtcAvailable;
  UINT8  MeUnconfigOnRtcClear;
  UINT8  CoreBiosDoneEnabled;
  UINT8  EndOfPostMessage;
  UINT8  MeJhiSupport;

  //
  // Intel AMT
  //
  UINT8  Amt;
  UINT8  AmtbxHotKeyPressed;        ///< @deprecated
  UINT8  AmtbxSelectionScreen;      ///< @deprecated
  UINT8  HideUnConfigureMeConfirm;  ///< @deprecated
  UINT8  MebxDebugMsg;              ///< @deprecated
  UINT8  UnConfigureMe;
  UINT8  AmtCiraRequest;
  UINT8  AmtCiraTimeout;            ///< @deprecated
  UINT8  UsbProvision;
  UINT8  FwProgress;

  //
  // ASF
  //
  UINT8  AsfSensorsTable;

  //
  // WatchDog
  //
  UINT8  WatchDogEnabled;
  UINT16 WatchDogTimerOs;
  UINT16 WatchDogTimerBios;

  //
  // MEBx resolution settings
  //
  UINT8  MebxNonUiTextMode;         ///< @deprecated
  UINT8  MebxUiTextMode;            ///< @deprecated
  UINT8  MebxGraphicsMode;          ///< @deprecated

  //
  // Extend ME/CSME Measurements to TPM-PCR
  //
  UINT8  ExtendMeMeasuredBoot;

  //
  // Anti-Rollback SVN
  //
  UINT8  AutoArbSvnCommit;

  //
  // ME-SMBIOS 130 capabilities
  //
  UINT8  BiosReflash;
  UINT8  BiosSetup;
  UINT8  BiosPause;
  UINT8  SecureBootExposureToFw;
  UINT8  vProTbtDock;

  //
  // MAC Pass Through
  //
  UINT8  MacPassThrough;

  //
  // One Click Recovery
  //
  UINT8  OcrBootHttps;
  UINT8  OcrBootPba;
  UINT8  OcrBootWinRe;
  UINT8  OcrAmtDisSecBoot;

  //
  // Remote Platform Erase
  //
  UINT8  EnableRpe;
  UINT8  RpeSsdEraseRealMode;

  // CSE Data Resilience
  UINT8  CseDataResilience;

} ME_SETUP;



/**
 Making any setup structure change after code frozen
 will need to maintain backward compatibility, bump up
 structure revision and update below history table\n
  <b>Revision 1</b>:  - Initial version.
  <b>Revision 2</b>:  - Adding PEG RTD3 Support Setup Variable
  <b>Revision 3</b>:  - Remove IedMemEnable.
  <b>Revision 4</b>:  - Added VmdGlobalMapping
  <b>Revision 5</b>:  - Added TCSS Tc C-State Limit.
  <b>Revision 6</b>:  - Adding support for TCSS CPU XHCI Port Disable Override.
  <b>Revision 7</b>:  - Added DisplayAudioLink
  <b>Revision 8</b>:  - Added PPR Enable Type
  <b>Revision 9</b>:  - Added LpddrRttWr and LpddrRttCa
  <b>Revision 10</b>: - Added MemoryVddqVoltage, MemoryVppVoltage
  <b>Revision 11</b>: - DisableDimmMcXChY changed to DisableMcXChY
  <b>Revision 12</b>: - Added PcieRootPortNewFom and DmiNewFom
  <b>Revision 13</b>: - Added DmiCdrRelock and PcieRootPortCdrRelock.
  <b>Revision 14</b>: - Added DIMMDFE
  <b>Revision 15</b>: - Added ExtendedBankHashing
  <b>Revision 16</b>: - Added IBECC Error Injection knobs: IbeccErrInjControl, IbeccErrInjAddress, IbeccErrInjMask, IbeccErrInjCount
  <b>Revision 17</b>: - Deprecated DdrOddRatioMode
  <b>Revision 18</b>: - Added DynamicMemoryBoost
  <b>Revision 19</b>: - Added HgSupport
  <b>Revision 20</b>: - Added Config Fusa
  <b>Revision 21</b>: - Added RefreshWm; Deprecated RefreshPanicWm and RefreshHpWm
  <b>Revision 22</b>: - Deprecated RpFunctionSwap
  <b>Revision 23</b>: - Added PcieFunc0LinkDisable
  <b>Revision 24</b>: - Added DebugValue
  <b>Revision 25</b>: - Added PcieResizableBarSupport
  <b>Revision 26</b>: - Added McRefreshRate; Deprecated McRefresh2X
  <b>Revision 27</b>: - Added RealtimeMemoryFrequency
  <b>Revision 28</b>: - Added PeriodicDcc and LpMode
  <b>Revision 29</b>: - Added TcColdPowerSavingFactorSwitch, IomStayInTCColdSeconds and IomBeforeEnteringTCColdSeconds
  <b>Revision 30</b>: - Added TX DQS DCC
  <b>Revision 31</b>: - Added Timing parameters, tRFCpb, tRFC2, ..., tWTR_S
  <b>Revision 32</b>: - Added EccErrInjAddress, EccErrInjMask, EccErrInjCount
  <b>Revision 33</b>: - Added FreqLimitMixedConfig and FirstDimmBitMask
  <b>Revision 34</b>: - Added SAGV SagvSwitchFactorIA/GT/IO/Stall, SagvHeuristicsUpControl, SagvHeuristicsDownControl
  <b>Revision 35</b>: - Added DRAMDCA
  <b>Revision 36</b>: - Added DynamicMemoryBoostTrainingFailed
  <b>Revision 37</b>: - Added FreqLimitMixedConfig_1R1R_8GB, FreqLimitMixedConfig_1R1R_16GB, FreqLimitMixedConfig_1R1R_8GB_16GB, FreqLimitMixedConfig_2R2R
  <b>Revision 38</b>: - Deprecated IgdDvmt50TotalAlloc
  <b>Revision 39</b>: - Added LctCmdEyeWidth
  <b>Revision 40</b>: - FirstDimmBitMaskEcc
  <b>Revision 41</b>: - Lp5BankMode
  <b>Revision 43</b>: - WRDS
  <b>Revision 44</b>: - EARLYDIMMDFE
**/

#define SA_SETUP_REVISION   44

typedef struct {
  UINT8   Revision;
  UINT8   BdatEnable;

  //
  // Memory related
  //
  UINT8   XmpProfileEnable;
  UINT16  MemoryVoltage;
  //
  // Memory timing override (these options are used for Custom/User Profile)
  //
  UINT8   DdrRefClk;
  UINT8   DdrRatio;
  UINT8   DdrOddRatioMode;  // Deprecated from revision 18
  UINT8   tCL;
  UINT8   tCWL;
  UINT16  tFAW;
  UINT16  tRAS;
  UINT8   tRCDtRP;
  UINT16  tREFI;
  UINT16  tRFC;
  UINT8   tRRD;
  UINT8   tRTP;
  UINT8   tWR;
  UINT8   tWTR;
  UINT8   NModeSupport;
  //
  // Default values to restore memory timings to in case of Platform instability
  //
  UINT8   tCLDefault;
  UINT8   tCWLDefault;
  UINT16  tFAWDefault;
  UINT16  tRASDefault;
  UINT8   tRCDtRPDefault;
  UINT16  tREFIDefault;
  UINT16  tRFCDefault;
  UINT8   tRRDDefault;
  UINT8   tRTPDefault;
  UINT8   tWRDefault;
  UINT8   tWTRDefault;
  UINT8   NModeDefault;

  UINT16  GtDid;                    // not a SETUP item, used by BIOS to pass GT SKU DID to SETUP items
  UINT8   IgdBootType;
  UINT8   DisplayPipeB;
  UINT8   LcdPanelType;

  UINT8   LcdPanelScaling;

  UINT8   EnableRenderStandby;
  UINT8   IgdLcdBlc;
  UINT8   AlsEnable;
  UINT8   ActiveLFP;
  UINT8   LfpColorDepth;
  UINT8   GTTSize;
  UINT8   ApertureSize;
  UINT8   GtPsmiSupport;
  UINT8   GtPsmiRegionSize;
  UINT8   PanelPowerEnable;
  UINT8   PmSupport;
  UINT8   PeiGraphicsPeimInit;
  UINT8   PavpEnable;
  UINT8   VbtSelect;
  UINT8   VbiosBrightness;
  UINT8   CdynmaxClampEnable;
  UINT8   CdClock;
  UINT8   GtFreqMax;
  UINT8   DisableTurboGt;
  UINT8   SkipCdClockInit;
  UINT8   RC1pFreqEnable;
  //
  // ICC Related
  //
  UINT8   BclkOverride;
  UINT32  BclkFrequency;

  //
  // PEG
  //
  UINT8   PegAspm[4];
  UINT8   Peg0Enable;
  UINT8   Peg1Enable;
  UINT8   Peg2Enable;
  UINT8   Peg3Enable;
  UINT8   Peg0PowerDownUnusedLanes;
  UINT8   Peg1PowerDownUnusedLanes;
  UINT8   Peg2PowerDownUnusedLanes;
  UINT8   Peg3PowerDownUnusedLanes;



  UINT8   PegMaxPayload[4];
  UINT8   Peg0LtrEnable;
  UINT8   Peg1LtrEnable;
  UINT8   Peg2LtrEnable;
  UINT8   Peg3LtrEnable;
  UINT8   Peg0ObffEnable;
  UINT8   Peg1ObffEnable;
  UINT8   Peg2ObffEnable;
  UINT8   Peg3ObffEnable;
  UINT8   InitAspmAfterOprom;
  UINT8   PegRootPortHPE[4];



  //
  // PEG Hotplug Resources
  //
  UINT8   PegExtraBusRsvd[4];
  UINT16  PegMemRsvd[4];
  UINT8   PegIoRsvd[4];

  UINT8   EpgEnable;
  UINT16  Idd3n;
  UINT16  Idd3p;

  //
  // DVMT5.0 Graphic memory setting
  //
  UINT8   IgdDvmt50PreAlloc;
  UINT8   IgdDvmt50TotalAlloc;  //Deprecated from revision 38
  //
  // SA Device Control
  //
  UINT8   SaDevice4;
  UINT8   EnableVtd;
  UINT8   EnableAbove4GBMmio;
  UINT8   GnaEnable;
  UINT8   SaIpuEnable;
  UINT8   SaIpuImrConfiguration;
  UINT8   EdramTestMode;
  UINT8   CpuTraceHubMode;
  UINT8   CpuTraceHubMemReg0Size;
  UINT8   CpuTraceHubMemReg1Size;
  //
  // VT-d
  //
  UINT8   X2ApicOptOut;
  UINT8   DmaControlGuarantee;
  UINT8   VtdIgdEnable;
  UINT8   VtdIpuEnable;
  UINT8   VtdIopEnable;
  //
  // DMI
  //
  UINT8   DmiMaxLinkSpeed;

  UINT8   DmiDeEmphasis;
  UINT8   DmiGen3EqPh2Enable;
  UINT8   DmiGen3EqPh3Method;
  UINT8   DmiGen3ProgramStaticEq;
  UINT8   DmiGen3RootPortPreset[8];
  UINT8   DmiGen3EndPointPreset[8];
  UINT8   DmiGen3EndPointHint[8];
  UINT8   DmiGen3RxCtlePeaking[4];
  UINT8   DmiAspm;
  UINT8   DmiAspmCtrl;
  UINT8   DmiAspmL1ExitLatency;
  // Stop Grant
  UINT8   AutoNsg;
  UINT8   NumStopGrant;
  // Primary Display Select
  UINT8   PrimaryDisplay;
  UINT8   PcieCardSelect;
  UINT16  DelayAfterPwrEn;
  UINT16  DelayAfterHoldReset;
  UINT8   InternalGraphics;
  // Graphics Turbo IMON Current
  UINT8   GfxTurboIMON;
  UINT8   PcieMultipleSegmentEnabled;
  // TCSS
  UINT8   TcssItbtPcie0En;
  UINT8   TcssItbtPcie1En;
  UINT8   TcssItbtPcie2En;
  UINT8   TcssItbtPcie3En;
  UINT8   TcssXhciEn;
  UINT8   TcssXdciEn;
  UINT8   TcssDma0En;
  UINT8   TcssDma1En;

  // VCCST Status
  UINT8   TcssVccstStatus;
  // USB Override for IOM
  UINT8   UsbOverride;
  //Enable D3 Cold in TCSS
  UINT8   D3ColdEnable;
  //Enable D3 hot in TCSS
  UINT8   D3HotEnable;

  //Enable PTM in TCSS
  UINT8   PtmEnabled[MAX_ITBT_PCIE_PORT];
  //
  // PCIe LTR Configuration
  //
  UINT8   SaPcieItbtLtrEnable[MAX_ITBT_PCIE_PORT];
  UINT8   SaPcieItbtSnoopLatencyOverrideMode[MAX_ITBT_PCIE_PORT];
  UINT8   SaPcieItbtSnoopLatencyOverrideMultiplier[MAX_ITBT_PCIE_PORT];
  UINT16  SaPcieItbtSnoopLatencyOverrideValue[MAX_ITBT_PCIE_PORT];
  UINT8   SaPcieItbtNonSnoopLatencyOverrideMode[MAX_ITBT_PCIE_PORT];
  UINT8   SaPcieItbtNonSnoopLatencyOverrideMultiplier[MAX_ITBT_PCIE_PORT];
  UINT16  SaPcieItbtNonSnoopLatencyOverrideValue[MAX_ITBT_PCIE_PORT];
  UINT8   SaPcieItbtForceLtrOverride[MAX_ITBT_PCIE_PORT];
  UINT8   SaPcieItbtLtrConfigLock[MAX_ITBT_PCIE_PORT];
  //
  // Cpu Telemetry Aggregator (CrashLog)
  //
  UINT8   CpuCrashLogDevice;

  //
  // VMD:Settings
  //
  UINT8   VmdSupported;
  UINT8   VmdEnable;
  UINT8   VmdPort[VMD_MAX_DEVICES];
  UINT8   VmdPortDev[VMD_MAX_DEVICES];
  UINT8   VmdPortFunc[VMD_MAX_DEVICES];
  UINT8   VmdPortPresent[VMD_MAX_DEVICES];
  UINT8   VmdCfgBarSize;
  UINT8   VmdCfgBarAttr;
  UINT8   VmdMemBarSize1;
  UINT8   VmdMemBar1Attr;
  UINT8   VmdMemBarSize2;
  UINT8   VmdMemBar2Attr;
  UINT8   VmdPortAPresent;
  UINT8   VmdPortBPresent;
  UINT8   VmdPortCPresent;
  UINT8   VmdPortDPresent;
  UINT8   RaidR0;
  UINT8   RaidR1;
  UINT8   RaidR10;
  UINT8   RaidR5;
  UINT8   Rrt;
  UINT8   RrtOnlyEsata;
  UINT8   OptaneMemory;
  UINT8   VmdRsvd[2];
  //
  // MRC settings
  //
  UINT8   HobBufferSize;
  UINT8   EccSupport;
  UINT8   MaxTolud;
  UINT16  DdrFreqLimit;
  UINT8   SpdProfileSelected;
  UINT8   MrcTimeMeasure;
  UINT8   MrcFastBoot;
  UINT8   RmtPerTask;
  UINT8   TrainTrace;
  UINT8   ChHashOverride;
  UINT8   ChHashEnable;
  UINT16  ChHashMask;
  UINT8   ChHashInterleaveBit;
  UINT8   PerBankRefresh;
  UINT8   Vc1ReadMeter;
  UINT8   StrongWkLeaker;
  UINT8   ForceSingleRank;
  UINT8   LpddrMemWriteLatencySet;
  UINT8   RetrainOnFastFail;
  UINT8   Ddr4OneDpc;
  UINT8   SafeMode;
  UINT8   BdatTestType;
  UINT8   RMTLoopCount;
  UINT8   MemTestOnWarmBoot;
  UINT8   PowerDownMode;
  UINT8   PwdwnIdleCounter;
  UINT8   DisPgCloseIdleTimeout;
  UINT8   Ibecc;
  UINT8   IbeccOperationMode;
  UINT8   IbeccProtectedRangeEnable[IBECC_REGIONS_MAX];
  UINT32  IbeccProtectedRangeBase[IBECC_REGIONS_MAX];
  UINT32  IbeccProtectedRangeMask[IBECC_REGIONS_MAX];
  UINT8   EccDftEn;
  UINT8   Write0;

  UINT8   SaGv;
  UINT16  SaGvFreq[4];
  UINT8   SaGvGear[4];
  UINT8   GearRatio;
  //
  // MRC Training Algorithms
  //
  UINT8   ECT;
  UINT8   SOT;
  UINT8   ERDMPRTC2D;
  UINT8   RDMPRT;
  UINT8   RCVET;
  UINT8   JWRL;
  UINT8   EWRTC2D;
  UINT8   ERDTC2D;
  UINT8   WRTC1D;
  UINT8   WRVC1D;
  UINT8   RDTC1D;
  UINT8   DIMMODTT;
  UINT8   DIMMRONT;
  UINT8   WRDSEQT;
  UINT8   WRSRT;
  UINT8   RDODTT;
  UINT8   RDEQT;
  UINT8   RDAPT;
  UINT8   WRTC2D;
  UINT8   RDTC2D;
  UINT8   CMDVC;
  UINT8   WRVC2D;
  UINT8   RDVC2D;
  UINT8   LCT;
  UINT8   RTL;
  UINT8   TAT;
  UINT8   RMT;
  UINT8   MEMTST;
  UINT8   ALIASCHK;
  UINT8   RCVENC1D;
  UINT8   RMC;
  UINT8   WRDSUDT;

  //
  // Power and Thermal Throttling Options
  //
  UINT8   EnablePwrDn;
  UINT8   EnablePwrDnLpddr;
  UINT8   Refresh2X;
  UINT8   LockPTMregs;

  UINT8   SrefCfgEna;
  UINT16  SrefCfgIdleTmr;
  UINT8   ThrtCkeMinDefeat;
  UINT8   ThrtCkeMinTmr;
  UINT8   ThrtCkeMinDefeatLpddr;
  UINT8   ThrtCkeMinTmrLpddr;
  UINT8   AllowOppRefBelowWriteThrehold;
  UINT8   WriteThreshold;

  UINT8   MemoryThermalManagement;
  UINT8   PeciInjectedTemp;
  UINT8   ExttsViaTsOnBoard;
  UINT8   ExttsViaTsOnDimm;
  UINT8   VirtualTempSensor;
  UINT8   ScramblerSupport;
  UINT8   ForceColdReset;
  UINT8   DisableMc0Ch0;
  UINT8   DisableMc0Ch1;
  UINT8   DisableMc0Ch2;
  UINT8   DisableMc0Ch3;
  UINT8   DisableMc1Ch0;
  UINT8   DisableMc1Ch1;
  UINT8   DisableMc1Ch2;
  UINT8   DisableMc1Ch3;
  UINT8   RemapEnable;
  UINT8   MrcSafeConfig;
  UINT8   LpDdrDqDqsReTraining;
  UINT8   RhSelect;
  UINT8   Lfsr0Mask;
  UINT8   Lfsr1Mask;
  UINT8   McRefresh2X;    // Deprecated from revision 26, use McRefreshRate instead
  UINT8   RefreshPanicWm; // Deprecated from revision 22, use RefreshWm instead
  UINT8   RefreshHpWm;    // Deprecated from revision 22, use RefreshWm instead
  UINT8   MaxRttWr;
  UINT8   ExitOnFailure;
  UINT8   NewFeatureEnable1;
  UINT8   NewFeatureEnable2;
  UINT8   RDVC1D;
  UINT8   TXTCO;
  UINT8   TXTCODQS;
  UINT8   VCCDLLBP;
  UINT8   CLKTCO;
  UINT8   CMDDSEQ;
  UINT8   CMDSR;
  UINT8   CMDDRUD;
  UINT8   DIMMODTCA;
  UINT8   PVTTDNLP;
  UINT8   RDVREFDC;
  UINT8   VDDQT;
  UINT8   DCC;
  UINT8   RMTBIT;
  UINT8   SkipExtGfxScan;
  //
  // CRID
  //
  UINT8   CridEnable;

  UINT8   SlateIndicatorRT;
  UINT8   SlateIndicatorSx;
  UINT8   DockIndicatorRT;
  UINT8   DockIndicatorSx;
  UINT8   IuerButtonEnable;
  UINT8   IuerConvertibleEnable;
  UINT8   IuerDockEnable;

  //
  // Uncore Related
  //
  UINT16  UncoreVoltageOffset;
  UINT8   UncoreVoltageOffsetPrefix;

  //
  // GT related
  //
  UINT8   GtMaxOcRatio;                 ///< Maximum GT turbo ratio override
  UINT8   GtVoltageMode;                ///< Specifies whether GT voltage is operating in Adaptive or Override mode - 0: Adaptive, 1: Override
  UINT16  GtVoltageOffset;              ///< The voltage offset applied to GT. Valid range from -1000mv to 1000mv
  UINT8   GtVoltageOffsetPrefix;
  UINT16  GtVoltageOverride;            ///< The GT voltage override which is applied to the entire range of GT frequencies
  UINT16  GtExtraTurboVoltage;          ///< The adaptive voltage applied during turbo frequencies. Valid range from 0 to 2000mV
  UINT8   GtusMaxOcRatio;                ///< Maximum GT turbo ratio override
  UINT8   GtusVoltageMode;               ///< Specifies whether GT unslice voltage is operating in Adaptive or Override mode - 0: Adaptive, 1: Override
  UINT16  GtusVoltageOffset;             ///< The voltage offset applied to GT unslice. Valid range from -1000mv to 1000mv
  UINT8   GtusVoltageOffsetPrefix;
  UINT16  GtusVoltageOverride;           ///< The GT unslice voltage override which is applied to the entire range of GT frequencies
  UINT16  GtusExtraTurboVoltage;         ///< The adaptive voltage applied during turbo frequencies. Valid range from 0 to 2000mV

  UINT8   RealtimeMemoryTiming;         ///< Realtime memory timing will allow system to perform realtime memory timing changes.
  UINT8   BclkChangeWarmReset;
  UINT8   BclkChangeContinues;

  UINT8   WrcFeatureEnable;

  //
  // PCI_EXPRESS_CONFIG, MAX  root ports
  //
  UINT8   PcieRootPortEn[CPU_PCIE_MAX_ROOT_PORTS];
  UINT8   PcieLaneCm[SA_PEG_MAX_LANE];
  UINT8   PcieLaneCp[SA_PEG_MAX_LANE];
  UINT8   PcieFiaProgramming;
  UINT8   PcieRootPortClockGating[CPU_PCIE_MAX_ROOT_PORTS];
  UINT8   PcieRootPortPowerGating[CPU_PCIE_MAX_ROOT_PORTS];
  UINT8   PcieComplianceTestMode;
  UINT8   RpFunctionSwap; //@deprecated
  UINT8   PegGen3RootPortPreset[SA_PEG_MAX_LANE];
  UINT8   PegGen3EndPointPreset[SA_PEG_MAX_LANE];
  UINT8   PegGen3EndPointHint[SA_PEG_MAX_LANE];
  UINT8   PegGen4RootPortPreset[SA_PEG_MAX_LANE];
  UINT8   PegGen4EndPointPreset[SA_PEG_MAX_LANE];
  UINT8   PegGen4EndPointHint[SA_PEG_MAX_LANE];
  UINT8   PcieRootPortLinkDownGpios;
  UINT8   PcieRootPortClockReqMsgEnable[CPU_PCIE_MAX_ROOT_PORTS];
  UINT8   PcieSlotSelection;
  UINT8   PcieRootPortHPE[CPU_PCIE_MAX_ROOT_PORTS];
  UINT8   PcieRootPortSI[CPU_PCIE_MAX_ROOT_PORTS];
  UINT8   PcieRootPortPMCE[CPU_PCIE_MAX_ROOT_PORTS];
  UINT8   PcieRootPortTHS[CPU_PCIE_MAX_ROOT_PORTS];
  UINT8   PcieRootPortACS[CPU_PCIE_MAX_ROOT_PORTS];
  UINT8   PcieRootPortAER[CPU_PCIE_MAX_ROOT_PORTS];
  UINT8   PcieRootPortURE[CPU_PCIE_MAX_ROOT_PORTS];
  UINT8   PcieRootPortFEE[CPU_PCIE_MAX_ROOT_PORTS];
  UINT8   PcieRootPortNFE[CPU_PCIE_MAX_ROOT_PORTS];
  UINT8   PcieRootPortCEE[CPU_PCIE_MAX_ROOT_PORTS];
  UINT8   PcieRootPortSFE[CPU_PCIE_MAX_ROOT_PORTS];
  UINT8   PcieRootPortSNE[CPU_PCIE_MAX_ROOT_PORTS];
  UINT8   PcieRootPortSCE[CPU_PCIE_MAX_ROOT_PORTS];
  UINT8   PcieRootPortDPC[CPU_PCIE_MAX_ROOT_PORTS];
  UINT8   PcieRootPortEDPC[CPU_PCIE_MAX_ROOT_PORTS];
  UINT8   PcieRootPortSpeed[CPU_PCIE_MAX_ROOT_PORTS];
  UINT8   PcieRootPortEqPh3Method[CPU_PCIE_MAX_ROOT_PORTS];
  UINT8   PcieRootPortGen4EqPh3Method[CPU_PCIE_MAX_ROOT_PORTS];
  UINT8   PcieRootPortGen5EqPh3Method[CPU_PCIE_MAX_ROOT_PORTS];
  UINT8   PcieRootPortAspm[CPU_PCIE_MAX_ROOT_PORTS];
  UINT8   PcieRootPortL1SubStates[CPU_PCIE_MAX_ROOT_PORTS];
  UINT8   PcieRootPortGen3Uptp[CPU_PCIE_MAX_ROOT_PORTS];
  UINT8   PcieRootPortGen3Dptp[CPU_PCIE_MAX_ROOT_PORTS];
  UINT8   PcieRootPortGen4Uptp[CPU_PCIE_MAX_ROOT_PORTS];
  UINT8   PcieRootPortGen4Dptp[CPU_PCIE_MAX_ROOT_PORTS];
  UINT8   PcieRootPortGen5Uptp[CPU_PCIE_MAX_ROOT_PORTS];
  UINT8   PcieRootPortGen5Dptp[CPU_PCIE_MAX_ROOT_PORTS];
  UINT8   PcieRootPortPTM[CPU_PCIE_MAX_ROOT_PORTS];
  UINT16  PcieDetectTimeoutMs[CPU_PCIE_MAX_ROOT_PORTS];
  UINT8   PcieRootPortMultiVc[CPU_PCIE_MAX_ROOT_PORTS];
  UINT8   PcieRootPortMultiVcSupported[CPU_PCIE_MAX_ROOT_PORTS];
  //
  // PCIe LTR Configuration
  //
  UINT8   CpuPcieLtrEnable[CPU_PCIE_MAX_ROOT_PORTS];
  UINT8   CpuPcieLtrConfigLock[CPU_PCIE_MAX_ROOT_PORTS];
  UINT8   CpuPcieSnoopLatencyOverrideMode[CPU_PCIE_MAX_ROOT_PORTS];
  UINT8   CpuPcieSnoopLatencyOverrideMultiplier[CPU_PCIE_MAX_ROOT_PORTS];
  UINT16  CpuPcieSnoopLatencyOverrideValue[CPU_PCIE_MAX_ROOT_PORTS];
  UINT8   CpuPcieNonSnoopLatencyOverrideMode[CPU_PCIE_MAX_ROOT_PORTS];
  UINT8   CpuPcieNonSnoopLatencyOverrideMultiplier[CPU_PCIE_MAX_ROOT_PORTS];
  UINT16  CpuPcieNonSnoopLatencyOverrideValue[CPU_PCIE_MAX_ROOT_PORTS];
  UINT8   CpuPcieForceLtrOverride[CPU_PCIE_MAX_ROOT_PORTS];

  //
  // FUSA
  //
  UINT8   DisplayFusaConfigEnable;
  UINT8   GraphicFusaConfigEnable;
  UINT8   OpioFusaConfigEnable;

  UINT8   GtClosEnable;

  UINT8   CpuPcieFomsCp[CPU_PCIE_MAX_ROOT_PORTS];
  UINT8   DpmemSupport;
  UINT8   CpuPcieRootPortPeerToPeerMode[CPU_PCIE_MAX_ROOT_PORTS];
  UINT8   VmdGlobalMapping;
  //Tc C-State limit
  UINT8   TcStateLimit;

  // Adding support for TCSS CPU XHCI Port Disable Override
  UINT8   CpuUsbPdoProgramming;
  UINT8   CpuUsbPortDisable;
  UINT8   CpuUsbSsPort[MAX_TCSS_USB3_PORTS];

  UINT8   DisplayAudioLink;

  // PPR Support
  UINT8   PprEnable;
  UINT8   MarginLimitCheck;
  UINT16  MarginLimitL2;

  UINT8   LpddrRttWr;
  UINT8   LpddrRttCa;

  UINT16  MemoryVddqVoltage;
  UINT16  MemoryVppVoltage;
  UINT8   PcieRootPortNewFom[CPU_PCIE_MAX_ROOT_PORTS];
  UINT8   DmiNewFom;
  UINT8   DmiCdrRelock;
  UINT8   PcieRootPortCdrRelock[CPU_PCIE_MAX_ROOT_PORTS];
  UINT8   DIMMDFE;              // MRC: DIMM DFE training
  UINT8   ExtendedBankHashing;
  UINT8   IbeccErrInjControl;
  UINT64  IbeccErrInjAddress;
  UINT64  IbeccErrInjMask;
  UINT32  IbeccErrInjCount;
  UINT8   DynamicMemoryBoost;
  UINT8   HgSupport;

  //FUSA
  UINT8   FusaConfigEnable;
  UINT8   FusaRunStartupArrayBist;
  UINT8   FusaRunStartupScanBist;
  UINT8   FusaRunPeriodicScanBist;
  UINT8   Module0Lockstep;
  UINT8   Module1Lockstep;
  UINT8   RefreshWm;
  UINT8   PcieFunc0LinkDisable [CPU_PCIE_MAX_ROOT_PORTS];
  UINT32  DebugValue;
  UINT8   PcieResizableBarSupport;
  UINT8   McRefreshRate;
  UINT8   RealtimeMemoryFrequency;
  UINT8   PeriodicDcc;
  UINT8   LpMode;
  UINT8   TcColdPowerSavingFactorSwitch;
  UINT8   IomStayInTCColdSeconds;
  UINT8   IomBeforeEnteringTCColdSeconds;
  UINT8   TXDQSDCC; //MRC: TX DQS DCC Training
  UINT16  tRFCpb;
  UINT16  tRFC2;
  UINT16  tRFC4;
  UINT8   tRRD_L;
  UINT8   tRRD_S;
  UINT8   tWTR_L;
  UINT8   tCCD_L;
  UINT8   tWTR_S;
  UINT16  tRFCpbDefault;
  UINT16  tRFC2Default;
  UINT16  tRFC4Default;
  UINT8   tRRD_LDefault;
  UINT8   tRRD_SDefault;
  UINT8   tWTR_LDefault;
  UINT8   tCCD_LDefault;
  UINT8   tWTR_SDefault;
  UINT64  EccErrInjAddress;
  UINT64  EccErrInjMask;
  UINT32  EccErrInjCount;
  UINT16  FreqLimitMixedConfig;
  UINT8   FirstDimmBitMask;
  UINT8   SagvSwitchFactorIA;
  UINT8   SagvSwitchFactorGT;
  UINT8   SagvSwitchFactorIO;
  UINT8   SagvSwitchFactorStall;
  UINT8   SagvHeuristicsUpControl;
  UINT8   SagvHeuristicsDownControl;
  UINT8   DRAMDCA;              // MRC: DRAM DCA training
  UINT8   DynamicMemoryBoostTrainingFailed;
  UINT16  FreqLimitMixedConfig_1R1R_8GB;
  UINT16  FreqLimitMixedConfig_1R1R_16GB;
  UINT16  FreqLimitMixedConfig_1R1R_8GB_16GB;
  UINT16  FreqLimitMixedConfig_2R2R;
  UINT16  LctCmdEyeWidth;
  UINT8   FirstDimmBitMaskEcc;
  UINT8   Lp5BankMode; // MRC: LP5 Bank Mode
  UINT8   WRDS;
  UINT8   EARLYDIMMDFE;            // MRC: EARLY DIMM DFE training
} SA_SETUP;

/**
 Making any setup structure change after code frozen
 will need to maintain backward compatibility, bump up
 structure revision and update below history table\n

  <b>Revision 1</b>:  - Initial version.
  <b>Revision 2</b>:  - Added PchXhciUaol to control UAOL feature enable status
  <b>Revision 3</b>:  - Added Foxville LAN support
  <b>Revision 4</b>:  - Deprecated DciModphyPg
  <b>Revision 5</b>:  - Added Serial Io Debug Uart Power Gating support for Kernel Debug
  <b>Revision 6</b>:  - Added CpuRootportUsedForHybridStorage and PchRootportUsedForCpuAttach to control Hybrid storage when it iss used with cpu root ports
  <b>Revision 7</b>:  - Added FUSA IOP config support
  <b>Revision 8</b>:  - Added AcpiL6dPmeHandling for _L6D. BIOS-ACPI can verify PMEENABLE and PMESTATUS of each device that requires GPE related wake
  <b>Revision 9</b>:  - Added CpuHybridStorageConnected to store the status of the hybrid device connected to CPU PCIE slot.
  <b>Revision 10</b>:  - Added PchScsEmmcEnabled, PchScsEmmcHs400Enabled, PchScsEmmcHs400DriverStrength, PchScsEmmcHs400SoftwareTuning for eMMC.
  <b>Revision 11</b>:  - Added PchSkipVccInConfig.
  <b>Revision 12</b>:  - Added PchHdaAlc245DmicConfiguration.
  <b>Revision 13</b>:  - Added Hid Over Spi support for THC.
  <b>Revision 14</b>:  - Add PchXhciHsiiEnable.
**/

#define PCH_SETUP_REVISION  14
typedef struct {
  UINT8   Revision;
  UINT8   ExternalDma;
  UINT16  RootPortDid;              // not a SETUP item, used by BIOS to pass Root Port DID to SETUP items

  UINT8   DeepSxMode;
  UINT8   LanWakeFromDeepSx;
  UINT8   DisableDsxAcPresentPulldown;
  UINT8   PmcReadDisable;
  UINT8   OsDebugPort;
  UINT8   PchLan;
  UINT8   DciUsb3TypecUfpDbg;
  UINT8   PchEnableDbcObs;
  UINT8   PchWakeOnLan;
  UINT8   SlpLanLowDc;
  UINT8   PchWakeOnWlan;
  UINT8   PchWakeOnWlanDeepSx;
  UINT8   PchEnergyReport;
  UINT8   EnableTcoTimer;
  UINT8   PchCrid;
  UINT8   PmcLpmS0i2p0En;
  UINT8   PmcLpmS0i2p1En;
  UINT8   PmcLpmS0i2p2En;
  UINT8   PmcLpmS0i3p0En;
  UINT8   PmcLpmS0i3p1En;
  UINT8   PmcLpmS0i3p2En;
  UINT8   PmcLpmS0i3p3En;
  UINT8   PmcLpmS0i3p4En;

  UINT8   PchRtcMemoryLock;
  UINT8   PchBiosLock;
  UINT8   UnlockGpioPads;
  UINT8   PsOnEnable;

  UINT8   Hpet;
  UINT8   StateAfterG3;
  UINT8   IchPort80Route;
  UINT8   EnhancePort8xhDecoding;
  UINT8   PciePllSsc;
  UINT8   WdtEnable;
  UINT8   PchS0ixAutoDemotion;
  UINT8   PchLatchEventsC10Exit;
  //
  // Usb Config
  //
  UINT8   PchUsbPortDisable;
  UINT8   PchUsbHsPort[SETUP_MAX_USB2_PORTS];
  UINT8   PchUsbSsPort[SETUP_MAX_USB3_PORTS];
  UINT8   PchUsbPdoProgramming;
  UINT8   PchUsbOverCurrentEnable;
  UINT8   PchXhciOcLock;
  UINT8   PchXhciAcLock;
  UINT8   PchUsb2SusWellPgEnable;

  //
  // xDCI Config
  //
  UINT8   PchXdciSupport;

  //
  // Sata CONFIG
  //
  UINT8   PchSata;
  UINT8   SataInterfaceMode;
  UINT8   SataPort[PCH_MAX_SATA_PORTS];
  UINT8   SataHotPlug[PCH_MAX_SATA_PORTS];
  UINT8   SataMechanicalSw[PCH_MAX_SATA_PORTS];
  UINT8   SataSpinUp[PCH_MAX_SATA_PORTS];
  UINT8   SataExternal[PCH_MAX_SATA_PORTS];
  UINT8   SataType[PCH_MAX_SATA_PORTS];
  UINT8   SataTopology[PCH_MAX_SATA_PORTS];
  UINT8   SataRaidR0;
  UINT8   SataRaidR1;
  UINT8   SataRaidR10;
  UINT8   SataRaidR5;
  UINT8   SataRaidIrrt;
  UINT8   SataRaidOub;
  UINT8   SataHddlk;
  UINT8   SataLedl;
  UINT8   SataRaidIooe;
  UINT8   SataRaidSrt;
  UINT8   SataRaidOromDelay;
  UINT8   SataRstForceForm;
  UINT8   SataLegacyOrom;
  UINT8   SataRstOptaneMemory;
  UINT8   SataRstCpuAttachedStorage;
  UINT8   RaidDeviceId;
  UINT8   SataSalp;
  UINT8   SataTestMode;
  UINT8   PxDevSlp[PCH_MAX_SATA_PORTS];
  UINT8   EnableDitoConfig[PCH_MAX_SATA_PORTS];
  UINT16  DitoVal[PCH_MAX_SATA_PORTS];
  UINT8   DmVal[PCH_MAX_SATA_PORTS];

  //
  // PCI_EXPRESS_CONFIG, MAX 16 root ports
  //
  UINT8   PcieRootPort8xhDecode;
  UINT8   Pcie8xhDecodePortIndex;
  UINT8   PcieRootPortPeerMemoryWriteEnable[PCH_MAX_PCIE_ROOT_PORTS];
  UINT8   PcieComplianceTestMode;

  // @TODO:
  //  "GetPchMaxPcieClockNum(18)" is for ADL-S specific, and the value is larger than PCH_MAX_PCIE_CLOCKS(16)
  //  For long term, we need to have a good way to deal with the condition.
  UINT8   PchPcieClockUsageOverride[18];
  UINT8   PchPcieClkReqSupport[18];

  UINT8   PcieRootPortEn[PCH_MAX_PCIE_ROOT_PORTS];
  UINT8   PcieRootPortAspm[PCH_MAX_PCIE_ROOT_PORTS];
  UINT8   PcieRootPortURE[PCH_MAX_PCIE_ROOT_PORTS];
  UINT8   PcieRootPortFEE[PCH_MAX_PCIE_ROOT_PORTS];
  UINT8   PcieRootPortNFE[PCH_MAX_PCIE_ROOT_PORTS];
  UINT8   PcieRootPortCEE[PCH_MAX_PCIE_ROOT_PORTS];
  UINT8   PcieRootPortCTD[PCH_MAX_PCIE_ROOT_PORTS];
  UINT8   PcieRootPortPIE[PCH_MAX_PCIE_ROOT_PORTS];
  UINT8   PcieRootPortSFE[PCH_MAX_PCIE_ROOT_PORTS];
  UINT8   PcieRootPortSNE[PCH_MAX_PCIE_ROOT_PORTS];
  UINT8   PcieRootPortSCE[PCH_MAX_PCIE_ROOT_PORTS];
  UINT8   PcieRootPortPMCE[PCH_MAX_PCIE_ROOT_PORTS];
  UINT8   PcieRootPortHPE[PCH_MAX_PCIE_ROOT_PORTS];
  UINT8   PcieRootPortAER[PCH_MAX_PCIE_ROOT_PORTS];
  UINT8   PcieRootPortSpeed[PCH_MAX_PCIE_ROOT_PORTS];
  UINT8   PcieRootPortTHS[PCH_MAX_PCIE_ROOT_PORTS];
  UINT8   PcieRootPortL1SubStates[PCH_MAX_PCIE_ROOT_PORTS];
  UINT8   PcieRootPortL1Low[PCH_MAX_PCIE_ROOT_PORTS];
  UINT8   PcieRootPortACS[PCH_MAX_PCIE_ROOT_PORTS];
  UINT8   PcieRootPortPTM[PCH_MAX_PCIE_ROOT_PORTS];
  UINT8   PcieRootPortDPC[PCH_MAX_PCIE_ROOT_PORTS];
  UINT8   PcieRootPortEDPC[PCH_MAX_PCIE_ROOT_PORTS];
  UINT8   PcieRootPortSI[PCH_MAX_PCIE_ROOT_PORTS];
  UINT16  PcieDetectTimeoutMs[PCH_MAX_PCIE_ROOT_PORTS];
  UINT8   RpFunctionSwap;

  //
  // PCIe EQ
  //
  UINT8   PcieEqOverrideDefault;
  UINT8   PcieEqMethod;
  UINT8   PcieEqMode;
  UINT8   PcieEqPh2LocalTransmitterOverrideEnable;
  UINT8   PcieEqPh1DownstreamPortTransmitterPreset;
  UINT8   PcieEqPh1UpstreamPortTransmitterPreset;
  UINT8   PcieEqPh3NumberOfPresetsOrCoefficients;
  UINT8   PcieEqPh3PreCursorList[10];
  UINT8   PcieEqPh3PostCursorList[10];
  UINT8   PcieEqPh3PresetList[11];
  UINT8   PcieEqPh2LocalTransmitterOverridePreset;

  //
  // PCIe LTR Configuration
  //
  UINT8   PchPcieLtrEnable[PCH_MAX_PCIE_ROOT_PORTS];
  UINT8   PchPcieLtrConfigLock[PCH_MAX_PCIE_ROOT_PORTS];
  UINT8   PchPcieSnoopLatencyOverrideMode[PCH_MAX_PCIE_ROOT_PORTS];
  UINT8   PchPcieSnoopLatencyOverrideMultiplier[PCH_MAX_PCIE_ROOT_PORTS];
  UINT8   PchPcieNonSnoopLatencyOverrideMode[PCH_MAX_PCIE_ROOT_PORTS];
  UINT8   PchPcieNonSnoopLatencyOverrideMultiplier[PCH_MAX_PCIE_ROOT_PORTS];
  UINT16  PchPcieSnoopLatencyOverrideValue[PCH_MAX_PCIE_ROOT_PORTS];
  UINT16  PchPcieNonSnoopLatencyOverrideValue[PCH_MAX_PCIE_ROOT_PORTS];
  UINT8   PchPcieForceLtrOverride[PCH_MAX_PCIE_ROOT_PORTS];

  //
  // DMI Configuration
  //
  UINT8   PchDmiAspm;

  //
  // THC Configuration
  //
  UINT8   ThcPort0Assignment;
  UINT8   ThcPort0Clock;
  UINT8   ThcPort0WakeOnTouch;
  UINT8   ThcPort1Assignment;
  UINT8   ThcPort1Clock;
  UINT8   ThcPort1WakeOnTouch;

  //
  // IEH Configuration
  //
  UINT8  IehMode;

  //
  // PCI Bridge Resources
  //
  UINT8   PcieExtraBusRsvd[PCH_MAX_PCIE_ROOT_PORTS];
  UINT16  PcieMemRsvd[PCH_MAX_PCIE_ROOT_PORTS];
  UINT8   PcieIoRsvd[PCH_MAX_PCIE_ROOT_PORTS];

  //
  // HD-Audio Configuration
  //
  UINT8   PchHdAudio;
  UINT8   PchHdAudioDsp;
  UINT8   PchHdAudioDspUaaCompliance;
  UINT8   PchHdaIDisplayCodecDisconnect;
  UINT8   PchHdAudioPme;
  UINT8   PchHdAudioHdaLinkEnable;
  UINT8   PchHdAudioDmicLinkEnable[PCH_MAX_HDA_DMIC_LINK_NUM];
  UINT8   PchHdAudioDmicClockSelect[PCH_MAX_HDA_DMIC_LINK_NUM];
  UINT8   PchHdAudioSndwLinkEnable[PCH_MAX_HDA_SNDW_LINK_NUM];
  UINT8   PchHdAudioSspLinkEnable[PCH_MAX_HDA_SSP_LINK_NUM];
  UINT8   PchHdaHdAudioLinkFreq;
  UINT8   PchHdaIDisplayLinkFreq;
  UINT8   PchHdaIDisplayLinkTmode;
  UINT8   PchHdAudioCodecSxWakeCapability;
  UINT8   PchHdaAutonomousClockStopSndw[PCH_MAX_HDA_SNDW_LINK_NUM];
  UINT8   PchHdaDataOnActiveIntervalSelectSndw[PCH_MAX_HDA_SNDW_LINK_NUM];
  UINT8   PchHdaDataOnDelaySelectSndw[PCH_MAX_HDA_SNDW_LINK_NUM];
  UINT8   PchHdAudioI2sCodecSelect;
  UINT8   PchHdAudioI2sCodecBusNumber;
  UINT8   PchHdAudioFeature[HDAUDIO_FEATURES];
  UINT8   PchHdAudioPostProcessingMod[HDAUDIO_PP_MODULES];
  CHAR16  PchHdAudioPostProcessingModCustomGuid1[GUID_CHARS_NUMBER];
  CHAR16  PchHdAudioPostProcessingModCustomGuid2[GUID_CHARS_NUMBER];
  CHAR16  PchHdAudioPostProcessingModCustomGuid3[GUID_CHARS_NUMBER];

  //
  // Interrupt Configuration
  //
  UINT8   PchIoApic24119Entries;
  UINT8   Enable8254ClockGating;

  //
  // PCH Thermal Throttling
  //
  UINT8   PchCrossThrottling;
  //
  // SCS Configuration
  //
  UINT8   PchScsUfsEnable[PCH_MAX_UFS_DEV_NUM];

  //
  // Integrated Sensor Hub (ISH) configuration
  //
  UINT8   PchIshEnable;
  UINT8   PchIshSpiEnable[PCH_MAX_ISH_SPI_CONTROLLERS];
  UINT8   PchIshSpiCs0Enable[PCH_MAX_ISH_SPI_CONTROLLERS];
  UINT8   PchIshUartEnable[PCH_MAX_ISH_UART_CONTROLLERS];
  UINT8   PchIshI2cEnable[PCH_MAX_ISH_I2C_CONTROLLERS];
  UINT8   PchIshGpEnable[PCH_MAX_ISH_GP_PINS];
  UINT8   PchIshPdtUnlock;

  //
  // Pch Serial Io configuration
  //
  UINT8   PchSpiCsPolarity[14]; // [PCH_MAX_SERIALIO_SPI_CONTROLLERS * PCH_MAX_SERIALIO_SPI_CHIP_SELECTS]
  UINT8   PchSerialIoSpi[PCH_MAX_SERIALIO_SPI_CONTROLLERS];
  UINT8   PchSpiCsPolaritySelect[PCH_MAX_SERIALIO_SPI_CHIP_SELECTS];
  UINT8   PchSerialIoUart[PCH_MAX_SERIALIO_UART_CONTROLLERS];
  UINT8   PchUartDmaEnable[PCH_MAX_SERIALIO_UART_CONTROLLERS];
  UINT8   PchUartHwFlowCtrl[PCH_MAX_SERIALIO_UART_CONTROLLERS];
  UINT8   PchUartPowerGating[PCH_MAX_SERIALIO_UART_CONTROLLERS];
  UINT8   PchSerialIoI2c[PCH_MAX_SERIALIO_I2C_CONTROLLERS];
  UINT8   PchSerialIoDeviceI2C4Support;
  UINT8   PchGpio;
  UINT8   PchGpioIrqRoute;
  UINT16  PchGpioDebounce;
  UINT8   PchGpioTestDevices;
  UINT8   PchAdditionalSerialIoDevices;
  //
  // TraceHub Setup Options
  //
  UINT8   PchTraceHubMode;
  UINT8   PchTraceHubMemReg0Size;
  UINT8   PchTraceHubMemReg1Size;
  //
  // Thermal Throttling Level options
  //
  UINT8   PchTtLevelSuggestSet;
  UINT16  PchThrmT0Level;
  UINT16  PchThrmT1Level;
  UINT16  PchThrmT2Level;
  UINT8   PchThrmTtEnable;
  UINT8   PchThrmTtState13Enable;
  UINT8   PchThrmTtLock;

  //
  // DMI Thermal Throttling
  //
  UINT8   PchDmiTsSuggestSet;
  UINT8   PchTs0Width;
  UINT8   PchTs1Width;
  UINT8   PchTs2Width;
  UINT8   PchTs3Width;
  UINT8   PchDmiTsawEn;
  //
  // SATA Thermal Throttling
  //
  UINT8   PchSataTsSuggestSet;
  UINT8   PchP0T1M;
  UINT8   PchP0T2M;
  UINT8   PchP0T3M;
  UINT8   PchP0TDisp;
  UINT8   PchP0Tinact;
  UINT8   PchP0TDispFinit;
  UINT8   PchP1T1M;
  UINT8   PchP1T2M;
  UINT8   PchP1T3M;
  UINT8   PchP1TDisp;
  UINT8   PchP1Tinact;
  UINT8   PchP1TDispFinit;
  //
  // CNVi
  //
  UINT8   CnviMode;
  UINT8   CnviWifiCore;
  UINT8   CnviBtCore;
  UINT8   CnviBtAudioOffload;
  UINT8   CnviDdrRfim;
  //
  // eSPI Setup Options
  //
  UINT8   PchEspiLgmrEnable;
  UINT8   PchEspiHostC10ReportEnable;

  //
  // SMBUS
  //
  UINT8   SmbusSpdWriteDisable;
  //
  // P2SB
  //
  UINT8   PchSidebandLock;
  //
  // Flash Protection configuration
  //
  UINT8   FprrEnable;

  //
  // Debug Panel
  //
  UINT8   DciEn;
  UINT8   DciDbcMode;
  UINT8   DciModphyPg;      // @deprecated - This policy is deprecated.
  UINT8   PmcDbgMsgEn;

  //Hammock Harbor
  UINT8   EnableTimedGpio0;
  UINT8   EnableTimedGpio1;

  //
  // FIVR Configuration;
  //
  UINT8   ExternalV1p05StateEnable[FIVR_RAIL_S0IX_SX_STATES_MAX];
  UINT8   ExternalVnnStateEnable[FIVR_RAIL_S0IX_SX_STATES_MAX];
  UINT8   ExternalVnnSxStateEnable[FIVR_RAIL_S0IX_SX_STATES_MAX];
  UINT8   ExternalV1p05SupportedVoltageStates[FIVR_RAIL_EXT_VOLTAGE_STATES_MAX];
  UINT8   ExternalVnnSupportedVoltageStates[FIVR_RAIL_EXT_VOLTAGE_STATES_MAX];
  UINT8   ExternalVnnSupportedVoltageConfigurations;

  UINT8   ExternalV1p05IccMax;    // @deprecated - This policy is deprecated, please use ExternalV1p05IccMaximum instead
  UINT16  ExternalV1p05Voltage;
  UINT8   ExternalVnnIccMax;      // @deprecated - This policy is deprecated, please use ExternalVnnIccMaximum instead
  UINT16  ExternalVnnVoltage;
  UINT8   ExternalVnnSxIccMax;    // @deprecated - This policy is deprecated, please use ExternalVnnSxIccMaximum instead
  UINT16  ExternalVnnSxVoltage;

  UINT8   PchRetToLowCurModeVolTranTime;
  UINT8   PchRetToHighCurModeVolTranTime;
  UINT8   PchLowToHighCurModeVolTranTime;
  UINT16  PchOffToHighCurModeVolTranTime;
  UINT8   PchFivrDynPm;

  //
  // OS IDLE MODE
  //
  UINT8   PchPmcOsIdleModeEnable;
  UINT8   PchLegacyIoLowLatency;

  //
  // PSF
  //
  UINT8   PsfTccEnable;

  //
  // TSN
  //
  UINT8   PchTsnEnable;
  UINT8   PchTsnLinkSpeed;
  UINT8   TsnPcsEnabled;

  // FUSA
  //
  UINT8   PsfFusaConfigEnable;

  //
  // Hybrid Storage
  //
  UINT8   HybridStorageMode;

  UINT8   TsnMultiVcEnable;

  UINT16  ExternalV1p05IccMaximum;
  UINT16  ExternalVnnIccMaximum;
  UINT16  ExternalVnnSxIccMaximum;
  UINT8   PmcC10DynamicThresholdAdjustment;

  // Extended BIOS Decode Range
  UINT8   ExtendedBiosDecodeRangeEnable;

  //
  // PMC configuration
  //
  UINT8   PmcAdrEn;
  UINT8   PmcAdrTimer1Val;
  UINT8   PmcAdrMultiplier1Val;
  UINT8   PmcAdrHostPartitionReset;

  //
  // UAOL setup option
  //
  UINT8   PchXhciUaol;

  //
  // Foxville LAN Config
  //
  UINT8   FoxvilleLanSupport;
  UINT8   FoxvilleWakeOnLan;
  //
  // Serial IO UART - Kernel support
  //
  UINT8   SerialIoDebugUartPowerGating;

  //
  // Support for two lanes of Hybrid Storage connected to CPU
  //
  UINT8   CpuRootportUsedForHybridStorage;
  UINT8   PchRootportUsedForCpuAttach;

  //
  // FUSA IOP config enable option
  //
  UINT8   IopFusaConfigEnable;
  //
  // ACPI L6D Handler option
  //
  UINT8   AcpiL6dPmeHandling;
  UINT8   CpuHybridStorageConnected;
  //
  // EMCC configurationn
  //
  UINT8   PchScsEmmcEnabled;
  UINT8   PchScsEmmcHs400Enabled;
  UINT8   PchScsEmmcHs400SoftwareTuning;
  UINT8   PchScsEmmcHs400DriverStrength;

  UINT8   PchSkipVccInConfig;
  UINT8   PchHdaAlc245DmicConfiguration;

  //
  // THC HidOver SPi
  //
  UINT8   ThcMode[PCH_MAX_THC_CONTROLLERS];
  UINT32  ThcActiveLtr[PCH_MAX_THC_CONTROLLERS];
  UINT32  ThcIdleLtr[PCH_MAX_THC_CONTROLLERS];
  UINT32  ThcLimitPacketSize[PCH_MAX_THC_CONTROLLERS];
  UINT32  ThcPerformanceLimitation[PCH_MAX_THC_CONTROLLERS];
  UINT32  ThcHidFlags[PCH_MAX_THC_CONTROLLERS];
  UINT32  ThcHidConnectionSpeed[PCH_MAX_THC_CONTROLLERS];
  UINT32  ThcHidInputReportBodyAddress[PCH_MAX_THC_CONTROLLERS];
  UINT32  ThcHidInputReportHeaderAddress[PCH_MAX_THC_CONTROLLERS];
  UINT32  ThcHidOutputReportAddress[PCH_MAX_THC_CONTROLLERS];
  UINT32  ThcHidReadOpcode[PCH_MAX_THC_CONTROLLERS];
  UINT32  ThcHidWriteOpcode[PCH_MAX_THC_CONTROLLERS];
  UINT32  ThcHidResetPadTrigger[PCH_MAX_THC_CONTROLLERS];

  UINT8   PchXhciHsiiEnable;
} PCH_SETUP;


/**
  Making any setup structure change after code frozen
  will need to maintain backward compatibility, bump up
  structure revision and update below history table.\n
  <b>Revision 1</b>:  - Initial version.
**/

#define SI_SETUP_REVISION  1

typedef struct {
  UINT8   Revision;
  UINT8   PlatformDebugConsent;
  //
  // PCIe IMR
  //
  UINT8   PcieImrEnable;
  UINT16  PcieImrSize;
  UINT8   PcieImrRpLocation;
  UINT8   PcieImrRpSelection;
} SI_SETUP;

/**
 Making any setup structure change after code frozen
 will need to maintain backward compatibility, bump up
 structure revision and update below history table\n
  <b>Revision 1</b>:  - Initial version.
**/
#define DEBUG_SETUP_VOLATILE_DATA_REVISION  1

typedef struct {
  UINT8   StreamingTraceSink;
  UINT8   TraceHubForceOn;
  UINT8   JtagC10PgDis;
  UINT8   UsbOverCurrentOvrd;
} DEBUG_SETUP_VOLATILE_DATA;

#define BOARD_INFO_SETUP_REVISION  2

typedef struct {
  // @todo decouple Board related variables from SETUP_DATA
  UINT16 FabId;
  UINT16 BoardBomId;
//  UINT8  PchGeneration;
  BOOLEAN EcPresent;
  UINT8   EcEspiFlashSharingMode;
  UINT8   EcMajorRevision;
  UINT8   EcMinorRevision;
  UINT16  BoardId;
  BOOLEAN SpdPresent;
  UINT8   PlatformGeneration;
  UINT16  DisplayId;
  UINT8   EcPeciMode;
} BOARD_INFO_SETUP;

#pragma pack()

#ifndef PLATFORM_SETUP_VARIABLE_NAME
#define PLATFORM_SETUP_VARIABLE_NAME             L"Setup"
#endif

#define SETUP_GUID \
 { 0xEC87D643, 0xEBA4, 0x4BB5, { 0xA1, 0xE5, 0x3F, 0x3E, 0x36, 0xB2, 0x0D, 0xA9 } }

#define SA_SETUP_GUID \
 { 0x72c5e28c, 0x7783, 0x43a1, { 0x87, 0x67, 0xfa, 0xd7, 0x3f, 0xcc, 0xaf, 0xa4 } }

#define ME_SETUP_GUID \
 { 0x5432122d, 0xd034, 0x49d2, { 0xa6, 0xde, 0x65, 0xa8, 0x29, 0xeb, 0x4c, 0x74 } }

#define CPU_SETUP_GUID \
 { 0xb08f97ff, 0xe6e8, 0x4193, { 0xa9, 0x97, 0x5e, 0x9e, 0x9b, 0xa,  0xdb, 0x32 } }

#define PCH_SETUP_GUID \
 { 0x4570b7f1, 0xade8, 0x4943, { 0x8d, 0xc3, 0x40, 0x64, 0x72, 0x84, 0x23, 0x84 } }

#define SI_SETUP_GUID \
 { 0xAAF8E719, 0x48F8, 0x4099, { 0xA6, 0xF7, 0x64, 0x5F, 0xBD, 0x69, 0x4C, 0x3D } }

#define DEBUG_CONFIG_GUID \
 { 0xDE0A5E74, 0x4E3E, 0x3D96, { 0xA4, 0x40, 0x2C, 0x96, 0xEC, 0xBD, 0x3C, 0x97 } }

#define BOARD_INFO_SETUP_GUID \
 { 0x1E785E1A, 0x8EC4, 0x49E4, { 0x82, 0x75, 0xFB, 0xBD, 0xED, 0xED, 0x18, 0xE7 } }

//[-start-191111-IB10189001-add]//
#define SA_SETUP_VARIABLE_NAME                   L"SaSetup"
#define ME_SETUP_VARIABLE_NAME                   L"MeSetup"
#define CPU_SETUP_VARIABLE_NAME                  L"CpuSetup"
#define PCH_SETUP_VARIABLE_NAME                  L"PchSetup"
#define SI_SETUP_VARIABLE_NAME                   L"SiSetup"
#define SETUP_VOLATILE_DATA_VARIABLE_NAME        L"SetupVolatileData"
#define CPU_SETUP_VOLATILE_DATA_VARIABLE_NAME    L"CpuSetupVolatileData"
#define SETUP_CPU_FEATURES_NAME                  L"SetupCpuFeatures"
#define ME_SETUP_STORAGE_VARIABLE_NAME           L"MeSetupStorage"
#define TBT_SETUP_VOLATILLE_VARIABLE_NAME        L"TbtSetupVolatileData"
//[-end-191111-IB10189001-add]//

#endif
