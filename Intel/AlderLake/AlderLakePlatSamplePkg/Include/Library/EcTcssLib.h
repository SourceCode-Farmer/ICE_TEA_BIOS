/** @file
  EC TCSS library functions.

@copyright
  INTEL CONFIDENTIAL
  Copyright 2018 - 2020 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains 'Framework Code' and is licensed as such under the terms
  of your license agreement with Intel or your vendor. This file may not be
  modified, except as allowed by additional terms of your license agreement.

@par Specification Reference:
**/

#ifndef _BASE_EC_TCSS_LIB_H_
#define _BASE_EC_TCSS_LIB_H_

//
// Include files
//
#include <Base.h>
#include <Uefi.h>
#include <Library/EcCommands.h>
#include <Library/EcLib.h>

//
// Retry time in milli-seconds for EC-TCSS commands with Timeout
//
#define EC_TCSS_RETRY_TIME_GET_USB_CONN_STATUS     10
#define EC_TCSS_RETRY_TIME_SX_ENTRY_WAIT           100
#define EC_TCSS_RETRY_TIME_SX_EXIT_WAIT            100

//
// Bitmasks for USBC IOM Ready Notify message request byte
//
#define EC_TCSS_BITMASK_REPLAY_MESSAGES     0x01
#define EC_TCSS_BITMASK_VCCST_ON            0x02

//
// Define all PD Mode supported by EC
//
typedef enum {
  RetimerFirmWareUpdateDisableMode = 0,
  RetimerFirmWareUpdateEnableMode  = 1,
  InvalidRetimerFirmWareUpdateMode
} EC_C_PD_RETIMER_FW_UPDATE_MODE;

#define EC_SET_PD_MODE_WAIT_TIME_IN_FACTOR_OF_50_MS  10   // EC Wait time after Sending SET PD MODE command in Factor of 50 MiliSec
#define EC_C_ALL_PD_CONTROLLER_INDEX                 0xFE // PD Controller Index to SET mode of all PD Controller

//
// Currently EC is supporting 2 PD controller and following BIT MAP for each
// controller.
//    BIT0 - 1 - PD0 I2C cmd successful ( 0 - fail )
//    BIT1 - 1 - PD0 in FORCE TBT Mode (0 - not in force tbt mode)
//    BIT2-BIT3 - Reserved
//    BIT4 - 1 - PD1 I2C cmd successful(0 - fail)
//    BIT5 - 1 - PD1 in FORCE TBT Mode (0 - not in force tbt mode)
//    BIT6-BIT7 - Reserved
// So currently EC is using 4 BIT for EACH Controller and 8 BIT in total.
//
#define GET_PD_MODE_STATUS_BIT_WIDTH_TOTAL            8
#define GET_PD_MODE_STATUS_BIT_WIDTH_PER_CONTROLLER   4
#define I2C_COMMAND_SUCCESSFUL                        BIT0  // I2C cmd successful ( 0 - fail )
#define RETIMER_FW_UPDATE_MODE                        BIT1  // FORCE TBT Mode (0 - not in force tbt mode)

/**
  Usbc Retimer Compliance Mode

  @param[in][out]  DataBuffer

  @retval          EFI_SUCCESS       Command success
  @retval          EFI_DEVICE_ERROR  Command error
**/
EFI_STATUS
UsbcRetimerCompliancePDMessage  (
  IN OUT UINT8     *DataBuffer
  );

/**
  USBC IOM Ready Notify

  @param[in][out]  DataBuffer

  @retval          EFI_SUCCESS       Command success
  @retval          EFI_DEVICE_ERROR  Command error
**/
EFI_STATUS
UsbcIomReadyNotify (
  IN OUT UINT8     *DataBuffer
  );

/**
  USBC Sx Entry Wait

  @param[in][out]  DataBuffer
  @param[in]       Timeout

  @retval          EFI_SUCCESS       Command success
  @retval          EFI_DEVICE_ERROR  Command error
**/
EFI_STATUS
UsbcGetUsbConnStatus (
  IN OUT UINT8     *DataBuffer,
  IN     UINT32    Timeout
  );

/**
  Send enable ACPI mode command to EC

  @param[in][out]  DataBuffer
  @param[in]       Timeout

  @retval          EFI_SUCCESS       Command success
  @retval          EFI_DEVICE_ERROR  Command error
**/
EFI_STATUS
UsbcSxEntry (
  IN OUT UINT8     *DataBuffer
  , IN     UINT32    Timeout
  );

/**
  USBC Sx Exit Wait

  @param[out]     DataBuffer
  @param[in]       Timeout

  @retval         EFI_SUCCESS         Command success
  @retval         EFI_DEVICE_ERROR    Command error
**/
EFI_STATUS
UsbcSxExit (
  OUT    UINT8     *DataBuffer
  , IN     UINT32    Timeout
  );

/**
  Set PD Controller Mode

  @param[in]      PdControllerMode
  @param[in]      PdControllerNumber

  @retval         EFI_SUCCESS            Command success
  @retval         EFI_TIMEOUT            Command timeout
  @retval         EFI_DEVICE_ERROR       Command error
  @retval         EFI_INVALID_PARAMETER  Command usage error.
**/
EFI_STATUS
SetPdControllerMode (
  IN     UINT8     PdControllerMode,
  IN     UINT8     PdControllerNumber
  );

/**
  Get PD Controller Mode

  @param[out]     *PdControllerModeBuffer
  @param[in]      PdControllerNumber
  @param[in]      TotalCountOfPdController

  @retval         EFI_SUCCESS            Command success
  @retval         EFI_TIMEOUT            Command timeout
  @retval         EFI_DEVICE_ERROR       Command error
  @retval         EFI_INVALID_PARAMETER  Command usage error.
**/
EFI_STATUS
GetPdControllerMode (
  OUT    UINT8     *PdControllerModeBuffer,
  IN     UINT8     PdControllerNumber,
  IN     UINT32    TotalCountOfPdController
  );
#endif // _BASE_EC_TCSS_LIB_H_
