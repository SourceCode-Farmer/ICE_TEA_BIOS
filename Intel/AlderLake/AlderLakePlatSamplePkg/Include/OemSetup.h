/** @file
 OEM Specific Setup Variables and Structures

@copyright
  INTEL CONFIDENTIAL
  Copyright 2011 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains a 'Sample Driver' and is licensed as such under the terms
  of your license agreement with Intel or your vendor. This file may be modified
  by the user, subject to the additional terms of the license agreement.

@par Specification Reference:
**/

#ifndef _OEMSETUP_H_
#define _OEMSETUP_H_

#include <PchLimits.h>
#include <CpuPcieInfo.h>

#ifdef __cplusplus
extern "C" {
#endif

// OS profile
#define OS_PROFILE_GUID \
 { 0xE59376D7, 0x2DD9, 0x42A3, { 0x9E, 0xC8, 0x1D, 0x71, 0xD5, 0xE3, 0xC1, 0xEC }}

// Currently enumerated OSes
#define OsNone                   0x00
#define OsAndroid                0x01
#define OsWinCs                  0x02
#define OsWinNonCs               0x03
#define OsMax                    0x04

#define PCH_MOBILE_SKU          0
#define PCH_DESKTOP_SKU         1
#define PCH_SERVER_SKU          2

#define PCH_RP_AVAILABLE       0
#define PCH_RP_FUSED_OFF       1
#define PCH_RP_MERGED          2
#define PCH_RP_DIFFERENT_BUS   3
#define PCH_RP_NOT_IMPLEMENTED 5

#define CPU_PCIE_RP_AVAILABLE       0
#define CPU_PCIE_RP_FUSED_OFF       1
#define PCH_TSN_GBE_LANE6           6
#define PCH_TSN_GBE_LANE7           7
#pragma pack(1)

/**
 Making any setup structure change after code frozen
 will need to maintain backward compatibility, bump up
 structure revision and update below history table\n
     Revision 1:      - Initial version.
  <b>Revision 2</b>:  - Remove Item EnableDebugSerialIoUartNumber
  <b>Revision 3</b>:  - Added Item PSOnSupported
  <b>Revision 4</b>:  - Move Board related variables to SETUP_DATA
  <b>Revision 5</b>:  - Added OcrBootHttpsSupported, OcrBootPbaSupported, OcrBootWinReSupported, and OcrAmtDisSecBootSupported
  <b>Revision 6</b>:  - Deprecate TbtVtdBaseSecurityGrayOut.
  <b>Revision 7</b>:  - Added TcssPdType
  <b>Revision 8</b>:  - Added TotalNumberOfTypeCPortsSupported
  <b>Revision 9</b>:  - Added MobileOcUnSupport
  <b>Revision 10</b>: - Added AudioDspFusedOut
  <b>Revision 11</b>: - Added UsbcEcPdNegotiation
**/

#define SETUP_VOLATILE_DATA_REVISION 11


typedef struct _SETUP_VOLATILE_DATA
{
  UINT16 PlatId;
  UINT8  PlatformFlavor;
  UINT8  PlatformType;
  UINT8  BoardType;
  UINT8  VTdAvailable;
  UINT8  PchGeneration;
  UINT8  GbeSupportByPch;            // Gbe is supported on current PCH
  UINT8  GbeAvailable;               // Gbe is configured and usable
  UINT8  ThcSupported;
  UINT8  ThcAvailable;
  UINT8  IshAvailable;
  UINT8  DimmPresent[16];
  UINT8  MemoryType;                // 0 - DDR4, 1 - DDR5, 2 - LPDDR5, 3 - LPDDR4, 4 - UnKnown
  UINT8  PciePortCfg[PCH_MAX_PCIE_ROOT_PORTS];
  UINT8  PchUsb2PortCount;
  UINT8  PchUsb3PortCount;
  UINT8  CpuPciePortCfg[CPU_PCIE_MAX_ROOT_PORTS];
  UINT8  EmmcSupported;
  UINT8  UfsSupported[PCH_MAX_UFS_DEV_NUM];
  UINT32 AcpiDebugAddress;
  UINT8  SataMpsPresent[8];
  UINT8  DockAttached;
  UINT8  AudioHdaLinkSupported;
  UINT8  AudioDmicLinkSupported[PCH_MAX_HDA_DMIC_LINK_NUM];
  UINT8  AudioSspLinkSupported[PCH_MAX_HDA_SSP_LINK_NUM];
  UINT8  AudioSndwLinkSupported[PCH_MAX_HDA_SNDW_LINK_NUM];
  UINT8  PchCridSupport;
  UINT8  IpuSupport;
  UINT8  TbtVtdBaseSecurityGrayOut; // Deprecated from Revision 6.
  UINT8  CdClockSelector;
  UINT8  TsnSupportByPch;
  UINT8  TsnAvailable;
  UINT8  PSOnSupported;
  UINT8  TsnGbeLaneNum;
  UINT8  OcrBootHttpsSupported;
  UINT8  OcrBootPbaSupported;
  UINT8  OcrBootWinReSupported;
  UINT8  OcrAmtDisSecBootSupported;
  UINT8  TcssPdType;
  UINT8  TotalNumberOfTypeCPortsSupported;
  UINT8  MobileOcUnSupport;
  UINT8  AudioDspFusedOut;
  UINT8  UsbcEcPdNegotiation;
} SETUP_VOLATILE_DATA;

/**
 Making any setup structure change after code frozen
 will need to maintain backward compatibility, bump up
 structure revision and update below history table\n
  <b>Revision 1</b>:  - Initial version.
  <b>Revision 2</b>:  - Removed SgxSupported
  <b>Revision 3</b>:  - Add NumSmallCores support
  <b>Revision 4</b>:  - Added PerCore HT Disable
  <b>Revision 5</b>:  - Added TmeSupported support
  <b>Revision 6</b>:  - Added GT Unslice supported
  <b>Revision 7</b>:  - Added IsFastMsrHwpSupport
**/

#define SETUP_CPU_FEATURES_REVISION 7

//This structure is used for setup.
typedef struct {
  UINT8 XDBitAvailable;
  UINT8 TurboModeAvailable;
  UINT8 XETdcTdpLimitAvailable;
  UINT8 CoreRatioLimitAvailable;
  UINT8 HTAvailable;
  UINT8 MultiCoreAvailable;
  UINT8 VTAvailable;
  UINT8 AESAvailable;
  UINT8 TXTAvailable;
  UINT8 CxAvailable;
  UINT8 C1Available;
  UINT8 C1EAvailable;
  UINT8 C3Available;
  UINT8 C6Available;
  UINT8 C7Available;
  UINT8 C8Available;
  UINT8 C9Available;
  UINT8 C10Available;
  UINT8 EISTAvailable;
  UINT8 HwpAvailable;
  UINT8 ItbmAvailable;
  UINT8 NumCores;
  UINT8 MultiSocketAvailable;
  UINT8 MultiSocketPopulated;
  UINT8 LocalX2ApicAvailable;
  UINT8 EnergyEfficientTurboAvailable;
  UINT8 Reserved;
  UINT8 C6DramSupported;
  UINT8 ApicIdFlag;
  UINT8 ConfigTdpAvailable;
  UINT8 PpinFeatureAvailable;
  UINT8 LpmAvailable;
  UINT8 TimedMwaitSupported;
  UINT8 DebugInterfaceSupported;
  UINT8 ProcTraceSupported;
  UINT8 SmmEnhancementSupported;
  UINT8 SmmTargetedSmiAvailable;
  UINT8 SmmCpuSvrstrAvailable;
  UINT8 SmmLongFlowAvailable;
  UINT8 HdcSupported;
  UINT8 HwpInterruptSupported;
  UINT8 CtdpDownSupported;
  UINT8 CtdpUpSupported;
  UINT8 FivrSupported;
  UINT8 FclkSupported;
  UINT8 NumSmallCores;
  UINT8 PerCoreHtDisableSupported;
  UINT8 GtUnsliceSupported;
  UINT8 CpuSkuOcSupported;
  UINT8 TmeSupported;
  BOOLEAN SaVrSupport;
  UINT8 IsFastMsrHwpSupport;
} SETUP_CPU_FEATURES;

/**
 Making any setup structure change after code frozen
 will need to maintain backward compatibility, bump up
 structure revision and update below history table\n
  <b>Revision 1</b>:  - Initial version.
**/
#define OS_PROFILE_REVISION  1

typedef struct {
  UINT8   OsProfile;
  UINT8   PrevOs;
} OS_PROFILE;

#pragma pack()

/****** DO NOT WRITE BELOW THIS LINE *******/
#ifdef __cplusplus
}
#endif
#endif
