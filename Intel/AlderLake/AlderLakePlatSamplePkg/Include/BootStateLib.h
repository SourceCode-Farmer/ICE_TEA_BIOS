/** @file
# Component Information file for BootState Variable.
#
#@copyright
#  INTEL CONFIDENTIAL
#  Copyright 2020 Intel Corporation.
#
#  The source code contained or described herein and all documents related to the
#  source code ("Material") are owned by Intel Corporation or its suppliers or
#  licensors. Title to the Material remains with Intel Corporation or its suppliers
#  and licensors. The Material may contain trade secrets and proprietary and
#  confidential information of Intel Corporation and its suppliers and licensors,
#  and is protected by worldwide copyright and trade secret laws and treaty
#  provisions. No part of the Material may be used, copied, reproduced, modified,
#  published, uploaded, posted, transmitted, distributed, or disclosed in any way
#  without Intel's prior express written permission.
#
#  No license under any patent, copyright, trade secret or other intellectual
#  property right is granted to or conferred upon you by disclosure or delivery
#  of the Materials, either expressly, by implication, inducement, estoppel or
#  otherwise. Any license under such intellectual property rights must be
#  express and approved by Intel in writing.
#
#  Unless otherwise agreed by Intel in writing, you may not remove or alter
#  this notice or any other notice embedded in Materials by Intel or
#  Intel's suppliers or licensors in any way.
#
#  This file contains a 'Sample Driver' and is licensed as such under the terms
#  of your license agreement with Intel or your vendor. This file may be modified
#  by the user, subject to the additional terms of the license agreement.
#
# @par Specification Reference:
**/

#ifndef _BOOT_STATE_H_
#define _BOOT_STATE_H_

#define BOOT_STATE_VARIABLE_NAME  L"BootState"

#endif

/**
 This function call Checks BootState variable is NULL or not

 @retval         FALSE                  It's the first boot after reflashing.
 @retval         TRUE                   It's not the first boot after reflashing.
**/

BOOLEAN
EFIAPI
IsBootStatePresent (
  VOID
);

/**
 This function checks is the  very first boot after BIOS flash or Capsule update for EC/BIOS.

 @param[in, out] BootStateAfterCapsule  out,0 its second boot after capsule updated
                                        out,1 its very first boot after capsule updated
 @retval         status                 Very first boot after capsule update
 @retval         EFI_SUCCESS            NOT the first boot path after capsule update
**/

EFI_STATUS
EFIAPI
GetBootStateAfterCapsule (
  BOOLEAN                           *BootStateAfterCapsule
);

/**
 Set BootStateAfterCapsule variable to indicate the following boot is the very first boot after Capsule Update.

 @param[in]      BootStateAfterCapsule  TRUE, Following boot is the first boot after Capsule Update.
                                        FALSE, following boot is NOT the first boot after Capsule update.
 @retval         EFI_SUCCESS            BootStateAfterCapsule variable set.
**/

EFI_STATUS
EFIAPI
SetBootStateAfterCapsule (
  IN BOOLEAN                        BootStateAfterCapsule
);

/**
 This function call use to set bootstate variable.
 In DXE  phase BootState variable got set using setvariable().Form onward it will treat as second boot.

 @param[in]      BootState              Set value for bootstate
**/

VOID
EFIAPI
SetBootState (
  VOID
);

/**
 Delete BootState variable to force next boot is FullConfiguration boot
**/

VOID
EFIAPI
UnsetBootState (
  VOID
);
