/** @file

@copyright
  INTEL CONFIDENTIAL
  Copyright 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains a 'Sample Driver' and is licensed as such under the terms
  of your license agreement with Intel or your vendor. This file may be modified
  by the user, subject to the additional terms of the license agreement.

@par Specification Reference:
**/

#ifndef _WHEA_SETUP_H_
#define _WHEA_SETUP_H_

#ifdef WHEA_SUPPORT_FLAG

#pragma pack(1)
#define WHEA_SETUP_KEY                            4105
#define WHEA_SETUP_VARSTORE\
  varstore WHEA_SETUP, \
    varid = WHEA_SETUP_KEY, \
    name  = WheaSetup, \
    guid  = SETUP_GUID;

/**
 Making any setup structure change after code frozen
 will need to maintain backward compatibility, bump up
 structure revision and update below history table\n
  <b>Revision 1</b>:  - Initial version.
**/
#define WHEA_SETUP_REVISION  1

typedef struct{
  UINT8   WheaSupport;
  UINT8   LogErrors;
  UINT8   ErrorGeneration;
  UINT8   ProcessorErrors; // function not available
  UINT8   SaErrors;
  UINT8   PchErrors;
  UINT8   PcieErrors;
  UINT8   PlatformErrors;
  UINT8   PropagateSerr;
  UINT8   PropagatePerr;
  UINT8   FatErr;
  UINT8   UnCorRecErr;
  UINT8   CorErr;
} WHEA_SETUP;

#pragma pack()
#endif // WHEA_SUPPORT_FLAG
#endif
