/** @file

@copyright
  INTEL CONFIDENTIAL
  Copyright 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains a 'Sample Driver' and is licensed as such under the terms
  of your license agreement with Intel or your vendor. This file may be modified
  by the user, subject to the additional terms of the license agreement.

@par Specification Reference:
**/

#ifndef _BOOT_MAINT_SETUP_H_
#define _BOOT_MAINT_SETUP_H_

#pragma pack(1)

typedef struct {
  UINT8   ComNum;
} SERIAL_PORT_TAG;

typedef struct _SERIAL_PORT_ATTRIBUTE {
  UINT64            BaudRate;
  UINT8             DataBits;
  UINT8             StopBits;
  UINT8             Parity;
  UINT8             TerminalType;
  UINT8             FlowControl;
  UINT8             LegacyResolution;
  UINT8             IsEnabled;
  UINT8             Length;
  SERIAL_PORT_TAG   ComTag;
} SERIAL_PORT_ATTRIBUTE;

/**
 Making any setup structure change after code frozen
 will need to maintain backward compatibility, bump up
 structure revision and update below history table\n
  <b>Revision 1</b>:  - Initial version.
**/
#define TOTAL_SERIAL_PORT_INFO_REVISION  1

typedef struct {
  UINT64 TotalSize;
  SERIAL_PORT_ATTRIBUTE PortAttributes[1];
} TOTAL_SERIAL_PORT_INFO;

/**
 Making any setup structure change after code frozen
 will need to maintain backward compatibility, bump up
 structure revision and update below history table\n
  <b>Revision 1</b>:  - Initial version.
**/
#define TIME_OUT_INFO_REVISION  1
typedef struct {
  UINT16  BootTimeOut;
} TIME_OUT_INFO;


#pragma pack()
#endif
