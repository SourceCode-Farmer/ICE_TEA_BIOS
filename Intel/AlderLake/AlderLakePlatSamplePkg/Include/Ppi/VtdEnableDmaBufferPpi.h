/** @file
  The definition for the VTd Enable DMA Buffer PPI.

  @copyright
   INTEL CONFIDENTIAL
   Copyright 2021 Intel Corporation.

  SPDX-License-Identifier: BSD-2-Clause-Patent

**/

#ifndef __VTD_ENABLE_DMA_BUFFER_PPI_H__
#define __VTD_ENABLE_DMA_BUFFER_PPI_H__

#define PLATFORM_VTD_ENABLE_DMA_BUFFER_PPI_GUID \
    { \
      0x2355a0a7, 0x4c52, 0x4b5b, { 0x96, 0xac 0xe5, 0x8e, 0x5e, 0xec, 0x79, 0xc8 } \
    }

//
// Forward declaration for the VTd Enable DMA Buffer PPI.
//

/*
  Enable DMA buffer in VTd

  @param[in] VtdEngineIndex         VTd engine index.

  @retval EFI_SUCCESS               Enable successfully.
  @retval EFI_INVALID_PARAMETER     Input parameters are invalid.
  @retval EFI_UNSUPPORTED           VTd base is zero.
  @retval EFI_OUT_OF_RESOURCES      There is no additional space in the PPI database.
*/
typedef
EFI_STATUS
(EFIAPI *VTD_ENABLE_DMA_BUFFER) (
  IN VTD_ENGINE_INDEX VtdEngineIndex
  );

extern EFI_GUID gPlatformVTdEnableDmaBufferPpiGuid;

#endif

