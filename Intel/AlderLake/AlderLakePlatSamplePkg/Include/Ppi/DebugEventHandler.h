/** @file

@copyright
  INTEL CONFIDENTIAL
  Copyright 2020 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains a 'Sample Driver' and is licensed as such under the terms
  of your license agreement with Intel or your vendor. This file may be modified
  by the user, subject to the additional terms of the license agreement.

@par Specification Reference:
**/

#ifndef __FSPWRAPPER_DEBUG_EVENT_HABDLER_PPI_H__
#define __FSPWRAPPER_DEBUG_EVENT_HABDLER_PPI_H__

#include <Pi/PiStatusCode.h>

//
// Global ID for the DEBUG_EVENT_HABDLER_PPI
//
#define DEBUG_EVENT_HABDLER_PPI_GUID \
  { \
    0x6e6f5daa, 0x77cb, 0x48ff, { 0xb3, 0x18, 0x2d, 0x5e, 0x62, 0x0, 0xd0, 0x1e } \
  }

///
/// Forward declaration for the DEBUG_EVENT_HABDLER_PPI
///
typedef struct _DEBUG_EVENT_HABDLER_PPI DEBUG_EVENT_HABDLER_PPI;

/*
  FSP Wrapper debug event handler

  @param[in] Type                   Indicates the type of event being reported.
                                    See MdePkg/Include/Pi/PiStatusCode.h for the definition of EFI_STATUS_CODE_TYPE.
  @param[in] Value                  Describes the current status of a hardware or software entity.
                                    This includes information about the class and subclass that is used to classify the entity as well as an operation.
                                    For progress events, the operation is the current activity. For error events, it is the exception.
                                    For debug events, it is not defined at this time.
                                    See MdePkg/Include/Pi/PiStatusCode.h for the definition of EFI_STATUS_CODE_VALUE.
  @param[in] Instance               The enumeration of a hardware or software entity within the system.
                                    A system may contain multiple entities that match a class/subclass pairing. The instance differentiates between them.
                                    An instance of 0 indicates that instance information is unavailable, not meaningful, or not relevant.
                                    Valid instance numbers start with 1.
  @param[in] *CallerId              This parameter can be used to identify the sub-module within the FSP generating the event.
                                    This parameter may be NULL.
  @param[in] *Data                  This optional parameter may be used to pass additional data. The contents can have event-specific data.
                                    For example, the FSP provides a EFI_STATUS_CODE_STRING_DATA instance to this parameter when sending debug messages.
                                    This parameter is NULL when no additional data is provided.

  @retval EFI_SUCCESS               The event was handled successfully.
  @retval EFI_INVALID_PARAMETER     Input parameters are invalid.
  @retval EFI_DEVICE_ERROR          The event handler failed.
*/
typedef
EFI_STATUS
(EFIAPI *FSP_EVENT_HANDLER) (
  IN          EFI_STATUS_CODE_TYPE   Type,
  IN          EFI_STATUS_CODE_VALUE  Value,
  IN          UINT32                 Instance,
  IN OPTIONAL EFI_GUID               *CallerId,
  IN OPTIONAL EFI_STATUS_CODE_DATA   *Data
  );

///
/// This PPI contains a set of services to print message to debug output device
///
struct _DEBUG_EVENT_HABDLER_PPI {
  FSP_EVENT_HANDLER                DebugEventHandler;
};

extern EFI_GUID gDebugEventHandlerPpiGuid;

#endif

