/** @file

  This file contains RST related PPI definitions.

@copyright
  INTEL CONFIDENTIAL
  Copyright 2019 - 2020 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains a 'Sample Driver' and is licensed as such under the terms
  of your license agreement with Intel or your vendor. This file may be modified
  by the user, subject to the additional terms of the license agreement.

@par Specification Reference:
**/


#ifndef __PEI_RST_CONFIG_PPI_H__
#define __PEI_RST_CONFIG_PPI_H__

//
// This PPI is installed by AhciHc driver when AHCI controller is RAID mode.
// RST driver will consume this PPI to produce passthru PPI/ SSCP interface of RST version.
//
#define RST_RAID_CONTROLLER_PPI_GUID \
{  \
  0x62f52aed, 0xf299, 0x4167, { 0x8e, 0x94, 0xea, 0x61, 0x9f, 0x5, 0x12, 0xad } \
}

extern EFI_GUID gRstRaidControllerPpiGuid;

//
// This PPI is installed by NvmeHc driver when NVMe device is hybrid device.
// RST driver will consume this PPI to produce passthru PPI/ SSCP interface of RST version.
//
#define HYBRID_NVME_EXPRESS_HOST_CONTROLLER_PPI_GUID \
{ \
  0x5da70373, 0xd684, 0x4b95, { 0x9a, 0xae, 0x72, 0x6d, 0xa3, 0xfd, 0x47, 0xcb } \
}

extern EFI_GUID gPeiHybridNvmExpressHostControllerPpiGuid;

#endif
