
/** @file
  Macros for platform to update different types of policy.

 @copyright
  INTEL CONFIDENTIAL
  Copyright 2019 - 2020 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains 'Framework Code' and is licensed as such under the terms
  of your license agreement with Intel or your vendor. This file may not be
  modified, except as allowed by additional terms of your license agreement.

@par Specification Reference:
**/

#ifndef _POLICY_UPDATE_MACRO_H_
#define _POLICY_UPDATE_MACRO_H_

#ifdef UPDATE_POLICY
#undef UPDATE_POLICY
#endif

#ifdef COPY_POLICY
#undef COPY_POLICY
#endif

#ifdef GET_POLICY
#undef GET_POLICY
#endif

#ifdef AND_POLICY
#undef AND_POLICY
#endif

#ifdef OR_POLICY
#undef OR_POLICY
#endif

#define NullIndex  -1
#define POLICY_DEBUG_WARNING(ConfigField, SetupField, ArrayIndex)  {\
  DEBUG ((DEBUG_INFO, "Policy Default Mismatch With Setup Default\n"));\
  DEBUG ((DEBUG_INFO, "ConfigBlock : " #ConfigField " = 0x%x\n", ConfigField));\
  DEBUG ((DEBUG_INFO, "Setup : " #SetupField " = 0x%x\n", SetupField));\
  if (ArrayIndex != NullIndex) {\
    DEBUG ((DEBUG_INFO, "ArrayIndex : " #ArrayIndex " = 0x%x\n", ArrayIndex));\
  }\
}

#if FixedPcdGet8(PcdFspModeSelection) == 1
//
// MACROS for platform code use
//
#define UPDATE_POLICY(UpdField, ConfigField, Value)  UpdField = Value;
#define COPY_POLICY(UpdField, ConfigField, Value, Size)  CopyMem (UpdField, Value, Size);
#define GET_POLICY(UpdField, ConfigField, Value)  Value = UpdField;
#define AND_POLICY(UpdField, ConfigField, Value)  UpdField &= Value;
#define OR_POLICY(UpdField, ConfigField, Value)  UpdField |= Value;
//
// Compare Policy Default and Setup Default not support for API mode yet
//
#define COMPARE_AND_UPDATE_POLICY(UpdField, ConfigField, Value, ArrayIndex)   UPDATE_POLICY(UpdField, ConfigField, Value);
#else
#define UPDATE_POLICY(UpdField, ConfigField, Value)  ConfigField = Value;
#define COPY_POLICY(UpdField, ConfigField, Value, Size)  CopyMem (ConfigField, Value, Size);
#define GET_POLICY(UpdField, ConfigField, Value)  Value = ConfigField;
#define AND_POLICY(UpdField, ConfigField, Value)  ConfigField &= Value;
#define OR_POLICY(UpdField, ConfigField, Value)  ConfigField |= Value;
//
// Compare Policy Default and Setup Default when FirstBoot and RvpSupport
//
#define COMPARE_AND_UPDATE_POLICY(UpdField, ConfigField, Value, ArrayIndex) {\
  if ((ConfigField != Value) && (IsPolicyDefaultCheckRequired())) {\
    POLICY_DEBUG_WARNING(ConfigField, Value, ArrayIndex);\
  }\
  UPDATE_POLICY(UpdField, ConfigField, Value);\
}
#endif


#endif //_POLICY_UPDATE_MACRO_H_
