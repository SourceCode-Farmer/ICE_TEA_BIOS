/** @file

@copyright
  INTEL CONFIDENTIAL
  Copyright 2018 - 2020 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains a 'Sample Driver' and is licensed as such under the terms
  of your license agreement with Intel or your vendor. This file may be modified
  by the user, subject to the additional terms of the license agreement.

@par Specification Reference:
**/

#ifndef __CRASHLOG_H__
#define __CRASHLOG_H__

#define CRASHLOG_SIZE_DEBUG_PURPOSE                   0x030  // It will help to verify the extracted binary from BERT by matching the binary content
#define CRASHLOG_MECH_LEGACY                          0      // Get PMC crash log by using legacy mode
#define CRASHLOG_MECH_DESCRIPTOR_TABLE                1      // Get PMC crash log by using descriptor table

//
// Structure of CrashLog Hob data
//
typedef struct {
  UINT64  AllocateAddress;
  UINT32  Size;
} CRASHLOG_HOB;

//
// Structure of CPU CrashLog Hob data
//
typedef struct {
  CRASHLOG_HOB  Main;
  CRASHLOG_HOB  Trace;
  CRASHLOG_HOB  Telemetry;
} CPU_CRASHLOG_HOB;

//
// CRASHLOG_VERSON
//
typedef union {
  struct {
    UINT32  Revision   : 8;  // Revision
    UINT32  HeaderType : 4;  // Crash record header type
    UINT32  ProductId  : 12; // Product ID
    UINT32  CrashType  : 6;  // Crash record type
    UINT32  Rsvd       : 1;  // Reserved
    UINT32  Consumed   : 1;  // Consumed
  } Bits;
  UINT32  Uint32;
} CRASHLOG_VERSION;

//
// CRASHLOG_REASON
//
typedef union {
  struct {
    UINT32  CWReset    : 1;  // Cold/Warm reset
    UINT32  GReset     : 1;  // Global reset
    UINT32  Error      : 1;  // Error
    UINT32  MInit      : 1;  // Manually Initiated
    UINT32  Reserved   : 28;
  } Bits;
  UINT32  Uint32;
} CRASHLOG_REASON;

#endif
