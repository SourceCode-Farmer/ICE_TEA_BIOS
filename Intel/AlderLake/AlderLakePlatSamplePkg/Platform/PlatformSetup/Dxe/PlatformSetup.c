/** @file

;******************************************************************************
;* Copyright (c) 2015 - 2020, Insyde Software Corporation. All Rights Reserved.
;*
;* You may not reproduce, distribute, publish, display, perform, modify, adapt,
;* transmit, broadcast, present, recite, release, license or otherwise exploit
;* any part of this publication in any form, by any means, without the prior
;* written permission of Insyde Software Corporation.
;*
;******************************************************************************
**/
/** @file
  Platform Policy Initialization Driver

@copyright
  INTEL CONFIDENTIAL
  Copyright 1999 - 2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains a 'Sample Driver' and is licensed as such under the terms
  of your license agreement with Intel or your vendor. This file may be modified
  by the user, subject to the additional terms of the license agreement.

@par Specification Reference:
**/

#include <Library/S3BootScriptLib.h>
#include <Library/PchInfoLib.h>
#include <PlatformSetup.h>
#include <OemSetup.h>
#include <Library/GpioLib.h>
#include <Library/MemoryAllocationLib.h>
#include <Protocol/CpuInfo.h>
#include <PlatformBoardConfig.h>
#include <Library/SataSocLib.h>
#include <Register/CommonMsr.h>
#include <PchHybridStorageHob.h>

#define  CPUID_FULL_FAMILY_MODEL_ALDERLAKE_ATOM  0x000B06E0
//
// Global variables
//

GLOBAL_REMOVE_IF_UNREFERENCED CPU_SETUP                       mCpuSetup;
GLOBAL_REMOVE_IF_UNREFERENCED EFI_PCI_ROOT_BRIDGE_IO_PROTOCOL *mPciRootBridgeIo;
GLOBAL_REMOVE_IF_UNREFERENCED CHAR16                          HexDigit[17] = L"0123456789ABCDEF";

//[-start-200902-IB17800095-remove]//
//GLOBAL_REMOVE_IF_UNREFERENCED EFI_PS2_POLICY_PROTOCOL         mPs2PolicyData = {
//  EFI_KEYBOARD_NUMLOCK,
//  (EFI_PS2_INIT_HARDWARE) Ps2InitHardware
//};
//[-end-200902-IB17800095-remove]//

GLOBAL_REMOVE_IF_UNREFERENCED SA_POLICY_PROTOCOL             *mSaPolicy;

GLOBAL_REMOVE_IF_UNREFERENCED UINT8                           mBigCoreCount;
GLOBAL_REMOVE_IF_UNREFERENCED UINT8                           mSmallCoreCount;

//[-start-200902-IB17800095-add]//
#define HbPciD0F0RegBase        (UINTN) PcdGet64 (PcdPciExpressBaseAddress) + (UINT32) (0 << 15) + (UINT32) (0 << 12)
//[-end-200902-IB17800095-add]//

GLOBAL_REMOVE_IF_UNREFERENCED SETUP_VOLATILE_DATA   mSetupVolatileData = {0};
GLOBAL_REMOVE_IF_UNREFERENCED UINTN                 mNumberOfCPUs;

//
// Functions
//
/**
  Get the cpu signature.

  @retval Cpu Signature

  @todo remove and use CpuPlatformLib
**/
UINT32 GetCpuSignature (
  VOID
  )
{
  CPUID_VERSION_INFO_EAX  CpuidVersionInfoEax;
  AsmCpuid (
          CPUID_VERSION_INFO,
          &CpuidVersionInfoEax.Uint32,
          NULL,
          NULL,
          NULL
          );

  return CpuidVersionInfoEax.Uint32;
}

/**
  Determine if CPU supports Execute Disable.

  @param[in] Features   Pointer to CPUID_EXTENDED_CPU_SIG_EDX buffer

  @retval True          if supported.

  @todo remove and use CPU Info protocol or CpuPlatformLib
**/
BOOLEAN
IsXDSupported (
  CPUID_EXTENDED_CPU_SIG_EDX *Features
  )
{
  UINT32 CpuSignature = GetCpuSignature() & 0xff0;    //Clear extended signature.
  return (BOOLEAN)(
      (CpuSignature >= 0xf41 || CpuSignature < 0xf00) && //conroe, woodcrest CpuSignature = 6fx
      Features->Bits.NX
  );
}

VOID
HexToString (
  CHAR16  *String,
  UINTN   Value,
  UINTN   Digits
  )
{
  for (; Digits > 0; Digits--, String++) {
    *String = HexDigit[((Value >> (4*(Digits-1))) & 0x0f)];
  }
}

/**
  Displays BIST result on Console.

  @param[in] Core         Number of Code
  @param[in] Thread       Number of Code
  @param[in] Eax          Value of Register Eax
**/
VOID
DisplaySelfTestBistResult (
  IN UINT32 Core,
  IN UINT32 Thread,
  IN UINT32 Eax
  )
{
  CHAR16                    String[20];
  CHAR16                    StrBuffer[10];

  gST->ConOut->OutputString (gST->ConOut, L"*******************BIST FAILED*****************\r\n");

//[-start-200902-IB17800095-modify]//
  //
  // Display  Core detail
  //
//[-start-200215-IB14630316-modify]//
//[-start-200215-IB14630314-modify]//
  StrCpyS (String, ARRAY_SIZE(String), L"CORE = 0x");
  HexToString(StrBuffer, Core, 2);
  StrCpyS (String + StrLen(String), ARRAY_SIZE(String) - StrLen (String), StrBuffer);
//[-end-200215-IB14630314-modify]//
//[-end-200215-IB14630316-modify]//
  gST->ConOut->OutputString (gST->ConOut,String);

  //
  // Display  Thread detail
  //
//[-start-200215-IB14630316-modify]//
//[-start-200215-IB14630314-modify]//
  StrCpyS (String, ARRAY_SIZE(String), L" Thread = 0x");
  HexToString(StrBuffer, Thread, 2);
  StrCpyS (String + StrLen(String), ARRAY_SIZE(String) - StrLen (String), StrBuffer);
//[-end-200215-IB14630314-modify]//
//[-end-200215-IB14630316-modify]//
  gST->ConOut->OutputString (gST->ConOut,String);

  //
  // Display  Eax detail
  //
//[-start-200215-IB14630316-modify]//
//[-start-200215-IB14630314-modify]//
  StrCpyS (String, ARRAY_SIZE(String), L" EAX = 0x");
  HexToString(StrBuffer, Eax, 4);
  StrCpyS (String + StrLen(String), ARRAY_SIZE(String) - StrLen (String), StrBuffer);
//[-end-200215-IB14630314-modify]//
//[-end-200215-IB14630316-modify]//
  gST->ConOut->OutputString (gST->ConOut,String);
  gST->ConOut->OutputString (gST->ConOut,L"\r\n");
//[-end-200902-IB17800095-modify]//
}

/**
  This function gets number of different cores enabled in the platform.

**/
STATIC
VOID
GetNumberOfHybridCores (
  VOID
  )
{
  CPUID_NATIVE_MODEL_ID_AND_CORE_TYPE_EAX     Eax;

  if (IsSecondaryThread ()) {
    return;
  }

  //
  // Check which is the running core by reading CPUID.(EAX=1AH, ECX=00H):EAX
  //
  AsmCpuid (CPUID_HYBRID_INFORMATION, &Eax.Uint32, NULL, NULL, NULL);

  if ((UINT8) Eax.Bits.CoreType == CPUID_CORE_TYPE_INTEL_CORE) {
    mBigCoreCount++;
  } else {
    mSmallCoreCount++;
  }
}

/**
  Self Test BIST result computed.

  @param[in] Event    Event whose notification function is being invoked.
  @param[in] Context  Pointer to the notification function's context.
**/
VOID
EFIAPI
SelfTestBistErrMsg (
  IN EFI_EVENT Event,
  IN VOID      *Context
  )
{
  EFI_STATUS                          Status;
  EFI_PROCESSOR_INFORMATION           MpContext;
  EFI_MP_SERVICES_PROTOCOL            *MpServices;
  UINT32                              Index;
  BOOLEAN                             BistFailed = FALSE;

  Status = gBS->LocateProtocol (
                  &gEfiMpServiceProtocolGuid,
                  NULL,
                  (VOID **) &MpServices
                  );
  ASSERT_EFI_ERROR (Status);

  gST->ConOut->ClearScreen (gST->ConOut);

  for (Index = 0; Index < mNumberOfCPUs; Index++) {
    Status = MpServices->GetProcessorInfo (MpServices, Index, &MpContext);
    ASSERT_EFI_ERROR (Status);
    if (MpContext.ProcessorId > 255) {
      break;
    }
    if ((MpContext.StatusFlag & PROCESSOR_HEALTH_STATUS_BIT) == 0) {
      BistFailed = TRUE;
      DEBUG ((DEBUG_ERROR, "BIST FAILED CORE=%x THREAD=%x EAX=%x\n", (UINT32)MpContext.Location.Core, (UINT32)MpContext.Location.Thread, MpContext.StatusFlag));
      DisplaySelfTestBistResult(
                   (UINT32)MpContext.Location.Core,
                   (UINT32)MpContext.Location.Thread,
                   MpContext.StatusFlag
                   );
    }
  }

  if (BistFailed) {
    DEBUG ((DEBUG_ERROR, "BIST Failed!!\n"));
    CpuDeadLoop();
  } else {
    DEBUG ((DEBUG_INFO, "BIST passed\n"));
  }
  gBS->CloseEvent(Event);

}

VOID
EFIAPI
CpuSetupInitCallback (
  IN EFI_EVENT     Event,
  IN VOID          *Context
  )
{
  EFI_STATUS                                   Status;
  SETUP_CPU_FEATURES                           SetupCpuFeatures;
  UINTN                                        Index;
  EFI_PROCESSOR_INFORMATION                    MpContext;
  VOID                                         *VariableWrite;
  UINT32                                       CpuidRegsEax;
  EFI_EVENT                                    ReadyToBootEvent;
  EFI_MP_SERVICES_PROTOCOL                     *MpServices;
  UINTN                                        NumberOfEnabledCPUs;
  UINT32                                       SetupCpuFeaturesAttr;
  UINTN                                        SetupCpuFeaturesSize;
  CPU_INFO_PROTOCOL                            *CpuInfo;
  CPUID_MONITOR_MWAIT_EDX                      MonitorMwaitEdx;
  CPUID_VERSION_INFO_ECX                       CpuIdVersionInfoEcx;
  CPUID_EXTENDED_CPU_SIG_EDX                   CpuidExtendCpuSigEdx;
  UINT32                                       CpuidExtendCpuFlagEdx;


  SetupCpuFeaturesAttr = 0;
  SetupCpuFeaturesSize = 0;
  Status = gBS->LocateProtocol (
                  &gEfiMpServiceProtocolGuid,
                  NULL,
                  (VOID **) &MpServices
                  );
  if (EFI_ERROR(Status)) {
    return;
  }

  Status = gBS->LocateProtocol (
                  &gEfiVariableWriteArchProtocolGuid,
                  NULL,
                  &VariableWrite
                  );
  if (EFI_ERROR(Status)) {
    return;
  }

  DEBUG ((DEBUG_INFO, "Running CpuSetupInitCallback...\n"));

  GetSupportedCpuFeatures ((UINT32 *) &CpuIdVersionInfoEcx);
  AsmCpuid (
    CPUID_SIGNATURE,
    &CpuidRegsEax,
    NULL,
    NULL,
    NULL
    );

  if(CpuidRegsEax > 3) {    //Check for largest CpuId
    AsmCpuid (
      CPUID_EXTENDED_CPU_SIG,
      NULL,
      NULL,
      NULL,
      &CpuidExtendCpuSigEdx.Uint32
      );
  }

  //
  // Only show as supported if all are supported.
  //
  ZeroMem(&SetupCpuFeatures, sizeof(SetupCpuFeatures));
  SetupCpuFeaturesSize = sizeof (SETUP_CPU_FEATURES);
  Status = gRT->GetVariable (
                  L"SetupCpuFeatures",
                  &gSetupVariableGuid,
                  &SetupCpuFeaturesAttr,
                  &SetupCpuFeaturesSize,
                  &SetupCpuFeatures
                  );

  if (EFI_ERROR (Status)) {
    SetupCpuFeaturesAttr = EFI_VARIABLE_BOOTSERVICE_ACCESS;
  }
  SetupCpuFeatures.XDBitAvailable = IsXDSupported (&CpuidExtendCpuSigEdx);
  SetupCpuFeatures.TurboModeAvailable = (UINT8) IsTurboModeSupported ();
  SetupCpuFeatures.XETdcTdpLimitAvailable = (UINT8) IsXETdcTdpLimitSupported ();
  SetupCpuFeatures.CoreRatioLimitAvailable = (UINT8) IsCoreRatioLimitSupported ();
  SetupCpuFeatures.ConfigTdpAvailable = (UINT8) IsConfigTdpSupported ();
  SetupCpuFeatures.LpmAvailable = (UINT8) IsLpmSupported ();
  SetupCpuFeatures.VTAvailable = (UINT8) CpuIdVersionInfoEcx.Bits.VMX;        // if VMX =1 , VT enable
  SetupCpuFeatures.AESAvailable = (UINT8) CpuIdVersionInfoEcx.Bits.AESNI;     // if AESNI =1, AES supported
  SetupCpuFeatures.HTAvailable = IsHyperThreadingSupported ();
  SetupCpuFeatures.MultiCoreAvailable = 1;
  SetupCpuFeatures.MultiSocketAvailable = 0;
  SetupCpuFeatures.MultiSocketPopulated = 0;
  SetupCpuFeatures.LocalX2ApicAvailable = (UINT8) CpuIdVersionInfoEcx.Bits.x2APIC;    // If X2APIC=1 , X2APIC supported
  SetupCpuFeatures.EnergyEfficientTurboAvailable = (UINT8) IsEnergyEfficientTurboSupported ();
  SetupCpuFeatures.TimedMwaitSupported = (UINT8) IsTimedMwaitSupported ();
  SetupCpuFeatures.HwpAvailable = (UINT8) IsHwpSupported ();
  DEBUG((DEBUG_INFO, "Is HWP supported  0x%x \n", SetupCpuFeatures.HwpAvailable));
  GetConfigTdpLevelsSupported(&SetupCpuFeatures.CtdpDownSupported, &SetupCpuFeatures.CtdpUpSupported);

  SetupCpuFeatures.ItbmAvailable = (UINT8) IsItbmSupported ();
  DEBUG((DEBUG_INFO, "Itbm: SetupCpuFeatures.ItbmAvailable = 0x%x \n", SetupCpuFeatures.ItbmAvailable));

  SetupCpuFeatures.PpinFeatureAvailable = IsPpinFeatureAvailable ();
  DEBUG((DEBUG_INFO, "SetupCpuFeatures.PpinFeatureAvailable = 0x%x \n", SetupCpuFeatures.PpinFeatureAvailable));

  SetupCpuFeatures.FivrSupported = 1;
  ///
  /// Since ADL doesn't support
  ///
  SetupCpuFeatures.FivrSupported = 1;
  SetupCpuFeatures.FclkSupported = 0;
  SetupCpuFeatures.GtUnsliceSupported = 0;
  SetupCpuFeatures.SaVrSupport = IsSaVrSupport ();
  SetupCpuFeatures.IsFastMsrHwpSupport = 0;

  ///
  /// Specific CPU features supported
  /// - PerCore HT Disable
  /// - OC Supported CPU Sku
  ///
  if (GetCpuSku () != EnumCpuUlx) {
    SetupCpuFeatures.PerCoreHtDisableSupported = 1;
    SetupCpuFeatures.CpuSkuOcSupported = 1;
  }

  SetupCpuFeatures.ApicIdFlag = 0;

  Status = MpServices->GetNumberOfProcessors (MpServices, &mNumberOfCPUs, &NumberOfEnabledCPUs);
  ASSERT_EFI_ERROR (Status);

  for (Index = 0; Index < mNumberOfCPUs; Index++) {
    Status = MpServices->GetProcessorInfo (MpServices, Index, &MpContext);
    ASSERT_EFI_ERROR (Status);
    if (MpContext.ProcessorId > 255) {
      SetupCpuFeatures.ApicIdFlag = 1;
      break;
    }
  }

  //
  // Setup For CPU PPM
  //
  SetupCpuFeatures.CxAvailable = 0;
  SetupCpuFeatures.C1Available = 0;
  SetupCpuFeatures.C1EAvailable = 0;
  SetupCpuFeatures.C3Available = 0;
  SetupCpuFeatures.C6Available = 0;
  SetupCpuFeatures.C7Available = 0;
  SetupCpuFeatures.C8Available = 0;
  SetupCpuFeatures.C9Available = 0;
  SetupCpuFeatures.C10Available = 0;

  if (CpuIdVersionInfoEcx.Bits.MONITOR) {
    GetSubCStateSupported (NULL, (UINT32 *)&MonitorMwaitEdx);
    //
    // Don't use mwait for now to halt CPU in BIOS.
    //
    //if (RegEcx & 0x01) {
    //  UseMwait = TRUE;
    //}
    if (MonitorMwaitEdx.Bits.C1States) {
      SetupCpuFeatures.C1Available = 1;
      if ((MonitorMwaitEdx.Bits.C1States) >= ENHANCED_CSTATE_SUPPORTED) {
        SetupCpuFeatures.C1EAvailable = 1;
      }
    }
    if (MonitorMwaitEdx.Bits.C2States) {
      SetupCpuFeatures.C3Available = 1;
    }
    if (MonitorMwaitEdx.Bits.C3States) {
      SetupCpuFeatures.C6Available = 1;
    }
    if (MonitorMwaitEdx.Bits.C4States) {
      SetupCpuFeatures.C7Available = 1;
    }
     if (PcdGet8 (PcdPlatformType) == TypeUltUlx) {
      if (MonitorMwaitEdx.Bits.C5States) {
        SetupCpuFeatures.C8Available = 1;
      }
      if (MonitorMwaitEdx.Bits.C6States) {
        SetupCpuFeatures.C9Available = 1;
      }
      if (MonitorMwaitEdx.Bits.C7States) {
        SetupCpuFeatures.C10Available = 1;
      }
    }
  }

  //
  // This allows the setup question to limit package C-states.
  // Enable after further testing.
  //
  SetupCpuFeatures.CxAvailable =
      SetupCpuFeatures.C1Available |
      SetupCpuFeatures.C3Available |
      SetupCpuFeatures.C6Available |
      SetupCpuFeatures.C7Available;

  //
  // Checking TXT capability after all CPU initialized
  //
  SetupCpuFeatures.TXTAvailable = (UINT8) CpuIdVersionInfoEcx.Bits.SMX;    //if SMX= 1, SMX supported

  SetupCpuFeatures.EISTAvailable = (UINT8) CpuIdVersionInfoEcx.Bits.EIST;
  //
  // Getting Number of Big Cores & Small Cores.
  //
  mBigCoreCount   = 0;
  mSmallCoreCount = 0;
  AsmCpuidEx (CPUID_STRUCTURED_EXTENDED_FEATURE_FLAGS, 0, NULL, NULL, NULL, &CpuidExtendCpuFlagEdx);
  //
  // Check if Hetero is supported.
  //
  if ((CpuidExtendCpuFlagEdx & BIT15) == BIT15) {
    GetNumberOfHybridCores (); // Hybrid Cores config
    MpServices->StartupAllAPs (
                  MpServices,
                  (EFI_AP_PROCEDURE) GetNumberOfHybridCores,
                  TRUE,
                  NULL,
                  0,
                  NULL,
                  NULL
                  );
    SetupCpuFeatures.NumCores      = mBigCoreCount;
    SetupCpuFeatures.NumSmallCores = mSmallCoreCount;
  } else {
    if (GetCpuFamily () == CPUID_FULL_FAMILY_MODEL_ALDERLAKE_ATOM) { // Atom Core Only
      mBigCoreCount = 0;
      mSmallCoreCount = (UINT8) GetEnabledCoreCount ();
    } else { // Big Core Only
      mBigCoreCount = (UINT8) GetEnabledCoreCount ();
      mSmallCoreCount = 0;
    }
  }
  SetupCpuFeatures.NumCores      = mBigCoreCount;
  SetupCpuFeatures.NumSmallCores = mSmallCoreCount;
  DEBUG ((DEBUG_INFO, "Number of cores enabled in platform: BigCores = %d  SmallCores = %d\n", mBigCoreCount, mSmallCoreCount));

  ///
  /// Debug interface is supported if CPUID (EAX=1): ECX[11] = 1
  ///
  SetupCpuFeatures.DebugInterfaceSupported = (UINT8) CpuIdVersionInfoEcx.Bits.SDBG;
  ///
  /// Processor supports HDC feature if silicon info indicates it
  ///
  SetupCpuFeatures.HdcSupported = 0;
  Status = gBS->LocateProtocol (&gCpuInfoProtocolGuid, NULL, (VOID **) &CpuInfo);
  if (EFI_ERROR (Status)) {
    DEBUG ((DEBUG_ERROR, "Located CpuInfo Protocol Status = %r\n", Status));
  } else {
    DEBUG ((DEBUG_INFO, "CpuInfo->CpuCommonFeatures = %016X\n", CpuInfo->CpuCommonFeatures));
    if (CpuInfo->CpuCommonFeatures & HDC_SUPPORT) {
      SetupCpuFeatures.HdcSupported = 1;
    }
  }



  ///
  /// set C6DRAM support
  ///
  SetupCpuFeatures.C6DramSupported = IsC6dramSupported ();

  ///
  /// Processor Trace feature is supported only on BDW and later,
  /// with CPUID.(EAX=7):EBX[25] = 1
  ///
  SetupCpuFeatures.ProcTraceSupported = (UINT8) IsIntelProcessorTraceSupported ();

  ///
  /// Set TME supported
  ///
  SetupCpuFeatures.TmeSupported = (UINT8) IsTmeSupported ();

  Status = gRT->SetVariable (
                  L"SetupCpuFeatures",
                  &gSetupVariableGuid,
                  SetupCpuFeaturesAttr,
                  sizeof (SETUP_CPU_FEATURES),
                  &SetupCpuFeatures
                  );
  ASSERT_EFI_ERROR(Status);

  //
  // Display Bist information to the user if Bist is enabled in Setup
  //
  if (mCpuSetup.BistOnReset) {
    //
    // Create an Ready to Boot event.
    //
    Status = EfiCreateEventReadyToBootEx (
               TPL_CALLBACK,
               SelfTestBistErrMsg,
               NULL,
               &ReadyToBootEvent
               );
    ASSERT_EFI_ERROR (Status);
  }

  gBS->CloseEvent (Event);
}

EFI_STATUS
CpuSetupInit (
  VOID
  )
{
  VOID                    *Registration1;
  VOID                    *Registration2;

  EfiCreateProtocolNotifyEvent (
    &gEfiMpServiceProtocolGuid,
    TPL_CALLBACK,
    CpuSetupInitCallback,
    NULL,
    &Registration1
    );

  EfiCreateProtocolNotifyEvent (
    &gEfiVariableWriteArchProtocolGuid,
    TPL_CALLBACK,
    CpuSetupInitCallback,
    NULL,
    &Registration2
    );

  return EFI_SUCCESS;
}

VOID
EFIAPI
PlatformPcdInit (
  VOID
  )
{
  UINTN                 VarSize;
  EFI_STATUS            Status;
  CPU_SETUP             CpuSetup;
  SETUP_DATA            SetupData;

  VarSize = sizeof (SETUP_DATA);
  Status = gRT->GetVariable (
                  L"Setup",
                  &gSetupVariableGuid,
                  NULL,
                  &VarSize,
                  &SetupData
                  );
  if (EFI_ERROR (Status)) {
    DEBUG((DEBUG_ERROR, "Failed to get setup variable\n"));
    return;
  }

  VarSize = sizeof (CPU_SETUP);
  Status = gRT->GetVariable (
                  L"CpuSetup",
                  &gCpuSetupVariableGuid,
                  NULL,
                  &VarSize,
                  &CpuSetup
                  );
  if (EFI_ERROR (Status)) {
    DEBUG((DEBUG_ERROR, "Failed to get CPU Setup variable\n"));
    return;
  }

  if (SetupData.FastBoot) {
    //
    // Disable MdeModulePkg core PcdAtaSmartEnable to skip HDD SMART enabling
    //
    PcdSetBoolS (PcdAtaSmartEnable, FALSE);
    //
    // Enable gEfiMdeModulePkgTokenSpaceGuid PcdFastPS2Detection to enable quick PS2 device detection
    //
    PcdSetBoolS (PcdFastPS2Detection, TRUE);
  }

  //
  // Change PcdAcpiDebugFeatureActive PCD to TRUE if ACPI DEBUG is enabled in Setup menu
  //
  if (SetupData.AcpiDebug) {
    PcdSetBoolS (PcdAcpiDebugFeatureActive, TRUE);
  } else {
    PcdSetBoolS (PcdAcpiDebugFeatureActive, FALSE);
  }

  //
  // Change PcdRuntimeOverclockEnable PCD to TRUE if Over-clocking Support is enabled in Setup menu
  //
  if (CpuSetup.OverclockingSupport) {
    PcdSetBoolS (PcdRuntimeOverclockEnable, TRUE);
  } else {
    PcdSetBoolS (PcdRuntimeOverclockEnable, FALSE);
  }

  //
  // Update AHCI max ports
  //
  if (MaxSataControllerNum () != 0) {
    PcdSet8S (PcdAhciMaxPorts, MaxSataPortNum (SATA_1_CONTROLLER_INDEX));
  }

  return;
}

/**
This is the callback function for PCI ENUMERATION COMPLETE.
**/
VOID
EFIAPI
ResetOnPciEnumComplete (
  IN EFI_EVENT    Event,
  IN VOID         *Context
)
{
  EFI_STATUS          Status;
  VOID                *ProtocolPointer;

  ///
  /// Check if this is first time called by EfiCreateProtocolNotifyEvent() or not,
  /// if it is, we will skip it until real event is triggered
  ///
  Status = gBS->LocateProtocol(&gEfiPciEnumerationCompleteProtocolGuid, NULL, (VOID **)&ProtocolPointer);
  if (EFI_SUCCESS != Status) {
    return;
  }
  gBS->CloseEvent(Event);
  //
  // Reset the system.
  //
  gRT->ResetSystem(EfiResetWarm, EFI_SUCCESS, 0, NULL);
}

/**
This function update the setup option with hob value.

**/
STATIC
VOID
HybridStorageSync (
  VOID
)
{
  PCH_SETUP               PchSetup;
  UINT32                  SetupVolAttributes;
  VOID                    *Hob;
  PCH_HYBRIDSTORAGE_HOB   *HybridStorageHob;
  EFI_STATUS              Status;
  VOID                    *Registration;
  UINTN                   VariableSize;

  VariableSize = sizeof (PCH_SETUP);
  ZeroMem(&PchSetup, VariableSize);
  Status = gRT->GetVariable(
                  L"PchSetup",
                  &gPchSetupVariableGuid,
                  &SetupVolAttributes,
                  &VariableSize,
                  &PchSetup);
  if (EFI_ERROR(Status)) {
    ASSERT_EFI_ERROR(Status);
    return;
  }
  if (PchSetup.CpuRootportUsedForHybridStorage == 0xFF) {
    return;
  }

  Hob = GetFirstGuidHob(&gHybridStorageHobGuid);
  if (Hob == NULL) {
    return;
  }
  HybridStorageHob = (PCH_HYBRIDSTORAGE_HOB *)GET_GUID_HOB_DATA(Hob);

  if (HybridStorageHob->CpuHybridStorageConnected != PchSetup.CpuHybridStorageConnected) {
    PchSetup.CpuHybridStorageConnected = HybridStorageHob->CpuHybridStorageConnected;
    Status = gRT->SetVariable(
                    L"PchSetup",
                    &gPchSetupVariableGuid,
                    SetupVolAttributes,
                    VariableSize,
                    &PchSetup
                    );
    ///
    /// Create PCI Enumeration Completed callback for Reset
    ///
    EfiCreateProtocolNotifyEvent(
      &gEfiPciEnumerationCompleteProtocolGuid,
      TPL_CALLBACK,
      ResetOnPciEnumComplete,
      NULL,
      &Registration
    );
  }

}
/**
  This is the standard  driver entry point for the Platform Setup Driver.
  This driver is responsible for setting up any platform specific policy or
  initialization information.

  @param[in] ImageHandle     - Handle for the image of this driver
  @param[in] SystemTable     - Pointer to the EFI System Table

  @retval EFI_SUCCESS        - Policy decisions are set successfully
**/
//[-start-200902-IB17800095-modify]//
EFI_STATUS
EFIAPI
PlatformSetupEntry (
//  IN EFI_HANDLE         ImageHandle,
//  IN EFI_SYSTEM_TABLE   *SystemTable
  IN OUT CHIPSET_CONFIGURATION  *SetupNvData
  )
//[-end-200902-IB17800095-modify]//
{
  EFI_STATUS                         Status;
  EFI_HANDLE                         Handle;
  UINTN                              VariableSize;
  UINT32                             SetupVolAttributes;
  MEM_INFO_PROTOCOL                  *MemoryInfoProtocol;

  Handle        = NULL;

  Status = gBS->LocateProtocol (&gEfiPciRootBridgeIoProtocolGuid, NULL, (VOID **) &mPciRootBridgeIo);
  ASSERT_EFI_ERROR (Status);

  Status = gBS->LocateProtocol (&gSaPolicyProtocolGuid, NULL, (VOID **) &mSaPolicy);
  ASSERT_EFI_ERROR (Status);

  //
  // Change PCD's value that depend on xxxSetup variable
  //
  PlatformPcdInit();

  //
  // Hybrid storage
  //
  HybridStorageSync ();

  //
  // Get platform Setup setting.
  //
  // @todo missing SETUP_DATA.MicrocodeUpdateSignature and SETUP_DATA.InterruptFilteringEnable
  //
  VariableSize = sizeof (CPU_SETUP);
  ZeroMem (&mCpuSetup, VariableSize);
  gRT->GetVariable (L"CpuSetup", &gCpuSetupVariableGuid, NULL, &VariableSize, &mCpuSetup);

  //
  // Create and zero initialize SetupVolatileData variable.
  //
  VariableSize = sizeof(SETUP_VOLATILE_DATA);
  Status = gRT->GetVariable (
                  L"SetupVolatileData",
                  &gSetupVariableGuid,
                  &SetupVolAttributes,
                  &VariableSize,
                  &mSetupVolatileData
                  );
  ASSERT (Status == EFI_NOT_FOUND);

//[-start-200902-IB17800095-add]//
  //
  // Get VTd status
  //
  mSetupVolatileData.VTdAvailable   = (MmioRead32 (HbPciD0F0RegBase + 0xe4) & BIT23) ? 0 : 1;
  mSetupVolatileData.PlatId         = PcdGet16 (PcdBoardId);
//[-end-200902-IB17800095-add]//
  mSetupVolatileData.PlatformFlavor = PcdGet8 (PcdPlatformFlavor); // Platform Reset will check this data for SusPwrDnAck setting
  if (PcdGet8 (PcdSkuType) == AdlPSkuType) {
    mSetupVolatileData.MobileOcUnSupport = TRUE;
  }
  SetupVolAttributes = EFI_VARIABLE_BOOTSERVICE_ACCESS;
  Status = gRT->SetVariable (
                  L"SetupVolatileData",
                  &gSetupVariableGuid,
                  SetupVolAttributes,
                  sizeof(SETUP_VOLATILE_DATA),
                  &mSetupVolatileData
                  );
  ASSERT_EFI_ERROR(Status);


  //
  // Initialize some variables for CPU Setup Page
  //
  CpuSetupInit();

  //
  // Initialize the MEM_INFO_PROTOCOL structure
  //
  MemoryInfoProtocol = AllocateZeroPool (sizeof (MEM_INFO_PROTOCOL));
  if (MemoryInfoProtocol == NULL) {
    return EFI_OUT_OF_RESOURCES;
  }

//[-start-200902-IB17800095-remove]//
#if 0
  //
  // Set up the policy protocols
  //
  SetupPlatformPolicies (
    MemoryInfoProtocol
    );
  Status = gBS->InstallMultipleProtocolInterfaces (
                  &Handle,
                  &gMemInfoProtocolGuid,
                  MemoryInfoProtocol,
                  &gDxePolicyProtocolGuid,
                  NULL,
                  NULL
                  );
  ASSERT_EFI_ERROR (Status);

  if (PcdGetBool (PcdEcPresent) == TRUE) {
    Status = gBS->InstallProtocolInterface (
                    &Handle,
                    &gEfiPs2PolicyProtocolGuid,
                    EFI_NATIVE_INTERFACE,
                    &mPs2PolicyData
                    );
    ASSERT_EFI_ERROR (Status);
  }
#endif
//[-end-200902-IB17800095-remove]//

  return EFI_SUCCESS;
}
