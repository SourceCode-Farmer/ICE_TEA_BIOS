## @file
#  Platform Package Description file
#
#******************************************************************************
#* Copyright (c) 2014 - 2021, Insyde Software Corp. All Rights Reserved.
#*
#* You may not reproduce, distribute, publish, display, perform, modify, adapt,
#* transmit, broadcast, present, recite, release, license or otherwise exploit
#* any part of this publication in any form, by any means, without the prior
#* written permission of Insyde Software Corporation.
#*
#******************************************************************************
##

[Defines]
DEFINE      CHIPSET_PKG                     = AlderLakeChipsetPkg
DEFINE      PLATFORMFSP_PACKAGE             = AlderLakeFspPkg
DEFINE      CLIENT_COMMON_PACKAGE           = ClientCommonPkg
DEFINE      PLATFORMSAMPLE_PACKAGE          = AlderLakePlatSamplePkg

#[-start-201030-IB16810136-add]#
  DEFINE    FMP_CLIENT_PLATFORM_SYSTEM_BIOS   = 6C8E136F-D3E6-4131-AC32-4687CB4ABD27 # gFmpDevicePlatformBiosGuid
#[-start-201230-IB16810143-add]#
  DEFINE    FMP_CLIENT_PLATFORM_SYSTEM_BTGACM = 4E88068B-41B2-4E05-893C-DB0B43F7D348 # gFmpDevicePlatformBtGAcmGuid
#[-end-201230-IB16810143-add]#
#[-start-201112-IB16810138-add]#
  DEFINE    FMP_CLIENT_PLATFORM_SYSTEM_ME     = 0EAB05C1-766A-4805-A039-3081DE0210C7 # gFmpDevicePlatformMeGuid
#[-end-201112-IB16810138-add]#
#[-end-201030-IB16810136-add]#
#[-start-201130-IB16810140-add]#
  DEFINE    FMP_CLIENT_PLATFORM_SYSTEM_UCODE  = 69585D92-B50A-4AD7-B265-2EB1AE066574 # gFmpDevicePlatformuCodeGuid
#[-end-201130-IB16810140-add]#

!include $(PLATFORMSAMPLE_PACKAGE)/Package.env

[Packages]

[PcdsFeatureFlag]

[PcdsDynamicDefault.common.DEFAULT]

[PcdsDynamicDefault]
!if gSiPkgTokenSpaceGuid.PcdFspWrapperEnable == TRUE
  #
  # FSP Binary base address will be set in FDF basing on flash map
  #
  # gIntelFsp2WrapperTokenSpaceGuid.PcdFspmBaseAddress|0
  # gIntelFsp2WrapperTokenSpaceGuid.PcdFspsBaseAddress|0
!endif

[Libraries]

[LibraryClasses.common]
#[-start-200911-IB17040161-add]#
 #
 # Telemetry
 #
 TelemetryFviLib|$(PLATFORM_FULL_PACKAGE)/Telemetry/Library/DxeTelemetryFviLib/DxeTelemetryFviLib.inf
 DxeTelemetryAcpiLib|$(PLATFORM_FULL_PACKAGE)/Telemetry/Library/DxeTelemetryAcpiLib/DxeTelemetryAcpiLib.inf
#[-end-200911-IB17040161-add]#

#
# Platform
#
!if gSiPkgTokenSpaceGuid.PcdFspWrapperEnable == TRUE
  PlatformInitLib|$(PLATFORMSAMPLE_PACKAGE)/Library/PlatformInitLib/PlatformInitLib.inf
!endif

  DisplayUpdateProgressLib|$(PLATFORM_FEATURES_PATH)/CapsuleUpdate/Library/PlatformDisplayUpdateProgressLib/PlatformDisplayUpdateProgressLib.inf

#
# PlatSamplePkg/Library had the same library but Intel no use it.
# Follow Intel RC, we don't use it too.
#
#[-start-191111-IB10189001-modify]#
# Insyde still need FwUpdateLib
#[-start-210413-IB16810148-remove]#
#FwUpdateLib|$(PLATFORM_FEATURES_PATH)/CapsuleUpdate/Library/FwUpdateLib/FwUpdateLib.inf
#[-end-210413-IB16810148-remove]#
!if gPlatformModuleTokenSpaceGuid.PcdCapsuleEnable == FALSE
  # PlatformFvAddressLib|$(PLATFORM_FULL_PACKAGE)/Library/PlatformFvAddressLib/PlatformFvAddressLib.inf
!endif
#[-start-191111-IB10189001-modify]#

  PlatformNvRamHookLib|$(PLATFORM_FEATURES_PATH)/Cmos/Library/PlatformNvRamHookLibCmos/PlatformNvRamHookLibCmos.inf
  CmosAccessLib|$(PLATFORM_FEATURES_PATH)/Cmos/Library/CmosAccessLib/CmosAccessLib.inf
  PlatformCmosAccessLib|$(PLATFORM_FEATURES_PATH)/Cmos/Library/PlatformCmosAccessLib/PlatformCmosAccessLib.inf

  SafeIntLib|MdePkg/Library/BaseSafeIntLib/BaseSafeIntLib.inf
  BmpSupportLib|MdeModulePkg/Library/BaseBmpSupportLib/BaseBmpSupportLib.inf

#
# HsPhy
#
  HsPhyDashboardLib|$(PLATFORM_FEATURES_PATH)/PciePhyFwLoading/Library/PeiDxeSmmHsPhyDashboardLib/PeiDxeSmmHsPhyDashboardLib.inf

#[-start-210105-IB19010015-removed]#
#[-start-201211-IB17510133-add]#
#  VmgExitLib|UefiCpuPkg/Library/VmgExitLibNull/VmgExitLibNull.inf
#[-end-201211-IB17510133-add]#
#[-end-210105-IB19010015-removed]#


  MicrocodeLib|UefiCpuPkg/Library/MicrocodeLib/MicrocodeLib.inf


#[-start-201030-IB16810136-add]#
!if gPlatformModuleTokenSpaceGuid.PcdCapsuleEnable == TRUE
  PlatformFlashAccessLib|$(PLATFORMSAMPLE_PACKAGE)/Features/CapsuleUpdate/Library/PlatformFlashAccessLib/PlatformFlashAccessLib.inf
  ComponentUpdateLib|$(PLATFORMSAMPLE_PACKAGE)/Features/CapsuleUpdate/Library/ComponentUpdateLib/ComponentUpdateLib.inf
  SeamlessRecoverySupportLib|$(PLATFORMSAMPLE_PACKAGE)/Features/CapsuleUpdate/Library/SeamlessRecoverySupportLib/SeamlessRecoverySupportLib.inf
#[-start-210426-IB16810151-modify]#
#[-start-201112-IB16810138-add]#
  SeamlessRecoveryVarLockLib|$(PLATFORMSAMPLE_PACKAGE)/Features/CapsuleUpdate/Library/SeamlessRecoverySupportLib/SeamlessRecoveryVarLockLib.inf
#[-end-201112-IB16810138-add]#
#[-end-210426-IB16810151-modify]#
!endif
#[-end-201030-IB16810136-add]#
#[-start-201217-IB16560232-add]#
  Usb3DebugPortDummyLib|$(PLATFORM_FULL_PACKAGE)/Library/BaseUsb3DebugPortDummyLib/Usb3DebugPortDummyLib.inf
!if gPlatformModuleTokenSpaceGuid.PcdSerialPortEnable == TRUE
  SerialPortLib|$(PLATFORM_FULL_PACKAGE)/Library/BaseSerialPortLib/BaseSerialPortLib.inf
!else
  SerialPortLib|MdePkg/Library/BaseSerialPortLibNull/BaseSerialPortLibNull.inf
!endif
#[-end-201217-IB16560232-add]#

[LibraryClasses]
#[-start-200420-IB17800058-2-modify]#
#ADL RC 1181.1 new Nhl , temporary remove
  #DxeInstallNhltTableLib|$(PLATFORMSAMPLE_PACKAGE)/Library/DxeInstallNhltTableLib/DxeInstallNhltTableLib.inf
#[-end-200420-IB17800058-2-modify]#

##[-start-180509-IB11270200-modify]#
  SerialPortLib|$(PLATFORMSAMPLE_PACKAGE)/Library/BaseSerialPortLib/BaseSerialPortLib.inf
##[-end-180509-IB11270200-modify]#


####
#### Override
####
!if gChipsetPkgTokenSpaceGuid.PcdUseCrbEcFlag == TRUE
  EcMiscLib|$(PLATFORMSAMPLE_PACKAGE)/Library/BaseEcMiscLib/BaseEcMiscLib.inf
  EcLib|$(PLATFORMSAMPLE_PACKAGE)/Library/BaseEcLib/BaseEcLib.inf
  EcHwLib|$(PLATFORMSAMPLE_PACKAGE)/Library/BaseEcHwLib/BaseEcHwLib.inf
  EcTcssLib|$(PLATFORMSAMPLE_PACKAGE)/Library/BaseEcTcssLib/BaseEcTcssLib.inf
!else
  EcMiscLib|$(PLATFORMSAMPLE_PACKAGE)/Library/BaseEcMiscLibNull/BaseEcMiscLibNull.inf
  EcLib|$(PLATFORMSAMPLE_PACKAGE)/Library/BaseEcLibNull/BaseEcLibNull.inf
  EcTcssLib|$(PLATFORMSAMPLE_PACKAGE)/Library/BaseEcTcssLibNull/BaseEcTcssLibNull.inf
!endif

  PeiWdtAppLib|$(PLATFORMSAMPLE_PACKAGE)/Library/PeiWdtAppLib/PeiWdtAppLib.inf

!if gSiPkgTokenSpaceGuid.PcdFspWrapperEnable == TRUE
  FspWrapperPlatformLib|$(PLATFORMSAMPLE_PACKAGE)/FspWrapper/Library/PeiFspWrapperPlatformLib/PeiFspWrapperPlatformLib.inf
  FspWrapperHobProcessLib|$(PLATFORMSAMPLE_PACKAGE)/FspWrapper/Library/PeiFspWrapperHobProcessLib/PeiFspWrapperHobProcessLib.inf
!endif

[LibraryClasses.common]
  SerialPortParameterLib|$(PLATFORM_FULL_PACKAGE)/Library/BaseSerialPortParameterLibPcd/BaseSerialPortParameterLibPcd.inf
#[-start-201217-IB16560232-add]#
  SerialIoUartDebugHelperLib|$(PLATFORM_FULL_PACKAGE)/Library/BaseSerialIoUartDebugHelperLib/BaseSerialIoUartDebugHelperLib.inf
  SerialIoUartDebugPropertyLib|$(PLATFORM_FULL_PACKAGE)/Library/SerialIoUartDebugPropertyLib/BaseSerialIoUartDebugPropertyLib.inf
#[-end-201217-IB16560232-add]#

  DxeTbtSecurityLib|$(PLATFORM_FEATURES_PATH)/Tbt/Library/DxeTbtSecurityLib/DxeTbtSecurityLib.inf
  DTbtCommonLib|$(PLATFORM_FEATURES_PATH)/Tbt/Library/DTbtCommonLib/DTbtCommonLib.inf


[LibraryClasses.common.SEC]
!if gSiPkgTokenSpaceGuid.PcdFspWrapperEnable == TRUE
  PlatformSecLib|$(PLATFORMSAMPLE_PACKAGE)/FspWrapper/Library/SecFspWrapperPlatformSecLib/SecFspWrapperPlatformSecLib.inf
!endif

[LibraryClasses.common.PEI_CORE]
  SerialPortParameterLib|$(PLATFORM_FULL_PACKAGE)/Library/PeiSerialPortParameterLib/PeiSerialPortParameterLib.inf

[LibraryClasses.IA32.SEC]
#[-start-201217-IB16560232-add]#
  Usb3DebugPortLib|Usb3DebugFeaturePkg/Library/Usb3DebugPortLib/Usb3DebugPortLibNull.inf
  SerialPortParameterLib|$(PLATFORM_FULL_PACKAGE)/Library/BaseSerialPortParameterLibPcd/BaseSerialPortParameterLibPcd.inf
!if $(TARGET) == DEBUG
  DebugPrintErrorLevelLib|MdePkg/Library/BaseDebugPrintErrorLevelLib/BaseDebugPrintErrorLevelLib.inf
!endif
#[-end-201217-IB16560232-add]#

[LibraryClasses.IA32]
  SetupDataCacheLib|$(PLATFORM_BOARD_PACKAGE)/Features/Setup/Library/PeiSetupDataCacheLib/PeiSetupDataCacheLib.inf

!if gSiPkgTokenSpaceGuid.PcdFspWrapperEnable == TRUE
  FspWrapperPlatformResetLib|$(PLATFORMSAMPLE_PACKAGE)/FspWrapper/Library/PeiFspWrapperPlatformResetLib/PeiFspWrapperPlatformResetLib.inf
!endif

#[-start-201217-IB16560232-add]#
!if gPlatformModuleTokenSpaceGuid.PcdUsb3SerialStatusCodeEnable == TRUE
  Usb3DebugPortLib|Usb3DebugFeaturePkg/Library/Usb3DebugPortLib/Usb3DebugPortLibPei.inf
!else
  Usb3DebugPortLib|Usb3DebugFeaturePkg/Library/Usb3DebugPortLib/Usb3DebugPortLibNull.inf
!endif
#[-end-201217-IB16560232-add]#

!if gIntelFsp2WrapperTokenSpaceGuid.PcdFspModeSelection == TRUE
  PeiPolicyBoardConfigLib|$(PLATFORM_BOARD_PACKAGE)/Library/PeiPolicyBoardConfigLib/PeiPolicyBoardConfigLibFsp.inf
  PeiPolicyUpdateLib|$(PLATFORMSAMPLE_PACKAGE)/Library/PeiPolicyUpdateLib/PeiPolicyUpdateLibFsp.inf
  PeiPolicyDebugLib|$(PLATFORMSAMPLE_PACKAGE)/Library/PeiPolicyDebugLib/PeiPolicyDebugLibFsp.inf
!else
  PeiPolicyBoardConfigLib|$(PLATFORM_BOARD_PACKAGE)/Library/PeiPolicyBoardConfigLib/PeiPolicyBoardConfigLib.inf
  PeiPolicyUpdateLib|$(PLATFORMSAMPLE_PACKAGE)/Library/PeiPolicyUpdateLib/PeiPolicyUpdateLib.inf
  PeiPolicyDebugLib|$(PLATFORMSAMPLE_PACKAGE)/Library/PeiPolicyDebugLib/PeiPolicyDebugLib.inf
!endif

#
# TBT
#
  PeiDTbtPolicyLib|$(PLATFORM_FEATURES_PATH)/Tbt/Library/PeiDTbtPolicyLib/PeiDTbtPolicyLib.inf
!if gPlatformModuleTokenSpaceGuid.PcdDTbtEnable == TRUE
  PeiDTbtInitLib|$(PLATFORM_FEATURES_PATH)/Tbt/Library/Private/PeiDTbtInitLib/PeiDTbtInitLib.inf
!else
  PeiDTbtInitLib|$(PLATFORM_FEATURES_PATH)/Tbt/Library/Private/PeiDTbtInitLibNull/PeiDTbtInitLibNull.inf
!endif


####
#### Override
####
!if gSiPkgTokenSpaceGuid.PcdFspWrapperEnable == TRUE
  FspWrapperPlatformResetLib|$(PLATFORMSAMPLE_PACKAGE)/FspWrapper/Library/PeiFspWrapperPlatformResetLib/PeiFspWrapperPlatformResetLib.inf
!endif

!if gSiPkgTokenSpaceGuid.PcdFspWrapperEnable == TRUE
  FspWrapperExtractGuidedLib|$(PLATFORMSAMPLE_PACKAGE)/FspWrapper/Library/PeiFspWrapperExtractGuidedLib/PeiFspWrapperExtractGuidedLib.inf
!endif

  PeiCpuPcieHsPhyInitLib|$(PLATFORM_FEATURES_PATH)/PciePhyFwLoading/Library/PeiCpuPcieHsPhyInitLib/PeiCpuPcieHsPhyInitLib.inf
  PeiCpuPcieHashMiscLib| $(PLATFORM_FEATURES_PATH)/PciePhyFwLoading/Library/PeiCpuPcieHashMiscLib/PeiCpuPcieHashMiscLib.inf

TraceHubDebugLib|$(PLATFORM_SI_PACKAGE)/Library/TraceHubDebugLibSvenTx/PeiTraceHubDebugLibSvenTx.inf
!if gPlatformModuleTokenSpaceGuid.PcdUsb3SerialStatusCodeEnable == TRUE
  Usb3DebugPortLib|Usb3DebugFeaturePkg/Library/Usb3DebugPortLib/Usb3DebugPortLibPei.inf
!else
  Usb3DebugPortLib|Usb3DebugFeaturePkg/Library/Usb3DebugPortLib/Usb3DebugPortLibNull.inf
!endif

#[-start-200420-IB17800056-add]#
  BiosIdLib|BoardModulePkg/Library/BiosIdLib/PeiBiosIdLib.inf
#[-end-200420-IB17800056-add]#
#[-start-201217-IB16560232-add]#
  SerialPortParameterLib|$(PLATFORM_FULL_PACKAGE)/Library/PeiSerialPortParameterLib/PeiSerialPortParameterLib.inf
#[-end-201217-IB16560232-add]#

[LibraryClasses.IA32.PEIM]
#
# PEI Boot State Library Instance
#
  PeiBootStateLib|$(PLATFORM_FULL_PACKAGE)/Library/PeiBootStateLib/PeiBootStateLib.inf
  #
  # Telemetry.
  #
  TelemetryFviLib|$(PLATFORM_FULL_PACKAGE)/Telemetry/Library/PeiTelemetryFviLib/PeiTelemetryFvilib.inf


[LibraryClasses.X64]
  DxeDTbtPolicyLib|$(PLATFORM_FEATURES_PATH)/Tbt/Library/DxeDTbtPolicyLib/DxeDTbtPolicyLib.inf
  DxePolicyBoardConfigLib|$(PLATFORM_FULL_PACKAGE)/Library/DxePolicyBoardConfigLib/DxePolicyBoardConfigLib.inf
  DxeAcpiGnvsInitLib|$(PLATFORM_FULL_PACKAGE)/Library/DxeAcpiGnvsInitLib/DxeAcpiGnvsInitLib.inf
  TraceHubDebugLib|$(PLATFORM_SI_PACKAGE)/Library/TraceHubDebugLibSvenTx/DxeTraceHubDebugLibSvenTx.inf
#[-start-200420-IB17800056-add]#
  BiosIdLib|BoardModulePkg/Library/BiosIdLib/DxeBiosIdLib.inf
#[-end-200420-IB17800056-add]#
!if (gPlatformModuleTokenSpaceGuid.PcdRemotePlatformEraseSupport == TRUE) OR (gPlatformModuleTokenSpaceGuid.PcdEnableSecureErase == TRUE)
  SecureEraseDxeLib|$(PLATFORM_FEATURES_PATH)/Amt/Library/SecureEraseDxeLib/SecureEraseDxeLib.inf
!endif

#
# DXE Boot State Library Instance
#
  DxeBootStateLib|$(PLATFORM_FULL_PACKAGE)/Library/DxeBootStateLib/DxeBootStateLib.inf

  PdtUpdateLib|$(PLATFORM_FULL_PACKAGE)/Library/PdtUpdateLib/PdtUpdateLib.inf
#[-start-201217-IB16560232-add]#
  SerialPortParameterLib|$(PLATFORM_FULL_PACKAGE)/Library/DxeSmmSerialPortParameterLib/DxeSmmSerialPortParameterLib.inf
!if gPlatformModuleTokenSpaceGuid.PcdUsb3SerialStatusCodeEnable == TRUE
  Usb3DebugPortLib|Usb3DebugFeaturePkg/Library/Usb3DebugPortLib/Usb3DebugPortLibDxe.inf
!else
  Usb3DebugPortLib|Usb3DebugFeaturePkg/Library/Usb3DebugPortLib/Usb3DebugPortLibNull.inf
!endif
#[-end-201217-IB16560232-add]#

#[-start-210720-IB16810158-add]#
  BluetoothLib|BluetoothPkg/Library/BluetoothLib/BluetoothLib.inf
  HidLib|BluetoothPkg/Library/HidLib/HidLib.inf
#[-end-210720-IB16810158-add]#

[LibraryClasses.X64.PEIM]
  SerialPortParameterLib|$(PLATFORM_FULL_PACKAGE)/Library/PeiSerialPortParameterLib/PeiSerialPortParameterLib.inf

[LibraryClasses.X64.DXE_CORE]
  SerialPortParameterLib|$(PLATFORM_FULL_PACKAGE)/Library/DxeCoreSerialPortParameterLib/DxeCoreSerialPortParameterLib.inf

[LibraryClasses.X64.DXE_SMM_DRIVER]
  DxeSmmAcpiCustomLib|$(PLATFORM_FULL_PACKAGE)/Library/DxeSmmAcpiCustomLib/DxeSmmAcpiCustomLib.inf

[LibraryClasses.common.DXE_RUNTIME_DRIVER]
  SerialPortParameterLib|$(PLATFORMSAMPLE_PACKAGE)/Library/DxeSmmSerialPortParameterLib/DxeSmmSerialPortParameterLib.inf
#[-start-201217-IB16560232-add]#
!if $(TARGET) == DEBUG
  SerialIoUartDebugPropertyLib|$(PLATFORM_FULL_PACKAGE)/Library/SerialIoUartDebugPropertyLib/DxeSmmSerialIoUartDebugPropertyLib.inf
!endif
#[-end-201217-IB16560232-add]#

[LibraryClasses.common.UEFI_DRIVER]


[LibraryClasses.common.DXE_DRIVER]
  DxePolicyUpdateLib|$(PLATFORMSAMPLE_PACKAGE)/Library/DxePolicyUpdateLib/DxePolicyUpdateLib.inf
  DxeFirmwareVersionInfoLib|$(PLATFORM_FULL_PACKAGE)/Library/DxeFirmwareVersionInfoLib/DxeFirmwareVersionInfoLib.inf

  FspWrapperPlatformLib|$(PLATFORM_PACKAGE)/FspWrapper/Library/DxeFspWrapperPlatformLib/DxeFspWrapperPlatformLib.inf

[LibraryClasses.common.DXE_SMM_DRIVER]
  DxeSmmScriptLib|$(PLATFORM_FULL_PACKAGE)/Library/DxeSmmScriptLib/DxeSmmScriptLib.inf

[LibraryClasses.common.COMBINED_SMM_DXE]

[LibraryClasses.common.SMM_CORE]

[LibraryClasses.common.UEFI_APPLICATION]
[LibraryClasses.common.SMM_CORE]

[PcdsFeatureFlag]

[PcdsFixedAtBuild]

[PcdsDynamicDefault]

[PcdsDynamicExDefault.common.DEFAULT]

[PcdsDynamicExDefault]

[PcdsPatchableInModule]

[PcdsDynamicHii.X64.DEFAULT]

[Components]

[Components.$(PEI_ARCH)]
#[-start-190806-IB16740000-add]
  UefiCpuPkg/SecCore/SecCore.inf {
    <LibraryClasses>
#[-start-190730-IB17700060-modify]#
       TimerLib|$(CHIPSET_PKG)/Override/EDK2/PcAtChipsetPkg/Library/TscAcpiTimerLib/BaseTscTimerLib.inf
#[-end-190730-IB17700060-modify]#
#[-start-201217-IB16560232-modify]#
!if $(TARGET) == DEBUG
      DebugLib|$(PLATFORM_FULL_PACKAGE)/Library/BaseDebugLibAllDebugPort/BaseDebugLibAllDebugPort.inf
      DebugPrintErrorLevelLib|MdePkg/Library/BaseDebugPrintErrorLevelLib/BaseDebugPrintErrorLevelLib.inf
      TraceHubDebugLib|$(PLATFORM_SI_PACKAGE)/Library/TraceHubDebugLibSvenTx/BaseTraceHubDebugLibSvenTx.inf
!endif
#[-end-201217-IB16560232-modify]#
  }
#[-end-190806-IB16740000-add]
#[-start-190702-16990087-add]#
!if gBoardModuleTokenSpaceGuid.PcdAdvancedFeatureEnable == TRUE
  $(PLATFORMSAMPLE_PACKAGE)/PlatformInitAdvanced/PlatformInitAdvancedPei/PlatformInitAdvancedPreMem.inf
!endif
#[-end-190702-16990087-add]#

!if gBoardModuleTokenSpaceGuid.PcdAdvancedFeatureEnable == TRUE
  $(PLATFORM_FULL_PACKAGE)/PlatformInitAdvanced/PlatformInitAdvancedPei/PlatformInitAdvancedPostMem.inf
!endif # PcdAdvancedFeatureEnable

#[-start-201030-IB16810136-remove]#
#!if gPlatformModuleTokenSpaceGuid.PcdCapsuleEnable == TRUE
#  $(PLATFORM_FULL_PACKAGE)/Features/CapsuleUpdate/PlatformInitRecovery/PlatformInitRecoveryPei.inf {
#  <LibraryClasses>
#    PeiPlatformRecoveryLib|$(PLATFORM_FULL_PACKAGE)/Features/CapsuleUpdate/Library/PeiPlatformRecoveryLib/PeiPlatformRecoveryLib.inf
#  }
#!endif
#[-end-201030-IB16810136-remove]#
#[-start-201223-IB19010009-remove]#
#!if gSiPkgTokenSpaceGuid.PcdAmtEnable == TRUE
#  $(PLATFORMSAMPLE_PACKAGE)/Features/Amt/AmtStatusCodePei/AmtStatusCodePei.inf
#!endif
#[-end-201223-IB19010009-remove]#

#################################################################################
##
## Components.IA32 Override
##
#################################################################################
#
## The override should be removed after the DDT debug drivers are ready for SKL from kernel code.
##
## Fix Post hang on CP A2 issue
##

!if gSiPkgTokenSpaceGuid.PcdFspWrapperEnable == TRUE
!if gSiPkgTokenSpaceGuid.PcdPpamEnable == TRUE
  $(PLATFORMSAMPLE_PACKAGE)/Features/NiftyRock/MsegSmramPei/MsegSmramPei.inf
!endif
  $(PLATFORMSAMPLE_PACKAGE)/FspWrapper/FspWrapperPeim/FspWrapperPeim.inf
#[-start-200911-IB17040161-add]#
  #
  # Telemetry
  #
  $(PLATFORM_FULL_PACKAGE)/Telemetry/FspFirmwareVersionPeim/FspFirmwareVersionPeim.inf
#[-end-200911-IB17040161-add]#
!endif

#[-start-200911-IB17040161-add]#
#
# Telemetry
#
!if gIntelFsp2WrapperTokenSpaceGuid.PcdFspModeSelection == 1
  $(PLATFORM_FULL_PACKAGE)/Telemetry/BiosGuardFirmwareVersionPeim/BiosGuardFirmwareVersionPeimFsp.inf
!else
#[-start-210730-IB16560267-modify]#
  $(PLATFORM_FULL_PACKAGE)/Telemetry/BiosGuardFirmwareVersionPeim/BiosGuardFirmwareVersionPeim.inf {
    <Depex>
    gEfiPeiMemoryDiscoveredPpiGuid
  }
#[-end-210730-IB16560267-modify]#
!endif

#
# VTd Iommu Security for Pei
#
  $(PLATFORM_FEATURES_PATH)/VTd/PlatformVTdInfoSamplePei/PlatformVTdInfoSamplePei.inf {
#    !if gPlatformModuleTokenSpaceGuid.PcdResiliencyEnable == TRUE
    !if gInsydeTokenSpaceGuid.PcdH2OBiosUpdateFaultToleranceResiliencyEnabled == TRUE
    # To reduce FvPrememory size in Resiliency debug build
      <LibraryClasses>
        DebugLib|MdePkg/Library/BaseDebugLibNull/BaseDebugLibNull.inf
    !endif
  }

#[-end-200911-IB17040161-add]#
!if gIntelFsp2WrapperTokenSpaceGuid.PcdFspModeSelection == 1
  $(PLATFORM_FULL_PACKAGE)/FspWrapper/DebugEventHandler/DebugEventHandler.inf
!endif #PcdFspModeSelection

#[-start-201217-IB16560232-add]#
$(PLATFORM_FEATURES_PATH)/PlatformStatusCodeHandler/Pei/PlatformStatusCodeHandlerPei.inf {
    <LibraryClasses>
!if gSiPkgTokenSpaceGuid.PcdSiCatalogDebugEnable == TRUE
      DebugLib|$(PLATFORM_FULL_PACKAGE)/Library/BaseDebugLibAllDebugPort/BaseDebugLibAllDebugPort.inf
!else
      # Use BaseDebugLibNull for save the binary size.
      DebugLib|MdePkg/Library/BaseDebugLibNull/BaseDebugLibNull.inf
!endif
}
#[-end-201217-IB16560232-add]#

#
# USB4 Connection Manager for Pei
#
!if gSiPkgTokenSpaceGuid.PcdAdlLpSupport == TRUE
  $(PLATFORM_FEATURES_PATH)/Usb4Cm/Usb4PlatformPei/Usb4PlatformPei.inf {
      <LibraryClasses>
        IUsb4HrPeiLib|$(PLATFORM_FEATURES_PATH)/Usb4Cm/Library/IUsb4HrPeiLib/IUsb4HrPeiLib.inf
        DUsb4HrPeiLib|$(PLATFORM_FEATURES_PATH)/Usb4Cm/Library/DUsb4HrPeiLib/DUsb4HrPeiLib.inf
  }
!endif

[Components.$(DXE_ARCH)]
  $(PLATFORMSAMPLE_PACKAGE)/Platform/SaveMemoryConfig/Dxe/SaveMemoryConfig.inf
  $(PLATFORMSAMPLE_PACKAGE)/PlatformInitAdvanced/PlatformInitAdvancedDxe/PlatformInitAdvancedDxe.inf {
    <LibraryClasses>
    DxeFirmwareVersionInfoLib|$(PLATFORM_FULL_PACKAGE)/Library/DxeFirmwareVersionInfoLib/DxeFirmwareVersionInfoLib.inf
#[-start-200420-IB17800056-add]#
    DxeSmbiosDataHobLib|IntelSiliconPkg/Library/DxeSmbiosDataHobLib/DxeSmbiosDataHobLib.inf
    BootGuardRevocationLib|$(PLATFORM_FULL_PACKAGE)/Library/BootGuardRevocationLib/BootGuardRevocationLib.inf
    DxeTelemetryAcpiLib|$(PLATFORM_FULL_PACKAGE)/Telemetry/Library/DxeTelemetryAcpiLib/DxeTelemetryAcpiLib.inf
#[-end-200420-IB17800056-add]#
!if gSiPkgTokenSpaceGuid.PcdAdlLpSupport == TRUE
!endif
  }
#[-start-201223-IB19010009-remove]#
##[-start-180420-IB11270199-modify]#
#  $(PLATFORMSAMPLE_PACKAGE)/Features/Amt/SecureEraseDxe/SecureEraseDxe.inf
##[-end-180420-IB11270199-modify]#
#[-end-201223-IB19010009-remove]#

##
## CasterCove Pmic Nvm update
##
  $(PLATFORM_FEATURES_PATH)/Pmic/PmicNvm/CastroCove/CastroCovePmicNvm.inf

!if gBoardModuleTokenSpaceGuid.PcdNvmeEnable == TRUE
  $(PLATFORM_FEATURES_PATH)/Nvme/PowerLossNotifyDxe/PowerLossNotifyDxe.inf
!endif

#[-start-200219-IB17800050-modfiy]#
!if gBoardModuleTokenSpaceGuid.PcdAdvancedFeatureEnable == TRUE
!if gSiPkgTokenSpaceGuid.PcdAcpiEnable == TRUE
  $(PLATFORMSAMPLE_PACKAGE)/PlatformInitAdvanced/PlatformInitAdvancedSmm/PlatformInitAdvancedSmm.inf {
     <BuildOptions>
       *_*_X64_CC_FLAGS      = -DDOCK_ENABLE
  }
!endif
!endif
#  $(PLATFORMSAMPLE_PACKAGE)/Platform/SmmPlatform/Smm/SmmPlatform.inf {
#    <LibraryClasses>
#      ReportStatusCodeLib|MdeModulePkg/Library/SmmReportStatusCodeLib/SmmReportStatusCodeLib.inf
#      ResetSystemLib|$(PLATFORM_SI_PACKAGE)/Pch/Library/BaseResetSystemLib/BaseResetSystemLib.inf
#   }
#[-end-200219-IB17800050-modfiy]#

##
##  Policy
##
  $(PLATFORMSAMPLE_PACKAGE)/PlatformInitAdvanced/PolicyInitAdvancedDxe/PolicyInitAdvancedDxe.inf
  $(PLATFORMSAMPLE_PACKAGE)/Features/Wdt/WdtAppDxe/WdtAppDxe.inf
!if gSiPkgTokenSpaceGuid.PcdAtaEnable == TRUE
  $(PLATFORM_FEATURES_PATH)/Sata/SataController/SataController.inf
!if gBoardModuleTokenSpaceGuid.PcdIntelRaidEnable == TRUE
  $(PLATFORM_FEATURES_PATH)/Sata/IntelUefiRaidDiskInfo/IntelUefiRaidDiskInfo.inf
!endif
!endif

!if gSiPkgTokenSpaceGuid.PcdEmbeddedEnable == TRUE
  $(PLATFORM_FULL_PACKAGE)/Sio/NuvotonNct6776fDxe/NuvotonNct6776f.inf
  $(PLATFORM_FULL_PACKAGE)/Sio/Nct6776fHwMonDxe/Nct6776fHwMon.inf
  $(PLATFORM_FEATURES_PATH)/LifeCycleState/LifeCycleState.inf
!endif


#[-start-210831-IB05660175-modify]#
  $(PLATFORM_FEATURES_PATH)/DashG/DxeDgOpregionInit.inf {
    <Depex>
    gEfiPciIoProtocolGuid
  }
#[-end-210831-IB05660175-modify]#


#[-start-201223-IB19010009-remove]#
#!if gSiPkgTokenSpaceGuid.PcdAmtEnable
#  $(PLATFORMSAMPLE_PACKAGE)/Features/Amt/AmtPetAlertDxe/AmtPetAlertDxe.inf {
#    <LibraryClasses>
#    PlatformBootManagerLib|$(PLATFORM_PACKAGE)/Bds/Library/DxePlatformBootManagerLib/DxePlatformBootManagerLib.inf
#  }
##[-start-180416-IB11270199-add]#
#  $(PLATFORMSAMPLE_PACKAGE)/Features/Amt/AmtSaveMebxConfigDxe/AmtSaveMebxConfigDxe.inf
##[-start-200420-IB17800056-remove]#
#  $(PLATFORMSAMPLE_PACKAGE)/Features/Amt/AmtWrapperDxe/AmtWrapperDxe.inf {
##[-start-201126-IB14630455-add]#
#   <LibraryClasses>
#     UefiBootManagerLib|MdeModulePkg/Library/UefiBootManagerLib/UefiBootManagerLib.inf
##[-end-201126-IB14630455-add]#
#  }
##[-end-200420-IB17800056-remove]#
##[-end-180416-IB11270199-add]#
#  $(PLATFORMSAMPLE_PACKAGE)/Features/Amt/AsfTable/AsfTable.inf
#  $(PLATFORM_FEATURES_PATH)/Amt/AmtMacPassThrough/AmtMacPassThrough.inf
##[-start-201029-IB16740120-remove]#
##!if gPlatformModuleTokenSpaceGuid.PcdOneClickRecoveryEnable == TRUE
##  $(PLATFORM_FEATURES_PATH)/Amt/OneClickRecovery/OneClickRecovery.inf
##!endif
##[-end-201029-IB16740120-remove]#
#!endif
#[-end-201223-IB19010009-remove]#

# [-start-190902-IB16740052-add]
  $(PLATFORMSAMPLE_PACKAGE)/Features/Rst/RstUefiDriverSupport/RstUefiDriverSupport.inf
# [-end-190902-IB16740052-add]

!if gChipsetPkgTokenSpaceGuid.PcdUseCrbEcFlag == TRUE  #&& gBoardModuleTokenSpaceGuid.PcdEcPresent == TRUE
  $(PLATFORMSAMPLE_PACKAGE)/EC/EC.inf
!endif

#
# USB4 Connection Manager
#
!if gSiPkgTokenSpaceGuid.PcdAdlLpSupport == TRUE
  $(PLATFORM_FEATURES_PATH)/Usb4Cm/Usb4CmDxe/Usb4CmDxe.inf {
    <LibraryClasses>
      CmUtilsLib|$(PLATFORM_FEATURES_PATH)/Usb4Cm/Library/CmUtilsLib/CmUtilsLib.inf
      Usb4HrSrvLib|$(PLATFORM_FEATURES_PATH)/Usb4Cm/Library/Usb4HrSrvLib/Usb4HrSrvLib.inf
      Usb4HiCoreLib|$(PLATFORM_FEATURES_PATH)/Usb4Cm/Library/Usb4HiCoreLib/Usb4HiCoreLib.inf
      Usb4DomainLib|$(PLATFORM_FEATURES_PATH)/Usb4Cm/Library/Usb4DomainLib/Usb4DomainLib.inf
      Usb4RtEnumLib|$(PLATFORM_FEATURES_PATH)/Usb4Cm/Library/Usb4RtEnumLib/Usb4RtEnumLib.inf
      Usb4CsLib|$(PLATFORM_FEATURES_PATH)/Usb4Cm/Library/Usb4CsLib/Usb4CsLib.inf
      CmEvtLib|$(PLATFORM_FEATURES_PATH)/Usb4Cm/Library/CmEvtLib/CmEvtLib.inf
      Usb4ProtocolsLib|$(PLATFORM_FEATURES_PATH)/Usb4Cm/Library/Usb4ProtocolsLib/Usb4ProtocolsLib.inf
      Tbt3SupportLib|$(PLATFORM_FEATURES_PATH)/Usb4Cm/Library/Tbt3SupportLib/Tbt3SupportLib.inf
  }
!endif

##
## Hsti
##
  $(PLATFORMSAMPLE_PACKAGE)/Features/Hsti/HstiIhvDxe/HstiIhvDxe.inf {
    <LibraryClasses>
      HstiLib|MdePkg/Library/DxeHstiLib/DxeHstiLib.inf
      Tpm2CommandLib|SecurityPkg/Library/Tpm2CommandLib/Tpm2CommandLib.inf
  }
  $(PLATFORMSAMPLE_PACKAGE)/Features/Hsti/HstiIhvSmm/HstiIhvSmm.inf
  $(PLATFORMSAMPLE_PACKAGE)/Features/Hsti/HstiResultDxe/HstiResultDxe.inf
  $(PLATFORM_FULL_PACKAGE)/Platform/PciPlatform/Dxe/PciPlatform.inf {
  <LibraryClasses>
    DxeOpromPatchLib|$(PLATFORM_FULL_PACKAGE)/Library/DxeOpromPatchLib/DxeOpromPatchLib.inf
}

##
## Crash Log Support
##
  $(PLATFORMSAMPLE_PACKAGE)/Features/CrashLogDxe/CrashLogDxe.inf
  $(PLATFORMSAMPLE_PACKAGE)/Features/Cnv/CnvUefiVariables/CnvUefiVariables.inf

  $(PLATFORMSAMPLE_PACKAGE)/Features/Me/MeExtMeasurement/Dxe/MeExtMeasurement.inf
  $(PLATFORMSAMPLE_PACKAGE)/Features/Me/MeSmbiosUpdateConfigDxe/MeSmbiosUpdateConfigDxe.inf

#
# AcpiTables
#
!if gPlatformModuleTokenSpaceGuid.PcdDTbtEnable == TRUE
  $(PLATFORM_FEATURES_PATH)/Tbt/TbtInit/Dxe/DTbtDxe.inf
!endif

# MultiPdt Support Driver
#
$(PLATFORM_FEATURES_PATH)/MultiPdt/MultiPdtDxe.inf {
  <LibraryClasses>
    PdtUpdateLib|$(PLATFORM_FULL_PACKAGE)/Library/PdtUpdateLib/PdtUpdateLib.inf
}

$(PLATFORM_FEATURES_PATH)/Tme/Dxe/TmeInitDxe.inf

#
# Extended BIOS Region validation driver
#
!if gPlatformModuleTokenSpaceGuid.PcdExtendedBiosRegionSupport == TRUE
  $(PLATFORM_FULL_PACKAGE)/Platform/ValidateExtendedBiosRegion/Dxe/ValidateExtendedBiosRegionDxe.inf
!endif


#################################################################################
##
## Components.X64 Override
##
#################################################################################
##[-start-180504-IB1446102-add]#
!if gPlatformModuleTokenSpaceGuid.PcdVtioEnable == TRUE
$(PLATFORMSAMPLE_PACKAGE)/Vtio/Dxe/VtioDxe.inf
!endif
##[-end-180504-IB1446102-add]#

#[-start-201217-IB16560232-add]#
  $(PLATFORM_FEATURES_PATH)/PlatformStatusCodeHandler/Smm/PlatformStatusCodeHandlerSmm.inf {
    <LibraryClasses>
      # Use BaseDebugLibNull for save the binary size.
      DebugLib|MdePkg/Library/BaseDebugLibNull/BaseDebugLibNull.inf
  }

  $(PLATFORM_FEATURES_PATH)/PlatformStatusCodeHandler/RuntimeDxe/PlatformStatusCodeHandlerRuntimeDxe.inf {
      <LibraryClasses>
        SerialIoUartLib|$(PLATFORM_SI_PACKAGE)/IpBlock/SerialIo/Uart/Library/PeiDxeSmmSerialIoUartLib/DxeRuntimeSerialIoUartLib.inf
  !if gSiPkgTokenSpaceGuid.PcdSiCatalogDebugEnable == TRUE
        DebugLib|$(PLATFORM_FULL_PACKAGE)/Library/BaseDebugLibAllDebugPort/BaseDebugLibAllDebugPort.inf
  !else
        # Use BaseDebugLibNull for save the binary size.
        DebugLib|MdePkg/Library/BaseDebugLibNull/BaseDebugLibNull.inf
  !endif
  }
#[-end-201217-IB16560232-add]#
  !if gSiPkgTokenSpaceGuid.PcdPpamEnable == TRUE
    $(PLATFORMSAMPLE_PACKAGE)/Features/NiftyRock/PpamPlatformSmm/PpamPlatformSmm.inf
  !endif

  $(PLATFORMSAMPLE_PACKAGE)/Features/Dptf/AcpiTables/DptfAcpiTables.inf
  $(PLATFORMSAMPLE_PACKAGE)/Features/Dptf/Dxe/Dptf.inf


  $(PLATFORMSAMPLE_PACKAGE)/Features/UsbTypeC/UsbTypeCDxe/UsbTypeCDxe.inf
#[-start-200911-IB17040161-add]#
#
# Telemetry
#
  $(PLATFORM_FULL_PACKAGE)/Telemetry/PlatformFirmwareVersionDxe/PlatformFirmwareVersionDxe.inf
  $(PLATFORM_FULL_PACKAGE)/Telemetry/SiFirmwareVersionDxe/SiFirmwareVersionDxe.inf
  $(PLATFORM_FULL_PACKAGE)/Telemetry/MotherBoardHealthDxe/MotherBoardHealthDxe.inf
  $(PLATFORM_FULL_PACKAGE)/Telemetry/NvmeHealthDxe/NvmeHealthDxe.inf
  $(PLATFORM_FULL_PACKAGE)/Telemetry/MrcHealthDxe/MrcHealthDataInit.inf {
    <LibraryClasses>
    #  NULL|CryptoPkg/Library/IntrinsicLib/IntrinsicLib.inf
    NULL|InsydeModulePkg/Library/IntrinsicLib/IntrinsicLib.inf
  }
  $(PLATFORM_FULL_PACKAGE)/Telemetry/CsmeHealthDxe/CsmeHealthDxe.inf
#[-end-200911-IB17040161-add]#

#
# VTd Iommu Security
#
  $(PLATFORM_FEATURES_PATH)/VTd/PlatformVTdSampleDxe/PlatformVTdSampleDxe.inf
#[-start-210408-IB17800122-modify]#
!if gChipsetPkgTokenSpaceGuid.PcdRetimerCapsuleUpdateSupported == TRUE
  $(PLATFORM_FEATURES_PATH)/TcssRetimerCapsuleUpdate/RetimerCapsuleSupportDriver/RetimerCapsuleUpdate.inf
!endif
#[-end-210408-IB17800122-modify]#


#[-start-201030-IB16810136-add]#
!if gPlatformModuleTokenSpaceGuid.PcdCapsuleEnable == TRUE
  !include $(PLATFORMSAMPLE_PACKAGE)/Features/CapsuleUpdate/FmpPlatformBios.dsc
#[-start-201230-IB16810143-add]#
  !include $(PLATFORMSAMPLE_PACKAGE)/Features/CapsuleUpdate/FmpPlatformBtGAcm.dsc
#[-end-201230-IB16810143-add]#
#[-start-201112-IB16810138-add]#
  !include $(PLATFORMSAMPLE_PACKAGE)/Features/CapsuleUpdate/FmpPlatformMe.dsc
#[-end-201112-IB16810138-add]#
#[-start-201130-IB16810140-add]#
  !include $(PLATFORMSAMPLE_PACKAGE)/Features/CapsuleUpdate/FmpPlatformuCode.dsc
#[-end-201130-IB16810140-add]#
!endif
#[-end-201030-IB16810136-add]#

[Components.X64]
#[-start-210720-IB16810158-modify]#
#
# Network
#
!if gPlatformModuleTokenSpaceGuid.PcdNetworkEnable == TRUE

  !if (gSiPkgTokenSpaceGuid.PcdAmtEnable == TRUE) AND (gPlatformModuleTokenSpaceGuid.PcdWifiProfileSyncEnable == TRUE)
    $(PLATFORM_FEATURES_PATH)/Amt/WifiProfileSync/WifiProfileSync.inf
  !endif

  !if gChipsetPkgTokenSpaceGuid.PcdUefiWirelessCnvtEnable == TRUE
  # There is a Kernel driver already: WifiConnectionManagerDxe.inf
  #$(PLATFORM_FEATURES_PATH)/WifiConnectionManagerDxe/WifiConnectionManagerDxe.inf
  BluetoothPkg/BluetoothConnectionManagerDxe/BluetoothConnectionManagerDxe.inf
  BluetoothPkg/UsbBtHciDxe/UsbBtHciDxe.inf
  BluetoothPkg/BluetoothBusDxe/BluetoothBusDxe.inf
  BluetoothPkg/BluetoothHidDxe/BluetoothHidDxe.inf
  BluetoothPkg/HidKbDxe/HidKbDxe.inf
  BluetoothPkg/HidMouseDxe/HidMouseDxe.inf
  !endif

!endif # gPlatformModuleTokenSpaceGuid.PcdNetworkEnable
#[-end-210720-IB16810158-modify]#