 //*****************************************************************************
 //
 // Copyright (c) 2012 - 2017, Hefei LCFC Information Technology Co.Ltd.
 // And/or its affiliates. All rights reserved.
 // Hefei LCFC Information Technology Co.Ltd. PROPRIETARY/CONFIDENTIAL.
 // Use is subject to license terms.
 //
 //******************************************************************************
 /*++
Abstract:
  LCFC EC common asl code definition, it's for all Hefei LBG projects.

History:
  Date          Name          Version    Change Notes
  2014.06.25    Steven Wang   V1.00      Initial Release.
  2014.07.22    Steven Wang   V1.01      Add CLID initial code for panel on.
  2014.07.29    Steven Wang   V1.02      Implement Lenovo Paper Looking Firmware feature.
  2014.10.16    Sea Zhou      V1.03      Add LfcNvsArea.asl into Ec.asl for easy porting.
  2014.10.17    Sea Zhou      V1.04      Add Cind.asl into Ec.asl for easy porting, add a LCFCTODO for it.
  2014.10.22    Steven Wang   V1.05      Add feature control switch for different project.
  2015.03.25    Cissie Gu     V1.06      Add Touchpad enable and disable function when press Fn+F6.
                                         Modify some comments according to LBC EC common spec 1.02.
  2015.03.31    Sea Zhou      V1.07      Support Win10.
                                         Add External for build error.
  2015.03.31    Sea Zhou      V1.08      Remove external code in PS2M.
  2015.06.15    Steven Wang   V1.09      Add condition for VPC1 device report,Win8,Win8.1 need BIOS enumerate the
                                         VPC1, other OS must not enumerate it.
  2015.07.04    Steven Wang   V1.10      Add Lcfc Wmi device for communication with App, App can get/set BIOS asl code
                                         and also get notification from BIOS.
  2015.07.06    Steven Wang   V1.11      Add Lenovo Mini Spec v1.35, battery inforamtion WMI interface.
  2015.07.08    Steven Wang   V1.12      Implement Lenovo Transition for Flex & Miix Wmi interface requirement.
  2015.07.28    Steven Wang   V1.13      Modify _PPC to CPPC for SKL platform.
                                         Add Mutex scheme for all EC access.
  2015.07.28    Steven Wang   V1.14      Add first used date information for GSBI method.
  2015.08.27    Steven Wang   V1.15      Add ECAV for EC opregion access safely.
  2015.09.08    Steven Wang   V1.16      Add EC 68&6C interface for command and all EC bank RAM access.
  2015.10.12    Steven Wang   V1.17      Add common CMOS extend RAM access method.
  2015.10.13    Steven Wang   V1.18      Fix _PCL return incorrect type object.
                                         The type of an object is incorrect and does not match the expected type. ACPI will generally abort execution of the AML op-code that causes this error.
  2015.10.23    Steven Wang   V1.19      Add AMD support and remove some unused method.
  2015.12.18    Cissie Gu     V1.20      Exchanged the flags representing to Linux and Windows Vista following the EC spec.
  2016.01.12    Steven Wang   V1.21      Add Gaming Zone feature.
  2016.02.24    Steven Wang   V1.22      Add LFCI common interface for MFG or other team application tool.
  2016.03.01    Steven Wang   V1.23      Implement Lenovo Game Zone Firmware SPEC V1.53
  2016.04.06    Steven Wang   V1.24      Implement Lenovo Game Zone Firmware SPEC V1.54
  2016.04.07    Cissie.Gu     V1.25      Add a function which will turn on/off LCD through Decreasing Brightness key/Increasing Brightness key.
  2016.07.07    Steven Wang   V1.26      When charging with battery capcity no full, average currency is 0, this is not normal status
                                         need modify this value for Remaining Battery Life calculation from ACPI spec
  2016.08.12    Steven Wang   V1.27      Optimize the battery access method, due to 68 6C will conflict with some tool, change to 62 66
                                         command method.
  2017.03.23    Steven Wang   V1.28      Optimized code for AMD platform porting
                                         Correct one critical logical error, mutex must release before method return
  2017.03.23    Steven Wang   V1.29      Add support for Lenovo PM Firmware Spec V1.74,in order to full support ITS function, need
                                         add the Lenovo ITS implementation, todo later
  2019.02.11    Sea Zhou      V1.30      Separate LENOVO_PAPER_LOOKING_FEATURE and LENOVO_YMC_DRIVER_FEATURE features
  2019.07.04    Yuerong Zhang V1.31      Add initial Super Key feature V1.0. Press super key button will invoke lenovo computer housekeeper APP
  2020.04.01    Storm Yin     v1.32      Add KBLC method for Lenovo PM Firmware Spec v1.80.
                                         not add ITHC method which required at v1.78. Reason: ITS3.0 requirement. ITS4.0 no need this.
Module Name:
  EC.asl

Note:
  You must take care about the prompt string "LCFCTODO:" and customize your
  project.

Support Version:
   Lenovo China Minimum BIOS Spec V1.38
   Lenovo PM Firmware Spec V1.74
   Lenovo Game Zone Firmware SPEC V1.86
 --*/

//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//                        F E T U R E S   S W I T C H E S
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//LCFCTODO: you need check your project about the below features enable or disable.
#define LENOVO_PAPER_LOOKING_FEATURE   0  // 0: Disable, 1: Enable
//[-start-210826-KEBIN00043-modify]//
#if defined(C970_SUPPORT) || defined(C770_SUPPORT)
#define LENOVO_YMC_DRIVER_FEATURE      1  // 0: Disable, 1: Enable
#else
#define  LENOVO_YMC_DRIVER_FEATURE      0  // 0: Disable, 1: Enable
#endif
//[-end-210826-KEBIN00043-modify]//
//#define LENOVO_YMC_DRIVER_FEATURE      0  // 0: Disable, 1: Enable
#define LCFC_WMI_MAPPER_DEVICE         0  // 0: Disable, 1: Enable
#define PLATFORM_VENDOR_INTEL          1  // 0: AMD      1: Intel
#define LENOVO_GAMING_ZONE_FEATURE     0  // 0: Disable, 1: Enable
//[-start-210831-Dongxu0013-modify]//
//[-start-210906-YUNLEI0130-modify]//
//[-start-210906-QINGLIN0044-modify]//
//[-start-210928-Ching000005-modify]//
#if defined(C970_SUPPORT) || defined(C770_SUPPORT) || defined(S570_SUPPORT) || defined(S370_SUPPORT) || defined(S77014_SUPPORT) || defined(S77013_SUPPORT) || defined(S77014IAH_SUPPORT)
//[-end-210928-Ching000005-modify]//
//[-end-210906-QINGLIN0044-modify]//
#define LENOVO_TURN_OFF_LCD_VIA_F11    0  // 0: Disable, 1: Enable
#else
#define LENOVO_TURN_OFF_LCD_VIA_F11    1  // 0: Disable, 1: Enable
#endif
//[-end-210906-YUNLEI0130-modify]//
//[-end-210831-Dongxu0013-modify]//
//[-start-210709-BAIN000021-modify]//
//[-start-210721-QINGLIN0001-modify]//
//[-start-210803-QINGLIN0008-modify]//
//[-start-210802-SHAONN0003-modify]//
//[-start-210918-Ching000002-modify]//
//[-start-210924-TAMT000012-modify]//
#if defined(C970_SUPPORT) || defined(S77014_SUPPORT) || defined(C770_SUPPORT) || defined(S570_SUPPORT) || defined(S370_SUPPORT) || defined(S77013_SUPPORT) || defined(S77014IAH_SUPPORT)
//[-end-210924-TAMT000012-modify]//
//[-end-210918-Ching000002-modify]//

#define LENOVO_SUPER_KEY_WMI           1  // 0: Disable, 1: Enable
#else
#define LENOVO_SUPER_KEY_WMI           0  // 0: Disable, 1: Enable
#endif
//[-end-210802-SHAONN0003-modify]//
//[-end-210803-QINGLIN0008-modify]//
//[-end-210721-QINGLIN0001-modify]//
//[-end-210709-BAIN000021-modify]//

//[-start-21907-KEBIN00045-modify]//
//[-start-210908-QINGLIN0049-modify]//
#define LENOVO_SUPER_RESOLUTION_WMI    1  // 0: Disable, 1: Enable
//[-end-210908-QINGLIN0049-modify]//
//[-end-21907-KEBIN00045-modify]//
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//                        M U T E X - D E F I N I T I O N
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// Note: all EC RAM access must add mutex control, avoid OS mutithread access conflict
// Usage:
/*++
  If(LEqual(Acquire(EC0_SCOPE.EC0.LfcM, 0xA000),0x0)){
    Store (1, EC0_SCOPE.EC0.XXXX)
    Release(EC0_SCOPE.EC0.LfcM)
  }
--*/

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//                        C O M M O N   D E F I N I T I O N S
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
#if PLATFORM_VENDOR_INTEL
//Intel
//[-start-210520-KEBIN00003-modify]//
//#define EC0_SCOPE          \_SB.PCI0.LPCB
//#define VGA_SCOPE          \_SB.PCI0.GFX0
//#define EDP_SCOPE          \_SB.PCI0.GFX0.DD1F
#define EC0_SCOPE          \_SB.PC00.LPCB
#define VGA_SCOPE          \_SB.PC00.GFX0
#define EDP_SCOPE          \_SB.PC00.GFX0.DD1F
//[-end-210520-KEBIN00003-modify]//
#else
//AMD
#define EC0_SCOPE          \_SB.PCI0.LPC0
#define VGA_SCOPE          \_SB.PCI0.GP17.VGA
#define EDP_SCOPE         \_SB.PCI0.GP17.VGA.LCD
#define OSTYPE_WINXP       0x00000008
#define OSTYPE_WINXPSP01   0x00000010
#define OSTYPE_WINXPSP02   0x00000011
#define OSTYPE_WINXPSP03   0x00000012
#define OSTYPE_WINNET_SRV  0x00000020
#define OSTYPE_VSTA        0x00000040
#define OSTYPE_VSTA_SP1    0x00000041
#define OSTYPE_WINDOWS7    0x00000050
#define OSTYPE_WINDOWS8    0x00000060
#define OSTYPE_WINDOWS81   0x00000061
#define OSTYPE_WINDOWS10   0x00000070
#define OSTYPE_LINUX       0x00000080
#endif
//
//LCFCTODO: please confirm with EC if ACPI ECram use bank 4.
//
#define EC0_ACPI_ECRAM     4

//[-start-210731-BAIN000026-add]//
//[start-210801-STORM1102-modify]//
//[-start-210802-QINGLIN0006-modify]//
//[-start-210803-QINGLIN0008-modify]//
//#if defined(C970_SUPPORT)
//[-start-210918-Ching000002-modify]//
//[-start-210924-TAMT000012-modify]//
#if defined(C970_SUPPORT) || defined(S77014_SUPPORT) || defined(C770_SUPPORT) || defined(S570_SUPPORT) || defined(S370_SUPPORT) || defined(S77013_SUPPORT) || defined(S77014IAH_SUPPORT)
//[-end-210924-TAMT000012-modify]//
//[-end-210918-Ching000002-modify]//
//[-end-210803-QINGLIN0008-modify]//
//[-end-210802-QINGLIN0006-modify]//
//[end-210801-STORM1102-modify]//
#define GPIO_VER2_LP_GPP_H20                 0x09070014 //FN_LED
#define GPIO_VER2_LP_GPP_H22                 0x09070016 //CAPS_LED
#endif
//[-end-210731-BAIN000026-add]//
//[-start-210802-QINGLIN0006-add]//
#if defined(S570_SUPPORT)
#define GPIO_VER2_LP_GPP_H21                 0x09070015 //NUM_LED
#endif
//[-end-210802-QINGLIN0006-add]//
//[-start-210803-QINGLIN0008-add]//
//[-start-210802-SHAONN0003-modify]//
//[-start-210819-YUNLEI0123-modify]//
#if defined(S370_SUPPORT) || defined(C770_SUPPORT) 
#define GPIO_VER2_LP_GPP_D4                  0x09080004 //NUM_LED
#endif
//[-end-210819-YUNLEI0123-modify]//
//[-end-210802-SHAONN0003-modify]//
//[-end-210803-QINGLIN0008-add]//


//LCFCTODO:If support multiple sensor participants, please add all sensor participants here.
//External(\_SB.PCI0.LPCB.EC0.SEN1, DeviceObj)

//[-start-210706-SHUI0001-add]//
External(\_SB.PC00.LPCB.EC0.SEN1, DeviceObj)
External(\_SB.PC00.LPCB.EC0.SEN2, DeviceObj)
External(\_SB.PC00.LPCB.EC0.SEN3, DeviceObj)
External(\_SB.PC00.LPCB.EC0.SEN4, DeviceObj)
//[start-220113-STORM1124-modify]
#if defined(C770_SUPPORT)
External(\_SB.PC00.LPCB.EC0.SEN5, DeviceObj)
External(\_SB.PC00.LPCB.EC0.SEN6, DeviceObj)
External(\_SB.PC00.LPCB.EC0.SEN7, DeviceObj)
#endif
//[end-220113-STORM1124-modify]
//[-end-210706-SHUI0001-add]//
//[-start-211117-SHAONN0018-add]//
//[-start-211201-JEPLIUT201-modify]// or S570
#if defined(S370_SUPPORT) || defined(S570_SUPPORT) 
//[-end-211201-JEPLIUT201-modify]// 
External(\_SB.PC00.LPCB.EC0.SEN5, DeviceObj)
#endif
//[-end-211117-SHAONN0018-add]//
//[-start-210830-BAIN000032-add]//
//[-start-210830-YUNLEI0127-modify]//
//[-start-210911-QINGLIN0064-modify]//
//[-start-211013-Ching000012-modify]//
//[-start-211111-QINGLIN0113-modify]//
//[-start-211115-Ching000016-modify]//
#if defined(C970_SUPPORT) || defined(C770_SUPPORT) || defined(S370_SUPPORT) || defined(S77014_SUPPORT) || defined(S570_SUPPORT) || defined(S77013_SUPPORT) || defined(S77014IAH_SUPPORT)
//[-end-211115-Ching000016-modify]//
//[-end-211111-QINGLIN0113-modify]//
//[-end-211013-Ching000012-modify]//
//[-end-210911-QINGLIN0064-modify]//
External(\_SB.UBTC,DeviceObj)
External(\_SB.UBTC.ECRD,MethodObj)
//[-start-211019-SHAONN0011-add]//
#if defined(S370_SUPPORT)
External(\_SB.IETM, DeviceObj)
#endif
//[-start-220111-JOYID00011-add]//
External(\_SB.PC00.LPCB.EC0.MLED)
//[-end-220111-JOYID00011-add]//
//[-end-211019-SHAONN0011-add]//
#define  I2C_ERROR                     0x8000
#define  I2C_UNKNOWN_FAIL              0x8007 // I2C_ERROR | 0x07
#define  I2C_ADRS_NOT_ACK              0x8010 // I2C_ERROR | 0x10
#define  I2C_DEV_ERROR                 0x8011 // I2C_ERROR | 0x11
#define  I2C_CMD_DENIED                0x8012 // I2C_ERROR | 0x12
#define  I2C_UNKNOWN_ERR               0x8013 // I2C_ERROR | 0x13
#define  I2C_ACC_DENIED                0x8017 // I2C_ERROR | 0x17
#define  I2C_TIME_OUT                  0x8018 // I2C_ERROR | 0x18
#define  I2C_NOT_SUPPORT               0x8019 // I2C_ERROR | 0x19
#define  I2C_BUSY                      0x801A // I2C_ERROR | 0x1A
#define  I2C_TIMEOUT                   0x8080 // I2C_ERROR | 0x80
#define  I2C_CMD_NOT_COMPLETE          0x8081 // I2C_ERROR | 0x81
#endif
//[-end-210830-YUNLEI0127-modify]//
//[-end-210830-BAIN000032-add]//
//[-start-211012-GEORGE0013-add]//
#if defined(S77014_SUPPORT) || defined(S77014IAH_SUPPORT)
External(\_SB.NPCF, DeviceObj)
#endif
//[-end-211012-GEORGE0013-add]//
//[-start-211210-MANDY0001-add]//
External(\_SB.WM00, DeviceObj)
//[-end-211210-MANDY0001-add]//
//[-start-220321-QINGLIN0166-add]//
#if defined(S370_SUPPORT)
External(UCMS)
#endif
//[-end-220321-QINGLIN0166-add]//
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//                        C O M M O N   E X T E R N A L
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
#if PLATFORM_VENDOR_INTEL
//Intel
#if LENOVO_SUPER_RESOLUTION_WMI
External(VGA_SCOPE, DeviceObj)
Scope(VGA_SCOPE)
{
  OperationRegion(GFXF, PCI_Config, 0x00, 0x4)
  Field(GFXF, AnyAcc, NoLock, Preserve)
  {
    VIDG, 16,
    DIDG, 16,
  }
}
#endif
#else
//AMD
External(TPOS)
#endif
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//                        G L O B A L   V A R I A B L E S
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
#if PLATFORM_VENDOR_INTEL
//Intel
#else
Scope (\)
{
//This is the global token for EC on/off at AMD platform
  Name(ECON,1)
}
#endif
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

Device(EC0)  // Hitachi Embedded Controller
{
  Name(_HID, EISAID("PNP0C09"))
  Name(_UID,1)
  Name(ECAV, Zero)   // Must not access EC space before EC operation region handler is ready
//[-start-211210-MANDY0001-add]//
  Name(WEID, 0)      //define the Q_evnet event ID for WMI Event 
//[-end-211210-MANDY0001-add]//
  Mutex(LfcM,0x0)
//[-start-210830-BAIN000032-add]//
//[-start-210830-YUNLEI0127-modify]//
//[-start-210911-QINGLIN0064-modify]//
//[-start-211013-Ching000012-modify]//
//[-start-211111-QINGLIN0113-modify]//
//[-start-211115-Ching000016-modify]//
#if defined(C970_SUPPORT) || defined(C770_SUPPORT) || defined(S370_SUPPORT) || defined(S77014_SUPPORT) || defined(S570_SUPPORT) || defined(S77013_SUPPORT) || defined(S77014IAH_SUPPORT)
//[-end-211115-Ching000016-modify]//
//[-end-211111-QINGLIN0113-modify]//
//[-end-211013-Ching000012-modify]//
//[-end-210911-QINGLIN0064-modify]//
  Mutex(HGCT,0x0)
#endif
//[-end-210830-YUNLEI0127-modify]//
//[-end-210830-BAIN000032-add]//

  //
  //LCFCTODO: you should choose a GPIO pin for Runtime SCI as HW design.
  //
//[-start-210524-KEBIN00004-modify]// 
//  Name(_GPE,3)  
Name(_GPE,0x6E)
//[-end-210524-KEBIN00004-modify]// 

  Method(_CRS, 0)
  {
    Name(BFFR, ResourceTemplate()
    {
      IO(Decode16,0x62,0x62,0,1)  // DIN/DOUT
      IO(Decode16,0x66,0x66,0,1)  // CMD/STS
    })
    Return(BFFR)
  }

  Method(_STA)
  {
//[-start-210520-KEBIN00003-remove]//  
//    If (LEqual(ECON,1)) {
      Return(0x0F)
//    }
//    Return(0x00)
//[-end-210520-KEBIN00003-remove]//    
  }

  OperationRegion(ERAM, EmbeddedControl, 0, 0xFF)
  Field(ERAM,ByteAcc,Lock,Preserve)
  {
    //EC name space defininition
    VCMD,8,  // 0x00       VPC command
//[-start-210823-KEBIN00041-add]// 
    Offset(0x60),
    SMPR,8,  // 0x60       SMBus protocol
//[-end-210823-KEBIN00041-add]//
  }

//LCFCTODO:Modfiy EC ram address as your project
// Bar-Code are stored at ECRAM bank2,offset 0x2E - 0x4D
//[-start-210524-KEBIN00004-modify]// 
//  OperationRegion(ECB2, SystemMemory, 0xff00d520, 0xFF)
   OperationRegion(ECB2, SystemMemory, 0xFE0B0520, 0xFF)
//[-end-210524-KEBIN00004-modify]//   
  Field(ECB2,AnyAcc,Lock,Preserve)
 {
   BAR1,184,// 0x2E-0x44
   BAR2,80,// 0x45-0x4D
 }
//LCFCTODO:Modfiy EC ram address as your project
//[-start-210524-KEBIN00004-modify]// 
//  OperationRegion(ERAX, SystemMemory, 0xFE00D400, 0xFF)
   OperationRegion(ERAX, SystemMemory, 0xFE0B0400, 0xFF)
//[-start-210524-KEBIN00004-modify]//   
  Field(ERAX,ByteAcc,Lock,Preserve)
 {
   //EC name space defininition

    Offset(0x01),
    VDAT,8,  // 0x01       VPC data
    VSTA,8,  // 0x02       VPC status

    Offset(0x06),
    FANS,8,  // 0x06       Fan Speed, if support two fan, this is fan1 speed
    BUSG,1,  // 0x07:0     Battery usage (1: Battery has been running over 72hrs, EM should pop out a message)
    BLEG,1,  // 0x07:1     Battery legal (1: Battery is legal; 0: Battery is illegal)
    BATF,1,  // 0x07:2     Battery performance  (1: Battery performance is on the normal status.0:indicates the battery?|s performance degradation has occurred. EM should show the warning to End-User.)
    BNSM,1,  // 0x07:3     Notify storage mode (1: Indicates close the storage mode)
    BTST,1,  // 0x07:4     Battery Testing Mode (1:Start Battery Testing Mode: turn off all LED lights except the Power Button LED, Make the step1 fan on temperature increased by 10 degrees in thermal table.)
    BBAD,1,  // 0x07:5     Bad battery (1: indicates the main battery is bad.)
    AUTO,1,  // 0x07:6     Auto mode (1: Start Auto mode; 0: Stop Auto mode)
    FCHG,1,  // 0x07:7     Fast charge mode (1: Fast Charge is running; 0: Fast Charge is stopping.)

    Offset(0x0A),
    EDCC,1,  // 0x0A:0     End the calibration cycle
        ,1,  // 0x0A:1     Reserved
    CDMB,1,  // 0x0A:2     calibration cycle for the main battery
    CCSB,1,  // 0x0A:3     calibration cycle for the secondary battery
    BTSM,1,  // 0x0A:4     Battery storage mode
    BTCM,1,  // 0x0A:5     Battery Cycle mode
        ,1,  // 0x0A:6     Reserved
        ,1,  // 0x0A:7     Reserved
    SGST,1,  // 0x0B:0     Switchable status(1: Discrete,  0: UMA)
    HDMI,1,  // 0x0B:1     HDMI exist (1 = HDMI plug in; 0 = No HDMI)
        ,1,  // 0x0B:2     Reserved
        ,1,  // 0x0B:3     Reserved
        ,1,  // 0x0B:4     Reserved
        ,1,  // 0x0B:5     Reserved
        ,1,  // 0x0B:6     Reserved
        ,1,  // 0x0B:7     Reserved
    ODPO,1,  // 0x0C:0     ODD power off(1:Power off)
    EODD,1,  // 0x0C:1     ODD power on by button(1:Power on by button)
    ODPK,1,  // 0x0C:2     ODD power on by key(1:Power on by key)
    CMEX,1,  // 0x0C:3     1:The camera is exist;0:The camera doesn't exist
    CMON,1,  // 0x0C:4     1:The camera power is on;0: Off
    SODD,1,  // 0x0C:5     Reserved for BIOS: Save ODD state when into S3 for BIOS used
    ODFB,1,  // 0x0C:6     ODD power on(1: ODD power on when system resume from S3/S4 or AC plug in.)
    EODS,1,  // 0x0C:7     ODD power status(1: ODD have Power 0:ODD no power)
    RTMP,8,  // 0x0D       Ram temperature
    MBTS,8,  // 0x0E       Main board thermal sensor/thermistor temperature
    MPTS,8,  // 0x0F       Main power thermal sensor/thermistor temperature
    PINF,3,  // 0x10[2:0]  Platform information is writed by BIOS(000->SV+UMA 001->ULV+UMA 011->SV+NV 010->SV+ATI 101->ULV+NV 100->ULV+ATI)
    SUPR,1,  // 0x10:3     1-Super performance running/0-Super performance stopped.
    GTMP,1,  // 0x10:4     1-indicate the temperature of ATI graphic platform CPU or GPU is too high
             //            0-indicate the temperature of ATI graphic platform CPU or GPU is too low
    QUIT,1,  // 0x10:5     1-start quite mode; 0-stop quite mode.
    LS35,1,  // 0x10:6     1-Enable Lenovo S3.5;0-Disable Lenovo S3.5
        ,1,  // 0x10:7     Reserved
    RMBT,1,  // 0x11:0     1-Disable reading information from primary battery for update battery firmware.
    RSBT,1,  // 0x11:1     1-Disable reading information from second battery for update battery firmware.
        ,2,  // 0x11[3:2]  Reserved
        ,4,  // 0x11[7:4]  Reserved
    FUSL,8,  // 0x12       Firmware update status low byte
    FUSH,8,  // 0x13       Firmware update status high byte
    FWBT,64, // 0x14 - 0x1B, Battery marker ID low/high byte, Hardware ID low/high byte, Firmware version low/high byte, Data Version low/high byte

    Offset(0x1D),
    SPMO,8,  // 0x1D       System Performance Mode-00:Intelligent Mode/Standard Mode,01:Extreme Performance Mode,02:Battery Saving Mode.
    TMCB,8,  // 0x1E       Thermal mode changed by: 0 "Fn+Q", 1 APP
    LSKV,8,  // 0x1F        Lenovo Super Key Value-00:no key ,01:super key ,02:voip answer ,03:voip end ,04:fnlk ,05:snap tool,06:service key,
             //                                   07:super key make,08:voice key,09:voice key make,10:voice key break;11:camera button on;
             //                                   12:camera button off;13:voip answer key(Utility solution);14:voip end key(Utility solution);
             //            15:Intenal panel Screen refresh rate;16:Launch vantage for c970;17:Switch Dolby Mode for c970;
             //            18:Switch night screen Mode for c970;19:RGB KBD Fn+L Gaming LOGO LED ON;20:RGB KBD Fn+L Gaming LOGO LED OFF;
             //            21:RGB KBD Fn+U Power plugin LED ON;22:RGB KBD Fn+U Power plugin LED OFF;23:RGB KBD Fn+??��1/?y Brightness off ;
             //            24:RGB KBD Fn+??��1/?y Brightness -;25:RGB KBD Fn+??��1/?y Brightness +;26:RGB KBD Lighting effects profile1,wave;
             //            27:RGB KBD Lighting effects profile 2,Breath;28:RGB KBD Lighting effects profile 3,Smooth;
             //            29:RGB KBD Lighting effects profile 4,always;30:RGB KBD Lighting effects profile 5,off;
             //            31:RGB KBD Lighting effects profile 1 screw the rainbow;32:RGB KBD Lighting effects profile 2 cool blue;
             //            33:RGB KBD Lighting effects profile 3 ripple;34:RGB KBD Lighting effects profile 4 raining;
             //            35:RGB KBD Lighting effects profile 5 audio wave middle to side;36:RGB KBD Lighting effects profile 6 audio wave left to right.
             //            37:RGB KBD Fn+" " Brightness + for x60,X70
             //                                   15:Intenal panel Screen refresh rate.

    FCMO,8,  // 0x20       Function and Mode-00:MMC-Intelligent Mode, 01:MMC-Extreme Performance Mode ;02:MMC-Battery Saving Mode;03:Auto Performance Mode;04:Auto Quiet Mode;05:Skin Temperature Protect
             //                              06:Intelligent Extreme Performance Mode, 07:Intelligent Battery Saving Mode
    BTFW,8,  // 0x21       Battery FW udpate flag
//[-start-210929-TAMT000018-add]// 
#if defined(S77013_SUPPORT) || defined(S77014_SUPPORT) || defined(S77014IAH_SUPPORT)
    Offset(0x23),
    KBRS,1,   //0x45  BIT3  notify EC OS Reboot to reset KeyBoard Backlight
#endif
//[-end-210929-TAMT000018-add]// 
//[-start-211122-JEPLIUT198-add]//  
#if defined(S570_SUPPORT)
    Offset(0x23),   //0x23 BIT0==1 :45W and below + AC exist
    ADP2,1,
#endif
//[-start-211122-JEPLIUT198-add]//
    Offset(0x24),
    BACT,16, // 0x24       Battery timer count - hours
    KBGS,32, // 0x26-0x29: keyboard light control get status, details in method KBLC

    FEPL, 8,  // 0x2A, flash EC password
    FEPH, 8,  // 0x2B, flash EC password
    KBSS,32,  // 0x2C-0x2F: keyboard light control set status, details in method KBLC

// 0x30 - 0x3F For Gaming Zone Feature
    Offset(0x31),
    GIRT, 8,   // 0x31      GPU C cover temperature
    PIRT, 8,   // 0x32      Left Palm rest  C cover
    KIRT, 8,   // 0x33      Game key C cover
    IRTI, 8,   // 0x34      Temperature Table ID 0:IR table 1 1:IR table 2

    Offset(0x36),
    DGPU, 8,   // 0x36      0: Set dGPU flag 0xd1~0xd5; BIT1 0xd1, BIT2 0xD2;
    GUST, 8,   // 0x37      0: dGPU Status callback function return value , BIT1~BIT5 mean set 0xd1~x0d5 sucess.
    GDST, 8,   // 0x38
               //           BIT1: Windows Key Lock Status (0: Disable, 1: Enable)
               //           BIT2: Touch Pad Status (0: Disable, 1: Enable)
    FCST, 8,   //0x39       BIT0: Fan Cooling Status (0: Disable, 1: Enable)
//[-start-220802-SUSIE0001-Modify]//
//[-start-220509-QINGLIN0169-add]//
#ifdef LCFC_SUPPORT
//[-end-220802-SUSIE0001-Modify]//
    Offset(0x40),
    HT0L,8,  //0x40: LSB of Trip Point Capacity for Battery
    HT0H,8,  //0x41: MSB of Trip Point Capacity for Battery
#endif
//[-end-220509-QINGLIN0169-add]//
    Offset(0x43),
    ECTP,8,  // for TP driver status
//[-start-211019-SHAONN0011-add]//
#if defined(S370_SUPPORT) 
    Offset(0x44),
    ADWT,8,   //0x44        Get Ac adapter watt
#endif
//[-end-211019-SHAONN0011-add]//
    Offset(0x45),
    KBGC,32, // 0x45-0x48: keyboard light control capability, details in method KBLC

    Offset(0x4A),
    //EC ship mode control
    ESMC,1,  // 0x4A 0: Disable OKO control ship mode
             //      1: Enable OKO control ship mode

    Offset(0x4B),
    //Pad mode for Flex&Miix&Yoga
    EMOD,8,  // 0x4B 1: NB mode
             //      5: Miix Pad Mode 1
             //      6: Miix Pad Mode 2

    Offset(0x4C),
    //Battery first use date
    BFUD,16, // 0x4C bit0~bit4  days
             //      bit5~bit8  month
             //      bit9~bit15 years
//[-start-210924-GEORGE0008-add]//
#if defined(S77014_SUPPORT) || defined(S77014IAH_SUPPORT)
    Offset(0x53),
    GPUT,8,  // 0x53       GPU DTS temperature 
#endif
//[-end-210924-GEORGE0008-add]//
//[-start-211228-GEORGE0038-modify]//
#if !defined(S77014_SUPPORT) && !defined(S77014IAH_SUPPORT)
    Offset(0x54),
        ,3,  // 0x54[2:0]   (0x2A [2:0])      Reserved
    PDMD,1,  // 0x54[3]     (0x2A [3])        1: PI  0:DLS
        ,4,  // 0x54[7:4]   (0x2A [7:4])      Reserved
        ,1,  // 0x55[0]     (0x2A [8])        Reserved
    TPMD,1,  // 0x55[1]     (0x2A [9])        1:Temporary Mode , 0:Permanent Mode 
        ,6,  // 0x55[7:2]   (0x2A [15:10])    Reserved
        ,8,  // 0x56[7:0]   (0x3E [7:0])      eserved
        ,5,  // 0x57[4:0]   (0x3E [12:8])     Reserved
    BTSB,2,  // 0x57[6:5]   (0x3E [14:13])    0x3: Sunport smart battry2.0 Mode
        ,1,  // 0x57[7]     (0x3E [15])       Reserved

    BTHT,4,  // 0x58[3:0]   (0x6A [3:0])      Reserved 

    BTTP,4,  // 0x58[7:4]   (0x6A [7:4])      Battery health tips 
    BTLF,4,  // 0x59[3:0]   (0x6A [11:8])     Battery health lifespan value/Battery health level
        ,4,  // 0x59[7:4]   (0x6A [15:12])    Reserved
#endif
//[-end-211228-GEORGE0038-modify]//
    Offset(0x5D),
    //External name space definition
    EXSI,8,  // 0x5D       EXT_NAMESPACE_INDEX
    EXSB,8,  // 0x5E       EXT_NAMESPACE_BANK
    EXND,8,  // 0x5F       EXT_NAMESPACE_DATA
    //SMBus EC interface ACPI RAM definition
//[-start-210823-KEBIN00041-modify]//
//    SMPR,8,  // 0x60       SMBus protocol
    HMPR,8,  // 0x60       SMBus protocol
//[-end-210823-KEBIN00041-modify]//
    SMST,8,  // 0x61       bit:0~4 SMBus Status;5 Reserved;6 ALARM;7 DONE
    SMAD,8,  // 0x62       SMBus Address
    SMCM,8,  // 0x63       SMBus Command
    //0x64~0x83 SMBus Data
    SMDA,256,// 0x64       SMBus Data
    BCNT,8,  // 0x84       SMBus BCNT
    SMAA,8,  // 0x85       SMBus alarm address
    SAD0,8,  // 0x86       SMBus alarm data 0
    SAD1,8,  // 0x87       SMBus alarm data 1

    Offset(0x88),
    ,1,
    ,1,
    ,1,
    ,1,
    FBDC,1, // 0x88:04,     FULLY_DISCHARGED:  Set: Remaining Capacity() = 0%, Reset :Remaining Capacity() > 20%
    FBFG,1, // 0x88:05,     Battery Full Charge Flag

    //0x8A KB LED and Always On USB control
    Offset(0x8A),
    KBLO,1,  // 0x8A:00        Set KB LED ON
             //   0: turn off, 1:turn on keyboard led
    UCHE,1,  // 0x8A:01        Set Always On USB Enable
             //   0: Disable, 1:Enable usb charge in S3/S4/S5 feature
    KLCH,1,  // 0x8A:02        KB LED Check
        ,1,  // 0x8A:03        RSV
    KLFS,1,  // 0x8A:04        Read KB LED Function Support
             //   0: not support, 1:support keyboard led feature
    KLOR,1,  // 0x8A:05        Read KB LED On
             //   0: OFF, 1:keyboard led turn on.
    CIBM,1,  // 0x8A:06        Set Charge in Battery Mode S3/S4/S5
             //   0: OFF, 1:Charge in Battery Mode S3/S4/S5.
    UCER,1,  // 0x8A:07        Read Always On USB Enable
             //   0: not support, 1: Support Always On USB S3/S4/S5 feature.

    //0x8B Touch Pad Vendor Indication
    Offset(0x8B),
    TPDV, 3 , // 0x8B[2:0]  (01: Elan TP, 02: Synmatic TP, 03: ALPS TP)
        , 5 , // Reserved
    //0x8C Battery Quick Charge Control
    QCHO, 1 , // 0x8C:00 Set Quick Charge
              //   0: turn off, 1: turn on quick charge
    BKLT, 1 , // 0x8C:01 Indicate the backlight should be lit up or turn down
              //   0: lit up , 1:  turn down
    BSFU, 1 , // 0x8C:02
        , 1 , // 0x8C:03
        , 1 , // 0x8C:04
    OKBS, 1 , // 0x8C:05   onekey battery enable, 0: not support, 1: support
        , 1 , // 0x8C:06
    QCBX, 1,  // 0x8C:07, battery non-support quick charge, 1: battery support quick charge
    FLBT, 1 , //0x8D:00
              //   0: disable flip to boot, 1: enable flip to boot
 //[-start-210707-KEBIN00031-modify]//  
    MLED, 1 , // 0x8D:01  00 Indicate MS breath LED off   01 Indicate MS breath LED on
         ,1,   //0x8D:02 reserved
 //[-end-210707-KEBIN00031-modify]//        
    LESR, 1 , //0x8D:03
              //   0:  Lenovo super resolution stop flag , 1: Lenovo super resolution start flag
//[-start-211021-KEBIN00063-modify]//   
    MSRF, 1 , //MS Resume Flag  EC will set the flag when MS resume ,BIOS will clear the flag
//[-start-211124-kebin000069-modify]//
//[-start-220113-YUNLEI0158-modify]//
//[-start-220117-SHAONN0027-modify]//
#if defined(C970_SUPPORT) || defined(C770_SUPPORT) || defined(S370_SUPPORT)
    UQMF, 1 , // Ultra Quiet Mode enable flag 1: Enable 0:Disable
        , 2 , //0x8D:[07:05] reserved
#else
        , 3 , //0x8D:[07:05] reserved
#endif
//[-end-220117-SHAONN0027-modify]//
//[-end-220113-YUNLEI0158-modify]//
//[-start-211124-kebin000069-modify]//
//[-end-211021-KEBIN00063-modify]//

    YGAM,8,  // 0x8E  Yoga Mode-0:unknow mode,1:Laptop mode,2:Tablet mode,3:Stand mode,4:Tent mode
    //Battery information reading
    Offset(0x8F),
    //0x8F~0x97 Battery Manufacture Name, don't occupy
    BMN0,72, // 0x8F~0x97  Battery Manufacture Name
    //0x98~0x9F Battery Device Name
    BDN0,64, // 0x98~0x9F       Battery Device Name

    //EC interface OEM common RAM definition
    Offset(0xA0),
    IBTL,1,  // 0xA0:0     ISCT battery temperature limitation
    IBCL,1,  // 0xA0:1     ISCT battery capacity limitation
    ISS0,1,  // 0xA0:2     ISCT S0_ISCT status
    IRTC,1,  // 0xA0:3     ISCT RTC wake status
    ISUP,1,  // 0xA0:4     ISCT support enable
    ISC2,1,  // 0xA0:5     ISCT EC wake up for ISCT2.0
    IWAK,1,  // 0xA0:6     ISCT Wake Up Reason
        ,1,  // 0xA0:7     Reserved
    //Key And Button Function
    FPFC,1,  // 0xA1:0     Smart Key - Fool Proof Fn Ctrl(0: disable  1: enable)
    VOUT,1,  // 0xA1:1     Video Output Key(1: Video output key pressed);
    TPAD,1,  // 0xA1:2     Touch pad button (1:enable)
    HKDB,1,  // 0xA1:3     Hotkey function disable(0:Enable, 1:Disable)
//[-start-210731-BAIN000026-modify]//
#ifdef LCFC_SUPPORT
    NUML,1,  // 0xA1:4     Hotkey function disable(0:Enable, 1:Disable)
    CAPL,1,  // 0xA1:5     Hotkey function disable(0:Enable, 1:Disable)
#else
        ,1,  // 0xA1:4     Reserved
        ,1,  // 0xA1:5     Reserved
#endif
//[-end-210731-BAIN000026-modify]//
    CCPW,1,  // 0xA1:6     For command use to cut power bit. (For APP Used)
    EECP,1,  // 0xA1:7     1: Enable EC power
        ,8,  // 0xA2       Reserved
    //SYS_STATUS: System indicator
    OSTY,3,  // 0xA3[2:0]  OS type(000:Windows XP,001:Windows Vista,010:Linux,011:Windows 7, 100: Windows 8, 101: WinBlue)
        ,1,  // 0xA3:3     Reserved
    ADPI,2,  // 0xA3[5:4]  AC Status (00: Normal Adaptor, 01: illegal adapter, 10 Adaptor power is not suitable)
        ,1,  // 0xA3:6     Reserved
    ADPT,1,  // 0xA3:7     AC adaptor(0: Offline,1:Online)
    //WAKEUP_ENABLE: Enable wake up function
    PMEW,1,  // 0xA4:0     PMEWAKE(PME Wk Enable:0=Disable,1=Enable)
    MODW,1,  // 0xA4:1     MDMWAKE(Modem Wk Enable:0=Disable,1=Enable)
    LANW,1,  // 0xA4:2     LANWAKE(LAN wakeup enable:0=Disable,1=Enable)
    RTCW,1,  // 0xA4:3     RTCWAKE(RTC wakeup Enable:0=DIsable,1=Enable)
    WLAW,1,  // 0xA4:4     WLANWAKE(Wireless LAN wakeup Enable:0=Disable,1=Enable)
    USBW,1,  // 0xA4:5     USBWAKE(USB wakeup enable: 0=Disable,1=Enable)
    KEYW,1,  // 0xA4:6     KEYWAKE(Keyboard wakeup enable:0=Disable,1=Enable)
    TPWK,1,  // 0xA4:7     TPWAKE(Touchpad wakeup enable:0=Disable,1=Enable)
    //ACOFF_SOURCE: Enable AC_OFF signal
    CHCR,1,  // 0xA5:0     CHARGECURRENT(1:AC_OFF for when stop charge but have charge current)
    ADPP,1,  // 0xA5:1     ADPOVP(1: AC_OFF for adaptor improper)
    LERN,1,  // 0xA5:2     BATTLEARN(1:AC_OFF for battery at learning mode)
    ACMD,1,  // 0xA5:3     CMD(1:AC_OFF for Command)
    BOVP,1,  // 0xA5:4     BATTOVP(1:AC_OFF for battery OVP)
    LEAK,1,  // 0xA5:5     LEAKCURRENT(1: AC_OFF for Leakage current)
    AIRP,1,  // 0xA5:6     AIRADAPTOR(1:AC_OFF for air-adaptor)
    ACOF,1,  // 0xA5:7     ACOFF(Control AC_OFF active,0:enable,1:disable)
    //ENTER_RESUME_STAT
        ,8,  // 0xA6       Reserved
        ,8,  // 0xA7       Reserved
        ,8,  // 0xA8       Reserved
    THRT,8,  // 0xA9       Throttling Temp put a temp in for system throttling
        ,8,  // 0xAA       Reserved
        ,8,  // 0xAB       Reserved
        ,8,  // 0xAC       Reserved
        ,8,  // 0xAD       Reserved
     //THROTTLING_INDEX
    TLVL,4,  // 0xAE[3:0]  Throttling level Number of P-State level
        ,2,  // 0xAE[5:4]  Reserved
    THSW,1,  // 0xAE:6     THRM_SW(1:SW Throttling active)
    TPIN,1,  // 0xAE:7     THRM_PIN(Throttling status:1=HW Throttling Pin active)
    //THROTTLING_SOURCE
        ,8,  // 0xAF       Reserved
    CPUT,8,  // 0xB0       CPU DTS temperature
    CPUS,8,  // 0xB1       CPU Sensor temperature
//[start-210903-STORM1111-modify]
//    PCHS,8,  // 0xB2       PCH thermal sensor/thermistor temperature
    PCTS,8,  // 0xB2       PCH thermal sensor/thermistor temperature
//[end-210903-STORM1111-modify]
    GPUS,8,  // 0xB3       GPU Sensor temperature
//[-start-210924-GEORGE0008-modify]//
#if defined(S77014_SUPPORT) || defined(S77014IAH_SUPPORT)
        ,8,  // 0xB4       Reserved
#else
    GPUT,8,  // 0xB4       GPU DTS temperature
#endif
//[-end-210924-GEORGE0008-modify]//
    SSDS,8,  // 0xB5       SSD thermal sensor/thermistor temperature
    PCHT,8,  // 0xB6       PCH DTS temperature
    CHIS,8,  // 0xB7       Charger IC thermal sensor/thermistor temperature
    //SWI Event indicators
        ,1,  // 0xB8:0     Reserved
    LSTE,1,  // 0xB8:1     Lid open event(0=off,1=on)
    PMEE,1,  // 0xB8:2     PME event(0=off,1=on)
    PWBE,1,  // 0xB8:3     Power button event(0=off,1=on)
    RNGE,1,  // 0xB8:4     Ring in event(0=off,1=on)
    BTWE,1,  // 0xB8:5     Blue tooth wake up event(0=off,1=on)
        ,2,  // 0xB8[7:6]  Reserved
    LCBV,8,  // 0xB9       LCD brightness value (0x00-0x07)
//[-start-211123-GEORGE0024-modify]//
//[-start-211126-JEPLIUT122-modify]// or S570
#if defined(S77014_SUPPORT) || defined(S570_SUPPORT) || defined(S77014IAH_SUPPORT)
//[-end-211126-JEPLIUT122-add]/
    DGDF,8,  // 0xBA       DGPU Downgrade frequence  0 = D1, 1 = D2, 2 = D3, 3 = D4, 4 = D5 
#else
        ,8,  // 0xBA       Reserved
#endif

//[-end-211123-GEORGE0024-modify]//

    //Device module status (TBD)
    WLAN,1,  // 0xBB:0     1:Wirless LAN active,0:Wirless LAN no active
    BLUE,1,  // 0xBB:1     1:BlueTooth active,0:BlueTooth no active
    WEXT,1,  // 0xBB:2     1:Wirless LAN exist,0:Wirless LAN no exist
    BEXT,1,  // 0xBB:3     1:BlueTooth exist,0:BlueTooth no exist
    KILL,1,  // 0xBB:4     1:Kill switch on,0:Kill switch off
    WLOK,1,  // 0xBB:5     1:WireLess LAN initial OK
    EN3G,1,  // 0xBB:6     1:3G active,0:3G no active
    EX3G,1,  // 0xBB:7     1:3G exist,0:3G no exist
        ,8,  // 0xBC       Reserved
    CTYP,3,  // 0xBD[2:0]  CPU_TYPE: CPU type(00:Tj85,01:Tj90,02:Tj100,03:Tj105,04-07:Reverve)
    CORE,3,  // 0xBD[5:3]  CPU Core Number(00:One Core,01:Dual Core,02:Quad Core,03-07:Reverve)
    GATY,2,  // 0xBD[7:6]  VGA Type(00:UMA,01:NV VGA,02:AMD VGA,03:Reserved)
    //Word registers to Emulate smart selector RAM definition
    //SEL_STATE0
    BA1P,1,  // 0xBE:0     PRESENT_A(Set if 1st battery present)
    BA2P,1,  // 0xBE:1     PRESENT_B(Set if 2nd battery present)
        ,2,  // 0xBE[3:2]  Reserved
    B1CH,1,  // 0xBE:4     CHARGE_A(Set if 1st battery be charging)
    B2CH,1,  // 0xBE:5     CHARGE_B(Set if 2nd battery be charging)
        ,2,  // 0xBE[7:6]  Reserved
    //SEL_STATE1
    PBY1,1,  // 0xBF:0     PWR_BY_A(Set if system power up by 1st)
    PBY2,1,  // 0xBF:1     PWR_BY_B(Set if system power up by 2nd)
        ,2,  // 0xBF[3:2]  Reserved
    SMB1,1,  // 0xBF:4     SMB_A(Set if 1st battery on SMBus)
    SMB2,1,  // 0xBF:5     SMB_B(Set if 2nd battery on SMBus)
        ,2,  // 0xBF[7:6]  Reserved
    //Control method for 1st battery pack RAM definition
    B1TY,1,  // 0xC0:0     Battery type(0:Li-Polymer,1:Li-ion)
    B1MD,1,  // 0xC0:1     Battery mode(For AP read only)
    B1LW,1,  // 0xC0:2     Low battery
        ,1,  // 0xC0:3     Reserved
    B1MF,3,  // 0xC0[6:4]  Battery Manufacture(001:SANYO,010:Sony,100:Panasonic,101:Samsung,011:LG,110:Celxpert,111:Simplo)
        ,1,  // 0xC0:7     Reserved
    B1ST,8,  // 0xC1       Battery Status(Bit0:Discharging, Bit1:Charging, Bit2:Discharging and Now is critical low)
             // 0xC1:0     Battery Status(Bit0:Discharging)
             // 0xC1:1     Battery Status(Bit1:Charging)
             // 0xC1:2     Battery Status(Bit2:Discharging and Now is critical low)
             // 0xC1[7:3]  Reserved
    B1RC,16, // 0xC2       1st battery Remaining Capacity
    B1SN,16, // 0xC4       1st battery Serial Number
    B1FV,16, // 0xC6       1st battery Present Voltage
    B1DV,16, // 0xC8       1st battery Design Voltage
    B1DC,16, // 0xCA       1st battery Design Capacity
    B1FC,16, // 0xCC       1st battery Full charge capacity
    B1GS,8,  // 0xCE       1st battery Gas gauge
        ,8,  // 0xCF       Reserved
    B1CR,16, // 0xD0       1st Battery current
    B1AC,16, // 0xD2       1st Battery average current
    B1PC,8,  // 0xD4       1st Battery power consumption
    B1VL,8,  // 0xD5       1st Battery Volt
    B1TM,8,  // 0xD6       1st Battery Temp
    B1AT,8,  // 0xD7       1st Battery Average Temp
    B1CC,16, // 0xD8       1st Battery charge current
    B1TC,8,  // 0xDA       1st Battery current Temp sample counter
    B1CI,8,  // 0xDB       1st Battery Command index for read battery through SMBus
    B1CU,8,  // 0xDC       1st battery Count up to communicate battery
    B1CA,8,  // 0xDD       1st battery Count up if battery is arrived over Temp
    B1SM,16, // 0xDE       1st Battery SMBus status
    B1VC,8,  // 0xE0       1st Battery over voltage counter
    B1FA,8,  // 0xE1       1st Battery Communication fail counter
    B1VA,8,  // 0xE2       1st Battery voltage of ADC
    B1C1,16, // 0xE3       1st Battery Cell1 voltage
    B1C2,16, // 0xE5       1st Battery Cell2 voltage
    B1C3,16, // 0xE7       1st Battery Cell3 voltage
    B1C4,16, // 0xE9       1st Battery Cell4 voltage

    Offset(0xEC),
    MAXE,16, // 0xEC       1st Battery Max Error
    B1CT,16, // 0xEE       1st Battery cycle counter
    B1EX,1,  // 0xF0:0     1st Battery exist
    B1FL,1,  // 0xF0:1     1st Battery Full
    B1EP,1,  // 0xF0:2     1st Battery Empty
    B1FI,1,  // 0xF0:3     1st Battery first in
        ,2,  // 0xF0:[4:5] Reserved
    B1RE,1,  // 0xF0:6     1st Battery Read Complete
        ,1,  // 0xF0:7     Reserved
    B1LL,1,  // 0xF1:0     1st low low Battery
    B1CE,1,  // 0xF1:1     1st Battery Check EDV1
    B1SE,1,  // 0xF1:2     1st Battery Set EDV1
    B1S5,1,  // 0xF1:3     1st Battery shutdown 5%
    B1SR,1,  // 0xF1:4     Start to read 1st Battery
    B1SC,1,  // 0xF1:5     1st Battery start to count communication counter
        ,2,  // 0xF1:[6:7] Reserved
    B1TO,1,  // 0xF2:0     1st Battery fast charging timeout
    B1BC,1,  // 0xF2:1     1st Battery bad Cell
    B1CF,1,  // 0xF2:2     1st Battery communicate fail
    B1CS,1,  // 0xF2:3     Use for detect 1st battery charging suspend
    B1SG,1,  // 0xF2:4     1st Battery command stop charge
    B1SU,1,  // 0xF2:5     1st Battery stop charge for Customer use
    B1OV,1,  // 0xF2:6     1st Battery over voltage
    B1OT,1,  // 0xF2:7     1st Battery over temperature
    B1TT,1,  // 0xF3:0     1st Battery trickle charging timeout
    B1SA,1,  // 0xF3:1     1st Battery Stop charge of Adapter throttling
    B1SS,1,  // 0xF3:2     1st Battery Stop charge of Sony battery
        ,1,  // 0xF3:3     Reserved
    B1SF,1,  // 0xF3:4     1st Battery Stop charge of flash
    B1WN,1,  // 0xF3:5     1st Battery Wrong Battery name
        ,2,  // 0xF3:[6:7] Reserved
    B1DA,16, // 0xF4       1st Batter Manufacture Date
        ,16, // 0xF6       Reserved
    B1CN,8,  // 0xF8       1st Battery Cell number
    ITMD,1,  // 0xF9:0
        ,7,  // 0xF9:[1:7] Reserved
        ,8,  // 0xFA       Reserved

// POST_HOTKEY 0xFC       Post Hotkey status
// 0xFC:0     F2 status (0= unpressed,1=pressed)
// 0xFC:1     F12 status (0= unpressed,1=pressed)

// For Gaming Zone Feature
    Offset(0xFE),
    FA2S,8   // 0xFE       FAN2 Speed
  }

  OperationRegion (ECMS, SystemIO, 0x72, 0x02)  //EXT. CMOS
  Field (ECMS, ByteAcc, Lock, Preserve)
  {
    INDX, 8,
    DATA, 8
  }

  Method(RECM, 1, Serialized)
  {
    Store (Arg0, INDX)
    Return (DATA)
  }

  Method(WECM, 2, Serialized)
  {
    Store (Arg0, INDX)
    Store (Arg1, DATA)
  }

  OperationRegion(CMDE, SystemIO, 0x62, 11)
  Field(CMDE, ByteAcc, Lock, Preserve)
  {
    EC62, 8,  // 0x62
    , 8,      // 0x63
    , 8,      // 0x64
    , 8,      // 0x65
    EC66, 8,  // 0x66
    , 8,      // 0x67
    EC68, 8,  // 0x68
    , 8,      // 0x69
    , 8,      // 0x6A
    , 8,      // 0x6B
    EC6C, 8   // 0x6C
  }

  #define ACPI_SUCCESS                      0x00
  #define DEVICE_ERROR                      0x01
  #define PARA_ERROR                        0x02
  #define KEY_OBF                           0x01
  #define KEY_IBF                           0x02
  #define ECRAM_READ                        0x7E
  #define EC6266CMD                         0x01
  #define EC686CCMD                         0x02

  Method(WIBE, 1, Serialized)
  {
    Store(0x10000, Local0)
    While(Local0) {
      If (LEqual(Arg0, EC6266CMD)) {
        Store(EC66, Local1)
      } ElseIf (LEqual(Arg0, EC686CCMD)){
        Store(EC6C, Local1)
      } Else {
        Return(PARA_ERROR)
      }
      If (LEqual(And(Local1, KEY_IBF),0)) {
        Return(ACPI_SUCCESS)
      }
      Stall(10)
      Decrement(Local0)
    }
    Return(DEVICE_ERROR)
  }

  Method(WOBF, 1, Serialized)
  {
    Store(0x10000, Local0)
    While(Local0) {
      If (LEqual(Arg0, EC6266CMD)) {
        Store(EC66, Local1)
      } ElseIf (LEqual(Arg0, EC686CCMD)){
        Store(EC6C, Local1)
      } Else {
        Return(PARA_ERROR)
      }
      If (LEqual(And(Local1, KEY_OBF),1)) {
        Return(ACPI_SUCCESS)
      }
      Stall(10)
      Decrement(Local0)
    }
    Return(DEVICE_ERROR)
  }

  Method(WOBE, 1, Serialized)
  {
    Store(0x10000, Local0)
    While(Local0) {
      If (LEqual(Arg0, EC6266CMD)) {
        Store(EC66, Local1)
      } ElseIf (LEqual(Arg0, EC686CCMD)){
        Store(EC6C, Local1)
      } Else {
        Return(PARA_ERROR)
      }
      If (LEqual(And(Local1, KEY_OBF),1)){
        If (LEqual(Arg0, EC6266CMD)) {
          Store(EC62, Local2)
        } ElseIf (LEqual(Arg0, EC686CCMD)){
          Store(EC68, Local2)
        } Else {
          Return(PARA_ERROR)
        }
      }
      Else
      {
        Return(ACPI_SUCCESS)
      }
      Stall(10)
      Decrement(Local0)
    }
    Return(DEVICE_ERROR)
  }
  
  // MBV 686C interface CMD + DATA1 + DATA2, EC return one Data
  // Arg0: Command
  // Arg1: Data1
  // Arg2: Data2
  // Return: EC return data
  
  Method(MBEI, 3, Serialized)
  {
    If (LNotEqual(WIBE(EC686CCMD), ACPI_SUCCESS)) {
      Return(DEVICE_ERROR)
    }
    If (LNotEqual(WOBE(EC686CCMD), ACPI_SUCCESS)) {
      Return(DEVICE_ERROR)
    }
    Store(Arg0, EC6C) //CMD
    If (LNotEqual(WIBE(EC686CCMD), ACPI_SUCCESS)) {
      Return(DEVICE_ERROR)
    }
	
    Store(Arg1, EC68) //DATA1
    If (LNotEqual(WIBE(EC686CCMD), ACPI_SUCCESS)) {
      Return(DEVICE_ERROR)
    }

    Store(Arg2, EC68) //DATA2
    If (LNotEqual(WIBE(EC686CCMD), ACPI_SUCCESS)) {
      Return(DEVICE_ERROR)
    }
	
	// Get one byte EC return data
    If (LNotEqual(WOBF(EC686CCMD), ACPI_SUCCESS)) {
      Return(DEVICE_ERROR)
    }
    Store(EC68, Local0)
    Return(Local0)
  }

  // 686C interface + EC has return one count value, then based on count return the following data
  Method(LCMD, 2, Serialized)
  {
    Name(LBUF, Buffer(30) {0x00})
    If (LNotEqual(WIBE(EC686CCMD), ACPI_SUCCESS)) {
      Return(DEVICE_ERROR)
    }
    If (LNotEqual(WOBE(EC686CCMD), ACPI_SUCCESS)) {
      Return(DEVICE_ERROR)
    }
    Store(Arg0, EC6C)
    If (LNotEqual(WIBE(EC686CCMD), ACPI_SUCCESS)) {
      Return(DEVICE_ERROR)
    }

    If(LAnd(LNotEqual(Arg1,0x00),LNotEqual(Arg1,0xff))) {
      Store(Arg1, EC68)
      If (LNotEqual(WIBE(EC686CCMD), ACPI_SUCCESS)) {
        Return(DEVICE_ERROR)
      }
    }

    If (LNotEqual(WOBF(EC686CCMD), ACPI_SUCCESS)) {
      Return(DEVICE_ERROR)
    }
    Store(EC68, Local0)
    Store(0, Local1)
    While(Local0) {
      If (LNotEqual(WOBF(EC686CCMD), ACPI_SUCCESS)) {
        Return(DEVICE_ERROR)
      }
      Store(EC68, Index(LBUF, Local1))
      Increment(Local1)
      Decrement(Local0)
    }
    Return(LBUF)
  }
    //686C interface + EC don't return value to BIOS
  Method(NCMD, 2, Serialized)
  {
    If (LNotEqual(WIBE(EC686CCMD), ACPI_SUCCESS)) {
      Return(DEVICE_ERROR)
    }
    If (LNotEqual(WOBE(EC686CCMD), ACPI_SUCCESS)) {
      Return(DEVICE_ERROR)
    }
    Store(Arg0, EC6C)
    If (LNotEqual(WIBE(EC686CCMD), ACPI_SUCCESS)) {
      Return(DEVICE_ERROR)
    }

    If(LAnd(LNotEqual(Arg1,0x00),LNotEqual(Arg1,0xff))) {
      Store(Arg1, EC68)
      If (LNotEqual(WIBE(EC686CCMD), ACPI_SUCCESS)) {
        Return(DEVICE_ERROR)
      }
    }
    Return(ACPI_SUCCESS)
  }
//#[-start-211028-Dongxu0027-add]##  
  //686C interface +  EC don't return value to BIOS + 3 Arg
  Method(FCMD, 3, Serialized)
  {
    If (LNotEqual(WIBE(EC686CCMD), ACPI_SUCCESS)) {
      Return(DEVICE_ERROR)
    }
    If (LNotEqual(WOBE(EC686CCMD), ACPI_SUCCESS)) {
      Return(DEVICE_ERROR)
    }
    Store(Arg0, EC6C)
    If (LNotEqual(WIBE(EC686CCMD), ACPI_SUCCESS)) {
      Return(DEVICE_ERROR)
    }

    If(LAnd(LNotEqual(Arg1,0x00),LNotEqual(Arg1,0xff))) {
      Store(Arg1, EC68)
      If (LNotEqual(WIBE(EC686CCMD), ACPI_SUCCESS)) {
        Return(DEVICE_ERROR)
      }
    }
    
    If(LAnd(LNotEqual(Arg2,0x00),LNotEqual(Arg2,0xff))) {
      Store(Arg2, EC68)
      If (LNotEqual(WIBE(EC686CCMD), ACPI_SUCCESS)) {
        Return(DEVICE_ERROR)
      }
    }
    Return(ACPI_SUCCESS)
  }

    //686C interface + EC has return one value + 3 Arg
    Method(BCMD, 3, Serialized)
  {
    Name(LBUF, Buffer(30) {0x00})
    If (LNotEqual(WIBE(EC686CCMD), ACPI_SUCCESS)) {
      Return(DEVICE_ERROR)
    }
    If (LNotEqual(WOBE(EC686CCMD), ACPI_SUCCESS)) {
      Return(DEVICE_ERROR)
    }
    Store(Arg0, EC6C)
    If (LNotEqual(WIBE(EC686CCMD), ACPI_SUCCESS)) {
      Return(DEVICE_ERROR)
    }

    If(LAnd(LNotEqual(Arg1,0x00),LNotEqual(Arg1,0xff))) {
      Store(Arg1, EC68)
      If (LNotEqual(WIBE(EC686CCMD), ACPI_SUCCESS)) {
        Return(DEVICE_ERROR)
      }
    }

    If(LAnd(LNotEqual(Arg2,0x00),LNotEqual(Arg2,0xff))) {
      Store(Arg2, EC68)
      If (LNotEqual(WIBE(EC686CCMD), ACPI_SUCCESS)) {
        Return(DEVICE_ERROR)
      }
    }

    If (LNotEqual(WOBF(EC686CCMD), ACPI_SUCCESS)) {
      Return(DEVICE_ERROR)
    }
    Store(EC68, Local0)
    Return(Local0)
    /*
    Store(0, Local1)
    While(Local0) {
      If (LNotEqual(WOBF(EC686CCMD), ACPI_SUCCESS)) {
        Return(DEVICE_ERROR)
      }
      Store(EC68, Index(LBUF, Local1))
      Increment(Local1)
      Decrement(Local0)
    }
    Return(LBUF)
    */
  }
//#[-end-211028-Dongxu0027-add]## 
    
  // 6266 interface + EC has return one count value, then based on count return the following data
  Method(SCMD, 2, Serialized)
  {
    Name(LBUF, Buffer(30) {0x00})
    If (LNotEqual(WIBE(EC6266CMD), ACPI_SUCCESS)) {
      Return(DEVICE_ERROR)
    }
    If (LNotEqual(WOBE(EC6266CMD), ACPI_SUCCESS)) {
      Return(DEVICE_ERROR)
    }
    Store(Arg0, EC66)
    If (LNotEqual(WIBE(EC6266CMD), ACPI_SUCCESS)) {
      Return(DEVICE_ERROR)
    }

    If(LAnd(LNotEqual(Arg1,0x00),LNotEqual(Arg1,0xff))) {
      Store(Arg1, EC62)
      If (LNotEqual(WIBE(EC6266CMD), ACPI_SUCCESS)) {
        Return(DEVICE_ERROR)
      }
    }

    If (LNotEqual(WOBF(EC6266CMD), ACPI_SUCCESS)) {
      Return(DEVICE_ERROR)
    }
    Store(EC62, Local0)
    Store(0, Local1)
    While(Local0) {
      If (LNotEqual(WOBF(EC6266CMD), ACPI_SUCCESS)) {
        Return(DEVICE_ERROR)
      }
      Store(EC62, Index(LBUF, Local1))
      Increment(Local1)
      Decrement(Local0)
    }
    Return(LBUF)
  }
  //6266 interface + EC don't return value to BIOS
  Method(TCMD, 2, Serialized)
  {
    //Name(LBUF, Buffer(30) {0x00})
    If (LNotEqual(WIBE(EC6266CMD), ACPI_SUCCESS)) {
      Return(DEVICE_ERROR)
    }
    If (LNotEqual(WOBE(EC6266CMD), ACPI_SUCCESS)) {
      Return(DEVICE_ERROR)
    }
    Store(Arg0, EC66)
    If (LNotEqual(WIBE(EC6266CMD), ACPI_SUCCESS)) {
      Return(DEVICE_ERROR)
    }

    If(LAnd(LNotEqual(Arg1,0x00),LNotEqual(Arg1,0xff))) {
      Store(Arg1, EC62)
      If (LNotEqual(WIBE(EC6266CMD), ACPI_SUCCESS)) {
        Return(DEVICE_ERROR)
      }
    }

    Return(ACPI_SUCCESS)
  }
  Method(LRAM, 2, Serialized)
  {
    If (LNotEqual(WIBE(EC686CCMD), ACPI_SUCCESS)) {
      Return(DEVICE_ERROR)
    }
    If (LNotEqual(WOBE(EC686CCMD), ACPI_SUCCESS)) {
      Return(DEVICE_ERROR)
    }
    Store(ECRAM_READ, EC6C)
    If (LNotEqual(WIBE(EC686CCMD), ACPI_SUCCESS)) {
      Return(DEVICE_ERROR)
    }

    // EC RAM Bank Index
    Store(Arg0, EC68)
    If (LNotEqual(WIBE(EC686CCMD), ACPI_SUCCESS)) {
      Return(DEVICE_ERROR)
    }

    // EC RAM Offset
    Store(Arg1, EC68)
    If (LNotEqual(WIBE(EC686CCMD), ACPI_SUCCESS)) {
      Return(DEVICE_ERROR)
    }

    If (LNotEqual(WOBF(EC686CCMD), ACPI_SUCCESS)) {
      Return(DEVICE_ERROR)
    }
    Return(EC68)
  }

  Method(SRAM, 2, Serialized)
  {
    If (LNotEqual(WIBE(EC6266CMD), ACPI_SUCCESS)) {
      Return(DEVICE_ERROR)
    }
    If (LNotEqual(WOBE(EC6266CMD), ACPI_SUCCESS)) {
      Return(DEVICE_ERROR)
    }
    Store(ECRAM_READ, EC66)
    If (LNotEqual(WIBE(EC6266CMD), ACPI_SUCCESS)) {
      Return(DEVICE_ERROR)
    }

    // EC RAM Bank Index
    Store(Arg0, EC62)
    If (LNotEqual(WIBE(EC6266CMD), ACPI_SUCCESS)) {
      Return(DEVICE_ERROR)
    }

    // EC RAM Offset
    Store(Arg1, EC62)
    If (LNotEqual(WIBE(EC6266CMD), ACPI_SUCCESS)) {
      Return(DEVICE_ERROR)
    }

    If (LNotEqual(WOBF(EC6266CMD), ACPI_SUCCESS)) {
      Return(DEVICE_ERROR)
    }
    Return(EC62)
  }

  Include("Battery.asl")
  Include("Vpc.asl")

  //LCFCTODO: if your project is Yoga, please add this code.

//[-start-210826-KEBIN00043-modify]//
#if  LENOVO_YMC_DRIVER_FEATURE
  Include ("Cind.asl")  
#endif
//[-end-210826-KEBIN00043-modify]//
  Scope (\)
  {
    Include("LfcNvsArea.asl")

    OperationRegion (SMIO, SystemIO, 0xB2, 0x02)
    Field (SMIO, ByteAcc, NoLock, Preserve) {
      SMBA, 8,
      SMBB, 8
    }

  }

  // Name: _REG - This method is called by drivers to register installation
  //              and removal.  For example; OS calls _REG(3,1) to indicate
  //              to the ACPI BIOS that the Embedded Controller Driver is
  //              present and functional.
  //
  //              ARG0: 0=Memory
  //                    1=I/O
  //                    2=PCI Config
  //                    3=Embedded Controller
  //                    4=SMBus
  //
  //              ARG1: 0=Handler Not Connected
  //                    1=Handler Connected
  // The _REG Method is needed because communication with the EC
  // before the driver is loaded is prohibited in WIN2000/WINXP.

  Method(_REG,2)
  {
    If(LEqual(Arg0,3))
    {
      Store(Arg1,ECAV)
    }

    If(LAnd(LEqual(Arg0,3),LEqual(Arg1,1)))
    {
      #if PLATFORM_VENDOR_INTEL

        If(LEqual(OSYS, 2006))  // Vista
        {
          Store(0x1,local0)
        }
        If(LEqual(OSYS, 1000))  // Linux
        {
          Store(0x2,local0)
        }
        If(LEqual(OSYS, 2009)) // Win7
        {
          Store(0x3,local0)
        }
        If(LEqual(OSYS, 2012)) // Win8
        {
          Store(0x4,local0)
        }
        If(LEqual(OSYS, 2013)) // WinBlue
        {
          Store(0x5,local0)
        }
        If(LEqual(OSYS, 2015)) // Win10
        {
          Store(0x6,local0)
        }
      #else

        If (LEqual(TPOS, OSTYPE_VSTA))
        {
          Store(0x1, Local0)
        }
        If (LEqual(TPOS, OSTYPE_LINUX))
        {
          Store(0x2, Local0)
        }
        If (LEqual(TPOS, OSTYPE_WINDOWS7))
        {
          Store(0x3, Local0)
        }
        If (LEqual(TPOS, OSTYPE_WINDOWS8))
        {
          Store(0x4, Local0)
        }
        If (LEqual(TPOS, OSTYPE_WINDOWS81))
        {
          Store(0x5, Local0)
        }
        If (LEqual(TPOS, OSTYPE_WINDOWS10))
        {
          Store(0x6, Local0)
        }
      #endif
      If(LEqual(Acquire(EC0_SCOPE.EC0.LfcM, 0xA000),0x0)){
        Store(local0, EC0_SCOPE.EC0.OSTY)
        #if PLATFORM_VENDOR_INTEL
          If (LEqual(LSTE, 0))
          {
            Store(0,VGA_SCOPE.CLID)
          }
          If (LEqual(LSTE, 1))
          {
            Store(3,VGA_SCOPE.CLID)
          }
        #endif
        Store(LSTE,LIDS)
        Release(EC0_SCOPE.EC0.LfcM)
      }
    }
  }

//[-start-210916-FLINT00020-modify]//
//[-start-210924-TAMT000013-modify]//
//[-start-211221-QINGLIN0131-modify]//
#if defined(C970_SUPPORT) || defined(C770_SUPPORT) || defined(S77013_SUPPORT) || defined(S77014_SUPPORT) || defined(S370_SUPPORT) || defined(S77014IAH_SUPPORT)
//[-end-211221-QINGLIN0131-modify]//
//[-end-210924-TAMT000013-modify]//
  Method (CMFC, 0, Serialized)
  {
    Name(EDXX, buffer(128) {})
    CreateField (EDXX,  0,64, EDI1)
    CreateField (EDXX,  64,16, EDI2)
    CreateField (EDXX,  80,16, EDI3)
    CreateField (EDXX,  96,928,EDI4)
    store(HEAD,EDI1)
    store(MFID,EDI2)
    store(PAID,EDI3)
    store(REST,EDI4)
    return (EDXX)
  }
#else
  Method (CMFC, 0, Serialized)
  {
    return (EDID)
  }
#endif
//[-end-210916-FLINT00020-modify]//

  Method (LFCI, 2, Serialized)
  {

    //Reserve two parameters for future use.

    Switch (ToInteger (Arg0))
    {
      case (0x01)
      {
        return (OKRB)
      }
      case (0x02)
      {
        Store(RECM(LFC_CMOS_CLEARED_INDEX), Local0)
        Store(Local0, P80B)

        return (Local0)
      }
    }
  }

//[-start-210830-BAIN000032-add]//
//[-start-210830-YUNLEI0127-modify]//
//[-start-210911-QINGLIN0064-modify]//
//[-start-211013-Ching000012-modify]//
//[-start-211111-QINGLIN0113-modify]//
//[-start-211115-Ching000016-modify]//
#if defined(C970_SUPPORT) || defined(C770_SUPPORT) || defined(S370_SUPPORT) || defined(S77014_SUPPORT) || defined(S570_SUPPORT) || defined(S77013_SUPPORT) || defined(S77014IAH_SUPPORT)
//[-end-211115-Ching000016-modify]//
//[-end-211111-QINGLIN0113-modify]//
//[-end-211013-Ching000012-modify]//
//[-end-210911-QINGLIN0064-modify]//
  // Routine Description:
  //     Check status of SM Bus Protocol
  //
  // Arguments:
  //
  // Return Value:
  //
  //     Zero: STATS_SUCCESS
  //
  //     Else: I2C error code
  //
  Method( CHKS, 0 ) {
    //
    // Wait for SMBus transition complete
    //
    Store( 1000, Local0 )

    while( HMPR ){
      //
      // Wait 1 msec
      //
      Sleep( 1 )

      Decrement( Local0 )
      If( LNot( Local0 ) ){
        //
        // Time out error.
        //
        Return( I2C_TIMEOUT )
      }
    }

    //
    // Check Status
    //
    If( And(SMST,0x80) ) {
        //
        // Completed
        //
        Return( Zero )
    }

    //
    // Command not completed
    //
    Return( I2C_CMD_NOT_COMPLETE )
  }
#endif
//[-end-210830-YUNLEI0127-modify]//
//[-end-210830-BAIN000032-add]//

  // For the below _Qxx Methods, The Runtime SCI has been asserted,
  // the EC Driver performed it's query, and the value returned from
  // the Query = xx.
  //
  //Decrease LCD panel backlight brightness
  //
  Method (_Q11, 0, NotSerialized)
  {
    #if LENOVO_TURN_OFF_LCD_VIA_F11     //Turn off LCD back-light when users push the brightness key
                                        //to the last level once again.
      If (IGDS){                        //Primary display
                                        //The lowest 7 bits of CBLV are used for brigtness levels
                                        //according to the test result.
        #if PLATFORM_VENDOR_INTEL
          If (LEqual(And(VGA_SCOPE.CBLV,0x7f), 1)) //CBLV changes from 0x80000064 to 0x80000001.
          {
            Store(1,BKLT) //turn off LCD back-light
          }
        #else
          //LCFCTODO: Add this feature for AMD platfom
        #endif
      }
    #endif
    Store(0x11, P80B)
    Notify(EDP_SCOPE, 0x87)
    Notify(EC0_SCOPE.EC0.VPC0,0x80)
//[-start-211210-MANDY0001-add]//
    Store(0xF5, WEID)
    Notify(\_SB.WM00, 0x80)
//[-end-211210-MANDY0001-add]//
  }

  //
  //Increase LCD panel backlight brightness
  //
  Method (_Q12, 0, NotSerialized)
  {
    #if LENOVO_TURN_OFF_LCD_VIA_F11
                                    //Turn on LCD back-light when users push the brightness key
                                    //the second last level.In addtion to that,EC will turn on
                                    //backlight when return to S0 from retart,S3,S4,S5.
      If (IGDS){                    //Primary display
        If (LEqual(BKLT,1)) //CBLV changes from 0x80000064 to 0x80000001.
        {
          Store(0,BKLT) //turn on LCD back-light
        } else {
          Store(0x12,P80B)
          Notify(EDP_SCOPE, 0x86)
          Notify(EC0_SCOPE.EC0.VPC0,0x80)
        }
      } else {
        Store(0x12,P80B)
        Notify(EDP_SCOPE, 0x86)
        Notify(EC0_SCOPE.EC0.VPC0,0x80)
      }
    #else
      Store(0x12,P80B)
      Notify(EDP_SCOPE, 0x86)
      Notify(EC0_SCOPE.EC0.VPC0,0x80)
    #endif
//[-start-211210-MANDY0001-add]//
      Store(0xF6, WEID)
      Notify(\_SB.WM00, 0x80)
//[-end-211210-MANDY0001-add]//
  }

  //
  //Cover lid open
  //
  Method (_Q15, 0, NotSerialized)
  {
    Store(0x15, P80B)
//[-start-210723-QINGLIN0001-modify]//
#ifndef LCFC_SUPPORT
    Notify(EC0_SCOPE.PS2M, 0x0E)
#endif
//[-end-210723-QINGLIN0001-modify]//

    If (ECAV) {
      If(LEqual(Acquire(EC0_SCOPE.EC0.LfcM, 0xA000),0x0)){
        Store(LSTE, LIDS)
        Release(EC0_SCOPE.EC0.LfcM)
      }
    }
    #if PLATFORM_VENDOR_INTEL
      VGA_SCOPE.GLID(LIDS)
    #endif
    Notify(\_SB.LID0, 0x80)

//[-start-210923-JOYID00005-add]//
//[-start-210924-QINGLIN0083-modify]//
//[-start-211012-TAMT000027-modify]//
#if defined(C970_SUPPORT) || defined(C770_SUPPORT) || defined(S570_SUPPORT) || defined(S370_SUPPORT) || defined(S77013_SUPPORT) || defined(S77014_SUPPORT) || defined(S77014IAH_SUPPORT)
//[-end-211012-TAMT000027-modify]//
//[-end-210924-QINGLIN0083-modify]//
//[-start-220111-JOYID00011-add]//
  If(\_SB.PC00.LPCB.EC0.MLED ==0)   {
    \_SB.SGOV(GPIO_VER2_LP_GPP_D15,1)
  }
//[-start-220111-JOYID00011-add]//  
#endif
//[-end-210723-JOYID00005-add]//


  }

  //
  //Cover lid close
  //
  Method (_Q16, 0, NotSerialized)
  {
    Store(0x16, P80B)
//[-start-210723-QINGLIN0001-modify]//
#ifndef LCFC_SUPPORT
    Notify(EC0_SCOPE.PS2M, 0x0D)
#endif
//[-end-210723-QINGLIN0001-modify]//

//[-start-210923-JOYID00005-add]//
//[-start-210924-QINGLIN0083-modify]//
//[-start-211012-TAMT000027-modify]//
#if defined(C970_SUPPORT) || defined(C770_SUPPORT) || defined(S570_SUPPORT) || defined(S370_SUPPORT) || defined(S77013_SUPPORT) || defined(S77014_SUPPORT) || defined(S77014IAH_SUPPORT)
//[-end-211012-TAMT000027-modify]//
//[-end-210924-QINGLIN0083-modify]//
    \_SB.SGOV(GPIO_VER2_LP_GPP_D15,0)
#endif
//[-end-210723-JOYID00005-add]//

    If (ECAV) {
      If(LEqual(Acquire(EC0_SCOPE.EC0.LfcM, 0xA000),0x0)){
        Store(LSTE, LIDS)
        Release(EC0_SCOPE.EC0.LfcM)
      }
    }
    #if PLATFORM_VENDOR_INTEL
      VGA_SCOPE.GLID(LIDS)
    #endif
    Notify(\_SB.LID0, 0x80)
  }


  #if PLATFORM_VENDOR_INTEL
    Method (_Q1D,0,NotSerialized)
    {
      Store(0x1D, P80B)
      // To use P-State transition to replace throttling function.
      If (ECAV) {
        If(LEqual(Acquire(EC0_SCOPE.EC0.LfcM, 0xA000),0x0)){
          Store(TLVL, Local0)
          Release(EC0_SCOPE.EC0.LfcM)
        }
      }
      // LCFCTODO: please check if _PPC is a varible or method
      //Store(Local0, \_PR.CPU0._PPC) // HSW, BDW
      Store(Local0, \_SB.CPPC) // IWL
      // Notify OS Performance Present Capabilities Changed.
//      PNOT()
    }
  #endif

//[-start-220802-SUSIE0001-Modify]//
//[-start-220509-QINGLIN0169-add]//
#ifdef LCFC_SUPPORT
//[-end-220802-SUSIE0001-Modify]//
  Method(_Q58) 
  {
    Notify(EC0_SCOPE.EC0.BAT0, 0x80)
  }
#endif
//[-end-220509-QINGLIN0169-add]//

  //
  //Battery pack plug in
  //
  Method (_Q25, 0, NotSerialized)
  {
    Store(0x25, P80B)
    Notify(\_SB.ADP0,0x80)
    Notify(EC0_SCOPE.EC0.BAT0,0x80)
    Notify(EC0_SCOPE.EC0.BAT0,0x81)
  }

  //
  //AC plug in
  //
  Method (_Q37, 0, NotSerialized)
  {
    Store(0x37, P80B)
    Sleep(300)
    Store(1,PWRS)
    Notify(\_SB.ADP0,0x80)
    Notify(EC0_SCOPE.EC0.BAT0,0x80)
    #if PLATFORM_VENDOR_INTEL
//      PNOT()
    #endif
//[-start-211019-SHAONN0011-add]//
#if defined(S370_SUPPORT)
    If (LOr(LEqual(OPTY, 1),LEqual(OPTY, 3)))  // 1- S370,3-S1415
    {
       If (LLessEqual(ADWT, 65))  // Watt <= 65
       {
         Store(65,\ODVC)
         Notify(\_SB.IETM, 0x88)
       }
       ElseIf(LGreaterEqual(ADWT, 95)) // Watt >= 95
       {
         Store(95,\ODVC)
         Notify(\_SB.IETM, 0x88)
       }
    }
#endif
//[-end-211019-SHAONN0011-add]//
//[-start-211125-JEPLIUT120-add]//  Add ODV21 for 45W adapter 
#if defined(S570_SUPPORT)
   If (LEqual(And (ADP2,0x1), 1)) 
   {
     Store(1,\ODVL)
     Notify(\_SB.IETM, 0x88)
   }
   else
   {
     Store(0,\ODVL) 
     Notify(\_SB.IETM, 0x88)  //temp , need to optimize
   }
#endif
//[-end-211125-JEPLIUT120-add]// 
  }

  //
  //AC removed
  //
  Method (_Q38, 0, NotSerialized)
  {
    Store(0x38, P80B)
    Sleep(300)
    Store(0,PWRS)
    Notify(\_SB.ADP0,0x80)
    Notify(EC0_SCOPE.EC0.BAT0,0x80)
    #if PLATFORM_VENDOR_INTEL
//      PNOT()
    #endif
//[-start-211125-JEPLIUT120-add]//
#if defined(S570_SUPPORT)
    Store(0,\ODVL) 
    Notify(\_SB.IETM, 0x88)  //temp , need to optimize
#endif
//[-end-211125-JEPLIUT120-add]//
  }

  //
  // Power Button Press Event
  //
  Method (_Q32, 0, NotSerialized)
  {
    Store(0x32, P80B)
    Notify(\_SB.PWRB, 0x80)
  }

#if LENOVO_SUPER_RESOLUTION_WMI
  //
  //Lenovo Super Resolution
  //
  Method(_Q3C)
  {
    Store(0x3C, P80B)
    Store(0x01,\_SB.WMIS.SDat)
    Notify(\_SB.WMIS, 0xD0)
  }

  Method(_Q3D)
  {
    Store(0x3D, P80B)
    Store(0x00,\_SB.WMIS.SDat)
    Notify(\_SB.WMIS, 0xD0)
  }
#endif
  // TP enable
  //
  Method (_Q3E, 0, NotSerialized)
  {
    Store(0x3E, P80B)
//[-start-210723-QINGLIN0001-modify]//
#ifndef LCFC_SUPPORT
    Notify(EC0_SCOPE.PS2M, 0x3E)
#endif
//[-end-210723-QINGLIN0001-modify]//
  }

  //
  //TP disable
  //
  Method (_Q3F, 0, NotSerialized)
  {
    Store(0x3F, P80B)
//[-start-210723-QINGLIN0001-modify]//
#ifndef LCFC_SUPPORT
    Notify(EC0_SCOPE.PS2M, 0x3F)
#endif
//[-end-210723-QINGLIN0001-modify]//
  }

  //
  // VPC notification event for Win7.
  //
  Method(_Q44)
  {
    Store(0x44, P80B)
    Notify(EC0_SCOPE.EC0.VPC0,0x80)
//[-start-211210-MANDY0001-add]//    
    Store(0xF4, WEID)
    Notify(\_SB.WM00, 0x80)
//[-end-211210-MANDY0001-add]//
  }

  Method(_Q70, 0, NotSerialized)  //Notify DPTF driver temperature has changed
  {
    Store(0x70, P80B)
    Sleep(5)
    //LCFCTODO:If support multiple sensor participants, please add all sensor participants here.
    //Notify(\_SB.PCI0.LPCB.EC0.SEN1, 0x90)
//[-start-210706-SHUI0001-add]//
    Notify(\_SB.PC00.LPCB.EC0.SEN1, 0x90)
    Sleep(5)
    Notify(\_SB.PC00.LPCB.EC0.SEN2, 0x90)
    Sleep(5)
    Notify(\_SB.PC00.LPCB.EC0.SEN3, 0x90)
    Sleep(5)
    Notify(\_SB.PC00.LPCB.EC0.SEN4, 0x90)
//[-end-210706-SHUI0001-add]//
//[-start-211117-SHAONN0018-add]//
//[-start-211201-JEPLIUT201-modify]// Or S570
#if defined(S370_SUPPORT) || defined(S570_SUPPORT) 
//[-end-211201-JEPLIUT201-modify]// 
    Sleep(5)
    Notify(\_SB.PC00.LPCB.EC0.SEN5, 0x90)
#endif

//[start-220113-STORM1124-modify]
#if defined(C770_SUPPORT) 
    Sleep(5)
    Notify(\_SB.PC00.LPCB.EC0.SEN5, 0x90)
    Sleep(5)
    Notify(\_SB.PC00.LPCB.EC0.SEN6, 0x90)
    Sleep(5)
    Notify(\_SB.PC00.LPCB.EC0.SEN7, 0x90)
#endif
//[end-220113-STORM1124-modify]

//[-end-211117-SHAONN0018-add]//    
  }

//[-start-211019-SHAONN0011-add]//
//[-start-220120-SHAONN0028-add]//
#if defined(S370_SUPPORT)
  Method(_Q73, 0, NotSerialized)
  {
    Store(0x73, P80B)
    Sleep(5)
    If (LOr(LEqual(OPTY, 1),LEqual(OPTY, 3)))  // 1- S370,3-S1415
    {
      Store(1,\ODVD)
      Notify(\_SB.IETM, 0x88)
    }
  }
  
  Method(_Q74, 0, NotSerialized)
  {
    Store(0x74, P80B)
    Sleep(5)
    If (LOr(LEqual(OPTY, 1),LEqual(OPTY, 3)))  // 1- S370,3-S1415
    {
      Store(0,\ODVD)
      Notify(\_SB.IETM, 0x88)
    }
  }

  Method(_Q77, 0, NotSerialized)
  {
    Store(0x77, P80B)
    Sleep(5)
    If (LOr(LEqual(OPTY, 1),LEqual(OPTY, 3)))  // 1- S370,3-S1415
    {
      Store(1,\ODVE)
      Notify(\_SB.IETM, 0x88)
    }
  }
  
  Method(_Q78, 0, NotSerialized)
  {
    Store(0x78, P80B)
    Sleep(5)
    If (LOr(LEqual(OPTY, 1),LEqual(OPTY, 3)))  // 1- S370,3-S1415
    {
      Store(0,\ODVE)
      Notify(\_SB.IETM, 0x88)
    }
  }
#endif
//[-end-220120-SHAONN0028-add]//
//[-end-211019-SHAONN0011-add]//


//[-start-210924-GEORGE0008-add]//
//[-start-210930-QINGLIN0089-modify]//
//#if defined(S77014_SUPPORT)
#if defined(S77014_SUPPORT) || defined(S77014IAH_SUPPORT)  
//[-end-210930-QINGLIN0089-modify]//
  Scope (\)
  {
    Name(GPUM, 0x0) //Default GPUM is 0
  }

  Method(_Q71, 0, NotSerialized)  
  {
    Store(0x71, P80B)
    Sleep(5)
//    Store(1, GPUM)   //Set GPU Target Temperature to 68/64
    Notify(\_SB.PC00.PEG2.PEGP, 0xC0) //notify driver to change GPS
  }

  Method(_Q72, 0, NotSerialized)  
  {
    Store(0x72, P80B)
    Sleep(5)
//    Store(0, GPUM) //Set GPU Target Temperature to 94
    Notify(\_SB.PC00.PEG2.PEGP, 0xC0)//notify driver to change GPS
  }

//[-start-220125-GEORGE0048-add]//
  Method(_Q73, 0, NotSerialized)
  {
    Store(0x73, P80B)
    Sleep(5)
    Store(1,\ODVF)
    Notify(\_SB.IETM, 0x88)
  }
  
  Method(_Q74, 0, NotSerialized)
  {
    Store(0x74, P80B)
    Sleep(5)
    Store(0,\ODVF)
    Notify(\_SB.IETM, 0x88)
  } 
//[-end-220125-GEORGE0048-add]//
#endif
//[-end-210924-GEORGE0008-add]//

//[-start-211203-JEPLIUT202-add]//  Align with EC event
#if defined(S570_SUPPORT)  
 Scope (\)
  {
    Name(GPUM, 0x0) //Default GPUM is 0
  }

  Method(_Q71, 0, NotSerialized)  
  {
    Store(0x71, P80B)
    Sleep(5)
    Store(1, GPUM)   //Set GPU Target Temperature to 68/64
    Notify(\_SB.PC00.PEG2.PEGP, 0xC0) //notify driver to change GPS
  }

  Method(_Q72, 0, NotSerialized)  
  {
    Store(0x72, P80B)
    Sleep(5)
    Store(0, GPUM) //Set GPU Target Temperature to  
    Notify(\_SB.PC00.PEG2.PEGP, 0xC0)//notify driver to change GPS
  }
 
  Method(_Q75, 0, NotSerialized)  
  {
    Store(0x75, P80B)
    Sleep(2000)
    Store(2, GPUM)   //Set GPU Target Temperature to  
    Notify(\_SB.PC00.PEG2.PEGP, 0xC0) //notify driver to change GPS
  }
//[-start-220415-JEPLIUT225-remove ]// 
//  Method(_Q76, 0, NotSerialized)  
//  {
//    Store(0x76, P80B)
//    Sleep(5)
//    Store(1, GPUM) //Set GPU Target Temperature  
//    Notify(\_SB.PC00.PEG2.PEGP, 0xC0)//notify driver to change GPS
//  }
//[-end-220415-JEPLIUT225-remove ]//
#endif
//[-end-211203-JEPLIUT202-add]// 


  Method(_Q86, 0, NotSerialized)  //STP enable
  {
    Store(0x86, P80B)
    Sleep(5)
    //LCFCTODO:if your project support ITS, please implement this.
//[-start-210701-KEBIN00030-modify]//    
//    \_SB.PCI0.LPCB.EC0.VPC0.DYTC(0x001F4001)
    EC0_SCOPE.EC0.VPC0.DYTC(0x001F4001)
//[-end-210701-KEBIN00030-modify]//    
  }

  Method(_Q87, 0, NotSerialized) //STP disable
  {
    Store(0x87, P80B)
    Sleep(5)
    //LCFCTODO:if your project support ITS, please implement this.
//[-start-210701-KEBIN00030-modify]//   
 //   \_SB.PCI0.LPCB.EC0.VPC0.DYTC(0x000F4001)
    EC0_SCOPE.EC0.VPC0.DYTC(0x000F4001)
//[-end-210701-KEBIN00030-modify]//    
  }

//[-start-211126-JEPLIUT123-add]// 
#if defined(S570_SUPPORT)
  Method(_Q88, 0, NotSerialized)  // battery present less than 10% + i3 ODVD  update 
  {
    Store(0x88, P80B)
    if( LEqual(\CPTY, 3) )
    { 
      Store(1,\ODVD)
      Notify(\_SB.IETM, 0x86)
    }
  }
  
  Method(_Q89, 0, NotSerialized)   //ODVD update
  {
    Store(0x89, P80B)
    if( LEqual(\ODVD, 1) ) 
    {
      Store(0,\ODVD)
      Notify(\_SB.IETM, 0x86)
    }
  }
#endif  
//[-end-211126-JEPLIUT123-add]// 

//[-start-210830-BAIN000032-add]//
//[-start-210830-YUNLEI0127-modify]//
//[-start-210911-QINGLIN0064-modify]//
//[-start-211013-Ching000012-modify]//
//[-start-211111-QINGLIN0113-modify]//
//[-start-211115-Ching000016-modify]//
#if defined(C970_SUPPORT) || defined(C770_SUPPORT) || defined(S370_SUPPORT) || defined(S77014_SUPPORT) || defined(S570_SUPPORT) || defined(S77013_SUPPORT) || defined(S77014IAH_SUPPORT)
//[-end-211115-Ching000016-modify]//
//[-end-211111-QINGLIN0113-modify]//
//[-end-211013-Ching000012-modify]//
//[-end-210911-QINGLIN0064-modify]//
  Method (_Q4F, 0, NotSerialized)
  {
    Store(0x4F, P80B)
//[-start-220321-QINGLIN0166-add]//
#if defined(S370_SUPPORT)
    If (LEqual(UCMS,1)) //Check if UCSI is supported
    {
#endif
//[-end-220321-QINGLIN0166-add]//
    \_SB.UBTC.ECRD()
    Sleep (One)
    Notify(\_SB.UBTC,0x80)
//[-start-220321-QINGLIN0166-add]//
#if defined(S370_SUPPORT)
    }
#endif
//[-end-220321-QINGLIN0166-add]//
  }
#endif
//[-end-210830-YUNLEI0127-modify]//
//[-end-210830-BAIN000032-add]//

#if LENOVO_GAMING_ZONE_FEATURE
  //
  // GPU OC Enable
  //
  Method (_Q62, 0, NotSerialized)
  {
    Store(0x62, P80B)
    If (ECAV) {
      If(LEqual(Acquire(EC0_SCOPE.EC0.LfcM, 0xA000),0x0)){
        Store(3, EC0_SCOPE.EC0.GUST)
        Notify(GZFD, 0xD0)
        Release(EC0_SCOPE.EC0.LfcM)
      }
    }
  }

  //
  // GPU OC Disable
  //
  Method (_Q63, 0, NotSerialized)
  {
    Store(0x63, P80B)
    If (ECAV) {
      If(LEqual(Acquire(EC0_SCOPE.EC0.LfcM, 0xA000),0x0)){
        Store(1, EC0_SCOPE.EC0.GUST)
        Notify(GZFD, 0xD0)
        Release(EC0_SCOPE.EC0.LfcM)
      }
    }
  }

  //
  // CPU OC Enable
  //
  Method (_Q66, 0, NotSerialized)
  {
    Store(0x66, P80B)
    If (ECAV) {
      If(LEqual(Acquire(EC0_SCOPE.EC0.LfcM, 0xA000),0x0)){
        Store(2, EC0_SCOPE.EC0.GUST)
        Notify(GZFD, 0xD0)
        Release(EC0_SCOPE.EC0.LfcM)
      }
    }
  }

  //
  // CPU OC Disable
  //
  Method (_Q67, 0, NotSerialized)
  {
    Store(0x67, P80B)
    If (ECAV) {
      If(LEqual(Acquire(EC0_SCOPE.EC0.LfcM, 0xA000),0x0)){
        Store(0, EC0_SCOPE.EC0.GUST)
        Notify(GZFD, 0xD0)
        Release(EC0_SCOPE.EC0.LfcM)
      }
    }
  }

  //
  // Turbo button event
  //
  Method (_Q68, 0, NotSerialized)
  {
    Store(0x68, P80B)
    Notify(GZFD, 0xD1)
  }

  //
  // Gpu Temperature change event
  //
  Method (_Q69, 0, NotSerialized)
  {
    Store(0x69, P80B)
    Notify(GZFD, 0xE0)
  }

  //
  // Fancooling finish event
  //
  Method (_Q6A, 0, NotSerialized)
  {
    Store(0x6A, P80B)
    Notify(GZFD, 0xE1)
  }

  //
  // Key lock status change event
  //
  Method (_Q6B, 0, NotSerialized)
  {
    Store(0x6B, P80B)
    Notify(GZFD, 0xE2)
  }

  //
  //Light Profile Change Event
  //
  Method (_QDE, 0, NotSerialized)
  {
    Store(0xDE, P80B)
    Notify(GZFD, 0xE6)
  }

  //
  // Set dGPU power
  //
  Method (_QD1, 0, NotSerialized)
  {
    Store(0xD1,P80B)
    If (ECAV) {
      If(LEqual(Acquire(EC0_SCOPE.EC0.LfcM, 0xA000),0x0)){
        Store(0x0, EC0_SCOPE.EC0.GUST)
//[-start-211114-JEPLIUT197-modify
#if defined(S570_SUPPORT)
//[-start-211203-JEPLIUT202-modify]//  
        Notify(\_SB.PC00.PEG2.PEGP, 0xD1)
//[-end-211203-JEPLIUT202-modify]//  		 
#else
        Notify(\_SB.PCI0.PEG0.PEGP, 0xD1)
#endif        
//[-end-211114-JEPLIUT197-modify
        Release(EC0_SCOPE.EC0.LfcM)
      }
    }
    Sleep(10)
  }

  Method (_QD2, 0, NotSerialized)
  {
    Store(0xD2,P80B)
    If (ECAV) {
      If(LEqual(Acquire(EC0_SCOPE.EC0.LfcM, 0xA000),0x0)){
        Store(0x0, EC0_SCOPE.EC0.GUST)
//[-start-211114-JEPLIUT197-modify//
#if defined(S570_SUPPORT)        
//[-start-211203-JEPLIUT202-modify]//  
        Notify(\_SB.PC00.PEG2.PEGP, 0xD2)
//[-end-211203-JEPLIUT202-modify]//  		
#else
        Notify(\_SB.PCI0.PEG0.PEGP, 0xD2)
#endif
//[-end-211114-JEPLIUT197-modify//      

        
        Release(EC0_SCOPE.EC0.LfcM)
      }
    }
    Sleep(10)
  }

  Method (_QD3, 0, NotSerialized)
  {
    Store(0xD3,P80B)
    If (ECAV) {
      If(LEqual(Acquire(EC0_SCOPE.EC0.LfcM, 0xA000),0x0)){
        Store(0x0, EC0_SCOPE.EC0.GUST)
//[-start-211114-JEPLIUT197-modify//
#if defined(S570_SUPPORT)  
//[-start-211203-JEPLIUT202-modify]//  
        Notify(\_SB.PC00.PEG2.PEGP, 0xD3)
//[-end-211203-JEPLIUT202-modify]// 		
//[-end-211203-JEPLIUT197-modify]// 		
#else        
        Notify(\_SB.PCI0.PEG0.PEGP, 0xD3)
#endif       
//[-end-211114-JEPLIUT197-modify//
        Release(EC0_SCOPE.EC0.LfcM)
      }
    }
    Sleep(10)
  }

  Method (_QD4, 0, NotSerialized)
  {
    Store(0xD4,P80B)
    If (ECAV) {
      If(LEqual(Acquire(EC0_SCOPE.EC0.LfcM, 0xA000),0x0)){
        Store(0x0, EC0_SCOPE.EC0.GUST)
//[-start-211114-JEPLIUT197-modify//
#if defined(S570_SUPPORT)  
//[-start-211203-JEPLIUT202-modify]//  
        Notify(\_SB.PC00.PEG2.PEGP, 0xD3)
//[-end-211203-JEPLIUT202-modify]// 
#else        
        Notify(\_SB.PCI0.PEG0.PEGP, 0xD4)
#endif       
//[-end-211114-JEPLIUT197-modify//
        
        Release(EC0_SCOPE.EC0.LfcM)
      }
    }
    Sleep(10)
  }

  Method (_QD5, 0, NotSerialized)
  {
    Store(0xD5,P80B)
    If (ECAV) {
      If(LEqual(Acquire(EC0_SCOPE.EC0.LfcM, 0xA000),0x0)){
        Store(0x0, EC0_SCOPE.EC0.GUST)
//[-start-211114-JEPLIUT197-modify//
#if defined(S570_SUPPORT)  
//[-start-211203-JEPLIUT202-modify]//  
        Notify(\_SB.PC00.PEG2.PEGP, 0xD3)
//[-end-211203-JEPLIUT202-modify]// 
#else        
        Notify(\_SB.PCI0.PEG0.PEGP, 0xD5)
#endif       
//[-end-211114-JEPLIUT197-modify//        
        Release(EC0_SCOPE.EC0.LfcM)
      }
    }
    Sleep(10)
  }
#else
#if PLATFORM_VENDOR_INTEL
  Scope (\)
  {
    Name(GPSF,0)              //Global flag for NV GPS Power Steering
    Name(DCPS,0)              //Global flag for lock P-State control, 1:Lock 0:Unlock
  }
  Name(DGST, 0xD1) //backup the dGPU vP-State
//[-start-211123-GEORGE0024-add]//
//[-start-211126-JEPLIUT122-modify]// or 570
#if defined(S77014_SUPPORT) || defined(S570_SUPPORT) || defined(S77014IAH_SUPPORT)
//[-end-211126-JEPLIUT122-modify]//
  Method(_Q1E) // Q event 0x1E
  {
    Store(0x1E, P80B)
    Store(DGDF, P80B)	
    If (LEqual(Acquire(\_SB.PC00.LPCB.EC0.HGCT, 0xA000),0x0)) {
      if (LEqual(GATY, 2))
      {
        If( LLessEqual(\_SB.PC00.LPCB.EC0.DGDF, 4) )  // DGDF = 0 ~ 4
        {
          Store(0xD1, Local0)
          Add(Local0, \_SB.PC00.LPCB.EC0.DGDF, Local0)  // Local0 = D1 ~ D5
          If(LEqual(Local0, 0xD1))  // D1
          {
            //Global flag for unlock P-State control
            Store(0, DCPS)  //Unlock P-State control
            If(LNotEqual(DGST,Local0))
            {
              Store(Local0, DGST)  //dGPU vP-State
              Notify(\_SB.PC00.PEG2.PEGP, Local0)
            }
            Sleep (10)

            //
            // Enable GPS Power Steering
            //
            If(LEqual(GPSF, 0))
            {
              Store(1, GPSF)
              Notify(\_SB.PC00.PEG2.PEGP, Local0)
            }
          }
          Else  // D2 ~ D5
          {
            //Global flag for lock P-State control
            Store(1, DCPS)
            Store(0x4E, P80B) 
            If(LNotEqual(DGST,Local0))
            {
              Store(0x5E, P80B)
              Store(Local0, DGST)
              Notify(\_SB.PC00.PEG2.PEGP, Local0)

            }
            Sleep (10)

            //
            // Disable GPS Power Steering
            //
            If(LEqual(GPSF, 1))
            {
              Store(0, GPSF)
              Notify(\_SB.PC00.PEG2.PEGP, 0xC0)
            }
          } 
        }
      }
    Release(EC0_SCOPE.EC0.HGCT) 
    }
  }
#endif
//[-end-211123-GEORGE0024-add]//
 //[-start-210520-KEBIN00003-modify] //
//[-start-210803-QINGLIN0008-modify]//
//[-start-210802-SHAONN0003-modify]//
//[-start-210914-DABING0006-modify]//
#if defined(C970_SUPPORT) || defined(C770_SUPPORT) || defined(S370_SUPPORT) || defined(S77013_SUPPORT)
//[-end-210914-DABING0006-modify]//
//[-end-210802-SHAONN0003-modify]//
//[-end-210803-QINGLIN0008-modify]//
#else
  Method(_Q62)
  {
    If(ECAV) {
      If(LEqual(Acquire(EC0_SCOPE.EC0.LfcM, 0xA000),0x0)){
        if( LEqual(GATY, 2) )                   //Nvidia VGA card
        {
          //Global flag for lock P-State control
          Store(1, DCPS)
          If(LNotEqual(DGST,0xD2))
          {
            Store(0x62,P80B)
            Store(0xD2, DGST)
            //LCFCTODO: please check the ACPI path as your project codebase.
//[-start-210722-QINGLIN0001-modify]//
//[-start-210817-DABING0002-modify]//
#if defined(S570_SUPPORT) || defined(S77014_SUPPORT) || defined(S77014IAH_SUPPORT)
            Notify(\_SB.PC00.PEG2.PEGP, 0xD2)
#else
            Notify(\_SB.PCI0.RP05.PXSX, 0xD2)
#endif
//[-end-210817-DABING0002-modify]//
//[-end-210722-QINGLIN0001-modify]//
          }
          Sleep (10)

          //
          //Disable GPS Power Steering
          //
          If(LEqual(GPSF,1))
          {
            Store(0, GPSF)
            //LCFCTODO: please check the ACPI path as your project codebase.
//[-start-210722-QINGLIN0001-modify]//
//[-start-210817-DABING0002-modify]//
#if defined(S570_SUPPORT) || defined(S77014_SUPPORT) || defined(S77014IAH_SUPPORT)
            Notify(\_SB.PC00.PEG2.PEGP, 0xC0)
#else
            Notify(\_SB.PCI0.RP05.PXSX, 0xC0)
#endif
//[-end-210817-DABING0002-modify]//
//[-end-210722-QINGLIN0001-modify]//
          }
        }
        Release(EC0_SCOPE.EC0.LfcM)
      }
    }
  }
  Method(_Q63)
  {
    If (ECAV) {
      If(LEqual(Acquire(EC0_SCOPE.EC0.LfcM, 0xA000),0x0)){
        if( LEqual(GATY, 2) )                 //Nvidia VGA card
        {
          //Global flag for unlock P-State control
          Store(0, DCPS)
          If(LNotEqual(DGST,0xD1))
          {
            Store(0x63, P80B)
            Store(0xD1, DGST)
            //LCFCTODO: please check the ACPI path as your project codebase.
//[-start-210722-QINGLIN0001-modify]//
//[-start-210817-DABING0002-modify]//
#if defined(S570_SUPPORT) || defined(S77014_SUPPORT) || defined(S77014IAH_SUPPORT)
            Notify(\_SB.PC00.PEG2.PEGP, 0xD1)
#else
            Notify(\_SB.PCI0.RP05.PXSX, 0xD1)
#endif
//[-end-210817-DABING0002-modify]//
//[-end-210722-QINGLIN0001-modify]//
          }
          Sleep (10)

          //
          //Enable GPS Power Steering
          //
          If(LEqual(GPSF,0))
          {
            Store(1, GPSF)
            //LCFCTODO: please check the ACPI path as your project codebase.
//[-start-210722-QINGLIN0001-modify]//
//[-start-210817-DABING0002-modify]//
#if defined(S570_SUPPORT) || defined(S77014_SUPPORT) || defined(S77014IAH_SUPPORT)
            Notify(\_SB.PC00.PEG2.PEGP, 0xC0)
#else
            Notify(\_SB.PCI0.RP05.PXSX, 0xC0)
#endif
//[-end-210817-DABING0002-modify]//
//[-end-210722-QINGLIN0001-modify]//
          }
        }
        Release(EC0_SCOPE.EC0.LfcM)
      }
    }
  }
 #endif
 //[-end-210520-KEBIN00003-modify]//
  Method(_Q66)
  {
  // Thermal event for turbo off
  // LCFCTODO: Need confirm if this software SMI conflict with other.
    Store (0x66, P80B)
    Store (0xCC, SMBA)   // Trigger the SW SMI 0xCC OEM_SMM_TURBO_OFF_CALLBACK for Turbo Off
  }

  Method(_Q67)
  {
  // Thermal event for turbo on
  // LCFCTODO: Need confirm if this software SMI conflict with other.
    Store (0x67, P80B)
    Store (0xCD, SMBA)   // Trigger the SW SMI 0xCD OEM_SMM_TURBO_ON_CALLBACK for Turbo ON
  }
#endif
#endif

//[-start-200327-CLYDE00001-add]//
  Method(_Q6C)
  {
  // Save current time SMI
  // LCFCTODO: Need confirm if this software SMI conflict with other.
    Store (0xCE, SMBA)   // Trigger the SW SMI 0xCE
  }
//[-end-200327-CLYDE00001-add]//

  // EC SCI method for hotkey of Paper Looking
#if  LENOVO_PAPER_LOOKING_FEATURE
  Method (_Q45, 0, NotSerialized)
  {
    Notify(\_SB.WMI1, 0x80)
  }
#endif

  // EC SCI method for notification for LCFC app
#if  LCFC_WMI_MAPPER_DEVICE
  Method (PMON, 0, NotSerialized)
  {
    Store(0, \_SB.LWMI.PIND)
    Notify(\_SB.LWMI, 0x90)
  }

  Method (PMOF, 0, NotSerialized)
  {
    Store(1, \_SB.LWMI.PIND)
    Notify(\_SB.LWMI, 0x90)
  }

  Method (PMTE, 1, NotSerialized)
  {
    Store(Arg0, Local0)
    Store(Local0, \_SB.LWMI.PIND)
    Notify(\_SB.LWMI, 0x90)
  }


#endif


#if  LENOVO_YMC_DRIVER_FEATURE
//[-start-210826-KEBIN00043-modify]//
  // Event notification for a new mode or data change event signal
  //LCFCTODO: please follow project design to use correct Q event
//   Method (_Q47, 0, NotSerialized)
//   {
//     Store(0x47,P80B)
//     Notify(\_SB.CYMC, 0x90)
//   }
    Method (_Q49, 0, NotSerialized)
  {  
    Store(0x49,P80B)
    Notify(WMIY, 0xD0)
  }
//[-end-210826-KEBIN00043-modify]//
#endif
// EC SCI method for notification for Lenovo PCM app

#if LENOVO_SUPER_KEY_WMI
  Scope (\)
  {
    Name(LSKD, 0x0) //Default LSK Data is 0
  }

  Method (_QDF, 0, NotSerialized)
  {
    Store(0xDF,P80B)
//[-start-210823-Dongxu0012-add]//      
    Store(0,LSKD) //clear LSKD value before notify Lenovo Vantange.
//[-end-210823-Dongxu0012-add]//  

//[-start-211021-KEBIN00063-Modify]//
If(LEqual(EC0_SCOPE.EC0.MSRF, 1))  //MS resume flag was set
    {
    Store(0,EC0_SCOPE.EC0.MSRF)
  }
 Else
   {
    If(LNotEqual(EC0_SCOPE.EC0.LSKV,0)) //A key is pressed.
    {
//[-start-211220-GEORGE0035-modify]//
//[-start-210930-QINGLIN0088-modify]//
#if defined(S370_SUPPORT) || defined(S77014_SUPPORT) || defined(S77013_SUPPORT) || defined(S77014IAH_SUPPORT)
      If(LLessEqual(EC0_SCOPE.EC0.LSKV,0x2F)) //LSKV must equal or less than 40
#else
      If(LLess(EC0_SCOPE.EC0.LSKV,0x2F)) //LSKV must equal or less than 40
#endif
//[-end-210930-QINGLIN0088-modify]//
//[-end-211220-GEORGE0035-modify]//
      {
        Store(EC0_SCOPE.EC0.LSKV,LSKD)
      }

      Store(0,EC0_SCOPE.EC0.LSKV)//clear this byte after read
      Notify(\_SB.WMIU, 0xD0)  //notify Supereky WIMI when superkey pressed
    }
//    Notify(\_SB.WMIU, 0xD0)  //notify Lenovo Vantange  
 }
//[-end-211021-KEBIN00063-Modify]//    
//[-start-210802-QINGLIN0006-modify]//
//[-start-210803-QINGLIN0008-modify]//
//[-start-210802-SHAONN0003-modify]//
//[start-210801-STORM1102-modify]//
//#if defined(C970_SUPPORT)
//[-start-210924-TAMT000012-A-modify]//
#if defined(C970_SUPPORT) || defined(C770_SUPPORT) || defined(S570_SUPPORT) || defined(S370_SUPPORT) || defined(S77013_SUPPORT)
//[-end-210924-TAMT000012-A-modify]//
//[end-210801-STORM1102-modify]//
      If(LEqual(EC0_SCOPE.EC0.HKDB,0)) //"FN+ESC" is pressed  [0xA1 Bit3]
      {
        \_SB.SGOV(GPIO_VER2_LP_GPP_H20, 1)  //"ESC" LED Active low level
      }
      Else
      {
        \_SB.SGOV(GPIO_VER2_LP_GPP_H20, 0)
      }
#endif
//[-start-210918-Ching000002-modify]//
//[-start-210924-TAMT000012-A-modify]//
#if defined(S77014_SUPPORT) || defined(S77014IAH_SUPPORT)
//[-end-210924-TAMT000012-A-modify]//
      If(LEqual(EC0_SCOPE.EC0.HKDB,0)) //"FN+ESC" is pressed  [0xA1 Bit3]
      {
        \_SB.SGOV(GPIO_VER2_LP_GPP_H20, 0)  //"ESC" LED Active low level
      }
      Else
      {
        \_SB.SGOV(GPIO_VER2_LP_GPP_H20, 1)
      }

      If(LEqual(EC0_SCOPE.EC0.CAPL,0)) //"CapsLk" is pressed  [0xA1 Bit5]
      {
        \_SB.SGOV(GPIO_VER2_LP_GPP_H22, 0)  //"CapsLk" LED Active low level
      }
      Else
      {
        \_SB.SGOV(GPIO_VER2_LP_GPP_H22, 1)
      }
#endif
//[-end-210918-Ching000002-modify]//

#if defined(S570_SUPPORT)
      If(LEqual(EC0_SCOPE.EC0.NUML,0)) //"NumLock" is pressed  [0xA1 Bit4]
      {
        \_SB.SGOV(GPIO_VER2_LP_GPP_H21, 1)  //"NumLock" LED Active low level
      }
      Else
      {
        \_SB.SGOV(GPIO_VER2_LP_GPP_H21, 0)
      }
#elif defined(S370_SUPPORT)
      If(LEqual(EC0_SCOPE.EC0.NUML,0)) //"NumLock" is pressed  [0xA1 Bit4]
      {
        \_SB.SGOV(GPIO_VER2_LP_GPP_D4, 1)  //"NumLock" LED Active low level
      }
      Else
      {
        \_SB.SGOV(GPIO_VER2_LP_GPP_D4, 0)
      }
//[-start-210819-YUNLEI0123-add]//
#elif defined(C770_SUPPORT)
      If (LEqual(MCSZ, 0x16))  // C770 16'
      {
        If(LEqual(EC0_SCOPE.EC0.NUML,0)) //"NumLock" is pressed  [0xA1 Bit4]
        {
          \_SB.SGOV(GPIO_VER2_LP_GPP_D4, 1)  //"NumLock" LED Active low level
        }
        Else
        {
          \_SB.SGOV(GPIO_VER2_LP_GPP_D4, 0)
        }
      }
//[-end-210819-YUNLEI0123-add]//
#else
#endif

//[start-210801-STORM1102-modify]//
//#if defined(C970_SUPPORT)
//[-start-210924-TAMT000012-A-modify]//
#if defined(C970_SUPPORT) || defined(C770_SUPPORT) || defined(S570_SUPPORT) || defined(S370_SUPPORT) || defined(S77013_SUPPORT)
//[-end-210924-TAMT000012-A-modify]//
//[end-210801-STORM1102-modify]//
      If(LEqual(EC0_SCOPE.EC0.CAPL,0)) //"CapsLk" is pressed  [0xA1 Bit5]
      {
        \_SB.SGOV(GPIO_VER2_LP_GPP_H22, 1)  //"CapsLk" LED Active low level
      }
      Else
      {
        \_SB.SGOV(GPIO_VER2_LP_GPP_H22, 0)
      }
#endif
//[-end-210802-SHAONN0003-modify]//
//[-end-210803-QINGLIN0008-modify]//
//[-end-210802-QINGLIN0006-modify]//

  }
#endif
}

Scope(\_SB)
{
  //
  // Define an AC Device.
  //
  Device(ADP0)
  {
    Name(_HID,"ACPI0003")
    #if PLATFORM_VENDOR_INTEL
    #else
    Name(XX00, Buffer(0x03){})
    #endif
    Method(_STA)
    {
      If (LEqual(ECON,1)){
        Return(0x0F)
      }
      Return(0x00)
    }

    Name(ACDC, 0xFF)   // 0:DC, 1:AC, others: Invalid

//[-start-220415-BAIN000101-modify]//
    //
    // Return the value that determines if running
    // from AC or not.
    Method(_PSR,0)
    {
      If (EC0_SCOPE.EC0.ECAV) {
        If(LEqual(Acquire(EC0_SCOPE.EC0.LfcM, 0xA000),0x0)){
          #if PLATFORM_VENDOR_INTEL
            Store(0x01,Local0)
            Store(EC0_SCOPE.EC0.ADPT,Local0)
            Release(EC0_SCOPE.EC0.LfcM)
            Return(Local0)
          #else
            Store(0x01,Local0)
            Store(EC0_SCOPE.EC0.ADPT,Local0)
            CreateWordField(XX00, 0, SSZE)          // Structure size field
            CreateByteField(XX00, 2, ACST)          // AC/DC field
            Store(3, SSZE)                          // table size 3
            //If the status not change, then it no need to notification 'AFN4' and
            //'ALIB' method.
      //LCFCTODO:Follow AMD BIOS Implemention spec to check this 'AC/DC STATUS CHANGE NOTIFICATION' again
            If(LNotEqual(Local0, ACDC)) {
                If(Local0)
                {
                    Store(0xECAC,P80B)
                    VGA_SCOPE.AFN4(0x01)            //notify the GPU driver
                    Store(0, ACST)
                }
                Else
                {
                    Store(0xECDC,P80B)
                    VGA_SCOPE.AFN4(0x02)           //notify the GPU driver
                    Store(1, ACST)
                }
                \_SB.ALIB(1, XX00)                 // notify SMU AC/DC Status
                Store(Local0, ACDC)

            }
            Release(EC0_SCOPE.EC0.LfcM)
            Return(Local0)
          #endif
        }
      }else{
        Return(0) //EC not available return DC status
      }
    }
//[-end-220415-BAIN000101-modify]//

    //
    // Return that everything runs off of AC.
    //
    Method(_PCL,0)
    {
      Return (
        Package() { _SB }
      )
    }
  }

  //
  // Define a Lid Switch.
  //
  Device(LID0)
  {
    Name(_HID,EISAID("PNP0C0D"))

    Method(_STA)
    {
      If(LEqual(ECON,1)){
        Return(0x0F)
      }
      Return(0x00)
    }

    Method(_LID,0)
    {
      // 0 = Closed, 1 = Open.
      Return (LIDS)
    }

  }

  //
  // Define a (Control Method) Power Button.
  //
  Device(PWRB)
  {
    Name(_HID,EISAID("PNP0C0C"))
    Method(_STA)
    {
      If(LEqual(ECON,1))
      {
        Return(0x0F)
      }
      Return(0x00)
    }
  }//end device PWRB

  //
  // Define Lenovo Paper Looking Wmi Device.
  //
#if  LENOVO_PAPER_LOOKING_FEATURE
  Include("PaperLookingFeature.asl")
#endif

#if LCFC_WMI_MAPPER_DEVICE
  Device(LWMI){
    Name(_HID, EISAID("PNP0C14")) // PNP0C14 is Plug and Play ID assigned to WMI mapper
    Name(_UID, "LCFC")            // We could define more WMI devices, but need define different unique _UID
    Name(_WDG, Buffer(){
      // {5A7C482A-37C3-4B2A-A7B6-E03CF752F158}
      0x2A, 0x48, 0x7C, 0x5A, 0xC3, 0x37, 0x2A, 0x4B, 0xA7, 0xB6, 0xE0, 0x3C, 0xF7, 0x52, 0xF1, 0x58,
      0x90, 0, // Notfy ID
      0x01,    // Instance Count
      0x08,    // Flags (WMIACPI_REGFLAG_EVENT)

      // Specific GUID for eject MOF file into ASL.
      0x21, 0x12, 0x90, 0x05, 0x66, 0xd5, 0xd1, 0x11, 0xb2, 0xf0, 0x00, 0xa0, 0xc9, 0x06, 0x29, 0x10,
      65, 66, // We could change the ID, but need match WQxx
      1,      // Instance Count
      0x00,   // Flag
      })
    Name(PIND, 0)
    // Return additional notify ID value to App.
    Method(_WED, 1){
      Switch(PIND){
        case(0){
          Return(0x55)
        }
        case(1){
          Return(0xAA)
        }
        case(2){
          Return(0x66)
        }
        case(3){
          Return(0xBB)
        }
        case(4){
          Return(0xCC)
        }
        case(5){
          Return(0xDD)
        }
      }
    }

    // Generated by mofcomp.exe version 6.1.7600.16385, the content correspond to MOF file.
    Name(WQAB, Buffer(567) {
      0x46, 0x4f, 0x4d, 0x42, 0x01, 0x00, 0x00, 0x00, 0x27, 0x02, 0x00, 0x00, 0xac, 0x05, 0x00, 0x00,
      0x44, 0x53, 0x00, 0x01, 0x1a, 0x7d, 0xda, 0x54, 0x18, 0xc0, 0x82, 0x00, 0x01, 0x06, 0x18, 0x42,
      0x10, 0x05, 0x10, 0x22, 0x21, 0x04, 0x12, 0x01, 0xa1, 0xc8, 0x2c, 0x0c, 0x86, 0x10, 0x38, 0x2e,
      0x84, 0x1c, 0x40, 0x48, 0x1c, 0x14, 0x4a, 0x08, 0x84, 0xfa, 0x13, 0xc8, 0xaf, 0x00, 0x84, 0x0e,
      0x05, 0xc8, 0x14, 0x60, 0x50, 0x80, 0x53, 0x04, 0x11, 0xf4, 0x2a, 0xc0, 0xa6, 0x00, 0x93, 0x02,
      0x2c, 0x0a, 0xd0, 0x2e, 0xc0, 0xb2, 0x00, 0xdd, 0x02, 0xa4, 0xc3, 0x12, 0x91, 0xe0, 0x28, 0x31,
      0xe0, 0x28, 0x9d, 0xd8, 0xc2, 0x0d, 0x1b, 0xbc, 0x50, 0x14, 0xcd, 0x20, 0x4a, 0x82, 0xca, 0x05,
      0xf8, 0x46, 0x10, 0x78, 0xb9, 0x02, 0x24, 0x4f, 0x40, 0x9a, 0x05, 0x18, 0x16, 0x60, 0x5d, 0x80,
      0xec, 0x21, 0x50, 0xa9, 0x43, 0x40, 0xc9, 0x19, 0x02, 0x6a, 0x00, 0xad, 0x4e, 0x40, 0xf8, 0x95,
      0x4e, 0x09, 0x49, 0x10, 0xce, 0x58, 0xc5, 0xe3, 0x6b, 0x16, 0x4d, 0xcf, 0x49, 0xce, 0x31, 0xe4,
      0x78, 0x5c, 0xe8, 0x41, 0x70, 0xd9, 0x12, 0x40, 0x98, 0xe4, 0x21, 0x4b, 0x28, 0x0c, 0x4a, 0xc3,
      0x58, 0xa8, 0x8b, 0x51, 0xa3, 0x46, 0xca, 0x06, 0x64, 0x88, 0x92, 0xe0, 0x50, 0xa3, 0xb6, 0x44,
      0x01, 0xe6, 0xc7, 0xa1, 0xd9, 0xb5, 0x3d, 0x16, 0x02, 0xc9, 0xa4, 0x0d, 0x05, 0x25, 0x05, 0x42,
      0x63, 0x3b, 0x53, 0xcf, 0x28, 0xe2, 0x91, 0x1a, 0x26, 0x81, 0x47, 0x11, 0xd9, 0x68, 0x7c, 0xf4,
      0x30, 0x86, 0x11, 0xf5, 0x00, 0x4f, 0xe2, 0x14, 0x2d, 0x7c, 0x7c, 0xc2, 0x08, 0x7a, 0x28, 0xc7,
      0x60, 0x41, 0x73, 0xd5, 0x20, 0x8d, 0x17, 0x29, 0x5e, 0x9c, 0x03, 0xc5, 0x8e, 0x07, 0x37, 0x30,
      0x0f, 0x3c, 0xe6, 0xff, 0xff, 0x0c, 0x0e, 0xec, 0x29, 0x20, 0x64, 0x02, 0x4f, 0x31, 0xfe, 0xb3,
      0x40, 0x61, 0x33, 0x17, 0x6a, 0x8c, 0xe6, 0xc6, 0xcc, 0xa0, 0x51, 0x32, 0x20, 0xe4, 0xf9, 0x44,
      0xa0, 0xb1, 0x99, 0xc0, 0xee, 0x05, 0x58, 0xbb, 0x08, 0xc8, 0xdb, 0x55, 0x40, 0xc0, 0x0f, 0x0d,
      0x95, 0xc1, 0xc8, 0x96, 0x00, 0x67, 0x40, 0x04, 0x23, 0x4a, 0xa8, 0x0a, 0xa1, 0x28, 0xa4, 0x61,
      0x6a, 0x43, 0xd1, 0xa0, 0x0e, 0x23, 0x5c, 0xc0, 0x1a, 0x91, 0x04, 0x1f, 0x2f, 0x48, 0x63, 0x20,
      0x82, 0x6f, 0x7f, 0x10, 0x24, 0xf6, 0x7b, 0x42, 0x27, 0x01, 0x47, 0x1a, 0x0d, 0xea, 0x98, 0xe0,
      0xd3, 0x80, 0x4f, 0x02, 0x67, 0xe9, 0x21, 0x9e, 0x5b, 0x90, 0xa3, 0x3b, 0xb8, 0x87, 0x87, 0xc7,
      0x00, 0x0f, 0x8f, 0xcd, 0xd3, 0xa7, 0x0a, 0x9f, 0x0b, 0xf0, 0xae, 0x01, 0x75, 0x21, 0x78, 0x1c,
      0x60, 0x63, 0x0e, 0x87, 0x19, 0xa2, 0xe7, 0x1d, 0xee, 0x04, 0x0e, 0x91, 0x01, 0x7a, 0x58, 0xc7,
      0x8d, 0x1d, 0xd9, 0xc9, 0x1c, 0xf4, 0x9b, 0x44, 0xb3, 0x77, 0x08, 0x42, 0x70, 0x3c, 0xbe, 0x3e,
      0xb0, 0x09, 0x27, 0xb0, 0xfc, 0x41, 0xa0, 0x46, 0x66, 0x68, 0x0f, 0xf5, 0xb4, 0xde, 0x09, 0x7c,
      0x1a, 0x30, 0x81, 0xc5, 0x1e, 0x42, 0xe8, 0x78, 0xc0, 0xaf, 0xf8, 0xc8, 0x41, 0x6e, 0x06, 0x9e,
      0xaf, 0x09, 0x86, 0x87, 0x90, 0x91, 0xf1, 0xa0, 0x06, 0x62, 0xd4, 0xd7, 0x05, 0xdc, 0xbc, 0x1f,
      0x3f, 0x3c, 0xa5, 0xa7, 0x8f, 0x03, 0x3e, 0x86, 0x53, 0x71, 0x44, 0x08, 0x7d, 0x31, 0x2c, 0x6a,
      0xec, 0xf4, 0x40, 0xc1, 0xff, 0xff, 0x67, 0x09, 0x76, 0x89, 0x38, 0x8d, 0xe7, 0x00, 0xc3, 0xb2,
      0xff, 0x8a, 0x47, 0x07, 0xff, 0x54, 0x01, 0x3c, 0x64, 0x8d, 0x17, 0xfa, 0x5d, 0xc2, 0xb0, 0x3e,
      0xcc, 0x58, 0xe0, 0x69, 0x06, 0x14, 0x43, 0xc6, 0x9e, 0x67, 0x80, 0xd5, 0x58, 0x9f, 0x67, 0xc0,
      0x76, 0xe4, 0x81, 0x33, 0x00, 0xae, 0xd0, 0xa6, 0x4f, 0x8d, 0x46, 0xad, 0x1a, 0x94, 0xa9, 0x51,
      0xa6, 0x41, 0xad, 0x3e, 0x95, 0x1a, 0x33, 0x76, 0x2e, 0xb0, 0x08, 0x0a, 0x81, 0x38, 0x02, 0x08,
      0x0d, 0xf0, 0x3c, 0x10, 0x88, 0xff, 0xff
      })
  }
#endif

  //
  // WMI-to-ACPI mapper device.
  //
  Device(WMI4)
  {
    // PNP0C14 is Plug and Play ID assigned to WMI mapper
    Name(_HID, EISAID("PNP0C14"))
    Name(_UID, 4)
    Mutex(MWMI,0x0)

    //
    // _WDG evaluates to a data structure that specifies the data
    // blocks supported by the ACPI device.
    //
    Name(_WDG, Buffer() {
      0x76, 0x37, 0xa0, 0xc3, 0xac, 0x51, 0xaa, 0x49, 0xad, 0x0f, 0xf2, 0xf7, 0xd6, 0x2c, 0x3f, 0x3c,
      65, 68, // Object ID (AD)
      3,      // Instance Count Should be 3 for Single Battery System.
      0x05,   // Flags WMIACPI_REGFLAG_EXPENSIVE & STRING
      0x21, 0x12, 0x90, 0x05, 0x66, 0xd5, 0xd1, 0x11, 0xb2, 0xf0, 0x00, 0xa0, 0xc9, 0x06, 0x29, 0x10,
      66, 68, // Object ID (BD)
      1,      // Instance Count
      0x00,   // Flags
      })

    //
    // Items
    //
    // Battery ID, Fucntion, Item name
    //
    Name(ITEM, Package() {
      Package() {0, 0, "BAT0 BatMaker"},
      Package() {0, 1, "BAT0 HwId "},
      Package() {0, 2, "BAT0 MfgDate "},
    })

    //
    // WQAD() - Data block query for Lenovo_BatteryInfomation
    //
    // This method returns the instance data for the specified
    // instance index.
    //
    // Arg0: Instance index of data block
    //
    // Return: Error string.
    //
    Method(WQAD, 1) {
      If (EC0_SCOPE.EC0.ECAV) {
        If(LEqual(Acquire(EC0_SCOPE.EC0.LfcM, 0xA000),0x0)){ // Serialize buffer access
          Store( PSAG(Arg0), Local0)
          Store(DerefOf(Index(ITEM, Local0)), Local1)
          Store(DerefOf(Index(Local1, 0)), Local2) // Local1: BAT ID
          Store(DerefOf(Index(Local1, 1)), Local3) // Local2: Function ID
          Store(DerefOf(Index(Local1, 2)), Local4) // Local3: Item name
          Store (BATD (Local2, Local3), Local5)
          Concatenate(Local4, ",", Local6)
          Concatenate(Local6, Local5, Local7)
          Release(EC0_SCOPE.EC0.LfcM)
        }
      }
      Return(Local7)
    }

    Method(PSAG, 1) {
      Return(Arg0)
    }

    //
    // BATD() - Return Battery Information
    //
    // This method returns the Battery Information.
    //
    // Arg0: Battery ID
    // 0 - Primary battery
    // 1 - Seconday battery
    //
    // Arg1: Requred Item
    // 0 - BatMaker
    // 1 - HwId
    // 2 - MfgDate
    //
    // Return: Requrend Information.
    //

    Method(BATD, 2) {
      Name(STRB, Buffer(10) {0x00})
      Name(BUFR, Buffer(0x08){})
      Store(EC0_SCOPE.EC0.FWBT, BUFR)
      // Prepare Byte field
      CreateWordField(BUFR, 0x00, MID0)
      CreateWordField(BUFR, 0x02, HID0)
      CreateWordField(BUFR, 0x04, FIR0)
      CreateWordField(BUFR, 0x06, DAT0)
      If (LEqual(Arg0, 0)) {
        If (LEqual(Arg1, 0)) {
          //
          // Return Primary Battery Marker
          // 0x37 Byte 0 & 1
          //
          Store(ToHexString(MID0), STRB)
        }
        If (LEqual(Arg1, 1)) {
          // Return Primary Battery ID
          // 0x37 Byte 2 & 3
          //
          Store(ToHexString(HID0), STRB)
        }
        If (LEqual(Arg1, 2)) {
          //
          // Return Primary Battery MfgDate
          // 0x1B
          //
          Store(EC0_SCOPE.EC0.B1DA, Local0)

          Name(DATB, Buffer(){0x30,0x30,0x30,0x30,0x30,0x30,0x30,0x30,0x00})
          Store(7, Local3)

          Store(And(Local0,0x1F), Local1)
          While(Local1) {
            Divide(Local1, 10, Local2, Local1)
            Add(Local2,0x30, Index(DATB, Local3))
            Decrement(Local3)
          }

          Store(5, Local3)
          Store(ShiftRight(And(Local0,0x1E0), 5), Local1)
          While(Local1) {
            Divide(Local1, 10, Local2, Local1)
            Add(Local2,0x30, Index(DATB, Local3))
            Decrement(Local3)
          }

          Store(3, Local3)
          Store(Add(ShiftRight(And(Local0, 0xFE00) , 9), 1980), Local1)
          While(Local1) {
            Divide(Local1, 10, Local2, Local1)
            Add(Local2,0x30, Index(DATB, Local3))
            Decrement(Local3)
          }
          Store(DATB, STRB)
        }
      }
      Return(ToString(STRB))
    }

    Name(WQBD, Buffer()
    {
      0x46,0x4F,0x4D,0x42,0x01,0x00,0x00,0x00,0x65,0x02,0x00,0x00,0xF8,0x05,0x00,0x00,
      0x44,0x53,0x00,0x01,0x1A,0x7D,0xDA,0x54,0x18,0xD1,0x82,0x00,0x01,0x06,0x18,0x42,
      0x10,0x05,0x10,0x8A,0x0D,0x21,0x02,0x0B,0x83,0x50,0x50,0x18,0x14,0xA0,0x45,0x41,
      0xC8,0x05,0x14,0x95,0x02,0x21,0xC3,0x02,0x14,0x0B,0x70,0x2E,0x40,0xBA,0x00,0xE5,
      0x28,0x72,0x0C,0x22,0x02,0xF7,0xEF,0x0F,0x31,0xD0,0x18,0xA8,0x50,0x08,0x89,0x00,
      0xA6,0x42,0xE0,0x08,0x41,0xBF,0x02,0x10,0x3A,0x14,0x20,0x53,0x80,0x41,0x01,0x4E,
      0x11,0x44,0x10,0xA5,0x65,0x01,0xBA,0x05,0xF8,0x16,0xA0,0x1D,0x42,0x68,0x91,0x9A,
      0x9F,0x04,0x81,0x6A,0x5B,0x80,0x45,0x01,0xB2,0x41,0x08,0xA0,0xC7,0xC1,0x44,0x0E,
      0x02,0x25,0x66,0x10,0x28,0x9D,0x73,0x90,0x4D,0x60,0xE1,0x9F,0x4C,0x94,0xF3,0x88,
      0x92,0xE0,0xA8,0x0E,0x22,0x42,0xF0,0x72,0x05,0x48,0x9E,0x80,0x34,0x4F,0x4C,0xD6,
      0x07,0xA1,0x21,0xB0,0x11,0xF0,0x88,0x12,0x40,0x58,0xA0,0x75,0x2A,0x14,0x0C,0xCA,
      0x03,0x88,0xE4,0x8C,0x15,0x05,0x6C,0xAF,0x13,0x91,0xC9,0x81,0x52,0x49,0x70,0xA8,
      0x61,0x5A,0xE2,0xEC,0x34,0xB2,0x13,0x39,0xB6,0xA6,0x87,0x2C,0x48,0x26,0x6D,0x28,
      0xA8,0xB1,0x7B,0x5A,0x27,0xE5,0x99,0x46,0x3C,0x28,0xC3,0x24,0xF0,0x28,0x18,0x1A,
      0x27,0x28,0x0B,0x42,0x0E,0x06,0x8A,0x02,0x3C,0x09,0xCF,0xB1,0x78,0x01,0xC2,0x67,
      0x4C,0xA6,0x1D,0x23,0x81,0xCF,0x04,0x1E,0xE6,0x31,0x63,0x47,0x14,0x2E,0xE0,0xF9,
      0x1C,0x43,0xE4,0xB8,0x87,0x1A,0xE3,0x28,0x22,0x3F,0x08,0x60,0x05,0x1D,0x04,0x90,
      0x38,0xFF,0xFF,0xE3,0x89,0x76,0xDA,0xC1,0x42,0xC7,0x39,0xBF,0xD0,0x18,0xD1,0xE3,
      0x40,0xC9,0x80,0x90,0x47,0x01,0x56,0x61,0x35,0x91,0x04,0xBE,0x07,0x74,0x76,0x12,
      0xD0,0xA5,0x21,0x46,0x6F,0x08,0xD2,0x26,0xC0,0x96,0x00,0x6B,0x02,0x8C,0xDD,0x06,
      0x08,0xCA,0xD1,0x36,0x87,0x22,0x84,0x28,0x21,0xE2,0x86,0xAC,0x11,0x45,0x10,0x95,
      0x41,0x08,0x35,0x50,0xD8,0x28,0xF1,0x8D,0x13,0x22,0x48,0x02,0x8F,0x1C,0x77,0x04,
      0xF0,0xD8,0x0E,0xE8,0x04,0x4F,0xE9,0x71,0xC1,0x04,0x9E,0xF7,0xC1,0x1D,0xEA,0x21,
      0x1C,0x70,0xD4,0x18,0xC7,0xF1,0x4C,0x40,0x16,0x2E,0x0D,0x20,0x8A,0x04,0x8F,0x3A,
      0x32,0xF8,0x70,0xE0,0x41,0x7A,0x9E,0x9E,0x40,0x90,0x43,0x38,0x82,0xC7,0x86,0xA7,
      0x02,0x8F,0x81,0x5D,0x17,0x7C,0x0E,0xF0,0x31,0x01,0xEF,0x1A,0x50,0xA3,0x7E,0x3A,
      0x60,0x93,0x0E,0x87,0x19,0xAE,0x87,0x1D,0xEE,0x04,0x1E,0x0E,0x1E,0x33,0xF8,0x91,
      0xC3,0x83,0xC3,0xCD,0xF0,0x64,0x8E,0xAC,0x54,0x01,0x66,0x4F,0x08,0x3A,0x4D,0xF8,
      0xCC,0xC1,0x6E,0x00,0xE7,0xD3,0x33,0x24,0x91,0x3F,0x08,0xD4,0xC8,0x0C,0xED,0x69,
      0xBF,0x7A,0x18,0xF2,0xA1,0xE0,0xB0,0x98,0xD8,0xB3,0x07,0x1D,0x0F,0xF8,0xAF,0x24,
      0x0F,0x1B,0x9E,0xBE,0xE7,0x6B,0x82,0x91,0x07,0x8E,0x1E,0x88,0xA1,0x9F,0x38,0x0E,
      0xE3,0x34,0x7C,0x09,0xF1,0x39,0xE0,0xFF,0x1F,0x24,0xC6,0x31,0x79,0x70,0x3C,0xD8,
      0xC8,0xE9,0x51,0xC5,0x47,0x0A,0x7E,0xBE,0xF0,0x91,0x82,0x5D,0x10,0x9E,0x1C,0x0C,
      0x71,0x38,0x67,0xE5,0x13,0x85,0x0F,0x2A,0xB8,0x13,0x05,0x5C,0x85,0xE8,0xE4,0x36,
      0x61,0xB4,0x67,0x81,0xC7,0x09,0x98,0x07,0x01,0xF0,0x8D,0xDF,0x07,0x19,0xB0,0x4D,
      0x09,0x3B,0x24,0x78,0x47,0x19,0xE0,0x71,0x32,0xC1,0x1D,0x27,0x3C,0x04,0x3E,0x80,
      0x87,0x90,0x93,0xB4,0xD2,0xA9,0x21,0xCF,0x3C,0x60,0x1B,0x06,0x57,0x68,0xD3,0xA7,
      0x46,0xA3,0x56,0x0D,0xCA,0xD4,0x28,0xD3,0xA0,0x56,0x9F,0x4A,0x8D,0x19,0xFB,0xE1,
      0x58,0xDC,0xBB,0x40,0x07,0x03,0x0B,0x7B,0x21,0xE8,0x88,0xE0,0x58,0x20,0x34,0x08,
      0x9D,0x40,0xFC,0xFF,0x07
      })
  }

#if LENOVO_YMC_DRIVER_FEATURE
  Include("YmcDriverFeature.asl")
#endif

#if LENOVO_GAMING_ZONE_FEATURE
  Include("GamingZoneFeature.asl")
#endif
#if LENOVO_SUPER_KEY_WMI
  Include("SuperKeyWmi.asl")
#endif
#if LENOVO_SUPER_RESOLUTION_WMI
  Include("SuperResolutionWmi.asl")
#endif

//[-start-210823-KEBIN00041-add]//
//[-start-210825-QINGLIN0034-modify]//
#if !defined(S570_SUPPORT) && !defined(S370_SUPPORT) && !defined(C770_SUPPORT)
  Include("LfcTof.asl")
#endif
//#[-start-211117-Dongxu0032-add]##  Thermal Tool support
#if defined(C970_SUPPORT) 
  Include("LFCWmi.asl")
#endif  
//#[-end-211117-Dongxu0032-add]##   
//[-end-210825-QINGLIN0034-modify]//
//[-start-210823-KEBIN00041-add]//
}//end scope _SB


