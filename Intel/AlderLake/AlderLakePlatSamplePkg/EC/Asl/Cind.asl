Device(CYMC)
{
  Name(_HID, EISAID("YMC2017"))
  Name(_UID, 0)
  Method(_STA) {
    Return(0x0f)
  }
}
Device(CIND)
{
   Name(_HID, "CIND0C60")
   Name(_CID, "PNP0C60")
   Method(_STA) {
    If(LGreaterEqual(OSYS,2012))
    {
      Return(0x0f)
    }
      Return(0)
   }
}
