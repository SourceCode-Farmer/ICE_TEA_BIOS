
//*****************************************************************************
//
// Copyright (c) 2012 - 2019, Hefei LCFC Information Technology Co.Ltd. 
// And/or its affiliates. All rights reserved. 
// Hefei LCFC Information Technology Co.Ltd. PROPRIETARY/CONFIDENTIAL. 
// Use is subject to license terms.
// 
//******************************************************************************
//++
// LISSD : Virtual device for Lenovo Intelligent Sensor Service (LISS) driver, For S740 is Zero Touch Driver.
  Device(LISD)
  {
    //
    // PnP ID for LISS (Lenovo Intelligent Sensor Service Device)
    //
    Name(_HID, "IDEA2002")  // PnP ID for Virtual LISS Device
    Method(_STA, 0, NotSerialized)
    {
      Return(0x0F)
    }
  }

//[-start-220124-DABING0036-add]//
#if defined(S77013_SUPPORT)
  Device(LSFD)
  {
    //
    // PnP ID for  (Lenovo TOF Gesture, Lenovo Sensor Fusion Driver)
    //
    Name(_HID, "IDEA200A")  // PnP ID for Virtual LSFD Device
    Method(_STA, 0, NotSerialized)
    {
      Return(0x0F)
    }
  }
#endif  
//[-end-220124-DABING0036-add]//
