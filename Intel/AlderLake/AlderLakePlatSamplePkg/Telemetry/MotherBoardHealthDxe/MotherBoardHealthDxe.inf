## @file
# 
#******************************************************************************
#* Copyright 2021 Insyde Software Corp. All Rights Reserved.
#*
#* You may not reproduce, distribute, publish, display, perform, modify, adapt,
#* transmit, broadcast, present, recite, release, license or otherwise exploit
#* any part of this publication in any form, by any means, without the prior
#* written permission of Insyde Software Corp.
#*
#******************************************************************************
## @file
#  Mother Board Health driver.
#
#  Copyright (c) 2019 - 2020, Intel Corporation. All rights reserved.<BR>
#  This program and the accompanying materials
#  are licensed and made available under the terms and conditions of the BSD License
#  which accompanies this distribution.  The full text of the license may be found at
#  http://opensource.org/licenses/bsd-license.php
#
#  THE PROGRAM IS DISTRIBUTED UNDER THE BSD LICENSE ON AN "AS IS" BASIS,
#  WITHOUT WARRANTIES OR REPRESENTATIONS OF ANY KIND, EITHER EXPRESS OR IMPLIED.
#
#
##

[Defines]
  INF_VERSION                    = 0x00010005
  BASE_NAME                      = MotherBoardHealthDxe
  FILE_GUID                      = 6A79EBF1-BBB6-42A7-BDA1-5EACF52F2480
  MODULE_TYPE                    = DXE_DRIVER
  VERSION_STRING                 = 1.0

  ENTRY_POINT                    = MotherBoardHealthDxeEntryPoint

[Sources.common]
  MotherBoardHealthDxe.c

[Packages]
  MdePkg/MdePkg.dec
  MdeModulePkg/MdeModulePkg.dec
  AlderLakeBoardPkg/BoardPkg.dec
  AlderLakePlatSamplePkg/PlatformPkg.dec

[LibraryClasses]
  UefiLib
  UefiDriverEntryPoint
  UefiBootServicesTableLib
  DebugLib
  BaseMemoryLib
  MemoryAllocationLib

[Protocols]
  gEfiAdapterInformationProtocolGuid            ## SOMETIMES_PRODUCES
  gEfiDevicePathProtocolGuid                    ## PRODUCES

[Guids]
  gAdapterInfoMotherBoardHealthGuid             ## SOMETIMES_CONSUMES ## GUID
  gMotherBoardHealthVariableGuid                ## SOMETIMES_CONSUMES ## Variable:L"MotherBoardHealth"

[Depex]
#[-start-210505-IB18170061-add]#
  gEfiFirmwareManagementProtocolGuid  AND
  gEfiVariableArchProtocolGuid        AND
  gEfiVariableWriteArchProtocolGuid
#[-end-210505-IB18170061-add]#
