/** @file
  Mother Board drive Health implementation head file.

Copyright (c) 2019 - 2020, Intel Corporation. All rights reserved.<BR>
This program and the accompanying materials
are licensed and made available under the terms and conditions of the BSD License
which accompanies this distribution.  The full text of the license may be found at
http://opensource.org/licenses/bsd-license.php

THE PROGRAM IS DISTRIBUTED UNDER THE BSD LICENSE ON AN "AS IS" BASIS,
WITHOUT WARRANTIES OR REPRESENTATIONS OF ANY KIND, EITHER EXPRESS OR IMPLIED.

**/

#ifndef _DXE_MOTHER_BOARD_H_
#define _DXE_MOTHER_BOARD_H_

#include <Library/UefiBootServicesTableLib.h>
#include <Library/DebugLib.h>
#include <Protocol/DevicePath.h>
#include <Library/MemoryAllocationLib.h>
#include <Library/BaseMemoryLib.h>
#include <Protocol/DevicePath.h>
#include <Library/DevicePathLib.h>
#include <Library/UefiLib.h>
#include <Library/UefiRuntimeServicesTableLib.h>

#include <Telemetry/Include/IndustryStandard/PlatformHealthAssesmentTable.h>
#include <Telemetry/Include/TelemetryCommon.h>

///
/// structure Revision (as defined in Telemetry Gen 2.0 spec.)
///
#define MOTHER_BOARD_AIP_HEADER_REVISION  0x01
#define MOTHER_BOARD_AIP_DATA_REVISION    0x01

typedef struct {
  EFI_HANDLE                                  Handle;
  EFI_ADAPTER_INFORMATION_PROTOCOL            AdapterInfo;
  EFI_DEVICE_PATH_PROTOCOL                    *MBHealthDevicePath;
} MOTHER_BOARD_HEALTH_DEV;

#endif // #ifndef _DXE_MOTHER_BOARD_H_

