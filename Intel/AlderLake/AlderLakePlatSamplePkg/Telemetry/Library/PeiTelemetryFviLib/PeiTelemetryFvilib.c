/** @file
  PEI Telemetry Lib implementation.

  Copyright (c) 2020, Intel Corporation. All rights reserved.<BR>
  This program and the accompanying materials
  are licensed and made available under the terms and conditions of the BSD License
  which accompanies this distribution. The full text of the license may be found at
  http://opensource.org/licenses/bsd-license.php

  THE PROGRAM IS DISTRIBUTED UNDER THE BSD LICENSE ON AN "AS IS" BASIS,
  WITHOUT WARRANTIES OR REPRESENTATIONS OF ANY KIND, EITHER EXPRESS OR IMPLIED.
**/

#include <Uefi.h>
#include <Library/BaseLib.h>
#include <Library/DebugLib.h>
#include <Library/BaseMemoryLib.h>
#include <Library/PeiServicesLib.h>
#include <Library/MemoryAllocationLib.h>
#include <Library/HobLib.h>
#include <Telemetry/Include/Pi/TelemetryHob.h>
#include <Telemetry/Include/TelemetryCommon.h>

/**
  Append an entry after FVI AIP table.

  @param[in] ComponentId      ComponentId for the FVI entry.
  @param[in] Version          Version for FVI entry.
  @param[in] ProducerId       ProducerId for FVI entry, e.g. 'INTC'.

  @retval EFI_SUCCESS           - if the data is successfully reported.
  @retval EFI_OUT_OF_RESOURCES  - if not able to get resources.
  @retval EFI_INVALID_PARAMETER - if Data == NULL.
**/
EFI_STATUS
EFIAPI
AppendTelemetryFviBlock (
  IN  EFI_GUID  ComponentId,
  IN  UINT64    Version,
  IN  UINT32    ProducerId
  )
{
  TELEMETRY_FIRMWARE_VERSION_INFO2_HOB *FviHob;
  TELEMETRY_DATA_RECORD                *TelemetryVersionRecord;
  UINT8                                FviHobSize;
  UINT8                                FviHobHeaderSize;
  VOID                                 *FviHobPtr;

  FviHobPtr        = NULL;
  FviHobHeaderSize = OFFSET_OF (TELEMETRY_FIRMWARE_VERSION_INFO2_HOB, Records);
  FviHobSize        = FviHobHeaderSize + sizeof (TELEMETRY_DATA_RECORD);

  //
  // Initialize FVI Telemetry data.
  //
  FviHob = (TELEMETRY_FIRMWARE_VERSION_INFO2_HOB *) AllocateZeroPool (FviHobSize);
  if (FviHob == NULL) {
    ASSERT_EFI_ERROR (EFI_OUT_OF_RESOURCES);
    return EFI_OUT_OF_RESOURCES;
  }

  FviHob->Header.HobType        = EFI_HOB_TYPE_GUID_EXTENSION;
  FviHob->Header.HobLength      = FviHobSize;
  FviHob->Header.Reserved       = 0;
  FviHob->Revision              = TELEMETRY_HOB_STRUCTURE_REVISION;
  CopyGuid (&FviHob->Signature, &gTelemetryVersionHobGuid);
  FviHob->RecordCount  = 1;

  TelemetryVersionRecord = (TELEMETRY_DATA_RECORD *) ((UINTN)FviHob + FviHobHeaderSize);

  TelemetryVersionRecord->Version     = Version;
  TelemetryVersionRecord->ProducerId  = ProducerId;
  CopyGuid (&TelemetryVersionRecord->ComponentId, &ComponentId);

  FviHobPtr = BuildGuidDataHob (
                &gTelemetryVersionHobGuid,
                (VOID *) FviHob,
                FviHobSize
                );
  ASSERT (FviHobPtr != NULL);

  FreePool (FviHob);
  return EFI_SUCCESS;
}
