/** @file
  Dxe Telemetry Acpi Lib implementation.

  Copyright (c) 2019 - 2021, Intel Corporation. All rights reserved.<BR>
  This program and the accompanying materials
  are licensed and made available under the terms and conditions of the BSD License
  which accompanies this distribution. The full text of the license may be found at
  http://opensource.org/licenses/bsd-license.php

  THE PROGRAM IS DISTRIBUTED UNDER THE BSD LICENSE ON AN "AS IS" BASIS,
  WITHOUT WARRANTIES OR REPRESENTATIONS OF ANY KIND, EITHER EXPRESS OR IMPLIED.
**/
#ifndef _DXE_TELEMETRY_ACPI_LIB_H_
#define _DXE_TELEMETRY_ACPI_LIB_H_

#include <Uefi.h>
#include <Library/BaseLib.h>
#include <Library/UefiLib.h>
#include <Library/DebugLib.h>
#include <Library/BaseMemoryLib.h>
#include <Telemetry/Include/IndustryStandard/PlatformHealthAssesmentTable.h>
#include <Protocol/AcpiTable.h>
#include <Guid/Acpi.h>
#include <Library/UefiBootServicesTableLib.h>
#include <Library/MemoryAllocationLib.h>
#include <Telemetry/Include/Pi/TelemetryHob.h>
#include <Library/HobLib.h>
#include <IndustryStandard/Acpi61.h>
#include <Protocol/ReportStatusCodeHandler.h>
#include <Guid/FirmwarePerformance.h>
#include <Library/UefiRuntimeServicesTableLib.h>
#include <Telemetry/Include/TelemetryCommon.h>
#include <Protocol/DevicePath.h>
#include <Library/DevicePathLib.h>
#include <Protocol/FirmwareManagement.h>
#include <Include/Guid/CapsuleReport.h>

#define CAPSULELAST                           L"CapsuleLast"
#define CAPSULE_VAR_NAME                      L"Capsule"


extern GLOBAL_REMOVE_IF_UNREFERENCED EFI_GUID  *mDriverHealthSupportList[];

#endif
