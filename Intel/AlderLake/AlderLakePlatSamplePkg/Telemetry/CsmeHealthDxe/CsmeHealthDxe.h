/** @file
  Definitions for CsmeHealthDxe driver

@copyright
  INTEL CONFIDENTIAL
  Copyright 2019 - 2020 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains an 'Intel Peripheral Driver' and is uniquely identified as
  "Intel Reference Module" and is licensed for Intel CPUs and chipsets under
  the terms of your license agreement with Intel or your vendor. This file may
  be modified by the user, subject to additional terms of the license agreement.

@par Specification Reference:
**/
#ifndef _CSME_HEALTH_DXE_H_
#define _CSME_HEALTH_DXE_H_

#include <PiDxe.h>
#include <Library/IoLib.h>
#include <Library/HobLib.h>
#include <Library/UefiLib.h>
#include <Library/DebugLib.h>
#include <Library/BaseMemoryLib.h>
#include <Library/MemoryAllocationLib.h>
#include <Library/UefiBootServicesTableLib.h>
#include <IndustryStandard/Pci22.h>
#include <Library/MeFwStsLib.h>
#include <Library/PciSegmentLib.h>
#include <Protocol/AdapterInformation.h>
#include <Telemetry/Include/Protocol/TelemetryAdapterInformation.h>
#include <Telemetry/Include/IndustryStandard/PlatformHealthAssesmentTable.h>
#include <Protocol/DevicePath.h>
#include <Library/DevicePathLib.h>
#include <MeFwHob.h>
#include <Register/HeciRegs.h>
#include <Telemetry/Include/TelemetryCommon.h>

///
/// structure Revision (as defined in Telemetry Gen 2.0 spec.)
///
#define CSME_AIP_HEADER_REVISION  0x01
#define CSME_AIP_DATA_REVISION    0x01

#endif
