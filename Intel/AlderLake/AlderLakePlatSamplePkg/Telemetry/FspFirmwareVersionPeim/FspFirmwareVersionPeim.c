/** @file
  FSP Firmware version for telemetry PEIM implementation

@copyright
  INTEL CONFIDENTIAL
  Copyright 2019 - 2020 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains a 'Sample Driver' and is licensed as such under the terms
  of your license agreement with Intel or your vendor. This file may be modified
  by the user, subject to the additional terms of the license agreement.

@par Specification Reference:

**/

#include <Uefi.h>
#include <PiPei.h>
#include <Library/DebugLib.h>
#include <Library/BaseMemoryLib.h>
#include <Library/PeiServicesLib.h>
#include <Library/MemoryAllocationLib.h>
#include <Library/HobLib.h>
#include <Pi/TelemetryHob.h>
#include <TelemetryCommon.h>
#include <Library/TelemetryFviLib.h>
#include <FspInfoHob.h>

#define  FSP_BIN_VERSION_STRING       L"FSP Binary Version"

/**
  Publish FSP binary version via Telemetry Version

  @param[in]     None

  @retval        EFI_SUCCESS
  @retval        EFI_NOT_FOUND.
  @retval        EFI_OUT_OF_RESOURCES.
**/
EFI_STATUS
PublishFspVersion ( VOID )
{

  UINT64            FspTelemetryVersionVal;
  FSP_INFO_HOB      *FspInfo;
  EFI_HOB_GUID_TYPE *GuidHob;

  //
  // Locate FSP_INFO_HOB
  //
  GuidHob = GetFirstGuidHob (&gFspInfoGuid);
  ASSERT (GuidHob != NULL);
  if (GuidHob == NULL) {
    return EFI_NOT_FOUND;
  }

  FspInfo = (FSP_INFO_HOB *) GET_GUID_HOB_DATA (GuidHob);

  //
  // Init FSP binary FVI Telemetry record
  //
  FspTelemetryVersionVal = (UINT64)(LShiftU64 ((UINT64)(FspInfo->SiliconInitVersionMajor), VERSION_MAJOR_VERSION_SHIFT)
                         | LShiftU64 ((UINT64)(FspInfo->SiliconInitVersionMinor)         , VERSION_MINOR_VERSION_SHIFT)
                         | LShiftU64 ((UINT64)(FspInfo->FspVersionRevision)              , VERSION_REVISION_SHIFT)
                         | LShiftU64 ((UINT64)(UINT16) FspInfo->FspVersionBuild          , VERSION_BUILD_NUM_SHIFT));

  AppendTelemetryFviBlock (gFspBinVersionComponentId, FspTelemetryVersionVal, FVI_INTEL_SIGNATURE);
  return EFI_SUCCESS;
}


/**
  The Entry point of the FSP Firmware Version PEIM

  @param[in]  FileHandle   Pointer to image file handle
  @param[in]  PeiServices  Pointer to PEI Services Table

  @return     EFI_SUCCESS.
**/
EFI_STATUS
EFIAPI
FspFirmwareVersionEntry (
  IN       EFI_PEI_FILE_HANDLE  FileHandle,
  IN CONST EFI_PEI_SERVICES     **PeiServices
  )
{
  EFI_STATUS                       Status;

  DEBUG ((DEBUG_INFO, "Entered FSP Firmware Version Peim entry\n"));

  Status = PublishFspVersion ();

  DEBUG ((DEBUG_INFO, "Exit FSP Firmware Version Peim\n"));

  return Status;
}
