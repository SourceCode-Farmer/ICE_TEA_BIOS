/** @file
  BIOS Guard Firmware Version Information Pei Module

@copyright
  INTEL CONFIDENTIAL
  Copyright 2019 - 2020 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains a 'Sample Driver' and is licensed as such under the terms
  of your license agreement with Intel or your vendor. This file may be modified
  by the user, subject to the additional terms of the license agreement.

@par Specification
**/

#ifndef _BIOS_GUARD_FIRMWARE_VERSION_PEIM_H_
#define _BIOS_GUARD_FIRMWARE_VERSION_PEIM_H_

#include <Uefi.h>
#include <Library/BaseLib.h>
#include <Library/DebugLib.h>
#include <Library/BaseMemoryLib.h>
#include <Library/PeiServicesLib.h>
#include <Library/ConfigBlockLib.h>
#include <Library/MemoryAllocationLib.h>
#include <Library/HobLib.h>
#include <Ppi/SiPolicy.h>
#include <Telemetry/Include/Pi/TelemetryHob.h>
#include <Telemetry/Include/TelemetryCommon.h>
#include <Telemetry/Include/Library/TelemetryFviLib.h>

#define  BIOS_GUARD_STRING       L"BIOS Guard"

/**
  Publish Bios Guard Telemetry Hob

  @param[in]     Address of BiosGuardModule

  @retval        EFI_SUCCESS
  @retval        EFI_OUT_OF_RESOURCES.
**/
EFI_STATUS
PublishBiosGuardTelemetryHob (
  IN EFI_PHYSICAL_ADDRESS  BiosGuardModulePtr
  );

/**
  Publish Bios Guard version via Telemetry

  @param[in]     None

  @retval        EFI_SUCCESS
  @retval        EFI_NOT_FOUND.
  @retval        EFI_OUT_OF_RESOURCES.
**/
EFI_STATUS
PublishBiosGuardVersion ( VOID );

/**
  Callback function to Publish Bios Guard Version when SiPolicyPpi is installed.

  @param[in] PeiServices          General purpose services available to every PEIM.
  @param[in] NotifyDescriptor     The notification structure this PEIM registered on install.
  @param[in] Ppi                  The memory discovered PPI.  Not used.

  @retval EFI_SUCCESS             Succeeds.
**/
EFI_STATUS
EFIAPI
PublishBiosGuardVersionCallback (
  IN EFI_PEI_SERVICES           **PeiServices,
  IN EFI_PEI_NOTIFY_DESCRIPTOR  *NotifyDescriptor,
  IN VOID                       *Ppi
  );

#endif
