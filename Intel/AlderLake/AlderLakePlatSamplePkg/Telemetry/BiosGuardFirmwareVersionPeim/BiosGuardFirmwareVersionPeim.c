/** @file
  BIOS Guard Firmware Version Information Pei Module

@copyright
  INTEL CONFIDENTIAL
  Copyright 2019 - 2020 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains a 'Sample Driver' and is licensed as such under the terms
  of your license agreement with Intel or your vendor. This file may be modified
  by the user, subject to the additional terms of the license agreement.

@par Specification
**/

#include "BiosGuardFirmwareVersionPeim.h"

static EFI_PEI_NOTIFY_DESCRIPTOR  mBiosGuardCallbackNotifyList = {
  (EFI_PEI_PPI_DESCRIPTOR_NOTIFY_CALLBACK | EFI_PEI_PPI_DESCRIPTOR_TERMINATE_LIST),
  &gSiPolicyReadyPpiGuid,
  PublishBiosGuardVersionCallback
};

/**
  Publish Bios Guard Telemetry Hob

  @param[in]     Address of BiosGuardModule

  @retval        EFI_SUCCESS
  @retval        EFI_OUT_OF_RESOURCES.
**/
EFI_STATUS
PublishBiosGuardTelemetryHob (
  IN EFI_PHYSICAL_ADDRESS  BiosGuardModulePtr
  )
{
  EFI_STATUS Status;
  UINT64     BiosGuardTelemetryVersionVal;

  // Init BIOS Guard FVI Telemetry record
  //
  BiosGuardTelemetryVersionVal = (UINT64)(LShiftU64 ((UINT64)*(UINT8 *)(UINTN)(BiosGuardModulePtr + BIOSGUARD_REVISION_ID_MAJOR_OFFSET), VERSION_MAJOR_VERSION_SHIFT)
                               | LShiftU64 ((UINT64)*(UINT8 *)(UINTN)(BiosGuardModulePtr + BIOSGUARD_REVISION_ID_MINOR_OFFSET)         , VERSION_MINOR_VERSION_SHIFT)
                               | LShiftU64 ((UINT64)*(UINT16 *)(UINTN)(BiosGuardModulePtr + BIOSGUARD_BUILD_NUMBER_OFFSET)             , VERSION_BUILD_NUM_SHIFT));

  Status = AppendTelemetryFviBlock (gBiosGuardComponentId, BiosGuardTelemetryVersionVal, FVI_INTEL_SIGNATURE);

  return Status;
}


/**
  Callback function to Publish Bios Guard Version when SiPolicyPpi is installed.

  @param[in] PeiServices          General purpose services available to every PEIM.
  @param[in] NotifyDescriptor     The notification structure this PEIM registered on install.
  @param[in] Ppi                  The memory discovered PPI.  Not used.

  @retval EFI_SUCCESS             Succeeds.
**/
EFI_STATUS
EFIAPI
PublishBiosGuardVersionCallback (
  IN EFI_PEI_SERVICES           **PeiServices,
  IN EFI_PEI_NOTIFY_DESCRIPTOR  *NotifyDescriptor,
  IN VOID                       *Ppi
  )
{
  EFI_STATUS                       Status;

  DEBUG ((DEBUG_INFO, "Entered Bios Guard Firmware Version Publish Callback\n"));

  Status = PublishBiosGuardVersion ();

  DEBUG ((DEBUG_INFO, "Exit Bios Guard Firmware Version Publish Callback\n"));

  return Status;
}

/**
  The Entry point of the Bios Guard Firmware Version PEIM

  @param[in]  FileHandle   Pointer to image file handle
  @param[in]  PeiServices  Pointer to PEI Services Table

  @return     EFI_SUCCESS.
**/
EFI_STATUS
EFIAPI
BiosGuardFirmwareVersionEntry (
  IN       EFI_PEI_FILE_HANDLE  FileHandle,
  IN CONST EFI_PEI_SERVICES     **PeiServices
  )
{
  EFI_STATUS                    Status;

  DEBUG ((DEBUG_INFO, "Bios Guard Firmware Version Publish Callback Notified\n"));

  Status = PeiServicesNotifyPpi (&mBiosGuardCallbackNotifyList);
  ASSERT_EFI_ERROR (Status);

  return EFI_SUCCESS;
}
