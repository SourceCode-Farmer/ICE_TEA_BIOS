/** @file
  This file implements platform firmware version information driver.

@copyright
  INTEL CONFIDENTIAL
  Copyright 2018 - 2020 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains a 'Sample Driver' and is licensed as such under the terms
  of your license agreement with Intel or your vendor. This file may be modified
  by the user, subject to the additional terms of the license agreement.

@par Specification Reference:

**/

#include "PlatformFirmwareVersion.h"

#include <PiDxe.h>
#include <Library/DebugLib.h>
#include <Library/UefiLib.h>
#include <Library/BaseMemoryLib.h>
#include <Library/MemoryAllocationLib.h>
#include <Protocol/AdapterInformation.h>

#include <Library/IoLib.h>
#include <Library/BaseLib.h>
#include <Library/DevicePathLib.h>
#include <Library/UefiBootServicesTableLib.h>
#include <Library/UefiRuntimeServicesTableLib.h>
#include <Guid/EventGroup.h>

#include <Protocol/Smbios.h>
#include <Protocol/LoadedImage.h>
#include <Protocol/FirmwareVolume2.h>

#include <PlatformInfo.h>
#include <Library/PciSegmentLib.h>
#include <IndustryStandard/Pci30.h>
#include <Register/PchRegs.h>
#include <Library/GbeLib.h>


//
// Arrays for Adapter Information
//
GLOBAL_REMOVE_IF_UNREFERENCED TELEMETRY_DATA_RECORD mPlatformTelemetryVersionData[] = {
  { SENSOR_FW_VERSION_COMPONENT_ID,        DEFAULT_FVI_TELEMETRY_VERSION, FVI_INTEL_SIGNATURE, },
  { DEBUG_MODE_STATUS_COMPONENT_ID,        DEFAULT_FVI_TELEMETRY_VERSION, FVI_INTEL_SIGNATURE, },
  { PERFORMANCE_MODE_STATUS_COMPONENT_ID,  DEFAULT_FVI_TELEMETRY_VERSION, FVI_INTEL_SIGNATURE, },
  { DEBUG_USE_USB_COMPONENT_ID,            DEFAULT_FVI_TELEMETRY_VERSION, FVI_INTEL_SIGNATURE, },
  { ICC_OVERLOCKING_VERSION_COMPONENT_ID,  DEFAULT_FVI_TELEMETRY_VERSION, FVI_INTEL_SIGNATURE, },
  { EDKII_STABLE_VERSION_COMPONENT_ID,     DEFAULT_FVI_TELEMETRY_VERSION, FVI_INTEL_SIGNATURE, },
  { PLATFORM_VERSION_COMPONENT_ID,         DEFAULT_FVI_TELEMETRY_VERSION, FVI_INTEL_SIGNATURE, },
  { SILICON_VERSION_COMPONENT_ID,          DEFAULT_FVI_TELEMETRY_VERSION, FVI_INTEL_SIGNATURE, },
};

GLOBAL_REMOVE_IF_UNREFERENCED TELEMETRY_DATA_RECORD mPlatformTelemetryVersion2Data[] = {
  { LAN_PHY_VERSION_COMPONENT_ID,          DEFAULT_FVI_TELEMETRY_VERSION, FVI_INTEL_SIGNATURE, },
  { EC_VERSION_COMPONENT_ID,               DEFAULT_FVI_TELEMETRY_VERSION, FVI_INTEL_SIGNATURE, },
};

/**
  Set FVI EC Version.

  @attention This function may receive untrusted input.
  Type0Record is external input, so this function will validate its data structure within this buffer before use.

  @param[in] Type0Record  A pointer to the SMBIOS_TABLE_TYPE0 instance.
**/
VOID
SetFviEcVersion (
  IN SMBIOS_TABLE_TYPE0   *Type0Record
  )
{
  if (Type0Record != NULL) {
    mPlatformTelemetryVersion2Data[EC_VER].Version = (UINT64)Type0Record->EmbeddedControllerFirmwareMajorRelease << VERSION_MAJOR_VERSION_SHIFT
                                                   | (UINT64)Type0Record->EmbeddedControllerFirmwareMinorRelease << VERSION_MINOR_VERSION_SHIFT;
  }
}

/**
  Get EC Version.
**/
VOID
GetEcVersion (
  VOID
  )
{
  EFI_STATUS              Status;
  EFI_SMBIOS_HANDLE       SmbiosHandle;
  EFI_SMBIOS_PROTOCOL     *mSmbios;
  SMBIOS_TABLE_TYPE0      *Type0Record;
  EFI_SMBIOS_TABLE_HEADER *Record;

  Status = gBS->LocateProtocol (
                  &gEfiSmbiosProtocolGuid,
                  NULL,
                  (VOID **)&mSmbios
                  );
  if (!EFI_ERROR (Status)) {
    SmbiosHandle = SMBIOS_HANDLE_PI_RESERVED;
    do {
      Status = mSmbios->GetNext (mSmbios, &SmbiosHandle, NULL, &Record, NULL);
      if (EFI_ERROR(Status)) {
        break;
      }
      if (Record->Type == EFI_SMBIOS_TYPE_BIOS_INFORMATION) {
        Type0Record = (SMBIOS_TABLE_TYPE0 *) Record;
        SetFviEcVersion (Type0Record);
        break;
      }
    } while (Record->Type != EFI_SMBIOS_TYPE_BIOS_INFORMATION);
  }
}

/**
  Get LanPhy Version.
**/
VOID
GetLanPhyVersion (
  VOID
  )
{
  EFI_STATUS         Status;
  UINT16             LanPhyRev;

  LanPhyRev = 0;

  Status = GbeGetLanPhyRevision (&LanPhyRev);
  if (EFI_ERROR (Status)) {
    DEBUG ((DEBUG_ERROR, "GetLanPhyVersion failed to obtain Phy Revision. Status: %r\n", Status));
    LanPhyRev = 0;
  }

  //
  // LAN PHY Revision
  //
  mPlatformTelemetryVersion2Data[LAN_PHY_VER].Version = LanPhyRev << VERSION_BUILD_NUM_SHIFT;
}


/**
  Get EDKII Version.
**/
VOID
GetEDKIIStableVersion (
  VOID
  )
{
  mPlatformTelemetryVersionData[EDKII_VER].Version = (UINT64)PcdGet8 (PcdEDKIIVersionMajor) << VERSION_MAJOR_VERSION_SHIFT
                                                   | (UINT64)PcdGet8 (PcdEDKIIVersionMinor) << VERSION_MINOR_VERSION_SHIFT
                                                   | (UINT64)PcdGet8 (PcdEDKIIVersionRevision) << VERSION_REVISION_SHIFT
                                                   | (UINT64)PcdGet8 (PcdEDKIIVersionBuild) << VERSION_BUILD_NUM_SHIFT;

}

/**
  Get Platform Version.
**/
VOID
GetPlatformVersion (
  VOID
  )
{
  mPlatformTelemetryVersionData[PLATFORM_VER].Version = (UINT64)PcdGet8 (PcdPlatformVersionMajor) << VERSION_MAJOR_VERSION_SHIFT
                                                      | (UINT64)PcdGet8 (PcdPlatformVersionMinor) << VERSION_MINOR_VERSION_SHIFT
                                                      | (UINT64)PcdGet8 (PcdPlatformVersionRevision) << VERSION_REVISION_SHIFT
                                                      | (UINT64)PcdGet8 (PcdPlatformVersionBuild) << VERSION_BUILD_NUM_SHIFT;
}

/**
  Get Silicon Version.
**/
VOID
GetSiliconVersion (
  VOID
  )
{
  mPlatformTelemetryVersionData[SILICON_VER].Version = (UINT64)PcdGet8 (PcdSiliconInitVersionMajor) << VERSION_MAJOR_VERSION_SHIFT
                                                     | (UINT64)PcdGet8 (PcdSiliconInitVersionMinor) << VERSION_MINOR_VERSION_SHIFT
                                                     | (UINT64)PcdGet8 (PcdSiliconInitVersionRevision) << VERSION_REVISION_SHIFT
                                                     | (UINT64)PcdGet8 (PcdSiliconInitVersionBuild) << VERSION_BUILD_NUM_SHIFT;
}

/**
  Get Platform Firmware Version Information.

  @param[in] Event    The Event this notify function registered to.
  @param[in] Context  Pointer to the context data registered to the Event.
**/
VOID
EFIAPI
InitializeFviDataCallback (
  IN EFI_EVENT Event,
  IN VOID      *Context
  )
{
  EFI_STATUS                       Status;
  VOID                             *ProtocolPointer;
  UINT16                           RrcordCount;
  UINT8                            Index;


  Status = gBS->LocateProtocol (&gBdsAllDriversConnectedProtocolGuid, NULL, (VOID **) &ProtocolPointer);
  if (EFI_ERROR (Status)) {
    DEBUG ((EFI_D_ERROR, "gBdsAllDriversConnectedProtocolGuid - Locate protocol failed = %r.\n", Status));
    return;
  }

  gBS->CloseEvent (Event);

  GetEcVersion ();
  GetLanPhyVersion ();

  RrcordCount = sizeof (mPlatformTelemetryVersion2Data) / sizeof (TELEMETRY_DATA_RECORD);
  for (Index = 0; Index < RrcordCount; Index++) {
    Status = AppendTelemetryFviBlock (mPlatformTelemetryVersion2Data[Index].ComponentId, mPlatformTelemetryVersion2Data[Index].Version,
                                      mPlatformTelemetryVersion2Data[Index].ProducerId);
    if (EFI_ERROR (Status)) {
      DEBUG ((EFI_D_ERROR, "ConnectAll Platform FVI: AppendTelemetryFviBlock returns %r.\n", Status));
      return;
    }
  }

  return;
}


/**
  This is driver entry point to register the notification event.

  @param[in] ImageHandle  A handle for the image that is initializing this driver
  @param[in] SystemTable  A pointer to the EFI system table

  @retval    EFI_SUCCESS  The initialization finished successfully.
**/
EFI_STATUS
EFIAPI
PlatformFirmwareVersionDxe (
  IN EFI_HANDLE        ImageHandle,
  IN EFI_SYSTEM_TABLE  *SystemTable
  )
{
  VOID       *Registration;
  EFI_STATUS Status;
  UINT16      RrcordCount;
  UINT8       Index;

  Index   = 0;
  Status  = EFI_SUCCESS;

  // For Version of EC, Lan Phy, please see function InitializeFviDataCallback()
  GetEDKIIStableVersion ();
  GetPlatformVersion ();
  GetSiliconVersion ();

  RrcordCount = sizeof (mPlatformTelemetryVersionData) / sizeof (TELEMETRY_DATA_RECORD);
  for (Index = 0; Index < RrcordCount; Index++) {
    Status = AppendTelemetryFviBlock (mPlatformTelemetryVersionData[Index].ComponentId, mPlatformTelemetryVersionData[Index].Version,
                                      mPlatformTelemetryVersionData[Index].ProducerId);
    if (EFI_ERROR (Status)) {
      DEBUG ((EFI_D_ERROR, "Platform FVI: AppendTelemetryFviBlock returns %r.\n", Status));
      return Status;
    }
  }

  //
  // Register InitializeFviDataCallback
  //
  EfiCreateProtocolNotifyEvent (
    &gBdsAllDriversConnectedProtocolGuid,
    TPL_CALLBACK,
    InitializeFviDataCallback,
    NULL,
    &Registration
    );

  return EFI_SUCCESS;
}
