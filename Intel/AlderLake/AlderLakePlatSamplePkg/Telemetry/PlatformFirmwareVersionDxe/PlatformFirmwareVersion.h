/** @file
  Private data for platform firmware version information for telemetry

@copyright
  INTEL CONFIDENTIAL
  Copyright 2018 - 2020 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains a 'Sample Driver' and is licensed as such under the terms
  of your license agreement with Intel or your vendor. This file may be modified
  by the user, subject to the additional terms of the license agreement.

@par Specification Reference:
**/

#ifndef _PLATFORM_FIRMWARE_VERSION_H_
#define _PLATFORM_FIRMWARE_VERSION_H_

#include <Uefi.h>
#include <Telemetry/Include/Guid/ComponentID.h>
#include <Telemetry/Include/TelemetryCommon.h>
#include <Telemetry/Include/Library/TelemetryFviLib.h>
#include <Telemetry/Include/IndustryStandard/PlatformHealthAssesmentTable.h>

enum {
  SENSOR_FW_VER = 0,
  DEBUG_MODE_VER,
  PERFORMANCE_MODE_VER,
  DEBUB_USE_USB_VER,
  ICC_OVERLOCKING_VER,
  EDKII_VER,
  PLATFORM_VER,
  SILICON_VER,
} PLATFORM_VERSION_INDEX;

//
// Create callback event to get following special firmware version infomation.
//
enum {
  LAN_PHY_VER = 0,
  EC_VER,
} PLATFORM_VERSION2_INDEX;

#define SENSOR_HUB_FW_VERSION_OFFSET 0x1000
#define SENSOR_HUB_FW_VERSION_LENGTH 0x04

//
// LAN PHY Revision definitions
//
#define LAN_PHY_REV_TC 0xA0
#define LAN_PHY_REV_A0 0xA1
#define LAN_PHY_REV_A1 0xA2
#define LAN_PHY_REV_A2 0xA3
#define LAN_PHY_REV_A3 0xA4
#define LAN_PHY_REV_B1 0xA5

#endif
