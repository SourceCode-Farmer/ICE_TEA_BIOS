/** @file
  Intel Platform Telememtry Data Table (PHAT) related definitions.

  Copyright (c) 2019 - 2020, Intel Corporation. All rights reserved.<BR>
  This program and the accompanying materials
  are licensed and made available under the terms and conditions of the BSD License
  which accompanies this distribution.  The full text of the license may be found at
  http://opensource.org/licenses/bsd-license.php

  THE PROGRAM IS DISTRIBUTED UNDER THE BSD LICENSE ON AN "AS IS" BASIS,
  WITHOUT WARRANTIES OR REPRESENTATIONS OF ANY KIND, EITHER EXPRESS OR IMPLIED.

**/

#ifndef __PLATFORM_HEALTH_ASSESSMENT_TABLE_H__
#define __PLATFORM_HEALTH_ASSESSMENT_TABLE_H__


#pragma pack(1)

///
/// "PHAT" Platform Health Assessment Table
///
#define EFI_ACPI_PLATFORM_HEALTH_ASSESSMENT_TABLE_SIGNATURE   SIGNATURE_32('P', 'H', 'A', 'T')
#define DEFAULT_FVI_TELEMETRY_PARENTVERSION       0X000000FFFFFFFFFF
#define DEFAULT_FVI_TELEMETRY_PARENTCOMPONENTID   { 0x00000000, 0x0000, 0x0000, { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 } }
#define DEFAULT_FVI_TELEMETRY_ATTRIBUTES          0X00000000

#define EFI_ACPI_PLATFORM_HEALTH_ASSESSMENT_TABLE_VERSION  0x1

typedef struct {
  UINT16            RecordType;   // Firmware Health Data Record.
  UINT16            RecordLength; // Varies - This value depicts the length of the
                                  // health data record, in bytes.
  UINT8             Revision;     // Revision of this Firmware Version Data Record, the value is fixed to 1.
} EFI_ACPI_PLATFORM_TELEMETRY_RECORD_HEADER;

typedef struct {
  EFI_ACPI_PLATFORM_TELEMETRY_RECORD_HEADER Header;                   // Common header.
  UINT16                                    Reserved;
  UINT8                                     AmHealthy;                // is the device healthy?
                                                                      // 0 = Errors found.
                                                                      // 1 = No errors found.
                                                                      // 2 = Unknown (treated as healthy).
                                                                      // 3 = Advisory - additional device-specific data exposed.
  EFI_GUID                                  DeviceSignature;          // The unique GUID associated with this device.
  UINT32                                    DeviceSpecificDataOffset; // Offset to the Device-specific Data from the start of this Data Record.
                                                                      // If 0, then there is no device-specific data.
} EFI_ACPI_TELEMETRY_HEALTH_DATA_RECORD_STRUCTURE;

typedef struct {
  EFI_TIME               LastUpdateTime;
  EFI_GUID               EsrtId;
} EFI_ACPI_LAST_UPDATE_DATA;

//
// SAS Gen2, chapter 2.5.4  Motherboard firmware health record
//
typedef struct {
  UINT8                                            Revision;                 // Motherboard Revision, 1 - Revision of this device-specific record
  UINT64                                           MeanBootTime;             // Average time in nanoseconds elapsed just prior to loading the OS boot loader.
  UINT64                                           CurrentBootTime;          // Time in nanoseconds elapsed just prior to loading the OS boot loader.
  UINT16                                           NumberOfBootsSinceUpdate; // Number of completed (reached READY_TO_BOOT) boots since the last successful firmware update.
} EFI_ACPI_MOTHERBOARD_HEALTH_DATA;

typedef struct {
  EFI_GUID  ComponentId;  // Unique component ID
  UINT64    Version;      // 64-bit version value
  UINT32    ProducerId;   // The ACPI Vendor ID(e.g.'ABCD')
                          // FFFF - no ID defined
                          // 0000 - invalid value
} TELEMETRY_DATA_RECORD;

#pragma pack()

#endif
