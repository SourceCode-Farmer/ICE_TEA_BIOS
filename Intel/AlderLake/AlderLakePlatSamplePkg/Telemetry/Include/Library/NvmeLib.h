/** @file

  Copyright (c) 2019 - 2020, Intel Corporation. All rights reserved.<BR>
  This program and the accompanying materials
  are licensed and made available under the terms and conditions of the BSD License
  which accompanies this distribution. The full text of the license may be found at
  http://opensource.org/licenses/bsd-license.php

  THE PROGRAM IS DISTRIBUTED UNDER THE BSD LICENSE ON AN "AS IS" BASIS,
  WITHOUT WARRANTIES OR REPRESENTATIONS OF ANY KIND, EITHER EXPRESS OR IMPLIED.
**/
#ifndef _NVME_LIB_H_
#define _NVME_LIB_H_

#include <Base.h>

/**
  Implement 'Get Log Page' 02h(SMART / Health Information) Command.

  @param[in]   NvmeDevice   The pointer to the NVME_PASS_THRU_DEVICE data structure.
  @param[out]  NamespaceId  NamespaceId for an NVM Express namespace present on the
                            NVM Express controller
  @param[out]  Buffer       The buffer used to store the log data.

  @return EFI_SUCCESS           Successfully get the identify controller data.
  @return EFI_DEVICE_ERROR      Fail to get the identify controller data.
  @return EFI_OUT_OF_RESOURCES  Unable to allocate required resources.
**/

EFI_STATUS
NvmeGetLogPageSmartInfo (
  IN  EFI_NVM_EXPRESS_PASS_THRU_PROTOCOL    *NvmeDevice,
  IN  UINT32                                NamespaceId,
  OUT VOID                                  *Buffer
  );

#endif
