/** @file
 * //TODO: modify this
  Telemetry API Lib for Firmware Information Version.

  Copyright (c) 2018 - 2020, Intel Corporation. All rights reserved.<BR>
  This program and the accompanying materials
  are licensed and made available under the terms and conditions of the BSD License
  which accompanies this distribution. The full text of the license may be found at
  http://opensource.org/licenses/bsd-license.php

  THE PROGRAM IS DISTRIBUTED UNDER THE BSD LICENSE ON AN "AS IS" BASIS,
  WITHOUT WARRANTIES OR REPRESENTATIONS OF ANY KIND, EITHER EXPRESS OR IMPLIED.
**/
#ifndef _TELEMETRY_LIB_H_
#define _TELEMETRY_LIB_H_

/**
  Append an entry after FVI AIP table.

  @param[in] ComponentId      ComponentId for the FVI entry.
  @param[in] Version          Version for FVI entry.
  @param[in] ProducerId       ProducerId for FVI entry, e.g. 'INTC'.

  @retval EFI_SUCCESS           - if the data is successfully reported.
  @retval EFI_OUT_OF_RESOURCES  - if not able to get resources.
  @retval EFI_INVALID_PARAMETER - if Data == NULL.
**/
EFI_STATUS
EFIAPI
AppendTelemetryFviBlock (
  IN     EFI_GUID                            ComponentId,
  IN     UINT64                              Version,
  IN     UINT32                              ProducerId
  );

#endif
