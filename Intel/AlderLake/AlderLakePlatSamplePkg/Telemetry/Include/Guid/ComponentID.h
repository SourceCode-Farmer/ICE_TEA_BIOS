/** @file
  Component ID of firmware version information.

  Copyright (c) 2018 - 2020, Intel Corporation. All rights reserved.<BR>
  This program and the accompanying materials
  are licensed and made available under the terms and conditions of the BSD License
  which accompanies this distribution. The full text of the license may be found at
  http://opensource.org/licenses/bsd-license.php

  THE PROGRAM IS DISTRIBUTED UNDER THE BSD LICENSE ON AN "AS IS" BASIS,
  WITHOUT WARRANTIES OR REPRESENTATIONS OF ANY KIND, EITHER EXPRESS OR IMPLIED.
**/
#ifndef _COMPONENTID_H_
#define _COMPONENTID_H_

#define BIOS_GUARD_COMPONENT_ID \
  { \
    0xdc4f0355, 0x5297, 0x4edb, {0xb4, 0xd7, 0x9b, 0x83, 0x61, 0x51, 0x36, 0xe4 } \
  }

#define FSP_BIN_VERSION_COMPONENT_ID \
  { \
    0x27097cfd, 0x46e5, 0x4e39, {0xb8, 0xe4, 0x33, 0xe4, 0x39, 0xa1, 0x3e, 0xaf } \
  }

#define LAN_PHY_VERSION_COMPONENT_ID \
  { \
    0x988c3fd6, 0x92b2, 0x41a0, {0xb5, 0xc3, 0x78, 0xa5, 0xcd, 0x47, 0x12, 0xf7 } \
  }

#define SENSOR_FW_VERSION_COMPONENT_ID \
  { \
    0xc519a3eb, 0x6d2a, 0x47d0, {0xaa, 0xd3, 0x5e, 0xb0, 0x06, 0xb6, 0x31, 0x21 } \
  }

#define DEBUG_MODE_STATUS_COMPONENT_ID \
  { \
    0xe1ec257d, 0x43d4, 0x415b, {0x95, 0x03, 0x9e, 0xc0, 0x4a, 0xc5, 0x61, 0x58 } \
  }

#define PERFORMANCE_MODE_STATUS_COMPONENT_ID \
  { \
    0xe323121f, 0xe60c, 0x43d8, {0x8e, 0x0f, 0x69, 0xd9, 0xd7, 0xda, 0xb3, 0xa0 } \
  }

#define DEBUG_USE_USB_COMPONENT_ID \
  { \
    0xab0cdeef, 0x0b27, 0x4c2b, {0xb6, 0xb7, 0x9b, 0x73, 0x40, 0x43, 0xe3, 0xde } \
  }

#define ICC_OVERLOCKING_VERSION_COMPONENT_ID \
  { \
    0xbb11c4ea, 0x6928, 0x4f6c, {0xb3, 0x48, 0x72, 0xc0, 0xcf, 0xc9, 0xd0, 0x4d } \
  }

#define UNDI_VERSION_COMPONENT_ID \
  { \
    0x103a9b25, 0xa4d2, 0x4889, {0x9e, 0xcc, 0x90, 0x8c, 0xfc, 0x31, 0x39, 0x39 } \
  }

#define EC_VERSION_COMPONENT_ID \
  { \
    0x03285589, 0x1e37, 0x4b72, {0xa5, 0xf9, 0x70, 0xd1, 0xad, 0xae, 0x5d, 0x34 } \
  }

#define GOP_VERSION_COMPONENT_ID \
  { \
    0xe823976e, 0x63ba, 0x4114, {0xb6, 0x4b, 0x81, 0x12, 0x72, 0x8d, 0x4e, 0x03 } \
  }

#define ROYALPARK_VERSION_COMPONENT_ID \
  { \
    0xdd9af3ff, 0xe26e, 0x4191, {0xbe, 0x2c, 0x4b, 0x9f, 0x43, 0xed, 0xc9, 0xd9 } \
  }

#define EDKII_STABLE_VERSION_COMPONENT_ID \
  { \
    0x3381c8e3, 0xb92c, 0x4bac, {0xb6, 0xc6, 0x43, 0x90, 0x91, 0x1e, 0x93, 0x4d } \
  }

#define PLATFORM_VERSION_COMPONENT_ID \
  { \
    0x427dcdb4, 0x1c33, 0x4f2b, {0xb7, 0x36, 0xf8, 0xda, 0xbe, 0x9e, 0x9a, 0xcd } \
  }

#define SILICON_VERSION_COMPONENT_ID \
  { \
    0xccb0bc86, 0x1bcd, 0x476f, {0xaa, 0xbe, 0xe1, 0x91, 0x59, 0x24, 0x4b, 0xfd } \
  }

#define PCH_COMPONENT_ID \
  { \
    0x64c1a63e, 0xba2d, 0x41de, {0x96, 0x55, 0x2f, 0x70, 0xfe, 0x61, 0x8f, 0x00 } \
  }

#define PCH_CRID_STATUS_COMPONENT_ID \
  { \
    0x2199cbed, 0x4d3e, 0x45ef, {0x85, 0xd1, 0xe1, 0xba, 0xd5, 0xa3, 0x70, 0xa9 } \
  }

#define PCH_CRID_ORIGINAL_COMPONENT_ID \
  { \
    0xa83f7361, 0xfefa, 0x42d6, {0x8b, 0x32, 0x95, 0xf2, 0x98, 0x9b, 0xf6, 0xd4 } \
  }

#define PCH_CRID_NEW_COMPONENT_ID \
  { \
    0xd84ca716, 0x7ed3, 0x4c4b, {0xb1, 0xd5, 0x2b, 0x8c, 0x27, 0x4a, 0x19, 0xf1 } \
  }

#define PCH_RAID_VERSION_COMPONENT_ID \
  { \
    0xe9b095bf, 0xdee2, 0x4ac9, {0x87, 0x78, 0x9e, 0x41, 0xfb, 0x86, 0xc4, 0x36 } \
  }

#define HSIO_LP_AX_COMPONENT_ID \
  { \
    0xe385dd67, 0x8f03, 0x4872, {0xab, 0x47, 0x3c, 0xa1, 0x14, 0xbe, 0x1e, 0x52 } \
  }
#define SA_RC_VERSION_COMPONENT_ID \
  { \
    0x38d8af9d, 0x0f3d, 0x4e48, {0xa3, 0x6b, 0xfa, 0xfd, 0x68, 0x61, 0x48, 0xa0 } \
  }

#define SA_MEM_RC_VERSION_COMPONENT_ID \
  { \
    0x73256ee6, 0xa990, 0x4b13, {0x8a, 0xba, 0x76, 0xe2, 0x2c, 0x3e, 0x99, 0x3c } \
  }

#define PCIE_VERSION_COMPONENT_ID \
  { \
    0xfa2b5b12, 0x3124, 0x43de, {0x84, 0xe6, 0xc4, 0x11, 0x4e, 0x88, 0x1f, 0x43 } \
  }

#define SA_CRID_STATUS_COMPONENT_ID \
  { \
    0x47a463e7, 0x196d, 0x4577, {0xb5, 0x36, 0x3b, 0x9a, 0x85, 0xb7, 0x38, 0x4b } \
  }

#define SA_CRID_ORIGINAL_COMPONENT_ID \
  { \
    0x3d3ce021, 0xce65, 0xa2f5, {0xa2, 0xf5, 0x00, 0x8b, 0x2b, 0xbb, 0x0c, 0xa2 } \
  }

#define SA_CRID_NEW_COMPONENT_ID \
  { \
    0x84a3ff3c, 0xce65, 0x448d, {0x81, 0xc4, 0x1a, 0x62, 0xa8, 0xa9, 0xc3, 0x6e } \
  }

#define SA_VBIOS_VERSION_COMPONENT_ID \
  { \
    0xfa50153e, 0x6627, 0x4714, {0x92, 0x20, 0xb8, 0xc7, 0x1d, 0xab, 0x42, 0x9a } \
  }
#define SA_IOM_COMPONENT_ID \
{ \
  0x0169518d, 0x0480, 0x5676, {0xa9, 0xc1, 0x5a, 0x89, 0x03, 0xc4, 0x99, 0x2b } \
}

#define SA_PHY_VERSION_COMPONENT_ID \
{ \
  0x0094dfcf, 0xd97b, 0x51a2, {0x8f, 0x85, 0xec, 0x24, 0x82, 0xbd, 0x62, 0x96 } \
}

#define SA_TBT_VERSION_COMPONENT_ID \
{ \
  0xd1add6fd, 0xb056, 0x5486, {0xad, 0x96, 0x51, 0x63, 0xd1, 0xb9, 0xcc, 0xdc } \
}

#define SA_SAM_FW_VERSION_COMPONENT_ID \
{ \
  0x8f90aa30, 0xb959, 0x57db, {0x98, 0xed, 0xae, 0xda, 0x14, 0xbb, 0x4f, 0x7f } \
}

#define CPU_RC_VERSION_COMPONENT_ID \
  { \
    0x877778b9, 0xcf22, 0x476a, {0x97, 0xa1, 0x27, 0x53, 0x0d, 0x9a, 0xfe, 0x42 } \
  }

#define CPU_UCODE_VERSION_COMPONENT_ID \
  { \
    0xda72fef3, 0x782b, 0x4c38, {0x85, 0x40, 0x2c, 0x90, 0x21, 0x7c, 0x16, 0x73 } \
  }

#define CPU_TXT_ACM_VERSION_COMPONENT_ID \
  { \
    0xec643dac, 0xabb9, 0x465f, {0x83, 0xa6, 0xa8, 0x57, 0xe1, 0xd0, 0x3b, 0xa2 } \
  }

#define ME_RC_VERSION_COMPONENT_ID \
  { \
    0x6858c460, 0x15ba, 0x4eab, {0xb6, 0x7c, 0x00, 0x53, 0xff, 0xcd, 0xed, 0x54 } \
  }

#define ME_BX_VERSION_COMPONENT_ID \
  { \
    0xd39a2d5d, 0x171e, 0x4da1, {0xb8, 0x4d, 0xf0, 0xfc, 0xde, 0x7c, 0xc4, 0x31 } \
  }

#define ME_FIRMWARE_VERSION_COMPONENT_ID \
  { \
    0xa62ba25d, 0xfffc, 0x4ac6, {0xa9, 0x0e, 0x24, 0x57, 0xac, 0x0e, 0x47, 0x7e } \
  }


extern EFI_GUID gBiosGuardComponentId;
extern EFI_GUID gFspBinVersionComponentId;
extern EFI_GUID gLanPhyVersionComponentId;
extern EFI_GUID gSensorFwVersionComponentId;
extern EFI_GUID gDebugModeStatusComponentId;
extern EFI_GUID gPerformanceModeStatusComponentId;
extern EFI_GUID gDebugUseUsbComponentId;
extern EFI_GUID gIccOverlockingVersionComponentId;
extern EFI_GUID gUndiVersionComponentId;
extern EFI_GUID gEcVersionComponentId;
extern EFI_GUID gGopVersionComponentId;
extern EFI_GUID gRoyalparkVersionComponentId;
extern EFI_GUID gPlatformVersionComponentId;
extern EFI_GUID gClientSiliconVersionComponentId;

#endif
