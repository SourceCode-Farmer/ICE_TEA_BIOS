/** @file
  EFI Telemetry Hob Headers [SAS_v03f].
  This file may be moved in future versions

  Copyright (c) 2018 - 2020, Intel Corporation. All rights reserved.<BR>
  This program and the accompanying materials
  are licensed and made available under the terms and conditions of the BSD License
  which accompanies this distribution. The full text of the license may be found at
  http://opensource.org/licenses/bsd-license.php

  THE PROGRAM IS DISTRIBUTED UNDER THE BSD LICENSE ON AN "AS IS" BASIS,
  WITHOUT WARRANTIES OR REPRESENTATIONS OF ANY KIND, EITHER EXPRESS OR IMPLIED.

**/

#ifndef __TELEMETRY_HOB_H__
#define __TELEMETRY_HOB_H__

#include <Pi/PiHob.h>
#include <Telemetry/Include/IndustryStandard/PlatformHealthAssesmentTable.h>

#define FVI_INTEL_SIGNATURE  SIGNATURE_32 ('I', 'N', 'T', 'C')

#define EFI_TELEMETRY_VERSION_HOB_GUID \
  { \
    0x02700400, 0x7d50, 0x493b, {0x86, 0x37, 0xb6, 0x80, 0x1e, 0xee, 0xe3, 0x05 } \
  }

#define EFI_TELEMETRY_VERSION_TEMP_HOB_GUID \
  { \
    0xcb2fc981, 0x5b86, 0x4a93, {0x87, 0xe3, 0x59, 0x0d, 0xfc, 0x5e, 0x3e, 0x49 } \
  }

#pragma pack(1)
typedef struct {
  EFI_HOB_GENERIC_HEADER  Header; // length of Header + health state
                                  // 30 + device health record size
  EFI_GUID        Signature;      // The Health Record GUID
                                  // EFI_TELEMETRY_HEALTH_RECORD_GUID
  UINT16          RecordType;     // The record type that follows
  BOOLEAN         Healthy;        // is the device healthy?
  UINT32          CID;            // _CID Class Code
  UINT32          UID;            // _UID Unique ID
  UINT32          HID;            // _HID PnP hardware ID
} EFI_HOB_HEALTH_HEADER;

///
/// Firmware Version Information HOB Structure
///
typedef struct {
  EFI_HOB_GENERIC_HEADER Header;      // length of Header + health state
                                      // 60 + length of DevicePath
                                      // + device version record size
  UINT8                 Revision;     // 0x01 - Revision of this struct
  EFI_GUID              Signature;    // The Version Record GUID
                                      // EFI_TELEMETRY_VERSION_RECORD2_GUID
  UINT16                RecordCount;  // The number of records that
                                      // follow.
  TELEMETRY_DATA_RECORD Records[];    // Records[RecordCount]
} TELEMETRY_FIRMWARE_VERSION_INFO2_HOB;

#pragma pack()

extern EFI_GUID gTelemetryVersionHobGuid;
extern EFI_GUID gTelemetryVersionTempHobGuid;

#endif


