/** @file
  Telemetry Lib Common Header.

  Copyright (c) 2018 - 2020, Intel Corporation. All rights reserved.<BR>
  This program and the accompanying materials
  are licensed and made available under the terms and conditions of the BSD License
  which accompanies this distribution. The full text of the license may be found at
  http://opensource.org/licenses/bsd-license.php

  THE PROGRAM IS DISTRIBUTED UNDER THE BSD LICENSE ON AN "AS IS" BASIS,
  WITHOUT WARRANTIES OR REPRESENTATIONS OF ANY KIND, EITHER EXPRESS OR IMPLIED.
**/
#ifndef _TELEMETRY_COMMON_H_
#define _TELEMETRY_COMMON_H_

#include <Protocol/AdapterInformation.h>
#include <Telemetry/Include/Protocol/TelemetryAdapterInformation.h>

#define MOTHERBOARD_HEALTH_DATA_NAME      L"MotherBoardHealth"
#define VARNAME_LASTUPDATETIME            L"LastUpdateTime"

///
/// Variable defined for PHAT implementation.
/// This Variable is produced by PHAT DXE module.
///
typedef struct {
  UINT64                 MeanBootTime;               // Average time in nanoseconds elapsed just prior to loading the OS boot loader.
  UINT16                 NumberOfBootsSinceUpdate;   // Counter since last firmware update.
} MOTHER_BOARD_HEALTH_VARIABLE;

//
// For firmware version shift
//
#define VERSION_MAJOR_VERSION_SHIFT 32
#define VERSION_MINOR_VERSION_SHIFT 24
#define VERSION_REVISION_SHIFT      16
#define VERSION_BUILD_NUM_SHIFT     0

///
/// This value depicts the format and contents
/// of the platform telemetry record.
///
typedef enum {
  FirmwareVersionDataRecord = 0,
  FirmwareHealthDataRecord  = 1
} FIRMWARE_TELEMETRY_RECORD_TYPE;

///
/// This value depicts the format and contents
/// of the platform telemetry record.
///
typedef enum {
  TelemetryUnhealthy = 0,
  TelemetryHealthy,
  TelemetryUnknown,
  TelemetryAdvisory,
} TELEMETRY_HEALTH_DRIVER_STATUS;

///
/// structure Revision (as defined in Telemetry Gen 2.0 spec.)
///
#define TELEMETRY_HOB_STRUCTURE_REVISION   0x01
#define TELEMETRY_ACPI_HEADER_REVISION     0x01
#define TELEMETRY_ACPI_STRUCTURE_REVISION  0x01
#define TELEMETRY_FVI_DATA_RECORD_REVISION 0x01

//
// For firmware version binary
//
#define EFI_ACPI_OEM_ID           'M','S','F','T',' ',' '
#define EFI_ACPI_OEM_TABLE_ID     SIGNATURE_64('M','F','S','T','E','D','K','2') // OEM table id 8 bytes long
#define EFI_ACPI_OEM_REVISION     0x00000005
#define EFI_ACPI_CREATOR_ID       SIGNATURE_32('M','S','F','T')
#define EFI_ACPI_CREATOR_REVISION 0x0100000D

#endif
