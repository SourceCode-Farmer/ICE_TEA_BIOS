/** @file
  EFI Adapter Information Protocol definition for Telemetry feature enabling.
  The EFI Adapter Information Protocol is used to dynamically and quickly discover
  or set device information for an adapter.

  Copyright (c) 2019 - 2020, Intel Corporation. All rights reserved.<BR>
  This program and the accompanying materials
  are licensed and made available under the terms and conditions of the BSD License
  which accompanies this distribution. The full text of the license may be found at
  http://opensource.org/licenses/bsd-license.php

  THE PROGRAM IS DISTRIBUTED UNDER THE BSD LICENSE ON AN "AS IS" BASIS,
  WITHOUT WARRANTIES OR REPRESENTATIONS OF ANY KIND, EITHER EXPRESS OR IMPLIED.

  @par Revision Reference:
  This Protocol is introduced in UEFI Specification 2.4

**/

#ifndef __EFI_TELEMETRY_ADAPTER_INFORMATION_PROTOCOL_H__
#define __EFI_TELEMETRY_ADAPTER_INFORMATION_PROTOCOL_H__

#include <Telemetry/Include/IndustryStandard/PlatformHealthAssesmentTable.h>

#define EFI_ADAPTER_INFO_HEALTH_RECORD_GUID \
  { \
    0x60B653B1, 0xC62F, 0x4C8B, { 0xA5, 0xF6, 0x30, 0xEC, 0xB5, 0x31, 0xDF, 0xB9 } \
  }

#define EFI_ADAPTER_INFO_EC_GUID \
  { \
    0xCD5FC1C5, 0xB30D, 0x4AFD, { 0x89, 0x28, 0xA3, 0x0A, 0x3A, 0xC8, 0x27, 0x80 } \
  }

#define EFI_ADAPTER_INFO_MOTHERBOARD_HEALTH_GUID \
  { \
    0x93A41C2F, 0xA09F, 0xE7C2, { 0xAC, 0x1F, 0xF2, 0x48, 0x8F, 0x03, 0xEE, 0xC3 } \
  }

#define EFI_ADAPTER_INFO_CSME_GUID \
  { \
    0xA30DFF09, 0x56BF, 0x4622, { 0xA9, 0xE7, 0x39, 0x9B, 0xA,  0x79, 0xE7, 0xC7 } \
  }

#define EFI_ADAPTER_INFO_MRC_GUID \
  { \
    0x46B29808, 0x6F0, 0x4CA0, { 0x9F, 0x92, 0x5C, 0x49, 0x67, 0x82, 0x92, 0x78 } \
  }

#define EFI_ADAPTER_INFO_TELEMETRY_VERSION_RECORD_GUID \
  { \
    0x13A83EC2, 0x26CF, 0x8F3E, { 0xEE, 0xA0, 0x36, 0x6E, 0xE2, 0x19, 0x2F, 0x1B} \
  }

#define EFI_TELEMETRY_VERSION_RECORD2_GUID \
  { \
    0x3FA018A3, 0xCDAE, 0xA0F9, { 0xA0, 0x1F, 0xE5, 0xAA, 0x33, 0x1B, 0xBF, 0xC3} \
  }

#define DEFAULT_FVI_TELEMETRY_VERSION       0X000000FFFFFFFFFF
#define FVI_INTEL_SIGNATURE                 SIGNATURE_32 ('I', 'N', 'T', 'C')


#pragma pack(1)
typedef struct {
  UINT16           Length;      // length of Header + attached health state record
                                // 30 + device health record size
  UINT8            Revision;    // 0x01-Revision of this struct
  UINT8            Healthy;     // is the device healthy?
                                // 0 = healthy, 1 = not healthy
                                // 2 = unknown health (treated as healthy)
  EFI_GUID         Signature;   // The GUIDed record type that follows
  EFI_DEVICE_PATH  *DevicePath; // Device Path of the component
} EFI_AIP_HEALTH_HEADER;


typedef struct {
  EFI_AIP_HEALTH_HEADER  Header;
  UINT8                  HealthEntry[1];
} EFI_AIP_HEALTH_RECORD;


typedef struct {
  UINT16                HealthEntryCount;
  EFI_AIP_HEALTH_RECORD HealthRecord[1];
} EFI_PLATFORM_TELEMETRY_DATA;


typedef struct {
  EFI_AIP_HEALTH_HEADER  Header;
  UINT8                  PowerOnSequenceErrorCode;
  UINT8                  LastHostCommand;
  UINT8                  SystemPowerSource;
  UINT16                 BatteryAgingIndicator;
  UINT8                  TempAndThermalErrorFlags;
  UINT8                  VoltageRailOutofSpec;
  UINT8                  CATErrorState;
} EFI_AIP_EC_HEALTH_STATE;

typedef struct {
  EFI_TIME               LastUpdateTime;
  EFI_GUID               EsrtId;
} EFI_LAST_UPDATE_DATA;

typedef struct {
  EFI_AIP_HEALTH_HEADER  Header;
  UINT8                  Revision;
  UINT64                 MeanBootTime;
  UINT64                 CurrentBootTime;
  UINT16                 NumberOfBootsSinceUpdate;
  EFI_LAST_UPDATE_DATA   LastUpdateTime[1];
} EFI_AIP_MOTHERBOARD_HEALTH_STATE;

typedef struct {
  UINT64                        Revision;    // 1 - Revision of the record
  UINT64                        RecordCount; // Number of Record entries (1-n)
  TELEMETRY_DATA_RECORD         Record[0];   // Array of PHAT Version Elements.
                                             // First entry is the original producer of
                                             // the component,and if there's a subsequent
                                             // entry,that means a second agent modified
                                             // the original component in some way,
                                             // whichever the last entry is, that's the
                                             // currently running instance of the component.
                                             // This allows for IHV/IBV/OEM/others
                                             // to establish a chain of data records
                                             // associated with a given component.
} EFI_AIP_TELEMETRY_VERSION_RECORD;

typedef struct {
  UINT16                PlatformRecordType;  // Firmware Version Data Record.
  UINT16                RecordLength;        // 12+28*RecordCount - This value depicts.
                                             // The length of the version data record, in bytes.
  UINT8                 Revision;            // Revision of this Firmware Version Data Record, the value is fixed to 1.
  UINT8                 Reserved[3];
  UINT32                RecordCount;         // PHAT Version Element Count.
  TELEMETRY_DATA_RECORD Records[];           // Array of PHAT Version Elements.
                                             // First entry is the original producer of
                                             // the component,and if there's a subsequent
                                             // entry,that means a second agent modified
                                             // the original component in some way,
                                             // whichever the last entry is, that's the
                                             // currently running instance of the component.
                                             // This allows for IHV/IBV/OEM/others
                                             // to establish a chain of data records
                                             // associated with a given component.
} EFI_COMPONENT_VERSION_DATA;

typedef struct {
  EFI_AIP_HEALTH_HEADER  Header;
  UINT8                  Revision;
  UINT8                  FwInitComplete;
  UINT8                  CurrentState;
  UINT8                  MfsFailure;
  UINT8                  MeOperationMode;
  UINT8                  ErrorCode;
  UINT8                  ManufacturingMode;
  UINT8                  FpfSocConfigLock;
  UINT8                  FwUpdateInprogress;
} EFI_AIP_CSME_HEALTH_STATE;

typedef enum {
  Reserved,
  ErrorInfo,
  SMARTHealth,
  SlotInfo,
  ChangedNamespaceList,
  CommandsSupported,
  SelfTest,
  TelemetryHost,
  TelemetryController
} TELEMETRY_NVME_LOG_ID;

typedef struct {
  TELEMETRY_NVME_LOG_ID  LogIdentifier; // Type 2 log
  UINT64                 NameSpaceId;   // 0 or xFFFFFFFF is for
                                        // controller, any other
                                        // value is a namespace.
  UINT32                 DataLength;
  UINT8                  LogData[1];    // NVME Spec 1.3 Log Data
} NVME_LOG_DATA;

typedef struct {
  EFI_AIP_HEALTH_HEADER  Header;
  UINT8                  Revision;      // 0x01 - Revision of this structure
  UINT32                 LogCount;
  NVME_LOG_DATA          LogData[1];    // NVME Spec 1.3 Log Data
} EFI_AIP_NVME_HEALTH_STATE;

#pragma pack()

extern EFI_GUID gEfiAdapterInfoMediaTypeGuid;

extern EFI_GUID gEfiAdapterInfoHealthRecordGuid;

extern EFI_GUID gEfiAdapterInfoEcGuid;

extern EFI_GUID gAdapterInfoMotherBoardHealthGuid;

extern EFI_GUID gAdapterInfoNvmeHealthGuid;

extern EFI_GUID gAdapterInfoTelemetryVersionRecordGuid;

extern EFI_GUID gTelemetryVersionRecord2Guid;

extern EFI_GUID gAdapterInfoTelemetryPchVersionRecordGuid;

extern EFI_GUID gAdapterInfoTelemetrySaVersionRecordGuid;

#endif
