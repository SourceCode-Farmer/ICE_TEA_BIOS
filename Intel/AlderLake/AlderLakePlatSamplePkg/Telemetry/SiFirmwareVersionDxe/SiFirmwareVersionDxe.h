/** @file
  Header file for Reference code Firmware Version Telemetry Info implementation.

 @copyright
  INTEL CONFIDENTIAL
  Copyright 2018-2021 Intel Corporation.

  The source code contained or described herein and all documents related to the
  source code ("Material") are owned by Intel Corporation or its suppliers or
  licensors. Title to the Material remains with Intel Corporation or its suppliers
  and licensors. The Material may contain trade secrets and proprietary and
  confidential information of Intel Corporation and its suppliers and licensors,
  and is protected by worldwide copyright and trade secret laws and treaty
  provisions. No part of the Material may be used, copied, reproduced, modified,
  published, uploaded, posted, transmitted, distributed, or disclosed in any way
  without Intel's prior express written permission.

  No license under any patent, copyright, trade secret or other intellectual
  property right is granted to or conferred upon you by disclosure or delivery
  of the Materials, either expressly, by implication, inducement, estoppel or
  otherwise. Any license under such intellectual property rights must be
  express and approved by Intel in writing.

  Unless otherwise agreed by Intel in writing, you may not remove or alter
  this notice or any other notice embedded in Materials by Intel or
  Intel's suppliers or licensors in any way.

  This file contains an 'Intel Peripheral Driver' and is uniquely identified as
  "Intel Reference Module" and is licensed for Intel CPUs and chipsets under
  the terms of your license agreement with Intel or your vendor. This file may
  be modified by the user, subject to additional terms of the license agreement.

@par Specification Reference:
**/
#ifndef _SI_FVI_H_
#define _SI_FVI_H_

///
/// Non-static SMBIOS table data to be filled later with a dynamically generated value
///

#pragma pack(1)

#define DEFAULT_FVI_TELEMETRY_VERSION   0X000000FFFFFFFFFF
#define FVI_PCH_ADL_LP_AX_HSIO_VERSION  0X0000000400000000

#pragma pack()

#define RST_DRIVER_NAME1  L"Intel RST"
#define RST_DRIVER_NAME2  L"Intel(R) RST"

typedef enum {
  Cpu_Rc_Ver= 0,
  Ucode_Ver,
  Txt_Ver
} CPU_FVI_INDEX;

typedef enum {
  EnumMeRc = 0,
  EnumMeFw
} ME_FVI_INDEX;

typedef enum {
  Rc_Ver        = 0,
  Pch_Crid_Status,
  Pch_Crid_Original,
  Pch_Crid_New,
  Raid_Ver,
  Hsio_Cnlpchlpax_Ver,
} PCH_FVI_INDEX;

#define CRID_DATA  0x69
#define CRID_LOCK  0x17

typedef enum {
  Sa_Rc_Ver = 0,
  Mem_Rc_Ver,
  Pcie_Ver,
  Crid_Status,
  Crid_Original,
  Crid_new,
  Vbios_Ver,
  Iom_Fw_Ver,
  Phy_Ver,
  Tbt_Fw_Ver,
  Sam_Fw_Ver
} SA_FVI_INDEX;

#endif
